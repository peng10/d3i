Here are the unorganized code and data for the paper "Deep Learning for High-Order Drug-Drug Interaction Prediction".
I am not able to organize the code now because I am on travel. I will organize the code and data after that.

In order to reproduce the results on BMC dataset.
Please go to ~/DDI_project/data/BMC_dataset and change the similarity profile in the 7th line of NS_run.sh.
The similarity profiles are named XXX_Jacarrd_sim.csv.
And then you can run experiments with the code ./NS_run.sh 0 0. 
The first parameter stands for using attention mechanism or not, 0: not, 1: using.
The second parameter stands for which folder to use on the 5 cross validation setting.
After experiments, you can use format_reports.sh to organize the results.

In order to reproduce results on FEARS dataset.
Please go to ~/DDI_project/sider/valid_data and type ./NS_run_negsam.sh 0 0 target 1054.
The first two parameters are descripted above. The thrid one is the short name of profile (i.e., se: side effect, indi: indication, chem: chemical structure, target).
The freq_PtrainX_XXX.txt file records the frequency of drugs in positive drug combinations. It's used in negative sampling.
After experiments, please use format_reports_negsam.sh to organize results.
