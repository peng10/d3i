import tensorflow as tf
import numpy as np

#operate Conv for each node
def node_level_Conv(node_adj_matrix, node_features, weight_self_list, weight_neig_list, b_matrix, degree_matrix, degree_max, degree_min, act):

	temp = tf.expand_dims(node_adj_matrix, 2)
	temp2 = tf.expand_dims(node_features, 0)
	neig = tf.multiply(temp2, temp)

	dm = tf.expand_dims(degree_matrix, 2)
	
	sum_list_n = []

	for i in xrange(degree_min, degree_max + 1):
		x = tf.multiply( neig, tf.expand_dims( dm[i], 0 ) )
		x = tf.reduce_sum(x, 1)
		x = tf.matmul(x, weight_neig_list[i]) + b_matrix[i]
		sum_list_n.append(x)

	sum_list_s = []

	for i in xrange(degree_min, degree_max + 1):
		y = tf.multiply( node_features, tf.expand_dims( degree_matrix[i], 1 ) )
		y = tf.matmul(y, weight_self_list[i])
		sum_list_s.append(x)

	atoms_features = tf.add_n(sum_list_s) + tf.add_n(sum_list_n)

	return act(atoms_features)



#make gamma,beta to tf.variable to updata
def node_level_normalization(atoms_features_matrix, gamma, beta, eps):

	r_dim = tf.shape(atoms_features_matrix)[0]
	mu, var = tf.nn.moments(atoms_features_matrix, axes = 1)
	mu = tf.reshape(mu, [r_dim, 1])
	var = tf.reshape(var, [r_dim, 1])
	return tf.nn.batch_normalization(atoms_features_matrix, mu, var, beta, gamma, eps)

	
def Graph_Pool(node_adj_matrix, normalized_node_feature_matrix, feature_size):

	r_dim = tf.shape(node_adj_matrix)[0]
	fill_one_node_adj_matrix = node_adj_matrix + tf.diag(tf.ones([r_dim]))

	node_and_neig = tf.multiply( tf.expand_dims(normalized_node_feature_matrix, 0), tf.expand_dims(fill_one_node_adj_matrix, 2) )
	
	poolized_matrix = tf.reduce_max(node_and_neig, 1)

	return  poolized_matrix


def Graph_Gathering(poolized_matrix, supernode_feature, weight_self_Gathering, weight_neig_Gathering, b_Gathering):

	sum_matrix = tf.reduce_sum(poolized_matrix, 0)
	sum_matrix = tf.expand_dims(sum_matrix, 0)
	Graph_feature = tf.matmul(supernode_feature, weight_self_Gathering) + tf.matmul(sum_matrix, weight_neig_Gathering) + b_Gathering

	return Graph_feature


def Combine_Multi_Graph(pool_method, Graph_feature_matrix):
	if(pool_method == 'max_pool'):
		combined_graph_feature = tf.reduce_max(Graph_feature_matrix, 0)
	if(pool_method == 'average_pool'):
		combined_graph_feature = tf.reduce_mean(Graph_feature_matrix, 0)
	
	combined_graph_feature = tf.expand_dims(combined_graph_feature, 0)

	return combined_graph_feature

def NN_relationship_encoder(combined_graph_feature_matrix, W_layers, b_layers, keep_prob, num_layers):
	for i in xrange(num_layers):
		if(i == 0):
			layer = tf.nn.tanh( tf.matmul(combined_graph_feature_matrix, W_layers[i]) + b_layers[i] )
			layer = tf.nn.dropout(layer, keep_prob = keep_prob)
		else:
			layer = tf.nn.tanh( tf.matmul(layer, W_layers[i]) + b_layers[i] )
			layer = tf.nn.dropout(layer, keep_prob = keep_prob)

	return layer


def Classifier(layer, combined_graph_matrix, class_weight, class_b):
	combination = layer + combined_graph_matrix
	hypothesis = tf.sigmoid( tf.matmul(combination, class_weight) + class_b )
	return hypothesis

def evaluation(hypothesis, Y_true, num_thresholds):
	predicted = tf.cast(hypothesis > 0.5 , dtype = tf.int32)
	accuracy = tf.reduce_mean( tf.cast( tf.equal(hypothesis, Y_true), dtype = tf.float32 ) )
	precision = tf.metrics.precision(Y_true, hypothesis)
	recall = tf.metrics.recall(Y_true, hypothesis)
	f1 = 2 * precision * recall/(precision + recall)
	auc = tf.metrics.auc(Y_ture, hypothesis, num_thresholds = num_thresholds)
	return accuracy, precision, recall, F1, AUC
	
