#!/bin/tcsh
#foreach folds (0 1 2 3 4)
foreach folds ($2)
#foreach folds (3)
	set num_test = `less PNN_entire_test$folds.txt | wc | awk '{print $1}'`
	set num_val = `less PNN_entire_validation$folds.txt | wc | awk '{print $1}'`
	set num_train = `less PN_train_$folds.txt | wc | awk '{print $1}'`

	foreach sim_file (chem_sim_1180.txt)
		#foreach size (32 128 512)
		foreach size (128)
			if($1 == 1) then
				#foreach L (16 64 128)
				foreach L (16)
					python frame_work_arbitraryOrder.py --attention $1 --train PN_train_$folds.txt --test PNN_entire_test$folds.txt --validation PNN_entire_validation$folds.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_file $sim_file --L_dim $L --freq_file freq_PNtrain$folds.txt --input_size $size --out_path ./batch_sample_results/prediction_value/  > ./batch_sample_results/log_validation_loss_rollback/log_$sim_file\_1N_attention_$1_L_$L\_feature_$size\_0_$folds.txt
					#end
				end
			else
				foreach L (32)
					#foreach layer (0 1 3 5)
					foreach layer (0)
						python frame_work_arbitraryOrder.py --attention $1 --train PN_train_$folds.txt --test PNN_entire_test$folds.txt --validation PNN_entire_validation$folds.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_file $sim_file --L_dim $L --freq_file freq_PNtrain$folds.txt --input_size $size --num_DAN $layer --max_pool 1 --out_path ./batch_sample_results/max_pool_prediction_value/  > ./batch_sample_results/max_pool/log_$sim_file\_1N_attention_$1_L_$L\_feature_$size\_$layer\_$folds.txt
					end
				end
			endif
		end
	end
end
