#!/bin/tcsh
foreach folds (0 1 2 3 4)
        foreach step (train validation test)
                set num_sample = `less PN_$step\_$folds\_run2.txt | wc | awk '{print $1}'`
                if($step == validation || $step == test) then
                        cat PN_$step\_$folds\_run2.txt PN_train_$folds\_run2.txt > temp
                        less temp | awk '{for(i=1;i<=NF-1;i++) {print $i} }' | sort | uniq -c | sort -k2n > freq_PN$step$folds\_run2.txt
                        python negsamp.py --avoid_file temp --train_file PN_$step\_$folds\_run2.txt --freq_file freq_PN$step$folds\_run2.txt --outfile PN_$step\_N_set$folds\_run2.txt
                        rm temp
                else
                        less PN_$step\_$folds\_run2.txt | awk '{for(i=1;i<=NF-1;i++) {print $i} }' | sort | uniq -c | sort -k2n > freq_PN$step$folds\_run2.txt
                        python negsamp.py --avoid_file PN_$step\_$folds\_run2.txt --train_file PN_$step\_$folds\_run2.txt --freq_file freq_PN$step$folds\_run2.txt --outfile PN_$step\_N_set$folds\_run2.txt
                endif
                cat PN_$step\_$folds\_run2.txt PN_$step\_N_set$folds\_run2.txt > PNN_entire_$step$folds\_run2.txt
        end
end
