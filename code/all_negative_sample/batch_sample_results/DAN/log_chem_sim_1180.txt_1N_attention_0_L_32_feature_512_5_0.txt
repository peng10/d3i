start to construct computing graph
graph construct over
3802
epochs start
#iterations: 0
currently lose_sum: 0.6941730976104736
time_elpased: 8.817
#iterations: 1
currently lose_sum: 0.6921882033348083
time_elpased: 8.595
#iterations: 2
currently lose_sum: 0.6910365223884583
time_elpased: 8.935
#iterations: 3
currently lose_sum: 0.6956198811531067
time_elpased: 8.954
#iterations: 4
currently lose_sum: 0.6940340995788574
time_elpased: 9.92
#iterations: 5
currently lose_sum: 0.6946144700050354
time_elpased: 8.808
#iterations: 6
currently lose_sum: 0.690402626991272
time_elpased: 9.38
#iterations: 7
currently lose_sum: 0.6920266151428223
time_elpased: 8.817
#iterations: 8
currently lose_sum: 0.691497266292572
time_elpased: 8.864
#iterations: 9
currently lose_sum: 0.6945944428443909
time_elpased: 9.258
#iterations: 10
currently lose_sum: 0.6935766339302063
time_elpased: 8.895
#iterations: 11
currently lose_sum: 0.6921612024307251
time_elpased: 8.851
#iterations: 12
currently lose_sum: 0.6900138854980469
time_elpased: 8.924
#iterations: 13
currently lose_sum: 0.6933671832084656
time_elpased: 8.797
#iterations: 14
currently lose_sum: 0.6920216083526611
time_elpased: 8.88
#iterations: 15
currently lose_sum: 0.6932928562164307
time_elpased: 8.895
#iterations: 16
currently lose_sum: 0.6924493312835693
time_elpased: 8.86
#iterations: 17
currently lose_sum: 0.6984374523162842
time_elpased: 8.907
#iterations: 18
currently lose_sum: 0.6837120056152344
time_elpased: 8.984
#iterations: 19
currently lose_sum: 0.6929271817207336
time_elpased: 8.803
start validation test
0.5032
0.506907545165
0.752365930599
0.605714285714
0.499559588676
17.298
#iterations: 20
currently lose_sum: 0.6928586363792419
time_elpased: 7.691
#iterations: 21
currently lose_sum: 0.6912558674812317
time_elpased: 7.155
#iterations: 22
currently lose_sum: 0.7000186443328857
time_elpased: 7.324
#iterations: 23
currently lose_sum: 0.6922354102134705
time_elpased: 7.263
#iterations: 24
currently lose_sum: 0.6930833458900452
time_elpased: 7.272
#iterations: 25
currently lose_sum: 0.6915977597236633
time_elpased: 7.149
#iterations: 26
currently lose_sum: 0.6890847682952881
time_elpased: 7.15
#iterations: 27
currently lose_sum: 0.692513108253479
time_elpased: 7.142
#iterations: 28
currently lose_sum: 0.6933904886245728
time_elpased: 7.114
#iterations: 29
currently lose_sum: 0.6949784159660339
time_elpased: 7.11
#iterations: 30
currently lose_sum: 0.687813401222229
time_elpased: 7.211
#iterations: 31
currently lose_sum: 0.6941325664520264
time_elpased: 7.116
#iterations: 32
currently lose_sum: 0.6907021403312683
time_elpased: 7.186
#iterations: 33
currently lose_sum: 0.6903672814369202
time_elpased: 7.091
#iterations: 34
currently lose_sum: 0.6900535821914673
time_elpased: 7.111
#iterations: 35
currently lose_sum: 0.6940863132476807
time_elpased: 7.083
#iterations: 36
currently lose_sum: 0.7079908847808838
time_elpased: 7.133
#iterations: 37
currently lose_sum: 0.6992208361625671
time_elpased: 7.079
#iterations: 38
currently lose_sum: 0.6919981241226196
time_elpased: 7.126
#iterations: 39
currently lose_sum: 0.6946802735328674
time_elpased: 7.123
start validation test
0.5108
0.630057803468
0.0859621451104
0.151283830673
0.517007046581
17.316
#iterations: 40
currently lose_sum: 0.6841863393783569
time_elpased: 7.106
#iterations: 41
currently lose_sum: 0.689278244972229
time_elpased: 7.117
#iterations: 42
currently lose_sum: 0.6973222494125366
time_elpased: 7.133
#iterations: 43
currently lose_sum: 0.6896554827690125
time_elpased: 7.844
#iterations: 44
currently lose_sum: 0.6958030462265015
time_elpased: 7.124
#iterations: 45
currently lose_sum: 0.6946917772293091
time_elpased: 7.419
#iterations: 46
currently lose_sum: 0.6911561489105225
time_elpased: 7.169
#iterations: 47
currently lose_sum: 0.6934372186660767
time_elpased: 7.193
#iterations: 48
currently lose_sum: 0.6558598279953003
time_elpased: 7.096
#iterations: 49
currently lose_sum: nan
time_elpased: 7.193
train finish final lose is: nan
acc: 0.512
pre: 0.513
rec: 0.774
F1: 0.617
auc: 0.508
