#!/bin/tcsh

foreach folds (0 1 2 3 4)
	set n=`less PNN_entire_test$folds.txt | wc | awk '{print $1}'`
	cat PNN_entire_test$folds.txt chem_sim_1180.txt_16_512_1_PNN_entire_test$folds.txt | awk -F",|\t" -v "n=$n" 'BEGIN{ORS=""}{if(NR<=n) {a[NR]=NF-1} else {for(i=1;i<=a[NR-n];i++) { if(i!=a[NR-n]) {print $i"\t"} else {print $i"\n"} } } }' > temp$folds.txt
	paste temp$folds.txt PNN_entire_test$folds.txt | awk 'BEGIN{ORS=""}{cutoff=((NF-1)/2); if(NR<=2500) {for(i=1;i<=cutoff;i++) {if(i!=cutoff) {print $i"\t"$(i+cutoff)"\t"} else {print $i"\t"$(i+cutoff)"\n"} } } }' > att_drugid_$folds.txt
	cat drug_maps.txt att_drugid_$folds.txt | awk -F";|\t" 'BEGIN{ORS=""}{if(NR<=1180) {a[NR]=$3} else {for(i=1;i<=NF;i++) {if(i!=NF) {if(i%2==0) {print a[$i]"\t"} else {print $i"\t"} } else {print a[$i]"\n"} } } }' > att_drugName$folds.txt
end
cat att_drugName0.txt att_drugName1.txt att_drugName2.txt att_drugName3.txt att_drugName4.txt > att_drugName.txt

