import sklearn.metrics 
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--ground_truth', type = str)
parser.add_argument('--prediction_value', type = str)
parser.add_argument('--out_path', type = str)
args = parser.parse_args()

ground_truth = np.loadtxt(args.ground_truth)
prediction_value = np.loadtxt(args.prediction_value)

accuracy = sklearn.metrics.accuracy_score(ground_truth, prediction_value)
precision = sklearn.metrics.precision_score(ground_truth, prediction_value)
recall = sklearn.metrics.recall_score(ground_truth, prediction_value)
F1 = 2 * recall * precision / (recall + precision)
AUC =  sklearn.metrics.roc_auc_score(ground_truth, prediction_value)

with open(args.out_path, 'w') as f:
	f.write(str(accuracy) + '\n')
	f.write(str(precision) + '\n')
	f.write(str(recall) + '\n')
	f.write(str(F1) + '\n')
	f.write(str(AUC) + '\n')

