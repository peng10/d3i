import GraphConv
import tensorflow as tf
import numpy as np
import pdb

path_adj = '../../data/sdf_1180/wen_data_order/'
path_feature = '../../data/sdf_1180/wen_data_feature/'
path_deg = '../../data/sdf_1180/wen_data_degree/'
degree_region = 9
feature_size = 20

filename = '970'
node_adj_matrix = np.loadtxt(path_adj + filename, dtype = np.float32, delimiter = ',', ndmin = 2)
node_features = np.loadtxt(path_feature + filename, dtype = np.float32, delimiter = ',', ndmin = 2)
degree_matrix = np.loadtxt(path_deg + filename, dtype = np.float32, delimiter = ',', ndmin = 2)

weight_self_list = tf.Variable(tf.random_uniform([degree_region, feature_size, feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + feature_size) ))

weight_neig_list = tf.Variable(tf.random_uniform([degree_region, feature_size, feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + feature_size) ))

b_matrix = tf.Variable(tf.random_uniform([degree_region, feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + feature_size) ))
act = tf.nn.tanh

temp = tf.expand_dims(node_adj_matrix, 2)
temp2 = tf.expand_dims(node_features, 0)
neig = tf.multiply(temp2, temp)
dm = tf.expand_dims(degree_matrix, 2)
neig_sum = tf.reduce_sum(neig, 1)
deg_list = tf.cast(tf.reduce_sum(node_adj_matrix, 1), tf.int32)
wei_self = tf.nn.embedding_lookup(weight_self_list, deg_list)
wei_neig = tf.nn.embedding_lookup(weight_neig_list, deg_list)
b = tf.nn.embedding_lookup(b_matrix, deg_list)
self_embedding = tf.matmul(tf.expand_dims(node_features, 1), wei_self)
neig_embedding = tf.matmul(tf.expand_dims(neig_sum, 1), wei_neig)
atoms_features = tf.squeeze(self_embedding + neig_embedding)
atoms_features = tf.add(atoms_features, b)
atoms_features = act(atoms_features)
r_dim = tf.shape(atoms_features)[0]
mu, var = tf.nn.moments(atoms_features, axes = 1)
mu = tf.reshape(mu, [r_dim, 1])
var = tf.reshape(var, [r_dim, 1])
bat = tf.nn.batch_normalization(atoms_features, mu, var, 0.1, 0.9, 0.001)
fill_one_node_adj_matrix = node_adj_matrix + tf.diag(tf.ones([r_dim]))
node_and_neig = tf.multiply( tf.expand_dims(bat, 0), tf.expand_dims(fill_one_node_adj_matrix, 2) )
poolized_matrix = tf.reduce_max(node_and_neig, 1)

sess=tf.Session()
sess.run(tf.global_variables_initializer())

pp = sess.run(atoms_features)
pdb.set_trace()
