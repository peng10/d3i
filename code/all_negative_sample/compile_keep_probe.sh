#!/bin/tcsh
foreach feature_size (`seq 16 16 160`)
	foreach keep_probe (`seq 0.6 0.1 1.0`)
		cat log_0_$feature_size\_$keep_probe.txt log_1_$feature_size\_$keep_probe.txt log_2_$feature_size\_$keep_probe.txt log_3_$feature_size\_$keep_probe.txt log_4_$feature_size\_$keep_probe.txt  | awk 'BEGIN{acc = 0; pre =0; rec = 0; f1=0; auc =0}{if(NR%5==2) {acc += $1}; if(NR%5==3) {pre += $1}; if(NR%5==4) {rec += $1}; if(NR%5==0) {auc += $1}}END{printf("%.3f\n",acc/5); printf("%.3f\n", pre/5); printf ("%.3f\n",rec/5); printf ( "%.3f\n", 2*(pre/5)*(rec/5)/(pre/5+rec/5) ); printf("%.3f\n", auc/5)}' > total_$feature_size\_$keep_probe
	end
end
