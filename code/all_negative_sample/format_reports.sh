#!/bin/tcsh
foreach sim_file (chem_sim_1180.txt)
	touch ./batch_reports/$sim_file\_max_$1
	foreach att (0)
		echo att: $att >> ./batch_reports/$sim_file\_max_$1
		if($att == 1) then
			foreach L (16 64 128)
				echo L: $L >> ./batch_reports/$sim_file\_$1
				foreach size (32 128 512)
					foreach folds (0 1 2 3 4)
						less ./batch_sample_results/multilayer/log_$sim_file\_1N_attention_$att\_L_$L\_feature_$size\_$1\_$folds.txt | tail -5 > temp_$folds
					end
					paste temp_0 temp_1 temp_2 temp_3 temp_4 | awk '{mean = ($2+$4+$6+$8+$10)/5.0;sum=0;for(i=2;i<=10;i=i+2) {sum=sum+($i-mean)^2};std=sqrt(sum/4.0); printf ("%.4f\tstd:\t%.4f\n", mean ,std) }' > temp$size.txt
				end
				echo "32\t\t\t128\t\t\t512" >> ./batch_reports/$sim_file\_$1
				paste temp32.txt temp128.txt temp512.txt >> ./batch_reports/$sim_file\_$1
				echo "" >> ./batch_reports/$sim_file
			end
		else
			foreach size (32 128 512)
				foreach folds (0 1 2 3 4)
						less ./batch_sample_results/max_pool/log_$sim_file\_1N_attention_$att\_L_32_feature_$size\_$1\_$folds.txt | tail -5 > temp_$folds
				end
				paste temp_0 temp_1 temp_2 temp_3 temp_4 | awk '{mean = ($2+$4+$6+$8+$10)/5.0;sum=0;for(i=2;i<=10;i=i+2) {sum=sum+($i-mean)^2};std=sqrt(sum/4.0);printf ("%.4f\tstd:\t%.4f\n", mean ,std) }' >> temp$size.txt
			end
			echo "32\t\t\t128\t\t\t512" >> ./batch_reports/$sim_file\_max_$1
			paste temp32.txt temp128.txt temp512.txt >> ./batch_reports/$sim_file\_max_$1
			echo "" >> ./batch_reports/$sim_file\_max_$1
		endif
	end
end
rm temp_0 temp_1 temp_2 temp_3 temp_4
rm temp32.txt temp128.txt temp512.txt
