#!/bin/tcsh
foreach a (0 1 2 3 4)
	if ($a == 4) then
		set b=0
	else 
		set b=`expr $a + 1`
	endif
	echo $b 
	paste z_2 labeled_total.txt | awk -v a="$a" 'BEGIN{ORS=""}{if($1==a) {for(i=2;i<=NF-1;i++) {print $i-1"\t"}; print $NF"\n"} }' > PN_test_$a.txt
	paste z_2 labeled_total.txt | awk -v b="$b" 'BEGIN{ORS=""}{if($1==b) {for(i=2;i<=NF-1;i++) {print $i-1"\t"}; print $NF"\n"} }' > PN_validation_$a.txt
	paste z_2 labeled_total.txt | awk -v a="$a" -v b="$b" 'BEGIN{ORS=""}{if($1!=a && $1!=b) {for(i=2;i<=NF-1;i++) {print $i-1"\t"}; print $NF"\n"} }' > PN_train_$a.txt
end
