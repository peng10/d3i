import tensorflow as tf
import numpy as np

#operate Conv for each node
def node_level_Conv(node_adj_matrix, node_features, weight_self_list, weight_neig_list, b_matrix, degree_matrix, degree_max, degree_min, act):

	temp = tf.expand_dims(node_adj_matrix, 2)
	temp2 = tf.expand_dims(node_features, 0)
	neig = tf.multiply(temp2, temp)

	dm = tf.expand_dims(degree_matrix, 2)
	
	sum_list_n = []

	for i in xrange(degree_min, degree_max + 1):
		x = tf.multiply( neig, tf.expand_dims( dm[i], 0 ) )
		x = tf.reduce_sum(x, 1)
		temp3 = tf.reduce_sum(x, 1)
		x_w = tf.matmul(x, weight_neig_list[i])
		temp4 = tf.cast(tf.not_equal(temp3, 0), tf.float32)
		b_temp = tf.matmul( tf.expand_dims(temp4,1), tf.expand_dims(b_matrix[i],0) )
		x_total = tf.add(x_w, b_temp)
		sum_list_n.append(x_total)

	sum_list_s = []

	for i in xrange(degree_min, degree_max + 1):
		y = tf.multiply( node_features, tf.expand_dims( degree_matrix[i], 1 ) )
		y = tf.matmul(y, weight_self_list[i])
		sum_list_s.append(y)

	atoms_features = tf.add_n(sum_list_s) + tf.add_n(sum_list_n)

	return act(atoms_features)



#make gamma,beta to tf.variable to updata
def node_level_normalization(atoms_features_matrix, gamma, beta, eps):

	r_dim = tf.shape(atoms_features_matrix)[0]
	mu, var = tf.nn.moments(atoms_features_matrix, axes = 1)
	mu = tf.reshape(mu, [r_dim, 1])
	var = tf.reshape(var, [r_dim, 1])
	return tf.nn.batch_normalization(atoms_features_matrix, mu, var, beta, gamma, eps)

	
def Graph_Pool(node_adj_matrix, normalized_node_feature_matrix, feature_size):

	r_dim = tf.shape(node_adj_matrix)[0]
	fill_one_node_adj_matrix = node_adj_matrix + tf.diag(tf.ones([r_dim]))

	node_and_neig = tf.multiply( tf.expand_dims(normalized_node_feature_matrix, 0), tf.expand_dims(fill_one_node_adj_matrix, 2) )
	
	poolized_matrix = tf.reduce_max(node_and_neig, 1)

	return  poolized_matrix


def Graph_Gathering(poolized_matrix, supernode_feature, weight_self_Gathering, weight_neig_Gathering, b_Gathering):

	sum_matrix = tf.reduce_sum(poolized_matrix, 0)
	sum_matrix = tf.expand_dims(sum_matrix, 0)
	Graph_feature = tf.matmul(supernode_feature, weight_self_Gathering) + tf.matmul(sum_matrix, weight_neig_Gathering) + b_Gathering

	return tf.nn.tanh(Graph_feature)


def Combine_Multi_Graph(Graph_feature_1, Graph_feature_2, Graph_feature_3, pair_wised_option):
	if(pair_wised_option == 1):
		combined_graph_feature_1 = tf.concat([Graph_feature_1, Graph_feature_2], 1)
		combined_graph_feature_2 = tf.concat([Graph_feature_2, Graph_feature_1], 1)
		combined_graph_feature_3 = tf.concat([Graph_feature_1, Graph_feature_3], 1)
		combined_graph_feature_4 = tf.concat([Graph_feature_3, Graph_feature_1], 1)
		combined_graph_feature_5 = tf.concat([Graph_feature_2, Graph_feature_3], 1)
		combined_graph_feature_6 = tf.concat([Graph_feature_3, Graph_feature_2], 1)
	else:
		combined_graph_feature_1 = tf.concat([Graph_feature_1, Graph_feature_2, Graph_feature_3], 1)
		combined_graph_feature_2 = tf.concat([Graph_feature_1, Graph_feature_3, Graph_feature_2], 1)
		combined_graph_feature_3 = tf.concat([Graph_feature_2, Graph_feature_1, Graph_feature_3], 1)
		combined_graph_feature_4 = tf.concat([Graph_feature_2, Graph_feature_3, Graph_feature_1], 1)
		combined_graph_feature_5 = tf.concat([Graph_feature_3, Graph_feature_1, Graph_feature_2], 1)
		combined_graph_feature_6 = tf.concat([Graph_feature_3, Graph_feature_2, Graph_feature_1], 1)

	return combined_graph_feature_1, combined_graph_feature_2, combined_graph_feature_3, combined_graph_feature_4, combined_graph_feature_5, combined_graph_feature_6

def NN_relationship_encoder(combined_graph_feature_1, combined_graph_feature_2, combined_graph_feature_3, combined_graph_feature_4, combined_graph_feature_5, combined_graph_feature_6, W_layers, b_layers, W_layers_d2, b_layers_d2, W_layers_d3, b_layers_d3, W_layers_3_combined, b_layers_3_combined, keep_prob, pair_wised_option):
	
	if(pair_wised_option == 1):
		layer_1 = tf.nn.tanh( tf.matmul(combined_graph_feature_1, W_layers) + b_layers )
		layer_1 = tf.nn.dropout(layer_1, keep_prob = keep_prob)
			
		layer_2 = tf.nn.tanh( tf.matmul(combined_graph_feature_2, W_layers) + b_layers )
		layer_2 = tf.nn.dropout(layer_2, keep_prob = keep_prob)
	
		layer_3 = tf.nn.tanh( tf.matmul(combined_graph_feature_3, W_layers_d2) + b_layers_d2 )
		layer_3 = tf.nn.dropout(layer_3, keep_prob = keep_prob)

        	layer_4 = tf.nn.tanh( tf.matmul(combined_graph_feature_4, W_layers_d2) + b_layers_d2 )
       	 	layer_4 = tf.nn.dropout(layer_4, keep_prob = keep_prob)

        	layer_5 = tf.nn.tanh( tf.matmul(combined_graph_feature_5, W_layers_d3) + b_layers_d3 )
        	layer_5 = tf.nn.dropout(layer_5, keep_prob = keep_prob)

        	layer_6 = tf.nn.tanh( tf.matmul(combined_graph_feature_6, W_layers_d3) + b_layers_d3 )
        	layer_6 = tf.nn.dropout(layer_6, keep_prob = keep_prob)
	else:
		layer_1 = tf.nn.tanh( tf.matmul(combined_graph_feature_1, W_layers_3_combined) + b_layers_3_combined)
                layer_1 = tf.nn.dropout(layer_1, keep_prob = keep_prob)
                        
                layer_2 = tf.nn.tanh( tf.matmul(combined_graph_feature_2, W_layers_3_combined) + b_layers_3_combined )
                layer_2 = tf.nn.dropout(layer_2, keep_prob = keep_prob)

                layer_3 = tf.nn.tanh( tf.matmul(combined_graph_feature_3, W_layers_3_combined) + b_layers_3_combined )
                layer_3 = tf.nn.dropout(layer_3, keep_prob = keep_prob)

                layer_4 = tf.nn.tanh( tf.matmul(combined_graph_feature_4, W_layers_3_combined) + b_layers_3_combined )
                layer_4 = tf.nn.dropout(layer_4, keep_prob = keep_prob)

                layer_5 = tf.nn.tanh( tf.matmul(combined_graph_feature_5, W_layers_3_combined) + b_layers_3_combined )
                layer_5 = tf.nn.dropout(layer_5, keep_prob = keep_prob)

                layer_6 = tf.nn.tanh( tf.matmul(combined_graph_feature_6, W_layers_3_combined) + b_layers_3_combined )
                layer_6 = tf.nn.dropout(layer_6, keep_prob = keep_prob)

	layer = tf.concat([layer_1, layer_2, layer_3, layer_4, layer_5, layer_6], 0)
	layer = tf.reduce_sum(layer, 0)

	return tf.expand_dims(layer, 0)


def Classifier(layer, graph_feature_1, graph_feature_2, graph_feature_3, class_weight, class_b, option):
	if(option == 1):
		combination = layer + graph_feature_1 + graph_feature_2 + graph_feature_3
	else:
		combination = layer
	hypothesis = tf.sigmoid( tf.matmul(combination, class_weight) + class_b )
	return hypothesis

def evaluation(hypothesis, Y_true, num_thresholds):
	predicted = tf.cast(hypothesis > 0.5 , dtype = tf.int32)
	accuracy = tf.reduce_mean( tf.cast( tf.equal(hypothesis, Y_true), dtype = tf.float32 ) )
	precision = tf.metrics.precision(Y_true, hypothesis)
	recall = tf.metrics.recall(Y_true, hypothesis)
	f1 = 2 * precision * recall/(precision + recall)
	auc = tf.metrics.auc(Y_ture, hypothesis, num_thresholds = num_thresholds)
	return accuracy, precision, recall, F1, AUC
	
