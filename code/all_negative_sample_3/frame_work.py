import tensorflow as tf
import numpy as np
import argparse
import GraphConv
import Graph_feature
import os
import re
import random
import pdb
import sys
import math

np.random.seed(234)
tf.set_random_seed(234)
random.seed(123)

path_adj = '../../data/sdf_1180/drug_3_all_ns_order/'
path_feature = '../../data/sdf_1180/drug_3_all_ns_feature/'
path_degree = '../../data/sdf_1180/drug_3_all_ns_degree_matrix/'

parser = argparse.ArgumentParser()
parser.add_argument('--train', default = 'train_0.txt', type = str)
parser.add_argument('--test', default = 'test_0.txt', type = str)
parser.add_argument('--num_train_sample', default = 2088, type = int)
parser.add_argument('--num_test_sample', default = 522, type = int)
parser.add_argument('--feature_size', default = 30, type = int)
parser.add_argument('--graph_feature_size', default = 96, type = int)
parser.add_argument('--batch_size', default = 1, type = int)
parser.add_argument('--max_degree', default = 8, type = int)
parser.add_argument('--min_degree', default = 0, type = int)
parser.add_argument('--pool_method', default = 'max_pool', type = str)
parser.add_argument('--num_layers', default = 1, type = int)
parser.add_argument('--keep_probe', default = 1.0, type = float)
parser.add_argument('--option', default = 1, type = int)
parser.add_argument('--pair_wised_option', default = 1, type = int)

args = parser.parse_args()

file_train = '../../data/modified_negative_sample_3/' + args.train
file_test = '../../data/modified_negative_sample_3/' + args.test

graph_adj = {}
graph_feature = {}
graph_degree = {}

degree_region = args.max_degree - args.min_degree + 1
degree_max = args.max_degree
degree_min = args.min_degree
feature_size = args.feature_size
graph_feature_size = args.graph_feature_size
num_layers = args.num_layers
keep_probe = args.keep_probe
batch_size = args.batch_size
option = args.option
pair_wised_option = args.pair_wised_option

for filename in os.listdir(path_adj):
	graph_adj[int(filename)] = np.loadtxt(path_adj + filename, dtype = np.float32, delimiter = ',', ndmin = 2)

for filename in os.listdir(path_feature):
	graph_feature[int(filename)] = np.loadtxt(path_feature + filename, dtype = np.float32, delimiter = ',', ndmin = 2)

for filename in os.listdir(path_degree):
	graph_degree[int(filename)] = np.loadtxt(path_degree + filename, dtype = np.float32, delimiter = ',', ndmin = 2)

#placeholder
label = tf.placeholder(tf.float32)

X_1_adj = tf.placeholder(tf.float32)
X_1_feature = tf.placeholder(tf.float32)
X_1_degree_matrix = tf.placeholder(tf.float32)

X_2_adj = tf.placeholder(tf.float32)
X_2_feature = tf.placeholder(tf.float32)
X_2_degree_matrix = tf.placeholder(tf.float32)

X_3_adj = tf.placeholder(tf.float32)
X_3_feature = tf.placeholder(tf.float32)
X_3_degree_matrix = tf.placeholder(tf.float32)

#learning_rate = tf.placeholder(tf.float32)

#initialize weight
weight_self_list = tf.Variable(tf.random_uniform([degree_region, feature_size, feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + feature_size) ))

weight_neig_list = tf.Variable(tf.random_uniform([degree_region, feature_size, feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + feature_size) ))

b_matrix = tf.Variable(tf.random_uniform([degree_region, feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + feature_size) ))

supernode_feature = tf.Variable(tf.random_uniform([1, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

weight_self_Gathering = tf.Variable(tf.random_uniform([graph_feature_size, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

weight_neig_Gathering = tf.Variable(tf.random_uniform([feature_size, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + graph_feature_size) ))

b_Gathering = tf.Variable(tf.random_uniform([graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

W_layers = tf.Variable(tf.random_uniform([2 * graph_feature_size, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(2 * graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(2 * graph_feature_size + graph_feature_size) ))

W_layers_d2 = tf.Variable(tf.random_uniform([2 * graph_feature_size, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(2 * graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(2 * graph_feature_size + graph_feature_size) ))

W_layers_d3 = tf.Variable(tf.random_uniform([2 * graph_feature_size, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(2 * graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(2 * graph_feature_size + graph_feature_size) ))

W_layers_3_combined = tf.Variable(tf.random_uniform([3 * graph_feature_size, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(2 * graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(2 * graph_feature_size + graph_feature_size) ))

b_layers_3_combined = tf.Variable(tf.random_uniform([graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

b_layers = tf.Variable(tf.random_uniform([graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

b_layers_d2 = tf.Variable(tf.random_uniform([graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

b_layers_d3 = tf.Variable(tf.random_uniform([graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

W_layers_2 = tf.Variable(tf.random_uniform([graph_feature_size, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

b_layers_2 = tf.Variable(tf.random_uniform([graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

class_weight = tf.Variable(tf.random_uniform([graph_feature_size, 1], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + 1), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + 1) ))

class_b = tf.Variable(tf.random_uniform([1], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

gamma = tf.Variable(0.90)
beta = tf.Variable(0.10)
eps = 0.001

#construct graph
Graph_feature_1 = Graph_feature.Graph_feature(X_1_adj, X_1_feature, X_1_degree_matrix, weight_self_list, weight_neig_list, b_matrix, tf.nn.tanh, degree_max, degree_min, feature_size, graph_feature_size, weight_self_Gathering, weight_neig_Gathering, b_Gathering, supernode_feature, gamma, beta, eps)
	
Graph_feature_2 = Graph_feature.Graph_feature(X_2_adj, X_2_feature, X_2_degree_matrix, weight_self_list, weight_neig_list, b_matrix, tf.nn.tanh, degree_max, degree_min, feature_size, graph_feature_size, weight_self_Gathering, weight_neig_Gathering, b_Gathering, supernode_feature, gamma, beta, eps)

Graph_feature_3 = Graph_feature.Graph_feature(X_3_adj, X_3_feature, X_3_degree_matrix, weight_self_list, weight_neig_list, b_matrix, tf.nn.tanh, degree_max, degree_min, feature_size, graph_feature_size, weight_self_Gathering, weight_neig_Gathering, b_Gathering, supernode_feature, gamma, beta, eps)

combined_graph_feature_1, combined_graph_feature_2, combined_graph_feature_3, combined_graph_feature_4, combined_graph_feature_5, combined_graph_feature_6 = GraphConv.Combine_Multi_Graph(Graph_feature_1, Graph_feature_2, Graph_feature_3, pair_wised_option)

layer = GraphConv.NN_relationship_encoder(combined_graph_feature_1, combined_graph_feature_2, combined_graph_feature_3, combined_graph_feature_4, combined_graph_feature_5, combined_graph_feature_6, W_layers, b_layers, W_layers_d2, b_layers_d2, W_layers_d3, b_layers_d3, W_layers_3_combined, b_layers_3_combined, keep_probe, pair_wised_option)

hypothesis = GraphConv.Classifier(layer, Graph_feature_1, Graph_feature_2, Graph_feature_3, class_weight, class_b, option)
		
cost = -tf.reduce_mean(label * tf.log(hypothesis) + (1 - label) * tf.log(1 - hypothesis))

#training step
global_step = tf.Variable(0, trainable=False)
starter_learning_rate = 1e-5
learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step, 50000, 0.80, staircase=True)

train_op = tf.train.AdamOptimizer(learning_rate, epsilon = 1e-2).minimize(cost, global_step = global_step)

session_conf = tf.ConfigProto(
      intra_op_parallelism_threads=12,
      inter_op_parallelism_threads=12)

sess = tf.Session(config=session_conf)
sess.run(tf.global_variables_initializer())

with open(file_train, 'r') as f:
	total_lines = f.read().splitlines() 

#learning_rate_adaptive = 1e-4
#decay = 0.8

print len(total_lines)

last_lose = 0
for i in range(60):

	sum_lose = 0
	random.shuffle(total_lines)

	for j in range(args.num_train_sample):
		rint = np.random.randint(0, args.num_train_sample)
		ind_1, ind_2, ind_3, label_d = (int(e) for e in total_lines[rint].rstrip('\n').rstrip('\t').rstrip(' ').split('\t'))
		feed_dict = {X_1_adj:graph_adj[ind_1], X_1_feature:graph_feature[ind_1], X_1_degree_matrix:graph_degree[ind_1], X_2_adj:graph_adj[ind_2], X_2_feature:graph_feature[ind_2], X_2_degree_matrix:graph_degree[ind_2], X_3_adj:graph_adj[ind_3], X_3_feature:graph_feature[ind_3], X_3_degree_matrix:graph_degree[ind_3], label:label_d}

	 	cos, _ = sess.run([cost, train_op], feed_dict)
		sum_lose += cos

	#print sum_lose
	if(sum_lose <= 300 or abs(last_lose - sum_lose) <= 5):
		break
	else:
		last_lose = sum_lose

predicted = tf.cast(hypothesis > 0.5 , dtype = tf.int32)
predicted_list = []
label_list = []

with open(file_test, 'r') as f:
	for line in f:
		ind_1, ind_2, ind_3, label_d = (int(e) for e in line.rstrip('\n').rstrip('\t').rstrip(' ').split('\t'))
		feed_dict_test = {X_1_adj:graph_adj[ind_1], X_1_feature:graph_feature[ind_1], X_1_degree_matrix:graph_degree[ind_1], X_2_adj:graph_adj[ind_2], X_2_feature:graph_feature[ind_2], X_2_degree_matrix:graph_degree[ind_2], X_3_adj:graph_adj[ind_3], X_3_feature:graph_feature[ind_3], X_3_degree_matrix:graph_degree[ind_3], label:label_d}
		temp = sess.run(predicted, feed_dict_test)
		temp = sess.run(tf.reshape(temp,[]))
		predicted_list.append(temp)
		label_list.append(label_d)

#print label_list
#print predicted_list

predicted_list = tf.reshape(predicted_list, [args.num_test_sample])
accuracy = tf.reduce_mean( tf.cast( tf.equal(predicted_list, label_list), dtype = tf.float32 ) )

print sess.run(accuracy)

label_list = tf.reshape(label_list, [1, args.num_test_sample])
predicted_list = tf.reshape(predicted_list, [1, args.num_test_sample])

precision = tf.metrics.precision(label_list, predicted_list)
recall = tf.metrics.recall(label_list, predicted_list)
auc = tf.metrics.auc(label_list, predicted_list)

sess.run(tf.local_variables_initializer())
print sess.run(precision)[1]
print sess.run(recall)[1]
print sess.run(auc)[1]
