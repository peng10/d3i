import os
import numpy as np

path = "../data/sdf_1180/sdf/"
path_save = "../data/sdf_1180/adj_matrix/"
for filename in os.listdir(path):
	if(filename.startswith('DB') or filename.startswith('CID')):
		with open(path + filename, 'r') as f:
			row_num = 0
			marker = 0
			for line in f:
				if(row_num == 3):
					num_vertex = int(line[0:3])
					num_edge = int(line[3:6])
					adj_matrix = np.zeros([num_vertex,num_vertex])
					marker = 1
				if( marker == 1 and row_num >= (3 + num_vertex + 1) and row_num <= (3 + num_vertex + num_edge) ):
					row_index = int(line[0:3]) - 1
					column_index = int(line[3:6]) - 1
					adj_matrix[row_index][column_index] = 1
					adj_matrix[column_index][row_index] = 1
				if( marker == 1 and row_num > (3 + num_vertex + num_edge) ):
					break
				
			
				row_num += 1

			np.savetxt(path_save + filename.split('.')[0] + '_matrix', adj_matrix, fmt = '%d', delimiter=',')

