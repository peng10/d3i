import scipy.io as sio
import numpy as np
import random
import argparse
import pdb

path = '../data/'
random.seed(123)
np.random.seed(123)

parser = argparse.ArgumentParser()
parser.add_argument('--num_sample', default = 4500, type = int)
args = parser.parse_args()

with open(path + 'drug_frequency_order_2.txt', 'r') as f:
        pre = f.read().splitlines()

pre_t = [int(e.lstrip(' ').split(' ')[0]) for e in pre]
lines = [int(e.lstrip(' ').split(' ')[1]) for e in pre]

temp = sum(pre_t)
preb = [(float(e)/temp) for e in pre_t]

candidate_list = []
lose_list = []
#pdb.set_trace()

mt_mat = sio.loadmat(path + 'selected_sparse_matrix.mat')
sparse_matrix = mt_mat['H']
print sparse_matrix.shape
#sparse_matrix = sparse_matrix[0:3000,:]

counter = 0
counter_test_751 = 0
counter_test_888 = 0
error = 0

for i in xrange(args.num_sample):
	#pdb.set_trace()
	counter_2 = 0
	while True:
		#pdb.set_trace()
		drug_1, drug_2 = np.random.choice(len(lines), 2, p = preb)
		#if(drug_1 == 234 or drug_2 == 234):
		#	counter_test_751 += 1
		#elif(drug_1 == 275 or drug_2 == 275):
		#	counter_test_888 += 1
		#pdb.set_trace()
		counter_resample = 1		
		
		#print drug_1, drug_2
		#pdb.set_trace()
        	while(drug_1 == drug_2 or (drug_1, drug_2) in candidate_list or (drug_2, drug_1) in candidate_list or (drug_1, drug_2) in lose_list):
			#pdb.set_trace()
			drug_1, drug_2 = np.random.choice(len(lines), 2, p = preb)

			counter_resample += 1
			if(counter_resample > 90000):
				print 'gg'
				break
		
		if(counter_resample > 90000):
			error += 1
			print 'gg'
			break

		#print "number_resample: " + str(counter_resample)

		sum_drug_1_2 = sparse_matrix[:,lines[drug_1]-1] + sparse_matrix[:,lines[drug_2]-1]
		#pdb.set_trace()
		if( np.any(sum_drug_1_2.todense() > 1) ):
			lose_list.append((drug_1,drug_2))
			lose_list.append((drug_2,drug_1))
                        counter_2 += 1
		else:
			candidate_list.append((drug_1,drug_2))
			break
		
	counter+=1
	print 'counter: ' + str(counter)
	print 'counter_2: ' + str(counter_2)
	#print 'num_751: ' + str(counter_test_751)
	#print 'num_888: ' + str(counter_test_888)
	print 'num_error: ' + str(error)


f = open(path + 'negtive_sample_test.txt','w')
for e in candidate_list:
	a, b= e
	f.write(str(lines[a]) + '\t' + str(lines[b]) + '\n')

f.close()

f = open(path + 'lose_list.txt','w')
for line in lose_list:
        f.write(str(line[0]) + '\t' + str(line[1]) +'\n')

f.close()

		
	
			 



