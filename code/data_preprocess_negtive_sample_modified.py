import scipy.io as sio
import numpy as np
import random
import argparse

path = '../data/modified_negative_sample_3/'
random.seed(123)
np.random.seed(123)

parser = argparse.ArgumentParser()
parser.add_argument('--num_sample', default = 1305, type = int)
args = parser.parse_args()

candidate_list = []
lose_list = []
drug_num = 1180

mt_mat = sio.loadmat(path + 'selected_sparse_matrix.mat')
sparse_matrix = mt_mat['H']
print sparse_matrix.shape
#sparse_matrix = sparse_matrix[0:3000,:]

counter = 0

for i in xrange(args.num_sample):
	
	print 'nb'
	counter_2 = 0
	while True:

        	drug_1 = np.random.randint(1, drug_num + 1)
       	 	drug_2 = np.random.randint(1, drug_num + 1)
		drug_3 = np.random.randint(1, drug_num + 1)
		counter_resample = 1		
		
		#print drug_1, drug_2

        	while(drug_1 == drug_2 or drug_1 == drug_3 or drug_2 == drug_3 or (str(drug_1) + ' ' + str(drug_2) + ' ' + str(drug_3)) in candidate_list or (str(drug_1) + ' ' + str(drug_2) + ' ' + str(drug_3)) in lose_list):

                	drug_1 = np.random.randint(1, drug_num + 1)
                	drug_2 = np.random.randint(1, drug_num + 1)
			drug_3 = np.random.randint(1, drug_num + 1)

			counter_resample += 1
			if(counter_resample > 90000):
				i = args.num_sample - 1
				break
		
		if(counter_resample > 90000):
			break

		#print "number_resample: " + str(counter_resample)

		sum_drug_1_2_3 = sparse_matrix[:, drug_1 - 1] + sparse_matrix[:, drug_2 - 1] + sparse_matrix[:, drug_3 - 1]

		if( np.any(sum_drug_1_2_3.todense() > 1) ):
			lose_list.append(str(drug_1) + ' ' + str(drug_2) + ' ' + str(drug_3))
                        counter_2 += 1
		else:
			candidate_list.append(str(drug_1) + ' ' + str(drug_2) + ' ' + str(drug_3))
			break
		
	counter+=1
	print 'counter: ' + str(counter)
	print 'counter_2: ' + str(counter_2)


f = open(path + 'negtive_sample_pool.txt','w')
for line in candidate_list:
	a, b, c = [int(e) for e in line.split(' ')]
	f.write(str(a) + ' ' +str(b) + ' ' + str(c) + '\n')

f.close()

f = open(path + 'lose_list.txt','w')
for line in lose_list:
        f.write(line+'\n')

f.close()

		
	
			 



