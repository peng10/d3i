import scipy.io as sio
import numpy as np
import random
import argparse
import pdb

path = '../data/'
random.seed(123)
np.random.seed(123)

parser = argparse.ArgumentParser()
parser.add_argument('--num_sample', default = 2981, type = int)
args = parser.parse_args()

with open(path + 'order_2_used_drug_all.txt', 'r') as f:
	lines = f.read().splitlines()

lines = [int(e) for e in lines]

with open(path + 'drug_frequency_order_2_all.txt', 'r') as f:
        preb = f.read().splitlines()

preb = [int(e.lstrip(' ').split(' ')[0]) for e in preb]

temp = sum(preb)
preb = [float(e) / sum(preb) for e in preb]
#pdb.set_trace()

candidate_list = []
lose_list = []

mt_mat = sio.loadmat(path + 'selected_sparse_matrix.mat')
sparse_matrix = mt_mat['H']
print sparse_matrix.shape
#sparse_matrix = sparse_matrix[0:3000,:]

counter = 0
error_count = 0

with open(path + 'positive_sample_2_all.txt', 'r') as f:
	positive_lines = f.read().splitlines()
#pdb.set_trace()

for e in positive_lines:
	error = 0
	tail = 0
	head = 0
	print 'nb'
	counter_2 = 0
	a,b,c = map(int, e.rstrip('\t').split('\t'))

	if(int(a) > int(b)):
		tail = 1
	if(int(a) < int(b)):
		head = 1

	while True:
        	drug_replacement = np.random.choice(len(lines), p = preb)

		counter_resample = 1
		
		#print drug_1, drug_2
		if(tail == 1):
        		while(lines[drug_replacement] == a or lines[drug_replacement] == b or (a, lines[drug_replacement]) in candidate_list or (a, lines[drug_replacement]) in lose_list):

				drug_replacement = np.random.choice(len(lines), p = preb)

				counter_resample += 1
				if(counter_resample > 90000):
					break
		
		if(counter_resample > 90000):
			error = 1
			error_count += 1
			print 'gg'
			break
		
		elif(head == 1):
                        while(lines[drug_replacement] == a or lines[drug_replacement] == b or (lines[drug_replacement], b) in candidate_list or (lines[drug_replacement], b) in lose_list):

                                drug_replacement = np.random.choice(len(lines), p = preb)

                                counter_resample += 1
                                if(counter_resample > 90000):
                                        break
		
		if(counter_resample > 90000):
			error = 1
			error_count += 1
			print 'gg'
                        break

		if(tail == 1):
			sum_drug_1_2 = sparse_matrix[:,lines[drug_replacement]-1] + sparse_matrix[:,a-1]
		elif(head == 1):
			sum_drug_1_2 = sparse_matrix[:,lines[drug_replacement]-1] + sparse_matrix[:,b-1]

		if( np.any(sum_drug_1_2.todense() > 1) ):
			if(tail == 1):
				lose_list.append((a, lines[drug_replacement]))
			elif(head == 1):
				lose_list.append((lines[drug_replacement], b))
                        counter_2 += 1
		else:
			if(tail == 1):
                                candidate_list.append((a, lines[drug_replacement]))
                        elif(head == 1):
                                candidate_list.append((lines[drug_replacement], b))
			break
		
	counter+=1
	print 'counter: ' + str(counter)
	print 'counter_2: ' + str(counter_2)
	print 'error_count: ' + str(error_count)


f = open(path + 'negtive_sample_pool.txt','w')
for e in candidate_list:
	a, b= e
	f.write(str(a) + '\t' + str(b) + '\n')

f.close()

f = open(path + 'lose_list.txt','w')
for line in lose_list:
        f.write(str(line[0]) + '\t' + str(line[1]) +'\n')

f.close()

		
	
			 



