start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 404.8572036623955
time_elpased: 0.957
batch start
#iterations: 1
currently lose_sum: 403.6550664305687
time_elpased: 0.925
batch start
#iterations: 2
currently lose_sum: 403.15890538692474
time_elpased: 0.916
batch start
#iterations: 3
currently lose_sum: 402.5814749598503
time_elpased: 0.925
batch start
#iterations: 4
currently lose_sum: 402.4674592614174
time_elpased: 0.915
batch start
#iterations: 5
currently lose_sum: 402.2761412858963
time_elpased: 0.929
batch start
#iterations: 6
currently lose_sum: 401.63022553920746
time_elpased: 0.93
batch start
#iterations: 7
currently lose_sum: 401.69983917474747
time_elpased: 0.942
batch start
#iterations: 8
currently lose_sum: 401.0212024450302
time_elpased: 0.93
batch start
#iterations: 9
currently lose_sum: 400.50135070085526
time_elpased: 0.924
batch start
#iterations: 10
currently lose_sum: 399.7047699689865
time_elpased: 0.926
batch start
#iterations: 11
currently lose_sum: 399.32972997426987
time_elpased: 0.923
batch start
#iterations: 12
currently lose_sum: 399.13343143463135
time_elpased: 0.92
batch start
#iterations: 13
currently lose_sum: 398.2308138012886
time_elpased: 0.922
batch start
#iterations: 14
currently lose_sum: 397.6264018416405
time_elpased: 0.928
batch start
#iterations: 15
currently lose_sum: 397.494409263134
time_elpased: 0.925
batch start
#iterations: 16
currently lose_sum: 396.99141377210617
time_elpased: 0.92
batch start
#iterations: 17
currently lose_sum: 396.3604943752289
time_elpased: 0.918
batch start
#iterations: 18
currently lose_sum: 395.31157702207565
time_elpased: 0.921
batch start
#iterations: 19
currently lose_sum: 395.48467141389847
time_elpased: 0.925
start validation test
0.274639175258
1.0
0.274639175258
0.430928502103
0
validation finish
batch start
#iterations: 20
currently lose_sum: 393.93280774354935
time_elpased: 0.922
batch start
#iterations: 21
currently lose_sum: 393.5963053703308
time_elpased: 0.919
batch start
#iterations: 22
currently lose_sum: 393.68397384881973
time_elpased: 0.923
batch start
#iterations: 23
currently lose_sum: 392.23560082912445
time_elpased: 0.926
batch start
#iterations: 24
currently lose_sum: 392.3210847377777
time_elpased: 0.924
batch start
#iterations: 25
currently lose_sum: 391.7768896818161
time_elpased: 0.911
batch start
#iterations: 26
currently lose_sum: 390.8498262166977
time_elpased: 0.918
batch start
#iterations: 27
currently lose_sum: 390.92617958784103
time_elpased: 0.91
batch start
#iterations: 28
currently lose_sum: 390.04919934272766
time_elpased: 0.924
batch start
#iterations: 29
currently lose_sum: 389.71538400650024
time_elpased: 0.915
batch start
#iterations: 30
currently lose_sum: 390.4090515971184
time_elpased: 0.926
batch start
#iterations: 31
currently lose_sum: 389.04499554634094
time_elpased: 0.926
batch start
#iterations: 32
currently lose_sum: 387.90500646829605
time_elpased: 0.934
batch start
#iterations: 33
currently lose_sum: 387.8693656921387
time_elpased: 0.913
batch start
#iterations: 34
currently lose_sum: 388.2765939235687
time_elpased: 0.921
batch start
#iterations: 35
currently lose_sum: 387.37825548648834
time_elpased: 0.913
batch start
#iterations: 36
currently lose_sum: 388.49259263277054
time_elpased: 0.916
batch start
#iterations: 37
currently lose_sum: 386.6252728700638
time_elpased: 0.917
batch start
#iterations: 38
currently lose_sum: 387.08988958597183
time_elpased: 0.907
batch start
#iterations: 39
currently lose_sum: 386.4620813727379
time_elpased: 0.915
start validation test
0.420103092784
1.0
0.420103092784
0.59165154265
0
validation finish
batch start
#iterations: 40
currently lose_sum: 386.7711378335953
time_elpased: 0.908
batch start
#iterations: 41
currently lose_sum: 385.84524607658386
time_elpased: 0.923
batch start
#iterations: 42
currently lose_sum: 385.0044538974762
time_elpased: 0.919
batch start
#iterations: 43
currently lose_sum: 385.6798965334892
time_elpased: 0.928
batch start
#iterations: 44
currently lose_sum: 386.3345172405243
time_elpased: 0.933
batch start
#iterations: 45
currently lose_sum: 383.9017214179039
time_elpased: 0.916
batch start
#iterations: 46
currently lose_sum: 383.65811240673065
time_elpased: 0.907
batch start
#iterations: 47
currently lose_sum: 384.8561635017395
time_elpased: 0.915
batch start
#iterations: 48
currently lose_sum: 384.38443291187286
time_elpased: 0.922
batch start
#iterations: 49
currently lose_sum: 383.91986441612244
time_elpased: 0.955
batch start
#iterations: 50
currently lose_sum: 384.10750526189804
time_elpased: 0.969
batch start
#iterations: 51
currently lose_sum: 382.96753907203674
time_elpased: 0.929
batch start
#iterations: 52
currently lose_sum: 382.4826447367668
time_elpased: 0.919
batch start
#iterations: 53
currently lose_sum: 382.5424665212631
time_elpased: 0.92
batch start
#iterations: 54
currently lose_sum: 382.65520018339157
time_elpased: 0.924
batch start
#iterations: 55
currently lose_sum: 383.19521856307983
time_elpased: 0.921
batch start
#iterations: 56
currently lose_sum: 381.93739825487137
time_elpased: 0.919
batch start
#iterations: 57
currently lose_sum: 383.2509194612503
time_elpased: 0.924
batch start
#iterations: 58
currently lose_sum: 383.09303337335587
time_elpased: 0.929
batch start
#iterations: 59
currently lose_sum: 382.391734957695
time_elpased: 0.957
start validation test
0.671958762887
1.0
0.671958762887
0.803798248859
0
validation finish
batch start
#iterations: 60
currently lose_sum: 383.6564631462097
time_elpased: 0.919
batch start
#iterations: 61
currently lose_sum: 381.4632006883621
time_elpased: 0.946
batch start
#iterations: 62
currently lose_sum: 381.5673475265503
time_elpased: 0.932
batch start
#iterations: 63
currently lose_sum: 382.22993195056915
time_elpased: 0.929
batch start
#iterations: 64
currently lose_sum: 380.9865777492523
time_elpased: 0.95
batch start
#iterations: 65
currently lose_sum: 380.1150177717209
time_elpased: 0.937
batch start
#iterations: 66
currently lose_sum: 382.7783449292183
time_elpased: 0.938
batch start
#iterations: 67
currently lose_sum: 382.5911561846733
time_elpased: 0.938
batch start
#iterations: 68
currently lose_sum: 380.83217418193817
time_elpased: 0.936
batch start
#iterations: 69
currently lose_sum: 380.29984706640244
time_elpased: 0.937
batch start
#iterations: 70
currently lose_sum: 380.22829765081406
time_elpased: 0.942
batch start
#iterations: 71
currently lose_sum: 380.40162098407745
time_elpased: 0.938
batch start
#iterations: 72
currently lose_sum: 380.9558479785919
time_elpased: 0.992
batch start
#iterations: 73
currently lose_sum: 381.0640477538109
time_elpased: 0.949
batch start
#iterations: 74
currently lose_sum: 381.9828255176544
time_elpased: 0.957
batch start
#iterations: 75
currently lose_sum: 379.6872201561928
time_elpased: 0.948
batch start
#iterations: 76
currently lose_sum: 379.67895156145096
time_elpased: 0.931
batch start
#iterations: 77
currently lose_sum: 379.9156448841095
time_elpased: 0.918
batch start
#iterations: 78
currently lose_sum: 379.18175745010376
time_elpased: 0.924
batch start
#iterations: 79
currently lose_sum: 380.1749014854431
time_elpased: 0.923
start validation test
0.79
1.0
0.79
0.882681564246
0
validation finish
batch start
#iterations: 80
currently lose_sum: 379.97238075733185
time_elpased: 0.928
batch start
#iterations: 81
currently lose_sum: 380.3273739218712
time_elpased: 0.949
batch start
#iterations: 82
currently lose_sum: 379.9660452604294
time_elpased: 0.929
batch start
#iterations: 83
currently lose_sum: 379.8506757616997
time_elpased: 0.936
batch start
#iterations: 84
currently lose_sum: 379.27831679582596
time_elpased: 0.964
batch start
#iterations: 85
currently lose_sum: 380.0183325409889
time_elpased: 0.958
batch start
#iterations: 86
currently lose_sum: 379.2066481113434
time_elpased: 0.919
batch start
#iterations: 87
currently lose_sum: 379.2494840621948
time_elpased: 0.927
batch start
#iterations: 88
currently lose_sum: 379.2551694512367
time_elpased: 0.926
batch start
#iterations: 89
currently lose_sum: 379.42977291345596
time_elpased: 0.97
batch start
#iterations: 90
currently lose_sum: 378.983909368515
time_elpased: 0.931
batch start
#iterations: 91
currently lose_sum: 378.71656250953674
time_elpased: 0.938
batch start
#iterations: 92
currently lose_sum: 379.0591167807579
time_elpased: 0.931
batch start
#iterations: 93
currently lose_sum: 380.0884566307068
time_elpased: 0.928
batch start
#iterations: 94
currently lose_sum: 379.1210615634918
time_elpased: 0.937
batch start
#iterations: 95
currently lose_sum: 378.8975598216057
time_elpased: 0.927
batch start
#iterations: 96
currently lose_sum: 378.12405973672867
time_elpased: 0.93
batch start
#iterations: 97
currently lose_sum: 378.08478677272797
time_elpased: 0.918
batch start
#iterations: 98
currently lose_sum: 378.68691086769104
time_elpased: 0.954
batch start
#iterations: 99
currently lose_sum: 379.06097930669785
time_elpased: 0.93
start validation test
0.677422680412
1.0
0.677422680412
0.807694671501
0
validation finish
batch start
#iterations: 100
currently lose_sum: 379.27253210544586
time_elpased: 0.921
batch start
#iterations: 101
currently lose_sum: 378.4373470544815
time_elpased: 0.949
batch start
#iterations: 102
currently lose_sum: 377.78583139181137
time_elpased: 0.923
batch start
#iterations: 103
currently lose_sum: 378.31016379594803
time_elpased: 0.916
batch start
#iterations: 104
currently lose_sum: 378.451435148716
time_elpased: 1.014
batch start
#iterations: 105
currently lose_sum: 378.05681359767914
time_elpased: 0.915
batch start
#iterations: 106
currently lose_sum: 379.8742221593857
time_elpased: 0.92
batch start
#iterations: 107
currently lose_sum: 378.56275337934494
time_elpased: 0.934
batch start
#iterations: 108
currently lose_sum: 378.1545171737671
time_elpased: 0.923
batch start
#iterations: 109
currently lose_sum: 377.98894757032394
time_elpased: 0.947
batch start
#iterations: 110
currently lose_sum: 377.7799841761589
time_elpased: 0.934
batch start
#iterations: 111
currently lose_sum: 377.3866397738457
time_elpased: 0.926
batch start
#iterations: 112
currently lose_sum: 377.4310728907585
time_elpased: 0.935
batch start
#iterations: 113
currently lose_sum: 379.8652683496475
time_elpased: 0.931
batch start
#iterations: 114
currently lose_sum: 379.44506174325943
time_elpased: 0.927
batch start
#iterations: 115
currently lose_sum: 378.528560757637
time_elpased: 0.943
batch start
#iterations: 116
currently lose_sum: 377.92004692554474
time_elpased: 0.943
batch start
#iterations: 117
currently lose_sum: 377.2612944841385
time_elpased: 0.931
batch start
#iterations: 118
currently lose_sum: 377.8679878115654
time_elpased: 0.949
batch start
#iterations: 119
currently lose_sum: 379.0173678994179
time_elpased: 0.935
start validation test
0.740824742268
1.0
0.740824742268
0.851119270402
0
validation finish
batch start
#iterations: 120
currently lose_sum: 376.17495745420456
time_elpased: 0.945
batch start
#iterations: 121
currently lose_sum: 379.3646851181984
time_elpased: 0.927
batch start
#iterations: 122
currently lose_sum: 377.0705373287201
time_elpased: 0.948
batch start
#iterations: 123
currently lose_sum: 377.4634866118431
time_elpased: 0.924
batch start
#iterations: 124
currently lose_sum: 377.28536731004715
time_elpased: 0.924
batch start
#iterations: 125
currently lose_sum: 377.77034986019135
time_elpased: 0.923
batch start
#iterations: 126
currently lose_sum: 377.88672000169754
time_elpased: 0.922
batch start
#iterations: 127
currently lose_sum: 378.0102416872978
time_elpased: 0.937
batch start
#iterations: 128
currently lose_sum: 378.5237690806389
time_elpased: 0.935
batch start
#iterations: 129
currently lose_sum: 377.62349385023117
time_elpased: 0.947
batch start
#iterations: 130
currently lose_sum: 377.6870760321617
time_elpased: 0.965
batch start
#iterations: 131
currently lose_sum: 376.6303805112839
time_elpased: 0.937
batch start
#iterations: 132
currently lose_sum: 378.84459483623505
time_elpased: 0.925
batch start
#iterations: 133
currently lose_sum: 376.3295151591301
time_elpased: 0.954
batch start
#iterations: 134
currently lose_sum: 377.05905401706696
time_elpased: 0.918
batch start
#iterations: 135
currently lose_sum: 377.2928610444069
time_elpased: 0.93
batch start
#iterations: 136
currently lose_sum: 376.8395000696182
time_elpased: 0.935
batch start
#iterations: 137
currently lose_sum: 377.116830766201
time_elpased: 0.933
batch start
#iterations: 138
currently lose_sum: 378.5095477104187
time_elpased: 0.948
batch start
#iterations: 139
currently lose_sum: 376.13881039619446
time_elpased: 0.926
start validation test
0.752783505155
1.0
0.752783505155
0.858957769674
0
validation finish
batch start
#iterations: 140
currently lose_sum: 377.10830092430115
time_elpased: 0.945
batch start
#iterations: 141
currently lose_sum: 376.70887595415115
time_elpased: 0.928
batch start
#iterations: 142
currently lose_sum: 376.9215421676636
time_elpased: 0.939
batch start
#iterations: 143
currently lose_sum: 377.3188853263855
time_elpased: 0.931
batch start
#iterations: 144
currently lose_sum: 376.2500709295273
time_elpased: 0.922
batch start
#iterations: 145
currently lose_sum: 376.3506254553795
time_elpased: 0.927
batch start
#iterations: 146
currently lose_sum: 375.09363543987274
time_elpased: 0.934
batch start
#iterations: 147
currently lose_sum: 377.05857557058334
time_elpased: 0.938
batch start
#iterations: 148
currently lose_sum: 376.6149203777313
time_elpased: 0.922
batch start
#iterations: 149
currently lose_sum: 376.42890471220016
time_elpased: 0.928
batch start
#iterations: 150
currently lose_sum: 376.75316178798676
time_elpased: 0.921
batch start
#iterations: 151
currently lose_sum: 376.3868054151535
time_elpased: 0.942
batch start
#iterations: 152
currently lose_sum: 377.1458417773247
time_elpased: 0.928
batch start
#iterations: 153
currently lose_sum: 376.681468129158
time_elpased: 0.933
batch start
#iterations: 154
currently lose_sum: 375.50649279356
time_elpased: 0.935
batch start
#iterations: 155
currently lose_sum: 376.5728715658188
time_elpased: 0.935
batch start
#iterations: 156
currently lose_sum: 375.0563753247261
time_elpased: 0.918
batch start
#iterations: 157
currently lose_sum: 376.429374396801
time_elpased: 0.97
batch start
#iterations: 158
currently lose_sum: 375.2473425269127
time_elpased: 0.932
batch start
#iterations: 159
currently lose_sum: 375.21946758031845
time_elpased: 0.924
start validation test
0.675257731959
1.0
0.675257731959
0.806153846154
0
validation finish
batch start
#iterations: 160
currently lose_sum: 376.34225499629974
time_elpased: 0.926
batch start
#iterations: 161
currently lose_sum: 377.2837824225426
time_elpased: 0.925
batch start
#iterations: 162
currently lose_sum: 375.58410197496414
time_elpased: 0.933
batch start
#iterations: 163
currently lose_sum: 376.1906512975693
time_elpased: 0.935
batch start
#iterations: 164
currently lose_sum: 375.2313355207443
time_elpased: 0.945
batch start
#iterations: 165
currently lose_sum: 376.8307111263275
time_elpased: 0.944
batch start
#iterations: 166
currently lose_sum: 377.1207091808319
time_elpased: 0.942
batch start
#iterations: 167
currently lose_sum: 375.741207242012
time_elpased: 0.928
batch start
#iterations: 168
currently lose_sum: 374.7256426811218
time_elpased: 0.92
batch start
#iterations: 169
currently lose_sum: 375.9998462200165
time_elpased: 0.926
batch start
#iterations: 170
currently lose_sum: 376.55607837438583
time_elpased: 0.929
batch start
#iterations: 171
currently lose_sum: 377.29038166999817
time_elpased: 0.922
batch start
#iterations: 172
currently lose_sum: 376.6224318742752
time_elpased: 0.933
batch start
#iterations: 173
currently lose_sum: 376.32930755615234
time_elpased: 0.94
batch start
#iterations: 174
currently lose_sum: 376.33117294311523
time_elpased: 0.936
batch start
#iterations: 175
currently lose_sum: 375.89029735326767
time_elpased: 0.962
batch start
#iterations: 176
currently lose_sum: 376.16263395547867
time_elpased: 0.94
batch start
#iterations: 177
currently lose_sum: 376.03339064121246
time_elpased: 0.942
batch start
#iterations: 178
currently lose_sum: 377.70575481653214
time_elpased: 0.944
batch start
#iterations: 179
currently lose_sum: 376.2829203605652
time_elpased: 0.934
start validation test
0.62618556701
1.0
0.62618556701
0.770128058831
0
validation finish
batch start
#iterations: 180
currently lose_sum: 376.2151688337326
time_elpased: 0.942
batch start
#iterations: 181
currently lose_sum: 374.76010167598724
time_elpased: 0.947
batch start
#iterations: 182
currently lose_sum: 376.65454119443893
time_elpased: 0.927
batch start
#iterations: 183
currently lose_sum: 376.8128679394722
time_elpased: 0.962
batch start
#iterations: 184
currently lose_sum: 375.77028024196625
time_elpased: 0.972
batch start
#iterations: 185
currently lose_sum: 376.2973577976227
time_elpased: 0.944
batch start
#iterations: 186
currently lose_sum: 376.1053657531738
time_elpased: 0.952
batch start
#iterations: 187
currently lose_sum: 375.91485899686813
time_elpased: 0.952
batch start
#iterations: 188
currently lose_sum: 376.39501374959946
time_elpased: 0.932
batch start
#iterations: 189
currently lose_sum: 376.38380444049835
time_elpased: 0.929
batch start
#iterations: 190
currently lose_sum: 376.1049726009369
time_elpased: 0.925
batch start
#iterations: 191
currently lose_sum: 376.26692020893097
time_elpased: 0.92
batch start
#iterations: 192
currently lose_sum: 377.1782235503197
time_elpased: 0.92
batch start
#iterations: 193
currently lose_sum: 375.2368476986885
time_elpased: 0.935
batch start
#iterations: 194
currently lose_sum: 376.0125341415405
time_elpased: 0.926
batch start
#iterations: 195
currently lose_sum: 375.63419938087463
time_elpased: 0.934
batch start
#iterations: 196
currently lose_sum: 377.5690850019455
time_elpased: 0.923
batch start
#iterations: 197
currently lose_sum: 376.1648580431938
time_elpased: 0.939
batch start
#iterations: 198
currently lose_sum: 375.56695806980133
time_elpased: 0.93
batch start
#iterations: 199
currently lose_sum: 375.01442289352417
time_elpased: 0.929
start validation test
0.726804123711
1.0
0.726804123711
0.841791044776
0
validation finish
batch start
#iterations: 200
currently lose_sum: 375.236823618412
time_elpased: 0.928
batch start
#iterations: 201
currently lose_sum: 375.7574127316475
time_elpased: 0.933
batch start
#iterations: 202
currently lose_sum: 375.77473217248917
time_elpased: 0.928
batch start
#iterations: 203
currently lose_sum: 375.21956849098206
time_elpased: 0.932
batch start
#iterations: 204
currently lose_sum: 376.0626348257065
time_elpased: 0.927
batch start
#iterations: 205
currently lose_sum: 374.9170418381691
time_elpased: 0.918
batch start
#iterations: 206
currently lose_sum: 375.097218811512
time_elpased: 0.928
batch start
#iterations: 207
currently lose_sum: 375.5969530940056
time_elpased: 0.931
batch start
#iterations: 208
currently lose_sum: 374.6903411746025
time_elpased: 0.926
batch start
#iterations: 209
currently lose_sum: 376.27697145938873
time_elpased: 0.917
batch start
#iterations: 210
currently lose_sum: 375.6046207547188
time_elpased: 0.931
batch start
#iterations: 211
currently lose_sum: 376.4949830174446
time_elpased: 0.93
batch start
#iterations: 212
currently lose_sum: 375.9877982735634
time_elpased: 0.924
batch start
#iterations: 213
currently lose_sum: 374.68807059526443
time_elpased: 0.934
batch start
#iterations: 214
currently lose_sum: 376.1065327525139
time_elpased: 0.934
batch start
#iterations: 215
currently lose_sum: 376.0950105190277
time_elpased: 0.932
batch start
#iterations: 216
currently lose_sum: 376.21884149312973
time_elpased: 0.946
batch start
#iterations: 217
currently lose_sum: 373.5888575911522
time_elpased: 0.949
batch start
#iterations: 218
currently lose_sum: 375.0286310315132
time_elpased: 0.926
batch start
#iterations: 219
currently lose_sum: 376.64913964271545
time_elpased: 0.926
start validation test
0.703711340206
1.0
0.703711340206
0.826092218323
0
validation finish
batch start
#iterations: 220
currently lose_sum: 375.6300075650215
time_elpased: 0.923
batch start
#iterations: 221
currently lose_sum: 374.92237186431885
time_elpased: 0.925
batch start
#iterations: 222
currently lose_sum: 375.68443554639816
time_elpased: 0.931
batch start
#iterations: 223
currently lose_sum: 375.20515644550323
time_elpased: 0.919
batch start
#iterations: 224
currently lose_sum: 375.5772058367729
time_elpased: 0.924
batch start
#iterations: 225
currently lose_sum: 374.2971484065056
time_elpased: 0.927
batch start
#iterations: 226
currently lose_sum: 375.3923549056053
time_elpased: 0.937
batch start
#iterations: 227
currently lose_sum: 375.65911841392517
time_elpased: 0.922
batch start
#iterations: 228
currently lose_sum: 375.8382406234741
time_elpased: 0.933
batch start
#iterations: 229
currently lose_sum: 375.0431984066963
time_elpased: 0.932
batch start
#iterations: 230
currently lose_sum: 375.6762572526932
time_elpased: 0.941
batch start
#iterations: 231
currently lose_sum: 375.6545973420143
time_elpased: 0.931
batch start
#iterations: 232
currently lose_sum: 375.14045345783234
time_elpased: 0.929
batch start
#iterations: 233
currently lose_sum: 375.3670751452446
time_elpased: 0.92
batch start
#iterations: 234
currently lose_sum: 375.6525961756706
time_elpased: 0.932
batch start
#iterations: 235
currently lose_sum: 374.9758062362671
time_elpased: 0.924
batch start
#iterations: 236
currently lose_sum: 375.0590849518776
time_elpased: 0.938
batch start
#iterations: 237
currently lose_sum: 375.8682475090027
time_elpased: 0.93
batch start
#iterations: 238
currently lose_sum: 375.2640023827553
time_elpased: 0.921
batch start
#iterations: 239
currently lose_sum: 374.18547463417053
time_elpased: 0.946
start validation test
0.644329896907
1.0
0.644329896907
0.783699059561
0
validation finish
batch start
#iterations: 240
currently lose_sum: 375.32432436943054
time_elpased: 0.922
batch start
#iterations: 241
currently lose_sum: 375.15794003009796
time_elpased: 0.927
batch start
#iterations: 242
currently lose_sum: 376.1616450548172
time_elpased: 0.923
batch start
#iterations: 243
currently lose_sum: 375.0845937728882
time_elpased: 0.926
batch start
#iterations: 244
currently lose_sum: 375.08938151597977
time_elpased: 0.916
batch start
#iterations: 245
currently lose_sum: 375.6183670759201
time_elpased: 0.925
batch start
#iterations: 246
currently lose_sum: 375.491284430027
time_elpased: 0.927
batch start
#iterations: 247
currently lose_sum: 373.6311209201813
time_elpased: 0.924
batch start
#iterations: 248
currently lose_sum: 375.34415143728256
time_elpased: 0.932
batch start
#iterations: 249
currently lose_sum: 373.8818733692169
time_elpased: 0.936
batch start
#iterations: 250
currently lose_sum: 374.7445887327194
time_elpased: 0.923
batch start
#iterations: 251
currently lose_sum: 374.29516303539276
time_elpased: 0.926
batch start
#iterations: 252
currently lose_sum: 373.75589883327484
time_elpased: 0.92
batch start
#iterations: 253
currently lose_sum: 375.07026278972626
time_elpased: 0.913
batch start
#iterations: 254
currently lose_sum: 374.1806669831276
time_elpased: 0.927
batch start
#iterations: 255
currently lose_sum: 374.7284109592438
time_elpased: 0.926
batch start
#iterations: 256
currently lose_sum: 374.339623272419
time_elpased: 0.919
batch start
#iterations: 257
currently lose_sum: 375.09247636795044
time_elpased: 0.927
batch start
#iterations: 258
currently lose_sum: 374.2867322564125
time_elpased: 0.939
batch start
#iterations: 259
currently lose_sum: 374.86762380599976
time_elpased: 0.929
start validation test
0.572371134021
1.0
0.572371134021
0.728035667453
0
validation finish
batch start
#iterations: 260
currently lose_sum: 375.70422607660294
time_elpased: 0.924
batch start
#iterations: 261
currently lose_sum: 374.41287791728973
time_elpased: 0.927
batch start
#iterations: 262
currently lose_sum: 373.37209540605545
time_elpased: 0.927
batch start
#iterations: 263
currently lose_sum: 375.0006962418556
time_elpased: 0.931
batch start
#iterations: 264
currently lose_sum: 376.10068345069885
time_elpased: 0.924
batch start
#iterations: 265
currently lose_sum: 374.4995050430298
time_elpased: 0.922
batch start
#iterations: 266
currently lose_sum: 374.5593000650406
time_elpased: 0.911
batch start
#iterations: 267
currently lose_sum: 374.20932298898697
time_elpased: 0.924
batch start
#iterations: 268
currently lose_sum: 375.8278514742851
time_elpased: 0.93
batch start
#iterations: 269
currently lose_sum: 375.4238852262497
time_elpased: 0.934
batch start
#iterations: 270
currently lose_sum: 374.8499245047569
time_elpased: 0.923
batch start
#iterations: 271
currently lose_sum: 374.62379747629166
time_elpased: 0.921
batch start
#iterations: 272
currently lose_sum: 374.6150498986244
time_elpased: 0.923
batch start
#iterations: 273
currently lose_sum: 375.3622387647629
time_elpased: 0.928
batch start
#iterations: 274
currently lose_sum: 374.89821296930313
time_elpased: 0.927
batch start
#iterations: 275
currently lose_sum: 374.1082669496536
time_elpased: 0.914
batch start
#iterations: 276
currently lose_sum: 375.45091342926025
time_elpased: 0.928
batch start
#iterations: 277
currently lose_sum: 375.304690182209
time_elpased: 0.915
batch start
#iterations: 278
currently lose_sum: 375.8371193408966
time_elpased: 0.933
batch start
#iterations: 279
currently lose_sum: 373.7334134578705
time_elpased: 0.921
start validation test
0.684742268041
1.0
0.684742268041
0.812874801126
0
validation finish
batch start
#iterations: 280
currently lose_sum: 374.84664887189865
time_elpased: 0.925
batch start
#iterations: 281
currently lose_sum: 373.612099647522
time_elpased: 0.93
batch start
#iterations: 282
currently lose_sum: 375.29167795181274
time_elpased: 0.933
batch start
#iterations: 283
currently lose_sum: 376.1942861676216
time_elpased: 0.923
batch start
#iterations: 284
currently lose_sum: 374.78468322753906
time_elpased: 0.926
batch start
#iterations: 285
currently lose_sum: 374.71544802188873
time_elpased: 0.926
batch start
#iterations: 286
currently lose_sum: 374.6425905227661
time_elpased: 0.924
batch start
#iterations: 287
currently lose_sum: 374.98762315511703
time_elpased: 0.923
batch start
#iterations: 288
currently lose_sum: 374.2551393508911
time_elpased: 0.926
batch start
#iterations: 289
currently lose_sum: 372.6355220079422
time_elpased: 0.924
batch start
#iterations: 290
currently lose_sum: 374.23387837409973
time_elpased: 0.928
batch start
#iterations: 291
currently lose_sum: 374.09799283742905
time_elpased: 0.928
batch start
#iterations: 292
currently lose_sum: 374.75729072093964
time_elpased: 0.926
batch start
#iterations: 293
currently lose_sum: 375.20015305280685
time_elpased: 0.927
batch start
#iterations: 294
currently lose_sum: 373.74626487493515
time_elpased: 0.93
batch start
#iterations: 295
currently lose_sum: 375.5196830034256
time_elpased: 0.93
batch start
#iterations: 296
currently lose_sum: 373.9618871808052
time_elpased: 0.925
batch start
#iterations: 297
currently lose_sum: 375.5335456132889
time_elpased: 0.925
batch start
#iterations: 298
currently lose_sum: 374.679771900177
time_elpased: 0.919
batch start
#iterations: 299
currently lose_sum: 373.7808725833893
time_elpased: 0.92
start validation test
0.570309278351
1.0
0.570309278351
0.726365546218
0
validation finish
batch start
#iterations: 300
currently lose_sum: 373.53780859708786
time_elpased: 0.921
batch start
#iterations: 301
currently lose_sum: 374.954694211483
time_elpased: 0.928
batch start
#iterations: 302
currently lose_sum: 374.36135959625244
time_elpased: 0.936
batch start
#iterations: 303
currently lose_sum: 375.53873467445374
time_elpased: 0.922
batch start
#iterations: 304
currently lose_sum: 374.9681045413017
time_elpased: 0.926
batch start
#iterations: 305
currently lose_sum: 373.6163801550865
time_elpased: 0.928
batch start
#iterations: 306
currently lose_sum: 374.5396041274071
time_elpased: 0.92
batch start
#iterations: 307
currently lose_sum: 373.6469621062279
time_elpased: 0.915
batch start
#iterations: 308
currently lose_sum: 373.64283668994904
time_elpased: 0.924
batch start
#iterations: 309
currently lose_sum: 373.57184451818466
time_elpased: 0.922
batch start
#iterations: 310
currently lose_sum: 374.77733862400055
time_elpased: 0.932
batch start
#iterations: 311
currently lose_sum: 374.23956352472305
time_elpased: 0.922
batch start
#iterations: 312
currently lose_sum: 373.0379227399826
time_elpased: 0.915
batch start
#iterations: 313
currently lose_sum: 374.70144921541214
time_elpased: 0.93
batch start
#iterations: 314
currently lose_sum: 373.6305677294731
time_elpased: 0.918
batch start
#iterations: 315
currently lose_sum: 373.79618924856186
time_elpased: 0.94
batch start
#iterations: 316
currently lose_sum: 373.32809948921204
time_elpased: 0.921
batch start
#iterations: 317
currently lose_sum: 374.4629902243614
time_elpased: 0.928
batch start
#iterations: 318
currently lose_sum: 373.6639094352722
time_elpased: 0.927
batch start
#iterations: 319
currently lose_sum: 374.2472335100174
time_elpased: 0.93
start validation test
0.609175257732
1.0
0.609175257732
0.757127298354
0
validation finish
batch start
#iterations: 320
currently lose_sum: 372.9367236495018
time_elpased: 0.94
batch start
#iterations: 321
currently lose_sum: 374.8629704117775
time_elpased: 0.921
batch start
#iterations: 322
currently lose_sum: 373.5343539118767
time_elpased: 0.925
batch start
#iterations: 323
currently lose_sum: 373.88208401203156
time_elpased: 0.921
batch start
#iterations: 324
currently lose_sum: 372.8731502890587
time_elpased: 0.921
batch start
#iterations: 325
currently lose_sum: 374.6455023884773
time_elpased: 0.933
batch start
#iterations: 326
currently lose_sum: 374.79838877916336
time_elpased: 0.928
batch start
#iterations: 327
currently lose_sum: 374.09113466739655
time_elpased: 0.931
batch start
#iterations: 328
currently lose_sum: 375.0646201968193
time_elpased: 0.915
batch start
#iterations: 329
currently lose_sum: 374.7510524392128
time_elpased: 0.92
batch start
#iterations: 330
currently lose_sum: 374.2595645189285
time_elpased: 0.918
batch start
#iterations: 331
currently lose_sum: 374.7451681494713
time_elpased: 0.943
batch start
#iterations: 332
currently lose_sum: 374.57299733161926
time_elpased: 0.937
batch start
#iterations: 333
currently lose_sum: 373.81462877988815
time_elpased: 0.913
batch start
#iterations: 334
currently lose_sum: 373.88387554883957
time_elpased: 0.918
batch start
#iterations: 335
currently lose_sum: 374.0449436903
time_elpased: 0.922
batch start
#iterations: 336
currently lose_sum: 372.75804311037064
time_elpased: 1.178
batch start
#iterations: 337
currently lose_sum: 374.3116839528084
time_elpased: 0.986
batch start
#iterations: 338
currently lose_sum: 374.5850529074669
time_elpased: 0.991
batch start
#iterations: 339
currently lose_sum: 373.6500181555748
time_elpased: 0.95
start validation test
0.660618556701
1.0
0.660618556701
0.795629500869
0
validation finish
batch start
#iterations: 340
currently lose_sum: 375.2806856036186
time_elpased: 1.037
batch start
#iterations: 341
currently lose_sum: 374.65538161993027
time_elpased: 0.928
batch start
#iterations: 342
currently lose_sum: 375.2194882631302
time_elpased: 0.933
batch start
#iterations: 343
currently lose_sum: 373.2039069533348
time_elpased: 1.187
batch start
#iterations: 344
currently lose_sum: 374.2982144355774
time_elpased: 0.941
batch start
#iterations: 345
currently lose_sum: 374.3145791888237
time_elpased: 0.918
batch start
#iterations: 346
currently lose_sum: 373.57785081863403
time_elpased: 0.931
batch start
#iterations: 347
currently lose_sum: 372.93842136859894
time_elpased: 0.941
batch start
#iterations: 348
currently lose_sum: 372.73247808218
time_elpased: 0.923
batch start
#iterations: 349
currently lose_sum: 372.4617247581482
time_elpased: 0.922
batch start
#iterations: 350
currently lose_sum: 374.459167778492
time_elpased: 0.921
batch start
#iterations: 351
currently lose_sum: 373.0275396704674
time_elpased: 0.916
batch start
#iterations: 352
currently lose_sum: 374.33139395713806
time_elpased: 0.927
batch start
#iterations: 353
currently lose_sum: 374.1769609451294
time_elpased: 0.929
batch start
#iterations: 354
currently lose_sum: 374.5082166194916
time_elpased: 0.918
batch start
#iterations: 355
currently lose_sum: 375.54081892967224
time_elpased: 0.91
batch start
#iterations: 356
currently lose_sum: 373.78837621212006
time_elpased: 0.939
batch start
#iterations: 357
currently lose_sum: 373.42655712366104
time_elpased: 0.914
batch start
#iterations: 358
currently lose_sum: 373.5429227948189
time_elpased: 0.925
batch start
#iterations: 359
currently lose_sum: 375.08488392829895
time_elpased: 0.925
start validation test
0.686494845361
1.0
0.686494845361
0.814108441836
0
validation finish
batch start
#iterations: 360
currently lose_sum: 374.42713379859924
time_elpased: 0.918
batch start
#iterations: 361
currently lose_sum: 374.41197019815445
time_elpased: 0.905
batch start
#iterations: 362
currently lose_sum: 374.26247119903564
time_elpased: 0.928
batch start
#iterations: 363
currently lose_sum: 374.5874213576317
time_elpased: 0.924
batch start
#iterations: 364
currently lose_sum: 373.2518547177315
time_elpased: 0.919
batch start
#iterations: 365
currently lose_sum: 374.5199360847473
time_elpased: 0.918
batch start
#iterations: 366
currently lose_sum: 374.77028554677963
time_elpased: 0.932
batch start
#iterations: 367
currently lose_sum: 374.2057682275772
time_elpased: 0.916
batch start
#iterations: 368
currently lose_sum: 374.4161202907562
time_elpased: 0.908
batch start
#iterations: 369
currently lose_sum: 372.5992354750633
time_elpased: 0.927
batch start
#iterations: 370
currently lose_sum: 373.0523040294647
time_elpased: 0.918
batch start
#iterations: 371
currently lose_sum: 373.9561690688133
time_elpased: 0.919
batch start
#iterations: 372
currently lose_sum: 372.9808064699173
time_elpased: 0.927
batch start
#iterations: 373
currently lose_sum: 373.7142215371132
time_elpased: 0.915
batch start
#iterations: 374
currently lose_sum: 374.99157297611237
time_elpased: 0.93
batch start
#iterations: 375
currently lose_sum: 372.87347197532654
time_elpased: 0.917
batch start
#iterations: 376
currently lose_sum: 373.58145785331726
time_elpased: 0.932
batch start
#iterations: 377
currently lose_sum: 373.8340417742729
time_elpased: 0.923
batch start
#iterations: 378
currently lose_sum: 373.16291773319244
time_elpased: 0.919
batch start
#iterations: 379
currently lose_sum: 375.05232560634613
time_elpased: 0.919
start validation test
0.734742268041
1.0
0.734742268041
0.847090984727
0
validation finish
batch start
#iterations: 380
currently lose_sum: 374.1992691755295
time_elpased: 0.936
batch start
#iterations: 381
currently lose_sum: 373.98175889253616
time_elpased: 0.922
batch start
#iterations: 382
currently lose_sum: 375.4153977036476
time_elpased: 0.916
batch start
#iterations: 383
currently lose_sum: 372.12155079841614
time_elpased: 0.923
batch start
#iterations: 384
currently lose_sum: 373.0661675930023
time_elpased: 0.922
batch start
#iterations: 385
currently lose_sum: 373.5232884287834
time_elpased: 0.925
batch start
#iterations: 386
currently lose_sum: 372.583258330822
time_elpased: 0.929
batch start
#iterations: 387
currently lose_sum: 372.8405249118805
time_elpased: 0.924
batch start
#iterations: 388
currently lose_sum: 373.33239805698395
time_elpased: 0.91
batch start
#iterations: 389
currently lose_sum: 374.39097714424133
time_elpased: 0.914
batch start
#iterations: 390
currently lose_sum: 373.01351499557495
time_elpased: 0.92
batch start
#iterations: 391
currently lose_sum: 372.8742034435272
time_elpased: 0.927
batch start
#iterations: 392
currently lose_sum: 373.73822832107544
time_elpased: 0.914
batch start
#iterations: 393
currently lose_sum: 373.1266368627548
time_elpased: 0.933
batch start
#iterations: 394
currently lose_sum: 374.0182666182518
time_elpased: 0.919
batch start
#iterations: 395
currently lose_sum: 373.14182955026627
time_elpased: 0.927
batch start
#iterations: 396
currently lose_sum: 372.9580385684967
time_elpased: 0.925
batch start
#iterations: 397
currently lose_sum: 375.0148001909256
time_elpased: 0.918
batch start
#iterations: 398
currently lose_sum: 373.8408702015877
time_elpased: 0.921
batch start
#iterations: 399
currently lose_sum: 374.4255779981613
time_elpased: 0.924
start validation test
0.60824742268
1.0
0.60824742268
0.75641025641
0
validation finish
acc: 0.789
pre: 1.000
rec: 0.789
F1: 0.882
auc: 0.000
