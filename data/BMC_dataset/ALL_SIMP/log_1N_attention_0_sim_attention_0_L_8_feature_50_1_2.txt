start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 404.91152065992355
time_elpased: 0.959
batch start
#iterations: 1
currently lose_sum: 403.72180873155594
time_elpased: 0.931
batch start
#iterations: 2
currently lose_sum: 402.86618196964264
time_elpased: 0.926
batch start
#iterations: 3
currently lose_sum: 402.5452888607979
time_elpased: 0.932
batch start
#iterations: 4
currently lose_sum: 401.89720463752747
time_elpased: 0.932
batch start
#iterations: 5
currently lose_sum: 401.5514520406723
time_elpased: 0.936
batch start
#iterations: 6
currently lose_sum: 401.06353056430817
time_elpased: 0.936
batch start
#iterations: 7
currently lose_sum: 400.5571748018265
time_elpased: 0.932
batch start
#iterations: 8
currently lose_sum: 400.3049148917198
time_elpased: 0.928
batch start
#iterations: 9
currently lose_sum: 399.2700664997101
time_elpased: 0.92
batch start
#iterations: 10
currently lose_sum: 399.0698202252388
time_elpased: 0.924
batch start
#iterations: 11
currently lose_sum: 398.5463764667511
time_elpased: 0.935
batch start
#iterations: 12
currently lose_sum: 398.2582378387451
time_elpased: 0.937
batch start
#iterations: 13
currently lose_sum: 397.77546364068985
time_elpased: 0.934
batch start
#iterations: 14
currently lose_sum: 396.1264009475708
time_elpased: 0.929
batch start
#iterations: 15
currently lose_sum: 396.27821815013885
time_elpased: 0.93
batch start
#iterations: 16
currently lose_sum: 395.32311350107193
time_elpased: 0.933
batch start
#iterations: 17
currently lose_sum: 395.6669558286667
time_elpased: 0.919
batch start
#iterations: 18
currently lose_sum: 394.48548674583435
time_elpased: 0.925
batch start
#iterations: 19
currently lose_sum: 394.57380497455597
time_elpased: 0.938
start validation test
0.280412371134
1.0
0.280412371134
0.438003220612
0
validation finish
batch start
#iterations: 20
currently lose_sum: 393.6866711974144
time_elpased: 0.938
batch start
#iterations: 21
currently lose_sum: 392.93456679582596
time_elpased: 0.966
batch start
#iterations: 22
currently lose_sum: 392.8952375650406
time_elpased: 0.94
batch start
#iterations: 23
currently lose_sum: 392.57455682754517
time_elpased: 0.938
batch start
#iterations: 24
currently lose_sum: 392.15531355142593
time_elpased: 0.94
batch start
#iterations: 25
currently lose_sum: 390.8411054611206
time_elpased: 0.928
batch start
#iterations: 26
currently lose_sum: 390.4557458758354
time_elpased: 0.936
batch start
#iterations: 27
currently lose_sum: 390.0476163625717
time_elpased: 0.928
batch start
#iterations: 28
currently lose_sum: 389.55941212177277
time_elpased: 0.939
batch start
#iterations: 29
currently lose_sum: 389.90661108493805
time_elpased: 0.93
batch start
#iterations: 30
currently lose_sum: 389.2187495827675
time_elpased: 0.943
batch start
#iterations: 31
currently lose_sum: 389.0047832131386
time_elpased: 0.952
batch start
#iterations: 32
currently lose_sum: 388.73926347494125
time_elpased: 0.935
batch start
#iterations: 33
currently lose_sum: 387.9942206144333
time_elpased: 0.939
batch start
#iterations: 34
currently lose_sum: 387.7988972067833
time_elpased: 0.936
batch start
#iterations: 35
currently lose_sum: 388.0864352583885
time_elpased: 0.94
batch start
#iterations: 36
currently lose_sum: 386.67234402894974
time_elpased: 0.952
batch start
#iterations: 37
currently lose_sum: 385.2815794348717
time_elpased: 0.93
batch start
#iterations: 38
currently lose_sum: 386.5536150932312
time_elpased: 0.936
batch start
#iterations: 39
currently lose_sum: 386.01066505908966
time_elpased: 0.944
start validation test
0.505360824742
1.0
0.505360824742
0.671414874675
0
validation finish
batch start
#iterations: 40
currently lose_sum: 385.72519487142563
time_elpased: 0.942
batch start
#iterations: 41
currently lose_sum: 385.51685506105423
time_elpased: 0.94
batch start
#iterations: 42
currently lose_sum: 384.2663736343384
time_elpased: 0.938
batch start
#iterations: 43
currently lose_sum: 385.0118756890297
time_elpased: 0.933
batch start
#iterations: 44
currently lose_sum: 385.00511890649796
time_elpased: 0.942
batch start
#iterations: 45
currently lose_sum: 384.6225861310959
time_elpased: 0.934
batch start
#iterations: 46
currently lose_sum: 384.83710277080536
time_elpased: 0.942
batch start
#iterations: 47
currently lose_sum: 383.9161533713341
time_elpased: 0.936
batch start
#iterations: 48
currently lose_sum: 382.65906888246536
time_elpased: 0.933
batch start
#iterations: 49
currently lose_sum: 383.84899508953094
time_elpased: 0.939
batch start
#iterations: 50
currently lose_sum: 382.72989225387573
time_elpased: 0.945
batch start
#iterations: 51
currently lose_sum: 382.59252643585205
time_elpased: 0.934
batch start
#iterations: 52
currently lose_sum: 383.36545890569687
time_elpased: 0.93
batch start
#iterations: 53
currently lose_sum: 382.5855801105499
time_elpased: 0.952
batch start
#iterations: 54
currently lose_sum: 383.34591245651245
time_elpased: 0.933
batch start
#iterations: 55
currently lose_sum: 382.1546662449837
time_elpased: 0.947
batch start
#iterations: 56
currently lose_sum: 382.954013466835
time_elpased: 0.93
batch start
#iterations: 57
currently lose_sum: 382.2428354024887
time_elpased: 0.936
batch start
#iterations: 58
currently lose_sum: 381.6475040912628
time_elpased: 0.942
batch start
#iterations: 59
currently lose_sum: 383.03579717874527
time_elpased: 0.947
start validation test
0.611134020619
1.0
0.611134020619
0.758638341438
0
validation finish
batch start
#iterations: 60
currently lose_sum: 381.7990481853485
time_elpased: 0.94
batch start
#iterations: 61
currently lose_sum: 381.0279392004013
time_elpased: 0.929
batch start
#iterations: 62
currently lose_sum: 382.03279453516006
time_elpased: 0.934
batch start
#iterations: 63
currently lose_sum: 380.2655280828476
time_elpased: 0.931
batch start
#iterations: 64
currently lose_sum: 380.8420481681824
time_elpased: 0.961
batch start
#iterations: 65
currently lose_sum: 381.1168739795685
time_elpased: 0.95
batch start
#iterations: 66
currently lose_sum: 381.1077127456665
time_elpased: 0.938
batch start
#iterations: 67
currently lose_sum: 380.0704253911972
time_elpased: 0.953
batch start
#iterations: 68
currently lose_sum: 381.6195179820061
time_elpased: 0.945
batch start
#iterations: 69
currently lose_sum: 379.867192029953
time_elpased: 0.992
batch start
#iterations: 70
currently lose_sum: 380.9544171690941
time_elpased: 0.934
batch start
#iterations: 71
currently lose_sum: 380.4817863702774
time_elpased: 0.936
batch start
#iterations: 72
currently lose_sum: 380.6371079683304
time_elpased: 0.937
batch start
#iterations: 73
currently lose_sum: 380.28151309490204
time_elpased: 0.947
batch start
#iterations: 74
currently lose_sum: 380.39031755924225
time_elpased: 0.939
batch start
#iterations: 75
currently lose_sum: 378.4013199210167
time_elpased: 0.94
batch start
#iterations: 76
currently lose_sum: 379.53042697906494
time_elpased: 0.937
batch start
#iterations: 77
currently lose_sum: 379.57552695274353
time_elpased: 0.942
batch start
#iterations: 78
currently lose_sum: 379.70543563365936
time_elpased: 0.94
batch start
#iterations: 79
currently lose_sum: 379.22215485572815
time_elpased: 0.936
start validation test
0.794742268041
1.0
0.794742268041
0.88563386754
0
validation finish
batch start
#iterations: 80
currently lose_sum: 378.41429489851
time_elpased: 0.935
batch start
#iterations: 81
currently lose_sum: 380.0215030312538
time_elpased: 0.941
batch start
#iterations: 82
currently lose_sum: 379.5478582382202
time_elpased: 0.944
batch start
#iterations: 83
currently lose_sum: 378.9491314291954
time_elpased: 0.944
batch start
#iterations: 84
currently lose_sum: 379.98238748311996
time_elpased: 0.933
batch start
#iterations: 85
currently lose_sum: 379.3166429400444
time_elpased: 0.945
batch start
#iterations: 86
currently lose_sum: 379.81751239299774
time_elpased: 0.949
batch start
#iterations: 87
currently lose_sum: 379.3134511709213
time_elpased: 0.936
batch start
#iterations: 88
currently lose_sum: 378.2984342575073
time_elpased: 0.93
batch start
#iterations: 89
currently lose_sum: 379.1071951985359
time_elpased: 0.935
batch start
#iterations: 90
currently lose_sum: 379.6730869412422
time_elpased: 0.93
batch start
#iterations: 91
currently lose_sum: 378.6238092780113
time_elpased: 0.943
batch start
#iterations: 92
currently lose_sum: 378.89073836803436
time_elpased: 0.939
batch start
#iterations: 93
currently lose_sum: 379.01553535461426
time_elpased: 0.923
batch start
#iterations: 94
currently lose_sum: 378.6212142109871
time_elpased: 0.935
batch start
#iterations: 95
currently lose_sum: 377.97548723220825
time_elpased: 0.93
batch start
#iterations: 96
currently lose_sum: 378.2461389899254
time_elpased: 0.938
batch start
#iterations: 97
currently lose_sum: 378.24824810028076
time_elpased: 0.927
batch start
#iterations: 98
currently lose_sum: 378.0942272543907
time_elpased: 0.946
batch start
#iterations: 99
currently lose_sum: 377.67062509059906
time_elpased: 0.937
start validation test
0.607010309278
1.0
0.607010309278
0.755452912497
0
validation finish
batch start
#iterations: 100
currently lose_sum: 378.3957850933075
time_elpased: 0.933
batch start
#iterations: 101
currently lose_sum: 377.24645256996155
time_elpased: 0.923
batch start
#iterations: 102
currently lose_sum: 377.3292090892792
time_elpased: 0.921
batch start
#iterations: 103
currently lose_sum: 377.9732555150986
time_elpased: 0.935
batch start
#iterations: 104
currently lose_sum: 378.04612946510315
time_elpased: 0.932
batch start
#iterations: 105
currently lose_sum: 377.55911362171173
time_elpased: 0.937
batch start
#iterations: 106
currently lose_sum: 377.9479321241379
time_elpased: 0.925
batch start
#iterations: 107
currently lose_sum: 377.7521342039108
time_elpased: 0.927
batch start
#iterations: 108
currently lose_sum: 377.6691094636917
time_elpased: 0.92
batch start
#iterations: 109
currently lose_sum: 376.70516753196716
time_elpased: 0.946
batch start
#iterations: 110
currently lose_sum: 378.0872811079025
time_elpased: 0.933
batch start
#iterations: 111
currently lose_sum: 377.41102051734924
time_elpased: 0.927
batch start
#iterations: 112
currently lose_sum: 376.3742261528969
time_elpased: 0.919
batch start
#iterations: 113
currently lose_sum: 378.3375045657158
time_elpased: 0.932
batch start
#iterations: 114
currently lose_sum: 376.991956949234
time_elpased: 0.937
batch start
#iterations: 115
currently lose_sum: 377.1957820057869
time_elpased: 0.94
batch start
#iterations: 116
currently lose_sum: 377.4818826317787
time_elpased: 0.937
batch start
#iterations: 117
currently lose_sum: 376.40818870067596
time_elpased: 0.933
batch start
#iterations: 118
currently lose_sum: 375.750430226326
time_elpased: 0.928
batch start
#iterations: 119
currently lose_sum: 377.108480989933
time_elpased: 0.921
start validation test
0.757113402062
1.0
0.757113402062
0.861769537667
0
validation finish
batch start
#iterations: 120
currently lose_sum: 377.50419414043427
time_elpased: 0.933
batch start
#iterations: 121
currently lose_sum: 376.6993396282196
time_elpased: 0.922
batch start
#iterations: 122
currently lose_sum: 375.62738078832626
time_elpased: 0.938
batch start
#iterations: 123
currently lose_sum: 375.80744630098343
time_elpased: 0.927
batch start
#iterations: 124
currently lose_sum: 375.66356164216995
time_elpased: 0.934
batch start
#iterations: 125
currently lose_sum: 376.76181572675705
time_elpased: 0.935
batch start
#iterations: 126
currently lose_sum: 378.4684109687805
time_elpased: 0.924
batch start
#iterations: 127
currently lose_sum: 376.53822779655457
time_elpased: 0.927
batch start
#iterations: 128
currently lose_sum: 375.79907512664795
time_elpased: 0.936
batch start
#iterations: 129
currently lose_sum: 377.3051725625992
time_elpased: 0.934
batch start
#iterations: 130
currently lose_sum: 375.6710222363472
time_elpased: 0.932
batch start
#iterations: 131
currently lose_sum: 376.90659314394
time_elpased: 0.942
batch start
#iterations: 132
currently lose_sum: 377.2988745570183
time_elpased: 0.938
batch start
#iterations: 133
currently lose_sum: 376.01686519384384
time_elpased: 0.93
batch start
#iterations: 134
currently lose_sum: 377.19440972805023
time_elpased: 0.947
batch start
#iterations: 135
currently lose_sum: 376.74298840761185
time_elpased: 0.934
batch start
#iterations: 136
currently lose_sum: 377.7393087744713
time_elpased: 0.935
batch start
#iterations: 137
currently lose_sum: 375.76549059152603
time_elpased: 0.932
batch start
#iterations: 138
currently lose_sum: 378.2221183180809
time_elpased: 0.938
batch start
#iterations: 139
currently lose_sum: 376.24579471349716
time_elpased: 0.942
start validation test
0.734536082474
1.0
0.734536082474
0.846953937593
0
validation finish
batch start
#iterations: 140
currently lose_sum: 376.3444585800171
time_elpased: 0.948
batch start
#iterations: 141
currently lose_sum: 377.3019674420357
time_elpased: 0.932
batch start
#iterations: 142
currently lose_sum: 376.7077005505562
time_elpased: 0.932
batch start
#iterations: 143
currently lose_sum: 377.7591199874878
time_elpased: 0.922
batch start
#iterations: 144
currently lose_sum: 376.220011472702
time_elpased: 0.948
batch start
#iterations: 145
currently lose_sum: 375.530144572258
time_elpased: 0.936
batch start
#iterations: 146
currently lose_sum: 375.24952936172485
time_elpased: 0.943
batch start
#iterations: 147
currently lose_sum: 376.3246977329254
time_elpased: 0.933
batch start
#iterations: 148
currently lose_sum: 375.93772250413895
time_elpased: 0.936
batch start
#iterations: 149
currently lose_sum: 376.25817912817
time_elpased: 0.93
batch start
#iterations: 150
currently lose_sum: 375.86033993959427
time_elpased: 0.937
batch start
#iterations: 151
currently lose_sum: 376.31916838884354
time_elpased: 0.938
batch start
#iterations: 152
currently lose_sum: 375.7061964273453
time_elpased: 0.931
batch start
#iterations: 153
currently lose_sum: 377.09577864408493
time_elpased: 0.936
batch start
#iterations: 154
currently lose_sum: 375.85770124197006
time_elpased: 0.941
batch start
#iterations: 155
currently lose_sum: 376.0298544764519
time_elpased: 0.926
batch start
#iterations: 156
currently lose_sum: 376.43725937604904
time_elpased: 0.933
batch start
#iterations: 157
currently lose_sum: 375.28711503744125
time_elpased: 0.934
batch start
#iterations: 158
currently lose_sum: 375.7606245279312
time_elpased: 0.931
batch start
#iterations: 159
currently lose_sum: 377.1763540506363
time_elpased: 0.921
start validation test
0.577319587629
1.0
0.577319587629
0.732026143791
0
validation finish
batch start
#iterations: 160
currently lose_sum: 375.94879680871964
time_elpased: 0.93
batch start
#iterations: 161
currently lose_sum: 376.526226580143
time_elpased: 0.938
batch start
#iterations: 162
currently lose_sum: 376.2837824225426
time_elpased: 0.939
batch start
#iterations: 163
currently lose_sum: 376.21617698669434
time_elpased: 0.933
batch start
#iterations: 164
currently lose_sum: 375.65652644634247
time_elpased: 0.931
batch start
#iterations: 165
currently lose_sum: 374.78243881464005
time_elpased: 0.947
batch start
#iterations: 166
currently lose_sum: 375.31384712457657
time_elpased: 0.924
batch start
#iterations: 167
currently lose_sum: 375.6696226000786
time_elpased: 0.916
batch start
#iterations: 168
currently lose_sum: 375.2675238251686
time_elpased: 0.926
batch start
#iterations: 169
currently lose_sum: 374.03099060058594
time_elpased: 0.939
batch start
#iterations: 170
currently lose_sum: 375.49248284101486
time_elpased: 0.929
batch start
#iterations: 171
currently lose_sum: 376.1500172019005
time_elpased: 0.94
batch start
#iterations: 172
currently lose_sum: 376.05897265672684
time_elpased: 0.926
batch start
#iterations: 173
currently lose_sum: 375.66676580905914
time_elpased: 0.932
batch start
#iterations: 174
currently lose_sum: 374.8532280921936
time_elpased: 0.929
batch start
#iterations: 175
currently lose_sum: 376.6612068414688
time_elpased: 0.933
batch start
#iterations: 176
currently lose_sum: 375.02988761663437
time_elpased: 0.944
batch start
#iterations: 177
currently lose_sum: 374.4332477450371
time_elpased: 0.93
batch start
#iterations: 178
currently lose_sum: 376.8632667064667
time_elpased: 0.941
batch start
#iterations: 179
currently lose_sum: 375.51208859682083
time_elpased: 0.933
start validation test
0.687835051546
1.0
0.687835051546
0.815050085512
0
validation finish
batch start
#iterations: 180
currently lose_sum: 375.94327676296234
time_elpased: 0.94
batch start
#iterations: 181
currently lose_sum: 375.0328763127327
time_elpased: 0.941
batch start
#iterations: 182
currently lose_sum: 375.1824497580528
time_elpased: 0.935
batch start
#iterations: 183
currently lose_sum: 375.55721443891525
time_elpased: 0.926
batch start
#iterations: 184
currently lose_sum: 375.1295589208603
time_elpased: 0.931
batch start
#iterations: 185
currently lose_sum: 375.66302514076233
time_elpased: 0.929
batch start
#iterations: 186
currently lose_sum: 374.39848029613495
time_elpased: 0.928
batch start
#iterations: 187
currently lose_sum: 373.482970058918
time_elpased: 0.95
batch start
#iterations: 188
currently lose_sum: 376.1680902838707
time_elpased: 0.948
batch start
#iterations: 189
currently lose_sum: 376.02683395147324
time_elpased: 0.94
batch start
#iterations: 190
currently lose_sum: 373.482832968235
time_elpased: 0.93
batch start
#iterations: 191
currently lose_sum: 375.13540756702423
time_elpased: 0.933
batch start
#iterations: 192
currently lose_sum: 376.01837491989136
time_elpased: 0.974
batch start
#iterations: 193
currently lose_sum: 376.12167316675186
time_elpased: 0.941
batch start
#iterations: 194
currently lose_sum: 374.2136971950531
time_elpased: 0.931
batch start
#iterations: 195
currently lose_sum: 374.90125304460526
time_elpased: 0.93
batch start
#iterations: 196
currently lose_sum: 376.4348328113556
time_elpased: 0.931
batch start
#iterations: 197
currently lose_sum: 374.2937436103821
time_elpased: 0.93
batch start
#iterations: 198
currently lose_sum: 375.6451616883278
time_elpased: 0.938
batch start
#iterations: 199
currently lose_sum: 374.51835334300995
time_elpased: 0.928
start validation test
0.671030927835
1.0
0.671030927835
0.803134061324
0
validation finish
batch start
#iterations: 200
currently lose_sum: 375.9162796139717
time_elpased: 0.929
batch start
#iterations: 201
currently lose_sum: 374.04313910007477
time_elpased: 0.933
batch start
#iterations: 202
currently lose_sum: 373.88180845975876
time_elpased: 0.952
batch start
#iterations: 203
currently lose_sum: 372.58137053251266
time_elpased: 0.927
batch start
#iterations: 204
currently lose_sum: 375.80787801742554
time_elpased: 0.946
batch start
#iterations: 205
currently lose_sum: 373.9019909501076
time_elpased: 0.93
batch start
#iterations: 206
currently lose_sum: 374.0780041217804
time_elpased: 0.946
batch start
#iterations: 207
currently lose_sum: 375.26815235614777
time_elpased: 0.945
batch start
#iterations: 208
currently lose_sum: 374.7682484984398
time_elpased: 0.926
batch start
#iterations: 209
currently lose_sum: 375.0860530138016
time_elpased: 0.925
batch start
#iterations: 210
currently lose_sum: 374.47377049922943
time_elpased: 0.932
batch start
#iterations: 211
currently lose_sum: 373.86459785699844
time_elpased: 0.945
batch start
#iterations: 212
currently lose_sum: 375.492518723011
time_elpased: 0.928
batch start
#iterations: 213
currently lose_sum: 375.05244940519333
time_elpased: 0.942
batch start
#iterations: 214
currently lose_sum: 375.2908365726471
time_elpased: 0.929
batch start
#iterations: 215
currently lose_sum: 374.82480174303055
time_elpased: 0.942
batch start
#iterations: 216
currently lose_sum: 374.92995113134384
time_elpased: 0.923
batch start
#iterations: 217
currently lose_sum: 374.02375715970993
time_elpased: 0.936
batch start
#iterations: 218
currently lose_sum: 374.6818425655365
time_elpased: 0.935
batch start
#iterations: 219
currently lose_sum: 375.7768767476082
time_elpased: 0.928
start validation test
0.70206185567
1.0
0.70206185567
0.824954572986
0
validation finish
batch start
#iterations: 220
currently lose_sum: 374.24224758148193
time_elpased: 0.93
batch start
#iterations: 221
currently lose_sum: 375.0922903418541
time_elpased: 0.938
batch start
#iterations: 222
currently lose_sum: 375.0625761151314
time_elpased: 0.933
batch start
#iterations: 223
currently lose_sum: 374.34763967990875
time_elpased: 0.94
batch start
#iterations: 224
currently lose_sum: 375.8542489409447
time_elpased: 0.928
batch start
#iterations: 225
currently lose_sum: 374.0081495642662
time_elpased: 0.933
batch start
#iterations: 226
currently lose_sum: 375.1114789843559
time_elpased: 0.94
batch start
#iterations: 227
currently lose_sum: 374.737517952919
time_elpased: 0.924
batch start
#iterations: 228
currently lose_sum: 374.3796241879463
time_elpased: 0.935
batch start
#iterations: 229
currently lose_sum: 373.55721044540405
time_elpased: 0.958
batch start
#iterations: 230
currently lose_sum: 374.46308475732803
time_elpased: 0.929
batch start
#iterations: 231
currently lose_sum: 374.6351305246353
time_elpased: 0.929
batch start
#iterations: 232
currently lose_sum: 374.84808415174484
time_elpased: 0.921
batch start
#iterations: 233
currently lose_sum: 374.75533026456833
time_elpased: 0.923
batch start
#iterations: 234
currently lose_sum: 374.32821148633957
time_elpased: 0.939
batch start
#iterations: 235
currently lose_sum: 374.09518426656723
time_elpased: 0.945
batch start
#iterations: 236
currently lose_sum: 374.14500403404236
time_elpased: 0.937
batch start
#iterations: 237
currently lose_sum: 373.84896117448807
time_elpased: 0.93
batch start
#iterations: 238
currently lose_sum: 374.5702465772629
time_elpased: 0.928
batch start
#iterations: 239
currently lose_sum: 373.933106303215
time_elpased: 0.935
start validation test
0.584329896907
1.0
0.584329896907
0.737636647579
0
validation finish
batch start
#iterations: 240
currently lose_sum: 373.1868752837181
time_elpased: 0.929
batch start
#iterations: 241
currently lose_sum: 373.75309133529663
time_elpased: 0.924
batch start
#iterations: 242
currently lose_sum: 372.72242480516434
time_elpased: 0.953
batch start
#iterations: 243
currently lose_sum: 374.7310903072357
time_elpased: 0.941
batch start
#iterations: 244
currently lose_sum: 375.0279968380928
time_elpased: 0.934
batch start
#iterations: 245
currently lose_sum: 374.1256780028343
time_elpased: 0.944
batch start
#iterations: 246
currently lose_sum: 374.41677939891815
time_elpased: 0.94
batch start
#iterations: 247
currently lose_sum: 374.83540111780167
time_elpased: 0.943
batch start
#iterations: 248
currently lose_sum: 375.33361291885376
time_elpased: 0.93
batch start
#iterations: 249
currently lose_sum: 374.19099497795105
time_elpased: 0.929
batch start
#iterations: 250
currently lose_sum: 374.64317685365677
time_elpased: 0.947
batch start
#iterations: 251
currently lose_sum: 375.113223195076
time_elpased: 0.952
batch start
#iterations: 252
currently lose_sum: 375.4475004673004
time_elpased: 0.942
batch start
#iterations: 253
currently lose_sum: 373.2324229478836
time_elpased: 0.938
batch start
#iterations: 254
currently lose_sum: 374.54530131816864
time_elpased: 0.933
batch start
#iterations: 255
currently lose_sum: 374.91427129507065
time_elpased: 0.925
batch start
#iterations: 256
currently lose_sum: 374.1657662987709
time_elpased: 0.939
batch start
#iterations: 257
currently lose_sum: 374.9233290553093
time_elpased: 0.935
batch start
#iterations: 258
currently lose_sum: 375.1617249250412
time_elpased: 0.924
batch start
#iterations: 259
currently lose_sum: 374.0988100171089
time_elpased: 0.93
start validation test
0.561134020619
1.0
0.561134020619
0.718880010566
0
validation finish
batch start
#iterations: 260
currently lose_sum: 373.95232635736465
time_elpased: 0.927
batch start
#iterations: 261
currently lose_sum: 373.00715762376785
time_elpased: 0.964
batch start
#iterations: 262
currently lose_sum: 374.5139483809471
time_elpased: 0.916
batch start
#iterations: 263
currently lose_sum: 376.5840806365013
time_elpased: 0.938
batch start
#iterations: 264
currently lose_sum: 374.4331251382828
time_elpased: 0.94
batch start
#iterations: 265
currently lose_sum: 373.7894266843796
time_elpased: 0.939
batch start
#iterations: 266
currently lose_sum: 374.6068784594536
time_elpased: 0.939
batch start
#iterations: 267
currently lose_sum: 374.15189480781555
time_elpased: 0.94
batch start
#iterations: 268
currently lose_sum: 374.35935467481613
time_elpased: 0.939
batch start
#iterations: 269
currently lose_sum: 374.48151302337646
time_elpased: 0.924
batch start
#iterations: 270
currently lose_sum: 373.0951097011566
time_elpased: 0.945
batch start
#iterations: 271
currently lose_sum: 374.4880992770195
time_elpased: 0.93
batch start
#iterations: 272
currently lose_sum: 374.45255625247955
time_elpased: 0.937
batch start
#iterations: 273
currently lose_sum: 374.64434891939163
time_elpased: 0.924
batch start
#iterations: 274
currently lose_sum: 375.08048635721207
time_elpased: 0.941
batch start
#iterations: 275
currently lose_sum: 375.14018028974533
time_elpased: 0.946
batch start
#iterations: 276
currently lose_sum: 373.0396149158478
time_elpased: 0.927
batch start
#iterations: 277
currently lose_sum: 374.40935212373734
time_elpased: 0.929
batch start
#iterations: 278
currently lose_sum: 373.5001367330551
time_elpased: 0.932
batch start
#iterations: 279
currently lose_sum: 373.6397730112076
time_elpased: 0.936
start validation test
0.652783505155
1.0
0.652783505155
0.789920159681
0
validation finish
batch start
#iterations: 280
currently lose_sum: 372.88567823171616
time_elpased: 0.948
batch start
#iterations: 281
currently lose_sum: 374.98284620046616
time_elpased: 0.944
batch start
#iterations: 282
currently lose_sum: 374.7868257164955
time_elpased: 0.933
batch start
#iterations: 283
currently lose_sum: 373.7432942390442
time_elpased: 0.933
batch start
#iterations: 284
currently lose_sum: 375.01302605867386
time_elpased: 0.93
batch start
#iterations: 285
currently lose_sum: 373.31488460302353
time_elpased: 0.953
batch start
#iterations: 286
currently lose_sum: 373.9736006259918
time_elpased: 0.932
batch start
#iterations: 287
currently lose_sum: 373.00817662477493
time_elpased: 0.929
batch start
#iterations: 288
currently lose_sum: 374.8636333346367
time_elpased: 0.936
batch start
#iterations: 289
currently lose_sum: 374.0677624344826
time_elpased: 0.933
batch start
#iterations: 290
currently lose_sum: 373.64766335487366
time_elpased: 0.936
batch start
#iterations: 291
currently lose_sum: 373.37805914878845
time_elpased: 0.92
batch start
#iterations: 292
currently lose_sum: 374.24569422006607
time_elpased: 0.931
batch start
#iterations: 293
currently lose_sum: 374.57915729284286
time_elpased: 0.942
batch start
#iterations: 294
currently lose_sum: 373.6527134180069
time_elpased: 0.933
batch start
#iterations: 295
currently lose_sum: 373.3011355996132
time_elpased: 0.931
batch start
#iterations: 296
currently lose_sum: 373.1610797047615
time_elpased: 0.939
batch start
#iterations: 297
currently lose_sum: 373.98478478193283
time_elpased: 0.929
batch start
#iterations: 298
currently lose_sum: 373.63840103149414
time_elpased: 0.933
batch start
#iterations: 299
currently lose_sum: 372.657119512558
time_elpased: 0.932
start validation test
0.568659793814
1.0
0.568659793814
0.725026288118
0
validation finish
batch start
#iterations: 300
currently lose_sum: 373.1124337911606
time_elpased: 0.933
batch start
#iterations: 301
currently lose_sum: 374.16755521297455
time_elpased: 0.932
batch start
#iterations: 302
currently lose_sum: 374.66616958379745
time_elpased: 0.971
batch start
#iterations: 303
currently lose_sum: 375.1466574668884
time_elpased: 0.945
batch start
#iterations: 304
currently lose_sum: 373.6511719226837
time_elpased: 0.934
batch start
#iterations: 305
currently lose_sum: 375.0807774066925
time_elpased: 0.928
batch start
#iterations: 306
currently lose_sum: 372.73524206876755
time_elpased: 0.943
batch start
#iterations: 307
currently lose_sum: 372.5046337842941
time_elpased: 0.936
batch start
#iterations: 308
currently lose_sum: 373.1724165081978
time_elpased: 0.934
batch start
#iterations: 309
currently lose_sum: 373.73517167568207
time_elpased: 0.945
batch start
#iterations: 310
currently lose_sum: 372.27991914749146
time_elpased: 0.934
batch start
#iterations: 311
currently lose_sum: 374.7064272761345
time_elpased: 0.939
batch start
#iterations: 312
currently lose_sum: 372.57077449560165
time_elpased: 0.933
batch start
#iterations: 313
currently lose_sum: 372.3474089503288
time_elpased: 0.944
batch start
#iterations: 314
currently lose_sum: 373.962340593338
time_elpased: 0.929
batch start
#iterations: 315
currently lose_sum: 374.5499796271324
time_elpased: 0.943
batch start
#iterations: 316
currently lose_sum: 373.72492188215256
time_elpased: 0.925
batch start
#iterations: 317
currently lose_sum: 373.852510869503
time_elpased: 0.934
batch start
#iterations: 318
currently lose_sum: 374.4366094470024
time_elpased: 0.931
batch start
#iterations: 319
currently lose_sum: 373.12024170160294
time_elpased: 0.939
start validation test
0.63412371134
1.0
0.63412371134
0.776102454104
0
validation finish
batch start
#iterations: 320
currently lose_sum: 373.5086979866028
time_elpased: 0.958
batch start
#iterations: 321
currently lose_sum: 373.7390205860138
time_elpased: 0.93
batch start
#iterations: 322
currently lose_sum: 374.7049735188484
time_elpased: 0.928
batch start
#iterations: 323
currently lose_sum: 374.36454379558563
time_elpased: 0.941
batch start
#iterations: 324
currently lose_sum: 372.86724507808685
time_elpased: 0.932
batch start
#iterations: 325
currently lose_sum: 374.3279911875725
time_elpased: 0.94
batch start
#iterations: 326
currently lose_sum: 374.0573155283928
time_elpased: 0.947
batch start
#iterations: 327
currently lose_sum: 373.32111662626266
time_elpased: 0.944
batch start
#iterations: 328
currently lose_sum: 373.3611489534378
time_elpased: 0.937
batch start
#iterations: 329
currently lose_sum: 372.51117384433746
time_elpased: 0.935
batch start
#iterations: 330
currently lose_sum: 373.6717258095741
time_elpased: 0.939
batch start
#iterations: 331
currently lose_sum: 373.38942539691925
time_elpased: 0.95
batch start
#iterations: 332
currently lose_sum: 373.7906492948532
time_elpased: 0.935
batch start
#iterations: 333
currently lose_sum: 373.98867577314377
time_elpased: 0.928
batch start
#iterations: 334
currently lose_sum: 373.7762004137039
time_elpased: 0.944
batch start
#iterations: 335
currently lose_sum: 373.1818438768387
time_elpased: 0.935
batch start
#iterations: 336
currently lose_sum: 374.6018745303154
time_elpased: 0.938
batch start
#iterations: 337
currently lose_sum: 372.3321225643158
time_elpased: 0.93
batch start
#iterations: 338
currently lose_sum: 374.041774392128
time_elpased: 0.939
batch start
#iterations: 339
currently lose_sum: 373.5988445878029
time_elpased: 0.937
start validation test
0.642474226804
1.0
0.642474226804
0.782324880743
0
validation finish
batch start
#iterations: 340
currently lose_sum: 373.9044123888016
time_elpased: 0.951
batch start
#iterations: 341
currently lose_sum: 374.5289562344551
time_elpased: 0.929
batch start
#iterations: 342
currently lose_sum: 373.69878524541855
time_elpased: 0.936
batch start
#iterations: 343
currently lose_sum: 373.0242617726326
time_elpased: 0.955
batch start
#iterations: 344
currently lose_sum: 372.95052993297577
time_elpased: 0.937
batch start
#iterations: 345
currently lose_sum: 373.4145687818527
time_elpased: 0.933
batch start
#iterations: 346
currently lose_sum: 373.689853310585
time_elpased: 0.937
batch start
#iterations: 347
currently lose_sum: 374.4447685480118
time_elpased: 0.926
batch start
#iterations: 348
currently lose_sum: 372.1048649549484
time_elpased: 0.933
batch start
#iterations: 349
currently lose_sum: 373.43321788311005
time_elpased: 0.94
batch start
#iterations: 350
currently lose_sum: 373.68700379133224
time_elpased: 0.919
batch start
#iterations: 351
currently lose_sum: 373.89927583932877
time_elpased: 0.932
batch start
#iterations: 352
currently lose_sum: 374.1120662689209
time_elpased: 0.94
batch start
#iterations: 353
currently lose_sum: 373.1581570506096
time_elpased: 0.94
batch start
#iterations: 354
currently lose_sum: 372.7989107966423
time_elpased: 0.943
batch start
#iterations: 355
currently lose_sum: 372.73472559452057
time_elpased: 0.94
batch start
#iterations: 356
currently lose_sum: 372.566468000412
time_elpased: 0.932
batch start
#iterations: 357
currently lose_sum: 373.5457639694214
time_elpased: 0.942
batch start
#iterations: 358
currently lose_sum: 373.1695411801338
time_elpased: 0.931
batch start
#iterations: 359
currently lose_sum: 371.9367163181305
time_elpased: 0.935
start validation test
0.670206185567
1.0
0.670206185567
0.802543052898
0
validation finish
batch start
#iterations: 360
currently lose_sum: 373.8208920955658
time_elpased: 0.94
batch start
#iterations: 361
currently lose_sum: 373.57435804605484
time_elpased: 0.935
batch start
#iterations: 362
currently lose_sum: 373.3958476781845
time_elpased: 0.936
batch start
#iterations: 363
currently lose_sum: 375.1117047071457
time_elpased: 0.939
batch start
#iterations: 364
currently lose_sum: 372.46226102113724
time_elpased: 0.924
batch start
#iterations: 365
currently lose_sum: 373.1116380095482
time_elpased: 0.943
batch start
#iterations: 366
currently lose_sum: 374.29462081193924
time_elpased: 0.94
batch start
#iterations: 367
currently lose_sum: 374.2862002849579
time_elpased: 0.947
batch start
#iterations: 368
currently lose_sum: 373.009527862072
time_elpased: 0.939
batch start
#iterations: 369
currently lose_sum: 371.6250629425049
time_elpased: 0.953
batch start
#iterations: 370
currently lose_sum: 372.40390396118164
time_elpased: 0.942
batch start
#iterations: 371
currently lose_sum: 373.5297293663025
time_elpased: 0.944
batch start
#iterations: 372
currently lose_sum: 372.614358484745
time_elpased: 0.942
batch start
#iterations: 373
currently lose_sum: 373.81930750608444
time_elpased: 0.938
batch start
#iterations: 374
currently lose_sum: 373.7228303551674
time_elpased: 0.932
batch start
#iterations: 375
currently lose_sum: 371.85704958438873
time_elpased: 0.934
batch start
#iterations: 376
currently lose_sum: 372.6833188533783
time_elpased: 0.927
batch start
#iterations: 377
currently lose_sum: 373.7135435938835
time_elpased: 0.936
batch start
#iterations: 378
currently lose_sum: 373.22345304489136
time_elpased: 0.945
batch start
#iterations: 379
currently lose_sum: 373.408649623394
time_elpased: 0.939
start validation test
0.71793814433
1.0
0.71793814433
0.835813730197
0
validation finish
batch start
#iterations: 380
currently lose_sum: 374.8663093447685
time_elpased: 0.945
batch start
#iterations: 381
currently lose_sum: 374.2621758580208
time_elpased: 0.931
batch start
#iterations: 382
currently lose_sum: 373.8483055830002
time_elpased: 0.943
batch start
#iterations: 383
currently lose_sum: 373.28599059581757
time_elpased: 0.932
batch start
#iterations: 384
currently lose_sum: 372.9142447113991
time_elpased: 0.93
batch start
#iterations: 385
currently lose_sum: 372.6985807418823
time_elpased: 0.932
batch start
#iterations: 386
currently lose_sum: 373.8137363791466
time_elpased: 0.956
batch start
#iterations: 387
currently lose_sum: 372.5136690735817
time_elpased: 0.939
batch start
#iterations: 388
currently lose_sum: 372.5145327448845
time_elpased: 0.933
batch start
#iterations: 389
currently lose_sum: 373.2791000008583
time_elpased: 0.929
batch start
#iterations: 390
currently lose_sum: 371.9636377096176
time_elpased: 0.935
batch start
#iterations: 391
currently lose_sum: 372.55075120925903
time_elpased: 0.946
batch start
#iterations: 392
currently lose_sum: 371.89071410894394
time_elpased: 0.93
batch start
#iterations: 393
currently lose_sum: 373.54204028844833
time_elpased: 0.932
batch start
#iterations: 394
currently lose_sum: 373.2134208083153
time_elpased: 0.93
batch start
#iterations: 395
currently lose_sum: 373.4674355983734
time_elpased: 0.94
batch start
#iterations: 396
currently lose_sum: 373.24662017822266
time_elpased: 0.942
batch start
#iterations: 397
currently lose_sum: 372.2344737648964
time_elpased: 0.95
batch start
#iterations: 398
currently lose_sum: 373.21321761608124
time_elpased: 0.928
batch start
#iterations: 399
currently lose_sum: 371.6941617131233
time_elpased: 0.956
start validation test
0.624948453608
1.0
0.624948453608
0.769191726938
0
validation finish
acc: 0.791
pre: 1.000
rec: 0.791
F1: 0.883
auc: 0.000
