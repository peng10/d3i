start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 396.27022367715836
time_elpased: 5.147
batch start
#iterations: 1
currently lose_sum: 388.3451145887375
time_elpased: 5.119
batch start
#iterations: 2
currently lose_sum: 382.90620839595795
time_elpased: 5.109
batch start
#iterations: 3
currently lose_sum: 380.39711850881577
time_elpased: 5.076
batch start
#iterations: 4
currently lose_sum: 379.36800557374954
time_elpased: 5.113
batch start
#iterations: 5
currently lose_sum: 378.1667901277542
time_elpased: 5.115
batch start
#iterations: 6
currently lose_sum: 377.8103040456772
time_elpased: 5.134
batch start
#iterations: 7
currently lose_sum: 377.1586836576462
time_elpased: 5.098
batch start
#iterations: 8
currently lose_sum: 376.3364129662514
time_elpased: 5.086
batch start
#iterations: 9
currently lose_sum: 377.2859492301941
time_elpased: 5.089
batch start
#iterations: 10
currently lose_sum: 375.4094756245613
time_elpased: 5.106
batch start
#iterations: 11
currently lose_sum: 375.51978546380997
time_elpased: 5.096
batch start
#iterations: 12
currently lose_sum: 376.00851410627365
time_elpased: 5.073
batch start
#iterations: 13
currently lose_sum: 376.6584061384201
time_elpased: 5.104
batch start
#iterations: 14
currently lose_sum: 373.22313195466995
time_elpased: 5.084
batch start
#iterations: 15
currently lose_sum: 374.93515235185623
time_elpased: 5.076
batch start
#iterations: 16
currently lose_sum: 374.55857241153717
time_elpased: 5.094
batch start
#iterations: 17
currently lose_sum: 376.1365661621094
time_elpased: 5.106
batch start
#iterations: 18
currently lose_sum: 373.69478315114975
time_elpased: 5.105
batch start
#iterations: 19
currently lose_sum: 375.58717703819275
time_elpased: 5.09
start validation test
0.441855670103
1.0
0.441855670103
0.612898612899
0
validation finish
batch start
#iterations: 20
currently lose_sum: 374.5847705602646
time_elpased: 5.094
batch start
#iterations: 21
currently lose_sum: 375.0658031105995
time_elpased: 5.077
batch start
#iterations: 22
currently lose_sum: 374.72805094718933
time_elpased: 5.059
batch start
#iterations: 23
currently lose_sum: 375.75878471136093
time_elpased: 5.071
batch start
#iterations: 24
currently lose_sum: 375.04526484012604
time_elpased: 5.093
batch start
#iterations: 25
currently lose_sum: 374.90821862220764
time_elpased: 5.09
batch start
#iterations: 26
currently lose_sum: 374.1681117415428
time_elpased: 5.076
batch start
#iterations: 27
currently lose_sum: 374.08593595027924
time_elpased: 5.063
batch start
#iterations: 28
currently lose_sum: 372.97738337516785
time_elpased: 5.074
batch start
#iterations: 29
currently lose_sum: 375.10364508628845
time_elpased: 5.074
batch start
#iterations: 30
currently lose_sum: 374.74472665786743
time_elpased: 5.071
batch start
#iterations: 31
currently lose_sum: 373.86437398195267
time_elpased: 5.109
batch start
#iterations: 32
currently lose_sum: 374.97899997234344
time_elpased: 5.104
batch start
#iterations: 33
currently lose_sum: 374.80259281396866
time_elpased: 5.104
batch start
#iterations: 34
currently lose_sum: 374.1425878405571
time_elpased: 5.096
batch start
#iterations: 35
currently lose_sum: 375.86026281118393
time_elpased: 5.112
batch start
#iterations: 36
currently lose_sum: 374.19241803884506
time_elpased: 5.072
batch start
#iterations: 37
currently lose_sum: 373.2508395910263
time_elpased: 5.067
batch start
#iterations: 38
currently lose_sum: 374.88129752874374
time_elpased: 5.07
batch start
#iterations: 39
currently lose_sum: 374.6160845756531
time_elpased: 5.086
start validation test
0.454329896907
1.0
0.454329896907
0.624796200468
0
validation finish
batch start
#iterations: 40
currently lose_sum: 373.6121276021004
time_elpased: 5.114
batch start
#iterations: 41
currently lose_sum: 374.08439058065414
time_elpased: 5.131
batch start
#iterations: 42
currently lose_sum: 372.51665437221527
time_elpased: 5.099
batch start
#iterations: 43
currently lose_sum: 373.19728887081146
time_elpased: 5.091
batch start
#iterations: 44
currently lose_sum: 375.06653797626495
time_elpased: 5.083
batch start
#iterations: 45
currently lose_sum: 373.1001720428467
time_elpased: 5.084
batch start
#iterations: 46
currently lose_sum: 373.1618201136589
time_elpased: 5.117
batch start
#iterations: 47
currently lose_sum: 373.67994779348373
time_elpased: 5.124
batch start
#iterations: 48
currently lose_sum: 372.7040866613388
time_elpased: 5.101
batch start
#iterations: 49
currently lose_sum: 374.0475865006447
time_elpased: 5.095
batch start
#iterations: 50
currently lose_sum: 372.2026104927063
time_elpased: 5.098
batch start
#iterations: 51
currently lose_sum: 373.46334052085876
time_elpased: 5.089
batch start
#iterations: 52
currently lose_sum: 374.2813779115677
time_elpased: 5.101
batch start
#iterations: 53
currently lose_sum: 373.4140781760216
time_elpased: 5.116
batch start
#iterations: 54
currently lose_sum: 373.7788571715355
time_elpased: 5.12
batch start
#iterations: 55
currently lose_sum: 372.9751372933388
time_elpased: 5.11
batch start
#iterations: 56
currently lose_sum: 373.9104609489441
time_elpased: 5.105
batch start
#iterations: 57
currently lose_sum: 373.1113373041153
time_elpased: 5.105
batch start
#iterations: 58
currently lose_sum: 372.4909374117851
time_elpased: 5.161
batch start
#iterations: 59
currently lose_sum: 374.03477042913437
time_elpased: 5.46
start validation test
0.623402061856
1.0
0.623402061856
0.768019305264
0
validation finish
batch start
#iterations: 60
currently lose_sum: 373.47947150468826
time_elpased: 5.475
batch start
#iterations: 61
currently lose_sum: 373.16425412893295
time_elpased: 5.382
batch start
#iterations: 62
currently lose_sum: 373.3653769493103
time_elpased: 5.116
batch start
#iterations: 63
currently lose_sum: 372.3888921737671
time_elpased: 5.136
batch start
#iterations: 64
currently lose_sum: 373.30958890914917
time_elpased: 5.129
batch start
#iterations: 65
currently lose_sum: 374.0075092315674
time_elpased: 5.112
batch start
#iterations: 66
currently lose_sum: 373.3631389737129
time_elpased: 5.123
batch start
#iterations: 67
currently lose_sum: 372.81233537197113
time_elpased: 5.13
batch start
#iterations: 68
currently lose_sum: 373.747800052166
time_elpased: 5.105
batch start
#iterations: 69
currently lose_sum: 372.80080342292786
time_elpased: 5.126
batch start
#iterations: 70
currently lose_sum: 372.8477389216423
time_elpased: 5.125
batch start
#iterations: 71
currently lose_sum: 373.32890713214874
time_elpased: 5.117
batch start
#iterations: 72
currently lose_sum: 373.09739887714386
time_elpased: 5.121
batch start
#iterations: 73
currently lose_sum: 373.45870810747147
time_elpased: 5.13
batch start
#iterations: 74
currently lose_sum: 373.04840183258057
time_elpased: 5.113
batch start
#iterations: 75
currently lose_sum: 371.354000210762
time_elpased: 5.092
batch start
#iterations: 76
currently lose_sum: 372.45285964012146
time_elpased: 5.092
batch start
#iterations: 77
currently lose_sum: 372.6960070133209
time_elpased: 5.191
batch start
#iterations: 78
currently lose_sum: 372.94560992717743
time_elpased: 5.443
batch start
#iterations: 79
currently lose_sum: 372.73663479089737
time_elpased: 5.401
start validation test
0.707525773196
1.0
0.707525773196
0.828714604842
0
validation finish
batch start
#iterations: 80
currently lose_sum: 372.1482689380646
time_elpased: 5.113
batch start
#iterations: 81
currently lose_sum: 373.81779086589813
time_elpased: 5.12
batch start
#iterations: 82
currently lose_sum: 373.04430425167084
time_elpased: 5.109
batch start
#iterations: 83
currently lose_sum: 371.7153595685959
time_elpased: 5.129
batch start
#iterations: 84
currently lose_sum: 373.8081876039505
time_elpased: 5.125
batch start
#iterations: 85
currently lose_sum: 373.18872129917145
time_elpased: 5.125
batch start
#iterations: 86
currently lose_sum: 373.3664884567261
time_elpased: 5.169
batch start
#iterations: 87
currently lose_sum: 372.6639473438263
time_elpased: 5.477
batch start
#iterations: 88
currently lose_sum: 372.1140843629837
time_elpased: 5.444
batch start
#iterations: 89
currently lose_sum: 373.3569349050522
time_elpased: 5.435
batch start
#iterations: 90
currently lose_sum: 373.97329246997833
time_elpased: 5.428
batch start
#iterations: 91
currently lose_sum: 373.3224051594734
time_elpased: 5.235
batch start
#iterations: 92
currently lose_sum: 372.5324451327324
time_elpased: 5.14
batch start
#iterations: 93
currently lose_sum: 373.4414384365082
time_elpased: 5.135
batch start
#iterations: 94
currently lose_sum: 372.9376135468483
time_elpased: 5.124
batch start
#iterations: 95
currently lose_sum: 371.94663149118423
time_elpased: 5.141
batch start
#iterations: 96
currently lose_sum: 373.36904978752136
time_elpased: 5.129
batch start
#iterations: 97
currently lose_sum: 372.2544199228287
time_elpased: 5.098
batch start
#iterations: 98
currently lose_sum: 372.306271314621
time_elpased: 5.078
batch start
#iterations: 99
currently lose_sum: 372.62104576826096
time_elpased: 5.086
start validation test
0.638762886598
1.0
0.638762886598
0.779567186714
0
validation finish
batch start
#iterations: 100
currently lose_sum: 372.83832931518555
time_elpased: 5.054
batch start
#iterations: 101
currently lose_sum: 372.51528656482697
time_elpased: 5.09
batch start
#iterations: 102
currently lose_sum: 372.1126610636711
time_elpased: 5.077
batch start
#iterations: 103
currently lose_sum: 372.9756551384926
time_elpased: 5.085
batch start
#iterations: 104
currently lose_sum: 373.15263921022415
time_elpased: 5.078
batch start
#iterations: 105
currently lose_sum: 372.4403151869774
time_elpased: 5.057
batch start
#iterations: 106
currently lose_sum: 373.27509063482285
time_elpased: 5.084
batch start
#iterations: 107
currently lose_sum: 372.44147008657455
time_elpased: 5.102
batch start
#iterations: 108
currently lose_sum: 372.71048617362976
time_elpased: 5.083
batch start
#iterations: 109
currently lose_sum: 372.1461510658264
time_elpased: 5.07
batch start
#iterations: 110
currently lose_sum: 373.56295388936996
time_elpased: 5.064
batch start
#iterations: 111
currently lose_sum: 372.016827583313
time_elpased: 5.075
batch start
#iterations: 112
currently lose_sum: 372.1704451441765
time_elpased: 5.051
batch start
#iterations: 113
currently lose_sum: 373.19163250923157
time_elpased: 5.046
batch start
#iterations: 114
currently lose_sum: 372.06140953302383
time_elpased: 5.068
batch start
#iterations: 115
currently lose_sum: 372.26478588581085
time_elpased: 5.067
batch start
#iterations: 116
currently lose_sum: 373.2371616959572
time_elpased: 5.05
batch start
#iterations: 117
currently lose_sum: 371.6809687614441
time_elpased: 5.077
batch start
#iterations: 118
currently lose_sum: 371.165851354599
time_elpased: 5.049
batch start
#iterations: 119
currently lose_sum: 372.55932146310806
time_elpased: 5.054
start validation test
0.629896907216
1.0
0.629896907216
0.772928526249
0
validation finish
batch start
#iterations: 120
currently lose_sum: 373.2752360701561
time_elpased: 5.041
batch start
#iterations: 121
currently lose_sum: 371.7056542634964
time_elpased: 5.052
batch start
#iterations: 122
currently lose_sum: 370.57834738492966
time_elpased: 5.062
batch start
#iterations: 123
currently lose_sum: 371.85574221611023
time_elpased: 5.024
batch start
#iterations: 124
currently lose_sum: 371.16963374614716
time_elpased: 5.046
batch start
#iterations: 125
currently lose_sum: 372.3148975968361
time_elpased: 5.063
batch start
#iterations: 126
currently lose_sum: 373.85470575094223
time_elpased: 5.059
batch start
#iterations: 127
currently lose_sum: 372.0437881946564
time_elpased: 5.069
batch start
#iterations: 128
currently lose_sum: 371.530813395977
time_elpased: 5.026
batch start
#iterations: 129
currently lose_sum: 372.5098156929016
time_elpased: 5.042
batch start
#iterations: 130
currently lose_sum: 371.39404159784317
time_elpased: 5.044
batch start
#iterations: 131
currently lose_sum: 372.9907850623131
time_elpased: 5.057
batch start
#iterations: 132
currently lose_sum: 372.61382073163986
time_elpased: 5.041
batch start
#iterations: 133
currently lose_sum: 371.50216829776764
time_elpased: 5.04
batch start
#iterations: 134
currently lose_sum: 373.2746489048004
time_elpased: 5.055
batch start
#iterations: 135
currently lose_sum: 372.6400822401047
time_elpased: 5.03
batch start
#iterations: 136
currently lose_sum: 373.8934899568558
time_elpased: 5.036
batch start
#iterations: 137
currently lose_sum: 371.4853721857071
time_elpased: 5.239
batch start
#iterations: 138
currently lose_sum: 373.43907338380814
time_elpased: 5.056
batch start
#iterations: 139
currently lose_sum: 372.5400475859642
time_elpased: 5.046
start validation test
0.653298969072
1.0
0.653298969072
0.790297437177
0
validation finish
batch start
#iterations: 140
currently lose_sum: 372.70491218566895
time_elpased: 5.078
batch start
#iterations: 141
currently lose_sum: 373.0985528230667
time_elpased: 5.109
batch start
#iterations: 142
currently lose_sum: 372.79823595285416
time_elpased: 5.021
batch start
#iterations: 143
currently lose_sum: 373.3181855082512
time_elpased: 5.045
batch start
#iterations: 144
currently lose_sum: 372.5616263151169
time_elpased: 5.123
batch start
#iterations: 145
currently lose_sum: 372.0376249551773
time_elpased: 5.112
batch start
#iterations: 146
currently lose_sum: 371.5497047305107
time_elpased: 5.127
batch start
#iterations: 147
currently lose_sum: 372.02305632829666
time_elpased: 5.14
batch start
#iterations: 148
currently lose_sum: 372.5385325551033
time_elpased: 5.114
batch start
#iterations: 149
currently lose_sum: 372.9299384355545
time_elpased: 5.135
batch start
#iterations: 150
currently lose_sum: 371.9013411998749
time_elpased: 5.133
batch start
#iterations: 151
currently lose_sum: 372.73531210422516
time_elpased: 5.113
batch start
#iterations: 152
currently lose_sum: 371.7394463419914
time_elpased: 5.095
batch start
#iterations: 153
currently lose_sum: 373.5561497807503
time_elpased: 5.104
batch start
#iterations: 154
currently lose_sum: 372.0759325623512
time_elpased: 5.139
batch start
#iterations: 155
currently lose_sum: 372.4679161310196
time_elpased: 5.166
batch start
#iterations: 156
currently lose_sum: 372.75987470149994
time_elpased: 5.153
batch start
#iterations: 157
currently lose_sum: 372.2274643778801
time_elpased: 5.133
batch start
#iterations: 158
currently lose_sum: 372.443305850029
time_elpased: 5.125
batch start
#iterations: 159
currently lose_sum: 373.8121635913849
time_elpased: 5.075
start validation test
0.693092783505
1.0
0.693092783505
0.818729830116
0
validation finish
batch start
#iterations: 160
currently lose_sum: 372.53618282079697
time_elpased: 5.083
batch start
#iterations: 161
currently lose_sum: 372.4790291786194
time_elpased: 5.091
batch start
#iterations: 162
currently lose_sum: 372.55560195446014
time_elpased: 5.092
batch start
#iterations: 163
currently lose_sum: 372.2492223381996
time_elpased: 5.077
batch start
#iterations: 164
currently lose_sum: 372.3034018278122
time_elpased: 5.067
batch start
#iterations: 165
currently lose_sum: 371.36759638786316
time_elpased: 5.102
batch start
#iterations: 166
currently lose_sum: 371.60381948947906
time_elpased: 5.062
batch start
#iterations: 167
currently lose_sum: 371.9398727416992
time_elpased: 5.105
batch start
#iterations: 168
currently lose_sum: 372.2795181274414
time_elpased: 5.083
batch start
#iterations: 169
currently lose_sum: 371.0658708810806
time_elpased: 5.076
batch start
#iterations: 170
currently lose_sum: 372.00763791799545
time_elpased: 5.074
batch start
#iterations: 171
currently lose_sum: 372.8273646235466
time_elpased: 5.075
batch start
#iterations: 172
currently lose_sum: 372.8407384753227
time_elpased: 5.09
batch start
#iterations: 173
currently lose_sum: 372.21382838487625
time_elpased: 5.106
batch start
#iterations: 174
currently lose_sum: 371.27163594961166
time_elpased: 5.086
batch start
#iterations: 175
currently lose_sum: 373.55045545101166
time_elpased: 5.105
batch start
#iterations: 176
currently lose_sum: 371.7500813007355
time_elpased: 5.095
batch start
#iterations: 177
currently lose_sum: 370.98810148239136
time_elpased: 5.101
batch start
#iterations: 178
currently lose_sum: 373.19942063093185
time_elpased: 5.097
batch start
#iterations: 179
currently lose_sum: 372.57355266809464
time_elpased: 5.088
start validation test
0.661237113402
1.0
0.661237113402
0.796077944644
0
validation finish
batch start
#iterations: 180
currently lose_sum: 372.53351098299026
time_elpased: 5.075
batch start
#iterations: 181
currently lose_sum: 371.83334243297577
time_elpased: 5.115
batch start
#iterations: 182
currently lose_sum: 372.0562433004379
time_elpased: 5.097
batch start
#iterations: 183
currently lose_sum: 372.51349401474
time_elpased: 5.104
batch start
#iterations: 184
currently lose_sum: 372.0748233795166
time_elpased: 5.079
batch start
#iterations: 185
currently lose_sum: 372.4098858833313
time_elpased: 5.086
batch start
#iterations: 186
currently lose_sum: 371.7039744257927
time_elpased: 5.093
batch start
#iterations: 187
currently lose_sum: 370.41233479976654
time_elpased: 5.105
batch start
#iterations: 188
currently lose_sum: 373.26788341999054
time_elpased: 5.094
batch start
#iterations: 189
currently lose_sum: 372.9067445397377
time_elpased: 5.107
batch start
#iterations: 190
currently lose_sum: 370.6307296156883
time_elpased: 5.103
batch start
#iterations: 191
currently lose_sum: 372.1572015285492
time_elpased: 5.087
batch start
#iterations: 192
currently lose_sum: 372.8350704908371
time_elpased: 5.077
batch start
#iterations: 193
currently lose_sum: 373.64422649145126
time_elpased: 5.111
batch start
#iterations: 194
currently lose_sum: 371.37540048360825
time_elpased: 5.114
batch start
#iterations: 195
currently lose_sum: 372.0047643184662
time_elpased: 5.083
batch start
#iterations: 196
currently lose_sum: 374.091067135334
time_elpased: 5.061
batch start
#iterations: 197
currently lose_sum: 371.1459312438965
time_elpased: 5.068
batch start
#iterations: 198
currently lose_sum: 372.84429109096527
time_elpased: 5.063
batch start
#iterations: 199
currently lose_sum: 371.52279245853424
time_elpased: 5.052
start validation test
0.604845360825
1.0
0.604845360825
0.753774009122
0
validation finish
batch start
#iterations: 200
currently lose_sum: 373.1488156914711
time_elpased: 5.074
batch start
#iterations: 201
currently lose_sum: 371.3461917042732
time_elpased: 5.077
batch start
#iterations: 202
currently lose_sum: 371.18162685632706
time_elpased: 5.055
batch start
#iterations: 203
currently lose_sum: 370.15978640317917
time_elpased: 5.103
batch start
#iterations: 204
currently lose_sum: 372.57575023174286
time_elpased: 5.084
batch start
#iterations: 205
currently lose_sum: 371.22116112709045
time_elpased: 5.097
batch start
#iterations: 206
currently lose_sum: 371.53148728609085
time_elpased: 5.08
batch start
#iterations: 207
currently lose_sum: 372.724209189415
time_elpased: 5.082
batch start
#iterations: 208
currently lose_sum: 372.35469138622284
time_elpased: 5.06
batch start
#iterations: 209
currently lose_sum: 372.3162225484848
time_elpased: 5.069
batch start
#iterations: 210
currently lose_sum: 371.5299045443535
time_elpased: 5.082
batch start
#iterations: 211
currently lose_sum: 371.4874332547188
time_elpased: 5.07
batch start
#iterations: 212
currently lose_sum: 373.15984535217285
time_elpased: 5.06
batch start
#iterations: 213
currently lose_sum: 373.0152208805084
time_elpased: 5.082
batch start
#iterations: 214
currently lose_sum: 372.7045369744301
time_elpased: 5.105
batch start
#iterations: 215
currently lose_sum: 372.13670456409454
time_elpased: 5.089
batch start
#iterations: 216
currently lose_sum: 372.1767593026161
time_elpased: 5.086
batch start
#iterations: 217
currently lose_sum: 371.88208597898483
time_elpased: 5.084
batch start
#iterations: 218
currently lose_sum: 371.9532683491707
time_elpased: 5.092
batch start
#iterations: 219
currently lose_sum: 373.3091490864754
time_elpased: 5.079
start validation test
0.621030927835
1.0
0.621030927835
0.76621724752
0
validation finish
batch start
#iterations: 220
currently lose_sum: 371.58787912130356
time_elpased: 5.106
batch start
#iterations: 221
currently lose_sum: 372.4607875943184
time_elpased: 5.092
batch start
#iterations: 222
currently lose_sum: 372.31112146377563
time_elpased: 5.075
batch start
#iterations: 223
currently lose_sum: 371.9065330028534
time_elpased: 5.071
batch start
#iterations: 224
currently lose_sum: 372.73922967910767
time_elpased: 5.06
batch start
#iterations: 225
currently lose_sum: 371.72830033302307
time_elpased: 5.048
batch start
#iterations: 226
currently lose_sum: 372.2936291694641
time_elpased: 5.072
batch start
#iterations: 227
currently lose_sum: 372.20348823070526
time_elpased: 5.066
batch start
#iterations: 228
currently lose_sum: 372.2612002491951
time_elpased: 5.067
batch start
#iterations: 229
currently lose_sum: 370.4144353866577
time_elpased: 5.044
batch start
#iterations: 230
currently lose_sum: 371.9685669541359
time_elpased: 5.104
batch start
#iterations: 231
currently lose_sum: 372.4982495903969
time_elpased: 5.108
batch start
#iterations: 232
currently lose_sum: 372.34238505363464
time_elpased: 5.096
batch start
#iterations: 233
currently lose_sum: 372.63348442316055
time_elpased: 5.074
batch start
#iterations: 234
currently lose_sum: 372.11464536190033
time_elpased: 5.082
batch start
#iterations: 235
currently lose_sum: 371.6198682188988
time_elpased: 5.078
batch start
#iterations: 236
currently lose_sum: 371.79536694288254
time_elpased: 5.089
batch start
#iterations: 237
currently lose_sum: 371.303922355175
time_elpased: 5.087
batch start
#iterations: 238
currently lose_sum: 372.54932528734207
time_elpased: 5.074
batch start
#iterations: 239
currently lose_sum: 371.70632034540176
time_elpased: 5.073
start validation test
0.637835051546
1.0
0.637835051546
0.778875810411
0
validation finish
batch start
#iterations: 240
currently lose_sum: 370.9561581015587
time_elpased: 5.264
batch start
#iterations: 241
currently lose_sum: 371.3693122267723
time_elpased: 5.185
batch start
#iterations: 242
currently lose_sum: 370.38030952215195
time_elpased: 5.105
batch start
#iterations: 243
currently lose_sum: 372.6056246161461
time_elpased: 5.084
batch start
#iterations: 244
currently lose_sum: 372.8101288676262
time_elpased: 5.081
batch start
#iterations: 245
currently lose_sum: 371.83070081472397
time_elpased: 5.057
batch start
#iterations: 246
currently lose_sum: 372.106021463871
time_elpased: 5.056
batch start
#iterations: 247
currently lose_sum: 373.0594679117203
time_elpased: 5.052
batch start
#iterations: 248
currently lose_sum: 373.1843386888504
time_elpased: 5.1
batch start
#iterations: 249
currently lose_sum: 371.91059732437134
time_elpased: 5.065
batch start
#iterations: 250
currently lose_sum: 372.50445932149887
time_elpased: 5.048
batch start
#iterations: 251
currently lose_sum: 373.01032787561417
time_elpased: 5.062
batch start
#iterations: 252
currently lose_sum: 373.5528755784035
time_elpased: 5.103
batch start
#iterations: 253
currently lose_sum: 371.26235258579254
time_elpased: 5.066
batch start
#iterations: 254
currently lose_sum: 372.6380949616432
time_elpased: 5.043
batch start
#iterations: 255
currently lose_sum: 372.8036609888077
time_elpased: 5.039
batch start
#iterations: 256
currently lose_sum: 372.25051951408386
time_elpased: 5.043
batch start
#iterations: 257
currently lose_sum: 372.7302882671356
time_elpased: 5.056
batch start
#iterations: 258
currently lose_sum: 373.16536170244217
time_elpased: 5.054
batch start
#iterations: 259
currently lose_sum: 371.89391952753067
time_elpased: 5.051
start validation test
0.662164948454
1.0
0.662164948454
0.796749984494
0
validation finish
batch start
#iterations: 260
currently lose_sum: 371.7763268351555
time_elpased: 5.057
batch start
#iterations: 261
currently lose_sum: 370.8518136739731
time_elpased: 5.088
batch start
#iterations: 262
currently lose_sum: 372.4425599575043
time_elpased: 5.101
batch start
#iterations: 263
currently lose_sum: 374.49357241392136
time_elpased: 5.088
batch start
#iterations: 264
currently lose_sum: 372.37421786785126
time_elpased: 5.074
batch start
#iterations: 265
currently lose_sum: 371.9766149520874
time_elpased: 5.057
batch start
#iterations: 266
currently lose_sum: 372.46350914239883
time_elpased: 5.063
batch start
#iterations: 267
currently lose_sum: 372.2004691362381
time_elpased: 5.091
batch start
#iterations: 268
currently lose_sum: 372.231182038784
time_elpased: 5.092
batch start
#iterations: 269
currently lose_sum: 372.66644674539566
time_elpased: 5.077
batch start
#iterations: 270
currently lose_sum: 371.02293944358826
time_elpased: 5.094
batch start
#iterations: 271
currently lose_sum: 372.3264743089676
time_elpased: 5.1
batch start
#iterations: 272
currently lose_sum: 372.82168370485306
time_elpased: 5.061
batch start
#iterations: 273
currently lose_sum: 372.7839711904526
time_elpased: 5.072
batch start
#iterations: 274
currently lose_sum: 372.90676122903824
time_elpased: 5.075
batch start
#iterations: 275
currently lose_sum: 373.1216971874237
time_elpased: 5.056
batch start
#iterations: 276
currently lose_sum: 371.13488578796387
time_elpased: 5.058
batch start
#iterations: 277
currently lose_sum: 372.28345787525177
time_elpased: 5.069
batch start
#iterations: 278
currently lose_sum: 371.7304467558861
time_elpased: 5.084
batch start
#iterations: 279
currently lose_sum: 371.65548354387283
time_elpased: 5.104
start validation test
0.607113402062
1.0
0.607113402062
0.75553274745
0
validation finish
batch start
#iterations: 280
currently lose_sum: 371.1791011095047
time_elpased: 5.101
batch start
#iterations: 281
currently lose_sum: 373.1948488354683
time_elpased: 5.101
batch start
#iterations: 282
currently lose_sum: 372.49673241376877
time_elpased: 5.091
batch start
#iterations: 283
currently lose_sum: 371.96277940273285
time_elpased: 5.11
batch start
#iterations: 284
currently lose_sum: 373.2751250267029
time_elpased: 5.062
batch start
#iterations: 285
currently lose_sum: 371.3552750349045
time_elpased: 5.067
batch start
#iterations: 286
currently lose_sum: 372.03750491142273
time_elpased: 5.067
batch start
#iterations: 287
currently lose_sum: 371.2082785964012
time_elpased: 5.059
batch start
#iterations: 288
currently lose_sum: 372.6790683865547
time_elpased: 5.057
batch start
#iterations: 289
currently lose_sum: 372.1002517938614
time_elpased: 5.049
batch start
#iterations: 290
currently lose_sum: 371.77346259355545
time_elpased: 5.024
batch start
#iterations: 291
currently lose_sum: 371.3263120651245
time_elpased: 5.055
batch start
#iterations: 292
currently lose_sum: 372.1299148797989
time_elpased: 5.075
batch start
#iterations: 293
currently lose_sum: 372.8739490509033
time_elpased: 5.047
batch start
#iterations: 294
currently lose_sum: 372.0245845913887
time_elpased: 5.035
batch start
#iterations: 295
currently lose_sum: 371.38411259651184
time_elpased: 5.091
batch start
#iterations: 296
currently lose_sum: 371.4486443400383
time_elpased: 5.098
batch start
#iterations: 297
currently lose_sum: 371.80333733558655
time_elpased: 5.099
batch start
#iterations: 298
currently lose_sum: 371.90321213006973
time_elpased: 5.08
batch start
#iterations: 299
currently lose_sum: 370.9106531739235
time_elpased: 5.09
start validation test
0.654845360825
1.0
0.654845360825
0.791427859457
0
validation finish
batch start
#iterations: 300
currently lose_sum: 371.0749551653862
time_elpased: 5.09
batch start
#iterations: 301
currently lose_sum: 372.162293612957
time_elpased: 5.045
batch start
#iterations: 302
currently lose_sum: 372.9857226610184
time_elpased: 5.063
batch start
#iterations: 303
currently lose_sum: 373.44232630729675
time_elpased: 5.106
batch start
#iterations: 304
currently lose_sum: 371.99232107400894
time_elpased: 5.083
batch start
#iterations: 305
currently lose_sum: 373.463655769825
time_elpased: 5.109
batch start
#iterations: 306
currently lose_sum: 370.7680339217186
time_elpased: 5.105
batch start
#iterations: 307
currently lose_sum: 370.72391706705093
time_elpased: 5.076
batch start
#iterations: 308
currently lose_sum: 371.54862785339355
time_elpased: 5.056
batch start
#iterations: 309
currently lose_sum: 372.0133840441704
time_elpased: 5.11
batch start
#iterations: 310
currently lose_sum: 370.04303854703903
time_elpased: 5.128
batch start
#iterations: 311
currently lose_sum: 372.9440113902092
time_elpased: 5.133
batch start
#iterations: 312
currently lose_sum: 371.0547723174095
time_elpased: 5.088
batch start
#iterations: 313
currently lose_sum: 370.803575694561
time_elpased: 5.121
batch start
#iterations: 314
currently lose_sum: 372.29668325185776
time_elpased: 5.116
batch start
#iterations: 315
currently lose_sum: 372.72192269563675
time_elpased: 5.114
batch start
#iterations: 316
currently lose_sum: 371.7502755522728
time_elpased: 5.447
batch start
#iterations: 317
currently lose_sum: 372.18137788772583
time_elpased: 5.531
batch start
#iterations: 318
currently lose_sum: 372.8578378558159
time_elpased: 5.511
batch start
#iterations: 319
currently lose_sum: 371.4236009120941
time_elpased: 5.132
start validation test
0.675979381443
1.0
0.675979381443
0.806667896906
0
validation finish
batch start
#iterations: 320
currently lose_sum: 371.74312222003937
time_elpased: 5.086
batch start
#iterations: 321
currently lose_sum: 372.1395072937012
time_elpased: 5.076
batch start
#iterations: 322
currently lose_sum: 372.9376800060272
time_elpased: 5.095
batch start
#iterations: 323
currently lose_sum: 373.0426696538925
time_elpased: 5.124
batch start
#iterations: 324
currently lose_sum: 371.1894598007202
time_elpased: 5.104
batch start
#iterations: 325
currently lose_sum: 372.4392814040184
time_elpased: 5.121
batch start
#iterations: 326
currently lose_sum: 372.7652530670166
time_elpased: 5.126
batch start
#iterations: 327
currently lose_sum: 371.6153978705406
time_elpased: 5.149
batch start
#iterations: 328
currently lose_sum: 371.88669365644455
time_elpased: 5.129
batch start
#iterations: 329
currently lose_sum: 371.1233341097832
time_elpased: 5.122
batch start
#iterations: 330
currently lose_sum: 372.2000544667244
time_elpased: 5.1
batch start
#iterations: 331
currently lose_sum: 371.80798441171646
time_elpased: 5.094
batch start
#iterations: 332
currently lose_sum: 372.31010192632675
time_elpased: 5.15
batch start
#iterations: 333
currently lose_sum: 372.68460685014725
time_elpased: 5.157
batch start
#iterations: 334
currently lose_sum: 372.16291773319244
time_elpased: 5.152
batch start
#iterations: 335
currently lose_sum: 371.36258840560913
time_elpased: 5.124
batch start
#iterations: 336
currently lose_sum: 373.1543279886246
time_elpased: 5.11
batch start
#iterations: 337
currently lose_sum: 370.88611167669296
time_elpased: 5.094
batch start
#iterations: 338
currently lose_sum: 372.4208315014839
time_elpased: 5.076
batch start
#iterations: 339
currently lose_sum: 372.11380791664124
time_elpased: 5.065
start validation test
0.644742268041
1.0
0.644742268041
0.784004011533
0
validation finish
batch start
#iterations: 340
currently lose_sum: 372.51177179813385
time_elpased: 5.114
batch start
#iterations: 341
currently lose_sum: 372.90478134155273
time_elpased: 5.09
batch start
#iterations: 342
currently lose_sum: 372.0043638944626
time_elpased: 5.09
batch start
#iterations: 343
currently lose_sum: 371.54912519454956
time_elpased: 5.092
batch start
#iterations: 344
currently lose_sum: 371.54639732837677
time_elpased: 5.119
batch start
#iterations: 345
currently lose_sum: 371.9240120649338
time_elpased: 5.093
batch start
#iterations: 346
currently lose_sum: 372.1988397836685
time_elpased: 5.081
batch start
#iterations: 347
currently lose_sum: 373.01490193605423
time_elpased: 5.084
batch start
#iterations: 348
currently lose_sum: 370.33059054613113
time_elpased: 5.093
batch start
#iterations: 349
currently lose_sum: 372.0977590084076
time_elpased: 5.077
batch start
#iterations: 350
currently lose_sum: 372.2692759037018
time_elpased: 5.077
batch start
#iterations: 351
currently lose_sum: 372.34869384765625
time_elpased: 5.094
batch start
#iterations: 352
currently lose_sum: 372.8349158167839
time_elpased: 5.079
batch start
#iterations: 353
currently lose_sum: 371.9520914554596
time_elpased: 5.138
batch start
#iterations: 354
currently lose_sum: 371.5002362728119
time_elpased: 5.385
batch start
#iterations: 355
currently lose_sum: 371.1947647333145
time_elpased: 5.358
batch start
#iterations: 356
currently lose_sum: 371.0592166185379
time_elpased: 5.257
batch start
#iterations: 357
currently lose_sum: 372.1389454603195
time_elpased: 5.085
batch start
#iterations: 358
currently lose_sum: 371.5507832169533
time_elpased: 5.075
batch start
#iterations: 359
currently lose_sum: 370.26470482349396
time_elpased: 5.072
start validation test
0.606907216495
1.0
0.606907216495
0.7553730673
0
validation finish
batch start
#iterations: 360
currently lose_sum: 372.3312693834305
time_elpased: 5.083
batch start
#iterations: 361
currently lose_sum: 372.3229831457138
time_elpased: 5.081
batch start
#iterations: 362
currently lose_sum: 371.9215625524521
time_elpased: 5.076
batch start
#iterations: 363
currently lose_sum: 373.5563277602196
time_elpased: 5.063
batch start
#iterations: 364
currently lose_sum: 370.9656710624695
time_elpased: 5.066
batch start
#iterations: 365
currently lose_sum: 371.7853834629059
time_elpased: 5.071
batch start
#iterations: 366
currently lose_sum: 372.87092530727386
time_elpased: 5.1
batch start
#iterations: 367
currently lose_sum: 372.96834486722946
time_elpased: 5.095
batch start
#iterations: 368
currently lose_sum: 371.4557309150696
time_elpased: 5.072
batch start
#iterations: 369
currently lose_sum: 370.4513917565346
time_elpased: 5.098
batch start
#iterations: 370
currently lose_sum: 371.1289572715759
time_elpased: 5.086
batch start
#iterations: 371
currently lose_sum: 372.0302063822746
time_elpased: 5.086
batch start
#iterations: 372
currently lose_sum: 371.56602531671524
time_elpased: 5.102
batch start
#iterations: 373
currently lose_sum: 372.6248641014099
time_elpased: 5.093
batch start
#iterations: 374
currently lose_sum: 372.2486374974251
time_elpased: 5.068
batch start
#iterations: 375
currently lose_sum: 370.2854917049408
time_elpased: 5.295
batch start
#iterations: 376
currently lose_sum: 371.67617720365524
time_elpased: 5.446
batch start
#iterations: 377
currently lose_sum: 372.1994337439537
time_elpased: 5.01
batch start
#iterations: 378
currently lose_sum: 371.63887226581573
time_elpased: 4.94
batch start
#iterations: 379
currently lose_sum: 372.23722714185715
time_elpased: 4.958
start validation test
0.643505154639
1.0
0.643505154639
0.783088696525
0
validation finish
batch start
#iterations: 380
currently lose_sum: 373.48544776439667
time_elpased: 4.924
batch start
#iterations: 381
currently lose_sum: 373.12681502103806
time_elpased: 4.941
batch start
#iterations: 382
currently lose_sum: 372.520146548748
time_elpased: 4.917
batch start
#iterations: 383
currently lose_sum: 371.8268173933029
time_elpased: 4.965
batch start
#iterations: 384
currently lose_sum: 371.5827425122261
time_elpased: 4.938
batch start
#iterations: 385
currently lose_sum: 371.4845519065857
time_elpased: 4.979
batch start
#iterations: 386
currently lose_sum: 372.5936271548271
time_elpased: 5.061
batch start
#iterations: 387
currently lose_sum: 371.3497126698494
time_elpased: 5.181
batch start
#iterations: 388
currently lose_sum: 371.03501933813095
time_elpased: 5.141
batch start
#iterations: 389
currently lose_sum: 372.04532635211945
time_elpased: 5.132
batch start
#iterations: 390
currently lose_sum: 370.65605586767197
time_elpased: 5.15
batch start
#iterations: 391
currently lose_sum: 371.28786754608154
time_elpased: 5.125
batch start
#iterations: 392
currently lose_sum: 370.80254632234573
time_elpased: 5.078
batch start
#iterations: 393
currently lose_sum: 372.4846296906471
time_elpased: 5.039
batch start
#iterations: 394
currently lose_sum: 372.0404989719391
time_elpased: 5.079
batch start
#iterations: 395
currently lose_sum: 372.20462316274643
time_elpased: 5.079
batch start
#iterations: 396
currently lose_sum: 372.1712026000023
time_elpased: 5.089
batch start
#iterations: 397
currently lose_sum: 371.08103704452515
time_elpased: 5.064
batch start
#iterations: 398
currently lose_sum: 371.94824331998825
time_elpased: 5.08
batch start
#iterations: 399
currently lose_sum: 370.47229409217834
time_elpased: 5.091
start validation test
0.663917525773
1.0
0.663917525773
0.798017348203
0
validation finish
acc: 0.703
pre: 1.000
rec: 0.703
F1: 0.826
auc: 0.000
