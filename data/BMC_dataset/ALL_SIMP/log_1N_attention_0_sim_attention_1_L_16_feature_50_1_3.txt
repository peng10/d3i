start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 395.16324388980865
time_elpased: 5.131
batch start
#iterations: 1
currently lose_sum: 386.0331838130951
time_elpased: 5.068
batch start
#iterations: 2
currently lose_sum: 383.3689103126526
time_elpased: 5.113
batch start
#iterations: 3
currently lose_sum: 379.2752174735069
time_elpased: 5.103
batch start
#iterations: 4
currently lose_sum: 379.5834478735924
time_elpased: 5.098
batch start
#iterations: 5
currently lose_sum: 377.9884093403816
time_elpased: 5.053
batch start
#iterations: 6
currently lose_sum: 378.0465279817581
time_elpased: 5.087
batch start
#iterations: 7
currently lose_sum: 377.6169282793999
time_elpased: 5.104
batch start
#iterations: 8
currently lose_sum: 377.18307971954346
time_elpased: 5.087
batch start
#iterations: 9
currently lose_sum: 376.31370824575424
time_elpased: 5.081
batch start
#iterations: 10
currently lose_sum: 376.5455114841461
time_elpased: 5.102
batch start
#iterations: 11
currently lose_sum: 374.9773806333542
time_elpased: 5.06
batch start
#iterations: 12
currently lose_sum: 375.00405514240265
time_elpased: 5.0
batch start
#iterations: 13
currently lose_sum: 374.6144605278969
time_elpased: 5.029
batch start
#iterations: 14
currently lose_sum: 374.45948725938797
time_elpased: 5.045
batch start
#iterations: 15
currently lose_sum: 373.66374856233597
time_elpased: 5.022
batch start
#iterations: 16
currently lose_sum: 375.8672653436661
time_elpased: 5.015
batch start
#iterations: 17
currently lose_sum: 375.05715423822403
time_elpased: 4.998
batch start
#iterations: 18
currently lose_sum: 374.3857082128525
time_elpased: 5.058
batch start
#iterations: 19
currently lose_sum: 373.73375445604324
time_elpased: 5.068
start validation test
0.731340206186
1.0
0.731340206186
0.844825532928
0
validation finish
batch start
#iterations: 20
currently lose_sum: 372.8276246190071
time_elpased: 5.077
batch start
#iterations: 21
currently lose_sum: 373.80993497371674
time_elpased: 5.111
batch start
#iterations: 22
currently lose_sum: 374.87967789173126
time_elpased: 5.116
batch start
#iterations: 23
currently lose_sum: 373.9689921736717
time_elpased: 5.086
batch start
#iterations: 24
currently lose_sum: 374.43309795856476
time_elpased: 5.103
batch start
#iterations: 25
currently lose_sum: 373.92804819345474
time_elpased: 5.128
batch start
#iterations: 26
currently lose_sum: 374.43337988853455
time_elpased: 5.095
batch start
#iterations: 27
currently lose_sum: 375.6581964492798
time_elpased: 5.126
batch start
#iterations: 28
currently lose_sum: 372.6971564888954
time_elpased: 5.117
batch start
#iterations: 29
currently lose_sum: 375.01570481061935
time_elpased: 5.129
batch start
#iterations: 30
currently lose_sum: 373.74789226055145
time_elpased: 5.096
batch start
#iterations: 31
currently lose_sum: 373.79499942064285
time_elpased: 5.106
batch start
#iterations: 32
currently lose_sum: 372.87525510787964
time_elpased: 5.116
batch start
#iterations: 33
currently lose_sum: 373.6489296555519
time_elpased: 5.085
batch start
#iterations: 34
currently lose_sum: 373.87944209575653
time_elpased: 5.09
batch start
#iterations: 35
currently lose_sum: 373.3173524737358
time_elpased: 5.123
batch start
#iterations: 36
currently lose_sum: 373.3111292719841
time_elpased: 5.031
batch start
#iterations: 37
currently lose_sum: 372.40986371040344
time_elpased: 5.062
batch start
#iterations: 38
currently lose_sum: 372.7356441617012
time_elpased: 5.059
batch start
#iterations: 39
currently lose_sum: 373.7241976261139
time_elpased: 5.062
start validation test
0.671340206186
1.0
0.671340206186
0.803355539107
0
validation finish
batch start
#iterations: 40
currently lose_sum: 372.4019213318825
time_elpased: 5.045
batch start
#iterations: 41
currently lose_sum: 372.9657407402992
time_elpased: 5.059
batch start
#iterations: 42
currently lose_sum: 372.77647107839584
time_elpased: 5.075
batch start
#iterations: 43
currently lose_sum: 373.2173984646797
time_elpased: 5.086
batch start
#iterations: 44
currently lose_sum: 371.2031950354576
time_elpased: 5.05
batch start
#iterations: 45
currently lose_sum: 372.05525916814804
time_elpased: 5.015
batch start
#iterations: 46
currently lose_sum: 374.52846252918243
time_elpased: 5.058
batch start
#iterations: 47
currently lose_sum: 372.9845598936081
time_elpased: 5.052
batch start
#iterations: 48
currently lose_sum: 373.2027699351311
time_elpased: 5.001
batch start
#iterations: 49
currently lose_sum: 372.2113005518913
time_elpased: 4.998
batch start
#iterations: 50
currently lose_sum: 375.7005769610405
time_elpased: 5.007
batch start
#iterations: 51
currently lose_sum: 373.3007638454437
time_elpased: 5.029
batch start
#iterations: 52
currently lose_sum: 372.76902931928635
time_elpased: 5.036
batch start
#iterations: 53
currently lose_sum: 374.3055413365364
time_elpased: 5.034
batch start
#iterations: 54
currently lose_sum: 371.6580696105957
time_elpased: 5.037
batch start
#iterations: 55
currently lose_sum: 372.11891293525696
time_elpased: 5.029
batch start
#iterations: 56
currently lose_sum: 373.3270555138588
time_elpased: 5.002
batch start
#iterations: 57
currently lose_sum: 372.8045715689659
time_elpased: 5.007
batch start
#iterations: 58
currently lose_sum: 373.36015313863754
time_elpased: 5.005
batch start
#iterations: 59
currently lose_sum: 374.3723295331001
time_elpased: 5.003
start validation test
0.622680412371
1.0
0.622680412371
0.767471410419
0
validation finish
batch start
#iterations: 60
currently lose_sum: 372.4764319062233
time_elpased: 5.009
batch start
#iterations: 61
currently lose_sum: 372.5192353129387
time_elpased: 4.998
batch start
#iterations: 62
currently lose_sum: 372.82130193710327
time_elpased: 5.03
batch start
#iterations: 63
currently lose_sum: 372.7790093421936
time_elpased: 4.986
batch start
#iterations: 64
currently lose_sum: 373.34838992357254
time_elpased: 5.016
batch start
#iterations: 65
currently lose_sum: 372.2470971941948
time_elpased: 4.988
batch start
#iterations: 66
currently lose_sum: 373.71323442459106
time_elpased: 5.014
batch start
#iterations: 67
currently lose_sum: 373.5369444489479
time_elpased: 5.01
batch start
#iterations: 68
currently lose_sum: 372.1713162660599
time_elpased: 5.008
batch start
#iterations: 69
currently lose_sum: 372.6962033510208
time_elpased: 5.008
batch start
#iterations: 70
currently lose_sum: 373.8037140965462
time_elpased: 5.037
batch start
#iterations: 71
currently lose_sum: 372.5867493748665
time_elpased: 5.024
batch start
#iterations: 72
currently lose_sum: 372.22675597667694
time_elpased: 5.006
batch start
#iterations: 73
currently lose_sum: 372.6170685887337
time_elpased: 5.02
batch start
#iterations: 74
currently lose_sum: 372.9111878871918
time_elpased: 5.014
batch start
#iterations: 75
currently lose_sum: 371.3754358291626
time_elpased: 5.007
batch start
#iterations: 76
currently lose_sum: 373.6096631884575
time_elpased: 4.988
batch start
#iterations: 77
currently lose_sum: 371.94684356451035
time_elpased: 4.994
batch start
#iterations: 78
currently lose_sum: 371.69933903217316
time_elpased: 4.997
batch start
#iterations: 79
currently lose_sum: 371.7221433520317
time_elpased: 5.033
start validation test
0.654536082474
1.0
0.654536082474
0.791201944046
0
validation finish
batch start
#iterations: 80
currently lose_sum: 372.006265938282
time_elpased: 5.016
batch start
#iterations: 81
currently lose_sum: 373.87298065423965
time_elpased: 5.031
batch start
#iterations: 82
currently lose_sum: 373.4883518218994
time_elpased: 5.032
batch start
#iterations: 83
currently lose_sum: 373.77610087394714
time_elpased: 5.013
batch start
#iterations: 84
currently lose_sum: 372.94304406642914
time_elpased: 5.064
batch start
#iterations: 85
currently lose_sum: 372.4926474094391
time_elpased: 5.099
batch start
#iterations: 86
currently lose_sum: 373.6442614197731
time_elpased: 5.105
batch start
#iterations: 87
currently lose_sum: 372.9266381263733
time_elpased: 5.09
batch start
#iterations: 88
currently lose_sum: 371.39260959625244
time_elpased: 5.102
batch start
#iterations: 89
currently lose_sum: 372.5505881309509
time_elpased: 5.152
batch start
#iterations: 90
currently lose_sum: 373.06963288784027
time_elpased: 5.113
batch start
#iterations: 91
currently lose_sum: 372.1126763224602
time_elpased: 5.126
batch start
#iterations: 92
currently lose_sum: 372.5436038374901
time_elpased: 5.13
batch start
#iterations: 93
currently lose_sum: 374.5531299710274
time_elpased: 5.143
batch start
#iterations: 94
currently lose_sum: 373.4728636741638
time_elpased: 5.106
batch start
#iterations: 95
currently lose_sum: 371.7712378501892
time_elpased: 5.114
batch start
#iterations: 96
currently lose_sum: 372.83093839883804
time_elpased: 5.146
batch start
#iterations: 97
currently lose_sum: 372.945980489254
time_elpased: 5.082
batch start
#iterations: 98
currently lose_sum: 372.2561041712761
time_elpased: 5.064
batch start
#iterations: 99
currently lose_sum: 373.71086382865906
time_elpased: 5.098
start validation test
0.611855670103
1.0
0.611855670103
0.759194115766
0
validation finish
batch start
#iterations: 100
currently lose_sum: 372.7490247488022
time_elpased: 5.073
batch start
#iterations: 101
currently lose_sum: 374.57534193992615
time_elpased: 5.072
batch start
#iterations: 102
currently lose_sum: 371.44660806655884
time_elpased: 5.097
batch start
#iterations: 103
currently lose_sum: 373.06319695711136
time_elpased: 5.086
batch start
#iterations: 104
currently lose_sum: 373.3523610830307
time_elpased: 5.066
batch start
#iterations: 105
currently lose_sum: 371.9380315542221
time_elpased: 5.078
batch start
#iterations: 106
currently lose_sum: 373.7613014578819
time_elpased: 5.072
batch start
#iterations: 107
currently lose_sum: 371.5414738059044
time_elpased: 5.065
batch start
#iterations: 108
currently lose_sum: 372.1638776063919
time_elpased: 5.039
batch start
#iterations: 109
currently lose_sum: 372.71399772167206
time_elpased: 5.084
batch start
#iterations: 110
currently lose_sum: 373.09265303611755
time_elpased: 5.076
batch start
#iterations: 111
currently lose_sum: 372.01193857192993
time_elpased: 5.011
batch start
#iterations: 112
currently lose_sum: 371.711314201355
time_elpased: 5.005
batch start
#iterations: 113
currently lose_sum: 372.5792442560196
time_elpased: 5.035
batch start
#iterations: 114
currently lose_sum: 373.0086921453476
time_elpased: 5.023
batch start
#iterations: 115
currently lose_sum: 371.4120858311653
time_elpased: 4.981
batch start
#iterations: 116
currently lose_sum: 372.6093670129776
time_elpased: 4.986
batch start
#iterations: 117
currently lose_sum: 372.4495978951454
time_elpased: 5.069
batch start
#iterations: 118
currently lose_sum: 372.2773749232292
time_elpased: 5.118
batch start
#iterations: 119
currently lose_sum: 372.45474153757095
time_elpased: 5.073
start validation test
0.615463917526
1.0
0.615463917526
0.761965539247
0
validation finish
batch start
#iterations: 120
currently lose_sum: 371.63987827301025
time_elpased: 5.083
batch start
#iterations: 121
currently lose_sum: 371.7796649336815
time_elpased: 5.127
batch start
#iterations: 122
currently lose_sum: 373.71160089969635
time_elpased: 5.152
batch start
#iterations: 123
currently lose_sum: 370.7964944243431
time_elpased: 5.107
batch start
#iterations: 124
currently lose_sum: 371.48500406742096
time_elpased: 5.082
batch start
#iterations: 125
currently lose_sum: 372.1347544193268
time_elpased: 5.13
batch start
#iterations: 126
currently lose_sum: 372.9329155087471
time_elpased: 5.094
batch start
#iterations: 127
currently lose_sum: 372.61686623096466
time_elpased: 5.124
batch start
#iterations: 128
currently lose_sum: 373.8343980908394
time_elpased: 5.17
batch start
#iterations: 129
currently lose_sum: 372.29812890291214
time_elpased: 5.189
batch start
#iterations: 130
currently lose_sum: 371.55249708890915
time_elpased: 5.187
batch start
#iterations: 131
currently lose_sum: 372.9343167543411
time_elpased: 5.202
batch start
#iterations: 132
currently lose_sum: 372.0740064382553
time_elpased: 5.132
batch start
#iterations: 133
currently lose_sum: 372.9592863917351
time_elpased: 5.071
batch start
#iterations: 134
currently lose_sum: 371.32465851306915
time_elpased: 5.128
batch start
#iterations: 135
currently lose_sum: 373.1779559850693
time_elpased: 5.138
batch start
#iterations: 136
currently lose_sum: 372.4873190522194
time_elpased: 5.136
batch start
#iterations: 137
currently lose_sum: 371.22001987695694
time_elpased: 5.108
batch start
#iterations: 138
currently lose_sum: 373.37296885252
time_elpased: 5.103
batch start
#iterations: 139
currently lose_sum: 372.00691479444504
time_elpased: 5.137
start validation test
0.66
1.0
0.66
0.795180722892
0
validation finish
batch start
#iterations: 140
currently lose_sum: 370.4504455924034
time_elpased: 5.056
batch start
#iterations: 141
currently lose_sum: 372.7775334715843
time_elpased: 5.034
batch start
#iterations: 142
currently lose_sum: 370.43266797065735
time_elpased: 5.036
batch start
#iterations: 143
currently lose_sum: 370.6935616135597
time_elpased: 5.049
batch start
#iterations: 144
currently lose_sum: 371.29817694425583
time_elpased: 5.066
batch start
#iterations: 145
currently lose_sum: 372.1928015947342
time_elpased: 5.055
batch start
#iterations: 146
currently lose_sum: 369.7127496600151
time_elpased: 5.06
batch start
#iterations: 147
currently lose_sum: 373.01918375492096
time_elpased: 5.062
batch start
#iterations: 148
currently lose_sum: 372.47326546907425
time_elpased: 5.093
batch start
#iterations: 149
currently lose_sum: 371.79985374212265
time_elpased: 5.071
batch start
#iterations: 150
currently lose_sum: 372.1259762644768
time_elpased: 5.101
batch start
#iterations: 151
currently lose_sum: 372.0805394053459
time_elpased: 5.108
batch start
#iterations: 152
currently lose_sum: 370.6921121478081
time_elpased: 5.1
batch start
#iterations: 153
currently lose_sum: 372.0583774447441
time_elpased: 5.063
batch start
#iterations: 154
currently lose_sum: 372.733769595623
time_elpased: 5.078
batch start
#iterations: 155
currently lose_sum: 372.10107296705246
time_elpased: 5.054
batch start
#iterations: 156
currently lose_sum: 373.0434985756874
time_elpased: 5.069
batch start
#iterations: 157
currently lose_sum: 373.06161814928055
time_elpased: 5.08
batch start
#iterations: 158
currently lose_sum: 372.0724593400955
time_elpased: 5.121
batch start
#iterations: 159
currently lose_sum: 371.29524087905884
time_elpased: 5.104
start validation test
0.69793814433
1.0
0.69793814433
0.822100789314
0
validation finish
batch start
#iterations: 160
currently lose_sum: 371.67322540283203
time_elpased: 5.115
batch start
#iterations: 161
currently lose_sum: 372.5777460336685
time_elpased: 5.133
batch start
#iterations: 162
currently lose_sum: 372.9971905350685
time_elpased: 5.062
batch start
#iterations: 163
currently lose_sum: 372.2721474766731
time_elpased: 5.03
batch start
#iterations: 164
currently lose_sum: 372.8519207239151
time_elpased: 5.061
batch start
#iterations: 165
currently lose_sum: 372.6681698560715
time_elpased: 5.055
batch start
#iterations: 166
currently lose_sum: 372.1125993132591
time_elpased: 5.094
batch start
#iterations: 167
currently lose_sum: 372.4898647069931
time_elpased: 5.065
batch start
#iterations: 168
currently lose_sum: 373.1201424598694
time_elpased: 5.077
batch start
#iterations: 169
currently lose_sum: 371.4912223815918
time_elpased: 5.089
batch start
#iterations: 170
currently lose_sum: 371.82061421871185
time_elpased: 5.048
batch start
#iterations: 171
currently lose_sum: 371.101897418499
time_elpased: 5.048
batch start
#iterations: 172
currently lose_sum: 372.28052258491516
time_elpased: 5.079
batch start
#iterations: 173
currently lose_sum: 372.20160537958145
time_elpased: 5.083
batch start
#iterations: 174
currently lose_sum: 371.8321843743324
time_elpased: 5.065
batch start
#iterations: 175
currently lose_sum: 372.45726293325424
time_elpased: 5.078
batch start
#iterations: 176
currently lose_sum: 371.68775647878647
time_elpased: 5.044
batch start
#iterations: 177
currently lose_sum: 372.9780341386795
time_elpased: 5.012
batch start
#iterations: 178
currently lose_sum: 370.84118086099625
time_elpased: 5.036
batch start
#iterations: 179
currently lose_sum: 371.58093774318695
time_elpased: 5.066
start validation test
0.60793814433
1.0
0.60793814433
0.756171058537
0
validation finish
batch start
#iterations: 180
currently lose_sum: 371.39034724235535
time_elpased: 5.045
batch start
#iterations: 181
currently lose_sum: 372.3848196864128
time_elpased: 5.036
batch start
#iterations: 182
currently lose_sum: 373.6910911202431
time_elpased: 5.066
batch start
#iterations: 183
currently lose_sum: 373.1025276184082
time_elpased: 5.04
batch start
#iterations: 184
currently lose_sum: 372.65619426965714
time_elpased: 5.051
batch start
#iterations: 185
currently lose_sum: 370.2153097987175
time_elpased: 5.042
batch start
#iterations: 186
currently lose_sum: 372.8326828479767
time_elpased: 5.096
batch start
#iterations: 187
currently lose_sum: 371.8395651578903
time_elpased: 5.043
batch start
#iterations: 188
currently lose_sum: 371.6930419206619
time_elpased: 5.051
batch start
#iterations: 189
currently lose_sum: 372.7905297279358
time_elpased: 5.067
batch start
#iterations: 190
currently lose_sum: 371.74850779771805
time_elpased: 5.081
batch start
#iterations: 191
currently lose_sum: 372.92286962270737
time_elpased: 5.066
batch start
#iterations: 192
currently lose_sum: 371.840131521225
time_elpased: 5.046
batch start
#iterations: 193
currently lose_sum: 372.39586132764816
time_elpased: 5.064
batch start
#iterations: 194
currently lose_sum: 371.19910621643066
time_elpased: 5.066
batch start
#iterations: 195
currently lose_sum: 371.3545937538147
time_elpased: 5.05
batch start
#iterations: 196
currently lose_sum: 373.0491886138916
time_elpased: 5.048
batch start
#iterations: 197
currently lose_sum: 373.56110763549805
time_elpased: 5.061
batch start
#iterations: 198
currently lose_sum: 372.91854178905487
time_elpased: 5.07
batch start
#iterations: 199
currently lose_sum: 372.43616580963135
time_elpased: 5.041
start validation test
0.667731958763
1.0
0.667731958763
0.800766520368
0
validation finish
batch start
#iterations: 200
currently lose_sum: 371.9463218450546
time_elpased: 5.055
batch start
#iterations: 201
currently lose_sum: 372.67613911628723
time_elpased: 5.066
batch start
#iterations: 202
currently lose_sum: 372.1029379963875
time_elpased: 5.067
batch start
#iterations: 203
currently lose_sum: 371.6648411154747
time_elpased: 5.056
batch start
#iterations: 204
currently lose_sum: 372.0408833026886
time_elpased: 5.073
batch start
#iterations: 205
currently lose_sum: 372.1664572954178
time_elpased: 5.1
batch start
#iterations: 206
currently lose_sum: 372.26708948612213
time_elpased: 5.079
batch start
#iterations: 207
currently lose_sum: 371.07227605581284
time_elpased: 5.07
batch start
#iterations: 208
currently lose_sum: 373.04230976104736
time_elpased: 5.051
batch start
#iterations: 209
currently lose_sum: 370.3391201496124
time_elpased: 5.038
batch start
#iterations: 210
currently lose_sum: 372.8276448249817
time_elpased: 5.038
batch start
#iterations: 211
currently lose_sum: 371.30960315465927
time_elpased: 5.053
batch start
#iterations: 212
currently lose_sum: 371.27552539110184
time_elpased: 5.056
batch start
#iterations: 213
currently lose_sum: 371.775756418705
time_elpased: 5.041
batch start
#iterations: 214
currently lose_sum: 372.65284472703934
time_elpased: 5.041
batch start
#iterations: 215
currently lose_sum: 371.33605164289474
time_elpased: 5.115
batch start
#iterations: 216
currently lose_sum: 372.1620771288872
time_elpased: 5.109
batch start
#iterations: 217
currently lose_sum: 371.4758539199829
time_elpased: 5.084
batch start
#iterations: 218
currently lose_sum: 370.4321154356003
time_elpased: 5.109
batch start
#iterations: 219
currently lose_sum: 373.12920194864273
time_elpased: 5.066
start validation test
0.650206185567
1.0
0.650206185567
0.788030236771
0
validation finish
batch start
#iterations: 220
currently lose_sum: 371.512138068676
time_elpased: 5.069
batch start
#iterations: 221
currently lose_sum: 371.7542981505394
time_elpased: 5.055
batch start
#iterations: 222
currently lose_sum: 371.8978692293167
time_elpased: 5.05
batch start
#iterations: 223
currently lose_sum: 371.9826632142067
time_elpased: 5.051
batch start
#iterations: 224
currently lose_sum: 370.67206436395645
time_elpased: 5.072
batch start
#iterations: 225
currently lose_sum: 370.6000984311104
time_elpased: 5.057
batch start
#iterations: 226
currently lose_sum: 371.7886507511139
time_elpased: 5.104
batch start
#iterations: 227
currently lose_sum: 370.972796022892
time_elpased: 5.154
batch start
#iterations: 228
currently lose_sum: 371.7886611819267
time_elpased: 5.117
batch start
#iterations: 229
currently lose_sum: 371.3402228951454
time_elpased: 5.202
batch start
#iterations: 230
currently lose_sum: 372.883948802948
time_elpased: 5.231
batch start
#iterations: 231
currently lose_sum: 371.5570575594902
time_elpased: 5.097
batch start
#iterations: 232
currently lose_sum: 372.9873804450035
time_elpased: 5.06
batch start
#iterations: 233
currently lose_sum: 370.39188581705093
time_elpased: 5.04
batch start
#iterations: 234
currently lose_sum: 371.50270080566406
time_elpased: 5.038
batch start
#iterations: 235
currently lose_sum: 370.9403512477875
time_elpased: 5.023
batch start
#iterations: 236
currently lose_sum: 372.7842447757721
time_elpased: 5.021
batch start
#iterations: 237
currently lose_sum: 372.3161380290985
time_elpased: 5.069
batch start
#iterations: 238
currently lose_sum: 371.29262870550156
time_elpased: 5.035
batch start
#iterations: 239
currently lose_sum: 370.736559510231
time_elpased: 5.048
start validation test
0.631855670103
1.0
0.631855670103
0.774401415124
0
validation finish
batch start
#iterations: 240
currently lose_sum: 371.69735860824585
time_elpased: 5.041
batch start
#iterations: 241
currently lose_sum: 371.1954687833786
time_elpased: 5.05
batch start
#iterations: 242
currently lose_sum: 370.60554552078247
time_elpased: 5.044
batch start
#iterations: 243
currently lose_sum: 373.4506581425667
time_elpased: 5.057
batch start
#iterations: 244
currently lose_sum: 372.81838685274124
time_elpased: 5.03
batch start
#iterations: 245
currently lose_sum: 372.06256479024887
time_elpased: 5.027
batch start
#iterations: 246
currently lose_sum: 371.02788269519806
time_elpased: 5.047
batch start
#iterations: 247
currently lose_sum: 372.40661293268204
time_elpased: 5.05
batch start
#iterations: 248
currently lose_sum: 372.56757378578186
time_elpased: 5.074
batch start
#iterations: 249
currently lose_sum: 373.0082970261574
time_elpased: 5.052
batch start
#iterations: 250
currently lose_sum: 372.5782504081726
time_elpased: 5.055
batch start
#iterations: 251
currently lose_sum: 372.45710772275925
time_elpased: 5.04
batch start
#iterations: 252
currently lose_sum: 371.5808206796646
time_elpased: 5.028
batch start
#iterations: 253
currently lose_sum: 371.6750460267067
time_elpased: 5.059
batch start
#iterations: 254
currently lose_sum: 370.9334437251091
time_elpased: 5.066
batch start
#iterations: 255
currently lose_sum: 372.4916107058525
time_elpased: 5.07
batch start
#iterations: 256
currently lose_sum: 372.43364107608795
time_elpased: 5.03
batch start
#iterations: 257
currently lose_sum: 371.8576055765152
time_elpased: 5.06
batch start
#iterations: 258
currently lose_sum: 372.63442373275757
time_elpased: 5.043
batch start
#iterations: 259
currently lose_sum: 372.81763541698456
time_elpased: 5.054
start validation test
0.609484536082
1.0
0.609484536082
0.757366128619
0
validation finish
batch start
#iterations: 260
currently lose_sum: 371.45723909139633
time_elpased: 5.046
batch start
#iterations: 261
currently lose_sum: 372.6625389456749
time_elpased: 5.03
batch start
#iterations: 262
currently lose_sum: 371.48053443431854
time_elpased: 5.036
batch start
#iterations: 263
currently lose_sum: 372.0265936255455
time_elpased: 5.068
batch start
#iterations: 264
currently lose_sum: 371.32228004932404
time_elpased: 5.065
batch start
#iterations: 265
currently lose_sum: 371.7497795820236
time_elpased: 5.053
batch start
#iterations: 266
currently lose_sum: 371.8957704305649
time_elpased: 5.058
batch start
#iterations: 267
currently lose_sum: 371.01462960243225
time_elpased: 5.04
batch start
#iterations: 268
currently lose_sum: 370.0087792277336
time_elpased: 5.035
batch start
#iterations: 269
currently lose_sum: 372.2282258272171
time_elpased: 5.011
batch start
#iterations: 270
currently lose_sum: 370.5619977116585
time_elpased: 5.075
batch start
#iterations: 271
currently lose_sum: 373.60888278484344
time_elpased: 5.065
batch start
#iterations: 272
currently lose_sum: 370.8037188053131
time_elpased: 5.059
batch start
#iterations: 273
currently lose_sum: 371.7527762055397
time_elpased: 5.061
batch start
#iterations: 274
currently lose_sum: 371.6146128773689
time_elpased: 5.061
batch start
#iterations: 275
currently lose_sum: 372.60055071115494
time_elpased: 5.055
batch start
#iterations: 276
currently lose_sum: 371.31016355752945
time_elpased: 5.046
batch start
#iterations: 277
currently lose_sum: 371.26795399188995
time_elpased: 5.048
batch start
#iterations: 278
currently lose_sum: 372.04436856508255
time_elpased: 5.038
batch start
#iterations: 279
currently lose_sum: 370.98741096258163
time_elpased: 5.062
start validation test
0.649793814433
1.0
0.649793814433
0.787727301131
0
validation finish
batch start
#iterations: 280
currently lose_sum: 371.43396240472794
time_elpased: 5.09
batch start
#iterations: 281
currently lose_sum: 371.2217583656311
time_elpased: 5.095
batch start
#iterations: 282
currently lose_sum: 372.0473989248276
time_elpased: 5.071
batch start
#iterations: 283
currently lose_sum: 370.5414342880249
time_elpased: 5.071
batch start
#iterations: 284
currently lose_sum: 372.93797367811203
time_elpased: 5.049
batch start
#iterations: 285
currently lose_sum: 371.68340438604355
time_elpased: 5.029
batch start
#iterations: 286
currently lose_sum: 372.2297963500023
time_elpased: 5.004
batch start
#iterations: 287
currently lose_sum: 373.43642300367355
time_elpased: 5.023
batch start
#iterations: 288
currently lose_sum: 372.411394238472
time_elpased: 5.018
batch start
#iterations: 289
currently lose_sum: 369.8968616127968
time_elpased: 5.023
batch start
#iterations: 290
currently lose_sum: 370.97760862112045
time_elpased: 5.055
batch start
#iterations: 291
currently lose_sum: 371.8339152932167
time_elpased: 5.38
batch start
#iterations: 292
currently lose_sum: 371.6235027909279
time_elpased: 5.407
batch start
#iterations: 293
currently lose_sum: 371.9383622407913
time_elpased: 5.244
batch start
#iterations: 294
currently lose_sum: 371.04799020290375
time_elpased: 5.054
batch start
#iterations: 295
currently lose_sum: 369.83129543066025
time_elpased: 5.03
batch start
#iterations: 296
currently lose_sum: 371.04545426368713
time_elpased: 5.061
batch start
#iterations: 297
currently lose_sum: 372.33211731910706
time_elpased: 5.005
batch start
#iterations: 298
currently lose_sum: 373.2342015504837
time_elpased: 4.958
batch start
#iterations: 299
currently lose_sum: 371.3760787844658
time_elpased: 4.993
start validation test
0.656288659794
1.0
0.656288659794
0.79248101581
0
validation finish
batch start
#iterations: 300
currently lose_sum: 371.3770053386688
time_elpased: 5.045
batch start
#iterations: 301
currently lose_sum: 372.2306681275368
time_elpased: 5.023
batch start
#iterations: 302
currently lose_sum: 371.43748503923416
time_elpased: 5.021
batch start
#iterations: 303
currently lose_sum: 370.2017090320587
time_elpased: 5.068
batch start
#iterations: 304
currently lose_sum: 372.36143815517426
time_elpased: 5.045
batch start
#iterations: 305
currently lose_sum: 371.96438163518906
time_elpased: 5.016
batch start
#iterations: 306
currently lose_sum: 371.1526062488556
time_elpased: 4.983
batch start
#iterations: 307
currently lose_sum: 372.8833522796631
time_elpased: 5.032
batch start
#iterations: 308
currently lose_sum: 371.54121762514114
time_elpased: 5.034
batch start
#iterations: 309
currently lose_sum: 371.97312450408936
time_elpased: 5.007
batch start
#iterations: 310
currently lose_sum: 372.4041569828987
time_elpased: 5.047
batch start
#iterations: 311
currently lose_sum: 373.0310552120209
time_elpased: 5.01
batch start
#iterations: 312
currently lose_sum: 371.1500422358513
time_elpased: 5.041
batch start
#iterations: 313
currently lose_sum: 372.6614693403244
time_elpased: 4.998
batch start
#iterations: 314
currently lose_sum: 372.01848644018173
time_elpased: 5.023
batch start
#iterations: 315
currently lose_sum: 371.7454071044922
time_elpased: 5.027
batch start
#iterations: 316
currently lose_sum: 371.22814959287643
time_elpased: 5.093
batch start
#iterations: 317
currently lose_sum: 371.61142921447754
time_elpased: 5.334
batch start
#iterations: 318
currently lose_sum: 372.5907133221626
time_elpased: 5.034
batch start
#iterations: 319
currently lose_sum: 369.987411737442
time_elpased: 5.072
start validation test
0.640927835052
1.0
0.640927835052
0.781177357542
0
validation finish
batch start
#iterations: 320
currently lose_sum: 371.3392102718353
time_elpased: 5.044
batch start
#iterations: 321
currently lose_sum: 370.63573455810547
time_elpased: 5.056
batch start
#iterations: 322
currently lose_sum: 370.7767405509949
time_elpased: 5.032
batch start
#iterations: 323
currently lose_sum: 372.2121288180351
time_elpased: 5.038
batch start
#iterations: 324
currently lose_sum: 369.7145472764969
time_elpased: 5.038
batch start
#iterations: 325
currently lose_sum: 371.3000358939171
time_elpased: 5.038
batch start
#iterations: 326
currently lose_sum: 370.8639217019081
time_elpased: 5.054
batch start
#iterations: 327
currently lose_sum: 372.19347524642944
time_elpased: 5.033
batch start
#iterations: 328
currently lose_sum: 373.1578637957573
time_elpased: 5.02
batch start
#iterations: 329
currently lose_sum: 372.65679609775543
time_elpased: 5.02
batch start
#iterations: 330
currently lose_sum: 370.81735706329346
time_elpased: 5.071
batch start
#iterations: 331
currently lose_sum: 373.2416909337044
time_elpased: 5.076
batch start
#iterations: 332
currently lose_sum: 371.83139073848724
time_elpased: 5.052
batch start
#iterations: 333
currently lose_sum: 372.332643032074
time_elpased: 5.055
batch start
#iterations: 334
currently lose_sum: 371.33790135383606
time_elpased: 5.081
batch start
#iterations: 335
currently lose_sum: 371.5969846844673
time_elpased: 5.098
batch start
#iterations: 336
currently lose_sum: 371.4337525963783
time_elpased: 5.041
batch start
#iterations: 337
currently lose_sum: 373.30322021245956
time_elpased: 5.05
batch start
#iterations: 338
currently lose_sum: 370.65001034736633
time_elpased: 5.066
batch start
#iterations: 339
currently lose_sum: 371.30548161268234
time_elpased: 5.047
start validation test
0.654845360825
1.0
0.654845360825
0.791427859457
0
validation finish
batch start
#iterations: 340
currently lose_sum: 371.5224608182907
time_elpased: 5.071
batch start
#iterations: 341
currently lose_sum: 370.8004783987999
time_elpased: 5.078
batch start
#iterations: 342
currently lose_sum: 372.3958665728569
time_elpased: 5.024
batch start
#iterations: 343
currently lose_sum: 371.35768860578537
time_elpased: 5.047
batch start
#iterations: 344
currently lose_sum: 371.4531396627426
time_elpased: 5.059
batch start
#iterations: 345
currently lose_sum: 372.4924393892288
time_elpased: 5.048
batch start
#iterations: 346
currently lose_sum: 371.8885473012924
time_elpased: 5.033
batch start
#iterations: 347
currently lose_sum: 371.9210480451584
time_elpased: 5.057
batch start
#iterations: 348
currently lose_sum: 371.94146460294724
time_elpased: 5.054
batch start
#iterations: 349
currently lose_sum: 372.2596139907837
time_elpased: 5.055
batch start
#iterations: 350
currently lose_sum: 371.6107803583145
time_elpased: 5.026
batch start
#iterations: 351
currently lose_sum: 371.54912704229355
time_elpased: 5.042
batch start
#iterations: 352
currently lose_sum: 371.1305316090584
time_elpased: 5.073
batch start
#iterations: 353
currently lose_sum: 370.91953068971634
time_elpased: 5.403
batch start
#iterations: 354
currently lose_sum: 370.7624363899231
time_elpased: 5.524
batch start
#iterations: 355
currently lose_sum: 369.9787979722023
time_elpased: 5.539
batch start
#iterations: 356
currently lose_sum: 371.72445064783096
time_elpased: 5.292
batch start
#iterations: 357
currently lose_sum: 370.77398014068604
time_elpased: 5.1
batch start
#iterations: 358
currently lose_sum: 371.35313814878464
time_elpased: 5.056
batch start
#iterations: 359
currently lose_sum: 371.28988885879517
time_elpased: 5.043
start validation test
0.637525773196
1.0
0.637525773196
0.778645177537
0
validation finish
batch start
#iterations: 360
currently lose_sum: 372.78429716825485
time_elpased: 5.002
batch start
#iterations: 361
currently lose_sum: 370.3345559835434
time_elpased: 5.026
batch start
#iterations: 362
currently lose_sum: 371.3959070444107
time_elpased: 5.053
batch start
#iterations: 363
currently lose_sum: 372.67797803878784
time_elpased: 5.05
batch start
#iterations: 364
currently lose_sum: 372.36174273490906
time_elpased: 5.033
batch start
#iterations: 365
currently lose_sum: 372.1381560564041
time_elpased: 5.048
batch start
#iterations: 366
currently lose_sum: 371.1486322283745
time_elpased: 5.053
batch start
#iterations: 367
currently lose_sum: 373.0291395187378
time_elpased: 5.034
batch start
#iterations: 368
currently lose_sum: 371.4425635933876
time_elpased: 5.031
batch start
#iterations: 369
currently lose_sum: 372.7228264808655
time_elpased: 5.067
batch start
#iterations: 370
currently lose_sum: 371.06889206171036
time_elpased: 5.048
batch start
#iterations: 371
currently lose_sum: 371.54400688409805
time_elpased: 5.069
batch start
#iterations: 372
currently lose_sum: 371.87902134656906
time_elpased: 5.055
batch start
#iterations: 373
currently lose_sum: 371.14680045843124
time_elpased: 5.036
batch start
#iterations: 374
currently lose_sum: 371.4849166274071
time_elpased: 5.046
batch start
#iterations: 375
currently lose_sum: 373.1208363175392
time_elpased: 5.034
batch start
#iterations: 376
currently lose_sum: 371.7828720808029
time_elpased: 5.027
batch start
#iterations: 377
currently lose_sum: 371.33927768468857
time_elpased: 5.014
batch start
#iterations: 378
currently lose_sum: 372.22138994932175
time_elpased: 5.04
batch start
#iterations: 379
currently lose_sum: 371.9811459183693
time_elpased: 5.031
start validation test
0.632371134021
1.0
0.632371134021
0.774788429961
0
validation finish
batch start
#iterations: 380
currently lose_sum: 372.09913462400436
time_elpased: 5.064
batch start
#iterations: 381
currently lose_sum: 372.00223499536514
time_elpased: 5.049
batch start
#iterations: 382
currently lose_sum: 370.60182124376297
time_elpased: 5.061
batch start
#iterations: 383
currently lose_sum: 370.6345382928848
time_elpased: 5.076
batch start
#iterations: 384
currently lose_sum: 370.67013108730316
time_elpased: 5.052
batch start
#iterations: 385
currently lose_sum: 370.5992228984833
time_elpased: 5.06
batch start
#iterations: 386
currently lose_sum: 371.58649414777756
time_elpased: 5.031
batch start
#iterations: 387
currently lose_sum: 370.7991701364517
time_elpased: 5.053
batch start
#iterations: 388
currently lose_sum: 372.77630203962326
time_elpased: 5.063
batch start
#iterations: 389
currently lose_sum: 371.5361782312393
time_elpased: 5.076
batch start
#iterations: 390
currently lose_sum: 370.7514891028404
time_elpased: 5.045
batch start
#iterations: 391
currently lose_sum: 370.5148655176163
time_elpased: 5.04
batch start
#iterations: 392
currently lose_sum: 372.42535907030106
time_elpased: 5.077
batch start
#iterations: 393
currently lose_sum: 373.3926278948784
time_elpased: 5.061
batch start
#iterations: 394
currently lose_sum: 371.1369770169258
time_elpased: 5.03
batch start
#iterations: 395
currently lose_sum: 372.5200642347336
time_elpased: 5.048
batch start
#iterations: 396
currently lose_sum: 371.4972275495529
time_elpased: 5.093
batch start
#iterations: 397
currently lose_sum: 372.1414849758148
time_elpased: 5.091
batch start
#iterations: 398
currently lose_sum: 372.25219732522964
time_elpased: 5.061
batch start
#iterations: 399
currently lose_sum: 372.5768189430237
time_elpased: 5.081
start validation test
0.656907216495
1.0
0.656907216495
0.792931806869
0
validation finish
acc: 0.737
pre: 1.000
rec: 0.737
F1: 0.849
auc: 0.000
