start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 396.20472460985184
time_elpased: 5.242
batch start
#iterations: 1
currently lose_sum: 386.890361726284
time_elpased: 5.219
batch start
#iterations: 2
currently lose_sum: 382.848584651947
time_elpased: 5.218
batch start
#iterations: 3
currently lose_sum: 380.3132213354111
time_elpased: 5.249
batch start
#iterations: 4
currently lose_sum: 379.8278539776802
time_elpased: 5.224
batch start
#iterations: 5
currently lose_sum: 380.0586668252945
time_elpased: 5.19
batch start
#iterations: 6
currently lose_sum: 378.25409096479416
time_elpased: 5.16
batch start
#iterations: 7
currently lose_sum: 377.6542300581932
time_elpased: 5.167
batch start
#iterations: 8
currently lose_sum: 378.33328771591187
time_elpased: 5.163
batch start
#iterations: 9
currently lose_sum: 377.7396867275238
time_elpased: 5.211
batch start
#iterations: 10
currently lose_sum: 376.35820430517197
time_elpased: 5.163
batch start
#iterations: 11
currently lose_sum: 376.5698712468147
time_elpased: 5.109
batch start
#iterations: 12
currently lose_sum: 375.997138440609
time_elpased: 5.126
batch start
#iterations: 13
currently lose_sum: 376.00690203905106
time_elpased: 5.118
batch start
#iterations: 14
currently lose_sum: 375.4844732284546
time_elpased: 5.116
batch start
#iterations: 15
currently lose_sum: 375.38562351465225
time_elpased: 5.138
batch start
#iterations: 16
currently lose_sum: 376.03712540864944
time_elpased: 5.132
batch start
#iterations: 17
currently lose_sum: 375.8820669054985
time_elpased: 5.08
batch start
#iterations: 18
currently lose_sum: 374.655810713768
time_elpased: 5.063
batch start
#iterations: 19
currently lose_sum: 374.8320242166519
time_elpased: 5.1
start validation test
0.493298969072
1.0
0.493298969072
0.660683465654
0
validation finish
batch start
#iterations: 20
currently lose_sum: 374.2585843205452
time_elpased: 5.467
batch start
#iterations: 21
currently lose_sum: 374.6495880484581
time_elpased: 5.268
batch start
#iterations: 22
currently lose_sum: 374.95172941684723
time_elpased: 5.252
batch start
#iterations: 23
currently lose_sum: 374.51900839805603
time_elpased: 5.15
batch start
#iterations: 24
currently lose_sum: 374.61792755126953
time_elpased: 5.089
batch start
#iterations: 25
currently lose_sum: 375.1874643564224
time_elpased: 5.105
batch start
#iterations: 26
currently lose_sum: 372.56717723608017
time_elpased: 5.113
batch start
#iterations: 27
currently lose_sum: 373.3319248557091
time_elpased: 5.108
batch start
#iterations: 28
currently lose_sum: 373.79532331228256
time_elpased: 5.07
batch start
#iterations: 29
currently lose_sum: 374.02479243278503
time_elpased: 5.115
batch start
#iterations: 30
currently lose_sum: 375.81635439395905
time_elpased: 5.147
batch start
#iterations: 31
currently lose_sum: 374.1253020167351
time_elpased: 5.114
batch start
#iterations: 32
currently lose_sum: 372.2879064679146
time_elpased: 5.092
batch start
#iterations: 33
currently lose_sum: 374.13595402240753
time_elpased: 5.103
batch start
#iterations: 34
currently lose_sum: 373.13470458984375
time_elpased: 5.206
batch start
#iterations: 35
currently lose_sum: 373.65351063013077
time_elpased: 5.238
batch start
#iterations: 36
currently lose_sum: 375.0497500896454
time_elpased: 5.269
batch start
#iterations: 37
currently lose_sum: 374.18205827474594
time_elpased: 5.24
batch start
#iterations: 38
currently lose_sum: 374.72615933418274
time_elpased: 5.184
batch start
#iterations: 39
currently lose_sum: 373.8930506706238
time_elpased: 5.165
start validation test
0.486494845361
1.0
0.486494845361
0.65455302032
0
validation finish
batch start
#iterations: 40
currently lose_sum: 374.46718496084213
time_elpased: 5.21
batch start
#iterations: 41
currently lose_sum: 373.9796122312546
time_elpased: 5.157
batch start
#iterations: 42
currently lose_sum: 372.86588138341904
time_elpased: 5.186
batch start
#iterations: 43
currently lose_sum: 373.37192046642303
time_elpased: 5.244
batch start
#iterations: 44
currently lose_sum: 375.25188732147217
time_elpased: 5.211
batch start
#iterations: 45
currently lose_sum: 373.87847858667374
time_elpased: 5.188
batch start
#iterations: 46
currently lose_sum: 371.33768063783646
time_elpased: 5.21
batch start
#iterations: 47
currently lose_sum: 373.9809760451317
time_elpased: 5.184
batch start
#iterations: 48
currently lose_sum: 373.75585263967514
time_elpased: 5.174
batch start
#iterations: 49
currently lose_sum: 374.60848408937454
time_elpased: 5.197
batch start
#iterations: 50
currently lose_sum: 373.1341214776039
time_elpased: 5.215
batch start
#iterations: 51
currently lose_sum: 372.9206288456917
time_elpased: 5.172
batch start
#iterations: 52
currently lose_sum: 372.759773850441
time_elpased: 5.24
batch start
#iterations: 53
currently lose_sum: 373.06376963853836
time_elpased: 5.255
batch start
#iterations: 54
currently lose_sum: 373.1715545654297
time_elpased: 5.213
batch start
#iterations: 55
currently lose_sum: 373.7499049901962
time_elpased: 5.193
batch start
#iterations: 56
currently lose_sum: 372.63380312919617
time_elpased: 5.2
batch start
#iterations: 57
currently lose_sum: 374.2829113006592
time_elpased: 5.192
batch start
#iterations: 58
currently lose_sum: 374.98623514175415
time_elpased: 5.142
batch start
#iterations: 59
currently lose_sum: 374.1275356411934
time_elpased: 5.193
start validation test
0.594639175258
1.0
0.594639175258
0.745797776054
0
validation finish
batch start
#iterations: 60
currently lose_sum: 374.12169855833054
time_elpased: 5.175
batch start
#iterations: 61
currently lose_sum: 372.7507619857788
time_elpased: 5.183
batch start
#iterations: 62
currently lose_sum: 373.07822066545486
time_elpased: 5.221
batch start
#iterations: 63
currently lose_sum: 373.7777528166771
time_elpased: 5.237
batch start
#iterations: 64
currently lose_sum: 373.2509112358093
time_elpased: 5.19
batch start
#iterations: 65
currently lose_sum: 372.1037535071373
time_elpased: 5.216
batch start
#iterations: 66
currently lose_sum: 374.1911269426346
time_elpased: 5.156
batch start
#iterations: 67
currently lose_sum: 374.3312010169029
time_elpased: 5.155
batch start
#iterations: 68
currently lose_sum: 372.8031296133995
time_elpased: 5.151
batch start
#iterations: 69
currently lose_sum: 372.7644419670105
time_elpased: 5.183
batch start
#iterations: 70
currently lose_sum: 372.18023324012756
time_elpased: 5.182
batch start
#iterations: 71
currently lose_sum: 371.73186671733856
time_elpased: 5.146
batch start
#iterations: 72
currently lose_sum: 373.62986785173416
time_elpased: 5.192
batch start
#iterations: 73
currently lose_sum: 374.20615047216415
time_elpased: 5.227
batch start
#iterations: 74
currently lose_sum: 373.78725296258926
time_elpased: 5.157
batch start
#iterations: 75
currently lose_sum: 372.91655296087265
time_elpased: 5.19
batch start
#iterations: 76
currently lose_sum: 372.26976704597473
time_elpased: 5.202
batch start
#iterations: 77
currently lose_sum: 372.75661140680313
time_elpased: 5.158
batch start
#iterations: 78
currently lose_sum: 372.37283819913864
time_elpased: 5.143
batch start
#iterations: 79
currently lose_sum: 373.61666238307953
time_elpased: 5.199
start validation test
0.696391752577
1.0
0.696391752577
0.821027043452
0
validation finish
batch start
#iterations: 80
currently lose_sum: 373.6763788461685
time_elpased: 5.144
batch start
#iterations: 81
currently lose_sum: 373.3841252923012
time_elpased: 5.167
batch start
#iterations: 82
currently lose_sum: 372.9438054561615
time_elpased: 5.182
batch start
#iterations: 83
currently lose_sum: 373.2381998896599
time_elpased: 5.158
batch start
#iterations: 84
currently lose_sum: 372.41778779029846
time_elpased: 5.192
batch start
#iterations: 85
currently lose_sum: 372.7728425860405
time_elpased: 5.228
batch start
#iterations: 86
currently lose_sum: 373.0178191065788
time_elpased: 5.294
batch start
#iterations: 87
currently lose_sum: 372.68779480457306
time_elpased: 5.277
batch start
#iterations: 88
currently lose_sum: 372.9088981747627
time_elpased: 5.276
batch start
#iterations: 89
currently lose_sum: 372.442262172699
time_elpased: 5.294
batch start
#iterations: 90
currently lose_sum: 372.45621305704117
time_elpased: 5.173
batch start
#iterations: 91
currently lose_sum: 372.2826050519943
time_elpased: 5.171
batch start
#iterations: 92
currently lose_sum: 373.1306260228157
time_elpased: 5.239
batch start
#iterations: 93
currently lose_sum: 374.1745867729187
time_elpased: 5.222
batch start
#iterations: 94
currently lose_sum: 372.2859995961189
time_elpased: 5.176
batch start
#iterations: 95
currently lose_sum: 373.11360335350037
time_elpased: 5.203
batch start
#iterations: 96
currently lose_sum: 372.93644458055496
time_elpased: 5.239
batch start
#iterations: 97
currently lose_sum: 372.1173602938652
time_elpased: 5.305
batch start
#iterations: 98
currently lose_sum: 372.8531860113144
time_elpased: 5.699
batch start
#iterations: 99
currently lose_sum: 373.19388818740845
time_elpased: 5.207
start validation test
0.661237113402
1.0
0.661237113402
0.796077944644
0
validation finish
batch start
#iterations: 100
currently lose_sum: 373.18563413619995
time_elpased: 5.179
batch start
#iterations: 101
currently lose_sum: 372.72085839509964
time_elpased: 5.162
batch start
#iterations: 102
currently lose_sum: 371.9028666615486
time_elpased: 5.187
batch start
#iterations: 103
currently lose_sum: 372.8600848913193
time_elpased: 5.161
batch start
#iterations: 104
currently lose_sum: 372.2107481956482
time_elpased: 5.182
batch start
#iterations: 105
currently lose_sum: 371.8229116797447
time_elpased: 5.178
batch start
#iterations: 106
currently lose_sum: 374.6322100162506
time_elpased: 5.194
batch start
#iterations: 107
currently lose_sum: 372.97630167007446
time_elpased: 5.157
batch start
#iterations: 108
currently lose_sum: 372.75738501548767
time_elpased: 5.184
batch start
#iterations: 109
currently lose_sum: 373.0059067606926
time_elpased: 5.174
batch start
#iterations: 110
currently lose_sum: 372.5266989469528
time_elpased: 5.159
batch start
#iterations: 111
currently lose_sum: 371.8010007739067
time_elpased: 5.17
batch start
#iterations: 112
currently lose_sum: 372.34821087121964
time_elpased: 5.194
batch start
#iterations: 113
currently lose_sum: 374.17839193344116
time_elpased: 5.168
batch start
#iterations: 114
currently lose_sum: 374.54062873125076
time_elpased: 5.167
batch start
#iterations: 115
currently lose_sum: 373.2936704158783
time_elpased: 5.187
batch start
#iterations: 116
currently lose_sum: 372.8269233703613
time_elpased: 5.17
batch start
#iterations: 117
currently lose_sum: 371.9074362516403
time_elpased: 5.195
batch start
#iterations: 118
currently lose_sum: 372.8603885769844
time_elpased: 5.171
batch start
#iterations: 119
currently lose_sum: 373.57652992010117
time_elpased: 5.191
start validation test
0.648865979381
1.0
0.648865979381
0.787045141928
0
validation finish
batch start
#iterations: 120
currently lose_sum: 371.27870202064514
time_elpased: 5.162
batch start
#iterations: 121
currently lose_sum: 374.1654961705208
time_elpased: 5.17
batch start
#iterations: 122
currently lose_sum: 371.77484172582626
time_elpased: 5.161
batch start
#iterations: 123
currently lose_sum: 372.8510320186615
time_elpased: 5.156
batch start
#iterations: 124
currently lose_sum: 372.11513048410416
time_elpased: 5.152
batch start
#iterations: 125
currently lose_sum: 373.2999547123909
time_elpased: 5.176
batch start
#iterations: 126
currently lose_sum: 372.94063502550125
time_elpased: 5.191
batch start
#iterations: 127
currently lose_sum: 373.0565583705902
time_elpased: 5.176
batch start
#iterations: 128
currently lose_sum: 373.0063579082489
time_elpased: 5.173
batch start
#iterations: 129
currently lose_sum: 372.67676359415054
time_elpased: 5.193
batch start
#iterations: 130
currently lose_sum: 372.8257935643196
time_elpased: 5.19
batch start
#iterations: 131
currently lose_sum: 371.998900949955
time_elpased: 5.163
batch start
#iterations: 132
currently lose_sum: 373.7206318974495
time_elpased: 5.189
batch start
#iterations: 133
currently lose_sum: 371.7215551137924
time_elpased: 5.176
batch start
#iterations: 134
currently lose_sum: 372.1772980093956
time_elpased: 5.169
batch start
#iterations: 135
currently lose_sum: 372.802976667881
time_elpased: 5.159
batch start
#iterations: 136
currently lose_sum: 372.78766691684723
time_elpased: 5.167
batch start
#iterations: 137
currently lose_sum: 372.6510185599327
time_elpased: 5.162
batch start
#iterations: 138
currently lose_sum: 372.84258967638016
time_elpased: 5.17
batch start
#iterations: 139
currently lose_sum: 372.16422939300537
time_elpased: 5.167
start validation test
0.635773195876
1.0
0.635773195876
0.777336610575
0
validation finish
batch start
#iterations: 140
currently lose_sum: 372.34180778265
time_elpased: 5.169
batch start
#iterations: 141
currently lose_sum: 372.0871610045433
time_elpased: 5.203
batch start
#iterations: 142
currently lose_sum: 372.45781821012497
time_elpased: 5.171
batch start
#iterations: 143
currently lose_sum: 373.043192923069
time_elpased: 5.184
batch start
#iterations: 144
currently lose_sum: 372.1385422348976
time_elpased: 5.19
batch start
#iterations: 145
currently lose_sum: 372.47318851947784
time_elpased: 5.164
batch start
#iterations: 146
currently lose_sum: 370.6490213871002
time_elpased: 5.173
batch start
#iterations: 147
currently lose_sum: 372.1091555953026
time_elpased: 5.164
batch start
#iterations: 148
currently lose_sum: 372.1411038637161
time_elpased: 5.158
batch start
#iterations: 149
currently lose_sum: 372.2042568922043
time_elpased: 5.177
batch start
#iterations: 150
currently lose_sum: 372.63197469711304
time_elpased: 5.192
batch start
#iterations: 151
currently lose_sum: 372.6412431001663
time_elpased: 5.201
batch start
#iterations: 152
currently lose_sum: 373.0920538306236
time_elpased: 5.19
batch start
#iterations: 153
currently lose_sum: 372.9702575802803
time_elpased: 5.158
batch start
#iterations: 154
currently lose_sum: 371.2914177775383
time_elpased: 5.14
batch start
#iterations: 155
currently lose_sum: 372.50409692525864
time_elpased: 5.17
batch start
#iterations: 156
currently lose_sum: 371.23377376794815
time_elpased: 5.148
batch start
#iterations: 157
currently lose_sum: 372.37360948324203
time_elpased: 5.144
batch start
#iterations: 158
currently lose_sum: 371.34760904312134
time_elpased: 5.151
batch start
#iterations: 159
currently lose_sum: 371.83286887407303
time_elpased: 5.121
start validation test
0.672371134021
1.0
0.672371134021
0.804093206756
0
validation finish
batch start
#iterations: 160
currently lose_sum: 372.26975882053375
time_elpased: 5.117
batch start
#iterations: 161
currently lose_sum: 373.45010483264923
time_elpased: 5.123
batch start
#iterations: 162
currently lose_sum: 371.6208395957947
time_elpased: 5.172
batch start
#iterations: 163
currently lose_sum: 372.18717420101166
time_elpased: 5.153
batch start
#iterations: 164
currently lose_sum: 371.5398390889168
time_elpased: 5.15
batch start
#iterations: 165
currently lose_sum: 373.33320873975754
time_elpased: 5.13
batch start
#iterations: 166
currently lose_sum: 373.3412457704544
time_elpased: 5.165
batch start
#iterations: 167
currently lose_sum: 371.5931878089905
time_elpased: 5.153
batch start
#iterations: 168
currently lose_sum: 371.11354476213455
time_elpased: 5.147
batch start
#iterations: 169
currently lose_sum: 371.90741378068924
time_elpased: 5.147
batch start
#iterations: 170
currently lose_sum: 373.25536918640137
time_elpased: 5.152
batch start
#iterations: 171
currently lose_sum: 373.270334482193
time_elpased: 5.153
batch start
#iterations: 172
currently lose_sum: 372.8752140402794
time_elpased: 5.135
batch start
#iterations: 173
currently lose_sum: 372.18430638313293
time_elpased: 5.135
batch start
#iterations: 174
currently lose_sum: 372.63085401058197
time_elpased: 5.124
batch start
#iterations: 175
currently lose_sum: 372.1810052394867
time_elpased: 5.21
batch start
#iterations: 176
currently lose_sum: 371.86116778850555
time_elpased: 5.417
batch start
#iterations: 177
currently lose_sum: 372.9535981416702
time_elpased: 5.438
batch start
#iterations: 178
currently lose_sum: 373.755188703537
time_elpased: 5.177
batch start
#iterations: 179
currently lose_sum: 372.9961900115013
time_elpased: 5.154
start validation test
0.642577319588
1.0
0.642577319588
0.782401305467
0
validation finish
batch start
#iterations: 180
currently lose_sum: 372.51664394140244
time_elpased: 5.125
batch start
#iterations: 181
currently lose_sum: 371.07427257299423
time_elpased: 5.158
batch start
#iterations: 182
currently lose_sum: 373.33785593509674
time_elpased: 5.163
batch start
#iterations: 183
currently lose_sum: 373.17412000894547
time_elpased: 5.16
batch start
#iterations: 184
currently lose_sum: 372.5226843357086
time_elpased: 5.15
batch start
#iterations: 185
currently lose_sum: 372.5402076244354
time_elpased: 5.158
batch start
#iterations: 186
currently lose_sum: 373.1448921561241
time_elpased: 5.163
batch start
#iterations: 187
currently lose_sum: 372.4202420115471
time_elpased: 5.145
batch start
#iterations: 188
currently lose_sum: 372.98428827524185
time_elpased: 5.147
batch start
#iterations: 189
currently lose_sum: 372.75667464733124
time_elpased: 5.158
batch start
#iterations: 190
currently lose_sum: 373.08181738853455
time_elpased: 5.161
batch start
#iterations: 191
currently lose_sum: 372.74993497133255
time_elpased: 5.159
batch start
#iterations: 192
currently lose_sum: 374.11889696121216
time_elpased: 5.158
batch start
#iterations: 193
currently lose_sum: 372.12995195388794
time_elpased: 5.133
batch start
#iterations: 194
currently lose_sum: 372.30041497945786
time_elpased: 5.133
batch start
#iterations: 195
currently lose_sum: 372.27990061044693
time_elpased: 5.172
batch start
#iterations: 196
currently lose_sum: 374.5034800171852
time_elpased: 5.194
batch start
#iterations: 197
currently lose_sum: 372.6752809882164
time_elpased: 5.167
batch start
#iterations: 198
currently lose_sum: 372.264911711216
time_elpased: 5.183
batch start
#iterations: 199
currently lose_sum: 372.10365229845047
time_elpased: 5.169
start validation test
0.626907216495
1.0
0.626907216495
0.770673594829
0
validation finish
batch start
#iterations: 200
currently lose_sum: 371.9532159566879
time_elpased: 5.16
batch start
#iterations: 201
currently lose_sum: 372.61366868019104
time_elpased: 5.164
batch start
#iterations: 202
currently lose_sum: 373.0714120864868
time_elpased: 5.177
batch start
#iterations: 203
currently lose_sum: 372.13004410266876
time_elpased: 5.158
batch start
#iterations: 204
currently lose_sum: 372.66037714481354
time_elpased: 5.142
batch start
#iterations: 205
currently lose_sum: 371.67347609996796
time_elpased: 5.13
batch start
#iterations: 206
currently lose_sum: 372.09556341171265
time_elpased: 5.117
batch start
#iterations: 207
currently lose_sum: 372.5856112241745
time_elpased: 5.156
batch start
#iterations: 208
currently lose_sum: 371.8096199631691
time_elpased: 5.155
batch start
#iterations: 209
currently lose_sum: 373.25906652212143
time_elpased: 5.156
batch start
#iterations: 210
currently lose_sum: 371.972129881382
time_elpased: 5.16
batch start
#iterations: 211
currently lose_sum: 373.80452013015747
time_elpased: 5.172
batch start
#iterations: 212
currently lose_sum: 372.93452048301697
time_elpased: 5.162
batch start
#iterations: 213
currently lose_sum: 371.97062826156616
time_elpased: 5.157
batch start
#iterations: 214
currently lose_sum: 373.33351790905
time_elpased: 5.176
batch start
#iterations: 215
currently lose_sum: 372.8590428829193
time_elpased: 5.173
batch start
#iterations: 216
currently lose_sum: 373.5619154572487
time_elpased: 5.18
batch start
#iterations: 217
currently lose_sum: 370.99353140592575
time_elpased: 5.169
batch start
#iterations: 218
currently lose_sum: 372.01321333646774
time_elpased: 5.145
batch start
#iterations: 219
currently lose_sum: 373.76656180620193
time_elpased: 5.158
start validation test
0.614639175258
1.0
0.614639175258
0.76133316307
0
validation finish
batch start
#iterations: 220
currently lose_sum: 373.1385164260864
time_elpased: 5.179
batch start
#iterations: 221
currently lose_sum: 371.95115661621094
time_elpased: 5.19
batch start
#iterations: 222
currently lose_sum: 372.93384152650833
time_elpased: 5.199
batch start
#iterations: 223
currently lose_sum: 372.36132431030273
time_elpased: 5.198
batch start
#iterations: 224
currently lose_sum: 372.15698021650314
time_elpased: 5.169
batch start
#iterations: 225
currently lose_sum: 371.32507759332657
time_elpased: 5.154
batch start
#iterations: 226
currently lose_sum: 372.0793939232826
time_elpased: 5.152
batch start
#iterations: 227
currently lose_sum: 372.83998787403107
time_elpased: 5.122
batch start
#iterations: 228
currently lose_sum: 372.9508045911789
time_elpased: 5.151
batch start
#iterations: 229
currently lose_sum: 371.83551955223083
time_elpased: 5.192
batch start
#iterations: 230
currently lose_sum: 372.7529888153076
time_elpased: 5.208
batch start
#iterations: 231
currently lose_sum: 372.86133497953415
time_elpased: 5.18
batch start
#iterations: 232
currently lose_sum: 372.606377184391
time_elpased: 5.219
batch start
#iterations: 233
currently lose_sum: 372.8643956184387
time_elpased: 5.199
batch start
#iterations: 234
currently lose_sum: 373.060418009758
time_elpased: 5.133
batch start
#iterations: 235
currently lose_sum: 371.9390261769295
time_elpased: 5.166
batch start
#iterations: 236
currently lose_sum: 372.4624655842781
time_elpased: 5.15
batch start
#iterations: 237
currently lose_sum: 373.0011540055275
time_elpased: 5.118
batch start
#iterations: 238
currently lose_sum: 372.51062512397766
time_elpased: 5.123
batch start
#iterations: 239
currently lose_sum: 371.76132905483246
time_elpased: 5.148
start validation test
0.64
1.0
0.64
0.780487804878
0
validation finish
batch start
#iterations: 240
currently lose_sum: 372.7093829512596
time_elpased: 5.126
batch start
#iterations: 241
currently lose_sum: 372.36181604862213
time_elpased: 5.163
batch start
#iterations: 242
currently lose_sum: 373.90307265520096
time_elpased: 5.141
batch start
#iterations: 243
currently lose_sum: 372.30721485614777
time_elpased: 5.144
batch start
#iterations: 244
currently lose_sum: 372.7288071513176
time_elpased: 5.139
batch start
#iterations: 245
currently lose_sum: 372.8028769493103
time_elpased: 5.191
batch start
#iterations: 246
currently lose_sum: 372.5512234568596
time_elpased: 5.17
batch start
#iterations: 247
currently lose_sum: 370.9694057106972
time_elpased: 5.137
batch start
#iterations: 248
currently lose_sum: 372.79258185625076
time_elpased: 5.115
batch start
#iterations: 249
currently lose_sum: 371.1152049303055
time_elpased: 5.163
batch start
#iterations: 250
currently lose_sum: 372.59306371212006
time_elpased: 5.161
batch start
#iterations: 251
currently lose_sum: 371.73518681526184
time_elpased: 5.154
batch start
#iterations: 252
currently lose_sum: 371.4964505434036
time_elpased: 5.154
batch start
#iterations: 253
currently lose_sum: 372.5157741904259
time_elpased: 5.214
batch start
#iterations: 254
currently lose_sum: 371.6921268105507
time_elpased: 5.191
batch start
#iterations: 255
currently lose_sum: 372.119442820549
time_elpased: 5.359
batch start
#iterations: 256
currently lose_sum: 372.0050458908081
time_elpased: 5.533
batch start
#iterations: 257
currently lose_sum: 372.6578842997551
time_elpased: 5.397
batch start
#iterations: 258
currently lose_sum: 371.94181340932846
time_elpased: 5.191
batch start
#iterations: 259
currently lose_sum: 372.41386288404465
time_elpased: 5.181
start validation test
0.650309278351
1.0
0.650309278351
0.788105947026
0
validation finish
batch start
#iterations: 260
currently lose_sum: 373.4260638952255
time_elpased: 5.192
batch start
#iterations: 261
currently lose_sum: 371.9814234972
time_elpased: 5.211
batch start
#iterations: 262
currently lose_sum: 370.8230519890785
time_elpased: 5.241
batch start
#iterations: 263
currently lose_sum: 372.2659071087837
time_elpased: 5.23
batch start
#iterations: 264
currently lose_sum: 373.69997531175613
time_elpased: 5.205
batch start
#iterations: 265
currently lose_sum: 372.2837728857994
time_elpased: 5.219
batch start
#iterations: 266
currently lose_sum: 372.1251692175865
time_elpased: 5.221
batch start
#iterations: 267
currently lose_sum: 372.3723939061165
time_elpased: 5.199
batch start
#iterations: 268
currently lose_sum: 373.6018908023834
time_elpased: 5.229
batch start
#iterations: 269
currently lose_sum: 373.3054328560829
time_elpased: 5.221
batch start
#iterations: 270
currently lose_sum: 372.3112936615944
time_elpased: 5.207
batch start
#iterations: 271
currently lose_sum: 372.307901263237
time_elpased: 5.233
batch start
#iterations: 272
currently lose_sum: 372.2690024971962
time_elpased: 5.244
batch start
#iterations: 273
currently lose_sum: 372.6123043894768
time_elpased: 5.208
batch start
#iterations: 274
currently lose_sum: 372.36963123083115
time_elpased: 5.228
batch start
#iterations: 275
currently lose_sum: 371.76993399858475
time_elpased: 5.32
batch start
#iterations: 276
currently lose_sum: 373.23380821943283
time_elpased: 5.232
batch start
#iterations: 277
currently lose_sum: 372.7515355348587
time_elpased: 5.18
batch start
#iterations: 278
currently lose_sum: 373.71525794267654
time_elpased: 5.182
batch start
#iterations: 279
currently lose_sum: 371.4688844680786
time_elpased: 5.183
start validation test
0.597216494845
1.0
0.597216494845
0.74782159685
0
validation finish
batch start
#iterations: 280
currently lose_sum: 372.8871582746506
time_elpased: 5.167
batch start
#iterations: 281
currently lose_sum: 371.4288310408592
time_elpased: 5.203
batch start
#iterations: 282
currently lose_sum: 372.9663357138634
time_elpased: 5.243
batch start
#iterations: 283
currently lose_sum: 373.9031963944435
time_elpased: 5.256
batch start
#iterations: 284
currently lose_sum: 372.4709432721138
time_elpased: 5.189
batch start
#iterations: 285
currently lose_sum: 372.5547971725464
time_elpased: 5.207
batch start
#iterations: 286
currently lose_sum: 372.3253980875015
time_elpased: 5.196
batch start
#iterations: 287
currently lose_sum: 373.08646100759506
time_elpased: 5.194
batch start
#iterations: 288
currently lose_sum: 371.88768005371094
time_elpased: 5.229
batch start
#iterations: 289
currently lose_sum: 370.1002024412155
time_elpased: 5.232
batch start
#iterations: 290
currently lose_sum: 371.9092112183571
time_elpased: 5.197
batch start
#iterations: 291
currently lose_sum: 372.4170772433281
time_elpased: 5.211
batch start
#iterations: 292
currently lose_sum: 372.2461574077606
time_elpased: 5.234
batch start
#iterations: 293
currently lose_sum: 373.0551207065582
time_elpased: 5.233
batch start
#iterations: 294
currently lose_sum: 372.00796192884445
time_elpased: 5.237
batch start
#iterations: 295
currently lose_sum: 373.36983972787857
time_elpased: 5.222
batch start
#iterations: 296
currently lose_sum: 372.01308155059814
time_elpased: 5.19
batch start
#iterations: 297
currently lose_sum: 373.5699418783188
time_elpased: 5.212
batch start
#iterations: 298
currently lose_sum: 372.6815422177315
time_elpased: 5.261
batch start
#iterations: 299
currently lose_sum: 371.86842209100723
time_elpased: 5.28
start validation test
0.661649484536
1.0
0.661649484536
0.796376721678
0
validation finish
batch start
#iterations: 300
currently lose_sum: 371.4702538847923
time_elpased: 5.266
batch start
#iterations: 301
currently lose_sum: 372.52883446216583
time_elpased: 5.265
batch start
#iterations: 302
currently lose_sum: 372.4398357272148
time_elpased: 5.254
batch start
#iterations: 303
currently lose_sum: 373.34254562854767
time_elpased: 5.232
batch start
#iterations: 304
currently lose_sum: 373.20736914873123
time_elpased: 5.244
batch start
#iterations: 305
currently lose_sum: 371.6327919960022
time_elpased: 5.224
batch start
#iterations: 306
currently lose_sum: 372.5264424085617
time_elpased: 5.247
batch start
#iterations: 307
currently lose_sum: 371.4866228699684
time_elpased: 5.227
batch start
#iterations: 308
currently lose_sum: 371.5003747344017
time_elpased: 5.218
batch start
#iterations: 309
currently lose_sum: 371.4849680662155
time_elpased: 5.22
batch start
#iterations: 310
currently lose_sum: 372.4908319115639
time_elpased: 5.263
batch start
#iterations: 311
currently lose_sum: 372.13115096092224
time_elpased: 5.268
batch start
#iterations: 312
currently lose_sum: 371.07084864377975
time_elpased: 5.235
batch start
#iterations: 313
currently lose_sum: 372.7092037796974
time_elpased: 5.235
batch start
#iterations: 314
currently lose_sum: 371.8570458292961
time_elpased: 5.24
batch start
#iterations: 315
currently lose_sum: 371.56366819143295
time_elpased: 5.232
batch start
#iterations: 316
currently lose_sum: 371.20923191308975
time_elpased: 5.296
batch start
#iterations: 317
currently lose_sum: 372.3939968943596
time_elpased: 5.633
batch start
#iterations: 318
currently lose_sum: 371.5963702201843
time_elpased: 5.366
batch start
#iterations: 319
currently lose_sum: 372.48761236667633
time_elpased: 5.179
start validation test
0.680309278351
1.0
0.680309278351
0.809742929014
0
validation finish
batch start
#iterations: 320
currently lose_sum: 370.82388240098953
time_elpased: 5.195
batch start
#iterations: 321
currently lose_sum: 372.8444623351097
time_elpased: 5.197
batch start
#iterations: 322
currently lose_sum: 371.1201511025429
time_elpased: 5.208
batch start
#iterations: 323
currently lose_sum: 372.1071415543556
time_elpased: 5.222
batch start
#iterations: 324
currently lose_sum: 370.90063977241516
time_elpased: 5.21
batch start
#iterations: 325
currently lose_sum: 372.66095089912415
time_elpased: 5.239
batch start
#iterations: 326
currently lose_sum: 373.0529289841652
time_elpased: 5.224
batch start
#iterations: 327
currently lose_sum: 372.18338680267334
time_elpased: 5.233
batch start
#iterations: 328
currently lose_sum: 373.2708897590637
time_elpased: 5.447
batch start
#iterations: 329
currently lose_sum: 372.6365090608597
time_elpased: 5.329
batch start
#iterations: 330
currently lose_sum: 372.4437005519867
time_elpased: 5.143
batch start
#iterations: 331
currently lose_sum: 373.0752405524254
time_elpased: 5.147
batch start
#iterations: 332
currently lose_sum: 373.0141481757164
time_elpased: 5.178
batch start
#iterations: 333
currently lose_sum: 372.12391340732574
time_elpased: 5.27
batch start
#iterations: 334
currently lose_sum: 372.1254531741142
time_elpased: 5.242
batch start
#iterations: 335
currently lose_sum: 371.9884540438652
time_elpased: 5.242
batch start
#iterations: 336
currently lose_sum: 370.7354858517647
time_elpased: 5.198
batch start
#iterations: 337
currently lose_sum: 372.7316763997078
time_elpased: 5.182
batch start
#iterations: 338
currently lose_sum: 372.82280772924423
time_elpased: 5.241
batch start
#iterations: 339
currently lose_sum: 371.9747973680496
time_elpased: 5.26
start validation test
0.640103092784
1.0
0.640103092784
0.780564460368
0
validation finish
batch start
#iterations: 340
currently lose_sum: 373.17373967170715
time_elpased: 5.218
batch start
#iterations: 341
currently lose_sum: 373.0456400513649
time_elpased: 5.221
batch start
#iterations: 342
currently lose_sum: 373.0389643907547
time_elpased: 5.229
batch start
#iterations: 343
currently lose_sum: 371.34399753808975
time_elpased: 5.204
batch start
#iterations: 344
currently lose_sum: 372.39884626865387
time_elpased: 5.165
batch start
#iterations: 345
currently lose_sum: 372.48262095451355
time_elpased: 5.199
batch start
#iterations: 346
currently lose_sum: 371.93188482522964
time_elpased: 5.158
batch start
#iterations: 347
currently lose_sum: 371.15199279785156
time_elpased: 5.183
batch start
#iterations: 348
currently lose_sum: 370.73123049736023
time_elpased: 5.215
batch start
#iterations: 349
currently lose_sum: 370.6279349923134
time_elpased: 5.211
batch start
#iterations: 350
currently lose_sum: 372.9866895675659
time_elpased: 5.219
batch start
#iterations: 351
currently lose_sum: 371.2606579065323
time_elpased: 5.233
batch start
#iterations: 352
currently lose_sum: 372.83967727422714
time_elpased: 5.198
batch start
#iterations: 353
currently lose_sum: 372.8184047937393
time_elpased: 5.201
batch start
#iterations: 354
currently lose_sum: 372.7209265232086
time_elpased: 5.216
batch start
#iterations: 355
currently lose_sum: 373.9515737891197
time_elpased: 5.188
batch start
#iterations: 356
currently lose_sum: 372.00825703144073
time_elpased: 5.176
batch start
#iterations: 357
currently lose_sum: 371.7485217452049
time_elpased: 5.181
batch start
#iterations: 358
currently lose_sum: 371.560289144516
time_elpased: 5.227
batch start
#iterations: 359
currently lose_sum: 373.44783556461334
time_elpased: 5.235
start validation test
0.609690721649
1.0
0.609690721649
0.75752529781
0
validation finish
batch start
#iterations: 360
currently lose_sum: 372.4982416033745
time_elpased: 5.215
batch start
#iterations: 361
currently lose_sum: 372.6368604898453
time_elpased: 5.231
batch start
#iterations: 362
currently lose_sum: 372.67331796884537
time_elpased: 5.195
batch start
#iterations: 363
currently lose_sum: 372.78103256225586
time_elpased: 5.159
batch start
#iterations: 364
currently lose_sum: 371.88732755184174
time_elpased: 5.208
batch start
#iterations: 365
currently lose_sum: 372.90669494867325
time_elpased: 5.198
batch start
#iterations: 366
currently lose_sum: 373.0309672355652
time_elpased: 5.188
batch start
#iterations: 367
currently lose_sum: 372.4234700202942
time_elpased: 5.197
batch start
#iterations: 368
currently lose_sum: 372.580517411232
time_elpased: 5.224
batch start
#iterations: 369
currently lose_sum: 370.84192085266113
time_elpased: 5.186
batch start
#iterations: 370
currently lose_sum: 371.2208287715912
time_elpased: 5.205
batch start
#iterations: 371
currently lose_sum: 372.55256974697113
time_elpased: 5.208
batch start
#iterations: 372
currently lose_sum: 371.2311576604843
time_elpased: 5.21
batch start
#iterations: 373
currently lose_sum: 372.23209393024445
time_elpased: 5.18
batch start
#iterations: 374
currently lose_sum: 373.4145950078964
time_elpased: 5.174
batch start
#iterations: 375
currently lose_sum: 371.23693680763245
time_elpased: 5.206
batch start
#iterations: 376
currently lose_sum: 372.14331316947937
time_elpased: 5.203
batch start
#iterations: 377
currently lose_sum: 372.2542949914932
time_elpased: 5.205
batch start
#iterations: 378
currently lose_sum: 371.32230228185654
time_elpased: 5.204
batch start
#iterations: 379
currently lose_sum: 373.2844324707985
time_elpased: 5.201
start validation test
0.649587628866
1.0
0.649587628866
0.787575776514
0
validation finish
batch start
#iterations: 380
currently lose_sum: 372.79709029197693
time_elpased: 5.216
batch start
#iterations: 381
currently lose_sum: 372.35171657800674
time_elpased: 5.186
batch start
#iterations: 382
currently lose_sum: 373.5775313973427
time_elpased: 5.204
batch start
#iterations: 383
currently lose_sum: 370.5249197483063
time_elpased: 5.184
batch start
#iterations: 384
currently lose_sum: 371.81122720241547
time_elpased: 5.295
batch start
#iterations: 385
currently lose_sum: 372.04772382974625
time_elpased: 5.514
batch start
#iterations: 386
currently lose_sum: 370.8809005022049
time_elpased: 5.183
batch start
#iterations: 387
currently lose_sum: 371.3750440478325
time_elpased: 5.174
batch start
#iterations: 388
currently lose_sum: 371.70285254716873
time_elpased: 5.166
batch start
#iterations: 389
currently lose_sum: 372.76349180936813
time_elpased: 5.196
batch start
#iterations: 390
currently lose_sum: 371.3670517206192
time_elpased: 5.218
batch start
#iterations: 391
currently lose_sum: 371.4162600636482
time_elpased: 5.243
batch start
#iterations: 392
currently lose_sum: 372.3725981116295
time_elpased: 5.236
batch start
#iterations: 393
currently lose_sum: 371.57447576522827
time_elpased: 5.213
batch start
#iterations: 394
currently lose_sum: 372.7925525903702
time_elpased: 5.259
batch start
#iterations: 395
currently lose_sum: 371.8452868461609
time_elpased: 5.212
batch start
#iterations: 396
currently lose_sum: 371.8517657518387
time_elpased: 5.246
batch start
#iterations: 397
currently lose_sum: 373.326769053936
time_elpased: 5.203
batch start
#iterations: 398
currently lose_sum: 372.4405815601349
time_elpased: 5.226
batch start
#iterations: 399
currently lose_sum: 373.2543562054634
time_elpased: 5.246
start validation test
0.643298969072
1.0
0.643298969072
0.782936010038
0
validation finish
acc: 0.698
pre: 1.000
rec: 0.698
F1: 0.822
auc: 0.000
