start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 395.68762016296387
time_elpased: 5.32
batch start
#iterations: 1
currently lose_sum: 387.41059082746506
time_elpased: 5.338
batch start
#iterations: 2
currently lose_sum: 383.72530621290207
time_elpased: 5.327
batch start
#iterations: 3
currently lose_sum: 381.3626943230629
time_elpased: 5.278
batch start
#iterations: 4
currently lose_sum: 380.4801720380783
time_elpased: 5.289
batch start
#iterations: 5
currently lose_sum: 379.32785987854004
time_elpased: 5.265
batch start
#iterations: 6
currently lose_sum: 378.56922310590744
time_elpased: 5.263
batch start
#iterations: 7
currently lose_sum: 376.683574616909
time_elpased: 5.296
batch start
#iterations: 8
currently lose_sum: 376.96289175748825
time_elpased: 5.274
batch start
#iterations: 9
currently lose_sum: 377.9277374744415
time_elpased: 5.272
batch start
#iterations: 10
currently lose_sum: 375.3857008218765
time_elpased: 5.267
batch start
#iterations: 11
currently lose_sum: 375.8776288628578
time_elpased: 5.278
batch start
#iterations: 12
currently lose_sum: 376.2956140637398
time_elpased: 5.251
batch start
#iterations: 13
currently lose_sum: 376.89369970560074
time_elpased: 5.27
batch start
#iterations: 14
currently lose_sum: 376.3933868408203
time_elpased: 5.272
batch start
#iterations: 15
currently lose_sum: 375.2373232245445
time_elpased: 5.321
batch start
#iterations: 16
currently lose_sum: 375.12868666648865
time_elpased: 5.424
batch start
#iterations: 17
currently lose_sum: 374.9295881986618
time_elpased: 5.372
batch start
#iterations: 18
currently lose_sum: 374.4376283288002
time_elpased: 5.202
batch start
#iterations: 19
currently lose_sum: 375.8073217868805
time_elpased: 5.198
start validation test
0.701855670103
1.0
0.701855670103
0.824812212261
0
validation finish
batch start
#iterations: 20
currently lose_sum: 374.5573187470436
time_elpased: 5.201
batch start
#iterations: 21
currently lose_sum: 376.26427721977234
time_elpased: 5.193
batch start
#iterations: 22
currently lose_sum: 374.212922334671
time_elpased: 5.23
batch start
#iterations: 23
currently lose_sum: 375.64330780506134
time_elpased: 5.175
batch start
#iterations: 24
currently lose_sum: 374.256182551384
time_elpased: 5.204
batch start
#iterations: 25
currently lose_sum: 374.7500182390213
time_elpased: 5.223
batch start
#iterations: 26
currently lose_sum: 373.9598500728607
time_elpased: 5.221
batch start
#iterations: 27
currently lose_sum: 374.75319361686707
time_elpased: 5.17
batch start
#iterations: 28
currently lose_sum: 374.37916272878647
time_elpased: 5.189
batch start
#iterations: 29
currently lose_sum: 374.55544233322144
time_elpased: 5.193
batch start
#iterations: 30
currently lose_sum: 373.41348600387573
time_elpased: 5.189
batch start
#iterations: 31
currently lose_sum: 375.2423020005226
time_elpased: 5.222
batch start
#iterations: 32
currently lose_sum: 374.42932510375977
time_elpased: 5.218
batch start
#iterations: 33
currently lose_sum: 374.18183517456055
time_elpased: 5.207
batch start
#iterations: 34
currently lose_sum: 372.4810869693756
time_elpased: 5.199
batch start
#iterations: 35
currently lose_sum: 372.5184081196785
time_elpased: 5.211
batch start
#iterations: 36
currently lose_sum: 373.8782991170883
time_elpased: 5.193
batch start
#iterations: 37
currently lose_sum: 373.0575916171074
time_elpased: 5.189
batch start
#iterations: 38
currently lose_sum: 374.41899478435516
time_elpased: 5.22
batch start
#iterations: 39
currently lose_sum: 374.3227931857109
time_elpased: 5.232
start validation test
0.682474226804
1.0
0.682474226804
0.811274509804
0
validation finish
batch start
#iterations: 40
currently lose_sum: 373.133688390255
time_elpased: 5.274
batch start
#iterations: 41
currently lose_sum: 374.857746720314
time_elpased: 5.288
batch start
#iterations: 42
currently lose_sum: 373.4666739106178
time_elpased: 5.274
batch start
#iterations: 43
currently lose_sum: 373.22657161951065
time_elpased: 5.267
batch start
#iterations: 44
currently lose_sum: 372.7807685136795
time_elpased: 5.293
batch start
#iterations: 45
currently lose_sum: 374.6173664331436
time_elpased: 5.292
batch start
#iterations: 46
currently lose_sum: 373.41444528102875
time_elpased: 5.315
batch start
#iterations: 47
currently lose_sum: 372.8878154158592
time_elpased: 5.334
batch start
#iterations: 48
currently lose_sum: 373.4881248474121
time_elpased: 5.291
batch start
#iterations: 49
currently lose_sum: 373.53839695453644
time_elpased: 5.277
batch start
#iterations: 50
currently lose_sum: 374.01894545555115
time_elpased: 5.308
batch start
#iterations: 51
currently lose_sum: 373.50142669677734
time_elpased: 5.355
batch start
#iterations: 52
currently lose_sum: 373.45095866918564
time_elpased: 5.256
batch start
#iterations: 53
currently lose_sum: 373.9123388528824
time_elpased: 5.27
batch start
#iterations: 54
currently lose_sum: 374.75453835725784
time_elpased: 5.246
batch start
#iterations: 55
currently lose_sum: 372.49912917613983
time_elpased: 5.266
batch start
#iterations: 56
currently lose_sum: 372.46372109651566
time_elpased: 5.261
batch start
#iterations: 57
currently lose_sum: 372.04282915592194
time_elpased: 5.217
batch start
#iterations: 58
currently lose_sum: 373.1745350956917
time_elpased: 5.236
batch start
#iterations: 59
currently lose_sum: 373.42726957798004
time_elpased: 5.235
start validation test
0.599690721649
1.0
0.599690721649
0.749758329574
0
validation finish
batch start
#iterations: 60
currently lose_sum: 372.9137083888054
time_elpased: 5.238
batch start
#iterations: 61
currently lose_sum: 373.9925332069397
time_elpased: 5.244
batch start
#iterations: 62
currently lose_sum: 373.3932616710663
time_elpased: 5.228
batch start
#iterations: 63
currently lose_sum: 374.6719841361046
time_elpased: 5.23
batch start
#iterations: 64
currently lose_sum: 374.0977535843849
time_elpased: 5.22
batch start
#iterations: 65
currently lose_sum: 372.6851843595505
time_elpased: 5.262
batch start
#iterations: 66
currently lose_sum: 373.92265021800995
time_elpased: 5.28
batch start
#iterations: 67
currently lose_sum: 372.75834280252457
time_elpased: 5.288
batch start
#iterations: 68
currently lose_sum: 372.12117475271225
time_elpased: 5.298
batch start
#iterations: 69
currently lose_sum: 372.9034614562988
time_elpased: 5.295
batch start
#iterations: 70
currently lose_sum: 373.7244448661804
time_elpased: 5.267
batch start
#iterations: 71
currently lose_sum: 373.6886888742447
time_elpased: 5.266
batch start
#iterations: 72
currently lose_sum: 371.75518852472305
time_elpased: 5.275
batch start
#iterations: 73
currently lose_sum: 373.6416093111038
time_elpased: 5.32
batch start
#iterations: 74
currently lose_sum: 373.10380655527115
time_elpased: 5.269
batch start
#iterations: 75
currently lose_sum: 373.52582693099976
time_elpased: 5.263
batch start
#iterations: 76
currently lose_sum: 373.2364624738693
time_elpased: 5.268
batch start
#iterations: 77
currently lose_sum: 372.5792051553726
time_elpased: 5.274
batch start
#iterations: 78
currently lose_sum: 373.5078589320183
time_elpased: 5.262
batch start
#iterations: 79
currently lose_sum: 371.8543366789818
time_elpased: 5.244
start validation test
0.690721649485
1.0
0.690721649485
0.817073170732
0
validation finish
batch start
#iterations: 80
currently lose_sum: 373.86497247219086
time_elpased: 5.263
batch start
#iterations: 81
currently lose_sum: 374.5319131016731
time_elpased: 5.284
batch start
#iterations: 82
currently lose_sum: 372.4288110136986
time_elpased: 5.271
batch start
#iterations: 83
currently lose_sum: 371.6288820505142
time_elpased: 5.264
batch start
#iterations: 84
currently lose_sum: 373.450501203537
time_elpased: 5.275
batch start
#iterations: 85
currently lose_sum: 373.04707968235016
time_elpased: 5.572
batch start
#iterations: 86
currently lose_sum: 372.0509216785431
time_elpased: 5.403
batch start
#iterations: 87
currently lose_sum: 373.4504409432411
time_elpased: 5.374
batch start
#iterations: 88
currently lose_sum: 373.43940484523773
time_elpased: 5.315
batch start
#iterations: 89
currently lose_sum: 373.6849517226219
time_elpased: 5.293
batch start
#iterations: 90
currently lose_sum: 373.5919885635376
time_elpased: 5.309
batch start
#iterations: 91
currently lose_sum: 371.62898606061935
time_elpased: 5.566
batch start
#iterations: 92
currently lose_sum: 373.983589887619
time_elpased: 5.241
batch start
#iterations: 93
currently lose_sum: 372.92666190862656
time_elpased: 5.248
batch start
#iterations: 94
currently lose_sum: 374.1830033659935
time_elpased: 5.244
batch start
#iterations: 95
currently lose_sum: 371.9014280438423
time_elpased: 5.21
batch start
#iterations: 96
currently lose_sum: 374.7421809434891
time_elpased: 5.233
batch start
#iterations: 97
currently lose_sum: 372.98562920093536
time_elpased: 5.235
batch start
#iterations: 98
currently lose_sum: 372.38289445638657
time_elpased: 5.228
batch start
#iterations: 99
currently lose_sum: 373.1911001801491
time_elpased: 5.256
start validation test
0.603298969072
1.0
0.603298969072
0.752572016461
0
validation finish
batch start
#iterations: 100
currently lose_sum: 373.1692505478859
time_elpased: 5.276
batch start
#iterations: 101
currently lose_sum: 374.42206501960754
time_elpased: 5.268
batch start
#iterations: 102
currently lose_sum: 373.0371518135071
time_elpased: 5.25
batch start
#iterations: 103
currently lose_sum: 372.9369512796402
time_elpased: 5.272
batch start
#iterations: 104
currently lose_sum: 372.4022343158722
time_elpased: 5.268
batch start
#iterations: 105
currently lose_sum: 373.6776474714279
time_elpased: 5.246
batch start
#iterations: 106
currently lose_sum: 372.7498406767845
time_elpased: 5.27
batch start
#iterations: 107
currently lose_sum: 373.42129242420197
time_elpased: 5.285
batch start
#iterations: 108
currently lose_sum: 374.3730245232582
time_elpased: 5.284
batch start
#iterations: 109
currently lose_sum: 371.5723980665207
time_elpased: 5.292
batch start
#iterations: 110
currently lose_sum: 372.9288691878319
time_elpased: 5.268
batch start
#iterations: 111
currently lose_sum: 372.2663956284523
time_elpased: 5.262
batch start
#iterations: 112
currently lose_sum: 372.7309779524803
time_elpased: 5.28
batch start
#iterations: 113
currently lose_sum: 373.39030808210373
time_elpased: 5.297
batch start
#iterations: 114
currently lose_sum: 372.2051618695259
time_elpased: 5.299
batch start
#iterations: 115
currently lose_sum: 371.9137516617775
time_elpased: 5.318
batch start
#iterations: 116
currently lose_sum: 373.5929344892502
time_elpased: 5.291
batch start
#iterations: 117
currently lose_sum: 373.63814437389374
time_elpased: 5.278
batch start
#iterations: 118
currently lose_sum: 372.08547163009644
time_elpased: 5.298
batch start
#iterations: 119
currently lose_sum: 374.05467945337296
time_elpased: 5.293
start validation test
0.617835051546
1.0
0.617835051546
0.763780029312
0
validation finish
batch start
#iterations: 120
currently lose_sum: 372.6231892108917
time_elpased: 5.29
batch start
#iterations: 121
currently lose_sum: 372.80268758535385
time_elpased: 5.276
batch start
#iterations: 122
currently lose_sum: 372.84767013788223
time_elpased: 5.263
batch start
#iterations: 123
currently lose_sum: 371.83188593387604
time_elpased: 5.278
batch start
#iterations: 124
currently lose_sum: 371.9739878773689
time_elpased: 5.29
batch start
#iterations: 125
currently lose_sum: 372.97187465429306
time_elpased: 5.273
batch start
#iterations: 126
currently lose_sum: 371.85764014720917
time_elpased: 5.248
batch start
#iterations: 127
currently lose_sum: 371.95803529024124
time_elpased: 5.295
batch start
#iterations: 128
currently lose_sum: 372.4686979651451
time_elpased: 5.257
batch start
#iterations: 129
currently lose_sum: 372.91737926006317
time_elpased: 5.213
batch start
#iterations: 130
currently lose_sum: 371.4734578728676
time_elpased: 5.228
batch start
#iterations: 131
currently lose_sum: 373.28085511922836
time_elpased: 5.201
batch start
#iterations: 132
currently lose_sum: 372.6009267568588
time_elpased: 5.259
batch start
#iterations: 133
currently lose_sum: 371.7393196821213
time_elpased: 5.275
batch start
#iterations: 134
currently lose_sum: 372.6700535416603
time_elpased: 5.269
batch start
#iterations: 135
currently lose_sum: 372.36736810207367
time_elpased: 5.229
batch start
#iterations: 136
currently lose_sum: 372.36537075042725
time_elpased: 5.22
batch start
#iterations: 137
currently lose_sum: 374.0402629971504
time_elpased: 5.308
batch start
#iterations: 138
currently lose_sum: 373.18579041957855
time_elpased: 5.228
batch start
#iterations: 139
currently lose_sum: 373.2794864177704
time_elpased: 5.259
start validation test
0.636288659794
1.0
0.636288659794
0.777721774194
0
validation finish
batch start
#iterations: 140
currently lose_sum: 373.14197957515717
time_elpased: 5.22
batch start
#iterations: 141
currently lose_sum: 373.4877014160156
time_elpased: 5.215
batch start
#iterations: 142
currently lose_sum: 372.4338381290436
time_elpased: 5.234
batch start
#iterations: 143
currently lose_sum: 372.2739621400833
time_elpased: 5.241
batch start
#iterations: 144
currently lose_sum: 373.9112448692322
time_elpased: 5.289
batch start
#iterations: 145
currently lose_sum: 372.7893381714821
time_elpased: 5.325
batch start
#iterations: 146
currently lose_sum: 372.95871579647064
time_elpased: 5.272
batch start
#iterations: 147
currently lose_sum: 372.9042955636978
time_elpased: 5.303
batch start
#iterations: 148
currently lose_sum: 374.108907520771
time_elpased: 5.597
batch start
#iterations: 149
currently lose_sum: 372.1277537345886
time_elpased: 5.271
batch start
#iterations: 150
currently lose_sum: 372.7971104979515
time_elpased: 5.302
batch start
#iterations: 151
currently lose_sum: 372.0363042354584
time_elpased: 5.305
batch start
#iterations: 152
currently lose_sum: 372.6111394762993
time_elpased: 5.34
batch start
#iterations: 153
currently lose_sum: 372.5636839270592
time_elpased: 5.282
batch start
#iterations: 154
currently lose_sum: 371.61655366420746
time_elpased: 5.337
batch start
#iterations: 155
currently lose_sum: 373.31177842617035
time_elpased: 5.321
batch start
#iterations: 156
currently lose_sum: 372.436964571476
time_elpased: 5.309
batch start
#iterations: 157
currently lose_sum: 371.78042966127396
time_elpased: 5.34
batch start
#iterations: 158
currently lose_sum: 371.54453974962234
time_elpased: 5.35
batch start
#iterations: 159
currently lose_sum: 371.64744156599045
time_elpased: 5.311
start validation test
0.695154639175
1.0
0.695154639175
0.820166636259
0
validation finish
batch start
#iterations: 160
currently lose_sum: 373.57423573732376
time_elpased: 5.356
batch start
#iterations: 161
currently lose_sum: 373.26922303438187
time_elpased: 5.327
batch start
#iterations: 162
currently lose_sum: 372.7554826140404
time_elpased: 5.303
batch start
#iterations: 163
currently lose_sum: 374.182974755764
time_elpased: 5.292
batch start
#iterations: 164
currently lose_sum: 373.00732469558716
time_elpased: 5.294
batch start
#iterations: 165
currently lose_sum: 373.0757516026497
time_elpased: 5.279
batch start
#iterations: 166
currently lose_sum: 372.6869287490845
time_elpased: 5.312
batch start
#iterations: 167
currently lose_sum: 371.63988757133484
time_elpased: 5.371
batch start
#iterations: 168
currently lose_sum: 372.07657992839813
time_elpased: 5.357
batch start
#iterations: 169
currently lose_sum: 372.229447722435
time_elpased: 5.335
batch start
#iterations: 170
currently lose_sum: 372.11715602874756
time_elpased: 5.338
batch start
#iterations: 171
currently lose_sum: 373.2604156136513
time_elpased: 5.333
batch start
#iterations: 172
currently lose_sum: 372.1598557829857
time_elpased: 5.308
batch start
#iterations: 173
currently lose_sum: 372.8737522959709
time_elpased: 5.328
batch start
#iterations: 174
currently lose_sum: 372.94175159931183
time_elpased: 5.284
batch start
#iterations: 175
currently lose_sum: 372.04826360940933
time_elpased: 5.32
batch start
#iterations: 176
currently lose_sum: 372.6063569188118
time_elpased: 5.344
batch start
#iterations: 177
currently lose_sum: 373.42990320920944
time_elpased: 5.295
batch start
#iterations: 178
currently lose_sum: 372.82069516181946
time_elpased: 5.275
batch start
#iterations: 179
currently lose_sum: 372.7738119959831
time_elpased: 5.27
start validation test
0.625360824742
1.0
0.625360824742
0.769503995941
0
validation finish
batch start
#iterations: 180
currently lose_sum: 371.4910313487053
time_elpased: 5.281
batch start
#iterations: 181
currently lose_sum: 371.6359652876854
time_elpased: 5.295
batch start
#iterations: 182
currently lose_sum: 372.22286862134933
time_elpased: 5.321
batch start
#iterations: 183
currently lose_sum: 372.76609379053116
time_elpased: 5.305
batch start
#iterations: 184
currently lose_sum: 371.64457869529724
time_elpased: 5.296
batch start
#iterations: 185
currently lose_sum: 371.67029440402985
time_elpased: 5.292
batch start
#iterations: 186
currently lose_sum: 372.4535559415817
time_elpased: 5.279
batch start
#iterations: 187
currently lose_sum: 373.01810282468796
time_elpased: 5.26
batch start
#iterations: 188
currently lose_sum: 371.09039628505707
time_elpased: 5.258
batch start
#iterations: 189
currently lose_sum: 371.37801933288574
time_elpased: 5.293
batch start
#iterations: 190
currently lose_sum: 371.71258771419525
time_elpased: 5.304
batch start
#iterations: 191
currently lose_sum: 371.19862765073776
time_elpased: 5.338
batch start
#iterations: 192
currently lose_sum: 373.1315750479698
time_elpased: 5.32
batch start
#iterations: 193
currently lose_sum: 371.2760249376297
time_elpased: 5.303
batch start
#iterations: 194
currently lose_sum: 372.07162779569626
time_elpased: 5.349
batch start
#iterations: 195
currently lose_sum: 371.536682844162
time_elpased: 5.695
batch start
#iterations: 196
currently lose_sum: 373.8297839164734
time_elpased: 5.329
batch start
#iterations: 197
currently lose_sum: 372.31373304128647
time_elpased: 5.321
batch start
#iterations: 198
currently lose_sum: 372.43807953596115
time_elpased: 5.289
batch start
#iterations: 199
currently lose_sum: 372.8850519657135
time_elpased: 5.296
start validation test
0.658659793814
1.0
0.658659793814
0.794207222326
0
validation finish
batch start
#iterations: 200
currently lose_sum: 373.6375507116318
time_elpased: 5.309
batch start
#iterations: 201
currently lose_sum: 371.23204374313354
time_elpased: 5.317
batch start
#iterations: 202
currently lose_sum: 372.16689294576645
time_elpased: 5.297
batch start
#iterations: 203
currently lose_sum: 372.29266238212585
time_elpased: 5.339
batch start
#iterations: 204
currently lose_sum: 372.8930030465126
time_elpased: 5.306
batch start
#iterations: 205
currently lose_sum: 372.1845808029175
time_elpased: 5.316
batch start
#iterations: 206
currently lose_sum: 372.2358921766281
time_elpased: 5.315
batch start
#iterations: 207
currently lose_sum: 371.58625572919846
time_elpased: 5.303
batch start
#iterations: 208
currently lose_sum: 373.2096955180168
time_elpased: 5.291
batch start
#iterations: 209
currently lose_sum: 370.97744619846344
time_elpased: 5.288
batch start
#iterations: 210
currently lose_sum: 372.6570835709572
time_elpased: 5.342
batch start
#iterations: 211
currently lose_sum: 371.7716413140297
time_elpased: 5.611
batch start
#iterations: 212
currently lose_sum: 371.19748574495316
time_elpased: 5.608
batch start
#iterations: 213
currently lose_sum: 372.1267920136452
time_elpased: 5.569
batch start
#iterations: 214
currently lose_sum: 371.9229776263237
time_elpased: 5.403
batch start
#iterations: 215
currently lose_sum: 372.6256293654442
time_elpased: 5.261
batch start
#iterations: 216
currently lose_sum: 372.1758655309677
time_elpased: 5.297
batch start
#iterations: 217
currently lose_sum: 373.3261342048645
time_elpased: 5.283
batch start
#iterations: 218
currently lose_sum: 372.2435534000397
time_elpased: 5.279
batch start
#iterations: 219
currently lose_sum: 371.7180840373039
time_elpased: 5.344
start validation test
0.63175257732
1.0
0.63175257732
0.774323982815
0
validation finish
batch start
#iterations: 220
currently lose_sum: 372.8213496208191
time_elpased: 5.325
batch start
#iterations: 221
currently lose_sum: 372.5674564242363
time_elpased: 5.316
batch start
#iterations: 222
currently lose_sum: 371.8715868592262
time_elpased: 5.31
batch start
#iterations: 223
currently lose_sum: 370.4058254957199
time_elpased: 5.336
batch start
#iterations: 224
currently lose_sum: 372.6484831571579
time_elpased: 5.371
batch start
#iterations: 225
currently lose_sum: 373.7560725212097
time_elpased: 5.353
batch start
#iterations: 226
currently lose_sum: 371.8808481693268
time_elpased: 5.354
batch start
#iterations: 227
currently lose_sum: 371.97305393218994
time_elpased: 5.319
batch start
#iterations: 228
currently lose_sum: 372.93768590688705
time_elpased: 5.295
batch start
#iterations: 229
currently lose_sum: 372.3956860899925
time_elpased: 5.304
batch start
#iterations: 230
currently lose_sum: 372.58959996700287
time_elpased: 5.296
batch start
#iterations: 231
currently lose_sum: 372.38866740465164
time_elpased: 5.332
batch start
#iterations: 232
currently lose_sum: 372.99648946523666
time_elpased: 5.292
batch start
#iterations: 233
currently lose_sum: 372.0558210015297
time_elpased: 5.291
batch start
#iterations: 234
currently lose_sum: 371.96430826187134
time_elpased: 5.265
batch start
#iterations: 235
currently lose_sum: 371.8264853358269
time_elpased: 5.292
batch start
#iterations: 236
currently lose_sum: 372.21274650096893
time_elpased: 5.321
batch start
#iterations: 237
currently lose_sum: 371.8553731441498
time_elpased: 5.273
batch start
#iterations: 238
currently lose_sum: 373.60572850704193
time_elpased: 5.293
batch start
#iterations: 239
currently lose_sum: 374.71912944316864
time_elpased: 5.331
start validation test
0.620309278351
1.0
0.620309278351
0.765667748298
0
validation finish
batch start
#iterations: 240
currently lose_sum: 372.16699385643005
time_elpased: 5.286
batch start
#iterations: 241
currently lose_sum: 371.6070218682289
time_elpased: 5.277
batch start
#iterations: 242
currently lose_sum: 371.43949687480927
time_elpased: 5.278
batch start
#iterations: 243
currently lose_sum: 373.4823660850525
time_elpased: 5.302
batch start
#iterations: 244
currently lose_sum: 372.5825739502907
time_elpased: 5.279
batch start
#iterations: 245
currently lose_sum: 372.1837512254715
time_elpased: 5.286
batch start
#iterations: 246
currently lose_sum: 372.570902466774
time_elpased: 5.265
batch start
#iterations: 247
currently lose_sum: 373.0886423587799
time_elpased: 5.291
batch start
#iterations: 248
currently lose_sum: 372.54156589508057
time_elpased: 5.259
batch start
#iterations: 249
currently lose_sum: 371.82351565361023
time_elpased: 5.229
batch start
#iterations: 250
currently lose_sum: 372.31360483169556
time_elpased: 5.208
batch start
#iterations: 251
currently lose_sum: 372.6879061460495
time_elpased: 5.213
batch start
#iterations: 252
currently lose_sum: 374.7040032148361
time_elpased: 5.224
batch start
#iterations: 253
currently lose_sum: 372.156773686409
time_elpased: 5.216
batch start
#iterations: 254
currently lose_sum: 372.2956565618515
time_elpased: 5.22
batch start
#iterations: 255
currently lose_sum: 372.6639366745949
time_elpased: 5.241
batch start
#iterations: 256
currently lose_sum: 373.3738979101181
time_elpased: 5.231
batch start
#iterations: 257
currently lose_sum: 372.94797319173813
time_elpased: 5.234
batch start
#iterations: 258
currently lose_sum: 371.9049073457718
time_elpased: 5.254
batch start
#iterations: 259
currently lose_sum: 371.22818207740784
time_elpased: 5.278
start validation test
0.622886597938
1.0
0.622886597938
0.767628001525
0
validation finish
batch start
#iterations: 260
currently lose_sum: 372.5414900779724
time_elpased: 5.291
batch start
#iterations: 261
currently lose_sum: 372.11920404434204
time_elpased: 5.294
batch start
#iterations: 262
currently lose_sum: 372.3374788761139
time_elpased: 5.338
batch start
#iterations: 263
currently lose_sum: 372.41880321502686
time_elpased: 5.331
batch start
#iterations: 264
currently lose_sum: 371.37687689065933
time_elpased: 5.337
batch start
#iterations: 265
currently lose_sum: 372.92987912893295
time_elpased: 5.34
batch start
#iterations: 266
currently lose_sum: 371.74490332603455
time_elpased: 5.293
batch start
#iterations: 267
currently lose_sum: 373.2027227282524
time_elpased: 5.246
batch start
#iterations: 268
currently lose_sum: 371.5352112054825
time_elpased: 5.329
batch start
#iterations: 269
currently lose_sum: 371.9399293065071
time_elpased: 5.303
batch start
#iterations: 270
currently lose_sum: 371.8682716488838
time_elpased: 5.26
batch start
#iterations: 271
currently lose_sum: 371.24581533670425
time_elpased: 5.283
batch start
#iterations: 272
currently lose_sum: 371.9074702858925
time_elpased: 5.316
batch start
#iterations: 273
currently lose_sum: 372.34333974123
time_elpased: 5.276
batch start
#iterations: 274
currently lose_sum: 372.530708193779
time_elpased: 5.31
batch start
#iterations: 275
currently lose_sum: 372.21952164173126
time_elpased: 5.282
batch start
#iterations: 276
currently lose_sum: 373.8990275859833
time_elpased: 5.275
batch start
#iterations: 277
currently lose_sum: 372.4285833835602
time_elpased: 5.294
batch start
#iterations: 278
currently lose_sum: 373.5141834616661
time_elpased: 5.365
batch start
#iterations: 279
currently lose_sum: 372.1724656820297
time_elpased: 5.295
start validation test
0.643195876289
1.0
0.643195876289
0.782859652425
0
validation finish
batch start
#iterations: 280
currently lose_sum: 371.65400606393814
time_elpased: 5.322
batch start
#iterations: 281
currently lose_sum: 371.9274541735649
time_elpased: 5.325
batch start
#iterations: 282
currently lose_sum: 373.264797270298
time_elpased: 5.371
batch start
#iterations: 283
currently lose_sum: 374.11678993701935
time_elpased: 5.342
batch start
#iterations: 284
currently lose_sum: 372.1685679554939
time_elpased: 5.362
batch start
#iterations: 285
currently lose_sum: 371.7463045120239
time_elpased: 5.318
batch start
#iterations: 286
currently lose_sum: 371.3103338479996
time_elpased: 5.309
batch start
#iterations: 287
currently lose_sum: 371.90577536821365
time_elpased: 5.302
batch start
#iterations: 288
currently lose_sum: 373.6951575279236
time_elpased: 5.272
batch start
#iterations: 289
currently lose_sum: 370.6811716556549
time_elpased: 5.277
batch start
#iterations: 290
currently lose_sum: 372.4457988142967
time_elpased: 5.291
batch start
#iterations: 291
currently lose_sum: 371.0140846967697
time_elpased: 5.274
batch start
#iterations: 292
currently lose_sum: 371.73596704006195
time_elpased: 5.298
batch start
#iterations: 293
currently lose_sum: 372.31337267160416
time_elpased: 5.297
batch start
#iterations: 294
currently lose_sum: 372.3849627375603
time_elpased: 5.274
batch start
#iterations: 295
currently lose_sum: 372.44320183992386
time_elpased: 5.28
batch start
#iterations: 296
currently lose_sum: 372.29848688840866
time_elpased: 5.27
batch start
#iterations: 297
currently lose_sum: 371.68533629179
time_elpased: 5.324
batch start
#iterations: 298
currently lose_sum: 373.15599524974823
time_elpased: 5.323
batch start
#iterations: 299
currently lose_sum: 373.0186619758606
time_elpased: 5.394
start validation test
0.652164948454
1.0
0.652164948454
0.789467115937
0
validation finish
batch start
#iterations: 300
currently lose_sum: 372.6938456892967
time_elpased: 5.363
batch start
#iterations: 301
currently lose_sum: 371.4354079961777
time_elpased: 5.397
batch start
#iterations: 302
currently lose_sum: 371.06295812129974
time_elpased: 5.363
batch start
#iterations: 303
currently lose_sum: 371.0288253426552
time_elpased: 5.547
batch start
#iterations: 304
currently lose_sum: 372.0286093354225
time_elpased: 5.702
batch start
#iterations: 305
currently lose_sum: 372.2470116019249
time_elpased: 5.578
batch start
#iterations: 306
currently lose_sum: 371.8694304227829
time_elpased: 5.525
batch start
#iterations: 307
currently lose_sum: 372.97693169116974
time_elpased: 5.332
batch start
#iterations: 308
currently lose_sum: 373.5594627857208
time_elpased: 5.25
batch start
#iterations: 309
currently lose_sum: 372.18746012449265
time_elpased: 5.252
batch start
#iterations: 310
currently lose_sum: 373.19631803035736
time_elpased: 5.317
batch start
#iterations: 311
currently lose_sum: 371.5248643755913
time_elpased: 5.407
batch start
#iterations: 312
currently lose_sum: 372.4751031398773
time_elpased: 5.418
batch start
#iterations: 313
currently lose_sum: 373.60418742895126
time_elpased: 5.382
batch start
#iterations: 314
currently lose_sum: 371.1741473674774
time_elpased: 5.49
batch start
#iterations: 315
currently lose_sum: 371.0029273033142
time_elpased: 5.535
batch start
#iterations: 316
currently lose_sum: 373.88440907001495
time_elpased: 5.381
batch start
#iterations: 317
currently lose_sum: 371.08588737249374
time_elpased: 5.263
batch start
#iterations: 318
currently lose_sum: 370.64759773015976
time_elpased: 5.288
batch start
#iterations: 319
currently lose_sum: 371.97410106658936
time_elpased: 5.276
start validation test
0.64793814433
1.0
0.64793814433
0.786362214576
0
validation finish
batch start
#iterations: 320
currently lose_sum: 371.09487146139145
time_elpased: 5.275
batch start
#iterations: 321
currently lose_sum: 373.48021590709686
time_elpased: 5.336
batch start
#iterations: 322
currently lose_sum: 372.8483805656433
time_elpased: 5.309
batch start
#iterations: 323
currently lose_sum: 370.4522163271904
time_elpased: 5.283
batch start
#iterations: 324
currently lose_sum: 371.8706873059273
time_elpased: 5.288
batch start
#iterations: 325
currently lose_sum: 372.0331751704216
time_elpased: 5.308
batch start
#iterations: 326
currently lose_sum: 372.8445665836334
time_elpased: 5.309
batch start
#iterations: 327
currently lose_sum: 371.7856904864311
time_elpased: 5.345
batch start
#iterations: 328
currently lose_sum: 372.3274962902069
time_elpased: 5.334
batch start
#iterations: 329
currently lose_sum: 372.8605368733406
time_elpased: 5.316
batch start
#iterations: 330
currently lose_sum: 372.10865622758865
time_elpased: 5.307
batch start
#iterations: 331
currently lose_sum: 372.1770229935646
time_elpased: 5.289
batch start
#iterations: 332
currently lose_sum: 372.38874983787537
time_elpased: 5.241
batch start
#iterations: 333
currently lose_sum: 371.9725882411003
time_elpased: 5.271
batch start
#iterations: 334
currently lose_sum: 371.07174557447433
time_elpased: 5.308
batch start
#iterations: 335
currently lose_sum: 371.11758840084076
time_elpased: 5.291
batch start
#iterations: 336
currently lose_sum: 371.6204507946968
time_elpased: 5.269
batch start
#iterations: 337
currently lose_sum: 371.97638499736786
time_elpased: 5.275
batch start
#iterations: 338
currently lose_sum: 372.9765023589134
time_elpased: 5.281
batch start
#iterations: 339
currently lose_sum: 373.89733815193176
time_elpased: 5.265
start validation test
0.637525773196
1.0
0.637525773196
0.778645177537
0
validation finish
batch start
#iterations: 340
currently lose_sum: 372.9053632616997
time_elpased: 5.297
batch start
#iterations: 341
currently lose_sum: 371.2413771748543
time_elpased: 5.243
batch start
#iterations: 342
currently lose_sum: 370.4515755176544
time_elpased: 5.248
batch start
#iterations: 343
currently lose_sum: 372.8651362657547
time_elpased: 5.251
batch start
#iterations: 344
currently lose_sum: 373.10619884729385
time_elpased: 5.25
batch start
#iterations: 345
currently lose_sum: 371.7363951206207
time_elpased: 5.245
batch start
#iterations: 346
currently lose_sum: 371.2017279267311
time_elpased: 5.3
batch start
#iterations: 347
currently lose_sum: 372.001866042614
time_elpased: 5.28
batch start
#iterations: 348
currently lose_sum: 372.014784514904
time_elpased: 5.318
batch start
#iterations: 349
currently lose_sum: 372.4356116056442
time_elpased: 5.317
batch start
#iterations: 350
currently lose_sum: 371.9092063307762
time_elpased: 5.289
batch start
#iterations: 351
currently lose_sum: 371.9856125116348
time_elpased: 5.288
batch start
#iterations: 352
currently lose_sum: 371.70692282915115
time_elpased: 5.297
batch start
#iterations: 353
currently lose_sum: 371.38566386699677
time_elpased: 5.32
batch start
#iterations: 354
currently lose_sum: 370.70454210042953
time_elpased: 5.316
batch start
#iterations: 355
currently lose_sum: 372.19844937324524
time_elpased: 5.373
batch start
#iterations: 356
currently lose_sum: 373.6595447063446
time_elpased: 5.38
batch start
#iterations: 357
currently lose_sum: 372.24257785081863
time_elpased: 5.373
batch start
#iterations: 358
currently lose_sum: 371.98616939783096
time_elpased: 5.37
batch start
#iterations: 359
currently lose_sum: 372.5713211297989
time_elpased: 5.347
start validation test
0.634226804124
1.0
0.634226804124
0.776179661872
0
validation finish
batch start
#iterations: 360
currently lose_sum: 370.6291731595993
time_elpased: 5.378
batch start
#iterations: 361
currently lose_sum: 373.0625224709511
time_elpased: 5.35
batch start
#iterations: 362
currently lose_sum: 372.75178903341293
time_elpased: 5.341
batch start
#iterations: 363
currently lose_sum: 371.92358660697937
time_elpased: 5.349
batch start
#iterations: 364
currently lose_sum: 371.59122455120087
time_elpased: 5.354
batch start
#iterations: 365
currently lose_sum: 371.14097833633423
time_elpased: 5.377
batch start
#iterations: 366
currently lose_sum: 372.0579991340637
time_elpased: 5.36
batch start
#iterations: 367
currently lose_sum: 372.6002056002617
time_elpased: 5.361
batch start
#iterations: 368
currently lose_sum: 372.8808396458626
time_elpased: 5.361
batch start
#iterations: 369
currently lose_sum: 372.56865894794464
time_elpased: 5.366
batch start
#iterations: 370
currently lose_sum: 373.41883558034897
time_elpased: 5.389
batch start
#iterations: 371
currently lose_sum: 372.9256477355957
time_elpased: 5.365
batch start
#iterations: 372
currently lose_sum: 371.5330688357353
time_elpased: 5.377
batch start
#iterations: 373
currently lose_sum: 372.9559100866318
time_elpased: 5.346
batch start
#iterations: 374
currently lose_sum: 372.6014207005501
time_elpased: 5.372
batch start
#iterations: 375
currently lose_sum: 372.3849279284477
time_elpased: 5.382
batch start
#iterations: 376
currently lose_sum: 372.5122436285019
time_elpased: 5.391
batch start
#iterations: 377
currently lose_sum: 372.6421059370041
time_elpased: 5.351
batch start
#iterations: 378
currently lose_sum: 371.481496989727
time_elpased: 5.362
batch start
#iterations: 379
currently lose_sum: 372.41276931762695
time_elpased: 5.344
start validation test
0.639381443299
1.0
0.639381443299
0.780027669476
0
validation finish
batch start
#iterations: 380
currently lose_sum: 372.22187173366547
time_elpased: 5.332
batch start
#iterations: 381
currently lose_sum: 371.89300483465195
time_elpased: 5.285
batch start
#iterations: 382
currently lose_sum: 372.5725066661835
time_elpased: 5.309
batch start
#iterations: 383
currently lose_sum: 371.84951478242874
time_elpased: 5.314
batch start
#iterations: 384
currently lose_sum: 372.1568564772606
time_elpased: 5.343
batch start
#iterations: 385
currently lose_sum: 370.7276721596718
time_elpased: 5.309
batch start
#iterations: 386
currently lose_sum: 372.19092977046967
time_elpased: 5.274
batch start
#iterations: 387
currently lose_sum: 373.13773053884506
time_elpased: 5.306
batch start
#iterations: 388
currently lose_sum: 372.32071429491043
time_elpased: 5.332
batch start
#iterations: 389
currently lose_sum: 372.5712929368019
time_elpased: 5.318
batch start
#iterations: 390
currently lose_sum: 373.5352939963341
time_elpased: 5.335
batch start
#iterations: 391
currently lose_sum: 372.0289498567581
time_elpased: 5.382
batch start
#iterations: 392
currently lose_sum: 372.0481763482094
time_elpased: 5.35
batch start
#iterations: 393
currently lose_sum: 370.5828668475151
time_elpased: 5.369
batch start
#iterations: 394
currently lose_sum: 371.3953221440315
time_elpased: 5.333
batch start
#iterations: 395
currently lose_sum: 371.7452672123909
time_elpased: 5.673
batch start
#iterations: 396
currently lose_sum: 371.124020755291
time_elpased: 5.8
batch start
#iterations: 397
currently lose_sum: 372.2232114672661
time_elpased: 5.632
batch start
#iterations: 398
currently lose_sum: 372.6238844394684
time_elpased: 5.318
batch start
#iterations: 399
currently lose_sum: 371.9405035376549
time_elpased: 5.24
start validation test
0.629175257732
1.0
0.629175257732
0.772384990192
0
validation finish
acc: 0.714
pre: 1.000
rec: 0.714
F1: 0.833
auc: 0.000
