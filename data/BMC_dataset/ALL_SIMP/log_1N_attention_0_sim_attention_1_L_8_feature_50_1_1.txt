start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 395.9349402189255
time_elpased: 5.037
batch start
#iterations: 1
currently lose_sum: 387.52380472421646
time_elpased: 5.066
batch start
#iterations: 2
currently lose_sum: 382.4720147252083
time_elpased: 5.057
batch start
#iterations: 3
currently lose_sum: 380.2371462583542
time_elpased: 5.042
batch start
#iterations: 4
currently lose_sum: 378.5569277405739
time_elpased: 5.056
batch start
#iterations: 5
currently lose_sum: 378.4758896827698
time_elpased: 5.041
batch start
#iterations: 6
currently lose_sum: 378.57259941101074
time_elpased: 4.995
batch start
#iterations: 7
currently lose_sum: 378.06608498096466
time_elpased: 5.014
batch start
#iterations: 8
currently lose_sum: 375.38997983932495
time_elpased: 5.0
batch start
#iterations: 9
currently lose_sum: 375.82798939943314
time_elpased: 5.0
batch start
#iterations: 10
currently lose_sum: 375.8729267716408
time_elpased: 5.185
batch start
#iterations: 11
currently lose_sum: 376.4645864367485
time_elpased: 5.43
batch start
#iterations: 12
currently lose_sum: 375.13307720422745
time_elpased: 5.084
batch start
#iterations: 13
currently lose_sum: 375.87669891119003
time_elpased: 5.003
batch start
#iterations: 14
currently lose_sum: 375.22666972875595
time_elpased: 4.995
batch start
#iterations: 15
currently lose_sum: 374.8062912225723
time_elpased: 5.014
batch start
#iterations: 16
currently lose_sum: 373.60578459501266
time_elpased: 5.047
batch start
#iterations: 17
currently lose_sum: 375.4272565841675
time_elpased: 5.054
batch start
#iterations: 18
currently lose_sum: 374.3703968524933
time_elpased: 5.006
batch start
#iterations: 19
currently lose_sum: 374.39662420749664
time_elpased: 5.03
start validation test
0.540721649485
1.0
0.540721649485
0.701906992305
0
validation finish
batch start
#iterations: 20
currently lose_sum: 374.53277254104614
time_elpased: 5.034
batch start
#iterations: 21
currently lose_sum: 373.93067467212677
time_elpased: 5.053
batch start
#iterations: 22
currently lose_sum: 373.71965700387955
time_elpased: 5.051
batch start
#iterations: 23
currently lose_sum: 373.86504209041595
time_elpased: 5.031
batch start
#iterations: 24
currently lose_sum: 374.4142129421234
time_elpased: 4.988
batch start
#iterations: 25
currently lose_sum: 374.08586567640305
time_elpased: 4.992
batch start
#iterations: 26
currently lose_sum: 373.90038657188416
time_elpased: 4.989
batch start
#iterations: 27
currently lose_sum: 373.4982079863548
time_elpased: 4.963
batch start
#iterations: 28
currently lose_sum: 373.50240713357925
time_elpased: 4.982
batch start
#iterations: 29
currently lose_sum: 373.8365864753723
time_elpased: 4.99
batch start
#iterations: 30
currently lose_sum: 373.54724234342575
time_elpased: 4.985
batch start
#iterations: 31
currently lose_sum: 374.26073133945465
time_elpased: 4.992
batch start
#iterations: 32
currently lose_sum: 372.99082535505295
time_elpased: 4.969
batch start
#iterations: 33
currently lose_sum: 373.1300137042999
time_elpased: 4.966
batch start
#iterations: 34
currently lose_sum: 373.4220017194748
time_elpased: 4.977
batch start
#iterations: 35
currently lose_sum: 372.6467945575714
time_elpased: 5.017
batch start
#iterations: 36
currently lose_sum: 373.33920192718506
time_elpased: 5.014
batch start
#iterations: 37
currently lose_sum: 373.3336048722267
time_elpased: 4.996
batch start
#iterations: 38
currently lose_sum: 372.31188428401947
time_elpased: 5.005
batch start
#iterations: 39
currently lose_sum: 374.17707896232605
time_elpased: 5.0
start validation test
0.511649484536
1.0
0.511649484536
0.676941962763
0
validation finish
batch start
#iterations: 40
currently lose_sum: 374.73020815849304
time_elpased: 4.97
batch start
#iterations: 41
currently lose_sum: 373.1259310245514
time_elpased: 5.013
batch start
#iterations: 42
currently lose_sum: 372.32075476646423
time_elpased: 4.984
batch start
#iterations: 43
currently lose_sum: 372.02133190631866
time_elpased: 4.978
batch start
#iterations: 44
currently lose_sum: 372.7562675476074
time_elpased: 4.973
batch start
#iterations: 45
currently lose_sum: 373.5398211479187
time_elpased: 4.979
batch start
#iterations: 46
currently lose_sum: 372.2762680053711
time_elpased: 5.004
batch start
#iterations: 47
currently lose_sum: 373.5510101914406
time_elpased: 5.002
batch start
#iterations: 48
currently lose_sum: 373.0820809006691
time_elpased: 4.963
batch start
#iterations: 49
currently lose_sum: 372.46021711826324
time_elpased: 4.977
batch start
#iterations: 50
currently lose_sum: 373.97222751379013
time_elpased: 4.979
batch start
#iterations: 51
currently lose_sum: 371.56959223747253
time_elpased: 4.974
batch start
#iterations: 52
currently lose_sum: 372.91229712963104
time_elpased: 4.972
batch start
#iterations: 53
currently lose_sum: 371.93815380334854
time_elpased: 4.979
batch start
#iterations: 54
currently lose_sum: 372.5124969482422
time_elpased: 4.98
batch start
#iterations: 55
currently lose_sum: 373.0279110074043
time_elpased: 4.998
batch start
#iterations: 56
currently lose_sum: 373.9702042937279
time_elpased: 4.973
batch start
#iterations: 57
currently lose_sum: 372.42644560337067
time_elpased: 5.005
batch start
#iterations: 58
currently lose_sum: 373.047760784626
time_elpased: 5.001
batch start
#iterations: 59
currently lose_sum: 372.5406395792961
time_elpased: 4.991
start validation test
0.646082474227
1.0
0.646082474227
0.784994050229
0
validation finish
batch start
#iterations: 60
currently lose_sum: 372.8256123661995
time_elpased: 4.952
batch start
#iterations: 61
currently lose_sum: 373.35393273830414
time_elpased: 4.968
batch start
#iterations: 62
currently lose_sum: 373.76591128110886
time_elpased: 4.98
batch start
#iterations: 63
currently lose_sum: 372.5462161898613
time_elpased: 4.977
batch start
#iterations: 64
currently lose_sum: 373.3082376718521
time_elpased: 5.035
batch start
#iterations: 65
currently lose_sum: 371.9454614520073
time_elpased: 5.438
batch start
#iterations: 66
currently lose_sum: 371.45731270313263
time_elpased: 5.464
batch start
#iterations: 67
currently lose_sum: 372.82259488105774
time_elpased: 5.288
batch start
#iterations: 68
currently lose_sum: 373.7704745531082
time_elpased: 4.973
batch start
#iterations: 69
currently lose_sum: 373.855964243412
time_elpased: 4.994
batch start
#iterations: 70
currently lose_sum: 372.2222566604614
time_elpased: 5.038
batch start
#iterations: 71
currently lose_sum: 373.17905831336975
time_elpased: 5.01
batch start
#iterations: 72
currently lose_sum: 371.6686117053032
time_elpased: 4.974
batch start
#iterations: 73
currently lose_sum: 373.5356602668762
time_elpased: 4.975
batch start
#iterations: 74
currently lose_sum: 372.0614715218544
time_elpased: 4.983
batch start
#iterations: 75
currently lose_sum: 372.401807308197
time_elpased: 4.981
batch start
#iterations: 76
currently lose_sum: 372.28107339143753
time_elpased: 5.001
batch start
#iterations: 77
currently lose_sum: 372.54071843624115
time_elpased: 4.958
batch start
#iterations: 78
currently lose_sum: 371.9139052629471
time_elpased: 4.981
batch start
#iterations: 79
currently lose_sum: 372.5422172546387
time_elpased: 4.949
start validation test
0.70587628866
1.0
0.70587628866
0.82758203904
0
validation finish
batch start
#iterations: 80
currently lose_sum: 372.88248240947723
time_elpased: 4.968
batch start
#iterations: 81
currently lose_sum: 372.69424575567245
time_elpased: 4.966
batch start
#iterations: 82
currently lose_sum: 372.83992290496826
time_elpased: 4.968
batch start
#iterations: 83
currently lose_sum: 372.47892850637436
time_elpased: 4.97
batch start
#iterations: 84
currently lose_sum: 372.8146730661392
time_elpased: 4.968
batch start
#iterations: 85
currently lose_sum: 371.55609714984894
time_elpased: 4.904
batch start
#iterations: 86
currently lose_sum: 372.016056060791
time_elpased: 4.917
batch start
#iterations: 87
currently lose_sum: 373.50229477882385
time_elpased: 4.909
batch start
#iterations: 88
currently lose_sum: 371.5092834830284
time_elpased: 4.883
batch start
#iterations: 89
currently lose_sum: 372.45344215631485
time_elpased: 4.935
batch start
#iterations: 90
currently lose_sum: 371.65161377191544
time_elpased: 4.934
batch start
#iterations: 91
currently lose_sum: 371.8496252298355
time_elpased: 4.898
batch start
#iterations: 92
currently lose_sum: 372.83619952201843
time_elpased: 4.902
batch start
#iterations: 93
currently lose_sum: 372.82247310876846
time_elpased: 4.89
batch start
#iterations: 94
currently lose_sum: 372.96427845954895
time_elpased: 4.957
batch start
#iterations: 95
currently lose_sum: 372.1207978129387
time_elpased: 4.907
batch start
#iterations: 96
currently lose_sum: 371.8724937438965
time_elpased: 4.877
batch start
#iterations: 97
currently lose_sum: 372.8784646987915
time_elpased: 4.894
batch start
#iterations: 98
currently lose_sum: 371.7039395570755
time_elpased: 4.918
batch start
#iterations: 99
currently lose_sum: 372.503110229969
time_elpased: 4.905
start validation test
0.66206185567
1.0
0.66206185567
0.796675350453
0
validation finish
batch start
#iterations: 100
currently lose_sum: 372.4112432003021
time_elpased: 4.885
batch start
#iterations: 101
currently lose_sum: 371.14537101984024
time_elpased: 4.889
batch start
#iterations: 102
currently lose_sum: 371.47479593753815
time_elpased: 4.95
batch start
#iterations: 103
currently lose_sum: 371.6715753674507
time_elpased: 4.926
batch start
#iterations: 104
currently lose_sum: 372.311154961586
time_elpased: 4.885
batch start
#iterations: 105
currently lose_sum: 371.4075965285301
time_elpased: 4.863
batch start
#iterations: 106
currently lose_sum: 373.5903374552727
time_elpased: 4.885
batch start
#iterations: 107
currently lose_sum: 372.0581211447716
time_elpased: 4.908
batch start
#iterations: 108
currently lose_sum: 371.641863822937
time_elpased: 4.909
batch start
#iterations: 109
currently lose_sum: 372.057124376297
time_elpased: 4.9
batch start
#iterations: 110
currently lose_sum: 372.2588186264038
time_elpased: 4.901
batch start
#iterations: 111
currently lose_sum: 371.1555046439171
time_elpased: 4.953
batch start
#iterations: 112
currently lose_sum: 370.9359355568886
time_elpased: 4.89
batch start
#iterations: 113
currently lose_sum: 372.0564532279968
time_elpased: 4.875
batch start
#iterations: 114
currently lose_sum: 372.3354188799858
time_elpased: 4.857
batch start
#iterations: 115
currently lose_sum: 373.41754800081253
time_elpased: 4.877
batch start
#iterations: 116
currently lose_sum: 371.96747064590454
time_elpased: 4.885
batch start
#iterations: 117
currently lose_sum: 372.27524411678314
time_elpased: 4.854
batch start
#iterations: 118
currently lose_sum: 371.9725853204727
time_elpased: 4.833
batch start
#iterations: 119
currently lose_sum: 371.3371154665947
time_elpased: 4.899
start validation test
0.641340206186
1.0
0.641340206186
0.781483575152
0
validation finish
batch start
#iterations: 120
currently lose_sum: 371.03514832258224
time_elpased: 4.971
batch start
#iterations: 121
currently lose_sum: 372.3267385959625
time_elpased: 4.909
batch start
#iterations: 122
currently lose_sum: 371.55814296007156
time_elpased: 4.884
batch start
#iterations: 123
currently lose_sum: 370.79961383342743
time_elpased: 4.896
batch start
#iterations: 124
currently lose_sum: 372.7167021036148
time_elpased: 4.919
batch start
#iterations: 125
currently lose_sum: 371.0682666897774
time_elpased: 4.89
batch start
#iterations: 126
currently lose_sum: 371.7462268471718
time_elpased: 4.871
batch start
#iterations: 127
currently lose_sum: 371.2044585943222
time_elpased: 4.881
batch start
#iterations: 128
currently lose_sum: 371.76254016160965
time_elpased: 4.917
batch start
#iterations: 129
currently lose_sum: 373.6390779018402
time_elpased: 4.977
batch start
#iterations: 130
currently lose_sum: 370.71293503046036
time_elpased: 4.93
batch start
#iterations: 131
currently lose_sum: 371.571932554245
time_elpased: 4.941
batch start
#iterations: 132
currently lose_sum: 370.8315724134445
time_elpased: 4.987
batch start
#iterations: 133
currently lose_sum: 371.39932894706726
time_elpased: 5.004
batch start
#iterations: 134
currently lose_sum: 370.8449395895004
time_elpased: 5.015
batch start
#iterations: 135
currently lose_sum: 372.657533288002
time_elpased: 4.969
batch start
#iterations: 136
currently lose_sum: 371.72692716121674
time_elpased: 5.003
batch start
#iterations: 137
currently lose_sum: 371.0677462220192
time_elpased: 5.008
batch start
#iterations: 138
currently lose_sum: 373.01352494955063
time_elpased: 4.958
batch start
#iterations: 139
currently lose_sum: 370.6338533759117
time_elpased: 4.964
start validation test
0.66175257732
1.0
0.66175257732
0.796451392766
0
validation finish
batch start
#iterations: 140
currently lose_sum: 370.56751388311386
time_elpased: 4.971
batch start
#iterations: 141
currently lose_sum: 371.4253149032593
time_elpased: 4.986
batch start
#iterations: 142
currently lose_sum: 373.0192715525627
time_elpased: 4.947
batch start
#iterations: 143
currently lose_sum: 372.0147004723549
time_elpased: 4.967
batch start
#iterations: 144
currently lose_sum: 371.67388528585434
time_elpased: 5.009
batch start
#iterations: 145
currently lose_sum: 373.0887672305107
time_elpased: 4.944
batch start
#iterations: 146
currently lose_sum: 372.55529046058655
time_elpased: 4.888
batch start
#iterations: 147
currently lose_sum: 371.6969393491745
time_elpased: 4.894
batch start
#iterations: 148
currently lose_sum: 372.9990019798279
time_elpased: 4.909
batch start
#iterations: 149
currently lose_sum: 371.66434919834137
time_elpased: 4.952
batch start
#iterations: 150
currently lose_sum: 370.5480816960335
time_elpased: 4.891
batch start
#iterations: 151
currently lose_sum: 372.0582786798477
time_elpased: 4.895
batch start
#iterations: 152
currently lose_sum: 371.2012175321579
time_elpased: 4.889
batch start
#iterations: 153
currently lose_sum: 372.9506102204323
time_elpased: 4.912
batch start
#iterations: 154
currently lose_sum: 372.20182079076767
time_elpased: 4.883
batch start
#iterations: 155
currently lose_sum: 371.43127769231796
time_elpased: 4.893
batch start
#iterations: 156
currently lose_sum: 373.1891879439354
time_elpased: 4.861
batch start
#iterations: 157
currently lose_sum: 371.7357286810875
time_elpased: 4.912
batch start
#iterations: 158
currently lose_sum: 370.9626055955887
time_elpased: 4.928
batch start
#iterations: 159
currently lose_sum: 373.004189491272
time_elpased: 4.889
start validation test
0.699381443299
1.0
0.699381443299
0.823101189032
0
validation finish
batch start
#iterations: 160
currently lose_sum: 373.0540856719017
time_elpased: 4.909
batch start
#iterations: 161
currently lose_sum: 372.24559128284454
time_elpased: 4.94
batch start
#iterations: 162
currently lose_sum: 372.1519083380699
time_elpased: 4.962
batch start
#iterations: 163
currently lose_sum: 371.659253180027
time_elpased: 4.945
batch start
#iterations: 164
currently lose_sum: 371.7928218841553
time_elpased: 4.956
batch start
#iterations: 165
currently lose_sum: 373.1703785061836
time_elpased: 4.971
batch start
#iterations: 166
currently lose_sum: 371.6241022348404
time_elpased: 4.965
batch start
#iterations: 167
currently lose_sum: 371.0785948038101
time_elpased: 4.925
batch start
#iterations: 168
currently lose_sum: 371.2086573243141
time_elpased: 4.916
batch start
#iterations: 169
currently lose_sum: 371.8299651145935
time_elpased: 4.935
batch start
#iterations: 170
currently lose_sum: 370.63441890478134
time_elpased: 5.168
batch start
#iterations: 171
currently lose_sum: 371.399458527565
time_elpased: 5.484
batch start
#iterations: 172
currently lose_sum: 372.58717036247253
time_elpased: 4.942
batch start
#iterations: 173
currently lose_sum: 370.97622323036194
time_elpased: 4.945
batch start
#iterations: 174
currently lose_sum: 371.3820444345474
time_elpased: 4.945
batch start
#iterations: 175
currently lose_sum: 371.62349224090576
time_elpased: 4.948
batch start
#iterations: 176
currently lose_sum: 371.1822432875633
time_elpased: 4.968
batch start
#iterations: 177
currently lose_sum: 372.7806260585785
time_elpased: 4.942
batch start
#iterations: 178
currently lose_sum: 371.41926741600037
time_elpased: 5.053
batch start
#iterations: 179
currently lose_sum: 372.5481251478195
time_elpased: 5.197
start validation test
0.667628865979
1.0
0.667628865979
0.800692383778
0
validation finish
batch start
#iterations: 180
currently lose_sum: 373.6409940123558
time_elpased: 5.212
batch start
#iterations: 181
currently lose_sum: 371.71995908021927
time_elpased: 5.196
batch start
#iterations: 182
currently lose_sum: 373.3822989463806
time_elpased: 5.055
batch start
#iterations: 183
currently lose_sum: 370.01142394542694
time_elpased: 5.149
batch start
#iterations: 184
currently lose_sum: 370.35973930358887
time_elpased: 5.173
batch start
#iterations: 185
currently lose_sum: 372.67901784181595
time_elpased: 5.183
batch start
#iterations: 186
currently lose_sum: 371.0908856391907
time_elpased: 5.199
batch start
#iterations: 187
currently lose_sum: 372.2862916588783
time_elpased: 5.204
batch start
#iterations: 188
currently lose_sum: 371.3263796567917
time_elpased: 5.14
batch start
#iterations: 189
currently lose_sum: 372.2671054005623
time_elpased: 4.986
batch start
#iterations: 190
currently lose_sum: 370.49415707588196
time_elpased: 4.97
batch start
#iterations: 191
currently lose_sum: 372.62251007556915
time_elpased: 4.987
batch start
#iterations: 192
currently lose_sum: 372.6124207377434
time_elpased: 4.981
batch start
#iterations: 193
currently lose_sum: 371.03876119852066
time_elpased: 4.963
batch start
#iterations: 194
currently lose_sum: 370.78497076034546
time_elpased: 4.961
batch start
#iterations: 195
currently lose_sum: 370.9581086039543
time_elpased: 4.947
batch start
#iterations: 196
currently lose_sum: 372.7451999783516
time_elpased: 4.955
batch start
#iterations: 197
currently lose_sum: 372.35539668798447
time_elpased: 4.963
batch start
#iterations: 198
currently lose_sum: 372.290221452713
time_elpased: 4.964
batch start
#iterations: 199
currently lose_sum: 371.14629554748535
time_elpased: 5.057
start validation test
0.618144329897
1.0
0.618144329897
0.764016309888
0
validation finish
batch start
#iterations: 200
currently lose_sum: 370.83870673179626
time_elpased: 5.29
batch start
#iterations: 201
currently lose_sum: 371.55878388881683
time_elpased: 5.194
batch start
#iterations: 202
currently lose_sum: 371.307961165905
time_elpased: 4.941
batch start
#iterations: 203
currently lose_sum: 372.5786184668541
time_elpased: 4.962
batch start
#iterations: 204
currently lose_sum: 371.04100227355957
time_elpased: 4.952
batch start
#iterations: 205
currently lose_sum: 371.9549997448921
time_elpased: 4.937
batch start
#iterations: 206
currently lose_sum: 371.7136246562004
time_elpased: 4.959
batch start
#iterations: 207
currently lose_sum: 372.5991780757904
time_elpased: 4.966
batch start
#iterations: 208
currently lose_sum: 371.122136592865
time_elpased: 4.991
batch start
#iterations: 209
currently lose_sum: 372.5639263391495
time_elpased: 4.973
batch start
#iterations: 210
currently lose_sum: 369.87834191322327
time_elpased: 4.985
batch start
#iterations: 211
currently lose_sum: 371.33097875118256
time_elpased: 4.987
batch start
#iterations: 212
currently lose_sum: 371.25240540504456
time_elpased: 4.974
batch start
#iterations: 213
currently lose_sum: 369.99107867479324
time_elpased: 4.95
batch start
#iterations: 214
currently lose_sum: 371.6560215353966
time_elpased: 4.962
batch start
#iterations: 215
currently lose_sum: 372.6011652946472
time_elpased: 4.954
batch start
#iterations: 216
currently lose_sum: 371.1373104453087
time_elpased: 4.958
batch start
#iterations: 217
currently lose_sum: 371.32647573947906
time_elpased: 4.945
batch start
#iterations: 218
currently lose_sum: 371.87864804267883
time_elpased: 4.966
batch start
#iterations: 219
currently lose_sum: 373.26712316274643
time_elpased: 4.97
start validation test
0.619484536082
1.0
0.619484536082
0.765039149532
0
validation finish
batch start
#iterations: 220
currently lose_sum: 371.4950007200241
time_elpased: 4.976
batch start
#iterations: 221
currently lose_sum: 370.9691416621208
time_elpased: 4.962
batch start
#iterations: 222
currently lose_sum: 370.0363549590111
time_elpased: 4.985
batch start
#iterations: 223
currently lose_sum: 371.6445883512497
time_elpased: 4.973
batch start
#iterations: 224
currently lose_sum: 371.7709873318672
time_elpased: 4.97
batch start
#iterations: 225
currently lose_sum: 371.2110542654991
time_elpased: 4.955
batch start
#iterations: 226
currently lose_sum: 372.82303577661514
time_elpased: 4.967
batch start
#iterations: 227
currently lose_sum: 373.0715392231941
time_elpased: 4.948
batch start
#iterations: 228
currently lose_sum: 371.9291803240776
time_elpased: 4.972
batch start
#iterations: 229
currently lose_sum: 370.82150518894196
time_elpased: 4.945
batch start
#iterations: 230
currently lose_sum: 371.9184312224388
time_elpased: 4.968
batch start
#iterations: 231
currently lose_sum: 371.28332406282425
time_elpased: 4.973
batch start
#iterations: 232
currently lose_sum: 372.26902544498444
time_elpased: 4.981
batch start
#iterations: 233
currently lose_sum: 372.90512132644653
time_elpased: 4.981
batch start
#iterations: 234
currently lose_sum: 371.4487645626068
time_elpased: 4.992
batch start
#iterations: 235
currently lose_sum: 372.32916885614395
time_elpased: 4.974
batch start
#iterations: 236
currently lose_sum: 371.17907202243805
time_elpased: 4.98
batch start
#iterations: 237
currently lose_sum: 372.81103134155273
time_elpased: 4.959
batch start
#iterations: 238
currently lose_sum: 371.59612452983856
time_elpased: 4.968
batch start
#iterations: 239
currently lose_sum: 371.780985891819
time_elpased: 4.962
start validation test
0.626701030928
1.0
0.626701030928
0.770517776792
0
validation finish
batch start
#iterations: 240
currently lose_sum: 370.54198467731476
time_elpased: 4.961
batch start
#iterations: 241
currently lose_sum: 372.61125403642654
time_elpased: 4.957
batch start
#iterations: 242
currently lose_sum: 372.10988199710846
time_elpased: 4.968
batch start
#iterations: 243
currently lose_sum: 371.4660795331001
time_elpased: 4.954
batch start
#iterations: 244
currently lose_sum: 371.274367749691
time_elpased: 4.949
batch start
#iterations: 245
currently lose_sum: 370.97978633642197
time_elpased: 4.962
batch start
#iterations: 246
currently lose_sum: 371.4192804694176
time_elpased: 4.994
batch start
#iterations: 247
currently lose_sum: 371.9422960281372
time_elpased: 4.992
batch start
#iterations: 248
currently lose_sum: 372.931138753891
time_elpased: 4.969
batch start
#iterations: 249
currently lose_sum: 371.30307602882385
time_elpased: 4.966
batch start
#iterations: 250
currently lose_sum: 371.88910311460495
time_elpased: 4.966
batch start
#iterations: 251
currently lose_sum: 372.0762882232666
time_elpased: 5.042
batch start
#iterations: 252
currently lose_sum: 371.3722923398018
time_elpased: 5.051
batch start
#iterations: 253
currently lose_sum: 372.4729238152504
time_elpased: 4.973
batch start
#iterations: 254
currently lose_sum: 371.5569282770157
time_elpased: 4.967
batch start
#iterations: 255
currently lose_sum: 371.3277994990349
time_elpased: 4.958
batch start
#iterations: 256
currently lose_sum: 371.89991414546967
time_elpased: 4.971
batch start
#iterations: 257
currently lose_sum: 372.158382833004
time_elpased: 4.956
batch start
#iterations: 258
currently lose_sum: 370.41124653816223
time_elpased: 4.964
batch start
#iterations: 259
currently lose_sum: 371.3433166742325
time_elpased: 4.987
start validation test
0.658969072165
1.0
0.658969072165
0.794432015909
0
validation finish
batch start
#iterations: 260
currently lose_sum: 371.5208353996277
time_elpased: 5.013
batch start
#iterations: 261
currently lose_sum: 371.7794468998909
time_elpased: 4.995
batch start
#iterations: 262
currently lose_sum: 371.5121595263481
time_elpased: 4.977
batch start
#iterations: 263
currently lose_sum: 371.2414860725403
time_elpased: 4.992
batch start
#iterations: 264
currently lose_sum: 371.6161515712738
time_elpased: 4.984
batch start
#iterations: 265
currently lose_sum: 372.33917957544327
time_elpased: 4.978
batch start
#iterations: 266
currently lose_sum: 371.6669780611992
time_elpased: 4.988
batch start
#iterations: 267
currently lose_sum: 370.77895551919937
time_elpased: 4.973
batch start
#iterations: 268
currently lose_sum: 371.69061410427094
time_elpased: 4.995
batch start
#iterations: 269
currently lose_sum: 372.0223444700241
time_elpased: 4.985
batch start
#iterations: 270
currently lose_sum: 371.9967430830002
time_elpased: 4.987
batch start
#iterations: 271
currently lose_sum: 372.31494176387787
time_elpased: 5.003
batch start
#iterations: 272
currently lose_sum: 371.4951729774475
time_elpased: 4.979
batch start
#iterations: 273
currently lose_sum: 371.1163884997368
time_elpased: 4.986
batch start
#iterations: 274
currently lose_sum: 372.01503854990005
time_elpased: 4.971
batch start
#iterations: 275
currently lose_sum: 372.0745042562485
time_elpased: 4.983
batch start
#iterations: 276
currently lose_sum: 372.2791676521301
time_elpased: 4.969
batch start
#iterations: 277
currently lose_sum: 372.9765743613243
time_elpased: 4.955
batch start
#iterations: 278
currently lose_sum: 370.41263645887375
time_elpased: 4.962
batch start
#iterations: 279
currently lose_sum: 372.6827191710472
time_elpased: 4.961
start validation test
0.615360824742
1.0
0.615360824742
0.761886527538
0
validation finish
batch start
#iterations: 280
currently lose_sum: 372.1227553486824
time_elpased: 4.957
batch start
#iterations: 281
currently lose_sum: 371.3427829146385
time_elpased: 4.955
batch start
#iterations: 282
currently lose_sum: 372.4440079331398
time_elpased: 4.99
batch start
#iterations: 283
currently lose_sum: 373.19229060411453
time_elpased: 4.971
batch start
#iterations: 284
currently lose_sum: 371.95738887786865
time_elpased: 4.961
batch start
#iterations: 285
currently lose_sum: 371.7524174451828
time_elpased: 4.947
batch start
#iterations: 286
currently lose_sum: 371.5340254306793
time_elpased: 4.986
batch start
#iterations: 287
currently lose_sum: 372.2788691520691
time_elpased: 4.997
batch start
#iterations: 288
currently lose_sum: 371.6365252137184
time_elpased: 4.985
batch start
#iterations: 289
currently lose_sum: 371.2822696566582
time_elpased: 4.948
batch start
#iterations: 290
currently lose_sum: 371.59939324855804
time_elpased: 4.976
batch start
#iterations: 291
currently lose_sum: 371.05864733457565
time_elpased: 4.978
batch start
#iterations: 292
currently lose_sum: 371.29407328367233
time_elpased: 4.961
batch start
#iterations: 293
currently lose_sum: 371.9440892338753
time_elpased: 4.925
batch start
#iterations: 294
currently lose_sum: 370.9015226960182
time_elpased: 4.951
batch start
#iterations: 295
currently lose_sum: 371.16972786188126
time_elpased: 4.959
batch start
#iterations: 296
currently lose_sum: 370.856193959713
time_elpased: 4.968
batch start
#iterations: 297
currently lose_sum: 372.1209960579872
time_elpased: 4.983
batch start
#iterations: 298
currently lose_sum: 371.28915625810623
time_elpased: 4.98
batch start
#iterations: 299
currently lose_sum: 371.87095338106155
time_elpased: 4.973
start validation test
0.664020618557
1.0
0.664020618557
0.798091815873
0
validation finish
batch start
#iterations: 300
currently lose_sum: 373.1104150414467
time_elpased: 4.95
batch start
#iterations: 301
currently lose_sum: 370.26034581661224
time_elpased: 4.925
batch start
#iterations: 302
currently lose_sum: 371.7918962240219
time_elpased: 4.934
batch start
#iterations: 303
currently lose_sum: 371.0557363629341
time_elpased: 4.97
batch start
#iterations: 304
currently lose_sum: 371.9380593895912
time_elpased: 4.964
batch start
#iterations: 305
currently lose_sum: 371.7417893409729
time_elpased: 4.959
batch start
#iterations: 306
currently lose_sum: 370.9692302942276
time_elpased: 4.962
batch start
#iterations: 307
currently lose_sum: 370.4977003335953
time_elpased: 5.04
batch start
#iterations: 308
currently lose_sum: 370.63539695739746
time_elpased: 4.956
batch start
#iterations: 309
currently lose_sum: 370.50174605846405
time_elpased: 4.945
batch start
#iterations: 310
currently lose_sum: 370.7923313379288
time_elpased: 4.936
batch start
#iterations: 311
currently lose_sum: 371.8750075697899
time_elpased: 4.977
batch start
#iterations: 312
currently lose_sum: 371.4464260339737
time_elpased: 4.967
batch start
#iterations: 313
currently lose_sum: 371.35731077194214
time_elpased: 4.957
batch start
#iterations: 314
currently lose_sum: 369.4242933392525
time_elpased: 4.963
batch start
#iterations: 315
currently lose_sum: 370.9868866801262
time_elpased: 4.969
batch start
#iterations: 316
currently lose_sum: 371.3207558989525
time_elpased: 4.987
batch start
#iterations: 317
currently lose_sum: 371.89035815000534
time_elpased: 4.971
batch start
#iterations: 318
currently lose_sum: 371.5235251188278
time_elpased: 4.955
batch start
#iterations: 319
currently lose_sum: 371.67497622966766
time_elpased: 4.954
start validation test
0.677628865979
1.0
0.677628865979
0.807841209365
0
validation finish
batch start
#iterations: 320
currently lose_sum: 372.7580505013466
time_elpased: 4.98
batch start
#iterations: 321
currently lose_sum: 373.9501186609268
time_elpased: 4.961
batch start
#iterations: 322
currently lose_sum: 371.48744946718216
time_elpased: 4.974
batch start
#iterations: 323
currently lose_sum: 371.05917835235596
time_elpased: 4.95
batch start
#iterations: 324
currently lose_sum: 371.42418217658997
time_elpased: 4.958
batch start
#iterations: 325
currently lose_sum: 370.6095291376114
time_elpased: 4.993
batch start
#iterations: 326
currently lose_sum: 372.25102984905243
time_elpased: 5.039
batch start
#iterations: 327
currently lose_sum: 372.02578699588776
time_elpased: 4.981
batch start
#iterations: 328
currently lose_sum: 371.3389256000519
time_elpased: 4.975
batch start
#iterations: 329
currently lose_sum: 370.49330627918243
time_elpased: 4.976
batch start
#iterations: 330
currently lose_sum: 372.89020669460297
time_elpased: 4.989
batch start
#iterations: 331
currently lose_sum: 372.46631544828415
time_elpased: 5.003
batch start
#iterations: 332
currently lose_sum: 371.2304330468178
time_elpased: 5.006
batch start
#iterations: 333
currently lose_sum: 371.1370061635971
time_elpased: 4.97
batch start
#iterations: 334
currently lose_sum: 372.34503453969955
time_elpased: 4.962
batch start
#iterations: 335
currently lose_sum: 370.7311696410179
time_elpased: 4.983
batch start
#iterations: 336
currently lose_sum: 370.4064880013466
time_elpased: 4.99
batch start
#iterations: 337
currently lose_sum: 371.9124459028244
time_elpased: 4.957
batch start
#iterations: 338
currently lose_sum: 371.6298717856407
time_elpased: 4.966
batch start
#iterations: 339
currently lose_sum: 370.78022319078445
time_elpased: 5.237
start validation test
0.643195876289
1.0
0.643195876289
0.782859652425
0
validation finish
batch start
#iterations: 340
currently lose_sum: 371.72770541906357
time_elpased: 5.226
batch start
#iterations: 341
currently lose_sum: 372.06417059898376
time_elpased: 5.21
batch start
#iterations: 342
currently lose_sum: 370.7079676389694
time_elpased: 5.25
batch start
#iterations: 343
currently lose_sum: 371.6638359427452
time_elpased: 5.205
batch start
#iterations: 344
currently lose_sum: 369.6777436733246
time_elpased: 4.974
batch start
#iterations: 345
currently lose_sum: 372.4172413945198
time_elpased: 4.884
batch start
#iterations: 346
currently lose_sum: 370.5614064335823
time_elpased: 4.913
batch start
#iterations: 347
currently lose_sum: 369.7315073609352
time_elpased: 4.908
batch start
#iterations: 348
currently lose_sum: 372.56558072566986
time_elpased: 4.962
batch start
#iterations: 349
currently lose_sum: 371.75788176059723
time_elpased: 4.983
batch start
#iterations: 350
currently lose_sum: 372.37089931964874
time_elpased: 4.985
batch start
#iterations: 351
currently lose_sum: 369.7902207374573
time_elpased: 4.996
batch start
#iterations: 352
currently lose_sum: 371.93182933330536
time_elpased: 4.969
batch start
#iterations: 353
currently lose_sum: 371.723186314106
time_elpased: 4.962
batch start
#iterations: 354
currently lose_sum: 370.3550860285759
time_elpased: 5.005
batch start
#iterations: 355
currently lose_sum: 371.2181017398834
time_elpased: 4.968
batch start
#iterations: 356
currently lose_sum: 371.0915734767914
time_elpased: 4.945
batch start
#iterations: 357
currently lose_sum: 373.09647983312607
time_elpased: 4.95
batch start
#iterations: 358
currently lose_sum: 372.3114238381386
time_elpased: 5.109
batch start
#iterations: 359
currently lose_sum: 371.92329436540604
time_elpased: 5.217
start validation test
0.626907216495
1.0
0.626907216495
0.770673594829
0
validation finish
batch start
#iterations: 360
currently lose_sum: 370.7315836548805
time_elpased: 5.129
batch start
#iterations: 361
currently lose_sum: 371.84410214424133
time_elpased: 5.124
batch start
#iterations: 362
currently lose_sum: 372.1387724876404
time_elpased: 5.138
batch start
#iterations: 363
currently lose_sum: 371.3243569135666
time_elpased: 5.146
batch start
#iterations: 364
currently lose_sum: 371.77187418937683
time_elpased: 5.133
batch start
#iterations: 365
currently lose_sum: 373.28079211711884
time_elpased: 5.142
batch start
#iterations: 366
currently lose_sum: 372.6800321340561
time_elpased: 5.105
batch start
#iterations: 367
currently lose_sum: 372.02998065948486
time_elpased: 5.032
batch start
#iterations: 368
currently lose_sum: 371.30432695150375
time_elpased: 5.019
batch start
#iterations: 369
currently lose_sum: 370.74912399053574
time_elpased: 5.016
batch start
#iterations: 370
currently lose_sum: 370.9208201766014
time_elpased: 5.019
batch start
#iterations: 371
currently lose_sum: 372.0917298197746
time_elpased: 5.005
batch start
#iterations: 372
currently lose_sum: 370.91889810562134
time_elpased: 4.993
batch start
#iterations: 373
currently lose_sum: 372.3679865002632
time_elpased: 4.99
batch start
#iterations: 374
currently lose_sum: 370.62419790029526
time_elpased: 4.978
batch start
#iterations: 375
currently lose_sum: 371.16834628582
time_elpased: 4.99
batch start
#iterations: 376
currently lose_sum: 371.7923598885536
time_elpased: 4.991
batch start
#iterations: 377
currently lose_sum: 372.5107484459877
time_elpased: 4.99
batch start
#iterations: 378
currently lose_sum: 371.29148721694946
time_elpased: 4.997
batch start
#iterations: 379
currently lose_sum: 371.4223708510399
time_elpased: 5.003
start validation test
0.645670103093
1.0
0.645670103093
0.784689594688
0
validation finish
batch start
#iterations: 380
currently lose_sum: 370.4514681696892
time_elpased: 4.973
batch start
#iterations: 381
currently lose_sum: 369.65664505958557
time_elpased: 4.98
batch start
#iterations: 382
currently lose_sum: 371.6168437600136
time_elpased: 4.976
batch start
#iterations: 383
currently lose_sum: 369.922187268734
time_elpased: 4.966
batch start
#iterations: 384
currently lose_sum: 371.22239965200424
time_elpased: 5.019
batch start
#iterations: 385
currently lose_sum: 371.1289749145508
time_elpased: 4.997
batch start
#iterations: 386
currently lose_sum: 371.43245112895966
time_elpased: 4.983
batch start
#iterations: 387
currently lose_sum: 372.200167119503
time_elpased: 5.009
batch start
#iterations: 388
currently lose_sum: 370.97754871845245
time_elpased: 5.038
batch start
#iterations: 389
currently lose_sum: 371.0296897292137
time_elpased: 5.026
batch start
#iterations: 390
currently lose_sum: 373.17711263895035
time_elpased: 5.033
batch start
#iterations: 391
currently lose_sum: 371.8802539110184
time_elpased: 5.039
batch start
#iterations: 392
currently lose_sum: 369.946282684803
time_elpased: 5.015
batch start
#iterations: 393
currently lose_sum: 370.9261322617531
time_elpased: 4.982
batch start
#iterations: 394
currently lose_sum: 372.50196063518524
time_elpased: 4.981
batch start
#iterations: 395
currently lose_sum: 371.68178230524063
time_elpased: 4.982
batch start
#iterations: 396
currently lose_sum: 371.0929220318794
time_elpased: 4.988
batch start
#iterations: 397
currently lose_sum: 372.28799146413803
time_elpased: 5.019
batch start
#iterations: 398
currently lose_sum: 371.0836515426636
time_elpased: 5.311
batch start
#iterations: 399
currently lose_sum: 371.2270710468292
time_elpased: 5.271
start validation test
0.65618556701
1.0
0.65618556701
0.792405851229
0
validation finish
acc: 0.700
pre: 1.000
rec: 0.700
F1: 0.823
auc: 0.000
