start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 396.9788038134575
time_elpased: 5.052
batch start
#iterations: 1
currently lose_sum: 388.5157052874565
time_elpased: 5.056
batch start
#iterations: 2
currently lose_sum: 383.08301758766174
time_elpased: 4.994
batch start
#iterations: 3
currently lose_sum: 380.77332735061646
time_elpased: 4.995
batch start
#iterations: 4
currently lose_sum: 379.7061628103256
time_elpased: 4.965
batch start
#iterations: 5
currently lose_sum: 378.3618725538254
time_elpased: 4.997
batch start
#iterations: 6
currently lose_sum: 378.22879576683044
time_elpased: 4.987
batch start
#iterations: 7
currently lose_sum: 377.30684381723404
time_elpased: 4.999
batch start
#iterations: 8
currently lose_sum: 376.7910521030426
time_elpased: 4.973
batch start
#iterations: 9
currently lose_sum: 377.5617332458496
time_elpased: 4.995
batch start
#iterations: 10
currently lose_sum: 375.42629116773605
time_elpased: 5.006
batch start
#iterations: 11
currently lose_sum: 375.52159547805786
time_elpased: 4.965
batch start
#iterations: 12
currently lose_sum: 376.173072040081
time_elpased: 4.974
batch start
#iterations: 13
currently lose_sum: 376.9256566166878
time_elpased: 4.984
batch start
#iterations: 14
currently lose_sum: 373.6177430152893
time_elpased: 5.026
batch start
#iterations: 15
currently lose_sum: 375.0487123131752
time_elpased: 4.991
batch start
#iterations: 16
currently lose_sum: 374.6160823106766
time_elpased: 4.961
batch start
#iterations: 17
currently lose_sum: 376.06806695461273
time_elpased: 4.991
batch start
#iterations: 18
currently lose_sum: 373.8109214901924
time_elpased: 4.967
batch start
#iterations: 19
currently lose_sum: 375.73163360357285
time_elpased: 4.989
start validation test
0.45175257732
1.0
0.45175257732
0.622354779151
0
validation finish
batch start
#iterations: 20
currently lose_sum: 374.58499121665955
time_elpased: 4.982
batch start
#iterations: 21
currently lose_sum: 374.9374761581421
time_elpased: 5.033
batch start
#iterations: 22
currently lose_sum: 374.7160723209381
time_elpased: 5.004
batch start
#iterations: 23
currently lose_sum: 375.72001081705093
time_elpased: 4.976
batch start
#iterations: 24
currently lose_sum: 375.2121517062187
time_elpased: 4.986
batch start
#iterations: 25
currently lose_sum: 374.9097183942795
time_elpased: 5.001
batch start
#iterations: 26
currently lose_sum: 374.1182773709297
time_elpased: 4.972
batch start
#iterations: 27
currently lose_sum: 374.0873404741287
time_elpased: 4.993
batch start
#iterations: 28
currently lose_sum: 373.16189682483673
time_elpased: 5.002
batch start
#iterations: 29
currently lose_sum: 375.14852756261826
time_elpased: 5.03
batch start
#iterations: 30
currently lose_sum: 374.5539827942848
time_elpased: 5.007
batch start
#iterations: 31
currently lose_sum: 373.8136157989502
time_elpased: 5.018
batch start
#iterations: 32
currently lose_sum: 374.87887674570084
time_elpased: 5.025
batch start
#iterations: 33
currently lose_sum: 374.70810920000076
time_elpased: 5.055
batch start
#iterations: 34
currently lose_sum: 374.2057386636734
time_elpased: 5.038
batch start
#iterations: 35
currently lose_sum: 375.9334326982498
time_elpased: 5.052
batch start
#iterations: 36
currently lose_sum: 374.23511576652527
time_elpased: 5.03
batch start
#iterations: 37
currently lose_sum: 373.1837397813797
time_elpased: 5.042
batch start
#iterations: 38
currently lose_sum: 375.07212126255035
time_elpased: 5.035
batch start
#iterations: 39
currently lose_sum: 374.58244401216507
time_elpased: 5.028
start validation test
0.458865979381
1.0
0.458865979381
0.629072150378
0
validation finish
batch start
#iterations: 40
currently lose_sum: 373.32192236185074
time_elpased: 4.986
batch start
#iterations: 41
currently lose_sum: 374.06355732679367
time_elpased: 5.003
batch start
#iterations: 42
currently lose_sum: 372.313016474247
time_elpased: 4.992
batch start
#iterations: 43
currently lose_sum: 373.17696756124496
time_elpased: 4.997
batch start
#iterations: 44
currently lose_sum: 374.8995949625969
time_elpased: 5.032
batch start
#iterations: 45
currently lose_sum: 373.09342658519745
time_elpased: 5.04
batch start
#iterations: 46
currently lose_sum: 373.1064642071724
time_elpased: 5.006
batch start
#iterations: 47
currently lose_sum: 373.57099199295044
time_elpased: 5.016
batch start
#iterations: 48
currently lose_sum: 372.67165476083755
time_elpased: 5.014
batch start
#iterations: 49
currently lose_sum: 373.9111965894699
time_elpased: 5.016
batch start
#iterations: 50
currently lose_sum: 372.26143980026245
time_elpased: 5.048
batch start
#iterations: 51
currently lose_sum: 373.60762733221054
time_elpased: 5.001
batch start
#iterations: 52
currently lose_sum: 374.25427907705307
time_elpased: 5.014
batch start
#iterations: 53
currently lose_sum: 373.49268984794617
time_elpased: 5.003
batch start
#iterations: 54
currently lose_sum: 373.5848208665848
time_elpased: 5.319
batch start
#iterations: 55
currently lose_sum: 372.96388453245163
time_elpased: 5.205
batch start
#iterations: 56
currently lose_sum: 373.93032544851303
time_elpased: 5.01
batch start
#iterations: 57
currently lose_sum: 373.18976283073425
time_elpased: 4.962
batch start
#iterations: 58
currently lose_sum: 372.4479434490204
time_elpased: 4.967
batch start
#iterations: 59
currently lose_sum: 373.86208641529083
time_elpased: 5.02
start validation test
0.61412371134
1.0
0.61412371134
0.760937599796
0
validation finish
batch start
#iterations: 60
currently lose_sum: 373.5617104768753
time_elpased: 4.98
batch start
#iterations: 61
currently lose_sum: 373.00771725177765
time_elpased: 4.996
batch start
#iterations: 62
currently lose_sum: 373.26641857624054
time_elpased: 5.001
batch start
#iterations: 63
currently lose_sum: 372.39534175395966
time_elpased: 5.002
batch start
#iterations: 64
currently lose_sum: 373.4007374048233
time_elpased: 4.958
batch start
#iterations: 65
currently lose_sum: 374.0910869836807
time_elpased: 4.958
batch start
#iterations: 66
currently lose_sum: 373.3522809147835
time_elpased: 4.947
batch start
#iterations: 67
currently lose_sum: 372.70804673433304
time_elpased: 5.002
batch start
#iterations: 68
currently lose_sum: 373.87482649087906
time_elpased: 4.952
batch start
#iterations: 69
currently lose_sum: 372.6974808573723
time_elpased: 4.944
batch start
#iterations: 70
currently lose_sum: 372.8439984321594
time_elpased: 5.297
batch start
#iterations: 71
currently lose_sum: 373.2197667956352
time_elpased: 5.642
batch start
#iterations: 72
currently lose_sum: 373.1316263079643
time_elpased: 5.624
batch start
#iterations: 73
currently lose_sum: 373.3682741522789
time_elpased: 5.352
batch start
#iterations: 74
currently lose_sum: 372.93498557806015
time_elpased: 5.006
batch start
#iterations: 75
currently lose_sum: 371.31037414073944
time_elpased: 5.004
batch start
#iterations: 76
currently lose_sum: 372.48547464609146
time_elpased: 5.008
batch start
#iterations: 77
currently lose_sum: 372.66753339767456
time_elpased: 5.029
batch start
#iterations: 78
currently lose_sum: 373.014375269413
time_elpased: 5.018
batch start
#iterations: 79
currently lose_sum: 372.6023159623146
time_elpased: 5.017
start validation test
0.70381443299
1.0
0.70381443299
0.826163248018
0
validation finish
batch start
#iterations: 80
currently lose_sum: 372.06564062833786
time_elpased: 5.029
batch start
#iterations: 81
currently lose_sum: 373.7544954419136
time_elpased: 5.032
batch start
#iterations: 82
currently lose_sum: 373.00232994556427
time_elpased: 5.006
batch start
#iterations: 83
currently lose_sum: 371.7867907881737
time_elpased: 4.986
batch start
#iterations: 84
currently lose_sum: 373.76865047216415
time_elpased: 4.966
batch start
#iterations: 85
currently lose_sum: 373.11583226919174
time_elpased: 4.961
batch start
#iterations: 86
currently lose_sum: 373.3028945326805
time_elpased: 4.948
batch start
#iterations: 87
currently lose_sum: 372.6177929639816
time_elpased: 4.956
batch start
#iterations: 88
currently lose_sum: 372.218688249588
time_elpased: 4.952
batch start
#iterations: 89
currently lose_sum: 373.431587934494
time_elpased: 4.984
batch start
#iterations: 90
currently lose_sum: 374.0472313761711
time_elpased: 4.946
batch start
#iterations: 91
currently lose_sum: 373.1084523797035
time_elpased: 4.968
batch start
#iterations: 92
currently lose_sum: 372.5090097784996
time_elpased: 4.974
batch start
#iterations: 93
currently lose_sum: 373.4051148891449
time_elpased: 4.979
batch start
#iterations: 94
currently lose_sum: 372.8653864264488
time_elpased: 4.957
batch start
#iterations: 95
currently lose_sum: 371.9401465058327
time_elpased: 4.953
batch start
#iterations: 96
currently lose_sum: 373.2497039437294
time_elpased: 4.97
batch start
#iterations: 97
currently lose_sum: 372.21711897850037
time_elpased: 4.967
batch start
#iterations: 98
currently lose_sum: 372.38095921278
time_elpased: 4.965
batch start
#iterations: 99
currently lose_sum: 372.5917142033577
time_elpased: 4.949
start validation test
0.634742268041
1.0
0.634742268041
0.776565554645
0
validation finish
batch start
#iterations: 100
currently lose_sum: 372.7438004016876
time_elpased: 4.972
batch start
#iterations: 101
currently lose_sum: 372.366926074028
time_elpased: 4.988
batch start
#iterations: 102
currently lose_sum: 371.99492305517197
time_elpased: 4.969
batch start
#iterations: 103
currently lose_sum: 373.0093004107475
time_elpased: 4.967
batch start
#iterations: 104
currently lose_sum: 373.0932658314705
time_elpased: 4.98
batch start
#iterations: 105
currently lose_sum: 372.57744085788727
time_elpased: 4.975
batch start
#iterations: 106
currently lose_sum: 373.0716880559921
time_elpased: 4.998
batch start
#iterations: 107
currently lose_sum: 372.4231996536255
time_elpased: 4.979
batch start
#iterations: 108
currently lose_sum: 372.6397162079811
time_elpased: 4.995
batch start
#iterations: 109
currently lose_sum: 372.28133976459503
time_elpased: 5.015
batch start
#iterations: 110
currently lose_sum: 373.42331624031067
time_elpased: 4.999
batch start
#iterations: 111
currently lose_sum: 372.0340428352356
time_elpased: 5.005
batch start
#iterations: 112
currently lose_sum: 372.0113581418991
time_elpased: 5.07
batch start
#iterations: 113
currently lose_sum: 373.08605521917343
time_elpased: 5.021
batch start
#iterations: 114
currently lose_sum: 372.1117562055588
time_elpased: 4.977
batch start
#iterations: 115
currently lose_sum: 372.28384536504745
time_elpased: 4.97
batch start
#iterations: 116
currently lose_sum: 373.04531306028366
time_elpased: 4.99
batch start
#iterations: 117
currently lose_sum: 371.57701736688614
time_elpased: 4.988
batch start
#iterations: 118
currently lose_sum: 371.19955056905746
time_elpased: 4.99
batch start
#iterations: 119
currently lose_sum: 372.5416184067726
time_elpased: 4.994
start validation test
0.635567010309
1.0
0.635567010309
0.777182477151
0
validation finish
batch start
#iterations: 120
currently lose_sum: 373.33055341243744
time_elpased: 5.005
batch start
#iterations: 121
currently lose_sum: 371.605699300766
time_elpased: 5.017
batch start
#iterations: 122
currently lose_sum: 370.57481986284256
time_elpased: 5.02
batch start
#iterations: 123
currently lose_sum: 371.81771671772003
time_elpased: 5.024
batch start
#iterations: 124
currently lose_sum: 371.1682540178299
time_elpased: 5.015
batch start
#iterations: 125
currently lose_sum: 372.38665241003036
time_elpased: 4.988
batch start
#iterations: 126
currently lose_sum: 373.70760077238083
time_elpased: 4.991
batch start
#iterations: 127
currently lose_sum: 372.0228553414345
time_elpased: 4.981
batch start
#iterations: 128
currently lose_sum: 371.5175644159317
time_elpased: 5.001
batch start
#iterations: 129
currently lose_sum: 372.50196582078934
time_elpased: 4.984
batch start
#iterations: 130
currently lose_sum: 371.42034047842026
time_elpased: 4.941
batch start
#iterations: 131
currently lose_sum: 372.93478667736053
time_elpased: 4.966
batch start
#iterations: 132
currently lose_sum: 372.60295909643173
time_elpased: 4.993
batch start
#iterations: 133
currently lose_sum: 371.525133728981
time_elpased: 4.983
batch start
#iterations: 134
currently lose_sum: 373.133218228817
time_elpased: 4.99
batch start
#iterations: 135
currently lose_sum: 372.50249898433685
time_elpased: 4.973
batch start
#iterations: 136
currently lose_sum: 373.91275322437286
time_elpased: 4.983
batch start
#iterations: 137
currently lose_sum: 371.52356255054474
time_elpased: 4.972
batch start
#iterations: 138
currently lose_sum: 373.48781764507294
time_elpased: 4.98
batch start
#iterations: 139
currently lose_sum: 372.4889415502548
time_elpased: 4.98
start validation test
0.649587628866
1.0
0.649587628866
0.787575776514
0
validation finish
batch start
#iterations: 140
currently lose_sum: 372.74874955415726
time_elpased: 4.971
batch start
#iterations: 141
currently lose_sum: 373.1484248638153
time_elpased: 4.983
batch start
#iterations: 142
currently lose_sum: 372.72323459386826
time_elpased: 4.975
batch start
#iterations: 143
currently lose_sum: 373.33113050460815
time_elpased: 4.987
batch start
#iterations: 144
currently lose_sum: 372.56492882966995
time_elpased: 4.984
batch start
#iterations: 145
currently lose_sum: 371.96553498506546
time_elpased: 4.971
batch start
#iterations: 146
currently lose_sum: 371.65417271852493
time_elpased: 4.962
batch start
#iterations: 147
currently lose_sum: 371.98497569561005
time_elpased: 4.976
batch start
#iterations: 148
currently lose_sum: 372.5282799601555
time_elpased: 4.971
batch start
#iterations: 149
currently lose_sum: 372.8402222394943
time_elpased: 4.959
batch start
#iterations: 150
currently lose_sum: 372.05348831415176
time_elpased: 4.938
batch start
#iterations: 151
currently lose_sum: 372.68957859277725
time_elpased: 4.971
batch start
#iterations: 152
currently lose_sum: 371.7367502450943
time_elpased: 5.305
batch start
#iterations: 153
currently lose_sum: 373.5525125861168
time_elpased: 5.324
batch start
#iterations: 154
currently lose_sum: 372.0831966996193
time_elpased: 5.321
batch start
#iterations: 155
currently lose_sum: 372.43146657943726
time_elpased: 5.357
batch start
#iterations: 156
currently lose_sum: 372.75501841306686
time_elpased: 4.92
batch start
#iterations: 157
currently lose_sum: 372.1922250986099
time_elpased: 4.937
batch start
#iterations: 158
currently lose_sum: 372.3838903307915
time_elpased: 4.922
batch start
#iterations: 159
currently lose_sum: 373.8616836667061
time_elpased: 4.956
start validation test
0.690721649485
1.0
0.690721649485
0.817073170732
0
validation finish
batch start
#iterations: 160
currently lose_sum: 372.6104930639267
time_elpased: 4.936
batch start
#iterations: 161
currently lose_sum: 372.30042320489883
time_elpased: 4.92
batch start
#iterations: 162
currently lose_sum: 372.5944430232048
time_elpased: 4.911
batch start
#iterations: 163
currently lose_sum: 372.34258848428726
time_elpased: 4.91
batch start
#iterations: 164
currently lose_sum: 372.22851580381393
time_elpased: 4.911
batch start
#iterations: 165
currently lose_sum: 371.3264670968056
time_elpased: 4.936
batch start
#iterations: 166
currently lose_sum: 371.7448329925537
time_elpased: 4.889
batch start
#iterations: 167
currently lose_sum: 371.9943681359291
time_elpased: 4.91
batch start
#iterations: 168
currently lose_sum: 372.15667003393173
time_elpased: 4.921
batch start
#iterations: 169
currently lose_sum: 371.0152156352997
time_elpased: 4.938
batch start
#iterations: 170
currently lose_sum: 372.0264916419983
time_elpased: 4.972
batch start
#iterations: 171
currently lose_sum: 372.871918797493
time_elpased: 4.961
batch start
#iterations: 172
currently lose_sum: 372.7670682668686
time_elpased: 4.992
batch start
#iterations: 173
currently lose_sum: 372.201225399971
time_elpased: 4.959
batch start
#iterations: 174
currently lose_sum: 371.2419791817665
time_elpased: 4.932
batch start
#iterations: 175
currently lose_sum: 373.5637792944908
time_elpased: 4.974
batch start
#iterations: 176
currently lose_sum: 371.64070957899094
time_elpased: 4.988
batch start
#iterations: 177
currently lose_sum: 370.9028379917145
time_elpased: 4.971
batch start
#iterations: 178
currently lose_sum: 373.1903244256973
time_elpased: 4.917
batch start
#iterations: 179
currently lose_sum: 372.5994318127632
time_elpased: 4.889
start validation test
0.662474226804
1.0
0.662474226804
0.79697383108
0
validation finish
batch start
#iterations: 180
currently lose_sum: 372.46841287612915
time_elpased: 4.906
batch start
#iterations: 181
currently lose_sum: 371.8305891752243
time_elpased: 4.914
batch start
#iterations: 182
currently lose_sum: 372.0222745537758
time_elpased: 4.884
batch start
#iterations: 183
currently lose_sum: 372.4937644004822
time_elpased: 4.885
batch start
#iterations: 184
currently lose_sum: 371.9215435385704
time_elpased: 4.946
batch start
#iterations: 185
currently lose_sum: 372.5278887748718
time_elpased: 4.948
batch start
#iterations: 186
currently lose_sum: 371.68056309223175
time_elpased: 4.916
batch start
#iterations: 187
currently lose_sum: 370.33749878406525
time_elpased: 4.942
batch start
#iterations: 188
currently lose_sum: 373.17336201667786
time_elpased: 4.946
batch start
#iterations: 189
currently lose_sum: 372.93195164203644
time_elpased: 4.95
batch start
#iterations: 190
currently lose_sum: 370.494498193264
time_elpased: 4.92
batch start
#iterations: 191
currently lose_sum: 372.103223323822
time_elpased: 4.95
batch start
#iterations: 192
currently lose_sum: 372.96826708316803
time_elpased: 4.949
batch start
#iterations: 193
currently lose_sum: 373.51410990953445
time_elpased: 4.965
batch start
#iterations: 194
currently lose_sum: 371.40488517284393
time_elpased: 4.95
batch start
#iterations: 195
currently lose_sum: 371.9972864985466
time_elpased: 4.946
batch start
#iterations: 196
currently lose_sum: 374.0607782006264
time_elpased: 4.966
batch start
#iterations: 197
currently lose_sum: 371.10768896341324
time_elpased: 4.98
batch start
#iterations: 198
currently lose_sum: 372.8857917189598
time_elpased: 4.927
batch start
#iterations: 199
currently lose_sum: 371.4219140410423
time_elpased: 4.898
start validation test
0.603711340206
1.0
0.603711340206
0.752892774492
0
validation finish
batch start
#iterations: 200
currently lose_sum: 373.0950502753258
time_elpased: 4.912
batch start
#iterations: 201
currently lose_sum: 371.338003218174
time_elpased: 4.937
batch start
#iterations: 202
currently lose_sum: 371.19647538661957
time_elpased: 4.932
batch start
#iterations: 203
currently lose_sum: 370.1647197008133
time_elpased: 4.946
batch start
#iterations: 204
currently lose_sum: 372.5930076241493
time_elpased: 4.95
batch start
#iterations: 205
currently lose_sum: 371.25715988874435
time_elpased: 4.993
batch start
#iterations: 206
currently lose_sum: 371.5005062818527
time_elpased: 4.944
batch start
#iterations: 207
currently lose_sum: 372.7306464314461
time_elpased: 4.918
batch start
#iterations: 208
currently lose_sum: 372.34369719028473
time_elpased: 4.908
batch start
#iterations: 209
currently lose_sum: 372.30489295721054
time_elpased: 4.937
batch start
#iterations: 210
currently lose_sum: 371.45168870687485
time_elpased: 4.926
batch start
#iterations: 211
currently lose_sum: 371.4868063926697
time_elpased: 4.88
batch start
#iterations: 212
currently lose_sum: 373.15812224149704
time_elpased: 4.903
batch start
#iterations: 213
currently lose_sum: 372.96896386146545
time_elpased: 4.914
batch start
#iterations: 214
currently lose_sum: 372.7350620627403
time_elpased: 4.909
batch start
#iterations: 215
currently lose_sum: 372.1525315642357
time_elpased: 4.898
batch start
#iterations: 216
currently lose_sum: 372.26364624500275
time_elpased: 4.923
batch start
#iterations: 217
currently lose_sum: 371.90479999780655
time_elpased: 5.003
batch start
#iterations: 218
currently lose_sum: 371.96414691209793
time_elpased: 5.038
batch start
#iterations: 219
currently lose_sum: 373.29605370759964
time_elpased: 5.022
start validation test
0.622164948454
1.0
0.622164948454
0.7670797585
0
validation finish
batch start
#iterations: 220
currently lose_sum: 371.572628736496
time_elpased: 4.996
batch start
#iterations: 221
currently lose_sum: 372.43398904800415
time_elpased: 4.976
batch start
#iterations: 222
currently lose_sum: 372.21463483572006
time_elpased: 4.985
batch start
#iterations: 223
currently lose_sum: 371.81477719545364
time_elpased: 4.957
batch start
#iterations: 224
currently lose_sum: 372.68211072683334
time_elpased: 4.957
batch start
#iterations: 225
currently lose_sum: 371.6487403512001
time_elpased: 4.966
batch start
#iterations: 226
currently lose_sum: 372.2516739368439
time_elpased: 4.984
batch start
#iterations: 227
currently lose_sum: 372.17108196020126
time_elpased: 4.965
batch start
#iterations: 228
currently lose_sum: 372.2348912358284
time_elpased: 4.964
batch start
#iterations: 229
currently lose_sum: 370.38358956575394
time_elpased: 4.902
batch start
#iterations: 230
currently lose_sum: 371.94882768392563
time_elpased: 4.931
batch start
#iterations: 231
currently lose_sum: 372.4189229607582
time_elpased: 4.898
batch start
#iterations: 232
currently lose_sum: 372.354108273983
time_elpased: 4.885
batch start
#iterations: 233
currently lose_sum: 372.6484124660492
time_elpased: 4.878
batch start
#iterations: 234
currently lose_sum: 372.139326274395
time_elpased: 4.911
batch start
#iterations: 235
currently lose_sum: 371.6124640107155
time_elpased: 4.906
batch start
#iterations: 236
currently lose_sum: 371.7400783896446
time_elpased: 4.91
batch start
#iterations: 237
currently lose_sum: 371.2974883913994
time_elpased: 4.913
batch start
#iterations: 238
currently lose_sum: 372.5513771176338
time_elpased: 4.922
batch start
#iterations: 239
currently lose_sum: 371.6139324903488
time_elpased: 4.934
start validation test
0.62175257732
1.0
0.62175257732
0.766766257708
0
validation finish
batch start
#iterations: 240
currently lose_sum: 370.9430778026581
time_elpased: 4.91
batch start
#iterations: 241
currently lose_sum: 371.35008132457733
time_elpased: 4.913
batch start
#iterations: 242
currently lose_sum: 370.3713947534561
time_elpased: 4.945
batch start
#iterations: 243
currently lose_sum: 372.6243390440941
time_elpased: 4.914
batch start
#iterations: 244
currently lose_sum: 372.7755189538002
time_elpased: 4.947
batch start
#iterations: 245
currently lose_sum: 371.88228648900986
time_elpased: 4.932
batch start
#iterations: 246
currently lose_sum: 372.1107956767082
time_elpased: 4.959
batch start
#iterations: 247
currently lose_sum: 372.95017182826996
time_elpased: 4.961
batch start
#iterations: 248
currently lose_sum: 373.08278501033783
time_elpased: 4.955
batch start
#iterations: 249
currently lose_sum: 371.897051692009
time_elpased: 4.923
batch start
#iterations: 250
currently lose_sum: 372.55890810489655
time_elpased: 4.93
batch start
#iterations: 251
currently lose_sum: 372.98410272598267
time_elpased: 4.926
batch start
#iterations: 252
currently lose_sum: 373.56949561834335
time_elpased: 4.92
batch start
#iterations: 253
currently lose_sum: 371.21114432811737
time_elpased: 4.914
batch start
#iterations: 254
currently lose_sum: 372.63454681634903
time_elpased: 4.907
batch start
#iterations: 255
currently lose_sum: 372.7960128188133
time_elpased: 4.927
batch start
#iterations: 256
currently lose_sum: 372.30326557159424
time_elpased: 4.922
batch start
#iterations: 257
currently lose_sum: 372.6960052847862
time_elpased: 4.881
batch start
#iterations: 258
currently lose_sum: 373.1427635550499
time_elpased: 4.853
batch start
#iterations: 259
currently lose_sum: 371.87862223386765
time_elpased: 4.881
start validation test
0.667010309278
1.0
0.667010309278
0.800247371676
0
validation finish
batch start
#iterations: 260
currently lose_sum: 371.6689430475235
time_elpased: 4.885
batch start
#iterations: 261
currently lose_sum: 370.8437450528145
time_elpased: 4.86
batch start
#iterations: 262
currently lose_sum: 372.46253031492233
time_elpased: 4.87
batch start
#iterations: 263
currently lose_sum: 374.4594458937645
time_elpased: 4.922
batch start
#iterations: 264
currently lose_sum: 372.32072949409485
time_elpased: 4.939
batch start
#iterations: 265
currently lose_sum: 372.00725710392
time_elpased: 4.948
batch start
#iterations: 266
currently lose_sum: 372.45554971694946
time_elpased: 4.956
batch start
#iterations: 267
currently lose_sum: 372.20087015628815
time_elpased: 4.908
batch start
#iterations: 268
currently lose_sum: 372.2635217308998
time_elpased: 4.956
batch start
#iterations: 269
currently lose_sum: 372.6434659957886
time_elpased: 4.937
batch start
#iterations: 270
currently lose_sum: 371.0716623663902
time_elpased: 4.913
batch start
#iterations: 271
currently lose_sum: 372.3206170797348
time_elpased: 4.951
batch start
#iterations: 272
currently lose_sum: 372.83968764543533
time_elpased: 4.913
batch start
#iterations: 273
currently lose_sum: 372.81404489278793
time_elpased: 4.897
batch start
#iterations: 274
currently lose_sum: 372.82120513916016
time_elpased: 4.844
batch start
#iterations: 275
currently lose_sum: 373.185878098011
time_elpased: 4.848
batch start
#iterations: 276
currently lose_sum: 371.11422646045685
time_elpased: 4.871
batch start
#iterations: 277
currently lose_sum: 372.3775864839554
time_elpased: 4.885
batch start
#iterations: 278
currently lose_sum: 371.6647650003433
time_elpased: 4.872
batch start
#iterations: 279
currently lose_sum: 371.6556199193001
time_elpased: 4.842
start validation test
0.601958762887
1.0
0.601958762887
0.751528412382
0
validation finish
batch start
#iterations: 280
currently lose_sum: 371.1895417571068
time_elpased: 4.874
batch start
#iterations: 281
currently lose_sum: 373.1700668334961
time_elpased: 4.923
batch start
#iterations: 282
currently lose_sum: 372.5381415486336
time_elpased: 4.915
batch start
#iterations: 283
currently lose_sum: 371.91676330566406
time_elpased: 4.899
batch start
#iterations: 284
currently lose_sum: 373.21735751628876
time_elpased: 4.919
batch start
#iterations: 285
currently lose_sum: 371.366058409214
time_elpased: 4.928
batch start
#iterations: 286
currently lose_sum: 372.07372546195984
time_elpased: 4.943
batch start
#iterations: 287
currently lose_sum: 371.1916430592537
time_elpased: 4.894
batch start
#iterations: 288
currently lose_sum: 372.67144894599915
time_elpased: 4.906
batch start
#iterations: 289
currently lose_sum: 372.14903485774994
time_elpased: 4.898
batch start
#iterations: 290
currently lose_sum: 371.75278002023697
time_elpased: 4.935
batch start
#iterations: 291
currently lose_sum: 371.3828254342079
time_elpased: 4.866
batch start
#iterations: 292
currently lose_sum: 372.0815045237541
time_elpased: 4.875
batch start
#iterations: 293
currently lose_sum: 372.83838272094727
time_elpased: 4.899
batch start
#iterations: 294
currently lose_sum: 372.06352561712265
time_elpased: 4.902
batch start
#iterations: 295
currently lose_sum: 371.408077955246
time_elpased: 4.881
batch start
#iterations: 296
currently lose_sum: 371.40949469804764
time_elpased: 4.848
batch start
#iterations: 297
currently lose_sum: 371.7724199295044
time_elpased: 4.869
batch start
#iterations: 298
currently lose_sum: 371.87616270780563
time_elpased: 4.886
batch start
#iterations: 299
currently lose_sum: 370.8671236038208
time_elpased: 4.886
start validation test
0.662680412371
1.0
0.662680412371
0.797123015873
0
validation finish
batch start
#iterations: 300
currently lose_sum: 371.0113400220871
time_elpased: 4.854
batch start
#iterations: 301
currently lose_sum: 372.1633271574974
time_elpased: 4.87
batch start
#iterations: 302
currently lose_sum: 372.9443587064743
time_elpased: 4.924
batch start
#iterations: 303
currently lose_sum: 373.42012226581573
time_elpased: 4.915
batch start
#iterations: 304
currently lose_sum: 372.03410279750824
time_elpased: 4.915
batch start
#iterations: 305
currently lose_sum: 373.4357389807701
time_elpased: 4.909
batch start
#iterations: 306
currently lose_sum: 370.7588891983032
time_elpased: 4.893
batch start
#iterations: 307
currently lose_sum: 370.6581918001175
time_elpased: 4.921
batch start
#iterations: 308
currently lose_sum: 371.45888924598694
time_elpased: 4.914
batch start
#iterations: 309
currently lose_sum: 371.9635478258133
time_elpased: 4.862
batch start
#iterations: 310
currently lose_sum: 370.0590147972107
time_elpased: 4.882
batch start
#iterations: 311
currently lose_sum: 372.92543882131577
time_elpased: 4.895
batch start
#iterations: 312
currently lose_sum: 370.9924378991127
time_elpased: 4.898
batch start
#iterations: 313
currently lose_sum: 370.84650456905365
time_elpased: 4.872
batch start
#iterations: 314
currently lose_sum: 372.2993052005768
time_elpased: 4.87
batch start
#iterations: 315
currently lose_sum: 372.65202683210373
time_elpased: 4.869
batch start
#iterations: 316
currently lose_sum: 371.75948017835617
time_elpased: 4.919
batch start
#iterations: 317
currently lose_sum: 372.08921414613724
time_elpased: 4.894
batch start
#iterations: 318
currently lose_sum: 372.83168572187424
time_elpased: 4.853
batch start
#iterations: 319
currently lose_sum: 371.4203431010246
time_elpased: 4.865
start validation test
0.680618556701
1.0
0.680618556701
0.809961967857
0
validation finish
batch start
#iterations: 320
currently lose_sum: 371.7458550333977
time_elpased: 4.939
batch start
#iterations: 321
currently lose_sum: 372.1071395277977
time_elpased: 4.884
batch start
#iterations: 322
currently lose_sum: 372.92170721292496
time_elpased: 4.867
batch start
#iterations: 323
currently lose_sum: 372.9750967621803
time_elpased: 4.883
batch start
#iterations: 324
currently lose_sum: 371.2525961995125
time_elpased: 4.906
batch start
#iterations: 325
currently lose_sum: 372.41724717617035
time_elpased: 4.948
batch start
#iterations: 326
currently lose_sum: 372.7389008998871
time_elpased: 4.935
batch start
#iterations: 327
currently lose_sum: 371.6248503923416
time_elpased: 4.901
batch start
#iterations: 328
currently lose_sum: 371.88218528032303
time_elpased: 4.924
batch start
#iterations: 329
currently lose_sum: 371.1359769105911
time_elpased: 4.954
batch start
#iterations: 330
currently lose_sum: 372.1623999476433
time_elpased: 4.917
batch start
#iterations: 331
currently lose_sum: 371.74811536073685
time_elpased: 4.919
batch start
#iterations: 332
currently lose_sum: 372.3544192314148
time_elpased: 4.932
batch start
#iterations: 333
currently lose_sum: 372.63559633493423
time_elpased: 4.945
batch start
#iterations: 334
currently lose_sum: 372.17636531591415
time_elpased: 4.946
batch start
#iterations: 335
currently lose_sum: 371.4126260280609
time_elpased: 4.913
batch start
#iterations: 336
currently lose_sum: 373.0781167149544
time_elpased: 4.966
batch start
#iterations: 337
currently lose_sum: 370.911159992218
time_elpased: 4.96
batch start
#iterations: 338
currently lose_sum: 372.4238083958626
time_elpased: 4.955
batch start
#iterations: 339
currently lose_sum: 372.1427213549614
time_elpased: 4.933
start validation test
0.640618556701
1.0
0.640618556701
0.780947593314
0
validation finish
batch start
#iterations: 340
currently lose_sum: 372.4793310761452
time_elpased: 4.934
batch start
#iterations: 341
currently lose_sum: 372.86397153139114
time_elpased: 4.959
batch start
#iterations: 342
currently lose_sum: 372.01978051662445
time_elpased: 4.93
batch start
#iterations: 343
currently lose_sum: 371.5242661833763
time_elpased: 4.89
batch start
#iterations: 344
currently lose_sum: 371.4899398088455
time_elpased: 4.928
batch start
#iterations: 345
currently lose_sum: 371.9342239499092
time_elpased: 4.944
batch start
#iterations: 346
currently lose_sum: 372.2701667547226
time_elpased: 4.938
batch start
#iterations: 347
currently lose_sum: 372.98234355449677
time_elpased: 4.91
batch start
#iterations: 348
currently lose_sum: 370.3219555616379
time_elpased: 4.891
batch start
#iterations: 349
currently lose_sum: 372.0053696036339
time_elpased: 4.939
batch start
#iterations: 350
currently lose_sum: 372.2234224677086
time_elpased: 4.917
batch start
#iterations: 351
currently lose_sum: 372.3090860247612
time_elpased: 4.897
batch start
#iterations: 352
currently lose_sum: 372.8095918893814
time_elpased: 4.903
batch start
#iterations: 353
currently lose_sum: 371.9996343255043
time_elpased: 4.923
batch start
#iterations: 354
currently lose_sum: 371.4995211958885
time_elpased: 4.891
batch start
#iterations: 355
currently lose_sum: 371.1788856983185
time_elpased: 4.922
batch start
#iterations: 356
currently lose_sum: 371.0433279275894
time_elpased: 4.894
batch start
#iterations: 357
currently lose_sum: 372.2116487622261
time_elpased: 4.903
batch start
#iterations: 358
currently lose_sum: 371.5798559784889
time_elpased: 4.907
batch start
#iterations: 359
currently lose_sum: 370.2267037034035
time_elpased: 4.879
start validation test
0.611340206186
1.0
0.611340206186
0.758797184901
0
validation finish
batch start
#iterations: 360
currently lose_sum: 372.33215749263763
time_elpased: 4.831
batch start
#iterations: 361
currently lose_sum: 372.37386804819107
time_elpased: 4.825
batch start
#iterations: 362
currently lose_sum: 371.9061349630356
time_elpased: 4.883
batch start
#iterations: 363
currently lose_sum: 373.5266116261482
time_elpased: 4.912
batch start
#iterations: 364
currently lose_sum: 371.0190243124962
time_elpased: 4.88
batch start
#iterations: 365
currently lose_sum: 371.7492400407791
time_elpased: 4.846
batch start
#iterations: 366
currently lose_sum: 372.8590870499611
time_elpased: 4.855
batch start
#iterations: 367
currently lose_sum: 372.8947076201439
time_elpased: 4.878
batch start
#iterations: 368
currently lose_sum: 371.43585300445557
time_elpased: 4.909
batch start
#iterations: 369
currently lose_sum: 370.4770069718361
time_elpased: 4.882
batch start
#iterations: 370
currently lose_sum: 371.03899973630905
time_elpased: 4.886
batch start
#iterations: 371
currently lose_sum: 372.02249920368195
time_elpased: 5.117
batch start
#iterations: 372
currently lose_sum: 371.57463228702545
time_elpased: 5.392
batch start
#iterations: 373
currently lose_sum: 372.58526933193207
time_elpased: 5.4
batch start
#iterations: 374
currently lose_sum: 372.3144342303276
time_elpased: 5.384
batch start
#iterations: 375
currently lose_sum: 370.30171298980713
time_elpased: 5.1
batch start
#iterations: 376
currently lose_sum: 371.67651373147964
time_elpased: 5.017
batch start
#iterations: 377
currently lose_sum: 372.2827187180519
time_elpased: 4.988
batch start
#iterations: 378
currently lose_sum: 371.6856419444084
time_elpased: 4.963
batch start
#iterations: 379
currently lose_sum: 372.1744248867035
time_elpased: 4.969
start validation test
0.642783505155
1.0
0.642783505155
0.782554126137
0
validation finish
batch start
#iterations: 380
currently lose_sum: 373.5105969309807
time_elpased: 4.96
batch start
#iterations: 381
currently lose_sum: 373.14756721258163
time_elpased: 4.968
batch start
#iterations: 382
currently lose_sum: 372.45704811811447
time_elpased: 4.961
batch start
#iterations: 383
currently lose_sum: 371.8257118463516
time_elpased: 4.989
batch start
#iterations: 384
currently lose_sum: 371.63100016117096
time_elpased: 4.937
batch start
#iterations: 385
currently lose_sum: 371.4932352900505
time_elpased: 4.968
batch start
#iterations: 386
currently lose_sum: 372.5745584368706
time_elpased: 4.976
batch start
#iterations: 387
currently lose_sum: 371.31746822595596
time_elpased: 4.951
batch start
#iterations: 388
currently lose_sum: 371.06429398059845
time_elpased: 4.952
batch start
#iterations: 389
currently lose_sum: 372.05824011564255
time_elpased: 4.967
batch start
#iterations: 390
currently lose_sum: 370.7351195216179
time_elpased: 4.959
batch start
#iterations: 391
currently lose_sum: 371.2466372847557
time_elpased: 4.965
batch start
#iterations: 392
currently lose_sum: 370.76333260536194
time_elpased: 4.954
batch start
#iterations: 393
currently lose_sum: 372.3874641060829
time_elpased: 4.959
batch start
#iterations: 394
currently lose_sum: 371.9947088956833
time_elpased: 4.979
batch start
#iterations: 395
currently lose_sum: 372.2582414150238
time_elpased: 4.966
batch start
#iterations: 396
currently lose_sum: 372.1443291902542
time_elpased: 4.954
batch start
#iterations: 397
currently lose_sum: 371.08450973033905
time_elpased: 4.954
batch start
#iterations: 398
currently lose_sum: 371.9406823515892
time_elpased: 4.957
batch start
#iterations: 399
currently lose_sum: 370.5146679878235
time_elpased: 4.983
start validation test
0.662577319588
1.0
0.662577319588
0.797048428102
0
validation finish
acc: 0.701
pre: 1.000
rec: 0.701
F1: 0.824
auc: 0.000
