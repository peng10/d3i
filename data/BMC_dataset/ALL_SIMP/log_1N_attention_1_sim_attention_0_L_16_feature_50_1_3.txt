start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 405.05479621887207
time_elpased: 2.422
batch start
#iterations: 1
currently lose_sum: 403.108922958374
time_elpased: 2.294
batch start
#iterations: 2
currently lose_sum: 402.712129175663
time_elpased: 2.319
batch start
#iterations: 3
currently lose_sum: 402.26590770483017
time_elpased: 2.319
batch start
#iterations: 4
currently lose_sum: 401.82749116420746
time_elpased: 2.311
batch start
#iterations: 5
currently lose_sum: 401.48304879665375
time_elpased: 2.392
batch start
#iterations: 6
currently lose_sum: 400.8932981491089
time_elpased: 2.289
batch start
#iterations: 7
currently lose_sum: 400.17778158187866
time_elpased: 2.386
batch start
#iterations: 8
currently lose_sum: 400.0390213727951
time_elpased: 2.424
batch start
#iterations: 9
currently lose_sum: 398.9748626947403
time_elpased: 2.356
batch start
#iterations: 10
currently lose_sum: 396.9319403767586
time_elpased: 2.355
batch start
#iterations: 11
currently lose_sum: 395.8065285086632
time_elpased: 2.3
batch start
#iterations: 12
currently lose_sum: 393.8373712301254
time_elpased: 2.316
batch start
#iterations: 13
currently lose_sum: 393.450051009655
time_elpased: 2.362
batch start
#iterations: 14
currently lose_sum: 392.495603621006
time_elpased: 2.312
batch start
#iterations: 15
currently lose_sum: 390.8282071352005
time_elpased: 2.395
batch start
#iterations: 16
currently lose_sum: 390.96441674232483
time_elpased: 2.296
batch start
#iterations: 17
currently lose_sum: 390.94142377376556
time_elpased: 2.309
batch start
#iterations: 18
currently lose_sum: 388.9958572983742
time_elpased: 2.344
batch start
#iterations: 19
currently lose_sum: 387.9287719130516
time_elpased: 2.29
start validation test
0.680927835052
1.0
0.680927835052
0.810180926096
0
validation finish
batch start
#iterations: 20
currently lose_sum: 387.7651143670082
time_elpased: 2.358
batch start
#iterations: 21
currently lose_sum: 386.96833235025406
time_elpased: 2.288
batch start
#iterations: 22
currently lose_sum: 386.73455715179443
time_elpased: 2.294
batch start
#iterations: 23
currently lose_sum: 385.38513308763504
time_elpased: 2.352
batch start
#iterations: 24
currently lose_sum: 386.0801389813423
time_elpased: 2.288
batch start
#iterations: 25
currently lose_sum: 386.18103075027466
time_elpased: 2.302
batch start
#iterations: 26
currently lose_sum: 384.019229888916
time_elpased: 2.353
batch start
#iterations: 27
currently lose_sum: 384.7354109287262
time_elpased: 2.326
batch start
#iterations: 28
currently lose_sum: 383.64893597364426
time_elpased: 2.303
batch start
#iterations: 29
currently lose_sum: 384.3536806702614
time_elpased: 2.303
batch start
#iterations: 30
currently lose_sum: 383.3700236082077
time_elpased: 2.319
batch start
#iterations: 31
currently lose_sum: 383.66090017557144
time_elpased: 2.392
batch start
#iterations: 32
currently lose_sum: 382.6026762723923
time_elpased: 2.383
batch start
#iterations: 33
currently lose_sum: 383.91552752256393
time_elpased: 2.4
batch start
#iterations: 34
currently lose_sum: 382.2759262919426
time_elpased: 2.345
batch start
#iterations: 35
currently lose_sum: 382.4791486263275
time_elpased: 2.285
batch start
#iterations: 36
currently lose_sum: 381.523530960083
time_elpased: 2.372
batch start
#iterations: 37
currently lose_sum: 380.3598615527153
time_elpased: 2.315
batch start
#iterations: 38
currently lose_sum: 380.34994810819626
time_elpased: 2.407
batch start
#iterations: 39
currently lose_sum: 380.9687200188637
time_elpased: 2.333
start validation test
0.713711340206
1.0
0.713711340206
0.832942308849
0
validation finish
batch start
#iterations: 40
currently lose_sum: 380.29980462789536
time_elpased: 2.28
batch start
#iterations: 41
currently lose_sum: 380.44174271821976
time_elpased: 2.331
batch start
#iterations: 42
currently lose_sum: 380.5764749646187
time_elpased: 2.353
batch start
#iterations: 43
currently lose_sum: 379.62216037511826
time_elpased: 2.328
batch start
#iterations: 44
currently lose_sum: 379.6380094885826
time_elpased: 2.402
batch start
#iterations: 45
currently lose_sum: 379.81890285015106
time_elpased: 2.419
batch start
#iterations: 46
currently lose_sum: 380.17658245563507
time_elpased: 2.305
batch start
#iterations: 47
currently lose_sum: 379.3858355283737
time_elpased: 2.315
batch start
#iterations: 48
currently lose_sum: 378.9616509079933
time_elpased: 2.271
batch start
#iterations: 49
currently lose_sum: 378.98526018857956
time_elpased: 2.303
batch start
#iterations: 50
currently lose_sum: 380.19727754592896
time_elpased: 2.308
batch start
#iterations: 51
currently lose_sum: 379.3250871896744
time_elpased: 2.318
batch start
#iterations: 52
currently lose_sum: 378.4117366075516
time_elpased: 2.299
batch start
#iterations: 53
currently lose_sum: 378.33717489242554
time_elpased: 2.374
batch start
#iterations: 54
currently lose_sum: 377.7851197719574
time_elpased: 2.376
batch start
#iterations: 55
currently lose_sum: 377.2293248772621
time_elpased: 2.454
batch start
#iterations: 56
currently lose_sum: 377.5550106167793
time_elpased: 2.293
batch start
#iterations: 57
currently lose_sum: 377.41230630874634
time_elpased: 2.304
batch start
#iterations: 58
currently lose_sum: 376.8470980525017
time_elpased: 2.353
batch start
#iterations: 59
currently lose_sum: 378.50045228004456
time_elpased: 2.311
start validation test
0.648453608247
1.0
0.648453608247
0.786741713571
0
validation finish
batch start
#iterations: 60
currently lose_sum: 376.26508474349976
time_elpased: 2.371
batch start
#iterations: 61
currently lose_sum: 376.46149945259094
time_elpased: 2.307
batch start
#iterations: 62
currently lose_sum: 375.96233850717545
time_elpased: 2.355
batch start
#iterations: 63
currently lose_sum: 375.8100743293762
time_elpased: 2.37
batch start
#iterations: 64
currently lose_sum: 376.76050198078156
time_elpased: 2.384
batch start
#iterations: 65
currently lose_sum: 375.89751321077347
time_elpased: 2.359
batch start
#iterations: 66
currently lose_sum: 376.45544147491455
time_elpased: 2.313
batch start
#iterations: 67
currently lose_sum: 376.0701211094856
time_elpased: 2.384
batch start
#iterations: 68
currently lose_sum: 374.7405785918236
time_elpased: 2.336
batch start
#iterations: 69
currently lose_sum: 375.0063872933388
time_elpased: 2.328
batch start
#iterations: 70
currently lose_sum: 374.961345911026
time_elpased: 2.339
batch start
#iterations: 71
currently lose_sum: 374.9927827715874
time_elpased: 2.338
batch start
#iterations: 72
currently lose_sum: 373.0296572446823
time_elpased: 2.311
batch start
#iterations: 73
currently lose_sum: 375.30375903844833
time_elpased: 2.457
batch start
#iterations: 74
currently lose_sum: 374.46905279159546
time_elpased: 2.391
batch start
#iterations: 75
currently lose_sum: 373.91974288225174
time_elpased: 2.325
batch start
#iterations: 76
currently lose_sum: 374.2589105963707
time_elpased: 2.358
batch start
#iterations: 77
currently lose_sum: 373.5045033097267
time_elpased: 2.381
batch start
#iterations: 78
currently lose_sum: 372.67968463897705
time_elpased: 2.383
batch start
#iterations: 79
currently lose_sum: 371.6708117723465
time_elpased: 2.311
start validation test
0.747628865979
1.0
0.747628865979
0.8555922605
0
validation finish
batch start
#iterations: 80
currently lose_sum: 373.3044925928116
time_elpased: 2.357
batch start
#iterations: 81
currently lose_sum: 373.6969558596611
time_elpased: 2.293
batch start
#iterations: 82
currently lose_sum: 374.7751319408417
time_elpased: 2.337
batch start
#iterations: 83
currently lose_sum: 372.8572043776512
time_elpased: 2.406
batch start
#iterations: 84
currently lose_sum: 373.3561758995056
time_elpased: 2.371
batch start
#iterations: 85
currently lose_sum: 372.94101160764694
time_elpased: 2.362
batch start
#iterations: 86
currently lose_sum: 372.8433766365051
time_elpased: 2.25
batch start
#iterations: 87
currently lose_sum: 373.777043402195
time_elpased: 2.287
batch start
#iterations: 88
currently lose_sum: 371.11719965934753
time_elpased: 2.373
batch start
#iterations: 89
currently lose_sum: 372.8357573747635
time_elpased: 2.305
batch start
#iterations: 90
currently lose_sum: 371.6411870121956
time_elpased: 2.314
batch start
#iterations: 91
currently lose_sum: 372.39745157957077
time_elpased: 2.353
batch start
#iterations: 92
currently lose_sum: 372.35660672187805
time_elpased: 2.3
batch start
#iterations: 93
currently lose_sum: 374.0340287089348
time_elpased: 2.334
batch start
#iterations: 94
currently lose_sum: 373.0237688422203
time_elpased: 2.352
batch start
#iterations: 95
currently lose_sum: 370.68401074409485
time_elpased: 2.205
batch start
#iterations: 96
currently lose_sum: 372.0348446369171
time_elpased: 2.348
batch start
#iterations: 97
currently lose_sum: 370.7193775177002
time_elpased: 2.289
batch start
#iterations: 98
currently lose_sum: 370.7115977406502
time_elpased: 2.331
batch start
#iterations: 99
currently lose_sum: 371.97298365831375
time_elpased: 2.318
start validation test
0.770412371134
1.0
0.770412371134
0.870319687882
0
validation finish
batch start
#iterations: 100
currently lose_sum: 372.5092507004738
time_elpased: 2.324
batch start
#iterations: 101
currently lose_sum: 373.58092683553696
time_elpased: 2.399
batch start
#iterations: 102
currently lose_sum: 370.146928191185
time_elpased: 2.349
batch start
#iterations: 103
currently lose_sum: 371.0567364692688
time_elpased: 2.33
batch start
#iterations: 104
currently lose_sum: 371.7934848666191
time_elpased: 2.356
batch start
#iterations: 105
currently lose_sum: 370.345055103302
time_elpased: 2.331
batch start
#iterations: 106
currently lose_sum: 371.64065915346146
time_elpased: 2.315
batch start
#iterations: 107
currently lose_sum: 370.12812745571136
time_elpased: 2.37
batch start
#iterations: 108
currently lose_sum: 370.6688176393509
time_elpased: 2.298
batch start
#iterations: 109
currently lose_sum: 371.07217502593994
time_elpased: 2.375
batch start
#iterations: 110
currently lose_sum: 370.77975165843964
time_elpased: 2.317
batch start
#iterations: 111
currently lose_sum: 370.60569620132446
time_elpased: 2.319
batch start
#iterations: 112
currently lose_sum: 369.9611629843712
time_elpased: 2.32
batch start
#iterations: 113
currently lose_sum: 370.5654282569885
time_elpased: 2.366
batch start
#iterations: 114
currently lose_sum: 372.73167914152145
time_elpased: 2.343
batch start
#iterations: 115
currently lose_sum: 369.7671558856964
time_elpased: 2.322
batch start
#iterations: 116
currently lose_sum: 369.9685611128807
time_elpased: 2.333
batch start
#iterations: 117
currently lose_sum: 369.83945870399475
time_elpased: 2.36
batch start
#iterations: 118
currently lose_sum: 369.7273976802826
time_elpased: 2.268
batch start
#iterations: 119
currently lose_sum: 369.8165371417999
time_elpased: 2.36
start validation test
0.73618556701
1.0
0.73618556701
0.848049403242
0
validation finish
batch start
#iterations: 120
currently lose_sum: 369.91046196222305
time_elpased: 2.333
batch start
#iterations: 121
currently lose_sum: 370.10264706611633
time_elpased: 2.272
batch start
#iterations: 122
currently lose_sum: 370.67087668180466
time_elpased: 2.398
batch start
#iterations: 123
currently lose_sum: 368.7906612753868
time_elpased: 2.291
batch start
#iterations: 124
currently lose_sum: 368.48743134737015
time_elpased: 2.296
batch start
#iterations: 125
currently lose_sum: 369.95767456293106
time_elpased: 2.311
batch start
#iterations: 126
currently lose_sum: 369.5090186595917
time_elpased: 2.299
batch start
#iterations: 127
currently lose_sum: 370.32831913232803
time_elpased: 2.341
batch start
#iterations: 128
currently lose_sum: 371.77888238430023
time_elpased: 2.351
batch start
#iterations: 129
currently lose_sum: 369.02983444929123
time_elpased: 2.343
batch start
#iterations: 130
currently lose_sum: 368.2147943973541
time_elpased: 2.329
batch start
#iterations: 131
currently lose_sum: 370.6862191557884
time_elpased: 2.393
batch start
#iterations: 132
currently lose_sum: 370.2389277815819
time_elpased: 2.424
batch start
#iterations: 133
currently lose_sum: 371.0611508488655
time_elpased: 2.327
batch start
#iterations: 134
currently lose_sum: 369.223436832428
time_elpased: 2.301
batch start
#iterations: 135
currently lose_sum: 369.6565673351288
time_elpased: 2.292
batch start
#iterations: 136
currently lose_sum: 368.12834841012955
time_elpased: 2.362
batch start
#iterations: 137
currently lose_sum: 368.6769136786461
time_elpased: 2.345
batch start
#iterations: 138
currently lose_sum: 370.2165720462799
time_elpased: 2.362
batch start
#iterations: 139
currently lose_sum: 368.06948524713516
time_elpased: 2.283
start validation test
0.705360824742
1.0
0.705360824742
0.827227662919
0
validation finish
batch start
#iterations: 140
currently lose_sum: 367.93262243270874
time_elpased: 2.403
batch start
#iterations: 141
currently lose_sum: 368.75296503305435
time_elpased: 2.307
batch start
#iterations: 142
currently lose_sum: 367.0984805226326
time_elpased: 2.367
batch start
#iterations: 143
currently lose_sum: 367.2292791604996
time_elpased: 2.339
batch start
#iterations: 144
currently lose_sum: 367.9147678017616
time_elpased: 2.34
batch start
#iterations: 145
currently lose_sum: 369.75320732593536
time_elpased: 2.377
batch start
#iterations: 146
currently lose_sum: 366.23138093948364
time_elpased: 2.39
batch start
#iterations: 147
currently lose_sum: 370.5423744916916
time_elpased: 2.158
batch start
#iterations: 148
currently lose_sum: 368.7941697239876
time_elpased: 2.383
batch start
#iterations: 149
currently lose_sum: 368.31335896253586
time_elpased: 2.301
batch start
#iterations: 150
currently lose_sum: 368.0300924181938
time_elpased: 2.3
batch start
#iterations: 151
currently lose_sum: 367.9992919564247
time_elpased: 2.301
batch start
#iterations: 152
currently lose_sum: 366.92582380771637
time_elpased: 2.393
batch start
#iterations: 153
currently lose_sum: 368.4094030857086
time_elpased: 2.348
batch start
#iterations: 154
currently lose_sum: 369.3328638076782
time_elpased: 2.324
batch start
#iterations: 155
currently lose_sum: 368.93850260972977
time_elpased: 2.377
batch start
#iterations: 156
currently lose_sum: 369.2779769897461
time_elpased: 2.361
batch start
#iterations: 157
currently lose_sum: 370.0594811439514
time_elpased: 2.328
batch start
#iterations: 158
currently lose_sum: 367.69561183452606
time_elpased: 2.324
batch start
#iterations: 159
currently lose_sum: 367.9203517436981
time_elpased: 2.348
start validation test
0.764432989691
1.0
0.764432989691
0.866491381829
0
validation finish
batch start
#iterations: 160
currently lose_sum: 368.5364999175072
time_elpased: 2.353
batch start
#iterations: 161
currently lose_sum: 368.6477605700493
time_elpased: 2.352
batch start
#iterations: 162
currently lose_sum: 368.20540207624435
time_elpased: 2.352
batch start
#iterations: 163
currently lose_sum: 367.8705204129219
time_elpased: 2.376
batch start
#iterations: 164
currently lose_sum: 368.73214733600616
time_elpased: 2.283
batch start
#iterations: 165
currently lose_sum: 367.1723338365555
time_elpased: 2.28
batch start
#iterations: 166
currently lose_sum: 368.7416526079178
time_elpased: 2.364
batch start
#iterations: 167
currently lose_sum: 367.9701140522957
time_elpased: 2.372
batch start
#iterations: 168
currently lose_sum: 369.5683795809746
time_elpased: 2.381
batch start
#iterations: 169
currently lose_sum: 367.8645120859146
time_elpased: 2.337
batch start
#iterations: 170
currently lose_sum: 368.04392969608307
time_elpased: 2.337
batch start
#iterations: 171
currently lose_sum: 366.57961040735245
time_elpased: 2.351
batch start
#iterations: 172
currently lose_sum: 367.0724541544914
time_elpased: 2.352
batch start
#iterations: 173
currently lose_sum: 367.85735231637955
time_elpased: 2.382
batch start
#iterations: 174
currently lose_sum: 367.202481508255
time_elpased: 2.365
batch start
#iterations: 175
currently lose_sum: 367.3444318175316
time_elpased: 2.364
batch start
#iterations: 176
currently lose_sum: 367.5015715956688
time_elpased: 2.416
batch start
#iterations: 177
currently lose_sum: 367.72740095853806
time_elpased: 2.345
batch start
#iterations: 178
currently lose_sum: 367.19622004032135
time_elpased: 2.352
batch start
#iterations: 179
currently lose_sum: 366.6287006139755
time_elpased: 2.345
start validation test
0.763505154639
1.0
0.763505154639
0.8658950076
0
validation finish
batch start
#iterations: 180
currently lose_sum: 367.28454726934433
time_elpased: 2.397
batch start
#iterations: 181
currently lose_sum: 367.21268624067307
time_elpased: 2.384
batch start
#iterations: 182
currently lose_sum: 369.17351657152176
time_elpased: 2.338
batch start
#iterations: 183
currently lose_sum: 367.82370179891586
time_elpased: 2.301
batch start
#iterations: 184
currently lose_sum: 367.2592725157738
time_elpased: 2.299
batch start
#iterations: 185
currently lose_sum: 366.522870182991
time_elpased: 2.358
batch start
#iterations: 186
currently lose_sum: 368.1952640414238
time_elpased: 2.322
batch start
#iterations: 187
currently lose_sum: 367.161907017231
time_elpased: 2.392
batch start
#iterations: 188
currently lose_sum: 366.0741009116173
time_elpased: 2.308
batch start
#iterations: 189
currently lose_sum: 368.06624537706375
time_elpased: 2.375
batch start
#iterations: 190
currently lose_sum: 367.68666046857834
time_elpased: 2.308
batch start
#iterations: 191
currently lose_sum: 368.0328985452652
time_elpased: 2.354
batch start
#iterations: 192
currently lose_sum: 367.59092223644257
time_elpased: 2.437
batch start
#iterations: 193
currently lose_sum: 367.18874335289
time_elpased: 2.358
batch start
#iterations: 194
currently lose_sum: 366.2495730519295
time_elpased: 2.369
batch start
#iterations: 195
currently lose_sum: 366.0048754811287
time_elpased: 2.367
batch start
#iterations: 196
currently lose_sum: 367.5282754302025
time_elpased: 2.418
batch start
#iterations: 197
currently lose_sum: 368.36850303411484
time_elpased: 2.35
batch start
#iterations: 198
currently lose_sum: 367.4045106768608
time_elpased: 2.35
batch start
#iterations: 199
currently lose_sum: 367.21387881040573
time_elpased: 2.299
start validation test
0.757628865979
1.0
0.757628865979
0.86210334917
0
validation finish
batch start
#iterations: 200
currently lose_sum: 365.1796957850456
time_elpased: 2.366
batch start
#iterations: 201
currently lose_sum: 368.10307812690735
time_elpased: 2.309
batch start
#iterations: 202
currently lose_sum: 368.026058614254
time_elpased: 2.315
batch start
#iterations: 203
currently lose_sum: 366.65088057518005
time_elpased: 2.346
batch start
#iterations: 204
currently lose_sum: 366.7686424255371
time_elpased: 2.336
batch start
#iterations: 205
currently lose_sum: 366.67515456676483
time_elpased: 2.387
batch start
#iterations: 206
currently lose_sum: 366.51348102092743
time_elpased: 2.33
batch start
#iterations: 207
currently lose_sum: 365.3291404247284
time_elpased: 2.335
batch start
#iterations: 208
currently lose_sum: 367.9726625084877
time_elpased: 2.368
batch start
#iterations: 209
currently lose_sum: 364.74514403939247
time_elpased: 2.368
batch start
#iterations: 210
currently lose_sum: 367.09213918447495
time_elpased: 2.356
batch start
#iterations: 211
currently lose_sum: 365.03462678194046
time_elpased: 2.332
batch start
#iterations: 212
currently lose_sum: 366.2978177666664
time_elpased: 2.279
batch start
#iterations: 213
currently lose_sum: 366.759791970253
time_elpased: 2.386
batch start
#iterations: 214
currently lose_sum: 367.38935446739197
time_elpased: 2.3
batch start
#iterations: 215
currently lose_sum: 365.3515235185623
time_elpased: 2.387
batch start
#iterations: 216
currently lose_sum: 366.58468502759933
time_elpased: 2.394
batch start
#iterations: 217
currently lose_sum: 368.18345606327057
time_elpased: 2.286
batch start
#iterations: 218
currently lose_sum: 364.9859842658043
time_elpased: 2.357
batch start
#iterations: 219
currently lose_sum: 367.559670984745
time_elpased: 2.326
start validation test
0.749175257732
1.0
0.749175257732
0.856603995992
0
validation finish
batch start
#iterations: 220
currently lose_sum: 366.11429023742676
time_elpased: 2.356
batch start
#iterations: 221
currently lose_sum: 366.02018719911575
time_elpased: 2.375
batch start
#iterations: 222
currently lose_sum: 365.8894764184952
time_elpased: 2.311
batch start
#iterations: 223
currently lose_sum: 366.2203960418701
time_elpased: 2.39
batch start
#iterations: 224
currently lose_sum: 364.6157821416855
time_elpased: 2.366
batch start
#iterations: 225
currently lose_sum: 365.74479454755783
time_elpased: 2.344
batch start
#iterations: 226
currently lose_sum: 366.5522789955139
time_elpased: 2.361
batch start
#iterations: 227
currently lose_sum: 366.5556566119194
time_elpased: 2.327
batch start
#iterations: 228
currently lose_sum: 364.64490634202957
time_elpased: 2.41
batch start
#iterations: 229
currently lose_sum: 365.41258931159973
time_elpased: 2.297
batch start
#iterations: 230
currently lose_sum: 366.5010038614273
time_elpased: 2.412
batch start
#iterations: 231
currently lose_sum: 367.32909297943115
time_elpased: 2.409
batch start
#iterations: 232
currently lose_sum: 366.58230620622635
time_elpased: 2.311
batch start
#iterations: 233
currently lose_sum: 364.69290989637375
time_elpased: 2.416
batch start
#iterations: 234
currently lose_sum: 364.40436470508575
time_elpased: 2.372
batch start
#iterations: 235
currently lose_sum: 365.997111082077
time_elpased: 2.305
batch start
#iterations: 236
currently lose_sum: 366.23342990875244
time_elpased: 2.317
batch start
#iterations: 237
currently lose_sum: 367.2740177512169
time_elpased: 2.393
batch start
#iterations: 238
currently lose_sum: 366.3906940817833
time_elpased: 2.373
batch start
#iterations: 239
currently lose_sum: 365.76751363277435
time_elpased: 2.367
start validation test
0.699484536082
1.0
0.699484536082
0.823172581134
0
validation finish
batch start
#iterations: 240
currently lose_sum: 366.41522657871246
time_elpased: 2.377
batch start
#iterations: 241
currently lose_sum: 365.75073981285095
time_elpased: 2.402
batch start
#iterations: 242
currently lose_sum: 364.03209632635117
time_elpased: 2.323
batch start
#iterations: 243
currently lose_sum: 368.57076478004456
time_elpased: 2.339
batch start
#iterations: 244
currently lose_sum: 367.04538637399673
time_elpased: 2.305
batch start
#iterations: 245
currently lose_sum: 366.51792800426483
time_elpased: 2.323
batch start
#iterations: 246
currently lose_sum: 365.22638922929764
time_elpased: 2.342
batch start
#iterations: 247
currently lose_sum: 366.14467602968216
time_elpased: 2.337
batch start
#iterations: 248
currently lose_sum: 366.1249021887779
time_elpased: 2.323
batch start
#iterations: 249
currently lose_sum: 367.21142768859863
time_elpased: 2.401
batch start
#iterations: 250
currently lose_sum: 366.0296582579613
time_elpased: 2.269
batch start
#iterations: 251
currently lose_sum: 366.06606167554855
time_elpased: 2.279
batch start
#iterations: 252
currently lose_sum: 365.22067457437515
time_elpased: 2.398
batch start
#iterations: 253
currently lose_sum: 365.32260805368423
time_elpased: 2.364
batch start
#iterations: 254
currently lose_sum: 365.8285311460495
time_elpased: 2.308
batch start
#iterations: 255
currently lose_sum: 366.3554081916809
time_elpased: 2.421
batch start
#iterations: 256
currently lose_sum: 366.57211124897003
time_elpased: 2.307
batch start
#iterations: 257
currently lose_sum: 365.45824360847473
time_elpased: 2.367
batch start
#iterations: 258
currently lose_sum: 365.95251935720444
time_elpased: 2.371
batch start
#iterations: 259
currently lose_sum: 366.55376410484314
time_elpased: 2.412
start validation test
0.746082474227
1.0
0.746082474227
0.854578732952
0
validation finish
batch start
#iterations: 260
currently lose_sum: 365.660248875618
time_elpased: 2.329
batch start
#iterations: 261
currently lose_sum: 366.0273258090019
time_elpased: 2.367
batch start
#iterations: 262
currently lose_sum: 365.5734249949455
time_elpased: 2.398
batch start
#iterations: 263
currently lose_sum: 365.1069965362549
time_elpased: 2.326
batch start
#iterations: 264
currently lose_sum: 364.54625606536865
time_elpased: 2.33
batch start
#iterations: 265
currently lose_sum: 366.082301735878
time_elpased: 2.407
batch start
#iterations: 266
currently lose_sum: 364.865009367466
time_elpased: 2.33
batch start
#iterations: 267
currently lose_sum: 364.42613983154297
time_elpased: 2.342
batch start
#iterations: 268
currently lose_sum: 364.4778330922127
time_elpased: 2.33
batch start
#iterations: 269
currently lose_sum: 365.4340600967407
time_elpased: 2.318
batch start
#iterations: 270
currently lose_sum: 363.99141389131546
time_elpased: 2.37
batch start
#iterations: 271
currently lose_sum: 365.94197058677673
time_elpased: 2.299
batch start
#iterations: 272
currently lose_sum: 364.6870487332344
time_elpased: 2.373
batch start
#iterations: 273
currently lose_sum: 364.27980774641037
time_elpased: 2.36
batch start
#iterations: 274
currently lose_sum: 364.7648519873619
time_elpased: 2.271
batch start
#iterations: 275
currently lose_sum: 366.28389436006546
time_elpased: 2.329
batch start
#iterations: 276
currently lose_sum: 364.2834207415581
time_elpased: 2.358
batch start
#iterations: 277
currently lose_sum: 365.13398069143295
time_elpased: 2.406
batch start
#iterations: 278
currently lose_sum: 364.7336741089821
time_elpased: 2.369
batch start
#iterations: 279
currently lose_sum: 365.6306499838829
time_elpased: 2.303
start validation test
0.73824742268
1.0
0.73824742268
0.849415811636
0
validation finish
batch start
#iterations: 280
currently lose_sum: 363.9209398627281
time_elpased: 2.293
batch start
#iterations: 281
currently lose_sum: 364.2351514697075
time_elpased: 2.334
batch start
#iterations: 282
currently lose_sum: 365.2477937936783
time_elpased: 2.333
batch start
#iterations: 283
currently lose_sum: 363.5408014059067
time_elpased: 2.406
batch start
#iterations: 284
currently lose_sum: 365.8231267929077
time_elpased: 2.355
batch start
#iterations: 285
currently lose_sum: 364.915689766407
time_elpased: 2.42
batch start
#iterations: 286
currently lose_sum: 365.43535405397415
time_elpased: 2.35
batch start
#iterations: 287
currently lose_sum: 367.285276055336
time_elpased: 2.357
batch start
#iterations: 288
currently lose_sum: 365.3282524943352
time_elpased: 2.362
batch start
#iterations: 289
currently lose_sum: 363.5929906964302
time_elpased: 2.272
batch start
#iterations: 290
currently lose_sum: 363.19972789287567
time_elpased: 2.309
batch start
#iterations: 291
currently lose_sum: 364.66232562065125
time_elpased: 2.499
batch start
#iterations: 292
currently lose_sum: 365.10764318704605
time_elpased: 2.314
batch start
#iterations: 293
currently lose_sum: 365.1728593111038
time_elpased: 2.415
batch start
#iterations: 294
currently lose_sum: 364.46178138256073
time_elpased: 2.302
batch start
#iterations: 295
currently lose_sum: 364.691358089447
time_elpased: 2.398
batch start
#iterations: 296
currently lose_sum: 364.0172930955887
time_elpased: 2.311
batch start
#iterations: 297
currently lose_sum: 365.01566714048386
time_elpased: 2.337
batch start
#iterations: 298
currently lose_sum: 365.8499472141266
time_elpased: 2.422
batch start
#iterations: 299
currently lose_sum: 363.68246269226074
time_elpased: 2.334
start validation test
0.764226804124
1.0
0.764226804124
0.866358908432
0
validation finish
batch start
#iterations: 300
currently lose_sum: 365.09942638874054
time_elpased: 2.375
batch start
#iterations: 301
currently lose_sum: 363.8848499059677
time_elpased: 2.433
batch start
#iterations: 302
currently lose_sum: 363.15408223867416
time_elpased: 2.222
batch start
#iterations: 303
currently lose_sum: 363.553368806839
time_elpased: 2.37
batch start
#iterations: 304
currently lose_sum: 364.1057072877884
time_elpased: 2.407
batch start
#iterations: 305
currently lose_sum: 364.8175703883171
time_elpased: 2.254
batch start
#iterations: 306
currently lose_sum: 364.90798485279083
time_elpased: 2.399
batch start
#iterations: 307
currently lose_sum: 364.806264936924
time_elpased: 2.404
batch start
#iterations: 308
currently lose_sum: 364.0304889678955
time_elpased: 2.388
batch start
#iterations: 309
currently lose_sum: 363.9125602245331
time_elpased: 2.307
batch start
#iterations: 310
currently lose_sum: 365.50338035821915
time_elpased: 2.367
batch start
#iterations: 311
currently lose_sum: 365.40300726890564
time_elpased: 2.355
batch start
#iterations: 312
currently lose_sum: 364.02880823612213
time_elpased: 2.349
batch start
#iterations: 313
currently lose_sum: 365.0391922593117
time_elpased: 2.364
batch start
#iterations: 314
currently lose_sum: 364.3512652516365
time_elpased: 2.382
batch start
#iterations: 315
currently lose_sum: 364.78295612335205
time_elpased: 2.411
batch start
#iterations: 316
currently lose_sum: 364.48340970277786
time_elpased: 2.459
batch start
#iterations: 317
currently lose_sum: 364.18819946050644
time_elpased: 2.368
batch start
#iterations: 318
currently lose_sum: 364.5960816144943
time_elpased: 2.364
batch start
#iterations: 319
currently lose_sum: 364.2104811668396
time_elpased: 2.367
start validation test
0.74175257732
1.0
0.74175257732
0.851731281444
0
validation finish
batch start
#iterations: 320
currently lose_sum: 363.6664835214615
time_elpased: 2.299
batch start
#iterations: 321
currently lose_sum: 363.5530138015747
time_elpased: 2.379
batch start
#iterations: 322
currently lose_sum: 363.46899485588074
time_elpased: 2.403
batch start
#iterations: 323
currently lose_sum: 364.5938637852669
time_elpased: 2.321
batch start
#iterations: 324
currently lose_sum: 361.61819607019424
time_elpased: 2.385
batch start
#iterations: 325
currently lose_sum: 363.22874307632446
time_elpased: 2.322
batch start
#iterations: 326
currently lose_sum: 363.75349324941635
time_elpased: 2.341
batch start
#iterations: 327
currently lose_sum: 364.22779709100723
time_elpased: 2.344
batch start
#iterations: 328
currently lose_sum: 365.13642686605453
time_elpased: 2.32
batch start
#iterations: 329
currently lose_sum: 366.27373492717743
time_elpased: 2.361
batch start
#iterations: 330
currently lose_sum: 363.80077034235
time_elpased: 2.336
batch start
#iterations: 331
currently lose_sum: 366.2749485373497
time_elpased: 2.338
batch start
#iterations: 332
currently lose_sum: 364.3496780395508
time_elpased: 2.349
batch start
#iterations: 333
currently lose_sum: 365.4083406329155
time_elpased: 2.356
batch start
#iterations: 334
currently lose_sum: 363.59687435626984
time_elpased: 2.444
batch start
#iterations: 335
currently lose_sum: 364.36551851034164
time_elpased: 2.345
batch start
#iterations: 336
currently lose_sum: 363.6616888642311
time_elpased: 2.333
batch start
#iterations: 337
currently lose_sum: 365.55482137203217
time_elpased: 2.382
batch start
#iterations: 338
currently lose_sum: 363.7323904931545
time_elpased: 2.349
batch start
#iterations: 339
currently lose_sum: 364.1593655347824
time_elpased: 2.38
start validation test
0.761546391753
1.0
0.761546391753
0.864633932229
0
validation finish
batch start
#iterations: 340
currently lose_sum: 363.51037842035294
time_elpased: 2.374
batch start
#iterations: 341
currently lose_sum: 363.3008009791374
time_elpased: 2.319
batch start
#iterations: 342
currently lose_sum: 364.898885011673
time_elpased: 2.325
batch start
#iterations: 343
currently lose_sum: 362.9448368549347
time_elpased: 2.259
batch start
#iterations: 344
currently lose_sum: 364.5874348282814
time_elpased: 2.391
batch start
#iterations: 345
currently lose_sum: 363.7597932815552
time_elpased: 2.35
batch start
#iterations: 346
currently lose_sum: 364.47551637887955
time_elpased: 2.33
batch start
#iterations: 347
currently lose_sum: 364.74442583322525
time_elpased: 2.343
batch start
#iterations: 348
currently lose_sum: 364.47804886102676
time_elpased: 2.479
batch start
#iterations: 349
currently lose_sum: 365.1536354422569
time_elpased: 2.369
batch start
#iterations: 350
currently lose_sum: 363.97348380088806
time_elpased: 2.326
batch start
#iterations: 351
currently lose_sum: 363.65640872716904
time_elpased: 2.285
batch start
#iterations: 352
currently lose_sum: 363.6087480187416
time_elpased: 2.395
batch start
#iterations: 353
currently lose_sum: 364.1344211101532
time_elpased: 2.327
batch start
#iterations: 354
currently lose_sum: 364.285576403141
time_elpased: 2.257
batch start
#iterations: 355
currently lose_sum: 362.4517123699188
time_elpased: 2.36
batch start
#iterations: 356
currently lose_sum: 364.22952967882156
time_elpased: 2.332
batch start
#iterations: 357
currently lose_sum: 362.56724750995636
time_elpased: 2.442
batch start
#iterations: 358
currently lose_sum: 363.4337994456291
time_elpased: 2.387
batch start
#iterations: 359
currently lose_sum: 363.22437930107117
time_elpased: 2.298
start validation test
0.767525773196
1.0
0.767525773196
0.868474773987
0
validation finish
batch start
#iterations: 360
currently lose_sum: 365.39394998550415
time_elpased: 2.328
batch start
#iterations: 361
currently lose_sum: 363.81083458662033
time_elpased: 2.304
batch start
#iterations: 362
currently lose_sum: 362.6572282910347
time_elpased: 2.358
batch start
#iterations: 363
currently lose_sum: 364.2508811354637
time_elpased: 2.424
batch start
#iterations: 364
currently lose_sum: 365.62585693597794
time_elpased: 2.34
batch start
#iterations: 365
currently lose_sum: 363.95966053009033
time_elpased: 2.42
batch start
#iterations: 366
currently lose_sum: 363.2021179199219
time_elpased: 2.38
batch start
#iterations: 367
currently lose_sum: 364.4039776325226
time_elpased: 2.364
batch start
#iterations: 368
currently lose_sum: 363.96964305639267
time_elpased: 2.367
batch start
#iterations: 369
currently lose_sum: 365.08946120738983
time_elpased: 2.339
batch start
#iterations: 370
currently lose_sum: 364.549154818058
time_elpased: 2.353
batch start
#iterations: 371
currently lose_sum: 363.9612720012665
time_elpased: 2.349
batch start
#iterations: 372
currently lose_sum: 363.45910453796387
time_elpased: 2.342
batch start
#iterations: 373
currently lose_sum: 363.2999567389488
time_elpased: 2.373
batch start
#iterations: 374
currently lose_sum: 364.7523075938225
time_elpased: 2.365
batch start
#iterations: 375
currently lose_sum: 364.3859622478485
time_elpased: 2.354
batch start
#iterations: 376
currently lose_sum: 364.3311402797699
time_elpased: 2.357
batch start
#iterations: 377
currently lose_sum: 363.53285378217697
time_elpased: 2.328
batch start
#iterations: 378
currently lose_sum: 364.5185207724571
time_elpased: 2.323
batch start
#iterations: 379
currently lose_sum: 363.40501952171326
time_elpased: 2.364
start validation test
0.761134020619
1.0
0.761134020619
0.864368085231
0
validation finish
batch start
#iterations: 380
currently lose_sum: 363.88336366415024
time_elpased: 2.392
batch start
#iterations: 381
currently lose_sum: 364.2588882446289
time_elpased: 2.36
batch start
#iterations: 382
currently lose_sum: 362.99108624458313
time_elpased: 2.354
batch start
#iterations: 383
currently lose_sum: 361.6349377632141
time_elpased: 2.358
batch start
#iterations: 384
currently lose_sum: 362.85343903303146
time_elpased: 2.345
batch start
#iterations: 385
currently lose_sum: 362.383642911911
time_elpased: 2.385
batch start
#iterations: 386
currently lose_sum: 363.7562199831009
time_elpased: 2.293
batch start
#iterations: 387
currently lose_sum: 363.1021480560303
time_elpased: 2.314
batch start
#iterations: 388
currently lose_sum: 364.56201285123825
time_elpased: 2.382
batch start
#iterations: 389
currently lose_sum: 362.9253631234169
time_elpased: 2.446
batch start
#iterations: 390
currently lose_sum: 362.74748319387436
time_elpased: 2.381
batch start
#iterations: 391
currently lose_sum: 362.87811982631683
time_elpased: 2.396
batch start
#iterations: 392
currently lose_sum: 363.5156292319298
time_elpased: 2.358
batch start
#iterations: 393
currently lose_sum: 364.79779267311096
time_elpased: 2.383
batch start
#iterations: 394
currently lose_sum: 362.47716838121414
time_elpased: 2.352
batch start
#iterations: 395
currently lose_sum: 363.8103495836258
time_elpased: 2.341
batch start
#iterations: 396
currently lose_sum: 363.3034038543701
time_elpased: 2.387
batch start
#iterations: 397
currently lose_sum: 363.89883303642273
time_elpased: 2.34
batch start
#iterations: 398
currently lose_sum: 363.8677605986595
time_elpased: 2.419
batch start
#iterations: 399
currently lose_sum: 363.8882405757904
time_elpased: 2.343
start validation test
0.758659793814
1.0
0.758659793814
0.862770385134
0
validation finish
acc: 0.782
pre: 1.000
rec: 0.782
F1: 0.878
auc: 0.000
