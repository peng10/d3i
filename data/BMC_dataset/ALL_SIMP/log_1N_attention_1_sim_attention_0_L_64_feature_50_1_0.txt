start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 404.80919218063354
time_elpased: 2.525
batch start
#iterations: 1
currently lose_sum: 403.4330368041992
time_elpased: 2.584
batch start
#iterations: 2
currently lose_sum: 402.8710275888443
time_elpased: 2.566
batch start
#iterations: 3
currently lose_sum: 402.13924366235733
time_elpased: 2.59
batch start
#iterations: 4
currently lose_sum: 402.09893095493317
time_elpased: 2.566
batch start
#iterations: 5
currently lose_sum: 402.40661495923996
time_elpased: 2.523
batch start
#iterations: 6
currently lose_sum: 401.695558488369
time_elpased: 2.564
batch start
#iterations: 7
currently lose_sum: 401.77131390571594
time_elpased: 2.488
batch start
#iterations: 8
currently lose_sum: 401.003604888916
time_elpased: 2.536
batch start
#iterations: 9
currently lose_sum: 399.96878319978714
time_elpased: 2.509
batch start
#iterations: 10
currently lose_sum: 398.1864442229271
time_elpased: 2.576
batch start
#iterations: 11
currently lose_sum: 397.4354728460312
time_elpased: 2.597
batch start
#iterations: 12
currently lose_sum: 396.3770461678505
time_elpased: 2.572
batch start
#iterations: 13
currently lose_sum: 394.95126873254776
time_elpased: 2.562
batch start
#iterations: 14
currently lose_sum: 393.2953197956085
time_elpased: 2.669
batch start
#iterations: 15
currently lose_sum: 392.6098138689995
time_elpased: 2.588
batch start
#iterations: 16
currently lose_sum: 392.2796624302864
time_elpased: 2.562
batch start
#iterations: 17
currently lose_sum: 392.28231877088547
time_elpased: 2.535
batch start
#iterations: 18
currently lose_sum: 390.0820447206497
time_elpased: 2.518
batch start
#iterations: 19
currently lose_sum: 389.8477860093117
time_elpased: 2.493
start validation test
0.286804123711
1.0
0.286804123711
0.445761897132
0
validation finish
batch start
#iterations: 20
currently lose_sum: 389.0307559967041
time_elpased: 2.584
batch start
#iterations: 21
currently lose_sum: 389.116932451725
time_elpased: 2.517
batch start
#iterations: 22
currently lose_sum: 388.88177835941315
time_elpased: 2.612
batch start
#iterations: 23
currently lose_sum: 388.3257048726082
time_elpased: 2.615
batch start
#iterations: 24
currently lose_sum: 387.094939827919
time_elpased: 2.551
batch start
#iterations: 25
currently lose_sum: 386.9975203871727
time_elpased: 2.555
batch start
#iterations: 26
currently lose_sum: 386.9357584118843
time_elpased: 2.481
batch start
#iterations: 27
currently lose_sum: 386.4178092479706
time_elpased: 2.589
batch start
#iterations: 28
currently lose_sum: 386.4448736310005
time_elpased: 2.52
batch start
#iterations: 29
currently lose_sum: 385.7354298233986
time_elpased: 2.524
batch start
#iterations: 30
currently lose_sum: 386.48455369472504
time_elpased: 2.532
batch start
#iterations: 31
currently lose_sum: 385.13571578264236
time_elpased: 2.551
batch start
#iterations: 32
currently lose_sum: 383.78589057922363
time_elpased: 2.474
batch start
#iterations: 33
currently lose_sum: 384.8987107872963
time_elpased: 2.508
batch start
#iterations: 34
currently lose_sum: 384.29199808835983
time_elpased: 2.595
batch start
#iterations: 35
currently lose_sum: 383.7196763753891
time_elpased: 2.492
batch start
#iterations: 36
currently lose_sum: 385.71602433919907
time_elpased: 2.612
batch start
#iterations: 37
currently lose_sum: 383.2353700995445
time_elpased: 2.566
batch start
#iterations: 38
currently lose_sum: 385.0064307451248
time_elpased: 2.548
batch start
#iterations: 39
currently lose_sum: 383.1782467365265
time_elpased: 2.576
start validation test
0.514948453608
1.0
0.514948453608
0.679823069071
0
validation finish
batch start
#iterations: 40
currently lose_sum: 382.13107949495316
time_elpased: 2.488
batch start
#iterations: 41
currently lose_sum: 382.2840539216995
time_elpased: 2.53
batch start
#iterations: 42
currently lose_sum: 382.0528390407562
time_elpased: 2.54
batch start
#iterations: 43
currently lose_sum: 382.0497200489044
time_elpased: 2.591
batch start
#iterations: 44
currently lose_sum: 383.2174997329712
time_elpased: 2.618
batch start
#iterations: 45
currently lose_sum: 381.62108075618744
time_elpased: 2.573
batch start
#iterations: 46
currently lose_sum: 380.161717236042
time_elpased: 2.621
batch start
#iterations: 47
currently lose_sum: 381.7671550512314
time_elpased: 2.507
batch start
#iterations: 48
currently lose_sum: 380.87951451539993
time_elpased: 2.594
batch start
#iterations: 49
currently lose_sum: 381.20895874500275
time_elpased: 2.575
batch start
#iterations: 50
currently lose_sum: 380.6032009124756
time_elpased: 2.553
batch start
#iterations: 51
currently lose_sum: 379.62557661533356
time_elpased: 2.509
batch start
#iterations: 52
currently lose_sum: 379.1139600276947
time_elpased: 2.563
batch start
#iterations: 53
currently lose_sum: 380.229288816452
time_elpased: 2.556
batch start
#iterations: 54
currently lose_sum: 379.2745923399925
time_elpased: 2.523
batch start
#iterations: 55
currently lose_sum: 379.9586865901947
time_elpased: 2.492
batch start
#iterations: 56
currently lose_sum: 378.5177335739136
time_elpased: 2.586
batch start
#iterations: 57
currently lose_sum: 379.70534086227417
time_elpased: 2.441
batch start
#iterations: 58
currently lose_sum: 380.4479179382324
time_elpased: 2.581
batch start
#iterations: 59
currently lose_sum: 378.90124905109406
time_elpased: 2.549
start validation test
0.681855670103
1.0
0.681855670103
0.810837317641
0
validation finish
batch start
#iterations: 60
currently lose_sum: 379.28926217556
time_elpased: 2.567
batch start
#iterations: 61
currently lose_sum: 377.3364872932434
time_elpased: 2.549
batch start
#iterations: 62
currently lose_sum: 377.97332203388214
time_elpased: 2.59
batch start
#iterations: 63
currently lose_sum: 378.27809220552444
time_elpased: 2.571
batch start
#iterations: 64
currently lose_sum: 377.4705408811569
time_elpased: 2.543
batch start
#iterations: 65
currently lose_sum: 376.7449294924736
time_elpased: 2.586
batch start
#iterations: 66
currently lose_sum: 378.8009970188141
time_elpased: 2.592
batch start
#iterations: 67
currently lose_sum: 377.3110477924347
time_elpased: 2.59
batch start
#iterations: 68
currently lose_sum: 376.4323042035103
time_elpased: 2.602
batch start
#iterations: 69
currently lose_sum: 376.942511677742
time_elpased: 2.532
batch start
#iterations: 70
currently lose_sum: 375.2990779876709
time_elpased: 2.562
batch start
#iterations: 71
currently lose_sum: 375.53115850687027
time_elpased: 2.524
batch start
#iterations: 72
currently lose_sum: 376.16265630722046
time_elpased: 2.556
batch start
#iterations: 73
currently lose_sum: 375.69221675395966
time_elpased: 2.538
batch start
#iterations: 74
currently lose_sum: 375.70552510023117
time_elpased: 2.542
batch start
#iterations: 75
currently lose_sum: 374.52732187509537
time_elpased: 2.54
batch start
#iterations: 76
currently lose_sum: 374.4581023454666
time_elpased: 2.501
batch start
#iterations: 77
currently lose_sum: 374.6979609131813
time_elpased: 2.568
batch start
#iterations: 78
currently lose_sum: 373.8664167523384
time_elpased: 2.505
batch start
#iterations: 79
currently lose_sum: 374.68599796295166
time_elpased: 2.638
start validation test
0.772474226804
1.0
0.772474226804
0.871633804455
0
validation finish
batch start
#iterations: 80
currently lose_sum: 374.32709312438965
time_elpased: 2.523
batch start
#iterations: 81
currently lose_sum: 375.4536520242691
time_elpased: 2.594
batch start
#iterations: 82
currently lose_sum: 374.6702483892441
time_elpased: 2.539
batch start
#iterations: 83
currently lose_sum: 373.1000815629959
time_elpased: 2.521
batch start
#iterations: 84
currently lose_sum: 373.00351840257645
time_elpased: 2.548
batch start
#iterations: 85
currently lose_sum: 373.20387464761734
time_elpased: 2.645
batch start
#iterations: 86
currently lose_sum: 373.14466547966003
time_elpased: 2.614
batch start
#iterations: 87
currently lose_sum: 372.7231693267822
time_elpased: 2.548
batch start
#iterations: 88
currently lose_sum: 373.27708607912064
time_elpased: 2.396
batch start
#iterations: 89
currently lose_sum: 372.1666864156723
time_elpased: 2.539
batch start
#iterations: 90
currently lose_sum: 372.7551265358925
time_elpased: 2.52
batch start
#iterations: 91
currently lose_sum: 372.52632862329483
time_elpased: 2.559
batch start
#iterations: 92
currently lose_sum: 372.1520446538925
time_elpased: 2.554
batch start
#iterations: 93
currently lose_sum: 373.94080382585526
time_elpased: 2.49
batch start
#iterations: 94
currently lose_sum: 372.7084800004959
time_elpased: 2.494
batch start
#iterations: 95
currently lose_sum: 371.84360605478287
time_elpased: 2.557
batch start
#iterations: 96
currently lose_sum: 371.62743347883224
time_elpased: 2.536
batch start
#iterations: 97
currently lose_sum: 372.2485681772232
time_elpased: 2.527
batch start
#iterations: 98
currently lose_sum: 371.9989035129547
time_elpased: 2.515
batch start
#iterations: 99
currently lose_sum: 371.78909534215927
time_elpased: 2.602
start validation test
0.737835051546
1.0
0.737835051546
0.849142789346
0
validation finish
batch start
#iterations: 100
currently lose_sum: 372.27966916561127
time_elpased: 2.562
batch start
#iterations: 101
currently lose_sum: 371.67241567373276
time_elpased: 2.584
batch start
#iterations: 102
currently lose_sum: 371.11346596479416
time_elpased: 2.615
batch start
#iterations: 103
currently lose_sum: 371.25509041547775
time_elpased: 2.527
batch start
#iterations: 104
currently lose_sum: 371.98118621110916
time_elpased: 2.549
batch start
#iterations: 105
currently lose_sum: 371.2954002022743
time_elpased: 2.495
batch start
#iterations: 106
currently lose_sum: 373.4789294600487
time_elpased: 2.553
batch start
#iterations: 107
currently lose_sum: 370.7718304991722
time_elpased: 2.546
batch start
#iterations: 108
currently lose_sum: 371.245574593544
time_elpased: 2.557
batch start
#iterations: 109
currently lose_sum: 371.2829256057739
time_elpased: 2.491
batch start
#iterations: 110
currently lose_sum: 370.44259893894196
time_elpased: 2.55
batch start
#iterations: 111
currently lose_sum: 370.29079788923264
time_elpased: 2.568
batch start
#iterations: 112
currently lose_sum: 370.8327647447586
time_elpased: 2.494
batch start
#iterations: 113
currently lose_sum: 372.21967601776123
time_elpased: 2.525
batch start
#iterations: 114
currently lose_sum: 371.5140211582184
time_elpased: 2.503
batch start
#iterations: 115
currently lose_sum: 372.8492406606674
time_elpased: 2.445
batch start
#iterations: 116
currently lose_sum: 370.2958912849426
time_elpased: 2.557
batch start
#iterations: 117
currently lose_sum: 369.6154885292053
time_elpased: 2.551
batch start
#iterations: 118
currently lose_sum: 370.84408926963806
time_elpased: 2.493
batch start
#iterations: 119
currently lose_sum: 372.0442425608635
time_elpased: 2.547
start validation test
0.769175257732
1.0
0.769175257732
0.869529747684
0
validation finish
batch start
#iterations: 120
currently lose_sum: 369.23710894584656
time_elpased: 2.556
batch start
#iterations: 121
currently lose_sum: 372.4289733171463
time_elpased: 2.451
batch start
#iterations: 122
currently lose_sum: 369.5992885828018
time_elpased: 2.555
batch start
#iterations: 123
currently lose_sum: 369.35011899471283
time_elpased: 2.555
batch start
#iterations: 124
currently lose_sum: 369.62248730659485
time_elpased: 2.567
batch start
#iterations: 125
currently lose_sum: 370.20678412914276
time_elpased: 2.565
batch start
#iterations: 126
currently lose_sum: 369.53953832387924
time_elpased: 2.497
batch start
#iterations: 127
currently lose_sum: 370.6806189417839
time_elpased: 2.604
batch start
#iterations: 128
currently lose_sum: 371.3332266807556
time_elpased: 2.643
batch start
#iterations: 129
currently lose_sum: 368.2306151986122
time_elpased: 2.639
batch start
#iterations: 130
currently lose_sum: 370.7651561498642
time_elpased: 2.651
batch start
#iterations: 131
currently lose_sum: 368.893014729023
time_elpased: 2.538
batch start
#iterations: 132
currently lose_sum: 372.12355345487595
time_elpased: 2.524
batch start
#iterations: 133
currently lose_sum: 369.2124927043915
time_elpased: 2.557
batch start
#iterations: 134
currently lose_sum: 370.0842860341072
time_elpased: 2.58
batch start
#iterations: 135
currently lose_sum: 369.6210318207741
time_elpased: 2.514
batch start
#iterations: 136
currently lose_sum: 370.6371833086014
time_elpased: 2.436
batch start
#iterations: 137
currently lose_sum: 368.0195787549019
time_elpased: 2.526
batch start
#iterations: 138
currently lose_sum: 370.91875088214874
time_elpased: 2.519
batch start
#iterations: 139
currently lose_sum: 369.1609281897545
time_elpased: 2.538
start validation test
0.793917525773
1.0
0.793917525773
0.885121544739
0
validation finish
batch start
#iterations: 140
currently lose_sum: 370.2940933704376
time_elpased: 2.524
batch start
#iterations: 141
currently lose_sum: 370.0335613489151
time_elpased: 2.524
batch start
#iterations: 142
currently lose_sum: 369.2497444152832
time_elpased: 2.551
batch start
#iterations: 143
currently lose_sum: 369.1785156726837
time_elpased: 2.548
batch start
#iterations: 144
currently lose_sum: 368.4271432161331
time_elpased: 2.534
batch start
#iterations: 145
currently lose_sum: 368.8440203666687
time_elpased: 2.587
batch start
#iterations: 146
currently lose_sum: 368.3029153943062
time_elpased: 2.572
batch start
#iterations: 147
currently lose_sum: 369.0768675208092
time_elpased: 2.624
batch start
#iterations: 148
currently lose_sum: 367.60365331172943
time_elpased: 2.629
batch start
#iterations: 149
currently lose_sum: 367.68056416511536
time_elpased: 2.56
batch start
#iterations: 150
currently lose_sum: 369.0414165854454
time_elpased: 2.596
batch start
#iterations: 151
currently lose_sum: 368.25079721212387
time_elpased: 2.596
batch start
#iterations: 152
currently lose_sum: 369.0965859889984
time_elpased: 2.477
batch start
#iterations: 153
currently lose_sum: 368.88190084695816
time_elpased: 2.538
batch start
#iterations: 154
currently lose_sum: 368.4192870259285
time_elpased: 2.554
batch start
#iterations: 155
currently lose_sum: 369.29683887958527
time_elpased: 2.506
batch start
#iterations: 156
currently lose_sum: 367.2173483967781
time_elpased: 2.571
batch start
#iterations: 157
currently lose_sum: 368.6806088089943
time_elpased: 2.504
batch start
#iterations: 158
currently lose_sum: 367.90918958187103
time_elpased: 2.515
batch start
#iterations: 159
currently lose_sum: 367.080681681633
time_elpased: 2.542
start validation test
0.782371134021
1.0
0.782371134021
0.877899242293
0
validation finish
batch start
#iterations: 160
currently lose_sum: 368.67786824703217
time_elpased: 2.558
batch start
#iterations: 161
currently lose_sum: 369.9574219584465
time_elpased: 2.586
batch start
#iterations: 162
currently lose_sum: 368.171282351017
time_elpased: 2.556
batch start
#iterations: 163
currently lose_sum: 369.0433743596077
time_elpased: 2.586
batch start
#iterations: 164
currently lose_sum: 367.96640211343765
time_elpased: 2.592
batch start
#iterations: 165
currently lose_sum: 368.7409930229187
time_elpased: 2.557
batch start
#iterations: 166
currently lose_sum: 368.9791776537895
time_elpased: 2.534
batch start
#iterations: 167
currently lose_sum: 367.9791153073311
time_elpased: 2.6
batch start
#iterations: 168
currently lose_sum: 366.33573961257935
time_elpased: 2.554
batch start
#iterations: 169
currently lose_sum: 367.7934060096741
time_elpased: 2.564
batch start
#iterations: 170
currently lose_sum: 368.424484372139
time_elpased: 2.547
batch start
#iterations: 171
currently lose_sum: 368.9032394886017
time_elpased: 2.568
batch start
#iterations: 172
currently lose_sum: 367.68072068691254
time_elpased: 2.573
batch start
#iterations: 173
currently lose_sum: 367.3442967534065
time_elpased: 2.555
batch start
#iterations: 174
currently lose_sum: 368.07648146152496
time_elpased: 2.621
batch start
#iterations: 175
currently lose_sum: 368.76410859823227
time_elpased: 2.545
batch start
#iterations: 176
currently lose_sum: 368.2343891263008
time_elpased: 2.515
batch start
#iterations: 177
currently lose_sum: 367.8620062470436
time_elpased: 2.574
batch start
#iterations: 178
currently lose_sum: 369.25204622745514
time_elpased: 2.466
batch start
#iterations: 179
currently lose_sum: 369.07182228565216
time_elpased: 2.562
start validation test
0.733092783505
1.0
0.733092783505
0.845993694605
0
validation finish
batch start
#iterations: 180
currently lose_sum: 368.3524885773659
time_elpased: 2.548
batch start
#iterations: 181
currently lose_sum: 365.888285279274
time_elpased: 2.562
batch start
#iterations: 182
currently lose_sum: 368.76177620887756
time_elpased: 2.582
batch start
#iterations: 183
currently lose_sum: 369.1369908452034
time_elpased: 2.411
batch start
#iterations: 184
currently lose_sum: 367.05054891109467
time_elpased: 2.62
batch start
#iterations: 185
currently lose_sum: 368.2012031674385
time_elpased: 2.531
batch start
#iterations: 186
currently lose_sum: 368.01979064941406
time_elpased: 2.6
batch start
#iterations: 187
currently lose_sum: 366.9112915992737
time_elpased: 2.604
batch start
#iterations: 188
currently lose_sum: 366.93063163757324
time_elpased: 2.51
batch start
#iterations: 189
currently lose_sum: 368.02958673238754
time_elpased: 2.522
batch start
#iterations: 190
currently lose_sum: 367.8406443595886
time_elpased: 2.543
batch start
#iterations: 191
currently lose_sum: 367.8026884198189
time_elpased: 2.62
batch start
#iterations: 192
currently lose_sum: 368.6324183344841
time_elpased: 2.485
batch start
#iterations: 193
currently lose_sum: 366.93554133176804
time_elpased: 2.541
batch start
#iterations: 194
currently lose_sum: 368.04135954380035
time_elpased: 2.593
batch start
#iterations: 195
currently lose_sum: 367.8794413805008
time_elpased: 2.537
batch start
#iterations: 196
currently lose_sum: 369.2990291118622
time_elpased: 2.541
batch start
#iterations: 197
currently lose_sum: 367.168265581131
time_elpased: 2.546
batch start
#iterations: 198
currently lose_sum: 367.1385591030121
time_elpased: 2.541
batch start
#iterations: 199
currently lose_sum: 367.14513063430786
time_elpased: 2.593
start validation test
0.776082474227
1.0
0.776082474227
0.873926166705
0
validation finish
batch start
#iterations: 200
currently lose_sum: 366.535195350647
time_elpased: 2.571
batch start
#iterations: 201
currently lose_sum: 366.72137129306793
time_elpased: 2.616
batch start
#iterations: 202
currently lose_sum: 367.55671018362045
time_elpased: 2.569
batch start
#iterations: 203
currently lose_sum: 366.124216735363
time_elpased: 2.618
batch start
#iterations: 204
currently lose_sum: 366.788026869297
time_elpased: 2.52
batch start
#iterations: 205
currently lose_sum: 368.4831494688988
time_elpased: 2.578
batch start
#iterations: 206
currently lose_sum: 365.558056473732
time_elpased: 2.535
batch start
#iterations: 207
currently lose_sum: 367.1036252975464
time_elpased: 2.561
batch start
#iterations: 208
currently lose_sum: 366.18276286125183
time_elpased: 2.59
batch start
#iterations: 209
currently lose_sum: 367.1877160668373
time_elpased: 2.537
batch start
#iterations: 210
currently lose_sum: 366.79331624507904
time_elpased: 2.566
batch start
#iterations: 211
currently lose_sum: 366.71746695041656
time_elpased: 2.579
batch start
#iterations: 212
currently lose_sum: 368.0165509581566
time_elpased: 2.55
batch start
#iterations: 213
currently lose_sum: 366.35426664352417
time_elpased: 2.586
batch start
#iterations: 214
currently lose_sum: 367.1554346084595
time_elpased: 2.504
batch start
#iterations: 215
currently lose_sum: 366.6351417899132
time_elpased: 2.532
batch start
#iterations: 216
currently lose_sum: 367.9582149386406
time_elpased: 2.574
batch start
#iterations: 217
currently lose_sum: 364.75843501091003
time_elpased: 2.612
batch start
#iterations: 218
currently lose_sum: 366.151751935482
time_elpased: 2.642
batch start
#iterations: 219
currently lose_sum: 368.0917336344719
time_elpased: 2.556
start validation test
0.773092783505
1.0
0.773092783505
0.872027443456
0
validation finish
batch start
#iterations: 220
currently lose_sum: 366.55952602624893
time_elpased: 2.571
batch start
#iterations: 221
currently lose_sum: 365.1094430088997
time_elpased: 2.533
batch start
#iterations: 222
currently lose_sum: 366.96494817733765
time_elpased: 2.658
batch start
#iterations: 223
currently lose_sum: 367.79427510499954
time_elpased: 2.605
batch start
#iterations: 224
currently lose_sum: 366.81987166404724
time_elpased: 2.556
batch start
#iterations: 225
currently lose_sum: 365.9668728709221
time_elpased: 2.555
batch start
#iterations: 226
currently lose_sum: 366.27150386571884
time_elpased: 2.616
batch start
#iterations: 227
currently lose_sum: 365.6002593636513
time_elpased: 2.591
batch start
#iterations: 228
currently lose_sum: 366.4177842140198
time_elpased: 2.522
batch start
#iterations: 229
currently lose_sum: 365.31398808956146
time_elpased: 2.516
batch start
#iterations: 230
currently lose_sum: 366.0765037536621
time_elpased: 2.449
batch start
#iterations: 231
currently lose_sum: 366.0757346153259
time_elpased: 2.546
batch start
#iterations: 232
currently lose_sum: 366.4133543372154
time_elpased: 2.651
batch start
#iterations: 233
currently lose_sum: 366.59068763256073
time_elpased: 2.518
batch start
#iterations: 234
currently lose_sum: 366.281996011734
time_elpased: 2.538
batch start
#iterations: 235
currently lose_sum: 366.6995640397072
time_elpased: 2.531
batch start
#iterations: 236
currently lose_sum: 366.08069801330566
time_elpased: 2.534
batch start
#iterations: 237
currently lose_sum: 367.0578498840332
time_elpased: 2.534
batch start
#iterations: 238
currently lose_sum: 366.56277471780777
time_elpased: 2.675
batch start
#iterations: 239
currently lose_sum: 366.3213996887207
time_elpased: 2.682
start validation test
0.757319587629
1.0
0.757319587629
0.861903085768
0
validation finish
batch start
#iterations: 240
currently lose_sum: 366.59721475839615
time_elpased: 2.524
batch start
#iterations: 241
currently lose_sum: 366.46715301275253
time_elpased: 2.569
batch start
#iterations: 242
currently lose_sum: 366.9206009507179
time_elpased: 2.519
batch start
#iterations: 243
currently lose_sum: 367.27860647439957
time_elpased: 2.615
batch start
#iterations: 244
currently lose_sum: 365.6416715979576
time_elpased: 2.546
batch start
#iterations: 245
currently lose_sum: 366.4373462796211
time_elpased: 2.533
batch start
#iterations: 246
currently lose_sum: 366.9705494046211
time_elpased: 2.531
batch start
#iterations: 247
currently lose_sum: 365.29638290405273
time_elpased: 2.507
batch start
#iterations: 248
currently lose_sum: 365.79490035772324
time_elpased: 2.497
batch start
#iterations: 249
currently lose_sum: 365.58333867788315
time_elpased: 2.57
batch start
#iterations: 250
currently lose_sum: 366.3302769064903
time_elpased: 2.555
batch start
#iterations: 251
currently lose_sum: 366.5418192744255
time_elpased: 2.611
batch start
#iterations: 252
currently lose_sum: 365.45721912384033
time_elpased: 2.499
batch start
#iterations: 253
currently lose_sum: 366.0186175107956
time_elpased: 2.566
batch start
#iterations: 254
currently lose_sum: 365.2190955877304
time_elpased: 2.546
batch start
#iterations: 255
currently lose_sum: 365.53508698940277
time_elpased: 2.574
batch start
#iterations: 256
currently lose_sum: 365.19729417562485
time_elpased: 2.532
batch start
#iterations: 257
currently lose_sum: 365.87916630506516
time_elpased: 2.524
batch start
#iterations: 258
currently lose_sum: 364.715599834919
time_elpased: 2.596
batch start
#iterations: 259
currently lose_sum: 365.10966128110886
time_elpased: 2.581
start validation test
0.720927835052
1.0
0.720927835052
0.837836218774
0
validation finish
batch start
#iterations: 260
currently lose_sum: 365.89987176656723
time_elpased: 2.489
batch start
#iterations: 261
currently lose_sum: 365.2530279159546
time_elpased: 2.439
batch start
#iterations: 262
currently lose_sum: 364.84773886203766
time_elpased: 2.535
batch start
#iterations: 263
currently lose_sum: 365.7108668088913
time_elpased: 2.531
batch start
#iterations: 264
currently lose_sum: 366.3039983510971
time_elpased: 2.518
batch start
#iterations: 265
currently lose_sum: 365.52373045682907
time_elpased: 2.57
batch start
#iterations: 266
currently lose_sum: 364.761034488678
time_elpased: 2.525
batch start
#iterations: 267
currently lose_sum: 364.96462923288345
time_elpased: 2.559
batch start
#iterations: 268
currently lose_sum: 365.9241378903389
time_elpased: 2.562
batch start
#iterations: 269
currently lose_sum: 365.80436408519745
time_elpased: 2.576
batch start
#iterations: 270
currently lose_sum: 365.7845114469528
time_elpased: 2.524
batch start
#iterations: 271
currently lose_sum: 364.84469336271286
time_elpased: 2.514
batch start
#iterations: 272
currently lose_sum: 365.57960444688797
time_elpased: 2.532
batch start
#iterations: 273
currently lose_sum: 365.6080259680748
time_elpased: 2.511
batch start
#iterations: 274
currently lose_sum: 366.0474790930748
time_elpased: 2.546
batch start
#iterations: 275
currently lose_sum: 366.180343747139
time_elpased: 2.519
batch start
#iterations: 276
currently lose_sum: 366.08539485931396
time_elpased: 2.513
batch start
#iterations: 277
currently lose_sum: 364.73559135198593
time_elpased: 2.551
batch start
#iterations: 278
currently lose_sum: 366.2608146071434
time_elpased: 2.534
batch start
#iterations: 279
currently lose_sum: 364.386934697628
time_elpased: 2.565
start validation test
0.758144329897
1.0
0.758144329897
0.862436964935
0
validation finish
batch start
#iterations: 280
currently lose_sum: 364.9641572833061
time_elpased: 2.535
batch start
#iterations: 281
currently lose_sum: 364.5918878316879
time_elpased: 2.567
batch start
#iterations: 282
currently lose_sum: 365.3261132836342
time_elpased: 2.553
batch start
#iterations: 283
currently lose_sum: 366.58552145957947
time_elpased: 2.542
batch start
#iterations: 284
currently lose_sum: 366.2945964336395
time_elpased: 2.667
batch start
#iterations: 285
currently lose_sum: 364.4476776123047
time_elpased: 2.578
batch start
#iterations: 286
currently lose_sum: 364.89731085300446
time_elpased: 2.55
batch start
#iterations: 287
currently lose_sum: 365.7382220029831
time_elpased: 2.577
batch start
#iterations: 288
currently lose_sum: 365.04095977544785
time_elpased: 2.597
batch start
#iterations: 289
currently lose_sum: 363.3170172572136
time_elpased: 2.609
batch start
#iterations: 290
currently lose_sum: 364.529428422451
time_elpased: 2.561
batch start
#iterations: 291
currently lose_sum: 365.67256742715836
time_elpased: 2.547
batch start
#iterations: 292
currently lose_sum: 365.7376482486725
time_elpased: 2.572
batch start
#iterations: 293
currently lose_sum: 365.1867987513542
time_elpased: 2.585
batch start
#iterations: 294
currently lose_sum: 363.6499487757683
time_elpased: 2.524
batch start
#iterations: 295
currently lose_sum: 366.28859400749207
time_elpased: 2.621
batch start
#iterations: 296
currently lose_sum: 364.69904416799545
time_elpased: 2.561
batch start
#iterations: 297
currently lose_sum: 365.79529947042465
time_elpased: 2.518
batch start
#iterations: 298
currently lose_sum: 364.5969937443733
time_elpased: 2.575
batch start
#iterations: 299
currently lose_sum: 364.8031198978424
time_elpased: 2.564
start validation test
0.740103092784
1.0
0.740103092784
0.850642810593
0
validation finish
batch start
#iterations: 300
currently lose_sum: 364.5783081650734
time_elpased: 2.464
batch start
#iterations: 301
currently lose_sum: 364.8341997861862
time_elpased: 2.638
batch start
#iterations: 302
currently lose_sum: 363.68149054050446
time_elpased: 2.507
batch start
#iterations: 303
currently lose_sum: 365.4829095005989
time_elpased: 2.567
batch start
#iterations: 304
currently lose_sum: 365.23503601551056
time_elpased: 2.603
batch start
#iterations: 305
currently lose_sum: 365.0540135502815
time_elpased: 2.572
batch start
#iterations: 306
currently lose_sum: 364.36019772291183
time_elpased: 2.578
batch start
#iterations: 307
currently lose_sum: 364.2758926153183
time_elpased: 2.548
batch start
#iterations: 308
currently lose_sum: 365.03576934337616
time_elpased: 2.554
batch start
#iterations: 309
currently lose_sum: 364.5026025176048
time_elpased: 2.521
batch start
#iterations: 310
currently lose_sum: 365.77690064907074
time_elpased: 2.533
batch start
#iterations: 311
currently lose_sum: 364.05806851387024
time_elpased: 2.52
batch start
#iterations: 312
currently lose_sum: 364.14773803949356
time_elpased: 2.578
batch start
#iterations: 313
currently lose_sum: 364.4768473505974
time_elpased: 2.529
batch start
#iterations: 314
currently lose_sum: 364.5029639005661
time_elpased: 2.508
batch start
#iterations: 315
currently lose_sum: 363.91040045022964
time_elpased: 2.619
batch start
#iterations: 316
currently lose_sum: 362.8781725168228
time_elpased: 2.681
batch start
#iterations: 317
currently lose_sum: 365.5994528532028
time_elpased: 2.536
batch start
#iterations: 318
currently lose_sum: 364.41940039396286
time_elpased: 2.515
batch start
#iterations: 319
currently lose_sum: 364.91141015291214
time_elpased: 2.663
start validation test
0.761443298969
1.0
0.761443298969
0.864567482149
0
validation finish
batch start
#iterations: 320
currently lose_sum: 363.351793885231
time_elpased: 2.561
batch start
#iterations: 321
currently lose_sum: 364.8049092888832
time_elpased: 2.524
batch start
#iterations: 322
currently lose_sum: 363.61452889442444
time_elpased: 2.571
batch start
#iterations: 323
currently lose_sum: 364.5575446486473
time_elpased: 2.54
batch start
#iterations: 324
currently lose_sum: 363.7607656121254
time_elpased: 2.548
batch start
#iterations: 325
currently lose_sum: 365.21535646915436
time_elpased: 2.505
batch start
#iterations: 326
currently lose_sum: 364.33682513237
time_elpased: 2.507
batch start
#iterations: 327
currently lose_sum: 364.27631133794785
time_elpased: 2.591
batch start
#iterations: 328
currently lose_sum: 365.22618478536606
time_elpased: 2.582
batch start
#iterations: 329
currently lose_sum: 363.80989211797714
time_elpased: 2.608
batch start
#iterations: 330
currently lose_sum: 365.18136060237885
time_elpased: 2.597
batch start
#iterations: 331
currently lose_sum: 364.7458091378212
time_elpased: 2.541
batch start
#iterations: 332
currently lose_sum: 364.86755925416946
time_elpased: 2.604
batch start
#iterations: 333
currently lose_sum: 364.4987136721611
time_elpased: 2.664
batch start
#iterations: 334
currently lose_sum: 363.3365159034729
time_elpased: 2.527
batch start
#iterations: 335
currently lose_sum: 363.9465140402317
time_elpased: 2.535
batch start
#iterations: 336
currently lose_sum: 362.8432803750038
time_elpased: 2.512
batch start
#iterations: 337
currently lose_sum: 363.5265778899193
time_elpased: 2.491
batch start
#iterations: 338
currently lose_sum: 363.8921992778778
time_elpased: 2.647
batch start
#iterations: 339
currently lose_sum: 363.6775984764099
time_elpased: 2.552
start validation test
0.772680412371
1.0
0.772680412371
0.871765047979
0
validation finish
batch start
#iterations: 340
currently lose_sum: 365.7030357122421
time_elpased: 2.62
batch start
#iterations: 341
currently lose_sum: 364.23104560375214
time_elpased: 2.796
batch start
#iterations: 342
currently lose_sum: 366.0364226102829
time_elpased: 2.552
batch start
#iterations: 343
currently lose_sum: 363.5129755139351
time_elpased: 2.566
batch start
#iterations: 344
currently lose_sum: 364.9655832648277
time_elpased: 2.533
batch start
#iterations: 345
currently lose_sum: 365.25960743427277
time_elpased: 2.604
batch start
#iterations: 346
currently lose_sum: 362.4946103692055
time_elpased: 2.532
batch start
#iterations: 347
currently lose_sum: 362.6711555123329
time_elpased: 2.534
batch start
#iterations: 348
currently lose_sum: 362.7960463166237
time_elpased: 2.542
batch start
#iterations: 349
currently lose_sum: 362.9621257185936
time_elpased: 2.506
batch start
#iterations: 350
currently lose_sum: 365.2682845890522
time_elpased: 2.599
batch start
#iterations: 351
currently lose_sum: 364.38815212249756
time_elpased: 2.545
batch start
#iterations: 352
currently lose_sum: 363.29670864343643
time_elpased: 2.54
batch start
#iterations: 353
currently lose_sum: 364.20395743846893
time_elpased: 2.553
batch start
#iterations: 354
currently lose_sum: 363.5543440580368
time_elpased: 2.478
batch start
#iterations: 355
currently lose_sum: 365.492393553257
time_elpased: 2.525
batch start
#iterations: 356
currently lose_sum: 364.2489624619484
time_elpased: 2.519
batch start
#iterations: 357
currently lose_sum: 363.3135851621628
time_elpased: 2.55
batch start
#iterations: 358
currently lose_sum: 363.6177623271942
time_elpased: 2.548
batch start
#iterations: 359
currently lose_sum: 365.7141674757004
time_elpased: 2.502
start validation test
0.775773195876
1.0
0.775773195876
0.873730043541
0
validation finish
batch start
#iterations: 360
currently lose_sum: 363.90045899152756
time_elpased: 2.55
batch start
#iterations: 361
currently lose_sum: 365.4860816001892
time_elpased: 2.529
batch start
#iterations: 362
currently lose_sum: 363.8808113336563
time_elpased: 2.512
batch start
#iterations: 363
currently lose_sum: 365.0381901860237
time_elpased: 2.636
batch start
#iterations: 364
currently lose_sum: 364.4345559477806
time_elpased: 2.533
batch start
#iterations: 365
currently lose_sum: 363.5885685682297
time_elpased: 2.587
batch start
#iterations: 366
currently lose_sum: 364.20031452178955
time_elpased: 2.552
batch start
#iterations: 367
currently lose_sum: 363.9832466840744
time_elpased: 2.63
batch start
#iterations: 368
currently lose_sum: 362.8122430443764
time_elpased: 2.574
batch start
#iterations: 369
currently lose_sum: 362.1313120126724
time_elpased: 2.585
batch start
#iterations: 370
currently lose_sum: 362.6330710053444
time_elpased: 2.538
batch start
#iterations: 371
currently lose_sum: 362.9550684094429
time_elpased: 2.573
batch start
#iterations: 372
currently lose_sum: 362.05750834941864
time_elpased: 2.562
batch start
#iterations: 373
currently lose_sum: 364.0954057574272
time_elpased: 2.453
batch start
#iterations: 374
currently lose_sum: 365.66390389204025
time_elpased: 2.58
batch start
#iterations: 375
currently lose_sum: 362.64134645462036
time_elpased: 2.556
batch start
#iterations: 376
currently lose_sum: 363.3619422316551
time_elpased: 2.538
batch start
#iterations: 377
currently lose_sum: 364.0475946068764
time_elpased: 2.543
batch start
#iterations: 378
currently lose_sum: 363.75392097234726
time_elpased: 2.554
batch start
#iterations: 379
currently lose_sum: 364.81145894527435
time_elpased: 2.577
start validation test
0.787525773196
1.0
0.787525773196
0.881135013553
0
validation finish
batch start
#iterations: 380
currently lose_sum: 363.67315155267715
time_elpased: 2.559
batch start
#iterations: 381
currently lose_sum: 363.76512783765793
time_elpased: 2.599
batch start
#iterations: 382
currently lose_sum: 364.44402319192886
time_elpased: 2.591
batch start
#iterations: 383
currently lose_sum: 363.37630504369736
time_elpased: 2.549
batch start
#iterations: 384
currently lose_sum: 361.5129566192627
time_elpased: 2.596
batch start
#iterations: 385
currently lose_sum: 363.9242831468582
time_elpased: 2.496
batch start
#iterations: 386
currently lose_sum: 362.8116935491562
time_elpased: 2.579
batch start
#iterations: 387
currently lose_sum: 362.3399113416672
time_elpased: 2.582
batch start
#iterations: 388
currently lose_sum: 361.6857467889786
time_elpased: 2.56
batch start
#iterations: 389
currently lose_sum: 364.0645561218262
time_elpased: 2.559
batch start
#iterations: 390
currently lose_sum: 362.1357766389847
time_elpased: 2.539
batch start
#iterations: 391
currently lose_sum: 363.01110035181046
time_elpased: 2.568
batch start
#iterations: 392
currently lose_sum: 363.67225700616837
time_elpased: 2.656
batch start
#iterations: 393
currently lose_sum: 361.8419632911682
time_elpased: 2.679
batch start
#iterations: 394
currently lose_sum: 362.8347542285919
time_elpased: 2.484
batch start
#iterations: 395
currently lose_sum: 362.39862793684006
time_elpased: 2.588
batch start
#iterations: 396
currently lose_sum: 364.07743495702744
time_elpased: 2.547
batch start
#iterations: 397
currently lose_sum: 364.0472377538681
time_elpased: 2.559
batch start
#iterations: 398
currently lose_sum: 363.8595591187477
time_elpased: 2.596
batch start
#iterations: 399
currently lose_sum: 362.1533198952675
time_elpased: 2.535
start validation test
0.753505154639
1.0
0.753505154639
0.859427361985
0
validation finish
acc: 0.790
pre: 1.000
rec: 0.790
F1: 0.882
auc: 0.000
