start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 404.65781408548355
time_elpased: 2.357
batch start
#iterations: 1
currently lose_sum: 403.4532241821289
time_elpased: 2.285
batch start
#iterations: 2
currently lose_sum: 402.49540239572525
time_elpased: 2.284
batch start
#iterations: 3
currently lose_sum: 402.1586287021637
time_elpased: 2.277
batch start
#iterations: 4
currently lose_sum: 401.52595245838165
time_elpased: 2.246
batch start
#iterations: 5
currently lose_sum: 400.98422211408615
time_elpased: 2.323
batch start
#iterations: 6
currently lose_sum: 400.21641820669174
time_elpased: 2.278
batch start
#iterations: 7
currently lose_sum: 399.98041611909866
time_elpased: 2.254
batch start
#iterations: 8
currently lose_sum: 398.65314066410065
time_elpased: 2.22
batch start
#iterations: 9
currently lose_sum: 397.8636310696602
time_elpased: 2.337
batch start
#iterations: 10
currently lose_sum: 396.48324620723724
time_elpased: 2.359
batch start
#iterations: 11
currently lose_sum: 395.6372472643852
time_elpased: 2.255
batch start
#iterations: 12
currently lose_sum: 394.47058326005936
time_elpased: 2.295
batch start
#iterations: 13
currently lose_sum: 393.7725813984871
time_elpased: 2.282
batch start
#iterations: 14
currently lose_sum: 392.2802307009697
time_elpased: 2.285
batch start
#iterations: 15
currently lose_sum: 392.3415589928627
time_elpased: 2.364
batch start
#iterations: 16
currently lose_sum: 391.16845232248306
time_elpased: 2.32
batch start
#iterations: 17
currently lose_sum: 391.2751312851906
time_elpased: 2.339
batch start
#iterations: 18
currently lose_sum: 391.0936915278435
time_elpased: 2.337
batch start
#iterations: 19
currently lose_sum: 389.6338036060333
time_elpased: 2.35
start validation test
0.491340206186
1.0
0.491340206186
0.658924374395
0
validation finish
batch start
#iterations: 20
currently lose_sum: 389.2586891055107
time_elpased: 2.243
batch start
#iterations: 21
currently lose_sum: 388.0816413164139
time_elpased: 2.344
batch start
#iterations: 22
currently lose_sum: 387.56973987817764
time_elpased: 2.328
batch start
#iterations: 23
currently lose_sum: 387.2957422733307
time_elpased: 2.283
batch start
#iterations: 24
currently lose_sum: 386.5982846021652
time_elpased: 2.249
batch start
#iterations: 25
currently lose_sum: 386.4317641854286
time_elpased: 2.311
batch start
#iterations: 26
currently lose_sum: 386.4877110719681
time_elpased: 2.266
batch start
#iterations: 27
currently lose_sum: 385.8867912888527
time_elpased: 2.342
batch start
#iterations: 28
currently lose_sum: 385.73296546936035
time_elpased: 2.171
batch start
#iterations: 29
currently lose_sum: 386.00780379772186
time_elpased: 2.28
batch start
#iterations: 30
currently lose_sum: 384.14424055814743
time_elpased: 2.243
batch start
#iterations: 31
currently lose_sum: 384.39957398176193
time_elpased: 2.269
batch start
#iterations: 32
currently lose_sum: 383.9243109226227
time_elpased: 2.235
batch start
#iterations: 33
currently lose_sum: 383.74494725465775
time_elpased: 2.285
batch start
#iterations: 34
currently lose_sum: 382.93515092134476
time_elpased: 2.322
batch start
#iterations: 35
currently lose_sum: 382.3587213754654
time_elpased: 2.267
batch start
#iterations: 36
currently lose_sum: 382.9114472270012
time_elpased: 2.262
batch start
#iterations: 37
currently lose_sum: 381.2690934538841
time_elpased: 2.268
batch start
#iterations: 38
currently lose_sum: 382.3562608361244
time_elpased: 2.405
batch start
#iterations: 39
currently lose_sum: 381.81053298711777
time_elpased: 2.332
start validation test
0.413505154639
1.0
0.413505154639
0.58507767486
0
validation finish
batch start
#iterations: 40
currently lose_sum: 382.4280896782875
time_elpased: 2.312
batch start
#iterations: 41
currently lose_sum: 380.56040984392166
time_elpased: 2.283
batch start
#iterations: 42
currently lose_sum: 382.01786226034164
time_elpased: 2.305
batch start
#iterations: 43
currently lose_sum: 379.669551551342
time_elpased: 2.304
batch start
#iterations: 44
currently lose_sum: 379.6364981532097
time_elpased: 2.247
batch start
#iterations: 45
currently lose_sum: 380.52859354019165
time_elpased: 2.25
batch start
#iterations: 46
currently lose_sum: 379.89860624074936
time_elpased: 2.316
batch start
#iterations: 47
currently lose_sum: 380.68998831510544
time_elpased: 2.268
batch start
#iterations: 48
currently lose_sum: 380.3023604154587
time_elpased: 2.291
batch start
#iterations: 49
currently lose_sum: 380.38648825883865
time_elpased: 2.288
batch start
#iterations: 50
currently lose_sum: 380.021855533123
time_elpased: 2.293
batch start
#iterations: 51
currently lose_sum: 378.50176709890366
time_elpased: 2.266
batch start
#iterations: 52
currently lose_sum: 378.3352501988411
time_elpased: 2.327
batch start
#iterations: 53
currently lose_sum: 377.9190998673439
time_elpased: 2.289
batch start
#iterations: 54
currently lose_sum: 378.8418564796448
time_elpased: 2.334
batch start
#iterations: 55
currently lose_sum: 378.932308614254
time_elpased: 2.28
batch start
#iterations: 56
currently lose_sum: 380.3255667090416
time_elpased: 2.258
batch start
#iterations: 57
currently lose_sum: 377.56344044208527
time_elpased: 2.36
batch start
#iterations: 58
currently lose_sum: 377.825998544693
time_elpased: 2.284
batch start
#iterations: 59
currently lose_sum: 377.0950976610184
time_elpased: 2.358
start validation test
0.709278350515
1.0
0.709278350515
0.829915560917
0
validation finish
batch start
#iterations: 60
currently lose_sum: 377.40631383657455
time_elpased: 2.278
batch start
#iterations: 61
currently lose_sum: 378.1440122127533
time_elpased: 2.342
batch start
#iterations: 62
currently lose_sum: 378.25694239139557
time_elpased: 2.349
batch start
#iterations: 63
currently lose_sum: 375.88080048561096
time_elpased: 2.288
batch start
#iterations: 64
currently lose_sum: 377.42499232292175
time_elpased: 2.218
batch start
#iterations: 65
currently lose_sum: 376.708353638649
time_elpased: 2.337
batch start
#iterations: 66
currently lose_sum: 377.23951214551926
time_elpased: 2.346
batch start
#iterations: 67
currently lose_sum: 375.6670118570328
time_elpased: 2.329
batch start
#iterations: 68
currently lose_sum: 377.7560223340988
time_elpased: 2.307
batch start
#iterations: 69
currently lose_sum: 377.1091292500496
time_elpased: 2.272
batch start
#iterations: 70
currently lose_sum: 376.1299372315407
time_elpased: 2.318
batch start
#iterations: 71
currently lose_sum: 376.55319756269455
time_elpased: 2.289
batch start
#iterations: 72
currently lose_sum: 375.25497245788574
time_elpased: 2.245
batch start
#iterations: 73
currently lose_sum: 376.62036085128784
time_elpased: 2.302
batch start
#iterations: 74
currently lose_sum: 376.2947146296501
time_elpased: 2.286
batch start
#iterations: 75
currently lose_sum: 375.13602966070175
time_elpased: 2.305
batch start
#iterations: 76
currently lose_sum: 374.55133748054504
time_elpased: 2.282
batch start
#iterations: 77
currently lose_sum: 375.7407856583595
time_elpased: 2.307
batch start
#iterations: 78
currently lose_sum: 375.57808643579483
time_elpased: 2.281
batch start
#iterations: 79
currently lose_sum: 375.67158991098404
time_elpased: 2.322
start validation test
0.790103092784
1.0
0.790103092784
0.88274591108
0
validation finish
batch start
#iterations: 80
currently lose_sum: 374.80129116773605
time_elpased: 2.268
batch start
#iterations: 81
currently lose_sum: 374.94044744968414
time_elpased: 2.374
batch start
#iterations: 82
currently lose_sum: 375.19037914276123
time_elpased: 2.28
batch start
#iterations: 83
currently lose_sum: 374.42250394821167
time_elpased: 2.306
batch start
#iterations: 84
currently lose_sum: 375.29269820451736
time_elpased: 2.247
batch start
#iterations: 85
currently lose_sum: 373.81698632240295
time_elpased: 2.278
batch start
#iterations: 86
currently lose_sum: 373.58265990018845
time_elpased: 2.282
batch start
#iterations: 87
currently lose_sum: 374.5322114825249
time_elpased: 2.29
batch start
#iterations: 88
currently lose_sum: 373.36236250400543
time_elpased: 2.223
batch start
#iterations: 89
currently lose_sum: 373.8267433643341
time_elpased: 2.31
batch start
#iterations: 90
currently lose_sum: 373.4378365278244
time_elpased: 2.291
batch start
#iterations: 91
currently lose_sum: 374.00223433971405
time_elpased: 2.306
batch start
#iterations: 92
currently lose_sum: 374.32719790935516
time_elpased: 2.271
batch start
#iterations: 93
currently lose_sum: 373.8096460700035
time_elpased: 2.323
batch start
#iterations: 94
currently lose_sum: 373.0764548778534
time_elpased: 2.306
batch start
#iterations: 95
currently lose_sum: 373.42432337999344
time_elpased: 2.319
batch start
#iterations: 96
currently lose_sum: 372.8395540714264
time_elpased: 2.264
batch start
#iterations: 97
currently lose_sum: 373.14890319108963
time_elpased: 2.309
batch start
#iterations: 98
currently lose_sum: 373.18994402885437
time_elpased: 2.322
batch start
#iterations: 99
currently lose_sum: 373.76306211948395
time_elpased: 2.267
start validation test
0.731649484536
1.0
0.731649484536
0.845031850926
0
validation finish
batch start
#iterations: 100
currently lose_sum: 372.22109121084213
time_elpased: 2.244
batch start
#iterations: 101
currently lose_sum: 372.82329231500626
time_elpased: 2.304
batch start
#iterations: 102
currently lose_sum: 373.0841064453125
time_elpased: 2.351
batch start
#iterations: 103
currently lose_sum: 371.97035986185074
time_elpased: 2.241
batch start
#iterations: 104
currently lose_sum: 372.91042000055313
time_elpased: 2.264
batch start
#iterations: 105
currently lose_sum: 372.83976727724075
time_elpased: 2.28
batch start
#iterations: 106
currently lose_sum: 373.76344460248947
time_elpased: 2.279
batch start
#iterations: 107
currently lose_sum: 373.1641672849655
time_elpased: 2.298
batch start
#iterations: 108
currently lose_sum: 372.15905874967575
time_elpased: 2.167
batch start
#iterations: 109
currently lose_sum: 371.28223609924316
time_elpased: 2.284
batch start
#iterations: 110
currently lose_sum: 372.81595331430435
time_elpased: 2.353
batch start
#iterations: 111
currently lose_sum: 371.72507417201996
time_elpased: 2.269
batch start
#iterations: 112
currently lose_sum: 371.21799314022064
time_elpased: 2.292
batch start
#iterations: 113
currently lose_sum: 372.7279236316681
time_elpased: 2.309
batch start
#iterations: 114
currently lose_sum: 372.3273643255234
time_elpased: 2.354
batch start
#iterations: 115
currently lose_sum: 372.86362767219543
time_elpased: 2.304
batch start
#iterations: 116
currently lose_sum: 370.3769176006317
time_elpased: 2.308
batch start
#iterations: 117
currently lose_sum: 372.38012820482254
time_elpased: 2.339
batch start
#iterations: 118
currently lose_sum: 370.910235285759
time_elpased: 2.297
batch start
#iterations: 119
currently lose_sum: 370.64818382263184
time_elpased: 2.332
start validation test
0.755360824742
1.0
0.755360824742
0.860633112116
0
validation finish
batch start
#iterations: 120
currently lose_sum: 370.8926157951355
time_elpased: 2.302
batch start
#iterations: 121
currently lose_sum: 371.84178894758224
time_elpased: 2.302
batch start
#iterations: 122
currently lose_sum: 370.5562683939934
time_elpased: 2.309
batch start
#iterations: 123
currently lose_sum: 368.5211382508278
time_elpased: 2.317
batch start
#iterations: 124
currently lose_sum: 371.97609907388687
time_elpased: 2.302
batch start
#iterations: 125
currently lose_sum: 370.55153715610504
time_elpased: 2.286
batch start
#iterations: 126
currently lose_sum: 369.9213103055954
time_elpased: 2.342
batch start
#iterations: 127
currently lose_sum: 370.2811814546585
time_elpased: 2.343
batch start
#iterations: 128
currently lose_sum: 370.82016760110855
time_elpased: 2.249
batch start
#iterations: 129
currently lose_sum: 372.86664456129074
time_elpased: 2.29
batch start
#iterations: 130
currently lose_sum: 369.92245811223984
time_elpased: 2.399
batch start
#iterations: 131
currently lose_sum: 370.83278900384903
time_elpased: 2.358
batch start
#iterations: 132
currently lose_sum: 369.47508853673935
time_elpased: 2.284
batch start
#iterations: 133
currently lose_sum: 370.4128710627556
time_elpased: 2.285
batch start
#iterations: 134
currently lose_sum: 370.1480025649071
time_elpased: 2.316
batch start
#iterations: 135
currently lose_sum: 370.58390974998474
time_elpased: 2.39
batch start
#iterations: 136
currently lose_sum: 370.46914249658585
time_elpased: 2.228
batch start
#iterations: 137
currently lose_sum: 368.2779158949852
time_elpased: 2.317
batch start
#iterations: 138
currently lose_sum: 371.168752014637
time_elpased: 2.351
batch start
#iterations: 139
currently lose_sum: 369.98644012212753
time_elpased: 2.276
start validation test
0.76824742268
1.0
0.76824742268
0.868936567164
0
validation finish
batch start
#iterations: 140
currently lose_sum: 368.28500759601593
time_elpased: 2.291
batch start
#iterations: 141
currently lose_sum: 369.29593819379807
time_elpased: 2.324
batch start
#iterations: 142
currently lose_sum: 370.50423872470856
time_elpased: 2.319
batch start
#iterations: 143
currently lose_sum: 370.0705010294914
time_elpased: 2.35
batch start
#iterations: 144
currently lose_sum: 369.07471150159836
time_elpased: 2.247
batch start
#iterations: 145
currently lose_sum: 371.8508733510971
time_elpased: 2.333
batch start
#iterations: 146
currently lose_sum: 370.3480077981949
time_elpased: 2.32
batch start
#iterations: 147
currently lose_sum: 370.42217940092087
time_elpased: 2.364
batch start
#iterations: 148
currently lose_sum: 370.4928198456764
time_elpased: 2.281
batch start
#iterations: 149
currently lose_sum: 369.4323874115944
time_elpased: 2.242
batch start
#iterations: 150
currently lose_sum: 367.8666849732399
time_elpased: 2.339
batch start
#iterations: 151
currently lose_sum: 369.79594510793686
time_elpased: 2.34
batch start
#iterations: 152
currently lose_sum: 369.0838022828102
time_elpased: 2.325
batch start
#iterations: 153
currently lose_sum: 370.55953192710876
time_elpased: 2.29
batch start
#iterations: 154
currently lose_sum: 369.3008469939232
time_elpased: 2.277
batch start
#iterations: 155
currently lose_sum: 369.1729345917702
time_elpased: 2.311
batch start
#iterations: 156
currently lose_sum: 370.3575831055641
time_elpased: 2.279
batch start
#iterations: 157
currently lose_sum: 368.5087636113167
time_elpased: 2.3
batch start
#iterations: 158
currently lose_sum: 367.977501809597
time_elpased: 2.383
batch start
#iterations: 159
currently lose_sum: 369.376852452755
time_elpased: 2.406
start validation test
0.758041237113
1.0
0.758041237113
0.862370257433
0
validation finish
batch start
#iterations: 160
currently lose_sum: 369.70745623111725
time_elpased: 2.316
batch start
#iterations: 161
currently lose_sum: 369.1253235936165
time_elpased: 2.346
batch start
#iterations: 162
currently lose_sum: 370.013699054718
time_elpased: 2.302
batch start
#iterations: 163
currently lose_sum: 369.0484331250191
time_elpased: 2.318
batch start
#iterations: 164
currently lose_sum: 369.05504125356674
time_elpased: 2.255
batch start
#iterations: 165
currently lose_sum: 369.41183733940125
time_elpased: 2.287
batch start
#iterations: 166
currently lose_sum: 368.6366173028946
time_elpased: 2.291
batch start
#iterations: 167
currently lose_sum: 367.40679639577866
time_elpased: 2.316
batch start
#iterations: 168
currently lose_sum: 368.0066233277321
time_elpased: 2.276
batch start
#iterations: 169
currently lose_sum: 369.5314954519272
time_elpased: 2.29
batch start
#iterations: 170
currently lose_sum: 367.3892850279808
time_elpased: 2.319
batch start
#iterations: 171
currently lose_sum: 366.2970455288887
time_elpased: 2.349
batch start
#iterations: 172
currently lose_sum: 368.76438295841217
time_elpased: 2.271
batch start
#iterations: 173
currently lose_sum: 368.0778331756592
time_elpased: 2.358
batch start
#iterations: 174
currently lose_sum: 367.8204558491707
time_elpased: 2.409
batch start
#iterations: 175
currently lose_sum: 367.863219499588
time_elpased: 2.277
batch start
#iterations: 176
currently lose_sum: 368.1626024246216
time_elpased: 2.257
batch start
#iterations: 177
currently lose_sum: 368.42640709877014
time_elpased: 2.306
batch start
#iterations: 178
currently lose_sum: 367.3882187604904
time_elpased: 2.255
batch start
#iterations: 179
currently lose_sum: 367.93025010824203
time_elpased: 2.299
start validation test
0.760515463918
1.0
0.760515463918
0.86396908122
0
validation finish
batch start
#iterations: 180
currently lose_sum: 369.29572236537933
time_elpased: 2.245
batch start
#iterations: 181
currently lose_sum: 367.9355813860893
time_elpased: 2.375
batch start
#iterations: 182
currently lose_sum: 368.72298723459244
time_elpased: 2.298
batch start
#iterations: 183
currently lose_sum: 366.4291577935219
time_elpased: 2.247
batch start
#iterations: 184
currently lose_sum: 366.83295130729675
time_elpased: 2.323
batch start
#iterations: 185
currently lose_sum: 368.20535612106323
time_elpased: 2.321
batch start
#iterations: 186
currently lose_sum: 365.63284236192703
time_elpased: 2.282
batch start
#iterations: 187
currently lose_sum: 366.56894087791443
time_elpased: 2.291
batch start
#iterations: 188
currently lose_sum: 366.92883491516113
time_elpased: 2.132
batch start
#iterations: 189
currently lose_sum: 366.9250775575638
time_elpased: 2.307
batch start
#iterations: 190
currently lose_sum: 367.5988771915436
time_elpased: 2.345
batch start
#iterations: 191
currently lose_sum: 367.1300936937332
time_elpased: 2.374
batch start
#iterations: 192
currently lose_sum: 367.9134294986725
time_elpased: 2.289
batch start
#iterations: 193
currently lose_sum: 367.32864487171173
time_elpased: 2.338
batch start
#iterations: 194
currently lose_sum: 366.36216539144516
time_elpased: 2.316
batch start
#iterations: 195
currently lose_sum: 365.6001774072647
time_elpased: 2.328
batch start
#iterations: 196
currently lose_sum: 367.8120487332344
time_elpased: 2.385
batch start
#iterations: 197
currently lose_sum: 367.59633845090866
time_elpased: 2.327
batch start
#iterations: 198
currently lose_sum: 367.56846356391907
time_elpased: 2.373
batch start
#iterations: 199
currently lose_sum: 367.3742119073868
time_elpased: 2.268
start validation test
0.73793814433
1.0
0.73793814433
0.849211057065
0
validation finish
batch start
#iterations: 200
currently lose_sum: 366.5474103093147
time_elpased: 2.277
batch start
#iterations: 201
currently lose_sum: 366.47292298078537
time_elpased: 2.287
batch start
#iterations: 202
currently lose_sum: 366.0371153354645
time_elpased: 2.344
batch start
#iterations: 203
currently lose_sum: 367.7666060626507
time_elpased: 2.302
batch start
#iterations: 204
currently lose_sum: 365.8858621120453
time_elpased: 2.319
batch start
#iterations: 205
currently lose_sum: 366.8070409297943
time_elpased: 2.295
batch start
#iterations: 206
currently lose_sum: 366.49663865566254
time_elpased: 2.34
batch start
#iterations: 207
currently lose_sum: 367.0050087571144
time_elpased: 2.3
batch start
#iterations: 208
currently lose_sum: 366.59467858076096
time_elpased: 2.342
batch start
#iterations: 209
currently lose_sum: 367.2239247560501
time_elpased: 2.316
batch start
#iterations: 210
currently lose_sum: 366.7835138440132
time_elpased: 2.285
batch start
#iterations: 211
currently lose_sum: 364.6853811144829
time_elpased: 2.379
batch start
#iterations: 212
currently lose_sum: 365.9775610566139
time_elpased: 2.299
batch start
#iterations: 213
currently lose_sum: 365.0357723236084
time_elpased: 2.415
batch start
#iterations: 214
currently lose_sum: 367.0232161283493
time_elpased: 2.377
batch start
#iterations: 215
currently lose_sum: 366.32560634613037
time_elpased: 2.237
batch start
#iterations: 216
currently lose_sum: 366.56057673692703
time_elpased: 2.321
batch start
#iterations: 217
currently lose_sum: 365.7783824801445
time_elpased: 2.336
batch start
#iterations: 218
currently lose_sum: 366.39245361089706
time_elpased: 2.406
batch start
#iterations: 219
currently lose_sum: 366.7171862125397
time_elpased: 2.227
start validation test
0.736082474227
1.0
0.736082474227
0.847980997625
0
validation finish
batch start
#iterations: 220
currently lose_sum: 366.34332317113876
time_elpased: 2.283
batch start
#iterations: 221
currently lose_sum: 364.925874710083
time_elpased: 2.31
batch start
#iterations: 222
currently lose_sum: 364.92879301309586
time_elpased: 2.285
batch start
#iterations: 223
currently lose_sum: 366.883083820343
time_elpased: 2.319
batch start
#iterations: 224
currently lose_sum: 367.59126538038254
time_elpased: 2.319
batch start
#iterations: 225
currently lose_sum: 365.85780131816864
time_elpased: 2.292
batch start
#iterations: 226
currently lose_sum: 367.87816232442856
time_elpased: 2.251
batch start
#iterations: 227
currently lose_sum: 366.89027440547943
time_elpased: 2.299
batch start
#iterations: 228
currently lose_sum: 366.2255236506462
time_elpased: 2.252
batch start
#iterations: 229
currently lose_sum: 366.3099772334099
time_elpased: 2.244
batch start
#iterations: 230
currently lose_sum: 364.7054571509361
time_elpased: 2.305
batch start
#iterations: 231
currently lose_sum: 366.1103445291519
time_elpased: 2.326
batch start
#iterations: 232
currently lose_sum: 365.1826556324959
time_elpased: 2.417
batch start
#iterations: 233
currently lose_sum: 366.99067455530167
time_elpased: 2.277
batch start
#iterations: 234
currently lose_sum: 365.6083844900131
time_elpased: 2.342
batch start
#iterations: 235
currently lose_sum: 367.1414861679077
time_elpased: 2.239
batch start
#iterations: 236
currently lose_sum: 365.4058708548546
time_elpased: 2.302
batch start
#iterations: 237
currently lose_sum: 366.94322538375854
time_elpased: 2.314
batch start
#iterations: 238
currently lose_sum: 365.4919499158859
time_elpased: 2.33
batch start
#iterations: 239
currently lose_sum: 366.4919179081917
time_elpased: 2.278
start validation test
0.74175257732
1.0
0.74175257732
0.851731281444
0
validation finish
batch start
#iterations: 240
currently lose_sum: 365.2813494205475
time_elpased: 2.298
batch start
#iterations: 241
currently lose_sum: 366.0955619215965
time_elpased: 2.32
batch start
#iterations: 242
currently lose_sum: 366.3767859339714
time_elpased: 2.296
batch start
#iterations: 243
currently lose_sum: 365.7740132212639
time_elpased: 2.274
batch start
#iterations: 244
currently lose_sum: 365.6334933042526
time_elpased: 2.311
batch start
#iterations: 245
currently lose_sum: 365.40132811665535
time_elpased: 2.333
batch start
#iterations: 246
currently lose_sum: 364.94742852449417
time_elpased: 2.301
batch start
#iterations: 247
currently lose_sum: 365.62809669971466
time_elpased: 2.289
batch start
#iterations: 248
currently lose_sum: 367.3106108903885
time_elpased: 2.273
batch start
#iterations: 249
currently lose_sum: 365.6480088829994
time_elpased: 2.324
batch start
#iterations: 250
currently lose_sum: 365.5262921452522
time_elpased: 2.336
batch start
#iterations: 251
currently lose_sum: 366.04270201921463
time_elpased: 2.333
batch start
#iterations: 252
currently lose_sum: 365.823161482811
time_elpased: 2.308
batch start
#iterations: 253
currently lose_sum: 365.3617840409279
time_elpased: 2.305
batch start
#iterations: 254
currently lose_sum: 365.2852326631546
time_elpased: 2.282
batch start
#iterations: 255
currently lose_sum: 364.5792358517647
time_elpased: 2.223
batch start
#iterations: 256
currently lose_sum: 365.62531942129135
time_elpased: 2.368
batch start
#iterations: 257
currently lose_sum: 365.558547437191
time_elpased: 2.338
batch start
#iterations: 258
currently lose_sum: 364.2146057486534
time_elpased: 2.334
batch start
#iterations: 259
currently lose_sum: 364.5121996998787
time_elpased: 2.294
start validation test
0.750927835052
1.0
0.750927835052
0.857748469147
0
validation finish
batch start
#iterations: 260
currently lose_sum: 364.00439167022705
time_elpased: 2.327
batch start
#iterations: 261
currently lose_sum: 365.1077094078064
time_elpased: 2.314
batch start
#iterations: 262
currently lose_sum: 366.1923677325249
time_elpased: 2.297
batch start
#iterations: 263
currently lose_sum: 364.57853376865387
time_elpased: 2.305
batch start
#iterations: 264
currently lose_sum: 365.1583928465843
time_elpased: 2.293
batch start
#iterations: 265
currently lose_sum: 366.04145765304565
time_elpased: 2.291
batch start
#iterations: 266
currently lose_sum: 365.359295964241
time_elpased: 2.323
batch start
#iterations: 267
currently lose_sum: 364.3600464463234
time_elpased: 2.15
batch start
#iterations: 268
currently lose_sum: 365.3168933391571
time_elpased: 2.272
batch start
#iterations: 269
currently lose_sum: 365.4291335940361
time_elpased: 2.358
batch start
#iterations: 270
currently lose_sum: 365.6290737390518
time_elpased: 2.319
batch start
#iterations: 271
currently lose_sum: 364.59869688749313
time_elpased: 2.225
batch start
#iterations: 272
currently lose_sum: 365.28749799728394
time_elpased: 2.305
batch start
#iterations: 273
currently lose_sum: 365.41273230314255
time_elpased: 2.284
batch start
#iterations: 274
currently lose_sum: 364.7129989862442
time_elpased: 2.331
batch start
#iterations: 275
currently lose_sum: 364.7716565132141
time_elpased: 2.242
batch start
#iterations: 276
currently lose_sum: 365.29720532894135
time_elpased: 2.37
batch start
#iterations: 277
currently lose_sum: 365.32248812913895
time_elpased: 2.361
batch start
#iterations: 278
currently lose_sum: 362.50680190324783
time_elpased: 2.335
batch start
#iterations: 279
currently lose_sum: 365.03510385751724
time_elpased: 2.36
start validation test
0.758041237113
1.0
0.758041237113
0.862370257433
0
validation finish
batch start
#iterations: 280
currently lose_sum: 364.96573317050934
time_elpased: 2.327
batch start
#iterations: 281
currently lose_sum: 364.9959960579872
time_elpased: 2.307
batch start
#iterations: 282
currently lose_sum: 364.94491589069366
time_elpased: 2.318
batch start
#iterations: 283
currently lose_sum: 366.63260918855667
time_elpased: 2.29
batch start
#iterations: 284
currently lose_sum: 364.99210226535797
time_elpased: 2.296
batch start
#iterations: 285
currently lose_sum: 364.7510938644409
time_elpased: 2.383
batch start
#iterations: 286
currently lose_sum: 363.7976944446564
time_elpased: 2.315
batch start
#iterations: 287
currently lose_sum: 365.7128767967224
time_elpased: 2.309
batch start
#iterations: 288
currently lose_sum: 363.6347774863243
time_elpased: 2.288
batch start
#iterations: 289
currently lose_sum: 363.96481162309647
time_elpased: 2.306
batch start
#iterations: 290
currently lose_sum: 363.9747648239136
time_elpased: 2.364
batch start
#iterations: 291
currently lose_sum: 364.7186023592949
time_elpased: 2.285
batch start
#iterations: 292
currently lose_sum: 364.48673754930496
time_elpased: 2.318
batch start
#iterations: 293
currently lose_sum: 364.341424703598
time_elpased: 2.269
batch start
#iterations: 294
currently lose_sum: 363.4900695681572
time_elpased: 2.299
batch start
#iterations: 295
currently lose_sum: 363.37969213724136
time_elpased: 2.257
batch start
#iterations: 296
currently lose_sum: 363.9138419032097
time_elpased: 2.338
batch start
#iterations: 297
currently lose_sum: 364.9142572879791
time_elpased: 2.321
batch start
#iterations: 298
currently lose_sum: 363.69619834423065
time_elpased: 2.328
batch start
#iterations: 299
currently lose_sum: 364.88218915462494
time_elpased: 2.314
start validation test
0.757525773196
1.0
0.757525773196
0.862036602534
0
validation finish
batch start
#iterations: 300
currently lose_sum: 365.4317607283592
time_elpased: 2.286
batch start
#iterations: 301
currently lose_sum: 362.52425956726074
time_elpased: 2.328
batch start
#iterations: 302
currently lose_sum: 365.9323670268059
time_elpased: 2.358
batch start
#iterations: 303
currently lose_sum: 363.6070348620415
time_elpased: 2.321
batch start
#iterations: 304
currently lose_sum: 365.4189507961273
time_elpased: 2.353
batch start
#iterations: 305
currently lose_sum: 363.7578983902931
time_elpased: 2.352
batch start
#iterations: 306
currently lose_sum: 363.7373197674751
time_elpased: 2.279
batch start
#iterations: 307
currently lose_sum: 362.9491857290268
time_elpased: 2.26
batch start
#iterations: 308
currently lose_sum: 362.7059432864189
time_elpased: 2.335
batch start
#iterations: 309
currently lose_sum: 363.95132207870483
time_elpased: 2.332
batch start
#iterations: 310
currently lose_sum: 363.9019974768162
time_elpased: 2.304
batch start
#iterations: 311
currently lose_sum: 365.1608240902424
time_elpased: 2.287
batch start
#iterations: 312
currently lose_sum: 363.54414772987366
time_elpased: 2.358
batch start
#iterations: 313
currently lose_sum: 364.225826382637
time_elpased: 2.331
batch start
#iterations: 314
currently lose_sum: 362.4847430586815
time_elpased: 2.315
batch start
#iterations: 315
currently lose_sum: 363.51477736234665
time_elpased: 2.356
batch start
#iterations: 316
currently lose_sum: 364.6475019454956
time_elpased: 2.343
batch start
#iterations: 317
currently lose_sum: 364.3512963652611
time_elpased: 2.305
batch start
#iterations: 318
currently lose_sum: 364.797065615654
time_elpased: 2.379
batch start
#iterations: 319
currently lose_sum: 364.48977118730545
time_elpased: 2.251
start validation test
0.756907216495
1.0
0.756907216495
0.861635958221
0
validation finish
batch start
#iterations: 320
currently lose_sum: 365.65720653533936
time_elpased: 2.329
batch start
#iterations: 321
currently lose_sum: 366.95749950408936
time_elpased: 2.314
batch start
#iterations: 322
currently lose_sum: 363.55650490522385
time_elpased: 2.244
batch start
#iterations: 323
currently lose_sum: 363.3414358496666
time_elpased: 2.297
batch start
#iterations: 324
currently lose_sum: 363.1742831468582
time_elpased: 2.303
batch start
#iterations: 325
currently lose_sum: 361.90288829803467
time_elpased: 2.334
batch start
#iterations: 326
currently lose_sum: 364.0762565732002
time_elpased: 2.27
batch start
#iterations: 327
currently lose_sum: 364.69900846481323
time_elpased: 2.292
batch start
#iterations: 328
currently lose_sum: 363.22057193517685
time_elpased: 2.323
batch start
#iterations: 329
currently lose_sum: 363.2216032743454
time_elpased: 2.3
batch start
#iterations: 330
currently lose_sum: 365.3588638305664
time_elpased: 2.305
batch start
#iterations: 331
currently lose_sum: 364.4920948147774
time_elpased: 2.286
batch start
#iterations: 332
currently lose_sum: 364.93113297224045
time_elpased: 2.397
batch start
#iterations: 333
currently lose_sum: 361.53588622808456
time_elpased: 2.346
batch start
#iterations: 334
currently lose_sum: 363.76811784505844
time_elpased: 2.356
batch start
#iterations: 335
currently lose_sum: 363.48031932115555
time_elpased: 2.311
batch start
#iterations: 336
currently lose_sum: 361.749472618103
time_elpased: 2.343
batch start
#iterations: 337
currently lose_sum: 363.3091094493866
time_elpased: 2.264
batch start
#iterations: 338
currently lose_sum: 362.59250807762146
time_elpased: 2.254
batch start
#iterations: 339
currently lose_sum: 361.8723335266113
time_elpased: 2.282
start validation test
0.760515463918
1.0
0.760515463918
0.86396908122
0
validation finish
batch start
#iterations: 340
currently lose_sum: 363.6615976691246
time_elpased: 2.305
batch start
#iterations: 341
currently lose_sum: 363.77173978090286
time_elpased: 2.26
batch start
#iterations: 342
currently lose_sum: 363.2141078710556
time_elpased: 2.288
batch start
#iterations: 343
currently lose_sum: 363.9000797867775
time_elpased: 2.336
batch start
#iterations: 344
currently lose_sum: 362.89226961135864
time_elpased: 2.293
batch start
#iterations: 345
currently lose_sum: 364.17815643548965
time_elpased: 2.444
batch start
#iterations: 346
currently lose_sum: 362.7451711297035
time_elpased: 2.143
batch start
#iterations: 347
currently lose_sum: 361.01520401239395
time_elpased: 2.28
batch start
#iterations: 348
currently lose_sum: 364.1082236170769
time_elpased: 2.359
batch start
#iterations: 349
currently lose_sum: 364.73898181319237
time_elpased: 2.374
batch start
#iterations: 350
currently lose_sum: 364.10995668172836
time_elpased: 2.255
batch start
#iterations: 351
currently lose_sum: 362.54829424619675
time_elpased: 2.28
batch start
#iterations: 352
currently lose_sum: 362.11853009462357
time_elpased: 2.296
batch start
#iterations: 353
currently lose_sum: 363.54056745767593
time_elpased: 2.397
batch start
#iterations: 354
currently lose_sum: 362.7871193885803
time_elpased: 2.342
batch start
#iterations: 355
currently lose_sum: 363.1934723854065
time_elpased: 2.338
batch start
#iterations: 356
currently lose_sum: 363.84154653549194
time_elpased: 2.374
batch start
#iterations: 357
currently lose_sum: 364.16763496398926
time_elpased: 2.332
batch start
#iterations: 358
currently lose_sum: 364.6944178342819
time_elpased: 2.298
batch start
#iterations: 359
currently lose_sum: 363.32775604724884
time_elpased: 2.327
start validation test
0.760103092784
1.0
0.760103092784
0.863702922744
0
validation finish
batch start
#iterations: 360
currently lose_sum: 362.9250955581665
time_elpased: 2.385
batch start
#iterations: 361
currently lose_sum: 363.84762293100357
time_elpased: 2.319
batch start
#iterations: 362
currently lose_sum: 364.6293008327484
time_elpased: 2.334
batch start
#iterations: 363
currently lose_sum: 363.4512654542923
time_elpased: 2.327
batch start
#iterations: 364
currently lose_sum: 363.62455001473427
time_elpased: 2.319
batch start
#iterations: 365
currently lose_sum: 364.5883442759514
time_elpased: 2.31
batch start
#iterations: 366
currently lose_sum: 364.06894516944885
time_elpased: 2.274
batch start
#iterations: 367
currently lose_sum: 363.49121618270874
time_elpased: 2.337
batch start
#iterations: 368
currently lose_sum: 363.4206892848015
time_elpased: 2.335
batch start
#iterations: 369
currently lose_sum: 362.29185581207275
time_elpased: 2.336
batch start
#iterations: 370
currently lose_sum: 362.56322690844536
time_elpased: 2.262
batch start
#iterations: 371
currently lose_sum: 362.95711225271225
time_elpased: 2.272
batch start
#iterations: 372
currently lose_sum: 361.6897924542427
time_elpased: 2.217
batch start
#iterations: 373
currently lose_sum: 363.77587473392487
time_elpased: 2.455
batch start
#iterations: 374
currently lose_sum: 362.7527442574501
time_elpased: 2.323
batch start
#iterations: 375
currently lose_sum: 363.9353213906288
time_elpased: 2.273
batch start
#iterations: 376
currently lose_sum: 363.59740030765533
time_elpased: 2.29
batch start
#iterations: 377
currently lose_sum: 363.6932507157326
time_elpased: 2.286
batch start
#iterations: 378
currently lose_sum: 362.86101108789444
time_elpased: 2.265
batch start
#iterations: 379
currently lose_sum: 362.92253118753433
time_elpased: 2.253
start validation test
0.775567010309
1.0
0.775567010309
0.873599256808
0
validation finish
batch start
#iterations: 380
currently lose_sum: 362.00739538669586
time_elpased: 2.324
batch start
#iterations: 381
currently lose_sum: 361.9627797603607
time_elpased: 2.321
batch start
#iterations: 382
currently lose_sum: 362.4288511276245
time_elpased: 2.287
batch start
#iterations: 383
currently lose_sum: 361.4201160669327
time_elpased: 2.324
batch start
#iterations: 384
currently lose_sum: 363.2227084636688
time_elpased: 2.335
batch start
#iterations: 385
currently lose_sum: 363.8917011618614
time_elpased: 2.343
batch start
#iterations: 386
currently lose_sum: 363.6664139032364
time_elpased: 2.223
batch start
#iterations: 387
currently lose_sum: 364.06638365983963
time_elpased: 2.295
batch start
#iterations: 388
currently lose_sum: 362.0480460524559
time_elpased: 2.261
batch start
#iterations: 389
currently lose_sum: 362.02948689460754
time_elpased: 2.27
batch start
#iterations: 390
currently lose_sum: 365.06358432769775
time_elpased: 2.217
batch start
#iterations: 391
currently lose_sum: 363.07246047258377
time_elpased: 2.355
batch start
#iterations: 392
currently lose_sum: 362.220604121685
time_elpased: 2.286
batch start
#iterations: 393
currently lose_sum: 362.45280081033707
time_elpased: 2.328
batch start
#iterations: 394
currently lose_sum: 363.3472577929497
time_elpased: 2.254
batch start
#iterations: 395
currently lose_sum: 362.6396841406822
time_elpased: 2.267
batch start
#iterations: 396
currently lose_sum: 362.1612160205841
time_elpased: 2.321
batch start
#iterations: 397
currently lose_sum: 363.7219166755676
time_elpased: 2.271
batch start
#iterations: 398
currently lose_sum: 362.69300216436386
time_elpased: 2.289
batch start
#iterations: 399
currently lose_sum: 362.936107814312
time_elpased: 2.328
start validation test
0.753608247423
1.0
0.753608247423
0.85949441505
0
validation finish
acc: 0.787
pre: 1.000
rec: 0.787
F1: 0.881
auc: 0.000
