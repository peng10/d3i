start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 394.45418506860733
time_elpased: 4.885
batch start
#iterations: 1
currently lose_sum: 382.9469496011734
time_elpased: 4.834
batch start
#iterations: 2
currently lose_sum: 378.6600251197815
time_elpased: 4.843
batch start
#iterations: 3
currently lose_sum: 373.4579379558563
time_elpased: 4.865
batch start
#iterations: 4
currently lose_sum: 372.563480257988
time_elpased: 4.879
batch start
#iterations: 5
currently lose_sum: 371.06848591566086
time_elpased: 4.828
batch start
#iterations: 6
currently lose_sum: 369.9703495502472
time_elpased: 4.851
batch start
#iterations: 7
currently lose_sum: 368.4632959365845
time_elpased: 4.829
batch start
#iterations: 8
currently lose_sum: 367.24574756622314
time_elpased: 4.835
batch start
#iterations: 9
currently lose_sum: 367.02421391010284
time_elpased: 4.822
batch start
#iterations: 10
currently lose_sum: 366.23925906419754
time_elpased: 4.853
batch start
#iterations: 11
currently lose_sum: 364.73456144332886
time_elpased: 4.845
batch start
#iterations: 12
currently lose_sum: 364.12205201387405
time_elpased: 4.826
batch start
#iterations: 13
currently lose_sum: 363.6757395863533
time_elpased: 4.838
batch start
#iterations: 14
currently lose_sum: 363.24689960479736
time_elpased: 4.872
batch start
#iterations: 15
currently lose_sum: 362.8993564248085
time_elpased: 4.842
batch start
#iterations: 16
currently lose_sum: 364.96578443050385
time_elpased: 4.85
batch start
#iterations: 17
currently lose_sum: 362.94167095422745
time_elpased: 4.837
batch start
#iterations: 18
currently lose_sum: 362.78231102228165
time_elpased: 4.849
batch start
#iterations: 19
currently lose_sum: 360.5665978193283
time_elpased: 4.874
start validation test
0.791958762887
1.0
0.791958762887
0.883902888045
0
validation finish
batch start
#iterations: 20
currently lose_sum: 360.39809983968735
time_elpased: 4.855
batch start
#iterations: 21
currently lose_sum: 361.65030950307846
time_elpased: 4.881
batch start
#iterations: 22
currently lose_sum: 362.5093729496002
time_elpased: 4.866
batch start
#iterations: 23
currently lose_sum: 360.3193263411522
time_elpased: 4.842
batch start
#iterations: 24
currently lose_sum: 361.47519743442535
time_elpased: 4.843
batch start
#iterations: 25
currently lose_sum: 361.31386440992355
time_elpased: 4.859
batch start
#iterations: 26
currently lose_sum: 360.434090256691
time_elpased: 4.84
batch start
#iterations: 27
currently lose_sum: 362.5968094468117
time_elpased: 4.86
batch start
#iterations: 28
currently lose_sum: 360.6786021888256
time_elpased: 4.869
batch start
#iterations: 29
currently lose_sum: 361.6438620686531
time_elpased: 4.851
batch start
#iterations: 30
currently lose_sum: 360.72676408290863
time_elpased: 4.829
batch start
#iterations: 31
currently lose_sum: 360.2285757660866
time_elpased: 4.863
batch start
#iterations: 32
currently lose_sum: 359.78033089637756
time_elpased: 4.823
batch start
#iterations: 33
currently lose_sum: 360.66851288080215
time_elpased: 4.829
batch start
#iterations: 34
currently lose_sum: 360.2588735818863
time_elpased: 4.845
batch start
#iterations: 35
currently lose_sum: 359.9565997123718
time_elpased: 4.868
batch start
#iterations: 36
currently lose_sum: 360.3881091475487
time_elpased: 4.839
batch start
#iterations: 37
currently lose_sum: 358.44105648994446
time_elpased: 4.866
batch start
#iterations: 38
currently lose_sum: 358.5084556341171
time_elpased: 4.834
batch start
#iterations: 39
currently lose_sum: 359.6113101243973
time_elpased: 4.87
start validation test
0.775670103093
1.0
0.775670103093
0.873664653971
0
validation finish
batch start
#iterations: 40
currently lose_sum: 358.4226599931717
time_elpased: 4.846
batch start
#iterations: 41
currently lose_sum: 358.3965662121773
time_elpased: 4.859
batch start
#iterations: 42
currently lose_sum: 359.4003646969795
time_elpased: 4.836
batch start
#iterations: 43
currently lose_sum: 357.5647280216217
time_elpased: 4.859
batch start
#iterations: 44
currently lose_sum: 357.71945610642433
time_elpased: 4.842
batch start
#iterations: 45
currently lose_sum: 357.60789546370506
time_elpased: 4.872
batch start
#iterations: 46
currently lose_sum: 360.50635343790054
time_elpased: 4.863
batch start
#iterations: 47
currently lose_sum: 359.2603590488434
time_elpased: 4.868
batch start
#iterations: 48
currently lose_sum: 357.88240933418274
time_elpased: 4.823
batch start
#iterations: 49
currently lose_sum: 357.5139230489731
time_elpased: 4.864
batch start
#iterations: 50
currently lose_sum: 361.12945449352264
time_elpased: 4.837
batch start
#iterations: 51
currently lose_sum: 359.1332570910454
time_elpased: 4.859
batch start
#iterations: 52
currently lose_sum: 358.9788528084755
time_elpased: 4.85
batch start
#iterations: 53
currently lose_sum: 359.08061772584915
time_elpased: 4.849
batch start
#iterations: 54
currently lose_sum: 357.6803090274334
time_elpased: 4.858
batch start
#iterations: 55
currently lose_sum: 357.1800768971443
time_elpased: 4.849
batch start
#iterations: 56
currently lose_sum: 359.191154897213
time_elpased: 4.846
batch start
#iterations: 57
currently lose_sum: 357.65902680158615
time_elpased: 4.86
batch start
#iterations: 58
currently lose_sum: 357.3241811990738
time_elpased: 4.859
batch start
#iterations: 59
currently lose_sum: 359.48962062597275
time_elpased: 4.847
start validation test
0.746494845361
1.0
0.746494845361
0.854849182457
0
validation finish
batch start
#iterations: 60
currently lose_sum: 357.65289175510406
time_elpased: 4.833
batch start
#iterations: 61
currently lose_sum: 357.49439573287964
time_elpased: 4.861
batch start
#iterations: 62
currently lose_sum: 357.1625724732876
time_elpased: 4.846
batch start
#iterations: 63
currently lose_sum: 359.0957788825035
time_elpased: 4.878
batch start
#iterations: 64
currently lose_sum: 358.27258026599884
time_elpased: 4.849
batch start
#iterations: 65
currently lose_sum: 357.21448546648026
time_elpased: 4.863
batch start
#iterations: 66
currently lose_sum: 358.62512546777725
time_elpased: 4.831
batch start
#iterations: 67
currently lose_sum: 359.19885048270226
time_elpased: 4.856
batch start
#iterations: 68
currently lose_sum: 357.3234015107155
time_elpased: 4.878
batch start
#iterations: 69
currently lose_sum: 357.99614226818085
time_elpased: 4.846
batch start
#iterations: 70
currently lose_sum: 357.67410975694656
time_elpased: 4.938
batch start
#iterations: 71
currently lose_sum: 357.62378427386284
time_elpased: 4.886
batch start
#iterations: 72
currently lose_sum: 356.8206711411476
time_elpased: 4.864
batch start
#iterations: 73
currently lose_sum: 357.90242087841034
time_elpased: 4.866
batch start
#iterations: 74
currently lose_sum: 358.1769714951515
time_elpased: 4.854
batch start
#iterations: 75
currently lose_sum: 356.69241109490395
time_elpased: 4.844
batch start
#iterations: 76
currently lose_sum: 358.3303636908531
time_elpased: 4.85
batch start
#iterations: 77
currently lose_sum: 357.335836827755
time_elpased: 4.855
batch start
#iterations: 78
currently lose_sum: 357.09856897592545
time_elpased: 4.855
batch start
#iterations: 79
currently lose_sum: 356.4224883913994
time_elpased: 4.848
start validation test
0.768969072165
1.0
0.768969072165
0.869397983565
0
validation finish
batch start
#iterations: 80
currently lose_sum: 356.44160771369934
time_elpased: 4.855
batch start
#iterations: 81
currently lose_sum: 358.82707357406616
time_elpased: 4.863
batch start
#iterations: 82
currently lose_sum: 357.22643834352493
time_elpased: 4.857
batch start
#iterations: 83
currently lose_sum: 356.9798022508621
time_elpased: 4.857
batch start
#iterations: 84
currently lose_sum: 357.65281215310097
time_elpased: 4.855
batch start
#iterations: 85
currently lose_sum: 357.1403703689575
time_elpased: 4.873
batch start
#iterations: 86
currently lose_sum: 358.01739996671677
time_elpased: 4.863
batch start
#iterations: 87
currently lose_sum: 357.7365024983883
time_elpased: 4.847
batch start
#iterations: 88
currently lose_sum: 355.3695437312126
time_elpased: 4.867
batch start
#iterations: 89
currently lose_sum: 356.7835566997528
time_elpased: 4.878
batch start
#iterations: 90
currently lose_sum: 356.23398900032043
time_elpased: 4.852
batch start
#iterations: 91
currently lose_sum: 356.2842370867729
time_elpased: 4.882
batch start
#iterations: 92
currently lose_sum: 356.7832522392273
time_elpased: 4.871
batch start
#iterations: 93
currently lose_sum: 359.3234797716141
time_elpased: 4.864
batch start
#iterations: 94
currently lose_sum: 358.0042628645897
time_elpased: 4.843
batch start
#iterations: 95
currently lose_sum: 354.7016668021679
time_elpased: 4.877
batch start
#iterations: 96
currently lose_sum: 355.88042414188385
time_elpased: 4.854
batch start
#iterations: 97
currently lose_sum: 356.6138234734535
time_elpased: 4.845
batch start
#iterations: 98
currently lose_sum: 355.3581038713455
time_elpased: 4.855
batch start
#iterations: 99
currently lose_sum: 358.2459378838539
time_elpased: 4.877
start validation test
0.762989690722
1.0
0.762989690722
0.865563417344
0
validation finish
batch start
#iterations: 100
currently lose_sum: 357.55986428260803
time_elpased: 4.86
batch start
#iterations: 101
currently lose_sum: 359.28384763002396
time_elpased: 4.863
batch start
#iterations: 102
currently lose_sum: 355.291113615036
time_elpased: 4.842
batch start
#iterations: 103
currently lose_sum: 357.5262850522995
time_elpased: 4.868
batch start
#iterations: 104
currently lose_sum: 357.30570244789124
time_elpased: 4.854
batch start
#iterations: 105
currently lose_sum: 356.4725806713104
time_elpased: 4.863
batch start
#iterations: 106
currently lose_sum: 356.88247990608215
time_elpased: 4.829
batch start
#iterations: 107
currently lose_sum: 354.34367060661316
time_elpased: 4.845
batch start
#iterations: 108
currently lose_sum: 356.58736848831177
time_elpased: 4.839
batch start
#iterations: 109
currently lose_sum: 356.3332445025444
time_elpased: 4.855
batch start
#iterations: 110
currently lose_sum: 356.3012945652008
time_elpased: 4.838
batch start
#iterations: 111
currently lose_sum: 355.8166091442108
time_elpased: 4.868
batch start
#iterations: 112
currently lose_sum: 355.56552785634995
time_elpased: 4.834
batch start
#iterations: 113
currently lose_sum: 356.9497631788254
time_elpased: 4.848
batch start
#iterations: 114
currently lose_sum: 357.6775808930397
time_elpased: 4.873
batch start
#iterations: 115
currently lose_sum: 355.425947368145
time_elpased: 4.852
batch start
#iterations: 116
currently lose_sum: 355.6720004081726
time_elpased: 4.846
batch start
#iterations: 117
currently lose_sum: 356.0816259384155
time_elpased: 4.844
batch start
#iterations: 118
currently lose_sum: 355.2351174354553
time_elpased: 4.874
batch start
#iterations: 119
currently lose_sum: 355.6110075712204
time_elpased: 4.866
start validation test
0.751030927835
1.0
0.751030927835
0.857815719753
0
validation finish
batch start
#iterations: 120
currently lose_sum: 355.5227816104889
time_elpased: 4.858
batch start
#iterations: 121
currently lose_sum: 355.76113349199295
time_elpased: 4.869
batch start
#iterations: 122
currently lose_sum: 356.58304953575134
time_elpased: 4.887
batch start
#iterations: 123
currently lose_sum: 354.36484283208847
time_elpased: 4.85
batch start
#iterations: 124
currently lose_sum: 354.7276381254196
time_elpased: 4.832
batch start
#iterations: 125
currently lose_sum: 355.2695525586605
time_elpased: 4.846
batch start
#iterations: 126
currently lose_sum: 356.16638857126236
time_elpased: 4.85
batch start
#iterations: 127
currently lose_sum: 356.5088216662407
time_elpased: 4.861
batch start
#iterations: 128
currently lose_sum: 357.9413164258003
time_elpased: 4.838
batch start
#iterations: 129
currently lose_sum: 354.793421626091
time_elpased: 4.874
batch start
#iterations: 130
currently lose_sum: 355.75234282016754
time_elpased: 4.864
batch start
#iterations: 131
currently lose_sum: 356.6886540353298
time_elpased: 4.896
batch start
#iterations: 132
currently lose_sum: 355.96975684165955
time_elpased: 4.868
batch start
#iterations: 133
currently lose_sum: 357.40139812231064
time_elpased: 4.867
batch start
#iterations: 134
currently lose_sum: 355.41821560263634
time_elpased: 4.878
batch start
#iterations: 135
currently lose_sum: 356.95294880867004
time_elpased: 4.863
batch start
#iterations: 136
currently lose_sum: 355.9163323044777
time_elpased: 4.856
batch start
#iterations: 137
currently lose_sum: 355.588266313076
time_elpased: 4.875
batch start
#iterations: 138
currently lose_sum: 357.04823207855225
time_elpased: 4.865
batch start
#iterations: 139
currently lose_sum: 354.5632144212723
time_elpased: 4.901
start validation test
0.769793814433
1.0
0.769793814433
0.869924855828
0
validation finish
batch start
#iterations: 140
currently lose_sum: 354.8531417250633
time_elpased: 4.883
batch start
#iterations: 141
currently lose_sum: 355.37701138854027
time_elpased: 4.845
batch start
#iterations: 142
currently lose_sum: 353.31199234724045
time_elpased: 4.847
batch start
#iterations: 143
currently lose_sum: 354.0283635854721
time_elpased: 4.862
batch start
#iterations: 144
currently lose_sum: 354.83672419190407
time_elpased: 4.862
batch start
#iterations: 145
currently lose_sum: 356.5308249592781
time_elpased: 4.862
batch start
#iterations: 146
currently lose_sum: 352.8623853623867
time_elpased: 4.865
batch start
#iterations: 147
currently lose_sum: 356.74031472206116
time_elpased: 4.874
batch start
#iterations: 148
currently lose_sum: 356.041450381279
time_elpased: 4.873
batch start
#iterations: 149
currently lose_sum: 354.8923415541649
time_elpased: 4.867
batch start
#iterations: 150
currently lose_sum: 356.0374211072922
time_elpased: 4.85
batch start
#iterations: 151
currently lose_sum: 355.794990837574
time_elpased: 4.847
batch start
#iterations: 152
currently lose_sum: 354.43652337789536
time_elpased: 4.878
batch start
#iterations: 153
currently lose_sum: 355.6399738788605
time_elpased: 4.871
batch start
#iterations: 154
currently lose_sum: 355.0621604323387
time_elpased: 4.852
batch start
#iterations: 155
currently lose_sum: 355.2449885606766
time_elpased: 4.835
batch start
#iterations: 156
currently lose_sum: 356.25241965055466
time_elpased: 4.873
batch start
#iterations: 157
currently lose_sum: 356.57703030109406
time_elpased: 4.853
batch start
#iterations: 158
currently lose_sum: 355.21148705482483
time_elpased: 4.844
batch start
#iterations: 159
currently lose_sum: 354.6449002325535
time_elpased: 4.856
start validation test
0.795154639175
1.0
0.795154639175
0.885889852409
0
validation finish
batch start
#iterations: 160
currently lose_sum: 355.880152374506
time_elpased: 4.88
batch start
#iterations: 161
currently lose_sum: 355.9443795681
time_elpased: 4.862
batch start
#iterations: 162
currently lose_sum: 355.81590086221695
time_elpased: 4.864
batch start
#iterations: 163
currently lose_sum: 354.702490568161
time_elpased: 4.883
batch start
#iterations: 164
currently lose_sum: 356.115227162838
time_elpased: 4.826
batch start
#iterations: 165
currently lose_sum: 354.5714308023453
time_elpased: 4.868
batch start
#iterations: 166
currently lose_sum: 354.9474268555641
time_elpased: 4.827
batch start
#iterations: 167
currently lose_sum: 355.8324428796768
time_elpased: 4.882
batch start
#iterations: 168
currently lose_sum: 355.79016172885895
time_elpased: 4.856
batch start
#iterations: 169
currently lose_sum: 354.9541075825691
time_elpased: 4.852
batch start
#iterations: 170
currently lose_sum: 354.91607451438904
time_elpased: 4.95
batch start
#iterations: 171
currently lose_sum: 354.24793085455894
time_elpased: 4.849
batch start
#iterations: 172
currently lose_sum: 355.3824481666088
time_elpased: 4.828
batch start
#iterations: 173
currently lose_sum: 355.95192301273346
time_elpased: 4.862
batch start
#iterations: 174
currently lose_sum: 355.0728434920311
time_elpased: 4.866
batch start
#iterations: 175
currently lose_sum: 355.20968544483185
time_elpased: 4.843
batch start
#iterations: 176
currently lose_sum: 354.83722442388535
time_elpased: 4.854
batch start
#iterations: 177
currently lose_sum: 355.1881292760372
time_elpased: 4.854
batch start
#iterations: 178
currently lose_sum: 354.8171411752701
time_elpased: 4.88
batch start
#iterations: 179
currently lose_sum: 354.7053643465042
time_elpased: 4.851
start validation test
0.751855670103
1.0
0.751855670103
0.858353439652
0
validation finish
batch start
#iterations: 180
currently lose_sum: 355.03916251659393
time_elpased: 4.875
batch start
#iterations: 181
currently lose_sum: 355.1243820786476
time_elpased: 4.847
batch start
#iterations: 182
currently lose_sum: 357.4245311021805
time_elpased: 4.834
batch start
#iterations: 183
currently lose_sum: 355.3278016448021
time_elpased: 4.856
batch start
#iterations: 184
currently lose_sum: 355.06270283460617
time_elpased: 4.858
batch start
#iterations: 185
currently lose_sum: 353.5113546848297
time_elpased: 4.84
batch start
#iterations: 186
currently lose_sum: 355.9968124628067
time_elpased: 4.878
batch start
#iterations: 187
currently lose_sum: 354.846864759922
time_elpased: 4.843
batch start
#iterations: 188
currently lose_sum: 354.3905334472656
time_elpased: 4.839
batch start
#iterations: 189
currently lose_sum: 355.88168543577194
time_elpased: 4.847
batch start
#iterations: 190
currently lose_sum: 355.55509811639786
time_elpased: 4.852
batch start
#iterations: 191
currently lose_sum: 356.4380056858063
time_elpased: 4.877
batch start
#iterations: 192
currently lose_sum: 355.76957750320435
time_elpased: 4.855
batch start
#iterations: 193
currently lose_sum: 354.91063034534454
time_elpased: 4.89
batch start
#iterations: 194
currently lose_sum: 353.89371621608734
time_elpased: 4.843
batch start
#iterations: 195
currently lose_sum: 354.1285210251808
time_elpased: 4.858
batch start
#iterations: 196
currently lose_sum: 355.1100689768791
time_elpased: 4.854
batch start
#iterations: 197
currently lose_sum: 357.3439041376114
time_elpased: 4.839
batch start
#iterations: 198
currently lose_sum: 355.4745010137558
time_elpased: 4.84
batch start
#iterations: 199
currently lose_sum: 355.7358441352844
time_elpased: 4.858
start validation test
0.772989690722
1.0
0.772989690722
0.87196185603
0
validation finish
batch start
#iterations: 200
currently lose_sum: 353.59878918528557
time_elpased: 4.893
batch start
#iterations: 201
currently lose_sum: 355.43779051303864
time_elpased: 4.862
batch start
#iterations: 202
currently lose_sum: 356.00728729367256
time_elpased: 4.861
batch start
#iterations: 203
currently lose_sum: 353.42807617783546
time_elpased: 4.85
batch start
#iterations: 204
currently lose_sum: 354.84186214208603
time_elpased: 4.871
batch start
#iterations: 205
currently lose_sum: 355.15798938274384
time_elpased: 4.851
batch start
#iterations: 206
currently lose_sum: 356.0158554315567
time_elpased: 4.862
batch start
#iterations: 207
currently lose_sum: 354.28224992752075
time_elpased: 4.858
batch start
#iterations: 208
currently lose_sum: 355.837018430233
time_elpased: 4.868
batch start
#iterations: 209
currently lose_sum: 351.92052403092384
time_elpased: 4.861
batch start
#iterations: 210
currently lose_sum: 355.2784701883793
time_elpased: 4.875
batch start
#iterations: 211
currently lose_sum: 353.5392484664917
time_elpased: 4.855
batch start
#iterations: 212
currently lose_sum: 354.0018099844456
time_elpased: 4.855
batch start
#iterations: 213
currently lose_sum: 353.79245990514755
time_elpased: 4.835
batch start
#iterations: 214
currently lose_sum: 355.3697243630886
time_elpased: 4.862
batch start
#iterations: 215
currently lose_sum: 353.96721637248993
time_elpased: 4.857
batch start
#iterations: 216
currently lose_sum: 354.38361179828644
time_elpased: 4.852
batch start
#iterations: 217
currently lose_sum: 354.76392582058907
time_elpased: 4.848
batch start
#iterations: 218
currently lose_sum: 353.6437305212021
time_elpased: 5.014
batch start
#iterations: 219
currently lose_sum: 356.3927544951439
time_elpased: 4.84
start validation test
0.775154639175
1.0
0.775154639175
0.873337592195
0
validation finish
batch start
#iterations: 220
currently lose_sum: 354.66839838027954
time_elpased: 4.833
batch start
#iterations: 221
currently lose_sum: 354.2874478697777
time_elpased: 4.857
batch start
#iterations: 222
currently lose_sum: 353.91929474473
time_elpased: 4.844
batch start
#iterations: 223
currently lose_sum: 354.51952213048935
time_elpased: 4.854
batch start
#iterations: 224
currently lose_sum: 352.439732670784
time_elpased: 4.85
batch start
#iterations: 225
currently lose_sum: 354.610458612442
time_elpased: 4.858
batch start
#iterations: 226
currently lose_sum: 355.40266621112823
time_elpased: 4.858
batch start
#iterations: 227
currently lose_sum: 353.76758444309235
time_elpased: 4.844
batch start
#iterations: 228
currently lose_sum: 354.1902075409889
time_elpased: 4.836
batch start
#iterations: 229
currently lose_sum: 353.6201663315296
time_elpased: 4.836
batch start
#iterations: 230
currently lose_sum: 354.79651713371277
time_elpased: 4.882
batch start
#iterations: 231
currently lose_sum: 355.4536028802395
time_elpased: 4.845
batch start
#iterations: 232
currently lose_sum: 355.78504914045334
time_elpased: 4.858
batch start
#iterations: 233
currently lose_sum: 353.2721438407898
time_elpased: 4.839
batch start
#iterations: 234
currently lose_sum: 352.27063724398613
time_elpased: 4.843
batch start
#iterations: 235
currently lose_sum: 354.9147650003433
time_elpased: 4.858
batch start
#iterations: 236
currently lose_sum: 354.1894033551216
time_elpased: 4.863
batch start
#iterations: 237
currently lose_sum: 354.97378572821617
time_elpased: 4.838
batch start
#iterations: 238
currently lose_sum: 354.1735867857933
time_elpased: 4.852
batch start
#iterations: 239
currently lose_sum: 353.523480117321
time_elpased: 4.894
start validation test
0.753402061856
1.0
0.753402061856
0.859360301035
0
validation finish
batch start
#iterations: 240
currently lose_sum: 355.1563295722008
time_elpased: 4.845
batch start
#iterations: 241
currently lose_sum: 353.45000952482224
time_elpased: 4.843
batch start
#iterations: 242
currently lose_sum: 352.2468782067299
time_elpased: 4.866
batch start
#iterations: 243
currently lose_sum: 356.89551094174385
time_elpased: 4.874
batch start
#iterations: 244
currently lose_sum: 355.93117639422417
time_elpased: 4.844
batch start
#iterations: 245
currently lose_sum: 355.4686835408211
time_elpased: 4.88
batch start
#iterations: 246
currently lose_sum: 354.29293194413185
time_elpased: 4.838
batch start
#iterations: 247
currently lose_sum: 354.4183052778244
time_elpased: 4.865
batch start
#iterations: 248
currently lose_sum: 355.1888445317745
time_elpased: 4.871
batch start
#iterations: 249
currently lose_sum: 356.2867664694786
time_elpased: 4.842
batch start
#iterations: 250
currently lose_sum: 354.70638224482536
time_elpased: 4.9
batch start
#iterations: 251
currently lose_sum: 354.9896811544895
time_elpased: 4.827
batch start
#iterations: 252
currently lose_sum: 354.13670486211777
time_elpased: 4.871
batch start
#iterations: 253
currently lose_sum: 354.3778237402439
time_elpased: 4.877
batch start
#iterations: 254
currently lose_sum: 353.99948528409004
time_elpased: 4.849
batch start
#iterations: 255
currently lose_sum: 355.85826247930527
time_elpased: 4.851
batch start
#iterations: 256
currently lose_sum: 355.10671842098236
time_elpased: 4.869
batch start
#iterations: 257
currently lose_sum: 354.4684065580368
time_elpased: 4.867
batch start
#iterations: 258
currently lose_sum: 355.62641417980194
time_elpased: 4.86
batch start
#iterations: 259
currently lose_sum: 356.1928765773773
time_elpased: 4.886
start validation test
0.764329896907
1.0
0.764329896907
0.866425149001
0
validation finish
batch start
#iterations: 260
currently lose_sum: 353.26563906669617
time_elpased: 4.858
batch start
#iterations: 261
currently lose_sum: 355.38681864738464
time_elpased: 4.888
batch start
#iterations: 262
currently lose_sum: 353.64484763145447
time_elpased: 4.865
batch start
#iterations: 263
currently lose_sum: 354.4204462170601
time_elpased: 4.832
batch start
#iterations: 264
currently lose_sum: 353.8833564221859
time_elpased: 4.849
batch start
#iterations: 265
currently lose_sum: 354.12489035725594
time_elpased: 4.865
batch start
#iterations: 266
currently lose_sum: 353.25031250715256
time_elpased: 4.854
batch start
#iterations: 267
currently lose_sum: 354.45526215434074
time_elpased: 4.875
batch start
#iterations: 268
currently lose_sum: 352.7702152132988
time_elpased: 4.841
batch start
#iterations: 269
currently lose_sum: 353.80694022774696
time_elpased: 4.859
batch start
#iterations: 270
currently lose_sum: 352.9034908413887
time_elpased: 4.841
batch start
#iterations: 271
currently lose_sum: 355.41853046417236
time_elpased: 4.845
batch start
#iterations: 272
currently lose_sum: 353.84815779328346
time_elpased: 4.9
batch start
#iterations: 273
currently lose_sum: 353.1064420938492
time_elpased: 4.945
batch start
#iterations: 274
currently lose_sum: 353.7363750934601
time_elpased: 4.846
batch start
#iterations: 275
currently lose_sum: 355.13218253850937
time_elpased: 4.833
batch start
#iterations: 276
currently lose_sum: 352.77555280923843
time_elpased: 4.883
batch start
#iterations: 277
currently lose_sum: 354.2755063176155
time_elpased: 4.876
batch start
#iterations: 278
currently lose_sum: 354.17220652103424
time_elpased: 4.841
batch start
#iterations: 279
currently lose_sum: 353.80570137500763
time_elpased: 4.85
start validation test
0.771134020619
1.0
0.771134020619
0.870779976717
0
validation finish
batch start
#iterations: 280
currently lose_sum: 352.70649468898773
time_elpased: 4.857
batch start
#iterations: 281
currently lose_sum: 353.3926566839218
time_elpased: 4.86
batch start
#iterations: 282
currently lose_sum: 354.02042603492737
time_elpased: 4.859
batch start
#iterations: 283
currently lose_sum: 352.0135074555874
time_elpased: 4.854
batch start
#iterations: 284
currently lose_sum: 353.9187936782837
time_elpased: 4.905
batch start
#iterations: 285
currently lose_sum: 353.4559642672539
time_elpased: 4.857
batch start
#iterations: 286
currently lose_sum: 354.81315916776657
time_elpased: 4.89
batch start
#iterations: 287
currently lose_sum: 356.32367193698883
time_elpased: 4.825
batch start
#iterations: 288
currently lose_sum: 354.54335564374924
time_elpased: 4.862
batch start
#iterations: 289
currently lose_sum: 351.83280551433563
time_elpased: 4.836
batch start
#iterations: 290
currently lose_sum: 352.829543530941
time_elpased: 4.846
batch start
#iterations: 291
currently lose_sum: 353.92021599411964
time_elpased: 4.83
batch start
#iterations: 292
currently lose_sum: 354.12642258405685
time_elpased: 4.869
batch start
#iterations: 293
currently lose_sum: 353.6742690205574
time_elpased: 4.842
batch start
#iterations: 294
currently lose_sum: 353.42376032471657
time_elpased: 4.885
batch start
#iterations: 295
currently lose_sum: 353.13164803385735
time_elpased: 4.847
batch start
#iterations: 296
currently lose_sum: 352.9519128501415
time_elpased: 4.848
batch start
#iterations: 297
currently lose_sum: 354.6032339334488
time_elpased: 4.868
batch start
#iterations: 298
currently lose_sum: 356.16673630476
time_elpased: 4.867
batch start
#iterations: 299
currently lose_sum: 353.82946968078613
time_elpased: 4.877
start validation test
0.781134020619
1.0
0.781134020619
0.877119870348
0
validation finish
batch start
#iterations: 300
currently lose_sum: 353.7045319080353
time_elpased: 4.843
batch start
#iterations: 301
currently lose_sum: 354.21180188655853
time_elpased: 4.895
batch start
#iterations: 302
currently lose_sum: 353.47953337430954
time_elpased: 4.874
batch start
#iterations: 303
currently lose_sum: 353.0622702240944
time_elpased: 4.876
batch start
#iterations: 304
currently lose_sum: 353.6807614862919
time_elpased: 4.865
batch start
#iterations: 305
currently lose_sum: 354.70050686597824
time_elpased: 4.855
batch start
#iterations: 306
currently lose_sum: 354.246947824955
time_elpased: 4.861
batch start
#iterations: 307
currently lose_sum: 354.6040860414505
time_elpased: 4.87
batch start
#iterations: 308
currently lose_sum: 353.1389222741127
time_elpased: 4.861
batch start
#iterations: 309
currently lose_sum: 353.2948327064514
time_elpased: 4.848
batch start
#iterations: 310
currently lose_sum: 354.54174092411995
time_elpased: 4.837
batch start
#iterations: 311
currently lose_sum: 355.89830243587494
time_elpased: 4.834
batch start
#iterations: 312
currently lose_sum: 353.65687713027
time_elpased: 4.861
batch start
#iterations: 313
currently lose_sum: 353.51004910469055
time_elpased: 4.861
batch start
#iterations: 314
currently lose_sum: 353.342671751976
time_elpased: 4.856
batch start
#iterations: 315
currently lose_sum: 354.2969390153885
time_elpased: 4.863
batch start
#iterations: 316
currently lose_sum: 353.0903117656708
time_elpased: 4.871
batch start
#iterations: 317
currently lose_sum: 353.889553129673
time_elpased: 4.866
batch start
#iterations: 318
currently lose_sum: 354.79742604494095
time_elpased: 4.862
batch start
#iterations: 319
currently lose_sum: 352.9118342399597
time_elpased: 4.884
start validation test
0.779793814433
1.0
0.779793814433
0.876274328082
0
validation finish
batch start
#iterations: 320
currently lose_sum: 353.67295691370964
time_elpased: 4.842
batch start
#iterations: 321
currently lose_sum: 354.0704135000706
time_elpased: 4.866
batch start
#iterations: 322
currently lose_sum: 353.284364759922
time_elpased: 4.851
batch start
#iterations: 323
currently lose_sum: 354.3529630303383
time_elpased: 4.871
batch start
#iterations: 324
currently lose_sum: 352.22399893403053
time_elpased: 4.85
batch start
#iterations: 325
currently lose_sum: 352.46321696043015
time_elpased: 4.87
batch start
#iterations: 326
currently lose_sum: 353.52355420589447
time_elpased: 4.864
batch start
#iterations: 327
currently lose_sum: 354.49095010757446
time_elpased: 4.876
batch start
#iterations: 328
currently lose_sum: 355.44787296652794
time_elpased: 4.867
batch start
#iterations: 329
currently lose_sum: 355.5065594315529
time_elpased: 4.836
batch start
#iterations: 330
currently lose_sum: 352.90160807967186
time_elpased: 4.877
batch start
#iterations: 331
currently lose_sum: 355.6900403499603
time_elpased: 4.847
batch start
#iterations: 332
currently lose_sum: 353.6387429833412
time_elpased: 4.876
batch start
#iterations: 333
currently lose_sum: 354.9695283770561
time_elpased: 4.873
batch start
#iterations: 334
currently lose_sum: 353.0222893357277
time_elpased: 4.874
batch start
#iterations: 335
currently lose_sum: 352.8823421895504
time_elpased: 4.89
batch start
#iterations: 336
currently lose_sum: 352.61918276548386
time_elpased: 4.852
batch start
#iterations: 337
currently lose_sum: 354.37051874399185
time_elpased: 4.855
batch start
#iterations: 338
currently lose_sum: 352.83931332826614
time_elpased: 4.853
batch start
#iterations: 339
currently lose_sum: 353.44957491755486
time_elpased: 4.852
start validation test
0.782989690722
1.0
0.782989690722
0.878288522694
0
validation finish
batch start
#iterations: 340
currently lose_sum: 353.16637951135635
time_elpased: 4.863
batch start
#iterations: 341
currently lose_sum: 352.7975978255272
time_elpased: 4.85
batch start
#iterations: 342
currently lose_sum: 354.04336616396904
time_elpased: 4.837
batch start
#iterations: 343
currently lose_sum: 353.38675478100777
time_elpased: 4.874
batch start
#iterations: 344
currently lose_sum: 353.4503105580807
time_elpased: 4.856
batch start
#iterations: 345
currently lose_sum: 354.01214304566383
time_elpased: 4.847
batch start
#iterations: 346
currently lose_sum: 353.89938989281654
time_elpased: 4.867
batch start
#iterations: 347
currently lose_sum: 353.7665456831455
time_elpased: 4.888
batch start
#iterations: 348
currently lose_sum: 354.10786670446396
time_elpased: 4.855
batch start
#iterations: 349
currently lose_sum: 354.60294210910797
time_elpased: 4.842
batch start
#iterations: 350
currently lose_sum: 353.8704379796982
time_elpased: 4.857
batch start
#iterations: 351
currently lose_sum: 354.0659585595131
time_elpased: 4.85
batch start
#iterations: 352
currently lose_sum: 352.45306235551834
time_elpased: 4.858
batch start
#iterations: 353
currently lose_sum: 353.80306243896484
time_elpased: 4.848
batch start
#iterations: 354
currently lose_sum: 353.21444299817085
time_elpased: 4.846
batch start
#iterations: 355
currently lose_sum: 352.26372116804123
time_elpased: 4.865
batch start
#iterations: 356
currently lose_sum: 353.98944050073624
time_elpased: 4.877
batch start
#iterations: 357
currently lose_sum: 353.09769481420517
time_elpased: 4.854
batch start
#iterations: 358
currently lose_sum: 353.3509730398655
time_elpased: 4.878
batch start
#iterations: 359
currently lose_sum: 352.8005541861057
time_elpased: 4.848
start validation test
0.774845360825
1.0
0.774845360825
0.873141263941
0
validation finish
batch start
#iterations: 360
currently lose_sum: 354.7428832948208
time_elpased: 4.87
batch start
#iterations: 361
currently lose_sum: 352.9504637122154
time_elpased: 4.86
batch start
#iterations: 362
currently lose_sum: 352.9005138874054
time_elpased: 4.863
batch start
#iterations: 363
currently lose_sum: 353.6802808046341
time_elpased: 4.843
batch start
#iterations: 364
currently lose_sum: 354.99129924178123
time_elpased: 4.871
batch start
#iterations: 365
currently lose_sum: 353.4714701771736
time_elpased: 4.864
batch start
#iterations: 366
currently lose_sum: 353.2479708790779
time_elpased: 4.859
batch start
#iterations: 367
currently lose_sum: 354.5840142071247
time_elpased: 4.839
batch start
#iterations: 368
currently lose_sum: 353.8198729753494
time_elpased: 4.847
batch start
#iterations: 369
currently lose_sum: 355.5286691188812
time_elpased: 4.855
batch start
#iterations: 370
currently lose_sum: 354.10127437114716
time_elpased: 4.868
batch start
#iterations: 371
currently lose_sum: 353.91629058122635
time_elpased: 4.846
batch start
#iterations: 372
currently lose_sum: 353.91954433918
time_elpased: 4.849
batch start
#iterations: 373
currently lose_sum: 353.072642236948
time_elpased: 4.858
batch start
#iterations: 374
currently lose_sum: 354.1182744503021
time_elpased: 4.862
batch start
#iterations: 375
currently lose_sum: 355.7621927559376
time_elpased: 4.834
batch start
#iterations: 376
currently lose_sum: 353.7028415799141
time_elpased: 4.852
batch start
#iterations: 377
currently lose_sum: 352.69193491339684
time_elpased: 4.855
batch start
#iterations: 378
currently lose_sum: 355.01988619565964
time_elpased: 4.853
batch start
#iterations: 379
currently lose_sum: 353.20490342378616
time_elpased: 4.883
start validation test
0.769896907216
1.0
0.769896907216
0.869990680336
0
validation finish
batch start
#iterations: 380
currently lose_sum: 354.1372453570366
time_elpased: 4.885
batch start
#iterations: 381
currently lose_sum: 353.99981424212456
time_elpased: 4.835
batch start
#iterations: 382
currently lose_sum: 353.86939656734467
time_elpased: 4.853
batch start
#iterations: 383
currently lose_sum: 351.60525301098824
time_elpased: 4.883
batch start
#iterations: 384
currently lose_sum: 353.80362263321877
time_elpased: 4.867
batch start
#iterations: 385
currently lose_sum: 352.27848893404007
time_elpased: 4.881
batch start
#iterations: 386
currently lose_sum: 353.9593031704426
time_elpased: 4.857
batch start
#iterations: 387
currently lose_sum: 352.94063290953636
time_elpased: 4.835
batch start
#iterations: 388
currently lose_sum: 353.5229859948158
time_elpased: 4.843
batch start
#iterations: 389
currently lose_sum: 353.39724284410477
time_elpased: 4.865
batch start
#iterations: 390
currently lose_sum: 352.4305148720741
time_elpased: 4.865
batch start
#iterations: 391
currently lose_sum: 352.27082872390747
time_elpased: 4.842
batch start
#iterations: 392
currently lose_sum: 354.197755664587
time_elpased: 4.857
batch start
#iterations: 393
currently lose_sum: 354.83296048641205
time_elpased: 4.863
batch start
#iterations: 394
currently lose_sum: 352.80106741189957
time_elpased: 4.834
batch start
#iterations: 395
currently lose_sum: 353.4822973012924
time_elpased: 4.842
batch start
#iterations: 396
currently lose_sum: 353.46929436922073
time_elpased: 4.886
batch start
#iterations: 397
currently lose_sum: 353.41808688640594
time_elpased: 4.864
batch start
#iterations: 398
currently lose_sum: 354.0212258398533
time_elpased: 4.878
batch start
#iterations: 399
currently lose_sum: 354.83559054136276
time_elpased: 4.834
start validation test
0.77824742268
1.0
0.77824742268
0.875297118674
0
validation finish
acc: 0.813
pre: 1.000
rec: 0.813
F1: 0.897
auc: 0.000
