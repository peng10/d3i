start to construct graph
graph construct over
58302
epochs start
batch start
#iterations: 0
currently lose_sum: 395.15651500225067
time_elpased: 5.159
batch start
#iterations: 1
currently lose_sum: 384.2629595398903
time_elpased: 5.101
batch start
#iterations: 2
currently lose_sum: 378.50792413949966
time_elpased: 5.117
batch start
#iterations: 3
currently lose_sum: 374.40192580223083
time_elpased: 5.12
batch start
#iterations: 4
currently lose_sum: 372.2868379354477
time_elpased: 5.119
batch start
#iterations: 5
currently lose_sum: 369.05229848623276
time_elpased: 5.115
batch start
#iterations: 6
currently lose_sum: 369.46963769197464
time_elpased: 5.131
batch start
#iterations: 7
currently lose_sum: 366.00667893886566
time_elpased: 5.118
batch start
#iterations: 8
currently lose_sum: 366.5656687319279
time_elpased: 5.152
batch start
#iterations: 9
currently lose_sum: 366.7336593270302
time_elpased: 5.127
batch start
#iterations: 10
currently lose_sum: 364.84602731466293
time_elpased: 5.115
batch start
#iterations: 11
currently lose_sum: 363.7403073310852
time_elpased: 5.118
batch start
#iterations: 12
currently lose_sum: 364.35109412670135
time_elpased: 5.107
batch start
#iterations: 13
currently lose_sum: 364.2650392651558
time_elpased: 5.092
batch start
#iterations: 14
currently lose_sum: 364.18454790115356
time_elpased: 5.13
batch start
#iterations: 15
currently lose_sum: 361.55132818222046
time_elpased: 5.12
batch start
#iterations: 16
currently lose_sum: 362.3798128962517
time_elpased: 5.102
batch start
#iterations: 17
currently lose_sum: 361.6256023645401
time_elpased: 5.149
batch start
#iterations: 18
currently lose_sum: 360.5939794778824
time_elpased: 5.103
batch start
#iterations: 19
currently lose_sum: 363.1550793647766
time_elpased: 5.162
start validation test
0.783505154639
1.0
0.783505154639
0.878612716763
0
validation finish
batch start
#iterations: 20
currently lose_sum: 361.4139032661915
time_elpased: 5.11
batch start
#iterations: 21
currently lose_sum: 362.5485137104988
time_elpased: 5.107
batch start
#iterations: 22
currently lose_sum: 360.2675891518593
time_elpased: 5.133
batch start
#iterations: 23
currently lose_sum: 361.8205563426018
time_elpased: 5.136
batch start
#iterations: 24
currently lose_sum: 359.71130043268204
time_elpased: 5.113
batch start
#iterations: 25
currently lose_sum: 360.20873177051544
time_elpased: 5.116
batch start
#iterations: 26
currently lose_sum: 359.9544820189476
time_elpased: 5.127
batch start
#iterations: 27
currently lose_sum: 359.6433948278427
time_elpased: 5.115
batch start
#iterations: 28
currently lose_sum: 359.61171209812164
time_elpased: 5.135
batch start
#iterations: 29
currently lose_sum: 359.80005353689194
time_elpased: 5.143
batch start
#iterations: 30
currently lose_sum: 358.7085949778557
time_elpased: 5.126
batch start
#iterations: 31
currently lose_sum: 359.94903713464737
time_elpased: 5.161
batch start
#iterations: 32
currently lose_sum: 360.118202149868
time_elpased: 5.114
batch start
#iterations: 33
currently lose_sum: 358.82103955745697
time_elpased: 5.13
batch start
#iterations: 34
currently lose_sum: 357.9832772612572
time_elpased: 5.127
batch start
#iterations: 35
currently lose_sum: 357.0990347266197
time_elpased: 5.119
batch start
#iterations: 36
currently lose_sum: 357.6117179989815
time_elpased: 5.114
batch start
#iterations: 37
currently lose_sum: 358.81210362911224
time_elpased: 5.102
batch start
#iterations: 38
currently lose_sum: 359.59411722421646
time_elpased: 5.143
batch start
#iterations: 39
currently lose_sum: 359.1491293311119
time_elpased: 5.125
start validation test
0.790721649485
1.0
0.790721649485
0.8831318365
0
validation finish
batch start
#iterations: 40
currently lose_sum: 357.9923793077469
time_elpased: 5.136
batch start
#iterations: 41
currently lose_sum: 358.4501855969429
time_elpased: 5.126
batch start
#iterations: 42
currently lose_sum: 358.1302162408829
time_elpased: 5.134
batch start
#iterations: 43
currently lose_sum: 357.5834656357765
time_elpased: 5.144
batch start
#iterations: 44
currently lose_sum: 356.67286962270737
time_elpased: 5.127
batch start
#iterations: 45
currently lose_sum: 358.3330253958702
time_elpased: 5.106
batch start
#iterations: 46
currently lose_sum: 357.4224828481674
time_elpased: 5.15
batch start
#iterations: 47
currently lose_sum: 358.1745819449425
time_elpased: 5.104
batch start
#iterations: 48
currently lose_sum: 358.0883713364601
time_elpased: 5.126
batch start
#iterations: 49
currently lose_sum: 358.34728783369064
time_elpased: 5.13
batch start
#iterations: 50
currently lose_sum: 358.07827466726303
time_elpased: 5.117
batch start
#iterations: 51
currently lose_sum: 357.62903463840485
time_elpased: 5.132
batch start
#iterations: 52
currently lose_sum: 358.6409035921097
time_elpased: 5.124
batch start
#iterations: 53
currently lose_sum: 357.70320576429367
time_elpased: 5.129
batch start
#iterations: 54
currently lose_sum: 359.52496659755707
time_elpased: 5.139
batch start
#iterations: 55
currently lose_sum: 357.16184347867966
time_elpased: 5.146
batch start
#iterations: 56
currently lose_sum: 356.2721723020077
time_elpased: 5.128
batch start
#iterations: 57
currently lose_sum: 356.0512225031853
time_elpased: 5.123
batch start
#iterations: 58
currently lose_sum: 357.7163615822792
time_elpased: 5.147
batch start
#iterations: 59
currently lose_sum: 356.9709953069687
time_elpased: 5.131
start validation test
0.751340206186
1.0
0.751340206186
0.858017424064
0
validation finish
batch start
#iterations: 60
currently lose_sum: 357.3564047217369
time_elpased: 5.144
batch start
#iterations: 61
currently lose_sum: 358.4397297501564
time_elpased: 5.146
batch start
#iterations: 62
currently lose_sum: 357.61228716373444
time_elpased: 5.116
batch start
#iterations: 63
currently lose_sum: 356.7057668566704
time_elpased: 5.146
batch start
#iterations: 64
currently lose_sum: 358.20282673835754
time_elpased: 5.118
batch start
#iterations: 65
currently lose_sum: 356.8462235033512
time_elpased: 5.14
batch start
#iterations: 66
currently lose_sum: 357.54468458890915
time_elpased: 5.147
batch start
#iterations: 67
currently lose_sum: 356.52475130558014
time_elpased: 5.115
batch start
#iterations: 68
currently lose_sum: 356.5663597583771
time_elpased: 5.114
batch start
#iterations: 69
currently lose_sum: 356.35788971185684
time_elpased: 5.124
batch start
#iterations: 70
currently lose_sum: 357.00472301244736
time_elpased: 5.123
batch start
#iterations: 71
currently lose_sum: 357.80861353874207
time_elpased: 5.151
batch start
#iterations: 72
currently lose_sum: 354.8917310833931
time_elpased: 5.131
batch start
#iterations: 73
currently lose_sum: 356.91939187049866
time_elpased: 5.144
batch start
#iterations: 74
currently lose_sum: 357.7311466932297
time_elpased: 5.136
batch start
#iterations: 75
currently lose_sum: 356.6411320567131
time_elpased: 5.163
batch start
#iterations: 76
currently lose_sum: 357.24418926239014
time_elpased: 5.141
batch start
#iterations: 77
currently lose_sum: 355.424781024456
time_elpased: 5.114
batch start
#iterations: 78
currently lose_sum: 357.35108309984207
time_elpased: 5.132
batch start
#iterations: 79
currently lose_sum: 354.75383284687996
time_elpased: 5.147
start validation test
0.780309278351
1.0
0.780309278351
0.876599687301
0
validation finish
batch start
#iterations: 80
currently lose_sum: 356.59438169002533
time_elpased: 5.131
batch start
#iterations: 81
currently lose_sum: 357.49183109402657
time_elpased: 5.143
batch start
#iterations: 82
currently lose_sum: 356.4342491030693
time_elpased: 5.127
batch start
#iterations: 83
currently lose_sum: 355.2970808148384
time_elpased: 5.153
batch start
#iterations: 84
currently lose_sum: 356.14670753479004
time_elpased: 5.11
batch start
#iterations: 85
currently lose_sum: 355.8013268709183
time_elpased: 5.14
batch start
#iterations: 86
currently lose_sum: 354.8621542453766
time_elpased: 5.121
batch start
#iterations: 87
currently lose_sum: 356.76176711916924
time_elpased: 5.184
batch start
#iterations: 88
currently lose_sum: 355.836927741766
time_elpased: 5.11
batch start
#iterations: 89
currently lose_sum: 355.6395550966263
time_elpased: 5.129
batch start
#iterations: 90
currently lose_sum: 357.06732136011124
time_elpased: 5.151
batch start
#iterations: 91
currently lose_sum: 355.29463571310043
time_elpased: 5.15
batch start
#iterations: 92
currently lose_sum: 356.8204730153084
time_elpased: 5.146
batch start
#iterations: 93
currently lose_sum: 355.18134620785713
time_elpased: 5.112
batch start
#iterations: 94
currently lose_sum: 357.54779148101807
time_elpased: 5.153
batch start
#iterations: 95
currently lose_sum: 354.6456136107445
time_elpased: 5.119
batch start
#iterations: 96
currently lose_sum: 358.1403212547302
time_elpased: 5.138
batch start
#iterations: 97
currently lose_sum: 356.50321185588837
time_elpased: 5.147
batch start
#iterations: 98
currently lose_sum: 355.9215233325958
time_elpased: 5.134
batch start
#iterations: 99
currently lose_sum: 355.30351758003235
time_elpased: 5.186
start validation test
0.768969072165
1.0
0.768969072165
0.869397983565
0
validation finish
batch start
#iterations: 100
currently lose_sum: 356.6886818408966
time_elpased: 5.163
batch start
#iterations: 101
currently lose_sum: 356.73248359560966
time_elpased: 5.158
batch start
#iterations: 102
currently lose_sum: 356.3407509922981
time_elpased: 5.158
batch start
#iterations: 103
currently lose_sum: 356.2258830964565
time_elpased: 5.121
batch start
#iterations: 104
currently lose_sum: 356.063340485096
time_elpased: 5.133
batch start
#iterations: 105
currently lose_sum: 355.91774195432663
time_elpased: 5.137
batch start
#iterations: 106
currently lose_sum: 354.52232879400253
time_elpased: 5.162
batch start
#iterations: 107
currently lose_sum: 356.861935377121
time_elpased: 5.146
batch start
#iterations: 108
currently lose_sum: 356.0753691494465
time_elpased: 5.173
batch start
#iterations: 109
currently lose_sum: 355.0947441458702
time_elpased: 5.136
batch start
#iterations: 110
currently lose_sum: 356.20319986343384
time_elpased: 5.153
batch start
#iterations: 111
currently lose_sum: 354.88665622472763
time_elpased: 5.168
batch start
#iterations: 112
currently lose_sum: 355.15084421634674
time_elpased: 5.139
batch start
#iterations: 113
currently lose_sum: 356.00588098168373
time_elpased: 5.14
batch start
#iterations: 114
currently lose_sum: 355.5449075102806
time_elpased: 5.133
batch start
#iterations: 115
currently lose_sum: 354.9776663184166
time_elpased: 5.11
batch start
#iterations: 116
currently lose_sum: 355.66889786720276
time_elpased: 5.165
batch start
#iterations: 117
currently lose_sum: 355.75886702537537
time_elpased: 5.145
batch start
#iterations: 118
currently lose_sum: 354.12886530160904
time_elpased: 5.135
batch start
#iterations: 119
currently lose_sum: 357.2556042075157
time_elpased: 5.131
start validation test
0.774845360825
1.0
0.774845360825
0.873141263941
0
validation finish
batch start
#iterations: 120
currently lose_sum: 355.1739604473114
time_elpased: 5.146
batch start
#iterations: 121
currently lose_sum: 355.6036222577095
time_elpased: 5.149
batch start
#iterations: 122
currently lose_sum: 355.93057560920715
time_elpased: 5.147
batch start
#iterations: 123
currently lose_sum: 354.6979821920395
time_elpased: 5.141
batch start
#iterations: 124
currently lose_sum: 354.6589500308037
time_elpased: 5.154
batch start
#iterations: 125
currently lose_sum: 354.424989759922
time_elpased: 5.113
batch start
#iterations: 126
currently lose_sum: 353.76538759469986
time_elpased: 5.136
batch start
#iterations: 127
currently lose_sum: 354.89307552576065
time_elpased: 5.145
batch start
#iterations: 128
currently lose_sum: 354.377500385046
time_elpased: 5.149
batch start
#iterations: 129
currently lose_sum: 355.06289798021317
time_elpased: 5.206
batch start
#iterations: 130
currently lose_sum: 353.8841894865036
time_elpased: 5.169
batch start
#iterations: 131
currently lose_sum: 355.2163727581501
time_elpased: 5.117
batch start
#iterations: 132
currently lose_sum: 354.5952253937721
time_elpased: 5.13
batch start
#iterations: 133
currently lose_sum: 354.97877538204193
time_elpased: 5.16
batch start
#iterations: 134
currently lose_sum: 353.37923738360405
time_elpased: 5.151
batch start
#iterations: 135
currently lose_sum: 354.17304986715317
time_elpased: 5.163
batch start
#iterations: 136
currently lose_sum: 354.01760095357895
time_elpased: 5.138
batch start
#iterations: 137
currently lose_sum: 356.0639696121216
time_elpased: 5.144
batch start
#iterations: 138
currently lose_sum: 355.99882584810257
time_elpased: 5.137
batch start
#iterations: 139
currently lose_sum: 354.2595083117485
time_elpased: 5.121
start validation test
0.78587628866
1.0
0.78587628866
0.88010159903
0
validation finish
batch start
#iterations: 140
currently lose_sum: 355.9103640317917
time_elpased: 5.169
batch start
#iterations: 141
currently lose_sum: 354.8104749917984
time_elpased: 5.164
batch start
#iterations: 142
currently lose_sum: 353.96467837691307
time_elpased: 5.153
batch start
#iterations: 143
currently lose_sum: 354.24579706788063
time_elpased: 5.141
batch start
#iterations: 144
currently lose_sum: 355.7478455901146
time_elpased: 5.129
batch start
#iterations: 145
currently lose_sum: 354.44576847553253
time_elpased: 5.136
batch start
#iterations: 146
currently lose_sum: 355.77750277519226
time_elpased: 5.15
batch start
#iterations: 147
currently lose_sum: 354.53640139102936
time_elpased: 5.164
batch start
#iterations: 148
currently lose_sum: 356.4647776186466
time_elpased: 5.167
batch start
#iterations: 149
currently lose_sum: 353.9595638513565
time_elpased: 5.155
batch start
#iterations: 150
currently lose_sum: 354.41990929841995
time_elpased: 5.137
batch start
#iterations: 151
currently lose_sum: 354.28255891799927
time_elpased: 5.165
batch start
#iterations: 152
currently lose_sum: 353.63021844625473
time_elpased: 5.102
batch start
#iterations: 153
currently lose_sum: 353.6948151886463
time_elpased: 5.142
batch start
#iterations: 154
currently lose_sum: 352.22996723651886
time_elpased: 5.168
batch start
#iterations: 155
currently lose_sum: 354.77910420298576
time_elpased: 5.176
batch start
#iterations: 156
currently lose_sum: 352.89279690384865
time_elpased: 5.166
batch start
#iterations: 157
currently lose_sum: 354.22928512096405
time_elpased: 5.125
batch start
#iterations: 158
currently lose_sum: 352.48158022761345
time_elpased: 5.139
batch start
#iterations: 159
currently lose_sum: 353.67277535796165
time_elpased: 5.122
start validation test
0.807835051546
1.0
0.807835051546
0.893704379562
0
validation finish
batch start
#iterations: 160
currently lose_sum: 355.8784476220608
time_elpased: 5.14
batch start
#iterations: 161
currently lose_sum: 354.7182011604309
time_elpased: 5.145
batch start
#iterations: 162
currently lose_sum: 354.59794452786446
time_elpased: 5.116
batch start
#iterations: 163
currently lose_sum: 355.50970941782
time_elpased: 5.152
batch start
#iterations: 164
currently lose_sum: 354.81454437971115
time_elpased: 5.148
batch start
#iterations: 165
currently lose_sum: 355.20458006858826
time_elpased: 5.18
batch start
#iterations: 166
currently lose_sum: 354.9642046689987
time_elpased: 5.152
batch start
#iterations: 167
currently lose_sum: 353.143179833889
time_elpased: 5.163
batch start
#iterations: 168
currently lose_sum: 353.931012570858
time_elpased: 5.142
batch start
#iterations: 169
currently lose_sum: 353.3135501742363
time_elpased: 5.171
batch start
#iterations: 170
currently lose_sum: 353.23025953769684
time_elpased: 5.138
batch start
#iterations: 171
currently lose_sum: 354.4592387676239
time_elpased: 5.144
batch start
#iterations: 172
currently lose_sum: 354.55633702874184
time_elpased: 5.134
batch start
#iterations: 173
currently lose_sum: 354.1985831260681
time_elpased: 5.158
batch start
#iterations: 174
currently lose_sum: 353.9663197696209
time_elpased: 5.122
batch start
#iterations: 175
currently lose_sum: 354.65977826714516
time_elpased: 5.12
batch start
#iterations: 176
currently lose_sum: 354.53177005052567
time_elpased: 5.128
batch start
#iterations: 177
currently lose_sum: 354.75688049197197
time_elpased: 5.138
batch start
#iterations: 178
currently lose_sum: 354.71530562639236
time_elpased: 5.175
batch start
#iterations: 179
currently lose_sum: 354.57429814338684
time_elpased: 5.128
start validation test
0.787010309278
1.0
0.787010309278
0.880812276451
0
validation finish
batch start
#iterations: 180
currently lose_sum: 352.7093569934368
time_elpased: 5.14
batch start
#iterations: 181
currently lose_sum: 353.67776414752007
time_elpased: 5.137
batch start
#iterations: 182
currently lose_sum: 353.3625573515892
time_elpased: 5.141
batch start
#iterations: 183
currently lose_sum: 353.4241449832916
time_elpased: 5.165
batch start
#iterations: 184
currently lose_sum: 353.106713950634
time_elpased: 5.151
batch start
#iterations: 185
currently lose_sum: 352.7037961781025
time_elpased: 5.152
batch start
#iterations: 186
currently lose_sum: 353.34718772768974
time_elpased: 5.148
batch start
#iterations: 187
currently lose_sum: 353.9520154595375
time_elpased: 5.139
batch start
#iterations: 188
currently lose_sum: 352.1152901351452
time_elpased: 5.188
batch start
#iterations: 189
currently lose_sum: 353.3370342850685
time_elpased: 5.119
batch start
#iterations: 190
currently lose_sum: 352.7797573208809
time_elpased: 5.14
batch start
#iterations: 191
currently lose_sum: 352.9703339934349
time_elpased: 5.14
batch start
#iterations: 192
currently lose_sum: 353.72448670864105
time_elpased: 5.137
batch start
#iterations: 193
currently lose_sum: 352.88989478349686
time_elpased: 5.156
batch start
#iterations: 194
currently lose_sum: 353.1810572743416
time_elpased: 5.158
batch start
#iterations: 195
currently lose_sum: 351.7496372461319
time_elpased: 5.112
batch start
#iterations: 196
currently lose_sum: 354.79886519908905
time_elpased: 5.133
batch start
#iterations: 197
currently lose_sum: 353.08336812257767
time_elpased: 5.144
batch start
#iterations: 198
currently lose_sum: 353.877975910902
time_elpased: 5.152
batch start
#iterations: 199
currently lose_sum: 354.33732521533966
time_elpased: 5.158
start validation test
0.793092783505
1.0
0.793092783505
0.884608750647
0
validation finish
batch start
#iterations: 200
currently lose_sum: 354.19068828225136
time_elpased: 5.138
batch start
#iterations: 201
currently lose_sum: 353.049443423748
time_elpased: 5.153
batch start
#iterations: 202
currently lose_sum: 353.3387297987938
time_elpased: 5.145
batch start
#iterations: 203
currently lose_sum: 353.64142283797264
time_elpased: 5.141
batch start
#iterations: 204
currently lose_sum: 354.54408636689186
time_elpased: 5.143
batch start
#iterations: 205
currently lose_sum: 352.6888543665409
time_elpased: 5.146
batch start
#iterations: 206
currently lose_sum: 353.74765318632126
time_elpased: 5.122
batch start
#iterations: 207
currently lose_sum: 353.7440152466297
time_elpased: 5.147
batch start
#iterations: 208
currently lose_sum: 353.3590243458748
time_elpased: 5.129
batch start
#iterations: 209
currently lose_sum: 353.3367246091366
time_elpased: 5.106
batch start
#iterations: 210
currently lose_sum: 353.11289167404175
time_elpased: 5.13
batch start
#iterations: 211
currently lose_sum: 352.99597704410553
time_elpased: 5.136
batch start
#iterations: 212
currently lose_sum: 352.3643633723259
time_elpased: 5.119
batch start
#iterations: 213
currently lose_sum: 353.3474006652832
time_elpased: 5.144
batch start
#iterations: 214
currently lose_sum: 352.69198629260063
time_elpased: 5.147
batch start
#iterations: 215
currently lose_sum: 351.91159960627556
time_elpased: 5.138
batch start
#iterations: 216
currently lose_sum: 352.33993315696716
time_elpased: 5.151
batch start
#iterations: 217
currently lose_sum: 354.2825329899788
time_elpased: 5.163
batch start
#iterations: 218
currently lose_sum: 352.7491690814495
time_elpased: 5.158
batch start
#iterations: 219
currently lose_sum: 351.8270305097103
time_elpased: 5.127
start validation test
0.797731958763
1.0
0.797731958763
0.887487097144
0
validation finish
batch start
#iterations: 220
currently lose_sum: 354.3645746707916
time_elpased: 5.139
batch start
#iterations: 221
currently lose_sum: 354.0970838069916
time_elpased: 5.148
batch start
#iterations: 222
currently lose_sum: 352.1256091892719
time_elpased: 5.112
batch start
#iterations: 223
currently lose_sum: 351.53529918193817
time_elpased: 5.152
batch start
#iterations: 224
currently lose_sum: 353.54029899835587
time_elpased: 5.109
batch start
#iterations: 225
currently lose_sum: 354.4578746557236
time_elpased: 5.122
batch start
#iterations: 226
currently lose_sum: 352.1579810678959
time_elpased: 5.116
batch start
#iterations: 227
currently lose_sum: 352.17756602168083
time_elpased: 5.143
batch start
#iterations: 228
currently lose_sum: 353.37324237823486
time_elpased: 5.153
batch start
#iterations: 229
currently lose_sum: 353.4310860335827
time_elpased: 5.142
batch start
#iterations: 230
currently lose_sum: 353.64508536458015
time_elpased: 5.122
batch start
#iterations: 231
currently lose_sum: 353.34143459796906
time_elpased: 5.125
batch start
#iterations: 232
currently lose_sum: 354.3856706023216
time_elpased: 5.148
batch start
#iterations: 233
currently lose_sum: 353.6056464314461
time_elpased: 5.117
batch start
#iterations: 234
currently lose_sum: 352.403001755476
time_elpased: 5.154
batch start
#iterations: 235
currently lose_sum: 352.1404366195202
time_elpased: 5.165
batch start
#iterations: 236
currently lose_sum: 353.9331285357475
time_elpased: 5.133
batch start
#iterations: 237
currently lose_sum: 352.70730265975
time_elpased: 5.129
batch start
#iterations: 238
currently lose_sum: 353.5141944885254
time_elpased: 5.11
batch start
#iterations: 239
currently lose_sum: 355.2893385887146
time_elpased: 5.148
start validation test
0.787835051546
1.0
0.787835051546
0.881328566486
0
validation finish
batch start
#iterations: 240
currently lose_sum: 353.1887758374214
time_elpased: 5.138
batch start
#iterations: 241
currently lose_sum: 352.4871678352356
time_elpased: 5.117
batch start
#iterations: 242
currently lose_sum: 352.48803383111954
time_elpased: 5.111
batch start
#iterations: 243
currently lose_sum: 353.6461529433727
time_elpased: 5.148
batch start
#iterations: 244
currently lose_sum: 353.77547812461853
time_elpased: 5.12
batch start
#iterations: 245
currently lose_sum: 353.1406368613243
time_elpased: 5.171
batch start
#iterations: 246
currently lose_sum: 353.1915586590767
time_elpased: 5.143
batch start
#iterations: 247
currently lose_sum: 353.42552894353867
time_elpased: 5.124
batch start
#iterations: 248
currently lose_sum: 352.1871926486492
time_elpased: 5.129
batch start
#iterations: 249
currently lose_sum: 352.56009954214096
time_elpased: 5.118
batch start
#iterations: 250
currently lose_sum: 352.3166587948799
time_elpased: 5.162
batch start
#iterations: 251
currently lose_sum: 352.6336646974087
time_elpased: 5.125
batch start
#iterations: 252
currently lose_sum: 354.1992295086384
time_elpased: 5.158
batch start
#iterations: 253
currently lose_sum: 353.5149722099304
time_elpased: 5.15
batch start
#iterations: 254
currently lose_sum: 352.5748662352562
time_elpased: 5.133
batch start
#iterations: 255
currently lose_sum: 353.3165245652199
time_elpased: 5.126
batch start
#iterations: 256
currently lose_sum: 353.6633042395115
time_elpased: 5.139
batch start
#iterations: 257
currently lose_sum: 353.88711178302765
time_elpased: 5.128
batch start
#iterations: 258
currently lose_sum: 352.4088016152382
time_elpased: 5.143
batch start
#iterations: 259
currently lose_sum: 351.5445355474949
time_elpased: 5.144
start validation test
0.792886597938
1.0
0.792886597938
0.884480478408
0
validation finish
batch start
#iterations: 260
currently lose_sum: 353.56236270070076
time_elpased: 5.145
batch start
#iterations: 261
currently lose_sum: 352.67560890316963
time_elpased: 5.161
batch start
#iterations: 262
currently lose_sum: 352.6717495918274
time_elpased: 5.136
batch start
#iterations: 263
currently lose_sum: 353.280020147562
time_elpased: 5.157
batch start
#iterations: 264
currently lose_sum: 352.3189509212971
time_elpased: 5.163
batch start
#iterations: 265
currently lose_sum: 353.8462915122509
time_elpased: 5.135
batch start
#iterations: 266
currently lose_sum: 352.2092105746269
time_elpased: 5.151
batch start
#iterations: 267
currently lose_sum: 353.7263576388359
time_elpased: 5.134
batch start
#iterations: 268
currently lose_sum: 351.4686938226223
time_elpased: 5.103
batch start
#iterations: 269
currently lose_sum: 352.5965985953808
time_elpased: 5.161
batch start
#iterations: 270
currently lose_sum: 351.67398965358734
time_elpased: 5.137
batch start
#iterations: 271
currently lose_sum: 351.4410437941551
time_elpased: 5.154
batch start
#iterations: 272
currently lose_sum: 352.6366392672062
time_elpased: 5.133
batch start
#iterations: 273
currently lose_sum: 351.23337721824646
time_elpased: 5.196
batch start
#iterations: 274
currently lose_sum: 354.1974065899849
time_elpased: 5.126
batch start
#iterations: 275
currently lose_sum: 352.9700424671173
time_elpased: 5.174
batch start
#iterations: 276
currently lose_sum: 352.5423808991909
time_elpased: 5.124
batch start
#iterations: 277
currently lose_sum: 352.7058898806572
time_elpased: 5.141
batch start
#iterations: 278
currently lose_sum: 353.67360258102417
time_elpased: 5.128
batch start
#iterations: 279
currently lose_sum: 352.1995325088501
time_elpased: 5.171
start validation test
0.805051546392
1.0
0.805051546392
0.891998400822
0
validation finish
batch start
#iterations: 280
currently lose_sum: 351.7863930761814
time_elpased: 5.123
batch start
#iterations: 281
currently lose_sum: 351.37015613913536
time_elpased: 5.136
batch start
#iterations: 282
currently lose_sum: 354.2215346097946
time_elpased: 5.157
batch start
#iterations: 283
currently lose_sum: 354.07517117261887
time_elpased: 5.165
batch start
#iterations: 284
currently lose_sum: 353.13710910081863
time_elpased: 5.135
batch start
#iterations: 285
currently lose_sum: 351.6635762453079
time_elpased: 5.149
batch start
#iterations: 286
currently lose_sum: 351.0242283344269
time_elpased: 5.122
batch start
#iterations: 287
currently lose_sum: 352.9288257062435
time_elpased: 5.161
batch start
#iterations: 288
currently lose_sum: 353.40884175896645
time_elpased: 5.118
batch start
#iterations: 289
currently lose_sum: 350.80714479088783
time_elpased: 5.167
batch start
#iterations: 290
currently lose_sum: 352.86432379484177
time_elpased: 5.13
batch start
#iterations: 291
currently lose_sum: 350.53115424513817
time_elpased: 5.151
batch start
#iterations: 292
currently lose_sum: 352.3789047598839
time_elpased: 5.151
batch start
#iterations: 293
currently lose_sum: 351.87704664468765
time_elpased: 5.129
batch start
#iterations: 294
currently lose_sum: 352.2449135184288
time_elpased: 5.133
batch start
#iterations: 295
currently lose_sum: 352.13336503505707
time_elpased: 5.154
batch start
#iterations: 296
currently lose_sum: 351.76413947343826
time_elpased: 5.121
batch start
#iterations: 297
currently lose_sum: 351.8510382473469
time_elpased: 5.146
batch start
#iterations: 298
currently lose_sum: 352.7410921752453
time_elpased: 5.144
batch start
#iterations: 299
currently lose_sum: 352.41742330789566
time_elpased: 5.162
start validation test
0.798453608247
1.0
0.798453608247
0.887933505302
0
validation finish
batch start
#iterations: 300
currently lose_sum: 353.2953342795372
time_elpased: 5.125
batch start
#iterations: 301
currently lose_sum: 352.2918307185173
time_elpased: 5.131
batch start
#iterations: 302
currently lose_sum: 352.101377338171
time_elpased: 5.135
batch start
#iterations: 303
currently lose_sum: 351.71042662858963
time_elpased: 5.156
batch start
#iterations: 304
currently lose_sum: 351.5122428536415
time_elpased: 5.152
batch start
#iterations: 305
currently lose_sum: 351.6873699426651
time_elpased: 5.117
batch start
#iterations: 306
currently lose_sum: 351.74592530727386
time_elpased: 5.148
batch start
#iterations: 307
currently lose_sum: 353.2245129942894
time_elpased: 5.148
batch start
#iterations: 308
currently lose_sum: 353.78439939022064
time_elpased: 5.2
batch start
#iterations: 309
currently lose_sum: 352.1828115582466
time_elpased: 5.138
batch start
#iterations: 310
currently lose_sum: 352.61928576231
time_elpased: 5.144
batch start
#iterations: 311
currently lose_sum: 349.94517543911934
time_elpased: 5.167
batch start
#iterations: 312
currently lose_sum: 352.28162348270416
time_elpased: 5.144
batch start
#iterations: 313
currently lose_sum: 352.1365279555321
time_elpased: 5.132
batch start
#iterations: 314
currently lose_sum: 351.5854504108429
time_elpased: 5.15
batch start
#iterations: 315
currently lose_sum: 349.9271315932274
time_elpased: 5.152
batch start
#iterations: 316
currently lose_sum: 353.5857549607754
time_elpased: 5.119
batch start
#iterations: 317
currently lose_sum: 350.93858593702316
time_elpased: 5.119
batch start
#iterations: 318
currently lose_sum: 350.2026877999306
time_elpased: 5.134
batch start
#iterations: 319
currently lose_sum: 352.96938744187355
time_elpased: 5.103
start validation test
0.798762886598
1.0
0.798762886598
0.888124713434
0
validation finish
batch start
#iterations: 320
currently lose_sum: 350.77973306179047
time_elpased: 5.095
batch start
#iterations: 321
currently lose_sum: 352.2512228488922
time_elpased: 5.15
batch start
#iterations: 322
currently lose_sum: 352.42480531334877
time_elpased: 5.144
batch start
#iterations: 323
currently lose_sum: 350.2857155203819
time_elpased: 5.143
batch start
#iterations: 324
currently lose_sum: 351.51789423823357
time_elpased: 5.123
batch start
#iterations: 325
currently lose_sum: 351.7829478979111
time_elpased: 5.112
batch start
#iterations: 326
currently lose_sum: 352.69014635682106
time_elpased: 5.131
batch start
#iterations: 327
currently lose_sum: 351.9323428571224
time_elpased: 5.148
batch start
#iterations: 328
currently lose_sum: 352.09349048137665
time_elpased: 5.154
batch start
#iterations: 329
currently lose_sum: 350.8307357430458
time_elpased: 5.117
batch start
#iterations: 330
currently lose_sum: 350.97810330986977
time_elpased: 5.146
batch start
#iterations: 331
currently lose_sum: 351.5676046907902
time_elpased: 5.12
batch start
#iterations: 332
currently lose_sum: 351.91164326667786
time_elpased: 5.139
batch start
#iterations: 333
currently lose_sum: 350.93376809358597
time_elpased: 5.127
batch start
#iterations: 334
currently lose_sum: 349.94582056999207
time_elpased: 5.117
batch start
#iterations: 335
currently lose_sum: 350.93176701664925
time_elpased: 5.131
batch start
#iterations: 336
currently lose_sum: 351.8225261271
time_elpased: 5.113
batch start
#iterations: 337
currently lose_sum: 351.55301770567894
time_elpased: 5.117
batch start
#iterations: 338
currently lose_sum: 351.68327099084854
time_elpased: 5.109
batch start
#iterations: 339
currently lose_sum: 352.01467353105545
time_elpased: 5.122
start validation test
0.802783505155
1.0
0.802783505155
0.890604449019
0
validation finish
batch start
#iterations: 340
currently lose_sum: 352.62173506617546
time_elpased: 5.139
batch start
#iterations: 341
currently lose_sum: 350.57758978009224
time_elpased: 5.129
batch start
#iterations: 342
currently lose_sum: 349.2066867649555
time_elpased: 5.17
batch start
#iterations: 343
currently lose_sum: 352.32094341516495
time_elpased: 5.144
batch start
#iterations: 344
currently lose_sum: 352.1774718463421
time_elpased: 5.127
batch start
#iterations: 345
currently lose_sum: 350.6123340725899
time_elpased: 5.18
batch start
#iterations: 346
currently lose_sum: 350.22973358631134
time_elpased: 5.139
batch start
#iterations: 347
currently lose_sum: 350.91112318634987
time_elpased: 5.167
batch start
#iterations: 348
currently lose_sum: 351.6996110677719
time_elpased: 5.137
batch start
#iterations: 349
currently lose_sum: 350.1397399902344
time_elpased: 5.179
batch start
#iterations: 350
currently lose_sum: 350.822478055954
time_elpased: 5.128
batch start
#iterations: 351
currently lose_sum: 352.4502183794975
time_elpased: 5.147
batch start
#iterations: 352
currently lose_sum: 351.85897755622864
time_elpased: 5.152
batch start
#iterations: 353
currently lose_sum: 351.6372617483139
time_elpased: 5.132
batch start
#iterations: 354
currently lose_sum: 350.1583715379238
time_elpased: 5.115
batch start
#iterations: 355
currently lose_sum: 350.76808455586433
time_elpased: 5.169
batch start
#iterations: 356
currently lose_sum: 353.03687447309494
time_elpased: 5.13
batch start
#iterations: 357
currently lose_sum: 350.49923199415207
time_elpased: 5.15
batch start
#iterations: 358
currently lose_sum: 351.24104392528534
time_elpased: 5.142
batch start
#iterations: 359
currently lose_sum: 352.0420432090759
time_elpased: 5.144
start validation test
0.800412371134
1.0
0.800412371134
0.889143380669
0
validation finish
batch start
#iterations: 360
currently lose_sum: 349.5155286490917
time_elpased: 5.164
batch start
#iterations: 361
currently lose_sum: 351.51404842734337
time_elpased: 5.152
batch start
#iterations: 362
currently lose_sum: 352.1318313777447
time_elpased: 5.116
batch start
#iterations: 363
currently lose_sum: 350.8403112888336
time_elpased: 5.149
batch start
#iterations: 364
currently lose_sum: 350.18902868032455
time_elpased: 5.163
batch start
#iterations: 365
currently lose_sum: 351.0044960975647
time_elpased: 5.148
batch start
#iterations: 366
currently lose_sum: 352.0457895100117
time_elpased: 5.178
batch start
#iterations: 367
currently lose_sum: 351.3950545191765
time_elpased: 5.133
batch start
#iterations: 368
currently lose_sum: 351.8827822506428
time_elpased: 5.167
batch start
#iterations: 369
currently lose_sum: 351.93534022569656
time_elpased: 5.12
batch start
#iterations: 370
currently lose_sum: 352.06567201018333
time_elpased: 5.136
batch start
#iterations: 371
currently lose_sum: 350.37814128398895
time_elpased: 5.132
batch start
#iterations: 372
currently lose_sum: 350.04892414808273
time_elpased: 5.124
batch start
#iterations: 373
currently lose_sum: 352.2203195989132
time_elpased: 5.144
batch start
#iterations: 374
currently lose_sum: 352.3132884502411
time_elpased: 5.159
batch start
#iterations: 375
currently lose_sum: 351.42415902018547
time_elpased: 5.16
batch start
#iterations: 376
currently lose_sum: 351.5828031003475
time_elpased: 5.127
batch start
#iterations: 377
currently lose_sum: 351.56880432367325
time_elpased: 5.144
batch start
#iterations: 378
currently lose_sum: 348.97921308875084
time_elpased: 5.136
batch start
#iterations: 379
currently lose_sum: 351.0473271906376
time_elpased: 5.142
start validation test
0.800103092784
1.0
0.800103092784
0.888952522765
0
validation finish
batch start
#iterations: 380
currently lose_sum: 352.0332123041153
time_elpased: 5.15
batch start
#iterations: 381
currently lose_sum: 350.338963329792
time_elpased: 5.152
batch start
#iterations: 382
currently lose_sum: 350.1359711289406
time_elpased: 5.14
batch start
#iterations: 383
currently lose_sum: 350.23781764507294
time_elpased: 5.143
batch start
#iterations: 384
currently lose_sum: 350.66965264081955
time_elpased: 5.121
batch start
#iterations: 385
currently lose_sum: 350.0939464867115
time_elpased: 5.148
batch start
#iterations: 386
currently lose_sum: 350.3404039442539
time_elpased: 5.133
batch start
#iterations: 387
currently lose_sum: 351.315510481596
time_elpased: 5.127
batch start
#iterations: 388
currently lose_sum: 351.5702596604824
time_elpased: 5.131
batch start
#iterations: 389
currently lose_sum: 351.06118923425674
time_elpased: 5.124
batch start
#iterations: 390
currently lose_sum: 351.7511630952358
time_elpased: 5.16
batch start
#iterations: 391
currently lose_sum: 350.067422747612
time_elpased: 5.15
batch start
#iterations: 392
currently lose_sum: 350.0639203488827
time_elpased: 5.146
batch start
#iterations: 393
currently lose_sum: 349.5912527143955
time_elpased: 5.131
batch start
#iterations: 394
currently lose_sum: 349.7590763270855
time_elpased: 5.137
batch start
#iterations: 395
currently lose_sum: 350.6197086274624
time_elpased: 5.131
batch start
#iterations: 396
currently lose_sum: 351.15140014886856
time_elpased: 5.15
batch start
#iterations: 397
currently lose_sum: 351.73451963067055
time_elpased: 5.138
batch start
#iterations: 398
currently lose_sum: 350.80390816926956
time_elpased: 5.145
batch start
#iterations: 399
currently lose_sum: 349.971872061491
time_elpased: 5.115
start validation test
0.797628865979
1.0
0.797628865979
0.887423295292
0
validation finish
acc: 0.802
pre: 1.000
rec: 0.802
F1: 0.890
auc: 0.000
