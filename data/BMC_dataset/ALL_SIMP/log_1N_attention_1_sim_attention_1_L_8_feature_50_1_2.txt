start to construct graph
graph construct over
58300
epochs start
batch start
#iterations: 0
currently lose_sum: 396.0903470516205
time_elpased: 4.816
batch start
#iterations: 1
currently lose_sum: 385.1922314763069
time_elpased: 4.769
batch start
#iterations: 2
currently lose_sum: 378.97066777944565
time_elpased: 4.754
batch start
#iterations: 3
currently lose_sum: 375.86933022737503
time_elpased: 4.755
batch start
#iterations: 4
currently lose_sum: 372.58230823278427
time_elpased: 4.757
batch start
#iterations: 5
currently lose_sum: 371.5618848204613
time_elpased: 4.744
batch start
#iterations: 6
currently lose_sum: 369.9493463039398
time_elpased: 4.732
batch start
#iterations: 7
currently lose_sum: 369.56105637550354
time_elpased: 4.761
batch start
#iterations: 8
currently lose_sum: 367.740609228611
time_elpased: 4.769
batch start
#iterations: 9
currently lose_sum: 369.20000064373016
time_elpased: 4.764
batch start
#iterations: 10
currently lose_sum: 365.4757955670357
time_elpased: 4.869
batch start
#iterations: 11
currently lose_sum: 365.8830703496933
time_elpased: 4.757
batch start
#iterations: 12
currently lose_sum: 366.0291127562523
time_elpased: 4.787
batch start
#iterations: 13
currently lose_sum: 367.9090702533722
time_elpased: 4.783
batch start
#iterations: 14
currently lose_sum: 363.00137439370155
time_elpased: 4.78
batch start
#iterations: 15
currently lose_sum: 364.7568262219429
time_elpased: 4.764
batch start
#iterations: 16
currently lose_sum: 363.51785773038864
time_elpased: 4.756
batch start
#iterations: 17
currently lose_sum: 365.1892938017845
time_elpased: 4.78
batch start
#iterations: 18
currently lose_sum: 362.46246349811554
time_elpased: 4.778
batch start
#iterations: 19
currently lose_sum: 364.20618587732315
time_elpased: 4.79
start validation test
0.637628865979
1.0
0.637628865979
0.778722064841
0
validation finish
batch start
#iterations: 20
currently lose_sum: 362.90599393844604
time_elpased: 4.761
batch start
#iterations: 21
currently lose_sum: 362.9545149207115
time_elpased: 4.785
batch start
#iterations: 22
currently lose_sum: 363.2934791445732
time_elpased: 4.778
batch start
#iterations: 23
currently lose_sum: 363.35260903835297
time_elpased: 4.764
batch start
#iterations: 24
currently lose_sum: 362.98574209213257
time_elpased: 4.766
batch start
#iterations: 25
currently lose_sum: 363.3751571178436
time_elpased: 4.754
batch start
#iterations: 26
currently lose_sum: 361.70535135269165
time_elpased: 4.773
batch start
#iterations: 27
currently lose_sum: 362.39669835567474
time_elpased: 4.766
batch start
#iterations: 28
currently lose_sum: 360.41733071208
time_elpased: 4.77
batch start
#iterations: 29
currently lose_sum: 362.4068850874901
time_elpased: 4.785
batch start
#iterations: 30
currently lose_sum: 362.2547974586487
time_elpased: 4.79
batch start
#iterations: 31
currently lose_sum: 360.893223464489
time_elpased: 4.775
batch start
#iterations: 32
currently lose_sum: 361.8909265398979
time_elpased: 4.778
batch start
#iterations: 33
currently lose_sum: 362.11998373270035
time_elpased: 4.781
batch start
#iterations: 34
currently lose_sum: 361.7936049103737
time_elpased: 4.768
batch start
#iterations: 35
currently lose_sum: 362.05468159914017
time_elpased: 4.736
batch start
#iterations: 36
currently lose_sum: 361.11538857221603
time_elpased: 4.753
batch start
#iterations: 37
currently lose_sum: 360.1705814599991
time_elpased: 4.769
batch start
#iterations: 38
currently lose_sum: 362.1239988207817
time_elpased: 4.78
batch start
#iterations: 39
currently lose_sum: 361.3053438067436
time_elpased: 4.751
start validation test
0.685257731959
1.0
0.685257731959
0.813237902979
0
validation finish
batch start
#iterations: 40
currently lose_sum: 360.39968794584274
time_elpased: 4.781
batch start
#iterations: 41
currently lose_sum: 360.9711798429489
time_elpased: 4.774
batch start
#iterations: 42
currently lose_sum: 359.2182756662369
time_elpased: 4.766
batch start
#iterations: 43
currently lose_sum: 360.0612491965294
time_elpased: 4.774
batch start
#iterations: 44
currently lose_sum: 360.63457864522934
time_elpased: 4.784
batch start
#iterations: 45
currently lose_sum: 358.96429777145386
time_elpased: 4.753
batch start
#iterations: 46
currently lose_sum: 359.50712168216705
time_elpased: 4.783
batch start
#iterations: 47
currently lose_sum: 360.3235877752304
time_elpased: 4.795
batch start
#iterations: 48
currently lose_sum: 359.8381560444832
time_elpased: 4.771
batch start
#iterations: 49
currently lose_sum: 360.1094903945923
time_elpased: 4.764
batch start
#iterations: 50
currently lose_sum: 358.0874955058098
time_elpased: 4.787
batch start
#iterations: 51
currently lose_sum: 359.90960162878036
time_elpased: 4.776
batch start
#iterations: 52
currently lose_sum: 359.85950100421906
time_elpased: 4.766
batch start
#iterations: 53
currently lose_sum: 360.7616917192936
time_elpased: 4.811
batch start
#iterations: 54
currently lose_sum: 360.0750262141228
time_elpased: 4.767
batch start
#iterations: 55
currently lose_sum: 358.80290311574936
time_elpased: 4.788
batch start
#iterations: 56
currently lose_sum: 360.15404683351517
time_elpased: 4.749
batch start
#iterations: 57
currently lose_sum: 360.2254394888878
time_elpased: 4.797
batch start
#iterations: 58
currently lose_sum: 358.71299013495445
time_elpased: 4.753
batch start
#iterations: 59
currently lose_sum: 359.95653861761093
time_elpased: 4.778
start validation test
0.784432989691
1.0
0.784432989691
0.879195794096
0
validation finish
batch start
#iterations: 60
currently lose_sum: 359.5217217206955
time_elpased: 4.774
batch start
#iterations: 61
currently lose_sum: 358.99651396274567
time_elpased: 4.765
batch start
#iterations: 62
currently lose_sum: 358.83006632328033
time_elpased: 4.753
batch start
#iterations: 63
currently lose_sum: 358.60845470428467
time_elpased: 4.777
batch start
#iterations: 64
currently lose_sum: 359.9496040344238
time_elpased: 4.76
batch start
#iterations: 65
currently lose_sum: 359.7511018514633
time_elpased: 4.763
batch start
#iterations: 66
currently lose_sum: 359.06004482507706
time_elpased: 4.765
batch start
#iterations: 67
currently lose_sum: 359.52401489019394
time_elpased: 4.792
batch start
#iterations: 68
currently lose_sum: 359.00053548812866
time_elpased: 4.76
batch start
#iterations: 69
currently lose_sum: 359.05722934007645
time_elpased: 4.785
batch start
#iterations: 70
currently lose_sum: 358.7696312069893
time_elpased: 4.751
batch start
#iterations: 71
currently lose_sum: 359.49664956331253
time_elpased: 4.763
batch start
#iterations: 72
currently lose_sum: 358.9922016263008
time_elpased: 4.759
batch start
#iterations: 73
currently lose_sum: 357.76865342259407
time_elpased: 4.756
batch start
#iterations: 74
currently lose_sum: 357.81721448898315
time_elpased: 4.762
batch start
#iterations: 75
currently lose_sum: 357.8354883790016
time_elpased: 4.765
batch start
#iterations: 76
currently lose_sum: 358.3426736295223
time_elpased: 4.776
batch start
#iterations: 77
currently lose_sum: 358.56637036800385
time_elpased: 4.743
batch start
#iterations: 78
currently lose_sum: 359.3336519598961
time_elpased: 4.784
batch start
#iterations: 79
currently lose_sum: 358.4685086607933
time_elpased: 4.777
start validation test
0.793092783505
1.0
0.793092783505
0.884608750647
0
validation finish
batch start
#iterations: 80
currently lose_sum: 357.5826371908188
time_elpased: 4.782
batch start
#iterations: 81
currently lose_sum: 359.5779107809067
time_elpased: 4.767
batch start
#iterations: 82
currently lose_sum: 358.74141705036163
time_elpased: 4.783
batch start
#iterations: 83
currently lose_sum: 358.20943158864975
time_elpased: 4.792
batch start
#iterations: 84
currently lose_sum: 359.5198786854744
time_elpased: 4.774
batch start
#iterations: 85
currently lose_sum: 359.1182005405426
time_elpased: 4.772
batch start
#iterations: 86
currently lose_sum: 358.8503713607788
time_elpased: 4.79
batch start
#iterations: 87
currently lose_sum: 358.05550253391266
time_elpased: 4.753
batch start
#iterations: 88
currently lose_sum: 358.0166335105896
time_elpased: 4.796
batch start
#iterations: 89
currently lose_sum: 357.81139662861824
time_elpased: 4.763
batch start
#iterations: 90
currently lose_sum: 359.2720347046852
time_elpased: 4.769
batch start
#iterations: 91
currently lose_sum: 358.17329466342926
time_elpased: 4.783
batch start
#iterations: 92
currently lose_sum: 356.8432583808899
time_elpased: 4.759
batch start
#iterations: 93
currently lose_sum: 359.9769214987755
time_elpased: 4.772
batch start
#iterations: 94
currently lose_sum: 357.87905353307724
time_elpased: 4.776
batch start
#iterations: 95
currently lose_sum: 357.7773687839508
time_elpased: 4.735
batch start
#iterations: 96
currently lose_sum: 359.3148263990879
time_elpased: 4.759
batch start
#iterations: 97
currently lose_sum: 358.1349598169327
time_elpased: 4.761
batch start
#iterations: 98
currently lose_sum: 358.099697470665
time_elpased: 4.758
batch start
#iterations: 99
currently lose_sum: 357.598825186491
time_elpased: 4.785
start validation test
0.764226804124
1.0
0.764226804124
0.866358908432
0
validation finish
batch start
#iterations: 100
currently lose_sum: 358.14385092258453
time_elpased: 4.759
batch start
#iterations: 101
currently lose_sum: 357.55273455381393
time_elpased: 4.79
batch start
#iterations: 102
currently lose_sum: 357.6650938987732
time_elpased: 4.769
batch start
#iterations: 103
currently lose_sum: 357.38551396131516
time_elpased: 4.783
batch start
#iterations: 104
currently lose_sum: 358.17550081014633
time_elpased: 4.768
batch start
#iterations: 105
currently lose_sum: 357.451837182045
time_elpased: 4.797
batch start
#iterations: 106
currently lose_sum: 358.30375599861145
time_elpased: 4.781
batch start
#iterations: 107
currently lose_sum: 357.82348704338074
time_elpased: 4.774
batch start
#iterations: 108
currently lose_sum: 358.1712466478348
time_elpased: 4.764
batch start
#iterations: 109
currently lose_sum: 358.17283564805984
time_elpased: 4.771
batch start
#iterations: 110
currently lose_sum: 358.8828803896904
time_elpased: 4.78
batch start
#iterations: 111
currently lose_sum: 357.5495766401291
time_elpased: 4.785
batch start
#iterations: 112
currently lose_sum: 357.45854580402374
time_elpased: 4.775
batch start
#iterations: 113
currently lose_sum: 357.36317324638367
time_elpased: 4.75
batch start
#iterations: 114
currently lose_sum: 356.6514649987221
time_elpased: 4.765
batch start
#iterations: 115
currently lose_sum: 356.83686727285385
time_elpased: 4.773
batch start
#iterations: 116
currently lose_sum: 358.39344638586044
time_elpased: 4.759
batch start
#iterations: 117
currently lose_sum: 356.91504991054535
time_elpased: 4.771
batch start
#iterations: 118
currently lose_sum: 356.39357644319534
time_elpased: 4.782
batch start
#iterations: 119
currently lose_sum: 356.60748344659805
time_elpased: 4.779
start validation test
0.766597938144
1.0
0.766597938144
0.867880485528
0
validation finish
batch start
#iterations: 120
currently lose_sum: 357.6025732755661
time_elpased: 4.795
batch start
#iterations: 121
currently lose_sum: 356.3113036453724
time_elpased: 4.77
batch start
#iterations: 122
currently lose_sum: 354.7709536254406
time_elpased: 4.755
batch start
#iterations: 123
currently lose_sum: 357.2975600361824
time_elpased: 4.764
batch start
#iterations: 124
currently lose_sum: 355.422012925148
time_elpased: 4.787
batch start
#iterations: 125
currently lose_sum: 357.1199543774128
time_elpased: 4.76
batch start
#iterations: 126
currently lose_sum: 358.93562906980515
time_elpased: 4.794
batch start
#iterations: 127
currently lose_sum: 356.9979187846184
time_elpased: 4.773
batch start
#iterations: 128
currently lose_sum: 356.6699450612068
time_elpased: 4.773
batch start
#iterations: 129
currently lose_sum: 358.1588001549244
time_elpased: 4.742
batch start
#iterations: 130
currently lose_sum: 356.91137331724167
time_elpased: 4.774
batch start
#iterations: 131
currently lose_sum: 356.2533307671547
time_elpased: 4.767
batch start
#iterations: 132
currently lose_sum: 356.99746096134186
time_elpased: 4.788
batch start
#iterations: 133
currently lose_sum: 356.3418954908848
time_elpased: 4.773
batch start
#iterations: 134
currently lose_sum: 357.378536939621
time_elpased: 4.755
batch start
#iterations: 135
currently lose_sum: 357.02858808636665
time_elpased: 4.761
batch start
#iterations: 136
currently lose_sum: 359.4892758131027
time_elpased: 4.797
batch start
#iterations: 137
currently lose_sum: 356.6515941619873
time_elpased: 4.75
batch start
#iterations: 138
currently lose_sum: 358.02743151783943
time_elpased: 4.758
batch start
#iterations: 139
currently lose_sum: 356.8016370534897
time_elpased: 4.768
start validation test
0.779484536082
1.0
0.779484536082
0.876079022073
0
validation finish
batch start
#iterations: 140
currently lose_sum: 357.3640377521515
time_elpased: 4.772
batch start
#iterations: 141
currently lose_sum: 358.35882914066315
time_elpased: 4.761
batch start
#iterations: 142
currently lose_sum: 357.7322679758072
time_elpased: 4.746
batch start
#iterations: 143
currently lose_sum: 356.69575715065
time_elpased: 4.76
batch start
#iterations: 144
currently lose_sum: 357.1523669362068
time_elpased: 4.791
batch start
#iterations: 145
currently lose_sum: 357.0709171295166
time_elpased: 4.751
batch start
#iterations: 146
currently lose_sum: 356.0871824026108
time_elpased: 4.779
batch start
#iterations: 147
currently lose_sum: 357.06907641887665
time_elpased: 4.769
batch start
#iterations: 148
currently lose_sum: 356.8862720131874
time_elpased: 4.756
batch start
#iterations: 149
currently lose_sum: 357.52255314588547
time_elpased: 4.779
batch start
#iterations: 150
currently lose_sum: 356.17362731695175
time_elpased: 4.772
batch start
#iterations: 151
currently lose_sum: 356.79611337184906
time_elpased: 4.764
batch start
#iterations: 152
currently lose_sum: 356.7759719491005
time_elpased: 4.791
batch start
#iterations: 153
currently lose_sum: 358.8221546411514
time_elpased: 4.745
batch start
#iterations: 154
currently lose_sum: 357.1074683070183
time_elpased: 4.757
batch start
#iterations: 155
currently lose_sum: 357.4012287259102
time_elpased: 4.756
batch start
#iterations: 156
currently lose_sum: 356.9268435239792
time_elpased: 4.769
batch start
#iterations: 157
currently lose_sum: 356.94380927085876
time_elpased: 4.771
batch start
#iterations: 158
currently lose_sum: 357.14687913656235
time_elpased: 4.777
batch start
#iterations: 159
currently lose_sum: 358.9032664299011
time_elpased: 4.786
start validation test
0.79824742268
1.0
0.79824742268
0.887805996675
0
validation finish
batch start
#iterations: 160
currently lose_sum: 357.27542155981064
time_elpased: 4.769
batch start
#iterations: 161
currently lose_sum: 356.6508637368679
time_elpased: 4.783
batch start
#iterations: 162
currently lose_sum: 357.09302216768265
time_elpased: 4.768
batch start
#iterations: 163
currently lose_sum: 357.20197999477386
time_elpased: 4.744
batch start
#iterations: 164
currently lose_sum: 356.536112010479
time_elpased: 4.777
batch start
#iterations: 165
currently lose_sum: 355.3563847541809
time_elpased: 4.757
batch start
#iterations: 166
currently lose_sum: 355.94453167915344
time_elpased: 4.764
batch start
#iterations: 167
currently lose_sum: 357.1225131750107
time_elpased: 4.748
batch start
#iterations: 168
currently lose_sum: 356.25049340724945
time_elpased: 4.766
batch start
#iterations: 169
currently lose_sum: 356.2142146229744
time_elpased: 4.789
batch start
#iterations: 170
currently lose_sum: 357.46824038028717
time_elpased: 4.785
batch start
#iterations: 171
currently lose_sum: 356.1497714519501
time_elpased: 4.753
batch start
#iterations: 172
currently lose_sum: 356.6020492911339
time_elpased: 4.778
batch start
#iterations: 173
currently lose_sum: 357.3273961544037
time_elpased: 4.755
batch start
#iterations: 174
currently lose_sum: 354.70281261205673
time_elpased: 4.759
batch start
#iterations: 175
currently lose_sum: 358.1005225777626
time_elpased: 4.772
batch start
#iterations: 176
currently lose_sum: 356.7608782052994
time_elpased: 4.763
batch start
#iterations: 177
currently lose_sum: 355.8859196603298
time_elpased: 4.763
batch start
#iterations: 178
currently lose_sum: 357.68160980939865
time_elpased: 4.755
batch start
#iterations: 179
currently lose_sum: 356.4192444682121
time_elpased: 4.761
start validation test
0.794226804124
1.0
0.794226804124
0.885313720984
0
validation finish
batch start
#iterations: 180
currently lose_sum: 357.3215497136116
time_elpased: 4.781
batch start
#iterations: 181
currently lose_sum: 354.90121644735336
time_elpased: 4.778
batch start
#iterations: 182
currently lose_sum: 356.7889466583729
time_elpased: 4.78
batch start
#iterations: 183
currently lose_sum: 356.87139946222305
time_elpased: 4.754
batch start
#iterations: 184
currently lose_sum: 355.7123175263405
time_elpased: 4.781
batch start
#iterations: 185
currently lose_sum: 356.6187233328819
time_elpased: 4.768
batch start
#iterations: 186
currently lose_sum: 356.76148360967636
time_elpased: 4.762
batch start
#iterations: 187
currently lose_sum: 354.9925502836704
time_elpased: 4.752
batch start
#iterations: 188
currently lose_sum: 357.5536885559559
time_elpased: 4.764
batch start
#iterations: 189
currently lose_sum: 356.8172699213028
time_elpased: 4.761
batch start
#iterations: 190
currently lose_sum: 355.08155834674835
time_elpased: 4.768
batch start
#iterations: 191
currently lose_sum: 356.1943284571171
time_elpased: 4.766
batch start
#iterations: 192
currently lose_sum: 357.5833242535591
time_elpased: 4.769
batch start
#iterations: 193
currently lose_sum: 357.0051512122154
time_elpased: 4.778
batch start
#iterations: 194
currently lose_sum: 356.48150154948235
time_elpased: 4.78
batch start
#iterations: 195
currently lose_sum: 356.2768121957779
time_elpased: 4.77
batch start
#iterations: 196
currently lose_sum: 358.0128726363182
time_elpased: 4.749
batch start
#iterations: 197
currently lose_sum: 356.27625489234924
time_elpased: 4.792
batch start
#iterations: 198
currently lose_sum: 357.04501217603683
time_elpased: 4.785
batch start
#iterations: 199
currently lose_sum: 355.87212550640106
time_elpased: 4.773
start validation test
0.757525773196
1.0
0.757525773196
0.862036602534
0
validation finish
batch start
#iterations: 200
currently lose_sum: 358.0525436401367
time_elpased: 4.76
batch start
#iterations: 201
currently lose_sum: 355.7745125889778
time_elpased: 4.783
batch start
#iterations: 202
currently lose_sum: 355.4323814511299
time_elpased: 4.775
batch start
#iterations: 203
currently lose_sum: 354.6088940501213
time_elpased: 4.769
batch start
#iterations: 204
currently lose_sum: 357.32675999403
time_elpased: 4.789
batch start
#iterations: 205
currently lose_sum: 354.6870898902416
time_elpased: 4.758
batch start
#iterations: 206
currently lose_sum: 357.11846566200256
time_elpased: 4.787
batch start
#iterations: 207
currently lose_sum: 358.3524577617645
time_elpased: 4.763
batch start
#iterations: 208
currently lose_sum: 356.9706435203552
time_elpased: 4.778
batch start
#iterations: 209
currently lose_sum: 355.1381423473358
time_elpased: 4.825
batch start
#iterations: 210
currently lose_sum: 355.91649371385574
time_elpased: 4.741
batch start
#iterations: 211
currently lose_sum: 355.99894642829895
time_elpased: 4.765
batch start
#iterations: 212
currently lose_sum: 356.3525857925415
time_elpased: 4.76
batch start
#iterations: 213
currently lose_sum: 357.44751638174057
time_elpased: 4.787
batch start
#iterations: 214
currently lose_sum: 356.3029662370682
time_elpased: 4.771
batch start
#iterations: 215
currently lose_sum: 355.0528107881546
time_elpased: 4.758
batch start
#iterations: 216
currently lose_sum: 356.6938987970352
time_elpased: 4.741
batch start
#iterations: 217
currently lose_sum: 356.44218757748604
time_elpased: 4.751
batch start
#iterations: 218
currently lose_sum: 356.2972582578659
time_elpased: 4.779
batch start
#iterations: 219
currently lose_sum: 357.102461874485
time_elpased: 4.767
start validation test
0.763917525773
1.0
0.763917525773
0.866160140269
0
validation finish
batch start
#iterations: 220
currently lose_sum: 354.8051310777664
time_elpased: 4.761
batch start
#iterations: 221
currently lose_sum: 356.0144916176796
time_elpased: 4.784
batch start
#iterations: 222
currently lose_sum: 357.0502275824547
time_elpased: 4.778
batch start
#iterations: 223
currently lose_sum: 356.9128831624985
time_elpased: 4.783
batch start
#iterations: 224
currently lose_sum: 356.18578857183456
time_elpased: 4.777
batch start
#iterations: 225
currently lose_sum: 356.1587233543396
time_elpased: 4.786
batch start
#iterations: 226
currently lose_sum: 355.7399207353592
time_elpased: 4.767
batch start
#iterations: 227
currently lose_sum: 357.212700009346
time_elpased: 4.796
batch start
#iterations: 228
currently lose_sum: 356.0326035618782
time_elpased: 4.746
batch start
#iterations: 229
currently lose_sum: 356.049133002758
time_elpased: 4.781
batch start
#iterations: 230
currently lose_sum: 356.39539325237274
time_elpased: 4.752
batch start
#iterations: 231
currently lose_sum: 356.69294661283493
time_elpased: 4.775
batch start
#iterations: 232
currently lose_sum: 356.5449796319008
time_elpased: 4.77
batch start
#iterations: 233
currently lose_sum: 356.07114613056183
time_elpased: 4.779
batch start
#iterations: 234
currently lose_sum: 356.3647970557213
time_elpased: 4.782
batch start
#iterations: 235
currently lose_sum: 354.80902022123337
time_elpased: 4.781
batch start
#iterations: 236
currently lose_sum: 355.82621681690216
time_elpased: 4.783
batch start
#iterations: 237
currently lose_sum: 355.80809009075165
time_elpased: 4.762
batch start
#iterations: 238
currently lose_sum: 356.2163238823414
time_elpased: 4.762
batch start
#iterations: 239
currently lose_sum: 354.2538819909096
time_elpased: 4.76
start validation test
0.77
1.0
0.77
0.870056497175
0
validation finish
batch start
#iterations: 240
currently lose_sum: 355.51830637454987
time_elpased: 4.753
batch start
#iterations: 241
currently lose_sum: 355.2604996562004
time_elpased: 4.769
batch start
#iterations: 242
currently lose_sum: 354.2144935131073
time_elpased: 4.749
batch start
#iterations: 243
currently lose_sum: 356.4274444580078
time_elpased: 4.77
batch start
#iterations: 244
currently lose_sum: 356.5072678923607
time_elpased: 4.755
batch start
#iterations: 245
currently lose_sum: 355.20052695274353
time_elpased: 4.787
batch start
#iterations: 246
currently lose_sum: 356.18684166669846
time_elpased: 4.79
batch start
#iterations: 247
currently lose_sum: 356.8189185857773
time_elpased: 4.781
batch start
#iterations: 248
currently lose_sum: 357.539841234684
time_elpased: 4.804
batch start
#iterations: 249
currently lose_sum: 355.21459302306175
time_elpased: 4.787
batch start
#iterations: 250
currently lose_sum: 356.81879913806915
time_elpased: 4.75
batch start
#iterations: 251
currently lose_sum: 357.58231377601624
time_elpased: 4.782
batch start
#iterations: 252
currently lose_sum: 356.3522013425827
time_elpased: 4.773
batch start
#iterations: 253
currently lose_sum: 355.0402327179909
time_elpased: 4.757
batch start
#iterations: 254
currently lose_sum: 356.36239862442017
time_elpased: 4.79
batch start
#iterations: 255
currently lose_sum: 356.6441188454628
time_elpased: 4.764
batch start
#iterations: 256
currently lose_sum: 355.81422477960587
time_elpased: 4.752
batch start
#iterations: 257
currently lose_sum: 357.6563910841942
time_elpased: 4.766
batch start
#iterations: 258
currently lose_sum: 357.86308443546295
time_elpased: 4.782
batch start
#iterations: 259
currently lose_sum: 355.7568693161011
time_elpased: 4.771
start validation test
0.783298969072
1.0
0.783298969072
0.878483061626
0
validation finish
batch start
#iterations: 260
currently lose_sum: 355.16384744644165
time_elpased: 4.793
batch start
#iterations: 261
currently lose_sum: 355.43394911289215
time_elpased: 4.78
batch start
#iterations: 262
currently lose_sum: 357.2411364912987
time_elpased: 4.771
batch start
#iterations: 263
currently lose_sum: 358.1976779103279
time_elpased: 4.78
batch start
#iterations: 264
currently lose_sum: 356.0849762558937
time_elpased: 4.776
batch start
#iterations: 265
currently lose_sum: 355.7163052558899
time_elpased: 4.768
batch start
#iterations: 266
currently lose_sum: 356.8996126651764
time_elpased: 4.771
batch start
#iterations: 267
currently lose_sum: 356.84082609415054
time_elpased: 4.784
batch start
#iterations: 268
currently lose_sum: 356.5972203016281
time_elpased: 4.763
batch start
#iterations: 269
currently lose_sum: 357.4464901685715
time_elpased: 4.755
batch start
#iterations: 270
currently lose_sum: 354.8407760858536
time_elpased: 4.765
batch start
#iterations: 271
currently lose_sum: 356.35625672340393
time_elpased: 4.779
batch start
#iterations: 272
currently lose_sum: 356.49678605794907
time_elpased: 4.764
batch start
#iterations: 273
currently lose_sum: 355.7888994216919
time_elpased: 4.909
batch start
#iterations: 274
currently lose_sum: 356.31236147880554
time_elpased: 4.767
batch start
#iterations: 275
currently lose_sum: 357.08422672748566
time_elpased: 4.756
batch start
#iterations: 276
currently lose_sum: 355.6465440392494
time_elpased: 4.789
batch start
#iterations: 277
currently lose_sum: 356.0541110634804
time_elpased: 4.775
batch start
#iterations: 278
currently lose_sum: 355.6366253197193
time_elpased: 4.766
batch start
#iterations: 279
currently lose_sum: 355.47937977313995
time_elpased: 4.761
start validation test
0.769896907216
1.0
0.769896907216
0.869990680336
0
validation finish
batch start
#iterations: 280
currently lose_sum: 355.38708198070526
time_elpased: 4.769
batch start
#iterations: 281
currently lose_sum: 357.3017089366913
time_elpased: 4.786
batch start
#iterations: 282
currently lose_sum: 356.0763785839081
time_elpased: 4.784
batch start
#iterations: 283
currently lose_sum: 354.9724406301975
time_elpased: 4.772
batch start
#iterations: 284
currently lose_sum: 356.371805369854
time_elpased: 4.759
batch start
#iterations: 285
currently lose_sum: 354.0325248837471
time_elpased: 4.792
batch start
#iterations: 286
currently lose_sum: 355.46145221590996
time_elpased: 4.786
batch start
#iterations: 287
currently lose_sum: 355.25967532396317
time_elpased: 4.774
batch start
#iterations: 288
currently lose_sum: 355.65690314769745
time_elpased: 4.812
batch start
#iterations: 289
currently lose_sum: 355.3842846751213
time_elpased: 4.796
batch start
#iterations: 290
currently lose_sum: 355.233884036541
time_elpased: 4.786
batch start
#iterations: 291
currently lose_sum: 355.52137917280197
time_elpased: 4.769
batch start
#iterations: 292
currently lose_sum: 356.2478774189949
time_elpased: 4.772
batch start
#iterations: 293
currently lose_sum: 356.0175335109234
time_elpased: 4.768
batch start
#iterations: 294
currently lose_sum: 354.7197390794754
time_elpased: 4.773
batch start
#iterations: 295
currently lose_sum: 355.5666832923889
time_elpased: 4.79
batch start
#iterations: 296
currently lose_sum: 355.0587849020958
time_elpased: 4.799
batch start
#iterations: 297
currently lose_sum: 355.50370758771896
time_elpased: 4.786
batch start
#iterations: 298
currently lose_sum: 354.673371553421
time_elpased: 4.783
batch start
#iterations: 299
currently lose_sum: 355.20520824193954
time_elpased: 4.772
start validation test
0.790309278351
1.0
0.790309278351
0.882874582518
0
validation finish
batch start
#iterations: 300
currently lose_sum: 355.1455553174019
time_elpased: 4.758
batch start
#iterations: 301
currently lose_sum: 355.6870827674866
time_elpased: 4.765
batch start
#iterations: 302
currently lose_sum: 357.2749761939049
time_elpased: 4.779
batch start
#iterations: 303
currently lose_sum: 356.82900977134705
time_elpased: 4.781
batch start
#iterations: 304
currently lose_sum: 356.0890946984291
time_elpased: 4.752
batch start
#iterations: 305
currently lose_sum: 356.45693004131317
time_elpased: 4.765
batch start
#iterations: 306
currently lose_sum: 354.2555707991123
time_elpased: 4.78
batch start
#iterations: 307
currently lose_sum: 354.77357590198517
time_elpased: 4.803
batch start
#iterations: 308
currently lose_sum: 354.7655894756317
time_elpased: 4.763
batch start
#iterations: 309
currently lose_sum: 355.4795297384262
time_elpased: 4.782
batch start
#iterations: 310
currently lose_sum: 355.0935006737709
time_elpased: 4.763
batch start
#iterations: 311
currently lose_sum: 356.807736068964
time_elpased: 4.774
batch start
#iterations: 312
currently lose_sum: 352.74966061115265
time_elpased: 4.767
batch start
#iterations: 313
currently lose_sum: 354.9045821726322
time_elpased: 4.765
batch start
#iterations: 314
currently lose_sum: 356.75745046138763
time_elpased: 4.753
batch start
#iterations: 315
currently lose_sum: 356.609034717083
time_elpased: 4.763
batch start
#iterations: 316
currently lose_sum: 354.9198417067528
time_elpased: 4.802
batch start
#iterations: 317
currently lose_sum: 357.1616048812866
time_elpased: 4.768
batch start
#iterations: 318
currently lose_sum: 356.9072415828705
time_elpased: 4.778
batch start
#iterations: 319
currently lose_sum: 355.86053627729416
time_elpased: 4.796
start validation test
0.788041237113
1.0
0.788041237113
0.881457564576
0
validation finish
batch start
#iterations: 320
currently lose_sum: 355.073670566082
time_elpased: 4.772
batch start
#iterations: 321
currently lose_sum: 356.1033199429512
time_elpased: 4.755
batch start
#iterations: 322
currently lose_sum: 355.3677918314934
time_elpased: 4.772
batch start
#iterations: 323
currently lose_sum: 356.60300356149673
time_elpased: 4.774
batch start
#iterations: 324
currently lose_sum: 356.15784752368927
time_elpased: 4.777
batch start
#iterations: 325
currently lose_sum: 356.0504646897316
time_elpased: 4.759
batch start
#iterations: 326
currently lose_sum: 356.0544458925724
time_elpased: 4.788
batch start
#iterations: 327
currently lose_sum: 355.1012496948242
time_elpased: 4.768
batch start
#iterations: 328
currently lose_sum: 354.6709736585617
time_elpased: 4.78
batch start
#iterations: 329
currently lose_sum: 355.23747104406357
time_elpased: 4.766
batch start
#iterations: 330
currently lose_sum: 356.50174313783646
time_elpased: 4.76
batch start
#iterations: 331
currently lose_sum: 354.1644561290741
time_elpased: 4.771
batch start
#iterations: 332
currently lose_sum: 355.03239104151726
time_elpased: 4.766
batch start
#iterations: 333
currently lose_sum: 355.86931306123734
time_elpased: 4.785
batch start
#iterations: 334
currently lose_sum: 355.17508071660995
time_elpased: 4.794
batch start
#iterations: 335
currently lose_sum: 355.2734405994415
time_elpased: 4.795
batch start
#iterations: 336
currently lose_sum: 356.6917201578617
time_elpased: 4.753
batch start
#iterations: 337
currently lose_sum: 354.13159626722336
time_elpased: 4.776
batch start
#iterations: 338
currently lose_sum: 355.94499427080154
time_elpased: 4.779
batch start
#iterations: 339
currently lose_sum: 355.15935415029526
time_elpased: 4.778
start validation test
0.776494845361
1.0
0.776494845361
0.874187558032
0
validation finish
batch start
#iterations: 340
currently lose_sum: 355.42608964443207
time_elpased: 4.769
batch start
#iterations: 341
currently lose_sum: 356.30550277233124
time_elpased: 4.758
batch start
#iterations: 342
currently lose_sum: 354.5624789595604
time_elpased: 4.791
batch start
#iterations: 343
currently lose_sum: 355.412508636713
time_elpased: 4.767
batch start
#iterations: 344
currently lose_sum: 354.5409994125366
time_elpased: 4.763
batch start
#iterations: 345
currently lose_sum: 354.8924402296543
time_elpased: 4.786
batch start
#iterations: 346
currently lose_sum: 355.533911049366
time_elpased: 4.793
batch start
#iterations: 347
currently lose_sum: 356.76267290115356
time_elpased: 4.766
batch start
#iterations: 348
currently lose_sum: 353.7730489373207
time_elpased: 4.764
batch start
#iterations: 349
currently lose_sum: 355.49554896354675
time_elpased: 4.764
batch start
#iterations: 350
currently lose_sum: 355.659539937973
time_elpased: 4.788
batch start
#iterations: 351
currently lose_sum: 355.6750929951668
time_elpased: 4.765
batch start
#iterations: 352
currently lose_sum: 357.6805348992348
time_elpased: 4.769
batch start
#iterations: 353
currently lose_sum: 355.5240150690079
time_elpased: 4.786
batch start
#iterations: 354
currently lose_sum: 355.12987142801285
time_elpased: 4.799
batch start
#iterations: 355
currently lose_sum: 354.02672958374023
time_elpased: 4.754
batch start
#iterations: 356
currently lose_sum: 355.85585719347
time_elpased: 4.764
batch start
#iterations: 357
currently lose_sum: 355.49888294935226
time_elpased: 4.763
batch start
#iterations: 358
currently lose_sum: 354.8252098560333
time_elpased: 4.783
batch start
#iterations: 359
currently lose_sum: 353.67320746183395
time_elpased: 4.775
start validation test
0.767113402062
1.0
0.767113402062
0.868210722828
0
validation finish
batch start
#iterations: 360
currently lose_sum: 356.2854525744915
time_elpased: 4.77
batch start
#iterations: 361
currently lose_sum: 355.1021952033043
time_elpased: 4.769
batch start
#iterations: 362
currently lose_sum: 354.50912857055664
time_elpased: 4.786
batch start
#iterations: 363
currently lose_sum: 357.8561969399452
time_elpased: 4.773
batch start
#iterations: 364
currently lose_sum: 354.46444940567017
time_elpased: 4.746
batch start
#iterations: 365
currently lose_sum: 355.20787304639816
time_elpased: 4.757
batch start
#iterations: 366
currently lose_sum: 355.9816257953644
time_elpased: 4.776
batch start
#iterations: 367
currently lose_sum: 356.030503988266
time_elpased: 4.771
batch start
#iterations: 368
currently lose_sum: 354.2408464550972
time_elpased: 4.801
batch start
#iterations: 369
currently lose_sum: 354.0577386021614
time_elpased: 4.802
batch start
#iterations: 370
currently lose_sum: 354.8763285279274
time_elpased: 4.771
batch start
#iterations: 371
currently lose_sum: 356.377878010273
time_elpased: 4.781
batch start
#iterations: 372
currently lose_sum: 356.19159722328186
time_elpased: 4.762
batch start
#iterations: 373
currently lose_sum: 356.07002568244934
time_elpased: 4.759
batch start
#iterations: 374
currently lose_sum: 356.3767844438553
time_elpased: 4.79
batch start
#iterations: 375
currently lose_sum: 354.4072954058647
time_elpased: 4.792
batch start
#iterations: 376
currently lose_sum: 355.1289309859276
time_elpased: 4.772
batch start
#iterations: 377
currently lose_sum: 355.52275878190994
time_elpased: 4.763
batch start
#iterations: 378
currently lose_sum: 355.2068659067154
time_elpased: 4.76
batch start
#iterations: 379
currently lose_sum: 355.2005697488785
time_elpased: 4.771
start validation test
0.781237113402
1.0
0.781237113402
0.877184859359
0
validation finish
batch start
#iterations: 380
currently lose_sum: 356.5568263232708
time_elpased: 4.782
batch start
#iterations: 381
currently lose_sum: 356.1700293123722
time_elpased: 4.771
batch start
#iterations: 382
currently lose_sum: 355.7947107255459
time_elpased: 4.785
batch start
#iterations: 383
currently lose_sum: 355.55203607678413
time_elpased: 4.795
batch start
#iterations: 384
currently lose_sum: 354.9847522974014
time_elpased: 4.792
batch start
#iterations: 385
currently lose_sum: 354.2734754085541
time_elpased: 4.811
batch start
#iterations: 386
currently lose_sum: 355.8824177980423
time_elpased: 4.761
batch start
#iterations: 387
currently lose_sum: 354.3148216307163
time_elpased: 4.808
batch start
#iterations: 388
currently lose_sum: 355.32779544591904
time_elpased: 4.771
batch start
#iterations: 389
currently lose_sum: 354.590612500906
time_elpased: 4.764
batch start
#iterations: 390
currently lose_sum: 354.2183177471161
time_elpased: 4.789
batch start
#iterations: 391
currently lose_sum: 354.33743965625763
time_elpased: 4.794
batch start
#iterations: 392
currently lose_sum: 355.64059096574783
time_elpased: 4.788
batch start
#iterations: 393
currently lose_sum: 355.9073370695114
time_elpased: 4.792
batch start
#iterations: 394
currently lose_sum: 356.23588824272156
time_elpased: 4.777
batch start
#iterations: 395
currently lose_sum: 355.5898902416229
time_elpased: 4.791
batch start
#iterations: 396
currently lose_sum: 356.2225340604782
time_elpased: 4.805
batch start
#iterations: 397
currently lose_sum: 353.30595514178276
time_elpased: 4.785
batch start
#iterations: 398
currently lose_sum: 355.60963904857635
time_elpased: 4.786
batch start
#iterations: 399
currently lose_sum: 354.0942684710026
time_elpased: 4.787
start validation test
0.784536082474
1.0
0.784536082474
0.879260543039
0
validation finish
acc: 0.792
pre: 1.000
rec: 0.792
F1: 0.884
auc: 0.000
