start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 557.1574749350548
time_elpased: 1.41
batch start
#iterations: 1
currently lose_sum: 555.192500948906
time_elpased: 1.361
batch start
#iterations: 2
currently lose_sum: 555.8406697511673
time_elpased: 1.37
batch start
#iterations: 3
currently lose_sum: 554.5752645134926
time_elpased: 1.387
batch start
#iterations: 4
currently lose_sum: 553.582300722599
time_elpased: 1.386
batch start
#iterations: 5
currently lose_sum: 553.575705230236
time_elpased: 1.383
batch start
#iterations: 6
currently lose_sum: 550.9169132113457
time_elpased: 1.381
batch start
#iterations: 7
currently lose_sum: 551.5113204717636
time_elpased: 1.39
batch start
#iterations: 8
currently lose_sum: 549.6914242506027
time_elpased: 1.382
batch start
#iterations: 9
currently lose_sum: 550.8370047807693
time_elpased: 1.384
batch start
#iterations: 10
currently lose_sum: 548.5119609832764
time_elpased: 1.386
batch start
#iterations: 11
currently lose_sum: 550.590972661972
time_elpased: 1.374
batch start
#iterations: 12
currently lose_sum: 548.2220547199249
time_elpased: 1.375
batch start
#iterations: 13
currently lose_sum: 545.7438339591026
time_elpased: 1.381
batch start
#iterations: 14
currently lose_sum: 545.0785501003265
time_elpased: 1.392
batch start
#iterations: 15
currently lose_sum: 544.5528307557106
time_elpased: 1.369
batch start
#iterations: 16
currently lose_sum: 543.6816576719284
time_elpased: 1.375
batch start
#iterations: 17
currently lose_sum: 542.3826813697815
time_elpased: 1.381
batch start
#iterations: 18
currently lose_sum: 541.8664419651031
time_elpased: 1.404
batch start
#iterations: 19
currently lose_sum: 540.8255308270454
time_elpased: 1.386
start validation test
0.112577319588
1.0
0.112577319588
0.202372127502
0
validation finish
batch start
#iterations: 20
currently lose_sum: 539.6991192996502
time_elpased: 1.393
batch start
#iterations: 21
currently lose_sum: 540.5380075871944
time_elpased: 1.404
batch start
#iterations: 22
currently lose_sum: 539.2730639576912
time_elpased: 1.404
batch start
#iterations: 23
currently lose_sum: 538.2653223574162
time_elpased: 1.377
batch start
#iterations: 24
currently lose_sum: 536.4657168388367
time_elpased: 1.379
batch start
#iterations: 25
currently lose_sum: 536.2567511200905
time_elpased: 1.388
batch start
#iterations: 26
currently lose_sum: 537.1802497506142
time_elpased: 1.4
batch start
#iterations: 27
currently lose_sum: 535.9542762041092
time_elpased: 1.379
batch start
#iterations: 28
currently lose_sum: 534.3167636394501
time_elpased: 1.394
batch start
#iterations: 29
currently lose_sum: 533.384459733963
time_elpased: 1.383
batch start
#iterations: 30
currently lose_sum: 532.6945796608925
time_elpased: 1.381
batch start
#iterations: 31
currently lose_sum: 533.7156197428703
time_elpased: 1.4
batch start
#iterations: 32
currently lose_sum: 533.571567595005
time_elpased: 1.387
batch start
#iterations: 33
currently lose_sum: 532.6373035609722
time_elpased: 1.401
batch start
#iterations: 34
currently lose_sum: 532.5968392491341
time_elpased: 1.391
batch start
#iterations: 35
currently lose_sum: 530.7586669027805
time_elpased: 1.401
batch start
#iterations: 36
currently lose_sum: 532.7147285938263
time_elpased: 1.386
batch start
#iterations: 37
currently lose_sum: 528.5057284832001
time_elpased: 1.379
batch start
#iterations: 38
currently lose_sum: 530.6473563611507
time_elpased: 1.385
batch start
#iterations: 39
currently lose_sum: 530.7589376866817
time_elpased: 1.395
start validation test
0.0910309278351
1.0
0.0910309278351
0.166871397524
0
validation finish
batch start
#iterations: 40
currently lose_sum: 531.1566133201122
time_elpased: 1.401
batch start
#iterations: 41
currently lose_sum: 529.8958609700203
time_elpased: 1.374
batch start
#iterations: 42
currently lose_sum: 528.8600576221943
time_elpased: 1.38
batch start
#iterations: 43
currently lose_sum: 527.501707226038
time_elpased: 1.391
batch start
#iterations: 44
currently lose_sum: 529.8307386338711
time_elpased: 1.374
batch start
#iterations: 45
currently lose_sum: 529.8039953708649
time_elpased: 1.384
batch start
#iterations: 46
currently lose_sum: 527.3829850256443
time_elpased: 1.389
batch start
#iterations: 47
currently lose_sum: 528.6814815104008
time_elpased: 1.402
batch start
#iterations: 48
currently lose_sum: 529.0160195529461
time_elpased: 1.387
batch start
#iterations: 49
currently lose_sum: 525.398545473814
time_elpased: 1.374
batch start
#iterations: 50
currently lose_sum: 527.7407675683498
time_elpased: 1.401
batch start
#iterations: 51
currently lose_sum: 527.2133196890354
time_elpased: 1.398
batch start
#iterations: 52
currently lose_sum: 526.8615820705891
time_elpased: 1.384
batch start
#iterations: 53
currently lose_sum: 528.5572427213192
time_elpased: 1.383
batch start
#iterations: 54
currently lose_sum: 525.4332267045975
time_elpased: 1.381
batch start
#iterations: 55
currently lose_sum: 528.0202205181122
time_elpased: 1.371
batch start
#iterations: 56
currently lose_sum: 526.9203128516674
time_elpased: 1.383
batch start
#iterations: 57
currently lose_sum: 525.2069502770901
time_elpased: 1.383
batch start
#iterations: 58
currently lose_sum: 527.2919037342072
time_elpased: 1.397
batch start
#iterations: 59
currently lose_sum: 526.1088152527809
time_elpased: 1.379
start validation test
0.130618556701
1.0
0.130618556701
0.231056806784
0
validation finish
batch start
#iterations: 60
currently lose_sum: 525.2259405553341
time_elpased: 1.389
batch start
#iterations: 61
currently lose_sum: 523.8935499191284
time_elpased: 1.392
batch start
#iterations: 62
currently lose_sum: 528.2737239599228
time_elpased: 1.38
batch start
#iterations: 63
currently lose_sum: 526.8028970658779
time_elpased: 1.373
batch start
#iterations: 64
currently lose_sum: 525.4401320815086
time_elpased: 1.387
batch start
#iterations: 65
currently lose_sum: 525.8935860395432
time_elpased: 1.398
batch start
#iterations: 66
currently lose_sum: 524.6222878992558
time_elpased: 1.381
batch start
#iterations: 67
currently lose_sum: 523.5925997495651
time_elpased: 1.4
batch start
#iterations: 68
currently lose_sum: 524.8476516604424
time_elpased: 1.394
batch start
#iterations: 69
currently lose_sum: 527.9733086228371
time_elpased: 1.382
batch start
#iterations: 70
currently lose_sum: 522.8086042702198
time_elpased: 1.386
batch start
#iterations: 71
currently lose_sum: 524.5852413475513
time_elpased: 1.392
batch start
#iterations: 72
currently lose_sum: 525.4114507138729
time_elpased: 1.402
batch start
#iterations: 73
currently lose_sum: 523.4863561093807
time_elpased: 1.39
batch start
#iterations: 74
currently lose_sum: 524.8251352608204
time_elpased: 1.384
batch start
#iterations: 75
currently lose_sum: 523.3713919222355
time_elpased: 1.395
batch start
#iterations: 76
currently lose_sum: 523.1485494077206
time_elpased: 1.387
batch start
#iterations: 77
currently lose_sum: 524.0039365887642
time_elpased: 1.386
batch start
#iterations: 78
currently lose_sum: 524.4330335855484
time_elpased: 1.368
batch start
#iterations: 79
currently lose_sum: 525.9013127982616
time_elpased: 1.394
start validation test
0.115360824742
1.0
0.115360824742
0.206858304834
0
validation finish
batch start
#iterations: 80
currently lose_sum: 523.8627794384956
time_elpased: 1.387
batch start
#iterations: 81
currently lose_sum: 523.0102933347225
time_elpased: 1.376
batch start
#iterations: 82
currently lose_sum: 525.1214751899242
time_elpased: 1.396
batch start
#iterations: 83
currently lose_sum: 523.2654485106468
time_elpased: 1.39
batch start
#iterations: 84
currently lose_sum: 523.1866002380848
time_elpased: 1.391
batch start
#iterations: 85
currently lose_sum: 522.0681505799294
time_elpased: 1.384
batch start
#iterations: 86
currently lose_sum: 523.308896958828
time_elpased: 1.393
batch start
#iterations: 87
currently lose_sum: 521.8845354914665
time_elpased: 1.398
batch start
#iterations: 88
currently lose_sum: 523.7933234274387
time_elpased: 1.374
batch start
#iterations: 89
currently lose_sum: 524.5259304642677
time_elpased: 1.385
batch start
#iterations: 90
currently lose_sum: 523.6572306454182
time_elpased: 1.391
batch start
#iterations: 91
currently lose_sum: 521.3341350853443
time_elpased: 1.397
batch start
#iterations: 92
currently lose_sum: 522.6120076477528
time_elpased: 1.396
batch start
#iterations: 93
currently lose_sum: 523.9600696861744
time_elpased: 1.386
batch start
#iterations: 94
currently lose_sum: 522.0703618824482
time_elpased: 1.378
batch start
#iterations: 95
currently lose_sum: 522.3579579889774
time_elpased: 1.408
batch start
#iterations: 96
currently lose_sum: 523.9640639126301
time_elpased: 1.386
batch start
#iterations: 97
currently lose_sum: 520.368626832962
time_elpased: 1.387
batch start
#iterations: 98
currently lose_sum: 523.036159902811
time_elpased: 1.39
batch start
#iterations: 99
currently lose_sum: 523.3973997831345
time_elpased: 1.389
start validation test
0.171237113402
1.0
0.171237113402
0.29240383769
0
validation finish
batch start
#iterations: 100
currently lose_sum: 522.3223212659359
time_elpased: 1.399
batch start
#iterations: 101
currently lose_sum: 523.1471477150917
time_elpased: 1.402
batch start
#iterations: 102
currently lose_sum: 520.4743420183659
time_elpased: 1.387
batch start
#iterations: 103
currently lose_sum: 522.7861883938313
time_elpased: 1.392
batch start
#iterations: 104
currently lose_sum: 522.1113622188568
time_elpased: 1.387
batch start
#iterations: 105
currently lose_sum: 521.9081188440323
time_elpased: 1.382
batch start
#iterations: 106
currently lose_sum: 523.739061653614
time_elpased: 1.385
batch start
#iterations: 107
currently lose_sum: 521.4188832342625
time_elpased: 1.393
batch start
#iterations: 108
currently lose_sum: 520.0238611400127
time_elpased: 1.407
batch start
#iterations: 109
currently lose_sum: 522.0351066589355
time_elpased: 1.388
batch start
#iterations: 110
currently lose_sum: 524.0903158187866
time_elpased: 1.378
batch start
#iterations: 111
currently lose_sum: 524.4984114468098
time_elpased: 1.382
batch start
#iterations: 112
currently lose_sum: 519.9401162862778
time_elpased: 1.383
batch start
#iterations: 113
currently lose_sum: 522.2595536112785
time_elpased: 1.409
batch start
#iterations: 114
currently lose_sum: 522.2930386066437
time_elpased: 1.395
batch start
#iterations: 115
currently lose_sum: 521.1172001063824
time_elpased: 1.389
batch start
#iterations: 116
currently lose_sum: 520.5372743904591
time_elpased: 1.404
batch start
#iterations: 117
currently lose_sum: 522.5690210163593
time_elpased: 1.412
batch start
#iterations: 118
currently lose_sum: 520.2588010132313
time_elpased: 1.385
batch start
#iterations: 119
currently lose_sum: 521.0985886156559
time_elpased: 1.39
start validation test
0.237113402062
1.0
0.237113402062
0.383333333333
0
validation finish
batch start
#iterations: 120
currently lose_sum: 522.2035889625549
time_elpased: 1.392
batch start
#iterations: 121
currently lose_sum: 520.6863481402397
time_elpased: 1.39
batch start
#iterations: 122
currently lose_sum: 520.5715952813625
time_elpased: 1.403
batch start
#iterations: 123
currently lose_sum: 520.6066507697105
time_elpased: 1.379
batch start
#iterations: 124
currently lose_sum: 520.379685997963
time_elpased: 1.383
batch start
#iterations: 125
currently lose_sum: 521.7914738953114
time_elpased: 1.393
batch start
#iterations: 126
currently lose_sum: 521.7909314036369
time_elpased: 1.376
batch start
#iterations: 127
currently lose_sum: 520.5404640436172
time_elpased: 1.379
batch start
#iterations: 128
currently lose_sum: 520.5666138529778
time_elpased: 1.384
batch start
#iterations: 129
currently lose_sum: 521.60068577528
time_elpased: 1.387
batch start
#iterations: 130
currently lose_sum: 520.2706990838051
time_elpased: 1.408
batch start
#iterations: 131
currently lose_sum: 521.4717777371407
time_elpased: 1.382
batch start
#iterations: 132
currently lose_sum: 520.7462185621262
time_elpased: 1.386
batch start
#iterations: 133
currently lose_sum: 522.4113129973412
time_elpased: 1.397
batch start
#iterations: 134
currently lose_sum: 520.1967404782772
time_elpased: 1.377
batch start
#iterations: 135
currently lose_sum: 518.8894695043564
time_elpased: 1.396
batch start
#iterations: 136
currently lose_sum: 520.0672575235367
time_elpased: 1.377
batch start
#iterations: 137
currently lose_sum: 519.4282894730568
time_elpased: 1.391
batch start
#iterations: 138
currently lose_sum: 519.1932121515274
time_elpased: 1.385
batch start
#iterations: 139
currently lose_sum: 520.3519628942013
time_elpased: 1.389
start validation test
0.116907216495
1.0
0.116907216495
0.209340963633
0
validation finish
batch start
#iterations: 140
currently lose_sum: 518.3550197482109
time_elpased: 1.379
batch start
#iterations: 141
currently lose_sum: 520.2995947897434
time_elpased: 1.39
batch start
#iterations: 142
currently lose_sum: 518.7852591574192
time_elpased: 1.38
batch start
#iterations: 143
currently lose_sum: 520.5043158829212
time_elpased: 1.396
batch start
#iterations: 144
currently lose_sum: 519.7274123728275
time_elpased: 1.402
batch start
#iterations: 145
currently lose_sum: 522.2390620410442
time_elpased: 1.416
batch start
#iterations: 146
currently lose_sum: 522.3755361437798
time_elpased: 1.393
batch start
#iterations: 147
currently lose_sum: 520.8364662826061
time_elpased: 1.38
batch start
#iterations: 148
currently lose_sum: 518.7713845968246
time_elpased: 1.382
batch start
#iterations: 149
currently lose_sum: 521.5096484422684
time_elpased: 1.369
batch start
#iterations: 150
currently lose_sum: 518.849717438221
time_elpased: 1.39
batch start
#iterations: 151
currently lose_sum: 518.8612486422062
time_elpased: 1.386
batch start
#iterations: 152
currently lose_sum: 519.7631861269474
time_elpased: 1.388
batch start
#iterations: 153
currently lose_sum: 517.530274450779
time_elpased: 1.392
batch start
#iterations: 154
currently lose_sum: 522.0036724507809
time_elpased: 1.388
batch start
#iterations: 155
currently lose_sum: 517.4820791780949
time_elpased: 1.384
batch start
#iterations: 156
currently lose_sum: 519.9873968064785
time_elpased: 1.368
batch start
#iterations: 157
currently lose_sum: 518.3457232415676
time_elpased: 1.402
batch start
#iterations: 158
currently lose_sum: 519.081981241703
time_elpased: 1.393
batch start
#iterations: 159
currently lose_sum: 519.8110594451427
time_elpased: 1.38
start validation test
0.159793814433
1.0
0.159793814433
0.275555555556
0
validation finish
batch start
#iterations: 160
currently lose_sum: 520.2889975011349
time_elpased: 1.395
batch start
#iterations: 161
currently lose_sum: 518.5832742154598
time_elpased: 1.374
batch start
#iterations: 162
currently lose_sum: 519.1284683644772
time_elpased: 1.386
batch start
#iterations: 163
currently lose_sum: 518.9679057896137
time_elpased: 1.399
batch start
#iterations: 164
currently lose_sum: 519.9510876238346
time_elpased: 1.388
batch start
#iterations: 165
currently lose_sum: 521.2288784384727
time_elpased: 1.391
batch start
#iterations: 166
currently lose_sum: 520.1964457333088
time_elpased: 1.404
batch start
#iterations: 167
currently lose_sum: 519.5050774514675
time_elpased: 1.389
batch start
#iterations: 168
currently lose_sum: 518.9934267699718
time_elpased: 1.392
batch start
#iterations: 169
currently lose_sum: 519.556848347187
time_elpased: 1.384
batch start
#iterations: 170
currently lose_sum: 519.4690425097942
time_elpased: 1.386
batch start
#iterations: 171
currently lose_sum: 517.794678747654
time_elpased: 1.385
batch start
#iterations: 172
currently lose_sum: 521.5477197766304
time_elpased: 1.386
batch start
#iterations: 173
currently lose_sum: 519.77874314785
time_elpased: 1.403
batch start
#iterations: 174
currently lose_sum: 519.9281747937202
time_elpased: 1.395
batch start
#iterations: 175
currently lose_sum: 518.8973067998886
time_elpased: 1.393
batch start
#iterations: 176
currently lose_sum: 518.7645618617535
time_elpased: 1.394
batch start
#iterations: 177
currently lose_sum: 520.342953979969
time_elpased: 1.394
batch start
#iterations: 178
currently lose_sum: 520.5137016177177
time_elpased: 1.385
batch start
#iterations: 179
currently lose_sum: 521.0556137561798
time_elpased: 1.392
start validation test
0.208041237113
1.0
0.208041237113
0.344427376685
0
validation finish
batch start
#iterations: 180
currently lose_sum: 519.8328466117382
time_elpased: 1.392
batch start
#iterations: 181
currently lose_sum: 520.8661553859711
time_elpased: 1.384
batch start
#iterations: 182
currently lose_sum: 519.4055109620094
time_elpased: 1.385
batch start
#iterations: 183
currently lose_sum: 519.5617384314537
time_elpased: 1.388
batch start
#iterations: 184
currently lose_sum: 518.5049301087856
time_elpased: 1.391
batch start
#iterations: 185
currently lose_sum: 520.2916790246964
time_elpased: 1.378
batch start
#iterations: 186
currently lose_sum: 519.7934103608131
time_elpased: 1.396
batch start
#iterations: 187
currently lose_sum: 520.1461162567139
time_elpased: 1.382
batch start
#iterations: 188
currently lose_sum: 518.3638195097446
time_elpased: 1.382
batch start
#iterations: 189
currently lose_sum: 519.6685553193092
time_elpased: 1.391
batch start
#iterations: 190
currently lose_sum: 518.5901590287685
time_elpased: 1.388
batch start
#iterations: 191
currently lose_sum: 519.9933250546455
time_elpased: 1.376
batch start
#iterations: 192
currently lose_sum: 517.1111381649971
time_elpased: 1.38
batch start
#iterations: 193
currently lose_sum: 519.3680908679962
time_elpased: 1.375
batch start
#iterations: 194
currently lose_sum: 520.2223468720913
time_elpased: 1.384
batch start
#iterations: 195
currently lose_sum: 521.9737989604473
time_elpased: 1.382
batch start
#iterations: 196
currently lose_sum: 516.4312562346458
time_elpased: 1.381
batch start
#iterations: 197
currently lose_sum: 520.5468195080757
time_elpased: 1.38
batch start
#iterations: 198
currently lose_sum: 517.9004536867142
time_elpased: 1.396
batch start
#iterations: 199
currently lose_sum: 518.9501457512379
time_elpased: 1.398
start validation test
0.162371134021
1.0
0.162371134021
0.279379157428
0
validation finish
batch start
#iterations: 200
currently lose_sum: 519.0424747765064
time_elpased: 1.389
batch start
#iterations: 201
currently lose_sum: 520.6783018708229
time_elpased: 1.396
batch start
#iterations: 202
currently lose_sum: 518.1288695633411
time_elpased: 1.4
batch start
#iterations: 203
currently lose_sum: 518.0075751543045
time_elpased: 1.401
batch start
#iterations: 204
currently lose_sum: 517.975517064333
time_elpased: 1.401
batch start
#iterations: 205
currently lose_sum: 518.9722921252251
time_elpased: 1.382
batch start
#iterations: 206
currently lose_sum: 521.4555951058865
time_elpased: 1.377
batch start
#iterations: 207
currently lose_sum: 517.8834691941738
time_elpased: 1.385
batch start
#iterations: 208
currently lose_sum: 517.0238624811172
time_elpased: 1.399
batch start
#iterations: 209
currently lose_sum: 518.8004869818687
time_elpased: 1.384
batch start
#iterations: 210
currently lose_sum: 518.1132575571537
time_elpased: 1.373
batch start
#iterations: 211
currently lose_sum: 519.4152965843678
time_elpased: 1.379
batch start
#iterations: 212
currently lose_sum: 518.6974999606609
time_elpased: 1.399
batch start
#iterations: 213
currently lose_sum: 518.3486480414867
time_elpased: 1.379
batch start
#iterations: 214
currently lose_sum: 519.7145879268646
time_elpased: 1.382
batch start
#iterations: 215
currently lose_sum: 518.5074118375778
time_elpased: 1.389
batch start
#iterations: 216
currently lose_sum: 519.2379349768162
time_elpased: 1.378
batch start
#iterations: 217
currently lose_sum: 518.4059087336063
time_elpased: 1.38
batch start
#iterations: 218
currently lose_sum: 517.8727183938026
time_elpased: 1.379
batch start
#iterations: 219
currently lose_sum: 520.1127082109451
time_elpased: 1.389
start validation test
0.130103092784
1.0
0.130103092784
0.230249954388
0
validation finish
batch start
#iterations: 220
currently lose_sum: 518.9872166514397
time_elpased: 1.375
batch start
#iterations: 221
currently lose_sum: 519.1455361545086
time_elpased: 1.376
batch start
#iterations: 222
currently lose_sum: 517.2665491104126
time_elpased: 1.372
batch start
#iterations: 223
currently lose_sum: 519.1039221584797
time_elpased: 1.388
batch start
#iterations: 224
currently lose_sum: 518.4817743897438
time_elpased: 1.382
batch start
#iterations: 225
currently lose_sum: 521.0913168787956
time_elpased: 1.385
batch start
#iterations: 226
currently lose_sum: 518.6929849386215
time_elpased: 1.375
batch start
#iterations: 227
currently lose_sum: 517.8894499242306
time_elpased: 1.374
batch start
#iterations: 228
currently lose_sum: 516.8377570211887
time_elpased: 1.394
batch start
#iterations: 229
currently lose_sum: 519.0221610367298
time_elpased: 1.395
batch start
#iterations: 230
currently lose_sum: 519.4088925719261
time_elpased: 1.4
batch start
#iterations: 231
currently lose_sum: 521.2536903321743
time_elpased: 1.409
batch start
#iterations: 232
currently lose_sum: 518.0233673155308
time_elpased: 1.381
batch start
#iterations: 233
currently lose_sum: 515.8107549548149
time_elpased: 1.387
batch start
#iterations: 234
currently lose_sum: 517.0924245715141
time_elpased: 1.391
batch start
#iterations: 235
currently lose_sum: 517.5420190691948
time_elpased: 1.39
batch start
#iterations: 236
currently lose_sum: 517.5043810904026
time_elpased: 1.382
batch start
#iterations: 237
currently lose_sum: 518.7708595991135
time_elpased: 1.387
batch start
#iterations: 238
currently lose_sum: 517.0047268271446
time_elpased: 1.385
batch start
#iterations: 239
currently lose_sum: 517.1446333527565
time_elpased: 1.385
start validation test
0.0979381443299
1.0
0.0979381443299
0.178403755869
0
validation finish
batch start
#iterations: 240
currently lose_sum: 518.2134182155132
time_elpased: 1.386
batch start
#iterations: 241
currently lose_sum: 516.8631076216698
time_elpased: 1.382
batch start
#iterations: 242
currently lose_sum: 518.3670912086964
time_elpased: 1.373
batch start
#iterations: 243
currently lose_sum: 516.5679910182953
time_elpased: 1.385
batch start
#iterations: 244
currently lose_sum: 518.1997427642345
time_elpased: 1.379
batch start
#iterations: 245
currently lose_sum: 517.2289769947529
time_elpased: 1.396
batch start
#iterations: 246
currently lose_sum: 517.2287430167198
time_elpased: 1.398
batch start
#iterations: 247
currently lose_sum: 517.8584578335285
time_elpased: 1.393
batch start
#iterations: 248
currently lose_sum: 517.2934396862984
time_elpased: 1.371
batch start
#iterations: 249
currently lose_sum: 518.4211936295033
time_elpased: 1.399
batch start
#iterations: 250
currently lose_sum: 517.2512639164925
time_elpased: 1.391
batch start
#iterations: 251
currently lose_sum: 519.1659519076347
time_elpased: 1.374
batch start
#iterations: 252
currently lose_sum: 520.0720731019974
time_elpased: 1.396
batch start
#iterations: 253
currently lose_sum: 516.9793259501457
time_elpased: 1.394
batch start
#iterations: 254
currently lose_sum: 520.1002288162708
time_elpased: 1.39
batch start
#iterations: 255
currently lose_sum: 517.1355265080929
time_elpased: 1.381
batch start
#iterations: 256
currently lose_sum: 519.7578691840172
time_elpased: 1.382
batch start
#iterations: 257
currently lose_sum: 516.2686934173107
time_elpased: 1.381
batch start
#iterations: 258
currently lose_sum: 516.5868940353394
time_elpased: 1.398
batch start
#iterations: 259
currently lose_sum: 519.1508500874043
time_elpased: 1.388
start validation test
0.165154639175
1.0
0.165154639175
0.28348964785
0
validation finish
batch start
#iterations: 260
currently lose_sum: 519.7536925971508
time_elpased: 1.379
batch start
#iterations: 261
currently lose_sum: 518.7536826431751
time_elpased: 1.383
batch start
#iterations: 262
currently lose_sum: 517.7683991193771
time_elpased: 1.397
batch start
#iterations: 263
currently lose_sum: 517.3837224543095
time_elpased: 1.393
batch start
#iterations: 264
currently lose_sum: 517.116772800684
time_elpased: 1.387
batch start
#iterations: 265
currently lose_sum: 518.9860035479069
time_elpased: 1.388
batch start
#iterations: 266
currently lose_sum: 517.8199679255486
time_elpased: 1.391
batch start
#iterations: 267
currently lose_sum: 516.3717120587826
time_elpased: 1.387
batch start
#iterations: 268
currently lose_sum: 517.1343339085579
time_elpased: 1.382
batch start
#iterations: 269
currently lose_sum: 516.3146245479584
time_elpased: 1.377
batch start
#iterations: 270
currently lose_sum: 518.8518793582916
time_elpased: 1.4
batch start
#iterations: 271
currently lose_sum: 515.74057289958
time_elpased: 1.4
batch start
#iterations: 272
currently lose_sum: 518.0662831068039
time_elpased: 1.395
batch start
#iterations: 273
currently lose_sum: 518.060349136591
time_elpased: 1.368
batch start
#iterations: 274
currently lose_sum: 518.2916206419468
time_elpased: 1.393
batch start
#iterations: 275
currently lose_sum: 517.3585934937
time_elpased: 1.388
batch start
#iterations: 276
currently lose_sum: 516.7775373458862
time_elpased: 1.39
batch start
#iterations: 277
currently lose_sum: 518.0589404702187
time_elpased: 1.388
batch start
#iterations: 278
currently lose_sum: 516.813609033823
time_elpased: 1.388
batch start
#iterations: 279
currently lose_sum: 518.247544080019
time_elpased: 1.382
start validation test
0.19381443299
1.0
0.19381443299
0.32469775475
0
validation finish
batch start
#iterations: 280
currently lose_sum: 516.830886721611
time_elpased: 1.39
batch start
#iterations: 281
currently lose_sum: 516.9834896028042
time_elpased: 1.403
batch start
#iterations: 282
currently lose_sum: 517.4651196300983
time_elpased: 1.393
batch start
#iterations: 283
currently lose_sum: 518.3491321504116
time_elpased: 1.388
batch start
#iterations: 284
currently lose_sum: 516.9468024373055
time_elpased: 1.373
batch start
#iterations: 285
currently lose_sum: 518.7999354600906
time_elpased: 1.392
batch start
#iterations: 286
currently lose_sum: 517.7696222066879
time_elpased: 1.384
batch start
#iterations: 287
currently lose_sum: 519.5200381278992
time_elpased: 1.378
batch start
#iterations: 288
currently lose_sum: 516.6915210485458
time_elpased: 1.397
batch start
#iterations: 289
currently lose_sum: 516.9691149294376
time_elpased: 1.402
batch start
#iterations: 290
currently lose_sum: 515.6237030029297
time_elpased: 1.402
batch start
#iterations: 291
currently lose_sum: 518.3549596369267
time_elpased: 1.39
batch start
#iterations: 292
currently lose_sum: 517.2589990198612
time_elpased: 1.399
batch start
#iterations: 293
currently lose_sum: 518.0993050336838
time_elpased: 1.399
batch start
#iterations: 294
currently lose_sum: 519.2639160752296
time_elpased: 1.397
batch start
#iterations: 295
currently lose_sum: 517.9587077200413
time_elpased: 1.395
batch start
#iterations: 296
currently lose_sum: 517.0033687353134
time_elpased: 1.384
batch start
#iterations: 297
currently lose_sum: 517.3474284112453
time_elpased: 1.389
batch start
#iterations: 298
currently lose_sum: 518.1124113798141
time_elpased: 1.392
batch start
#iterations: 299
currently lose_sum: 518.927443176508
time_elpased: 1.377
start validation test
0.206701030928
1.0
0.206701030928
0.342588637334
0
validation finish
batch start
#iterations: 300
currently lose_sum: 517.8995448350906
time_elpased: 1.385
batch start
#iterations: 301
currently lose_sum: 516.2766795754433
time_elpased: 1.383
batch start
#iterations: 302
currently lose_sum: 518.4936074614525
time_elpased: 1.385
batch start
#iterations: 303
currently lose_sum: 518.50971737504
time_elpased: 1.401
batch start
#iterations: 304
currently lose_sum: 516.367005109787
time_elpased: 1.384
batch start
#iterations: 305
currently lose_sum: 518.9005873501301
time_elpased: 1.4
batch start
#iterations: 306
currently lose_sum: 518.0030282735825
time_elpased: 1.38
batch start
#iterations: 307
currently lose_sum: 519.1626816987991
time_elpased: 1.393
batch start
#iterations: 308
currently lose_sum: 517.8665346503258
time_elpased: 1.38
batch start
#iterations: 309
currently lose_sum: 517.3251166343689
time_elpased: 1.402
batch start
#iterations: 310
currently lose_sum: 515.6700072288513
time_elpased: 1.399
batch start
#iterations: 311
currently lose_sum: 516.8606361448765
time_elpased: 1.383
batch start
#iterations: 312
currently lose_sum: 518.3330662250519
time_elpased: 1.387
batch start
#iterations: 313
currently lose_sum: 518.2784399986267
time_elpased: 1.386
batch start
#iterations: 314
currently lose_sum: 516.4603382050991
time_elpased: 1.381
batch start
#iterations: 315
currently lose_sum: 518.7821145653725
time_elpased: 1.378
batch start
#iterations: 316
currently lose_sum: 518.2735531330109
time_elpased: 1.378
batch start
#iterations: 317
currently lose_sum: 516.7567039728165
time_elpased: 1.391
batch start
#iterations: 318
currently lose_sum: 519.4229541122913
time_elpased: 1.403
batch start
#iterations: 319
currently lose_sum: 516.6953312456608
time_elpased: 1.391
start validation test
0.109175257732
1.0
0.109175257732
0.196858444093
0
validation finish
batch start
#iterations: 320
currently lose_sum: 518.112799346447
time_elpased: 1.41
batch start
#iterations: 321
currently lose_sum: 518.6670053303242
time_elpased: 1.379
batch start
#iterations: 322
currently lose_sum: 516.2128213644028
time_elpased: 1.392
batch start
#iterations: 323
currently lose_sum: 516.5345565974712
time_elpased: 1.389
batch start
#iterations: 324
currently lose_sum: 519.2195666730404
time_elpased: 1.398
batch start
#iterations: 325
currently lose_sum: 517.0268946588039
time_elpased: 1.383
batch start
#iterations: 326
currently lose_sum: 518.2713014781475
time_elpased: 1.398
batch start
#iterations: 327
currently lose_sum: 517.9676246345043
time_elpased: 1.374
batch start
#iterations: 328
currently lose_sum: 517.9768035709858
time_elpased: 1.395
batch start
#iterations: 329
currently lose_sum: 516.3898561894894
time_elpased: 1.375
batch start
#iterations: 330
currently lose_sum: 518.9996039867401
time_elpased: 1.394
batch start
#iterations: 331
currently lose_sum: 519.0899869501591
time_elpased: 1.386
batch start
#iterations: 332
currently lose_sum: 518.4266715049744
time_elpased: 1.394
batch start
#iterations: 333
currently lose_sum: 517.9678645730019
time_elpased: 1.39
batch start
#iterations: 334
currently lose_sum: 516.3965176343918
time_elpased: 1.383
batch start
#iterations: 335
currently lose_sum: 517.154052644968
time_elpased: 1.392
batch start
#iterations: 336
currently lose_sum: 518.5815565288067
time_elpased: 1.381
batch start
#iterations: 337
currently lose_sum: 517.8707233965397
time_elpased: 1.377
batch start
#iterations: 338
currently lose_sum: 515.5869009494781
time_elpased: 1.388
batch start
#iterations: 339
currently lose_sum: 518.8165648281574
time_elpased: 1.386
start validation test
0.161443298969
1.0
0.161443298969
0.278004615658
0
validation finish
batch start
#iterations: 340
currently lose_sum: 515.9051319360733
time_elpased: 1.397
batch start
#iterations: 341
currently lose_sum: 517.3166823685169
time_elpased: 1.387
batch start
#iterations: 342
currently lose_sum: 519.1617856025696
time_elpased: 1.394
batch start
#iterations: 343
currently lose_sum: 518.8296688497066
time_elpased: 1.377
batch start
#iterations: 344
currently lose_sum: 519.3992942273617
time_elpased: 1.389
batch start
#iterations: 345
currently lose_sum: 518.8944023251534
time_elpased: 1.386
batch start
#iterations: 346
currently lose_sum: 517.5562342405319
time_elpased: 1.407
batch start
#iterations: 347
currently lose_sum: 517.7611431479454
time_elpased: 1.388
batch start
#iterations: 348
currently lose_sum: 518.2516984343529
time_elpased: 1.386
batch start
#iterations: 349
currently lose_sum: 519.1281482875347
time_elpased: 1.392
batch start
#iterations: 350
currently lose_sum: 518.1659670174122
time_elpased: 1.388
batch start
#iterations: 351
currently lose_sum: 517.8644942343235
time_elpased: 1.386
batch start
#iterations: 352
currently lose_sum: 517.8028848171234
time_elpased: 1.385
batch start
#iterations: 353
currently lose_sum: 518.9833819270134
time_elpased: 1.397
batch start
#iterations: 354
currently lose_sum: 517.8987808525562
time_elpased: 1.378
batch start
#iterations: 355
currently lose_sum: 517.6548515558243
time_elpased: 1.379
batch start
#iterations: 356
currently lose_sum: 516.2903238832951
time_elpased: 1.383
batch start
#iterations: 357
currently lose_sum: 516.2399734854698
time_elpased: 1.382
batch start
#iterations: 358
currently lose_sum: 518.5883828699589
time_elpased: 1.381
batch start
#iterations: 359
currently lose_sum: 516.4792380928993
time_elpased: 1.387
start validation test
0.179175257732
1.0
0.179175257732
0.303899283091
0
validation finish
batch start
#iterations: 360
currently lose_sum: 516.9943390786648
time_elpased: 1.401
batch start
#iterations: 361
currently lose_sum: 516.1823092103004
time_elpased: 1.378
batch start
#iterations: 362
currently lose_sum: 517.4560443758965
time_elpased: 1.384
batch start
#iterations: 363
currently lose_sum: 517.5924719274044
time_elpased: 1.368
batch start
#iterations: 364
currently lose_sum: 518.1190515756607
time_elpased: 1.391
batch start
#iterations: 365
currently lose_sum: 517.2504833042622
time_elpased: 1.401
batch start
#iterations: 366
currently lose_sum: 517.994861394167
time_elpased: 1.382
batch start
#iterations: 367
currently lose_sum: 517.391125112772
time_elpased: 1.384
batch start
#iterations: 368
currently lose_sum: 516.0004163384438
time_elpased: 1.393
batch start
#iterations: 369
currently lose_sum: 516.17426648736
time_elpased: 1.378
batch start
#iterations: 370
currently lose_sum: 515.7000467181206
time_elpased: 1.378
batch start
#iterations: 371
currently lose_sum: 517.4072291254997
time_elpased: 1.378
batch start
#iterations: 372
currently lose_sum: 516.4830811917782
time_elpased: 1.395
batch start
#iterations: 373
currently lose_sum: 517.214311093092
time_elpased: 1.384
batch start
#iterations: 374
currently lose_sum: 518.4064828455448
time_elpased: 1.378
batch start
#iterations: 375
currently lose_sum: 518.6265309751034
time_elpased: 1.39
batch start
#iterations: 376
currently lose_sum: 517.5081653594971
time_elpased: 1.376
batch start
#iterations: 377
currently lose_sum: 516.4100341200829
time_elpased: 1.386
batch start
#iterations: 378
currently lose_sum: 518.8780308365822
time_elpased: 1.394
batch start
#iterations: 379
currently lose_sum: 517.2811763584614
time_elpased: 1.374
start validation test
0.186804123711
1.0
0.186804123711
0.314801945796
0
validation finish
batch start
#iterations: 380
currently lose_sum: 517.5171002447605
time_elpased: 1.397
batch start
#iterations: 381
currently lose_sum: 516.185300976038
time_elpased: 1.391
batch start
#iterations: 382
currently lose_sum: 517.4144316315651
time_elpased: 1.39
batch start
#iterations: 383
currently lose_sum: 515.0450308322906
time_elpased: 1.38
batch start
#iterations: 384
currently lose_sum: 515.1840065419674
time_elpased: 1.397
batch start
#iterations: 385
currently lose_sum: 516.2587404251099
time_elpased: 1.383
batch start
#iterations: 386
currently lose_sum: 517.2959315180779
time_elpased: 1.377
batch start
#iterations: 387
currently lose_sum: 516.0545673370361
time_elpased: 1.399
batch start
#iterations: 388
currently lose_sum: 516.8367124795914
time_elpased: 1.378
batch start
#iterations: 389
currently lose_sum: 518.6700324416161
time_elpased: 1.387
batch start
#iterations: 390
currently lose_sum: 516.636680483818
time_elpased: 1.385
batch start
#iterations: 391
currently lose_sum: 517.7189465463161
time_elpased: 1.385
batch start
#iterations: 392
currently lose_sum: 516.8619744479656
time_elpased: 1.392
batch start
#iterations: 393
currently lose_sum: 516.1471793353558
time_elpased: 1.392
batch start
#iterations: 394
currently lose_sum: 516.0100889801979
time_elpased: 1.381
batch start
#iterations: 395
currently lose_sum: 516.5782939195633
time_elpased: 1.38
batch start
#iterations: 396
currently lose_sum: 516.6677644252777
time_elpased: 1.391
batch start
#iterations: 397
currently lose_sum: 518.1419313251972
time_elpased: 1.377
batch start
#iterations: 398
currently lose_sum: 516.4531888067722
time_elpased: 1.39
batch start
#iterations: 399
currently lose_sum: 516.2881316840649
time_elpased: 1.392
start validation test
0.158865979381
1.0
0.158865979381
0.274174895472
0
validation finish
acc: 0.241
pre: 1.000
rec: 0.241
F1: 0.389
auc: 0.000
