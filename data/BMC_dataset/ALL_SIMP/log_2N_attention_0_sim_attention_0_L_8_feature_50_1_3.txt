start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 556.0285651683807
time_elpased: 1.396
batch start
#iterations: 1
currently lose_sum: 555.5743078589439
time_elpased: 1.385
batch start
#iterations: 2
currently lose_sum: 554.9701272249222
time_elpased: 1.384
batch start
#iterations: 3
currently lose_sum: 554.9174642562866
time_elpased: 1.376
batch start
#iterations: 4
currently lose_sum: 550.7059562206268
time_elpased: 1.381
batch start
#iterations: 5
currently lose_sum: 550.7736509442329
time_elpased: 1.378
batch start
#iterations: 6
currently lose_sum: 550.3816777467728
time_elpased: 1.367
batch start
#iterations: 7
currently lose_sum: 548.9571309685707
time_elpased: 1.388
batch start
#iterations: 8
currently lose_sum: 550.024195432663
time_elpased: 1.379
batch start
#iterations: 9
currently lose_sum: 547.0010335445404
time_elpased: 1.378
batch start
#iterations: 10
currently lose_sum: 546.8905752897263
time_elpased: 1.381
batch start
#iterations: 11
currently lose_sum: 547.5504199266434
time_elpased: 1.366
batch start
#iterations: 12
currently lose_sum: 548.303463190794
time_elpased: 1.37
batch start
#iterations: 13
currently lose_sum: 544.529030919075
time_elpased: 1.373
batch start
#iterations: 14
currently lose_sum: 545.7851182222366
time_elpased: 1.378
batch start
#iterations: 15
currently lose_sum: 543.2159588932991
time_elpased: 1.383
batch start
#iterations: 16
currently lose_sum: 542.619288533926
time_elpased: 1.368
batch start
#iterations: 17
currently lose_sum: 542.793907046318
time_elpased: 1.391
batch start
#iterations: 18
currently lose_sum: 541.6898362636566
time_elpased: 1.37
batch start
#iterations: 19
currently lose_sum: 540.2343292534351
time_elpased: 1.395
start validation test
0.0874226804124
1.0
0.0874226804124
0.160788775123
0
validation finish
batch start
#iterations: 20
currently lose_sum: 540.9639523625374
time_elpased: 1.388
batch start
#iterations: 21
currently lose_sum: 538.1843907833099
time_elpased: 1.377
batch start
#iterations: 22
currently lose_sum: 539.0577233433723
time_elpased: 1.371
batch start
#iterations: 23
currently lose_sum: 537.3282104432583
time_elpased: 1.381
batch start
#iterations: 24
currently lose_sum: 537.1397099494934
time_elpased: 1.373
batch start
#iterations: 25
currently lose_sum: 535.6309938132763
time_elpased: 1.376
batch start
#iterations: 26
currently lose_sum: 535.1839910149574
time_elpased: 1.381
batch start
#iterations: 27
currently lose_sum: 535.1662746667862
time_elpased: 1.393
batch start
#iterations: 28
currently lose_sum: 533.2133612632751
time_elpased: 1.373
batch start
#iterations: 29
currently lose_sum: 532.8112041056156
time_elpased: 1.383
batch start
#iterations: 30
currently lose_sum: 532.4384700059891
time_elpased: 1.388
batch start
#iterations: 31
currently lose_sum: 532.8532821834087
time_elpased: 1.379
batch start
#iterations: 32
currently lose_sum: 533.1098448336124
time_elpased: 1.377
batch start
#iterations: 33
currently lose_sum: 531.9929438233376
time_elpased: 1.376
batch start
#iterations: 34
currently lose_sum: 530.2181708216667
time_elpased: 1.378
batch start
#iterations: 35
currently lose_sum: 532.1399078667164
time_elpased: 1.373
batch start
#iterations: 36
currently lose_sum: 530.958701133728
time_elpased: 1.378
batch start
#iterations: 37
currently lose_sum: 530.2208772599697
time_elpased: 1.365
batch start
#iterations: 38
currently lose_sum: 530.304995059967
time_elpased: 1.374
batch start
#iterations: 39
currently lose_sum: 530.9231006801128
time_elpased: 1.374
start validation test
0.199175257732
1.0
0.199175257732
0.332187070151
0
validation finish
batch start
#iterations: 40
currently lose_sum: 530.6667020618916
time_elpased: 1.383
batch start
#iterations: 41
currently lose_sum: 530.9752058386803
time_elpased: 1.371
batch start
#iterations: 42
currently lose_sum: 529.5274726748466
time_elpased: 1.379
batch start
#iterations: 43
currently lose_sum: 529.5463006198406
time_elpased: 1.37
batch start
#iterations: 44
currently lose_sum: 526.5533322691917
time_elpased: 1.375
batch start
#iterations: 45
currently lose_sum: 527.3355147838593
time_elpased: 1.373
batch start
#iterations: 46
currently lose_sum: 530.7236261963844
time_elpased: 1.38
batch start
#iterations: 47
currently lose_sum: 527.5354809165001
time_elpased: 1.382
batch start
#iterations: 48
currently lose_sum: 525.6094450056553
time_elpased: 1.379
batch start
#iterations: 49
currently lose_sum: 526.7795739471912
time_elpased: 1.39
batch start
#iterations: 50
currently lose_sum: 526.622400611639
time_elpased: 1.385
batch start
#iterations: 51
currently lose_sum: 528.071645885706
time_elpased: 1.405
batch start
#iterations: 52
currently lose_sum: 528.865453004837
time_elpased: 1.366
batch start
#iterations: 53
currently lose_sum: 526.2645760774612
time_elpased: 1.382
batch start
#iterations: 54
currently lose_sum: 526.2544260025024
time_elpased: 1.386
batch start
#iterations: 55
currently lose_sum: 525.0511804819107
time_elpased: 1.382
batch start
#iterations: 56
currently lose_sum: 525.0528458952904
time_elpased: 1.379
batch start
#iterations: 57
currently lose_sum: 524.3597946465015
time_elpased: 1.373
batch start
#iterations: 58
currently lose_sum: 526.2226686775684
time_elpased: 1.387
batch start
#iterations: 59
currently lose_sum: 524.7842574417591
time_elpased: 1.383
start validation test
0.10412371134
1.0
0.10412371134
0.188608776844
0
validation finish
batch start
#iterations: 60
currently lose_sum: 526.4894383251667
time_elpased: 1.386
batch start
#iterations: 61
currently lose_sum: 523.9808975756168
time_elpased: 1.371
batch start
#iterations: 62
currently lose_sum: 524.3751935958862
time_elpased: 1.381
batch start
#iterations: 63
currently lose_sum: 527.5474565923214
time_elpased: 1.372
batch start
#iterations: 64
currently lose_sum: 524.57728561759
time_elpased: 1.374
batch start
#iterations: 65
currently lose_sum: 525.7544359564781
time_elpased: 1.376
batch start
#iterations: 66
currently lose_sum: 523.577618420124
time_elpased: 1.371
batch start
#iterations: 67
currently lose_sum: 522.3559446334839
time_elpased: 1.398
batch start
#iterations: 68
currently lose_sum: 522.5671416819096
time_elpased: 1.383
batch start
#iterations: 69
currently lose_sum: 523.0140518844128
time_elpased: 1.374
batch start
#iterations: 70
currently lose_sum: 524.8437761366367
time_elpased: 1.381
batch start
#iterations: 71
currently lose_sum: 523.8597783744335
time_elpased: 1.379
batch start
#iterations: 72
currently lose_sum: 521.4277261197567
time_elpased: 1.381
batch start
#iterations: 73
currently lose_sum: 524.092436760664
time_elpased: 1.393
batch start
#iterations: 74
currently lose_sum: 524.1223433315754
time_elpased: 1.378
batch start
#iterations: 75
currently lose_sum: 523.11825761199
time_elpased: 1.38
batch start
#iterations: 76
currently lose_sum: 523.0063157975674
time_elpased: 1.381
batch start
#iterations: 77
currently lose_sum: 523.2461412549019
time_elpased: 1.374
batch start
#iterations: 78
currently lose_sum: 521.6990876197815
time_elpased: 1.376
batch start
#iterations: 79
currently lose_sum: 523.5929981172085
time_elpased: 1.367
start validation test
0.259690721649
1.0
0.259690721649
0.412308699566
0
validation finish
batch start
#iterations: 80
currently lose_sum: 522.5547354221344
time_elpased: 1.379
batch start
#iterations: 81
currently lose_sum: 523.92817273736
time_elpased: 1.375
batch start
#iterations: 82
currently lose_sum: 523.6367533802986
time_elpased: 1.378
batch start
#iterations: 83
currently lose_sum: 520.1165277659893
time_elpased: 1.383
batch start
#iterations: 84
currently lose_sum: 521.3621384203434
time_elpased: 1.384
batch start
#iterations: 85
currently lose_sum: 524.231469810009
time_elpased: 1.388
batch start
#iterations: 86
currently lose_sum: 523.1351574063301
time_elpased: 1.378
batch start
#iterations: 87
currently lose_sum: 524.0304512679577
time_elpased: 1.377
batch start
#iterations: 88
currently lose_sum: 522.5194741487503
time_elpased: 1.38
batch start
#iterations: 89
currently lose_sum: 521.1422770619392
time_elpased: 1.392
batch start
#iterations: 90
currently lose_sum: 522.070393294096
time_elpased: 1.371
batch start
#iterations: 91
currently lose_sum: 520.2280949652195
time_elpased: 1.384
batch start
#iterations: 92
currently lose_sum: 520.49886110425
time_elpased: 1.392
batch start
#iterations: 93
currently lose_sum: 519.838886141777
time_elpased: 1.388
batch start
#iterations: 94
currently lose_sum: 522.3250056803226
time_elpased: 1.387
batch start
#iterations: 95
currently lose_sum: 520.3411381244659
time_elpased: 1.372
batch start
#iterations: 96
currently lose_sum: 522.4262247979641
time_elpased: 1.387
batch start
#iterations: 97
currently lose_sum: 523.5021376311779
time_elpased: 1.382
batch start
#iterations: 98
currently lose_sum: 519.075140029192
time_elpased: 1.385
batch start
#iterations: 99
currently lose_sum: 521.4527153372765
time_elpased: 1.38
start validation test
0.157113402062
1.0
0.157113402062
0.271560940841
0
validation finish
batch start
#iterations: 100
currently lose_sum: 521.1582652926445
time_elpased: 1.385
batch start
#iterations: 101
currently lose_sum: 521.1629683077335
time_elpased: 1.383
batch start
#iterations: 102
currently lose_sum: 522.3555288016796
time_elpased: 1.382
batch start
#iterations: 103
currently lose_sum: 519.0783505737782
time_elpased: 1.379
batch start
#iterations: 104
currently lose_sum: 519.5064389705658
time_elpased: 1.403
batch start
#iterations: 105
currently lose_sum: 519.9550182521343
time_elpased: 1.376
batch start
#iterations: 106
currently lose_sum: 521.6258282363415
time_elpased: 1.379
batch start
#iterations: 107
currently lose_sum: 521.2163803875446
time_elpased: 1.387
batch start
#iterations: 108
currently lose_sum: 519.4027697741985
time_elpased: 1.381
batch start
#iterations: 109
currently lose_sum: 522.3764963150024
time_elpased: 1.392
batch start
#iterations: 110
currently lose_sum: 520.290716946125
time_elpased: 1.376
batch start
#iterations: 111
currently lose_sum: 522.1853383183479
time_elpased: 1.39
batch start
#iterations: 112
currently lose_sum: 519.2461365759373
time_elpased: 1.379
batch start
#iterations: 113
currently lose_sum: 520.7459409832954
time_elpased: 1.406
batch start
#iterations: 114
currently lose_sum: 521.634997099638
time_elpased: 1.394
batch start
#iterations: 115
currently lose_sum: 521.4243505001068
time_elpased: 1.379
batch start
#iterations: 116
currently lose_sum: 520.8305326998234
time_elpased: 1.378
batch start
#iterations: 117
currently lose_sum: 520.3195567727089
time_elpased: 1.375
batch start
#iterations: 118
currently lose_sum: 520.8409705460072
time_elpased: 1.386
batch start
#iterations: 119
currently lose_sum: 518.3633800446987
time_elpased: 1.375
start validation test
0.208453608247
1.0
0.208453608247
0.344992322129
0
validation finish
batch start
#iterations: 120
currently lose_sum: 519.7445635199547
time_elpased: 1.374
batch start
#iterations: 121
currently lose_sum: 520.7994456589222
time_elpased: 1.372
batch start
#iterations: 122
currently lose_sum: 522.272678732872
time_elpased: 1.386
batch start
#iterations: 123
currently lose_sum: 523.269101202488
time_elpased: 1.385
batch start
#iterations: 124
currently lose_sum: 519.702126711607
time_elpased: 1.366
batch start
#iterations: 125
currently lose_sum: 517.955752313137
time_elpased: 1.373
batch start
#iterations: 126
currently lose_sum: 517.7202263176441
time_elpased: 1.374
batch start
#iterations: 127
currently lose_sum: 519.7500431537628
time_elpased: 1.395
batch start
#iterations: 128
currently lose_sum: 520.971148699522
time_elpased: 1.368
batch start
#iterations: 129
currently lose_sum: 519.3496232032776
time_elpased: 1.399
batch start
#iterations: 130
currently lose_sum: 518.6430252194405
time_elpased: 1.382
batch start
#iterations: 131
currently lose_sum: 519.7368736565113
time_elpased: 1.388
batch start
#iterations: 132
currently lose_sum: 518.806905657053
time_elpased: 1.393
batch start
#iterations: 133
currently lose_sum: 519.030820608139
time_elpased: 1.383
batch start
#iterations: 134
currently lose_sum: 518.6410253047943
time_elpased: 1.38
batch start
#iterations: 135
currently lose_sum: 518.3273497521877
time_elpased: 1.377
batch start
#iterations: 136
currently lose_sum: 520.8136429190636
time_elpased: 1.363
batch start
#iterations: 137
currently lose_sum: 519.5878848731518
time_elpased: 1.383
batch start
#iterations: 138
currently lose_sum: 521.1312375664711
time_elpased: 1.384
batch start
#iterations: 139
currently lose_sum: 519.7744300961494
time_elpased: 1.382
start validation test
0.165773195876
1.0
0.165773195876
0.284400424478
0
validation finish
batch start
#iterations: 140
currently lose_sum: 518.7481575906277
time_elpased: 1.381
batch start
#iterations: 141
currently lose_sum: 517.9524307549
time_elpased: 1.38
batch start
#iterations: 142
currently lose_sum: 519.5809834897518
time_elpased: 1.374
batch start
#iterations: 143
currently lose_sum: 518.8326037228107
time_elpased: 1.383
batch start
#iterations: 144
currently lose_sum: 519.5419321358204
time_elpased: 1.373
batch start
#iterations: 145
currently lose_sum: 519.6833398640156
time_elpased: 1.392
batch start
#iterations: 146
currently lose_sum: 519.5499981343746
time_elpased: 1.385
batch start
#iterations: 147
currently lose_sum: 518.8067518174648
time_elpased: 1.396
batch start
#iterations: 148
currently lose_sum: 518.3776834905148
time_elpased: 1.37
batch start
#iterations: 149
currently lose_sum: 520.2545581161976
time_elpased: 1.374
batch start
#iterations: 150
currently lose_sum: 519.5194543898106
time_elpased: 1.394
batch start
#iterations: 151
currently lose_sum: 517.5848905742168
time_elpased: 1.368
batch start
#iterations: 152
currently lose_sum: 518.6831781864166
time_elpased: 1.388
batch start
#iterations: 153
currently lose_sum: 515.9480000734329
time_elpased: 1.381
batch start
#iterations: 154
currently lose_sum: 520.4354635775089
time_elpased: 1.392
batch start
#iterations: 155
currently lose_sum: 520.3284430205822
time_elpased: 1.383
batch start
#iterations: 156
currently lose_sum: 519.1705056726933
time_elpased: 1.392
batch start
#iterations: 157
currently lose_sum: 518.498163163662
time_elpased: 1.378
batch start
#iterations: 158
currently lose_sum: 519.4356888532639
time_elpased: 1.388
batch start
#iterations: 159
currently lose_sum: 517.8271478712559
time_elpased: 1.375
start validation test
0.125360824742
1.0
0.125360824742
0.222792231587
0
validation finish
batch start
#iterations: 160
currently lose_sum: 518.7926022708416
time_elpased: 1.369
batch start
#iterations: 161
currently lose_sum: 519.2608494460583
time_elpased: 1.372
batch start
#iterations: 162
currently lose_sum: 517.5449420511723
time_elpased: 1.389
batch start
#iterations: 163
currently lose_sum: 518.9008559286594
time_elpased: 1.376
batch start
#iterations: 164
currently lose_sum: 518.7980917692184
time_elpased: 1.372
batch start
#iterations: 165
currently lose_sum: 517.9828333854675
time_elpased: 1.368
batch start
#iterations: 166
currently lose_sum: 517.6967920064926
time_elpased: 1.378
batch start
#iterations: 167
currently lose_sum: 518.2062598764896
time_elpased: 1.372
batch start
#iterations: 168
currently lose_sum: 518.9652188122272
time_elpased: 1.376
batch start
#iterations: 169
currently lose_sum: 518.29072868824
time_elpased: 1.396
batch start
#iterations: 170
currently lose_sum: 518.6707063317299
time_elpased: 1.379
batch start
#iterations: 171
currently lose_sum: 518.4972368180752
time_elpased: 1.375
batch start
#iterations: 172
currently lose_sum: 517.4814061522484
time_elpased: 1.371
batch start
#iterations: 173
currently lose_sum: 516.5985307693481
time_elpased: 1.399
batch start
#iterations: 174
currently lose_sum: 518.8241373300552
time_elpased: 1.367
batch start
#iterations: 175
currently lose_sum: 516.6579574048519
time_elpased: 1.375
batch start
#iterations: 176
currently lose_sum: 518.4286864995956
time_elpased: 1.381
batch start
#iterations: 177
currently lose_sum: 518.8035407662392
time_elpased: 1.38
batch start
#iterations: 178
currently lose_sum: 518.1027558743954
time_elpased: 1.38
batch start
#iterations: 179
currently lose_sum: 520.9573456645012
time_elpased: 1.383
start validation test
0.198865979381
1.0
0.198865979381
0.331756814859
0
validation finish
batch start
#iterations: 180
currently lose_sum: 518.2643163204193
time_elpased: 1.381
batch start
#iterations: 181
currently lose_sum: 519.5397371351719
time_elpased: 1.376
batch start
#iterations: 182
currently lose_sum: 517.8676552772522
time_elpased: 1.398
batch start
#iterations: 183
currently lose_sum: 517.9682441055775
time_elpased: 1.37
batch start
#iterations: 184
currently lose_sum: 517.8120563626289
time_elpased: 1.377
batch start
#iterations: 185
currently lose_sum: 518.1331100165844
time_elpased: 1.384
batch start
#iterations: 186
currently lose_sum: 516.9241605103016
time_elpased: 1.38
batch start
#iterations: 187
currently lose_sum: 520.2101627886295
time_elpased: 1.384
batch start
#iterations: 188
currently lose_sum: 518.4579022824764
time_elpased: 1.383
batch start
#iterations: 189
currently lose_sum: 518.9854013621807
time_elpased: 1.373
batch start
#iterations: 190
currently lose_sum: 518.5652842223644
time_elpased: 1.385
batch start
#iterations: 191
currently lose_sum: 518.4530085623264
time_elpased: 1.377
batch start
#iterations: 192
currently lose_sum: 517.1260614693165
time_elpased: 1.378
batch start
#iterations: 193
currently lose_sum: 517.5439651012421
time_elpased: 1.368
batch start
#iterations: 194
currently lose_sum: 519.0361594557762
time_elpased: 1.373
batch start
#iterations: 195
currently lose_sum: 516.7155675292015
time_elpased: 1.38
batch start
#iterations: 196
currently lose_sum: 519.4182071089745
time_elpased: 1.374
batch start
#iterations: 197
currently lose_sum: 518.7799755632877
time_elpased: 1.378
batch start
#iterations: 198
currently lose_sum: 517.9474051892757
time_elpased: 1.384
batch start
#iterations: 199
currently lose_sum: 520.0704893767834
time_elpased: 1.393
start validation test
0.188659793814
1.0
0.188659793814
0.317432784042
0
validation finish
batch start
#iterations: 200
currently lose_sum: 514.5932209193707
time_elpased: 1.388
batch start
#iterations: 201
currently lose_sum: 518.4820516109467
time_elpased: 1.385
batch start
#iterations: 202
currently lose_sum: 515.9458268582821
time_elpased: 1.369
batch start
#iterations: 203
currently lose_sum: 518.2425487339497
time_elpased: 1.375
batch start
#iterations: 204
currently lose_sum: 518.4209527373314
time_elpased: 1.376
batch start
#iterations: 205
currently lose_sum: 516.9428696334362
time_elpased: 1.397
batch start
#iterations: 206
currently lose_sum: 514.9571804404259
time_elpased: 1.378
batch start
#iterations: 207
currently lose_sum: 516.3127611875534
time_elpased: 1.38
batch start
#iterations: 208
currently lose_sum: 517.906264513731
time_elpased: 1.38
batch start
#iterations: 209
currently lose_sum: 518.33408421278
time_elpased: 1.396
batch start
#iterations: 210
currently lose_sum: 517.5012359619141
time_elpased: 1.388
batch start
#iterations: 211
currently lose_sum: 517.4121860861778
time_elpased: 1.387
batch start
#iterations: 212
currently lose_sum: 517.0483994185925
time_elpased: 1.382
batch start
#iterations: 213
currently lose_sum: 519.3019243180752
time_elpased: 1.387
batch start
#iterations: 214
currently lose_sum: 519.3729716837406
time_elpased: 1.381
batch start
#iterations: 215
currently lose_sum: 517.7506326436996
time_elpased: 1.408
batch start
#iterations: 216
currently lose_sum: 518.719047665596
time_elpased: 1.377
batch start
#iterations: 217
currently lose_sum: 516.9449731111526
time_elpased: 1.378
batch start
#iterations: 218
currently lose_sum: 518.7864154875278
time_elpased: 1.386
batch start
#iterations: 219
currently lose_sum: 517.6758915781975
time_elpased: 1.386
start validation test
0.111340206186
1.0
0.111340206186
0.200371057514
0
validation finish
batch start
#iterations: 220
currently lose_sum: 517.968716442585
time_elpased: 1.388
batch start
#iterations: 221
currently lose_sum: 519.1333223879337
time_elpased: 1.378
batch start
#iterations: 222
currently lose_sum: 517.3939799666405
time_elpased: 1.379
batch start
#iterations: 223
currently lose_sum: 514.1265726089478
time_elpased: 1.379
batch start
#iterations: 224
currently lose_sum: 519.6648045778275
time_elpased: 1.369
batch start
#iterations: 225
currently lose_sum: 517.7139806747437
time_elpased: 1.381
batch start
#iterations: 226
currently lose_sum: 518.6145873367786
time_elpased: 1.382
batch start
#iterations: 227
currently lose_sum: 515.3140628933907
time_elpased: 1.385
batch start
#iterations: 228
currently lose_sum: 517.0450225174427
time_elpased: 1.377
batch start
#iterations: 229
currently lose_sum: 518.2899481356144
time_elpased: 1.386
batch start
#iterations: 230
currently lose_sum: 517.552803426981
time_elpased: 1.383
batch start
#iterations: 231
currently lose_sum: 521.5606434941292
time_elpased: 1.382
batch start
#iterations: 232
currently lose_sum: 518.0040085911751
time_elpased: 1.378
batch start
#iterations: 233
currently lose_sum: 517.2672110795975
time_elpased: 1.38
batch start
#iterations: 234
currently lose_sum: 517.1145531535149
time_elpased: 1.384
batch start
#iterations: 235
currently lose_sum: 517.9067495763302
time_elpased: 1.376
batch start
#iterations: 236
currently lose_sum: 516.1492462158203
time_elpased: 1.39
batch start
#iterations: 237
currently lose_sum: 516.4313252270222
time_elpased: 1.381
batch start
#iterations: 238
currently lose_sum: 515.9761886298656
time_elpased: 1.378
batch start
#iterations: 239
currently lose_sum: 520.8647817075253
time_elpased: 1.37
start validation test
0.196701030928
1.0
0.196701030928
0.328738800827
0
validation finish
batch start
#iterations: 240
currently lose_sum: 517.9964492022991
time_elpased: 1.377
batch start
#iterations: 241
currently lose_sum: 517.8616701960564
time_elpased: 1.381
batch start
#iterations: 242
currently lose_sum: 517.2295370101929
time_elpased: 1.373
batch start
#iterations: 243
currently lose_sum: 516.9585488140583
time_elpased: 1.395
batch start
#iterations: 244
currently lose_sum: 519.3945435583591
time_elpased: 1.378
batch start
#iterations: 245
currently lose_sum: 516.8529710173607
time_elpased: 1.374
batch start
#iterations: 246
currently lose_sum: 517.706248998642
time_elpased: 1.374
batch start
#iterations: 247
currently lose_sum: 518.6156696379185
time_elpased: 1.387
batch start
#iterations: 248
currently lose_sum: 515.5523774921894
time_elpased: 1.382
batch start
#iterations: 249
currently lose_sum: 518.175449937582
time_elpased: 1.39
batch start
#iterations: 250
currently lose_sum: 517.9180704951286
time_elpased: 1.372
batch start
#iterations: 251
currently lose_sum: 516.768507540226
time_elpased: 1.391
batch start
#iterations: 252
currently lose_sum: 517.3223987221718
time_elpased: 1.374
batch start
#iterations: 253
currently lose_sum: 516.5423775315285
time_elpased: 1.371
batch start
#iterations: 254
currently lose_sum: 518.9065158963203
time_elpased: 1.388
batch start
#iterations: 255
currently lose_sum: 516.7028354406357
time_elpased: 1.373
batch start
#iterations: 256
currently lose_sum: 517.723120868206
time_elpased: 1.367
batch start
#iterations: 257
currently lose_sum: 515.6564504206181
time_elpased: 1.383
batch start
#iterations: 258
currently lose_sum: 515.2591126859188
time_elpased: 1.38
batch start
#iterations: 259
currently lose_sum: 517.6596453785896
time_elpased: 1.379
start validation test
0.184639175258
1.0
0.184639175258
0.311722217388
0
validation finish
batch start
#iterations: 260
currently lose_sum: 517.0929846167564
time_elpased: 1.37
batch start
#iterations: 261
currently lose_sum: 516.9068447053432
time_elpased: 1.401
batch start
#iterations: 262
currently lose_sum: 517.6340579390526
time_elpased: 1.377
batch start
#iterations: 263
currently lose_sum: 517.4069934487343
time_elpased: 1.375
batch start
#iterations: 264
currently lose_sum: 517.98597240448
time_elpased: 1.388
batch start
#iterations: 265
currently lose_sum: 518.551275074482
time_elpased: 1.377
batch start
#iterations: 266
currently lose_sum: 517.6992616057396
time_elpased: 1.377
batch start
#iterations: 267
currently lose_sum: 517.1411437392235
time_elpased: 1.379
batch start
#iterations: 268
currently lose_sum: 514.5617997646332
time_elpased: 1.387
batch start
#iterations: 269
currently lose_sum: 515.7114299237728
time_elpased: 1.377
batch start
#iterations: 270
currently lose_sum: 517.8475851416588
time_elpased: 1.396
batch start
#iterations: 271
currently lose_sum: 519.1101944744587
time_elpased: 1.375
batch start
#iterations: 272
currently lose_sum: 516.7994930446148
time_elpased: 1.371
batch start
#iterations: 273
currently lose_sum: 517.8840823173523
time_elpased: 1.375
batch start
#iterations: 274
currently lose_sum: 516.9642136991024
time_elpased: 1.372
batch start
#iterations: 275
currently lose_sum: 516.1413061916828
time_elpased: 1.374
batch start
#iterations: 276
currently lose_sum: 518.3064993917942
time_elpased: 1.38
batch start
#iterations: 277
currently lose_sum: 516.2684887051582
time_elpased: 1.381
batch start
#iterations: 278
currently lose_sum: 514.9099713265896
time_elpased: 1.38
batch start
#iterations: 279
currently lose_sum: 515.1593247950077
time_elpased: 1.377
start validation test
0.20412371134
1.0
0.20412371134
0.33904109589
0
validation finish
batch start
#iterations: 280
currently lose_sum: 518.355782777071
time_elpased: 1.376
batch start
#iterations: 281
currently lose_sum: 516.4242207407951
time_elpased: 1.39
batch start
#iterations: 282
currently lose_sum: 518.4806012809277
time_elpased: 1.381
batch start
#iterations: 283
currently lose_sum: 516.6386265158653
time_elpased: 1.38
batch start
#iterations: 284
currently lose_sum: 516.1131948232651
time_elpased: 1.384
batch start
#iterations: 285
currently lose_sum: 515.4852700829506
time_elpased: 1.381
batch start
#iterations: 286
currently lose_sum: 516.7448496818542
time_elpased: 1.386
batch start
#iterations: 287
currently lose_sum: 517.6171745061874
time_elpased: 1.38
batch start
#iterations: 288
currently lose_sum: 519.0411300361156
time_elpased: 1.382
batch start
#iterations: 289
currently lose_sum: 518.704503506422
time_elpased: 1.383
batch start
#iterations: 290
currently lose_sum: 515.4542120397091
time_elpased: 1.389
batch start
#iterations: 291
currently lose_sum: 515.1272309720516
time_elpased: 1.375
batch start
#iterations: 292
currently lose_sum: 516.4385040700436
time_elpased: 1.41
batch start
#iterations: 293
currently lose_sum: 518.0486489832401
time_elpased: 1.376
batch start
#iterations: 294
currently lose_sum: 516.0957064032555
time_elpased: 1.382
batch start
#iterations: 295
currently lose_sum: 515.7055110931396
time_elpased: 1.388
batch start
#iterations: 296
currently lose_sum: 517.2020098567009
time_elpased: 1.388
batch start
#iterations: 297
currently lose_sum: 515.8279950022697
time_elpased: 1.375
batch start
#iterations: 298
currently lose_sum: 516.79365670681
time_elpased: 1.382
batch start
#iterations: 299
currently lose_sum: 518.2620092332363
time_elpased: 1.378
start validation test
0.161546391753
1.0
0.161546391753
0.278157450963
0
validation finish
batch start
#iterations: 300
currently lose_sum: 517.142242193222
time_elpased: 1.38
batch start
#iterations: 301
currently lose_sum: 517.0506157875061
time_elpased: 1.388
batch start
#iterations: 302
currently lose_sum: 518.0103107988834
time_elpased: 1.388
batch start
#iterations: 303
currently lose_sum: 516.7325299978256
time_elpased: 1.379
batch start
#iterations: 304
currently lose_sum: 514.9575456380844
time_elpased: 1.38
batch start
#iterations: 305
currently lose_sum: 516.2561421990395
time_elpased: 1.378
batch start
#iterations: 306
currently lose_sum: 517.1808501780033
time_elpased: 1.377
batch start
#iterations: 307
currently lose_sum: 515.4727552235126
time_elpased: 1.386
batch start
#iterations: 308
currently lose_sum: 517.2284917533398
time_elpased: 1.379
batch start
#iterations: 309
currently lose_sum: 516.5142683386803
time_elpased: 1.381
batch start
#iterations: 310
currently lose_sum: 518.5378413796425
time_elpased: 1.379
batch start
#iterations: 311
currently lose_sum: 515.7468967735767
time_elpased: 1.366
batch start
#iterations: 312
currently lose_sum: 514.1217011213303
time_elpased: 1.377
batch start
#iterations: 313
currently lose_sum: 516.2337861359119
time_elpased: 1.383
batch start
#iterations: 314
currently lose_sum: 518.4049134552479
time_elpased: 1.377
batch start
#iterations: 315
currently lose_sum: 516.4568447172642
time_elpased: 1.372
batch start
#iterations: 316
currently lose_sum: 516.4231132268906
time_elpased: 1.389
batch start
#iterations: 317
currently lose_sum: 516.2938912808895
time_elpased: 1.375
batch start
#iterations: 318
currently lose_sum: 514.8891568183899
time_elpased: 1.386
batch start
#iterations: 319
currently lose_sum: 515.386734932661
time_elpased: 1.381
start validation test
0.150103092784
1.0
0.150103092784
0.261025457153
0
validation finish
batch start
#iterations: 320
currently lose_sum: 518.7097487151623
time_elpased: 1.37
batch start
#iterations: 321
currently lose_sum: 515.9762471914291
time_elpased: 1.38
batch start
#iterations: 322
currently lose_sum: 513.5294373333454
time_elpased: 1.376
batch start
#iterations: 323
currently lose_sum: 517.5596836805344
time_elpased: 1.369
batch start
#iterations: 324
currently lose_sum: 515.8270385563374
time_elpased: 1.393
batch start
#iterations: 325
currently lose_sum: 517.4389960467815
time_elpased: 1.379
batch start
#iterations: 326
currently lose_sum: 516.3882171809673
time_elpased: 1.387
batch start
#iterations: 327
currently lose_sum: 516.0323281884193
time_elpased: 1.383
batch start
#iterations: 328
currently lose_sum: 517.9914884567261
time_elpased: 1.377
batch start
#iterations: 329
currently lose_sum: 516.0397408902645
time_elpased: 1.378
batch start
#iterations: 330
currently lose_sum: 518.2087337374687
time_elpased: 1.37
batch start
#iterations: 331
currently lose_sum: 515.7931697070599
time_elpased: 1.383
batch start
#iterations: 332
currently lose_sum: 516.9349170327187
time_elpased: 1.37
batch start
#iterations: 333
currently lose_sum: 516.2207720577717
time_elpased: 1.386
batch start
#iterations: 334
currently lose_sum: 515.3009169697762
time_elpased: 1.37
batch start
#iterations: 335
currently lose_sum: 515.0368132293224
time_elpased: 1.375
batch start
#iterations: 336
currently lose_sum: 516.938029885292
time_elpased: 1.385
batch start
#iterations: 337
currently lose_sum: 516.5477389395237
time_elpased: 1.367
batch start
#iterations: 338
currently lose_sum: 516.5466437935829
time_elpased: 1.375
batch start
#iterations: 339
currently lose_sum: 517.6785788834095
time_elpased: 1.376
start validation test
0.234536082474
1.0
0.234536082474
0.379958246347
0
validation finish
batch start
#iterations: 340
currently lose_sum: 517.4984956979752
time_elpased: 1.378
batch start
#iterations: 341
currently lose_sum: 515.8121074736118
time_elpased: 1.392
batch start
#iterations: 342
currently lose_sum: 515.3748343288898
time_elpased: 1.384
batch start
#iterations: 343
currently lose_sum: 516.4238547682762
time_elpased: 1.384
batch start
#iterations: 344
currently lose_sum: 518.4707346856594
time_elpased: 1.394
batch start
#iterations: 345
currently lose_sum: 518.5660062432289
time_elpased: 1.375
batch start
#iterations: 346
currently lose_sum: 513.3412912786007
time_elpased: 1.381
batch start
#iterations: 347
currently lose_sum: 516.1278532445431
time_elpased: 1.4
batch start
#iterations: 348
currently lose_sum: 516.164905756712
time_elpased: 1.386
batch start
#iterations: 349
currently lose_sum: 516.2195165157318
time_elpased: 1.376
batch start
#iterations: 350
currently lose_sum: 517.0191023051739
time_elpased: 1.391
batch start
#iterations: 351
currently lose_sum: 517.1734514534473
time_elpased: 1.398
batch start
#iterations: 352
currently lose_sum: 518.1361349523067
time_elpased: 1.371
batch start
#iterations: 353
currently lose_sum: 517.3484272658825
time_elpased: 1.381
batch start
#iterations: 354
currently lose_sum: 519.0090776085854
time_elpased: 1.386
batch start
#iterations: 355
currently lose_sum: 517.0472505688667
time_elpased: 1.374
batch start
#iterations: 356
currently lose_sum: 515.6764124035835
time_elpased: 1.378
batch start
#iterations: 357
currently lose_sum: 515.880561798811
time_elpased: 1.391
batch start
#iterations: 358
currently lose_sum: 515.410777002573
time_elpased: 1.387
batch start
#iterations: 359
currently lose_sum: 516.6043955683708
time_elpased: 1.375
start validation test
0.223092783505
1.0
0.223092783505
0.364801078894
0
validation finish
batch start
#iterations: 360
currently lose_sum: 516.68812546134
time_elpased: 1.392
batch start
#iterations: 361
currently lose_sum: 517.3487483859062
time_elpased: 1.384
batch start
#iterations: 362
currently lose_sum: 516.325348585844
time_elpased: 1.382
batch start
#iterations: 363
currently lose_sum: 515.539776623249
time_elpased: 1.383
batch start
#iterations: 364
currently lose_sum: 516.9847936034203
time_elpased: 1.382
batch start
#iterations: 365
currently lose_sum: 515.1788087189198
time_elpased: 1.389
batch start
#iterations: 366
currently lose_sum: 516.9224746525288
time_elpased: 1.383
batch start
#iterations: 367
currently lose_sum: 519.1157907247543
time_elpased: 1.381
batch start
#iterations: 368
currently lose_sum: 515.7676034867764
time_elpased: 1.37
batch start
#iterations: 369
currently lose_sum: 515.6816019415855
time_elpased: 1.374
batch start
#iterations: 370
currently lose_sum: 517.9828995168209
time_elpased: 1.385
batch start
#iterations: 371
currently lose_sum: 514.16145414114
time_elpased: 1.396
batch start
#iterations: 372
currently lose_sum: 517.9504814147949
time_elpased: 1.391
batch start
#iterations: 373
currently lose_sum: 518.6029042005539
time_elpased: 1.396
batch start
#iterations: 374
currently lose_sum: 515.2089812457561
time_elpased: 1.372
batch start
#iterations: 375
currently lose_sum: 517.7069641947746
time_elpased: 1.377
batch start
#iterations: 376
currently lose_sum: 515.7466633915901
time_elpased: 1.395
batch start
#iterations: 377
currently lose_sum: 515.8536083102226
time_elpased: 1.381
batch start
#iterations: 378
currently lose_sum: 516.144084841013
time_elpased: 1.389
batch start
#iterations: 379
currently lose_sum: 515.2629306316376
time_elpased: 1.41
start validation test
0.202680412371
1.0
0.202680412371
0.337047831305
0
validation finish
batch start
#iterations: 380
currently lose_sum: 516.2557826936245
time_elpased: 1.367
batch start
#iterations: 381
currently lose_sum: 515.4957103431225
time_elpased: 1.396
batch start
#iterations: 382
currently lose_sum: 513.7727771997452
time_elpased: 1.371
batch start
#iterations: 383
currently lose_sum: 515.1660286784172
time_elpased: 1.37
batch start
#iterations: 384
currently lose_sum: 517.156851798296
time_elpased: 1.38
batch start
#iterations: 385
currently lose_sum: 517.2152722775936
time_elpased: 1.382
batch start
#iterations: 386
currently lose_sum: 518.9323681592941
time_elpased: 1.404
batch start
#iterations: 387
currently lose_sum: 515.4481398463249
time_elpased: 1.385
batch start
#iterations: 388
currently lose_sum: 515.7906863093376
time_elpased: 1.378
batch start
#iterations: 389
currently lose_sum: 516.031834334135
time_elpased: 1.38
batch start
#iterations: 390
currently lose_sum: 516.9125834703445
time_elpased: 1.371
batch start
#iterations: 391
currently lose_sum: 514.9105479121208
time_elpased: 1.393
batch start
#iterations: 392
currently lose_sum: 516.3610030114651
time_elpased: 1.389
batch start
#iterations: 393
currently lose_sum: 517.6350145339966
time_elpased: 1.384
batch start
#iterations: 394
currently lose_sum: 515.7399445176125
time_elpased: 1.389
batch start
#iterations: 395
currently lose_sum: 517.1461324691772
time_elpased: 1.388
batch start
#iterations: 396
currently lose_sum: 515.1852476000786
time_elpased: 1.378
batch start
#iterations: 397
currently lose_sum: 517.009780973196
time_elpased: 1.384
batch start
#iterations: 398
currently lose_sum: 515.1390554010868
time_elpased: 1.38
batch start
#iterations: 399
currently lose_sum: 517.4318547844887
time_elpased: 1.38
start validation test
0.141134020619
1.0
0.141134020619
0.247357484868
0
validation finish
acc: 0.252
pre: 1.000
rec: 0.252
F1: 0.402
auc: 0.000
