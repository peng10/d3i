start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 544.1958711445332
time_elpased: 7.827
batch start
#iterations: 1
currently lose_sum: 530.7851583957672
time_elpased: 7.824
batch start
#iterations: 2
currently lose_sum: 526.782274633646
time_elpased: 7.784
batch start
#iterations: 3
currently lose_sum: 524.8241279423237
time_elpased: 7.774
batch start
#iterations: 4
currently lose_sum: 523.5707868635654
time_elpased: 7.801
batch start
#iterations: 5
currently lose_sum: 521.2922931611538
time_elpased: 7.731
batch start
#iterations: 6
currently lose_sum: 520.4168956875801
time_elpased: 7.76
batch start
#iterations: 7
currently lose_sum: 520.3363857865334
time_elpased: 7.791
batch start
#iterations: 8
currently lose_sum: 519.8165427446365
time_elpased: 7.762
batch start
#iterations: 9
currently lose_sum: 521.5863638818264
time_elpased: 7.789
batch start
#iterations: 10
currently lose_sum: 520.8959859311581
time_elpased: 7.804
batch start
#iterations: 11
currently lose_sum: 522.433990508318
time_elpased: 7.828
batch start
#iterations: 12
currently lose_sum: 521.451906144619
time_elpased: 7.793
batch start
#iterations: 13
currently lose_sum: 518.9912222027779
time_elpased: 7.798
batch start
#iterations: 14
currently lose_sum: 519.8979162275791
time_elpased: 7.825
batch start
#iterations: 15
currently lose_sum: 518.4184702932835
time_elpased: 7.821
batch start
#iterations: 16
currently lose_sum: 518.8690284192562
time_elpased: 7.878
batch start
#iterations: 17
currently lose_sum: 520.5257225930691
time_elpased: 7.85
batch start
#iterations: 18
currently lose_sum: 519.7523822188377
time_elpased: 7.851
batch start
#iterations: 19
currently lose_sum: 519.6367271840572
time_elpased: 7.841
start validation test
0.218350515464
1.0
0.218350515464
0.358436283635
0
validation finish
batch start
#iterations: 20
currently lose_sum: 518.1232676506042
time_elpased: 7.835
batch start
#iterations: 21
currently lose_sum: 519.4125887453556
time_elpased: 7.9
batch start
#iterations: 22
currently lose_sum: 519.5384508371353
time_elpased: 7.898
batch start
#iterations: 23
currently lose_sum: 520.0102870464325
time_elpased: 7.902
batch start
#iterations: 24
currently lose_sum: 518.0289108157158
time_elpased: 7.906
batch start
#iterations: 25
currently lose_sum: 518.1702030003071
time_elpased: 7.857
batch start
#iterations: 26
currently lose_sum: 518.9013986289501
time_elpased: 7.849
batch start
#iterations: 27
currently lose_sum: 518.5010889470577
time_elpased: 7.854
batch start
#iterations: 28
currently lose_sum: 517.1124593019485
time_elpased: 7.86
batch start
#iterations: 29
currently lose_sum: 516.3014968633652
time_elpased: 7.852
batch start
#iterations: 30
currently lose_sum: 516.59392619133
time_elpased: 7.855
batch start
#iterations: 31
currently lose_sum: 518.0151716768742
time_elpased: 7.883
batch start
#iterations: 32
currently lose_sum: 518.9785457253456
time_elpased: 7.844
batch start
#iterations: 33
currently lose_sum: 518.1839681565762
time_elpased: 7.812
batch start
#iterations: 34
currently lose_sum: 517.7551428973675
time_elpased: 7.813
batch start
#iterations: 35
currently lose_sum: 517.6615424752235
time_elpased: 7.853
batch start
#iterations: 36
currently lose_sum: 518.1909627616405
time_elpased: 7.896
batch start
#iterations: 37
currently lose_sum: 515.871183514595
time_elpased: 7.876
batch start
#iterations: 38
currently lose_sum: 517.2342076599598
time_elpased: 7.893
batch start
#iterations: 39
currently lose_sum: 517.6795129179955
time_elpased: 7.863
start validation test
0.167216494845
1.0
0.167216494845
0.286521815934
0
validation finish
batch start
#iterations: 40
currently lose_sum: 517.7778399884701
time_elpased: 7.846
batch start
#iterations: 41
currently lose_sum: 517.9809814691544
time_elpased: 7.849
batch start
#iterations: 42
currently lose_sum: 516.8768240511417
time_elpased: 7.885
batch start
#iterations: 43
currently lose_sum: 515.6592191457748
time_elpased: 7.847
batch start
#iterations: 44
currently lose_sum: 518.8810174763203
time_elpased: 7.851
batch start
#iterations: 45
currently lose_sum: 518.7414031028748
time_elpased: 7.883
batch start
#iterations: 46
currently lose_sum: 516.2458943128586
time_elpased: 7.875
batch start
#iterations: 47
currently lose_sum: 517.9484089612961
time_elpased: 7.864
batch start
#iterations: 48
currently lose_sum: 518.1339962184429
time_elpased: 7.896
batch start
#iterations: 49
currently lose_sum: 514.2441863119602
time_elpased: 7.889
batch start
#iterations: 50
currently lose_sum: 518.2889449596405
time_elpased: 7.881
batch start
#iterations: 51
currently lose_sum: 516.9948571920395
time_elpased: 7.847
batch start
#iterations: 52
currently lose_sum: 516.8340996801853
time_elpased: 7.838
batch start
#iterations: 53
currently lose_sum: 517.7710690796375
time_elpased: 7.797
batch start
#iterations: 54
currently lose_sum: 516.2527535259724
time_elpased: 7.765
batch start
#iterations: 55
currently lose_sum: 518.1671443879604
time_elpased: 7.752
batch start
#iterations: 56
currently lose_sum: 517.6493494212627
time_elpased: 7.799
batch start
#iterations: 57
currently lose_sum: 516.28163382411
time_elpased: 7.752
batch start
#iterations: 58
currently lose_sum: 517.4503246247768
time_elpased: 7.775
batch start
#iterations: 59
currently lose_sum: 516.493311136961
time_elpased: 7.743
start validation test
0.177731958763
1.0
0.177731958763
0.301820728291
0
validation finish
batch start
#iterations: 60
currently lose_sum: 516.108170568943
time_elpased: 7.729
batch start
#iterations: 61
currently lose_sum: 515.765145868063
time_elpased: 7.75
batch start
#iterations: 62
currently lose_sum: 518.9760354757309
time_elpased: 7.721
batch start
#iterations: 63
currently lose_sum: 518.6362216174603
time_elpased: 7.737
batch start
#iterations: 64
currently lose_sum: 516.6625039875507
time_elpased: 7.723
batch start
#iterations: 65
currently lose_sum: 517.1647869646549
time_elpased: 7.715
batch start
#iterations: 66
currently lose_sum: 517.0069842338562
time_elpased: 7.7
batch start
#iterations: 67
currently lose_sum: 515.2008627951145
time_elpased: 7.867
batch start
#iterations: 68
currently lose_sum: 516.7274954915047
time_elpased: 7.798
batch start
#iterations: 69
currently lose_sum: 519.4002386033535
time_elpased: 7.781
batch start
#iterations: 70
currently lose_sum: 514.5456490516663
time_elpased: 7.779
batch start
#iterations: 71
currently lose_sum: 517.8519959449768
time_elpased: 7.797
batch start
#iterations: 72
currently lose_sum: 518.2581276893616
time_elpased: 7.852
batch start
#iterations: 73
currently lose_sum: 515.6271677315235
time_elpased: 7.857
batch start
#iterations: 74
currently lose_sum: 517.8410549759865
time_elpased: 7.836
batch start
#iterations: 75
currently lose_sum: 516.3053537607193
time_elpased: 7.768
batch start
#iterations: 76
currently lose_sum: 516.1148313581944
time_elpased: 7.775
batch start
#iterations: 77
currently lose_sum: 517.1178347170353
time_elpased: 7.788
batch start
#iterations: 78
currently lose_sum: 517.286980509758
time_elpased: 7.791
batch start
#iterations: 79
currently lose_sum: 519.2010188698769
time_elpased: 8.125
start validation test
0.214020618557
1.0
0.214020618557
0.352581521739
0
validation finish
batch start
#iterations: 80
currently lose_sum: 516.5827031433582
time_elpased: 8.209
batch start
#iterations: 81
currently lose_sum: 516.1013461053371
time_elpased: 7.892
batch start
#iterations: 82
currently lose_sum: 517.3593209087849
time_elpased: 7.732
batch start
#iterations: 83
currently lose_sum: 515.9683618843555
time_elpased: 7.844
batch start
#iterations: 84
currently lose_sum: 516.8637557625771
time_elpased: 7.829
batch start
#iterations: 85
currently lose_sum: 515.3022022843361
time_elpased: 7.837
batch start
#iterations: 86
currently lose_sum: 516.6710084378719
time_elpased: 7.872
batch start
#iterations: 87
currently lose_sum: 515.7946871817112
time_elpased: 8.002
batch start
#iterations: 88
currently lose_sum: 517.4220120310783
time_elpased: 8.048
batch start
#iterations: 89
currently lose_sum: 517.7280664145947
time_elpased: 8.003
batch start
#iterations: 90
currently lose_sum: 516.933179050684
time_elpased: 7.702
batch start
#iterations: 91
currently lose_sum: 515.097643405199
time_elpased: 7.696
batch start
#iterations: 92
currently lose_sum: 516.1902550458908
time_elpased: 7.711
batch start
#iterations: 93
currently lose_sum: 516.864269554615
time_elpased: 7.729
batch start
#iterations: 94
currently lose_sum: 516.1259790062904
time_elpased: 7.751
batch start
#iterations: 95
currently lose_sum: 516.1436893343925
time_elpased: 7.776
batch start
#iterations: 96
currently lose_sum: 517.8890430629253
time_elpased: 7.817
batch start
#iterations: 97
currently lose_sum: 514.0449830293655
time_elpased: 7.849
batch start
#iterations: 98
currently lose_sum: 517.267963796854
time_elpased: 7.848
batch start
#iterations: 99
currently lose_sum: 517.601890027523
time_elpased: 7.843
start validation test
0.173711340206
1.0
0.173711340206
0.296003513395
0
validation finish
batch start
#iterations: 100
currently lose_sum: 516.4206611514091
time_elpased: 7.834
batch start
#iterations: 101
currently lose_sum: 517.3162831664085
time_elpased: 7.801
batch start
#iterations: 102
currently lose_sum: 514.9500350058079
time_elpased: 7.857
batch start
#iterations: 103
currently lose_sum: 517.3543214797974
time_elpased: 7.793
batch start
#iterations: 104
currently lose_sum: 516.6662206947803
time_elpased: 7.818
batch start
#iterations: 105
currently lose_sum: 516.5441647469997
time_elpased: 7.787
batch start
#iterations: 106
currently lose_sum: 518.1308436393738
time_elpased: 7.76
batch start
#iterations: 107
currently lose_sum: 516.0775979757309
time_elpased: 8.389
batch start
#iterations: 108
currently lose_sum: 514.8263519704342
time_elpased: 8.088
batch start
#iterations: 109
currently lose_sum: 517.0487232208252
time_elpased: 7.795
batch start
#iterations: 110
currently lose_sum: 518.7714836299419
time_elpased: 7.783
batch start
#iterations: 111
currently lose_sum: 519.6108394861221
time_elpased: 7.812
batch start
#iterations: 112
currently lose_sum: 515.2073419392109
time_elpased: 7.9
batch start
#iterations: 113
currently lose_sum: 516.9513800442219
time_elpased: 8.159
batch start
#iterations: 114
currently lose_sum: 516.8684331178665
time_elpased: 7.627
batch start
#iterations: 115
currently lose_sum: 516.2802278697491
time_elpased: 7.615
batch start
#iterations: 116
currently lose_sum: 515.3062822520733
time_elpased: 7.648
batch start
#iterations: 117
currently lose_sum: 517.8188882172108
time_elpased: 7.595
batch start
#iterations: 118
currently lose_sum: 515.8960384428501
time_elpased: 7.62
batch start
#iterations: 119
currently lose_sum: 516.2561802864075
time_elpased: 7.635
start validation test
0.178350515464
1.0
0.178350515464
0.30271216098
0
validation finish
batch start
#iterations: 120
currently lose_sum: 516.851142257452
time_elpased: 7.664
batch start
#iterations: 121
currently lose_sum: 515.8048758804798
time_elpased: 7.681
batch start
#iterations: 122
currently lose_sum: 515.4720773398876
time_elpased: 7.756
batch start
#iterations: 123
currently lose_sum: 516.0562869608402
time_elpased: 7.754
batch start
#iterations: 124
currently lose_sum: 516.5273689329624
time_elpased: 7.79
batch start
#iterations: 125
currently lose_sum: 517.0872701704502
time_elpased: 7.746
batch start
#iterations: 126
currently lose_sum: 517.3963709175587
time_elpased: 7.734
batch start
#iterations: 127
currently lose_sum: 516.2301822304726
time_elpased: 7.724
batch start
#iterations: 128
currently lose_sum: 516.4069877564907
time_elpased: 7.735
batch start
#iterations: 129
currently lose_sum: 516.6696962714195
time_elpased: 7.745
batch start
#iterations: 130
currently lose_sum: 515.7171796262264
time_elpased: 7.795
batch start
#iterations: 131
currently lose_sum: 516.7855438292027
time_elpased: 7.809
batch start
#iterations: 132
currently lose_sum: 516.9944072663784
time_elpased: 7.809
batch start
#iterations: 133
currently lose_sum: 517.9816670417786
time_elpased: 7.818
batch start
#iterations: 134
currently lose_sum: 515.737127661705
time_elpased: 8.264
batch start
#iterations: 135
currently lose_sum: 515.078303784132
time_elpased: 7.77
batch start
#iterations: 136
currently lose_sum: 516.2225830256939
time_elpased: 7.725
batch start
#iterations: 137
currently lose_sum: 515.7144681811333
time_elpased: 7.7
batch start
#iterations: 138
currently lose_sum: 514.5534463822842
time_elpased: 7.765
batch start
#iterations: 139
currently lose_sum: 516.5570212900639
time_elpased: 7.811
start validation test
0.177422680412
1.0
0.177422680412
0.301374660713
0
validation finish
batch start
#iterations: 140
currently lose_sum: 513.6471219956875
time_elpased: 7.822
batch start
#iterations: 141
currently lose_sum: 515.9370405972004
time_elpased: 7.863
batch start
#iterations: 142
currently lose_sum: 514.4507282376289
time_elpased: 7.86
batch start
#iterations: 143
currently lose_sum: 516.7521570622921
time_elpased: 7.863
batch start
#iterations: 144
currently lose_sum: 515.8448729813099
time_elpased: 7.859
batch start
#iterations: 145
currently lose_sum: 518.1047924757004
time_elpased: 7.832
batch start
#iterations: 146
currently lose_sum: 518.3490308225155
time_elpased: 7.802
batch start
#iterations: 147
currently lose_sum: 517.1963400840759
time_elpased: 7.854
batch start
#iterations: 148
currently lose_sum: 514.8485848605633
time_elpased: 7.813
batch start
#iterations: 149
currently lose_sum: 518.0616780817509
time_elpased: 7.828
batch start
#iterations: 150
currently lose_sum: 514.9562104642391
time_elpased: 7.792
batch start
#iterations: 151
currently lose_sum: 514.7269521355629
time_elpased: 7.793
batch start
#iterations: 152
currently lose_sum: 515.6088293492794
time_elpased: 7.815
batch start
#iterations: 153
currently lose_sum: 513.7759520113468
time_elpased: 7.84
batch start
#iterations: 154
currently lose_sum: 518.3412740826607
time_elpased: 7.777
batch start
#iterations: 155
currently lose_sum: 513.6606459319592
time_elpased: 7.739
batch start
#iterations: 156
currently lose_sum: 516.0096353888512
time_elpased: 7.756
batch start
#iterations: 157
currently lose_sum: 514.4655618667603
time_elpased: 7.773
batch start
#iterations: 158
currently lose_sum: 515.1252726316452
time_elpased: 7.815
batch start
#iterations: 159
currently lose_sum: 515.9883655905724
time_elpased: 7.772
start validation test
0.185979381443
1.0
0.185979381443
0.313630041725
0
validation finish
batch start
#iterations: 160
currently lose_sum: 516.5370064973831
time_elpased: 7.837
batch start
#iterations: 161
currently lose_sum: 514.786801815033
time_elpased: 7.796
batch start
#iterations: 162
currently lose_sum: 515.4025561213493
time_elpased: 7.784
batch start
#iterations: 163
currently lose_sum: 515.8216745853424
time_elpased: 7.724
batch start
#iterations: 164
currently lose_sum: 516.1783447265625
time_elpased: 7.812
batch start
#iterations: 165
currently lose_sum: 517.9198638796806
time_elpased: 7.872
batch start
#iterations: 166
currently lose_sum: 516.6396087110043
time_elpased: 7.892
batch start
#iterations: 167
currently lose_sum: 515.9732217490673
time_elpased: 7.85
batch start
#iterations: 168
currently lose_sum: 515.6293558776379
time_elpased: 7.873
batch start
#iterations: 169
currently lose_sum: 515.9525854289532
time_elpased: 7.873
batch start
#iterations: 170
currently lose_sum: 515.5771593749523
time_elpased: 7.83
batch start
#iterations: 171
currently lose_sum: 514.1881689727306
time_elpased: 7.797
batch start
#iterations: 172
currently lose_sum: 518.0309059023857
time_elpased: 7.815
batch start
#iterations: 173
currently lose_sum: 516.402175784111
time_elpased: 7.81
batch start
#iterations: 174
currently lose_sum: 517.0225444436073
time_elpased: 7.845
batch start
#iterations: 175
currently lose_sum: 515.6065362691879
time_elpased: 7.836
batch start
#iterations: 176
currently lose_sum: 515.7582794129848
time_elpased: 7.82
batch start
#iterations: 177
currently lose_sum: 516.9453776180744
time_elpased: 7.799
batch start
#iterations: 178
currently lose_sum: 517.5786545276642
time_elpased: 7.809
batch start
#iterations: 179
currently lose_sum: 517.8133279383183
time_elpased: 7.852
start validation test
0.188350515464
1.0
0.188350515464
0.316994881582
0
validation finish
batch start
#iterations: 180
currently lose_sum: 517.1592048704624
time_elpased: 7.802
batch start
#iterations: 181
currently lose_sum: 518.1035203039646
time_elpased: 7.812
batch start
#iterations: 182
currently lose_sum: 515.8965855240822
time_elpased: 7.829
batch start
#iterations: 183
currently lose_sum: 516.6140073537827
time_elpased: 7.813
batch start
#iterations: 184
currently lose_sum: 515.4372026324272
time_elpased: 7.775
batch start
#iterations: 185
currently lose_sum: 517.4388525187969
time_elpased: 7.766
batch start
#iterations: 186
currently lose_sum: 516.9470406770706
time_elpased: 7.81
batch start
#iterations: 187
currently lose_sum: 516.9457674026489
time_elpased: 7.796
batch start
#iterations: 188
currently lose_sum: 515.5751756429672
time_elpased: 7.817
batch start
#iterations: 189
currently lose_sum: 516.1720411777496
time_elpased: 7.754
batch start
#iterations: 190
currently lose_sum: 515.8310266137123
time_elpased: 7.805
batch start
#iterations: 191
currently lose_sum: 517.1952497661114
time_elpased: 7.756
batch start
#iterations: 192
currently lose_sum: 514.0619795322418
time_elpased: 7.739
batch start
#iterations: 193
currently lose_sum: 516.625362277031
time_elpased: 7.776
batch start
#iterations: 194
currently lose_sum: 517.3647882044315
time_elpased: 7.808
batch start
#iterations: 195
currently lose_sum: 519.2115430235863
time_elpased: 7.753
batch start
#iterations: 196
currently lose_sum: 513.8966912329197
time_elpased: 7.771
batch start
#iterations: 197
currently lose_sum: 517.0756062269211
time_elpased: 7.798
batch start
#iterations: 198
currently lose_sum: 515.1862835884094
time_elpased: 7.81
batch start
#iterations: 199
currently lose_sum: 515.9508398473263
time_elpased: 7.812
start validation test
0.185360824742
1.0
0.185360824742
0.312750043486
0
validation finish
batch start
#iterations: 200
currently lose_sum: 516.1785715520382
time_elpased: 7.796
batch start
#iterations: 201
currently lose_sum: 517.8774779140949
time_elpased: 7.78
batch start
#iterations: 202
currently lose_sum: 515.2222956418991
time_elpased: 7.803
batch start
#iterations: 203
currently lose_sum: 515.0281440913677
time_elpased: 7.773
batch start
#iterations: 204
currently lose_sum: 515.0956855118275
time_elpased: 7.826
batch start
#iterations: 205
currently lose_sum: 516.2959225475788
time_elpased: 7.807
batch start
#iterations: 206
currently lose_sum: 518.5902621746063
time_elpased: 7.818
batch start
#iterations: 207
currently lose_sum: 515.3995983302593
time_elpased: 7.848
batch start
#iterations: 208
currently lose_sum: 514.4946078360081
time_elpased: 7.849
batch start
#iterations: 209
currently lose_sum: 516.330065459013
time_elpased: 7.92
batch start
#iterations: 210
currently lose_sum: 515.3102120161057
time_elpased: 7.883
batch start
#iterations: 211
currently lose_sum: 516.3731860220432
time_elpased: 7.837
batch start
#iterations: 212
currently lose_sum: 515.9981945157051
time_elpased: 7.839
batch start
#iterations: 213
currently lose_sum: 516.0588592290878
time_elpased: 7.866
batch start
#iterations: 214
currently lose_sum: 517.2846829891205
time_elpased: 7.852
batch start
#iterations: 215
currently lose_sum: 516.0934003591537
time_elpased: 7.859
batch start
#iterations: 216
currently lose_sum: 516.8898167014122
time_elpased: 7.798
batch start
#iterations: 217
currently lose_sum: 515.638831526041
time_elpased: 7.813
batch start
#iterations: 218
currently lose_sum: 515.3139774501324
time_elpased: 7.783
batch start
#iterations: 219
currently lose_sum: 517.6350015699863
time_elpased: 7.752
start validation test
0.190927835052
1.0
0.190927835052
0.320637119114
0
validation finish
batch start
#iterations: 220
currently lose_sum: 516.1768015623093
time_elpased: 7.793
batch start
#iterations: 221
currently lose_sum: 516.5819833278656
time_elpased: 7.784
batch start
#iterations: 222
currently lose_sum: 514.7970185577869
time_elpased: 7.778
batch start
#iterations: 223
currently lose_sum: 516.5597868859768
time_elpased: 7.774
batch start
#iterations: 224
currently lose_sum: 516.3659252524376
time_elpased: 7.774
batch start
#iterations: 225
currently lose_sum: 518.9828835129738
time_elpased: 7.777
batch start
#iterations: 226
currently lose_sum: 516.0539547502995
time_elpased: 7.788
batch start
#iterations: 227
currently lose_sum: 515.4889772236347
time_elpased: 7.737
batch start
#iterations: 228
currently lose_sum: 514.3180884122849
time_elpased: 7.706
batch start
#iterations: 229
currently lose_sum: 516.4752385616302
time_elpased: 7.678
batch start
#iterations: 230
currently lose_sum: 517.1718097627163
time_elpased: 7.674
batch start
#iterations: 231
currently lose_sum: 518.4064246714115
time_elpased: 7.757
batch start
#iterations: 232
currently lose_sum: 515.7807519137859
time_elpased: 7.705
batch start
#iterations: 233
currently lose_sum: 513.7903863191605
time_elpased: 7.758
batch start
#iterations: 234
currently lose_sum: 514.6836293339729
time_elpased: 7.74
batch start
#iterations: 235
currently lose_sum: 515.2740822732449
time_elpased: 7.694
batch start
#iterations: 236
currently lose_sum: 515.1326606273651
time_elpased: 7.733
batch start
#iterations: 237
currently lose_sum: 516.5241180956364
time_elpased: 7.806
batch start
#iterations: 238
currently lose_sum: 514.6588977277279
time_elpased: 7.813
batch start
#iterations: 239
currently lose_sum: 515.0556475520134
time_elpased: 7.821
start validation test
0.167525773196
1.0
0.167525773196
0.286975717439
0
validation finish
batch start
#iterations: 240
currently lose_sum: 515.7330230176449
time_elpased: 7.811
batch start
#iterations: 241
currently lose_sum: 514.6800404191017
time_elpased: 7.82
batch start
#iterations: 242
currently lose_sum: 516.2139593362808
time_elpased: 7.809
batch start
#iterations: 243
currently lose_sum: 514.6499862372875
time_elpased: 7.787
batch start
#iterations: 244
currently lose_sum: 516.1412037909031
time_elpased: 7.8
batch start
#iterations: 245
currently lose_sum: 514.7996005117893
time_elpased: 7.807
batch start
#iterations: 246
currently lose_sum: 515.0805540382862
time_elpased: 7.78
batch start
#iterations: 247
currently lose_sum: 515.6129408478737
time_elpased: 7.744
batch start
#iterations: 248
currently lose_sum: 515.0151315033436
time_elpased: 7.711
batch start
#iterations: 249
currently lose_sum: 515.8019076287746
time_elpased: 7.767
batch start
#iterations: 250
currently lose_sum: 515.4070029258728
time_elpased: 7.774
batch start
#iterations: 251
currently lose_sum: 517.3756230175495
time_elpased: 7.76
batch start
#iterations: 252
currently lose_sum: 518.0031195282936
time_elpased: 7.743
batch start
#iterations: 253
currently lose_sum: 514.8689516186714
time_elpased: 7.743
batch start
#iterations: 254
currently lose_sum: 517.819521009922
time_elpased: 7.796
batch start
#iterations: 255
currently lose_sum: 515.0643485486507
time_elpased: 7.835
batch start
#iterations: 256
currently lose_sum: 517.638314217329
time_elpased: 7.905
batch start
#iterations: 257
currently lose_sum: 514.229746222496
time_elpased: 7.828
batch start
#iterations: 258
currently lose_sum: 514.6621840894222
time_elpased: 7.836
batch start
#iterations: 259
currently lose_sum: 516.9439296424389
time_elpased: 7.79
start validation test
0.196288659794
1.0
0.196288659794
0.328162702516
0
validation finish
batch start
#iterations: 260
currently lose_sum: 517.7424988150597
time_elpased: 7.805
batch start
#iterations: 261
currently lose_sum: 516.5118412673473
time_elpased: 7.797
batch start
#iterations: 262
currently lose_sum: 515.6453883647919
time_elpased: 7.779
batch start
#iterations: 263
currently lose_sum: 515.3943713903427
time_elpased: 7.787
batch start
#iterations: 264
currently lose_sum: 515.2218000590801
time_elpased: 7.803
batch start
#iterations: 265
currently lose_sum: 516.7793440818787
time_elpased: 7.783
batch start
#iterations: 266
currently lose_sum: 516.2479194700718
time_elpased: 7.831
batch start
#iterations: 267
currently lose_sum: 514.4423380792141
time_elpased: 7.838
batch start
#iterations: 268
currently lose_sum: 515.3946970105171
time_elpased: 7.845
batch start
#iterations: 269
currently lose_sum: 514.260864764452
time_elpased: 7.837
batch start
#iterations: 270
currently lose_sum: 517.0376192033291
time_elpased: 7.827
batch start
#iterations: 271
currently lose_sum: 514.0036253929138
time_elpased: 7.819
batch start
#iterations: 272
currently lose_sum: 515.6771959662437
time_elpased: 7.825
batch start
#iterations: 273
currently lose_sum: 516.0887713432312
time_elpased: 7.888
batch start
#iterations: 274
currently lose_sum: 516.1278060376644
time_elpased: 7.876
batch start
#iterations: 275
currently lose_sum: 515.4160255789757
time_elpased: 7.828
batch start
#iterations: 276
currently lose_sum: 514.8749031722546
time_elpased: 7.762
batch start
#iterations: 277
currently lose_sum: 516.310906201601
time_elpased: 7.78
batch start
#iterations: 278
currently lose_sum: 514.7304237484932
time_elpased: 7.772
batch start
#iterations: 279
currently lose_sum: 516.4658328592777
time_elpased: 7.766
start validation test
0.182577319588
1.0
0.182577319588
0.308778659228
0
validation finish
batch start
#iterations: 280
currently lose_sum: 515.2513191401958
time_elpased: 7.826
batch start
#iterations: 281
currently lose_sum: 515.0309920310974
time_elpased: 7.878
batch start
#iterations: 282
currently lose_sum: 515.5622589886189
time_elpased: 7.81
batch start
#iterations: 283
currently lose_sum: 516.2922312021255
time_elpased: 7.747
batch start
#iterations: 284
currently lose_sum: 515.3518520593643
time_elpased: 7.75
batch start
#iterations: 285
currently lose_sum: 516.8936807215214
time_elpased: 7.811
batch start
#iterations: 286
currently lose_sum: 515.9129693806171
time_elpased: 7.87
batch start
#iterations: 287
currently lose_sum: 517.7095714211464
time_elpased: 7.84
batch start
#iterations: 288
currently lose_sum: 515.0910142958164
time_elpased: 7.853
batch start
#iterations: 289
currently lose_sum: 515.3190172314644
time_elpased: 7.845
batch start
#iterations: 290
currently lose_sum: 513.7265320122242
time_elpased: 7.879
batch start
#iterations: 291
currently lose_sum: 516.7012784779072
time_elpased: 7.85
batch start
#iterations: 292
currently lose_sum: 515.5858548879623
time_elpased: 7.829
batch start
#iterations: 293
currently lose_sum: 516.4306904673576
time_elpased: 7.877
batch start
#iterations: 294
currently lose_sum: 517.3433867692947
time_elpased: 7.788
batch start
#iterations: 295
currently lose_sum: 516.2997032701969
time_elpased: 7.839
batch start
#iterations: 296
currently lose_sum: 515.218930721283
time_elpased: 7.842
batch start
#iterations: 297
currently lose_sum: 515.6097803413868
time_elpased: 7.791
batch start
#iterations: 298
currently lose_sum: 516.6174368262291
time_elpased: 7.818
batch start
#iterations: 299
currently lose_sum: 517.2348183095455
time_elpased: 7.854
start validation test
0.192680412371
1.0
0.192680412371
0.323104849166
0
validation finish
batch start
#iterations: 300
currently lose_sum: 516.286385089159
time_elpased: 8.111
batch start
#iterations: 301
currently lose_sum: 514.555410951376
time_elpased: 7.697
batch start
#iterations: 302
currently lose_sum: 516.7304684519768
time_elpased: 7.662
batch start
#iterations: 303
currently lose_sum: 516.9100761711597
time_elpased: 7.659
batch start
#iterations: 304
currently lose_sum: 514.4422606825829
time_elpased: 7.682
batch start
#iterations: 305
currently lose_sum: 517.3218896389008
time_elpased: 7.724
batch start
#iterations: 306
currently lose_sum: 516.410940349102
time_elpased: 7.857
batch start
#iterations: 307
currently lose_sum: 517.344518750906
time_elpased: 7.857
batch start
#iterations: 308
currently lose_sum: 516.2113889753819
time_elpased: 7.855
batch start
#iterations: 309
currently lose_sum: 515.6807707250118
time_elpased: 8.058
batch start
#iterations: 310
currently lose_sum: 514.2920902371407
time_elpased: 7.775
batch start
#iterations: 311
currently lose_sum: 515.351142257452
time_elpased: 7.742
batch start
#iterations: 312
currently lose_sum: 517.026661247015
time_elpased: 7.905
batch start
#iterations: 313
currently lose_sum: 516.6038022339344
time_elpased: 7.848
batch start
#iterations: 314
currently lose_sum: 514.9401529729366
time_elpased: 7.897
batch start
#iterations: 315
currently lose_sum: 517.0640730559826
time_elpased: 7.886
batch start
#iterations: 316
currently lose_sum: 516.5927426218987
time_elpased: 7.888
batch start
#iterations: 317
currently lose_sum: 514.9165264964104
time_elpased: 7.84
batch start
#iterations: 318
currently lose_sum: 517.5780066549778
time_elpased: 7.827
batch start
#iterations: 319
currently lose_sum: 515.0954317450523
time_elpased: 7.9
start validation test
0.191649484536
1.0
0.191649484536
0.321654122329
0
validation finish
batch start
#iterations: 320
currently lose_sum: 516.5479216873646
time_elpased: 7.927
batch start
#iterations: 321
currently lose_sum: 517.0150691866875
time_elpased: 7.901
batch start
#iterations: 322
currently lose_sum: 514.7536954581738
time_elpased: 7.88
batch start
#iterations: 323
currently lose_sum: 515.0204024910927
time_elpased: 7.886
batch start
#iterations: 324
currently lose_sum: 517.7299852669239
time_elpased: 7.911
batch start
#iterations: 325
currently lose_sum: 515.9668710529804
time_elpased: 7.869
batch start
#iterations: 326
currently lose_sum: 516.5624326765537
time_elpased: 7.843
batch start
#iterations: 327
currently lose_sum: 516.5067077279091
time_elpased: 7.894
batch start
#iterations: 328
currently lose_sum: 516.6679606437683
time_elpased: 7.835
batch start
#iterations: 329
currently lose_sum: 514.9510869085789
time_elpased: 7.863
batch start
#iterations: 330
currently lose_sum: 517.5211244821548
time_elpased: 7.837
batch start
#iterations: 331
currently lose_sum: 517.6509944796562
time_elpased: 7.874
batch start
#iterations: 332
currently lose_sum: 517.227110683918
time_elpased: 7.832
batch start
#iterations: 333
currently lose_sum: 516.3449681401253
time_elpased: 7.832
batch start
#iterations: 334
currently lose_sum: 515.1488426327705
time_elpased: 7.805
batch start
#iterations: 335
currently lose_sum: 515.7261022925377
time_elpased: 7.847
batch start
#iterations: 336
currently lose_sum: 517.0019298493862
time_elpased: 7.826
batch start
#iterations: 337
currently lose_sum: 515.9531449973583
time_elpased: 7.813
batch start
#iterations: 338
currently lose_sum: 514.0849497914314
time_elpased: 7.842
batch start
#iterations: 339
currently lose_sum: 517.4009998440742
time_elpased: 7.836
start validation test
0.192268041237
1.0
0.192268041237
0.32252485949
0
validation finish
batch start
#iterations: 340
currently lose_sum: 514.5659957826138
time_elpased: 8.132
batch start
#iterations: 341
currently lose_sum: 515.9850161373615
time_elpased: 7.791
batch start
#iterations: 342
currently lose_sum: 517.792355120182
time_elpased: 7.732
batch start
#iterations: 343
currently lose_sum: 517.3142257034779
time_elpased: 7.714
batch start
#iterations: 344
currently lose_sum: 517.9570385217667
time_elpased: 7.816
batch start
#iterations: 345
currently lose_sum: 517.298905402422
time_elpased: 7.873
batch start
#iterations: 346
currently lose_sum: 516.2642543315887
time_elpased: 7.819
batch start
#iterations: 347
currently lose_sum: 516.49009090662
time_elpased: 7.823
batch start
#iterations: 348
currently lose_sum: 516.9066083729267
time_elpased: 7.785
batch start
#iterations: 349
currently lose_sum: 518.0322310328484
time_elpased: 7.801
batch start
#iterations: 350
currently lose_sum: 516.8901914656162
time_elpased: 7.872
batch start
#iterations: 351
currently lose_sum: 516.597204118967
time_elpased: 7.851
batch start
#iterations: 352
currently lose_sum: 516.174652993679
time_elpased: 7.817
batch start
#iterations: 353
currently lose_sum: 517.4453591704369
time_elpased: 7.772
batch start
#iterations: 354
currently lose_sum: 516.5409165918827
time_elpased: 7.701
batch start
#iterations: 355
currently lose_sum: 516.3802063465118
time_elpased: 7.714
batch start
#iterations: 356
currently lose_sum: 515.0290007293224
time_elpased: 7.695
batch start
#iterations: 357
currently lose_sum: 514.809631884098
time_elpased: 7.867
batch start
#iterations: 358
currently lose_sum: 517.1461118459702
time_elpased: 8.044
batch start
#iterations: 359
currently lose_sum: 515.0352281332016
time_elpased: 7.867
start validation test
0.184329896907
1.0
0.184329896907
0.311281337047
0
validation finish
batch start
#iterations: 360
currently lose_sum: 516.0486123859882
time_elpased: 7.809
batch start
#iterations: 361
currently lose_sum: 514.8721458911896
time_elpased: 7.785
batch start
#iterations: 362
currently lose_sum: 516.089096724987
time_elpased: 7.809
batch start
#iterations: 363
currently lose_sum: 516.2988211512566
time_elpased: 7.857
batch start
#iterations: 364
currently lose_sum: 516.5463439524174
time_elpased: 7.767
batch start
#iterations: 365
currently lose_sum: 516.1280636787415
time_elpased: 7.773
batch start
#iterations: 366
currently lose_sum: 516.589538782835
time_elpased: 7.832
batch start
#iterations: 367
currently lose_sum: 516.2497343122959
time_elpased: 7.881
batch start
#iterations: 368
currently lose_sum: 514.9399716854095
time_elpased: 7.845
batch start
#iterations: 369
currently lose_sum: 514.6083472371101
time_elpased: 7.85
batch start
#iterations: 370
currently lose_sum: 514.4700638353825
time_elpased: 7.831
batch start
#iterations: 371
currently lose_sum: 516.2748837172985
time_elpased: 7.92
batch start
#iterations: 372
currently lose_sum: 514.9355431199074
time_elpased: 7.859
batch start
#iterations: 373
currently lose_sum: 515.8159118890762
time_elpased: 7.859
batch start
#iterations: 374
currently lose_sum: 517.0893889665604
time_elpased: 7.852
batch start
#iterations: 375
currently lose_sum: 517.3013526797295
time_elpased: 7.873
batch start
#iterations: 376
currently lose_sum: 516.2022985517979
time_elpased: 7.909
batch start
#iterations: 377
currently lose_sum: 514.858894109726
time_elpased: 7.877
batch start
#iterations: 378
currently lose_sum: 517.6539106667042
time_elpased: 7.899
batch start
#iterations: 379
currently lose_sum: 516.0701064169407
time_elpased: 7.831
start validation test
0.188041237113
1.0
0.188041237113
0.316556751128
0
validation finish
batch start
#iterations: 380
currently lose_sum: 516.2150302827358
time_elpased: 7.825
batch start
#iterations: 381
currently lose_sum: 514.9355762600899
time_elpased: 7.785
batch start
#iterations: 382
currently lose_sum: 516.122078448534
time_elpased: 7.775
batch start
#iterations: 383
currently lose_sum: 513.8209500312805
time_elpased: 7.79
batch start
#iterations: 384
currently lose_sum: 514.0443629324436
time_elpased: 7.771
batch start
#iterations: 385
currently lose_sum: 515.133484184742
time_elpased: 7.766
batch start
#iterations: 386
currently lose_sum: 516.2408594787121
time_elpased: 7.787
batch start
#iterations: 387
currently lose_sum: 514.8882780969143
time_elpased: 7.821
batch start
#iterations: 388
currently lose_sum: 515.6042766571045
time_elpased: 7.804
batch start
#iterations: 389
currently lose_sum: 517.3453493714333
time_elpased: 7.77
batch start
#iterations: 390
currently lose_sum: 515.5892935097218
time_elpased: 7.781
batch start
#iterations: 391
currently lose_sum: 516.7798037827015
time_elpased: 7.749
batch start
#iterations: 392
currently lose_sum: 515.8038128614426
time_elpased: 7.786
batch start
#iterations: 393
currently lose_sum: 514.8660075366497
time_elpased: 7.779
batch start
#iterations: 394
currently lose_sum: 514.9620808362961
time_elpased: 7.709
batch start
#iterations: 395
currently lose_sum: 515.4738149046898
time_elpased: 7.754
batch start
#iterations: 396
currently lose_sum: 515.5620826780796
time_elpased: 7.799
batch start
#iterations: 397
currently lose_sum: 517.2412010133266
time_elpased: 7.847
batch start
#iterations: 398
currently lose_sum: 515.1905632615089
time_elpased: 7.873
batch start
#iterations: 399
currently lose_sum: 514.986975133419
time_elpased: 7.829
start validation test
0.180721649485
1.0
0.180721649485
0.306120667074
0
validation finish
acc: 0.226
pre: 1.000
rec: 0.226
F1: 0.369
auc: 0.000
