start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 543.9974965453148
time_elpased: 7.786
batch start
#iterations: 1
currently lose_sum: 529.7776817977428
time_elpased: 7.795
batch start
#iterations: 2
currently lose_sum: 526.9284635484219
time_elpased: 7.787
batch start
#iterations: 3
currently lose_sum: 522.6154684722424
time_elpased: 7.759
batch start
#iterations: 4
currently lose_sum: 522.1714041829109
time_elpased: 7.79
batch start
#iterations: 5
currently lose_sum: 521.8425056934357
time_elpased: 7.755
batch start
#iterations: 6
currently lose_sum: 519.6431954205036
time_elpased: 7.776
batch start
#iterations: 7
currently lose_sum: 520.7740237116814
time_elpased: 7.776
batch start
#iterations: 8
currently lose_sum: 518.9583539068699
time_elpased: 7.777
batch start
#iterations: 9
currently lose_sum: 519.3829109370708
time_elpased: 7.81
batch start
#iterations: 10
currently lose_sum: 519.9876780509949
time_elpased: 7.793
batch start
#iterations: 11
currently lose_sum: 522.2772170603275
time_elpased: 7.844
batch start
#iterations: 12
currently lose_sum: 518.2248121500015
time_elpased: 7.83
batch start
#iterations: 13
currently lose_sum: 519.2020265161991
time_elpased: 7.837
batch start
#iterations: 14
currently lose_sum: 517.0048987269402
time_elpased: 7.753
batch start
#iterations: 15
currently lose_sum: 518.2482783794403
time_elpased: 7.756
batch start
#iterations: 16
currently lose_sum: 519.8958124816418
time_elpased: 7.8
batch start
#iterations: 17
currently lose_sum: 517.1951903700829
time_elpased: 7.834
batch start
#iterations: 18
currently lose_sum: 517.7727755904198
time_elpased: 7.848
batch start
#iterations: 19
currently lose_sum: 516.0798083841801
time_elpased: 7.85
start validation test
0.239896907216
1.0
0.239896907216
0.386962667332
0
validation finish
batch start
#iterations: 20
currently lose_sum: 517.1117835342884
time_elpased: 7.81
batch start
#iterations: 21
currently lose_sum: 519.5978884100914
time_elpased: 7.772
batch start
#iterations: 22
currently lose_sum: 518.5972554385662
time_elpased: 7.766
batch start
#iterations: 23
currently lose_sum: 517.1300987303257
time_elpased: 7.828
batch start
#iterations: 24
currently lose_sum: 516.554961681366
time_elpased: 7.837
batch start
#iterations: 25
currently lose_sum: 517.3722831606865
time_elpased: 7.827
batch start
#iterations: 26
currently lose_sum: 518.1287195682526
time_elpased: 7.803
batch start
#iterations: 27
currently lose_sum: 517.092114508152
time_elpased: 7.811
batch start
#iterations: 28
currently lose_sum: 516.8605765104294
time_elpased: 7.811
batch start
#iterations: 29
currently lose_sum: 517.4822062551975
time_elpased: 7.839
batch start
#iterations: 30
currently lose_sum: 515.8253746330738
time_elpased: 7.821
batch start
#iterations: 31
currently lose_sum: 517.0568919181824
time_elpased: 8.305
batch start
#iterations: 32
currently lose_sum: 517.9709733724594
time_elpased: 7.898
batch start
#iterations: 33
currently lose_sum: 514.6908675730228
time_elpased: 7.865
batch start
#iterations: 34
currently lose_sum: 516.5832858085632
time_elpased: 7.878
batch start
#iterations: 35
currently lose_sum: 516.5068479776382
time_elpased: 7.866
batch start
#iterations: 36
currently lose_sum: 516.6240465044975
time_elpased: 7.82
batch start
#iterations: 37
currently lose_sum: 516.249100536108
time_elpased: 7.805
batch start
#iterations: 38
currently lose_sum: 517.0516919493675
time_elpased: 7.837
batch start
#iterations: 39
currently lose_sum: 518.1837621033192
time_elpased: 7.856
start validation test
0.190103092784
1.0
0.190103092784
0.319473319473
0
validation finish
batch start
#iterations: 40
currently lose_sum: 517.6586633324623
time_elpased: 7.892
batch start
#iterations: 41
currently lose_sum: 516.4113318622112
time_elpased: 7.911
batch start
#iterations: 42
currently lose_sum: 515.593091070652
time_elpased: 7.9
batch start
#iterations: 43
currently lose_sum: 516.1435729563236
time_elpased: 7.875
batch start
#iterations: 44
currently lose_sum: 515.5851576328278
time_elpased: 7.902
batch start
#iterations: 45
currently lose_sum: 517.331495821476
time_elpased: 7.893
batch start
#iterations: 46
currently lose_sum: 515.9901714026928
time_elpased: 7.893
batch start
#iterations: 47
currently lose_sum: 518.1097700297832
time_elpased: 7.853
batch start
#iterations: 48
currently lose_sum: 514.7070249915123
time_elpased: 7.851
batch start
#iterations: 49
currently lose_sum: 515.8223450481892
time_elpased: 7.811
batch start
#iterations: 50
currently lose_sum: 517.9433490931988
time_elpased: 7.732
batch start
#iterations: 51
currently lose_sum: 516.3311119675636
time_elpased: 7.734
batch start
#iterations: 52
currently lose_sum: 516.6890105605125
time_elpased: 7.747
batch start
#iterations: 53
currently lose_sum: 516.6594417393208
time_elpased: 7.751
batch start
#iterations: 54
currently lose_sum: 516.0400087237358
time_elpased: 7.746
batch start
#iterations: 55
currently lose_sum: 516.4192204475403
time_elpased: 7.765
batch start
#iterations: 56
currently lose_sum: 517.0736225247383
time_elpased: 7.876
batch start
#iterations: 57
currently lose_sum: 515.1573680341244
time_elpased: 7.786
batch start
#iterations: 58
currently lose_sum: 514.522613555193
time_elpased: 7.78
batch start
#iterations: 59
currently lose_sum: 515.4196931421757
time_elpased: 7.761
start validation test
0.218969072165
1.0
0.218969072165
0.359269282815
0
validation finish
batch start
#iterations: 60
currently lose_sum: 515.6373528838158
time_elpased: 7.817
batch start
#iterations: 61
currently lose_sum: 516.2717433273792
time_elpased: 7.794
batch start
#iterations: 62
currently lose_sum: 517.8363009095192
time_elpased: 7.795
batch start
#iterations: 63
currently lose_sum: 515.896374642849
time_elpased: 7.807
batch start
#iterations: 64
currently lose_sum: 514.8196925222874
time_elpased: 7.808
batch start
#iterations: 65
currently lose_sum: 516.7791050970554
time_elpased: 7.807
batch start
#iterations: 66
currently lose_sum: 516.006519317627
time_elpased: 7.8
batch start
#iterations: 67
currently lose_sum: 514.7834239304066
time_elpased: 7.797
batch start
#iterations: 68
currently lose_sum: 517.7673914432526
time_elpased: 7.819
batch start
#iterations: 69
currently lose_sum: 516.839039772749
time_elpased: 7.792
batch start
#iterations: 70
currently lose_sum: 515.535750567913
time_elpased: 7.766
batch start
#iterations: 71
currently lose_sum: 515.4098751246929
time_elpased: 7.803
batch start
#iterations: 72
currently lose_sum: 516.8899474143982
time_elpased: 7.772
batch start
#iterations: 73
currently lose_sum: 515.7810087800026
time_elpased: 7.728
batch start
#iterations: 74
currently lose_sum: 516.4178120791912
time_elpased: 7.721
batch start
#iterations: 75
currently lose_sum: 516.4629344642162
time_elpased: 7.779
batch start
#iterations: 76
currently lose_sum: 516.4747315645218
time_elpased: 7.763
batch start
#iterations: 77
currently lose_sum: 516.090577095747
time_elpased: 7.78
batch start
#iterations: 78
currently lose_sum: 514.6991463005543
time_elpased: 7.736
batch start
#iterations: 79
currently lose_sum: 517.3207294642925
time_elpased: 7.762
start validation test
0.223092783505
1.0
0.223092783505
0.364801078894
0
validation finish
batch start
#iterations: 80
currently lose_sum: 515.7828966677189
time_elpased: 7.789
batch start
#iterations: 81
currently lose_sum: 516.3099489212036
time_elpased: 7.762
batch start
#iterations: 82
currently lose_sum: 514.7468747496605
time_elpased: 7.73
batch start
#iterations: 83
currently lose_sum: 515.7728496789932
time_elpased: 7.773
batch start
#iterations: 84
currently lose_sum: 515.5052764713764
time_elpased: 7.742
batch start
#iterations: 85
currently lose_sum: 515.5165141224861
time_elpased: 7.767
batch start
#iterations: 86
currently lose_sum: 515.1506138145924
time_elpased: 7.735
batch start
#iterations: 87
currently lose_sum: 513.7135389745235
time_elpased: 7.768
batch start
#iterations: 88
currently lose_sum: 514.8915748000145
time_elpased: 7.769
batch start
#iterations: 89
currently lose_sum: 516.57661652565
time_elpased: 7.718
batch start
#iterations: 90
currently lose_sum: 514.9099783301353
time_elpased: 7.705
batch start
#iterations: 91
currently lose_sum: 513.7305091023445
time_elpased: 7.746
batch start
#iterations: 92
currently lose_sum: 515.4780061841011
time_elpased: 7.732
batch start
#iterations: 93
currently lose_sum: 515.6647502779961
time_elpased: 7.769
batch start
#iterations: 94
currently lose_sum: 514.1923258602619
time_elpased: 7.802
batch start
#iterations: 95
currently lose_sum: 515.7901492714882
time_elpased: 7.797
batch start
#iterations: 96
currently lose_sum: 516.7076198458672
time_elpased: 7.742
batch start
#iterations: 97
currently lose_sum: 514.6717530786991
time_elpased: 7.763
batch start
#iterations: 98
currently lose_sum: 515.7212692499161
time_elpased: 7.741
batch start
#iterations: 99
currently lose_sum: 515.7005041837692
time_elpased: 7.754
start validation test
0.16412371134
1.0
0.16412371134
0.281969535955
0
validation finish
batch start
#iterations: 100
currently lose_sum: 516.0807455480099
time_elpased: 7.748
batch start
#iterations: 101
currently lose_sum: 515.4558828175068
time_elpased: 7.742
batch start
#iterations: 102
currently lose_sum: 513.3063952028751
time_elpased: 7.798
batch start
#iterations: 103
currently lose_sum: 514.4189901649952
time_elpased: 7.783
batch start
#iterations: 104
currently lose_sum: 516.0755299627781
time_elpased: 7.771
batch start
#iterations: 105
currently lose_sum: 515.1562676727772
time_elpased: 7.742
batch start
#iterations: 106
currently lose_sum: 516.4763915836811
time_elpased: 7.773
batch start
#iterations: 107
currently lose_sum: 517.4818123281002
time_elpased: 7.77
batch start
#iterations: 108
currently lose_sum: 514.8257523775101
time_elpased: 7.796
batch start
#iterations: 109
currently lose_sum: 515.739842414856
time_elpased: 7.791
batch start
#iterations: 110
currently lose_sum: 515.2118916809559
time_elpased: 7.753
batch start
#iterations: 111
currently lose_sum: 517.9531908929348
time_elpased: 7.744
batch start
#iterations: 112
currently lose_sum: 515.2948876321316
time_elpased: 7.759
batch start
#iterations: 113
currently lose_sum: 514.8740474581718
time_elpased: 7.787
batch start
#iterations: 114
currently lose_sum: 514.8923648893833
time_elpased: 7.717
batch start
#iterations: 115
currently lose_sum: 515.301534563303
time_elpased: 7.694
batch start
#iterations: 116
currently lose_sum: 514.6864269375801
time_elpased: 7.772
batch start
#iterations: 117
currently lose_sum: 514.3637451827526
time_elpased: 7.762
batch start
#iterations: 118
currently lose_sum: 516.156713783741
time_elpased: 7.747
batch start
#iterations: 119
currently lose_sum: 515.9126046001911
time_elpased: 7.779
start validation test
0.218556701031
1.0
0.218556701031
0.358714043993
0
validation finish
batch start
#iterations: 120
currently lose_sum: 515.3897175788879
time_elpased: 8.049
batch start
#iterations: 121
currently lose_sum: 515.2134149074554
time_elpased: 7.795
batch start
#iterations: 122
currently lose_sum: 514.3917524814606
time_elpased: 7.758
batch start
#iterations: 123
currently lose_sum: 514.8315082788467
time_elpased: 7.718
batch start
#iterations: 124
currently lose_sum: 515.8436331748962
time_elpased: 7.684
batch start
#iterations: 125
currently lose_sum: 517.1598377227783
time_elpased: 7.702
batch start
#iterations: 126
currently lose_sum: 514.7398234009743
time_elpased: 7.754
batch start
#iterations: 127
currently lose_sum: 517.7270560264587
time_elpased: 7.733
batch start
#iterations: 128
currently lose_sum: 516.0384471416473
time_elpased: 7.769
batch start
#iterations: 129
currently lose_sum: 516.0737542212009
time_elpased: 7.698
batch start
#iterations: 130
currently lose_sum: 515.2258945703506
time_elpased: 7.759
batch start
#iterations: 131
currently lose_sum: 515.3471719622612
time_elpased: 7.786
batch start
#iterations: 132
currently lose_sum: 514.745508491993
time_elpased: 7.754
batch start
#iterations: 133
currently lose_sum: 517.7852421402931
time_elpased: 7.742
batch start
#iterations: 134
currently lose_sum: 515.6368530988693
time_elpased: 7.781
batch start
#iterations: 135
currently lose_sum: 513.6691095232964
time_elpased: 7.769
batch start
#iterations: 136
currently lose_sum: 513.5380903482437
time_elpased: 7.737
batch start
#iterations: 137
currently lose_sum: 514.6922355294228
time_elpased: 7.721
batch start
#iterations: 138
currently lose_sum: 516.0813529789448
time_elpased: 7.658
batch start
#iterations: 139
currently lose_sum: 515.8942337334156
time_elpased: 7.703
start validation test
0.173298969072
1.0
0.173298969072
0.295404621738
0
validation finish
batch start
#iterations: 140
currently lose_sum: 512.7239067256451
time_elpased: 7.8
batch start
#iterations: 141
currently lose_sum: 514.6863598823547
time_elpased: 7.854
batch start
#iterations: 142
currently lose_sum: 514.6609150469303
time_elpased: 7.798
batch start
#iterations: 143
currently lose_sum: 516.0197443366051
time_elpased: 7.751
batch start
#iterations: 144
currently lose_sum: 515.7970009446144
time_elpased: 7.786
batch start
#iterations: 145
currently lose_sum: 514.2231596112251
time_elpased: 7.742
batch start
#iterations: 146
currently lose_sum: 517.2688469588757
time_elpased: 7.805
batch start
#iterations: 147
currently lose_sum: 517.0408895015717
time_elpased: 7.795
batch start
#iterations: 148
currently lose_sum: 513.9917801618576
time_elpased: 7.824
batch start
#iterations: 149
currently lose_sum: 517.2560344040394
time_elpased: 7.854
batch start
#iterations: 150
currently lose_sum: 514.0333386063576
time_elpased: 7.879
batch start
#iterations: 151
currently lose_sum: 513.5688599348068
time_elpased: 7.855
batch start
#iterations: 152
currently lose_sum: 514.3847890198231
time_elpased: 7.835
batch start
#iterations: 153
currently lose_sum: 513.8539654612541
time_elpased: 7.793
batch start
#iterations: 154
currently lose_sum: 518.2308628261089
time_elpased: 7.795
batch start
#iterations: 155
currently lose_sum: 513.9384118914604
time_elpased: 7.79
batch start
#iterations: 156
currently lose_sum: 516.2575298547745
time_elpased: 7.8
batch start
#iterations: 157
currently lose_sum: 515.3497958183289
time_elpased: 7.776
batch start
#iterations: 158
currently lose_sum: 513.4122915565968
time_elpased: 7.744
batch start
#iterations: 159
currently lose_sum: 516.4632371366024
time_elpased: 7.739
start validation test
0.188659793814
1.0
0.188659793814
0.317432784042
0
validation finish
batch start
#iterations: 160
currently lose_sum: 516.5466273725033
time_elpased: 7.713
batch start
#iterations: 161
currently lose_sum: 516.0664973258972
time_elpased: 7.654
batch start
#iterations: 162
currently lose_sum: 513.7544249594212
time_elpased: 7.765
batch start
#iterations: 163
currently lose_sum: 512.6846900582314
time_elpased: 7.827
batch start
#iterations: 164
currently lose_sum: 516.7582624256611
time_elpased: 7.776
batch start
#iterations: 165
currently lose_sum: 516.5544199645519
time_elpased: 7.728
batch start
#iterations: 166
currently lose_sum: 517.1182679235935
time_elpased: 7.777
batch start
#iterations: 167
currently lose_sum: 515.4966654777527
time_elpased: 7.76
batch start
#iterations: 168
currently lose_sum: 515.7314103245735
time_elpased: 7.798
batch start
#iterations: 169
currently lose_sum: 516.6922810673714
time_elpased: 7.837
batch start
#iterations: 170
currently lose_sum: 515.2597034275532
time_elpased: 7.874
batch start
#iterations: 171
currently lose_sum: 514.026116669178
time_elpased: 7.876
batch start
#iterations: 172
currently lose_sum: 517.7618634402752
time_elpased: 7.867
batch start
#iterations: 173
currently lose_sum: 516.2054715156555
time_elpased: 7.832
batch start
#iterations: 174
currently lose_sum: 515.9093042016029
time_elpased: 7.841
batch start
#iterations: 175
currently lose_sum: 514.8277653455734
time_elpased: 7.933
batch start
#iterations: 176
currently lose_sum: 515.8577353954315
time_elpased: 7.789
batch start
#iterations: 177
currently lose_sum: 514.2324492037296
time_elpased: 7.826
batch start
#iterations: 178
currently lose_sum: 514.5839503705502
time_elpased: 7.793
batch start
#iterations: 179
currently lose_sum: 516.2723363935947
time_elpased: 7.808
start validation test
0.197525773196
1.0
0.197525773196
0.329889807163
0
validation finish
batch start
#iterations: 180
currently lose_sum: 516.0487170517445
time_elpased: 7.818
batch start
#iterations: 181
currently lose_sum: 516.5268358886242
time_elpased: 7.838
batch start
#iterations: 182
currently lose_sum: 515.051098048687
time_elpased: 7.775
batch start
#iterations: 183
currently lose_sum: 516.4213340580463
time_elpased: 7.777
batch start
#iterations: 184
currently lose_sum: 514.8066171705723
time_elpased: 7.745
batch start
#iterations: 185
currently lose_sum: 514.5638209581375
time_elpased: 7.704
batch start
#iterations: 186
currently lose_sum: 517.0223578810692
time_elpased: 7.725
batch start
#iterations: 187
currently lose_sum: 514.8040620684624
time_elpased: 7.729
batch start
#iterations: 188
currently lose_sum: 514.9923854768276
time_elpased: 7.707
batch start
#iterations: 189
currently lose_sum: 516.301258713007
time_elpased: 7.778
batch start
#iterations: 190
currently lose_sum: 514.3768879771233
time_elpased: 7.778
batch start
#iterations: 191
currently lose_sum: 516.6314308345318
time_elpased: 7.763
batch start
#iterations: 192
currently lose_sum: 514.7466118633747
time_elpased: 7.726
batch start
#iterations: 193
currently lose_sum: 514.5988526046276
time_elpased: 7.76
batch start
#iterations: 194
currently lose_sum: 516.6777465641499
time_elpased: 7.796
batch start
#iterations: 195
currently lose_sum: 515.0256013274193
time_elpased: 7.77
batch start
#iterations: 196
currently lose_sum: 514.6477060616016
time_elpased: 7.741
batch start
#iterations: 197
currently lose_sum: 515.9878122210503
time_elpased: 7.71
batch start
#iterations: 198
currently lose_sum: 515.2378724217415
time_elpased: 7.745
batch start
#iterations: 199
currently lose_sum: 513.7627267837524
time_elpased: 7.75
start validation test
0.192783505155
1.0
0.192783505155
0.323249783924
0
validation finish
batch start
#iterations: 200
currently lose_sum: 515.4606787264347
time_elpased: 7.772
batch start
#iterations: 201
currently lose_sum: 514.7858428359032
time_elpased: 7.739
batch start
#iterations: 202
currently lose_sum: 515.2826977074146
time_elpased: 7.809
batch start
#iterations: 203
currently lose_sum: 514.5999770462513
time_elpased: 7.837
batch start
#iterations: 204
currently lose_sum: 515.1750744283199
time_elpased: 7.824
batch start
#iterations: 205
currently lose_sum: 514.4908859431744
time_elpased: 7.843
batch start
#iterations: 206
currently lose_sum: 517.8568747937679
time_elpased: 7.813
batch start
#iterations: 207
currently lose_sum: 516.610242754221
time_elpased: 7.818
batch start
#iterations: 208
currently lose_sum: 514.4648678302765
time_elpased: 7.824
batch start
#iterations: 209
currently lose_sum: 514.441391736269
time_elpased: 8.038
batch start
#iterations: 210
currently lose_sum: 515.3119530081749
time_elpased: 7.735
batch start
#iterations: 211
currently lose_sum: 515.799696713686
time_elpased: 7.703
batch start
#iterations: 212
currently lose_sum: 515.5293433070183
time_elpased: 7.713
batch start
#iterations: 213
currently lose_sum: 514.0424028337002
time_elpased: 7.676
batch start
#iterations: 214
currently lose_sum: 515.4251838028431
time_elpased: 7.725
batch start
#iterations: 215
currently lose_sum: 515.8309093415737
time_elpased: 7.732
batch start
#iterations: 216
currently lose_sum: 515.3547556698322
time_elpased: 7.805
batch start
#iterations: 217
currently lose_sum: 516.0115920007229
time_elpased: 7.801
batch start
#iterations: 218
currently lose_sum: 515.2179351449013
time_elpased: 7.795
batch start
#iterations: 219
currently lose_sum: 516.6587899625301
time_elpased: 7.836
start validation test
0.206804123711
1.0
0.206804123711
0.342730223817
0
validation finish
batch start
#iterations: 220
currently lose_sum: 514.4902558624744
time_elpased: 7.762
batch start
#iterations: 221
currently lose_sum: 517.5009127557278
time_elpased: 7.793
batch start
#iterations: 222
currently lose_sum: 514.1940005719662
time_elpased: 7.777
batch start
#iterations: 223
currently lose_sum: 514.349747300148
time_elpased: 7.745
batch start
#iterations: 224
currently lose_sum: 517.1146241426468
time_elpased: 7.787
batch start
#iterations: 225
currently lose_sum: 516.9124098420143
time_elpased: 7.771
batch start
#iterations: 226
currently lose_sum: 515.9346702992916
time_elpased: 7.757
batch start
#iterations: 227
currently lose_sum: 515.7478505969048
time_elpased: 7.741
batch start
#iterations: 228
currently lose_sum: 512.9661300182343
time_elpased: 7.71
batch start
#iterations: 229
currently lose_sum: 516.875631570816
time_elpased: 7.726
batch start
#iterations: 230
currently lose_sum: 516.4953234791756
time_elpased: 7.72
batch start
#iterations: 231
currently lose_sum: 516.6570570766926
time_elpased: 7.77
batch start
#iterations: 232
currently lose_sum: 512.464779406786
time_elpased: 7.773
batch start
#iterations: 233
currently lose_sum: 513.11289909482
time_elpased: 7.712
batch start
#iterations: 234
currently lose_sum: 514.823913693428
time_elpased: 7.733
batch start
#iterations: 235
currently lose_sum: 515.4609159529209
time_elpased: 7.751
batch start
#iterations: 236
currently lose_sum: 515.4179372787476
time_elpased: 7.774
batch start
#iterations: 237
currently lose_sum: 516.3047646284103
time_elpased: 7.773
batch start
#iterations: 238
currently lose_sum: 513.8783333003521
time_elpased: 7.748
batch start
#iterations: 239
currently lose_sum: 513.166956871748
time_elpased: 7.736
start validation test
0.180515463918
1.0
0.180515463918
0.305824818793
0
validation finish
batch start
#iterations: 240
currently lose_sum: 512.9040297567844
time_elpased: 7.714
batch start
#iterations: 241
currently lose_sum: 514.1274830698967
time_elpased: 7.778
batch start
#iterations: 242
currently lose_sum: 512.8363516032696
time_elpased: 8.168
batch start
#iterations: 243
currently lose_sum: 515.1675045788288
time_elpased: 7.805
batch start
#iterations: 244
currently lose_sum: 515.4495932161808
time_elpased: 7.725
batch start
#iterations: 245
currently lose_sum: 515.281359910965
time_elpased: 7.731
batch start
#iterations: 246
currently lose_sum: 515.538110345602
time_elpased: 7.696
batch start
#iterations: 247
currently lose_sum: 515.6308569014072
time_elpased: 7.701
batch start
#iterations: 248
currently lose_sum: 515.7315596342087
time_elpased: 7.751
batch start
#iterations: 249
currently lose_sum: 515.9561469852924
time_elpased: 7.76
batch start
#iterations: 250
currently lose_sum: 514.5204503238201
time_elpased: 7.746
batch start
#iterations: 251
currently lose_sum: 515.3475696444511
time_elpased: 7.725
batch start
#iterations: 252
currently lose_sum: 515.2793202996254
time_elpased: 7.765
batch start
#iterations: 253
currently lose_sum: 513.2528418600559
time_elpased: 7.696
batch start
#iterations: 254
currently lose_sum: 517.5894457697868
time_elpased: 7.673
batch start
#iterations: 255
currently lose_sum: 515.7551562488079
time_elpased: 7.703
batch start
#iterations: 256
currently lose_sum: 514.8965068459511
time_elpased: 7.666
batch start
#iterations: 257
currently lose_sum: 513.6592752635479
time_elpased: 7.703
batch start
#iterations: 258
currently lose_sum: 515.1455363333225
time_elpased: 7.738
batch start
#iterations: 259
currently lose_sum: 516.0528534054756
time_elpased: 7.764
start validation test
0.206804123711
1.0
0.206804123711
0.342730223817
0
validation finish
batch start
#iterations: 260
currently lose_sum: 517.2014111876488
time_elpased: 7.721
batch start
#iterations: 261
currently lose_sum: 517.0449223816395
time_elpased: 7.711
batch start
#iterations: 262
currently lose_sum: 515.2299745678902
time_elpased: 7.762
batch start
#iterations: 263
currently lose_sum: 515.674094080925
time_elpased: 7.771
batch start
#iterations: 264
currently lose_sum: 514.7811053693295
time_elpased: 7.779
batch start
#iterations: 265
currently lose_sum: 514.5961463153362
time_elpased: 7.771
batch start
#iterations: 266
currently lose_sum: 515.4367387890816
time_elpased: 7.685
batch start
#iterations: 267
currently lose_sum: 514.5905048847198
time_elpased: 7.721
batch start
#iterations: 268
currently lose_sum: 516.5261546075344
time_elpased: 7.715
batch start
#iterations: 269
currently lose_sum: 515.745545655489
time_elpased: 7.756
batch start
#iterations: 270
currently lose_sum: 515.0220731496811
time_elpased: 7.729
batch start
#iterations: 271
currently lose_sum: 511.86820861697197
time_elpased: 7.748
batch start
#iterations: 272
currently lose_sum: 515.7117022573948
time_elpased: 7.705
batch start
#iterations: 273
currently lose_sum: 514.3273744285107
time_elpased: 7.762
batch start
#iterations: 274
currently lose_sum: 515.8025951981544
time_elpased: 7.702
batch start
#iterations: 275
currently lose_sum: 515.6985773444176
time_elpased: 7.765
batch start
#iterations: 276
currently lose_sum: 513.4529735744
time_elpased: 7.738
batch start
#iterations: 277
currently lose_sum: 517.1860868632793
time_elpased: 7.75
batch start
#iterations: 278
currently lose_sum: 515.7594265043736
time_elpased: 7.772
batch start
#iterations: 279
currently lose_sum: 515.1556795537472
time_elpased: 7.728
start validation test
0.194639175258
1.0
0.194639175258
0.325854332068
0
validation finish
batch start
#iterations: 280
currently lose_sum: 514.2116023004055
time_elpased: 7.788
batch start
#iterations: 281
currently lose_sum: 514.7277821004391
time_elpased: 7.8
batch start
#iterations: 282
currently lose_sum: 514.7102846503258
time_elpased: 7.754
batch start
#iterations: 283
currently lose_sum: 514.2654523849487
time_elpased: 7.746
batch start
#iterations: 284
currently lose_sum: 516.0270216166973
time_elpased: 7.727
batch start
#iterations: 285
currently lose_sum: 514.6381357312202
time_elpased: 7.756
batch start
#iterations: 286
currently lose_sum: 515.2960647642612
time_elpased: 7.726
batch start
#iterations: 287
currently lose_sum: 515.640270024538
time_elpased: 7.742
batch start
#iterations: 288
currently lose_sum: 515.9646703004837
time_elpased: 7.718
batch start
#iterations: 289
currently lose_sum: 515.5247210562229
time_elpased: 7.745
batch start
#iterations: 290
currently lose_sum: 513.5348995625973
time_elpased: 7.695
batch start
#iterations: 291
currently lose_sum: 516.6332573890686
time_elpased: 7.75
batch start
#iterations: 292
currently lose_sum: 515.0058566629887
time_elpased: 7.725
batch start
#iterations: 293
currently lose_sum: 514.5528404712677
time_elpased: 7.744
batch start
#iterations: 294
currently lose_sum: 514.6402299106121
time_elpased: 7.713
batch start
#iterations: 295
currently lose_sum: 515.8330866992474
time_elpased: 7.739
batch start
#iterations: 296
currently lose_sum: 512.3018398582935
time_elpased: 7.71
batch start
#iterations: 297
currently lose_sum: 515.1359307467937
time_elpased: 7.725
batch start
#iterations: 298
currently lose_sum: 514.5722520649433
time_elpased: 7.724
batch start
#iterations: 299
currently lose_sum: 514.0542140901089
time_elpased: 7.715
start validation test
0.21206185567
1.0
0.21206185567
0.349919197074
0
validation finish
batch start
#iterations: 300
currently lose_sum: 514.3705158233643
time_elpased: 7.707
batch start
#iterations: 301
currently lose_sum: 514.5978763997555
time_elpased: 7.689
batch start
#iterations: 302
currently lose_sum: 516.0772423446178
time_elpased: 7.71
batch start
#iterations: 303
currently lose_sum: 514.2857866883278
time_elpased: 7.723
batch start
#iterations: 304
currently lose_sum: 515.2797762155533
time_elpased: 7.688
batch start
#iterations: 305
currently lose_sum: 517.1039109826088
time_elpased: 7.743
batch start
#iterations: 306
currently lose_sum: 514.2458391785622
time_elpased: 7.748
batch start
#iterations: 307
currently lose_sum: 517.9168606698513
time_elpased: 7.782
batch start
#iterations: 308
currently lose_sum: 515.0610338449478
time_elpased: 7.723
batch start
#iterations: 309
currently lose_sum: 515.9948845207691
time_elpased: 7.75
batch start
#iterations: 310
currently lose_sum: 514.1323164403439
time_elpased: 7.717
batch start
#iterations: 311
currently lose_sum: 513.4441947340965
time_elpased: 7.746
batch start
#iterations: 312
currently lose_sum: 514.0470731854439
time_elpased: 7.684
batch start
#iterations: 313
currently lose_sum: 515.4936593472958
time_elpased: 7.673
batch start
#iterations: 314
currently lose_sum: 514.000171393156
time_elpased: 7.667
batch start
#iterations: 315
currently lose_sum: 515.4521515369415
time_elpased: 7.736
batch start
#iterations: 316
currently lose_sum: 514.9949436783791
time_elpased: 7.714
batch start
#iterations: 317
currently lose_sum: 514.0305254757404
time_elpased: 7.765
batch start
#iterations: 318
currently lose_sum: 517.1053282022476
time_elpased: 7.723
batch start
#iterations: 319
currently lose_sum: 515.7335641682148
time_elpased: 7.7
start validation test
0.195257731959
1.0
0.195257731959
0.326720717613
0
validation finish
batch start
#iterations: 320
currently lose_sum: 515.0156931877136
time_elpased: 7.718
batch start
#iterations: 321
currently lose_sum: 514.2563373148441
time_elpased: 7.764
batch start
#iterations: 322
currently lose_sum: 513.8191181123257
time_elpased: 7.798
batch start
#iterations: 323
currently lose_sum: 514.2209329009056
time_elpased: 7.941
batch start
#iterations: 324
currently lose_sum: 517.2424202859402
time_elpased: 7.714
batch start
#iterations: 325
currently lose_sum: 516.4150503873825
time_elpased: 7.704
batch start
#iterations: 326
currently lose_sum: 515.9423902928829
time_elpased: 7.691
batch start
#iterations: 327
currently lose_sum: 515.4748729467392
time_elpased: 7.689
batch start
#iterations: 328
currently lose_sum: 514.6406524479389
time_elpased: 7.673
batch start
#iterations: 329
currently lose_sum: 514.8956330418587
time_elpased: 7.718
batch start
#iterations: 330
currently lose_sum: 516.4546994566917
time_elpased: 7.684
batch start
#iterations: 331
currently lose_sum: 516.4450368881226
time_elpased: 7.686
batch start
#iterations: 332
currently lose_sum: 514.745004594326
time_elpased: 7.681
batch start
#iterations: 333
currently lose_sum: 515.7167064249516
time_elpased: 7.767
batch start
#iterations: 334
currently lose_sum: 515.4005492329597
time_elpased: 7.797
batch start
#iterations: 335
currently lose_sum: 513.3666428029537
time_elpased: 7.978
batch start
#iterations: 336
currently lose_sum: 515.9694817662239
time_elpased: 7.791
batch start
#iterations: 337
currently lose_sum: 514.5628997087479
time_elpased: 7.828
batch start
#iterations: 338
currently lose_sum: 516.4785597622395
time_elpased: 7.742
batch start
#iterations: 339
currently lose_sum: 514.6722028553486
time_elpased: 7.87
start validation test
0.209896907216
1.0
0.209896907216
0.3469665985
0
validation finish
batch start
#iterations: 340
currently lose_sum: 514.9390870034695
time_elpased: 7.755
batch start
#iterations: 341
currently lose_sum: 515.4128558039665
time_elpased: 7.723
batch start
#iterations: 342
currently lose_sum: 514.5803276598454
time_elpased: 7.734
batch start
#iterations: 343
currently lose_sum: 515.3173978626728
time_elpased: 7.688
batch start
#iterations: 344
currently lose_sum: 515.5777813494205
time_elpased: 7.741
batch start
#iterations: 345
currently lose_sum: 515.7218499779701
time_elpased: 7.793
batch start
#iterations: 346
currently lose_sum: 514.1442114412785
time_elpased: 7.844
batch start
#iterations: 347
currently lose_sum: 514.481806486845
time_elpased: 7.885
batch start
#iterations: 348
currently lose_sum: 515.3486974239349
time_elpased: 7.754
batch start
#iterations: 349
currently lose_sum: 515.4372161328793
time_elpased: 7.724
batch start
#iterations: 350
currently lose_sum: 514.698938369751
time_elpased: 7.73
batch start
#iterations: 351
currently lose_sum: 515.1040103435516
time_elpased: 7.722
batch start
#iterations: 352
currently lose_sum: 515.295172303915
time_elpased: 7.673
batch start
#iterations: 353
currently lose_sum: 516.8013311326504
time_elpased: 7.756
batch start
#iterations: 354
currently lose_sum: 513.6184345781803
time_elpased: 7.719
batch start
#iterations: 355
currently lose_sum: 515.2557352781296
time_elpased: 7.743
batch start
#iterations: 356
currently lose_sum: 514.9511751234531
time_elpased: 7.71
batch start
#iterations: 357
currently lose_sum: 515.0316644608974
time_elpased: 7.739
batch start
#iterations: 358
currently lose_sum: 515.4036830365658
time_elpased: 7.706
batch start
#iterations: 359
currently lose_sum: 515.3652566671371
time_elpased: 7.688
start validation test
0.187731958763
1.0
0.187731958763
0.316118392501
0
validation finish
batch start
#iterations: 360
currently lose_sum: 515.9973994791508
time_elpased: 7.727
batch start
#iterations: 361
currently lose_sum: 512.6794601976871
time_elpased: 7.734
batch start
#iterations: 362
currently lose_sum: 515.9953677654266
time_elpased: 7.729
batch start
#iterations: 363
currently lose_sum: 514.4100835323334
time_elpased: 7.721
batch start
#iterations: 364
currently lose_sum: 515.0517709851265
time_elpased: 7.741
batch start
#iterations: 365
currently lose_sum: 515.9061141908169
time_elpased: 7.971
batch start
#iterations: 366
currently lose_sum: 516.6575499176979
time_elpased: 8.184
batch start
#iterations: 367
currently lose_sum: 515.6044569313526
time_elpased: 7.773
batch start
#iterations: 368
currently lose_sum: 512.8756182789803
time_elpased: 7.748
batch start
#iterations: 369
currently lose_sum: 514.1353589892387
time_elpased: 7.799
batch start
#iterations: 370
currently lose_sum: 513.5584557056427
time_elpased: 7.797
batch start
#iterations: 371
currently lose_sum: 515.5782804191113
time_elpased: 7.769
batch start
#iterations: 372
currently lose_sum: 514.8887338638306
time_elpased: 7.776
batch start
#iterations: 373
currently lose_sum: 516.4202722907066
time_elpased: 7.741
batch start
#iterations: 374
currently lose_sum: 516.3907748758793
time_elpased: 7.721
batch start
#iterations: 375
currently lose_sum: 515.1842178702354
time_elpased: 7.741
batch start
#iterations: 376
currently lose_sum: 515.2933523356915
time_elpased: 7.694
batch start
#iterations: 377
currently lose_sum: 515.6565534770489
time_elpased: 7.772
batch start
#iterations: 378
currently lose_sum: 514.9099379479885
time_elpased: 7.752
batch start
#iterations: 379
currently lose_sum: 514.1299125254154
time_elpased: 7.732
start validation test
0.195051546392
1.0
0.195051546392
0.326432022084
0
validation finish
batch start
#iterations: 380
currently lose_sum: 515.6654430627823
time_elpased: 7.718
batch start
#iterations: 381
currently lose_sum: 514.3307964503765
time_elpased: 7.734
batch start
#iterations: 382
currently lose_sum: 515.6313015818596
time_elpased: 7.701
batch start
#iterations: 383
currently lose_sum: 513.1591922044754
time_elpased: 7.73
batch start
#iterations: 384
currently lose_sum: 514.8166897892952
time_elpased: 7.775
batch start
#iterations: 385
currently lose_sum: 513.1772159934044
time_elpased: 7.769
batch start
#iterations: 386
currently lose_sum: 514.3940049111843
time_elpased: 7.725
batch start
#iterations: 387
currently lose_sum: 514.8569430112839
time_elpased: 7.8
batch start
#iterations: 388
currently lose_sum: 514.9834126532078
time_elpased: 7.732
batch start
#iterations: 389
currently lose_sum: 513.6552403271198
time_elpased: 7.774
batch start
#iterations: 390
currently lose_sum: 512.8110518753529
time_elpased: 7.756
batch start
#iterations: 391
currently lose_sum: 514.7645537257195
time_elpased: 7.669
batch start
#iterations: 392
currently lose_sum: 514.8365483880043
time_elpased: 7.734
batch start
#iterations: 393
currently lose_sum: 515.2444951832294
time_elpased: 7.7
batch start
#iterations: 394
currently lose_sum: 513.9677146673203
time_elpased: 7.721
batch start
#iterations: 395
currently lose_sum: 513.6364393532276
time_elpased: 7.687
batch start
#iterations: 396
currently lose_sum: 514.5630455315113
time_elpased: 7.675
batch start
#iterations: 397
currently lose_sum: 516.0141534805298
time_elpased: 7.662
batch start
#iterations: 398
currently lose_sum: 514.3807659447193
time_elpased: 7.67
batch start
#iterations: 399
currently lose_sum: 516.083212852478
time_elpased: 7.666
start validation test
0.193092783505
1.0
0.193092783505
0.323684437916
0
validation finish
acc: 0.232
pre: 1.000
rec: 0.232
F1: 0.377
auc: 0.000
