start to construct graph
graph construct over
87453
epochs start
batch start
#iterations: 0
currently lose_sum: 542.9806832075119
time_elpased: 7.505
batch start
#iterations: 1
currently lose_sum: 533.0136409401894
time_elpased: 7.467
batch start
#iterations: 2
currently lose_sum: 527.2851209044456
time_elpased: 7.468
batch start
#iterations: 3
currently lose_sum: 525.323473662138
time_elpased: 7.487
batch start
#iterations: 4
currently lose_sum: 522.0639809072018
time_elpased: 7.498
batch start
#iterations: 5
currently lose_sum: 521.9772256314754
time_elpased: 7.492
batch start
#iterations: 6
currently lose_sum: 521.0682429075241
time_elpased: 7.473
batch start
#iterations: 7
currently lose_sum: 520.415000140667
time_elpased: 7.47
batch start
#iterations: 8
currently lose_sum: 521.1217926442623
time_elpased: 7.477
batch start
#iterations: 9
currently lose_sum: 519.4522396028042
time_elpased: 7.524
batch start
#iterations: 10
currently lose_sum: 518.3295905590057
time_elpased: 7.459
batch start
#iterations: 11
currently lose_sum: 519.161179870367
time_elpased: 7.432
batch start
#iterations: 12
currently lose_sum: 521.5130077898502
time_elpased: 7.436
batch start
#iterations: 13
currently lose_sum: 518.2942091822624
time_elpased: 7.433
batch start
#iterations: 14
currently lose_sum: 518.847140699625
time_elpased: 7.433
batch start
#iterations: 15
currently lose_sum: 517.8519150018692
time_elpased: 7.531
batch start
#iterations: 16
currently lose_sum: 518.5177285671234
time_elpased: 7.571
batch start
#iterations: 17
currently lose_sum: 519.4197126924992
time_elpased: 7.497
batch start
#iterations: 18
currently lose_sum: 519.0628913938999
time_elpased: 7.46
batch start
#iterations: 19
currently lose_sum: 517.625475436449
time_elpased: 7.445
start validation test
0.19824742268
1.0
0.19824742268
0.330895637959
0
validation finish
batch start
#iterations: 20
currently lose_sum: 519.6634301841259
time_elpased: 7.469
batch start
#iterations: 21
currently lose_sum: 517.0943357348442
time_elpased: 7.497
batch start
#iterations: 22
currently lose_sum: 518.8206914663315
time_elpased: 7.471
batch start
#iterations: 23
currently lose_sum: 517.7823468446732
time_elpased: 7.896
batch start
#iterations: 24
currently lose_sum: 518.0754242241383
time_elpased: 8.044
batch start
#iterations: 25
currently lose_sum: 516.1959636509418
time_elpased: 7.506
batch start
#iterations: 26
currently lose_sum: 516.9924949705601
time_elpased: 7.463
batch start
#iterations: 27
currently lose_sum: 518.3490028977394
time_elpased: 7.443
batch start
#iterations: 28
currently lose_sum: 516.290185302496
time_elpased: 7.44
batch start
#iterations: 29
currently lose_sum: 515.7329815924168
time_elpased: 7.432
batch start
#iterations: 30
currently lose_sum: 517.2086593806744
time_elpased: 7.414
batch start
#iterations: 31
currently lose_sum: 517.6442633569241
time_elpased: 7.42
batch start
#iterations: 32
currently lose_sum: 517.8233169019222
time_elpased: 7.435
batch start
#iterations: 33
currently lose_sum: 516.0358271896839
time_elpased: 7.395
batch start
#iterations: 34
currently lose_sum: 515.9529911875725
time_elpased: 7.691
batch start
#iterations: 35
currently lose_sum: 518.9871085584164
time_elpased: 7.568
batch start
#iterations: 36
currently lose_sum: 518.1201948523521
time_elpased: 7.469
batch start
#iterations: 37
currently lose_sum: 517.8461437523365
time_elpased: 7.63
batch start
#iterations: 38
currently lose_sum: 517.5091260671616
time_elpased: 7.512
batch start
#iterations: 39
currently lose_sum: 517.8837126791477
time_elpased: 7.45
start validation test
0.198659793814
1.0
0.198659793814
0.331469854649
0
validation finish
batch start
#iterations: 40
currently lose_sum: 516.7999319136143
time_elpased: 7.453
batch start
#iterations: 41
currently lose_sum: 518.0645730793476
time_elpased: 7.413
batch start
#iterations: 42
currently lose_sum: 517.7297838926315
time_elpased: 7.417
batch start
#iterations: 43
currently lose_sum: 517.7674372196198
time_elpased: 7.45
batch start
#iterations: 44
currently lose_sum: 515.755268394947
time_elpased: 7.461
batch start
#iterations: 45
currently lose_sum: 515.2635441422462
time_elpased: 7.417
batch start
#iterations: 46
currently lose_sum: 519.8349516689777
time_elpased: 7.42
batch start
#iterations: 47
currently lose_sum: 516.7933326661587
time_elpased: 7.458
batch start
#iterations: 48
currently lose_sum: 514.938225209713
time_elpased: 7.42
batch start
#iterations: 49
currently lose_sum: 516.8542510271072
time_elpased: 7.405
batch start
#iterations: 50
currently lose_sum: 515.9786015748978
time_elpased: 7.43
batch start
#iterations: 51
currently lose_sum: 518.441192150116
time_elpased: 7.413
batch start
#iterations: 52
currently lose_sum: 518.8005205392838
time_elpased: 7.436
batch start
#iterations: 53
currently lose_sum: 516.7633445560932
time_elpased: 7.442
batch start
#iterations: 54
currently lose_sum: 516.2486110925674
time_elpased: 7.448
batch start
#iterations: 55
currently lose_sum: 514.9766151607037
time_elpased: 7.434
batch start
#iterations: 56
currently lose_sum: 515.8977393805981
time_elpased: 7.454
batch start
#iterations: 57
currently lose_sum: 515.1684890389442
time_elpased: 7.445
batch start
#iterations: 58
currently lose_sum: 516.8152423501015
time_elpased: 7.396
batch start
#iterations: 59
currently lose_sum: 515.9816221296787
time_elpased: 7.42
start validation test
0.156288659794
1.0
0.156288659794
0.27032810271
0
validation finish
batch start
#iterations: 60
currently lose_sum: 517.1918502748013
time_elpased: 7.446
batch start
#iterations: 61
currently lose_sum: 516.1898197829723
time_elpased: 7.505
batch start
#iterations: 62
currently lose_sum: 515.360653847456
time_elpased: 7.444
batch start
#iterations: 63
currently lose_sum: 519.0131911337376
time_elpased: 7.421
batch start
#iterations: 64
currently lose_sum: 516.8887157142162
time_elpased: 7.407
batch start
#iterations: 65
currently lose_sum: 517.8443427979946
time_elpased: 7.444
batch start
#iterations: 66
currently lose_sum: 515.2644726932049
time_elpased: 7.451
batch start
#iterations: 67
currently lose_sum: 515.121349543333
time_elpased: 7.435
batch start
#iterations: 68
currently lose_sum: 514.4756712913513
time_elpased: 7.427
batch start
#iterations: 69
currently lose_sum: 515.9838878810406
time_elpased: 7.435
batch start
#iterations: 70
currently lose_sum: 516.6564654707909
time_elpased: 7.42
batch start
#iterations: 71
currently lose_sum: 516.370604544878
time_elpased: 7.424
batch start
#iterations: 72
currently lose_sum: 513.8676936924458
time_elpased: 7.45
batch start
#iterations: 73
currently lose_sum: 516.1967006623745
time_elpased: 7.481
batch start
#iterations: 74
currently lose_sum: 517.0264849662781
time_elpased: 7.433
batch start
#iterations: 75
currently lose_sum: 516.0744712650776
time_elpased: 7.432
batch start
#iterations: 76
currently lose_sum: 516.4429560303688
time_elpased: 7.444
batch start
#iterations: 77
currently lose_sum: 516.3652009963989
time_elpased: 7.467
batch start
#iterations: 78
currently lose_sum: 515.3149851262569
time_elpased: 7.44
batch start
#iterations: 79
currently lose_sum: 516.3227099180222
time_elpased: 7.457
start validation test
0.205463917526
1.0
0.205463917526
0.340887710596
0
validation finish
batch start
#iterations: 80
currently lose_sum: 515.5916728079319
time_elpased: 7.441
batch start
#iterations: 81
currently lose_sum: 517.8391496241093
time_elpased: 7.473
batch start
#iterations: 82
currently lose_sum: 516.8048579394817
time_elpased: 7.449
batch start
#iterations: 83
currently lose_sum: 514.1139758229256
time_elpased: 7.398
batch start
#iterations: 84
currently lose_sum: 515.0314563810825
time_elpased: 7.401
batch start
#iterations: 85
currently lose_sum: 518.1914008259773
time_elpased: 7.406
batch start
#iterations: 86
currently lose_sum: 517.1084186136723
time_elpased: 7.39
batch start
#iterations: 87
currently lose_sum: 517.5683647692204
time_elpased: 7.474
batch start
#iterations: 88
currently lose_sum: 516.3130225241184
time_elpased: 7.4
batch start
#iterations: 89
currently lose_sum: 515.2704735696316
time_elpased: 7.396
batch start
#iterations: 90
currently lose_sum: 516.1538845300674
time_elpased: 7.428
batch start
#iterations: 91
currently lose_sum: 514.5197296440601
time_elpased: 7.438
batch start
#iterations: 92
currently lose_sum: 514.4688954353333
time_elpased: 7.432
batch start
#iterations: 93
currently lose_sum: 513.6034115552902
time_elpased: 7.448
batch start
#iterations: 94
currently lose_sum: 516.5500401556492
time_elpased: 7.513
batch start
#iterations: 95
currently lose_sum: 514.7683981955051
time_elpased: 8.073
batch start
#iterations: 96
currently lose_sum: 516.6509473621845
time_elpased: 8.022
batch start
#iterations: 97
currently lose_sum: 518.0061348378658
time_elpased: 7.551
batch start
#iterations: 98
currently lose_sum: 513.8980687856674
time_elpased: 7.452
batch start
#iterations: 99
currently lose_sum: 516.3114448487759
time_elpased: 7.471
start validation test
0.178453608247
1.0
0.178453608247
0.302860642114
0
validation finish
batch start
#iterations: 100
currently lose_sum: 515.687687754631
time_elpased: 7.409
batch start
#iterations: 101
currently lose_sum: 515.5084762573242
time_elpased: 7.393
batch start
#iterations: 102
currently lose_sum: 517.0979234576225
time_elpased: 7.441
batch start
#iterations: 103
currently lose_sum: 513.7058827579021
time_elpased: 7.428
batch start
#iterations: 104
currently lose_sum: 514.4237250089645
time_elpased: 7.451
batch start
#iterations: 105
currently lose_sum: 514.7791437804699
time_elpased: 7.462
batch start
#iterations: 106
currently lose_sum: 516.6264960169792
time_elpased: 7.506
batch start
#iterations: 107
currently lose_sum: 516.1518773138523
time_elpased: 7.515
batch start
#iterations: 108
currently lose_sum: 514.8174882531166
time_elpased: 7.464
batch start
#iterations: 109
currently lose_sum: 517.5837421417236
time_elpased: 7.444
batch start
#iterations: 110
currently lose_sum: 515.6381939053535
time_elpased: 7.443
batch start
#iterations: 111
currently lose_sum: 517.3231338560581
time_elpased: 7.376
batch start
#iterations: 112
currently lose_sum: 514.3044013082981
time_elpased: 7.396
batch start
#iterations: 113
currently lose_sum: 515.8576922416687
time_elpased: 7.404
batch start
#iterations: 114
currently lose_sum: 516.645162820816
time_elpased: 7.409
batch start
#iterations: 115
currently lose_sum: 516.8488471806049
time_elpased: 7.423
batch start
#iterations: 116
currently lose_sum: 515.9559489190578
time_elpased: 7.387
batch start
#iterations: 117
currently lose_sum: 515.6594291329384
time_elpased: 7.401
batch start
#iterations: 118
currently lose_sum: 516.482199549675
time_elpased: 7.371
batch start
#iterations: 119
currently lose_sum: 514.0395337641239
time_elpased: 7.372
start validation test
0.170618556701
1.0
0.170618556701
0.291501541171
0
validation finish
batch start
#iterations: 120
currently lose_sum: 516.0582068860531
time_elpased: 7.499
batch start
#iterations: 121
currently lose_sum: 516.2168166041374
time_elpased: 7.603
batch start
#iterations: 122
currently lose_sum: 517.5032700896263
time_elpased: 7.576
batch start
#iterations: 123
currently lose_sum: 518.7433141767979
time_elpased: 7.422
batch start
#iterations: 124
currently lose_sum: 515.939174413681
time_elpased: 7.497
batch start
#iterations: 125
currently lose_sum: 513.7121556699276
time_elpased: 7.463
batch start
#iterations: 126
currently lose_sum: 513.8438373804092
time_elpased: 7.441
batch start
#iterations: 127
currently lose_sum: 515.2410815954208
time_elpased: 7.442
batch start
#iterations: 128
currently lose_sum: 516.4489968419075
time_elpased: 7.447
batch start
#iterations: 129
currently lose_sum: 515.3839116096497
time_elpased: 7.466
batch start
#iterations: 130
currently lose_sum: 514.3821170330048
time_elpased: 7.404
batch start
#iterations: 131
currently lose_sum: 515.6390352249146
time_elpased: 7.378
batch start
#iterations: 132
currently lose_sum: 514.9944081604481
time_elpased: 7.322
batch start
#iterations: 133
currently lose_sum: 515.1792817115784
time_elpased: 7.298
batch start
#iterations: 134
currently lose_sum: 514.869676887989
time_elpased: 7.317
batch start
#iterations: 135
currently lose_sum: 513.6880609691143
time_elpased: 7.279
batch start
#iterations: 136
currently lose_sum: 516.8215765058994
time_elpased: 7.297
batch start
#iterations: 137
currently lose_sum: 516.1637181043625
time_elpased: 7.334
batch start
#iterations: 138
currently lose_sum: 517.2776326835155
time_elpased: 7.403
batch start
#iterations: 139
currently lose_sum: 516.1892355680466
time_elpased: 7.441
start validation test
0.211237113402
1.0
0.211237113402
0.348795642182
0
validation finish
batch start
#iterations: 140
currently lose_sum: 515.2100691497326
time_elpased: 7.407
batch start
#iterations: 141
currently lose_sum: 514.3230346143246
time_elpased: 7.48
batch start
#iterations: 142
currently lose_sum: 515.3617097139359
time_elpased: 8.208
batch start
#iterations: 143
currently lose_sum: 515.186523348093
time_elpased: 7.966
batch start
#iterations: 144
currently lose_sum: 515.9485329091549
time_elpased: 7.451
batch start
#iterations: 145
currently lose_sum: 515.7952891588211
time_elpased: 7.427
batch start
#iterations: 146
currently lose_sum: 516.0881562530994
time_elpased: 7.52
batch start
#iterations: 147
currently lose_sum: 515.0729548931122
time_elpased: 7.466
batch start
#iterations: 148
currently lose_sum: 514.6273835003376
time_elpased: 7.472
batch start
#iterations: 149
currently lose_sum: 516.9702063500881
time_elpased: 7.499
batch start
#iterations: 150
currently lose_sum: 515.7131408452988
time_elpased: 7.518
batch start
#iterations: 151
currently lose_sum: 513.8477341532707
time_elpased: 7.51
batch start
#iterations: 152
currently lose_sum: 514.8083192408085
time_elpased: 7.528
batch start
#iterations: 153
currently lose_sum: 512.6182107031345
time_elpased: 7.553
batch start
#iterations: 154
currently lose_sum: 516.2599337697029
time_elpased: 7.529
batch start
#iterations: 155
currently lose_sum: 516.7581909894943
time_elpased: 7.601
batch start
#iterations: 156
currently lose_sum: 515.8829899430275
time_elpased: 7.569
batch start
#iterations: 157
currently lose_sum: 514.895985275507
time_elpased: 7.531
batch start
#iterations: 158
currently lose_sum: 515.9199907779694
time_elpased: 7.674
batch start
#iterations: 159
currently lose_sum: 514.0693470239639
time_elpased: 7.331
start validation test
0.197216494845
1.0
0.197216494845
0.329458365625
0
validation finish
batch start
#iterations: 160
currently lose_sum: 515.6401796042919
time_elpased: 7.302
batch start
#iterations: 161
currently lose_sum: 516.2307948470116
time_elpased: 7.286
batch start
#iterations: 162
currently lose_sum: 514.3419287502766
time_elpased: 7.319
batch start
#iterations: 163
currently lose_sum: 515.6624203622341
time_elpased: 7.304
batch start
#iterations: 164
currently lose_sum: 515.3807574808598
time_elpased: 7.341
batch start
#iterations: 165
currently lose_sum: 514.5086015760899
time_elpased: 7.294
batch start
#iterations: 166
currently lose_sum: 514.8380781710148
time_elpased: 7.283
batch start
#iterations: 167
currently lose_sum: 514.6540899574757
time_elpased: 7.278
batch start
#iterations: 168
currently lose_sum: 516.2201487123966
time_elpased: 7.318
batch start
#iterations: 169
currently lose_sum: 514.9606025218964
time_elpased: 7.343
batch start
#iterations: 170
currently lose_sum: 515.5603005290031
time_elpased: 7.327
batch start
#iterations: 171
currently lose_sum: 515.5577940940857
time_elpased: 7.282
batch start
#iterations: 172
currently lose_sum: 514.539537280798
time_elpased: 7.316
batch start
#iterations: 173
currently lose_sum: 513.5244092345238
time_elpased: 7.324
batch start
#iterations: 174
currently lose_sum: 515.6290498971939
time_elpased: 7.308
batch start
#iterations: 175
currently lose_sum: 513.6495599448681
time_elpased: 7.323
batch start
#iterations: 176
currently lose_sum: 515.3371331393719
time_elpased: 7.34
batch start
#iterations: 177
currently lose_sum: 515.9542337357998
time_elpased: 7.312
batch start
#iterations: 178
currently lose_sum: 515.0682360827923
time_elpased: 7.663
batch start
#iterations: 179
currently lose_sum: 517.7327193021774
time_elpased: 8.035
start validation test
0.195670103093
1.0
0.195670103093
0.327297809967
0
validation finish
batch start
#iterations: 180
currently lose_sum: 515.610416084528
time_elpased: 7.41
batch start
#iterations: 181
currently lose_sum: 516.2126997709274
time_elpased: 7.43
batch start
#iterations: 182
currently lose_sum: 515.0228609442711
time_elpased: 7.374
batch start
#iterations: 183
currently lose_sum: 515.1490784585476
time_elpased: 7.46
batch start
#iterations: 184
currently lose_sum: 514.998584985733
time_elpased: 7.482
batch start
#iterations: 185
currently lose_sum: 515.4940150678158
time_elpased: 7.504
batch start
#iterations: 186
currently lose_sum: 513.8469136059284
time_elpased: 7.478
batch start
#iterations: 187
currently lose_sum: 517.7504032254219
time_elpased: 7.526
batch start
#iterations: 188
currently lose_sum: 515.6540204882622
time_elpased: 7.48
batch start
#iterations: 189
currently lose_sum: 516.0153404474258
time_elpased: 7.472
batch start
#iterations: 190
currently lose_sum: 515.7741710245609
time_elpased: 7.449
batch start
#iterations: 191
currently lose_sum: 515.4682419598103
time_elpased: 7.459
batch start
#iterations: 192
currently lose_sum: 514.5058305561543
time_elpased: 7.447
batch start
#iterations: 193
currently lose_sum: 514.8054600954056
time_elpased: 7.442
batch start
#iterations: 194
currently lose_sum: 516.3339751064777
time_elpased: 7.449
batch start
#iterations: 195
currently lose_sum: 514.4688948094845
time_elpased: 7.835
batch start
#iterations: 196
currently lose_sum: 516.8502359986305
time_elpased: 7.967
batch start
#iterations: 197
currently lose_sum: 516.1643436551094
time_elpased: 8.027
batch start
#iterations: 198
currently lose_sum: 515.3903459310532
time_elpased: 7.889
batch start
#iterations: 199
currently lose_sum: 517.2750684916973
time_elpased: 7.444
start validation test
0.200103092784
1.0
0.200103092784
0.333476505455
0
validation finish
batch start
#iterations: 200
currently lose_sum: 512.1521684229374
time_elpased: 7.988
batch start
#iterations: 201
currently lose_sum: 515.5224924087524
time_elpased: 7.925
batch start
#iterations: 202
currently lose_sum: 513.4048333764076
time_elpased: 7.617
batch start
#iterations: 203
currently lose_sum: 516.0346830189228
time_elpased: 7.396
batch start
#iterations: 204
currently lose_sum: 515.6914429366589
time_elpased: 7.407
batch start
#iterations: 205
currently lose_sum: 514.7147453129292
time_elpased: 7.401
batch start
#iterations: 206
currently lose_sum: 512.514344394207
time_elpased: 7.346
batch start
#iterations: 207
currently lose_sum: 513.9119495749474
time_elpased: 7.359
batch start
#iterations: 208
currently lose_sum: 515.7467346191406
time_elpased: 7.347
batch start
#iterations: 209
currently lose_sum: 516.0179172456264
time_elpased: 7.33
batch start
#iterations: 210
currently lose_sum: 514.8810198009014
time_elpased: 8.043
batch start
#iterations: 211
currently lose_sum: 514.9337649345398
time_elpased: 7.557
batch start
#iterations: 212
currently lose_sum: 514.8232426047325
time_elpased: 7.286
batch start
#iterations: 213
currently lose_sum: 516.6533499956131
time_elpased: 7.318
batch start
#iterations: 214
currently lose_sum: 517.0184830129147
time_elpased: 7.342
batch start
#iterations: 215
currently lose_sum: 515.2307671308517
time_elpased: 7.326
batch start
#iterations: 216
currently lose_sum: 516.2046186029911
time_elpased: 7.485
batch start
#iterations: 217
currently lose_sum: 514.5835146307945
time_elpased: 7.647
batch start
#iterations: 218
currently lose_sum: 516.6501446664333
time_elpased: 8.128
batch start
#iterations: 219
currently lose_sum: 515.3046748042107
time_elpased: 8.008
start validation test
0.178453608247
1.0
0.178453608247
0.302860642114
0
validation finish
batch start
#iterations: 220
currently lose_sum: 515.9328611195087
time_elpased: 7.697
batch start
#iterations: 221
currently lose_sum: 516.6983095109463
time_elpased: 7.499
batch start
#iterations: 222
currently lose_sum: 515.3351000249386
time_elpased: 7.578
batch start
#iterations: 223
currently lose_sum: 511.62638410925865
time_elpased: 7.417
batch start
#iterations: 224
currently lose_sum: 517.3678813278675
time_elpased: 7.352
batch start
#iterations: 225
currently lose_sum: 515.6234259903431
time_elpased: 7.361
batch start
#iterations: 226
currently lose_sum: 516.4067647755146
time_elpased: 7.357
batch start
#iterations: 227
currently lose_sum: 513.3147391676903
time_elpased: 7.341
batch start
#iterations: 228
currently lose_sum: 514.7939632833004
time_elpased: 7.355
batch start
#iterations: 229
currently lose_sum: 516.4340257048607
time_elpased: 7.333
batch start
#iterations: 230
currently lose_sum: 515.2395172715187
time_elpased: 7.394
batch start
#iterations: 231
currently lose_sum: 519.379783898592
time_elpased: 7.371
batch start
#iterations: 232
currently lose_sum: 515.6432322859764
time_elpased: 7.381
batch start
#iterations: 233
currently lose_sum: 514.979773402214
time_elpased: 7.437
batch start
#iterations: 234
currently lose_sum: 514.970192939043
time_elpased: 7.414
batch start
#iterations: 235
currently lose_sum: 515.7039842903614
time_elpased: 7.42
batch start
#iterations: 236
currently lose_sum: 513.9655584692955
time_elpased: 7.413
batch start
#iterations: 237
currently lose_sum: 514.3649546802044
time_elpased: 7.363
batch start
#iterations: 238
currently lose_sum: 514.0039013624191
time_elpased: 7.378
batch start
#iterations: 239
currently lose_sum: 518.7475055754185
time_elpased: 7.394
start validation test
0.190721649485
1.0
0.190721649485
0.320346320346
0
validation finish
batch start
#iterations: 240
currently lose_sum: 516.0378229320049
time_elpased: 7.361
batch start
#iterations: 241
currently lose_sum: 515.6250828206539
time_elpased: 7.403
batch start
#iterations: 242
currently lose_sum: 514.8452349603176
time_elpased: 7.407
batch start
#iterations: 243
currently lose_sum: 514.8380616903305
time_elpased: 7.356
batch start
#iterations: 244
currently lose_sum: 517.4094496071339
time_elpased: 7.384
batch start
#iterations: 245
currently lose_sum: 515.1207458078861
time_elpased: 7.703
batch start
#iterations: 246
currently lose_sum: 515.6442463994026
time_elpased: 7.919
batch start
#iterations: 247
currently lose_sum: 516.5243926048279
time_elpased: 7.565
batch start
#iterations: 248
currently lose_sum: 513.7661487460136
time_elpased: 7.453
batch start
#iterations: 249
currently lose_sum: 516.4128674268723
time_elpased: 7.411
batch start
#iterations: 250
currently lose_sum: 515.8067868947983
time_elpased: 7.44
batch start
#iterations: 251
currently lose_sum: 514.8732390999794
time_elpased: 7.427
batch start
#iterations: 252
currently lose_sum: 515.3894489109516
time_elpased: 7.437
batch start
#iterations: 253
currently lose_sum: 514.5394277870655
time_elpased: 7.448
batch start
#iterations: 254
currently lose_sum: 517.0777209103107
time_elpased: 7.417
batch start
#iterations: 255
currently lose_sum: 514.7473868727684
time_elpased: 7.406
batch start
#iterations: 256
currently lose_sum: 515.8265180587769
time_elpased: 7.407
batch start
#iterations: 257
currently lose_sum: 513.9083431363106
time_elpased: 7.435
batch start
#iterations: 258
currently lose_sum: 513.0945783853531
time_elpased: 7.443
batch start
#iterations: 259
currently lose_sum: 515.5803276598454
time_elpased: 7.37
start validation test
0.211855670103
1.0
0.211855670103
0.349638451723
0
validation finish
batch start
#iterations: 260
currently lose_sum: 515.4483134150505
time_elpased: 7.408
batch start
#iterations: 261
currently lose_sum: 515.4126965105534
time_elpased: 7.347
batch start
#iterations: 262
currently lose_sum: 515.71536013484
time_elpased: 7.446
batch start
#iterations: 263
currently lose_sum: 515.5869394540787
time_elpased: 7.407
batch start
#iterations: 264
currently lose_sum: 516.1121208965778
time_elpased: 7.453
batch start
#iterations: 265
currently lose_sum: 517.2265074551105
time_elpased: 7.446
batch start
#iterations: 266
currently lose_sum: 515.9022848010063
time_elpased: 7.428
batch start
#iterations: 267
currently lose_sum: 515.3065582811832
time_elpased: 7.471
batch start
#iterations: 268
currently lose_sum: 512.8931228220463
time_elpased: 7.427
batch start
#iterations: 269
currently lose_sum: 513.816866248846
time_elpased: 7.478
batch start
#iterations: 270
currently lose_sum: 516.0190801918507
time_elpased: 7.429
batch start
#iterations: 271
currently lose_sum: 517.2500110566616
time_elpased: 7.362
batch start
#iterations: 272
currently lose_sum: 514.7867699563503
time_elpased: 7.395
batch start
#iterations: 273
currently lose_sum: 516.2843445837498
time_elpased: 7.436
batch start
#iterations: 274
currently lose_sum: 514.7886511981487
time_elpased: 7.43
batch start
#iterations: 275
currently lose_sum: 514.3724487125874
time_elpased: 7.475
batch start
#iterations: 276
currently lose_sum: 516.3519351780415
time_elpased: 7.463
batch start
#iterations: 277
currently lose_sum: 514.0894928276539
time_elpased: 7.43
batch start
#iterations: 278
currently lose_sum: 513.3743943274021
time_elpased: 7.412
batch start
#iterations: 279
currently lose_sum: 513.7345904707909
time_elpased: 7.447
start validation test
0.201134020619
1.0
0.201134020619
0.334906874946
0
validation finish
batch start
#iterations: 280
currently lose_sum: 516.5218847990036
time_elpased: 7.462
batch start
#iterations: 281
currently lose_sum: 514.9998879730701
time_elpased: 7.451
batch start
#iterations: 282
currently lose_sum: 516.8753536045551
time_elpased: 7.468
batch start
#iterations: 283
currently lose_sum: 515.2563743591309
time_elpased: 7.487
batch start
#iterations: 284
currently lose_sum: 514.2658539414406
time_elpased: 7.444
batch start
#iterations: 285
currently lose_sum: 513.821823567152
time_elpased: 7.436
batch start
#iterations: 286
currently lose_sum: 515.0976144969463
time_elpased: 7.476
batch start
#iterations: 287
currently lose_sum: 515.6901536881924
time_elpased: 7.414
batch start
#iterations: 288
currently lose_sum: 517.5664550364017
time_elpased: 7.439
batch start
#iterations: 289
currently lose_sum: 517.576088398695
time_elpased: 7.427
batch start
#iterations: 290
currently lose_sum: 513.8113606274128
time_elpased: 7.963
batch start
#iterations: 291
currently lose_sum: 513.299852848053
time_elpased: 7.662
batch start
#iterations: 292
currently lose_sum: 514.5991401076317
time_elpased: 8.143
batch start
#iterations: 293
currently lose_sum: 516.5507250130177
time_elpased: 7.491
batch start
#iterations: 294
currently lose_sum: 514.6232934892178
time_elpased: 7.483
batch start
#iterations: 295
currently lose_sum: 514.3623550236225
time_elpased: 7.426
batch start
#iterations: 296
currently lose_sum: 515.6160440146923
time_elpased: 7.478
batch start
#iterations: 297
currently lose_sum: 514.0199665427208
time_elpased: 7.439
batch start
#iterations: 298
currently lose_sum: 515.4095028042793
time_elpased: 7.47
batch start
#iterations: 299
currently lose_sum: 517.0053592026234
time_elpased: 7.46
start validation test
0.192577319588
1.0
0.192577319588
0.32295988935
0
validation finish
batch start
#iterations: 300
currently lose_sum: 515.3840890526772
time_elpased: 7.443
batch start
#iterations: 301
currently lose_sum: 515.3021908402443
time_elpased: 7.381
batch start
#iterations: 302
currently lose_sum: 516.6608645617962
time_elpased: 7.453
batch start
#iterations: 303
currently lose_sum: 515.1482811868191
time_elpased: 7.376
batch start
#iterations: 304
currently lose_sum: 513.373882651329
time_elpased: 7.444
batch start
#iterations: 305
currently lose_sum: 514.5322287976742
time_elpased: 7.409
batch start
#iterations: 306
currently lose_sum: 515.6638098061085
time_elpased: 7.43
batch start
#iterations: 307
currently lose_sum: 513.8043859899044
time_elpased: 7.424
batch start
#iterations: 308
currently lose_sum: 515.8529080152512
time_elpased: 7.422
batch start
#iterations: 309
currently lose_sum: 515.1957882344723
time_elpased: 7.399
batch start
#iterations: 310
currently lose_sum: 517.169074088335
time_elpased: 7.444
batch start
#iterations: 311
currently lose_sum: 514.3832240402699
time_elpased: 7.425
batch start
#iterations: 312
currently lose_sum: 512.640928208828
time_elpased: 7.426
batch start
#iterations: 313
currently lose_sum: 514.9420815706253
time_elpased: 7.437
batch start
#iterations: 314
currently lose_sum: 516.8924611508846
time_elpased: 7.439
batch start
#iterations: 315
currently lose_sum: 515.2657058238983
time_elpased: 7.479
batch start
#iterations: 316
currently lose_sum: 514.7369202673435
time_elpased: 7.455
batch start
#iterations: 317
currently lose_sum: 514.9809978306293
time_elpased: 7.465
batch start
#iterations: 318
currently lose_sum: 513.6953313350677
time_elpased: 7.426
batch start
#iterations: 319
currently lose_sum: 513.8770308792591
time_elpased: 7.463
start validation test
0.19587628866
1.0
0.19587628866
0.327586206897
0
validation finish
batch start
#iterations: 320
currently lose_sum: 517.3953923881054
time_elpased: 7.487
batch start
#iterations: 321
currently lose_sum: 514.2986816763878
time_elpased: 7.462
batch start
#iterations: 322
currently lose_sum: 512.1127122342587
time_elpased: 7.47
batch start
#iterations: 323
currently lose_sum: 516.063590079546
time_elpased: 7.454
batch start
#iterations: 324
currently lose_sum: 514.4207845628262
time_elpased: 7.507
batch start
#iterations: 325
currently lose_sum: 515.944010257721
time_elpased: 7.531
batch start
#iterations: 326
currently lose_sum: 514.9655552208424
time_elpased: 7.745
batch start
#iterations: 327
currently lose_sum: 514.5495069026947
time_elpased: 7.635
batch start
#iterations: 328
currently lose_sum: 516.554455190897
time_elpased: 7.435
batch start
#iterations: 329
currently lose_sum: 514.5661700367928
time_elpased: 7.408
batch start
#iterations: 330
currently lose_sum: 516.896782875061
time_elpased: 7.386
batch start
#iterations: 331
currently lose_sum: 514.4144267141819
time_elpased: 7.385
batch start
#iterations: 332
currently lose_sum: 515.5629116594791
time_elpased: 7.39
batch start
#iterations: 333
currently lose_sum: 514.708301961422
time_elpased: 7.415
batch start
#iterations: 334
currently lose_sum: 513.9998206794262
time_elpased: 7.376
batch start
#iterations: 335
currently lose_sum: 513.7577883899212
time_elpased: 7.375
batch start
#iterations: 336
currently lose_sum: 515.6542627513409
time_elpased: 7.404
batch start
#iterations: 337
currently lose_sum: 515.1813783347607
time_elpased: 7.387
batch start
#iterations: 338
currently lose_sum: 515.2621038258076
time_elpased: 7.393
batch start
#iterations: 339
currently lose_sum: 516.2293099164963
time_elpased: 7.399
start validation test
0.195567010309
1.0
0.195567010309
0.3271535742
0
validation finish
batch start
#iterations: 340
currently lose_sum: 516.3759267926216
time_elpased: 7.398
batch start
#iterations: 341
currently lose_sum: 514.1704422235489
time_elpased: 7.388
batch start
#iterations: 342
currently lose_sum: 514.1506446897984
time_elpased: 7.371
batch start
#iterations: 343
currently lose_sum: 515.301106929779
time_elpased: 7.385
batch start
#iterations: 344
currently lose_sum: 517.4743468463421
time_elpased: 7.365
batch start
#iterations: 345
currently lose_sum: 517.2181126177311
time_elpased: 7.396
batch start
#iterations: 346
currently lose_sum: 512.1821597218513
time_elpased: 7.358
batch start
#iterations: 347
currently lose_sum: 514.9408845305443
time_elpased: 7.393
batch start
#iterations: 348
currently lose_sum: 514.8463861346245
time_elpased: 7.386
batch start
#iterations: 349
currently lose_sum: 515.1691125929356
time_elpased: 7.391
batch start
#iterations: 350
currently lose_sum: 515.9295060932636
time_elpased: 7.435
batch start
#iterations: 351
currently lose_sum: 515.8814212083817
time_elpased: 7.424
batch start
#iterations: 352
currently lose_sum: 516.7972259819508
time_elpased: 7.397
batch start
#iterations: 353
currently lose_sum: 516.1655013263226
time_elpased: 7.414
batch start
#iterations: 354
currently lose_sum: 517.7524072825909
time_elpased: 7.383
batch start
#iterations: 355
currently lose_sum: 515.9980973899364
time_elpased: 7.395
batch start
#iterations: 356
currently lose_sum: 514.36330768466
time_elpased: 7.382
batch start
#iterations: 357
currently lose_sum: 514.4833787381649
time_elpased: 7.402
batch start
#iterations: 358
currently lose_sum: 513.8843396306038
time_elpased: 7.46
batch start
#iterations: 359
currently lose_sum: 515.287884414196
time_elpased: 7.444
start validation test
0.200206185567
1.0
0.200206185567
0.333619652981
0
validation finish
batch start
#iterations: 360
currently lose_sum: 515.7236569225788
time_elpased: 7.489
batch start
#iterations: 361
currently lose_sum: 516.1336466670036
time_elpased: 7.471
batch start
#iterations: 362
currently lose_sum: 515.1965323984623
time_elpased: 7.48
batch start
#iterations: 363
currently lose_sum: 514.3974312841892
time_elpased: 7.391
batch start
#iterations: 364
currently lose_sum: 515.8849618732929
time_elpased: 7.392
batch start
#iterations: 365
currently lose_sum: 513.9178246259689
time_elpased: 7.398
batch start
#iterations: 366
currently lose_sum: 515.7658217549324
time_elpased: 7.444
batch start
#iterations: 367
currently lose_sum: 518.0555637776852
time_elpased: 7.833
batch start
#iterations: 368
currently lose_sum: 514.5641579031944
time_elpased: 7.849
batch start
#iterations: 369
currently lose_sum: 514.4532114565372
time_elpased: 7.575
batch start
#iterations: 370
currently lose_sum: 516.7080405950546
time_elpased: 7.541
batch start
#iterations: 371
currently lose_sum: 512.9393673837185
time_elpased: 7.458
batch start
#iterations: 372
currently lose_sum: 516.8900355696678
time_elpased: 7.484
batch start
#iterations: 373
currently lose_sum: 517.2248372733593
time_elpased: 7.438
batch start
#iterations: 374
currently lose_sum: 513.9990030229092
time_elpased: 7.642
batch start
#iterations: 375
currently lose_sum: 516.4133778512478
time_elpased: 7.508
batch start
#iterations: 376
currently lose_sum: 514.7829952538013
time_elpased: 7.368
batch start
#iterations: 377
currently lose_sum: 514.7583261728287
time_elpased: 7.405
batch start
#iterations: 378
currently lose_sum: 514.903911203146
time_elpased: 7.394
batch start
#iterations: 379
currently lose_sum: 514.161449700594
time_elpased: 7.357
start validation test
0.198556701031
1.0
0.198556701031
0.331326337519
0
validation finish
batch start
#iterations: 380
currently lose_sum: 515.2146808207035
time_elpased: 7.384
batch start
#iterations: 381
currently lose_sum: 514.3667946457863
time_elpased: 7.358
batch start
#iterations: 382
currently lose_sum: 512.6929411888123
time_elpased: 7.446
batch start
#iterations: 383
currently lose_sum: 513.9442964792252
time_elpased: 7.44
batch start
#iterations: 384
currently lose_sum: 515.6518514752388
time_elpased: 7.397
batch start
#iterations: 385
currently lose_sum: 516.1310960054398
time_elpased: 7.429
batch start
#iterations: 386
currently lose_sum: 517.9867753088474
time_elpased: 7.513
batch start
#iterations: 387
currently lose_sum: 514.4035463929176
time_elpased: 7.521
batch start
#iterations: 388
currently lose_sum: 514.6257200241089
time_elpased: 7.527
batch start
#iterations: 389
currently lose_sum: 515.1008671820164
time_elpased: 7.547
batch start
#iterations: 390
currently lose_sum: 515.9096770584583
time_elpased: 7.521
batch start
#iterations: 391
currently lose_sum: 513.8768865764141
time_elpased: 7.439
batch start
#iterations: 392
currently lose_sum: 515.1346017718315
time_elpased: 7.42
batch start
#iterations: 393
currently lose_sum: 516.4632665216923
time_elpased: 7.407
batch start
#iterations: 394
currently lose_sum: 514.5335608720779
time_elpased: 7.41
batch start
#iterations: 395
currently lose_sum: 516.0298531353474
time_elpased: 7.434
batch start
#iterations: 396
currently lose_sum: 514.2762133181095
time_elpased: 7.4
batch start
#iterations: 397
currently lose_sum: 515.9279518425465
time_elpased: 7.382
batch start
#iterations: 398
currently lose_sum: 513.9219164252281
time_elpased: 7.356
batch start
#iterations: 399
currently lose_sum: 516.5256596803665
time_elpased: 7.456
start validation test
0.193195876289
1.0
0.193195876289
0.323829272507
0
validation finish
acc: 0.209
pre: 1.000
rec: 0.209
F1: 0.345
auc: 0.000
