start to construct graph
graph construct over
87450
epochs start
batch start
#iterations: 0
currently lose_sum: 543.9164522886276
time_elpased: 9.906
batch start
#iterations: 1
currently lose_sum: 526.213601410389
time_elpased: 9.85
batch start
#iterations: 2
currently lose_sum: 519.1281523108482
time_elpased: 9.734
batch start
#iterations: 3
currently lose_sum: 512.4269259870052
time_elpased: 9.777
batch start
#iterations: 4
currently lose_sum: 509.602274119854
time_elpased: 9.779
batch start
#iterations: 5
currently lose_sum: 509.9056416749954
time_elpased: 9.821
batch start
#iterations: 6
currently lose_sum: 505.5290046930313
time_elpased: 9.801
batch start
#iterations: 7
currently lose_sum: 506.51032516360283
time_elpased: 9.752
batch start
#iterations: 8
currently lose_sum: 503.38261291384697
time_elpased: 9.793
batch start
#iterations: 9
currently lose_sum: 503.8151311278343
time_elpased: 9.824
batch start
#iterations: 10
currently lose_sum: 503.5914845764637
time_elpased: 9.802
batch start
#iterations: 11
currently lose_sum: 504.651109367609
time_elpased: 9.786
batch start
#iterations: 12
currently lose_sum: 501.33594423532486
time_elpased: 9.845
batch start
#iterations: 13
currently lose_sum: 501.48004204034805
time_elpased: 9.777
batch start
#iterations: 14
currently lose_sum: 499.29974484443665
time_elpased: 9.728
batch start
#iterations: 15
currently lose_sum: 500.23061940073967
time_elpased: 9.768
batch start
#iterations: 16
currently lose_sum: 501.73570600152016
time_elpased: 9.778
batch start
#iterations: 17
currently lose_sum: 499.98260521888733
time_elpased: 9.822
batch start
#iterations: 18
currently lose_sum: 499.03356471657753
time_elpased: 9.804
batch start
#iterations: 19
currently lose_sum: 498.40914195775986
time_elpased: 9.845
start validation test
0.229484536082
1.0
0.229484536082
0.37330202918
0
validation finish
batch start
#iterations: 20
currently lose_sum: 499.16516134142876
time_elpased: 9.807
batch start
#iterations: 21
currently lose_sum: 501.0937549471855
time_elpased: 9.855
batch start
#iterations: 22
currently lose_sum: 499.6618323326111
time_elpased: 9.816
batch start
#iterations: 23
currently lose_sum: 498.0854332149029
time_elpased: 9.785
batch start
#iterations: 24
currently lose_sum: 497.31526589393616
time_elpased: 9.868
batch start
#iterations: 25
currently lose_sum: 498.829173207283
time_elpased: 9.79
batch start
#iterations: 26
currently lose_sum: 498.46133893728256
time_elpased: 9.765
batch start
#iterations: 27
currently lose_sum: 498.3211789429188
time_elpased: 9.757
batch start
#iterations: 28
currently lose_sum: 497.4500148296356
time_elpased: 9.717
batch start
#iterations: 29
currently lose_sum: 498.07451406121254
time_elpased: 9.811
batch start
#iterations: 30
currently lose_sum: 496.83344453573227
time_elpased: 9.743
batch start
#iterations: 31
currently lose_sum: 496.89398112893105
time_elpased: 9.804
batch start
#iterations: 32
currently lose_sum: 498.2384105324745
time_elpased: 9.745
batch start
#iterations: 33
currently lose_sum: 495.51214241981506
time_elpased: 9.833
batch start
#iterations: 34
currently lose_sum: 497.57511964440346
time_elpased: 9.799
batch start
#iterations: 35
currently lose_sum: 495.42672047019005
time_elpased: 9.873
batch start
#iterations: 36
currently lose_sum: 496.94689363241196
time_elpased: 9.207
batch start
#iterations: 37
currently lose_sum: 496.49749851226807
time_elpased: 9.856
batch start
#iterations: 38
currently lose_sum: 496.61980843544006
time_elpased: 9.736
batch start
#iterations: 39
currently lose_sum: 498.60194420814514
time_elpased: 9.785
start validation test
0.187113402062
1.0
0.187113402062
0.315240990013
0
validation finish
batch start
#iterations: 40
currently lose_sum: 496.0715053677559
time_elpased: 9.815
batch start
#iterations: 41
currently lose_sum: 496.3862497508526
time_elpased: 9.84
batch start
#iterations: 42
currently lose_sum: 494.8843823969364
time_elpased: 9.883
batch start
#iterations: 43
currently lose_sum: 496.081678211689
time_elpased: 9.782
batch start
#iterations: 44
currently lose_sum: 495.5055555701256
time_elpased: 9.746
batch start
#iterations: 45
currently lose_sum: 496.49001890420914
time_elpased: 9.786
batch start
#iterations: 46
currently lose_sum: 494.9370846748352
time_elpased: 9.78
batch start
#iterations: 47
currently lose_sum: 497.0992402136326
time_elpased: 9.85
batch start
#iterations: 48
currently lose_sum: 494.1726104617119
time_elpased: 9.75
batch start
#iterations: 49
currently lose_sum: 494.8288382291794
time_elpased: 9.799
batch start
#iterations: 50
currently lose_sum: 497.399560213089
time_elpased: 9.834
batch start
#iterations: 51
currently lose_sum: 495.6706104874611
time_elpased: 9.813
batch start
#iterations: 52
currently lose_sum: 494.911841571331
time_elpased: 9.761
batch start
#iterations: 53
currently lose_sum: 495.86853355169296
time_elpased: 9.777
batch start
#iterations: 54
currently lose_sum: 494.56822767853737
time_elpased: 9.786
batch start
#iterations: 55
currently lose_sum: 495.17688968777657
time_elpased: 9.771
batch start
#iterations: 56
currently lose_sum: 496.4751387536526
time_elpased: 9.776
batch start
#iterations: 57
currently lose_sum: 493.7985783815384
time_elpased: 9.87
batch start
#iterations: 58
currently lose_sum: 493.7986332476139
time_elpased: 9.846
batch start
#iterations: 59
currently lose_sum: 493.3456007242203
time_elpased: 9.827
start validation test
0.319484536082
1.0
0.319484536082
0.484256582546
0
validation finish
batch start
#iterations: 60
currently lose_sum: 495.12122762203217
time_elpased: 9.789
batch start
#iterations: 61
currently lose_sum: 495.32680198550224
time_elpased: 9.756
batch start
#iterations: 62
currently lose_sum: 496.11298301815987
time_elpased: 9.858
batch start
#iterations: 63
currently lose_sum: 493.47814774513245
time_elpased: 9.746
batch start
#iterations: 64
currently lose_sum: 494.0453001856804
time_elpased: 9.76
batch start
#iterations: 65
currently lose_sum: 494.87155455350876
time_elpased: 9.749
batch start
#iterations: 66
currently lose_sum: 494.286562025547
time_elpased: 9.77
batch start
#iterations: 67
currently lose_sum: 493.3130835592747
time_elpased: 9.8
batch start
#iterations: 68
currently lose_sum: 496.47831720113754
time_elpased: 9.847
batch start
#iterations: 69
currently lose_sum: 494.57296311855316
time_elpased: 9.774
batch start
#iterations: 70
currently lose_sum: 494.9259215593338
time_elpased: 9.793
batch start
#iterations: 71
currently lose_sum: 494.02883341908455
time_elpased: 9.775
batch start
#iterations: 72
currently lose_sum: 495.463395178318
time_elpased: 9.816
batch start
#iterations: 73
currently lose_sum: 494.83064767718315
time_elpased: 9.834
batch start
#iterations: 74
currently lose_sum: 494.26501527428627
time_elpased: 9.753
batch start
#iterations: 75
currently lose_sum: 494.41139113903046
time_elpased: 9.805
batch start
#iterations: 76
currently lose_sum: 495.12620067596436
time_elpased: 9.758
batch start
#iterations: 77
currently lose_sum: 493.8101954162121
time_elpased: 9.773
batch start
#iterations: 78
currently lose_sum: 493.0194002687931
time_elpased: 9.802
batch start
#iterations: 79
currently lose_sum: 496.1240673661232
time_elpased: 9.7
start validation test
0.317216494845
1.0
0.317216494845
0.48164670893
0
validation finish
batch start
#iterations: 80
currently lose_sum: 493.21410262584686
time_elpased: 9.748
batch start
#iterations: 81
currently lose_sum: 493.95725706219673
time_elpased: 9.758
batch start
#iterations: 82
currently lose_sum: 493.9840229153633
time_elpased: 9.821
batch start
#iterations: 83
currently lose_sum: 494.4732664525509
time_elpased: 9.737
batch start
#iterations: 84
currently lose_sum: 493.9325051307678
time_elpased: 9.86
batch start
#iterations: 85
currently lose_sum: 493.7813293635845
time_elpased: 10.504
batch start
#iterations: 86
currently lose_sum: 495.2073695063591
time_elpased: 10.942
batch start
#iterations: 87
currently lose_sum: 492.40595239400864
time_elpased: 10.967
batch start
#iterations: 88
currently lose_sum: 492.5307143032551
time_elpased: 10.048
batch start
#iterations: 89
currently lose_sum: 496.1363155543804
time_elpased: 9.787
batch start
#iterations: 90
currently lose_sum: 493.3356917798519
time_elpased: 9.826
batch start
#iterations: 91
currently lose_sum: 490.954986512661
time_elpased: 9.77
batch start
#iterations: 92
currently lose_sum: 493.8148013651371
time_elpased: 9.853
batch start
#iterations: 93
currently lose_sum: 493.97099193930626
time_elpased: 9.816
batch start
#iterations: 94
currently lose_sum: 492.5125008523464
time_elpased: 9.776
batch start
#iterations: 95
currently lose_sum: 492.7331575155258
time_elpased: 9.814
batch start
#iterations: 96
currently lose_sum: 494.3017894625664
time_elpased: 9.775
batch start
#iterations: 97
currently lose_sum: 492.8301878273487
time_elpased: 9.795
batch start
#iterations: 98
currently lose_sum: 493.50817570090294
time_elpased: 9.855
batch start
#iterations: 99
currently lose_sum: 493.8741542994976
time_elpased: 9.736
start validation test
0.140618556701
1.0
0.140618556701
0.246565437455
0
validation finish
batch start
#iterations: 100
currently lose_sum: 493.9743785262108
time_elpased: 9.814
batch start
#iterations: 101
currently lose_sum: 493.47242602705956
time_elpased: 9.799
batch start
#iterations: 102
currently lose_sum: 490.8363962471485
time_elpased: 9.757
batch start
#iterations: 103
currently lose_sum: 492.00339463353157
time_elpased: 9.8
batch start
#iterations: 104
currently lose_sum: 494.05434080958366
time_elpased: 9.782
batch start
#iterations: 105
currently lose_sum: 492.49322962760925
time_elpased: 9.816
batch start
#iterations: 106
currently lose_sum: 495.6372193992138
time_elpased: 9.749
batch start
#iterations: 107
currently lose_sum: 494.2710947096348
time_elpased: 9.759
batch start
#iterations: 108
currently lose_sum: 492.5624749958515
time_elpased: 9.826
batch start
#iterations: 109
currently lose_sum: 493.2951226234436
time_elpased: 9.81
batch start
#iterations: 110
currently lose_sum: 493.1636913716793
time_elpased: 9.866
batch start
#iterations: 111
currently lose_sum: 496.50918647646904
time_elpased: 9.784
batch start
#iterations: 112
currently lose_sum: 493.034564524889
time_elpased: 9.766
batch start
#iterations: 113
currently lose_sum: 492.7536722123623
time_elpased: 9.819
batch start
#iterations: 114
currently lose_sum: 492.20050954818726
time_elpased: 9.793
batch start
#iterations: 115
currently lose_sum: 494.04534152150154
time_elpased: 9.772
batch start
#iterations: 116
currently lose_sum: 493.40940088033676
time_elpased: 9.72
batch start
#iterations: 117
currently lose_sum: 492.12108889222145
time_elpased: 9.843
batch start
#iterations: 118
currently lose_sum: 493.6057717204094
time_elpased: 9.749
batch start
#iterations: 119
currently lose_sum: 493.4723743200302
time_elpased: 9.75
start validation test
0.267010309278
1.0
0.267010309278
0.421480878763
0
validation finish
batch start
#iterations: 120
currently lose_sum: 492.4053921699524
time_elpased: 9.783
batch start
#iterations: 121
currently lose_sum: 492.0611221194267
time_elpased: 9.812
batch start
#iterations: 122
currently lose_sum: 493.42467987537384
time_elpased: 9.796
batch start
#iterations: 123
currently lose_sum: 493.77486538887024
time_elpased: 9.815
batch start
#iterations: 124
currently lose_sum: 493.41997569799423
time_elpased: 9.762
batch start
#iterations: 125
currently lose_sum: 494.763242661953
time_elpased: 9.83
batch start
#iterations: 126
currently lose_sum: 491.6557636857033
time_elpased: 9.814
batch start
#iterations: 127
currently lose_sum: 495.5113676786423
time_elpased: 9.818
batch start
#iterations: 128
currently lose_sum: 493.9214354157448
time_elpased: 9.762
batch start
#iterations: 129
currently lose_sum: 492.50823321938515
time_elpased: 9.759
batch start
#iterations: 130
currently lose_sum: 492.78041410446167
time_elpased: 9.772
batch start
#iterations: 131
currently lose_sum: 492.6006692945957
time_elpased: 9.789
batch start
#iterations: 132
currently lose_sum: 493.1393177509308
time_elpased: 9.763
batch start
#iterations: 133
currently lose_sum: 495.87005376815796
time_elpased: 9.828
batch start
#iterations: 134
currently lose_sum: 492.98956567049026
time_elpased: 9.805
batch start
#iterations: 135
currently lose_sum: 491.08781987428665
time_elpased: 9.825
batch start
#iterations: 136
currently lose_sum: 491.9939084947109
time_elpased: 9.858
batch start
#iterations: 137
currently lose_sum: 491.58388021588326
time_elpased: 9.82
batch start
#iterations: 138
currently lose_sum: 493.36383906006813
time_elpased: 9.764
batch start
#iterations: 139
currently lose_sum: 492.9775108397007
time_elpased: 9.815
start validation test
0.2
1.0
0.2
0.333333333333
0
validation finish
batch start
#iterations: 140
currently lose_sum: 490.65461564064026
time_elpased: 9.946
batch start
#iterations: 141
currently lose_sum: 490.9974054992199
time_elpased: 9.153
batch start
#iterations: 142
currently lose_sum: 492.6484751999378
time_elpased: 9.86
batch start
#iterations: 143
currently lose_sum: 492.6243149638176
time_elpased: 9.754
batch start
#iterations: 144
currently lose_sum: 494.4627782702446
time_elpased: 9.776
batch start
#iterations: 145
currently lose_sum: 491.3704175055027
time_elpased: 9.801
batch start
#iterations: 146
currently lose_sum: 494.33438447117805
time_elpased: 9.872
batch start
#iterations: 147
currently lose_sum: 493.8568988740444
time_elpased: 9.774
batch start
#iterations: 148
currently lose_sum: 491.2743577361107
time_elpased: 9.727
batch start
#iterations: 149
currently lose_sum: 493.84228003025055
time_elpased: 9.749
batch start
#iterations: 150
currently lose_sum: 491.0393759608269
time_elpased: 9.85
batch start
#iterations: 151
currently lose_sum: 489.9536990225315
time_elpased: 9.766
batch start
#iterations: 152
currently lose_sum: 492.89414551854134
time_elpased: 9.762
batch start
#iterations: 153
currently lose_sum: 490.31242802739143
time_elpased: 9.892
batch start
#iterations: 154
currently lose_sum: 495.5915987789631
time_elpased: 9.846
batch start
#iterations: 155
currently lose_sum: 492.21301370859146
time_elpased: 9.8
batch start
#iterations: 156
currently lose_sum: 492.4582735300064
time_elpased: 9.806
batch start
#iterations: 157
currently lose_sum: 491.7763266861439
time_elpased: 9.776
batch start
#iterations: 158
currently lose_sum: 491.1971747279167
time_elpased: 9.852
batch start
#iterations: 159
currently lose_sum: 493.37506082654
time_elpased: 9.788
start validation test
0.20793814433
1.0
0.20793814433
0.344286080055
0
validation finish
batch start
#iterations: 160
currently lose_sum: 493.52999621629715
time_elpased: 9.805
batch start
#iterations: 161
currently lose_sum: 493.3695600628853
time_elpased: 9.901
batch start
#iterations: 162
currently lose_sum: 490.68001371622086
time_elpased: 9.811
batch start
#iterations: 163
currently lose_sum: 490.09070959687233
time_elpased: 9.765
batch start
#iterations: 164
currently lose_sum: 493.0175215601921
time_elpased: 9.851
batch start
#iterations: 165
currently lose_sum: 493.7966718971729
time_elpased: 9.77
batch start
#iterations: 166
currently lose_sum: 493.99840447306633
time_elpased: 9.803
batch start
#iterations: 167
currently lose_sum: 492.2337514460087
time_elpased: 9.808
batch start
#iterations: 168
currently lose_sum: 492.7654584944248
time_elpased: 9.778
batch start
#iterations: 169
currently lose_sum: 491.57036739587784
time_elpased: 9.776
batch start
#iterations: 170
currently lose_sum: 492.1217843592167
time_elpased: 9.863
batch start
#iterations: 171
currently lose_sum: 491.3081297278404
time_elpased: 9.76
batch start
#iterations: 172
currently lose_sum: 495.29669892787933
time_elpased: 9.799
batch start
#iterations: 173
currently lose_sum: 493.52379181981087
time_elpased: 9.76
batch start
#iterations: 174
currently lose_sum: 492.86052909493446
time_elpased: 9.763
batch start
#iterations: 175
currently lose_sum: 491.75222185254097
time_elpased: 9.761
batch start
#iterations: 176
currently lose_sum: 492.5081546008587
time_elpased: 9.756
batch start
#iterations: 177
currently lose_sum: 491.92347517609596
time_elpased: 9.778
batch start
#iterations: 178
currently lose_sum: 492.084727615118
time_elpased: 9.793
batch start
#iterations: 179
currently lose_sum: 492.9373064637184
time_elpased: 9.71
start validation test
0.202680412371
1.0
0.202680412371
0.337047831305
0
validation finish
batch start
#iterations: 180
currently lose_sum: 492.32849568128586
time_elpased: 9.777
batch start
#iterations: 181
currently lose_sum: 493.0144840180874
time_elpased: 9.87
batch start
#iterations: 182
currently lose_sum: 492.68598341941833
time_elpased: 9.733
batch start
#iterations: 183
currently lose_sum: 492.24053049087524
time_elpased: 9.752
batch start
#iterations: 184
currently lose_sum: 491.62641111016273
time_elpased: 9.747
batch start
#iterations: 185
currently lose_sum: 491.690326243639
time_elpased: 9.842
batch start
#iterations: 186
currently lose_sum: 493.30381658673286
time_elpased: 9.838
batch start
#iterations: 187
currently lose_sum: 490.4300439655781
time_elpased: 9.785
batch start
#iterations: 188
currently lose_sum: 491.82642313838005
time_elpased: 9.771
batch start
#iterations: 189
currently lose_sum: 493.1487590074539
time_elpased: 9.755
batch start
#iterations: 190
currently lose_sum: 491.9118476510048
time_elpased: 9.772
batch start
#iterations: 191
currently lose_sum: 493.7319819331169
time_elpased: 9.843
batch start
#iterations: 192
currently lose_sum: 491.4750514626503
time_elpased: 9.745
batch start
#iterations: 193
currently lose_sum: 491.84155571460724
time_elpased: 9.783
batch start
#iterations: 194
currently lose_sum: 493.5020943582058
time_elpased: 9.758
batch start
#iterations: 195
currently lose_sum: 492.1025684773922
time_elpased: 9.77
batch start
#iterations: 196
currently lose_sum: 490.50552240014076
time_elpased: 9.79
batch start
#iterations: 197
currently lose_sum: 493.2961963415146
time_elpased: 9.84
batch start
#iterations: 198
currently lose_sum: 491.5520938038826
time_elpased: 9.844
batch start
#iterations: 199
currently lose_sum: 489.69622617959976
time_elpased: 9.781
start validation test
0.230927835052
1.0
0.230927835052
0.375209380235
0
validation finish
batch start
#iterations: 200
currently lose_sum: 491.72543528676033
time_elpased: 9.799
batch start
#iterations: 201
currently lose_sum: 490.8159776031971
time_elpased: 9.797
batch start
#iterations: 202
currently lose_sum: 491.68360993266106
time_elpased: 9.771
batch start
#iterations: 203
currently lose_sum: 491.3953337073326
time_elpased: 9.842
batch start
#iterations: 204
currently lose_sum: 491.4351316988468
time_elpased: 9.795
batch start
#iterations: 205
currently lose_sum: 491.9387874305248
time_elpased: 9.741
batch start
#iterations: 206
currently lose_sum: 494.847093552351
time_elpased: 9.808
batch start
#iterations: 207
currently lose_sum: 492.7396588027477
time_elpased: 9.804
batch start
#iterations: 208
currently lose_sum: 491.0963915884495
time_elpased: 9.809
batch start
#iterations: 209
currently lose_sum: 491.22227269411087
time_elpased: 9.891
batch start
#iterations: 210
currently lose_sum: 491.16546285152435
time_elpased: 9.81
batch start
#iterations: 211
currently lose_sum: 492.36576279997826
time_elpased: 9.81
batch start
#iterations: 212
currently lose_sum: 491.99799275398254
time_elpased: 9.829
batch start
#iterations: 213
currently lose_sum: 491.3365615308285
time_elpased: 9.807
batch start
#iterations: 214
currently lose_sum: 492.86431884765625
time_elpased: 9.771
batch start
#iterations: 215
currently lose_sum: 492.46538946032524
time_elpased: 9.794
batch start
#iterations: 216
currently lose_sum: 491.73934867978096
time_elpased: 9.791
batch start
#iterations: 217
currently lose_sum: 491.3224530816078
time_elpased: 9.761
batch start
#iterations: 218
currently lose_sum: 492.3395703136921
time_elpased: 9.797
batch start
#iterations: 219
currently lose_sum: 492.20625883340836
time_elpased: 9.896
start validation test
0.281237113402
1.0
0.281237113402
0.439008690055
0
validation finish
batch start
#iterations: 220
currently lose_sum: 491.1145850121975
time_elpased: 9.805
batch start
#iterations: 221
currently lose_sum: 493.98824724555016
time_elpased: 9.814
batch start
#iterations: 222
currently lose_sum: 490.96132999658585
time_elpased: 9.814
batch start
#iterations: 223
currently lose_sum: 490.7508876621723
time_elpased: 9.768
batch start
#iterations: 224
currently lose_sum: 494.5474626123905
time_elpased: 9.769
batch start
#iterations: 225
currently lose_sum: 493.0538930594921
time_elpased: 9.773
batch start
#iterations: 226
currently lose_sum: 492.9003054499626
time_elpased: 9.785
batch start
#iterations: 227
currently lose_sum: 492.74792245030403
time_elpased: 9.764
batch start
#iterations: 228
currently lose_sum: 489.5367232263088
time_elpased: 9.787
batch start
#iterations: 229
currently lose_sum: 492.05061492323875
time_elpased: 9.826
batch start
#iterations: 230
currently lose_sum: 493.0419559776783
time_elpased: 9.767
batch start
#iterations: 231
currently lose_sum: 493.89162519574165
time_elpased: 9.788
batch start
#iterations: 232
currently lose_sum: 488.1261911690235
time_elpased: 9.816
batch start
#iterations: 233
currently lose_sum: 490.92127707600594
time_elpased: 9.813
batch start
#iterations: 234
currently lose_sum: 491.8330787420273
time_elpased: 9.858
batch start
#iterations: 235
currently lose_sum: 492.3299149572849
time_elpased: 9.862
batch start
#iterations: 236
currently lose_sum: 491.43496868014336
time_elpased: 9.768
batch start
#iterations: 237
currently lose_sum: 492.5333333015442
time_elpased: 9.806
batch start
#iterations: 238
currently lose_sum: 489.8646047115326
time_elpased: 9.806
batch start
#iterations: 239
currently lose_sum: 489.7798899114132
time_elpased: 9.838
start validation test
0.194536082474
1.0
0.194536082474
0.325709847243
0
validation finish
batch start
#iterations: 240
currently lose_sum: 488.3696946501732
time_elpased: 9.77
batch start
#iterations: 241
currently lose_sum: 491.10692578554153
time_elpased: 9.773
batch start
#iterations: 242
currently lose_sum: 488.9054873287678
time_elpased: 9.818
batch start
#iterations: 243
currently lose_sum: 490.6572540998459
time_elpased: 9.771
batch start
#iterations: 244
currently lose_sum: 491.4930283129215
time_elpased: 9.821
batch start
#iterations: 245
currently lose_sum: 491.9549200832844
time_elpased: 9.761
batch start
#iterations: 246
currently lose_sum: 491.9324096441269
time_elpased: 9.827
batch start
#iterations: 247
currently lose_sum: 491.69690239429474
time_elpased: 9.798
batch start
#iterations: 248
currently lose_sum: 491.0682444870472
time_elpased: 9.789
batch start
#iterations: 249
currently lose_sum: 493.0842270851135
time_elpased: 9.789
batch start
#iterations: 250
currently lose_sum: 490.2786775827408
time_elpased: 9.798
batch start
#iterations: 251
currently lose_sum: 491.1954730153084
time_elpased: 9.79
batch start
#iterations: 252
currently lose_sum: 490.8334057033062
time_elpased: 9.809
batch start
#iterations: 253
currently lose_sum: 489.3800915181637
time_elpased: 9.817
batch start
#iterations: 254
currently lose_sum: 493.844471603632
time_elpased: 9.818
batch start
#iterations: 255
currently lose_sum: 491.94497683644295
time_elpased: 9.741
batch start
#iterations: 256
currently lose_sum: 489.97689613699913
time_elpased: 9.806
batch start
#iterations: 257
currently lose_sum: 490.3471680879593
time_elpased: 9.81
batch start
#iterations: 258
currently lose_sum: 491.1596451699734
time_elpased: 9.832
batch start
#iterations: 259
currently lose_sum: 492.93999311327934
time_elpased: 9.817
start validation test
0.278453608247
1.0
0.278453608247
0.435610031449
0
validation finish
batch start
#iterations: 260
currently lose_sum: 492.09504249691963
time_elpased: 9.821
batch start
#iterations: 261
currently lose_sum: 492.4902475178242
time_elpased: 9.725
batch start
#iterations: 262
currently lose_sum: 491.62328639626503
time_elpased: 9.746
batch start
#iterations: 263
currently lose_sum: 491.62326911091805
time_elpased: 9.903
batch start
#iterations: 264
currently lose_sum: 490.24323520064354
time_elpased: 9.781
batch start
#iterations: 265
currently lose_sum: 490.91319647431374
time_elpased: 9.911
batch start
#iterations: 266
currently lose_sum: 492.0189405977726
time_elpased: 9.842
batch start
#iterations: 267
currently lose_sum: 491.5985096693039
time_elpased: 9.835
batch start
#iterations: 268
currently lose_sum: 491.96959912776947
time_elpased: 9.774
batch start
#iterations: 269
currently lose_sum: 493.0034292936325
time_elpased: 9.8
batch start
#iterations: 270
currently lose_sum: 491.49952271580696
time_elpased: 9.859
batch start
#iterations: 271
currently lose_sum: 487.300543397665
time_elpased: 9.793
batch start
#iterations: 272
currently lose_sum: 492.1156310737133
time_elpased: 9.81
batch start
#iterations: 273
currently lose_sum: 490.31188902258873
time_elpased: 9.75
batch start
#iterations: 274
currently lose_sum: 491.4724523127079
time_elpased: 9.788
batch start
#iterations: 275
currently lose_sum: 492.5600136220455
time_elpased: 9.792
batch start
#iterations: 276
currently lose_sum: 489.41568687558174
time_elpased: 9.739
batch start
#iterations: 277
currently lose_sum: 493.2677864432335
time_elpased: 9.848
batch start
#iterations: 278
currently lose_sum: 493.25707653164864
time_elpased: 9.788
batch start
#iterations: 279
currently lose_sum: 491.1990890800953
time_elpased: 9.799
start validation test
0.235773195876
1.0
0.235773195876
0.381580045049
0
validation finish
batch start
#iterations: 280
currently lose_sum: 489.72659227252007
time_elpased: 9.744
batch start
#iterations: 281
currently lose_sum: 491.4764556288719
time_elpased: 9.732
batch start
#iterations: 282
currently lose_sum: 492.30250558257103
time_elpased: 9.815
batch start
#iterations: 283
currently lose_sum: 490.72679647803307
time_elpased: 9.79
batch start
#iterations: 284
currently lose_sum: 492.17815947532654
time_elpased: 9.812
batch start
#iterations: 285
currently lose_sum: 491.3292297422886
time_elpased: 9.774
batch start
#iterations: 286
currently lose_sum: 491.420472830534
time_elpased: 9.831
batch start
#iterations: 287
currently lose_sum: 491.46550196409225
time_elpased: 9.876
batch start
#iterations: 288
currently lose_sum: 492.0184051692486
time_elpased: 9.748
batch start
#iterations: 289
currently lose_sum: 490.90436351299286
time_elpased: 9.781
batch start
#iterations: 290
currently lose_sum: 489.63375252485275
time_elpased: 9.815
batch start
#iterations: 291
currently lose_sum: 491.8065296411514
time_elpased: 9.797
batch start
#iterations: 292
currently lose_sum: 490.4177328646183
time_elpased: 9.734
batch start
#iterations: 293
currently lose_sum: 490.62656673789024
time_elpased: 9.782
batch start
#iterations: 294
currently lose_sum: 491.868188560009
time_elpased: 7.999
batch start
#iterations: 295
currently lose_sum: 491.41090869903564
time_elpased: 7.735
batch start
#iterations: 296
currently lose_sum: 489.37015584111214
time_elpased: 7.769
batch start
#iterations: 297
currently lose_sum: 491.32770681381226
time_elpased: 7.716
batch start
#iterations: 298
currently lose_sum: 489.83350867033005
time_elpased: 7.743
batch start
#iterations: 299
currently lose_sum: 489.4903267920017
time_elpased: 7.759
start validation test
0.30206185567
1.0
0.30206185567
0.4639746635
0
validation finish
batch start
#iterations: 300
currently lose_sum: 489.2806686460972
time_elpased: 7.722
batch start
#iterations: 301
currently lose_sum: 490.56834304332733
time_elpased: 7.707
batch start
#iterations: 302
currently lose_sum: 491.4017210006714
time_elpased: 7.732
batch start
#iterations: 303
currently lose_sum: 490.5889811515808
time_elpased: 7.734
batch start
#iterations: 304
currently lose_sum: 491.4386839270592
time_elpased: 7.731
batch start
#iterations: 305
currently lose_sum: 492.1120392382145
time_elpased: 7.748
batch start
#iterations: 306
currently lose_sum: 490.38914227485657
time_elpased: 7.737
batch start
#iterations: 307
currently lose_sum: 493.9269420802593
time_elpased: 7.707
batch start
#iterations: 308
currently lose_sum: 490.5752678513527
time_elpased: 7.718
batch start
#iterations: 309
currently lose_sum: 493.28072103857994
time_elpased: 7.726
batch start
#iterations: 310
currently lose_sum: 489.542219042778
time_elpased: 7.72
batch start
#iterations: 311
currently lose_sum: 490.05122351646423
time_elpased: 7.71
batch start
#iterations: 312
currently lose_sum: 490.5334947705269
time_elpased: 7.734
batch start
#iterations: 313
currently lose_sum: 490.6775292754173
time_elpased: 7.728
batch start
#iterations: 314
currently lose_sum: 489.05330538749695
time_elpased: 7.69
batch start
#iterations: 315
currently lose_sum: 490.9563114643097
time_elpased: 7.698
batch start
#iterations: 316
currently lose_sum: 491.0524271726608
time_elpased: 7.759
batch start
#iterations: 317
currently lose_sum: 489.31313705444336
time_elpased: 7.748
batch start
#iterations: 318
currently lose_sum: 493.01793599128723
time_elpased: 7.733
batch start
#iterations: 319
currently lose_sum: 491.4534015059471
time_elpased: 7.708
start validation test
0.224329896907
1.0
0.224329896907
0.366453351297
0
validation finish
batch start
#iterations: 320
currently lose_sum: 491.50816372036934
time_elpased: 7.727
batch start
#iterations: 321
currently lose_sum: 489.00416323542595
time_elpased: 7.701
batch start
#iterations: 322
currently lose_sum: 489.27687472105026
time_elpased: 7.726
batch start
#iterations: 323
currently lose_sum: 490.7973049581051
time_elpased: 7.735
batch start
#iterations: 324
currently lose_sum: 492.85919457674026
time_elpased: 7.731
batch start
#iterations: 325
currently lose_sum: 491.6293058991432
time_elpased: 7.682
batch start
#iterations: 326
currently lose_sum: 492.1664886176586
time_elpased: 7.704
batch start
#iterations: 327
currently lose_sum: 491.09308537840843
time_elpased: 7.723
batch start
#iterations: 328
currently lose_sum: 490.68611258268356
time_elpased: 7.727
batch start
#iterations: 329
currently lose_sum: 491.3521688580513
time_elpased: 7.719
batch start
#iterations: 330
currently lose_sum: 492.319895863533
time_elpased: 7.81
batch start
#iterations: 331
currently lose_sum: 492.752184599638
time_elpased: 7.739
batch start
#iterations: 332
currently lose_sum: 490.879417181015
time_elpased: 7.725
batch start
#iterations: 333
currently lose_sum: 491.82630559802055
time_elpased: 7.729
batch start
#iterations: 334
currently lose_sum: 490.96325850486755
time_elpased: 7.712
batch start
#iterations: 335
currently lose_sum: 488.77514839172363
time_elpased: 7.719
batch start
#iterations: 336
currently lose_sum: 492.1012024283409
time_elpased: 7.688
batch start
#iterations: 337
currently lose_sum: 490.5966837108135
time_elpased: 7.742
batch start
#iterations: 338
currently lose_sum: 492.20132026076317
time_elpased: 7.71
batch start
#iterations: 339
currently lose_sum: 490.1188113093376
time_elpased: 7.726
start validation test
0.255670103093
1.0
0.255670103093
0.407224958949
0
validation finish
batch start
#iterations: 340
currently lose_sum: 491.4802483320236
time_elpased: 7.731
batch start
#iterations: 341
currently lose_sum: 491.54504376649857
time_elpased: 7.741
batch start
#iterations: 342
currently lose_sum: 490.1474606990814
time_elpased: 7.72
batch start
#iterations: 343
currently lose_sum: 491.83397725224495
time_elpased: 7.713
batch start
#iterations: 344
currently lose_sum: 491.54537042975426
time_elpased: 7.721
batch start
#iterations: 345
currently lose_sum: 491.36575534939766
time_elpased: 7.697
batch start
#iterations: 346
currently lose_sum: 490.48106423020363
time_elpased: 7.723
batch start
#iterations: 347
currently lose_sum: 490.50988057255745
time_elpased: 7.723
batch start
#iterations: 348
currently lose_sum: 492.1732240021229
time_elpased: 7.725
batch start
#iterations: 349
currently lose_sum: 491.65982127189636
time_elpased: 7.713
batch start
#iterations: 350
currently lose_sum: 492.5944148302078
time_elpased: 7.717
batch start
#iterations: 351
currently lose_sum: 490.3150280714035
time_elpased: 7.692
batch start
#iterations: 352
currently lose_sum: 490.2644368112087
time_elpased: 7.705
batch start
#iterations: 353
currently lose_sum: 492.37542030215263
time_elpased: 7.732
batch start
#iterations: 354
currently lose_sum: 490.2018155157566
time_elpased: 7.7
batch start
#iterations: 355
currently lose_sum: 491.8689319193363
time_elpased: 7.741
batch start
#iterations: 356
currently lose_sum: 492.57567474246025
time_elpased: 7.721
batch start
#iterations: 357
currently lose_sum: 491.4270483851433
time_elpased: 7.71
batch start
#iterations: 358
currently lose_sum: 491.6904616653919
time_elpased: 7.724
batch start
#iterations: 359
currently lose_sum: 491.33978378772736
time_elpased: 7.729
start validation test
0.207731958763
1.0
0.207731958763
0.344003414426
0
validation finish
batch start
#iterations: 360
currently lose_sum: 491.67438918352127
time_elpased: 7.688
batch start
#iterations: 361
currently lose_sum: 490.29004088044167
time_elpased: 7.731
batch start
#iterations: 362
currently lose_sum: 491.1997320353985
time_elpased: 7.724
batch start
#iterations: 363
currently lose_sum: 490.1277779340744
time_elpased: 7.701
batch start
#iterations: 364
currently lose_sum: 490.27510899305344
time_elpased: 7.691
batch start
#iterations: 365
currently lose_sum: 491.4818126261234
time_elpased: 7.729
batch start
#iterations: 366
currently lose_sum: 492.34101513028145
time_elpased: 7.697
batch start
#iterations: 367
currently lose_sum: 492.28964498639107
time_elpased: 7.774
batch start
#iterations: 368
currently lose_sum: 489.91018134355545
time_elpased: 7.718
batch start
#iterations: 369
currently lose_sum: 489.8328549861908
time_elpased: 7.738
batch start
#iterations: 370
currently lose_sum: 489.2060287296772
time_elpased: 7.768
batch start
#iterations: 371
currently lose_sum: 491.70260152220726
time_elpased: 7.717
batch start
#iterations: 372
currently lose_sum: 490.2589620947838
time_elpased: 7.734
batch start
#iterations: 373
currently lose_sum: 490.5760011970997
time_elpased: 7.737
batch start
#iterations: 374
currently lose_sum: 491.48164188861847
time_elpased: 7.685
batch start
#iterations: 375
currently lose_sum: 490.6058574616909
time_elpased: 7.741
batch start
#iterations: 376
currently lose_sum: 491.56240540742874
time_elpased: 7.707
batch start
#iterations: 377
currently lose_sum: 492.0217609703541
time_elpased: 7.746
batch start
#iterations: 378
currently lose_sum: 491.3576165139675
time_elpased: 7.728
batch start
#iterations: 379
currently lose_sum: 491.5011005103588
time_elpased: 7.712
start validation test
0.201340206186
1.0
0.201340206186
0.335192654252
0
validation finish
batch start
#iterations: 380
currently lose_sum: 490.51599338650703
time_elpased: 7.704
batch start
#iterations: 381
currently lose_sum: 490.10175690054893
time_elpased: 7.703
batch start
#iterations: 382
currently lose_sum: 490.9480683505535
time_elpased: 7.707
batch start
#iterations: 383
currently lose_sum: 488.72943076491356
time_elpased: 7.695
batch start
#iterations: 384
currently lose_sum: 489.954851269722
time_elpased: 7.72
batch start
#iterations: 385
currently lose_sum: 488.61428755521774
time_elpased: 7.743
batch start
#iterations: 386
currently lose_sum: 491.2747214436531
time_elpased: 7.726
batch start
#iterations: 387
currently lose_sum: 491.42208564281464
time_elpased: 7.701
batch start
#iterations: 388
currently lose_sum: 491.8483394384384
time_elpased: 7.736
batch start
#iterations: 389
currently lose_sum: 488.2161581516266
time_elpased: 7.713
batch start
#iterations: 390
currently lose_sum: 489.9790548682213
time_elpased: 7.725
batch start
#iterations: 391
currently lose_sum: 490.62677323818207
time_elpased: 7.723
batch start
#iterations: 392
currently lose_sum: 490.0379048585892
time_elpased: 7.738
batch start
#iterations: 393
currently lose_sum: 490.3747432231903
time_elpased: 7.743
batch start
#iterations: 394
currently lose_sum: 490.87459245324135
time_elpased: 7.687
batch start
#iterations: 395
currently lose_sum: 489.485491335392
time_elpased: 7.742
batch start
#iterations: 396
currently lose_sum: 490.2193452119827
time_elpased: 7.7
batch start
#iterations: 397
currently lose_sum: 491.4518049657345
time_elpased: 7.685
batch start
#iterations: 398
currently lose_sum: 490.03702533245087
time_elpased: 7.701
batch start
#iterations: 399
currently lose_sum: 491.734068185091
time_elpased: 7.759
start validation test
0.218350515464
1.0
0.218350515464
0.358436283635
0
validation finish
acc: 0.311
pre: 1.000
rec: 0.311
F1: 0.474
auc: 0.000
