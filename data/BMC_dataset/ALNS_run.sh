#!/bin/tcsh
foreach folds (0 1 2 3 4)
	less PN_train_$folds.txt | awk '{print $1;print $2}' | sort | uniq -c | sort -k2n > freq_PNtrain$folds.txt
	set num_sample = `less PN_train_$folds.txt | wc | awk '{print $1}'`
	python negsamp.py --num_sample $num_sample --train_file PN_train_$folds.txt --freq_file freq_PNtrain$folds.txt --outfile PN_N_set$folds.txt
	cat PN_train_$folds.txt PN_N_set$folds.txt > PNN_train$folds.txt
	cat PN_train_$folds.txt PN_N_set$folds.txt PN_N_set$folds.txt > PN2N_train$folds.txt

	set num_test = `less test_$folds.txt | wc | awk '{print $1}'`
	set num_val = `less validation_$folds.txt | wc | awk '{print $1}'`
	set num_train = `less PNN_train$folds.txt | wc | awk '{print $1}'`
	set num_train2 = `less PN2N_train$folds.txt | wc | awk '{print $1}'`

	foreach L (8 16 32 64)
		python frame_work_test.py --attention $1 --sim_attention $2 --train PNN_train$folds.txt --test test_$folds.txt --validation validation_$folds.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --all_sim $2 --L_dim $L > ./ALL_SIM/log_1N_attention_$1\_sim_attention_$2\_L_$L\_feature_50_1_$folds.txt
		python frame_work_test.py --attention $1 --sim_attention $2 --train PN2N_train$folds.txt --test test_$folds.txt --validation validation_$folds.txt --num_train_sample $num_train2 --num_validation_sample $num_val --num_test_sample $num_test --all_sim $2 --L_dim $L > ./ALL_SIM/log_1N_attention_$1\_sim_attention_$2\_L_$L\_feature_50_1_$folds.txt
	end
end
