import tensorflow as tf
import numpy as np
import GraphConv

def Graph_feature(graph_matrix, graph_feature, degree_matrix, weight_self_list, weight_neig_list, b_matrix, act, degree_max, degree_min, feature_size, Graph_feature_size, weight_self_Gathering, weight_neig_Gathering, b_Gathering, supernode_feature, gamma, beta, eps, keep_prob):

	level_1_nodes_matrix = GraphConv.node_level_Conv(graph_matrix, graph_feature, weight_self_list, weight_neig_list, b_matrix, degree_matrix, degree_max, degree_min, act)

	#level_1_nodes_matrix = tf.nn.dropout(level_1_nodes_matrix, keep_prob = keep_prob)
	
	level_1_nodes_matrix_n = GraphConv.node_level_normalization(level_1_nodes_matrix, gamma, beta, eps)
	
	poolized_matrix_1 = GraphConv.Graph_Pool(graph_matrix, level_1_nodes_matrix_n, feature_size)

	level_2_nodes_matrix = GraphConv.node_level_Conv(graph_matrix, poolized_matrix_1, weight_self_list, weight_neig_list, b_matrix, degree_matrix, degree_max, degree_min, act)

	#level_2_nodes_matrix = tf.nn.dropout(level_2_nodes_matrix, keep_prob = keep_prob)
	
	level_2_nodes_matrix_n = GraphConv.node_level_normalization(level_2_nodes_matrix, gamma, beta, eps)

	poolized_matrix_2 = GraphConv.Graph_Pool(graph_matrix, level_2_nodes_matrix_n, feature_size)

	Graph_feature = GraphConv.Graph_Gathering(poolized_matrix_2, supernode_feature, weight_self_Gathering, weight_neig_Gathering, b_Gathering)

	return Graph_feature


	
