#!/bin/tcsh
foreach folds (0 1 2 3 4)
        foreach step (train validation test)
                set num_sample = `less PN_$step\_$folds.txt | wc | awk '{print $1}'`
                if($step == test || $step == validation) then
                        cat PN_$step\_$folds.txt PN_train\_$folds.txt > temp
                        less temp | awk '{print$1;print$2}' | sort | uniq -c | sort -k2n > freq_PN$step$folds.txt
                        python negsamp_tf.py --num_sample $num_sample --train_file temp --freq_file freq_PN$step$folds.txt --outfile PN_$step\_N_set$folds.txt --remove 1
                else
                        less PN_$step\_$folds.txt | awk '{print$1;print$2}' | sort | uniq -c | sort -k2n > freq_PN$step$folds.txt
                        python negsamp_tf.py --num_sample $num_sample --train_file PN_$step\_$folds.txt --freq_file freq_PN$step$folds.txt --outfile PN_$step\_N_set$folds.txt --remove 1
                endif
                cat PN_$step\_$folds.txt PN_$step\_N_set$folds.txt > PNN_Jure_$step$folds.txt
        end
end
