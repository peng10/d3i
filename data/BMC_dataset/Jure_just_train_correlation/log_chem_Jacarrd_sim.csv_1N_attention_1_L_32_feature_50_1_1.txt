start to construct graph
graph construct over
58388
epochs start
batch start
#iterations: 0
currently lose_sum: 404.0236060619354
time_elpased: 1.131
batch start
#iterations: 1
currently lose_sum: 402.4643868803978
time_elpased: 1.11
batch start
#iterations: 2
currently lose_sum: 402.26009130477905
time_elpased: 1.13
batch start
#iterations: 3
currently lose_sum: 401.2981557250023
time_elpased: 1.12
batch start
#iterations: 4
currently lose_sum: 401.3405564427376
time_elpased: 1.109
batch start
#iterations: 5
currently lose_sum: 401.53947472572327
time_elpased: 1.107
batch start
#iterations: 6
currently lose_sum: 401.01841574907303
time_elpased: 1.123
batch start
#iterations: 7
currently lose_sum: 401.1389864087105
time_elpased: 1.13
batch start
#iterations: 8
currently lose_sum: 400.75275832414627
time_elpased: 1.116
batch start
#iterations: 9
currently lose_sum: 400.8897136449814
time_elpased: 1.112
batch start
#iterations: 10
currently lose_sum: 400.34445291757584
time_elpased: 1.113
batch start
#iterations: 11
currently lose_sum: 400.3950632214546
time_elpased: 1.115
batch start
#iterations: 12
currently lose_sum: 399.7281346321106
time_elpased: 1.106
batch start
#iterations: 13
currently lose_sum: 400.39506393671036
time_elpased: 1.123
batch start
#iterations: 14
currently lose_sum: 398.8382281064987
time_elpased: 1.126
batch start
#iterations: 15
currently lose_sum: 398.8628638982773
time_elpased: 1.1
batch start
#iterations: 16
currently lose_sum: 398.60793828964233
time_elpased: 1.115
batch start
#iterations: 17
currently lose_sum: 398.78361707925797
time_elpased: 1.105
batch start
#iterations: 18
currently lose_sum: 397.2160949707031
time_elpased: 1.114
batch start
#iterations: 19
currently lose_sum: 396.5796321630478
time_elpased: 1.136
start validation test
0.555773195876
0.571726928599
0.450756406298
0.504085625504
0
validation finish
batch start
#iterations: 20
currently lose_sum: 397.03872311115265
time_elpased: 1.112
batch start
#iterations: 21
currently lose_sum: 395.5084083080292
time_elpased: 1.113
batch start
#iterations: 22
currently lose_sum: 396.3439202308655
time_elpased: 1.108
batch start
#iterations: 23
currently lose_sum: 396.28300935029984
time_elpased: 1.127
batch start
#iterations: 24
currently lose_sum: 394.6795412302017
time_elpased: 1.12
batch start
#iterations: 25
currently lose_sum: 394.6571998000145
time_elpased: 1.172
batch start
#iterations: 26
currently lose_sum: 394.3001310825348
time_elpased: 1.109
batch start
#iterations: 27
currently lose_sum: 394.02093625068665
time_elpased: 1.116
batch start
#iterations: 28
currently lose_sum: 393.4904406070709
time_elpased: 1.126
batch start
#iterations: 29
currently lose_sum: 392.54460376501083
time_elpased: 1.169
batch start
#iterations: 30
currently lose_sum: 393.8027558326721
time_elpased: 1.117
batch start
#iterations: 31
currently lose_sum: 392.47727006673813
time_elpased: 1.167
batch start
#iterations: 32
currently lose_sum: 393.0200848579407
time_elpased: 1.103
batch start
#iterations: 33
currently lose_sum: 392.9451127052307
time_elpased: 1.111
batch start
#iterations: 34
currently lose_sum: 391.4627476334572
time_elpased: 1.11
batch start
#iterations: 35
currently lose_sum: 392.3704840540886
time_elpased: 1.106
batch start
#iterations: 36
currently lose_sum: 391.5946096777916
time_elpased: 1.1
batch start
#iterations: 37
currently lose_sum: 391.0519461631775
time_elpased: 1.106
batch start
#iterations: 38
currently lose_sum: 390.9976249933243
time_elpased: 1.11
batch start
#iterations: 39
currently lose_sum: 389.8158022761345
time_elpased: 1.101
start validation test
0.604793814433
0.60778128286
0.594833796439
0.601237842617
0
validation finish
batch start
#iterations: 40
currently lose_sum: 390.8078514933586
time_elpased: 1.127
batch start
#iterations: 41
currently lose_sum: 390.282921731472
time_elpased: 1.151
batch start
#iterations: 42
currently lose_sum: 389.9189320206642
time_elpased: 1.124
batch start
#iterations: 43
currently lose_sum: 389.4059998989105
time_elpased: 1.101
batch start
#iterations: 44
currently lose_sum: 389.4299507141113
time_elpased: 1.11
batch start
#iterations: 45
currently lose_sum: 389.78586173057556
time_elpased: 1.12
batch start
#iterations: 46
currently lose_sum: 389.4116015434265
time_elpased: 1.117
batch start
#iterations: 47
currently lose_sum: 388.04086166620255
time_elpased: 1.12
batch start
#iterations: 48
currently lose_sum: 389.34635758399963
time_elpased: 1.108
batch start
#iterations: 49
currently lose_sum: 388.21769124269485
time_elpased: 1.124
batch start
#iterations: 50
currently lose_sum: 387.95591843128204
time_elpased: 1.152
batch start
#iterations: 51
currently lose_sum: 388.20644468069077
time_elpased: 1.111
batch start
#iterations: 52
currently lose_sum: 387.31823164224625
time_elpased: 1.18
batch start
#iterations: 53
currently lose_sum: 387.98046827316284
time_elpased: 1.118
batch start
#iterations: 54
currently lose_sum: 389.54294872283936
time_elpased: 1.114
batch start
#iterations: 55
currently lose_sum: 387.68841022253036
time_elpased: 1.121
batch start
#iterations: 56
currently lose_sum: 387.6573238968849
time_elpased: 1.109
batch start
#iterations: 57
currently lose_sum: 386.87168926000595
time_elpased: 1.11
batch start
#iterations: 58
currently lose_sum: 386.4756893515587
time_elpased: 1.106
batch start
#iterations: 59
currently lose_sum: 387.01741844415665
time_elpased: 1.118
start validation test
0.60706185567
0.618573046433
0.562107646393
0.588990133175
0
validation finish
batch start
#iterations: 60
currently lose_sum: 386.7891337275505
time_elpased: 1.131
batch start
#iterations: 61
currently lose_sum: 386.8972065448761
time_elpased: 1.114
batch start
#iterations: 62
currently lose_sum: 386.90501070022583
time_elpased: 1.128
batch start
#iterations: 63
currently lose_sum: 387.37987780570984
time_elpased: 1.123
batch start
#iterations: 64
currently lose_sum: 385.9919421672821
time_elpased: 1.107
batch start
#iterations: 65
currently lose_sum: 385.4637033343315
time_elpased: 1.125
batch start
#iterations: 66
currently lose_sum: 386.2459014058113
time_elpased: 1.11
batch start
#iterations: 67
currently lose_sum: 386.41342598199844
time_elpased: 1.115
batch start
#iterations: 68
currently lose_sum: 386.3020910024643
time_elpased: 1.121
batch start
#iterations: 69
currently lose_sum: 386.61541801691055
time_elpased: 1.122
batch start
#iterations: 70
currently lose_sum: 386.59831857681274
time_elpased: 1.112
batch start
#iterations: 71
currently lose_sum: 385.6298341155052
time_elpased: 1.143
batch start
#iterations: 72
currently lose_sum: 386.31232219934464
time_elpased: 1.108
batch start
#iterations: 73
currently lose_sum: 385.4827473163605
time_elpased: 1.12
batch start
#iterations: 74
currently lose_sum: 385.5507541894913
time_elpased: 1.132
batch start
#iterations: 75
currently lose_sum: 385.07808262109756
time_elpased: 1.106
batch start
#iterations: 76
currently lose_sum: 385.40374344587326
time_elpased: 1.113
batch start
#iterations: 77
currently lose_sum: 385.50800347328186
time_elpased: 1.149
batch start
#iterations: 78
currently lose_sum: 385.7581143975258
time_elpased: 1.115
batch start
#iterations: 79
currently lose_sum: 385.7523446083069
time_elpased: 1.096
start validation test
0.627680412371
0.598608255575
0.779047030977
0.677011134463
0
validation finish
batch start
#iterations: 80
currently lose_sum: 384.44718784093857
time_elpased: 1.125
batch start
#iterations: 81
currently lose_sum: 384.8981634378433
time_elpased: 1.124
batch start
#iterations: 82
currently lose_sum: 384.03050035238266
time_elpased: 1.111
batch start
#iterations: 83
currently lose_sum: 384.5951991081238
time_elpased: 1.116
batch start
#iterations: 84
currently lose_sum: 384.3837356567383
time_elpased: 1.159
batch start
#iterations: 85
currently lose_sum: 384.0999246239662
time_elpased: 1.125
batch start
#iterations: 86
currently lose_sum: 385.50626707077026
time_elpased: 1.1
batch start
#iterations: 87
currently lose_sum: 384.448262989521
time_elpased: 1.151
batch start
#iterations: 88
currently lose_sum: 383.7203291654587
time_elpased: 1.113
batch start
#iterations: 89
currently lose_sum: 382.9272336959839
time_elpased: 1.105
batch start
#iterations: 90
currently lose_sum: 384.93353313207626
time_elpased: 1.139
batch start
#iterations: 91
currently lose_sum: 384.44293999671936
time_elpased: 1.12
batch start
#iterations: 92
currently lose_sum: 383.49590957164764
time_elpased: 1.176
batch start
#iterations: 93
currently lose_sum: 384.60010850429535
time_elpased: 1.098
batch start
#iterations: 94
currently lose_sum: 384.2100763320923
time_elpased: 1.116
batch start
#iterations: 95
currently lose_sum: 385.2147468328476
time_elpased: 1.153
batch start
#iterations: 96
currently lose_sum: 382.9010691046715
time_elpased: 1.111
batch start
#iterations: 97
currently lose_sum: 383.39199620485306
time_elpased: 1.115
batch start
#iterations: 98
currently lose_sum: 382.8290351033211
time_elpased: 1.107
batch start
#iterations: 99
currently lose_sum: 384.0248129963875
time_elpased: 1.106
start validation test
0.634896907216
0.612448770492
0.73819079963
0.669466610668
0
validation finish
batch start
#iterations: 100
currently lose_sum: 382.42208725214005
time_elpased: 1.125
batch start
#iterations: 101
currently lose_sum: 383.19124615192413
time_elpased: 1.172
batch start
#iterations: 102
currently lose_sum: 383.0387902855873
time_elpased: 1.119
batch start
#iterations: 103
currently lose_sum: 381.35137617588043
time_elpased: 1.109
batch start
#iterations: 104
currently lose_sum: 382.28049397468567
time_elpased: 1.119
batch start
#iterations: 105
currently lose_sum: 381.88580816984177
time_elpased: 1.116
batch start
#iterations: 106
currently lose_sum: 382.00629234313965
time_elpased: 1.13
batch start
#iterations: 107
currently lose_sum: 382.4175103902817
time_elpased: 1.142
batch start
#iterations: 108
currently lose_sum: 382.0106751918793
time_elpased: 1.154
batch start
#iterations: 109
currently lose_sum: 382.43121415376663
time_elpased: 1.11
batch start
#iterations: 110
currently lose_sum: 382.43020141124725
time_elpased: 1.115
batch start
#iterations: 111
currently lose_sum: 382.0854686498642
time_elpased: 1.118
batch start
#iterations: 112
currently lose_sum: 383.0191136598587
time_elpased: 1.11
batch start
#iterations: 113
currently lose_sum: 383.20966601371765
time_elpased: 1.11
batch start
#iterations: 114
currently lose_sum: 382.3630723953247
time_elpased: 1.129
batch start
#iterations: 115
currently lose_sum: 381.6999329328537
time_elpased: 1.107
batch start
#iterations: 116
currently lose_sum: 381.5248143672943
time_elpased: 1.117
batch start
#iterations: 117
currently lose_sum: 380.7058866620064
time_elpased: 1.122
batch start
#iterations: 118
currently lose_sum: 382.34055280685425
time_elpased: 1.16
batch start
#iterations: 119
currently lose_sum: 381.67164784669876
time_elpased: 1.118
start validation test
0.636907216495
0.609182256352
0.767417927344
0.679205756444
0
validation finish
batch start
#iterations: 120
currently lose_sum: 380.4724262356758
time_elpased: 1.127
batch start
#iterations: 121
currently lose_sum: 382.0222083926201
time_elpased: 1.111
batch start
#iterations: 122
currently lose_sum: 379.5693113207817
time_elpased: 1.15
batch start
#iterations: 123
currently lose_sum: 382.20530992746353
time_elpased: 1.117
batch start
#iterations: 124
currently lose_sum: 381.8644452691078
time_elpased: 1.131
batch start
#iterations: 125
currently lose_sum: 381.2610305547714
time_elpased: 1.178
batch start
#iterations: 126
currently lose_sum: 380.6494888663292
time_elpased: 1.115
batch start
#iterations: 127
currently lose_sum: 381.5043789744377
time_elpased: 1.102
batch start
#iterations: 128
currently lose_sum: 381.7932557463646
time_elpased: 1.11
batch start
#iterations: 129
currently lose_sum: 380.01496452093124
time_elpased: 1.116
batch start
#iterations: 130
currently lose_sum: 382.120668053627
time_elpased: 1.109
batch start
#iterations: 131
currently lose_sum: 380.56187999248505
time_elpased: 1.161
batch start
#iterations: 132
currently lose_sum: 381.76875936985016
time_elpased: 1.179
batch start
#iterations: 133
currently lose_sum: 380.12122666835785
time_elpased: 1.137
batch start
#iterations: 134
currently lose_sum: 382.5692822933197
time_elpased: 1.111
batch start
#iterations: 135
currently lose_sum: 380.93055683374405
time_elpased: 1.116
batch start
#iterations: 136
currently lose_sum: 381.0935742855072
time_elpased: 1.15
batch start
#iterations: 137
currently lose_sum: 380.81290382146835
time_elpased: 1.129
batch start
#iterations: 138
currently lose_sum: 380.3355475664139
time_elpased: 1.113
batch start
#iterations: 139
currently lose_sum: 380.5806296467781
time_elpased: 1.14
start validation test
0.636546391753
0.620568017366
0.706082124112
0.660569007847
0
validation finish
batch start
#iterations: 140
currently lose_sum: 380.68908393383026
time_elpased: 1.125
batch start
#iterations: 141
currently lose_sum: 381.06676441431046
time_elpased: 1.126
batch start
#iterations: 142
currently lose_sum: 381.14124661684036
time_elpased: 1.128
batch start
#iterations: 143
currently lose_sum: 380.96408945322037
time_elpased: 1.117
batch start
#iterations: 144
currently lose_sum: 380.79403710365295
time_elpased: 1.108
batch start
#iterations: 145
currently lose_sum: 380.0373312830925
time_elpased: 1.165
batch start
#iterations: 146
currently lose_sum: 380.3123008608818
time_elpased: 1.111
batch start
#iterations: 147
currently lose_sum: 380.8123236298561
time_elpased: 1.117
batch start
#iterations: 148
currently lose_sum: 380.4249289035797
time_elpased: 1.121
batch start
#iterations: 149
currently lose_sum: 380.717719912529
time_elpased: 1.16
batch start
#iterations: 150
currently lose_sum: 379.50092869997025
time_elpased: 1.17
batch start
#iterations: 151
currently lose_sum: 379.7651991248131
time_elpased: 1.162
batch start
#iterations: 152
currently lose_sum: 380.6026238799095
time_elpased: 1.108
batch start
#iterations: 153
currently lose_sum: 380.43837893009186
time_elpased: 1.111
batch start
#iterations: 154
currently lose_sum: 378.57848358154297
time_elpased: 1.153
batch start
#iterations: 155
currently lose_sum: 379.7265319824219
time_elpased: 1.108
batch start
#iterations: 156
currently lose_sum: 381.1587896347046
time_elpased: 1.152
batch start
#iterations: 157
currently lose_sum: 379.65028017759323
time_elpased: 1.165
batch start
#iterations: 158
currently lose_sum: 380.8672730922699
time_elpased: 1.118
batch start
#iterations: 159
currently lose_sum: 380.22629100084305
time_elpased: 1.161
start validation test
0.642731958763
0.615066908971
0.766285890707
0.682399303487
0
validation finish
batch start
#iterations: 160
currently lose_sum: 380.28997844457626
time_elpased: 1.126
batch start
#iterations: 161
currently lose_sum: 379.39059168100357
time_elpased: 1.162
batch start
#iterations: 162
currently lose_sum: 380.83511555194855
time_elpased: 1.145
batch start
#iterations: 163
currently lose_sum: 380.709636092186
time_elpased: 1.121
batch start
#iterations: 164
currently lose_sum: 380.350619494915
time_elpased: 1.116
batch start
#iterations: 165
currently lose_sum: 379.4071736931801
time_elpased: 1.114
batch start
#iterations: 166
currently lose_sum: 379.78730368614197
time_elpased: 1.116
batch start
#iterations: 167
currently lose_sum: 378.46173173189163
time_elpased: 1.16
batch start
#iterations: 168
currently lose_sum: 380.18611603975296
time_elpased: 1.124
batch start
#iterations: 169
currently lose_sum: 379.529208779335
time_elpased: 1.11
batch start
#iterations: 170
currently lose_sum: 378.94479179382324
time_elpased: 1.124
batch start
#iterations: 171
currently lose_sum: 378.6604910492897
time_elpased: 1.17
batch start
#iterations: 172
currently lose_sum: 379.96315705776215
time_elpased: 1.113
batch start
#iterations: 173
currently lose_sum: 379.23906648159027
time_elpased: 1.119
batch start
#iterations: 174
currently lose_sum: 380.06574326753616
time_elpased: 1.121
batch start
#iterations: 175
currently lose_sum: 379.6605859398842
time_elpased: 1.116
batch start
#iterations: 176
currently lose_sum: 378.6616942882538
time_elpased: 1.135
batch start
#iterations: 177
currently lose_sum: 378.8445127606392
time_elpased: 1.138
batch start
#iterations: 178
currently lose_sum: 378.66705840826035
time_elpased: 1.133
batch start
#iterations: 179
currently lose_sum: 380.27348321676254
time_elpased: 1.182
start validation test
0.633402061856
0.62866739109
0.654934650612
0.641532258065
0
validation finish
batch start
#iterations: 180
currently lose_sum: 379.0019115805626
time_elpased: 1.108
batch start
#iterations: 181
currently lose_sum: 378.0968959927559
time_elpased: 1.117
batch start
#iterations: 182
currently lose_sum: 377.0901582837105
time_elpased: 1.165
batch start
#iterations: 183
currently lose_sum: 378.1980341076851
time_elpased: 1.119
batch start
#iterations: 184
currently lose_sum: 379.1263297200203
time_elpased: 1.121
batch start
#iterations: 185
currently lose_sum: 378.07370698451996
time_elpased: 1.173
batch start
#iterations: 186
currently lose_sum: 379.7811476588249
time_elpased: 1.116
batch start
#iterations: 187
currently lose_sum: 377.8679223060608
time_elpased: 1.124
batch start
#iterations: 188
currently lose_sum: 379.24687391519547
time_elpased: 1.153
batch start
#iterations: 189
currently lose_sum: 378.69400668144226
time_elpased: 1.138
batch start
#iterations: 190
currently lose_sum: 379.06040090322495
time_elpased: 1.127
batch start
#iterations: 191
currently lose_sum: 378.18740648031235
time_elpased: 1.115
batch start
#iterations: 192
currently lose_sum: 378.87443393468857
time_elpased: 1.182
batch start
#iterations: 193
currently lose_sum: 377.96547800302505
time_elpased: 1.137
batch start
#iterations: 194
currently lose_sum: 379.2916207909584
time_elpased: 1.13
batch start
#iterations: 195
currently lose_sum: 379.41147911548615
time_elpased: 1.174
batch start
#iterations: 196
currently lose_sum: 378.44002336263657
time_elpased: 1.122
batch start
#iterations: 197
currently lose_sum: 378.2079080939293
time_elpased: 1.125
batch start
#iterations: 198
currently lose_sum: 378.04063737392426
time_elpased: 1.111
batch start
#iterations: 199
currently lose_sum: 378.828949034214
time_elpased: 1.11
start validation test
0.638711340206
0.611532125206
0.764021817433
0.679324701469
0
validation finish
batch start
#iterations: 200
currently lose_sum: 377.36903661489487
time_elpased: 1.15
batch start
#iterations: 201
currently lose_sum: 378.06890285015106
time_elpased: 1.129
batch start
#iterations: 202
currently lose_sum: 377.5060860514641
time_elpased: 1.118
batch start
#iterations: 203
currently lose_sum: 377.0893796682358
time_elpased: 1.109
batch start
#iterations: 204
currently lose_sum: 378.4174474477768
time_elpased: 1.104
batch start
#iterations: 205
currently lose_sum: 377.9176981449127
time_elpased: 1.106
batch start
#iterations: 206
currently lose_sum: 377.5855122208595
time_elpased: 1.17
batch start
#iterations: 207
currently lose_sum: 378.6058028936386
time_elpased: 1.122
batch start
#iterations: 208
currently lose_sum: 378.2712482213974
time_elpased: 1.138
batch start
#iterations: 209
currently lose_sum: 376.45796406269073
time_elpased: 1.153
batch start
#iterations: 210
currently lose_sum: 378.6156619787216
time_elpased: 1.128
batch start
#iterations: 211
currently lose_sum: 378.42795300483704
time_elpased: 1.14
batch start
#iterations: 212
currently lose_sum: 378.11880910396576
time_elpased: 1.118
batch start
#iterations: 213
currently lose_sum: 376.980937898159
time_elpased: 1.123
batch start
#iterations: 214
currently lose_sum: 377.78808027505875
time_elpased: 1.126
batch start
#iterations: 215
currently lose_sum: 378.7284129858017
time_elpased: 1.114
batch start
#iterations: 216
currently lose_sum: 379.2657378911972
time_elpased: 1.163
batch start
#iterations: 217
currently lose_sum: 378.20634442567825
time_elpased: 1.154
batch start
#iterations: 218
currently lose_sum: 377.3731458187103
time_elpased: 1.109
batch start
#iterations: 219
currently lose_sum: 377.53847563266754
time_elpased: 1.179
start validation test
0.64324742268
0.620103092784
0.742821858598
0.67593763169
0
validation finish
batch start
#iterations: 220
currently lose_sum: 378.16447871923447
time_elpased: 1.147
batch start
#iterations: 221
currently lose_sum: 377.63304793834686
time_elpased: 1.107
batch start
#iterations: 222
currently lose_sum: 376.9945004582405
time_elpased: 1.106
batch start
#iterations: 223
currently lose_sum: 378.21048152446747
time_elpased: 1.118
batch start
#iterations: 224
currently lose_sum: 376.7571385502815
time_elpased: 1.124
batch start
#iterations: 225
currently lose_sum: 377.30585128068924
time_elpased: 1.108
batch start
#iterations: 226
currently lose_sum: 376.99757021665573
time_elpased: 1.122
batch start
#iterations: 227
currently lose_sum: 376.22773665189743
time_elpased: 1.121
batch start
#iterations: 228
currently lose_sum: 377.6457585096359
time_elpased: 1.119
batch start
#iterations: 229
currently lose_sum: 377.6117693185806
time_elpased: 1.123
batch start
#iterations: 230
currently lose_sum: 377.04752975702286
time_elpased: 1.113
batch start
#iterations: 231
currently lose_sum: 377.2239230275154
time_elpased: 1.122
batch start
#iterations: 232
currently lose_sum: 377.5385596752167
time_elpased: 1.142
batch start
#iterations: 233
currently lose_sum: 378.75550365448
time_elpased: 1.115
batch start
#iterations: 234
currently lose_sum: 378.3649979233742
time_elpased: 1.178
batch start
#iterations: 235
currently lose_sum: 377.60336458683014
time_elpased: 1.165
batch start
#iterations: 236
currently lose_sum: 377.3226403594017
time_elpased: 1.167
batch start
#iterations: 237
currently lose_sum: 377.7886751294136
time_elpased: 1.107
batch start
#iterations: 238
currently lose_sum: 379.45739710330963
time_elpased: 1.11
batch start
#iterations: 239
currently lose_sum: 377.38272976875305
time_elpased: 1.11
start validation test
0.641288659794
0.610126177927
0.786250900484
0.687081253653
0
validation finish
batch start
#iterations: 240
currently lose_sum: 378.3006021976471
time_elpased: 1.115
batch start
#iterations: 241
currently lose_sum: 376.9412657022476
time_elpased: 1.108
batch start
#iterations: 242
currently lose_sum: 377.37892520427704
time_elpased: 1.111
batch start
#iterations: 243
currently lose_sum: 376.52235555648804
time_elpased: 1.128
batch start
#iterations: 244
currently lose_sum: 377.2500054240227
time_elpased: 1.127
batch start
#iterations: 245
currently lose_sum: 378.48503082990646
time_elpased: 1.128
batch start
#iterations: 246
currently lose_sum: 377.6921286582947
time_elpased: 1.113
batch start
#iterations: 247
currently lose_sum: 377.7773715853691
time_elpased: 1.143
batch start
#iterations: 248
currently lose_sum: 376.67295920848846
time_elpased: 1.118
batch start
#iterations: 249
currently lose_sum: 378.13052892684937
time_elpased: 1.14
batch start
#iterations: 250
currently lose_sum: 376.3186952471733
time_elpased: 1.111
batch start
#iterations: 251
currently lose_sum: 378.6145704984665
time_elpased: 1.138
batch start
#iterations: 252
currently lose_sum: 377.7167328596115
time_elpased: 1.121
batch start
#iterations: 253
currently lose_sum: 377.5207068324089
time_elpased: 1.114
batch start
#iterations: 254
currently lose_sum: 377.3711797595024
time_elpased: 1.14
batch start
#iterations: 255
currently lose_sum: 376.78875452280045
time_elpased: 1.12
batch start
#iterations: 256
currently lose_sum: 378.0798116326332
time_elpased: 1.137
batch start
#iterations: 257
currently lose_sum: 377.060151219368
time_elpased: 1.126
batch start
#iterations: 258
currently lose_sum: 375.5336063504219
time_elpased: 1.17
batch start
#iterations: 259
currently lose_sum: 377.37961196899414
time_elpased: 1.128
start validation test
0.642577319588
0.612535382127
0.779458680663
0.68598858799
0
validation finish
batch start
#iterations: 260
currently lose_sum: 375.47564882040024
time_elpased: 1.152
batch start
#iterations: 261
currently lose_sum: 375.6099964976311
time_elpased: 1.156
batch start
#iterations: 262
currently lose_sum: 376.1541889309883
time_elpased: 1.127
batch start
#iterations: 263
currently lose_sum: 376.4832840561867
time_elpased: 1.118
batch start
#iterations: 264
currently lose_sum: 377.4095694422722
time_elpased: 1.152
batch start
#iterations: 265
currently lose_sum: 376.3552415370941
time_elpased: 1.119
batch start
#iterations: 266
currently lose_sum: 377.44570058584213
time_elpased: 1.131
batch start
#iterations: 267
currently lose_sum: 377.06055921316147
time_elpased: 1.112
batch start
#iterations: 268
currently lose_sum: 377.32458633184433
time_elpased: 1.117
batch start
#iterations: 269
currently lose_sum: 377.0804074406624
time_elpased: 1.117
batch start
#iterations: 270
currently lose_sum: 376.13986390829086
time_elpased: 1.149
batch start
#iterations: 271
currently lose_sum: 376.77350771427155
time_elpased: 1.122
batch start
#iterations: 272
currently lose_sum: 376.47380524873734
time_elpased: 1.122
batch start
#iterations: 273
currently lose_sum: 376.4748246073723
time_elpased: 1.116
batch start
#iterations: 274
currently lose_sum: 377.5027245879173
time_elpased: 1.163
batch start
#iterations: 275
currently lose_sum: 377.06983429193497
time_elpased: 1.105
batch start
#iterations: 276
currently lose_sum: 376.5386507511139
time_elpased: 1.172
batch start
#iterations: 277
currently lose_sum: 375.98551028966904
time_elpased: 1.166
batch start
#iterations: 278
currently lose_sum: 377.2897026538849
time_elpased: 1.114
batch start
#iterations: 279
currently lose_sum: 376.9565997123718
time_elpased: 1.11
start validation test
0.641443298969
0.610131631432
0.787074199856
0.687398885493
0
validation finish
batch start
#iterations: 280
currently lose_sum: 376.48528975248337
time_elpased: 1.137
batch start
#iterations: 281
currently lose_sum: 376.61453998088837
time_elpased: 1.121
batch start
#iterations: 282
currently lose_sum: 375.7537139058113
time_elpased: 1.117
batch start
#iterations: 283
currently lose_sum: 375.80026227235794
time_elpased: 1.12
batch start
#iterations: 284
currently lose_sum: 375.8637462258339
time_elpased: 1.122
batch start
#iterations: 285
currently lose_sum: 375.5436540842056
time_elpased: 1.106
batch start
#iterations: 286
currently lose_sum: 375.9585784673691
time_elpased: 1.144
batch start
#iterations: 287
currently lose_sum: 377.62502896785736
time_elpased: 1.114
batch start
#iterations: 288
currently lose_sum: 375.4490460753441
time_elpased: 1.129
batch start
#iterations: 289
currently lose_sum: 375.57013416290283
time_elpased: 1.113
batch start
#iterations: 290
currently lose_sum: 376.5354017019272
time_elpased: 1.125
batch start
#iterations: 291
currently lose_sum: 377.4949746131897
time_elpased: 1.113
batch start
#iterations: 292
currently lose_sum: 377.1831897497177
time_elpased: 1.117
batch start
#iterations: 293
currently lose_sum: 375.87336015701294
time_elpased: 1.117
batch start
#iterations: 294
currently lose_sum: 376.7058990597725
time_elpased: 1.124
batch start
#iterations: 295
currently lose_sum: 377.79728841781616
time_elpased: 1.115
batch start
#iterations: 296
currently lose_sum: 376.2616329193115
time_elpased: 1.15
batch start
#iterations: 297
currently lose_sum: 376.53588950634
time_elpased: 1.169
batch start
#iterations: 298
currently lose_sum: 376.7328004837036
time_elpased: 1.163
batch start
#iterations: 299
currently lose_sum: 376.1977998018265
time_elpased: 1.134
start validation test
0.640360824742
0.609145952836
0.786868375013
0.686694508061
0
validation finish
batch start
#iterations: 300
currently lose_sum: 376.5650449991226
time_elpased: 1.109
batch start
#iterations: 301
currently lose_sum: 375.1499757170677
time_elpased: 1.156
batch start
#iterations: 302
currently lose_sum: 374.84241771698
time_elpased: 1.109
batch start
#iterations: 303
currently lose_sum: 376.9228322505951
time_elpased: 1.111
batch start
#iterations: 304
currently lose_sum: 376.5531951189041
time_elpased: 1.117
batch start
#iterations: 305
currently lose_sum: 375.4739999771118
time_elpased: 1.168
batch start
#iterations: 306
currently lose_sum: 375.57679384946823
time_elpased: 1.111
batch start
#iterations: 307
currently lose_sum: 375.37969851493835
time_elpased: 1.142
batch start
#iterations: 308
currently lose_sum: 375.305881857872
time_elpased: 1.141
batch start
#iterations: 309
currently lose_sum: 375.0762932896614
time_elpased: 1.116
batch start
#iterations: 310
currently lose_sum: 375.7988660335541
time_elpased: 1.156
batch start
#iterations: 311
currently lose_sum: 376.4076916575432
time_elpased: 1.127
batch start
#iterations: 312
currently lose_sum: 376.5807144641876
time_elpased: 1.171
batch start
#iterations: 313
currently lose_sum: 377.7639945745468
time_elpased: 1.183
batch start
#iterations: 314
currently lose_sum: 376.06531155109406
time_elpased: 1.116
batch start
#iterations: 315
currently lose_sum: 375.9818414449692
time_elpased: 1.169
batch start
#iterations: 316
currently lose_sum: 375.98860639333725
time_elpased: 1.174
batch start
#iterations: 317
currently lose_sum: 375.85292583703995
time_elpased: 1.111
batch start
#iterations: 318
currently lose_sum: 376.1348266005516
time_elpased: 1.133
batch start
#iterations: 319
currently lose_sum: 374.96537083387375
time_elpased: 1.143
start validation test
0.642989690722
0.613005101628
0.779047030977
0.686123447838
0
validation finish
batch start
#iterations: 320
currently lose_sum: 376.63662827014923
time_elpased: 1.122
batch start
#iterations: 321
currently lose_sum: 374.3577872514725
time_elpased: 1.122
batch start
#iterations: 322
currently lose_sum: 376.6034963130951
time_elpased: 1.127
batch start
#iterations: 323
currently lose_sum: 376.21912986040115
time_elpased: 1.141
batch start
#iterations: 324
currently lose_sum: 375.79750376939774
time_elpased: 1.136
batch start
#iterations: 325
currently lose_sum: 376.00300335884094
time_elpased: 1.128
batch start
#iterations: 326
currently lose_sum: 374.8601212501526
time_elpased: 1.115
batch start
#iterations: 327
currently lose_sum: 376.72558295726776
time_elpased: 1.107
batch start
#iterations: 328
currently lose_sum: 376.00527036190033
time_elpased: 1.118
batch start
#iterations: 329
currently lose_sum: 375.7255120277405
time_elpased: 1.109
batch start
#iterations: 330
currently lose_sum: 375.9075229167938
time_elpased: 1.134
batch start
#iterations: 331
currently lose_sum: 375.67035979032516
time_elpased: 1.127
batch start
#iterations: 332
currently lose_sum: 375.24651950597763
time_elpased: 1.114
batch start
#iterations: 333
currently lose_sum: 376.06340008974075
time_elpased: 1.119
batch start
#iterations: 334
currently lose_sum: 376.0304009914398
time_elpased: 1.106
batch start
#iterations: 335
currently lose_sum: 376.185209274292
time_elpased: 1.155
batch start
#iterations: 336
currently lose_sum: 375.54351192712784
time_elpased: 1.113
batch start
#iterations: 337
currently lose_sum: 375.55180752277374
time_elpased: 1.133
batch start
#iterations: 338
currently lose_sum: 376.13650465011597
time_elpased: 1.151
batch start
#iterations: 339
currently lose_sum: 376.2343491911888
time_elpased: 1.168
start validation test
0.645927835052
0.619223040857
0.761140269631
0.682886293338
0
validation finish
batch start
#iterations: 340
currently lose_sum: 375.89936339855194
time_elpased: 1.126
batch start
#iterations: 341
currently lose_sum: 375.92538422346115
time_elpased: 1.154
batch start
#iterations: 342
currently lose_sum: 374.741253554821
time_elpased: 1.119
batch start
#iterations: 343
currently lose_sum: 375.73140120506287
time_elpased: 1.12
batch start
#iterations: 344
currently lose_sum: 375.67576056718826
time_elpased: 1.137
batch start
#iterations: 345
currently lose_sum: 375.0826844573021
time_elpased: 1.151
batch start
#iterations: 346
currently lose_sum: 375.3076487183571
time_elpased: 1.167
batch start
#iterations: 347
currently lose_sum: 375.1145518422127
time_elpased: 1.13
batch start
#iterations: 348
currently lose_sum: 374.3620063662529
time_elpased: 1.117
batch start
#iterations: 349
currently lose_sum: 374.3513825535774
time_elpased: 1.112
batch start
#iterations: 350
currently lose_sum: 375.29905968904495
time_elpased: 1.125
batch start
#iterations: 351
currently lose_sum: 376.7911646962166
time_elpased: 1.167
batch start
#iterations: 352
currently lose_sum: 374.64169973134995
time_elpased: 1.14
batch start
#iterations: 353
currently lose_sum: 375.32111662626266
time_elpased: 1.124
batch start
#iterations: 354
currently lose_sum: 374.8980407714844
time_elpased: 1.163
batch start
#iterations: 355
currently lose_sum: 374.2868964076042
time_elpased: 1.151
batch start
#iterations: 356
currently lose_sum: 375.8147260546684
time_elpased: 1.133
batch start
#iterations: 357
currently lose_sum: 375.47955870628357
time_elpased: 1.15
batch start
#iterations: 358
currently lose_sum: 375.4169816374779
time_elpased: 1.122
batch start
#iterations: 359
currently lose_sum: 375.8636332154274
time_elpased: 1.122
start validation test
0.644329896907
0.614913926736
0.775650921066
0.685992536634
0
validation finish
batch start
#iterations: 360
currently lose_sum: 374.53747022151947
time_elpased: 1.13
batch start
#iterations: 361
currently lose_sum: 375.77356028556824
time_elpased: 1.111
batch start
#iterations: 362
currently lose_sum: 376.1438571214676
time_elpased: 1.105
batch start
#iterations: 363
currently lose_sum: 375.7460721731186
time_elpased: 1.113
batch start
#iterations: 364
currently lose_sum: 373.89329612255096
time_elpased: 1.125
batch start
#iterations: 365
currently lose_sum: 376.18921929597855
time_elpased: 1.113
batch start
#iterations: 366
currently lose_sum: 375.89182060956955
time_elpased: 1.138
batch start
#iterations: 367
currently lose_sum: 374.4079572558403
time_elpased: 1.12
batch start
#iterations: 368
currently lose_sum: 374.7168662548065
time_elpased: 1.118
batch start
#iterations: 369
currently lose_sum: 375.4290174841881
time_elpased: 1.184
batch start
#iterations: 370
currently lose_sum: 373.4817753434181
time_elpased: 1.175
batch start
#iterations: 371
currently lose_sum: 375.015453517437
time_elpased: 1.142
batch start
#iterations: 372
currently lose_sum: 375.4900393486023
time_elpased: 1.11
batch start
#iterations: 373
currently lose_sum: 375.8813336491585
time_elpased: 1.114
batch start
#iterations: 374
currently lose_sum: 375.7840645313263
time_elpased: 1.116
batch start
#iterations: 375
currently lose_sum: 375.44894248247147
time_elpased: 1.115
batch start
#iterations: 376
currently lose_sum: 375.95854461193085
time_elpased: 1.115
batch start
#iterations: 377
currently lose_sum: 374.72098475694656
time_elpased: 1.116
batch start
#iterations: 378
currently lose_sum: 372.66628259420395
time_elpased: 1.117
batch start
#iterations: 379
currently lose_sum: 374.8088558912277
time_elpased: 1.11
start validation test
0.645309278351
0.614873622813
0.781105279407
0.688092108245
0
validation finish
batch start
#iterations: 380
currently lose_sum: 373.5613189339638
time_elpased: 1.127
batch start
#iterations: 381
currently lose_sum: 375.29104340076447
time_elpased: 1.118
batch start
#iterations: 382
currently lose_sum: 374.89919555187225
time_elpased: 1.174
batch start
#iterations: 383
currently lose_sum: 376.1016266942024
time_elpased: 1.11
batch start
#iterations: 384
currently lose_sum: 373.981565952301
time_elpased: 1.112
batch start
#iterations: 385
currently lose_sum: 375.1805247068405
time_elpased: 1.173
batch start
#iterations: 386
currently lose_sum: 374.58763033151627
time_elpased: 1.124
batch start
#iterations: 387
currently lose_sum: 374.0557346343994
time_elpased: 1.117
batch start
#iterations: 388
currently lose_sum: 374.2532711625099
time_elpased: 1.156
batch start
#iterations: 389
currently lose_sum: 375.7176493406296
time_elpased: 1.177
batch start
#iterations: 390
currently lose_sum: 375.61897295713425
time_elpased: 1.119
batch start
#iterations: 391
currently lose_sum: 374.87203109264374
time_elpased: 1.124
batch start
#iterations: 392
currently lose_sum: 373.78224605321884
time_elpased: 1.179
batch start
#iterations: 393
currently lose_sum: 375.68162471055984
time_elpased: 1.12
batch start
#iterations: 394
currently lose_sum: 374.42393696308136
time_elpased: 1.166
batch start
#iterations: 395
currently lose_sum: 375.8569827079773
time_elpased: 1.122
batch start
#iterations: 396
currently lose_sum: 374.9839677810669
time_elpased: 1.114
batch start
#iterations: 397
currently lose_sum: 375.12430089712143
time_elpased: 1.11
batch start
#iterations: 398
currently lose_sum: 375.2091306447983
time_elpased: 1.162
batch start
#iterations: 399
currently lose_sum: 374.11406910419464
time_elpased: 1.131
start validation test
0.643505154639
0.612498995903
0.784707214161
0.687990616259
0
validation finish
acc: 0.643
pre: 0.613
rec: 0.778
F1: 0.686
auc: 0.000
