start to construct graph
graph construct over
93119
epochs start
batch start
#iterations: 0
currently lose_sum: 614.8018869161606
time_elpased: 1.798
batch start
#iterations: 1
currently lose_sum: 615.9371231198311
time_elpased: 1.839
batch start
#iterations: 2
currently lose_sum: 613.399282515049
time_elpased: 1.822
batch start
#iterations: 3
currently lose_sum: 615.159875690937
time_elpased: 1.862
batch start
#iterations: 4
currently lose_sum: 615.4754830598831
time_elpased: 1.867
batch start
#iterations: 5
currently lose_sum: 614.1017986536026
time_elpased: 1.84
batch start
#iterations: 6
currently lose_sum: 613.9158933758736
time_elpased: 1.812
batch start
#iterations: 7
currently lose_sum: 613.3900178670883
time_elpased: 1.845
batch start
#iterations: 8
currently lose_sum: 613.3152027726173
time_elpased: 1.809
batch start
#iterations: 9
currently lose_sum: 612.4547098875046
time_elpased: 1.814
batch start
#iterations: 10
currently lose_sum: 610.7649130821228
time_elpased: 1.808
batch start
#iterations: 11
currently lose_sum: 610.3843443393707
time_elpased: 1.812
batch start
#iterations: 12
currently lose_sum: 610.4761738181114
time_elpased: 1.834
batch start
#iterations: 13
currently lose_sum: 612.6517215371132
time_elpased: 1.818
batch start
#iterations: 14
currently lose_sum: 611.2517338395119
time_elpased: 1.829
batch start
#iterations: 15
currently lose_sum: 609.867575109005
time_elpased: 1.803
batch start
#iterations: 16
currently lose_sum: 608.5040466189384
time_elpased: 1.8
batch start
#iterations: 17
currently lose_sum: 608.9388570189476
time_elpased: 1.817
batch start
#iterations: 18
currently lose_sum: 608.5291755199432
time_elpased: 1.802
batch start
#iterations: 19
currently lose_sum: 608.1394283175468
time_elpased: 1.819
start validation test
0.54243767313
0.54150062621
0.978902953586
0.697284023018
0
validation finish
batch start
#iterations: 20
currently lose_sum: 608.9726233482361
time_elpased: 1.869
batch start
#iterations: 21
currently lose_sum: 607.9948180913925
time_elpased: 1.842
batch start
#iterations: 22
currently lose_sum: 606.160276889801
time_elpased: 1.868
batch start
#iterations: 23
currently lose_sum: 606.4909265041351
time_elpased: 1.861
batch start
#iterations: 24
currently lose_sum: 606.5357977747917
time_elpased: 1.904
batch start
#iterations: 25
currently lose_sum: 607.4219018816948
time_elpased: 1.842
batch start
#iterations: 26
currently lose_sum: 605.4509462118149
time_elpased: 1.813
batch start
#iterations: 27
currently lose_sum: 604.2177856564522
time_elpased: 1.804
batch start
#iterations: 28
currently lose_sum: 604.3180562853813
time_elpased: 1.809
batch start
#iterations: 29
currently lose_sum: 604.0812320113182
time_elpased: 1.836
batch start
#iterations: 30
currently lose_sum: 604.3434755206108
time_elpased: 1.838
batch start
#iterations: 31
currently lose_sum: 602.5772289037704
time_elpased: 1.83
batch start
#iterations: 32
currently lose_sum: 602.2860587239265
time_elpased: 1.815
batch start
#iterations: 33
currently lose_sum: 601.6611580848694
time_elpased: 1.831
batch start
#iterations: 34
currently lose_sum: 602.2760366797447
time_elpased: 1.81
batch start
#iterations: 35
currently lose_sum: 601.1956689357758
time_elpased: 1.819
batch start
#iterations: 36
currently lose_sum: 600.0548274517059
time_elpased: 1.793
batch start
#iterations: 37
currently lose_sum: 602.7614328861237
time_elpased: 1.82
batch start
#iterations: 38
currently lose_sum: 601.1925548911095
time_elpased: 1.817
batch start
#iterations: 39
currently lose_sum: 599.7288597226143
time_elpased: 1.797
start validation test
0.550193905817
0.545945945946
0.977050529999
0.700483270004
0
validation finish
batch start
#iterations: 40
currently lose_sum: 599.2297018766403
time_elpased: 1.814
batch start
#iterations: 41
currently lose_sum: 600.6343314647675
time_elpased: 1.775
batch start
#iterations: 42
currently lose_sum: 600.1467671394348
time_elpased: 1.826
batch start
#iterations: 43
currently lose_sum: 596.82151222229
time_elpased: 1.794
batch start
#iterations: 44
currently lose_sum: 599.982667863369
time_elpased: 1.841
batch start
#iterations: 45
currently lose_sum: 597.8685033321381
time_elpased: 1.792
batch start
#iterations: 46
currently lose_sum: 599.8479700684547
time_elpased: 1.806
batch start
#iterations: 47
currently lose_sum: 598.6018347144127
time_elpased: 1.811
batch start
#iterations: 48
currently lose_sum: 597.9626422524452
time_elpased: 1.818
batch start
#iterations: 49
currently lose_sum: 598.065453350544
time_elpased: 1.834
batch start
#iterations: 50
currently lose_sum: 599.4341306686401
time_elpased: 1.812
batch start
#iterations: 51
currently lose_sum: 596.8359359502792
time_elpased: 1.803
batch start
#iterations: 52
currently lose_sum: 598.6555001139641
time_elpased: 1.781
batch start
#iterations: 53
currently lose_sum: 598.188735306263
time_elpased: 1.801
batch start
#iterations: 54
currently lose_sum: 597.9931977391243
time_elpased: 1.802
batch start
#iterations: 55
currently lose_sum: 598.3879774808884
time_elpased: 1.831
batch start
#iterations: 56
currently lose_sum: 596.9740570783615
time_elpased: 1.796
batch start
#iterations: 57
currently lose_sum: 596.2149061560631
time_elpased: 1.825
batch start
#iterations: 58
currently lose_sum: 594.5921840071678
time_elpased: 1.814
batch start
#iterations: 59
currently lose_sum: 597.1341705918312
time_elpased: 1.819
start validation test
0.57324099723
0.565791192996
0.891221570444
0.692163209847
0
validation finish
batch start
#iterations: 60
currently lose_sum: 594.9164605736732
time_elpased: 1.811
batch start
#iterations: 61
currently lose_sum: 594.3179369568825
time_elpased: 1.781
batch start
#iterations: 62
currently lose_sum: 597.3367148041725
time_elpased: 1.812
batch start
#iterations: 63
currently lose_sum: 596.0201112031937
time_elpased: 1.798
batch start
#iterations: 64
currently lose_sum: 595.5168638825417
time_elpased: 1.776
batch start
#iterations: 65
currently lose_sum: 594.2648478746414
time_elpased: 1.803
batch start
#iterations: 66
currently lose_sum: 595.0839875340462
time_elpased: 1.806
batch start
#iterations: 67
currently lose_sum: 593.6219285726547
time_elpased: 1.793
batch start
#iterations: 68
currently lose_sum: 594.1579103469849
time_elpased: 1.822
batch start
#iterations: 69
currently lose_sum: 594.9932291507721
time_elpased: 1.812
batch start
#iterations: 70
currently lose_sum: 592.2227215766907
time_elpased: 1.834
batch start
#iterations: 71
currently lose_sum: 595.217649102211
time_elpased: 1.823
batch start
#iterations: 72
currently lose_sum: 593.397370994091
time_elpased: 1.858
batch start
#iterations: 73
currently lose_sum: 595.5672053098679
time_elpased: 1.857
batch start
#iterations: 74
currently lose_sum: 594.9410035014153
time_elpased: 1.872
batch start
#iterations: 75
currently lose_sum: 593.7197588086128
time_elpased: 1.892
batch start
#iterations: 76
currently lose_sum: 594.5563117265701
time_elpased: 1.81
batch start
#iterations: 77
currently lose_sum: 594.2479764223099
time_elpased: 1.849
batch start
#iterations: 78
currently lose_sum: 595.0799338817596
time_elpased: 1.856
batch start
#iterations: 79
currently lose_sum: 595.646177649498
time_elpased: 1.894
start validation test
0.572409972299
0.562566510172
0.924873932284
0.699595204733
0
validation finish
batch start
#iterations: 80
currently lose_sum: 594.4332895874977
time_elpased: 1.937
batch start
#iterations: 81
currently lose_sum: 594.4833179712296
time_elpased: 1.85
batch start
#iterations: 82
currently lose_sum: 594.8738422989845
time_elpased: 1.826
batch start
#iterations: 83
currently lose_sum: 595.0437440872192
time_elpased: 1.867
batch start
#iterations: 84
currently lose_sum: 592.9772511720657
time_elpased: 1.83
batch start
#iterations: 85
currently lose_sum: 593.1630567312241
time_elpased: 1.868
batch start
#iterations: 86
currently lose_sum: 592.1494476795197
time_elpased: 1.835
batch start
#iterations: 87
currently lose_sum: 594.5525808334351
time_elpased: 1.81
batch start
#iterations: 88
currently lose_sum: 591.9830879569054
time_elpased: 1.825
batch start
#iterations: 89
currently lose_sum: 592.3268771767616
time_elpased: 1.922
batch start
#iterations: 90
currently lose_sum: 591.9865430593491
time_elpased: 1.874
batch start
#iterations: 91
currently lose_sum: 593.0760833621025
time_elpased: 1.885
batch start
#iterations: 92
currently lose_sum: 594.5763467550278
time_elpased: 1.847
batch start
#iterations: 93
currently lose_sum: 591.1822292208672
time_elpased: 1.896
batch start
#iterations: 94
currently lose_sum: 593.6870731711388
time_elpased: 1.911
batch start
#iterations: 95
currently lose_sum: 591.7288271188736
time_elpased: 1.873
batch start
#iterations: 96
currently lose_sum: 594.2686820030212
time_elpased: 1.885
batch start
#iterations: 97
currently lose_sum: 592.394603908062
time_elpased: 1.845
batch start
#iterations: 98
currently lose_sum: 593.1587830781937
time_elpased: 1.921
batch start
#iterations: 99
currently lose_sum: 590.8202424049377
time_elpased: 1.858
start validation test
0.570886426593
0.560310807917
0.942471956365
0.702799148169
0
validation finish
batch start
#iterations: 100
currently lose_sum: 593.971982061863
time_elpased: 1.966
batch start
#iterations: 101
currently lose_sum: 592.3864715695381
time_elpased: 1.968
batch start
#iterations: 102
currently lose_sum: 592.2792571187019
time_elpased: 1.872
batch start
#iterations: 103
currently lose_sum: 591.6771873831749
time_elpased: 1.927
batch start
#iterations: 104
currently lose_sum: 592.4983805418015
time_elpased: 1.87
batch start
#iterations: 105
currently lose_sum: 591.0661489963531
time_elpased: 1.905
batch start
#iterations: 106
currently lose_sum: 591.3780446648598
time_elpased: 1.949
batch start
#iterations: 107
currently lose_sum: 591.9355146884918
time_elpased: 1.915
batch start
#iterations: 108
currently lose_sum: 592.3617165088654
time_elpased: 1.889
batch start
#iterations: 109
currently lose_sum: 592.1189904808998
time_elpased: 1.863
batch start
#iterations: 110
currently lose_sum: 592.8839353322983
time_elpased: 1.875
batch start
#iterations: 111
currently lose_sum: 593.3182763457298
time_elpased: 1.947
batch start
#iterations: 112
currently lose_sum: 592.7248068451881
time_elpased: 1.967
batch start
#iterations: 113
currently lose_sum: 592.7046248912811
time_elpased: 1.894
batch start
#iterations: 114
currently lose_sum: 591.7915253043175
time_elpased: 1.939
batch start
#iterations: 115
currently lose_sum: 591.4427614808083
time_elpased: 1.926
batch start
#iterations: 116
currently lose_sum: 591.8560557365417
time_elpased: 1.945
batch start
#iterations: 117
currently lose_sum: 590.4789971113205
time_elpased: 1.903
batch start
#iterations: 118
currently lose_sum: 592.1841136217117
time_elpased: 1.903
batch start
#iterations: 119
currently lose_sum: 593.6214795708656
time_elpased: 1.899
start validation test
0.574570637119
0.562423425631
0.94483894206
0.705118851043
0
validation finish
batch start
#iterations: 120
currently lose_sum: 592.6696652770042
time_elpased: 1.917
batch start
#iterations: 121
currently lose_sum: 590.4338895082474
time_elpased: 1.864
batch start
#iterations: 122
currently lose_sum: 590.9310830235481
time_elpased: 1.918
batch start
#iterations: 123
currently lose_sum: 591.4941341280937
time_elpased: 1.9
batch start
#iterations: 124
currently lose_sum: 589.8759454488754
time_elpased: 1.899
batch start
#iterations: 125
currently lose_sum: 592.8856974840164
time_elpased: 1.874
batch start
#iterations: 126
currently lose_sum: 591.3663688898087
time_elpased: 1.984
batch start
#iterations: 127
currently lose_sum: 591.2220249772072
time_elpased: 1.914
batch start
#iterations: 128
currently lose_sum: 589.4620687365532
time_elpased: 1.913
batch start
#iterations: 129
currently lose_sum: 590.3856652379036
time_elpased: 1.933
batch start
#iterations: 130
currently lose_sum: 590.5813225507736
time_elpased: 1.89
batch start
#iterations: 131
currently lose_sum: 589.9227929115295
time_elpased: 1.889
batch start
#iterations: 132
currently lose_sum: 590.0997327566147
time_elpased: 1.912
batch start
#iterations: 133
currently lose_sum: 590.7775048613548
time_elpased: 1.912
batch start
#iterations: 134
currently lose_sum: 590.0967891812325
time_elpased: 1.877
batch start
#iterations: 135
currently lose_sum: 590.4118348956108
time_elpased: 1.912
batch start
#iterations: 136
currently lose_sum: 592.4207888841629
time_elpased: 1.848
batch start
#iterations: 137
currently lose_sum: 590.3077387213707
time_elpased: 1.963
batch start
#iterations: 138
currently lose_sum: 590.0738324522972
time_elpased: 1.908
batch start
#iterations: 139
currently lose_sum: 590.9056860208511
time_elpased: 1.893
start validation test
0.573905817175
0.561881490531
0.946588453226
0.705178824702
0
validation finish
batch start
#iterations: 140
currently lose_sum: 588.8826671242714
time_elpased: 1.859
batch start
#iterations: 141
currently lose_sum: 591.7527700066566
time_elpased: 1.92
batch start
#iterations: 142
currently lose_sum: 590.0912615060806
time_elpased: 1.897
batch start
#iterations: 143
currently lose_sum: 590.1470507979393
time_elpased: 1.917
batch start
#iterations: 144
currently lose_sum: 590.8144100308418
time_elpased: 1.872
batch start
#iterations: 145
currently lose_sum: 591.070513188839
time_elpased: 1.921
batch start
#iterations: 146
currently lose_sum: 591.954741358757
time_elpased: 1.852
batch start
#iterations: 147
currently lose_sum: 591.0224351882935
time_elpased: 1.926
batch start
#iterations: 148
currently lose_sum: 591.5028536319733
time_elpased: 1.884
batch start
#iterations: 149
currently lose_sum: 589.2558915615082
time_elpased: 1.907
batch start
#iterations: 150
currently lose_sum: 589.2087658047676
time_elpased: 1.928
batch start
#iterations: 151
currently lose_sum: 589.8589578270912
time_elpased: 1.889
batch start
#iterations: 152
currently lose_sum: 591.0501058101654
time_elpased: 1.94
batch start
#iterations: 153
currently lose_sum: 590.3636282682419
time_elpased: 1.944
batch start
#iterations: 154
currently lose_sum: 591.1908987164497
time_elpased: 1.929
batch start
#iterations: 155
currently lose_sum: 589.0777326226234
time_elpased: 1.915
batch start
#iterations: 156
currently lose_sum: 590.1901380419731
time_elpased: 1.887
batch start
#iterations: 157
currently lose_sum: 589.114926815033
time_elpased: 1.973
batch start
#iterations: 158
currently lose_sum: 589.3362842798233
time_elpased: 1.859
batch start
#iterations: 159
currently lose_sum: 589.911240696907
time_elpased: 1.897
start validation test
0.576592797784
0.564838255977
0.929916640939
0.702794143382
0
validation finish
batch start
#iterations: 160
currently lose_sum: 589.9587714076042
time_elpased: 1.976
batch start
#iterations: 161
currently lose_sum: 589.0844887495041
time_elpased: 1.894
batch start
#iterations: 162
currently lose_sum: 589.0047293305397
time_elpased: 1.915
batch start
#iterations: 163
currently lose_sum: 590.8633204102516
time_elpased: 1.92
batch start
#iterations: 164
currently lose_sum: 589.1058207154274
time_elpased: 1.94
batch start
#iterations: 165
currently lose_sum: 590.6404278278351
time_elpased: 1.891
batch start
#iterations: 166
currently lose_sum: 590.2702355384827
time_elpased: 1.94
batch start
#iterations: 167
currently lose_sum: 589.6014143824577
time_elpased: 1.884
batch start
#iterations: 168
currently lose_sum: 589.8297329545021
time_elpased: 1.89
batch start
#iterations: 169
currently lose_sum: 588.9588223099709
time_elpased: 1.891
batch start
#iterations: 170
currently lose_sum: 589.663401722908
time_elpased: 1.878
batch start
#iterations: 171
currently lose_sum: 588.7768285274506
time_elpased: 1.884
batch start
#iterations: 172
currently lose_sum: 588.9676117300987
time_elpased: 1.888
batch start
#iterations: 173
currently lose_sum: 590.3965709209442
time_elpased: 1.879
batch start
#iterations: 174
currently lose_sum: 589.527405321598
time_elpased: 1.909
batch start
#iterations: 175
currently lose_sum: 589.4211375117302
time_elpased: 1.9
batch start
#iterations: 176
currently lose_sum: 589.9939852952957
time_elpased: 1.908
batch start
#iterations: 177
currently lose_sum: 589.2957582473755
time_elpased: 1.89
batch start
#iterations: 178
currently lose_sum: 589.5792197585106
time_elpased: 1.955
batch start
#iterations: 179
currently lose_sum: 588.1470818817616
time_elpased: 1.927
start validation test
0.578254847645
0.565513806307
0.93475352475
0.70469577361
0
validation finish
batch start
#iterations: 180
currently lose_sum: 589.5261607766151
time_elpased: 1.936
batch start
#iterations: 181
currently lose_sum: 589.465057015419
time_elpased: 1.916
batch start
#iterations: 182
currently lose_sum: 589.6482644081116
time_elpased: 1.879
batch start
#iterations: 183
currently lose_sum: 588.1441084742546
time_elpased: 1.879
batch start
#iterations: 184
currently lose_sum: 590.7747985720634
time_elpased: 1.854
batch start
#iterations: 185
currently lose_sum: 589.9754182100296
time_elpased: 1.885
batch start
#iterations: 186
currently lose_sum: 589.8985759615898
time_elpased: 1.946
batch start
#iterations: 187
currently lose_sum: 588.9325665235519
time_elpased: 1.957
batch start
#iterations: 188
currently lose_sum: 588.3868574500084
time_elpased: 1.916
batch start
#iterations: 189
currently lose_sum: 590.6888279914856
time_elpased: 1.88
batch start
#iterations: 190
currently lose_sum: 589.4090682864189
time_elpased: 1.893
batch start
#iterations: 191
currently lose_sum: 588.0984479784966
time_elpased: 1.919
batch start
#iterations: 192
currently lose_sum: 591.2491464614868
time_elpased: 1.867
batch start
#iterations: 193
currently lose_sum: 588.2898064255714
time_elpased: 1.871
batch start
#iterations: 194
currently lose_sum: 589.0629134774208
time_elpased: 1.882
batch start
#iterations: 195
currently lose_sum: 589.7781711816788
time_elpased: 1.862
batch start
#iterations: 196
currently lose_sum: 589.417588531971
time_elpased: 1.845
batch start
#iterations: 197
currently lose_sum: 589.4121179580688
time_elpased: 1.869
batch start
#iterations: 198
currently lose_sum: 586.7756930589676
time_elpased: 1.91
batch start
#iterations: 199
currently lose_sum: 590.031690120697
time_elpased: 1.96
start validation test
0.577174515235
0.563968828619
0.945868066276
0.70661951257
0
validation finish
batch start
#iterations: 200
currently lose_sum: 589.2531189322472
time_elpased: 1.928
batch start
#iterations: 201
currently lose_sum: 589.8288418650627
time_elpased: 1.93
batch start
#iterations: 202
currently lose_sum: 588.2755620479584
time_elpased: 1.938
batch start
#iterations: 203
currently lose_sum: 589.7387717366219
time_elpased: 1.962
batch start
#iterations: 204
currently lose_sum: 588.468387901783
time_elpased: 1.944
batch start
#iterations: 205
currently lose_sum: 588.721275806427
time_elpased: 1.924
batch start
#iterations: 206
currently lose_sum: 588.8392672538757
time_elpased: 1.891
batch start
#iterations: 207
currently lose_sum: 589.0042840242386
time_elpased: 1.889
batch start
#iterations: 208
currently lose_sum: 589.9791231155396
time_elpased: 1.928
batch start
#iterations: 209
currently lose_sum: 587.88071423769
time_elpased: 1.961
batch start
#iterations: 210
currently lose_sum: 588.1434080004692
time_elpased: 1.9
batch start
#iterations: 211
currently lose_sum: 589.4641350507736
time_elpased: 1.857
batch start
#iterations: 212
currently lose_sum: 588.3753030896187
time_elpased: 1.912
batch start
#iterations: 213
currently lose_sum: 587.8407627940178
time_elpased: 1.917
batch start
#iterations: 214
currently lose_sum: 587.6949309110641
time_elpased: 1.934
batch start
#iterations: 215
currently lose_sum: 588.1913121342659
time_elpased: 1.935
batch start
#iterations: 216
currently lose_sum: 590.4884083867073
time_elpased: 1.901
batch start
#iterations: 217
currently lose_sum: 587.8335831165314
time_elpased: 1.875
batch start
#iterations: 218
currently lose_sum: 589.8346695303917
time_elpased: 1.936
batch start
#iterations: 219
currently lose_sum: 587.6876556277275
time_elpased: 1.961
start validation test
0.584819944598
0.572481252038
0.903468148606
0.70086220661
0
validation finish
batch start
#iterations: 220
currently lose_sum: 588.2144974470139
time_elpased: 2.018
batch start
#iterations: 221
currently lose_sum: 589.1147683858871
time_elpased: 1.916
batch start
#iterations: 222
currently lose_sum: 588.4844581484795
time_elpased: 1.897
batch start
#iterations: 223
currently lose_sum: 590.6004944443703
time_elpased: 1.923
batch start
#iterations: 224
currently lose_sum: 588.243756711483
time_elpased: 1.925
batch start
#iterations: 225
currently lose_sum: 588.7481386065483
time_elpased: 1.909
batch start
#iterations: 226
currently lose_sum: 589.8020604252815
time_elpased: 1.928
batch start
#iterations: 227
currently lose_sum: 588.892071723938
time_elpased: 1.873
batch start
#iterations: 228
currently lose_sum: 590.1587616205215
time_elpased: 1.904
batch start
#iterations: 229
currently lose_sum: 589.4531213641167
time_elpased: 1.882
batch start
#iterations: 230
currently lose_sum: 590.2941237688065
time_elpased: 1.861
batch start
#iterations: 231
currently lose_sum: 586.6388444900513
time_elpased: 1.896
batch start
#iterations: 232
currently lose_sum: 588.5630276203156
time_elpased: 1.92
batch start
#iterations: 233
currently lose_sum: 588.9064664840698
time_elpased: 1.886
batch start
#iterations: 234
currently lose_sum: 587.8846802711487
time_elpased: 1.884
batch start
#iterations: 235
currently lose_sum: 587.9115849733353
time_elpased: 1.904
batch start
#iterations: 236
currently lose_sum: 590.5792604088783
time_elpased: 1.945
batch start
#iterations: 237
currently lose_sum: 588.2403993010521
time_elpased: 1.883
batch start
#iterations: 238
currently lose_sum: 587.5579250454903
time_elpased: 1.9
batch start
#iterations: 239
currently lose_sum: 587.7914872169495
time_elpased: 1.896
start validation test
0.582825484765
0.569468267581
0.922506946588
0.704218713175
0
validation finish
batch start
#iterations: 240
currently lose_sum: 588.1206426620483
time_elpased: 1.939
batch start
#iterations: 241
currently lose_sum: 589.2761008739471
time_elpased: 1.897
batch start
#iterations: 242
currently lose_sum: 587.8087542057037
time_elpased: 1.911
batch start
#iterations: 243
currently lose_sum: 587.2608476877213
time_elpased: 1.904
batch start
#iterations: 244
currently lose_sum: 588.0061234235764
time_elpased: 1.902
batch start
#iterations: 245
currently lose_sum: 589.707172691822
time_elpased: 1.876
batch start
#iterations: 246
currently lose_sum: 587.7446383833885
time_elpased: 1.92
batch start
#iterations: 247
currently lose_sum: 587.5578766465187
time_elpased: 1.913
batch start
#iterations: 248
currently lose_sum: 587.4468997716904
time_elpased: 1.872
batch start
#iterations: 249
currently lose_sum: 589.6244176030159
time_elpased: 1.907
batch start
#iterations: 250
currently lose_sum: 587.5553615093231
time_elpased: 1.907
batch start
#iterations: 251
currently lose_sum: 588.3904907107353
time_elpased: 1.882
batch start
#iterations: 252
currently lose_sum: 589.0745062828064
time_elpased: 1.916
batch start
#iterations: 253
currently lose_sum: 587.6846849918365
time_elpased: 1.89
batch start
#iterations: 254
currently lose_sum: 588.9836216568947
time_elpased: 1.882
batch start
#iterations: 255
currently lose_sum: 585.4013564586639
time_elpased: 1.886
batch start
#iterations: 256
currently lose_sum: 589.6096472144127
time_elpased: 1.929
batch start
#iterations: 257
currently lose_sum: 588.8246122002602
time_elpased: 1.878
batch start
#iterations: 258
currently lose_sum: 586.7607498764992
time_elpased: 1.872
batch start
#iterations: 259
currently lose_sum: 587.6355935335159
time_elpased: 1.904
start validation test
0.581911357341
0.568145427145
0.931151589997
0.705703422053
0
validation finish
batch start
#iterations: 260
currently lose_sum: 588.8624510765076
time_elpased: 1.91
batch start
#iterations: 261
currently lose_sum: 590.5862044095993
time_elpased: 1.931
batch start
#iterations: 262
currently lose_sum: 587.1114161610603
time_elpased: 1.885
batch start
#iterations: 263
currently lose_sum: 588.2980033159256
time_elpased: 1.917
batch start
#iterations: 264
currently lose_sum: 587.590617954731
time_elpased: 1.898
batch start
#iterations: 265
currently lose_sum: 587.9214752912521
time_elpased: 1.934
batch start
#iterations: 266
currently lose_sum: 587.5486441254616
time_elpased: 1.889
batch start
#iterations: 267
currently lose_sum: 587.2891910076141
time_elpased: 1.89
batch start
#iterations: 268
currently lose_sum: 588.2797971367836
time_elpased: 1.93
batch start
#iterations: 269
currently lose_sum: 588.1287208795547
time_elpased: 1.888
batch start
#iterations: 270
currently lose_sum: 587.648375749588
time_elpased: 1.871
batch start
#iterations: 271
currently lose_sum: 589.5726175308228
time_elpased: 1.919
batch start
#iterations: 272
currently lose_sum: 589.0164371132851
time_elpased: 1.892
batch start
#iterations: 273
currently lose_sum: 587.0141485929489
time_elpased: 1.93
batch start
#iterations: 274
currently lose_sum: 588.3298242688179
time_elpased: 1.951
batch start
#iterations: 275
currently lose_sum: 588.6256911754608
time_elpased: 1.887
batch start
#iterations: 276
currently lose_sum: 588.3801621198654
time_elpased: 1.906
batch start
#iterations: 277
currently lose_sum: 587.5278093218803
time_elpased: 1.892
batch start
#iterations: 278
currently lose_sum: 588.5299450755119
time_elpased: 1.912
batch start
#iterations: 279
currently lose_sum: 587.7935688495636
time_elpased: 1.915
start validation test
0.585872576177
0.572744970798
0.908305032417
0.702511242886
0
validation finish
batch start
#iterations: 280
currently lose_sum: 586.8616442084312
time_elpased: 1.873
batch start
#iterations: 281
currently lose_sum: 586.624607026577
time_elpased: 1.909
batch start
#iterations: 282
currently lose_sum: 588.9615195393562
time_elpased: 1.892
batch start
#iterations: 283
currently lose_sum: 588.5340713858604
time_elpased: 1.981
batch start
#iterations: 284
currently lose_sum: 588.5460562109947
time_elpased: 1.928
batch start
#iterations: 285
currently lose_sum: 588.0451249480247
time_elpased: 1.898
batch start
#iterations: 286
currently lose_sum: 588.0910402536392
time_elpased: 1.903
batch start
#iterations: 287
currently lose_sum: 588.9335420131683
time_elpased: 1.954
batch start
#iterations: 288
currently lose_sum: 586.7504758834839
time_elpased: 1.869
batch start
#iterations: 289
currently lose_sum: 587.5631648302078
time_elpased: 1.896
batch start
#iterations: 290
currently lose_sum: 589.5272712111473
time_elpased: 1.935
batch start
#iterations: 291
currently lose_sum: 587.3445479273796
time_elpased: 1.929
batch start
#iterations: 292
currently lose_sum: 587.8082651495934
time_elpased: 1.908
batch start
#iterations: 293
currently lose_sum: 588.9248107075691
time_elpased: 1.934
batch start
#iterations: 294
currently lose_sum: 588.795868575573
time_elpased: 1.933
batch start
#iterations: 295
currently lose_sum: 588.1058554649353
time_elpased: 1.871
batch start
#iterations: 296
currently lose_sum: 586.8812284469604
time_elpased: 1.898
batch start
#iterations: 297
currently lose_sum: 588.4492841959
time_elpased: 1.906
batch start
#iterations: 298
currently lose_sum: 587.640687763691
time_elpased: 1.903
batch start
#iterations: 299
currently lose_sum: 587.4745789170265
time_elpased: 1.967
start validation test
0.582686980609
0.568723062887
0.930225378203
0.705882352941
0
validation finish
batch start
#iterations: 300
currently lose_sum: 587.9714963436127
time_elpased: 1.941
batch start
#iterations: 301
currently lose_sum: 585.9402132034302
time_elpased: 1.944
batch start
#iterations: 302
currently lose_sum: 587.2149140238762
time_elpased: 1.947
batch start
#iterations: 303
currently lose_sum: 587.7044837474823
time_elpased: 1.905
batch start
#iterations: 304
currently lose_sum: 585.7927942872047
time_elpased: 1.901
batch start
#iterations: 305
currently lose_sum: 588.4885468482971
time_elpased: 1.921
batch start
#iterations: 306
currently lose_sum: 588.7196565270424
time_elpased: 1.937
batch start
#iterations: 307
currently lose_sum: 588.2880278229713
time_elpased: 1.917
batch start
#iterations: 308
currently lose_sum: 587.3011256456375
time_elpased: 1.942
batch start
#iterations: 309
currently lose_sum: 587.5339042544365
time_elpased: 1.902
batch start
#iterations: 310
currently lose_sum: 588.8944070339203
time_elpased: 1.957
batch start
#iterations: 311
currently lose_sum: 588.1184136867523
time_elpased: 1.92
batch start
#iterations: 312
currently lose_sum: 587.3423399329185
time_elpased: 1.893
batch start
#iterations: 313
currently lose_sum: 586.4055456519127
time_elpased: 1.929
batch start
#iterations: 314
currently lose_sum: 587.8867270350456
time_elpased: 1.947
batch start
#iterations: 315
currently lose_sum: 588.5392299294472
time_elpased: 1.892
batch start
#iterations: 316
currently lose_sum: 586.8283994793892
time_elpased: 1.918
batch start
#iterations: 317
currently lose_sum: 587.063058078289
time_elpased: 1.875
batch start
#iterations: 318
currently lose_sum: 586.6406690478325
time_elpased: 1.954
batch start
#iterations: 319
currently lose_sum: 586.9876812696457
time_elpased: 1.896
start validation test
0.585844875346
0.572350795649
0.912421529279
0.703441436081
0
validation finish
batch start
#iterations: 320
currently lose_sum: 588.1294395327568
time_elpased: 1.947
batch start
#iterations: 321
currently lose_sum: 587.5931835770607
time_elpased: 1.927
batch start
#iterations: 322
currently lose_sum: 586.6128317713737
time_elpased: 1.925
batch start
#iterations: 323
currently lose_sum: 588.8258419036865
time_elpased: 1.914
batch start
#iterations: 324
currently lose_sum: 586.9555243253708
time_elpased: 1.92
batch start
#iterations: 325
currently lose_sum: 588.4133614897728
time_elpased: 1.862
batch start
#iterations: 326
currently lose_sum: 587.5068076848984
time_elpased: 1.93
batch start
#iterations: 327
currently lose_sum: 590.0223723649979
time_elpased: 1.848
batch start
#iterations: 328
currently lose_sum: 588.8175864815712
time_elpased: 1.887
batch start
#iterations: 329
currently lose_sum: 588.135564148426
time_elpased: 1.874
batch start
#iterations: 330
currently lose_sum: 587.8388033509254
time_elpased: 1.866
batch start
#iterations: 331
currently lose_sum: 586.0916785001755
time_elpased: 1.838
batch start
#iterations: 332
currently lose_sum: 587.7551535964012
time_elpased: 1.835
batch start
#iterations: 333
currently lose_sum: 588.8202531337738
time_elpased: 1.831
batch start
#iterations: 334
currently lose_sum: 590.1749309301376
time_elpased: 1.913
batch start
#iterations: 335
currently lose_sum: 588.9849764704704
time_elpased: 1.86
batch start
#iterations: 336
currently lose_sum: 586.8745902776718
time_elpased: 1.896
batch start
#iterations: 337
currently lose_sum: 586.9649886488914
time_elpased: 1.9
batch start
#iterations: 338
currently lose_sum: 587.9315139651299
time_elpased: 1.945
batch start
#iterations: 339
currently lose_sum: 586.4370816349983
time_elpased: 1.91
start validation test
0.577950138504
0.564422073538
0.946279715962
0.707090126115
0
validation finish
batch start
#iterations: 340
currently lose_sum: 588.198971748352
time_elpased: 1.885
batch start
#iterations: 341
currently lose_sum: 587.4108195304871
time_elpased: 1.882
batch start
#iterations: 342
currently lose_sum: 587.6475898623466
time_elpased: 1.901
batch start
#iterations: 343
currently lose_sum: 587.952107667923
time_elpased: 1.937
batch start
#iterations: 344
currently lose_sum: 587.7127969264984
time_elpased: 1.923
batch start
#iterations: 345
currently lose_sum: 586.8842057585716
time_elpased: 1.928
batch start
#iterations: 346
currently lose_sum: 589.1070890426636
time_elpased: 1.95
batch start
#iterations: 347
currently lose_sum: 588.511433005333
time_elpased: 1.991
batch start
#iterations: 348
currently lose_sum: 586.8835343718529
time_elpased: 2.005
batch start
#iterations: 349
currently lose_sum: 587.014776468277
time_elpased: 1.9
batch start
#iterations: 350
currently lose_sum: 587.5539081692696
time_elpased: 1.868
batch start
#iterations: 351
currently lose_sum: 587.3261834979057
time_elpased: 1.837
batch start
#iterations: 352
currently lose_sum: 587.4711912870407
time_elpased: 1.84
batch start
#iterations: 353
currently lose_sum: 590.7444851994514
time_elpased: 1.855
batch start
#iterations: 354
currently lose_sum: 587.4700483083725
time_elpased: 1.885
batch start
#iterations: 355
currently lose_sum: 586.2516782283783
time_elpased: 1.886
batch start
#iterations: 356
currently lose_sum: 589.3071798682213
time_elpased: 1.857
batch start
#iterations: 357
currently lose_sum: 587.2896847724915
time_elpased: 1.861
batch start
#iterations: 358
currently lose_sum: 587.8106281757355
time_elpased: 1.909
batch start
#iterations: 359
currently lose_sum: 588.3418678641319
time_elpased: 1.879
start validation test
0.586121883657
0.572629400925
0.911392405063
0.703345577286
0
validation finish
batch start
#iterations: 360
currently lose_sum: 589.58043384552
time_elpased: 1.899
batch start
#iterations: 361
currently lose_sum: 586.8483483195305
time_elpased: 1.925
batch start
#iterations: 362
currently lose_sum: 587.8672615885735
time_elpased: 1.88
batch start
#iterations: 363
currently lose_sum: 588.3731797337532
time_elpased: 1.891
batch start
#iterations: 364
currently lose_sum: 587.2739210724831
time_elpased: 1.935
batch start
#iterations: 365
currently lose_sum: 587.3421144485474
time_elpased: 1.866
batch start
#iterations: 366
currently lose_sum: 588.4639675021172
time_elpased: 1.915
batch start
#iterations: 367
currently lose_sum: 588.4646697044373
time_elpased: 1.87
batch start
#iterations: 368
currently lose_sum: 588.0769822001457
time_elpased: 1.952
batch start
#iterations: 369
currently lose_sum: 586.20452272892
time_elpased: 1.919
batch start
#iterations: 370
currently lose_sum: 586.676427423954
time_elpased: 1.883
batch start
#iterations: 371
currently lose_sum: 588.7978474497795
time_elpased: 1.853
batch start
#iterations: 372
currently lose_sum: 586.8255094885826
time_elpased: 1.951
batch start
#iterations: 373
currently lose_sum: 587.2473363280296
time_elpased: 1.955
batch start
#iterations: 374
currently lose_sum: 586.4496935606003
time_elpased: 1.955
batch start
#iterations: 375
currently lose_sum: 588.4101841449738
time_elpased: 1.901
batch start
#iterations: 376
currently lose_sum: 587.2315168976784
time_elpased: 1.912
batch start
#iterations: 377
currently lose_sum: 588.6665157675743
time_elpased: 1.861
batch start
#iterations: 378
currently lose_sum: 586.0837855935097
time_elpased: 1.899
batch start
#iterations: 379
currently lose_sum: 585.9307022094727
time_elpased: 1.914
start validation test
0.585900277008
0.572801350518
0.907893382731
0.702430479527
0
validation finish
batch start
#iterations: 380
currently lose_sum: 588.4216921329498
time_elpased: 1.865
batch start
#iterations: 381
currently lose_sum: 589.0489936470985
time_elpased: 1.853
batch start
#iterations: 382
currently lose_sum: 589.3279680609703
time_elpased: 1.895
batch start
#iterations: 383
currently lose_sum: 586.6445234417915
time_elpased: 1.867
batch start
#iterations: 384
currently lose_sum: 588.9793734550476
time_elpased: 1.905
batch start
#iterations: 385
currently lose_sum: 588.088546693325
time_elpased: 1.922
batch start
#iterations: 386
currently lose_sum: 587.6793745160103
time_elpased: 1.959
batch start
#iterations: 387
currently lose_sum: 588.7029563784599
time_elpased: 1.947
batch start
#iterations: 388
currently lose_sum: 585.6823214888573
time_elpased: 1.894
batch start
#iterations: 389
currently lose_sum: 586.372988820076
time_elpased: 1.945
batch start
#iterations: 390
currently lose_sum: 587.6778252124786
time_elpased: 1.913
batch start
#iterations: 391
currently lose_sum: 587.1771749854088
time_elpased: 1.911
batch start
#iterations: 392
currently lose_sum: 587.1295533776283
time_elpased: 1.87
batch start
#iterations: 393
currently lose_sum: 588.5609933137894
time_elpased: 1.869
batch start
#iterations: 394
currently lose_sum: 587.8837810754776
time_elpased: 1.92
batch start
#iterations: 395
currently lose_sum: 587.4095253944397
time_elpased: 1.904
batch start
#iterations: 396
currently lose_sum: 587.624028980732
time_elpased: 1.858
batch start
#iterations: 397
currently lose_sum: 588.5346770882607
time_elpased: 1.852
batch start
#iterations: 398
currently lose_sum: 588.5502864122391
time_elpased: 1.866
batch start
#iterations: 399
currently lose_sum: 586.2088373303413
time_elpased: 1.908
start validation test
0.584847645429
0.571309451268
0.916640938561
0.703902005334
0
validation finish
acc: 0.578
pre: 0.564
rec: 0.949
F1: 0.708
auc: 0.000
