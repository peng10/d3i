start to construct graph
graph construct over
93281
epochs start
batch start
#iterations: 0
currently lose_sum: 616.9208938479424
time_elpased: 1.869
batch start
#iterations: 1
currently lose_sum: 617.08513712883
time_elpased: 1.834
batch start
#iterations: 2
currently lose_sum: 615.9839883446693
time_elpased: 1.831
batch start
#iterations: 3
currently lose_sum: 615.6635292172432
time_elpased: 1.885
batch start
#iterations: 4
currently lose_sum: 615.3569250702858
time_elpased: 1.823
batch start
#iterations: 5
currently lose_sum: 614.0598166584969
time_elpased: 1.873
batch start
#iterations: 6
currently lose_sum: 613.9316215515137
time_elpased: 1.875
batch start
#iterations: 7
currently lose_sum: 614.4856783151627
time_elpased: 1.833
batch start
#iterations: 8
currently lose_sum: 613.722141623497
time_elpased: 1.848
batch start
#iterations: 9
currently lose_sum: 613.7639348506927
time_elpased: 1.818
batch start
#iterations: 10
currently lose_sum: 615.0363509654999
time_elpased: 1.819
batch start
#iterations: 11
currently lose_sum: 612.4479115009308
time_elpased: 1.82
batch start
#iterations: 12
currently lose_sum: 610.7361564040184
time_elpased: 1.834
batch start
#iterations: 13
currently lose_sum: 612.0581113100052
time_elpased: 1.814
batch start
#iterations: 14
currently lose_sum: 612.6841372251511
time_elpased: 1.828
batch start
#iterations: 15
currently lose_sum: 611.1412897706032
time_elpased: 1.826
batch start
#iterations: 16
currently lose_sum: 611.819727897644
time_elpased: 1.794
batch start
#iterations: 17
currently lose_sum: 609.5655070543289
time_elpased: 1.825
batch start
#iterations: 18
currently lose_sum: 610.0047091841698
time_elpased: 1.854
batch start
#iterations: 19
currently lose_sum: 610.7684856653214
time_elpased: 1.83
start validation test
0.540692520776
0.539964675208
0.991148620832
0.699079871509
0
validation finish
batch start
#iterations: 20
currently lose_sum: 609.2114562988281
time_elpased: 1.832
batch start
#iterations: 21
currently lose_sum: 609.523753464222
time_elpased: 1.829
batch start
#iterations: 22
currently lose_sum: 609.1889250278473
time_elpased: 1.823
batch start
#iterations: 23
currently lose_sum: 608.9453482031822
time_elpased: 1.841
batch start
#iterations: 24
currently lose_sum: 607.9723033308983
time_elpased: 1.815
batch start
#iterations: 25
currently lose_sum: 609.0310164690018
time_elpased: 1.845
batch start
#iterations: 26
currently lose_sum: 606.7702933549881
time_elpased: 1.824
batch start
#iterations: 27
currently lose_sum: 604.4524208903313
time_elpased: 1.813
batch start
#iterations: 28
currently lose_sum: 607.2411172389984
time_elpased: 1.82
batch start
#iterations: 29
currently lose_sum: 607.7623043656349
time_elpased: 1.825
batch start
#iterations: 30
currently lose_sum: 606.0832145214081
time_elpased: 1.799
batch start
#iterations: 31
currently lose_sum: 605.5836465358734
time_elpased: 1.836
batch start
#iterations: 32
currently lose_sum: 603.9140644669533
time_elpased: 1.81
batch start
#iterations: 33
currently lose_sum: 604.4407649636269
time_elpased: 1.816
batch start
#iterations: 34
currently lose_sum: 604.9476422667503
time_elpased: 1.849
batch start
#iterations: 35
currently lose_sum: 602.878395140171
time_elpased: 1.845
batch start
#iterations: 36
currently lose_sum: 605.2414627075195
time_elpased: 1.826
batch start
#iterations: 37
currently lose_sum: 603.41025608778
time_elpased: 1.83
batch start
#iterations: 38
currently lose_sum: 604.4658548235893
time_elpased: 1.787
batch start
#iterations: 39
currently lose_sum: 603.0695733428001
time_elpased: 1.8
start validation test
0.555041551247
0.55081907865
0.939584191025
0.694497803306
0
validation finish
batch start
#iterations: 40
currently lose_sum: 603.452398121357
time_elpased: 1.861
batch start
#iterations: 41
currently lose_sum: 602.6844831705093
time_elpased: 1.913
batch start
#iterations: 42
currently lose_sum: 602.7629873752594
time_elpased: 1.899
batch start
#iterations: 43
currently lose_sum: 600.929844558239
time_elpased: 1.933
batch start
#iterations: 44
currently lose_sum: 602.1943364143372
time_elpased: 1.979
batch start
#iterations: 45
currently lose_sum: 601.6911380887032
time_elpased: 1.952
batch start
#iterations: 46
currently lose_sum: 598.5084781646729
time_elpased: 1.937
batch start
#iterations: 47
currently lose_sum: 601.3614528179169
time_elpased: 1.976
batch start
#iterations: 48
currently lose_sum: 600.8565149903297
time_elpased: 1.901
batch start
#iterations: 49
currently lose_sum: 601.5589318275452
time_elpased: 1.912
batch start
#iterations: 50
currently lose_sum: 601.2066848278046
time_elpased: 1.959
batch start
#iterations: 51
currently lose_sum: 599.0972096920013
time_elpased: 1.879
batch start
#iterations: 52
currently lose_sum: 600.7712957859039
time_elpased: 1.948
batch start
#iterations: 53
currently lose_sum: 599.9056015610695
time_elpased: 1.91
batch start
#iterations: 54
currently lose_sum: 600.4399328827858
time_elpased: 1.912
batch start
#iterations: 55
currently lose_sum: 602.6173137426376
time_elpased: 1.972
batch start
#iterations: 56
currently lose_sum: 598.6027375459671
time_elpased: 1.797
batch start
#iterations: 57
currently lose_sum: 597.7534722685814
time_elpased: 1.849
batch start
#iterations: 58
currently lose_sum: 600.0213984251022
time_elpased: 1.896
batch start
#iterations: 59
currently lose_sum: 598.4204969406128
time_elpased: 1.952
start validation test
0.557091412742
0.551069447329
0.955948950185
0.699121205849
0
validation finish
batch start
#iterations: 60
currently lose_sum: 597.8607078790665
time_elpased: 1.867
batch start
#iterations: 61
currently lose_sum: 597.2345435023308
time_elpased: 1.902
batch start
#iterations: 62
currently lose_sum: 597.6573656797409
time_elpased: 1.917
batch start
#iterations: 63
currently lose_sum: 596.7439358234406
time_elpased: 1.943
batch start
#iterations: 64
currently lose_sum: 598.9912904500961
time_elpased: 1.892
batch start
#iterations: 65
currently lose_sum: 599.2925571799278
time_elpased: 1.852
batch start
#iterations: 66
currently lose_sum: 596.1476392149925
time_elpased: 1.945
batch start
#iterations: 67
currently lose_sum: 597.1779871582985
time_elpased: 1.981
batch start
#iterations: 68
currently lose_sum: 599.5588141679764
time_elpased: 1.948
batch start
#iterations: 69
currently lose_sum: 597.6975553035736
time_elpased: 1.942
batch start
#iterations: 70
currently lose_sum: 596.4600179195404
time_elpased: 1.966
batch start
#iterations: 71
currently lose_sum: 597.7663449645042
time_elpased: 1.974
batch start
#iterations: 72
currently lose_sum: 597.960972905159
time_elpased: 1.926
batch start
#iterations: 73
currently lose_sum: 595.7944196462631
time_elpased: 1.918
batch start
#iterations: 74
currently lose_sum: 596.2588520646095
time_elpased: 1.99
batch start
#iterations: 75
currently lose_sum: 598.0243428945541
time_elpased: 1.956
batch start
#iterations: 76
currently lose_sum: 596.3291122913361
time_elpased: 1.894
batch start
#iterations: 77
currently lose_sum: 594.9284422397614
time_elpased: 1.904
batch start
#iterations: 78
currently lose_sum: 595.7936697006226
time_elpased: 1.865
batch start
#iterations: 79
currently lose_sum: 595.5774763226509
time_elpased: 1.867
start validation test
0.57351800554
0.564971023825
0.903046521202
0.695080408778
0
validation finish
batch start
#iterations: 80
currently lose_sum: 595.7618623971939
time_elpased: 1.843
batch start
#iterations: 81
currently lose_sum: 596.7564699053764
time_elpased: 1.87
batch start
#iterations: 82
currently lose_sum: 596.5093075633049
time_elpased: 1.844
batch start
#iterations: 83
currently lose_sum: 596.4990440011024
time_elpased: 1.836
batch start
#iterations: 84
currently lose_sum: 594.5648420453072
time_elpased: 1.829
batch start
#iterations: 85
currently lose_sum: 595.8360211849213
time_elpased: 1.82
batch start
#iterations: 86
currently lose_sum: 595.0403771400452
time_elpased: 1.812
batch start
#iterations: 87
currently lose_sum: 595.6194063425064
time_elpased: 1.853
batch start
#iterations: 88
currently lose_sum: 594.3514568209648
time_elpased: 1.821
batch start
#iterations: 89
currently lose_sum: 595.3621891736984
time_elpased: 1.824
batch start
#iterations: 90
currently lose_sum: 595.9515120387077
time_elpased: 1.831
batch start
#iterations: 91
currently lose_sum: 595.6505986452103
time_elpased: 1.821
batch start
#iterations: 92
currently lose_sum: 592.5579500794411
time_elpased: 1.815
batch start
#iterations: 93
currently lose_sum: 594.6053956747055
time_elpased: 1.832
batch start
#iterations: 94
currently lose_sum: 595.9877281785011
time_elpased: 1.812
batch start
#iterations: 95
currently lose_sum: 593.7445495724678
time_elpased: 1.824
batch start
#iterations: 96
currently lose_sum: 591.4882943630219
time_elpased: 1.831
batch start
#iterations: 97
currently lose_sum: 596.2819007635117
time_elpased: 1.825
batch start
#iterations: 98
currently lose_sum: 594.7510458230972
time_elpased: 1.888
batch start
#iterations: 99
currently lose_sum: 595.4269244074821
time_elpased: 1.894
start validation test
0.565761772853
0.55622754491
0.956051873199
0.703285887341
0
validation finish
batch start
#iterations: 100
currently lose_sum: 595.0681289434433
time_elpased: 1.855
batch start
#iterations: 101
currently lose_sum: 592.5513405203819
time_elpased: 1.842
batch start
#iterations: 102
currently lose_sum: 594.2222347259521
time_elpased: 1.837
batch start
#iterations: 103
currently lose_sum: 594.5233500599861
time_elpased: 1.817
batch start
#iterations: 104
currently lose_sum: 594.8157451152802
time_elpased: 1.841
batch start
#iterations: 105
currently lose_sum: 593.0266273617744
time_elpased: 1.843
batch start
#iterations: 106
currently lose_sum: 592.2280953526497
time_elpased: 1.832
batch start
#iterations: 107
currently lose_sum: 594.3645308017731
time_elpased: 1.834
batch start
#iterations: 108
currently lose_sum: 594.5717358589172
time_elpased: 1.852
batch start
#iterations: 109
currently lose_sum: 592.8701327443123
time_elpased: 1.87
batch start
#iterations: 110
currently lose_sum: 592.6424197554588
time_elpased: 1.87
batch start
#iterations: 111
currently lose_sum: 592.1532778739929
time_elpased: 1.85
batch start
#iterations: 112
currently lose_sum: 592.5035572648048
time_elpased: 1.862
batch start
#iterations: 113
currently lose_sum: 593.0486461520195
time_elpased: 1.834
batch start
#iterations: 114
currently lose_sum: 592.1275991201401
time_elpased: 1.817
batch start
#iterations: 115
currently lose_sum: 592.3236368298531
time_elpased: 1.833
batch start
#iterations: 116
currently lose_sum: 591.6742700338364
time_elpased: 1.842
batch start
#iterations: 117
currently lose_sum: 592.0287746191025
time_elpased: 1.842
batch start
#iterations: 118
currently lose_sum: 592.6669008731842
time_elpased: 1.852
batch start
#iterations: 119
currently lose_sum: 593.6122520565987
time_elpased: 1.833
start validation test
0.572520775623
0.561109753117
0.945039110745
0.704141104294
0
validation finish
batch start
#iterations: 120
currently lose_sum: 593.7451016902924
time_elpased: 1.833
batch start
#iterations: 121
currently lose_sum: 591.8052509427071
time_elpased: 1.821
batch start
#iterations: 122
currently lose_sum: 593.0630877614021
time_elpased: 1.826
batch start
#iterations: 123
currently lose_sum: 593.062197804451
time_elpased: 1.837
batch start
#iterations: 124
currently lose_sum: 591.0978451371193
time_elpased: 1.843
batch start
#iterations: 125
currently lose_sum: 592.3035494685173
time_elpased: 1.847
batch start
#iterations: 126
currently lose_sum: 592.1015706062317
time_elpased: 1.814
batch start
#iterations: 127
currently lose_sum: 592.7390864491463
time_elpased: 1.814
batch start
#iterations: 128
currently lose_sum: 592.9330969452858
time_elpased: 1.826
batch start
#iterations: 129
currently lose_sum: 591.3253875374794
time_elpased: 1.83
batch start
#iterations: 130
currently lose_sum: 590.8144426941872
time_elpased: 1.832
batch start
#iterations: 131
currently lose_sum: 592.2271114587784
time_elpased: 1.839
batch start
#iterations: 132
currently lose_sum: 592.4264430999756
time_elpased: 1.799
batch start
#iterations: 133
currently lose_sum: 593.353372991085
time_elpased: 1.838
batch start
#iterations: 134
currently lose_sum: 592.9720452427864
time_elpased: 1.825
batch start
#iterations: 135
currently lose_sum: 591.3257871866226
time_elpased: 1.806
batch start
#iterations: 136
currently lose_sum: 592.9798029661179
time_elpased: 1.816
batch start
#iterations: 137
currently lose_sum: 591.6274380683899
time_elpased: 1.798
batch start
#iterations: 138
currently lose_sum: 590.2889730334282
time_elpased: 1.808
batch start
#iterations: 139
currently lose_sum: 593.0738236904144
time_elpased: 1.82
start validation test
0.577811634349
0.570657854807
0.870934540963
0.689523111084
0
validation finish
batch start
#iterations: 140
currently lose_sum: 593.9118472337723
time_elpased: 1.859
batch start
#iterations: 141
currently lose_sum: 591.5421318411827
time_elpased: 1.819
batch start
#iterations: 142
currently lose_sum: 592.3610273003578
time_elpased: 1.841
batch start
#iterations: 143
currently lose_sum: 590.6207974553108
time_elpased: 1.824
batch start
#iterations: 144
currently lose_sum: 591.7707461118698
time_elpased: 1.8
batch start
#iterations: 145
currently lose_sum: 591.1317862868309
time_elpased: 1.837
batch start
#iterations: 146
currently lose_sum: 592.3950371742249
time_elpased: 1.837
batch start
#iterations: 147
currently lose_sum: 592.1166349053383
time_elpased: 1.842
batch start
#iterations: 148
currently lose_sum: 591.0695080161095
time_elpased: 1.857
batch start
#iterations: 149
currently lose_sum: 591.0197740793228
time_elpased: 1.942
batch start
#iterations: 150
currently lose_sum: 591.8211984038353
time_elpased: 2.014
batch start
#iterations: 151
currently lose_sum: 590.4452426433563
time_elpased: 1.918
batch start
#iterations: 152
currently lose_sum: 592.3373824954033
time_elpased: 1.942
batch start
#iterations: 153
currently lose_sum: 592.3205769062042
time_elpased: 1.888
batch start
#iterations: 154
currently lose_sum: 590.8794794082642
time_elpased: 1.956
batch start
#iterations: 155
currently lose_sum: 592.5183774232864
time_elpased: 1.884
batch start
#iterations: 156
currently lose_sum: 592.6325914859772
time_elpased: 1.987
batch start
#iterations: 157
currently lose_sum: 591.2680689692497
time_elpased: 1.851
batch start
#iterations: 158
currently lose_sum: 592.2724205851555
time_elpased: 1.965
batch start
#iterations: 159
currently lose_sum: 589.6139255166054
time_elpased: 1.924
start validation test
0.576703601108
0.565628458498
0.920543433512
0.700707052902
0
validation finish
batch start
#iterations: 160
currently lose_sum: 588.2422541975975
time_elpased: 1.906
batch start
#iterations: 161
currently lose_sum: 590.1607710719109
time_elpased: 1.873
batch start
#iterations: 162
currently lose_sum: 590.4009578227997
time_elpased: 1.983
batch start
#iterations: 163
currently lose_sum: 591.9697514176369
time_elpased: 1.943
batch start
#iterations: 164
currently lose_sum: 590.0805146694183
time_elpased: 1.866
batch start
#iterations: 165
currently lose_sum: 591.0772810578346
time_elpased: 1.897
batch start
#iterations: 166
currently lose_sum: 592.033597111702
time_elpased: 1.918
batch start
#iterations: 167
currently lose_sum: 591.0073598623276
time_elpased: 1.883
batch start
#iterations: 168
currently lose_sum: 590.3741971254349
time_elpased: 1.86
batch start
#iterations: 169
currently lose_sum: 590.747548699379
time_elpased: 1.847
batch start
#iterations: 170
currently lose_sum: 589.9564033150673
time_elpased: 1.904
batch start
#iterations: 171
currently lose_sum: 591.1671181023121
time_elpased: 1.948
batch start
#iterations: 172
currently lose_sum: 590.9820654988289
time_elpased: 1.864
batch start
#iterations: 173
currently lose_sum: 592.3905111551285
time_elpased: 1.833
batch start
#iterations: 174
currently lose_sum: 591.5412589907646
time_elpased: 1.938
batch start
#iterations: 175
currently lose_sum: 591.230940580368
time_elpased: 1.954
batch start
#iterations: 176
currently lose_sum: 590.6359399557114
time_elpased: 1.886
batch start
#iterations: 177
currently lose_sum: 593.3806350231171
time_elpased: 1.856
batch start
#iterations: 178
currently lose_sum: 591.3645896315575
time_elpased: 1.885
batch start
#iterations: 179
currently lose_sum: 591.6402755379677
time_elpased: 1.892
start validation test
0.580415512465
0.568278147806
0.917661589131
0.701895258901
0
validation finish
batch start
#iterations: 180
currently lose_sum: 589.1329758763313
time_elpased: 1.911
batch start
#iterations: 181
currently lose_sum: 590.1706917285919
time_elpased: 1.879
batch start
#iterations: 182
currently lose_sum: 590.2869053483009
time_elpased: 1.86
batch start
#iterations: 183
currently lose_sum: 591.0885493159294
time_elpased: 1.992
batch start
#iterations: 184
currently lose_sum: 591.6336476802826
time_elpased: 1.9
batch start
#iterations: 185
currently lose_sum: 590.3332056999207
time_elpased: 1.948
batch start
#iterations: 186
currently lose_sum: 590.0327227711678
time_elpased: 1.959
batch start
#iterations: 187
currently lose_sum: 589.8620883822441
time_elpased: 1.981
batch start
#iterations: 188
currently lose_sum: 589.2964714765549
time_elpased: 1.872
batch start
#iterations: 189
currently lose_sum: 589.6217680573463
time_elpased: 1.949
batch start
#iterations: 190
currently lose_sum: 591.4257362484932
time_elpased: 1.855
batch start
#iterations: 191
currently lose_sum: 589.128466129303
time_elpased: 1.957
batch start
#iterations: 192
currently lose_sum: 590.8690321445465
time_elpased: 1.899
batch start
#iterations: 193
currently lose_sum: 591.4103426337242
time_elpased: 1.929
batch start
#iterations: 194
currently lose_sum: 591.0088459253311
time_elpased: 1.865
batch start
#iterations: 195
currently lose_sum: 591.5336165428162
time_elpased: 1.844
batch start
#iterations: 196
currently lose_sum: 590.3534073233604
time_elpased: 1.937
batch start
#iterations: 197
currently lose_sum: 587.4075650572777
time_elpased: 1.875
batch start
#iterations: 198
currently lose_sum: 591.2158409357071
time_elpased: 1.86
batch start
#iterations: 199
currently lose_sum: 590.6685743331909
time_elpased: 1.911
start validation test
0.581274238227
0.569353387325
0.911692054343
0.700957505737
0
validation finish
batch start
#iterations: 200
currently lose_sum: 591.4318900108337
time_elpased: 1.931
batch start
#iterations: 201
currently lose_sum: 591.2170470356941
time_elpased: 1.916
batch start
#iterations: 202
currently lose_sum: 590.1135876178741
time_elpased: 1.873
batch start
#iterations: 203
currently lose_sum: 590.8656599521637
time_elpased: 1.858
batch start
#iterations: 204
currently lose_sum: 590.723028242588
time_elpased: 1.886
batch start
#iterations: 205
currently lose_sum: 590.1160210371017
time_elpased: 1.839
batch start
#iterations: 206
currently lose_sum: 588.2470804452896
time_elpased: 1.938
batch start
#iterations: 207
currently lose_sum: 588.6697565317154
time_elpased: 1.91
batch start
#iterations: 208
currently lose_sum: 592.5145129263401
time_elpased: 1.851
batch start
#iterations: 209
currently lose_sum: 589.4716110825539
time_elpased: 1.946
batch start
#iterations: 210
currently lose_sum: 587.9747675657272
time_elpased: 1.885
batch start
#iterations: 211
currently lose_sum: 588.9380565285683
time_elpased: 1.839
batch start
#iterations: 212
currently lose_sum: 587.770435154438
time_elpased: 1.917
batch start
#iterations: 213
currently lose_sum: 590.7085735201836
time_elpased: 1.887
batch start
#iterations: 214
currently lose_sum: 590.0616679191589
time_elpased: 1.9
batch start
#iterations: 215
currently lose_sum: 591.582009613514
time_elpased: 1.868
batch start
#iterations: 216
currently lose_sum: 589.2688045501709
time_elpased: 1.933
batch start
#iterations: 217
currently lose_sum: 589.4846855401993
time_elpased: 1.944
batch start
#iterations: 218
currently lose_sum: 590.3492863178253
time_elpased: 1.931
batch start
#iterations: 219
currently lose_sum: 590.7531744241714
time_elpased: 1.867
start validation test
0.572271468144
0.560009623192
0.958316179498
0.706918477745
0
validation finish
batch start
#iterations: 220
currently lose_sum: 589.0985147953033
time_elpased: 1.954
batch start
#iterations: 221
currently lose_sum: 590.6439859867096
time_elpased: 1.889
batch start
#iterations: 222
currently lose_sum: 592.1754255890846
time_elpased: 1.91
batch start
#iterations: 223
currently lose_sum: 589.4669097661972
time_elpased: 1.862
batch start
#iterations: 224
currently lose_sum: 589.0823324918747
time_elpased: 1.885
batch start
#iterations: 225
currently lose_sum: 591.5353660583496
time_elpased: 1.889
batch start
#iterations: 226
currently lose_sum: 590.5998315215111
time_elpased: 1.888
batch start
#iterations: 227
currently lose_sum: 589.817848265171
time_elpased: 1.903
batch start
#iterations: 228
currently lose_sum: 590.6808606386185
time_elpased: 1.91
batch start
#iterations: 229
currently lose_sum: 589.8057404756546
time_elpased: 1.898
batch start
#iterations: 230
currently lose_sum: 589.2259817123413
time_elpased: 1.882
batch start
#iterations: 231
currently lose_sum: 589.8911489844322
time_elpased: 1.854
batch start
#iterations: 232
currently lose_sum: 590.3315642476082
time_elpased: 1.861
batch start
#iterations: 233
currently lose_sum: 588.8244588971138
time_elpased: 1.869
batch start
#iterations: 234
currently lose_sum: 589.56970256567
time_elpased: 1.855
batch start
#iterations: 235
currently lose_sum: 590.5421072840691
time_elpased: 1.842
batch start
#iterations: 236
currently lose_sum: 589.700670003891
time_elpased: 1.806
batch start
#iterations: 237
currently lose_sum: 587.6122193932533
time_elpased: 1.836
batch start
#iterations: 238
currently lose_sum: 591.1379956007004
time_elpased: 1.813
batch start
#iterations: 239
currently lose_sum: 588.4724678397179
time_elpased: 1.825
start validation test
0.578448753463
0.565922032413
0.93083573487
0.70389539635
0
validation finish
batch start
#iterations: 240
currently lose_sum: 588.3947095870972
time_elpased: 1.818
batch start
#iterations: 241
currently lose_sum: 589.6623067259789
time_elpased: 1.851
batch start
#iterations: 242
currently lose_sum: 589.6740652918816
time_elpased: 1.849
batch start
#iterations: 243
currently lose_sum: 591.2018810510635
time_elpased: 1.814
batch start
#iterations: 244
currently lose_sum: 589.6824361085892
time_elpased: 1.816
batch start
#iterations: 245
currently lose_sum: 590.9714141488075
time_elpased: 1.817
batch start
#iterations: 246
currently lose_sum: 588.6413086652756
time_elpased: 1.805
batch start
#iterations: 247
currently lose_sum: 590.5450068116188
time_elpased: 1.793
batch start
#iterations: 248
currently lose_sum: 588.8873696923256
time_elpased: 1.823
batch start
#iterations: 249
currently lose_sum: 586.1782880425453
time_elpased: 1.832
batch start
#iterations: 250
currently lose_sum: 588.2478596568108
time_elpased: 1.835
batch start
#iterations: 251
currently lose_sum: 589.1431322097778
time_elpased: 1.821
batch start
#iterations: 252
currently lose_sum: 588.5114559531212
time_elpased: 1.826
batch start
#iterations: 253
currently lose_sum: 590.9007451534271
time_elpased: 1.814
batch start
#iterations: 254
currently lose_sum: 590.2281509041786
time_elpased: 1.857
batch start
#iterations: 255
currently lose_sum: 589.857305586338
time_elpased: 1.818
batch start
#iterations: 256
currently lose_sum: 589.157058596611
time_elpased: 1.83
batch start
#iterations: 257
currently lose_sum: 589.0968562960625
time_elpased: 1.828
batch start
#iterations: 258
currently lose_sum: 591.0908833146095
time_elpased: 1.801
batch start
#iterations: 259
currently lose_sum: 587.5766414403915
time_elpased: 1.81
start validation test
0.58324099723
0.574383668487
0.871655002058
0.692463359294
0
validation finish
batch start
#iterations: 260
currently lose_sum: 590.4475036263466
time_elpased: 1.836
batch start
#iterations: 261
currently lose_sum: 589.5092149972916
time_elpased: 1.826
batch start
#iterations: 262
currently lose_sum: 588.8734863996506
time_elpased: 1.808
batch start
#iterations: 263
currently lose_sum: 589.1162505149841
time_elpased: 1.823
batch start
#iterations: 264
currently lose_sum: 590.4984567761421
time_elpased: 1.81
batch start
#iterations: 265
currently lose_sum: 590.8554617166519
time_elpased: 1.828
batch start
#iterations: 266
currently lose_sum: 588.3553511500359
time_elpased: 1.822
batch start
#iterations: 267
currently lose_sum: 589.4126325249672
time_elpased: 1.814
batch start
#iterations: 268
currently lose_sum: 589.4146515727043
time_elpased: 1.803
batch start
#iterations: 269
currently lose_sum: 588.6071004271507
time_elpased: 1.808
batch start
#iterations: 270
currently lose_sum: 587.0682538747787
time_elpased: 1.806
batch start
#iterations: 271
currently lose_sum: 588.6954613924026
time_elpased: 1.823
batch start
#iterations: 272
currently lose_sum: 590.1945739984512
time_elpased: 1.805
batch start
#iterations: 273
currently lose_sum: 589.9452188014984
time_elpased: 1.811
batch start
#iterations: 274
currently lose_sum: 589.6528345346451
time_elpased: 1.819
batch start
#iterations: 275
currently lose_sum: 588.2580990791321
time_elpased: 1.815
batch start
#iterations: 276
currently lose_sum: 589.32790350914
time_elpased: 1.842
batch start
#iterations: 277
currently lose_sum: 588.8554936647415
time_elpased: 1.825
batch start
#iterations: 278
currently lose_sum: 589.6680731773376
time_elpased: 1.862
batch start
#iterations: 279
currently lose_sum: 589.9428544640541
time_elpased: 1.812
start validation test
0.575983379501
0.563494751101
0.941951420338
0.705152653376
0
validation finish
batch start
#iterations: 280
currently lose_sum: 589.5422197580338
time_elpased: 1.843
batch start
#iterations: 281
currently lose_sum: 589.5048513412476
time_elpased: 1.811
batch start
#iterations: 282
currently lose_sum: 588.6993860602379
time_elpased: 1.829
batch start
#iterations: 283
currently lose_sum: 590.2079827189445
time_elpased: 1.803
batch start
#iterations: 284
currently lose_sum: 590.2282174825668
time_elpased: 1.819
batch start
#iterations: 285
currently lose_sum: 589.5987437367439
time_elpased: 1.828
batch start
#iterations: 286
currently lose_sum: 587.5403363108635
time_elpased: 1.821
batch start
#iterations: 287
currently lose_sum: 589.0036816000938
time_elpased: 1.827
batch start
#iterations: 288
currently lose_sum: 588.0056087970734
time_elpased: 1.818
batch start
#iterations: 289
currently lose_sum: 589.2624115943909
time_elpased: 1.83
batch start
#iterations: 290
currently lose_sum: 588.1579447984695
time_elpased: 1.817
batch start
#iterations: 291
currently lose_sum: 587.7550575733185
time_elpased: 1.86
batch start
#iterations: 292
currently lose_sum: 587.7948192358017
time_elpased: 1.809
batch start
#iterations: 293
currently lose_sum: 590.5939002037048
time_elpased: 1.848
batch start
#iterations: 294
currently lose_sum: 588.3647941350937
time_elpased: 1.797
batch start
#iterations: 295
currently lose_sum: 588.7304778695107
time_elpased: 1.837
batch start
#iterations: 296
currently lose_sum: 590.5344904661179
time_elpased: 1.799
batch start
#iterations: 297
currently lose_sum: 588.3482438325882
time_elpased: 1.831
batch start
#iterations: 298
currently lose_sum: 588.3740828037262
time_elpased: 1.813
batch start
#iterations: 299
currently lose_sum: 587.6841052174568
time_elpased: 1.828
start validation test
0.584182825485
0.571015517075
0.914676821737
0.703099349275
0
validation finish
batch start
#iterations: 300
currently lose_sum: 589.6589320600033
time_elpased: 1.816
batch start
#iterations: 301
currently lose_sum: 589.7098143696785
time_elpased: 1.783
batch start
#iterations: 302
currently lose_sum: 590.1006860733032
time_elpased: 1.782
batch start
#iterations: 303
currently lose_sum: 588.5057995319366
time_elpased: 1.785
batch start
#iterations: 304
currently lose_sum: 589.2928162813187
time_elpased: 1.829
batch start
#iterations: 305
currently lose_sum: 590.4977985024452
time_elpased: 1.803
batch start
#iterations: 306
currently lose_sum: 589.6298129558563
time_elpased: 1.831
batch start
#iterations: 307
currently lose_sum: 588.156013250351
time_elpased: 1.801
batch start
#iterations: 308
currently lose_sum: 590.5796310305595
time_elpased: 1.828
batch start
#iterations: 309
currently lose_sum: 589.8315358161926
time_elpased: 1.845
batch start
#iterations: 310
currently lose_sum: 590.6186060905457
time_elpased: 1.822
batch start
#iterations: 311
currently lose_sum: 588.1305629611015
time_elpased: 1.816
batch start
#iterations: 312
currently lose_sum: 588.2193279266357
time_elpased: 1.795
batch start
#iterations: 313
currently lose_sum: 589.173519551754
time_elpased: 1.802
batch start
#iterations: 314
currently lose_sum: 591.2551819086075
time_elpased: 1.823
batch start
#iterations: 315
currently lose_sum: 589.1755208969116
time_elpased: 1.806
batch start
#iterations: 316
currently lose_sum: 588.726262152195
time_elpased: 1.83
batch start
#iterations: 317
currently lose_sum: 588.652017056942
time_elpased: 1.835
batch start
#iterations: 318
currently lose_sum: 590.2222049236298
time_elpased: 1.839
batch start
#iterations: 319
currently lose_sum: 589.3625858426094
time_elpased: 1.847
start validation test
0.580110803324
0.566986395837
0.93083573487
0.704718120544
0
validation finish
batch start
#iterations: 320
currently lose_sum: 587.4396122694016
time_elpased: 1.819
batch start
#iterations: 321
currently lose_sum: 586.9855219721794
time_elpased: 1.813
batch start
#iterations: 322
currently lose_sum: 587.8478792905807
time_elpased: 1.809
batch start
#iterations: 323
currently lose_sum: 587.3573008775711
time_elpased: 1.813
batch start
#iterations: 324
currently lose_sum: 588.8993595838547
time_elpased: 1.834
batch start
#iterations: 325
currently lose_sum: 587.8063563108444
time_elpased: 1.836
batch start
#iterations: 326
currently lose_sum: 589.4524328112602
time_elpased: 1.806
batch start
#iterations: 327
currently lose_sum: 589.5277591943741
time_elpased: 1.816
batch start
#iterations: 328
currently lose_sum: 588.1273842453957
time_elpased: 1.826
batch start
#iterations: 329
currently lose_sum: 588.2986560463905
time_elpased: 1.825
batch start
#iterations: 330
currently lose_sum: 590.016680419445
time_elpased: 1.957
batch start
#iterations: 331
currently lose_sum: 587.135920882225
time_elpased: 1.93
batch start
#iterations: 332
currently lose_sum: 588.3128107190132
time_elpased: 1.947
batch start
#iterations: 333
currently lose_sum: 588.5058117508888
time_elpased: 1.962
batch start
#iterations: 334
currently lose_sum: 588.7640396356583
time_elpased: 2.001
batch start
#iterations: 335
currently lose_sum: 589.1824803948402
time_elpased: 1.797
batch start
#iterations: 336
currently lose_sum: 587.4967824220657
time_elpased: 1.808
batch start
#iterations: 337
currently lose_sum: 589.7650821208954
time_elpased: 1.815
batch start
#iterations: 338
currently lose_sum: 589.8900144696236
time_elpased: 1.855
batch start
#iterations: 339
currently lose_sum: 587.2187569141388
time_elpased: 1.809
start validation test
0.583268698061
0.570719443012
0.911177439275
0.701839226257
0
validation finish
batch start
#iterations: 340
currently lose_sum: 588.9612783193588
time_elpased: 1.877
batch start
#iterations: 341
currently lose_sum: 589.5698825120926
time_elpased: 1.846
batch start
#iterations: 342
currently lose_sum: 590.3258506059647
time_elpased: 1.824
batch start
#iterations: 343
currently lose_sum: 589.8740727305412
time_elpased: 1.814
batch start
#iterations: 344
currently lose_sum: 588.654606282711
time_elpased: 1.824
batch start
#iterations: 345
currently lose_sum: 589.4281358122826
time_elpased: 1.843
batch start
#iterations: 346
currently lose_sum: 589.0411603450775
time_elpased: 1.833
batch start
#iterations: 347
currently lose_sum: 587.4588617682457
time_elpased: 1.837
batch start
#iterations: 348
currently lose_sum: 588.3141763210297
time_elpased: 1.849
batch start
#iterations: 349
currently lose_sum: 588.8738174438477
time_elpased: 1.836
batch start
#iterations: 350
currently lose_sum: 588.6367000937462
time_elpased: 1.83
batch start
#iterations: 351
currently lose_sum: 588.8957723379135
time_elpased: 1.864
batch start
#iterations: 352
currently lose_sum: 589.2017765045166
time_elpased: 1.82
batch start
#iterations: 353
currently lose_sum: 589.1314945220947
time_elpased: 1.825
batch start
#iterations: 354
currently lose_sum: 587.4455058574677
time_elpased: 1.817
batch start
#iterations: 355
currently lose_sum: 588.6537878513336
time_elpased: 1.818
batch start
#iterations: 356
currently lose_sum: 589.0218958258629
time_elpased: 1.844
batch start
#iterations: 357
currently lose_sum: 588.3463572859764
time_elpased: 1.806
batch start
#iterations: 358
currently lose_sum: 587.1602318882942
time_elpased: 1.817
batch start
#iterations: 359
currently lose_sum: 589.5152049064636
time_elpased: 1.817
start validation test
0.582132963989
0.568866639207
0.92393989296
0.704175082855
0
validation finish
batch start
#iterations: 360
currently lose_sum: 587.6318125128746
time_elpased: 1.866
batch start
#iterations: 361
currently lose_sum: 588.0725798010826
time_elpased: 1.839
batch start
#iterations: 362
currently lose_sum: 588.5229415893555
time_elpased: 1.818
batch start
#iterations: 363
currently lose_sum: 589.4001722335815
time_elpased: 1.815
batch start
#iterations: 364
currently lose_sum: 588.583932518959
time_elpased: 1.794
batch start
#iterations: 365
currently lose_sum: 590.148216187954
time_elpased: 1.816
batch start
#iterations: 366
currently lose_sum: 587.6735452413559
time_elpased: 1.827
batch start
#iterations: 367
currently lose_sum: 586.4760296344757
time_elpased: 1.832
batch start
#iterations: 368
currently lose_sum: 589.4555167555809
time_elpased: 1.833
batch start
#iterations: 369
currently lose_sum: 590.3930712342262
time_elpased: 1.823
batch start
#iterations: 370
currently lose_sum: 589.4142503142357
time_elpased: 1.801
batch start
#iterations: 371
currently lose_sum: 588.4647107124329
time_elpased: 1.86
batch start
#iterations: 372
currently lose_sum: 590.0926474928856
time_elpased: 1.843
batch start
#iterations: 373
currently lose_sum: 587.7515884637833
time_elpased: 1.823
batch start
#iterations: 374
currently lose_sum: 589.2074338793755
time_elpased: 1.862
batch start
#iterations: 375
currently lose_sum: 588.7342241406441
time_elpased: 1.848
batch start
#iterations: 376
currently lose_sum: 587.7347608804703
time_elpased: 1.853
batch start
#iterations: 377
currently lose_sum: 588.9958310723305
time_elpased: 1.847
batch start
#iterations: 378
currently lose_sum: 587.788507938385
time_elpased: 1.853
batch start
#iterations: 379
currently lose_sum: 588.430048763752
time_elpased: 1.842
start validation test
0.583379501385
0.570601851852
0.913338822561
0.702390375178
0
validation finish
batch start
#iterations: 380
currently lose_sum: 588.9595859646797
time_elpased: 1.828
batch start
#iterations: 381
currently lose_sum: 588.2157008647919
time_elpased: 1.796
batch start
#iterations: 382
currently lose_sum: 587.2336276173592
time_elpased: 1.84
batch start
#iterations: 383
currently lose_sum: 589.4504978656769
time_elpased: 1.815
batch start
#iterations: 384
currently lose_sum: 589.2350989580154
time_elpased: 1.827
batch start
#iterations: 385
currently lose_sum: 586.0622484087944
time_elpased: 1.836
batch start
#iterations: 386
currently lose_sum: 588.9758543372154
time_elpased: 1.826
batch start
#iterations: 387
currently lose_sum: 588.6238743662834
time_elpased: 1.816
batch start
#iterations: 388
currently lose_sum: 588.2738462090492
time_elpased: 1.874
batch start
#iterations: 389
currently lose_sum: 586.6005257368088
time_elpased: 1.802
batch start
#iterations: 390
currently lose_sum: 588.3291271924973
time_elpased: 1.784
batch start
#iterations: 391
currently lose_sum: 589.796915948391
time_elpased: 1.81
batch start
#iterations: 392
currently lose_sum: 587.6633062958717
time_elpased: 1.835
batch start
#iterations: 393
currently lose_sum: 587.0550260543823
time_elpased: 1.803
batch start
#iterations: 394
currently lose_sum: 588.5760935544968
time_elpased: 1.824
batch start
#iterations: 395
currently lose_sum: 587.0000513792038
time_elpased: 1.817
batch start
#iterations: 396
currently lose_sum: 587.9474402666092
time_elpased: 1.828
batch start
#iterations: 397
currently lose_sum: 587.8878543972969
time_elpased: 1.847
batch start
#iterations: 398
currently lose_sum: 586.3705567717552
time_elpased: 1.816
batch start
#iterations: 399
currently lose_sum: 587.8119325041771
time_elpased: 1.847
start validation test
0.581385041551
0.568044354839
0.92795389049
0.704705330624
0
validation finish
acc: 0.576
pre: 0.561
rec: 0.959
F1: 0.708
auc: 0.000
