start to construct graph
graph construct over
93177
epochs start
batch start
#iterations: 0
currently lose_sum: 617.1700859069824
time_elpased: 5.962
batch start
#iterations: 1
currently lose_sum: 615.0593344569206
time_elpased: 6.173
batch start
#iterations: 2
currently lose_sum: 615.6677639484406
time_elpased: 6.074
batch start
#iterations: 3
currently lose_sum: 614.7779816389084
time_elpased: 5.963
batch start
#iterations: 4
currently lose_sum: 614.3047807216644
time_elpased: 6.051
batch start
#iterations: 5
currently lose_sum: 613.4054139256477
time_elpased: 5.902
batch start
#iterations: 6
currently lose_sum: 615.1077639460564
time_elpased: 6.029
batch start
#iterations: 7
currently lose_sum: 612.636666238308
time_elpased: 6.037
batch start
#iterations: 8
currently lose_sum: 613.3102775812149
time_elpased: 6.218
batch start
#iterations: 9
currently lose_sum: 611.4399456977844
time_elpased: 5.921
batch start
#iterations: 10
currently lose_sum: 611.9189964532852
time_elpased: 5.799
batch start
#iterations: 11
currently lose_sum: 611.3884149193764
time_elpased: 6.15
batch start
#iterations: 12
currently lose_sum: 610.7150281071663
time_elpased: 5.824
batch start
#iterations: 13
currently lose_sum: 607.960155248642
time_elpased: 6.236
batch start
#iterations: 14
currently lose_sum: 606.7372626662254
time_elpased: 5.821
batch start
#iterations: 15
currently lose_sum: 606.2905017137527
time_elpased: 6.123
batch start
#iterations: 16
currently lose_sum: 607.0063839554787
time_elpased: 6.232
batch start
#iterations: 17
currently lose_sum: 605.2895140051842
time_elpased: 6.074
batch start
#iterations: 18
currently lose_sum: 602.9559600353241
time_elpased: 5.948
batch start
#iterations: 19
currently lose_sum: 601.2204575538635
time_elpased: 5.972
start validation test
0.546077348066
0.544222497201
0.950499125244
0.692146282974
0
validation finish
batch start
#iterations: 20
currently lose_sum: 600.239627957344
time_elpased: 6.266
batch start
#iterations: 21
currently lose_sum: 601.7059318423271
time_elpased: 6.048
batch start
#iterations: 22
currently lose_sum: 600.9285169243813
time_elpased: 6.075
batch start
#iterations: 23
currently lose_sum: 598.0991933345795
time_elpased: 7.128
batch start
#iterations: 24
currently lose_sum: 598.5638927817345
time_elpased: 6.06
batch start
#iterations: 25
currently lose_sum: 596.0917637348175
time_elpased: 6.143
batch start
#iterations: 26
currently lose_sum: 595.5751929879189
time_elpased: 6.007
batch start
#iterations: 27
currently lose_sum: 596.331969678402
time_elpased: 5.881
batch start
#iterations: 28
currently lose_sum: 595.5335285663605
time_elpased: 6.02
batch start
#iterations: 29
currently lose_sum: 594.8858751058578
time_elpased: 6.068
batch start
#iterations: 30
currently lose_sum: 594.5264680981636
time_elpased: 6.177
batch start
#iterations: 31
currently lose_sum: 596.2672762274742
time_elpased: 5.832
batch start
#iterations: 32
currently lose_sum: 594.2809339165688
time_elpased: 6.181
batch start
#iterations: 33
currently lose_sum: 593.07399725914
time_elpased: 6.293
batch start
#iterations: 34
currently lose_sum: 591.2018278241158
time_elpased: 6.161
batch start
#iterations: 35
currently lose_sum: 593.2341328859329
time_elpased: 5.977
batch start
#iterations: 36
currently lose_sum: 591.4014256596565
time_elpased: 6.046
batch start
#iterations: 37
currently lose_sum: 591.7696475982666
time_elpased: 5.988
batch start
#iterations: 38
currently lose_sum: 592.2893687486649
time_elpased: 5.97
batch start
#iterations: 39
currently lose_sum: 590.1912533044815
time_elpased: 5.763
start validation test
0.567569060773
0.566568047337
0.82772460636
0.672688495797
0
validation finish
batch start
#iterations: 40
currently lose_sum: 591.3506492972374
time_elpased: 5.983
batch start
#iterations: 41
currently lose_sum: 591.8798014521599
time_elpased: 5.977
batch start
#iterations: 42
currently lose_sum: 588.1993761062622
time_elpased: 6.064
batch start
#iterations: 43
currently lose_sum: 590.8498201966286
time_elpased: 5.954
batch start
#iterations: 44
currently lose_sum: 590.5579615831375
time_elpased: 6.166
batch start
#iterations: 45
currently lose_sum: 588.5838849544525
time_elpased: 6.14
batch start
#iterations: 46
currently lose_sum: 589.2247751951218
time_elpased: 6.116
batch start
#iterations: 47
currently lose_sum: 589.5445109009743
time_elpased: 6.188
batch start
#iterations: 48
currently lose_sum: 588.3371719717979
time_elpased: 6.053
batch start
#iterations: 49
currently lose_sum: 588.0793587565422
time_elpased: 6.215
batch start
#iterations: 50
currently lose_sum: 589.5838697552681
time_elpased: 6.262
batch start
#iterations: 51
currently lose_sum: 587.0408264398575
time_elpased: 6.093
batch start
#iterations: 52
currently lose_sum: 588.9065620303154
time_elpased: 5.91
batch start
#iterations: 53
currently lose_sum: 588.285030901432
time_elpased: 5.975
batch start
#iterations: 54
currently lose_sum: 587.6372353434563
time_elpased: 6.261
batch start
#iterations: 55
currently lose_sum: 586.8502574563026
time_elpased: 6.062
batch start
#iterations: 56
currently lose_sum: 586.125771343708
time_elpased: 6.106
batch start
#iterations: 57
currently lose_sum: 586.3137733340263
time_elpased: 6.174
batch start
#iterations: 58
currently lose_sum: 585.6848643422127
time_elpased: 6.038
batch start
#iterations: 59
currently lose_sum: 586.540896832943
time_elpased: 6.137
start validation test
0.565138121547
0.560397853684
0.881341977977
0.685147405896
0
validation finish
batch start
#iterations: 60
currently lose_sum: 586.4458562731743
time_elpased: 6.15
batch start
#iterations: 61
currently lose_sum: 585.8690029382706
time_elpased: 6.037
batch start
#iterations: 62
currently lose_sum: 586.569830596447
time_elpased: 5.696
batch start
#iterations: 63
currently lose_sum: 585.3906168341637
time_elpased: 6.182
batch start
#iterations: 64
currently lose_sum: 585.7453491091728
time_elpased: 6.209
batch start
#iterations: 65
currently lose_sum: 583.7370803952217
time_elpased: 6.184
batch start
#iterations: 66
currently lose_sum: 585.5088174939156
time_elpased: 6.063
batch start
#iterations: 67
currently lose_sum: 584.9012605547905
time_elpased: 6.017
batch start
#iterations: 68
currently lose_sum: 583.9324719905853
time_elpased: 6.005
batch start
#iterations: 69
currently lose_sum: 583.8700205981731
time_elpased: 6.122
batch start
#iterations: 70
currently lose_sum: 584.9595647454262
time_elpased: 6.013
batch start
#iterations: 71
currently lose_sum: 582.9488455057144
time_elpased: 6.051
batch start
#iterations: 72
currently lose_sum: 583.8859772086143
time_elpased: 6.052
batch start
#iterations: 73
currently lose_sum: 583.6729389429092
time_elpased: 6.126
batch start
#iterations: 74
currently lose_sum: 583.7834340333939
time_elpased: 6.037
batch start
#iterations: 75
currently lose_sum: 582.6113591194153
time_elpased: 6.112
batch start
#iterations: 76
currently lose_sum: 582.6886227726936
time_elpased: 6.489
batch start
#iterations: 77
currently lose_sum: 583.6754686534405
time_elpased: 6.086
batch start
#iterations: 78
currently lose_sum: 583.3161973953247
time_elpased: 6.006
batch start
#iterations: 79
currently lose_sum: 581.8761641979218
time_elpased: 5.973
start validation test
0.574198895028
0.568977350721
0.853143974478
0.682669741014
0
validation finish
batch start
#iterations: 80
currently lose_sum: 581.6918664872646
time_elpased: 6.175
batch start
#iterations: 81
currently lose_sum: 580.6751442849636
time_elpased: 5.849
batch start
#iterations: 82
currently lose_sum: 581.3554466962814
time_elpased: 6.05
batch start
#iterations: 83
currently lose_sum: 582.2329609394073
time_elpased: 5.99
batch start
#iterations: 84
currently lose_sum: 581.1867609024048
time_elpased: 6.061
batch start
#iterations: 85
currently lose_sum: 583.2710758447647
time_elpased: 5.921
batch start
#iterations: 86
currently lose_sum: 582.5677974820137
time_elpased: 6.17
batch start
#iterations: 87
currently lose_sum: 580.6253775954247
time_elpased: 6.066
batch start
#iterations: 88
currently lose_sum: 581.4326902031898
time_elpased: 6.035
batch start
#iterations: 89
currently lose_sum: 582.1515043377876
time_elpased: 5.787
batch start
#iterations: 90
currently lose_sum: 582.3522539138794
time_elpased: 6.014
batch start
#iterations: 91
currently lose_sum: 581.4910550415516
time_elpased: 6.144
batch start
#iterations: 92
currently lose_sum: 579.3850205540657
time_elpased: 5.929
batch start
#iterations: 93
currently lose_sum: 580.7833520770073
time_elpased: 5.887
batch start
#iterations: 94
currently lose_sum: 580.3968552350998
time_elpased: 6.153
batch start
#iterations: 95
currently lose_sum: 579.2260456085205
time_elpased: 6.028
batch start
#iterations: 96
currently lose_sum: 579.5577381253242
time_elpased: 6.085
batch start
#iterations: 97
currently lose_sum: 578.8172996640205
time_elpased: 6.051
batch start
#iterations: 98
currently lose_sum: 579.1210629940033
time_elpased: 6.08
batch start
#iterations: 99
currently lose_sum: 581.1923124194145
time_elpased: 6.141
start validation test
0.570386740331
0.564105951516
0.87887207986
0.687158030254
0
validation finish
batch start
#iterations: 100
currently lose_sum: 578.5331490635872
time_elpased: 6.025
batch start
#iterations: 101
currently lose_sum: 579.9749345779419
time_elpased: 5.643
batch start
#iterations: 102
currently lose_sum: 577.3144021034241
time_elpased: 6.26
batch start
#iterations: 103
currently lose_sum: 577.5351376533508
time_elpased: 6.065
batch start
#iterations: 104
currently lose_sum: 579.7414019107819
time_elpased: 6.157
batch start
#iterations: 105
currently lose_sum: 578.8749136328697
time_elpased: 6.037
batch start
#iterations: 106
currently lose_sum: 577.0133211612701
time_elpased: 6.01
batch start
#iterations: 107
currently lose_sum: 580.161469399929
time_elpased: 6.131
batch start
#iterations: 108
currently lose_sum: 578.253100335598
time_elpased: 6.141
batch start
#iterations: 109
currently lose_sum: 577.2131023406982
time_elpased: 5.904
batch start
#iterations: 110
currently lose_sum: 579.68631118536
time_elpased: 5.826
batch start
#iterations: 111
currently lose_sum: 578.0507103800774
time_elpased: 6.1
batch start
#iterations: 112
currently lose_sum: 577.0435108244419
time_elpased: 6.04
batch start
#iterations: 113
currently lose_sum: 576.8446785211563
time_elpased: 5.974
batch start
#iterations: 114
currently lose_sum: 578.6741746068001
time_elpased: 6.033
batch start
#iterations: 115
currently lose_sum: 577.4544949531555
time_elpased: 6.204
batch start
#iterations: 116
currently lose_sum: 577.6892259120941
time_elpased: 6.121
batch start
#iterations: 117
currently lose_sum: 579.8354172706604
time_elpased: 6.281
batch start
#iterations: 118
currently lose_sum: 577.5516679286957
time_elpased: 6.187
batch start
#iterations: 119
currently lose_sum: 577.9436461627483
time_elpased: 6.15
start validation test
0.573591160221
0.566126364539
0.880621591026
0.689191365979
0
validation finish
batch start
#iterations: 120
currently lose_sum: 577.404613494873
time_elpased: 5.892
batch start
#iterations: 121
currently lose_sum: 575.8069628477097
time_elpased: 5.951
batch start
#iterations: 122
currently lose_sum: 576.584258377552
time_elpased: 5.991
batch start
#iterations: 123
currently lose_sum: 577.6627072989941
time_elpased: 5.939
batch start
#iterations: 124
currently lose_sum: 576.9486475586891
time_elpased: 5.909
batch start
#iterations: 125
currently lose_sum: 576.3615274429321
time_elpased: 6.094
batch start
#iterations: 126
currently lose_sum: 578.1320551633835
time_elpased: 6.011
batch start
#iterations: 127
currently lose_sum: 577.002138376236
time_elpased: 5.936
batch start
#iterations: 128
currently lose_sum: 576.6025334596634
time_elpased: 6.206
batch start
#iterations: 129
currently lose_sum: 576.4633795619011
time_elpased: 6.08
batch start
#iterations: 130
currently lose_sum: 575.5697664618492
time_elpased: 5.905
batch start
#iterations: 131
currently lose_sum: 577.1194459199905
time_elpased: 5.918
batch start
#iterations: 132
currently lose_sum: 576.9483264684677
time_elpased: 6.027
batch start
#iterations: 133
currently lose_sum: 574.2638860046864
time_elpased: 6.04
batch start
#iterations: 134
currently lose_sum: 574.6777386665344
time_elpased: 6.101
batch start
#iterations: 135
currently lose_sum: 575.683695346117
time_elpased: 5.897
batch start
#iterations: 136
currently lose_sum: 577.5358855724335
time_elpased: 6.182
batch start
#iterations: 137
currently lose_sum: 576.845573246479
time_elpased: 6.007
batch start
#iterations: 138
currently lose_sum: 577.1843236088753
time_elpased: 5.753
batch start
#iterations: 139
currently lose_sum: 577.3520475625992
time_elpased: 6.239
start validation test
0.575801104972
0.568244193052
0.873623546362
0.688595068138
0
validation finish
batch start
#iterations: 140
currently lose_sum: 576.9725566506386
time_elpased: 6.034
batch start
#iterations: 141
currently lose_sum: 574.2864070236683
time_elpased: 6.135
batch start
#iterations: 142
currently lose_sum: 576.7020410299301
time_elpased: 5.883
batch start
#iterations: 143
currently lose_sum: 577.3415321111679
time_elpased: 6.077
batch start
#iterations: 144
currently lose_sum: 576.4115549325943
time_elpased: 6.218
batch start
#iterations: 145
currently lose_sum: 575.7613349556923
time_elpased: 6.036
batch start
#iterations: 146
currently lose_sum: 576.4632532000542
time_elpased: 6.168
batch start
#iterations: 147
currently lose_sum: 575.3486614823341
time_elpased: 5.962
batch start
#iterations: 148
currently lose_sum: 577.087818801403
time_elpased: 6.056
batch start
#iterations: 149
currently lose_sum: 576.3184997439384
time_elpased: 6.129
batch start
#iterations: 150
currently lose_sum: 575.5951673686504
time_elpased: 5.996
batch start
#iterations: 151
currently lose_sum: 577.2499234676361
time_elpased: 6.139
batch start
#iterations: 152
currently lose_sum: 575.7583052515984
time_elpased: 5.711
batch start
#iterations: 153
currently lose_sum: 574.136879503727
time_elpased: 6.104
batch start
#iterations: 154
currently lose_sum: 574.9464947879314
time_elpased: 6.125
batch start
#iterations: 155
currently lose_sum: 575.1832612752914
time_elpased: 6.22
batch start
#iterations: 156
currently lose_sum: 576.585246026516
time_elpased: 6.116
batch start
#iterations: 157
currently lose_sum: 575.5844675004482
time_elpased: 5.898
batch start
#iterations: 158
currently lose_sum: 575.8622708916664
time_elpased: 6.208
batch start
#iterations: 159
currently lose_sum: 575.4088832139969
time_elpased: 6.291
start validation test
0.573508287293
0.566911198205
0.870844910981
0.686753099195
0
validation finish
batch start
#iterations: 160
currently lose_sum: 577.5563611984253
time_elpased: 6.103
batch start
#iterations: 161
currently lose_sum: 574.3055753409863
time_elpased: 5.802
batch start
#iterations: 162
currently lose_sum: 575.4825964570045
time_elpased: 6.144
batch start
#iterations: 163
currently lose_sum: 575.8031513094902
time_elpased: 6.251
batch start
#iterations: 164
currently lose_sum: 575.0525653362274
time_elpased: 6.102
batch start
#iterations: 165
currently lose_sum: 573.1655402779579
time_elpased: 5.938
batch start
#iterations: 166
currently lose_sum: 578.204262137413
time_elpased: 5.86
batch start
#iterations: 167
currently lose_sum: 575.5272829532623
time_elpased: 6.228
batch start
#iterations: 168
currently lose_sum: 576.502791762352
time_elpased: 6.203
batch start
#iterations: 169
currently lose_sum: 572.4859993755817
time_elpased: 6.116
batch start
#iterations: 170
currently lose_sum: 575.2518176436424
time_elpased: 5.98
batch start
#iterations: 171
currently lose_sum: 576.4744943380356
time_elpased: 6.001
batch start
#iterations: 172
currently lose_sum: 571.740520119667
time_elpased: 6.024
batch start
#iterations: 173
currently lose_sum: 573.7825328409672
time_elpased: 6.135
batch start
#iterations: 174
currently lose_sum: 575.5740450620651
time_elpased: 6.251
batch start
#iterations: 175
currently lose_sum: 574.5413816273212
time_elpased: 6.037
batch start
#iterations: 176
currently lose_sum: 577.1471408009529
time_elpased: 6.362
batch start
#iterations: 177
currently lose_sum: 573.3469698429108
time_elpased: 5.887
batch start
#iterations: 178
currently lose_sum: 573.4809964895248
time_elpased: 5.992
batch start
#iterations: 179
currently lose_sum: 574.390075802803
time_elpased: 6.008
start validation test
0.576049723757
0.570609170381
0.849747864567
0.682749354005
0
validation finish
batch start
#iterations: 180
currently lose_sum: 574.2218076586723
time_elpased: 5.854
batch start
#iterations: 181
currently lose_sum: 574.7546433210373
time_elpased: 6.14
batch start
#iterations: 182
currently lose_sum: 574.89010232687
time_elpased: 6.034
batch start
#iterations: 183
currently lose_sum: 575.8154934048653
time_elpased: 5.772
batch start
#iterations: 184
currently lose_sum: 573.7928349673748
time_elpased: 6.032
batch start
#iterations: 185
currently lose_sum: 574.0191056728363
time_elpased: 6.192
batch start
#iterations: 186
currently lose_sum: 573.0980703830719
time_elpased: 6.144
batch start
#iterations: 187
currently lose_sum: 573.5817306637764
time_elpased: 6.07
batch start
#iterations: 188
currently lose_sum: 574.2172238230705
time_elpased: 6.081
batch start
#iterations: 189
currently lose_sum: 574.177194416523
time_elpased: 5.95
batch start
#iterations: 190
currently lose_sum: 571.5170297622681
time_elpased: 6.127
batch start
#iterations: 191
currently lose_sum: 573.6877336502075
time_elpased: 6.083
batch start
#iterations: 192
currently lose_sum: 572.6068985760212
time_elpased: 6.191
batch start
#iterations: 193
currently lose_sum: 573.4707905054092
time_elpased: 6.181
batch start
#iterations: 194
currently lose_sum: 571.8305116891861
time_elpased: 6.101
batch start
#iterations: 195
currently lose_sum: 573.0309645235538
time_elpased: 6.287
batch start
#iterations: 196
currently lose_sum: 575.0329837501049
time_elpased: 6.152
batch start
#iterations: 197
currently lose_sum: 572.8283947706223
time_elpased: 6.125
batch start
#iterations: 198
currently lose_sum: 574.1100625991821
time_elpased: 6.192
batch start
#iterations: 199
currently lose_sum: 572.9787979125977
time_elpased: 5.949
start validation test
0.577127071823
0.571241107811
0.851188638469
0.683666721772
0
validation finish
batch start
#iterations: 200
currently lose_sum: 570.2296578288078
time_elpased: 6.19
batch start
#iterations: 201
currently lose_sum: 572.2991982996464
time_elpased: 6.12
batch start
#iterations: 202
currently lose_sum: 571.7372510135174
time_elpased: 6.144
batch start
#iterations: 203
currently lose_sum: 573.3308631777763
time_elpased: 5.921
batch start
#iterations: 204
currently lose_sum: 573.1416235864162
time_elpased: 5.896
batch start
#iterations: 205
currently lose_sum: 572.9684465825558
time_elpased: 6.061
batch start
#iterations: 206
currently lose_sum: 571.8703798055649
time_elpased: 6.12
batch start
#iterations: 207
currently lose_sum: 571.2641372680664
time_elpased: 6.041
batch start
#iterations: 208
currently lose_sum: 571.2000523209572
time_elpased: 5.983
batch start
#iterations: 209
currently lose_sum: 573.0928082168102
time_elpased: 6.346
batch start
#iterations: 210
currently lose_sum: 573.6256555914879
time_elpased: 6.088
batch start
#iterations: 211
currently lose_sum: 571.6177690029144
time_elpased: 6.033
batch start
#iterations: 212
currently lose_sum: 573.1825562119484
time_elpased: 6.163
batch start
#iterations: 213
currently lose_sum: 571.011209666729
time_elpased: 6.105
batch start
#iterations: 214
currently lose_sum: 572.3507339954376
time_elpased: 5.958
batch start
#iterations: 215
currently lose_sum: 573.2638280689716
time_elpased: 6.14
batch start
#iterations: 216
currently lose_sum: 572.3590037822723
time_elpased: 6.052
batch start
#iterations: 217
currently lose_sum: 571.600092113018
time_elpased: 6.185
batch start
#iterations: 218
currently lose_sum: 571.1106661856174
time_elpased: 6.096
batch start
#iterations: 219
currently lose_sum: 573.9936715364456
time_elpased: 6.38
start validation test
0.574723756906
0.567055151576
0.878769167439
0.689310003834
0
validation finish
batch start
#iterations: 220
currently lose_sum: 573.037199318409
time_elpased: 5.79
batch start
#iterations: 221
currently lose_sum: 570.6634220182896
time_elpased: 6.277
batch start
#iterations: 222
currently lose_sum: 571.1839764118195
time_elpased: 6.023
batch start
#iterations: 223
currently lose_sum: 572.5624541044235
time_elpased: 5.952
batch start
#iterations: 224
currently lose_sum: 571.1104619204998
time_elpased: 5.823
batch start
#iterations: 225
currently lose_sum: 571.8880085349083
time_elpased: 6.047
batch start
#iterations: 226
currently lose_sum: 573.9358291625977
time_elpased: 5.782
batch start
#iterations: 227
currently lose_sum: 572.1467444300652
time_elpased: 5.896
batch start
#iterations: 228
currently lose_sum: 569.6667221486568
time_elpased: 6.014
batch start
#iterations: 229
currently lose_sum: 572.1570371985435
time_elpased: 6.048
batch start
#iterations: 230
currently lose_sum: 570.443659722805
time_elpased: 5.833
batch start
#iterations: 231
currently lose_sum: 571.8475423455238
time_elpased: 6.149
batch start
#iterations: 232
currently lose_sum: 571.9316265583038
time_elpased: 6.193
batch start
#iterations: 233
currently lose_sum: 571.9593771100044
time_elpased: 6.022
batch start
#iterations: 234
currently lose_sum: 570.8594996333122
time_elpased: 6.057
batch start
#iterations: 235
currently lose_sum: 571.8613972067833
time_elpased: 6.097
batch start
#iterations: 236
currently lose_sum: 570.3565881252289
time_elpased: 5.991
batch start
#iterations: 237
currently lose_sum: 572.952220261097
time_elpased: 6.191
batch start
#iterations: 238
currently lose_sum: 571.0135043561459
time_elpased: 5.757
batch start
#iterations: 239
currently lose_sum: 573.4345162808895
time_elpased: 6.205
start validation test
0.575303867403
0.56891634981
0.862303179994
0.685538965024
0
validation finish
batch start
#iterations: 240
currently lose_sum: 571.5580121576786
time_elpased: 6.024
batch start
#iterations: 241
currently lose_sum: 572.7301129102707
time_elpased: 6.237
batch start
#iterations: 242
currently lose_sum: 571.4503011405468
time_elpased: 5.984
batch start
#iterations: 243
currently lose_sum: 571.598512172699
time_elpased: 6.05
batch start
#iterations: 244
currently lose_sum: 570.6798556447029
time_elpased: 6.121
batch start
#iterations: 245
currently lose_sum: 572.464829146862
time_elpased: 5.992
batch start
#iterations: 246
currently lose_sum: 570.5758534669876
time_elpased: 6.143
batch start
#iterations: 247
currently lose_sum: 571.760678678751
time_elpased: 6.079
batch start
#iterations: 248
currently lose_sum: 572.0779414772987
time_elpased: 6.13
batch start
#iterations: 249
currently lose_sum: 569.9470019340515
time_elpased: 6.023
batch start
#iterations: 250
currently lose_sum: 571.2189154624939
time_elpased: 6.11
batch start
#iterations: 251
currently lose_sum: 569.4793169498444
time_elpased: 5.801
batch start
#iterations: 252
currently lose_sum: 571.5722097456455
time_elpased: 6.153
batch start
#iterations: 253
currently lose_sum: 572.0564436912537
time_elpased: 6.056
batch start
#iterations: 254
currently lose_sum: 571.3427675068378
time_elpased: 6.263
batch start
#iterations: 255
currently lose_sum: 570.6473379135132
time_elpased: 6.01
batch start
#iterations: 256
currently lose_sum: 573.1273931264877
time_elpased: 5.833
batch start
#iterations: 257
currently lose_sum: 573.0617763996124
time_elpased: 5.974
batch start
#iterations: 258
currently lose_sum: 570.3655836880207
time_elpased: 6.185
batch start
#iterations: 259
currently lose_sum: 569.8771926164627
time_elpased: 6.017
start validation test
0.575220994475
0.568292765041
0.868580837707
0.687058631988
0
validation finish
batch start
#iterations: 260
currently lose_sum: 571.8680518865585
time_elpased: 5.894
batch start
#iterations: 261
currently lose_sum: 571.1702542304993
time_elpased: 6.202
batch start
#iterations: 262
currently lose_sum: 573.3749491870403
time_elpased: 6.102
batch start
#iterations: 263
currently lose_sum: 568.6152309775352
time_elpased: 6.041
batch start
#iterations: 264
currently lose_sum: 571.3783727288246
time_elpased: 6.197
batch start
#iterations: 265
currently lose_sum: 573.173155605793
time_elpased: 5.961
batch start
#iterations: 266
currently lose_sum: 569.7479463815689
time_elpased: 5.982
batch start
#iterations: 267
currently lose_sum: 572.4535056352615
time_elpased: 5.967
batch start
#iterations: 268
currently lose_sum: 571.8234810233116
time_elpased: 6.197
batch start
#iterations: 269
currently lose_sum: 571.2399463653564
time_elpased: 6.156
batch start
#iterations: 270
currently lose_sum: 570.6800433695316
time_elpased: 6.075
batch start
#iterations: 271
currently lose_sum: 569.6450462937355
time_elpased: 6.128
batch start
#iterations: 272
currently lose_sum: 569.2149049043655
time_elpased: 5.673
batch start
#iterations: 273
currently lose_sum: 571.8939712047577
time_elpased: 5.983
batch start
#iterations: 274
currently lose_sum: 569.6888208985329
time_elpased: 6.312
batch start
#iterations: 275
currently lose_sum: 573.1100549399853
time_elpased: 6.036
batch start
#iterations: 276
currently lose_sum: 568.6655835807323
time_elpased: 6.24
batch start
#iterations: 277
currently lose_sum: 569.0776335895061
time_elpased: 6.144
batch start
#iterations: 278
currently lose_sum: 570.7289224565029
time_elpased: 5.913
batch start
#iterations: 279
currently lose_sum: 571.8040715456009
time_elpased: 6.117
start validation test
0.575524861878
0.569306255963
0.859730369456
0.68500676479
0
validation finish
batch start
#iterations: 280
currently lose_sum: 570.9205773472786
time_elpased: 6.07
batch start
#iterations: 281
currently lose_sum: 569.6750776171684
time_elpased: 6.168
batch start
#iterations: 282
currently lose_sum: 570.7911592721939
time_elpased: 5.96
batch start
#iterations: 283
currently lose_sum: 568.9350937306881
time_elpased: 6.086
batch start
#iterations: 284
currently lose_sum: 571.5283470749855
time_elpased: 6.01
batch start
#iterations: 285
currently lose_sum: 569.6616230607033
time_elpased: 6.115
batch start
#iterations: 286
currently lose_sum: 571.5480902493
time_elpased: 6.204
batch start
#iterations: 287
currently lose_sum: 572.0453650951385
time_elpased: 5.971
batch start
#iterations: 288
currently lose_sum: 570.2187043428421
time_elpased: 6.04
batch start
#iterations: 289
currently lose_sum: 570.5232566297054
time_elpased: 6.061
batch start
#iterations: 290
currently lose_sum: 570.8985970020294
time_elpased: 6.104
batch start
#iterations: 291
currently lose_sum: 569.8995922207832
time_elpased: 6.157
batch start
#iterations: 292
currently lose_sum: 569.1955444216728
time_elpased: 6.047
batch start
#iterations: 293
currently lose_sum: 567.9998578727245
time_elpased: 5.92
batch start
#iterations: 294
currently lose_sum: 570.6881291270256
time_elpased: 6.123
batch start
#iterations: 295
currently lose_sum: 571.6757547259331
time_elpased: 6.247
batch start
#iterations: 296
currently lose_sum: 570.8720083832741
time_elpased: 5.958
batch start
#iterations: 297
currently lose_sum: 571.7563035786152
time_elpased: 6.034
batch start
#iterations: 298
currently lose_sum: 572.1264939904213
time_elpased: 6.232
batch start
#iterations: 299
currently lose_sum: 568.5992359519005
time_elpased: 6.038
start validation test
0.574723756906
0.569895822518
0.847277966451
0.681441016409
0
validation finish
batch start
#iterations: 300
currently lose_sum: 569.6262246072292
time_elpased: 5.875
batch start
#iterations: 301
currently lose_sum: 567.3517854809761
time_elpased: 6.038
batch start
#iterations: 302
currently lose_sum: 570.3813307285309
time_elpased: 6.199
batch start
#iterations: 303
currently lose_sum: 570.5493099093437
time_elpased: 6.176
batch start
#iterations: 304
currently lose_sum: 568.6080961227417
time_elpased: 6.064
batch start
#iterations: 305
currently lose_sum: 570.5558571219444
time_elpased: 5.751
batch start
#iterations: 306
currently lose_sum: 569.1352714002132
time_elpased: 6.014
batch start
#iterations: 307
currently lose_sum: 568.5991461277008
time_elpased: 6.076
batch start
#iterations: 308
currently lose_sum: 570.3519233167171
time_elpased: 6.275
batch start
#iterations: 309
currently lose_sum: 570.7335087060928
time_elpased: 6.19
batch start
#iterations: 310
currently lose_sum: 569.1305070221424
time_elpased: 5.956
batch start
#iterations: 311
currently lose_sum: 570.6139869689941
time_elpased: 5.971
batch start
#iterations: 312
currently lose_sum: 568.7262786626816
time_elpased: 6.104
batch start
#iterations: 313
currently lose_sum: 570.2785543203354
time_elpased: 6.197
batch start
#iterations: 314
currently lose_sum: 570.5792290568352
time_elpased: 6.236
batch start
#iterations: 315
currently lose_sum: 568.1598351299763
time_elpased: 5.942
batch start
#iterations: 316
currently lose_sum: 570.1752013862133
time_elpased: 5.96
batch start
#iterations: 317
currently lose_sum: 567.4342088699341
time_elpased: 6.096
batch start
#iterations: 318
currently lose_sum: 569.7479350864887
time_elpased: 5.991
batch start
#iterations: 319
currently lose_sum: 567.7602268457413
time_elpased: 5.892
start validation test
0.575801104972
0.570412321293
0.84995368941
0.682674822285
0
validation finish
batch start
#iterations: 320
currently lose_sum: 571.7030973434448
time_elpased: 6.031
batch start
#iterations: 321
currently lose_sum: 569.7297907471657
time_elpased: 6.105
batch start
#iterations: 322
currently lose_sum: 569.8697550296783
time_elpased: 6.1
batch start
#iterations: 323
currently lose_sum: 571.036269068718
time_elpased: 6.028
batch start
#iterations: 324
currently lose_sum: 570.4147633314133
time_elpased: 6.023
batch start
#iterations: 325
currently lose_sum: 570.8345011770725
time_elpased: 6.143
batch start
#iterations: 326
currently lose_sum: 571.2287207841873
time_elpased: 6.106
batch start
#iterations: 327
currently lose_sum: 570.2141333818436
time_elpased: 6.238
batch start
#iterations: 328
currently lose_sum: 569.7823258042336
time_elpased: 5.966
batch start
#iterations: 329
currently lose_sum: 569.4039438962936
time_elpased: 5.973
batch start
#iterations: 330
currently lose_sum: 568.059097468853
time_elpased: 6.071
batch start
#iterations: 331
currently lose_sum: 568.6892474889755
time_elpased: 6.235
batch start
#iterations: 332
currently lose_sum: 567.7551292479038
time_elpased: 6.116
batch start
#iterations: 333
currently lose_sum: 568.1221341490746
time_elpased: 6.031
batch start
#iterations: 334
currently lose_sum: 570.0285573005676
time_elpased: 5.931
batch start
#iterations: 335
currently lose_sum: 568.6418996453285
time_elpased: 6.213
batch start
#iterations: 336
currently lose_sum: 571.7685680985451
time_elpased: 6.26
batch start
#iterations: 337
currently lose_sum: 568.9051340222359
time_elpased: 6.279
batch start
#iterations: 338
currently lose_sum: 569.8633072376251
time_elpased: 6.094
batch start
#iterations: 339
currently lose_sum: 569.3364027142525
time_elpased: 6.137
start validation test
0.574972375691
0.570473537604
0.843058557168
0.680483448935
0
validation finish
batch start
#iterations: 340
currently lose_sum: 570.7214256823063
time_elpased: 5.923
batch start
#iterations: 341
currently lose_sum: 570.5881110727787
time_elpased: 6.084
batch start
#iterations: 342
currently lose_sum: 570.7537535727024
time_elpased: 6.011
batch start
#iterations: 343
currently lose_sum: 569.7597375512123
time_elpased: 6.062
batch start
#iterations: 344
currently lose_sum: 568.9432511925697
time_elpased: 6.05
batch start
#iterations: 345
currently lose_sum: 569.3989822268486
time_elpased: 6.039
batch start
#iterations: 346
currently lose_sum: 568.7247340083122
time_elpased: 5.989
batch start
#iterations: 347
currently lose_sum: 569.6945263743401
time_elpased: 6.11
batch start
#iterations: 348
currently lose_sum: 568.236322671175
time_elpased: 6.037
batch start
#iterations: 349
currently lose_sum: 568.5605828166008
time_elpased: 6.11
batch start
#iterations: 350
currently lose_sum: 569.5838694274426
time_elpased: 6.119
batch start
#iterations: 351
currently lose_sum: 569.5002499818802
time_elpased: 5.966
batch start
#iterations: 352
currently lose_sum: 568.3558378815651
time_elpased: 6.074
batch start
#iterations: 353
currently lose_sum: 568.6429736316204
time_elpased: 6.065
batch start
#iterations: 354
currently lose_sum: 568.1942436099052
time_elpased: 6.313
batch start
#iterations: 355
currently lose_sum: 569.1198509335518
time_elpased: 5.925
batch start
#iterations: 356
currently lose_sum: 566.4835304021835
time_elpased: 6.151
batch start
#iterations: 357
currently lose_sum: 568.4338903129101
time_elpased: 6.267
batch start
#iterations: 358
currently lose_sum: 570.4078848958015
time_elpased: 6.119
batch start
#iterations: 359
currently lose_sum: 568.0364877581596
time_elpased: 6.077
start validation test
0.575690607735
0.570255915017
0.850776988783
0.682828115966
0
validation finish
batch start
#iterations: 360
currently lose_sum: 569.9930374324322
time_elpased: 5.749
batch start
#iterations: 361
currently lose_sum: 568.4707968533039
time_elpased: 5.985
batch start
#iterations: 362
currently lose_sum: 570.3016229271889
time_elpased: 6.139
batch start
#iterations: 363
currently lose_sum: 565.9853872656822
time_elpased: 6.042
batch start
#iterations: 364
currently lose_sum: 569.0827969908714
time_elpased: 6.032
batch start
#iterations: 365
currently lose_sum: 568.7888197600842
time_elpased: 6.158
batch start
#iterations: 366
currently lose_sum: 567.1020986735821
time_elpased: 6.222
batch start
#iterations: 367
currently lose_sum: 568.4030044078827
time_elpased: 6.221
batch start
#iterations: 368
currently lose_sum: 570.406537771225
time_elpased: 6.186
batch start
#iterations: 369
currently lose_sum: 570.8029680550098
time_elpased: 6.039
batch start
#iterations: 370
currently lose_sum: 569.152493417263
time_elpased: 5.897
batch start
#iterations: 371
currently lose_sum: 569.4155247807503
time_elpased: 6.093
batch start
#iterations: 372
currently lose_sum: 569.5656314492226
time_elpased: 5.998
batch start
#iterations: 373
currently lose_sum: 569.8694767057896
time_elpased: 6.161
batch start
#iterations: 374
currently lose_sum: 569.7178472280502
time_elpased: 5.856
batch start
#iterations: 375
currently lose_sum: 567.969713807106
time_elpased: 6.133
batch start
#iterations: 376
currently lose_sum: 567.377431422472
time_elpased: 6.33
batch start
#iterations: 377
currently lose_sum: 570.6682273745537
time_elpased: 6.088
batch start
#iterations: 378
currently lose_sum: 567.9154868721962
time_elpased: 5.933
batch start
#iterations: 379
currently lose_sum: 567.4531678557396
time_elpased: 6.003
start validation test
0.574917127072
0.569409181363
0.85396727385
0.683244133388
0
validation finish
batch start
#iterations: 380
currently lose_sum: 566.8105720281601
time_elpased: 6.018
batch start
#iterations: 381
currently lose_sum: 567.7076705694199
time_elpased: 6.167
batch start
#iterations: 382
currently lose_sum: 569.0211380124092
time_elpased: 6.217
batch start
#iterations: 383
currently lose_sum: 570.5350966453552
time_elpased: 5.996
batch start
#iterations: 384
currently lose_sum: 572.1734952032566
time_elpased: 6.197
batch start
#iterations: 385
currently lose_sum: 567.7778599262238
time_elpased: 5.926
batch start
#iterations: 386
currently lose_sum: 567.7387693524361
time_elpased: 6.24
batch start
#iterations: 387
currently lose_sum: 569.1829689443111
time_elpased: 6.017
batch start
#iterations: 388
currently lose_sum: 570.9246323108673
time_elpased: 6.034
batch start
#iterations: 389
currently lose_sum: 571.7749288380146
time_elpased: 6.135
batch start
#iterations: 390
currently lose_sum: 568.625068128109
time_elpased: 5.976
batch start
#iterations: 391
currently lose_sum: 566.8770866692066
time_elpased: 6.285
batch start
#iterations: 392
currently lose_sum: 567.5380631685257
time_elpased: 6.382
batch start
#iterations: 393
currently lose_sum: 567.8245438337326
time_elpased: 6.292
batch start
#iterations: 394
currently lose_sum: 570.4321538209915
time_elpased: 6.256
batch start
#iterations: 395
currently lose_sum: 568.3090683221817
time_elpased: 5.948
batch start
#iterations: 396
currently lose_sum: 569.975350946188
time_elpased: 6.255
batch start
#iterations: 397
currently lose_sum: 569.0639667212963
time_elpased: 6.099
batch start
#iterations: 398
currently lose_sum: 570.3470471203327
time_elpased: 6.197
batch start
#iterations: 399
currently lose_sum: 568.2883133292198
time_elpased: 5.963
start validation test
0.575055248619
0.569784668389
0.850982813626
0.682556387874
0
validation finish
acc: 0.546
pre: 0.544
rec: 0.947
F1: 0.691
auc: 0.000
