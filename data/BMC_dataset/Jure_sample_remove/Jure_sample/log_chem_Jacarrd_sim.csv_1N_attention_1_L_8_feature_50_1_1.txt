start to construct graph
graph construct over
93177
epochs start
batch start
#iterations: 0
currently lose_sum: 617.0850486159325
time_elpased: 5.832
batch start
#iterations: 1
currently lose_sum: 615.0521773695946
time_elpased: 5.779
batch start
#iterations: 2
currently lose_sum: 615.7220829129219
time_elpased: 5.87
batch start
#iterations: 3
currently lose_sum: 614.8608799576759
time_elpased: 5.678
batch start
#iterations: 4
currently lose_sum: 614.1766978502274
time_elpased: 5.834
batch start
#iterations: 5
currently lose_sum: 613.2058449983597
time_elpased: 5.834
batch start
#iterations: 6
currently lose_sum: 614.9364183545113
time_elpased: 5.946
batch start
#iterations: 7
currently lose_sum: 612.6736605763435
time_elpased: 5.93
batch start
#iterations: 8
currently lose_sum: 613.0673462152481
time_elpased: 5.827
batch start
#iterations: 9
currently lose_sum: 611.1884375810623
time_elpased: 5.869
batch start
#iterations: 10
currently lose_sum: 611.5503212809563
time_elpased: 5.847
batch start
#iterations: 11
currently lose_sum: 610.8217045068741
time_elpased: 5.753
batch start
#iterations: 12
currently lose_sum: 609.9009432792664
time_elpased: 5.905
batch start
#iterations: 13
currently lose_sum: 607.316858112812
time_elpased: 5.893
batch start
#iterations: 14
currently lose_sum: 605.8613527417183
time_elpased: 5.99
batch start
#iterations: 15
currently lose_sum: 605.4032278060913
time_elpased: 5.723
batch start
#iterations: 16
currently lose_sum: 605.8936628699303
time_elpased: 6.118
batch start
#iterations: 17
currently lose_sum: 604.47619587183
time_elpased: 6.089
batch start
#iterations: 18
currently lose_sum: 602.0433304309845
time_elpased: 5.535
batch start
#iterations: 19
currently lose_sum: 600.1374514102936
time_elpased: 6.004
start validation test
0.543259668508
0.542752683099
0.947205927755
0.690084348641
0
validation finish
batch start
#iterations: 20
currently lose_sum: 599.1903865933418
time_elpased: 5.538
batch start
#iterations: 21
currently lose_sum: 601.4077637791634
time_elpased: 6.01
batch start
#iterations: 22
currently lose_sum: 600.5221263170242
time_elpased: 6.051
batch start
#iterations: 23
currently lose_sum: 597.6456553936005
time_elpased: 5.871
batch start
#iterations: 24
currently lose_sum: 598.2785192728043
time_elpased: 6.017
batch start
#iterations: 25
currently lose_sum: 595.7049632072449
time_elpased: 5.756
batch start
#iterations: 26
currently lose_sum: 594.746609389782
time_elpased: 5.895
batch start
#iterations: 27
currently lose_sum: 595.9755619168282
time_elpased: 5.611
batch start
#iterations: 28
currently lose_sum: 595.3348682522774
time_elpased: 5.985
batch start
#iterations: 29
currently lose_sum: 594.7136306762695
time_elpased: 5.712
batch start
#iterations: 30
currently lose_sum: 594.6171322464943
time_elpased: 5.96
batch start
#iterations: 31
currently lose_sum: 596.5474086403847
time_elpased: 5.823
batch start
#iterations: 32
currently lose_sum: 594.4257334470749
time_elpased: 5.773
batch start
#iterations: 33
currently lose_sum: 591.9625207185745
time_elpased: 5.89
batch start
#iterations: 34
currently lose_sum: 590.7909488081932
time_elpased: 5.878
batch start
#iterations: 35
currently lose_sum: 592.9148283004761
time_elpased: 6.004
batch start
#iterations: 36
currently lose_sum: 591.0725632309914
time_elpased: 5.564
batch start
#iterations: 37
currently lose_sum: 591.1906052827835
time_elpased: 6.007
batch start
#iterations: 38
currently lose_sum: 591.512237071991
time_elpased: 5.606
batch start
#iterations: 39
currently lose_sum: 589.6950333118439
time_elpased: 6.069
start validation test
0.56820441989
0.566159841342
0.837295461562
0.675537104307
0
validation finish
batch start
#iterations: 40
currently lose_sum: 591.009226500988
time_elpased: 5.715
batch start
#iterations: 41
currently lose_sum: 591.0434795618057
time_elpased: 5.941
batch start
#iterations: 42
currently lose_sum: 588.3637595176697
time_elpased: 5.915
batch start
#iterations: 43
currently lose_sum: 589.8840534687042
time_elpased: 5.844
batch start
#iterations: 44
currently lose_sum: 589.7516042590141
time_elpased: 5.984
batch start
#iterations: 45
currently lose_sum: 588.2668347358704
time_elpased: 5.689
batch start
#iterations: 46
currently lose_sum: 588.5786531567574
time_elpased: 5.657
batch start
#iterations: 47
currently lose_sum: 588.8833782076836
time_elpased: 5.609
batch start
#iterations: 48
currently lose_sum: 587.2371509671211
time_elpased: 6.152
batch start
#iterations: 49
currently lose_sum: 587.381657898426
time_elpased: 5.821
batch start
#iterations: 50
currently lose_sum: 588.7569410800934
time_elpased: 5.926
batch start
#iterations: 51
currently lose_sum: 586.2481716871262
time_elpased: 6.035
batch start
#iterations: 52
currently lose_sum: 588.3283421993256
time_elpased: 5.987
batch start
#iterations: 53
currently lose_sum: 587.6911436617374
time_elpased: 5.847
batch start
#iterations: 54
currently lose_sum: 587.0143311023712
time_elpased: 5.952
batch start
#iterations: 55
currently lose_sum: 586.1243132352829
time_elpased: 5.755
batch start
#iterations: 56
currently lose_sum: 585.2387212514877
time_elpased: 6.022
batch start
#iterations: 57
currently lose_sum: 585.8868487477303
time_elpased: 5.84
batch start
#iterations: 58
currently lose_sum: 584.7358911633492
time_elpased: 6.0
batch start
#iterations: 59
currently lose_sum: 585.9906619787216
time_elpased: 5.951
start validation test
0.571160220994
0.566078550666
0.861788617886
0.683312933497
0
validation finish
batch start
#iterations: 60
currently lose_sum: 585.3063345551491
time_elpased: 5.626
batch start
#iterations: 61
currently lose_sum: 584.5176109671593
time_elpased: 5.736
batch start
#iterations: 62
currently lose_sum: 585.6866929531097
time_elpased: 5.788
batch start
#iterations: 63
currently lose_sum: 584.8070612549782
time_elpased: 5.739
batch start
#iterations: 64
currently lose_sum: 585.0258903503418
time_elpased: 5.809
batch start
#iterations: 65
currently lose_sum: 582.7760535478592
time_elpased: 5.986
batch start
#iterations: 66
currently lose_sum: 584.1914850473404
time_elpased: 5.897
batch start
#iterations: 67
currently lose_sum: 583.8433155417442
time_elpased: 8.323
batch start
#iterations: 68
currently lose_sum: 583.1807748675346
time_elpased: 6.414
batch start
#iterations: 69
currently lose_sum: 583.25807929039
time_elpased: 5.841
batch start
#iterations: 70
currently lose_sum: 583.5518460273743
time_elpased: 6.074
batch start
#iterations: 71
currently lose_sum: 582.2031837701797
time_elpased: 5.8
batch start
#iterations: 72
currently lose_sum: 582.3498719334602
time_elpased: 5.949
batch start
#iterations: 73
currently lose_sum: 582.5587072372437
time_elpased: 5.787
batch start
#iterations: 74
currently lose_sum: 583.1068699359894
time_elpased: 6.015
batch start
#iterations: 75
currently lose_sum: 582.1928851604462
time_elpased: 6.118
batch start
#iterations: 76
currently lose_sum: 582.5782715678215
time_elpased: 5.861
batch start
#iterations: 77
currently lose_sum: 582.7940718531609
time_elpased: 5.892
batch start
#iterations: 78
currently lose_sum: 582.9515832066536
time_elpased: 6.377
batch start
#iterations: 79
currently lose_sum: 581.099243760109
time_elpased: 6.222
start validation test
0.575635359116
0.569726027397
0.856025522281
0.684130443722
0
validation finish
batch start
#iterations: 80
currently lose_sum: 581.3533222973347
time_elpased: 6.165
batch start
#iterations: 81
currently lose_sum: 580.8201281130314
time_elpased: 5.759
batch start
#iterations: 82
currently lose_sum: 581.1864123344421
time_elpased: 6.548
batch start
#iterations: 83
currently lose_sum: 581.5490372180939
time_elpased: 6.199
batch start
#iterations: 84
currently lose_sum: 581.1136030554771
time_elpased: 5.88
batch start
#iterations: 85
currently lose_sum: 582.7023533582687
time_elpased: 5.828
batch start
#iterations: 86
currently lose_sum: 582.0574354529381
time_elpased: 5.83
batch start
#iterations: 87
currently lose_sum: 580.5521769523621
time_elpased: 5.829
batch start
#iterations: 88
currently lose_sum: 581.1417939662933
time_elpased: 5.828
batch start
#iterations: 89
currently lose_sum: 581.6044152379036
time_elpased: 5.722
batch start
#iterations: 90
currently lose_sum: 581.8941656947136
time_elpased: 5.807
batch start
#iterations: 91
currently lose_sum: 582.2829438447952
time_elpased: 5.893
batch start
#iterations: 92
currently lose_sum: 579.534689605236
time_elpased: 5.726
batch start
#iterations: 93
currently lose_sum: 581.0142697095871
time_elpased: 5.905
batch start
#iterations: 94
currently lose_sum: 581.1098986268044
time_elpased: 5.661
batch start
#iterations: 95
currently lose_sum: 579.2600598931313
time_elpased: 6.169
batch start
#iterations: 96
currently lose_sum: 579.5040686130524
time_elpased: 5.842
batch start
#iterations: 97
currently lose_sum: 578.8650112748146
time_elpased: 5.922
batch start
#iterations: 98
currently lose_sum: 579.3879527449608
time_elpased: 6.208
batch start
#iterations: 99
currently lose_sum: 580.8029463887215
time_elpased: 6.058
start validation test
0.574447513812
0.567250876314
0.874343933313
0.688088440746
0
validation finish
batch start
#iterations: 100
currently lose_sum: 578.6209544539452
time_elpased: 6.244
batch start
#iterations: 101
currently lose_sum: 579.4849908947945
time_elpased: 5.901
batch start
#iterations: 102
currently lose_sum: 578.465006262064
time_elpased: 5.825
batch start
#iterations: 103
currently lose_sum: 578.1670433282852
time_elpased: 6.016
batch start
#iterations: 104
currently lose_sum: 579.9612496495247
time_elpased: 5.72
batch start
#iterations: 105
currently lose_sum: 578.3450125455856
time_elpased: 6.035
batch start
#iterations: 106
currently lose_sum: 577.0141583681107
time_elpased: 6.071
batch start
#iterations: 107
currently lose_sum: 580.480338037014
time_elpased: 5.897
batch start
#iterations: 108
currently lose_sum: 577.610951423645
time_elpased: 6.136
batch start
#iterations: 109
currently lose_sum: 578.0745900273323
time_elpased: 5.927
batch start
#iterations: 110
currently lose_sum: 579.6723579764366
time_elpased: 5.839
batch start
#iterations: 111
currently lose_sum: 578.553394138813
time_elpased: 5.82
batch start
#iterations: 112
currently lose_sum: 577.1718185842037
time_elpased: 5.849
batch start
#iterations: 113
currently lose_sum: 577.2084626555443
time_elpased: 6.104
batch start
#iterations: 114
currently lose_sum: 578.641209423542
time_elpased: 5.94
batch start
#iterations: 115
currently lose_sum: 578.0810575783253
time_elpased: 5.904
batch start
#iterations: 116
currently lose_sum: 578.7867126464844
time_elpased: 5.899
batch start
#iterations: 117
currently lose_sum: 579.1727785468102
time_elpased: 5.784
batch start
#iterations: 118
currently lose_sum: 578.2721821665764
time_elpased: 6.057
batch start
#iterations: 119
currently lose_sum: 578.1580570340157
time_elpased: 5.684
start validation test
0.577403314917
0.569136132656
0.875990532057
0.68998500385
0
validation finish
batch start
#iterations: 120
currently lose_sum: 577.89817070961
time_elpased: 5.804
batch start
#iterations: 121
currently lose_sum: 576.0135679841042
time_elpased: 5.845
batch start
#iterations: 122
currently lose_sum: 576.9301800131798
time_elpased: 6.271
batch start
#iterations: 123
currently lose_sum: 578.1885586678982
time_elpased: 5.903
batch start
#iterations: 124
currently lose_sum: 576.9730046987534
time_elpased: 5.881
batch start
#iterations: 125
currently lose_sum: 576.4133824110031
time_elpased: 6.194
batch start
#iterations: 126
currently lose_sum: 578.5542125105858
time_elpased: 5.906
batch start
#iterations: 127
currently lose_sum: 577.2319723367691
time_elpased: 5.988
batch start
#iterations: 128
currently lose_sum: 577.0932868719101
time_elpased: 6.041
batch start
#iterations: 129
currently lose_sum: 576.7970095574856
time_elpased: 5.631
batch start
#iterations: 130
currently lose_sum: 576.3197646141052
time_elpased: 5.946
batch start
#iterations: 131
currently lose_sum: 577.1225673556328
time_elpased: 5.566
batch start
#iterations: 132
currently lose_sum: 576.5445613861084
time_elpased: 5.94
batch start
#iterations: 133
currently lose_sum: 575.8986035585403
time_elpased: 5.625
batch start
#iterations: 134
currently lose_sum: 574.4856548309326
time_elpased: 5.915
batch start
#iterations: 135
currently lose_sum: 575.7118883430958
time_elpased: 5.739
batch start
#iterations: 136
currently lose_sum: 577.7772336602211
time_elpased: 6.023
batch start
#iterations: 137
currently lose_sum: 576.1257411837578
time_elpased: 5.923
batch start
#iterations: 138
currently lose_sum: 576.5999000072479
time_elpased: 6.012
batch start
#iterations: 139
currently lose_sum: 577.7053748071194
time_elpased: 5.948
start validation test
0.577569060773
0.569001132805
0.878769167439
0.690745834007
0
validation finish
batch start
#iterations: 140
currently lose_sum: 577.0443914532661
time_elpased: 5.632
batch start
#iterations: 141
currently lose_sum: 573.7421294152737
time_elpased: 5.997
batch start
#iterations: 142
currently lose_sum: 576.6638697981834
time_elpased: 5.665
batch start
#iterations: 143
currently lose_sum: 576.8778643608093
time_elpased: 5.918
batch start
#iterations: 144
currently lose_sum: 576.6294605433941
time_elpased: 6.084
batch start
#iterations: 145
currently lose_sum: 576.4009660482407
time_elpased: 5.676
batch start
#iterations: 146
currently lose_sum: 576.1457222104073
time_elpased: 5.886
batch start
#iterations: 147
currently lose_sum: 575.9834940731525
time_elpased: 5.844
batch start
#iterations: 148
currently lose_sum: 576.5133529007435
time_elpased: 5.708
batch start
#iterations: 149
currently lose_sum: 576.4084738492966
time_elpased: 5.706
batch start
#iterations: 150
currently lose_sum: 576.1449395418167
time_elpased: 5.892
batch start
#iterations: 151
currently lose_sum: 576.6518645882607
time_elpased: 5.803
batch start
#iterations: 152
currently lose_sum: 575.5551325678825
time_elpased: 6.007
batch start
#iterations: 153
currently lose_sum: 573.7466052174568
time_elpased: 5.772
batch start
#iterations: 154
currently lose_sum: 574.0375330448151
time_elpased: 6.045
batch start
#iterations: 155
currently lose_sum: 575.5529860854149
time_elpased: 5.884
batch start
#iterations: 156
currently lose_sum: 575.7443197965622
time_elpased: 5.84
batch start
#iterations: 157
currently lose_sum: 575.7634189426899
time_elpased: 6.061
batch start
#iterations: 158
currently lose_sum: 575.1465494036674
time_elpased: 5.793
batch start
#iterations: 159
currently lose_sum: 574.9107761383057
time_elpased: 6.105
start validation test
0.577762430939
0.569485848267
0.87485849542
0.689890239201
0
validation finish
batch start
#iterations: 160
currently lose_sum: 577.5396656394005
time_elpased: 5.61
batch start
#iterations: 161
currently lose_sum: 574.8607841730118
time_elpased: 5.969
batch start
#iterations: 162
currently lose_sum: 575.205235004425
time_elpased: 5.488
batch start
#iterations: 163
currently lose_sum: 575.2192685008049
time_elpased: 6.003
batch start
#iterations: 164
currently lose_sum: 574.2519304156303
time_elpased: 5.805
batch start
#iterations: 165
currently lose_sum: 572.86678647995
time_elpased: 5.928
batch start
#iterations: 166
currently lose_sum: 578.4705022871494
time_elpased: 5.916
batch start
#iterations: 167
currently lose_sum: 575.3054648637772
time_elpased: 5.758
batch start
#iterations: 168
currently lose_sum: 576.3187094926834
time_elpased: 5.921
batch start
#iterations: 169
currently lose_sum: 572.6655035018921
time_elpased: 5.891
batch start
#iterations: 170
currently lose_sum: 575.1263279914856
time_elpased: 5.827
batch start
#iterations: 171
currently lose_sum: 576.3430558145046
time_elpased: 5.817
batch start
#iterations: 172
currently lose_sum: 571.183735281229
time_elpased: 5.731
batch start
#iterations: 173
currently lose_sum: 574.1767002344131
time_elpased: 5.989
batch start
#iterations: 174
currently lose_sum: 575.5067520737648
time_elpased: 5.765
batch start
#iterations: 175
currently lose_sum: 574.6213265061378
time_elpased: 5.993
batch start
#iterations: 176
currently lose_sum: 577.5711032748222
time_elpased: 5.589
batch start
#iterations: 177
currently lose_sum: 573.3225763440132
time_elpased: 6.792
batch start
#iterations: 178
currently lose_sum: 573.2426086068153
time_elpased: 8.479
batch start
#iterations: 179
currently lose_sum: 574.8516457080841
time_elpased: 5.692
start validation test
0.578867403315
0.573267569175
0.843264382011
0.682534724391
0
validation finish
batch start
#iterations: 180
currently lose_sum: 573.914100497961
time_elpased: 6.005
batch start
#iterations: 181
currently lose_sum: 575.2626871466637
time_elpased: 5.654
batch start
#iterations: 182
currently lose_sum: 574.1283213794231
time_elpased: 5.565
batch start
#iterations: 183
currently lose_sum: 575.9712979197502
time_elpased: 5.546
batch start
#iterations: 184
currently lose_sum: 574.092524766922
time_elpased: 6.006
batch start
#iterations: 185
currently lose_sum: 574.3464735746384
time_elpased: 5.512
batch start
#iterations: 186
currently lose_sum: 572.6002057790756
time_elpased: 5.753
batch start
#iterations: 187
currently lose_sum: 573.9698736667633
time_elpased: 5.576
batch start
#iterations: 188
currently lose_sum: 573.907556772232
time_elpased: 5.495
batch start
#iterations: 189
currently lose_sum: 574.7218207716942
time_elpased: 7.048
batch start
#iterations: 190
currently lose_sum: 572.3282436132431
time_elpased: 5.332
batch start
#iterations: 191
currently lose_sum: 574.3893498182297
time_elpased: 5.861
batch start
#iterations: 192
currently lose_sum: 573.0150279402733
time_elpased: 5.881
batch start
#iterations: 193
currently lose_sum: 573.2528230547905
time_elpased: 5.871
batch start
#iterations: 194
currently lose_sum: 572.3302465677261
time_elpased: 5.94
batch start
#iterations: 195
currently lose_sum: 572.7512966096401
time_elpased: 5.924
batch start
#iterations: 196
currently lose_sum: 574.4015306532383
time_elpased: 5.523
batch start
#iterations: 197
currently lose_sum: 572.6036055088043
time_elpased: 5.632
batch start
#iterations: 198
currently lose_sum: 574.4682103097439
time_elpased: 5.88
batch start
#iterations: 199
currently lose_sum: 573.2199551165104
time_elpased: 5.756
start validation test
0.58044198895
0.57231419034
0.864567253267
0.688719462207
0
validation finish
batch start
#iterations: 200
currently lose_sum: 570.6708458065987
time_elpased: 5.687
batch start
#iterations: 201
currently lose_sum: 572.0575231909752
time_elpased: 5.77
batch start
#iterations: 202
currently lose_sum: 570.7159743905067
time_elpased: 5.58
batch start
#iterations: 203
currently lose_sum: 572.8587039113045
time_elpased: 5.766
batch start
#iterations: 204
currently lose_sum: 573.5601924359798
time_elpased: 5.724
batch start
#iterations: 205
currently lose_sum: 573.0158717632294
time_elpased: 5.88
batch start
#iterations: 206
currently lose_sum: 571.7026980519295
time_elpased: 5.568
batch start
#iterations: 207
currently lose_sum: 571.8166022896767
time_elpased: 5.879
batch start
#iterations: 208
currently lose_sum: 570.4782861471176
time_elpased: 5.836
batch start
#iterations: 209
currently lose_sum: 573.1685194671154
time_elpased: 5.646
batch start
#iterations: 210
currently lose_sum: 573.819985806942
time_elpased: 5.634
batch start
#iterations: 211
currently lose_sum: 571.4969566762447
time_elpased: 5.854
batch start
#iterations: 212
currently lose_sum: 573.6821378469467
time_elpased: 5.697
batch start
#iterations: 213
currently lose_sum: 572.2586491703987
time_elpased: 5.512
batch start
#iterations: 214
currently lose_sum: 572.3319109678268
time_elpased: 5.729
batch start
#iterations: 215
currently lose_sum: 573.7917539179325
time_elpased: 5.806
batch start
#iterations: 216
currently lose_sum: 571.473474919796
time_elpased: 5.636
batch start
#iterations: 217
currently lose_sum: 572.0016873180866
time_elpased: 5.854
batch start
#iterations: 218
currently lose_sum: 571.3988103270531
time_elpased: 5.767
batch start
#iterations: 219
currently lose_sum: 573.3142875432968
time_elpased: 5.833
start validation test
0.577016574586
0.56818632965
0.883708963672
0.691663310511
0
validation finish
batch start
#iterations: 220
currently lose_sum: 573.0573400259018
time_elpased: 5.475
batch start
#iterations: 221
currently lose_sum: 571.1250862181187
time_elpased: 5.78
batch start
#iterations: 222
currently lose_sum: 571.2067026793957
time_elpased: 5.756
batch start
#iterations: 223
currently lose_sum: 571.9200239479542
time_elpased: 5.96
batch start
#iterations: 224
currently lose_sum: 571.0877240300179
time_elpased: 5.86
batch start
#iterations: 225
currently lose_sum: 572.1340590715408
time_elpased: 5.625
batch start
#iterations: 226
currently lose_sum: 574.6266508698463
time_elpased: 5.886
batch start
#iterations: 227
currently lose_sum: 571.4316853880882
time_elpased: 5.57
batch start
#iterations: 228
currently lose_sum: 568.6277640759945
time_elpased: 6.02
batch start
#iterations: 229
currently lose_sum: 572.6929142177105
time_elpased: 5.591
batch start
#iterations: 230
currently lose_sum: 570.8838099241257
time_elpased: 5.821
batch start
#iterations: 231
currently lose_sum: 571.5953357815742
time_elpased: 5.53
batch start
#iterations: 232
currently lose_sum: 572.7487102150917
time_elpased: 5.959
batch start
#iterations: 233
currently lose_sum: 572.369348347187
time_elpased: 5.498
batch start
#iterations: 234
currently lose_sum: 570.284126996994
time_elpased: 5.801
batch start
#iterations: 235
currently lose_sum: 571.8549253046513
time_elpased: 5.593
batch start
#iterations: 236
currently lose_sum: 569.8801272213459
time_elpased: 5.838
batch start
#iterations: 237
currently lose_sum: 572.9300006628036
time_elpased: 5.712
batch start
#iterations: 238
currently lose_sum: 570.8988212049007
time_elpased: 5.413
batch start
#iterations: 239
currently lose_sum: 572.8080983161926
time_elpased: 5.821
start validation test
0.577458563536
0.569109492952
0.876710919008
0.690188770963
0
validation finish
batch start
#iterations: 240
currently lose_sum: 571.6798425912857
time_elpased: 5.748
batch start
#iterations: 241
currently lose_sum: 572.6764364242554
time_elpased: 5.733
batch start
#iterations: 242
currently lose_sum: 572.1359921097755
time_elpased: 5.64
batch start
#iterations: 243
currently lose_sum: 571.75739762187
time_elpased: 5.702
batch start
#iterations: 244
currently lose_sum: 570.1112566590309
time_elpased: 5.623
batch start
#iterations: 245
currently lose_sum: 572.1227887272835
time_elpased: 5.852
batch start
#iterations: 246
currently lose_sum: 569.996820807457
time_elpased: 5.724
batch start
#iterations: 247
currently lose_sum: 572.0816510021687
time_elpased: 5.883
batch start
#iterations: 248
currently lose_sum: 573.1734908521175
time_elpased: 5.613
batch start
#iterations: 249
currently lose_sum: 570.2103185653687
time_elpased: 5.834
batch start
#iterations: 250
currently lose_sum: 571.0037583112717
time_elpased: 5.565
batch start
#iterations: 251
currently lose_sum: 569.2092951536179
time_elpased: 5.772
batch start
#iterations: 252
currently lose_sum: 571.7447736859322
time_elpased: 5.94
batch start
#iterations: 253
currently lose_sum: 572.2497217655182
time_elpased: 5.651
batch start
#iterations: 254
currently lose_sum: 571.8703796565533
time_elpased: 5.65
batch start
#iterations: 255
currently lose_sum: 570.6212176382542
time_elpased: 5.82
batch start
#iterations: 256
currently lose_sum: 573.0015431940556
time_elpased: 5.631
batch start
#iterations: 257
currently lose_sum: 572.8015419244766
time_elpased: 5.72
batch start
#iterations: 258
currently lose_sum: 570.3599701523781
time_elpased: 5.816
batch start
#iterations: 259
currently lose_sum: 570.2702358365059
time_elpased: 5.625
start validation test
0.578480662983
0.569405193337
0.881239065555
0.691805861324
0
validation finish
batch start
#iterations: 260
currently lose_sum: 571.7066962718964
time_elpased: 5.822
batch start
#iterations: 261
currently lose_sum: 572.0366003811359
time_elpased: 5.594
batch start
#iterations: 262
currently lose_sum: 572.3643593788147
time_elpased: 5.908
batch start
#iterations: 263
currently lose_sum: 569.0465510189533
time_elpased: 5.722
batch start
#iterations: 264
currently lose_sum: 571.529079914093
time_elpased: 5.983
batch start
#iterations: 265
currently lose_sum: 573.3725680112839
time_elpased: 5.678
batch start
#iterations: 266
currently lose_sum: 570.5631544589996
time_elpased: 5.835
batch start
#iterations: 267
currently lose_sum: 572.251173555851
time_elpased: 5.926
batch start
#iterations: 268
currently lose_sum: 571.7864897251129
time_elpased: 5.786
batch start
#iterations: 269
currently lose_sum: 571.3871304988861
time_elpased: 5.803
batch start
#iterations: 270
currently lose_sum: 571.1359328627586
time_elpased: 5.579
batch start
#iterations: 271
currently lose_sum: 569.52069491148
time_elpased: 5.936
batch start
#iterations: 272
currently lose_sum: 569.3383612632751
time_elpased: 5.953
batch start
#iterations: 273
currently lose_sum: 571.7922114729881
time_elpased: 5.618
batch start
#iterations: 274
currently lose_sum: 570.249994546175
time_elpased: 5.768
batch start
#iterations: 275
currently lose_sum: 573.4603268504143
time_elpased: 5.739
batch start
#iterations: 276
currently lose_sum: 568.8650260567665
time_elpased: 6.354
batch start
#iterations: 277
currently lose_sum: 569.0624570250511
time_elpased: 5.907
batch start
#iterations: 278
currently lose_sum: 570.539665043354
time_elpased: 5.854
batch start
#iterations: 279
currently lose_sum: 571.9656391739845
time_elpased: 5.75
start validation test
0.578867403315
0.570562274703
0.87146238551
0.689618665635
0
validation finish
batch start
#iterations: 280
currently lose_sum: 571.0246843099594
time_elpased: 5.842
batch start
#iterations: 281
currently lose_sum: 570.2769506275654
time_elpased: 6.0
batch start
#iterations: 282
currently lose_sum: 570.9646104872227
time_elpased: 5.641
batch start
#iterations: 283
currently lose_sum: 568.2727570235729
time_elpased: 5.993
batch start
#iterations: 284
currently lose_sum: 571.312314748764
time_elpased: 5.927
batch start
#iterations: 285
currently lose_sum: 570.5605221986771
time_elpased: 5.799
batch start
#iterations: 286
currently lose_sum: 571.4243916273117
time_elpased: 5.859
batch start
#iterations: 287
currently lose_sum: 572.30269998312
time_elpased: 5.681
batch start
#iterations: 288
currently lose_sum: 570.5662658214569
time_elpased: 5.733
batch start
#iterations: 289
currently lose_sum: 571.1606746912003
time_elpased: 6.083
batch start
#iterations: 290
currently lose_sum: 570.0810183882713
time_elpased: 5.729
batch start
#iterations: 291
currently lose_sum: 570.5005919635296
time_elpased: 5.788
batch start
#iterations: 292
currently lose_sum: 569.5002731978893
time_elpased: 6.076
batch start
#iterations: 293
currently lose_sum: 567.4965061545372
time_elpased: 5.867
batch start
#iterations: 294
currently lose_sum: 571.1674606800079
time_elpased: 5.738
batch start
#iterations: 295
currently lose_sum: 571.2509577870369
time_elpased: 5.601
batch start
#iterations: 296
currently lose_sum: 570.7283599674702
time_elpased: 6.055
batch start
#iterations: 297
currently lose_sum: 572.2237649857998
time_elpased: 5.808
batch start
#iterations: 298
currently lose_sum: 572.4972210526466
time_elpased: 5.845
batch start
#iterations: 299
currently lose_sum: 568.9246937334538
time_elpased: 5.847
start validation test
0.579337016575
0.570946629782
0.870844910981
0.689705762491
0
validation finish
batch start
#iterations: 300
currently lose_sum: 569.7294185161591
time_elpased: 5.702
batch start
#iterations: 301
currently lose_sum: 567.49720916152
time_elpased: 5.498
batch start
#iterations: 302
currently lose_sum: 570.493520706892
time_elpased: 6.114
batch start
#iterations: 303
currently lose_sum: 569.8300816714764
time_elpased: 5.526
batch start
#iterations: 304
currently lose_sum: 568.94276034832
time_elpased: 6.208
batch start
#iterations: 305
currently lose_sum: 570.295486330986
time_elpased: 5.858
batch start
#iterations: 306
currently lose_sum: 568.64278241992
time_elpased: 5.523
batch start
#iterations: 307
currently lose_sum: 568.2350061833858
time_elpased: 6.028
batch start
#iterations: 308
currently lose_sum: 570.5020222663879
time_elpased: 5.783
batch start
#iterations: 309
currently lose_sum: 571.5558103322983
time_elpased: 5.976
batch start
#iterations: 310
currently lose_sum: 569.0528816580772
time_elpased: 5.651
batch start
#iterations: 311
currently lose_sum: 571.5497236251831
time_elpased: 6.084
batch start
#iterations: 312
currently lose_sum: 570.4335144162178
time_elpased: 5.759
batch start
#iterations: 313
currently lose_sum: 570.6436731815338
time_elpased: 5.762
batch start
#iterations: 314
currently lose_sum: 570.160273373127
time_elpased: 5.812
batch start
#iterations: 315
currently lose_sum: 569.1459380984306
time_elpased: 5.805
batch start
#iterations: 316
currently lose_sum: 570.4663565456867
time_elpased: 5.925
batch start
#iterations: 317
currently lose_sum: 568.0643018484116
time_elpased: 5.82
batch start
#iterations: 318
currently lose_sum: 569.66984552145
time_elpased: 5.965
batch start
#iterations: 319
currently lose_sum: 568.2497614622116
time_elpased: 5.648
start validation test
0.579447513812
0.572431353658
0.856025522281
0.686077202243
0
validation finish
batch start
#iterations: 320
currently lose_sum: 571.3590986132622
time_elpased: 6.071
batch start
#iterations: 321
currently lose_sum: 570.1871072947979
time_elpased: 5.483
batch start
#iterations: 322
currently lose_sum: 570.3436383903027
time_elpased: 6.066
batch start
#iterations: 323
currently lose_sum: 571.0054568052292
time_elpased: 5.618
batch start
#iterations: 324
currently lose_sum: 570.1088138222694
time_elpased: 5.804
batch start
#iterations: 325
currently lose_sum: 570.3810509443283
time_elpased: 5.709
batch start
#iterations: 326
currently lose_sum: 571.0704916715622
time_elpased: 6.017
batch start
#iterations: 327
currently lose_sum: 570.275550365448
time_elpased: 8.159
batch start
#iterations: 328
currently lose_sum: 569.5156202316284
time_elpased: 7.019
batch start
#iterations: 329
currently lose_sum: 569.9175046086311
time_elpased: 5.683
batch start
#iterations: 330
currently lose_sum: 568.5057116150856
time_elpased: 5.82
batch start
#iterations: 331
currently lose_sum: 569.3239035606384
time_elpased: 5.875
batch start
#iterations: 332
currently lose_sum: 568.3383297622204
time_elpased: 5.491
batch start
#iterations: 333
currently lose_sum: 567.9301906526089
time_elpased: 5.62
batch start
#iterations: 334
currently lose_sum: 569.8517746329308
time_elpased: 5.641
batch start
#iterations: 335
currently lose_sum: 568.6387436389923
time_elpased: 5.745
batch start
#iterations: 336
currently lose_sum: 570.7075173258781
time_elpased: 5.531
batch start
#iterations: 337
currently lose_sum: 569.0048228502274
time_elpased: 5.5
batch start
#iterations: 338
currently lose_sum: 568.8206812143326
time_elpased: 5.958
batch start
#iterations: 339
currently lose_sum: 569.1718907654285
time_elpased: 5.472
start validation test
0.578591160221
0.571107708014
0.863538129052
0.687519203589
0
validation finish
batch start
#iterations: 340
currently lose_sum: 570.2917022109032
time_elpased: 5.868
batch start
#iterations: 341
currently lose_sum: 570.5568205714226
time_elpased: 5.843
batch start
#iterations: 342
currently lose_sum: 570.60861992836
time_elpased: 5.761
batch start
#iterations: 343
currently lose_sum: 570.5903598964214
time_elpased: 5.611
batch start
#iterations: 344
currently lose_sum: 569.2276460528374
time_elpased: 5.7
batch start
#iterations: 345
currently lose_sum: 570.3438292741776
time_elpased: 5.723
batch start
#iterations: 346
currently lose_sum: 569.0642525851727
time_elpased: 5.669
batch start
#iterations: 347
currently lose_sum: 569.5516847670078
time_elpased: 5.866
batch start
#iterations: 348
currently lose_sum: 568.2842803001404
time_elpased: 5.656
batch start
#iterations: 349
currently lose_sum: 568.6874525547028
time_elpased: 5.848
batch start
#iterations: 350
currently lose_sum: 569.5898461043835
time_elpased: 6.108
batch start
#iterations: 351
currently lose_sum: 569.4704498052597
time_elpased: 5.527
batch start
#iterations: 352
currently lose_sum: 568.7028581500053
time_elpased: 5.901
batch start
#iterations: 353
currently lose_sum: 569.4004057049751
time_elpased: 5.51
batch start
#iterations: 354
currently lose_sum: 568.9460941255093
time_elpased: 5.743
batch start
#iterations: 355
currently lose_sum: 568.3719075322151
time_elpased: 5.655
batch start
#iterations: 356
currently lose_sum: 567.2185840904713
time_elpased: 5.635
batch start
#iterations: 357
currently lose_sum: 569.5344962477684
time_elpased: 5.688
batch start
#iterations: 358
currently lose_sum: 570.4085603952408
time_elpased: 5.752
batch start
#iterations: 359
currently lose_sum: 568.5735941231251
time_elpased: 5.673
start validation test
0.577955801105
0.570593830684
0.864258516003
0.687374667485
0
validation finish
batch start
#iterations: 360
currently lose_sum: 569.9303241372108
time_elpased: 5.763
batch start
#iterations: 361
currently lose_sum: 568.1526186466217
time_elpased: 5.702
batch start
#iterations: 362
currently lose_sum: 570.6024252176285
time_elpased: 5.878
batch start
#iterations: 363
currently lose_sum: 566.4037801921368
time_elpased: 5.841
batch start
#iterations: 364
currently lose_sum: 569.7618295550346
time_elpased: 5.842
batch start
#iterations: 365
currently lose_sum: 569.3143762648106
time_elpased: 5.717
batch start
#iterations: 366
currently lose_sum: 567.9049707353115
time_elpased: 5.739
batch start
#iterations: 367
currently lose_sum: 568.690063804388
time_elpased: 5.872
batch start
#iterations: 368
currently lose_sum: 570.2079682350159
time_elpased: 5.448
batch start
#iterations: 369
currently lose_sum: 569.8698379397392
time_elpased: 5.877
batch start
#iterations: 370
currently lose_sum: 568.9155416488647
time_elpased: 5.672
batch start
#iterations: 371
currently lose_sum: 568.8868688642979
time_elpased: 5.755
batch start
#iterations: 372
currently lose_sum: 570.6466906070709
time_elpased: 5.572
batch start
#iterations: 373
currently lose_sum: 570.9228964745998
time_elpased: 5.74
batch start
#iterations: 374
currently lose_sum: 570.5125907659531
time_elpased: 5.455
batch start
#iterations: 375
currently lose_sum: 568.2319472432137
time_elpased: 5.881
batch start
#iterations: 376
currently lose_sum: 568.0929888188839
time_elpased: 5.474
batch start
#iterations: 377
currently lose_sum: 571.4638653993607
time_elpased: 5.681
batch start
#iterations: 378
currently lose_sum: 568.5926951766014
time_elpased: 5.63
batch start
#iterations: 379
currently lose_sum: 568.2839193940163
time_elpased: 5.883
start validation test
0.578480662983
0.570856386409
0.86539055264
0.687923100522
0
validation finish
batch start
#iterations: 380
currently lose_sum: 567.7245962619781
time_elpased: 5.533
batch start
#iterations: 381
currently lose_sum: 568.7550910115242
time_elpased: 5.866
batch start
#iterations: 382
currently lose_sum: 570.0130330622196
time_elpased: 5.623
batch start
#iterations: 383
currently lose_sum: 570.6732987761497
time_elpased: 5.862
batch start
#iterations: 384
currently lose_sum: 572.1534033119678
time_elpased: 5.751
batch start
#iterations: 385
currently lose_sum: 567.8784175515175
time_elpased: 5.761
batch start
#iterations: 386
currently lose_sum: 568.128117710352
time_elpased: 5.61
batch start
#iterations: 387
currently lose_sum: 570.2344862520695
time_elpased: 5.72
batch start
#iterations: 388
currently lose_sum: 571.2375508546829
time_elpased: 5.624
batch start
#iterations: 389
currently lose_sum: 571.7085261642933
time_elpased: 5.719
batch start
#iterations: 390
currently lose_sum: 568.7513510882854
time_elpased: 5.914
batch start
#iterations: 391
currently lose_sum: 567.6458356976509
time_elpased: 5.95
batch start
#iterations: 392
currently lose_sum: 567.853875875473
time_elpased: 5.761
batch start
#iterations: 393
currently lose_sum: 568.6396704912186
time_elpased: 5.618
batch start
#iterations: 394
currently lose_sum: 570.4747103750706
time_elpased: 5.641
batch start
#iterations: 395
currently lose_sum: 567.8842747807503
time_elpased: 5.851
batch start
#iterations: 396
currently lose_sum: 569.9253407716751
time_elpased: 5.558
batch start
#iterations: 397
currently lose_sum: 570.2618188261986
time_elpased: 5.821
batch start
#iterations: 398
currently lose_sum: 570.8969967365265
time_elpased: 5.68
batch start
#iterations: 399
currently lose_sum: 568.858984619379
time_elpased: 5.863
start validation test
0.577845303867
0.570344266739
0.86611093959
0.687778367997
0
validation finish
acc: 0.574
pre: 0.567
rec: 0.874
F1: 0.688
auc: 0.000
