start to construct graph
graph construct over
92947
epochs start
batch start
#iterations: 0
currently lose_sum: 615.0665267109871
time_elpased: 5.591
batch start
#iterations: 1
currently lose_sum: 612.9181856513023
time_elpased: 5.555
batch start
#iterations: 2
currently lose_sum: 612.7106900215149
time_elpased: 5.38
batch start
#iterations: 3
currently lose_sum: 611.1344727873802
time_elpased: 5.642
batch start
#iterations: 4
currently lose_sum: 611.6144168376923
time_elpased: 5.859
batch start
#iterations: 5
currently lose_sum: 610.7134996056557
time_elpased: 5.703
batch start
#iterations: 6
currently lose_sum: 610.8328118920326
time_elpased: 5.561
batch start
#iterations: 7
currently lose_sum: 610.9695779085159
time_elpased: 5.453
batch start
#iterations: 8
currently lose_sum: 610.1119630336761
time_elpased: 5.707
batch start
#iterations: 9
currently lose_sum: 610.4223144650459
time_elpased: 5.487
batch start
#iterations: 10
currently lose_sum: 610.5851088762283
time_elpased: 6.21
batch start
#iterations: 11
currently lose_sum: 608.5640864372253
time_elpased: 6.034
batch start
#iterations: 12
currently lose_sum: 608.7551332712173
time_elpased: 5.601
batch start
#iterations: 13
currently lose_sum: 607.1556832790375
time_elpased: 5.654
batch start
#iterations: 14
currently lose_sum: 608.9757359027863
time_elpased: 5.75
batch start
#iterations: 15
currently lose_sum: 604.588842689991
time_elpased: 5.516
batch start
#iterations: 16
currently lose_sum: 604.23929053545
time_elpased: 5.698
batch start
#iterations: 17
currently lose_sum: 602.0023005604744
time_elpased: 5.521
batch start
#iterations: 18
currently lose_sum: 601.8216421604156
time_elpased: 5.669
batch start
#iterations: 19
currently lose_sum: 600.6688557267189
time_elpased: 5.534
start validation test
0.542797783934
0.541422712488
0.984974786457
0.698753399405
0
validation finish
batch start
#iterations: 20
currently lose_sum: 600.3324787020683
time_elpased: 5.191
batch start
#iterations: 21
currently lose_sum: 597.9065994024277
time_elpased: 5.747
batch start
#iterations: 22
currently lose_sum: 598.3450148701668
time_elpased: 5.625
batch start
#iterations: 23
currently lose_sum: 598.6437414884567
time_elpased: 5.694
batch start
#iterations: 24
currently lose_sum: 596.4032411575317
time_elpased: 5.719
batch start
#iterations: 25
currently lose_sum: 596.753581225872
time_elpased: 5.524
batch start
#iterations: 26
currently lose_sum: 596.7136940360069
time_elpased: 5.491
batch start
#iterations: 27
currently lose_sum: 594.1326285600662
time_elpased: 5.818
batch start
#iterations: 28
currently lose_sum: 594.1843992471695
time_elpased: 5.798
batch start
#iterations: 29
currently lose_sum: 594.9103040099144
time_elpased: 5.51
batch start
#iterations: 30
currently lose_sum: 593.4088546037674
time_elpased: 5.519
batch start
#iterations: 31
currently lose_sum: 592.0854179263115
time_elpased: 5.594
batch start
#iterations: 32
currently lose_sum: 591.0717421770096
time_elpased: 5.667
batch start
#iterations: 33
currently lose_sum: 590.4549498558044
time_elpased: 5.516
batch start
#iterations: 34
currently lose_sum: 591.1826570630074
time_elpased: 5.521
batch start
#iterations: 35
currently lose_sum: 590.5317051410675
time_elpased: 5.745
batch start
#iterations: 36
currently lose_sum: 590.6199926733971
time_elpased: 5.368
batch start
#iterations: 37
currently lose_sum: 590.790088891983
time_elpased: 5.804
batch start
#iterations: 38
currently lose_sum: 587.3106144070625
time_elpased: 5.345
batch start
#iterations: 39
currently lose_sum: 588.0244106650352
time_elpased: 5.846
start validation test
0.573157894737
0.566924944298
0.877225481116
0.688738511261
0
validation finish
batch start
#iterations: 40
currently lose_sum: 585.6166508197784
time_elpased: 5.884
batch start
#iterations: 41
currently lose_sum: 586.5682845115662
time_elpased: 5.574
batch start
#iterations: 42
currently lose_sum: 585.5672491788864
time_elpased: 5.926
batch start
#iterations: 43
currently lose_sum: 585.862497985363
time_elpased: 5.585
batch start
#iterations: 44
currently lose_sum: 585.0751900672913
time_elpased: 5.681
batch start
#iterations: 45
currently lose_sum: 583.3376398086548
time_elpased: 5.398
batch start
#iterations: 46
currently lose_sum: 586.9256938099861
time_elpased: 5.771
batch start
#iterations: 47
currently lose_sum: 584.3031429648399
time_elpased: 5.446
batch start
#iterations: 48
currently lose_sum: 586.7805368304253
time_elpased: 5.6
batch start
#iterations: 49
currently lose_sum: 584.3826835155487
time_elpased: 5.382
batch start
#iterations: 50
currently lose_sum: 584.1149955093861
time_elpased: 5.424
batch start
#iterations: 51
currently lose_sum: 584.3419733047485
time_elpased: 5.887
batch start
#iterations: 52
currently lose_sum: 584.6663883328438
time_elpased: 5.489
batch start
#iterations: 53
currently lose_sum: 581.7930588722229
time_elpased: 5.778
batch start
#iterations: 54
currently lose_sum: 583.656763792038
time_elpased: 5.861
batch start
#iterations: 55
currently lose_sum: 581.6815149188042
time_elpased: 5.448
batch start
#iterations: 56
currently lose_sum: 581.1283021569252
time_elpased: 5.654
batch start
#iterations: 57
currently lose_sum: 582.006097316742
time_elpased: 5.446
batch start
#iterations: 58
currently lose_sum: 580.6406878232956
time_elpased: 5.83
batch start
#iterations: 59
currently lose_sum: 582.9066665768623
time_elpased: 5.374
start validation test
0.571966759003
0.563990486598
0.902953586498
0.694310358471
0
validation finish
batch start
#iterations: 60
currently lose_sum: 581.7849947214127
time_elpased: 5.504
batch start
#iterations: 61
currently lose_sum: 583.1434743404388
time_elpased: 5.696
batch start
#iterations: 62
currently lose_sum: 582.2410854101181
time_elpased: 5.643
batch start
#iterations: 63
currently lose_sum: 581.1171886324883
time_elpased: 5.5
batch start
#iterations: 64
currently lose_sum: 581.6002808213234
time_elpased: 5.625
batch start
#iterations: 65
currently lose_sum: 579.3313522934914
time_elpased: 5.647
batch start
#iterations: 66
currently lose_sum: 579.7219217419624
time_elpased: 5.661
batch start
#iterations: 67
currently lose_sum: 580.454322874546
time_elpased: 5.48
batch start
#iterations: 68
currently lose_sum: 578.5749129652977
time_elpased: 5.605
batch start
#iterations: 69
currently lose_sum: 577.4155645370483
time_elpased: 5.559
batch start
#iterations: 70
currently lose_sum: 579.0496905446053
time_elpased: 5.603
batch start
#iterations: 71
currently lose_sum: 581.4071459174156
time_elpased: 5.63
batch start
#iterations: 72
currently lose_sum: 578.3482624292374
time_elpased: 5.316
batch start
#iterations: 73
currently lose_sum: 577.2819222211838
time_elpased: 5.63
batch start
#iterations: 74
currently lose_sum: 579.097287863493
time_elpased: 5.413
batch start
#iterations: 75
currently lose_sum: 579.3738375902176
time_elpased: 5.564
batch start
#iterations: 76
currently lose_sum: 578.7607228159904
time_elpased: 5.648
batch start
#iterations: 77
currently lose_sum: 578.5822573900223
time_elpased: 5.612
batch start
#iterations: 78
currently lose_sum: 576.0377203822136
time_elpased: 5.683
batch start
#iterations: 79
currently lose_sum: 580.9085156321526
time_elpased: 5.496
start validation test
0.577451523546
0.568795260039
0.889163322013
0.693780864817
0
validation finish
batch start
#iterations: 80
currently lose_sum: 578.1808075904846
time_elpased: 5.655
batch start
#iterations: 81
currently lose_sum: 577.7043958306313
time_elpased: 5.571
batch start
#iterations: 82
currently lose_sum: 576.9889344274998
time_elpased: 5.683
batch start
#iterations: 83
currently lose_sum: 578.7619414925575
time_elpased: 5.546
batch start
#iterations: 84
currently lose_sum: 575.6875959634781
time_elpased: 5.621
batch start
#iterations: 85
currently lose_sum: 579.1091784238815
time_elpased: 5.679
batch start
#iterations: 86
currently lose_sum: 577.2691700458527
time_elpased: 5.568
batch start
#iterations: 87
currently lose_sum: 578.1916178762913
time_elpased: 5.439
batch start
#iterations: 88
currently lose_sum: 576.6317818164825
time_elpased: 5.737
batch start
#iterations: 89
currently lose_sum: 575.7005739808083
time_elpased: 5.496
batch start
#iterations: 90
currently lose_sum: 577.87363666296
time_elpased: 5.866
batch start
#iterations: 91
currently lose_sum: 574.2198615074158
time_elpased: 5.592
batch start
#iterations: 92
currently lose_sum: 578.1460701823235
time_elpased: 5.507
batch start
#iterations: 93
currently lose_sum: 576.8380752205849
time_elpased: 5.553
batch start
#iterations: 94
currently lose_sum: 575.1868404746056
time_elpased: 5.604
batch start
#iterations: 95
currently lose_sum: 574.47716152668
time_elpased: 5.553
batch start
#iterations: 96
currently lose_sum: 575.6168009638786
time_elpased: 5.552
batch start
#iterations: 97
currently lose_sum: 576.5828975737095
time_elpased: 5.647
batch start
#iterations: 98
currently lose_sum: 575.3476857244968
time_elpased: 5.635
batch start
#iterations: 99
currently lose_sum: 576.074329584837
time_elpased: 5.701
start validation test
0.584736842105
0.576590243062
0.860553668828
0.690517971056
0
validation finish
batch start
#iterations: 100
currently lose_sum: 573.1900852024555
time_elpased: 5.73
batch start
#iterations: 101
currently lose_sum: 575.1668236553669
time_elpased: 5.411
batch start
#iterations: 102
currently lose_sum: 572.65309420228
time_elpased: 5.792
batch start
#iterations: 103
currently lose_sum: 575.1034207642078
time_elpased: 5.377
batch start
#iterations: 104
currently lose_sum: 574.537700355053
time_elpased: 5.896
batch start
#iterations: 105
currently lose_sum: 574.4875565171242
time_elpased: 5.746
batch start
#iterations: 106
currently lose_sum: 574.7563021481037
time_elpased: 5.683
batch start
#iterations: 107
currently lose_sum: 573.6801313459873
time_elpased: 5.677
batch start
#iterations: 108
currently lose_sum: 573.8146351575851
time_elpased: 5.539
batch start
#iterations: 109
currently lose_sum: 572.4310345053673
time_elpased: 5.435
batch start
#iterations: 110
currently lose_sum: 573.5963782072067
time_elpased: 5.708
batch start
#iterations: 111
currently lose_sum: 575.1509707570076
time_elpased: 5.441
batch start
#iterations: 112
currently lose_sum: 574.4932042956352
time_elpased: 5.838
batch start
#iterations: 113
currently lose_sum: 574.4712781608105
time_elpased: 5.35
batch start
#iterations: 114
currently lose_sum: 574.6667764782906
time_elpased: 5.812
batch start
#iterations: 115
currently lose_sum: 574.4570052623749
time_elpased: 5.487
batch start
#iterations: 116
currently lose_sum: 572.997133076191
time_elpased: 5.759
batch start
#iterations: 117
currently lose_sum: 573.7601507008076
time_elpased: 5.556
batch start
#iterations: 118
currently lose_sum: 570.808171749115
time_elpased: 5.641
batch start
#iterations: 119
currently lose_sum: 571.944987475872
time_elpased: 5.534
start validation test
0.581191135734
0.572058381484
0.881341977977
0.693792405063
0
validation finish
batch start
#iterations: 120
currently lose_sum: 572.1113058924675
time_elpased: 5.712
batch start
#iterations: 121
currently lose_sum: 572.2483159303665
time_elpased: 5.502
batch start
#iterations: 122
currently lose_sum: 573.2349046468735
time_elpased: 5.806
batch start
#iterations: 123
currently lose_sum: 572.2095036506653
time_elpased: 5.505
batch start
#iterations: 124
currently lose_sum: 570.0041440427303
time_elpased: 5.676
batch start
#iterations: 125
currently lose_sum: 572.9820446968079
time_elpased: 5.391
batch start
#iterations: 126
currently lose_sum: 569.5012845396996
time_elpased: 5.759
batch start
#iterations: 127
currently lose_sum: 570.9012563228607
time_elpased: 5.482
batch start
#iterations: 128
currently lose_sum: 571.5544808804989
time_elpased: 5.713
batch start
#iterations: 129
currently lose_sum: 570.885280251503
time_elpased: 5.424
batch start
#iterations: 130
currently lose_sum: 571.1644713878632
time_elpased: 5.886
batch start
#iterations: 131
currently lose_sum: 570.2982181310654
time_elpased: 5.717
batch start
#iterations: 132
currently lose_sum: 573.5994856357574
time_elpased: 5.644
batch start
#iterations: 133
currently lose_sum: 571.1269647479057
time_elpased: 5.56
batch start
#iterations: 134
currently lose_sum: 569.7737930417061
time_elpased: 5.74
batch start
#iterations: 135
currently lose_sum: 569.7672333419323
time_elpased: 5.657
batch start
#iterations: 136
currently lose_sum: 570.2524617910385
time_elpased: 5.712
batch start
#iterations: 137
currently lose_sum: 572.8546253144741
time_elpased: 5.488
batch start
#iterations: 138
currently lose_sum: 571.7544249296188
time_elpased: 5.611
batch start
#iterations: 139
currently lose_sum: 570.6716935038567
time_elpased: 6.007
start validation test
0.587617728532
0.580435167168
0.844190593805
0.687897020902
0
validation finish
batch start
#iterations: 140
currently lose_sum: 574.0176097750664
time_elpased: 5.669
batch start
#iterations: 141
currently lose_sum: 569.5091150403023
time_elpased: 5.894
batch start
#iterations: 142
currently lose_sum: 569.3210463225842
time_elpased: 5.953
batch start
#iterations: 143
currently lose_sum: 570.1263496279716
time_elpased: 5.743
batch start
#iterations: 144
currently lose_sum: 568.7813999652863
time_elpased: 5.68
batch start
#iterations: 145
currently lose_sum: 570.213353484869
time_elpased: 5.304
batch start
#iterations: 146
currently lose_sum: 569.6839754581451
time_elpased: 5.811
batch start
#iterations: 147
currently lose_sum: 568.0989598929882
time_elpased: 5.38
batch start
#iterations: 148
currently lose_sum: 572.2880417108536
time_elpased: 5.752
batch start
#iterations: 149
currently lose_sum: 569.4982641935349
time_elpased: 5.591
batch start
#iterations: 150
currently lose_sum: 569.8341872096062
time_elpased: 5.621
batch start
#iterations: 151
currently lose_sum: 570.7944315075874
time_elpased: 5.466
batch start
#iterations: 152
currently lose_sum: 570.2363032698631
time_elpased: 5.695
batch start
#iterations: 153
currently lose_sum: 569.4368292093277
time_elpased: 5.642
batch start
#iterations: 154
currently lose_sum: 568.5583546757698
time_elpased: 5.366
batch start
#iterations: 155
currently lose_sum: 570.2741820216179
time_elpased: 5.501
batch start
#iterations: 156
currently lose_sum: 568.3994087576866
time_elpased: 5.787
batch start
#iterations: 157
currently lose_sum: 568.5912859141827
time_elpased: 5.695
batch start
#iterations: 158
currently lose_sum: 568.8130723834038
time_elpased: 5.738
batch start
#iterations: 159
currently lose_sum: 568.8179357647896
time_elpased: 5.441
start validation test
0.585152354571
0.576351305063
0.865802202326
0.69202928354
0
validation finish
batch start
#iterations: 160
currently lose_sum: 567.3733390271664
time_elpased: 5.67
batch start
#iterations: 161
currently lose_sum: 569.0919154286385
time_elpased: 5.446
batch start
#iterations: 162
currently lose_sum: 566.2863144278526
time_elpased: 5.736
batch start
#iterations: 163
currently lose_sum: 568.9082382917404
time_elpased: 5.327
batch start
#iterations: 164
currently lose_sum: 568.6066297888756
time_elpased: 5.615
batch start
#iterations: 165
currently lose_sum: 570.7206251621246
time_elpased: 5.672
batch start
#iterations: 166
currently lose_sum: 569.3046951293945
time_elpased: 5.627
batch start
#iterations: 167
currently lose_sum: 568.0596122145653
time_elpased: 5.59
batch start
#iterations: 168
currently lose_sum: 567.9789940714836
time_elpased: 5.309
batch start
#iterations: 169
currently lose_sum: 566.7081118822098
time_elpased: 5.683
batch start
#iterations: 170
currently lose_sum: 567.6738109588623
time_elpased: 5.396
batch start
#iterations: 171
currently lose_sum: 569.1366549730301
time_elpased: 5.514
batch start
#iterations: 172
currently lose_sum: 568.4329329431057
time_elpased: 5.766
batch start
#iterations: 173
currently lose_sum: 569.6006114780903
time_elpased: 5.799
batch start
#iterations: 174
currently lose_sum: 568.2973247766495
time_elpased: 5.514
batch start
#iterations: 175
currently lose_sum: 568.9701075851917
time_elpased: 5.512
batch start
#iterations: 176
currently lose_sum: 568.4815064072609
time_elpased: 5.706
batch start
#iterations: 177
currently lose_sum: 567.7203674912453
time_elpased: 5.481
batch start
#iterations: 178
currently lose_sum: 570.8599454164505
time_elpased: 5.482
batch start
#iterations: 179
currently lose_sum: 568.4582259953022
time_elpased: 5.436
start validation test
0.586260387812
0.577503618444
0.862303179994
0.691736151242
0
validation finish
batch start
#iterations: 180
currently lose_sum: 568.0927385091782
time_elpased: 5.794
batch start
#iterations: 181
currently lose_sum: 568.1748425364494
time_elpased: 5.515
batch start
#iterations: 182
currently lose_sum: 566.7461922168732
time_elpased: 5.637
batch start
#iterations: 183
currently lose_sum: 568.1457318365574
time_elpased: 5.494
batch start
#iterations: 184
currently lose_sum: 565.6915526986122
time_elpased: 5.7
batch start
#iterations: 185
currently lose_sum: 566.7639310956001
time_elpased: 5.544
batch start
#iterations: 186
currently lose_sum: 567.0652861297131
time_elpased: 5.819
batch start
#iterations: 187
currently lose_sum: 567.5461726784706
time_elpased: 5.482
batch start
#iterations: 188
currently lose_sum: 563.9802676439285
time_elpased: 5.676
batch start
#iterations: 189
currently lose_sum: 568.2966664433479
time_elpased: 5.559
batch start
#iterations: 190
currently lose_sum: 566.7199037075043
time_elpased: 5.829
batch start
#iterations: 191
currently lose_sum: 566.3731641769409
time_elpased: 5.491
batch start
#iterations: 192
currently lose_sum: 567.6868795156479
time_elpased: 5.778
batch start
#iterations: 193
currently lose_sum: 565.9849776029587
time_elpased: 5.578
batch start
#iterations: 194
currently lose_sum: 569.2997705340385
time_elpased: 5.703
batch start
#iterations: 195
currently lose_sum: 566.5832275748253
time_elpased: 5.509
batch start
#iterations: 196
currently lose_sum: 567.3368021249771
time_elpased: 5.722
batch start
#iterations: 197
currently lose_sum: 567.1580595374107
time_elpased: 5.506
batch start
#iterations: 198
currently lose_sum: 566.4576094150543
time_elpased: 5.789
batch start
#iterations: 199
currently lose_sum: 565.1185638010502
time_elpased: 5.816
start validation test
0.585180055402
0.575975464304
0.869712874344
0.693003136596
0
validation finish
batch start
#iterations: 200
currently lose_sum: 568.291336864233
time_elpased: 5.451
batch start
#iterations: 201
currently lose_sum: 564.7757169008255
time_elpased: 5.64
batch start
#iterations: 202
currently lose_sum: 566.7187221348286
time_elpased: 5.928
batch start
#iterations: 203
currently lose_sum: 568.7445198893547
time_elpased: 5.676
batch start
#iterations: 204
currently lose_sum: 565.3460408747196
time_elpased: 5.585
batch start
#iterations: 205
currently lose_sum: 566.3994785547256
time_elpased: 5.682
batch start
#iterations: 206
currently lose_sum: 566.9803135693073
time_elpased: 5.563
batch start
#iterations: 207
currently lose_sum: 566.0210408568382
time_elpased: 5.628
batch start
#iterations: 208
currently lose_sum: 565.078645825386
time_elpased: 5.622
batch start
#iterations: 209
currently lose_sum: 566.3136102557182
time_elpased: 5.145
batch start
#iterations: 210
currently lose_sum: 565.8278658688068
time_elpased: 5.651
batch start
#iterations: 211
currently lose_sum: 568.1644480228424
time_elpased: 5.645
batch start
#iterations: 212
currently lose_sum: 566.5524482727051
time_elpased: 5.884
batch start
#iterations: 213
currently lose_sum: 567.5834531784058
time_elpased: 5.708
batch start
#iterations: 214
currently lose_sum: 566.7950026094913
time_elpased: 5.634
batch start
#iterations: 215
currently lose_sum: 564.5472408533096
time_elpased: 5.615
batch start
#iterations: 216
currently lose_sum: 564.9784392118454
time_elpased: 5.766
batch start
#iterations: 217
currently lose_sum: 566.165983080864
time_elpased: 5.805
batch start
#iterations: 218
currently lose_sum: 565.4651208817959
time_elpased: 5.439
batch start
#iterations: 219
currently lose_sum: 565.7055574655533
time_elpased: 6.011
start validation test
0.586869806094
0.577705955164
0.864567253267
0.692608928645
0
validation finish
batch start
#iterations: 220
currently lose_sum: 565.2913810610771
time_elpased: 5.596
batch start
#iterations: 221
currently lose_sum: 563.8440800905228
time_elpased: 5.519
batch start
#iterations: 222
currently lose_sum: 566.6430652141571
time_elpased: 5.463
batch start
#iterations: 223
currently lose_sum: 564.6500839591026
time_elpased: 5.681
batch start
#iterations: 224
currently lose_sum: 567.3564530611038
time_elpased: 5.609
batch start
#iterations: 225
currently lose_sum: 566.0744307041168
time_elpased: 5.811
batch start
#iterations: 226
currently lose_sum: 564.3227357268333
time_elpased: 5.581
batch start
#iterations: 227
currently lose_sum: 566.3888592720032
time_elpased: 5.601
batch start
#iterations: 228
currently lose_sum: 566.9744166731834
time_elpased: 5.723
batch start
#iterations: 229
currently lose_sum: 565.9723569154739
time_elpased: 5.488
batch start
#iterations: 230
currently lose_sum: 565.8951129317284
time_elpased: 5.474
batch start
#iterations: 231
currently lose_sum: 565.0643248558044
time_elpased: 5.705
batch start
#iterations: 232
currently lose_sum: 566.5621676146984
time_elpased: 5.519
batch start
#iterations: 233
currently lose_sum: 564.857800245285
time_elpased: 5.594
batch start
#iterations: 234
currently lose_sum: 563.9023593366146
time_elpased: 5.701
batch start
#iterations: 235
currently lose_sum: 565.2770144343376
time_elpased: 5.665
batch start
#iterations: 236
currently lose_sum: 564.8373765349388
time_elpased: 5.377
batch start
#iterations: 237
currently lose_sum: 564.3682625293732
time_elpased: 5.757
batch start
#iterations: 238
currently lose_sum: 566.0891861617565
time_elpased: 5.738
batch start
#iterations: 239
currently lose_sum: 563.0154175758362
time_elpased: 5.754
start validation test
0.58728531856
0.578609811059
0.858804157662
0.691397916278
0
validation finish
batch start
#iterations: 240
currently lose_sum: 564.7616790831089
time_elpased: 5.573
batch start
#iterations: 241
currently lose_sum: 564.6129802763462
time_elpased: 5.704
batch start
#iterations: 242
currently lose_sum: 565.3000943958759
time_elpased: 5.848
batch start
#iterations: 243
currently lose_sum: 564.5454278886318
time_elpased: 5.397
batch start
#iterations: 244
currently lose_sum: 565.5267805457115
time_elpased: 5.456
batch start
#iterations: 245
currently lose_sum: 564.4254644215107
time_elpased: 5.539
batch start
#iterations: 246
currently lose_sum: 564.5133486688137
time_elpased: 5.535
batch start
#iterations: 247
currently lose_sum: 565.20741969347
time_elpased: 5.608
batch start
#iterations: 248
currently lose_sum: 564.5916949510574
time_elpased: 5.398
batch start
#iterations: 249
currently lose_sum: 565.6807028055191
time_elpased: 5.759
batch start
#iterations: 250
currently lose_sum: 562.5827538669109
time_elpased: 5.498
batch start
#iterations: 251
currently lose_sum: 563.1339146494865
time_elpased: 5.754
batch start
#iterations: 252
currently lose_sum: 565.2146895825863
time_elpased: 5.477
batch start
#iterations: 253
currently lose_sum: 564.4590435028076
time_elpased: 5.707
batch start
#iterations: 254
currently lose_sum: 563.0890080332756
time_elpased: 5.493
batch start
#iterations: 255
currently lose_sum: 564.9849799275398
time_elpased: 5.636
batch start
#iterations: 256
currently lose_sum: 564.1732130646706
time_elpased: 5.501
batch start
#iterations: 257
currently lose_sum: 565.3619569540024
time_elpased: 5.752
batch start
#iterations: 258
currently lose_sum: 562.9094603955746
time_elpased: 5.468
batch start
#iterations: 259
currently lose_sum: 563.2819780707359
time_elpased: 5.796
start validation test
0.585484764543
0.57649394209
0.86672841412
0.69242785497
0
validation finish
batch start
#iterations: 260
currently lose_sum: 562.9815911352634
time_elpased: 5.577
batch start
#iterations: 261
currently lose_sum: 566.4521127343178
time_elpased: 5.803
batch start
#iterations: 262
currently lose_sum: 563.9639246463776
time_elpased: 5.471
batch start
#iterations: 263
currently lose_sum: 561.7109492719173
time_elpased: 5.768
batch start
#iterations: 264
currently lose_sum: 565.0456159412861
time_elpased: 5.425
batch start
#iterations: 265
currently lose_sum: 564.6096985936165
time_elpased: 5.51
batch start
#iterations: 266
currently lose_sum: 564.6068452000618
time_elpased: 5.701
batch start
#iterations: 267
currently lose_sum: 565.0092187821865
time_elpased: 5.647
batch start
#iterations: 268
currently lose_sum: 563.2114171981812
time_elpased: 5.401
batch start
#iterations: 269
currently lose_sum: 563.4330264925957
time_elpased: 5.431
batch start
#iterations: 270
currently lose_sum: 562.5833454728127
time_elpased: 5.851
batch start
#iterations: 271
currently lose_sum: 564.7671430408955
time_elpased: 5.546
batch start
#iterations: 272
currently lose_sum: 563.4596958756447
time_elpased: 5.721
batch start
#iterations: 273
currently lose_sum: 562.8443112671375
time_elpased: 5.412
batch start
#iterations: 274
currently lose_sum: 563.1482489705086
time_elpased: 5.735
batch start
#iterations: 275
currently lose_sum: 563.9979230463505
time_elpased: 5.518
batch start
#iterations: 276
currently lose_sum: 565.4786345660686
time_elpased: 5.733
batch start
#iterations: 277
currently lose_sum: 563.4368261098862
time_elpased: 5.503
batch start
#iterations: 278
currently lose_sum: 564.1015078425407
time_elpased: 5.762
batch start
#iterations: 279
currently lose_sum: 564.2137078344822
time_elpased: 5.62
start validation test
0.586869806094
0.578363384189
0.858289595554
0.691055226416
0
validation finish
batch start
#iterations: 280
currently lose_sum: 564.0619485974312
time_elpased: 5.721
batch start
#iterations: 281
currently lose_sum: 564.8795014619827
time_elpased: 5.757
batch start
#iterations: 282
currently lose_sum: 562.575182646513
time_elpased: 5.543
batch start
#iterations: 283
currently lose_sum: 564.2408069074154
time_elpased: 5.535
batch start
#iterations: 284
currently lose_sum: 563.3118341565132
time_elpased: 5.608
batch start
#iterations: 285
currently lose_sum: 562.5322287678719
time_elpased: 5.55
batch start
#iterations: 286
currently lose_sum: 562.3775189518929
time_elpased: 5.61
batch start
#iterations: 287
currently lose_sum: 563.2652323246002
time_elpased: 5.57
batch start
#iterations: 288
currently lose_sum: 564.7816985547543
time_elpased: 5.706
batch start
#iterations: 289
currently lose_sum: 562.5985375642776
time_elpased: 5.53
batch start
#iterations: 290
currently lose_sum: 562.3644502460957
time_elpased: 5.679
batch start
#iterations: 291
currently lose_sum: 563.4490856826305
time_elpased: 5.412
batch start
#iterations: 292
currently lose_sum: 563.6504465341568
time_elpased: 5.658
batch start
#iterations: 293
currently lose_sum: 563.9972484111786
time_elpased: 5.545
batch start
#iterations: 294
currently lose_sum: 561.7918175160885
time_elpased: 5.786
batch start
#iterations: 295
currently lose_sum: 565.3143525123596
time_elpased: 5.622
batch start
#iterations: 296
currently lose_sum: 565.6501053273678
time_elpased: 5.756
batch start
#iterations: 297
currently lose_sum: 562.7964671850204
time_elpased: 5.301
batch start
#iterations: 298
currently lose_sum: 564.7305713891983
time_elpased: 5.477
batch start
#iterations: 299
currently lose_sum: 562.7777444720268
time_elpased: 5.527
start validation test
0.585263157895
0.576210972194
0.867963363178
0.692617229203
0
validation finish
batch start
#iterations: 300
currently lose_sum: 563.0253405869007
time_elpased: 5.545
batch start
#iterations: 301
currently lose_sum: 562.8504710793495
time_elpased: 5.686
batch start
#iterations: 302
currently lose_sum: 562.9818688631058
time_elpased: 5.547
batch start
#iterations: 303
currently lose_sum: 562.2408500015736
time_elpased: 5.486
batch start
#iterations: 304
currently lose_sum: 562.9166132211685
time_elpased: 5.445
batch start
#iterations: 305
currently lose_sum: 562.1317956745625
time_elpased: 5.57
batch start
#iterations: 306
currently lose_sum: 562.1089937984943
time_elpased: 5.581
batch start
#iterations: 307
currently lose_sum: 563.8921736478806
time_elpased: 5.52
batch start
#iterations: 308
currently lose_sum: 562.3353719115257
time_elpased: 5.726
batch start
#iterations: 309
currently lose_sum: 564.9260312020779
time_elpased: 5.475
batch start
#iterations: 310
currently lose_sum: 562.6088925302029
time_elpased: 5.81
batch start
#iterations: 311
currently lose_sum: 561.4339237809181
time_elpased: 5.723
batch start
#iterations: 312
currently lose_sum: 561.394510447979
time_elpased: 5.503
batch start
#iterations: 313
currently lose_sum: 562.0150141119957
time_elpased: 5.657
batch start
#iterations: 314
currently lose_sum: 563.7487059831619
time_elpased: 5.654
batch start
#iterations: 315
currently lose_sum: 561.6976958215237
time_elpased: 5.673
batch start
#iterations: 316
currently lose_sum: 561.6472201645374
time_elpased: 5.488
batch start
#iterations: 317
currently lose_sum: 560.5966810286045
time_elpased: 5.723
batch start
#iterations: 318
currently lose_sum: 562.2205181121826
time_elpased: 5.584
batch start
#iterations: 319
currently lose_sum: 561.9283228814602
time_elpased: 5.367
start validation test
0.587396121884
0.578180440249
0.863641041474
0.692652126364
0
validation finish
batch start
#iterations: 320
currently lose_sum: 563.8160924911499
time_elpased: 5.689
batch start
#iterations: 321
currently lose_sum: 562.5422389805317
time_elpased: 5.401
batch start
#iterations: 322
currently lose_sum: 562.1428824067116
time_elpased: 5.908
batch start
#iterations: 323
currently lose_sum: 562.4762644171715
time_elpased: 5.596
batch start
#iterations: 324
currently lose_sum: 564.0686225295067
time_elpased: 5.873
batch start
#iterations: 325
currently lose_sum: 564.662779211998
time_elpased: 5.597
batch start
#iterations: 326
currently lose_sum: 561.8576816320419
time_elpased: 5.49
batch start
#iterations: 327
currently lose_sum: 564.040924847126
time_elpased: 5.689
batch start
#iterations: 328
currently lose_sum: 562.7414584159851
time_elpased: 5.696
batch start
#iterations: 329
currently lose_sum: 562.7143962681293
time_elpased: 5.639
batch start
#iterations: 330
currently lose_sum: 561.2828089296818
time_elpased: 5.678
batch start
#iterations: 331
currently lose_sum: 562.3217712044716
time_elpased: 5.854
batch start
#iterations: 332
currently lose_sum: 564.5350252091885
time_elpased: 5.439
batch start
#iterations: 333
currently lose_sum: 562.9919149577618
time_elpased: 5.784
batch start
#iterations: 334
currently lose_sum: 563.2295253276825
time_elpased: 5.357
batch start
#iterations: 335
currently lose_sum: 561.6900236904621
time_elpased: 5.767
batch start
#iterations: 336
currently lose_sum: 562.3046472072601
time_elpased: 5.563
batch start
#iterations: 337
currently lose_sum: 561.8133150339127
time_elpased: 5.688
batch start
#iterations: 338
currently lose_sum: 564.0994898378849
time_elpased: 5.383
batch start
#iterations: 339
currently lose_sum: 563.5859167873859
time_elpased: 5.797
start validation test
0.586288088643
0.577395492861
0.863538129052
0.692055506299
0
validation finish
batch start
#iterations: 340
currently lose_sum: 562.232848584652
time_elpased: 5.613
batch start
#iterations: 341
currently lose_sum: 562.5500426888466
time_elpased: 5.874
batch start
#iterations: 342
currently lose_sum: 564.5368384718895
time_elpased: 5.653
batch start
#iterations: 343
currently lose_sum: 563.5451458096504
time_elpased: 5.657
batch start
#iterations: 344
currently lose_sum: 562.5027748942375
time_elpased: 5.54
batch start
#iterations: 345
currently lose_sum: 562.3984120488167
time_elpased: 5.634
batch start
#iterations: 346
currently lose_sum: 561.9599768817425
time_elpased: 5.449
batch start
#iterations: 347
currently lose_sum: 563.4922026395798
time_elpased: 5.777
batch start
#iterations: 348
currently lose_sum: 563.1068453192711
time_elpased: 5.569
batch start
#iterations: 349
currently lose_sum: 562.3140147328377
time_elpased: 5.907
batch start
#iterations: 350
currently lose_sum: 563.0960666239262
time_elpased: 5.676
batch start
#iterations: 351
currently lose_sum: 562.93145814538
time_elpased: 5.582
batch start
#iterations: 352
currently lose_sum: 563.3235637247562
time_elpased: 5.423
batch start
#iterations: 353
currently lose_sum: 562.8392540812492
time_elpased: 5.699
batch start
#iterations: 354
currently lose_sum: 562.2850791811943
time_elpased: 5.619
batch start
#iterations: 355
currently lose_sum: 562.7678261995316
time_elpased: 5.736
batch start
#iterations: 356
currently lose_sum: 562.1936162114143
time_elpased: 5.629
batch start
#iterations: 357
currently lose_sum: 564.3673445284367
time_elpased: 5.781
batch start
#iterations: 358
currently lose_sum: 561.2564965188503
time_elpased: 5.819
batch start
#iterations: 359
currently lose_sum: 564.0796132683754
time_elpased: 5.59
start validation test
0.586842105263
0.578029492005
0.861274055779
0.691781529623
0
validation finish
batch start
#iterations: 360
currently lose_sum: 561.7415291368961
time_elpased: 5.469
batch start
#iterations: 361
currently lose_sum: 562.0452008545399
time_elpased: 5.455
batch start
#iterations: 362
currently lose_sum: 562.3580785095692
time_elpased: 5.785
batch start
#iterations: 363
currently lose_sum: 562.4959211945534
time_elpased: 5.392
batch start
#iterations: 364
currently lose_sum: 561.4003005027771
time_elpased: 5.78
batch start
#iterations: 365
currently lose_sum: 562.6306652724743
time_elpased: 5.587
batch start
#iterations: 366
currently lose_sum: 563.0145542025566
time_elpased: 5.776
batch start
#iterations: 367
currently lose_sum: 561.7141493856907
time_elpased: 5.431
batch start
#iterations: 368
currently lose_sum: 561.2931396663189
time_elpased: 5.729
batch start
#iterations: 369
currently lose_sum: 561.5118730664253
time_elpased: 5.528
batch start
#iterations: 370
currently lose_sum: 562.8912935256958
time_elpased: 5.695
batch start
#iterations: 371
currently lose_sum: 561.683836042881
time_elpased: 5.34
batch start
#iterations: 372
currently lose_sum: 561.6928308010101
time_elpased: 5.598
batch start
#iterations: 373
currently lose_sum: 561.2237082123756
time_elpased: 5.567
batch start
#iterations: 374
currently lose_sum: 562.3716534376144
time_elpased: 5.358
batch start
#iterations: 375
currently lose_sum: 562.9510124921799
time_elpased: 5.636
batch start
#iterations: 376
currently lose_sum: 560.6398597955704
time_elpased: 5.713
batch start
#iterations: 377
currently lose_sum: 562.7117576897144
time_elpased: 5.919
batch start
#iterations: 378
currently lose_sum: 560.9269884824753
time_elpased: 5.585
batch start
#iterations: 379
currently lose_sum: 563.2536776959896
time_elpased: 5.624
start validation test
0.586260387812
0.577205629935
0.865184727797
0.692447080142
0
validation finish
batch start
#iterations: 380
currently lose_sum: 560.4823316931725
time_elpased: 5.904
batch start
#iterations: 381
currently lose_sum: 560.264516979456
time_elpased: 5.483
batch start
#iterations: 382
currently lose_sum: 560.9443946480751
time_elpased: 5.889
batch start
#iterations: 383
currently lose_sum: 563.5094077587128
time_elpased: 5.647
batch start
#iterations: 384
currently lose_sum: 561.1585737764835
time_elpased: 5.666
batch start
#iterations: 385
currently lose_sum: 561.1622443795204
time_elpased: 5.546
batch start
#iterations: 386
currently lose_sum: 561.3032160401344
time_elpased: 5.685
batch start
#iterations: 387
currently lose_sum: 562.8980686068535
time_elpased: 5.679
batch start
#iterations: 388
currently lose_sum: 562.6822099089622
time_elpased: 5.537
batch start
#iterations: 389
currently lose_sum: 561.9142401814461
time_elpased: 5.711
batch start
#iterations: 390
currently lose_sum: 562.6902112960815
time_elpased: 5.431
batch start
#iterations: 391
currently lose_sum: 561.5483168959618
time_elpased: 5.825
batch start
#iterations: 392
currently lose_sum: 566.365208119154
time_elpased: 5.752
batch start
#iterations: 393
currently lose_sum: 561.7292620837688
time_elpased: 5.623
batch start
#iterations: 394
currently lose_sum: 563.6199257075787
time_elpased: 5.663
batch start
#iterations: 395
currently lose_sum: 562.0820024013519
time_elpased: 5.72
batch start
#iterations: 396
currently lose_sum: 562.0033213496208
time_elpased: 5.556
batch start
#iterations: 397
currently lose_sum: 563.094068467617
time_elpased: 5.773
batch start
#iterations: 398
currently lose_sum: 561.1031789183617
time_elpased: 5.425
batch start
#iterations: 399
currently lose_sum: 563.1961581408978
time_elpased: 5.594
start validation test
0.586066481994
0.577524598654
0.860759493671
0.691253951528
0
validation finish
acc: 0.540
pre: 0.539
rec: 0.982
F1: 0.696
auc: 0.000
