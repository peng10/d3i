start to construct graph
graph construct over
93159
epochs start
batch start
#iterations: 0
currently lose_sum: 615.5971058607101
time_elpased: 1.89
batch start
#iterations: 1
currently lose_sum: 607.9577287435532
time_elpased: 1.859
batch start
#iterations: 2
currently lose_sum: 607.7070562839508
time_elpased: 1.855
batch start
#iterations: 3
currently lose_sum: 605.278815984726
time_elpased: 1.86
batch start
#iterations: 4
currently lose_sum: 603.2448515892029
time_elpased: 1.854
batch start
#iterations: 5
currently lose_sum: 603.2726627588272
time_elpased: 1.874
batch start
#iterations: 6
currently lose_sum: 603.3590579032898
time_elpased: 1.861
batch start
#iterations: 7
currently lose_sum: 603.0889140367508
time_elpased: 1.858
batch start
#iterations: 8
currently lose_sum: 604.1122943162918
time_elpased: 1.903
batch start
#iterations: 9
currently lose_sum: 601.1297668218613
time_elpased: 1.93
batch start
#iterations: 10
currently lose_sum: 600.8104417920113
time_elpased: 1.822
batch start
#iterations: 11
currently lose_sum: 602.6948580741882
time_elpased: 1.815
batch start
#iterations: 12
currently lose_sum: 600.4676649570465
time_elpased: 1.82
batch start
#iterations: 13
currently lose_sum: 601.1135657429695
time_elpased: 1.833
batch start
#iterations: 14
currently lose_sum: 601.70253688097
time_elpased: 1.839
batch start
#iterations: 15
currently lose_sum: 599.9321593642235
time_elpased: 1.825
batch start
#iterations: 16
currently lose_sum: 603.3150691986084
time_elpased: 1.839
batch start
#iterations: 17
currently lose_sum: 600.2358918190002
time_elpased: 1.834
batch start
#iterations: 18
currently lose_sum: 599.8702853918076
time_elpased: 1.872
batch start
#iterations: 19
currently lose_sum: 600.4484861493111
time_elpased: 1.829
start validation test
0.557513812155
0.550991163124
0.949675825872
0.697373890043
0
validation finish
batch start
#iterations: 20
currently lose_sum: 599.753165781498
time_elpased: 1.846
batch start
#iterations: 21
currently lose_sum: 599.8226191997528
time_elpased: 1.822
batch start
#iterations: 22
currently lose_sum: 600.8000960350037
time_elpased: 1.813
batch start
#iterations: 23
currently lose_sum: 599.6059507131577
time_elpased: 1.816
batch start
#iterations: 24
currently lose_sum: 598.8688706755638
time_elpased: 1.802
batch start
#iterations: 25
currently lose_sum: 599.1278345584869
time_elpased: 1.807
batch start
#iterations: 26
currently lose_sum: 599.7282316684723
time_elpased: 1.807
batch start
#iterations: 27
currently lose_sum: 599.4808310866356
time_elpased: 1.833
batch start
#iterations: 28
currently lose_sum: 598.9822721481323
time_elpased: 1.848
batch start
#iterations: 29
currently lose_sum: 598.3134910464287
time_elpased: 1.806
batch start
#iterations: 30
currently lose_sum: 599.0228996872902
time_elpased: 1.808
batch start
#iterations: 31
currently lose_sum: 600.28385668993
time_elpased: 1.803
batch start
#iterations: 32
currently lose_sum: 599.7601693868637
time_elpased: 1.818
batch start
#iterations: 33
currently lose_sum: 598.4306027293205
time_elpased: 1.814
batch start
#iterations: 34
currently lose_sum: 600.2867993116379
time_elpased: 1.823
batch start
#iterations: 35
currently lose_sum: 600.3986037969589
time_elpased: 1.816
batch start
#iterations: 36
currently lose_sum: 598.6486459970474
time_elpased: 1.814
batch start
#iterations: 37
currently lose_sum: 598.5163511633873
time_elpased: 1.82
batch start
#iterations: 38
currently lose_sum: 599.8172491788864
time_elpased: 1.826
batch start
#iterations: 39
currently lose_sum: 598.516225039959
time_elpased: 1.812
start validation test
0.55864640884
0.552316958746
0.938972934033
0.695519600556
0
validation finish
batch start
#iterations: 40
currently lose_sum: 600.4058900475502
time_elpased: 1.817
batch start
#iterations: 41
currently lose_sum: 599.3870272040367
time_elpased: 1.829
batch start
#iterations: 42
currently lose_sum: 599.0791478157043
time_elpased: 1.805
batch start
#iterations: 43
currently lose_sum: 599.2423839569092
time_elpased: 1.829
batch start
#iterations: 44
currently lose_sum: 597.731577694416
time_elpased: 1.822
batch start
#iterations: 45
currently lose_sum: 600.1109625697136
time_elpased: 1.806
batch start
#iterations: 46
currently lose_sum: 599.3327983617783
time_elpased: 1.813
batch start
#iterations: 47
currently lose_sum: 601.2477767467499
time_elpased: 1.789
batch start
#iterations: 48
currently lose_sum: 599.9513734579086
time_elpased: 1.815
batch start
#iterations: 49
currently lose_sum: 598.8048669099808
time_elpased: 1.799
batch start
#iterations: 50
currently lose_sum: 599.3730883002281
time_elpased: 1.787
batch start
#iterations: 51
currently lose_sum: 598.4744107723236
time_elpased: 1.806
batch start
#iterations: 52
currently lose_sum: 598.5674769878387
time_elpased: 1.816
batch start
#iterations: 53
currently lose_sum: 599.476334631443
time_elpased: 1.792
batch start
#iterations: 54
currently lose_sum: 598.3384383320808
time_elpased: 1.815
batch start
#iterations: 55
currently lose_sum: 597.701958835125
time_elpased: 1.803
batch start
#iterations: 56
currently lose_sum: 600.1986652612686
time_elpased: 1.819
batch start
#iterations: 57
currently lose_sum: 598.745875775814
time_elpased: 1.804
batch start
#iterations: 58
currently lose_sum: 599.0222966074944
time_elpased: 1.803
batch start
#iterations: 59
currently lose_sum: 598.6354088783264
time_elpased: 1.81
start validation test
0.559861878453
0.553711147249
0.928578779459
0.6937433926
0
validation finish
batch start
#iterations: 60
currently lose_sum: 597.7551943063736
time_elpased: 1.837
batch start
#iterations: 61
currently lose_sum: 600.6989076137543
time_elpased: 1.821
batch start
#iterations: 62
currently lose_sum: 598.2353330254555
time_elpased: 1.794
batch start
#iterations: 63
currently lose_sum: 598.5836418867111
time_elpased: 1.818
batch start
#iterations: 64
currently lose_sum: 598.7229207754135
time_elpased: 1.805
batch start
#iterations: 65
currently lose_sum: 599.1239993572235
time_elpased: 1.8
batch start
#iterations: 66
currently lose_sum: 598.9696137309074
time_elpased: 1.826
batch start
#iterations: 67
currently lose_sum: 598.3728576898575
time_elpased: 1.822
batch start
#iterations: 68
currently lose_sum: 598.8533049225807
time_elpased: 1.783
batch start
#iterations: 69
currently lose_sum: 599.4078975319862
time_elpased: 1.813
batch start
#iterations: 70
currently lose_sum: 600.8303192257881
time_elpased: 1.828
batch start
#iterations: 71
currently lose_sum: 599.8896650671959
time_elpased: 1.798
batch start
#iterations: 72
currently lose_sum: 598.9320493340492
time_elpased: 1.811
batch start
#iterations: 73
currently lose_sum: 598.3720086812973
time_elpased: 1.823
batch start
#iterations: 74
currently lose_sum: 599.5423095226288
time_elpased: 1.817
batch start
#iterations: 75
currently lose_sum: 600.4422734379768
time_elpased: 1.818
batch start
#iterations: 76
currently lose_sum: 600.9065578579903
time_elpased: 1.791
batch start
#iterations: 77
currently lose_sum: 598.0565856099129
time_elpased: 1.821
batch start
#iterations: 78
currently lose_sum: 598.4901469349861
time_elpased: 1.811
batch start
#iterations: 79
currently lose_sum: 598.3763136863708
time_elpased: 1.816
start validation test
0.563011049724
0.557038720061
0.908305032417
0.690569802242
0
validation finish
batch start
#iterations: 80
currently lose_sum: 596.6615447998047
time_elpased: 1.846
batch start
#iterations: 81
currently lose_sum: 599.3925753235817
time_elpased: 1.839
batch start
#iterations: 82
currently lose_sum: 597.6036919355392
time_elpased: 1.818
batch start
#iterations: 83
currently lose_sum: 599.3754839897156
time_elpased: 1.808
batch start
#iterations: 84
currently lose_sum: 597.4449635744095
time_elpased: 1.803
batch start
#iterations: 85
currently lose_sum: 597.5259266495705
time_elpased: 1.799
batch start
#iterations: 86
currently lose_sum: 598.740528345108
time_elpased: 1.778
batch start
#iterations: 87
currently lose_sum: 599.3021557331085
time_elpased: 1.802
batch start
#iterations: 88
currently lose_sum: 598.2328194975853
time_elpased: 1.802
batch start
#iterations: 89
currently lose_sum: 597.9463176727295
time_elpased: 1.793
batch start
#iterations: 90
currently lose_sum: 596.2923052310944
time_elpased: 1.802
batch start
#iterations: 91
currently lose_sum: 599.9525111913681
time_elpased: 1.805
batch start
#iterations: 92
currently lose_sum: 597.6470843553543
time_elpased: 1.81
batch start
#iterations: 93
currently lose_sum: 598.3323940634727
time_elpased: 1.801
batch start
#iterations: 94
currently lose_sum: 597.5172201395035
time_elpased: 1.803
batch start
#iterations: 95
currently lose_sum: 599.8164131045341
time_elpased: 1.809
batch start
#iterations: 96
currently lose_sum: 598.4588850140572
time_elpased: 1.819
batch start
#iterations: 97
currently lose_sum: 598.3015656471252
time_elpased: 1.814
batch start
#iterations: 98
currently lose_sum: 597.4936193227768
time_elpased: 1.821
batch start
#iterations: 99
currently lose_sum: 598.4829511642456
time_elpased: 1.794
start validation test
0.562513812155
0.555374241818
0.928167129773
0.694931905303
0
validation finish
batch start
#iterations: 100
currently lose_sum: 598.0399367809296
time_elpased: 1.819
batch start
#iterations: 101
currently lose_sum: 598.9713389873505
time_elpased: 1.806
batch start
#iterations: 102
currently lose_sum: 599.4021599292755
time_elpased: 1.805
batch start
#iterations: 103
currently lose_sum: 597.6257856488228
time_elpased: 1.801
batch start
#iterations: 104
currently lose_sum: 599.8126094341278
time_elpased: 1.794
batch start
#iterations: 105
currently lose_sum: 599.474572300911
time_elpased: 1.807
batch start
#iterations: 106
currently lose_sum: 597.7050246596336
time_elpased: 1.804
batch start
#iterations: 107
currently lose_sum: 598.8984190821648
time_elpased: 1.825
batch start
#iterations: 108
currently lose_sum: 600.6606100797653
time_elpased: 1.793
batch start
#iterations: 109
currently lose_sum: 597.5993877053261
time_elpased: 1.816
batch start
#iterations: 110
currently lose_sum: 597.8523643612862
time_elpased: 1.808
batch start
#iterations: 111
currently lose_sum: 597.0054849982262
time_elpased: 1.831
batch start
#iterations: 112
currently lose_sum: 598.2534092664719
time_elpased: 1.795
batch start
#iterations: 113
currently lose_sum: 597.250929236412
time_elpased: 1.82
batch start
#iterations: 114
currently lose_sum: 597.0490415096283
time_elpased: 1.808
batch start
#iterations: 115
currently lose_sum: 598.2268465161324
time_elpased: 1.817
batch start
#iterations: 116
currently lose_sum: 597.3971920013428
time_elpased: 1.812
batch start
#iterations: 117
currently lose_sum: 599.6232575178146
time_elpased: 1.797
batch start
#iterations: 118
currently lose_sum: 599.694990336895
time_elpased: 1.81
batch start
#iterations: 119
currently lose_sum: 598.4757958650589
time_elpased: 1.816
start validation test
0.559861878453
0.552487181794
0.948132139549
0.698152884342
0
validation finish
batch start
#iterations: 120
currently lose_sum: 599.625017285347
time_elpased: 1.801
batch start
#iterations: 121
currently lose_sum: 598.802948474884
time_elpased: 1.796
batch start
#iterations: 122
currently lose_sum: 599.1634273529053
time_elpased: 1.81
batch start
#iterations: 123
currently lose_sum: 597.4465266466141
time_elpased: 1.812
batch start
#iterations: 124
currently lose_sum: 598.5398936867714
time_elpased: 1.815
batch start
#iterations: 125
currently lose_sum: 598.8980666399002
time_elpased: 1.794
batch start
#iterations: 126
currently lose_sum: 598.6106157898903
time_elpased: 1.827
batch start
#iterations: 127
currently lose_sum: 599.0030778646469
time_elpased: 1.811
batch start
#iterations: 128
currently lose_sum: 598.4809694290161
time_elpased: 1.826
batch start
#iterations: 129
currently lose_sum: 599.5990570187569
time_elpased: 1.831
batch start
#iterations: 130
currently lose_sum: 599.4718018174171
time_elpased: 1.796
batch start
#iterations: 131
currently lose_sum: 597.74064463377
time_elpased: 1.801
batch start
#iterations: 132
currently lose_sum: 599.2282350063324
time_elpased: 1.807
batch start
#iterations: 133
currently lose_sum: 599.2938566207886
time_elpased: 1.812
batch start
#iterations: 134
currently lose_sum: 598.9721788167953
time_elpased: 1.801
batch start
#iterations: 135
currently lose_sum: 598.1008944511414
time_elpased: 1.826
batch start
#iterations: 136
currently lose_sum: 598.3300368189812
time_elpased: 1.796
batch start
#iterations: 137
currently lose_sum: 596.2408844232559
time_elpased: 1.809
batch start
#iterations: 138
currently lose_sum: 598.8172970414162
time_elpased: 1.791
batch start
#iterations: 139
currently lose_sum: 600.0104189515114
time_elpased: 1.806
start validation test
0.558701657459
0.551773686574
0.948440876814
0.697666496338
0
validation finish
batch start
#iterations: 140
currently lose_sum: 598.8909565806389
time_elpased: 1.833
batch start
#iterations: 141
currently lose_sum: 598.0977430343628
time_elpased: 1.799
batch start
#iterations: 142
currently lose_sum: 597.71467846632
time_elpased: 1.801
batch start
#iterations: 143
currently lose_sum: 596.7705355286598
time_elpased: 1.817
batch start
#iterations: 144
currently lose_sum: 598.9171117544174
time_elpased: 1.804
batch start
#iterations: 145
currently lose_sum: 598.3368967175484
time_elpased: 1.83
batch start
#iterations: 146
currently lose_sum: 598.0754416584969
time_elpased: 1.805
batch start
#iterations: 147
currently lose_sum: 597.8530992269516
time_elpased: 1.78
batch start
#iterations: 148
currently lose_sum: 597.626557290554
time_elpased: 1.827
batch start
#iterations: 149
currently lose_sum: 597.3784772157669
time_elpased: 1.804
batch start
#iterations: 150
currently lose_sum: 598.6135481595993
time_elpased: 1.808
batch start
#iterations: 151
currently lose_sum: 597.5331115722656
time_elpased: 1.817
batch start
#iterations: 152
currently lose_sum: 598.5197479724884
time_elpased: 1.824
batch start
#iterations: 153
currently lose_sum: 596.8610842823982
time_elpased: 1.836
batch start
#iterations: 154
currently lose_sum: 598.532167494297
time_elpased: 1.824
batch start
#iterations: 155
currently lose_sum: 598.8566446900368
time_elpased: 1.812
batch start
#iterations: 156
currently lose_sum: 599.8708161711693
time_elpased: 1.799
batch start
#iterations: 157
currently lose_sum: 598.288013458252
time_elpased: 1.837
batch start
#iterations: 158
currently lose_sum: 596.8698315024376
time_elpased: 1.809
batch start
#iterations: 159
currently lose_sum: 598.1655250191689
time_elpased: 1.81
start validation test
0.561436464088
0.553827534039
0.941854481836
0.697507811905
0
validation finish
batch start
#iterations: 160
currently lose_sum: 597.769305408001
time_elpased: 1.834
batch start
#iterations: 161
currently lose_sum: 596.9229833483696
time_elpased: 1.834
batch start
#iterations: 162
currently lose_sum: 600.1407959461212
time_elpased: 1.805
batch start
#iterations: 163
currently lose_sum: 597.8259397149086
time_elpased: 1.819
batch start
#iterations: 164
currently lose_sum: 598.8232815265656
time_elpased: 1.821
batch start
#iterations: 165
currently lose_sum: 600.5512484312057
time_elpased: 1.832
batch start
#iterations: 166
currently lose_sum: 597.8522883653641
time_elpased: 1.82
batch start
#iterations: 167
currently lose_sum: 600.0686731338501
time_elpased: 1.815
batch start
#iterations: 168
currently lose_sum: 599.3357807397842
time_elpased: 1.797
batch start
#iterations: 169
currently lose_sum: 597.6681349873543
time_elpased: 1.837
batch start
#iterations: 170
currently lose_sum: 597.6165212392807
time_elpased: 1.824
batch start
#iterations: 171
currently lose_sum: 596.3051819205284
time_elpased: 1.793
batch start
#iterations: 172
currently lose_sum: 597.1886495947838
time_elpased: 1.817
batch start
#iterations: 173
currently lose_sum: 597.4564781785011
time_elpased: 1.821
batch start
#iterations: 174
currently lose_sum: 598.0215066075325
time_elpased: 1.845
batch start
#iterations: 175
currently lose_sum: 598.2394201159477
time_elpased: 1.843
batch start
#iterations: 176
currently lose_sum: 598.8736300468445
time_elpased: 1.831
batch start
#iterations: 177
currently lose_sum: 596.7782886624336
time_elpased: 1.827
batch start
#iterations: 178
currently lose_sum: 597.7748880982399
time_elpased: 1.833
batch start
#iterations: 179
currently lose_sum: 598.4730174541473
time_elpased: 1.818
start validation test
0.562624309392
0.554555646456
0.941751569414
0.698056715678
0
validation finish
batch start
#iterations: 180
currently lose_sum: 596.1583279371262
time_elpased: 1.812
batch start
#iterations: 181
currently lose_sum: 597.2733894586563
time_elpased: 1.827
batch start
#iterations: 182
currently lose_sum: 597.5475793480873
time_elpased: 1.839
batch start
#iterations: 183
currently lose_sum: 598.7024610638618
time_elpased: 1.84
batch start
#iterations: 184
currently lose_sum: 597.5575405955315
time_elpased: 1.817
batch start
#iterations: 185
currently lose_sum: 598.691029548645
time_elpased: 1.83
batch start
#iterations: 186
currently lose_sum: 598.3802946209908
time_elpased: 1.817
batch start
#iterations: 187
currently lose_sum: 598.6734860539436
time_elpased: 1.831
batch start
#iterations: 188
currently lose_sum: 598.725327193737
time_elpased: 1.815
batch start
#iterations: 189
currently lose_sum: 599.5391146540642
time_elpased: 1.83
batch start
#iterations: 190
currently lose_sum: 595.5714009404182
time_elpased: 1.81
batch start
#iterations: 191
currently lose_sum: 596.9905380606651
time_elpased: 1.821
batch start
#iterations: 192
currently lose_sum: 598.5057556629181
time_elpased: 1.803
batch start
#iterations: 193
currently lose_sum: 599.6618920564651
time_elpased: 1.832
batch start
#iterations: 194
currently lose_sum: 597.7240774035454
time_elpased: 1.821
batch start
#iterations: 195
currently lose_sum: 599.2627359032631
time_elpased: 1.799
batch start
#iterations: 196
currently lose_sum: 598.1142775416374
time_elpased: 1.811
batch start
#iterations: 197
currently lose_sum: 598.7410846352577
time_elpased: 1.814
batch start
#iterations: 198
currently lose_sum: 597.8380751609802
time_elpased: 1.811
batch start
#iterations: 199
currently lose_sum: 597.794558763504
time_elpased: 1.819
start validation test
0.559944751381
0.552809259706
0.943706905423
0.6972058544
0
validation finish
batch start
#iterations: 200
currently lose_sum: 596.6572712063789
time_elpased: 1.821
batch start
#iterations: 201
currently lose_sum: 599.3516448736191
time_elpased: 1.813
batch start
#iterations: 202
currently lose_sum: 597.6338986754417
time_elpased: 1.831
batch start
#iterations: 203
currently lose_sum: 597.8687800168991
time_elpased: 1.815
batch start
#iterations: 204
currently lose_sum: 596.4035326838493
time_elpased: 1.837
batch start
#iterations: 205
currently lose_sum: 596.1208610534668
time_elpased: 1.821
batch start
#iterations: 206
currently lose_sum: 598.9964086413383
time_elpased: 1.829
batch start
#iterations: 207
currently lose_sum: 598.275329887867
time_elpased: 1.811
batch start
#iterations: 208
currently lose_sum: 597.4722799658775
time_elpased: 1.814
batch start
#iterations: 209
currently lose_sum: 597.8217357993126
time_elpased: 1.805
batch start
#iterations: 210
currently lose_sum: 597.084426343441
time_elpased: 1.815
batch start
#iterations: 211
currently lose_sum: 598.1738556027412
time_elpased: 1.792
batch start
#iterations: 212
currently lose_sum: 597.1550714969635
time_elpased: 1.802
batch start
#iterations: 213
currently lose_sum: 598.9361476898193
time_elpased: 1.811
batch start
#iterations: 214
currently lose_sum: 597.707990527153
time_elpased: 1.796
batch start
#iterations: 215
currently lose_sum: 598.3769772648811
time_elpased: 1.829
batch start
#iterations: 216
currently lose_sum: 597.1663575768471
time_elpased: 1.816
batch start
#iterations: 217
currently lose_sum: 596.8061804175377
time_elpased: 1.823
batch start
#iterations: 218
currently lose_sum: 599.8408121466637
time_elpased: 1.835
batch start
#iterations: 219
currently lose_sum: 597.4505065083504
time_elpased: 1.821
start validation test
0.56
0.55298954114
0.941339919728
0.696701957499
0
validation finish
batch start
#iterations: 220
currently lose_sum: 597.7106649279594
time_elpased: 1.829
batch start
#iterations: 221
currently lose_sum: 596.8756833076477
time_elpased: 1.829
batch start
#iterations: 222
currently lose_sum: 595.2343572974205
time_elpased: 1.832
batch start
#iterations: 223
currently lose_sum: 596.9043251872063
time_elpased: 1.812
batch start
#iterations: 224
currently lose_sum: 597.7457595467567
time_elpased: 1.816
batch start
#iterations: 225
currently lose_sum: 597.7807163000107
time_elpased: 1.825
batch start
#iterations: 226
currently lose_sum: 597.46931630373
time_elpased: 1.804
batch start
#iterations: 227
currently lose_sum: 598.2769656181335
time_elpased: 1.813
batch start
#iterations: 228
currently lose_sum: 596.8778235912323
time_elpased: 1.814
batch start
#iterations: 229
currently lose_sum: 597.9880400896072
time_elpased: 1.816
batch start
#iterations: 230
currently lose_sum: 599.2119296789169
time_elpased: 1.838
batch start
#iterations: 231
currently lose_sum: 596.814883351326
time_elpased: 1.826
batch start
#iterations: 232
currently lose_sum: 597.4231599569321
time_elpased: 1.811
batch start
#iterations: 233
currently lose_sum: 598.5954154729843
time_elpased: 1.829
batch start
#iterations: 234
currently lose_sum: 598.0989421010017
time_elpased: 1.812
batch start
#iterations: 235
currently lose_sum: 597.0999609231949
time_elpased: 1.815
batch start
#iterations: 236
currently lose_sum: 599.4747879505157
time_elpased: 1.814
batch start
#iterations: 237
currently lose_sum: 597.7434774041176
time_elpased: 1.811
batch start
#iterations: 238
currently lose_sum: 599.8769958615303
time_elpased: 1.805
batch start
#iterations: 239
currently lose_sum: 596.9669002890587
time_elpased: 1.805
start validation test
0.559171270718
0.551917791851
0.950704950087
0.698393498393
0
validation finish
batch start
#iterations: 240
currently lose_sum: 599.4796557426453
time_elpased: 1.82
batch start
#iterations: 241
currently lose_sum: 597.234027504921
time_elpased: 1.896
batch start
#iterations: 242
currently lose_sum: 597.9082962274551
time_elpased: 1.81
batch start
#iterations: 243
currently lose_sum: 598.2820372581482
time_elpased: 1.83
batch start
#iterations: 244
currently lose_sum: 596.7807865738869
time_elpased: 1.823
batch start
#iterations: 245
currently lose_sum: 597.3931900858879
time_elpased: 1.827
batch start
#iterations: 246
currently lose_sum: 599.028472661972
time_elpased: 1.839
batch start
#iterations: 247
currently lose_sum: 597.1751881837845
time_elpased: 1.869
batch start
#iterations: 248
currently lose_sum: 598.8496180772781
time_elpased: 1.898
batch start
#iterations: 249
currently lose_sum: 598.5415994524956
time_elpased: 1.837
batch start
#iterations: 250
currently lose_sum: 598.7922758460045
time_elpased: 1.835
batch start
#iterations: 251
currently lose_sum: 597.3546720147133
time_elpased: 1.929
batch start
#iterations: 252
currently lose_sum: 596.1969911456108
time_elpased: 1.876
batch start
#iterations: 253
currently lose_sum: 597.1398006677628
time_elpased: 1.828
batch start
#iterations: 254
currently lose_sum: 598.0251869559288
time_elpased: 1.824
batch start
#iterations: 255
currently lose_sum: 598.0085619091988
time_elpased: 1.837
batch start
#iterations: 256
currently lose_sum: 597.0728625655174
time_elpased: 1.822
batch start
#iterations: 257
currently lose_sum: 598.853976547718
time_elpased: 1.883
batch start
#iterations: 258
currently lose_sum: 597.0679497122765
time_elpased: 1.932
batch start
#iterations: 259
currently lose_sum: 598.3187504410744
time_elpased: 1.932
start validation test
0.560110497238
0.552871000783
0.944324379953
0.697423424793
0
validation finish
batch start
#iterations: 260
currently lose_sum: 599.1499531269073
time_elpased: 1.856
batch start
#iterations: 261
currently lose_sum: 599.3361250162125
time_elpased: 1.896
batch start
#iterations: 262
currently lose_sum: 598.848810851574
time_elpased: 1.855
batch start
#iterations: 263
currently lose_sum: 600.2088751196861
time_elpased: 1.945
batch start
#iterations: 264
currently lose_sum: 598.0838463306427
time_elpased: 1.96
batch start
#iterations: 265
currently lose_sum: 598.368744790554
time_elpased: 1.92
batch start
#iterations: 266
currently lose_sum: 595.4807677268982
time_elpased: 1.913
batch start
#iterations: 267
currently lose_sum: 597.2952928543091
time_elpased: 1.903
batch start
#iterations: 268
currently lose_sum: 596.537622153759
time_elpased: 1.962
batch start
#iterations: 269
currently lose_sum: 599.0546703338623
time_elpased: 1.959
batch start
#iterations: 270
currently lose_sum: 597.7557110190392
time_elpased: 1.896
batch start
#iterations: 271
currently lose_sum: 598.1268044710159
time_elpased: 1.928
batch start
#iterations: 272
currently lose_sum: 599.6224882006645
time_elpased: 1.882
batch start
#iterations: 273
currently lose_sum: 598.2408050894737
time_elpased: 1.91
batch start
#iterations: 274
currently lose_sum: 598.6784038543701
time_elpased: 1.933
batch start
#iterations: 275
currently lose_sum: 599.0543103814125
time_elpased: 1.946
batch start
#iterations: 276
currently lose_sum: 598.9852766990662
time_elpased: 1.966
batch start
#iterations: 277
currently lose_sum: 598.5939961075783
time_elpased: 1.933
batch start
#iterations: 278
currently lose_sum: 597.7420917749405
time_elpased: 1.928
batch start
#iterations: 279
currently lose_sum: 597.6848627924919
time_elpased: 1.921
start validation test
0.562983425414
0.554940711462
0.939178758876
0.697653084627
0
validation finish
batch start
#iterations: 280
currently lose_sum: 597.5954645872116
time_elpased: 1.943
batch start
#iterations: 281
currently lose_sum: 598.2458844780922
time_elpased: 1.94
batch start
#iterations: 282
currently lose_sum: 597.9279629588127
time_elpased: 1.9
batch start
#iterations: 283
currently lose_sum: 599.5673816204071
time_elpased: 1.933
batch start
#iterations: 284
currently lose_sum: 597.742592394352
time_elpased: 1.917
batch start
#iterations: 285
currently lose_sum: 596.7186160683632
time_elpased: 1.996
batch start
#iterations: 286
currently lose_sum: 597.7516360878944
time_elpased: 1.906
batch start
#iterations: 287
currently lose_sum: 599.9042101502419
time_elpased: 1.951
batch start
#iterations: 288
currently lose_sum: 597.8324844837189
time_elpased: 1.916
batch start
#iterations: 289
currently lose_sum: 595.8719145655632
time_elpased: 1.892
batch start
#iterations: 290
currently lose_sum: 597.4437268972397
time_elpased: 1.928
batch start
#iterations: 291
currently lose_sum: 597.6344764232635
time_elpased: 1.899
batch start
#iterations: 292
currently lose_sum: 597.8830887675285
time_elpased: 1.9
batch start
#iterations: 293
currently lose_sum: 597.6524918675423
time_elpased: 1.944
batch start
#iterations: 294
currently lose_sum: 598.8896462917328
time_elpased: 1.883
batch start
#iterations: 295
currently lose_sum: 599.7654197216034
time_elpased: 1.915
batch start
#iterations: 296
currently lose_sum: 597.7172535061836
time_elpased: 1.953
batch start
#iterations: 297
currently lose_sum: 599.3745720386505
time_elpased: 1.936
batch start
#iterations: 298
currently lose_sum: 598.9084156751633
time_elpased: 1.928
batch start
#iterations: 299
currently lose_sum: 599.4030857086182
time_elpased: 1.937
start validation test
0.561077348066
0.553752843063
0.939590408562
0.696826880879
0
validation finish
batch start
#iterations: 300
currently lose_sum: 599.5019277334213
time_elpased: 1.948
batch start
#iterations: 301
currently lose_sum: 597.400193810463
time_elpased: 1.924
batch start
#iterations: 302
currently lose_sum: 598.9996197223663
time_elpased: 1.869
batch start
#iterations: 303
currently lose_sum: 598.8002278804779
time_elpased: 1.88
batch start
#iterations: 304
currently lose_sum: 598.1453693509102
time_elpased: 1.908
batch start
#iterations: 305
currently lose_sum: 597.3489154577255
time_elpased: 1.893
batch start
#iterations: 306
currently lose_sum: 599.4122089743614
time_elpased: 1.886
batch start
#iterations: 307
currently lose_sum: 597.3552277684212
time_elpased: 1.996
batch start
#iterations: 308
currently lose_sum: 598.105471432209
time_elpased: 1.927
batch start
#iterations: 309
currently lose_sum: 598.1642082929611
time_elpased: 1.892
batch start
#iterations: 310
currently lose_sum: 597.8137964010239
time_elpased: 1.89
batch start
#iterations: 311
currently lose_sum: 597.3119965195656
time_elpased: 1.898
batch start
#iterations: 312
currently lose_sum: 597.0210501551628
time_elpased: 1.892
batch start
#iterations: 313
currently lose_sum: 599.094387948513
time_elpased: 1.938
batch start
#iterations: 314
currently lose_sum: 597.5486109256744
time_elpased: 1.927
batch start
#iterations: 315
currently lose_sum: 597.5850293636322
time_elpased: 1.909
batch start
#iterations: 316
currently lose_sum: 596.0601065158844
time_elpased: 1.866
batch start
#iterations: 317
currently lose_sum: 597.8104125261307
time_elpased: 1.854
batch start
#iterations: 318
currently lose_sum: 597.5483030676842
time_elpased: 1.839
batch start
#iterations: 319
currently lose_sum: 598.4283146858215
time_elpased: 1.875
start validation test
0.56044198895
0.553019810923
0.945147679325
0.697766296915
0
validation finish
batch start
#iterations: 320
currently lose_sum: 596.5066854953766
time_elpased: 1.943
batch start
#iterations: 321
currently lose_sum: 596.3343119621277
time_elpased: 1.878
batch start
#iterations: 322
currently lose_sum: 596.704069674015
time_elpased: 1.823
batch start
#iterations: 323
currently lose_sum: 598.7115141749382
time_elpased: 1.896
batch start
#iterations: 324
currently lose_sum: 597.773327589035
time_elpased: 1.886
batch start
#iterations: 325
currently lose_sum: 597.3326146602631
time_elpased: 1.89
batch start
#iterations: 326
currently lose_sum: 598.1558651924133
time_elpased: 1.849
batch start
#iterations: 327
currently lose_sum: 599.3424336314201
time_elpased: 1.844
batch start
#iterations: 328
currently lose_sum: 596.8986698389053
time_elpased: 1.847
batch start
#iterations: 329
currently lose_sum: 597.5461021065712
time_elpased: 1.858
batch start
#iterations: 330
currently lose_sum: 598.0118662118912
time_elpased: 1.874
batch start
#iterations: 331
currently lose_sum: 598.2856222391129
time_elpased: 1.882
batch start
#iterations: 332
currently lose_sum: 596.826088309288
time_elpased: 1.875
batch start
#iterations: 333
currently lose_sum: 597.2913630604744
time_elpased: 1.868
batch start
#iterations: 334
currently lose_sum: 599.0663551688194
time_elpased: 1.864
batch start
#iterations: 335
currently lose_sum: 597.2048829197884
time_elpased: 1.834
batch start
#iterations: 336
currently lose_sum: 597.9786672592163
time_elpased: 1.883
batch start
#iterations: 337
currently lose_sum: 597.6238179802895
time_elpased: 1.849
batch start
#iterations: 338
currently lose_sum: 597.7056344151497
time_elpased: 1.849
batch start
#iterations: 339
currently lose_sum: 599.918739438057
time_elpased: 1.844
start validation test
0.559364640884
0.552264337805
0.946897190491
0.69764003412
0
validation finish
batch start
#iterations: 340
currently lose_sum: 595.9167352318764
time_elpased: 1.885
batch start
#iterations: 341
currently lose_sum: 598.5979702472687
time_elpased: 1.87
batch start
#iterations: 342
currently lose_sum: 598.7361868023872
time_elpased: 1.856
batch start
#iterations: 343
currently lose_sum: 598.0779670476913
time_elpased: 1.847
batch start
#iterations: 344
currently lose_sum: 597.5669448971748
time_elpased: 1.835
batch start
#iterations: 345
currently lose_sum: 597.3658168911934
time_elpased: 1.854
batch start
#iterations: 346
currently lose_sum: 597.2554732561111
time_elpased: 1.857
batch start
#iterations: 347
currently lose_sum: 599.5077158808708
time_elpased: 1.859
batch start
#iterations: 348
currently lose_sum: 597.6332630515099
time_elpased: 1.873
batch start
#iterations: 349
currently lose_sum: 597.865421295166
time_elpased: 1.852
batch start
#iterations: 350
currently lose_sum: 599.550011754036
time_elpased: 1.874
batch start
#iterations: 351
currently lose_sum: 597.2357286810875
time_elpased: 1.887
batch start
#iterations: 352
currently lose_sum: 598.9545961618423
time_elpased: 1.849
batch start
#iterations: 353
currently lose_sum: 598.4028586149216
time_elpased: 1.874
batch start
#iterations: 354
currently lose_sum: 596.922990322113
time_elpased: 1.868
batch start
#iterations: 355
currently lose_sum: 598.6601051092148
time_elpased: 1.868
batch start
#iterations: 356
currently lose_sum: 597.5603194236755
time_elpased: 1.851
batch start
#iterations: 357
currently lose_sum: 596.6816000342369
time_elpased: 1.846
batch start
#iterations: 358
currently lose_sum: 598.2712191343307
time_elpased: 1.819
batch start
#iterations: 359
currently lose_sum: 595.5021997690201
time_elpased: 1.841
start validation test
0.55955801105
0.552336392538
0.947617577442
0.697892981658
0
validation finish
batch start
#iterations: 360
currently lose_sum: 597.8212794065475
time_elpased: 1.886
batch start
#iterations: 361
currently lose_sum: 597.6203882098198
time_elpased: 1.848
batch start
#iterations: 362
currently lose_sum: 598.1550372838974
time_elpased: 1.864
batch start
#iterations: 363
currently lose_sum: 598.5279175639153
time_elpased: 1.842
batch start
#iterations: 364
currently lose_sum: 598.383524119854
time_elpased: 1.86
batch start
#iterations: 365
currently lose_sum: 598.570100903511
time_elpased: 1.856
batch start
#iterations: 366
currently lose_sum: 597.1437860131264
time_elpased: 1.844
batch start
#iterations: 367
currently lose_sum: 598.2123880386353
time_elpased: 1.844
batch start
#iterations: 368
currently lose_sum: 597.1504834890366
time_elpased: 1.888
batch start
#iterations: 369
currently lose_sum: 597.242058634758
time_elpased: 1.833
batch start
#iterations: 370
currently lose_sum: 596.2063453793526
time_elpased: 1.831
batch start
#iterations: 371
currently lose_sum: 596.3434164524078
time_elpased: 1.824
batch start
#iterations: 372
currently lose_sum: 598.2732233405113
time_elpased: 1.825
batch start
#iterations: 373
currently lose_sum: 599.5271106362343
time_elpased: 1.834
batch start
#iterations: 374
currently lose_sum: 596.8019331693649
time_elpased: 1.865
batch start
#iterations: 375
currently lose_sum: 597.9425362944603
time_elpased: 1.864
batch start
#iterations: 376
currently lose_sum: 597.4990252256393
time_elpased: 1.821
batch start
#iterations: 377
currently lose_sum: 596.9033630490303
time_elpased: 1.879
batch start
#iterations: 378
currently lose_sum: 597.477849304676
time_elpased: 1.841
batch start
#iterations: 379
currently lose_sum: 597.2432042956352
time_elpased: 1.847
start validation test
0.560718232044
0.553372823985
0.942163219101
0.697231636267
0
validation finish
batch start
#iterations: 380
currently lose_sum: 598.4183991551399
time_elpased: 1.86
batch start
#iterations: 381
currently lose_sum: 597.6643405556679
time_elpased: 1.843
batch start
#iterations: 382
currently lose_sum: 597.6715772747993
time_elpased: 1.855
batch start
#iterations: 383
currently lose_sum: 597.3592526316643
time_elpased: 1.842
batch start
#iterations: 384
currently lose_sum: 599.2993093132973
time_elpased: 1.844
batch start
#iterations: 385
currently lose_sum: 597.6923748850822
time_elpased: 1.851
batch start
#iterations: 386
currently lose_sum: 599.0066378116608
time_elpased: 1.86
batch start
#iterations: 387
currently lose_sum: 598.2887156009674
time_elpased: 1.885
batch start
#iterations: 388
currently lose_sum: 599.1609155535698
time_elpased: 1.864
batch start
#iterations: 389
currently lose_sum: 597.057948410511
time_elpased: 1.867
batch start
#iterations: 390
currently lose_sum: 598.4601884484291
time_elpased: 1.836
batch start
#iterations: 391
currently lose_sum: 599.7428403496742
time_elpased: 1.868
batch start
#iterations: 392
currently lose_sum: 597.4461808204651
time_elpased: 1.836
batch start
#iterations: 393
currently lose_sum: 598.5803343057632
time_elpased: 1.846
batch start
#iterations: 394
currently lose_sum: 598.2384560704231
time_elpased: 1.857
batch start
#iterations: 395
currently lose_sum: 598.8367820978165
time_elpased: 1.835
batch start
#iterations: 396
currently lose_sum: 598.5571321845055
time_elpased: 1.87
batch start
#iterations: 397
currently lose_sum: 597.8061593770981
time_elpased: 1.85
batch start
#iterations: 398
currently lose_sum: 597.7501884102821
time_elpased: 1.834
batch start
#iterations: 399
currently lose_sum: 597.8472076654434
time_elpased: 1.833
start validation test
0.560635359116
0.553181229091
0.944427292374
0.697698287495
0
validation finish
acc: 0.559
pre: 0.553
rec: 0.949
F1: 0.698
auc: 0.000
