start to construct graph
graph construct over
93366
epochs start
batch start
#iterations: 0
currently lose_sum: 616.6079949736595
time_elpased: 5.917
batch start
#iterations: 1
currently lose_sum: 609.2434265613556
time_elpased: 5.757
batch start
#iterations: 2
currently lose_sum: 606.0260932445526
time_elpased: 5.658
batch start
#iterations: 3
currently lose_sum: 603.6388859152794
time_elpased: 5.597
batch start
#iterations: 4
currently lose_sum: 602.7214133143425
time_elpased: 5.655
batch start
#iterations: 5
currently lose_sum: 600.4348289966583
time_elpased: 5.337
batch start
#iterations: 6
currently lose_sum: 600.2059118747711
time_elpased: 5.74
batch start
#iterations: 7
currently lose_sum: 598.2396019101143
time_elpased: 5.558
batch start
#iterations: 8
currently lose_sum: 596.0489326715469
time_elpased: 5.85
batch start
#iterations: 9
currently lose_sum: 597.5282261371613
time_elpased: 5.639
batch start
#iterations: 10
currently lose_sum: 596.2352672815323
time_elpased: 5.559
batch start
#iterations: 11
currently lose_sum: 596.7915716171265
time_elpased: 5.492
batch start
#iterations: 12
currently lose_sum: 594.2240436077118
time_elpased: 5.736
batch start
#iterations: 13
currently lose_sum: 596.3306129574776
time_elpased: 5.719
batch start
#iterations: 14
currently lose_sum: 594.8736172914505
time_elpased: 5.657
batch start
#iterations: 15
currently lose_sum: 594.0304743051529
time_elpased: 5.697
batch start
#iterations: 16
currently lose_sum: 592.504660487175
time_elpased: 5.544
batch start
#iterations: 17
currently lose_sum: 594.5186552405357
time_elpased: 5.562
batch start
#iterations: 18
currently lose_sum: 593.6308032274246
time_elpased: 5.751
batch start
#iterations: 19
currently lose_sum: 596.3478657603264
time_elpased: 5.79
start validation test
0.559639889197
0.553534130468
0.940928270042
0.697021098172
0
validation finish
batch start
#iterations: 20
currently lose_sum: 592.91909968853
time_elpased: 5.689
batch start
#iterations: 21
currently lose_sum: 593.6503008008003
time_elpased: 5.606
batch start
#iterations: 22
currently lose_sum: 593.9196743965149
time_elpased: 5.658
batch start
#iterations: 23
currently lose_sum: 592.7414185404778
time_elpased: 5.651
batch start
#iterations: 24
currently lose_sum: 593.1005101203918
time_elpased: 5.795
batch start
#iterations: 25
currently lose_sum: 592.5011451244354
time_elpased: 5.499
batch start
#iterations: 26
currently lose_sum: 591.3457542657852
time_elpased: 5.689
batch start
#iterations: 27
currently lose_sum: 591.2989961504936
time_elpased: 5.561
batch start
#iterations: 28
currently lose_sum: 591.3170555233955
time_elpased: 5.545
batch start
#iterations: 29
currently lose_sum: 591.6641337275505
time_elpased: 5.741
batch start
#iterations: 30
currently lose_sum: 591.9249523282051
time_elpased: 5.907
batch start
#iterations: 31
currently lose_sum: 591.912841796875
time_elpased: 5.461
batch start
#iterations: 32
currently lose_sum: 590.2412369251251
time_elpased: 5.64
batch start
#iterations: 33
currently lose_sum: 588.4547836780548
time_elpased: 5.612
batch start
#iterations: 34
currently lose_sum: 591.3163457512856
time_elpased: 5.883
batch start
#iterations: 35
currently lose_sum: 591.7093762159348
time_elpased: 5.606
batch start
#iterations: 36
currently lose_sum: 591.494509100914
time_elpased: 5.537
batch start
#iterations: 37
currently lose_sum: 591.431528031826
time_elpased: 5.755
batch start
#iterations: 38
currently lose_sum: 590.8652908205986
time_elpased: 5.672
batch start
#iterations: 39
currently lose_sum: 591.1061764359474
time_elpased: 5.608
start validation test
0.559335180055
0.554659887146
0.920551610579
0.692230304906
0
validation finish
batch start
#iterations: 40
currently lose_sum: 588.662880897522
time_elpased: 5.467
batch start
#iterations: 41
currently lose_sum: 588.9282276630402
time_elpased: 5.776
batch start
#iterations: 42
currently lose_sum: 590.8411672711372
time_elpased: 5.746
batch start
#iterations: 43
currently lose_sum: 588.3922984600067
time_elpased: 5.819
batch start
#iterations: 44
currently lose_sum: 589.6684865951538
time_elpased: 5.828
batch start
#iterations: 45
currently lose_sum: 589.6460894942284
time_elpased: 5.854
batch start
#iterations: 46
currently lose_sum: 590.2728087306023
time_elpased: 5.747
batch start
#iterations: 47
currently lose_sum: 589.6964750289917
time_elpased: 5.697
batch start
#iterations: 48
currently lose_sum: 590.4003530144691
time_elpased: 5.478
batch start
#iterations: 49
currently lose_sum: 591.5437170267105
time_elpased: 5.61
batch start
#iterations: 50
currently lose_sum: 589.5401867628098
time_elpased: 5.656
batch start
#iterations: 51
currently lose_sum: 588.4382578134537
time_elpased: 5.862
batch start
#iterations: 52
currently lose_sum: 589.9001926779747
time_elpased: 5.81
batch start
#iterations: 53
currently lose_sum: 589.3634040951729
time_elpased: 5.669
batch start
#iterations: 54
currently lose_sum: 590.6760137081146
time_elpased: 5.42
batch start
#iterations: 55
currently lose_sum: 588.8698251843452
time_elpased: 5.852
batch start
#iterations: 56
currently lose_sum: 589.4206428527832
time_elpased: 5.829
batch start
#iterations: 57
currently lose_sum: 588.6267142295837
time_elpased: 5.454
batch start
#iterations: 58
currently lose_sum: 591.319527387619
time_elpased: 5.785
batch start
#iterations: 59
currently lose_sum: 587.9982134103775
time_elpased: 5.543
start validation test
0.559501385042
0.55430504305
0.927549655243
0.693921545983
0
validation finish
batch start
#iterations: 60
currently lose_sum: 589.1738882660866
time_elpased: 5.446
batch start
#iterations: 61
currently lose_sum: 587.4030584692955
time_elpased: 5.672
batch start
#iterations: 62
currently lose_sum: 590.574237704277
time_elpased: 5.609
batch start
#iterations: 63
currently lose_sum: 589.5244677066803
time_elpased: 5.908
batch start
#iterations: 64
currently lose_sum: 587.3808028101921
time_elpased: 5.598
batch start
#iterations: 65
currently lose_sum: 588.4336820244789
time_elpased: 5.618
batch start
#iterations: 66
currently lose_sum: 587.5257222056389
time_elpased: 5.495
batch start
#iterations: 67
currently lose_sum: 587.7811492681503
time_elpased: 5.898
batch start
#iterations: 68
currently lose_sum: 588.4056258201599
time_elpased: 5.859
batch start
#iterations: 69
currently lose_sum: 588.4625598192215
time_elpased: 5.624
batch start
#iterations: 70
currently lose_sum: 588.8841435313225
time_elpased: 5.689
batch start
#iterations: 71
currently lose_sum: 588.4329518079758
time_elpased: 5.604
batch start
#iterations: 72
currently lose_sum: 587.6319186091423
time_elpased: 5.539
batch start
#iterations: 73
currently lose_sum: 589.2171522974968
time_elpased: 5.553
batch start
#iterations: 74
currently lose_sum: 587.5102758407593
time_elpased: 5.812
batch start
#iterations: 75
currently lose_sum: 587.0668278336525
time_elpased: 5.572
batch start
#iterations: 76
currently lose_sum: 588.8193606138229
time_elpased: 5.957
batch start
#iterations: 77
currently lose_sum: 585.0050964057446
time_elpased: 5.674
batch start
#iterations: 78
currently lose_sum: 588.5654101371765
time_elpased: 5.63
batch start
#iterations: 79
currently lose_sum: 588.2239719629288
time_elpased: 5.589
start validation test
0.559501385042
0.554378618056
0.926417618607
0.693662107494
0
validation finish
batch start
#iterations: 80
currently lose_sum: 588.0433859825134
time_elpased: 5.601
batch start
#iterations: 81
currently lose_sum: 586.0100105404854
time_elpased: 5.845
batch start
#iterations: 82
currently lose_sum: 587.8233191370964
time_elpased: 5.749
batch start
#iterations: 83
currently lose_sum: 586.1946139335632
time_elpased: 5.771
batch start
#iterations: 84
currently lose_sum: 585.9993831515312
time_elpased: 5.759
batch start
#iterations: 85
currently lose_sum: 588.22089022398
time_elpased: 5.659
batch start
#iterations: 86
currently lose_sum: 587.1961023211479
time_elpased: 5.868
batch start
#iterations: 87
currently lose_sum: 587.9732196331024
time_elpased: 5.784
batch start
#iterations: 88
currently lose_sum: 588.3981570601463
time_elpased: 5.621
batch start
#iterations: 89
currently lose_sum: 585.9998185038567
time_elpased: 5.635
batch start
#iterations: 90
currently lose_sum: 587.3946451544762
time_elpased: 5.692
batch start
#iterations: 91
currently lose_sum: 586.863574385643
time_elpased: 5.873
batch start
#iterations: 92
currently lose_sum: 586.2038838863373
time_elpased: 5.657
batch start
#iterations: 93
currently lose_sum: 589.5274533629417
time_elpased: 5.687
batch start
#iterations: 94
currently lose_sum: 586.214443385601
time_elpased: 5.699
batch start
#iterations: 95
currently lose_sum: 586.0115962028503
time_elpased: 5.648
batch start
#iterations: 96
currently lose_sum: 585.9101899862289
time_elpased: 5.849
batch start
#iterations: 97
currently lose_sum: 585.9300849437714
time_elpased: 5.623
batch start
#iterations: 98
currently lose_sum: 586.0840599536896
time_elpased: 5.724
batch start
#iterations: 99
currently lose_sum: 586.8361275196075
time_elpased: 5.554
start validation test
0.558005540166
0.553047404063
0.932901101163
0.694423165313
0
validation finish
batch start
#iterations: 100
currently lose_sum: 585.9171085357666
time_elpased: 5.501
batch start
#iterations: 101
currently lose_sum: 587.2919031381607
time_elpased: 5.857
batch start
#iterations: 102
currently lose_sum: 585.1311041116714
time_elpased: 5.477
batch start
#iterations: 103
currently lose_sum: 588.0137221217155
time_elpased: 5.771
batch start
#iterations: 104
currently lose_sum: 588.2865447998047
time_elpased: 5.453
batch start
#iterations: 105
currently lose_sum: 585.5557214021683
time_elpased: 5.885
batch start
#iterations: 106
currently lose_sum: 586.625908613205
time_elpased: 5.864
batch start
#iterations: 107
currently lose_sum: 585.1426410079002
time_elpased: 5.497
batch start
#iterations: 108
currently lose_sum: 588.2742000222206
time_elpased: 5.647
batch start
#iterations: 109
currently lose_sum: 586.8120185732841
time_elpased: 5.551
batch start
#iterations: 110
currently lose_sum: 585.8355878591537
time_elpased: 5.515
batch start
#iterations: 111
currently lose_sum: 585.2531195878983
time_elpased: 5.765
batch start
#iterations: 112
currently lose_sum: 587.3493701219559
time_elpased: 5.811
batch start
#iterations: 113
currently lose_sum: 586.4664512872696
time_elpased: 5.683
batch start
#iterations: 114
currently lose_sum: 585.7547620534897
time_elpased: 5.9
batch start
#iterations: 115
currently lose_sum: 585.1480492949486
time_elpased: 5.77
batch start
#iterations: 116
currently lose_sum: 586.5465587377548
time_elpased: 5.69
batch start
#iterations: 117
currently lose_sum: 586.1027104258537
time_elpased: 5.849
batch start
#iterations: 118
currently lose_sum: 585.6616189479828
time_elpased: 5.497
batch start
#iterations: 119
currently lose_sum: 588.3430860042572
time_elpased: 5.729
start validation test
0.559307479224
0.554232437921
0.926829268293
0.693662988851
0
validation finish
batch start
#iterations: 120
currently lose_sum: 586.9875828623772
time_elpased: 5.671
batch start
#iterations: 121
currently lose_sum: 586.3667275309563
time_elpased: 5.682
batch start
#iterations: 122
currently lose_sum: 585.731557905674
time_elpased: 5.798
batch start
#iterations: 123
currently lose_sum: 586.0897129178047
time_elpased: 5.493
batch start
#iterations: 124
currently lose_sum: 585.7393642663956
time_elpased: 5.844
batch start
#iterations: 125
currently lose_sum: 585.8667584061623
time_elpased: 5.476
batch start
#iterations: 126
currently lose_sum: 585.3949082493782
time_elpased: 5.848
batch start
#iterations: 127
currently lose_sum: 585.4164302349091
time_elpased: 5.457
batch start
#iterations: 128
currently lose_sum: 587.6255090534687
time_elpased: 5.818
batch start
#iterations: 129
currently lose_sum: 587.3438705801964
time_elpased: 5.696
batch start
#iterations: 130
currently lose_sum: 587.0835944414139
time_elpased: 5.713
batch start
#iterations: 131
currently lose_sum: 587.0678504705429
time_elpased: 5.581
batch start
#iterations: 132
currently lose_sum: 587.2247229814529
time_elpased: 5.702
batch start
#iterations: 133
currently lose_sum: 583.7852929830551
time_elpased: 5.855
batch start
#iterations: 134
currently lose_sum: 586.5067055225372
time_elpased: 5.691
batch start
#iterations: 135
currently lose_sum: 585.8737059235573
time_elpased: 5.616
batch start
#iterations: 136
currently lose_sum: 585.0735594630241
time_elpased: 5.7
batch start
#iterations: 137
currently lose_sum: 585.9105309247971
time_elpased: 5.73
batch start
#iterations: 138
currently lose_sum: 587.291590988636
time_elpased: 5.802
batch start
#iterations: 139
currently lose_sum: 586.316662967205
time_elpased: 5.48
start validation test
0.557783933518
0.553256799067
0.927446742822
0.693070829809
0
validation finish
batch start
#iterations: 140
currently lose_sum: 585.036184668541
time_elpased: 5.682
batch start
#iterations: 141
currently lose_sum: 584.4442191123962
time_elpased: 5.721
batch start
#iterations: 142
currently lose_sum: 585.9183079600334
time_elpased: 5.614
batch start
#iterations: 143
currently lose_sum: 587.2612897753716
time_elpased: 5.707
batch start
#iterations: 144
currently lose_sum: 586.4956939220428
time_elpased: 5.607
batch start
#iterations: 145
currently lose_sum: 584.5279662013054
time_elpased: 5.934
batch start
#iterations: 146
currently lose_sum: 584.8940851688385
time_elpased: 5.929
batch start
#iterations: 147
currently lose_sum: 583.8823788762093
time_elpased: 5.5
batch start
#iterations: 148
currently lose_sum: 586.9129791855812
time_elpased: 5.807
batch start
#iterations: 149
currently lose_sum: 583.3875470757484
time_elpased: 5.653
batch start
#iterations: 150
currently lose_sum: 586.1924005150795
time_elpased: 5.772
batch start
#iterations: 151
currently lose_sum: 585.4415447413921
time_elpased: 5.333
batch start
#iterations: 152
currently lose_sum: 585.5857334733009
time_elpased: 5.711
batch start
#iterations: 153
currently lose_sum: 583.5455158352852
time_elpased: 5.866
batch start
#iterations: 154
currently lose_sum: 585.1264620423317
time_elpased: 5.832
batch start
#iterations: 155
currently lose_sum: 587.4088794589043
time_elpased: 5.86
batch start
#iterations: 156
currently lose_sum: 585.8969112634659
time_elpased: 5.505
batch start
#iterations: 157
currently lose_sum: 584.7264566421509
time_elpased: 5.765
batch start
#iterations: 158
currently lose_sum: 586.2966787815094
time_elpased: 5.71
batch start
#iterations: 159
currently lose_sum: 583.0808245539665
time_elpased: 5.676
start validation test
0.558531855956
0.553914464556
0.924359370176
0.69272148848
0
validation finish
batch start
#iterations: 160
currently lose_sum: 586.0141414403915
time_elpased: 5.8
batch start
#iterations: 161
currently lose_sum: 586.5880935192108
time_elpased: 5.601
batch start
#iterations: 162
currently lose_sum: 586.2721076011658
time_elpased: 5.715
batch start
#iterations: 163
currently lose_sum: 584.745064496994
time_elpased: 5.695
batch start
#iterations: 164
currently lose_sum: 587.216475725174
time_elpased: 5.731
batch start
#iterations: 165
currently lose_sum: 585.105369925499
time_elpased: 5.63
batch start
#iterations: 166
currently lose_sum: 584.3983510732651
time_elpased: 5.704
batch start
#iterations: 167
currently lose_sum: 585.9026223421097
time_elpased: 5.781
batch start
#iterations: 168
currently lose_sum: 585.677100777626
time_elpased: 5.549
batch start
#iterations: 169
currently lose_sum: 584.2641357183456
time_elpased: 5.775
batch start
#iterations: 170
currently lose_sum: 584.7158609628677
time_elpased: 6.057
batch start
#iterations: 171
currently lose_sum: 585.1521750092506
time_elpased: 5.519
batch start
#iterations: 172
currently lose_sum: 583.7850939035416
time_elpased: 5.763
batch start
#iterations: 173
currently lose_sum: 583.6132256984711
time_elpased: 5.828
batch start
#iterations: 174
currently lose_sum: 583.6729091405869
time_elpased: 5.702
batch start
#iterations: 175
currently lose_sum: 585.8847662806511
time_elpased: 5.946
batch start
#iterations: 176
currently lose_sum: 586.282320022583
time_elpased: 5.616
batch start
#iterations: 177
currently lose_sum: 585.8072868585587
time_elpased: 5.723
batch start
#iterations: 178
currently lose_sum: 583.1825882792473
time_elpased: 5.672
batch start
#iterations: 179
currently lose_sum: 585.4993392825127
time_elpased: 5.662
start validation test
0.55836565097
0.554267060469
0.917361325512
0.691021143821
0
validation finish
batch start
#iterations: 180
currently lose_sum: 583.838329911232
time_elpased: 5.6
batch start
#iterations: 181
currently lose_sum: 584.2716810703278
time_elpased: 5.65
batch start
#iterations: 182
currently lose_sum: 586.2605636715889
time_elpased: 5.506
batch start
#iterations: 183
currently lose_sum: 585.6164563894272
time_elpased: 5.476
batch start
#iterations: 184
currently lose_sum: 584.8857232928276
time_elpased: 5.749
batch start
#iterations: 185
currently lose_sum: 586.4027547836304
time_elpased: 5.688
batch start
#iterations: 186
currently lose_sum: 585.5428993701935
time_elpased: 5.594
batch start
#iterations: 187
currently lose_sum: 585.338425040245
time_elpased: 5.513
batch start
#iterations: 188
currently lose_sum: 583.2713236808777
time_elpased: 5.801
batch start
#iterations: 189
currently lose_sum: 584.6009291410446
time_elpased: 5.785
batch start
#iterations: 190
currently lose_sum: 585.4047901034355
time_elpased: 5.596
batch start
#iterations: 191
currently lose_sum: 585.7384662628174
time_elpased: 5.6
batch start
#iterations: 192
currently lose_sum: 584.100957095623
time_elpased: 5.659
batch start
#iterations: 193
currently lose_sum: 584.6422008872032
time_elpased: 5.919
batch start
#iterations: 194
currently lose_sum: 584.1090204119682
time_elpased: 5.611
batch start
#iterations: 195
currently lose_sum: 584.8991787433624
time_elpased: 5.81
batch start
#iterations: 196
currently lose_sum: 584.9350844621658
time_elpased: 5.889
batch start
#iterations: 197
currently lose_sum: 585.8027123808861
time_elpased: 5.54
batch start
#iterations: 198
currently lose_sum: 584.9288234710693
time_elpased: 5.766
batch start
#iterations: 199
currently lose_sum: 583.1733847856522
time_elpased: 5.802
start validation test
0.55783933518
0.553408811223
0.925594319234
0.692672032038
0
validation finish
batch start
#iterations: 200
currently lose_sum: 583.9807296395302
time_elpased: 5.601
batch start
#iterations: 201
currently lose_sum: 583.6016017198563
time_elpased: 5.56
batch start
#iterations: 202
currently lose_sum: 584.7638259530067
time_elpased: 5.707
batch start
#iterations: 203
currently lose_sum: 585.1516810059547
time_elpased: 6.098
batch start
#iterations: 204
currently lose_sum: 584.8599237799644
time_elpased: 5.988
batch start
#iterations: 205
currently lose_sum: 585.7753618359566
time_elpased: 5.751
batch start
#iterations: 206
currently lose_sum: 584.4774888157845
time_elpased: 5.72
batch start
#iterations: 207
currently lose_sum: 586.5001661777496
time_elpased: 5.917
batch start
#iterations: 208
currently lose_sum: 584.0893243551254
time_elpased: 5.72
batch start
#iterations: 209
currently lose_sum: 582.6253605484962
time_elpased: 5.741
batch start
#iterations: 210
currently lose_sum: 584.8716753125191
time_elpased: 5.82
batch start
#iterations: 211
currently lose_sum: 586.7296971082687
time_elpased: 5.497
batch start
#iterations: 212
currently lose_sum: 582.0573952794075
time_elpased: 5.7
batch start
#iterations: 213
currently lose_sum: 585.1041829586029
time_elpased: 5.566
batch start
#iterations: 214
currently lose_sum: 585.4303936958313
time_elpased: 5.659
batch start
#iterations: 215
currently lose_sum: 585.1008265018463
time_elpased: 5.609
batch start
#iterations: 216
currently lose_sum: 582.8066453337669
time_elpased: 5.776
batch start
#iterations: 217
currently lose_sum: 585.2364349365234
time_elpased: 5.388
batch start
#iterations: 218
currently lose_sum: 584.8795289993286
time_elpased: 5.446
batch start
#iterations: 219
currently lose_sum: 584.6275106072426
time_elpased: 5.836
start validation test
0.558614958449
0.554005678311
0.923741895647
0.692619314017
0
validation finish
batch start
#iterations: 220
currently lose_sum: 584.5188696980476
time_elpased: 5.841
batch start
#iterations: 221
currently lose_sum: 584.9932345151901
time_elpased: 5.405
batch start
#iterations: 222
currently lose_sum: 584.0157951116562
time_elpased: 5.703
batch start
#iterations: 223
currently lose_sum: 585.9120374917984
time_elpased: 5.506
batch start
#iterations: 224
currently lose_sum: 584.6320860981941
time_elpased: 5.941
batch start
#iterations: 225
currently lose_sum: 584.5292848944664
time_elpased: 5.936
batch start
#iterations: 226
currently lose_sum: 585.3897742629051
time_elpased: 5.478
batch start
#iterations: 227
currently lose_sum: 585.5851253867149
time_elpased: 5.826
batch start
#iterations: 228
currently lose_sum: 584.1748350858688
time_elpased: 5.567
batch start
#iterations: 229
currently lose_sum: 586.3291507363319
time_elpased: 5.787
batch start
#iterations: 230
currently lose_sum: 584.7673994898796
time_elpased: 5.513
batch start
#iterations: 231
currently lose_sum: 584.3922211527824
time_elpased: 5.449
batch start
#iterations: 232
currently lose_sum: 584.6851304769516
time_elpased: 5.817
batch start
#iterations: 233
currently lose_sum: 585.0503823161125
time_elpased: 5.707
batch start
#iterations: 234
currently lose_sum: 584.8690156340599
time_elpased: 5.477
batch start
#iterations: 235
currently lose_sum: 584.3860818743706
time_elpased: 5.737
batch start
#iterations: 236
currently lose_sum: 584.9854860007763
time_elpased: 5.782
batch start
#iterations: 237
currently lose_sum: 583.3089069128036
time_elpased: 5.542
batch start
#iterations: 238
currently lose_sum: 584.6987956166267
time_elpased: 5.798
batch start
#iterations: 239
currently lose_sum: 584.2901580929756
time_elpased: 5.67
start validation test
0.558060941828
0.553558236887
0.925388494391
0.6927314048
0
validation finish
batch start
#iterations: 240
currently lose_sum: 581.9653540253639
time_elpased: 5.687
batch start
#iterations: 241
currently lose_sum: 585.4017643332481
time_elpased: 5.649
batch start
#iterations: 242
currently lose_sum: 583.1313514709473
time_elpased: 5.444
batch start
#iterations: 243
currently lose_sum: 583.8213905096054
time_elpased: 5.543
batch start
#iterations: 244
currently lose_sum: 584.451220035553
time_elpased: 5.712
batch start
#iterations: 245
currently lose_sum: 584.1625666618347
time_elpased: 5.786
batch start
#iterations: 246
currently lose_sum: 583.5098813176155
time_elpased: 5.533
batch start
#iterations: 247
currently lose_sum: 583.9612008333206
time_elpased: 5.509
batch start
#iterations: 248
currently lose_sum: 584.5100286602974
time_elpased: 5.877
batch start
#iterations: 249
currently lose_sum: 582.323406457901
time_elpased: 5.614
batch start
#iterations: 250
currently lose_sum: 584.9692668318748
time_elpased: 5.597
batch start
#iterations: 251
currently lose_sum: 583.7321697473526
time_elpased: 5.899
batch start
#iterations: 252
currently lose_sum: 584.1436088085175
time_elpased: 5.745
batch start
#iterations: 253
currently lose_sum: 584.2269788384438
time_elpased: 5.514
batch start
#iterations: 254
currently lose_sum: 581.9643579721451
time_elpased: 5.32
batch start
#iterations: 255
currently lose_sum: 582.690535902977
time_elpased: 5.701
batch start
#iterations: 256
currently lose_sum: 583.0709646344185
time_elpased: 5.614
batch start
#iterations: 257
currently lose_sum: 583.1516919732094
time_elpased: 5.547
batch start
#iterations: 258
currently lose_sum: 584.3535708189011
time_elpased: 5.619
batch start
#iterations: 259
currently lose_sum: 582.7958087325096
time_elpased: 5.892
start validation test
0.558448753463
0.553876518843
0.924153545333
0.692634014655
0
validation finish
batch start
#iterations: 260
currently lose_sum: 585.4592051506042
time_elpased: 5.811
batch start
#iterations: 261
currently lose_sum: 585.6989399194717
time_elpased: 5.719
batch start
#iterations: 262
currently lose_sum: 586.519680082798
time_elpased: 5.597
batch start
#iterations: 263
currently lose_sum: 582.9969317317009
time_elpased: 5.461
batch start
#iterations: 264
currently lose_sum: 584.7369583845139
time_elpased: 5.779
batch start
#iterations: 265
currently lose_sum: 585.1893720626831
time_elpased: 5.484
batch start
#iterations: 266
currently lose_sum: 584.3492298126221
time_elpased: 5.455
batch start
#iterations: 267
currently lose_sum: 584.6292503476143
time_elpased: 5.663
batch start
#iterations: 268
currently lose_sum: 583.0585317611694
time_elpased: 5.628
batch start
#iterations: 269
currently lose_sum: 584.041942179203
time_elpased: 5.621
batch start
#iterations: 270
currently lose_sum: 584.5918513536453
time_elpased: 5.694
batch start
#iterations: 271
currently lose_sum: 584.1068531870842
time_elpased: 5.693
batch start
#iterations: 272
currently lose_sum: 583.1465772986412
time_elpased: 5.906
batch start
#iterations: 273
currently lose_sum: 584.1179239153862
time_elpased: 5.672
batch start
#iterations: 274
currently lose_sum: 582.9800253510475
time_elpased: 5.66
batch start
#iterations: 275
currently lose_sum: 585.3503620624542
time_elpased: 5.804
batch start
#iterations: 276
currently lose_sum: 583.7154781222343
time_elpased: 5.534
batch start
#iterations: 277
currently lose_sum: 584.535609126091
time_elpased: 5.556
batch start
#iterations: 278
currently lose_sum: 585.7567193508148
time_elpased: 5.567
batch start
#iterations: 279
currently lose_sum: 581.7139815092087
time_elpased: 5.41
start validation test
0.558531855956
0.553630034046
0.928784604302
0.693737148567
0
validation finish
batch start
#iterations: 280
currently lose_sum: 583.4169997572899
time_elpased: 5.66
batch start
#iterations: 281
currently lose_sum: 584.2646697759628
time_elpased: 5.595
batch start
#iterations: 282
currently lose_sum: 585.1937810778618
time_elpased: 5.787
batch start
#iterations: 283
currently lose_sum: 584.0044394731522
time_elpased: 5.896
batch start
#iterations: 284
currently lose_sum: 580.5076665878296
time_elpased: 5.797
batch start
#iterations: 285
currently lose_sum: 583.1621805429459
time_elpased: 5.555
batch start
#iterations: 286
currently lose_sum: 583.9938353300095
time_elpased: 5.337
batch start
#iterations: 287
currently lose_sum: 585.0910422801971
time_elpased: 5.876
batch start
#iterations: 288
currently lose_sum: 585.8805402517319
time_elpased: 5.785
batch start
#iterations: 289
currently lose_sum: 583.3719057440758
time_elpased: 5.614
batch start
#iterations: 290
currently lose_sum: 585.5476574897766
time_elpased: 5.62
batch start
#iterations: 291
currently lose_sum: 586.05544257164
time_elpased: 5.721
batch start
#iterations: 292
currently lose_sum: 583.1813113689423
time_elpased: 5.533
batch start
#iterations: 293
currently lose_sum: 581.0940954685211
time_elpased: 5.797
batch start
#iterations: 294
currently lose_sum: 584.4928404688835
time_elpased: 5.887
batch start
#iterations: 295
currently lose_sum: 582.8361310362816
time_elpased: 5.59
batch start
#iterations: 296
currently lose_sum: 583.5774177908897
time_elpased: 5.667
batch start
#iterations: 297
currently lose_sum: 583.6356555223465
time_elpased: 5.444
batch start
#iterations: 298
currently lose_sum: 583.6615232229233
time_elpased: 5.744
batch start
#iterations: 299
currently lose_sum: 581.2424488663673
time_elpased: 5.429
start validation test
0.558781163435
0.553697237028
0.930122465782
0.694162826421
0
validation finish
batch start
#iterations: 300
currently lose_sum: 583.4580320119858
time_elpased: 5.646
batch start
#iterations: 301
currently lose_sum: 584.668240904808
time_elpased: 5.929
batch start
#iterations: 302
currently lose_sum: 583.5232414603233
time_elpased: 5.41
batch start
#iterations: 303
currently lose_sum: 584.7185846567154
time_elpased: 5.835
batch start
#iterations: 304
currently lose_sum: 582.5805411934853
time_elpased: 5.47
batch start
#iterations: 305
currently lose_sum: 585.0435090661049
time_elpased: 5.781
batch start
#iterations: 306
currently lose_sum: 582.7684996724129
time_elpased: 5.707
batch start
#iterations: 307
currently lose_sum: 583.5299924612045
time_elpased: 5.675
batch start
#iterations: 308
currently lose_sum: 584.4976986050606
time_elpased: 5.535
batch start
#iterations: 309
currently lose_sum: 583.1615698337555
time_elpased: 5.592
batch start
#iterations: 310
currently lose_sum: 583.8809461593628
time_elpased: 5.421
batch start
#iterations: 311
currently lose_sum: 581.3453550338745
time_elpased: 5.854
batch start
#iterations: 312
currently lose_sum: 582.6970472931862
time_elpased: 5.482
batch start
#iterations: 313
currently lose_sum: 582.552003622055
time_elpased: 5.675
batch start
#iterations: 314
currently lose_sum: 583.3585241436958
time_elpased: 5.537
batch start
#iterations: 315
currently lose_sum: 582.3016082644463
time_elpased: 5.876
batch start
#iterations: 316
currently lose_sum: 584.8259292244911
time_elpased: 5.422
batch start
#iterations: 317
currently lose_sum: 583.5042213201523
time_elpased: 5.791
batch start
#iterations: 318
currently lose_sum: 584.0858719944954
time_elpased: 5.763
batch start
#iterations: 319
currently lose_sum: 583.1013272404671
time_elpased: 5.61
start validation test
0.558421052632
0.553680651606
0.926932180714
0.69325944313
0
validation finish
batch start
#iterations: 320
currently lose_sum: 583.462352335453
time_elpased: 5.921
batch start
#iterations: 321
currently lose_sum: 581.9952884912491
time_elpased: 5.747
batch start
#iterations: 322
currently lose_sum: 582.4901055693626
time_elpased: 5.8
batch start
#iterations: 323
currently lose_sum: 583.9024649262428
time_elpased: 5.805
batch start
#iterations: 324
currently lose_sum: 584.1764731407166
time_elpased: 5.487
batch start
#iterations: 325
currently lose_sum: 584.282245695591
time_elpased: 5.695
batch start
#iterations: 326
currently lose_sum: 584.2420817613602
time_elpased: 5.435
batch start
#iterations: 327
currently lose_sum: 583.5671961903572
time_elpased: 5.785
batch start
#iterations: 328
currently lose_sum: 583.316282749176
time_elpased: 5.478
batch start
#iterations: 329
currently lose_sum: 583.7348585128784
time_elpased: 5.868
batch start
#iterations: 330
currently lose_sum: 583.0376003980637
time_elpased: 5.444
batch start
#iterations: 331
currently lose_sum: 583.0859850645065
time_elpased: 5.943
batch start
#iterations: 332
currently lose_sum: 583.8880358934402
time_elpased: 5.954
batch start
#iterations: 333
currently lose_sum: 582.6692820191383
time_elpased: 5.473
batch start
#iterations: 334
currently lose_sum: 583.5118761658669
time_elpased: 5.889
batch start
#iterations: 335
currently lose_sum: 582.179958820343
time_elpased: 5.413
batch start
#iterations: 336
currently lose_sum: 584.0161724090576
time_elpased: 5.862
batch start
#iterations: 337
currently lose_sum: 584.1561710238457
time_elpased: 5.471
batch start
#iterations: 338
currently lose_sum: 583.1986035108566
time_elpased: 5.8
batch start
#iterations: 339
currently lose_sum: 583.3540875315666
time_elpased: 5.665
start validation test
0.557950138504
0.55378140859
0.920860347844
0.691632850242
0
validation finish
batch start
#iterations: 340
currently lose_sum: 583.7233228683472
time_elpased: 5.603
batch start
#iterations: 341
currently lose_sum: 582.7829475402832
time_elpased: 5.846
batch start
#iterations: 342
currently lose_sum: 583.1125048100948
time_elpased: 5.75
batch start
#iterations: 343
currently lose_sum: 584.6331455111504
time_elpased: 5.583
batch start
#iterations: 344
currently lose_sum: 583.1594825387001
time_elpased: 5.839
batch start
#iterations: 345
currently lose_sum: 582.6072042584419
time_elpased: 5.607
batch start
#iterations: 346
currently lose_sum: 584.2802865505219
time_elpased: 5.642
batch start
#iterations: 347
currently lose_sum: 584.7083342671394
time_elpased: 5.804
batch start
#iterations: 348
currently lose_sum: 584.7308062911034
time_elpased: 5.574
batch start
#iterations: 349
currently lose_sum: 583.681346654892
time_elpased: 5.873
batch start
#iterations: 350
currently lose_sum: 582.6349968314171
time_elpased: 5.535
batch start
#iterations: 351
currently lose_sum: 582.7950139045715
time_elpased: 5.879
batch start
#iterations: 352
currently lose_sum: 584.2913382053375
time_elpased: 5.464
batch start
#iterations: 353
currently lose_sum: 583.7316990494728
time_elpased: 5.781
batch start
#iterations: 354
currently lose_sum: 585.7329533100128
time_elpased: 5.539
batch start
#iterations: 355
currently lose_sum: 582.1118793487549
time_elpased: 5.65
batch start
#iterations: 356
currently lose_sum: 583.6924765110016
time_elpased: 5.442
batch start
#iterations: 357
currently lose_sum: 583.640885591507
time_elpased: 5.862
batch start
#iterations: 358
currently lose_sum: 584.1790153980255
time_elpased: 5.728
batch start
#iterations: 359
currently lose_sum: 584.032911002636
time_elpased: 5.545
start validation test
0.557950138504
0.553556021201
0.924359370176
0.692441120919
0
validation finish
batch start
#iterations: 360
currently lose_sum: 583.2351093888283
time_elpased: 5.84
batch start
#iterations: 361
currently lose_sum: 584.0942636728287
time_elpased: 5.72
batch start
#iterations: 362
currently lose_sum: 584.3489900827408
time_elpased: 5.646
batch start
#iterations: 363
currently lose_sum: 583.3559458255768
time_elpased: 5.719
batch start
#iterations: 364
currently lose_sum: 582.7926294207573
time_elpased: 5.674
batch start
#iterations: 365
currently lose_sum: 582.674142062664
time_elpased: 5.993
batch start
#iterations: 366
currently lose_sum: 581.8730252981186
time_elpased: 5.745
batch start
#iterations: 367
currently lose_sum: 581.2504628896713
time_elpased: 5.563
batch start
#iterations: 368
currently lose_sum: 581.9774225950241
time_elpased: 5.881
batch start
#iterations: 369
currently lose_sum: 584.093266248703
time_elpased: 5.599
batch start
#iterations: 370
currently lose_sum: 582.847633421421
time_elpased: 5.684
batch start
#iterations: 371
currently lose_sum: 583.3013754487038
time_elpased: 5.802
batch start
#iterations: 372
currently lose_sum: 585.4159364700317
time_elpased: 5.679
batch start
#iterations: 373
currently lose_sum: 582.5699250102043
time_elpased: 5.563
batch start
#iterations: 374
currently lose_sum: 583.762083530426
time_elpased: 5.588
batch start
#iterations: 375
currently lose_sum: 584.395387172699
time_elpased: 5.924
batch start
#iterations: 376
currently lose_sum: 581.626606464386
time_elpased: 5.504
batch start
#iterations: 377
currently lose_sum: 582.2859496474266
time_elpased: 6.016
batch start
#iterations: 378
currently lose_sum: 585.2655389904976
time_elpased: 5.851
batch start
#iterations: 379
currently lose_sum: 583.4903059601784
time_elpased: 5.501
start validation test
0.558781163435
0.554282529262
0.921066172687
0.692081657903
0
validation finish
batch start
#iterations: 380
currently lose_sum: 582.8271590471268
time_elpased: 5.72
batch start
#iterations: 381
currently lose_sum: 584.8871285915375
time_elpased: 5.889
batch start
#iterations: 382
currently lose_sum: 581.6462209820747
time_elpased: 5.494
batch start
#iterations: 383
currently lose_sum: 582.7093186974525
time_elpased: 5.885
batch start
#iterations: 384
currently lose_sum: 583.9360465407372
time_elpased: 5.692
batch start
#iterations: 385
currently lose_sum: 584.6063113212585
time_elpased: 5.793
batch start
#iterations: 386
currently lose_sum: 582.2131653428078
time_elpased: 5.818
batch start
#iterations: 387
currently lose_sum: 583.1163294911385
time_elpased: 5.615
batch start
#iterations: 388
currently lose_sum: 583.2042329907417
time_elpased: 5.831
batch start
#iterations: 389
currently lose_sum: 583.840069770813
time_elpased: 5.704
batch start
#iterations: 390
currently lose_sum: 583.641974568367
time_elpased: 5.786
batch start
#iterations: 391
currently lose_sum: 583.0760787129402
time_elpased: 5.896
batch start
#iterations: 392
currently lose_sum: 582.9520329833031
time_elpased: 5.536
batch start
#iterations: 393
currently lose_sum: 582.4961788654327
time_elpased: 5.655
batch start
#iterations: 394
currently lose_sum: 583.2906421124935
time_elpased: 5.456
batch start
#iterations: 395
currently lose_sum: 583.5206313729286
time_elpased: 5.846
batch start
#iterations: 396
currently lose_sum: 583.8869135379791
time_elpased: 5.688
batch start
#iterations: 397
currently lose_sum: 580.2852612137794
time_elpased: 5.756
batch start
#iterations: 398
currently lose_sum: 583.5986020565033
time_elpased: 5.841
batch start
#iterations: 399
currently lose_sum: 584.6200715303421
time_elpased: 5.873
start validation test
0.558282548476
0.554013875124
0.920448698158
0.691697923514
0
validation finish
acc: 0.558
pre: 0.553
rec: 0.936
F1: 0.695
auc: 0.000
