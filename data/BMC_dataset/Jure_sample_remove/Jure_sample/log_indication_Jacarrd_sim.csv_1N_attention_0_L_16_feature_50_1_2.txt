start to construct graph
graph construct over
93050
epochs start
batch start
#iterations: 0
currently lose_sum: 604.2200461626053
time_elpased: 1.822
batch start
#iterations: 1
currently lose_sum: 599.1007103919983
time_elpased: 1.788
batch start
#iterations: 2
currently lose_sum: 598.1649816036224
time_elpased: 1.845
batch start
#iterations: 3
currently lose_sum: 597.575410425663
time_elpased: 1.791
batch start
#iterations: 4
currently lose_sum: 596.6751518845558
time_elpased: 1.803
batch start
#iterations: 5
currently lose_sum: 596.1249825358391
time_elpased: 1.813
batch start
#iterations: 6
currently lose_sum: 593.8195301294327
time_elpased: 1.812
batch start
#iterations: 7
currently lose_sum: 596.0578364133835
time_elpased: 1.819
batch start
#iterations: 8
currently lose_sum: 593.4076997041702
time_elpased: 1.808
batch start
#iterations: 9
currently lose_sum: 593.1593257188797
time_elpased: 1.812
batch start
#iterations: 10
currently lose_sum: 592.363235116005
time_elpased: 1.812
batch start
#iterations: 11
currently lose_sum: 591.3755411505699
time_elpased: 1.814
batch start
#iterations: 12
currently lose_sum: 592.1057749390602
time_elpased: 1.854
batch start
#iterations: 13
currently lose_sum: 592.4380039572716
time_elpased: 1.809
batch start
#iterations: 14
currently lose_sum: 590.4698684811592
time_elpased: 1.815
batch start
#iterations: 15
currently lose_sum: 592.9761936068535
time_elpased: 1.796
batch start
#iterations: 16
currently lose_sum: 591.567479968071
time_elpased: 1.817
batch start
#iterations: 17
currently lose_sum: 591.8311342000961
time_elpased: 1.836
batch start
#iterations: 18
currently lose_sum: 588.0306749939919
time_elpased: 1.83
batch start
#iterations: 19
currently lose_sum: 589.2099900841713
time_elpased: 1.858
start validation test
0.58364640884
0.570514064016
0.907996295153
0.700738622826
0
validation finish
batch start
#iterations: 20
currently lose_sum: 588.9522352814674
time_elpased: 1.892
batch start
#iterations: 21
currently lose_sum: 588.5970731973648
time_elpased: 1.85
batch start
#iterations: 22
currently lose_sum: 588.0786458849907
time_elpased: 1.818
batch start
#iterations: 23
currently lose_sum: 591.1796703338623
time_elpased: 1.813
batch start
#iterations: 24
currently lose_sum: 588.2338635325432
time_elpased: 1.868
batch start
#iterations: 25
currently lose_sum: 590.077162861824
time_elpased: 1.876
batch start
#iterations: 26
currently lose_sum: 588.9297136068344
time_elpased: 1.883
batch start
#iterations: 27
currently lose_sum: 586.8327683806419
time_elpased: 1.823
batch start
#iterations: 28
currently lose_sum: 585.7155179977417
time_elpased: 1.816
batch start
#iterations: 29
currently lose_sum: 587.9040687084198
time_elpased: 1.826
batch start
#iterations: 30
currently lose_sum: 588.3454958200455
time_elpased: 1.842
batch start
#iterations: 31
currently lose_sum: 587.4647016525269
time_elpased: 1.816
batch start
#iterations: 32
currently lose_sum: 589.6421821713448
time_elpased: 1.829
batch start
#iterations: 33
currently lose_sum: 589.2971882820129
time_elpased: 1.814
batch start
#iterations: 34
currently lose_sum: 586.795432984829
time_elpased: 1.874
batch start
#iterations: 35
currently lose_sum: 587.0754924416542
time_elpased: 1.867
batch start
#iterations: 36
currently lose_sum: 587.2264902591705
time_elpased: 1.827
batch start
#iterations: 37
currently lose_sum: 588.7159823179245
time_elpased: 1.859
batch start
#iterations: 38
currently lose_sum: 586.1551985144615
time_elpased: 1.857
batch start
#iterations: 39
currently lose_sum: 586.4781876802444
time_elpased: 1.833
start validation test
0.583453038674
0.569664395175
0.916229288875
0.702531021286
0
validation finish
batch start
#iterations: 40
currently lose_sum: 588.2712019979954
time_elpased: 1.853
batch start
#iterations: 41
currently lose_sum: 587.9707441329956
time_elpased: 1.82
batch start
#iterations: 42
currently lose_sum: 587.7119778394699
time_elpased: 1.848
batch start
#iterations: 43
currently lose_sum: 587.3799656629562
time_elpased: 1.829
batch start
#iterations: 44
currently lose_sum: 587.8515769541264
time_elpased: 1.844
batch start
#iterations: 45
currently lose_sum: 587.7177506685257
time_elpased: 1.809
batch start
#iterations: 46
currently lose_sum: 587.3811340332031
time_elpased: 1.819
batch start
#iterations: 47
currently lose_sum: 586.7276219129562
time_elpased: 1.811
batch start
#iterations: 48
currently lose_sum: 590.7068008780479
time_elpased: 1.817
batch start
#iterations: 49
currently lose_sum: 586.6700428724289
time_elpased: 1.791
batch start
#iterations: 50
currently lose_sum: 586.0487984418869
time_elpased: 1.815
batch start
#iterations: 51
currently lose_sum: 585.4198540449142
time_elpased: 1.843
batch start
#iterations: 52
currently lose_sum: 587.0033260583878
time_elpased: 1.824
batch start
#iterations: 53
currently lose_sum: 587.7986718416214
time_elpased: 1.798
batch start
#iterations: 54
currently lose_sum: 585.6937659978867
time_elpased: 1.808
batch start
#iterations: 55
currently lose_sum: 587.6463833451271
time_elpased: 1.765
batch start
#iterations: 56
currently lose_sum: 587.5925471782684
time_elpased: 1.827
batch start
#iterations: 57
currently lose_sum: 585.8840022683144
time_elpased: 1.848
batch start
#iterations: 58
currently lose_sum: 589.0847272276878
time_elpased: 1.822
batch start
#iterations: 59
currently lose_sum: 585.5197168588638
time_elpased: 1.799
start validation test
0.585055248619
0.570524498993
0.918493362149
0.703850476134
0
validation finish
batch start
#iterations: 60
currently lose_sum: 585.8655014634132
time_elpased: 1.813
batch start
#iterations: 61
currently lose_sum: 587.8994386792183
time_elpased: 1.829
batch start
#iterations: 62
currently lose_sum: 588.2197123765945
time_elpased: 1.828
batch start
#iterations: 63
currently lose_sum: 585.6443467736244
time_elpased: 1.827
batch start
#iterations: 64
currently lose_sum: 587.7834480404854
time_elpased: 1.833
batch start
#iterations: 65
currently lose_sum: 586.1589305400848
time_elpased: 1.837
batch start
#iterations: 66
currently lose_sum: 584.6994739770889
time_elpased: 1.824
batch start
#iterations: 67
currently lose_sum: 584.4634938240051
time_elpased: 1.804
batch start
#iterations: 68
currently lose_sum: 586.5341507792473
time_elpased: 1.811
batch start
#iterations: 69
currently lose_sum: 587.8579831719398
time_elpased: 1.797
batch start
#iterations: 70
currently lose_sum: 588.3488776683807
time_elpased: 1.836
batch start
#iterations: 71
currently lose_sum: 585.895385146141
time_elpased: 1.807
batch start
#iterations: 72
currently lose_sum: 587.8289040923119
time_elpased: 1.805
batch start
#iterations: 73
currently lose_sum: 588.0991334915161
time_elpased: 1.837
batch start
#iterations: 74
currently lose_sum: 587.3356154561043
time_elpased: 1.902
batch start
#iterations: 75
currently lose_sum: 586.0884150862694
time_elpased: 1.8
batch start
#iterations: 76
currently lose_sum: 585.4144107103348
time_elpased: 1.808
batch start
#iterations: 77
currently lose_sum: 585.4988332390785
time_elpased: 1.802
batch start
#iterations: 78
currently lose_sum: 588.7518276572227
time_elpased: 1.794
batch start
#iterations: 79
currently lose_sum: 584.2499002218246
time_elpased: 1.787
start validation test
0.582044198895
0.568187579214
0.922712771432
0.703298427266
0
validation finish
batch start
#iterations: 80
currently lose_sum: 586.59686845541
time_elpased: 1.832
batch start
#iterations: 81
currently lose_sum: 586.8069124817848
time_elpased: 1.803
batch start
#iterations: 82
currently lose_sum: 587.1513879299164
time_elpased: 1.794
batch start
#iterations: 83
currently lose_sum: 585.2250428199768
time_elpased: 1.786
batch start
#iterations: 84
currently lose_sum: 586.5380829572678
time_elpased: 1.806
batch start
#iterations: 85
currently lose_sum: 586.3712573647499
time_elpased: 1.808
batch start
#iterations: 86
currently lose_sum: 585.0094481110573
time_elpased: 1.826
batch start
#iterations: 87
currently lose_sum: 586.863032579422
time_elpased: 1.839
batch start
#iterations: 88
currently lose_sum: 585.4871249198914
time_elpased: 1.843
batch start
#iterations: 89
currently lose_sum: 585.2296671271324
time_elpased: 1.824
batch start
#iterations: 90
currently lose_sum: 586.6525459885597
time_elpased: 1.817
batch start
#iterations: 91
currently lose_sum: 586.5194426774979
time_elpased: 1.83
batch start
#iterations: 92
currently lose_sum: 586.1274100542068
time_elpased: 1.806
batch start
#iterations: 93
currently lose_sum: 586.1055245399475
time_elpased: 1.784
batch start
#iterations: 94
currently lose_sum: 586.3136004805565
time_elpased: 1.794
batch start
#iterations: 95
currently lose_sum: 586.3254733085632
time_elpased: 1.811
batch start
#iterations: 96
currently lose_sum: 587.1715928316116
time_elpased: 1.795
batch start
#iterations: 97
currently lose_sum: 586.7227334976196
time_elpased: 1.786
batch start
#iterations: 98
currently lose_sum: 585.1903765797615
time_elpased: 1.812
batch start
#iterations: 99
currently lose_sum: 586.3278945088387
time_elpased: 1.811
start validation test
0.583232044199
0.56838131194
0.929607903674
0.705441340128
0
validation finish
batch start
#iterations: 100
currently lose_sum: 587.5568406581879
time_elpased: 1.854
batch start
#iterations: 101
currently lose_sum: 587.3198547959328
time_elpased: 1.813
batch start
#iterations: 102
currently lose_sum: 583.7767629027367
time_elpased: 1.789
batch start
#iterations: 103
currently lose_sum: 588.1507068872452
time_elpased: 1.772
batch start
#iterations: 104
currently lose_sum: 585.774335026741
time_elpased: 1.796
batch start
#iterations: 105
currently lose_sum: 585.4980388879776
time_elpased: 1.822
batch start
#iterations: 106
currently lose_sum: 587.3120256662369
time_elpased: 1.806
batch start
#iterations: 107
currently lose_sum: 586.6455519199371
time_elpased: 1.785
batch start
#iterations: 108
currently lose_sum: 585.8771604299545
time_elpased: 1.812
batch start
#iterations: 109
currently lose_sum: 585.5071291327477
time_elpased: 1.813
batch start
#iterations: 110
currently lose_sum: 587.1883935332298
time_elpased: 1.816
batch start
#iterations: 111
currently lose_sum: 585.9992919564247
time_elpased: 1.777
batch start
#iterations: 112
currently lose_sum: 584.8143710494041
time_elpased: 1.812
batch start
#iterations: 113
currently lose_sum: 588.8938626646996
time_elpased: 1.778
batch start
#iterations: 114
currently lose_sum: 586.2195845842361
time_elpased: 1.796
batch start
#iterations: 115
currently lose_sum: 585.6584493517876
time_elpased: 1.78
batch start
#iterations: 116
currently lose_sum: 585.8432428836823
time_elpased: 1.816
batch start
#iterations: 117
currently lose_sum: 588.1028571724892
time_elpased: 1.793
batch start
#iterations: 118
currently lose_sum: 587.2125747799873
time_elpased: 1.799
batch start
#iterations: 119
currently lose_sum: 585.4121581912041
time_elpased: 1.793
start validation test
0.582486187845
0.567703109328
0.931974889369
0.705598192372
0
validation finish
batch start
#iterations: 120
currently lose_sum: 588.1425066292286
time_elpased: 1.787
batch start
#iterations: 121
currently lose_sum: 586.4196189641953
time_elpased: 1.806
batch start
#iterations: 122
currently lose_sum: 586.9541903734207
time_elpased: 1.836
batch start
#iterations: 123
currently lose_sum: 585.4409041404724
time_elpased: 1.826
batch start
#iterations: 124
currently lose_sum: 588.8540126681328
time_elpased: 1.871
batch start
#iterations: 125
currently lose_sum: 585.5450372695923
time_elpased: 1.818
batch start
#iterations: 126
currently lose_sum: 587.5203378796577
time_elpased: 1.825
batch start
#iterations: 127
currently lose_sum: 587.4024429321289
time_elpased: 1.812
batch start
#iterations: 128
currently lose_sum: 585.8515422344208
time_elpased: 1.809
batch start
#iterations: 129
currently lose_sum: 585.8657037615776
time_elpased: 1.812
batch start
#iterations: 130
currently lose_sum: 585.9373891949654
time_elpased: 1.806
batch start
#iterations: 131
currently lose_sum: 585.2858814001083
time_elpased: 1.817
batch start
#iterations: 132
currently lose_sum: 584.7404393553734
time_elpased: 1.808
batch start
#iterations: 133
currently lose_sum: 586.900658428669
time_elpased: 1.833
batch start
#iterations: 134
currently lose_sum: 588.2213467359543
time_elpased: 1.808
batch start
#iterations: 135
currently lose_sum: 586.9853028655052
time_elpased: 1.829
batch start
#iterations: 136
currently lose_sum: 584.9076111316681
time_elpased: 1.82
batch start
#iterations: 137
currently lose_sum: 586.2794381380081
time_elpased: 1.817
batch start
#iterations: 138
currently lose_sum: 587.7400888204575
time_elpased: 1.822
batch start
#iterations: 139
currently lose_sum: 587.2297545075417
time_elpased: 1.813
start validation test
0.584392265193
0.570096144632
0.918390449727
0.703494215723
0
validation finish
batch start
#iterations: 140
currently lose_sum: 585.9884028434753
time_elpased: 1.824
batch start
#iterations: 141
currently lose_sum: 586.631481051445
time_elpased: 1.81
batch start
#iterations: 142
currently lose_sum: 585.6938986778259
time_elpased: 1.808
batch start
#iterations: 143
currently lose_sum: 588.4750949144363
time_elpased: 1.81
batch start
#iterations: 144
currently lose_sum: 585.8004857301712
time_elpased: 1.794
batch start
#iterations: 145
currently lose_sum: 587.5154513120651
time_elpased: 1.798
batch start
#iterations: 146
currently lose_sum: 584.4092579483986
time_elpased: 1.82
batch start
#iterations: 147
currently lose_sum: 586.1407899260521
time_elpased: 1.802
batch start
#iterations: 148
currently lose_sum: 585.6291229724884
time_elpased: 1.789
batch start
#iterations: 149
currently lose_sum: 585.6916913986206
time_elpased: 1.787
batch start
#iterations: 150
currently lose_sum: 585.3565385341644
time_elpased: 1.794
batch start
#iterations: 151
currently lose_sum: 586.2060749828815
time_elpased: 1.783
batch start
#iterations: 152
currently lose_sum: 585.4755248427391
time_elpased: 1.797
batch start
#iterations: 153
currently lose_sum: 585.0842997431755
time_elpased: 1.804
batch start
#iterations: 154
currently lose_sum: 584.8777641654015
time_elpased: 1.796
batch start
#iterations: 155
currently lose_sum: 586.180064201355
time_elpased: 1.799
batch start
#iterations: 156
currently lose_sum: 586.2554159760475
time_elpased: 1.797
batch start
#iterations: 157
currently lose_sum: 584.9453774094582
time_elpased: 1.829
batch start
#iterations: 158
currently lose_sum: 585.3876116871834
time_elpased: 1.793
batch start
#iterations: 159
currently lose_sum: 585.5433847904205
time_elpased: 1.788
start validation test
0.583480662983
0.568784739768
0.926726355871
0.704919957728
0
validation finish
batch start
#iterations: 160
currently lose_sum: 586.977271437645
time_elpased: 1.807
batch start
#iterations: 161
currently lose_sum: 586.961437702179
time_elpased: 1.791
batch start
#iterations: 162
currently lose_sum: 585.5268566608429
time_elpased: 1.79
batch start
#iterations: 163
currently lose_sum: 585.7212657928467
time_elpased: 1.805
batch start
#iterations: 164
currently lose_sum: 585.6423872113228
time_elpased: 1.801
batch start
#iterations: 165
currently lose_sum: 586.8044705986977
time_elpased: 1.802
batch start
#iterations: 166
currently lose_sum: 586.2031156420708
time_elpased: 1.807
batch start
#iterations: 167
currently lose_sum: 585.8370411992073
time_elpased: 1.79
batch start
#iterations: 168
currently lose_sum: 587.5615219473839
time_elpased: 1.803
batch start
#iterations: 169
currently lose_sum: 585.1271433234215
time_elpased: 1.815
batch start
#iterations: 170
currently lose_sum: 584.7107067108154
time_elpased: 1.809
batch start
#iterations: 171
currently lose_sum: 584.8605434894562
time_elpased: 1.783
batch start
#iterations: 172
currently lose_sum: 586.6038796901703
time_elpased: 1.798
batch start
#iterations: 173
currently lose_sum: 586.0874083638191
time_elpased: 1.788
batch start
#iterations: 174
currently lose_sum: 585.1692578792572
time_elpased: 1.817
batch start
#iterations: 175
currently lose_sum: 586.3210178613663
time_elpased: 1.817
batch start
#iterations: 176
currently lose_sum: 586.0365034341812
time_elpased: 1.789
batch start
#iterations: 177
currently lose_sum: 584.8971022367477
time_elpased: 1.797
batch start
#iterations: 178
currently lose_sum: 585.246013879776
time_elpased: 1.802
batch start
#iterations: 179
currently lose_sum: 585.9530252218246
time_elpased: 1.8
start validation test
0.584613259669
0.569902387841
0.922301121745
0.704490517834
0
validation finish
batch start
#iterations: 180
currently lose_sum: 584.8518463969231
time_elpased: 1.81
batch start
#iterations: 181
currently lose_sum: 584.6925427913666
time_elpased: 1.801
batch start
#iterations: 182
currently lose_sum: 585.9492132663727
time_elpased: 1.794
batch start
#iterations: 183
currently lose_sum: 585.1786593198776
time_elpased: 1.792
batch start
#iterations: 184
currently lose_sum: 586.6376618146896
time_elpased: 1.81
batch start
#iterations: 185
currently lose_sum: 585.6892192363739
time_elpased: 1.816
batch start
#iterations: 186
currently lose_sum: 586.7633095383644
time_elpased: 1.814
batch start
#iterations: 187
currently lose_sum: 585.9141600131989
time_elpased: 1.81
batch start
#iterations: 188
currently lose_sum: 584.5386347770691
time_elpased: 1.836
batch start
#iterations: 189
currently lose_sum: 586.8964136838913
time_elpased: 1.824
batch start
#iterations: 190
currently lose_sum: 585.8422819972038
time_elpased: 1.827
batch start
#iterations: 191
currently lose_sum: 584.248085141182
time_elpased: 1.819
batch start
#iterations: 192
currently lose_sum: 586.1022616624832
time_elpased: 1.814
batch start
#iterations: 193
currently lose_sum: 586.1427462697029
time_elpased: 1.807
batch start
#iterations: 194
currently lose_sum: 584.0686144828796
time_elpased: 1.824
batch start
#iterations: 195
currently lose_sum: 586.2004316449165
time_elpased: 1.808
batch start
#iterations: 196
currently lose_sum: 585.1656821966171
time_elpased: 1.814
batch start
#iterations: 197
currently lose_sum: 587.7477949261665
time_elpased: 1.814
batch start
#iterations: 198
currently lose_sum: 586.8111007809639
time_elpased: 1.827
batch start
#iterations: 199
currently lose_sum: 585.0614879131317
time_elpased: 1.818
start validation test
0.585082872928
0.570380764079
0.920345785736
0.704284139235
0
validation finish
batch start
#iterations: 200
currently lose_sum: 587.5319159626961
time_elpased: 1.844
batch start
#iterations: 201
currently lose_sum: 583.5885632634163
time_elpased: 1.81
batch start
#iterations: 202
currently lose_sum: 584.7898719906807
time_elpased: 1.822
batch start
#iterations: 203
currently lose_sum: 585.0071701407433
time_elpased: 1.805
batch start
#iterations: 204
currently lose_sum: 585.8866975903511
time_elpased: 1.819
batch start
#iterations: 205
currently lose_sum: 586.0436719655991
time_elpased: 1.798
batch start
#iterations: 206
currently lose_sum: 586.2419251799583
time_elpased: 1.819
batch start
#iterations: 207
currently lose_sum: 587.206649184227
time_elpased: 1.789
batch start
#iterations: 208
currently lose_sum: 587.3167968392372
time_elpased: 1.837
batch start
#iterations: 209
currently lose_sum: 587.4502727985382
time_elpased: 1.776
batch start
#iterations: 210
currently lose_sum: 585.9583247900009
time_elpased: 1.816
batch start
#iterations: 211
currently lose_sum: 586.470705986023
time_elpased: 1.839
batch start
#iterations: 212
currently lose_sum: 584.8682019710541
time_elpased: 1.788
batch start
#iterations: 213
currently lose_sum: 586.6442286968231
time_elpased: 1.801
batch start
#iterations: 214
currently lose_sum: 584.7861071228981
time_elpased: 1.806
batch start
#iterations: 215
currently lose_sum: 584.493780195713
time_elpased: 1.907
batch start
#iterations: 216
currently lose_sum: 585.3730784058571
time_elpased: 1.8
batch start
#iterations: 217
currently lose_sum: 586.40113312006
time_elpased: 1.795
batch start
#iterations: 218
currently lose_sum: 587.5814464092255
time_elpased: 1.774
batch start
#iterations: 219
currently lose_sum: 586.884207367897
time_elpased: 1.827
start validation test
0.583839779006
0.569661022354
0.9192137491
0.703404012364
0
validation finish
batch start
#iterations: 220
currently lose_sum: 585.0986915826797
time_elpased: 1.812
batch start
#iterations: 221
currently lose_sum: 586.4737924337387
time_elpased: 1.783
batch start
#iterations: 222
currently lose_sum: 587.4666822552681
time_elpased: 1.794
batch start
#iterations: 223
currently lose_sum: 585.5993388295174
time_elpased: 1.791
batch start
#iterations: 224
currently lose_sum: 588.1996155977249
time_elpased: 1.785
batch start
#iterations: 225
currently lose_sum: 585.6744111180305
time_elpased: 1.813
batch start
#iterations: 226
currently lose_sum: 587.2157568335533
time_elpased: 1.825
batch start
#iterations: 227
currently lose_sum: 584.5752575993538
time_elpased: 1.815
batch start
#iterations: 228
currently lose_sum: 584.1887813210487
time_elpased: 1.835
batch start
#iterations: 229
currently lose_sum: 586.4713263511658
time_elpased: 1.804
batch start
#iterations: 230
currently lose_sum: 584.5275621414185
time_elpased: 1.798
batch start
#iterations: 231
currently lose_sum: 585.9117367267609
time_elpased: 1.785
batch start
#iterations: 232
currently lose_sum: 586.2600536346436
time_elpased: 1.813
batch start
#iterations: 233
currently lose_sum: 586.7901304960251
time_elpased: 1.787
batch start
#iterations: 234
currently lose_sum: 585.7187752723694
time_elpased: 1.797
batch start
#iterations: 235
currently lose_sum: 585.2650734186172
time_elpased: 1.798
batch start
#iterations: 236
currently lose_sum: 586.0356110334396
time_elpased: 1.81
batch start
#iterations: 237
currently lose_sum: 584.8803059458733
time_elpased: 1.817
batch start
#iterations: 238
currently lose_sum: 586.5881805419922
time_elpased: 1.8
batch start
#iterations: 239
currently lose_sum: 584.0318875312805
time_elpased: 1.82
start validation test
0.583839779006
0.569096947651
0.925800144077
0.704891378871
0
validation finish
batch start
#iterations: 240
currently lose_sum: 584.3956929445267
time_elpased: 1.843
batch start
#iterations: 241
currently lose_sum: 584.9128786325455
time_elpased: 1.8
batch start
#iterations: 242
currently lose_sum: 586.6011377573013
time_elpased: 1.808
batch start
#iterations: 243
currently lose_sum: 586.1433112621307
time_elpased: 1.806
batch start
#iterations: 244
currently lose_sum: 586.8481646776199
time_elpased: 1.833
batch start
#iterations: 245
currently lose_sum: 586.1628532409668
time_elpased: 1.789
batch start
#iterations: 246
currently lose_sum: 585.1557102203369
time_elpased: 1.809
batch start
#iterations: 247
currently lose_sum: 586.7786650657654
time_elpased: 1.807
batch start
#iterations: 248
currently lose_sum: 586.8597974777222
time_elpased: 1.813
batch start
#iterations: 249
currently lose_sum: 586.498073220253
time_elpased: 1.779
batch start
#iterations: 250
currently lose_sum: 586.1554015278816
time_elpased: 1.787
batch start
#iterations: 251
currently lose_sum: 586.2583972215652
time_elpased: 1.794
batch start
#iterations: 252
currently lose_sum: 584.7474540472031
time_elpased: 1.809
batch start
#iterations: 253
currently lose_sum: 586.7782184481621
time_elpased: 1.818
batch start
#iterations: 254
currently lose_sum: 585.1971648335457
time_elpased: 1.816
batch start
#iterations: 255
currently lose_sum: 585.018366754055
time_elpased: 1.814
batch start
#iterations: 256
currently lose_sum: 585.4545659422874
time_elpased: 1.789
batch start
#iterations: 257
currently lose_sum: 587.7436220645905
time_elpased: 1.795
batch start
#iterations: 258
currently lose_sum: 585.199011683464
time_elpased: 1.784
batch start
#iterations: 259
currently lose_sum: 584.8023090362549
time_elpased: 1.795
start validation test
0.584585635359
0.569991083938
0.921066172687
0.704197647429
0
validation finish
batch start
#iterations: 260
currently lose_sum: 585.265435397625
time_elpased: 1.818
batch start
#iterations: 261
currently lose_sum: 587.0220950841904
time_elpased: 1.813
batch start
#iterations: 262
currently lose_sum: 583.8857579231262
time_elpased: 1.779
batch start
#iterations: 263
currently lose_sum: 588.3144040107727
time_elpased: 1.805
batch start
#iterations: 264
currently lose_sum: 584.7259616851807
time_elpased: 1.804
batch start
#iterations: 265
currently lose_sum: 586.8342082500458
time_elpased: 1.796
batch start
#iterations: 266
currently lose_sum: 585.7394708991051
time_elpased: 1.815
batch start
#iterations: 267
currently lose_sum: 587.2405290007591
time_elpased: 1.822
batch start
#iterations: 268
currently lose_sum: 584.481503546238
time_elpased: 1.841
batch start
#iterations: 269
currently lose_sum: 585.9585487246513
time_elpased: 1.789
batch start
#iterations: 270
currently lose_sum: 584.2830001711845
time_elpased: 1.835
batch start
#iterations: 271
currently lose_sum: 585.8806766867638
time_elpased: 1.793
batch start
#iterations: 272
currently lose_sum: 586.5893987417221
time_elpased: 1.789
batch start
#iterations: 273
currently lose_sum: 586.3939131498337
time_elpased: 1.798
batch start
#iterations: 274
currently lose_sum: 586.4893718361855
time_elpased: 1.805
batch start
#iterations: 275
currently lose_sum: 585.9751908779144
time_elpased: 1.792
batch start
#iterations: 276
currently lose_sum: 585.9605869650841
time_elpased: 1.8
batch start
#iterations: 277
currently lose_sum: 586.4342730641365
time_elpased: 1.773
batch start
#iterations: 278
currently lose_sum: 584.5806319713593
time_elpased: 1.774
batch start
#iterations: 279
currently lose_sum: 585.3267071247101
time_elpased: 1.809
start validation test
0.583591160221
0.568978610302
0.92528558197
0.704651436185
0
validation finish
batch start
#iterations: 280
currently lose_sum: 587.9890224933624
time_elpased: 1.784
batch start
#iterations: 281
currently lose_sum: 585.3383776545525
time_elpased: 1.785
batch start
#iterations: 282
currently lose_sum: 584.7600848674774
time_elpased: 1.813
batch start
#iterations: 283
currently lose_sum: 585.2244253158569
time_elpased: 1.809
batch start
#iterations: 284
currently lose_sum: 586.0711039304733
time_elpased: 1.801
batch start
#iterations: 285
currently lose_sum: 585.8508023619652
time_elpased: 1.806
batch start
#iterations: 286
currently lose_sum: 587.3669754266739
time_elpased: 1.844
batch start
#iterations: 287
currently lose_sum: 585.9978094696999
time_elpased: 1.801
batch start
#iterations: 288
currently lose_sum: 586.4940333366394
time_elpased: 1.812
batch start
#iterations: 289
currently lose_sum: 585.0766547322273
time_elpased: 1.84
batch start
#iterations: 290
currently lose_sum: 585.8347698450089
time_elpased: 1.815
batch start
#iterations: 291
currently lose_sum: 586.7368415594101
time_elpased: 1.799
batch start
#iterations: 292
currently lose_sum: 587.4818151593208
time_elpased: 1.806
batch start
#iterations: 293
currently lose_sum: 587.3982966542244
time_elpased: 1.813
batch start
#iterations: 294
currently lose_sum: 585.0290989279747
time_elpased: 1.772
batch start
#iterations: 295
currently lose_sum: 588.5184874534607
time_elpased: 1.787
batch start
#iterations: 296
currently lose_sum: 585.6800498366356
time_elpased: 1.809
batch start
#iterations: 297
currently lose_sum: 585.3613722920418
time_elpased: 1.825
batch start
#iterations: 298
currently lose_sum: 585.7782534956932
time_elpased: 1.816
batch start
#iterations: 299
currently lose_sum: 587.4903137087822
time_elpased: 1.809
start validation test
0.582099447514
0.56777686835
0.928064217351
0.70453125
0
validation finish
batch start
#iterations: 300
currently lose_sum: 585.0607969760895
time_elpased: 1.812
batch start
#iterations: 301
currently lose_sum: 585.7011169195175
time_elpased: 1.788
batch start
#iterations: 302
currently lose_sum: 584.0370461344719
time_elpased: 1.819
batch start
#iterations: 303
currently lose_sum: 584.589005112648
time_elpased: 1.807
batch start
#iterations: 304
currently lose_sum: 584.7367540597916
time_elpased: 1.787
batch start
#iterations: 305
currently lose_sum: 585.4756245613098
time_elpased: 1.793
batch start
#iterations: 306
currently lose_sum: 587.0543918013573
time_elpased: 1.781
batch start
#iterations: 307
currently lose_sum: 586.5932968854904
time_elpased: 1.808
batch start
#iterations: 308
currently lose_sum: 585.3848247528076
time_elpased: 1.811
batch start
#iterations: 309
currently lose_sum: 586.0660096406937
time_elpased: 1.818
batch start
#iterations: 310
currently lose_sum: 584.1902737617493
time_elpased: 1.788
batch start
#iterations: 311
currently lose_sum: 587.3319006562233
time_elpased: 1.821
batch start
#iterations: 312
currently lose_sum: 585.666386961937
time_elpased: 1.809
batch start
#iterations: 313
currently lose_sum: 584.3172398805618
time_elpased: 1.802
batch start
#iterations: 314
currently lose_sum: 584.2592177391052
time_elpased: 1.806
batch start
#iterations: 315
currently lose_sum: 585.1140983700752
time_elpased: 1.792
batch start
#iterations: 316
currently lose_sum: 587.3512924313545
time_elpased: 1.779
batch start
#iterations: 317
currently lose_sum: 583.8614611029625
time_elpased: 1.786
batch start
#iterations: 318
currently lose_sum: 586.0172727108002
time_elpased: 1.828
batch start
#iterations: 319
currently lose_sum: 586.834261238575
time_elpased: 1.798
start validation test
0.583839779006
0.56932499762
0.923124421118
0.704288939052
0
validation finish
batch start
#iterations: 320
currently lose_sum: 585.2563517093658
time_elpased: 1.838
batch start
#iterations: 321
currently lose_sum: 587.410294353962
time_elpased: 1.806
batch start
#iterations: 322
currently lose_sum: 584.9985806941986
time_elpased: 1.824
batch start
#iterations: 323
currently lose_sum: 585.8286418914795
time_elpased: 1.792
batch start
#iterations: 324
currently lose_sum: 585.5586943030357
time_elpased: 1.806
batch start
#iterations: 325
currently lose_sum: 585.0676888227463
time_elpased: 1.822
batch start
#iterations: 326
currently lose_sum: 584.0766950845718
time_elpased: 1.806
batch start
#iterations: 327
currently lose_sum: 586.8847401738167
time_elpased: 1.822
batch start
#iterations: 328
currently lose_sum: 586.8109473586082
time_elpased: 1.816
batch start
#iterations: 329
currently lose_sum: 583.7199608683586
time_elpased: 1.797
batch start
#iterations: 330
currently lose_sum: 584.9972524046898
time_elpased: 1.789
batch start
#iterations: 331
currently lose_sum: 588.0207312107086
time_elpased: 1.81
batch start
#iterations: 332
currently lose_sum: 586.1440713405609
time_elpased: 1.803
batch start
#iterations: 333
currently lose_sum: 586.600558757782
time_elpased: 1.815
batch start
#iterations: 334
currently lose_sum: 586.2663189768791
time_elpased: 1.793
batch start
#iterations: 335
currently lose_sum: 586.3230920433998
time_elpased: 1.8
batch start
#iterations: 336
currently lose_sum: 583.286871433258
time_elpased: 1.797
batch start
#iterations: 337
currently lose_sum: 583.9344299435616
time_elpased: 1.8
batch start
#iterations: 338
currently lose_sum: 586.0132118463516
time_elpased: 1.807
batch start
#iterations: 339
currently lose_sum: 584.8058723807335
time_elpased: 1.804
start validation test
0.586270718232
0.571300591905
0.918802099413
0.70453155517
0
validation finish
batch start
#iterations: 340
currently lose_sum: 586.8706609010696
time_elpased: 1.839
batch start
#iterations: 341
currently lose_sum: 585.690299808979
time_elpased: 1.805
batch start
#iterations: 342
currently lose_sum: 587.1152193546295
time_elpased: 1.821
batch start
#iterations: 343
currently lose_sum: 585.5156696438789
time_elpased: 1.807
batch start
#iterations: 344
currently lose_sum: 586.8164449930191
time_elpased: 1.811
batch start
#iterations: 345
currently lose_sum: 585.2579588890076
time_elpased: 1.795
batch start
#iterations: 346
currently lose_sum: 587.5446838140488
time_elpased: 1.817
batch start
#iterations: 347
currently lose_sum: 584.8662536144257
time_elpased: 1.841
batch start
#iterations: 348
currently lose_sum: 585.2298004627228
time_elpased: 1.802
batch start
#iterations: 349
currently lose_sum: 584.305017888546
time_elpased: 1.811
batch start
#iterations: 350
currently lose_sum: 583.9525795578957
time_elpased: 1.845
batch start
#iterations: 351
currently lose_sum: 586.8040143251419
time_elpased: 1.848
batch start
#iterations: 352
currently lose_sum: 586.1205121278763
time_elpased: 1.838
batch start
#iterations: 353
currently lose_sum: 583.4799470901489
time_elpased: 1.844
batch start
#iterations: 354
currently lose_sum: 586.0482732057571
time_elpased: 1.82
batch start
#iterations: 355
currently lose_sum: 584.438357770443
time_elpased: 1.841
batch start
#iterations: 356
currently lose_sum: 587.1006786823273
time_elpased: 1.865
batch start
#iterations: 357
currently lose_sum: 587.2123919129372
time_elpased: 1.847
batch start
#iterations: 358
currently lose_sum: 587.5021830201149
time_elpased: 1.879
batch start
#iterations: 359
currently lose_sum: 586.296015560627
time_elpased: 1.862
start validation test
0.585635359116
0.571345819656
0.913553565915
0.703017343787
0
validation finish
batch start
#iterations: 360
currently lose_sum: 581.9181588888168
time_elpased: 1.861
batch start
#iterations: 361
currently lose_sum: 583.9512251615524
time_elpased: 1.88
batch start
#iterations: 362
currently lose_sum: 585.8682065606117
time_elpased: 1.873
batch start
#iterations: 363
currently lose_sum: 585.6812821030617
time_elpased: 1.864
batch start
#iterations: 364
currently lose_sum: 587.5172111392021
time_elpased: 1.866
batch start
#iterations: 365
currently lose_sum: 585.3864449262619
time_elpased: 1.903
batch start
#iterations: 366
currently lose_sum: 585.6914404034615
time_elpased: 1.868
batch start
#iterations: 367
currently lose_sum: 584.5466881990433
time_elpased: 1.904
batch start
#iterations: 368
currently lose_sum: 585.5250390768051
time_elpased: 1.926
batch start
#iterations: 369
currently lose_sum: 585.8516785800457
time_elpased: 1.892
batch start
#iterations: 370
currently lose_sum: 585.7595320940018
time_elpased: 1.912
batch start
#iterations: 371
currently lose_sum: 585.6597584486008
time_elpased: 1.863
batch start
#iterations: 372
currently lose_sum: 585.35939848423
time_elpased: 1.844
batch start
#iterations: 373
currently lose_sum: 586.0108509063721
time_elpased: 1.886
batch start
#iterations: 374
currently lose_sum: 583.09846585989
time_elpased: 1.903
batch start
#iterations: 375
currently lose_sum: 586.9733534455299
time_elpased: 1.867
batch start
#iterations: 376
currently lose_sum: 585.1708464622498
time_elpased: 1.825
batch start
#iterations: 377
currently lose_sum: 586.4880555272102
time_elpased: 1.875
batch start
#iterations: 378
currently lose_sum: 586.4436074495316
time_elpased: 1.887
batch start
#iterations: 379
currently lose_sum: 585.4873698353767
time_elpased: 1.914
start validation test
0.584834254144
0.570163422637
0.920963260266
0.704299065421
0
validation finish
batch start
#iterations: 380
currently lose_sum: 585.7612082958221
time_elpased: 1.876
batch start
#iterations: 381
currently lose_sum: 585.4849967956543
time_elpased: 1.9
batch start
#iterations: 382
currently lose_sum: 586.5348097681999
time_elpased: 1.912
batch start
#iterations: 383
currently lose_sum: 583.9769603013992
time_elpased: 1.893
batch start
#iterations: 384
currently lose_sum: 585.8544380664825
time_elpased: 1.913
batch start
#iterations: 385
currently lose_sum: 586.1167773008347
time_elpased: 1.845
batch start
#iterations: 386
currently lose_sum: 585.9649690389633
time_elpased: 1.891
batch start
#iterations: 387
currently lose_sum: 587.7683188915253
time_elpased: 1.876
batch start
#iterations: 388
currently lose_sum: 586.1176288723946
time_elpased: 1.863
batch start
#iterations: 389
currently lose_sum: 585.5078684687614
time_elpased: 1.842
batch start
#iterations: 390
currently lose_sum: 584.6386801600456
time_elpased: 1.883
batch start
#iterations: 391
currently lose_sum: 584.7694285213947
time_elpased: 1.911
batch start
#iterations: 392
currently lose_sum: 586.3926445841789
time_elpased: 1.962
batch start
#iterations: 393
currently lose_sum: 586.3823043107986
time_elpased: 1.908
batch start
#iterations: 394
currently lose_sum: 586.1768026351929
time_elpased: 1.902
batch start
#iterations: 395
currently lose_sum: 584.907731115818
time_elpased: 1.873
batch start
#iterations: 396
currently lose_sum: 586.5116123557091
time_elpased: 1.897
batch start
#iterations: 397
currently lose_sum: 585.2227542996407
time_elpased: 1.824
batch start
#iterations: 398
currently lose_sum: 585.3911917209625
time_elpased: 1.854
batch start
#iterations: 399
currently lose_sum: 584.2077719569206
time_elpased: 1.884
start validation test
0.582817679558
0.568483622107
0.925182669548
0.704241902001
0
validation finish
acc: 0.579
pre: 0.566
rec: 0.926
F1: 0.703
auc: 0.000
