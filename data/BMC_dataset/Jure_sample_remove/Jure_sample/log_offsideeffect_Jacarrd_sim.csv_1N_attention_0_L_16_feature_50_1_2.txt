start to construct graph
graph construct over
93050
epochs start
batch start
#iterations: 0
currently lose_sum: 612.4339970350266
time_elpased: 1.862
batch start
#iterations: 1
currently lose_sum: 607.3769236803055
time_elpased: 1.809
batch start
#iterations: 2
currently lose_sum: 606.3840400576591
time_elpased: 1.869
batch start
#iterations: 3
currently lose_sum: 607.0570312738419
time_elpased: 1.814
batch start
#iterations: 4
currently lose_sum: 605.8452122807503
time_elpased: 1.896
batch start
#iterations: 5
currently lose_sum: 605.4345881342888
time_elpased: 1.818
batch start
#iterations: 6
currently lose_sum: 603.0938026309013
time_elpased: 1.803
batch start
#iterations: 7
currently lose_sum: 604.247100353241
time_elpased: 1.819
batch start
#iterations: 8
currently lose_sum: 602.8242058753967
time_elpased: 1.808
batch start
#iterations: 9
currently lose_sum: 602.4962157011032
time_elpased: 1.794
batch start
#iterations: 10
currently lose_sum: 602.3741067051888
time_elpased: 1.795
batch start
#iterations: 11
currently lose_sum: 601.0910318493843
time_elpased: 1.814
batch start
#iterations: 12
currently lose_sum: 599.6833236217499
time_elpased: 1.809
batch start
#iterations: 13
currently lose_sum: 601.2462606430054
time_elpased: 1.797
batch start
#iterations: 14
currently lose_sum: 598.906382381916
time_elpased: 1.795
batch start
#iterations: 15
currently lose_sum: 601.0361372232437
time_elpased: 1.813
batch start
#iterations: 16
currently lose_sum: 600.4249193668365
time_elpased: 1.787
batch start
#iterations: 17
currently lose_sum: 600.1604551672935
time_elpased: 1.782
batch start
#iterations: 18
currently lose_sum: 597.2540660500526
time_elpased: 1.796
batch start
#iterations: 19
currently lose_sum: 598.6363502144814
time_elpased: 1.806
start validation test
0.559254143646
0.553739689209
0.922301121745
0.692006254464
0
validation finish
batch start
#iterations: 20
currently lose_sum: 596.748174726963
time_elpased: 1.828
batch start
#iterations: 21
currently lose_sum: 596.5814501643181
time_elpased: 1.826
batch start
#iterations: 22
currently lose_sum: 596.2398067712784
time_elpased: 1.803
batch start
#iterations: 23
currently lose_sum: 599.0953202843666
time_elpased: 1.81
batch start
#iterations: 24
currently lose_sum: 596.8143023252487
time_elpased: 1.811
batch start
#iterations: 25
currently lose_sum: 597.6950952410698
time_elpased: 1.781
batch start
#iterations: 26
currently lose_sum: 596.6886043548584
time_elpased: 1.793
batch start
#iterations: 27
currently lose_sum: 594.5085289478302
time_elpased: 1.819
batch start
#iterations: 28
currently lose_sum: 593.7120420336723
time_elpased: 1.792
batch start
#iterations: 29
currently lose_sum: 594.9033639431
time_elpased: 1.793
batch start
#iterations: 30
currently lose_sum: 595.6845726966858
time_elpased: 1.796
batch start
#iterations: 31
currently lose_sum: 594.7058044075966
time_elpased: 1.793
batch start
#iterations: 32
currently lose_sum: 596.3162667155266
time_elpased: 1.802
batch start
#iterations: 33
currently lose_sum: 596.116133928299
time_elpased: 1.813
batch start
#iterations: 34
currently lose_sum: 593.622327029705
time_elpased: 1.808
batch start
#iterations: 35
currently lose_sum: 594.7785732746124
time_elpased: 1.812
batch start
#iterations: 36
currently lose_sum: 593.6145561933517
time_elpased: 1.776
batch start
#iterations: 37
currently lose_sum: 595.3229984045029
time_elpased: 1.787
batch start
#iterations: 38
currently lose_sum: 593.0874967575073
time_elpased: 1.781
batch start
#iterations: 39
currently lose_sum: 592.5433434247971
time_elpased: 1.795
start validation test
0.574337016575
0.565360007794
0.895749716991
0.693200597312
0
validation finish
batch start
#iterations: 40
currently lose_sum: 594.5210514068604
time_elpased: 1.805
batch start
#iterations: 41
currently lose_sum: 594.1484090089798
time_elpased: 1.798
batch start
#iterations: 42
currently lose_sum: 594.0410022139549
time_elpased: 1.792
batch start
#iterations: 43
currently lose_sum: 593.3846724033356
time_elpased: 1.808
batch start
#iterations: 44
currently lose_sum: 593.9094099998474
time_elpased: 1.923
batch start
#iterations: 45
currently lose_sum: 593.8336378335953
time_elpased: 1.92
batch start
#iterations: 46
currently lose_sum: 592.9232493638992
time_elpased: 1.873
batch start
#iterations: 47
currently lose_sum: 592.650359749794
time_elpased: 1.88
batch start
#iterations: 48
currently lose_sum: 596.1989548802376
time_elpased: 1.921
batch start
#iterations: 49
currently lose_sum: 592.0316111445427
time_elpased: 1.93
batch start
#iterations: 50
currently lose_sum: 592.4696981310844
time_elpased: 1.821
batch start
#iterations: 51
currently lose_sum: 591.2207412719727
time_elpased: 1.792
batch start
#iterations: 52
currently lose_sum: 592.2132819890976
time_elpased: 1.806
batch start
#iterations: 53
currently lose_sum: 593.0346928536892
time_elpased: 1.791
batch start
#iterations: 54
currently lose_sum: 591.6631111502647
time_elpased: 1.774
batch start
#iterations: 55
currently lose_sum: 593.4713286757469
time_elpased: 1.79
batch start
#iterations: 56
currently lose_sum: 592.7322388291359
time_elpased: 1.79
batch start
#iterations: 57
currently lose_sum: 591.5701501965523
time_elpased: 1.787
batch start
#iterations: 58
currently lose_sum: 593.5001782774925
time_elpased: 1.807
batch start
#iterations: 59
currently lose_sum: 590.8126776218414
time_elpased: 1.8
start validation test
0.566298342541
0.556123369206
0.951939899146
0.702087286528
0
validation finish
batch start
#iterations: 60
currently lose_sum: 591.1380621790886
time_elpased: 1.802
batch start
#iterations: 61
currently lose_sum: 592.6570531129837
time_elpased: 1.8
batch start
#iterations: 62
currently lose_sum: 593.2272948622704
time_elpased: 1.792
batch start
#iterations: 63
currently lose_sum: 590.7574313879013
time_elpased: 1.798
batch start
#iterations: 64
currently lose_sum: 591.7788263559341
time_elpased: 1.776
batch start
#iterations: 65
currently lose_sum: 591.4332257509232
time_elpased: 1.806
batch start
#iterations: 66
currently lose_sum: 589.6855941414833
time_elpased: 1.78
batch start
#iterations: 67
currently lose_sum: 588.5757894515991
time_elpased: 1.784
batch start
#iterations: 68
currently lose_sum: 591.1341491937637
time_elpased: 1.774
batch start
#iterations: 69
currently lose_sum: 592.055360853672
time_elpased: 1.774
batch start
#iterations: 70
currently lose_sum: 592.3800328969955
time_elpased: 1.792
batch start
#iterations: 71
currently lose_sum: 590.5054817199707
time_elpased: 1.779
batch start
#iterations: 72
currently lose_sum: 591.8906502127647
time_elpased: 1.805
batch start
#iterations: 73
currently lose_sum: 592.3157686591148
time_elpased: 1.764
batch start
#iterations: 74
currently lose_sum: 591.0547845959663
time_elpased: 1.775
batch start
#iterations: 75
currently lose_sum: 590.5551971793175
time_elpased: 1.803
batch start
#iterations: 76
currently lose_sum: 589.5865095257759
time_elpased: 1.789
batch start
#iterations: 77
currently lose_sum: 589.632759988308
time_elpased: 1.781
batch start
#iterations: 78
currently lose_sum: 592.4688131213188
time_elpased: 1.833
batch start
#iterations: 79
currently lose_sum: 588.3514464497566
time_elpased: 1.919
start validation test
0.564475138122
0.554151411362
0.965730163631
0.704213725564
0
validation finish
batch start
#iterations: 80
currently lose_sum: 590.490603685379
time_elpased: 1.841
batch start
#iterations: 81
currently lose_sum: 590.5485988259315
time_elpased: 1.768
batch start
#iterations: 82
currently lose_sum: 591.1815217733383
time_elpased: 1.786
batch start
#iterations: 83
currently lose_sum: 589.6889378428459
time_elpased: 1.776
batch start
#iterations: 84
currently lose_sum: 590.8220043182373
time_elpased: 1.777
batch start
#iterations: 85
currently lose_sum: 590.1091574430466
time_elpased: 1.772
batch start
#iterations: 86
currently lose_sum: 588.8456594347954
time_elpased: 1.8
batch start
#iterations: 87
currently lose_sum: 590.0792170166969
time_elpased: 1.79
batch start
#iterations: 88
currently lose_sum: 588.6206194758415
time_elpased: 1.814
batch start
#iterations: 89
currently lose_sum: 589.1382273435593
time_elpased: 1.789
batch start
#iterations: 90
currently lose_sum: 590.1568398475647
time_elpased: 1.803
batch start
#iterations: 91
currently lose_sum: 590.0420827865601
time_elpased: 1.782
batch start
#iterations: 92
currently lose_sum: 589.2470069527626
time_elpased: 1.814
batch start
#iterations: 93
currently lose_sum: 589.3826941251755
time_elpased: 1.788
batch start
#iterations: 94
currently lose_sum: 589.399155318737
time_elpased: 1.804
batch start
#iterations: 95
currently lose_sum: 589.4648654460907
time_elpased: 1.806
batch start
#iterations: 96
currently lose_sum: 590.8998412489891
time_elpased: 1.809
batch start
#iterations: 97
currently lose_sum: 589.6714313626289
time_elpased: 1.802
batch start
#iterations: 98
currently lose_sum: 588.6913986802101
time_elpased: 1.805
batch start
#iterations: 99
currently lose_sum: 590.3441656827927
time_elpased: 1.795
start validation test
0.575745856354
0.562947862614
0.937840897396
0.70357073924
0
validation finish
batch start
#iterations: 100
currently lose_sum: 590.9910369515419
time_elpased: 1.796
batch start
#iterations: 101
currently lose_sum: 590.0236317515373
time_elpased: 1.787
batch start
#iterations: 102
currently lose_sum: 587.1002418398857
time_elpased: 1.792
batch start
#iterations: 103
currently lose_sum: 591.2178328633308
time_elpased: 1.789
batch start
#iterations: 104
currently lose_sum: 588.1877331733704
time_elpased: 1.785
batch start
#iterations: 105
currently lose_sum: 588.566316485405
time_elpased: 1.792
batch start
#iterations: 106
currently lose_sum: 589.8603266477585
time_elpased: 1.796
batch start
#iterations: 107
currently lose_sum: 589.2065769433975
time_elpased: 1.764
batch start
#iterations: 108
currently lose_sum: 589.0171968340874
time_elpased: 1.788
batch start
#iterations: 109
currently lose_sum: 588.182612657547
time_elpased: 1.797
batch start
#iterations: 110
currently lose_sum: 589.5043524503708
time_elpased: 1.794
batch start
#iterations: 111
currently lose_sum: 588.5987441539764
time_elpased: 1.772
batch start
#iterations: 112
currently lose_sum: 587.8305391967297
time_elpased: 1.779
batch start
#iterations: 113
currently lose_sum: 591.6599745750427
time_elpased: 1.783
batch start
#iterations: 114
currently lose_sum: 588.8630927801132
time_elpased: 1.798
batch start
#iterations: 115
currently lose_sum: 588.723391532898
time_elpased: 1.788
batch start
#iterations: 116
currently lose_sum: 588.8097414374352
time_elpased: 1.83
batch start
#iterations: 117
currently lose_sum: 590.906068623066
time_elpased: 1.805
batch start
#iterations: 118
currently lose_sum: 589.7323068380356
time_elpased: 1.774
batch start
#iterations: 119
currently lose_sum: 588.040167927742
time_elpased: 1.787
start validation test
0.584171270718
0.570947368421
0.907070083359
0.700789123219
0
validation finish
batch start
#iterations: 120
currently lose_sum: 590.3836954534054
time_elpased: 1.811
batch start
#iterations: 121
currently lose_sum: 589.4535971879959
time_elpased: 1.801
batch start
#iterations: 122
currently lose_sum: 589.160424053669
time_elpased: 1.834
batch start
#iterations: 123
currently lose_sum: 587.6505979895592
time_elpased: 1.816
batch start
#iterations: 124
currently lose_sum: 591.5538499951363
time_elpased: 1.817
batch start
#iterations: 125
currently lose_sum: 588.1447974443436
time_elpased: 1.828
batch start
#iterations: 126
currently lose_sum: 589.8230096101761
time_elpased: 1.866
batch start
#iterations: 127
currently lose_sum: 589.6970924735069
time_elpased: 1.842
batch start
#iterations: 128
currently lose_sum: 588.4297542572021
time_elpased: 1.839
batch start
#iterations: 129
currently lose_sum: 588.7111784815788
time_elpased: 1.839
batch start
#iterations: 130
currently lose_sum: 588.4503654241562
time_elpased: 1.859
batch start
#iterations: 131
currently lose_sum: 587.9850007891655
time_elpased: 1.841
batch start
#iterations: 132
currently lose_sum: 587.4327347278595
time_elpased: 1.857
batch start
#iterations: 133
currently lose_sum: 589.0781133770943
time_elpased: 1.814
batch start
#iterations: 134
currently lose_sum: 590.7774141430855
time_elpased: 1.863
batch start
#iterations: 135
currently lose_sum: 589.4326645731926
time_elpased: 1.885
batch start
#iterations: 136
currently lose_sum: 587.4518370628357
time_elpased: 1.852
batch start
#iterations: 137
currently lose_sum: 588.2502780556679
time_elpased: 1.865
batch start
#iterations: 138
currently lose_sum: 590.4929134249687
time_elpased: 1.84
batch start
#iterations: 139
currently lose_sum: 589.2825962305069
time_elpased: 1.897
start validation test
0.57546961326
0.562243585818
0.944941854482
0.705006142506
0
validation finish
batch start
#iterations: 140
currently lose_sum: 588.0799019932747
time_elpased: 1.852
batch start
#iterations: 141
currently lose_sum: 588.2979129552841
time_elpased: 1.868
batch start
#iterations: 142
currently lose_sum: 587.7156597971916
time_elpased: 1.86
batch start
#iterations: 143
currently lose_sum: 590.4355630874634
time_elpased: 1.846
batch start
#iterations: 144
currently lose_sum: 587.9620531201363
time_elpased: 1.829
batch start
#iterations: 145
currently lose_sum: 589.2871530056
time_elpased: 1.915
batch start
#iterations: 146
currently lose_sum: 586.257480442524
time_elpased: 1.892
batch start
#iterations: 147
currently lose_sum: 587.6929318904877
time_elpased: 1.875
batch start
#iterations: 148
currently lose_sum: 587.7812203764915
time_elpased: 1.877
batch start
#iterations: 149
currently lose_sum: 588.0166333913803
time_elpased: 1.858
batch start
#iterations: 150
currently lose_sum: 587.4947350025177
time_elpased: 1.879
batch start
#iterations: 151
currently lose_sum: 588.5793351829052
time_elpased: 1.94
batch start
#iterations: 152
currently lose_sum: 587.6717989444733
time_elpased: 1.879
batch start
#iterations: 153
currently lose_sum: 587.12497651577
time_elpased: 1.932
batch start
#iterations: 154
currently lose_sum: 586.7710867524147
time_elpased: 1.858
batch start
#iterations: 155
currently lose_sum: 587.85782289505
time_elpased: 1.833
batch start
#iterations: 156
currently lose_sum: 588.4644631743431
time_elpased: 1.905
batch start
#iterations: 157
currently lose_sum: 587.2295953035355
time_elpased: 1.895
batch start
#iterations: 158
currently lose_sum: 587.2576784491539
time_elpased: 1.864
batch start
#iterations: 159
currently lose_sum: 587.434101998806
time_elpased: 1.825
start validation test
0.58726519337
0.575358089296
0.882576927035
0.696598639456
0
validation finish
batch start
#iterations: 160
currently lose_sum: 588.671164393425
time_elpased: 1.908
batch start
#iterations: 161
currently lose_sum: 588.6204208135605
time_elpased: 1.894
batch start
#iterations: 162
currently lose_sum: 587.685317337513
time_elpased: 1.906
batch start
#iterations: 163
currently lose_sum: 587.3050646185875
time_elpased: 1.93
batch start
#iterations: 164
currently lose_sum: 587.5849777460098
time_elpased: 1.949
batch start
#iterations: 165
currently lose_sum: 588.628036737442
time_elpased: 1.925
batch start
#iterations: 166
currently lose_sum: 588.0866650938988
time_elpased: 1.95
batch start
#iterations: 167
currently lose_sum: 587.7043713331223
time_elpased: 1.826
batch start
#iterations: 168
currently lose_sum: 588.9904789328575
time_elpased: 1.993
batch start
#iterations: 169
currently lose_sum: 587.1561566591263
time_elpased: 1.881
batch start
#iterations: 170
currently lose_sum: 586.6401287317276
time_elpased: 1.904
batch start
#iterations: 171
currently lose_sum: 586.6691027879715
time_elpased: 1.841
batch start
#iterations: 172
currently lose_sum: 588.3083717823029
time_elpased: 1.878
batch start
#iterations: 173
currently lose_sum: 587.4743658900261
time_elpased: 1.877
batch start
#iterations: 174
currently lose_sum: 586.6482829451561
time_elpased: 1.936
batch start
#iterations: 175
currently lose_sum: 587.772625207901
time_elpased: 1.884
batch start
#iterations: 176
currently lose_sum: 587.7096511721611
time_elpased: 1.862
batch start
#iterations: 177
currently lose_sum: 586.9837572574615
time_elpased: 1.851
batch start
#iterations: 178
currently lose_sum: 586.4592914581299
time_elpased: 1.885
batch start
#iterations: 179
currently lose_sum: 587.5177503228188
time_elpased: 1.877
start validation test
0.580138121547
0.566327329679
0.930328290625
0.704063552639
0
validation finish
batch start
#iterations: 180
currently lose_sum: 586.2951594591141
time_elpased: 1.962
batch start
#iterations: 181
currently lose_sum: 586.5228967666626
time_elpased: 1.844
batch start
#iterations: 182
currently lose_sum: 587.8404030799866
time_elpased: 1.87
batch start
#iterations: 183
currently lose_sum: 587.0556178092957
time_elpased: 1.828
batch start
#iterations: 184
currently lose_sum: 587.980749964714
time_elpased: 1.879
batch start
#iterations: 185
currently lose_sum: 587.1423501968384
time_elpased: 1.857
batch start
#iterations: 186
currently lose_sum: 588.3549317717552
time_elpased: 1.903
batch start
#iterations: 187
currently lose_sum: 587.2403556108475
time_elpased: 1.927
batch start
#iterations: 188
currently lose_sum: 586.1672335267067
time_elpased: 1.857
batch start
#iterations: 189
currently lose_sum: 588.1937626600266
time_elpased: 1.939
batch start
#iterations: 190
currently lose_sum: 587.3456754088402
time_elpased: 1.871
batch start
#iterations: 191
currently lose_sum: 585.8204018473625
time_elpased: 1.911
batch start
#iterations: 192
currently lose_sum: 587.523852288723
time_elpased: 1.859
batch start
#iterations: 193
currently lose_sum: 587.54009360075
time_elpased: 1.912
batch start
#iterations: 194
currently lose_sum: 585.5402173399925
time_elpased: 1.89
batch start
#iterations: 195
currently lose_sum: 587.4010173082352
time_elpased: 1.982
batch start
#iterations: 196
currently lose_sum: 586.4158226847649
time_elpased: 1.916
batch start
#iterations: 197
currently lose_sum: 589.0450647473335
time_elpased: 1.875
batch start
#iterations: 198
currently lose_sum: 587.9550473690033
time_elpased: 1.934
batch start
#iterations: 199
currently lose_sum: 586.5073766708374
time_elpased: 1.901
start validation test
0.580165745856
0.565850018654
0.936503035916
0.705453699756
0
validation finish
batch start
#iterations: 200
currently lose_sum: 588.8094635009766
time_elpased: 1.966
batch start
#iterations: 201
currently lose_sum: 584.7691873311996
time_elpased: 1.945
batch start
#iterations: 202
currently lose_sum: 586.0716434717178
time_elpased: 1.855
batch start
#iterations: 203
currently lose_sum: 586.0681281685829
time_elpased: 1.837
batch start
#iterations: 204
currently lose_sum: 587.1190412044525
time_elpased: 1.844
batch start
#iterations: 205
currently lose_sum: 587.1858150362968
time_elpased: 1.85
batch start
#iterations: 206
currently lose_sum: 587.5149661302567
time_elpased: 1.892
batch start
#iterations: 207
currently lose_sum: 588.5456851124763
time_elpased: 1.853
batch start
#iterations: 208
currently lose_sum: 588.3747302889824
time_elpased: 1.872
batch start
#iterations: 209
currently lose_sum: 588.8782983422279
time_elpased: 1.892
batch start
#iterations: 210
currently lose_sum: 587.0779193937778
time_elpased: 1.898
batch start
#iterations: 211
currently lose_sum: 587.8128606677055
time_elpased: 1.854
batch start
#iterations: 212
currently lose_sum: 586.0868569016457
time_elpased: 1.837
batch start
#iterations: 213
currently lose_sum: 587.8831362128258
time_elpased: 1.886
batch start
#iterations: 214
currently lose_sum: 585.8608644604683
time_elpased: 1.961
batch start
#iterations: 215
currently lose_sum: 586.1463579535484
time_elpased: 1.87
batch start
#iterations: 216
currently lose_sum: 586.9067711830139
time_elpased: 1.899
batch start
#iterations: 217
currently lose_sum: 587.6616407036781
time_elpased: 1.943
batch start
#iterations: 218
currently lose_sum: 588.6042145490646
time_elpased: 1.903
batch start
#iterations: 219
currently lose_sum: 588.0529394745827
time_elpased: 1.872
start validation test
0.585110497238
0.570642260552
0.917567150355
0.703667896533
0
validation finish
batch start
#iterations: 220
currently lose_sum: 586.1609961986542
time_elpased: 1.922
batch start
#iterations: 221
currently lose_sum: 587.4710815548897
time_elpased: 1.868
batch start
#iterations: 222
currently lose_sum: 588.5515266060829
time_elpased: 1.851
batch start
#iterations: 223
currently lose_sum: 586.7281858325005
time_elpased: 1.919
batch start
#iterations: 224
currently lose_sum: 589.0392338633537
time_elpased: 1.905
batch start
#iterations: 225
currently lose_sum: 586.7755207419395
time_elpased: 1.845
batch start
#iterations: 226
currently lose_sum: 588.6050854325294
time_elpased: 1.907
batch start
#iterations: 227
currently lose_sum: 585.6544909477234
time_elpased: 1.914
batch start
#iterations: 228
currently lose_sum: 585.2539936304092
time_elpased: 1.936
batch start
#iterations: 229
currently lose_sum: 587.734690964222
time_elpased: 1.914
batch start
#iterations: 230
currently lose_sum: 585.8071876764297
time_elpased: 1.939
batch start
#iterations: 231
currently lose_sum: 586.7988001704216
time_elpased: 1.968
batch start
#iterations: 232
currently lose_sum: 587.941343665123
time_elpased: 1.907
batch start
#iterations: 233
currently lose_sum: 587.867450594902
time_elpased: 1.889
batch start
#iterations: 234
currently lose_sum: 587.0032002925873
time_elpased: 1.907
batch start
#iterations: 235
currently lose_sum: 586.3995312452316
time_elpased: 1.859
batch start
#iterations: 236
currently lose_sum: 587.0046195983887
time_elpased: 1.864
batch start
#iterations: 237
currently lose_sum: 585.7576382756233
time_elpased: 1.918
batch start
#iterations: 238
currently lose_sum: 587.6871159076691
time_elpased: 1.954
batch start
#iterations: 239
currently lose_sum: 585.1322454810143
time_elpased: 1.893
start validation test
0.576878453039
0.562776371565
0.949572913451
0.706711345141
0
validation finish
batch start
#iterations: 240
currently lose_sum: 585.4125700592995
time_elpased: 1.894
batch start
#iterations: 241
currently lose_sum: 585.7600041031837
time_elpased: 1.918
batch start
#iterations: 242
currently lose_sum: 587.3342497944832
time_elpased: 1.919
batch start
#iterations: 243
currently lose_sum: 587.4591498970985
time_elpased: 1.892
batch start
#iterations: 244
currently lose_sum: 587.9983430504799
time_elpased: 1.898
batch start
#iterations: 245
currently lose_sum: 587.0361920595169
time_elpased: 1.907
batch start
#iterations: 246
currently lose_sum: 586.0793865323067
time_elpased: 1.865
batch start
#iterations: 247
currently lose_sum: 587.6831262111664
time_elpased: 1.886
batch start
#iterations: 248
currently lose_sum: 588.0521693825722
time_elpased: 1.903
batch start
#iterations: 249
currently lose_sum: 587.5075514316559
time_elpased: 1.864
batch start
#iterations: 250
currently lose_sum: 587.4249329566956
time_elpased: 1.887
batch start
#iterations: 251
currently lose_sum: 586.9035249948502
time_elpased: 1.954
batch start
#iterations: 252
currently lose_sum: 585.6102764904499
time_elpased: 1.932
batch start
#iterations: 253
currently lose_sum: 587.4468308091164
time_elpased: 1.963
batch start
#iterations: 254
currently lose_sum: 585.7730031013489
time_elpased: 1.86
batch start
#iterations: 255
currently lose_sum: 585.9044642448425
time_elpased: 1.94
batch start
#iterations: 256
currently lose_sum: 586.7495873570442
time_elpased: 1.89
batch start
#iterations: 257
currently lose_sum: 588.5815856456757
time_elpased: 1.948
batch start
#iterations: 258
currently lose_sum: 586.1251903772354
time_elpased: 1.836
batch start
#iterations: 259
currently lose_sum: 585.8220380544662
time_elpased: 1.872
start validation test
0.576602209945
0.563043011083
0.943706905423
0.705289672544
0
validation finish
batch start
#iterations: 260
currently lose_sum: 586.0839472413063
time_elpased: 1.89
batch start
#iterations: 261
currently lose_sum: 587.9295024871826
time_elpased: 1.816
batch start
#iterations: 262
currently lose_sum: 584.9451444149017
time_elpased: 1.83
batch start
#iterations: 263
currently lose_sum: 588.9653915762901
time_elpased: 1.871
batch start
#iterations: 264
currently lose_sum: 585.7766906619072
time_elpased: 1.85
batch start
#iterations: 265
currently lose_sum: 587.8126636743546
time_elpased: 1.835
batch start
#iterations: 266
currently lose_sum: 586.6118606925011
time_elpased: 1.822
batch start
#iterations: 267
currently lose_sum: 588.2094188928604
time_elpased: 1.807
batch start
#iterations: 268
currently lose_sum: 585.0702233016491
time_elpased: 1.857
batch start
#iterations: 269
currently lose_sum: 586.96551913023
time_elpased: 1.85
batch start
#iterations: 270
currently lose_sum: 584.9630752801895
time_elpased: 1.846
batch start
#iterations: 271
currently lose_sum: 586.6081089377403
time_elpased: 1.822
batch start
#iterations: 272
currently lose_sum: 587.4594486951828
time_elpased: 1.883
batch start
#iterations: 273
currently lose_sum: 587.5953974723816
time_elpased: 1.827
batch start
#iterations: 274
currently lose_sum: 587.1688334941864
time_elpased: 1.857
batch start
#iterations: 275
currently lose_sum: 586.8645131587982
time_elpased: 1.84
batch start
#iterations: 276
currently lose_sum: 586.4728803038597
time_elpased: 1.843
batch start
#iterations: 277
currently lose_sum: 586.8630732297897
time_elpased: 1.849
batch start
#iterations: 278
currently lose_sum: 585.4525330066681
time_elpased: 1.874
batch start
#iterations: 279
currently lose_sum: 586.2765780091286
time_elpased: 1.881
start validation test
0.582016574586
0.567663616064
0.928784604302
0.704651480549
0
validation finish
batch start
#iterations: 280
currently lose_sum: 588.7608942985535
time_elpased: 1.939
batch start
#iterations: 281
currently lose_sum: 586.0925262570381
time_elpased: 1.847
batch start
#iterations: 282
currently lose_sum: 585.4635909199715
time_elpased: 1.942
batch start
#iterations: 283
currently lose_sum: 585.9948096871376
time_elpased: 1.891
batch start
#iterations: 284
currently lose_sum: 586.7229108214378
time_elpased: 1.908
batch start
#iterations: 285
currently lose_sum: 586.5485174655914
time_elpased: 1.883
batch start
#iterations: 286
currently lose_sum: 588.1155245304108
time_elpased: 1.858
batch start
#iterations: 287
currently lose_sum: 586.8362343907356
time_elpased: 1.834
batch start
#iterations: 288
currently lose_sum: 587.4176934361458
time_elpased: 1.871
batch start
#iterations: 289
currently lose_sum: 585.8432074189186
time_elpased: 1.858
batch start
#iterations: 290
currently lose_sum: 586.528384745121
time_elpased: 1.85
batch start
#iterations: 291
currently lose_sum: 587.4952080845833
time_elpased: 1.89
batch start
#iterations: 292
currently lose_sum: 588.1814419031143
time_elpased: 1.868
batch start
#iterations: 293
currently lose_sum: 587.9128081202507
time_elpased: 1.935
batch start
#iterations: 294
currently lose_sum: 585.7727807164192
time_elpased: 1.847
batch start
#iterations: 295
currently lose_sum: 589.4159483909607
time_elpased: 1.949
batch start
#iterations: 296
currently lose_sum: 586.2896339297295
time_elpased: 1.93
batch start
#iterations: 297
currently lose_sum: 586.1125284433365
time_elpased: 1.974
batch start
#iterations: 298
currently lose_sum: 586.697758436203
time_elpased: 1.94
batch start
#iterations: 299
currently lose_sum: 587.955572783947
time_elpased: 1.924
start validation test
0.582044198895
0.567410098985
0.932077801791
0.705401300674
0
validation finish
batch start
#iterations: 300
currently lose_sum: 585.7215459942818
time_elpased: 1.972
batch start
#iterations: 301
currently lose_sum: 586.2040460109711
time_elpased: 1.945
batch start
#iterations: 302
currently lose_sum: 584.795978307724
time_elpased: 1.954
batch start
#iterations: 303
currently lose_sum: 585.3240685462952
time_elpased: 1.962
batch start
#iterations: 304
currently lose_sum: 585.4786588549614
time_elpased: 1.961
batch start
#iterations: 305
currently lose_sum: 586.1411813497543
time_elpased: 1.939
batch start
#iterations: 306
currently lose_sum: 588.0954198241234
time_elpased: 1.935
batch start
#iterations: 307
currently lose_sum: 587.0699399709702
time_elpased: 1.947
batch start
#iterations: 308
currently lose_sum: 586.2248656749725
time_elpased: 1.923
batch start
#iterations: 309
currently lose_sum: 586.6225824654102
time_elpased: 1.94
batch start
#iterations: 310
currently lose_sum: 584.9834131598473
time_elpased: 1.853
batch start
#iterations: 311
currently lose_sum: 588.0579489469528
time_elpased: 1.875
batch start
#iterations: 312
currently lose_sum: 586.2854146957397
time_elpased: 1.835
batch start
#iterations: 313
currently lose_sum: 585.2123607993126
time_elpased: 1.872
batch start
#iterations: 314
currently lose_sum: 585.0878853797913
time_elpased: 1.899
batch start
#iterations: 315
currently lose_sum: 585.6184282898903
time_elpased: 1.902
batch start
#iterations: 316
currently lose_sum: 587.7055117487907
time_elpased: 1.905
batch start
#iterations: 317
currently lose_sum: 584.6106259226799
time_elpased: 1.885
batch start
#iterations: 318
currently lose_sum: 586.5115692615509
time_elpased: 1.954
batch start
#iterations: 319
currently lose_sum: 587.3799121379852
time_elpased: 1.92
start validation test
0.585331491713
0.571336408503
0.911392405063
0.702369386339
0
validation finish
batch start
#iterations: 320
currently lose_sum: 585.9983326792717
time_elpased: 1.965
batch start
#iterations: 321
currently lose_sum: 587.869479060173
time_elpased: 1.909
batch start
#iterations: 322
currently lose_sum: 585.6928799152374
time_elpased: 1.861
batch start
#iterations: 323
currently lose_sum: 586.5532936453819
time_elpased: 1.872
batch start
#iterations: 324
currently lose_sum: 586.2653471827507
time_elpased: 1.91
batch start
#iterations: 325
currently lose_sum: 585.3402887582779
time_elpased: 1.936
batch start
#iterations: 326
currently lose_sum: 584.7538036108017
time_elpased: 1.935
batch start
#iterations: 327
currently lose_sum: 587.4574189186096
time_elpased: 1.966
batch start
#iterations: 328
currently lose_sum: 587.370328783989
time_elpased: 1.88
batch start
#iterations: 329
currently lose_sum: 584.4505624771118
time_elpased: 1.94
batch start
#iterations: 330
currently lose_sum: 585.5731804966927
time_elpased: 1.921
batch start
#iterations: 331
currently lose_sum: 588.5794515013695
time_elpased: 1.931
batch start
#iterations: 332
currently lose_sum: 586.8498938083649
time_elpased: 1.882
batch start
#iterations: 333
currently lose_sum: 587.3342214822769
time_elpased: 1.957
batch start
#iterations: 334
currently lose_sum: 586.6176898479462
time_elpased: 1.961
batch start
#iterations: 335
currently lose_sum: 586.9565846920013
time_elpased: 1.905
batch start
#iterations: 336
currently lose_sum: 583.8828241229057
time_elpased: 1.915
batch start
#iterations: 337
currently lose_sum: 584.6634477376938
time_elpased: 1.882
batch start
#iterations: 338
currently lose_sum: 586.7511225938797
time_elpased: 1.865
batch start
#iterations: 339
currently lose_sum: 585.5437445640564
time_elpased: 1.861
start validation test
0.587044198895
0.574672838067
0.888031285376
0.697786313555
0
validation finish
batch start
#iterations: 340
currently lose_sum: 587.380449295044
time_elpased: 1.924
batch start
#iterations: 341
currently lose_sum: 586.3866642713547
time_elpased: 1.867
batch start
#iterations: 342
currently lose_sum: 587.6836524009705
time_elpased: 1.843
batch start
#iterations: 343
currently lose_sum: 586.0479572415352
time_elpased: 1.85
batch start
#iterations: 344
currently lose_sum: 587.2533160448074
time_elpased: 1.846
batch start
#iterations: 345
currently lose_sum: 585.7417331337929
time_elpased: 1.872
batch start
#iterations: 346
currently lose_sum: 587.8871156573296
time_elpased: 1.815
batch start
#iterations: 347
currently lose_sum: 585.4142721891403
time_elpased: 1.817
batch start
#iterations: 348
currently lose_sum: 585.8749595880508
time_elpased: 1.839
batch start
#iterations: 349
currently lose_sum: 584.5474347472191
time_elpased: 1.839
batch start
#iterations: 350
currently lose_sum: 584.4731175899506
time_elpased: 1.921
batch start
#iterations: 351
currently lose_sum: 587.185300886631
time_elpased: 1.869
batch start
#iterations: 352
currently lose_sum: 586.5099091529846
time_elpased: 1.853
batch start
#iterations: 353
currently lose_sum: 583.8748598098755
time_elpased: 1.868
batch start
#iterations: 354
currently lose_sum: 586.5201278924942
time_elpased: 1.834
batch start
#iterations: 355
currently lose_sum: 584.9322091937065
time_elpased: 1.9
batch start
#iterations: 356
currently lose_sum: 587.3675181269646
time_elpased: 1.879
batch start
#iterations: 357
currently lose_sum: 587.6125361323357
time_elpased: 1.939
batch start
#iterations: 358
currently lose_sum: 588.1462862491608
time_elpased: 1.848
batch start
#iterations: 359
currently lose_sum: 586.6724529266357
time_elpased: 1.905
start validation test
0.584060773481
0.569162215972
0.926726355871
0.705209781311
0
validation finish
batch start
#iterations: 360
currently lose_sum: 582.3481009602547
time_elpased: 1.908
batch start
#iterations: 361
currently lose_sum: 584.3362836241722
time_elpased: 1.909
batch start
#iterations: 362
currently lose_sum: 586.5472808480263
time_elpased: 1.949
batch start
#iterations: 363
currently lose_sum: 586.4433250427246
time_elpased: 1.892
batch start
#iterations: 364
currently lose_sum: 587.8469964265823
time_elpased: 1.922
batch start
#iterations: 365
currently lose_sum: 585.8662689328194
time_elpased: 1.894
batch start
#iterations: 366
currently lose_sum: 586.1992126703262
time_elpased: 1.821
batch start
#iterations: 367
currently lose_sum: 584.8536318540573
time_elpased: 1.891
batch start
#iterations: 368
currently lose_sum: 585.9359175562859
time_elpased: 1.898
batch start
#iterations: 369
currently lose_sum: 586.539640545845
time_elpased: 1.877
batch start
#iterations: 370
currently lose_sum: 586.4079768657684
time_elpased: 1.873
batch start
#iterations: 371
currently lose_sum: 586.0252812504768
time_elpased: 1.901
batch start
#iterations: 372
currently lose_sum: 585.7656366229057
time_elpased: 1.854
batch start
#iterations: 373
currently lose_sum: 586.2982214391232
time_elpased: 1.862
batch start
#iterations: 374
currently lose_sum: 583.5768957138062
time_elpased: 1.859
batch start
#iterations: 375
currently lose_sum: 587.5412147045135
time_elpased: 1.818
batch start
#iterations: 376
currently lose_sum: 585.5750932693481
time_elpased: 1.889
batch start
#iterations: 377
currently lose_sum: 586.6912534832954
time_elpased: 1.86
batch start
#iterations: 378
currently lose_sum: 586.7063590884209
time_elpased: 1.838
batch start
#iterations: 379
currently lose_sum: 586.0135744810104
time_elpased: 1.837
start validation test
0.583701657459
0.568832807571
0.927858392508
0.705284155357
0
validation finish
batch start
#iterations: 380
currently lose_sum: 586.1375902891159
time_elpased: 1.899
batch start
#iterations: 381
currently lose_sum: 585.6863451004028
time_elpased: 1.886
batch start
#iterations: 382
currently lose_sum: 587.0188217759132
time_elpased: 1.915
batch start
#iterations: 383
currently lose_sum: 584.4614142775536
time_elpased: 1.906
batch start
#iterations: 384
currently lose_sum: 586.441287279129
time_elpased: 1.855
batch start
#iterations: 385
currently lose_sum: 586.5883004069328
time_elpased: 1.846
batch start
#iterations: 386
currently lose_sum: 586.0514930486679
time_elpased: 1.838
batch start
#iterations: 387
currently lose_sum: 588.1848591566086
time_elpased: 1.902
batch start
#iterations: 388
currently lose_sum: 586.6184750795364
time_elpased: 1.845
batch start
#iterations: 389
currently lose_sum: 585.7574683427811
time_elpased: 1.841
batch start
#iterations: 390
currently lose_sum: 584.9980497956276
time_elpased: 1.83
batch start
#iterations: 391
currently lose_sum: 585.0679915547371
time_elpased: 1.835
batch start
#iterations: 392
currently lose_sum: 586.8382013440132
time_elpased: 1.825
batch start
#iterations: 393
currently lose_sum: 586.9047678112984
time_elpased: 1.848
batch start
#iterations: 394
currently lose_sum: 586.6803787350655
time_elpased: 1.822
batch start
#iterations: 395
currently lose_sum: 585.3816481232643
time_elpased: 1.89
batch start
#iterations: 396
currently lose_sum: 586.8995429873466
time_elpased: 1.843
batch start
#iterations: 397
currently lose_sum: 585.4354526400566
time_elpased: 1.856
batch start
#iterations: 398
currently lose_sum: 585.7977187633514
time_elpased: 1.849
batch start
#iterations: 399
currently lose_sum: 584.742759168148
time_elpased: 1.89
start validation test
0.583232044199
0.5681582991
0.932283626634
0.706038228475
0
validation finish
acc: 0.574
pre: 0.562
rec: 0.943
F1: 0.704
auc: 0.000
