start to construct graph
graph construct over
92947
epochs start
batch start
#iterations: 0
currently lose_sum: 609.984211564064
time_elpased: 5.593
batch start
#iterations: 1
currently lose_sum: 603.076886177063
time_elpased: 5.742
batch start
#iterations: 2
currently lose_sum: 600.7159969210625
time_elpased: 5.668
batch start
#iterations: 3
currently lose_sum: 594.6145409941673
time_elpased: 5.728
batch start
#iterations: 4
currently lose_sum: 593.1215169429779
time_elpased: 5.548
batch start
#iterations: 5
currently lose_sum: 589.6740849018097
time_elpased: 5.712
batch start
#iterations: 6
currently lose_sum: 587.4534544348717
time_elpased: 5.697
batch start
#iterations: 7
currently lose_sum: 586.1095540523529
time_elpased: 5.651
batch start
#iterations: 8
currently lose_sum: 584.5263946056366
time_elpased: 5.87
batch start
#iterations: 9
currently lose_sum: 583.1889404952526
time_elpased: 5.503
batch start
#iterations: 10
currently lose_sum: 585.4695402383804
time_elpased: 5.762
batch start
#iterations: 11
currently lose_sum: 582.4150054454803
time_elpased: 5.648
batch start
#iterations: 12
currently lose_sum: 582.8341233730316
time_elpased: 5.688
batch start
#iterations: 13
currently lose_sum: 579.5239562392235
time_elpased: 5.961
batch start
#iterations: 14
currently lose_sum: 582.7521013617516
time_elpased: 6.134
batch start
#iterations: 15
currently lose_sum: 578.3659796714783
time_elpased: 6.018
batch start
#iterations: 16
currently lose_sum: 578.3369058668613
time_elpased: 5.832
batch start
#iterations: 17
currently lose_sum: 575.9275951385498
time_elpased: 5.796
batch start
#iterations: 18
currently lose_sum: 577.3450082540512
time_elpased: 5.687
batch start
#iterations: 19
currently lose_sum: 575.5859261155128
time_elpased: 5.922
start validation test
0.580914127424
0.569232253707
0.910672018113
0.70056407719
0
validation finish
batch start
#iterations: 20
currently lose_sum: 576.6600106358528
time_elpased: 5.378
batch start
#iterations: 21
currently lose_sum: 574.6123090982437
time_elpased: 5.867
batch start
#iterations: 22
currently lose_sum: 574.5323857665062
time_elpased: 5.593
batch start
#iterations: 23
currently lose_sum: 575.4228058457375
time_elpased: 5.741
batch start
#iterations: 24
currently lose_sum: 573.9906443953514
time_elpased: 5.503
batch start
#iterations: 25
currently lose_sum: 574.6620219945908
time_elpased: 5.729
batch start
#iterations: 26
currently lose_sum: 575.3089692592621
time_elpased: 5.592
batch start
#iterations: 27
currently lose_sum: 571.8862389922142
time_elpased: 5.714
batch start
#iterations: 28
currently lose_sum: 573.4278973937035
time_elpased: 5.945
batch start
#iterations: 29
currently lose_sum: 573.1095221936703
time_elpased: 6.002
batch start
#iterations: 30
currently lose_sum: 573.2649037241936
time_elpased: 5.492
batch start
#iterations: 31
currently lose_sum: 571.9851279556751
time_elpased: 6.029
batch start
#iterations: 32
currently lose_sum: 571.2050574719906
time_elpased: 5.71
batch start
#iterations: 33
currently lose_sum: 570.6871252954006
time_elpased: 5.888
batch start
#iterations: 34
currently lose_sum: 570.9181887209415
time_elpased: 5.498
batch start
#iterations: 35
currently lose_sum: 570.9265338182449
time_elpased: 5.628
batch start
#iterations: 36
currently lose_sum: 572.5316087901592
time_elpased: 5.578
batch start
#iterations: 37
currently lose_sum: 571.3681388497353
time_elpased: 5.522
batch start
#iterations: 38
currently lose_sum: 570.0351376831532
time_elpased: 5.682
batch start
#iterations: 39
currently lose_sum: 569.4608051776886
time_elpased: 5.611
start validation test
0.588725761773
0.575491260985
0.899660389009
0.701957241795
0
validation finish
batch start
#iterations: 40
currently lose_sum: 568.3891043663025
time_elpased: 5.872
batch start
#iterations: 41
currently lose_sum: 568.5928933918476
time_elpased: 5.613
batch start
#iterations: 42
currently lose_sum: 568.9571163356304
time_elpased: 5.839
batch start
#iterations: 43
currently lose_sum: 569.9646052718163
time_elpased: 5.703
batch start
#iterations: 44
currently lose_sum: 568.4131003022194
time_elpased: 5.663
batch start
#iterations: 45
currently lose_sum: 566.6828981637955
time_elpased: 5.607
batch start
#iterations: 46
currently lose_sum: 570.7070043981075
time_elpased: 5.7
batch start
#iterations: 47
currently lose_sum: 568.110765337944
time_elpased: 5.848
batch start
#iterations: 48
currently lose_sum: 570.4909804463387
time_elpased: 5.759
batch start
#iterations: 49
currently lose_sum: 569.1039069890976
time_elpased: 5.54
batch start
#iterations: 50
currently lose_sum: 568.3693285882473
time_elpased: 5.692
batch start
#iterations: 51
currently lose_sum: 568.4402086138725
time_elpased: 5.737
batch start
#iterations: 52
currently lose_sum: 568.6491070091724
time_elpased: 5.839
batch start
#iterations: 53
currently lose_sum: 567.8841938972473
time_elpased: 5.355
batch start
#iterations: 54
currently lose_sum: 567.9138269126415
time_elpased: 5.813
batch start
#iterations: 55
currently lose_sum: 566.410730600357
time_elpased: 5.778
batch start
#iterations: 56
currently lose_sum: 565.5791355669498
time_elpased: 5.738
batch start
#iterations: 57
currently lose_sum: 566.8744854331017
time_elpased: 5.605
batch start
#iterations: 58
currently lose_sum: 566.202377140522
time_elpased: 6.021
batch start
#iterations: 59
currently lose_sum: 567.234880566597
time_elpased: 6.041
start validation test
0.586011080332
0.572545977569
0.911495317485
0.703313282909
0
validation finish
batch start
#iterations: 60
currently lose_sum: 566.6745154857635
time_elpased: 5.787
batch start
#iterations: 61
currently lose_sum: 568.0846210718155
time_elpased: 5.625
batch start
#iterations: 62
currently lose_sum: 567.3146833777428
time_elpased: 5.641
batch start
#iterations: 63
currently lose_sum: 565.3047842681408
time_elpased: 5.827
batch start
#iterations: 64
currently lose_sum: 566.2311210632324
time_elpased: 5.435
batch start
#iterations: 65
currently lose_sum: 565.1856726408005
time_elpased: 5.938
batch start
#iterations: 66
currently lose_sum: 564.3983352482319
time_elpased: 5.724
batch start
#iterations: 67
currently lose_sum: 566.3027649819851
time_elpased: 5.549
batch start
#iterations: 68
currently lose_sum: 563.8683229088783
time_elpased: 5.983
batch start
#iterations: 69
currently lose_sum: 562.5550745725632
time_elpased: 5.472
batch start
#iterations: 70
currently lose_sum: 563.7110700905323
time_elpased: 6.055
batch start
#iterations: 71
currently lose_sum: 565.140548735857
time_elpased: 5.641
batch start
#iterations: 72
currently lose_sum: 563.741461277008
time_elpased: 5.562
batch start
#iterations: 73
currently lose_sum: 563.3587615191936
time_elpased: 5.596
batch start
#iterations: 74
currently lose_sum: 564.2866452336311
time_elpased: 5.888
batch start
#iterations: 75
currently lose_sum: 565.3903574943542
time_elpased: 5.926
batch start
#iterations: 76
currently lose_sum: 564.6983075141907
time_elpased: 5.723
batch start
#iterations: 77
currently lose_sum: 565.266176700592
time_elpased: 5.653
batch start
#iterations: 78
currently lose_sum: 561.9186581969261
time_elpased: 5.858
batch start
#iterations: 79
currently lose_sum: 566.0782185196877
time_elpased: 5.672
start validation test
0.586177285319
0.572134672786
0.91725841309
0.704710323971
0
validation finish
batch start
#iterations: 80
currently lose_sum: 564.4222799837589
time_elpased: 5.977
batch start
#iterations: 81
currently lose_sum: 564.1701397299767
time_elpased: 5.544
batch start
#iterations: 82
currently lose_sum: 563.6913506388664
time_elpased: 5.782
batch start
#iterations: 83
currently lose_sum: 563.3776715099812
time_elpased: 5.49
batch start
#iterations: 84
currently lose_sum: 561.9637126922607
time_elpased: 5.847
batch start
#iterations: 85
currently lose_sum: 565.927615493536
time_elpased: 5.689
batch start
#iterations: 86
currently lose_sum: 562.6141627430916
time_elpased: 5.668
batch start
#iterations: 87
currently lose_sum: 564.5706807076931
time_elpased: 5.734
batch start
#iterations: 88
currently lose_sum: 562.6511578559875
time_elpased: 5.495
batch start
#iterations: 89
currently lose_sum: 562.6545470058918
time_elpased: 5.896
batch start
#iterations: 90
currently lose_sum: 564.3733369708061
time_elpased: 5.583
batch start
#iterations: 91
currently lose_sum: 560.8065302371979
time_elpased: 5.748
batch start
#iterations: 92
currently lose_sum: 565.1629889905453
time_elpased: 5.376
batch start
#iterations: 93
currently lose_sum: 562.6595799922943
time_elpased: 5.897
batch start
#iterations: 94
currently lose_sum: 562.2924102544785
time_elpased: 6.275
batch start
#iterations: 95
currently lose_sum: 562.360493183136
time_elpased: 6.157
batch start
#iterations: 96
currently lose_sum: 561.764641314745
time_elpased: 5.992
batch start
#iterations: 97
currently lose_sum: 564.1225938498974
time_elpased: 5.533
batch start
#iterations: 98
currently lose_sum: 561.9791817963123
time_elpased: 5.843
batch start
#iterations: 99
currently lose_sum: 562.1369113922119
time_elpased: 5.396
start validation test
0.591412742382
0.581240460663
0.862200267572
0.694376528117
0
validation finish
batch start
#iterations: 100
currently lose_sum: 559.8375661373138
time_elpased: 5.766
batch start
#iterations: 101
currently lose_sum: 561.5902015566826
time_elpased: 5.608
batch start
#iterations: 102
currently lose_sum: 559.8654340803623
time_elpased: 5.764
batch start
#iterations: 103
currently lose_sum: 561.8278462588787
time_elpased: 5.64
batch start
#iterations: 104
currently lose_sum: 562.0632255673409
time_elpased: 5.694
batch start
#iterations: 105
currently lose_sum: 560.7670851647854
time_elpased: 5.967
batch start
#iterations: 106
currently lose_sum: 562.6930944025517
time_elpased: 5.778
batch start
#iterations: 107
currently lose_sum: 560.7935212254524
time_elpased: 5.754
batch start
#iterations: 108
currently lose_sum: 559.6817583441734
time_elpased: 5.992
batch start
#iterations: 109
currently lose_sum: 559.7993575334549
time_elpased: 5.763
batch start
#iterations: 110
currently lose_sum: 561.1736969649792
time_elpased: 5.73
batch start
#iterations: 111
currently lose_sum: 562.2949036061764
time_elpased: 5.822
batch start
#iterations: 112
currently lose_sum: 560.1211187839508
time_elpased: 5.548
batch start
#iterations: 113
currently lose_sum: 562.295346736908
time_elpased: 5.945
batch start
#iterations: 114
currently lose_sum: 560.6673918068409
time_elpased: 5.584
batch start
#iterations: 115
currently lose_sum: 561.4731034338474
time_elpased: 5.748
batch start
#iterations: 116
currently lose_sum: 559.6456565260887
time_elpased: 5.581
batch start
#iterations: 117
currently lose_sum: 560.9315896630287
time_elpased: 5.577
batch start
#iterations: 118
currently lose_sum: 559.1818471252918
time_elpased: 5.874
batch start
#iterations: 119
currently lose_sum: 560.2778881788254
time_elpased: 5.626
start validation test
0.588670360111
0.57734741388
0.880518678605
0.697409981049
0
validation finish
batch start
#iterations: 120
currently lose_sum: 560.380786895752
time_elpased: 5.953
batch start
#iterations: 121
currently lose_sum: 559.4155242741108
time_elpased: 5.553
batch start
#iterations: 122
currently lose_sum: 561.6166468560696
time_elpased: 5.681
batch start
#iterations: 123
currently lose_sum: 559.0830943584442
time_elpased: 5.462
batch start
#iterations: 124
currently lose_sum: 558.051967561245
time_elpased: 5.748
batch start
#iterations: 125
currently lose_sum: 560.6590741574764
time_elpased: 5.692
batch start
#iterations: 126
currently lose_sum: 557.1512160301208
time_elpased: 5.65
batch start
#iterations: 127
currently lose_sum: 558.4844031631947
time_elpased: 5.975
batch start
#iterations: 128
currently lose_sum: 560.0043617486954
time_elpased: 5.774
batch start
#iterations: 129
currently lose_sum: 558.4995567500591
time_elpased: 5.78
batch start
#iterations: 130
currently lose_sum: 560.2342157959938
time_elpased: 5.852
batch start
#iterations: 131
currently lose_sum: 558.557703435421
time_elpased: 5.563
batch start
#iterations: 132
currently lose_sum: 561.2854900360107
time_elpased: 5.787
batch start
#iterations: 133
currently lose_sum: 558.8599357008934
time_elpased: 5.437
batch start
#iterations: 134
currently lose_sum: 557.7286121249199
time_elpased: 5.979
batch start
#iterations: 135
currently lose_sum: 558.4271684587002
time_elpased: 5.72
batch start
#iterations: 136
currently lose_sum: 557.6329335272312
time_elpased: 5.681
batch start
#iterations: 137
currently lose_sum: 560.8298860490322
time_elpased: 5.772
batch start
#iterations: 138
currently lose_sum: 559.532619446516
time_elpased: 5.478
batch start
#iterations: 139
currently lose_sum: 558.7097985744476
time_elpased: 5.776
start validation test
0.592354570637
0.580605480763
0.874343933313
0.697823408624
0
validation finish
batch start
#iterations: 140
currently lose_sum: 561.5151391923428
time_elpased: 5.814
batch start
#iterations: 141
currently lose_sum: 558.4933151602745
time_elpased: 5.918
batch start
#iterations: 142
currently lose_sum: 556.8516047894955
time_elpased: 5.952
batch start
#iterations: 143
currently lose_sum: 558.7365048527718
time_elpased: 5.467
batch start
#iterations: 144
currently lose_sum: 558.0828149020672
time_elpased: 5.908
batch start
#iterations: 145
currently lose_sum: 558.8935273587704
time_elpased: 5.578
batch start
#iterations: 146
currently lose_sum: 557.4124884903431
time_elpased: 5.623
batch start
#iterations: 147
currently lose_sum: 556.8117462098598
time_elpased: 5.675
batch start
#iterations: 148
currently lose_sum: 560.2682774066925
time_elpased: 5.759
batch start
#iterations: 149
currently lose_sum: 559.052383929491
time_elpased: 5.921
batch start
#iterations: 150
currently lose_sum: 559.5679042935371
time_elpased: 5.585
batch start
#iterations: 151
currently lose_sum: 559.1504576206207
time_elpased: 6.081
batch start
#iterations: 152
currently lose_sum: 558.8351560235023
time_elpased: 6.012
batch start
#iterations: 153
currently lose_sum: 557.1760602891445
time_elpased: 5.532
batch start
#iterations: 154
currently lose_sum: 556.3191567361355
time_elpased: 5.751
batch start
#iterations: 155
currently lose_sum: 560.3265188634396
time_elpased: 5.861
batch start
#iterations: 156
currently lose_sum: 556.2027762234211
time_elpased: 5.684
batch start
#iterations: 157
currently lose_sum: 557.9708535075188
time_elpased: 5.901
batch start
#iterations: 158
currently lose_sum: 558.5373784005642
time_elpased: 5.749
batch start
#iterations: 159
currently lose_sum: 558.0918560028076
time_elpased: 5.773
start validation test
0.590775623269
0.578555297131
0.883194401564
0.699130363944
0
validation finish
batch start
#iterations: 160
currently lose_sum: 555.9964161217213
time_elpased: 5.923
batch start
#iterations: 161
currently lose_sum: 557.3153316676617
time_elpased: 5.487
batch start
#iterations: 162
currently lose_sum: 555.4449517726898
time_elpased: 5.884
batch start
#iterations: 163
currently lose_sum: 556.9250054955482
time_elpased: 5.803
batch start
#iterations: 164
currently lose_sum: 557.7501254975796
time_elpased: 5.728
batch start
#iterations: 165
currently lose_sum: 559.788244754076
time_elpased: 5.996
batch start
#iterations: 166
currently lose_sum: 558.0401473343372
time_elpased: 5.662
batch start
#iterations: 167
currently lose_sum: 557.5287078619003
time_elpased: 5.856
batch start
#iterations: 168
currently lose_sum: 557.7269431650639
time_elpased: 5.949
batch start
#iterations: 169
currently lose_sum: 556.7402964234352
time_elpased: 5.734
batch start
#iterations: 170
currently lose_sum: 556.3752432763577
time_elpased: 5.928
batch start
#iterations: 171
currently lose_sum: 557.6238676011562
time_elpased: 5.915
batch start
#iterations: 172
currently lose_sum: 557.4967582821846
time_elpased: 5.882
batch start
#iterations: 173
currently lose_sum: 559.7299710214138
time_elpased: 6.133
batch start
#iterations: 174
currently lose_sum: 556.8884820640087
time_elpased: 5.724
batch start
#iterations: 175
currently lose_sum: 557.9797259271145
time_elpased: 6.047
batch start
#iterations: 176
currently lose_sum: 557.9314116537571
time_elpased: 5.848
batch start
#iterations: 177
currently lose_sum: 558.059132963419
time_elpased: 5.78
batch start
#iterations: 178
currently lose_sum: 560.2866686582565
time_elpased: 5.606
batch start
#iterations: 179
currently lose_sum: 558.4953543841839
time_elpased: 5.786
start validation test
0.591274238227
0.579930985001
0.873417721519
0.697041250026
0
validation finish
batch start
#iterations: 180
currently lose_sum: 557.4207267463207
time_elpased: 5.645
batch start
#iterations: 181
currently lose_sum: 557.3955000340939
time_elpased: 5.798
batch start
#iterations: 182
currently lose_sum: 555.9581079185009
time_elpased: 5.619
batch start
#iterations: 183
currently lose_sum: 557.1868086457253
time_elpased: 5.895
batch start
#iterations: 184
currently lose_sum: 554.8492601811886
time_elpased: 5.804
batch start
#iterations: 185
currently lose_sum: 555.4780321121216
time_elpased: 5.732
batch start
#iterations: 186
currently lose_sum: 556.3450994491577
time_elpased: 5.977
batch start
#iterations: 187
currently lose_sum: 557.3601201474667
time_elpased: 5.532
batch start
#iterations: 188
currently lose_sum: 555.5866542160511
time_elpased: 5.939
batch start
#iterations: 189
currently lose_sum: 557.6150169074535
time_elpased: 5.961
batch start
#iterations: 190
currently lose_sum: 556.2298224270344
time_elpased: 5.923
batch start
#iterations: 191
currently lose_sum: 558.0477887094021
time_elpased: 6.045
batch start
#iterations: 192
currently lose_sum: 558.8247316479683
time_elpased: 5.952
batch start
#iterations: 193
currently lose_sum: 556.3737350702286
time_elpased: 5.682
batch start
#iterations: 194
currently lose_sum: 558.3705244660378
time_elpased: 5.771
batch start
#iterations: 195
currently lose_sum: 558.0470691621304
time_elpased: 5.577
batch start
#iterations: 196
currently lose_sum: 556.4837751090527
time_elpased: 5.777
batch start
#iterations: 197
currently lose_sum: 557.0332172811031
time_elpased: 5.775
batch start
#iterations: 198
currently lose_sum: 555.8110798299313
time_elpased: 5.599
batch start
#iterations: 199
currently lose_sum: 554.4795414209366
time_elpased: 5.828
start validation test
0.591329639889
0.578945593146
0.883194401564
0.699415252338
0
validation finish
batch start
#iterations: 200
currently lose_sum: 558.0111993253231
time_elpased: 5.633
batch start
#iterations: 201
currently lose_sum: 554.4938398301601
time_elpased: 5.902
batch start
#iterations: 202
currently lose_sum: 557.4496577084064
time_elpased: 5.794
batch start
#iterations: 203
currently lose_sum: 558.6564209163189
time_elpased: 5.923
batch start
#iterations: 204
currently lose_sum: 556.2565002441406
time_elpased: 5.927
batch start
#iterations: 205
currently lose_sum: 556.3226439356804
time_elpased: 6.025
batch start
#iterations: 206
currently lose_sum: 556.9830156564713
time_elpased: 5.62
batch start
#iterations: 207
currently lose_sum: 556.7908875644207
time_elpased: 5.794
batch start
#iterations: 208
currently lose_sum: 554.6924294829369
time_elpased: 5.779
batch start
#iterations: 209
currently lose_sum: 556.1611510813236
time_elpased: 5.589
batch start
#iterations: 210
currently lose_sum: 555.486399024725
time_elpased: 5.734
batch start
#iterations: 211
currently lose_sum: 558.37838986516
time_elpased: 5.612
batch start
#iterations: 212
currently lose_sum: 556.067913711071
time_elpased: 5.987
batch start
#iterations: 213
currently lose_sum: 557.5939317643642
time_elpased: 5.674
batch start
#iterations: 214
currently lose_sum: 556.1484214961529
time_elpased: 5.792
batch start
#iterations: 215
currently lose_sum: 557.4960106313229
time_elpased: 5.962
batch start
#iterations: 216
currently lose_sum: 555.2806439399719
time_elpased: 5.978
batch start
#iterations: 217
currently lose_sum: 555.0076197087765
time_elpased: 5.417
batch start
#iterations: 218
currently lose_sum: 555.364804059267
time_elpased: 5.805
batch start
#iterations: 219
currently lose_sum: 556.1986760497093
time_elpased: 5.653
start validation test
0.591689750693
0.5794892637
0.880415766183
0.698937908497
0
validation finish
batch start
#iterations: 220
currently lose_sum: 554.6622238457203
time_elpased: 5.612
batch start
#iterations: 221
currently lose_sum: 553.1477479636669
time_elpased: 5.612
batch start
#iterations: 222
currently lose_sum: 557.3044766783714
time_elpased: 5.701
batch start
#iterations: 223
currently lose_sum: 554.145768225193
time_elpased: 5.842
batch start
#iterations: 224
currently lose_sum: 557.1712839901447
time_elpased: 5.567
batch start
#iterations: 225
currently lose_sum: 556.3962199985981
time_elpased: 5.985
batch start
#iterations: 226
currently lose_sum: 553.732061445713
time_elpased: 5.547
batch start
#iterations: 227
currently lose_sum: 556.5820849835873
time_elpased: 5.696
batch start
#iterations: 228
currently lose_sum: 556.6546948254108
time_elpased: 5.756
batch start
#iterations: 229
currently lose_sum: 555.2750964462757
time_elpased: 5.753
batch start
#iterations: 230
currently lose_sum: 555.886332988739
time_elpased: 5.934
batch start
#iterations: 231
currently lose_sum: 555.5434909164906
time_elpased: 5.661
batch start
#iterations: 232
currently lose_sum: 555.8964948654175
time_elpased: 5.961
batch start
#iterations: 233
currently lose_sum: 554.7128856778145
time_elpased: 5.633
batch start
#iterations: 234
currently lose_sum: 555.0190853774548
time_elpased: 5.679
batch start
#iterations: 235
currently lose_sum: 556.6918926239014
time_elpased: 5.855
batch start
#iterations: 236
currently lose_sum: 553.8312074542046
time_elpased: 5.455
batch start
#iterations: 237
currently lose_sum: 555.2678889632225
time_elpased: 5.774
batch start
#iterations: 238
currently lose_sum: 556.2236195802689
time_elpased: 5.467
batch start
#iterations: 239
currently lose_sum: 553.9034761786461
time_elpased: 5.837
start validation test
0.590997229917
0.579138275874
0.879077904703
0.698260887336
0
validation finish
batch start
#iterations: 240
currently lose_sum: 554.3613584637642
time_elpased: 5.75
batch start
#iterations: 241
currently lose_sum: 553.8386087715626
time_elpased: 5.678
batch start
#iterations: 242
currently lose_sum: 555.1865618824959
time_elpased: 5.75
batch start
#iterations: 243
currently lose_sum: 555.5306347310543
time_elpased: 5.732
batch start
#iterations: 244
currently lose_sum: 555.3182128965855
time_elpased: 5.906
batch start
#iterations: 245
currently lose_sum: 554.9199447631836
time_elpased: 5.656
batch start
#iterations: 246
currently lose_sum: 553.9698760211468
time_elpased: 5.837
batch start
#iterations: 247
currently lose_sum: 556.2277042865753
time_elpased: 5.935
batch start
#iterations: 248
currently lose_sum: 554.6560995280743
time_elpased: 5.529
batch start
#iterations: 249
currently lose_sum: 555.0994397103786
time_elpased: 5.764
batch start
#iterations: 250
currently lose_sum: 551.8839358985424
time_elpased: 5.587
batch start
#iterations: 251
currently lose_sum: 552.9014147818089
time_elpased: 5.857
batch start
#iterations: 252
currently lose_sum: 556.242744743824
time_elpased: 5.881
batch start
#iterations: 253
currently lose_sum: 555.3541228473186
time_elpased: 5.513
batch start
#iterations: 254
currently lose_sum: 554.2380021810532
time_elpased: 5.895
batch start
#iterations: 255
currently lose_sum: 555.3862082064152
time_elpased: 5.642
batch start
#iterations: 256
currently lose_sum: 553.3847145736217
time_elpased: 5.972
batch start
#iterations: 257
currently lose_sum: 555.9827920794487
time_elpased: 5.84
batch start
#iterations: 258
currently lose_sum: 554.2535364329815
time_elpased: 5.645
batch start
#iterations: 259
currently lose_sum: 553.5024332106113
time_elpased: 5.89
start validation test
0.592049861496
0.579475230473
0.882988576721
0.699736986972
0
validation finish
batch start
#iterations: 260
currently lose_sum: 554.3932614028454
time_elpased: 5.869
batch start
#iterations: 261
currently lose_sum: 556.4102194011211
time_elpased: 5.774
batch start
#iterations: 262
currently lose_sum: 554.2649042904377
time_elpased: 5.897
batch start
#iterations: 263
currently lose_sum: 552.5902709960938
time_elpased: 5.74
batch start
#iterations: 264
currently lose_sum: 556.4264741837978
time_elpased: 5.757
batch start
#iterations: 265
currently lose_sum: 555.778744995594
time_elpased: 5.528
batch start
#iterations: 266
currently lose_sum: 554.9018552005291
time_elpased: 5.685
batch start
#iterations: 267
currently lose_sum: 554.2729519307613
time_elpased: 5.635
batch start
#iterations: 268
currently lose_sum: 553.9994285106659
time_elpased: 5.614
batch start
#iterations: 269
currently lose_sum: 553.6428311765194
time_elpased: 5.722
batch start
#iterations: 270
currently lose_sum: 553.7566972672939
time_elpased: 5.477
batch start
#iterations: 271
currently lose_sum: 554.0898016393185
time_elpased: 5.754
batch start
#iterations: 272
currently lose_sum: 554.4228518307209
time_elpased: 5.538
batch start
#iterations: 273
currently lose_sum: 553.4091871082783
time_elpased: 5.786
batch start
#iterations: 274
currently lose_sum: 554.2657331228256
time_elpased: 5.604
batch start
#iterations: 275
currently lose_sum: 554.0478109717369
time_elpased: 5.789
batch start
#iterations: 276
currently lose_sum: 554.7545936703682
time_elpased: 5.729
batch start
#iterations: 277
currently lose_sum: 554.0877769291401
time_elpased: 5.572
batch start
#iterations: 278
currently lose_sum: 554.0605064928532
time_elpased: 5.395
batch start
#iterations: 279
currently lose_sum: 554.6028919219971
time_elpased: 5.934
start validation test
0.590747922438
0.578535796144
0.883194401564
0.699116125616
0
validation finish
batch start
#iterations: 280
currently lose_sum: 553.1808374524117
time_elpased: 5.889
batch start
#iterations: 281
currently lose_sum: 554.166455745697
time_elpased: 5.952
batch start
#iterations: 282
currently lose_sum: 552.8615964055061
time_elpased: 5.702
batch start
#iterations: 283
currently lose_sum: 555.6915461719036
time_elpased: 5.721
batch start
#iterations: 284
currently lose_sum: 554.1415033042431
time_elpased: 5.776
batch start
#iterations: 285
currently lose_sum: 553.2080104351044
time_elpased: 5.678
batch start
#iterations: 286
currently lose_sum: 552.5317462086678
time_elpased: 5.456
batch start
#iterations: 287
currently lose_sum: 554.598705559969
time_elpased: 5.833
batch start
#iterations: 288
currently lose_sum: 554.625193297863
time_elpased: 5.523
batch start
#iterations: 289
currently lose_sum: 553.1713767945766
time_elpased: 5.824
batch start
#iterations: 290
currently lose_sum: 553.3239331841469
time_elpased: 5.974
batch start
#iterations: 291
currently lose_sum: 554.8161105513573
time_elpased: 5.594
batch start
#iterations: 292
currently lose_sum: 554.6733578443527
time_elpased: 6.087
batch start
#iterations: 293
currently lose_sum: 553.3909619152546
time_elpased: 5.693
batch start
#iterations: 294
currently lose_sum: 553.6312502622604
time_elpased: 5.646
batch start
#iterations: 295
currently lose_sum: 554.990036636591
time_elpased: 5.682
batch start
#iterations: 296
currently lose_sum: 555.4838632643223
time_elpased: 5.554
batch start
#iterations: 297
currently lose_sum: 554.0235576331615
time_elpased: 5.874
batch start
#iterations: 298
currently lose_sum: 554.9696826636791
time_elpased: 5.698
batch start
#iterations: 299
currently lose_sum: 552.926530122757
time_elpased: 5.842
start validation test
0.590443213296
0.577849224689
0.887825460533
0.700058831883
0
validation finish
batch start
#iterations: 300
currently lose_sum: 554.1997977495193
time_elpased: 5.813
batch start
#iterations: 301
currently lose_sum: 553.9446003437042
time_elpased: 5.709
batch start
#iterations: 302
currently lose_sum: 553.9374058544636
time_elpased: 5.747
batch start
#iterations: 303
currently lose_sum: 552.7936142086983
time_elpased: 5.552
batch start
#iterations: 304
currently lose_sum: 554.3864514529705
time_elpased: 5.872
batch start
#iterations: 305
currently lose_sum: 552.9091992378235
time_elpased: 5.761
batch start
#iterations: 306
currently lose_sum: 552.2820431888103
time_elpased: 5.652
batch start
#iterations: 307
currently lose_sum: 553.7536576390266
time_elpased: 5.61
batch start
#iterations: 308
currently lose_sum: 554.0903104841709
time_elpased: 5.925
batch start
#iterations: 309
currently lose_sum: 554.942350924015
time_elpased: 5.864
batch start
#iterations: 310
currently lose_sum: 553.1375201046467
time_elpased: 5.433
batch start
#iterations: 311
currently lose_sum: 551.6401214003563
time_elpased: 5.986
batch start
#iterations: 312
currently lose_sum: 552.7376971840858
time_elpased: 5.801
batch start
#iterations: 313
currently lose_sum: 552.3733221590519
time_elpased: 5.653
batch start
#iterations: 314
currently lose_sum: 554.3897031843662
time_elpased: 5.855
batch start
#iterations: 315
currently lose_sum: 551.9668508470058
time_elpased: 5.818
batch start
#iterations: 316
currently lose_sum: 551.06757158041
time_elpased: 5.691
batch start
#iterations: 317
currently lose_sum: 550.225036084652
time_elpased: 5.709
batch start
#iterations: 318
currently lose_sum: 553.2830454111099
time_elpased: 5.761
batch start
#iterations: 319
currently lose_sum: 553.3015067577362
time_elpased: 5.947
start validation test
0.59108033241
0.578759271746
0.883297313986
0.69931152483
0
validation finish
batch start
#iterations: 320
currently lose_sum: 553.1453382372856
time_elpased: 5.68
batch start
#iterations: 321
currently lose_sum: 553.1107010543346
time_elpased: 5.745
batch start
#iterations: 322
currently lose_sum: 553.7466877698898
time_elpased: 5.842
batch start
#iterations: 323
currently lose_sum: 551.9750654101372
time_elpased: 5.587
batch start
#iterations: 324
currently lose_sum: 554.3221040964127
time_elpased: 5.595
batch start
#iterations: 325
currently lose_sum: 555.1367703676224
time_elpased: 5.785
batch start
#iterations: 326
currently lose_sum: 553.802025437355
time_elpased: 5.881
batch start
#iterations: 327
currently lose_sum: 554.6082602739334
time_elpased: 5.348
batch start
#iterations: 328
currently lose_sum: 553.2366338074207
time_elpased: 5.766
batch start
#iterations: 329
currently lose_sum: 553.349465161562
time_elpased: 7.128
batch start
#iterations: 330
currently lose_sum: 551.9525441229343
time_elpased: 5.785
batch start
#iterations: 331
currently lose_sum: 552.6388273239136
time_elpased: 5.88
batch start
#iterations: 332
currently lose_sum: 554.9264217615128
time_elpased: 5.596
batch start
#iterations: 333
currently lose_sum: 553.0138056576252
time_elpased: 5.583
batch start
#iterations: 334
currently lose_sum: 553.1894680261612
time_elpased: 5.64
batch start
#iterations: 335
currently lose_sum: 551.9991475045681
time_elpased: 5.824
batch start
#iterations: 336
currently lose_sum: 552.9542406201363
time_elpased: 5.542
batch start
#iterations: 337
currently lose_sum: 551.4801521599293
time_elpased: 5.858
batch start
#iterations: 338
currently lose_sum: 554.4261273443699
time_elpased: 5.51
batch start
#iterations: 339
currently lose_sum: 554.5597381293774
time_elpased: 5.845
start validation test
0.590249307479
0.577985350447
0.885149737573
0.699325148386
0
validation finish
batch start
#iterations: 340
currently lose_sum: 552.6758411228657
time_elpased: 5.817
batch start
#iterations: 341
currently lose_sum: 553.359235137701
time_elpased: 5.811
batch start
#iterations: 342
currently lose_sum: 556.448459059
time_elpased: 5.637
batch start
#iterations: 343
currently lose_sum: 554.0050169825554
time_elpased: 5.453
batch start
#iterations: 344
currently lose_sum: 553.5266412496567
time_elpased: 5.68
batch start
#iterations: 345
currently lose_sum: 552.4898025989532
time_elpased: 5.641
batch start
#iterations: 346
currently lose_sum: 552.281555891037
time_elpased: 6.148
batch start
#iterations: 347
currently lose_sum: 552.9197806715965
time_elpased: 5.707
batch start
#iterations: 348
currently lose_sum: 553.8153946101665
time_elpased: 5.798
batch start
#iterations: 349
currently lose_sum: 553.796898573637
time_elpased: 5.758
batch start
#iterations: 350
currently lose_sum: 553.2319526672363
time_elpased: 5.674
batch start
#iterations: 351
currently lose_sum: 553.9176030158997
time_elpased: 5.899
batch start
#iterations: 352
currently lose_sum: 553.9769032597542
time_elpased: 5.517
batch start
#iterations: 353
currently lose_sum: 552.991931527853
time_elpased: 5.91
batch start
#iterations: 354
currently lose_sum: 553.1029885113239
time_elpased: 5.909
batch start
#iterations: 355
currently lose_sum: 553.5399281680584
time_elpased: 5.767
batch start
#iterations: 356
currently lose_sum: 553.0612026453018
time_elpased: 5.708
batch start
#iterations: 357
currently lose_sum: 555.0899816155434
time_elpased: 5.765
batch start
#iterations: 358
currently lose_sum: 552.3514966070652
time_elpased: 5.684
batch start
#iterations: 359
currently lose_sum: 554.1854467391968
time_elpased: 5.904
start validation test
0.590526315789
0.578783362688
0.879283729546
0.698067731525
0
validation finish
batch start
#iterations: 360
currently lose_sum: 552.3927311897278
time_elpased: 5.733
batch start
#iterations: 361
currently lose_sum: 551.9016178250313
time_elpased: 5.461
batch start
#iterations: 362
currently lose_sum: 551.7959794402122
time_elpased: 5.528
batch start
#iterations: 363
currently lose_sum: 552.9144653975964
time_elpased: 5.863
batch start
#iterations: 364
currently lose_sum: 552.6677402853966
time_elpased: 5.86
batch start
#iterations: 365
currently lose_sum: 552.6307584047318
time_elpased: 5.52
batch start
#iterations: 366
currently lose_sum: 553.4147826135159
time_elpased: 5.961
batch start
#iterations: 367
currently lose_sum: 552.8689182698727
time_elpased: 5.825
batch start
#iterations: 368
currently lose_sum: 553.052631765604
time_elpased: 5.655
batch start
#iterations: 369
currently lose_sum: 552.4095166623592
time_elpased: 5.798
batch start
#iterations: 370
currently lose_sum: 554.055209338665
time_elpased: 5.725
batch start
#iterations: 371
currently lose_sum: 553.3470931649208
time_elpased: 5.893
batch start
#iterations: 372
currently lose_sum: 552.6001023054123
time_elpased: 5.535
batch start
#iterations: 373
currently lose_sum: 551.6930820643902
time_elpased: 5.907
batch start
#iterations: 374
currently lose_sum: 552.8816978931427
time_elpased: 5.724
batch start
#iterations: 375
currently lose_sum: 553.228837043047
time_elpased: 5.627
batch start
#iterations: 376
currently lose_sum: 551.8458393514156
time_elpased: 5.668
batch start
#iterations: 377
currently lose_sum: 551.9636205136776
time_elpased: 5.827
batch start
#iterations: 378
currently lose_sum: 550.8738961219788
time_elpased: 5.914
batch start
#iterations: 379
currently lose_sum: 553.3498768210411
time_elpased: 5.873
start validation test
0.590914127424
0.578325387766
0.886384686632
0.699959366111
0
validation finish
batch start
#iterations: 380
currently lose_sum: 552.7021619975567
time_elpased: 5.681
batch start
#iterations: 381
currently lose_sum: 549.5996927022934
time_elpased: 5.713
batch start
#iterations: 382
currently lose_sum: 552.0077003240585
time_elpased: 5.799
batch start
#iterations: 383
currently lose_sum: 553.1818126440048
time_elpased: 5.877
batch start
#iterations: 384
currently lose_sum: 552.3211997747421
time_elpased: 5.9
batch start
#iterations: 385
currently lose_sum: 550.9114865958691
time_elpased: 5.566
batch start
#iterations: 386
currently lose_sum: 552.0480473935604
time_elpased: 5.938
batch start
#iterations: 387
currently lose_sum: 554.4052959084511
time_elpased: 5.645
batch start
#iterations: 388
currently lose_sum: 553.1586390435696
time_elpased: 5.926
batch start
#iterations: 389
currently lose_sum: 553.0949397981167
time_elpased: 6.006
batch start
#iterations: 390
currently lose_sum: 552.6703401505947
time_elpased: 5.855
batch start
#iterations: 391
currently lose_sum: 553.1451492905617
time_elpased: 5.723
batch start
#iterations: 392
currently lose_sum: 557.3229385316372
time_elpased: 5.616
batch start
#iterations: 393
currently lose_sum: 551.9384083747864
time_elpased: 5.918
batch start
#iterations: 394
currently lose_sum: 553.9996320903301
time_elpased: 5.814
batch start
#iterations: 395
currently lose_sum: 552.9919985234737
time_elpased: 5.928
batch start
#iterations: 396
currently lose_sum: 553.2228657007217
time_elpased: 5.696
batch start
#iterations: 397
currently lose_sum: 553.4792691171169
time_elpased: 5.653
batch start
#iterations: 398
currently lose_sum: 552.3688997626305
time_elpased: 5.833
batch start
#iterations: 399
currently lose_sum: 554.8319263160229
time_elpased: 5.672
start validation test
0.59108033241
0.578961600865
0.881341977977
0.698845322127
0
validation finish
acc: 0.580
pre: 0.567
rec: 0.913
F1: 0.700
auc: 0.000
