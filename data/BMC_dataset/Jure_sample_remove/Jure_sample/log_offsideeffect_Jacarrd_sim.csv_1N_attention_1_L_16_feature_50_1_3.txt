start to construct graph
graph construct over
93195
epochs start
batch start
#iterations: 0
currently lose_sum: 612.1053040623665
time_elpased: 6.088
batch start
#iterations: 1
currently lose_sum: 605.0104725956917
time_elpased: 5.812
batch start
#iterations: 2
currently lose_sum: 602.2134284377098
time_elpased: 5.81
batch start
#iterations: 3
currently lose_sum: 597.8358612656593
time_elpased: 5.443
batch start
#iterations: 4
currently lose_sum: 594.5982186198235
time_elpased: 5.909
batch start
#iterations: 5
currently lose_sum: 591.3603402376175
time_elpased: 5.532
batch start
#iterations: 6
currently lose_sum: 590.9731532931328
time_elpased: 5.72
batch start
#iterations: 7
currently lose_sum: 590.2653495669365
time_elpased: 5.525
batch start
#iterations: 8
currently lose_sum: 586.7829646468163
time_elpased: 5.876
batch start
#iterations: 9
currently lose_sum: 587.2087296247482
time_elpased: 5.567
batch start
#iterations: 10
currently lose_sum: 583.6515838503838
time_elpased: 5.837
batch start
#iterations: 11
currently lose_sum: 583.7796373963356
time_elpased: 5.653
batch start
#iterations: 12
currently lose_sum: 584.9796835780144
time_elpased: 5.58
batch start
#iterations: 13
currently lose_sum: 582.6434696614742
time_elpased: 5.54
batch start
#iterations: 14
currently lose_sum: 580.994588971138
time_elpased: 5.689
batch start
#iterations: 15
currently lose_sum: 581.9454964399338
time_elpased: 5.91
batch start
#iterations: 16
currently lose_sum: 579.8214031755924
time_elpased: 5.822
batch start
#iterations: 17
currently lose_sum: 581.2926503717899
time_elpased: 5.688
batch start
#iterations: 18
currently lose_sum: 579.0895906090736
time_elpased: 5.966
batch start
#iterations: 19
currently lose_sum: 576.6122922599316
time_elpased: 5.462
start validation test
0.580831024931
0.573139202613
0.867023466447
0.690095846645
0
validation finish
batch start
#iterations: 20
currently lose_sum: 577.9724791049957
time_elpased: 5.815
batch start
#iterations: 21
currently lose_sum: 579.2008576393127
time_elpased: 5.59
batch start
#iterations: 22
currently lose_sum: 576.9354077577591
time_elpased: 5.805
batch start
#iterations: 23
currently lose_sum: 577.7610241770744
time_elpased: 5.834
batch start
#iterations: 24
currently lose_sum: 577.42500269413
time_elpased: 5.614
batch start
#iterations: 25
currently lose_sum: 575.2129751443863
time_elpased: 5.689
batch start
#iterations: 26
currently lose_sum: 576.020061314106
time_elpased: 5.647
batch start
#iterations: 27
currently lose_sum: 574.1592219471931
time_elpased: 5.809
batch start
#iterations: 28
currently lose_sum: 575.4491540491581
time_elpased: 5.658
batch start
#iterations: 29
currently lose_sum: 575.9621830880642
time_elpased: 5.748
batch start
#iterations: 30
currently lose_sum: 575.0211141109467
time_elpased: 5.713
batch start
#iterations: 31
currently lose_sum: 573.2326391339302
time_elpased: 5.783
batch start
#iterations: 32
currently lose_sum: 574.0471678376198
time_elpased: 5.718
batch start
#iterations: 33
currently lose_sum: 574.4133223295212
time_elpased: 5.779
batch start
#iterations: 34
currently lose_sum: 574.2924880385399
time_elpased: 5.862
batch start
#iterations: 35
currently lose_sum: 572.7117264866829
time_elpased: 5.821
batch start
#iterations: 36
currently lose_sum: 574.157832711935
time_elpased: 5.716
batch start
#iterations: 37
currently lose_sum: 574.1205995082855
time_elpased: 5.802
batch start
#iterations: 38
currently lose_sum: 572.3357770442963
time_elpased: 5.531
batch start
#iterations: 39
currently lose_sum: 572.0573167204857
time_elpased: 6.018
start validation test
0.583490304709
0.573384081197
0.883799917662
0.695528916248
0
validation finish
batch start
#iterations: 40
currently lose_sum: 572.0346282124519
time_elpased: 5.977
batch start
#iterations: 41
currently lose_sum: 571.8233171403408
time_elpased: 5.551
batch start
#iterations: 42
currently lose_sum: 570.9495067596436
time_elpased: 5.88
batch start
#iterations: 43
currently lose_sum: 573.0112604498863
time_elpased: 5.583
batch start
#iterations: 44
currently lose_sum: 572.1137828230858
time_elpased: 5.896
batch start
#iterations: 45
currently lose_sum: 569.1747115254402
time_elpased: 6.108
batch start
#iterations: 46
currently lose_sum: 571.0012083947659
time_elpased: 5.724
batch start
#iterations: 47
currently lose_sum: 570.5981906950474
time_elpased: 5.875
batch start
#iterations: 48
currently lose_sum: 571.8633831143379
time_elpased: 5.547
batch start
#iterations: 49
currently lose_sum: 568.122948884964
time_elpased: 5.92
batch start
#iterations: 50
currently lose_sum: 570.5225360393524
time_elpased: 6.031
batch start
#iterations: 51
currently lose_sum: 571.4137116372585
time_elpased: 5.631
batch start
#iterations: 52
currently lose_sum: 568.5744416117668
time_elpased: 5.86
batch start
#iterations: 53
currently lose_sum: 568.2413746416569
time_elpased: 5.646
batch start
#iterations: 54
currently lose_sum: 569.2158396542072
time_elpased: 5.88
batch start
#iterations: 55
currently lose_sum: 569.4326896369457
time_elpased: 5.831
batch start
#iterations: 56
currently lose_sum: 569.7430409491062
time_elpased: 5.741
batch start
#iterations: 57
currently lose_sum: 570.603611946106
time_elpased: 5.763
batch start
#iterations: 58
currently lose_sum: 569.2888227403164
time_elpased: 5.852
batch start
#iterations: 59
currently lose_sum: 569.4760488271713
time_elpased: 5.644
start validation test
0.58404432133
0.573806658644
0.883388225607
0.695712085596
0
validation finish
batch start
#iterations: 60
currently lose_sum: 569.3632111847401
time_elpased: 5.757
batch start
#iterations: 61
currently lose_sum: 567.8576567173004
time_elpased: 5.695
batch start
#iterations: 62
currently lose_sum: 567.0573044717312
time_elpased: 5.65
batch start
#iterations: 63
currently lose_sum: 565.6869225800037
time_elpased: 5.882
batch start
#iterations: 64
currently lose_sum: 567.4029552340508
time_elpased: 5.534
batch start
#iterations: 65
currently lose_sum: 568.7844600379467
time_elpased: 5.784
batch start
#iterations: 66
currently lose_sum: 568.2365941405296
time_elpased: 5.897
batch start
#iterations: 67
currently lose_sum: 567.2256160080433
time_elpased: 5.443
batch start
#iterations: 68
currently lose_sum: 567.4113264381886
time_elpased: 5.721
batch start
#iterations: 69
currently lose_sum: 568.9494235515594
time_elpased: 5.593
batch start
#iterations: 70
currently lose_sum: 566.4030941724777
time_elpased: 5.976
batch start
#iterations: 71
currently lose_sum: 566.3702412247658
time_elpased: 5.778
batch start
#iterations: 72
currently lose_sum: 566.7815059125423
time_elpased: 5.792
batch start
#iterations: 73
currently lose_sum: 566.9042673110962
time_elpased: 5.834
batch start
#iterations: 74
currently lose_sum: 565.8853502571583
time_elpased: 5.678
batch start
#iterations: 75
currently lose_sum: 564.2382967174053
time_elpased: 5.618
batch start
#iterations: 76
currently lose_sum: 567.2016680538654
time_elpased: 5.553
batch start
#iterations: 77
currently lose_sum: 567.4014817774296
time_elpased: 5.856
batch start
#iterations: 78
currently lose_sum: 568.1240613460541
time_elpased: 5.85
batch start
#iterations: 79
currently lose_sum: 563.3764275610447
time_elpased: 5.509
start validation test
0.58728531856
0.577199495896
0.872066694113
0.694636305876
0
validation finish
batch start
#iterations: 80
currently lose_sum: 564.9931743443012
time_elpased: 5.678
batch start
#iterations: 81
currently lose_sum: 566.3461077809334
time_elpased: 5.539
batch start
#iterations: 82
currently lose_sum: 565.7287106513977
time_elpased: 5.731
batch start
#iterations: 83
currently lose_sum: 565.2661137878895
time_elpased: 5.805
batch start
#iterations: 84
currently lose_sum: 564.7517776787281
time_elpased: 5.775
batch start
#iterations: 85
currently lose_sum: 565.8774207532406
time_elpased: 5.713
batch start
#iterations: 86
currently lose_sum: 564.6133985519409
time_elpased: 5.873
batch start
#iterations: 87
currently lose_sum: 564.3536539673805
time_elpased: 5.633
batch start
#iterations: 88
currently lose_sum: 565.2705153226852
time_elpased: 5.717
batch start
#iterations: 89
currently lose_sum: 565.8877061605453
time_elpased: 5.983
batch start
#iterations: 90
currently lose_sum: 565.4798229336739
time_elpased: 5.789
batch start
#iterations: 91
currently lose_sum: 563.7110027074814
time_elpased: 5.73
batch start
#iterations: 92
currently lose_sum: 564.380372107029
time_elpased: 5.545
batch start
#iterations: 93
currently lose_sum: 562.8926846683025
time_elpased: 6.898
batch start
#iterations: 94
currently lose_sum: 564.7400680780411
time_elpased: 6.034
batch start
#iterations: 95
currently lose_sum: 565.8841517269611
time_elpased: 5.85
batch start
#iterations: 96
currently lose_sum: 565.7245100736618
time_elpased: 5.575
batch start
#iterations: 97
currently lose_sum: 562.7002180814743
time_elpased: 5.814
batch start
#iterations: 98
currently lose_sum: 564.3311444222927
time_elpased: 5.723
batch start
#iterations: 99
currently lose_sum: 564.688835978508
time_elpased: 5.633
start validation test
0.586343490305
0.575942743324
0.877933305887
0.695574174872
0
validation finish
batch start
#iterations: 100
currently lose_sum: 563.6690981090069
time_elpased: 5.478
batch start
#iterations: 101
currently lose_sum: 561.3119888007641
time_elpased: 5.94
batch start
#iterations: 102
currently lose_sum: 564.4748913645744
time_elpased: 5.868
batch start
#iterations: 103
currently lose_sum: 562.0816273987293
time_elpased: 5.595
batch start
#iterations: 104
currently lose_sum: 566.139529466629
time_elpased: 5.832
batch start
#iterations: 105
currently lose_sum: 564.1698994636536
time_elpased: 5.889
batch start
#iterations: 106
currently lose_sum: 563.5753785073757
time_elpased: 5.493
batch start
#iterations: 107
currently lose_sum: 563.3177414238453
time_elpased: 6.022
batch start
#iterations: 108
currently lose_sum: 560.9986622929573
time_elpased: 5.834
batch start
#iterations: 109
currently lose_sum: 564.0836315751076
time_elpased: 6.052
batch start
#iterations: 110
currently lose_sum: 563.268820464611
time_elpased: 6.114
batch start
#iterations: 111
currently lose_sum: 563.131244212389
time_elpased: 5.438
batch start
#iterations: 112
currently lose_sum: 563.6542460620403
time_elpased: 5.596
batch start
#iterations: 113
currently lose_sum: 561.118236809969
time_elpased: 5.704
batch start
#iterations: 114
currently lose_sum: 565.2135069072247
time_elpased: 5.834
batch start
#iterations: 115
currently lose_sum: 565.8769167661667
time_elpased: 5.553
batch start
#iterations: 116
currently lose_sum: 564.4818634390831
time_elpased: 5.808
batch start
#iterations: 117
currently lose_sum: 562.410192579031
time_elpased: 5.839
batch start
#iterations: 118
currently lose_sum: 564.3787181079388
time_elpased: 5.797
batch start
#iterations: 119
currently lose_sum: 563.1509709656239
time_elpased: 5.838
start validation test
0.587783933518
0.580267381565
0.846541786744
0.688558213516
0
validation finish
batch start
#iterations: 120
currently lose_sum: 562.8289794027805
time_elpased: 5.698
batch start
#iterations: 121
currently lose_sum: 560.6896117329597
time_elpased: 5.796
batch start
#iterations: 122
currently lose_sum: 562.5196309089661
time_elpased: 5.771
batch start
#iterations: 123
currently lose_sum: 562.5159213244915
time_elpased: 5.457
batch start
#iterations: 124
currently lose_sum: 562.4866679310799
time_elpased: 5.917
batch start
#iterations: 125
currently lose_sum: 563.1839470565319
time_elpased: 5.774
batch start
#iterations: 126
currently lose_sum: 564.3068977594376
time_elpased: 5.743
batch start
#iterations: 127
currently lose_sum: 561.5974870026112
time_elpased: 5.99
batch start
#iterations: 128
currently lose_sum: 563.1430085897446
time_elpased: 5.863
batch start
#iterations: 129
currently lose_sum: 561.3263637423515
time_elpased: 5.52
batch start
#iterations: 130
currently lose_sum: 563.6275380849838
time_elpased: 5.739
batch start
#iterations: 131
currently lose_sum: 562.0403354167938
time_elpased: 5.639
batch start
#iterations: 132
currently lose_sum: 562.3865166902542
time_elpased: 5.78
batch start
#iterations: 133
currently lose_sum: 561.1181941628456
time_elpased: 5.433
batch start
#iterations: 134
currently lose_sum: 562.0559476315975
time_elpased: 6.07
batch start
#iterations: 135
currently lose_sum: 561.5918405056
time_elpased: 5.6
batch start
#iterations: 136
currently lose_sum: 559.8692417144775
time_elpased: 5.657
batch start
#iterations: 137
currently lose_sum: 562.8890688717365
time_elpased: 5.934
batch start
#iterations: 138
currently lose_sum: 561.9551642537117
time_elpased: 5.712
batch start
#iterations: 139
currently lose_sum: 561.4916673898697
time_elpased: 5.435
start validation test
0.58756232687
0.578195462839
0.864347468094
0.692889998144
0
validation finish
batch start
#iterations: 140
currently lose_sum: 561.1022023260593
time_elpased: 5.977
batch start
#iterations: 141
currently lose_sum: 561.4022080302238
time_elpased: 6.031
batch start
#iterations: 142
currently lose_sum: 560.3827029764652
time_elpased: 6.004
batch start
#iterations: 143
currently lose_sum: 561.6446057856083
time_elpased: 5.449
batch start
#iterations: 144
currently lose_sum: 561.3413732945919
time_elpased: 5.887
batch start
#iterations: 145
currently lose_sum: 560.2963478565216
time_elpased: 5.856
batch start
#iterations: 146
currently lose_sum: 562.7402050197124
time_elpased: 5.537
batch start
#iterations: 147
currently lose_sum: 559.5998999476433
time_elpased: 5.739
batch start
#iterations: 148
currently lose_sum: 560.8464477658272
time_elpased: 5.6
batch start
#iterations: 149
currently lose_sum: 563.8423909246922
time_elpased: 5.817
batch start
#iterations: 150
currently lose_sum: 562.0739786624908
time_elpased: 5.794
batch start
#iterations: 151
currently lose_sum: 559.3224800229073
time_elpased: 5.578
batch start
#iterations: 152
currently lose_sum: 560.6295706629753
time_elpased: 5.772
batch start
#iterations: 153
currently lose_sum: 560.3626874387264
time_elpased: 5.686
batch start
#iterations: 154
currently lose_sum: 561.0036506950855
time_elpased: 5.94
batch start
#iterations: 155
currently lose_sum: 561.5832112729549
time_elpased: 5.63
batch start
#iterations: 156
currently lose_sum: 558.7245016098022
time_elpased: 5.677
batch start
#iterations: 157
currently lose_sum: 561.0793799757957
time_elpased: 5.804
batch start
#iterations: 158
currently lose_sum: 561.4980427026749
time_elpased: 5.506
batch start
#iterations: 159
currently lose_sum: 561.0080763995647
time_elpased: 5.813
start validation test
0.586620498615
0.578425574843
0.855701934953
0.690259241578
0
validation finish
batch start
#iterations: 160
currently lose_sum: 561.3030643165112
time_elpased: 5.532
batch start
#iterations: 161
currently lose_sum: 561.4737519621849
time_elpased: 5.845
batch start
#iterations: 162
currently lose_sum: 560.6481973826885
time_elpased: 5.475
batch start
#iterations: 163
currently lose_sum: 559.8701388537884
time_elpased: 5.946
batch start
#iterations: 164
currently lose_sum: 560.1565875709057
time_elpased: 5.641
batch start
#iterations: 165
currently lose_sum: 559.5800207853317
time_elpased: 5.913
batch start
#iterations: 166
currently lose_sum: 561.3152428865433
time_elpased: 5.836
batch start
#iterations: 167
currently lose_sum: 559.3444649279118
time_elpased: 5.684
batch start
#iterations: 168
currently lose_sum: 560.5397689342499
time_elpased: 5.895
batch start
#iterations: 169
currently lose_sum: 559.6018564403057
time_elpased: 5.477
batch start
#iterations: 170
currently lose_sum: 563.1512203216553
time_elpased: 5.818
batch start
#iterations: 171
currently lose_sum: 559.0227172374725
time_elpased: 5.564
batch start
#iterations: 172
currently lose_sum: 560.3247716128826
time_elpased: 5.777
batch start
#iterations: 173
currently lose_sum: 559.9323560297489
time_elpased: 5.976
batch start
#iterations: 174
currently lose_sum: 560.4403782188892
time_elpased: 5.531
batch start
#iterations: 175
currently lose_sum: 561.151281774044
time_elpased: 5.788
batch start
#iterations: 176
currently lose_sum: 558.3343346714973
time_elpased: 5.838
batch start
#iterations: 177
currently lose_sum: 560.2738760709763
time_elpased: 5.716
batch start
#iterations: 178
currently lose_sum: 562.385400891304
time_elpased: 5.869
batch start
#iterations: 179
currently lose_sum: 563.363060772419
time_elpased: 5.925
start validation test
0.58703601108
0.576110363392
0.881123919308
0.696695963542
0
validation finish
batch start
#iterations: 180
currently lose_sum: 558.0275101661682
time_elpased: 5.546
batch start
#iterations: 181
currently lose_sum: 557.143018335104
time_elpased: 5.947
batch start
#iterations: 182
currently lose_sum: 559.5337029397488
time_elpased: 5.679
batch start
#iterations: 183
currently lose_sum: 560.4496712386608
time_elpased: 5.711
batch start
#iterations: 184
currently lose_sum: 560.2747783958912
time_elpased: 5.684
batch start
#iterations: 185
currently lose_sum: 558.7239702939987
time_elpased: 5.962
batch start
#iterations: 186
currently lose_sum: 560.1969417631626
time_elpased: 5.982
batch start
#iterations: 187
currently lose_sum: 558.8668328821659
time_elpased: 5.456
batch start
#iterations: 188
currently lose_sum: 560.2965902090073
time_elpased: 5.988
batch start
#iterations: 189
currently lose_sum: 558.6789741814137
time_elpased: 5.606
batch start
#iterations: 190
currently lose_sum: 557.8416186571121
time_elpased: 5.802
batch start
#iterations: 191
currently lose_sum: 559.1976285874844
time_elpased: 5.43
batch start
#iterations: 192
currently lose_sum: 559.0721036195755
time_elpased: 5.74
batch start
#iterations: 193
currently lose_sum: 561.1767952442169
time_elpased: 5.637
batch start
#iterations: 194
currently lose_sum: 559.6262736618519
time_elpased: 5.671
batch start
#iterations: 195
currently lose_sum: 560.0075975954533
time_elpased: 5.566
batch start
#iterations: 196
currently lose_sum: 559.0314245522022
time_elpased: 5.805
batch start
#iterations: 197
currently lose_sum: 562.3615154623985
time_elpased: 5.665
batch start
#iterations: 198
currently lose_sum: 559.9503021538258
time_elpased: 5.573
batch start
#iterations: 199
currently lose_sum: 557.1557973325253
time_elpased: 5.888
start validation test
0.585872576177
0.575287240476
0.881226842322
0.696125858775
0
validation finish
batch start
#iterations: 200
currently lose_sum: 558.6891411244869
time_elpased: 5.786
batch start
#iterations: 201
currently lose_sum: 559.6876742243767
time_elpased: 5.756
batch start
#iterations: 202
currently lose_sum: 559.455263286829
time_elpased: 5.891
batch start
#iterations: 203
currently lose_sum: 558.2053113877773
time_elpased: 5.778
batch start
#iterations: 204
currently lose_sum: 559.1130201220512
time_elpased: 5.718
batch start
#iterations: 205
currently lose_sum: 556.3008306920528
time_elpased: 6.04
batch start
#iterations: 206
currently lose_sum: 558.7630715966225
time_elpased: 5.944
batch start
#iterations: 207
currently lose_sum: 557.3928263783455
time_elpased: 5.762
batch start
#iterations: 208
currently lose_sum: 559.865856051445
time_elpased: 5.626
batch start
#iterations: 209
currently lose_sum: 560.1603269279003
time_elpased: 5.464
batch start
#iterations: 210
currently lose_sum: 559.3275495171547
time_elpased: 5.9
batch start
#iterations: 211
currently lose_sum: 559.3650397360325
time_elpased: 5.888
batch start
#iterations: 212
currently lose_sum: 556.3763336539268
time_elpased: 5.422
batch start
#iterations: 213
currently lose_sum: 561.091135174036
time_elpased: 5.912
batch start
#iterations: 214
currently lose_sum: 559.0457873046398
time_elpased: 5.493
batch start
#iterations: 215
currently lose_sum: 560.7453657984734
time_elpased: 5.848
batch start
#iterations: 216
currently lose_sum: 559.7408351302147
time_elpased: 5.738
batch start
#iterations: 217
currently lose_sum: 559.2672815322876
time_elpased: 5.838
batch start
#iterations: 218
currently lose_sum: 559.7168065309525
time_elpased: 5.757
batch start
#iterations: 219
currently lose_sum: 558.0338317453861
time_elpased: 5.633
start validation test
0.586620498615
0.575142485751
0.888019761219
0.698128856074
0
validation finish
batch start
#iterations: 220
currently lose_sum: 558.1655939221382
time_elpased: 5.748
batch start
#iterations: 221
currently lose_sum: 559.2473534345627
time_elpased: 5.707
batch start
#iterations: 222
currently lose_sum: 558.7218324542046
time_elpased: 5.687
batch start
#iterations: 223
currently lose_sum: 559.2304607927799
time_elpased: 5.599
batch start
#iterations: 224
currently lose_sum: 558.0127547681332
time_elpased: 5.83
batch start
#iterations: 225
currently lose_sum: 557.1111310124397
time_elpased: 5.628
batch start
#iterations: 226
currently lose_sum: 559.0061386525631
time_elpased: 5.539
batch start
#iterations: 227
currently lose_sum: 560.27053591609
time_elpased: 5.815
batch start
#iterations: 228
currently lose_sum: 559.4131080508232
time_elpased: 5.489
batch start
#iterations: 229
currently lose_sum: 558.0563755631447
time_elpased: 5.784
batch start
#iterations: 230
currently lose_sum: 557.4313607811928
time_elpased: 5.686
batch start
#iterations: 231
currently lose_sum: 558.5983416438103
time_elpased: 5.704
batch start
#iterations: 232
currently lose_sum: 560.246775329113
time_elpased: 5.972
batch start
#iterations: 233
currently lose_sum: 557.7271226942539
time_elpased: 5.621
batch start
#iterations: 234
currently lose_sum: 558.6360954940319
time_elpased: 5.811
batch start
#iterations: 235
currently lose_sum: 557.0569732189178
time_elpased: 5.723
batch start
#iterations: 236
currently lose_sum: 556.7128012478352
time_elpased: 5.651
batch start
#iterations: 237
currently lose_sum: 556.7872527241707
time_elpased: 5.86
batch start
#iterations: 238
currently lose_sum: 557.2682321667671
time_elpased: 5.494
batch start
#iterations: 239
currently lose_sum: 560.0605250298977
time_elpased: 5.68
start validation test
0.586952908587
0.576257463819
0.879065459037
0.696158940397
0
validation finish
batch start
#iterations: 240
currently lose_sum: 557.8353089392185
time_elpased: 5.618
batch start
#iterations: 241
currently lose_sum: 557.8774926066399
time_elpased: 5.797
batch start
#iterations: 242
currently lose_sum: 557.354061037302
time_elpased: 5.928
batch start
#iterations: 243
currently lose_sum: 557.0084394812584
time_elpased: 5.783
batch start
#iterations: 244
currently lose_sum: 557.2679225802422
time_elpased: 5.602
batch start
#iterations: 245
currently lose_sum: 557.343659222126
time_elpased: 5.726
batch start
#iterations: 246
currently lose_sum: 557.1315814554691
time_elpased: 5.535
batch start
#iterations: 247
currently lose_sum: 556.322174847126
time_elpased: 5.926
batch start
#iterations: 248
currently lose_sum: 557.9313034713268
time_elpased: 5.384
batch start
#iterations: 249
currently lose_sum: 559.0092212557793
time_elpased: 5.941
batch start
#iterations: 250
currently lose_sum: 556.5851028263569
time_elpased: 5.752
batch start
#iterations: 251
currently lose_sum: 558.7400661110878
time_elpased: 5.751
batch start
#iterations: 252
currently lose_sum: 558.335994631052
time_elpased: 6.01
batch start
#iterations: 253
currently lose_sum: 557.236114948988
time_elpased: 5.87
batch start
#iterations: 254
currently lose_sum: 557.5597234964371
time_elpased: 5.728
batch start
#iterations: 255
currently lose_sum: 557.2272496521473
time_elpased: 6.004
batch start
#iterations: 256
currently lose_sum: 557.7305742502213
time_elpased: 5.717
batch start
#iterations: 257
currently lose_sum: 558.0424478054047
time_elpased: 5.68
batch start
#iterations: 258
currently lose_sum: 556.8895398378372
time_elpased: 5.542
batch start
#iterations: 259
currently lose_sum: 557.1902951598167
time_elpased: 5.891
start validation test
0.586786703601
0.576212821984
0.878344997942
0.69590035268
0
validation finish
batch start
#iterations: 260
currently lose_sum: 557.0408910214901
time_elpased: 5.778
batch start
#iterations: 261
currently lose_sum: 553.561193972826
time_elpased: 5.779
batch start
#iterations: 262
currently lose_sum: 554.2665497660637
time_elpased: 5.856
batch start
#iterations: 263
currently lose_sum: 558.5542715191841
time_elpased: 5.884
batch start
#iterations: 264
currently lose_sum: 554.2007293701172
time_elpased: 5.527
batch start
#iterations: 265
currently lose_sum: 554.7816987037659
time_elpased: 5.734
batch start
#iterations: 266
currently lose_sum: 557.4889958202839
time_elpased: 5.7
batch start
#iterations: 267
currently lose_sum: 556.6328491270542
time_elpased: 5.657
batch start
#iterations: 268
currently lose_sum: 557.1606079638004
time_elpased: 5.625
batch start
#iterations: 269
currently lose_sum: 555.5371301472187
time_elpased: 5.581
batch start
#iterations: 270
currently lose_sum: 556.6490474939346
time_elpased: 5.737
batch start
#iterations: 271
currently lose_sum: 554.8888192176819
time_elpased: 5.671
batch start
#iterations: 272
currently lose_sum: 558.5670135617256
time_elpased: 5.645
batch start
#iterations: 273
currently lose_sum: 556.0830877125263
time_elpased: 5.718
batch start
#iterations: 274
currently lose_sum: 555.2268006205559
time_elpased: 5.753
batch start
#iterations: 275
currently lose_sum: 556.7530846893787
time_elpased: 5.765
batch start
#iterations: 276
currently lose_sum: 558.5797815322876
time_elpased: 6.031
batch start
#iterations: 277
currently lose_sum: 555.7336772680283
time_elpased: 5.975
batch start
#iterations: 278
currently lose_sum: 557.950742483139
time_elpased: 5.649
batch start
#iterations: 279
currently lose_sum: 555.1606568992138
time_elpased: 5.659
start validation test
0.585263157895
0.575767871704
0.872066694113
0.693598559267
0
validation finish
batch start
#iterations: 280
currently lose_sum: 556.7835417687893
time_elpased: 5.489
batch start
#iterations: 281
currently lose_sum: 556.68641102314
time_elpased: 5.91
batch start
#iterations: 282
currently lose_sum: 557.9360445439816
time_elpased: 5.675
batch start
#iterations: 283
currently lose_sum: 556.558216124773
time_elpased: 5.599
batch start
#iterations: 284
currently lose_sum: 557.6190147697926
time_elpased: 5.662
batch start
#iterations: 285
currently lose_sum: 555.9226317703724
time_elpased: 5.49
batch start
#iterations: 286
currently lose_sum: 555.1832900643349
time_elpased: 5.85
batch start
#iterations: 287
currently lose_sum: 557.5902041196823
time_elpased: 5.564
batch start
#iterations: 288
currently lose_sum: 556.8959630727768
time_elpased: 5.92
batch start
#iterations: 289
currently lose_sum: 558.8950369358063
time_elpased: 5.654
batch start
#iterations: 290
currently lose_sum: 556.013894379139
time_elpased: 5.684
batch start
#iterations: 291
currently lose_sum: 557.1389292180538
time_elpased: 5.818
batch start
#iterations: 292
currently lose_sum: 556.8747216165066
time_elpased: 5.755
batch start
#iterations: 293
currently lose_sum: 555.7526874244213
time_elpased: 5.587
batch start
#iterations: 294
currently lose_sum: 557.267886787653
time_elpased: 5.624
batch start
#iterations: 295
currently lose_sum: 558.0688496828079
time_elpased: 5.729
batch start
#iterations: 296
currently lose_sum: 556.154047191143
time_elpased: 5.771
batch start
#iterations: 297
currently lose_sum: 556.5386954545975
time_elpased: 5.638
batch start
#iterations: 298
currently lose_sum: 559.1995466351509
time_elpased: 5.959
batch start
#iterations: 299
currently lose_sum: 555.9983583986759
time_elpased: 5.789
start validation test
0.585041551247
0.575560081466
0.872581309181
0.69361040661
0
validation finish
batch start
#iterations: 300
currently lose_sum: 556.5735581219196
time_elpased: 5.626
batch start
#iterations: 301
currently lose_sum: 555.4720871448517
time_elpased: 5.792
batch start
#iterations: 302
currently lose_sum: 555.4426737427711
time_elpased: 5.524
batch start
#iterations: 303
currently lose_sum: 557.9302111268044
time_elpased: 5.977
batch start
#iterations: 304
currently lose_sum: 555.3835589289665
time_elpased: 5.693
batch start
#iterations: 305
currently lose_sum: 556.7716233730316
time_elpased: 5.544
batch start
#iterations: 306
currently lose_sum: 557.9852832555771
time_elpased: 5.535
batch start
#iterations: 307
currently lose_sum: 558.6722029745579
time_elpased: 5.663
batch start
#iterations: 308
currently lose_sum: 555.8836751282215
time_elpased: 5.868
batch start
#iterations: 309
currently lose_sum: 555.4806671142578
time_elpased: 5.848
batch start
#iterations: 310
currently lose_sum: 557.176915705204
time_elpased: 5.454
batch start
#iterations: 311
currently lose_sum: 557.4875581860542
time_elpased: 5.764
batch start
#iterations: 312
currently lose_sum: 556.0975244641304
time_elpased: 5.772
batch start
#iterations: 313
currently lose_sum: 556.4695702493191
time_elpased: 5.999
batch start
#iterations: 314
currently lose_sum: 556.8978980779648
time_elpased: 5.974
batch start
#iterations: 315
currently lose_sum: 556.603050082922
time_elpased: 5.862
batch start
#iterations: 316
currently lose_sum: 555.7954507172108
time_elpased: 6.0
batch start
#iterations: 317
currently lose_sum: 557.5934367179871
time_elpased: 5.791
batch start
#iterations: 318
currently lose_sum: 557.0692192912102
time_elpased: 5.463
batch start
#iterations: 319
currently lose_sum: 557.297622025013
time_elpased: 5.518
start validation test
0.585650969529
0.576207671868
0.870419925895
0.693395646292
0
validation finish
batch start
#iterations: 320
currently lose_sum: 556.2263049483299
time_elpased: 5.878
batch start
#iterations: 321
currently lose_sum: 555.5751395225525
time_elpased: 5.873
batch start
#iterations: 322
currently lose_sum: 558.2407247722149
time_elpased: 5.546
batch start
#iterations: 323
currently lose_sum: 555.7582614123821
time_elpased: 5.872
batch start
#iterations: 324
currently lose_sum: 558.5824635326862
time_elpased: 5.555
batch start
#iterations: 325
currently lose_sum: 558.0868458747864
time_elpased: 5.856
batch start
#iterations: 326
currently lose_sum: 554.5504744946957
time_elpased: 5.851
batch start
#iterations: 327
currently lose_sum: 554.8652102053165
time_elpased: 5.65
batch start
#iterations: 328
currently lose_sum: 556.4047201573849
time_elpased: 5.812
batch start
#iterations: 329
currently lose_sum: 556.2544390261173
time_elpased: 5.781
batch start
#iterations: 330
currently lose_sum: 553.7343860566616
time_elpased: 5.579
batch start
#iterations: 331
currently lose_sum: 553.6611095368862
time_elpased: 5.678
batch start
#iterations: 332
currently lose_sum: 554.4017874598503
time_elpased: 5.757
batch start
#iterations: 333
currently lose_sum: 556.769484937191
time_elpased: 6.046
batch start
#iterations: 334
currently lose_sum: 556.4004371464252
time_elpased: 5.785
batch start
#iterations: 335
currently lose_sum: 554.802876740694
time_elpased: 5.618
batch start
#iterations: 336
currently lose_sum: 555.484777033329
time_elpased: 5.61
batch start
#iterations: 337
currently lose_sum: 554.5713142454624
time_elpased: 5.759
batch start
#iterations: 338
currently lose_sum: 556.7273396253586
time_elpased: 5.906
batch start
#iterations: 339
currently lose_sum: 555.3700839281082
time_elpased: 5.631
start validation test
0.584653739612
0.575491596925
0.870522848909
0.692909515422
0
validation finish
batch start
#iterations: 340
currently lose_sum: 555.9182277321815
time_elpased: 5.783
batch start
#iterations: 341
currently lose_sum: 555.4652792811394
time_elpased: 5.896
batch start
#iterations: 342
currently lose_sum: 554.8806042969227
time_elpased: 5.704
batch start
#iterations: 343
currently lose_sum: 556.8127285242081
time_elpased: 5.826
batch start
#iterations: 344
currently lose_sum: 552.4851560890675
time_elpased: 5.914
batch start
#iterations: 345
currently lose_sum: 556.9431507885456
time_elpased: 5.913
batch start
#iterations: 346
currently lose_sum: 555.2840174138546
time_elpased: 5.907
batch start
#iterations: 347
currently lose_sum: 557.4766496121883
time_elpased: 5.855
batch start
#iterations: 348
currently lose_sum: 553.544546931982
time_elpased: 5.754
batch start
#iterations: 349
currently lose_sum: 553.5692565739155
time_elpased: 5.751
batch start
#iterations: 350
currently lose_sum: 556.3139924705029
time_elpased: 5.777
batch start
#iterations: 351
currently lose_sum: 555.4461553990841
time_elpased: 5.83
batch start
#iterations: 352
currently lose_sum: 556.5889576673508
time_elpased: 5.817
batch start
#iterations: 353
currently lose_sum: 555.2744579315186
time_elpased: 5.391
batch start
#iterations: 354
currently lose_sum: 555.010849148035
time_elpased: 6.041
batch start
#iterations: 355
currently lose_sum: 554.7582750618458
time_elpased: 6.013
batch start
#iterations: 356
currently lose_sum: 557.1512742340565
time_elpased: 5.83
batch start
#iterations: 357
currently lose_sum: 554.6111492216587
time_elpased: 5.613
batch start
#iterations: 358
currently lose_sum: 555.3198117911816
time_elpased: 5.927
batch start
#iterations: 359
currently lose_sum: 556.7752053439617
time_elpased: 5.736
start validation test
0.584072022161
0.575347139299
0.867846850556
0.691955768008
0
validation finish
batch start
#iterations: 360
currently lose_sum: 557.9211758971214
time_elpased: 5.589
batch start
#iterations: 361
currently lose_sum: 557.8111071586609
time_elpased: 6.01
batch start
#iterations: 362
currently lose_sum: 556.7938102781773
time_elpased: 5.766
batch start
#iterations: 363
currently lose_sum: 555.7613213658333
time_elpased: 5.72
batch start
#iterations: 364
currently lose_sum: 555.2370244562626
time_elpased: 5.834
batch start
#iterations: 365
currently lose_sum: 553.5919576883316
time_elpased: 5.629
batch start
#iterations: 366
currently lose_sum: 555.0007363259792
time_elpased: 5.631
batch start
#iterations: 367
currently lose_sum: 554.6523904502392
time_elpased: 5.756
batch start
#iterations: 368
currently lose_sum: 556.5349158346653
time_elpased: 5.603
batch start
#iterations: 369
currently lose_sum: 555.8308434486389
time_elpased: 5.633
batch start
#iterations: 370
currently lose_sum: 555.4581468105316
time_elpased: 5.856
batch start
#iterations: 371
currently lose_sum: 556.633556753397
time_elpased: 5.926
batch start
#iterations: 372
currently lose_sum: 554.9250723719597
time_elpased: 5.871
batch start
#iterations: 373
currently lose_sum: 557.6048882007599
time_elpased: 5.593
batch start
#iterations: 374
currently lose_sum: 552.8334437906742
time_elpased: 5.931
batch start
#iterations: 375
currently lose_sum: 556.0340147316456
time_elpased: 5.916
batch start
#iterations: 376
currently lose_sum: 556.1502775847912
time_elpased: 5.622
batch start
#iterations: 377
currently lose_sum: 556.0169096589088
time_elpased: 5.699
batch start
#iterations: 378
currently lose_sum: 554.3223601579666
time_elpased: 5.537
batch start
#iterations: 379
currently lose_sum: 556.8756483197212
time_elpased: 5.931
start validation test
0.585207756233
0.576278146602
0.866611774393
0.692234965265
0
validation finish
batch start
#iterations: 380
currently lose_sum: 550.96431183815
time_elpased: 5.4
batch start
#iterations: 381
currently lose_sum: 555.9055585563183
time_elpased: 5.952
batch start
#iterations: 382
currently lose_sum: 554.4077833592892
time_elpased: 5.756
batch start
#iterations: 383
currently lose_sum: 555.876558214426
time_elpased: 5.587
batch start
#iterations: 384
currently lose_sum: 554.8687753975391
time_elpased: 5.806
batch start
#iterations: 385
currently lose_sum: 552.8459734022617
time_elpased: 5.821
batch start
#iterations: 386
currently lose_sum: 556.2971353530884
time_elpased: 5.693
batch start
#iterations: 387
currently lose_sum: 554.5176711976528
time_elpased: 5.565
batch start
#iterations: 388
currently lose_sum: 554.4782798886299
time_elpased: 5.614
batch start
#iterations: 389
currently lose_sum: 554.985261708498
time_elpased: 5.919
batch start
#iterations: 390
currently lose_sum: 555.5694017112255
time_elpased: 5.629
batch start
#iterations: 391
currently lose_sum: 555.3425480723381
time_elpased: 5.799
batch start
#iterations: 392
currently lose_sum: 553.2653506994247
time_elpased: 5.777
batch start
#iterations: 393
currently lose_sum: 553.2112982571125
time_elpased: 5.661
batch start
#iterations: 394
currently lose_sum: 555.1825255453587
time_elpased: 5.899
batch start
#iterations: 395
currently lose_sum: 554.2309381961823
time_elpased: 5.587
batch start
#iterations: 396
currently lose_sum: 554.8739570081234
time_elpased: 5.856
batch start
#iterations: 397
currently lose_sum: 554.9712015688419
time_elpased: 5.659
batch start
#iterations: 398
currently lose_sum: 554.7609701752663
time_elpased: 5.835
batch start
#iterations: 399
currently lose_sum: 557.0109329223633
time_elpased: 5.922
start validation test
0.58567867036
0.575865459601
0.874022231371
0.694287174246
0
validation finish
acc: 0.589
pre: 0.576
rec: 0.890
F1: 0.699
auc: 0.000
