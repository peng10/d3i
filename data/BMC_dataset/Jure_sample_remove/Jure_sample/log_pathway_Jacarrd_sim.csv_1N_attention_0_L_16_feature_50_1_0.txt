start to construct graph
graph construct over
93159
epochs start
batch start
#iterations: 0
currently lose_sum: 616.102342069149
time_elpased: 1.881
batch start
#iterations: 1
currently lose_sum: 609.628128349781
time_elpased: 1.835
batch start
#iterations: 2
currently lose_sum: 607.7345052957535
time_elpased: 1.905
batch start
#iterations: 3
currently lose_sum: 605.4637987613678
time_elpased: 1.876
batch start
#iterations: 4
currently lose_sum: 604.1491572260857
time_elpased: 1.85
batch start
#iterations: 5
currently lose_sum: 603.3313084244728
time_elpased: 1.845
batch start
#iterations: 6
currently lose_sum: 602.5491368770599
time_elpased: 1.881
batch start
#iterations: 7
currently lose_sum: 602.4292913079262
time_elpased: 1.964
batch start
#iterations: 8
currently lose_sum: 603.4409614801407
time_elpased: 1.886
batch start
#iterations: 9
currently lose_sum: 599.4455390572548
time_elpased: 1.85
batch start
#iterations: 10
currently lose_sum: 599.2249664068222
time_elpased: 1.838
batch start
#iterations: 11
currently lose_sum: 601.232225894928
time_elpased: 1.867
batch start
#iterations: 12
currently lose_sum: 598.6859274506569
time_elpased: 1.862
batch start
#iterations: 13
currently lose_sum: 599.4037182331085
time_elpased: 1.819
batch start
#iterations: 14
currently lose_sum: 598.9833267331123
time_elpased: 1.822
batch start
#iterations: 15
currently lose_sum: 597.9497393965721
time_elpased: 1.841
batch start
#iterations: 16
currently lose_sum: 600.789800465107
time_elpased: 1.867
batch start
#iterations: 17
currently lose_sum: 598.2435572147369
time_elpased: 1.83
batch start
#iterations: 18
currently lose_sum: 598.0560188293457
time_elpased: 1.896
batch start
#iterations: 19
currently lose_sum: 598.4846760630608
time_elpased: 1.874
start validation test
0.56317679558
0.554338365497
0.950396212823
0.700244535856
0
validation finish
batch start
#iterations: 20
currently lose_sum: 597.1426314115524
time_elpased: 1.858
batch start
#iterations: 21
currently lose_sum: 598.2345675826073
time_elpased: 1.831
batch start
#iterations: 22
currently lose_sum: 598.1836828589439
time_elpased: 1.847
batch start
#iterations: 23
currently lose_sum: 596.9276075959206
time_elpased: 1.854
batch start
#iterations: 24
currently lose_sum: 596.235545694828
time_elpased: 1.848
batch start
#iterations: 25
currently lose_sum: 596.478925704956
time_elpased: 1.838
batch start
#iterations: 26
currently lose_sum: 597.7599148154259
time_elpased: 1.835
batch start
#iterations: 27
currently lose_sum: 596.9622393250465
time_elpased: 1.86
batch start
#iterations: 28
currently lose_sum: 595.8671335577965
time_elpased: 1.845
batch start
#iterations: 29
currently lose_sum: 595.2439846992493
time_elpased: 1.819
batch start
#iterations: 30
currently lose_sum: 595.644139945507
time_elpased: 1.837
batch start
#iterations: 31
currently lose_sum: 596.9555289149284
time_elpased: 1.824
batch start
#iterations: 32
currently lose_sum: 596.3471044898033
time_elpased: 1.829
batch start
#iterations: 33
currently lose_sum: 596.0053189396858
time_elpased: 1.836
batch start
#iterations: 34
currently lose_sum: 596.6173621416092
time_elpased: 1.844
batch start
#iterations: 35
currently lose_sum: 596.9362630844116
time_elpased: 1.84
batch start
#iterations: 36
currently lose_sum: 595.4020217061043
time_elpased: 1.827
batch start
#iterations: 37
currently lose_sum: 595.7433182001114
time_elpased: 1.839
batch start
#iterations: 38
currently lose_sum: 597.212042093277
time_elpased: 1.85
batch start
#iterations: 39
currently lose_sum: 595.5953575372696
time_elpased: 1.862
start validation test
0.571906077348
0.561463764948
0.92528558197
0.698859330367
0
validation finish
batch start
#iterations: 40
currently lose_sum: 596.7611757516861
time_elpased: 1.843
batch start
#iterations: 41
currently lose_sum: 596.2659922838211
time_elpased: 1.836
batch start
#iterations: 42
currently lose_sum: 595.93458122015
time_elpased: 1.827
batch start
#iterations: 43
currently lose_sum: 596.9651161432266
time_elpased: 1.803
batch start
#iterations: 44
currently lose_sum: 594.6968304514885
time_elpased: 1.835
batch start
#iterations: 45
currently lose_sum: 596.6118644475937
time_elpased: 1.793
batch start
#iterations: 46
currently lose_sum: 596.8157700896263
time_elpased: 1.834
batch start
#iterations: 47
currently lose_sum: 598.17089676857
time_elpased: 1.838
batch start
#iterations: 48
currently lose_sum: 596.6882982254028
time_elpased: 1.831
batch start
#iterations: 49
currently lose_sum: 595.3974667191505
time_elpased: 1.798
batch start
#iterations: 50
currently lose_sum: 596.0538209676743
time_elpased: 1.827
batch start
#iterations: 51
currently lose_sum: 595.2689186930656
time_elpased: 1.825
batch start
#iterations: 52
currently lose_sum: 594.6927565336227
time_elpased: 1.84
batch start
#iterations: 53
currently lose_sum: 596.5818157196045
time_elpased: 1.85
batch start
#iterations: 54
currently lose_sum: 595.3008877038956
time_elpased: 1.804
batch start
#iterations: 55
currently lose_sum: 594.0307278037071
time_elpased: 1.8
batch start
#iterations: 56
currently lose_sum: 597.0077103972435
time_elpased: 1.817
batch start
#iterations: 57
currently lose_sum: 594.9494874477386
time_elpased: 1.841
batch start
#iterations: 58
currently lose_sum: 596.2727090716362
time_elpased: 1.831
batch start
#iterations: 59
currently lose_sum: 595.1487755179405
time_elpased: 1.822
start validation test
0.573093922652
0.562815656566
0.917464237934
0.697656219431
0
validation finish
batch start
#iterations: 60
currently lose_sum: 593.7455056905746
time_elpased: 1.832
batch start
#iterations: 61
currently lose_sum: 597.3356781601906
time_elpased: 1.842
batch start
#iterations: 62
currently lose_sum: 594.2647741436958
time_elpased: 1.832
batch start
#iterations: 63
currently lose_sum: 595.3205271363258
time_elpased: 1.847
batch start
#iterations: 64
currently lose_sum: 595.7750973105431
time_elpased: 1.85
batch start
#iterations: 65
currently lose_sum: 595.9070490002632
time_elpased: 1.827
batch start
#iterations: 66
currently lose_sum: 594.8638426065445
time_elpased: 1.824
batch start
#iterations: 67
currently lose_sum: 595.2179674506187
time_elpased: 1.832
batch start
#iterations: 68
currently lose_sum: 595.7588291168213
time_elpased: 1.83
batch start
#iterations: 69
currently lose_sum: 595.545426428318
time_elpased: 1.828
batch start
#iterations: 70
currently lose_sum: 597.7630221247673
time_elpased: 1.833
batch start
#iterations: 71
currently lose_sum: 597.1397359967232
time_elpased: 1.81
batch start
#iterations: 72
currently lose_sum: 595.7666190266609
time_elpased: 1.832
batch start
#iterations: 73
currently lose_sum: 595.5825928449631
time_elpased: 1.824
batch start
#iterations: 74
currently lose_sum: 595.423032104969
time_elpased: 1.83
batch start
#iterations: 75
currently lose_sum: 596.9011402130127
time_elpased: 1.851
batch start
#iterations: 76
currently lose_sum: 597.3470528721809
time_elpased: 1.842
batch start
#iterations: 77
currently lose_sum: 595.105171918869
time_elpased: 1.817
batch start
#iterations: 78
currently lose_sum: 595.2537996768951
time_elpased: 1.831
batch start
#iterations: 79
currently lose_sum: 595.4255816340446
time_elpased: 1.818
start validation test
0.575994475138
0.565416519873
0.908407944839
0.697001401583
0
validation finish
batch start
#iterations: 80
currently lose_sum: 592.9916787147522
time_elpased: 1.819
batch start
#iterations: 81
currently lose_sum: 595.7876799106598
time_elpased: 1.832
batch start
#iterations: 82
currently lose_sum: 594.4671038389206
time_elpased: 1.837
batch start
#iterations: 83
currently lose_sum: 596.5276227593422
time_elpased: 1.83
batch start
#iterations: 84
currently lose_sum: 594.2123349308968
time_elpased: 1.848
batch start
#iterations: 85
currently lose_sum: 594.6954510807991
time_elpased: 1.818
batch start
#iterations: 86
currently lose_sum: 595.6325189471245
time_elpased: 1.822
batch start
#iterations: 87
currently lose_sum: 595.5449672937393
time_elpased: 1.818
batch start
#iterations: 88
currently lose_sum: 595.0335507392883
time_elpased: 1.858
batch start
#iterations: 89
currently lose_sum: 593.8079009652138
time_elpased: 1.823
batch start
#iterations: 90
currently lose_sum: 592.7447065114975
time_elpased: 1.836
batch start
#iterations: 91
currently lose_sum: 595.7686049938202
time_elpased: 1.821
batch start
#iterations: 92
currently lose_sum: 595.2317296266556
time_elpased: 1.813
batch start
#iterations: 93
currently lose_sum: 595.5892235040665
time_elpased: 1.828
batch start
#iterations: 94
currently lose_sum: 593.7403125166893
time_elpased: 1.832
batch start
#iterations: 95
currently lose_sum: 596.2761104106903
time_elpased: 1.827
batch start
#iterations: 96
currently lose_sum: 595.5443522930145
time_elpased: 1.798
batch start
#iterations: 97
currently lose_sum: 595.3064687252045
time_elpased: 1.817
batch start
#iterations: 98
currently lose_sum: 594.0194170475006
time_elpased: 1.825
batch start
#iterations: 99
currently lose_sum: 594.707513153553
time_elpased: 1.809
start validation test
0.570220994475
0.560029736092
0.930328290625
0.699176302255
0
validation finish
batch start
#iterations: 100
currently lose_sum: 594.0791693925858
time_elpased: 1.863
batch start
#iterations: 101
currently lose_sum: 595.7381703257561
time_elpased: 1.89
batch start
#iterations: 102
currently lose_sum: 595.2933707237244
time_elpased: 1.868
batch start
#iterations: 103
currently lose_sum: 594.5074571371078
time_elpased: 1.884
batch start
#iterations: 104
currently lose_sum: 596.1245940327644
time_elpased: 1.903
batch start
#iterations: 105
currently lose_sum: 595.5178220272064
time_elpased: 1.851
batch start
#iterations: 106
currently lose_sum: 594.7791449427605
time_elpased: 1.87
batch start
#iterations: 107
currently lose_sum: 595.243094265461
time_elpased: 1.863
batch start
#iterations: 108
currently lose_sum: 597.3037350177765
time_elpased: 1.852
batch start
#iterations: 109
currently lose_sum: 594.2134625911713
time_elpased: 1.842
batch start
#iterations: 110
currently lose_sum: 594.9851959347725
time_elpased: 1.866
batch start
#iterations: 111
currently lose_sum: 594.0603268146515
time_elpased: 1.881
batch start
#iterations: 112
currently lose_sum: 594.0680901408195
time_elpased: 1.906
batch start
#iterations: 113
currently lose_sum: 593.6023762822151
time_elpased: 1.871
batch start
#iterations: 114
currently lose_sum: 593.1831622123718
time_elpased: 1.89
batch start
#iterations: 115
currently lose_sum: 595.1240764260292
time_elpased: 1.881
batch start
#iterations: 116
currently lose_sum: 594.7178200483322
time_elpased: 1.897
batch start
#iterations: 117
currently lose_sum: 596.7302661538124
time_elpased: 1.826
batch start
#iterations: 118
currently lose_sum: 596.5411584377289
time_elpased: 1.851
batch start
#iterations: 119
currently lose_sum: 594.9381349086761
time_elpased: 1.873
start validation test
0.572375690608
0.561158200829
0.933415663271
0.700927357032
0
validation finish
batch start
#iterations: 120
currently lose_sum: 595.6101794838905
time_elpased: 1.924
batch start
#iterations: 121
currently lose_sum: 595.8516293168068
time_elpased: 1.878
batch start
#iterations: 122
currently lose_sum: 595.6189671158791
time_elpased: 1.834
batch start
#iterations: 123
currently lose_sum: 593.7836037874222
time_elpased: 1.85
batch start
#iterations: 124
currently lose_sum: 595.1775349974632
time_elpased: 1.864
batch start
#iterations: 125
currently lose_sum: 595.366123855114
time_elpased: 1.888
batch start
#iterations: 126
currently lose_sum: 595.4275966882706
time_elpased: 1.874
batch start
#iterations: 127
currently lose_sum: 595.1551497578621
time_elpased: 1.907
batch start
#iterations: 128
currently lose_sum: 594.0241898298264
time_elpased: 1.861
batch start
#iterations: 129
currently lose_sum: 595.6891055703163
time_elpased: 1.878
batch start
#iterations: 130
currently lose_sum: 595.9103717803955
time_elpased: 1.857
batch start
#iterations: 131
currently lose_sum: 593.5469255447388
time_elpased: 1.828
batch start
#iterations: 132
currently lose_sum: 595.8543546795845
time_elpased: 1.834
batch start
#iterations: 133
currently lose_sum: 594.7415044903755
time_elpased: 1.827
batch start
#iterations: 134
currently lose_sum: 595.3326752781868
time_elpased: 1.836
batch start
#iterations: 135
currently lose_sum: 594.3806204795837
time_elpased: 1.83
batch start
#iterations: 136
currently lose_sum: 594.4877976775169
time_elpased: 1.919
batch start
#iterations: 137
currently lose_sum: 592.5598668456078
time_elpased: 1.856
batch start
#iterations: 138
currently lose_sum: 594.9680392742157
time_elpased: 1.852
batch start
#iterations: 139
currently lose_sum: 596.6447116732597
time_elpased: 1.844
start validation test
0.568839779006
0.558206048804
0.944015642688
0.701567877629
0
validation finish
batch start
#iterations: 140
currently lose_sum: 595.1907404065132
time_elpased: 1.871
batch start
#iterations: 141
currently lose_sum: 594.4712491631508
time_elpased: 1.812
batch start
#iterations: 142
currently lose_sum: 593.8458101153374
time_elpased: 1.862
batch start
#iterations: 143
currently lose_sum: 593.6395958662033
time_elpased: 1.848
batch start
#iterations: 144
currently lose_sum: 595.4925907850266
time_elpased: 1.824
batch start
#iterations: 145
currently lose_sum: 595.0757061839104
time_elpased: 1.857
batch start
#iterations: 146
currently lose_sum: 594.5510769486427
time_elpased: 1.845
batch start
#iterations: 147
currently lose_sum: 593.6035367250443
time_elpased: 1.872
batch start
#iterations: 148
currently lose_sum: 593.8453893065453
time_elpased: 1.868
batch start
#iterations: 149
currently lose_sum: 594.461030125618
time_elpased: 1.828
batch start
#iterations: 150
currently lose_sum: 594.9674696922302
time_elpased: 1.837
batch start
#iterations: 151
currently lose_sum: 593.1600162386894
time_elpased: 1.857
batch start
#iterations: 152
currently lose_sum: 594.4503415226936
time_elpased: 1.834
batch start
#iterations: 153
currently lose_sum: 592.8605983257294
time_elpased: 1.859
batch start
#iterations: 154
currently lose_sum: 595.0516785383224
time_elpased: 1.84
batch start
#iterations: 155
currently lose_sum: 594.7603492736816
time_elpased: 1.827
batch start
#iterations: 156
currently lose_sum: 596.8779056668282
time_elpased: 1.843
batch start
#iterations: 157
currently lose_sum: 594.6752954125404
time_elpased: 1.838
batch start
#iterations: 158
currently lose_sum: 592.998208284378
time_elpased: 1.856
batch start
#iterations: 159
currently lose_sum: 595.1103592514992
time_elpased: 1.823
start validation test
0.571878453039
0.561538461538
0.924050632911
0.698564593301
0
validation finish
batch start
#iterations: 160
currently lose_sum: 594.0119611620903
time_elpased: 1.845
batch start
#iterations: 161
currently lose_sum: 593.0386318564415
time_elpased: 1.815
batch start
#iterations: 162
currently lose_sum: 594.9647849798203
time_elpased: 1.834
batch start
#iterations: 163
currently lose_sum: 594.3783993124962
time_elpased: 1.835
batch start
#iterations: 164
currently lose_sum: 595.7522473335266
time_elpased: 1.805
batch start
#iterations: 165
currently lose_sum: 596.6632694602013
time_elpased: 1.815
batch start
#iterations: 166
currently lose_sum: 593.6369351148605
time_elpased: 1.818
batch start
#iterations: 167
currently lose_sum: 596.8902414441109
time_elpased: 1.824
batch start
#iterations: 168
currently lose_sum: 595.1966469287872
time_elpased: 1.857
batch start
#iterations: 169
currently lose_sum: 593.935567855835
time_elpased: 1.822
batch start
#iterations: 170
currently lose_sum: 593.2407973408699
time_elpased: 1.838
batch start
#iterations: 171
currently lose_sum: 592.7632555365562
time_elpased: 1.837
batch start
#iterations: 172
currently lose_sum: 593.6881406903267
time_elpased: 1.805
batch start
#iterations: 173
currently lose_sum: 594.0260370373726
time_elpased: 1.833
batch start
#iterations: 174
currently lose_sum: 594.0708072781563
time_elpased: 1.85
batch start
#iterations: 175
currently lose_sum: 594.9396961927414
time_elpased: 1.831
batch start
#iterations: 176
currently lose_sum: 595.569120824337
time_elpased: 1.818
batch start
#iterations: 177
currently lose_sum: 592.9569337964058
time_elpased: 1.817
batch start
#iterations: 178
currently lose_sum: 595.4907697439194
time_elpased: 1.857
batch start
#iterations: 179
currently lose_sum: 594.9450696706772
time_elpased: 1.818
start validation test
0.572513812155
0.561603336134
0.928578779459
0.69990497799
0
validation finish
batch start
#iterations: 180
currently lose_sum: 593.1004102230072
time_elpased: 1.822
batch start
#iterations: 181
currently lose_sum: 593.3984804153442
time_elpased: 1.806
batch start
#iterations: 182
currently lose_sum: 594.1110061407089
time_elpased: 1.856
batch start
#iterations: 183
currently lose_sum: 595.2041106820107
time_elpased: 1.845
batch start
#iterations: 184
currently lose_sum: 593.9666174054146
time_elpased: 1.815
batch start
#iterations: 185
currently lose_sum: 594.9600702524185
time_elpased: 1.849
batch start
#iterations: 186
currently lose_sum: 594.3575086593628
time_elpased: 1.837
batch start
#iterations: 187
currently lose_sum: 595.2960157990456
time_elpased: 1.839
batch start
#iterations: 188
currently lose_sum: 594.3805232644081
time_elpased: 1.843
batch start
#iterations: 189
currently lose_sum: 595.7687776684761
time_elpased: 1.817
batch start
#iterations: 190
currently lose_sum: 592.7870006561279
time_elpased: 1.818
batch start
#iterations: 191
currently lose_sum: 593.8159649968147
time_elpased: 1.841
batch start
#iterations: 192
currently lose_sum: 595.8397298455238
time_elpased: 1.814
batch start
#iterations: 193
currently lose_sum: 596.7608645558357
time_elpased: 1.827
batch start
#iterations: 194
currently lose_sum: 593.9629390239716
time_elpased: 1.851
batch start
#iterations: 195
currently lose_sum: 596.0892520546913
time_elpased: 1.84
batch start
#iterations: 196
currently lose_sum: 594.2537623643875
time_elpased: 1.852
batch start
#iterations: 197
currently lose_sum: 594.1866370439529
time_elpased: 1.84
batch start
#iterations: 198
currently lose_sum: 593.8708244562149
time_elpased: 1.819
batch start
#iterations: 199
currently lose_sum: 594.8216565847397
time_elpased: 1.854
start validation test
0.569502762431
0.558699762152
0.94278069363
0.701615991422
0
validation finish
batch start
#iterations: 200
currently lose_sum: 592.7170350551605
time_elpased: 1.846
batch start
#iterations: 201
currently lose_sum: 594.8196116685867
time_elpased: 1.837
batch start
#iterations: 202
currently lose_sum: 593.8882960677147
time_elpased: 1.834
batch start
#iterations: 203
currently lose_sum: 594.7582043409348
time_elpased: 1.835
batch start
#iterations: 204
currently lose_sum: 592.900525867939
time_elpased: 1.853
batch start
#iterations: 205
currently lose_sum: 591.8368042707443
time_elpased: 1.836
batch start
#iterations: 206
currently lose_sum: 596.4802398085594
time_elpased: 1.844
batch start
#iterations: 207
currently lose_sum: 595.1823979616165
time_elpased: 1.834
batch start
#iterations: 208
currently lose_sum: 593.199974656105
time_elpased: 1.819
batch start
#iterations: 209
currently lose_sum: 594.1481688022614
time_elpased: 1.797
batch start
#iterations: 210
currently lose_sum: 593.2857931256294
time_elpased: 1.823
batch start
#iterations: 211
currently lose_sum: 593.6159453988075
time_elpased: 1.841
batch start
#iterations: 212
currently lose_sum: 592.9567912220955
time_elpased: 1.834
batch start
#iterations: 213
currently lose_sum: 594.7182840108871
time_elpased: 1.814
batch start
#iterations: 214
currently lose_sum: 593.9315342903137
time_elpased: 1.825
batch start
#iterations: 215
currently lose_sum: 594.2940300107002
time_elpased: 1.836
batch start
#iterations: 216
currently lose_sum: 593.4208226799965
time_elpased: 1.836
batch start
#iterations: 217
currently lose_sum: 593.0620030164719
time_elpased: 1.839
batch start
#iterations: 218
currently lose_sum: 596.1602630615234
time_elpased: 1.879
batch start
#iterations: 219
currently lose_sum: 593.200457751751
time_elpased: 1.841
start validation test
0.572624309392
0.5618735363
0.925903056499
0.699352882878
0
validation finish
batch start
#iterations: 220
currently lose_sum: 593.8978702425957
time_elpased: 1.857
batch start
#iterations: 221
currently lose_sum: 593.1403504014015
time_elpased: 1.838
batch start
#iterations: 222
currently lose_sum: 591.2819728851318
time_elpased: 1.826
batch start
#iterations: 223
currently lose_sum: 592.9727745056152
time_elpased: 1.844
batch start
#iterations: 224
currently lose_sum: 593.8345865607262
time_elpased: 1.826
batch start
#iterations: 225
currently lose_sum: 594.2818117141724
time_elpased: 1.822
batch start
#iterations: 226
currently lose_sum: 594.2185422182083
time_elpased: 1.808
batch start
#iterations: 227
currently lose_sum: 594.3343076705933
time_elpased: 1.847
batch start
#iterations: 228
currently lose_sum: 594.0854184627533
time_elpased: 1.826
batch start
#iterations: 229
currently lose_sum: 593.2740412950516
time_elpased: 1.825
batch start
#iterations: 230
currently lose_sum: 595.1675472855568
time_elpased: 1.827
batch start
#iterations: 231
currently lose_sum: 593.1115668416023
time_elpased: 1.808
batch start
#iterations: 232
currently lose_sum: 594.3291448950768
time_elpased: 1.806
batch start
#iterations: 233
currently lose_sum: 594.3636032342911
time_elpased: 1.822
batch start
#iterations: 234
currently lose_sum: 593.6188440322876
time_elpased: 1.818
batch start
#iterations: 235
currently lose_sum: 593.6947174072266
time_elpased: 1.8
batch start
#iterations: 236
currently lose_sum: 595.3060081601143
time_elpased: 1.834
batch start
#iterations: 237
currently lose_sum: 594.2448766231537
time_elpased: 1.848
batch start
#iterations: 238
currently lose_sum: 596.2017933130264
time_elpased: 1.844
batch start
#iterations: 239
currently lose_sum: 592.9104145169258
time_elpased: 1.85
start validation test
0.568756906077
0.558169258391
0.943809817845
0.701481977244
0
validation finish
batch start
#iterations: 240
currently lose_sum: 596.1393283009529
time_elpased: 1.831
batch start
#iterations: 241
currently lose_sum: 593.7861588001251
time_elpased: 1.82
batch start
#iterations: 242
currently lose_sum: 593.424876511097
time_elpased: 1.838
batch start
#iterations: 243
currently lose_sum: 595.4161951541901
time_elpased: 1.831
batch start
#iterations: 244
currently lose_sum: 593.8258871436119
time_elpased: 1.853
batch start
#iterations: 245
currently lose_sum: 593.3903993964195
time_elpased: 1.836
batch start
#iterations: 246
currently lose_sum: 595.8094789981842
time_elpased: 1.826
batch start
#iterations: 247
currently lose_sum: 593.3352409005165
time_elpased: 1.827
batch start
#iterations: 248
currently lose_sum: 594.7906083464622
time_elpased: 1.816
batch start
#iterations: 249
currently lose_sum: 594.8392970561981
time_elpased: 1.854
batch start
#iterations: 250
currently lose_sum: 595.3341367840767
time_elpased: 1.842
batch start
#iterations: 251
currently lose_sum: 593.5346332192421
time_elpased: 1.83
batch start
#iterations: 252
currently lose_sum: 592.4322986006737
time_elpased: 1.822
batch start
#iterations: 253
currently lose_sum: 592.6673104166985
time_elpased: 1.829
batch start
#iterations: 254
currently lose_sum: 594.4234933257103
time_elpased: 1.829
batch start
#iterations: 255
currently lose_sum: 594.4387568831444
time_elpased: 1.843
batch start
#iterations: 256
currently lose_sum: 592.9781228303909
time_elpased: 1.835
batch start
#iterations: 257
currently lose_sum: 595.5246109962463
time_elpased: 1.834
batch start
#iterations: 258
currently lose_sum: 593.8398833870888
time_elpased: 1.81
batch start
#iterations: 259
currently lose_sum: 594.1162632703781
time_elpased: 1.81
start validation test
0.569917127072
0.559092438003
0.940825357621
0.701382895065
0
validation finish
batch start
#iterations: 260
currently lose_sum: 594.6924747228622
time_elpased: 1.824
batch start
#iterations: 261
currently lose_sum: 596.0666401386261
time_elpased: 1.798
batch start
#iterations: 262
currently lose_sum: 595.228680729866
time_elpased: 1.81
batch start
#iterations: 263
currently lose_sum: 596.3086892962456
time_elpased: 1.814
batch start
#iterations: 264
currently lose_sum: 594.3255187273026
time_elpased: 1.818
batch start
#iterations: 265
currently lose_sum: 595.0662535429001
time_elpased: 1.842
batch start
#iterations: 266
currently lose_sum: 591.9845843911171
time_elpased: 1.811
batch start
#iterations: 267
currently lose_sum: 593.1498154997826
time_elpased: 1.826
batch start
#iterations: 268
currently lose_sum: 593.115740954876
time_elpased: 1.811
batch start
#iterations: 269
currently lose_sum: 595.1784027814865
time_elpased: 1.841
batch start
#iterations: 270
currently lose_sum: 593.2905213236809
time_elpased: 1.845
batch start
#iterations: 271
currently lose_sum: 594.0016958117485
time_elpased: 1.844
batch start
#iterations: 272
currently lose_sum: 596.4416586756706
time_elpased: 1.824
batch start
#iterations: 273
currently lose_sum: 594.6337174773216
time_elpased: 1.832
batch start
#iterations: 274
currently lose_sum: 594.569185256958
time_elpased: 1.843
batch start
#iterations: 275
currently lose_sum: 596.1531029343605
time_elpased: 1.806
batch start
#iterations: 276
currently lose_sum: 595.1611902713776
time_elpased: 1.811
batch start
#iterations: 277
currently lose_sum: 594.1444036364555
time_elpased: 1.819
batch start
#iterations: 278
currently lose_sum: 594.6543543934822
time_elpased: 1.803
batch start
#iterations: 279
currently lose_sum: 594.3630467653275
time_elpased: 1.812
start validation test
0.571464088398
0.560541024612
0.9340331378
0.700619487813
0
validation finish
batch start
#iterations: 280
currently lose_sum: 594.2815501093864
time_elpased: 1.821
batch start
#iterations: 281
currently lose_sum: 594.532040655613
time_elpased: 1.834
batch start
#iterations: 282
currently lose_sum: 594.6459114551544
time_elpased: 1.806
batch start
#iterations: 283
currently lose_sum: 596.9119963049889
time_elpased: 1.816
batch start
#iterations: 284
currently lose_sum: 594.5529311299324
time_elpased: 1.822
batch start
#iterations: 285
currently lose_sum: 592.4845988750458
time_elpased: 1.829
batch start
#iterations: 286
currently lose_sum: 593.9181587100029
time_elpased: 1.819
batch start
#iterations: 287
currently lose_sum: 596.4924378991127
time_elpased: 1.827
batch start
#iterations: 288
currently lose_sum: 594.0074135661125
time_elpased: 1.816
batch start
#iterations: 289
currently lose_sum: 592.2124302983284
time_elpased: 1.862
batch start
#iterations: 290
currently lose_sum: 593.3949320912361
time_elpased: 1.842
batch start
#iterations: 291
currently lose_sum: 593.6867673397064
time_elpased: 1.834
batch start
#iterations: 292
currently lose_sum: 594.1085036993027
time_elpased: 1.841
batch start
#iterations: 293
currently lose_sum: 593.9159981608391
time_elpased: 1.806
batch start
#iterations: 294
currently lose_sum: 595.571624994278
time_elpased: 1.821
batch start
#iterations: 295
currently lose_sum: 595.7090246677399
time_elpased: 1.799
batch start
#iterations: 296
currently lose_sum: 593.7824405431747
time_elpased: 1.824
batch start
#iterations: 297
currently lose_sum: 595.3499641418457
time_elpased: 1.811
batch start
#iterations: 298
currently lose_sum: 594.229111969471
time_elpased: 1.829
batch start
#iterations: 299
currently lose_sum: 596.2864340543747
time_elpased: 1.816
start validation test
0.57229281768
0.560895163528
0.936297211073
0.70153253012
0
validation finish
batch start
#iterations: 300
currently lose_sum: 595.9218711256981
time_elpased: 1.846
batch start
#iterations: 301
currently lose_sum: 593.0371452569962
time_elpased: 1.845
batch start
#iterations: 302
currently lose_sum: 595.3216770291328
time_elpased: 1.82
batch start
#iterations: 303
currently lose_sum: 595.0742329359055
time_elpased: 1.831
batch start
#iterations: 304
currently lose_sum: 593.8731221556664
time_elpased: 1.833
batch start
#iterations: 305
currently lose_sum: 593.0993633270264
time_elpased: 1.84
batch start
#iterations: 306
currently lose_sum: 595.3359690308571
time_elpased: 1.862
batch start
#iterations: 307
currently lose_sum: 593.6236970424652
time_elpased: 1.836
batch start
#iterations: 308
currently lose_sum: 594.5952525138855
time_elpased: 1.853
batch start
#iterations: 309
currently lose_sum: 594.5214605927467
time_elpased: 1.872
batch start
#iterations: 310
currently lose_sum: 594.3454034924507
time_elpased: 1.817
batch start
#iterations: 311
currently lose_sum: 593.9517657160759
time_elpased: 1.831
batch start
#iterations: 312
currently lose_sum: 593.6337287425995
time_elpased: 1.84
batch start
#iterations: 313
currently lose_sum: 595.8184455633163
time_elpased: 1.824
batch start
#iterations: 314
currently lose_sum: 594.6434999108315
time_elpased: 1.839
batch start
#iterations: 315
currently lose_sum: 594.0639087557793
time_elpased: 1.826
batch start
#iterations: 316
currently lose_sum: 593.1505182385445
time_elpased: 1.807
batch start
#iterations: 317
currently lose_sum: 594.5573028326035
time_elpased: 1.819
batch start
#iterations: 318
currently lose_sum: 593.615049302578
time_elpased: 1.836
batch start
#iterations: 319
currently lose_sum: 594.11703145504
time_elpased: 1.837
start validation test
0.572679558011
0.561197715697
0.935473911701
0.701537749137
0
validation finish
batch start
#iterations: 320
currently lose_sum: 592.5008478164673
time_elpased: 1.83
batch start
#iterations: 321
currently lose_sum: 593.3111680746078
time_elpased: 1.829
batch start
#iterations: 322
currently lose_sum: 593.343340575695
time_elpased: 1.813
batch start
#iterations: 323
currently lose_sum: 594.6854408979416
time_elpased: 1.798
batch start
#iterations: 324
currently lose_sum: 594.5140272974968
time_elpased: 1.794
batch start
#iterations: 325
currently lose_sum: 593.0791601538658
time_elpased: 1.8
batch start
#iterations: 326
currently lose_sum: 594.475154697895
time_elpased: 1.822
batch start
#iterations: 327
currently lose_sum: 595.6157940626144
time_elpased: 1.8
batch start
#iterations: 328
currently lose_sum: 592.9084042906761
time_elpased: 1.821
batch start
#iterations: 329
currently lose_sum: 593.1993893384933
time_elpased: 1.78
batch start
#iterations: 330
currently lose_sum: 594.327770113945
time_elpased: 1.815
batch start
#iterations: 331
currently lose_sum: 595.1086165308952
time_elpased: 1.811
batch start
#iterations: 332
currently lose_sum: 593.3343968987465
time_elpased: 1.823
batch start
#iterations: 333
currently lose_sum: 593.855170249939
time_elpased: 1.833
batch start
#iterations: 334
currently lose_sum: 595.3364928364754
time_elpased: 1.845
batch start
#iterations: 335
currently lose_sum: 593.9754219651222
time_elpased: 1.802
batch start
#iterations: 336
currently lose_sum: 593.5817140936852
time_elpased: 1.814
batch start
#iterations: 337
currently lose_sum: 594.162973344326
time_elpased: 1.8
batch start
#iterations: 338
currently lose_sum: 593.9929043650627
time_elpased: 1.819
batch start
#iterations: 339
currently lose_sum: 595.8651978969574
time_elpased: 1.838
start validation test
0.568895027624
0.558403515196
0.941648656993
0.701068842662
0
validation finish
batch start
#iterations: 340
currently lose_sum: 591.2800460457802
time_elpased: 1.809
batch start
#iterations: 341
currently lose_sum: 594.6821511983871
time_elpased: 1.801
batch start
#iterations: 342
currently lose_sum: 594.5469563603401
time_elpased: 1.817
batch start
#iterations: 343
currently lose_sum: 594.531814455986
time_elpased: 1.818
batch start
#iterations: 344
currently lose_sum: 593.1871490478516
time_elpased: 1.834
batch start
#iterations: 345
currently lose_sum: 593.6919825673103
time_elpased: 1.84
batch start
#iterations: 346
currently lose_sum: 594.1170959472656
time_elpased: 1.836
batch start
#iterations: 347
currently lose_sum: 595.7875229120255
time_elpased: 1.823
batch start
#iterations: 348
currently lose_sum: 594.4174608588219
time_elpased: 1.838
batch start
#iterations: 349
currently lose_sum: 593.8170068860054
time_elpased: 1.803
batch start
#iterations: 350
currently lose_sum: 595.707387149334
time_elpased: 1.81
batch start
#iterations: 351
currently lose_sum: 593.9492869377136
time_elpased: 1.811
batch start
#iterations: 352
currently lose_sum: 595.1638690233231
time_elpased: 1.802
batch start
#iterations: 353
currently lose_sum: 594.7326598763466
time_elpased: 1.822
batch start
#iterations: 354
currently lose_sum: 592.898617208004
time_elpased: 1.818
batch start
#iterations: 355
currently lose_sum: 594.3018454909325
time_elpased: 1.845
batch start
#iterations: 356
currently lose_sum: 593.2803230285645
time_elpased: 1.832
batch start
#iterations: 357
currently lose_sum: 592.8924868106842
time_elpased: 1.835
batch start
#iterations: 358
currently lose_sum: 595.1087564229965
time_elpased: 1.817
batch start
#iterations: 359
currently lose_sum: 591.7407066822052
time_elpased: 1.834
start validation test
0.56908839779
0.558558558559
0.941134094885
0.701048314456
0
validation finish
batch start
#iterations: 360
currently lose_sum: 593.5301685333252
time_elpased: 1.82
batch start
#iterations: 361
currently lose_sum: 593.4709205627441
time_elpased: 1.819
batch start
#iterations: 362
currently lose_sum: 594.6213244795799
time_elpased: 1.819
batch start
#iterations: 363
currently lose_sum: 594.6167550086975
time_elpased: 1.824
batch start
#iterations: 364
currently lose_sum: 594.4540188908577
time_elpased: 1.849
batch start
#iterations: 365
currently lose_sum: 594.3787482976913
time_elpased: 1.841
batch start
#iterations: 366
currently lose_sum: 593.1575244665146
time_elpased: 1.809
batch start
#iterations: 367
currently lose_sum: 593.835702419281
time_elpased: 1.836
batch start
#iterations: 368
currently lose_sum: 593.360212624073
time_elpased: 1.813
batch start
#iterations: 369
currently lose_sum: 593.9420389533043
time_elpased: 1.823
batch start
#iterations: 370
currently lose_sum: 591.7292903661728
time_elpased: 1.819
batch start
#iterations: 371
currently lose_sum: 592.4063431024551
time_elpased: 1.822
batch start
#iterations: 372
currently lose_sum: 593.9815708398819
time_elpased: 1.817
batch start
#iterations: 373
currently lose_sum: 595.5327131152153
time_elpased: 1.828
batch start
#iterations: 374
currently lose_sum: 593.1019361615181
time_elpased: 1.858
batch start
#iterations: 375
currently lose_sum: 594.7069283723831
time_elpased: 1.822
batch start
#iterations: 376
currently lose_sum: 593.1589690446854
time_elpased: 1.839
batch start
#iterations: 377
currently lose_sum: 592.906062245369
time_elpased: 1.817
batch start
#iterations: 378
currently lose_sum: 593.2823240756989
time_elpased: 1.833
batch start
#iterations: 379
currently lose_sum: 593.7664269208908
time_elpased: 1.809
start validation test
0.57179558011
0.560688825109
0.934856437172
0.700966491116
0
validation finish
batch start
#iterations: 380
currently lose_sum: 593.6119850873947
time_elpased: 1.828
batch start
#iterations: 381
currently lose_sum: 593.6304271221161
time_elpased: 1.799
batch start
#iterations: 382
currently lose_sum: 594.2326261401176
time_elpased: 1.82
batch start
#iterations: 383
currently lose_sum: 593.5804164409637
time_elpased: 1.839
batch start
#iterations: 384
currently lose_sum: 596.2332534790039
time_elpased: 1.81
batch start
#iterations: 385
currently lose_sum: 594.3228883743286
time_elpased: 1.781
batch start
#iterations: 386
currently lose_sum: 594.6796221733093
time_elpased: 1.795
batch start
#iterations: 387
currently lose_sum: 594.0167148113251
time_elpased: 1.792
batch start
#iterations: 388
currently lose_sum: 595.4481227993965
time_elpased: 1.827
batch start
#iterations: 389
currently lose_sum: 593.164475440979
time_elpased: 1.818
batch start
#iterations: 390
currently lose_sum: 594.8864066004753
time_elpased: 1.84
batch start
#iterations: 391
currently lose_sum: 596.4562419056892
time_elpased: 1.821
batch start
#iterations: 392
currently lose_sum: 593.6053855419159
time_elpased: 1.814
batch start
#iterations: 393
currently lose_sum: 595.028614461422
time_elpased: 1.81
batch start
#iterations: 394
currently lose_sum: 594.842126250267
time_elpased: 1.827
batch start
#iterations: 395
currently lose_sum: 595.028876543045
time_elpased: 1.832
batch start
#iterations: 396
currently lose_sum: 594.6215808391571
time_elpased: 1.812
batch start
#iterations: 397
currently lose_sum: 594.6713246107101
time_elpased: 1.809
batch start
#iterations: 398
currently lose_sum: 594.3458588719368
time_elpased: 1.803
batch start
#iterations: 399
currently lose_sum: 594.4122848510742
time_elpased: 1.83
start validation test
0.569005524862
0.558493100501
0.941339919728
0.701053841732
0
validation finish
acc: 0.568
pre: 0.559
rec: 0.938
F1: 0.700
auc: 0.000
