start to construct graph
graph construct over
93050
epochs start
batch start
#iterations: 0
currently lose_sum: 613.4866806864738
time_elpased: 1.852
batch start
#iterations: 1
currently lose_sum: 607.1537585258484
time_elpased: 1.796
batch start
#iterations: 2
currently lose_sum: 605.790461242199
time_elpased: 1.816
batch start
#iterations: 3
currently lose_sum: 604.4242213368416
time_elpased: 1.812
batch start
#iterations: 4
currently lose_sum: 603.1424520611763
time_elpased: 1.815
batch start
#iterations: 5
currently lose_sum: 602.1392189264297
time_elpased: 1.815
batch start
#iterations: 6
currently lose_sum: 599.1230466365814
time_elpased: 1.869
batch start
#iterations: 7
currently lose_sum: 602.129699409008
time_elpased: 1.834
batch start
#iterations: 8
currently lose_sum: 599.1606135964394
time_elpased: 1.83
batch start
#iterations: 9
currently lose_sum: 598.41224193573
time_elpased: 1.804
batch start
#iterations: 10
currently lose_sum: 598.0755993127823
time_elpased: 1.804
batch start
#iterations: 11
currently lose_sum: 597.2655056118965
time_elpased: 1.8
batch start
#iterations: 12
currently lose_sum: 597.1249506473541
time_elpased: 1.855
batch start
#iterations: 13
currently lose_sum: 597.8582127094269
time_elpased: 1.803
batch start
#iterations: 14
currently lose_sum: 596.0636093616486
time_elpased: 1.824
batch start
#iterations: 15
currently lose_sum: 597.774430334568
time_elpased: 1.821
batch start
#iterations: 16
currently lose_sum: 597.2747775912285
time_elpased: 1.813
batch start
#iterations: 17
currently lose_sum: 597.241296172142
time_elpased: 1.798
batch start
#iterations: 18
currently lose_sum: 594.1816803216934
time_elpased: 1.8
batch start
#iterations: 19
currently lose_sum: 595.3170244693756
time_elpased: 1.823
start validation test
0.569585635359
0.560874648466
0.913347741072
0.694974648108
0
validation finish
batch start
#iterations: 20
currently lose_sum: 594.8969326615334
time_elpased: 1.828
batch start
#iterations: 21
currently lose_sum: 593.9178510904312
time_elpased: 1.795
batch start
#iterations: 22
currently lose_sum: 593.9226009249687
time_elpased: 1.783
batch start
#iterations: 23
currently lose_sum: 596.4946488142014
time_elpased: 1.827
batch start
#iterations: 24
currently lose_sum: 594.7650837302208
time_elpased: 1.807
batch start
#iterations: 25
currently lose_sum: 595.9196102023125
time_elpased: 1.788
batch start
#iterations: 26
currently lose_sum: 594.8391338586807
time_elpased: 1.781
batch start
#iterations: 27
currently lose_sum: 592.3627833127975
time_elpased: 1.797
batch start
#iterations: 28
currently lose_sum: 592.1347101330757
time_elpased: 1.788
batch start
#iterations: 29
currently lose_sum: 593.4872137308121
time_elpased: 1.778
batch start
#iterations: 30
currently lose_sum: 594.4841052293777
time_elpased: 1.816
batch start
#iterations: 31
currently lose_sum: 593.2454192638397
time_elpased: 1.799
batch start
#iterations: 32
currently lose_sum: 594.8861377239227
time_elpased: 1.79
batch start
#iterations: 33
currently lose_sum: 594.8647285103798
time_elpased: 1.767
batch start
#iterations: 34
currently lose_sum: 592.5983552932739
time_elpased: 1.8
batch start
#iterations: 35
currently lose_sum: 593.2011258006096
time_elpased: 1.78
batch start
#iterations: 36
currently lose_sum: 592.9235073924065
time_elpased: 1.782
batch start
#iterations: 37
currently lose_sum: 594.0013943314552
time_elpased: 1.767
batch start
#iterations: 38
currently lose_sum: 593.1229806542397
time_elpased: 1.807
batch start
#iterations: 39
currently lose_sum: 591.7356334328651
time_elpased: 1.813
start validation test
0.572209944751
0.562005277045
0.920654523001
0.697952018724
0
validation finish
batch start
#iterations: 40
currently lose_sum: 594.7224693000317
time_elpased: 1.853
batch start
#iterations: 41
currently lose_sum: 593.6511506438255
time_elpased: 1.812
batch start
#iterations: 42
currently lose_sum: 594.1274983286858
time_elpased: 1.79
batch start
#iterations: 43
currently lose_sum: 593.5298256874084
time_elpased: 1.831
batch start
#iterations: 44
currently lose_sum: 594.1473028063774
time_elpased: 1.818
batch start
#iterations: 45
currently lose_sum: 593.7933573722839
time_elpased: 1.799
batch start
#iterations: 46
currently lose_sum: 593.5170632600784
time_elpased: 1.785
batch start
#iterations: 47
currently lose_sum: 592.8560407757759
time_elpased: 1.794
batch start
#iterations: 48
currently lose_sum: 596.722783446312
time_elpased: 1.8
batch start
#iterations: 49
currently lose_sum: 592.8471239805222
time_elpased: 1.801
batch start
#iterations: 50
currently lose_sum: 592.6462326049805
time_elpased: 1.795
batch start
#iterations: 51
currently lose_sum: 592.1855018734932
time_elpased: 1.783
batch start
#iterations: 52
currently lose_sum: 593.3578178286552
time_elpased: 1.785
batch start
#iterations: 53
currently lose_sum: 593.9915788769722
time_elpased: 1.797
batch start
#iterations: 54
currently lose_sum: 592.5054190158844
time_elpased: 1.797
batch start
#iterations: 55
currently lose_sum: 593.1779964566231
time_elpased: 1.772
batch start
#iterations: 56
currently lose_sum: 593.83994525671
time_elpased: 1.817
batch start
#iterations: 57
currently lose_sum: 591.9435470700264
time_elpased: 1.763
batch start
#iterations: 58
currently lose_sum: 594.9503911733627
time_elpased: 1.79
batch start
#iterations: 59
currently lose_sum: 592.1603254675865
time_elpased: 1.786
start validation test
0.570718232044
0.560140853772
0.933106926006
0.700046324892
0
validation finish
batch start
#iterations: 60
currently lose_sum: 592.2656389474869
time_elpased: 1.873
batch start
#iterations: 61
currently lose_sum: 594.1323380470276
time_elpased: 1.789
batch start
#iterations: 62
currently lose_sum: 594.2547323107719
time_elpased: 1.794
batch start
#iterations: 63
currently lose_sum: 591.7526704668999
time_elpased: 1.793
batch start
#iterations: 64
currently lose_sum: 594.1784746050835
time_elpased: 1.799
batch start
#iterations: 65
currently lose_sum: 592.0623483657837
time_elpased: 1.789
batch start
#iterations: 66
currently lose_sum: 591.5055660009384
time_elpased: 1.818
batch start
#iterations: 67
currently lose_sum: 590.6758492588997
time_elpased: 1.786
batch start
#iterations: 68
currently lose_sum: 593.2211441397667
time_elpased: 1.783
batch start
#iterations: 69
currently lose_sum: 593.9606412053108
time_elpased: 1.798
batch start
#iterations: 70
currently lose_sum: 594.8769127130508
time_elpased: 1.795
batch start
#iterations: 71
currently lose_sum: 591.9777575731277
time_elpased: 1.81
batch start
#iterations: 72
currently lose_sum: 593.6695347428322
time_elpased: 1.792
batch start
#iterations: 73
currently lose_sum: 593.7296530604362
time_elpased: 1.774
batch start
#iterations: 74
currently lose_sum: 593.681508243084
time_elpased: 1.787
batch start
#iterations: 75
currently lose_sum: 592.0610820055008
time_elpased: 1.804
batch start
#iterations: 76
currently lose_sum: 591.2481539845467
time_elpased: 1.802
batch start
#iterations: 77
currently lose_sum: 591.6036176085472
time_elpased: 1.796
batch start
#iterations: 78
currently lose_sum: 594.7354412674904
time_elpased: 1.894
batch start
#iterations: 79
currently lose_sum: 590.573272228241
time_elpased: 1.932
start validation test
0.567182320442
0.55693293826
0.947823402285
0.701607374114
0
validation finish
batch start
#iterations: 80
currently lose_sum: 592.5857794880867
time_elpased: 1.906
batch start
#iterations: 81
currently lose_sum: 592.9194568395615
time_elpased: 1.804
batch start
#iterations: 82
currently lose_sum: 593.0173192620277
time_elpased: 1.823
batch start
#iterations: 83
currently lose_sum: 591.1615143418312
time_elpased: 1.816
batch start
#iterations: 84
currently lose_sum: 592.8638211488724
time_elpased: 1.822
batch start
#iterations: 85
currently lose_sum: 591.9244566559792
time_elpased: 1.815
batch start
#iterations: 86
currently lose_sum: 590.9460969567299
time_elpased: 1.774
batch start
#iterations: 87
currently lose_sum: 592.7499700784683
time_elpased: 1.811
batch start
#iterations: 88
currently lose_sum: 591.0360174775124
time_elpased: 1.937
batch start
#iterations: 89
currently lose_sum: 591.0017389655113
time_elpased: 1.886
batch start
#iterations: 90
currently lose_sum: 592.1157602667809
time_elpased: 1.893
batch start
#iterations: 91
currently lose_sum: 592.2366009950638
time_elpased: 1.824
batch start
#iterations: 92
currently lose_sum: 591.9076989293098
time_elpased: 1.821
batch start
#iterations: 93
currently lose_sum: 591.8200608491898
time_elpased: 1.844
batch start
#iterations: 94
currently lose_sum: 592.0036686658859
time_elpased: 1.819
batch start
#iterations: 95
currently lose_sum: 591.7248999476433
time_elpased: 1.802
batch start
#iterations: 96
currently lose_sum: 592.8794776797295
time_elpased: 1.796
batch start
#iterations: 97
currently lose_sum: 593.0892149806023
time_elpased: 1.812
batch start
#iterations: 98
currently lose_sum: 590.9046240448952
time_elpased: 1.818
batch start
#iterations: 99
currently lose_sum: 591.9542057514191
time_elpased: 1.813
start validation test
0.572016574586
0.560879908549
0.934136050221
0.700913109786
0
validation finish
batch start
#iterations: 100
currently lose_sum: 593.473641037941
time_elpased: 1.816
batch start
#iterations: 101
currently lose_sum: 593.048436164856
time_elpased: 1.838
batch start
#iterations: 102
currently lose_sum: 589.8302530050278
time_elpased: 1.815
batch start
#iterations: 103
currently lose_sum: 594.1398370265961
time_elpased: 1.813
batch start
#iterations: 104
currently lose_sum: 591.3898385763168
time_elpased: 1.795
batch start
#iterations: 105
currently lose_sum: 591.3332805633545
time_elpased: 1.791
batch start
#iterations: 106
currently lose_sum: 593.2074447274208
time_elpased: 1.788
batch start
#iterations: 107
currently lose_sum: 592.0553811192513
time_elpased: 1.762
batch start
#iterations: 108
currently lose_sum: 592.0309012532234
time_elpased: 1.767
batch start
#iterations: 109
currently lose_sum: 591.9272859096527
time_elpased: 1.778
batch start
#iterations: 110
currently lose_sum: 593.3076831102371
time_elpased: 1.802
batch start
#iterations: 111
currently lose_sum: 591.6983733177185
time_elpased: 1.787
batch start
#iterations: 112
currently lose_sum: 590.8875176906586
time_elpased: 1.793
batch start
#iterations: 113
currently lose_sum: 594.3462698459625
time_elpased: 1.794
batch start
#iterations: 114
currently lose_sum: 592.4533772468567
time_elpased: 1.796
batch start
#iterations: 115
currently lose_sum: 591.4344989061356
time_elpased: 1.84
batch start
#iterations: 116
currently lose_sum: 591.8928700089455
time_elpased: 1.785
batch start
#iterations: 117
currently lose_sum: 593.4431974887848
time_elpased: 1.792
batch start
#iterations: 118
currently lose_sum: 592.7698798179626
time_elpased: 1.797
batch start
#iterations: 119
currently lose_sum: 591.4219079017639
time_elpased: 1.793
start validation test
0.567955801105
0.557572078907
0.945353504168
0.701435552841
0
validation finish
batch start
#iterations: 120
currently lose_sum: 593.7236259579659
time_elpased: 1.8
batch start
#iterations: 121
currently lose_sum: 592.6317883729935
time_elpased: 1.817
batch start
#iterations: 122
currently lose_sum: 593.2710964679718
time_elpased: 1.816
batch start
#iterations: 123
currently lose_sum: 591.2770663499832
time_elpased: 1.847
batch start
#iterations: 124
currently lose_sum: 594.1021945476532
time_elpased: 1.801
batch start
#iterations: 125
currently lose_sum: 591.6375985145569
time_elpased: 1.798
batch start
#iterations: 126
currently lose_sum: 593.5132958889008
time_elpased: 1.754
batch start
#iterations: 127
currently lose_sum: 592.9854679703712
time_elpased: 1.783
batch start
#iterations: 128
currently lose_sum: 592.3200941681862
time_elpased: 1.783
batch start
#iterations: 129
currently lose_sum: 591.7140777111053
time_elpased: 1.792
batch start
#iterations: 130
currently lose_sum: 591.328272819519
time_elpased: 1.812
batch start
#iterations: 131
currently lose_sum: 591.0103580355644
time_elpased: 1.788
batch start
#iterations: 132
currently lose_sum: 590.8871338367462
time_elpased: 1.798
batch start
#iterations: 133
currently lose_sum: 593.6588401794434
time_elpased: 1.792
batch start
#iterations: 134
currently lose_sum: 594.0478656291962
time_elpased: 1.763
batch start
#iterations: 135
currently lose_sum: 592.5658602118492
time_elpased: 1.767
batch start
#iterations: 136
currently lose_sum: 590.5112943649292
time_elpased: 1.774
batch start
#iterations: 137
currently lose_sum: 592.2242801785469
time_elpased: 1.807
batch start
#iterations: 138
currently lose_sum: 593.1087028980255
time_elpased: 1.768
batch start
#iterations: 139
currently lose_sum: 593.0311140418053
time_elpased: 1.814
start validation test
0.576049723757
0.564829796009
0.916126376454
0.698812677853
0
validation finish
batch start
#iterations: 140
currently lose_sum: 591.753764629364
time_elpased: 1.824
batch start
#iterations: 141
currently lose_sum: 592.8582732081413
time_elpased: 1.824
batch start
#iterations: 142
currently lose_sum: 591.5832344293594
time_elpased: 1.788
batch start
#iterations: 143
currently lose_sum: 593.8368526697159
time_elpased: 1.807
batch start
#iterations: 144
currently lose_sum: 591.6626975536346
time_elpased: 1.798
batch start
#iterations: 145
currently lose_sum: 593.1873451471329
time_elpased: 1.822
batch start
#iterations: 146
currently lose_sum: 590.3691124916077
time_elpased: 1.832
batch start
#iterations: 147
currently lose_sum: 591.7210575938225
time_elpased: 1.844
batch start
#iterations: 148
currently lose_sum: 591.8622192144394
time_elpased: 1.79
batch start
#iterations: 149
currently lose_sum: 591.9236447811127
time_elpased: 1.787
batch start
#iterations: 150
currently lose_sum: 591.1927421689034
time_elpased: 1.813
batch start
#iterations: 151
currently lose_sum: 592.5515049099922
time_elpased: 1.814
batch start
#iterations: 152
currently lose_sum: 591.5050928592682
time_elpased: 1.791
batch start
#iterations: 153
currently lose_sum: 591.1446458101273
time_elpased: 1.795
batch start
#iterations: 154
currently lose_sum: 590.7526822686195
time_elpased: 1.82
batch start
#iterations: 155
currently lose_sum: 592.3126843571663
time_elpased: 1.787
batch start
#iterations: 156
currently lose_sum: 591.5200058221817
time_elpased: 1.845
batch start
#iterations: 157
currently lose_sum: 591.0977843403816
time_elpased: 1.803
batch start
#iterations: 158
currently lose_sum: 591.4299845695496
time_elpased: 1.798
batch start
#iterations: 159
currently lose_sum: 591.5846257209778
time_elpased: 1.8
start validation test
0.57273480663
0.561582166475
0.930739940311
0.700501520051
0
validation finish
batch start
#iterations: 160
currently lose_sum: 592.3534742593765
time_elpased: 1.812
batch start
#iterations: 161
currently lose_sum: 593.1797387599945
time_elpased: 1.814
batch start
#iterations: 162
currently lose_sum: 591.4446001052856
time_elpased: 1.808
batch start
#iterations: 163
currently lose_sum: 591.0554491877556
time_elpased: 1.775
batch start
#iterations: 164
currently lose_sum: 591.1851812005043
time_elpased: 1.776
batch start
#iterations: 165
currently lose_sum: 592.4613481760025
time_elpased: 1.801
batch start
#iterations: 166
currently lose_sum: 592.5232194662094
time_elpased: 1.815
batch start
#iterations: 167
currently lose_sum: 591.8440864682198
time_elpased: 1.848
batch start
#iterations: 168
currently lose_sum: 593.2384693026543
time_elpased: 1.832
batch start
#iterations: 169
currently lose_sum: 591.3241138458252
time_elpased: 1.804
batch start
#iterations: 170
currently lose_sum: 590.5135817527771
time_elpased: 1.813
batch start
#iterations: 171
currently lose_sum: 590.7184776067734
time_elpased: 1.82
batch start
#iterations: 172
currently lose_sum: 592.5002616047859
time_elpased: 1.797
batch start
#iterations: 173
currently lose_sum: 592.0132722258568
time_elpased: 1.819
batch start
#iterations: 174
currently lose_sum: 590.9866896867752
time_elpased: 1.821
batch start
#iterations: 175
currently lose_sum: 592.1386624574661
time_elpased: 1.849
batch start
#iterations: 176
currently lose_sum: 592.2632231116295
time_elpased: 1.813
batch start
#iterations: 177
currently lose_sum: 590.7648594975471
time_elpased: 1.817
batch start
#iterations: 178
currently lose_sum: 591.2138935923576
time_elpased: 1.786
batch start
#iterations: 179
currently lose_sum: 592.235033929348
time_elpased: 1.81
start validation test
0.572651933702
0.561591050342
0.929916640939
0.700275119154
0
validation finish
batch start
#iterations: 180
currently lose_sum: 591.0688599348068
time_elpased: 1.811
batch start
#iterations: 181
currently lose_sum: 591.0935395359993
time_elpased: 1.792
batch start
#iterations: 182
currently lose_sum: 591.7998685240746
time_elpased: 1.784
batch start
#iterations: 183
currently lose_sum: 591.1941268444061
time_elpased: 1.78
batch start
#iterations: 184
currently lose_sum: 592.1800358295441
time_elpased: 1.809
batch start
#iterations: 185
currently lose_sum: 591.4923545122147
time_elpased: 1.811
batch start
#iterations: 186
currently lose_sum: 591.979677438736
time_elpased: 1.917
batch start
#iterations: 187
currently lose_sum: 591.9198474884033
time_elpased: 1.892
batch start
#iterations: 188
currently lose_sum: 590.3660436868668
time_elpased: 1.809
batch start
#iterations: 189
currently lose_sum: 592.5469138026237
time_elpased: 1.794
batch start
#iterations: 190
currently lose_sum: 591.2803427577019
time_elpased: 1.783
batch start
#iterations: 191
currently lose_sum: 590.2579466700554
time_elpased: 1.784
batch start
#iterations: 192
currently lose_sum: 591.4075205922127
time_elpased: 1.786
batch start
#iterations: 193
currently lose_sum: 592.4780588746071
time_elpased: 1.778
batch start
#iterations: 194
currently lose_sum: 590.3766103386879
time_elpased: 1.815
batch start
#iterations: 195
currently lose_sum: 592.1292529702187
time_elpased: 1.803
batch start
#iterations: 196
currently lose_sum: 590.9723073840141
time_elpased: 1.791
batch start
#iterations: 197
currently lose_sum: 593.7441703677177
time_elpased: 1.798
batch start
#iterations: 198
currently lose_sum: 591.6413712501526
time_elpased: 1.802
batch start
#iterations: 199
currently lose_sum: 590.9367642998695
time_elpased: 1.789
start validation test
0.573093922652
0.561648079306
0.932901101163
0.701164094829
0
validation finish
batch start
#iterations: 200
currently lose_sum: 592.7783917784691
time_elpased: 1.801
batch start
#iterations: 201
currently lose_sum: 589.2072353959084
time_elpased: 1.821
batch start
#iterations: 202
currently lose_sum: 590.962588429451
time_elpased: 1.809
batch start
#iterations: 203
currently lose_sum: 590.7322924137115
time_elpased: 1.806
batch start
#iterations: 204
currently lose_sum: 591.4959009885788
time_elpased: 1.818
batch start
#iterations: 205
currently lose_sum: 591.7651310563087
time_elpased: 1.793
batch start
#iterations: 206
currently lose_sum: 592.162351667881
time_elpased: 1.807
batch start
#iterations: 207
currently lose_sum: 592.4854692816734
time_elpased: 1.805
batch start
#iterations: 208
currently lose_sum: 592.792112827301
time_elpased: 1.815
batch start
#iterations: 209
currently lose_sum: 592.8263870477676
time_elpased: 1.795
batch start
#iterations: 210
currently lose_sum: 591.8445337414742
time_elpased: 1.819
batch start
#iterations: 211
currently lose_sum: 592.4067966938019
time_elpased: 1.808
batch start
#iterations: 212
currently lose_sum: 590.6788218617439
time_elpased: 1.796
batch start
#iterations: 213
currently lose_sum: 592.9375925064087
time_elpased: 1.801
batch start
#iterations: 214
currently lose_sum: 590.9877940416336
time_elpased: 1.801
batch start
#iterations: 215
currently lose_sum: 590.4908699393272
time_elpased: 1.781
batch start
#iterations: 216
currently lose_sum: 591.7437242269516
time_elpased: 1.783
batch start
#iterations: 217
currently lose_sum: 592.6501630544662
time_elpased: 1.824
batch start
#iterations: 218
currently lose_sum: 593.2378996610641
time_elpased: 1.786
batch start
#iterations: 219
currently lose_sum: 592.5932125449181
time_elpased: 1.818
start validation test
0.57044198895
0.559490258547
0.939796233405
0.701409424325
0
validation finish
batch start
#iterations: 220
currently lose_sum: 590.6509634852409
time_elpased: 1.827
batch start
#iterations: 221
currently lose_sum: 592.1084437966347
time_elpased: 1.821
batch start
#iterations: 222
currently lose_sum: 593.4147027134895
time_elpased: 1.81
batch start
#iterations: 223
currently lose_sum: 591.6488239765167
time_elpased: 1.827
batch start
#iterations: 224
currently lose_sum: 593.3646920323372
time_elpased: 1.817
batch start
#iterations: 225
currently lose_sum: 591.7982484102249
time_elpased: 1.792
batch start
#iterations: 226
currently lose_sum: 592.880527973175
time_elpased: 1.786
batch start
#iterations: 227
currently lose_sum: 590.3374025821686
time_elpased: 1.783
batch start
#iterations: 228
currently lose_sum: 590.3578556776047
time_elpased: 1.812
batch start
#iterations: 229
currently lose_sum: 592.5294418334961
time_elpased: 1.796
batch start
#iterations: 230
currently lose_sum: 590.7010445594788
time_elpased: 1.841
batch start
#iterations: 231
currently lose_sum: 591.6286720633507
time_elpased: 1.768
batch start
#iterations: 232
currently lose_sum: 592.0653511881828
time_elpased: 1.796
batch start
#iterations: 233
currently lose_sum: 592.4669936299324
time_elpased: 1.782
batch start
#iterations: 234
currently lose_sum: 591.1868385076523
time_elpased: 1.836
batch start
#iterations: 235
currently lose_sum: 590.8914315104485
time_elpased: 1.796
batch start
#iterations: 236
currently lose_sum: 592.0119273662567
time_elpased: 1.792
batch start
#iterations: 237
currently lose_sum: 590.6510808467865
time_elpased: 1.799
batch start
#iterations: 238
currently lose_sum: 592.3130326271057
time_elpased: 1.797
batch start
#iterations: 239
currently lose_sum: 590.0944433808327
time_elpased: 1.781
start validation test
0.571740331492
0.56049739912
0.937017598024
0.701423261368
0
validation finish
batch start
#iterations: 240
currently lose_sum: 590.5088494420052
time_elpased: 1.79
batch start
#iterations: 241
currently lose_sum: 590.7231595516205
time_elpased: 1.801
batch start
#iterations: 242
currently lose_sum: 592.5819701552391
time_elpased: 1.803
batch start
#iterations: 243
currently lose_sum: 591.6127611398697
time_elpased: 1.793
batch start
#iterations: 244
currently lose_sum: 592.5587123036385
time_elpased: 1.805
batch start
#iterations: 245
currently lose_sum: 592.004597067833
time_elpased: 1.806
batch start
#iterations: 246
currently lose_sum: 591.3601134419441
time_elpased: 1.777
batch start
#iterations: 247
currently lose_sum: 592.2311341762543
time_elpased: 1.77
batch start
#iterations: 248
currently lose_sum: 592.6352480649948
time_elpased: 1.779
batch start
#iterations: 249
currently lose_sum: 592.1147302389145
time_elpased: 1.772
batch start
#iterations: 250
currently lose_sum: 591.6423934102058
time_elpased: 1.786
batch start
#iterations: 251
currently lose_sum: 592.0509829521179
time_elpased: 1.78
batch start
#iterations: 252
currently lose_sum: 590.7186296582222
time_elpased: 1.809
batch start
#iterations: 253
currently lose_sum: 592.5269200801849
time_elpased: 1.81
batch start
#iterations: 254
currently lose_sum: 590.2479392290115
time_elpased: 1.805
batch start
#iterations: 255
currently lose_sum: 590.5026423931122
time_elpased: 1.798
batch start
#iterations: 256
currently lose_sum: 591.6585050225258
time_elpased: 1.793
batch start
#iterations: 257
currently lose_sum: 593.6279891133308
time_elpased: 1.787
batch start
#iterations: 258
currently lose_sum: 590.6000009179115
time_elpased: 1.782
batch start
#iterations: 259
currently lose_sum: 590.9148085713387
time_elpased: 1.755
start validation test
0.573370165746
0.561991175191
0.930637027889
0.700790452573
0
validation finish
batch start
#iterations: 260
currently lose_sum: 591.2285398244858
time_elpased: 1.815
batch start
#iterations: 261
currently lose_sum: 592.7071204781532
time_elpased: 1.794
batch start
#iterations: 262
currently lose_sum: 590.2142034769058
time_elpased: 1.789
batch start
#iterations: 263
currently lose_sum: 593.4345457553864
time_elpased: 1.783
batch start
#iterations: 264
currently lose_sum: 591.1017421483994
time_elpased: 1.793
batch start
#iterations: 265
currently lose_sum: 592.632133603096
time_elpased: 1.778
batch start
#iterations: 266
currently lose_sum: 592.0283404588699
time_elpased: 1.826
batch start
#iterations: 267
currently lose_sum: 592.6040660738945
time_elpased: 1.779
batch start
#iterations: 268
currently lose_sum: 590.5892699360847
time_elpased: 1.8
batch start
#iterations: 269
currently lose_sum: 592.6997874975204
time_elpased: 1.791
batch start
#iterations: 270
currently lose_sum: 590.6062322854996
time_elpased: 1.782
batch start
#iterations: 271
currently lose_sum: 591.7503631711006
time_elpased: 1.789
batch start
#iterations: 272
currently lose_sum: 592.4908666610718
time_elpased: 1.788
batch start
#iterations: 273
currently lose_sum: 591.3861439228058
time_elpased: 1.81
batch start
#iterations: 274
currently lose_sum: 591.8396161794662
time_elpased: 1.807
batch start
#iterations: 275
currently lose_sum: 591.895804464817
time_elpased: 1.829
batch start
#iterations: 276
currently lose_sum: 590.9870216846466
time_elpased: 1.789
batch start
#iterations: 277
currently lose_sum: 591.8124600052834
time_elpased: 1.788
batch start
#iterations: 278
currently lose_sum: 590.6979460716248
time_elpased: 1.816
batch start
#iterations: 279
currently lose_sum: 591.0047248601913
time_elpased: 1.814
start validation test
0.572872928177
0.561281165144
0.935988473809
0.701747617762
0
validation finish
batch start
#iterations: 280
currently lose_sum: 593.3349814414978
time_elpased: 1.816
batch start
#iterations: 281
currently lose_sum: 590.8646325469017
time_elpased: 1.82
batch start
#iterations: 282
currently lose_sum: 590.1370331645012
time_elpased: 1.772
batch start
#iterations: 283
currently lose_sum: 591.1892797350883
time_elpased: 1.797
batch start
#iterations: 284
currently lose_sum: 591.6714487075806
time_elpased: 1.795
batch start
#iterations: 285
currently lose_sum: 591.9435913562775
time_elpased: 1.78
batch start
#iterations: 286
currently lose_sum: 592.8779764771461
time_elpased: 1.807
batch start
#iterations: 287
currently lose_sum: 592.0982741117477
time_elpased: 1.779
batch start
#iterations: 288
currently lose_sum: 592.0584167838097
time_elpased: 1.776
batch start
#iterations: 289
currently lose_sum: 591.1555528044701
time_elpased: 1.788
batch start
#iterations: 290
currently lose_sum: 591.1988367438316
time_elpased: 1.787
batch start
#iterations: 291
currently lose_sum: 592.2479833364487
time_elpased: 1.778
batch start
#iterations: 292
currently lose_sum: 593.3312881588936
time_elpased: 1.806
batch start
#iterations: 293
currently lose_sum: 593.0276716351509
time_elpased: 1.775
batch start
#iterations: 294
currently lose_sum: 590.8971091508865
time_elpased: 1.788
batch start
#iterations: 295
currently lose_sum: 594.4690274000168
time_elpased: 1.788
batch start
#iterations: 296
currently lose_sum: 591.4650431275368
time_elpased: 1.798
batch start
#iterations: 297
currently lose_sum: 591.0316378474236
time_elpased: 1.811
batch start
#iterations: 298
currently lose_sum: 591.7110713124275
time_elpased: 1.817
batch start
#iterations: 299
currently lose_sum: 593.0171027183533
time_elpased: 1.801
start validation test
0.572651933702
0.561293913904
0.933930225378
0.701178288584
0
validation finish
batch start
#iterations: 300
currently lose_sum: 591.0919618010521
time_elpased: 1.82
batch start
#iterations: 301
currently lose_sum: 591.3931859135628
time_elpased: 1.801
batch start
#iterations: 302
currently lose_sum: 590.2887672781944
time_elpased: 1.826
batch start
#iterations: 303
currently lose_sum: 590.2987191081047
time_elpased: 1.789
batch start
#iterations: 304
currently lose_sum: 590.4100484251976
time_elpased: 1.772
batch start
#iterations: 305
currently lose_sum: 591.0480499863625
time_elpased: 1.809
batch start
#iterations: 306
currently lose_sum: 593.1605487465858
time_elpased: 1.809
batch start
#iterations: 307
currently lose_sum: 591.5921524167061
time_elpased: 1.8
batch start
#iterations: 308
currently lose_sum: 590.8726840019226
time_elpased: 1.808
batch start
#iterations: 309
currently lose_sum: 591.8175341486931
time_elpased: 1.78
batch start
#iterations: 310
currently lose_sum: 590.393328845501
time_elpased: 1.761
batch start
#iterations: 311
currently lose_sum: 593.0163246393204
time_elpased: 1.77
batch start
#iterations: 312
currently lose_sum: 591.4420356750488
time_elpased: 1.772
batch start
#iterations: 313
currently lose_sum: 590.3957557678223
time_elpased: 1.82
batch start
#iterations: 314
currently lose_sum: 590.1542788147926
time_elpased: 1.849
batch start
#iterations: 315
currently lose_sum: 591.0361655950546
time_elpased: 1.819
batch start
#iterations: 316
currently lose_sum: 593.083081305027
time_elpased: 1.811
batch start
#iterations: 317
currently lose_sum: 590.4183451533318
time_elpased: 1.831
batch start
#iterations: 318
currently lose_sum: 591.8087794184685
time_elpased: 1.827
batch start
#iterations: 319
currently lose_sum: 592.44640147686
time_elpased: 1.824
start validation test
0.572983425414
0.56187749004
0.928887516723
0.700205577751
0
validation finish
batch start
#iterations: 320
currently lose_sum: 591.2087668776512
time_elpased: 1.835
batch start
#iterations: 321
currently lose_sum: 592.6385452151299
time_elpased: 1.796
batch start
#iterations: 322
currently lose_sum: 590.0890467762947
time_elpased: 1.788
batch start
#iterations: 323
currently lose_sum: 591.8577999472618
time_elpased: 1.821
batch start
#iterations: 324
currently lose_sum: 591.694111764431
time_elpased: 1.838
batch start
#iterations: 325
currently lose_sum: 590.4456750750542
time_elpased: 1.805
batch start
#iterations: 326
currently lose_sum: 589.9738542437553
time_elpased: 1.856
batch start
#iterations: 327
currently lose_sum: 592.3650653958321
time_elpased: 1.845
batch start
#iterations: 328
currently lose_sum: 592.0000393986702
time_elpased: 1.824
batch start
#iterations: 329
currently lose_sum: 589.821962416172
time_elpased: 1.84
batch start
#iterations: 330
currently lose_sum: 591.177783370018
time_elpased: 1.811
batch start
#iterations: 331
currently lose_sum: 593.523423075676
time_elpased: 1.829
batch start
#iterations: 332
currently lose_sum: 591.4414595961571
time_elpased: 1.834
batch start
#iterations: 333
currently lose_sum: 591.966328561306
time_elpased: 1.833
batch start
#iterations: 334
currently lose_sum: 592.3542090654373
time_elpased: 1.809
batch start
#iterations: 335
currently lose_sum: 592.2049567103386
time_elpased: 1.819
batch start
#iterations: 336
currently lose_sum: 588.6764678359032
time_elpased: 1.823
batch start
#iterations: 337
currently lose_sum: 590.0351609587669
time_elpased: 1.8
batch start
#iterations: 338
currently lose_sum: 591.9520468711853
time_elpased: 1.767
batch start
#iterations: 339
currently lose_sum: 590.5974945425987
time_elpased: 1.8
start validation test
0.574917127072
0.563821061266
0.919625398786
0.699053430337
0
validation finish
batch start
#iterations: 340
currently lose_sum: 592.3440483808517
time_elpased: 1.834
batch start
#iterations: 341
currently lose_sum: 591.5998841524124
time_elpased: 1.839
batch start
#iterations: 342
currently lose_sum: 592.7880620360374
time_elpased: 1.796
batch start
#iterations: 343
currently lose_sum: 591.5244305729866
time_elpased: 1.825
batch start
#iterations: 344
currently lose_sum: 592.989805996418
time_elpased: 1.836
batch start
#iterations: 345
currently lose_sum: 590.7533764839172
time_elpased: 1.825
batch start
#iterations: 346
currently lose_sum: 592.7369812130928
time_elpased: 1.802
batch start
#iterations: 347
currently lose_sum: 591.0126312375069
time_elpased: 1.796
batch start
#iterations: 348
currently lose_sum: 591.0931532979012
time_elpased: 1.804
batch start
#iterations: 349
currently lose_sum: 590.5350247621536
time_elpased: 1.802
batch start
#iterations: 350
currently lose_sum: 589.84068608284
time_elpased: 1.796
batch start
#iterations: 351
currently lose_sum: 592.4198831915855
time_elpased: 1.811
batch start
#iterations: 352
currently lose_sum: 591.6813318729401
time_elpased: 1.834
batch start
#iterations: 353
currently lose_sum: 589.6132587194443
time_elpased: 1.885
batch start
#iterations: 354
currently lose_sum: 591.0850841403008
time_elpased: 1.832
batch start
#iterations: 355
currently lose_sum: 590.1246203780174
time_elpased: 1.786
batch start
#iterations: 356
currently lose_sum: 592.8341792821884
time_elpased: 1.843
batch start
#iterations: 357
currently lose_sum: 592.4964584112167
time_elpased: 1.803
batch start
#iterations: 358
currently lose_sum: 593.0320091247559
time_elpased: 1.805
batch start
#iterations: 359
currently lose_sum: 591.4335585832596
time_elpased: 1.815
start validation test
0.575414364641
0.563787041688
0.924153545333
0.700331448625
0
validation finish
batch start
#iterations: 360
currently lose_sum: 588.0088153481483
time_elpased: 1.857
batch start
#iterations: 361
currently lose_sum: 589.5333516597748
time_elpased: 1.831
batch start
#iterations: 362
currently lose_sum: 591.703282892704
time_elpased: 1.814
batch start
#iterations: 363
currently lose_sum: 591.1116071939468
time_elpased: 1.815
batch start
#iterations: 364
currently lose_sum: 592.4085181355476
time_elpased: 1.85
batch start
#iterations: 365
currently lose_sum: 590.5766911506653
time_elpased: 1.814
batch start
#iterations: 366
currently lose_sum: 590.995375752449
time_elpased: 1.812
batch start
#iterations: 367
currently lose_sum: 590.1333517432213
time_elpased: 1.839
batch start
#iterations: 368
currently lose_sum: 591.269099354744
time_elpased: 1.846
batch start
#iterations: 369
currently lose_sum: 591.1796093583107
time_elpased: 1.834
batch start
#iterations: 370
currently lose_sum: 591.8499006032944
time_elpased: 1.818
batch start
#iterations: 371
currently lose_sum: 590.9149407148361
time_elpased: 1.82
batch start
#iterations: 372
currently lose_sum: 591.0649617314339
time_elpased: 1.9
batch start
#iterations: 373
currently lose_sum: 591.8393375873566
time_elpased: 1.94
batch start
#iterations: 374
currently lose_sum: 588.662669479847
time_elpased: 1.837
batch start
#iterations: 375
currently lose_sum: 593.2013485431671
time_elpased: 1.802
batch start
#iterations: 376
currently lose_sum: 591.216136932373
time_elpased: 1.852
batch start
#iterations: 377
currently lose_sum: 592.0575072169304
time_elpased: 1.858
batch start
#iterations: 378
currently lose_sum: 592.3336757421494
time_elpased: 1.872
batch start
#iterations: 379
currently lose_sum: 591.2458983063698
time_elpased: 1.866
start validation test
0.572016574586
0.561189951247
0.929916640939
0.699963204679
0
validation finish
batch start
#iterations: 380
currently lose_sum: 591.4954389333725
time_elpased: 1.901
batch start
#iterations: 381
currently lose_sum: 591.3955491781235
time_elpased: 1.855
batch start
#iterations: 382
currently lose_sum: 592.586320579052
time_elpased: 1.853
batch start
#iterations: 383
currently lose_sum: 590.0979555845261
time_elpased: 1.823
batch start
#iterations: 384
currently lose_sum: 591.8000529408455
time_elpased: 1.919
batch start
#iterations: 385
currently lose_sum: 591.8162381649017
time_elpased: 1.901
batch start
#iterations: 386
currently lose_sum: 591.5441284775734
time_elpased: 1.912
batch start
#iterations: 387
currently lose_sum: 592.9253442287445
time_elpased: 1.842
batch start
#iterations: 388
currently lose_sum: 591.7189759016037
time_elpased: 1.893
batch start
#iterations: 389
currently lose_sum: 591.741469681263
time_elpased: 1.844
batch start
#iterations: 390
currently lose_sum: 590.4782293438911
time_elpased: 1.855
batch start
#iterations: 391
currently lose_sum: 590.8318826556206
time_elpased: 1.871
batch start
#iterations: 392
currently lose_sum: 592.080229640007
time_elpased: 1.907
batch start
#iterations: 393
currently lose_sum: 592.2947311997414
time_elpased: 1.959
batch start
#iterations: 394
currently lose_sum: 591.5576411485672
time_elpased: 1.943
batch start
#iterations: 395
currently lose_sum: 590.8210936188698
time_elpased: 1.894
batch start
#iterations: 396
currently lose_sum: 592.531845510006
time_elpased: 1.889
batch start
#iterations: 397
currently lose_sum: 590.7252667546272
time_elpased: 1.887
batch start
#iterations: 398
currently lose_sum: 591.42490285635
time_elpased: 1.845
batch start
#iterations: 399
currently lose_sum: 590.8749096989632
time_elpased: 1.841
start validation test
0.572044198895
0.560582774943
0.938458371925
0.701893472906
0
validation finish
acc: 0.572
pre: 0.561
rec: 0.934
F1: 0.701
auc: 0.000
