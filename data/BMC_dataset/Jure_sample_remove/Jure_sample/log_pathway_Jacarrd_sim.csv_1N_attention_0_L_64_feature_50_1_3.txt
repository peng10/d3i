start to construct graph
graph construct over
93281
epochs start
batch start
#iterations: 0
currently lose_sum: 614.9711726307869
time_elpased: 1.831
batch start
#iterations: 1
currently lose_sum: 611.2254155278206
time_elpased: 1.799
batch start
#iterations: 2
currently lose_sum: 607.0239061713219
time_elpased: 1.827
batch start
#iterations: 3
currently lose_sum: 606.5699099898338
time_elpased: 1.797
batch start
#iterations: 4
currently lose_sum: 605.0192702412605
time_elpased: 1.812
batch start
#iterations: 5
currently lose_sum: 602.642201423645
time_elpased: 1.811
batch start
#iterations: 6
currently lose_sum: 600.6674291491508
time_elpased: 1.819
batch start
#iterations: 7
currently lose_sum: 602.0181868672371
time_elpased: 1.787
batch start
#iterations: 8
currently lose_sum: 600.8268656730652
time_elpased: 1.809
batch start
#iterations: 9
currently lose_sum: 601.2093599438667
time_elpased: 1.823
batch start
#iterations: 10
currently lose_sum: 602.036807179451
time_elpased: 1.802
batch start
#iterations: 11
currently lose_sum: 598.2382062077522
time_elpased: 1.823
batch start
#iterations: 12
currently lose_sum: 597.406431555748
time_elpased: 1.8
batch start
#iterations: 13
currently lose_sum: 598.0084470510483
time_elpased: 1.873
batch start
#iterations: 14
currently lose_sum: 599.9584749341011
time_elpased: 1.812
batch start
#iterations: 15
currently lose_sum: 597.1104126572609
time_elpased: 1.82
batch start
#iterations: 16
currently lose_sum: 598.6449304223061
time_elpased: 1.787
batch start
#iterations: 17
currently lose_sum: 596.6009672880173
time_elpased: 1.822
batch start
#iterations: 18
currently lose_sum: 596.4006315469742
time_elpased: 1.808
batch start
#iterations: 19
currently lose_sum: 597.6163374781609
time_elpased: 1.793
start validation test
0.565706371191
0.558858576356
0.917146974063
0.694516971279
0
validation finish
batch start
#iterations: 20
currently lose_sum: 595.8491832017899
time_elpased: 1.8
batch start
#iterations: 21
currently lose_sum: 597.1834218502045
time_elpased: 1.898
batch start
#iterations: 22
currently lose_sum: 597.9021248221397
time_elpased: 1.868
batch start
#iterations: 23
currently lose_sum: 597.4927796721458
time_elpased: 1.796
batch start
#iterations: 24
currently lose_sum: 596.9916132688522
time_elpased: 1.783
batch start
#iterations: 25
currently lose_sum: 597.7100678682327
time_elpased: 1.791
batch start
#iterations: 26
currently lose_sum: 596.6581418514252
time_elpased: 1.809
batch start
#iterations: 27
currently lose_sum: 594.7452741265297
time_elpased: 1.801
batch start
#iterations: 28
currently lose_sum: 596.7928266525269
time_elpased: 1.784
batch start
#iterations: 29
currently lose_sum: 598.9529238343239
time_elpased: 1.802
batch start
#iterations: 30
currently lose_sum: 596.5339068770409
time_elpased: 1.803
batch start
#iterations: 31
currently lose_sum: 596.9965476989746
time_elpased: 1.786
batch start
#iterations: 32
currently lose_sum: 595.8089752793312
time_elpased: 1.799
batch start
#iterations: 33
currently lose_sum: 595.7721504569054
time_elpased: 1.792
batch start
#iterations: 34
currently lose_sum: 595.9606673717499
time_elpased: 1.79
batch start
#iterations: 35
currently lose_sum: 595.4669997096062
time_elpased: 1.803
batch start
#iterations: 36
currently lose_sum: 598.3444529175758
time_elpased: 1.786
batch start
#iterations: 37
currently lose_sum: 595.3625771403313
time_elpased: 1.797
batch start
#iterations: 38
currently lose_sum: 597.4912750720978
time_elpased: 1.78
batch start
#iterations: 39
currently lose_sum: 596.5909808278084
time_elpased: 1.79
start validation test
0.570304709141
0.562420382166
0.908810209963
0.694837897387
0
validation finish
batch start
#iterations: 40
currently lose_sum: 597.5522821545601
time_elpased: 1.799
batch start
#iterations: 41
currently lose_sum: 595.6200684905052
time_elpased: 1.826
batch start
#iterations: 42
currently lose_sum: 597.4181396365166
time_elpased: 1.806
batch start
#iterations: 43
currently lose_sum: 595.2485015392303
time_elpased: 1.819
batch start
#iterations: 44
currently lose_sum: 596.421714246273
time_elpased: 1.796
batch start
#iterations: 45
currently lose_sum: 596.4769759774208
time_elpased: 1.784
batch start
#iterations: 46
currently lose_sum: 593.2254508137703
time_elpased: 1.779
batch start
#iterations: 47
currently lose_sum: 596.8235570192337
time_elpased: 1.788
batch start
#iterations: 48
currently lose_sum: 595.8833354711533
time_elpased: 1.772
batch start
#iterations: 49
currently lose_sum: 596.2293249368668
time_elpased: 1.779
batch start
#iterations: 50
currently lose_sum: 596.6448327898979
time_elpased: 1.799
batch start
#iterations: 51
currently lose_sum: 594.8015438318253
time_elpased: 1.792
batch start
#iterations: 52
currently lose_sum: 595.82915610075
time_elpased: 1.817
batch start
#iterations: 53
currently lose_sum: 595.7083591222763
time_elpased: 1.865
batch start
#iterations: 54
currently lose_sum: 596.4659516215324
time_elpased: 1.906
batch start
#iterations: 55
currently lose_sum: 598.5348232388496
time_elpased: 1.846
batch start
#iterations: 56
currently lose_sum: 594.5153556466103
time_elpased: 1.806
batch start
#iterations: 57
currently lose_sum: 594.7758226394653
time_elpased: 1.823
batch start
#iterations: 58
currently lose_sum: 597.2542418837547
time_elpased: 1.814
batch start
#iterations: 59
currently lose_sum: 595.0145632624626
time_elpased: 1.813
start validation test
0.569861495845
0.56050086784
0.930629888843
0.699628597957
0
validation finish
batch start
#iterations: 60
currently lose_sum: 595.4590504169464
time_elpased: 1.801
batch start
#iterations: 61
currently lose_sum: 594.9113364815712
time_elpased: 1.796
batch start
#iterations: 62
currently lose_sum: 595.2151932120323
time_elpased: 1.825
batch start
#iterations: 63
currently lose_sum: 594.5593931674957
time_elpased: 1.795
batch start
#iterations: 64
currently lose_sum: 596.1408671736717
time_elpased: 1.822
batch start
#iterations: 65
currently lose_sum: 596.276950776577
time_elpased: 1.801
batch start
#iterations: 66
currently lose_sum: 594.029391169548
time_elpased: 1.805
batch start
#iterations: 67
currently lose_sum: 593.9604935050011
time_elpased: 1.792
batch start
#iterations: 68
currently lose_sum: 597.7517286539078
time_elpased: 1.826
batch start
#iterations: 69
currently lose_sum: 595.0009840130806
time_elpased: 1.822
batch start
#iterations: 70
currently lose_sum: 594.4629571437836
time_elpased: 1.839
batch start
#iterations: 71
currently lose_sum: 595.4805117249489
time_elpased: 1.788
batch start
#iterations: 72
currently lose_sum: 595.7600588798523
time_elpased: 1.811
batch start
#iterations: 73
currently lose_sum: 594.8309155106544
time_elpased: 1.817
batch start
#iterations: 74
currently lose_sum: 594.9956830739975
time_elpased: 1.843
batch start
#iterations: 75
currently lose_sum: 596.1863191723824
time_elpased: 1.799
batch start
#iterations: 76
currently lose_sum: 595.2711874246597
time_elpased: 1.82
batch start
#iterations: 77
currently lose_sum: 593.8576741814613
time_elpased: 1.813
batch start
#iterations: 78
currently lose_sum: 595.0415802598
time_elpased: 1.849
batch start
#iterations: 79
currently lose_sum: 593.5063103437424
time_elpased: 1.792
start validation test
0.567894736842
0.559832662109
0.922807739811
0.696889027068
0
validation finish
batch start
#iterations: 80
currently lose_sum: 594.4971206188202
time_elpased: 1.85
batch start
#iterations: 81
currently lose_sum: 595.6962788105011
time_elpased: 1.829
batch start
#iterations: 82
currently lose_sum: 595.9580602049828
time_elpased: 1.804
batch start
#iterations: 83
currently lose_sum: 596.2649266719818
time_elpased: 1.843
batch start
#iterations: 84
currently lose_sum: 595.1541312336922
time_elpased: 1.836
batch start
#iterations: 85
currently lose_sum: 594.9298951625824
time_elpased: 1.818
batch start
#iterations: 86
currently lose_sum: 594.9735599756241
time_elpased: 1.805
batch start
#iterations: 87
currently lose_sum: 595.6518647670746
time_elpased: 1.795
batch start
#iterations: 88
currently lose_sum: 594.236274600029
time_elpased: 1.774
batch start
#iterations: 89
currently lose_sum: 594.5016117691994
time_elpased: 1.811
batch start
#iterations: 90
currently lose_sum: 595.5456267595291
time_elpased: 1.798
batch start
#iterations: 91
currently lose_sum: 595.5563305020332
time_elpased: 1.822
batch start
#iterations: 92
currently lose_sum: 593.1702441573143
time_elpased: 1.811
batch start
#iterations: 93
currently lose_sum: 594.9373697638512
time_elpased: 1.856
batch start
#iterations: 94
currently lose_sum: 596.0284335017204
time_elpased: 1.823
batch start
#iterations: 95
currently lose_sum: 593.6612539291382
time_elpased: 1.813
batch start
#iterations: 96
currently lose_sum: 592.0855178833008
time_elpased: 1.807
batch start
#iterations: 97
currently lose_sum: 596.1445589661598
time_elpased: 1.837
batch start
#iterations: 98
currently lose_sum: 594.508865237236
time_elpased: 1.811
batch start
#iterations: 99
currently lose_sum: 595.9079648256302
time_elpased: 1.82
start validation test
0.565013850416
0.556066756879
0.951626183615
0.701956839448
0
validation finish
batch start
#iterations: 100
currently lose_sum: 595.9057215452194
time_elpased: 1.823
batch start
#iterations: 101
currently lose_sum: 592.7330033183098
time_elpased: 1.82
batch start
#iterations: 102
currently lose_sum: 593.99777084589
time_elpased: 1.818
batch start
#iterations: 103
currently lose_sum: 595.7907336354256
time_elpased: 1.803
batch start
#iterations: 104
currently lose_sum: 595.7085423469543
time_elpased: 1.81
batch start
#iterations: 105
currently lose_sum: 593.8624441623688
time_elpased: 1.854
batch start
#iterations: 106
currently lose_sum: 593.4703480005264
time_elpased: 1.869
batch start
#iterations: 107
currently lose_sum: 595.151730298996
time_elpased: 1.815
batch start
#iterations: 108
currently lose_sum: 595.879481613636
time_elpased: 1.812
batch start
#iterations: 109
currently lose_sum: 593.7072856426239
time_elpased: 1.81
batch start
#iterations: 110
currently lose_sum: 594.4540794789791
time_elpased: 1.819
batch start
#iterations: 111
currently lose_sum: 593.3554409742355
time_elpased: 1.793
batch start
#iterations: 112
currently lose_sum: 593.6410405039787
time_elpased: 1.816
batch start
#iterations: 113
currently lose_sum: 594.2827361822128
time_elpased: 1.801
batch start
#iterations: 114
currently lose_sum: 594.3059348464012
time_elpased: 1.812
batch start
#iterations: 115
currently lose_sum: 593.8191623687744
time_elpased: 1.767
batch start
#iterations: 116
currently lose_sum: 593.3735689520836
time_elpased: 1.831
batch start
#iterations: 117
currently lose_sum: 593.8110162615776
time_elpased: 1.834
batch start
#iterations: 118
currently lose_sum: 593.9112247824669
time_elpased: 1.802
batch start
#iterations: 119
currently lose_sum: 594.590293943882
time_elpased: 1.789
start validation test
0.565457063712
0.556648010891
0.94689172499
0.701125993103
0
validation finish
batch start
#iterations: 120
currently lose_sum: 595.0932785868645
time_elpased: 1.791
batch start
#iterations: 121
currently lose_sum: 593.5348209738731
time_elpased: 1.804
batch start
#iterations: 122
currently lose_sum: 594.7379177212715
time_elpased: 1.789
batch start
#iterations: 123
currently lose_sum: 594.8378785252571
time_elpased: 1.793
batch start
#iterations: 124
currently lose_sum: 593.0143709778786
time_elpased: 1.787
batch start
#iterations: 125
currently lose_sum: 593.9550071358681
time_elpased: 1.79
batch start
#iterations: 126
currently lose_sum: 593.4745535850525
time_elpased: 1.784
batch start
#iterations: 127
currently lose_sum: 594.2318437695503
time_elpased: 1.79
batch start
#iterations: 128
currently lose_sum: 594.812483727932
time_elpased: 1.788
batch start
#iterations: 129
currently lose_sum: 593.2984521985054
time_elpased: 1.808
batch start
#iterations: 130
currently lose_sum: 592.3479029536247
time_elpased: 1.809
batch start
#iterations: 131
currently lose_sum: 594.4793938398361
time_elpased: 1.792
batch start
#iterations: 132
currently lose_sum: 594.6487902998924
time_elpased: 1.801
batch start
#iterations: 133
currently lose_sum: 595.7537585496902
time_elpased: 1.797
batch start
#iterations: 134
currently lose_sum: 595.7723669409752
time_elpased: 1.794
batch start
#iterations: 135
currently lose_sum: 593.1607576012611
time_elpased: 1.821
batch start
#iterations: 136
currently lose_sum: 594.9431704878807
time_elpased: 1.796
batch start
#iterations: 137
currently lose_sum: 593.797469317913
time_elpased: 1.787
batch start
#iterations: 138
currently lose_sum: 593.3399977087975
time_elpased: 1.81
batch start
#iterations: 139
currently lose_sum: 594.9297962784767
time_elpased: 1.804
start validation test
0.569667590028
0.561321793863
0.917867435159
0.696623508505
0
validation finish
batch start
#iterations: 140
currently lose_sum: 595.8123237490654
time_elpased: 1.812
batch start
#iterations: 141
currently lose_sum: 593.9435866475105
time_elpased: 1.795
batch start
#iterations: 142
currently lose_sum: 594.3620367646217
time_elpased: 1.797
batch start
#iterations: 143
currently lose_sum: 593.9317733645439
time_elpased: 1.785
batch start
#iterations: 144
currently lose_sum: 593.9829800128937
time_elpased: 1.803
batch start
#iterations: 145
currently lose_sum: 593.1185834407806
time_elpased: 1.793
batch start
#iterations: 146
currently lose_sum: 594.9033085107803
time_elpased: 1.81
batch start
#iterations: 147
currently lose_sum: 594.6706237792969
time_elpased: 1.822
batch start
#iterations: 148
currently lose_sum: 594.0771392583847
time_elpased: 1.789
batch start
#iterations: 149
currently lose_sum: 593.1305859684944
time_elpased: 1.792
batch start
#iterations: 150
currently lose_sum: 594.2958841919899
time_elpased: 1.784
batch start
#iterations: 151
currently lose_sum: 594.046239733696
time_elpased: 1.785
batch start
#iterations: 152
currently lose_sum: 595.1691401004791
time_elpased: 1.75
batch start
#iterations: 153
currently lose_sum: 594.988100707531
time_elpased: 1.77
batch start
#iterations: 154
currently lose_sum: 593.3161200284958
time_elpased: 1.795
batch start
#iterations: 155
currently lose_sum: 595.7328599095345
time_elpased: 1.78
batch start
#iterations: 156
currently lose_sum: 595.184862434864
time_elpased: 1.777
batch start
#iterations: 157
currently lose_sum: 594.3549118041992
time_elpased: 1.763
batch start
#iterations: 158
currently lose_sum: 595.043940782547
time_elpased: 1.798
batch start
#iterations: 159
currently lose_sum: 593.0532538890839
time_elpased: 1.786
start validation test
0.568975069252
0.561080262494
0.915191436805
0.695665780003
0
validation finish
batch start
#iterations: 160
currently lose_sum: 591.7003325819969
time_elpased: 1.811
batch start
#iterations: 161
currently lose_sum: 593.1592026948929
time_elpased: 1.788
batch start
#iterations: 162
currently lose_sum: 593.8884153962135
time_elpased: 1.812
batch start
#iterations: 163
currently lose_sum: 594.2822363376617
time_elpased: 1.781
batch start
#iterations: 164
currently lose_sum: 593.0504823923111
time_elpased: 1.777
batch start
#iterations: 165
currently lose_sum: 594.2132570147514
time_elpased: 1.794
batch start
#iterations: 166
currently lose_sum: 595.0832403898239
time_elpased: 1.807
batch start
#iterations: 167
currently lose_sum: 594.158729672432
time_elpased: 1.802
batch start
#iterations: 168
currently lose_sum: 594.1561454534531
time_elpased: 1.812
batch start
#iterations: 169
currently lose_sum: 593.7977195382118
time_elpased: 1.783
batch start
#iterations: 170
currently lose_sum: 592.5794214010239
time_elpased: 1.787
batch start
#iterations: 171
currently lose_sum: 594.5087322592735
time_elpased: 1.77
batch start
#iterations: 172
currently lose_sum: 594.3072570562363
time_elpased: 1.794
batch start
#iterations: 173
currently lose_sum: 595.4099196195602
time_elpased: 1.818
batch start
#iterations: 174
currently lose_sum: 594.4034008979797
time_elpased: 1.782
batch start
#iterations: 175
currently lose_sum: 594.5759501457214
time_elpased: 1.833
batch start
#iterations: 176
currently lose_sum: 593.8099475502968
time_elpased: 1.806
batch start
#iterations: 177
currently lose_sum: 596.7026360630989
time_elpased: 1.766
batch start
#iterations: 178
currently lose_sum: 594.8793365359306
time_elpased: 1.796
batch start
#iterations: 179
currently lose_sum: 595.1207200884819
time_elpased: 1.814
start validation test
0.570166204986
0.561108856492
0.924969123096
0.698494122219
0
validation finish
batch start
#iterations: 180
currently lose_sum: 592.6176652312279
time_elpased: 1.795
batch start
#iterations: 181
currently lose_sum: 594.3815851211548
time_elpased: 1.805
batch start
#iterations: 182
currently lose_sum: 594.0590941309929
time_elpased: 1.81
batch start
#iterations: 183
currently lose_sum: 594.4437728524208
time_elpased: 1.792
batch start
#iterations: 184
currently lose_sum: 595.0889954566956
time_elpased: 1.787
batch start
#iterations: 185
currently lose_sum: 593.6447522640228
time_elpased: 1.771
batch start
#iterations: 186
currently lose_sum: 593.6327220201492
time_elpased: 1.8
batch start
#iterations: 187
currently lose_sum: 593.7409507632256
time_elpased: 1.777
batch start
#iterations: 188
currently lose_sum: 593.3849499225616
time_elpased: 1.772
batch start
#iterations: 189
currently lose_sum: 593.8128287792206
time_elpased: 1.784
batch start
#iterations: 190
currently lose_sum: 595.728692471981
time_elpased: 1.79
batch start
#iterations: 191
currently lose_sum: 593.0567069649696
time_elpased: 1.827
batch start
#iterations: 192
currently lose_sum: 594.2388487458229
time_elpased: 1.824
batch start
#iterations: 193
currently lose_sum: 595.0006011128426
time_elpased: 1.797
batch start
#iterations: 194
currently lose_sum: 594.1460520625114
time_elpased: 1.787
batch start
#iterations: 195
currently lose_sum: 595.3119254112244
time_elpased: 1.821
batch start
#iterations: 196
currently lose_sum: 593.9235190749168
time_elpased: 1.785
batch start
#iterations: 197
currently lose_sum: 592.0962144732475
time_elpased: 1.78
batch start
#iterations: 198
currently lose_sum: 594.6214808225632
time_elpased: 1.814
batch start
#iterations: 199
currently lose_sum: 593.7804355621338
time_elpased: 1.814
start validation test
0.569667590028
0.560439219579
0.929806504734
0.699347796636
0
validation finish
batch start
#iterations: 200
currently lose_sum: 594.9602179527283
time_elpased: 1.815
batch start
#iterations: 201
currently lose_sum: 594.492523252964
time_elpased: 1.807
batch start
#iterations: 202
currently lose_sum: 594.0048909187317
time_elpased: 1.795
batch start
#iterations: 203
currently lose_sum: 594.4894812107086
time_elpased: 1.78
batch start
#iterations: 204
currently lose_sum: 594.7068527340889
time_elpased: 1.781
batch start
#iterations: 205
currently lose_sum: 593.8005314469337
time_elpased: 1.795
batch start
#iterations: 206
currently lose_sum: 591.7201091051102
time_elpased: 1.82
batch start
#iterations: 207
currently lose_sum: 592.6068150401115
time_elpased: 1.786
batch start
#iterations: 208
currently lose_sum: 596.1577815413475
time_elpased: 1.783
batch start
#iterations: 209
currently lose_sum: 594.1146879196167
time_elpased: 1.834
batch start
#iterations: 210
currently lose_sum: 592.3605345487595
time_elpased: 1.806
batch start
#iterations: 211
currently lose_sum: 592.6665120124817
time_elpased: 1.804
batch start
#iterations: 212
currently lose_sum: 591.5417022705078
time_elpased: 1.784
batch start
#iterations: 213
currently lose_sum: 594.9179525971413
time_elpased: 1.816
batch start
#iterations: 214
currently lose_sum: 594.8176180720329
time_elpased: 1.8
batch start
#iterations: 215
currently lose_sum: 596.2445411682129
time_elpased: 1.824
batch start
#iterations: 216
currently lose_sum: 593.930525302887
time_elpased: 1.81
batch start
#iterations: 217
currently lose_sum: 593.4555473923683
time_elpased: 1.898
batch start
#iterations: 218
currently lose_sum: 594.4953468441963
time_elpased: 1.929
batch start
#iterations: 219
currently lose_sum: 594.8301050662994
time_elpased: 1.841
start validation test
0.569418282548
0.55962458594
0.938966652944
0.701283726651
0
validation finish
batch start
#iterations: 220
currently lose_sum: 593.7921334505081
time_elpased: 1.85
batch start
#iterations: 221
currently lose_sum: 594.2683038115501
time_elpased: 1.825
batch start
#iterations: 222
currently lose_sum: 596.1542707681656
time_elpased: 1.879
batch start
#iterations: 223
currently lose_sum: 593.4822352528572
time_elpased: 1.927
batch start
#iterations: 224
currently lose_sum: 592.8440914154053
time_elpased: 1.844
batch start
#iterations: 225
currently lose_sum: 595.7562720179558
time_elpased: 1.808
batch start
#iterations: 226
currently lose_sum: 594.6707330942154
time_elpased: 1.83
batch start
#iterations: 227
currently lose_sum: 593.6995338201523
time_elpased: 1.789
batch start
#iterations: 228
currently lose_sum: 593.9875472784042
time_elpased: 1.819
batch start
#iterations: 229
currently lose_sum: 593.750663459301
time_elpased: 1.807
batch start
#iterations: 230
currently lose_sum: 593.8578245043755
time_elpased: 1.801
batch start
#iterations: 231
currently lose_sum: 594.3370751738548
time_elpased: 1.793
batch start
#iterations: 232
currently lose_sum: 594.419346511364
time_elpased: 1.804
batch start
#iterations: 233
currently lose_sum: 592.2624803185463
time_elpased: 1.768
batch start
#iterations: 234
currently lose_sum: 594.3482393622398
time_elpased: 1.798
batch start
#iterations: 235
currently lose_sum: 594.9475397467613
time_elpased: 1.822
batch start
#iterations: 236
currently lose_sum: 593.9107111692429
time_elpased: 1.827
batch start
#iterations: 237
currently lose_sum: 592.1125314235687
time_elpased: 1.793
batch start
#iterations: 238
currently lose_sum: 595.1892358660698
time_elpased: 1.826
batch start
#iterations: 239
currently lose_sum: 593.4532222151756
time_elpased: 1.81
start validation test
0.571412742382
0.562326869806
0.919308357349
0.6978125
0
validation finish
batch start
#iterations: 240
currently lose_sum: 592.3043682575226
time_elpased: 1.859
batch start
#iterations: 241
currently lose_sum: 594.1929343342781
time_elpased: 1.823
batch start
#iterations: 242
currently lose_sum: 594.0436470508575
time_elpased: 1.83
batch start
#iterations: 243
currently lose_sum: 595.0452213287354
time_elpased: 1.824
batch start
#iterations: 244
currently lose_sum: 594.0037305355072
time_elpased: 1.821
batch start
#iterations: 245
currently lose_sum: 595.5901560783386
time_elpased: 1.828
batch start
#iterations: 246
currently lose_sum: 593.3088493347168
time_elpased: 1.813
batch start
#iterations: 247
currently lose_sum: 594.9604170918465
time_elpased: 1.822
batch start
#iterations: 248
currently lose_sum: 593.694856107235
time_elpased: 1.84
batch start
#iterations: 249
currently lose_sum: 590.8598764538765
time_elpased: 1.827
batch start
#iterations: 250
currently lose_sum: 592.7004506587982
time_elpased: 1.794
batch start
#iterations: 251
currently lose_sum: 594.3198278546333
time_elpased: 1.842
batch start
#iterations: 252
currently lose_sum: 592.9240903258324
time_elpased: 1.818
batch start
#iterations: 253
currently lose_sum: 595.2603787779808
time_elpased: 1.833
batch start
#iterations: 254
currently lose_sum: 594.7848535180092
time_elpased: 1.83
batch start
#iterations: 255
currently lose_sum: 594.1497699022293
time_elpased: 1.814
batch start
#iterations: 256
currently lose_sum: 594.0813779830933
time_elpased: 1.816
batch start
#iterations: 257
currently lose_sum: 594.0223957300186
time_elpased: 1.819
batch start
#iterations: 258
currently lose_sum: 595.6455713510513
time_elpased: 1.814
batch start
#iterations: 259
currently lose_sum: 592.6771139502525
time_elpased: 1.779
start validation test
0.570360110803
0.56103330221
0.927645121449
0.699197083123
0
validation finish
batch start
#iterations: 260
currently lose_sum: 594.8974965214729
time_elpased: 1.832
batch start
#iterations: 261
currently lose_sum: 593.8574793934822
time_elpased: 1.795
batch start
#iterations: 262
currently lose_sum: 593.6556916236877
time_elpased: 1.779
batch start
#iterations: 263
currently lose_sum: 593.9056487679482
time_elpased: 1.787
batch start
#iterations: 264
currently lose_sum: 594.9160605669022
time_elpased: 1.812
batch start
#iterations: 265
currently lose_sum: 594.9268451333046
time_elpased: 1.843
batch start
#iterations: 266
currently lose_sum: 593.0238538384438
time_elpased: 1.805
batch start
#iterations: 267
currently lose_sum: 594.0350984930992
time_elpased: 1.846
batch start
#iterations: 268
currently lose_sum: 594.2724115848541
time_elpased: 1.836
batch start
#iterations: 269
currently lose_sum: 593.5448625683784
time_elpased: 1.829
batch start
#iterations: 270
currently lose_sum: 591.4231575131416
time_elpased: 1.822
batch start
#iterations: 271
currently lose_sum: 592.8854817152023
time_elpased: 1.855
batch start
#iterations: 272
currently lose_sum: 594.9200094342232
time_elpased: 1.807
batch start
#iterations: 273
currently lose_sum: 594.847475528717
time_elpased: 1.818
batch start
#iterations: 274
currently lose_sum: 594.6983903050423
time_elpased: 1.812
batch start
#iterations: 275
currently lose_sum: 593.0440884828568
time_elpased: 1.811
batch start
#iterations: 276
currently lose_sum: 594.1795616149902
time_elpased: 1.829
batch start
#iterations: 277
currently lose_sum: 593.5512412190437
time_elpased: 1.818
batch start
#iterations: 278
currently lose_sum: 594.4422315955162
time_elpased: 1.837
batch start
#iterations: 279
currently lose_sum: 594.4384562969208
time_elpased: 1.814
start validation test
0.569639889197
0.56045183714
0.92939481268
0.699241133653
0
validation finish
batch start
#iterations: 280
currently lose_sum: 594.363082408905
time_elpased: 1.83
batch start
#iterations: 281
currently lose_sum: 594.4680856466293
time_elpased: 1.822
batch start
#iterations: 282
currently lose_sum: 593.5488786697388
time_elpased: 1.862
batch start
#iterations: 283
currently lose_sum: 594.6028804183006
time_elpased: 1.836
batch start
#iterations: 284
currently lose_sum: 595.2697870135307
time_elpased: 1.839
batch start
#iterations: 285
currently lose_sum: 594.4993773698807
time_elpased: 1.834
batch start
#iterations: 286
currently lose_sum: 592.5714657902718
time_elpased: 1.843
batch start
#iterations: 287
currently lose_sum: 593.47473269701
time_elpased: 1.829
batch start
#iterations: 288
currently lose_sum: 593.2365030646324
time_elpased: 1.816
batch start
#iterations: 289
currently lose_sum: 594.3066072463989
time_elpased: 1.808
batch start
#iterations: 290
currently lose_sum: 593.6268429756165
time_elpased: 1.828
batch start
#iterations: 291
currently lose_sum: 593.3243051767349
time_elpased: 1.826
batch start
#iterations: 292
currently lose_sum: 593.3898134827614
time_elpased: 1.808
batch start
#iterations: 293
currently lose_sum: 595.1983577609062
time_elpased: 1.808
batch start
#iterations: 294
currently lose_sum: 593.7475938200951
time_elpased: 1.8
batch start
#iterations: 295
currently lose_sum: 593.6370847821236
time_elpased: 1.809
batch start
#iterations: 296
currently lose_sum: 595.194578230381
time_elpased: 1.82
batch start
#iterations: 297
currently lose_sum: 593.8350586295128
time_elpased: 1.833
batch start
#iterations: 298
currently lose_sum: 593.4614540338516
time_elpased: 1.821
batch start
#iterations: 299
currently lose_sum: 592.7882631421089
time_elpased: 1.821
start validation test
0.568725761773
0.559319431221
0.937216961713
0.700555844056
0
validation finish
batch start
#iterations: 300
currently lose_sum: 595.0928711295128
time_elpased: 1.815
batch start
#iterations: 301
currently lose_sum: 594.9161360263824
time_elpased: 1.823
batch start
#iterations: 302
currently lose_sum: 595.0282359719276
time_elpased: 1.795
batch start
#iterations: 303
currently lose_sum: 593.2464383840561
time_elpased: 1.821
batch start
#iterations: 304
currently lose_sum: 593.9753476381302
time_elpased: 1.816
batch start
#iterations: 305
currently lose_sum: 595.0188586115837
time_elpased: 1.819
batch start
#iterations: 306
currently lose_sum: 594.491526722908
time_elpased: 1.805
batch start
#iterations: 307
currently lose_sum: 592.8401046395302
time_elpased: 1.821
batch start
#iterations: 308
currently lose_sum: 595.1060107946396
time_elpased: 1.843
batch start
#iterations: 309
currently lose_sum: 594.5682116150856
time_elpased: 1.807
batch start
#iterations: 310
currently lose_sum: 594.8198735713959
time_elpased: 1.814
batch start
#iterations: 311
currently lose_sum: 593.4593450427055
time_elpased: 1.809
batch start
#iterations: 312
currently lose_sum: 593.1992294788361
time_elpased: 1.817
batch start
#iterations: 313
currently lose_sum: 593.5170763731003
time_elpased: 1.822
batch start
#iterations: 314
currently lose_sum: 595.9671833515167
time_elpased: 1.799
batch start
#iterations: 315
currently lose_sum: 594.3095812797546
time_elpased: 1.826
batch start
#iterations: 316
currently lose_sum: 593.3066627383232
time_elpased: 1.824
batch start
#iterations: 317
currently lose_sum: 594.0358625650406
time_elpased: 1.818
batch start
#iterations: 318
currently lose_sum: 595.0003285706043
time_elpased: 1.817
batch start
#iterations: 319
currently lose_sum: 594.1600385308266
time_elpased: 1.842
start validation test
0.57027700831
0.561087383483
0.926204199259
0.698829330797
0
validation finish
batch start
#iterations: 320
currently lose_sum: 592.9345607161522
time_elpased: 1.838
batch start
#iterations: 321
currently lose_sum: 591.8610995411873
time_elpased: 1.831
batch start
#iterations: 322
currently lose_sum: 592.3715883493423
time_elpased: 1.836
batch start
#iterations: 323
currently lose_sum: 592.7019455432892
time_elpased: 1.847
batch start
#iterations: 324
currently lose_sum: 594.3188987970352
time_elpased: 1.824
batch start
#iterations: 325
currently lose_sum: 593.311014354229
time_elpased: 1.863
batch start
#iterations: 326
currently lose_sum: 594.2136756181717
time_elpased: 1.836
batch start
#iterations: 327
currently lose_sum: 594.1519902944565
time_elpased: 1.813
batch start
#iterations: 328
currently lose_sum: 593.705991268158
time_elpased: 1.809
batch start
#iterations: 329
currently lose_sum: 593.7523750066757
time_elpased: 1.858
batch start
#iterations: 330
currently lose_sum: 594.4102228283882
time_elpased: 1.834
batch start
#iterations: 331
currently lose_sum: 591.9349752664566
time_elpased: 1.833
batch start
#iterations: 332
currently lose_sum: 593.8162659406662
time_elpased: 1.804
batch start
#iterations: 333
currently lose_sum: 594.1805721521378
time_elpased: 1.824
batch start
#iterations: 334
currently lose_sum: 593.3905426263809
time_elpased: 1.831
batch start
#iterations: 335
currently lose_sum: 594.2615073919296
time_elpased: 1.823
batch start
#iterations: 336
currently lose_sum: 592.6489456295967
time_elpased: 1.834
batch start
#iterations: 337
currently lose_sum: 594.9698130488396
time_elpased: 1.842
batch start
#iterations: 338
currently lose_sum: 594.3168743848801
time_elpased: 1.827
batch start
#iterations: 339
currently lose_sum: 592.7434626221657
time_elpased: 1.837
start validation test
0.56864265928
0.559421182266
0.935055578427
0.70003082139
0
validation finish
batch start
#iterations: 340
currently lose_sum: 594.312866806984
time_elpased: 1.836
batch start
#iterations: 341
currently lose_sum: 594.7286787629128
time_elpased: 1.859
batch start
#iterations: 342
currently lose_sum: 595.2176579833031
time_elpased: 1.828
batch start
#iterations: 343
currently lose_sum: 594.6546989679337
time_elpased: 1.814
batch start
#iterations: 344
currently lose_sum: 594.242090523243
time_elpased: 1.836
batch start
#iterations: 345
currently lose_sum: 594.0477247238159
time_elpased: 1.843
batch start
#iterations: 346
currently lose_sum: 594.4864474534988
time_elpased: 1.834
batch start
#iterations: 347
currently lose_sum: 593.1220026612282
time_elpased: 1.839
batch start
#iterations: 348
currently lose_sum: 593.7948975563049
time_elpased: 1.833
batch start
#iterations: 349
currently lose_sum: 593.9147780537605
time_elpased: 1.83
batch start
#iterations: 350
currently lose_sum: 593.3588677048683
time_elpased: 1.861
batch start
#iterations: 351
currently lose_sum: 593.7784538269043
time_elpased: 1.931
batch start
#iterations: 352
currently lose_sum: 594.0879573822021
time_elpased: 1.901
batch start
#iterations: 353
currently lose_sum: 594.5792585015297
time_elpased: 1.883
batch start
#iterations: 354
currently lose_sum: 593.0164625048637
time_elpased: 1.837
batch start
#iterations: 355
currently lose_sum: 593.8881930708885
time_elpased: 1.875
batch start
#iterations: 356
currently lose_sum: 594.7254831194878
time_elpased: 1.848
batch start
#iterations: 357
currently lose_sum: 593.5954620838165
time_elpased: 1.821
batch start
#iterations: 358
currently lose_sum: 591.6456773281097
time_elpased: 1.869
batch start
#iterations: 359
currently lose_sum: 594.8169783353806
time_elpased: 1.864
start validation test
0.570526315789
0.561145703611
0.927542198436
0.699255121043
0
validation finish
batch start
#iterations: 360
currently lose_sum: 593.0892018079758
time_elpased: 1.864
batch start
#iterations: 361
currently lose_sum: 593.258252799511
time_elpased: 1.838
batch start
#iterations: 362
currently lose_sum: 593.8791404366493
time_elpased: 1.843
batch start
#iterations: 363
currently lose_sum: 594.5831431150436
time_elpased: 1.876
batch start
#iterations: 364
currently lose_sum: 594.0158875584602
time_elpased: 1.869
batch start
#iterations: 365
currently lose_sum: 595.1765859127045
time_elpased: 1.885
batch start
#iterations: 366
currently lose_sum: 592.8064169883728
time_elpased: 1.869
batch start
#iterations: 367
currently lose_sum: 592.4547781944275
time_elpased: 1.913
batch start
#iterations: 368
currently lose_sum: 594.8562856316566
time_elpased: 1.868
batch start
#iterations: 369
currently lose_sum: 595.3278577327728
time_elpased: 1.876
batch start
#iterations: 370
currently lose_sum: 594.821330010891
time_elpased: 1.859
batch start
#iterations: 371
currently lose_sum: 593.8051152825356
time_elpased: 1.912
batch start
#iterations: 372
currently lose_sum: 594.8959358334541
time_elpased: 1.9
batch start
#iterations: 373
currently lose_sum: 592.7903637290001
time_elpased: 1.971
batch start
#iterations: 374
currently lose_sum: 594.1657181978226
time_elpased: 2.007
batch start
#iterations: 375
currently lose_sum: 594.2849860191345
time_elpased: 1.921
batch start
#iterations: 376
currently lose_sum: 593.6694569587708
time_elpased: 1.943
batch start
#iterations: 377
currently lose_sum: 594.0814737081528
time_elpased: 1.864
batch start
#iterations: 378
currently lose_sum: 592.9689043164253
time_elpased: 1.909
batch start
#iterations: 379
currently lose_sum: 593.7035595178604
time_elpased: 1.904
start validation test
0.57135734072
0.562174049639
0.920852202552
0.698138972338
0
validation finish
batch start
#iterations: 380
currently lose_sum: 594.0097423791885
time_elpased: 1.921
batch start
#iterations: 381
currently lose_sum: 593.1078053712845
time_elpased: 1.901
batch start
#iterations: 382
currently lose_sum: 592.8653249144554
time_elpased: 1.858
batch start
#iterations: 383
currently lose_sum: 595.0550710558891
time_elpased: 1.86
batch start
#iterations: 384
currently lose_sum: 594.2657620310783
time_elpased: 1.876
batch start
#iterations: 385
currently lose_sum: 591.6661030054092
time_elpased: 1.885
batch start
#iterations: 386
currently lose_sum: 594.4442778229713
time_elpased: 1.861
batch start
#iterations: 387
currently lose_sum: 593.2926666736603
time_elpased: 1.841
batch start
#iterations: 388
currently lose_sum: 593.9474863409996
time_elpased: 1.832
batch start
#iterations: 389
currently lose_sum: 592.5817182660103
time_elpased: 1.859
batch start
#iterations: 390
currently lose_sum: 593.6489083170891
time_elpased: 1.847
batch start
#iterations: 391
currently lose_sum: 594.7883453369141
time_elpased: 1.834
batch start
#iterations: 392
currently lose_sum: 593.1053085923195
time_elpased: 1.844
batch start
#iterations: 393
currently lose_sum: 592.3362590670586
time_elpased: 1.881
batch start
#iterations: 394
currently lose_sum: 593.957036793232
time_elpased: 1.848
batch start
#iterations: 395
currently lose_sum: 592.2835751771927
time_elpased: 1.833
batch start
#iterations: 396
currently lose_sum: 593.3385562300682
time_elpased: 1.82
batch start
#iterations: 397
currently lose_sum: 593.1192167401314
time_elpased: 1.852
batch start
#iterations: 398
currently lose_sum: 591.7570374011993
time_elpased: 1.835
batch start
#iterations: 399
currently lose_sum: 593.4832566380501
time_elpased: 1.86
start validation test
0.570941828255
0.561730907725
0.923219431865
0.698475734392
0
validation finish
acc: 0.570
pre: 0.558
rec: 0.951
F1: 0.703
auc: 0.000
