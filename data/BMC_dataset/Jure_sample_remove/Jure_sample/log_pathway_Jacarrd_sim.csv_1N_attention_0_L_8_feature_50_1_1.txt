start to construct graph
graph construct over
93119
epochs start
batch start
#iterations: 0
currently lose_sum: 612.1692423224449
time_elpased: 1.895
batch start
#iterations: 1
currently lose_sum: 609.3915433883667
time_elpased: 1.842
batch start
#iterations: 2
currently lose_sum: 604.1415306925774
time_elpased: 1.901
batch start
#iterations: 3
currently lose_sum: 605.2623615860939
time_elpased: 1.899
batch start
#iterations: 4
currently lose_sum: 604.2415796518326
time_elpased: 1.814
batch start
#iterations: 5
currently lose_sum: 602.3275842666626
time_elpased: 1.833
batch start
#iterations: 6
currently lose_sum: 601.400883436203
time_elpased: 1.826
batch start
#iterations: 7
currently lose_sum: 600.7253044843674
time_elpased: 1.874
batch start
#iterations: 8
currently lose_sum: 599.3807355761528
time_elpased: 1.841
batch start
#iterations: 9
currently lose_sum: 598.6591309905052
time_elpased: 1.897
batch start
#iterations: 10
currently lose_sum: 596.4606141448021
time_elpased: 1.847
batch start
#iterations: 11
currently lose_sum: 597.4625489115715
time_elpased: 1.842
batch start
#iterations: 12
currently lose_sum: 596.5303785204887
time_elpased: 1.839
batch start
#iterations: 13
currently lose_sum: 599.8644374012947
time_elpased: 1.829
batch start
#iterations: 14
currently lose_sum: 597.3594428896904
time_elpased: 1.819
batch start
#iterations: 15
currently lose_sum: 596.2661101222038
time_elpased: 1.834
batch start
#iterations: 16
currently lose_sum: 595.2181560397148
time_elpased: 1.83
batch start
#iterations: 17
currently lose_sum: 595.2783311009407
time_elpased: 1.829
batch start
#iterations: 18
currently lose_sum: 595.8871259093285
time_elpased: 1.849
batch start
#iterations: 19
currently lose_sum: 595.6664521694183
time_elpased: 1.83
start validation test
0.566011080332
0.557464075419
0.940207883091
0.699929133707
0
validation finish
batch start
#iterations: 20
currently lose_sum: 596.4276190400124
time_elpased: 1.819
batch start
#iterations: 21
currently lose_sum: 597.2254414558411
time_elpased: 1.815
batch start
#iterations: 22
currently lose_sum: 594.190581202507
time_elpased: 1.831
batch start
#iterations: 23
currently lose_sum: 595.2081011533737
time_elpased: 1.826
batch start
#iterations: 24
currently lose_sum: 596.2784775495529
time_elpased: 1.814
batch start
#iterations: 25
currently lose_sum: 597.1715792417526
time_elpased: 1.834
batch start
#iterations: 26
currently lose_sum: 594.8158794045448
time_elpased: 1.828
batch start
#iterations: 27
currently lose_sum: 593.8159885406494
time_elpased: 1.837
batch start
#iterations: 28
currently lose_sum: 595.1761410236359
time_elpased: 1.836
batch start
#iterations: 29
currently lose_sum: 594.8702593445778
time_elpased: 1.82
batch start
#iterations: 30
currently lose_sum: 595.2341555356979
time_elpased: 1.833
batch start
#iterations: 31
currently lose_sum: 593.6816839575768
time_elpased: 1.806
batch start
#iterations: 32
currently lose_sum: 594.4763556718826
time_elpased: 1.826
batch start
#iterations: 33
currently lose_sum: 594.4652140140533
time_elpased: 1.815
batch start
#iterations: 34
currently lose_sum: 595.4041061401367
time_elpased: 1.825
batch start
#iterations: 35
currently lose_sum: 594.2932081818581
time_elpased: 1.831
batch start
#iterations: 36
currently lose_sum: 593.8039551973343
time_elpased: 1.818
batch start
#iterations: 37
currently lose_sum: 595.536796271801
time_elpased: 1.844
batch start
#iterations: 38
currently lose_sum: 595.2600130438805
time_elpased: 1.848
batch start
#iterations: 39
currently lose_sum: 594.6181334257126
time_elpased: 1.852
start validation test
0.564764542936
0.555469448584
0.95893794381
0.703457647592
0
validation finish
batch start
#iterations: 40
currently lose_sum: 592.3787233829498
time_elpased: 1.862
batch start
#iterations: 41
currently lose_sum: 595.0724114775658
time_elpased: 1.814
batch start
#iterations: 42
currently lose_sum: 595.1105828881264
time_elpased: 1.829
batch start
#iterations: 43
currently lose_sum: 591.246105670929
time_elpased: 1.817
batch start
#iterations: 44
currently lose_sum: 595.1688796281815
time_elpased: 1.809
batch start
#iterations: 45
currently lose_sum: 592.7731959223747
time_elpased: 1.823
batch start
#iterations: 46
currently lose_sum: 595.1959851980209
time_elpased: 1.806
batch start
#iterations: 47
currently lose_sum: 593.9501373767853
time_elpased: 1.816
batch start
#iterations: 48
currently lose_sum: 594.0461598038673
time_elpased: 1.808
batch start
#iterations: 49
currently lose_sum: 593.3172388076782
time_elpased: 1.834
batch start
#iterations: 50
currently lose_sum: 594.8433002829552
time_elpased: 1.814
batch start
#iterations: 51
currently lose_sum: 593.1755938529968
time_elpased: 1.824
batch start
#iterations: 52
currently lose_sum: 594.9637678265572
time_elpased: 1.844
batch start
#iterations: 53
currently lose_sum: 594.3317057490349
time_elpased: 1.803
batch start
#iterations: 54
currently lose_sum: 595.3140051960945
time_elpased: 1.817
batch start
#iterations: 55
currently lose_sum: 594.7401010394096
time_elpased: 1.817
batch start
#iterations: 56
currently lose_sum: 593.80055975914
time_elpased: 1.811
batch start
#iterations: 57
currently lose_sum: 592.9505172371864
time_elpased: 1.83
batch start
#iterations: 58
currently lose_sum: 591.8303571939468
time_elpased: 1.808
batch start
#iterations: 59
currently lose_sum: 595.5050479769707
time_elpased: 1.809
start validation test
0.571329639889
0.56121470761
0.933827312957
0.701087481409
0
validation finish
batch start
#iterations: 60
currently lose_sum: 591.9452311992645
time_elpased: 1.862
batch start
#iterations: 61
currently lose_sum: 592.4934899806976
time_elpased: 1.791
batch start
#iterations: 62
currently lose_sum: 595.040562748909
time_elpased: 1.807
batch start
#iterations: 63
currently lose_sum: 594.6684963107109
time_elpased: 1.808
batch start
#iterations: 64
currently lose_sum: 593.3072546720505
time_elpased: 1.806
batch start
#iterations: 65
currently lose_sum: 592.2684658169746
time_elpased: 1.825
batch start
#iterations: 66
currently lose_sum: 592.8182562589645
time_elpased: 1.813
batch start
#iterations: 67
currently lose_sum: 592.1957548856735
time_elpased: 1.826
batch start
#iterations: 68
currently lose_sum: 593.0282864570618
time_elpased: 1.844
batch start
#iterations: 69
currently lose_sum: 593.9057413935661
time_elpased: 1.83
batch start
#iterations: 70
currently lose_sum: 590.6923357844353
time_elpased: 1.828
batch start
#iterations: 71
currently lose_sum: 593.2630327939987
time_elpased: 1.839
batch start
#iterations: 72
currently lose_sum: 591.474240899086
time_elpased: 1.839
batch start
#iterations: 73
currently lose_sum: 595.1405608057976
time_elpased: 1.819
batch start
#iterations: 74
currently lose_sum: 593.3906065225601
time_elpased: 1.834
batch start
#iterations: 75
currently lose_sum: 592.8214368224144
time_elpased: 1.807
batch start
#iterations: 76
currently lose_sum: 594.7847828865051
time_elpased: 1.827
batch start
#iterations: 77
currently lose_sum: 593.2965562939644
time_elpased: 1.8
batch start
#iterations: 78
currently lose_sum: 594.2049334645271
time_elpased: 1.791
batch start
#iterations: 79
currently lose_sum: 594.7106538414955
time_elpased: 1.807
start validation test
0.575263157895
0.564240734359
0.926726355871
0.701419586003
0
validation finish
batch start
#iterations: 80
currently lose_sum: 593.8528104424477
time_elpased: 1.851
batch start
#iterations: 81
currently lose_sum: 593.379962682724
time_elpased: 1.813
batch start
#iterations: 82
currently lose_sum: 594.7710680365562
time_elpased: 1.806
batch start
#iterations: 83
currently lose_sum: 594.3893418908119
time_elpased: 1.838
batch start
#iterations: 84
currently lose_sum: 592.7541265487671
time_elpased: 1.812
batch start
#iterations: 85
currently lose_sum: 591.8815034031868
time_elpased: 1.829
batch start
#iterations: 86
currently lose_sum: 591.9090427160263
time_elpased: 1.824
batch start
#iterations: 87
currently lose_sum: 594.2893082499504
time_elpased: 1.815
batch start
#iterations: 88
currently lose_sum: 591.656857252121
time_elpased: 1.829
batch start
#iterations: 89
currently lose_sum: 592.6144614815712
time_elpased: 1.837
batch start
#iterations: 90
currently lose_sum: 591.5308123230934
time_elpased: 1.825
batch start
#iterations: 91
currently lose_sum: 593.1816573739052
time_elpased: 1.843
batch start
#iterations: 92
currently lose_sum: 594.7579992413521
time_elpased: 1.85
batch start
#iterations: 93
currently lose_sum: 591.8121474981308
time_elpased: 1.856
batch start
#iterations: 94
currently lose_sum: 593.7991034388542
time_elpased: 1.834
batch start
#iterations: 95
currently lose_sum: 592.3925103545189
time_elpased: 1.813
batch start
#iterations: 96
currently lose_sum: 594.0623109936714
time_elpased: 1.806
batch start
#iterations: 97
currently lose_sum: 592.5071615576744
time_elpased: 1.787
batch start
#iterations: 98
currently lose_sum: 593.5731506347656
time_elpased: 1.919
batch start
#iterations: 99
currently lose_sum: 591.7909902334213
time_elpased: 1.957
start validation test
0.575983379501
0.5647901033
0.925594319234
0.701519021898
0
validation finish
batch start
#iterations: 100
currently lose_sum: 593.9381691813469
time_elpased: 1.905
batch start
#iterations: 101
currently lose_sum: 592.5928033590317
time_elpased: 1.927
batch start
#iterations: 102
currently lose_sum: 592.8980298638344
time_elpased: 1.845
batch start
#iterations: 103
currently lose_sum: 592.4058135151863
time_elpased: 1.841
batch start
#iterations: 104
currently lose_sum: 593.6372665166855
time_elpased: 1.794
batch start
#iterations: 105
currently lose_sum: 592.633039534092
time_elpased: 1.842
batch start
#iterations: 106
currently lose_sum: 592.7758221030235
time_elpased: 1.821
batch start
#iterations: 107
currently lose_sum: 593.1310637593269
time_elpased: 1.829
batch start
#iterations: 108
currently lose_sum: 593.7474852204323
time_elpased: 1.836
batch start
#iterations: 109
currently lose_sum: 593.2846456170082
time_elpased: 1.815
batch start
#iterations: 110
currently lose_sum: 593.2093178033829
time_elpased: 1.819
batch start
#iterations: 111
currently lose_sum: 594.5557025074959
time_elpased: 1.838
batch start
#iterations: 112
currently lose_sum: 594.1392060518265
time_elpased: 1.874
batch start
#iterations: 113
currently lose_sum: 593.7463131546974
time_elpased: 1.832
batch start
#iterations: 114
currently lose_sum: 592.5158847570419
time_elpased: 1.834
batch start
#iterations: 115
currently lose_sum: 592.1009069681168
time_elpased: 1.829
batch start
#iterations: 116
currently lose_sum: 593.0383440256119
time_elpased: 1.825
batch start
#iterations: 117
currently lose_sum: 592.3196757435799
time_elpased: 1.841
batch start
#iterations: 118
currently lose_sum: 593.9709722995758
time_elpased: 1.823
batch start
#iterations: 119
currently lose_sum: 595.2910635471344
time_elpased: 1.804
start validation test
0.57675900277
0.565246066392
0.926108881342
0.702018527548
0
validation finish
batch start
#iterations: 120
currently lose_sum: 594.9467712640762
time_elpased: 1.851
batch start
#iterations: 121
currently lose_sum: 592.5557672977448
time_elpased: 1.848
batch start
#iterations: 122
currently lose_sum: 592.7534153461456
time_elpased: 1.795
batch start
#iterations: 123
currently lose_sum: 593.449548959732
time_elpased: 1.826
batch start
#iterations: 124
currently lose_sum: 591.524739086628
time_elpased: 1.852
batch start
#iterations: 125
currently lose_sum: 595.0173828601837
time_elpased: 1.88
batch start
#iterations: 126
currently lose_sum: 593.4425987005234
time_elpased: 1.874
batch start
#iterations: 127
currently lose_sum: 593.4621402025223
time_elpased: 1.871
batch start
#iterations: 128
currently lose_sum: 590.4153942465782
time_elpased: 1.865
batch start
#iterations: 129
currently lose_sum: 592.599917948246
time_elpased: 1.835
batch start
#iterations: 130
currently lose_sum: 592.3197475671768
time_elpased: 1.83
batch start
#iterations: 131
currently lose_sum: 591.8753252625465
time_elpased: 1.847
batch start
#iterations: 132
currently lose_sum: 592.3551723957062
time_elpased: 1.829
batch start
#iterations: 133
currently lose_sum: 592.9832889437675
time_elpased: 1.816
batch start
#iterations: 134
currently lose_sum: 592.1365746855736
time_elpased: 1.828
batch start
#iterations: 135
currently lose_sum: 592.370467364788
time_elpased: 1.842
batch start
#iterations: 136
currently lose_sum: 593.8996619582176
time_elpased: 1.829
batch start
#iterations: 137
currently lose_sum: 592.8574122786522
time_elpased: 1.816
batch start
#iterations: 138
currently lose_sum: 592.0351259708405
time_elpased: 1.89
batch start
#iterations: 139
currently lose_sum: 593.0525807142258
time_elpased: 1.855
start validation test
0.572382271468
0.561388419598
0.940413707935
0.703071803651
0
validation finish
batch start
#iterations: 140
currently lose_sum: 591.1563267111778
time_elpased: 1.85
batch start
#iterations: 141
currently lose_sum: 593.7267837524414
time_elpased: 1.9
batch start
#iterations: 142
currently lose_sum: 592.7189623117447
time_elpased: 1.887
batch start
#iterations: 143
currently lose_sum: 592.4034968018532
time_elpased: 1.899
batch start
#iterations: 144
currently lose_sum: 593.6214987635612
time_elpased: 1.872
batch start
#iterations: 145
currently lose_sum: 592.6939334273338
time_elpased: 1.873
batch start
#iterations: 146
currently lose_sum: 593.6459900140762
time_elpased: 1.91
batch start
#iterations: 147
currently lose_sum: 593.1541523337364
time_elpased: 1.903
batch start
#iterations: 148
currently lose_sum: 593.0857332348824
time_elpased: 1.948
batch start
#iterations: 149
currently lose_sum: 592.374498128891
time_elpased: 1.866
batch start
#iterations: 150
currently lose_sum: 591.6469922661781
time_elpased: 1.922
batch start
#iterations: 151
currently lose_sum: 592.5387122631073
time_elpased: 1.893
batch start
#iterations: 152
currently lose_sum: 593.8129601478577
time_elpased: 1.912
batch start
#iterations: 153
currently lose_sum: 592.4334387779236
time_elpased: 1.842
batch start
#iterations: 154
currently lose_sum: 593.3947427272797
time_elpased: 1.896
batch start
#iterations: 155
currently lose_sum: 591.9985647797585
time_elpased: 1.893
batch start
#iterations: 156
currently lose_sum: 592.0561119914055
time_elpased: 1.935
batch start
#iterations: 157
currently lose_sum: 591.5825707316399
time_elpased: 1.881
batch start
#iterations: 158
currently lose_sum: 592.2304946184158
time_elpased: 1.851
batch start
#iterations: 159
currently lose_sum: 592.8683766126633
time_elpased: 1.863
start validation test
0.574293628809
0.562851666358
0.936811773181
0.703205870993
0
validation finish
batch start
#iterations: 160
currently lose_sum: 592.621787250042
time_elpased: 1.847
batch start
#iterations: 161
currently lose_sum: 592.6352006196976
time_elpased: 1.853
batch start
#iterations: 162
currently lose_sum: 591.861753821373
time_elpased: 1.848
batch start
#iterations: 163
currently lose_sum: 593.4494736790657
time_elpased: 1.858
batch start
#iterations: 164
currently lose_sum: 591.9572757482529
time_elpased: 1.879
batch start
#iterations: 165
currently lose_sum: 592.941248178482
time_elpased: 1.843
batch start
#iterations: 166
currently lose_sum: 593.3038320541382
time_elpased: 1.867
batch start
#iterations: 167
currently lose_sum: 592.8183243870735
time_elpased: 1.854
batch start
#iterations: 168
currently lose_sum: 592.5653917789459
time_elpased: 1.879
batch start
#iterations: 169
currently lose_sum: 592.3309202194214
time_elpased: 1.871
batch start
#iterations: 170
currently lose_sum: 592.3869295120239
time_elpased: 1.836
batch start
#iterations: 171
currently lose_sum: 591.6429052948952
time_elpased: 1.855
batch start
#iterations: 172
currently lose_sum: 592.1325578093529
time_elpased: 1.871
batch start
#iterations: 173
currently lose_sum: 592.8441472649574
time_elpased: 1.839
batch start
#iterations: 174
currently lose_sum: 592.4083121418953
time_elpased: 1.862
batch start
#iterations: 175
currently lose_sum: 592.6033269762993
time_elpased: 1.843
batch start
#iterations: 176
currently lose_sum: 593.124873995781
time_elpased: 1.841
batch start
#iterations: 177
currently lose_sum: 592.6639876365662
time_elpased: 1.835
batch start
#iterations: 178
currently lose_sum: 592.7694967389107
time_elpased: 1.894
batch start
#iterations: 179
currently lose_sum: 591.0461240410805
time_elpased: 1.841
start validation test
0.570083102493
0.559371397367
0.948749614078
0.703794182762
0
validation finish
batch start
#iterations: 180
currently lose_sum: 592.6150211691856
time_elpased: 1.866
batch start
#iterations: 181
currently lose_sum: 592.1554545760155
time_elpased: 1.859
batch start
#iterations: 182
currently lose_sum: 592.8333615660667
time_elpased: 1.875
batch start
#iterations: 183
currently lose_sum: 591.7839825749397
time_elpased: 1.863
batch start
#iterations: 184
currently lose_sum: 593.7305498123169
time_elpased: 1.888
batch start
#iterations: 185
currently lose_sum: 592.9882329702377
time_elpased: 1.893
batch start
#iterations: 186
currently lose_sum: 592.581650018692
time_elpased: 1.914
batch start
#iterations: 187
currently lose_sum: 592.2227120995522
time_elpased: 1.853
batch start
#iterations: 188
currently lose_sum: 592.1493328213692
time_elpased: 1.848
batch start
#iterations: 189
currently lose_sum: 593.8033779263496
time_elpased: 1.924
batch start
#iterations: 190
currently lose_sum: 592.6683112382889
time_elpased: 1.907
batch start
#iterations: 191
currently lose_sum: 591.1615584492683
time_elpased: 1.921
batch start
#iterations: 192
currently lose_sum: 594.2172315716743
time_elpased: 1.913
batch start
#iterations: 193
currently lose_sum: 591.5544253587723
time_elpased: 1.919
batch start
#iterations: 194
currently lose_sum: 592.3497748970985
time_elpased: 1.916
batch start
#iterations: 195
currently lose_sum: 592.4901791214943
time_elpased: 1.917
batch start
#iterations: 196
currently lose_sum: 593.3618020415306
time_elpased: 1.857
batch start
#iterations: 197
currently lose_sum: 592.2789906263351
time_elpased: 1.956
batch start
#iterations: 198
currently lose_sum: 590.6738621592522
time_elpased: 1.873
batch start
#iterations: 199
currently lose_sum: 593.4750649333
time_elpased: 1.903
start validation test
0.573988919668
0.562877965576
0.933930225378
0.702412972387
0
validation finish
batch start
#iterations: 200
currently lose_sum: 591.7562729716301
time_elpased: 1.919
batch start
#iterations: 201
currently lose_sum: 593.0393121242523
time_elpased: 1.874
batch start
#iterations: 202
currently lose_sum: 591.4365735650063
time_elpased: 1.886
batch start
#iterations: 203
currently lose_sum: 593.1687109470367
time_elpased: 1.852
batch start
#iterations: 204
currently lose_sum: 591.809409558773
time_elpased: 1.898
batch start
#iterations: 205
currently lose_sum: 592.1643272042274
time_elpased: 1.871
batch start
#iterations: 206
currently lose_sum: 591.4903013110161
time_elpased: 1.847
batch start
#iterations: 207
currently lose_sum: 592.6055426597595
time_elpased: 1.862
batch start
#iterations: 208
currently lose_sum: 593.8650683760643
time_elpased: 1.949
batch start
#iterations: 209
currently lose_sum: 591.159376680851
time_elpased: 1.947
batch start
#iterations: 210
currently lose_sum: 591.3536807894707
time_elpased: 1.927
batch start
#iterations: 211
currently lose_sum: 593.0094622373581
time_elpased: 1.939
batch start
#iterations: 212
currently lose_sum: 592.1874228715897
time_elpased: 1.9
batch start
#iterations: 213
currently lose_sum: 591.6949743032455
time_elpased: 1.864
batch start
#iterations: 214
currently lose_sum: 591.7065042257309
time_elpased: 1.903
batch start
#iterations: 215
currently lose_sum: 591.612258374691
time_elpased: 1.953
batch start
#iterations: 216
currently lose_sum: 593.8989037275314
time_elpased: 1.872
batch start
#iterations: 217
currently lose_sum: 591.5061965584755
time_elpased: 1.915
batch start
#iterations: 218
currently lose_sum: 593.5737136006355
time_elpased: 1.944
batch start
#iterations: 219
currently lose_sum: 591.8218804001808
time_elpased: 1.962
start validation test
0.575512465374
0.563713027842
0.935576824123
0.703528865501
0
validation finish
batch start
#iterations: 220
currently lose_sum: 592.207687497139
time_elpased: 1.92
batch start
#iterations: 221
currently lose_sum: 592.7015931010246
time_elpased: 1.926
batch start
#iterations: 222
currently lose_sum: 592.4821643233299
time_elpased: 1.937
batch start
#iterations: 223
currently lose_sum: 594.4690237045288
time_elpased: 1.9
batch start
#iterations: 224
currently lose_sum: 591.752371609211
time_elpased: 1.956
batch start
#iterations: 225
currently lose_sum: 593.1101729273796
time_elpased: 1.976
batch start
#iterations: 226
currently lose_sum: 593.1071575284004
time_elpased: 1.924
batch start
#iterations: 227
currently lose_sum: 592.5410630702972
time_elpased: 1.933
batch start
#iterations: 228
currently lose_sum: 593.9942296147346
time_elpased: 1.939
batch start
#iterations: 229
currently lose_sum: 593.4984000325203
time_elpased: 1.898
batch start
#iterations: 230
currently lose_sum: 594.111297249794
time_elpased: 1.944
batch start
#iterations: 231
currently lose_sum: 591.0567815899849
time_elpased: 1.957
batch start
#iterations: 232
currently lose_sum: 592.995759665966
time_elpased: 1.911
batch start
#iterations: 233
currently lose_sum: 592.9244781136513
time_elpased: 1.898
batch start
#iterations: 234
currently lose_sum: 591.815370619297
time_elpased: 1.944
batch start
#iterations: 235
currently lose_sum: 591.8848553299904
time_elpased: 1.941
batch start
#iterations: 236
currently lose_sum: 594.9160272479057
time_elpased: 1.866
batch start
#iterations: 237
currently lose_sum: 592.3164093494415
time_elpased: 1.875
batch start
#iterations: 238
currently lose_sum: 591.7621888518333
time_elpased: 1.927
batch start
#iterations: 239
currently lose_sum: 591.6178025603294
time_elpased: 1.887
start validation test
0.575457063712
0.564027431421
0.931048677575
0.702488643864
0
validation finish
batch start
#iterations: 240
currently lose_sum: 591.7838059663773
time_elpased: 1.988
batch start
#iterations: 241
currently lose_sum: 592.637442111969
time_elpased: 1.916
batch start
#iterations: 242
currently lose_sum: 591.5801312327385
time_elpased: 1.987
batch start
#iterations: 243
currently lose_sum: 590.9260504841805
time_elpased: 1.893
batch start
#iterations: 244
currently lose_sum: 592.0511466264725
time_elpased: 1.897
batch start
#iterations: 245
currently lose_sum: 593.7351121306419
time_elpased: 1.915
batch start
#iterations: 246
currently lose_sum: 591.9401791095734
time_elpased: 1.915
batch start
#iterations: 247
currently lose_sum: 591.7856028676033
time_elpased: 1.904
batch start
#iterations: 248
currently lose_sum: 591.6267659664154
time_elpased: 1.916
batch start
#iterations: 249
currently lose_sum: 593.8135360479355
time_elpased: 1.899
batch start
#iterations: 250
currently lose_sum: 591.1547263860703
time_elpased: 1.893
batch start
#iterations: 251
currently lose_sum: 592.0560652017593
time_elpased: 1.932
batch start
#iterations: 252
currently lose_sum: 592.9787843227386
time_elpased: 1.891
batch start
#iterations: 253
currently lose_sum: 592.2047346234322
time_elpased: 1.874
batch start
#iterations: 254
currently lose_sum: 593.1080161333084
time_elpased: 1.928
batch start
#iterations: 255
currently lose_sum: 589.5155513882637
time_elpased: 1.919
batch start
#iterations: 256
currently lose_sum: 593.1423667669296
time_elpased: 1.892
batch start
#iterations: 257
currently lose_sum: 592.8617450594902
time_elpased: 1.885
batch start
#iterations: 258
currently lose_sum: 590.8198757767677
time_elpased: 1.958
batch start
#iterations: 259
currently lose_sum: 591.7356582283974
time_elpased: 1.941
start validation test
0.57432132964
0.563150212726
0.933106926006
0.702391788516
0
validation finish
batch start
#iterations: 260
currently lose_sum: 593.1343952417374
time_elpased: 1.882
batch start
#iterations: 261
currently lose_sum: 593.5824840664864
time_elpased: 1.877
batch start
#iterations: 262
currently lose_sum: 591.8085152506828
time_elpased: 1.872
batch start
#iterations: 263
currently lose_sum: 591.66943192482
time_elpased: 1.944
batch start
#iterations: 264
currently lose_sum: 592.1080216765404
time_elpased: 1.954
batch start
#iterations: 265
currently lose_sum: 591.9073647260666
time_elpased: 1.95
batch start
#iterations: 266
currently lose_sum: 591.5782774686813
time_elpased: 1.912
batch start
#iterations: 267
currently lose_sum: 591.1870344877243
time_elpased: 1.885
batch start
#iterations: 268
currently lose_sum: 592.9643288850784
time_elpased: 1.885
batch start
#iterations: 269
currently lose_sum: 591.8766081929207
time_elpased: 1.907
batch start
#iterations: 270
currently lose_sum: 591.9637423753738
time_elpased: 1.862
batch start
#iterations: 271
currently lose_sum: 593.9948841929436
time_elpased: 1.881
batch start
#iterations: 272
currently lose_sum: 593.1531975269318
time_elpased: 1.865
batch start
#iterations: 273
currently lose_sum: 591.5748824477196
time_elpased: 1.89
batch start
#iterations: 274
currently lose_sum: 592.128158390522
time_elpased: 1.887
batch start
#iterations: 275
currently lose_sum: 592.8303865194321
time_elpased: 1.957
batch start
#iterations: 276
currently lose_sum: 593.069318652153
time_elpased: 1.881
batch start
#iterations: 277
currently lose_sum: 591.3395985364914
time_elpased: 1.91
batch start
#iterations: 278
currently lose_sum: 593.1761857271194
time_elpased: 1.949
batch start
#iterations: 279
currently lose_sum: 592.2710041999817
time_elpased: 1.898
start validation test
0.576260387812
0.564731653888
0.928578779459
0.702329292261
0
validation finish
batch start
#iterations: 280
currently lose_sum: 591.4626072645187
time_elpased: 1.901
batch start
#iterations: 281
currently lose_sum: 590.8576869368553
time_elpased: 1.934
batch start
#iterations: 282
currently lose_sum: 592.6539242863655
time_elpased: 1.892
batch start
#iterations: 283
currently lose_sum: 593.060064136982
time_elpased: 1.945
batch start
#iterations: 284
currently lose_sum: 592.8266462683678
time_elpased: 1.922
batch start
#iterations: 285
currently lose_sum: 592.2921699881554
time_elpased: 1.919
batch start
#iterations: 286
currently lose_sum: 592.5438138246536
time_elpased: 1.897
batch start
#iterations: 287
currently lose_sum: 592.9105315208435
time_elpased: 1.876
batch start
#iterations: 288
currently lose_sum: 591.0400124192238
time_elpased: 1.894
batch start
#iterations: 289
currently lose_sum: 591.926867544651
time_elpased: 1.908
batch start
#iterations: 290
currently lose_sum: 593.8770084381104
time_elpased: 1.851
batch start
#iterations: 291
currently lose_sum: 591.5896770358086
time_elpased: 1.904
batch start
#iterations: 292
currently lose_sum: 592.4182294607162
time_elpased: 1.87
batch start
#iterations: 293
currently lose_sum: 592.4821670055389
time_elpased: 1.853
batch start
#iterations: 294
currently lose_sum: 593.1519345641136
time_elpased: 1.853
batch start
#iterations: 295
currently lose_sum: 591.4742684960365
time_elpased: 1.84
batch start
#iterations: 296
currently lose_sum: 591.155918776989
time_elpased: 1.893
batch start
#iterations: 297
currently lose_sum: 592.3261224031448
time_elpased: 1.894
batch start
#iterations: 298
currently lose_sum: 591.4825750589371
time_elpased: 1.871
batch start
#iterations: 299
currently lose_sum: 591.1767185330391
time_elpased: 1.839
start validation test
0.574542936288
0.562946028608
0.937635072553
0.70351137965
0
validation finish
batch start
#iterations: 300
currently lose_sum: 592.8221451044083
time_elpased: 1.848
batch start
#iterations: 301
currently lose_sum: 589.767651617527
time_elpased: 1.825
batch start
#iterations: 302
currently lose_sum: 592.1230830550194
time_elpased: 1.902
batch start
#iterations: 303
currently lose_sum: 592.0456708073616
time_elpased: 1.864
batch start
#iterations: 304
currently lose_sum: 590.3685311079025
time_elpased: 1.854
batch start
#iterations: 305
currently lose_sum: 592.4693989753723
time_elpased: 1.855
batch start
#iterations: 306
currently lose_sum: 593.4475356936455
time_elpased: 1.844
batch start
#iterations: 307
currently lose_sum: 592.9962598085403
time_elpased: 1.831
batch start
#iterations: 308
currently lose_sum: 591.8419305086136
time_elpased: 1.899
batch start
#iterations: 309
currently lose_sum: 592.3584986329079
time_elpased: 1.843
batch start
#iterations: 310
currently lose_sum: 593.7140946388245
time_elpased: 1.87
batch start
#iterations: 311
currently lose_sum: 592.5902265906334
time_elpased: 1.843
batch start
#iterations: 312
currently lose_sum: 591.3075766563416
time_elpased: 1.863
batch start
#iterations: 313
currently lose_sum: 590.9228605628014
time_elpased: 1.89
batch start
#iterations: 314
currently lose_sum: 592.2147436141968
time_elpased: 1.894
batch start
#iterations: 315
currently lose_sum: 592.9501678347588
time_elpased: 1.845
batch start
#iterations: 316
currently lose_sum: 591.6078646183014
time_elpased: 1.897
batch start
#iterations: 317
currently lose_sum: 591.720921754837
time_elpased: 1.861
batch start
#iterations: 318
currently lose_sum: 591.0053450465202
time_elpased: 1.871
batch start
#iterations: 319
currently lose_sum: 591.2544044852257
time_elpased: 1.911
start validation test
0.575041551247
0.563755880245
0.931151589997
0.702307259426
0
validation finish
batch start
#iterations: 320
currently lose_sum: 592.8801780343056
time_elpased: 1.938
batch start
#iterations: 321
currently lose_sum: 592.2323866486549
time_elpased: 1.846
batch start
#iterations: 322
currently lose_sum: 591.7315655946732
time_elpased: 1.853
batch start
#iterations: 323
currently lose_sum: 592.9332221746445
time_elpased: 1.899
batch start
#iterations: 324
currently lose_sum: 591.3111268877983
time_elpased: 1.861
batch start
#iterations: 325
currently lose_sum: 592.815406024456
time_elpased: 1.895
batch start
#iterations: 326
currently lose_sum: 591.8199510574341
time_elpased: 1.853
batch start
#iterations: 327
currently lose_sum: 594.8636175394058
time_elpased: 1.943
batch start
#iterations: 328
currently lose_sum: 593.0484648942947
time_elpased: 1.899
batch start
#iterations: 329
currently lose_sum: 592.6514191031456
time_elpased: 1.874
batch start
#iterations: 330
currently lose_sum: 592.2185904383659
time_elpased: 1.879
batch start
#iterations: 331
currently lose_sum: 590.7105470299721
time_elpased: 1.841
batch start
#iterations: 332
currently lose_sum: 592.9261242747307
time_elpased: 1.924
batch start
#iterations: 333
currently lose_sum: 592.7024331092834
time_elpased: 1.929
batch start
#iterations: 334
currently lose_sum: 594.1164296269417
time_elpased: 1.973
batch start
#iterations: 335
currently lose_sum: 592.6518834233284
time_elpased: 1.981
batch start
#iterations: 336
currently lose_sum: 590.4538323283195
time_elpased: 1.917
batch start
#iterations: 337
currently lose_sum: 591.111544907093
time_elpased: 1.876
batch start
#iterations: 338
currently lose_sum: 592.8672471642494
time_elpased: 1.885
batch start
#iterations: 339
currently lose_sum: 590.7401958107948
time_elpased: 1.869
start validation test
0.573628808864
0.562411216108
0.937120510446
0.702948896094
0
validation finish
batch start
#iterations: 340
currently lose_sum: 592.1251884698868
time_elpased: 1.847
batch start
#iterations: 341
currently lose_sum: 591.6660806536674
time_elpased: 1.853
batch start
#iterations: 342
currently lose_sum: 592.7213807106018
time_elpased: 1.879
batch start
#iterations: 343
currently lose_sum: 592.533826649189
time_elpased: 1.914
batch start
#iterations: 344
currently lose_sum: 592.3898925185204
time_elpased: 1.985
batch start
#iterations: 345
currently lose_sum: 591.5759718418121
time_elpased: 1.966
batch start
#iterations: 346
currently lose_sum: 593.1184614300728
time_elpased: 1.951
batch start
#iterations: 347
currently lose_sum: 592.9381489753723
time_elpased: 1.91
batch start
#iterations: 348
currently lose_sum: 592.1919917464256
time_elpased: 1.883
batch start
#iterations: 349
currently lose_sum: 592.3753851652145
time_elpased: 1.926
batch start
#iterations: 350
currently lose_sum: 592.7416546344757
time_elpased: 1.864
batch start
#iterations: 351
currently lose_sum: 591.9137449860573
time_elpased: 1.873
batch start
#iterations: 352
currently lose_sum: 592.3592230081558
time_elpased: 1.865
batch start
#iterations: 353
currently lose_sum: 595.0327755212784
time_elpased: 1.904
batch start
#iterations: 354
currently lose_sum: 591.9076280593872
time_elpased: 1.957
batch start
#iterations: 355
currently lose_sum: 590.9341688752174
time_elpased: 1.983
batch start
#iterations: 356
currently lose_sum: 593.6643802523613
time_elpased: 1.948
batch start
#iterations: 357
currently lose_sum: 591.6406415700912
time_elpased: 1.944
batch start
#iterations: 358
currently lose_sum: 591.9355438947678
time_elpased: 1.89
batch start
#iterations: 359
currently lose_sum: 592.5593099594116
time_elpased: 1.862
start validation test
0.577008310249
0.565331994477
0.927035093136
0.702350785544
0
validation finish
batch start
#iterations: 360
currently lose_sum: 593.7778763175011
time_elpased: 1.9
batch start
#iterations: 361
currently lose_sum: 591.0862444639206
time_elpased: 1.881
batch start
#iterations: 362
currently lose_sum: 592.778057038784
time_elpased: 1.843
batch start
#iterations: 363
currently lose_sum: 592.8325735330582
time_elpased: 1.865
batch start
#iterations: 364
currently lose_sum: 591.942821264267
time_elpased: 1.838
batch start
#iterations: 365
currently lose_sum: 591.7514671087265
time_elpased: 1.861
batch start
#iterations: 366
currently lose_sum: 592.7461467981339
time_elpased: 1.848
batch start
#iterations: 367
currently lose_sum: 592.6652457118034
time_elpased: 1.839
batch start
#iterations: 368
currently lose_sum: 592.2005054354668
time_elpased: 1.941
batch start
#iterations: 369
currently lose_sum: 591.4071972966194
time_elpased: 1.965
batch start
#iterations: 370
currently lose_sum: 591.2906496524811
time_elpased: 1.875
batch start
#iterations: 371
currently lose_sum: 592.5566288232803
time_elpased: 1.949
batch start
#iterations: 372
currently lose_sum: 591.6404755711555
time_elpased: 1.928
batch start
#iterations: 373
currently lose_sum: 591.7477373480797
time_elpased: 1.93
batch start
#iterations: 374
currently lose_sum: 591.2581954598427
time_elpased: 1.984
batch start
#iterations: 375
currently lose_sum: 593.0203809142113
time_elpased: 1.992
batch start
#iterations: 376
currently lose_sum: 591.4161533117294
time_elpased: 1.991
batch start
#iterations: 377
currently lose_sum: 593.5193251371384
time_elpased: 1.958
batch start
#iterations: 378
currently lose_sum: 590.9222419857979
time_elpased: 1.982
batch start
#iterations: 379
currently lose_sum: 591.3273708224297
time_elpased: 1.976
start validation test
0.575844875346
0.56445055976
0.928784604302
0.702170699448
0
validation finish
batch start
#iterations: 380
currently lose_sum: 593.1521843671799
time_elpased: 1.987
batch start
#iterations: 381
currently lose_sum: 593.3388839364052
time_elpased: 2.015
batch start
#iterations: 382
currently lose_sum: 593.5232776403427
time_elpased: 1.898
batch start
#iterations: 383
currently lose_sum: 591.9847766757011
time_elpased: 2.013
batch start
#iterations: 384
currently lose_sum: 593.553047478199
time_elpased: 1.968
batch start
#iterations: 385
currently lose_sum: 593.4402302503586
time_elpased: 1.935
batch start
#iterations: 386
currently lose_sum: 591.9174392223358
time_elpased: 1.877
batch start
#iterations: 387
currently lose_sum: 593.6918528676033
time_elpased: 1.847
batch start
#iterations: 388
currently lose_sum: 590.6403647065163
time_elpased: 1.842
batch start
#iterations: 389
currently lose_sum: 590.8396589756012
time_elpased: 1.851
batch start
#iterations: 390
currently lose_sum: 591.9496377706528
time_elpased: 1.882
batch start
#iterations: 391
currently lose_sum: 591.5252343416214
time_elpased: 1.916
batch start
#iterations: 392
currently lose_sum: 591.7871254086494
time_elpased: 1.943
batch start
#iterations: 393
currently lose_sum: 592.7759917974472
time_elpased: 1.954
batch start
#iterations: 394
currently lose_sum: 592.3331282138824
time_elpased: 1.895
batch start
#iterations: 395
currently lose_sum: 591.7374827861786
time_elpased: 1.903
batch start
#iterations: 396
currently lose_sum: 592.7806277275085
time_elpased: 1.891
batch start
#iterations: 397
currently lose_sum: 592.8589814305305
time_elpased: 1.992
batch start
#iterations: 398
currently lose_sum: 593.2563775777817
time_elpased: 1.963
batch start
#iterations: 399
currently lose_sum: 591.1466653943062
time_elpased: 1.958
start validation test
0.576897506925
0.565220117898
0.927549655243
0.702412032888
0
validation finish
acc: 0.567
pre: 0.557
rec: 0.950
F1: 0.702
auc: 0.000
