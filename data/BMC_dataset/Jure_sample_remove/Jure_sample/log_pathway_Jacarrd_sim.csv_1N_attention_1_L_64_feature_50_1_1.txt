start to construct graph
graph construct over
93177
epochs start
batch start
#iterations: 0
currently lose_sum: 615.2724409103394
time_elpased: 6.179
batch start
#iterations: 1
currently lose_sum: 606.2750985622406
time_elpased: 6.08
batch start
#iterations: 2
currently lose_sum: 603.930962562561
time_elpased: 6.329
batch start
#iterations: 3
currently lose_sum: 600.2505914568901
time_elpased: 6.108
batch start
#iterations: 4
currently lose_sum: 596.3022947907448
time_elpased: 6.163
batch start
#iterations: 5
currently lose_sum: 593.6398159265518
time_elpased: 6.328
batch start
#iterations: 6
currently lose_sum: 593.3659836053848
time_elpased: 6.229
batch start
#iterations: 7
currently lose_sum: 590.5538039207458
time_elpased: 6.117
batch start
#iterations: 8
currently lose_sum: 590.7861298918724
time_elpased: 5.957
batch start
#iterations: 9
currently lose_sum: 588.0048201680183
time_elpased: 6.002
batch start
#iterations: 10
currently lose_sum: 588.7217213511467
time_elpased: 5.905
batch start
#iterations: 11
currently lose_sum: 588.2474531531334
time_elpased: 5.896
batch start
#iterations: 12
currently lose_sum: 588.0134574770927
time_elpased: 6.206
batch start
#iterations: 13
currently lose_sum: 586.0718957781792
time_elpased: 6.147
batch start
#iterations: 14
currently lose_sum: 584.4325041770935
time_elpased: 6.016
batch start
#iterations: 15
currently lose_sum: 585.2785531878471
time_elpased: 5.943
batch start
#iterations: 16
currently lose_sum: 584.7599346041679
time_elpased: 6.001
batch start
#iterations: 17
currently lose_sum: 586.3997456431389
time_elpased: 6.072
batch start
#iterations: 18
currently lose_sum: 584.4099243879318
time_elpased: 6.186
batch start
#iterations: 19
currently lose_sum: 584.1069033145905
time_elpased: 5.863
start validation test
0.572348066298
0.561829386555
0.924153545333
0.698819089123
0
validation finish
batch start
#iterations: 20
currently lose_sum: 581.975156545639
time_elpased: 6.098
batch start
#iterations: 21
currently lose_sum: 583.8477318882942
time_elpased: 6.062
batch start
#iterations: 22
currently lose_sum: 585.6555764079094
time_elpased: 6.154
batch start
#iterations: 23
currently lose_sum: 582.5710639953613
time_elpased: 6.026
batch start
#iterations: 24
currently lose_sum: 584.0531381964684
time_elpased: 6.241
batch start
#iterations: 25
currently lose_sum: 581.2905524373055
time_elpased: 6.24
batch start
#iterations: 26
currently lose_sum: 580.5897042751312
time_elpased: 5.895
batch start
#iterations: 27
currently lose_sum: 580.9297023415565
time_elpased: 5.918
batch start
#iterations: 28
currently lose_sum: 579.666805267334
time_elpased: 6.186
batch start
#iterations: 29
currently lose_sum: 582.3006797134876
time_elpased: 6.003
batch start
#iterations: 30
currently lose_sum: 580.7919611930847
time_elpased: 5.68
batch start
#iterations: 31
currently lose_sum: 582.3902678489685
time_elpased: 6.353
batch start
#iterations: 32
currently lose_sum: 580.8729255199432
time_elpased: 6.258
batch start
#iterations: 33
currently lose_sum: 579.0425766706467
time_elpased: 6.212
batch start
#iterations: 34
currently lose_sum: 578.0160474777222
time_elpased: 6.142
batch start
#iterations: 35
currently lose_sum: 578.8179452419281
time_elpased: 6.022
batch start
#iterations: 36
currently lose_sum: 577.5223131775856
time_elpased: 6.083
batch start
#iterations: 37
currently lose_sum: 580.3390545248985
time_elpased: 6.22
batch start
#iterations: 38
currently lose_sum: 579.1278531551361
time_elpased: 6.141
batch start
#iterations: 39
currently lose_sum: 577.386444658041
time_elpased: 5.957
start validation test
0.577679558011
0.569298656148
0.876299269322
0.690200210748
0
validation finish
batch start
#iterations: 40
currently lose_sum: 578.771518945694
time_elpased: 5.924
batch start
#iterations: 41
currently lose_sum: 579.2720419764519
time_elpased: 5.995
batch start
#iterations: 42
currently lose_sum: 577.9799809455872
time_elpased: 6.063
batch start
#iterations: 43
currently lose_sum: 580.76803201437
time_elpased: 6.222
batch start
#iterations: 44
currently lose_sum: 579.0369048118591
time_elpased: 6.12
batch start
#iterations: 45
currently lose_sum: 576.9819338917732
time_elpased: 6.276
batch start
#iterations: 46
currently lose_sum: 579.6124170422554
time_elpased: 6.179
batch start
#iterations: 47
currently lose_sum: 578.4843272268772
time_elpased: 6.259
batch start
#iterations: 48
currently lose_sum: 576.2540802359581
time_elpased: 6.01
batch start
#iterations: 49
currently lose_sum: 577.3682107329369
time_elpased: 6.074
batch start
#iterations: 50
currently lose_sum: 580.7757044434547
time_elpased: 6.137
batch start
#iterations: 51
currently lose_sum: 577.1546186804771
time_elpased: 6.263
batch start
#iterations: 52
currently lose_sum: 577.2875828146935
time_elpased: 6.203
batch start
#iterations: 53
currently lose_sum: 577.5812503099442
time_elpased: 6.106
batch start
#iterations: 54
currently lose_sum: 576.4064544439316
time_elpased: 6.105
batch start
#iterations: 55
currently lose_sum: 575.7748833298683
time_elpased: 6.038
batch start
#iterations: 56
currently lose_sum: 577.9291209578514
time_elpased: 6.139
batch start
#iterations: 57
currently lose_sum: 577.5477075576782
time_elpased: 6.163
batch start
#iterations: 58
currently lose_sum: 576.3212323784828
time_elpased: 6.105
batch start
#iterations: 59
currently lose_sum: 578.1619212031364
time_elpased: 6.049
start validation test
0.575828729282
0.566665577093
0.892044869816
0.693065744498
0
validation finish
batch start
#iterations: 60
currently lose_sum: 577.2125024199486
time_elpased: 5.983
batch start
#iterations: 61
currently lose_sum: 575.9311460256577
time_elpased: 6.262
batch start
#iterations: 62
currently lose_sum: 577.3148300051689
time_elpased: 6.075
batch start
#iterations: 63
currently lose_sum: 576.8108433485031
time_elpased: 6.174
batch start
#iterations: 64
currently lose_sum: 577.190332531929
time_elpased: 5.977
batch start
#iterations: 65
currently lose_sum: 574.8703368902206
time_elpased: 6.097
batch start
#iterations: 66
currently lose_sum: 575.9319630563259
time_elpased: 6.269
batch start
#iterations: 67
currently lose_sum: 575.7879807054996
time_elpased: 6.036
batch start
#iterations: 68
currently lose_sum: 575.070207297802
time_elpased: 6.195
batch start
#iterations: 69
currently lose_sum: 575.0728836655617
time_elpased: 6.161
batch start
#iterations: 70
currently lose_sum: 576.7084223330021
time_elpased: 5.978
batch start
#iterations: 71
currently lose_sum: 575.2980280518532
time_elpased: 5.917
batch start
#iterations: 72
currently lose_sum: 576.2377773523331
time_elpased: 6.116
batch start
#iterations: 73
currently lose_sum: 575.0504626631737
time_elpased: 6.258
batch start
#iterations: 74
currently lose_sum: 575.8602213263512
time_elpased: 6.002
batch start
#iterations: 75
currently lose_sum: 574.878868162632
time_elpased: 6.044
batch start
#iterations: 76
currently lose_sum: 575.4025756716728
time_elpased: 5.974
batch start
#iterations: 77
currently lose_sum: 577.0689460039139
time_elpased: 6.013
batch start
#iterations: 78
currently lose_sum: 576.2898325622082
time_elpased: 5.944
batch start
#iterations: 79
currently lose_sum: 574.7475958168507
time_elpased: 5.911
start validation test
0.578977900552
0.569588740996
0.882988576721
0.692479974173
0
validation finish
batch start
#iterations: 80
currently lose_sum: 573.6060093641281
time_elpased: 5.886
batch start
#iterations: 81
currently lose_sum: 573.3818389475346
time_elpased: 6.154
batch start
#iterations: 82
currently lose_sum: 573.8323647677898
time_elpased: 5.883
batch start
#iterations: 83
currently lose_sum: 573.180797368288
time_elpased: 6.069
batch start
#iterations: 84
currently lose_sum: 574.0922032594681
time_elpased: 6.097
batch start
#iterations: 85
currently lose_sum: 575.5782510638237
time_elpased: 6.251
batch start
#iterations: 86
currently lose_sum: 576.1524504721165
time_elpased: 6.087
batch start
#iterations: 87
currently lose_sum: 573.3448611199856
time_elpased: 6.029
batch start
#iterations: 88
currently lose_sum: 575.0151064991951
time_elpased: 5.996
batch start
#iterations: 89
currently lose_sum: 576.0778013467789
time_elpased: 6.221
batch start
#iterations: 90
currently lose_sum: 575.6097112298012
time_elpased: 6.08
batch start
#iterations: 91
currently lose_sum: 574.4153128862381
time_elpased: 5.909
batch start
#iterations: 92
currently lose_sum: 573.8795882165432
time_elpased: 5.905
batch start
#iterations: 93
currently lose_sum: 574.3232116103172
time_elpased: 6.189
batch start
#iterations: 94
currently lose_sum: 574.2774504423141
time_elpased: 6.136
batch start
#iterations: 95
currently lose_sum: 573.486074090004
time_elpased: 6.135
batch start
#iterations: 96
currently lose_sum: 574.1562205553055
time_elpased: 5.954
batch start
#iterations: 97
currently lose_sum: 573.7217324972153
time_elpased: 6.179
batch start
#iterations: 98
currently lose_sum: 574.0611552000046
time_elpased: 6.162
batch start
#iterations: 99
currently lose_sum: 574.2605597376823
time_elpased: 6.093
start validation test
0.574337016575
0.565013729607
0.899969126274
0.694199130763
0
validation finish
batch start
#iterations: 100
currently lose_sum: 573.8365471363068
time_elpased: 5.977
batch start
#iterations: 101
currently lose_sum: 574.0152516961098
time_elpased: 6.002
batch start
#iterations: 102
currently lose_sum: 575.5973928272724
time_elpased: 6.363
batch start
#iterations: 103
currently lose_sum: 572.6054911017418
time_elpased: 6.25
batch start
#iterations: 104
currently lose_sum: 573.5520755052567
time_elpased: 6.32
batch start
#iterations: 105
currently lose_sum: 573.0782023072243
time_elpased: 6.05
batch start
#iterations: 106
currently lose_sum: 571.7054399251938
time_elpased: 6.149
batch start
#iterations: 107
currently lose_sum: 574.134984433651
time_elpased: 5.947
batch start
#iterations: 108
currently lose_sum: 572.5000611245632
time_elpased: 5.825
batch start
#iterations: 109
currently lose_sum: 571.9662830531597
time_elpased: 6.067
batch start
#iterations: 110
currently lose_sum: 574.2177046537399
time_elpased: 5.907
batch start
#iterations: 111
currently lose_sum: 573.999977350235
time_elpased: 6.048
batch start
#iterations: 112
currently lose_sum: 571.0353989005089
time_elpased: 5.97
batch start
#iterations: 113
currently lose_sum: 574.6195444762707
time_elpased: 6.371
batch start
#iterations: 114
currently lose_sum: 574.6417699754238
time_elpased: 6.162
batch start
#iterations: 115
currently lose_sum: 573.723567545414
time_elpased: 6.026
batch start
#iterations: 116
currently lose_sum: 572.8245015442371
time_elpased: 5.962
batch start
#iterations: 117
currently lose_sum: 573.374139457941
time_elpased: 6.13
batch start
#iterations: 118
currently lose_sum: 572.0055654346943
time_elpased: 6.376
batch start
#iterations: 119
currently lose_sum: 574.031933516264
time_elpased: 6.243
start validation test
0.57729281768
0.567313958035
0.895955541834
0.694729282209
0
validation finish
batch start
#iterations: 120
currently lose_sum: 573.0442529320717
time_elpased: 6.202
batch start
#iterations: 121
currently lose_sum: 571.5962344408035
time_elpased: 6.255
batch start
#iterations: 122
currently lose_sum: 571.0724947154522
time_elpased: 5.893
batch start
#iterations: 123
currently lose_sum: 573.4978740811348
time_elpased: 6.232
batch start
#iterations: 124
currently lose_sum: 573.2277368307114
time_elpased: 6.252
batch start
#iterations: 125
currently lose_sum: 572.9300987124443
time_elpased: 6.308
batch start
#iterations: 126
currently lose_sum: 574.2547324299812
time_elpased: 5.934
batch start
#iterations: 127
currently lose_sum: 573.5153868794441
time_elpased: 5.952
batch start
#iterations: 128
currently lose_sum: 573.7833405137062
time_elpased: 6.193
batch start
#iterations: 129
currently lose_sum: 572.4880312085152
time_elpased: 6.081
batch start
#iterations: 130
currently lose_sum: 573.1735826134682
time_elpased: 5.829
batch start
#iterations: 131
currently lose_sum: 574.8045715689659
time_elpased: 6.107
batch start
#iterations: 132
currently lose_sum: 573.3148981332779
time_elpased: 6.357
batch start
#iterations: 133
currently lose_sum: 570.857922077179
time_elpased: 6.079
batch start
#iterations: 134
currently lose_sum: 572.2689400911331
time_elpased: 6.152
batch start
#iterations: 135
currently lose_sum: 571.3944702744484
time_elpased: 6.155
batch start
#iterations: 136
currently lose_sum: 574.4751504659653
time_elpased: 5.931
batch start
#iterations: 137
currently lose_sum: 573.3714745044708
time_elpased: 7.533
batch start
#iterations: 138
currently lose_sum: 573.2164312899113
time_elpased: 5.986
batch start
#iterations: 139
currently lose_sum: 573.9343922436237
time_elpased: 6.046
start validation test
0.578149171271
0.569159716915
0.881444890398
0.691688033757
0
validation finish
batch start
#iterations: 140
currently lose_sum: 573.3709814548492
time_elpased: 6.183
batch start
#iterations: 141
currently lose_sum: 572.0296877622604
time_elpased: 6.176
batch start
#iterations: 142
currently lose_sum: 573.0499136447906
time_elpased: 5.965
batch start
#iterations: 143
currently lose_sum: 572.1612531542778
time_elpased: 6.09
batch start
#iterations: 144
currently lose_sum: 573.3720624744892
time_elpased: 6.265
batch start
#iterations: 145
currently lose_sum: 573.836162686348
time_elpased: 5.962
batch start
#iterations: 146
currently lose_sum: 573.0520488619804
time_elpased: 6.033
batch start
#iterations: 147
currently lose_sum: 573.5220857262611
time_elpased: 6.081
batch start
#iterations: 148
currently lose_sum: 574.658757507801
time_elpased: 6.143
batch start
#iterations: 149
currently lose_sum: 573.1112134456635
time_elpased: 5.905
batch start
#iterations: 150
currently lose_sum: 573.2272862195969
time_elpased: 6.018
batch start
#iterations: 151
currently lose_sum: 574.7713829278946
time_elpased: 6.232
batch start
#iterations: 152
currently lose_sum: 572.8064040541649
time_elpased: 6.126
batch start
#iterations: 153
currently lose_sum: 572.0410047471523
time_elpased: 6.294
batch start
#iterations: 154
currently lose_sum: 571.2770093083382
time_elpased: 6.01
batch start
#iterations: 155
currently lose_sum: 572.437789618969
time_elpased: 6.165
batch start
#iterations: 156
currently lose_sum: 572.3122712373734
time_elpased: 5.959
batch start
#iterations: 157
currently lose_sum: 571.9286221563816
time_elpased: 6.18
batch start
#iterations: 158
currently lose_sum: 574.2080167233944
time_elpased: 5.772
batch start
#iterations: 159
currently lose_sum: 572.4263033568859
time_elpased: 5.887
start validation test
0.577790055249
0.567974841119
0.892147782237
0.694075260208
0
validation finish
batch start
#iterations: 160
currently lose_sum: 573.4548197984695
time_elpased: 6.199
batch start
#iterations: 161
currently lose_sum: 574.5284952521324
time_elpased: 6.094
batch start
#iterations: 162
currently lose_sum: 572.4243485033512
time_elpased: 5.824
batch start
#iterations: 163
currently lose_sum: 573.3655322790146
time_elpased: 6.146
batch start
#iterations: 164
currently lose_sum: 572.3672696948051
time_elpased: 6.159
batch start
#iterations: 165
currently lose_sum: 570.6797885298729
time_elpased: 6.223
batch start
#iterations: 166
currently lose_sum: 575.7193850278854
time_elpased: 6.244
batch start
#iterations: 167
currently lose_sum: 572.1091403961182
time_elpased: 6.241
batch start
#iterations: 168
currently lose_sum: 572.5017595887184
time_elpased: 5.876
batch start
#iterations: 169
currently lose_sum: 571.0726574063301
time_elpased: 6.318
batch start
#iterations: 170
currently lose_sum: 573.9584566354752
time_elpased: 6.025
batch start
#iterations: 171
currently lose_sum: 573.6006051898003
time_elpased: 6.184
batch start
#iterations: 172
currently lose_sum: 569.7751460373402
time_elpased: 6.126
batch start
#iterations: 173
currently lose_sum: 571.4624327123165
time_elpased: 6.183
batch start
#iterations: 174
currently lose_sum: 571.7807005941868
time_elpased: 6.055
batch start
#iterations: 175
currently lose_sum: 571.3590468764305
time_elpased: 6.198
batch start
#iterations: 176
currently lose_sum: 575.2286209762096
time_elpased: 6.194
batch start
#iterations: 177
currently lose_sum: 569.9487643837929
time_elpased: 6.103
batch start
#iterations: 178
currently lose_sum: 573.5410761237144
time_elpased: 6.042
batch start
#iterations: 179
currently lose_sum: 572.9833287000656
time_elpased: 6.02
start validation test
0.57770718232
0.568620313069
0.884120613358
0.692110934322
0
validation finish
batch start
#iterations: 180
currently lose_sum: 572.266962647438
time_elpased: 6.113
batch start
#iterations: 181
currently lose_sum: 574.5276343226433
time_elpased: 6.357
batch start
#iterations: 182
currently lose_sum: 571.9113581776619
time_elpased: 6.122
batch start
#iterations: 183
currently lose_sum: 572.460974574089
time_elpased: 5.984
batch start
#iterations: 184
currently lose_sum: 572.7658154964447
time_elpased: 5.873
batch start
#iterations: 185
currently lose_sum: 571.4379776120186
time_elpased: 6.316
batch start
#iterations: 186
currently lose_sum: 570.8404541313648
time_elpased: 6.039
batch start
#iterations: 187
currently lose_sum: 572.1587778031826
time_elpased: 6.152
batch start
#iterations: 188
currently lose_sum: 572.856082201004
time_elpased: 6.28
batch start
#iterations: 189
currently lose_sum: 573.2677161693573
time_elpased: 6.154
batch start
#iterations: 190
currently lose_sum: 570.0610563755035
time_elpased: 5.972
batch start
#iterations: 191
currently lose_sum: 572.9869954586029
time_elpased: 6.072
batch start
#iterations: 192
currently lose_sum: 571.5304697751999
time_elpased: 6.273
batch start
#iterations: 193
currently lose_sum: 570.7631689310074
time_elpased: 6.303
batch start
#iterations: 194
currently lose_sum: 570.031030446291
time_elpased: 6.004
batch start
#iterations: 195
currently lose_sum: 570.5314128994942
time_elpased: 5.93
batch start
#iterations: 196
currently lose_sum: 573.8673683702946
time_elpased: 6.033
batch start
#iterations: 197
currently lose_sum: 570.942745745182
time_elpased: 6.183
batch start
#iterations: 198
currently lose_sum: 571.2765048146248
time_elpased: 6.082
batch start
#iterations: 199
currently lose_sum: 572.0289002656937
time_elpased: 6.233
start validation test
0.576878453039
0.567361497431
0.892147782237
0.693617106395
0
validation finish
batch start
#iterations: 200
currently lose_sum: 570.1202834248543
time_elpased: 5.874
batch start
#iterations: 201
currently lose_sum: 570.1189874410629
time_elpased: 6.25
batch start
#iterations: 202
currently lose_sum: 570.5636442303658
time_elpased: 6.129
batch start
#iterations: 203
currently lose_sum: 571.3824235200882
time_elpased: 6.178
batch start
#iterations: 204
currently lose_sum: 572.0270193219185
time_elpased: 6.105
batch start
#iterations: 205
currently lose_sum: 572.1094668507576
time_elpased: 6.167
batch start
#iterations: 206
currently lose_sum: 571.3528353571892
time_elpased: 6.153
batch start
#iterations: 207
currently lose_sum: 571.1946089267731
time_elpased: 5.932
batch start
#iterations: 208
currently lose_sum: 569.8196767568588
time_elpased: 6.234
batch start
#iterations: 209
currently lose_sum: 570.6940574645996
time_elpased: 6.325
batch start
#iterations: 210
currently lose_sum: 572.9319952130318
time_elpased: 5.903
batch start
#iterations: 211
currently lose_sum: 569.8681200146675
time_elpased: 5.736
batch start
#iterations: 212
currently lose_sum: 572.4432645440102
time_elpased: 6.051
batch start
#iterations: 213
currently lose_sum: 570.7920015454292
time_elpased: 6.155
batch start
#iterations: 214
currently lose_sum: 570.0679529309273
time_elpased: 6.058
batch start
#iterations: 215
currently lose_sum: 572.1764004826546
time_elpased: 5.974
batch start
#iterations: 216
currently lose_sum: 570.6030066013336
time_elpased: 6.11
batch start
#iterations: 217
currently lose_sum: 569.7369573414326
time_elpased: 6.067
batch start
#iterations: 218
currently lose_sum: 571.0487805604935
time_elpased: 6.15
batch start
#iterations: 219
currently lose_sum: 572.5179737210274
time_elpased: 6.105
start validation test
0.577679558011
0.568141476563
0.889369146856
0.693356867779
0
validation finish
batch start
#iterations: 220
currently lose_sum: 571.0354360342026
time_elpased: 5.978
batch start
#iterations: 221
currently lose_sum: 570.7571684718132
time_elpased: 6.107
batch start
#iterations: 222
currently lose_sum: 569.4857084453106
time_elpased: 5.966
batch start
#iterations: 223
currently lose_sum: 571.6991548538208
time_elpased: 6.205
batch start
#iterations: 224
currently lose_sum: 571.7518829405308
time_elpased: 6.139
batch start
#iterations: 225
currently lose_sum: 571.8384209871292
time_elpased: 6.022
batch start
#iterations: 226
currently lose_sum: 572.3327466845512
time_elpased: 6.224
batch start
#iterations: 227
currently lose_sum: 570.0225570201874
time_elpased: 6.011
batch start
#iterations: 228
currently lose_sum: 568.199122518301
time_elpased: 6.081
batch start
#iterations: 229
currently lose_sum: 572.1746892333031
time_elpased: 5.992
batch start
#iterations: 230
currently lose_sum: 570.4265667796135
time_elpased: 6.114
batch start
#iterations: 231
currently lose_sum: 570.5027233660221
time_elpased: 6.15
batch start
#iterations: 232
currently lose_sum: 571.5759478509426
time_elpased: 6.168
batch start
#iterations: 233
currently lose_sum: 571.4493173956871
time_elpased: 6.193
batch start
#iterations: 234
currently lose_sum: 570.2461130321026
time_elpased: 5.978
batch start
#iterations: 235
currently lose_sum: 571.4532525539398
time_elpased: 6.032
batch start
#iterations: 236
currently lose_sum: 570.7655518054962
time_elpased: 6.056
batch start
#iterations: 237
currently lose_sum: 572.3075783848763
time_elpased: 6.168
batch start
#iterations: 238
currently lose_sum: 570.456752717495
time_elpased: 6.076
batch start
#iterations: 239
currently lose_sum: 573.2380591034889
time_elpased: 6.197
start validation test
0.577044198895
0.567980214345
0.88628177421
0.692296870918
0
validation finish
batch start
#iterations: 240
currently lose_sum: 570.8925260603428
time_elpased: 6.013
batch start
#iterations: 241
currently lose_sum: 571.7360665798187
time_elpased: 5.999
batch start
#iterations: 242
currently lose_sum: 572.0074473023415
time_elpased: 6.196
batch start
#iterations: 243
currently lose_sum: 570.499239295721
time_elpased: 6.222
batch start
#iterations: 244
currently lose_sum: 570.1421778798103
time_elpased: 6.296
batch start
#iterations: 245
currently lose_sum: 571.5263366401196
time_elpased: 6.156
batch start
#iterations: 246
currently lose_sum: 569.1867704093456
time_elpased: 6.015
batch start
#iterations: 247
currently lose_sum: 571.8916073143482
time_elpased: 5.884
batch start
#iterations: 248
currently lose_sum: 572.8269998431206
time_elpased: 6.208
batch start
#iterations: 249
currently lose_sum: 570.6841000318527
time_elpased: 6.224
batch start
#iterations: 250
currently lose_sum: 571.7048166394234
time_elpased: 6.105
batch start
#iterations: 251
currently lose_sum: 568.1993522047997
time_elpased: 5.996
batch start
#iterations: 252
currently lose_sum: 571.4545048475266
time_elpased: 6.223
batch start
#iterations: 253
currently lose_sum: 571.7499457597733
time_elpased: 6.049
batch start
#iterations: 254
currently lose_sum: 571.6227544546127
time_elpased: 6.366
batch start
#iterations: 255
currently lose_sum: 571.0830767154694
time_elpased: 6.343
batch start
#iterations: 256
currently lose_sum: 572.7500885725021
time_elpased: 6.135
batch start
#iterations: 257
currently lose_sum: 572.5075083673
time_elpased: 5.953
batch start
#iterations: 258
currently lose_sum: 569.4662035107613
time_elpased: 6.07
batch start
#iterations: 259
currently lose_sum: 569.503059566021
time_elpased: 6.081
start validation test
0.576602209945
0.566991811568
0.894308943089
0.693992453132
0
validation finish
batch start
#iterations: 260
currently lose_sum: 571.8473752737045
time_elpased: 6.218
batch start
#iterations: 261
currently lose_sum: 570.9923909902573
time_elpased: 6.182
batch start
#iterations: 262
currently lose_sum: 571.001989543438
time_elpased: 6.242
batch start
#iterations: 263
currently lose_sum: 569.9795283973217
time_elpased: 6.254
batch start
#iterations: 264
currently lose_sum: 570.6332390904427
time_elpased: 6.287
batch start
#iterations: 265
currently lose_sum: 572.0346127152443
time_elpased: 5.979
batch start
#iterations: 266
currently lose_sum: 570.268291592598
time_elpased: 6.094
batch start
#iterations: 267
currently lose_sum: 570.7858096659184
time_elpased: 6.123
batch start
#iterations: 268
currently lose_sum: 571.9733964800835
time_elpased: 6.202
batch start
#iterations: 269
currently lose_sum: 571.3069560825825
time_elpased: 5.875
batch start
#iterations: 270
currently lose_sum: 569.8636939823627
time_elpased: 5.97
batch start
#iterations: 271
currently lose_sum: 570.8829274177551
time_elpased: 6.492
batch start
#iterations: 272
currently lose_sum: 569.7720205187798
time_elpased: 6.011
batch start
#iterations: 273
currently lose_sum: 572.1770054996014
time_elpased: 6.227
batch start
#iterations: 274
currently lose_sum: 568.5657486915588
time_elpased: 6.372
batch start
#iterations: 275
currently lose_sum: 573.6065793931484
time_elpased: 6.298
batch start
#iterations: 276
currently lose_sum: 568.7609242200851
time_elpased: 6.142
batch start
#iterations: 277
currently lose_sum: 570.382588326931
time_elpased: 6.221
batch start
#iterations: 278
currently lose_sum: 570.8391686081886
time_elpased: 5.999
batch start
#iterations: 279
currently lose_sum: 571.4382690787315
time_elpased: 6.077
start validation test
0.576933701657
0.567257764279
0.893794380982
0.694036559784
0
validation finish
batch start
#iterations: 280
currently lose_sum: 571.2269553542137
time_elpased: 6.03
batch start
#iterations: 281
currently lose_sum: 569.5801138877869
time_elpased: 6.326
batch start
#iterations: 282
currently lose_sum: 571.2589991688728
time_elpased: 6.366
batch start
#iterations: 283
currently lose_sum: 569.1579311490059
time_elpased: 6.254
batch start
#iterations: 284
currently lose_sum: 571.1192469000816
time_elpased: 6.192
batch start
#iterations: 285
currently lose_sum: 570.6964665055275
time_elpased: 5.901
batch start
#iterations: 286
currently lose_sum: 570.6842594742775
time_elpased: 6.145
batch start
#iterations: 287
currently lose_sum: 573.0219239294529
time_elpased: 5.968
batch start
#iterations: 288
currently lose_sum: 571.475011408329
time_elpased: 6.127
batch start
#iterations: 289
currently lose_sum: 570.5926658809185
time_elpased: 6.167
batch start
#iterations: 290
currently lose_sum: 571.9444073140621
time_elpased: 6.059
batch start
#iterations: 291
currently lose_sum: 571.3861998915672
time_elpased: 6.04
batch start
#iterations: 292
currently lose_sum: 570.9958012104034
time_elpased: 6.258
batch start
#iterations: 293
currently lose_sum: 567.5807077288628
time_elpased: 6.03
batch start
#iterations: 294
currently lose_sum: 571.2525820732117
time_elpased: 6.018
batch start
#iterations: 295
currently lose_sum: 571.6772805452347
time_elpased: 5.888
batch start
#iterations: 296
currently lose_sum: 571.7005080580711
time_elpased: 6.246
batch start
#iterations: 297
currently lose_sum: 571.9763460755348
time_elpased: 5.946
batch start
#iterations: 298
currently lose_sum: 571.0976946353912
time_elpased: 6.247
batch start
#iterations: 299
currently lose_sum: 568.3519579172134
time_elpased: 6.045
start validation test
0.576685082873
0.566733779308
0.898013790264
0.694911204906
0
validation finish
batch start
#iterations: 300
currently lose_sum: 570.4314906001091
time_elpased: 6.069
batch start
#iterations: 301
currently lose_sum: 568.6980510354042
time_elpased: 5.828
batch start
#iterations: 302
currently lose_sum: 571.1273102164268
time_elpased: 6.204
batch start
#iterations: 303
currently lose_sum: 571.3757425248623
time_elpased: 6.037
batch start
#iterations: 304
currently lose_sum: 569.638959467411
time_elpased: 6.082
batch start
#iterations: 305
currently lose_sum: 570.372233748436
time_elpased: 6.095
batch start
#iterations: 306
currently lose_sum: 570.6510408520699
time_elpased: 5.77
batch start
#iterations: 307
currently lose_sum: 569.7151818275452
time_elpased: 6.201
batch start
#iterations: 308
currently lose_sum: 570.0011115670204
time_elpased: 6.026
batch start
#iterations: 309
currently lose_sum: 571.1116631031036
time_elpased: 6.255
batch start
#iterations: 310
currently lose_sum: 569.2138938903809
time_elpased: 6.202
batch start
#iterations: 311
currently lose_sum: 569.650832682848
time_elpased: 6.181
batch start
#iterations: 312
currently lose_sum: 569.9954291880131
time_elpased: 6.394
batch start
#iterations: 313
currently lose_sum: 572.375502884388
time_elpased: 6.022
batch start
#iterations: 314
currently lose_sum: 570.9337622225285
time_elpased: 5.94
batch start
#iterations: 315
currently lose_sum: 570.4609686136246
time_elpased: 6.185
batch start
#iterations: 316
currently lose_sum: 569.8882766664028
time_elpased: 6.32
batch start
#iterations: 317
currently lose_sum: 569.6194013953209
time_elpased: 6.417
batch start
#iterations: 318
currently lose_sum: 570.2705332636833
time_elpased: 6.015
batch start
#iterations: 319
currently lose_sum: 569.8281487226486
time_elpased: 5.717
start validation test
0.577430939227
0.568080834677
0.888134197798
0.692936146295
0
validation finish
batch start
#iterations: 320
currently lose_sum: 571.3862298727036
time_elpased: 6.126
batch start
#iterations: 321
currently lose_sum: 569.8970583081245
time_elpased: 6.093
batch start
#iterations: 322
currently lose_sum: 571.3414580821991
time_elpased: 6.024
batch start
#iterations: 323
currently lose_sum: 570.4121678471565
time_elpased: 5.964
batch start
#iterations: 324
currently lose_sum: 571.3334273695946
time_elpased: 6.017
batch start
#iterations: 325
currently lose_sum: 570.7420428097248
time_elpased: 6.108
batch start
#iterations: 326
currently lose_sum: 571.7661362290382
time_elpased: 5.758
batch start
#iterations: 327
currently lose_sum: 571.4289481937885
time_elpased: 6.482
batch start
#iterations: 328
currently lose_sum: 570.2711140215397
time_elpased: 6.028
batch start
#iterations: 329
currently lose_sum: 570.1069766879082
time_elpased: 6.208
batch start
#iterations: 330
currently lose_sum: 568.6580446362495
time_elpased: 6.263
batch start
#iterations: 331
currently lose_sum: 568.7858112752438
time_elpased: 6.137
batch start
#iterations: 332
currently lose_sum: 570.5157867968082
time_elpased: 6.124
batch start
#iterations: 333
currently lose_sum: 569.2138137817383
time_elpased: 5.795
batch start
#iterations: 334
currently lose_sum: 570.9573090076447
time_elpased: 6.119
batch start
#iterations: 335
currently lose_sum: 569.4655214250088
time_elpased: 6.008
batch start
#iterations: 336
currently lose_sum: 572.1239086091518
time_elpased: 5.831
batch start
#iterations: 337
currently lose_sum: 571.131655305624
time_elpased: 5.943
batch start
#iterations: 338
currently lose_sum: 569.5697493553162
time_elpased: 6.193
batch start
#iterations: 339
currently lose_sum: 571.2676871418953
time_elpased: 6.129
start validation test
0.577403314917
0.568440561292
0.883811876093
0.691883182276
0
validation finish
batch start
#iterations: 340
currently lose_sum: 570.3984641432762
time_elpased: 6.211
batch start
#iterations: 341
currently lose_sum: 571.7546508610249
time_elpased: 6.07
batch start
#iterations: 342
currently lose_sum: 572.2447753548622
time_elpased: 6.212
batch start
#iterations: 343
currently lose_sum: 568.8666578829288
time_elpased: 6.168
batch start
#iterations: 344
currently lose_sum: 570.6761916875839
time_elpased: 6.237
batch start
#iterations: 345
currently lose_sum: 570.5868732631207
time_elpased: 6.155
batch start
#iterations: 346
currently lose_sum: 570.7436023950577
time_elpased: 6.177
batch start
#iterations: 347
currently lose_sum: 570.8178127706051
time_elpased: 6.052
batch start
#iterations: 348
currently lose_sum: 570.1307826042175
time_elpased: 5.99
batch start
#iterations: 349
currently lose_sum: 569.7506737411022
time_elpased: 6.097
batch start
#iterations: 350
currently lose_sum: 570.4769941568375
time_elpased: 6.449
batch start
#iterations: 351
currently lose_sum: 569.2481512129307
time_elpased: 6.037
batch start
#iterations: 352
currently lose_sum: 570.7575702369213
time_elpased: 5.856
batch start
#iterations: 353
currently lose_sum: 569.5346649885178
time_elpased: 5.888
batch start
#iterations: 354
currently lose_sum: 572.3630481958389
time_elpased: 6.042
batch start
#iterations: 355
currently lose_sum: 568.857230246067
time_elpased: 6.19
batch start
#iterations: 356
currently lose_sum: 567.9060907959938
time_elpased: 5.946
batch start
#iterations: 357
currently lose_sum: 569.5911962389946
time_elpased: 5.975
batch start
#iterations: 358
currently lose_sum: 572.0017294585705
time_elpased: 6.176
batch start
#iterations: 359
currently lose_sum: 570.7590965926647
time_elpased: 6.122
start validation test
0.576022099448
0.56675161733
0.892559431923
0.693285371703
0
validation finish
batch start
#iterations: 360
currently lose_sum: 570.7988784909248
time_elpased: 6.209
batch start
#iterations: 361
currently lose_sum: 569.6749213337898
time_elpased: 6.085
batch start
#iterations: 362
currently lose_sum: 572.4130919575691
time_elpased: 6.127
batch start
#iterations: 363
currently lose_sum: 568.9353412389755
time_elpased: 6.041
batch start
#iterations: 364
currently lose_sum: 571.389119297266
time_elpased: 6.007
batch start
#iterations: 365
currently lose_sum: 570.3725859820843
time_elpased: 6.203
batch start
#iterations: 366
currently lose_sum: 568.4859709143639
time_elpased: 6.141
batch start
#iterations: 367
currently lose_sum: 570.4988214373589
time_elpased: 5.965
batch start
#iterations: 368
currently lose_sum: 571.0604637861252
time_elpased: 5.997
batch start
#iterations: 369
currently lose_sum: 570.820395886898
time_elpased: 6.119
batch start
#iterations: 370
currently lose_sum: 571.7986268103123
time_elpased: 5.882
batch start
#iterations: 371
currently lose_sum: 569.131865799427
time_elpased: 5.975
batch start
#iterations: 372
currently lose_sum: 570.846195101738
time_elpased: 6.098
batch start
#iterations: 373
currently lose_sum: 571.5577412843704
time_elpased: 6.019
batch start
#iterations: 374
currently lose_sum: 571.4281354546547
time_elpased: 6.144
batch start
#iterations: 375
currently lose_sum: 569.499557942152
time_elpased: 5.874
batch start
#iterations: 376
currently lose_sum: 568.8852863013744
time_elpased: 6.335
batch start
#iterations: 377
currently lose_sum: 570.8276236653328
time_elpased: 6.152
batch start
#iterations: 378
currently lose_sum: 570.2189803719521
time_elpased: 6.082
batch start
#iterations: 379
currently lose_sum: 569.3827690184116
time_elpased: 5.973
start validation test
0.577651933702
0.568519191986
0.884841000309
0.692256597089
0
validation finish
batch start
#iterations: 380
currently lose_sum: 569.8363693356514
time_elpased: 6.086
batch start
#iterations: 381
currently lose_sum: 571.0512555241585
time_elpased: 6.027
batch start
#iterations: 382
currently lose_sum: 571.358989238739
time_elpased: 6.155
batch start
#iterations: 383
currently lose_sum: 573.1378654837608
time_elpased: 6.209
batch start
#iterations: 384
currently lose_sum: 572.0777642726898
time_elpased: 6.19
batch start
#iterations: 385
currently lose_sum: 569.8400075137615
time_elpased: 5.94
batch start
#iterations: 386
currently lose_sum: 570.4600657522678
time_elpased: 5.822
batch start
#iterations: 387
currently lose_sum: 570.5526593327522
time_elpased: 6.161
batch start
#iterations: 388
currently lose_sum: 571.9982014596462
time_elpased: 6.038
batch start
#iterations: 389
currently lose_sum: 571.6495704948902
time_elpased: 6.17
batch start
#iterations: 390
currently lose_sum: 570.3222007751465
time_elpased: 6.132
batch start
#iterations: 391
currently lose_sum: 569.5612018108368
time_elpased: 6.022
batch start
#iterations: 392
currently lose_sum: 569.3959417939186
time_elpased: 6.018
batch start
#iterations: 393
currently lose_sum: 571.4188951849937
time_elpased: 6.235
batch start
#iterations: 394
currently lose_sum: 571.5107825398445
time_elpased: 6.232
batch start
#iterations: 395
currently lose_sum: 571.3072974681854
time_elpased: 5.873
batch start
#iterations: 396
currently lose_sum: 571.1322867870331
time_elpased: 6.147
batch start
#iterations: 397
currently lose_sum: 571.1343547403812
time_elpased: 6.166
batch start
#iterations: 398
currently lose_sum: 572.103084564209
time_elpased: 6.158
batch start
#iterations: 399
currently lose_sum: 570.3644172549248
time_elpased: 6.148
start validation test
0.577071823204
0.567954126022
0.886796336318
0.692434408775
0
validation finish
acc: 0.570
pre: 0.560
rec: 0.921
F1: 0.697
auc: 0.000
