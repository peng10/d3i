start to construct graph
graph construct over
93366
epochs start
batch start
#iterations: 0
currently lose_sum: 616.7020134329796
time_elpased: 5.938
batch start
#iterations: 1
currently lose_sum: 610.7819360494614
time_elpased: 5.851
batch start
#iterations: 2
currently lose_sum: 603.6856770515442
time_elpased: 5.538
batch start
#iterations: 3
currently lose_sum: 599.86931848526
time_elpased: 5.559
batch start
#iterations: 4
currently lose_sum: 596.9632337093353
time_elpased: 5.705
batch start
#iterations: 5
currently lose_sum: 595.175738632679
time_elpased: 5.895
batch start
#iterations: 6
currently lose_sum: 593.9161091446877
time_elpased: 5.818
batch start
#iterations: 7
currently lose_sum: 592.4132920503616
time_elpased: 5.954
batch start
#iterations: 8
currently lose_sum: 589.0716787576675
time_elpased: 5.683
batch start
#iterations: 9
currently lose_sum: 590.7696336507797
time_elpased: 5.693
batch start
#iterations: 10
currently lose_sum: 588.4269962310791
time_elpased: 5.807
batch start
#iterations: 11
currently lose_sum: 588.4307625889778
time_elpased: 5.873
batch start
#iterations: 12
currently lose_sum: 586.374770283699
time_elpased: 5.845
batch start
#iterations: 13
currently lose_sum: 588.0276836156845
time_elpased: 5.763
batch start
#iterations: 14
currently lose_sum: 585.8244758248329
time_elpased: 5.744
batch start
#iterations: 15
currently lose_sum: 584.7059928178787
time_elpased: 5.527
batch start
#iterations: 16
currently lose_sum: 584.0845411419868
time_elpased: 5.835
batch start
#iterations: 17
currently lose_sum: 585.7769269347191
time_elpased: 5.566
batch start
#iterations: 18
currently lose_sum: 585.234356701374
time_elpased: 5.956
batch start
#iterations: 19
currently lose_sum: 588.025739133358
time_elpased: 5.619
start validation test
0.571218836565
0.561633161914
0.927240917979
0.699547739669
0
validation finish
batch start
#iterations: 20
currently lose_sum: 585.681117773056
time_elpased: 5.613
batch start
#iterations: 21
currently lose_sum: 584.8769094944
time_elpased: 5.685
batch start
#iterations: 22
currently lose_sum: 585.4348394870758
time_elpased: 5.577
batch start
#iterations: 23
currently lose_sum: 583.8478572368622
time_elpased: 5.962
batch start
#iterations: 24
currently lose_sum: 583.4968541562557
time_elpased: 6.032
batch start
#iterations: 25
currently lose_sum: 584.6809495091438
time_elpased: 5.635
batch start
#iterations: 26
currently lose_sum: 582.1612950563431
time_elpased: 5.858
batch start
#iterations: 27
currently lose_sum: 581.7691071033478
time_elpased: 5.872
batch start
#iterations: 28
currently lose_sum: 581.7798743247986
time_elpased: 5.548
batch start
#iterations: 29
currently lose_sum: 582.766478061676
time_elpased: 5.857
batch start
#iterations: 30
currently lose_sum: 583.0524287819862
time_elpased: 5.718
batch start
#iterations: 31
currently lose_sum: 582.2082251906395
time_elpased: 5.827
batch start
#iterations: 32
currently lose_sum: 580.8930307030678
time_elpased: 5.933
batch start
#iterations: 33
currently lose_sum: 580.5433743596077
time_elpased: 6.002
batch start
#iterations: 34
currently lose_sum: 581.0856728851795
time_elpased: 5.644
batch start
#iterations: 35
currently lose_sum: 581.287201821804
time_elpased: 5.854
batch start
#iterations: 36
currently lose_sum: 580.9041612744331
time_elpased: 5.986
batch start
#iterations: 37
currently lose_sum: 580.8020868897438
time_elpased: 5.715
batch start
#iterations: 38
currently lose_sum: 580.7018026709557
time_elpased: 5.55
batch start
#iterations: 39
currently lose_sum: 581.5024037063122
time_elpased: 5.487
start validation test
0.573074792244
0.56473314878
0.902747761655
0.694811881188
0
validation finish
batch start
#iterations: 40
currently lose_sum: 579.8844180107117
time_elpased: 5.779
batch start
#iterations: 41
currently lose_sum: 578.7297129034996
time_elpased: 6.058
batch start
#iterations: 42
currently lose_sum: 581.2115344405174
time_elpased: 5.898
batch start
#iterations: 43
currently lose_sum: 579.664813041687
time_elpased: 5.773
batch start
#iterations: 44
currently lose_sum: 580.6784152686596
time_elpased: 5.763
batch start
#iterations: 45
currently lose_sum: 580.0125188827515
time_elpased: 5.786
batch start
#iterations: 46
currently lose_sum: 581.0186108350754
time_elpased: 5.592
batch start
#iterations: 47
currently lose_sum: 580.0163889527321
time_elpased: 5.748
batch start
#iterations: 48
currently lose_sum: 580.9298560619354
time_elpased: 5.685
batch start
#iterations: 49
currently lose_sum: 579.9056466817856
time_elpased: 5.762
batch start
#iterations: 50
currently lose_sum: 579.4461035132408
time_elpased: 5.894
batch start
#iterations: 51
currently lose_sum: 577.6518596410751
time_elpased: 5.777
batch start
#iterations: 52
currently lose_sum: 578.6517180204391
time_elpased: 5.554
batch start
#iterations: 53
currently lose_sum: 579.2034701704979
time_elpased: 5.709
batch start
#iterations: 54
currently lose_sum: 580.3870644569397
time_elpased: 5.72
batch start
#iterations: 55
currently lose_sum: 578.0188300609589
time_elpased: 5.568
batch start
#iterations: 56
currently lose_sum: 579.1411951184273
time_elpased: 5.685
batch start
#iterations: 57
currently lose_sum: 577.5726127028465
time_elpased: 5.9
batch start
#iterations: 58
currently lose_sum: 581.766255915165
time_elpased: 5.672
batch start
#iterations: 59
currently lose_sum: 576.8514828085899
time_elpased: 5.81
start validation test
0.573850415512
0.565521258008
0.899351651744
0.694398092968
0
validation finish
batch start
#iterations: 60
currently lose_sum: 579.4007216095924
time_elpased: 5.862
batch start
#iterations: 61
currently lose_sum: 577.3965012729168
time_elpased: 5.755
batch start
#iterations: 62
currently lose_sum: 578.4789761304855
time_elpased: 5.793
batch start
#iterations: 63
currently lose_sum: 578.4488990008831
time_elpased: 5.86
batch start
#iterations: 64
currently lose_sum: 575.86542314291
time_elpased: 5.832
batch start
#iterations: 65
currently lose_sum: 577.2989819347858
time_elpased: 5.9
batch start
#iterations: 66
currently lose_sum: 576.9744176864624
time_elpased: 5.814
batch start
#iterations: 67
currently lose_sum: 577.0595853924751
time_elpased: 5.958
batch start
#iterations: 68
currently lose_sum: 576.6767355203629
time_elpased: 5.681
batch start
#iterations: 69
currently lose_sum: 578.3908217251301
time_elpased: 5.823
batch start
#iterations: 70
currently lose_sum: 576.744453728199
time_elpased: 5.824
batch start
#iterations: 71
currently lose_sum: 578.9369887411594
time_elpased: 5.503
batch start
#iterations: 72
currently lose_sum: 576.4510412812233
time_elpased: 5.882
batch start
#iterations: 73
currently lose_sum: 579.2522388100624
time_elpased: 5.556
batch start
#iterations: 74
currently lose_sum: 577.3099175691605
time_elpased: 5.848
batch start
#iterations: 75
currently lose_sum: 575.8810838460922
time_elpased: 5.664
batch start
#iterations: 76
currently lose_sum: 577.3091099560261
time_elpased: 5.555
batch start
#iterations: 77
currently lose_sum: 573.8861608803272
time_elpased: 5.44
batch start
#iterations: 78
currently lose_sum: 577.0132057666779
time_elpased: 5.821
batch start
#iterations: 79
currently lose_sum: 577.7745608091354
time_elpased: 5.721
start validation test
0.57351800554
0.563815664707
0.91787588762
0.698543233083
0
validation finish
batch start
#iterations: 80
currently lose_sum: 578.3084810972214
time_elpased: 6.114
batch start
#iterations: 81
currently lose_sum: 574.0671379566193
time_elpased: 5.831
batch start
#iterations: 82
currently lose_sum: 576.6033425927162
time_elpased: 5.579
batch start
#iterations: 83
currently lose_sum: 575.4161939620972
time_elpased: 5.774
batch start
#iterations: 84
currently lose_sum: 573.6502214372158
time_elpased: 5.604
batch start
#iterations: 85
currently lose_sum: 576.8815299272537
time_elpased: 5.737
batch start
#iterations: 86
currently lose_sum: 576.8017272949219
time_elpased: 5.753
batch start
#iterations: 87
currently lose_sum: 576.9598808288574
time_elpased: 5.775
batch start
#iterations: 88
currently lose_sum: 578.0030726790428
time_elpased: 5.52
batch start
#iterations: 89
currently lose_sum: 574.3009015321732
time_elpased: 5.856
batch start
#iterations: 90
currently lose_sum: 575.6348392367363
time_elpased: 6.0
batch start
#iterations: 91
currently lose_sum: 574.5214842557907
time_elpased: 5.667
batch start
#iterations: 92
currently lose_sum: 574.1420111060143
time_elpased: 5.789
batch start
#iterations: 93
currently lose_sum: 577.4511548280716
time_elpased: 5.815
batch start
#iterations: 94
currently lose_sum: 574.4648424983025
time_elpased: 5.689
batch start
#iterations: 95
currently lose_sum: 573.5150616765022
time_elpased: 5.83
batch start
#iterations: 96
currently lose_sum: 574.1041425466537
time_elpased: 5.437
batch start
#iterations: 97
currently lose_sum: 572.8309431672096
time_elpased: 5.844
batch start
#iterations: 98
currently lose_sum: 573.6579081714153
time_elpased: 5.813
batch start
#iterations: 99
currently lose_sum: 576.0806450843811
time_elpased: 5.773
start validation test
0.57351800554
0.564115592252
0.914068128023
0.697667111774
0
validation finish
batch start
#iterations: 100
currently lose_sum: 573.0820092260838
time_elpased: 5.683
batch start
#iterations: 101
currently lose_sum: 575.5549378991127
time_elpased: 5.666
batch start
#iterations: 102
currently lose_sum: 574.0350624918938
time_elpased: 5.807
batch start
#iterations: 103
currently lose_sum: 575.5344500541687
time_elpased: 5.804
batch start
#iterations: 104
currently lose_sum: 575.7561246752739
time_elpased: 5.486
batch start
#iterations: 105
currently lose_sum: 574.8387973308563
time_elpased: 5.837
batch start
#iterations: 106
currently lose_sum: 575.7092489004135
time_elpased: 5.812
batch start
#iterations: 107
currently lose_sum: 575.1081449687481
time_elpased: 5.803
batch start
#iterations: 108
currently lose_sum: 576.641291320324
time_elpased: 5.995
batch start
#iterations: 109
currently lose_sum: 575.6152674555779
time_elpased: 5.961
batch start
#iterations: 110
currently lose_sum: 573.8605749607086
time_elpased: 5.853
batch start
#iterations: 111
currently lose_sum: 574.1660167574883
time_elpased: 5.77
batch start
#iterations: 112
currently lose_sum: 574.6410926580429
time_elpased: 5.691
batch start
#iterations: 113
currently lose_sum: 575.1640566289425
time_elpased: 5.56
batch start
#iterations: 114
currently lose_sum: 572.969575881958
time_elpased: 5.885
batch start
#iterations: 115
currently lose_sum: 574.2432964742184
time_elpased: 5.77
batch start
#iterations: 116
currently lose_sum: 577.3220572471619
time_elpased: 5.845
batch start
#iterations: 117
currently lose_sum: 574.9455871582031
time_elpased: 5.791
batch start
#iterations: 118
currently lose_sum: 573.5849129855633
time_elpased: 5.621
batch start
#iterations: 119
currently lose_sum: 574.608427464962
time_elpased: 5.949
start validation test
0.574099722992
0.565916398714
0.896573016363
0.693865360492
0
validation finish
batch start
#iterations: 120
currently lose_sum: 574.2539195418358
time_elpased: 5.836
batch start
#iterations: 121
currently lose_sum: 575.3824819922447
time_elpased: 5.714
batch start
#iterations: 122
currently lose_sum: 574.668522387743
time_elpased: 5.641
batch start
#iterations: 123
currently lose_sum: 575.7937394976616
time_elpased: 6.078
batch start
#iterations: 124
currently lose_sum: 574.9169658124447
time_elpased: 5.582
batch start
#iterations: 125
currently lose_sum: 574.7493405342102
time_elpased: 5.902
batch start
#iterations: 126
currently lose_sum: 575.5113778710365
time_elpased: 5.79
batch start
#iterations: 127
currently lose_sum: 573.5995969474316
time_elpased: 5.727
batch start
#iterations: 128
currently lose_sum: 574.8733676970005
time_elpased: 5.87
batch start
#iterations: 129
currently lose_sum: 574.6101275682449
time_elpased: 5.348
batch start
#iterations: 130
currently lose_sum: 574.6711912453175
time_elpased: 5.894
batch start
#iterations: 131
currently lose_sum: 574.8783760070801
time_elpased: 5.791
batch start
#iterations: 132
currently lose_sum: 574.6873876452446
time_elpased: 5.766
batch start
#iterations: 133
currently lose_sum: 572.4600206613541
time_elpased: 5.581
batch start
#iterations: 134
currently lose_sum: 574.0018716752529
time_elpased: 5.966
batch start
#iterations: 135
currently lose_sum: 573.7650228738785
time_elpased: 5.666
batch start
#iterations: 136
currently lose_sum: 573.1986538171768
time_elpased: 5.834
batch start
#iterations: 137
currently lose_sum: 573.4383072853088
time_elpased: 5.66
batch start
#iterations: 138
currently lose_sum: 575.554449737072
time_elpased: 5.866
batch start
#iterations: 139
currently lose_sum: 574.8255333304405
time_elpased: 5.605
start validation test
0.574016620499
0.565844155844
0.896778841206
0.693872675877
0
validation finish
batch start
#iterations: 140
currently lose_sum: 572.3980712890625
time_elpased: 5.572
batch start
#iterations: 141
currently lose_sum: 572.9220914244652
time_elpased: 5.823
batch start
#iterations: 142
currently lose_sum: 573.3937497735023
time_elpased: 5.592
batch start
#iterations: 143
currently lose_sum: 575.3936852216721
time_elpased: 5.836
batch start
#iterations: 144
currently lose_sum: 573.5182311534882
time_elpased: 5.732
batch start
#iterations: 145
currently lose_sum: 572.7880382835865
time_elpased: 5.697
batch start
#iterations: 146
currently lose_sum: 572.5203142762184
time_elpased: 5.89
batch start
#iterations: 147
currently lose_sum: 571.227340221405
time_elpased: 5.859
batch start
#iterations: 148
currently lose_sum: 573.7575067281723
time_elpased: 5.744
batch start
#iterations: 149
currently lose_sum: 572.8125668168068
time_elpased: 5.836
batch start
#iterations: 150
currently lose_sum: 574.5170533657074
time_elpased: 5.915
batch start
#iterations: 151
currently lose_sum: 572.4114234447479
time_elpased: 5.729
batch start
#iterations: 152
currently lose_sum: 574.736681073904
time_elpased: 5.938
batch start
#iterations: 153
currently lose_sum: 571.0123395323753
time_elpased: 5.695
batch start
#iterations: 154
currently lose_sum: 572.5925860404968
time_elpased: 5.721
batch start
#iterations: 155
currently lose_sum: 575.4248033165932
time_elpased: 5.847
batch start
#iterations: 156
currently lose_sum: 573.4990494847298
time_elpased: 5.645
batch start
#iterations: 157
currently lose_sum: 573.2632152438164
time_elpased: 5.846
batch start
#iterations: 158
currently lose_sum: 574.0861927270889
time_elpased: 5.638
batch start
#iterations: 159
currently lose_sum: 571.8286840319633
time_elpased: 5.79
start validation test
0.573074792244
0.564975767367
0.89976330143
0.694109241029
0
validation finish
batch start
#iterations: 160
currently lose_sum: 573.3916017413139
time_elpased: 5.626
batch start
#iterations: 161
currently lose_sum: 573.4595783352852
time_elpased: 5.647
batch start
#iterations: 162
currently lose_sum: 572.7492586970329
time_elpased: 5.613
batch start
#iterations: 163
currently lose_sum: 572.6421591043472
time_elpased: 5.84
batch start
#iterations: 164
currently lose_sum: 575.1628579497337
time_elpased: 5.929
batch start
#iterations: 165
currently lose_sum: 574.2970852255821
time_elpased: 5.726
batch start
#iterations: 166
currently lose_sum: 572.6736804246902
time_elpased: 5.713
batch start
#iterations: 167
currently lose_sum: 575.3588298559189
time_elpased: 5.637
batch start
#iterations: 168
currently lose_sum: 574.6567925810814
time_elpased: 6.04
batch start
#iterations: 169
currently lose_sum: 571.7461507022381
time_elpased: 5.813
batch start
#iterations: 170
currently lose_sum: 573.4281329512596
time_elpased: 5.715
batch start
#iterations: 171
currently lose_sum: 572.8224288225174
time_elpased: 5.875
batch start
#iterations: 172
currently lose_sum: 571.3838075995445
time_elpased: 5.808
batch start
#iterations: 173
currently lose_sum: 572.4311060905457
time_elpased: 5.667
batch start
#iterations: 174
currently lose_sum: 570.6321956813335
time_elpased: 5.984
batch start
#iterations: 175
currently lose_sum: 573.3167024850845
time_elpased: 5.864
batch start
#iterations: 176
currently lose_sum: 573.3777452707291
time_elpased: 5.641
batch start
#iterations: 177
currently lose_sum: 572.9572457671165
time_elpased: 5.734
batch start
#iterations: 178
currently lose_sum: 571.5600060224533
time_elpased: 5.504
batch start
#iterations: 179
currently lose_sum: 572.0109893381596
time_elpased: 5.914
start validation test
0.573684210526
0.566069794798
0.891427395287
0.69243375035
0
validation finish
batch start
#iterations: 180
currently lose_sum: 572.4334390163422
time_elpased: 5.46
batch start
#iterations: 181
currently lose_sum: 573.3815389573574
time_elpased: 5.896
batch start
#iterations: 182
currently lose_sum: 573.8651131391525
time_elpased: 5.69
batch start
#iterations: 183
currently lose_sum: 573.8924542963505
time_elpased: 5.674
batch start
#iterations: 184
currently lose_sum: 572.2965344786644
time_elpased: 5.584
batch start
#iterations: 185
currently lose_sum: 574.6955421566963
time_elpased: 5.604
batch start
#iterations: 186
currently lose_sum: 572.9563093185425
time_elpased: 5.591
batch start
#iterations: 187
currently lose_sum: 573.833175599575
time_elpased: 5.797
batch start
#iterations: 188
currently lose_sum: 570.8436192274094
time_elpased: 5.906
batch start
#iterations: 189
currently lose_sum: 571.8787496685982
time_elpased: 5.511
batch start
#iterations: 190
currently lose_sum: 574.2980388700962
time_elpased: 5.923
batch start
#iterations: 191
currently lose_sum: 572.8031856119633
time_elpased: 5.666
batch start
#iterations: 192
currently lose_sum: 572.5544076561928
time_elpased: 5.733
batch start
#iterations: 193
currently lose_sum: 571.7611879110336
time_elpased: 6.068
batch start
#iterations: 194
currently lose_sum: 571.668080329895
time_elpased: 5.87
batch start
#iterations: 195
currently lose_sum: 572.085312962532
time_elpased: 5.549
batch start
#iterations: 196
currently lose_sum: 571.6803983747959
time_elpased: 5.797
batch start
#iterations: 197
currently lose_sum: 573.2849689126015
time_elpased: 5.643
batch start
#iterations: 198
currently lose_sum: 572.7556858062744
time_elpased: 6.017
batch start
#iterations: 199
currently lose_sum: 570.4516784846783
time_elpased: 5.837
start validation test
0.572686980609
0.564528593509
0.902130287126
0.694474153298
0
validation finish
batch start
#iterations: 200
currently lose_sum: 572.8684994578362
time_elpased: 5.697
batch start
#iterations: 201
currently lose_sum: 571.9726459681988
time_elpased: 5.607
batch start
#iterations: 202
currently lose_sum: 573.8024313747883
time_elpased: 5.685
batch start
#iterations: 203
currently lose_sum: 573.3655897676945
time_elpased: 5.726
batch start
#iterations: 204
currently lose_sum: 571.1705130040646
time_elpased: 5.855
batch start
#iterations: 205
currently lose_sum: 574.7673044204712
time_elpased: 5.474
batch start
#iterations: 206
currently lose_sum: 571.4696534276009
time_elpased: 5.933
batch start
#iterations: 207
currently lose_sum: 575.0322268009186
time_elpased: 5.726
batch start
#iterations: 208
currently lose_sum: 571.9629380106926
time_elpased: 5.576
batch start
#iterations: 209
currently lose_sum: 570.7897100746632
time_elpased: 5.599
batch start
#iterations: 210
currently lose_sum: 573.3215685486794
time_elpased: 5.778
batch start
#iterations: 211
currently lose_sum: 573.4721025526524
time_elpased: 5.973
batch start
#iterations: 212
currently lose_sum: 569.6749691665173
time_elpased: 5.765
batch start
#iterations: 213
currently lose_sum: 573.9364657402039
time_elpased: 5.781
batch start
#iterations: 214
currently lose_sum: 573.39062333107
time_elpased: 5.793
batch start
#iterations: 215
currently lose_sum: 572.2059100866318
time_elpased: 5.585
batch start
#iterations: 216
currently lose_sum: 572.3894263505936
time_elpased: 5.861
batch start
#iterations: 217
currently lose_sum: 574.4682631492615
time_elpased: 5.727
batch start
#iterations: 218
currently lose_sum: 572.7930091619492
time_elpased: 5.698
batch start
#iterations: 219
currently lose_sum: 572.6426911354065
time_elpased: 5.808
start validation test
0.574349030471
0.565562145436
0.902850674076
0.695469499386
0
validation finish
batch start
#iterations: 220
currently lose_sum: 574.5574058294296
time_elpased: 6.041
batch start
#iterations: 221
currently lose_sum: 572.5120724737644
time_elpased: 5.756
batch start
#iterations: 222
currently lose_sum: 572.7956958115101
time_elpased: 5.823
batch start
#iterations: 223
currently lose_sum: 573.6313692331314
time_elpased: 5.836
batch start
#iterations: 224
currently lose_sum: 573.1140839755535
time_elpased: 5.931
batch start
#iterations: 225
currently lose_sum: 572.5905575156212
time_elpased: 5.65
batch start
#iterations: 226
currently lose_sum: 572.9760414958
time_elpased: 5.859
batch start
#iterations: 227
currently lose_sum: 573.5647895038128
time_elpased: 5.418
batch start
#iterations: 228
currently lose_sum: 571.0187566280365
time_elpased: 5.806
batch start
#iterations: 229
currently lose_sum: 573.36172914505
time_elpased: 5.715
batch start
#iterations: 230
currently lose_sum: 572.7689163088799
time_elpased: 5.761
batch start
#iterations: 231
currently lose_sum: 572.2855538725853
time_elpased: 5.81
batch start
#iterations: 232
currently lose_sum: 572.0354181528091
time_elpased: 5.762
batch start
#iterations: 233
currently lose_sum: 571.6307888925076
time_elpased: 5.769
batch start
#iterations: 234
currently lose_sum: 572.9943981766701
time_elpased: 5.643
batch start
#iterations: 235
currently lose_sum: 572.0676721930504
time_elpased: 5.638
batch start
#iterations: 236
currently lose_sum: 572.6065457165241
time_elpased: 5.77
batch start
#iterations: 237
currently lose_sum: 570.3566944003105
time_elpased: 5.759
batch start
#iterations: 238
currently lose_sum: 573.544018805027
time_elpased: 5.701
batch start
#iterations: 239
currently lose_sum: 571.022748619318
time_elpased: 5.671
start validation test
0.573351800554
0.564441887227
0.908613769682
0.69632083284
0
validation finish
batch start
#iterations: 240
currently lose_sum: 570.9543174505234
time_elpased: 5.582
batch start
#iterations: 241
currently lose_sum: 572.719638466835
time_elpased: 5.861
batch start
#iterations: 242
currently lose_sum: 571.314510166645
time_elpased: 5.937
batch start
#iterations: 243
currently lose_sum: 570.2650255560875
time_elpased: 5.509
batch start
#iterations: 244
currently lose_sum: 571.9101361036301
time_elpased: 5.928
batch start
#iterations: 245
currently lose_sum: 570.7524504065514
time_elpased: 5.322
batch start
#iterations: 246
currently lose_sum: 571.2329257130623
time_elpased: 5.96
batch start
#iterations: 247
currently lose_sum: 570.5873094201088
time_elpased: 5.564
batch start
#iterations: 248
currently lose_sum: 571.1320832371712
time_elpased: 5.721
batch start
#iterations: 249
currently lose_sum: 569.4966826438904
time_elpased: 5.966
batch start
#iterations: 250
currently lose_sum: 572.3904403448105
time_elpased: 5.487
batch start
#iterations: 251
currently lose_sum: 572.7122107744217
time_elpased: 5.743
batch start
#iterations: 252
currently lose_sum: 571.7930202484131
time_elpased: 5.892
batch start
#iterations: 253
currently lose_sum: 572.1463989019394
time_elpased: 5.767
batch start
#iterations: 254
currently lose_sum: 570.1680918335915
time_elpased: 5.977
batch start
#iterations: 255
currently lose_sum: 571.3761064708233
time_elpased: 5.774
batch start
#iterations: 256
currently lose_sum: 569.8622536063194
time_elpased: 5.674
batch start
#iterations: 257
currently lose_sum: 570.9664031863213
time_elpased: 5.875
batch start
#iterations: 258
currently lose_sum: 571.2320593893528
time_elpased: 5.709
batch start
#iterations: 259
currently lose_sum: 570.1098974645138
time_elpased: 5.696
start validation test
0.573739612188
0.565161373446
0.902850674076
0.695166402536
0
validation finish
batch start
#iterations: 260
currently lose_sum: 573.5022026002407
time_elpased: 5.847
batch start
#iterations: 261
currently lose_sum: 573.6712689399719
time_elpased: 5.78
batch start
#iterations: 262
currently lose_sum: 574.1473341584206
time_elpased: 5.719
batch start
#iterations: 263
currently lose_sum: 570.8681917190552
time_elpased: 5.772
batch start
#iterations: 264
currently lose_sum: 571.4816825985909
time_elpased: 5.885
batch start
#iterations: 265
currently lose_sum: 573.625308573246
time_elpased: 5.68
batch start
#iterations: 266
currently lose_sum: 571.792839884758
time_elpased: 5.861
batch start
#iterations: 267
currently lose_sum: 572.5730885565281
time_elpased: 5.593
batch start
#iterations: 268
currently lose_sum: 570.1542228162289
time_elpased: 5.766
batch start
#iterations: 269
currently lose_sum: 572.4090051054955
time_elpased: 5.802
batch start
#iterations: 270
currently lose_sum: 572.0387870073318
time_elpased: 5.633
batch start
#iterations: 271
currently lose_sum: 570.6195054650307
time_elpased: 5.683
batch start
#iterations: 272
currently lose_sum: 570.4503906369209
time_elpased: 5.604
batch start
#iterations: 273
currently lose_sum: 571.3720222115517
time_elpased: 5.639
batch start
#iterations: 274
currently lose_sum: 570.8324526250362
time_elpased: 5.559
batch start
#iterations: 275
currently lose_sum: 571.8379219174385
time_elpased: 5.502
batch start
#iterations: 276
currently lose_sum: 571.6710232496262
time_elpased: 5.921
batch start
#iterations: 277
currently lose_sum: 573.0765923857689
time_elpased: 5.677
batch start
#iterations: 278
currently lose_sum: 573.3260072469711
time_elpased: 5.653
batch start
#iterations: 279
currently lose_sum: 570.8602957725525
time_elpased: 5.817
start validation test
0.573019390582
0.564847077042
0.900895338067
0.694348602023
0
validation finish
batch start
#iterations: 280
currently lose_sum: 571.4960672259331
time_elpased: 6.006
batch start
#iterations: 281
currently lose_sum: 572.9100812673569
time_elpased: 5.817
batch start
#iterations: 282
currently lose_sum: 571.7533674240112
time_elpased: 5.714
batch start
#iterations: 283
currently lose_sum: 572.27409273386
time_elpased: 5.923
batch start
#iterations: 284
currently lose_sum: 570.1112431883812
time_elpased: 5.951
batch start
#iterations: 285
currently lose_sum: 571.6288061738014
time_elpased: 5.713
batch start
#iterations: 286
currently lose_sum: 573.6824634671211
time_elpased: 5.734
batch start
#iterations: 287
currently lose_sum: 572.8403204679489
time_elpased: 5.477
batch start
#iterations: 288
currently lose_sum: 575.2842460870743
time_elpased: 5.883
batch start
#iterations: 289
currently lose_sum: 571.7945600748062
time_elpased: 5.64
batch start
#iterations: 290
currently lose_sum: 573.4778850674629
time_elpased: 5.789
batch start
#iterations: 291
currently lose_sum: 573.8166338205338
time_elpased: 6.0
batch start
#iterations: 292
currently lose_sum: 570.4865235090256
time_elpased: 5.802
batch start
#iterations: 293
currently lose_sum: 570.3915918469429
time_elpased: 5.581
batch start
#iterations: 294
currently lose_sum: 571.0583348870277
time_elpased: 5.685
batch start
#iterations: 295
currently lose_sum: 570.161503970623
time_elpased: 5.642
batch start
#iterations: 296
currently lose_sum: 570.8165112137794
time_elpased: 5.919
batch start
#iterations: 297
currently lose_sum: 570.7281629741192
time_elpased: 5.525
batch start
#iterations: 298
currently lose_sum: 571.226365417242
time_elpased: 5.849
batch start
#iterations: 299
currently lose_sum: 569.1418030261993
time_elpased: 5.579
start validation test
0.573407202216
0.56509391338
0.900998250489
0.694565648552
0
validation finish
batch start
#iterations: 300
currently lose_sum: 571.6856932640076
time_elpased: 5.722
batch start
#iterations: 301
currently lose_sum: 571.626151740551
time_elpased: 5.895
batch start
#iterations: 302
currently lose_sum: 571.93499776721
time_elpased: 5.508
batch start
#iterations: 303
currently lose_sum: 573.1376158297062
time_elpased: 5.833
batch start
#iterations: 304
currently lose_sum: 569.4640272855759
time_elpased: 5.523
batch start
#iterations: 305
currently lose_sum: 573.1912966370583
time_elpased: 5.764
batch start
#iterations: 306
currently lose_sum: 571.3757935166359
time_elpased: 5.759
batch start
#iterations: 307
currently lose_sum: 570.511072397232
time_elpased: 5.681
batch start
#iterations: 308
currently lose_sum: 571.8894289731979
time_elpased: 5.776
batch start
#iterations: 309
currently lose_sum: 572.100122064352
time_elpased: 5.644
batch start
#iterations: 310
currently lose_sum: 570.7565492987633
time_elpased: 5.934
batch start
#iterations: 311
currently lose_sum: 568.4978673458099
time_elpased: 5.9
batch start
#iterations: 312
currently lose_sum: 571.8230660557747
time_elpased: 5.632
batch start
#iterations: 313
currently lose_sum: 572.1834070682526
time_elpased: 5.79
batch start
#iterations: 314
currently lose_sum: 571.4949158728123
time_elpased: 5.81
batch start
#iterations: 315
currently lose_sum: 570.4452331662178
time_elpased: 5.708
batch start
#iterations: 316
currently lose_sum: 573.0924462080002
time_elpased: 5.923
batch start
#iterations: 317
currently lose_sum: 571.4751278162003
time_elpased: 5.551
batch start
#iterations: 318
currently lose_sum: 574.1049196720123
time_elpased: 5.819
batch start
#iterations: 319
currently lose_sum: 568.9420286417007
time_elpased: 5.886
start validation test
0.573545706371
0.565336956873
0.899145826901
0.694197803071
0
validation finish
batch start
#iterations: 320
currently lose_sum: 571.3111561536789
time_elpased: 5.796
batch start
#iterations: 321
currently lose_sum: 571.1023273766041
time_elpased: 5.606
batch start
#iterations: 322
currently lose_sum: 570.0274412631989
time_elpased: 5.897
batch start
#iterations: 323
currently lose_sum: 572.1091822385788
time_elpased: 5.766
batch start
#iterations: 324
currently lose_sum: 572.4492802619934
time_elpased: 5.872
batch start
#iterations: 325
currently lose_sum: 571.8249333798885
time_elpased: 5.631
batch start
#iterations: 326
currently lose_sum: 571.4073104262352
time_elpased: 5.746
batch start
#iterations: 327
currently lose_sum: 570.277576893568
time_elpased: 6.061
batch start
#iterations: 328
currently lose_sum: 572.173303425312
time_elpased: 5.957
batch start
#iterations: 329
currently lose_sum: 570.042914390564
time_elpased: 5.946
batch start
#iterations: 330
currently lose_sum: 570.4983565807343
time_elpased: 5.683
batch start
#iterations: 331
currently lose_sum: 571.7503615617752
time_elpased: 5.932
batch start
#iterations: 332
currently lose_sum: 571.70127171278
time_elpased: 5.614
batch start
#iterations: 333
currently lose_sum: 571.4209696650505
time_elpased: 5.53
batch start
#iterations: 334
currently lose_sum: 570.9643203616142
time_elpased: 5.769
batch start
#iterations: 335
currently lose_sum: 571.02602404356
time_elpased: 5.711
batch start
#iterations: 336
currently lose_sum: 570.827383518219
time_elpased: 5.52
batch start
#iterations: 337
currently lose_sum: 571.3802616596222
time_elpased: 5.636
batch start
#iterations: 338
currently lose_sum: 570.9842384755611
time_elpased: 5.526
batch start
#iterations: 339
currently lose_sum: 572.2140992283821
time_elpased: 5.851
start validation test
0.573684210526
0.565234223771
0.901512812596
0.694824509221
0
validation finish
batch start
#iterations: 340
currently lose_sum: 571.2409910261631
time_elpased: 5.663
batch start
#iterations: 341
currently lose_sum: 570.185360699892
time_elpased: 6.041
batch start
#iterations: 342
currently lose_sum: 571.7879531979561
time_elpased: 5.844
batch start
#iterations: 343
currently lose_sum: 573.2987303733826
time_elpased: 5.526
batch start
#iterations: 344
currently lose_sum: 571.5013796687126
time_elpased: 5.866
batch start
#iterations: 345
currently lose_sum: 571.0817828774452
time_elpased: 5.62
batch start
#iterations: 346
currently lose_sum: 571.1880321800709
time_elpased: 5.938
batch start
#iterations: 347
currently lose_sum: 571.6467074155807
time_elpased: 5.845
batch start
#iterations: 348
currently lose_sum: 572.3019205331802
time_elpased: 6.105
batch start
#iterations: 349
currently lose_sum: 570.5040130913258
time_elpased: 5.511
batch start
#iterations: 350
currently lose_sum: 571.5876608490944
time_elpased: 5.838
batch start
#iterations: 351
currently lose_sum: 572.2511276602745
time_elpased: 5.486
batch start
#iterations: 352
currently lose_sum: 571.7575372457504
time_elpased: 5.731
batch start
#iterations: 353
currently lose_sum: 571.919480741024
time_elpased: 5.603
batch start
#iterations: 354
currently lose_sum: 571.5780238211155
time_elpased: 5.703
batch start
#iterations: 355
currently lose_sum: 571.5306111574173
time_elpased: 5.634
batch start
#iterations: 356
currently lose_sum: 571.047246336937
time_elpased: 5.853
batch start
#iterations: 357
currently lose_sum: 571.5942733883858
time_elpased: 5.731
batch start
#iterations: 358
currently lose_sum: 572.4895613193512
time_elpased: 5.772
batch start
#iterations: 359
currently lose_sum: 569.8393754065037
time_elpased: 5.43
start validation test
0.573268698061
0.565137258706
0.899351651744
0.694108536367
0
validation finish
batch start
#iterations: 360
currently lose_sum: 570.7410782277584
time_elpased: 5.569
batch start
#iterations: 361
currently lose_sum: 572.1248183846474
time_elpased: 5.844
batch start
#iterations: 362
currently lose_sum: 572.2202715873718
time_elpased: 5.536
batch start
#iterations: 363
currently lose_sum: 571.3779309689999
time_elpased: 5.713
batch start
#iterations: 364
currently lose_sum: 571.2486619055271
time_elpased: 5.599
batch start
#iterations: 365
currently lose_sum: 570.8104006946087
time_elpased: 5.715
batch start
#iterations: 366
currently lose_sum: 569.8183621168137
time_elpased: 5.483
batch start
#iterations: 367
currently lose_sum: 570.4798374474049
time_elpased: 5.897
batch start
#iterations: 368
currently lose_sum: 570.4892041087151
time_elpased: 5.836
batch start
#iterations: 369
currently lose_sum: 571.4102453887463
time_elpased: 5.601
batch start
#iterations: 370
currently lose_sum: 570.0271875560284
time_elpased: 5.91
batch start
#iterations: 371
currently lose_sum: 571.8084113001823
time_elpased: 5.968
batch start
#iterations: 372
currently lose_sum: 572.8051266372204
time_elpased: 5.556
batch start
#iterations: 373
currently lose_sum: 569.9984987974167
time_elpased: 5.946
batch start
#iterations: 374
currently lose_sum: 572.7595604360104
time_elpased: 5.903
batch start
#iterations: 375
currently lose_sum: 571.8733752667904
time_elpased: 5.828
batch start
#iterations: 376
currently lose_sum: 570.0984888970852
time_elpased: 5.792
batch start
#iterations: 377
currently lose_sum: 571.1714154779911
time_elpased: 5.852
batch start
#iterations: 378
currently lose_sum: 573.4966412782669
time_elpased: 5.6
batch start
#iterations: 379
currently lose_sum: 570.3362246155739
time_elpased: 5.701
start validation test
0.573822714681
0.565553864586
0.898734177215
0.694238527734
0
validation finish
batch start
#iterations: 380
currently lose_sum: 570.4774544239044
time_elpased: 6.03
batch start
#iterations: 381
currently lose_sum: 572.6909880936146
time_elpased: 5.685
batch start
#iterations: 382
currently lose_sum: 569.1670497655869
time_elpased: 5.916
batch start
#iterations: 383
currently lose_sum: 571.3273971676826
time_elpased: 5.782
batch start
#iterations: 384
currently lose_sum: 571.9868885874748
time_elpased: 5.865
batch start
#iterations: 385
currently lose_sum: 572.3838613629341
time_elpased: 5.578
batch start
#iterations: 386
currently lose_sum: 571.7928631305695
time_elpased: 5.96
batch start
#iterations: 387
currently lose_sum: 570.258663892746
time_elpased: 5.387
batch start
#iterations: 388
currently lose_sum: 571.3769986629486
time_elpased: 5.791
batch start
#iterations: 389
currently lose_sum: 573.618575334549
time_elpased: 5.714
batch start
#iterations: 390
currently lose_sum: 572.1545178592205
time_elpased: 5.956
batch start
#iterations: 391
currently lose_sum: 570.3102743327618
time_elpased: 6.044
batch start
#iterations: 392
currently lose_sum: 572.238888323307
time_elpased: 5.929
batch start
#iterations: 393
currently lose_sum: 570.9147145748138
time_elpased: 5.725
batch start
#iterations: 394
currently lose_sum: 569.1863748729229
time_elpased: 5.735
batch start
#iterations: 395
currently lose_sum: 570.1855020523071
time_elpased: 5.525
batch start
#iterations: 396
currently lose_sum: 569.898493796587
time_elpased: 5.719
batch start
#iterations: 397
currently lose_sum: 568.2417437434196
time_elpased: 5.799
batch start
#iterations: 398
currently lose_sum: 569.5635459423065
time_elpased: 5.762
batch start
#iterations: 399
currently lose_sum: 572.888730108738
time_elpased: 5.915
start validation test
0.572991689751
0.56512298668
0.897293403314
0.693483923565
0
validation finish
acc: 0.572
pre: 0.562
rec: 0.927
F1: 0.700
auc: 0.000
