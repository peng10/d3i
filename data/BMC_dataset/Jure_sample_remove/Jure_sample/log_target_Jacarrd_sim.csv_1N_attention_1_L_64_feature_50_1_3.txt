start to construct graph
graph construct over
93195
epochs start
batch start
#iterations: 0
currently lose_sum: 609.6437779068947
time_elpased: 6.327
batch start
#iterations: 1
currently lose_sum: 597.3725459575653
time_elpased: 6.099
batch start
#iterations: 2
currently lose_sum: 592.5793551206589
time_elpased: 6.181
batch start
#iterations: 3
currently lose_sum: 588.8386735916138
time_elpased: 6.178
batch start
#iterations: 4
currently lose_sum: 586.2106665968895
time_elpased: 5.815
batch start
#iterations: 5
currently lose_sum: 584.3002907633781
time_elpased: 5.88
batch start
#iterations: 6
currently lose_sum: 583.5645499229431
time_elpased: 5.946
batch start
#iterations: 7
currently lose_sum: 582.4107045531273
time_elpased: 6.286
batch start
#iterations: 8
currently lose_sum: 579.9431445002556
time_elpased: 6.055
batch start
#iterations: 9
currently lose_sum: 579.6196910738945
time_elpased: 6.02
batch start
#iterations: 10
currently lose_sum: 577.0095313191414
time_elpased: 5.992
batch start
#iterations: 11
currently lose_sum: 576.2667272686958
time_elpased: 6.022
batch start
#iterations: 12
currently lose_sum: 578.6419017910957
time_elpased: 6.14
batch start
#iterations: 13
currently lose_sum: 577.1380575299263
time_elpased: 5.982
batch start
#iterations: 14
currently lose_sum: 575.7805774211884
time_elpased: 5.927
batch start
#iterations: 15
currently lose_sum: 575.6962354183197
time_elpased: 5.857
batch start
#iterations: 16
currently lose_sum: 574.2354428172112
time_elpased: 6.02
batch start
#iterations: 17
currently lose_sum: 574.9583278894424
time_elpased: 6.05
batch start
#iterations: 18
currently lose_sum: 574.7039187550545
time_elpased: 5.995
batch start
#iterations: 19
currently lose_sum: 571.318284124136
time_elpased: 5.957
start validation test
0.575650969529
0.567243239708
0.892754219844
0.693711886434
0
validation finish
batch start
#iterations: 20
currently lose_sum: 572.7362135648727
time_elpased: 6.455
batch start
#iterations: 21
currently lose_sum: 573.7797634005547
time_elpased: 6.349
batch start
#iterations: 22
currently lose_sum: 571.9931163191795
time_elpased: 6.246
batch start
#iterations: 23
currently lose_sum: 572.4852823615074
time_elpased: 5.872
batch start
#iterations: 24
currently lose_sum: 572.1932428181171
time_elpased: 5.775
batch start
#iterations: 25
currently lose_sum: 571.7972345352173
time_elpased: 6.092
batch start
#iterations: 26
currently lose_sum: 572.8361178040504
time_elpased: 6.141
batch start
#iterations: 27
currently lose_sum: 571.0797374844551
time_elpased: 6.044
batch start
#iterations: 28
currently lose_sum: 572.7840310633183
time_elpased: 5.755
batch start
#iterations: 29
currently lose_sum: 572.0190992951393
time_elpased: 6.045
batch start
#iterations: 30
currently lose_sum: 571.404046535492
time_elpased: 6.211
batch start
#iterations: 31
currently lose_sum: 570.2258922457695
time_elpased: 6.038
batch start
#iterations: 32
currently lose_sum: 570.8521916866302
time_elpased: 6.202
batch start
#iterations: 33
currently lose_sum: 571.5005573630333
time_elpased: 5.996
batch start
#iterations: 34
currently lose_sum: 571.982266664505
time_elpased: 6.09
batch start
#iterations: 35
currently lose_sum: 569.5194364190102
time_elpased: 6.106
batch start
#iterations: 36
currently lose_sum: 570.8341072201729
time_elpased: 6.199
batch start
#iterations: 37
currently lose_sum: 571.7491849958897
time_elpased: 6.471
batch start
#iterations: 38
currently lose_sum: 569.9192454218864
time_elpased: 6.053
batch start
#iterations: 39
currently lose_sum: 569.0566624701023
time_elpased: 6.01
start validation test
0.577396121884
0.567951054413
0.89810621655
0.695853269537
0
validation finish
batch start
#iterations: 40
currently lose_sum: 569.5396634042263
time_elpased: 5.91
batch start
#iterations: 41
currently lose_sum: 569.8073689937592
time_elpased: 6.025
batch start
#iterations: 42
currently lose_sum: 569.5543575286865
time_elpased: 6.379
batch start
#iterations: 43
currently lose_sum: 570.4008217453957
time_elpased: 5.905
batch start
#iterations: 44
currently lose_sum: 570.4067016541958
time_elpased: 5.949
batch start
#iterations: 45
currently lose_sum: 566.4776562154293
time_elpased: 6.151
batch start
#iterations: 46
currently lose_sum: 567.6163167357445
time_elpased: 6.016
batch start
#iterations: 47
currently lose_sum: 569.2222727239132
time_elpased: 6.0
batch start
#iterations: 48
currently lose_sum: 570.5426113307476
time_elpased: 6.018
batch start
#iterations: 49
currently lose_sum: 566.8408329188824
time_elpased: 5.989
batch start
#iterations: 50
currently lose_sum: 567.8418971300125
time_elpased: 6.128
batch start
#iterations: 51
currently lose_sum: 571.0737875699997
time_elpased: 6.297
batch start
#iterations: 52
currently lose_sum: 565.8861528933048
time_elpased: 6.209
batch start
#iterations: 53
currently lose_sum: 567.3416836261749
time_elpased: 5.73
batch start
#iterations: 54
currently lose_sum: 569.1989874839783
time_elpased: 6.075
batch start
#iterations: 55
currently lose_sum: 568.1802218556404
time_elpased: 5.941
batch start
#iterations: 56
currently lose_sum: 567.9060008823872
time_elpased: 6.083
batch start
#iterations: 57
currently lose_sum: 569.2985928952694
time_elpased: 5.816
batch start
#iterations: 58
currently lose_sum: 567.1961480975151
time_elpased: 5.905
batch start
#iterations: 59
currently lose_sum: 569.3213394582272
time_elpased: 6.039
start validation test
0.577922437673
0.568355874206
0.897488678469
0.695971426861
0
validation finish
batch start
#iterations: 60
currently lose_sum: 568.4683228135109
time_elpased: 6.018
batch start
#iterations: 61
currently lose_sum: 567.3624513447285
time_elpased: 6.159
batch start
#iterations: 62
currently lose_sum: 565.9425212740898
time_elpased: 6.028
batch start
#iterations: 63
currently lose_sum: 565.0492173433304
time_elpased: 6.175
batch start
#iterations: 64
currently lose_sum: 567.1604933142662
time_elpased: 6.132
batch start
#iterations: 65
currently lose_sum: 568.7710126638412
time_elpased: 6.27
batch start
#iterations: 66
currently lose_sum: 568.1193722486496
time_elpased: 6.048
batch start
#iterations: 67
currently lose_sum: 566.4424305558205
time_elpased: 5.961
batch start
#iterations: 68
currently lose_sum: 566.8350431919098
time_elpased: 5.91
batch start
#iterations: 69
currently lose_sum: 568.3946506083012
time_elpased: 6.0
batch start
#iterations: 70
currently lose_sum: 567.1876538097858
time_elpased: 6.058
batch start
#iterations: 71
currently lose_sum: 567.3628368973732
time_elpased: 6.047
batch start
#iterations: 72
currently lose_sum: 566.3783700466156
time_elpased: 5.93
batch start
#iterations: 73
currently lose_sum: 567.2936871051788
time_elpased: 6.092
batch start
#iterations: 74
currently lose_sum: 566.268607467413
time_elpased: 6.053
batch start
#iterations: 75
currently lose_sum: 564.0095373690128
time_elpased: 6.331
batch start
#iterations: 76
currently lose_sum: 565.8933896422386
time_elpased: 6.222
batch start
#iterations: 77
currently lose_sum: 566.9355272352695
time_elpased: 6.064
batch start
#iterations: 78
currently lose_sum: 567.1841778159142
time_elpased: 6.007
batch start
#iterations: 79
currently lose_sum: 563.9964733123779
time_elpased: 6.158
start validation test
0.576620498615
0.568214708591
0.889048991354
0.69331407015
0
validation finish
batch start
#iterations: 80
currently lose_sum: 564.9076784849167
time_elpased: 5.94
batch start
#iterations: 81
currently lose_sum: 567.0055113732815
time_elpased: 5.977
batch start
#iterations: 82
currently lose_sum: 565.9623039364815
time_elpased: 7.366
batch start
#iterations: 83
currently lose_sum: 566.4607028961182
time_elpased: 6.491
batch start
#iterations: 84
currently lose_sum: 564.4109778702259
time_elpased: 6.077
batch start
#iterations: 85
currently lose_sum: 565.479559481144
time_elpased: 6.104
batch start
#iterations: 86
currently lose_sum: 564.6996784806252
time_elpased: 6.122
batch start
#iterations: 87
currently lose_sum: 564.8491655886173
time_elpased: 6.104
batch start
#iterations: 88
currently lose_sum: 565.5854005217552
time_elpased: 5.947
batch start
#iterations: 89
currently lose_sum: 566.4265932440758
time_elpased: 5.734
batch start
#iterations: 90
currently lose_sum: 566.0167422294617
time_elpased: 6.09
batch start
#iterations: 91
currently lose_sum: 564.3651829361916
time_elpased: 5.754
batch start
#iterations: 92
currently lose_sum: 565.8616853356361
time_elpased: 6.043
batch start
#iterations: 93
currently lose_sum: 563.8943544626236
time_elpased: 5.87
batch start
#iterations: 94
currently lose_sum: 564.897142380476
time_elpased: 5.941
batch start
#iterations: 95
currently lose_sum: 564.3377784788609
time_elpased: 5.851
batch start
#iterations: 96
currently lose_sum: 565.9335194826126
time_elpased: 5.818
batch start
#iterations: 97
currently lose_sum: 562.3265598714352
time_elpased: 6.257
batch start
#iterations: 98
currently lose_sum: 566.2636964917183
time_elpased: 6.071
batch start
#iterations: 99
currently lose_sum: 564.928236246109
time_elpased: 6.077
start validation test
0.576094182825
0.567859842882
0.889048991354
0.693049844549
0
validation finish
batch start
#iterations: 100
currently lose_sum: 564.6174222230911
time_elpased: 6.204
batch start
#iterations: 101
currently lose_sum: 562.295628786087
time_elpased: 5.892
batch start
#iterations: 102
currently lose_sum: 564.7651067376137
time_elpased: 6.031
batch start
#iterations: 103
currently lose_sum: 563.8685875535011
time_elpased: 6.047
batch start
#iterations: 104
currently lose_sum: 567.0612306296825
time_elpased: 6.118
batch start
#iterations: 105
currently lose_sum: 564.3630568385124
time_elpased: 6.213
batch start
#iterations: 106
currently lose_sum: 565.1198042035103
time_elpased: 6.28
batch start
#iterations: 107
currently lose_sum: 562.832394361496
time_elpased: 6.204
batch start
#iterations: 108
currently lose_sum: 561.5064634382725
time_elpased: 5.98
batch start
#iterations: 109
currently lose_sum: 563.9067521095276
time_elpased: 6.14
batch start
#iterations: 110
currently lose_sum: 565.3187444210052
time_elpased: 6.051
batch start
#iterations: 111
currently lose_sum: 563.3408123850822
time_elpased: 5.948
batch start
#iterations: 112
currently lose_sum: 565.1216144561768
time_elpased: 6.433
batch start
#iterations: 113
currently lose_sum: 561.3762328326702
time_elpased: 6.333
batch start
#iterations: 114
currently lose_sum: 565.7250579595566
time_elpased: 6.196
batch start
#iterations: 115
currently lose_sum: 565.935429751873
time_elpased: 6.047
batch start
#iterations: 116
currently lose_sum: 564.526467949152
time_elpased: 5.878
batch start
#iterations: 117
currently lose_sum: 564.4080328941345
time_elpased: 5.98
batch start
#iterations: 118
currently lose_sum: 565.4531573653221
time_elpased: 5.908
batch start
#iterations: 119
currently lose_sum: 563.3083431720734
time_elpased: 6.077
start validation test
0.577534626039
0.568274600738
0.895430218197
0.695290803381
0
validation finish
batch start
#iterations: 120
currently lose_sum: 563.5780474245548
time_elpased: 5.779
batch start
#iterations: 121
currently lose_sum: 562.6937280595303
time_elpased: 5.864
batch start
#iterations: 122
currently lose_sum: 562.8840111196041
time_elpased: 5.937
batch start
#iterations: 123
currently lose_sum: 562.6505077779293
time_elpased: 5.985
batch start
#iterations: 124
currently lose_sum: 563.4603129327297
time_elpased: 6.025
batch start
#iterations: 125
currently lose_sum: 564.3041910529137
time_elpased: 5.895
batch start
#iterations: 126
currently lose_sum: 564.8550934493542
time_elpased: 6.159
batch start
#iterations: 127
currently lose_sum: 561.8564890325069
time_elpased: 6.028
batch start
#iterations: 128
currently lose_sum: 563.6629077196121
time_elpased: 6.214
batch start
#iterations: 129
currently lose_sum: 563.2532832920551
time_elpased: 5.956
batch start
#iterations: 130
currently lose_sum: 563.550293982029
time_elpased: 5.967
batch start
#iterations: 131
currently lose_sum: 562.9285653233528
time_elpased: 6.116
batch start
#iterations: 132
currently lose_sum: 562.4283460378647
time_elpased: 6.282
batch start
#iterations: 133
currently lose_sum: 562.751521140337
time_elpased: 6.243
batch start
#iterations: 134
currently lose_sum: 563.1229643821716
time_elpased: 6.183
batch start
#iterations: 135
currently lose_sum: 563.5463564991951
time_elpased: 5.925
batch start
#iterations: 136
currently lose_sum: 562.4124113917351
time_elpased: 6.073
batch start
#iterations: 137
currently lose_sum: 564.2579255402088
time_elpased: 6.04
batch start
#iterations: 138
currently lose_sum: 564.4171544015408
time_elpased: 6.276
batch start
#iterations: 139
currently lose_sum: 562.2434477210045
time_elpased: 6.078
start validation test
0.575872576177
0.566806082417
0.899650061754
0.695454997514
0
validation finish
batch start
#iterations: 140
currently lose_sum: 562.2173064053059
time_elpased: 5.967
batch start
#iterations: 141
currently lose_sum: 562.8817747831345
time_elpased: 6.038
batch start
#iterations: 142
currently lose_sum: 562.6185348629951
time_elpased: 6.134
batch start
#iterations: 143
currently lose_sum: 563.9610262513161
time_elpased: 6.159
batch start
#iterations: 144
currently lose_sum: 561.8503024578094
time_elpased: 6.061
batch start
#iterations: 145
currently lose_sum: 563.2072438001633
time_elpased: 5.898
batch start
#iterations: 146
currently lose_sum: 564.3603821992874
time_elpased: 5.967
batch start
#iterations: 147
currently lose_sum: 560.946031332016
time_elpased: 5.858
batch start
#iterations: 148
currently lose_sum: 562.9419606924057
time_elpased: 6.152
batch start
#iterations: 149
currently lose_sum: 565.4549887776375
time_elpased: 6.11
batch start
#iterations: 150
currently lose_sum: 562.1513874232769
time_elpased: 6.012
batch start
#iterations: 151
currently lose_sum: 560.5469012260437
time_elpased: 6.107
batch start
#iterations: 152
currently lose_sum: 561.6625406444073
time_elpased: 5.955
batch start
#iterations: 153
currently lose_sum: 561.5761339664459
time_elpased: 6.051
batch start
#iterations: 154
currently lose_sum: 562.3942927420139
time_elpased: 6.039
batch start
#iterations: 155
currently lose_sum: 563.9048607647419
time_elpased: 6.147
batch start
#iterations: 156
currently lose_sum: 560.2588366270065
time_elpased: 5.885
batch start
#iterations: 157
currently lose_sum: 563.1877270042896
time_elpased: 6.224
batch start
#iterations: 158
currently lose_sum: 563.5536303520203
time_elpased: 6.175
batch start
#iterations: 159
currently lose_sum: 562.8724925816059
time_elpased: 6.074
start validation test
0.576454293629
0.568174336691
0.888225607246
0.693033527404
0
validation finish
batch start
#iterations: 160
currently lose_sum: 561.477256923914
time_elpased: 6.25
batch start
#iterations: 161
currently lose_sum: 562.1999860703945
time_elpased: 5.96
batch start
#iterations: 162
currently lose_sum: 562.0416661500931
time_elpased: 5.905
batch start
#iterations: 163
currently lose_sum: 562.3033775389194
time_elpased: 6.082
batch start
#iterations: 164
currently lose_sum: 563.4038326144218
time_elpased: 6.38
batch start
#iterations: 165
currently lose_sum: 561.9924342930317
time_elpased: 6.221
batch start
#iterations: 166
currently lose_sum: 563.2136380076408
time_elpased: 6.087
batch start
#iterations: 167
currently lose_sum: 561.0966556370258
time_elpased: 5.798
batch start
#iterations: 168
currently lose_sum: 562.0375017523766
time_elpased: 6.153
batch start
#iterations: 169
currently lose_sum: 561.2069287896156
time_elpased: 6.15
batch start
#iterations: 170
currently lose_sum: 563.8887774050236
time_elpased: 6.197
batch start
#iterations: 171
currently lose_sum: 560.6596016585827
time_elpased: 5.984
batch start
#iterations: 172
currently lose_sum: 561.3343917429447
time_elpased: 5.844
batch start
#iterations: 173
currently lose_sum: 561.5315471887589
time_elpased: 6.238
batch start
#iterations: 174
currently lose_sum: 562.3754235506058
time_elpased: 6.159
batch start
#iterations: 175
currently lose_sum: 562.9872463941574
time_elpased: 6.209
batch start
#iterations: 176
currently lose_sum: 561.1322546601295
time_elpased: 6.097
batch start
#iterations: 177
currently lose_sum: 562.0169970393181
time_elpased: 6.024
batch start
#iterations: 178
currently lose_sum: 563.2283533215523
time_elpased: 5.892
batch start
#iterations: 179
currently lose_sum: 564.1897099018097
time_elpased: 6.054
start validation test
0.576786703601
0.568228105906
0.890181144504
0.69366804347
0
validation finish
batch start
#iterations: 180
currently lose_sum: 559.8991843760014
time_elpased: 6.05
batch start
#iterations: 181
currently lose_sum: 558.7420865595341
time_elpased: 6.37
batch start
#iterations: 182
currently lose_sum: 563.5890846848488
time_elpased: 6.065
batch start
#iterations: 183
currently lose_sum: 563.1821611523628
time_elpased: 5.899
batch start
#iterations: 184
currently lose_sum: 562.5985491275787
time_elpased: 6.08
batch start
#iterations: 185
currently lose_sum: 561.6912703216076
time_elpased: 6.251
batch start
#iterations: 186
currently lose_sum: 563.1163248419762
time_elpased: 6.192
batch start
#iterations: 187
currently lose_sum: 560.8507725298405
time_elpased: 5.983
batch start
#iterations: 188
currently lose_sum: 563.1214627325535
time_elpased: 5.968
batch start
#iterations: 189
currently lose_sum: 561.6536013484001
time_elpased: 5.927
batch start
#iterations: 190
currently lose_sum: 560.1496961712837
time_elpased: 6.245
batch start
#iterations: 191
currently lose_sum: 561.4172085821629
time_elpased: 6.116
batch start
#iterations: 192
currently lose_sum: 561.7302632331848
time_elpased: 6.167
batch start
#iterations: 193
currently lose_sum: 562.7119262814522
time_elpased: 6.163
batch start
#iterations: 194
currently lose_sum: 563.3049901425838
time_elpased: 6.077
batch start
#iterations: 195
currently lose_sum: 562.1359040141106
time_elpased: 6.03
batch start
#iterations: 196
currently lose_sum: 562.0310625433922
time_elpased: 6.173
batch start
#iterations: 197
currently lose_sum: 564.8901607394218
time_elpased: 6.149
batch start
#iterations: 198
currently lose_sum: 561.6796760857105
time_elpased: 6.276
batch start
#iterations: 199
currently lose_sum: 559.9164129495621
time_elpased: 6.099
start validation test
0.575041551247
0.566315448209
0.898929600659
0.694870417885
0
validation finish
batch start
#iterations: 200
currently lose_sum: 562.3343895077705
time_elpased: 6.315
batch start
#iterations: 201
currently lose_sum: 561.832711815834
time_elpased: 6.0
batch start
#iterations: 202
currently lose_sum: 560.7225570678711
time_elpased: 6.018
batch start
#iterations: 203
currently lose_sum: 560.794298350811
time_elpased: 5.968
batch start
#iterations: 204
currently lose_sum: 561.5560100972652
time_elpased: 6.157
batch start
#iterations: 205
currently lose_sum: 559.5016590952873
time_elpased: 6.193
batch start
#iterations: 206
currently lose_sum: 561.4517837166786
time_elpased: 6.283
batch start
#iterations: 207
currently lose_sum: 559.5206497907639
time_elpased: 6.021
batch start
#iterations: 208
currently lose_sum: 562.7459970414639
time_elpased: 6.136
batch start
#iterations: 209
currently lose_sum: 561.9260219335556
time_elpased: 5.99
batch start
#iterations: 210
currently lose_sum: 561.7649036049843
time_elpased: 6.189
batch start
#iterations: 211
currently lose_sum: 562.0180604159832
time_elpased: 5.949
batch start
#iterations: 212
currently lose_sum: 560.7686007618904
time_elpased: 6.267
batch start
#iterations: 213
currently lose_sum: 564.0140395462513
time_elpased: 5.894
batch start
#iterations: 214
currently lose_sum: 560.0469846725464
time_elpased: 6.207
batch start
#iterations: 215
currently lose_sum: 564.0708823800087
time_elpased: 5.997
batch start
#iterations: 216
currently lose_sum: 563.8551337122917
time_elpased: 5.941
batch start
#iterations: 217
currently lose_sum: 562.2994897663593
time_elpased: 6.055
batch start
#iterations: 218
currently lose_sum: 562.3172961771488
time_elpased: 6.035
batch start
#iterations: 219
currently lose_sum: 560.3904755711555
time_elpased: 5.934
start validation test
0.576094182825
0.567083671812
0.89810621655
0.695201864282
0
validation finish
batch start
#iterations: 220
currently lose_sum: 561.2684217691422
time_elpased: 6.083
batch start
#iterations: 221
currently lose_sum: 562.4033845067024
time_elpased: 6.076
batch start
#iterations: 222
currently lose_sum: 560.6905574500561
time_elpased: 5.823
batch start
#iterations: 223
currently lose_sum: 561.0179885327816
time_elpased: 6.332
batch start
#iterations: 224
currently lose_sum: 560.6203089654446
time_elpased: 6.055
batch start
#iterations: 225
currently lose_sum: 559.272812217474
time_elpased: 6.227
batch start
#iterations: 226
currently lose_sum: 561.9540652930737
time_elpased: 5.989
batch start
#iterations: 227
currently lose_sum: 563.3514578044415
time_elpased: 5.853
batch start
#iterations: 228
currently lose_sum: 562.4571821689606
time_elpased: 6.147
batch start
#iterations: 229
currently lose_sum: 560.7570911347866
time_elpased: 5.988
batch start
#iterations: 230
currently lose_sum: 560.5655309557915
time_elpased: 6.27
batch start
#iterations: 231
currently lose_sum: 561.279055505991
time_elpased: 6.022
batch start
#iterations: 232
currently lose_sum: 565.267216771841
time_elpased: 6.008
batch start
#iterations: 233
currently lose_sum: 560.6178539395332
time_elpased: 6.08
batch start
#iterations: 234
currently lose_sum: 561.0638464391232
time_elpased: 6.023
batch start
#iterations: 235
currently lose_sum: 560.0397026240826
time_elpased: 6.044
batch start
#iterations: 236
currently lose_sum: 560.9786580204964
time_elpased: 6.095
batch start
#iterations: 237
currently lose_sum: 560.0556701719761
time_elpased: 6.432
batch start
#iterations: 238
currently lose_sum: 560.8725401163101
time_elpased: 6.23
batch start
#iterations: 239
currently lose_sum: 563.191660284996
time_elpased: 6.24
start validation test
0.575872576177
0.567259670312
0.894298065047
0.694189785687
0
validation finish
batch start
#iterations: 240
currently lose_sum: 561.2089709937572
time_elpased: 6.022
batch start
#iterations: 241
currently lose_sum: 562.7694057524204
time_elpased: 6.076
batch start
#iterations: 242
currently lose_sum: 560.6602508723736
time_elpased: 6.359
batch start
#iterations: 243
currently lose_sum: 559.7891865968704
time_elpased: 6.11
batch start
#iterations: 244
currently lose_sum: 560.9865257143974
time_elpased: 6.204
batch start
#iterations: 245
currently lose_sum: 561.6304281055927
time_elpased: 6.245
batch start
#iterations: 246
currently lose_sum: 559.6907019615173
time_elpased: 6.203
batch start
#iterations: 247
currently lose_sum: 560.2615265548229
time_elpased: 5.976
batch start
#iterations: 248
currently lose_sum: 562.1464374959469
time_elpased: 6.082
batch start
#iterations: 249
currently lose_sum: 563.4020928442478
time_elpased: 6.044
batch start
#iterations: 250
currently lose_sum: 559.2650173008442
time_elpased: 6.257
batch start
#iterations: 251
currently lose_sum: 561.7847325503826
time_elpased: 5.92
batch start
#iterations: 252
currently lose_sum: 562.5653595924377
time_elpased: 5.794
batch start
#iterations: 253
currently lose_sum: 561.9013848900795
time_elpased: 6.239
batch start
#iterations: 254
currently lose_sum: 560.6959426403046
time_elpased: 6.018
batch start
#iterations: 255
currently lose_sum: 561.2479496598244
time_elpased: 6.084
batch start
#iterations: 256
currently lose_sum: 560.6238941252232
time_elpased: 5.882
batch start
#iterations: 257
currently lose_sum: 562.1490783393383
time_elpased: 6.004
batch start
#iterations: 258
currently lose_sum: 561.6222117245197
time_elpased: 6.12
batch start
#iterations: 259
currently lose_sum: 561.0875302255154
time_elpased: 5.995
start validation test
0.575429362881
0.566676412305
0.897694524496
0.694772478343
0
validation finish
batch start
#iterations: 260
currently lose_sum: 560.2533445954323
time_elpased: 6.09
batch start
#iterations: 261
currently lose_sum: 556.8111547231674
time_elpased: 6.153
batch start
#iterations: 262
currently lose_sum: 558.6760478019714
time_elpased: 5.815
batch start
#iterations: 263
currently lose_sum: 561.3774546384811
time_elpased: 5.921
batch start
#iterations: 264
currently lose_sum: 557.0813578665257
time_elpased: 6.021
batch start
#iterations: 265
currently lose_sum: 557.7917982637882
time_elpased: 5.999
batch start
#iterations: 266
currently lose_sum: 560.9076954722404
time_elpased: 6.044
batch start
#iterations: 267
currently lose_sum: 560.2470442950726
time_elpased: 6.145
batch start
#iterations: 268
currently lose_sum: 560.22216334939
time_elpased: 6.077
batch start
#iterations: 269
currently lose_sum: 559.5879659056664
time_elpased: 6.14
batch start
#iterations: 270
currently lose_sum: 559.8902203142643
time_elpased: 6.035
batch start
#iterations: 271
currently lose_sum: 558.3939600586891
time_elpased: 6.024
batch start
#iterations: 272
currently lose_sum: 562.1144569814205
time_elpased: 5.949
batch start
#iterations: 273
currently lose_sum: 560.1247247457504
time_elpased: 6.193
batch start
#iterations: 274
currently lose_sum: 557.8958441317081
time_elpased: 6.167
batch start
#iterations: 275
currently lose_sum: 560.7851639091969
time_elpased: 6.027
batch start
#iterations: 276
currently lose_sum: 560.4499078691006
time_elpased: 5.948
batch start
#iterations: 277
currently lose_sum: 559.095168530941
time_elpased: 6.159
batch start
#iterations: 278
currently lose_sum: 561.6435117721558
time_elpased: 6.365
batch start
#iterations: 279
currently lose_sum: 557.8432810008526
time_elpased: 6.212
start validation test
0.576260387812
0.567202990411
0.898003293536
0.695260672949
0
validation finish
batch start
#iterations: 280
currently lose_sum: 560.3366049230099
time_elpased: 6.151
batch start
#iterations: 281
currently lose_sum: 559.7465541362762
time_elpased: 6.114
batch start
#iterations: 282
currently lose_sum: 560.3305230140686
time_elpased: 5.829
batch start
#iterations: 283
currently lose_sum: 559.3027146458626
time_elpased: 6.178
batch start
#iterations: 284
currently lose_sum: 561.3572318255901
time_elpased: 6.242
batch start
#iterations: 285
currently lose_sum: 558.6921769976616
time_elpased: 6.089
batch start
#iterations: 286
currently lose_sum: 558.1597946882248
time_elpased: 6.059
batch start
#iterations: 287
currently lose_sum: 561.6479273438454
time_elpased: 5.831
batch start
#iterations: 288
currently lose_sum: 558.8302119076252
time_elpased: 5.829
batch start
#iterations: 289
currently lose_sum: 561.531153678894
time_elpased: 6.259
batch start
#iterations: 290
currently lose_sum: 559.7445498108864
time_elpased: 6.174
batch start
#iterations: 291
currently lose_sum: 558.877125620842
time_elpased: 6.082
batch start
#iterations: 292
currently lose_sum: 560.2143519222736
time_elpased: 6.239
batch start
#iterations: 293
currently lose_sum: 558.6783077716827
time_elpased: 6.253
batch start
#iterations: 294
currently lose_sum: 561.1988928318024
time_elpased: 6.268
batch start
#iterations: 295
currently lose_sum: 560.9540269374847
time_elpased: 6.168
batch start
#iterations: 296
currently lose_sum: 559.1563631594181
time_elpased: 5.934
batch start
#iterations: 297
currently lose_sum: 559.5704941153526
time_elpased: 6.197
batch start
#iterations: 298
currently lose_sum: 562.5856585800648
time_elpased: 6.243
batch start
#iterations: 299
currently lose_sum: 560.1401950418949
time_elpased: 6.123
start validation test
0.576537396122
0.567431267285
0.897488678469
0.695277772241
0
validation finish
batch start
#iterations: 300
currently lose_sum: 560.9360485076904
time_elpased: 6.109
batch start
#iterations: 301
currently lose_sum: 559.5108688771725
time_elpased: 6.123
batch start
#iterations: 302
currently lose_sum: 560.0169875621796
time_elpased: 6.017
batch start
#iterations: 303
currently lose_sum: 560.5391523838043
time_elpased: 6.18
batch start
#iterations: 304
currently lose_sum: 560.5914117693901
time_elpased: 5.867
batch start
#iterations: 305
currently lose_sum: 561.1906186640263
time_elpased: 6.048
batch start
#iterations: 306
currently lose_sum: 561.31376054883
time_elpased: 6.008
batch start
#iterations: 307
currently lose_sum: 561.7529877722263
time_elpased: 6.626
batch start
#iterations: 308
currently lose_sum: 560.9879197180271
time_elpased: 6.303
batch start
#iterations: 309
currently lose_sum: 558.1907421946526
time_elpased: 6.089
batch start
#iterations: 310
currently lose_sum: 561.0340619683266
time_elpased: 6.091
batch start
#iterations: 311
currently lose_sum: 560.4547275006771
time_elpased: 6.227
batch start
#iterations: 312
currently lose_sum: 560.3042260706425
time_elpased: 6.148
batch start
#iterations: 313
currently lose_sum: 559.656650185585
time_elpased: 6.114
batch start
#iterations: 314
currently lose_sum: 560.1510286927223
time_elpased: 5.998
batch start
#iterations: 315
currently lose_sum: 560.3082539737225
time_elpased: 6.101
batch start
#iterations: 316
currently lose_sum: 559.8922037780285
time_elpased: 6.019
batch start
#iterations: 317
currently lose_sum: 560.7709225714207
time_elpased: 5.922
batch start
#iterations: 318
currently lose_sum: 560.8884331882
time_elpased: 5.887
batch start
#iterations: 319
currently lose_sum: 560.6522586941719
time_elpased: 6.134
start validation test
0.576537396122
0.567282407558
0.899238369699
0.695690255798
0
validation finish
batch start
#iterations: 320
currently lose_sum: 559.5070967376232
time_elpased: 5.994
batch start
#iterations: 321
currently lose_sum: 560.0226245820522
time_elpased: 6.225
batch start
#iterations: 322
currently lose_sum: 562.7977633476257
time_elpased: 6.309
batch start
#iterations: 323
currently lose_sum: 560.0187219977379
time_elpased: 6.153
batch start
#iterations: 324
currently lose_sum: 560.5885161459446
time_elpased: 6.168
batch start
#iterations: 325
currently lose_sum: 560.2437440156937
time_elpased: 5.782
batch start
#iterations: 326
currently lose_sum: 558.0542589128017
time_elpased: 6.113
batch start
#iterations: 327
currently lose_sum: 559.7235952317715
time_elpased: 6.341
batch start
#iterations: 328
currently lose_sum: 559.94901740551
time_elpased: 6.11
batch start
#iterations: 329
currently lose_sum: 559.9968527257442
time_elpased: 6.058
batch start
#iterations: 330
currently lose_sum: 558.3000577688217
time_elpased: 5.833
batch start
#iterations: 331
currently lose_sum: 557.9931078851223
time_elpased: 6.387
batch start
#iterations: 332
currently lose_sum: 559.7849828004837
time_elpased: 6.125
batch start
#iterations: 333
currently lose_sum: 561.0195551514626
time_elpased: 6.228
batch start
#iterations: 334
currently lose_sum: 561.2234439253807
time_elpased: 6.355
batch start
#iterations: 335
currently lose_sum: 559.3569535017014
time_elpased: 6.023
batch start
#iterations: 336
currently lose_sum: 559.839719504118
time_elpased: 5.999
batch start
#iterations: 337
currently lose_sum: 558.4951760172844
time_elpased: 5.997
batch start
#iterations: 338
currently lose_sum: 561.3875127732754
time_elpased: 5.797
batch start
#iterations: 339
currently lose_sum: 559.1839214265347
time_elpased: 6.244
start validation test
0.576426592798
0.567173863673
0.899650061754
0.695731767983
0
validation finish
batch start
#iterations: 340
currently lose_sum: 560.9358622133732
time_elpased: 5.96
batch start
#iterations: 341
currently lose_sum: 559.892916738987
time_elpased: 6.012
batch start
#iterations: 342
currently lose_sum: 558.7797314524651
time_elpased: 6.058
batch start
#iterations: 343
currently lose_sum: 560.8369441330433
time_elpased: 5.998
batch start
#iterations: 344
currently lose_sum: 556.211657345295
time_elpased: 6.171
batch start
#iterations: 345
currently lose_sum: 559.7859613001347
time_elpased: 6.144
batch start
#iterations: 346
currently lose_sum: 560.5919369161129
time_elpased: 6.37
batch start
#iterations: 347
currently lose_sum: 560.6495724618435
time_elpased: 6.373
batch start
#iterations: 348
currently lose_sum: 559.189763635397
time_elpased: 5.979
batch start
#iterations: 349
currently lose_sum: 557.4622358381748
time_elpased: 6.169
batch start
#iterations: 350
currently lose_sum: 560.8812602162361
time_elpased: 6.023
batch start
#iterations: 351
currently lose_sum: 559.2609586119652
time_elpased: 6.182
batch start
#iterations: 352
currently lose_sum: 559.488402068615
time_elpased: 5.942
batch start
#iterations: 353
currently lose_sum: 558.1834530234337
time_elpased: 6.379
batch start
#iterations: 354
currently lose_sum: 560.0172017812729
time_elpased: 5.94
batch start
#iterations: 355
currently lose_sum: 558.5884543061256
time_elpased: 6.226
batch start
#iterations: 356
currently lose_sum: 561.6495542526245
time_elpased: 5.992
batch start
#iterations: 357
currently lose_sum: 558.5356720387936
time_elpased: 6.058
batch start
#iterations: 358
currently lose_sum: 559.293009519577
time_elpased: 5.963
batch start
#iterations: 359
currently lose_sum: 561.9827263355255
time_elpased: 6.005
start validation test
0.575512465374
0.566888107334
0.895841910251
0.694375747906
0
validation finish
batch start
#iterations: 360
currently lose_sum: 561.9965956509113
time_elpased: 6.029
batch start
#iterations: 361
currently lose_sum: 561.8535078167915
time_elpased: 6.201
batch start
#iterations: 362
currently lose_sum: 560.779721647501
time_elpased: 5.965
batch start
#iterations: 363
currently lose_sum: 559.5952451229095
time_elpased: 6.212
batch start
#iterations: 364
currently lose_sum: 559.7107829451561
time_elpased: 5.901
batch start
#iterations: 365
currently lose_sum: 557.0176655650139
time_elpased: 6.124
batch start
#iterations: 366
currently lose_sum: 559.2980243563652
time_elpased: 6.268
batch start
#iterations: 367
currently lose_sum: 559.6993333697319
time_elpased: 5.901
batch start
#iterations: 368
currently lose_sum: 561.32385841012
time_elpased: 5.783
batch start
#iterations: 369
currently lose_sum: 561.1744809448719
time_elpased: 6.114
batch start
#iterations: 370
currently lose_sum: 558.6073046028614
time_elpased: 6.068
batch start
#iterations: 371
currently lose_sum: 559.9018488526344
time_elpased: 6.079
batch start
#iterations: 372
currently lose_sum: 558.2001013159752
time_elpased: 5.852
batch start
#iterations: 373
currently lose_sum: 560.1965491473675
time_elpased: 5.945
batch start
#iterations: 374
currently lose_sum: 558.5253429412842
time_elpased: 6.116
batch start
#iterations: 375
currently lose_sum: 561.3953967094421
time_elpased: 5.97
batch start
#iterations: 376
currently lose_sum: 560.2891575098038
time_elpased: 5.915
batch start
#iterations: 377
currently lose_sum: 558.9617099761963
time_elpased: 6.093
batch start
#iterations: 378
currently lose_sum: 559.1755802333355
time_elpased: 6.316
batch start
#iterations: 379
currently lose_sum: 561.2391704916954
time_elpased: 6.188
start validation test
0.57620498615
0.56734999837
0.895841910251
0.69472213908
0
validation finish
batch start
#iterations: 380
currently lose_sum: 556.7674233019352
time_elpased: 5.974
batch start
#iterations: 381
currently lose_sum: 559.9784646034241
time_elpased: 6.069
batch start
#iterations: 382
currently lose_sum: 559.4721224606037
time_elpased: 6.038
batch start
#iterations: 383
currently lose_sum: 560.1907219588757
time_elpased: 6.381
batch start
#iterations: 384
currently lose_sum: 558.4556718468666
time_elpased: 6.105
batch start
#iterations: 385
currently lose_sum: 557.699480265379
time_elpased: 6.114
batch start
#iterations: 386
currently lose_sum: 560.8995981812477
time_elpased: 6.07
batch start
#iterations: 387
currently lose_sum: 559.2438410818577
time_elpased: 5.764
batch start
#iterations: 388
currently lose_sum: 559.1230864226818
time_elpased: 6.085
batch start
#iterations: 389
currently lose_sum: 559.0569362938404
time_elpased: 6.143
batch start
#iterations: 390
currently lose_sum: 560.0477608144283
time_elpased: 6.239
batch start
#iterations: 391
currently lose_sum: 561.6410363018513
time_elpased: 6.16
batch start
#iterations: 392
currently lose_sum: 557.8606239259243
time_elpased: 6.134
batch start
#iterations: 393
currently lose_sum: 556.2462684214115
time_elpased: 6.023
batch start
#iterations: 394
currently lose_sum: 558.4113842248917
time_elpased: 5.941
batch start
#iterations: 395
currently lose_sum: 558.0597366988659
time_elpased: 6.14
batch start
#iterations: 396
currently lose_sum: 559.3056643605232
time_elpased: 6.049
batch start
#iterations: 397
currently lose_sum: 558.8567069768906
time_elpased: 6.091
batch start
#iterations: 398
currently lose_sum: 558.7605295181274
time_elpased: 6.157
batch start
#iterations: 399
currently lose_sum: 561.3051756620407
time_elpased: 6.232
start validation test
0.576260387812
0.567246706782
0.897488678469
0.695139207206
0
validation finish
acc: 0.581
pre: 0.569
rec: 0.901
F1: 0.697
auc: 0.000
