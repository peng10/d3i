start to construct graph
graph construct over
93366
epochs start
batch start
#iterations: 0
currently lose_sum: 610.8358697295189
time_elpased: 5.444
batch start
#iterations: 1
currently lose_sum: 600.2394561171532
time_elpased: 5.138
batch start
#iterations: 2
currently lose_sum: 593.9736012816429
time_elpased: 5.477
batch start
#iterations: 3
currently lose_sum: 589.5506189465523
time_elpased: 5.284
batch start
#iterations: 4
currently lose_sum: 586.9002376794815
time_elpased: 5.311
batch start
#iterations: 5
currently lose_sum: 585.3020251691341
time_elpased: 5.271
batch start
#iterations: 6
currently lose_sum: 584.475773692131
time_elpased: 5.385
batch start
#iterations: 7
currently lose_sum: 583.8445654511452
time_elpased: 5.358
batch start
#iterations: 8
currently lose_sum: 578.9766156673431
time_elpased: 5.284
batch start
#iterations: 9
currently lose_sum: 581.6229588389397
time_elpased: 5.272
batch start
#iterations: 10
currently lose_sum: 579.4117643237114
time_elpased: 5.345
batch start
#iterations: 11
currently lose_sum: 579.3653776049614
time_elpased: 5.281
batch start
#iterations: 12
currently lose_sum: 576.6406873464584
time_elpased: 5.27
batch start
#iterations: 13
currently lose_sum: 579.4266927242279
time_elpased: 5.337
batch start
#iterations: 14
currently lose_sum: 577.5865569114685
time_elpased: 5.487
batch start
#iterations: 15
currently lose_sum: 576.8920404911041
time_elpased: 5.38
batch start
#iterations: 16
currently lose_sum: 574.3538081049919
time_elpased: 5.308
batch start
#iterations: 17
currently lose_sum: 577.4591543972492
time_elpased: 5.487
batch start
#iterations: 18
currently lose_sum: 575.6422688663006
time_elpased: 5.449
batch start
#iterations: 19
currently lose_sum: 577.811889320612
time_elpased: 5.302
start validation test
0.578476454294
0.568952549135
0.895235154883
0.695739107832
0
validation finish
batch start
#iterations: 20
currently lose_sum: 575.6443530321121
time_elpased: 5.562
batch start
#iterations: 21
currently lose_sum: 576.3608014583588
time_elpased: 5.549
batch start
#iterations: 22
currently lose_sum: 576.0539953112602
time_elpased: 5.602
batch start
#iterations: 23
currently lose_sum: 574.9889279007912
time_elpased: 5.245
batch start
#iterations: 24
currently lose_sum: 575.0421240031719
time_elpased: 5.452
batch start
#iterations: 25
currently lose_sum: 575.7191709280014
time_elpased: 5.367
batch start
#iterations: 26
currently lose_sum: 572.8645151257515
time_elpased: 5.276
batch start
#iterations: 27
currently lose_sum: 572.812995404005
time_elpased: 5.418
batch start
#iterations: 28
currently lose_sum: 572.1165197491646
time_elpased: 5.32
batch start
#iterations: 29
currently lose_sum: 574.0804550647736
time_elpased: 5.364
batch start
#iterations: 30
currently lose_sum: 572.9786590337753
time_elpased: 5.194
batch start
#iterations: 31
currently lose_sum: 572.7796433269978
time_elpased: 5.17
batch start
#iterations: 32
currently lose_sum: 572.1860348284245
time_elpased: 5.243
batch start
#iterations: 33
currently lose_sum: 570.3020912110806
time_elpased: 5.396
batch start
#iterations: 34
currently lose_sum: 572.2874093949795
time_elpased: 5.361
batch start
#iterations: 35
currently lose_sum: 572.037118434906
time_elpased: 5.254
batch start
#iterations: 36
currently lose_sum: 571.8745465278625
time_elpased: 5.162
batch start
#iterations: 37
currently lose_sum: 571.1283006966114
time_elpased: 5.261
batch start
#iterations: 38
currently lose_sum: 572.5430180430412
time_elpased: 5.155
batch start
#iterations: 39
currently lose_sum: 572.8131913542747
time_elpased: 5.296
start validation test
0.58055401662
0.570545693623
0.893073994031
0.696273117503
0
validation finish
batch start
#iterations: 40
currently lose_sum: 570.8669358193874
time_elpased: 5.242
batch start
#iterations: 41
currently lose_sum: 568.9067868590355
time_elpased: 5.204
batch start
#iterations: 42
currently lose_sum: 571.7559106349945
time_elpased: 5.505
batch start
#iterations: 43
currently lose_sum: 569.2536588311195
time_elpased: 5.416
batch start
#iterations: 44
currently lose_sum: 571.3860905170441
time_elpased: 5.245
batch start
#iterations: 45
currently lose_sum: 569.6179187893867
time_elpased: 5.158
batch start
#iterations: 46
currently lose_sum: 571.1363921165466
time_elpased: 5.255
batch start
#iterations: 47
currently lose_sum: 569.8539010882378
time_elpased: 5.347
batch start
#iterations: 48
currently lose_sum: 571.5022400319576
time_elpased: 5.402
batch start
#iterations: 49
currently lose_sum: 570.9131635427475
time_elpased: 5.313
batch start
#iterations: 50
currently lose_sum: 569.6647272109985
time_elpased: 5.329
batch start
#iterations: 51
currently lose_sum: 567.7446985244751
time_elpased: 5.603
batch start
#iterations: 52
currently lose_sum: 570.4008425176144
time_elpased: 5.279
batch start
#iterations: 53
currently lose_sum: 568.9414796233177
time_elpased: 5.193
batch start
#iterations: 54
currently lose_sum: 569.3667864501476
time_elpased: 5.359
batch start
#iterations: 55
currently lose_sum: 569.888780683279
time_elpased: 5.345
batch start
#iterations: 56
currently lose_sum: 569.1372276842594
time_elpased: 5.154
batch start
#iterations: 57
currently lose_sum: 567.0990508198738
time_elpased: 5.198
batch start
#iterations: 58
currently lose_sum: 571.2245497703552
time_elpased: 5.137
batch start
#iterations: 59
currently lose_sum: 567.3159832954407
time_elpased: 5.284
start validation test
0.581052631579
0.570753168297
0.894514767932
0.696865228894
0
validation finish
batch start
#iterations: 60
currently lose_sum: 569.1009084880352
time_elpased: 5.29
batch start
#iterations: 61
currently lose_sum: 567.0455664396286
time_elpased: 5.279
batch start
#iterations: 62
currently lose_sum: 570.3853279054165
time_elpased: 5.231
batch start
#iterations: 63
currently lose_sum: 568.2396378517151
time_elpased: 5.623
batch start
#iterations: 64
currently lose_sum: 566.0674270987511
time_elpased: 5.259
batch start
#iterations: 65
currently lose_sum: 567.0352708697319
time_elpased: 5.334
batch start
#iterations: 66
currently lose_sum: 568.1071718335152
time_elpased: 5.198
batch start
#iterations: 67
currently lose_sum: 568.1892323195934
time_elpased: 5.376
batch start
#iterations: 68
currently lose_sum: 565.8847145736217
time_elpased: 5.367
batch start
#iterations: 69
currently lose_sum: 569.1170980632305
time_elpased: 5.319
batch start
#iterations: 70
currently lose_sum: 568.6435419023037
time_elpased: 5.285
batch start
#iterations: 71
currently lose_sum: 568.3659285008907
time_elpased: 5.313
batch start
#iterations: 72
currently lose_sum: 567.4487248659134
time_elpased: 5.381
batch start
#iterations: 73
currently lose_sum: 568.1734001636505
time_elpased: 5.296
batch start
#iterations: 74
currently lose_sum: 566.7571759223938
time_elpased: 5.174
batch start
#iterations: 75
currently lose_sum: 566.5865640342236
time_elpased: 5.465
batch start
#iterations: 76
currently lose_sum: 566.898501008749
time_elpased: 5.403
batch start
#iterations: 77
currently lose_sum: 562.5048524737358
time_elpased: 5.292
batch start
#iterations: 78
currently lose_sum: 566.8780127763748
time_elpased: 5.142
batch start
#iterations: 79
currently lose_sum: 566.6548637449741
time_elpased: 5.279
start validation test
0.580581717452
0.571210562983
0.885973036946
0.694596284567
0
validation finish
batch start
#iterations: 80
currently lose_sum: 568.3587355017662
time_elpased: 5.315
batch start
#iterations: 81
currently lose_sum: 566.0567158460617
time_elpased: 5.441
batch start
#iterations: 82
currently lose_sum: 567.7453590631485
time_elpased: 5.401
batch start
#iterations: 83
currently lose_sum: 566.3267527222633
time_elpased: 5.346
batch start
#iterations: 84
currently lose_sum: 564.2765761911869
time_elpased: 5.32
batch start
#iterations: 85
currently lose_sum: 567.7740870118141
time_elpased: 5.051
batch start
#iterations: 86
currently lose_sum: 567.0737320780754
time_elpased: 5.151
batch start
#iterations: 87
currently lose_sum: 565.9421457648277
time_elpased: 5.297
batch start
#iterations: 88
currently lose_sum: 568.557948499918
time_elpased: 5.558
batch start
#iterations: 89
currently lose_sum: 564.7721113562584
time_elpased: 5.154
batch start
#iterations: 90
currently lose_sum: 565.6014844179153
time_elpased: 5.226
batch start
#iterations: 91
currently lose_sum: 566.321057677269
time_elpased: 5.533
batch start
#iterations: 92
currently lose_sum: 564.2307880222797
time_elpased: 5.077
batch start
#iterations: 93
currently lose_sum: 568.0709535181522
time_elpased: 5.36
batch start
#iterations: 94
currently lose_sum: 565.7736869156361
time_elpased: 5.319
batch start
#iterations: 95
currently lose_sum: 563.8392559885979
time_elpased: 5.353
batch start
#iterations: 96
currently lose_sum: 564.225410014391
time_elpased: 5.52
batch start
#iterations: 97
currently lose_sum: 565.003770917654
time_elpased: 5.302
batch start
#iterations: 98
currently lose_sum: 564.6075397133827
time_elpased: 5.308
batch start
#iterations: 99
currently lose_sum: 565.6730441451073
time_elpased: 5.363
start validation test
0.578476454294
0.568880467806
0.896058454255
0.695933659706
0
validation finish
batch start
#iterations: 100
currently lose_sum: 563.8399401307106
time_elpased: 5.265
batch start
#iterations: 101
currently lose_sum: 566.3040534257889
time_elpased: 5.417
batch start
#iterations: 102
currently lose_sum: 563.9608666300774
time_elpased: 5.312
batch start
#iterations: 103
currently lose_sum: 567.3931875526905
time_elpased: 5.247
batch start
#iterations: 104
currently lose_sum: 567.1727938055992
time_elpased: 5.328
batch start
#iterations: 105
currently lose_sum: 564.7084684967995
time_elpased: 5.087
batch start
#iterations: 106
currently lose_sum: 566.7407472133636
time_elpased: 5.671
batch start
#iterations: 107
currently lose_sum: 564.8210031688213
time_elpased: 5.247
batch start
#iterations: 108
currently lose_sum: 566.7610291838646
time_elpased: 5.291
batch start
#iterations: 109
currently lose_sum: 565.7060285806656
time_elpased: 5.17
batch start
#iterations: 110
currently lose_sum: 564.924860060215
time_elpased: 5.474
batch start
#iterations: 111
currently lose_sum: 565.2752408385277
time_elpased: 5.46
batch start
#iterations: 112
currently lose_sum: 565.9609983563423
time_elpased: 5.185
batch start
#iterations: 113
currently lose_sum: 565.8280571401119
time_elpased: 5.495
batch start
#iterations: 114
currently lose_sum: 564.2400717437267
time_elpased: 5.14
batch start
#iterations: 115
currently lose_sum: 565.3580492436886
time_elpased: 5.337
batch start
#iterations: 116
currently lose_sum: 567.2883231639862
time_elpased: 4.91
batch start
#iterations: 117
currently lose_sum: 564.9370725154877
time_elpased: 5.381
batch start
#iterations: 118
currently lose_sum: 564.1841746270657
time_elpased: 5.26
batch start
#iterations: 119
currently lose_sum: 565.3554089069366
time_elpased: 5.207
start validation test
0.580415512465
0.570230333213
0.895543892148
0.696787108398
0
validation finish
batch start
#iterations: 120
currently lose_sum: 566.2205415964127
time_elpased: 5.427
batch start
#iterations: 121
currently lose_sum: 565.1203566193581
time_elpased: 5.202
batch start
#iterations: 122
currently lose_sum: 564.8208528459072
time_elpased: 5.295
batch start
#iterations: 123
currently lose_sum: 566.2180091440678
time_elpased: 5.476
batch start
#iterations: 124
currently lose_sum: 565.4197134673595
time_elpased: 5.352
batch start
#iterations: 125
currently lose_sum: 565.0678403377533
time_elpased: 5.254
batch start
#iterations: 126
currently lose_sum: 565.8756039142609
time_elpased: 5.227
batch start
#iterations: 127
currently lose_sum: 564.4337977170944
time_elpased: 5.328
batch start
#iterations: 128
currently lose_sum: 565.6615301072598
time_elpased: 5.428
batch start
#iterations: 129
currently lose_sum: 564.9420820772648
time_elpased: 5.216
batch start
#iterations: 130
currently lose_sum: 564.8917074203491
time_elpased: 5.316
batch start
#iterations: 131
currently lose_sum: 565.6592600941658
time_elpased: 5.582
batch start
#iterations: 132
currently lose_sum: 565.0561039745808
time_elpased: 5.409
batch start
#iterations: 133
currently lose_sum: 562.9550402462482
time_elpased: 5.391
batch start
#iterations: 134
currently lose_sum: 564.4588992297649
time_elpased: 5.279
batch start
#iterations: 135
currently lose_sum: 565.4492801129818
time_elpased: 5.408
batch start
#iterations: 136
currently lose_sum: 564.2457953691483
time_elpased: 5.342
batch start
#iterations: 137
currently lose_sum: 565.1941224336624
time_elpased: 5.386
batch start
#iterations: 138
currently lose_sum: 565.881295144558
time_elpased: 5.488
batch start
#iterations: 139
currently lose_sum: 564.1698330938816
time_elpased: 5.291
start validation test
0.580415512465
0.570424154812
0.893382731296
0.696276393094
0
validation finish
batch start
#iterations: 140
currently lose_sum: 563.280951321125
time_elpased: 5.235
batch start
#iterations: 141
currently lose_sum: 563.7273112535477
time_elpased: 5.459
batch start
#iterations: 142
currently lose_sum: 564.9149473905563
time_elpased: 5.24
batch start
#iterations: 143
currently lose_sum: 565.2821395993233
time_elpased: 5.319
batch start
#iterations: 144
currently lose_sum: 564.5075576305389
time_elpased: 5.447
batch start
#iterations: 145
currently lose_sum: 564.1882904171944
time_elpased: 5.42
batch start
#iterations: 146
currently lose_sum: 563.630044490099
time_elpased: 5.331
batch start
#iterations: 147
currently lose_sum: 562.2181766033173
time_elpased: 5.574
batch start
#iterations: 148
currently lose_sum: 565.3222511410713
time_elpased: 5.483
batch start
#iterations: 149
currently lose_sum: 563.9306016862392
time_elpased: 5.245
batch start
#iterations: 150
currently lose_sum: 565.348981410265
time_elpased: 5.64
batch start
#iterations: 151
currently lose_sum: 563.8176332116127
time_elpased: 5.178
batch start
#iterations: 152
currently lose_sum: 566.4783181250095
time_elpased: 5.302
batch start
#iterations: 153
currently lose_sum: 562.2841050326824
time_elpased: 5.295
batch start
#iterations: 154
currently lose_sum: 563.9280289411545
time_elpased: 5.216
batch start
#iterations: 155
currently lose_sum: 565.6672194898129
time_elpased: 5.382
batch start
#iterations: 156
currently lose_sum: 565.6628024578094
time_elpased: 5.292
batch start
#iterations: 157
currently lose_sum: 564.3027118742466
time_elpased: 5.41
batch start
#iterations: 158
currently lose_sum: 565.9738975465298
time_elpased: 5.43
batch start
#iterations: 159
currently lose_sum: 563.6272933781147
time_elpased: 5.044
start validation test
0.579085872576
0.569745631643
0.890912833179
0.695020372117
0
validation finish
batch start
#iterations: 160
currently lose_sum: 564.9092211127281
time_elpased: 5.267
batch start
#iterations: 161
currently lose_sum: 565.1281740069389
time_elpased: 5.265
batch start
#iterations: 162
currently lose_sum: 564.1350477337837
time_elpased: 5.215
batch start
#iterations: 163
currently lose_sum: 562.9188939034939
time_elpased: 5.005
batch start
#iterations: 164
currently lose_sum: 567.0113290250301
time_elpased: 5.215
batch start
#iterations: 165
currently lose_sum: 565.3278228342533
time_elpased: 5.471
batch start
#iterations: 166
currently lose_sum: 564.1140676140785
time_elpased: 5.281
batch start
#iterations: 167
currently lose_sum: 566.0428553521633
time_elpased: 5.482
batch start
#iterations: 168
currently lose_sum: 564.2722637653351
time_elpased: 5.514
batch start
#iterations: 169
currently lose_sum: 564.1888111531734
time_elpased: 5.449
batch start
#iterations: 170
currently lose_sum: 564.0758237838745
time_elpased: 5.448
batch start
#iterations: 171
currently lose_sum: 563.7584199607372
time_elpased: 5.36
batch start
#iterations: 172
currently lose_sum: 562.7824469208717
time_elpased: 5.17
batch start
#iterations: 173
currently lose_sum: 562.0663750767708
time_elpased: 5.083
batch start
#iterations: 174
currently lose_sum: 562.8305777907372
time_elpased: 5.159
batch start
#iterations: 175
currently lose_sum: 563.7258240878582
time_elpased: 5.393
batch start
#iterations: 176
currently lose_sum: 564.4069157838821
time_elpased: 5.446
batch start
#iterations: 177
currently lose_sum: 565.8197484016418
time_elpased: 5.293
batch start
#iterations: 178
currently lose_sum: 563.2139128446579
time_elpased: 5.457
batch start
#iterations: 179
currently lose_sum: 563.288740247488
time_elpased: 5.377
start validation test
0.579501385042
0.570110092953
0.889986621385
0.695009242144
0
validation finish
batch start
#iterations: 180
currently lose_sum: 563.3845385015011
time_elpased: 5.234
batch start
#iterations: 181
currently lose_sum: 564.6573751866817
time_elpased: 5.416
batch start
#iterations: 182
currently lose_sum: 564.0328360199928
time_elpased: 5.409
batch start
#iterations: 183
currently lose_sum: 564.336891502142
time_elpased: 5.242
batch start
#iterations: 184
currently lose_sum: 563.6665306091309
time_elpased: 5.2
batch start
#iterations: 185
currently lose_sum: 566.1393302977085
time_elpased: 5.474
batch start
#iterations: 186
currently lose_sum: 563.3163533508778
time_elpased: 5.359
batch start
#iterations: 187
currently lose_sum: 564.6399813592434
time_elpased: 5.272
batch start
#iterations: 188
currently lose_sum: 561.0770489275455
time_elpased: 5.267
batch start
#iterations: 189
currently lose_sum: 562.7120751738548
time_elpased: 5.462
batch start
#iterations: 190
currently lose_sum: 564.9796127676964
time_elpased: 5.325
batch start
#iterations: 191
currently lose_sum: 564.3374778032303
time_elpased: 5.108
batch start
#iterations: 192
currently lose_sum: 563.6510172486305
time_elpased: 5.382
batch start
#iterations: 193
currently lose_sum: 563.18756249547
time_elpased: 5.651
batch start
#iterations: 194
currently lose_sum: 562.1290084123611
time_elpased: 5.139
batch start
#iterations: 195
currently lose_sum: 562.0701377987862
time_elpased: 5.436
batch start
#iterations: 196
currently lose_sum: 563.7837433516979
time_elpased: 5.352
batch start
#iterations: 197
currently lose_sum: 564.1069889068604
time_elpased: 4.957
batch start
#iterations: 198
currently lose_sum: 562.7046807706356
time_elpased: 5.634
batch start
#iterations: 199
currently lose_sum: 561.7313773036003
time_elpased: 5.281
start validation test
0.578448753463
0.569033272203
0.894103118246
0.695457274365
0
validation finish
batch start
#iterations: 200
currently lose_sum: 563.4816393852234
time_elpased: 5.611
batch start
#iterations: 201
currently lose_sum: 563.1979591548443
time_elpased: 5.24
batch start
#iterations: 202
currently lose_sum: 563.8393010497093
time_elpased: 5.248
batch start
#iterations: 203
currently lose_sum: 563.5974026024342
time_elpased: 5.326
batch start
#iterations: 204
currently lose_sum: 562.6533868014812
time_elpased: 5.405
batch start
#iterations: 205
currently lose_sum: 562.9396550357342
time_elpased: 5.435
batch start
#iterations: 206
currently lose_sum: 562.1727200448513
time_elpased: 5.308
batch start
#iterations: 207
currently lose_sum: 566.3424849510193
time_elpased: 5.214
batch start
#iterations: 208
currently lose_sum: 563.2258846759796
time_elpased: 5.333
batch start
#iterations: 209
currently lose_sum: 561.5386785268784
time_elpased: 5.463
batch start
#iterations: 210
currently lose_sum: 564.0826772451401
time_elpased: 5.274
batch start
#iterations: 211
currently lose_sum: 565.5150854885578
time_elpased: 5.36
batch start
#iterations: 212
currently lose_sum: 560.6777264475822
time_elpased: 5.452
batch start
#iterations: 213
currently lose_sum: 564.7537786960602
time_elpased: 5.248
batch start
#iterations: 214
currently lose_sum: 564.9729807376862
time_elpased: 5.148
batch start
#iterations: 215
currently lose_sum: 563.8132319152355
time_elpased: 5.211
batch start
#iterations: 216
currently lose_sum: 563.2326616644859
time_elpased: 5.665
batch start
#iterations: 217
currently lose_sum: 564.1551646590233
time_elpased: 5.315
batch start
#iterations: 218
currently lose_sum: 562.7300751507282
time_elpased: 5.507
batch start
#iterations: 219
currently lose_sum: 562.5010295808315
time_elpased: 5.434
start validation test
0.578808864266
0.56920307642
0.894926417619
0.695833083278
0
validation finish
batch start
#iterations: 220
currently lose_sum: 563.4792124927044
time_elpased: 5.2
batch start
#iterations: 221
currently lose_sum: 565.1343851983547
time_elpased: 5.291
batch start
#iterations: 222
currently lose_sum: 563.8819254040718
time_elpased: 5.31
batch start
#iterations: 223
currently lose_sum: 563.2074389457703
time_elpased: 5.252
batch start
#iterations: 224
currently lose_sum: 563.2886147797108
time_elpased: 5.391
batch start
#iterations: 225
currently lose_sum: 562.5157415866852
time_elpased: 5.349
batch start
#iterations: 226
currently lose_sum: 564.3087685108185
time_elpased: 5.524
batch start
#iterations: 227
currently lose_sum: 564.2456894516945
time_elpased: 5.42
batch start
#iterations: 228
currently lose_sum: 561.9769362807274
time_elpased: 5.203
batch start
#iterations: 229
currently lose_sum: 565.3371175527573
time_elpased: 5.271
batch start
#iterations: 230
currently lose_sum: 563.4009882211685
time_elpased: 5.655
batch start
#iterations: 231
currently lose_sum: 562.1533261537552
time_elpased: 5.275
batch start
#iterations: 232
currently lose_sum: 564.7345222234726
time_elpased: 5.281
batch start
#iterations: 233
currently lose_sum: 564.5720766782761
time_elpased: 5.367
batch start
#iterations: 234
currently lose_sum: 563.5053181052208
time_elpased: 5.385
batch start
#iterations: 235
currently lose_sum: 563.8780529499054
time_elpased: 5.39
batch start
#iterations: 236
currently lose_sum: 564.0815881788731
time_elpased: 5.471
batch start
#iterations: 237
currently lose_sum: 561.0347391664982
time_elpased: 5.336
batch start
#iterations: 238
currently lose_sum: 564.5406067371368
time_elpased: 5.219
batch start
#iterations: 239
currently lose_sum: 562.8620022535324
time_elpased: 5.251
start validation test
0.577091412742
0.568515899898
0.889574971699
0.693698212387
0
validation finish
batch start
#iterations: 240
currently lose_sum: 560.8264955282211
time_elpased: 5.587
batch start
#iterations: 241
currently lose_sum: 565.145111054182
time_elpased: 5.285
batch start
#iterations: 242
currently lose_sum: 562.0895673632622
time_elpased: 5.342
batch start
#iterations: 243
currently lose_sum: 561.4671421945095
time_elpased: 5.407
batch start
#iterations: 244
currently lose_sum: 563.7382539510727
time_elpased: 5.431
batch start
#iterations: 245
currently lose_sum: 563.3557123839855
time_elpased: 5.171
batch start
#iterations: 246
currently lose_sum: 564.2713476419449
time_elpased: 5.157
batch start
#iterations: 247
currently lose_sum: 563.9842992722988
time_elpased: 5.312
batch start
#iterations: 248
currently lose_sum: 561.8835179209709
time_elpased: 5.177
batch start
#iterations: 249
currently lose_sum: 562.170853883028
time_elpased: 5.399
batch start
#iterations: 250
currently lose_sum: 564.9095162153244
time_elpased: 5.429
batch start
#iterations: 251
currently lose_sum: 562.8854975104332
time_elpased: 5.337
batch start
#iterations: 252
currently lose_sum: 562.8212822079659
time_elpased: 5.472
batch start
#iterations: 253
currently lose_sum: 563.2265352904797
time_elpased: 5.58
batch start
#iterations: 254
currently lose_sum: 561.8009316325188
time_elpased: 5.167
batch start
#iterations: 255
currently lose_sum: 561.7862710654736
time_elpased: 5.729
batch start
#iterations: 256
currently lose_sum: 562.100713133812
time_elpased: 5.34
batch start
#iterations: 257
currently lose_sum: 561.685576736927
time_elpased: 5.214
batch start
#iterations: 258
currently lose_sum: 562.6845327913761
time_elpased: 5.124
batch start
#iterations: 259
currently lose_sum: 561.6182545423508
time_elpased: 5.364
start validation test
0.577146814404
0.56881581988
0.886590511475
0.6930115636
0
validation finish
batch start
#iterations: 260
currently lose_sum: 564.5977036952972
time_elpased: 5.384
batch start
#iterations: 261
currently lose_sum: 565.4177198708057
time_elpased: 5.355
batch start
#iterations: 262
currently lose_sum: 564.2192777693272
time_elpased: 5.458
batch start
#iterations: 263
currently lose_sum: 561.7184948027134
time_elpased: 5.072
batch start
#iterations: 264
currently lose_sum: 562.9387158155441
time_elpased: 5.186
batch start
#iterations: 265
currently lose_sum: 563.9619876742363
time_elpased: 5.262
batch start
#iterations: 266
currently lose_sum: 564.108118057251
time_elpased: 5.223
batch start
#iterations: 267
currently lose_sum: 563.4525500535965
time_elpased: 5.795
batch start
#iterations: 268
currently lose_sum: 562.8657217621803
time_elpased: 5.213
batch start
#iterations: 269
currently lose_sum: 563.3250199854374
time_elpased: 5.297
batch start
#iterations: 270
currently lose_sum: 562.5851157605648
time_elpased: 5.301
batch start
#iterations: 271
currently lose_sum: 562.7267451584339
time_elpased: 5.552
batch start
#iterations: 272
currently lose_sum: 561.9935232400894
time_elpased: 5.378
batch start
#iterations: 273
currently lose_sum: 562.2146352231503
time_elpased: 5.335
batch start
#iterations: 274
currently lose_sum: 562.1809102296829
time_elpased: 5.505
batch start
#iterations: 275
currently lose_sum: 563.4459348917007
time_elpased: 5.407
batch start
#iterations: 276
currently lose_sum: 563.2315689623356
time_elpased: 5.417
batch start
#iterations: 277
currently lose_sum: 565.2197218835354
time_elpased: 5.438
batch start
#iterations: 278
currently lose_sum: 563.4107988476753
time_elpased: 5.253
batch start
#iterations: 279
currently lose_sum: 560.537678360939
time_elpased: 5.188
start validation test
0.57811634349
0.568990416174
0.892044869816
0.694801811551
0
validation finish
batch start
#iterations: 280
currently lose_sum: 562.743881970644
time_elpased: 5.286
batch start
#iterations: 281
currently lose_sum: 562.9436877965927
time_elpased: 5.461
batch start
#iterations: 282
currently lose_sum: 563.189316123724
time_elpased: 5.461
batch start
#iterations: 283
currently lose_sum: 562.2386469841003
time_elpased: 5.38
batch start
#iterations: 284
currently lose_sum: 558.9033533632755
time_elpased: 5.416
batch start
#iterations: 285
currently lose_sum: 561.1946569979191
time_elpased: 5.485
batch start
#iterations: 286
currently lose_sum: 563.3200064003468
time_elpased: 5.323
batch start
#iterations: 287
currently lose_sum: 562.6108010709286
time_elpased: 5.408
batch start
#iterations: 288
currently lose_sum: 564.8798118829727
time_elpased: 5.16
batch start
#iterations: 289
currently lose_sum: 561.6603112220764
time_elpased: 5.339
batch start
#iterations: 290
currently lose_sum: 565.6370360851288
time_elpased: 5.617
batch start
#iterations: 291
currently lose_sum: 563.5429312884808
time_elpased: 5.292
batch start
#iterations: 292
currently lose_sum: 562.0980891883373
time_elpased: 5.295
batch start
#iterations: 293
currently lose_sum: 561.754606872797
time_elpased: 5.417
batch start
#iterations: 294
currently lose_sum: 561.8300176262856
time_elpased: 5.414
batch start
#iterations: 295
currently lose_sum: 560.1642400920391
time_elpased: 5.276
batch start
#iterations: 296
currently lose_sum: 561.1660240292549
time_elpased: 5.186
batch start
#iterations: 297
currently lose_sum: 563.9058522880077
time_elpased: 5.62
batch start
#iterations: 298
currently lose_sum: 562.9836585819721
time_elpased: 5.19
batch start
#iterations: 299
currently lose_sum: 560.4412160515785
time_elpased: 5.29
start validation test
0.57836565097
0.5688241252
0.895852629412
0.695829419876
0
validation finish
batch start
#iterations: 300
currently lose_sum: 562.1391440033913
time_elpased: 5.505
batch start
#iterations: 301
currently lose_sum: 565.1675603687763
time_elpased: 5.398
batch start
#iterations: 302
currently lose_sum: 562.0514824688435
time_elpased: 5.241
batch start
#iterations: 303
currently lose_sum: 563.9690091311932
time_elpased: 5.51
batch start
#iterations: 304
currently lose_sum: 560.365825176239
time_elpased: 5.455
batch start
#iterations: 305
currently lose_sum: 565.2665407657623
time_elpased: 5.362
batch start
#iterations: 306
currently lose_sum: 561.1589606702328
time_elpased: 5.445
batch start
#iterations: 307
currently lose_sum: 562.1718673408031
time_elpased: 5.355
batch start
#iterations: 308
currently lose_sum: 563.4376206994057
time_elpased: 5.375
batch start
#iterations: 309
currently lose_sum: 563.2580093443394
time_elpased: 5.568
batch start
#iterations: 310
currently lose_sum: 561.6190531849861
time_elpased: 5.516
batch start
#iterations: 311
currently lose_sum: 560.2041379511356
time_elpased: 5.328
batch start
#iterations: 312
currently lose_sum: 562.3421086668968
time_elpased: 5.51
batch start
#iterations: 313
currently lose_sum: 561.5982710421085
time_elpased: 5.317
batch start
#iterations: 314
currently lose_sum: 560.3021729588509
time_elpased: 5.279
batch start
#iterations: 315
currently lose_sum: 561.0789220333099
time_elpased: 5.371
batch start
#iterations: 316
currently lose_sum: 562.9831490814686
time_elpased: 5.503
batch start
#iterations: 317
currently lose_sum: 562.01720058918
time_elpased: 5.213
batch start
#iterations: 318
currently lose_sum: 562.5396020710468
time_elpased: 5.333
batch start
#iterations: 319
currently lose_sum: 561.312639683485
time_elpased: 5.529
start validation test
0.57811634349
0.569044803574
0.891427395287
0.694654958098
0
validation finish
batch start
#iterations: 320
currently lose_sum: 561.8047468364239
time_elpased: 5.184
batch start
#iterations: 321
currently lose_sum: 561.1358169913292
time_elpased: 5.257
batch start
#iterations: 322
currently lose_sum: 561.7855110168457
time_elpased: 5.572
batch start
#iterations: 323
currently lose_sum: 562.9602914750576
time_elpased: 5.268
batch start
#iterations: 324
currently lose_sum: 562.8357942998409
time_elpased: 5.415
batch start
#iterations: 325
currently lose_sum: 563.2457493245602
time_elpased: 5.37
batch start
#iterations: 326
currently lose_sum: 563.0549784302711
time_elpased: 5.168
batch start
#iterations: 327
currently lose_sum: 562.1213262677193
time_elpased: 5.383
batch start
#iterations: 328
currently lose_sum: 561.8521132469177
time_elpased: 5.234
batch start
#iterations: 329
currently lose_sum: 562.5600583553314
time_elpased: 5.307
batch start
#iterations: 330
currently lose_sum: 562.0237458050251
time_elpased: 5.436
batch start
#iterations: 331
currently lose_sum: 562.1232552826405
time_elpased: 5.359
batch start
#iterations: 332
currently lose_sum: 562.8183086514473
time_elpased: 5.373
batch start
#iterations: 333
currently lose_sum: 562.3088220357895
time_elpased: 5.101
batch start
#iterations: 334
currently lose_sum: 561.2321065962315
time_elpased: 5.335
batch start
#iterations: 335
currently lose_sum: 562.7915008068085
time_elpased: 5.438
batch start
#iterations: 336
currently lose_sum: 562.8053684532642
time_elpased: 5.298
batch start
#iterations: 337
currently lose_sum: 562.7844908237457
time_elpased: 5.365
batch start
#iterations: 338
currently lose_sum: 561.5634912848473
time_elpased: 5.29
batch start
#iterations: 339
currently lose_sum: 563.1402417421341
time_elpased: 5.541
start validation test
0.577922437673
0.568941161011
0.891118658022
0.694483989333
0
validation finish
batch start
#iterations: 340
currently lose_sum: 562.6325289905071
time_elpased: 5.243
batch start
#iterations: 341
currently lose_sum: 561.822598695755
time_elpased: 5.196
batch start
#iterations: 342
currently lose_sum: 561.9022816717625
time_elpased: 5.55
batch start
#iterations: 343
currently lose_sum: 565.1989891827106
time_elpased: 5.456
batch start
#iterations: 344
currently lose_sum: 562.9098901450634
time_elpased: 5.187
batch start
#iterations: 345
currently lose_sum: 562.82192543149
time_elpased: 5.481
batch start
#iterations: 346
currently lose_sum: 562.0488789975643
time_elpased: 5.443
batch start
#iterations: 347
currently lose_sum: 561.3711513578892
time_elpased: 5.349
batch start
#iterations: 348
currently lose_sum: 563.4471417665482
time_elpased: 5.428
batch start
#iterations: 349
currently lose_sum: 561.6879991590977
time_elpased: 5.41
batch start
#iterations: 350
currently lose_sum: 562.0375665128231
time_elpased: 5.358
batch start
#iterations: 351
currently lose_sum: 562.8708734214306
time_elpased: 5.249
batch start
#iterations: 352
currently lose_sum: 562.6130136549473
time_elpased: 5.566
batch start
#iterations: 353
currently lose_sum: 562.9116840362549
time_elpased: 5.352
batch start
#iterations: 354
currently lose_sum: 563.0037093162537
time_elpased: 5.098
batch start
#iterations: 355
currently lose_sum: 562.4830462634563
time_elpased: 5.308
batch start
#iterations: 356
currently lose_sum: 561.7499214112759
time_elpased: 5.428
batch start
#iterations: 357
currently lose_sum: 562.1987815201283
time_elpased: 5.273
batch start
#iterations: 358
currently lose_sum: 562.0408566892147
time_elpased: 5.283
batch start
#iterations: 359
currently lose_sum: 562.1203007400036
time_elpased: 5.532
start validation test
0.578033240997
0.569134055228
0.889780796542
0.694220848305
0
validation finish
batch start
#iterations: 360
currently lose_sum: 562.193824917078
time_elpased: 5.351
batch start
#iterations: 361
currently lose_sum: 563.3146463632584
time_elpased: 5.074
batch start
#iterations: 362
currently lose_sum: 562.1318426430225
time_elpased: 5.322
batch start
#iterations: 363
currently lose_sum: 561.953793823719
time_elpased: 5.523
batch start
#iterations: 364
currently lose_sum: 562.7427258491516
time_elpased: 5.283
batch start
#iterations: 365
currently lose_sum: 561.0696875751019
time_elpased: 5.26
batch start
#iterations: 366
currently lose_sum: 561.2188734412193
time_elpased: 5.161
batch start
#iterations: 367
currently lose_sum: 560.7012234926224
time_elpased: 5.602
batch start
#iterations: 368
currently lose_sum: 559.8561195731163
time_elpased: 5.372
batch start
#iterations: 369
currently lose_sum: 562.1748808324337
time_elpased: 5.397
batch start
#iterations: 370
currently lose_sum: 561.828785866499
time_elpased: 5.275
batch start
#iterations: 371
currently lose_sum: 562.4568178057671
time_elpased: 5.471
batch start
#iterations: 372
currently lose_sum: 564.5990386903286
time_elpased: 5.471
batch start
#iterations: 373
currently lose_sum: 561.5759881734848
time_elpased: 5.509
batch start
#iterations: 374
currently lose_sum: 561.7196282744408
time_elpased: 5.251
batch start
#iterations: 375
currently lose_sum: 563.0505175292492
time_elpased: 5.191
batch start
#iterations: 376
currently lose_sum: 560.8460755348206
time_elpased: 5.511
batch start
#iterations: 377
currently lose_sum: 562.0680986940861
time_elpased: 5.547
batch start
#iterations: 378
currently lose_sum: 563.8123692572117
time_elpased: 5.144
batch start
#iterations: 379
currently lose_sum: 563.332526654005
time_elpased: 5.306
start validation test
0.578227146814
0.568884232583
0.894103118246
0.695345952219
0
validation finish
batch start
#iterations: 380
currently lose_sum: 562.0052472352982
time_elpased: 5.439
batch start
#iterations: 381
currently lose_sum: 562.695662945509
time_elpased: 5.213
batch start
#iterations: 382
currently lose_sum: 559.1544003784657
time_elpased: 5.224
batch start
#iterations: 383
currently lose_sum: 562.1197485923767
time_elpased: 5.33
batch start
#iterations: 384
currently lose_sum: 562.8496303260326
time_elpased: 5.137
batch start
#iterations: 385
currently lose_sum: 561.8616671264172
time_elpased: 5.176
batch start
#iterations: 386
currently lose_sum: 561.085033595562
time_elpased: 5.536
batch start
#iterations: 387
currently lose_sum: 561.4113526642323
time_elpased: 5.322
batch start
#iterations: 388
currently lose_sum: 562.2296735048294
time_elpased: 5.345
batch start
#iterations: 389
currently lose_sum: 564.130945622921
time_elpased: 5.189
batch start
#iterations: 390
currently lose_sum: 561.9351018667221
time_elpased: 5.656
batch start
#iterations: 391
currently lose_sum: 561.8032131791115
time_elpased: 5.285
batch start
#iterations: 392
currently lose_sum: 560.7348988652229
time_elpased: 5.22
batch start
#iterations: 393
currently lose_sum: 561.5134872496128
time_elpased: 5.522
batch start
#iterations: 394
currently lose_sum: 560.7544070780277
time_elpased: 5.454
batch start
#iterations: 395
currently lose_sum: 562.8304719626904
time_elpased: 5.539
batch start
#iterations: 396
currently lose_sum: 561.0958504676819
time_elpased: 5.23
batch start
#iterations: 397
currently lose_sum: 558.9887706935406
time_elpased: 5.557
batch start
#iterations: 398
currently lose_sum: 561.9819629192352
time_elpased: 5.377
batch start
#iterations: 399
currently lose_sum: 562.6049621999264
time_elpased: 5.537
start validation test
0.578254847645
0.56886678229
0.894514767932
0.695457364031
0
validation finish
acc: 0.581
pre: 0.571
rec: 0.892
F1: 0.696
auc: 0.000
