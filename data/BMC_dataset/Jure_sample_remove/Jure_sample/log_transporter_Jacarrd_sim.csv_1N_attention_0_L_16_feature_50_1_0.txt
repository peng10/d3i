start to construct graph
graph construct over
93159
epochs start
batch start
#iterations: 0
currently lose_sum: 617.1549198031425
time_elpased: 1.87
batch start
#iterations: 1
currently lose_sum: 610.634453356266
time_elpased: 1.912
batch start
#iterations: 2
currently lose_sum: 610.8282652497292
time_elpased: 1.875
batch start
#iterations: 3
currently lose_sum: 608.8443301916122
time_elpased: 1.839
batch start
#iterations: 4
currently lose_sum: 608.1108139753342
time_elpased: 1.816
batch start
#iterations: 5
currently lose_sum: 607.5474939346313
time_elpased: 1.849
batch start
#iterations: 6
currently lose_sum: 607.4629187583923
time_elpased: 1.866
batch start
#iterations: 7
currently lose_sum: 607.7177480459213
time_elpased: 1.838
batch start
#iterations: 8
currently lose_sum: 608.9662570953369
time_elpased: 1.822
batch start
#iterations: 9
currently lose_sum: 605.8985804915428
time_elpased: 1.812
batch start
#iterations: 10
currently lose_sum: 605.5233209133148
time_elpased: 1.808
batch start
#iterations: 11
currently lose_sum: 607.5848225951195
time_elpased: 1.799
batch start
#iterations: 12
currently lose_sum: 605.3750817775726
time_elpased: 1.799
batch start
#iterations: 13
currently lose_sum: 607.6057730317116
time_elpased: 1.793
batch start
#iterations: 14
currently lose_sum: 606.8753178715706
time_elpased: 1.805
batch start
#iterations: 15
currently lose_sum: 604.8598454594612
time_elpased: 1.806
batch start
#iterations: 16
currently lose_sum: 607.7081137895584
time_elpased: 1.792
batch start
#iterations: 17
currently lose_sum: 604.9836333394051
time_elpased: 1.791
batch start
#iterations: 18
currently lose_sum: 605.2789207100868
time_elpased: 1.786
batch start
#iterations: 19
currently lose_sum: 605.3265870213509
time_elpased: 1.787
start validation test
0.546077348066
0.543545111691
0.964083564886
0.695161769071
0
validation finish
batch start
#iterations: 20
currently lose_sum: 605.5069913864136
time_elpased: 1.806
batch start
#iterations: 21
currently lose_sum: 605.4015868902206
time_elpased: 1.786
batch start
#iterations: 22
currently lose_sum: 606.5965730547905
time_elpased: 1.814
batch start
#iterations: 23
currently lose_sum: 604.9886203408241
time_elpased: 1.807
batch start
#iterations: 24
currently lose_sum: 604.7314087152481
time_elpased: 1.777
batch start
#iterations: 25
currently lose_sum: 604.6208337545395
time_elpased: 1.813
batch start
#iterations: 26
currently lose_sum: 605.3175045251846
time_elpased: 1.798
batch start
#iterations: 27
currently lose_sum: 604.7583719491959
time_elpased: 1.803
batch start
#iterations: 28
currently lose_sum: 604.4974063634872
time_elpased: 1.794
batch start
#iterations: 29
currently lose_sum: 604.2580505609512
time_elpased: 1.802
batch start
#iterations: 30
currently lose_sum: 603.5657356977463
time_elpased: 1.815
batch start
#iterations: 31
currently lose_sum: 604.7208313941956
time_elpased: 1.806
batch start
#iterations: 32
currently lose_sum: 604.876807153225
time_elpased: 1.806
batch start
#iterations: 33
currently lose_sum: 603.904737830162
time_elpased: 1.82
batch start
#iterations: 34
currently lose_sum: 605.2293920516968
time_elpased: 1.813
batch start
#iterations: 35
currently lose_sum: 605.6064019203186
time_elpased: 1.803
batch start
#iterations: 36
currently lose_sum: 603.8014768362045
time_elpased: 1.799
batch start
#iterations: 37
currently lose_sum: 603.8101953864098
time_elpased: 1.816
batch start
#iterations: 38
currently lose_sum: 604.4378855228424
time_elpased: 1.798
batch start
#iterations: 39
currently lose_sum: 603.7154724597931
time_elpased: 1.804
start validation test
0.544779005525
0.543104705779
0.957908819595
0.693191338832
0
validation finish
batch start
#iterations: 40
currently lose_sum: 605.6891243457794
time_elpased: 1.779
batch start
#iterations: 41
currently lose_sum: 604.3800528049469
time_elpased: 1.823
batch start
#iterations: 42
currently lose_sum: 605.3063734173775
time_elpased: 1.923
batch start
#iterations: 43
currently lose_sum: 604.3675574064255
time_elpased: 1.868
batch start
#iterations: 44
currently lose_sum: 603.0409502983093
time_elpased: 1.917
batch start
#iterations: 45
currently lose_sum: 604.141245663166
time_elpased: 1.885
batch start
#iterations: 46
currently lose_sum: 604.9513615369797
time_elpased: 1.897
batch start
#iterations: 47
currently lose_sum: 606.61215955019
time_elpased: 1.846
batch start
#iterations: 48
currently lose_sum: 604.8302403092384
time_elpased: 1.818
batch start
#iterations: 49
currently lose_sum: 604.040109038353
time_elpased: 1.832
batch start
#iterations: 50
currently lose_sum: 604.3263176083565
time_elpased: 1.893
batch start
#iterations: 51
currently lose_sum: 602.8688797950745
time_elpased: 1.935
batch start
#iterations: 52
currently lose_sum: 603.6383438110352
time_elpased: 1.886
batch start
#iterations: 53
currently lose_sum: 604.7451168894768
time_elpased: 1.856
batch start
#iterations: 54
currently lose_sum: 603.5080661177635
time_elpased: 1.84
batch start
#iterations: 55
currently lose_sum: 602.9920600652695
time_elpased: 1.832
batch start
#iterations: 56
currently lose_sum: 604.9088516831398
time_elpased: 1.803
batch start
#iterations: 57
currently lose_sum: 604.093701004982
time_elpased: 1.87
batch start
#iterations: 58
currently lose_sum: 604.354828774929
time_elpased: 1.796
batch start
#iterations: 59
currently lose_sum: 603.8643333315849
time_elpased: 1.82
start validation test
0.546325966851
0.543748002208
0.962848615828
0.695006221331
0
validation finish
batch start
#iterations: 60
currently lose_sum: 602.6219905018806
time_elpased: 1.879
batch start
#iterations: 61
currently lose_sum: 605.4035468101501
time_elpased: 1.842
batch start
#iterations: 62
currently lose_sum: 603.1994735002518
time_elpased: 1.864
batch start
#iterations: 63
currently lose_sum: 604.1981432437897
time_elpased: 1.842
batch start
#iterations: 64
currently lose_sum: 603.9730376005173
time_elpased: 1.853
batch start
#iterations: 65
currently lose_sum: 604.4045619368553
time_elpased: 1.903
batch start
#iterations: 66
currently lose_sum: 603.8751932382584
time_elpased: 1.851
batch start
#iterations: 67
currently lose_sum: 603.0343670845032
time_elpased: 1.866
batch start
#iterations: 68
currently lose_sum: 603.6123353838921
time_elpased: 1.826
batch start
#iterations: 69
currently lose_sum: 604.2847598791122
time_elpased: 1.842
batch start
#iterations: 70
currently lose_sum: 605.0207085609436
time_elpased: 1.825
batch start
#iterations: 71
currently lose_sum: 605.0706222057343
time_elpased: 1.821
batch start
#iterations: 72
currently lose_sum: 604.8784316778183
time_elpased: 1.831
batch start
#iterations: 73
currently lose_sum: 603.7991163134575
time_elpased: 1.847
batch start
#iterations: 74
currently lose_sum: 603.6889989972115
time_elpased: 1.808
batch start
#iterations: 75
currently lose_sum: 604.7769730687141
time_elpased: 1.83
batch start
#iterations: 76
currently lose_sum: 606.2077034115791
time_elpased: 1.839
batch start
#iterations: 77
currently lose_sum: 603.4261901974678
time_elpased: 1.819
batch start
#iterations: 78
currently lose_sum: 603.7409429550171
time_elpased: 1.821
batch start
#iterations: 79
currently lose_sum: 603.5064745545387
time_elpased: 1.828
start validation test
0.548038674033
0.546663832113
0.926211793764
0.687534615458
0
validation finish
batch start
#iterations: 80
currently lose_sum: 602.242127597332
time_elpased: 1.843
batch start
#iterations: 81
currently lose_sum: 604.9100849628448
time_elpased: 1.822
batch start
#iterations: 82
currently lose_sum: 602.8261317610741
time_elpased: 1.817
batch start
#iterations: 83
currently lose_sum: 604.3823877573013
time_elpased: 1.773
batch start
#iterations: 84
currently lose_sum: 602.7699048519135
time_elpased: 1.814
batch start
#iterations: 85
currently lose_sum: 602.6708869934082
time_elpased: 1.803
batch start
#iterations: 86
currently lose_sum: 604.3074940443039
time_elpased: 1.78
batch start
#iterations: 87
currently lose_sum: 604.403987467289
time_elpased: 1.825
batch start
#iterations: 88
currently lose_sum: 603.5958626270294
time_elpased: 1.788
batch start
#iterations: 89
currently lose_sum: 602.9560305476189
time_elpased: 1.818
batch start
#iterations: 90
currently lose_sum: 600.9373434782028
time_elpased: 1.781
batch start
#iterations: 91
currently lose_sum: 604.428073823452
time_elpased: 1.828
batch start
#iterations: 92
currently lose_sum: 604.3210831284523
time_elpased: 1.815
batch start
#iterations: 93
currently lose_sum: 603.6881859302521
time_elpased: 1.797
batch start
#iterations: 94
currently lose_sum: 602.840871989727
time_elpased: 1.799
batch start
#iterations: 95
currently lose_sum: 603.9800879359245
time_elpased: 1.82
batch start
#iterations: 96
currently lose_sum: 603.8445667028427
time_elpased: 1.833
batch start
#iterations: 97
currently lose_sum: 603.1291691660881
time_elpased: 1.817
batch start
#iterations: 98
currently lose_sum: 603.3509231209755
time_elpased: 1.798
batch start
#iterations: 99
currently lose_sum: 603.4661011099815
time_elpased: 1.833
start validation test
0.548480662983
0.54514102002
0.959761243182
0.695334488993
0
validation finish
batch start
#iterations: 100
currently lose_sum: 603.6763828396797
time_elpased: 1.808
batch start
#iterations: 101
currently lose_sum: 603.1624000072479
time_elpased: 1.81
batch start
#iterations: 102
currently lose_sum: 603.6716663837433
time_elpased: 1.792
batch start
#iterations: 103
currently lose_sum: 603.0230771899223
time_elpased: 1.778
batch start
#iterations: 104
currently lose_sum: 605.1694124341011
time_elpased: 1.766
batch start
#iterations: 105
currently lose_sum: 604.7729706764221
time_elpased: 1.794
batch start
#iterations: 106
currently lose_sum: 603.5705854892731
time_elpased: 1.799
batch start
#iterations: 107
currently lose_sum: 603.7464069724083
time_elpased: 1.825
batch start
#iterations: 108
currently lose_sum: 605.0118176937103
time_elpased: 1.804
batch start
#iterations: 109
currently lose_sum: 602.0888587832451
time_elpased: 1.784
batch start
#iterations: 110
currently lose_sum: 603.1128265857697
time_elpased: 1.845
batch start
#iterations: 111
currently lose_sum: 602.9161669015884
time_elpased: 1.853
batch start
#iterations: 112
currently lose_sum: 603.1264755129814
time_elpased: 1.835
batch start
#iterations: 113
currently lose_sum: 602.144152879715
time_elpased: 1.825
batch start
#iterations: 114
currently lose_sum: 601.92408233881
time_elpased: 1.822
batch start
#iterations: 115
currently lose_sum: 603.1583676338196
time_elpased: 1.812
batch start
#iterations: 116
currently lose_sum: 602.73965716362
time_elpased: 1.795
batch start
#iterations: 117
currently lose_sum: 605.1768046617508
time_elpased: 1.835
batch start
#iterations: 118
currently lose_sum: 604.70435744524
time_elpased: 1.803
batch start
#iterations: 119
currently lose_sum: 603.1946026682854
time_elpased: 1.824
start validation test
0.546629834254
0.543627649131
0.968817536277
0.696456314271
0
validation finish
batch start
#iterations: 120
currently lose_sum: 604.801537156105
time_elpased: 1.805
batch start
#iterations: 121
currently lose_sum: 603.9229714274406
time_elpased: 1.834
batch start
#iterations: 122
currently lose_sum: 603.8791827559471
time_elpased: 1.804
batch start
#iterations: 123
currently lose_sum: 601.9282081127167
time_elpased: 1.813
batch start
#iterations: 124
currently lose_sum: 603.3515417575836
time_elpased: 1.822
batch start
#iterations: 125
currently lose_sum: 604.6940095424652
time_elpased: 1.832
batch start
#iterations: 126
currently lose_sum: 603.6870061159134
time_elpased: 1.812
batch start
#iterations: 127
currently lose_sum: 603.9020075798035
time_elpased: 1.801
batch start
#iterations: 128
currently lose_sum: 602.4364424943924
time_elpased: 1.801
batch start
#iterations: 129
currently lose_sum: 605.7453846931458
time_elpased: 1.802
batch start
#iterations: 130
currently lose_sum: 605.0619021058083
time_elpased: 1.796
batch start
#iterations: 131
currently lose_sum: 601.4987874031067
time_elpased: 1.793
batch start
#iterations: 132
currently lose_sum: 604.2472638487816
time_elpased: 1.833
batch start
#iterations: 133
currently lose_sum: 603.1728979349136
time_elpased: 1.775
batch start
#iterations: 134
currently lose_sum: 604.0444511771202
time_elpased: 1.809
batch start
#iterations: 135
currently lose_sum: 603.8536965250969
time_elpased: 1.821
batch start
#iterations: 136
currently lose_sum: 603.8438462018967
time_elpased: 1.825
batch start
#iterations: 137
currently lose_sum: 602.0169745087624
time_elpased: 1.799
batch start
#iterations: 138
currently lose_sum: 603.6446534991264
time_elpased: 1.815
batch start
#iterations: 139
currently lose_sum: 604.7338233590126
time_elpased: 1.774
start validation test
0.545939226519
0.543285479072
0.967788412061
0.695909570237
0
validation finish
batch start
#iterations: 140
currently lose_sum: 604.1520244479179
time_elpased: 1.791
batch start
#iterations: 141
currently lose_sum: 602.7441611289978
time_elpased: 1.774
batch start
#iterations: 142
currently lose_sum: 603.7090792655945
time_elpased: 1.788
batch start
#iterations: 143
currently lose_sum: 602.3786861896515
time_elpased: 1.785
batch start
#iterations: 144
currently lose_sum: 603.7601801156998
time_elpased: 1.78
batch start
#iterations: 145
currently lose_sum: 603.5470029115677
time_elpased: 1.821
batch start
#iterations: 146
currently lose_sum: 603.237488090992
time_elpased: 1.812
batch start
#iterations: 147
currently lose_sum: 603.0038828849792
time_elpased: 1.811
batch start
#iterations: 148
currently lose_sum: 603.3716118335724
time_elpased: 1.801
batch start
#iterations: 149
currently lose_sum: 603.2865557074547
time_elpased: 1.824
batch start
#iterations: 150
currently lose_sum: 603.8200650811195
time_elpased: 1.83
batch start
#iterations: 151
currently lose_sum: 602.4129191637039
time_elpased: 1.831
batch start
#iterations: 152
currently lose_sum: 602.6925355195999
time_elpased: 1.803
batch start
#iterations: 153
currently lose_sum: 602.9565972685814
time_elpased: 1.794
batch start
#iterations: 154
currently lose_sum: 604.1147108674049
time_elpased: 1.816
batch start
#iterations: 155
currently lose_sum: 603.8989260196686
time_elpased: 1.83
batch start
#iterations: 156
currently lose_sum: 605.0762481689453
time_elpased: 1.934
batch start
#iterations: 157
currently lose_sum: 603.457098543644
time_elpased: 1.806
batch start
#iterations: 158
currently lose_sum: 603.0420989394188
time_elpased: 1.804
batch start
#iterations: 159
currently lose_sum: 603.6423690319061
time_elpased: 1.796
start validation test
0.547127071823
0.544273564022
0.961510754348
0.695086113901
0
validation finish
batch start
#iterations: 160
currently lose_sum: 603.6758222579956
time_elpased: 1.824
batch start
#iterations: 161
currently lose_sum: 602.9304276108742
time_elpased: 1.794
batch start
#iterations: 162
currently lose_sum: 605.1074224710464
time_elpased: 1.795
batch start
#iterations: 163
currently lose_sum: 603.0028492212296
time_elpased: 1.804
batch start
#iterations: 164
currently lose_sum: 604.6907539963722
time_elpased: 1.8
batch start
#iterations: 165
currently lose_sum: 605.8883652687073
time_elpased: 1.804
batch start
#iterations: 166
currently lose_sum: 602.7690034508705
time_elpased: 1.804
batch start
#iterations: 167
currently lose_sum: 604.9572348594666
time_elpased: 1.792
batch start
#iterations: 168
currently lose_sum: 604.6653532981873
time_elpased: 1.794
batch start
#iterations: 169
currently lose_sum: 603.4262756705284
time_elpased: 1.821
batch start
#iterations: 170
currently lose_sum: 603.0436156988144
time_elpased: 1.788
batch start
#iterations: 171
currently lose_sum: 601.8740081191063
time_elpased: 1.82
batch start
#iterations: 172
currently lose_sum: 602.5208874344826
time_elpased: 1.811
batch start
#iterations: 173
currently lose_sum: 603.2288079857826
time_elpased: 1.811
batch start
#iterations: 174
currently lose_sum: 603.2795701026917
time_elpased: 1.797
batch start
#iterations: 175
currently lose_sum: 603.581467628479
time_elpased: 1.8
batch start
#iterations: 176
currently lose_sum: 604.772516310215
time_elpased: 1.791
batch start
#iterations: 177
currently lose_sum: 601.956040263176
time_elpased: 1.796
batch start
#iterations: 178
currently lose_sum: 603.0674260854721
time_elpased: 1.791
batch start
#iterations: 179
currently lose_sum: 603.9997918605804
time_elpased: 1.824
start validation test
0.547016574586
0.544071536407
0.964289389729
0.695645718104
0
validation finish
batch start
#iterations: 180
currently lose_sum: 601.4276639223099
time_elpased: 1.828
batch start
#iterations: 181
currently lose_sum: 603.7997590303421
time_elpased: 1.82
batch start
#iterations: 182
currently lose_sum: 602.4085148572922
time_elpased: 1.829
batch start
#iterations: 183
currently lose_sum: 604.148416697979
time_elpased: 1.814
batch start
#iterations: 184
currently lose_sum: 602.3047623038292
time_elpased: 1.788
batch start
#iterations: 185
currently lose_sum: 603.7762591838837
time_elpased: 1.8
batch start
#iterations: 186
currently lose_sum: 602.9566161632538
time_elpased: 1.797
batch start
#iterations: 187
currently lose_sum: 603.851484298706
time_elpased: 1.812
batch start
#iterations: 188
currently lose_sum: 604.3966996669769
time_elpased: 1.844
batch start
#iterations: 189
currently lose_sum: 605.0531870126724
time_elpased: 1.798
batch start
#iterations: 190
currently lose_sum: 600.9940596818924
time_elpased: 1.781
batch start
#iterations: 191
currently lose_sum: 602.2533670067787
time_elpased: 1.789
batch start
#iterations: 192
currently lose_sum: 603.7584518194199
time_elpased: 1.782
batch start
#iterations: 193
currently lose_sum: 604.5023480653763
time_elpased: 1.835
batch start
#iterations: 194
currently lose_sum: 603.1518948078156
time_elpased: 1.822
batch start
#iterations: 195
currently lose_sum: 604.4996030926704
time_elpased: 1.805
batch start
#iterations: 196
currently lose_sum: 603.4071879982948
time_elpased: 1.803
batch start
#iterations: 197
currently lose_sum: 603.8994990587234
time_elpased: 1.827
batch start
#iterations: 198
currently lose_sum: 603.7446964979172
time_elpased: 1.829
batch start
#iterations: 199
currently lose_sum: 603.5551124811172
time_elpased: 1.795
start validation test
0.547348066298
0.544158553547
0.96634763816
0.696251807363
0
validation finish
batch start
#iterations: 200
currently lose_sum: 601.2001148462296
time_elpased: 1.829
batch start
#iterations: 201
currently lose_sum: 604.191168487072
time_elpased: 1.827
batch start
#iterations: 202
currently lose_sum: 602.685246527195
time_elpased: 1.794
batch start
#iterations: 203
currently lose_sum: 603.5032020807266
time_elpased: 1.81
batch start
#iterations: 204
currently lose_sum: 601.727353990078
time_elpased: 1.85
batch start
#iterations: 205
currently lose_sum: 601.1565440893173
time_elpased: 1.812
batch start
#iterations: 206
currently lose_sum: 603.5405914187431
time_elpased: 1.807
batch start
#iterations: 207
currently lose_sum: 603.7404348254204
time_elpased: 1.803
batch start
#iterations: 208
currently lose_sum: 603.6012898683548
time_elpased: 1.822
batch start
#iterations: 209
currently lose_sum: 603.1961280107498
time_elpased: 1.81
batch start
#iterations: 210
currently lose_sum: 602.2676219940186
time_elpased: 1.83
batch start
#iterations: 211
currently lose_sum: 603.1180486083031
time_elpased: 1.797
batch start
#iterations: 212
currently lose_sum: 602.8765512704849
time_elpased: 1.821
batch start
#iterations: 213
currently lose_sum: 604.1396464705467
time_elpased: 1.822
batch start
#iterations: 214
currently lose_sum: 603.6881228685379
time_elpased: 1.81
batch start
#iterations: 215
currently lose_sum: 603.4713581800461
time_elpased: 1.802
batch start
#iterations: 216
currently lose_sum: 603.0465678572655
time_elpased: 1.839
batch start
#iterations: 217
currently lose_sum: 603.4509974718094
time_elpased: 1.83
batch start
#iterations: 218
currently lose_sum: 604.5943878293037
time_elpased: 1.841
batch start
#iterations: 219
currently lose_sum: 602.956250846386
time_elpased: 1.879
start validation test
0.546988950276
0.544168582822
0.962025316456
0.695134873864
0
validation finish
batch start
#iterations: 220
currently lose_sum: 603.7408955693245
time_elpased: 1.848
batch start
#iterations: 221
currently lose_sum: 602.5575520992279
time_elpased: 1.817
batch start
#iterations: 222
currently lose_sum: 601.490521132946
time_elpased: 1.83
batch start
#iterations: 223
currently lose_sum: 603.1852254271507
time_elpased: 1.813
batch start
#iterations: 224
currently lose_sum: 603.2464395165443
time_elpased: 1.841
batch start
#iterations: 225
currently lose_sum: 603.141762971878
time_elpased: 1.873
batch start
#iterations: 226
currently lose_sum: 601.83551633358
time_elpased: 1.816
batch start
#iterations: 227
currently lose_sum: 603.8712692856789
time_elpased: 1.811
batch start
#iterations: 228
currently lose_sum: 602.5924265384674
time_elpased: 1.831
batch start
#iterations: 229
currently lose_sum: 603.1418407559395
time_elpased: 1.837
batch start
#iterations: 230
currently lose_sum: 603.5930322408676
time_elpased: 1.834
batch start
#iterations: 231
currently lose_sum: 602.1719851493835
time_elpased: 1.84
batch start
#iterations: 232
currently lose_sum: 604.2010543346405
time_elpased: 1.84
batch start
#iterations: 233
currently lose_sum: 604.172426044941
time_elpased: 1.827
batch start
#iterations: 234
currently lose_sum: 603.1706634163857
time_elpased: 1.833
batch start
#iterations: 235
currently lose_sum: 602.7012758255005
time_elpased: 1.831
batch start
#iterations: 236
currently lose_sum: 603.6586825251579
time_elpased: 1.826
batch start
#iterations: 237
currently lose_sum: 603.2459199428558
time_elpased: 1.822
batch start
#iterations: 238
currently lose_sum: 604.2936497330666
time_elpased: 1.837
batch start
#iterations: 239
currently lose_sum: 602.5273580551147
time_elpased: 1.822
start validation test
0.546022099448
0.543103448276
0.972522383452
0.696979754398
0
validation finish
batch start
#iterations: 240
currently lose_sum: 604.3208729028702
time_elpased: 1.822
batch start
#iterations: 241
currently lose_sum: 602.8834787011147
time_elpased: 1.812
batch start
#iterations: 242
currently lose_sum: 602.9690302014351
time_elpased: 1.835
batch start
#iterations: 243
currently lose_sum: 603.6364234089851
time_elpased: 1.81
batch start
#iterations: 244
currently lose_sum: 602.8843802809715
time_elpased: 1.822
batch start
#iterations: 245
currently lose_sum: 602.7220826745033
time_elpased: 1.804
batch start
#iterations: 246
currently lose_sum: 604.178286254406
time_elpased: 1.816
batch start
#iterations: 247
currently lose_sum: 602.6012588739395
time_elpased: 1.798
batch start
#iterations: 248
currently lose_sum: 603.6666006445885
time_elpased: 1.837
batch start
#iterations: 249
currently lose_sum: 603.6381219625473
time_elpased: 1.823
batch start
#iterations: 250
currently lose_sum: 604.4782548546791
time_elpased: 1.816
batch start
#iterations: 251
currently lose_sum: 602.9422040581703
time_elpased: 1.834
batch start
#iterations: 252
currently lose_sum: 601.6136421561241
time_elpased: 1.817
batch start
#iterations: 253
currently lose_sum: 602.7165491580963
time_elpased: 1.827
batch start
#iterations: 254
currently lose_sum: 602.8466153740883
time_elpased: 1.815
batch start
#iterations: 255
currently lose_sum: 602.6743484735489
time_elpased: 1.874
batch start
#iterations: 256
currently lose_sum: 601.9415712356567
time_elpased: 1.846
batch start
#iterations: 257
currently lose_sum: 603.9359779953957
time_elpased: 1.883
batch start
#iterations: 258
currently lose_sum: 602.8763996958733
time_elpased: 1.835
batch start
#iterations: 259
currently lose_sum: 603.5321373343468
time_elpased: 1.82
start validation test
0.546878453039
0.543844929842
0.967273849954
0.696235115465
0
validation finish
batch start
#iterations: 260
currently lose_sum: 603.7983725070953
time_elpased: 1.831
batch start
#iterations: 261
currently lose_sum: 605.0711016058922
time_elpased: 1.827
batch start
#iterations: 262
currently lose_sum: 603.2989233136177
time_elpased: 1.828
batch start
#iterations: 263
currently lose_sum: 604.9233437180519
time_elpased: 1.862
batch start
#iterations: 264
currently lose_sum: 603.1072530150414
time_elpased: 1.853
batch start
#iterations: 265
currently lose_sum: 603.8727039694786
time_elpased: 1.838
batch start
#iterations: 266
currently lose_sum: 601.2102820277214
time_elpased: 1.869
batch start
#iterations: 267
currently lose_sum: 602.1400001049042
time_elpased: 1.836
batch start
#iterations: 268
currently lose_sum: 601.7460874915123
time_elpased: 1.841
batch start
#iterations: 269
currently lose_sum: 604.4584531784058
time_elpased: 1.867
batch start
#iterations: 270
currently lose_sum: 602.9024775624275
time_elpased: 1.85
batch start
#iterations: 271
currently lose_sum: 603.0984164476395
time_elpased: 1.839
batch start
#iterations: 272
currently lose_sum: 604.9603377580643
time_elpased: 1.841
batch start
#iterations: 273
currently lose_sum: 603.5294820666313
time_elpased: 1.866
batch start
#iterations: 274
currently lose_sum: 603.6103630065918
time_elpased: 1.868
batch start
#iterations: 275
currently lose_sum: 604.6442401409149
time_elpased: 1.88
batch start
#iterations: 276
currently lose_sum: 604.3150084614754
time_elpased: 1.874
batch start
#iterations: 277
currently lose_sum: 603.6543945670128
time_elpased: 1.885
batch start
#iterations: 278
currently lose_sum: 603.0461518168449
time_elpased: 1.874
batch start
#iterations: 279
currently lose_sum: 603.1520810723305
time_elpased: 1.927
start validation test
0.547430939227
0.544170020558
0.967068025111
0.696448092494
0
validation finish
batch start
#iterations: 280
currently lose_sum: 603.1075743436813
time_elpased: 1.918
batch start
#iterations: 281
currently lose_sum: 603.6381397247314
time_elpased: 1.884
batch start
#iterations: 282
currently lose_sum: 603.6247862577438
time_elpased: 1.882
batch start
#iterations: 283
currently lose_sum: 604.4311357736588
time_elpased: 1.86
batch start
#iterations: 284
currently lose_sum: 603.1580549478531
time_elpased: 1.89
batch start
#iterations: 285
currently lose_sum: 602.6521481871605
time_elpased: 1.948
batch start
#iterations: 286
currently lose_sum: 603.5260456204414
time_elpased: 1.931
batch start
#iterations: 287
currently lose_sum: 606.1445735692978
time_elpased: 1.933
batch start
#iterations: 288
currently lose_sum: 602.9399943947792
time_elpased: 1.921
batch start
#iterations: 289
currently lose_sum: 601.2156155109406
time_elpased: 1.874
batch start
#iterations: 290
currently lose_sum: 602.8700298666954
time_elpased: 1.88
batch start
#iterations: 291
currently lose_sum: 603.1459367275238
time_elpased: 1.891
batch start
#iterations: 292
currently lose_sum: 603.4339523911476
time_elpased: 1.899
batch start
#iterations: 293
currently lose_sum: 603.3580843806267
time_elpased: 1.904
batch start
#iterations: 294
currently lose_sum: 603.4874757528305
time_elpased: 1.98
batch start
#iterations: 295
currently lose_sum: 604.2380676865578
time_elpased: 1.916
batch start
#iterations: 296
currently lose_sum: 602.8603066802025
time_elpased: 1.932
batch start
#iterations: 297
currently lose_sum: 605.422824382782
time_elpased: 1.894
batch start
#iterations: 298
currently lose_sum: 604.3105323910713
time_elpased: 1.89
batch start
#iterations: 299
currently lose_sum: 604.7372244000435
time_elpased: 1.925
start validation test
0.547845303867
0.544509610359
0.96500977668
0.696191253991
0
validation finish
batch start
#iterations: 300
currently lose_sum: 605.1443831324577
time_elpased: 1.911
batch start
#iterations: 301
currently lose_sum: 602.992427945137
time_elpased: 1.886
batch start
#iterations: 302
currently lose_sum: 603.427081823349
time_elpased: 1.885
batch start
#iterations: 303
currently lose_sum: 604.4399183988571
time_elpased: 1.893
batch start
#iterations: 304
currently lose_sum: 602.779341340065
time_elpased: 1.88
batch start
#iterations: 305
currently lose_sum: 602.8263716697693
time_elpased: 1.882
batch start
#iterations: 306
currently lose_sum: 604.6470314264297
time_elpased: 1.878
batch start
#iterations: 307
currently lose_sum: 602.6093144416809
time_elpased: 1.87
batch start
#iterations: 308
currently lose_sum: 602.4886503219604
time_elpased: 1.899
batch start
#iterations: 309
currently lose_sum: 603.015623152256
time_elpased: 1.938
batch start
#iterations: 310
currently lose_sum: 603.0762330293655
time_elpased: 1.936
batch start
#iterations: 311
currently lose_sum: 602.8342502117157
time_elpased: 1.891
batch start
#iterations: 312
currently lose_sum: 602.5398486852646
time_elpased: 1.933
batch start
#iterations: 313
currently lose_sum: 604.1349071860313
time_elpased: 1.911
batch start
#iterations: 314
currently lose_sum: 602.5595459938049
time_elpased: 1.928
batch start
#iterations: 315
currently lose_sum: 602.9477128982544
time_elpased: 1.887
batch start
#iterations: 316
currently lose_sum: 601.2242974638939
time_elpased: 1.895
batch start
#iterations: 317
currently lose_sum: 602.9771293997765
time_elpased: 1.899
batch start
#iterations: 318
currently lose_sum: 603.50570333004
time_elpased: 1.908
batch start
#iterations: 319
currently lose_sum: 603.8964533805847
time_elpased: 1.879
start validation test
0.547486187845
0.544319600499
0.964701039415
0.695955602576
0
validation finish
batch start
#iterations: 320
currently lose_sum: 601.7437968850136
time_elpased: 1.984
batch start
#iterations: 321
currently lose_sum: 601.9502210617065
time_elpased: 1.872
batch start
#iterations: 322
currently lose_sum: 602.6305764317513
time_elpased: 1.869
batch start
#iterations: 323
currently lose_sum: 603.6292780041695
time_elpased: 1.895
batch start
#iterations: 324
currently lose_sum: 603.2373248934746
time_elpased: 1.938
batch start
#iterations: 325
currently lose_sum: 601.9072463512421
time_elpased: 1.894
batch start
#iterations: 326
currently lose_sum: 603.9053387641907
time_elpased: 1.902
batch start
#iterations: 327
currently lose_sum: 604.258852481842
time_elpased: 1.912
batch start
#iterations: 328
currently lose_sum: 602.0552707910538
time_elpased: 1.904
batch start
#iterations: 329
currently lose_sum: 603.2776345014572
time_elpased: 1.896
batch start
#iterations: 330
currently lose_sum: 603.8317718505859
time_elpased: 1.936
batch start
#iterations: 331
currently lose_sum: 603.2358236312866
time_elpased: 1.958
batch start
#iterations: 332
currently lose_sum: 602.85083091259
time_elpased: 1.934
batch start
#iterations: 333
currently lose_sum: 603.0791738629341
time_elpased: 1.901
batch start
#iterations: 334
currently lose_sum: 603.8406769633293
time_elpased: 1.933
batch start
#iterations: 335
currently lose_sum: 603.0527901053429
time_elpased: 1.928
batch start
#iterations: 336
currently lose_sum: 602.8130676150322
time_elpased: 1.917
batch start
#iterations: 337
currently lose_sum: 603.1058821082115
time_elpased: 1.915
batch start
#iterations: 338
currently lose_sum: 602.8440905213356
time_elpased: 1.886
batch start
#iterations: 339
currently lose_sum: 603.9407193660736
time_elpased: 1.881
start validation test
0.547541436464
0.544340909751
0.964906864259
0.6960265761
0
validation finish
batch start
#iterations: 340
currently lose_sum: 601.1967228055
time_elpased: 1.917
batch start
#iterations: 341
currently lose_sum: 603.2394052743912
time_elpased: 1.895
batch start
#iterations: 342
currently lose_sum: 603.4070885777473
time_elpased: 1.954
batch start
#iterations: 343
currently lose_sum: 603.694172680378
time_elpased: 1.895
batch start
#iterations: 344
currently lose_sum: 602.3216243386269
time_elpased: 1.89
batch start
#iterations: 345
currently lose_sum: 603.6616004705429
time_elpased: 1.9
batch start
#iterations: 346
currently lose_sum: 602.4677025675774
time_elpased: 1.905
batch start
#iterations: 347
currently lose_sum: 605.114645421505
time_elpased: 1.923
batch start
#iterations: 348
currently lose_sum: 602.6267722845078
time_elpased: 1.919
batch start
#iterations: 349
currently lose_sum: 603.3398951888084
time_elpased: 1.941
batch start
#iterations: 350
currently lose_sum: 603.9897454380989
time_elpased: 1.954
batch start
#iterations: 351
currently lose_sum: 602.5410895347595
time_elpased: 1.906
batch start
#iterations: 352
currently lose_sum: 603.9623702168465
time_elpased: 1.902
batch start
#iterations: 353
currently lose_sum: 603.0631693005562
time_elpased: 1.917
batch start
#iterations: 354
currently lose_sum: 601.0221717953682
time_elpased: 1.937
batch start
#iterations: 355
currently lose_sum: 603.68957477808
time_elpased: 1.967
batch start
#iterations: 356
currently lose_sum: 602.9762061238289
time_elpased: 1.945
batch start
#iterations: 357
currently lose_sum: 601.8448015451431
time_elpased: 1.931
batch start
#iterations: 358
currently lose_sum: 603.372722864151
time_elpased: 1.905
batch start
#iterations: 359
currently lose_sum: 601.1338599920273
time_elpased: 1.934
start validation test
0.546961325967
0.543856605955
0.967994236904
0.696431215756
0
validation finish
batch start
#iterations: 360
currently lose_sum: 602.8917890191078
time_elpased: 1.923
batch start
#iterations: 361
currently lose_sum: 602.3844806551933
time_elpased: 1.895
batch start
#iterations: 362
currently lose_sum: 602.361321747303
time_elpased: 1.893
batch start
#iterations: 363
currently lose_sum: 603.8443636894226
time_elpased: 1.896
batch start
#iterations: 364
currently lose_sum: 603.7255461812019
time_elpased: 1.9
batch start
#iterations: 365
currently lose_sum: 604.7346340417862
time_elpased: 1.911
batch start
#iterations: 366
currently lose_sum: 602.240176141262
time_elpased: 1.903
batch start
#iterations: 367
currently lose_sum: 603.4891299009323
time_elpased: 1.909
batch start
#iterations: 368
currently lose_sum: 603.2827512025833
time_elpased: 1.898
batch start
#iterations: 369
currently lose_sum: 602.6575071811676
time_elpased: 1.895
batch start
#iterations: 370
currently lose_sum: 601.6075338125229
time_elpased: 1.849
batch start
#iterations: 371
currently lose_sum: 602.1411656141281
time_elpased: 1.899
batch start
#iterations: 372
currently lose_sum: 603.7241020202637
time_elpased: 1.906
batch start
#iterations: 373
currently lose_sum: 604.4887395501137
time_elpased: 1.872
batch start
#iterations: 374
currently lose_sum: 602.2428814172745
time_elpased: 1.854
batch start
#iterations: 375
currently lose_sum: 603.9620087742805
time_elpased: 1.846
batch start
#iterations: 376
currently lose_sum: 603.3982238173485
time_elpased: 1.91
batch start
#iterations: 377
currently lose_sum: 602.0093740224838
time_elpased: 1.879
batch start
#iterations: 378
currently lose_sum: 602.6821493506432
time_elpased: 1.874
batch start
#iterations: 379
currently lose_sum: 602.2164379954338
time_elpased: 1.863
start validation test
0.547983425414
0.544604212055
0.964701039415
0.696188195102
0
validation finish
batch start
#iterations: 380
currently lose_sum: 603.4941521883011
time_elpased: 1.868
batch start
#iterations: 381
currently lose_sum: 602.9260328412056
time_elpased: 1.851
batch start
#iterations: 382
currently lose_sum: 602.770816385746
time_elpased: 1.851
batch start
#iterations: 383
currently lose_sum: 602.4347603917122
time_elpased: 1.871
batch start
#iterations: 384
currently lose_sum: 604.3709241747856
time_elpased: 1.878
batch start
#iterations: 385
currently lose_sum: 603.4779981970787
time_elpased: 1.886
batch start
#iterations: 386
currently lose_sum: 604.1954488158226
time_elpased: 1.884
batch start
#iterations: 387
currently lose_sum: 603.5551238656044
time_elpased: 1.914
batch start
#iterations: 388
currently lose_sum: 603.3873630166054
time_elpased: 1.856
batch start
#iterations: 389
currently lose_sum: 601.7301793694496
time_elpased: 1.919
batch start
#iterations: 390
currently lose_sum: 603.6342902183533
time_elpased: 1.891
batch start
#iterations: 391
currently lose_sum: 604.8890523314476
time_elpased: 1.979
batch start
#iterations: 392
currently lose_sum: 602.7717120051384
time_elpased: 1.943
batch start
#iterations: 393
currently lose_sum: 602.7987604737282
time_elpased: 1.89
batch start
#iterations: 394
currently lose_sum: 603.49053555727
time_elpased: 1.868
batch start
#iterations: 395
currently lose_sum: 604.511680662632
time_elpased: 1.836
batch start
#iterations: 396
currently lose_sum: 604.4028685092926
time_elpased: 1.868
batch start
#iterations: 397
currently lose_sum: 603.394467651844
time_elpased: 1.873
batch start
#iterations: 398
currently lose_sum: 602.8072710633278
time_elpased: 1.864
batch start
#iterations: 399
currently lose_sum: 603.7300047874451
time_elpased: 1.942
start validation test
0.546906077348
0.543769488394
0.969126273541
0.696652487516
0
validation finish
acc: 0.548
pre: 0.545
rec: 0.973
F1: 0.698
auc: 0.000
