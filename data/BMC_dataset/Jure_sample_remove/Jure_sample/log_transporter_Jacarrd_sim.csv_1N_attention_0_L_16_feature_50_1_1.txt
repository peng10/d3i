start to construct graph
graph construct over
93119
epochs start
batch start
#iterations: 0
currently lose_sum: 615.2970389723778
time_elpased: 1.917
batch start
#iterations: 1
currently lose_sum: 612.3094472885132
time_elpased: 1.837
batch start
#iterations: 2
currently lose_sum: 608.290561914444
time_elpased: 1.846
batch start
#iterations: 3
currently lose_sum: 610.6464625000954
time_elpased: 1.872
batch start
#iterations: 4
currently lose_sum: 608.9706861376762
time_elpased: 1.897
batch start
#iterations: 5
currently lose_sum: 608.2979077100754
time_elpased: 1.863
batch start
#iterations: 6
currently lose_sum: 607.2478667497635
time_elpased: 1.825
batch start
#iterations: 7
currently lose_sum: 607.0720859766006
time_elpased: 1.897
batch start
#iterations: 8
currently lose_sum: 607.0209797620773
time_elpased: 1.902
batch start
#iterations: 9
currently lose_sum: 606.5198080539703
time_elpased: 1.914
batch start
#iterations: 10
currently lose_sum: 604.6620018482208
time_elpased: 1.842
batch start
#iterations: 11
currently lose_sum: 604.7681751251221
time_elpased: 1.855
batch start
#iterations: 12
currently lose_sum: 605.4829468131065
time_elpased: 1.864
batch start
#iterations: 13
currently lose_sum: 607.9734038114548
time_elpased: 1.838
batch start
#iterations: 14
currently lose_sum: 606.2457394599915
time_elpased: 1.85
batch start
#iterations: 15
currently lose_sum: 605.1069974899292
time_elpased: 1.851
batch start
#iterations: 16
currently lose_sum: 604.5928422808647
time_elpased: 1.856
batch start
#iterations: 17
currently lose_sum: 604.151763856411
time_elpased: 1.845
batch start
#iterations: 18
currently lose_sum: 605.4783304333687
time_elpased: 1.861
batch start
#iterations: 19
currently lose_sum: 605.5417046546936
time_elpased: 1.822
start validation test
0.547728531856
0.545248012117
0.963260265514
0.696337902431
0
validation finish
batch start
#iterations: 20
currently lose_sum: 605.4337522983551
time_elpased: 1.835
batch start
#iterations: 21
currently lose_sum: 605.1604542136192
time_elpased: 1.836
batch start
#iterations: 22
currently lose_sum: 603.8087303042412
time_elpased: 1.852
batch start
#iterations: 23
currently lose_sum: 604.5585063099861
time_elpased: 1.807
batch start
#iterations: 24
currently lose_sum: 605.7890270352364
time_elpased: 1.866
batch start
#iterations: 25
currently lose_sum: 605.2525712251663
time_elpased: 1.828
batch start
#iterations: 26
currently lose_sum: 604.0918300747871
time_elpased: 1.845
batch start
#iterations: 27
currently lose_sum: 603.3424788713455
time_elpased: 1.843
batch start
#iterations: 28
currently lose_sum: 604.4247325062752
time_elpased: 1.792
batch start
#iterations: 29
currently lose_sum: 604.0285803079605
time_elpased: 1.835
batch start
#iterations: 30
currently lose_sum: 605.9800172448158
time_elpased: 1.826
batch start
#iterations: 31
currently lose_sum: 603.3244841694832
time_elpased: 1.83
batch start
#iterations: 32
currently lose_sum: 604.0383262038231
time_elpased: 1.82
batch start
#iterations: 33
currently lose_sum: 604.2696412205696
time_elpased: 1.806
batch start
#iterations: 34
currently lose_sum: 604.6191098690033
time_elpased: 1.826
batch start
#iterations: 35
currently lose_sum: 604.9180690646172
time_elpased: 1.81
batch start
#iterations: 36
currently lose_sum: 603.5696583390236
time_elpased: 1.8
batch start
#iterations: 37
currently lose_sum: 604.9521515965462
time_elpased: 1.825
batch start
#iterations: 38
currently lose_sum: 604.057499229908
time_elpased: 1.834
batch start
#iterations: 39
currently lose_sum: 604.2170785665512
time_elpased: 1.841
start validation test
0.545706371191
0.54391754965
0.966759287846
0.696161256855
0
validation finish
batch start
#iterations: 40
currently lose_sum: 603.4952776432037
time_elpased: 1.845
batch start
#iterations: 41
currently lose_sum: 604.9247843027115
time_elpased: 1.822
batch start
#iterations: 42
currently lose_sum: 604.2869848608971
time_elpased: 1.836
batch start
#iterations: 43
currently lose_sum: 602.2717128396034
time_elpased: 1.814
batch start
#iterations: 44
currently lose_sum: 604.8188459277153
time_elpased: 1.811
batch start
#iterations: 45
currently lose_sum: 602.9711091518402
time_elpased: 1.833
batch start
#iterations: 46
currently lose_sum: 604.9264479279518
time_elpased: 1.823
batch start
#iterations: 47
currently lose_sum: 603.662554204464
time_elpased: 1.801
batch start
#iterations: 48
currently lose_sum: 603.5206672549248
time_elpased: 1.819
batch start
#iterations: 49
currently lose_sum: 603.5686658620834
time_elpased: 1.822
batch start
#iterations: 50
currently lose_sum: 605.0764766335487
time_elpased: 1.814
batch start
#iterations: 51
currently lose_sum: 603.8346343040466
time_elpased: 1.821
batch start
#iterations: 52
currently lose_sum: 604.8389208912849
time_elpased: 1.838
batch start
#iterations: 53
currently lose_sum: 603.5070178508759
time_elpased: 1.814
batch start
#iterations: 54
currently lose_sum: 604.8366211652756
time_elpased: 1.816
batch start
#iterations: 55
currently lose_sum: 605.0349295735359
time_elpased: 1.812
batch start
#iterations: 56
currently lose_sum: 604.1219974160194
time_elpased: 1.808
batch start
#iterations: 57
currently lose_sum: 603.7812528014183
time_elpased: 1.807
batch start
#iterations: 58
currently lose_sum: 603.0997910499573
time_elpased: 1.786
batch start
#iterations: 59
currently lose_sum: 604.5494705438614
time_elpased: 1.796
start validation test
0.547008310249
0.544606920515
0.967788412061
0.696992718042
0
validation finish
batch start
#iterations: 60
currently lose_sum: 602.5830626487732
time_elpased: 1.841
batch start
#iterations: 61
currently lose_sum: 603.5188310742378
time_elpased: 1.809
batch start
#iterations: 62
currently lose_sum: 604.8053423166275
time_elpased: 1.819
batch start
#iterations: 63
currently lose_sum: 604.0439648628235
time_elpased: 1.847
batch start
#iterations: 64
currently lose_sum: 603.5824529528618
time_elpased: 1.808
batch start
#iterations: 65
currently lose_sum: 602.4061194062233
time_elpased: 1.823
batch start
#iterations: 66
currently lose_sum: 603.0240141749382
time_elpased: 1.798
batch start
#iterations: 67
currently lose_sum: 603.2652803063393
time_elpased: 1.825
batch start
#iterations: 68
currently lose_sum: 603.308503985405
time_elpased: 1.819
batch start
#iterations: 69
currently lose_sum: 604.4941392540932
time_elpased: 1.811
batch start
#iterations: 70
currently lose_sum: 601.2777702212334
time_elpased: 1.804
batch start
#iterations: 71
currently lose_sum: 604.0249453186989
time_elpased: 1.809
batch start
#iterations: 72
currently lose_sum: 602.1441541314125
time_elpased: 1.812
batch start
#iterations: 73
currently lose_sum: 604.1879348754883
time_elpased: 1.801
batch start
#iterations: 74
currently lose_sum: 603.5408959984779
time_elpased: 1.81
batch start
#iterations: 75
currently lose_sum: 602.6650233268738
time_elpased: 1.825
batch start
#iterations: 76
currently lose_sum: 605.0098625421524
time_elpased: 1.784
batch start
#iterations: 77
currently lose_sum: 602.9073954820633
time_elpased: 1.9
batch start
#iterations: 78
currently lose_sum: 604.7939731478691
time_elpased: 1.925
batch start
#iterations: 79
currently lose_sum: 604.2309680581093
time_elpased: 1.908
start validation test
0.545761772853
0.543690997007
0.972007821344
0.697331019971
0
validation finish
batch start
#iterations: 80
currently lose_sum: 603.4850547909737
time_elpased: 1.827
batch start
#iterations: 81
currently lose_sum: 604.2153649926186
time_elpased: 1.805
batch start
#iterations: 82
currently lose_sum: 605.162145793438
time_elpased: 1.818
batch start
#iterations: 83
currently lose_sum: 604.3713997602463
time_elpased: 1.798
batch start
#iterations: 84
currently lose_sum: 603.598712682724
time_elpased: 1.802
batch start
#iterations: 85
currently lose_sum: 601.9943413734436
time_elpased: 1.826
batch start
#iterations: 86
currently lose_sum: 603.1680939793587
time_elpased: 1.799
batch start
#iterations: 87
currently lose_sum: 603.5003643035889
time_elpased: 1.816
batch start
#iterations: 88
currently lose_sum: 602.5276880264282
time_elpased: 1.934
batch start
#iterations: 89
currently lose_sum: 602.1603430509567
time_elpased: 1.817
batch start
#iterations: 90
currently lose_sum: 602.5191326737404
time_elpased: 1.815
batch start
#iterations: 91
currently lose_sum: 603.2914505004883
time_elpased: 1.828
batch start
#iterations: 92
currently lose_sum: 605.2656064033508
time_elpased: 1.816
batch start
#iterations: 93
currently lose_sum: 601.8712126016617
time_elpased: 1.793
batch start
#iterations: 94
currently lose_sum: 604.7816677093506
time_elpased: 1.803
batch start
#iterations: 95
currently lose_sum: 602.4483350515366
time_elpased: 1.805
batch start
#iterations: 96
currently lose_sum: 604.7087131142616
time_elpased: 1.795
batch start
#iterations: 97
currently lose_sum: 602.6019268035889
time_elpased: 1.783
batch start
#iterations: 98
currently lose_sum: 603.7840278148651
time_elpased: 1.757
batch start
#iterations: 99
currently lose_sum: 602.1371070146561
time_elpased: 1.812
start validation test
0.549141274238
0.546394781689
0.956879695379
0.695593626094
0
validation finish
batch start
#iterations: 100
currently lose_sum: 604.0814014673233
time_elpased: 1.821
batch start
#iterations: 101
currently lose_sum: 602.3689027428627
time_elpased: 1.828
batch start
#iterations: 102
currently lose_sum: 604.262154519558
time_elpased: 1.804
batch start
#iterations: 103
currently lose_sum: 601.7319188714027
time_elpased: 1.821
batch start
#iterations: 104
currently lose_sum: 602.8423858880997
time_elpased: 1.792
batch start
#iterations: 105
currently lose_sum: 602.0179899334908
time_elpased: 1.813
batch start
#iterations: 106
currently lose_sum: 603.8519537448883
time_elpased: 1.811
batch start
#iterations: 107
currently lose_sum: 603.7343349456787
time_elpased: 1.816
batch start
#iterations: 108
currently lose_sum: 604.8597146272659
time_elpased: 1.796
batch start
#iterations: 109
currently lose_sum: 603.7870783209801
time_elpased: 1.838
batch start
#iterations: 110
currently lose_sum: 603.9946734905243
time_elpased: 1.789
batch start
#iterations: 111
currently lose_sum: 604.5057401657104
time_elpased: 1.807
batch start
#iterations: 112
currently lose_sum: 603.8045436143875
time_elpased: 1.81
batch start
#iterations: 113
currently lose_sum: 604.1542251706123
time_elpased: 1.801
batch start
#iterations: 114
currently lose_sum: 603.3625876903534
time_elpased: 1.811
batch start
#iterations: 115
currently lose_sum: 602.6782190799713
time_elpased: 1.832
batch start
#iterations: 116
currently lose_sum: 603.9106366038322
time_elpased: 1.816
batch start
#iterations: 117
currently lose_sum: 602.0862767696381
time_elpased: 1.795
batch start
#iterations: 118
currently lose_sum: 604.517532646656
time_elpased: 1.821
batch start
#iterations: 119
currently lose_sum: 604.8416202664375
time_elpased: 1.795
start validation test
0.548670360111
0.545772492787
0.963569002779
0.696846218253
0
validation finish
batch start
#iterations: 120
currently lose_sum: 604.2466498613358
time_elpased: 1.837
batch start
#iterations: 121
currently lose_sum: 602.39091360569
time_elpased: 1.847
batch start
#iterations: 122
currently lose_sum: 603.3489056825638
time_elpased: 1.82
batch start
#iterations: 123
currently lose_sum: 603.4955108761787
time_elpased: 1.825
batch start
#iterations: 124
currently lose_sum: 601.7531251311302
time_elpased: 1.803
batch start
#iterations: 125
currently lose_sum: 605.3388913273811
time_elpased: 1.818
batch start
#iterations: 126
currently lose_sum: 604.6981719136238
time_elpased: 1.785
batch start
#iterations: 127
currently lose_sum: 604.0534504652023
time_elpased: 1.787
batch start
#iterations: 128
currently lose_sum: 601.2081179022789
time_elpased: 1.816
batch start
#iterations: 129
currently lose_sum: 602.8510308265686
time_elpased: 1.825
batch start
#iterations: 130
currently lose_sum: 603.1536364555359
time_elpased: 1.806
batch start
#iterations: 131
currently lose_sum: 602.1048335433006
time_elpased: 1.828
batch start
#iterations: 132
currently lose_sum: 602.7630423903465
time_elpased: 1.811
batch start
#iterations: 133
currently lose_sum: 603.6023133993149
time_elpased: 1.803
batch start
#iterations: 134
currently lose_sum: 603.2281919717789
time_elpased: 1.828
batch start
#iterations: 135
currently lose_sum: 602.7541801333427
time_elpased: 1.843
batch start
#iterations: 136
currently lose_sum: 604.3131504058838
time_elpased: 1.8
batch start
#iterations: 137
currently lose_sum: 602.9202563762665
time_elpased: 1.812
batch start
#iterations: 138
currently lose_sum: 602.4102081656456
time_elpased: 1.838
batch start
#iterations: 139
currently lose_sum: 603.963122010231
time_elpased: 1.844
start validation test
0.546897506925
0.544502617801
0.968611711434
0.697120636978
0
validation finish
batch start
#iterations: 140
currently lose_sum: 601.591458261013
time_elpased: 1.827
batch start
#iterations: 141
currently lose_sum: 604.1505417227745
time_elpased: 1.808
batch start
#iterations: 142
currently lose_sum: 603.4277520179749
time_elpased: 1.829
batch start
#iterations: 143
currently lose_sum: 603.0330513119698
time_elpased: 1.83
batch start
#iterations: 144
currently lose_sum: 604.4736062288284
time_elpased: 1.819
batch start
#iterations: 145
currently lose_sum: 603.4566918611526
time_elpased: 1.829
batch start
#iterations: 146
currently lose_sum: 603.9105093479156
time_elpased: 1.824
batch start
#iterations: 147
currently lose_sum: 604.0355731844902
time_elpased: 1.827
batch start
#iterations: 148
currently lose_sum: 603.6008114218712
time_elpased: 1.796
batch start
#iterations: 149
currently lose_sum: 602.7720879912376
time_elpased: 1.828
batch start
#iterations: 150
currently lose_sum: 602.3751474618912
time_elpased: 1.847
batch start
#iterations: 151
currently lose_sum: 602.4977439045906
time_elpased: 1.944
batch start
#iterations: 152
currently lose_sum: 603.3380963802338
time_elpased: 1.979
batch start
#iterations: 153
currently lose_sum: 602.9399909973145
time_elpased: 1.867
batch start
#iterations: 154
currently lose_sum: 604.2065389752388
time_elpased: 1.81
batch start
#iterations: 155
currently lose_sum: 602.6314271688461
time_elpased: 1.873
batch start
#iterations: 156
currently lose_sum: 603.3307309746742
time_elpased: 1.952
batch start
#iterations: 157
currently lose_sum: 603.4784954786301
time_elpased: 1.926
batch start
#iterations: 158
currently lose_sum: 602.7080090045929
time_elpased: 1.901
batch start
#iterations: 159
currently lose_sum: 602.8943623304367
time_elpased: 1.921
start validation test
0.547783933518
0.545274501238
0.963363177936
0.696386393647
0
validation finish
batch start
#iterations: 160
currently lose_sum: 602.5924794077873
time_elpased: 1.854
batch start
#iterations: 161
currently lose_sum: 603.20205950737
time_elpased: 1.851
batch start
#iterations: 162
currently lose_sum: 602.8312996029854
time_elpased: 1.918
batch start
#iterations: 163
currently lose_sum: 603.4938839673996
time_elpased: 1.951
batch start
#iterations: 164
currently lose_sum: 602.3262286186218
time_elpased: 1.902
batch start
#iterations: 165
currently lose_sum: 603.9445453286171
time_elpased: 1.884
batch start
#iterations: 166
currently lose_sum: 603.8059636950493
time_elpased: 1.867
batch start
#iterations: 167
currently lose_sum: 603.0686475038528
time_elpased: 1.853
batch start
#iterations: 168
currently lose_sum: 603.485443174839
time_elpased: 1.875
batch start
#iterations: 169
currently lose_sum: 603.8659847378731
time_elpased: 1.924
batch start
#iterations: 170
currently lose_sum: 603.7059520483017
time_elpased: 1.921
batch start
#iterations: 171
currently lose_sum: 600.9299370646477
time_elpased: 1.871
batch start
#iterations: 172
currently lose_sum: 603.3613788485527
time_elpased: 1.842
batch start
#iterations: 173
currently lose_sum: 604.2228475809097
time_elpased: 1.914
batch start
#iterations: 174
currently lose_sum: 602.394073843956
time_elpased: 1.909
batch start
#iterations: 175
currently lose_sum: 602.6646577119827
time_elpased: 1.867
batch start
#iterations: 176
currently lose_sum: 602.5000959634781
time_elpased: 1.842
batch start
#iterations: 177
currently lose_sum: 603.4517847299576
time_elpased: 1.838
batch start
#iterations: 178
currently lose_sum: 603.7088860869408
time_elpased: 1.836
batch start
#iterations: 179
currently lose_sum: 602.5776581168175
time_elpased: 1.868
start validation test
0.546537396122
0.544078720221
0.973036945559
0.697914744418
0
validation finish
batch start
#iterations: 180
currently lose_sum: 603.0100609660149
time_elpased: 1.859
batch start
#iterations: 181
currently lose_sum: 603.1882389783859
time_elpased: 1.864
batch start
#iterations: 182
currently lose_sum: 603.247922539711
time_elpased: 1.853
batch start
#iterations: 183
currently lose_sum: 603.3284667134285
time_elpased: 1.844
batch start
#iterations: 184
currently lose_sum: 603.50355052948
time_elpased: 1.895
batch start
#iterations: 185
currently lose_sum: 603.8494553565979
time_elpased: 1.915
batch start
#iterations: 186
currently lose_sum: 603.9061179161072
time_elpased: 1.832
batch start
#iterations: 187
currently lose_sum: 602.9999311566353
time_elpased: 1.846
batch start
#iterations: 188
currently lose_sum: 602.2874138355255
time_elpased: 1.814
batch start
#iterations: 189
currently lose_sum: 603.424174785614
time_elpased: 1.852
batch start
#iterations: 190
currently lose_sum: 603.3194246292114
time_elpased: 1.899
batch start
#iterations: 191
currently lose_sum: 602.2487681508064
time_elpased: 1.84
batch start
#iterations: 192
currently lose_sum: 604.3199661970139
time_elpased: 1.829
batch start
#iterations: 193
currently lose_sum: 602.0585429668427
time_elpased: 1.837
batch start
#iterations: 194
currently lose_sum: 602.4608727693558
time_elpased: 1.822
batch start
#iterations: 195
currently lose_sum: 602.8667687177658
time_elpased: 1.834
batch start
#iterations: 196
currently lose_sum: 604.1824851036072
time_elpased: 1.831
batch start
#iterations: 197
currently lose_sum: 602.882099211216
time_elpased: 1.844
batch start
#iterations: 198
currently lose_sum: 602.0569490790367
time_elpased: 1.839
batch start
#iterations: 199
currently lose_sum: 604.5026741027832
time_elpased: 1.847
start validation test
0.548393351801
0.545752111524
0.960893279819
0.696128683529
0
validation finish
batch start
#iterations: 200
currently lose_sum: 603.5790337324142
time_elpased: 1.832
batch start
#iterations: 201
currently lose_sum: 603.6673669219017
time_elpased: 1.84
batch start
#iterations: 202
currently lose_sum: 602.6299006342888
time_elpased: 1.823
batch start
#iterations: 203
currently lose_sum: 603.5962597131729
time_elpased: 1.842
batch start
#iterations: 204
currently lose_sum: 602.1255322694778
time_elpased: 1.827
batch start
#iterations: 205
currently lose_sum: 603.4974926710129
time_elpased: 1.837
batch start
#iterations: 206
currently lose_sum: 602.8552393317223
time_elpased: 1.847
batch start
#iterations: 207
currently lose_sum: 603.1206879019737
time_elpased: 1.839
batch start
#iterations: 208
currently lose_sum: 602.5346870422363
time_elpased: 1.816
batch start
#iterations: 209
currently lose_sum: 602.8189648389816
time_elpased: 1.823
batch start
#iterations: 210
currently lose_sum: 603.3458377122879
time_elpased: 1.798
batch start
#iterations: 211
currently lose_sum: 603.2254675626755
time_elpased: 1.821
batch start
#iterations: 212
currently lose_sum: 603.2186529040337
time_elpased: 1.834
batch start
#iterations: 213
currently lose_sum: 601.3703090548515
time_elpased: 1.854
batch start
#iterations: 214
currently lose_sum: 602.5864753723145
time_elpased: 1.826
batch start
#iterations: 215
currently lose_sum: 602.8626368641853
time_elpased: 1.857
batch start
#iterations: 216
currently lose_sum: 604.2409076094627
time_elpased: 1.833
batch start
#iterations: 217
currently lose_sum: 602.0198833942413
time_elpased: 1.817
batch start
#iterations: 218
currently lose_sum: 604.3789060115814
time_elpased: 1.824
batch start
#iterations: 219
currently lose_sum: 602.1683822870255
time_elpased: 1.834
start validation test
0.548725761773
0.545814990817
0.963363177936
0.696827021494
0
validation finish
batch start
#iterations: 220
currently lose_sum: 603.3278552293777
time_elpased: 1.857
batch start
#iterations: 221
currently lose_sum: 603.4650338888168
time_elpased: 1.829
batch start
#iterations: 222
currently lose_sum: 603.3544620871544
time_elpased: 1.852
batch start
#iterations: 223
currently lose_sum: 604.4373212456703
time_elpased: 1.849
batch start
#iterations: 224
currently lose_sum: 601.9153712391853
time_elpased: 1.835
batch start
#iterations: 225
currently lose_sum: 602.4705564379692
time_elpased: 1.849
batch start
#iterations: 226
currently lose_sum: 603.9879114627838
time_elpased: 1.823
batch start
#iterations: 227
currently lose_sum: 603.4713001251221
time_elpased: 1.846
batch start
#iterations: 228
currently lose_sum: 604.999929189682
time_elpased: 1.836
batch start
#iterations: 229
currently lose_sum: 603.9199904799461
time_elpased: 1.86
batch start
#iterations: 230
currently lose_sum: 603.4811935424805
time_elpased: 1.839
batch start
#iterations: 231
currently lose_sum: 601.949033677578
time_elpased: 1.843
batch start
#iterations: 232
currently lose_sum: 602.7813995480537
time_elpased: 1.838
batch start
#iterations: 233
currently lose_sum: 603.2780049443245
time_elpased: 1.873
batch start
#iterations: 234
currently lose_sum: 602.3026788234711
time_elpased: 1.83
batch start
#iterations: 235
currently lose_sum: 602.85343927145
time_elpased: 1.844
batch start
#iterations: 236
currently lose_sum: 604.6595641970634
time_elpased: 1.853
batch start
#iterations: 237
currently lose_sum: 603.2917465567589
time_elpased: 1.854
batch start
#iterations: 238
currently lose_sum: 601.583471596241
time_elpased: 1.836
batch start
#iterations: 239
currently lose_sum: 602.7205594778061
time_elpased: 1.834
start validation test
0.548476454294
0.545370316753
0.969229185963
0.697991551175
0
validation finish
batch start
#iterations: 240
currently lose_sum: 601.9881112575531
time_elpased: 1.848
batch start
#iterations: 241
currently lose_sum: 603.1779869198799
time_elpased: 1.863
batch start
#iterations: 242
currently lose_sum: 603.1964800953865
time_elpased: 1.813
batch start
#iterations: 243
currently lose_sum: 602.5760282874107
time_elpased: 1.836
batch start
#iterations: 244
currently lose_sum: 602.816089451313
time_elpased: 1.823
batch start
#iterations: 245
currently lose_sum: 603.9317362308502
time_elpased: 1.835
batch start
#iterations: 246
currently lose_sum: 602.7268089652061
time_elpased: 1.806
batch start
#iterations: 247
currently lose_sum: 602.752283513546
time_elpased: 1.85
batch start
#iterations: 248
currently lose_sum: 601.5009073615074
time_elpased: 1.845
batch start
#iterations: 249
currently lose_sum: 604.2646811008453
time_elpased: 1.846
batch start
#iterations: 250
currently lose_sum: 602.1237758398056
time_elpased: 1.813
batch start
#iterations: 251
currently lose_sum: 603.290509045124
time_elpased: 1.816
batch start
#iterations: 252
currently lose_sum: 603.330587208271
time_elpased: 1.833
batch start
#iterations: 253
currently lose_sum: 602.8029543161392
time_elpased: 1.85
batch start
#iterations: 254
currently lose_sum: 602.6040795445442
time_elpased: 1.817
batch start
#iterations: 255
currently lose_sum: 601.1697934269905
time_elpased: 1.824
batch start
#iterations: 256
currently lose_sum: 603.6929488778114
time_elpased: 1.807
batch start
#iterations: 257
currently lose_sum: 604.2505175471306
time_elpased: 1.852
batch start
#iterations: 258
currently lose_sum: 601.5818449854851
time_elpased: 1.801
batch start
#iterations: 259
currently lose_sum: 603.160196185112
time_elpased: 1.833
start validation test
0.547479224377
0.544637196703
0.972522383452
0.698241465938
0
validation finish
batch start
#iterations: 260
currently lose_sum: 602.8208151459694
time_elpased: 1.83
batch start
#iterations: 261
currently lose_sum: 604.2986637353897
time_elpased: 1.845
batch start
#iterations: 262
currently lose_sum: 601.3508847355843
time_elpased: 1.823
batch start
#iterations: 263
currently lose_sum: 602.8190430402756
time_elpased: 1.839
batch start
#iterations: 264
currently lose_sum: 602.4552069306374
time_elpased: 1.847
batch start
#iterations: 265
currently lose_sum: 603.2211021184921
time_elpased: 1.841
batch start
#iterations: 266
currently lose_sum: 601.8879359960556
time_elpased: 1.829
batch start
#iterations: 267
currently lose_sum: 601.7355480194092
time_elpased: 1.816
batch start
#iterations: 268
currently lose_sum: 603.0614260435104
time_elpased: 1.826
batch start
#iterations: 269
currently lose_sum: 603.0134716629982
time_elpased: 1.802
batch start
#iterations: 270
currently lose_sum: 602.7847872376442
time_elpased: 1.822
batch start
#iterations: 271
currently lose_sum: 603.3801888823509
time_elpased: 1.826
batch start
#iterations: 272
currently lose_sum: 604.4946656823158
time_elpased: 1.795
batch start
#iterations: 273
currently lose_sum: 602.0605311393738
time_elpased: 1.82
batch start
#iterations: 274
currently lose_sum: 601.7269662618637
time_elpased: 1.816
batch start
#iterations: 275
currently lose_sum: 603.6311313509941
time_elpased: 1.855
batch start
#iterations: 276
currently lose_sum: 603.3048868775368
time_elpased: 1.83
batch start
#iterations: 277
currently lose_sum: 601.974762737751
time_elpased: 1.831
batch start
#iterations: 278
currently lose_sum: 603.641369998455
time_elpased: 1.826
batch start
#iterations: 279
currently lose_sum: 603.1016433238983
time_elpased: 1.818
start validation test
0.54864265928
0.545879602572
0.961202017083
0.696313415589
0
validation finish
batch start
#iterations: 280
currently lose_sum: 602.8375973701477
time_elpased: 1.776
batch start
#iterations: 281
currently lose_sum: 602.2576893568039
time_elpased: 1.808
batch start
#iterations: 282
currently lose_sum: 603.61113935709
time_elpased: 1.821
batch start
#iterations: 283
currently lose_sum: 603.4856963157654
time_elpased: 1.824
batch start
#iterations: 284
currently lose_sum: 602.878864467144
time_elpased: 1.805
batch start
#iterations: 285
currently lose_sum: 603.1653398871422
time_elpased: 1.795
batch start
#iterations: 286
currently lose_sum: 603.0016643404961
time_elpased: 1.807
batch start
#iterations: 287
currently lose_sum: 603.1062632203102
time_elpased: 1.812
batch start
#iterations: 288
currently lose_sum: 601.1356499195099
time_elpased: 1.827
batch start
#iterations: 289
currently lose_sum: 603.093141913414
time_elpased: 1.785
batch start
#iterations: 290
currently lose_sum: 603.9631459116936
time_elpased: 1.794
batch start
#iterations: 291
currently lose_sum: 602.6787974834442
time_elpased: 1.791
batch start
#iterations: 292
currently lose_sum: 603.2892590761185
time_elpased: 1.803
batch start
#iterations: 293
currently lose_sum: 603.735957980156
time_elpased: 1.809
batch start
#iterations: 294
currently lose_sum: 603.6550045013428
time_elpased: 1.853
batch start
#iterations: 295
currently lose_sum: 602.8798868060112
time_elpased: 1.821
batch start
#iterations: 296
currently lose_sum: 601.7042325735092
time_elpased: 1.822
batch start
#iterations: 297
currently lose_sum: 603.5776594281197
time_elpased: 1.8
batch start
#iterations: 298
currently lose_sum: 603.0729395747185
time_elpased: 1.825
batch start
#iterations: 299
currently lose_sum: 602.3895679116249
time_elpased: 1.809
start validation test
0.548199445983
0.545422821915
0.965112689102
0.696964066739
0
validation finish
batch start
#iterations: 300
currently lose_sum: 603.6813658475876
time_elpased: 1.838
batch start
#iterations: 301
currently lose_sum: 601.4592670798302
time_elpased: 1.815
batch start
#iterations: 302
currently lose_sum: 602.9293995499611
time_elpased: 1.813
batch start
#iterations: 303
currently lose_sum: 603.4432416558266
time_elpased: 1.809
batch start
#iterations: 304
currently lose_sum: 601.5745658278465
time_elpased: 1.827
batch start
#iterations: 305
currently lose_sum: 603.666793525219
time_elpased: 1.829
batch start
#iterations: 306
currently lose_sum: 604.4764420390129
time_elpased: 1.843
batch start
#iterations: 307
currently lose_sum: 603.0960713624954
time_elpased: 1.821
batch start
#iterations: 308
currently lose_sum: 602.4794087409973
time_elpased: 1.85
batch start
#iterations: 309
currently lose_sum: 602.1804874539375
time_elpased: 1.815
batch start
#iterations: 310
currently lose_sum: 603.5232045054436
time_elpased: 1.818
batch start
#iterations: 311
currently lose_sum: 603.1988234519958
time_elpased: 1.818
batch start
#iterations: 312
currently lose_sum: 602.1143996119499
time_elpased: 1.817
batch start
#iterations: 313
currently lose_sum: 602.2359699010849
time_elpased: 1.81
batch start
#iterations: 314
currently lose_sum: 603.0853564739227
time_elpased: 1.828
batch start
#iterations: 315
currently lose_sum: 602.8283893465996
time_elpased: 1.826
batch start
#iterations: 316
currently lose_sum: 601.4049026966095
time_elpased: 1.823
batch start
#iterations: 317
currently lose_sum: 602.3407743573189
time_elpased: 1.83
batch start
#iterations: 318
currently lose_sum: 601.3061239719391
time_elpased: 1.816
batch start
#iterations: 319
currently lose_sum: 602.0174371600151
time_elpased: 1.823
start validation test
0.547645429363
0.545237262446
0.962539878563
0.696140820959
0
validation finish
batch start
#iterations: 320
currently lose_sum: 603.8345031142235
time_elpased: 1.834
batch start
#iterations: 321
currently lose_sum: 603.0010421276093
time_elpased: 1.794
batch start
#iterations: 322
currently lose_sum: 602.9498056173325
time_elpased: 1.805
batch start
#iterations: 323
currently lose_sum: 603.8095501065254
time_elpased: 1.833
batch start
#iterations: 324
currently lose_sum: 602.5989729166031
time_elpased: 1.819
batch start
#iterations: 325
currently lose_sum: 603.8883408904076
time_elpased: 1.812
batch start
#iterations: 326
currently lose_sum: 602.4359607696533
time_elpased: 1.804
batch start
#iterations: 327
currently lose_sum: 604.1505274176598
time_elpased: 1.847
batch start
#iterations: 328
currently lose_sum: 604.16963493824
time_elpased: 1.815
batch start
#iterations: 329
currently lose_sum: 602.5696503520012
time_elpased: 1.839
batch start
#iterations: 330
currently lose_sum: 603.1620265245438
time_elpased: 1.812
batch start
#iterations: 331
currently lose_sum: 601.9036047458649
time_elpased: 1.841
batch start
#iterations: 332
currently lose_sum: 603.5447798371315
time_elpased: 1.84
batch start
#iterations: 333
currently lose_sum: 603.4342388510704
time_elpased: 1.829
batch start
#iterations: 334
currently lose_sum: 604.3917402029037
time_elpased: 1.81
batch start
#iterations: 335
currently lose_sum: 603.101420879364
time_elpased: 1.821
batch start
#iterations: 336
currently lose_sum: 602.034977555275
time_elpased: 1.823
batch start
#iterations: 337
currently lose_sum: 601.8611739873886
time_elpased: 1.789
batch start
#iterations: 338
currently lose_sum: 602.9634975194931
time_elpased: 1.828
batch start
#iterations: 339
currently lose_sum: 602.0729518532753
time_elpased: 1.828
start validation test
0.547811634349
0.545054180912
0.967994236904
0.697412322978
0
validation finish
batch start
#iterations: 340
currently lose_sum: 602.9844886064529
time_elpased: 1.838
batch start
#iterations: 341
currently lose_sum: 602.9246115684509
time_elpased: 1.83
batch start
#iterations: 342
currently lose_sum: 603.1161332130432
time_elpased: 1.819
batch start
#iterations: 343
currently lose_sum: 603.4897604584694
time_elpased: 1.812
batch start
#iterations: 344
currently lose_sum: 602.7402392625809
time_elpased: 1.844
batch start
#iterations: 345
currently lose_sum: 601.7776302695274
time_elpased: 1.834
batch start
#iterations: 346
currently lose_sum: 604.133477807045
time_elpased: 1.851
batch start
#iterations: 347
currently lose_sum: 603.1008847355843
time_elpased: 1.846
batch start
#iterations: 348
currently lose_sum: 602.6371004581451
time_elpased: 1.839
batch start
#iterations: 349
currently lose_sum: 601.8922924399376
time_elpased: 1.817
batch start
#iterations: 350
currently lose_sum: 602.2109742164612
time_elpased: 1.834
batch start
#iterations: 351
currently lose_sum: 603.3275021910667
time_elpased: 1.831
batch start
#iterations: 352
currently lose_sum: 602.883796453476
time_elpased: 1.803
batch start
#iterations: 353
currently lose_sum: 605.5250830054283
time_elpased: 1.824
batch start
#iterations: 354
currently lose_sum: 602.4905388355255
time_elpased: 1.789
batch start
#iterations: 355
currently lose_sum: 603.026782810688
time_elpased: 1.795
batch start
#iterations: 356
currently lose_sum: 604.2496168613434
time_elpased: 1.797
batch start
#iterations: 357
currently lose_sum: 602.9606040716171
time_elpased: 1.831
batch start
#iterations: 358
currently lose_sum: 602.6823756694794
time_elpased: 1.83
batch start
#iterations: 359
currently lose_sum: 604.3267918229103
time_elpased: 1.813
start validation test
0.548614958449
0.545869012479
0.961099104662
0.696277795381
0
validation finish
batch start
#iterations: 360
currently lose_sum: 604.2958977222443
time_elpased: 1.827
batch start
#iterations: 361
currently lose_sum: 602.6722081303596
time_elpased: 1.803
batch start
#iterations: 362
currently lose_sum: 603.1325623393059
time_elpased: 1.805
batch start
#iterations: 363
currently lose_sum: 604.061517894268
time_elpased: 1.849
batch start
#iterations: 364
currently lose_sum: 603.0675463080406
time_elpased: 1.833
batch start
#iterations: 365
currently lose_sum: 602.8178260326385
time_elpased: 1.849
batch start
#iterations: 366
currently lose_sum: 604.2251297235489
time_elpased: 1.841
batch start
#iterations: 367
currently lose_sum: 603.5044716596603
time_elpased: 1.783
batch start
#iterations: 368
currently lose_sum: 603.3457889556885
time_elpased: 1.815
batch start
#iterations: 369
currently lose_sum: 602.2562349438667
time_elpased: 1.809
batch start
#iterations: 370
currently lose_sum: 602.0206618309021
time_elpased: 1.799
batch start
#iterations: 371
currently lose_sum: 605.2895065546036
time_elpased: 1.796
batch start
#iterations: 372
currently lose_sum: 600.9542842507362
time_elpased: 1.787
batch start
#iterations: 373
currently lose_sum: 602.2374306321144
time_elpased: 1.776
batch start
#iterations: 374
currently lose_sum: 602.1521748900414
time_elpased: 1.82
batch start
#iterations: 375
currently lose_sum: 603.5275287628174
time_elpased: 1.814
batch start
#iterations: 376
currently lose_sum: 602.2741755843163
time_elpased: 1.824
batch start
#iterations: 377
currently lose_sum: 602.9983841180801
time_elpased: 1.82
batch start
#iterations: 378
currently lose_sum: 601.7645319104195
time_elpased: 1.814
batch start
#iterations: 379
currently lose_sum: 601.5849187374115
time_elpased: 1.787
start validation test
0.548559556787
0.545692894806
0.963877740043
0.696862037537
0
validation finish
batch start
#iterations: 380
currently lose_sum: 603.7193928360939
time_elpased: 1.831
batch start
#iterations: 381
currently lose_sum: 602.8298616409302
time_elpased: 1.871
batch start
#iterations: 382
currently lose_sum: 603.7006433010101
time_elpased: 1.916
batch start
#iterations: 383
currently lose_sum: 602.3760296106339
time_elpased: 1.848
batch start
#iterations: 384
currently lose_sum: 604.1126450300217
time_elpased: 1.842
batch start
#iterations: 385
currently lose_sum: 604.3839963078499
time_elpased: 1.857
batch start
#iterations: 386
currently lose_sum: 602.7289571762085
time_elpased: 1.849
batch start
#iterations: 387
currently lose_sum: 603.9521469473839
time_elpased: 1.831
batch start
#iterations: 388
currently lose_sum: 601.5653918981552
time_elpased: 1.81
batch start
#iterations: 389
currently lose_sum: 602.9586138725281
time_elpased: 1.868
batch start
#iterations: 390
currently lose_sum: 603.42101085186
time_elpased: 1.81
batch start
#iterations: 391
currently lose_sum: 603.125387430191
time_elpased: 1.833
batch start
#iterations: 392
currently lose_sum: 602.5892533063889
time_elpased: 1.851
batch start
#iterations: 393
currently lose_sum: 603.5749453306198
time_elpased: 1.828
batch start
#iterations: 394
currently lose_sum: 602.7858994603157
time_elpased: 1.83
batch start
#iterations: 395
currently lose_sum: 602.8913635015488
time_elpased: 1.847
batch start
#iterations: 396
currently lose_sum: 603.0751578807831
time_elpased: 1.84
batch start
#iterations: 397
currently lose_sum: 603.7234866023064
time_elpased: 1.847
batch start
#iterations: 398
currently lose_sum: 602.9805365800858
time_elpased: 1.832
batch start
#iterations: 399
currently lose_sum: 602.0302523970604
time_elpased: 1.853
start validation test
0.549113573407
0.546140130367
0.961407841927
0.69657936434
0
validation finish
acc: 0.546
pre: 0.544
rec: 0.972
F1: 0.698
auc: 0.000
