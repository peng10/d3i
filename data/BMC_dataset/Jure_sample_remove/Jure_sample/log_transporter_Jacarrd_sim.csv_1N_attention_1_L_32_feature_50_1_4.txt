start to construct graph
graph construct over
93366
epochs start
batch start
#iterations: 0
currently lose_sum: 618.9200463891029
time_elpased: 5.624
batch start
#iterations: 1
currently lose_sum: 613.3948881030083
time_elpased: 5.532
batch start
#iterations: 2
currently lose_sum: 611.1132515072823
time_elpased: 5.508
batch start
#iterations: 3
currently lose_sum: 609.8074392676353
time_elpased: 5.884
batch start
#iterations: 4
currently lose_sum: 609.0999465584755
time_elpased: 5.542
batch start
#iterations: 5
currently lose_sum: 607.7639311552048
time_elpased: 5.672
batch start
#iterations: 6
currently lose_sum: 607.4964010119438
time_elpased: 5.724
batch start
#iterations: 7
currently lose_sum: 606.693964600563
time_elpased: 5.669
batch start
#iterations: 8
currently lose_sum: 605.0020727515221
time_elpased: 5.666
batch start
#iterations: 9
currently lose_sum: 606.0662537813187
time_elpased: 5.673
batch start
#iterations: 10
currently lose_sum: 605.8081488609314
time_elpased: 5.687
batch start
#iterations: 11
currently lose_sum: 605.4795840382576
time_elpased: 5.361
batch start
#iterations: 12
currently lose_sum: 603.35191822052
time_elpased: 5.649
batch start
#iterations: 13
currently lose_sum: 605.6954128146172
time_elpased: 5.731
batch start
#iterations: 14
currently lose_sum: 604.2508121728897
time_elpased: 5.906
batch start
#iterations: 15
currently lose_sum: 603.4824190735817
time_elpased: 5.494
batch start
#iterations: 16
currently lose_sum: 603.082218170166
time_elpased: 5.88
batch start
#iterations: 17
currently lose_sum: 604.420794069767
time_elpased: 5.511
batch start
#iterations: 18
currently lose_sum: 602.9388983845711
time_elpased: 5.806
batch start
#iterations: 19
currently lose_sum: 604.9395635128021
time_elpased: 5.306
start validation test
0.54728531856
0.545385134937
0.955644746321
0.694449118477
0
validation finish
batch start
#iterations: 20
currently lose_sum: 602.9111347198486
time_elpased: 5.504
batch start
#iterations: 21
currently lose_sum: 603.5673829317093
time_elpased: 5.532
batch start
#iterations: 22
currently lose_sum: 603.9356223940849
time_elpased: 5.475
batch start
#iterations: 23
currently lose_sum: 603.4643728137016
time_elpased: 5.279
batch start
#iterations: 24
currently lose_sum: 602.2287226319313
time_elpased: 5.562
batch start
#iterations: 25
currently lose_sum: 603.6022567152977
time_elpased: 5.474
batch start
#iterations: 26
currently lose_sum: 601.7532582879066
time_elpased: 5.467
batch start
#iterations: 27
currently lose_sum: 602.3670320510864
time_elpased: 5.795
batch start
#iterations: 28
currently lose_sum: 600.6910981535912
time_elpased: 5.456
batch start
#iterations: 29
currently lose_sum: 602.0876774191856
time_elpased: 5.655
batch start
#iterations: 30
currently lose_sum: 602.1541808843613
time_elpased: 5.506
batch start
#iterations: 31
currently lose_sum: 602.8250647187233
time_elpased: 5.372
batch start
#iterations: 32
currently lose_sum: 600.623303592205
time_elpased: 5.623
batch start
#iterations: 33
currently lose_sum: 599.912739098072
time_elpased: 5.711
batch start
#iterations: 34
currently lose_sum: 601.6232303380966
time_elpased: 5.612
batch start
#iterations: 35
currently lose_sum: 602.1854328513145
time_elpased: 5.567
batch start
#iterations: 36
currently lose_sum: 601.6038411259651
time_elpased: 5.465
batch start
#iterations: 37
currently lose_sum: 602.0109956860542
time_elpased: 5.753
batch start
#iterations: 38
currently lose_sum: 600.6546154022217
time_elpased: 5.408
batch start
#iterations: 39
currently lose_sum: 602.0177261829376
time_elpased: 5.687
start validation test
0.548725761773
0.546802871013
0.944736029639
0.692686423572
0
validation finish
batch start
#iterations: 40
currently lose_sum: 601.2656468153
time_elpased: 5.467
batch start
#iterations: 41
currently lose_sum: 601.4503510594368
time_elpased: 5.592
batch start
#iterations: 42
currently lose_sum: 601.9285249710083
time_elpased: 5.562
batch start
#iterations: 43
currently lose_sum: 600.4727250337601
time_elpased: 5.515
batch start
#iterations: 44
currently lose_sum: 601.5782290101051
time_elpased: 5.533
batch start
#iterations: 45
currently lose_sum: 600.3339521288872
time_elpased: 5.404
batch start
#iterations: 46
currently lose_sum: 601.4231504797935
time_elpased: 5.406
batch start
#iterations: 47
currently lose_sum: 600.5018041133881
time_elpased: 5.419
batch start
#iterations: 48
currently lose_sum: 602.5069337487221
time_elpased: 5.693
batch start
#iterations: 49
currently lose_sum: 601.5369244813919
time_elpased: 5.434
batch start
#iterations: 50
currently lose_sum: 600.535155415535
time_elpased: 5.37
batch start
#iterations: 51
currently lose_sum: 600.2559388279915
time_elpased: 5.724
batch start
#iterations: 52
currently lose_sum: 600.6955878138542
time_elpased: 5.556
batch start
#iterations: 53
currently lose_sum: 601.4079676270485
time_elpased: 5.606
batch start
#iterations: 54
currently lose_sum: 600.9212020039558
time_elpased: 5.623
batch start
#iterations: 55
currently lose_sum: 599.5850734114647
time_elpased: 5.543
batch start
#iterations: 56
currently lose_sum: 599.4487033486366
time_elpased: 6.202
batch start
#iterations: 57
currently lose_sum: 599.7794013023376
time_elpased: 7.603
batch start
#iterations: 58
currently lose_sum: 603.1371366977692
time_elpased: 5.819
batch start
#iterations: 59
currently lose_sum: 598.7825922369957
time_elpased: 5.398
start validation test
0.549224376731
0.5475552881
0.936400123495
0.691032675768
0
validation finish
batch start
#iterations: 60
currently lose_sum: 600.489237844944
time_elpased: 5.29
batch start
#iterations: 61
currently lose_sum: 599.3203549981117
time_elpased: 5.344
batch start
#iterations: 62
currently lose_sum: 601.6083383560181
time_elpased: 5.329
batch start
#iterations: 63
currently lose_sum: 600.1923481822014
time_elpased: 5.542
batch start
#iterations: 64
currently lose_sum: 598.7381609678268
time_elpased: 5.357
batch start
#iterations: 65
currently lose_sum: 600.2314392328262
time_elpased: 5.482
batch start
#iterations: 66
currently lose_sum: 599.4528970122337
time_elpased: 5.334
batch start
#iterations: 67
currently lose_sum: 599.962095618248
time_elpased: 5.4
batch start
#iterations: 68
currently lose_sum: 598.5560549497604
time_elpased: 5.569
batch start
#iterations: 69
currently lose_sum: 600.3069616556168
time_elpased: 5.081
batch start
#iterations: 70
currently lose_sum: 600.1561233401299
time_elpased: 5.446
batch start
#iterations: 71
currently lose_sum: 600.18189650774
time_elpased: 5.347
batch start
#iterations: 72
currently lose_sum: 600.0793990492821
time_elpased: 5.395
batch start
#iterations: 73
currently lose_sum: 600.8052561879158
time_elpased: 5.483
batch start
#iterations: 74
currently lose_sum: 599.3224672079086
time_elpased: 5.128
batch start
#iterations: 75
currently lose_sum: 599.3616333603859
time_elpased: 5.41
batch start
#iterations: 76
currently lose_sum: 599.1594285964966
time_elpased: 5.476
batch start
#iterations: 77
currently lose_sum: 595.934139072895
time_elpased: 5.402
batch start
#iterations: 78
currently lose_sum: 600.4133424162865
time_elpased: 5.306
batch start
#iterations: 79
currently lose_sum: 599.4818081855774
time_elpased: 5.43
start validation test
0.549362880886
0.547093472958
0.946279715962
0.693334338712
0
validation finish
batch start
#iterations: 80
currently lose_sum: 599.5601665377617
time_elpased: 5.34
batch start
#iterations: 81
currently lose_sum: 598.3407118320465
time_elpased: 5.332
batch start
#iterations: 82
currently lose_sum: 600.4456053972244
time_elpased: 5.553
batch start
#iterations: 83
currently lose_sum: 599.2833531498909
time_elpased: 5.481
batch start
#iterations: 84
currently lose_sum: 598.86611109972
time_elpased: 5.529
batch start
#iterations: 85
currently lose_sum: 600.1937493681908
time_elpased: 5.531
batch start
#iterations: 86
currently lose_sum: 599.3835705518723
time_elpased: 5.379
batch start
#iterations: 87
currently lose_sum: 600.0967643260956
time_elpased: 5.51
batch start
#iterations: 88
currently lose_sum: 598.8101089596748
time_elpased: 5.331
batch start
#iterations: 89
currently lose_sum: 598.3684470057487
time_elpased: 5.293
batch start
#iterations: 90
currently lose_sum: 599.8402262330055
time_elpased: 5.676
batch start
#iterations: 91
currently lose_sum: 598.9073634147644
time_elpased: 5.403
batch start
#iterations: 92
currently lose_sum: 596.9813075065613
time_elpased: 5.382
batch start
#iterations: 93
currently lose_sum: 600.9176867008209
time_elpased: 5.633
batch start
#iterations: 94
currently lose_sum: 598.5313653945923
time_elpased: 5.341
batch start
#iterations: 95
currently lose_sum: 597.1834794282913
time_elpased: 5.305
batch start
#iterations: 96
currently lose_sum: 598.5022013783455
time_elpased: 5.605
batch start
#iterations: 97
currently lose_sum: 598.7490873336792
time_elpased: 5.316
batch start
#iterations: 98
currently lose_sum: 597.8613858222961
time_elpased: 5.254
batch start
#iterations: 99
currently lose_sum: 598.8158993124962
time_elpased: 5.442
start validation test
0.549556786704
0.547376593902
0.943192343316
0.692730674024
0
validation finish
batch start
#iterations: 100
currently lose_sum: 598.069733440876
time_elpased: 5.305
batch start
#iterations: 101
currently lose_sum: 599.2387605309486
time_elpased: 5.384
batch start
#iterations: 102
currently lose_sum: 597.6898989081383
time_elpased: 5.431
batch start
#iterations: 103
currently lose_sum: 598.893123447895
time_elpased: 5.47
batch start
#iterations: 104
currently lose_sum: 599.1987602710724
time_elpased: 5.375
batch start
#iterations: 105
currently lose_sum: 597.4247090220451
time_elpased: 5.528
batch start
#iterations: 106
currently lose_sum: 599.6299862861633
time_elpased: 5.01
batch start
#iterations: 107
currently lose_sum: 598.2368503212929
time_elpased: 5.39
batch start
#iterations: 108
currently lose_sum: 600.0244271159172
time_elpased: 5.296
batch start
#iterations: 109
currently lose_sum: 599.2486288547516
time_elpased: 5.299
batch start
#iterations: 110
currently lose_sum: 597.7505073547363
time_elpased: 5.67
batch start
#iterations: 111
currently lose_sum: 598.6751635074615
time_elpased: 5.423
batch start
#iterations: 112
currently lose_sum: 599.8986299037933
time_elpased: 5.66
batch start
#iterations: 113
currently lose_sum: 598.3405330181122
time_elpased: 5.3
batch start
#iterations: 114
currently lose_sum: 596.9465606212616
time_elpased: 5.568
batch start
#iterations: 115
currently lose_sum: 597.9745550751686
time_elpased: 5.467
batch start
#iterations: 116
currently lose_sum: 600.3647500872612
time_elpased: 5.289
batch start
#iterations: 117
currently lose_sum: 598.6549598574638
time_elpased: 5.539
batch start
#iterations: 118
currently lose_sum: 598.3829404115677
time_elpased: 5.422
batch start
#iterations: 119
currently lose_sum: 599.2986546158791
time_elpased: 5.293
start validation test
0.550747922438
0.548486310457
0.935988473809
0.691661279897
0
validation finish
batch start
#iterations: 120
currently lose_sum: 599.3877727389336
time_elpased: 5.433
batch start
#iterations: 121
currently lose_sum: 599.7848782539368
time_elpased: 5.438
batch start
#iterations: 122
currently lose_sum: 598.3396785259247
time_elpased: 5.44
batch start
#iterations: 123
currently lose_sum: 598.4094262719154
time_elpased: 5.492
batch start
#iterations: 124
currently lose_sum: 598.8924637436867
time_elpased: 5.322
batch start
#iterations: 125
currently lose_sum: 598.2688962221146
time_elpased: 5.499
batch start
#iterations: 126
currently lose_sum: 598.0672025680542
time_elpased: 5.818
batch start
#iterations: 127
currently lose_sum: 598.070870757103
time_elpased: 5.425
batch start
#iterations: 128
currently lose_sum: 598.9104626774788
time_elpased: 5.623
batch start
#iterations: 129
currently lose_sum: 599.0498796701431
time_elpased: 5.31
batch start
#iterations: 130
currently lose_sum: 598.8066313266754
time_elpased: 5.667
batch start
#iterations: 131
currently lose_sum: 598.8959966301918
time_elpased: 5.279
batch start
#iterations: 132
currently lose_sum: 597.8188946843147
time_elpased: 5.375
batch start
#iterations: 133
currently lose_sum: 596.3109337091446
time_elpased: 5.861
batch start
#iterations: 134
currently lose_sum: 598.3555921912193
time_elpased: 5.27
batch start
#iterations: 135
currently lose_sum: 598.2870777845383
time_elpased: 5.389
batch start
#iterations: 136
currently lose_sum: 597.7867507338524
time_elpased: 5.757
batch start
#iterations: 137
currently lose_sum: 598.5724641680717
time_elpased: 5.871
batch start
#iterations: 138
currently lose_sum: 599.4729982018471
time_elpased: 5.669
batch start
#iterations: 139
currently lose_sum: 598.3378403782845
time_elpased: 5.546
start validation test
0.54836565097
0.546591247395
0.944736029639
0.692516596258
0
validation finish
batch start
#iterations: 140
currently lose_sum: 597.4319931268692
time_elpased: 5.714
batch start
#iterations: 141
currently lose_sum: 596.9253016114235
time_elpased: 5.547
batch start
#iterations: 142
currently lose_sum: 598.747337281704
time_elpased: 5.645
batch start
#iterations: 143
currently lose_sum: 598.5718033909798
time_elpased: 5.625
batch start
#iterations: 144
currently lose_sum: 599.4450157284737
time_elpased: 5.439
batch start
#iterations: 145
currently lose_sum: 597.4382193684578
time_elpased: 5.527
batch start
#iterations: 146
currently lose_sum: 596.8812928795815
time_elpased: 5.495
batch start
#iterations: 147
currently lose_sum: 596.4423826336861
time_elpased: 5.396
batch start
#iterations: 148
currently lose_sum: 598.4788061380386
time_elpased: 5.915
batch start
#iterations: 149
currently lose_sum: 597.4435809850693
time_elpased: 5.44
batch start
#iterations: 150
currently lose_sum: 598.7070750594139
time_elpased: 5.356
batch start
#iterations: 151
currently lose_sum: 597.7617215514183
time_elpased: 5.663
batch start
#iterations: 152
currently lose_sum: 600.057585477829
time_elpased: 5.759
batch start
#iterations: 153
currently lose_sum: 596.9124419093132
time_elpased: 5.269
batch start
#iterations: 154
currently lose_sum: 598.3735377788544
time_elpased: 5.489
batch start
#iterations: 155
currently lose_sum: 599.6733977198601
time_elpased: 5.575
batch start
#iterations: 156
currently lose_sum: 597.5583699941635
time_elpased: 5.517
batch start
#iterations: 157
currently lose_sum: 597.5829802751541
time_elpased: 5.775
batch start
#iterations: 158
currently lose_sum: 599.3441883921623
time_elpased: 5.663
batch start
#iterations: 159
currently lose_sum: 596.3073546886444
time_elpased: 5.883
start validation test
0.548476454294
0.546545476148
0.946794278069
0.693032015066
0
validation finish
batch start
#iterations: 160
currently lose_sum: 598.169971883297
time_elpased: 5.671
batch start
#iterations: 161
currently lose_sum: 599.5835415124893
time_elpased: 5.482
batch start
#iterations: 162
currently lose_sum: 597.7343593835831
time_elpased: 5.6
batch start
#iterations: 163
currently lose_sum: 596.7970402240753
time_elpased: 5.67
batch start
#iterations: 164
currently lose_sum: 598.5764445066452
time_elpased: 5.494
batch start
#iterations: 165
currently lose_sum: 597.9684297442436
time_elpased: 5.651
batch start
#iterations: 166
currently lose_sum: 597.6014779210091
time_elpased: 5.484
batch start
#iterations: 167
currently lose_sum: 598.7357777953148
time_elpased: 5.653
batch start
#iterations: 168
currently lose_sum: 598.3687219619751
time_elpased: 5.741
batch start
#iterations: 169
currently lose_sum: 597.3241865038872
time_elpased: 5.642
batch start
#iterations: 170
currently lose_sum: 597.3030924797058
time_elpased: 5.542
batch start
#iterations: 171
currently lose_sum: 598.3755766749382
time_elpased: 5.342
batch start
#iterations: 172
currently lose_sum: 597.3877603411674
time_elpased: 5.609
batch start
#iterations: 173
currently lose_sum: 596.950032889843
time_elpased: 5.662
batch start
#iterations: 174
currently lose_sum: 596.4085819721222
time_elpased: 5.659
batch start
#iterations: 175
currently lose_sum: 598.6294777393341
time_elpased: 5.557
batch start
#iterations: 176
currently lose_sum: 597.8856297135353
time_elpased: 5.375
batch start
#iterations: 177
currently lose_sum: 599.026729285717
time_elpased: 5.805
batch start
#iterations: 178
currently lose_sum: 597.1938557624817
time_elpased: 5.401
batch start
#iterations: 179
currently lose_sum: 597.3077211976051
time_elpased: 5.551
start validation test
0.549002770083
0.54759532651
0.933312750849
0.690222044637
0
validation finish
batch start
#iterations: 180
currently lose_sum: 597.6093223690987
time_elpased: 5.553
batch start
#iterations: 181
currently lose_sum: 597.4918901324272
time_elpased: 5.934
batch start
#iterations: 182
currently lose_sum: 598.6313478350639
time_elpased: 5.486
batch start
#iterations: 183
currently lose_sum: 597.6677099466324
time_elpased: 5.609
batch start
#iterations: 184
currently lose_sum: 597.0610086321831
time_elpased: 5.521
batch start
#iterations: 185
currently lose_sum: 599.1679462790489
time_elpased: 5.456
batch start
#iterations: 186
currently lose_sum: 597.9780332446098
time_elpased: 5.787
batch start
#iterations: 187
currently lose_sum: 597.5214769244194
time_elpased: 5.492
batch start
#iterations: 188
currently lose_sum: 595.8281951546669
time_elpased: 5.64
batch start
#iterations: 189
currently lose_sum: 598.3992879390717
time_elpased: 5.796
batch start
#iterations: 190
currently lose_sum: 598.0860697627068
time_elpased: 5.803
batch start
#iterations: 191
currently lose_sum: 598.4431221485138
time_elpased: 6.015
batch start
#iterations: 192
currently lose_sum: 596.4678916931152
time_elpased: 5.493
batch start
#iterations: 193
currently lose_sum: 597.5092914700508
time_elpased: 5.41
batch start
#iterations: 194
currently lose_sum: 596.786125421524
time_elpased: 5.828
batch start
#iterations: 195
currently lose_sum: 597.6032209992409
time_elpased: 5.356
batch start
#iterations: 196
currently lose_sum: 597.7458212375641
time_elpased: 5.908
batch start
#iterations: 197
currently lose_sum: 599.1312716007233
time_elpased: 5.405
batch start
#iterations: 198
currently lose_sum: 597.1423309445381
time_elpased: 5.385
batch start
#iterations: 199
currently lose_sum: 597.0520915389061
time_elpased: 5.661
start validation test
0.549612188366
0.547150197511
0.947926314706
0.69382144135
0
validation finish
batch start
#iterations: 200
currently lose_sum: 597.7486590147018
time_elpased: 5.406
batch start
#iterations: 201
currently lose_sum: 596.3602868914604
time_elpased: 5.564
batch start
#iterations: 202
currently lose_sum: 596.3744895458221
time_elpased: 5.604
batch start
#iterations: 203
currently lose_sum: 597.4469667673111
time_elpased: 5.41
batch start
#iterations: 204
currently lose_sum: 597.7101982235909
time_elpased: 5.82
batch start
#iterations: 205
currently lose_sum: 597.5357475280762
time_elpased: 5.505
batch start
#iterations: 206
currently lose_sum: 596.3609293103218
time_elpased: 5.48
batch start
#iterations: 207
currently lose_sum: 599.9863294959068
time_elpased: 5.529
batch start
#iterations: 208
currently lose_sum: 596.3836888670921
time_elpased: 5.438
batch start
#iterations: 209
currently lose_sum: 595.5023438334465
time_elpased: 5.706
batch start
#iterations: 210
currently lose_sum: 598.0971865057945
time_elpased: 5.7
batch start
#iterations: 211
currently lose_sum: 598.7642404437065
time_elpased: 5.578
batch start
#iterations: 212
currently lose_sum: 595.5744861960411
time_elpased: 5.602
batch start
#iterations: 213
currently lose_sum: 597.6204379200935
time_elpased: 5.457
batch start
#iterations: 214
currently lose_sum: 598.4372493624687
time_elpased: 5.611
batch start
#iterations: 215
currently lose_sum: 598.2640706300735
time_elpased: 5.54
batch start
#iterations: 216
currently lose_sum: 596.2198624014854
time_elpased: 5.522
batch start
#iterations: 217
currently lose_sum: 598.6729261279106
time_elpased: 5.673
batch start
#iterations: 218
currently lose_sum: 597.8412430882454
time_elpased: 5.485
batch start
#iterations: 219
currently lose_sum: 597.2381241321564
time_elpased: 5.512
start validation test
0.548698060942
0.547168678316
0.937737984975
0.691088357983
0
validation finish
batch start
#iterations: 220
currently lose_sum: 598.5094377398491
time_elpased: 5.457
batch start
#iterations: 221
currently lose_sum: 597.7160357832909
time_elpased: 5.701
batch start
#iterations: 222
currently lose_sum: 596.6709170341492
time_elpased: 5.676
batch start
#iterations: 223
currently lose_sum: 598.0165609717369
time_elpased: 5.592
batch start
#iterations: 224
currently lose_sum: 597.8252118825912
time_elpased: 5.838
batch start
#iterations: 225
currently lose_sum: 597.4623394608498
time_elpased: 5.594
batch start
#iterations: 226
currently lose_sum: 599.2309694886208
time_elpased: 5.556
batch start
#iterations: 227
currently lose_sum: 599.7397207021713
time_elpased: 5.481
batch start
#iterations: 228
currently lose_sum: 597.0527963042259
time_elpased: 5.526
batch start
#iterations: 229
currently lose_sum: 598.0883547663689
time_elpased: 5.714
batch start
#iterations: 230
currently lose_sum: 597.5230258107185
time_elpased: 5.547
batch start
#iterations: 231
currently lose_sum: 596.6673362255096
time_elpased: 5.875
batch start
#iterations: 232
currently lose_sum: 599.1038938760757
time_elpased: 5.421
batch start
#iterations: 233
currently lose_sum: 598.1584228873253
time_elpased: 5.555
batch start
#iterations: 234
currently lose_sum: 598.6154147982597
time_elpased: 5.729
batch start
#iterations: 235
currently lose_sum: 596.6727504730225
time_elpased: 5.259
batch start
#iterations: 236
currently lose_sum: 597.6633573770523
time_elpased: 5.881
batch start
#iterations: 237
currently lose_sum: 596.3712773919106
time_elpased: 5.59
batch start
#iterations: 238
currently lose_sum: 598.1821203827858
time_elpased: 5.766
batch start
#iterations: 239
currently lose_sum: 597.1104888319969
time_elpased: 5.694
start validation test
0.548614958449
0.546787896855
0.943809817845
0.692425300591
0
validation finish
batch start
#iterations: 240
currently lose_sum: 596.2725458145142
time_elpased: 5.609
batch start
#iterations: 241
currently lose_sum: 599.112411737442
time_elpased: 5.494
batch start
#iterations: 242
currently lose_sum: 596.7103539705276
time_elpased: 5.662
batch start
#iterations: 243
currently lose_sum: 595.4412390589714
time_elpased: 5.717
batch start
#iterations: 244
currently lose_sum: 597.2875202894211
time_elpased: 5.573
batch start
#iterations: 245
currently lose_sum: 596.0112915039062
time_elpased: 5.523
batch start
#iterations: 246
currently lose_sum: 597.5640264749527
time_elpased: 5.805
batch start
#iterations: 247
currently lose_sum: 597.079583644867
time_elpased: 5.439
batch start
#iterations: 248
currently lose_sum: 597.1524767279625
time_elpased: 5.569
batch start
#iterations: 249
currently lose_sum: 595.3896064162254
time_elpased: 5.548
batch start
#iterations: 250
currently lose_sum: 598.157353579998
time_elpased: 5.527
batch start
#iterations: 251
currently lose_sum: 596.73111307621
time_elpased: 5.329
batch start
#iterations: 252
currently lose_sum: 597.4468531608582
time_elpased: 5.592
batch start
#iterations: 253
currently lose_sum: 598.0017837285995
time_elpased: 5.647
batch start
#iterations: 254
currently lose_sum: 595.8242253065109
time_elpased: 5.386
batch start
#iterations: 255
currently lose_sum: 597.3980025649071
time_elpased: 5.674
batch start
#iterations: 256
currently lose_sum: 597.5287310481071
time_elpased: 5.692
batch start
#iterations: 257
currently lose_sum: 596.7716688513756
time_elpased: 5.519
batch start
#iterations: 258
currently lose_sum: 597.4759745001793
time_elpased: 5.868
batch start
#iterations: 259
currently lose_sum: 595.8649228215218
time_elpased: 5.565
start validation test
0.548421052632
0.546925566343
0.939178758876
0.691285081241
0
validation finish
batch start
#iterations: 260
currently lose_sum: 598.1697377562523
time_elpased: 5.877
batch start
#iterations: 261
currently lose_sum: 599.0644568800926
time_elpased: 5.66
batch start
#iterations: 262
currently lose_sum: 598.1797339916229
time_elpased: 5.764
batch start
#iterations: 263
currently lose_sum: 595.5470141768456
time_elpased: 5.593
batch start
#iterations: 264
currently lose_sum: 598.9068291187286
time_elpased: 5.507
batch start
#iterations: 265
currently lose_sum: 598.3243852257729
time_elpased: 5.998
batch start
#iterations: 266
currently lose_sum: 597.1939446926117
time_elpased: 5.466
batch start
#iterations: 267
currently lose_sum: 597.6533416509628
time_elpased: 5.364
batch start
#iterations: 268
currently lose_sum: 595.7932561039925
time_elpased: 5.633
batch start
#iterations: 269
currently lose_sum: 596.9864912033081
time_elpased: 5.627
batch start
#iterations: 270
currently lose_sum: 597.249884724617
time_elpased: 5.327
batch start
#iterations: 271
currently lose_sum: 597.1807863116264
time_elpased: 5.77
batch start
#iterations: 272
currently lose_sum: 596.6765298843384
time_elpased: 5.404
batch start
#iterations: 273
currently lose_sum: 597.9469861984253
time_elpased: 5.715
batch start
#iterations: 274
currently lose_sum: 596.4741452336311
time_elpased: 5.862
batch start
#iterations: 275
currently lose_sum: 598.1867950558662
time_elpased: 5.579
batch start
#iterations: 276
currently lose_sum: 597.0191482901573
time_elpased: 5.571
batch start
#iterations: 277
currently lose_sum: 598.014671087265
time_elpased: 5.544
batch start
#iterations: 278
currently lose_sum: 598.6980656981468
time_elpased: 5.428
batch start
#iterations: 279
currently lose_sum: 596.7581707239151
time_elpased: 5.643
start validation test
0.549141274238
0.547631975867
0.934136050221
0.690476190476
0
validation finish
batch start
#iterations: 280
currently lose_sum: 597.0502055287361
time_elpased: 5.51
batch start
#iterations: 281
currently lose_sum: 596.9593877196312
time_elpased: 5.688
batch start
#iterations: 282
currently lose_sum: 597.7062192559242
time_elpased: 5.847
batch start
#iterations: 283
currently lose_sum: 597.3051611185074
time_elpased: 5.476
batch start
#iterations: 284
currently lose_sum: 595.5328850746155
time_elpased: 5.738
batch start
#iterations: 285
currently lose_sum: 596.9031829833984
time_elpased: 5.583
batch start
#iterations: 286
currently lose_sum: 597.4309982061386
time_elpased: 5.574
batch start
#iterations: 287
currently lose_sum: 597.9832643270493
time_elpased: 5.697
batch start
#iterations: 288
currently lose_sum: 598.6967824697495
time_elpased: 5.695
batch start
#iterations: 289
currently lose_sum: 596.2109167575836
time_elpased: 5.715
batch start
#iterations: 290
currently lose_sum: 598.4565165638924
time_elpased: 5.552
batch start
#iterations: 291
currently lose_sum: 597.7962961196899
time_elpased: 5.392
batch start
#iterations: 292
currently lose_sum: 596.0871545672417
time_elpased: 5.429
batch start
#iterations: 293
currently lose_sum: 596.1309969425201
time_elpased: 5.388
batch start
#iterations: 294
currently lose_sum: 598.5232366919518
time_elpased: 5.649
batch start
#iterations: 295
currently lose_sum: 596.5007635354996
time_elpased: 5.608
batch start
#iterations: 296
currently lose_sum: 597.0043384432793
time_elpased: 5.707
batch start
#iterations: 297
currently lose_sum: 598.0808348059654
time_elpased: 5.602
batch start
#iterations: 298
currently lose_sum: 595.8893077969551
time_elpased: 5.461
batch start
#iterations: 299
currently lose_sum: 595.6165500283241
time_elpased: 5.706
start validation test
0.548448753463
0.546885756188
0.940207883091
0.691531838395
0
validation finish
batch start
#iterations: 300
currently lose_sum: 597.2550582289696
time_elpased: 5.584
batch start
#iterations: 301
currently lose_sum: 598.322847366333
time_elpased: 5.554
batch start
#iterations: 302
currently lose_sum: 596.215279340744
time_elpased: 5.637
batch start
#iterations: 303
currently lose_sum: 597.0506581068039
time_elpased: 5.524
batch start
#iterations: 304
currently lose_sum: 596.5530080795288
time_elpased: 5.406
batch start
#iterations: 305
currently lose_sum: 597.3589003682137
time_elpased: 5.775
batch start
#iterations: 306
currently lose_sum: 595.0768613815308
time_elpased: 5.421
batch start
#iterations: 307
currently lose_sum: 596.405297100544
time_elpased: 5.494
batch start
#iterations: 308
currently lose_sum: 598.4502210021019
time_elpased: 5.966
batch start
#iterations: 309
currently lose_sum: 596.6083434820175
time_elpased: 5.804
batch start
#iterations: 310
currently lose_sum: 596.6728843450546
time_elpased: 5.824
batch start
#iterations: 311
currently lose_sum: 596.3142236471176
time_elpased: 5.717
batch start
#iterations: 312
currently lose_sum: 596.3814767003059
time_elpased: 5.655
batch start
#iterations: 313
currently lose_sum: 596.2238744497299
time_elpased: 5.535
batch start
#iterations: 314
currently lose_sum: 596.9988480806351
time_elpased: 5.7
batch start
#iterations: 315
currently lose_sum: 596.0944900512695
time_elpased: 5.724
batch start
#iterations: 316
currently lose_sum: 598.0448074936867
time_elpased: 5.783
batch start
#iterations: 317
currently lose_sum: 596.5348435640335
time_elpased: 5.568
batch start
#iterations: 318
currently lose_sum: 597.8026064634323
time_elpased: 5.494
batch start
#iterations: 319
currently lose_sum: 596.4448320865631
time_elpased: 5.577
start validation test
0.547756232687
0.546449067432
0.940722445199
0.691321610891
0
validation finish
batch start
#iterations: 320
currently lose_sum: 596.7263631224632
time_elpased: 5.618
batch start
#iterations: 321
currently lose_sum: 596.8321059942245
time_elpased: 5.418
batch start
#iterations: 322
currently lose_sum: 595.996967792511
time_elpased: 5.529
batch start
#iterations: 323
currently lose_sum: 596.9987276792526
time_elpased: 5.599
batch start
#iterations: 324
currently lose_sum: 596.4872716069221
time_elpased: 5.404
batch start
#iterations: 325
currently lose_sum: 598.2116096615791
time_elpased: 5.645
batch start
#iterations: 326
currently lose_sum: 598.5790999531746
time_elpased: 5.568
batch start
#iterations: 327
currently lose_sum: 596.322386264801
time_elpased: 5.576
batch start
#iterations: 328
currently lose_sum: 597.6057052016258
time_elpased: 5.817
batch start
#iterations: 329
currently lose_sum: 596.8065801858902
time_elpased: 5.414
batch start
#iterations: 330
currently lose_sum: 597.7805738449097
time_elpased: 5.741
batch start
#iterations: 331
currently lose_sum: 597.5439642071724
time_elpased: 5.692
batch start
#iterations: 332
currently lose_sum: 596.8388808369637
time_elpased: 5.436
batch start
#iterations: 333
currently lose_sum: 596.473283290863
time_elpased: 5.579
batch start
#iterations: 334
currently lose_sum: 596.0025216937065
time_elpased: 5.55
batch start
#iterations: 335
currently lose_sum: 596.5893709659576
time_elpased: 5.508
batch start
#iterations: 336
currently lose_sum: 597.7129265666008
time_elpased: 5.884
batch start
#iterations: 337
currently lose_sum: 597.3329702615738
time_elpased: 5.696
batch start
#iterations: 338
currently lose_sum: 596.0553356409073
time_elpased: 5.852
batch start
#iterations: 339
currently lose_sum: 596.4480730295181
time_elpased: 5.417
start validation test
0.548393351801
0.546796998774
0.941237007307
0.691738990678
0
validation finish
batch start
#iterations: 340
currently lose_sum: 597.118160545826
time_elpased: 5.668
batch start
#iterations: 341
currently lose_sum: 596.507542014122
time_elpased: 5.413
batch start
#iterations: 342
currently lose_sum: 596.3070993423462
time_elpased: 5.657
batch start
#iterations: 343
currently lose_sum: 597.4561293721199
time_elpased: 5.488
batch start
#iterations: 344
currently lose_sum: 597.1851410865784
time_elpased: 5.458
batch start
#iterations: 345
currently lose_sum: 597.0426568388939
time_elpased: 5.78
batch start
#iterations: 346
currently lose_sum: 596.6731620430946
time_elpased: 5.372
batch start
#iterations: 347
currently lose_sum: 596.6044536828995
time_elpased: 5.584
batch start
#iterations: 348
currently lose_sum: 598.4476315975189
time_elpased: 5.802
batch start
#iterations: 349
currently lose_sum: 596.6569358706474
time_elpased: 5.58
batch start
#iterations: 350
currently lose_sum: 596.1236745119095
time_elpased: 5.446
batch start
#iterations: 351
currently lose_sum: 596.6987701654434
time_elpased: 5.553
batch start
#iterations: 352
currently lose_sum: 597.4705957770348
time_elpased: 5.625
batch start
#iterations: 353
currently lose_sum: 596.8136432766914
time_elpased: 5.597
batch start
#iterations: 354
currently lose_sum: 597.556512594223
time_elpased: 5.56
batch start
#iterations: 355
currently lose_sum: 596.6254958510399
time_elpased: 5.425
batch start
#iterations: 356
currently lose_sum: 597.1650556921959
time_elpased: 5.435
batch start
#iterations: 357
currently lose_sum: 596.9986709952354
time_elpased: 5.593
batch start
#iterations: 358
currently lose_sum: 596.7988721728325
time_elpased: 5.595
batch start
#iterations: 359
currently lose_sum: 597.5453128218651
time_elpased: 5.406
start validation test
0.54891966759
0.547260397287
0.938458371925
0.691357088704
0
validation finish
batch start
#iterations: 360
currently lose_sum: 596.4633345007896
time_elpased: 5.632
batch start
#iterations: 361
currently lose_sum: 598.7627851963043
time_elpased: 5.631
batch start
#iterations: 362
currently lose_sum: 596.0874714255333
time_elpased: 5.722
batch start
#iterations: 363
currently lose_sum: 596.244425714016
time_elpased: 5.418
batch start
#iterations: 364
currently lose_sum: 596.905876159668
time_elpased: 5.762
batch start
#iterations: 365
currently lose_sum: 596.8923785090446
time_elpased: 5.324
batch start
#iterations: 366
currently lose_sum: 596.4782608747482
time_elpased: 5.487
batch start
#iterations: 367
currently lose_sum: 594.2906324863434
time_elpased: 5.818
batch start
#iterations: 368
currently lose_sum: 595.9032232761383
time_elpased: 5.571
batch start
#iterations: 369
currently lose_sum: 596.8224368691444
time_elpased: 5.656
batch start
#iterations: 370
currently lose_sum: 596.1488553285599
time_elpased: 5.618
batch start
#iterations: 371
currently lose_sum: 596.5790739655495
time_elpased: 5.466
batch start
#iterations: 372
currently lose_sum: 599.6538021564484
time_elpased: 5.809
batch start
#iterations: 373
currently lose_sum: 596.3374608755112
time_elpased: 5.668
batch start
#iterations: 374
currently lose_sum: 597.1990191340446
time_elpased: 5.667
batch start
#iterations: 375
currently lose_sum: 596.3888192176819
time_elpased: 5.464
batch start
#iterations: 376
currently lose_sum: 595.3698766827583
time_elpased: 5.666
batch start
#iterations: 377
currently lose_sum: 596.1580535769463
time_elpased: 5.708
batch start
#iterations: 378
currently lose_sum: 598.0361919403076
time_elpased: 5.856
batch start
#iterations: 379
currently lose_sum: 596.0450515151024
time_elpased: 5.695
start validation test
0.548836565097
0.547404573253
0.934959349593
0.690520075247
0
validation finish
batch start
#iterations: 380
currently lose_sum: 595.8172670602798
time_elpased: 5.609
batch start
#iterations: 381
currently lose_sum: 597.4474419355392
time_elpased: 5.305
batch start
#iterations: 382
currently lose_sum: 595.9999651312828
time_elpased: 5.69
batch start
#iterations: 383
currently lose_sum: 597.1286218166351
time_elpased: 5.651
batch start
#iterations: 384
currently lose_sum: 597.4383745193481
time_elpased: 5.509
batch start
#iterations: 385
currently lose_sum: 597.8267326951027
time_elpased: 5.974
batch start
#iterations: 386
currently lose_sum: 595.8997960686684
time_elpased: 5.769
batch start
#iterations: 387
currently lose_sum: 596.2385067343712
time_elpased: 5.786
batch start
#iterations: 388
currently lose_sum: 596.471175134182
time_elpased: 5.73
batch start
#iterations: 389
currently lose_sum: 597.7926548719406
time_elpased: 5.412
batch start
#iterations: 390
currently lose_sum: 597.5472869277
time_elpased: 5.893
batch start
#iterations: 391
currently lose_sum: 596.176702439785
time_elpased: 5.503
batch start
#iterations: 392
currently lose_sum: 595.5072743296623
time_elpased: 5.709
batch start
#iterations: 393
currently lose_sum: 596.4679299592972
time_elpased: 5.613
batch start
#iterations: 394
currently lose_sum: 597.4513756036758
time_elpased: 6.079
batch start
#iterations: 395
currently lose_sum: 595.9784436821938
time_elpased: 5.75
batch start
#iterations: 396
currently lose_sum: 596.7204415202141
time_elpased: 5.465
batch start
#iterations: 397
currently lose_sum: 594.6675392985344
time_elpased: 5.474
batch start
#iterations: 398
currently lose_sum: 597.5841047763824
time_elpased: 5.479
batch start
#iterations: 399
currently lose_sum: 597.8006734848022
time_elpased: 5.652
start validation test
0.548891966759
0.547312118754
0.937223422867
0.691062925654
0
validation finish
acc: 0.545
pre: 0.544
rec: 0.952
F1: 0.692
auc: 0.000
