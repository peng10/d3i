start to construct graph
graph construct over
92947
epochs start
batch start
#iterations: 0
currently lose_sum: 615.0712485313416
time_elpased: 5.856
batch start
#iterations: 1
currently lose_sum: 609.9483958482742
time_elpased: 5.83
batch start
#iterations: 2
currently lose_sum: 607.9806470274925
time_elpased: 6.223
batch start
#iterations: 3
currently lose_sum: 604.812405705452
time_elpased: 6.037
batch start
#iterations: 4
currently lose_sum: 604.9238030314445
time_elpased: 6.215
batch start
#iterations: 5
currently lose_sum: 603.8609182834625
time_elpased: 5.943
batch start
#iterations: 6
currently lose_sum: 603.2843686342239
time_elpased: 5.989
batch start
#iterations: 7
currently lose_sum: 603.2524558901787
time_elpased: 6.6
batch start
#iterations: 8
currently lose_sum: 601.9903489351273
time_elpased: 6.443
batch start
#iterations: 9
currently lose_sum: 601.6265133023262
time_elpased: 6.422
batch start
#iterations: 10
currently lose_sum: 603.361289203167
time_elpased: 5.875
batch start
#iterations: 11
currently lose_sum: 601.0534357428551
time_elpased: 5.895
batch start
#iterations: 12
currently lose_sum: 601.8152239322662
time_elpased: 6.121
batch start
#iterations: 13
currently lose_sum: 599.6332197189331
time_elpased: 6.0
batch start
#iterations: 14
currently lose_sum: 600.963442325592
time_elpased: 6.251
batch start
#iterations: 15
currently lose_sum: 600.1481859087944
time_elpased: 6.242
batch start
#iterations: 16
currently lose_sum: 601.031240940094
time_elpased: 6.314
batch start
#iterations: 17
currently lose_sum: 598.3403890132904
time_elpased: 5.688
batch start
#iterations: 18
currently lose_sum: 599.1851431131363
time_elpased: 6.009
batch start
#iterations: 19
currently lose_sum: 598.658252120018
time_elpased: 6.134
start validation test
0.548393351801
0.545977855444
0.956570958115
0.695174167492
0
validation finish
batch start
#iterations: 20
currently lose_sum: 599.8685270547867
time_elpased: 6.033
batch start
#iterations: 21
currently lose_sum: 597.3087517023087
time_elpased: 6.096
batch start
#iterations: 22
currently lose_sum: 598.64517390728
time_elpased: 6.166
batch start
#iterations: 23
currently lose_sum: 599.7249241471291
time_elpased: 5.857
batch start
#iterations: 24
currently lose_sum: 598.3729541897774
time_elpased: 6.135
batch start
#iterations: 25
currently lose_sum: 599.1009927988052
time_elpased: 6.208
batch start
#iterations: 26
currently lose_sum: 598.8863430023193
time_elpased: 6.325
batch start
#iterations: 27
currently lose_sum: 597.1764978170395
time_elpased: 6.149
batch start
#iterations: 28
currently lose_sum: 597.5316659808159
time_elpased: 5.873
batch start
#iterations: 29
currently lose_sum: 598.1981391310692
time_elpased: 5.874
batch start
#iterations: 30
currently lose_sum: 598.2062324285507
time_elpased: 6.087
batch start
#iterations: 31
currently lose_sum: 598.0825128555298
time_elpased: 6.064
batch start
#iterations: 32
currently lose_sum: 599.4349011778831
time_elpased: 6.199
batch start
#iterations: 33
currently lose_sum: 596.6978170871735
time_elpased: 6.144
batch start
#iterations: 34
currently lose_sum: 597.6104689836502
time_elpased: 6.136
batch start
#iterations: 35
currently lose_sum: 597.3353829979897
time_elpased: 5.731
batch start
#iterations: 36
currently lose_sum: 598.1240797638893
time_elpased: 6.237
batch start
#iterations: 37
currently lose_sum: 597.7438976764679
time_elpased: 6.167
batch start
#iterations: 38
currently lose_sum: 596.8472214341164
time_elpased: 6.223
batch start
#iterations: 39
currently lose_sum: 595.5294020175934
time_elpased: 6.225
start validation test
0.549473684211
0.546979666845
0.949572913451
0.6941247273
0
validation finish
batch start
#iterations: 40
currently lose_sum: 596.5428175330162
time_elpased: 6.02
batch start
#iterations: 41
currently lose_sum: 596.2271031141281
time_elpased: 6.22
batch start
#iterations: 42
currently lose_sum: 596.7450624704361
time_elpased: 6.299
batch start
#iterations: 43
currently lose_sum: 596.4405326247215
time_elpased: 6.269
batch start
#iterations: 44
currently lose_sum: 597.2810763716698
time_elpased: 5.864
batch start
#iterations: 45
currently lose_sum: 596.4522449374199
time_elpased: 6.032
batch start
#iterations: 46
currently lose_sum: 596.4785194396973
time_elpased: 6.081
batch start
#iterations: 47
currently lose_sum: 595.1019628047943
time_elpased: 6.072
batch start
#iterations: 48
currently lose_sum: 597.9896686077118
time_elpased: 6.063
batch start
#iterations: 49
currently lose_sum: 597.3699581027031
time_elpased: 5.867
batch start
#iterations: 50
currently lose_sum: 596.2358018755913
time_elpased: 6.209
batch start
#iterations: 51
currently lose_sum: 596.345023393631
time_elpased: 6.027
batch start
#iterations: 52
currently lose_sum: 597.3042773604393
time_elpased: 6.241
batch start
#iterations: 53
currently lose_sum: 595.6238580346107
time_elpased: 6.156
batch start
#iterations: 54
currently lose_sum: 596.4855927228928
time_elpased: 6.185
batch start
#iterations: 55
currently lose_sum: 596.108854830265
time_elpased: 6.174
batch start
#iterations: 56
currently lose_sum: 595.1153635382652
time_elpased: 5.881
batch start
#iterations: 57
currently lose_sum: 596.3533928990364
time_elpased: 6.124
batch start
#iterations: 58
currently lose_sum: 595.3713929057121
time_elpased: 6.118
batch start
#iterations: 59
currently lose_sum: 596.7468501329422
time_elpased: 6.1
start validation test
0.548393351801
0.546763449533
0.941854481836
0.691878815369
0
validation finish
batch start
#iterations: 60
currently lose_sum: 595.224635541439
time_elpased: 6.045
batch start
#iterations: 61
currently lose_sum: 597.1024026870728
time_elpased: 6.151
batch start
#iterations: 62
currently lose_sum: 596.4404036402702
time_elpased: 6.009
batch start
#iterations: 63
currently lose_sum: 595.9471265673637
time_elpased: 5.825
batch start
#iterations: 64
currently lose_sum: 596.5560747385025
time_elpased: 6.107
batch start
#iterations: 65
currently lose_sum: 595.7651845216751
time_elpased: 5.956
batch start
#iterations: 66
currently lose_sum: 594.5632408857346
time_elpased: 6.125
batch start
#iterations: 67
currently lose_sum: 596.2901474237442
time_elpased: 6.261
batch start
#iterations: 68
currently lose_sum: 594.3296057581902
time_elpased: 6.212
batch start
#iterations: 69
currently lose_sum: 593.9650690555573
time_elpased: 6.224
batch start
#iterations: 70
currently lose_sum: 595.2032850384712
time_elpased: 5.878
batch start
#iterations: 71
currently lose_sum: 596.6796048879623
time_elpased: 5.913
batch start
#iterations: 72
currently lose_sum: 595.7666644454002
time_elpased: 6.02
batch start
#iterations: 73
currently lose_sum: 594.437363922596
time_elpased: 5.927
batch start
#iterations: 74
currently lose_sum: 595.5822550058365
time_elpased: 5.853
batch start
#iterations: 75
currently lose_sum: 595.3755297660828
time_elpased: 6.15
batch start
#iterations: 76
currently lose_sum: 595.7912691831589
time_elpased: 6.037
batch start
#iterations: 77
currently lose_sum: 595.1239804625511
time_elpased: 6.22
batch start
#iterations: 78
currently lose_sum: 593.9033299088478
time_elpased: 6.051
batch start
#iterations: 79
currently lose_sum: 595.4582319259644
time_elpased: 5.945
start validation test
0.548864265928
0.546502008981
0.951836986724
0.694343305431
0
validation finish
batch start
#iterations: 80
currently lose_sum: 595.1529806256294
time_elpased: 5.984
batch start
#iterations: 81
currently lose_sum: 596.4759603142738
time_elpased: 6.077
batch start
#iterations: 82
currently lose_sum: 595.0021396279335
time_elpased: 6.173
batch start
#iterations: 83
currently lose_sum: 594.6315425634384
time_elpased: 6.225
batch start
#iterations: 84
currently lose_sum: 593.7682846188545
time_elpased: 5.826
batch start
#iterations: 85
currently lose_sum: 595.2039086222649
time_elpased: 5.954
batch start
#iterations: 86
currently lose_sum: 595.1338108778
time_elpased: 6.059
batch start
#iterations: 87
currently lose_sum: 595.3108485937119
time_elpased: 6.307
batch start
#iterations: 88
currently lose_sum: 595.491539478302
time_elpased: 6.056
batch start
#iterations: 89
currently lose_sum: 594.2254269719124
time_elpased: 6.275
batch start
#iterations: 90
currently lose_sum: 595.457933485508
time_elpased: 6.098
batch start
#iterations: 91
currently lose_sum: 593.3193811774254
time_elpased: 6.141
batch start
#iterations: 92
currently lose_sum: 595.6702944040298
time_elpased: 6.267
batch start
#iterations: 93
currently lose_sum: 594.9581596851349
time_elpased: 6.019
batch start
#iterations: 94
currently lose_sum: 593.885640501976
time_elpased: 6.091
batch start
#iterations: 95
currently lose_sum: 594.4322234392166
time_elpased: 6.115
batch start
#iterations: 96
currently lose_sum: 594.0956129431725
time_elpased: 6.25
batch start
#iterations: 97
currently lose_sum: 595.1155854463577
time_elpased: 6.213
batch start
#iterations: 98
currently lose_sum: 595.2083293199539
time_elpased: 6.397
batch start
#iterations: 99
currently lose_sum: 594.1317789554596
time_elpased: 6.202
start validation test
0.549307479224
0.547541771848
0.937532160132
0.691329918422
0
validation finish
batch start
#iterations: 100
currently lose_sum: 593.0899332761765
time_elpased: 6.03
batch start
#iterations: 101
currently lose_sum: 593.7766671776772
time_elpased: 6.132
batch start
#iterations: 102
currently lose_sum: 593.703955590725
time_elpased: 6.183
batch start
#iterations: 103
currently lose_sum: 594.292963206768
time_elpased: 6.189
batch start
#iterations: 104
currently lose_sum: 593.854548573494
time_elpased: 6.441
batch start
#iterations: 105
currently lose_sum: 594.342066347599
time_elpased: 6.011
batch start
#iterations: 106
currently lose_sum: 594.1560958623886
time_elpased: 6.197
batch start
#iterations: 107
currently lose_sum: 593.6059670448303
time_elpased: 6.02
batch start
#iterations: 108
currently lose_sum: 592.4021636843681
time_elpased: 6.025
batch start
#iterations: 109
currently lose_sum: 592.2633652091026
time_elpased: 6.355
batch start
#iterations: 110
currently lose_sum: 594.7348908185959
time_elpased: 6.145
batch start
#iterations: 111
currently lose_sum: 595.3744062781334
time_elpased: 6.008
batch start
#iterations: 112
currently lose_sum: 594.1516514420509
time_elpased: 6.107
batch start
#iterations: 113
currently lose_sum: 595.002927839756
time_elpased: 6.031
batch start
#iterations: 114
currently lose_sum: 594.2590128183365
time_elpased: 6.154
batch start
#iterations: 115
currently lose_sum: 594.4028598666191
time_elpased: 6.077
batch start
#iterations: 116
currently lose_sum: 594.2697620391846
time_elpased: 6.017
batch start
#iterations: 117
currently lose_sum: 593.5337927937508
time_elpased: 6.027
batch start
#iterations: 118
currently lose_sum: 592.9841031432152
time_elpased: 6.175
batch start
#iterations: 119
currently lose_sum: 594.2493808269501
time_elpased: 6.053
start validation test
0.54972299169
0.547503063268
0.942677781208
0.692693071179
0
validation finish
batch start
#iterations: 120
currently lose_sum: 594.2215883135796
time_elpased: 6.262
batch start
#iterations: 121
currently lose_sum: 593.8097173571587
time_elpased: 5.872
batch start
#iterations: 122
currently lose_sum: 594.4134259223938
time_elpased: 6.107
batch start
#iterations: 123
currently lose_sum: 593.7833676934242
time_elpased: 6.013
batch start
#iterations: 124
currently lose_sum: 592.9940791726112
time_elpased: 6.115
batch start
#iterations: 125
currently lose_sum: 594.3975241780281
time_elpased: 6.267
batch start
#iterations: 126
currently lose_sum: 592.163421869278
time_elpased: 6.254
batch start
#iterations: 127
currently lose_sum: 593.2854540348053
time_elpased: 6.006
batch start
#iterations: 128
currently lose_sum: 592.7557218074799
time_elpased: 5.978
batch start
#iterations: 129
currently lose_sum: 593.4686067700386
time_elpased: 6.2
batch start
#iterations: 130
currently lose_sum: 594.3465868234634
time_elpased: 5.783
batch start
#iterations: 131
currently lose_sum: 592.8509483933449
time_elpased: 6.284
batch start
#iterations: 132
currently lose_sum: 594.9166145324707
time_elpased: 6.152
batch start
#iterations: 133
currently lose_sum: 593.7521036267281
time_elpased: 6.041
batch start
#iterations: 134
currently lose_sum: 593.2498707175255
time_elpased: 6.139
batch start
#iterations: 135
currently lose_sum: 593.3832644820213
time_elpased: 6.039
batch start
#iterations: 136
currently lose_sum: 593.6203338503838
time_elpased: 6.078
batch start
#iterations: 137
currently lose_sum: 594.2270220518112
time_elpased: 6.0
batch start
#iterations: 138
currently lose_sum: 593.4150987863541
time_elpased: 6.047
batch start
#iterations: 139
currently lose_sum: 592.6860409975052
time_elpased: 6.028
start validation test
0.5491966759
0.547124791219
0.943912730267
0.692723084476
0
validation finish
batch start
#iterations: 140
currently lose_sum: 596.036937057972
time_elpased: 6.204
batch start
#iterations: 141
currently lose_sum: 592.7108153700829
time_elpased: 6.425
batch start
#iterations: 142
currently lose_sum: 592.8553839325905
time_elpased: 6.225
batch start
#iterations: 143
currently lose_sum: 593.758198082447
time_elpased: 5.906
batch start
#iterations: 144
currently lose_sum: 593.0677762031555
time_elpased: 6.344
batch start
#iterations: 145
currently lose_sum: 593.5781655907631
time_elpased: 6.297
batch start
#iterations: 146
currently lose_sum: 594.268824338913
time_elpased: 6.137
batch start
#iterations: 147
currently lose_sum: 592.4080515503883
time_elpased: 6.12
batch start
#iterations: 148
currently lose_sum: 595.0829994678497
time_elpased: 5.899
batch start
#iterations: 149
currently lose_sum: 593.57673817873
time_elpased: 5.739
batch start
#iterations: 150
currently lose_sum: 593.7220274209976
time_elpased: 6.011
batch start
#iterations: 151
currently lose_sum: 594.678524017334
time_elpased: 6.251
batch start
#iterations: 152
currently lose_sum: 594.3009672760963
time_elpased: 5.981
batch start
#iterations: 153
currently lose_sum: 592.997004210949
time_elpased: 5.981
batch start
#iterations: 154
currently lose_sum: 593.1161017417908
time_elpased: 6.074
batch start
#iterations: 155
currently lose_sum: 594.7219375967979
time_elpased: 6.158
batch start
#iterations: 156
currently lose_sum: 593.1698372960091
time_elpased: 6.09
batch start
#iterations: 157
currently lose_sum: 592.8973067998886
time_elpased: 6.582
batch start
#iterations: 158
currently lose_sum: 593.190188407898
time_elpased: 6.018
batch start
#iterations: 159
currently lose_sum: 593.0450094342232
time_elpased: 6.151
start validation test
0.548171745152
0.546986429152
0.93537099928
0.690299428485
0
validation finish
batch start
#iterations: 160
currently lose_sum: 591.5974401831627
time_elpased: 5.842
batch start
#iterations: 161
currently lose_sum: 593.5236783027649
time_elpased: 6.044
batch start
#iterations: 162
currently lose_sum: 591.9598295092583
time_elpased: 6.127
batch start
#iterations: 163
currently lose_sum: 593.6183696389198
time_elpased: 6.185
batch start
#iterations: 164
currently lose_sum: 593.8509317040443
time_elpased: 6.011
batch start
#iterations: 165
currently lose_sum: 596.0445418357849
time_elpased: 6.177
batch start
#iterations: 166
currently lose_sum: 593.6069685816765
time_elpased: 5.999
batch start
#iterations: 167
currently lose_sum: 593.2697623372078
time_elpased: 6.196
batch start
#iterations: 168
currently lose_sum: 593.3060622215271
time_elpased: 5.988
batch start
#iterations: 169
currently lose_sum: 593.1268728971481
time_elpased: 6.059
batch start
#iterations: 170
currently lose_sum: 592.5076897144318
time_elpased: 6.24
batch start
#iterations: 171
currently lose_sum: 593.1004700660706
time_elpased: 6.237
batch start
#iterations: 172
currently lose_sum: 593.2304280996323
time_elpased: 6.161
batch start
#iterations: 173
currently lose_sum: 594.1379010677338
time_elpased: 5.988
batch start
#iterations: 174
currently lose_sum: 592.8050035834312
time_elpased: 6.176
batch start
#iterations: 175
currently lose_sum: 592.9526382684708
time_elpased: 6.156
batch start
#iterations: 176
currently lose_sum: 593.7026264667511
time_elpased: 5.87
batch start
#iterations: 177
currently lose_sum: 592.3142319321632
time_elpased: 6.205
batch start
#iterations: 178
currently lose_sum: 595.0514146089554
time_elpased: 6.206
batch start
#iterations: 179
currently lose_sum: 593.8754465579987
time_elpased: 6.121
start validation test
0.548975069252
0.547435588731
0.935885561387
0.690797219796
0
validation finish
batch start
#iterations: 180
currently lose_sum: 594.1464470028877
time_elpased: 6.071
batch start
#iterations: 181
currently lose_sum: 593.5390328168869
time_elpased: 5.757
batch start
#iterations: 182
currently lose_sum: 592.3530321121216
time_elpased: 5.952
batch start
#iterations: 183
currently lose_sum: 593.6363669633865
time_elpased: 6.109
batch start
#iterations: 184
currently lose_sum: 591.7336826920509
time_elpased: 6.042
batch start
#iterations: 185
currently lose_sum: 592.6210160851479
time_elpased: 5.927
batch start
#iterations: 186
currently lose_sum: 592.7866210341454
time_elpased: 6.108
batch start
#iterations: 187
currently lose_sum: 593.9179564118385
time_elpased: 5.98
batch start
#iterations: 188
currently lose_sum: 591.3900380730629
time_elpased: 6.177
batch start
#iterations: 189
currently lose_sum: 592.9962067604065
time_elpased: 6.258
batch start
#iterations: 190
currently lose_sum: 592.3669741153717
time_elpased: 6.269
batch start
#iterations: 191
currently lose_sum: 593.866524875164
time_elpased: 6.278
batch start
#iterations: 192
currently lose_sum: 594.3094394207001
time_elpased: 6.035
batch start
#iterations: 193
currently lose_sum: 592.8940673470497
time_elpased: 5.908
batch start
#iterations: 194
currently lose_sum: 595.007498383522
time_elpased: 6.214
batch start
#iterations: 195
currently lose_sum: 592.8173371553421
time_elpased: 5.892
batch start
#iterations: 196
currently lose_sum: 594.2832425236702
time_elpased: 6.017
batch start
#iterations: 197
currently lose_sum: 592.8859133124352
time_elpased: 6.126
batch start
#iterations: 198
currently lose_sum: 593.3161964416504
time_elpased: 5.891
batch start
#iterations: 199
currently lose_sum: 592.6392270326614
time_elpased: 6.035
start validation test
0.549639889197
0.547116069309
0.9488525265
0.694041928563
0
validation finish
batch start
#iterations: 200
currently lose_sum: 594.5306351184845
time_elpased: 6.046
batch start
#iterations: 201
currently lose_sum: 591.4849354028702
time_elpased: 6.096
batch start
#iterations: 202
currently lose_sum: 593.1313151717186
time_elpased: 6.163
batch start
#iterations: 203
currently lose_sum: 594.1704325079918
time_elpased: 6.149
batch start
#iterations: 204
currently lose_sum: 591.8502035140991
time_elpased: 6.079
batch start
#iterations: 205
currently lose_sum: 593.5351869463921
time_elpased: 6.237
batch start
#iterations: 206
currently lose_sum: 592.652579665184
time_elpased: 5.832
batch start
#iterations: 207
currently lose_sum: 594.1146460175514
time_elpased: 5.894
batch start
#iterations: 208
currently lose_sum: 592.466786801815
time_elpased: 6.221
batch start
#iterations: 209
currently lose_sum: 590.5363482236862
time_elpased: 6.001
batch start
#iterations: 210
currently lose_sum: 593.2835988402367
time_elpased: 6.331
batch start
#iterations: 211
currently lose_sum: 593.8103473782539
time_elpased: 6.251
batch start
#iterations: 212
currently lose_sum: 593.6426787376404
time_elpased: 6.305
batch start
#iterations: 213
currently lose_sum: 594.584328353405
time_elpased: 6.206
batch start
#iterations: 214
currently lose_sum: 593.9439188241959
time_elpased: 6.111
batch start
#iterations: 215
currently lose_sum: 593.6748278737068
time_elpased: 6.236
batch start
#iterations: 216
currently lose_sum: 592.3550807833672
time_elpased: 6.242
batch start
#iterations: 217
currently lose_sum: 593.8106219768524
time_elpased: 5.938
batch start
#iterations: 218
currently lose_sum: 593.2132172584534
time_elpased: 6.184
batch start
#iterations: 219
currently lose_sum: 593.1294087171555
time_elpased: 6.043
start validation test
0.548448753463
0.546779347209
0.942163219101
0.691974830408
0
validation finish
batch start
#iterations: 220
currently lose_sum: 593.2995542883873
time_elpased: 6.144
batch start
#iterations: 221
currently lose_sum: 591.5022749900818
time_elpased: 6.075
batch start
#iterations: 222
currently lose_sum: 593.2693009972572
time_elpased: 6.356
batch start
#iterations: 223
currently lose_sum: 592.7033260464668
time_elpased: 6.016
batch start
#iterations: 224
currently lose_sum: 594.3184819221497
time_elpased: 6.275
batch start
#iterations: 225
currently lose_sum: 592.4472083449364
time_elpased: 5.897
batch start
#iterations: 226
currently lose_sum: 590.3858751654625
time_elpased: 5.912
batch start
#iterations: 227
currently lose_sum: 593.2411468029022
time_elpased: 6.092
batch start
#iterations: 228
currently lose_sum: 593.8457550406456
time_elpased: 6.049
batch start
#iterations: 229
currently lose_sum: 593.2193576693535
time_elpased: 6.197
batch start
#iterations: 230
currently lose_sum: 593.0431154966354
time_elpased: 6.243
batch start
#iterations: 231
currently lose_sum: 593.4564397931099
time_elpased: 6.019
batch start
#iterations: 232
currently lose_sum: 593.7190101742744
time_elpased: 5.999
batch start
#iterations: 233
currently lose_sum: 594.2458925247192
time_elpased: 5.986
batch start
#iterations: 234
currently lose_sum: 591.9196891188622
time_elpased: 6.093
batch start
#iterations: 235
currently lose_sum: 593.1154062151909
time_elpased: 6.151
batch start
#iterations: 236
currently lose_sum: 592.664573431015
time_elpased: 6.106
batch start
#iterations: 237
currently lose_sum: 593.3516824841499
time_elpased: 5.948
batch start
#iterations: 238
currently lose_sum: 593.889970600605
time_elpased: 7.37
batch start
#iterations: 239
currently lose_sum: 591.3873602151871
time_elpased: 6.044
start validation test
0.549058171745
0.547314117753
0.938972934033
0.691539554713
0
validation finish
batch start
#iterations: 240
currently lose_sum: 594.0117430686951
time_elpased: 6.18
batch start
#iterations: 241
currently lose_sum: 591.1878878474236
time_elpased: 6.082
batch start
#iterations: 242
currently lose_sum: 593.4014562964439
time_elpased: 5.949
batch start
#iterations: 243
currently lose_sum: 593.2062794566154
time_elpased: 5.968
batch start
#iterations: 244
currently lose_sum: 593.5916585922241
time_elpased: 6.18
batch start
#iterations: 245
currently lose_sum: 592.7962409257889
time_elpased: 6.303
batch start
#iterations: 246
currently lose_sum: 592.0475977063179
time_elpased: 6.197
batch start
#iterations: 247
currently lose_sum: 592.8248805403709
time_elpased: 6.149
batch start
#iterations: 248
currently lose_sum: 592.2029946446419
time_elpased: 6.134
batch start
#iterations: 249
currently lose_sum: 594.2011917829514
time_elpased: 5.933
batch start
#iterations: 250
currently lose_sum: 592.3000514507294
time_elpased: 6.112
batch start
#iterations: 251
currently lose_sum: 592.25261759758
time_elpased: 6.138
batch start
#iterations: 252
currently lose_sum: 593.1803122162819
time_elpased: 6.125
batch start
#iterations: 253
currently lose_sum: 592.8276754021645
time_elpased: 5.98
batch start
#iterations: 254
currently lose_sum: 591.9342660903931
time_elpased: 5.911
batch start
#iterations: 255
currently lose_sum: 592.5846344828606
time_elpased: 6.123
batch start
#iterations: 256
currently lose_sum: 591.8535437583923
time_elpased: 6.159
batch start
#iterations: 257
currently lose_sum: 594.9530273079872
time_elpased: 6.121
batch start
#iterations: 258
currently lose_sum: 592.5323687791824
time_elpased: 6.136
batch start
#iterations: 259
currently lose_sum: 591.5025401711464
time_elpased: 5.872
start validation test
0.548698060942
0.546814470469
0.944221467531
0.69255736715
0
validation finish
batch start
#iterations: 260
currently lose_sum: 592.0205233097076
time_elpased: 6.128
batch start
#iterations: 261
currently lose_sum: 593.4238575696945
time_elpased: 6.267
batch start
#iterations: 262
currently lose_sum: 592.2508302330971
time_elpased: 6.294
batch start
#iterations: 263
currently lose_sum: 591.4648443460464
time_elpased: 6.298
batch start
#iterations: 264
currently lose_sum: 592.9233357906342
time_elpased: 6.237
batch start
#iterations: 265
currently lose_sum: 594.8593670129776
time_elpased: 6.123
batch start
#iterations: 266
currently lose_sum: 592.0281187295914
time_elpased: 6.254
batch start
#iterations: 267
currently lose_sum: 593.7587886452675
time_elpased: 6.289
batch start
#iterations: 268
currently lose_sum: 593.216916680336
time_elpased: 6.223
batch start
#iterations: 269
currently lose_sum: 591.6944470405579
time_elpased: 6.058
batch start
#iterations: 270
currently lose_sum: 592.3759559988976
time_elpased: 5.932
batch start
#iterations: 271
currently lose_sum: 593.3890013098717
time_elpased: 6.148
batch start
#iterations: 272
currently lose_sum: 593.3249651789665
time_elpased: 6.213
batch start
#iterations: 273
currently lose_sum: 590.4632467627525
time_elpased: 6.335
batch start
#iterations: 274
currently lose_sum: 592.3215716481209
time_elpased: 6.244
batch start
#iterations: 275
currently lose_sum: 593.0827001333237
time_elpased: 5.948
batch start
#iterations: 276
currently lose_sum: 593.6762673258781
time_elpased: 6.072
batch start
#iterations: 277
currently lose_sum: 593.7436947226524
time_elpased: 5.988
batch start
#iterations: 278
currently lose_sum: 592.7153217196465
time_elpased: 6.184
batch start
#iterations: 279
currently lose_sum: 593.0243802666664
time_elpased: 6.317
start validation test
0.548476454294
0.547003419521
0.938355459504
0.691124080952
0
validation finish
batch start
#iterations: 280
currently lose_sum: 593.2385100722313
time_elpased: 6.048
batch start
#iterations: 281
currently lose_sum: 592.6944361329079
time_elpased: 6.19
batch start
#iterations: 282
currently lose_sum: 591.9479581713676
time_elpased: 5.883
batch start
#iterations: 283
currently lose_sum: 594.3074600696564
time_elpased: 6.311
batch start
#iterations: 284
currently lose_sum: 592.462361574173
time_elpased: 6.281
batch start
#iterations: 285
currently lose_sum: 592.1957732439041
time_elpased: 6.183
batch start
#iterations: 286
currently lose_sum: 591.617507994175
time_elpased: 5.946
batch start
#iterations: 287
currently lose_sum: 593.3338173031807
time_elpased: 6.224
batch start
#iterations: 288
currently lose_sum: 591.4578477740288
time_elpased: 5.868
batch start
#iterations: 289
currently lose_sum: 591.8547818660736
time_elpased: 6.105
batch start
#iterations: 290
currently lose_sum: 590.9672165513039
time_elpased: 6.167
batch start
#iterations: 291
currently lose_sum: 591.8051581382751
time_elpased: 6.022
batch start
#iterations: 292
currently lose_sum: 593.1324439644814
time_elpased: 6.17
batch start
#iterations: 293
currently lose_sum: 590.9749304652214
time_elpased: 6.057
batch start
#iterations: 294
currently lose_sum: 593.3514808416367
time_elpased: 6.004
batch start
#iterations: 295
currently lose_sum: 592.8632535338402
time_elpased: 6.079
batch start
#iterations: 296
currently lose_sum: 593.9197425842285
time_elpased: 6.164
batch start
#iterations: 297
currently lose_sum: 592.4859630465508
time_elpased: 6.087
batch start
#iterations: 298
currently lose_sum: 593.1016381382942
time_elpased: 6.169
batch start
#iterations: 299
currently lose_sum: 593.2337272763252
time_elpased: 6.133
start validation test
0.548587257618
0.546990116801
0.939796233405
0.691503861881
0
validation finish
batch start
#iterations: 300
currently lose_sum: 592.4658414721489
time_elpased: 6.19
batch start
#iterations: 301
currently lose_sum: 592.4265848994255
time_elpased: 5.816
batch start
#iterations: 302
currently lose_sum: 592.07320946455
time_elpased: 5.927
batch start
#iterations: 303
currently lose_sum: 592.7319042086601
time_elpased: 6.116
batch start
#iterations: 304
currently lose_sum: 592.6368005871773
time_elpased: 6.16
batch start
#iterations: 305
currently lose_sum: 592.6480203270912
time_elpased: 6.037
batch start
#iterations: 306
currently lose_sum: 592.4253821372986
time_elpased: 5.913
batch start
#iterations: 307
currently lose_sum: 593.4949421882629
time_elpased: 6.118
batch start
#iterations: 308
currently lose_sum: 591.927229821682
time_elpased: 6.059
batch start
#iterations: 309
currently lose_sum: 594.0118060708046
time_elpased: 6.157
batch start
#iterations: 310
currently lose_sum: 592.233545601368
time_elpased: 6.214
batch start
#iterations: 311
currently lose_sum: 591.8131700158119
time_elpased: 6.062
batch start
#iterations: 312
currently lose_sum: 591.5140223503113
time_elpased: 6.039
batch start
#iterations: 313
currently lose_sum: 592.3594609498978
time_elpased: 6.091
batch start
#iterations: 314
currently lose_sum: 593.0697174668312
time_elpased: 6.216
batch start
#iterations: 315
currently lose_sum: 591.8310179710388
time_elpased: 6.186
batch start
#iterations: 316
currently lose_sum: 591.8992012143135
time_elpased: 6.121
batch start
#iterations: 317
currently lose_sum: 590.8831292390823
time_elpased: 6.26
batch start
#iterations: 318
currently lose_sum: 592.0683045983315
time_elpased: 6.092
batch start
#iterations: 319
currently lose_sum: 591.6292575597763
time_elpased: 6.072
start validation test
0.548698060942
0.547282248841
0.935679736544
0.690619065705
0
validation finish
batch start
#iterations: 320
currently lose_sum: 592.8202041983604
time_elpased: 6.068
batch start
#iterations: 321
currently lose_sum: 594.0122720003128
time_elpased: 6.051
batch start
#iterations: 322
currently lose_sum: 590.7950546741486
time_elpased: 6.147
batch start
#iterations: 323
currently lose_sum: 592.9286941289902
time_elpased: 6.237
batch start
#iterations: 324
currently lose_sum: 593.5284815430641
time_elpased: 5.883
batch start
#iterations: 325
currently lose_sum: 594.3030074238777
time_elpased: 5.683
batch start
#iterations: 326
currently lose_sum: 592.0464348196983
time_elpased: 6.123
batch start
#iterations: 327
currently lose_sum: 593.4910819530487
time_elpased: 6.236
batch start
#iterations: 328
currently lose_sum: 592.5721585154533
time_elpased: 6.011
batch start
#iterations: 329
currently lose_sum: 591.7781454324722
time_elpased: 6.323
batch start
#iterations: 330
currently lose_sum: 591.6862688064575
time_elpased: 6.262
batch start
#iterations: 331
currently lose_sum: 592.0702212452888
time_elpased: 6.07
batch start
#iterations: 332
currently lose_sum: 593.6216117143631
time_elpased: 6.297
batch start
#iterations: 333
currently lose_sum: 591.8163219094276
time_elpased: 6.078
batch start
#iterations: 334
currently lose_sum: 592.9186215996742
time_elpased: 5.963
batch start
#iterations: 335
currently lose_sum: 592.7322243452072
time_elpased: 6.074
batch start
#iterations: 336
currently lose_sum: 592.1841099262238
time_elpased: 6.121
batch start
#iterations: 337
currently lose_sum: 591.1445569396019
time_elpased: 6.068
batch start
#iterations: 338
currently lose_sum: 592.407693862915
time_elpased: 5.955
batch start
#iterations: 339
currently lose_sum: 592.8799711465836
time_elpased: 6.23
start validation test
0.548199445983
0.54707655214
0.9340331378
0.690006462158
0
validation finish
batch start
#iterations: 340
currently lose_sum: 591.5836995244026
time_elpased: 6.047
batch start
#iterations: 341
currently lose_sum: 592.2697315812111
time_elpased: 6.296
batch start
#iterations: 342
currently lose_sum: 593.9695518016815
time_elpased: 6.2
batch start
#iterations: 343
currently lose_sum: 593.2143934965134
time_elpased: 6.253
batch start
#iterations: 344
currently lose_sum: 592.6173954606056
time_elpased: 6.09
batch start
#iterations: 345
currently lose_sum: 592.3243287801743
time_elpased: 5.921
batch start
#iterations: 346
currently lose_sum: 591.989651799202
time_elpased: 6.186
batch start
#iterations: 347
currently lose_sum: 592.3162113428116
time_elpased: 5.994
batch start
#iterations: 348
currently lose_sum: 592.6356990933418
time_elpased: 6.223
batch start
#iterations: 349
currently lose_sum: 592.6330945491791
time_elpased: 6.207
batch start
#iterations: 350
currently lose_sum: 593.0137971043587
time_elpased: 6.161
batch start
#iterations: 351
currently lose_sum: 593.0683507323265
time_elpased: 6.213
batch start
#iterations: 352
currently lose_sum: 592.9632905721664
time_elpased: 6.196
batch start
#iterations: 353
currently lose_sum: 592.54818546772
time_elpased: 6.108
batch start
#iterations: 354
currently lose_sum: 591.8927565813065
time_elpased: 5.801
batch start
#iterations: 355
currently lose_sum: 592.439208984375
time_elpased: 6.276
batch start
#iterations: 356
currently lose_sum: 591.9861080646515
time_elpased: 6.342
batch start
#iterations: 357
currently lose_sum: 593.7236191034317
time_elpased: 6.275
batch start
#iterations: 358
currently lose_sum: 592.5630066990852
time_elpased: 6.222
batch start
#iterations: 359
currently lose_sum: 593.364751458168
time_elpased: 6.214
start validation test
0.547977839335
0.546922057583
0.934444787486
0.68999582051
0
validation finish
batch start
#iterations: 360
currently lose_sum: 591.5694323182106
time_elpased: 6.208
batch start
#iterations: 361
currently lose_sum: 592.6205915212631
time_elpased: 6.051
batch start
#iterations: 362
currently lose_sum: 592.5956169962883
time_elpased: 5.922
batch start
#iterations: 363
currently lose_sum: 592.4616503119469
time_elpased: 6.116
batch start
#iterations: 364
currently lose_sum: 589.8197592496872
time_elpased: 6.278
batch start
#iterations: 365
currently lose_sum: 593.6064312458038
time_elpased: 6.377
batch start
#iterations: 366
currently lose_sum: 591.8519017696381
time_elpased: 6.398
batch start
#iterations: 367
currently lose_sum: 591.9889125227928
time_elpased: 6.282
batch start
#iterations: 368
currently lose_sum: 593.4553999900818
time_elpased: 5.9
batch start
#iterations: 369
currently lose_sum: 594.1280729174614
time_elpased: 6.503
batch start
#iterations: 370
currently lose_sum: 593.645530462265
time_elpased: 5.679
batch start
#iterations: 371
currently lose_sum: 592.3276243209839
time_elpased: 6.135
batch start
#iterations: 372
currently lose_sum: 591.9713298678398
time_elpased: 6.087
batch start
#iterations: 373
currently lose_sum: 590.7927270829678
time_elpased: 6.303
batch start
#iterations: 374
currently lose_sum: 591.8781574368477
time_elpased: 5.993
batch start
#iterations: 375
currently lose_sum: 593.2701270580292
time_elpased: 5.953
batch start
#iterations: 376
currently lose_sum: 591.0919414758682
time_elpased: 6.236
batch start
#iterations: 377
currently lose_sum: 592.675431728363
time_elpased: 6.19
batch start
#iterations: 378
currently lose_sum: 590.8886689543724
time_elpased: 6.034
batch start
#iterations: 379
currently lose_sum: 592.4069029688835
time_elpased: 6.031
start validation test
0.548005540166
0.546808830155
0.936811773181
0.690549792334
0
validation finish
batch start
#iterations: 380
currently lose_sum: 591.188659787178
time_elpased: 5.897
batch start
#iterations: 381
currently lose_sum: 589.9196202158928
time_elpased: 6.054
batch start
#iterations: 382
currently lose_sum: 590.9640292525291
time_elpased: 6.143
batch start
#iterations: 383
currently lose_sum: 591.9458464384079
time_elpased: 6.164
batch start
#iterations: 384
currently lose_sum: 592.9613736271858
time_elpased: 6.23
batch start
#iterations: 385
currently lose_sum: 592.1079167723656
time_elpased: 6.098
batch start
#iterations: 386
currently lose_sum: 592.3669720888138
time_elpased: 6.125
batch start
#iterations: 387
currently lose_sum: 593.7621914148331
time_elpased: 6.061
batch start
#iterations: 388
currently lose_sum: 592.2731410264969
time_elpased: 6.015
batch start
#iterations: 389
currently lose_sum: 592.7113549113274
time_elpased: 6.084
batch start
#iterations: 390
currently lose_sum: 590.807240664959
time_elpased: 6.115
batch start
#iterations: 391
currently lose_sum: 591.3736537694931
time_elpased: 6.248
batch start
#iterations: 392
currently lose_sum: 594.7062982916832
time_elpased: 6.018
batch start
#iterations: 393
currently lose_sum: 592.8110824227333
time_elpased: 5.961
batch start
#iterations: 394
currently lose_sum: 593.3041313886642
time_elpased: 6.163
batch start
#iterations: 395
currently lose_sum: 591.4283683896065
time_elpased: 6.37
batch start
#iterations: 396
currently lose_sum: 592.5958654880524
time_elpased: 6.283
batch start
#iterations: 397
currently lose_sum: 593.2217896580696
time_elpased: 6.213
batch start
#iterations: 398
currently lose_sum: 592.1675547361374
time_elpased: 6.223
batch start
#iterations: 399
currently lose_sum: 592.4412280321121
time_elpased: 6.108
start validation test
0.547811634349
0.546682677875
0.937017598024
0.690505081147
0
validation finish
acc: 0.547
pre: 0.545
rec: 0.951
F1: 0.693
auc: 0.000
