start to construct graph
graph construct over
92947
epochs start
batch start
#iterations: 0
currently lose_sum: 614.7938818335533
time_elpased: 5.993
batch start
#iterations: 1
currently lose_sum: 609.7248967289925
time_elpased: 5.59
batch start
#iterations: 2
currently lose_sum: 607.9032453298569
time_elpased: 5.762
batch start
#iterations: 3
currently lose_sum: 604.8761382102966
time_elpased: 5.553
batch start
#iterations: 4
currently lose_sum: 604.9096690416336
time_elpased: 5.756
batch start
#iterations: 5
currently lose_sum: 603.8219764828682
time_elpased: 5.366
batch start
#iterations: 6
currently lose_sum: 603.2687375545502
time_elpased: 5.644
batch start
#iterations: 7
currently lose_sum: 603.2043287754059
time_elpased: 5.561
batch start
#iterations: 8
currently lose_sum: 602.0736823678017
time_elpased: 5.689
batch start
#iterations: 9
currently lose_sum: 601.6779258847237
time_elpased: 5.739
batch start
#iterations: 10
currently lose_sum: 603.4785575270653
time_elpased: 5.626
batch start
#iterations: 11
currently lose_sum: 601.0214812159538
time_elpased: 5.588
batch start
#iterations: 12
currently lose_sum: 601.7279273271561
time_elpased: 5.644
batch start
#iterations: 13
currently lose_sum: 599.6428849101067
time_elpased: 5.665
batch start
#iterations: 14
currently lose_sum: 600.8850337862968
time_elpased: 5.638
batch start
#iterations: 15
currently lose_sum: 600.1493787765503
time_elpased: 5.792
batch start
#iterations: 16
currently lose_sum: 601.1360113620758
time_elpased: 5.381
batch start
#iterations: 17
currently lose_sum: 598.3971506357193
time_elpased: 5.698
batch start
#iterations: 18
currently lose_sum: 598.9851832389832
time_elpased: 5.685
batch start
#iterations: 19
currently lose_sum: 598.6199803948402
time_elpased: 5.635
start validation test
0.548559556787
0.545917620539
0.959555418339
0.695911779523
0
validation finish
batch start
#iterations: 20
currently lose_sum: 599.8980922698975
time_elpased: 5.983
batch start
#iterations: 21
currently lose_sum: 597.3156340122223
time_elpased: 5.542
batch start
#iterations: 22
currently lose_sum: 598.6204417943954
time_elpased: 5.568
batch start
#iterations: 23
currently lose_sum: 599.6342623829842
time_elpased: 5.644
batch start
#iterations: 24
currently lose_sum: 598.5647946000099
time_elpased: 5.672
batch start
#iterations: 25
currently lose_sum: 599.1066895127296
time_elpased: 5.613
batch start
#iterations: 26
currently lose_sum: 598.978305876255
time_elpased: 5.672
batch start
#iterations: 27
currently lose_sum: 597.0492269396782
time_elpased: 5.588
batch start
#iterations: 28
currently lose_sum: 597.472750544548
time_elpased: 5.97
batch start
#iterations: 29
currently lose_sum: 598.3818719983101
time_elpased: 5.576
batch start
#iterations: 30
currently lose_sum: 598.2588487267494
time_elpased: 5.684
batch start
#iterations: 31
currently lose_sum: 598.0649923682213
time_elpased: 5.613
batch start
#iterations: 32
currently lose_sum: 599.3093773126602
time_elpased: 5.707
batch start
#iterations: 33
currently lose_sum: 596.5701631903648
time_elpased: 5.78
batch start
#iterations: 34
currently lose_sum: 597.4937888383865
time_elpased: 5.478
batch start
#iterations: 35
currently lose_sum: 597.1757784485817
time_elpased: 5.855
batch start
#iterations: 36
currently lose_sum: 597.9861664772034
time_elpased: 5.532
batch start
#iterations: 37
currently lose_sum: 597.6186829805374
time_elpased: 5.783
batch start
#iterations: 38
currently lose_sum: 596.6221962571144
time_elpased: 5.598
batch start
#iterations: 39
currently lose_sum: 595.7771838307381
time_elpased: 5.794
start validation test
0.549030470914
0.546609919016
0.951631161881
0.694375610122
0
validation finish
batch start
#iterations: 40
currently lose_sum: 596.7501150965691
time_elpased: 5.625
batch start
#iterations: 41
currently lose_sum: 596.201907157898
time_elpased: 5.722
batch start
#iterations: 42
currently lose_sum: 596.6790692210197
time_elpased: 5.68
batch start
#iterations: 43
currently lose_sum: 596.433754503727
time_elpased: 5.514
batch start
#iterations: 44
currently lose_sum: 596.9405065178871
time_elpased: 5.796
batch start
#iterations: 45
currently lose_sum: 596.329351902008
time_elpased: 5.914
batch start
#iterations: 46
currently lose_sum: 596.24537140131
time_elpased: 5.588
batch start
#iterations: 47
currently lose_sum: 595.1177080869675
time_elpased: 5.926
batch start
#iterations: 48
currently lose_sum: 597.5470441579819
time_elpased: 5.887
batch start
#iterations: 49
currently lose_sum: 597.4330370426178
time_elpased: 5.834
batch start
#iterations: 50
currently lose_sum: 596.0439703464508
time_elpased: 5.675
batch start
#iterations: 51
currently lose_sum: 596.4293203353882
time_elpased: 5.862
batch start
#iterations: 52
currently lose_sum: 596.9088817834854
time_elpased: 5.628
batch start
#iterations: 53
currently lose_sum: 595.6124002337456
time_elpased: 5.85
batch start
#iterations: 54
currently lose_sum: 596.2449487447739
time_elpased: 5.955
batch start
#iterations: 55
currently lose_sum: 596.0283655524254
time_elpased: 5.53
batch start
#iterations: 56
currently lose_sum: 594.7914718389511
time_elpased: 5.824
batch start
#iterations: 57
currently lose_sum: 595.5342301726341
time_elpased: 5.291
batch start
#iterations: 58
currently lose_sum: 595.1549153327942
time_elpased: 5.693
batch start
#iterations: 59
currently lose_sum: 596.5415140390396
time_elpased: 5.505
start validation test
0.550249307479
0.547762709839
0.943603993002
0.693150892047
0
validation finish
batch start
#iterations: 60
currently lose_sum: 595.2034301757812
time_elpased: 5.759
batch start
#iterations: 61
currently lose_sum: 596.7039644122124
time_elpased: 5.759
batch start
#iterations: 62
currently lose_sum: 596.4833489060402
time_elpased: 5.676
batch start
#iterations: 63
currently lose_sum: 595.4260023236275
time_elpased: 5.594
batch start
#iterations: 64
currently lose_sum: 596.4648710489273
time_elpased: 5.624
batch start
#iterations: 65
currently lose_sum: 595.6166521906853
time_elpased: 5.566
batch start
#iterations: 66
currently lose_sum: 594.1880650520325
time_elpased: 5.614
batch start
#iterations: 67
currently lose_sum: 596.3534145355225
time_elpased: 5.209
batch start
#iterations: 68
currently lose_sum: 594.4299225211143
time_elpased: 5.838
batch start
#iterations: 69
currently lose_sum: 593.7798449993134
time_elpased: 5.71
batch start
#iterations: 70
currently lose_sum: 594.9618729352951
time_elpased: 5.648
batch start
#iterations: 71
currently lose_sum: 596.6131728291512
time_elpased: 5.937
batch start
#iterations: 72
currently lose_sum: 595.528707921505
time_elpased: 5.538
batch start
#iterations: 73
currently lose_sum: 594.3877764940262
time_elpased: 5.724
batch start
#iterations: 74
currently lose_sum: 595.0982874631882
time_elpased: 5.658
batch start
#iterations: 75
currently lose_sum: 595.3496817946434
time_elpased: 5.54
batch start
#iterations: 76
currently lose_sum: 595.6031376719475
time_elpased: 5.636
batch start
#iterations: 77
currently lose_sum: 594.8936562538147
time_elpased: 5.581
batch start
#iterations: 78
currently lose_sum: 593.6657183170319
time_elpased: 5.867
batch start
#iterations: 79
currently lose_sum: 595.6510561108589
time_elpased: 5.47
start validation test
0.548421052632
0.545650652985
0.963157353093
0.696639249693
0
validation finish
batch start
#iterations: 80
currently lose_sum: 595.2774885296822
time_elpased: 5.706
batch start
#iterations: 81
currently lose_sum: 596.4352504611015
time_elpased: 5.804
batch start
#iterations: 82
currently lose_sum: 594.8793236613274
time_elpased: 5.559
batch start
#iterations: 83
currently lose_sum: 594.3872891664505
time_elpased: 5.872
batch start
#iterations: 84
currently lose_sum: 593.7208510637283
time_elpased: 5.545
batch start
#iterations: 85
currently lose_sum: 595.2787415385246
time_elpased: 5.79
batch start
#iterations: 86
currently lose_sum: 594.8922058343887
time_elpased: 5.604
batch start
#iterations: 87
currently lose_sum: 595.2111440300941
time_elpased: 5.627
batch start
#iterations: 88
currently lose_sum: 595.192680478096
time_elpased: 5.931
batch start
#iterations: 89
currently lose_sum: 594.0034910440445
time_elpased: 5.627
batch start
#iterations: 90
currently lose_sum: 595.6754159331322
time_elpased: 5.83
batch start
#iterations: 91
currently lose_sum: 593.1487600207329
time_elpased: 5.605
batch start
#iterations: 92
currently lose_sum: 595.5926743149757
time_elpased: 5.671
batch start
#iterations: 93
currently lose_sum: 594.8278883099556
time_elpased: 5.658
batch start
#iterations: 94
currently lose_sum: 594.2273846268654
time_elpased: 5.54
batch start
#iterations: 95
currently lose_sum: 594.3537209630013
time_elpased: 5.821
batch start
#iterations: 96
currently lose_sum: 594.1172449588776
time_elpased: 5.635
batch start
#iterations: 97
currently lose_sum: 595.1481425762177
time_elpased: 5.734
batch start
#iterations: 98
currently lose_sum: 594.7944557666779
time_elpased: 5.795
batch start
#iterations: 99
currently lose_sum: 594.10870885849
time_elpased: 5.505
start validation test
0.550193905817
0.547844311377
0.941545744571
0.692660029526
0
validation finish
batch start
#iterations: 100
currently lose_sum: 592.7992265224457
time_elpased: 5.568
batch start
#iterations: 101
currently lose_sum: 593.9069809317589
time_elpased: 5.617
batch start
#iterations: 102
currently lose_sum: 593.568864762783
time_elpased: 5.759
batch start
#iterations: 103
currently lose_sum: 594.3278797864914
time_elpased: 5.388
batch start
#iterations: 104
currently lose_sum: 593.6596887111664
time_elpased: 5.648
batch start
#iterations: 105
currently lose_sum: 594.2041945457458
time_elpased: 5.688
batch start
#iterations: 106
currently lose_sum: 593.8724431395531
time_elpased: 5.656
batch start
#iterations: 107
currently lose_sum: 593.0407783389091
time_elpased: 5.63
batch start
#iterations: 108
currently lose_sum: 592.3143962025642
time_elpased: 5.612
batch start
#iterations: 109
currently lose_sum: 592.0754653215408
time_elpased: 5.916
batch start
#iterations: 110
currently lose_sum: 594.582138478756
time_elpased: 5.496
batch start
#iterations: 111
currently lose_sum: 595.0373885631561
time_elpased: 5.808
batch start
#iterations: 112
currently lose_sum: 594.5247191786766
time_elpased: 5.561
batch start
#iterations: 113
currently lose_sum: 594.9195628762245
time_elpased: 5.895
batch start
#iterations: 114
currently lose_sum: 594.1420775651932
time_elpased: 5.807
batch start
#iterations: 115
currently lose_sum: 594.3766450285912
time_elpased: 5.76
batch start
#iterations: 116
currently lose_sum: 594.2713516950607
time_elpased: 5.828
batch start
#iterations: 117
currently lose_sum: 593.7239723801613
time_elpased: 5.785
batch start
#iterations: 118
currently lose_sum: 592.9338755011559
time_elpased: 5.671
batch start
#iterations: 119
currently lose_sum: 594.1580740213394
time_elpased: 5.774
start validation test
0.549002770083
0.546260160216
0.957908819595
0.695756171398
0
validation finish
batch start
#iterations: 120
currently lose_sum: 594.0668586492538
time_elpased: 5.36
batch start
#iterations: 121
currently lose_sum: 593.8034895062447
time_elpased: 5.736
batch start
#iterations: 122
currently lose_sum: 594.6811479926109
time_elpased: 5.609
batch start
#iterations: 123
currently lose_sum: 593.7783971428871
time_elpased: 5.712
batch start
#iterations: 124
currently lose_sum: 592.7016445994377
time_elpased: 5.516
batch start
#iterations: 125
currently lose_sum: 594.2139794826508
time_elpased: 5.784
batch start
#iterations: 126
currently lose_sum: 592.1242232918739
time_elpased: 5.785
batch start
#iterations: 127
currently lose_sum: 593.1440334320068
time_elpased: 5.676
batch start
#iterations: 128
currently lose_sum: 592.6105982661247
time_elpased: 6.003
batch start
#iterations: 129
currently lose_sum: 593.5013417005539
time_elpased: 5.619
batch start
#iterations: 130
currently lose_sum: 594.0211896300316
time_elpased: 5.708
batch start
#iterations: 131
currently lose_sum: 592.7424284219742
time_elpased: 5.722
batch start
#iterations: 132
currently lose_sum: 594.785148024559
time_elpased: 5.675
batch start
#iterations: 133
currently lose_sum: 593.7239307761192
time_elpased: 5.755
batch start
#iterations: 134
currently lose_sum: 593.1897914409637
time_elpased: 5.52
batch start
#iterations: 135
currently lose_sum: 593.2084856033325
time_elpased: 5.82
batch start
#iterations: 136
currently lose_sum: 593.4486969709396
time_elpased: 5.676
batch start
#iterations: 137
currently lose_sum: 594.4407469034195
time_elpased: 5.651
batch start
#iterations: 138
currently lose_sum: 593.6888220906258
time_elpased: 5.895
batch start
#iterations: 139
currently lose_sum: 592.7348772287369
time_elpased: 5.773
start validation test
0.550221606648
0.547660932049
0.945147679325
0.693485360467
0
validation finish
batch start
#iterations: 140
currently lose_sum: 596.1092146039009
time_elpased: 5.83
batch start
#iterations: 141
currently lose_sum: 592.6343820095062
time_elpased: 5.638
batch start
#iterations: 142
currently lose_sum: 592.7882963418961
time_elpased: 5.755
batch start
#iterations: 143
currently lose_sum: 593.4658617377281
time_elpased: 5.736
batch start
#iterations: 144
currently lose_sum: 592.6876888275146
time_elpased: 5.625
batch start
#iterations: 145
currently lose_sum: 593.3678109049797
time_elpased: 5.753
batch start
#iterations: 146
currently lose_sum: 593.6216791272163
time_elpased: 5.61
batch start
#iterations: 147
currently lose_sum: 592.3920835256577
time_elpased: 5.731
batch start
#iterations: 148
currently lose_sum: 594.9760548472404
time_elpased: 5.822
batch start
#iterations: 149
currently lose_sum: 593.6062884926796
time_elpased: 5.418
batch start
#iterations: 150
currently lose_sum: 593.6480258107185
time_elpased: 5.951
batch start
#iterations: 151
currently lose_sum: 594.620517373085
time_elpased: 5.728
batch start
#iterations: 152
currently lose_sum: 594.2522122859955
time_elpased: 5.722
batch start
#iterations: 153
currently lose_sum: 592.9302055239677
time_elpased: 5.778
batch start
#iterations: 154
currently lose_sum: 592.968695640564
time_elpased: 5.644
batch start
#iterations: 155
currently lose_sum: 594.5280467271805
time_elpased: 5.7
batch start
#iterations: 156
currently lose_sum: 593.3201315402985
time_elpased: 5.753
batch start
#iterations: 157
currently lose_sum: 593.131534576416
time_elpased: 5.691
batch start
#iterations: 158
currently lose_sum: 593.1890305280685
time_elpased: 5.935
batch start
#iterations: 159
currently lose_sum: 593.4241917133331
time_elpased: 5.405
start validation test
0.549806094183
0.547574905807
0.942266131522
0.692639382707
0
validation finish
batch start
#iterations: 160
currently lose_sum: 592.137886762619
time_elpased: 5.764
batch start
#iterations: 161
currently lose_sum: 593.6191713809967
time_elpased: 5.739
batch start
#iterations: 162
currently lose_sum: 591.7585907578468
time_elpased: 5.872
batch start
#iterations: 163
currently lose_sum: 593.2730359435081
time_elpased: 5.83
batch start
#iterations: 164
currently lose_sum: 593.5829793214798
time_elpased: 5.743
batch start
#iterations: 165
currently lose_sum: 596.0902512073517
time_elpased: 5.564
batch start
#iterations: 166
currently lose_sum: 593.7871558666229
time_elpased: 5.676
batch start
#iterations: 167
currently lose_sum: 593.2351450324059
time_elpased: 5.625
batch start
#iterations: 168
currently lose_sum: 593.518658220768
time_elpased: 5.625
batch start
#iterations: 169
currently lose_sum: 592.9146730899811
time_elpased: 5.669
batch start
#iterations: 170
currently lose_sum: 592.3808169960976
time_elpased: 5.848
batch start
#iterations: 171
currently lose_sum: 593.3180981874466
time_elpased: 5.603
batch start
#iterations: 172
currently lose_sum: 592.9708938002586
time_elpased: 5.745
batch start
#iterations: 173
currently lose_sum: 593.7916790246964
time_elpased: 5.702
batch start
#iterations: 174
currently lose_sum: 592.6916045546532
time_elpased: 5.587
batch start
#iterations: 175
currently lose_sum: 592.8074981570244
time_elpased: 5.905
batch start
#iterations: 176
currently lose_sum: 593.4980156421661
time_elpased: 5.668
batch start
#iterations: 177
currently lose_sum: 592.3506327271461
time_elpased: 5.711
batch start
#iterations: 178
currently lose_sum: 594.8251059651375
time_elpased: 5.655
batch start
#iterations: 179
currently lose_sum: 593.8904812932014
time_elpased: 5.765
start validation test
0.549418282548
0.546975088968
0.949058351343
0.693983519585
0
validation finish
batch start
#iterations: 180
currently lose_sum: 593.9265340566635
time_elpased: 5.77
batch start
#iterations: 181
currently lose_sum: 593.532035946846
time_elpased: 5.731
batch start
#iterations: 182
currently lose_sum: 592.4696082472801
time_elpased: 5.727
batch start
#iterations: 183
currently lose_sum: 593.6927157044411
time_elpased: 5.844
batch start
#iterations: 184
currently lose_sum: 591.7147242426872
time_elpased: 5.394
batch start
#iterations: 185
currently lose_sum: 592.4109047055244
time_elpased: 5.803
batch start
#iterations: 186
currently lose_sum: 592.8916647434235
time_elpased: 5.531
batch start
#iterations: 187
currently lose_sum: 594.3076323270798
time_elpased: 5.566
batch start
#iterations: 188
currently lose_sum: 591.303437769413
time_elpased: 5.847
batch start
#iterations: 189
currently lose_sum: 592.8211155533791
time_elpased: 5.655
batch start
#iterations: 190
currently lose_sum: 592.6257776021957
time_elpased: 5.777
batch start
#iterations: 191
currently lose_sum: 593.66192060709
time_elpased: 5.508
batch start
#iterations: 192
currently lose_sum: 594.2955923676491
time_elpased: 5.944
batch start
#iterations: 193
currently lose_sum: 592.8941787481308
time_elpased: 5.853
batch start
#iterations: 194
currently lose_sum: 594.7425365447998
time_elpased: 5.642
batch start
#iterations: 195
currently lose_sum: 592.5991925001144
time_elpased: 5.775
batch start
#iterations: 196
currently lose_sum: 594.280780851841
time_elpased: 5.528
batch start
#iterations: 197
currently lose_sum: 593.2420745491982
time_elpased: 5.83
batch start
#iterations: 198
currently lose_sum: 593.7875365614891
time_elpased: 5.808
batch start
#iterations: 199
currently lose_sum: 593.1318202614784
time_elpased: 5.572
start validation test
0.549085872576
0.546389934149
0.956365133272
0.695453695042
0
validation finish
batch start
#iterations: 200
currently lose_sum: 594.4061548113823
time_elpased: 5.773
batch start
#iterations: 201
currently lose_sum: 591.2003358006477
time_elpased: 5.901
batch start
#iterations: 202
currently lose_sum: 592.6561713814735
time_elpased: 6.142
batch start
#iterations: 203
currently lose_sum: 594.313855946064
time_elpased: 5.61
batch start
#iterations: 204
currently lose_sum: 591.5701954960823
time_elpased: 5.809
batch start
#iterations: 205
currently lose_sum: 593.0507336258888
time_elpased: 5.777
batch start
#iterations: 206
currently lose_sum: 592.1822493076324
time_elpased: 5.454
batch start
#iterations: 207
currently lose_sum: 594.0244093537331
time_elpased: 5.675
batch start
#iterations: 208
currently lose_sum: 592.335341155529
time_elpased: 5.535
batch start
#iterations: 209
currently lose_sum: 590.4999027252197
time_elpased: 5.743
batch start
#iterations: 210
currently lose_sum: 593.0726960897446
time_elpased: 5.479
batch start
#iterations: 211
currently lose_sum: 593.9593569040298
time_elpased: 6.037
batch start
#iterations: 212
currently lose_sum: 593.198944747448
time_elpased: 5.721
batch start
#iterations: 213
currently lose_sum: 594.0553932189941
time_elpased: 5.661
batch start
#iterations: 214
currently lose_sum: 593.6888163089752
time_elpased: 5.688
batch start
#iterations: 215
currently lose_sum: 593.7327188849449
time_elpased: 5.694
batch start
#iterations: 216
currently lose_sum: 592.2174906134605
time_elpased: 5.928
batch start
#iterations: 217
currently lose_sum: 593.7484318614006
time_elpased: 5.614
batch start
#iterations: 218
currently lose_sum: 593.4571022391319
time_elpased: 5.631
batch start
#iterations: 219
currently lose_sum: 592.9253398180008
time_elpased: 5.813
start validation test
0.55
0.547507672139
0.945559329011
0.693473215465
0
validation finish
batch start
#iterations: 220
currently lose_sum: 593.0404798388481
time_elpased: 5.53
batch start
#iterations: 221
currently lose_sum: 591.4479984641075
time_elpased: 5.799
batch start
#iterations: 222
currently lose_sum: 593.2997941374779
time_elpased: 5.752
batch start
#iterations: 223
currently lose_sum: 592.7098643183708
time_elpased: 5.681
batch start
#iterations: 224
currently lose_sum: 593.6299273371696
time_elpased: 5.718
batch start
#iterations: 225
currently lose_sum: 592.5007727742195
time_elpased: 5.739
batch start
#iterations: 226
currently lose_sum: 591.1106789112091
time_elpased: 5.871
batch start
#iterations: 227
currently lose_sum: 593.2125325202942
time_elpased: 5.527
batch start
#iterations: 228
currently lose_sum: 594.052493929863
time_elpased: 5.781
batch start
#iterations: 229
currently lose_sum: 592.7128429412842
time_elpased: 5.512
batch start
#iterations: 230
currently lose_sum: 592.8638427257538
time_elpased: 5.743
batch start
#iterations: 231
currently lose_sum: 593.8531365394592
time_elpased: 5.623
batch start
#iterations: 232
currently lose_sum: 593.9434587359428
time_elpased: 5.706
batch start
#iterations: 233
currently lose_sum: 593.8578140735626
time_elpased: 5.673
batch start
#iterations: 234
currently lose_sum: 591.8661245107651
time_elpased: 5.56
batch start
#iterations: 235
currently lose_sum: 593.4547131061554
time_elpased: 5.609
batch start
#iterations: 236
currently lose_sum: 592.9874067902565
time_elpased: 5.752
batch start
#iterations: 237
currently lose_sum: 592.728962302208
time_elpased: 6.011
batch start
#iterations: 238
currently lose_sum: 593.3665313124657
time_elpased: 5.537
batch start
#iterations: 239
currently lose_sum: 591.2788633108139
time_elpased: 5.665
start validation test
0.550443213296
0.547717595212
0.946485540805
0.693890638852
0
validation finish
batch start
#iterations: 240
currently lose_sum: 594.3650782108307
time_elpased: 5.712
batch start
#iterations: 241
currently lose_sum: 591.0245115160942
time_elpased: 5.629
batch start
#iterations: 242
currently lose_sum: 593.7632859349251
time_elpased: 5.622
batch start
#iterations: 243
currently lose_sum: 593.3092430233955
time_elpased: 5.671
batch start
#iterations: 244
currently lose_sum: 594.3575894236565
time_elpased: 5.562
batch start
#iterations: 245
currently lose_sum: 593.0469037294388
time_elpased: 5.616
batch start
#iterations: 246
currently lose_sum: 591.8029792904854
time_elpased: 5.83
batch start
#iterations: 247
currently lose_sum: 593.0454558134079
time_elpased: 5.508
batch start
#iterations: 248
currently lose_sum: 592.3915526270866
time_elpased: 5.809
batch start
#iterations: 249
currently lose_sum: 594.4474326968193
time_elpased: 5.738
batch start
#iterations: 250
currently lose_sum: 592.6538313031197
time_elpased: 5.649
batch start
#iterations: 251
currently lose_sum: 592.1309641599655
time_elpased: 5.929
batch start
#iterations: 252
currently lose_sum: 593.1137878894806
time_elpased: 5.544
batch start
#iterations: 253
currently lose_sum: 592.4178523421288
time_elpased: 5.747
batch start
#iterations: 254
currently lose_sum: 592.0159667134285
time_elpased: 5.664
batch start
#iterations: 255
currently lose_sum: 592.3341217637062
time_elpased: 5.517
batch start
#iterations: 256
currently lose_sum: 591.4553625583649
time_elpased: 5.971
batch start
#iterations: 257
currently lose_sum: 594.7031906247139
time_elpased: 5.471
batch start
#iterations: 258
currently lose_sum: 592.4705192446709
time_elpased: 5.807
batch start
#iterations: 259
currently lose_sum: 591.5871397852898
time_elpased: 5.521
start validation test
0.549501385042
0.546735445836
0.954409797263
0.695215427575
0
validation finish
batch start
#iterations: 260
currently lose_sum: 592.0383661985397
time_elpased: 5.912
batch start
#iterations: 261
currently lose_sum: 593.5895488262177
time_elpased: 5.672
batch start
#iterations: 262
currently lose_sum: 592.3304206728935
time_elpased: 5.668
batch start
#iterations: 263
currently lose_sum: 591.1609542965889
time_elpased: 5.515
batch start
#iterations: 264
currently lose_sum: 592.9296920895576
time_elpased: 5.839
batch start
#iterations: 265
currently lose_sum: 594.2872011065483
time_elpased: 5.801
batch start
#iterations: 266
currently lose_sum: 591.7717980146408
time_elpased: 5.652
batch start
#iterations: 267
currently lose_sum: 594.134556889534
time_elpased: 5.764
batch start
#iterations: 268
currently lose_sum: 592.5077284574509
time_elpased: 5.718
batch start
#iterations: 269
currently lose_sum: 592.136413693428
time_elpased: 5.612
batch start
#iterations: 270
currently lose_sum: 592.873179256916
time_elpased: 5.461
batch start
#iterations: 271
currently lose_sum: 593.3208808898926
time_elpased: 5.789
batch start
#iterations: 272
currently lose_sum: 593.2794939279556
time_elpased: 5.546
batch start
#iterations: 273
currently lose_sum: 590.3142591118813
time_elpased: 5.739
batch start
#iterations: 274
currently lose_sum: 592.5036867260933
time_elpased: 5.769
batch start
#iterations: 275
currently lose_sum: 592.9348958134651
time_elpased: 5.685
batch start
#iterations: 276
currently lose_sum: 593.3054950237274
time_elpased: 5.822
batch start
#iterations: 277
currently lose_sum: 593.3110363483429
time_elpased: 5.765
batch start
#iterations: 278
currently lose_sum: 592.9568573832512
time_elpased: 5.547
batch start
#iterations: 279
currently lose_sum: 593.2991824150085
time_elpased: 5.922
start validation test
0.549529085873
0.547090261283
0.948132139549
0.693828369168
0
validation finish
batch start
#iterations: 280
currently lose_sum: 592.9460368156433
time_elpased: 5.737
batch start
#iterations: 281
currently lose_sum: 592.6936822533607
time_elpased: 5.689
batch start
#iterations: 282
currently lose_sum: 592.1351799368858
time_elpased: 5.402
batch start
#iterations: 283
currently lose_sum: 594.3045764565468
time_elpased: 5.691
batch start
#iterations: 284
currently lose_sum: 592.552586376667
time_elpased: 5.782
batch start
#iterations: 285
currently lose_sum: 592.0519528985023
time_elpased: 5.564
batch start
#iterations: 286
currently lose_sum: 591.9215965270996
time_elpased: 5.87
batch start
#iterations: 287
currently lose_sum: 593.1313907504082
time_elpased: 5.625
batch start
#iterations: 288
currently lose_sum: 591.4052084684372
time_elpased: 5.722
batch start
#iterations: 289
currently lose_sum: 591.8303052783012
time_elpased: 5.756
batch start
#iterations: 290
currently lose_sum: 590.917859852314
time_elpased: 5.491
batch start
#iterations: 291
currently lose_sum: 592.1027581691742
time_elpased: 5.599
batch start
#iterations: 292
currently lose_sum: 593.2554029226303
time_elpased: 5.709
batch start
#iterations: 293
currently lose_sum: 590.9803454875946
time_elpased: 5.766
batch start
#iterations: 294
currently lose_sum: 593.1178452968597
time_elpased: 5.816
batch start
#iterations: 295
currently lose_sum: 592.7690199017525
time_elpased: 5.746
batch start
#iterations: 296
currently lose_sum: 594.0227162837982
time_elpased: 5.82
batch start
#iterations: 297
currently lose_sum: 592.4259405732155
time_elpased: 5.407
batch start
#iterations: 298
currently lose_sum: 593.0147881507874
time_elpased: 5.806
batch start
#iterations: 299
currently lose_sum: 593.1255873441696
time_elpased: 5.877
start validation test
0.549667590028
0.547126709586
0.948955438921
0.694078018855
0
validation finish
batch start
#iterations: 300
currently lose_sum: 592.4087693691254
time_elpased: 5.605
batch start
#iterations: 301
currently lose_sum: 592.4132954478264
time_elpased: 5.549
batch start
#iterations: 302
currently lose_sum: 591.835534632206
time_elpased: 5.642
batch start
#iterations: 303
currently lose_sum: 592.7072975635529
time_elpased: 5.844
batch start
#iterations: 304
currently lose_sum: 592.5330250263214
time_elpased: 5.578
batch start
#iterations: 305
currently lose_sum: 592.9879004955292
time_elpased: 5.792
batch start
#iterations: 306
currently lose_sum: 592.0615711212158
time_elpased: 5.794
batch start
#iterations: 307
currently lose_sum: 593.2602430582047
time_elpased: 5.7
batch start
#iterations: 308
currently lose_sum: 592.1761009693146
time_elpased: 5.842
batch start
#iterations: 309
currently lose_sum: 593.8782430291176
time_elpased: 5.814
batch start
#iterations: 310
currently lose_sum: 592.4536329507828
time_elpased: 5.8
batch start
#iterations: 311
currently lose_sum: 592.131453871727
time_elpased: 5.742
batch start
#iterations: 312
currently lose_sum: 591.7935736775398
time_elpased: 5.92
batch start
#iterations: 313
currently lose_sum: 592.3576157093048
time_elpased: 5.372
batch start
#iterations: 314
currently lose_sum: 593.2134700417519
time_elpased: 5.822
batch start
#iterations: 315
currently lose_sum: 591.6742531061172
time_elpased: 5.782
batch start
#iterations: 316
currently lose_sum: 591.5924724936485
time_elpased: 5.601
batch start
#iterations: 317
currently lose_sum: 591.2153378725052
time_elpased: 5.804
batch start
#iterations: 318
currently lose_sum: 592.2624659538269
time_elpased: 5.502
batch start
#iterations: 319
currently lose_sum: 591.7419021129608
time_elpased: 5.752
start validation test
0.550886426593
0.547875977288
0.948337964392
0.694515101841
0
validation finish
batch start
#iterations: 320
currently lose_sum: 592.7915539741516
time_elpased: 5.474
batch start
#iterations: 321
currently lose_sum: 593.924994289875
time_elpased: 5.779
batch start
#iterations: 322
currently lose_sum: 590.6082832217216
time_elpased: 5.681
batch start
#iterations: 323
currently lose_sum: 592.5971979498863
time_elpased: 5.578
batch start
#iterations: 324
currently lose_sum: 593.4515978097916
time_elpased: 5.696
batch start
#iterations: 325
currently lose_sum: 594.4620506763458
time_elpased: 5.639
batch start
#iterations: 326
currently lose_sum: 591.865231692791
time_elpased: 5.904
batch start
#iterations: 327
currently lose_sum: 593.6725996732712
time_elpased: 5.678
batch start
#iterations: 328
currently lose_sum: 592.6893753409386
time_elpased: 5.708
batch start
#iterations: 329
currently lose_sum: 591.818773150444
time_elpased: 5.562
batch start
#iterations: 330
currently lose_sum: 590.8746939897537
time_elpased: 5.832
batch start
#iterations: 331
currently lose_sum: 591.9238878488541
time_elpased: 5.707
batch start
#iterations: 332
currently lose_sum: 593.4352611899376
time_elpased: 5.704
batch start
#iterations: 333
currently lose_sum: 591.3679589033127
time_elpased: 5.742
batch start
#iterations: 334
currently lose_sum: 593.0090920329094
time_elpased: 5.793
batch start
#iterations: 335
currently lose_sum: 592.6015160083771
time_elpased: 5.643
batch start
#iterations: 336
currently lose_sum: 591.9023920297623
time_elpased: 5.883
batch start
#iterations: 337
currently lose_sum: 591.313043653965
time_elpased: 5.746
batch start
#iterations: 338
currently lose_sum: 592.6310963630676
time_elpased: 5.785
batch start
#iterations: 339
currently lose_sum: 592.8970718979836
time_elpased: 5.881
start validation test
0.549750692521
0.547372184483
0.945353504168
0.693309181479
0
validation finish
batch start
#iterations: 340
currently lose_sum: 591.7846888899803
time_elpased: 5.676
batch start
#iterations: 341
currently lose_sum: 592.1690135002136
time_elpased: 5.81
batch start
#iterations: 342
currently lose_sum: 594.2677268981934
time_elpased: 5.616
batch start
#iterations: 343
currently lose_sum: 593.3909960389137
time_elpased: 5.544
batch start
#iterations: 344
currently lose_sum: 592.7963365912437
time_elpased: 6.03
batch start
#iterations: 345
currently lose_sum: 592.1207592487335
time_elpased: 5.736
batch start
#iterations: 346
currently lose_sum: 591.9727864265442
time_elpased: 5.794
batch start
#iterations: 347
currently lose_sum: 592.2393397688866
time_elpased: 5.706
batch start
#iterations: 348
currently lose_sum: 592.8781327605247
time_elpased: 5.568
batch start
#iterations: 349
currently lose_sum: 592.8108758330345
time_elpased: 5.795
batch start
#iterations: 350
currently lose_sum: 592.7289574742317
time_elpased: 5.446
batch start
#iterations: 351
currently lose_sum: 593.2474163770676
time_elpased: 5.88
batch start
#iterations: 352
currently lose_sum: 592.8784236907959
time_elpased: 5.717
batch start
#iterations: 353
currently lose_sum: 592.4494584202766
time_elpased: 5.546
batch start
#iterations: 354
currently lose_sum: 591.5559521913528
time_elpased: 5.66
batch start
#iterations: 355
currently lose_sum: 592.1724390387535
time_elpased: 5.672
batch start
#iterations: 356
currently lose_sum: 592.2800678610802
time_elpased: 5.61
batch start
#iterations: 357
currently lose_sum: 593.8947221040726
time_elpased: 5.537
batch start
#iterations: 358
currently lose_sum: 592.5193566083908
time_elpased: 5.692
batch start
#iterations: 359
currently lose_sum: 593.5309431552887
time_elpased: 5.561
start validation test
0.549833795014
0.547353387485
0.946588453226
0.693625926135
0
validation finish
batch start
#iterations: 360
currently lose_sum: 591.4983878731728
time_elpased: 5.527
batch start
#iterations: 361
currently lose_sum: 591.9259223341942
time_elpased: 5.686
batch start
#iterations: 362
currently lose_sum: 592.6854940652847
time_elpased: 5.811
batch start
#iterations: 363
currently lose_sum: 592.6950173974037
time_elpased: 5.816
batch start
#iterations: 364
currently lose_sum: 590.1448011398315
time_elpased: 5.636
batch start
#iterations: 365
currently lose_sum: 593.5247264504433
time_elpased: 5.962
batch start
#iterations: 366
currently lose_sum: 592.1081201434135
time_elpased: 5.668
batch start
#iterations: 367
currently lose_sum: 592.0071379542351
time_elpased: 5.805
batch start
#iterations: 368
currently lose_sum: 593.3755540251732
time_elpased: 5.799
batch start
#iterations: 369
currently lose_sum: 593.9919630885124
time_elpased: 5.679
batch start
#iterations: 370
currently lose_sum: 593.3281879425049
time_elpased: 5.651
batch start
#iterations: 371
currently lose_sum: 592.3016473054886
time_elpased: 5.517
batch start
#iterations: 372
currently lose_sum: 592.1529626846313
time_elpased: 5.712
batch start
#iterations: 373
currently lose_sum: 590.7433365881443
time_elpased: 5.581
batch start
#iterations: 374
currently lose_sum: 591.6118795275688
time_elpased: 5.676
batch start
#iterations: 375
currently lose_sum: 592.8868972659111
time_elpased: 5.473
batch start
#iterations: 376
currently lose_sum: 591.0562320351601
time_elpased: 5.592
batch start
#iterations: 377
currently lose_sum: 592.6202651262283
time_elpased: 5.57
batch start
#iterations: 378
currently lose_sum: 590.8953747153282
time_elpased: 5.724
batch start
#iterations: 379
currently lose_sum: 592.2024484872818
time_elpased: 5.529
start validation test
0.550360110803
0.547527162619
0.949058351343
0.694427710843
0
validation finish
batch start
#iterations: 380
currently lose_sum: 591.1816087961197
time_elpased: 5.764
batch start
#iterations: 381
currently lose_sum: 590.2895174622536
time_elpased: 5.645
batch start
#iterations: 382
currently lose_sum: 591.2140089869499
time_elpased: 5.647
batch start
#iterations: 383
currently lose_sum: 592.0313486456871
time_elpased: 5.685
batch start
#iterations: 384
currently lose_sum: 593.0707753300667
time_elpased: 5.585
batch start
#iterations: 385
currently lose_sum: 591.7395572662354
time_elpased: 5.878
batch start
#iterations: 386
currently lose_sum: 592.5723344087601
time_elpased: 5.595
batch start
#iterations: 387
currently lose_sum: 593.5862439870834
time_elpased: 5.893
batch start
#iterations: 388
currently lose_sum: 592.403721511364
time_elpased: 5.696
batch start
#iterations: 389
currently lose_sum: 593.101125895977
time_elpased: 5.686
batch start
#iterations: 390
currently lose_sum: 590.7625947594643
time_elpased: 5.795
batch start
#iterations: 391
currently lose_sum: 591.2990783452988
time_elpased: 5.806
batch start
#iterations: 392
currently lose_sum: 595.0023812055588
time_elpased: 5.939
batch start
#iterations: 393
currently lose_sum: 592.5393195152283
time_elpased: 6.022
batch start
#iterations: 394
currently lose_sum: 593.4634827375412
time_elpased: 5.725
batch start
#iterations: 395
currently lose_sum: 591.7579324245453
time_elpased: 5.606
batch start
#iterations: 396
currently lose_sum: 592.9524558186531
time_elpased: 5.53
batch start
#iterations: 397
currently lose_sum: 593.2149743437767
time_elpased: 5.692
batch start
#iterations: 398
currently lose_sum: 591.9282252788544
time_elpased: 5.817
batch start
#iterations: 399
currently lose_sum: 592.4211276173592
time_elpased: 5.524
start validation test
0.55027700831
0.547699281305
0.945044766903
0.693488398437
0
validation finish
acc: 0.547
pre: 0.544
rec: 0.961
F1: 0.695
auc: 0.000
