#!/bin/tcsh
foreach folds (0 1 2 3 4)
	foreach step (train validation test)
		less PN_$step\_$folds.txt | awk '{print $2"\t"$1"\t"$3}' > PN_$step\_reorder$folds.txt
		cat PN_$step\_$folds.txt PN_$step\_reorder$folds.txt > entire_PN_$step\_$folds.txt
		less entire_PN_$step\_$folds.txt | awk '{print$1;print$2}' | sort | uniq -c | sort -k2n > freq_PN$step$folds.txt
		set num_sample = `less PN_$step\_$folds.txt | wc | awk '{print $1}'`
		python negsamp_tf.py --num_sample $num_sample --train_file PN_$step\_$folds.txt --freq_file freq_PN$step$folds.txt --outfile PN_$step\_N_set$folds.txt --remove 1
		cat entire_PN_$step\_$folds.txt PN_$step\_N_set$folds.txt > PNN_entire_$step$folds.txt
	end

	set num_test = `less PNN_entire_test$folds.txt | wc | awk '{print $1}'`
	set num_val = `less PNN_entire_validation$folds.txt | wc | awk '{print $1}'`
	set num_train = `less PNN_entire_train$folds.txt | wc | awk '{print $1}'`

	foreach sim_file (chem_Jacarrd_sim.csv enzyme_Jacarrd_sim.csv indication_Jacarrd_sim.csv offsideeffect_Jacarrd_sim.csv pathway_Jacarrd_sim.csv target_Jacarrd_sim.csv transporter_Jacarrd_sim.csv)
		foreach L (8 16 32 64)
			python frame_work_test.py --attention $1 --train PNN_entire_train$folds.txt --test PNN_entire_test$folds.txt --validation PNN_entire_validation$folds.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_file $sim_file --L_dim $L > ./Jure_sample/log_$sim_file\_1N_attention_$1_L_$L\_feature_50_1_$folds.txt
		end
	end
end
