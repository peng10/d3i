import tensorflow as tf
import numpy as np
import GraphConv, Graph_feature
import pdb, math, random, re, os, sys, time, argparse
import sklearn.metrics
import scipy.io as sio
from sklearn.decomposition import TruncatedSVD

np.random.seed(234)
tf.set_random_seed(234)
random.seed(234)

path_adj = '../../../data/sdf_1180/wen_data_order/'
path_feature = '../../../data/sdf_1180/wen_data_feature/'
path_degree = '../../../data/sdf_1180/wen_data_degree/'
path_data = '../../'
path_sim = '../'

#the graph feature size stands for input feature size
parser = argparse.ArgumentParser()
parser.add_argument('--train', default = 'PN2N_train0.txt', type = str)
parser.add_argument('--test', default = 'PN_test_0.txt', type = str)
parser.add_argument('--validation', default = 'PN_validation_0.txt', type = str)
parser.add_argument('--num_train_sample', default = 87450, type = int)
parser.add_argument('--num_validation_sample', default = 9717, type = int)
parser.add_argument('--num_test_sample', default = 9717, type = int)
parser.add_argument('--feature_size', default = 20, type = int)
parser.add_argument('--graph_feature_size', default = 50, type = int)
parser.add_argument('--sim_dim', default = 548, type = int)
parser.add_argument('--L_dim', default = 32, type = int)
parser.add_argument('--batch_size', default = 100, type = int)
parser.add_argument('--max_degree', default = 8, type = int)
parser.add_argument('--min_degree', default = 0, type = int)
parser.add_argument('--pool_method', default = 'max_pool', type = str)
parser.add_argument('--num_drugs', default = 548, type = int)
parser.add_argument('--num_layers', default = 1, type = int)
parser.add_argument('--keep_probe', default = 1.0, type = float)
parser.add_argument('--option', default = 0, type = int)
parser.add_argument('--Graph', default = 0, type = int)
parser.add_argument('--activity', default = 0, type = int)
parser.add_argument('--order2', default = 0, type = int)
parser.add_argument('--NN', default = 0, type = int)
parser.add_argument('--attention', default = 0, type = int)
parser.add_argument('--bootstrap', default = 0,type = int)
parser.add_argument('--chem_sim', default = 1,type = int)
parser.add_argument('--enzyme_sim', default = 0,type = int)
parser.add_argument('--indication_sim', default = 0,type = int)
parser.add_argument('--offsideeffect_sim', default = 0,type = int)
parser.add_argument('--pathway_sim', default = 0,type = int)
parser.add_argument('--sideeffect_sim', default = 0,type = int)
parser.add_argument('--target_sim', default = 0,type = int)
parser.add_argument('--transporter_sim', default = 0,type = int)
parser.add_argument('--sim_file', default = 'chem_Jacarrd_sim.csv',type = str)
parser.add_argument('--all_sim', default = 0,type = int)
parser.add_argument('--sim_attention', default = 0,type = int)

args = parser.parse_args()

file_train = './' + args.train
file_validation = './' + args.validation
file_test = './' + args.test

graph_adj = {}
graph_feature = {}
graph_degree = {}

all_sim = args.all_sim
degree_region = args.max_degree - args.min_degree + 1
degree_max = args.max_degree
degree_min = args.min_degree
feature_size = args.feature_size
graph_feature_size = args.graph_feature_size
num_layers = args.num_layers
keep_probe = args.keep_probe
batch_size = args.batch_size
option = args.option
activity = args.activity
num_train_sample = args.num_train_sample
order2 = args.order2
L_dim = args.L_dim
sim_dim = args.sim_dim
attention = args.attention
Graph = args.Graph
chem_sim = args.chem_sim
NN = args.NN

if(args.Graph == 1):
	#load data
	for filename in os.listdir(path_adj):
		graph_adj[filename] = np.loadtxt(path_adj + filename, dtype = np.float32, delimiter = ',', ndmin = 2)

	for filename in os.listdir(path_feature):
		graph_feature[filename] = np.loadtxt(path_feature + filename, dtype = np.float32, delimiter = ',', ndmin = 2)

	for filename in os.listdir(path_degree):
		graph_degree[filename] = np.loadtxt(path_degree + filename, dtype = np.float32, delimiter = ',', ndmin = 2)

id_matrixID_dict = {}
with open(path_data + 'id_matrixId.txt', 'r') as f:
	for line in f:
		Id, matrixId = line.rstrip('\n').split('\t')
		id_matrixID_dict[Id] = int(matrixId)

#dimension reduction
matrix_activity = sio.loadmat(path_data + 'activity_outcome_matrix_012.mat')
svd = TruncatedSVD(n_components = graph_feature_size) 
activity_matrix = svd.fit_transform(matrix_activity['H'].T)
#pdb.set_trace()

#load the chemsim matrix
chemsim_matrix = np.loadtxt(path_sim + args.sim_file, delimiter = ',', skiprows = 1, usecols=range(1, args.num_drugs + 1))

#placeholder
label = tf.placeholder(tf.float32)
keep_probe_tf = tf.placeholder(tf.float32)
if(args.Graph == 1):
	X_1_adj = [tf.placeholder(tf.float32) for _ in range(batch_size)]
	X_1_feature = [tf.placeholder(tf.float32, [None, feature_size]) for _ in range(batch_size)]
	X_1_degree_matrix = [tf.placeholder(tf.float32, [degree_region, None]) for _ in range(batch_size)]
	X_2_adj = [tf.placeholder(tf.float32) for _ in range(batch_size)]
	X_2_feature = [tf.placeholder(tf.float32, [None, feature_size]) for _ in range(batch_size)]
	X_2_degree_matrix = [tf.placeholder(tf.float32, [degree_region, None]) for _ in range(batch_size)]
else:
	#make small fake placeholder for saving memory
	X_1_adj = tf.placeholder(tf.float32)
	X_1_feature = tf.placeholder(tf.float32)
	X_1_degree_matrix = tf.placeholder(tf.float32)
	X_2_adj = tf.placeholder(tf.float32)
	X_2_feature = tf.placeholder(tf.float32)
	X_2_degree_matrix = tf.placeholder(tf.float32)

chem_sim1 = tf.placeholder(tf.float32, [None, args.sim_dim])
chem_sim2 = tf.placeholder(tf.float32, [None, args.sim_dim])
activity_1 = tf.placeholder(tf.float32, [None, graph_feature_size])
activity_2 = tf.placeholder(tf.float32, [None, graph_feature_size])

#8: number of type of similarities
sim_list1 = tf.placeholder(tf.float32, [8, None, args.sim_dim])
sim_list2 = tf.placeholder(tf.float32, [8, None, args.sim_dim])

#read the rest 7 sim matrix
enzymesim_matrix = np.loadtxt(path_sim + 'enzyme_Jacarrd_sim.csv', delimiter = ',', skiprows = 1, usecols=range(1, args.num_drugs + 1))

indicationsim_matrix = np.loadtxt(path_sim + 'indication_Jacarrd_sim.csv', delimiter = ',', skiprows = 1, usecols=range(1, args.num_drugs + 1))

offsideeffectsim_matrix = np.loadtxt(path_sim + 'offsideeffect_Jacarrd_sim.csv', delimiter = ',', skiprows = 1, usecols=range(1, args.num_drugs + 1))

pathwaysim_matrix = np.loadtxt(path_sim + 'pathway_Jacarrd_sim.csv', delimiter = ',', skiprows = 1, usecols=range(1, args.num_drugs + 1))

sideeffectsim_matrix = np.loadtxt(path_sim + 'sideeffect_Jacarrd_sim.csv', delimiter = ',', skiprows = 1, usecols=range(1, args.num_drugs + 1))

targetsim_matrix = np.loadtxt(path_sim + 'target_Jacarrd_sim.csv', delimiter = ',', skiprows = 1, usecols=range(1, args.num_drugs + 1))

transportersim_matrix = np.loadtxt(path_sim + 'transporter_Jacarrd_sim.csv', delimiter = ',', skiprows = 1, usecols=range(1, args.num_drugs + 1))

#initialize weight
weight_self_list = tf.Variable(tf.random_uniform([degree_region, feature_size, feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + feature_size) ))

weight_neig_list = tf.Variable(tf.random_uniform([degree_region, feature_size, feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + feature_size) ))

b_matrix = tf.Variable(tf.random_uniform([degree_region, feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + feature_size) ))

supernode_feature = tf.Variable(tf.random_uniform([1, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

weight_self_Gathering = tf.Variable(tf.random_uniform([graph_feature_size, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

weight_neig_Gathering = tf.Variable(tf.random_uniform([feature_size, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(feature_size + graph_feature_size) ))

b_Gathering = tf.Variable(tf.random_uniform([graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

R_matrix = tf.get_variable('R_matrix', shape = [graph_feature_size, graph_feature_size], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

W_transsim = tf.get_variable( 'W_transsim', shape = [sim_dim, graph_feature_size], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(sim_dim + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(sim_dim + graph_feature_size) ) )

W_layers = tf.get_variable( 'W_layers', shape = [2 * graph_feature_size, graph_feature_size], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ) )

W_layers_hidden = tf.Variable(tf.random_uniform([num_layers - 1, graph_feature_size, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(2 * graph_feature_size + 2 * graph_feature_size), maxval = np.sqrt(6)/np.sqrt(2 * graph_feature_size + 2 * graph_feature_size) ))

b_layers = tf.Variable(tf.random_uniform([graph_feature_size], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))

att_V = tf.get_variable( 'att_V', shape = [L_dim, graph_feature_size], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(L_dim + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(L_dim + graph_feature_size) ) )

att_W = tf.get_variable( 'att_W', shape = [1, L_dim], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(L_dim), maxval = np.sqrt(6)/np.sqrt(L_dim) ) )

b_layers_hidden = tf.Variable(tf.random_uniform([num_layers - 1, graph_feature_size], minval = -np.sqrt(6)/np.sqrt(2 * graph_feature_size + 2 * graph_feature_size), maxval = np.sqrt(6)/np.sqrt(2 * graph_feature_size + 2 * graph_feature_size) ))

class_weight = tf.get_variable('class_weight', shape = [graph_feature_size, 1], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(graph_feature_size + 1), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + 1) ), regularizer = tf.contrib.layers.l2_regularizer(0.2) )

class_b = tf.Variable(tf.random_uniform([1], minval = -np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(graph_feature_size + graph_feature_size) ))
gamma = tf.Variable(0.90)
beta = tf.Variable(0.10)
eps = 0.001
#attention for all similarities
simatt_V = tf.get_variable( 'simatt_V', shape = [L_dim, graph_feature_size], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(L_dim + graph_feature_size), maxval = np.sqrt(6)/np.sqrt(L_dim + graph_feature_size) ) )

simatt_W = tf.get_variable( 'simatt_W', shape = [1, L_dim], initializer = tf.random_uniform_initializer( minval = -np.sqrt(6)/np.sqrt(L_dim), maxval = np.sqrt(6)/np.sqrt(L_dim) ) )

print 'start to construct graph'

#construct graph
if(args.Graph == 1):
	for i in xrange(batch_size):

		if(i == 0):	
			Graph_matrix_1 = []
			Graph_matrix_2 = []
		Graph_feature_1 = Graph_feature.Graph_feature(X_1_adj[i], X_1_feature[i], X_1_degree_matrix[i], weight_self_list, weight_neig_list, b_matrix, tf.nn.tanh, degree_max, degree_min, feature_size, graph_feature_size, weight_self_Gathering, weight_neig_Gathering, b_Gathering, supernode_feature, gamma, beta, eps, keep_probe)
	
		Graph_feature_2 = Graph_feature.Graph_feature(X_2_adj[i], X_2_feature[i], X_2_degree_matrix[i], weight_self_list, weight_neig_list, b_matrix, tf.nn.tanh, degree_max, degree_min, feature_size, graph_feature_size, weight_self_Gathering, weight_neig_Gathering, b_Gathering, supernode_feature, gamma, beta, eps, keep_probe)
	
		Graph_matrix_1.append(Graph_feature_1)
		Graph_matrix_2.append(Graph_feature_2)
	Graph_matrix_1 = tf.reshape(Graph_matrix_1, [batch_size, graph_feature_size])
	Graph_matrix_2 = tf.reshape(Graph_matrix_2, [batch_size, graph_feature_size])
else:
	if(all_sim == 0):
		Graph_matrix_1, Graph_matrix_2 = GraphConv.mixfeature(chem_sim1,chem_sim2,W_transsim)
	else:
		Graph_matrix_1, Graph_matrix_2 = GraphConv.mixfeature_all(sim_list1, sim_list2, 
					W_transsim, args.sim_attention, simatt_V,simatt_W, batch_size)
	
combined_matrix_1, combined_matrix_2, feature1, feature2 = GraphConv.Combine_Multi_Graph(Graph_matrix_1, Graph_matrix_2, activity_1, activity_2, activity)

layer = GraphConv.NN_relationship_encoder(combined_matrix_1, combined_matrix_2, NN, W_layers, b_layers, W_layers_hidden, b_layers_hidden, keep_probe_tf, num_layers, feature1, feature2, attention, att_V, att_W, batch_size, graph_feature_size)

hypothesis = GraphConv.Classifier(layer, feature1, feature2, class_weight, class_b, option, R_matrix, order2)

hypothesis = tf.reshape(hypothesis,[batch_size])

if(order2 == 1):
	l1_regularizer = tf.contrib.layers.l1_regularizer(scale=0.005, scope=None)
	weights = [R_matrix]
	regularization_penalty = tf.contrib.layers.apply_regularization(l1_regularizer, weights)
	cost = -tf.reduce_mean(label * tf.log(hypothesis) + (1 - label) * tf.log(1 - hypothesis)) + regularization_penalty
else:
	cost = -tf.reduce_mean(label * tf.log(hypothesis) + (1 - label) * tf.log(1 - hypothesis))
#evaluation
predicted = tf.cast(hypothesis > 0.5 , dtype = tf.int32)

#train setup
global_step = tf.Variable(0, trainable=False)
starter_learning_rate = 1e-3
learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step, 5000, 0.96, staircase=True)
train_op = tf.train.AdamOptimizer(learning_rate, epsilon = 1e-4).minimize(cost, global_step = global_step)

session_conf = tf.ConfigProto(
      intra_op_parallelism_threads=25,
      inter_op_parallelism_threads=25)

sess = tf.Session(config=session_conf)
sess.run(tf.global_variables_initializer())

with open(file_train, 'r') as f:
	total_lines = f.read().splitlines() 

print 'graph construct over'
print len(total_lines)

minimum = 1000
last_sum_lose = 0
maximum_F1 = 0
break_marker = 0

print 'epochs start'
for epochs in range(400):

	sum_lose = 0
	start_time = time.time()
	random.shuffle(total_lines)
	if(epochs+1 % 40 == 0):
		learning_rate = 0.8 * learning_rate
	learning_rate = tf.maximum(1e-4, learning_rate)

	print 'batch start'
	for i in range(args.num_train_sample/batch_size):
		feed_dict = {}
		label_list = []
		ind_list_1 = []
		ind_list_2 = []
		for j in range(batch_size):
			rint = np.random.randint(0, args.num_train_sample)
			ind_1, ind_2, label_d = (e for e in total_lines[rint].rstrip('\n').rstrip('\t').strip(' ').split('\t'))
			ind_list_1.append(ind_1)
			ind_list_2.append(ind_2)
			label_list.append(int(label_d))
		#update feed_dict
		feed_dict = GraphConv.update_feed_dict(graph_adj, graph_feature, graph_degree, activity_matrix, id_matrixID_dict, ind_list_1, ind_list_2, X_1_adj, X_1_feature, X_1_degree_matrix, X_2_adj, X_2_feature, X_2_degree_matrix, label, activity_1, activity_2, label_list, feed_dict, keep_probe_tf, keep_probe, Graph, chem_sim, activity, chem_sim1, chem_sim2, chemsim_matrix, enzymesim_matrix, indicationsim_matrix, offsideeffectsim_matrix, pathwaysim_matrix, sideeffectsim_matrix, targetsim_matrix, transportersim_matrix, sim_list1, sim_list2, all_sim)
		#pdb.set_trace()
	 	cos, _ = sess.run([cost, train_op], feed_dict)
		sum_lose += cos
	
	end_time = time.time()

	print '#iterations: ' + str(epochs)
	print 'currently lose_sum: ' + str(sum_lose)
	print 'time_elpased: ' + str(round(end_time - start_time, 3))
	
	if((epochs + 1)%20 == 0):
		print 'start validation test'
		with open(file_validation, 'r') as f:
        		validation_lines = f.read().splitlines()

		predicted_list = []
		label_list = []
		for i_v in range(args.num_validation_sample/batch_size):
        		feed_dict_validation = {}
			ind_list_1 = []
			ind_list_2 = []
        		for j_v in range(batch_size):
                		ind_1, ind_2, label_d = (e for e in validation_lines[i_v * batch_size + j_v].rstrip('\n').rstrip('\t').rstrip(' ').split('\t'))
				ind_list_1.append(ind_1)
				ind_list_2.append(ind_2)
                		label_list.append(int(label_d))
			feed_dict_validation = GraphConv.update_feed_dict(graph_adj, graph_feature, graph_degree, activity_matrix, id_matrixID_dict, ind_list_1, ind_list_2, X_1_adj, X_1_feature, X_1_degree_matrix, X_2_adj, X_2_feature, X_2_degree_matrix, label, activity_1, activity_2, label_list, feed_dict_validation, keep_probe_tf, 1.0, Graph, chem_sim, activity, chem_sim1, chem_sim2, chemsim_matrix, enzymesim_matrix, indicationsim_matrix, offsideeffectsim_matrix, pathwaysim_matrix, sideeffectsim_matrix, targetsim_matrix, transportersim_matrix, sim_list1, sim_list2, all_sim)
        		temp = sess.run(predicted, feed_dict_validation).tolist()
        		predicted_list += temp
		#pdb.set_trace()
		acc, pre, rec, F1, auc = GraphConv.evaluation(label_list, predicted_list)
		print acc
		print pre
		print rec
		print F1
		print auc
		print 'validation finish'

                if(maximum_F1 < F1):
                        Variable_list = sess.run(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))
                        maximum_F1 = F1
                elif(math.isnan(F1)):
			break_marker = 1
			break

	if(sum_lose <= 1 and abs((last_sum_lose - sum_lose) <= 0.01) or math.isnan(sum_lose) or break_marker == 1):
		print 'train finish final lose is: ' + str(sum_lose)
		break
	else:
		last_sum_lose = sum_lose

#retract variables
sess.run(weight_self_list.assign(Variable_list[0]))
sess.run(weight_neig_list.assign(Variable_list[1]))
sess.run(b_matrix.assign(Variable_list[2]))
sess.run(supernode_feature.assign(Variable_list[3]))
sess.run(weight_self_Gathering.assign(Variable_list[4]))
sess.run(weight_neig_Gathering.assign(Variable_list[5]))
sess.run(b_Gathering.assign(Variable_list[6]))
sess.run(R_matrix.assign(Variable_list[7]))
sess.run(W_transsim.assign(Variable_list[8]))
sess.run(W_layers.assign(Variable_list[9]))
sess.run(W_layers_hidden.assign(Variable_list[10]))
sess.run(b_layers.assign(Variable_list[11]))
sess.run(att_V.assign(Variable_list[12]))
sess.run(att_W.assign(Variable_list[13]))
sess.run(b_layers_hidden.assign(Variable_list[14]))
sess.run(class_weight.assign(Variable_list[15]))
sess.run(class_b.assign(Variable_list[16]))
sess.run(gamma.assign(Variable_list[17]))
sess.run(beta.assign(Variable_list[18]))
sess.run(simatt_V.assign(Variable_list[19]))
sess.run(simatt_W.assign(Variable_list[20]))

predicted = tf.cast(hypothesis > 0.5 , dtype = tf.int32)
predicted_list = []
label_list = []

with open(file_test, 'r') as f:
	test_lines = f.read().splitlines()

for i in range(args.num_test_sample/batch_size):
	ind_list_1 = []
	ind_list_2 = []
	feed_dict_test = {}
	for j in range(batch_size):
		ind_1, ind_2, label_d = (e for e in test_lines[i * batch_size + j].rstrip('\n').rstrip('\t').rstrip(' ').split('\t'))
		ind_list_1.append(ind_1)
		ind_list_2.append(ind_2)
		label_list.append(int(label_d))

	feed_dict_test = GraphConv.update_feed_dict(graph_adj, graph_feature, graph_degree, activity_matrix, id_matrixID_dict, ind_list_1, ind_list_2, X_1_adj, X_1_feature, X_1_degree_matrix, X_2_adj, X_2_feature, X_2_degree_matrix, label, activity_1, activity_2, label_list, feed_dict_test, keep_probe_tf, 1.0, Graph, chem_sim, activity, chem_sim1, chem_sim2, chemsim_matrix, enzymesim_matrix, indicationsim_matrix, offsideeffectsim_matrix, pathwaysim_matrix, sideeffectsim_matrix, targetsim_matrix, transportersim_matrix, sim_list1, sim_list2, all_sim)
	temp = sess.run(predicted, feed_dict_test).tolist()
	predicted_list += temp

acc, pre, rec, F1, auc = GraphConv.evaluation(label_list, predicted_list)
print ('acc: %.3f' % acc)
print ('pre: %.3f' % pre)
print ('rec: %.3f' % rec)
print ('F1: %.3f' % F1)
print ('auc: %.3f' % auc)
with open('prediction_value_' + args.sim_file + '_' + args.test + '_' + str(args.L_dim), 'w') as f:
	for e in predicted_list:
		f.write("%d\n" % e)
'''
with open('./debug/' + 'relation_embedding', 'w') as f_debug:
	np.savetxt('./debug/' + 'relation_embedding', W_layers, fmt = '%.6f', delimiter = ',')

test_X_adj = tf.placeholder(tf.float32)
test_X_feature = tf.placeholder(tf.float32)
test_X_deg = tf.placeholder(tf.float32)

Graph_feature = Graph_feature.Graph_feature(test_X_adj, test_X_feature, test_X_deg, weight_self_list, weight_neig_list, b_matrix, tf.nn.tanh, degree_max, degree_min, feature_size, graph_feature_size, weight_self_Gathering, weight_neig_Gathering, b_Gathering, supernode_feature, gamma, beta, eps)

for keys in graph_adj:
'''	
