import scipy.io as sio
import numpy as np
import random
import argparse
import pdb
import tensorflow as tf
import copy

path = './'
random.seed(123)
np.random.seed(123)
tf.set_random_seed(123)

parser = argparse.ArgumentParser()
parser.add_argument('--num_sample', default = 29150, type = int)
parser.add_argument('--num_drugs', default = 548, type = int)
parser.add_argument('--train_file', type = str)
parser.add_argument('--freq_file',  type = str)
parser.add_argument('--outfile',  type = str)
parser.add_argument('--remove',  default = 0, type = int)

args = parser.parse_args()

with open(path + args.freq_file, 'r') as f:
        pre = f.read().splitlines()

pre_t = [int(e.lstrip(' ').split(' ')[0]) for e in pre]
#temp = sum(pre_t)
#preb = [(float(e)/temp) for e in pre_t]
#print len(pre_t)

drugId = [int(e.lstrip(' ').split(' ')[1]) for e in pre]
ID_drugId = {}
for i in range(len(drugId)):
	ID_drugId[i] = drugId[i]
#pdb.set_trace()

positive_list = []
positive_triplets = []
with open(args.train_file, 'r') as f:
	for line in f:
		temp = line.rstrip('\n').split('\t')
		positive_list.append(int(temp[1]))
		positive_triplets.append((int(temp[0]), int(temp[1])))
		positive_triplets.append((int(temp[1]), int(temp[0])))
	f.seek(0)
	for line in f:
                temp = line.rstrip('\n').split('\t')
                positive_list.append(int(temp[0]))
#data = np.array(positive_triplets, dtype=int)
#result = np.zeros(shape=(args.num_drugs, args.num_drugs), dtype=int)
#result[data[:,1], data[:,0]] = 1
#result[data[:,0], data[:,1]] = 1

sample_graph = tf.Graph()
initializer = tf.global_variables_initializer()
with sample_graph.as_default():
	neg_samples, _, _ = tf.nn.fixed_unigram_candidate_sampler(
		true_classes = np.ones((2 * args.num_sample, 1), dtype = int),
		num_true = 1,
		num_sampled = 2 * args.num_sample,
		unique = False,
		range_max = len(pre_t),
		distortion = 0.75,
		unigrams = pre_t)

sample_sess = tf.Session(graph = sample_graph)
#sample_sess.run(initializer)
neg_samples_array = sample_sess.run(neg_samples)
neg_samples_array = [drugId[e] for e in neg_samples_array]
#pdb.set_trace()
#always replace the first item
candidate = zip(neg_samples_array, positive_list)
if(args.remove == 1):
	for i in xrange(len(candidate)):
		if(candidate[i] in positive_triplets):
			candidate[i] = 0

f = open(path + args.outfile,'w')
for e in candidate:
	#print e
	if(e != 0):
		f.write('%d\t%d\t0\n' % (e[0], e[1]))

f.close()
