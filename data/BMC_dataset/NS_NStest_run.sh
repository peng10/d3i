#!/bin/tcsh
foreach folds (0)
	less PN_train_$folds.txt | awk '{print $1;print $2}' | sort | uniq -c | sort -k2n > freq_PNtrain$folds.txt
	set num_sample = `less PN_train_$folds.txt | wc | awk '{print $1}'`
	python negsamp.py --num_sample $num_sample --train_file PN_train_$folds.txt --freq_file freq_PNtrain$folds.txt --outfile PN_N_set$folds.txt
	cat PN_train_$folds.txt PN_N_set$folds.txt > PNN_train$folds.txt
	cat PN_train_$folds.txt PN_N_set$folds.txt PN_N_set$folds.txt > PN2N_train$folds.txt

	set num_test = `less PN_test_$folds.txt | wc | awk '{print $1}'`
	set num_val = `less PN_validation_$folds.txt | wc | awk '{print $1}'`
	set num_train = `less PNN_train$folds.txt | wc | awk '{print $1}'`
	set num_train2 = `less PN2N_train$folds.txt | wc | awk '{print $1}'`

	foreach sim_file (chem_Jacarrd_sim.csv enzyme_Jacarrd_sim.csv indication_Jacarrd_sim.csv offsideeffect_Jacarrd_sim.csv pathway_Jacarrd_sim.csv target_Jacarrd_sim.csv transporter_Jacarrd_sim.csv)
		foreach L (8 16 64)
			python frame_work_test.py --attention $1 --train PNN_train$folds.txt --test PN_test_$folds.txt --validation PN_validation_$folds.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_file $sim_file --L_dim $L > ./simmilarity_effects_results_Ponly/log_$sim_file\_1N_attention_$1_L_$L\_feature_50_1_$folds.txt
			python frame_work_test.py --attention $1 --train PN2N_train$folds.txt --test PN_test_$folds.txt --validation PN_validation_$folds.txt --num_train_sample $num_train2 --num_validation_sample $num_val --num_test_sample $num_test --sim_file $sim_file --L_dim $L > ./simmilarity_effects_results_Ponly/log_$sim_file\_2N_attention_$1_L_$L\_feature_50_1_$folds.txt
		end
	end
end
