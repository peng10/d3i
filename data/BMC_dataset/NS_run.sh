#!/bin/tcsh
foreach folds ($2)
	set num_test = `less PNN_entire_test$folds.txt | wc | awk '{print $1}'`
	set num_val = `less PNN_entire_validation$folds.txt | wc | awk '{print $1}'`
	set num_train = `less PN_train_$folds.txt | wc | awk '{print $1}'`

	foreach sim_file (transporter_Jacarrd_sim.csv)
		foreach size (64 128)
			if($1 == 1) then
				foreach L (8 16 32 64 128)
					python frame_work_test.py --attention $1 --train PN_train_$folds.txt --test PNN_entire_test$folds.txt --validation PNN_entire_validation$folds.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_file $sim_file --L_dim $L --freq_file freq_PNtrain$folds.txt --graph_feature_size $size --out_path ./batch_sample_results/log_hypo_prediction_value/ > ./batch_sample_results/log_hypo/log_$sim_file\_1N_attention_$1_L_$L\_feature_$size\_1_$folds.txt
				end
			else
				foreach L (32)
					foreach layer (1 2 4 6)
						python frame_work_test.py --attention $1 --train PN_train_$folds.txt --test PNN_entire_test$folds.txt --validation PNN_entire_validation$folds.txt --num_train_sample $num_train --num_validation_sample $num_val --num_test_sample $num_test --sim_file $sim_file --L_dim $L --freq_file freq_PNtrain$folds.txt --graph_feature_size $size --num_layers $layer --max_pool 0 --out_path ./batch_sample_results/mean_hypo_prediction_value/ > ./batch_sample_results/mean_hypo/log_$sim_file\_1N_attention_$1_L_$L\_feature_$size\_$layer\_$folds.txt
					end
				end
			endif
		end
	end
end
