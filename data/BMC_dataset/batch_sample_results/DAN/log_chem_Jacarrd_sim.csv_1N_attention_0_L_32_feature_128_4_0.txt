start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.50670284032822
time_elpased: 2.081
batch start
#iterations: 1
currently lose_sum: 100.46248453855515
time_elpased: 2.07
batch start
#iterations: 2
currently lose_sum: 100.4224436879158
time_elpased: 2.115
batch start
#iterations: 3
currently lose_sum: 100.45118165016174
time_elpased: 2.089
batch start
#iterations: 4
currently lose_sum: 100.40555602312088
time_elpased: 2.077
batch start
#iterations: 5
currently lose_sum: 100.44308489561081
time_elpased: 2.089
batch start
#iterations: 6
currently lose_sum: 100.33962887525558
time_elpased: 2.068
batch start
#iterations: 7
currently lose_sum: 100.30524599552155
time_elpased: 2.06
batch start
#iterations: 8
currently lose_sum: 100.19142937660217
time_elpased: 2.059
batch start
#iterations: 9
currently lose_sum: 100.19305229187012
time_elpased: 2.084
batch start
#iterations: 10
currently lose_sum: 100.10328561067581
time_elpased: 2.102
batch start
#iterations: 11
currently lose_sum: 100.10318356752396
time_elpased: 2.113
batch start
#iterations: 12
currently lose_sum: 100.00350898504257
time_elpased: 2.051
batch start
#iterations: 13
currently lose_sum: 100.04792761802673
time_elpased: 2.101
batch start
#iterations: 14
currently lose_sum: 100.09456020593643
time_elpased: 2.052
batch start
#iterations: 15
currently lose_sum: 99.82548028230667
time_elpased: 2.09
batch start
#iterations: 16
currently lose_sum: 100.00428527593613
time_elpased: 2.044
batch start
#iterations: 17
currently lose_sum: 99.90300804376602
time_elpased: 2.081
batch start
#iterations: 18
currently lose_sum: 100.0228328704834
time_elpased: 2.067
batch start
#iterations: 19
currently lose_sum: 99.87047511339188
time_elpased: 2.021
start validation test
0.600257731959
0.614656381487
0.541216424822
0.575603349204
0.60036138808
65.608
batch start
#iterations: 20
currently lose_sum: 100.00929749011993
time_elpased: 2.07
batch start
#iterations: 21
currently lose_sum: 100.11751645803452
time_elpased: 1.987
batch start
#iterations: 22
currently lose_sum: 99.88381510972977
time_elpased: 2.084
batch start
#iterations: 23
currently lose_sum: 99.59273988008499
time_elpased: 2.085
batch start
#iterations: 24
currently lose_sum: 99.71584689617157
time_elpased: 2.185
batch start
#iterations: 25
currently lose_sum: 99.7852675318718
time_elpased: 2.113
batch start
#iterations: 26
currently lose_sum: 99.80454123020172
time_elpased: 2.092
batch start
#iterations: 27
currently lose_sum: 99.70491659641266
time_elpased: 2.053
batch start
#iterations: 28
currently lose_sum: 99.73949748277664
time_elpased: 2.028
batch start
#iterations: 29
currently lose_sum: 100.10402542352676
time_elpased: 2.052
batch start
#iterations: 30
currently lose_sum: 99.40929406881332
time_elpased: 2.072
batch start
#iterations: 31
currently lose_sum: 99.78616070747375
time_elpased: 2.112
batch start
#iterations: 32
currently lose_sum: 99.93538177013397
time_elpased: 2.064
batch start
#iterations: 33
currently lose_sum: 100.02834892272949
time_elpased: 2.039
batch start
#iterations: 34
currently lose_sum: 99.57480871677399
time_elpased: 2.075
batch start
#iterations: 35
currently lose_sum: 99.76290875673294
time_elpased: 2.047
batch start
#iterations: 36
currently lose_sum: 100.19715225696564
time_elpased: 2.05
batch start
#iterations: 37
currently lose_sum: 99.6343834400177
time_elpased: 2.083
batch start
#iterations: 38
currently lose_sum: 99.7157793045044
time_elpased: 2.05
batch start
#iterations: 39
currently lose_sum: 100.4207381606102
time_elpased: 2.108
start validation test
0.563350515464
0.550032123354
0.704847175054
0.617889846182
0.563102096254
67.159
batch start
#iterations: 40
currently lose_sum: 99.88601362705231
time_elpased: 2.052
batch start
#iterations: 41
currently lose_sum: 100.0483176112175
time_elpased: 2.069
batch start
#iterations: 42
currently lose_sum: 100.40357530117035
time_elpased: 2.023
batch start
#iterations: 43
currently lose_sum: 100.50460064411163
time_elpased: 2.082
batch start
#iterations: 44
currently lose_sum: 100.50359135866165
time_elpased: 2.028
batch start
#iterations: 45
currently lose_sum: 100.50068020820618
time_elpased: 2.072
batch start
#iterations: 46
currently lose_sum: 100.4887627363205
time_elpased: 2.065
batch start
#iterations: 47
currently lose_sum: 100.38838505744934
time_elpased: 2.074
batch start
#iterations: 48
currently lose_sum: 100.12441623210907
time_elpased: 2.093
batch start
#iterations: 49
currently lose_sum: 100.05009704828262
time_elpased: 2.03
batch start
#iterations: 50
currently lose_sum: 100.49935710430145
time_elpased: 2.057
batch start
#iterations: 51
currently lose_sum: 100.30659514665604
time_elpased: 2.007
batch start
#iterations: 52
currently lose_sum: 99.98674035072327
time_elpased: 2.095
batch start
#iterations: 53
currently lose_sum: 99.67131972312927
time_elpased: 2.072
batch start
#iterations: 54
currently lose_sum: 99.62364882230759
time_elpased: 2.082
batch start
#iterations: 55
currently lose_sum: 100.20072090625763
time_elpased: 2.099
batch start
#iterations: 56
currently lose_sum: 100.48234099149704
time_elpased: 2.05
batch start
#iterations: 57
currently lose_sum: 99.92058271169662
time_elpased: 2.076
batch start
#iterations: 58
currently lose_sum: 100.61751455068588
time_elpased: 2.081
batch start
#iterations: 59
currently lose_sum: 100.5061696767807
time_elpased: 2.032
start validation test
0.517783505155
0.511884438608
0.802305238242
0.625005010623
0.517283983368
67.235
batch start
#iterations: 60
currently lose_sum: 100.50623351335526
time_elpased: 2.059
batch start
#iterations: 61
currently lose_sum: 100.50612270832062
time_elpased: 2.032
batch start
#iterations: 62
currently lose_sum: 100.50613671541214
time_elpased: 2.002
batch start
#iterations: 63
currently lose_sum: 100.50607693195343
time_elpased: 2.056
batch start
#iterations: 64
currently lose_sum: 100.50595337152481
time_elpased: 2.059
batch start
#iterations: 65
currently lose_sum: 100.50592112541199
time_elpased: 2.033
batch start
#iterations: 66
currently lose_sum: 100.5056911110878
time_elpased: 2.034
batch start
#iterations: 67
currently lose_sum: 100.50584399700165
time_elpased: 2.123
batch start
#iterations: 68
currently lose_sum: 100.50567334890366
time_elpased: 2.075
batch start
#iterations: 69
currently lose_sum: 100.50553059577942
time_elpased: 2.001
batch start
#iterations: 70
currently lose_sum: 100.50536918640137
time_elpased: 2.103
batch start
#iterations: 71
currently lose_sum: 100.50487685203552
time_elpased: 2.028
batch start
#iterations: 72
currently lose_sum: 100.50398594141006
time_elpased: 2.193
batch start
#iterations: 73
currently lose_sum: 100.50032812356949
time_elpased: 2.032
batch start
#iterations: 74
currently lose_sum: 100.44396376609802
time_elpased: 2.05
batch start
#iterations: 75
currently lose_sum: 100.30543130636215
time_elpased: 2.078
batch start
#iterations: 76
currently lose_sum: 100.23155492544174
time_elpased: 2.039
batch start
#iterations: 77
currently lose_sum: 100.16813349723816
time_elpased: 2.096
batch start
#iterations: 78
currently lose_sum: 100.04645639657974
time_elpased: 2.091
batch start
#iterations: 79
currently lose_sum: 100.3581355214119
time_elpased: 2.081
start validation test
0.567474226804
0.560338551147
0.633631779356
0.594735571118
0.567358077017
67.209
batch start
#iterations: 80
currently lose_sum: 100.18623667955399
time_elpased: 2.059
batch start
#iterations: 81
currently lose_sum: 100.53397637605667
time_elpased: 2.008
batch start
#iterations: 82
currently lose_sum: 100.47926115989685
time_elpased: 2.064
batch start
#iterations: 83
currently lose_sum: 100.07548028230667
time_elpased: 2.037
batch start
#iterations: 84
currently lose_sum: 100.07033085823059
time_elpased: 2.065
batch start
#iterations: 85
currently lose_sum: 99.98327833414078
time_elpased: 2.054
batch start
#iterations: 86
currently lose_sum: 100.30992448329926
time_elpased: 2.027
batch start
#iterations: 87
currently lose_sum: 100.24841630458832
time_elpased: 2.088
batch start
#iterations: 88
currently lose_sum: 100.50624561309814
time_elpased: 2.045
batch start
#iterations: 89
currently lose_sum: 100.50614559650421
time_elpased: 2.074
batch start
#iterations: 90
currently lose_sum: 100.50611120462418
time_elpased: 2.105
batch start
#iterations: 91
currently lose_sum: 100.50614911317825
time_elpased: 2.044
batch start
#iterations: 92
currently lose_sum: 100.50614589452744
time_elpased: 2.098
batch start
#iterations: 93
currently lose_sum: 100.50602388381958
time_elpased: 2.023
batch start
#iterations: 94
currently lose_sum: 100.50603652000427
time_elpased: 2.06
batch start
#iterations: 95
currently lose_sum: 100.50604647397995
time_elpased: 2.063
batch start
#iterations: 96
currently lose_sum: 100.50606745481491
time_elpased: 2.058
batch start
#iterations: 97
currently lose_sum: 100.50589942932129
time_elpased: 2.041
batch start
#iterations: 98
currently lose_sum: 100.50593400001526
time_elpased: 2.096
batch start
#iterations: 99
currently lose_sum: 100.50579398870468
time_elpased: 2.077
start validation test
0.52587628866
0.520437898716
0.680045281465
0.589631480325
0.525605621214
67.234
batch start
#iterations: 100
currently lose_sum: 100.50552147626877
time_elpased: 2.098
batch start
#iterations: 101
currently lose_sum: 100.50525206327438
time_elpased: 2.029
batch start
#iterations: 102
currently lose_sum: 100.50466269254684
time_elpased: 2.108
batch start
#iterations: 103
currently lose_sum: 100.50328415632248
time_elpased: 2.115
batch start
#iterations: 104
currently lose_sum: 100.49739134311676
time_elpased: 2.102
batch start
#iterations: 105
currently lose_sum: 100.44325828552246
time_elpased: 2.096
batch start
#iterations: 106
currently lose_sum: 100.3535607457161
time_elpased: 2.042
batch start
#iterations: 107
currently lose_sum: 100.06781786680222
time_elpased: 2.058
batch start
#iterations: 108
currently lose_sum: 99.86218255758286
time_elpased: 2.074
batch start
#iterations: 109
currently lose_sum: 99.97496598958969
time_elpased: 2.052
batch start
#iterations: 110
currently lose_sum: 99.96199852228165
time_elpased: 2.09
batch start
#iterations: 111
currently lose_sum: 99.83571708202362
time_elpased: 2.049
batch start
#iterations: 112
currently lose_sum: 100.50933575630188
time_elpased: 2.025
batch start
#iterations: 113
currently lose_sum: 100.48386371135712
time_elpased: 2.081
batch start
#iterations: 114
currently lose_sum: 99.97068059444427
time_elpased: 2.075
batch start
#iterations: 115
currently lose_sum: 100.00135785341263
time_elpased: 2.074
batch start
#iterations: 116
currently lose_sum: 99.75705921649933
time_elpased: 2.028
batch start
#iterations: 117
currently lose_sum: 99.97419518232346
time_elpased: 2.039
batch start
#iterations: 118
currently lose_sum: 99.56197619438171
time_elpased: 2.057
batch start
#iterations: 119
currently lose_sum: 100.50640344619751
time_elpased: 2.037
start validation test
0.59618556701
0.609770315961
0.538231964598
0.571772165737
0.596287313498
67.010
batch start
#iterations: 120
currently lose_sum: 99.50129508972168
time_elpased: 2.063
batch start
#iterations: 121
currently lose_sum: 99.48321604728699
time_elpased: 2.027
batch start
#iterations: 122
currently lose_sum: 99.38161742687225
time_elpased: 2.077
batch start
#iterations: 123
currently lose_sum: 100.50833237171173
time_elpased: 2.02
batch start
#iterations: 124
currently lose_sum: 100.50507646799088
time_elpased: 2.056
batch start
#iterations: 125
currently lose_sum: 100.50344198942184
time_elpased: 2.056
batch start
#iterations: 126
currently lose_sum: 100.50063073635101
time_elpased: 2.029
batch start
#iterations: 127
currently lose_sum: 100.48494017124176
time_elpased: 2.059
batch start
#iterations: 128
currently lose_sum: 100.09433925151825
time_elpased: 2.115
batch start
#iterations: 129
currently lose_sum: 100.4012680053711
time_elpased: 2.038
batch start
#iterations: 130
currently lose_sum: 100.5041834115982
time_elpased: 2.127
batch start
#iterations: 131
currently lose_sum: 100.48036241531372
time_elpased: 2.045
batch start
#iterations: 132
currently lose_sum: 99.95459347963333
time_elpased: 2.07
batch start
#iterations: 133
currently lose_sum: 99.97519272565842
time_elpased: 1.979
batch start
#iterations: 134
currently lose_sum: 99.86983150243759
time_elpased: 2.056
batch start
#iterations: 135
currently lose_sum: 99.59749460220337
time_elpased: 2.051
batch start
#iterations: 136
currently lose_sum: 99.52968668937683
time_elpased: 2.073
batch start
#iterations: 137
currently lose_sum: 99.4004967212677
time_elpased: 2.067
batch start
#iterations: 138
currently lose_sum: 100.1617329120636
time_elpased: 2.09
batch start
#iterations: 139
currently lose_sum: 100.50111520290375
time_elpased: 2.052
start validation test
0.548917525773
0.533847231955
0.78398682721
0.635177387752
0.548504825358
67.227
batch start
#iterations: 140
currently lose_sum: 100.49432933330536
time_elpased: 2.037
batch start
#iterations: 141
currently lose_sum: 100.3482363820076
time_elpased: 2.02
batch start
#iterations: 142
currently lose_sum: 99.69082367420197
time_elpased: 2.07
batch start
#iterations: 143
currently lose_sum: 99.58423507213593
time_elpased: 2.018
batch start
#iterations: 144
currently lose_sum: 100.50338613986969
time_elpased: 2.06
batch start
#iterations: 145
currently lose_sum: 100.50211817026138
time_elpased: 2.068
batch start
#iterations: 146
currently lose_sum: 100.49378073215485
time_elpased: 2.071
batch start
#iterations: 147
currently lose_sum: 100.26834809780121
time_elpased: 2.047
batch start
#iterations: 148
currently lose_sum: 100.19467848539352
time_elpased: 2.041
batch start
#iterations: 149
currently lose_sum: 99.41965419054031
time_elpased: 2.103
batch start
#iterations: 150
currently lose_sum: 99.50161516666412
time_elpased: 2.014
batch start
#iterations: 151
currently lose_sum: 99.05261117219925
time_elpased: 2.108
batch start
#iterations: 152
currently lose_sum: 100.50787377357483
time_elpased: 2.058
batch start
#iterations: 153
currently lose_sum: 100.50099956989288
time_elpased: 2.088
batch start
#iterations: 154
currently lose_sum: 100.48177790641785
time_elpased: 2.072
batch start
#iterations: 155
currently lose_sum: 99.7662587761879
time_elpased: 2.062
batch start
#iterations: 156
currently lose_sum: 100.01634114980698
time_elpased: 2.072
batch start
#iterations: 157
currently lose_sum: 100.34579813480377
time_elpased: 2.019
batch start
#iterations: 158
currently lose_sum: 99.827317237854
time_elpased: 2.085
batch start
#iterations: 159
currently lose_sum: 100.09247493743896
time_elpased: 2.103
start validation test
0.577422680412
0.65427584806
0.331480909746
0.440027322404
0.577854469125
64.660
batch start
#iterations: 160
currently lose_sum: 99.21850764751434
time_elpased: 2.039
batch start
#iterations: 161
currently lose_sum: 99.88469189405441
time_elpased: 2.048
batch start
#iterations: 162
currently lose_sum: 99.37622660398483
time_elpased: 2.075
batch start
#iterations: 163
currently lose_sum: 100.52052062749863
time_elpased: 2.133
batch start
#iterations: 164
currently lose_sum: 100.48625594377518
time_elpased: 2.057
batch start
#iterations: 165
currently lose_sum: 99.62691801786423
time_elpased: 2.078
batch start
#iterations: 166
currently lose_sum: 99.36615854501724
time_elpased: 2.066
batch start
#iterations: 167
currently lose_sum: 99.06525987386703
time_elpased: 1.984
batch start
#iterations: 168
currently lose_sum: 98.70437324047089
time_elpased: 2.111
batch start
#iterations: 169
currently lose_sum: 98.44557982683182
time_elpased: 2.056
batch start
#iterations: 170
currently lose_sum: 100.20338106155396
time_elpased: 2.084
batch start
#iterations: 171
currently lose_sum: 100.50345492362976
time_elpased: 2.04
batch start
#iterations: 172
currently lose_sum: 100.48186898231506
time_elpased: 2.048
batch start
#iterations: 173
currently lose_sum: 99.59653931856155
time_elpased: 2.068
batch start
#iterations: 174
currently lose_sum: 100.48233330249786
time_elpased: 2.032
batch start
#iterations: 175
currently lose_sum: 99.59465527534485
time_elpased: 2.089
batch start
#iterations: 176
currently lose_sum: 98.96267265081406
time_elpased: 2.054
batch start
#iterations: 177
currently lose_sum: 98.03524607419968
time_elpased: 2.034
batch start
#iterations: 178
currently lose_sum: 98.69542390108109
time_elpased: 2.039
batch start
#iterations: 179
currently lose_sum: 98.59766280651093
time_elpased: 2.066
start validation test
0.575721649485
0.543886591849
0.947617577442
0.691109693399
0.575068728822
65.251
batch start
#iterations: 180
currently lose_sum: 98.63463652133942
time_elpased: 2.108
batch start
#iterations: 181
currently lose_sum: 98.40685099363327
time_elpased: 2.065
batch start
#iterations: 182
currently lose_sum: 100.08476138114929
time_elpased: 2.109
batch start
#iterations: 183
currently lose_sum: 98.55666470527649
time_elpased: 2.025
batch start
#iterations: 184
currently lose_sum: 100.50600576400757
time_elpased: 2.087
batch start
#iterations: 185
currently lose_sum: 100.5050128698349
time_elpased: 2.074
batch start
#iterations: 186
currently lose_sum: 100.50339615345001
time_elpased: 2.069
batch start
#iterations: 187
currently lose_sum: 100.49989837408066
time_elpased: 2.065
batch start
#iterations: 188
currently lose_sum: 100.46480000019073
time_elpased: 2.029
batch start
#iterations: 189
currently lose_sum: 98.85294026136398
time_elpased: 2.083
batch start
#iterations: 190
currently lose_sum: 99.5696012377739
time_elpased: 2.109
batch start
#iterations: 191
currently lose_sum: 99.96874910593033
time_elpased: 2.028
batch start
#iterations: 192
currently lose_sum: 100.50374627113342
time_elpased: 2.049
batch start
#iterations: 193
currently lose_sum: 100.50208020210266
time_elpased: 2.092
batch start
#iterations: 194
currently lose_sum: 100.49759835004807
time_elpased: 2.043
batch start
#iterations: 195
currently lose_sum: 100.40859633684158
time_elpased: 2.028
batch start
#iterations: 196
currently lose_sum: 99.18818497657776
time_elpased: 2.056
batch start
#iterations: 197
currently lose_sum: 99.37983328104019
time_elpased: 2.064
batch start
#iterations: 198
currently lose_sum: 100.50756984949112
time_elpased: 2.024
batch start
#iterations: 199
currently lose_sum: 100.50689798593521
time_elpased: 2.053
start validation test
0.50087628866
0.50087628866
1.0
0.667445135144
0.5
67.236
batch start
#iterations: 200
currently lose_sum: 100.50622999668121
time_elpased: 1.999
batch start
#iterations: 201
currently lose_sum: 100.50561046600342
time_elpased: 2.118
batch start
#iterations: 202
currently lose_sum: 100.50453335046768
time_elpased: 2.064
batch start
#iterations: 203
currently lose_sum: 100.5022800564766
time_elpased: 2.04
batch start
#iterations: 204
currently lose_sum: 100.49362063407898
time_elpased: 2.073
batch start
#iterations: 205
currently lose_sum: 99.9175626039505
time_elpased: 2.035
batch start
#iterations: 206
currently lose_sum: 100.59893989562988
time_elpased: 2.079
batch start
#iterations: 207
currently lose_sum: 99.81583666801453
time_elpased: 2.031
batch start
#iterations: 208
currently lose_sum: 99.30920022726059
time_elpased: 2.084
batch start
#iterations: 209
currently lose_sum: 99.67769867181778
time_elpased: 2.091
batch start
#iterations: 210
currently lose_sum: 100.50413888692856
time_elpased: 2.069
batch start
#iterations: 211
currently lose_sum: 100.5032286643982
time_elpased: 2.028
batch start
#iterations: 212
currently lose_sum: 100.50257700681686
time_elpased: 2.098
batch start
#iterations: 213
currently lose_sum: 100.50007808208466
time_elpased: 2.089
batch start
#iterations: 214
currently lose_sum: 100.48384428024292
time_elpased: 2.049
batch start
#iterations: 215
currently lose_sum: 99.4083684682846
time_elpased: 2.036
batch start
#iterations: 216
currently lose_sum: 98.97330033779144
time_elpased: 2.058
batch start
#iterations: 217
currently lose_sum: 98.92537158727646
time_elpased: 2.032
batch start
#iterations: 218
currently lose_sum: 100.50400137901306
time_elpased: 2.066
batch start
#iterations: 219
currently lose_sum: 100.48825341463089
time_elpased: 2.005
start validation test
0.588865979381
0.569746013941
0.731810229495
0.640688350302
0.588615018703
67.169
batch start
#iterations: 220
currently lose_sum: 99.38495951890945
time_elpased: 2.085
batch start
#iterations: 221
currently lose_sum: 98.96204215288162
time_elpased: 2.04
batch start
#iterations: 222
currently lose_sum: 100.4976966381073
time_elpased: 2.137
batch start
#iterations: 223
currently lose_sum: 99.80113101005554
time_elpased: 2.06
batch start
#iterations: 224
currently lose_sum: 98.70572847127914
time_elpased: 2.027
batch start
#iterations: 225
currently lose_sum: 98.94584357738495
time_elpased: 2.055
batch start
#iterations: 226
currently lose_sum: 100.04643768072128
time_elpased: 2.03
batch start
#iterations: 227
currently lose_sum: 100.47602242231369
time_elpased: 2.001
batch start
#iterations: 228
currently lose_sum: 98.97593295574188
time_elpased: 2.118
batch start
#iterations: 229
currently lose_sum: 98.44039869308472
time_elpased: 2.027
batch start
#iterations: 230
currently lose_sum: 98.98641002178192
time_elpased: 2.074
batch start
#iterations: 231
currently lose_sum: 98.32924193143845
time_elpased: 2.08
batch start
#iterations: 232
currently lose_sum: 98.48469507694244
time_elpased: 2.094
batch start
#iterations: 233
currently lose_sum: 98.72670257091522
time_elpased: 2.066
batch start
#iterations: 234
currently lose_sum: 98.04582726955414
time_elpased: 2.029
batch start
#iterations: 235
currently lose_sum: 99.83462691307068
time_elpased: 2.1
batch start
#iterations: 236
currently lose_sum: 100.50266313552856
time_elpased: 2.038
batch start
#iterations: 237
currently lose_sum: 100.47501558065414
time_elpased: 2.081
batch start
#iterations: 238
currently lose_sum: 98.612479865551
time_elpased: 2.131
batch start
#iterations: 239
currently lose_sum: 100.51053190231323
time_elpased: 2.058
start validation test
0.563092783505
0.538778826323
0.887207986004
0.670425383
0.562523749276
67.226
batch start
#iterations: 240
currently lose_sum: 100.46869134902954
time_elpased: 2.12
batch start
#iterations: 241
currently lose_sum: 99.98175936937332
time_elpased: 2.04
batch start
#iterations: 242
currently lose_sum: 98.31836944818497
time_elpased: 2.066
batch start
#iterations: 243
currently lose_sum: 98.47367161512375
time_elpased: 2.064
batch start
#iterations: 244
currently lose_sum: 99.21224695444107
time_elpased: 2.068
batch start
#iterations: 245
currently lose_sum: 100.49742555618286
time_elpased: 2.05
batch start
#iterations: 246
currently lose_sum: 99.4466422200203
time_elpased: 2.056
batch start
#iterations: 247
currently lose_sum: 98.26901227235794
time_elpased: 2.056
batch start
#iterations: 248
currently lose_sum: 99.854740858078
time_elpased: 2.075
batch start
#iterations: 249
currently lose_sum: 99.15937346220016
time_elpased: 2.116
batch start
#iterations: 250
currently lose_sum: 98.28842049837112
time_elpased: 2.116
batch start
#iterations: 251
currently lose_sum: 98.34490245580673
time_elpased: 2.113
batch start
#iterations: 252
currently lose_sum: 98.21780210733414
time_elpased: 2.121
batch start
#iterations: 253
currently lose_sum: 99.31213456392288
time_elpased: 2.084
batch start
#iterations: 254
currently lose_sum: 99.19156408309937
time_elpased: 2.121
batch start
#iterations: 255
currently lose_sum: 98.10722708702087
time_elpased: 2.067
batch start
#iterations: 256
currently lose_sum: 98.23434692621231
time_elpased: 2.093
batch start
#iterations: 257
currently lose_sum: 98.63765376806259
time_elpased: 2.098
batch start
#iterations: 258
currently lose_sum: 99.26943880319595
time_elpased: 2.066
batch start
#iterations: 259
currently lose_sum: 98.26017981767654
time_elpased: 2.124
start validation test
0.653969072165
0.62398877332
0.77791499434
0.692501488709
0.653751465981
62.989
batch start
#iterations: 260
currently lose_sum: 97.42789369821548
time_elpased: 2.092
batch start
#iterations: 261
currently lose_sum: 97.87900865077972
time_elpased: 2.089
batch start
#iterations: 262
currently lose_sum: 98.2200477719307
time_elpased: 2.055
batch start
#iterations: 263
currently lose_sum: 99.25388479232788
time_elpased: 2.064
batch start
#iterations: 264
currently lose_sum: 100.49797707796097
time_elpased: 2.083
batch start
#iterations: 265
currently lose_sum: 100.31114858388901
time_elpased: 2.062
batch start
#iterations: 266
currently lose_sum: 99.88513267040253
time_elpased: 2.153
batch start
#iterations: 267
currently lose_sum: 100.48583954572678
time_elpased: 2.037
batch start
#iterations: 268
currently lose_sum: 99.3317699432373
time_elpased: 2.099
batch start
#iterations: 269
currently lose_sum: 98.19207900762558
time_elpased: 2.07
batch start
#iterations: 270
currently lose_sum: 100.27959853410721
time_elpased: 2.043
batch start
#iterations: 271
currently lose_sum: 98.30122315883636
time_elpased: 2.057
batch start
#iterations: 272
currently lose_sum: 97.64164054393768
time_elpased: 2.104
batch start
#iterations: 273
currently lose_sum: 99.05551564693451
time_elpased: 2.048
batch start
#iterations: 274
currently lose_sum: 99.70624071359634
time_elpased: 2.118
batch start
#iterations: 275
currently lose_sum: 97.62691509723663
time_elpased: 2.105
batch start
#iterations: 276
currently lose_sum: 97.58976286649704
time_elpased: 2.091
batch start
#iterations: 277
currently lose_sum: 98.33826994895935
time_elpased: 2.075
batch start
#iterations: 278
currently lose_sum: 100.50923997163773
time_elpased: 2.102
batch start
#iterations: 279
currently lose_sum: 100.50524002313614
time_elpased: 2.087
start validation test
0.546958762887
0.527287697012
0.922712771432
0.671082669062
0.546299068769
67.233
batch start
#iterations: 280
currently lose_sum: 100.50457012653351
time_elpased: 2.094
batch start
#iterations: 281
currently lose_sum: 100.50321418046951
time_elpased: 2.066
batch start
#iterations: 282
currently lose_sum: 100.49980306625366
time_elpased: 2.128
batch start
#iterations: 283
currently lose_sum: 100.47354298830032
time_elpased: 2.053
batch start
#iterations: 284
currently lose_sum: 98.83856761455536
time_elpased: 2.084
batch start
#iterations: 285
currently lose_sum: 97.41217344999313
time_elpased: 2.035
batch start
#iterations: 286
currently lose_sum: 100.40818160772324
time_elpased: 2.05
batch start
#iterations: 287
currently lose_sum: 100.50591552257538
time_elpased: 2.084
batch start
#iterations: 288
currently lose_sum: 100.50521874427795
time_elpased: 2.089
batch start
#iterations: 289
currently lose_sum: 100.504509806633
time_elpased: 2.072
batch start
#iterations: 290
currently lose_sum: 100.49946796894073
time_elpased: 2.045
batch start
#iterations: 291
currently lose_sum: 100.21625578403473
time_elpased: 2.013
batch start
#iterations: 292
currently lose_sum: 98.397445499897
time_elpased: 2.103
batch start
#iterations: 293
currently lose_sum: 99.54518687725067
time_elpased: 2.091
batch start
#iterations: 294
currently lose_sum: 98.0141476392746
time_elpased: 2.09
batch start
#iterations: 295
currently lose_sum: 97.81525361537933
time_elpased: 2.039
batch start
#iterations: 296
currently lose_sum: 97.8441327214241
time_elpased: 2.026
batch start
#iterations: 297
currently lose_sum: 97.47505229711533
time_elpased: 2.138
batch start
#iterations: 298
currently lose_sum: 100.54407262802124
time_elpased: 2.047
batch start
#iterations: 299
currently lose_sum: 100.5055747628212
time_elpased: 2.097
start validation test
0.529536082474
0.516791894353
0.934341875064
0.665493861096
0.528825383468
67.233
batch start
#iterations: 300
currently lose_sum: 100.50406992435455
time_elpased: 2.086
batch start
#iterations: 301
currently lose_sum: 100.49758440256119
time_elpased: 2.046
batch start
#iterations: 302
currently lose_sum: 99.9607338309288
time_elpased: 2.057
batch start
#iterations: 303
currently lose_sum: 99.83623665571213
time_elpased: 2.073
batch start
#iterations: 304
currently lose_sum: 98.9744005203247
time_elpased: 2.119
batch start
#iterations: 305
currently lose_sum: 100.50306159257889
time_elpased: 2.071
batch start
#iterations: 306
currently lose_sum: 100.49084377288818
time_elpased: 2.104
batch start
#iterations: 307
currently lose_sum: 98.82244151830673
time_elpased: 2.07
batch start
#iterations: 308
currently lose_sum: 100.51014113426208
time_elpased: 2.137
batch start
#iterations: 309
currently lose_sum: 100.5060271024704
time_elpased: 2.072
batch start
#iterations: 310
currently lose_sum: 100.50590604543686
time_elpased: 2.02
batch start
#iterations: 311
currently lose_sum: 100.50584244728088
time_elpased: 2.093
batch start
#iterations: 312
currently lose_sum: 100.50574445724487
time_elpased: 2.081
batch start
#iterations: 313
currently lose_sum: 100.50575429201126
time_elpased: 2.123
batch start
#iterations: 314
currently lose_sum: 100.50560700893402
time_elpased: 2.074
batch start
#iterations: 315
currently lose_sum: 100.50549012422562
time_elpased: 2.047
batch start
#iterations: 316
currently lose_sum: 100.50521266460419
time_elpased: 2.119
batch start
#iterations: 317
currently lose_sum: 100.5044230222702
time_elpased: 2.038
batch start
#iterations: 318
currently lose_sum: 100.50316417217255
time_elpased: 2.084
batch start
#iterations: 319
currently lose_sum: 100.48794060945511
time_elpased: 2.063
start validation test
0.560103092784
0.556771283233
0.596994957291
0.576181962654
0.560038323425
67.163
batch start
#iterations: 320
currently lose_sum: 99.74871283769608
time_elpased: 2.133
batch start
#iterations: 321
currently lose_sum: 100.4837755560875
time_elpased: 2.061
batch start
#iterations: 322
currently lose_sum: 99.02204948663712
time_elpased: 2.081
batch start
#iterations: 323
currently lose_sum: 100.48812264204025
time_elpased: 2.056
batch start
#iterations: 324
currently lose_sum: 100.50619804859161
time_elpased: 2.05
batch start
#iterations: 325
currently lose_sum: 100.5062125325203
time_elpased: 2.079
batch start
#iterations: 326
currently lose_sum: 100.50616616010666
time_elpased: 2.057
batch start
#iterations: 327
currently lose_sum: 100.50619804859161
time_elpased: 2.073
batch start
#iterations: 328
currently lose_sum: 100.5062347650528
time_elpased: 2.087
batch start
#iterations: 329
currently lose_sum: 100.50618678331375
time_elpased: 2.031
batch start
#iterations: 330
currently lose_sum: 100.50618523359299
time_elpased: 2.106
batch start
#iterations: 331
currently lose_sum: 100.50620800256729
time_elpased: 2.065
batch start
#iterations: 332
currently lose_sum: 100.50619775056839
time_elpased: 2.103
batch start
#iterations: 333
currently lose_sum: 100.50617915391922
time_elpased: 2.069
batch start
#iterations: 334
currently lose_sum: 100.50622242689133
time_elpased: 2.125
batch start
#iterations: 335
currently lose_sum: 100.50616502761841
time_elpased: 2.063
batch start
#iterations: 336
currently lose_sum: 100.50619584321976
time_elpased: 2.092
batch start
#iterations: 337
currently lose_sum: 100.50617319345474
time_elpased: 2.13
batch start
#iterations: 338
currently lose_sum: 100.50623279809952
time_elpased: 2.102
batch start
#iterations: 339
currently lose_sum: 100.50612533092499
time_elpased: 2.071
start validation test
0.528144329897
0.517092719655
0.876402181743
0.650423890629
0.527532909523
67.235
batch start
#iterations: 340
currently lose_sum: 100.50609129667282
time_elpased: 2.082
batch start
#iterations: 341
currently lose_sum: 100.50615602731705
time_elpased: 2.052
batch start
#iterations: 342
currently lose_sum: 100.50609248876572
time_elpased: 2.111
batch start
#iterations: 343
currently lose_sum: 100.5060243010521
time_elpased: 2.068
batch start
#iterations: 344
currently lose_sum: 100.50603675842285
time_elpased: 2.096
batch start
#iterations: 345
currently lose_sum: 100.50596487522125
time_elpased: 2.079
batch start
#iterations: 346
currently lose_sum: 100.50599610805511
time_elpased: 2.084
batch start
#iterations: 347
currently lose_sum: 100.50595211982727
time_elpased: 2.044
batch start
#iterations: 348
currently lose_sum: 100.5058583021164
time_elpased: 2.087
batch start
#iterations: 349
currently lose_sum: 100.50573235750198
time_elpased: 2.069
batch start
#iterations: 350
currently lose_sum: 100.50536334514618
time_elpased: 2.082
batch start
#iterations: 351
currently lose_sum: 100.50429993867874
time_elpased: 2.12
batch start
#iterations: 352
currently lose_sum: 100.48914104700089
time_elpased: 2.053
batch start
#iterations: 353
currently lose_sum: 99.81707578897476
time_elpased: 2.076
batch start
#iterations: 354
currently lose_sum: 99.14203417301178
time_elpased: 2.104
batch start
#iterations: 355
currently lose_sum: 100.51323401927948
time_elpased: 2.064
batch start
#iterations: 356
currently lose_sum: 100.50624525547028
time_elpased: 2.109
batch start
#iterations: 357
currently lose_sum: 100.50625729560852
time_elpased: 2.047
batch start
#iterations: 358
currently lose_sum: 100.50626438856125
time_elpased: 2.122
batch start
#iterations: 359
currently lose_sum: 100.50625622272491
time_elpased: 2.076
start validation test
0.544381443299
0.538495264819
0.631985180611
0.581506557455
0.544227641426
67.235
batch start
#iterations: 360
currently lose_sum: 100.50625032186508
time_elpased: 2.11
batch start
#iterations: 361
currently lose_sum: 100.50625771284103
time_elpased: 2.113
batch start
#iterations: 362
currently lose_sum: 100.50624024868011
time_elpased: 2.098
batch start
#iterations: 363
currently lose_sum: 100.50624740123749
time_elpased: 2.121
batch start
#iterations: 364
currently lose_sum: 100.50624573230743
time_elpased: 2.063
batch start
#iterations: 365
currently lose_sum: 100.50624841451645
time_elpased: 2.13
batch start
#iterations: 366
currently lose_sum: 100.50622993707657
time_elpased: 2.074
batch start
#iterations: 367
currently lose_sum: 100.50622296333313
time_elpased: 2.042
batch start
#iterations: 368
currently lose_sum: 100.50619924068451
time_elpased: 2.124
batch start
#iterations: 369
currently lose_sum: 100.50622922182083
time_elpased: 2.039
batch start
#iterations: 370
currently lose_sum: 100.50619298219681
time_elpased: 2.099
batch start
#iterations: 371
currently lose_sum: 100.50618463754654
time_elpased: 2.099
batch start
#iterations: 372
currently lose_sum: 100.50616830587387
time_elpased: 2.051
batch start
#iterations: 373
currently lose_sum: 100.50614941120148
time_elpased: 2.15
batch start
#iterations: 374
currently lose_sum: 100.5061622262001
time_elpased: 2.079
batch start
#iterations: 375
currently lose_sum: 100.5061297416687
time_elpased: 2.087
batch start
#iterations: 376
currently lose_sum: 100.50610893964767
time_elpased: 2.076
batch start
#iterations: 377
currently lose_sum: 100.50606262683868
time_elpased: 2.062
batch start
#iterations: 378
currently lose_sum: 100.50601363182068
time_elpased: 2.086
batch start
#iterations: 379
currently lose_sum: 100.50589275360107
time_elpased: 2.111
start validation test
0.532628865979
0.521280775275
0.819285787795
0.637160350554
0.532125595539
67.235
batch start
#iterations: 380
currently lose_sum: 100.50591599941254
time_elpased: 2.035
batch start
#iterations: 381
currently lose_sum: 100.50560057163239
time_elpased: 2.054
batch start
#iterations: 382
currently lose_sum: 100.50514501333237
time_elpased: 2.076
batch start
#iterations: 383
currently lose_sum: 100.50308436155319
time_elpased: 2.086
batch start
#iterations: 384
currently lose_sum: 100.45014262199402
time_elpased: 2.088
batch start
#iterations: 385
currently lose_sum: 99.72185587882996
time_elpased: 2.081
batch start
#iterations: 386
currently lose_sum: 98.96892869472504
time_elpased: 2.099
batch start
#iterations: 387
currently lose_sum: 99.85627144575119
time_elpased: 2.09
batch start
#iterations: 388
currently lose_sum: 98.12922781705856
time_elpased: 2.035
batch start
#iterations: 389
currently lose_sum: 99.43681186437607
time_elpased: 2.116
batch start
#iterations: 390
currently lose_sum: 100.50626879930496
time_elpased: 2.04
batch start
#iterations: 391
currently lose_sum: 100.50611883401871
time_elpased: 2.061
batch start
#iterations: 392
currently lose_sum: 100.5058718919754
time_elpased: 2.016
batch start
#iterations: 393
currently lose_sum: 100.50563687086105
time_elpased: 2.064
batch start
#iterations: 394
currently lose_sum: 100.50496274232864
time_elpased: 2.117
batch start
#iterations: 395
currently lose_sum: 100.50097793340683
time_elpased: 2.105
batch start
#iterations: 396
currently lose_sum: 100.49038112163544
time_elpased: 2.063
batch start
#iterations: 397
currently lose_sum: 100.31488561630249
time_elpased: 2.022
batch start
#iterations: 398
currently lose_sum: 98.64971649646759
time_elpased: 2.058
batch start
#iterations: 399
currently lose_sum: 98.38954383134842
time_elpased: 2.107
start validation test
0.507577319588
0.504271278258
0.996295152825
0.669617845409
0.506719300052
67.139
acc: 0.650
pre: 0.621
rec: 0.776
F1: 0.690
auc: 0.650
