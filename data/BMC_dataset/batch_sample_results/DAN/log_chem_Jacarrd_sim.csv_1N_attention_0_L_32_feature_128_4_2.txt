start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.5032958984375
time_elpased: 2.135
batch start
#iterations: 1
currently lose_sum: 100.44852310419083
time_elpased: 2.091
batch start
#iterations: 2
currently lose_sum: 100.39141649007797
time_elpased: 2.091
batch start
#iterations: 3
currently lose_sum: 100.36590892076492
time_elpased: 2.107
batch start
#iterations: 4
currently lose_sum: 100.38441079854965
time_elpased: 2.076
batch start
#iterations: 5
currently lose_sum: 100.27127146720886
time_elpased: 2.08
batch start
#iterations: 6
currently lose_sum: 100.22629761695862
time_elpased: 2.121
batch start
#iterations: 7
currently lose_sum: 100.17561984062195
time_elpased: 2.093
batch start
#iterations: 8
currently lose_sum: 100.11464059352875
time_elpased: 2.094
batch start
#iterations: 9
currently lose_sum: 100.14341074228287
time_elpased: 2.086
batch start
#iterations: 10
currently lose_sum: 100.10608905553818
time_elpased: 2.058
batch start
#iterations: 11
currently lose_sum: 99.95645946264267
time_elpased: 2.141
batch start
#iterations: 12
currently lose_sum: 100.43605214357376
time_elpased: 2.114
batch start
#iterations: 13
currently lose_sum: 100.13179564476013
time_elpased: 2.097
batch start
#iterations: 14
currently lose_sum: 99.85727441310883
time_elpased: 2.076
batch start
#iterations: 15
currently lose_sum: 99.90382200479507
time_elpased: 2.112
batch start
#iterations: 16
currently lose_sum: 99.68503135442734
time_elpased: 2.078
batch start
#iterations: 17
currently lose_sum: 99.69030672311783
time_elpased: 2.087
batch start
#iterations: 18
currently lose_sum: 99.5927603840828
time_elpased: 2.084
batch start
#iterations: 19
currently lose_sum: 99.62744224071503
time_elpased: 2.097
start validation test
0.544639175258
0.642281662907
0.205104456108
0.310920436817
0.545235280827
66.229
batch start
#iterations: 20
currently lose_sum: 99.65901750326157
time_elpased: 2.106
batch start
#iterations: 21
currently lose_sum: 99.68330639600754
time_elpased: 2.134
batch start
#iterations: 22
currently lose_sum: 99.36627984046936
time_elpased: 2.05
batch start
#iterations: 23
currently lose_sum: 99.94147104024887
time_elpased: 2.123
batch start
#iterations: 24
currently lose_sum: 99.43849205970764
time_elpased: 2.098
batch start
#iterations: 25
currently lose_sum: 99.72423607110977
time_elpased: 2.133
batch start
#iterations: 26
currently lose_sum: 99.5777235031128
time_elpased: 2.086
batch start
#iterations: 27
currently lose_sum: 99.97300446033478
time_elpased: 2.098
batch start
#iterations: 28
currently lose_sum: 99.40885955095291
time_elpased: 2.098
batch start
#iterations: 29
currently lose_sum: 99.57344645261765
time_elpased: 2.103
batch start
#iterations: 30
currently lose_sum: 99.92347323894501
time_elpased: 2.064
batch start
#iterations: 31
currently lose_sum: 99.80774652957916
time_elpased: 2.105
batch start
#iterations: 32
currently lose_sum: 99.75667524337769
time_elpased: 2.155
batch start
#iterations: 33
currently lose_sum: 99.55087327957153
time_elpased: 2.042
batch start
#iterations: 34
currently lose_sum: 99.51560616493225
time_elpased: 2.144
batch start
#iterations: 35
currently lose_sum: 99.32029265165329
time_elpased: 2.089
batch start
#iterations: 36
currently lose_sum: 99.31748014688492
time_elpased: 2.071
batch start
#iterations: 37
currently lose_sum: 100.04016315937042
time_elpased: 2.122
batch start
#iterations: 38
currently lose_sum: 99.97989898920059
time_elpased: 2.081
batch start
#iterations: 39
currently lose_sum: 99.25812530517578
time_elpased: 2.063
start validation test
0.511288659794
0.621900826446
0.0619532777606
0.112681328966
0.512077537362
65.692
batch start
#iterations: 40
currently lose_sum: 100.0004797577858
time_elpased: 2.119
batch start
#iterations: 41
currently lose_sum: 99.61605751514435
time_elpased: 2.106
batch start
#iterations: 42
currently lose_sum: 99.26737064123154
time_elpased: 2.092
batch start
#iterations: 43
currently lose_sum: 100.45732796192169
time_elpased: 2.091
batch start
#iterations: 44
currently lose_sum: 99.9664409160614
time_elpased: 2.098
batch start
#iterations: 45
currently lose_sum: 99.38956838846207
time_elpased: 2.09
batch start
#iterations: 46
currently lose_sum: 100.50277781486511
time_elpased: 2.075
batch start
#iterations: 47
currently lose_sum: 99.63406628370285
time_elpased: 2.111
batch start
#iterations: 48
currently lose_sum: 99.31697130203247
time_elpased: 2.105
batch start
#iterations: 49
currently lose_sum: 100.50877898931503
time_elpased: 2.102
batch start
#iterations: 50
currently lose_sum: 100.46683049201965
time_elpased: 2.088
batch start
#iterations: 51
currently lose_sum: 99.8921988606453
time_elpased: 2.077
batch start
#iterations: 52
currently lose_sum: 99.51441180706024
time_elpased: 2.11
batch start
#iterations: 53
currently lose_sum: 99.9042580127716
time_elpased: 2.069
batch start
#iterations: 54
currently lose_sum: 100.49173855781555
time_elpased: 2.106
batch start
#iterations: 55
currently lose_sum: 99.90401083230972
time_elpased: 2.084
batch start
#iterations: 56
currently lose_sum: 100.59351015090942
time_elpased: 2.053
batch start
#iterations: 57
currently lose_sum: 100.50616478919983
time_elpased: 2.092
batch start
#iterations: 58
currently lose_sum: 100.50623667240143
time_elpased: 2.074
batch start
#iterations: 59
currently lose_sum: 100.50606894493103
time_elpased: 2.075
start validation test
0.513350515464
0.512853949329
0.56663579294
0.538405123943
0.513256964941
67.235
batch start
#iterations: 60
currently lose_sum: 100.50611066818237
time_elpased: 2.097
batch start
#iterations: 61
currently lose_sum: 100.50603467226028
time_elpased: 2.082
batch start
#iterations: 62
currently lose_sum: 100.50593024492264
time_elpased: 2.078
batch start
#iterations: 63
currently lose_sum: 100.50575840473175
time_elpased: 2.079
batch start
#iterations: 64
currently lose_sum: 100.50537300109863
time_elpased: 2.077
batch start
#iterations: 65
currently lose_sum: 100.50467473268509
time_elpased: 2.062
batch start
#iterations: 66
currently lose_sum: 100.50302362442017
time_elpased: 2.153
batch start
#iterations: 67
currently lose_sum: 100.49274504184723
time_elpased: 2.121
batch start
#iterations: 68
currently lose_sum: 100.45035821199417
time_elpased: 2.076
batch start
#iterations: 69
currently lose_sum: 100.2827621102333
time_elpased: 2.102
batch start
#iterations: 70
currently lose_sum: 100.17390257120132
time_elpased: 2.119
batch start
#iterations: 71
currently lose_sum: 100.02863305807114
time_elpased: 2.109
batch start
#iterations: 72
currently lose_sum: 99.92723727226257
time_elpased: 2.183
batch start
#iterations: 73
currently lose_sum: 99.6233382821083
time_elpased: 2.066
batch start
#iterations: 74
currently lose_sum: 99.6938019990921
time_elpased: 2.1
batch start
#iterations: 75
currently lose_sum: 99.99092000722885
time_elpased: 2.133
batch start
#iterations: 76
currently lose_sum: 100.46374332904816
time_elpased: 2.127
batch start
#iterations: 77
currently lose_sum: 100.42235231399536
time_elpased: 2.08
batch start
#iterations: 78
currently lose_sum: 99.91751790046692
time_elpased: 2.094
batch start
#iterations: 79
currently lose_sum: 99.59633904695511
time_elpased: 2.136
start validation test
0.573453608247
0.543158146774
0.933827312957
0.6868258714
0.572820916625
65.908
batch start
#iterations: 80
currently lose_sum: 99.43270033597946
time_elpased: 2.117
batch start
#iterations: 81
currently lose_sum: 99.5781517624855
time_elpased: 2.072
batch start
#iterations: 82
currently lose_sum: 99.72417265176773
time_elpased: 2.085
batch start
#iterations: 83
currently lose_sum: 100.42154145240784
time_elpased: 2.074
batch start
#iterations: 84
currently lose_sum: 99.77463275194168
time_elpased: 2.048
batch start
#iterations: 85
currently lose_sum: 99.53204220533371
time_elpased: 2.087
batch start
#iterations: 86
currently lose_sum: 100.48055499792099
time_elpased: 2.124
batch start
#iterations: 87
currently lose_sum: 99.78803318738937
time_elpased: 2.078
batch start
#iterations: 88
currently lose_sum: 100.05353045463562
time_elpased: 2.113
batch start
#iterations: 89
currently lose_sum: 99.93145817518234
time_elpased: 2.179
batch start
#iterations: 90
currently lose_sum: 100.50630128383636
time_elpased: 2.102
batch start
#iterations: 91
currently lose_sum: 100.50583124160767
time_elpased: 2.089
batch start
#iterations: 92
currently lose_sum: 100.50594687461853
time_elpased: 2.061
batch start
#iterations: 93
currently lose_sum: 100.50566637516022
time_elpased: 2.112
batch start
#iterations: 94
currently lose_sum: 100.5055605173111
time_elpased: 2.075
batch start
#iterations: 95
currently lose_sum: 100.50549471378326
time_elpased: 2.134
batch start
#iterations: 96
currently lose_sum: 100.50519770383835
time_elpased: 2.081
batch start
#iterations: 97
currently lose_sum: 100.50452524423599
time_elpased: 2.116
batch start
#iterations: 98
currently lose_sum: 100.50383657217026
time_elpased: 2.136
batch start
#iterations: 99
currently lose_sum: 100.49946010112762
time_elpased: 2.124
start validation test
0.535773195876
0.538873701476
0.507152413296
0.522532075072
0.535823444075
67.223
batch start
#iterations: 100
currently lose_sum: 100.4501017332077
time_elpased: 2.071
batch start
#iterations: 101
currently lose_sum: 100.01549220085144
time_elpased: 2.073
batch start
#iterations: 102
currently lose_sum: 99.68143308162689
time_elpased: 2.109
batch start
#iterations: 103
currently lose_sum: 100.69072949886322
time_elpased: 2.062
batch start
#iterations: 104
currently lose_sum: 100.50545656681061
time_elpased: 2.126
batch start
#iterations: 105
currently lose_sum: 100.50496727228165
time_elpased: 2.092
batch start
#iterations: 106
currently lose_sum: 100.5042809844017
time_elpased: 2.09
batch start
#iterations: 107
currently lose_sum: 100.50373530387878
time_elpased: 2.058
batch start
#iterations: 108
currently lose_sum: 100.50244402885437
time_elpased: 2.059
batch start
#iterations: 109
currently lose_sum: 100.49569767713547
time_elpased: 2.112
batch start
#iterations: 110
currently lose_sum: 100.26069647073746
time_elpased: 2.044
batch start
#iterations: 111
currently lose_sum: 100.36600458621979
time_elpased: 2.132
batch start
#iterations: 112
currently lose_sum: 100.07361328601837
time_elpased: 2.094
batch start
#iterations: 113
currently lose_sum: 100.35676389932632
time_elpased: 2.094
batch start
#iterations: 114
currently lose_sum: 100.50292068719864
time_elpased: 2.149
batch start
#iterations: 115
currently lose_sum: 100.50141263008118
time_elpased: 2.086
batch start
#iterations: 116
currently lose_sum: 100.49735569953918
time_elpased: 2.052
batch start
#iterations: 117
currently lose_sum: 100.46976017951965
time_elpased: 2.081
batch start
#iterations: 118
currently lose_sum: 99.89436858892441
time_elpased: 2.066
batch start
#iterations: 119
currently lose_sum: 100.5897746682167
time_elpased: 2.113
start validation test
0.522422680412
0.513725252034
0.870536173716
0.646144444869
0.521811513482
67.234
batch start
#iterations: 120
currently lose_sum: 100.5050710439682
time_elpased: 2.063
batch start
#iterations: 121
currently lose_sum: 100.5049564242363
time_elpased: 2.088
batch start
#iterations: 122
currently lose_sum: 100.50513523817062
time_elpased: 2.059
batch start
#iterations: 123
currently lose_sum: 100.50447082519531
time_elpased: 2.117
batch start
#iterations: 124
currently lose_sum: 100.50408154726028
time_elpased: 2.096
batch start
#iterations: 125
currently lose_sum: 100.5036124587059
time_elpased: 2.101
batch start
#iterations: 126
currently lose_sum: 100.50235718488693
time_elpased: 2.078
batch start
#iterations: 127
currently lose_sum: 100.49959760904312
time_elpased: 2.05
batch start
#iterations: 128
currently lose_sum: 100.46152704954147
time_elpased: 2.059
batch start
#iterations: 129
currently lose_sum: 99.85182815790176
time_elpased: 2.128
batch start
#iterations: 130
currently lose_sum: 99.72467052936554
time_elpased: 2.073
batch start
#iterations: 131
currently lose_sum: 99.66333734989166
time_elpased: 2.097
batch start
#iterations: 132
currently lose_sum: 99.81165462732315
time_elpased: 2.056
batch start
#iterations: 133
currently lose_sum: 100.50540119409561
time_elpased: 2.08
batch start
#iterations: 134
currently lose_sum: 100.50363177061081
time_elpased: 2.127
batch start
#iterations: 135
currently lose_sum: 100.50076133012772
time_elpased: 2.084
batch start
#iterations: 136
currently lose_sum: 100.48903077840805
time_elpased: 2.074
batch start
#iterations: 137
currently lose_sum: 100.01250034570694
time_elpased: 2.136
batch start
#iterations: 138
currently lose_sum: 99.57823956012726
time_elpased: 2.09
batch start
#iterations: 139
currently lose_sum: 100.46281254291534
time_elpased: 2.073
start validation test
0.50381443299
0.502365233664
0.994545641659
0.667541617738
0.502952878663
67.235
batch start
#iterations: 140
currently lose_sum: 100.50600761175156
time_elpased: 2.069
batch start
#iterations: 141
currently lose_sum: 100.50591826438904
time_elpased: 2.127
batch start
#iterations: 142
currently lose_sum: 100.50603365898132
time_elpased: 2.119
batch start
#iterations: 143
currently lose_sum: 100.50594902038574
time_elpased: 2.064
batch start
#iterations: 144
currently lose_sum: 100.50580829381943
time_elpased: 2.076
batch start
#iterations: 145
currently lose_sum: 100.5057464838028
time_elpased: 2.052
batch start
#iterations: 146
currently lose_sum: 100.50542587041855
time_elpased: 2.167
batch start
#iterations: 147
currently lose_sum: 100.50538420677185
time_elpased: 2.115
batch start
#iterations: 148
currently lose_sum: 100.50507432222366
time_elpased: 2.083
batch start
#iterations: 149
currently lose_sum: 100.50423783063889
time_elpased: 2.089
batch start
#iterations: 150
currently lose_sum: 100.50318521261215
time_elpased: 2.142
batch start
#iterations: 151
currently lose_sum: 100.501673579216
time_elpased: 2.084
batch start
#iterations: 152
currently lose_sum: 100.48055583238602
time_elpased: 2.137
batch start
#iterations: 153
currently lose_sum: 100.04295319318771
time_elpased: 2.085
batch start
#iterations: 154
currently lose_sum: 99.78894853591919
time_elpased: 2.074
batch start
#iterations: 155
currently lose_sum: 99.36512726545334
time_elpased: 2.127
batch start
#iterations: 156
currently lose_sum: 99.96366333961487
time_elpased: 2.071
batch start
#iterations: 157
currently lose_sum: 100.50250124931335
time_elpased: 2.097
batch start
#iterations: 158
currently lose_sum: 100.49846440553665
time_elpased: 2.148
batch start
#iterations: 159
currently lose_sum: 100.44801157712936
time_elpased: 2.112
start validation test
0.596237113402
0.609433085502
0.539878563343
0.572551159618
0.596336059529
66.755
batch start
#iterations: 160
currently lose_sum: 99.8387690782547
time_elpased: 2.106
batch start
#iterations: 161
currently lose_sum: 99.4965672492981
time_elpased: 2.093
batch start
#iterations: 162
currently lose_sum: 99.49314492940903
time_elpased: 2.207
batch start
#iterations: 163
currently lose_sum: 100.51414287090302
time_elpased: 2.107
batch start
#iterations: 164
currently lose_sum: 100.50553393363953
time_elpased: 2.094
batch start
#iterations: 165
currently lose_sum: 100.50514763593674
time_elpased: 2.082
batch start
#iterations: 166
currently lose_sum: 100.50479185581207
time_elpased: 2.082
batch start
#iterations: 167
currently lose_sum: 100.50408220291138
time_elpased: 2.113
batch start
#iterations: 168
currently lose_sum: 100.5026051402092
time_elpased: 2.062
batch start
#iterations: 169
currently lose_sum: 100.50037801265717
time_elpased: 2.106
batch start
#iterations: 170
currently lose_sum: 100.47038805484772
time_elpased: 2.084
batch start
#iterations: 171
currently lose_sum: 99.56651955842972
time_elpased: 2.111
batch start
#iterations: 172
currently lose_sum: 100.29345721006393
time_elpased: 2.082
batch start
#iterations: 173
currently lose_sum: 100.50585079193115
time_elpased: 2.045
batch start
#iterations: 174
currently lose_sum: 100.50573593378067
time_elpased: 2.095
batch start
#iterations: 175
currently lose_sum: 100.50549668073654
time_elpased: 2.063
batch start
#iterations: 176
currently lose_sum: 100.50524830818176
time_elpased: 2.115
batch start
#iterations: 177
currently lose_sum: 100.50494754314423
time_elpased: 2.099
batch start
#iterations: 178
currently lose_sum: 100.50441974401474
time_elpased: 2.107
batch start
#iterations: 179
currently lose_sum: 100.50332868099213
time_elpased: 2.06
start validation test
0.549948453608
0.546317173995
0.598435731193
0.571190020137
0.549863326714
67.231
batch start
#iterations: 180
currently lose_sum: 100.5001203417778
time_elpased: 2.12
batch start
#iterations: 181
currently lose_sum: 100.47738552093506
time_elpased: 2.132
batch start
#iterations: 182
currently lose_sum: 99.80259382724762
time_elpased: 2.069
batch start
#iterations: 183
currently lose_sum: 100.57290142774582
time_elpased: 2.059
batch start
#iterations: 184
currently lose_sum: 100.48978233337402
time_elpased: 2.073
batch start
#iterations: 185
currently lose_sum: 100.49156874418259
time_elpased: 2.079
batch start
#iterations: 186
currently lose_sum: 100.53718107938766
time_elpased: 2.122
batch start
#iterations: 187
currently lose_sum: 100.3925170302391
time_elpased: 2.102
batch start
#iterations: 188
currently lose_sum: 99.56560564041138
time_elpased: 2.1
batch start
#iterations: 189
currently lose_sum: 99.11392146348953
time_elpased: 2.053
batch start
#iterations: 190
currently lose_sum: 99.62773144245148
time_elpased: 2.092
batch start
#iterations: 191
currently lose_sum: 99.2958116531372
time_elpased: 2.085
batch start
#iterations: 192
currently lose_sum: 98.67840611934662
time_elpased: 2.079
batch start
#iterations: 193
currently lose_sum: 99.52013754844666
time_elpased: 2.121
batch start
#iterations: 194
currently lose_sum: 100.4857549071312
time_elpased: 2.07
batch start
#iterations: 195
currently lose_sum: 99.23754858970642
time_elpased: 2.079
batch start
#iterations: 196
currently lose_sum: 99.1300840973854
time_elpased: 2.045
batch start
#iterations: 197
currently lose_sum: 98.54746103286743
time_elpased: 2.059
batch start
#iterations: 198
currently lose_sum: 98.72248244285583
time_elpased: 2.044
batch start
#iterations: 199
currently lose_sum: 100.23792511224747
time_elpased: 2.079
start validation test
0.60793814433
0.654561429199
0.460018524236
0.540311857851
0.608197840038
65.574
batch start
#iterations: 200
currently lose_sum: 98.55263721942902
time_elpased: 2.092
batch start
#iterations: 201
currently lose_sum: 99.1139310002327
time_elpased: 2.113
batch start
#iterations: 202
currently lose_sum: 98.56210148334503
time_elpased: 2.086
batch start
#iterations: 203
currently lose_sum: 98.3674076795578
time_elpased: 2.102
batch start
#iterations: 204
currently lose_sum: 98.03257417678833
time_elpased: 2.088
batch start
#iterations: 205
currently lose_sum: 99.83383899927139
time_elpased: 2.07
batch start
#iterations: 206
currently lose_sum: 98.06496357917786
time_elpased: 2.102
batch start
#iterations: 207
currently lose_sum: 99.62723362445831
time_elpased: 2.097
batch start
#iterations: 208
currently lose_sum: 99.89677882194519
time_elpased: 2.071
batch start
#iterations: 209
currently lose_sum: 98.23549526929855
time_elpased: 2.055
batch start
#iterations: 210
currently lose_sum: 99.31775790452957
time_elpased: 2.06
batch start
#iterations: 211
currently lose_sum: 98.68927997350693
time_elpased: 2.128
batch start
#iterations: 212
currently lose_sum: 99.29759740829468
time_elpased: 2.096
batch start
#iterations: 213
currently lose_sum: 98.35997396707535
time_elpased: 2.082
batch start
#iterations: 214
currently lose_sum: 98.82574909925461
time_elpased: 2.101
batch start
#iterations: 215
currently lose_sum: 100.50875079631805
time_elpased: 2.065
batch start
#iterations: 216
currently lose_sum: 100.50256133079529
time_elpased: 2.117
batch start
#iterations: 217
currently lose_sum: 100.49713468551636
time_elpased: 2.058
batch start
#iterations: 218
currently lose_sum: 100.13266515731812
time_elpased: 2.107
batch start
#iterations: 219
currently lose_sum: 99.64080333709717
time_elpased: 2.115
start validation test
0.625979381443
0.642237891573
0.571781414017
0.604965156794
0.626074534335
66.767
batch start
#iterations: 220
currently lose_sum: 98.32696634531021
time_elpased: 2.12
batch start
#iterations: 221
currently lose_sum: 98.19949340820312
time_elpased: 2.118
batch start
#iterations: 222
currently lose_sum: 98.82874804735184
time_elpased: 2.067
batch start
#iterations: 223
currently lose_sum: 99.25750601291656
time_elpased: 2.074
batch start
#iterations: 224
currently lose_sum: 97.66479814052582
time_elpased: 2.123
batch start
#iterations: 225
currently lose_sum: 99.41198390722275
time_elpased: 2.08
batch start
#iterations: 226
currently lose_sum: 99.09829068183899
time_elpased: 2.087
batch start
#iterations: 227
currently lose_sum: 99.48513668775558
time_elpased: 2.058
batch start
#iterations: 228
currently lose_sum: 99.51579409837723
time_elpased: 2.131
batch start
#iterations: 229
currently lose_sum: 99.16887879371643
time_elpased: 2.072
batch start
#iterations: 230
currently lose_sum: 97.97096997499466
time_elpased: 2.102
batch start
#iterations: 231
currently lose_sum: 99.33222138881683
time_elpased: 2.083
batch start
#iterations: 232
currently lose_sum: 99.33537566661835
time_elpased: 2.085
batch start
#iterations: 233
currently lose_sum: 98.35731518268585
time_elpased: 2.06
batch start
#iterations: 234
currently lose_sum: 100.25822675228119
time_elpased: 2.101
batch start
#iterations: 235
currently lose_sum: 99.55360901355743
time_elpased: 2.099
batch start
#iterations: 236
currently lose_sum: 100.4978819489479
time_elpased: 2.097
batch start
#iterations: 237
currently lose_sum: 100.22741287946701
time_elpased: 2.055
batch start
#iterations: 238
currently lose_sum: 98.00370407104492
time_elpased: 2.098
batch start
#iterations: 239
currently lose_sum: 99.50840497016907
time_elpased: 2.083
start validation test
0.49912371134
0.0
0.0
nan
0.5
67.190
acc: 0.607
pre: 0.654
rec: 0.456
F1: 0.537
auc: 0.607
