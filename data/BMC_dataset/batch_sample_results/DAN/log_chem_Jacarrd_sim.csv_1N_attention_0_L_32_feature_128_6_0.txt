start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.4891750216484
time_elpased: 2.323
batch start
#iterations: 1
currently lose_sum: 100.44885605573654
time_elpased: 2.307
batch start
#iterations: 2
currently lose_sum: 100.4165745973587
time_elpased: 2.327
batch start
#iterations: 3
currently lose_sum: 100.44869250059128
time_elpased: 2.392
batch start
#iterations: 4
currently lose_sum: 100.41010522842407
time_elpased: 2.356
batch start
#iterations: 5
currently lose_sum: 100.47409671545029
time_elpased: 2.353
batch start
#iterations: 6
currently lose_sum: 100.40747940540314
time_elpased: 2.317
batch start
#iterations: 7
currently lose_sum: 100.31316673755646
time_elpased: 2.281
batch start
#iterations: 8
currently lose_sum: 100.27884119749069
time_elpased: 2.304
batch start
#iterations: 9
currently lose_sum: 100.24264466762543
time_elpased: 2.306
batch start
#iterations: 10
currently lose_sum: 100.17776906490326
time_elpased: 2.272
batch start
#iterations: 11
currently lose_sum: 100.24330240488052
time_elpased: 2.332
batch start
#iterations: 12
currently lose_sum: 100.520363509655
time_elpased: 2.324
batch start
#iterations: 13
currently lose_sum: 100.31344175338745
time_elpased: 2.306
batch start
#iterations: 14
currently lose_sum: 100.40506559610367
time_elpased: 2.27
batch start
#iterations: 15
currently lose_sum: 100.16952657699585
time_elpased: 2.285
batch start
#iterations: 16
currently lose_sum: 100.09086257219315
time_elpased: 2.305
batch start
#iterations: 17
currently lose_sum: 100.22160029411316
time_elpased: 2.3
batch start
#iterations: 18
currently lose_sum: 100.4213325381279
time_elpased: 2.283
batch start
#iterations: 19
currently lose_sum: 99.99388617277145
time_elpased: 2.299
start validation test
0.541340206186
0.632911392405
0.200679221982
0.304735114862
0.541938289087
66.549
batch start
#iterations: 20
currently lose_sum: 100.02638077735901
time_elpased: 2.298
batch start
#iterations: 21
currently lose_sum: 99.99628514051437
time_elpased: 2.321
batch start
#iterations: 22
currently lose_sum: 99.94756466150284
time_elpased: 2.335
batch start
#iterations: 23
currently lose_sum: 99.83953392505646
time_elpased: 2.292
batch start
#iterations: 24
currently lose_sum: 99.94509202241898
time_elpased: 2.296
batch start
#iterations: 25
currently lose_sum: 99.87668496370316
time_elpased: 2.309
batch start
#iterations: 26
currently lose_sum: 100.507976770401
time_elpased: 2.307
batch start
#iterations: 27
currently lose_sum: 100.25995922088623
time_elpased: 2.33
batch start
#iterations: 28
currently lose_sum: 99.88284170627594
time_elpased: 2.298
batch start
#iterations: 29
currently lose_sum: 100.36981576681137
time_elpased: 2.357
batch start
#iterations: 30
currently lose_sum: 99.93612325191498
time_elpased: 2.314
batch start
#iterations: 31
currently lose_sum: 99.99309712648392
time_elpased: 2.343
batch start
#iterations: 32
currently lose_sum: 100.00258827209473
time_elpased: 2.269
batch start
#iterations: 33
currently lose_sum: 99.89437222480774
time_elpased: 2.265
batch start
#iterations: 34
currently lose_sum: 100.47849529981613
time_elpased: 2.279
batch start
#iterations: 35
currently lose_sum: 100.03285801410675
time_elpased: 2.302
batch start
#iterations: 36
currently lose_sum: 99.86025041341782
time_elpased: 2.298
batch start
#iterations: 37
currently lose_sum: 100.28904539346695
time_elpased: 2.295
batch start
#iterations: 38
currently lose_sum: 100.49367302656174
time_elpased: 2.328
batch start
#iterations: 39
currently lose_sum: 100.29067277908325
time_elpased: 2.305
start validation test
0.592731958763
0.623068582272
0.47308840177
0.537818075461
0.592942011481
65.564
batch start
#iterations: 40
currently lose_sum: 100.08949762582779
time_elpased: 2.315
batch start
#iterations: 41
currently lose_sum: 100.13637501001358
time_elpased: 2.29
batch start
#iterations: 42
currently lose_sum: 100.21722567081451
time_elpased: 2.298
batch start
#iterations: 43
currently lose_sum: 100.18379974365234
time_elpased: 2.283
batch start
#iterations: 44
currently lose_sum: 99.73161005973816
time_elpased: 2.291
batch start
#iterations: 45
currently lose_sum: 100.28139090538025
time_elpased: 2.257
batch start
#iterations: 46
currently lose_sum: 100.50508481264114
time_elpased: 2.344
batch start
#iterations: 47
currently lose_sum: 100.50074565410614
time_elpased: 2.313
batch start
#iterations: 48
currently lose_sum: 100.34367001056671
time_elpased: 2.301
batch start
#iterations: 49
currently lose_sum: 100.230319917202
time_elpased: 2.271
batch start
#iterations: 50
currently lose_sum: 100.19167655706406
time_elpased: 2.313
batch start
#iterations: 51
currently lose_sum: 100.25166827440262
time_elpased: 2.304
batch start
#iterations: 52
currently lose_sum: 99.90891873836517
time_elpased: 2.3
batch start
#iterations: 53
currently lose_sum: 100.06045114994049
time_elpased: 2.307
batch start
#iterations: 54
currently lose_sum: 100.00781548023224
time_elpased: 2.292
batch start
#iterations: 55
currently lose_sum: 99.8114247918129
time_elpased: 2.315
batch start
#iterations: 56
currently lose_sum: 100.34053683280945
time_elpased: 2.261
batch start
#iterations: 57
currently lose_sum: 100.5007501244545
time_elpased: 2.301
batch start
#iterations: 58
currently lose_sum: 100.47966504096985
time_elpased: 2.262
batch start
#iterations: 59
currently lose_sum: 99.8345662355423
time_elpased: 2.325
start validation test
0.529329896907
0.646646646647
0.132962848616
0.220571916347
0.530025780396
66.787
batch start
#iterations: 60
currently lose_sum: 100.15399223566055
time_elpased: 2.273
batch start
#iterations: 61
currently lose_sum: 100.24900186061859
time_elpased: 2.33
batch start
#iterations: 62
currently lose_sum: 99.8563796877861
time_elpased: 2.258
batch start
#iterations: 63
currently lose_sum: 100.50622320175171
time_elpased: 2.306
batch start
#iterations: 64
currently lose_sum: 100.5058564543724
time_elpased: 2.266
batch start
#iterations: 65
currently lose_sum: 100.50563341379166
time_elpased: 2.286
batch start
#iterations: 66
currently lose_sum: 100.50506484508514
time_elpased: 2.299
batch start
#iterations: 67
currently lose_sum: 100.50485318899155
time_elpased: 2.273
batch start
#iterations: 68
currently lose_sum: 100.50336599349976
time_elpased: 2.264
batch start
#iterations: 69
currently lose_sum: 100.49976509809494
time_elpased: 2.309
batch start
#iterations: 70
currently lose_sum: 100.42778182029724
time_elpased: 2.333
batch start
#iterations: 71
currently lose_sum: 100.26837092638016
time_elpased: 2.251
batch start
#iterations: 72
currently lose_sum: 100.4370756149292
time_elpased: 2.291
batch start
#iterations: 73
currently lose_sum: 100.45966422557831
time_elpased: 2.324
batch start
#iterations: 74
currently lose_sum: 100.47316771745682
time_elpased: 2.321
batch start
#iterations: 75
currently lose_sum: 100.3207083940506
time_elpased: 2.359
batch start
#iterations: 76
currently lose_sum: 100.50398230552673
time_elpased: 2.314
batch start
#iterations: 77
currently lose_sum: 100.50102370977402
time_elpased: 2.253
batch start
#iterations: 78
currently lose_sum: 100.48780608177185
time_elpased: 2.241
batch start
#iterations: 79
currently lose_sum: 100.34519273042679
time_elpased: 2.309
start validation test
0.49912371134
0.0
0.0
nan
0.5
67.227
acc: 0.592
pre: 0.621
rec: 0.474
F1: 0.538
auc: 0.592
