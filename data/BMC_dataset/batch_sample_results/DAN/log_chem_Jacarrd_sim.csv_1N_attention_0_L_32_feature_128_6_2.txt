start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.47690540552139
time_elpased: 2.341
batch start
#iterations: 1
currently lose_sum: 100.43480885028839
time_elpased: 2.241
batch start
#iterations: 2
currently lose_sum: 100.42057973146439
time_elpased: 2.225
batch start
#iterations: 3
currently lose_sum: 100.41603660583496
time_elpased: 2.33
batch start
#iterations: 4
currently lose_sum: 100.39862436056137
time_elpased: 2.275
batch start
#iterations: 5
currently lose_sum: 100.31438201665878
time_elpased: 2.237
batch start
#iterations: 6
currently lose_sum: 100.25810503959656
time_elpased: 2.268
batch start
#iterations: 7
currently lose_sum: 100.17832505702972
time_elpased: 2.252
batch start
#iterations: 8
currently lose_sum: 100.20586717128754
time_elpased: 2.272
batch start
#iterations: 9
currently lose_sum: 100.25771635770798
time_elpased: 2.243
batch start
#iterations: 10
currently lose_sum: 100.17080384492874
time_elpased: 2.258
batch start
#iterations: 11
currently lose_sum: 100.09660279750824
time_elpased: 2.28
batch start
#iterations: 12
currently lose_sum: 100.28583079576492
time_elpased: 2.262
batch start
#iterations: 13
currently lose_sum: 100.04463863372803
time_elpased: 2.301
batch start
#iterations: 14
currently lose_sum: 99.91683566570282
time_elpased: 2.263
batch start
#iterations: 15
currently lose_sum: 99.85510885715485
time_elpased: 2.286
batch start
#iterations: 16
currently lose_sum: 99.89441341161728
time_elpased: 2.243
batch start
#iterations: 17
currently lose_sum: 100.11999434232712
time_elpased: 2.256
batch start
#iterations: 18
currently lose_sum: 99.95011293888092
time_elpased: 2.272
batch start
#iterations: 19
currently lose_sum: 99.8262990117073
time_elpased: 2.239
start validation test
0.565206185567
0.541344169247
0.863743953895
0.665556480711
0.564682056468
66.162
batch start
#iterations: 20
currently lose_sum: 100.12756699323654
time_elpased: 2.312
batch start
#iterations: 21
currently lose_sum: 99.84557622671127
time_elpased: 2.22
batch start
#iterations: 22
currently lose_sum: 99.68142437934875
time_elpased: 2.297
batch start
#iterations: 23
currently lose_sum: 99.965178668499
time_elpased: 2.246
batch start
#iterations: 24
currently lose_sum: 100.26827263832092
time_elpased: 2.253
batch start
#iterations: 25
currently lose_sum: 99.82317066192627
time_elpased: 2.265
batch start
#iterations: 26
currently lose_sum: 100.04793751239777
time_elpased: 2.245
batch start
#iterations: 27
currently lose_sum: 99.92119145393372
time_elpased: 2.231
batch start
#iterations: 28
currently lose_sum: 100.01359808444977
time_elpased: 2.27
batch start
#iterations: 29
currently lose_sum: 100.18689751625061
time_elpased: 2.232
batch start
#iterations: 30
currently lose_sum: 99.68295216560364
time_elpased: 2.233
batch start
#iterations: 31
currently lose_sum: 99.70713871717453
time_elpased: 2.221
batch start
#iterations: 32
currently lose_sum: 99.98361730575562
time_elpased: 2.251
batch start
#iterations: 33
currently lose_sum: 99.86577653884888
time_elpased: 2.264
batch start
#iterations: 34
currently lose_sum: 100.25335133075714
time_elpased: 2.234
batch start
#iterations: 35
currently lose_sum: 99.7517300248146
time_elpased: 2.247
batch start
#iterations: 36
currently lose_sum: 100.35081708431244
time_elpased: 2.243
batch start
#iterations: 37
currently lose_sum: 100.50559759140015
time_elpased: 2.234
batch start
#iterations: 38
currently lose_sum: 100.50041723251343
time_elpased: 2.233
batch start
#iterations: 39
currently lose_sum: 100.40320974588394
time_elpased: 2.274
start validation test
0.578659793814
0.554205016511
0.811773181023
0.658705636743
0.578250527308
66.196
batch start
#iterations: 40
currently lose_sum: 100.02094006538391
time_elpased: 2.222
batch start
#iterations: 41
currently lose_sum: 100.5051549077034
time_elpased: 2.229
batch start
#iterations: 42
currently lose_sum: 99.86766177415848
time_elpased: 2.252
batch start
#iterations: 43
currently lose_sum: 99.82983636856079
time_elpased: 2.207
batch start
#iterations: 44
currently lose_sum: 99.89117175340652
time_elpased: 2.238
batch start
#iterations: 45
currently lose_sum: 100.30915147066116
time_elpased: 2.264
batch start
#iterations: 46
currently lose_sum: 99.63008719682693
time_elpased: 2.252
batch start
#iterations: 47
currently lose_sum: 99.96191120147705
time_elpased: 2.305
batch start
#iterations: 48
currently lose_sum: 99.93412601947784
time_elpased: 2.267
batch start
#iterations: 49
currently lose_sum: 100.04290616512299
time_elpased: 2.275
batch start
#iterations: 50
currently lose_sum: 100.14780348539352
time_elpased: 2.252
batch start
#iterations: 51
currently lose_sum: 100.17676782608032
time_elpased: 2.316
batch start
#iterations: 52
currently lose_sum: 99.79039931297302
time_elpased: 2.285
batch start
#iterations: 53
currently lose_sum: 100.26516002416611
time_elpased: 2.245
batch start
#iterations: 54
currently lose_sum: 99.41171199083328
time_elpased: 2.248
batch start
#iterations: 55
currently lose_sum: 100.0012012720108
time_elpased: 2.251
batch start
#iterations: 56
currently lose_sum: 99.71786522865295
time_elpased: 2.222
batch start
#iterations: 57
currently lose_sum: 99.88310980796814
time_elpased: 2.242
batch start
#iterations: 58
currently lose_sum: 99.26843601465225
time_elpased: 2.245
batch start
#iterations: 59
currently lose_sum: 99.92600136995316
time_elpased: 2.313
start validation test
0.608505154639
0.574929378531
0.83781002367
0.681911462914
0.608102574574
64.564
batch start
#iterations: 60
currently lose_sum: 100.20527046918869
time_elpased: 2.254
batch start
#iterations: 61
currently lose_sum: 99.65961825847626
time_elpased: 2.273
batch start
#iterations: 62
currently lose_sum: 100.4929751753807
time_elpased: 2.21
batch start
#iterations: 63
currently lose_sum: 99.88928681612015
time_elpased: 2.24
batch start
#iterations: 64
currently lose_sum: 99.33909392356873
time_elpased: 2.296
batch start
#iterations: 65
currently lose_sum: 99.89065545797348
time_elpased: 2.24
batch start
#iterations: 66
currently lose_sum: 99.63094413280487
time_elpased: 2.245
batch start
#iterations: 67
currently lose_sum: 99.85542595386505
time_elpased: 2.228
batch start
#iterations: 68
currently lose_sum: 100.46733886003494
time_elpased: 2.271
batch start
#iterations: 69
currently lose_sum: 99.94876432418823
time_elpased: 2.241
batch start
#iterations: 70
currently lose_sum: 100.06207275390625
time_elpased: 2.304
batch start
#iterations: 71
currently lose_sum: 100.36828488111496
time_elpased: 2.27
batch start
#iterations: 72
currently lose_sum: 99.90334790945053
time_elpased: 2.28
batch start
#iterations: 73
currently lose_sum: 99.84874773025513
time_elpased: 2.287
batch start
#iterations: 74
currently lose_sum: 100.02160847187042
time_elpased: 2.282
batch start
#iterations: 75
currently lose_sum: 100.50368744134903
time_elpased: 2.264
batch start
#iterations: 76
currently lose_sum: 100.50013250112534
time_elpased: 2.239
batch start
#iterations: 77
currently lose_sum: 100.47252416610718
time_elpased: 2.232
batch start
#iterations: 78
currently lose_sum: 100.0057760477066
time_elpased: 2.273
batch start
#iterations: 79
currently lose_sum: 99.47550916671753
time_elpased: 2.222
start validation test
0.510515463918
0.50580692627
0.990532057219
0.669658387254
0.509672720751
67.199
batch start
#iterations: 80
currently lose_sum: 100.27415424585342
time_elpased: 2.269
batch start
#iterations: 81
currently lose_sum: 99.2883670926094
time_elpased: 2.225
batch start
#iterations: 82
currently lose_sum: 99.7378830909729
time_elpased: 2.249
batch start
#iterations: 83
currently lose_sum: 99.74763685464859
time_elpased: 2.239
batch start
#iterations: 84
currently lose_sum: 100.25939989089966
time_elpased: 2.301
batch start
#iterations: 85
currently lose_sum: 99.31745845079422
time_elpased: 2.259
batch start
#iterations: 86
currently lose_sum: 100.49885189533234
time_elpased: 2.244
batch start
#iterations: 87
currently lose_sum: 100.5030762553215
time_elpased: 2.267
batch start
#iterations: 88
currently lose_sum: 100.49747735261917
time_elpased: 2.344
batch start
#iterations: 89
currently lose_sum: 100.23317790031433
time_elpased: 2.284
batch start
#iterations: 90
currently lose_sum: 100.09655368328094
time_elpased: 2.25
batch start
#iterations: 91
currently lose_sum: 100.48984146118164
time_elpased: 2.221
batch start
#iterations: 92
currently lose_sum: 100.02942889928818
time_elpased: 2.277
batch start
#iterations: 93
currently lose_sum: 99.3246728181839
time_elpased: 2.282
batch start
#iterations: 94
currently lose_sum: 100.31267040967941
time_elpased: 2.198
batch start
#iterations: 95
currently lose_sum: 100.4918497800827
time_elpased: 2.302
batch start
#iterations: 96
currently lose_sum: 99.81610673666
time_elpased: 2.233
batch start
#iterations: 97
currently lose_sum: 99.7779541015625
time_elpased: 2.226
batch start
#iterations: 98
currently lose_sum: 100.28458595275879
time_elpased: 2.234
batch start
#iterations: 99
currently lose_sum: 99.97558748722076
time_elpased: 2.212
start validation test
0.564226804124
0.66015724068
0.267881033241
0.381112737921
0.564747084833
65.385
batch start
#iterations: 100
currently lose_sum: 99.60562819242477
time_elpased: 2.196
batch start
#iterations: 101
currently lose_sum: 99.55100643634796
time_elpased: 2.223
batch start
#iterations: 102
currently lose_sum: 100.50036895275116
time_elpased: 2.292
batch start
#iterations: 103
currently lose_sum: 100.35728651285172
time_elpased: 2.271
batch start
#iterations: 104
currently lose_sum: 99.64293295145035
time_elpased: 2.242
batch start
#iterations: 105
currently lose_sum: 99.59714812040329
time_elpased: 2.25
batch start
#iterations: 106
currently lose_sum: 99.07791477441788
time_elpased: 2.223
batch start
#iterations: 107
currently lose_sum: 99.0491333603859
time_elpased: 2.237
batch start
#iterations: 108
currently lose_sum: 100.06463599205017
time_elpased: 2.3
batch start
#iterations: 109
currently lose_sum: 100.50669711828232
time_elpased: 2.238
batch start
#iterations: 110
currently lose_sum: 100.5060350894928
time_elpased: 2.277
batch start
#iterations: 111
currently lose_sum: 100.50537997484207
time_elpased: 2.241
batch start
#iterations: 112
currently lose_sum: 100.50374817848206
time_elpased: 2.285
batch start
#iterations: 113
currently lose_sum: 100.50115841627121
time_elpased: 2.224
batch start
#iterations: 114
currently lose_sum: 100.4543046951294
time_elpased: 2.233
batch start
#iterations: 115
currently lose_sum: 99.78816467523575
time_elpased: 2.259
batch start
#iterations: 116
currently lose_sum: 99.33333629369736
time_elpased: 2.22
batch start
#iterations: 117
currently lose_sum: 100.50447624921799
time_elpased: 2.243
batch start
#iterations: 118
currently lose_sum: 100.49547916650772
time_elpased: 2.259
batch start
#iterations: 119
currently lose_sum: 99.91021424531937
time_elpased: 2.265
start validation test
0.520670103093
0.511168109437
0.984460224349
0.67292743836
0.519855848
66.796
batch start
#iterations: 120
currently lose_sum: 99.2279800772667
time_elpased: 2.295
batch start
#iterations: 121
currently lose_sum: 100.4972557425499
time_elpased: 2.291
batch start
#iterations: 122
currently lose_sum: 100.5032622218132
time_elpased: 2.235
batch start
#iterations: 123
currently lose_sum: 100.49648374319077
time_elpased: 2.271
batch start
#iterations: 124
currently lose_sum: 100.06573992967606
time_elpased: 2.225
batch start
#iterations: 125
currently lose_sum: 100.38763296604156
time_elpased: 2.273
batch start
#iterations: 126
currently lose_sum: 100.50550937652588
time_elpased: 2.285
batch start
#iterations: 127
currently lose_sum: 100.50535327196121
time_elpased: 2.281
batch start
#iterations: 128
currently lose_sum: 100.5050260424614
time_elpased: 2.222
batch start
#iterations: 129
currently lose_sum: 100.50488644838333
time_elpased: 2.287
batch start
#iterations: 130
currently lose_sum: 100.50451308488846
time_elpased: 2.263
batch start
#iterations: 131
currently lose_sum: 100.50264793634415
time_elpased: 2.233
batch start
#iterations: 132
currently lose_sum: 100.49937891960144
time_elpased: 2.222
batch start
#iterations: 133
currently lose_sum: 100.29722934961319
time_elpased: 2.269
batch start
#iterations: 134
currently lose_sum: 100.06285989284515
time_elpased: 2.199
batch start
#iterations: 135
currently lose_sum: 99.91732496023178
time_elpased: 2.269
batch start
#iterations: 136
currently lose_sum: 100.50867569446564
time_elpased: 2.281
batch start
#iterations: 137
currently lose_sum: 100.5060943365097
time_elpased: 2.245
batch start
#iterations: 138
currently lose_sum: 100.5061252117157
time_elpased: 2.298
batch start
#iterations: 139
currently lose_sum: 100.50598412752151
time_elpased: 2.266
start validation test
0.534329896907
0.528003280033
0.662653082227
0.587714494341
0.534104605763
67.235
batch start
#iterations: 140
currently lose_sum: 100.50602167844772
time_elpased: 2.262
batch start
#iterations: 141
currently lose_sum: 100.50593692064285
time_elpased: 2.269
batch start
#iterations: 142
currently lose_sum: 100.5060840845108
time_elpased: 2.261
batch start
#iterations: 143
currently lose_sum: 100.50600689649582
time_elpased: 2.256
batch start
#iterations: 144
currently lose_sum: 100.50590890645981
time_elpased: 2.295
batch start
#iterations: 145
currently lose_sum: 100.50590199232101
time_elpased: 2.274
batch start
#iterations: 146
currently lose_sum: 100.50566935539246
time_elpased: 2.261
batch start
#iterations: 147
currently lose_sum: 100.50565540790558
time_elpased: 2.254
batch start
#iterations: 148
currently lose_sum: 100.50547581911087
time_elpased: 2.271
batch start
#iterations: 149
currently lose_sum: 100.50500565767288
time_elpased: 2.333
batch start
#iterations: 150
currently lose_sum: 100.50449275970459
time_elpased: 2.237
batch start
#iterations: 151
currently lose_sum: 100.50416040420532
time_elpased: 2.225
batch start
#iterations: 152
currently lose_sum: 100.5004072189331
time_elpased: 2.295
batch start
#iterations: 153
currently lose_sum: 100.43492221832275
time_elpased: 2.272
batch start
#iterations: 154
currently lose_sum: 100.24870002269745
time_elpased: 2.201
batch start
#iterations: 155
currently lose_sum: 100.50233721733093
time_elpased: 2.261
batch start
#iterations: 156
currently lose_sum: 100.47510975599289
time_elpased: 2.315
batch start
#iterations: 157
currently lose_sum: 100.01592856645584
time_elpased: 2.286
batch start
#iterations: 158
currently lose_sum: 99.78125804662704
time_elpased: 2.244
batch start
#iterations: 159
currently lose_sum: 100.09284269809723
time_elpased: 2.235
start validation test
0.529690721649
0.516648885395
0.946897190491
0.668531570152
0.528958251344
67.227
batch start
#iterations: 160
currently lose_sum: 100.48834681510925
time_elpased: 2.249
batch start
#iterations: 161
currently lose_sum: 99.7946464419365
time_elpased: 2.255
batch start
#iterations: 162
currently lose_sum: 99.41042333841324
time_elpased: 2.286
batch start
#iterations: 163
currently lose_sum: 100.5059842467308
time_elpased: 2.218
batch start
#iterations: 164
currently lose_sum: 100.50449407100677
time_elpased: 2.286
batch start
#iterations: 165
currently lose_sum: 100.50262761116028
time_elpased: 2.258
batch start
#iterations: 166
currently lose_sum: 100.49579232931137
time_elpased: 2.289
batch start
#iterations: 167
currently lose_sum: 99.88514345884323
time_elpased: 2.252
batch start
#iterations: 168
currently lose_sum: 99.69722932577133
time_elpased: 2.241
batch start
#iterations: 169
currently lose_sum: 99.46305495500565
time_elpased: 2.321
batch start
#iterations: 170
currently lose_sum: 99.7910692691803
time_elpased: 2.292
batch start
#iterations: 171
currently lose_sum: 100.25550198554993
time_elpased: 2.262
batch start
#iterations: 172
currently lose_sum: 99.46367704868317
time_elpased: 2.329
batch start
#iterations: 173
currently lose_sum: 99.29783743619919
time_elpased: 2.251
batch start
#iterations: 174
currently lose_sum: 99.92013573646545
time_elpased: 2.251
batch start
#iterations: 175
currently lose_sum: 100.49843859672546
time_elpased: 2.286
batch start
#iterations: 176
currently lose_sum: 99.73782300949097
time_elpased: 2.276
batch start
#iterations: 177
currently lose_sum: 99.96001213788986
time_elpased: 2.24
batch start
#iterations: 178
currently lose_sum: 99.75597411394119
time_elpased: 2.291
batch start
#iterations: 179
currently lose_sum: 99.17021489143372
time_elpased: 2.252
start validation test
0.49912371134
0.0
0.0
nan
0.5
67.177
acc: 0.614
pre: 0.579
rec: 0.840
F1: 0.686
auc: 0.614
