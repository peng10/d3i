start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.53615486621857
time_elpased: 1.736
batch start
#iterations: 1
currently lose_sum: 100.43310183286667
time_elpased: 1.708
batch start
#iterations: 2
currently lose_sum: 100.37550508975983
time_elpased: 1.738
batch start
#iterations: 3
currently lose_sum: 100.3682461977005
time_elpased: 1.772
batch start
#iterations: 4
currently lose_sum: 100.27771598100662
time_elpased: 1.766
batch start
#iterations: 5
currently lose_sum: 100.29131001234055
time_elpased: 1.713
batch start
#iterations: 6
currently lose_sum: 100.20617836713791
time_elpased: 1.797
batch start
#iterations: 7
currently lose_sum: 100.17098838090897
time_elpased: 1.739
batch start
#iterations: 8
currently lose_sum: 100.02295958995819
time_elpased: 1.786
batch start
#iterations: 9
currently lose_sum: 100.07445788383484
time_elpased: 1.778
batch start
#iterations: 10
currently lose_sum: 99.93888014554977
time_elpased: 1.76
batch start
#iterations: 11
currently lose_sum: 99.88054656982422
time_elpased: 1.718
batch start
#iterations: 12
currently lose_sum: 99.99844264984131
time_elpased: 1.811
batch start
#iterations: 13
currently lose_sum: 99.94512337446213
time_elpased: 1.768
batch start
#iterations: 14
currently lose_sum: 99.74453055858612
time_elpased: 1.749
batch start
#iterations: 15
currently lose_sum: 99.88714081048965
time_elpased: 1.744
batch start
#iterations: 16
currently lose_sum: 99.71924477815628
time_elpased: 1.728
batch start
#iterations: 17
currently lose_sum: 99.68235516548157
time_elpased: 1.79
batch start
#iterations: 18
currently lose_sum: 99.6375423669815
time_elpased: 1.75
batch start
#iterations: 19
currently lose_sum: 99.47842901945114
time_elpased: 1.777
start validation test
0.597886597938
0.582444061962
0.69651126891
0.634390964053
0.597713447116
65.939
batch start
#iterations: 20
currently lose_sum: 99.50047099590302
time_elpased: 1.756
batch start
#iterations: 21
currently lose_sum: 99.41249459981918
time_elpased: 1.758
batch start
#iterations: 22
currently lose_sum: 99.4666485786438
time_elpased: 1.804
batch start
#iterations: 23
currently lose_sum: 99.50617760419846
time_elpased: 1.701
batch start
#iterations: 24
currently lose_sum: 99.33910083770752
time_elpased: 1.737
batch start
#iterations: 25
currently lose_sum: 99.4687774181366
time_elpased: 1.702
batch start
#iterations: 26
currently lose_sum: 99.23325181007385
time_elpased: 1.768
batch start
#iterations: 27
currently lose_sum: 99.47579801082611
time_elpased: 1.735
batch start
#iterations: 28
currently lose_sum: 99.44596660137177
time_elpased: 1.778
batch start
#iterations: 29
currently lose_sum: 99.75841045379639
time_elpased: 1.73
batch start
#iterations: 30
currently lose_sum: 99.45504266023636
time_elpased: 1.742
batch start
#iterations: 31
currently lose_sum: 99.84985965490341
time_elpased: 1.721
batch start
#iterations: 32
currently lose_sum: 98.9989567399025
time_elpased: 1.736
batch start
#iterations: 33
currently lose_sum: 99.65700727701187
time_elpased: 1.734
batch start
#iterations: 34
currently lose_sum: 99.58434563875198
time_elpased: 1.736
batch start
#iterations: 35
currently lose_sum: 99.29508006572723
time_elpased: 1.734
batch start
#iterations: 36
currently lose_sum: 98.88038289546967
time_elpased: 1.784
batch start
#iterations: 37
currently lose_sum: 99.4328624010086
time_elpased: 1.749
batch start
#iterations: 38
currently lose_sum: 99.22863060235977
time_elpased: 1.761
batch start
#iterations: 39
currently lose_sum: 99.2145739197731
time_elpased: 1.75
start validation test
0.516443298969
0.508893594494
0.989297108161
0.67207326878
0.515613131174
66.886
batch start
#iterations: 40
currently lose_sum: 99.67950052022934
time_elpased: 1.71
batch start
#iterations: 41
currently lose_sum: 98.86707895994186
time_elpased: 1.794
batch start
#iterations: 42
currently lose_sum: 99.27684205770493
time_elpased: 1.724
batch start
#iterations: 43
currently lose_sum: 99.82343155145645
time_elpased: 1.769
batch start
#iterations: 44
currently lose_sum: 98.99187725782394
time_elpased: 1.775
batch start
#iterations: 45
currently lose_sum: 99.12676322460175
time_elpased: 1.733
batch start
#iterations: 46
currently lose_sum: 99.5103012919426
time_elpased: 1.793
batch start
#iterations: 47
currently lose_sum: 100.07654023170471
time_elpased: 1.768
batch start
#iterations: 48
currently lose_sum: 99.53276342153549
time_elpased: 1.745
batch start
#iterations: 49
currently lose_sum: 98.90376389026642
time_elpased: 1.755
batch start
#iterations: 50
currently lose_sum: 99.75447994470596
time_elpased: 1.724
batch start
#iterations: 51
currently lose_sum: 100.1532312631607
time_elpased: 1.716
batch start
#iterations: 52
currently lose_sum: 99.57662910223007
time_elpased: 1.822
batch start
#iterations: 53
currently lose_sum: 99.66761648654938
time_elpased: 1.772
batch start
#iterations: 54
currently lose_sum: 100.50171720981598
time_elpased: 1.756
batch start
#iterations: 55
currently lose_sum: 100.49782633781433
time_elpased: 1.751
batch start
#iterations: 56
currently lose_sum: 100.48883563280106
time_elpased: 1.73
batch start
#iterations: 57
currently lose_sum: 100.43065702915192
time_elpased: 1.72
batch start
#iterations: 58
currently lose_sum: 99.44036746025085
time_elpased: 1.752
batch start
#iterations: 59
currently lose_sum: 100.27371442317963
time_elpased: 1.723
start validation test
0.585309278351
0.55487724826
0.869918699187
0.677568033345
0.584809602614
67.119
batch start
#iterations: 60
currently lose_sum: 99.81776851415634
time_elpased: 1.75
batch start
#iterations: 61
currently lose_sum: 99.67533314228058
time_elpased: 1.765
batch start
#iterations: 62
currently lose_sum: 99.6207886338234
time_elpased: 1.752
batch start
#iterations: 63
currently lose_sum: 100.4982213973999
time_elpased: 1.731
batch start
#iterations: 64
currently lose_sum: 100.48522943258286
time_elpased: 1.721
batch start
#iterations: 65
currently lose_sum: 100.32215690612793
time_elpased: 1.731
batch start
#iterations: 66
currently lose_sum: 99.78713709115982
time_elpased: 1.777
batch start
#iterations: 67
currently lose_sum: 100.64484637975693
time_elpased: 1.695
batch start
#iterations: 68
currently lose_sum: 100.50571495294571
time_elpased: 1.746
batch start
#iterations: 69
currently lose_sum: 100.50579917430878
time_elpased: 1.738
batch start
#iterations: 70
currently lose_sum: 100.50560998916626
time_elpased: 1.711
batch start
#iterations: 71
currently lose_sum: 100.50569975376129
time_elpased: 1.692
batch start
#iterations: 72
currently lose_sum: 100.50567424297333
time_elpased: 1.79
batch start
#iterations: 73
currently lose_sum: 100.505455493927
time_elpased: 1.742
batch start
#iterations: 74
currently lose_sum: 100.50542622804642
time_elpased: 1.713
batch start
#iterations: 75
currently lose_sum: 100.50533163547516
time_elpased: 1.696
batch start
#iterations: 76
currently lose_sum: 100.50514620542526
time_elpased: 1.75
batch start
#iterations: 77
currently lose_sum: 100.5049192905426
time_elpased: 1.718
batch start
#iterations: 78
currently lose_sum: 100.50390756130219
time_elpased: 1.768
batch start
#iterations: 79
currently lose_sum: 100.50196319818497
time_elpased: 1.726
start validation test
0.55412371134
0.550268538585
0.601008541731
0.574520413182
0.554041397789
67.228
batch start
#iterations: 80
currently lose_sum: 100.49576330184937
time_elpased: 1.744
batch start
#iterations: 81
currently lose_sum: 100.44283550977707
time_elpased: 1.704
batch start
#iterations: 82
currently lose_sum: 99.98223751783371
time_elpased: 1.743
batch start
#iterations: 83
currently lose_sum: 99.81741774082184
time_elpased: 1.733
batch start
#iterations: 84
currently lose_sum: 99.98664450645447
time_elpased: 1.693
batch start
#iterations: 85
currently lose_sum: 99.60514426231384
time_elpased: 1.78
batch start
#iterations: 86
currently lose_sum: 100.75135976076126
time_elpased: 1.714
batch start
#iterations: 87
currently lose_sum: 100.50564336776733
time_elpased: 1.707
batch start
#iterations: 88
currently lose_sum: 100.5056044459343
time_elpased: 1.714
batch start
#iterations: 89
currently lose_sum: 100.50565385818481
time_elpased: 1.717
batch start
#iterations: 90
currently lose_sum: 100.50546431541443
time_elpased: 1.722
batch start
#iterations: 91
currently lose_sum: 100.50548249483109
time_elpased: 1.72
batch start
#iterations: 92
currently lose_sum: 100.5054383277893
time_elpased: 1.752
batch start
#iterations: 93
currently lose_sum: 100.50533384084702
time_elpased: 1.728
batch start
#iterations: 94
currently lose_sum: 100.50548183917999
time_elpased: 1.702
batch start
#iterations: 95
currently lose_sum: 100.50489288568497
time_elpased: 1.701
batch start
#iterations: 96
currently lose_sum: 100.50480508804321
time_elpased: 1.763
batch start
#iterations: 97
currently lose_sum: 100.5045753121376
time_elpased: 1.776
batch start
#iterations: 98
currently lose_sum: 100.50451529026031
time_elpased: 1.748
batch start
#iterations: 99
currently lose_sum: 100.50315111875534
time_elpased: 1.706
start validation test
0.540515463918
0.536122357175
0.613255119893
0.572100614439
0.540387758232
67.230
batch start
#iterations: 100
currently lose_sum: 100.49914181232452
time_elpased: 1.734
batch start
#iterations: 101
currently lose_sum: 100.47623234987259
time_elpased: 1.753
batch start
#iterations: 102
currently lose_sum: 100.146016061306
time_elpased: 1.72
batch start
#iterations: 103
currently lose_sum: 99.69938886165619
time_elpased: 1.689
batch start
#iterations: 104
currently lose_sum: 100.01120722293854
time_elpased: 1.752
batch start
#iterations: 105
currently lose_sum: 99.66613012552261
time_elpased: 1.7
batch start
#iterations: 106
currently lose_sum: 100.4394508600235
time_elpased: 1.759
batch start
#iterations: 107
currently lose_sum: 100.49729508161545
time_elpased: 1.704
batch start
#iterations: 108
currently lose_sum: 100.49059396982193
time_elpased: 1.712
batch start
#iterations: 109
currently lose_sum: 100.39603638648987
time_elpased: 1.759
batch start
#iterations: 110
currently lose_sum: 99.60803252458572
time_elpased: 1.764
batch start
#iterations: 111
currently lose_sum: 100.015695810318
time_elpased: 1.684
batch start
#iterations: 112
currently lose_sum: 100.50092720985413
time_elpased: 1.683
batch start
#iterations: 113
currently lose_sum: 100.49817728996277
time_elpased: 1.726
batch start
#iterations: 114
currently lose_sum: 100.48929715156555
time_elpased: 1.738
batch start
#iterations: 115
currently lose_sum: 100.39639788866043
time_elpased: 1.693
batch start
#iterations: 116
currently lose_sum: 99.31119960546494
time_elpased: 1.691
batch start
#iterations: 117
currently lose_sum: 99.23878729343414
time_elpased: 1.748
batch start
#iterations: 118
currently lose_sum: 100.09769612550735
time_elpased: 1.727
batch start
#iterations: 119
currently lose_sum: 100.23662978410721
time_elpased: 1.701
start validation test
0.55324742268
0.53685244981
0.787074199856
0.638317406001
0.552836903708
67.221
batch start
#iterations: 120
currently lose_sum: 100.48308479785919
time_elpased: 1.703
batch start
#iterations: 121
currently lose_sum: 100.24736261367798
time_elpased: 1.711
batch start
#iterations: 122
currently lose_sum: 99.66748541593552
time_elpased: 1.745
batch start
#iterations: 123
currently lose_sum: 100.27173155546188
time_elpased: 1.686
batch start
#iterations: 124
currently lose_sum: 99.63124346733093
time_elpased: 1.758
batch start
#iterations: 125
currently lose_sum: 99.52653086185455
time_elpased: 1.711
batch start
#iterations: 126
currently lose_sum: 100.56711971759796
time_elpased: 1.728
batch start
#iterations: 127
currently lose_sum: 99.6773619055748
time_elpased: 1.73
batch start
#iterations: 128
currently lose_sum: 99.2081647515297
time_elpased: 1.749
batch start
#iterations: 129
currently lose_sum: 99.88711273670197
time_elpased: 1.731
batch start
#iterations: 130
currently lose_sum: 100.5060288310051
time_elpased: 1.737
batch start
#iterations: 131
currently lose_sum: 100.50522965192795
time_elpased: 1.717
batch start
#iterations: 132
currently lose_sum: 100.50547194480896
time_elpased: 1.749
batch start
#iterations: 133
currently lose_sum: 100.5050283074379
time_elpased: 1.727
batch start
#iterations: 134
currently lose_sum: 100.50506526231766
time_elpased: 1.702
batch start
#iterations: 135
currently lose_sum: 100.5050978064537
time_elpased: 1.7
batch start
#iterations: 136
currently lose_sum: 100.50470340251923
time_elpased: 1.746
batch start
#iterations: 137
currently lose_sum: 100.50449001789093
time_elpased: 1.71
batch start
#iterations: 138
currently lose_sum: 100.50446546077728
time_elpased: 1.742
batch start
#iterations: 139
currently lose_sum: 100.50371956825256
time_elpased: 1.754
start validation test
0.549020618557
0.536341793062
0.735103426984
0.620186672455
0.548693921485
67.232
batch start
#iterations: 140
currently lose_sum: 100.50299268960953
time_elpased: 1.751
batch start
#iterations: 141
currently lose_sum: 100.5012868642807
time_elpased: 1.713
batch start
#iterations: 142
currently lose_sum: 100.49850529432297
time_elpased: 1.762
batch start
#iterations: 143
currently lose_sum: 100.47387230396271
time_elpased: 1.725
batch start
#iterations: 144
currently lose_sum: 100.1599513888359
time_elpased: 1.738
batch start
#iterations: 145
currently lose_sum: 100.0074103474617
time_elpased: 1.749
batch start
#iterations: 146
currently lose_sum: 99.88701444864273
time_elpased: 1.714
batch start
#iterations: 147
currently lose_sum: 99.81046748161316
time_elpased: 1.716
batch start
#iterations: 148
currently lose_sum: 99.54616189002991
time_elpased: 1.724
batch start
#iterations: 149
currently lose_sum: 100.52339351177216
time_elpased: 1.777
batch start
#iterations: 150
currently lose_sum: 100.50563335418701
time_elpased: 1.756
batch start
#iterations: 151
currently lose_sum: 100.50559139251709
time_elpased: 1.75
batch start
#iterations: 152
currently lose_sum: 100.50560706853867
time_elpased: 1.713
batch start
#iterations: 153
currently lose_sum: 100.50563472509384
time_elpased: 1.724
batch start
#iterations: 154
currently lose_sum: 100.50582492351532
time_elpased: 1.726
batch start
#iterations: 155
currently lose_sum: 100.50553387403488
time_elpased: 1.746
batch start
#iterations: 156
currently lose_sum: 100.50525456666946
time_elpased: 1.732
batch start
#iterations: 157
currently lose_sum: 100.50535637140274
time_elpased: 1.734
batch start
#iterations: 158
currently lose_sum: 100.50526076555252
time_elpased: 1.701
batch start
#iterations: 159
currently lose_sum: 100.50525623559952
time_elpased: 1.79
start validation test
0.525206185567
0.518076593312
0.746217968509
0.611563277527
0.524818165293
67.234
batch start
#iterations: 160
currently lose_sum: 100.50503265857697
time_elpased: 1.739
batch start
#iterations: 161
currently lose_sum: 100.5052719116211
time_elpased: 1.725
batch start
#iterations: 162
currently lose_sum: 100.50469774007797
time_elpased: 1.74
batch start
#iterations: 163
currently lose_sum: 100.50500375032425
time_elpased: 1.765
batch start
#iterations: 164
currently lose_sum: 100.50345289707184
time_elpased: 1.762
batch start
#iterations: 165
currently lose_sum: 100.50366395711899
time_elpased: 1.72
batch start
#iterations: 166
currently lose_sum: 100.50305104255676
time_elpased: 1.707
batch start
#iterations: 167
currently lose_sum: 100.49916589260101
time_elpased: 1.822
batch start
#iterations: 168
currently lose_sum: 100.47926688194275
time_elpased: 1.774
batch start
#iterations: 169
currently lose_sum: 100.01602828502655
time_elpased: 1.734
batch start
#iterations: 170
currently lose_sum: 100.39099478721619
time_elpased: 1.732
batch start
#iterations: 171
currently lose_sum: 100.27360081672668
time_elpased: 1.757
batch start
#iterations: 172
currently lose_sum: 99.57286757230759
time_elpased: 1.773
batch start
#iterations: 173
currently lose_sum: 99.33204394578934
time_elpased: 1.723
batch start
#iterations: 174
currently lose_sum: 100.01626944541931
time_elpased: 1.717
batch start
#iterations: 175
currently lose_sum: 99.20528221130371
time_elpased: 1.775
batch start
#iterations: 176
currently lose_sum: 99.17817145586014
time_elpased: 1.777
batch start
#iterations: 177
currently lose_sum: 98.94964551925659
time_elpased: 1.731
batch start
#iterations: 178
currently lose_sum: 99.06168448925018
time_elpased: 1.724
batch start
#iterations: 179
currently lose_sum: 98.88423430919647
time_elpased: 1.757
start validation test
0.539432989691
0.521462290043
0.977668004528
0.680150349025
0.538663600529
66.291
batch start
#iterations: 180
currently lose_sum: 100.16278791427612
time_elpased: 1.73
batch start
#iterations: 181
currently lose_sum: 100.49764090776443
time_elpased: 1.744
batch start
#iterations: 182
currently lose_sum: 100.48653709888458
time_elpased: 1.724
batch start
#iterations: 183
currently lose_sum: 100.07890731096268
time_elpased: 1.719
batch start
#iterations: 184
currently lose_sum: 98.90188956260681
time_elpased: 1.744
batch start
#iterations: 185
currently lose_sum: 98.59228640794754
time_elpased: 1.728
batch start
#iterations: 186
currently lose_sum: 99.8418555855751
time_elpased: 1.794
batch start
#iterations: 187
currently lose_sum: 98.7918718457222
time_elpased: 1.73
batch start
#iterations: 188
currently lose_sum: 99.56179744005203
time_elpased: 1.733
batch start
#iterations: 189
currently lose_sum: 98.4162575006485
time_elpased: 1.741
batch start
#iterations: 190
currently lose_sum: 98.98214602470398
time_elpased: 1.731
batch start
#iterations: 191
currently lose_sum: 98.8757056593895
time_elpased: 1.753
batch start
#iterations: 192
currently lose_sum: 98.43394136428833
time_elpased: 1.703
batch start
#iterations: 193
currently lose_sum: 100.49776673316956
time_elpased: 1.767
batch start
#iterations: 194
currently lose_sum: 99.55531316995621
time_elpased: 1.768
batch start
#iterations: 195
currently lose_sum: 98.43203842639923
time_elpased: 1.79
batch start
#iterations: 196
currently lose_sum: 98.87612169981003
time_elpased: 1.789
batch start
#iterations: 197
currently lose_sum: 98.73498368263245
time_elpased: 1.76
batch start
#iterations: 198
currently lose_sum: 98.76128458976746
time_elpased: 1.708
batch start
#iterations: 199
currently lose_sum: 98.6566566824913
time_elpased: 1.779
start validation test
0.49912371134
0.0
0.0
nan
0.5
65.931
batch start
#iterations: 200
currently lose_sum: 99.01275366544724
time_elpased: 1.686
batch start
#iterations: 201
currently lose_sum: 99.36555314064026
time_elpased: 1.779
batch start
#iterations: 202
currently lose_sum: 98.59527480602264
time_elpased: 1.743
batch start
#iterations: 203
currently lose_sum: 98.6765273809433
time_elpased: 1.753
batch start
#iterations: 204
currently lose_sum: 97.84830057621002
time_elpased: 1.745
batch start
#iterations: 205
currently lose_sum: 97.57185280323029
time_elpased: 1.773
batch start
#iterations: 206
currently lose_sum: 99.53487455844879
time_elpased: 1.732
batch start
#iterations: 207
currently lose_sum: 100.50317573547363
time_elpased: 1.737
batch start
#iterations: 208
currently lose_sum: 100.49332851171494
time_elpased: 1.808
batch start
#iterations: 209
currently lose_sum: 100.1782054901123
time_elpased: 1.778
batch start
#iterations: 210
currently lose_sum: 98.08716154098511
time_elpased: 1.72
batch start
#iterations: 211
currently lose_sum: 99.48132705688477
time_elpased: 1.736
batch start
#iterations: 212
currently lose_sum: 98.0483146905899
time_elpased: 1.757
batch start
#iterations: 213
currently lose_sum: 98.99939614534378
time_elpased: 1.744
batch start
#iterations: 214
currently lose_sum: 100.4810476899147
time_elpased: 1.759
batch start
#iterations: 215
currently lose_sum: 99.9185471534729
time_elpased: 1.808
batch start
#iterations: 216
currently lose_sum: 97.68730002641678
time_elpased: 1.739
batch start
#iterations: 217
currently lose_sum: 98.10315209627151
time_elpased: 1.733
batch start
#iterations: 218
currently lose_sum: 99.63240844011307
time_elpased: 1.734
batch start
#iterations: 219
currently lose_sum: 98.22335070371628
time_elpased: 1.744
start validation test
0.647525773196
0.647898900647
0.648965730164
0.648431876607
0.647523245129
62.086
batch start
#iterations: 220
currently lose_sum: 97.9556582570076
time_elpased: 1.759
batch start
#iterations: 221
currently lose_sum: 97.98293662071228
time_elpased: 1.762
batch start
#iterations: 222
currently lose_sum: 97.4605085849762
time_elpased: 1.765
batch start
#iterations: 223
currently lose_sum: 97.88413232564926
time_elpased: 1.725
batch start
#iterations: 224
currently lose_sum: 99.31975138187408
time_elpased: 1.739
batch start
#iterations: 225
currently lose_sum: 97.81868153810501
time_elpased: 1.699
batch start
#iterations: 226
currently lose_sum: 97.50387191772461
time_elpased: 1.742
batch start
#iterations: 227
currently lose_sum: 98.5944100022316
time_elpased: 1.762
batch start
#iterations: 228
currently lose_sum: 99.30335903167725
time_elpased: 1.744
batch start
#iterations: 229
currently lose_sum: 97.91304349899292
time_elpased: 1.787
batch start
#iterations: 230
currently lose_sum: 97.74138796329498
time_elpased: 1.752
batch start
#iterations: 231
currently lose_sum: 99.8482255935669
time_elpased: 1.73
batch start
#iterations: 232
currently lose_sum: 97.5507727265358
time_elpased: 1.731
batch start
#iterations: 233
currently lose_sum: 99.15619426965714
time_elpased: 1.753
batch start
#iterations: 234
currently lose_sum: 97.80167549848557
time_elpased: 1.767
batch start
#iterations: 235
currently lose_sum: 99.10956311225891
time_elpased: 1.787
batch start
#iterations: 236
currently lose_sum: 100.47144216299057
time_elpased: 1.748
batch start
#iterations: 237
currently lose_sum: 98.33898192644119
time_elpased: 1.738
batch start
#iterations: 238
currently lose_sum: 99.35410928726196
time_elpased: 1.762
batch start
#iterations: 239
currently lose_sum: 100.48638159036636
time_elpased: 1.73
start validation test
0.560309278351
0.54553824906
0.731707317073
0.625054945055
0.560008362657
67.185
batch start
#iterations: 240
currently lose_sum: 99.56375414133072
time_elpased: 1.77
batch start
#iterations: 241
currently lose_sum: 97.94534838199615
time_elpased: 1.762
batch start
#iterations: 242
currently lose_sum: 98.72551095485687
time_elpased: 1.766
batch start
#iterations: 243
currently lose_sum: 97.97667932510376
time_elpased: 1.768
batch start
#iterations: 244
currently lose_sum: 99.47756725549698
time_elpased: 1.721
batch start
#iterations: 245
currently lose_sum: 99.5660103559494
time_elpased: 1.739
batch start
#iterations: 246
currently lose_sum: 98.86122387647629
time_elpased: 1.772
batch start
#iterations: 247
currently lose_sum: 97.67014104127884
time_elpased: 1.771
batch start
#iterations: 248
currently lose_sum: 98.18677175045013
time_elpased: 1.762
batch start
#iterations: 249
currently lose_sum: 98.74615478515625
time_elpased: 1.737
batch start
#iterations: 250
currently lose_sum: 98.23650634288788
time_elpased: 1.726
batch start
#iterations: 251
currently lose_sum: 100.48363846540451
time_elpased: 1.768
batch start
#iterations: 252
currently lose_sum: 98.32922208309174
time_elpased: 1.719
batch start
#iterations: 253
currently lose_sum: 97.73689383268356
time_elpased: 1.738
batch start
#iterations: 254
currently lose_sum: 98.3063040971756
time_elpased: 1.73
batch start
#iterations: 255
currently lose_sum: 99.84030771255493
time_elpased: 1.81
batch start
#iterations: 256
currently lose_sum: 97.52559101581573
time_elpased: 1.736
batch start
#iterations: 257
currently lose_sum: 97.46960389614105
time_elpased: 1.768
batch start
#iterations: 258
currently lose_sum: 97.86458361148834
time_elpased: 1.728
batch start
#iterations: 259
currently lose_sum: 98.29234874248505
time_elpased: 1.747
start validation test
0.49912371134
0.0
0.0
nan
0.5
67.218
acc: 0.645
pre: 0.646
rec: 0.646
F1: 0.646
auc: 0.645
