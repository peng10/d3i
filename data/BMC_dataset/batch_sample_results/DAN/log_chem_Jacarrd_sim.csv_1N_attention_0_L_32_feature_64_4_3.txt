start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.5329178571701
time_elpased: 1.733
batch start
#iterations: 1
currently lose_sum: 100.41828715801239
time_elpased: 1.732
batch start
#iterations: 2
currently lose_sum: 100.37772464752197
time_elpased: 1.794
batch start
#iterations: 3
currently lose_sum: 100.37519657611847
time_elpased: 1.746
batch start
#iterations: 4
currently lose_sum: 100.28178119659424
time_elpased: 1.749
batch start
#iterations: 5
currently lose_sum: 100.2562370300293
time_elpased: 1.726
batch start
#iterations: 6
currently lose_sum: 100.26933068037033
time_elpased: 1.756
batch start
#iterations: 7
currently lose_sum: 100.20956927537918
time_elpased: 1.737
batch start
#iterations: 8
currently lose_sum: 100.12910306453705
time_elpased: 1.683
batch start
#iterations: 9
currently lose_sum: 100.04479539394379
time_elpased: 1.759
batch start
#iterations: 10
currently lose_sum: 100.07875144481659
time_elpased: 1.751
batch start
#iterations: 11
currently lose_sum: 100.03202664852142
time_elpased: 1.738
batch start
#iterations: 12
currently lose_sum: 99.97178834676743
time_elpased: 1.731
batch start
#iterations: 13
currently lose_sum: 99.8469688296318
time_elpased: 1.741
batch start
#iterations: 14
currently lose_sum: 99.74708414077759
time_elpased: 1.746
batch start
#iterations: 15
currently lose_sum: 99.66535645723343
time_elpased: 1.724
batch start
#iterations: 16
currently lose_sum: 99.79594266414642
time_elpased: 1.751
batch start
#iterations: 17
currently lose_sum: 99.60374301671982
time_elpased: 1.733
batch start
#iterations: 18
currently lose_sum: 99.59957414865494
time_elpased: 1.754
batch start
#iterations: 19
currently lose_sum: 99.94668984413147
time_elpased: 1.702
start validation test
0.56675257732
0.653548840478
0.287155207904
0.398998927422
0.567214530842
66.383
batch start
#iterations: 20
currently lose_sum: 99.45807099342346
time_elpased: 1.685
batch start
#iterations: 21
currently lose_sum: 99.36962187290192
time_elpased: 1.685
batch start
#iterations: 22
currently lose_sum: 99.86542224884033
time_elpased: 1.698
batch start
#iterations: 23
currently lose_sum: 99.56758123636246
time_elpased: 1.73
batch start
#iterations: 24
currently lose_sum: 99.33399438858032
time_elpased: 1.747
batch start
#iterations: 25
currently lose_sum: 99.31857740879059
time_elpased: 1.73
batch start
#iterations: 26
currently lose_sum: 99.39568662643433
time_elpased: 1.749
batch start
#iterations: 27
currently lose_sum: 99.56112617254257
time_elpased: 1.748
batch start
#iterations: 28
currently lose_sum: 99.66681498289108
time_elpased: 1.761
batch start
#iterations: 29
currently lose_sum: 99.47512942552567
time_elpased: 1.752
batch start
#iterations: 30
currently lose_sum: 100.04740285873413
time_elpased: 1.757
batch start
#iterations: 31
currently lose_sum: 99.30783009529114
time_elpased: 1.709
batch start
#iterations: 32
currently lose_sum: 99.13570284843445
time_elpased: 1.726
batch start
#iterations: 33
currently lose_sum: 99.40255039930344
time_elpased: 1.758
batch start
#iterations: 34
currently lose_sum: 99.71995943784714
time_elpased: 1.742
batch start
#iterations: 35
currently lose_sum: 100.26999032497406
time_elpased: 1.753
batch start
#iterations: 36
currently lose_sum: 99.18537920713425
time_elpased: 1.71
batch start
#iterations: 37
currently lose_sum: 99.20433384180069
time_elpased: 1.76
batch start
#iterations: 38
currently lose_sum: 99.54436355829239
time_elpased: 1.747
batch start
#iterations: 39
currently lose_sum: 99.23428720235825
time_elpased: 1.745
start validation test
0.625773195876
0.596647253266
0.780259365994
0.67621086433
0.625517952307
65.290
batch start
#iterations: 40
currently lose_sum: 99.68037086725235
time_elpased: 1.729
batch start
#iterations: 41
currently lose_sum: 100.507060110569
time_elpased: 1.778
batch start
#iterations: 42
currently lose_sum: 100.49560707807541
time_elpased: 1.76
batch start
#iterations: 43
currently lose_sum: 100.47739458084106
time_elpased: 1.72
batch start
#iterations: 44
currently lose_sum: 100.19683873653412
time_elpased: 1.753
batch start
#iterations: 45
currently lose_sum: 100.26967406272888
time_elpased: 1.691
batch start
#iterations: 46
currently lose_sum: 99.55505645275116
time_elpased: 1.721
batch start
#iterations: 47
currently lose_sum: 99.58016854524612
time_elpased: 1.73
batch start
#iterations: 48
currently lose_sum: 99.19077557325363
time_elpased: 1.743
batch start
#iterations: 49
currently lose_sum: 99.77886211872101
time_elpased: 1.778
batch start
#iterations: 50
currently lose_sum: 99.49741119146347
time_elpased: 1.735
batch start
#iterations: 51
currently lose_sum: 100.29359084367752
time_elpased: 1.773
batch start
#iterations: 52
currently lose_sum: 100.27135676145554
time_elpased: 1.746
batch start
#iterations: 53
currently lose_sum: 99.77668172121048
time_elpased: 1.739
batch start
#iterations: 54
currently lose_sum: 100.50799715518951
time_elpased: 1.737
batch start
#iterations: 55
currently lose_sum: 100.50441944599152
time_elpased: 1.761
batch start
#iterations: 56
currently lose_sum: 100.50388479232788
time_elpased: 1.719
batch start
#iterations: 57
currently lose_sum: 100.50311589241028
time_elpased: 1.759
batch start
#iterations: 58
currently lose_sum: 100.5023843050003
time_elpased: 1.755
batch start
#iterations: 59
currently lose_sum: 100.50185626745224
time_elpased: 1.725
start validation test
0.554896907216
0.546145308631
0.658398517909
0.597041392505
0.554725900838
67.228
batch start
#iterations: 60
currently lose_sum: 100.499018907547
time_elpased: 1.72
batch start
#iterations: 61
currently lose_sum: 100.49475860595703
time_elpased: 1.706
batch start
#iterations: 62
currently lose_sum: 100.4663599729538
time_elpased: 1.723
batch start
#iterations: 63
currently lose_sum: 100.12399995326996
time_elpased: 1.744
batch start
#iterations: 64
currently lose_sum: 100.12169981002808
time_elpased: 1.709
batch start
#iterations: 65
currently lose_sum: 100.93731528520584
time_elpased: 1.723
batch start
#iterations: 66
currently lose_sum: 100.50191962718964
time_elpased: 1.762
batch start
#iterations: 67
currently lose_sum: 100.49920386075974
time_elpased: 1.721
batch start
#iterations: 68
currently lose_sum: 100.49351274967194
time_elpased: 1.775
batch start
#iterations: 69
currently lose_sum: 100.46817988157272
time_elpased: 1.786
batch start
#iterations: 70
currently lose_sum: 99.85938268899918
time_elpased: 1.735
batch start
#iterations: 71
currently lose_sum: 100.45801424980164
time_elpased: 1.716
batch start
#iterations: 72
currently lose_sum: 100.36415797472
time_elpased: 1.781
batch start
#iterations: 73
currently lose_sum: 99.69988638162613
time_elpased: 1.76
batch start
#iterations: 74
currently lose_sum: 100.4897671341896
time_elpased: 1.749
batch start
#iterations: 75
currently lose_sum: 100.37894541025162
time_elpased: 1.8
batch start
#iterations: 76
currently lose_sum: 99.50635796785355
time_elpased: 1.752
batch start
#iterations: 77
currently lose_sum: 100.60857605934143
time_elpased: 1.737
batch start
#iterations: 78
currently lose_sum: 99.398481965065
time_elpased: 1.785
batch start
#iterations: 79
currently lose_sum: 99.12770742177963
time_elpased: 1.763
start validation test
0.567731958763
0.663873829473
0.2772745986
0.391171772905
0.568211855269
64.884
batch start
#iterations: 80
currently lose_sum: 100.1814723610878
time_elpased: 1.734
batch start
#iterations: 81
currently lose_sum: 100.1522091627121
time_elpased: 1.803
batch start
#iterations: 82
currently lose_sum: 99.74212920665741
time_elpased: 1.788
batch start
#iterations: 83
currently lose_sum: 99.3487988114357
time_elpased: 1.762
batch start
#iterations: 84
currently lose_sum: 99.50724291801453
time_elpased: 1.72
batch start
#iterations: 85
currently lose_sum: 99.3432126045227
time_elpased: 1.697
batch start
#iterations: 86
currently lose_sum: 98.86812072992325
time_elpased: 1.74
batch start
#iterations: 87
currently lose_sum: 98.79039877653122
time_elpased: 1.856
batch start
#iterations: 88
currently lose_sum: 100.28432536125183
time_elpased: 1.739
batch start
#iterations: 89
currently lose_sum: 100.50543129444122
time_elpased: 1.729
batch start
#iterations: 90
currently lose_sum: 100.50504666566849
time_elpased: 1.754
batch start
#iterations: 91
currently lose_sum: 100.50446617603302
time_elpased: 1.731
batch start
#iterations: 92
currently lose_sum: 100.5036723613739
time_elpased: 1.728
batch start
#iterations: 93
currently lose_sum: 100.50192302465439
time_elpased: 1.751
batch start
#iterations: 94
currently lose_sum: 100.49679094552994
time_elpased: 1.741
batch start
#iterations: 95
currently lose_sum: 100.43585360050201
time_elpased: 1.765
batch start
#iterations: 96
currently lose_sum: 99.70194244384766
time_elpased: 1.76
batch start
#iterations: 97
currently lose_sum: 100.51444345712662
time_elpased: 1.718
batch start
#iterations: 98
currently lose_sum: 100.50601005554199
time_elpased: 1.715
batch start
#iterations: 99
currently lose_sum: 100.50610333681107
time_elpased: 1.708
start validation test
0.524690721649
0.517162471396
0.767599835323
0.617972407507
0.524289384824
67.235
batch start
#iterations: 100
currently lose_sum: 100.50609791278839
time_elpased: 1.744
batch start
#iterations: 101
currently lose_sum: 100.50611293315887
time_elpased: 1.697
batch start
#iterations: 102
currently lose_sum: 100.50603276491165
time_elpased: 1.726
batch start
#iterations: 103
currently lose_sum: 100.50601136684418
time_elpased: 1.768
batch start
#iterations: 104
currently lose_sum: 100.50604033470154
time_elpased: 1.759
batch start
#iterations: 105
currently lose_sum: 100.50600636005402
time_elpased: 1.719
batch start
#iterations: 106
currently lose_sum: 100.50592416524887
time_elpased: 1.763
batch start
#iterations: 107
currently lose_sum: 100.50599485635757
time_elpased: 1.723
batch start
#iterations: 108
currently lose_sum: 100.5059460401535
time_elpased: 1.73
batch start
#iterations: 109
currently lose_sum: 100.50602608919144
time_elpased: 1.735
batch start
#iterations: 110
currently lose_sum: 100.50590080022812
time_elpased: 1.731
batch start
#iterations: 111
currently lose_sum: 100.5058200955391
time_elpased: 1.742
batch start
#iterations: 112
currently lose_sum: 100.50574344396591
time_elpased: 1.758
batch start
#iterations: 113
currently lose_sum: 100.50575888156891
time_elpased: 1.74
batch start
#iterations: 114
currently lose_sum: 100.50570899248123
time_elpased: 1.725
batch start
#iterations: 115
currently lose_sum: 100.50568997859955
time_elpased: 1.732
batch start
#iterations: 116
currently lose_sum: 100.50558304786682
time_elpased: 1.721
batch start
#iterations: 117
currently lose_sum: 100.50558078289032
time_elpased: 1.722
batch start
#iterations: 118
currently lose_sum: 100.50538331270218
time_elpased: 1.736
batch start
#iterations: 119
currently lose_sum: 100.50520724058151
time_elpased: 1.72
start validation test
0.534845360825
0.525218658892
0.7416632359
0.614951356887
0.534503654298
67.234
batch start
#iterations: 120
currently lose_sum: 100.50482511520386
time_elpased: 1.743
batch start
#iterations: 121
currently lose_sum: 100.5042136311531
time_elpased: 1.8
batch start
#iterations: 122
currently lose_sum: 100.50311851501465
time_elpased: 1.731
batch start
#iterations: 123
currently lose_sum: 100.49962055683136
time_elpased: 1.714
batch start
#iterations: 124
currently lose_sum: 100.44770383834839
time_elpased: 1.746
batch start
#iterations: 125
currently lose_sum: 100.73892056941986
time_elpased: 1.75
batch start
#iterations: 126
currently lose_sum: 100.50584691762924
time_elpased: 1.746
batch start
#iterations: 127
currently lose_sum: 100.50584954023361
time_elpased: 1.757
batch start
#iterations: 128
currently lose_sum: 100.50575703382492
time_elpased: 1.727
batch start
#iterations: 129
currently lose_sum: 100.50555676221848
time_elpased: 1.782
batch start
#iterations: 130
currently lose_sum: 100.50570118427277
time_elpased: 1.72
batch start
#iterations: 131
currently lose_sum: 100.50559175014496
time_elpased: 1.696
batch start
#iterations: 132
currently lose_sum: 100.50517988204956
time_elpased: 1.773
batch start
#iterations: 133
currently lose_sum: 100.50505781173706
time_elpased: 1.755
batch start
#iterations: 134
currently lose_sum: 100.50440722703934
time_elpased: 1.704
batch start
#iterations: 135
currently lose_sum: 100.50400751829147
time_elpased: 1.793
batch start
#iterations: 136
currently lose_sum: 100.50050389766693
time_elpased: 1.754
batch start
#iterations: 137
currently lose_sum: 100.48571002483368
time_elpased: 1.716
batch start
#iterations: 138
currently lose_sum: 100.41684144735336
time_elpased: 1.76
batch start
#iterations: 139
currently lose_sum: 100.09141337871552
time_elpased: 1.752
start validation test
0.589432989691
0.579598145286
0.65613421161
0.615496017379
0.589322785276
66.712
batch start
#iterations: 140
currently lose_sum: 99.75358426570892
time_elpased: 1.733
batch start
#iterations: 141
currently lose_sum: 99.76772916316986
time_elpased: 1.71
batch start
#iterations: 142
currently lose_sum: 99.44377732276917
time_elpased: 1.764
batch start
#iterations: 143
currently lose_sum: 100.26140654087067
time_elpased: 1.772
batch start
#iterations: 144
currently lose_sum: 100.00422132015228
time_elpased: 1.771
batch start
#iterations: 145
currently lose_sum: 99.29012900590897
time_elpased: 1.729
batch start
#iterations: 146
currently lose_sum: 99.6462881565094
time_elpased: 1.747
batch start
#iterations: 147
currently lose_sum: 99.0729268193245
time_elpased: 1.734
batch start
#iterations: 148
currently lose_sum: 99.00207716226578
time_elpased: 1.769
batch start
#iterations: 149
currently lose_sum: 99.92642736434937
time_elpased: 1.767
batch start
#iterations: 150
currently lose_sum: 100.48580980300903
time_elpased: 1.714
batch start
#iterations: 151
currently lose_sum: 99.64773225784302
time_elpased: 1.736
batch start
#iterations: 152
currently lose_sum: 99.7382824420929
time_elpased: 1.747
batch start
#iterations: 153
currently lose_sum: 100.05038261413574
time_elpased: 1.725
batch start
#iterations: 154
currently lose_sum: 99.05932241678238
time_elpased: 1.735
batch start
#iterations: 155
currently lose_sum: 99.46144145727158
time_elpased: 1.707
batch start
#iterations: 156
currently lose_sum: 98.83180272579193
time_elpased: 1.73
batch start
#iterations: 157
currently lose_sum: 99.26257234811783
time_elpased: 1.807
batch start
#iterations: 158
currently lose_sum: 98.80669063329697
time_elpased: 1.732
batch start
#iterations: 159
currently lose_sum: 99.92860281467438
time_elpased: 1.74
start validation test
0.499175257732
0.0
0.0
nan
0.5
67.221
acc: 0.568
pre: 0.665
rec: 0.278
F1: 0.392
auc: 0.569
