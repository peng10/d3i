start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.60628283023834
time_elpased: 1.822
batch start
#iterations: 1
currently lose_sum: 100.35064780712128
time_elpased: 1.81
batch start
#iterations: 2
currently lose_sum: 100.2567845582962
time_elpased: 1.786
batch start
#iterations: 3
currently lose_sum: 100.2271717786789
time_elpased: 1.782
batch start
#iterations: 4
currently lose_sum: 100.10256731510162
time_elpased: 1.897
batch start
#iterations: 5
currently lose_sum: 100.0676566362381
time_elpased: 1.786
batch start
#iterations: 6
currently lose_sum: 100.02082389593124
time_elpased: 1.803
batch start
#iterations: 7
currently lose_sum: 99.91428178548813
time_elpased: 1.845
batch start
#iterations: 8
currently lose_sum: 99.71817606687546
time_elpased: 1.815
batch start
#iterations: 9
currently lose_sum: 99.89444839954376
time_elpased: 1.813
batch start
#iterations: 10
currently lose_sum: 99.69175708293915
time_elpased: 1.8
batch start
#iterations: 11
currently lose_sum: 99.84699702262878
time_elpased: 1.804
batch start
#iterations: 12
currently lose_sum: 99.62317550182343
time_elpased: 1.825
batch start
#iterations: 13
currently lose_sum: 99.46542048454285
time_elpased: 1.811
batch start
#iterations: 14
currently lose_sum: 99.6969284415245
time_elpased: 1.824
batch start
#iterations: 15
currently lose_sum: 99.35732823610306
time_elpased: 1.805
batch start
#iterations: 16
currently lose_sum: 99.47980046272278
time_elpased: 1.771
batch start
#iterations: 17
currently lose_sum: 99.39356565475464
time_elpased: 1.828
batch start
#iterations: 18
currently lose_sum: 99.2691820859909
time_elpased: 1.791
batch start
#iterations: 19
currently lose_sum: 99.48336577415466
time_elpased: 1.802
start validation test
0.591494845361
0.635552193646
0.432335082844
0.514607705028
0.591774274873
65.343
batch start
#iterations: 20
currently lose_sum: 99.11383485794067
time_elpased: 1.838
batch start
#iterations: 21
currently lose_sum: 99.04333889484406
time_elpased: 1.839
batch start
#iterations: 22
currently lose_sum: 99.01310551166534
time_elpased: 1.845
batch start
#iterations: 23
currently lose_sum: 99.02022677659988
time_elpased: 1.835
batch start
#iterations: 24
currently lose_sum: 98.8670181632042
time_elpased: 1.831
batch start
#iterations: 25
currently lose_sum: 98.87925803661346
time_elpased: 1.817
batch start
#iterations: 26
currently lose_sum: 99.09859895706177
time_elpased: 1.836
batch start
#iterations: 27
currently lose_sum: 98.92491137981415
time_elpased: 1.84
batch start
#iterations: 28
currently lose_sum: 98.53071337938309
time_elpased: 1.873
batch start
#iterations: 29
currently lose_sum: 98.56468713283539
time_elpased: 1.783
batch start
#iterations: 30
currently lose_sum: 98.68176066875458
time_elpased: 1.923
batch start
#iterations: 31
currently lose_sum: 98.67897045612335
time_elpased: 1.855
batch start
#iterations: 32
currently lose_sum: 98.32638871669769
time_elpased: 1.811
batch start
#iterations: 33
currently lose_sum: 98.46000427007675
time_elpased: 1.872
batch start
#iterations: 34
currently lose_sum: 98.62348306179047
time_elpased: 1.887
batch start
#iterations: 35
currently lose_sum: 98.42826610803604
time_elpased: 1.821
batch start
#iterations: 36
currently lose_sum: 98.70449757575989
time_elpased: 1.784
batch start
#iterations: 37
currently lose_sum: 98.2763312458992
time_elpased: 1.855
batch start
#iterations: 38
currently lose_sum: 98.36589521169662
time_elpased: 1.827
batch start
#iterations: 39
currently lose_sum: 98.59423053264618
time_elpased: 1.82
start validation test
0.615309278351
0.608178153196
0.65205310281
0.629351874845
0.615244768899
64.309
batch start
#iterations: 40
currently lose_sum: 98.55526894330978
time_elpased: 1.835
batch start
#iterations: 41
currently lose_sum: 98.36804842948914
time_elpased: 1.885
batch start
#iterations: 42
currently lose_sum: 98.05609381198883
time_elpased: 1.856
batch start
#iterations: 43
currently lose_sum: 98.0711732506752
time_elpased: 1.832
batch start
#iterations: 44
currently lose_sum: 98.23903685808182
time_elpased: 1.841
batch start
#iterations: 45
currently lose_sum: 98.0631691813469
time_elpased: 1.832
batch start
#iterations: 46
currently lose_sum: 98.26897639036179
time_elpased: 1.917
batch start
#iterations: 47
currently lose_sum: 98.31995952129364
time_elpased: 1.931
batch start
#iterations: 48
currently lose_sum: 98.18076127767563
time_elpased: 1.88
batch start
#iterations: 49
currently lose_sum: 98.28487414121628
time_elpased: 1.966
batch start
#iterations: 50
currently lose_sum: 98.2535629272461
time_elpased: 1.873
batch start
#iterations: 51
currently lose_sum: 98.047483086586
time_elpased: 1.839
batch start
#iterations: 52
currently lose_sum: 97.95161813497543
time_elpased: 1.851
batch start
#iterations: 53
currently lose_sum: 98.42846357822418
time_elpased: 1.807
batch start
#iterations: 54
currently lose_sum: 97.82394182682037
time_elpased: 1.754
batch start
#iterations: 55
currently lose_sum: 97.83254647254944
time_elpased: 1.837
batch start
#iterations: 56
currently lose_sum: 98.0376667380333
time_elpased: 1.758
batch start
#iterations: 57
currently lose_sum: 97.91262710094452
time_elpased: 1.843
batch start
#iterations: 58
currently lose_sum: 97.97713452577591
time_elpased: 1.84
batch start
#iterations: 59
currently lose_sum: 97.8023020029068
time_elpased: 1.835
start validation test
0.615154639175
0.634742008859
0.545641658948
0.58682899834
0.615276679934
63.970
batch start
#iterations: 60
currently lose_sum: 97.85934764146805
time_elpased: 1.822
batch start
#iterations: 61
currently lose_sum: 98.2102764248848
time_elpased: 1.841
batch start
#iterations: 62
currently lose_sum: 97.72274518013
time_elpased: 1.837
batch start
#iterations: 63
currently lose_sum: 97.61600822210312
time_elpased: 1.849
batch start
#iterations: 64
currently lose_sum: 97.73467272520065
time_elpased: 1.84
batch start
#iterations: 65
currently lose_sum: 97.42406558990479
time_elpased: 1.812
batch start
#iterations: 66
currently lose_sum: 97.63367021083832
time_elpased: 1.962
batch start
#iterations: 67
currently lose_sum: 97.70195060968399
time_elpased: 1.854
batch start
#iterations: 68
currently lose_sum: 97.49352896213531
time_elpased: 1.779
batch start
#iterations: 69
currently lose_sum: 97.54602211713791
time_elpased: 1.799
batch start
#iterations: 70
currently lose_sum: 97.23610818386078
time_elpased: 1.807
batch start
#iterations: 71
currently lose_sum: 97.39686626195908
time_elpased: 1.802
batch start
#iterations: 72
currently lose_sum: 97.05776369571686
time_elpased: 1.78
batch start
#iterations: 73
currently lose_sum: 97.23843276500702
time_elpased: 1.743
batch start
#iterations: 74
currently lose_sum: 97.01388972997665
time_elpased: 1.831
batch start
#iterations: 75
currently lose_sum: 97.22919714450836
time_elpased: 1.947
batch start
#iterations: 76
currently lose_sum: 97.74096900224686
time_elpased: 1.828
batch start
#iterations: 77
currently lose_sum: 97.32378870248795
time_elpased: 1.801
batch start
#iterations: 78
currently lose_sum: 97.54620879888535
time_elpased: 1.81
batch start
#iterations: 79
currently lose_sum: 97.33575737476349
time_elpased: 1.855
start validation test
0.594175257732
0.619678089304
0.491303900381
0.548074163366
0.594355864267
64.657
batch start
#iterations: 80
currently lose_sum: 97.66151797771454
time_elpased: 1.789
batch start
#iterations: 81
currently lose_sum: 97.61748737096786
time_elpased: 1.796
batch start
#iterations: 82
currently lose_sum: 97.31508028507233
time_elpased: 1.786
batch start
#iterations: 83
currently lose_sum: 97.21795511245728
time_elpased: 1.772
batch start
#iterations: 84
currently lose_sum: 97.09859657287598
time_elpased: 1.801
batch start
#iterations: 85
currently lose_sum: 97.16375315189362
time_elpased: 1.843
batch start
#iterations: 86
currently lose_sum: 97.34995347261429
time_elpased: 1.811
batch start
#iterations: 87
currently lose_sum: 97.28972887992859
time_elpased: 1.804
batch start
#iterations: 88
currently lose_sum: 97.44335120916367
time_elpased: 1.797
batch start
#iterations: 89
currently lose_sum: 97.16813403367996
time_elpased: 1.825
batch start
#iterations: 90
currently lose_sum: 96.93772047758102
time_elpased: 1.759
batch start
#iterations: 91
currently lose_sum: 96.91912335157394
time_elpased: 1.806
batch start
#iterations: 92
currently lose_sum: 97.41664397716522
time_elpased: 1.81
batch start
#iterations: 93
currently lose_sum: 96.76032590866089
time_elpased: 1.817
batch start
#iterations: 94
currently lose_sum: 97.08805620670319
time_elpased: 1.786
batch start
#iterations: 95
currently lose_sum: 97.18906152248383
time_elpased: 1.763
batch start
#iterations: 96
currently lose_sum: 97.12861227989197
time_elpased: 1.788
batch start
#iterations: 97
currently lose_sum: 96.76070088148117
time_elpased: 1.801
batch start
#iterations: 98
currently lose_sum: 97.10085964202881
time_elpased: 1.778
batch start
#iterations: 99
currently lose_sum: 96.88502836227417
time_elpased: 1.774
start validation test
0.619536082474
0.635373203523
0.564165894824
0.597656037067
0.619633293379
63.504
batch start
#iterations: 100
currently lose_sum: 96.96409833431244
time_elpased: 1.8
batch start
#iterations: 101
currently lose_sum: 96.54913979768753
time_elpased: 1.789
batch start
#iterations: 102
currently lose_sum: 96.72084879875183
time_elpased: 1.844
batch start
#iterations: 103
currently lose_sum: 96.97367310523987
time_elpased: 1.838
batch start
#iterations: 104
currently lose_sum: 96.70820546150208
time_elpased: 1.812
batch start
#iterations: 105
currently lose_sum: 96.63602721691132
time_elpased: 1.828
batch start
#iterations: 106
currently lose_sum: 96.81637001037598
time_elpased: 1.844
batch start
#iterations: 107
currently lose_sum: 96.41426748037338
time_elpased: 1.9
batch start
#iterations: 108
currently lose_sum: 96.46325051784515
time_elpased: 1.875
batch start
#iterations: 109
currently lose_sum: 96.73641055822372
time_elpased: 1.826
batch start
#iterations: 110
currently lose_sum: 96.67291378974915
time_elpased: 1.843
batch start
#iterations: 111
currently lose_sum: 96.81309247016907
time_elpased: 1.846
batch start
#iterations: 112
currently lose_sum: 96.67720866203308
time_elpased: 1.804
batch start
#iterations: 113
currently lose_sum: 96.54534095525742
time_elpased: 1.808
batch start
#iterations: 114
currently lose_sum: 96.44142228364944
time_elpased: 1.796
batch start
#iterations: 115
currently lose_sum: 96.4228567481041
time_elpased: 1.812
batch start
#iterations: 116
currently lose_sum: 96.51859557628632
time_elpased: 1.894
batch start
#iterations: 117
currently lose_sum: 96.59267818927765
time_elpased: 1.849
batch start
#iterations: 118
currently lose_sum: 96.68893206119537
time_elpased: 1.887
batch start
#iterations: 119
currently lose_sum: 96.14782083034515
time_elpased: 1.754
start validation test
0.614381443299
0.636142230882
0.537614490069
0.582743041999
0.614516219526
63.654
batch start
#iterations: 120
currently lose_sum: 96.84190326929092
time_elpased: 1.758
batch start
#iterations: 121
currently lose_sum: 96.47054374217987
time_elpased: 1.793
batch start
#iterations: 122
currently lose_sum: 96.57956767082214
time_elpased: 1.788
batch start
#iterations: 123
currently lose_sum: 96.21758276224136
time_elpased: 1.827
batch start
#iterations: 124
currently lose_sum: 96.57703220844269
time_elpased: 1.743
batch start
#iterations: 125
currently lose_sum: 96.42608445882797
time_elpased: 1.778
batch start
#iterations: 126
currently lose_sum: 96.18963199853897
time_elpased: 1.803
batch start
#iterations: 127
currently lose_sum: 96.28394359350204
time_elpased: 1.806
batch start
#iterations: 128
currently lose_sum: 96.39026218652725
time_elpased: 1.794
batch start
#iterations: 129
currently lose_sum: 96.28965920209885
time_elpased: 1.787
batch start
#iterations: 130
currently lose_sum: 96.06823360919952
time_elpased: 1.776
batch start
#iterations: 131
currently lose_sum: 96.39063656330109
time_elpased: 1.77
batch start
#iterations: 132
currently lose_sum: 96.16677576303482
time_elpased: 1.791
batch start
#iterations: 133
currently lose_sum: 96.35805606842041
time_elpased: 1.764
batch start
#iterations: 134
currently lose_sum: 96.05385762453079
time_elpased: 1.768
batch start
#iterations: 135
currently lose_sum: 96.29481184482574
time_elpased: 1.797
batch start
#iterations: 136
currently lose_sum: 95.98869079351425
time_elpased: 1.787
batch start
#iterations: 137
currently lose_sum: 95.97206884622574
time_elpased: 1.802
batch start
#iterations: 138
currently lose_sum: 95.67305088043213
time_elpased: 1.925
batch start
#iterations: 139
currently lose_sum: 95.91024404764175
time_elpased: 1.869
start validation test
0.610567010309
0.620944282837
0.571163939487
0.595014741356
0.610636188478
63.684
batch start
#iterations: 140
currently lose_sum: 96.12076538801193
time_elpased: 1.946
batch start
#iterations: 141
currently lose_sum: 96.05250924825668
time_elpased: 1.855
batch start
#iterations: 142
currently lose_sum: 96.0210429430008
time_elpased: 1.805
batch start
#iterations: 143
currently lose_sum: 95.98131781816483
time_elpased: 1.786
batch start
#iterations: 144
currently lose_sum: 96.09642314910889
time_elpased: 1.902
batch start
#iterations: 145
currently lose_sum: 96.29832172393799
time_elpased: 1.793
batch start
#iterations: 146
currently lose_sum: 96.09556698799133
time_elpased: 1.773
batch start
#iterations: 147
currently lose_sum: 96.06449657678604
time_elpased: 1.79
batch start
#iterations: 148
currently lose_sum: 95.89018875360489
time_elpased: 1.799
batch start
#iterations: 149
currently lose_sum: 95.66102588176727
time_elpased: 1.83
batch start
#iterations: 150
currently lose_sum: 95.89451217651367
time_elpased: 1.855
batch start
#iterations: 151
currently lose_sum: 95.873002409935
time_elpased: 1.742
batch start
#iterations: 152
currently lose_sum: 96.16611075401306
time_elpased: 1.809
batch start
#iterations: 153
currently lose_sum: 96.02116698026657
time_elpased: 1.832
batch start
#iterations: 154
currently lose_sum: 95.91084784269333
time_elpased: 1.819
batch start
#iterations: 155
currently lose_sum: 95.66314792633057
time_elpased: 1.905
batch start
#iterations: 156
currently lose_sum: 95.7201275229454
time_elpased: 1.873
batch start
#iterations: 157
currently lose_sum: 95.75717240571976
time_elpased: 2.019
batch start
#iterations: 158
currently lose_sum: 95.62918323278427
time_elpased: 1.936
batch start
#iterations: 159
currently lose_sum: 95.71835190057755
time_elpased: 1.805
start validation test
0.608195876289
0.633081761006
0.517958217557
0.569762834663
0.608354302417
63.748
batch start
#iterations: 160
currently lose_sum: 95.4572543501854
time_elpased: 1.93
batch start
#iterations: 161
currently lose_sum: 95.79407197237015
time_elpased: 1.96
batch start
#iterations: 162
currently lose_sum: 95.84724980592728
time_elpased: 1.806
batch start
#iterations: 163
currently lose_sum: 95.53196054697037
time_elpased: 1.905
batch start
#iterations: 164
currently lose_sum: 95.65124255418777
time_elpased: 1.862
batch start
#iterations: 165
currently lose_sum: 95.38794893026352
time_elpased: 1.839
batch start
#iterations: 166
currently lose_sum: 95.25355052947998
time_elpased: 1.917
batch start
#iterations: 167
currently lose_sum: 95.15512478351593
time_elpased: 1.834
batch start
#iterations: 168
currently lose_sum: 95.40302383899689
time_elpased: 1.874
batch start
#iterations: 169
currently lose_sum: 95.17316764593124
time_elpased: 1.891
batch start
#iterations: 170
currently lose_sum: 95.17550897598267
time_elpased: 1.919
batch start
#iterations: 171
currently lose_sum: 95.07832992076874
time_elpased: 1.843
batch start
#iterations: 172
currently lose_sum: 95.25279343128204
time_elpased: 1.777
batch start
#iterations: 173
currently lose_sum: 94.95353227853775
time_elpased: 1.787
batch start
#iterations: 174
currently lose_sum: 95.24473249912262
time_elpased: 1.895
batch start
#iterations: 175
currently lose_sum: 95.08136510848999
time_elpased: 1.799
batch start
#iterations: 176
currently lose_sum: 95.14982265233994
time_elpased: 1.762
batch start
#iterations: 177
currently lose_sum: 94.98116838932037
time_elpased: 1.785
batch start
#iterations: 178
currently lose_sum: 95.00502324104309
time_elpased: 1.794
batch start
#iterations: 179
currently lose_sum: 95.0348829627037
time_elpased: 1.781
start validation test
0.598195876289
0.608171994597
0.556035813523
0.58093650879
0.598269894782
64.412
batch start
#iterations: 180
currently lose_sum: 95.15791088342667
time_elpased: 1.798
batch start
#iterations: 181
currently lose_sum: 94.4792810678482
time_elpased: 1.762
batch start
#iterations: 182
currently lose_sum: 95.10996854305267
time_elpased: 1.806
batch start
#iterations: 183
currently lose_sum: 95.13261300325394
time_elpased: 1.805
batch start
#iterations: 184
currently lose_sum: 94.6471164226532
time_elpased: 1.795
batch start
#iterations: 185
currently lose_sum: 94.57904750108719
time_elpased: 1.762
batch start
#iterations: 186
currently lose_sum: 94.67026174068451
time_elpased: 1.787
batch start
#iterations: 187
currently lose_sum: 94.58584094047546
time_elpased: 1.8
batch start
#iterations: 188
currently lose_sum: 94.80116075277328
time_elpased: 1.812
batch start
#iterations: 189
currently lose_sum: 94.80262929201126
time_elpased: 1.794
batch start
#iterations: 190
currently lose_sum: 94.87669444084167
time_elpased: 1.81
batch start
#iterations: 191
currently lose_sum: 94.46305346488953
time_elpased: 1.806
batch start
#iterations: 192
currently lose_sum: 94.40414333343506
time_elpased: 1.871
batch start
#iterations: 193
currently lose_sum: 94.6184778213501
time_elpased: 1.859
batch start
#iterations: 194
currently lose_sum: 94.6334337592125
time_elpased: 1.868
batch start
#iterations: 195
currently lose_sum: 94.5842512845993
time_elpased: 1.909
batch start
#iterations: 196
currently lose_sum: 94.50576305389404
time_elpased: 1.873
batch start
#iterations: 197
currently lose_sum: 95.00884115695953
time_elpased: 1.878
batch start
#iterations: 198
currently lose_sum: 94.4318670630455
time_elpased: 1.85
batch start
#iterations: 199
currently lose_sum: 94.74977648258209
time_elpased: 1.916
start validation test
0.597422680412
0.628625387832
0.479571884326
0.544074722709
0.597629585662
64.546
batch start
#iterations: 200
currently lose_sum: 94.15722984075546
time_elpased: 1.846
batch start
#iterations: 201
currently lose_sum: 94.70576441287994
time_elpased: 1.885
batch start
#iterations: 202
currently lose_sum: 94.4050965309143
time_elpased: 1.902
batch start
#iterations: 203
currently lose_sum: 94.47697281837463
time_elpased: 1.874
batch start
#iterations: 204
currently lose_sum: 94.54753857851028
time_elpased: 1.879
batch start
#iterations: 205
currently lose_sum: 94.47058933973312
time_elpased: 1.872
batch start
#iterations: 206
currently lose_sum: 94.11391818523407
time_elpased: 1.778
batch start
#iterations: 207
currently lose_sum: 94.17027443647385
time_elpased: 1.738
batch start
#iterations: 208
currently lose_sum: 94.07439559698105
time_elpased: 1.85
batch start
#iterations: 209
currently lose_sum: 94.3067552447319
time_elpased: 1.95
batch start
#iterations: 210
currently lose_sum: 93.84073078632355
time_elpased: 1.839
batch start
#iterations: 211
currently lose_sum: 93.57343500852585
time_elpased: 1.785
batch start
#iterations: 212
currently lose_sum: 94.14348661899567
time_elpased: 1.806
batch start
#iterations: 213
currently lose_sum: 94.20997869968414
time_elpased: 1.782
batch start
#iterations: 214
currently lose_sum: 94.02479320764542
time_elpased: 1.801
batch start
#iterations: 215
currently lose_sum: 94.22565746307373
time_elpased: 1.772
batch start
#iterations: 216
currently lose_sum: 94.06935912370682
time_elpased: 1.836
batch start
#iterations: 217
currently lose_sum: 93.62913513183594
time_elpased: 1.823
batch start
#iterations: 218
currently lose_sum: 94.23225885629654
time_elpased: 1.771
batch start
#iterations: 219
currently lose_sum: 93.81130719184875
time_elpased: 1.83
start validation test
0.596134020619
0.619234668018
0.502933004014
0.555057073088
0.596297649378
64.870
batch start
#iterations: 220
currently lose_sum: 94.31964653730392
time_elpased: 1.82
batch start
#iterations: 221
currently lose_sum: 93.98444074392319
time_elpased: 1.755
batch start
#iterations: 222
currently lose_sum: 93.71083641052246
time_elpased: 1.835
batch start
#iterations: 223
currently lose_sum: 93.32227832078934
time_elpased: 1.783
batch start
#iterations: 224
currently lose_sum: 93.45220875740051
time_elpased: 1.821
batch start
#iterations: 225
currently lose_sum: 93.68190377950668
time_elpased: 1.829
batch start
#iterations: 226
currently lose_sum: 93.8854169845581
time_elpased: 1.749
batch start
#iterations: 227
currently lose_sum: 93.72262895107269
time_elpased: 1.784
batch start
#iterations: 228
currently lose_sum: 93.58958768844604
time_elpased: 1.777
batch start
#iterations: 229
currently lose_sum: 93.28142327070236
time_elpased: 1.813
batch start
#iterations: 230
currently lose_sum: 93.43163329362869
time_elpased: 1.782
batch start
#iterations: 231
currently lose_sum: 93.6072154045105
time_elpased: 1.78
batch start
#iterations: 232
currently lose_sum: 93.62828636169434
time_elpased: 1.825
batch start
#iterations: 233
currently lose_sum: 93.35182631015778
time_elpased: 1.791
batch start
#iterations: 234
currently lose_sum: 93.51109975576401
time_elpased: 1.789
batch start
#iterations: 235
currently lose_sum: 93.25654876232147
time_elpased: 1.811
batch start
#iterations: 236
currently lose_sum: 93.47621536254883
time_elpased: 1.809
batch start
#iterations: 237
currently lose_sum: 93.06642556190491
time_elpased: 1.771
batch start
#iterations: 238
currently lose_sum: 93.55407083034515
time_elpased: 1.755
batch start
#iterations: 239
currently lose_sum: 92.89251327514648
time_elpased: 1.803
start validation test
0.591804123711
0.613912823112
0.498610682309
0.550286785167
0.591967739172
65.327
batch start
#iterations: 240
currently lose_sum: 93.37031811475754
time_elpased: 1.742
batch start
#iterations: 241
currently lose_sum: 93.37204271554947
time_elpased: 1.776
batch start
#iterations: 242
currently lose_sum: 93.18039929866791
time_elpased: 1.768
batch start
#iterations: 243
currently lose_sum: 93.09982758760452
time_elpased: 1.808
batch start
#iterations: 244
currently lose_sum: 92.95899987220764
time_elpased: 1.807
batch start
#iterations: 245
currently lose_sum: 93.05289310216904
time_elpased: 1.796
batch start
#iterations: 246
currently lose_sum: 93.0977913737297
time_elpased: 1.755
batch start
#iterations: 247
currently lose_sum: 93.49128222465515
time_elpased: 1.776
batch start
#iterations: 248
currently lose_sum: 93.2909078001976
time_elpased: 1.804
batch start
#iterations: 249
currently lose_sum: 92.21280246973038
time_elpased: 1.776
batch start
#iterations: 250
currently lose_sum: 93.15101379156113
time_elpased: 1.802
batch start
#iterations: 251
currently lose_sum: 92.8711696267128
time_elpased: 1.777
batch start
#iterations: 252
currently lose_sum: 93.11271613836288
time_elpased: 1.782
batch start
#iterations: 253
currently lose_sum: 92.8791155219078
time_elpased: 1.81
batch start
#iterations: 254
currently lose_sum: 92.59393709897995
time_elpased: 1.827
batch start
#iterations: 255
currently lose_sum: 92.53206074237823
time_elpased: 1.878
batch start
#iterations: 256
currently lose_sum: 93.03587204217911
time_elpased: 1.883
batch start
#iterations: 257
currently lose_sum: 92.83129411935806
time_elpased: 1.924
batch start
#iterations: 258
currently lose_sum: 92.91777861118317
time_elpased: 1.897
batch start
#iterations: 259
currently lose_sum: 92.40377068519592
time_elpased: 1.825
start validation test
0.593711340206
0.609447691757
0.525779561593
0.56453038674
0.593830604921
65.202
batch start
#iterations: 260
currently lose_sum: 92.24641793966293
time_elpased: 1.852
batch start
#iterations: 261
currently lose_sum: 92.86005651950836
time_elpased: 1.979
batch start
#iterations: 262
currently lose_sum: 92.40702998638153
time_elpased: 1.923
batch start
#iterations: 263
currently lose_sum: 92.59125167131424
time_elpased: 1.904
batch start
#iterations: 264
currently lose_sum: 92.54440557956696
time_elpased: 1.933
batch start
#iterations: 265
currently lose_sum: 92.47712051868439
time_elpased: 1.894
batch start
#iterations: 266
currently lose_sum: 91.95562255382538
time_elpased: 1.86
batch start
#iterations: 267
currently lose_sum: 92.75313621759415
time_elpased: 1.81
batch start
#iterations: 268
currently lose_sum: 92.55589336156845
time_elpased: 1.828
batch start
#iterations: 269
currently lose_sum: 92.13863754272461
time_elpased: 1.793
batch start
#iterations: 270
currently lose_sum: 92.49928307533264
time_elpased: 1.796
batch start
#iterations: 271
currently lose_sum: 92.39859956502914
time_elpased: 1.828
batch start
#iterations: 272
currently lose_sum: 92.04074209928513
time_elpased: 1.824
batch start
#iterations: 273
currently lose_sum: 92.37310111522675
time_elpased: 1.779
batch start
#iterations: 274
currently lose_sum: 92.18589568138123
time_elpased: 1.783
batch start
#iterations: 275
currently lose_sum: 92.26991498470306
time_elpased: 1.798
batch start
#iterations: 276
currently lose_sum: 92.21155208349228
time_elpased: 1.808
batch start
#iterations: 277
currently lose_sum: 92.34547996520996
time_elpased: 1.763
batch start
#iterations: 278
currently lose_sum: 92.38031005859375
time_elpased: 1.766
batch start
#iterations: 279
currently lose_sum: 92.10966628789902
time_elpased: 1.767
start validation test
0.585309278351
0.60888252149
0.481115570649
0.537510778959
0.585492206475
66.078
batch start
#iterations: 280
currently lose_sum: 92.01087462902069
time_elpased: 1.811
batch start
#iterations: 281
currently lose_sum: 92.20101976394653
time_elpased: 1.768
batch start
#iterations: 282
currently lose_sum: 91.71352803707123
time_elpased: 1.821
batch start
#iterations: 283
currently lose_sum: 92.48265475034714
time_elpased: 1.753
batch start
#iterations: 284
currently lose_sum: 91.55114668607712
time_elpased: 1.803
batch start
#iterations: 285
currently lose_sum: 91.49323052167892
time_elpased: 1.79
batch start
#iterations: 286
currently lose_sum: 91.94109743833542
time_elpased: 1.751
batch start
#iterations: 287
currently lose_sum: 91.75868153572083
time_elpased: 1.782
batch start
#iterations: 288
currently lose_sum: 91.32836627960205
time_elpased: 1.773
batch start
#iterations: 289
currently lose_sum: 91.5119115114212
time_elpased: 1.849
batch start
#iterations: 290
currently lose_sum: 91.33645731210709
time_elpased: 1.829
batch start
#iterations: 291
currently lose_sum: 91.33283364772797
time_elpased: 1.827
batch start
#iterations: 292
currently lose_sum: 91.20569485425949
time_elpased: 1.776
batch start
#iterations: 293
currently lose_sum: 91.22297662496567
time_elpased: 1.747
batch start
#iterations: 294
currently lose_sum: 91.98556762933731
time_elpased: 1.73
batch start
#iterations: 295
currently lose_sum: 91.1501841545105
time_elpased: 1.764
batch start
#iterations: 296
currently lose_sum: 92.03819996118546
time_elpased: 1.806
batch start
#iterations: 297
currently lose_sum: 91.2831237912178
time_elpased: 1.739
batch start
#iterations: 298
currently lose_sum: 91.49142116308212
time_elpased: 1.783
batch start
#iterations: 299
currently lose_sum: 91.15860724449158
time_elpased: 1.77
start validation test
0.584278350515
0.61568627451
0.452403005043
0.521563742066
0.584509878025
66.271
batch start
#iterations: 300
currently lose_sum: 91.54553282260895
time_elpased: 1.748
batch start
#iterations: 301
currently lose_sum: 91.29243278503418
time_elpased: 1.775
batch start
#iterations: 302
currently lose_sum: 91.17573165893555
time_elpased: 1.74
batch start
#iterations: 303
currently lose_sum: 91.34084290266037
time_elpased: 1.822
batch start
#iterations: 304
currently lose_sum: 91.35801756381989
time_elpased: 1.758
batch start
#iterations: 305
currently lose_sum: 91.27855885028839
time_elpased: 1.779
batch start
#iterations: 306
currently lose_sum: 91.49595510959625
time_elpased: 1.796
batch start
#iterations: 307
currently lose_sum: 91.26242476701736
time_elpased: 1.75
batch start
#iterations: 308
currently lose_sum: 90.93114972114563
time_elpased: 1.768
batch start
#iterations: 309
currently lose_sum: 91.30670803785324
time_elpased: 1.802
batch start
#iterations: 310
currently lose_sum: 91.38206827640533
time_elpased: 1.749
batch start
#iterations: 311
currently lose_sum: 90.86993855237961
time_elpased: 1.759
batch start
#iterations: 312
currently lose_sum: 90.81625896692276
time_elpased: 1.788
batch start
#iterations: 313
currently lose_sum: 90.92246001958847
time_elpased: 1.802
batch start
#iterations: 314
currently lose_sum: 90.99102544784546
time_elpased: 1.785
batch start
#iterations: 315
currently lose_sum: 90.90992206335068
time_elpased: 1.791
batch start
#iterations: 316
currently lose_sum: 90.95745182037354
time_elpased: 1.824
batch start
#iterations: 317
currently lose_sum: 90.20441454648972
time_elpased: 1.821
batch start
#iterations: 318
currently lose_sum: 90.59242516756058
time_elpased: 1.815
batch start
#iterations: 319
currently lose_sum: 90.752789914608
time_elpased: 1.788
start validation test
0.584948453608
0.62066966227
0.440670988988
0.515406836784
0.585201754951
66.985
batch start
#iterations: 320
currently lose_sum: 90.97190982103348
time_elpased: 1.781
batch start
#iterations: 321
currently lose_sum: 90.88454514741898
time_elpased: 1.77
batch start
#iterations: 322
currently lose_sum: 91.10762709379196
time_elpased: 1.775
batch start
#iterations: 323
currently lose_sum: 90.70656841993332
time_elpased: 1.801
batch start
#iterations: 324
currently lose_sum: 90.43652468919754
time_elpased: 1.771
batch start
#iterations: 325
currently lose_sum: 90.27968513965607
time_elpased: 1.758
batch start
#iterations: 326
currently lose_sum: 90.30209970474243
time_elpased: 1.83
batch start
#iterations: 327
currently lose_sum: 90.64720261096954
time_elpased: 1.792
batch start
#iterations: 328
currently lose_sum: 90.02062249183655
time_elpased: 1.787
batch start
#iterations: 329
currently lose_sum: 90.39185577630997
time_elpased: 1.783
batch start
#iterations: 330
currently lose_sum: 90.35323959589005
time_elpased: 1.805
batch start
#iterations: 331
currently lose_sum: 90.38651037216187
time_elpased: 1.825
batch start
#iterations: 332
currently lose_sum: 90.45048427581787
time_elpased: 1.775
batch start
#iterations: 333
currently lose_sum: 90.28387075662613
time_elpased: 1.796
batch start
#iterations: 334
currently lose_sum: 90.21617805957794
time_elpased: 1.799
batch start
#iterations: 335
currently lose_sum: 90.1877549290657
time_elpased: 1.826
batch start
#iterations: 336
currently lose_sum: 89.61386215686798
time_elpased: 1.749
batch start
#iterations: 337
currently lose_sum: 90.28290045261383
time_elpased: 1.791
batch start
#iterations: 338
currently lose_sum: 89.82827705144882
time_elpased: 1.785
batch start
#iterations: 339
currently lose_sum: 90.12420243024826
time_elpased: 1.782
start validation test
0.586134020619
0.610615989515
0.479468971905
0.537153398282
0.586321287564
66.891
batch start
#iterations: 340
currently lose_sum: 89.9388455748558
time_elpased: 1.845
batch start
#iterations: 341
currently lose_sum: 89.81316512823105
time_elpased: 1.843
batch start
#iterations: 342
currently lose_sum: 90.11140811443329
time_elpased: 1.822
batch start
#iterations: 343
currently lose_sum: 89.60980212688446
time_elpased: 1.815
batch start
#iterations: 344
currently lose_sum: 90.06574827432632
time_elpased: 1.839
batch start
#iterations: 345
currently lose_sum: 89.6151515841484
time_elpased: 1.833
batch start
#iterations: 346
currently lose_sum: 89.53169775009155
time_elpased: 1.877
batch start
#iterations: 347
currently lose_sum: 89.66508346796036
time_elpased: 1.763
batch start
#iterations: 348
currently lose_sum: 89.87703025341034
time_elpased: 1.772
batch start
#iterations: 349
currently lose_sum: 89.70597577095032
time_elpased: 1.881
batch start
#iterations: 350
currently lose_sum: 89.46109521389008
time_elpased: 1.819
batch start
#iterations: 351
currently lose_sum: 89.33158367872238
time_elpased: 1.777
batch start
#iterations: 352
currently lose_sum: 90.26207166910172
time_elpased: 1.769
batch start
#iterations: 353
currently lose_sum: 89.87127149105072
time_elpased: 1.755
batch start
#iterations: 354
currently lose_sum: 89.65903586149216
time_elpased: 1.806
batch start
#iterations: 355
currently lose_sum: 89.44774001836777
time_elpased: 1.82
batch start
#iterations: 356
currently lose_sum: 89.41739577054977
time_elpased: 1.75
batch start
#iterations: 357
currently lose_sum: 89.39990490674973
time_elpased: 1.804
batch start
#iterations: 358
currently lose_sum: 89.41534072160721
time_elpased: 1.755
batch start
#iterations: 359
currently lose_sum: 89.44743114709854
time_elpased: 1.755
start validation test
0.58381443299
0.619630115043
0.437892353607
0.513145200193
0.584070621707
67.950
batch start
#iterations: 360
currently lose_sum: 89.5822479724884
time_elpased: 1.8
batch start
#iterations: 361
currently lose_sum: 89.29259276390076
time_elpased: 1.784
batch start
#iterations: 362
currently lose_sum: 89.46738594770432
time_elpased: 1.769
batch start
#iterations: 363
currently lose_sum: 89.21080923080444
time_elpased: 1.795
batch start
#iterations: 364
currently lose_sum: 89.74442130327225
time_elpased: 1.869
batch start
#iterations: 365
currently lose_sum: 89.12174236774445
time_elpased: 1.764
batch start
#iterations: 366
currently lose_sum: 88.7677788734436
time_elpased: 1.772
batch start
#iterations: 367
currently lose_sum: 89.34989458322525
time_elpased: 1.76
batch start
#iterations: 368
currently lose_sum: 89.09787231683731
time_elpased: 1.811
batch start
#iterations: 369
currently lose_sum: 89.13624584674835
time_elpased: 1.762
batch start
#iterations: 370
currently lose_sum: 89.09224200248718
time_elpased: 1.762
batch start
#iterations: 371
currently lose_sum: 88.74770092964172
time_elpased: 1.779
batch start
#iterations: 372
currently lose_sum: 88.81644290685654
time_elpased: 1.74
batch start
#iterations: 373
currently lose_sum: 88.9765236377716
time_elpased: 1.786
batch start
#iterations: 374
currently lose_sum: 88.85725319385529
time_elpased: 1.731
batch start
#iterations: 375
currently lose_sum: 88.55531376600266
time_elpased: 1.794
batch start
#iterations: 376
currently lose_sum: 88.92599421739578
time_elpased: 1.827
batch start
#iterations: 377
currently lose_sum: 88.74656635522842
time_elpased: 1.752
batch start
#iterations: 378
currently lose_sum: 88.98284059762955
time_elpased: 1.77
batch start
#iterations: 379
currently lose_sum: 88.67890936136246
time_elpased: 1.767
start validation test
0.570360824742
0.60406626506
0.412781722754
0.490432230849
0.570637479161
68.834
batch start
#iterations: 380
currently lose_sum: 88.43170142173767
time_elpased: 1.782
batch start
#iterations: 381
currently lose_sum: 88.75811541080475
time_elpased: 1.774
batch start
#iterations: 382
currently lose_sum: 88.49316012859344
time_elpased: 1.81
batch start
#iterations: 383
currently lose_sum: 88.49624186754227
time_elpased: 1.775
batch start
#iterations: 384
currently lose_sum: 88.76284492015839
time_elpased: 1.784
batch start
#iterations: 385
currently lose_sum: 88.15386801958084
time_elpased: 1.754
batch start
#iterations: 386
currently lose_sum: 88.33911746740341
time_elpased: 1.793
batch start
#iterations: 387
currently lose_sum: 88.5281754732132
time_elpased: 1.766
batch start
#iterations: 388
currently lose_sum: 88.96608835458755
time_elpased: 1.782
batch start
#iterations: 389
currently lose_sum: 88.50690191984177
time_elpased: 1.761
batch start
#iterations: 390
currently lose_sum: 88.24805265665054
time_elpased: 1.754
batch start
#iterations: 391
currently lose_sum: 88.13210010528564
time_elpased: 1.792
batch start
#iterations: 392
currently lose_sum: 88.43114000558853
time_elpased: 1.796
batch start
#iterations: 393
currently lose_sum: 88.37855511903763
time_elpased: 1.791
batch start
#iterations: 394
currently lose_sum: 87.53597539663315
time_elpased: 1.757
batch start
#iterations: 395
currently lose_sum: 87.73754960298538
time_elpased: 1.828
batch start
#iterations: 396
currently lose_sum: 88.65044510364532
time_elpased: 1.769
batch start
#iterations: 397
currently lose_sum: 88.68568205833435
time_elpased: 1.772
batch start
#iterations: 398
currently lose_sum: 87.74345874786377
time_elpased: 1.777
batch start
#iterations: 399
currently lose_sum: 87.82627457380295
time_elpased: 1.759
start validation test
0.56824742268
0.593579902303
0.437686528764
0.503850254709
0.568476642467
68.848
acc: 0.622
pre: 0.637
rec: 0.571
F1: 0.602
auc: 0.622
