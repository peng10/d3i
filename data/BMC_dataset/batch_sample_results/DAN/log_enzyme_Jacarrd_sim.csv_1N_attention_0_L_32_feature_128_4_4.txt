start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.5896018743515
time_elpased: 2.094
batch start
#iterations: 1
currently lose_sum: 100.38517600297928
time_elpased: 2.02
batch start
#iterations: 2
currently lose_sum: 100.42096710205078
time_elpased: 2.047
batch start
#iterations: 3
currently lose_sum: 100.2632400393486
time_elpased: 2.043
batch start
#iterations: 4
currently lose_sum: 100.2237656712532
time_elpased: 2.066
batch start
#iterations: 5
currently lose_sum: 100.12632435560226
time_elpased: 2.039
batch start
#iterations: 6
currently lose_sum: 100.04180312156677
time_elpased: 2.057
batch start
#iterations: 7
currently lose_sum: 99.97173792123795
time_elpased: 2.068
batch start
#iterations: 8
currently lose_sum: 99.94021475315094
time_elpased: 2.06
batch start
#iterations: 9
currently lose_sum: 99.91722279787064
time_elpased: 2.006
batch start
#iterations: 10
currently lose_sum: 99.83480197191238
time_elpased: 2.08
batch start
#iterations: 11
currently lose_sum: 99.82936465740204
time_elpased: 2.063
batch start
#iterations: 12
currently lose_sum: 99.58028221130371
time_elpased: 2.074
batch start
#iterations: 13
currently lose_sum: 99.69671750068665
time_elpased: 2.027
batch start
#iterations: 14
currently lose_sum: 99.45867079496384
time_elpased: 2.042
batch start
#iterations: 15
currently lose_sum: 99.42069166898727
time_elpased: 2.016
batch start
#iterations: 16
currently lose_sum: 99.32051134109497
time_elpased: 1.978
batch start
#iterations: 17
currently lose_sum: 99.33513724803925
time_elpased: 2.037
batch start
#iterations: 18
currently lose_sum: 99.19457882642746
time_elpased: 2.028
batch start
#iterations: 19
currently lose_sum: 99.48010414838791
time_elpased: 2.026
start validation test
0.607525773196
0.599961973572
0.649480292271
0.623739869539
0.607452115567
64.953
batch start
#iterations: 20
currently lose_sum: 99.2828723192215
time_elpased: 2.023
batch start
#iterations: 21
currently lose_sum: 98.98838913440704
time_elpased: 2.03
batch start
#iterations: 22
currently lose_sum: 98.93904626369476
time_elpased: 2.036
batch start
#iterations: 23
currently lose_sum: 99.12081491947174
time_elpased: 2.057
batch start
#iterations: 24
currently lose_sum: 98.87926059961319
time_elpased: 2.035
batch start
#iterations: 25
currently lose_sum: 98.84105563163757
time_elpased: 2.033
batch start
#iterations: 26
currently lose_sum: 99.0915117263794
time_elpased: 2.045
batch start
#iterations: 27
currently lose_sum: 98.59283649921417
time_elpased: 1.996
batch start
#iterations: 28
currently lose_sum: 98.6559351682663
time_elpased: 1.982
batch start
#iterations: 29
currently lose_sum: 98.4851581454277
time_elpased: 2.033
batch start
#iterations: 30
currently lose_sum: 98.67698776721954
time_elpased: 1.989
batch start
#iterations: 31
currently lose_sum: 98.54068106412888
time_elpased: 2.041
batch start
#iterations: 32
currently lose_sum: 98.77112609148026
time_elpased: 2.035
batch start
#iterations: 33
currently lose_sum: 98.56765937805176
time_elpased: 2.03
batch start
#iterations: 34
currently lose_sum: 98.3914202451706
time_elpased: 2.027
batch start
#iterations: 35
currently lose_sum: 98.7113499045372
time_elpased: 1.995
batch start
#iterations: 36
currently lose_sum: 98.39220118522644
time_elpased: 2.012
batch start
#iterations: 37
currently lose_sum: 98.45127546787262
time_elpased: 2.046
batch start
#iterations: 38
currently lose_sum: 98.57710832357407
time_elpased: 2.028
batch start
#iterations: 39
currently lose_sum: 98.3791076540947
time_elpased: 2.015
start validation test
0.620670103093
0.609613239122
0.674796747967
0.64055096957
0.620575075419
64.323
batch start
#iterations: 40
currently lose_sum: 98.41739147901535
time_elpased: 2.065
batch start
#iterations: 41
currently lose_sum: 98.29553580284119
time_elpased: 2.038
batch start
#iterations: 42
currently lose_sum: 98.31181091070175
time_elpased: 2.047
batch start
#iterations: 43
currently lose_sum: 98.14263689517975
time_elpased: 2.042
batch start
#iterations: 44
currently lose_sum: 98.30436944961548
time_elpased: 2.074
batch start
#iterations: 45
currently lose_sum: 98.0736455321312
time_elpased: 1.99
batch start
#iterations: 46
currently lose_sum: 98.18811929225922
time_elpased: 2.03
batch start
#iterations: 47
currently lose_sum: 98.44450974464417
time_elpased: 2.051
batch start
#iterations: 48
currently lose_sum: 98.11285871267319
time_elpased: 2.053
batch start
#iterations: 49
currently lose_sum: 97.91376382112503
time_elpased: 2.075
batch start
#iterations: 50
currently lose_sum: 97.99495714902878
time_elpased: 2.048
batch start
#iterations: 51
currently lose_sum: 98.00347703695297
time_elpased: 2.043
batch start
#iterations: 52
currently lose_sum: 98.23390871286392
time_elpased: 2.037
batch start
#iterations: 53
currently lose_sum: 97.76316118240356
time_elpased: 1.99
batch start
#iterations: 54
currently lose_sum: 97.91680192947388
time_elpased: 2.063
batch start
#iterations: 55
currently lose_sum: 97.96693289279938
time_elpased: 2.046
batch start
#iterations: 56
currently lose_sum: 97.98710596561432
time_elpased: 2.014
batch start
#iterations: 57
currently lose_sum: 98.08150833845139
time_elpased: 2.046
batch start
#iterations: 58
currently lose_sum: 97.9284862279892
time_elpased: 2.018
batch start
#iterations: 59
currently lose_sum: 98.15719717741013
time_elpased: 1.997
start validation test
0.623144329897
0.646814742495
0.545435834105
0.591815085702
0.623280759147
63.613
batch start
#iterations: 60
currently lose_sum: 97.91996258497238
time_elpased: 2.096
batch start
#iterations: 61
currently lose_sum: 98.00121814012527
time_elpased: 2.077
batch start
#iterations: 62
currently lose_sum: 98.00425285100937
time_elpased: 2.073
batch start
#iterations: 63
currently lose_sum: 97.66501206159592
time_elpased: 2.089
batch start
#iterations: 64
currently lose_sum: 97.87930130958557
time_elpased: 2.034
batch start
#iterations: 65
currently lose_sum: 97.52559196949005
time_elpased: 2.034
batch start
#iterations: 66
currently lose_sum: 98.06400853395462
time_elpased: 2.07
batch start
#iterations: 67
currently lose_sum: 97.49303072690964
time_elpased: 2.072
batch start
#iterations: 68
currently lose_sum: 97.49079954624176
time_elpased: 2.052
batch start
#iterations: 69
currently lose_sum: 97.844870865345
time_elpased: 2.07
batch start
#iterations: 70
currently lose_sum: 97.82884275913239
time_elpased: 2.037
batch start
#iterations: 71
currently lose_sum: 97.86394250392914
time_elpased: 2.071
batch start
#iterations: 72
currently lose_sum: 97.64982634782791
time_elpased: 2.045
batch start
#iterations: 73
currently lose_sum: 97.67743945121765
time_elpased: 2.078
batch start
#iterations: 74
currently lose_sum: 97.48033499717712
time_elpased: 2.029
batch start
#iterations: 75
currently lose_sum: 97.65110850334167
time_elpased: 2.035
batch start
#iterations: 76
currently lose_sum: 97.63995814323425
time_elpased: 2.08
batch start
#iterations: 77
currently lose_sum: 97.50368648767471
time_elpased: 2.022
batch start
#iterations: 78
currently lose_sum: 97.76535856723785
time_elpased: 2.115
batch start
#iterations: 79
currently lose_sum: 97.59714847803116
time_elpased: 2.005
start validation test
0.624175257732
0.62484561548
0.624781311104
0.624813461637
0.624174193712
63.476
batch start
#iterations: 80
currently lose_sum: 97.54716044664383
time_elpased: 2.031
batch start
#iterations: 81
currently lose_sum: 97.5327056646347
time_elpased: 2.049
batch start
#iterations: 82
currently lose_sum: 97.89380306005478
time_elpased: 2.077
batch start
#iterations: 83
currently lose_sum: 97.47574645280838
time_elpased: 2.023
batch start
#iterations: 84
currently lose_sum: 97.57648974657059
time_elpased: 2.06
batch start
#iterations: 85
currently lose_sum: 97.52895653247833
time_elpased: 2.045
batch start
#iterations: 86
currently lose_sum: 97.58045768737793
time_elpased: 2.047
batch start
#iterations: 87
currently lose_sum: 97.6367740035057
time_elpased: 2.021
batch start
#iterations: 88
currently lose_sum: 97.3945095539093
time_elpased: 2.012
batch start
#iterations: 89
currently lose_sum: 97.57512319087982
time_elpased: 2.013
batch start
#iterations: 90
currently lose_sum: 97.09903275966644
time_elpased: 2.037
batch start
#iterations: 91
currently lose_sum: 97.61885744333267
time_elpased: 2.038
batch start
#iterations: 92
currently lose_sum: 97.48751157522202
time_elpased: 2.002
batch start
#iterations: 93
currently lose_sum: 96.99358081817627
time_elpased: 2.043
batch start
#iterations: 94
currently lose_sum: 97.36929732561111
time_elpased: 2.086
batch start
#iterations: 95
currently lose_sum: 97.67068135738373
time_elpased: 2.043
batch start
#iterations: 96
currently lose_sum: 97.54693204164505
time_elpased: 2.11
batch start
#iterations: 97
currently lose_sum: 97.58465731143951
time_elpased: 2.03
batch start
#iterations: 98
currently lose_sum: 97.28342390060425
time_elpased: 2.024
batch start
#iterations: 99
currently lose_sum: 97.49141353368759
time_elpased: 2.05
start validation test
0.620979381443
0.63371040724
0.576515385407
0.603761383844
0.621057444846
63.321
batch start
#iterations: 100
currently lose_sum: 97.16896885633469
time_elpased: 2.109
batch start
#iterations: 101
currently lose_sum: 97.46352326869965
time_elpased: 2.009
batch start
#iterations: 102
currently lose_sum: 97.29502606391907
time_elpased: 2.025
batch start
#iterations: 103
currently lose_sum: 97.3136460185051
time_elpased: 2.077
batch start
#iterations: 104
currently lose_sum: 97.29413080215454
time_elpased: 2.039
batch start
#iterations: 105
currently lose_sum: 97.36963558197021
time_elpased: 2.057
batch start
#iterations: 106
currently lose_sum: 97.34895706176758
time_elpased: 2.051
batch start
#iterations: 107
currently lose_sum: 97.15998250246048
time_elpased: 2.037
batch start
#iterations: 108
currently lose_sum: 97.3571965098381
time_elpased: 2.075
batch start
#iterations: 109
currently lose_sum: 97.2135899066925
time_elpased: 2.051
batch start
#iterations: 110
currently lose_sum: 96.90518397092819
time_elpased: 2.036
batch start
#iterations: 111
currently lose_sum: 97.23127853870392
time_elpased: 2.034
batch start
#iterations: 112
currently lose_sum: 97.14529377222061
time_elpased: 2.016
batch start
#iterations: 113
currently lose_sum: 96.70793431997299
time_elpased: 2.032
batch start
#iterations: 114
currently lose_sum: 97.03032088279724
time_elpased: 2.063
batch start
#iterations: 115
currently lose_sum: 96.91944992542267
time_elpased: 2.054
batch start
#iterations: 116
currently lose_sum: 97.17733758687973
time_elpased: 2.092
batch start
#iterations: 117
currently lose_sum: 96.99473732709885
time_elpased: 1.988
batch start
#iterations: 118
currently lose_sum: 97.19978642463684
time_elpased: 2.054
batch start
#iterations: 119
currently lose_sum: 97.00999218225479
time_elpased: 2.08
start validation test
0.621907216495
0.631689517912
0.587938664197
0.609029369437
0.621966853528
63.297
batch start
#iterations: 120
currently lose_sum: 96.98391163349152
time_elpased: 2.017
batch start
#iterations: 121
currently lose_sum: 96.9807962179184
time_elpased: 2.048
batch start
#iterations: 122
currently lose_sum: 97.02040487527847
time_elpased: 2.017
batch start
#iterations: 123
currently lose_sum: 96.80636513233185
time_elpased: 2.028
batch start
#iterations: 124
currently lose_sum: 96.91226309537888
time_elpased: 2.085
batch start
#iterations: 125
currently lose_sum: 96.90189927816391
time_elpased: 2.0
batch start
#iterations: 126
currently lose_sum: 96.55073469877243
time_elpased: 2.039
batch start
#iterations: 127
currently lose_sum: 96.94578647613525
time_elpased: 2.042
batch start
#iterations: 128
currently lose_sum: 96.75384044647217
time_elpased: 2.069
batch start
#iterations: 129
currently lose_sum: 96.93248850107193
time_elpased: 2.028
batch start
#iterations: 130
currently lose_sum: 96.78406184911728
time_elpased: 2.029
batch start
#iterations: 131
currently lose_sum: 96.69593065977097
time_elpased: 2.05
batch start
#iterations: 132
currently lose_sum: 96.71557009220123
time_elpased: 2.017
batch start
#iterations: 133
currently lose_sum: 96.6448147892952
time_elpased: 2.05
batch start
#iterations: 134
currently lose_sum: 97.06637209653854
time_elpased: 2.097
batch start
#iterations: 135
currently lose_sum: 97.09132701158524
time_elpased: 2.014
batch start
#iterations: 136
currently lose_sum: 96.61155897378922
time_elpased: 2.069
batch start
#iterations: 137
currently lose_sum: 96.80184257030487
time_elpased: 2.013
batch start
#iterations: 138
currently lose_sum: 96.57728010416031
time_elpased: 2.007
batch start
#iterations: 139
currently lose_sum: 96.71937698125839
time_elpased: 2.021
start validation test
0.605206185567
0.623499759962
0.534630029845
0.575655160951
0.605330092894
64.282
batch start
#iterations: 140
currently lose_sum: 96.57718968391418
time_elpased: 2.114
batch start
#iterations: 141
currently lose_sum: 96.88193637132645
time_elpased: 2.028
batch start
#iterations: 142
currently lose_sum: 96.71950227022171
time_elpased: 2.079
batch start
#iterations: 143
currently lose_sum: 96.53450334072113
time_elpased: 2.043
batch start
#iterations: 144
currently lose_sum: 96.70034599304199
time_elpased: 2.055
batch start
#iterations: 145
currently lose_sum: 96.6639643907547
time_elpased: 2.093
batch start
#iterations: 146
currently lose_sum: 96.64872145652771
time_elpased: 2.068
batch start
#iterations: 147
currently lose_sum: 96.91415876150131
time_elpased: 2.002
batch start
#iterations: 148
currently lose_sum: 96.42212057113647
time_elpased: 2.079
batch start
#iterations: 149
currently lose_sum: 96.67599016427994
time_elpased: 2.02
batch start
#iterations: 150
currently lose_sum: 96.23644983768463
time_elpased: 2.09
batch start
#iterations: 151
currently lose_sum: 96.57735645771027
time_elpased: 2.04
batch start
#iterations: 152
currently lose_sum: 96.3245644569397
time_elpased: 2.016
batch start
#iterations: 153
currently lose_sum: 96.87084871530533
time_elpased: 2.068
batch start
#iterations: 154
currently lose_sum: 96.15228325128555
time_elpased: 2.056
batch start
#iterations: 155
currently lose_sum: 96.11931550502777
time_elpased: 2.024
batch start
#iterations: 156
currently lose_sum: 96.76279884576797
time_elpased: 2.047
batch start
#iterations: 157
currently lose_sum: 96.12170201539993
time_elpased: 2.049
batch start
#iterations: 158
currently lose_sum: 96.3496081829071
time_elpased: 2.02
batch start
#iterations: 159
currently lose_sum: 96.51872706413269
time_elpased: 2.018
start validation test
0.611649484536
0.635069917089
0.528146547288
0.576694010563
0.611796086822
63.617
batch start
#iterations: 160
currently lose_sum: 96.2643648982048
time_elpased: 2.068
batch start
#iterations: 161
currently lose_sum: 96.27143847942352
time_elpased: 2.022
batch start
#iterations: 162
currently lose_sum: 96.43272387981415
time_elpased: 2.058
batch start
#iterations: 163
currently lose_sum: 96.18792599439621
time_elpased: 2.024
batch start
#iterations: 164
currently lose_sum: 96.41861975193024
time_elpased: 2.027
batch start
#iterations: 165
currently lose_sum: 96.02300107479095
time_elpased: 1.996
batch start
#iterations: 166
currently lose_sum: 95.88564592599869
time_elpased: 2.072
batch start
#iterations: 167
currently lose_sum: 96.30083292722702
time_elpased: 2.063
batch start
#iterations: 168
currently lose_sum: 96.61135244369507
time_elpased: 2.057
batch start
#iterations: 169
currently lose_sum: 95.9367024898529
time_elpased: 2.112
batch start
#iterations: 170
currently lose_sum: 96.07881706953049
time_elpased: 2.075
batch start
#iterations: 171
currently lose_sum: 96.07577931880951
time_elpased: 2.071
batch start
#iterations: 172
currently lose_sum: 95.90681129693985
time_elpased: 2.034
batch start
#iterations: 173
currently lose_sum: 95.79399991035461
time_elpased: 2.016
batch start
#iterations: 174
currently lose_sum: 96.16752660274506
time_elpased: 2.038
batch start
#iterations: 175
currently lose_sum: 95.9564761519432
time_elpased: 2.037
batch start
#iterations: 176
currently lose_sum: 95.67760586738586
time_elpased: 2.061
batch start
#iterations: 177
currently lose_sum: 95.67470848560333
time_elpased: 2.082
batch start
#iterations: 178
currently lose_sum: 95.78842031955719
time_elpased: 2.025
batch start
#iterations: 179
currently lose_sum: 95.34658551216125
time_elpased: 2.054
start validation test
0.621701030928
0.648143533516
0.535350416795
0.586372090402
0.62185263275
63.352
batch start
#iterations: 180
currently lose_sum: 95.92138367891312
time_elpased: 2.134
batch start
#iterations: 181
currently lose_sum: 95.94606095552444
time_elpased: 2.044
batch start
#iterations: 182
currently lose_sum: 95.31469517946243
time_elpased: 2.048
batch start
#iterations: 183
currently lose_sum: 95.3590726852417
time_elpased: 2.036
batch start
#iterations: 184
currently lose_sum: 95.5597904920578
time_elpased: 2.026
batch start
#iterations: 185
currently lose_sum: 95.10028737783432
time_elpased: 2.099
batch start
#iterations: 186
currently lose_sum: 95.68835508823395
time_elpased: 2.014
batch start
#iterations: 187
currently lose_sum: 95.55571866035461
time_elpased: 1.96
batch start
#iterations: 188
currently lose_sum: 95.43041050434113
time_elpased: 2.031
batch start
#iterations: 189
currently lose_sum: 95.27605551481247
time_elpased: 2.003
batch start
#iterations: 190
currently lose_sum: 95.70383590459824
time_elpased: 2.055
batch start
#iterations: 191
currently lose_sum: 95.47096520662308
time_elpased: 2.007
batch start
#iterations: 192
currently lose_sum: 95.69193387031555
time_elpased: 2.019
batch start
#iterations: 193
currently lose_sum: 95.37750768661499
time_elpased: 2.073
batch start
#iterations: 194
currently lose_sum: 95.26093393564224
time_elpased: 2.07
batch start
#iterations: 195
currently lose_sum: 94.79232686758041
time_elpased: 2.053
batch start
#iterations: 196
currently lose_sum: 94.575310587883
time_elpased: 2.025
batch start
#iterations: 197
currently lose_sum: 95.454057097435
time_elpased: 2.049
batch start
#iterations: 198
currently lose_sum: 95.12268996238708
time_elpased: 2.049
batch start
#iterations: 199
currently lose_sum: 95.21482020616531
time_elpased: 2.023
start validation test
0.608350515464
0.611467648606
0.598126993928
0.604723754032
0.608368464433
64.333
batch start
#iterations: 200
currently lose_sum: 95.27550554275513
time_elpased: 2.053
batch start
#iterations: 201
currently lose_sum: 95.13157850503922
time_elpased: 2.045
batch start
#iterations: 202
currently lose_sum: 94.80932986736298
time_elpased: 2.055
batch start
#iterations: 203
currently lose_sum: 95.2069051861763
time_elpased: 2.035
batch start
#iterations: 204
currently lose_sum: 95.41124212741852
time_elpased: 2.07
batch start
#iterations: 205
currently lose_sum: 94.99001944065094
time_elpased: 1.993
batch start
#iterations: 206
currently lose_sum: 94.47294503450394
time_elpased: 2.055
batch start
#iterations: 207
currently lose_sum: 95.17484259605408
time_elpased: 2.045
batch start
#iterations: 208
currently lose_sum: 94.82058072090149
time_elpased: 2.042
batch start
#iterations: 209
currently lose_sum: 94.4444277882576
time_elpased: 2.063
batch start
#iterations: 210
currently lose_sum: 94.46060633659363
time_elpased: 2.033
batch start
#iterations: 211
currently lose_sum: 94.5421108007431
time_elpased: 2.055
batch start
#iterations: 212
currently lose_sum: 94.51536750793457
time_elpased: 2.044
batch start
#iterations: 213
currently lose_sum: 94.49175900220871
time_elpased: 2.082
batch start
#iterations: 214
currently lose_sum: 94.683678150177
time_elpased: 2.043
batch start
#iterations: 215
currently lose_sum: 94.26891469955444
time_elpased: 1.991
batch start
#iterations: 216
currently lose_sum: 94.42415273189545
time_elpased: 2.084
batch start
#iterations: 217
currently lose_sum: 94.3428435921669
time_elpased: 2.062
batch start
#iterations: 218
currently lose_sum: 94.41235333681107
time_elpased: 2.073
batch start
#iterations: 219
currently lose_sum: 93.96260875463486
time_elpased: 1.99
start validation test
0.601546391753
0.632696674235
0.487496140784
0.550685887003
0.601746624559
64.800
batch start
#iterations: 220
currently lose_sum: 94.25835585594177
time_elpased: 2.08
batch start
#iterations: 221
currently lose_sum: 94.40332645177841
time_elpased: 2.073
batch start
#iterations: 222
currently lose_sum: 94.16080594062805
time_elpased: 2.069
batch start
#iterations: 223
currently lose_sum: 94.26933151483536
time_elpased: 2.039
batch start
#iterations: 224
currently lose_sum: 94.18511509895325
time_elpased: 2.028
batch start
#iterations: 225
currently lose_sum: 93.99736416339874
time_elpased: 2.033
batch start
#iterations: 226
currently lose_sum: 93.75995606184006
time_elpased: 2.063
batch start
#iterations: 227
currently lose_sum: 93.52822589874268
time_elpased: 2.01
batch start
#iterations: 228
currently lose_sum: 94.2233134508133
time_elpased: 2.032
batch start
#iterations: 229
currently lose_sum: 93.55686432123184
time_elpased: 2.028
batch start
#iterations: 230
currently lose_sum: 93.920012652874
time_elpased: 2.028
batch start
#iterations: 231
currently lose_sum: 93.56659108400345
time_elpased: 2.028
batch start
#iterations: 232
currently lose_sum: 93.7951632142067
time_elpased: 2.033
batch start
#iterations: 233
currently lose_sum: 93.67702209949493
time_elpased: 2.058
batch start
#iterations: 234
currently lose_sum: 93.79133194684982
time_elpased: 2.074
batch start
#iterations: 235
currently lose_sum: 93.53611099720001
time_elpased: 2.059
batch start
#iterations: 236
currently lose_sum: 93.34097850322723
time_elpased: 2.112
batch start
#iterations: 237
currently lose_sum: 93.30052250623703
time_elpased: 2.051
batch start
#iterations: 238
currently lose_sum: 93.32740187644958
time_elpased: 1.989
batch start
#iterations: 239
currently lose_sum: 93.31373918056488
time_elpased: 2.044
start validation test
0.603762886598
0.630832688837
0.503653390964
0.560114449213
0.603938644258
65.623
batch start
#iterations: 240
currently lose_sum: 93.35181665420532
time_elpased: 2.037
batch start
#iterations: 241
currently lose_sum: 93.12114351987839
time_elpased: 2.03
batch start
#iterations: 242
currently lose_sum: 92.72292059659958
time_elpased: 2.087
batch start
#iterations: 243
currently lose_sum: 93.18515801429749
time_elpased: 2.044
batch start
#iterations: 244
currently lose_sum: 93.01305139064789
time_elpased: 2.055
batch start
#iterations: 245
currently lose_sum: 92.60692685842514
time_elpased: 2.106
batch start
#iterations: 246
currently lose_sum: 92.71351063251495
time_elpased: 2.031
batch start
#iterations: 247
currently lose_sum: 92.63396328687668
time_elpased: 1.993
batch start
#iterations: 248
currently lose_sum: 92.53665125370026
time_elpased: 2.048
batch start
#iterations: 249
currently lose_sum: 92.56423097848892
time_elpased: 2.013
batch start
#iterations: 250
currently lose_sum: 92.53298532962799
time_elpased: 2.041
batch start
#iterations: 251
currently lose_sum: 92.57414150238037
time_elpased: 2.053
batch start
#iterations: 252
currently lose_sum: 92.39151388406754
time_elpased: 2.018
batch start
#iterations: 253
currently lose_sum: 92.30911177396774
time_elpased: 2.023
batch start
#iterations: 254
currently lose_sum: 92.22398018836975
time_elpased: 2.052
batch start
#iterations: 255
currently lose_sum: 92.17728143930435
time_elpased: 1.997
batch start
#iterations: 256
currently lose_sum: 92.00231623649597
time_elpased: 2.047
batch start
#iterations: 257
currently lose_sum: 92.24709016084671
time_elpased: 2.033
batch start
#iterations: 258
currently lose_sum: 91.87298089265823
time_elpased: 2.007
batch start
#iterations: 259
currently lose_sum: 91.54136937856674
time_elpased: 2.034
start validation test
0.599226804124
0.620681083768
0.513944633117
0.562292405562
0.599376530129
66.949
batch start
#iterations: 260
currently lose_sum: 92.04707360267639
time_elpased: 2.068
batch start
#iterations: 261
currently lose_sum: 91.27576851844788
time_elpased: 2.067
batch start
#iterations: 262
currently lose_sum: 91.92880320549011
time_elpased: 2.041
batch start
#iterations: 263
currently lose_sum: 91.30744582414627
time_elpased: 2.075
batch start
#iterations: 264
currently lose_sum: 91.56062436103821
time_elpased: 2.002
batch start
#iterations: 265
currently lose_sum: 91.8522355556488
time_elpased: 2.023
batch start
#iterations: 266
currently lose_sum: 91.752938747406
time_elpased: 2.072
batch start
#iterations: 267
currently lose_sum: 90.64160639047623
time_elpased: 2.032
batch start
#iterations: 268
currently lose_sum: 91.31799167394638
time_elpased: 2.061
batch start
#iterations: 269
currently lose_sum: 91.60193765163422
time_elpased: 2.034
batch start
#iterations: 270
currently lose_sum: 90.92142587900162
time_elpased: 2.086
batch start
#iterations: 271
currently lose_sum: 90.28075033426285
time_elpased: 2.029
batch start
#iterations: 272
currently lose_sum: 91.4735779762268
time_elpased: 2.058
batch start
#iterations: 273
currently lose_sum: 90.83600497245789
time_elpased: 2.009
batch start
#iterations: 274
currently lose_sum: 90.63055276870728
time_elpased: 2.056
batch start
#iterations: 275
currently lose_sum: 90.98418354988098
time_elpased: 2.017
batch start
#iterations: 276
currently lose_sum: 90.45614004135132
time_elpased: 2.064
batch start
#iterations: 277
currently lose_sum: 90.51538002490997
time_elpased: 2.074
batch start
#iterations: 278
currently lose_sum: 90.15425103902817
time_elpased: 2.055
batch start
#iterations: 279
currently lose_sum: 90.28239393234253
time_elpased: 2.052
start validation test
0.594484536082
0.625270855905
0.475146650201
0.539968422899
0.594694052148
67.249
batch start
#iterations: 280
currently lose_sum: 90.0363290309906
time_elpased: 2.093
batch start
#iterations: 281
currently lose_sum: 90.36155390739441
time_elpased: 2.04
batch start
#iterations: 282
currently lose_sum: 89.82274961471558
time_elpased: 2.041
batch start
#iterations: 283
currently lose_sum: 90.4069994688034
time_elpased: 2.049
batch start
#iterations: 284
currently lose_sum: 89.65969717502594
time_elpased: 2.057
batch start
#iterations: 285
currently lose_sum: 89.51852148771286
time_elpased: 2.052
batch start
#iterations: 286
currently lose_sum: 90.07771021127701
time_elpased: 2.055
batch start
#iterations: 287
currently lose_sum: 89.26167339086533
time_elpased: 2.044
batch start
#iterations: 288
currently lose_sum: 89.46699684858322
time_elpased: 2.048
batch start
#iterations: 289
currently lose_sum: 89.65743088722229
time_elpased: 2.072
batch start
#iterations: 290
currently lose_sum: 89.65428793430328
time_elpased: 1.991
batch start
#iterations: 291
currently lose_sum: 88.47280633449554
time_elpased: 2.022
batch start
#iterations: 292
currently lose_sum: 88.62353479862213
time_elpased: 2.064
batch start
#iterations: 293
currently lose_sum: 89.00256723165512
time_elpased: 2.055
batch start
#iterations: 294
currently lose_sum: 88.70498484373093
time_elpased: 2.039
batch start
#iterations: 295
currently lose_sum: 88.63850045204163
time_elpased: 2.072
batch start
#iterations: 296
currently lose_sum: 88.50447362661362
time_elpased: 2.062
batch start
#iterations: 297
currently lose_sum: 88.62911742925644
time_elpased: 2.045
batch start
#iterations: 298
currently lose_sum: 88.30251640081406
time_elpased: 2.031
batch start
#iterations: 299
currently lose_sum: 88.35807585716248
time_elpased: 2.047
start validation test
0.588659793814
0.632494279176
0.42667489966
0.509587020649
0.588944183281
71.395
batch start
#iterations: 300
currently lose_sum: 88.61281424760818
time_elpased: 2.044
batch start
#iterations: 301
currently lose_sum: 88.60556048154831
time_elpased: 2.051
batch start
#iterations: 302
currently lose_sum: 87.90638715028763
time_elpased: 2.04
batch start
#iterations: 303
currently lose_sum: 87.9334619641304
time_elpased: 2.02
batch start
#iterations: 304
currently lose_sum: 88.2161710858345
time_elpased: 2.045
batch start
#iterations: 305
currently lose_sum: 88.04914683103561
time_elpased: 2.028
batch start
#iterations: 306
currently lose_sum: 88.02077436447144
time_elpased: 2.026
batch start
#iterations: 307
currently lose_sum: 87.55583745241165
time_elpased: 2.073
batch start
#iterations: 308
currently lose_sum: 87.99906575679779
time_elpased: 2.038
batch start
#iterations: 309
currently lose_sum: 87.60699754953384
time_elpased: 2.033
batch start
#iterations: 310
currently lose_sum: 87.69472700357437
time_elpased: 2.031
batch start
#iterations: 311
currently lose_sum: 86.53269129991531
time_elpased: 2.045
batch start
#iterations: 312
currently lose_sum: 87.00025916099548
time_elpased: 2.077
batch start
#iterations: 313
currently lose_sum: 86.45527851581573
time_elpased: 1.999
batch start
#iterations: 314
currently lose_sum: 86.71712636947632
time_elpased: 2.04
batch start
#iterations: 315
currently lose_sum: 86.89326196908951
time_elpased: 2.031
batch start
#iterations: 316
currently lose_sum: 86.68083399534225
time_elpased: 2.042
batch start
#iterations: 317
currently lose_sum: 86.42104494571686
time_elpased: 2.019
batch start
#iterations: 318
currently lose_sum: 86.05895614624023
time_elpased: 2.022
batch start
#iterations: 319
currently lose_sum: 85.95211005210876
time_elpased: 2.043
start validation test
0.58324742268
0.611292962357
0.461253473294
0.525778638043
0.583461601875
72.873
batch start
#iterations: 320
currently lose_sum: 85.95032250881195
time_elpased: 2.074
batch start
#iterations: 321
currently lose_sum: 86.31808453798294
time_elpased: 2.027
batch start
#iterations: 322
currently lose_sum: 86.13988822698593
time_elpased: 2.048
batch start
#iterations: 323
currently lose_sum: 85.54085695743561
time_elpased: 2.012
batch start
#iterations: 324
currently lose_sum: 85.23603004217148
time_elpased: 2.076
batch start
#iterations: 325
currently lose_sum: 85.14943104982376
time_elpased: 2.027
batch start
#iterations: 326
currently lose_sum: 86.08252882957458
time_elpased: 2.006
batch start
#iterations: 327
currently lose_sum: 85.14694613218307
time_elpased: 2.042
batch start
#iterations: 328
currently lose_sum: 85.05247557163239
time_elpased: 2.026
batch start
#iterations: 329
currently lose_sum: 85.58200025558472
time_elpased: 1.988
batch start
#iterations: 330
currently lose_sum: 84.54637032747269
time_elpased: 2.049
batch start
#iterations: 331
currently lose_sum: 84.93997836112976
time_elpased: 2.04
batch start
#iterations: 332
currently lose_sum: 84.65605628490448
time_elpased: 2.064
batch start
#iterations: 333
currently lose_sum: 84.46231436729431
time_elpased: 2.081
batch start
#iterations: 334
currently lose_sum: 84.29796278476715
time_elpased: 2.04
batch start
#iterations: 335
currently lose_sum: 84.03036344051361
time_elpased: 2.019
batch start
#iterations: 336
currently lose_sum: 84.19515234231949
time_elpased: 2.049
batch start
#iterations: 337
currently lose_sum: 83.68128335475922
time_elpased: 2.036
batch start
#iterations: 338
currently lose_sum: 83.40864539146423
time_elpased: 2.032
batch start
#iterations: 339
currently lose_sum: 84.07887029647827
time_elpased: 2.057
start validation test
0.57706185567
0.61783042394
0.407944838942
0.491415111882
0.577358766677
77.053
batch start
#iterations: 340
currently lose_sum: 83.41482517123222
time_elpased: 2.092
batch start
#iterations: 341
currently lose_sum: 84.24722480773926
time_elpased: 2.012
batch start
#iterations: 342
currently lose_sum: 83.5376307964325
time_elpased: 2.089
batch start
#iterations: 343
currently lose_sum: 83.70732915401459
time_elpased: 2.021
batch start
#iterations: 344
currently lose_sum: 83.19350016117096
time_elpased: 2.004
batch start
#iterations: 345
currently lose_sum: 82.12418949604034
time_elpased: 2.055
batch start
#iterations: 346
currently lose_sum: 82.5106805562973
time_elpased: 2.058
batch start
#iterations: 347
currently lose_sum: 82.02080518007278
time_elpased: 2.031
batch start
#iterations: 348
currently lose_sum: 82.079445540905
time_elpased: 2.068
batch start
#iterations: 349
currently lose_sum: 82.13192746043205
time_elpased: 2.021
batch start
#iterations: 350
currently lose_sum: 81.9307177066803
time_elpased: 2.031
batch start
#iterations: 351
currently lose_sum: 81.81496024131775
time_elpased: 2.029
batch start
#iterations: 352
currently lose_sum: 82.0409283041954
time_elpased: 2.08
batch start
#iterations: 353
currently lose_sum: 81.54048666357994
time_elpased: 2.046
batch start
#iterations: 354
currently lose_sum: 81.16004729270935
time_elpased: 2.018
batch start
#iterations: 355
currently lose_sum: 80.71134239435196
time_elpased: 2.045
batch start
#iterations: 356
currently lose_sum: 80.88136687874794
time_elpased: 2.001
batch start
#iterations: 357
currently lose_sum: 81.62874668836594
time_elpased: 2.011
batch start
#iterations: 358
currently lose_sum: 80.7115067243576
time_elpased: 2.048
batch start
#iterations: 359
currently lose_sum: 80.449567258358
time_elpased: 1.98
start validation test
0.564329896907
0.595442885167
0.406092415355
0.482868330886
0.564607707213
80.843
batch start
#iterations: 360
currently lose_sum: 80.36954867839813
time_elpased: 2.023
batch start
#iterations: 361
currently lose_sum: 80.3863887488842
time_elpased: 2.004
batch start
#iterations: 362
currently lose_sum: 80.6970562338829
time_elpased: 2.033
batch start
#iterations: 363
currently lose_sum: 79.17314073443413
time_elpased: 2.117
batch start
#iterations: 364
currently lose_sum: 80.11298650503159
time_elpased: 2.052
batch start
#iterations: 365
currently lose_sum: 80.2237406373024
time_elpased: 2.073
batch start
#iterations: 366
currently lose_sum: 79.97244307398796
time_elpased: 2.026
batch start
#iterations: 367
currently lose_sum: 79.15974792838097
time_elpased: 2.031
batch start
#iterations: 368
currently lose_sum: 79.33228886127472
time_elpased: 2.048
batch start
#iterations: 369
currently lose_sum: 78.9483562707901
time_elpased: 2.047
batch start
#iterations: 370
currently lose_sum: 78.29731196165085
time_elpased: 2.09
batch start
#iterations: 371
currently lose_sum: 78.69975221157074
time_elpased: 2.082
batch start
#iterations: 372
currently lose_sum: 78.73603492975235
time_elpased: 2.049
batch start
#iterations: 373
currently lose_sum: 78.97753182053566
time_elpased: 2.063
batch start
#iterations: 374
currently lose_sum: 79.1536974310875
time_elpased: 1.993
batch start
#iterations: 375
currently lose_sum: 78.40341985225677
time_elpased: 2.061
batch start
#iterations: 376
currently lose_sum: 78.05718457698822
time_elpased: 2.047
batch start
#iterations: 377
currently lose_sum: 78.42393904924393
time_elpased: 2.092
batch start
#iterations: 378
currently lose_sum: 77.84649091959
time_elpased: 2.056
batch start
#iterations: 379
currently lose_sum: 78.22818550467491
time_elpased: 2.063
start validation test
0.56
0.602535162355
0.357106102707
0.448436288447
0.560356211531
90.052
batch start
#iterations: 380
currently lose_sum: 77.93287456035614
time_elpased: 2.043
batch start
#iterations: 381
currently lose_sum: 77.63145691156387
time_elpased: 2.047
batch start
#iterations: 382
currently lose_sum: 77.41281187534332
time_elpased: 2.074
batch start
#iterations: 383
currently lose_sum: 77.61884725093842
time_elpased: 2.053
batch start
#iterations: 384
currently lose_sum: 77.25111725926399
time_elpased: 2.021
batch start
#iterations: 385
currently lose_sum: 77.60639172792435
time_elpased: 2.091
batch start
#iterations: 386
currently lose_sum: 76.94491159915924
time_elpased: 2.059
batch start
#iterations: 387
currently lose_sum: 76.34748259186745
time_elpased: 2.023
batch start
#iterations: 388
currently lose_sum: 76.51984509825706
time_elpased: 2.072
batch start
#iterations: 389
currently lose_sum: 76.2975682914257
time_elpased: 2.079
batch start
#iterations: 390
currently lose_sum: 76.3284540772438
time_elpased: 2.005
batch start
#iterations: 391
currently lose_sum: 76.09506323933601
time_elpased: 2.055
batch start
#iterations: 392
currently lose_sum: 75.46844866871834
time_elpased: 2.01
batch start
#iterations: 393
currently lose_sum: 75.38615208864212
time_elpased: 2.091
batch start
#iterations: 394
currently lose_sum: 75.81454902887344
time_elpased: 2.06
batch start
#iterations: 395
currently lose_sum: 75.67018395662308
time_elpased: 1.994
batch start
#iterations: 396
currently lose_sum: 75.70552587509155
time_elpased: 2.05
batch start
#iterations: 397
currently lose_sum: 75.75688374042511
time_elpased: 2.032
batch start
#iterations: 398
currently lose_sum: 75.44239249825478
time_elpased: 2.004
batch start
#iterations: 399
currently lose_sum: 74.74103158712387
time_elpased: 2.096
start validation test
0.554278350515
0.586373910236
0.373777914994
0.456539500974
0.55459524687
96.572
acc: 0.622
pre: 0.633
rec: 0.585
F1: 0.608
auc: 0.622
