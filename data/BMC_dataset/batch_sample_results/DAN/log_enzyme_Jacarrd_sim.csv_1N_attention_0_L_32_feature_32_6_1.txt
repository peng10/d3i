start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.41229236125946
time_elpased: 1.583
batch start
#iterations: 1
currently lose_sum: 100.2379252910614
time_elpased: 1.599
batch start
#iterations: 2
currently lose_sum: 100.05448424816132
time_elpased: 1.579
batch start
#iterations: 3
currently lose_sum: 100.08643764257431
time_elpased: 1.633
batch start
#iterations: 4
currently lose_sum: 99.99550461769104
time_elpased: 1.573
batch start
#iterations: 5
currently lose_sum: 99.88175225257874
time_elpased: 1.585
batch start
#iterations: 6
currently lose_sum: 99.87920588254929
time_elpased: 1.568
batch start
#iterations: 7
currently lose_sum: 99.57372808456421
time_elpased: 1.571
batch start
#iterations: 8
currently lose_sum: 99.6859096288681
time_elpased: 1.566
batch start
#iterations: 9
currently lose_sum: 99.59691673517227
time_elpased: 1.582
batch start
#iterations: 10
currently lose_sum: 99.56856524944305
time_elpased: 1.588
batch start
#iterations: 11
currently lose_sum: 99.42797070741653
time_elpased: 1.667
batch start
#iterations: 12
currently lose_sum: 99.43651658296585
time_elpased: 1.54
batch start
#iterations: 13
currently lose_sum: 99.18582552671432
time_elpased: 1.577
batch start
#iterations: 14
currently lose_sum: 99.25840771198273
time_elpased: 1.592
batch start
#iterations: 15
currently lose_sum: 99.00535786151886
time_elpased: 1.585
batch start
#iterations: 16
currently lose_sum: 98.83427113294601
time_elpased: 1.565
batch start
#iterations: 17
currently lose_sum: 98.80779206752777
time_elpased: 1.585
batch start
#iterations: 18
currently lose_sum: 98.72803103923798
time_elpased: 1.573
batch start
#iterations: 19
currently lose_sum: 98.88582104444504
time_elpased: 1.572
start validation test
0.597989690722
0.582544327767
0.69651126891
0.63445043356
0.597816720895
64.770
batch start
#iterations: 20
currently lose_sum: 98.66387909650803
time_elpased: 1.568
batch start
#iterations: 21
currently lose_sum: 98.37096202373505
time_elpased: 1.585
batch start
#iterations: 22
currently lose_sum: 98.8046442270279
time_elpased: 1.563
batch start
#iterations: 23
currently lose_sum: 98.56996065378189
time_elpased: 1.562
batch start
#iterations: 24
currently lose_sum: 98.61188119649887
time_elpased: 1.56
batch start
#iterations: 25
currently lose_sum: 98.56418067216873
time_elpased: 1.622
batch start
#iterations: 26
currently lose_sum: 98.4212281703949
time_elpased: 1.553
batch start
#iterations: 27
currently lose_sum: 98.41265642642975
time_elpased: 1.55
batch start
#iterations: 28
currently lose_sum: 98.29252082109451
time_elpased: 1.558
batch start
#iterations: 29
currently lose_sum: 98.33807456493378
time_elpased: 1.587
batch start
#iterations: 30
currently lose_sum: 98.3950662612915
time_elpased: 1.597
batch start
#iterations: 31
currently lose_sum: 98.01477891206741
time_elpased: 1.564
batch start
#iterations: 32
currently lose_sum: 98.07247078418732
time_elpased: 1.564
batch start
#iterations: 33
currently lose_sum: 98.24966788291931
time_elpased: 1.612
batch start
#iterations: 34
currently lose_sum: 98.10044038295746
time_elpased: 1.579
batch start
#iterations: 35
currently lose_sum: 98.01827877759933
time_elpased: 1.561
batch start
#iterations: 36
currently lose_sum: 97.66941046714783
time_elpased: 1.584
batch start
#iterations: 37
currently lose_sum: 98.32321989536285
time_elpased: 1.598
batch start
#iterations: 38
currently lose_sum: 98.05429631471634
time_elpased: 1.559
batch start
#iterations: 39
currently lose_sum: 97.81555199623108
time_elpased: 1.59
start validation test
0.614690721649
0.626011690647
0.573119275497
0.598398968463
0.614763706735
63.915
batch start
#iterations: 40
currently lose_sum: 97.85034972429276
time_elpased: 1.54
batch start
#iterations: 41
currently lose_sum: 97.9819445014
time_elpased: 1.598
batch start
#iterations: 42
currently lose_sum: 97.39211249351501
time_elpased: 1.624
batch start
#iterations: 43
currently lose_sum: 98.00628411769867
time_elpased: 1.57
batch start
#iterations: 44
currently lose_sum: 97.64198052883148
time_elpased: 1.601
batch start
#iterations: 45
currently lose_sum: 97.80027049779892
time_elpased: 1.549
batch start
#iterations: 46
currently lose_sum: 97.36296820640564
time_elpased: 1.566
batch start
#iterations: 47
currently lose_sum: 97.51646506786346
time_elpased: 1.594
batch start
#iterations: 48
currently lose_sum: 97.88391077518463
time_elpased: 1.561
batch start
#iterations: 49
currently lose_sum: 97.52959531545639
time_elpased: 1.566
batch start
#iterations: 50
currently lose_sum: 98.00098168849945
time_elpased: 1.568
batch start
#iterations: 51
currently lose_sum: 97.52343326807022
time_elpased: 1.568
batch start
#iterations: 52
currently lose_sum: 97.680528819561
time_elpased: 1.609
batch start
#iterations: 53
currently lose_sum: 97.6335923075676
time_elpased: 1.651
batch start
#iterations: 54
currently lose_sum: 97.79768204689026
time_elpased: 1.545
batch start
#iterations: 55
currently lose_sum: 97.910924077034
time_elpased: 1.582
batch start
#iterations: 56
currently lose_sum: 97.35795652866364
time_elpased: 1.589
batch start
#iterations: 57
currently lose_sum: 97.64340806007385
time_elpased: 1.576
batch start
#iterations: 58
currently lose_sum: 97.58794528245926
time_elpased: 1.572
batch start
#iterations: 59
currently lose_sum: 97.71523857116699
time_elpased: 1.674
start validation test
0.617113402062
0.626926915826
0.581763918905
0.603501654745
0.617175463532
63.550
batch start
#iterations: 60
currently lose_sum: 97.54894530773163
time_elpased: 1.582
batch start
#iterations: 61
currently lose_sum: 97.41153627634048
time_elpased: 1.603
batch start
#iterations: 62
currently lose_sum: 97.5045354962349
time_elpased: 1.574
batch start
#iterations: 63
currently lose_sum: 97.81115174293518
time_elpased: 1.63
batch start
#iterations: 64
currently lose_sum: 97.44204950332642
time_elpased: 1.649
batch start
#iterations: 65
currently lose_sum: 97.47498738765717
time_elpased: 1.691
batch start
#iterations: 66
currently lose_sum: 97.25853127241135
time_elpased: 1.549
batch start
#iterations: 67
currently lose_sum: 97.34684336185455
time_elpased: 1.54
batch start
#iterations: 68
currently lose_sum: 97.49908190965652
time_elpased: 1.584
batch start
#iterations: 69
currently lose_sum: 97.35923141241074
time_elpased: 1.608
batch start
#iterations: 70
currently lose_sum: 97.43728333711624
time_elpased: 1.573
batch start
#iterations: 71
currently lose_sum: 97.16266882419586
time_elpased: 1.558
batch start
#iterations: 72
currently lose_sum: 97.20928752422333
time_elpased: 1.564
batch start
#iterations: 73
currently lose_sum: 97.0487699508667
time_elpased: 1.55
batch start
#iterations: 74
currently lose_sum: 97.31319719552994
time_elpased: 1.554
batch start
#iterations: 75
currently lose_sum: 97.04431825876236
time_elpased: 1.574
batch start
#iterations: 76
currently lose_sum: 97.03338122367859
time_elpased: 1.571
batch start
#iterations: 77
currently lose_sum: 97.32373350858688
time_elpased: 1.576
batch start
#iterations: 78
currently lose_sum: 97.28049552440643
time_elpased: 1.577
batch start
#iterations: 79
currently lose_sum: 97.40155589580536
time_elpased: 1.587
start validation test
0.606443298969
0.59053748478
0.698775342184
0.640113127504
0.606281195826
63.594
batch start
#iterations: 80
currently lose_sum: 97.28489500284195
time_elpased: 1.622
batch start
#iterations: 81
currently lose_sum: 97.17165231704712
time_elpased: 1.558
batch start
#iterations: 82
currently lose_sum: 97.21422415971756
time_elpased: 1.574
batch start
#iterations: 83
currently lose_sum: 96.7525001168251
time_elpased: 1.563
batch start
#iterations: 84
currently lose_sum: 97.03416281938553
time_elpased: 1.566
batch start
#iterations: 85
currently lose_sum: 97.0396859049797
time_elpased: 1.577
batch start
#iterations: 86
currently lose_sum: 97.1205667257309
time_elpased: 1.567
batch start
#iterations: 87
currently lose_sum: 96.93653005361557
time_elpased: 1.589
batch start
#iterations: 88
currently lose_sum: 97.07483071088791
time_elpased: 1.548
batch start
#iterations: 89
currently lose_sum: 96.75379395484924
time_elpased: 1.597
batch start
#iterations: 90
currently lose_sum: 97.15493381023407
time_elpased: 1.569
batch start
#iterations: 91
currently lose_sum: 97.02573800086975
time_elpased: 1.544
batch start
#iterations: 92
currently lose_sum: 96.63584744930267
time_elpased: 1.596
batch start
#iterations: 93
currently lose_sum: 97.17540323734283
time_elpased: 1.588
batch start
#iterations: 94
currently lose_sum: 97.06576859951019
time_elpased: 1.582
batch start
#iterations: 95
currently lose_sum: 96.93144404888153
time_elpased: 1.571
batch start
#iterations: 96
currently lose_sum: 96.76924687623978
time_elpased: 1.568
batch start
#iterations: 97
currently lose_sum: 96.69027203321457
time_elpased: 1.575
batch start
#iterations: 98
currently lose_sum: 97.01921361684799
time_elpased: 1.585
batch start
#iterations: 99
currently lose_sum: 96.6466274857521
time_elpased: 1.567
start validation test
0.616804123711
0.624035640552
0.591026036843
0.607082452431
0.616849381119
63.358
batch start
#iterations: 100
currently lose_sum: 97.14021933078766
time_elpased: 1.602
batch start
#iterations: 101
currently lose_sum: 96.82349252700806
time_elpased: 1.569
batch start
#iterations: 102
currently lose_sum: 96.63242048025131
time_elpased: 1.581
batch start
#iterations: 103
currently lose_sum: 96.88055497407913
time_elpased: 1.562
batch start
#iterations: 104
currently lose_sum: 96.95205944776535
time_elpased: 1.6
batch start
#iterations: 105
currently lose_sum: 96.90882605314255
time_elpased: 1.596
batch start
#iterations: 106
currently lose_sum: 96.73079919815063
time_elpased: 1.588
batch start
#iterations: 107
currently lose_sum: 96.44168204069138
time_elpased: 1.581
batch start
#iterations: 108
currently lose_sum: 96.66491430997849
time_elpased: 1.564
batch start
#iterations: 109
currently lose_sum: 96.89483779668808
time_elpased: 1.552
batch start
#iterations: 110
currently lose_sum: 96.48089563846588
time_elpased: 1.577
batch start
#iterations: 111
currently lose_sum: 96.62769263982773
time_elpased: 1.569
batch start
#iterations: 112
currently lose_sum: 96.47219145298004
time_elpased: 1.601
batch start
#iterations: 113
currently lose_sum: 96.44868612289429
time_elpased: 1.563
batch start
#iterations: 114
currently lose_sum: 96.46354758739471
time_elpased: 1.572
batch start
#iterations: 115
currently lose_sum: 96.58567833900452
time_elpased: 1.578
batch start
#iterations: 116
currently lose_sum: 97.00443190336227
time_elpased: 1.617
batch start
#iterations: 117
currently lose_sum: 96.52527177333832
time_elpased: 1.565
batch start
#iterations: 118
currently lose_sum: 96.48061627149582
time_elpased: 1.563
batch start
#iterations: 119
currently lose_sum: 96.68568748235703
time_elpased: 1.553
start validation test
0.621597938144
0.632678132678
0.582998867963
0.606823415993
0.621665704766
63.360
batch start
#iterations: 120
currently lose_sum: 96.73285377025604
time_elpased: 1.598
batch start
#iterations: 121
currently lose_sum: 96.48425936698914
time_elpased: 1.568
batch start
#iterations: 122
currently lose_sum: 96.67270517349243
time_elpased: 1.569
batch start
#iterations: 123
currently lose_sum: 96.46764403581619
time_elpased: 1.604
batch start
#iterations: 124
currently lose_sum: 96.20188838243484
time_elpased: 1.574
batch start
#iterations: 125
currently lose_sum: 96.46647667884827
time_elpased: 1.536
batch start
#iterations: 126
currently lose_sum: 96.57471019029617
time_elpased: 1.579
batch start
#iterations: 127
currently lose_sum: 96.53386509418488
time_elpased: 1.569
batch start
#iterations: 128
currently lose_sum: 96.7801011800766
time_elpased: 1.58
batch start
#iterations: 129
currently lose_sum: 96.48102843761444
time_elpased: 1.556
batch start
#iterations: 130
currently lose_sum: 96.42587131261826
time_elpased: 1.58
batch start
#iterations: 131
currently lose_sum: 96.09353810548782
time_elpased: 1.618
batch start
#iterations: 132
currently lose_sum: 95.99981635808945
time_elpased: 1.604
batch start
#iterations: 133
currently lose_sum: 96.33942353725433
time_elpased: 1.602
batch start
#iterations: 134
currently lose_sum: 96.0606637597084
time_elpased: 1.588
batch start
#iterations: 135
currently lose_sum: 96.38252526521683
time_elpased: 1.58
batch start
#iterations: 136
currently lose_sum: 96.14396637678146
time_elpased: 1.563
batch start
#iterations: 137
currently lose_sum: 96.6652295589447
time_elpased: 1.558
batch start
#iterations: 138
currently lose_sum: 96.35866588354111
time_elpased: 1.622
batch start
#iterations: 139
currently lose_sum: 96.22924745082855
time_elpased: 1.61
start validation test
0.616958762887
0.630241567912
0.569208603478
0.598172281404
0.617042595656
63.281
batch start
#iterations: 140
currently lose_sum: 96.42336791753769
time_elpased: 1.54
batch start
#iterations: 141
currently lose_sum: 96.68492090702057
time_elpased: 1.625
batch start
#iterations: 142
currently lose_sum: 96.53163748979568
time_elpased: 1.578
batch start
#iterations: 143
currently lose_sum: 96.12503635883331
time_elpased: 1.586
batch start
#iterations: 144
currently lose_sum: 96.11896246671677
time_elpased: 1.591
batch start
#iterations: 145
currently lose_sum: 96.51726794242859
time_elpased: 1.546
batch start
#iterations: 146
currently lose_sum: 95.76593965291977
time_elpased: 1.614
batch start
#iterations: 147
currently lose_sum: 95.67114299535751
time_elpased: 1.575
batch start
#iterations: 148
currently lose_sum: 96.49188202619553
time_elpased: 1.551
batch start
#iterations: 149
currently lose_sum: 96.34985715150833
time_elpased: 1.586
batch start
#iterations: 150
currently lose_sum: 95.83007681369781
time_elpased: 1.594
batch start
#iterations: 151
currently lose_sum: 96.04390692710876
time_elpased: 1.578
batch start
#iterations: 152
currently lose_sum: 96.34439498186111
time_elpased: 1.572
batch start
#iterations: 153
currently lose_sum: 96.25828623771667
time_elpased: 1.568
batch start
#iterations: 154
currently lose_sum: 96.0949991941452
time_elpased: 1.554
batch start
#iterations: 155
currently lose_sum: 96.05831426382065
time_elpased: 1.576
batch start
#iterations: 156
currently lose_sum: 95.90381264686584
time_elpased: 1.539
batch start
#iterations: 157
currently lose_sum: 96.60657840967178
time_elpased: 1.564
batch start
#iterations: 158
currently lose_sum: 96.52531653642654
time_elpased: 1.572
batch start
#iterations: 159
currently lose_sum: 96.40219926834106
time_elpased: 1.542
start validation test
0.618092783505
0.632918682331
0.565503756303
0.597315071471
0.618185111654
63.279
batch start
#iterations: 160
currently lose_sum: 96.31677740812302
time_elpased: 1.574
batch start
#iterations: 161
currently lose_sum: 95.99179339408875
time_elpased: 1.565
batch start
#iterations: 162
currently lose_sum: 95.74669390916824
time_elpased: 1.606
batch start
#iterations: 163
currently lose_sum: 95.90518772602081
time_elpased: 1.631
batch start
#iterations: 164
currently lose_sum: 96.15378826856613
time_elpased: 1.56
batch start
#iterations: 165
currently lose_sum: 96.21947342157364
time_elpased: 1.615
batch start
#iterations: 166
currently lose_sum: 95.89606660604477
time_elpased: 1.571
batch start
#iterations: 167
currently lose_sum: 96.12845742702484
time_elpased: 1.647
batch start
#iterations: 168
currently lose_sum: 95.68847465515137
time_elpased: 1.58
batch start
#iterations: 169
currently lose_sum: 95.6306232213974
time_elpased: 1.564
batch start
#iterations: 170
currently lose_sum: 95.95777148008347
time_elpased: 1.58
batch start
#iterations: 171
currently lose_sum: 95.77754980325699
time_elpased: 1.546
batch start
#iterations: 172
currently lose_sum: 96.12796187400818
time_elpased: 1.587
batch start
#iterations: 173
currently lose_sum: 95.6530624628067
time_elpased: 1.611
batch start
#iterations: 174
currently lose_sum: 95.53420180082321
time_elpased: 1.57
batch start
#iterations: 175
currently lose_sum: 95.59377002716064
time_elpased: 1.571
batch start
#iterations: 176
currently lose_sum: 95.89968401193619
time_elpased: 1.574
batch start
#iterations: 177
currently lose_sum: 95.841253221035
time_elpased: 1.566
batch start
#iterations: 178
currently lose_sum: 95.33866685628891
time_elpased: 1.567
batch start
#iterations: 179
currently lose_sum: 95.99618673324585
time_elpased: 1.577
start validation test
0.609690721649
0.615508885299
0.58814448904
0.601515629934
0.609728549384
63.740
batch start
#iterations: 180
currently lose_sum: 95.60058093070984
time_elpased: 1.605
batch start
#iterations: 181
currently lose_sum: 95.90712946653366
time_elpased: 1.581
batch start
#iterations: 182
currently lose_sum: 95.78424370288849
time_elpased: 1.61
batch start
#iterations: 183
currently lose_sum: 95.4375267624855
time_elpased: 1.586
batch start
#iterations: 184
currently lose_sum: 95.49962109327316
time_elpased: 1.555
batch start
#iterations: 185
currently lose_sum: 95.66667371988297
time_elpased: 1.581
batch start
#iterations: 186
currently lose_sum: 95.70921021699905
time_elpased: 1.623
batch start
#iterations: 187
currently lose_sum: 95.2990158200264
time_elpased: 1.551
batch start
#iterations: 188
currently lose_sum: 95.82775634527206
time_elpased: 1.576
batch start
#iterations: 189
currently lose_sum: 95.31802850961685
time_elpased: 1.607
batch start
#iterations: 190
currently lose_sum: 95.82539290189743
time_elpased: 1.579
batch start
#iterations: 191
currently lose_sum: 95.5246399641037
time_elpased: 1.58
batch start
#iterations: 192
currently lose_sum: 95.25719064474106
time_elpased: 1.606
batch start
#iterations: 193
currently lose_sum: 95.80319899320602
time_elpased: 1.551
batch start
#iterations: 194
currently lose_sum: 95.3852853178978
time_elpased: 1.612
batch start
#iterations: 195
currently lose_sum: 95.45049166679382
time_elpased: 1.608
batch start
#iterations: 196
currently lose_sum: 95.70699107646942
time_elpased: 1.596
batch start
#iterations: 197
currently lose_sum: 95.39333713054657
time_elpased: 1.588
batch start
#iterations: 198
currently lose_sum: 95.53990399837494
time_elpased: 1.59
batch start
#iterations: 199
currently lose_sum: 95.41908359527588
time_elpased: 1.555
start validation test
0.610721649485
0.623277531033
0.56323968303
0.591739647529
0.6108050114
63.740
batch start
#iterations: 200
currently lose_sum: 95.19817811250687
time_elpased: 1.577
batch start
#iterations: 201
currently lose_sum: 95.37450486421585
time_elpased: 1.606
batch start
#iterations: 202
currently lose_sum: 95.03382885456085
time_elpased: 1.561
batch start
#iterations: 203
currently lose_sum: 95.29793518781662
time_elpased: 1.555
batch start
#iterations: 204
currently lose_sum: 95.16945731639862
time_elpased: 1.54
batch start
#iterations: 205
currently lose_sum: 95.29848939180374
time_elpased: 1.569
batch start
#iterations: 206
currently lose_sum: 95.65143483877182
time_elpased: 1.59
batch start
#iterations: 207
currently lose_sum: 95.60386580228806
time_elpased: 1.601
batch start
#iterations: 208
currently lose_sum: 95.12442082166672
time_elpased: 1.635
batch start
#iterations: 209
currently lose_sum: 95.10962665081024
time_elpased: 1.599
batch start
#iterations: 210
currently lose_sum: 95.11156976222992
time_elpased: 1.576
batch start
#iterations: 211
currently lose_sum: 95.31771206855774
time_elpased: 1.648
batch start
#iterations: 212
currently lose_sum: 95.0181034207344
time_elpased: 1.58
batch start
#iterations: 213
currently lose_sum: 95.22508239746094
time_elpased: 1.596
batch start
#iterations: 214
currently lose_sum: 95.13661110401154
time_elpased: 1.59
batch start
#iterations: 215
currently lose_sum: 95.33150571584702
time_elpased: 1.593
batch start
#iterations: 216
currently lose_sum: 94.96420216560364
time_elpased: 1.595
batch start
#iterations: 217
currently lose_sum: 95.15959447622299
time_elpased: 1.588
batch start
#iterations: 218
currently lose_sum: 95.40614420175552
time_elpased: 1.621
batch start
#iterations: 219
currently lose_sum: 95.22008925676346
time_elpased: 1.608
start validation test
0.607371134021
0.62297962052
0.547391170114
0.582744453574
0.607476438099
64.201
batch start
#iterations: 220
currently lose_sum: 94.96206468343735
time_elpased: 1.566
batch start
#iterations: 221
currently lose_sum: 94.60816431045532
time_elpased: 1.58
batch start
#iterations: 222
currently lose_sum: 94.92533046007156
time_elpased: 1.568
batch start
#iterations: 223
currently lose_sum: 94.82975059747696
time_elpased: 1.583
batch start
#iterations: 224
currently lose_sum: 94.93014752864838
time_elpased: 1.589
batch start
#iterations: 225
currently lose_sum: 94.84176081418991
time_elpased: 1.547
batch start
#iterations: 226
currently lose_sum: 94.99940288066864
time_elpased: 1.63
batch start
#iterations: 227
currently lose_sum: 94.90637797117233
time_elpased: 1.57
batch start
#iterations: 228
currently lose_sum: 94.61199015378952
time_elpased: 1.571
batch start
#iterations: 229
currently lose_sum: 94.33683258295059
time_elpased: 1.587
batch start
#iterations: 230
currently lose_sum: 95.230519592762
time_elpased: 1.559
batch start
#iterations: 231
currently lose_sum: 94.7462130188942
time_elpased: 1.583
batch start
#iterations: 232
currently lose_sum: 94.920878469944
time_elpased: 1.616
batch start
#iterations: 233
currently lose_sum: 94.92134857177734
time_elpased: 1.596
batch start
#iterations: 234
currently lose_sum: 94.9179710149765
time_elpased: 1.589
batch start
#iterations: 235
currently lose_sum: 94.98431944847107
time_elpased: 1.587
batch start
#iterations: 236
currently lose_sum: 94.8016175031662
time_elpased: 1.588
batch start
#iterations: 237
currently lose_sum: 94.9959312081337
time_elpased: 1.572
batch start
#iterations: 238
currently lose_sum: 95.02738094329834
time_elpased: 1.653
batch start
#iterations: 239
currently lose_sum: 94.77920937538147
time_elpased: 1.576
start validation test
0.600824742268
0.624385323414
0.509622311413
0.561196736174
0.600984862202
64.278
batch start
#iterations: 240
currently lose_sum: 94.56578439474106
time_elpased: 1.613
batch start
#iterations: 241
currently lose_sum: 94.69951128959656
time_elpased: 1.585
batch start
#iterations: 242
currently lose_sum: 94.66554474830627
time_elpased: 1.578
batch start
#iterations: 243
currently lose_sum: 94.64299178123474
time_elpased: 1.567
batch start
#iterations: 244
currently lose_sum: 94.74214833974838
time_elpased: 1.621
batch start
#iterations: 245
currently lose_sum: 94.92948508262634
time_elpased: 1.66
batch start
#iterations: 246
currently lose_sum: 94.31854790449142
time_elpased: 1.547
batch start
#iterations: 247
currently lose_sum: 94.44602924585342
time_elpased: 1.544
batch start
#iterations: 248
currently lose_sum: 94.74263721704483
time_elpased: 1.567
batch start
#iterations: 249
currently lose_sum: 94.85290205478668
time_elpased: 1.598
batch start
#iterations: 250
currently lose_sum: 94.53033751249313
time_elpased: 1.583
batch start
#iterations: 251
currently lose_sum: 94.53914874792099
time_elpased: 1.605
batch start
#iterations: 252
currently lose_sum: 94.47920840978622
time_elpased: 1.596
batch start
#iterations: 253
currently lose_sum: 94.6361398100853
time_elpased: 1.58
batch start
#iterations: 254
currently lose_sum: 94.4799969792366
time_elpased: 1.578
batch start
#iterations: 255
currently lose_sum: 94.44836360216141
time_elpased: 1.576
batch start
#iterations: 256
currently lose_sum: 94.4083845615387
time_elpased: 1.569
batch start
#iterations: 257
currently lose_sum: 93.90213710069656
time_elpased: 1.593
batch start
#iterations: 258
currently lose_sum: 94.3914960026741
time_elpased: 1.555
batch start
#iterations: 259
currently lose_sum: 94.25097793340683
time_elpased: 1.581
start validation test
0.608350515464
0.61989362906
0.563754245137
0.590492616147
0.608428811095
64.267
batch start
#iterations: 260
currently lose_sum: 94.0453497171402
time_elpased: 1.574
batch start
#iterations: 261
currently lose_sum: 94.06469893455505
time_elpased: 1.581
batch start
#iterations: 262
currently lose_sum: 93.85917156934738
time_elpased: 1.584
batch start
#iterations: 263
currently lose_sum: 94.39723592996597
time_elpased: 1.568
batch start
#iterations: 264
currently lose_sum: 93.86961805820465
time_elpased: 1.553
batch start
#iterations: 265
currently lose_sum: 94.43249076604843
time_elpased: 1.624
batch start
#iterations: 266
currently lose_sum: 94.12211740016937
time_elpased: 1.581
batch start
#iterations: 267
currently lose_sum: 94.00625556707382
time_elpased: 1.607
batch start
#iterations: 268
currently lose_sum: 93.79120749235153
time_elpased: 1.579
batch start
#iterations: 269
currently lose_sum: 94.24804902076721
time_elpased: 1.58
batch start
#iterations: 270
currently lose_sum: 93.61554783582687
time_elpased: 1.583
batch start
#iterations: 271
currently lose_sum: 93.87305438518524
time_elpased: 1.568
batch start
#iterations: 272
currently lose_sum: 94.22043669223785
time_elpased: 1.59
batch start
#iterations: 273
currently lose_sum: 93.95284521579742
time_elpased: 1.608
batch start
#iterations: 274
currently lose_sum: 94.05457764863968
time_elpased: 1.624
batch start
#iterations: 275
currently lose_sum: 94.39186543226242
time_elpased: 1.633
batch start
#iterations: 276
currently lose_sum: 93.49476438760757
time_elpased: 1.6
batch start
#iterations: 277
currently lose_sum: 93.99088662862778
time_elpased: 1.632
batch start
#iterations: 278
currently lose_sum: 93.7083488702774
time_elpased: 1.556
batch start
#iterations: 279
currently lose_sum: 94.05206954479218
time_elpased: 1.59
start validation test
0.613195876289
0.636554362582
0.530822270248
0.578900112233
0.613340495859
63.968
batch start
#iterations: 280
currently lose_sum: 93.37700313329697
time_elpased: 1.614
batch start
#iterations: 281
currently lose_sum: 93.85930120944977
time_elpased: 1.602
batch start
#iterations: 282
currently lose_sum: 93.93485897779465
time_elpased: 1.555
batch start
#iterations: 283
currently lose_sum: 93.83881217241287
time_elpased: 1.628
batch start
#iterations: 284
currently lose_sum: 93.77390396595001
time_elpased: 1.585
batch start
#iterations: 285
currently lose_sum: 93.52601784467697
time_elpased: 1.578
batch start
#iterations: 286
currently lose_sum: 93.89613556861877
time_elpased: 1.574
batch start
#iterations: 287
currently lose_sum: 93.73331063985825
time_elpased: 1.597
batch start
#iterations: 288
currently lose_sum: 93.5187885761261
time_elpased: 1.617
batch start
#iterations: 289
currently lose_sum: 93.73952102661133
time_elpased: 1.572
batch start
#iterations: 290
currently lose_sum: 93.92397993803024
time_elpased: 1.576
batch start
#iterations: 291
currently lose_sum: 93.37849545478821
time_elpased: 1.61
batch start
#iterations: 292
currently lose_sum: 93.32839620113373
time_elpased: 1.577
batch start
#iterations: 293
currently lose_sum: 93.65670597553253
time_elpased: 1.587
batch start
#iterations: 294
currently lose_sum: 93.44322419166565
time_elpased: 1.579
batch start
#iterations: 295
currently lose_sum: 93.3295801281929
time_elpased: 1.611
batch start
#iterations: 296
currently lose_sum: 93.64957231283188
time_elpased: 1.551
batch start
#iterations: 297
currently lose_sum: 93.61885756254196
time_elpased: 1.657
batch start
#iterations: 298
currently lose_sum: 93.67148244380951
time_elpased: 1.588
batch start
#iterations: 299
currently lose_sum: 93.57442826032639
time_elpased: 1.637
start validation test
0.603711340206
0.615323405706
0.557064937738
0.584746678189
0.60379323516
64.669
batch start
#iterations: 300
currently lose_sum: 93.59465998411179
time_elpased: 1.572
batch start
#iterations: 301
currently lose_sum: 93.22351151704788
time_elpased: 1.59
batch start
#iterations: 302
currently lose_sum: 92.87679350376129
time_elpased: 1.624
batch start
#iterations: 303
currently lose_sum: 93.72386646270752
time_elpased: 1.634
batch start
#iterations: 304
currently lose_sum: 93.31290656328201
time_elpased: 1.573
batch start
#iterations: 305
currently lose_sum: 93.60052472352982
time_elpased: 1.583
batch start
#iterations: 306
currently lose_sum: 93.38212484121323
time_elpased: 1.574
batch start
#iterations: 307
currently lose_sum: 93.31058251857758
time_elpased: 1.623
batch start
#iterations: 308
currently lose_sum: 93.25345408916473
time_elpased: 1.577
batch start
#iterations: 309
currently lose_sum: 93.62823241949081
time_elpased: 1.61
batch start
#iterations: 310
currently lose_sum: 93.36018186807632
time_elpased: 1.591
batch start
#iterations: 311
currently lose_sum: 93.21456789970398
time_elpased: 1.597
batch start
#iterations: 312
currently lose_sum: 92.85766100883484
time_elpased: 1.582
batch start
#iterations: 313
currently lose_sum: 92.75282913446426
time_elpased: 1.626
batch start
#iterations: 314
currently lose_sum: 92.90104562044144
time_elpased: 1.557
batch start
#iterations: 315
currently lose_sum: 93.05966079235077
time_elpased: 1.617
batch start
#iterations: 316
currently lose_sum: 93.16684567928314
time_elpased: 1.614
batch start
#iterations: 317
currently lose_sum: 93.51406049728394
time_elpased: 1.621
batch start
#iterations: 318
currently lose_sum: 93.21613198518753
time_elpased: 1.577
batch start
#iterations: 319
currently lose_sum: 92.6239025592804
time_elpased: 1.58
start validation test
0.602474226804
0.622750091833
0.523412575898
0.568776560054
0.602613031727
65.389
batch start
#iterations: 320
currently lose_sum: 93.38067734241486
time_elpased: 1.594
batch start
#iterations: 321
currently lose_sum: 92.99624198675156
time_elpased: 1.655
batch start
#iterations: 322
currently lose_sum: 93.04109811782837
time_elpased: 1.586
batch start
#iterations: 323
currently lose_sum: 92.77816212177277
time_elpased: 1.586
batch start
#iterations: 324
currently lose_sum: 92.82032054662704
time_elpased: 1.571
batch start
#iterations: 325
currently lose_sum: 92.71230417490005
time_elpased: 1.61
batch start
#iterations: 326
currently lose_sum: 93.02177345752716
time_elpased: 1.574
batch start
#iterations: 327
currently lose_sum: 92.80134063959122
time_elpased: 1.583
batch start
#iterations: 328
currently lose_sum: 92.9787078499794
time_elpased: 1.603
batch start
#iterations: 329
currently lose_sum: 92.6486177444458
time_elpased: 1.591
batch start
#iterations: 330
currently lose_sum: 92.4944788813591
time_elpased: 1.591
batch start
#iterations: 331
currently lose_sum: 92.13127076625824
time_elpased: 1.581
batch start
#iterations: 332
currently lose_sum: 92.48528456687927
time_elpased: 1.636
batch start
#iterations: 333
currently lose_sum: 92.3067838549614
time_elpased: 1.587
batch start
#iterations: 334
currently lose_sum: 92.07124078273773
time_elpased: 1.616
batch start
#iterations: 335
currently lose_sum: 92.68899136781693
time_elpased: 1.593
batch start
#iterations: 336
currently lose_sum: 92.12582808732986
time_elpased: 1.646
batch start
#iterations: 337
currently lose_sum: 92.6908991932869
time_elpased: 1.575
batch start
#iterations: 338
currently lose_sum: 92.45792067050934
time_elpased: 1.595
batch start
#iterations: 339
currently lose_sum: 92.43812018632889
time_elpased: 1.602
start validation test
0.608195876289
0.63172310757
0.52217762684
0.571750521156
0.608346894593
64.442
batch start
#iterations: 340
currently lose_sum: 92.68633985519409
time_elpased: 1.569
batch start
#iterations: 341
currently lose_sum: 92.83700209856033
time_elpased: 1.654
batch start
#iterations: 342
currently lose_sum: 92.33578073978424
time_elpased: 1.58
batch start
#iterations: 343
currently lose_sum: 92.74687027931213
time_elpased: 1.643
batch start
#iterations: 344
currently lose_sum: 92.03353571891785
time_elpased: 1.559
batch start
#iterations: 345
currently lose_sum: 92.00074011087418
time_elpased: 1.611
batch start
#iterations: 346
currently lose_sum: 92.15970593690872
time_elpased: 1.604
batch start
#iterations: 347
currently lose_sum: 92.43721812963486
time_elpased: 1.611
batch start
#iterations: 348
currently lose_sum: 91.86218059062958
time_elpased: 1.591
batch start
#iterations: 349
currently lose_sum: 92.13491368293762
time_elpased: 1.589
batch start
#iterations: 350
currently lose_sum: 92.26879489421844
time_elpased: 1.582
batch start
#iterations: 351
currently lose_sum: 92.74750089645386
time_elpased: 1.586
batch start
#iterations: 352
currently lose_sum: 92.54622972011566
time_elpased: 1.571
batch start
#iterations: 353
currently lose_sum: 92.60613703727722
time_elpased: 1.622
batch start
#iterations: 354
currently lose_sum: 92.4149956703186
time_elpased: 1.593
batch start
#iterations: 355
currently lose_sum: 92.50615561008453
time_elpased: 1.667
batch start
#iterations: 356
currently lose_sum: 92.28786909580231
time_elpased: 1.647
batch start
#iterations: 357
currently lose_sum: 91.78809064626694
time_elpased: 1.625
batch start
#iterations: 358
currently lose_sum: 92.49392187595367
time_elpased: 1.581
batch start
#iterations: 359
currently lose_sum: 92.39670485258102
time_elpased: 1.616
start validation test
0.602989690722
0.62015503876
0.535144591952
0.574522152248
0.603108803257
65.429
batch start
#iterations: 360
currently lose_sum: 91.69789099693298
time_elpased: 1.612
batch start
#iterations: 361
currently lose_sum: 92.30960321426392
time_elpased: 1.653
batch start
#iterations: 362
currently lose_sum: 92.10205346345901
time_elpased: 1.592
batch start
#iterations: 363
currently lose_sum: 91.6045686006546
time_elpased: 1.571
batch start
#iterations: 364
currently lose_sum: 92.32280826568604
time_elpased: 1.578
batch start
#iterations: 365
currently lose_sum: 91.93663257360458
time_elpased: 1.605
batch start
#iterations: 366
currently lose_sum: 91.69953745603561
time_elpased: 1.604
batch start
#iterations: 367
currently lose_sum: 91.88262921571732
time_elpased: 1.66
batch start
#iterations: 368
currently lose_sum: 91.58908480405807
time_elpased: 1.566
batch start
#iterations: 369
currently lose_sum: 91.58113634586334
time_elpased: 1.632
batch start
#iterations: 370
currently lose_sum: 91.8090769648552
time_elpased: 1.581
batch start
#iterations: 371
currently lose_sum: 91.36212599277496
time_elpased: 1.637
batch start
#iterations: 372
currently lose_sum: 92.11450064182281
time_elpased: 1.62
batch start
#iterations: 373
currently lose_sum: 91.3319902420044
time_elpased: 1.586
batch start
#iterations: 374
currently lose_sum: 91.82351261377335
time_elpased: 1.595
batch start
#iterations: 375
currently lose_sum: 91.90800386667252
time_elpased: 1.541
batch start
#iterations: 376
currently lose_sum: 91.2498727440834
time_elpased: 1.612
batch start
#iterations: 377
currently lose_sum: 92.25873774290085
time_elpased: 1.564
batch start
#iterations: 378
currently lose_sum: 91.40604078769684
time_elpased: 1.605
batch start
#iterations: 379
currently lose_sum: 91.20619571208954
time_elpased: 1.592
start validation test
0.598865979381
0.622051217358
0.507461150561
0.558943550215
0.599026454657
66.098
batch start
#iterations: 380
currently lose_sum: 91.81860888004303
time_elpased: 1.62
batch start
#iterations: 381
currently lose_sum: 91.51269114017487
time_elpased: 1.581
batch start
#iterations: 382
currently lose_sum: 91.40967404842377
time_elpased: 1.576
batch start
#iterations: 383
currently lose_sum: 91.70633435249329
time_elpased: 1.581
batch start
#iterations: 384
currently lose_sum: 91.1662950515747
time_elpased: 1.591
batch start
#iterations: 385
currently lose_sum: 91.15662854909897
time_elpased: 1.587
batch start
#iterations: 386
currently lose_sum: 91.37935024499893
time_elpased: 1.582
batch start
#iterations: 387
currently lose_sum: 90.92782056331635
time_elpased: 1.582
batch start
#iterations: 388
currently lose_sum: 91.10031831264496
time_elpased: 1.561
batch start
#iterations: 389
currently lose_sum: 91.46714162826538
time_elpased: 1.603
batch start
#iterations: 390
currently lose_sum: 91.07525837421417
time_elpased: 1.587
batch start
#iterations: 391
currently lose_sum: 91.04747080802917
time_elpased: 1.578
batch start
#iterations: 392
currently lose_sum: 91.02035051584244
time_elpased: 1.577
batch start
#iterations: 393
currently lose_sum: 91.07543915510178
time_elpased: 1.596
batch start
#iterations: 394
currently lose_sum: 91.27213042974472
time_elpased: 1.599
batch start
#iterations: 395
currently lose_sum: 91.1356788277626
time_elpased: 1.623
batch start
#iterations: 396
currently lose_sum: 90.75782865285873
time_elpased: 1.617
batch start
#iterations: 397
currently lose_sum: 90.94126641750336
time_elpased: 1.622
batch start
#iterations: 398
currently lose_sum: 90.74743449687958
time_elpased: 1.645
batch start
#iterations: 399
currently lose_sum: 90.71467471122742
time_elpased: 1.626
start validation test
0.599072164948
0.624919469141
0.499125244417
0.554983407713
0.599247637183
66.442
acc: 0.622
pre: 0.637
rec: 0.569
F1: 0.601
auc: 0.622
