start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.44445413351059
time_elpased: 1.634
batch start
#iterations: 1
currently lose_sum: 100.26562702655792
time_elpased: 1.623
batch start
#iterations: 2
currently lose_sum: 100.15119075775146
time_elpased: 1.615
batch start
#iterations: 3
currently lose_sum: 100.0875433087349
time_elpased: 1.632
batch start
#iterations: 4
currently lose_sum: 100.02054464817047
time_elpased: 1.617
batch start
#iterations: 5
currently lose_sum: 99.89781719446182
time_elpased: 1.605
batch start
#iterations: 6
currently lose_sum: 99.77530217170715
time_elpased: 1.634
batch start
#iterations: 7
currently lose_sum: 99.7084025144577
time_elpased: 1.593
batch start
#iterations: 8
currently lose_sum: 99.64890152215958
time_elpased: 1.621
batch start
#iterations: 9
currently lose_sum: 99.75158232450485
time_elpased: 1.618
batch start
#iterations: 10
currently lose_sum: 99.44695097208023
time_elpased: 1.633
batch start
#iterations: 11
currently lose_sum: 99.32862555980682
time_elpased: 1.624
batch start
#iterations: 12
currently lose_sum: 99.14334660768509
time_elpased: 1.61
batch start
#iterations: 13
currently lose_sum: 99.41594749689102
time_elpased: 1.587
batch start
#iterations: 14
currently lose_sum: 99.27548170089722
time_elpased: 1.595
batch start
#iterations: 15
currently lose_sum: 99.20419013500214
time_elpased: 1.617
batch start
#iterations: 16
currently lose_sum: 99.24638795852661
time_elpased: 1.594
batch start
#iterations: 17
currently lose_sum: 99.39069592952728
time_elpased: 1.627
batch start
#iterations: 18
currently lose_sum: 99.15272212028503
time_elpased: 1.643
batch start
#iterations: 19
currently lose_sum: 98.98999512195587
time_elpased: 1.603
start validation test
0.608402061856
0.622669908533
0.553519967065
0.58606222416
0.608492738592
64.766
batch start
#iterations: 20
currently lose_sum: 98.8359003663063
time_elpased: 1.589
batch start
#iterations: 21
currently lose_sum: 98.71453875303268
time_elpased: 1.616
batch start
#iterations: 22
currently lose_sum: 98.57257235050201
time_elpased: 1.572
batch start
#iterations: 23
currently lose_sum: 98.84935635328293
time_elpased: 1.61
batch start
#iterations: 24
currently lose_sum: 98.65515440702438
time_elpased: 1.592
batch start
#iterations: 25
currently lose_sum: 98.62353932857513
time_elpased: 1.596
batch start
#iterations: 26
currently lose_sum: 98.53185969591141
time_elpased: 1.615
batch start
#iterations: 27
currently lose_sum: 98.38105708360672
time_elpased: 1.581
batch start
#iterations: 28
currently lose_sum: 98.66506785154343
time_elpased: 1.584
batch start
#iterations: 29
currently lose_sum: 98.3727245926857
time_elpased: 1.61
batch start
#iterations: 30
currently lose_sum: 98.55411970615387
time_elpased: 1.598
batch start
#iterations: 31
currently lose_sum: 98.33949029445648
time_elpased: 1.609
batch start
#iterations: 32
currently lose_sum: 98.06747704744339
time_elpased: 1.58
batch start
#iterations: 33
currently lose_sum: 98.25723898410797
time_elpased: 1.667
batch start
#iterations: 34
currently lose_sum: 98.34994834661484
time_elpased: 1.614
batch start
#iterations: 35
currently lose_sum: 98.66680759191513
time_elpased: 1.607
batch start
#iterations: 36
currently lose_sum: 98.34471714496613
time_elpased: 1.619
batch start
#iterations: 37
currently lose_sum: 98.32069283723831
time_elpased: 1.63
batch start
#iterations: 38
currently lose_sum: 98.36613202095032
time_elpased: 1.598
batch start
#iterations: 39
currently lose_sum: 98.25079810619354
time_elpased: 1.595
start validation test
0.614329896907
0.631349952963
0.552593659942
0.589352360044
0.614431898125
64.156
batch start
#iterations: 40
currently lose_sum: 98.05247312784195
time_elpased: 1.591
batch start
#iterations: 41
currently lose_sum: 98.17117327451706
time_elpased: 1.572
batch start
#iterations: 42
currently lose_sum: 98.04031354188919
time_elpased: 1.597
batch start
#iterations: 43
currently lose_sum: 98.26498878002167
time_elpased: 1.628
batch start
#iterations: 44
currently lose_sum: 98.10595536231995
time_elpased: 1.65
batch start
#iterations: 45
currently lose_sum: 98.16292625665665
time_elpased: 1.645
batch start
#iterations: 46
currently lose_sum: 98.00882756710052
time_elpased: 1.625
batch start
#iterations: 47
currently lose_sum: 97.82938396930695
time_elpased: 1.591
batch start
#iterations: 48
currently lose_sum: 98.13416349887848
time_elpased: 1.603
batch start
#iterations: 49
currently lose_sum: 97.69029760360718
time_elpased: 1.606
batch start
#iterations: 50
currently lose_sum: 97.94178813695908
time_elpased: 1.592
batch start
#iterations: 51
currently lose_sum: 98.19549661874771
time_elpased: 1.586
batch start
#iterations: 52
currently lose_sum: 97.5690723657608
time_elpased: 1.62
batch start
#iterations: 53
currently lose_sum: 97.95419418811798
time_elpased: 1.601
batch start
#iterations: 54
currently lose_sum: 97.89027500152588
time_elpased: 1.594
batch start
#iterations: 55
currently lose_sum: 97.75997281074524
time_elpased: 1.576
batch start
#iterations: 56
currently lose_sum: 97.7748162150383
time_elpased: 1.629
batch start
#iterations: 57
currently lose_sum: 97.62411946058273
time_elpased: 1.598
batch start
#iterations: 58
currently lose_sum: 97.52303051948547
time_elpased: 1.628
batch start
#iterations: 59
currently lose_sum: 97.8629914522171
time_elpased: 1.597
start validation test
0.619381443299
0.631188118812
0.577398106217
0.603096108364
0.619450808581
63.888
batch start
#iterations: 60
currently lose_sum: 97.61704844236374
time_elpased: 1.598
batch start
#iterations: 61
currently lose_sum: 97.94278770685196
time_elpased: 1.588
batch start
#iterations: 62
currently lose_sum: 97.90448349714279
time_elpased: 1.583
batch start
#iterations: 63
currently lose_sum: 97.70022654533386
time_elpased: 1.602
batch start
#iterations: 64
currently lose_sum: 97.79809963703156
time_elpased: 1.627
batch start
#iterations: 65
currently lose_sum: 97.64328694343567
time_elpased: 1.588
batch start
#iterations: 66
currently lose_sum: 97.63185036182404
time_elpased: 1.605
batch start
#iterations: 67
currently lose_sum: 97.6237610578537
time_elpased: 1.592
batch start
#iterations: 68
currently lose_sum: 97.57120472192764
time_elpased: 1.577
batch start
#iterations: 69
currently lose_sum: 97.7229232788086
time_elpased: 1.599
batch start
#iterations: 70
currently lose_sum: 97.80010890960693
time_elpased: 1.637
batch start
#iterations: 71
currently lose_sum: 97.44213354587555
time_elpased: 1.631
batch start
#iterations: 72
currently lose_sum: 97.68203145265579
time_elpased: 1.577
batch start
#iterations: 73
currently lose_sum: 97.4205202460289
time_elpased: 1.63
batch start
#iterations: 74
currently lose_sum: 97.35316389799118
time_elpased: 1.639
batch start
#iterations: 75
currently lose_sum: 97.5508798956871
time_elpased: 1.622
batch start
#iterations: 76
currently lose_sum: 97.4459969997406
time_elpased: 1.618
batch start
#iterations: 77
currently lose_sum: 97.48928052186966
time_elpased: 1.624
batch start
#iterations: 78
currently lose_sum: 97.69237262010574
time_elpased: 1.608
batch start
#iterations: 79
currently lose_sum: 97.41889089345932
time_elpased: 1.606
start validation test
0.627680412371
0.642148477592
0.579559489502
0.609250743846
0.627759918233
63.247
batch start
#iterations: 80
currently lose_sum: 97.4683308005333
time_elpased: 1.623
batch start
#iterations: 81
currently lose_sum: 97.48400110006332
time_elpased: 1.585
batch start
#iterations: 82
currently lose_sum: 97.47527229785919
time_elpased: 1.632
batch start
#iterations: 83
currently lose_sum: 97.42775273323059
time_elpased: 1.579
batch start
#iterations: 84
currently lose_sum: 97.40375399589539
time_elpased: 1.58
batch start
#iterations: 85
currently lose_sum: 97.26087099313736
time_elpased: 1.609
batch start
#iterations: 86
currently lose_sum: 97.50749105215073
time_elpased: 1.63
batch start
#iterations: 87
currently lose_sum: 96.98695468902588
time_elpased: 1.617
batch start
#iterations: 88
currently lose_sum: 97.24909222126007
time_elpased: 1.631
batch start
#iterations: 89
currently lose_sum: 97.37928980588913
time_elpased: 1.612
batch start
#iterations: 90
currently lose_sum: 97.4371919631958
time_elpased: 1.63
batch start
#iterations: 91
currently lose_sum: 97.2735846042633
time_elpased: 1.576
batch start
#iterations: 92
currently lose_sum: 97.3502916097641
time_elpased: 1.623
batch start
#iterations: 93
currently lose_sum: 97.29549258947372
time_elpased: 1.59
batch start
#iterations: 94
currently lose_sum: 97.13554787635803
time_elpased: 1.608
batch start
#iterations: 95
currently lose_sum: 97.32942897081375
time_elpased: 1.575
batch start
#iterations: 96
currently lose_sum: 97.30569332838058
time_elpased: 1.648
batch start
#iterations: 97
currently lose_sum: 97.16993534564972
time_elpased: 1.601
batch start
#iterations: 98
currently lose_sum: 97.09921002388
time_elpased: 1.632
batch start
#iterations: 99
currently lose_sum: 97.10705816745758
time_elpased: 1.642
start validation test
0.611134020619
0.621612541993
0.571325648415
0.595409203046
0.611199792402
63.721
batch start
#iterations: 100
currently lose_sum: 97.10770028829575
time_elpased: 1.616
batch start
#iterations: 101
currently lose_sum: 97.24561727046967
time_elpased: 1.611
batch start
#iterations: 102
currently lose_sum: 97.20963376760483
time_elpased: 1.61
batch start
#iterations: 103
currently lose_sum: 97.34640008211136
time_elpased: 1.59
batch start
#iterations: 104
currently lose_sum: 96.96532541513443
time_elpased: 1.618
batch start
#iterations: 105
currently lose_sum: 97.13702201843262
time_elpased: 1.669
batch start
#iterations: 106
currently lose_sum: 97.12443894147873
time_elpased: 1.584
batch start
#iterations: 107
currently lose_sum: 97.28343534469604
time_elpased: 1.579
batch start
#iterations: 108
currently lose_sum: 96.96324020624161
time_elpased: 1.599
batch start
#iterations: 109
currently lose_sum: 96.89670652151108
time_elpased: 1.63
batch start
#iterations: 110
currently lose_sum: 97.22196477651596
time_elpased: 1.608
batch start
#iterations: 111
currently lose_sum: 96.99710988998413
time_elpased: 1.606
batch start
#iterations: 112
currently lose_sum: 97.11504280567169
time_elpased: 1.603
batch start
#iterations: 113
currently lose_sum: 96.86393094062805
time_elpased: 1.564
batch start
#iterations: 114
currently lose_sum: 97.0366205573082
time_elpased: 1.599
batch start
#iterations: 115
currently lose_sum: 96.64228653907776
time_elpased: 1.616
batch start
#iterations: 116
currently lose_sum: 97.4070816040039
time_elpased: 1.614
batch start
#iterations: 117
currently lose_sum: 96.58208590745926
time_elpased: 1.603
batch start
#iterations: 118
currently lose_sum: 97.20631790161133
time_elpased: 1.658
batch start
#iterations: 119
currently lose_sum: 96.8754934668541
time_elpased: 1.615
start validation test
0.615412371134
0.619730275035
0.600658707287
0.610045471175
0.615436747282
63.866
batch start
#iterations: 120
currently lose_sum: 96.63604444265366
time_elpased: 1.617
batch start
#iterations: 121
currently lose_sum: 96.8813625574112
time_elpased: 1.637
batch start
#iterations: 122
currently lose_sum: 96.88458687067032
time_elpased: 1.614
batch start
#iterations: 123
currently lose_sum: 96.8834468126297
time_elpased: 1.601
batch start
#iterations: 124
currently lose_sum: 97.03551006317139
time_elpased: 1.675
batch start
#iterations: 125
currently lose_sum: 97.0660839676857
time_elpased: 1.618
batch start
#iterations: 126
currently lose_sum: 96.53027498722076
time_elpased: 1.622
batch start
#iterations: 127
currently lose_sum: 96.93128603696823
time_elpased: 1.602
batch start
#iterations: 128
currently lose_sum: 96.49079740047455
time_elpased: 1.66
batch start
#iterations: 129
currently lose_sum: 96.84702217578888
time_elpased: 1.597
batch start
#iterations: 130
currently lose_sum: 96.98691177368164
time_elpased: 1.598
batch start
#iterations: 131
currently lose_sum: 96.47106039524078
time_elpased: 1.596
batch start
#iterations: 132
currently lose_sum: 96.77123034000397
time_elpased: 1.621
batch start
#iterations: 133
currently lose_sum: 96.96073043346405
time_elpased: 1.612
batch start
#iterations: 134
currently lose_sum: 96.52436184883118
time_elpased: 1.61
batch start
#iterations: 135
currently lose_sum: 97.1164880990982
time_elpased: 1.607
batch start
#iterations: 136
currently lose_sum: 96.81587719917297
time_elpased: 1.626
batch start
#iterations: 137
currently lose_sum: 96.87700641155243
time_elpased: 1.621
batch start
#iterations: 138
currently lose_sum: 96.6033490896225
time_elpased: 1.637
batch start
#iterations: 139
currently lose_sum: 96.55965477228165
time_elpased: 1.6
start validation test
0.61087628866
0.614377705056
0.59901193907
0.606597529835
0.610895891055
63.741
batch start
#iterations: 140
currently lose_sum: 96.3636988401413
time_elpased: 1.604
batch start
#iterations: 141
currently lose_sum: 96.56584948301315
time_elpased: 1.597
batch start
#iterations: 142
currently lose_sum: 96.80867964029312
time_elpased: 1.684
batch start
#iterations: 143
currently lose_sum: 96.57456564903259
time_elpased: 1.612
batch start
#iterations: 144
currently lose_sum: 96.42771059274673
time_elpased: 1.635
batch start
#iterations: 145
currently lose_sum: 96.22953003644943
time_elpased: 1.608
batch start
#iterations: 146
currently lose_sum: 96.54645192623138
time_elpased: 1.644
batch start
#iterations: 147
currently lose_sum: 96.71042668819427
time_elpased: 1.635
batch start
#iterations: 148
currently lose_sum: 96.61736363172531
time_elpased: 1.683
batch start
#iterations: 149
currently lose_sum: 96.50824403762817
time_elpased: 1.644
batch start
#iterations: 150
currently lose_sum: 96.5774821639061
time_elpased: 1.608
batch start
#iterations: 151
currently lose_sum: 96.8822769522667
time_elpased: 1.581
batch start
#iterations: 152
currently lose_sum: 96.76748871803284
time_elpased: 1.623
batch start
#iterations: 153
currently lose_sum: 96.59126788377762
time_elpased: 1.583
batch start
#iterations: 154
currently lose_sum: 96.74639475345612
time_elpased: 1.604
batch start
#iterations: 155
currently lose_sum: 96.34563440084457
time_elpased: 1.592
batch start
#iterations: 156
currently lose_sum: 96.50142395496368
time_elpased: 1.615
batch start
#iterations: 157
currently lose_sum: 96.25753796100616
time_elpased: 1.638
batch start
#iterations: 158
currently lose_sum: 96.64840543270111
time_elpased: 1.621
batch start
#iterations: 159
currently lose_sum: 96.58734196424484
time_elpased: 1.609
start validation test
0.605463917526
0.615453527436
0.565664882668
0.589509814437
0.605529673882
63.900
batch start
#iterations: 160
currently lose_sum: 96.3951467871666
time_elpased: 1.63
batch start
#iterations: 161
currently lose_sum: 96.49477744102478
time_elpased: 1.614
batch start
#iterations: 162
currently lose_sum: 96.34278100728989
time_elpased: 1.631
batch start
#iterations: 163
currently lose_sum: 96.58512544631958
time_elpased: 1.622
batch start
#iterations: 164
currently lose_sum: 96.34510165452957
time_elpased: 1.59
batch start
#iterations: 165
currently lose_sum: 96.42372769117355
time_elpased: 1.601
batch start
#iterations: 166
currently lose_sum: 96.21666240692139
time_elpased: 1.646
batch start
#iterations: 167
currently lose_sum: 96.07316410541534
time_elpased: 1.598
batch start
#iterations: 168
currently lose_sum: 96.27898502349854
time_elpased: 1.614
batch start
#iterations: 169
currently lose_sum: 96.17987841367722
time_elpased: 1.613
batch start
#iterations: 170
currently lose_sum: 96.32199585437775
time_elpased: 1.624
batch start
#iterations: 171
currently lose_sum: 96.15322256088257
time_elpased: 1.61
batch start
#iterations: 172
currently lose_sum: 96.08053243160248
time_elpased: 1.657
batch start
#iterations: 173
currently lose_sum: 96.18588864803314
time_elpased: 1.616
batch start
#iterations: 174
currently lose_sum: 96.03210365772247
time_elpased: 1.637
batch start
#iterations: 175
currently lose_sum: 95.9523286819458
time_elpased: 1.62
batch start
#iterations: 176
currently lose_sum: 96.3266813158989
time_elpased: 1.596
batch start
#iterations: 177
currently lose_sum: 96.55103659629822
time_elpased: 1.618
batch start
#iterations: 178
currently lose_sum: 96.06204843521118
time_elpased: 1.618
batch start
#iterations: 179
currently lose_sum: 95.83571022748947
time_elpased: 1.617
start validation test
0.621649484536
0.632117437722
0.585014409222
0.607654479367
0.621710013368
63.176
batch start
#iterations: 180
currently lose_sum: 95.93380743265152
time_elpased: 1.626
batch start
#iterations: 181
currently lose_sum: 96.58645290136337
time_elpased: 1.611
batch start
#iterations: 182
currently lose_sum: 96.15991127490997
time_elpased: 1.587
batch start
#iterations: 183
currently lose_sum: 96.01604777574539
time_elpased: 1.585
batch start
#iterations: 184
currently lose_sum: 95.94482791423798
time_elpased: 1.618
batch start
#iterations: 185
currently lose_sum: 96.07638221979141
time_elpased: 1.574
batch start
#iterations: 186
currently lose_sum: 95.84969013929367
time_elpased: 1.576
batch start
#iterations: 187
currently lose_sum: 95.92940586805344
time_elpased: 1.605
batch start
#iterations: 188
currently lose_sum: 95.86405658721924
time_elpased: 1.637
batch start
#iterations: 189
currently lose_sum: 96.19554805755615
time_elpased: 1.599
batch start
#iterations: 190
currently lose_sum: 96.26639050245285
time_elpased: 1.605
batch start
#iterations: 191
currently lose_sum: 95.72355628013611
time_elpased: 1.619
batch start
#iterations: 192
currently lose_sum: 96.06187909841537
time_elpased: 1.59
batch start
#iterations: 193
currently lose_sum: 95.82736992835999
time_elpased: 1.571
batch start
#iterations: 194
currently lose_sum: 96.08283710479736
time_elpased: 1.608
batch start
#iterations: 195
currently lose_sum: 95.83851283788681
time_elpased: 1.571
batch start
#iterations: 196
currently lose_sum: 95.71472388505936
time_elpased: 1.625
batch start
#iterations: 197
currently lose_sum: 95.6608659029007
time_elpased: 1.62
batch start
#iterations: 198
currently lose_sum: 95.70273053646088
time_elpased: 1.632
batch start
#iterations: 199
currently lose_sum: 95.50807416439056
time_elpased: 1.623
start validation test
0.606804123711
0.62199111942
0.547859201317
0.582576337967
0.606901513092
64.187
batch start
#iterations: 200
currently lose_sum: 95.81828886270523
time_elpased: 1.612
batch start
#iterations: 201
currently lose_sum: 95.60033148527145
time_elpased: 1.597
batch start
#iterations: 202
currently lose_sum: 95.89401024580002
time_elpased: 1.61
batch start
#iterations: 203
currently lose_sum: 95.95496064424515
time_elpased: 1.57
batch start
#iterations: 204
currently lose_sum: 95.6244889497757
time_elpased: 1.601
batch start
#iterations: 205
currently lose_sum: 95.58209365606308
time_elpased: 1.621
batch start
#iterations: 206
currently lose_sum: 95.83569401502609
time_elpased: 1.615
batch start
#iterations: 207
currently lose_sum: 95.49786162376404
time_elpased: 1.617
batch start
#iterations: 208
currently lose_sum: 95.65469944477081
time_elpased: 1.59
batch start
#iterations: 209
currently lose_sum: 95.56949841976166
time_elpased: 1.592
batch start
#iterations: 210
currently lose_sum: 95.65778654813766
time_elpased: 1.616
batch start
#iterations: 211
currently lose_sum: 95.17680388689041
time_elpased: 1.644
batch start
#iterations: 212
currently lose_sum: 95.84440493583679
time_elpased: 1.6
batch start
#iterations: 213
currently lose_sum: 95.87870925664902
time_elpased: 1.587
batch start
#iterations: 214
currently lose_sum: 95.52057909965515
time_elpased: 1.617
batch start
#iterations: 215
currently lose_sum: 95.4402568936348
time_elpased: 1.602
batch start
#iterations: 216
currently lose_sum: 95.076081097126
time_elpased: 1.606
batch start
#iterations: 217
currently lose_sum: 95.65679615736008
time_elpased: 1.617
batch start
#iterations: 218
currently lose_sum: 95.55483931303024
time_elpased: 1.619
batch start
#iterations: 219
currently lose_sum: 95.31870770454407
time_elpased: 1.608
start validation test
0.615412371134
0.641627936189
0.525730753396
0.577926118685
0.615560543984
63.607
batch start
#iterations: 220
currently lose_sum: 95.24014949798584
time_elpased: 1.612
batch start
#iterations: 221
currently lose_sum: 95.40392285585403
time_elpased: 1.589
batch start
#iterations: 222
currently lose_sum: 95.2039042711258
time_elpased: 1.633
batch start
#iterations: 223
currently lose_sum: 94.97710502147675
time_elpased: 1.643
batch start
#iterations: 224
currently lose_sum: 95.01460653543472
time_elpased: 1.602
batch start
#iterations: 225
currently lose_sum: 95.16004830598831
time_elpased: 1.608
batch start
#iterations: 226
currently lose_sum: 95.05802893638611
time_elpased: 1.625
batch start
#iterations: 227
currently lose_sum: 94.9518181681633
time_elpased: 1.598
batch start
#iterations: 228
currently lose_sum: 95.0132554769516
time_elpased: 1.596
batch start
#iterations: 229
currently lose_sum: 95.23869609832764
time_elpased: 1.624
batch start
#iterations: 230
currently lose_sum: 95.41681563854218
time_elpased: 1.594
batch start
#iterations: 231
currently lose_sum: 95.14923226833344
time_elpased: 1.598
batch start
#iterations: 232
currently lose_sum: 95.28403371572495
time_elpased: 1.585
batch start
#iterations: 233
currently lose_sum: 95.11988115310669
time_elpased: 1.629
batch start
#iterations: 234
currently lose_sum: 94.9263334274292
time_elpased: 1.59
batch start
#iterations: 235
currently lose_sum: 95.25278490781784
time_elpased: 1.591
batch start
#iterations: 236
currently lose_sum: 95.33351492881775
time_elpased: 1.599
batch start
#iterations: 237
currently lose_sum: 94.7797464132309
time_elpased: 1.625
batch start
#iterations: 238
currently lose_sum: 95.32222956418991
time_elpased: 1.683
batch start
#iterations: 239
currently lose_sum: 95.2832762002945
time_elpased: 1.64
start validation test
0.607680412371
0.620658030494
0.557225195554
0.587233580997
0.607763774976
64.100
batch start
#iterations: 240
currently lose_sum: 94.819773375988
time_elpased: 1.602
batch start
#iterations: 241
currently lose_sum: 94.65323626995087
time_elpased: 1.598
batch start
#iterations: 242
currently lose_sum: 94.93541431427002
time_elpased: 1.595
batch start
#iterations: 243
currently lose_sum: 95.5115156173706
time_elpased: 1.617
batch start
#iterations: 244
currently lose_sum: 94.99851834774017
time_elpased: 1.604
batch start
#iterations: 245
currently lose_sum: 94.96986794471741
time_elpased: 1.593
batch start
#iterations: 246
currently lose_sum: 95.17053246498108
time_elpased: 1.619
batch start
#iterations: 247
currently lose_sum: 94.55177783966064
time_elpased: 1.592
batch start
#iterations: 248
currently lose_sum: 94.93813824653625
time_elpased: 1.641
batch start
#iterations: 249
currently lose_sum: 95.04445773363113
time_elpased: 1.589
batch start
#iterations: 250
currently lose_sum: 94.86129993200302
time_elpased: 1.592
batch start
#iterations: 251
currently lose_sum: 94.81783473491669
time_elpased: 1.597
batch start
#iterations: 252
currently lose_sum: 94.94637590646744
time_elpased: 1.665
batch start
#iterations: 253
currently lose_sum: 94.9608843922615
time_elpased: 1.588
batch start
#iterations: 254
currently lose_sum: 94.95402097702026
time_elpased: 1.574
batch start
#iterations: 255
currently lose_sum: 94.61283642053604
time_elpased: 1.581
batch start
#iterations: 256
currently lose_sum: 94.40601539611816
time_elpased: 1.59
batch start
#iterations: 257
currently lose_sum: 94.71947890520096
time_elpased: 1.604
batch start
#iterations: 258
currently lose_sum: 94.82816433906555
time_elpased: 1.624
batch start
#iterations: 259
currently lose_sum: 94.54448187351227
time_elpased: 1.593
start validation test
0.606391752577
0.629708156648
0.519658295595
0.569414683659
0.606535054448
64.623
batch start
#iterations: 260
currently lose_sum: 94.94354569911957
time_elpased: 1.608
batch start
#iterations: 261
currently lose_sum: 94.76544761657715
time_elpased: 1.631
batch start
#iterations: 262
currently lose_sum: 94.69857531785965
time_elpased: 1.613
batch start
#iterations: 263
currently lose_sum: 94.61574876308441
time_elpased: 1.599
batch start
#iterations: 264
currently lose_sum: 94.69416046142578
time_elpased: 1.609
batch start
#iterations: 265
currently lose_sum: 94.47792237997055
time_elpased: 1.613
batch start
#iterations: 266
currently lose_sum: 94.43860536813736
time_elpased: 1.581
batch start
#iterations: 267
currently lose_sum: 94.28537356853485
time_elpased: 1.653
batch start
#iterations: 268
currently lose_sum: 94.56804245710373
time_elpased: 1.625
batch start
#iterations: 269
currently lose_sum: 94.49728405475616
time_elpased: 1.609
batch start
#iterations: 270
currently lose_sum: 94.35323083400726
time_elpased: 1.593
batch start
#iterations: 271
currently lose_sum: 94.54555332660675
time_elpased: 1.62
batch start
#iterations: 272
currently lose_sum: 94.80249613523483
time_elpased: 1.636
batch start
#iterations: 273
currently lose_sum: 94.68751537799835
time_elpased: 1.598
batch start
#iterations: 274
currently lose_sum: 94.35588812828064
time_elpased: 1.63
batch start
#iterations: 275
currently lose_sum: 94.2694885134697
time_elpased: 1.62
batch start
#iterations: 276
currently lose_sum: 94.20444643497467
time_elpased: 1.633
batch start
#iterations: 277
currently lose_sum: 94.27986657619476
time_elpased: 1.612
batch start
#iterations: 278
currently lose_sum: 94.28800755739212
time_elpased: 1.606
batch start
#iterations: 279
currently lose_sum: 94.32703286409378
time_elpased: 1.609
start validation test
0.605463917526
0.643833705357
0.474989707699
0.54667140488
0.605679488298
64.546
batch start
#iterations: 280
currently lose_sum: 94.44025534391403
time_elpased: 1.627
batch start
#iterations: 281
currently lose_sum: 94.22793537378311
time_elpased: 1.587
batch start
#iterations: 282
currently lose_sum: 93.6707221865654
time_elpased: 1.603
batch start
#iterations: 283
currently lose_sum: 94.52792245149612
time_elpased: 1.585
batch start
#iterations: 284
currently lose_sum: 94.27649056911469
time_elpased: 1.606
batch start
#iterations: 285
currently lose_sum: 93.59944677352905
time_elpased: 1.621
batch start
#iterations: 286
currently lose_sum: 94.19644272327423
time_elpased: 1.628
batch start
#iterations: 287
currently lose_sum: 93.97318524122238
time_elpased: 1.604
batch start
#iterations: 288
currently lose_sum: 94.18493735790253
time_elpased: 1.588
batch start
#iterations: 289
currently lose_sum: 94.00412905216217
time_elpased: 1.611
batch start
#iterations: 290
currently lose_sum: 93.93355315923691
time_elpased: 1.572
batch start
#iterations: 291
currently lose_sum: 94.00361239910126
time_elpased: 1.58
batch start
#iterations: 292
currently lose_sum: 94.15660721063614
time_elpased: 1.628
batch start
#iterations: 293
currently lose_sum: 93.85369139909744
time_elpased: 1.593
batch start
#iterations: 294
currently lose_sum: 93.97591060400009
time_elpased: 1.6
batch start
#iterations: 295
currently lose_sum: 93.97459816932678
time_elpased: 1.64
batch start
#iterations: 296
currently lose_sum: 93.97732573747635
time_elpased: 1.636
batch start
#iterations: 297
currently lose_sum: 93.84926164150238
time_elpased: 1.632
batch start
#iterations: 298
currently lose_sum: 93.67573606967926
time_elpased: 1.627
batch start
#iterations: 299
currently lose_sum: 93.82197678089142
time_elpased: 1.624
start validation test
0.609175257732
0.622671878593
0.557431041581
0.588248072119
0.609260750035
64.393
batch start
#iterations: 300
currently lose_sum: 94.29771798849106
time_elpased: 1.626
batch start
#iterations: 301
currently lose_sum: 93.9123882651329
time_elpased: 1.607
batch start
#iterations: 302
currently lose_sum: 93.61785042285919
time_elpased: 1.618
batch start
#iterations: 303
currently lose_sum: 93.76496481895447
time_elpased: 1.592
batch start
#iterations: 304
currently lose_sum: 93.70265412330627
time_elpased: 1.577
batch start
#iterations: 305
currently lose_sum: 93.75039559602737
time_elpased: 1.609
batch start
#iterations: 306
currently lose_sum: 94.05682480335236
time_elpased: 1.639
batch start
#iterations: 307
currently lose_sum: 93.90198040008545
time_elpased: 1.645
batch start
#iterations: 308
currently lose_sum: 93.63824510574341
time_elpased: 1.592
batch start
#iterations: 309
currently lose_sum: 93.27408647537231
time_elpased: 1.574
batch start
#iterations: 310
currently lose_sum: 93.47584062814713
time_elpased: 1.707
batch start
#iterations: 311
currently lose_sum: 93.34723275899887
time_elpased: 1.617
batch start
#iterations: 312
currently lose_sum: 93.67024046182632
time_elpased: 1.593
batch start
#iterations: 313
currently lose_sum: 93.63136392831802
time_elpased: 1.608
batch start
#iterations: 314
currently lose_sum: 93.77481836080551
time_elpased: 1.598
batch start
#iterations: 315
currently lose_sum: 93.40920305252075
time_elpased: 1.602
batch start
#iterations: 316
currently lose_sum: 93.364761531353
time_elpased: 1.577
batch start
#iterations: 317
currently lose_sum: 93.42581474781036
time_elpased: 1.594
batch start
#iterations: 318
currently lose_sum: 93.36493813991547
time_elpased: 1.584
batch start
#iterations: 319
currently lose_sum: 93.39287203550339
time_elpased: 1.595
start validation test
0.607422680412
0.624792013311
0.541066282421
0.579922779923
0.607532315105
65.138
batch start
#iterations: 320
currently lose_sum: 93.82559579610825
time_elpased: 1.581
batch start
#iterations: 321
currently lose_sum: 93.65037870407104
time_elpased: 1.617
batch start
#iterations: 322
currently lose_sum: 93.36769598722458
time_elpased: 1.687
batch start
#iterations: 323
currently lose_sum: 93.11792695522308
time_elpased: 1.592
batch start
#iterations: 324
currently lose_sum: 93.44408351182938
time_elpased: 1.637
batch start
#iterations: 325
currently lose_sum: 93.49696934223175
time_elpased: 1.622
batch start
#iterations: 326
currently lose_sum: 93.19844353199005
time_elpased: 1.592
batch start
#iterations: 327
currently lose_sum: 93.23778289556503
time_elpased: 1.584
batch start
#iterations: 328
currently lose_sum: 92.99233692884445
time_elpased: 1.604
batch start
#iterations: 329
currently lose_sum: 93.08075261116028
time_elpased: 1.616
batch start
#iterations: 330
currently lose_sum: 93.02784413099289
time_elpased: 1.599
batch start
#iterations: 331
currently lose_sum: 93.21732872724533
time_elpased: 1.612
batch start
#iterations: 332
currently lose_sum: 93.18880695104599
time_elpased: 1.629
batch start
#iterations: 333
currently lose_sum: 93.2787976861
time_elpased: 1.606
batch start
#iterations: 334
currently lose_sum: 93.35109782218933
time_elpased: 1.62
batch start
#iterations: 335
currently lose_sum: 93.22798591852188
time_elpased: 1.616
batch start
#iterations: 336
currently lose_sum: 92.56978273391724
time_elpased: 1.631
batch start
#iterations: 337
currently lose_sum: 93.10751104354858
time_elpased: 1.602
batch start
#iterations: 338
currently lose_sum: 93.25301897525787
time_elpased: 1.603
batch start
#iterations: 339
currently lose_sum: 92.9098806977272
time_elpased: 1.611
start validation test
0.606649484536
0.626871120847
0.530156442981
0.574471644454
0.606775867091
65.418
batch start
#iterations: 340
currently lose_sum: 92.89438027143478
time_elpased: 1.676
batch start
#iterations: 341
currently lose_sum: 92.72891610860825
time_elpased: 1.647
batch start
#iterations: 342
currently lose_sum: 93.20626085996628
time_elpased: 1.635
batch start
#iterations: 343
currently lose_sum: 92.95388507843018
time_elpased: 1.638
batch start
#iterations: 344
currently lose_sum: 92.98805457353592
time_elpased: 1.588
batch start
#iterations: 345
currently lose_sum: 92.97378063201904
time_elpased: 1.606
batch start
#iterations: 346
currently lose_sum: 92.66549557447433
time_elpased: 1.615
batch start
#iterations: 347
currently lose_sum: 92.60222059488297
time_elpased: 1.653
batch start
#iterations: 348
currently lose_sum: 92.91650992631912
time_elpased: 1.611
batch start
#iterations: 349
currently lose_sum: 92.55709874629974
time_elpased: 1.602
batch start
#iterations: 350
currently lose_sum: 92.75007653236389
time_elpased: 1.651
batch start
#iterations: 351
currently lose_sum: 93.01648360490799
time_elpased: 1.624
batch start
#iterations: 352
currently lose_sum: 92.67785596847534
time_elpased: 1.577
batch start
#iterations: 353
currently lose_sum: 93.19634115695953
time_elpased: 1.619
batch start
#iterations: 354
currently lose_sum: 92.37656092643738
time_elpased: 1.69
batch start
#iterations: 355
currently lose_sum: 92.73463505506516
time_elpased: 1.687
batch start
#iterations: 356
currently lose_sum: 92.28218191862106
time_elpased: 1.602
batch start
#iterations: 357
currently lose_sum: 92.43069982528687
time_elpased: 1.639
batch start
#iterations: 358
currently lose_sum: 92.78768855333328
time_elpased: 1.605
batch start
#iterations: 359
currently lose_sum: 92.28069067001343
time_elpased: 1.629
start validation test
0.598195876289
0.631305536569
0.475298476739
0.54230520815
0.59839892858
65.673
batch start
#iterations: 360
currently lose_sum: 92.56236958503723
time_elpased: 1.593
batch start
#iterations: 361
currently lose_sum: 92.49179595708847
time_elpased: 1.603
batch start
#iterations: 362
currently lose_sum: 92.63646203279495
time_elpased: 1.626
batch start
#iterations: 363
currently lose_sum: 92.39344453811646
time_elpased: 1.592
batch start
#iterations: 364
currently lose_sum: 92.21854656934738
time_elpased: 1.599
batch start
#iterations: 365
currently lose_sum: 92.71478909254074
time_elpased: 1.602
batch start
#iterations: 366
currently lose_sum: 92.27782714366913
time_elpased: 1.624
batch start
#iterations: 367
currently lose_sum: 92.29059565067291
time_elpased: 1.628
batch start
#iterations: 368
currently lose_sum: 92.64698523283005
time_elpased: 1.604
batch start
#iterations: 369
currently lose_sum: 92.12991511821747
time_elpased: 1.688
batch start
#iterations: 370
currently lose_sum: 92.41088908910751
time_elpased: 1.608
batch start
#iterations: 371
currently lose_sum: 91.89332693815231
time_elpased: 1.642
batch start
#iterations: 372
currently lose_sum: 92.37861502170563
time_elpased: 1.615
batch start
#iterations: 373
currently lose_sum: 92.01611024141312
time_elpased: 1.604
batch start
#iterations: 374
currently lose_sum: 92.42774277925491
time_elpased: 1.611
batch start
#iterations: 375
currently lose_sum: 91.55997955799103
time_elpased: 1.645
batch start
#iterations: 376
currently lose_sum: 91.58147704601288
time_elpased: 1.598
batch start
#iterations: 377
currently lose_sum: 92.12277966737747
time_elpased: 1.578
batch start
#iterations: 378
currently lose_sum: 92.63303250074387
time_elpased: 1.612
batch start
#iterations: 379
currently lose_sum: 92.1360769867897
time_elpased: 1.63
start validation test
0.595463917526
0.627282638321
0.473754631536
0.539814706227
0.595665006805
67.049
batch start
#iterations: 380
currently lose_sum: 91.4077033996582
time_elpased: 1.622
batch start
#iterations: 381
currently lose_sum: 91.78206449747086
time_elpased: 1.638
batch start
#iterations: 382
currently lose_sum: 92.31523019075394
time_elpased: 1.593
batch start
#iterations: 383
currently lose_sum: 92.09601885080338
time_elpased: 1.629
batch start
#iterations: 384
currently lose_sum: 91.7173593044281
time_elpased: 1.603
batch start
#iterations: 385
currently lose_sum: 92.14955878257751
time_elpased: 1.59
batch start
#iterations: 386
currently lose_sum: 91.44122362136841
time_elpased: 1.603
batch start
#iterations: 387
currently lose_sum: 91.52597379684448
time_elpased: 1.638
batch start
#iterations: 388
currently lose_sum: 91.88300693035126
time_elpased: 1.609
batch start
#iterations: 389
currently lose_sum: 91.7669757604599
time_elpased: 1.604
batch start
#iterations: 390
currently lose_sum: 91.81704378128052
time_elpased: 1.63
batch start
#iterations: 391
currently lose_sum: 91.86018580198288
time_elpased: 1.583
batch start
#iterations: 392
currently lose_sum: 91.29959881305695
time_elpased: 1.586
batch start
#iterations: 393
currently lose_sum: 91.30131077766418
time_elpased: 1.6
batch start
#iterations: 394
currently lose_sum: 91.4909839630127
time_elpased: 1.598
batch start
#iterations: 395
currently lose_sum: 91.6813793182373
time_elpased: 1.62
batch start
#iterations: 396
currently lose_sum: 91.6135488152504
time_elpased: 1.632
batch start
#iterations: 397
currently lose_sum: 91.35183358192444
time_elpased: 1.63
batch start
#iterations: 398
currently lose_sum: 91.23922377824783
time_elpased: 1.586
batch start
#iterations: 399
currently lose_sum: 91.70621156692505
time_elpased: 1.608
start validation test
0.590773195876
0.620670922178
0.470358172087
0.535160138181
0.590972146762
66.913
acc: 0.621
pre: 0.631
rec: 0.585
F1: 0.607
auc: 0.621
