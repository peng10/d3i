start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.37753468751907
time_elpased: 1.936
batch start
#iterations: 1
currently lose_sum: 100.14912623167038
time_elpased: 1.907
batch start
#iterations: 2
currently lose_sum: 100.23292338848114
time_elpased: 1.904
batch start
#iterations: 3
currently lose_sum: 100.1188452243805
time_elpased: 1.876
batch start
#iterations: 4
currently lose_sum: 100.06362414360046
time_elpased: 1.859
batch start
#iterations: 5
currently lose_sum: 99.95349657535553
time_elpased: 1.871
batch start
#iterations: 6
currently lose_sum: 99.7553738951683
time_elpased: 1.909
batch start
#iterations: 7
currently lose_sum: 99.85142916440964
time_elpased: 1.928
batch start
#iterations: 8
currently lose_sum: 99.70399433374405
time_elpased: 1.85
batch start
#iterations: 9
currently lose_sum: 99.69914090633392
time_elpased: 1.836
batch start
#iterations: 10
currently lose_sum: 99.55238783359528
time_elpased: 1.925
batch start
#iterations: 11
currently lose_sum: 99.44862312078476
time_elpased: 1.912
batch start
#iterations: 12
currently lose_sum: 99.45108211040497
time_elpased: 1.923
batch start
#iterations: 13
currently lose_sum: 99.24061912298203
time_elpased: 1.91
batch start
#iterations: 14
currently lose_sum: 99.22397065162659
time_elpased: 1.898
batch start
#iterations: 15
currently lose_sum: 98.96655583381653
time_elpased: 1.981
batch start
#iterations: 16
currently lose_sum: 99.0427440404892
time_elpased: 1.963
batch start
#iterations: 17
currently lose_sum: 99.12756538391113
time_elpased: 1.93
batch start
#iterations: 18
currently lose_sum: 98.73267382383347
time_elpased: 1.913
batch start
#iterations: 19
currently lose_sum: 98.99951481819153
time_elpased: 1.925
start validation test
0.593608247423
0.583008785436
0.662447257384
0.62019462376
0.593487389923
65.221
batch start
#iterations: 20
currently lose_sum: 98.7506313920021
time_elpased: 1.887
batch start
#iterations: 21
currently lose_sum: 98.69610047340393
time_elpased: 1.893
batch start
#iterations: 22
currently lose_sum: 98.78662556409836
time_elpased: 1.862
batch start
#iterations: 23
currently lose_sum: 98.83089101314545
time_elpased: 1.901
batch start
#iterations: 24
currently lose_sum: 98.64427810907364
time_elpased: 1.901
batch start
#iterations: 25
currently lose_sum: 98.64821380376816
time_elpased: 1.897
batch start
#iterations: 26
currently lose_sum: 98.67316037416458
time_elpased: 1.864
batch start
#iterations: 27
currently lose_sum: 98.50962525606155
time_elpased: 1.895
batch start
#iterations: 28
currently lose_sum: 98.46646583080292
time_elpased: 1.864
batch start
#iterations: 29
currently lose_sum: 98.59007567167282
time_elpased: 1.869
batch start
#iterations: 30
currently lose_sum: 98.36135858297348
time_elpased: 1.907
batch start
#iterations: 31
currently lose_sum: 98.18996822834015
time_elpased: 1.902
batch start
#iterations: 32
currently lose_sum: 98.32677417993546
time_elpased: 1.92
batch start
#iterations: 33
currently lose_sum: 98.32393705844879
time_elpased: 1.876
batch start
#iterations: 34
currently lose_sum: 98.09613311290741
time_elpased: 1.854
batch start
#iterations: 35
currently lose_sum: 98.20555329322815
time_elpased: 1.9
batch start
#iterations: 36
currently lose_sum: 98.14594721794128
time_elpased: 1.841
batch start
#iterations: 37
currently lose_sum: 98.22170376777649
time_elpased: 1.92
batch start
#iterations: 38
currently lose_sum: 98.07461386919022
time_elpased: 1.837
batch start
#iterations: 39
currently lose_sum: 98.11444759368896
time_elpased: 1.843
start validation test
0.621030927835
0.619795360146
0.629618194916
0.624668164182
0.621015851563
63.841
batch start
#iterations: 40
currently lose_sum: 98.09708005189896
time_elpased: 1.837
batch start
#iterations: 41
currently lose_sum: 97.82406783103943
time_elpased: 1.948
batch start
#iterations: 42
currently lose_sum: 97.95276564359665
time_elpased: 1.882
batch start
#iterations: 43
currently lose_sum: 98.13885462284088
time_elpased: 1.884
batch start
#iterations: 44
currently lose_sum: 97.92473196983337
time_elpased: 1.852
batch start
#iterations: 45
currently lose_sum: 97.6224000453949
time_elpased: 1.919
batch start
#iterations: 46
currently lose_sum: 97.61990749835968
time_elpased: 1.848
batch start
#iterations: 47
currently lose_sum: 97.8656359910965
time_elpased: 1.894
batch start
#iterations: 48
currently lose_sum: 98.09665268659592
time_elpased: 1.874
batch start
#iterations: 49
currently lose_sum: 97.75817614793777
time_elpased: 1.894
batch start
#iterations: 50
currently lose_sum: 97.98729878664017
time_elpased: 1.884
batch start
#iterations: 51
currently lose_sum: 97.76824724674225
time_elpased: 1.886
batch start
#iterations: 52
currently lose_sum: 98.08393585681915
time_elpased: 1.912
batch start
#iterations: 53
currently lose_sum: 97.9821480512619
time_elpased: 1.907
batch start
#iterations: 54
currently lose_sum: 97.8359227180481
time_elpased: 1.902
batch start
#iterations: 55
currently lose_sum: 97.72863578796387
time_elpased: 1.871
batch start
#iterations: 56
currently lose_sum: 97.81095123291016
time_elpased: 1.874
batch start
#iterations: 57
currently lose_sum: 97.80149894952774
time_elpased: 1.938
batch start
#iterations: 58
currently lose_sum: 97.79448121786118
time_elpased: 1.902
batch start
#iterations: 59
currently lose_sum: 97.79753452539444
time_elpased: 1.911
start validation test
0.618556701031
0.639932358981
0.545230009262
0.588797510558
0.618685437348
63.666
batch start
#iterations: 60
currently lose_sum: 97.63804113864899
time_elpased: 1.903
batch start
#iterations: 61
currently lose_sum: 97.95482349395752
time_elpased: 1.86
batch start
#iterations: 62
currently lose_sum: 97.44392424821854
time_elpased: 1.857
batch start
#iterations: 63
currently lose_sum: 97.95121389627457
time_elpased: 1.902
batch start
#iterations: 64
currently lose_sum: 97.61498618125916
time_elpased: 1.878
batch start
#iterations: 65
currently lose_sum: 97.58637690544128
time_elpased: 1.867
batch start
#iterations: 66
currently lose_sum: 97.53747928142548
time_elpased: 1.863
batch start
#iterations: 67
currently lose_sum: 97.53095585107803
time_elpased: 1.924
batch start
#iterations: 68
currently lose_sum: 97.6409564614296
time_elpased: 1.863
batch start
#iterations: 69
currently lose_sum: 97.84033185243607
time_elpased: 1.935
batch start
#iterations: 70
currently lose_sum: 97.40167832374573
time_elpased: 1.862
batch start
#iterations: 71
currently lose_sum: 97.47417867183685
time_elpased: 1.871
batch start
#iterations: 72
currently lose_sum: 97.46628403663635
time_elpased: 1.917
batch start
#iterations: 73
currently lose_sum: 97.37185961008072
time_elpased: 1.896
batch start
#iterations: 74
currently lose_sum: 97.3352814912796
time_elpased: 1.887
batch start
#iterations: 75
currently lose_sum: 97.14568030834198
time_elpased: 1.894
batch start
#iterations: 76
currently lose_sum: 97.32526642084122
time_elpased: 1.863
batch start
#iterations: 77
currently lose_sum: 97.74709713459015
time_elpased: 1.862
batch start
#iterations: 78
currently lose_sum: 97.51201057434082
time_elpased: 1.909
batch start
#iterations: 79
currently lose_sum: 97.58902895450592
time_elpased: 1.909
start validation test
0.615051546392
0.63334519151
0.549655243388
0.588539944904
0.615166359688
63.631
batch start
#iterations: 80
currently lose_sum: 97.29067385196686
time_elpased: 1.87
batch start
#iterations: 81
currently lose_sum: 97.40040373802185
time_elpased: 1.827
batch start
#iterations: 82
currently lose_sum: 97.34375965595245
time_elpased: 1.908
batch start
#iterations: 83
currently lose_sum: 97.07322907447815
time_elpased: 1.871
batch start
#iterations: 84
currently lose_sum: 97.13729858398438
time_elpased: 1.864
batch start
#iterations: 85
currently lose_sum: 97.31347864866257
time_elpased: 1.944
batch start
#iterations: 86
currently lose_sum: 97.25481712818146
time_elpased: 1.903
batch start
#iterations: 87
currently lose_sum: 97.08909046649933
time_elpased: 1.834
batch start
#iterations: 88
currently lose_sum: 97.3719470500946
time_elpased: 1.863
batch start
#iterations: 89
currently lose_sum: 96.99211120605469
time_elpased: 1.882
batch start
#iterations: 90
currently lose_sum: 97.28401792049408
time_elpased: 1.876
batch start
#iterations: 91
currently lose_sum: 96.98625206947327
time_elpased: 1.857
batch start
#iterations: 92
currently lose_sum: 97.0146479010582
time_elpased: 1.817
batch start
#iterations: 93
currently lose_sum: 97.30719774961472
time_elpased: 1.865
batch start
#iterations: 94
currently lose_sum: 97.1153569817543
time_elpased: 1.899
batch start
#iterations: 95
currently lose_sum: 96.88420289754868
time_elpased: 1.859
batch start
#iterations: 96
currently lose_sum: 97.06840002536774
time_elpased: 1.905
batch start
#iterations: 97
currently lose_sum: 97.02550637722015
time_elpased: 1.849
batch start
#iterations: 98
currently lose_sum: 97.00757801532745
time_elpased: 1.887
batch start
#iterations: 99
currently lose_sum: 96.87024468183517
time_elpased: 1.904
start validation test
0.608917525773
0.623722118959
0.552536791191
0.585975443383
0.609016510849
63.850
batch start
#iterations: 100
currently lose_sum: 97.15036886930466
time_elpased: 1.817
batch start
#iterations: 101
currently lose_sum: 97.25481486320496
time_elpased: 1.934
batch start
#iterations: 102
currently lose_sum: 96.49366861581802
time_elpased: 1.891
batch start
#iterations: 103
currently lose_sum: 97.15827226638794
time_elpased: 1.854
batch start
#iterations: 104
currently lose_sum: 96.88169193267822
time_elpased: 1.86
batch start
#iterations: 105
currently lose_sum: 97.16423898935318
time_elpased: 1.881
batch start
#iterations: 106
currently lose_sum: 96.6385902762413
time_elpased: 1.911
batch start
#iterations: 107
currently lose_sum: 96.75314462184906
time_elpased: 1.892
batch start
#iterations: 108
currently lose_sum: 96.99119424819946
time_elpased: 1.848
batch start
#iterations: 109
currently lose_sum: 96.6319489479065
time_elpased: 1.941
batch start
#iterations: 110
currently lose_sum: 96.51749330759048
time_elpased: 1.872
batch start
#iterations: 111
currently lose_sum: 96.8350585103035
time_elpased: 1.916
batch start
#iterations: 112
currently lose_sum: 96.64452940225601
time_elpased: 1.915
batch start
#iterations: 113
currently lose_sum: 96.52188926935196
time_elpased: 1.868
batch start
#iterations: 114
currently lose_sum: 96.65039139986038
time_elpased: 1.853
batch start
#iterations: 115
currently lose_sum: 96.89529466629028
time_elpased: 1.862
batch start
#iterations: 116
currently lose_sum: 96.9985944032669
time_elpased: 1.913
batch start
#iterations: 117
currently lose_sum: 96.7505658864975
time_elpased: 1.88
batch start
#iterations: 118
currently lose_sum: 96.44435966014862
time_elpased: 1.84
batch start
#iterations: 119
currently lose_sum: 96.80577492713928
time_elpased: 1.929
start validation test
0.614793814433
0.635671100363
0.541010599979
0.584533274031
0.614923352246
63.655
batch start
#iterations: 120
currently lose_sum: 96.72083282470703
time_elpased: 1.885
batch start
#iterations: 121
currently lose_sum: 96.47438675165176
time_elpased: 1.87
batch start
#iterations: 122
currently lose_sum: 96.73936057090759
time_elpased: 1.937
batch start
#iterations: 123
currently lose_sum: 96.52220642566681
time_elpased: 1.918
batch start
#iterations: 124
currently lose_sum: 96.30218994617462
time_elpased: 1.92
batch start
#iterations: 125
currently lose_sum: 96.69093316793442
time_elpased: 1.899
batch start
#iterations: 126
currently lose_sum: 96.73561328649521
time_elpased: 1.926
batch start
#iterations: 127
currently lose_sum: 96.75633651018143
time_elpased: 1.931
batch start
#iterations: 128
currently lose_sum: 96.43317639827728
time_elpased: 1.864
batch start
#iterations: 129
currently lose_sum: 96.79366332292557
time_elpased: 1.887
batch start
#iterations: 130
currently lose_sum: 96.46753942966461
time_elpased: 1.903
batch start
#iterations: 131
currently lose_sum: 96.26903939247131
time_elpased: 1.855
batch start
#iterations: 132
currently lose_sum: 96.39368915557861
time_elpased: 1.945
batch start
#iterations: 133
currently lose_sum: 96.26676404476166
time_elpased: 1.852
batch start
#iterations: 134
currently lose_sum: 96.13396245241165
time_elpased: 1.868
batch start
#iterations: 135
currently lose_sum: 96.47079050540924
time_elpased: 1.867
batch start
#iterations: 136
currently lose_sum: 96.44807237386703
time_elpased: 1.905
batch start
#iterations: 137
currently lose_sum: 96.61677974462509
time_elpased: 1.887
batch start
#iterations: 138
currently lose_sum: 96.13385182619095
time_elpased: 1.888
batch start
#iterations: 139
currently lose_sum: 96.54510343074799
time_elpased: 1.855
start validation test
0.619536082474
0.632666969559
0.573222187918
0.60147940176
0.61961739366
63.295
batch start
#iterations: 140
currently lose_sum: 96.56355583667755
time_elpased: 1.901
batch start
#iterations: 141
currently lose_sum: 96.49061471223831
time_elpased: 1.866
batch start
#iterations: 142
currently lose_sum: 96.38740712404251
time_elpased: 1.888
batch start
#iterations: 143
currently lose_sum: 96.03972351551056
time_elpased: 1.909
batch start
#iterations: 144
currently lose_sum: 96.6695926785469
time_elpased: 1.835
batch start
#iterations: 145
currently lose_sum: 96.16475832462311
time_elpased: 1.899
batch start
#iterations: 146
currently lose_sum: 95.70108413696289
time_elpased: 1.909
batch start
#iterations: 147
currently lose_sum: 96.23010236024857
time_elpased: 1.892
batch start
#iterations: 148
currently lose_sum: 96.73571622371674
time_elpased: 1.967
batch start
#iterations: 149
currently lose_sum: 95.99939060211182
time_elpased: 1.94
batch start
#iterations: 150
currently lose_sum: 95.73847657442093
time_elpased: 1.879
batch start
#iterations: 151
currently lose_sum: 96.4423161149025
time_elpased: 1.882
batch start
#iterations: 152
currently lose_sum: 96.39297133684158
time_elpased: 1.9
batch start
#iterations: 153
currently lose_sum: 96.20462173223495
time_elpased: 1.984
batch start
#iterations: 154
currently lose_sum: 95.90216112136841
time_elpased: 1.899
batch start
#iterations: 155
currently lose_sum: 95.77077257633209
time_elpased: 1.972
batch start
#iterations: 156
currently lose_sum: 96.20917081832886
time_elpased: 1.869
batch start
#iterations: 157
currently lose_sum: 96.1055359840393
time_elpased: 1.941
batch start
#iterations: 158
currently lose_sum: 96.60732793807983
time_elpased: 1.87
batch start
#iterations: 159
currently lose_sum: 96.13215363025665
time_elpased: 1.934
start validation test
0.622268041237
0.655554108608
0.518061129978
0.578753736491
0.622450992543
63.405
batch start
#iterations: 160
currently lose_sum: 96.19696980714798
time_elpased: 1.845
batch start
#iterations: 161
currently lose_sum: 95.80628025531769
time_elpased: 1.909
batch start
#iterations: 162
currently lose_sum: 95.87696599960327
time_elpased: 1.925
batch start
#iterations: 163
currently lose_sum: 95.79564189910889
time_elpased: 1.892
batch start
#iterations: 164
currently lose_sum: 96.00592976808548
time_elpased: 1.9
batch start
#iterations: 165
currently lose_sum: 95.93369495868683
time_elpased: 1.877
batch start
#iterations: 166
currently lose_sum: 95.90590316057205
time_elpased: 1.881
batch start
#iterations: 167
currently lose_sum: 95.86288392543793
time_elpased: 1.928
batch start
#iterations: 168
currently lose_sum: 95.58085185289383
time_elpased: 1.878
batch start
#iterations: 169
currently lose_sum: 95.63019615411758
time_elpased: 1.905
batch start
#iterations: 170
currently lose_sum: 95.65366369485855
time_elpased: 1.885
batch start
#iterations: 171
currently lose_sum: 95.78640365600586
time_elpased: 1.868
batch start
#iterations: 172
currently lose_sum: 95.47953230142593
time_elpased: 1.91
batch start
#iterations: 173
currently lose_sum: 95.58879041671753
time_elpased: 1.928
batch start
#iterations: 174
currently lose_sum: 95.57577979564667
time_elpased: 1.929
batch start
#iterations: 175
currently lose_sum: 95.3469415307045
time_elpased: 1.947
batch start
#iterations: 176
currently lose_sum: 95.58990621566772
time_elpased: 1.947
batch start
#iterations: 177
currently lose_sum: 95.32045632600784
time_elpased: 1.91
batch start
#iterations: 178
currently lose_sum: 95.34205323457718
time_elpased: 1.948
batch start
#iterations: 179
currently lose_sum: 95.5423292517662
time_elpased: 1.868
start validation test
0.610773195876
0.621794871795
0.569002778635
0.594228599065
0.610846530286
64.242
batch start
#iterations: 180
currently lose_sum: 95.74846065044403
time_elpased: 1.944
batch start
#iterations: 181
currently lose_sum: 95.1669647693634
time_elpased: 1.895
batch start
#iterations: 182
currently lose_sum: 95.37521851062775
time_elpased: 1.863
batch start
#iterations: 183
currently lose_sum: 95.06954038143158
time_elpased: 1.876
batch start
#iterations: 184
currently lose_sum: 95.17084950208664
time_elpased: 1.966
batch start
#iterations: 185
currently lose_sum: 95.52862364053726
time_elpased: 1.894
batch start
#iterations: 186
currently lose_sum: 95.18139773607254
time_elpased: 1.84
batch start
#iterations: 187
currently lose_sum: 95.18274116516113
time_elpased: 1.929
batch start
#iterations: 188
currently lose_sum: 95.19423472881317
time_elpased: 1.876
batch start
#iterations: 189
currently lose_sum: 95.29745757579803
time_elpased: 1.901
batch start
#iterations: 190
currently lose_sum: 95.49634528160095
time_elpased: 1.881
batch start
#iterations: 191
currently lose_sum: 94.96351706981659
time_elpased: 1.93
batch start
#iterations: 192
currently lose_sum: 94.92645621299744
time_elpased: 1.897
batch start
#iterations: 193
currently lose_sum: 94.9806740283966
time_elpased: 1.896
batch start
#iterations: 194
currently lose_sum: 95.25097179412842
time_elpased: 1.873
batch start
#iterations: 195
currently lose_sum: 95.12941324710846
time_elpased: 1.859
batch start
#iterations: 196
currently lose_sum: 95.32171219587326
time_elpased: 1.894
batch start
#iterations: 197
currently lose_sum: 94.9918103814125
time_elpased: 1.857
batch start
#iterations: 198
currently lose_sum: 94.73283463716507
time_elpased: 1.889
batch start
#iterations: 199
currently lose_sum: 94.82491827011108
time_elpased: 1.874
start validation test
0.600051546392
0.611376564278
0.553051353298
0.580753228508
0.60013406248
64.431
batch start
#iterations: 200
currently lose_sum: 95.14168298244476
time_elpased: 1.899
batch start
#iterations: 201
currently lose_sum: 95.09228867292404
time_elpased: 1.884
batch start
#iterations: 202
currently lose_sum: 94.75245571136475
time_elpased: 1.88
batch start
#iterations: 203
currently lose_sum: 94.8660963177681
time_elpased: 1.903
batch start
#iterations: 204
currently lose_sum: 94.64138889312744
time_elpased: 1.924
batch start
#iterations: 205
currently lose_sum: 95.30381864309311
time_elpased: 1.908
batch start
#iterations: 206
currently lose_sum: 94.8573984503746
time_elpased: 1.882
batch start
#iterations: 207
currently lose_sum: 94.72162061929703
time_elpased: 1.882
batch start
#iterations: 208
currently lose_sum: 94.14809727668762
time_elpased: 1.888
batch start
#iterations: 209
currently lose_sum: 94.5426869392395
time_elpased: 1.931
batch start
#iterations: 210
currently lose_sum: 94.68754380941391
time_elpased: 1.943
batch start
#iterations: 211
currently lose_sum: 94.27867197990417
time_elpased: 1.906
batch start
#iterations: 212
currently lose_sum: 94.37113296985626
time_elpased: 1.89
batch start
#iterations: 213
currently lose_sum: 94.31525683403015
time_elpased: 1.918
batch start
#iterations: 214
currently lose_sum: 94.56506645679474
time_elpased: 1.912
batch start
#iterations: 215
currently lose_sum: 94.36008220911026
time_elpased: 1.881
batch start
#iterations: 216
currently lose_sum: 94.40228062868118
time_elpased: 1.901
batch start
#iterations: 217
currently lose_sum: 94.30271005630493
time_elpased: 1.869
batch start
#iterations: 218
currently lose_sum: 94.57561373710632
time_elpased: 1.871
batch start
#iterations: 219
currently lose_sum: 94.44807291030884
time_elpased: 1.943
start validation test
0.613041237113
0.631956054454
0.544612534733
0.58504228622
0.613161374255
63.972
batch start
#iterations: 220
currently lose_sum: 93.89612871408463
time_elpased: 1.856
batch start
#iterations: 221
currently lose_sum: 93.9826831817627
time_elpased: 1.866
batch start
#iterations: 222
currently lose_sum: 93.82314240932465
time_elpased: 1.876
batch start
#iterations: 223
currently lose_sum: 93.75017243623734
time_elpased: 1.877
batch start
#iterations: 224
currently lose_sum: 93.95650506019592
time_elpased: 1.895
batch start
#iterations: 225
currently lose_sum: 94.17213875055313
time_elpased: 1.863
batch start
#iterations: 226
currently lose_sum: 93.88575249910355
time_elpased: 1.871
batch start
#iterations: 227
currently lose_sum: 93.79516184329987
time_elpased: 1.871
batch start
#iterations: 228
currently lose_sum: 93.94235861301422
time_elpased: 1.881
batch start
#iterations: 229
currently lose_sum: 93.77941691875458
time_elpased: 1.962
batch start
#iterations: 230
currently lose_sum: 93.97858113050461
time_elpased: 1.875
batch start
#iterations: 231
currently lose_sum: 93.9724548459053
time_elpased: 1.915
batch start
#iterations: 232
currently lose_sum: 93.957350730896
time_elpased: 1.873
batch start
#iterations: 233
currently lose_sum: 93.78134888410568
time_elpased: 1.899
batch start
#iterations: 234
currently lose_sum: 93.85969191789627
time_elpased: 1.87
batch start
#iterations: 235
currently lose_sum: 93.54922449588776
time_elpased: 1.925
batch start
#iterations: 236
currently lose_sum: 93.4823949933052
time_elpased: 1.847
batch start
#iterations: 237
currently lose_sum: 93.69490599632263
time_elpased: 1.869
batch start
#iterations: 238
currently lose_sum: 93.94982320070267
time_elpased: 1.843
batch start
#iterations: 239
currently lose_sum: 93.18322211503983
time_elpased: 1.883
start validation test
0.60206185567
0.622440220723
0.522383451683
0.568039391226
0.602201743398
64.870
batch start
#iterations: 240
currently lose_sum: 93.50681710243225
time_elpased: 1.858
batch start
#iterations: 241
currently lose_sum: 93.47046715021133
time_elpased: 1.88
batch start
#iterations: 242
currently lose_sum: 93.21152943372726
time_elpased: 1.917
batch start
#iterations: 243
currently lose_sum: 93.32598888874054
time_elpased: 1.903
batch start
#iterations: 244
currently lose_sum: 93.49854552745819
time_elpased: 1.883
batch start
#iterations: 245
currently lose_sum: 93.23382717370987
time_elpased: 1.884
batch start
#iterations: 246
currently lose_sum: 92.95901030302048
time_elpased: 1.915
batch start
#iterations: 247
currently lose_sum: 92.94055700302124
time_elpased: 1.914
batch start
#iterations: 248
currently lose_sum: 93.26583576202393
time_elpased: 1.904
batch start
#iterations: 249
currently lose_sum: 93.43147760629654
time_elpased: 1.902
batch start
#iterations: 250
currently lose_sum: 92.96530544757843
time_elpased: 1.9
batch start
#iterations: 251
currently lose_sum: 93.0110182762146
time_elpased: 1.875
batch start
#iterations: 252
currently lose_sum: 92.75850129127502
time_elpased: 1.999
batch start
#iterations: 253
currently lose_sum: 92.58856278657913
time_elpased: 1.911
batch start
#iterations: 254
currently lose_sum: 92.76980304718018
time_elpased: 1.892
batch start
#iterations: 255
currently lose_sum: 92.73089349269867
time_elpased: 1.849
batch start
#iterations: 256
currently lose_sum: 92.57220733165741
time_elpased: 1.961
batch start
#iterations: 257
currently lose_sum: 92.4817761182785
time_elpased: 1.893
batch start
#iterations: 258
currently lose_sum: 92.48780184984207
time_elpased: 1.904
batch start
#iterations: 259
currently lose_sum: 92.65835076570511
time_elpased: 1.941
start validation test
0.607422680412
0.632188247137
0.517032005763
0.56884057971
0.607581375184
65.326
batch start
#iterations: 260
currently lose_sum: 92.3668504357338
time_elpased: 1.864
batch start
#iterations: 261
currently lose_sum: 92.52622330188751
time_elpased: 1.904
batch start
#iterations: 262
currently lose_sum: 92.01685202121735
time_elpased: 1.928
batch start
#iterations: 263
currently lose_sum: 91.71808809041977
time_elpased: 1.916
batch start
#iterations: 264
currently lose_sum: 92.65285390615463
time_elpased: 1.865
batch start
#iterations: 265
currently lose_sum: 92.6220383644104
time_elpased: 1.906
batch start
#iterations: 266
currently lose_sum: 92.53044056892395
time_elpased: 1.888
batch start
#iterations: 267
currently lose_sum: 92.2584919333458
time_elpased: 1.923
batch start
#iterations: 268
currently lose_sum: 92.08842557668686
time_elpased: 1.871
batch start
#iterations: 269
currently lose_sum: 91.71119493246078
time_elpased: 1.862
batch start
#iterations: 270
currently lose_sum: 91.87671065330505
time_elpased: 1.848
batch start
#iterations: 271
currently lose_sum: 91.74469006061554
time_elpased: 1.922
batch start
#iterations: 272
currently lose_sum: 92.09401106834412
time_elpased: 1.886
batch start
#iterations: 273
currently lose_sum: 91.43233215808868
time_elpased: 1.955
batch start
#iterations: 274
currently lose_sum: 91.9492775797844
time_elpased: 1.866
batch start
#iterations: 275
currently lose_sum: 91.87160205841064
time_elpased: 1.897
batch start
#iterations: 276
currently lose_sum: 91.16223269701004
time_elpased: 1.913
batch start
#iterations: 277
currently lose_sum: 91.36890888214111
time_elpased: 1.84
batch start
#iterations: 278
currently lose_sum: 92.01352548599243
time_elpased: 1.905
batch start
#iterations: 279
currently lose_sum: 91.05460011959076
time_elpased: 1.864
start validation test
0.600773195876
0.63349580287
0.481527220335
0.547155469801
0.600982550579
66.912
batch start
#iterations: 280
currently lose_sum: 91.29468250274658
time_elpased: 1.885
batch start
#iterations: 281
currently lose_sum: 91.8558052778244
time_elpased: 1.898
batch start
#iterations: 282
currently lose_sum: 91.2082993388176
time_elpased: 1.861
batch start
#iterations: 283
currently lose_sum: 91.3978380560875
time_elpased: 1.908
batch start
#iterations: 284
currently lose_sum: 91.37223947048187
time_elpased: 1.885
batch start
#iterations: 285
currently lose_sum: 90.92548418045044
time_elpased: 1.895
batch start
#iterations: 286
currently lose_sum: 90.73753219842911
time_elpased: 1.932
batch start
#iterations: 287
currently lose_sum: 90.80875754356384
time_elpased: 1.944
batch start
#iterations: 288
currently lose_sum: 90.32824975252151
time_elpased: 1.883
batch start
#iterations: 289
currently lose_sum: 91.41961348056793
time_elpased: 1.925
batch start
#iterations: 290
currently lose_sum: 91.08387809991837
time_elpased: 1.891
batch start
#iterations: 291
currently lose_sum: 90.17750400304794
time_elpased: 1.92
batch start
#iterations: 292
currently lose_sum: 90.83837580680847
time_elpased: 1.943
batch start
#iterations: 293
currently lose_sum: 90.87807261943817
time_elpased: 1.895
batch start
#iterations: 294
currently lose_sum: 90.60931378602982
time_elpased: 1.906
batch start
#iterations: 295
currently lose_sum: 90.4295991063118
time_elpased: 1.842
batch start
#iterations: 296
currently lose_sum: 90.60947227478027
time_elpased: 1.859
batch start
#iterations: 297
currently lose_sum: 90.72037798166275
time_elpased: 1.954
batch start
#iterations: 298
currently lose_sum: 90.39583587646484
time_elpased: 1.862
batch start
#iterations: 299
currently lose_sum: 90.2203716635704
time_elpased: 1.882
start validation test
0.600309278351
0.631339488826
0.485540804775
0.548923792903
0.600510772108
68.524
batch start
#iterations: 300
currently lose_sum: 90.57919871807098
time_elpased: 1.857
batch start
#iterations: 301
currently lose_sum: 90.00111001729965
time_elpased: 1.94
batch start
#iterations: 302
currently lose_sum: 90.01794499158859
time_elpased: 1.859
batch start
#iterations: 303
currently lose_sum: 90.29794389009476
time_elpased: 1.949
batch start
#iterations: 304
currently lose_sum: 89.94404464960098
time_elpased: 1.916
batch start
#iterations: 305
currently lose_sum: 89.61208671331406
time_elpased: 1.949
batch start
#iterations: 306
currently lose_sum: 89.70303183794022
time_elpased: 1.913
batch start
#iterations: 307
currently lose_sum: 89.87649947404861
time_elpased: 1.937
batch start
#iterations: 308
currently lose_sum: 89.95173585414886
time_elpased: 1.904
batch start
#iterations: 309
currently lose_sum: 89.79752516746521
time_elpased: 1.915
batch start
#iterations: 310
currently lose_sum: 89.23028069734573
time_elpased: 1.906
batch start
#iterations: 311
currently lose_sum: 89.21304768323898
time_elpased: 1.989
batch start
#iterations: 312
currently lose_sum: 88.90417468547821
time_elpased: 1.905
batch start
#iterations: 313
currently lose_sum: 89.01120936870575
time_elpased: 1.896
batch start
#iterations: 314
currently lose_sum: 88.70077610015869
time_elpased: 1.875
batch start
#iterations: 315
currently lose_sum: 89.51620888710022
time_elpased: 1.884
batch start
#iterations: 316
currently lose_sum: 89.05693078041077
time_elpased: 1.88
batch start
#iterations: 317
currently lose_sum: 89.42726701498032
time_elpased: 1.897
batch start
#iterations: 318
currently lose_sum: 89.03007996082306
time_elpased: 1.923
batch start
#iterations: 319
currently lose_sum: 88.53937631845474
time_elpased: 1.919
start validation test
0.59118556701
0.611597100725
0.503653390964
0.552401377053
0.591339243246
69.674
batch start
#iterations: 320
currently lose_sum: 88.9592199921608
time_elpased: 1.88
batch start
#iterations: 321
currently lose_sum: 88.99739247560501
time_elpased: 1.882
batch start
#iterations: 322
currently lose_sum: 88.35646271705627
time_elpased: 1.917
batch start
#iterations: 323
currently lose_sum: 88.99397045373917
time_elpased: 1.895
batch start
#iterations: 324
currently lose_sum: 88.2640368938446
time_elpased: 1.872
batch start
#iterations: 325
currently lose_sum: 88.22682094573975
time_elpased: 1.884
batch start
#iterations: 326
currently lose_sum: 88.14890730381012
time_elpased: 1.926
batch start
#iterations: 327
currently lose_sum: 88.3616886138916
time_elpased: 1.92
batch start
#iterations: 328
currently lose_sum: 87.99455392360687
time_elpased: 1.887
batch start
#iterations: 329
currently lose_sum: 87.84331613779068
time_elpased: 1.879
batch start
#iterations: 330
currently lose_sum: 87.88635325431824
time_elpased: 1.876
batch start
#iterations: 331
currently lose_sum: 88.2403102517128
time_elpased: 1.914
batch start
#iterations: 332
currently lose_sum: 87.6338928937912
time_elpased: 1.95
batch start
#iterations: 333
currently lose_sum: 87.6594386100769
time_elpased: 1.9
batch start
#iterations: 334
currently lose_sum: 87.0123193860054
time_elpased: 1.887
batch start
#iterations: 335
currently lose_sum: 87.559550344944
time_elpased: 1.897
batch start
#iterations: 336
currently lose_sum: 87.41161894798279
time_elpased: 1.883
batch start
#iterations: 337
currently lose_sum: 87.12160241603851
time_elpased: 1.881
batch start
#iterations: 338
currently lose_sum: 87.47084647417068
time_elpased: 1.905
batch start
#iterations: 339
currently lose_sum: 87.04416620731354
time_elpased: 1.93
start validation test
0.591494845361
0.621904761905
0.47041267881
0.535653600516
0.59170742378
71.592
batch start
#iterations: 340
currently lose_sum: 87.38974094390869
time_elpased: 1.874
batch start
#iterations: 341
currently lose_sum: 87.07602620124817
time_elpased: 1.899
batch start
#iterations: 342
currently lose_sum: 86.93352717161179
time_elpased: 1.874
batch start
#iterations: 343
currently lose_sum: 87.20678389072418
time_elpased: 1.965
batch start
#iterations: 344
currently lose_sum: 86.92670524120331
time_elpased: 1.903
batch start
#iterations: 345
currently lose_sum: 86.45158350467682
time_elpased: 1.889
batch start
#iterations: 346
currently lose_sum: 86.96911770105362
time_elpased: 1.905
batch start
#iterations: 347
currently lose_sum: 86.26160395145416
time_elpased: 1.924
batch start
#iterations: 348
currently lose_sum: 86.08959412574768
time_elpased: 1.87
batch start
#iterations: 349
currently lose_sum: 86.10901921987534
time_elpased: 1.902
batch start
#iterations: 350
currently lose_sum: 86.05001950263977
time_elpased: 1.917
batch start
#iterations: 351
currently lose_sum: 86.51734673976898
time_elpased: 1.877
batch start
#iterations: 352
currently lose_sum: 86.04605638980865
time_elpased: 1.89
batch start
#iterations: 353
currently lose_sum: 86.62023317813873
time_elpased: 1.873
batch start
#iterations: 354
currently lose_sum: 86.05969125032425
time_elpased: 1.888
batch start
#iterations: 355
currently lose_sum: 85.37816578149796
time_elpased: 1.881
batch start
#iterations: 356
currently lose_sum: 84.95192128419876
time_elpased: 1.923
batch start
#iterations: 357
currently lose_sum: 85.38843482732773
time_elpased: 1.888
batch start
#iterations: 358
currently lose_sum: 85.60801088809967
time_elpased: 1.847
batch start
#iterations: 359
currently lose_sum: 85.14496177434921
time_elpased: 1.935
start validation test
0.587835051546
0.616111186075
0.469898116703
0.533162073797
0.588042108026
73.807
batch start
#iterations: 360
currently lose_sum: 85.03866320848465
time_elpased: 1.956
batch start
#iterations: 361
currently lose_sum: 85.4310992360115
time_elpased: 1.898
batch start
#iterations: 362
currently lose_sum: 85.20004552602768
time_elpased: 1.896
batch start
#iterations: 363
currently lose_sum: 85.1395982503891
time_elpased: 1.887
batch start
#iterations: 364
currently lose_sum: 84.93496751785278
time_elpased: 1.933
batch start
#iterations: 365
currently lose_sum: 84.6044362783432
time_elpased: 1.939
batch start
#iterations: 366
currently lose_sum: 84.85333508253098
time_elpased: 1.918
batch start
#iterations: 367
currently lose_sum: 84.40062808990479
time_elpased: 1.933
batch start
#iterations: 368
currently lose_sum: 85.14041513204575
time_elpased: 1.909
batch start
#iterations: 369
currently lose_sum: 84.35630363225937
time_elpased: 1.936
batch start
#iterations: 370
currently lose_sum: 84.02741926908493
time_elpased: 1.965
batch start
#iterations: 371
currently lose_sum: 84.80192965269089
time_elpased: 1.9
batch start
#iterations: 372
currently lose_sum: 83.87332570552826
time_elpased: 1.913
batch start
#iterations: 373
currently lose_sum: 84.58519542217255
time_elpased: 2.025
batch start
#iterations: 374
currently lose_sum: 84.01785570383072
time_elpased: 1.852
batch start
#iterations: 375
currently lose_sum: 83.35254645347595
time_elpased: 1.881
batch start
#iterations: 376
currently lose_sum: 84.557277739048
time_elpased: 1.913
batch start
#iterations: 377
currently lose_sum: 83.82197338342667
time_elpased: 1.933
batch start
#iterations: 378
currently lose_sum: 82.93995070457458
time_elpased: 1.913
batch start
#iterations: 379
currently lose_sum: 83.5507909655571
time_elpased: 1.896
start validation test
0.583092783505
0.617958001448
0.439127302665
0.513415954759
0.583345537112
79.999
batch start
#iterations: 380
currently lose_sum: 83.36966109275818
time_elpased: 1.862
batch start
#iterations: 381
currently lose_sum: 83.23268020153046
time_elpased: 1.961
batch start
#iterations: 382
currently lose_sum: 82.96599757671356
time_elpased: 1.967
batch start
#iterations: 383
currently lose_sum: 83.09531283378601
time_elpased: 1.946
batch start
#iterations: 384
currently lose_sum: 82.43384265899658
time_elpased: 1.893
batch start
#iterations: 385
currently lose_sum: 82.63535010814667
time_elpased: 1.883
batch start
#iterations: 386
currently lose_sum: 82.17353951931
time_elpased: 1.882
batch start
#iterations: 387
currently lose_sum: 82.30435794591904
time_elpased: 1.996
batch start
#iterations: 388
currently lose_sum: 83.10867539048195
time_elpased: 1.89
batch start
#iterations: 389
currently lose_sum: 82.48654520511627
time_elpased: 1.919
batch start
#iterations: 390
currently lose_sum: 82.01083433628082
time_elpased: 1.867
batch start
#iterations: 391
currently lose_sum: 82.4253596663475
time_elpased: 1.906
batch start
#iterations: 392
currently lose_sum: 81.72092136740685
time_elpased: 1.883
batch start
#iterations: 393
currently lose_sum: 82.36242941021919
time_elpased: 1.94
batch start
#iterations: 394
currently lose_sum: 82.72222805023193
time_elpased: 1.866
batch start
#iterations: 395
currently lose_sum: 81.81426158547401
time_elpased: 1.911
batch start
#iterations: 396
currently lose_sum: 81.21543544530869
time_elpased: 1.884
batch start
#iterations: 397
currently lose_sum: 81.15761002898216
time_elpased: 1.929
batch start
#iterations: 398
currently lose_sum: 81.35213506221771
time_elpased: 1.889
batch start
#iterations: 399
currently lose_sum: 81.07189857959747
time_elpased: 1.926
start validation test
0.574329896907
0.60384341637
0.436554492127
0.506749492295
0.574571782881
83.644
acc: 0.624
pre: 0.637
rec: 0.580
F1: 0.607
auc: 0.624
