start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.46533787250519
time_elpased: 1.885
batch start
#iterations: 1
currently lose_sum: 100.34729725122452
time_elpased: 1.832
batch start
#iterations: 2
currently lose_sum: 100.27373915910721
time_elpased: 1.839
batch start
#iterations: 3
currently lose_sum: 100.25598227977753
time_elpased: 1.859
batch start
#iterations: 4
currently lose_sum: 100.14422059059143
time_elpased: 1.857
batch start
#iterations: 5
currently lose_sum: 100.0851257443428
time_elpased: 1.848
batch start
#iterations: 6
currently lose_sum: 99.99093163013458
time_elpased: 1.849
batch start
#iterations: 7
currently lose_sum: 99.94145274162292
time_elpased: 1.931
batch start
#iterations: 8
currently lose_sum: 99.7973403930664
time_elpased: 1.859
batch start
#iterations: 9
currently lose_sum: 99.57495045661926
time_elpased: 1.803
batch start
#iterations: 10
currently lose_sum: 99.78548580408096
time_elpased: 1.876
batch start
#iterations: 11
currently lose_sum: 99.6958178281784
time_elpased: 1.856
batch start
#iterations: 12
currently lose_sum: 99.5835410952568
time_elpased: 1.839
batch start
#iterations: 13
currently lose_sum: 99.59915345907211
time_elpased: 1.845
batch start
#iterations: 14
currently lose_sum: 99.21320295333862
time_elpased: 1.824
batch start
#iterations: 15
currently lose_sum: 99.44940859079361
time_elpased: 1.817
batch start
#iterations: 16
currently lose_sum: 99.335642516613
time_elpased: 1.855
batch start
#iterations: 17
currently lose_sum: 99.20559585094452
time_elpased: 1.842
batch start
#iterations: 18
currently lose_sum: 99.14184761047363
time_elpased: 1.832
batch start
#iterations: 19
currently lose_sum: 99.03514236211777
time_elpased: 1.85
start validation test
0.591288659794
0.568067610781
0.76782957703
0.653012997243
0.590978714984
65.668
batch start
#iterations: 20
currently lose_sum: 99.14287662506104
time_elpased: 1.892
batch start
#iterations: 21
currently lose_sum: 99.10073333978653
time_elpased: 1.88
batch start
#iterations: 22
currently lose_sum: 99.09496062994003
time_elpased: 1.821
batch start
#iterations: 23
currently lose_sum: 98.81376433372498
time_elpased: 1.902
batch start
#iterations: 24
currently lose_sum: 98.72719377279282
time_elpased: 1.88
batch start
#iterations: 25
currently lose_sum: 98.84972083568573
time_elpased: 1.874
batch start
#iterations: 26
currently lose_sum: 98.85888719558716
time_elpased: 1.816
batch start
#iterations: 27
currently lose_sum: 98.29498583078384
time_elpased: 1.846
batch start
#iterations: 28
currently lose_sum: 98.75899052619934
time_elpased: 1.859
batch start
#iterations: 29
currently lose_sum: 98.26695454120636
time_elpased: 1.871
batch start
#iterations: 30
currently lose_sum: 98.59204161167145
time_elpased: 1.877
batch start
#iterations: 31
currently lose_sum: 98.54311829805374
time_elpased: 1.835
batch start
#iterations: 32
currently lose_sum: 98.60048365592957
time_elpased: 1.836
batch start
#iterations: 33
currently lose_sum: 98.47235941886902
time_elpased: 1.885
batch start
#iterations: 34
currently lose_sum: 98.3566906452179
time_elpased: 1.91
batch start
#iterations: 35
currently lose_sum: 98.5715280175209
time_elpased: 1.827
batch start
#iterations: 36
currently lose_sum: 98.45093548297882
time_elpased: 1.868
batch start
#iterations: 37
currently lose_sum: 98.21312671899796
time_elpased: 1.87
batch start
#iterations: 38
currently lose_sum: 98.20265340805054
time_elpased: 1.836
batch start
#iterations: 39
currently lose_sum: 98.43203097581863
time_elpased: 1.857
start validation test
0.619484536082
0.618805332248
0.62581043532
0.622288170282
0.619473429991
64.270
batch start
#iterations: 40
currently lose_sum: 98.14678472280502
time_elpased: 1.823
batch start
#iterations: 41
currently lose_sum: 98.18221807479858
time_elpased: 1.826
batch start
#iterations: 42
currently lose_sum: 98.34792536497116
time_elpased: 1.931
batch start
#iterations: 43
currently lose_sum: 98.09521895647049
time_elpased: 1.865
batch start
#iterations: 44
currently lose_sum: 98.0743213891983
time_elpased: 1.902
batch start
#iterations: 45
currently lose_sum: 97.79651778936386
time_elpased: 1.87
batch start
#iterations: 46
currently lose_sum: 98.0049689412117
time_elpased: 1.879
batch start
#iterations: 47
currently lose_sum: 98.21748608350754
time_elpased: 1.874
batch start
#iterations: 48
currently lose_sum: 98.0529956817627
time_elpased: 1.849
batch start
#iterations: 49
currently lose_sum: 97.82876461744308
time_elpased: 1.863
batch start
#iterations: 50
currently lose_sum: 97.75821876525879
time_elpased: 1.832
batch start
#iterations: 51
currently lose_sum: 97.9802416563034
time_elpased: 1.865
batch start
#iterations: 52
currently lose_sum: 98.08575558662415
time_elpased: 1.937
batch start
#iterations: 53
currently lose_sum: 97.65608859062195
time_elpased: 1.852
batch start
#iterations: 54
currently lose_sum: 97.94593745470047
time_elpased: 1.92
batch start
#iterations: 55
currently lose_sum: 97.7286701798439
time_elpased: 1.835
batch start
#iterations: 56
currently lose_sum: 97.73537403345108
time_elpased: 1.921
batch start
#iterations: 57
currently lose_sum: 98.07320982217789
time_elpased: 1.85
batch start
#iterations: 58
currently lose_sum: 97.689992249012
time_elpased: 1.879
batch start
#iterations: 59
currently lose_sum: 97.94764637947083
time_elpased: 1.861
start validation test
0.612319587629
0.612384851586
0.61572501801
0.614050392569
0.612313608871
64.226
batch start
#iterations: 60
currently lose_sum: 97.79105317592621
time_elpased: 1.795
batch start
#iterations: 61
currently lose_sum: 98.02430820465088
time_elpased: 1.82
batch start
#iterations: 62
currently lose_sum: 97.70886766910553
time_elpased: 1.857
batch start
#iterations: 63
currently lose_sum: 97.72272509336472
time_elpased: 1.85
batch start
#iterations: 64
currently lose_sum: 97.99079191684723
time_elpased: 1.871
batch start
#iterations: 65
currently lose_sum: 97.63098275661469
time_elpased: 1.864
batch start
#iterations: 66
currently lose_sum: 97.63772189617157
time_elpased: 1.872
batch start
#iterations: 67
currently lose_sum: 97.3123437166214
time_elpased: 1.852
batch start
#iterations: 68
currently lose_sum: 97.5309129357338
time_elpased: 1.861
batch start
#iterations: 69
currently lose_sum: 97.75025409460068
time_elpased: 1.881
batch start
#iterations: 70
currently lose_sum: 97.43679451942444
time_elpased: 1.899
batch start
#iterations: 71
currently lose_sum: 97.76219522953033
time_elpased: 1.88
batch start
#iterations: 72
currently lose_sum: 97.49386817216873
time_elpased: 1.893
batch start
#iterations: 73
currently lose_sum: 97.58470845222473
time_elpased: 1.855
batch start
#iterations: 74
currently lose_sum: 97.4984820485115
time_elpased: 1.812
batch start
#iterations: 75
currently lose_sum: 97.47522765398026
time_elpased: 1.831
batch start
#iterations: 76
currently lose_sum: 97.35620111227036
time_elpased: 1.863
batch start
#iterations: 77
currently lose_sum: 97.63454085588455
time_elpased: 1.853
batch start
#iterations: 78
currently lose_sum: 97.63030874729156
time_elpased: 1.879
batch start
#iterations: 79
currently lose_sum: 97.1638211607933
time_elpased: 1.878
start validation test
0.616546391753
0.636081242533
0.547905732222
0.588710123293
0.616666901017
63.738
batch start
#iterations: 80
currently lose_sum: 97.68592768907547
time_elpased: 1.855
batch start
#iterations: 81
currently lose_sum: 97.32883036136627
time_elpased: 1.847
batch start
#iterations: 82
currently lose_sum: 97.56498712301254
time_elpased: 1.806
batch start
#iterations: 83
currently lose_sum: 97.37334251403809
time_elpased: 1.856
batch start
#iterations: 84
currently lose_sum: 97.5593312382698
time_elpased: 1.857
batch start
#iterations: 85
currently lose_sum: 97.36380219459534
time_elpased: 1.884
batch start
#iterations: 86
currently lose_sum: 97.34223711490631
time_elpased: 1.87
batch start
#iterations: 87
currently lose_sum: 97.59161072969437
time_elpased: 1.85
batch start
#iterations: 88
currently lose_sum: 97.36066919565201
time_elpased: 1.865
batch start
#iterations: 89
currently lose_sum: 97.4865392446518
time_elpased: 1.886
batch start
#iterations: 90
currently lose_sum: 97.33516639471054
time_elpased: 1.842
batch start
#iterations: 91
currently lose_sum: 97.57635825872421
time_elpased: 1.879
batch start
#iterations: 92
currently lose_sum: 97.02004659175873
time_elpased: 1.826
batch start
#iterations: 93
currently lose_sum: 97.16043710708618
time_elpased: 1.881
batch start
#iterations: 94
currently lose_sum: 97.31896907091141
time_elpased: 1.832
batch start
#iterations: 95
currently lose_sum: 97.16990864276886
time_elpased: 1.842
batch start
#iterations: 96
currently lose_sum: 97.39395809173584
time_elpased: 1.874
batch start
#iterations: 97
currently lose_sum: 97.49182331562042
time_elpased: 1.847
batch start
#iterations: 98
currently lose_sum: 97.21807765960693
time_elpased: 1.869
batch start
#iterations: 99
currently lose_sum: 97.39267599582672
time_elpased: 1.839
start validation test
0.628041237113
0.632791759584
0.613255119893
0.622870283265
0.628067196423
63.287
batch start
#iterations: 100
currently lose_sum: 96.95696312189102
time_elpased: 1.831
batch start
#iterations: 101
currently lose_sum: 97.11277401447296
time_elpased: 1.858
batch start
#iterations: 102
currently lose_sum: 97.1603102684021
time_elpased: 1.866
batch start
#iterations: 103
currently lose_sum: 97.22799360752106
time_elpased: 1.892
batch start
#iterations: 104
currently lose_sum: 97.03458780050278
time_elpased: 1.89
batch start
#iterations: 105
currently lose_sum: 97.05879986286163
time_elpased: 1.833
batch start
#iterations: 106
currently lose_sum: 97.1037425994873
time_elpased: 1.858
batch start
#iterations: 107
currently lose_sum: 96.87512159347534
time_elpased: 1.9
batch start
#iterations: 108
currently lose_sum: 97.19221436977386
time_elpased: 1.876
batch start
#iterations: 109
currently lose_sum: 97.0876629948616
time_elpased: 1.833
batch start
#iterations: 110
currently lose_sum: 96.99279814958572
time_elpased: 1.937
batch start
#iterations: 111
currently lose_sum: 97.09466868638992
time_elpased: 1.864
batch start
#iterations: 112
currently lose_sum: 96.70813870429993
time_elpased: 1.854
batch start
#iterations: 113
currently lose_sum: 96.58388727903366
time_elpased: 1.864
batch start
#iterations: 114
currently lose_sum: 96.92912685871124
time_elpased: 1.808
batch start
#iterations: 115
currently lose_sum: 97.00393199920654
time_elpased: 1.838
batch start
#iterations: 116
currently lose_sum: 96.69156217575073
time_elpased: 1.87
batch start
#iterations: 117
currently lose_sum: 96.81775724887848
time_elpased: 1.853
batch start
#iterations: 118
currently lose_sum: 97.00114583969116
time_elpased: 1.848
batch start
#iterations: 119
currently lose_sum: 96.80632621049881
time_elpased: 1.854
start validation test
0.619020618557
0.620994590096
0.614284244108
0.617621190957
0.619028933993
63.664
batch start
#iterations: 120
currently lose_sum: 96.82240754365921
time_elpased: 1.855
batch start
#iterations: 121
currently lose_sum: 96.7723296880722
time_elpased: 1.854
batch start
#iterations: 122
currently lose_sum: 96.70185452699661
time_elpased: 1.853
batch start
#iterations: 123
currently lose_sum: 96.52524942159653
time_elpased: 1.845
batch start
#iterations: 124
currently lose_sum: 96.66228240728378
time_elpased: 1.942
batch start
#iterations: 125
currently lose_sum: 96.59963494539261
time_elpased: 1.84
batch start
#iterations: 126
currently lose_sum: 96.7242516875267
time_elpased: 1.825
batch start
#iterations: 127
currently lose_sum: 96.92086523771286
time_elpased: 1.877
batch start
#iterations: 128
currently lose_sum: 96.51859658956528
time_elpased: 1.862
batch start
#iterations: 129
currently lose_sum: 96.66566920280457
time_elpased: 1.858
batch start
#iterations: 130
currently lose_sum: 96.54667967557907
time_elpased: 1.871
batch start
#iterations: 131
currently lose_sum: 96.75864750146866
time_elpased: 1.862
batch start
#iterations: 132
currently lose_sum: 96.55871140956879
time_elpased: 1.855
batch start
#iterations: 133
currently lose_sum: 96.3059504032135
time_elpased: 1.807
batch start
#iterations: 134
currently lose_sum: 96.5850179195404
time_elpased: 1.84
batch start
#iterations: 135
currently lose_sum: 96.68402868509293
time_elpased: 1.851
batch start
#iterations: 136
currently lose_sum: 96.49745404720306
time_elpased: 1.906
batch start
#iterations: 137
currently lose_sum: 96.60113716125488
time_elpased: 1.867
batch start
#iterations: 138
currently lose_sum: 96.52992463111877
time_elpased: 1.918
batch start
#iterations: 139
currently lose_sum: 96.45715206861496
time_elpased: 1.821
start validation test
0.613556701031
0.626625598905
0.56529793146
0.594384028567
0.613641426744
63.660
batch start
#iterations: 140
currently lose_sum: 96.12695956230164
time_elpased: 1.865
batch start
#iterations: 141
currently lose_sum: 96.65512990951538
time_elpased: 1.855
batch start
#iterations: 142
currently lose_sum: 96.2218102812767
time_elpased: 1.868
batch start
#iterations: 143
currently lose_sum: 96.28615790605545
time_elpased: 1.836
batch start
#iterations: 144
currently lose_sum: 96.38611626625061
time_elpased: 1.83
batch start
#iterations: 145
currently lose_sum: 96.18498343229294
time_elpased: 1.832
batch start
#iterations: 146
currently lose_sum: 96.4680860042572
time_elpased: 1.857
batch start
#iterations: 147
currently lose_sum: 96.6526175737381
time_elpased: 1.839
batch start
#iterations: 148
currently lose_sum: 96.3683071732521
time_elpased: 1.864
batch start
#iterations: 149
currently lose_sum: 96.44923454523087
time_elpased: 1.891
batch start
#iterations: 150
currently lose_sum: 96.28248077630997
time_elpased: 1.857
batch start
#iterations: 151
currently lose_sum: 96.10675400495529
time_elpased: 1.803
batch start
#iterations: 152
currently lose_sum: 96.1882758140564
time_elpased: 1.872
batch start
#iterations: 153
currently lose_sum: 96.12786400318146
time_elpased: 1.849
batch start
#iterations: 154
currently lose_sum: 95.96218436956406
time_elpased: 1.822
batch start
#iterations: 155
currently lose_sum: 96.08425039052963
time_elpased: 1.854
batch start
#iterations: 156
currently lose_sum: 96.24931854009628
time_elpased: 1.898
batch start
#iterations: 157
currently lose_sum: 95.82215577363968
time_elpased: 1.885
batch start
#iterations: 158
currently lose_sum: 96.27332752943039
time_elpased: 1.853
batch start
#iterations: 159
currently lose_sum: 96.00138247013092
time_elpased: 1.831
start validation test
0.621134020619
0.632932719308
0.579911495317
0.605263157895
0.62120639312
63.236
batch start
#iterations: 160
currently lose_sum: 96.46807020902634
time_elpased: 1.909
batch start
#iterations: 161
currently lose_sum: 95.95222419500351
time_elpased: 1.875
batch start
#iterations: 162
currently lose_sum: 95.67779690027237
time_elpased: 1.857
batch start
#iterations: 163
currently lose_sum: 95.79299277067184
time_elpased: 1.842
batch start
#iterations: 164
currently lose_sum: 96.04831004142761
time_elpased: 1.864
batch start
#iterations: 165
currently lose_sum: 95.61554104089737
time_elpased: 1.87
batch start
#iterations: 166
currently lose_sum: 95.90548288822174
time_elpased: 1.851
batch start
#iterations: 167
currently lose_sum: 96.04970610141754
time_elpased: 1.842
batch start
#iterations: 168
currently lose_sum: 95.93256497383118
time_elpased: 1.836
batch start
#iterations: 169
currently lose_sum: 95.6955024600029
time_elpased: 1.851
batch start
#iterations: 170
currently lose_sum: 95.80578261613846
time_elpased: 1.89
batch start
#iterations: 171
currently lose_sum: 95.7136909365654
time_elpased: 1.832
batch start
#iterations: 172
currently lose_sum: 95.54073131084442
time_elpased: 1.901
batch start
#iterations: 173
currently lose_sum: 95.46807563304901
time_elpased: 1.856
batch start
#iterations: 174
currently lose_sum: 95.85425704717636
time_elpased: 1.833
batch start
#iterations: 175
currently lose_sum: 95.63701665401459
time_elpased: 1.817
batch start
#iterations: 176
currently lose_sum: 95.59816038608551
time_elpased: 1.909
batch start
#iterations: 177
currently lose_sum: 95.55458998680115
time_elpased: 1.84
batch start
#iterations: 178
currently lose_sum: 95.08523046970367
time_elpased: 1.836
batch start
#iterations: 179
currently lose_sum: 95.59366548061371
time_elpased: 1.84
start validation test
0.621391752577
0.644176999757
0.545332921684
0.590648163629
0.621525285586
63.601
batch start
#iterations: 180
currently lose_sum: 95.29054600000381
time_elpased: 1.848
batch start
#iterations: 181
currently lose_sum: 95.76785773038864
time_elpased: 1.876
batch start
#iterations: 182
currently lose_sum: 95.43191117048264
time_elpased: 1.838
batch start
#iterations: 183
currently lose_sum: 95.49747443199158
time_elpased: 1.858
batch start
#iterations: 184
currently lose_sum: 95.19542115926743
time_elpased: 1.89
batch start
#iterations: 185
currently lose_sum: 95.31258428096771
time_elpased: 1.806
batch start
#iterations: 186
currently lose_sum: 95.3535926938057
time_elpased: 1.854
batch start
#iterations: 187
currently lose_sum: 95.34988701343536
time_elpased: 1.811
batch start
#iterations: 188
currently lose_sum: 95.35371482372284
time_elpased: 1.916
batch start
#iterations: 189
currently lose_sum: 95.0720687508583
time_elpased: 1.824
batch start
#iterations: 190
currently lose_sum: 95.18931692838669
time_elpased: 1.845
batch start
#iterations: 191
currently lose_sum: 95.37491464614868
time_elpased: 1.898
batch start
#iterations: 192
currently lose_sum: 95.45530742406845
time_elpased: 1.813
batch start
#iterations: 193
currently lose_sum: 95.50061720609665
time_elpased: 1.822
batch start
#iterations: 194
currently lose_sum: 94.93304508924484
time_elpased: 1.894
batch start
#iterations: 195
currently lose_sum: 95.12515032291412
time_elpased: 1.79
batch start
#iterations: 196
currently lose_sum: 94.72248739004135
time_elpased: 1.875
batch start
#iterations: 197
currently lose_sum: 95.23935961723328
time_elpased: 1.835
batch start
#iterations: 198
currently lose_sum: 94.5730612874031
time_elpased: 1.838
batch start
#iterations: 199
currently lose_sum: 95.21809774637222
time_elpased: 1.896
start validation test
0.612371134021
0.622286541245
0.575280436349
0.597860962567
0.612436252461
64.150
batch start
#iterations: 200
currently lose_sum: 95.30751770734787
time_elpased: 1.811
batch start
#iterations: 201
currently lose_sum: 94.95873576402664
time_elpased: 1.877
batch start
#iterations: 202
currently lose_sum: 94.92500722408295
time_elpased: 1.833
batch start
#iterations: 203
currently lose_sum: 95.16070455312729
time_elpased: 1.853
batch start
#iterations: 204
currently lose_sum: 94.76280361413956
time_elpased: 1.89
batch start
#iterations: 205
currently lose_sum: 94.78542041778564
time_elpased: 1.884
batch start
#iterations: 206
currently lose_sum: 94.68979203701019
time_elpased: 1.848
batch start
#iterations: 207
currently lose_sum: 94.95400321483612
time_elpased: 1.85
batch start
#iterations: 208
currently lose_sum: 94.8235651254654
time_elpased: 1.88
batch start
#iterations: 209
currently lose_sum: 94.52736449241638
time_elpased: 1.874
batch start
#iterations: 210
currently lose_sum: 95.03308194875717
time_elpased: 1.844
batch start
#iterations: 211
currently lose_sum: 94.93307614326477
time_elpased: 1.859
batch start
#iterations: 212
currently lose_sum: 94.44382011890411
time_elpased: 1.852
batch start
#iterations: 213
currently lose_sum: 94.5574482679367
time_elpased: 1.838
batch start
#iterations: 214
currently lose_sum: 94.37104028463364
time_elpased: 1.834
batch start
#iterations: 215
currently lose_sum: 94.38491427898407
time_elpased: 1.873
batch start
#iterations: 216
currently lose_sum: 94.50426036119461
time_elpased: 1.829
batch start
#iterations: 217
currently lose_sum: 94.59209281206131
time_elpased: 1.876
batch start
#iterations: 218
currently lose_sum: 94.3264747262001
time_elpased: 1.811
batch start
#iterations: 219
currently lose_sum: 94.21854937076569
time_elpased: 1.875
start validation test
0.617628865979
0.640097501523
0.540496037872
0.586095301864
0.617764284556
63.927
batch start
#iterations: 220
currently lose_sum: 94.2538697719574
time_elpased: 1.844
batch start
#iterations: 221
currently lose_sum: 94.13867312669754
time_elpased: 1.824
batch start
#iterations: 222
currently lose_sum: 94.3728796839714
time_elpased: 1.859
batch start
#iterations: 223
currently lose_sum: 94.419608771801
time_elpased: 1.853
batch start
#iterations: 224
currently lose_sum: 94.45451647043228
time_elpased: 1.845
batch start
#iterations: 225
currently lose_sum: 93.94117850065231
time_elpased: 1.922
batch start
#iterations: 226
currently lose_sum: 94.05069577693939
time_elpased: 1.829
batch start
#iterations: 227
currently lose_sum: 93.99329668283463
time_elpased: 1.903
batch start
#iterations: 228
currently lose_sum: 94.31242418289185
time_elpased: 1.87
batch start
#iterations: 229
currently lose_sum: 94.69327461719513
time_elpased: 1.851
batch start
#iterations: 230
currently lose_sum: 94.47791147232056
time_elpased: 1.83
batch start
#iterations: 231
currently lose_sum: 93.9952821135521
time_elpased: 1.857
batch start
#iterations: 232
currently lose_sum: 94.22355192899704
time_elpased: 1.906
batch start
#iterations: 233
currently lose_sum: 93.90525883436203
time_elpased: 1.828
batch start
#iterations: 234
currently lose_sum: 94.1334239244461
time_elpased: 1.877
batch start
#iterations: 235
currently lose_sum: 94.19529402256012
time_elpased: 1.866
batch start
#iterations: 236
currently lose_sum: 93.89233952760696
time_elpased: 1.82
batch start
#iterations: 237
currently lose_sum: 94.24125409126282
time_elpased: 1.865
batch start
#iterations: 238
currently lose_sum: 93.64076107740402
time_elpased: 1.854
batch start
#iterations: 239
currently lose_sum: 93.73278158903122
time_elpased: 1.813
start validation test
0.603556701031
0.624020568071
0.524544612535
0.569974839251
0.603695418939
64.684
batch start
#iterations: 240
currently lose_sum: 94.1924786567688
time_elpased: 1.847
batch start
#iterations: 241
currently lose_sum: 93.78409177064896
time_elpased: 1.869
batch start
#iterations: 242
currently lose_sum: 93.55618089437485
time_elpased: 1.875
batch start
#iterations: 243
currently lose_sum: 94.12033933401108
time_elpased: 1.863
batch start
#iterations: 244
currently lose_sum: 93.5781381726265
time_elpased: 1.892
batch start
#iterations: 245
currently lose_sum: 93.57620733976364
time_elpased: 1.937
batch start
#iterations: 246
currently lose_sum: 93.86255007982254
time_elpased: 1.872
batch start
#iterations: 247
currently lose_sum: 93.45294106006622
time_elpased: 1.839
batch start
#iterations: 248
currently lose_sum: 93.56015205383301
time_elpased: 1.866
batch start
#iterations: 249
currently lose_sum: 93.4195157289505
time_elpased: 1.948
batch start
#iterations: 250
currently lose_sum: 93.33643454313278
time_elpased: 1.867
batch start
#iterations: 251
currently lose_sum: 93.18744707107544
time_elpased: 1.872
batch start
#iterations: 252
currently lose_sum: 93.70246535539627
time_elpased: 1.813
batch start
#iterations: 253
currently lose_sum: 93.23373413085938
time_elpased: 1.881
batch start
#iterations: 254
currently lose_sum: 93.65840137004852
time_elpased: 1.883
batch start
#iterations: 255
currently lose_sum: 93.26306116580963
time_elpased: 1.837
batch start
#iterations: 256
currently lose_sum: 92.88326036930084
time_elpased: 1.883
batch start
#iterations: 257
currently lose_sum: 93.49713289737701
time_elpased: 1.858
batch start
#iterations: 258
currently lose_sum: 92.76282757520676
time_elpased: 1.864
batch start
#iterations: 259
currently lose_sum: 93.0883606672287
time_elpased: 1.82
start validation test
0.603298969072
0.6239421072
0.523515488319
0.569334079463
0.603439041278
66.037
batch start
#iterations: 260
currently lose_sum: 92.72738015651703
time_elpased: 1.836
batch start
#iterations: 261
currently lose_sum: 92.8372153043747
time_elpased: 1.864
batch start
#iterations: 262
currently lose_sum: 92.67844784259796
time_elpased: 1.812
batch start
#iterations: 263
currently lose_sum: 92.5998073220253
time_elpased: 1.884
batch start
#iterations: 264
currently lose_sum: 93.29031497240067
time_elpased: 1.842
batch start
#iterations: 265
currently lose_sum: 92.60153698921204
time_elpased: 1.852
batch start
#iterations: 266
currently lose_sum: 92.98099029064178
time_elpased: 1.854
batch start
#iterations: 267
currently lose_sum: 92.78919208049774
time_elpased: 1.923
batch start
#iterations: 268
currently lose_sum: 92.49809819459915
time_elpased: 1.949
batch start
#iterations: 269
currently lose_sum: 92.69202464818954
time_elpased: 1.856
batch start
#iterations: 270
currently lose_sum: 92.57888436317444
time_elpased: 1.874
batch start
#iterations: 271
currently lose_sum: 92.64384573698044
time_elpased: 1.859
batch start
#iterations: 272
currently lose_sum: 92.29034036397934
time_elpased: 1.863
batch start
#iterations: 273
currently lose_sum: 92.5720277428627
time_elpased: 1.868
batch start
#iterations: 274
currently lose_sum: 92.51092374324799
time_elpased: 1.82
batch start
#iterations: 275
currently lose_sum: 92.17491698265076
time_elpased: 1.819
batch start
#iterations: 276
currently lose_sum: 92.27650713920593
time_elpased: 1.883
batch start
#iterations: 277
currently lose_sum: 92.42307251691818
time_elpased: 1.84
batch start
#iterations: 278
currently lose_sum: 92.45568364858627
time_elpased: 1.882
batch start
#iterations: 279
currently lose_sum: 91.76586711406708
time_elpased: 1.893
start validation test
0.607010309278
0.627048682773
0.531542657199
0.57535925142
0.607142804382
66.651
batch start
#iterations: 280
currently lose_sum: 91.89382994174957
time_elpased: 1.845
batch start
#iterations: 281
currently lose_sum: 92.22496086359024
time_elpased: 1.859
batch start
#iterations: 282
currently lose_sum: 92.03980851173401
time_elpased: 1.916
batch start
#iterations: 283
currently lose_sum: 92.2347229719162
time_elpased: 1.907
batch start
#iterations: 284
currently lose_sum: 91.93592631816864
time_elpased: 1.841
batch start
#iterations: 285
currently lose_sum: 91.80952334403992
time_elpased: 1.836
batch start
#iterations: 286
currently lose_sum: 92.28837370872498
time_elpased: 1.821
batch start
#iterations: 287
currently lose_sum: 91.59571886062622
time_elpased: 1.848
batch start
#iterations: 288
currently lose_sum: 91.91078674793243
time_elpased: 1.824
batch start
#iterations: 289
currently lose_sum: 91.64129358530045
time_elpased: 1.892
batch start
#iterations: 290
currently lose_sum: 91.5270984172821
time_elpased: 1.918
batch start
#iterations: 291
currently lose_sum: 91.16752910614014
time_elpased: 1.868
batch start
#iterations: 292
currently lose_sum: 91.4346432685852
time_elpased: 1.851
batch start
#iterations: 293
currently lose_sum: 91.59809011220932
time_elpased: 1.885
batch start
#iterations: 294
currently lose_sum: 91.07875472307205
time_elpased: 1.846
batch start
#iterations: 295
currently lose_sum: 91.47159314155579
time_elpased: 1.844
batch start
#iterations: 296
currently lose_sum: 91.2462529540062
time_elpased: 1.832
batch start
#iterations: 297
currently lose_sum: 91.30118662118912
time_elpased: 1.899
batch start
#iterations: 298
currently lose_sum: 91.5233548283577
time_elpased: 1.844
batch start
#iterations: 299
currently lose_sum: 90.85849720239639
time_elpased: 1.797
start validation test
0.595773195876
0.627118644068
0.475969949573
0.541188860286
0.595983528954
67.320
batch start
#iterations: 300
currently lose_sum: 91.06447690725327
time_elpased: 1.836
batch start
#iterations: 301
currently lose_sum: 91.44202810525894
time_elpased: 1.857
batch start
#iterations: 302
currently lose_sum: 90.96462750434875
time_elpased: 1.844
batch start
#iterations: 303
currently lose_sum: 90.7631961107254
time_elpased: 1.829
batch start
#iterations: 304
currently lose_sum: 91.36530297994614
time_elpased: 1.831
batch start
#iterations: 305
currently lose_sum: 91.0659830570221
time_elpased: 1.872
batch start
#iterations: 306
currently lose_sum: 91.05030727386475
time_elpased: 1.864
batch start
#iterations: 307
currently lose_sum: 91.05527538061142
time_elpased: 1.827
batch start
#iterations: 308
currently lose_sum: 91.12888634204865
time_elpased: 1.89
batch start
#iterations: 309
currently lose_sum: 90.84464752674103
time_elpased: 1.928
batch start
#iterations: 310
currently lose_sum: 90.29747074842453
time_elpased: 1.865
batch start
#iterations: 311
currently lose_sum: 90.3382374048233
time_elpased: 1.901
batch start
#iterations: 312
currently lose_sum: 90.50994890928268
time_elpased: 1.842
batch start
#iterations: 313
currently lose_sum: 90.1997344493866
time_elpased: 1.855
batch start
#iterations: 314
currently lose_sum: 89.95671606063843
time_elpased: 1.897
batch start
#iterations: 315
currently lose_sum: 89.7217149734497
time_elpased: 1.883
batch start
#iterations: 316
currently lose_sum: 90.4083724617958
time_elpased: 1.893
batch start
#iterations: 317
currently lose_sum: 90.26765620708466
time_elpased: 1.885
batch start
#iterations: 318
currently lose_sum: 89.92141848802567
time_elpased: 1.812
batch start
#iterations: 319
currently lose_sum: 89.5095756649971
time_elpased: 1.882
start validation test
0.599742268041
0.621362845063
0.514253370382
0.562756911988
0.599892356987
69.675
batch start
#iterations: 320
currently lose_sum: 90.08439874649048
time_elpased: 1.859
batch start
#iterations: 321
currently lose_sum: 89.96750539541245
time_elpased: 1.868
batch start
#iterations: 322
currently lose_sum: 89.46022588014603
time_elpased: 1.868
batch start
#iterations: 323
currently lose_sum: 90.08446782827377
time_elpased: 1.885
batch start
#iterations: 324
currently lose_sum: 89.80648869276047
time_elpased: 1.878
batch start
#iterations: 325
currently lose_sum: 89.87857556343079
time_elpased: 1.866
batch start
#iterations: 326
currently lose_sum: 89.76847863197327
time_elpased: 1.899
batch start
#iterations: 327
currently lose_sum: 88.94878816604614
time_elpased: 1.905
batch start
#iterations: 328
currently lose_sum: 89.28317207098007
time_elpased: 1.851
batch start
#iterations: 329
currently lose_sum: 89.16709613800049
time_elpased: 1.843
batch start
#iterations: 330
currently lose_sum: 89.79662322998047
time_elpased: 1.827
batch start
#iterations: 331
currently lose_sum: 89.60531729459763
time_elpased: 1.853
batch start
#iterations: 332
currently lose_sum: 89.03761696815491
time_elpased: 1.908
batch start
#iterations: 333
currently lose_sum: 89.00290584564209
time_elpased: 1.892
batch start
#iterations: 334
currently lose_sum: 89.1790611743927
time_elpased: 1.832
batch start
#iterations: 335
currently lose_sum: 88.8691897392273
time_elpased: 1.859
batch start
#iterations: 336
currently lose_sum: 88.95884770154953
time_elpased: 1.846
batch start
#iterations: 337
currently lose_sum: 89.03155761957169
time_elpased: 1.857
batch start
#iterations: 338
currently lose_sum: 88.31875884532928
time_elpased: 1.865
batch start
#iterations: 339
currently lose_sum: 88.71316301822662
time_elpased: 1.865
start validation test
0.592371134021
0.621588923242
0.475867037151
0.539053392399
0.592575674932
70.061
batch start
#iterations: 340
currently lose_sum: 89.0742130279541
time_elpased: 1.861
batch start
#iterations: 341
currently lose_sum: 88.82872271537781
time_elpased: 1.834
batch start
#iterations: 342
currently lose_sum: 89.26975345611572
time_elpased: 1.83
batch start
#iterations: 343
currently lose_sum: 89.08212274312973
time_elpased: 1.86
batch start
#iterations: 344
currently lose_sum: 87.80268108844757
time_elpased: 1.859
batch start
#iterations: 345
currently lose_sum: 88.9705822467804
time_elpased: 1.837
batch start
#iterations: 346
currently lose_sum: 88.43571573495865
time_elpased: 1.84
batch start
#iterations: 347
currently lose_sum: 87.80341339111328
time_elpased: 1.847
batch start
#iterations: 348
currently lose_sum: 88.40078240633011
time_elpased: 1.885
batch start
#iterations: 349
currently lose_sum: 87.78383553028107
time_elpased: 1.863
batch start
#iterations: 350
currently lose_sum: 87.78553819656372
time_elpased: 1.801
batch start
#iterations: 351
currently lose_sum: 87.55563133955002
time_elpased: 1.856
batch start
#iterations: 352
currently lose_sum: 87.48366570472717
time_elpased: 1.86
batch start
#iterations: 353
currently lose_sum: 87.6922914981842
time_elpased: 1.854
batch start
#iterations: 354
currently lose_sum: 87.09785383939743
time_elpased: 1.909
batch start
#iterations: 355
currently lose_sum: 87.61664980649948
time_elpased: 1.85
batch start
#iterations: 356
currently lose_sum: 87.62470835447311
time_elpased: 1.908
batch start
#iterations: 357
currently lose_sum: 87.3796694278717
time_elpased: 1.837
batch start
#iterations: 358
currently lose_sum: 86.8592044711113
time_elpased: 1.841
batch start
#iterations: 359
currently lose_sum: 87.20406687259674
time_elpased: 1.875
start validation test
0.594278350515
0.616924246263
0.501183492848
0.553063426268
0.594441792897
74.542
batch start
#iterations: 360
currently lose_sum: 87.13182425498962
time_elpased: 1.887
batch start
#iterations: 361
currently lose_sum: 87.3394445180893
time_elpased: 1.888
batch start
#iterations: 362
currently lose_sum: 87.21695804595947
time_elpased: 1.869
batch start
#iterations: 363
currently lose_sum: 86.50851833820343
time_elpased: 1.881
batch start
#iterations: 364
currently lose_sum: 86.81890988349915
time_elpased: 1.913
batch start
#iterations: 365
currently lose_sum: 86.86148208379745
time_elpased: 1.899
batch start
#iterations: 366
currently lose_sum: 87.13430297374725
time_elpased: 1.853
batch start
#iterations: 367
currently lose_sum: 86.66811174154282
time_elpased: 1.88
batch start
#iterations: 368
currently lose_sum: 86.95962679386139
time_elpased: 1.928
batch start
#iterations: 369
currently lose_sum: 86.37221336364746
time_elpased: 1.832
batch start
#iterations: 370
currently lose_sum: 86.48377376794815
time_elpased: 1.856
batch start
#iterations: 371
currently lose_sum: 86.18380057811737
time_elpased: 1.833
batch start
#iterations: 372
currently lose_sum: 85.49886059761047
time_elpased: 1.893
batch start
#iterations: 373
currently lose_sum: 85.94469451904297
time_elpased: 1.878
batch start
#iterations: 374
currently lose_sum: 86.19740557670593
time_elpased: 1.856
batch start
#iterations: 375
currently lose_sum: 86.3272989988327
time_elpased: 1.852
batch start
#iterations: 376
currently lose_sum: 85.7955008149147
time_elpased: 1.833
batch start
#iterations: 377
currently lose_sum: 86.47184079885483
time_elpased: 1.871
batch start
#iterations: 378
currently lose_sum: 85.59602200984955
time_elpased: 1.871
batch start
#iterations: 379
currently lose_sum: 86.55650722980499
time_elpased: 1.844
start validation test
0.583144329897
0.61408174692
0.451476793249
0.52037245715
0.583375492566
80.564
batch start
#iterations: 380
currently lose_sum: 85.8242729306221
time_elpased: 1.917
batch start
#iterations: 381
currently lose_sum: 85.46813839673996
time_elpased: 1.825
batch start
#iterations: 382
currently lose_sum: 85.46863454580307
time_elpased: 1.87
batch start
#iterations: 383
currently lose_sum: 85.33917313814163
time_elpased: 1.882
batch start
#iterations: 384
currently lose_sum: 85.27072358131409
time_elpased: 1.865
batch start
#iterations: 385
currently lose_sum: 85.71100777387619
time_elpased: 1.833
batch start
#iterations: 386
currently lose_sum: 85.22596603631973
time_elpased: 1.878
batch start
#iterations: 387
currently lose_sum: 85.06098562479019
time_elpased: 1.848
batch start
#iterations: 388
currently lose_sum: 84.7593931555748
time_elpased: 1.854
batch start
#iterations: 389
currently lose_sum: 85.12404161691666
time_elpased: 1.866
batch start
#iterations: 390
currently lose_sum: 84.89316046237946
time_elpased: 1.837
batch start
#iterations: 391
currently lose_sum: 84.57708859443665
time_elpased: 1.818
batch start
#iterations: 392
currently lose_sum: 84.94814240932465
time_elpased: 1.866
batch start
#iterations: 393
currently lose_sum: 84.26514428853989
time_elpased: 1.856
batch start
#iterations: 394
currently lose_sum: 84.05868023633957
time_elpased: 1.871
batch start
#iterations: 395
currently lose_sum: 84.55156695842743
time_elpased: 1.864
batch start
#iterations: 396
currently lose_sum: 84.79739564657211
time_elpased: 1.872
batch start
#iterations: 397
currently lose_sum: 83.80379974842072
time_elpased: 1.908
batch start
#iterations: 398
currently lose_sum: 84.38213938474655
time_elpased: 1.849
batch start
#iterations: 399
currently lose_sum: 84.14764553308487
time_elpased: 1.936
start validation test
0.580206185567
0.604518272425
0.468148605537
0.52766500406
0.580402919932
82.229
acc: 0.623
pre: 0.635
rec: 0.580
F1: 0.606
auc: 0.623
