start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.07016742229462
time_elpased: 1.421
batch start
#iterations: 1
currently lose_sum: 99.66347175836563
time_elpased: 1.403
batch start
#iterations: 2
currently lose_sum: 99.58876997232437
time_elpased: 1.382
batch start
#iterations: 3
currently lose_sum: 99.16813230514526
time_elpased: 1.408
batch start
#iterations: 4
currently lose_sum: 99.33507186174393
time_elpased: 1.421
batch start
#iterations: 5
currently lose_sum: 99.17027670145035
time_elpased: 1.386
batch start
#iterations: 6
currently lose_sum: 99.19382029771805
time_elpased: 1.437
batch start
#iterations: 7
currently lose_sum: 98.76846212148666
time_elpased: 1.429
batch start
#iterations: 8
currently lose_sum: 98.69401925802231
time_elpased: 1.459
batch start
#iterations: 9
currently lose_sum: 98.27638620138168
time_elpased: 1.41
batch start
#iterations: 10
currently lose_sum: 97.84219002723694
time_elpased: 1.453
batch start
#iterations: 11
currently lose_sum: 97.60684525966644
time_elpased: 1.419
batch start
#iterations: 12
currently lose_sum: 97.3939641714096
time_elpased: 1.441
batch start
#iterations: 13
currently lose_sum: 97.3484473824501
time_elpased: 1.429
batch start
#iterations: 14
currently lose_sum: 96.8689649105072
time_elpased: 1.4
batch start
#iterations: 15
currently lose_sum: 96.57541918754578
time_elpased: 1.451
batch start
#iterations: 16
currently lose_sum: 96.56972849369049
time_elpased: 1.45
batch start
#iterations: 17
currently lose_sum: 96.35141015052795
time_elpased: 1.435
batch start
#iterations: 18
currently lose_sum: 96.17558950185776
time_elpased: 1.4
batch start
#iterations: 19
currently lose_sum: 95.76839572191238
time_elpased: 1.519
start validation test
0.647577319588
0.640589901358
0.67507204611
0.657379102982
0.64753189253
61.623
batch start
#iterations: 20
currently lose_sum: 95.75180679559708
time_elpased: 1.425
batch start
#iterations: 21
currently lose_sum: 95.72131568193436
time_elpased: 1.396
batch start
#iterations: 22
currently lose_sum: 95.54217809438705
time_elpased: 1.418
batch start
#iterations: 23
currently lose_sum: 95.33959585428238
time_elpased: 1.411
batch start
#iterations: 24
currently lose_sum: 95.0585303902626
time_elpased: 1.403
batch start
#iterations: 25
currently lose_sum: 95.03311455249786
time_elpased: 1.437
batch start
#iterations: 26
currently lose_sum: 95.12657576799393
time_elpased: 1.409
batch start
#iterations: 27
currently lose_sum: 94.84983289241791
time_elpased: 1.416
batch start
#iterations: 28
currently lose_sum: 94.6139584183693
time_elpased: 1.42
batch start
#iterations: 29
currently lose_sum: 94.91125464439392
time_elpased: 1.431
batch start
#iterations: 30
currently lose_sum: 94.714115858078
time_elpased: 1.407
batch start
#iterations: 31
currently lose_sum: 94.06509357690811
time_elpased: 1.459
batch start
#iterations: 32
currently lose_sum: 93.80626034736633
time_elpased: 1.406
batch start
#iterations: 33
currently lose_sum: 94.07469654083252
time_elpased: 1.419
batch start
#iterations: 34
currently lose_sum: 94.10081726312637
time_elpased: 1.405
batch start
#iterations: 35
currently lose_sum: 93.93567258119583
time_elpased: 1.433
batch start
#iterations: 36
currently lose_sum: 93.96729093790054
time_elpased: 1.487
batch start
#iterations: 37
currently lose_sum: 93.58796620368958
time_elpased: 1.415
batch start
#iterations: 38
currently lose_sum: 93.41007906198502
time_elpased: 1.385
batch start
#iterations: 39
currently lose_sum: 93.64385557174683
time_elpased: 1.435
start validation test
0.642010309278
0.652202570581
0.611053931659
0.630958074287
0.64206145571
60.886
batch start
#iterations: 40
currently lose_sum: 93.5204508304596
time_elpased: 1.416
batch start
#iterations: 41
currently lose_sum: 93.5432665348053
time_elpased: 1.402
batch start
#iterations: 42
currently lose_sum: 93.62275975942612
time_elpased: 1.423
batch start
#iterations: 43
currently lose_sum: 93.16340744495392
time_elpased: 1.469
batch start
#iterations: 44
currently lose_sum: 93.25184506177902
time_elpased: 1.423
batch start
#iterations: 45
currently lose_sum: 93.3388015627861
time_elpased: 1.416
batch start
#iterations: 46
currently lose_sum: 92.66017812490463
time_elpased: 1.411
batch start
#iterations: 47
currently lose_sum: 92.71538627147675
time_elpased: 1.413
batch start
#iterations: 48
currently lose_sum: 93.06411969661713
time_elpased: 1.41
batch start
#iterations: 49
currently lose_sum: 92.63578963279724
time_elpased: 1.412
batch start
#iterations: 50
currently lose_sum: 93.00182718038559
time_elpased: 1.414
batch start
#iterations: 51
currently lose_sum: 92.41283571720123
time_elpased: 1.381
batch start
#iterations: 52
currently lose_sum: 92.3865527510643
time_elpased: 1.396
batch start
#iterations: 53
currently lose_sum: 92.5338779091835
time_elpased: 1.418
batch start
#iterations: 54
currently lose_sum: 92.63267570734024
time_elpased: 1.448
batch start
#iterations: 55
currently lose_sum: 92.59611701965332
time_elpased: 1.431
batch start
#iterations: 56
currently lose_sum: 92.22835546731949
time_elpased: 1.443
batch start
#iterations: 57
currently lose_sum: 92.15500575304031
time_elpased: 1.47
batch start
#iterations: 58
currently lose_sum: 92.30395203828812
time_elpased: 1.454
batch start
#iterations: 59
currently lose_sum: 92.56600111722946
time_elpased: 1.444
start validation test
0.659278350515
0.643956247683
0.715006175381
0.677623878268
0.659186276455
59.799
batch start
#iterations: 60
currently lose_sum: 91.83819508552551
time_elpased: 1.391
batch start
#iterations: 61
currently lose_sum: 92.53124994039536
time_elpased: 1.409
batch start
#iterations: 62
currently lose_sum: 91.91330802440643
time_elpased: 1.396
batch start
#iterations: 63
currently lose_sum: 92.18138599395752
time_elpased: 1.412
batch start
#iterations: 64
currently lose_sum: 92.12866085767746
time_elpased: 1.421
batch start
#iterations: 65
currently lose_sum: 91.83408921957016
time_elpased: 1.434
batch start
#iterations: 66
currently lose_sum: 92.09490078687668
time_elpased: 1.423
batch start
#iterations: 67
currently lose_sum: 92.20421016216278
time_elpased: 1.457
batch start
#iterations: 68
currently lose_sum: 91.658154129982
time_elpased: 1.398
batch start
#iterations: 69
currently lose_sum: 91.35000365972519
time_elpased: 1.444
batch start
#iterations: 70
currently lose_sum: 91.83264070749283
time_elpased: 1.382
batch start
#iterations: 71
currently lose_sum: 91.42042803764343
time_elpased: 1.431
batch start
#iterations: 72
currently lose_sum: 91.4270122051239
time_elpased: 1.404
batch start
#iterations: 73
currently lose_sum: 91.20698142051697
time_elpased: 1.392
batch start
#iterations: 74
currently lose_sum: 91.53631842136383
time_elpased: 1.413
batch start
#iterations: 75
currently lose_sum: 91.59525364637375
time_elpased: 1.417
batch start
#iterations: 76
currently lose_sum: 91.45238780975342
time_elpased: 1.412
batch start
#iterations: 77
currently lose_sum: 91.3763125538826
time_elpased: 1.414
batch start
#iterations: 78
currently lose_sum: 91.5807718038559
time_elpased: 1.406
batch start
#iterations: 79
currently lose_sum: 91.6792322397232
time_elpased: 1.404
start validation test
0.667886597938
0.66525295365
0.678056813503
0.671593863092
0.667869794608
59.001
batch start
#iterations: 80
currently lose_sum: 91.17570495605469
time_elpased: 1.425
batch start
#iterations: 81
currently lose_sum: 91.31749790906906
time_elpased: 1.419
batch start
#iterations: 82
currently lose_sum: 91.13274377584457
time_elpased: 1.398
batch start
#iterations: 83
currently lose_sum: 91.62307441234589
time_elpased: 1.471
batch start
#iterations: 84
currently lose_sum: 90.78335303068161
time_elpased: 1.407
batch start
#iterations: 85
currently lose_sum: 90.7410124540329
time_elpased: 1.427
batch start
#iterations: 86
currently lose_sum: 91.10697209835052
time_elpased: 1.401
batch start
#iterations: 87
currently lose_sum: 90.60924595594406
time_elpased: 1.4
batch start
#iterations: 88
currently lose_sum: 90.782251060009
time_elpased: 1.419
batch start
#iterations: 89
currently lose_sum: 90.70780998468399
time_elpased: 1.403
batch start
#iterations: 90
currently lose_sum: 91.4352000951767
time_elpased: 1.45
batch start
#iterations: 91
currently lose_sum: 90.74749755859375
time_elpased: 1.419
batch start
#iterations: 92
currently lose_sum: 90.82034629583359
time_elpased: 1.41
batch start
#iterations: 93
currently lose_sum: 90.39240425825119
time_elpased: 1.396
batch start
#iterations: 94
currently lose_sum: 90.85968244075775
time_elpased: 1.435
batch start
#iterations: 95
currently lose_sum: 90.8448680639267
time_elpased: 1.405
batch start
#iterations: 96
currently lose_sum: 90.11050462722778
time_elpased: 1.399
batch start
#iterations: 97
currently lose_sum: 90.44837486743927
time_elpased: 1.426
batch start
#iterations: 98
currently lose_sum: 90.19266355037689
time_elpased: 1.442
batch start
#iterations: 99
currently lose_sum: 90.20527654886246
time_elpased: 1.424
start validation test
0.664948453608
0.66015936255
0.682173734047
0.670986029561
0.664919993831
59.009
batch start
#iterations: 100
currently lose_sum: 90.5747742652893
time_elpased: 1.413
batch start
#iterations: 101
currently lose_sum: 90.81971222162247
time_elpased: 1.432
batch start
#iterations: 102
currently lose_sum: 90.13289153575897
time_elpased: 1.4
batch start
#iterations: 103
currently lose_sum: 90.88837200403214
time_elpased: 1.421
batch start
#iterations: 104
currently lose_sum: 90.25810080766678
time_elpased: 1.425
batch start
#iterations: 105
currently lose_sum: 90.14176505804062
time_elpased: 1.432
batch start
#iterations: 106
currently lose_sum: 90.18896389007568
time_elpased: 1.449
batch start
#iterations: 107
currently lose_sum: 90.35116863250732
time_elpased: 1.446
batch start
#iterations: 108
currently lose_sum: 90.04077994823456
time_elpased: 1.454
batch start
#iterations: 109
currently lose_sum: 90.22884607315063
time_elpased: 1.442
batch start
#iterations: 110
currently lose_sum: 90.63694435358047
time_elpased: 1.409
batch start
#iterations: 111
currently lose_sum: 90.15420979261398
time_elpased: 1.417
batch start
#iterations: 112
currently lose_sum: 90.00505554676056
time_elpased: 1.453
batch start
#iterations: 113
currently lose_sum: 89.68559801578522
time_elpased: 1.433
batch start
#iterations: 114
currently lose_sum: 90.33230519294739
time_elpased: 1.438
batch start
#iterations: 115
currently lose_sum: 89.94701534509659
time_elpased: 1.396
batch start
#iterations: 116
currently lose_sum: 90.24506360292435
time_elpased: 1.418
batch start
#iterations: 117
currently lose_sum: 89.4877958893776
time_elpased: 1.483
batch start
#iterations: 118
currently lose_sum: 90.42612189054489
time_elpased: 1.45
batch start
#iterations: 119
currently lose_sum: 90.01379734277725
time_elpased: 1.388
start validation test
0.665773195876
0.665336607326
0.669308357349
0.667316572601
0.665767355048
58.564
batch start
#iterations: 120
currently lose_sum: 89.54739302396774
time_elpased: 1.429
batch start
#iterations: 121
currently lose_sum: 89.42304229736328
time_elpased: 1.429
batch start
#iterations: 122
currently lose_sum: 89.75231897830963
time_elpased: 1.424
batch start
#iterations: 123
currently lose_sum: 89.53241032361984
time_elpased: 1.448
batch start
#iterations: 124
currently lose_sum: 90.07377862930298
time_elpased: 1.404
batch start
#iterations: 125
currently lose_sum: 89.8377513885498
time_elpased: 1.414
batch start
#iterations: 126
currently lose_sum: 89.02769333124161
time_elpased: 1.417
batch start
#iterations: 127
currently lose_sum: 89.20251363515854
time_elpased: 1.46
batch start
#iterations: 128
currently lose_sum: 89.4310422539711
time_elpased: 1.442
batch start
#iterations: 129
currently lose_sum: 89.08121228218079
time_elpased: 1.415
batch start
#iterations: 130
currently lose_sum: 89.61033153533936
time_elpased: 1.427
batch start
#iterations: 131
currently lose_sum: 89.43324679136276
time_elpased: 1.418
batch start
#iterations: 132
currently lose_sum: 89.52850675582886
time_elpased: 1.432
batch start
#iterations: 133
currently lose_sum: 89.05994659662247
time_elpased: 1.402
batch start
#iterations: 134
currently lose_sum: 89.33089172840118
time_elpased: 1.428
batch start
#iterations: 135
currently lose_sum: 89.62695509195328
time_elpased: 1.4
batch start
#iterations: 136
currently lose_sum: 89.24187338352203
time_elpased: 1.429
batch start
#iterations: 137
currently lose_sum: 88.98783695697784
time_elpased: 1.392
batch start
#iterations: 138
currently lose_sum: 89.13514417409897
time_elpased: 1.406
batch start
#iterations: 139
currently lose_sum: 88.88499891757965
time_elpased: 1.413
start validation test
0.683453608247
0.669704737492
0.726018937834
0.696725764235
0.683383281391
57.651
batch start
#iterations: 140
currently lose_sum: 89.06428974866867
time_elpased: 1.425
batch start
#iterations: 141
currently lose_sum: 88.69605141878128
time_elpased: 1.419
batch start
#iterations: 142
currently lose_sum: 88.98380559682846
time_elpased: 1.404
batch start
#iterations: 143
currently lose_sum: 88.91476112604141
time_elpased: 1.401
batch start
#iterations: 144
currently lose_sum: 88.94697391986847
time_elpased: 1.448
batch start
#iterations: 145
currently lose_sum: 88.45985823869705
time_elpased: 1.457
batch start
#iterations: 146
currently lose_sum: 88.99395102262497
time_elpased: 1.422
batch start
#iterations: 147
currently lose_sum: 88.7302171587944
time_elpased: 1.421
batch start
#iterations: 148
currently lose_sum: 88.97385323047638
time_elpased: 1.421
batch start
#iterations: 149
currently lose_sum: 88.67220717668533
time_elpased: 1.428
batch start
#iterations: 150
currently lose_sum: 88.96674883365631
time_elpased: 1.437
batch start
#iterations: 151
currently lose_sum: 89.29248958826065
time_elpased: 1.399
batch start
#iterations: 152
currently lose_sum: 88.82324928045273
time_elpased: 1.417
batch start
#iterations: 153
currently lose_sum: 88.54969048500061
time_elpased: 1.412
batch start
#iterations: 154
currently lose_sum: 89.17953199148178
time_elpased: 1.444
batch start
#iterations: 155
currently lose_sum: 88.62829852104187
time_elpased: 1.402
batch start
#iterations: 156
currently lose_sum: 88.46453142166138
time_elpased: 1.41
batch start
#iterations: 157
currently lose_sum: 88.16820621490479
time_elpased: 1.422
batch start
#iterations: 158
currently lose_sum: 88.9524165391922
time_elpased: 1.402
batch start
#iterations: 159
currently lose_sum: 88.40737855434418
time_elpased: 1.421
start validation test
0.67206185567
0.663865546218
0.699258954302
0.681102756892
0.672016920356
57.975
batch start
#iterations: 160
currently lose_sum: 88.53871494531631
time_elpased: 1.506
batch start
#iterations: 161
currently lose_sum: 88.85667324066162
time_elpased: 1.43
batch start
#iterations: 162
currently lose_sum: 88.68830817937851
time_elpased: 1.411
batch start
#iterations: 163
currently lose_sum: 88.60326206684113
time_elpased: 1.419
batch start
#iterations: 164
currently lose_sum: 88.6038966178894
time_elpased: 1.404
batch start
#iterations: 165
currently lose_sum: 88.40585315227509
time_elpased: 1.406
batch start
#iterations: 166
currently lose_sum: 88.4620965719223
time_elpased: 1.417
batch start
#iterations: 167
currently lose_sum: 88.11312365531921
time_elpased: 1.437
batch start
#iterations: 168
currently lose_sum: 88.34821903705597
time_elpased: 1.437
batch start
#iterations: 169
currently lose_sum: 87.93351513147354
time_elpased: 1.439
batch start
#iterations: 170
currently lose_sum: 88.42969465255737
time_elpased: 1.397
batch start
#iterations: 171
currently lose_sum: 88.21891784667969
time_elpased: 1.402
batch start
#iterations: 172
currently lose_sum: 88.24894213676453
time_elpased: 1.423
batch start
#iterations: 173
currently lose_sum: 87.95298558473587
time_elpased: 1.383
batch start
#iterations: 174
currently lose_sum: 87.86682450771332
time_elpased: 1.436
batch start
#iterations: 175
currently lose_sum: 87.92971724271774
time_elpased: 1.415
batch start
#iterations: 176
currently lose_sum: 88.03353852033615
time_elpased: 1.415
batch start
#iterations: 177
currently lose_sum: 88.24407458305359
time_elpased: 1.429
batch start
#iterations: 178
currently lose_sum: 87.92480832338333
time_elpased: 1.457
batch start
#iterations: 179
currently lose_sum: 87.40477275848389
time_elpased: 1.4
start validation test
0.666958762887
0.669266770671
0.662309592425
0.665770006725
0.666966444292
58.084
batch start
#iterations: 180
currently lose_sum: 87.66404694318771
time_elpased: 1.411
batch start
#iterations: 181
currently lose_sum: 88.12449580430984
time_elpased: 1.415
batch start
#iterations: 182
currently lose_sum: 88.7053993344307
time_elpased: 1.423
batch start
#iterations: 183
currently lose_sum: 88.0340626835823
time_elpased: 1.412
batch start
#iterations: 184
currently lose_sum: 87.9091368317604
time_elpased: 1.415
batch start
#iterations: 185
currently lose_sum: 88.20770245790482
time_elpased: 1.398
batch start
#iterations: 186
currently lose_sum: 87.21514254808426
time_elpased: 1.415
batch start
#iterations: 187
currently lose_sum: 87.3907425403595
time_elpased: 1.43
batch start
#iterations: 188
currently lose_sum: 87.7321829199791
time_elpased: 1.426
batch start
#iterations: 189
currently lose_sum: 87.65470278263092
time_elpased: 1.402
batch start
#iterations: 190
currently lose_sum: 87.3953537940979
time_elpased: 1.436
batch start
#iterations: 191
currently lose_sum: 87.33741480112076
time_elpased: 1.384
batch start
#iterations: 192
currently lose_sum: 87.89525359869003
time_elpased: 1.401
batch start
#iterations: 193
currently lose_sum: 87.8477224111557
time_elpased: 1.385
batch start
#iterations: 194
currently lose_sum: 87.78243345022202
time_elpased: 1.426
batch start
#iterations: 195
currently lose_sum: 87.60094892978668
time_elpased: 1.417
batch start
#iterations: 196
currently lose_sum: 86.96475261449814
time_elpased: 1.469
batch start
#iterations: 197
currently lose_sum: 87.47531932592392
time_elpased: 1.407
batch start
#iterations: 198
currently lose_sum: 86.99314206838608
time_elpased: 1.395
batch start
#iterations: 199
currently lose_sum: 86.84029823541641
time_elpased: 1.419
start validation test
0.655618556701
0.659451507828
0.645944833265
0.652628295118
0.655634539722
58.655
batch start
#iterations: 200
currently lose_sum: 87.05673491954803
time_elpased: 1.461
batch start
#iterations: 201
currently lose_sum: 86.97726559638977
time_elpased: 1.411
batch start
#iterations: 202
currently lose_sum: 87.26809567213058
time_elpased: 1.409
batch start
#iterations: 203
currently lose_sum: 87.4204889535904
time_elpased: 1.392
batch start
#iterations: 204
currently lose_sum: 87.16277527809143
time_elpased: 1.418
batch start
#iterations: 205
currently lose_sum: 87.67785263061523
time_elpased: 1.433
batch start
#iterations: 206
currently lose_sum: 87.18446183204651
time_elpased: 1.418
batch start
#iterations: 207
currently lose_sum: 87.10784608125687
time_elpased: 1.413
batch start
#iterations: 208
currently lose_sum: 86.84928774833679
time_elpased: 1.419
batch start
#iterations: 209
currently lose_sum: 87.37859040498734
time_elpased: 1.441
batch start
#iterations: 210
currently lose_sum: 86.94022357463837
time_elpased: 1.405
batch start
#iterations: 211
currently lose_sum: 86.42932033538818
time_elpased: 1.383
batch start
#iterations: 212
currently lose_sum: 87.28817391395569
time_elpased: 1.393
batch start
#iterations: 213
currently lose_sum: 87.43929523229599
time_elpased: 1.417
batch start
#iterations: 214
currently lose_sum: 86.98823773860931
time_elpased: 1.427
batch start
#iterations: 215
currently lose_sum: 86.92955332994461
time_elpased: 1.431
batch start
#iterations: 216
currently lose_sum: 86.62031477689743
time_elpased: 1.429
batch start
#iterations: 217
currently lose_sum: 86.7883877158165
time_elpased: 1.411
batch start
#iterations: 218
currently lose_sum: 87.2319478392601
time_elpased: 1.459
batch start
#iterations: 219
currently lose_sum: 86.88759279251099
time_elpased: 1.384
start validation test
0.661649484536
0.67852288174
0.616508851379
0.64603106126
0.661724066334
58.357
batch start
#iterations: 220
currently lose_sum: 86.98673337697983
time_elpased: 1.401
batch start
#iterations: 221
currently lose_sum: 86.67852455377579
time_elpased: 1.431
batch start
#iterations: 222
currently lose_sum: 86.66496455669403
time_elpased: 1.426
batch start
#iterations: 223
currently lose_sum: 85.7096186876297
time_elpased: 1.404
batch start
#iterations: 224
currently lose_sum: 86.62422913312912
time_elpased: 1.421
batch start
#iterations: 225
currently lose_sum: 86.81372874975204
time_elpased: 1.41
batch start
#iterations: 226
currently lose_sum: 86.1530933380127
time_elpased: 1.405
batch start
#iterations: 227
currently lose_sum: 86.53399753570557
time_elpased: 1.419
batch start
#iterations: 228
currently lose_sum: 86.2437858581543
time_elpased: 1.429
batch start
#iterations: 229
currently lose_sum: 85.98555153608322
time_elpased: 1.429
batch start
#iterations: 230
currently lose_sum: 86.35768926143646
time_elpased: 1.415
batch start
#iterations: 231
currently lose_sum: 86.68019729852676
time_elpased: 1.423
batch start
#iterations: 232
currently lose_sum: 86.78628599643707
time_elpased: 1.399
batch start
#iterations: 233
currently lose_sum: 86.95606428384781
time_elpased: 1.432
batch start
#iterations: 234
currently lose_sum: 86.86586582660675
time_elpased: 1.458
batch start
#iterations: 235
currently lose_sum: 87.12745726108551
time_elpased: 1.47
batch start
#iterations: 236
currently lose_sum: 86.55340623855591
time_elpased: 1.41
batch start
#iterations: 237
currently lose_sum: 86.04464173316956
time_elpased: 1.444
batch start
#iterations: 238
currently lose_sum: 86.52526044845581
time_elpased: 1.476
batch start
#iterations: 239
currently lose_sum: 86.96569699048996
time_elpased: 1.418
start validation test
0.659175257732
0.659244818387
0.661280362289
0.660261021478
0.659171779658
58.517
batch start
#iterations: 240
currently lose_sum: 86.25191140174866
time_elpased: 1.482
batch start
#iterations: 241
currently lose_sum: 85.77305275201797
time_elpased: 1.414
batch start
#iterations: 242
currently lose_sum: 86.2418851852417
time_elpased: 1.398
batch start
#iterations: 243
currently lose_sum: 86.01805472373962
time_elpased: 1.469
batch start
#iterations: 244
currently lose_sum: 86.30570602416992
time_elpased: 1.441
batch start
#iterations: 245
currently lose_sum: 86.5184218287468
time_elpased: 1.428
batch start
#iterations: 246
currently lose_sum: 86.24798905849457
time_elpased: 1.426
batch start
#iterations: 247
currently lose_sum: 86.09179306030273
time_elpased: 1.426
batch start
#iterations: 248
currently lose_sum: 86.33756196498871
time_elpased: 1.432
batch start
#iterations: 249
currently lose_sum: 86.20785117149353
time_elpased: 1.441
batch start
#iterations: 250
currently lose_sum: 85.98422104120255
time_elpased: 1.43
batch start
#iterations: 251
currently lose_sum: 86.03408813476562
time_elpased: 1.389
batch start
#iterations: 252
currently lose_sum: 86.42516523599625
time_elpased: 1.428
batch start
#iterations: 253
currently lose_sum: 86.43457156419754
time_elpased: 1.416
batch start
#iterations: 254
currently lose_sum: 85.37073040008545
time_elpased: 1.401
batch start
#iterations: 255
currently lose_sum: 85.64997917413712
time_elpased: 1.407
batch start
#iterations: 256
currently lose_sum: 86.09285259246826
time_elpased: 1.426
batch start
#iterations: 257
currently lose_sum: 86.12558943033218
time_elpased: 1.476
batch start
#iterations: 258
currently lose_sum: 86.29790163040161
time_elpased: 1.42
batch start
#iterations: 259
currently lose_sum: 85.31147563457489
time_elpased: 1.405
start validation test
0.655
0.666850645767
0.621757925072
0.643515312916
0.655054922883
58.724
batch start
#iterations: 260
currently lose_sum: 86.38687777519226
time_elpased: 1.429
batch start
#iterations: 261
currently lose_sum: 86.16783845424652
time_elpased: 1.406
batch start
#iterations: 262
currently lose_sum: 85.29119169712067
time_elpased: 1.406
batch start
#iterations: 263
currently lose_sum: 85.83614468574524
time_elpased: 1.423
batch start
#iterations: 264
currently lose_sum: 86.32235217094421
time_elpased: 1.439
batch start
#iterations: 265
currently lose_sum: 85.9292596578598
time_elpased: 1.4
batch start
#iterations: 266
currently lose_sum: 85.48932814598083
time_elpased: 1.401
batch start
#iterations: 267
currently lose_sum: 85.63332015275955
time_elpased: 1.396
batch start
#iterations: 268
currently lose_sum: 85.66365575790405
time_elpased: 1.425
batch start
#iterations: 269
currently lose_sum: 85.96693462133408
time_elpased: 1.43
batch start
#iterations: 270
currently lose_sum: 85.2898320555687
time_elpased: 1.438
batch start
#iterations: 271
currently lose_sum: 86.14439398050308
time_elpased: 1.406
batch start
#iterations: 272
currently lose_sum: 85.77466893196106
time_elpased: 1.425
batch start
#iterations: 273
currently lose_sum: 85.83222359418869
time_elpased: 1.402
batch start
#iterations: 274
currently lose_sum: 85.41479635238647
time_elpased: 1.426
batch start
#iterations: 275
currently lose_sum: 85.26783043146133
time_elpased: 1.412
batch start
#iterations: 276
currently lose_sum: 85.64875596761703
time_elpased: 1.399
batch start
#iterations: 277
currently lose_sum: 85.66093254089355
time_elpased: 1.421
batch start
#iterations: 278
currently lose_sum: 85.50189930200577
time_elpased: 1.416
batch start
#iterations: 279
currently lose_sum: 85.64417290687561
time_elpased: 1.406
start validation test
0.662164948454
0.675121843155
0.627315767806
0.650341442595
0.662222526612
58.465
batch start
#iterations: 280
currently lose_sum: 85.19144707918167
time_elpased: 1.412
batch start
#iterations: 281
currently lose_sum: 85.34362030029297
time_elpased: 1.458
batch start
#iterations: 282
currently lose_sum: 85.00963938236237
time_elpased: 1.412
batch start
#iterations: 283
currently lose_sum: 85.9205904006958
time_elpased: 1.406
batch start
#iterations: 284
currently lose_sum: 85.7540711760521
time_elpased: 1.397
batch start
#iterations: 285
currently lose_sum: 85.2443578839302
time_elpased: 1.416
batch start
#iterations: 286
currently lose_sum: 84.93859934806824
time_elpased: 1.405
batch start
#iterations: 287
currently lose_sum: 85.4808354973793
time_elpased: 1.469
batch start
#iterations: 288
currently lose_sum: 85.47661924362183
time_elpased: 1.43
batch start
#iterations: 289
currently lose_sum: 85.5678169131279
time_elpased: 1.436
batch start
#iterations: 290
currently lose_sum: 85.48438096046448
time_elpased: 1.417
batch start
#iterations: 291
currently lose_sum: 85.33415198326111
time_elpased: 1.436
batch start
#iterations: 292
currently lose_sum: 85.68357956409454
time_elpased: 1.406
batch start
#iterations: 293
currently lose_sum: 84.89437240362167
time_elpased: 1.461
batch start
#iterations: 294
currently lose_sum: 84.8558920621872
time_elpased: 1.431
batch start
#iterations: 295
currently lose_sum: 84.7819316983223
time_elpased: 1.481
batch start
#iterations: 296
currently lose_sum: 85.27382355928421
time_elpased: 1.421
batch start
#iterations: 297
currently lose_sum: 85.38208317756653
time_elpased: 1.413
batch start
#iterations: 298
currently lose_sum: 84.9689462184906
time_elpased: 1.404
batch start
#iterations: 299
currently lose_sum: 85.05602025985718
time_elpased: 1.399
start validation test
0.663453608247
0.681430035295
0.615994236311
0.647062003352
0.663532021088
58.643
batch start
#iterations: 300
currently lose_sum: 85.22563403844833
time_elpased: 1.424
batch start
#iterations: 301
currently lose_sum: 85.00814509391785
time_elpased: 1.423
batch start
#iterations: 302
currently lose_sum: 84.65218549966812
time_elpased: 1.424
batch start
#iterations: 303
currently lose_sum: 84.50293236970901
time_elpased: 1.414
batch start
#iterations: 304
currently lose_sum: 84.70667237043381
time_elpased: 1.46
batch start
#iterations: 305
currently lose_sum: 84.50749671459198
time_elpased: 1.453
batch start
#iterations: 306
currently lose_sum: 85.01767414808273
time_elpased: 1.413
batch start
#iterations: 307
currently lose_sum: 85.49505043029785
time_elpased: 1.415
batch start
#iterations: 308
currently lose_sum: 85.22469460964203
time_elpased: 1.405
batch start
#iterations: 309
currently lose_sum: 84.36072874069214
time_elpased: 1.397
batch start
#iterations: 310
currently lose_sum: 84.5423972606659
time_elpased: 1.403
batch start
#iterations: 311
currently lose_sum: 85.1005049943924
time_elpased: 1.432
batch start
#iterations: 312
currently lose_sum: 84.57023900747299
time_elpased: 1.416
batch start
#iterations: 313
currently lose_sum: 84.35417699813843
time_elpased: 1.435
batch start
#iterations: 314
currently lose_sum: 84.8688195347786
time_elpased: 1.403
batch start
#iterations: 315
currently lose_sum: 84.34646511077881
time_elpased: 1.398
batch start
#iterations: 316
currently lose_sum: 84.37958061695099
time_elpased: 1.412
batch start
#iterations: 317
currently lose_sum: 84.47706007957458
time_elpased: 1.414
batch start
#iterations: 318
currently lose_sum: 84.5079950094223
time_elpased: 1.408
batch start
#iterations: 319
currently lose_sum: 84.36279547214508
time_elpased: 1.406
start validation test
0.662783505155
0.678314606742
0.621346233018
0.648581865062
0.662851968223
58.640
batch start
#iterations: 320
currently lose_sum: 85.02299964427948
time_elpased: 1.419
batch start
#iterations: 321
currently lose_sum: 84.80525451898575
time_elpased: 1.42
batch start
#iterations: 322
currently lose_sum: 84.86139899492264
time_elpased: 1.444
batch start
#iterations: 323
currently lose_sum: 84.13817149400711
time_elpased: 1.387
batch start
#iterations: 324
currently lose_sum: 84.72703635692596
time_elpased: 1.427
batch start
#iterations: 325
currently lose_sum: 84.42691111564636
time_elpased: 1.421
batch start
#iterations: 326
currently lose_sum: 84.03037869930267
time_elpased: 1.423
batch start
#iterations: 327
currently lose_sum: 84.66590183973312
time_elpased: 1.402
batch start
#iterations: 328
currently lose_sum: 84.41847670078278
time_elpased: 1.474
batch start
#iterations: 329
currently lose_sum: 84.70117169618607
time_elpased: 1.411
batch start
#iterations: 330
currently lose_sum: 84.32965314388275
time_elpased: 1.399
batch start
#iterations: 331
currently lose_sum: 84.18110245466232
time_elpased: 1.396
batch start
#iterations: 332
currently lose_sum: 84.4723769724369
time_elpased: 1.433
batch start
#iterations: 333
currently lose_sum: 84.350840985775
time_elpased: 1.444
batch start
#iterations: 334
currently lose_sum: 84.24605840444565
time_elpased: 1.414
batch start
#iterations: 335
currently lose_sum: 83.93202698230743
time_elpased: 1.415
batch start
#iterations: 336
currently lose_sum: 83.61360782384872
time_elpased: 1.397
batch start
#iterations: 337
currently lose_sum: 83.9904996752739
time_elpased: 1.422
batch start
#iterations: 338
currently lose_sum: 84.40904831886292
time_elpased: 1.414
batch start
#iterations: 339
currently lose_sum: 84.48263090848923
time_elpased: 1.42
start validation test
0.655773195876
0.677328975018
0.597159324825
0.634722678044
0.65587003829
58.959
batch start
#iterations: 340
currently lose_sum: 84.36138823628426
time_elpased: 1.413
batch start
#iterations: 341
currently lose_sum: 84.00426641106606
time_elpased: 1.441
batch start
#iterations: 342
currently lose_sum: 84.31732589006424
time_elpased: 1.394
batch start
#iterations: 343
currently lose_sum: 84.64900422096252
time_elpased: 1.436
batch start
#iterations: 344
currently lose_sum: 84.1937205195427
time_elpased: 1.384
batch start
#iterations: 345
currently lose_sum: 84.33153685927391
time_elpased: 1.435
batch start
#iterations: 346
currently lose_sum: 84.099165558815
time_elpased: 1.415
batch start
#iterations: 347
currently lose_sum: 84.24196910858154
time_elpased: 1.4
batch start
#iterations: 348
currently lose_sum: 83.99953126907349
time_elpased: 1.435
batch start
#iterations: 349
currently lose_sum: 84.44990891218185
time_elpased: 1.399
batch start
#iterations: 350
currently lose_sum: 84.21272611618042
time_elpased: 1.399
batch start
#iterations: 351
currently lose_sum: 84.22860074043274
time_elpased: 1.394
batch start
#iterations: 352
currently lose_sum: 84.48594826459885
time_elpased: 1.44
batch start
#iterations: 353
currently lose_sum: 84.8505699634552
time_elpased: 1.447
batch start
#iterations: 354
currently lose_sum: 83.55414116382599
time_elpased: 1.458
batch start
#iterations: 355
currently lose_sum: 84.04914575815201
time_elpased: 1.426
batch start
#iterations: 356
currently lose_sum: 83.6286091208458
time_elpased: 1.404
batch start
#iterations: 357
currently lose_sum: 84.41312664747238
time_elpased: 1.388
batch start
#iterations: 358
currently lose_sum: 83.51819807291031
time_elpased: 1.421
batch start
#iterations: 359
currently lose_sum: 83.44277864694595
time_elpased: 1.427
start validation test
0.662164948454
0.673736263736
0.631020996295
0.651679421769
0.662216404798
58.717
batch start
#iterations: 360
currently lose_sum: 83.9191942512989
time_elpased: 1.407
batch start
#iterations: 361
currently lose_sum: 83.81108146905899
time_elpased: 1.401
batch start
#iterations: 362
currently lose_sum: 83.88313841819763
time_elpased: 1.41
batch start
#iterations: 363
currently lose_sum: 83.98338347673416
time_elpased: 1.436
batch start
#iterations: 364
currently lose_sum: 83.81278175115585
time_elpased: 1.399
batch start
#iterations: 365
currently lose_sum: 84.11795109510422
time_elpased: 1.417
batch start
#iterations: 366
currently lose_sum: 84.02592128515244
time_elpased: 1.411
batch start
#iterations: 367
currently lose_sum: 83.42913806438446
time_elpased: 1.454
batch start
#iterations: 368
currently lose_sum: 83.99014076590538
time_elpased: 1.394
batch start
#iterations: 369
currently lose_sum: 83.72976392507553
time_elpased: 1.403
batch start
#iterations: 370
currently lose_sum: 83.52678963541985
time_elpased: 1.417
batch start
#iterations: 371
currently lose_sum: 83.29518604278564
time_elpased: 1.39
batch start
#iterations: 372
currently lose_sum: 83.72276854515076
time_elpased: 1.424
batch start
#iterations: 373
currently lose_sum: 84.2735920548439
time_elpased: 1.403
batch start
#iterations: 374
currently lose_sum: 83.82254713773727
time_elpased: 1.394
batch start
#iterations: 375
currently lose_sum: 82.94604659080505
time_elpased: 1.415
batch start
#iterations: 376
currently lose_sum: 84.0128858089447
time_elpased: 1.432
batch start
#iterations: 377
currently lose_sum: 83.14505517482758
time_elpased: 1.406
batch start
#iterations: 378
currently lose_sum: 83.78841000795364
time_elpased: 1.432
batch start
#iterations: 379
currently lose_sum: 83.54420211911201
time_elpased: 1.459
start validation test
0.648762886598
0.671189240208
0.58552902429
0.625439753738
0.648867362207
59.460
batch start
#iterations: 380
currently lose_sum: 82.91632622480392
time_elpased: 1.42
batch start
#iterations: 381
currently lose_sum: 83.78780847787857
time_elpased: 1.413
batch start
#iterations: 382
currently lose_sum: 83.67077958583832
time_elpased: 1.419
batch start
#iterations: 383
currently lose_sum: 83.75897747278214
time_elpased: 1.475
batch start
#iterations: 384
currently lose_sum: 83.14810568094254
time_elpased: 1.422
batch start
#iterations: 385
currently lose_sum: 83.32055735588074
time_elpased: 1.423
batch start
#iterations: 386
currently lose_sum: 83.81988352537155
time_elpased: 1.427
batch start
#iterations: 387
currently lose_sum: 83.3879879117012
time_elpased: 1.388
batch start
#iterations: 388
currently lose_sum: 83.13571244478226
time_elpased: 1.421
batch start
#iterations: 389
currently lose_sum: 83.48293924331665
time_elpased: 1.407
batch start
#iterations: 390
currently lose_sum: 83.37539011240005
time_elpased: 1.403
batch start
#iterations: 391
currently lose_sum: 82.96191138029099
time_elpased: 1.413
batch start
#iterations: 392
currently lose_sum: 83.62321496009827
time_elpased: 1.431
batch start
#iterations: 393
currently lose_sum: 82.76824477314949
time_elpased: 1.416
batch start
#iterations: 394
currently lose_sum: 83.08829781413078
time_elpased: 1.46
batch start
#iterations: 395
currently lose_sum: 83.04983749985695
time_elpased: 1.419
batch start
#iterations: 396
currently lose_sum: 82.69495803117752
time_elpased: 1.444
batch start
#iterations: 397
currently lose_sum: 83.00917154550552
time_elpased: 1.407
batch start
#iterations: 398
currently lose_sum: 83.10680043697357
time_elpased: 1.43
batch start
#iterations: 399
currently lose_sum: 83.20332127809525
time_elpased: 1.442
start validation test
0.651340206186
0.670793797732
0.596644709757
0.631550277808
0.651430574622
59.368
acc: 0.691
pre: 0.676
rec: 0.733
F1: 0.703
auc: 0.690
