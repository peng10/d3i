start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.12181931734085
time_elpased: 1.432
batch start
#iterations: 1
currently lose_sum: 99.75154232978821
time_elpased: 1.384
batch start
#iterations: 2
currently lose_sum: 99.6393603682518
time_elpased: 1.379
batch start
#iterations: 3
currently lose_sum: 99.46732896566391
time_elpased: 1.41
batch start
#iterations: 4
currently lose_sum: 99.39905679225922
time_elpased: 1.375
batch start
#iterations: 5
currently lose_sum: 99.22023051977158
time_elpased: 1.406
batch start
#iterations: 6
currently lose_sum: 99.02160197496414
time_elpased: 1.377
batch start
#iterations: 7
currently lose_sum: 98.99197345972061
time_elpased: 1.392
batch start
#iterations: 8
currently lose_sum: 98.91100150346756
time_elpased: 1.403
batch start
#iterations: 9
currently lose_sum: 98.49059921503067
time_elpased: 1.419
batch start
#iterations: 10
currently lose_sum: 98.18629741668701
time_elpased: 1.432
batch start
#iterations: 11
currently lose_sum: 98.04571944475174
time_elpased: 1.399
batch start
#iterations: 12
currently lose_sum: 97.5583645105362
time_elpased: 1.401
batch start
#iterations: 13
currently lose_sum: 97.53007942438126
time_elpased: 1.398
batch start
#iterations: 14
currently lose_sum: 96.95994466543198
time_elpased: 1.375
batch start
#iterations: 15
currently lose_sum: 96.84794336557388
time_elpased: 1.408
batch start
#iterations: 16
currently lose_sum: 96.79476082324982
time_elpased: 1.401
batch start
#iterations: 17
currently lose_sum: 96.20329320430756
time_elpased: 1.417
batch start
#iterations: 18
currently lose_sum: 96.21760618686676
time_elpased: 1.443
batch start
#iterations: 19
currently lose_sum: 95.95954859256744
time_elpased: 1.426
start validation test
0.647886597938
0.63531507877
0.697231655861
0.664834895246
0.647799965078
61.441
batch start
#iterations: 20
currently lose_sum: 96.01402848958969
time_elpased: 1.39
batch start
#iterations: 21
currently lose_sum: 95.4354887008667
time_elpased: 1.411
batch start
#iterations: 22
currently lose_sum: 95.67332005500793
time_elpased: 1.427
batch start
#iterations: 23
currently lose_sum: 95.73967069387436
time_elpased: 1.39
batch start
#iterations: 24
currently lose_sum: 95.52957653999329
time_elpased: 1.394
batch start
#iterations: 25
currently lose_sum: 95.06548231840134
time_elpased: 1.376
batch start
#iterations: 26
currently lose_sum: 94.9704264998436
time_elpased: 1.394
batch start
#iterations: 27
currently lose_sum: 94.73536545038223
time_elpased: 1.417
batch start
#iterations: 28
currently lose_sum: 94.57413798570633
time_elpased: 1.376
batch start
#iterations: 29
currently lose_sum: 94.39678627252579
time_elpased: 1.442
batch start
#iterations: 30
currently lose_sum: 94.1932801604271
time_elpased: 1.384
batch start
#iterations: 31
currently lose_sum: 94.1851915717125
time_elpased: 1.418
batch start
#iterations: 32
currently lose_sum: 94.12876504659653
time_elpased: 1.39
batch start
#iterations: 33
currently lose_sum: 93.82264214754105
time_elpased: 1.413
batch start
#iterations: 34
currently lose_sum: 93.94055008888245
time_elpased: 1.395
batch start
#iterations: 35
currently lose_sum: 93.1780195236206
time_elpased: 1.427
batch start
#iterations: 36
currently lose_sum: 93.56231141090393
time_elpased: 1.413
batch start
#iterations: 37
currently lose_sum: 93.56213009357452
time_elpased: 1.43
batch start
#iterations: 38
currently lose_sum: 93.04075986146927
time_elpased: 1.402
batch start
#iterations: 39
currently lose_sum: 92.98901861906052
time_elpased: 1.409
start validation test
0.665412371134
0.6515692539
0.713594730884
0.681172945626
0.66532777957
59.420
batch start
#iterations: 40
currently lose_sum: 93.16496533155441
time_elpased: 1.385
batch start
#iterations: 41
currently lose_sum: 92.66617119312286
time_elpased: 1.425
batch start
#iterations: 42
currently lose_sum: 92.53376132249832
time_elpased: 1.395
batch start
#iterations: 43
currently lose_sum: 93.3772024512291
time_elpased: 1.404
batch start
#iterations: 44
currently lose_sum: 92.71424877643585
time_elpased: 1.399
batch start
#iterations: 45
currently lose_sum: 92.71751320362091
time_elpased: 1.382
batch start
#iterations: 46
currently lose_sum: 92.51365226507187
time_elpased: 1.415
batch start
#iterations: 47
currently lose_sum: 92.66094905138016
time_elpased: 1.428
batch start
#iterations: 48
currently lose_sum: 92.47682726383209
time_elpased: 1.407
batch start
#iterations: 49
currently lose_sum: 92.07151144742966
time_elpased: 1.397
batch start
#iterations: 50
currently lose_sum: 92.19946682453156
time_elpased: 1.405
batch start
#iterations: 51
currently lose_sum: 92.02314728498459
time_elpased: 1.388
batch start
#iterations: 52
currently lose_sum: 91.99757850170135
time_elpased: 1.4
batch start
#iterations: 53
currently lose_sum: 91.84850972890854
time_elpased: 1.401
batch start
#iterations: 54
currently lose_sum: 91.74242222309113
time_elpased: 1.407
batch start
#iterations: 55
currently lose_sum: 92.00949817895889
time_elpased: 1.397
batch start
#iterations: 56
currently lose_sum: 91.71015870571136
time_elpased: 1.457
batch start
#iterations: 57
currently lose_sum: 91.6725869178772
time_elpased: 1.382
batch start
#iterations: 58
currently lose_sum: 91.32542556524277
time_elpased: 1.413
batch start
#iterations: 59
currently lose_sum: 91.78541278839111
time_elpased: 1.417
start validation test
0.659639175258
0.660681114551
0.65884532263
0.659761941567
0.659640568988
59.105
batch start
#iterations: 60
currently lose_sum: 92.14742356538773
time_elpased: 1.413
batch start
#iterations: 61
currently lose_sum: 91.80143684148788
time_elpased: 1.444
batch start
#iterations: 62
currently lose_sum: 91.71069180965424
time_elpased: 1.43
batch start
#iterations: 63
currently lose_sum: 91.34310638904572
time_elpased: 1.372
batch start
#iterations: 64
currently lose_sum: 91.58310997486115
time_elpased: 1.415
batch start
#iterations: 65
currently lose_sum: 91.42038869857788
time_elpased: 1.387
batch start
#iterations: 66
currently lose_sum: 91.27543145418167
time_elpased: 1.387
batch start
#iterations: 67
currently lose_sum: 90.7558005452156
time_elpased: 1.408
batch start
#iterations: 68
currently lose_sum: 91.0201388001442
time_elpased: 1.396
batch start
#iterations: 69
currently lose_sum: 91.11682176589966
time_elpased: 1.392
batch start
#iterations: 70
currently lose_sum: 91.39994084835052
time_elpased: 1.397
batch start
#iterations: 71
currently lose_sum: 90.82356935739517
time_elpased: 1.395
batch start
#iterations: 72
currently lose_sum: 90.88620126247406
time_elpased: 1.381
batch start
#iterations: 73
currently lose_sum: 90.67965465784073
time_elpased: 1.409
batch start
#iterations: 74
currently lose_sum: 90.24405872821808
time_elpased: 1.437
batch start
#iterations: 75
currently lose_sum: 90.99559569358826
time_elpased: 1.413
batch start
#iterations: 76
currently lose_sum: 90.42787700891495
time_elpased: 1.396
batch start
#iterations: 77
currently lose_sum: 90.48785227537155
time_elpased: 1.468
batch start
#iterations: 78
currently lose_sum: 90.54193073511124
time_elpased: 1.394
batch start
#iterations: 79
currently lose_sum: 90.33200663328171
time_elpased: 1.397
start validation test
0.664432989691
0.648101967304
0.722136461871
0.683119158879
0.664331682345
58.792
batch start
#iterations: 80
currently lose_sum: 90.37408816814423
time_elpased: 1.402
batch start
#iterations: 81
currently lose_sum: 90.98958694934845
time_elpased: 1.407
batch start
#iterations: 82
currently lose_sum: 90.54318571090698
time_elpased: 1.383
batch start
#iterations: 83
currently lose_sum: 90.5550764799118
time_elpased: 1.396
batch start
#iterations: 84
currently lose_sum: 90.98681092262268
time_elpased: 1.413
batch start
#iterations: 85
currently lose_sum: 90.77797871828079
time_elpased: 1.383
batch start
#iterations: 86
currently lose_sum: 90.38997530937195
time_elpased: 1.444
batch start
#iterations: 87
currently lose_sum: 90.3715888261795
time_elpased: 1.421
batch start
#iterations: 88
currently lose_sum: 89.85301268100739
time_elpased: 1.417
batch start
#iterations: 89
currently lose_sum: 90.59059470891953
time_elpased: 1.392
batch start
#iterations: 90
currently lose_sum: 89.46402245759964
time_elpased: 1.397
batch start
#iterations: 91
currently lose_sum: 90.48723828792572
time_elpased: 1.416
batch start
#iterations: 92
currently lose_sum: 90.15328216552734
time_elpased: 1.384
batch start
#iterations: 93
currently lose_sum: 89.30205714702606
time_elpased: 1.397
batch start
#iterations: 94
currently lose_sum: 90.13569647073746
time_elpased: 1.384
batch start
#iterations: 95
currently lose_sum: 89.98525220155716
time_elpased: 1.386
batch start
#iterations: 96
currently lose_sum: 89.84535717964172
time_elpased: 1.374
batch start
#iterations: 97
currently lose_sum: 89.57029795646667
time_elpased: 1.402
batch start
#iterations: 98
currently lose_sum: 89.47063624858856
time_elpased: 1.385
batch start
#iterations: 99
currently lose_sum: 89.7349112033844
time_elpased: 1.399
start validation test
0.668195876289
0.665056360709
0.680045281465
0.67246730779
0.66817507283
58.112
batch start
#iterations: 100
currently lose_sum: 89.67641711235046
time_elpased: 1.394
batch start
#iterations: 101
currently lose_sum: 89.62045735120773
time_elpased: 1.423
batch start
#iterations: 102
currently lose_sum: 89.77687960863113
time_elpased: 1.378
batch start
#iterations: 103
currently lose_sum: 89.36634945869446
time_elpased: 1.406
batch start
#iterations: 104
currently lose_sum: 88.90160900354385
time_elpased: 1.428
batch start
#iterations: 105
currently lose_sum: 89.64045351743698
time_elpased: 1.384
batch start
#iterations: 106
currently lose_sum: 89.29128503799438
time_elpased: 1.397
batch start
#iterations: 107
currently lose_sum: 89.42531114816666
time_elpased: 1.376
batch start
#iterations: 108
currently lose_sum: 89.4586352109909
time_elpased: 1.425
batch start
#iterations: 109
currently lose_sum: 89.30919194221497
time_elpased: 1.38
batch start
#iterations: 110
currently lose_sum: 89.4945097565651
time_elpased: 1.412
batch start
#iterations: 111
currently lose_sum: 89.54564201831818
time_elpased: 1.401
batch start
#iterations: 112
currently lose_sum: 88.95030575990677
time_elpased: 1.382
batch start
#iterations: 113
currently lose_sum: 88.47014886140823
time_elpased: 1.4
batch start
#iterations: 114
currently lose_sum: 88.80155390501022
time_elpased: 1.391
batch start
#iterations: 115
currently lose_sum: 89.14160388708115
time_elpased: 1.42
batch start
#iterations: 116
currently lose_sum: 89.14651238918304
time_elpased: 1.382
batch start
#iterations: 117
currently lose_sum: 88.8948101401329
time_elpased: 1.428
batch start
#iterations: 118
currently lose_sum: 89.02758574485779
time_elpased: 1.392
batch start
#iterations: 119
currently lose_sum: 88.75010025501251
time_elpased: 1.388
start validation test
0.676701030928
0.668591563081
0.702994751467
0.685361693589
0.676654868246
57.604
batch start
#iterations: 120
currently lose_sum: 88.63796669244766
time_elpased: 1.39
batch start
#iterations: 121
currently lose_sum: 89.81870430707932
time_elpased: 1.429
batch start
#iterations: 122
currently lose_sum: 88.93925309181213
time_elpased: 1.392
batch start
#iterations: 123
currently lose_sum: 88.78015500307083
time_elpased: 1.395
batch start
#iterations: 124
currently lose_sum: 88.52026081085205
time_elpased: 1.38
batch start
#iterations: 125
currently lose_sum: 88.49783653020859
time_elpased: 1.399
batch start
#iterations: 126
currently lose_sum: 88.81788623332977
time_elpased: 1.402
batch start
#iterations: 127
currently lose_sum: 88.97849476337433
time_elpased: 1.39
batch start
#iterations: 128
currently lose_sum: 88.1029703617096
time_elpased: 1.431
batch start
#iterations: 129
currently lose_sum: 88.663625061512
time_elpased: 1.396
batch start
#iterations: 130
currently lose_sum: 88.79557460546494
time_elpased: 1.446
batch start
#iterations: 131
currently lose_sum: 88.745654463768
time_elpased: 1.428
batch start
#iterations: 132
currently lose_sum: 89.06261134147644
time_elpased: 1.416
batch start
#iterations: 133
currently lose_sum: 88.24737912416458
time_elpased: 1.404
batch start
#iterations: 134
currently lose_sum: 88.04998761415482
time_elpased: 1.456
batch start
#iterations: 135
currently lose_sum: 88.489104449749
time_elpased: 1.429
batch start
#iterations: 136
currently lose_sum: 88.80974787473679
time_elpased: 1.401
batch start
#iterations: 137
currently lose_sum: 88.62940096855164
time_elpased: 1.399
batch start
#iterations: 138
currently lose_sum: 88.23875361680984
time_elpased: 1.396
batch start
#iterations: 139
currently lose_sum: 88.73929101228714
time_elpased: 1.422
start validation test
0.677010309278
0.679795769511
0.671400638057
0.675572123848
0.677020157921
57.149
batch start
#iterations: 140
currently lose_sum: 88.14619147777557
time_elpased: 1.431
batch start
#iterations: 141
currently lose_sum: 88.59046649932861
time_elpased: 1.394
batch start
#iterations: 142
currently lose_sum: 88.59500396251678
time_elpased: 1.396
batch start
#iterations: 143
currently lose_sum: 87.89980697631836
time_elpased: 1.417
batch start
#iterations: 144
currently lose_sum: 88.42008501291275
time_elpased: 1.405
batch start
#iterations: 145
currently lose_sum: 88.05241006612778
time_elpased: 1.424
batch start
#iterations: 146
currently lose_sum: 88.47974109649658
time_elpased: 1.411
batch start
#iterations: 147
currently lose_sum: 88.23398333787918
time_elpased: 1.413
batch start
#iterations: 148
currently lose_sum: 88.00364768505096
time_elpased: 1.405
batch start
#iterations: 149
currently lose_sum: 88.31154251098633
time_elpased: 1.405
batch start
#iterations: 150
currently lose_sum: 88.38668519258499
time_elpased: 1.42
batch start
#iterations: 151
currently lose_sum: 88.30304038524628
time_elpased: 1.396
batch start
#iterations: 152
currently lose_sum: 87.77425521612167
time_elpased: 1.391
batch start
#iterations: 153
currently lose_sum: 88.20042669773102
time_elpased: 1.391
batch start
#iterations: 154
currently lose_sum: 87.37421715259552
time_elpased: 1.403
batch start
#iterations: 155
currently lose_sum: 88.08649319410324
time_elpased: 1.414
batch start
#iterations: 156
currently lose_sum: 88.13069748878479
time_elpased: 1.412
batch start
#iterations: 157
currently lose_sum: 88.08598625659943
time_elpased: 1.428
batch start
#iterations: 158
currently lose_sum: 88.24568283557892
time_elpased: 1.417
batch start
#iterations: 159
currently lose_sum: 88.12329465150833
time_elpased: 1.397
start validation test
0.685360824742
0.6780680138
0.7079345477
0.69267948847
0.68532119309
56.865
batch start
#iterations: 160
currently lose_sum: 87.97165769338608
time_elpased: 1.404
batch start
#iterations: 161
currently lose_sum: 88.00201433897018
time_elpased: 1.394
batch start
#iterations: 162
currently lose_sum: 87.71710288524628
time_elpased: 1.402
batch start
#iterations: 163
currently lose_sum: 87.63415396213531
time_elpased: 1.417
batch start
#iterations: 164
currently lose_sum: 87.95324146747589
time_elpased: 1.418
batch start
#iterations: 165
currently lose_sum: 87.3827520608902
time_elpased: 1.41
batch start
#iterations: 166
currently lose_sum: 87.4587996006012
time_elpased: 1.414
batch start
#iterations: 167
currently lose_sum: 87.99751597642899
time_elpased: 1.407
batch start
#iterations: 168
currently lose_sum: 87.83747285604477
time_elpased: 1.39
batch start
#iterations: 169
currently lose_sum: 87.38425159454346
time_elpased: 1.402
batch start
#iterations: 170
currently lose_sum: 87.25373989343643
time_elpased: 1.423
batch start
#iterations: 171
currently lose_sum: 87.15424299240112
time_elpased: 1.467
batch start
#iterations: 172
currently lose_sum: 87.4828297495842
time_elpased: 1.418
batch start
#iterations: 173
currently lose_sum: 86.97410494089127
time_elpased: 1.415
batch start
#iterations: 174
currently lose_sum: 87.01802277565002
time_elpased: 1.403
batch start
#iterations: 175
currently lose_sum: 87.62475055456161
time_elpased: 1.38
batch start
#iterations: 176
currently lose_sum: 87.54987680912018
time_elpased: 1.403
batch start
#iterations: 177
currently lose_sum: 87.7949840426445
time_elpased: 1.404
batch start
#iterations: 178
currently lose_sum: 86.90979427099228
time_elpased: 1.447
batch start
#iterations: 179
currently lose_sum: 87.29552191495895
time_elpased: 1.409
start validation test
0.673298969072
0.675678486014
0.668724915097
0.672183717803
0.673306999529
57.474
batch start
#iterations: 180
currently lose_sum: 87.44705355167389
time_elpased: 1.39
batch start
#iterations: 181
currently lose_sum: 86.99204474687576
time_elpased: 1.435
batch start
#iterations: 182
currently lose_sum: 87.27944284677505
time_elpased: 1.391
batch start
#iterations: 183
currently lose_sum: 87.44611990451813
time_elpased: 1.398
batch start
#iterations: 184
currently lose_sum: 87.00696021318436
time_elpased: 1.387
batch start
#iterations: 185
currently lose_sum: 87.37472891807556
time_elpased: 1.412
batch start
#iterations: 186
currently lose_sum: 86.99658119678497
time_elpased: 1.396
batch start
#iterations: 187
currently lose_sum: 87.4721040725708
time_elpased: 1.384
batch start
#iterations: 188
currently lose_sum: 87.02039867639542
time_elpased: 1.406
batch start
#iterations: 189
currently lose_sum: 87.66148090362549
time_elpased: 1.373
batch start
#iterations: 190
currently lose_sum: 86.40537697076797
time_elpased: 1.402
batch start
#iterations: 191
currently lose_sum: 86.96533972024918
time_elpased: 1.4
batch start
#iterations: 192
currently lose_sum: 87.17178291082382
time_elpased: 1.39
batch start
#iterations: 193
currently lose_sum: 86.91239178180695
time_elpased: 1.424
batch start
#iterations: 194
currently lose_sum: 87.15512120723724
time_elpased: 1.45
batch start
#iterations: 195
currently lose_sum: 86.95262569189072
time_elpased: 1.369
batch start
#iterations: 196
currently lose_sum: 86.58543080091476
time_elpased: 1.411
batch start
#iterations: 197
currently lose_sum: 87.59916216135025
time_elpased: 1.381
batch start
#iterations: 198
currently lose_sum: 86.48572152853012
time_elpased: 1.415
batch start
#iterations: 199
currently lose_sum: 86.60079151391983
time_elpased: 1.41
start validation test
0.66412371134
0.667398807656
0.656684161778
0.661998132586
0.664136772617
58.135
batch start
#iterations: 200
currently lose_sum: 87.30874532461166
time_elpased: 1.413
batch start
#iterations: 201
currently lose_sum: 86.63606142997742
time_elpased: 1.416
batch start
#iterations: 202
currently lose_sum: 86.9878534078598
time_elpased: 1.424
batch start
#iterations: 203
currently lose_sum: 86.62614238262177
time_elpased: 1.387
batch start
#iterations: 204
currently lose_sum: 86.57664638757706
time_elpased: 1.381
batch start
#iterations: 205
currently lose_sum: 86.489572763443
time_elpased: 1.449
batch start
#iterations: 206
currently lose_sum: 86.83541291952133
time_elpased: 1.412
batch start
#iterations: 207
currently lose_sum: 86.41170406341553
time_elpased: 1.396
batch start
#iterations: 208
currently lose_sum: 86.45765256881714
time_elpased: 1.401
batch start
#iterations: 209
currently lose_sum: 86.38134974241257
time_elpased: 1.408
batch start
#iterations: 210
currently lose_sum: 86.5158640742302
time_elpased: 1.457
batch start
#iterations: 211
currently lose_sum: 86.76468336582184
time_elpased: 1.415
batch start
#iterations: 212
currently lose_sum: 86.20405149459839
time_elpased: 1.419
batch start
#iterations: 213
currently lose_sum: 86.42388796806335
time_elpased: 1.426
batch start
#iterations: 214
currently lose_sum: 86.63129180669785
time_elpased: 1.385
batch start
#iterations: 215
currently lose_sum: 86.30003201961517
time_elpased: 1.395
batch start
#iterations: 216
currently lose_sum: 86.78921842575073
time_elpased: 1.413
batch start
#iterations: 217
currently lose_sum: 86.55477386713028
time_elpased: 1.393
batch start
#iterations: 218
currently lose_sum: 86.35837721824646
time_elpased: 1.396
batch start
#iterations: 219
currently lose_sum: 86.380868434906
time_elpased: 1.397
start validation test
0.668762886598
0.68494998314
0.627148296799
0.654775975073
0.668835947429
57.727
batch start
#iterations: 220
currently lose_sum: 86.75856810808182
time_elpased: 1.44
batch start
#iterations: 221
currently lose_sum: 86.6575499176979
time_elpased: 1.451
batch start
#iterations: 222
currently lose_sum: 86.68519252538681
time_elpased: 1.414
batch start
#iterations: 223
currently lose_sum: 86.05539178848267
time_elpased: 1.417
batch start
#iterations: 224
currently lose_sum: 86.09902483224869
time_elpased: 1.44
batch start
#iterations: 225
currently lose_sum: 86.4977560043335
time_elpased: 1.404
batch start
#iterations: 226
currently lose_sum: 85.94306081533432
time_elpased: 1.413
batch start
#iterations: 227
currently lose_sum: 85.74378871917725
time_elpased: 1.402
batch start
#iterations: 228
currently lose_sum: 85.98641794919968
time_elpased: 1.427
batch start
#iterations: 229
currently lose_sum: 85.911885201931
time_elpased: 1.383
batch start
#iterations: 230
currently lose_sum: 86.4850018620491
time_elpased: 1.388
batch start
#iterations: 231
currently lose_sum: 85.93097013235092
time_elpased: 1.383
batch start
#iterations: 232
currently lose_sum: 86.39793264865875
time_elpased: 1.435
batch start
#iterations: 233
currently lose_sum: 85.66706085205078
time_elpased: 1.385
batch start
#iterations: 234
currently lose_sum: 85.68528246879578
time_elpased: 1.44
batch start
#iterations: 235
currently lose_sum: 86.37147498130798
time_elpased: 1.431
batch start
#iterations: 236
currently lose_sum: 86.06436365842819
time_elpased: 1.429
batch start
#iterations: 237
currently lose_sum: 85.71118611097336
time_elpased: 1.409
batch start
#iterations: 238
currently lose_sum: 86.40559607744217
time_elpased: 1.413
batch start
#iterations: 239
currently lose_sum: 85.84137773513794
time_elpased: 1.4
start validation test
0.672371134021
0.692695791767
0.621693938458
0.655277145027
0.672460105654
57.387
batch start
#iterations: 240
currently lose_sum: 85.64020019769669
time_elpased: 1.415
batch start
#iterations: 241
currently lose_sum: 86.32863438129425
time_elpased: 1.413
batch start
#iterations: 242
currently lose_sum: 86.09627336263657
time_elpased: 1.42
batch start
#iterations: 243
currently lose_sum: 86.45739203691483
time_elpased: 1.412
batch start
#iterations: 244
currently lose_sum: 86.06608533859253
time_elpased: 1.412
batch start
#iterations: 245
currently lose_sum: 86.10579341650009
time_elpased: 1.382
batch start
#iterations: 246
currently lose_sum: 86.0245012640953
time_elpased: 1.436
batch start
#iterations: 247
currently lose_sum: 85.73214209079742
time_elpased: 1.383
batch start
#iterations: 248
currently lose_sum: 86.30433189868927
time_elpased: 1.389
batch start
#iterations: 249
currently lose_sum: 85.48400521278381
time_elpased: 1.416
batch start
#iterations: 250
currently lose_sum: 85.14620411396027
time_elpased: 1.397
batch start
#iterations: 251
currently lose_sum: 85.87706458568573
time_elpased: 1.404
batch start
#iterations: 252
currently lose_sum: 85.63345539569855
time_elpased: 1.4
batch start
#iterations: 253
currently lose_sum: 85.70715647935867
time_elpased: 1.4
batch start
#iterations: 254
currently lose_sum: 85.43818598985672
time_elpased: 1.408
batch start
#iterations: 255
currently lose_sum: 85.83202749490738
time_elpased: 1.394
batch start
#iterations: 256
currently lose_sum: 84.85792344808578
time_elpased: 1.403
batch start
#iterations: 257
currently lose_sum: 85.67531424760818
time_elpased: 1.406
batch start
#iterations: 258
currently lose_sum: 85.20860874652863
time_elpased: 1.427
batch start
#iterations: 259
currently lose_sum: 85.15715581178665
time_elpased: 1.401
start validation test
0.661649484536
0.677314137892
0.619738602449
0.647248495271
0.661723065554
58.181
batch start
#iterations: 260
currently lose_sum: 85.13376635313034
time_elpased: 1.394
batch start
#iterations: 261
currently lose_sum: 85.27403485774994
time_elpased: 1.467
batch start
#iterations: 262
currently lose_sum: 85.33977448940277
time_elpased: 1.393
batch start
#iterations: 263
currently lose_sum: 85.75177943706512
time_elpased: 1.412
batch start
#iterations: 264
currently lose_sum: 85.89744263887405
time_elpased: 1.401
batch start
#iterations: 265
currently lose_sum: 85.6912932395935
time_elpased: 1.405
batch start
#iterations: 266
currently lose_sum: 85.16990786790848
time_elpased: 1.393
batch start
#iterations: 267
currently lose_sum: 86.04563015699387
time_elpased: 1.431
batch start
#iterations: 268
currently lose_sum: 85.2706727385521
time_elpased: 1.386
batch start
#iterations: 269
currently lose_sum: 85.93795818090439
time_elpased: 1.434
batch start
#iterations: 270
currently lose_sum: 85.10290938615799
time_elpased: 1.4
batch start
#iterations: 271
currently lose_sum: 85.29027724266052
time_elpased: 1.405
batch start
#iterations: 272
currently lose_sum: 85.2600029706955
time_elpased: 1.45
batch start
#iterations: 273
currently lose_sum: 85.09677767753601
time_elpased: 1.396
batch start
#iterations: 274
currently lose_sum: 85.40218329429626
time_elpased: 1.412
batch start
#iterations: 275
currently lose_sum: 85.19334727525711
time_elpased: 1.409
batch start
#iterations: 276
currently lose_sum: 84.87070053815842
time_elpased: 1.436
batch start
#iterations: 277
currently lose_sum: 85.50764638185501
time_elpased: 1.424
batch start
#iterations: 278
currently lose_sum: 84.58181077241898
time_elpased: 1.392
batch start
#iterations: 279
currently lose_sum: 85.54441285133362
time_elpased: 1.396
start validation test
0.670360824742
0.684391651865
0.634455078728
0.658477970628
0.670423862817
57.484
batch start
#iterations: 280
currently lose_sum: 84.86821955442429
time_elpased: 1.398
batch start
#iterations: 281
currently lose_sum: 85.92160218954086
time_elpased: 1.379
batch start
#iterations: 282
currently lose_sum: 85.1534406542778
time_elpased: 1.399
batch start
#iterations: 283
currently lose_sum: 84.49113774299622
time_elpased: 1.398
batch start
#iterations: 284
currently lose_sum: 84.69990688562393
time_elpased: 1.43
batch start
#iterations: 285
currently lose_sum: 85.03114783763885
time_elpased: 1.411
batch start
#iterations: 286
currently lose_sum: 85.2009140253067
time_elpased: 1.38
batch start
#iterations: 287
currently lose_sum: 84.55729299783707
time_elpased: 1.395
batch start
#iterations: 288
currently lose_sum: 85.13471513986588
time_elpased: 1.444
batch start
#iterations: 289
currently lose_sum: 84.96401751041412
time_elpased: 1.409
batch start
#iterations: 290
currently lose_sum: 84.90526920557022
time_elpased: 1.405
batch start
#iterations: 291
currently lose_sum: 84.38937401771545
time_elpased: 1.401
batch start
#iterations: 292
currently lose_sum: 84.60451132059097
time_elpased: 1.419
batch start
#iterations: 293
currently lose_sum: 85.02765756845474
time_elpased: 1.405
batch start
#iterations: 294
currently lose_sum: 84.72073704004288
time_elpased: 1.385
batch start
#iterations: 295
currently lose_sum: 83.84137326478958
time_elpased: 1.391
batch start
#iterations: 296
currently lose_sum: 84.20658302307129
time_elpased: 1.413
batch start
#iterations: 297
currently lose_sum: 84.40519925951958
time_elpased: 1.396
batch start
#iterations: 298
currently lose_sum: 84.61744821071625
time_elpased: 1.417
batch start
#iterations: 299
currently lose_sum: 84.45865589380264
time_elpased: 1.385
start validation test
0.672422680412
0.675469728601
0.665946279716
0.670674198062
0.672434050733
57.471
batch start
#iterations: 300
currently lose_sum: 84.37975347042084
time_elpased: 1.411
batch start
#iterations: 301
currently lose_sum: 84.87310916185379
time_elpased: 1.393
batch start
#iterations: 302
currently lose_sum: 85.01780867576599
time_elpased: 1.387
batch start
#iterations: 303
currently lose_sum: 85.08775341510773
time_elpased: 1.436
batch start
#iterations: 304
currently lose_sum: 84.82151186466217
time_elpased: 1.419
batch start
#iterations: 305
currently lose_sum: 84.7789078950882
time_elpased: 1.374
batch start
#iterations: 306
currently lose_sum: 84.76194834709167
time_elpased: 1.397
batch start
#iterations: 307
currently lose_sum: 84.47288447618484
time_elpased: 1.383
batch start
#iterations: 308
currently lose_sum: 84.38720858097076
time_elpased: 1.404
batch start
#iterations: 309
currently lose_sum: 84.06236708164215
time_elpased: 1.408
batch start
#iterations: 310
currently lose_sum: 84.89465802907944
time_elpased: 1.442
batch start
#iterations: 311
currently lose_sum: 83.74457156658173
time_elpased: 1.375
batch start
#iterations: 312
currently lose_sum: 84.46617186069489
time_elpased: 1.402
batch start
#iterations: 313
currently lose_sum: 84.27271127700806
time_elpased: 1.428
batch start
#iterations: 314
currently lose_sum: 84.25688010454178
time_elpased: 1.446
batch start
#iterations: 315
currently lose_sum: 83.88777840137482
time_elpased: 1.379
batch start
#iterations: 316
currently lose_sum: 84.36265307664871
time_elpased: 1.397
batch start
#iterations: 317
currently lose_sum: 84.52529752254486
time_elpased: 1.425
batch start
#iterations: 318
currently lose_sum: 84.56388705968857
time_elpased: 1.372
batch start
#iterations: 319
currently lose_sum: 84.02053731679916
time_elpased: 1.432
start validation test
0.668144329897
0.672488164124
0.657816198415
0.6650712725
0.668162462525
57.892
batch start
#iterations: 320
currently lose_sum: 83.99638390541077
time_elpased: 1.442
batch start
#iterations: 321
currently lose_sum: 83.97748938202858
time_elpased: 1.385
batch start
#iterations: 322
currently lose_sum: 84.85068464279175
time_elpased: 1.476
batch start
#iterations: 323
currently lose_sum: 84.20733857154846
time_elpased: 1.403
batch start
#iterations: 324
currently lose_sum: 84.38454204797745
time_elpased: 1.401
batch start
#iterations: 325
currently lose_sum: 84.73055624961853
time_elpased: 1.409
batch start
#iterations: 326
currently lose_sum: 83.7889376282692
time_elpased: 1.392
batch start
#iterations: 327
currently lose_sum: 84.22382086515427
time_elpased: 1.438
batch start
#iterations: 328
currently lose_sum: 84.37038892507553
time_elpased: 1.405
batch start
#iterations: 329
currently lose_sum: 84.13092404603958
time_elpased: 1.392
batch start
#iterations: 330
currently lose_sum: 84.30072891712189
time_elpased: 1.448
batch start
#iterations: 331
currently lose_sum: 84.60258972644806
time_elpased: 1.426
batch start
#iterations: 332
currently lose_sum: 84.10421413183212
time_elpased: 1.419
batch start
#iterations: 333
currently lose_sum: 83.73176145553589
time_elpased: 1.445
batch start
#iterations: 334
currently lose_sum: 84.24577832221985
time_elpased: 1.409
batch start
#iterations: 335
currently lose_sum: 84.1885376572609
time_elpased: 1.394
batch start
#iterations: 336
currently lose_sum: 84.19618266820908
time_elpased: 1.478
batch start
#iterations: 337
currently lose_sum: 84.22265934944153
time_elpased: 1.381
batch start
#iterations: 338
currently lose_sum: 83.90348267555237
time_elpased: 1.407
batch start
#iterations: 339
currently lose_sum: 83.8499271273613
time_elpased: 1.396
start validation test
0.663762886598
0.682556012803
0.614490068951
0.646737070133
0.663849392629
58.093
batch start
#iterations: 340
currently lose_sum: 83.9906114935875
time_elpased: 1.405
batch start
#iterations: 341
currently lose_sum: 84.55357623100281
time_elpased: 1.395
batch start
#iterations: 342
currently lose_sum: 83.98319679498672
time_elpased: 1.402
batch start
#iterations: 343
currently lose_sum: 84.0462332367897
time_elpased: 1.396
batch start
#iterations: 344
currently lose_sum: 84.12613296508789
time_elpased: 1.407
batch start
#iterations: 345
currently lose_sum: 84.68569338321686
time_elpased: 1.393
batch start
#iterations: 346
currently lose_sum: 83.42546039819717
time_elpased: 1.426
batch start
#iterations: 347
currently lose_sum: 83.7925215959549
time_elpased: 1.386
batch start
#iterations: 348
currently lose_sum: 83.88367503881454
time_elpased: 1.385
batch start
#iterations: 349
currently lose_sum: 84.12084746360779
time_elpased: 1.403
batch start
#iterations: 350
currently lose_sum: 83.52137434482574
time_elpased: 1.4
batch start
#iterations: 351
currently lose_sum: 83.5732974410057
time_elpased: 1.429
batch start
#iterations: 352
currently lose_sum: 83.18052780628204
time_elpased: 1.42
batch start
#iterations: 353
currently lose_sum: 83.62663093209267
time_elpased: 1.415
batch start
#iterations: 354
currently lose_sum: 83.24361082911491
time_elpased: 1.409
batch start
#iterations: 355
currently lose_sum: 84.06235682964325
time_elpased: 1.384
batch start
#iterations: 356
currently lose_sum: 84.09465289115906
time_elpased: 1.392
batch start
#iterations: 357
currently lose_sum: 83.3310182094574
time_elpased: 1.399
batch start
#iterations: 358
currently lose_sum: 83.64925891160965
time_elpased: 1.402
batch start
#iterations: 359
currently lose_sum: 83.60500764846802
time_elpased: 1.391
start validation test
0.66706185567
0.674299165418
0.648554080478
0.661176100299
0.667094348924
58.020
batch start
#iterations: 360
currently lose_sum: 84.05916577577591
time_elpased: 1.416
batch start
#iterations: 361
currently lose_sum: 84.42372965812683
time_elpased: 1.405
batch start
#iterations: 362
currently lose_sum: 83.4445326924324
time_elpased: 1.403
batch start
#iterations: 363
currently lose_sum: 83.72094333171844
time_elpased: 1.403
batch start
#iterations: 364
currently lose_sum: 83.43262547254562
time_elpased: 1.475
batch start
#iterations: 365
currently lose_sum: 83.45105302333832
time_elpased: 1.421
batch start
#iterations: 366
currently lose_sum: 83.58822977542877
time_elpased: 1.451
batch start
#iterations: 367
currently lose_sum: 83.13137635588646
time_elpased: 1.376
batch start
#iterations: 368
currently lose_sum: 83.01994213461876
time_elpased: 1.407
batch start
#iterations: 369
currently lose_sum: 84.34492546319962
time_elpased: 1.399
batch start
#iterations: 370
currently lose_sum: 83.35419172048569
time_elpased: 1.393
batch start
#iterations: 371
currently lose_sum: 83.17682653665543
time_elpased: 1.39
batch start
#iterations: 372
currently lose_sum: 83.18221521377563
time_elpased: 1.384
batch start
#iterations: 373
currently lose_sum: 83.17820763587952
time_elpased: 1.376
batch start
#iterations: 374
currently lose_sum: 83.54636770486832
time_elpased: 1.394
batch start
#iterations: 375
currently lose_sum: 83.65389627218246
time_elpased: 1.392
batch start
#iterations: 376
currently lose_sum: 83.58939191699028
time_elpased: 1.392
batch start
#iterations: 377
currently lose_sum: 83.44815635681152
time_elpased: 1.407
batch start
#iterations: 378
currently lose_sum: 83.00741589069366
time_elpased: 1.389
batch start
#iterations: 379
currently lose_sum: 83.4488674402237
time_elpased: 1.391
start validation test
0.667525773196
0.687349466682
0.616754142225
0.650141028423
0.667614910625
58.065
batch start
#iterations: 380
currently lose_sum: 83.13416665792465
time_elpased: 1.434
batch start
#iterations: 381
currently lose_sum: 83.82337337732315
time_elpased: 1.385
batch start
#iterations: 382
currently lose_sum: 83.13455694913864
time_elpased: 1.397
batch start
#iterations: 383
currently lose_sum: 82.58856612443924
time_elpased: 1.417
batch start
#iterations: 384
currently lose_sum: 82.52916693687439
time_elpased: 1.392
batch start
#iterations: 385
currently lose_sum: 83.47551554441452
time_elpased: 1.394
batch start
#iterations: 386
currently lose_sum: 83.14956718683243
time_elpased: 1.43
batch start
#iterations: 387
currently lose_sum: 83.56063985824585
time_elpased: 1.394
batch start
#iterations: 388
currently lose_sum: 83.92295628786087
time_elpased: 1.416
batch start
#iterations: 389
currently lose_sum: 83.43022847175598
time_elpased: 1.413
batch start
#iterations: 390
currently lose_sum: 83.46824592351913
time_elpased: 1.388
batch start
#iterations: 391
currently lose_sum: 83.30064511299133
time_elpased: 1.393
batch start
#iterations: 392
currently lose_sum: 83.65233087539673
time_elpased: 1.447
batch start
#iterations: 393
currently lose_sum: 82.95976975560188
time_elpased: 1.386
batch start
#iterations: 394
currently lose_sum: 82.70488935709
time_elpased: 1.387
batch start
#iterations: 395
currently lose_sum: 82.9759749174118
time_elpased: 1.414
batch start
#iterations: 396
currently lose_sum: 83.18709170818329
time_elpased: 1.41
batch start
#iterations: 397
currently lose_sum: 83.14577940106392
time_elpased: 1.398
batch start
#iterations: 398
currently lose_sum: 83.30472522974014
time_elpased: 1.439
batch start
#iterations: 399
currently lose_sum: 82.87853455543518
time_elpased: 1.4
start validation test
0.66618556701
0.682962628429
0.622517237831
0.651340583611
0.666262233498
58.401
acc: 0.678
pre: 0.671
rec: 0.700
F1: 0.685
auc: 0.678
