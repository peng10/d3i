start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.12520575523376
time_elpased: 1.654
batch start
#iterations: 1
currently lose_sum: 99.68239885568619
time_elpased: 1.601
batch start
#iterations: 2
currently lose_sum: 99.22287386655807
time_elpased: 1.583
batch start
#iterations: 3
currently lose_sum: 99.06383782625198
time_elpased: 1.573
batch start
#iterations: 4
currently lose_sum: 98.63475376367569
time_elpased: 1.62
batch start
#iterations: 5
currently lose_sum: 98.20141518115997
time_elpased: 1.577
batch start
#iterations: 6
currently lose_sum: 98.1392959356308
time_elpased: 1.629
batch start
#iterations: 7
currently lose_sum: 97.43120837211609
time_elpased: 1.586
batch start
#iterations: 8
currently lose_sum: 97.63346356153488
time_elpased: 1.582
batch start
#iterations: 9
currently lose_sum: 97.61874967813492
time_elpased: 1.604
batch start
#iterations: 10
currently lose_sum: 97.44503372907639
time_elpased: 1.562
batch start
#iterations: 11
currently lose_sum: 97.16754257678986
time_elpased: 1.608
batch start
#iterations: 12
currently lose_sum: 96.96030479669571
time_elpased: 1.568
batch start
#iterations: 13
currently lose_sum: 97.05654269456863
time_elpased: 1.616
batch start
#iterations: 14
currently lose_sum: 96.96236222982407
time_elpased: 1.558
batch start
#iterations: 15
currently lose_sum: 96.76610571146011
time_elpased: 1.603
batch start
#iterations: 16
currently lose_sum: 96.47896772623062
time_elpased: 1.612
batch start
#iterations: 17
currently lose_sum: 96.4191106557846
time_elpased: 1.631
batch start
#iterations: 18
currently lose_sum: 96.16363906860352
time_elpased: 1.586
batch start
#iterations: 19
currently lose_sum: 96.15995854139328
time_elpased: 1.597
start validation test
0.655979381443
0.614683048165
0.839250797571
0.709624086321
0.655657620205
61.103
batch start
#iterations: 20
currently lose_sum: 95.89724439382553
time_elpased: 1.65
batch start
#iterations: 21
currently lose_sum: 95.60622608661652
time_elpased: 1.612
batch start
#iterations: 22
currently lose_sum: 95.71754997968674
time_elpased: 1.595
batch start
#iterations: 23
currently lose_sum: 95.5428501367569
time_elpased: 1.585
batch start
#iterations: 24
currently lose_sum: 95.72517937421799
time_elpased: 1.595
batch start
#iterations: 25
currently lose_sum: 95.01531130075455
time_elpased: 1.589
batch start
#iterations: 26
currently lose_sum: 95.16427147388458
time_elpased: 1.567
batch start
#iterations: 27
currently lose_sum: 94.9717720746994
time_elpased: 1.589
batch start
#iterations: 28
currently lose_sum: 94.71590441465378
time_elpased: 1.576
batch start
#iterations: 29
currently lose_sum: 95.03263598680496
time_elpased: 1.575
batch start
#iterations: 30
currently lose_sum: 94.87908178567886
time_elpased: 1.566
batch start
#iterations: 31
currently lose_sum: 94.38346379995346
time_elpased: 1.573
batch start
#iterations: 32
currently lose_sum: 94.13636213541031
time_elpased: 1.588
batch start
#iterations: 33
currently lose_sum: 94.24302953481674
time_elpased: 1.649
batch start
#iterations: 34
currently lose_sum: 94.44483810663223
time_elpased: 1.564
batch start
#iterations: 35
currently lose_sum: 94.14943426847458
time_elpased: 1.585
batch start
#iterations: 36
currently lose_sum: 94.45729184150696
time_elpased: 1.55
batch start
#iterations: 37
currently lose_sum: 94.087018430233
time_elpased: 1.58
batch start
#iterations: 38
currently lose_sum: 94.16347289085388
time_elpased: 1.58
batch start
#iterations: 39
currently lose_sum: 93.41586947441101
time_elpased: 1.569
start validation test
0.665309278351
0.645068394528
0.737676237522
0.688271160401
0.665182226992
59.863
batch start
#iterations: 40
currently lose_sum: 93.73092901706696
time_elpased: 1.577
batch start
#iterations: 41
currently lose_sum: 93.4856349825859
time_elpased: 1.597
batch start
#iterations: 42
currently lose_sum: 93.46047055721283
time_elpased: 1.599
batch start
#iterations: 43
currently lose_sum: 93.44446820020676
time_elpased: 1.576
batch start
#iterations: 44
currently lose_sum: 93.21867227554321
time_elpased: 1.568
batch start
#iterations: 45
currently lose_sum: 93.32657051086426
time_elpased: 1.557
batch start
#iterations: 46
currently lose_sum: 92.81445199251175
time_elpased: 1.612
batch start
#iterations: 47
currently lose_sum: 93.4217699766159
time_elpased: 1.575
batch start
#iterations: 48
currently lose_sum: 93.42829805612564
time_elpased: 1.592
batch start
#iterations: 49
currently lose_sum: 93.19995045661926
time_elpased: 1.585
batch start
#iterations: 50
currently lose_sum: 93.32336068153381
time_elpased: 1.611
batch start
#iterations: 51
currently lose_sum: 92.93630945682526
time_elpased: 1.562
batch start
#iterations: 52
currently lose_sum: 92.97327846288681
time_elpased: 1.588
batch start
#iterations: 53
currently lose_sum: 92.74202489852905
time_elpased: 1.583
batch start
#iterations: 54
currently lose_sum: 92.96452170610428
time_elpased: 1.584
batch start
#iterations: 55
currently lose_sum: 92.74977642297745
time_elpased: 1.606
batch start
#iterations: 56
currently lose_sum: 92.32673913240433
time_elpased: 1.576
batch start
#iterations: 57
currently lose_sum: 92.25356823205948
time_elpased: 1.636
batch start
#iterations: 58
currently lose_sum: 92.40211933851242
time_elpased: 1.552
batch start
#iterations: 59
currently lose_sum: 91.81164073944092
time_elpased: 1.585
start validation test
0.664948453608
0.633076859436
0.787485849542
0.701889561548
0.66473332031
59.428
batch start
#iterations: 60
currently lose_sum: 92.40928375720978
time_elpased: 1.592
batch start
#iterations: 61
currently lose_sum: 91.82964551448822
time_elpased: 1.609
batch start
#iterations: 62
currently lose_sum: 91.74186092615128
time_elpased: 1.6
batch start
#iterations: 63
currently lose_sum: 92.14107865095139
time_elpased: 1.619
batch start
#iterations: 64
currently lose_sum: 91.66431629657745
time_elpased: 1.553
batch start
#iterations: 65
currently lose_sum: 91.42842382192612
time_elpased: 1.599
batch start
#iterations: 66
currently lose_sum: 91.46458059549332
time_elpased: 1.633
batch start
#iterations: 67
currently lose_sum: 91.55868726968765
time_elpased: 1.585
batch start
#iterations: 68
currently lose_sum: 91.57483518123627
time_elpased: 1.605
batch start
#iterations: 69
currently lose_sum: 91.56519258022308
time_elpased: 1.566
batch start
#iterations: 70
currently lose_sum: 91.61046069860458
time_elpased: 1.577
batch start
#iterations: 71
currently lose_sum: 90.76053041219711
time_elpased: 1.622
batch start
#iterations: 72
currently lose_sum: 91.20497995615005
time_elpased: 1.595
batch start
#iterations: 73
currently lose_sum: 91.13002121448517
time_elpased: 1.617
batch start
#iterations: 74
currently lose_sum: 90.78855109214783
time_elpased: 1.597
batch start
#iterations: 75
currently lose_sum: 90.85102033615112
time_elpased: 1.598
batch start
#iterations: 76
currently lose_sum: 90.36287677288055
time_elpased: 1.607
batch start
#iterations: 77
currently lose_sum: 90.9154960513115
time_elpased: 1.6
batch start
#iterations: 78
currently lose_sum: 90.85722708702087
time_elpased: 1.6
batch start
#iterations: 79
currently lose_sum: 90.74531102180481
time_elpased: 1.578
start validation test
0.670824742268
0.64351572598
0.768549963981
0.700497139105
0.670653170568
58.439
batch start
#iterations: 80
currently lose_sum: 90.75254940986633
time_elpased: 1.588
batch start
#iterations: 81
currently lose_sum: 91.14060252904892
time_elpased: 1.58
batch start
#iterations: 82
currently lose_sum: 91.03326958417892
time_elpased: 1.61
batch start
#iterations: 83
currently lose_sum: 90.69997268915176
time_elpased: 1.63
batch start
#iterations: 84
currently lose_sum: 90.44993942975998
time_elpased: 1.603
batch start
#iterations: 85
currently lose_sum: 90.60038048028946
time_elpased: 1.622
batch start
#iterations: 86
currently lose_sum: 90.46974050998688
time_elpased: 1.571
batch start
#iterations: 87
currently lose_sum: 89.89011812210083
time_elpased: 1.597
batch start
#iterations: 88
currently lose_sum: 90.19739943742752
time_elpased: 1.584
batch start
#iterations: 89
currently lose_sum: 89.94433867931366
time_elpased: 1.646
batch start
#iterations: 90
currently lose_sum: 90.32959926128387
time_elpased: 1.595
batch start
#iterations: 91
currently lose_sum: 90.49108266830444
time_elpased: 1.618
batch start
#iterations: 92
currently lose_sum: 89.91083019971848
time_elpased: 1.595
batch start
#iterations: 93
currently lose_sum: 90.46563988924026
time_elpased: 1.584
batch start
#iterations: 94
currently lose_sum: 89.98886781930923
time_elpased: 1.593
batch start
#iterations: 95
currently lose_sum: 90.21541434526443
time_elpased: 1.603
batch start
#iterations: 96
currently lose_sum: 90.10043174028397
time_elpased: 1.593
batch start
#iterations: 97
currently lose_sum: 89.74401783943176
time_elpased: 1.615
batch start
#iterations: 98
currently lose_sum: 89.74753856658936
time_elpased: 1.59
batch start
#iterations: 99
currently lose_sum: 89.47404009103775
time_elpased: 1.649
start validation test
0.682422680412
0.67115902965
0.717505402902
0.693558816215
0.682361087282
58.303
batch start
#iterations: 100
currently lose_sum: 90.01987290382385
time_elpased: 1.6
batch start
#iterations: 101
currently lose_sum: 89.35649234056473
time_elpased: 1.64
batch start
#iterations: 102
currently lose_sum: 89.47944408655167
time_elpased: 1.622
batch start
#iterations: 103
currently lose_sum: 89.34502059221268
time_elpased: 1.597
batch start
#iterations: 104
currently lose_sum: 89.89423078298569
time_elpased: 1.599
batch start
#iterations: 105
currently lose_sum: 89.10551339387894
time_elpased: 1.643
batch start
#iterations: 106
currently lose_sum: 89.86012530326843
time_elpased: 1.592
batch start
#iterations: 107
currently lose_sum: 89.07410770654678
time_elpased: 1.583
batch start
#iterations: 108
currently lose_sum: 89.52940738201141
time_elpased: 1.618
batch start
#iterations: 109
currently lose_sum: 89.2859976887703
time_elpased: 1.59
batch start
#iterations: 110
currently lose_sum: 88.88137972354889
time_elpased: 1.606
batch start
#iterations: 111
currently lose_sum: 89.03390628099442
time_elpased: 1.613
batch start
#iterations: 112
currently lose_sum: 88.73853898048401
time_elpased: 1.666
batch start
#iterations: 113
currently lose_sum: 88.72360736131668
time_elpased: 1.556
batch start
#iterations: 114
currently lose_sum: 89.09370905160904
time_elpased: 1.625
batch start
#iterations: 115
currently lose_sum: 88.4420952796936
time_elpased: 1.59
batch start
#iterations: 116
currently lose_sum: 88.83433383703232
time_elpased: 1.61
batch start
#iterations: 117
currently lose_sum: 88.81484788656235
time_elpased: 1.576
batch start
#iterations: 118
currently lose_sum: 88.98382765054703
time_elpased: 1.576
batch start
#iterations: 119
currently lose_sum: 88.6680850982666
time_elpased: 1.612
start validation test
0.674536082474
0.641827123448
0.792425645775
0.709219858156
0.674329109163
58.005
batch start
#iterations: 120
currently lose_sum: 88.66096103191376
time_elpased: 1.626
batch start
#iterations: 121
currently lose_sum: 88.86270540952682
time_elpased: 1.585
batch start
#iterations: 122
currently lose_sum: 88.40470147132874
time_elpased: 1.56
batch start
#iterations: 123
currently lose_sum: 88.52058017253876
time_elpased: 1.618
batch start
#iterations: 124
currently lose_sum: 88.3792833685875
time_elpased: 1.613
batch start
#iterations: 125
currently lose_sum: 88.91579705476761
time_elpased: 1.617
batch start
#iterations: 126
currently lose_sum: 88.13182145357132
time_elpased: 1.622
batch start
#iterations: 127
currently lose_sum: 88.4138954281807
time_elpased: 1.617
batch start
#iterations: 128
currently lose_sum: 88.53156226873398
time_elpased: 1.588
batch start
#iterations: 129
currently lose_sum: 87.35024815797806
time_elpased: 1.61
batch start
#iterations: 130
currently lose_sum: 87.90198159217834
time_elpased: 1.583
batch start
#iterations: 131
currently lose_sum: 87.68650579452515
time_elpased: 1.587
batch start
#iterations: 132
currently lose_sum: 87.76612669229507
time_elpased: 1.616
batch start
#iterations: 133
currently lose_sum: 88.26923090219498
time_elpased: 1.553
batch start
#iterations: 134
currently lose_sum: 87.61171358823776
time_elpased: 1.606
batch start
#iterations: 135
currently lose_sum: 88.39631998538971
time_elpased: 1.579
batch start
#iterations: 136
currently lose_sum: 87.2344451546669
time_elpased: 1.599
batch start
#iterations: 137
currently lose_sum: 88.28700685501099
time_elpased: 1.652
batch start
#iterations: 138
currently lose_sum: 87.61479753255844
time_elpased: 1.622
batch start
#iterations: 139
currently lose_sum: 87.46019262075424
time_elpased: 1.568
start validation test
0.67381443299
0.66411622276
0.705670474426
0.6842630476
0.673758504795
58.074
batch start
#iterations: 140
currently lose_sum: 87.86265271902084
time_elpased: 1.602
batch start
#iterations: 141
currently lose_sum: 88.21449106931686
time_elpased: 1.6
batch start
#iterations: 142
currently lose_sum: 87.22953337430954
time_elpased: 1.576
batch start
#iterations: 143
currently lose_sum: 88.04033774137497
time_elpased: 1.637
batch start
#iterations: 144
currently lose_sum: 87.81045073270798
time_elpased: 1.599
batch start
#iterations: 145
currently lose_sum: 87.56921166181564
time_elpased: 1.547
batch start
#iterations: 146
currently lose_sum: 87.53273779153824
time_elpased: 1.621
batch start
#iterations: 147
currently lose_sum: 87.3626778125763
time_elpased: 1.617
batch start
#iterations: 148
currently lose_sum: 87.35714143514633
time_elpased: 1.621
batch start
#iterations: 149
currently lose_sum: 87.57459592819214
time_elpased: 1.605
batch start
#iterations: 150
currently lose_sum: 86.92632216215134
time_elpased: 1.607
batch start
#iterations: 151
currently lose_sum: 87.09933984279633
time_elpased: 1.594
batch start
#iterations: 152
currently lose_sum: 87.42010456323624
time_elpased: 1.603
batch start
#iterations: 153
currently lose_sum: 87.75809055566788
time_elpased: 1.605
batch start
#iterations: 154
currently lose_sum: 87.29046678543091
time_elpased: 1.586
batch start
#iterations: 155
currently lose_sum: 86.98242259025574
time_elpased: 1.601
batch start
#iterations: 156
currently lose_sum: 86.95510542392731
time_elpased: 1.636
batch start
#iterations: 157
currently lose_sum: 87.4150675535202
time_elpased: 1.63
batch start
#iterations: 158
currently lose_sum: 87.42233377695084
time_elpased: 1.616
batch start
#iterations: 159
currently lose_sum: 87.55112260580063
time_elpased: 1.586
start validation test
0.689020618557
0.67949717404
0.717608315324
0.698032934581
0.688970428446
58.058
batch start
#iterations: 160
currently lose_sum: 86.69489032030106
time_elpased: 1.599
batch start
#iterations: 161
currently lose_sum: 86.91702777147293
time_elpased: 1.611
batch start
#iterations: 162
currently lose_sum: 86.54884320497513
time_elpased: 1.564
batch start
#iterations: 163
currently lose_sum: 86.64616894721985
time_elpased: 1.617
batch start
#iterations: 164
currently lose_sum: 86.80438458919525
time_elpased: 1.644
batch start
#iterations: 165
currently lose_sum: 86.92849564552307
time_elpased: 1.617
batch start
#iterations: 166
currently lose_sum: 86.7168333530426
time_elpased: 1.583
batch start
#iterations: 167
currently lose_sum: 86.8184968829155
time_elpased: 1.596
batch start
#iterations: 168
currently lose_sum: 86.16409975290298
time_elpased: 1.597
batch start
#iterations: 169
currently lose_sum: 86.27966970205307
time_elpased: 1.595
batch start
#iterations: 170
currently lose_sum: 86.47663879394531
time_elpased: 1.589
batch start
#iterations: 171
currently lose_sum: 86.80037206411362
time_elpased: 1.611
batch start
#iterations: 172
currently lose_sum: 86.77075237035751
time_elpased: 1.598
batch start
#iterations: 173
currently lose_sum: 86.12498986721039
time_elpased: 1.665
batch start
#iterations: 174
currently lose_sum: 86.50743836164474
time_elpased: 1.571
batch start
#iterations: 175
currently lose_sum: 86.24486988782883
time_elpased: 1.62
batch start
#iterations: 176
currently lose_sum: 86.6375784277916
time_elpased: 1.564
batch start
#iterations: 177
currently lose_sum: 85.76260131597519
time_elpased: 1.594
batch start
#iterations: 178
currently lose_sum: 85.69038873910904
time_elpased: 1.599
batch start
#iterations: 179
currently lose_sum: 86.21880900859833
time_elpased: 1.611
start validation test
0.662680412371
0.657125878974
0.682823916847
0.669728474816
0.662645047342
59.046
batch start
#iterations: 180
currently lose_sum: 86.31305891275406
time_elpased: 1.64
batch start
#iterations: 181
currently lose_sum: 86.0607488155365
time_elpased: 1.667
batch start
#iterations: 182
currently lose_sum: 85.93346279859543
time_elpased: 1.59
batch start
#iterations: 183
currently lose_sum: 85.21759009361267
time_elpased: 1.573
batch start
#iterations: 184
currently lose_sum: 85.9010780453682
time_elpased: 1.585
batch start
#iterations: 185
currently lose_sum: 86.24731355905533
time_elpased: 1.617
batch start
#iterations: 186
currently lose_sum: 85.78790217638016
time_elpased: 1.566
batch start
#iterations: 187
currently lose_sum: 85.64620476961136
time_elpased: 1.573
batch start
#iterations: 188
currently lose_sum: 85.81492877006531
time_elpased: 1.567
batch start
#iterations: 189
currently lose_sum: 85.9021046757698
time_elpased: 1.609
batch start
#iterations: 190
currently lose_sum: 86.04463756084442
time_elpased: 1.571
batch start
#iterations: 191
currently lose_sum: 85.56739062070847
time_elpased: 1.581
batch start
#iterations: 192
currently lose_sum: 85.87856632471085
time_elpased: 1.633
batch start
#iterations: 193
currently lose_sum: 85.45491832494736
time_elpased: 1.584
batch start
#iterations: 194
currently lose_sum: 85.79090565443039
time_elpased: 1.642
batch start
#iterations: 195
currently lose_sum: 85.62730538845062
time_elpased: 1.599
batch start
#iterations: 196
currently lose_sum: 86.07419675588608
time_elpased: 1.599
batch start
#iterations: 197
currently lose_sum: 85.42438399791718
time_elpased: 1.586
batch start
#iterations: 198
currently lose_sum: 85.34602999687195
time_elpased: 1.584
batch start
#iterations: 199
currently lose_sum: 84.92510557174683
time_elpased: 1.577
start validation test
0.668298969072
0.663576555024
0.68508799012
0.674160716998
0.668269493356
59.404
batch start
#iterations: 200
currently lose_sum: 85.14436227083206
time_elpased: 1.568
batch start
#iterations: 201
currently lose_sum: 84.82974302768707
time_elpased: 1.602
batch start
#iterations: 202
currently lose_sum: 85.05335015058517
time_elpased: 1.574
batch start
#iterations: 203
currently lose_sum: 85.29966938495636
time_elpased: 1.662
batch start
#iterations: 204
currently lose_sum: 85.43643498420715
time_elpased: 1.594
batch start
#iterations: 205
currently lose_sum: 84.97577112913132
time_elpased: 1.592
batch start
#iterations: 206
currently lose_sum: 85.40928024053574
time_elpased: 1.646
batch start
#iterations: 207
currently lose_sum: 85.20871615409851
time_elpased: 1.579
batch start
#iterations: 208
currently lose_sum: 84.60668212175369
time_elpased: 1.593
batch start
#iterations: 209
currently lose_sum: 84.4876977801323
time_elpased: 1.633
batch start
#iterations: 210
currently lose_sum: 85.22125959396362
time_elpased: 1.594
batch start
#iterations: 211
currently lose_sum: 85.07579219341278
time_elpased: 1.57
batch start
#iterations: 212
currently lose_sum: 85.00192975997925
time_elpased: 1.59
batch start
#iterations: 213
currently lose_sum: 84.86229610443115
time_elpased: 1.623
batch start
#iterations: 214
currently lose_sum: 84.52465003728867
time_elpased: 1.584
batch start
#iterations: 215
currently lose_sum: 85.05448395013809
time_elpased: 1.598
batch start
#iterations: 216
currently lose_sum: 84.08771181106567
time_elpased: 1.606
batch start
#iterations: 217
currently lose_sum: 84.84213840961456
time_elpased: 1.599
batch start
#iterations: 218
currently lose_sum: 85.12024503946304
time_elpased: 1.599
batch start
#iterations: 219
currently lose_sum: 84.98939090967178
time_elpased: 1.57
start validation test
0.666855670103
0.660232420721
0.689924873932
0.674752151376
0.666815168558
59.139
batch start
#iterations: 220
currently lose_sum: 84.53725975751877
time_elpased: 1.589
batch start
#iterations: 221
currently lose_sum: 83.57775634527206
time_elpased: 1.623
batch start
#iterations: 222
currently lose_sum: 84.66341435909271
time_elpased: 1.593
batch start
#iterations: 223
currently lose_sum: 84.15423485636711
time_elpased: 1.612
batch start
#iterations: 224
currently lose_sum: 84.33798331022263
time_elpased: 1.579
batch start
#iterations: 225
currently lose_sum: 84.4390200972557
time_elpased: 1.607
batch start
#iterations: 226
currently lose_sum: 84.24340164661407
time_elpased: 1.568
batch start
#iterations: 227
currently lose_sum: 84.32449519634247
time_elpased: 1.583
batch start
#iterations: 228
currently lose_sum: 83.67311573028564
time_elpased: 1.607
batch start
#iterations: 229
currently lose_sum: 83.71442252397537
time_elpased: 1.605
batch start
#iterations: 230
currently lose_sum: 84.45068222284317
time_elpased: 1.6
batch start
#iterations: 231
currently lose_sum: 84.77089148759842
time_elpased: 1.616
batch start
#iterations: 232
currently lose_sum: 83.92877984046936
time_elpased: 1.596
batch start
#iterations: 233
currently lose_sum: 84.1164156794548
time_elpased: 1.602
batch start
#iterations: 234
currently lose_sum: 84.20539438724518
time_elpased: 1.626
batch start
#iterations: 235
currently lose_sum: 84.4375963807106
time_elpased: 1.614
batch start
#iterations: 236
currently lose_sum: 83.78394120931625
time_elpased: 1.592
batch start
#iterations: 237
currently lose_sum: 83.86795777082443
time_elpased: 1.594
batch start
#iterations: 238
currently lose_sum: 83.89394527673721
time_elpased: 1.595
batch start
#iterations: 239
currently lose_sum: 84.10000330209732
time_elpased: 1.584
start validation test
0.665979381443
0.669423217837
0.65812493568
0.663725998962
0.665993171134
60.355
batch start
#iterations: 240
currently lose_sum: 83.79347747564316
time_elpased: 1.589
batch start
#iterations: 241
currently lose_sum: 83.77378484606743
time_elpased: 1.583
batch start
#iterations: 242
currently lose_sum: 83.5010957121849
time_elpased: 1.614
batch start
#iterations: 243
currently lose_sum: 83.01509037613869
time_elpased: 1.583
batch start
#iterations: 244
currently lose_sum: 83.56998842954636
time_elpased: 1.578
batch start
#iterations: 245
currently lose_sum: 83.8693958222866
time_elpased: 1.606
batch start
#iterations: 246
currently lose_sum: 83.06219100952148
time_elpased: 1.595
batch start
#iterations: 247
currently lose_sum: 83.53112930059433
time_elpased: 1.599
batch start
#iterations: 248
currently lose_sum: 83.24114418029785
time_elpased: 1.582
batch start
#iterations: 249
currently lose_sum: 83.22651052474976
time_elpased: 1.592
batch start
#iterations: 250
currently lose_sum: 84.08886057138443
time_elpased: 1.615
batch start
#iterations: 251
currently lose_sum: 83.3352906703949
time_elpased: 1.57
batch start
#iterations: 252
currently lose_sum: 83.31246030330658
time_elpased: 1.58
batch start
#iterations: 253
currently lose_sum: 82.87133172154427
time_elpased: 1.578
batch start
#iterations: 254
currently lose_sum: 83.4382511973381
time_elpased: 1.582
batch start
#iterations: 255
currently lose_sum: 83.0741017460823
time_elpased: 1.549
batch start
#iterations: 256
currently lose_sum: 83.63789653778076
time_elpased: 1.601
batch start
#iterations: 257
currently lose_sum: 82.9228064417839
time_elpased: 1.573
batch start
#iterations: 258
currently lose_sum: 83.00296732783318
time_elpased: 1.64
batch start
#iterations: 259
currently lose_sum: 83.00853782892227
time_elpased: 1.575
start validation test
0.679381443299
0.6724188936
0.701759802408
0.686776110384
0.679342154638
61.285
batch start
#iterations: 260
currently lose_sum: 83.24930074810982
time_elpased: 1.597
batch start
#iterations: 261
currently lose_sum: 83.15710282325745
time_elpased: 1.651
batch start
#iterations: 262
currently lose_sum: 83.0587100982666
time_elpased: 1.599
batch start
#iterations: 263
currently lose_sum: 82.4117061495781
time_elpased: 1.583
batch start
#iterations: 264
currently lose_sum: 82.50952011346817
time_elpased: 1.593
batch start
#iterations: 265
currently lose_sum: 82.67062187194824
time_elpased: 1.589
batch start
#iterations: 266
currently lose_sum: 82.96037352085114
time_elpased: 1.606
batch start
#iterations: 267
currently lose_sum: 83.03150475025177
time_elpased: 1.588
batch start
#iterations: 268
currently lose_sum: 82.10516291856766
time_elpased: 1.585
batch start
#iterations: 269
currently lose_sum: 82.20944619178772
time_elpased: 1.634
batch start
#iterations: 270
currently lose_sum: 81.97507035732269
time_elpased: 1.576
batch start
#iterations: 271
currently lose_sum: 82.66833901405334
time_elpased: 1.584
batch start
#iterations: 272
currently lose_sum: 83.01219129562378
time_elpased: 1.66
batch start
#iterations: 273
currently lose_sum: 82.29739886522293
time_elpased: 1.593
batch start
#iterations: 274
currently lose_sum: 82.60641929507256
time_elpased: 1.601
batch start
#iterations: 275
currently lose_sum: 82.58604165911674
time_elpased: 1.62
batch start
#iterations: 276
currently lose_sum: 81.68670830130577
time_elpased: 1.609
batch start
#iterations: 277
currently lose_sum: 82.47588610649109
time_elpased: 1.587
batch start
#iterations: 278
currently lose_sum: 82.28140074014664
time_elpased: 1.604
batch start
#iterations: 279
currently lose_sum: 82.4651507139206
time_elpased: 1.576
start validation test
0.674536082474
0.666016196702
0.702480189359
0.683762396073
0.674487022285
61.441
batch start
#iterations: 280
currently lose_sum: 81.56987875699997
time_elpased: 1.563
batch start
#iterations: 281
currently lose_sum: 82.10587647557259
time_elpased: 1.632
batch start
#iterations: 282
currently lose_sum: 81.99080485105515
time_elpased: 1.591
batch start
#iterations: 283
currently lose_sum: 81.76802536845207
time_elpased: 1.609
batch start
#iterations: 284
currently lose_sum: 82.17033666372299
time_elpased: 1.593
batch start
#iterations: 285
currently lose_sum: 81.96536976099014
time_elpased: 1.6
batch start
#iterations: 286
currently lose_sum: 81.80233231186867
time_elpased: 1.626
batch start
#iterations: 287
currently lose_sum: 81.52760076522827
time_elpased: 1.592
batch start
#iterations: 288
currently lose_sum: 81.35513308644295
time_elpased: 1.618
batch start
#iterations: 289
currently lose_sum: 81.66031229496002
time_elpased: 1.606
batch start
#iterations: 290
currently lose_sum: 81.87906008958817
time_elpased: 1.635
batch start
#iterations: 291
currently lose_sum: 81.76573440432549
time_elpased: 1.576
batch start
#iterations: 292
currently lose_sum: 81.47581794857979
time_elpased: 1.579
batch start
#iterations: 293
currently lose_sum: 81.64541926980019
time_elpased: 1.614
batch start
#iterations: 294
currently lose_sum: 81.18431410193443
time_elpased: 1.56
batch start
#iterations: 295
currently lose_sum: 81.41599768400192
time_elpased: 1.603
batch start
#iterations: 296
currently lose_sum: 81.5131873190403
time_elpased: 1.57
batch start
#iterations: 297
currently lose_sum: 80.92017385363579
time_elpased: 1.583
batch start
#iterations: 298
currently lose_sum: 81.82045128941536
time_elpased: 1.603
batch start
#iterations: 299
currently lose_sum: 81.50613856315613
time_elpased: 1.593
start validation test
0.665618556701
0.671407344513
0.651023978594
0.661058571503
0.665644179734
64.830
batch start
#iterations: 300
currently lose_sum: 81.3125196993351
time_elpased: 1.575
batch start
#iterations: 301
currently lose_sum: 81.25899225473404
time_elpased: 1.576
batch start
#iterations: 302
currently lose_sum: 80.7726241350174
time_elpased: 1.593
batch start
#iterations: 303
currently lose_sum: 81.26530337333679
time_elpased: 1.613
batch start
#iterations: 304
currently lose_sum: 81.33671009540558
time_elpased: 1.6
batch start
#iterations: 305
currently lose_sum: 81.07253113389015
time_elpased: 1.559
batch start
#iterations: 306
currently lose_sum: 80.89800724387169
time_elpased: 1.632
batch start
#iterations: 307
currently lose_sum: 80.69832682609558
time_elpased: 1.607
batch start
#iterations: 308
currently lose_sum: 81.27614277601242
time_elpased: 1.594
batch start
#iterations: 309
currently lose_sum: 81.53457242250443
time_elpased: 1.62
batch start
#iterations: 310
currently lose_sum: 80.90764057636261
time_elpased: 1.603
batch start
#iterations: 311
currently lose_sum: 80.70500794053078
time_elpased: 1.583
batch start
#iterations: 312
currently lose_sum: 80.43150147795677
time_elpased: 1.595
batch start
#iterations: 313
currently lose_sum: 80.2812904715538
time_elpased: 1.609
batch start
#iterations: 314
currently lose_sum: 81.34079086780548
time_elpased: 1.624
batch start
#iterations: 315
currently lose_sum: 80.5065889954567
time_elpased: 1.587
batch start
#iterations: 316
currently lose_sum: 80.67211723327637
time_elpased: 1.621
batch start
#iterations: 317
currently lose_sum: 81.03155452013016
time_elpased: 1.578
batch start
#iterations: 318
currently lose_sum: 80.11130219697952
time_elpased: 1.607
batch start
#iterations: 319
currently lose_sum: 79.9146853685379
time_elpased: 1.629
start validation test
0.66206185567
0.660375443937
0.669754039313
0.665031677907
0.662048350855
64.754
batch start
#iterations: 320
currently lose_sum: 81.44746345281601
time_elpased: 1.656
batch start
#iterations: 321
currently lose_sum: 80.43503481149673
time_elpased: 1.634
batch start
#iterations: 322
currently lose_sum: 80.31663200259209
time_elpased: 1.634
batch start
#iterations: 323
currently lose_sum: 80.22197845578194
time_elpased: 1.593
batch start
#iterations: 324
currently lose_sum: 80.15941399335861
time_elpased: 1.643
batch start
#iterations: 325
currently lose_sum: 80.2611882686615
time_elpased: 1.633
batch start
#iterations: 326
currently lose_sum: 79.33130005002022
time_elpased: 1.612
batch start
#iterations: 327
currently lose_sum: 80.13138672709465
time_elpased: 1.587
batch start
#iterations: 328
currently lose_sum: 80.159822255373
time_elpased: 1.582
batch start
#iterations: 329
currently lose_sum: 80.45325508713722
time_elpased: 1.6
batch start
#iterations: 330
currently lose_sum: 80.04133692383766
time_elpased: 1.582
batch start
#iterations: 331
currently lose_sum: 79.91114622354507
time_elpased: 1.619
batch start
#iterations: 332
currently lose_sum: 79.49089089035988
time_elpased: 1.606
batch start
#iterations: 333
currently lose_sum: 79.11169025301933
time_elpased: 1.63
batch start
#iterations: 334
currently lose_sum: 79.3839420080185
time_elpased: 1.625
batch start
#iterations: 335
currently lose_sum: 79.52116620540619
time_elpased: 1.592
batch start
#iterations: 336
currently lose_sum: 79.73723423480988
time_elpased: 1.646
batch start
#iterations: 337
currently lose_sum: 79.93311542272568
time_elpased: 1.601
batch start
#iterations: 338
currently lose_sum: 79.60514387488365
time_elpased: 1.637
batch start
#iterations: 339
currently lose_sum: 79.81061962246895
time_elpased: 1.602
start validation test
0.660206185567
0.660207115759
0.662653082227
0.661427837699
0.660201889663
66.372
batch start
#iterations: 340
currently lose_sum: 79.34163835644722
time_elpased: 1.604
batch start
#iterations: 341
currently lose_sum: 79.86044928431511
time_elpased: 1.635
batch start
#iterations: 342
currently lose_sum: 79.68409487605095
time_elpased: 1.647
batch start
#iterations: 343
currently lose_sum: 78.69531518220901
time_elpased: 1.581
batch start
#iterations: 344
currently lose_sum: 79.11774763464928
time_elpased: 1.607
batch start
#iterations: 345
currently lose_sum: 79.15023949742317
time_elpased: 1.576
batch start
#iterations: 346
currently lose_sum: 78.83467850089073
time_elpased: 1.598
batch start
#iterations: 347
currently lose_sum: 79.53904730081558
time_elpased: 1.582
batch start
#iterations: 348
currently lose_sum: 79.18992033600807
time_elpased: 1.582
batch start
#iterations: 349
currently lose_sum: 79.60061025619507
time_elpased: 1.662
batch start
#iterations: 350
currently lose_sum: 78.73781633377075
time_elpased: 1.568
batch start
#iterations: 351
currently lose_sum: 79.0840068757534
time_elpased: 1.567
batch start
#iterations: 352
currently lose_sum: 79.53007590770721
time_elpased: 1.587
batch start
#iterations: 353
currently lose_sum: 79.01800313591957
time_elpased: 1.601
batch start
#iterations: 354
currently lose_sum: 78.88678485155106
time_elpased: 1.568
batch start
#iterations: 355
currently lose_sum: 79.03751602768898
time_elpased: 1.639
batch start
#iterations: 356
currently lose_sum: 78.59433737397194
time_elpased: 1.599
batch start
#iterations: 357
currently lose_sum: 78.77415400743484
time_elpased: 1.601
batch start
#iterations: 358
currently lose_sum: 78.72063583135605
time_elpased: 1.564
batch start
#iterations: 359
currently lose_sum: 78.92766353487968
time_elpased: 1.596
start validation test
0.649639175258
0.657633340531
0.626839559535
0.641867327046
0.6496792035
68.000
batch start
#iterations: 360
currently lose_sum: 78.84651777148247
time_elpased: 1.568
batch start
#iterations: 361
currently lose_sum: 78.9339509010315
time_elpased: 1.627
batch start
#iterations: 362
currently lose_sum: 78.62922358512878
time_elpased: 1.588
batch start
#iterations: 363
currently lose_sum: 78.42676183581352
time_elpased: 1.593
batch start
#iterations: 364
currently lose_sum: 78.76475405693054
time_elpased: 1.636
batch start
#iterations: 365
currently lose_sum: 78.34888532757759
time_elpased: 1.57
batch start
#iterations: 366
currently lose_sum: 78.57352817058563
time_elpased: 1.579
batch start
#iterations: 367
currently lose_sum: 78.84363457560539
time_elpased: 1.582
batch start
#iterations: 368
currently lose_sum: 78.30519449710846
time_elpased: 1.622
batch start
#iterations: 369
currently lose_sum: 78.2997895181179
time_elpased: 1.642
batch start
#iterations: 370
currently lose_sum: 77.98716005682945
time_elpased: 1.574
batch start
#iterations: 371
currently lose_sum: 78.05178850889206
time_elpased: 1.595
batch start
#iterations: 372
currently lose_sum: 78.56816378235817
time_elpased: 1.611
batch start
#iterations: 373
currently lose_sum: 78.1713415980339
time_elpased: 1.609
batch start
#iterations: 374
currently lose_sum: 77.87692701816559
time_elpased: 1.584
batch start
#iterations: 375
currently lose_sum: 78.33548179268837
time_elpased: 1.591
batch start
#iterations: 376
currently lose_sum: 77.77103447914124
time_elpased: 1.64
batch start
#iterations: 377
currently lose_sum: 77.83721914887428
time_elpased: 1.63
batch start
#iterations: 378
currently lose_sum: 78.43114602565765
time_elpased: 1.629
batch start
#iterations: 379
currently lose_sum: 77.89582228660583
time_elpased: 1.606
start validation test
0.64175257732
0.642643571502
0.641453123392
0.642047795632
0.641753103057
67.607
batch start
#iterations: 380
currently lose_sum: 78.20087203383446
time_elpased: 1.581
batch start
#iterations: 381
currently lose_sum: 77.59837731719017
time_elpased: 1.592
batch start
#iterations: 382
currently lose_sum: 77.72963082790375
time_elpased: 1.578
batch start
#iterations: 383
currently lose_sum: 77.76210427284241
time_elpased: 1.618
batch start
#iterations: 384
currently lose_sum: 76.90660384297371
time_elpased: 1.573
batch start
#iterations: 385
currently lose_sum: 77.74145385622978
time_elpased: 1.638
batch start
#iterations: 386
currently lose_sum: 77.99386641383171
time_elpased: 1.583
batch start
#iterations: 387
currently lose_sum: 77.45454502105713
time_elpased: 1.544
batch start
#iterations: 388
currently lose_sum: 77.25467881560326
time_elpased: 1.638
batch start
#iterations: 389
currently lose_sum: 77.20377671718597
time_elpased: 1.583
batch start
#iterations: 390
currently lose_sum: 77.54123583436012
time_elpased: 1.6
batch start
#iterations: 391
currently lose_sum: 77.17244705557823
time_elpased: 1.596
batch start
#iterations: 392
currently lose_sum: 77.0611882507801
time_elpased: 1.581
batch start
#iterations: 393
currently lose_sum: 76.99151510000229
time_elpased: 1.589
batch start
#iterations: 394
currently lose_sum: 77.5741359591484
time_elpased: 1.561
batch start
#iterations: 395
currently lose_sum: 77.10828486084938
time_elpased: 1.573
batch start
#iterations: 396
currently lose_sum: 76.6574854850769
time_elpased: 1.589
batch start
#iterations: 397
currently lose_sum: 77.32076838612556
time_elpased: 1.572
batch start
#iterations: 398
currently lose_sum: 76.72554457187653
time_elpased: 1.567
batch start
#iterations: 399
currently lose_sum: 77.5630002617836
time_elpased: 1.623
start validation test
0.645309278351
0.657485561972
0.609241535453
0.632444848032
0.645372600836
71.155
acc: 0.670
pre: 0.638
rec: 0.791
F1: 0.706
auc: 0.670
