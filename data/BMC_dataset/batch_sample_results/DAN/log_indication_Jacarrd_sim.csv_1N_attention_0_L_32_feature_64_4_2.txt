start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.13390827178955
time_elpased: 1.784
batch start
#iterations: 1
currently lose_sum: 99.79917562007904
time_elpased: 1.754
batch start
#iterations: 2
currently lose_sum: 99.49775171279907
time_elpased: 1.754
batch start
#iterations: 3
currently lose_sum: 98.86861193180084
time_elpased: 1.786
batch start
#iterations: 4
currently lose_sum: 98.53839111328125
time_elpased: 1.685
batch start
#iterations: 5
currently lose_sum: 98.3193524479866
time_elpased: 1.762
batch start
#iterations: 6
currently lose_sum: 97.96556860208511
time_elpased: 1.729
batch start
#iterations: 7
currently lose_sum: 97.93740409612656
time_elpased: 1.726
batch start
#iterations: 8
currently lose_sum: 97.76238411664963
time_elpased: 1.74
batch start
#iterations: 9
currently lose_sum: 97.55603909492493
time_elpased: 1.723
batch start
#iterations: 10
currently lose_sum: 97.06483083963394
time_elpased: 1.738
batch start
#iterations: 11
currently lose_sum: 97.46760183572769
time_elpased: 1.749
batch start
#iterations: 12
currently lose_sum: 97.21558803319931
time_elpased: 1.733
batch start
#iterations: 13
currently lose_sum: 97.13337028026581
time_elpased: 1.78
batch start
#iterations: 14
currently lose_sum: 96.78188574314117
time_elpased: 1.738
batch start
#iterations: 15
currently lose_sum: 96.81136125326157
time_elpased: 1.779
batch start
#iterations: 16
currently lose_sum: 96.83718967437744
time_elpased: 1.731
batch start
#iterations: 17
currently lose_sum: 96.5861896276474
time_elpased: 1.736
batch start
#iterations: 18
currently lose_sum: 96.70236700773239
time_elpased: 1.718
batch start
#iterations: 19
currently lose_sum: 96.56679785251617
time_elpased: 1.75
start validation test
0.625206185567
0.603855298913
0.731810229495
0.661703810543
0.625019025725
62.765
batch start
#iterations: 20
currently lose_sum: 96.57200580835342
time_elpased: 1.735
batch start
#iterations: 21
currently lose_sum: 96.405681848526
time_elpased: 1.738
batch start
#iterations: 22
currently lose_sum: 96.10768634080887
time_elpased: 1.733
batch start
#iterations: 23
currently lose_sum: 95.977873980999
time_elpased: 1.749
batch start
#iterations: 24
currently lose_sum: 96.04575026035309
time_elpased: 1.738
batch start
#iterations: 25
currently lose_sum: 96.3004754781723
time_elpased: 1.775
batch start
#iterations: 26
currently lose_sum: 95.96962732076645
time_elpased: 1.779
batch start
#iterations: 27
currently lose_sum: 96.03947645425797
time_elpased: 1.741
batch start
#iterations: 28
currently lose_sum: 95.74985927343369
time_elpased: 1.784
batch start
#iterations: 29
currently lose_sum: 96.0777724981308
time_elpased: 1.808
batch start
#iterations: 30
currently lose_sum: 95.57285118103027
time_elpased: 1.742
batch start
#iterations: 31
currently lose_sum: 95.2366002202034
time_elpased: 1.761
batch start
#iterations: 32
currently lose_sum: 95.0174167752266
time_elpased: 1.748
batch start
#iterations: 33
currently lose_sum: 95.22036373615265
time_elpased: 1.73
batch start
#iterations: 34
currently lose_sum: 94.62972402572632
time_elpased: 1.731
batch start
#iterations: 35
currently lose_sum: 94.83456712961197
time_elpased: 1.774
batch start
#iterations: 36
currently lose_sum: 94.35499793291092
time_elpased: 1.758
batch start
#iterations: 37
currently lose_sum: 94.50852757692337
time_elpased: 1.785
batch start
#iterations: 38
currently lose_sum: 94.69523376226425
time_elpased: 1.771
batch start
#iterations: 39
currently lose_sum: 94.35157924890518
time_elpased: 1.733
start validation test
0.670979381443
0.640960595298
0.780076155192
0.703708861347
0.67078784523
59.694
batch start
#iterations: 40
currently lose_sum: 94.15144383907318
time_elpased: 1.729
batch start
#iterations: 41
currently lose_sum: 94.26488125324249
time_elpased: 1.768
batch start
#iterations: 42
currently lose_sum: 93.76814895868301
time_elpased: 1.713
batch start
#iterations: 43
currently lose_sum: 93.8257160782814
time_elpased: 1.787
batch start
#iterations: 44
currently lose_sum: 93.64390379190445
time_elpased: 1.74
batch start
#iterations: 45
currently lose_sum: 93.58255970478058
time_elpased: 1.813
batch start
#iterations: 46
currently lose_sum: 93.2960798740387
time_elpased: 1.768
batch start
#iterations: 47
currently lose_sum: 93.45123380422592
time_elpased: 1.702
batch start
#iterations: 48
currently lose_sum: 93.42921841144562
time_elpased: 1.755
batch start
#iterations: 49
currently lose_sum: 93.54116117954254
time_elpased: 1.823
batch start
#iterations: 50
currently lose_sum: 93.24091225862503
time_elpased: 1.712
batch start
#iterations: 51
currently lose_sum: 92.92462915182114
time_elpased: 1.731
batch start
#iterations: 52
currently lose_sum: 93.12533462047577
time_elpased: 1.812
batch start
#iterations: 53
currently lose_sum: 92.68064022064209
time_elpased: 1.8
batch start
#iterations: 54
currently lose_sum: 92.75255590677261
time_elpased: 1.742
batch start
#iterations: 55
currently lose_sum: 92.76094025373459
time_elpased: 1.757
batch start
#iterations: 56
currently lose_sum: 93.17157024145126
time_elpased: 1.745
batch start
#iterations: 57
currently lose_sum: 92.84217309951782
time_elpased: 1.762
batch start
#iterations: 58
currently lose_sum: 92.62129640579224
time_elpased: 1.794
batch start
#iterations: 59
currently lose_sum: 92.32325446605682
time_elpased: 1.798
start validation test
0.653917525773
0.640866873065
0.702994751467
0.670494699647
0.653831363134
59.677
batch start
#iterations: 60
currently lose_sum: 92.3603932261467
time_elpased: 1.76
batch start
#iterations: 61
currently lose_sum: 92.5004671216011
time_elpased: 1.759
batch start
#iterations: 62
currently lose_sum: 92.54856526851654
time_elpased: 1.721
batch start
#iterations: 63
currently lose_sum: 92.04952418804169
time_elpased: 1.742
batch start
#iterations: 64
currently lose_sum: 92.40032786130905
time_elpased: 1.766
batch start
#iterations: 65
currently lose_sum: 91.68797874450684
time_elpased: 1.761
batch start
#iterations: 66
currently lose_sum: 91.72065198421478
time_elpased: 1.742
batch start
#iterations: 67
currently lose_sum: 91.60780489444733
time_elpased: 1.791
batch start
#iterations: 68
currently lose_sum: 91.23049241304398
time_elpased: 1.805
batch start
#iterations: 69
currently lose_sum: 91.33221131563187
time_elpased: 1.747
batch start
#iterations: 70
currently lose_sum: 91.20368701219559
time_elpased: 1.755
batch start
#iterations: 71
currently lose_sum: 91.36333507299423
time_elpased: 1.743
batch start
#iterations: 72
currently lose_sum: 91.21598780155182
time_elpased: 1.739
batch start
#iterations: 73
currently lose_sum: 91.33676469326019
time_elpased: 1.853
batch start
#iterations: 74
currently lose_sum: 90.90279030799866
time_elpased: 1.816
batch start
#iterations: 75
currently lose_sum: 90.82324260473251
time_elpased: 1.752
batch start
#iterations: 76
currently lose_sum: 90.9727126955986
time_elpased: 1.778
batch start
#iterations: 77
currently lose_sum: 90.93422877788544
time_elpased: 1.741
batch start
#iterations: 78
currently lose_sum: 90.89939796924591
time_elpased: 1.752
batch start
#iterations: 79
currently lose_sum: 91.28652757406235
time_elpased: 1.762
start validation test
0.658608247423
0.657055837563
0.666049192137
0.661521950222
0.658595183697
58.956
batch start
#iterations: 80
currently lose_sum: 90.776182949543
time_elpased: 1.766
batch start
#iterations: 81
currently lose_sum: 90.65045082569122
time_elpased: 1.771
batch start
#iterations: 82
currently lose_sum: 90.73088693618774
time_elpased: 1.746
batch start
#iterations: 83
currently lose_sum: 90.49805498123169
time_elpased: 1.72
batch start
#iterations: 84
currently lose_sum: 90.68721407651901
time_elpased: 1.813
batch start
#iterations: 85
currently lose_sum: 90.14161890745163
time_elpased: 1.834
batch start
#iterations: 86
currently lose_sum: 89.97985100746155
time_elpased: 1.732
batch start
#iterations: 87
currently lose_sum: 90.1437960267067
time_elpased: 1.765
batch start
#iterations: 88
currently lose_sum: 90.41158419847488
time_elpased: 1.752
batch start
#iterations: 89
currently lose_sum: 90.32820409536362
time_elpased: 1.799
batch start
#iterations: 90
currently lose_sum: 89.94336324930191
time_elpased: 1.748
batch start
#iterations: 91
currently lose_sum: 90.41910928487778
time_elpased: 1.762
batch start
#iterations: 92
currently lose_sum: 89.63979119062424
time_elpased: 1.76
batch start
#iterations: 93
currently lose_sum: 89.7744933962822
time_elpased: 1.753
batch start
#iterations: 94
currently lose_sum: 89.66177153587341
time_elpased: 1.741
batch start
#iterations: 95
currently lose_sum: 89.27050578594208
time_elpased: 1.717
batch start
#iterations: 96
currently lose_sum: 89.35251808166504
time_elpased: 1.736
batch start
#iterations: 97
currently lose_sum: 88.96527445316315
time_elpased: 1.768
batch start
#iterations: 98
currently lose_sum: 89.26370465755463
time_elpased: 1.767
batch start
#iterations: 99
currently lose_sum: 89.06534230709076
time_elpased: 1.781
start validation test
0.666958762887
0.670008354219
0.660286096532
0.665111698544
0.666970477782
58.642
batch start
#iterations: 100
currently lose_sum: 89.7244023680687
time_elpased: 1.705
batch start
#iterations: 101
currently lose_sum: 89.33086514472961
time_elpased: 1.765
batch start
#iterations: 102
currently lose_sum: 89.05895394086838
time_elpased: 1.733
batch start
#iterations: 103
currently lose_sum: 89.38621765375137
time_elpased: 1.774
batch start
#iterations: 104
currently lose_sum: 89.37293750047684
time_elpased: 1.787
batch start
#iterations: 105
currently lose_sum: 89.02379149198532
time_elpased: 1.756
batch start
#iterations: 106
currently lose_sum: 88.80396348237991
time_elpased: 1.759
batch start
#iterations: 107
currently lose_sum: 88.54507875442505
time_elpased: 1.793
batch start
#iterations: 108
currently lose_sum: 88.34011018276215
time_elpased: 1.727
batch start
#iterations: 109
currently lose_sum: 88.88158202171326
time_elpased: 1.736
batch start
#iterations: 110
currently lose_sum: 88.86839497089386
time_elpased: 1.748
batch start
#iterations: 111
currently lose_sum: 88.26889026165009
time_elpased: 1.789
batch start
#iterations: 112
currently lose_sum: 88.42919844388962
time_elpased: 1.768
batch start
#iterations: 113
currently lose_sum: 87.85296612977982
time_elpased: 1.749
batch start
#iterations: 114
currently lose_sum: 88.2844917178154
time_elpased: 1.706
batch start
#iterations: 115
currently lose_sum: 88.6767361164093
time_elpased: 1.743
batch start
#iterations: 116
currently lose_sum: 87.8347396850586
time_elpased: 1.746
batch start
#iterations: 117
currently lose_sum: 88.08964097499847
time_elpased: 1.781
batch start
#iterations: 118
currently lose_sum: 87.91983276605606
time_elpased: 1.745
batch start
#iterations: 119
currently lose_sum: 87.92808550596237
time_elpased: 1.738
start validation test
0.653505154639
0.645742092457
0.682823916847
0.663765506202
0.65345368103
59.015
batch start
#iterations: 120
currently lose_sum: 87.47319769859314
time_elpased: 1.756
batch start
#iterations: 121
currently lose_sum: 87.9041046500206
time_elpased: 1.76
batch start
#iterations: 122
currently lose_sum: 87.13482797145844
time_elpased: 1.731
batch start
#iterations: 123
currently lose_sum: 87.81742113828659
time_elpased: 1.777
batch start
#iterations: 124
currently lose_sum: 87.99838674068451
time_elpased: 1.74
batch start
#iterations: 125
currently lose_sum: 87.43074268102646
time_elpased: 1.745
batch start
#iterations: 126
currently lose_sum: 88.30832010507584
time_elpased: 1.763
batch start
#iterations: 127
currently lose_sum: 88.2191544175148
time_elpased: 1.808
batch start
#iterations: 128
currently lose_sum: 87.34701466560364
time_elpased: 1.758
batch start
#iterations: 129
currently lose_sum: 87.94022697210312
time_elpased: 1.815
batch start
#iterations: 130
currently lose_sum: 87.45499104261398
time_elpased: 1.76
batch start
#iterations: 131
currently lose_sum: 87.58668178319931
time_elpased: 1.779
batch start
#iterations: 132
currently lose_sum: 87.06207901239395
time_elpased: 1.745
batch start
#iterations: 133
currently lose_sum: 86.89314872026443
time_elpased: 1.741
batch start
#iterations: 134
currently lose_sum: 87.2645229101181
time_elpased: 1.808
batch start
#iterations: 135
currently lose_sum: 87.46716058254242
time_elpased: 1.815
batch start
#iterations: 136
currently lose_sum: 86.8687133193016
time_elpased: 1.722
batch start
#iterations: 137
currently lose_sum: 87.03397691249847
time_elpased: 1.796
batch start
#iterations: 138
currently lose_sum: 86.75628304481506
time_elpased: 1.735
batch start
#iterations: 139
currently lose_sum: 86.56559664011002
time_elpased: 1.799
start validation test
0.649536082474
0.647612302711
0.658742410209
0.65312994235
0.649519919346
59.172
batch start
#iterations: 140
currently lose_sum: 86.68278622627258
time_elpased: 1.8
batch start
#iterations: 141
currently lose_sum: 86.85053884983063
time_elpased: 1.778
batch start
#iterations: 142
currently lose_sum: 86.46273291110992
time_elpased: 1.801
batch start
#iterations: 143
currently lose_sum: 86.77025425434113
time_elpased: 1.762
batch start
#iterations: 144
currently lose_sum: 86.56648015975952
time_elpased: 1.773
batch start
#iterations: 145
currently lose_sum: 86.64339113235474
time_elpased: 1.786
batch start
#iterations: 146
currently lose_sum: 85.85560500621796
time_elpased: 1.768
batch start
#iterations: 147
currently lose_sum: 86.29878008365631
time_elpased: 1.757
batch start
#iterations: 148
currently lose_sum: 86.21593555808067
time_elpased: 1.762
batch start
#iterations: 149
currently lose_sum: 86.24014019966125
time_elpased: 1.771
batch start
#iterations: 150
currently lose_sum: 86.45933705568314
time_elpased: 1.729
batch start
#iterations: 151
currently lose_sum: 85.41898268461227
time_elpased: 1.732
batch start
#iterations: 152
currently lose_sum: 86.17760801315308
time_elpased: 1.746
batch start
#iterations: 153
currently lose_sum: 86.13497805595398
time_elpased: 1.792
batch start
#iterations: 154
currently lose_sum: 85.79887092113495
time_elpased: 1.774
batch start
#iterations: 155
currently lose_sum: 85.30815184116364
time_elpased: 1.785
batch start
#iterations: 156
currently lose_sum: 85.46371233463287
time_elpased: 1.784
batch start
#iterations: 157
currently lose_sum: 85.15132242441177
time_elpased: 1.777
batch start
#iterations: 158
currently lose_sum: 85.62224960327148
time_elpased: 1.794
batch start
#iterations: 159
currently lose_sum: 85.06829136610031
time_elpased: 1.775
start validation test
0.636494845361
0.665097261802
0.552433878769
0.60355295705
0.636642427353
60.648
batch start
#iterations: 160
currently lose_sum: 85.35846793651581
time_elpased: 1.769
batch start
#iterations: 161
currently lose_sum: 85.47143143415451
time_elpased: 1.754
batch start
#iterations: 162
currently lose_sum: 85.33991676568985
time_elpased: 1.727
batch start
#iterations: 163
currently lose_sum: 85.05944395065308
time_elpased: 1.769
batch start
#iterations: 164
currently lose_sum: 84.46732813119888
time_elpased: 1.718
batch start
#iterations: 165
currently lose_sum: 84.4991711974144
time_elpased: 1.759
batch start
#iterations: 166
currently lose_sum: 84.87138867378235
time_elpased: 1.759
batch start
#iterations: 167
currently lose_sum: 84.72165405750275
time_elpased: 1.793
batch start
#iterations: 168
currently lose_sum: 84.94454550743103
time_elpased: 1.781
batch start
#iterations: 169
currently lose_sum: 84.42203080654144
time_elpased: 1.784
batch start
#iterations: 170
currently lose_sum: 85.00828963518143
time_elpased: 1.825
batch start
#iterations: 171
currently lose_sum: 84.92024475336075
time_elpased: 1.745
batch start
#iterations: 172
currently lose_sum: 84.57323533296585
time_elpased: 1.736
batch start
#iterations: 173
currently lose_sum: 84.55256479978561
time_elpased: 1.747
batch start
#iterations: 174
currently lose_sum: 83.9883724451065
time_elpased: 1.816
batch start
#iterations: 175
currently lose_sum: 83.84387850761414
time_elpased: 1.76
batch start
#iterations: 176
currently lose_sum: 83.60957443714142
time_elpased: 1.741
batch start
#iterations: 177
currently lose_sum: 83.28563290834427
time_elpased: 1.762
batch start
#iterations: 178
currently lose_sum: 83.46732354164124
time_elpased: 1.743
batch start
#iterations: 179
currently lose_sum: 83.54309394955635
time_elpased: 1.739
start validation test
0.644948453608
0.660720372685
0.598435731193
0.628037585052
0.645030113867
60.523
batch start
#iterations: 180
currently lose_sum: 84.14404368400574
time_elpased: 1.739
batch start
#iterations: 181
currently lose_sum: 83.75893074274063
time_elpased: 1.786
batch start
#iterations: 182
currently lose_sum: 83.72269541025162
time_elpased: 1.728
batch start
#iterations: 183
currently lose_sum: 83.67289596796036
time_elpased: 1.755
batch start
#iterations: 184
currently lose_sum: 83.68540066480637
time_elpased: 1.724
batch start
#iterations: 185
currently lose_sum: 83.11053869128227
time_elpased: 1.772
batch start
#iterations: 186
currently lose_sum: 83.49423432350159
time_elpased: 1.768
batch start
#iterations: 187
currently lose_sum: 83.86864167451859
time_elpased: 1.754
batch start
#iterations: 188
currently lose_sum: 83.2210493683815
time_elpased: 1.733
batch start
#iterations: 189
currently lose_sum: 83.92001321911812
time_elpased: 1.812
batch start
#iterations: 190
currently lose_sum: 82.82295697927475
time_elpased: 1.761
batch start
#iterations: 191
currently lose_sum: 83.09785217046738
time_elpased: 1.788
batch start
#iterations: 192
currently lose_sum: 82.93327909708023
time_elpased: 1.785
batch start
#iterations: 193
currently lose_sum: 83.01173084974289
time_elpased: 1.766
batch start
#iterations: 194
currently lose_sum: 82.27978816628456
time_elpased: 1.755
batch start
#iterations: 195
currently lose_sum: 82.74345979094505
time_elpased: 1.75
batch start
#iterations: 196
currently lose_sum: 82.6655361354351
time_elpased: 1.746
batch start
#iterations: 197
currently lose_sum: 82.6043004989624
time_elpased: 1.784
batch start
#iterations: 198
currently lose_sum: 82.53167882561684
time_elpased: 1.743
batch start
#iterations: 199
currently lose_sum: 82.19367450475693
time_elpased: 1.779
start validation test
0.635515463918
0.654773046327
0.576000823299
0.612866137421
0.635619951049
61.477
batch start
#iterations: 200
currently lose_sum: 82.21460658311844
time_elpased: 1.753
batch start
#iterations: 201
currently lose_sum: 82.56037724018097
time_elpased: 1.748
batch start
#iterations: 202
currently lose_sum: 82.01724809408188
time_elpased: 1.744
batch start
#iterations: 203
currently lose_sum: 81.8613333106041
time_elpased: 1.725
batch start
#iterations: 204
currently lose_sum: 81.34996527433395
time_elpased: 1.732
batch start
#iterations: 205
currently lose_sum: 81.72627085447311
time_elpased: 1.754
batch start
#iterations: 206
currently lose_sum: 82.1525519490242
time_elpased: 1.734
batch start
#iterations: 207
currently lose_sum: 81.43501535058022
time_elpased: 1.798
batch start
#iterations: 208
currently lose_sum: 81.53581196069717
time_elpased: 1.737
batch start
#iterations: 209
currently lose_sum: 81.48445552587509
time_elpased: 1.736
batch start
#iterations: 210
currently lose_sum: 81.15916591882706
time_elpased: 1.792
batch start
#iterations: 211
currently lose_sum: 81.67513293027878
time_elpased: 1.8
batch start
#iterations: 212
currently lose_sum: 81.09973138570786
time_elpased: 1.751
batch start
#iterations: 213
currently lose_sum: 80.85157895088196
time_elpased: 1.767
batch start
#iterations: 214
currently lose_sum: 81.1966707110405
time_elpased: 1.743
batch start
#iterations: 215
currently lose_sum: 80.66620320081711
time_elpased: 1.8
batch start
#iterations: 216
currently lose_sum: 80.0554935336113
time_elpased: 1.749
batch start
#iterations: 217
currently lose_sum: 80.97456067800522
time_elpased: 1.732
batch start
#iterations: 218
currently lose_sum: 79.85709458589554
time_elpased: 1.737
batch start
#iterations: 219
currently lose_sum: 80.97888812422752
time_elpased: 1.756
start validation test
0.616546391753
0.642019950125
0.529896058454
0.580594238033
0.616698519778
63.419
batch start
#iterations: 220
currently lose_sum: 80.24117550253868
time_elpased: 1.716
batch start
#iterations: 221
currently lose_sum: 80.41199201345444
time_elpased: 1.755
batch start
#iterations: 222
currently lose_sum: 79.84093922376633
time_elpased: 1.741
batch start
#iterations: 223
currently lose_sum: 80.08294129371643
time_elpased: 1.759
batch start
#iterations: 224
currently lose_sum: 79.85216873884201
time_elpased: 1.711
batch start
#iterations: 225
currently lose_sum: 80.17793288826942
time_elpased: 1.753
batch start
#iterations: 226
currently lose_sum: 80.03390553593636
time_elpased: 1.729
batch start
#iterations: 227
currently lose_sum: 80.06221413612366
time_elpased: 1.712
batch start
#iterations: 228
currently lose_sum: 79.0165866613388
time_elpased: 1.832
batch start
#iterations: 229
currently lose_sum: 79.0266962647438
time_elpased: 1.784
batch start
#iterations: 230
currently lose_sum: 79.36562913656235
time_elpased: 1.765
batch start
#iterations: 231
currently lose_sum: 79.16636297106743
time_elpased: 1.755
batch start
#iterations: 232
currently lose_sum: 79.87789362668991
time_elpased: 1.736
batch start
#iterations: 233
currently lose_sum: 79.31735768914223
time_elpased: 1.764
batch start
#iterations: 234
currently lose_sum: 79.4342011809349
time_elpased: 1.758
batch start
#iterations: 235
currently lose_sum: 78.31167975068092
time_elpased: 1.73
batch start
#iterations: 236
currently lose_sum: 79.09352537989616
time_elpased: 1.725
batch start
#iterations: 237
currently lose_sum: 78.57689893245697
time_elpased: 1.757
batch start
#iterations: 238
currently lose_sum: 79.59900242090225
time_elpased: 1.749
batch start
#iterations: 239
currently lose_sum: 78.36703670024872
time_elpased: 1.713
start validation test
0.61793814433
0.647510559324
0.520633940517
0.577181973759
0.618108976868
64.742
batch start
#iterations: 240
currently lose_sum: 78.06172361969948
time_elpased: 1.805
batch start
#iterations: 241
currently lose_sum: 78.71802371740341
time_elpased: 1.743
batch start
#iterations: 242
currently lose_sum: 78.0057532787323
time_elpased: 1.728
batch start
#iterations: 243
currently lose_sum: 78.57874545454979
time_elpased: 1.776
batch start
#iterations: 244
currently lose_sum: 78.37720081210136
time_elpased: 1.8
batch start
#iterations: 245
currently lose_sum: 77.60752320289612
time_elpased: 1.733
batch start
#iterations: 246
currently lose_sum: 78.72539022564888
time_elpased: 1.751
batch start
#iterations: 247
currently lose_sum: 77.22722390294075
time_elpased: 1.748
batch start
#iterations: 248
currently lose_sum: 77.88432502746582
time_elpased: 1.726
batch start
#iterations: 249
currently lose_sum: 77.66846868395805
time_elpased: 1.741
batch start
#iterations: 250
currently lose_sum: 76.7050319314003
time_elpased: 1.76
batch start
#iterations: 251
currently lose_sum: 76.78667721152306
time_elpased: 1.714
batch start
#iterations: 252
currently lose_sum: 77.19505113363266
time_elpased: 1.721
batch start
#iterations: 253
currently lose_sum: 76.6425214111805
time_elpased: 1.764
batch start
#iterations: 254
currently lose_sum: 77.68830436468124
time_elpased: 1.762
batch start
#iterations: 255
currently lose_sum: 77.26046600937843
time_elpased: 1.731
batch start
#iterations: 256
currently lose_sum: 76.84591636061668
time_elpased: 1.731
batch start
#iterations: 257
currently lose_sum: 76.9309421479702
time_elpased: 1.74
batch start
#iterations: 258
currently lose_sum: 76.9986075758934
time_elpased: 1.779
batch start
#iterations: 259
currently lose_sum: 77.14571332931519
time_elpased: 1.78
start validation test
0.620103092784
0.656947973786
0.505505814552
0.571362103059
0.620304285981
66.970
batch start
#iterations: 260
currently lose_sum: 76.32982385158539
time_elpased: 1.708
batch start
#iterations: 261
currently lose_sum: 75.56408295035362
time_elpased: 1.775
batch start
#iterations: 262
currently lose_sum: 76.79519227147102
time_elpased: 1.72
batch start
#iterations: 263
currently lose_sum: 76.22090518474579
time_elpased: 1.727
batch start
#iterations: 264
currently lose_sum: 76.3659493625164
time_elpased: 1.765
batch start
#iterations: 265
currently lose_sum: 76.07074534893036
time_elpased: 1.702
batch start
#iterations: 266
currently lose_sum: 75.96800273656845
time_elpased: 1.814
batch start
#iterations: 267
currently lose_sum: 75.69060030579567
time_elpased: 1.759
batch start
#iterations: 268
currently lose_sum: 76.12027588486671
time_elpased: 1.711
batch start
#iterations: 269
currently lose_sum: 75.91645523905754
time_elpased: 1.856
batch start
#iterations: 270
currently lose_sum: 75.23341527581215
time_elpased: 1.775
batch start
#iterations: 271
currently lose_sum: 75.32322078943253
time_elpased: 1.759
batch start
#iterations: 272
currently lose_sum: 75.46955469250679
time_elpased: 1.733
batch start
#iterations: 273
currently lose_sum: 74.65854534506798
time_elpased: 1.751
batch start
#iterations: 274
currently lose_sum: 75.92115792632103
time_elpased: 1.73
batch start
#iterations: 275
currently lose_sum: 74.86086714267731
time_elpased: 1.755
batch start
#iterations: 276
currently lose_sum: 75.35339519381523
time_elpased: 1.759
batch start
#iterations: 277
currently lose_sum: 74.79548120498657
time_elpased: 1.789
batch start
#iterations: 278
currently lose_sum: 74.43922254443169
time_elpased: 1.72
batch start
#iterations: 279
currently lose_sum: 75.1382702589035
time_elpased: 1.753
start validation test
0.605360824742
0.656302138632
0.445302047957
0.530594727161
0.605641832612
69.250
batch start
#iterations: 280
currently lose_sum: 74.54301556944847
time_elpased: 1.802
batch start
#iterations: 281
currently lose_sum: 74.33269664645195
time_elpased: 1.737
batch start
#iterations: 282
currently lose_sum: 74.17879036068916
time_elpased: 1.782
batch start
#iterations: 283
currently lose_sum: 73.89427068829536
time_elpased: 1.706
batch start
#iterations: 284
currently lose_sum: 73.73901027441025
time_elpased: 1.712
batch start
#iterations: 285
currently lose_sum: 73.69761145114899
time_elpased: 1.728
batch start
#iterations: 286
currently lose_sum: 74.15732342004776
time_elpased: 1.732
batch start
#iterations: 287
currently lose_sum: 73.93083417415619
time_elpased: 1.747
batch start
#iterations: 288
currently lose_sum: 73.79858663678169
time_elpased: 1.735
batch start
#iterations: 289
currently lose_sum: 73.75497338175774
time_elpased: 1.723
batch start
#iterations: 290
currently lose_sum: 74.14935666322708
time_elpased: 1.739
batch start
#iterations: 291
currently lose_sum: 74.00421780347824
time_elpased: 1.704
batch start
#iterations: 292
currently lose_sum: 73.12424349784851
time_elpased: 1.742
batch start
#iterations: 293
currently lose_sum: 72.81063929200172
time_elpased: 1.793
batch start
#iterations: 294
currently lose_sum: 73.16619026660919
time_elpased: 1.743
batch start
#iterations: 295
currently lose_sum: 73.3642715215683
time_elpased: 1.773
batch start
#iterations: 296
currently lose_sum: 72.75707069039345
time_elpased: 1.724
batch start
#iterations: 297
currently lose_sum: 73.21227219700813
time_elpased: 1.779
batch start
#iterations: 298
currently lose_sum: 72.9704122543335
time_elpased: 1.761
batch start
#iterations: 299
currently lose_sum: 72.48009395599365
time_elpased: 1.732
start validation test
0.594587628866
0.641893962611
0.431100133786
0.515791417842
0.59487465638
71.630
batch start
#iterations: 300
currently lose_sum: 72.23408445715904
time_elpased: 1.736
batch start
#iterations: 301
currently lose_sum: 72.73039564490318
time_elpased: 1.76
batch start
#iterations: 302
currently lose_sum: 72.05799382925034
time_elpased: 1.746
batch start
#iterations: 303
currently lose_sum: 71.83594316244125
time_elpased: 1.818
batch start
#iterations: 304
currently lose_sum: 72.23840227723122
time_elpased: 1.755
batch start
#iterations: 305
currently lose_sum: 71.52996382117271
time_elpased: 1.786
batch start
#iterations: 306
currently lose_sum: 71.55141979455948
time_elpased: 1.753
batch start
#iterations: 307
currently lose_sum: 72.10861897468567
time_elpased: 1.739
batch start
#iterations: 308
currently lose_sum: 71.55599182844162
time_elpased: 1.8
batch start
#iterations: 309
currently lose_sum: 71.0748642385006
time_elpased: 1.71
batch start
#iterations: 310
currently lose_sum: 71.38447844982147
time_elpased: 1.765
batch start
#iterations: 311
currently lose_sum: 70.71828904747963
time_elpased: 1.763
batch start
#iterations: 312
currently lose_sum: 70.97007796168327
time_elpased: 1.721
batch start
#iterations: 313
currently lose_sum: 70.90598794817924
time_elpased: 1.765
batch start
#iterations: 314
currently lose_sum: 70.78344765305519
time_elpased: 1.805
batch start
#iterations: 315
currently lose_sum: 70.46121490001678
time_elpased: 1.702
batch start
#iterations: 316
currently lose_sum: 71.38519123196602
time_elpased: 1.822
batch start
#iterations: 317
currently lose_sum: 69.96254301071167
time_elpased: 1.796
batch start
#iterations: 318
currently lose_sum: 69.97756922245026
time_elpased: 1.738
batch start
#iterations: 319
currently lose_sum: 70.77649939060211
time_elpased: 1.756
start validation test
0.590257731959
0.633252939403
0.432335082844
0.51385236377
0.590534989527
73.169
batch start
#iterations: 320
currently lose_sum: 70.37263616919518
time_elpased: 1.766
batch start
#iterations: 321
currently lose_sum: 70.04097068309784
time_elpased: 1.762
batch start
#iterations: 322
currently lose_sum: 70.25858408212662
time_elpased: 1.742
batch start
#iterations: 323
currently lose_sum: 70.14855170249939
time_elpased: 1.753
batch start
#iterations: 324
currently lose_sum: 70.0318765938282
time_elpased: 1.749
batch start
#iterations: 325
currently lose_sum: 69.925361931324
time_elpased: 1.763
batch start
#iterations: 326
currently lose_sum: 69.01746273040771
time_elpased: 1.785
batch start
#iterations: 327
currently lose_sum: 69.72582876682281
time_elpased: 1.746
batch start
#iterations: 328
currently lose_sum: 69.04876604676247
time_elpased: 1.739
batch start
#iterations: 329
currently lose_sum: 68.49955496191978
time_elpased: 1.771
batch start
#iterations: 330
currently lose_sum: 69.49667349457741
time_elpased: 1.753
batch start
#iterations: 331
currently lose_sum: 68.77243465185165
time_elpased: 1.735
batch start
#iterations: 332
currently lose_sum: 68.3849729001522
time_elpased: 1.739
batch start
#iterations: 333
currently lose_sum: 68.30313271284103
time_elpased: 1.761
batch start
#iterations: 334
currently lose_sum: 67.93636962771416
time_elpased: 1.749
batch start
#iterations: 335
currently lose_sum: 67.92468512058258
time_elpased: 1.716
batch start
#iterations: 336
currently lose_sum: 68.91792371869087
time_elpased: 1.734
batch start
#iterations: 337
currently lose_sum: 68.33657857775688
time_elpased: 1.753
batch start
#iterations: 338
currently lose_sum: 67.84542909264565
time_elpased: 1.784
batch start
#iterations: 339
currently lose_sum: 68.069600969553
time_elpased: 1.747
start validation test
0.584690721649
0.638795986622
0.393125450242
0.48671720711
0.58502704403
76.040
batch start
#iterations: 340
currently lose_sum: 68.21508449316025
time_elpased: 1.809
batch start
#iterations: 341
currently lose_sum: 68.11111760139465
time_elpased: 1.753
batch start
#iterations: 342
currently lose_sum: 68.30994421243668
time_elpased: 1.772
batch start
#iterations: 343
currently lose_sum: 66.44365325570107
time_elpased: 1.785
batch start
#iterations: 344
currently lose_sum: 67.52494397759438
time_elpased: 1.757
batch start
#iterations: 345
currently lose_sum: 68.11125805974007
time_elpased: 1.786
batch start
#iterations: 346
currently lose_sum: 67.01782515645027
time_elpased: 1.71
batch start
#iterations: 347
currently lose_sum: 67.04620799422264
time_elpased: 1.77
batch start
#iterations: 348
currently lose_sum: 67.0411222577095
time_elpased: 1.813
batch start
#iterations: 349
currently lose_sum: 67.2064118385315
time_elpased: 1.775
batch start
#iterations: 350
currently lose_sum: 66.53640267252922
time_elpased: 1.742
batch start
#iterations: 351
currently lose_sum: 66.69140151143074
time_elpased: 1.787
batch start
#iterations: 352
currently lose_sum: 67.13686555624008
time_elpased: 1.751
batch start
#iterations: 353
currently lose_sum: 66.39670222997665
time_elpased: 1.722
batch start
#iterations: 354
currently lose_sum: 66.07028833031654
time_elpased: 1.74
batch start
#iterations: 355
currently lose_sum: 65.87042832374573
time_elpased: 1.756
batch start
#iterations: 356
currently lose_sum: 66.81148558855057
time_elpased: 1.766
batch start
#iterations: 357
currently lose_sum: 66.05663692951202
time_elpased: 1.717
batch start
#iterations: 358
currently lose_sum: 66.35385763645172
time_elpased: 1.761
batch start
#iterations: 359
currently lose_sum: 66.18453603982925
time_elpased: 1.733
start validation test
0.583298969072
0.644028929264
0.375733251003
0.474587287144
0.583663382705
80.106
batch start
#iterations: 360
currently lose_sum: 66.03179007768631
time_elpased: 1.726
batch start
#iterations: 361
currently lose_sum: 65.63086599111557
time_elpased: 1.757
batch start
#iterations: 362
currently lose_sum: 64.78075668215752
time_elpased: 1.734
batch start
#iterations: 363
currently lose_sum: 65.0224681198597
time_elpased: 1.764
batch start
#iterations: 364
currently lose_sum: 65.31465324759483
time_elpased: 1.728
batch start
#iterations: 365
currently lose_sum: 65.21702966094017
time_elpased: 1.764
batch start
#iterations: 366
currently lose_sum: 65.29743954539299
time_elpased: 1.727
batch start
#iterations: 367
currently lose_sum: 65.00301441550255
time_elpased: 1.739
batch start
#iterations: 368
currently lose_sum: 64.97116580605507
time_elpased: 1.73
batch start
#iterations: 369
currently lose_sum: 65.2521944642067
time_elpased: 1.777
batch start
#iterations: 370
currently lose_sum: 64.6256352365017
time_elpased: 1.756
batch start
#iterations: 371
currently lose_sum: 65.03902024030685
time_elpased: 1.743
batch start
#iterations: 372
currently lose_sum: 63.79772460460663
time_elpased: 1.736
batch start
#iterations: 373
currently lose_sum: 64.80989307165146
time_elpased: 1.721
batch start
#iterations: 374
currently lose_sum: 64.05504417419434
time_elpased: 1.745
batch start
#iterations: 375
currently lose_sum: 63.89457181096077
time_elpased: 1.74
batch start
#iterations: 376
currently lose_sum: 64.60400032997131
time_elpased: 1.741
batch start
#iterations: 377
currently lose_sum: 63.29931554198265
time_elpased: 1.744
batch start
#iterations: 378
currently lose_sum: 63.41571643948555
time_elpased: 1.766
batch start
#iterations: 379
currently lose_sum: 63.73564311861992
time_elpased: 1.765
start validation test
0.588865979381
0.639079725196
0.411649686117
0.50075112669
0.589177109918
80.494
batch start
#iterations: 380
currently lose_sum: 62.947175681591034
time_elpased: 1.754
batch start
#iterations: 381
currently lose_sum: 63.90321749448776
time_elpased: 1.737
batch start
#iterations: 382
currently lose_sum: 63.65569543838501
time_elpased: 1.743
batch start
#iterations: 383
currently lose_sum: 62.81597724556923
time_elpased: 1.748
batch start
#iterations: 384
currently lose_sum: 63.971520990133286
time_elpased: 1.746
batch start
#iterations: 385
currently lose_sum: 63.10054549574852
time_elpased: 1.711
batch start
#iterations: 386
currently lose_sum: 62.34629613161087
time_elpased: 1.722
batch start
#iterations: 387
currently lose_sum: 62.139309883117676
time_elpased: 1.821
batch start
#iterations: 388
currently lose_sum: 63.42605087161064
time_elpased: 1.709
batch start
#iterations: 389
currently lose_sum: 62.752482295036316
time_elpased: 1.779
batch start
#iterations: 390
currently lose_sum: 62.625234454870224
time_elpased: 1.744
batch start
#iterations: 391
currently lose_sum: 62.487858802080154
time_elpased: 1.738
batch start
#iterations: 392
currently lose_sum: 62.24689531326294
time_elpased: 1.69
batch start
#iterations: 393
currently lose_sum: 61.77310407161713
time_elpased: 1.723
batch start
#iterations: 394
currently lose_sum: 62.12250679731369
time_elpased: 1.757
batch start
#iterations: 395
currently lose_sum: 61.962639182806015
time_elpased: 1.791
batch start
#iterations: 396
currently lose_sum: 61.38866877555847
time_elpased: 1.757
batch start
#iterations: 397
currently lose_sum: 62.13161239027977
time_elpased: 1.747
batch start
#iterations: 398
currently lose_sum: 61.746460527181625
time_elpased: 1.821
batch start
#iterations: 399
currently lose_sum: 61.92230787873268
time_elpased: 1.806
start validation test
0.576907216495
0.630966846034
0.374086652259
0.46969892751
0.577263299278
84.362
acc: 0.664
pre: 0.666
rec: 0.658
F1: 0.662
auc: 0.664
