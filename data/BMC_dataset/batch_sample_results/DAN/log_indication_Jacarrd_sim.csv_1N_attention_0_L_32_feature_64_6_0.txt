start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.10395443439484
time_elpased: 1.856
batch start
#iterations: 1
currently lose_sum: 99.66595941781998
time_elpased: 1.89
batch start
#iterations: 2
currently lose_sum: 99.544100522995
time_elpased: 1.891
batch start
#iterations: 3
currently lose_sum: 98.83945745229721
time_elpased: 1.855
batch start
#iterations: 4
currently lose_sum: 98.68220287561417
time_elpased: 1.857
batch start
#iterations: 5
currently lose_sum: 98.39185059070587
time_elpased: 1.872
batch start
#iterations: 6
currently lose_sum: 98.00767183303833
time_elpased: 1.94
batch start
#iterations: 7
currently lose_sum: 97.9754444360733
time_elpased: 1.842
batch start
#iterations: 8
currently lose_sum: 97.50574618577957
time_elpased: 1.859
batch start
#iterations: 9
currently lose_sum: 97.53966957330704
time_elpased: 1.832
batch start
#iterations: 10
currently lose_sum: 97.23434466123581
time_elpased: 1.89
batch start
#iterations: 11
currently lose_sum: 97.48405545949936
time_elpased: 1.93
batch start
#iterations: 12
currently lose_sum: 97.39905732870102
time_elpased: 1.866
batch start
#iterations: 13
currently lose_sum: 96.83645725250244
time_elpased: 1.88
batch start
#iterations: 14
currently lose_sum: 96.58014887571335
time_elpased: 1.872
batch start
#iterations: 15
currently lose_sum: 96.79624927043915
time_elpased: 1.858
batch start
#iterations: 16
currently lose_sum: 96.41321820020676
time_elpased: 1.884
batch start
#iterations: 17
currently lose_sum: 96.85040146112442
time_elpased: 1.873
batch start
#iterations: 18
currently lose_sum: 96.40230542421341
time_elpased: 1.886
batch start
#iterations: 19
currently lose_sum: 96.27948951721191
time_elpased: 1.918
start validation test
0.639896907216
0.620552661782
0.723371410929
0.668028891846
0.63975035485
62.143
batch start
#iterations: 20
currently lose_sum: 96.51798862218857
time_elpased: 1.847
batch start
#iterations: 21
currently lose_sum: 95.90727776288986
time_elpased: 1.905
batch start
#iterations: 22
currently lose_sum: 95.84434759616852
time_elpased: 1.85
batch start
#iterations: 23
currently lose_sum: 96.09620851278305
time_elpased: 1.893
batch start
#iterations: 24
currently lose_sum: 95.73487913608551
time_elpased: 1.885
batch start
#iterations: 25
currently lose_sum: 95.67545807361603
time_elpased: 1.863
batch start
#iterations: 26
currently lose_sum: 95.24459517002106
time_elpased: 1.902
batch start
#iterations: 27
currently lose_sum: 94.98010689020157
time_elpased: 1.881
batch start
#iterations: 28
currently lose_sum: 95.11213999986649
time_elpased: 1.868
batch start
#iterations: 29
currently lose_sum: 94.56510382890701
time_elpased: 1.893
batch start
#iterations: 30
currently lose_sum: 94.98208248615265
time_elpased: 1.893
batch start
#iterations: 31
currently lose_sum: 94.21126252412796
time_elpased: 1.832
batch start
#iterations: 32
currently lose_sum: 93.97703820466995
time_elpased: 1.899
batch start
#iterations: 33
currently lose_sum: 94.41967308521271
time_elpased: 1.907
batch start
#iterations: 34
currently lose_sum: 94.49939912557602
time_elpased: 1.855
batch start
#iterations: 35
currently lose_sum: 94.08954536914825
time_elpased: 1.866
batch start
#iterations: 36
currently lose_sum: 94.07250100374222
time_elpased: 1.867
batch start
#iterations: 37
currently lose_sum: 93.5909035205841
time_elpased: 1.903
batch start
#iterations: 38
currently lose_sum: 94.0713564157486
time_elpased: 1.87
batch start
#iterations: 39
currently lose_sum: 93.49889397621155
time_elpased: 1.95
start validation test
0.654536082474
0.646203084085
0.68570546465
0.665368484122
0.654481359816
60.077
batch start
#iterations: 40
currently lose_sum: 93.37905812263489
time_elpased: 1.933
batch start
#iterations: 41
currently lose_sum: 93.50539684295654
time_elpased: 1.854
batch start
#iterations: 42
currently lose_sum: 93.51564568281174
time_elpased: 1.848
batch start
#iterations: 43
currently lose_sum: 93.17627316713333
time_elpased: 1.896
batch start
#iterations: 44
currently lose_sum: 92.70664268732071
time_elpased: 1.909
batch start
#iterations: 45
currently lose_sum: 92.53794127702713
time_elpased: 2.043
batch start
#iterations: 46
currently lose_sum: 92.80438071489334
time_elpased: 2.687
batch start
#iterations: 47
currently lose_sum: 92.93271803855896
time_elpased: 2.172
batch start
#iterations: 48
currently lose_sum: 93.12597119808197
time_elpased: 2.196
batch start
#iterations: 49
currently lose_sum: 93.66434746980667
time_elpased: 2.161
batch start
#iterations: 50
currently lose_sum: 92.43288373947144
time_elpased: 2.242
batch start
#iterations: 51
currently lose_sum: 92.45142447948456
time_elpased: 2.835
batch start
#iterations: 52
currently lose_sum: 92.54079467058182
time_elpased: 2.795
batch start
#iterations: 53
currently lose_sum: 92.4632905125618
time_elpased: 1.947
batch start
#iterations: 54
currently lose_sum: 92.55496460199356
time_elpased: 1.891
batch start
#iterations: 55
currently lose_sum: 92.8113716840744
time_elpased: 1.987
batch start
#iterations: 56
currently lose_sum: 92.30194228887558
time_elpased: 2.008
batch start
#iterations: 57
currently lose_sum: 92.37537038326263
time_elpased: 1.994
batch start
#iterations: 58
currently lose_sum: 92.50942754745483
time_elpased: 2.052
batch start
#iterations: 59
currently lose_sum: 92.60108798742294
time_elpased: 2.02
start validation test
0.658659793814
0.642403607251
0.718431614696
0.678293820443
0.658554855164
59.805
batch start
#iterations: 60
currently lose_sum: 92.17196440696716
time_elpased: 1.832
batch start
#iterations: 61
currently lose_sum: 92.7469893693924
time_elpased: 1.952
batch start
#iterations: 62
currently lose_sum: 92.10030353069305
time_elpased: 1.825
batch start
#iterations: 63
currently lose_sum: 92.11721616983414
time_elpased: 1.903
batch start
#iterations: 64
currently lose_sum: 92.46179813146591
time_elpased: 1.856
batch start
#iterations: 65
currently lose_sum: 91.9193332195282
time_elpased: 1.868
batch start
#iterations: 66
currently lose_sum: 90.86328500509262
time_elpased: 1.855
batch start
#iterations: 67
currently lose_sum: 91.9287229180336
time_elpased: 1.842
batch start
#iterations: 68
currently lose_sum: 92.15613394975662
time_elpased: 1.87
batch start
#iterations: 69
currently lose_sum: 91.52582061290741
time_elpased: 2.289
batch start
#iterations: 70
currently lose_sum: 91.61772209405899
time_elpased: 2.645
batch start
#iterations: 71
currently lose_sum: 91.36136370897293
time_elpased: 2.353
batch start
#iterations: 72
currently lose_sum: 90.45157754421234
time_elpased: 2.685
batch start
#iterations: 73
currently lose_sum: 90.92864161729813
time_elpased: 2.486
batch start
#iterations: 74
currently lose_sum: 91.00259381532669
time_elpased: 1.998
batch start
#iterations: 75
currently lose_sum: 91.34238117933273
time_elpased: 1.896
batch start
#iterations: 76
currently lose_sum: 90.74915015697479
time_elpased: 1.861
batch start
#iterations: 77
currently lose_sum: 91.04325020313263
time_elpased: 1.943
batch start
#iterations: 78
currently lose_sum: 90.86904537677765
time_elpased: 1.883
batch start
#iterations: 79
currently lose_sum: 90.42613852024078
time_elpased: 1.884
start validation test
0.637886597938
0.625700410908
0.689513224246
0.65605875153
0.637795959433
60.346
batch start
#iterations: 80
currently lose_sum: 91.43376237154007
time_elpased: 1.955
batch start
#iterations: 81
currently lose_sum: 90.55199533700943
time_elpased: 2.009
batch start
#iterations: 82
currently lose_sum: 90.6672471165657
time_elpased: 2.089
batch start
#iterations: 83
currently lose_sum: 91.06876218318939
time_elpased: 1.913
batch start
#iterations: 84
currently lose_sum: 90.95625072717667
time_elpased: 1.836
batch start
#iterations: 85
currently lose_sum: 90.25429499149323
time_elpased: 2.032
batch start
#iterations: 86
currently lose_sum: 90.40911513566971
time_elpased: 1.972
batch start
#iterations: 87
currently lose_sum: 90.62660670280457
time_elpased: 1.995
batch start
#iterations: 88
currently lose_sum: 90.10191136598587
time_elpased: 1.927
batch start
#iterations: 89
currently lose_sum: 90.0801745057106
time_elpased: 1.901
batch start
#iterations: 90
currently lose_sum: 90.92622584104538
time_elpased: 1.857
batch start
#iterations: 91
currently lose_sum: 89.57373356819153
time_elpased: 1.86
batch start
#iterations: 92
currently lose_sum: 90.07738596200943
time_elpased: 2.035
batch start
#iterations: 93
currently lose_sum: 89.8557425737381
time_elpased: 1.953
batch start
#iterations: 94
currently lose_sum: 90.3597155213356
time_elpased: 2.352
batch start
#iterations: 95
currently lose_sum: 89.80031144618988
time_elpased: 3.037
batch start
#iterations: 96
currently lose_sum: 90.08984488248825
time_elpased: 2.162
batch start
#iterations: 97
currently lose_sum: 89.52258259057999
time_elpased: 2.47
batch start
#iterations: 98
currently lose_sum: 89.59486246109009
time_elpased: 3.037
batch start
#iterations: 99
currently lose_sum: 89.62236034870148
time_elpased: 2.4
start validation test
0.675979381443
0.66534939759
0.710404445817
0.687139159865
0.675918942933
58.522
batch start
#iterations: 100
currently lose_sum: 89.7241523861885
time_elpased: 2.848
batch start
#iterations: 101
currently lose_sum: 89.72394168376923
time_elpased: 2.203
batch start
#iterations: 102
currently lose_sum: 89.50291097164154
time_elpased: 2.165
batch start
#iterations: 103
currently lose_sum: 89.17537206411362
time_elpased: 2.2
batch start
#iterations: 104
currently lose_sum: 88.61082375049591
time_elpased: 2.153
batch start
#iterations: 105
currently lose_sum: 89.47274565696716
time_elpased: 1.852
batch start
#iterations: 106
currently lose_sum: 89.35316026210785
time_elpased: 1.838
batch start
#iterations: 107
currently lose_sum: 88.87490230798721
time_elpased: 1.868
batch start
#iterations: 108
currently lose_sum: 88.61128062009811
time_elpased: 1.88
batch start
#iterations: 109
currently lose_sum: 89.37581396102905
time_elpased: 1.865
batch start
#iterations: 110
currently lose_sum: 88.88263005018234
time_elpased: 1.869
batch start
#iterations: 111
currently lose_sum: 89.02303272485733
time_elpased: 1.876
batch start
#iterations: 112
currently lose_sum: 88.73901975154877
time_elpased: 1.871
batch start
#iterations: 113
currently lose_sum: 88.86682456731796
time_elpased: 1.905
batch start
#iterations: 114
currently lose_sum: 88.86121737957001
time_elpased: 1.842
batch start
#iterations: 115
currently lose_sum: 88.37550926208496
time_elpased: 1.85
batch start
#iterations: 116
currently lose_sum: 88.76436626911163
time_elpased: 1.872
batch start
#iterations: 117
currently lose_sum: 88.66647279262543
time_elpased: 1.898
batch start
#iterations: 118
currently lose_sum: 88.43932050466537
time_elpased: 1.908
batch start
#iterations: 119
currently lose_sum: 88.59989726543427
time_elpased: 1.884
start validation test
0.662010309278
0.646676568882
0.716887928373
0.679974620528
0.661913963154
58.606
batch start
#iterations: 120
currently lose_sum: 88.19087564945221
time_elpased: 1.853
batch start
#iterations: 121
currently lose_sum: 88.33155834674835
time_elpased: 1.904
batch start
#iterations: 122
currently lose_sum: 88.2113026380539
time_elpased: 1.861
batch start
#iterations: 123
currently lose_sum: 88.58286905288696
time_elpased: 1.81
batch start
#iterations: 124
currently lose_sum: 88.78811311721802
time_elpased: 1.957
batch start
#iterations: 125
currently lose_sum: 88.3428156375885
time_elpased: 1.935
batch start
#iterations: 126
currently lose_sum: 87.91858494281769
time_elpased: 1.837
batch start
#iterations: 127
currently lose_sum: 87.76441824436188
time_elpased: 1.869
batch start
#iterations: 128
currently lose_sum: 87.8780968785286
time_elpased: 1.843
batch start
#iterations: 129
currently lose_sum: 87.45384329557419
time_elpased: 1.865
batch start
#iterations: 130
currently lose_sum: 87.18590271472931
time_elpased: 1.875
batch start
#iterations: 131
currently lose_sum: 87.78038036823273
time_elpased: 1.915
batch start
#iterations: 132
currently lose_sum: 87.83892303705215
time_elpased: 1.862
batch start
#iterations: 133
currently lose_sum: 87.31103080511093
time_elpased: 1.918
batch start
#iterations: 134
currently lose_sum: 86.958711206913
time_elpased: 1.897
batch start
#iterations: 135
currently lose_sum: 88.2168670296669
time_elpased: 1.861
batch start
#iterations: 136
currently lose_sum: 87.40039891004562
time_elpased: 1.939
batch start
#iterations: 137
currently lose_sum: 87.11795192956924
time_elpased: 1.952
batch start
#iterations: 138
currently lose_sum: 86.98517119884491
time_elpased: 1.965
batch start
#iterations: 139
currently lose_sum: 86.89816069602966
time_elpased: 1.94
start validation test
0.665670103093
0.65343337449
0.708037460121
0.6796404228
0.665595720663
58.511
batch start
#iterations: 140
currently lose_sum: 86.70189374685287
time_elpased: 1.965
batch start
#iterations: 141
currently lose_sum: 86.94919049739838
time_elpased: 2.169
batch start
#iterations: 142
currently lose_sum: 86.60098958015442
time_elpased: 2.057
batch start
#iterations: 143
currently lose_sum: 86.35650759935379
time_elpased: 1.86
batch start
#iterations: 144
currently lose_sum: 86.31600832939148
time_elpased: 1.881
batch start
#iterations: 145
currently lose_sum: 86.60354834794998
time_elpased: 1.896
batch start
#iterations: 146
currently lose_sum: 86.83010405302048
time_elpased: 1.896
batch start
#iterations: 147
currently lose_sum: 86.0679681301117
time_elpased: 1.865
batch start
#iterations: 148
currently lose_sum: 86.42222082614899
time_elpased: 1.873
batch start
#iterations: 149
currently lose_sum: 86.51950681209564
time_elpased: 1.839
batch start
#iterations: 150
currently lose_sum: 85.8729157447815
time_elpased: 1.859
batch start
#iterations: 151
currently lose_sum: 85.93004268407822
time_elpased: 1.841
batch start
#iterations: 152
currently lose_sum: 86.35453355312347
time_elpased: 1.848
batch start
#iterations: 153
currently lose_sum: 85.66192317008972
time_elpased: 1.839
batch start
#iterations: 154
currently lose_sum: 86.13177067041397
time_elpased: 1.897
batch start
#iterations: 155
currently lose_sum: 85.84599751234055
time_elpased: 1.898
batch start
#iterations: 156
currently lose_sum: 85.90489494800568
time_elpased: 1.909
batch start
#iterations: 157
currently lose_sum: 85.43900084495544
time_elpased: 1.812
batch start
#iterations: 158
currently lose_sum: 85.5394800901413
time_elpased: 1.856
batch start
#iterations: 159
currently lose_sum: 85.82447355985641
time_elpased: 1.824
start validation test
0.647474226804
0.657785087719
0.617371616754
0.636937941286
0.647527076579
60.164
batch start
#iterations: 160
currently lose_sum: 85.57043272256851
time_elpased: 1.854
batch start
#iterations: 161
currently lose_sum: 84.91304612159729
time_elpased: 1.878
batch start
#iterations: 162
currently lose_sum: 85.49928456544876
time_elpased: 1.849
batch start
#iterations: 163
currently lose_sum: 85.07742726802826
time_elpased: 1.824
batch start
#iterations: 164
currently lose_sum: 84.87266683578491
time_elpased: 1.872
batch start
#iterations: 165
currently lose_sum: 84.93387645483017
time_elpased: 1.904
batch start
#iterations: 166
currently lose_sum: 84.9512409567833
time_elpased: 1.836
batch start
#iterations: 167
currently lose_sum: 84.3829353749752
time_elpased: 1.906
batch start
#iterations: 168
currently lose_sum: 84.58960169553757
time_elpased: 1.942
batch start
#iterations: 169
currently lose_sum: 84.01199322938919
time_elpased: 1.929
batch start
#iterations: 170
currently lose_sum: 83.92740231752396
time_elpased: 1.951
batch start
#iterations: 171
currently lose_sum: 84.05864417552948
time_elpased: 1.949
batch start
#iterations: 172
currently lose_sum: 83.72716838121414
time_elpased: 1.958
batch start
#iterations: 173
currently lose_sum: 83.85216903686523
time_elpased: 2.108
batch start
#iterations: 174
currently lose_sum: 84.16312929987907
time_elpased: 2.014
batch start
#iterations: 175
currently lose_sum: 83.61172544956207
time_elpased: 1.924
batch start
#iterations: 176
currently lose_sum: 84.06475949287415
time_elpased: 1.902
batch start
#iterations: 177
currently lose_sum: 83.47733747959137
time_elpased: 1.863
batch start
#iterations: 178
currently lose_sum: 83.34787344932556
time_elpased: 1.916
batch start
#iterations: 179
currently lose_sum: 83.25388890504837
time_elpased: 1.876
start validation test
0.643969072165
0.648803219657
0.630441494288
0.63949057884
0.643992821914
61.315
batch start
#iterations: 180
currently lose_sum: 83.10900634527206
time_elpased: 2.053
batch start
#iterations: 181
currently lose_sum: 83.23357412219048
time_elpased: 1.857
batch start
#iterations: 182
currently lose_sum: 82.87698340415955
time_elpased: 2.072
batch start
#iterations: 183
currently lose_sum: 82.5141626894474
time_elpased: 3.163
batch start
#iterations: 184
currently lose_sum: 82.56524461507797
time_elpased: 2.651
batch start
#iterations: 185
currently lose_sum: 82.98771896958351
time_elpased: 2.745
batch start
#iterations: 186
currently lose_sum: 82.98047924041748
time_elpased: 2.114
batch start
#iterations: 187
currently lose_sum: 82.40337190032005
time_elpased: 1.928
batch start
#iterations: 188
currently lose_sum: 81.99833929538727
time_elpased: 1.877
batch start
#iterations: 189
currently lose_sum: 82.23129296302795
time_elpased: 1.87
batch start
#iterations: 190
currently lose_sum: 82.15997511148453
time_elpased: 1.906
batch start
#iterations: 191
currently lose_sum: 81.86589580774307
time_elpased: 1.937
batch start
#iterations: 192
currently lose_sum: 81.43703877925873
time_elpased: 1.963
batch start
#iterations: 193
currently lose_sum: 81.94379621744156
time_elpased: 1.982
batch start
#iterations: 194
currently lose_sum: 81.4556754231453
time_elpased: 1.989
batch start
#iterations: 195
currently lose_sum: 81.42899784445763
time_elpased: 1.909
batch start
#iterations: 196
currently lose_sum: 81.62062275409698
time_elpased: 2.157
batch start
#iterations: 197
currently lose_sum: 81.37549787759781
time_elpased: 1.892
batch start
#iterations: 198
currently lose_sum: 81.25476908683777
time_elpased: 1.856
batch start
#iterations: 199
currently lose_sum: 81.88281440734863
time_elpased: 1.885
start validation test
0.641804123711
0.655435759209
0.600596892045
0.626819182643
0.641876469362
63.659
batch start
#iterations: 200
currently lose_sum: 81.2644993364811
time_elpased: 1.886
batch start
#iterations: 201
currently lose_sum: 80.86173665523529
time_elpased: 1.89
batch start
#iterations: 202
currently lose_sum: 81.28600934147835
time_elpased: 1.901
batch start
#iterations: 203
currently lose_sum: 80.44794139266014
time_elpased: 1.859
batch start
#iterations: 204
currently lose_sum: 80.46241149306297
time_elpased: 1.858
batch start
#iterations: 205
currently lose_sum: 80.06888502836227
time_elpased: 1.891
batch start
#iterations: 206
currently lose_sum: 80.36543613672256
time_elpased: 1.898
batch start
#iterations: 207
currently lose_sum: 80.64712062478065
time_elpased: 1.913
batch start
#iterations: 208
currently lose_sum: 79.44801738858223
time_elpased: 1.963
batch start
#iterations: 209
currently lose_sum: 80.3040489256382
time_elpased: 1.881
batch start
#iterations: 210
currently lose_sum: 79.40525844693184
time_elpased: 1.899
batch start
#iterations: 211
currently lose_sum: 79.42649203538895
time_elpased: 1.845
batch start
#iterations: 212
currently lose_sum: 79.4954961836338
time_elpased: 1.924
batch start
#iterations: 213
currently lose_sum: 79.2530729174614
time_elpased: 1.856
batch start
#iterations: 214
currently lose_sum: 79.1503504216671
time_elpased: 1.915
batch start
#iterations: 215
currently lose_sum: 79.35963463783264
time_elpased: 1.862
batch start
#iterations: 216
currently lose_sum: 79.31634593009949
time_elpased: 1.883
batch start
#iterations: 217
currently lose_sum: 79.19704937934875
time_elpased: 1.909
batch start
#iterations: 218
currently lose_sum: 79.05765274167061
time_elpased: 1.949
batch start
#iterations: 219
currently lose_sum: 78.85000193119049
time_elpased: 1.829
start validation test
0.633453608247
0.654971455756
0.566738705362
0.607668965517
0.63357073655
67.085
batch start
#iterations: 220
currently lose_sum: 78.49259832501411
time_elpased: 1.873
batch start
#iterations: 221
currently lose_sum: 78.29001933336258
time_elpased: 1.895
batch start
#iterations: 222
currently lose_sum: 78.47347828745842
time_elpased: 1.868
batch start
#iterations: 223
currently lose_sum: 78.51440697908401
time_elpased: 1.846
batch start
#iterations: 224
currently lose_sum: 78.0343424975872
time_elpased: 1.813
batch start
#iterations: 225
currently lose_sum: 78.05724442005157
time_elpased: 1.827
batch start
#iterations: 226
currently lose_sum: 77.98256042599678
time_elpased: 1.843
batch start
#iterations: 227
currently lose_sum: 76.87606275081635
time_elpased: 1.935
batch start
#iterations: 228
currently lose_sum: 77.3998920917511
time_elpased: 1.801
batch start
#iterations: 229
currently lose_sum: 77.4391179382801
time_elpased: 1.885
batch start
#iterations: 230
currently lose_sum: 76.8153440952301
time_elpased: 1.872
batch start
#iterations: 231
currently lose_sum: 76.45067030191422
time_elpased: 1.808
batch start
#iterations: 232
currently lose_sum: 76.14551419019699
time_elpased: 1.86
batch start
#iterations: 233
currently lose_sum: 76.91225159168243
time_elpased: 1.923
batch start
#iterations: 234
currently lose_sum: 76.8097670674324
time_elpased: 1.871
batch start
#iterations: 235
currently lose_sum: 76.45790535211563
time_elpased: 1.841
batch start
#iterations: 236
currently lose_sum: 76.99077090620995
time_elpased: 1.901
batch start
#iterations: 237
currently lose_sum: 76.06139752268791
time_elpased: 1.961
batch start
#iterations: 238
currently lose_sum: 75.98298847675323
time_elpased: 1.853
batch start
#iterations: 239
currently lose_sum: 76.24019202589989
time_elpased: 1.856
start validation test
0.624742268041
0.650043098141
0.543274673253
0.591882498038
0.624885296969
68.810
batch start
#iterations: 240
currently lose_sum: 75.99983888864517
time_elpased: 1.881
batch start
#iterations: 241
currently lose_sum: 75.83273407816887
time_elpased: 1.89
batch start
#iterations: 242
currently lose_sum: 75.34654134511948
time_elpased: 1.841
batch start
#iterations: 243
currently lose_sum: 74.9461010992527
time_elpased: 1.857
batch start
#iterations: 244
currently lose_sum: 75.92560213804245
time_elpased: 1.88
batch start
#iterations: 245
currently lose_sum: 74.6566099524498
time_elpased: 1.856
batch start
#iterations: 246
currently lose_sum: 75.48467019200325
time_elpased: 1.855
batch start
#iterations: 247
currently lose_sum: 74.52884182333946
time_elpased: 1.873
batch start
#iterations: 248
currently lose_sum: 74.7808835208416
time_elpased: 1.893
batch start
#iterations: 249
currently lose_sum: 74.63479033112526
time_elpased: 1.869
batch start
#iterations: 250
currently lose_sum: 73.55955457687378
time_elpased: 1.874
batch start
#iterations: 251
currently lose_sum: 74.34903731942177
time_elpased: 1.904
batch start
#iterations: 252
currently lose_sum: 74.060525983572
time_elpased: 1.896
batch start
#iterations: 253
currently lose_sum: 73.60915192961693
time_elpased: 1.892
batch start
#iterations: 254
currently lose_sum: 73.84890869259834
time_elpased: 1.959
batch start
#iterations: 255
currently lose_sum: 73.63268631696701
time_elpased: 1.969
batch start
#iterations: 256
currently lose_sum: 73.00376343727112
time_elpased: 1.891
batch start
#iterations: 257
currently lose_sum: 73.44422540068626
time_elpased: 1.951
batch start
#iterations: 258
currently lose_sum: 73.18136265873909
time_elpased: 1.858
batch start
#iterations: 259
currently lose_sum: 72.6676649749279
time_elpased: 1.909
start validation test
0.611804123711
0.651595006935
0.483482556345
0.555089501979
0.612029412015
74.217
batch start
#iterations: 260
currently lose_sum: 72.885547041893
time_elpased: 1.894
batch start
#iterations: 261
currently lose_sum: 72.38698890805244
time_elpased: 1.891
batch start
#iterations: 262
currently lose_sum: 73.02136039733887
time_elpased: 2.197
batch start
#iterations: 263
currently lose_sum: 72.54391279816628
time_elpased: 2.331
batch start
#iterations: 264
currently lose_sum: 71.43208831548691
time_elpased: 2.388
batch start
#iterations: 265
currently lose_sum: 71.75363400578499
time_elpased: 2.315
batch start
#iterations: 266
currently lose_sum: 71.18109726905823
time_elpased: 2.142
batch start
#iterations: 267
currently lose_sum: 72.40374758839607
time_elpased: 2.488
batch start
#iterations: 268
currently lose_sum: 71.30271697044373
time_elpased: 2.523
batch start
#iterations: 269
currently lose_sum: 71.38205376267433
time_elpased: 2.254
batch start
#iterations: 270
currently lose_sum: 72.31598898768425
time_elpased: 2.675
batch start
#iterations: 271
currently lose_sum: 70.5350878238678
time_elpased: 2.974
batch start
#iterations: 272
currently lose_sum: 71.93390008807182
time_elpased: 2.946
batch start
#iterations: 273
currently lose_sum: 71.15639612078667
time_elpased: 3.046
batch start
#iterations: 274
currently lose_sum: 70.96306759119034
time_elpased: 2.451
batch start
#iterations: 275
currently lose_sum: 70.5163294672966
time_elpased: 3.004
batch start
#iterations: 276
currently lose_sum: 70.05510139465332
time_elpased: 2.361
batch start
#iterations: 277
currently lose_sum: 70.5212008357048
time_elpased: 2.412
batch start
#iterations: 278
currently lose_sum: 69.87269309163094
time_elpased: 2.777
batch start
#iterations: 279
currently lose_sum: 69.45577880740166
time_elpased: 3.277
start validation test
0.606443298969
0.653675819309
0.455696202532
0.537020192832
0.606707958748
79.547
batch start
#iterations: 280
currently lose_sum: 69.85075747966766
time_elpased: 1.948
batch start
#iterations: 281
currently lose_sum: 70.13568645715714
time_elpased: 2.437
batch start
#iterations: 282
currently lose_sum: 68.66788175702095
time_elpased: 2.012
batch start
#iterations: 283
currently lose_sum: 68.96076685190201
time_elpased: 2.041
batch start
#iterations: 284
currently lose_sum: 68.53553178906441
time_elpased: 2.121
batch start
#iterations: 285
currently lose_sum: 68.6301035284996
time_elpased: 2.751
batch start
#iterations: 286
currently lose_sum: 69.57766410708427
time_elpased: 3.072
batch start
#iterations: 287
currently lose_sum: 68.5559791624546
time_elpased: 2.212
batch start
#iterations: 288
currently lose_sum: 67.90641349554062
time_elpased: 1.937
batch start
#iterations: 289
currently lose_sum: 68.09886544942856
time_elpased: 1.992
batch start
#iterations: 290
currently lose_sum: 67.81264185905457
time_elpased: 1.999
batch start
#iterations: 291
currently lose_sum: 68.92410844564438
time_elpased: 1.978
batch start
#iterations: 292
currently lose_sum: 67.51317325234413
time_elpased: 1.976
batch start
#iterations: 293
currently lose_sum: 66.64167559146881
time_elpased: 2.207
batch start
#iterations: 294
currently lose_sum: 67.28559359908104
time_elpased: 2.196
batch start
#iterations: 295
currently lose_sum: 67.02984708547592
time_elpased: 2.013
batch start
#iterations: 296
currently lose_sum: 68.87453272938728
time_elpased: 1.943
batch start
#iterations: 297
currently lose_sum: 67.31223651766777
time_elpased: 1.978
batch start
#iterations: 298
currently lose_sum: 67.05170658230782
time_elpased: 1.891
batch start
#iterations: 299
currently lose_sum: 66.21512335538864
time_elpased: 2.798
start validation test
0.604536082474
0.648038222094
0.460635998765
0.538498556304
0.604788721266
85.292
batch start
#iterations: 300
currently lose_sum: 66.59615075588226
time_elpased: 2.605
batch start
#iterations: 301
currently lose_sum: 66.53992438316345
time_elpased: 2.346
batch start
#iterations: 302
currently lose_sum: 66.53242102265358
time_elpased: 2.004
batch start
#iterations: 303
currently lose_sum: 66.15246567130089
time_elpased: 1.996
batch start
#iterations: 304
currently lose_sum: 65.47580027580261
time_elpased: 1.925
batch start
#iterations: 305
currently lose_sum: 65.62377658486366
time_elpased: 1.982
batch start
#iterations: 306
currently lose_sum: 66.10250553488731
time_elpased: 2.206
batch start
#iterations: 307
currently lose_sum: 65.30988010764122
time_elpased: 1.948
batch start
#iterations: 308
currently lose_sum: 65.47648146748543
time_elpased: 1.891
batch start
#iterations: 309
currently lose_sum: 65.10223200917244
time_elpased: 1.958
batch start
#iterations: 310
currently lose_sum: 65.16349160671234
time_elpased: 2.073
batch start
#iterations: 311
currently lose_sum: 64.19955426454544
time_elpased: 2.139
batch start
#iterations: 312
currently lose_sum: 64.77629968523979
time_elpased: 2.678
batch start
#iterations: 313
currently lose_sum: 63.68078127503395
time_elpased: 3.358
batch start
#iterations: 314
currently lose_sum: 64.63264155387878
time_elpased: 3.182
batch start
#iterations: 315
currently lose_sum: 64.10887849330902
time_elpased: 2.551
batch start
#iterations: 316
currently lose_sum: 63.63563475012779
time_elpased: 3.14
batch start
#iterations: 317
currently lose_sum: 63.694867342710495
time_elpased: 3.723
batch start
#iterations: 318
currently lose_sum: 63.44943490624428
time_elpased: 2.574
batch start
#iterations: 319
currently lose_sum: 62.83155843615532
time_elpased: 2.537
start validation test
0.585721649485
0.635746606335
0.404857466296
0.494687205281
0.586039184455
90.119
batch start
#iterations: 320
currently lose_sum: 63.806680887937546
time_elpased: 3.026
batch start
#iterations: 321
currently lose_sum: 62.93445187807083
time_elpased: 2.5
batch start
#iterations: 322
currently lose_sum: 62.177835553884506
time_elpased: 2.349
batch start
#iterations: 323
currently lose_sum: 62.568972647190094
time_elpased: 2.871
batch start
#iterations: 324
currently lose_sum: 62.74374148249626
time_elpased: 3.433
batch start
#iterations: 325
currently lose_sum: 62.62891763448715
time_elpased: 2.709
batch start
#iterations: 326
currently lose_sum: 61.79121485352516
time_elpased: 2.152
batch start
#iterations: 327
currently lose_sum: 62.092942893505096
time_elpased: 1.913
batch start
#iterations: 328
currently lose_sum: 62.161103278398514
time_elpased: 1.865
batch start
#iterations: 329
currently lose_sum: 62.41465497016907
time_elpased: 1.879
batch start
#iterations: 330
currently lose_sum: 61.65253493189812
time_elpased: 1.886
batch start
#iterations: 331
currently lose_sum: 60.80704939365387
time_elpased: 1.907
batch start
#iterations: 332
currently lose_sum: 61.51743644475937
time_elpased: 1.926
batch start
#iterations: 333
currently lose_sum: 60.763022631406784
time_elpased: 1.912
batch start
#iterations: 334
currently lose_sum: 61.30593481659889
time_elpased: 1.902
batch start
#iterations: 335
currently lose_sum: 60.96183469891548
time_elpased: 1.944
batch start
#iterations: 336
currently lose_sum: 60.746629267930984
time_elpased: 1.92
batch start
#iterations: 337
currently lose_sum: 60.89609515666962
time_elpased: 1.891
batch start
#iterations: 338
currently lose_sum: 60.43699249625206
time_elpased: 1.845
batch start
#iterations: 339
currently lose_sum: 60.8776490688324
time_elpased: 1.95
start validation test
0.582371134021
0.629096722622
0.404960378718
0.492737290258
0.582682605965
96.548
batch start
#iterations: 340
currently lose_sum: 60.252467185258865
time_elpased: 1.959
batch start
#iterations: 341
currently lose_sum: 60.10947245359421
time_elpased: 2.024
batch start
#iterations: 342
currently lose_sum: 59.99226254224777
time_elpased: 2.224
batch start
#iterations: 343
currently lose_sum: 60.29686385393143
time_elpased: 2.036
batch start
#iterations: 344
currently lose_sum: 60.0505231320858
time_elpased: 1.873
batch start
#iterations: 345
currently lose_sum: 59.3041807115078
time_elpased: 1.906
batch start
#iterations: 346
currently lose_sum: 59.01712262630463
time_elpased: 1.958
batch start
#iterations: 347
currently lose_sum: 59.44352725148201
time_elpased: 2.17
batch start
#iterations: 348
currently lose_sum: 59.621841073036194
time_elpased: 2.439
batch start
#iterations: 349
currently lose_sum: 58.82429078221321
time_elpased: 2.368
batch start
#iterations: 350
currently lose_sum: 58.29056856036186
time_elpased: 2.387
batch start
#iterations: 351
currently lose_sum: 58.12037035822868
time_elpased: 2.92
batch start
#iterations: 352
currently lose_sum: 57.080497950315475
time_elpased: 2.56
batch start
#iterations: 353
currently lose_sum: 58.13111424446106
time_elpased: 1.882
batch start
#iterations: 354
currently lose_sum: 58.4927434027195
time_elpased: 1.911
batch start
#iterations: 355
currently lose_sum: 58.24350115656853
time_elpased: 1.872
batch start
#iterations: 356
currently lose_sum: 57.771674662828445
time_elpased: 1.884
batch start
#iterations: 357
currently lose_sum: 57.18837335705757
time_elpased: 1.961
batch start
#iterations: 358
currently lose_sum: 57.98288628458977
time_elpased: 2.032
batch start
#iterations: 359
currently lose_sum: 57.46134829521179
time_elpased: 2.051
start validation test
0.576443298969
0.633785230111
0.365647833694
0.463747307968
0.576813382921
104.572
batch start
#iterations: 360
currently lose_sum: 57.4705465734005
time_elpased: 2.226
batch start
#iterations: 361
currently lose_sum: 55.934894323349
time_elpased: 1.872
batch start
#iterations: 362
currently lose_sum: 57.308042734861374
time_elpased: 1.876
batch start
#iterations: 363
currently lose_sum: 56.52160358428955
time_elpased: 1.859
batch start
#iterations: 364
currently lose_sum: 56.57543361186981
time_elpased: 1.92
batch start
#iterations: 365
currently lose_sum: 55.556156784296036
time_elpased: 1.9
batch start
#iterations: 366
currently lose_sum: 56.08562949299812
time_elpased: 1.863
batch start
#iterations: 367
currently lose_sum: 55.0245600938797
time_elpased: 1.847
batch start
#iterations: 368
currently lose_sum: 55.4982576072216
time_elpased: 1.887
batch start
#iterations: 369
currently lose_sum: 55.22824662923813
time_elpased: 1.903
batch start
#iterations: 370
currently lose_sum: 55.43176734447479
time_elpased: 1.903
batch start
#iterations: 371
currently lose_sum: 55.76551562547684
time_elpased: 1.903
batch start
#iterations: 372
currently lose_sum: 54.71731421351433
time_elpased: 1.875
batch start
#iterations: 373
currently lose_sum: 54.62026181817055
time_elpased: 1.857
batch start
#iterations: 374
currently lose_sum: 55.33796098828316
time_elpased: 1.831
batch start
#iterations: 375
currently lose_sum: 54.65040647983551
time_elpased: 1.895
batch start
#iterations: 376
currently lose_sum: 55.395323276519775
time_elpased: 1.834
batch start
#iterations: 377
currently lose_sum: 54.73309049010277
time_elpased: 1.865
batch start
#iterations: 378
currently lose_sum: 53.58365532755852
time_elpased: 1.877
batch start
#iterations: 379
currently lose_sum: 54.982306092977524
time_elpased: 1.889
start validation test
0.57706185567
0.635144797998
0.365647833694
0.464110770035
0.577433025594
111.383
batch start
#iterations: 380
currently lose_sum: 53.299431174993515
time_elpased: 1.895
batch start
#iterations: 381
currently lose_sum: 53.692805767059326
time_elpased: 1.928
batch start
#iterations: 382
currently lose_sum: 53.36836618185043
time_elpased: 1.858
batch start
#iterations: 383
currently lose_sum: 53.604225635528564
time_elpased: 1.912
batch start
#iterations: 384
currently lose_sum: 54.07865187525749
time_elpased: 1.895
batch start
#iterations: 385
currently lose_sum: 53.5551715195179
time_elpased: 1.903
batch start
#iterations: 386
currently lose_sum: 52.75516137480736
time_elpased: 1.92
batch start
#iterations: 387
currently lose_sum: 53.251193314790726
time_elpased: 1.89
batch start
#iterations: 388
currently lose_sum: 53.38246497511864
time_elpased: 1.916
batch start
#iterations: 389
currently lose_sum: 53.36017379164696
time_elpased: 1.981
batch start
#iterations: 390
currently lose_sum: 52.301024347543716
time_elpased: 2.002
batch start
#iterations: 391
currently lose_sum: 53.33034631609917
time_elpased: 1.857
batch start
#iterations: 392
currently lose_sum: 52.417388558387756
time_elpased: 2.039
batch start
#iterations: 393
currently lose_sum: 51.58350247144699
time_elpased: 2.102
batch start
#iterations: 394
currently lose_sum: 51.741651713848114
time_elpased: 2.41
batch start
#iterations: 395
currently lose_sum: 52.543323904275894
time_elpased: 2.261
batch start
#iterations: 396
currently lose_sum: 50.59133565425873
time_elpased: 2.273
batch start
#iterations: 397
currently lose_sum: 50.87175107002258
time_elpased: 2.654
batch start
#iterations: 398
currently lose_sum: 51.02253195643425
time_elpased: 2.159
batch start
#iterations: 399
currently lose_sum: 51.75263258814812
time_elpased: 2.33
start validation test
0.566340206186
0.639495079161
0.307605227951
0.415398512959
0.566794455347
124.316
acc: 0.667
pre: 0.654
rec: 0.709
F1: 0.680
auc: 0.667
