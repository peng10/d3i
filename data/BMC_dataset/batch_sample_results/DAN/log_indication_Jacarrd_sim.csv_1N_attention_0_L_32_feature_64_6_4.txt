start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.2416523694992
time_elpased: 1.886
batch start
#iterations: 1
currently lose_sum: 99.61273914575577
time_elpased: 1.903
batch start
#iterations: 2
currently lose_sum: 99.7147827744484
time_elpased: 1.885
batch start
#iterations: 3
currently lose_sum: 98.87699502706528
time_elpased: 1.869
batch start
#iterations: 4
currently lose_sum: 98.85013514757156
time_elpased: 1.893
batch start
#iterations: 5
currently lose_sum: 98.16587692499161
time_elpased: 1.851
batch start
#iterations: 6
currently lose_sum: 97.82426065206528
time_elpased: 1.913
batch start
#iterations: 7
currently lose_sum: 98.00673168897629
time_elpased: 1.856
batch start
#iterations: 8
currently lose_sum: 97.77280801534653
time_elpased: 1.873
batch start
#iterations: 9
currently lose_sum: 96.91254097223282
time_elpased: 1.871
batch start
#iterations: 10
currently lose_sum: 97.30573296546936
time_elpased: 1.868
batch start
#iterations: 11
currently lose_sum: 97.02068716287613
time_elpased: 1.841
batch start
#iterations: 12
currently lose_sum: 97.20238679647446
time_elpased: 1.854
batch start
#iterations: 13
currently lose_sum: 96.79419857263565
time_elpased: 1.845
batch start
#iterations: 14
currently lose_sum: 96.3968306183815
time_elpased: 1.81
batch start
#iterations: 15
currently lose_sum: 96.55277067422867
time_elpased: 1.876
batch start
#iterations: 16
currently lose_sum: 96.33444738388062
time_elpased: 1.866
batch start
#iterations: 17
currently lose_sum: 95.89651036262512
time_elpased: 1.854
batch start
#iterations: 18
currently lose_sum: 95.74718856811523
time_elpased: 1.927
batch start
#iterations: 19
currently lose_sum: 95.39661431312561
time_elpased: 1.857
start validation test
0.630567010309
0.621013667426
0.673355974066
0.646126499778
0.630491887684
61.803
batch start
#iterations: 20
currently lose_sum: 95.78000152111053
time_elpased: 1.845
batch start
#iterations: 21
currently lose_sum: 95.19423192739487
time_elpased: 1.868
batch start
#iterations: 22
currently lose_sum: 95.42833912372589
time_elpased: 1.919
batch start
#iterations: 23
currently lose_sum: 94.96597760915756
time_elpased: 1.839
batch start
#iterations: 24
currently lose_sum: 94.99977213144302
time_elpased: 1.917
batch start
#iterations: 25
currently lose_sum: 94.81548297405243
time_elpased: 1.851
batch start
#iterations: 26
currently lose_sum: 94.72502988576889
time_elpased: 1.828
batch start
#iterations: 27
currently lose_sum: 93.98501056432724
time_elpased: 1.863
batch start
#iterations: 28
currently lose_sum: 94.54358452558517
time_elpased: 1.889
batch start
#iterations: 29
currently lose_sum: 93.94497013092041
time_elpased: 1.886
batch start
#iterations: 30
currently lose_sum: 94.26508033275604
time_elpased: 1.852
batch start
#iterations: 31
currently lose_sum: 93.89891242980957
time_elpased: 1.888
batch start
#iterations: 32
currently lose_sum: 93.98113238811493
time_elpased: 1.824
batch start
#iterations: 33
currently lose_sum: 94.10048204660416
time_elpased: 1.828
batch start
#iterations: 34
currently lose_sum: 93.27835142612457
time_elpased: 1.868
batch start
#iterations: 35
currently lose_sum: 93.36325067281723
time_elpased: 1.852
batch start
#iterations: 36
currently lose_sum: 93.67796301841736
time_elpased: 1.862
batch start
#iterations: 37
currently lose_sum: 93.16765516996384
time_elpased: 1.859
batch start
#iterations: 38
currently lose_sum: 93.06143301725388
time_elpased: 1.857
batch start
#iterations: 39
currently lose_sum: 93.31649667024612
time_elpased: 1.904
start validation test
0.668556701031
0.64581669772
0.749099516312
0.693634457785
0.668415295696
59.379
batch start
#iterations: 40
currently lose_sum: 92.94103443622589
time_elpased: 1.91
batch start
#iterations: 41
currently lose_sum: 92.73680907487869
time_elpased: 1.855
batch start
#iterations: 42
currently lose_sum: 92.78989094495773
time_elpased: 1.897
batch start
#iterations: 43
currently lose_sum: 92.72851324081421
time_elpased: 1.871
batch start
#iterations: 44
currently lose_sum: 92.67759203910828
time_elpased: 1.906
batch start
#iterations: 45
currently lose_sum: 92.55158138275146
time_elpased: 1.891
batch start
#iterations: 46
currently lose_sum: 92.50779873132706
time_elpased: 1.853
batch start
#iterations: 47
currently lose_sum: 92.75075995922089
time_elpased: 1.844
batch start
#iterations: 48
currently lose_sum: 92.16164469718933
time_elpased: 1.816
batch start
#iterations: 49
currently lose_sum: 92.55957579612732
time_elpased: 1.871
batch start
#iterations: 50
currently lose_sum: 92.13694089651108
time_elpased: 1.843
batch start
#iterations: 51
currently lose_sum: 91.83834731578827
time_elpased: 1.932
batch start
#iterations: 52
currently lose_sum: 92.13739269971848
time_elpased: 1.879
batch start
#iterations: 53
currently lose_sum: 91.40427434444427
time_elpased: 1.843
batch start
#iterations: 54
currently lose_sum: 91.85806494951248
time_elpased: 1.819
batch start
#iterations: 55
currently lose_sum: 91.68534344434738
time_elpased: 1.874
batch start
#iterations: 56
currently lose_sum: 91.71959060430527
time_elpased: 1.836
batch start
#iterations: 57
currently lose_sum: 91.46513658761978
time_elpased: 1.931
batch start
#iterations: 58
currently lose_sum: 91.35962361097336
time_elpased: 1.86
batch start
#iterations: 59
currently lose_sum: 91.73718184232712
time_elpased: 1.821
start validation test
0.661082474227
0.640720171981
0.736132551199
0.685120444423
0.660950712241
59.040
batch start
#iterations: 60
currently lose_sum: 91.433374106884
time_elpased: 1.847
batch start
#iterations: 61
currently lose_sum: 91.72150403261185
time_elpased: 1.869
batch start
#iterations: 62
currently lose_sum: 91.02568304538727
time_elpased: 1.914
batch start
#iterations: 63
currently lose_sum: 91.15018045902252
time_elpased: 1.851
batch start
#iterations: 64
currently lose_sum: 91.69146257638931
time_elpased: 1.856
batch start
#iterations: 65
currently lose_sum: 90.78768408298492
time_elpased: 1.839
batch start
#iterations: 66
currently lose_sum: 90.7582466006279
time_elpased: 1.832
batch start
#iterations: 67
currently lose_sum: 90.75726789236069
time_elpased: 1.828
batch start
#iterations: 68
currently lose_sum: 90.69293546676636
time_elpased: 1.814
batch start
#iterations: 69
currently lose_sum: 91.18206715583801
time_elpased: 1.922
batch start
#iterations: 70
currently lose_sum: 90.95430123806
time_elpased: 1.804
batch start
#iterations: 71
currently lose_sum: 90.4494354724884
time_elpased: 1.82
batch start
#iterations: 72
currently lose_sum: 90.45527982711792
time_elpased: 1.872
batch start
#iterations: 73
currently lose_sum: 90.01347678899765
time_elpased: 1.827
batch start
#iterations: 74
currently lose_sum: 90.19069600105286
time_elpased: 1.857
batch start
#iterations: 75
currently lose_sum: 90.52900809049606
time_elpased: 1.816
batch start
#iterations: 76
currently lose_sum: 89.78449338674545
time_elpased: 1.844
batch start
#iterations: 77
currently lose_sum: 90.62668025493622
time_elpased: 1.837
batch start
#iterations: 78
currently lose_sum: 89.87061536312103
time_elpased: 1.883
batch start
#iterations: 79
currently lose_sum: 89.65670359134674
time_elpased: 1.847
start validation test
0.650515463918
0.628714173021
0.73819079963
0.679068446464
0.650361536343
59.781
batch start
#iterations: 80
currently lose_sum: 90.36229258775711
time_elpased: 1.869
batch start
#iterations: 81
currently lose_sum: 90.06955510377884
time_elpased: 1.874
batch start
#iterations: 82
currently lose_sum: 90.03137636184692
time_elpased: 1.812
batch start
#iterations: 83
currently lose_sum: 90.35013687610626
time_elpased: 1.875
batch start
#iterations: 84
currently lose_sum: 90.35465222597122
time_elpased: 1.877
batch start
#iterations: 85
currently lose_sum: 89.96465933322906
time_elpased: 1.841
batch start
#iterations: 86
currently lose_sum: 89.8080649971962
time_elpased: 1.844
batch start
#iterations: 87
currently lose_sum: 89.60165685415268
time_elpased: 1.861
batch start
#iterations: 88
currently lose_sum: 89.29509860277176
time_elpased: 1.951
batch start
#iterations: 89
currently lose_sum: 89.52488076686859
time_elpased: 1.887
batch start
#iterations: 90
currently lose_sum: 89.08292400836945
time_elpased: 1.84
batch start
#iterations: 91
currently lose_sum: 89.77132332324982
time_elpased: 1.882
batch start
#iterations: 92
currently lose_sum: 88.78562051057816
time_elpased: 1.858
batch start
#iterations: 93
currently lose_sum: 89.13199245929718
time_elpased: 1.881
batch start
#iterations: 94
currently lose_sum: 89.31249630451202
time_elpased: 1.861
batch start
#iterations: 95
currently lose_sum: 89.06767857074738
time_elpased: 1.92
batch start
#iterations: 96
currently lose_sum: 88.8992812037468
time_elpased: 1.866
batch start
#iterations: 97
currently lose_sum: 89.01847302913666
time_elpased: 1.855
batch start
#iterations: 98
currently lose_sum: 88.6633311510086
time_elpased: 1.851
batch start
#iterations: 99
currently lose_sum: 89.06701797246933
time_elpased: 1.864
start validation test
0.674690721649
0.657161314138
0.73283935371
0.692940203377
0.674588632757
58.037
batch start
#iterations: 100
currently lose_sum: 88.65529239177704
time_elpased: 1.793
batch start
#iterations: 101
currently lose_sum: 88.8900271654129
time_elpased: 1.861
batch start
#iterations: 102
currently lose_sum: 88.32128691673279
time_elpased: 1.842
batch start
#iterations: 103
currently lose_sum: 88.20634704828262
time_elpased: 1.838
batch start
#iterations: 104
currently lose_sum: 88.0922846198082
time_elpased: 1.842
batch start
#iterations: 105
currently lose_sum: 88.63202583789825
time_elpased: 1.847
batch start
#iterations: 106
currently lose_sum: 88.33329713344574
time_elpased: 1.833
batch start
#iterations: 107
currently lose_sum: 88.44367706775665
time_elpased: 1.894
batch start
#iterations: 108
currently lose_sum: 88.52601963281631
time_elpased: 1.889
batch start
#iterations: 109
currently lose_sum: 88.15788578987122
time_elpased: 1.922
batch start
#iterations: 110
currently lose_sum: 88.1591192483902
time_elpased: 1.878
batch start
#iterations: 111
currently lose_sum: 88.4988551735878
time_elpased: 1.902
batch start
#iterations: 112
currently lose_sum: 87.49505549669266
time_elpased: 1.862
batch start
#iterations: 113
currently lose_sum: 87.36735063791275
time_elpased: 1.834
batch start
#iterations: 114
currently lose_sum: 88.09750908613205
time_elpased: 1.859
batch start
#iterations: 115
currently lose_sum: 87.81626278162003
time_elpased: 1.836
batch start
#iterations: 116
currently lose_sum: 87.38894128799438
time_elpased: 1.811
batch start
#iterations: 117
currently lose_sum: 87.43581759929657
time_elpased: 1.89
batch start
#iterations: 118
currently lose_sum: 87.904616355896
time_elpased: 1.841
batch start
#iterations: 119
currently lose_sum: 87.2508333325386
time_elpased: 1.856
start validation test
0.66
0.653668143772
0.683132654111
0.66807568438
0.659959387058
58.541
batch start
#iterations: 120
currently lose_sum: 87.63192355632782
time_elpased: 1.843
batch start
#iterations: 121
currently lose_sum: 87.6701712012291
time_elpased: 1.911
batch start
#iterations: 122
currently lose_sum: 87.50736117362976
time_elpased: 1.852
batch start
#iterations: 123
currently lose_sum: 87.56749588251114
time_elpased: 1.894
batch start
#iterations: 124
currently lose_sum: 87.04749590158463
time_elpased: 1.87
batch start
#iterations: 125
currently lose_sum: 87.54807013273239
time_elpased: 1.886
batch start
#iterations: 126
currently lose_sum: 86.7227394580841
time_elpased: 1.867
batch start
#iterations: 127
currently lose_sum: 86.76690655946732
time_elpased: 1.899
batch start
#iterations: 128
currently lose_sum: 86.83886337280273
time_elpased: 1.806
batch start
#iterations: 129
currently lose_sum: 87.05364054441452
time_elpased: 1.862
batch start
#iterations: 130
currently lose_sum: 86.61882954835892
time_elpased: 1.823
batch start
#iterations: 131
currently lose_sum: 87.43120265007019
time_elpased: 1.853
batch start
#iterations: 132
currently lose_sum: 87.1817102432251
time_elpased: 1.879
batch start
#iterations: 133
currently lose_sum: 86.0530417561531
time_elpased: 1.862
batch start
#iterations: 134
currently lose_sum: 86.36322718858719
time_elpased: 1.879
batch start
#iterations: 135
currently lose_sum: 87.03862655162811
time_elpased: 1.833
batch start
#iterations: 136
currently lose_sum: 87.24902880191803
time_elpased: 1.859
batch start
#iterations: 137
currently lose_sum: 86.46761530637741
time_elpased: 1.875
batch start
#iterations: 138
currently lose_sum: 85.94127058982849
time_elpased: 1.852
batch start
#iterations: 139
currently lose_sum: 86.18554097414017
time_elpased: 1.926
start validation test
0.662783505155
0.670095360549
0.643614284244
0.656587926509
0.662817159679
59.307
batch start
#iterations: 140
currently lose_sum: 86.04044675827026
time_elpased: 1.869
batch start
#iterations: 141
currently lose_sum: 86.30578380823135
time_elpased: 1.848
batch start
#iterations: 142
currently lose_sum: 85.86196804046631
time_elpased: 1.905
batch start
#iterations: 143
currently lose_sum: 85.76642102003098
time_elpased: 1.868
batch start
#iterations: 144
currently lose_sum: 86.06151008605957
time_elpased: 1.879
batch start
#iterations: 145
currently lose_sum: 85.46312934160233
time_elpased: 1.876
batch start
#iterations: 146
currently lose_sum: 86.08983701467514
time_elpased: 1.854
batch start
#iterations: 147
currently lose_sum: 85.64872670173645
time_elpased: 1.847
batch start
#iterations: 148
currently lose_sum: 85.58280178904533
time_elpased: 1.894
batch start
#iterations: 149
currently lose_sum: 85.69829231500626
time_elpased: 1.876
batch start
#iterations: 150
currently lose_sum: 85.72597849369049
time_elpased: 1.847
batch start
#iterations: 151
currently lose_sum: 85.16546702384949
time_elpased: 1.871
batch start
#iterations: 152
currently lose_sum: 85.45829838514328
time_elpased: 1.847
batch start
#iterations: 153
currently lose_sum: 84.86337965726852
time_elpased: 1.932
batch start
#iterations: 154
currently lose_sum: 84.78848177194595
time_elpased: 1.888
batch start
#iterations: 155
currently lose_sum: 85.34781968593597
time_elpased: 1.861
batch start
#iterations: 156
currently lose_sum: 84.93843710422516
time_elpased: 1.859
batch start
#iterations: 157
currently lose_sum: 84.74499136209488
time_elpased: 1.907
batch start
#iterations: 158
currently lose_sum: 85.73211866617203
time_elpased: 1.845
batch start
#iterations: 159
currently lose_sum: 84.96663051843643
time_elpased: 1.86
start validation test
0.678865979381
0.660410341338
0.738705361737
0.697367142718
0.678760922116
58.759
batch start
#iterations: 160
currently lose_sum: 84.91739583015442
time_elpased: 1.857
batch start
#iterations: 161
currently lose_sum: 84.62842845916748
time_elpased: 1.868
batch start
#iterations: 162
currently lose_sum: 83.96569830179214
time_elpased: 1.887
batch start
#iterations: 163
currently lose_sum: 84.6000623703003
time_elpased: 1.935
batch start
#iterations: 164
currently lose_sum: 84.29598462581635
time_elpased: 1.857
batch start
#iterations: 165
currently lose_sum: 83.9165136218071
time_elpased: 1.989
batch start
#iterations: 166
currently lose_sum: 83.9325744509697
time_elpased: 1.863
batch start
#iterations: 167
currently lose_sum: 84.3234429359436
time_elpased: 1.845
batch start
#iterations: 168
currently lose_sum: 83.81192725896835
time_elpased: 1.861
batch start
#iterations: 169
currently lose_sum: 83.89168360829353
time_elpased: 1.849
batch start
#iterations: 170
currently lose_sum: 83.85186797380447
time_elpased: 1.875
batch start
#iterations: 171
currently lose_sum: 83.20502346754074
time_elpased: 1.851
batch start
#iterations: 172
currently lose_sum: 83.54310119152069
time_elpased: 1.869
batch start
#iterations: 173
currently lose_sum: 83.46994978189468
time_elpased: 1.901
batch start
#iterations: 174
currently lose_sum: 83.5477711558342
time_elpased: 1.856
batch start
#iterations: 175
currently lose_sum: 83.18899357318878
time_elpased: 1.85
batch start
#iterations: 176
currently lose_sum: 83.62201422452927
time_elpased: 1.815
batch start
#iterations: 177
currently lose_sum: 83.18952637910843
time_elpased: 1.864
batch start
#iterations: 178
currently lose_sum: 83.12641948461533
time_elpased: 1.868
batch start
#iterations: 179
currently lose_sum: 83.1003879904747
time_elpased: 1.834
start validation test
0.646907216495
0.659330888074
0.610476484512
0.633963877311
0.646971176264
61.375
batch start
#iterations: 180
currently lose_sum: 83.0528182387352
time_elpased: 1.859
batch start
#iterations: 181
currently lose_sum: 82.99023705720901
time_elpased: 1.897
batch start
#iterations: 182
currently lose_sum: 83.24366754293442
time_elpased: 1.81
batch start
#iterations: 183
currently lose_sum: 82.57224524021149
time_elpased: 1.892
batch start
#iterations: 184
currently lose_sum: 82.66649675369263
time_elpased: 1.825
batch start
#iterations: 185
currently lose_sum: 82.39302253723145
time_elpased: 1.823
batch start
#iterations: 186
currently lose_sum: 82.50645381212234
time_elpased: 1.863
batch start
#iterations: 187
currently lose_sum: 82.50253269076347
time_elpased: 1.871
batch start
#iterations: 188
currently lose_sum: 82.81929683685303
time_elpased: 1.883
batch start
#iterations: 189
currently lose_sum: 81.91413849592209
time_elpased: 1.885
batch start
#iterations: 190
currently lose_sum: 82.22401612997055
time_elpased: 1.831
batch start
#iterations: 191
currently lose_sum: 81.91257789731026
time_elpased: 1.919
batch start
#iterations: 192
currently lose_sum: 82.37684068083763
time_elpased: 1.866
batch start
#iterations: 193
currently lose_sum: 82.04334303736687
time_elpased: 1.892
batch start
#iterations: 194
currently lose_sum: 81.75725251436234
time_elpased: 1.817
batch start
#iterations: 195
currently lose_sum: 81.766520768404
time_elpased: 1.913
batch start
#iterations: 196
currently lose_sum: 81.70960655808449
time_elpased: 1.89
batch start
#iterations: 197
currently lose_sum: 81.51993981003761
time_elpased: 1.887
batch start
#iterations: 198
currently lose_sum: 80.66973146796227
time_elpased: 1.852
batch start
#iterations: 199
currently lose_sum: 81.47727257013321
time_elpased: 1.851
start validation test
0.640103092784
0.643148749084
0.632293917876
0.637675142709
0.640116802995
63.812
batch start
#iterations: 200
currently lose_sum: 82.03310236334801
time_elpased: 1.861
batch start
#iterations: 201
currently lose_sum: 81.27938595414162
time_elpased: 1.861
batch start
#iterations: 202
currently lose_sum: 80.5919379889965
time_elpased: 1.849
batch start
#iterations: 203
currently lose_sum: 80.75979518890381
time_elpased: 1.882
batch start
#iterations: 204
currently lose_sum: 80.63322457671165
time_elpased: 1.916
batch start
#iterations: 205
currently lose_sum: 80.45901393890381
time_elpased: 1.85
batch start
#iterations: 206
currently lose_sum: 80.52793914079666
time_elpased: 1.858
batch start
#iterations: 207
currently lose_sum: 80.20080521702766
time_elpased: 1.82
batch start
#iterations: 208
currently lose_sum: 80.12002894282341
time_elpased: 1.893
batch start
#iterations: 209
currently lose_sum: 79.74233227968216
time_elpased: 1.861
batch start
#iterations: 210
currently lose_sum: 79.79176479578018
time_elpased: 1.896
batch start
#iterations: 211
currently lose_sum: 79.98413541913033
time_elpased: 1.876
batch start
#iterations: 212
currently lose_sum: 80.03049328923225
time_elpased: 1.81
batch start
#iterations: 213
currently lose_sum: 80.40683269500732
time_elpased: 1.852
batch start
#iterations: 214
currently lose_sum: 79.38703998923302
time_elpased: 1.846
batch start
#iterations: 215
currently lose_sum: 79.62838578224182
time_elpased: 1.911
batch start
#iterations: 216
currently lose_sum: 79.4919722378254
time_elpased: 1.908
batch start
#iterations: 217
currently lose_sum: 79.56390291452408
time_elpased: 1.819
batch start
#iterations: 218
currently lose_sum: 79.62192785739899
time_elpased: 1.879
batch start
#iterations: 219
currently lose_sum: 78.9557855129242
time_elpased: 1.9
start validation test
0.648195876289
0.65904091509
0.616651229803
0.637141793822
0.648251257781
65.574
batch start
#iterations: 220
currently lose_sum: 78.47292956709862
time_elpased: 1.893
batch start
#iterations: 221
currently lose_sum: 78.74642646312714
time_elpased: 1.842
batch start
#iterations: 222
currently lose_sum: 78.77990746498108
time_elpased: 1.828
batch start
#iterations: 223
currently lose_sum: 77.93704211711884
time_elpased: 1.842
batch start
#iterations: 224
currently lose_sum: 78.63749980926514
time_elpased: 1.853
batch start
#iterations: 225
currently lose_sum: 78.11636158823967
time_elpased: 1.879
batch start
#iterations: 226
currently lose_sum: 78.0415487587452
time_elpased: 1.876
batch start
#iterations: 227
currently lose_sum: 77.9806569814682
time_elpased: 1.859
batch start
#iterations: 228
currently lose_sum: 77.55463749170303
time_elpased: 1.843
batch start
#iterations: 229
currently lose_sum: 78.07296034693718
time_elpased: 1.859
batch start
#iterations: 230
currently lose_sum: 78.52194836735725
time_elpased: 1.87
batch start
#iterations: 231
currently lose_sum: 77.61226898431778
time_elpased: 1.863
batch start
#iterations: 232
currently lose_sum: 77.52858942747116
time_elpased: 1.856
batch start
#iterations: 233
currently lose_sum: 77.00959977507591
time_elpased: 1.836
batch start
#iterations: 234
currently lose_sum: 77.26801064610481
time_elpased: 1.855
batch start
#iterations: 235
currently lose_sum: 77.317411839962
time_elpased: 1.876
batch start
#iterations: 236
currently lose_sum: 77.17066204547882
time_elpased: 1.823
batch start
#iterations: 237
currently lose_sum: 77.10531428456306
time_elpased: 1.891
batch start
#iterations: 238
currently lose_sum: 76.41251736879349
time_elpased: 1.896
batch start
#iterations: 239
currently lose_sum: 76.71891167759895
time_elpased: 1.85
start validation test
0.642113402062
0.653361344538
0.608109498817
0.62992377805
0.642173101159
68.488
batch start
#iterations: 240
currently lose_sum: 76.60536485910416
time_elpased: 1.827
batch start
#iterations: 241
currently lose_sum: 77.2800744175911
time_elpased: 1.853
batch start
#iterations: 242
currently lose_sum: 76.3274966776371
time_elpased: 1.903
batch start
#iterations: 243
currently lose_sum: 76.83050417900085
time_elpased: 1.824
batch start
#iterations: 244
currently lose_sum: 76.01544785499573
time_elpased: 1.87
batch start
#iterations: 245
currently lose_sum: 75.97328171133995
time_elpased: 1.851
batch start
#iterations: 246
currently lose_sum: 75.9497646689415
time_elpased: 1.855
batch start
#iterations: 247
currently lose_sum: 75.28202605247498
time_elpased: 1.91
batch start
#iterations: 248
currently lose_sum: 75.770110309124
time_elpased: 1.885
batch start
#iterations: 249
currently lose_sum: 75.34330040216446
time_elpased: 1.858
batch start
#iterations: 250
currently lose_sum: 75.0643001794815
time_elpased: 1.841
batch start
#iterations: 251
currently lose_sum: 74.70315542817116
time_elpased: 1.884
batch start
#iterations: 252
currently lose_sum: 75.33383774757385
time_elpased: 1.874
batch start
#iterations: 253
currently lose_sum: 74.98960283398628
time_elpased: 1.864
batch start
#iterations: 254
currently lose_sum: 74.8290097117424
time_elpased: 1.868
batch start
#iterations: 255
currently lose_sum: 74.50155609846115
time_elpased: 1.853
batch start
#iterations: 256
currently lose_sum: 74.29579558968544
time_elpased: 1.859
batch start
#iterations: 257
currently lose_sum: 74.63687992095947
time_elpased: 1.851
batch start
#iterations: 258
currently lose_sum: 73.99121403694153
time_elpased: 1.839
batch start
#iterations: 259
currently lose_sum: 74.0752846300602
time_elpased: 1.94
start validation test
0.636804123711
0.667881835324
0.546773695585
0.60129017655
0.636962186014
76.246
batch start
#iterations: 260
currently lose_sum: 73.46319109201431
time_elpased: 1.792
batch start
#iterations: 261
currently lose_sum: 73.73391950130463
time_elpased: 1.862
batch start
#iterations: 262
currently lose_sum: 73.72051134705544
time_elpased: 1.862
batch start
#iterations: 263
currently lose_sum: 73.3078244626522
time_elpased: 1.903
batch start
#iterations: 264
currently lose_sum: 73.58335855603218
time_elpased: 1.815
batch start
#iterations: 265
currently lose_sum: 73.4586187005043
time_elpased: 1.863
batch start
#iterations: 266
currently lose_sum: 73.7504774928093
time_elpased: 1.918
batch start
#iterations: 267
currently lose_sum: 72.86560800671577
time_elpased: 1.881
batch start
#iterations: 268
currently lose_sum: 72.86456361413002
time_elpased: 1.851
batch start
#iterations: 269
currently lose_sum: 72.83439201116562
time_elpased: 1.893
batch start
#iterations: 270
currently lose_sum: 72.4066313803196
time_elpased: 1.921
batch start
#iterations: 271
currently lose_sum: 71.98218223452568
time_elpased: 1.865
batch start
#iterations: 272
currently lose_sum: 72.00146317481995
time_elpased: 1.841
batch start
#iterations: 273
currently lose_sum: 71.91755998134613
time_elpased: 1.897
batch start
#iterations: 274
currently lose_sum: 71.78655925393105
time_elpased: 1.903
batch start
#iterations: 275
currently lose_sum: 71.31472834944725
time_elpased: 1.847
batch start
#iterations: 276
currently lose_sum: 71.5555035173893
time_elpased: 1.822
batch start
#iterations: 277
currently lose_sum: 70.49051994085312
time_elpased: 1.844
batch start
#iterations: 278
currently lose_sum: 70.78411903977394
time_elpased: 1.879
batch start
#iterations: 279
currently lose_sum: 71.44621446728706
time_elpased: 1.903
start validation test
0.625
0.659774928029
0.518884429351
0.580909038539
0.625186302251
81.406
batch start
#iterations: 280
currently lose_sum: 70.86882308125496
time_elpased: 1.849
batch start
#iterations: 281
currently lose_sum: 70.71123853325844
time_elpased: 1.845
batch start
#iterations: 282
currently lose_sum: 70.50763040781021
time_elpased: 1.865
batch start
#iterations: 283
currently lose_sum: 70.02807372808456
time_elpased: 1.839
batch start
#iterations: 284
currently lose_sum: 69.9561153948307
time_elpased: 1.838
batch start
#iterations: 285
currently lose_sum: 70.31432062387466
time_elpased: 1.862
batch start
#iterations: 286
currently lose_sum: 70.40847563743591
time_elpased: 1.87
batch start
#iterations: 287
currently lose_sum: 69.52273613214493
time_elpased: 1.859
batch start
#iterations: 288
currently lose_sum: 69.20608213543892
time_elpased: 1.842
batch start
#iterations: 289
currently lose_sum: 69.1051159799099
time_elpased: 1.865
batch start
#iterations: 290
currently lose_sum: 68.84033924341202
time_elpased: 1.878
batch start
#iterations: 291
currently lose_sum: 68.84981828927994
time_elpased: 1.815
batch start
#iterations: 292
currently lose_sum: 68.93506410717964
time_elpased: 1.892
batch start
#iterations: 293
currently lose_sum: 69.09465298056602
time_elpased: 1.851
batch start
#iterations: 294
currently lose_sum: 68.5908715724945
time_elpased: 1.925
batch start
#iterations: 295
currently lose_sum: 67.74963361024857
time_elpased: 1.851
batch start
#iterations: 296
currently lose_sum: 68.32210904359818
time_elpased: 1.825
batch start
#iterations: 297
currently lose_sum: 68.34570172429085
time_elpased: 1.822
batch start
#iterations: 298
currently lose_sum: 66.96978244185448
time_elpased: 1.858
batch start
#iterations: 299
currently lose_sum: 68.08216381072998
time_elpased: 1.836
start validation test
0.61412371134
0.656517468781
0.481527220335
0.555568748516
0.614356504932
87.257
batch start
#iterations: 300
currently lose_sum: 68.34102541208267
time_elpased: 1.859
batch start
#iterations: 301
currently lose_sum: 68.44202747941017
time_elpased: 1.899
batch start
#iterations: 302
currently lose_sum: 67.49694547057152
time_elpased: 1.866
batch start
#iterations: 303
currently lose_sum: 67.28741866350174
time_elpased: 1.889
batch start
#iterations: 304
currently lose_sum: 67.61997392773628
time_elpased: 1.901
batch start
#iterations: 305
currently lose_sum: 67.11839377880096
time_elpased: 1.88
batch start
#iterations: 306
currently lose_sum: 67.14643406867981
time_elpased: 1.848
batch start
#iterations: 307
currently lose_sum: 66.32291874289513
time_elpased: 1.867
batch start
#iterations: 308
currently lose_sum: 66.44798254966736
time_elpased: 1.81
batch start
#iterations: 309
currently lose_sum: 66.86463767290115
time_elpased: 1.871
batch start
#iterations: 310
currently lose_sum: 66.11605152487755
time_elpased: 1.867
batch start
#iterations: 311
currently lose_sum: 65.65832763910294
time_elpased: 1.883
batch start
#iterations: 312
currently lose_sum: 65.06630113720894
time_elpased: 1.868
batch start
#iterations: 313
currently lose_sum: 65.14701169729233
time_elpased: 1.883
batch start
#iterations: 314
currently lose_sum: 64.60162815451622
time_elpased: 1.835
batch start
#iterations: 315
currently lose_sum: 64.36825254559517
time_elpased: 1.84
batch start
#iterations: 316
currently lose_sum: 64.55350962281227
time_elpased: 1.89
batch start
#iterations: 317
currently lose_sum: 64.83792528510094
time_elpased: 1.889
batch start
#iterations: 318
currently lose_sum: 63.800416111946106
time_elpased: 1.829
batch start
#iterations: 319
currently lose_sum: 64.37485167384148
time_elpased: 1.908
start validation test
0.609175257732
0.645972924928
0.486158279304
0.554785672343
0.609391233012
91.441
batch start
#iterations: 320
currently lose_sum: 64.3185907304287
time_elpased: 1.876
batch start
#iterations: 321
currently lose_sum: 64.06672123074532
time_elpased: 1.852
batch start
#iterations: 322
currently lose_sum: 63.49058774113655
time_elpased: 1.857
batch start
#iterations: 323
currently lose_sum: 64.29670938849449
time_elpased: 1.881
batch start
#iterations: 324
currently lose_sum: 64.03473073244095
time_elpased: 1.844
batch start
#iterations: 325
currently lose_sum: 63.89601829648018
time_elpased: 1.878
batch start
#iterations: 326
currently lose_sum: 62.490644842386246
time_elpased: 1.885
batch start
#iterations: 327
currently lose_sum: 63.421968549489975
time_elpased: 1.924
batch start
#iterations: 328
currently lose_sum: 62.937431305646896
time_elpased: 1.819
batch start
#iterations: 329
currently lose_sum: 63.46874859929085
time_elpased: 1.884
batch start
#iterations: 330
currently lose_sum: 63.474910229444504
time_elpased: 1.848
batch start
#iterations: 331
currently lose_sum: 62.6532778441906
time_elpased: 1.878
batch start
#iterations: 332
currently lose_sum: 61.19531524181366
time_elpased: 1.872
batch start
#iterations: 333
currently lose_sum: 62.000956028699875
time_elpased: 1.887
batch start
#iterations: 334
currently lose_sum: 62.144617825746536
time_elpased: 1.86
batch start
#iterations: 335
currently lose_sum: 62.08952385187149
time_elpased: 1.826
batch start
#iterations: 336
currently lose_sum: 61.59740340709686
time_elpased: 1.896
batch start
#iterations: 337
currently lose_sum: 61.43909880518913
time_elpased: 1.838
batch start
#iterations: 338
currently lose_sum: 60.79837495088577
time_elpased: 1.91
batch start
#iterations: 339
currently lose_sum: 62.49209749698639
time_elpased: 1.869
start validation test
0.594896907216
0.650372288767
0.413502109705
0.505567788613
0.595215373762
105.749
batch start
#iterations: 340
currently lose_sum: 61.53114613890648
time_elpased: 1.847
batch start
#iterations: 341
currently lose_sum: 61.794407457113266
time_elpased: 1.836
batch start
#iterations: 342
currently lose_sum: 60.83992901444435
time_elpased: 1.929
batch start
#iterations: 343
currently lose_sum: 61.10074684023857
time_elpased: 1.798
batch start
#iterations: 344
currently lose_sum: 60.81693705916405
time_elpased: 1.891
batch start
#iterations: 345
currently lose_sum: 60.39526754617691
time_elpased: 1.861
batch start
#iterations: 346
currently lose_sum: 59.60540163516998
time_elpased: 1.852
batch start
#iterations: 347
currently lose_sum: 59.035471111536026
time_elpased: 1.808
batch start
#iterations: 348
currently lose_sum: 60.83876368403435
time_elpased: 1.87
batch start
#iterations: 349
currently lose_sum: 60.50523501634598
time_elpased: 1.894
batch start
#iterations: 350
currently lose_sum: 59.71768829226494
time_elpased: 1.854
batch start
#iterations: 351
currently lose_sum: 58.66272583603859
time_elpased: 1.863
batch start
#iterations: 352
currently lose_sum: 59.66792315244675
time_elpased: 1.857
batch start
#iterations: 353
currently lose_sum: 59.12802419066429
time_elpased: 1.94
batch start
#iterations: 354
currently lose_sum: 58.91312354803085
time_elpased: 1.874
batch start
#iterations: 355
currently lose_sum: 59.206086844205856
time_elpased: 1.828
batch start
#iterations: 356
currently lose_sum: 58.728385508060455
time_elpased: 1.882
batch start
#iterations: 357
currently lose_sum: 57.95648214221001
time_elpased: 1.893
batch start
#iterations: 358
currently lose_sum: 58.751648873090744
time_elpased: 1.882
batch start
#iterations: 359
currently lose_sum: 58.478664457798004
time_elpased: 1.836
start validation test
0.599536082474
0.655243863564
0.423072964907
0.514164217372
0.599845890695
111.793
batch start
#iterations: 360
currently lose_sum: 58.157281786203384
time_elpased: 1.88
batch start
#iterations: 361
currently lose_sum: 58.76667162775993
time_elpased: 1.867
batch start
#iterations: 362
currently lose_sum: 57.4883117377758
time_elpased: 1.88
batch start
#iterations: 363
currently lose_sum: 57.93244385719299
time_elpased: 1.888
batch start
#iterations: 364
currently lose_sum: 56.80440807342529
time_elpased: 1.896
batch start
#iterations: 365
currently lose_sum: 57.30416056513786
time_elpased: 1.867
batch start
#iterations: 366
currently lose_sum: 57.34037107229233
time_elpased: 1.895
batch start
#iterations: 367
currently lose_sum: 57.01929333806038
time_elpased: 1.845
batch start
#iterations: 368
currently lose_sum: 57.005073577165604
time_elpased: 1.86
batch start
#iterations: 369
currently lose_sum: 56.72151041030884
time_elpased: 1.835
batch start
#iterations: 370
currently lose_sum: 56.90077570080757
time_elpased: 1.838
batch start
#iterations: 371
currently lose_sum: 56.462020218372345
time_elpased: 1.844
batch start
#iterations: 372
currently lose_sum: 56.87848237156868
time_elpased: 1.894
batch start
#iterations: 373
currently lose_sum: 56.5912809073925
time_elpased: 1.826
batch start
#iterations: 374
currently lose_sum: 56.07579469680786
time_elpased: 1.862
batch start
#iterations: 375
currently lose_sum: 55.72295039892197
time_elpased: 1.844
batch start
#iterations: 376
currently lose_sum: 56.06672003865242
time_elpased: 1.85
batch start
#iterations: 377
currently lose_sum: 55.5673750936985
time_elpased: 1.813
batch start
#iterations: 378
currently lose_sum: 55.30997163057327
time_elpased: 1.874
batch start
#iterations: 379
currently lose_sum: 55.79871991276741
time_elpased: 1.87
start validation test
0.591443298969
0.650833754421
0.397653596789
0.493675737831
0.591783526681
121.445
batch start
#iterations: 380
currently lose_sum: 55.152088671922684
time_elpased: 1.907
batch start
#iterations: 381
currently lose_sum: 54.46985340118408
time_elpased: 1.855
batch start
#iterations: 382
currently lose_sum: 54.467368096113205
time_elpased: 1.869
batch start
#iterations: 383
currently lose_sum: 54.079917788505554
time_elpased: 1.88
batch start
#iterations: 384
currently lose_sum: 54.53572478890419
time_elpased: 1.867
batch start
#iterations: 385
currently lose_sum: 54.50682258605957
time_elpased: 1.833
batch start
#iterations: 386
currently lose_sum: 54.213713735342026
time_elpased: 1.888
batch start
#iterations: 387
currently lose_sum: 54.35679903626442
time_elpased: 1.885
batch start
#iterations: 388
currently lose_sum: 54.08703503012657
time_elpased: 1.953
batch start
#iterations: 389
currently lose_sum: 54.53643813729286
time_elpased: 1.863
batch start
#iterations: 390
currently lose_sum: 54.20105245709419
time_elpased: 1.888
batch start
#iterations: 391
currently lose_sum: 53.221093595027924
time_elpased: 1.867
batch start
#iterations: 392
currently lose_sum: 54.55287104845047
time_elpased: 1.875
batch start
#iterations: 393
currently lose_sum: 52.77456060051918
time_elpased: 1.835
batch start
#iterations: 394
currently lose_sum: 53.146275997161865
time_elpased: 1.899
batch start
#iterations: 395
currently lose_sum: 53.568092077970505
time_elpased: 1.876
batch start
#iterations: 396
currently lose_sum: 53.45570632815361
time_elpased: 1.888
batch start
#iterations: 397
currently lose_sum: 53.60995915532112
time_elpased: 1.842
batch start
#iterations: 398
currently lose_sum: 52.45075127482414
time_elpased: 1.89
batch start
#iterations: 399
currently lose_sum: 52.617304891347885
time_elpased: 1.888
start validation test
0.576649484536
0.642424242424
0.349078933827
0.452357138094
0.577049019738
137.177
acc: 0.665
pre: 0.649
rec: 0.722
F1: 0.683
auc: 0.665
