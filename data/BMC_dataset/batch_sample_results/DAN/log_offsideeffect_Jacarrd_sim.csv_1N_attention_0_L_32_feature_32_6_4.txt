start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.23876816034317
time_elpased: 1.556
batch start
#iterations: 1
currently lose_sum: 100.06594544649124
time_elpased: 1.532
batch start
#iterations: 2
currently lose_sum: 99.69398283958435
time_elpased: 1.534
batch start
#iterations: 3
currently lose_sum: 99.68365460634232
time_elpased: 1.559
batch start
#iterations: 4
currently lose_sum: 99.70904678106308
time_elpased: 1.549
batch start
#iterations: 5
currently lose_sum: 99.50589573383331
time_elpased: 1.547
batch start
#iterations: 6
currently lose_sum: 99.24829983711243
time_elpased: 1.543
batch start
#iterations: 7
currently lose_sum: 99.2737364768982
time_elpased: 1.521
batch start
#iterations: 8
currently lose_sum: 99.58654069900513
time_elpased: 1.551
batch start
#iterations: 9
currently lose_sum: 99.3738242983818
time_elpased: 1.57
batch start
#iterations: 10
currently lose_sum: 99.22556167840958
time_elpased: 1.545
batch start
#iterations: 11
currently lose_sum: 99.178542137146
time_elpased: 1.555
batch start
#iterations: 12
currently lose_sum: 99.29952055215836
time_elpased: 1.558
batch start
#iterations: 13
currently lose_sum: 99.82995116710663
time_elpased: 1.578
batch start
#iterations: 14
currently lose_sum: 99.12741428613663
time_elpased: 1.57
batch start
#iterations: 15
currently lose_sum: 99.01335853338242
time_elpased: 1.561
batch start
#iterations: 16
currently lose_sum: 99.44146943092346
time_elpased: 1.597
batch start
#iterations: 17
currently lose_sum: 99.7961705327034
time_elpased: 1.549
batch start
#iterations: 18
currently lose_sum: 98.71685647964478
time_elpased: 1.586
batch start
#iterations: 19
currently lose_sum: 99.76881086826324
time_elpased: 1.574
start validation test
0.61
0.585786073223
0.755788823711
0.660016176867
0.609744045234
66.519
batch start
#iterations: 20
currently lose_sum: 99.18167304992676
time_elpased: 1.579
batch start
#iterations: 21
currently lose_sum: 98.70350313186646
time_elpased: 1.585
batch start
#iterations: 22
currently lose_sum: 99.54824489355087
time_elpased: 1.55
batch start
#iterations: 23
currently lose_sum: 99.20341914892197
time_elpased: 1.537
batch start
#iterations: 24
currently lose_sum: 99.13330632448196
time_elpased: 1.558
batch start
#iterations: 25
currently lose_sum: 98.88160330057144
time_elpased: 1.565
batch start
#iterations: 26
currently lose_sum: 98.77666699886322
time_elpased: 1.545
batch start
#iterations: 27
currently lose_sum: 98.81134015321732
time_elpased: 1.569
batch start
#iterations: 28
currently lose_sum: 98.45747965574265
time_elpased: 1.546
batch start
#iterations: 29
currently lose_sum: 98.65135341882706
time_elpased: 1.545
batch start
#iterations: 30
currently lose_sum: 99.1446281671524
time_elpased: 1.539
batch start
#iterations: 31
currently lose_sum: 100.10145127773285
time_elpased: 1.652
batch start
#iterations: 32
currently lose_sum: 98.74670112133026
time_elpased: 1.538
batch start
#iterations: 33
currently lose_sum: 98.74966341257095
time_elpased: 1.536
batch start
#iterations: 34
currently lose_sum: 98.59200757741928
time_elpased: 1.572
batch start
#iterations: 35
currently lose_sum: 98.34397882223129
time_elpased: 1.596
batch start
#iterations: 36
currently lose_sum: 99.06475162506104
time_elpased: 1.573
batch start
#iterations: 37
currently lose_sum: 98.20527243614197
time_elpased: 1.584
batch start
#iterations: 38
currently lose_sum: 97.95914274454117
time_elpased: 1.531
batch start
#iterations: 39
currently lose_sum: 100.490678191185
time_elpased: 1.53
start validation test
0.605103092784
0.577961474291
0.784295564475
0.665502335939
0.604788492761
67.156
batch start
#iterations: 40
currently lose_sum: 99.31111967563629
time_elpased: 1.547
batch start
#iterations: 41
currently lose_sum: 98.78128522634506
time_elpased: 1.566
batch start
#iterations: 42
currently lose_sum: 98.02478003501892
time_elpased: 1.543
batch start
#iterations: 43
currently lose_sum: 97.96601796150208
time_elpased: 1.538
batch start
#iterations: 44
currently lose_sum: 98.12226641178131
time_elpased: 1.532
batch start
#iterations: 45
currently lose_sum: 98.71338886022568
time_elpased: 1.559
batch start
#iterations: 46
currently lose_sum: 97.78246933221817
time_elpased: 1.533
batch start
#iterations: 47
currently lose_sum: 98.29128444194794
time_elpased: 1.629
batch start
#iterations: 48
currently lose_sum: 97.953622341156
time_elpased: 1.541
batch start
#iterations: 49
currently lose_sum: 97.92058753967285
time_elpased: 1.57
batch start
#iterations: 50
currently lose_sum: 97.46743530035019
time_elpased: 1.548
batch start
#iterations: 51
currently lose_sum: 97.27027624845505
time_elpased: 1.539
batch start
#iterations: 52
currently lose_sum: 99.68979090452194
time_elpased: 1.527
batch start
#iterations: 53
currently lose_sum: 99.19699174165726
time_elpased: 1.555
batch start
#iterations: 54
currently lose_sum: 97.71526026725769
time_elpased: 1.568
batch start
#iterations: 55
currently lose_sum: 99.30752897262573
time_elpased: 1.572
batch start
#iterations: 56
currently lose_sum: 100.49972331523895
time_elpased: 1.607
batch start
#iterations: 57
currently lose_sum: 99.45466250181198
time_elpased: 1.556
batch start
#iterations: 58
currently lose_sum: 97.0795128941536
time_elpased: 1.547
batch start
#iterations: 59
currently lose_sum: 97.77414292097092
time_elpased: 1.571
start validation test
0.630103092784
0.59348098006
0.830091592055
0.692122876266
0.629751982127
62.467
batch start
#iterations: 60
currently lose_sum: 97.44662660360336
time_elpased: 1.551
batch start
#iterations: 61
currently lose_sum: 97.25675302743912
time_elpased: 1.532
batch start
#iterations: 62
currently lose_sum: 97.31072545051575
time_elpased: 1.533
batch start
#iterations: 63
currently lose_sum: 96.86817634105682
time_elpased: 1.595
batch start
#iterations: 64
currently lose_sum: 96.85583102703094
time_elpased: 1.552
batch start
#iterations: 65
currently lose_sum: 97.4766475558281
time_elpased: 1.555
batch start
#iterations: 66
currently lose_sum: 100.51559627056122
time_elpased: 1.572
batch start
#iterations: 67
currently lose_sum: 100.50647336244583
time_elpased: 1.579
batch start
#iterations: 68
currently lose_sum: 100.48757779598236
time_elpased: 1.577
batch start
#iterations: 69
currently lose_sum: 100.19671505689621
time_elpased: 1.554
batch start
#iterations: 70
currently lose_sum: 98.0631365776062
time_elpased: 1.53
batch start
#iterations: 71
currently lose_sum: 97.89303779602051
time_elpased: 1.539
batch start
#iterations: 72
currently lose_sum: 97.09263741970062
time_elpased: 1.521
batch start
#iterations: 73
currently lose_sum: 97.28061217069626
time_elpased: 1.576
batch start
#iterations: 74
currently lose_sum: 96.10382604598999
time_elpased: 1.569
batch start
#iterations: 75
currently lose_sum: 96.5059729218483
time_elpased: 1.52
batch start
#iterations: 76
currently lose_sum: 97.03855109214783
time_elpased: 1.542
batch start
#iterations: 77
currently lose_sum: 96.58874374628067
time_elpased: 1.535
batch start
#iterations: 78
currently lose_sum: 96.23139548301697
time_elpased: 1.557
batch start
#iterations: 79
currently lose_sum: 95.5498104095459
time_elpased: 1.579
start validation test
0.636597938144
0.592907406117
0.875784707214
0.707104279186
0.636178008879
63.049
batch start
#iterations: 80
currently lose_sum: 96.124806702137
time_elpased: 1.56
batch start
#iterations: 81
currently lose_sum: 96.50576084852219
time_elpased: 1.557
batch start
#iterations: 82
currently lose_sum: 96.00276070833206
time_elpased: 1.554
batch start
#iterations: 83
currently lose_sum: 96.83109325170517
time_elpased: 1.634
batch start
#iterations: 84
currently lose_sum: 100.5318820476532
time_elpased: 1.573
batch start
#iterations: 85
currently lose_sum: 100.5170909166336
time_elpased: 1.567
batch start
#iterations: 86
currently lose_sum: 100.5127888917923
time_elpased: 1.541
batch start
#iterations: 87
currently lose_sum: 100.51085358858109
time_elpased: 1.573
batch start
#iterations: 88
currently lose_sum: 100.50935196876526
time_elpased: 1.574
batch start
#iterations: 89
currently lose_sum: 100.50830578804016
time_elpased: 1.605
batch start
#iterations: 90
currently lose_sum: 100.50766640901566
time_elpased: 1.549
batch start
#iterations: 91
currently lose_sum: 100.5068541765213
time_elpased: 1.534
batch start
#iterations: 92
currently lose_sum: 100.50540661811829
time_elpased: 1.542
batch start
#iterations: 93
currently lose_sum: 100.50171267986298
time_elpased: 1.652
batch start
#iterations: 94
currently lose_sum: 100.49476850032806
time_elpased: 1.56
batch start
#iterations: 95
currently lose_sum: 100.39608609676361
time_elpased: 1.569
batch start
#iterations: 96
currently lose_sum: 97.75666463375092
time_elpased: 1.585
batch start
#iterations: 97
currently lose_sum: 96.78220856189728
time_elpased: 1.545
batch start
#iterations: 98
currently lose_sum: 96.14769273996353
time_elpased: 1.548
batch start
#iterations: 99
currently lose_sum: 96.65235579013824
time_elpased: 1.545
start validation test
0.619432989691
0.574407039021
0.927138005557
0.709342151884
0.618892766075
64.048
batch start
#iterations: 100
currently lose_sum: 96.02968770265579
time_elpased: 1.594
batch start
#iterations: 101
currently lose_sum: 95.6862108707428
time_elpased: 1.561
batch start
#iterations: 102
currently lose_sum: 95.57669174671173
time_elpased: 1.531
batch start
#iterations: 103
currently lose_sum: 95.89395213127136
time_elpased: 1.577
batch start
#iterations: 104
currently lose_sum: 95.40216261148453
time_elpased: 1.531
batch start
#iterations: 105
currently lose_sum: 95.3560443520546
time_elpased: 1.547
batch start
#iterations: 106
currently lose_sum: 95.8822004199028
time_elpased: 1.567
batch start
#iterations: 107
currently lose_sum: 95.84153753519058
time_elpased: 1.523
batch start
#iterations: 108
currently lose_sum: 95.16970604658127
time_elpased: 1.573
batch start
#iterations: 109
currently lose_sum: 95.25153422355652
time_elpased: 1.572
batch start
#iterations: 110
currently lose_sum: 95.72876262664795
time_elpased: 1.551
batch start
#iterations: 111
currently lose_sum: 96.05820155143738
time_elpased: 1.563
batch start
#iterations: 112
currently lose_sum: 95.02031981945038
time_elpased: 1.558
batch start
#iterations: 113
currently lose_sum: 93.92912656068802
time_elpased: 1.582
batch start
#iterations: 114
currently lose_sum: 95.04698556661606
time_elpased: 1.558
batch start
#iterations: 115
currently lose_sum: 95.01201075315475
time_elpased: 1.573
batch start
#iterations: 116
currently lose_sum: 95.03918850421906
time_elpased: 1.567
batch start
#iterations: 117
currently lose_sum: 94.40039378404617
time_elpased: 1.563
batch start
#iterations: 118
currently lose_sum: 94.78252130746841
time_elpased: 1.536
batch start
#iterations: 119
currently lose_sum: 94.70096707344055
time_elpased: 1.546
start validation test
0.666134020619
0.663240628779
0.677369558506
0.670230639988
0.666114294899
59.569
batch start
#iterations: 120
currently lose_sum: 94.5356936454773
time_elpased: 1.552
batch start
#iterations: 121
currently lose_sum: 95.6477541923523
time_elpased: 1.521
batch start
#iterations: 122
currently lose_sum: 94.76889389753342
time_elpased: 1.538
batch start
#iterations: 123
currently lose_sum: 94.46730399131775
time_elpased: 1.559
batch start
#iterations: 124
currently lose_sum: 94.15265381336212
time_elpased: 1.547
batch start
#iterations: 125
currently lose_sum: 94.45876884460449
time_elpased: 1.54
batch start
#iterations: 126
currently lose_sum: 94.59546917676926
time_elpased: 1.56
batch start
#iterations: 127
currently lose_sum: 94.48457109928131
time_elpased: 1.593
batch start
#iterations: 128
currently lose_sum: 94.87036019563675
time_elpased: 1.531
batch start
#iterations: 129
currently lose_sum: 94.4231545329094
time_elpased: 1.52
batch start
#iterations: 130
currently lose_sum: 94.84493321180344
time_elpased: 1.521
batch start
#iterations: 131
currently lose_sum: 94.4167468547821
time_elpased: 1.533
batch start
#iterations: 132
currently lose_sum: 94.50870144367218
time_elpased: 1.557
batch start
#iterations: 133
currently lose_sum: 94.09986340999603
time_elpased: 1.537
batch start
#iterations: 134
currently lose_sum: 94.10550040006638
time_elpased: 1.536
batch start
#iterations: 135
currently lose_sum: 94.07921946048737
time_elpased: 1.571
batch start
#iterations: 136
currently lose_sum: 94.77095258235931
time_elpased: 1.555
batch start
#iterations: 137
currently lose_sum: 95.06064331531525
time_elpased: 1.507
batch start
#iterations: 138
currently lose_sum: 94.80040431022644
time_elpased: 1.548
batch start
#iterations: 139
currently lose_sum: 94.35804694890976
time_elpased: 1.52
start validation test
0.633350515464
0.607656689267
0.756303385819
0.673880152217
0.633134652736
61.228
batch start
#iterations: 140
currently lose_sum: 94.14207047224045
time_elpased: 1.546
batch start
#iterations: 141
currently lose_sum: 93.74761819839478
time_elpased: 1.631
batch start
#iterations: 142
currently lose_sum: 94.29290264844894
time_elpased: 1.545
batch start
#iterations: 143
currently lose_sum: 94.22430211305618
time_elpased: 1.595
batch start
#iterations: 144
currently lose_sum: 94.51310682296753
time_elpased: 1.525
batch start
#iterations: 145
currently lose_sum: 94.37978559732437
time_elpased: 1.541
batch start
#iterations: 146
currently lose_sum: 94.86161959171295
time_elpased: 1.557
batch start
#iterations: 147
currently lose_sum: 94.266082406044
time_elpased: 1.557
batch start
#iterations: 148
currently lose_sum: 94.47568136453629
time_elpased: 1.551
batch start
#iterations: 149
currently lose_sum: 93.94374364614487
time_elpased: 1.536
batch start
#iterations: 150
currently lose_sum: 93.63238352537155
time_elpased: 1.562
batch start
#iterations: 151
currently lose_sum: 94.06869006156921
time_elpased: 1.525
batch start
#iterations: 152
currently lose_sum: 93.5923723578453
time_elpased: 1.554
batch start
#iterations: 153
currently lose_sum: 94.25404804944992
time_elpased: 1.568
batch start
#iterations: 154
currently lose_sum: 94.29593336582184
time_elpased: 1.542
batch start
#iterations: 155
currently lose_sum: 93.8484417796135
time_elpased: 1.502
batch start
#iterations: 156
currently lose_sum: 95.45958119630814
time_elpased: 1.51
batch start
#iterations: 157
currently lose_sum: 94.22559517621994
time_elpased: 1.538
batch start
#iterations: 158
currently lose_sum: 94.0155041217804
time_elpased: 1.534
batch start
#iterations: 159
currently lose_sum: 93.90275502204895
time_elpased: 1.53
start validation test
0.67293814433
0.659508041627
0.717402490481
0.687238132794
0.672860080312
58.728
batch start
#iterations: 160
currently lose_sum: 93.78098219633102
time_elpased: 1.498
batch start
#iterations: 161
currently lose_sum: 93.9930944442749
time_elpased: 1.543
batch start
#iterations: 162
currently lose_sum: 93.77134042978287
time_elpased: 1.556
batch start
#iterations: 163
currently lose_sum: 94.28801482915878
time_elpased: 1.537
batch start
#iterations: 164
currently lose_sum: 93.82052809000015
time_elpased: 1.59
batch start
#iterations: 165
currently lose_sum: 93.5025823712349
time_elpased: 1.605
batch start
#iterations: 166
currently lose_sum: 93.58846479654312
time_elpased: 1.533
batch start
#iterations: 167
currently lose_sum: 94.20531797409058
time_elpased: 1.537
batch start
#iterations: 168
currently lose_sum: 94.10696744918823
time_elpased: 1.545
batch start
#iterations: 169
currently lose_sum: 93.4815987944603
time_elpased: 1.559
batch start
#iterations: 170
currently lose_sum: 93.82046681642532
time_elpased: 1.58
batch start
#iterations: 171
currently lose_sum: 93.41963791847229
time_elpased: 1.597
batch start
#iterations: 172
currently lose_sum: 93.6455129981041
time_elpased: 1.545
batch start
#iterations: 173
currently lose_sum: 94.11346578598022
time_elpased: 1.533
batch start
#iterations: 174
currently lose_sum: 92.94243001937866
time_elpased: 1.573
batch start
#iterations: 175
currently lose_sum: 93.61830204725266
time_elpased: 1.557
batch start
#iterations: 176
currently lose_sum: 93.66499495506287
time_elpased: 1.584
batch start
#iterations: 177
currently lose_sum: 93.91984403133392
time_elpased: 1.525
batch start
#iterations: 178
currently lose_sum: 93.68467849493027
time_elpased: 1.569
batch start
#iterations: 179
currently lose_sum: 93.79957431554794
time_elpased: 1.568
start validation test
0.596958762887
0.572453809742
0.771637336627
0.657286872671
0.596652087708
63.147
batch start
#iterations: 180
currently lose_sum: 93.14878410100937
time_elpased: 1.583
batch start
#iterations: 181
currently lose_sum: 93.0687443614006
time_elpased: 1.579
batch start
#iterations: 182
currently lose_sum: 93.29352331161499
time_elpased: 1.59
batch start
#iterations: 183
currently lose_sum: 93.37604087591171
time_elpased: 1.549
batch start
#iterations: 184
currently lose_sum: 93.92172956466675
time_elpased: 1.58
batch start
#iterations: 185
currently lose_sum: 93.47283279895782
time_elpased: 1.559
batch start
#iterations: 186
currently lose_sum: 93.42095690965652
time_elpased: 1.546
batch start
#iterations: 187
currently lose_sum: 93.23358684778214
time_elpased: 1.52
batch start
#iterations: 188
currently lose_sum: 93.12284964323044
time_elpased: 1.543
batch start
#iterations: 189
currently lose_sum: 93.70975488424301
time_elpased: 1.584
batch start
#iterations: 190
currently lose_sum: 92.74060052633286
time_elpased: 1.54
batch start
#iterations: 191
currently lose_sum: 94.04965907335281
time_elpased: 1.537
batch start
#iterations: 192
currently lose_sum: 93.81332659721375
time_elpased: 1.56
batch start
#iterations: 193
currently lose_sum: 92.79712957143784
time_elpased: 1.59
batch start
#iterations: 194
currently lose_sum: 93.3008942604065
time_elpased: 1.551
batch start
#iterations: 195
currently lose_sum: 93.22570639848709
time_elpased: 1.574
batch start
#iterations: 196
currently lose_sum: 93.7006066441536
time_elpased: 1.539
batch start
#iterations: 197
currently lose_sum: 94.6751520037651
time_elpased: 1.544
batch start
#iterations: 198
currently lose_sum: 92.94780373573303
time_elpased: 1.58
batch start
#iterations: 199
currently lose_sum: 92.55710107088089
time_elpased: 1.603
start validation test
0.61412371134
0.602915398099
0.672532674694
0.63582409029
0.614021165396
61.471
batch start
#iterations: 200
currently lose_sum: 93.28197729587555
time_elpased: 1.593
batch start
#iterations: 201
currently lose_sum: 93.00836896896362
time_elpased: 1.541
batch start
#iterations: 202
currently lose_sum: 92.64372628927231
time_elpased: 1.574
batch start
#iterations: 203
currently lose_sum: 93.23231643438339
time_elpased: 1.516
batch start
#iterations: 204
currently lose_sum: 92.79913955926895
time_elpased: 1.589
batch start
#iterations: 205
currently lose_sum: 93.55373483896255
time_elpased: 1.557
batch start
#iterations: 206
currently lose_sum: 93.60325127840042
time_elpased: 1.545
batch start
#iterations: 207
currently lose_sum: 93.16492050886154
time_elpased: 1.545
batch start
#iterations: 208
currently lose_sum: 92.87647306919098
time_elpased: 1.573
batch start
#iterations: 209
currently lose_sum: 92.80510640144348
time_elpased: 1.549
batch start
#iterations: 210
currently lose_sum: 92.92088836431503
time_elpased: 1.528
batch start
#iterations: 211
currently lose_sum: 92.68692123889923
time_elpased: 1.541
batch start
#iterations: 212
currently lose_sum: 93.03861224651337
time_elpased: 1.55
batch start
#iterations: 213
currently lose_sum: 92.8486955165863
time_elpased: 1.548
batch start
#iterations: 214
currently lose_sum: 93.01972597837448
time_elpased: 1.54
batch start
#iterations: 215
currently lose_sum: 92.38894975185394
time_elpased: 1.562
batch start
#iterations: 216
currently lose_sum: 92.51472687721252
time_elpased: 1.603
batch start
#iterations: 217
currently lose_sum: 92.9725598692894
time_elpased: 1.535
batch start
#iterations: 218
currently lose_sum: 93.1851025223732
time_elpased: 1.536
batch start
#iterations: 219
currently lose_sum: 92.52732121944427
time_elpased: 1.548
start validation test
0.650515463918
0.644722578102
0.673253061645
0.658679017318
0.650475544558
59.528
batch start
#iterations: 220
currently lose_sum: 92.7431612610817
time_elpased: 1.536
batch start
#iterations: 221
currently lose_sum: 92.89613366127014
time_elpased: 1.553
batch start
#iterations: 222
currently lose_sum: 92.75621712207794
time_elpased: 1.505
batch start
#iterations: 223
currently lose_sum: 92.82702177762985
time_elpased: 1.543
batch start
#iterations: 224
currently lose_sum: 93.13533276319504
time_elpased: 1.56
batch start
#iterations: 225
currently lose_sum: 92.72742211818695
time_elpased: 1.542
batch start
#iterations: 226
currently lose_sum: 92.29902827739716
time_elpased: 1.579
batch start
#iterations: 227
currently lose_sum: 92.53089320659637
time_elpased: 1.54
batch start
#iterations: 228
currently lose_sum: 92.63300424814224
time_elpased: 1.546
batch start
#iterations: 229
currently lose_sum: 92.77509331703186
time_elpased: 1.557
batch start
#iterations: 230
currently lose_sum: 92.71656662225723
time_elpased: 1.53
batch start
#iterations: 231
currently lose_sum: 92.40965259075165
time_elpased: 1.554
batch start
#iterations: 232
currently lose_sum: 92.46123361587524
time_elpased: 1.622
batch start
#iterations: 233
currently lose_sum: 92.43833714723587
time_elpased: 1.504
batch start
#iterations: 234
currently lose_sum: 92.3730309009552
time_elpased: 1.594
batch start
#iterations: 235
currently lose_sum: 92.20809805393219
time_elpased: 1.511
batch start
#iterations: 236
currently lose_sum: 92.60850143432617
time_elpased: 1.583
batch start
#iterations: 237
currently lose_sum: 92.43457329273224
time_elpased: 1.529
batch start
#iterations: 238
currently lose_sum: 93.29870045185089
time_elpased: 1.533
batch start
#iterations: 239
currently lose_sum: 91.80033951997757
time_elpased: 1.535
start validation test
0.682628865979
0.666324051579
0.733868477925
0.698467113962
0.682538906937
57.292
batch start
#iterations: 240
currently lose_sum: 92.90430355072021
time_elpased: 1.549
batch start
#iterations: 241
currently lose_sum: 92.47502440214157
time_elpased: 1.535
batch start
#iterations: 242
currently lose_sum: 92.05501013994217
time_elpased: 1.563
batch start
#iterations: 243
currently lose_sum: 92.72582870721817
time_elpased: 1.508
batch start
#iterations: 244
currently lose_sum: 92.35203051567078
time_elpased: 1.653
batch start
#iterations: 245
currently lose_sum: 92.28356897830963
time_elpased: 1.59
batch start
#iterations: 246
currently lose_sum: 92.53423750400543
time_elpased: 1.553
batch start
#iterations: 247
currently lose_sum: 92.38056552410126
time_elpased: 1.529
batch start
#iterations: 248
currently lose_sum: 92.67288583517075
time_elpased: 1.551
batch start
#iterations: 249
currently lose_sum: 92.7403633594513
time_elpased: 1.514
batch start
#iterations: 250
currently lose_sum: 91.95179980993271
time_elpased: 1.546
batch start
#iterations: 251
currently lose_sum: 92.78569853305817
time_elpased: 1.525
batch start
#iterations: 252
currently lose_sum: 92.21189081668854
time_elpased: 1.534
batch start
#iterations: 253
currently lose_sum: 92.49206793308258
time_elpased: 1.53
batch start
#iterations: 254
currently lose_sum: 92.08925539255142
time_elpased: 1.568
batch start
#iterations: 255
currently lose_sum: 92.02446210384369
time_elpased: 1.526
batch start
#iterations: 256
currently lose_sum: 91.35314399003983
time_elpased: 1.517
batch start
#iterations: 257
currently lose_sum: 92.4051103591919
time_elpased: 1.539
batch start
#iterations: 258
currently lose_sum: 92.11122870445251
time_elpased: 1.542
batch start
#iterations: 259
currently lose_sum: 91.72952830791473
time_elpased: 1.551
start validation test
0.67
0.667712233128
0.679119069672
0.673367346939
0.669983990067
58.438
batch start
#iterations: 260
currently lose_sum: 92.22617244720459
time_elpased: 1.598
batch start
#iterations: 261
currently lose_sum: 91.95223236083984
time_elpased: 1.553
batch start
#iterations: 262
currently lose_sum: 92.06810259819031
time_elpased: 1.577
batch start
#iterations: 263
currently lose_sum: 92.0093297958374
time_elpased: 1.543
batch start
#iterations: 264
currently lose_sum: 92.07662445306778
time_elpased: 1.567
batch start
#iterations: 265
currently lose_sum: 92.16492354869843
time_elpased: 1.546
batch start
#iterations: 266
currently lose_sum: 92.5114449262619
time_elpased: 1.526
batch start
#iterations: 267
currently lose_sum: 92.55163425207138
time_elpased: 1.562
batch start
#iterations: 268
currently lose_sum: 91.72673255205154
time_elpased: 1.502
batch start
#iterations: 269
currently lose_sum: 92.44038116931915
time_elpased: 1.568
batch start
#iterations: 270
currently lose_sum: 92.31921243667603
time_elpased: 1.513
batch start
#iterations: 271
currently lose_sum: 92.05588591098785
time_elpased: 1.533
batch start
#iterations: 272
currently lose_sum: 92.17247349023819
time_elpased: 1.593
batch start
#iterations: 273
currently lose_sum: 91.79504835605621
time_elpased: 1.569
batch start
#iterations: 274
currently lose_sum: 92.10109335184097
time_elpased: 1.562
batch start
#iterations: 275
currently lose_sum: 91.89768373966217
time_elpased: 1.541
batch start
#iterations: 276
currently lose_sum: 92.16974151134491
time_elpased: 1.541
batch start
#iterations: 277
currently lose_sum: 91.63288223743439
time_elpased: 1.61
batch start
#iterations: 278
currently lose_sum: 91.87011992931366
time_elpased: 1.54
batch start
#iterations: 279
currently lose_sum: 92.30654835700989
time_elpased: 1.517
start validation test
0.676237113402
0.653997848691
0.750849027478
0.69908494227
0.676106120679
58.107
batch start
#iterations: 280
currently lose_sum: 91.6500495672226
time_elpased: 1.573
batch start
#iterations: 281
currently lose_sum: 91.68990844488144
time_elpased: 1.535
batch start
#iterations: 282
currently lose_sum: 91.57190716266632
time_elpased: 1.566
batch start
#iterations: 283
currently lose_sum: 91.50977289676666
time_elpased: 1.526
batch start
#iterations: 284
currently lose_sum: 91.68725961446762
time_elpased: 1.587
batch start
#iterations: 285
currently lose_sum: 91.82970827817917
time_elpased: 1.551
batch start
#iterations: 286
currently lose_sum: 91.88414239883423
time_elpased: 1.534
batch start
#iterations: 287
currently lose_sum: 91.7003333568573
time_elpased: 1.528
batch start
#iterations: 288
currently lose_sum: 91.94116467237473
time_elpased: 1.553
batch start
#iterations: 289
currently lose_sum: 92.06264984607697
time_elpased: 1.528
batch start
#iterations: 290
currently lose_sum: 91.39294213056564
time_elpased: 1.6
batch start
#iterations: 291
currently lose_sum: 91.26965481042862
time_elpased: 1.555
batch start
#iterations: 292
currently lose_sum: 90.95926249027252
time_elpased: 1.565
batch start
#iterations: 293
currently lose_sum: 91.28059208393097
time_elpased: 1.52
batch start
#iterations: 294
currently lose_sum: 92.33543390035629
time_elpased: 1.527
batch start
#iterations: 295
currently lose_sum: 91.59245389699936
time_elpased: 1.57
batch start
#iterations: 296
currently lose_sum: 91.2280666232109
time_elpased: 1.558
batch start
#iterations: 297
currently lose_sum: 91.47946292161942
time_elpased: 1.521
batch start
#iterations: 298
currently lose_sum: 91.72321373224258
time_elpased: 1.554
batch start
#iterations: 299
currently lose_sum: 91.59449028968811
time_elpased: 1.57
start validation test
0.669278350515
0.637461480803
0.787691674385
0.704658442276
0.669070457661
58.793
batch start
#iterations: 300
currently lose_sum: 91.5959290266037
time_elpased: 1.549
batch start
#iterations: 301
currently lose_sum: 91.28645950555801
time_elpased: 1.535
batch start
#iterations: 302
currently lose_sum: 91.47636342048645
time_elpased: 1.575
batch start
#iterations: 303
currently lose_sum: 91.61471509933472
time_elpased: 1.548
batch start
#iterations: 304
currently lose_sum: 91.20854806900024
time_elpased: 1.589
batch start
#iterations: 305
currently lose_sum: 91.23497658967972
time_elpased: 1.583
batch start
#iterations: 306
currently lose_sum: 91.8229284286499
time_elpased: 1.605
batch start
#iterations: 307
currently lose_sum: 91.4710687994957
time_elpased: 1.574
batch start
#iterations: 308
currently lose_sum: 90.89965903759003
time_elpased: 1.528
batch start
#iterations: 309
currently lose_sum: 91.01508355140686
time_elpased: 1.554
batch start
#iterations: 310
currently lose_sum: 92.79937934875488
time_elpased: 1.595
batch start
#iterations: 311
currently lose_sum: 91.02538108825684
time_elpased: 1.546
batch start
#iterations: 312
currently lose_sum: 90.91935700178146
time_elpased: 1.59
batch start
#iterations: 313
currently lose_sum: 90.77286195755005
time_elpased: 1.535
batch start
#iterations: 314
currently lose_sum: 91.20375752449036
time_elpased: 1.538
batch start
#iterations: 315
currently lose_sum: 91.1397123336792
time_elpased: 1.578
batch start
#iterations: 316
currently lose_sum: 91.03794765472412
time_elpased: 1.525
batch start
#iterations: 317
currently lose_sum: 91.4661420583725
time_elpased: 1.543
batch start
#iterations: 318
currently lose_sum: 91.29364997148514
time_elpased: 1.534
batch start
#iterations: 319
currently lose_sum: 90.62330973148346
time_elpased: 1.545
start validation test
0.658298969072
0.628026533997
0.779458680663
0.695596271295
0.658086254511
58.730
batch start
#iterations: 320
currently lose_sum: 91.20260632038116
time_elpased: 1.533
batch start
#iterations: 321
currently lose_sum: 90.85852473974228
time_elpased: 1.517
batch start
#iterations: 322
currently lose_sum: 91.32042229175568
time_elpased: 1.571
batch start
#iterations: 323
currently lose_sum: 91.68351411819458
time_elpased: 1.54
batch start
#iterations: 324
currently lose_sum: 90.95429199934006
time_elpased: 1.557
batch start
#iterations: 325
currently lose_sum: 91.3164593577385
time_elpased: 1.566
batch start
#iterations: 326
currently lose_sum: 91.43769919872284
time_elpased: 1.513
batch start
#iterations: 327
currently lose_sum: 91.02568310499191
time_elpased: 1.546
batch start
#iterations: 328
currently lose_sum: 91.68233853578568
time_elpased: 1.535
batch start
#iterations: 329
currently lose_sum: 91.04621040821075
time_elpased: 1.544
batch start
#iterations: 330
currently lose_sum: 90.97450524568558
time_elpased: 1.548
batch start
#iterations: 331
currently lose_sum: 91.91807568073273
time_elpased: 1.562
batch start
#iterations: 332
currently lose_sum: 90.61731803417206
time_elpased: 1.534
batch start
#iterations: 333
currently lose_sum: 90.59228956699371
time_elpased: 1.547
batch start
#iterations: 334
currently lose_sum: 91.41405147314072
time_elpased: 1.567
batch start
#iterations: 335
currently lose_sum: 91.10161936283112
time_elpased: 1.568
batch start
#iterations: 336
currently lose_sum: 91.17088752985
time_elpased: 1.556
batch start
#iterations: 337
currently lose_sum: 90.96116405725479
time_elpased: 1.521
batch start
#iterations: 338
currently lose_sum: 90.67122662067413
time_elpased: 1.565
batch start
#iterations: 339
currently lose_sum: 90.82791900634766
time_elpased: 1.568
start validation test
0.676082474227
0.652482899529
0.755891736133
0.700390960236
0.675942356758
57.439
batch start
#iterations: 340
currently lose_sum: 91.13660871982574
time_elpased: 1.528
batch start
#iterations: 341
currently lose_sum: 92.04923903942108
time_elpased: 1.514
batch start
#iterations: 342
currently lose_sum: 90.84815692901611
time_elpased: 1.549
batch start
#iterations: 343
currently lose_sum: 91.2626970410347
time_elpased: 1.524
batch start
#iterations: 344
currently lose_sum: 90.53741127252579
time_elpased: 1.538
batch start
#iterations: 345
currently lose_sum: 91.82858550548553
time_elpased: 1.545
batch start
#iterations: 346
currently lose_sum: 90.63876736164093
time_elpased: 1.526
batch start
#iterations: 347
currently lose_sum: 90.90688788890839
time_elpased: 1.591
batch start
#iterations: 348
currently lose_sum: 90.6989678144455
time_elpased: 1.571
batch start
#iterations: 349
currently lose_sum: 90.86510211229324
time_elpased: 1.543
batch start
#iterations: 350
currently lose_sum: 90.79087233543396
time_elpased: 1.55
batch start
#iterations: 351
currently lose_sum: 90.82098931074142
time_elpased: 1.549
batch start
#iterations: 352
currently lose_sum: 90.26930451393127
time_elpased: 1.551
batch start
#iterations: 353
currently lose_sum: 91.38608360290527
time_elpased: 1.593
batch start
#iterations: 354
currently lose_sum: 90.53944057226181
time_elpased: 1.542
batch start
#iterations: 355
currently lose_sum: 90.93952769041061
time_elpased: 1.561
batch start
#iterations: 356
currently lose_sum: 90.80650627613068
time_elpased: 1.55
batch start
#iterations: 357
currently lose_sum: 90.66222846508026
time_elpased: 1.534
batch start
#iterations: 358
currently lose_sum: 90.36497151851654
time_elpased: 1.575
batch start
#iterations: 359
currently lose_sum: 90.81887567043304
time_elpased: 1.568
start validation test
0.680412371134
0.656408431913
0.759493670886
0.704198473282
0.680273531715
57.088
batch start
#iterations: 360
currently lose_sum: 91.31018751859665
time_elpased: 1.572
batch start
#iterations: 361
currently lose_sum: 90.8171638250351
time_elpased: 1.602
batch start
#iterations: 362
currently lose_sum: 90.72271579504013
time_elpased: 1.59
batch start
#iterations: 363
currently lose_sum: 90.95591825246811
time_elpased: 1.513
batch start
#iterations: 364
currently lose_sum: 90.46036964654922
time_elpased: 1.538
batch start
#iterations: 365
currently lose_sum: 90.60986423492432
time_elpased: 1.549
batch start
#iterations: 366
currently lose_sum: 91.03450745344162
time_elpased: 1.612
batch start
#iterations: 367
currently lose_sum: 90.5215470790863
time_elpased: 1.529
batch start
#iterations: 368
currently lose_sum: 90.46822011470795
time_elpased: 1.563
batch start
#iterations: 369
currently lose_sum: 90.68820971250534
time_elpased: 1.53
batch start
#iterations: 370
currently lose_sum: 90.51963692903519
time_elpased: 1.548
batch start
#iterations: 371
currently lose_sum: 90.1342002749443
time_elpased: 1.537
batch start
#iterations: 372
currently lose_sum: 90.47773033380508
time_elpased: 1.588
batch start
#iterations: 373
currently lose_sum: 90.57700669765472
time_elpased: 1.516
batch start
#iterations: 374
currently lose_sum: 90.76781302690506
time_elpased: 1.611
batch start
#iterations: 375
currently lose_sum: 91.1437965631485
time_elpased: 1.563
batch start
#iterations: 376
currently lose_sum: 90.49016857147217
time_elpased: 1.593
batch start
#iterations: 377
currently lose_sum: 90.67997580766678
time_elpased: 1.564
batch start
#iterations: 378
currently lose_sum: 90.9200496673584
time_elpased: 1.6
batch start
#iterations: 379
currently lose_sum: 90.49161159992218
time_elpased: 1.567
start validation test
0.684072164948
0.670305676856
0.726664608418
0.697348279097
0.683997387345
56.962
batch start
#iterations: 380
currently lose_sum: 90.88768887519836
time_elpased: 1.6
batch start
#iterations: 381
currently lose_sum: 90.88265645503998
time_elpased: 1.588
batch start
#iterations: 382
currently lose_sum: 90.20213067531586
time_elpased: 1.611
batch start
#iterations: 383
currently lose_sum: 90.08524978160858
time_elpased: 1.499
batch start
#iterations: 384
currently lose_sum: 89.85375344753265
time_elpased: 1.511
batch start
#iterations: 385
currently lose_sum: 91.11896985769272
time_elpased: 1.523
batch start
#iterations: 386
currently lose_sum: 90.65443623065948
time_elpased: 1.604
batch start
#iterations: 387
currently lose_sum: 90.39006644487381
time_elpased: 1.574
batch start
#iterations: 388
currently lose_sum: 91.25913828611374
time_elpased: 1.566
batch start
#iterations: 389
currently lose_sum: 90.98188501596451
time_elpased: 1.555
batch start
#iterations: 390
currently lose_sum: 90.17681300640106
time_elpased: 1.609
batch start
#iterations: 391
currently lose_sum: 90.62566822767258
time_elpased: 1.537
batch start
#iterations: 392
currently lose_sum: 90.19777077436447
time_elpased: 1.559
batch start
#iterations: 393
currently lose_sum: 90.06640988588333
time_elpased: 1.531
batch start
#iterations: 394
currently lose_sum: 89.94347953796387
time_elpased: 1.547
batch start
#iterations: 395
currently lose_sum: 90.48498183488846
time_elpased: 1.576
batch start
#iterations: 396
currently lose_sum: 90.53141212463379
time_elpased: 1.598
batch start
#iterations: 397
currently lose_sum: 90.85466140508652
time_elpased: 1.559
batch start
#iterations: 398
currently lose_sum: 90.47245520353317
time_elpased: 1.55
batch start
#iterations: 399
currently lose_sum: 90.63162004947662
time_elpased: 1.588
start validation test
0.683659793814
0.670476190476
0.724503447566
0.69644358708
0.683588086481
56.934
acc: 0.676
pre: 0.662
rec: 0.724
F1: 0.691
auc: 0.676
