start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.31508773565292
time_elpased: 1.709
batch start
#iterations: 1
currently lose_sum: 99.88121503591537
time_elpased: 1.74
batch start
#iterations: 2
currently lose_sum: 99.81960272789001
time_elpased: 1.732
batch start
#iterations: 3
currently lose_sum: 99.8378557562828
time_elpased: 1.729
batch start
#iterations: 4
currently lose_sum: 99.77652221918106
time_elpased: 1.693
batch start
#iterations: 5
currently lose_sum: 99.5694751739502
time_elpased: 1.71
batch start
#iterations: 6
currently lose_sum: 99.4179350733757
time_elpased: 1.743
batch start
#iterations: 7
currently lose_sum: 99.53616362810135
time_elpased: 1.697
batch start
#iterations: 8
currently lose_sum: 99.54640954732895
time_elpased: 1.751
batch start
#iterations: 9
currently lose_sum: 99.18540281057358
time_elpased: 1.731
batch start
#iterations: 10
currently lose_sum: 99.27354550361633
time_elpased: 1.711
batch start
#iterations: 11
currently lose_sum: 99.36264300346375
time_elpased: 1.711
batch start
#iterations: 12
currently lose_sum: 99.21221297979355
time_elpased: 1.771
batch start
#iterations: 13
currently lose_sum: 99.4083708524704
time_elpased: 1.714
batch start
#iterations: 14
currently lose_sum: 98.86689132452011
time_elpased: 1.706
batch start
#iterations: 15
currently lose_sum: 99.2737637758255
time_elpased: 1.751
batch start
#iterations: 16
currently lose_sum: 99.12700951099396
time_elpased: 1.709
batch start
#iterations: 17
currently lose_sum: 98.80959266424179
time_elpased: 1.735
batch start
#iterations: 18
currently lose_sum: 99.04901975393295
time_elpased: 1.725
batch start
#iterations: 19
currently lose_sum: 98.89835774898529
time_elpased: 1.71
start validation test
0.614948453608
0.588083104665
0.771946073891
0.667586329655
0.614672820071
64.660
batch start
#iterations: 20
currently lose_sum: 98.79690474271774
time_elpased: 1.707
batch start
#iterations: 21
currently lose_sum: 99.58095180988312
time_elpased: 1.77
batch start
#iterations: 22
currently lose_sum: 98.93237793445587
time_elpased: 1.709
batch start
#iterations: 23
currently lose_sum: 98.7643272280693
time_elpased: 1.701
batch start
#iterations: 24
currently lose_sum: 98.79248023033142
time_elpased: 1.732
batch start
#iterations: 25
currently lose_sum: 98.73137295246124
time_elpased: 1.703
batch start
#iterations: 26
currently lose_sum: 98.80662715435028
time_elpased: 1.694
batch start
#iterations: 27
currently lose_sum: 98.48494905233383
time_elpased: 1.719
batch start
#iterations: 28
currently lose_sum: 99.0340507030487
time_elpased: 1.754
batch start
#iterations: 29
currently lose_sum: 98.16555833816528
time_elpased: 1.727
batch start
#iterations: 30
currently lose_sum: 98.25530689954758
time_elpased: 1.717
batch start
#iterations: 31
currently lose_sum: 98.57253980636597
time_elpased: 1.705
batch start
#iterations: 32
currently lose_sum: 98.18272238969803
time_elpased: 1.696
batch start
#iterations: 33
currently lose_sum: 98.04402828216553
time_elpased: 1.752
batch start
#iterations: 34
currently lose_sum: 98.0319983959198
time_elpased: 1.691
batch start
#iterations: 35
currently lose_sum: 97.50565212965012
time_elpased: 1.683
batch start
#iterations: 36
currently lose_sum: 98.05774337053299
time_elpased: 1.7
batch start
#iterations: 37
currently lose_sum: 97.53422683477402
time_elpased: 1.721
batch start
#iterations: 38
currently lose_sum: 97.24960368871689
time_elpased: 1.692
batch start
#iterations: 39
currently lose_sum: 97.33825498819351
time_elpased: 1.704
start validation test
0.614896907216
0.662002308136
0.472265102398
0.551264340201
0.615147319349
63.256
batch start
#iterations: 40
currently lose_sum: 97.30088526010513
time_elpased: 1.705
batch start
#iterations: 41
currently lose_sum: 97.23107349872589
time_elpased: 1.712
batch start
#iterations: 42
currently lose_sum: 97.2995046377182
time_elpased: 1.692
batch start
#iterations: 43
currently lose_sum: 97.42317533493042
time_elpased: 1.718
batch start
#iterations: 44
currently lose_sum: 97.13943642377853
time_elpased: 1.738
batch start
#iterations: 45
currently lose_sum: 96.9163698554039
time_elpased: 1.76
batch start
#iterations: 46
currently lose_sum: 96.897405564785
time_elpased: 1.782
batch start
#iterations: 47
currently lose_sum: 96.56752383708954
time_elpased: 1.711
batch start
#iterations: 48
currently lose_sum: 96.20115596055984
time_elpased: 1.775
batch start
#iterations: 49
currently lose_sum: 96.75361704826355
time_elpased: 1.758
batch start
#iterations: 50
currently lose_sum: 96.60179889202118
time_elpased: 1.696
batch start
#iterations: 51
currently lose_sum: 96.31174182891846
time_elpased: 1.746
batch start
#iterations: 52
currently lose_sum: 96.22966349124908
time_elpased: 1.739
batch start
#iterations: 53
currently lose_sum: 95.95803564786911
time_elpased: 1.718
batch start
#iterations: 54
currently lose_sum: 96.18815582990646
time_elpased: 1.793
batch start
#iterations: 55
currently lose_sum: 96.0036928653717
time_elpased: 1.706
batch start
#iterations: 56
currently lose_sum: 96.42047613859177
time_elpased: 1.731
batch start
#iterations: 57
currently lose_sum: 95.91857320070267
time_elpased: 1.803
batch start
#iterations: 58
currently lose_sum: 95.94090121984482
time_elpased: 1.722
batch start
#iterations: 59
currently lose_sum: 96.1652603149414
time_elpased: 1.792
start validation test
0.649639175258
0.623980978261
0.756200473397
0.683757502443
0.649452090463
61.223
batch start
#iterations: 60
currently lose_sum: 96.03570371866226
time_elpased: 1.734
batch start
#iterations: 61
currently lose_sum: 96.45979982614517
time_elpased: 1.714
batch start
#iterations: 62
currently lose_sum: 96.33878779411316
time_elpased: 1.704
batch start
#iterations: 63
currently lose_sum: 96.04678517580032
time_elpased: 1.701
batch start
#iterations: 64
currently lose_sum: 95.93312752246857
time_elpased: 1.696
batch start
#iterations: 65
currently lose_sum: 96.10714787244797
time_elpased: 1.785
batch start
#iterations: 66
currently lose_sum: 95.77455544471741
time_elpased: 1.693
batch start
#iterations: 67
currently lose_sum: 96.00457870960236
time_elpased: 1.709
batch start
#iterations: 68
currently lose_sum: 95.27683037519455
time_elpased: 1.761
batch start
#iterations: 69
currently lose_sum: 95.47555071115494
time_elpased: 1.729
batch start
#iterations: 70
currently lose_sum: 95.83566898107529
time_elpased: 1.718
batch start
#iterations: 71
currently lose_sum: 95.90215539932251
time_elpased: 1.734
batch start
#iterations: 72
currently lose_sum: 95.56903231143951
time_elpased: 1.731
batch start
#iterations: 73
currently lose_sum: 95.22067803144455
time_elpased: 1.789
batch start
#iterations: 74
currently lose_sum: 95.29125380516052
time_elpased: 1.682
batch start
#iterations: 75
currently lose_sum: 96.07702952623367
time_elpased: 1.706
batch start
#iterations: 76
currently lose_sum: 94.75900882482529
time_elpased: 1.676
batch start
#iterations: 77
currently lose_sum: 95.67380481958389
time_elpased: 1.69
batch start
#iterations: 78
currently lose_sum: 95.09486830234528
time_elpased: 1.713
batch start
#iterations: 79
currently lose_sum: 94.93313276767731
time_elpased: 1.721
start validation test
0.614484536082
0.591917200591
0.74158690954
0.658352747705
0.614261388262
62.599
batch start
#iterations: 80
currently lose_sum: 95.64003449678421
time_elpased: 1.711
batch start
#iterations: 81
currently lose_sum: 95.84583693742752
time_elpased: 1.704
batch start
#iterations: 82
currently lose_sum: 95.30087411403656
time_elpased: 1.722
batch start
#iterations: 83
currently lose_sum: 95.13977146148682
time_elpased: 1.758
batch start
#iterations: 84
currently lose_sum: 95.36427253484726
time_elpased: 1.735
batch start
#iterations: 85
currently lose_sum: 94.85714161396027
time_elpased: 1.775
batch start
#iterations: 86
currently lose_sum: 94.98161315917969
time_elpased: 1.73
batch start
#iterations: 87
currently lose_sum: 95.3301151394844
time_elpased: 1.717
batch start
#iterations: 88
currently lose_sum: 95.18455731868744
time_elpased: 1.747
batch start
#iterations: 89
currently lose_sum: 94.6983847618103
time_elpased: 1.733
batch start
#iterations: 90
currently lose_sum: 94.60513836145401
time_elpased: 1.729
batch start
#iterations: 91
currently lose_sum: 95.3770500421524
time_elpased: 1.738
batch start
#iterations: 92
currently lose_sum: 94.78145796060562
time_elpased: 1.725
batch start
#iterations: 93
currently lose_sum: 94.90530264377594
time_elpased: 1.734
batch start
#iterations: 94
currently lose_sum: 95.11067396402359
time_elpased: 1.69
batch start
#iterations: 95
currently lose_sum: 95.07419151067734
time_elpased: 1.73
batch start
#iterations: 96
currently lose_sum: 94.48005002737045
time_elpased: 1.692
batch start
#iterations: 97
currently lose_sum: 94.59695172309875
time_elpased: 1.729
batch start
#iterations: 98
currently lose_sum: 94.71139353513718
time_elpased: 1.661
batch start
#iterations: 99
currently lose_sum: 94.96424698829651
time_elpased: 1.704
start validation test
0.647422680412
0.603199655642
0.865287640218
0.710855596889
0.647040184872
61.539
batch start
#iterations: 100
currently lose_sum: 94.74095648527145
time_elpased: 1.695
batch start
#iterations: 101
currently lose_sum: 94.51778554916382
time_elpased: 1.736
batch start
#iterations: 102
currently lose_sum: 95.45831888914108
time_elpased: 1.733
batch start
#iterations: 103
currently lose_sum: 94.82752877473831
time_elpased: 1.766
batch start
#iterations: 104
currently lose_sum: 95.27086383104324
time_elpased: 1.698
batch start
#iterations: 105
currently lose_sum: 95.06862139701843
time_elpased: 1.725
batch start
#iterations: 106
currently lose_sum: 95.03163117170334
time_elpased: 1.728
batch start
#iterations: 107
currently lose_sum: 94.10879445075989
time_elpased: 1.733
batch start
#iterations: 108
currently lose_sum: 94.44276756048203
time_elpased: 1.764
batch start
#iterations: 109
currently lose_sum: 95.07461005449295
time_elpased: 1.744
batch start
#iterations: 110
currently lose_sum: 94.37455713748932
time_elpased: 1.73
batch start
#iterations: 111
currently lose_sum: 95.05757093429565
time_elpased: 1.75
batch start
#iterations: 112
currently lose_sum: 93.65460807085037
time_elpased: 1.76
batch start
#iterations: 113
currently lose_sum: 94.12944090366364
time_elpased: 1.704
batch start
#iterations: 114
currently lose_sum: 94.600859105587
time_elpased: 1.773
batch start
#iterations: 115
currently lose_sum: 94.10920113325119
time_elpased: 1.773
batch start
#iterations: 116
currently lose_sum: 94.14330160617828
time_elpased: 1.715
batch start
#iterations: 117
currently lose_sum: 94.02005797624588
time_elpased: 1.71
batch start
#iterations: 118
currently lose_sum: 94.15491276979446
time_elpased: 1.695
batch start
#iterations: 119
currently lose_sum: 94.20831966400146
time_elpased: 1.725
start validation test
0.644587628866
0.65414026655
0.616239580117
0.634624556197
0.644637398238
60.618
batch start
#iterations: 120
currently lose_sum: 94.45689982175827
time_elpased: 1.752
batch start
#iterations: 121
currently lose_sum: 93.94298249483109
time_elpased: 1.711
batch start
#iterations: 122
currently lose_sum: 94.26398611068726
time_elpased: 1.747
batch start
#iterations: 123
currently lose_sum: 93.67375016212463
time_elpased: 1.775
batch start
#iterations: 124
currently lose_sum: 94.26636791229248
time_elpased: 1.706
batch start
#iterations: 125
currently lose_sum: 94.09106308221817
time_elpased: 1.712
batch start
#iterations: 126
currently lose_sum: 93.58501613140106
time_elpased: 1.718
batch start
#iterations: 127
currently lose_sum: 94.99934709072113
time_elpased: 1.746
batch start
#iterations: 128
currently lose_sum: 94.25845628976822
time_elpased: 1.699
batch start
#iterations: 129
currently lose_sum: 93.69675624370575
time_elpased: 1.688
batch start
#iterations: 130
currently lose_sum: 93.87364661693573
time_elpased: 1.723
batch start
#iterations: 131
currently lose_sum: 94.51768028736115
time_elpased: 1.715
batch start
#iterations: 132
currently lose_sum: 93.72769457101822
time_elpased: 1.731
batch start
#iterations: 133
currently lose_sum: 93.58722597360611
time_elpased: 1.721
batch start
#iterations: 134
currently lose_sum: 93.58772552013397
time_elpased: 1.774
batch start
#iterations: 135
currently lose_sum: 93.9209930896759
time_elpased: 1.805
batch start
#iterations: 136
currently lose_sum: 94.56305682659149
time_elpased: 1.704
batch start
#iterations: 137
currently lose_sum: 94.61440414190292
time_elpased: 1.778
batch start
#iterations: 138
currently lose_sum: 93.66676986217499
time_elpased: 1.717
batch start
#iterations: 139
currently lose_sum: 93.44001072645187
time_elpased: 1.702
start validation test
0.646082474227
0.667528499236
0.584542554286
0.623285416438
0.646190517048
60.101
batch start
#iterations: 140
currently lose_sum: 93.2928757071495
time_elpased: 1.74
batch start
#iterations: 141
currently lose_sum: 93.51398468017578
time_elpased: 1.697
batch start
#iterations: 142
currently lose_sum: 93.20061337947845
time_elpased: 1.736
batch start
#iterations: 143
currently lose_sum: 94.08826678991318
time_elpased: 1.725
batch start
#iterations: 144
currently lose_sum: 93.65467971563339
time_elpased: 1.734
batch start
#iterations: 145
currently lose_sum: 93.799092233181
time_elpased: 1.712
batch start
#iterations: 146
currently lose_sum: 93.42480963468552
time_elpased: 1.696
batch start
#iterations: 147
currently lose_sum: 94.43224900960922
time_elpased: 1.722
batch start
#iterations: 148
currently lose_sum: 93.4515335559845
time_elpased: 1.7
batch start
#iterations: 149
currently lose_sum: 93.3807590007782
time_elpased: 1.74
batch start
#iterations: 150
currently lose_sum: 93.49057775735855
time_elpased: 1.709
batch start
#iterations: 151
currently lose_sum: 93.32104736566544
time_elpased: 1.779
batch start
#iterations: 152
currently lose_sum: 93.11269187927246
time_elpased: 1.715
batch start
#iterations: 153
currently lose_sum: 93.10122311115265
time_elpased: 1.739
batch start
#iterations: 154
currently lose_sum: 93.57060813903809
time_elpased: 1.717
batch start
#iterations: 155
currently lose_sum: 93.51079380512238
time_elpased: 1.72
batch start
#iterations: 156
currently lose_sum: 92.89586561918259
time_elpased: 1.733
batch start
#iterations: 157
currently lose_sum: 93.40055960416794
time_elpased: 1.736
batch start
#iterations: 158
currently lose_sum: 94.39391756057739
time_elpased: 1.7
batch start
#iterations: 159
currently lose_sum: 92.59147012233734
time_elpased: 1.711
start validation test
0.672731958763
0.652315484805
0.742204384069
0.694362874886
0.672609989205
58.664
batch start
#iterations: 160
currently lose_sum: 93.50633454322815
time_elpased: 1.703
batch start
#iterations: 161
currently lose_sum: 93.0757040977478
time_elpased: 1.729
batch start
#iterations: 162
currently lose_sum: 91.96527594327927
time_elpased: 1.697
batch start
#iterations: 163
currently lose_sum: 93.14670342206955
time_elpased: 1.714
batch start
#iterations: 164
currently lose_sum: 93.38547098636627
time_elpased: 1.714
batch start
#iterations: 165
currently lose_sum: 92.45774805545807
time_elpased: 1.711
batch start
#iterations: 166
currently lose_sum: 93.22655272483826
time_elpased: 1.697
batch start
#iterations: 167
currently lose_sum: 93.24018168449402
time_elpased: 1.729
batch start
#iterations: 168
currently lose_sum: 93.46130990982056
time_elpased: 1.787
batch start
#iterations: 169
currently lose_sum: 92.49287885427475
time_elpased: 1.684
batch start
#iterations: 170
currently lose_sum: 92.51046204566956
time_elpased: 1.715
batch start
#iterations: 171
currently lose_sum: 92.51563054323196
time_elpased: 1.716
batch start
#iterations: 172
currently lose_sum: 93.10360342264175
time_elpased: 1.719
batch start
#iterations: 173
currently lose_sum: 92.49625730514526
time_elpased: 1.776
batch start
#iterations: 174
currently lose_sum: 91.8785685300827
time_elpased: 1.695
batch start
#iterations: 175
currently lose_sum: 93.5434268116951
time_elpased: 1.747
batch start
#iterations: 176
currently lose_sum: 93.0564831495285
time_elpased: 1.656
batch start
#iterations: 177
currently lose_sum: 92.8621631860733
time_elpased: 1.726
batch start
#iterations: 178
currently lose_sum: 92.92794167995453
time_elpased: 1.699
batch start
#iterations: 179
currently lose_sum: 92.00980007648468
time_elpased: 1.761
start validation test
0.583659793814
0.568470273881
0.700627765771
0.627667911308
0.583454438499
63.711
batch start
#iterations: 180
currently lose_sum: 92.49444168806076
time_elpased: 1.715
batch start
#iterations: 181
currently lose_sum: 93.13031351566315
time_elpased: 1.705
batch start
#iterations: 182
currently lose_sum: 92.5601994395256
time_elpased: 1.669
batch start
#iterations: 183
currently lose_sum: 92.56626683473587
time_elpased: 1.717
batch start
#iterations: 184
currently lose_sum: 92.6075793504715
time_elpased: 1.74
batch start
#iterations: 185
currently lose_sum: 91.88679498434067
time_elpased: 1.74
batch start
#iterations: 186
currently lose_sum: 92.60259753465652
time_elpased: 1.737
batch start
#iterations: 187
currently lose_sum: 93.66480803489685
time_elpased: 1.737
batch start
#iterations: 188
currently lose_sum: 93.18288969993591
time_elpased: 1.705
batch start
#iterations: 189
currently lose_sum: 91.86604350805283
time_elpased: 1.747
batch start
#iterations: 190
currently lose_sum: 92.48827511072159
time_elpased: 1.661
batch start
#iterations: 191
currently lose_sum: 92.40007865428925
time_elpased: 1.702
batch start
#iterations: 192
currently lose_sum: 92.53718411922455
time_elpased: 1.729
batch start
#iterations: 193
currently lose_sum: 92.3539428114891
time_elpased: 1.725
batch start
#iterations: 194
currently lose_sum: 92.99214339256287
time_elpased: 1.719
batch start
#iterations: 195
currently lose_sum: 92.52312219142914
time_elpased: 1.714
batch start
#iterations: 196
currently lose_sum: 94.03051191568375
time_elpased: 1.749
batch start
#iterations: 197
currently lose_sum: 92.63185256719589
time_elpased: 1.765
batch start
#iterations: 198
currently lose_sum: 91.86836355924606
time_elpased: 1.729
batch start
#iterations: 199
currently lose_sum: 92.40442550182343
time_elpased: 1.737
start validation test
0.645412371134
0.626066098081
0.725223834517
0.672006865971
0.6452722498
60.313
batch start
#iterations: 200
currently lose_sum: 92.49438405036926
time_elpased: 1.716
batch start
#iterations: 201
currently lose_sum: 92.2636067867279
time_elpased: 1.686
batch start
#iterations: 202
currently lose_sum: 92.11177587509155
time_elpased: 1.695
batch start
#iterations: 203
currently lose_sum: 92.27289825677872
time_elpased: 1.729
batch start
#iterations: 204
currently lose_sum: 92.03835499286652
time_elpased: 1.696
batch start
#iterations: 205
currently lose_sum: 92.37867790460587
time_elpased: 1.773
batch start
#iterations: 206
currently lose_sum: 92.43839305639267
time_elpased: 1.719
batch start
#iterations: 207
currently lose_sum: 92.19349956512451
time_elpased: 1.684
batch start
#iterations: 208
currently lose_sum: 92.25028389692307
time_elpased: 1.716
batch start
#iterations: 209
currently lose_sum: 92.63084983825684
time_elpased: 1.704
batch start
#iterations: 210
currently lose_sum: 92.29025453329086
time_elpased: 1.682
batch start
#iterations: 211
currently lose_sum: 91.86133170127869
time_elpased: 1.739
batch start
#iterations: 212
currently lose_sum: 92.55901801586151
time_elpased: 1.761
batch start
#iterations: 213
currently lose_sum: 92.55443596839905
time_elpased: 1.712
batch start
#iterations: 214
currently lose_sum: 91.90725076198578
time_elpased: 1.728
batch start
#iterations: 215
currently lose_sum: 91.9287918806076
time_elpased: 1.755
batch start
#iterations: 216
currently lose_sum: 91.73226088285446
time_elpased: 1.697
batch start
#iterations: 217
currently lose_sum: 92.15411424636841
time_elpased: 1.734
batch start
#iterations: 218
currently lose_sum: 91.42646187543869
time_elpased: 1.715
batch start
#iterations: 219
currently lose_sum: 92.03389793634415
time_elpased: 1.687
start validation test
0.67293814433
0.656954012288
0.726252958732
0.689867539958
0.67284454195
58.281
batch start
#iterations: 220
currently lose_sum: 92.17979502677917
time_elpased: 1.746
batch start
#iterations: 221
currently lose_sum: 91.86919796466827
time_elpased: 1.729
batch start
#iterations: 222
currently lose_sum: 92.10897195339203
time_elpased: 1.697
batch start
#iterations: 223
currently lose_sum: 92.0873014330864
time_elpased: 1.707
batch start
#iterations: 224
currently lose_sum: 91.80786263942719
time_elpased: 1.726
batch start
#iterations: 225
currently lose_sum: 91.29954010248184
time_elpased: 1.759
batch start
#iterations: 226
currently lose_sum: 92.0366781949997
time_elpased: 1.729
batch start
#iterations: 227
currently lose_sum: 91.77982956171036
time_elpased: 1.722
batch start
#iterations: 228
currently lose_sum: 91.86091256141663
time_elpased: 1.736
batch start
#iterations: 229
currently lose_sum: 92.17051315307617
time_elpased: 1.72
batch start
#iterations: 230
currently lose_sum: 92.16894292831421
time_elpased: 1.71
batch start
#iterations: 231
currently lose_sum: 92.10765933990479
time_elpased: 1.746
batch start
#iterations: 232
currently lose_sum: 91.7261957526207
time_elpased: 1.664
batch start
#iterations: 233
currently lose_sum: 91.33079236745834
time_elpased: 1.703
batch start
#iterations: 234
currently lose_sum: 91.85192096233368
time_elpased: 1.696
batch start
#iterations: 235
currently lose_sum: 91.39387089014053
time_elpased: 1.706
batch start
#iterations: 236
currently lose_sum: 92.31387197971344
time_elpased: 1.676
batch start
#iterations: 237
currently lose_sum: 92.05139142274857
time_elpased: 1.764
batch start
#iterations: 238
currently lose_sum: 91.32890951633453
time_elpased: 1.678
batch start
#iterations: 239
currently lose_sum: 91.5455247759819
time_elpased: 1.727
start validation test
0.673505154639
0.680272833848
0.656889986621
0.668376963351
0.673534325129
57.904
batch start
#iterations: 240
currently lose_sum: 91.85751527547836
time_elpased: 1.762
batch start
#iterations: 241
currently lose_sum: 92.19762128591537
time_elpased: 1.712
batch start
#iterations: 242
currently lose_sum: 91.54315477609634
time_elpased: 1.777
batch start
#iterations: 243
currently lose_sum: 91.6739074587822
time_elpased: 1.733
batch start
#iterations: 244
currently lose_sum: 91.23489654064178
time_elpased: 1.691
batch start
#iterations: 245
currently lose_sum: 92.01695489883423
time_elpased: 1.752
batch start
#iterations: 246
currently lose_sum: 91.35291785001755
time_elpased: 1.717
batch start
#iterations: 247
currently lose_sum: 91.06843638420105
time_elpased: 1.739
batch start
#iterations: 248
currently lose_sum: 92.0822097659111
time_elpased: 1.714
batch start
#iterations: 249
currently lose_sum: 90.92690318822861
time_elpased: 1.696
batch start
#iterations: 250
currently lose_sum: 90.84669232368469
time_elpased: 1.71
batch start
#iterations: 251
currently lose_sum: 91.19113349914551
time_elpased: 1.717
batch start
#iterations: 252
currently lose_sum: 91.42265367507935
time_elpased: 1.723
batch start
#iterations: 253
currently lose_sum: 91.02280622720718
time_elpased: 1.717
batch start
#iterations: 254
currently lose_sum: 91.31513231992722
time_elpased: 1.69
batch start
#iterations: 255
currently lose_sum: 90.70022881031036
time_elpased: 1.696
batch start
#iterations: 256
currently lose_sum: 90.8459153175354
time_elpased: 1.763
batch start
#iterations: 257
currently lose_sum: 91.37581992149353
time_elpased: 1.721
batch start
#iterations: 258
currently lose_sum: 90.90665888786316
time_elpased: 1.694
batch start
#iterations: 259
currently lose_sum: 91.00697427988052
time_elpased: 1.748
start validation test
0.678659793814
0.664338963858
0.724503447566
0.693118046667
0.678579308209
57.517
batch start
#iterations: 260
currently lose_sum: 91.14377862215042
time_elpased: 1.751
batch start
#iterations: 261
currently lose_sum: 91.1599754691124
time_elpased: 1.728
batch start
#iterations: 262
currently lose_sum: 90.56378918886185
time_elpased: 1.691
batch start
#iterations: 263
currently lose_sum: 90.71854996681213
time_elpased: 1.723
batch start
#iterations: 264
currently lose_sum: 91.54272204637527
time_elpased: 1.71
batch start
#iterations: 265
currently lose_sum: 91.15814846754074
time_elpased: 1.735
batch start
#iterations: 266
currently lose_sum: 91.40917259454727
time_elpased: 1.698
batch start
#iterations: 267
currently lose_sum: 90.66035634279251
time_elpased: 1.725
batch start
#iterations: 268
currently lose_sum: 90.97974187135696
time_elpased: 1.74
batch start
#iterations: 269
currently lose_sum: 91.1498646736145
time_elpased: 1.703
batch start
#iterations: 270
currently lose_sum: 90.71641045808792
time_elpased: 1.754
batch start
#iterations: 271
currently lose_sum: 91.22862237691879
time_elpased: 1.716
batch start
#iterations: 272
currently lose_sum: 91.00290584564209
time_elpased: 1.709
batch start
#iterations: 273
currently lose_sum: 90.52558606863022
time_elpased: 1.693
batch start
#iterations: 274
currently lose_sum: 91.46567803621292
time_elpased: 1.672
batch start
#iterations: 275
currently lose_sum: 90.66289836168289
time_elpased: 1.764
batch start
#iterations: 276
currently lose_sum: 90.91435480117798
time_elpased: 1.737
batch start
#iterations: 277
currently lose_sum: 90.65942305326462
time_elpased: 1.762
batch start
#iterations: 278
currently lose_sum: 91.13538068532944
time_elpased: 1.721
batch start
#iterations: 279
currently lose_sum: 90.68921929597855
time_elpased: 1.75
start validation test
0.675515463918
0.689269911504
0.641247298549
0.664391960335
0.675575626967
57.675
batch start
#iterations: 280
currently lose_sum: 90.5805259346962
time_elpased: 1.689
batch start
#iterations: 281
currently lose_sum: 90.65144318342209
time_elpased: 1.73
batch start
#iterations: 282
currently lose_sum: 89.8418481349945
time_elpased: 1.736
batch start
#iterations: 283
currently lose_sum: 90.36545479297638
time_elpased: 1.742
batch start
#iterations: 284
currently lose_sum: 90.92639356851578
time_elpased: 1.71
batch start
#iterations: 285
currently lose_sum: 90.59519010782242
time_elpased: 1.712
batch start
#iterations: 286
currently lose_sum: 91.06135082244873
time_elpased: 1.728
batch start
#iterations: 287
currently lose_sum: 90.66009950637817
time_elpased: 1.738
batch start
#iterations: 288
currently lose_sum: 90.30715245008469
time_elpased: 1.74
batch start
#iterations: 289
currently lose_sum: 90.63156908750534
time_elpased: 1.708
batch start
#iterations: 290
currently lose_sum: 90.29012775421143
time_elpased: 1.698
batch start
#iterations: 291
currently lose_sum: 90.45368945598602
time_elpased: 1.714
batch start
#iterations: 292
currently lose_sum: 90.48243123292923
time_elpased: 1.672
batch start
#iterations: 293
currently lose_sum: 90.26449602842331
time_elpased: 1.711
batch start
#iterations: 294
currently lose_sum: 90.31333124637604
time_elpased: 1.693
batch start
#iterations: 295
currently lose_sum: 89.8738421201706
time_elpased: 1.753
batch start
#iterations: 296
currently lose_sum: 90.21134799718857
time_elpased: 1.668
batch start
#iterations: 297
currently lose_sum: 91.47884380817413
time_elpased: 1.704
batch start
#iterations: 298
currently lose_sum: 90.21596902608871
time_elpased: 1.673
batch start
#iterations: 299
currently lose_sum: 90.13555520772934
time_elpased: 1.682
start validation test
0.655618556701
0.646158290006
0.690645260883
0.667661543053
0.655557061919
59.079
batch start
#iterations: 300
currently lose_sum: 90.27544915676117
time_elpased: 1.744
batch start
#iterations: 301
currently lose_sum: 90.22928881645203
time_elpased: 1.725
batch start
#iterations: 302
currently lose_sum: 90.21688514947891
time_elpased: 1.696
batch start
#iterations: 303
currently lose_sum: 90.329913854599
time_elpased: 1.704
batch start
#iterations: 304
currently lose_sum: 90.46384644508362
time_elpased: 1.709
batch start
#iterations: 305
currently lose_sum: 90.63871717453003
time_elpased: 1.712
batch start
#iterations: 306
currently lose_sum: 90.38320136070251
time_elpased: 1.72
batch start
#iterations: 307
currently lose_sum: 89.86490899324417
time_elpased: 1.713
batch start
#iterations: 308
currently lose_sum: 89.90052551031113
time_elpased: 1.722
batch start
#iterations: 309
currently lose_sum: 90.42940527200699
time_elpased: 1.717
batch start
#iterations: 310
currently lose_sum: 90.16939556598663
time_elpased: 1.71
batch start
#iterations: 311
currently lose_sum: 90.13704198598862
time_elpased: 1.7
batch start
#iterations: 312
currently lose_sum: 89.60862618684769
time_elpased: 1.727
batch start
#iterations: 313
currently lose_sum: 89.67657870054245
time_elpased: 1.725
batch start
#iterations: 314
currently lose_sum: 89.43996840715408
time_elpased: 1.67
batch start
#iterations: 315
currently lose_sum: 89.87993693351746
time_elpased: 1.711
batch start
#iterations: 316
currently lose_sum: 90.05510491132736
time_elpased: 1.722
batch start
#iterations: 317
currently lose_sum: 90.11404484510422
time_elpased: 1.791
batch start
#iterations: 318
currently lose_sum: 89.6363490819931
time_elpased: 1.742
batch start
#iterations: 319
currently lose_sum: 89.82946187257767
time_elpased: 1.733
start validation test
0.683195876289
0.67042092202
0.722856848822
0.695652173913
0.683126245334
57.121
batch start
#iterations: 320
currently lose_sum: 89.33002161979675
time_elpased: 1.707
batch start
#iterations: 321
currently lose_sum: 90.12456160783768
time_elpased: 1.712
batch start
#iterations: 322
currently lose_sum: 89.67369711399078
time_elpased: 1.737
batch start
#iterations: 323
currently lose_sum: 89.8811057806015
time_elpased: 1.734
batch start
#iterations: 324
currently lose_sum: 89.67236268520355
time_elpased: 1.703
batch start
#iterations: 325
currently lose_sum: 90.23408478498459
time_elpased: 1.776
batch start
#iterations: 326
currently lose_sum: 89.04314732551575
time_elpased: 1.722
batch start
#iterations: 327
currently lose_sum: 90.28109365701675
time_elpased: 1.727
batch start
#iterations: 328
currently lose_sum: 90.75583082437515
time_elpased: 1.685
batch start
#iterations: 329
currently lose_sum: 89.85456842184067
time_elpased: 1.733
batch start
#iterations: 330
currently lose_sum: 89.79455506801605
time_elpased: 1.739
batch start
#iterations: 331
currently lose_sum: 89.62925511598587
time_elpased: 1.756
batch start
#iterations: 332
currently lose_sum: 89.12922275066376
time_elpased: 1.689
batch start
#iterations: 333
currently lose_sum: 89.87640452384949
time_elpased: 1.716
batch start
#iterations: 334
currently lose_sum: 89.70951706171036
time_elpased: 1.739
batch start
#iterations: 335
currently lose_sum: 89.53939628601074
time_elpased: 1.733
batch start
#iterations: 336
currently lose_sum: 90.50942772626877
time_elpased: 1.699
batch start
#iterations: 337
currently lose_sum: 89.44084072113037
time_elpased: 1.754
batch start
#iterations: 338
currently lose_sum: 89.19755816459656
time_elpased: 1.795
batch start
#iterations: 339
currently lose_sum: 89.64877605438232
time_elpased: 1.713
start validation test
0.653505154639
0.666075191305
0.618092003705
0.641187146365
0.653567327888
58.703
batch start
#iterations: 340
currently lose_sum: 89.49938559532166
time_elpased: 1.677
batch start
#iterations: 341
currently lose_sum: 90.02518236637115
time_elpased: 1.722
batch start
#iterations: 342
currently lose_sum: 89.10374623537064
time_elpased: 1.719
batch start
#iterations: 343
currently lose_sum: 89.53555405139923
time_elpased: 1.736
batch start
#iterations: 344
currently lose_sum: 89.421462059021
time_elpased: 1.79
batch start
#iterations: 345
currently lose_sum: 90.05237764120102
time_elpased: 1.694
batch start
#iterations: 346
currently lose_sum: 89.29911363124847
time_elpased: 1.745
batch start
#iterations: 347
currently lose_sum: 89.13469511270523
time_elpased: 1.728
batch start
#iterations: 348
currently lose_sum: 89.57277727127075
time_elpased: 1.71
batch start
#iterations: 349
currently lose_sum: 89.67081469297409
time_elpased: 1.718
batch start
#iterations: 350
currently lose_sum: 89.5785129070282
time_elpased: 1.674
batch start
#iterations: 351
currently lose_sum: 88.81637048721313
time_elpased: 1.741
batch start
#iterations: 352
currently lose_sum: 89.28690183162689
time_elpased: 1.762
batch start
#iterations: 353
currently lose_sum: 88.18223041296005
time_elpased: 1.762
batch start
#iterations: 354
currently lose_sum: 90.72317707538605
time_elpased: 1.739
batch start
#iterations: 355
currently lose_sum: 89.20231300592422
time_elpased: 1.7
batch start
#iterations: 356
currently lose_sum: 89.30698704719543
time_elpased: 1.695
batch start
#iterations: 357
currently lose_sum: 89.49392449855804
time_elpased: 1.762
batch start
#iterations: 358
currently lose_sum: 88.63992869853973
time_elpased: 1.693
batch start
#iterations: 359
currently lose_sum: 89.2631271481514
time_elpased: 1.771
start validation test
0.63793814433
0.617814331963
0.726664608418
0.667833159936
0.637782371337
60.179
batch start
#iterations: 360
currently lose_sum: 89.16027265787125
time_elpased: 1.771
batch start
#iterations: 361
currently lose_sum: 89.96193289756775
time_elpased: 1.687
batch start
#iterations: 362
currently lose_sum: 88.71517264842987
time_elpased: 1.738
batch start
#iterations: 363
currently lose_sum: 89.66750502586365
time_elpased: 1.751
batch start
#iterations: 364
currently lose_sum: 88.25039303302765
time_elpased: 1.695
batch start
#iterations: 365
currently lose_sum: 89.49077361822128
time_elpased: 1.774
batch start
#iterations: 366
currently lose_sum: 88.96877330541611
time_elpased: 1.709
batch start
#iterations: 367
currently lose_sum: 89.02672439813614
time_elpased: 1.785
batch start
#iterations: 368
currently lose_sum: 88.89507961273193
time_elpased: 1.721
batch start
#iterations: 369
currently lose_sum: 88.90168726444244
time_elpased: 1.721
batch start
#iterations: 370
currently lose_sum: 88.72274327278137
time_elpased: 1.76
batch start
#iterations: 371
currently lose_sum: 88.49712359905243
time_elpased: 1.695
batch start
#iterations: 372
currently lose_sum: 88.20021486282349
time_elpased: 1.683
batch start
#iterations: 373
currently lose_sum: 89.10705560445786
time_elpased: 1.812
batch start
#iterations: 374
currently lose_sum: 88.72093266248703
time_elpased: 1.704
batch start
#iterations: 375
currently lose_sum: 89.09889566898346
time_elpased: 1.76
batch start
#iterations: 376
currently lose_sum: 88.77003467082977
time_elpased: 1.749
batch start
#iterations: 377
currently lose_sum: 88.8213467001915
time_elpased: 1.712
batch start
#iterations: 378
currently lose_sum: 88.77619034051895
time_elpased: 1.733
batch start
#iterations: 379
currently lose_sum: 88.85973423719406
time_elpased: 1.733
start validation test
0.680515463918
0.680925449871
0.681486055367
0.68120563728
0.680513759895
56.960
batch start
#iterations: 380
currently lose_sum: 88.72094410657883
time_elpased: 1.704
batch start
#iterations: 381
currently lose_sum: 88.74707210063934
time_elpased: 1.746
batch start
#iterations: 382
currently lose_sum: 88.66599482297897
time_elpased: 1.732
batch start
#iterations: 383
currently lose_sum: 88.45371836423874
time_elpased: 1.742
batch start
#iterations: 384
currently lose_sum: 88.70464849472046
time_elpased: 1.729
batch start
#iterations: 385
currently lose_sum: 89.10764682292938
time_elpased: 1.734
batch start
#iterations: 386
currently lose_sum: 88.92228055000305
time_elpased: 1.706
batch start
#iterations: 387
currently lose_sum: 88.8456409573555
time_elpased: 1.736
batch start
#iterations: 388
currently lose_sum: 89.23919123411179
time_elpased: 1.741
batch start
#iterations: 389
currently lose_sum: 89.29424524307251
time_elpased: 1.709
batch start
#iterations: 390
currently lose_sum: 88.62705147266388
time_elpased: 1.743
batch start
#iterations: 391
currently lose_sum: 88.67225968837738
time_elpased: 1.708
batch start
#iterations: 392
currently lose_sum: 88.35120624303818
time_elpased: 1.698
batch start
#iterations: 393
currently lose_sum: 88.27233231067657
time_elpased: 1.737
batch start
#iterations: 394
currently lose_sum: 87.95739328861237
time_elpased: 1.686
batch start
#iterations: 395
currently lose_sum: 88.16010284423828
time_elpased: 1.743
batch start
#iterations: 396
currently lose_sum: 88.60329210758209
time_elpased: 1.729
batch start
#iterations: 397
currently lose_sum: 88.7749953866005
time_elpased: 1.695
batch start
#iterations: 398
currently lose_sum: 87.97740226984024
time_elpased: 1.73
batch start
#iterations: 399
currently lose_sum: 88.3066799044609
time_elpased: 1.705
start validation test
0.672577319588
0.674950608298
0.668004528147
0.671459604841
0.672585347828
57.453
acc: 0.678
pre: 0.678
rec: 0.678
F1: 0.678
auc: 0.678
