start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.70367646217346
time_elpased: 1.8
batch start
#iterations: 1
currently lose_sum: 100.3575564622879
time_elpased: 1.794
batch start
#iterations: 2
currently lose_sum: 100.35962015390396
time_elpased: 1.808
batch start
#iterations: 3
currently lose_sum: 100.06230038404465
time_elpased: 1.781
batch start
#iterations: 4
currently lose_sum: 100.0695281624794
time_elpased: 1.791
batch start
#iterations: 5
currently lose_sum: 100.01120430231094
time_elpased: 1.802
batch start
#iterations: 6
currently lose_sum: 99.89500480890274
time_elpased: 1.801
batch start
#iterations: 7
currently lose_sum: 99.70152652263641
time_elpased: 1.824
batch start
#iterations: 8
currently lose_sum: 99.46582478284836
time_elpased: 1.825
batch start
#iterations: 9
currently lose_sum: 99.59045106172562
time_elpased: 1.772
batch start
#iterations: 10
currently lose_sum: 99.59858345985413
time_elpased: 1.763
batch start
#iterations: 11
currently lose_sum: 99.49583035707474
time_elpased: 1.807
batch start
#iterations: 12
currently lose_sum: 99.3461999297142
time_elpased: 1.791
batch start
#iterations: 13
currently lose_sum: 99.26766496896744
time_elpased: 1.77
batch start
#iterations: 14
currently lose_sum: 99.24659126996994
time_elpased: 1.824
batch start
#iterations: 15
currently lose_sum: 99.10252749919891
time_elpased: 1.804
batch start
#iterations: 16
currently lose_sum: 99.11906737089157
time_elpased: 1.776
batch start
#iterations: 17
currently lose_sum: 98.90211164951324
time_elpased: 1.797
batch start
#iterations: 18
currently lose_sum: 98.85906374454498
time_elpased: 1.824
batch start
#iterations: 19
currently lose_sum: 98.6638794541359
time_elpased: 1.792
start validation test
0.600979381443
0.602066115702
0.599773592673
0.600917667681
0.600981498391
65.065
batch start
#iterations: 20
currently lose_sum: 98.6935760974884
time_elpased: 1.807
batch start
#iterations: 21
currently lose_sum: 98.4431381225586
time_elpased: 1.852
batch start
#iterations: 22
currently lose_sum: 98.47763031721115
time_elpased: 1.805
batch start
#iterations: 23
currently lose_sum: 98.38612198829651
time_elpased: 1.781
batch start
#iterations: 24
currently lose_sum: 98.08719301223755
time_elpased: 1.787
batch start
#iterations: 25
currently lose_sum: 97.92703247070312
time_elpased: 1.801
batch start
#iterations: 26
currently lose_sum: 98.1157539486885
time_elpased: 1.816
batch start
#iterations: 27
currently lose_sum: 97.78134149312973
time_elpased: 1.808
batch start
#iterations: 28
currently lose_sum: 97.65622299909592
time_elpased: 1.798
batch start
#iterations: 29
currently lose_sum: 97.69805073738098
time_elpased: 1.794
batch start
#iterations: 30
currently lose_sum: 97.30271303653717
time_elpased: 1.83
batch start
#iterations: 31
currently lose_sum: 97.34890419244766
time_elpased: 1.817
batch start
#iterations: 32
currently lose_sum: 97.28680181503296
time_elpased: 1.776
batch start
#iterations: 33
currently lose_sum: 97.31964647769928
time_elpased: 1.781
batch start
#iterations: 34
currently lose_sum: 97.12819159030914
time_elpased: 1.768
batch start
#iterations: 35
currently lose_sum: 96.94855612516403
time_elpased: 1.821
batch start
#iterations: 36
currently lose_sum: 96.99420231580734
time_elpased: 1.797
batch start
#iterations: 37
currently lose_sum: 96.90341436862946
time_elpased: 1.816
batch start
#iterations: 38
currently lose_sum: 96.9333063364029
time_elpased: 1.804
batch start
#iterations: 39
currently lose_sum: 96.89002299308777
time_elpased: 1.803
start validation test
0.612989690722
0.581410776148
0.811773181023
0.677546813262
0.612640695644
63.617
batch start
#iterations: 40
currently lose_sum: 96.85282379388809
time_elpased: 1.765
batch start
#iterations: 41
currently lose_sum: 97.00915330648422
time_elpased: 1.822
batch start
#iterations: 42
currently lose_sum: 96.28134262561798
time_elpased: 1.834
batch start
#iterations: 43
currently lose_sum: 96.1946946978569
time_elpased: 1.824
batch start
#iterations: 44
currently lose_sum: 96.46592992544174
time_elpased: 1.822
batch start
#iterations: 45
currently lose_sum: 96.27950423955917
time_elpased: 1.809
batch start
#iterations: 46
currently lose_sum: 96.14417165517807
time_elpased: 1.787
batch start
#iterations: 47
currently lose_sum: 96.53118920326233
time_elpased: 1.778
batch start
#iterations: 48
currently lose_sum: 96.32782393693924
time_elpased: 1.791
batch start
#iterations: 49
currently lose_sum: 96.29830223321915
time_elpased: 1.765
batch start
#iterations: 50
currently lose_sum: 96.16371250152588
time_elpased: 1.788
batch start
#iterations: 51
currently lose_sum: 95.90832591056824
time_elpased: 1.8
batch start
#iterations: 52
currently lose_sum: 96.12212598323822
time_elpased: 1.807
batch start
#iterations: 53
currently lose_sum: 95.41553276777267
time_elpased: 1.814
batch start
#iterations: 54
currently lose_sum: 95.89703685045242
time_elpased: 1.834
batch start
#iterations: 55
currently lose_sum: 95.99162608385086
time_elpased: 1.802
batch start
#iterations: 56
currently lose_sum: 96.12251007556915
time_elpased: 1.83
batch start
#iterations: 57
currently lose_sum: 95.75975006818771
time_elpased: 1.797
batch start
#iterations: 58
currently lose_sum: 96.013680934906
time_elpased: 1.798
batch start
#iterations: 59
currently lose_sum: 95.83214408159256
time_elpased: 1.788
start validation test
0.633865979381
0.617261797954
0.708037460121
0.659540813881
0.633735759907
62.187
batch start
#iterations: 60
currently lose_sum: 95.76902168989182
time_elpased: 1.773
batch start
#iterations: 61
currently lose_sum: 95.86124461889267
time_elpased: 1.783
batch start
#iterations: 62
currently lose_sum: 95.42476564645767
time_elpased: 1.788
batch start
#iterations: 63
currently lose_sum: 95.48960000276566
time_elpased: 1.796
batch start
#iterations: 64
currently lose_sum: 95.66511970758438
time_elpased: 1.784
batch start
#iterations: 65
currently lose_sum: 95.05135655403137
time_elpased: 1.782
batch start
#iterations: 66
currently lose_sum: 95.3215103149414
time_elpased: 1.806
batch start
#iterations: 67
currently lose_sum: 95.55426806211472
time_elpased: 1.836
batch start
#iterations: 68
currently lose_sum: 95.168954372406
time_elpased: 1.786
batch start
#iterations: 69
currently lose_sum: 95.02343958616257
time_elpased: 1.762
batch start
#iterations: 70
currently lose_sum: 94.61150586605072
time_elpased: 1.777
batch start
#iterations: 71
currently lose_sum: 94.77199244499207
time_elpased: 1.838
batch start
#iterations: 72
currently lose_sum: 95.04109054803848
time_elpased: 1.811
batch start
#iterations: 73
currently lose_sum: 94.80185580253601
time_elpased: 1.786
batch start
#iterations: 74
currently lose_sum: 94.60324621200562
time_elpased: 1.81
batch start
#iterations: 75
currently lose_sum: 94.8800698518753
time_elpased: 1.824
batch start
#iterations: 76
currently lose_sum: 94.94130951166153
time_elpased: 1.787
batch start
#iterations: 77
currently lose_sum: 95.09228748083115
time_elpased: 1.793
batch start
#iterations: 78
currently lose_sum: 94.9855929017067
time_elpased: 1.783
batch start
#iterations: 79
currently lose_sum: 94.47991704940796
time_elpased: 1.785
start validation test
0.615309278351
0.61943620178
0.601523103839
0.610348248316
0.615333482106
62.679
batch start
#iterations: 80
currently lose_sum: 94.52717864513397
time_elpased: 1.763
batch start
#iterations: 81
currently lose_sum: 95.11459732055664
time_elpased: 1.785
batch start
#iterations: 82
currently lose_sum: 94.91921037435532
time_elpased: 1.764
batch start
#iterations: 83
currently lose_sum: 94.88395607471466
time_elpased: 1.761
batch start
#iterations: 84
currently lose_sum: 94.43371802568436
time_elpased: 1.74
batch start
#iterations: 85
currently lose_sum: 94.78927648067474
time_elpased: 1.795
batch start
#iterations: 86
currently lose_sum: 94.68823945522308
time_elpased: 1.817
batch start
#iterations: 87
currently lose_sum: 94.45394945144653
time_elpased: 1.756
batch start
#iterations: 88
currently lose_sum: 94.18856030702591
time_elpased: 1.778
batch start
#iterations: 89
currently lose_sum: 94.3648824095726
time_elpased: 1.757
batch start
#iterations: 90
currently lose_sum: 94.3494935631752
time_elpased: 1.788
batch start
#iterations: 91
currently lose_sum: 94.31855124235153
time_elpased: 1.792
batch start
#iterations: 92
currently lose_sum: 94.3173720240593
time_elpased: 1.771
batch start
#iterations: 93
currently lose_sum: 93.9765539765358
time_elpased: 1.801
batch start
#iterations: 94
currently lose_sum: 94.04165035486221
time_elpased: 1.744
batch start
#iterations: 95
currently lose_sum: 94.66015189886093
time_elpased: 1.768
batch start
#iterations: 96
currently lose_sum: 94.24637848138809
time_elpased: 1.786
batch start
#iterations: 97
currently lose_sum: 93.95878726243973
time_elpased: 1.755
batch start
#iterations: 98
currently lose_sum: 94.32193917036057
time_elpased: 1.798
batch start
#iterations: 99
currently lose_sum: 94.14698588848114
time_elpased: 1.772
start validation test
0.634226804124
0.618909354868
0.701965627251
0.65782621275
0.634107878172
61.676
batch start
#iterations: 100
currently lose_sum: 94.04682368040085
time_elpased: 1.77
batch start
#iterations: 101
currently lose_sum: 93.56499791145325
time_elpased: 1.828
batch start
#iterations: 102
currently lose_sum: 93.89447617530823
time_elpased: 1.77
batch start
#iterations: 103
currently lose_sum: 93.82949376106262
time_elpased: 1.77
batch start
#iterations: 104
currently lose_sum: 93.4580911397934
time_elpased: 1.777
batch start
#iterations: 105
currently lose_sum: 93.72637927532196
time_elpased: 1.784
batch start
#iterations: 106
currently lose_sum: 93.32487803697586
time_elpased: 1.826
batch start
#iterations: 107
currently lose_sum: 93.55252027511597
time_elpased: 1.785
batch start
#iterations: 108
currently lose_sum: 93.64569872617722
time_elpased: 1.828
batch start
#iterations: 109
currently lose_sum: 93.4362964630127
time_elpased: 1.78
batch start
#iterations: 110
currently lose_sum: 93.26822531223297
time_elpased: 1.821
batch start
#iterations: 111
currently lose_sum: 93.70945966243744
time_elpased: 1.814
batch start
#iterations: 112
currently lose_sum: 93.66875773668289
time_elpased: 1.809
batch start
#iterations: 113
currently lose_sum: 93.56969976425171
time_elpased: 1.827
batch start
#iterations: 114
currently lose_sum: 93.1990059018135
time_elpased: 1.786
batch start
#iterations: 115
currently lose_sum: 93.23308229446411
time_elpased: 1.792
batch start
#iterations: 116
currently lose_sum: 93.06923395395279
time_elpased: 1.806
batch start
#iterations: 117
currently lose_sum: 93.18562114238739
time_elpased: 1.792
batch start
#iterations: 118
currently lose_sum: 93.35301399230957
time_elpased: 1.803
batch start
#iterations: 119
currently lose_sum: 92.90743660926819
time_elpased: 1.773
start validation test
0.641701030928
0.640577353121
0.648554080478
0.644541038098
0.641688999342
61.477
batch start
#iterations: 120
currently lose_sum: 93.40688461065292
time_elpased: 1.785
batch start
#iterations: 121
currently lose_sum: 93.47217911481857
time_elpased: 1.792
batch start
#iterations: 122
currently lose_sum: 93.30010670423508
time_elpased: 1.781
batch start
#iterations: 123
currently lose_sum: 93.21177405118942
time_elpased: 1.789
batch start
#iterations: 124
currently lose_sum: 93.64522087574005
time_elpased: 1.798
batch start
#iterations: 125
currently lose_sum: 93.01189017295837
time_elpased: 1.789
batch start
#iterations: 126
currently lose_sum: 93.19503211975098
time_elpased: 1.791
batch start
#iterations: 127
currently lose_sum: 92.95974040031433
time_elpased: 1.761
batch start
#iterations: 128
currently lose_sum: 92.94021558761597
time_elpased: 1.789
batch start
#iterations: 129
currently lose_sum: 93.04480582475662
time_elpased: 1.772
batch start
#iterations: 130
currently lose_sum: 93.02570712566376
time_elpased: 1.793
batch start
#iterations: 131
currently lose_sum: 92.59923130273819
time_elpased: 1.804
batch start
#iterations: 132
currently lose_sum: 92.82494831085205
time_elpased: 1.806
batch start
#iterations: 133
currently lose_sum: 93.11596214771271
time_elpased: 1.782
batch start
#iterations: 134
currently lose_sum: 93.07156527042389
time_elpased: 1.79
batch start
#iterations: 135
currently lose_sum: 92.73892021179199
time_elpased: 1.788
batch start
#iterations: 136
currently lose_sum: 92.43998181819916
time_elpased: 1.785
batch start
#iterations: 137
currently lose_sum: 92.69866555929184
time_elpased: 1.793
batch start
#iterations: 138
currently lose_sum: 92.26074630022049
time_elpased: 1.756
batch start
#iterations: 139
currently lose_sum: 92.65993291139603
time_elpased: 1.758
start validation test
0.621134020619
0.606439428006
0.693938458372
0.64724515262
0.621006201199
62.369
batch start
#iterations: 140
currently lose_sum: 92.49303996562958
time_elpased: 1.757
batch start
#iterations: 141
currently lose_sum: 92.53340578079224
time_elpased: 1.822
batch start
#iterations: 142
currently lose_sum: 92.20849764347076
time_elpased: 1.811
batch start
#iterations: 143
currently lose_sum: 92.64740270376205
time_elpased: 1.788
batch start
#iterations: 144
currently lose_sum: 91.97314637899399
time_elpased: 1.78
batch start
#iterations: 145
currently lose_sum: 92.33407670259476
time_elpased: 1.791
batch start
#iterations: 146
currently lose_sum: 92.39648401737213
time_elpased: 1.777
batch start
#iterations: 147
currently lose_sum: 92.25808197259903
time_elpased: 1.769
batch start
#iterations: 148
currently lose_sum: 92.23083621263504
time_elpased: 1.779
batch start
#iterations: 149
currently lose_sum: 92.31107234954834
time_elpased: 1.774
batch start
#iterations: 150
currently lose_sum: 92.16463106870651
time_elpased: 1.779
batch start
#iterations: 151
currently lose_sum: 92.25658804178238
time_elpased: 1.767
batch start
#iterations: 152
currently lose_sum: 92.56564104557037
time_elpased: 1.772
batch start
#iterations: 153
currently lose_sum: 91.75137943029404
time_elpased: 1.769
batch start
#iterations: 154
currently lose_sum: 92.24351769685745
time_elpased: 1.755
batch start
#iterations: 155
currently lose_sum: 92.2820143699646
time_elpased: 1.761
batch start
#iterations: 156
currently lose_sum: 91.54191899299622
time_elpased: 1.794
batch start
#iterations: 157
currently lose_sum: 92.24717473983765
time_elpased: 1.77
batch start
#iterations: 158
currently lose_sum: 92.30314433574677
time_elpased: 1.843
batch start
#iterations: 159
currently lose_sum: 91.75708132982254
time_elpased: 1.771
start validation test
0.621701030928
0.628735383283
0.597612431821
0.612778979581
0.621743322179
62.038
batch start
#iterations: 160
currently lose_sum: 92.04056537151337
time_elpased: 1.849
batch start
#iterations: 161
currently lose_sum: 91.65445786714554
time_elpased: 1.82
batch start
#iterations: 162
currently lose_sum: 91.92333161830902
time_elpased: 1.772
batch start
#iterations: 163
currently lose_sum: 91.35202533006668
time_elpased: 1.773
batch start
#iterations: 164
currently lose_sum: 91.64799112081528
time_elpased: 1.804
batch start
#iterations: 165
currently lose_sum: 91.61550110578537
time_elpased: 1.799
batch start
#iterations: 166
currently lose_sum: 91.76504570245743
time_elpased: 1.739
batch start
#iterations: 167
currently lose_sum: 91.70602488517761
time_elpased: 1.776
batch start
#iterations: 168
currently lose_sum: 91.11954087018967
time_elpased: 1.761
batch start
#iterations: 169
currently lose_sum: 91.53561156988144
time_elpased: 1.811
batch start
#iterations: 170
currently lose_sum: 91.05801784992218
time_elpased: 1.811
batch start
#iterations: 171
currently lose_sum: 91.33844721317291
time_elpased: 1.797
batch start
#iterations: 172
currently lose_sum: 91.1516792178154
time_elpased: 1.824
batch start
#iterations: 173
currently lose_sum: 91.15068179368973
time_elpased: 1.782
batch start
#iterations: 174
currently lose_sum: 91.77164143323898
time_elpased: 1.802
batch start
#iterations: 175
currently lose_sum: 91.64268469810486
time_elpased: 1.794
batch start
#iterations: 176
currently lose_sum: 91.47379153966904
time_elpased: 1.756
batch start
#iterations: 177
currently lose_sum: 90.88814187049866
time_elpased: 1.753
batch start
#iterations: 178
currently lose_sum: 90.73051625490189
time_elpased: 1.788
batch start
#iterations: 179
currently lose_sum: 90.97121757268906
time_elpased: 1.763
start validation test
0.616597938144
0.615744032504
0.62385509931
0.619773029343
0.616585197079
62.793
batch start
#iterations: 180
currently lose_sum: 91.22002094984055
time_elpased: 1.778
batch start
#iterations: 181
currently lose_sum: 90.52461540699005
time_elpased: 1.751
batch start
#iterations: 182
currently lose_sum: 91.4041565656662
time_elpased: 1.778
batch start
#iterations: 183
currently lose_sum: 90.49918305873871
time_elpased: 1.768
batch start
#iterations: 184
currently lose_sum: 90.92341411113739
time_elpased: 1.802
batch start
#iterations: 185
currently lose_sum: 90.67361903190613
time_elpased: 1.79
batch start
#iterations: 186
currently lose_sum: 90.86638087034225
time_elpased: 1.779
batch start
#iterations: 187
currently lose_sum: 90.57207822799683
time_elpased: 1.767
batch start
#iterations: 188
currently lose_sum: 90.73929584026337
time_elpased: 1.812
batch start
#iterations: 189
currently lose_sum: 90.62139374017715
time_elpased: 1.787
batch start
#iterations: 190
currently lose_sum: 90.65311354398727
time_elpased: 1.797
batch start
#iterations: 191
currently lose_sum: 89.75284314155579
time_elpased: 1.814
batch start
#iterations: 192
currently lose_sum: 90.31909883022308
time_elpased: 1.849
batch start
#iterations: 193
currently lose_sum: 90.69282871484756
time_elpased: 1.813
batch start
#iterations: 194
currently lose_sum: 90.21761006116867
time_elpased: 1.78
batch start
#iterations: 195
currently lose_sum: 90.78146666288376
time_elpased: 1.761
batch start
#iterations: 196
currently lose_sum: 90.5330958366394
time_elpased: 1.796
batch start
#iterations: 197
currently lose_sum: 91.02325397729874
time_elpased: 1.818
batch start
#iterations: 198
currently lose_sum: 90.48180055618286
time_elpased: 1.808
batch start
#iterations: 199
currently lose_sum: 90.29047179222107
time_elpased: 1.816
start validation test
0.600257731959
0.625351392793
0.503653390964
0.557943339224
0.60042733578
63.584
batch start
#iterations: 200
currently lose_sum: 90.07941043376923
time_elpased: 1.791
batch start
#iterations: 201
currently lose_sum: 90.1969605088234
time_elpased: 1.805
batch start
#iterations: 202
currently lose_sum: 90.05243337154388
time_elpased: 1.81
batch start
#iterations: 203
currently lose_sum: 90.29408764839172
time_elpased: 1.787
batch start
#iterations: 204
currently lose_sum: 90.43777775764465
time_elpased: 1.785
batch start
#iterations: 205
currently lose_sum: 90.24424022436142
time_elpased: 1.764
batch start
#iterations: 206
currently lose_sum: 90.23479199409485
time_elpased: 1.812
batch start
#iterations: 207
currently lose_sum: 89.73726737499237
time_elpased: 1.814
batch start
#iterations: 208
currently lose_sum: 90.07076132297516
time_elpased: 1.826
batch start
#iterations: 209
currently lose_sum: 89.70252072811127
time_elpased: 1.817
batch start
#iterations: 210
currently lose_sum: 89.54420757293701
time_elpased: 1.775
batch start
#iterations: 211
currently lose_sum: 89.29670989513397
time_elpased: 1.792
batch start
#iterations: 212
currently lose_sum: 89.7663004398346
time_elpased: 1.8
batch start
#iterations: 213
currently lose_sum: 89.73440927267075
time_elpased: 1.811
batch start
#iterations: 214
currently lose_sum: 89.5544605255127
time_elpased: 1.806
batch start
#iterations: 215
currently lose_sum: 89.957515001297
time_elpased: 1.786
batch start
#iterations: 216
currently lose_sum: 89.70807141065598
time_elpased: 1.8
batch start
#iterations: 217
currently lose_sum: 89.52020359039307
time_elpased: 1.78
batch start
#iterations: 218
currently lose_sum: 89.74694746732712
time_elpased: 1.773
batch start
#iterations: 219
currently lose_sum: 89.08700895309448
time_elpased: 1.813
start validation test
0.603762886598
0.610134548611
0.578676546259
0.593989330798
0.603806929538
63.569
batch start
#iterations: 220
currently lose_sum: 89.84037357568741
time_elpased: 1.843
batch start
#iterations: 221
currently lose_sum: 89.57344859838486
time_elpased: 1.811
batch start
#iterations: 222
currently lose_sum: 89.43142092227936
time_elpased: 1.8
batch start
#iterations: 223
currently lose_sum: 89.22645497322083
time_elpased: 1.774
batch start
#iterations: 224
currently lose_sum: 89.4470643401146
time_elpased: 1.842
batch start
#iterations: 225
currently lose_sum: 89.72160631418228
time_elpased: 1.792
batch start
#iterations: 226
currently lose_sum: 89.22339516878128
time_elpased: 1.8
batch start
#iterations: 227
currently lose_sum: 89.12371128797531
time_elpased: 1.777
batch start
#iterations: 228
currently lose_sum: 89.22488284111023
time_elpased: 1.834
batch start
#iterations: 229
currently lose_sum: 88.81490635871887
time_elpased: 1.802
batch start
#iterations: 230
currently lose_sum: 88.59824430942535
time_elpased: 1.827
batch start
#iterations: 231
currently lose_sum: 89.31021225452423
time_elpased: 1.786
batch start
#iterations: 232
currently lose_sum: 89.31766015291214
time_elpased: 1.809
batch start
#iterations: 233
currently lose_sum: 89.38282978534698
time_elpased: 1.779
batch start
#iterations: 234
currently lose_sum: 89.20530849695206
time_elpased: 1.841
batch start
#iterations: 235
currently lose_sum: 89.08222687244415
time_elpased: 1.829
batch start
#iterations: 236
currently lose_sum: 89.33410066366196
time_elpased: 1.809
batch start
#iterations: 237
currently lose_sum: 88.55473113059998
time_elpased: 1.795
batch start
#iterations: 238
currently lose_sum: 89.01525354385376
time_elpased: 1.818
batch start
#iterations: 239
currently lose_sum: 88.72869795560837
time_elpased: 1.789
start validation test
0.607835051546
0.620776543351
0.557785324689
0.587597571552
0.607922921562
63.660
batch start
#iterations: 240
currently lose_sum: 89.04360747337341
time_elpased: 1.793
batch start
#iterations: 241
currently lose_sum: 89.03125178813934
time_elpased: 1.769
batch start
#iterations: 242
currently lose_sum: 88.56913566589355
time_elpased: 1.837
batch start
#iterations: 243
currently lose_sum: 88.83155816793442
time_elpased: 1.775
batch start
#iterations: 244
currently lose_sum: 88.52553486824036
time_elpased: 1.778
batch start
#iterations: 245
currently lose_sum: 88.27409607172012
time_elpased: 1.8
batch start
#iterations: 246
currently lose_sum: 88.69423347711563
time_elpased: 1.792
batch start
#iterations: 247
currently lose_sum: 88.78620493412018
time_elpased: 1.784
batch start
#iterations: 248
currently lose_sum: 88.68157368898392
time_elpased: 1.841
batch start
#iterations: 249
currently lose_sum: 87.7846029996872
time_elpased: 1.796
batch start
#iterations: 250
currently lose_sum: 88.85610729455948
time_elpased: 1.808
batch start
#iterations: 251
currently lose_sum: 88.02193075418472
time_elpased: 1.813
batch start
#iterations: 252
currently lose_sum: 88.22763478755951
time_elpased: 1.805
batch start
#iterations: 253
currently lose_sum: 88.36955291032791
time_elpased: 1.826
batch start
#iterations: 254
currently lose_sum: 88.15022760629654
time_elpased: 1.854
batch start
#iterations: 255
currently lose_sum: 88.51205110549927
time_elpased: 1.768
batch start
#iterations: 256
currently lose_sum: 88.45049548149109
time_elpased: 1.787
batch start
#iterations: 257
currently lose_sum: 88.29937332868576
time_elpased: 1.802
batch start
#iterations: 258
currently lose_sum: 88.27719086408615
time_elpased: 1.787
batch start
#iterations: 259
currently lose_sum: 88.30300176143646
time_elpased: 1.803
start validation test
0.599278350515
0.61652872736
0.52896984666
0.569402902404
0.599401787938
64.220
batch start
#iterations: 260
currently lose_sum: 87.47503411769867
time_elpased: 1.864
batch start
#iterations: 261
currently lose_sum: 87.95509093999863
time_elpased: 1.803
batch start
#iterations: 262
currently lose_sum: 87.86206996440887
time_elpased: 1.79
batch start
#iterations: 263
currently lose_sum: 87.49375516176224
time_elpased: 1.8
batch start
#iterations: 264
currently lose_sum: 88.36608171463013
time_elpased: 1.808
batch start
#iterations: 265
currently lose_sum: 87.50210958719254
time_elpased: 1.815
batch start
#iterations: 266
currently lose_sum: 87.80502504110336
time_elpased: 1.814
batch start
#iterations: 267
currently lose_sum: 88.48040121793747
time_elpased: 1.847
batch start
#iterations: 268
currently lose_sum: 87.95518296957016
time_elpased: 1.819
batch start
#iterations: 269
currently lose_sum: 87.05636072158813
time_elpased: 1.809
batch start
#iterations: 270
currently lose_sum: 87.98792988061905
time_elpased: 1.813
batch start
#iterations: 271
currently lose_sum: 87.77081042528152
time_elpased: 1.791
batch start
#iterations: 272
currently lose_sum: 87.08789294958115
time_elpased: 1.82
batch start
#iterations: 273
currently lose_sum: 87.69372177124023
time_elpased: 1.823
batch start
#iterations: 274
currently lose_sum: 87.61115998029709
time_elpased: 1.796
batch start
#iterations: 275
currently lose_sum: 87.70618319511414
time_elpased: 1.794
batch start
#iterations: 276
currently lose_sum: 87.18946349620819
time_elpased: 1.806
batch start
#iterations: 277
currently lose_sum: 88.05104166269302
time_elpased: 1.793
batch start
#iterations: 278
currently lose_sum: 87.34020256996155
time_elpased: 1.815
batch start
#iterations: 279
currently lose_sum: 87.59323006868362
time_elpased: 1.809
start validation test
0.599639175258
0.611428571429
0.550581455182
0.579411923972
0.599725303652
64.389
batch start
#iterations: 280
currently lose_sum: 86.96817624568939
time_elpased: 1.8
batch start
#iterations: 281
currently lose_sum: 87.1614408493042
time_elpased: 1.794
batch start
#iterations: 282
currently lose_sum: 87.3032836318016
time_elpased: 1.846
batch start
#iterations: 283
currently lose_sum: 87.21820586919785
time_elpased: 1.785
batch start
#iterations: 284
currently lose_sum: 87.27207839488983
time_elpased: 1.805
batch start
#iterations: 285
currently lose_sum: 87.10373276472092
time_elpased: 1.788
batch start
#iterations: 286
currently lose_sum: 87.29182147979736
time_elpased: 1.821
batch start
#iterations: 287
currently lose_sum: 86.81743323802948
time_elpased: 1.778
batch start
#iterations: 288
currently lose_sum: 86.51147264242172
time_elpased: 1.78
batch start
#iterations: 289
currently lose_sum: 86.23772859573364
time_elpased: 1.787
batch start
#iterations: 290
currently lose_sum: 86.32247966527939
time_elpased: 1.801
batch start
#iterations: 291
currently lose_sum: 86.47132003307343
time_elpased: 1.755
batch start
#iterations: 292
currently lose_sum: 86.4744553565979
time_elpased: 1.834
batch start
#iterations: 293
currently lose_sum: 86.42880147695541
time_elpased: 1.81
batch start
#iterations: 294
currently lose_sum: 86.61250299215317
time_elpased: 1.832
batch start
#iterations: 295
currently lose_sum: 86.83727926015854
time_elpased: 1.784
batch start
#iterations: 296
currently lose_sum: 86.6108540892601
time_elpased: 1.812
batch start
#iterations: 297
currently lose_sum: 86.17409187555313
time_elpased: 1.794
batch start
#iterations: 298
currently lose_sum: 86.77753204107285
time_elpased: 1.815
batch start
#iterations: 299
currently lose_sum: 86.6173364520073
time_elpased: 1.796
start validation test
0.602422680412
0.620810224259
0.529896058454
0.571761701183
0.602550012084
64.965
batch start
#iterations: 300
currently lose_sum: 87.08037424087524
time_elpased: 1.792
batch start
#iterations: 301
currently lose_sum: 86.53534638881683
time_elpased: 1.81
batch start
#iterations: 302
currently lose_sum: 86.72112184762955
time_elpased: 1.799
batch start
#iterations: 303
currently lose_sum: 86.97333949804306
time_elpased: 1.802
batch start
#iterations: 304
currently lose_sum: 86.4740394949913
time_elpased: 1.827
batch start
#iterations: 305
currently lose_sum: 86.09476602077484
time_elpased: 1.811
batch start
#iterations: 306
currently lose_sum: 86.41000658273697
time_elpased: 1.809
batch start
#iterations: 307
currently lose_sum: 85.81859946250916
time_elpased: 1.82
batch start
#iterations: 308
currently lose_sum: 85.9269026517868
time_elpased: 1.797
batch start
#iterations: 309
currently lose_sum: 86.2244553565979
time_elpased: 1.804
batch start
#iterations: 310
currently lose_sum: 86.12110584974289
time_elpased: 1.86
batch start
#iterations: 311
currently lose_sum: 85.6196221113205
time_elpased: 1.778
batch start
#iterations: 312
currently lose_sum: 86.02972054481506
time_elpased: 1.823
batch start
#iterations: 313
currently lose_sum: 85.85930782556534
time_elpased: 1.835
batch start
#iterations: 314
currently lose_sum: 86.85246866941452
time_elpased: 1.841
batch start
#iterations: 315
currently lose_sum: 86.25844889879227
time_elpased: 1.789
batch start
#iterations: 316
currently lose_sum: 85.79671555757523
time_elpased: 1.807
batch start
#iterations: 317
currently lose_sum: 86.21868896484375
time_elpased: 1.78
batch start
#iterations: 318
currently lose_sum: 86.18808400630951
time_elpased: 1.845
batch start
#iterations: 319
currently lose_sum: 85.22648823261261
time_elpased: 1.842
start validation test
0.59324742268
0.601988382484
0.554595039621
0.577320692056
0.593315282901
65.593
batch start
#iterations: 320
currently lose_sum: 86.65370374917984
time_elpased: 1.772
batch start
#iterations: 321
currently lose_sum: 86.47869396209717
time_elpased: 1.783
batch start
#iterations: 322
currently lose_sum: 85.89772403240204
time_elpased: 1.816
batch start
#iterations: 323
currently lose_sum: 85.99264913797379
time_elpased: 1.798
batch start
#iterations: 324
currently lose_sum: 86.10343676805496
time_elpased: 1.836
batch start
#iterations: 325
currently lose_sum: 85.36983853578568
time_elpased: 1.787
batch start
#iterations: 326
currently lose_sum: 84.86499869823456
time_elpased: 1.847
batch start
#iterations: 327
currently lose_sum: 85.59847021102905
time_elpased: 1.78
batch start
#iterations: 328
currently lose_sum: 85.20196694135666
time_elpased: 1.793
batch start
#iterations: 329
currently lose_sum: 85.4301894903183
time_elpased: 1.802
batch start
#iterations: 330
currently lose_sum: 85.60387045145035
time_elpased: 1.827
batch start
#iterations: 331
currently lose_sum: 85.82501816749573
time_elpased: 1.803
batch start
#iterations: 332
currently lose_sum: 85.3028067946434
time_elpased: 1.81
batch start
#iterations: 333
currently lose_sum: 84.91414484381676
time_elpased: 1.791
batch start
#iterations: 334
currently lose_sum: 85.31671613454819
time_elpased: 1.799
batch start
#iterations: 335
currently lose_sum: 85.57993549108505
time_elpased: 1.791
batch start
#iterations: 336
currently lose_sum: 85.32042968273163
time_elpased: 1.799
batch start
#iterations: 337
currently lose_sum: 85.48500418663025
time_elpased: 1.819
batch start
#iterations: 338
currently lose_sum: 85.26086747646332
time_elpased: 1.783
batch start
#iterations: 339
currently lose_sum: 85.09629124403
time_elpased: 1.794
start validation test
0.602268041237
0.617941765885
0.539466913656
0.576043956044
0.602378298303
65.357
batch start
#iterations: 340
currently lose_sum: 85.0303367972374
time_elpased: 1.843
batch start
#iterations: 341
currently lose_sum: 85.13577312231064
time_elpased: 1.817
batch start
#iterations: 342
currently lose_sum: 85.04771399497986
time_elpased: 1.824
batch start
#iterations: 343
currently lose_sum: 85.14408427476883
time_elpased: 1.809
batch start
#iterations: 344
currently lose_sum: 85.31418603658676
time_elpased: 1.797
batch start
#iterations: 345
currently lose_sum: 85.01878547668457
time_elpased: 1.794
batch start
#iterations: 346
currently lose_sum: 84.87667644023895
time_elpased: 1.822
batch start
#iterations: 347
currently lose_sum: 84.71208906173706
time_elpased: 1.809
batch start
#iterations: 348
currently lose_sum: 84.94890278577805
time_elpased: 1.806
batch start
#iterations: 349
currently lose_sum: 84.61615309119225
time_elpased: 1.786
batch start
#iterations: 350
currently lose_sum: 84.55034446716309
time_elpased: 1.833
batch start
#iterations: 351
currently lose_sum: 85.20392751693726
time_elpased: 1.789
batch start
#iterations: 352
currently lose_sum: 85.26503640413284
time_elpased: 1.844
batch start
#iterations: 353
currently lose_sum: 85.01013189554214
time_elpased: 1.818
batch start
#iterations: 354
currently lose_sum: 84.41309380531311
time_elpased: 1.791
batch start
#iterations: 355
currently lose_sum: 84.50966656208038
time_elpased: 1.81
batch start
#iterations: 356
currently lose_sum: 84.59486442804337
time_elpased: 1.82
batch start
#iterations: 357
currently lose_sum: 84.34202349185944
time_elpased: 1.79
batch start
#iterations: 358
currently lose_sum: 84.62044894695282
time_elpased: 1.833
batch start
#iterations: 359
currently lose_sum: 84.84287977218628
time_elpased: 1.789
start validation test
0.594381443299
0.619906566312
0.491612637645
0.548355621879
0.594561869788
66.483
batch start
#iterations: 360
currently lose_sum: 85.03566366434097
time_elpased: 1.79
batch start
#iterations: 361
currently lose_sum: 84.53256058692932
time_elpased: 1.783
batch start
#iterations: 362
currently lose_sum: 84.30186659097672
time_elpased: 1.817
batch start
#iterations: 363
currently lose_sum: 84.27770298719406
time_elpased: 1.816
batch start
#iterations: 364
currently lose_sum: 84.41959327459335
time_elpased: 1.811
batch start
#iterations: 365
currently lose_sum: 84.02654206752777
time_elpased: 1.807
batch start
#iterations: 366
currently lose_sum: 84.86660408973694
time_elpased: 1.79
batch start
#iterations: 367
currently lose_sum: 84.23593080043793
time_elpased: 1.788
batch start
#iterations: 368
currently lose_sum: 84.3824392259121
time_elpased: 1.821
batch start
#iterations: 369
currently lose_sum: 84.10467931628227
time_elpased: 1.829
batch start
#iterations: 370
currently lose_sum: 84.20129787921906
time_elpased: 1.845
batch start
#iterations: 371
currently lose_sum: 83.59970736503601
time_elpased: 1.822
batch start
#iterations: 372
currently lose_sum: 83.98825365304947
time_elpased: 1.807
batch start
#iterations: 373
currently lose_sum: 83.7529710829258
time_elpased: 1.784
batch start
#iterations: 374
currently lose_sum: 83.61930388212204
time_elpased: 1.773
batch start
#iterations: 375
currently lose_sum: 83.66253310441971
time_elpased: 1.832
batch start
#iterations: 376
currently lose_sum: 84.28814387321472
time_elpased: 1.835
batch start
#iterations: 377
currently lose_sum: 84.659478276968
time_elpased: 1.782
batch start
#iterations: 378
currently lose_sum: 83.78379935026169
time_elpased: 1.8
batch start
#iterations: 379
currently lose_sum: 84.03102946281433
time_elpased: 1.79
start validation test
0.598144329897
0.630166689253
0.478542760111
0.54398689752
0.5983543089
67.499
batch start
#iterations: 380
currently lose_sum: 83.37009006738663
time_elpased: 1.813
batch start
#iterations: 381
currently lose_sum: 84.26924294233322
time_elpased: 1.791
batch start
#iterations: 382
currently lose_sum: 83.47647607326508
time_elpased: 1.816
batch start
#iterations: 383
currently lose_sum: 83.58909511566162
time_elpased: 1.807
batch start
#iterations: 384
currently lose_sum: 84.22818011045456
time_elpased: 1.809
batch start
#iterations: 385
currently lose_sum: 83.55117440223694
time_elpased: 1.845
batch start
#iterations: 386
currently lose_sum: 83.54403907060623
time_elpased: 1.873
batch start
#iterations: 387
currently lose_sum: 84.22677630186081
time_elpased: 1.831
batch start
#iterations: 388
currently lose_sum: 83.80533701181412
time_elpased: 1.777
batch start
#iterations: 389
currently lose_sum: 83.71491339802742
time_elpased: 1.783
batch start
#iterations: 390
currently lose_sum: 83.70882177352905
time_elpased: 1.798
batch start
#iterations: 391
currently lose_sum: 83.34264415502548
time_elpased: 1.806
batch start
#iterations: 392
currently lose_sum: 83.34244403243065
time_elpased: 1.801
batch start
#iterations: 393
currently lose_sum: 83.79290467500687
time_elpased: 1.793
batch start
#iterations: 394
currently lose_sum: 82.94051831960678
time_elpased: 1.823
batch start
#iterations: 395
currently lose_sum: 83.64590156078339
time_elpased: 1.795
batch start
#iterations: 396
currently lose_sum: 83.21303510665894
time_elpased: 1.801
batch start
#iterations: 397
currently lose_sum: 83.54415655136108
time_elpased: 1.819
batch start
#iterations: 398
currently lose_sum: 82.82156497240067
time_elpased: 1.777
batch start
#iterations: 399
currently lose_sum: 83.93610805273056
time_elpased: 1.777
start validation test
0.584896907216
0.600192678227
0.512915508902
0.553132456578
0.585023281664
67.452
acc: 0.636
pre: 0.637
rec: 0.639
F1: 0.638
auc: 0.636
