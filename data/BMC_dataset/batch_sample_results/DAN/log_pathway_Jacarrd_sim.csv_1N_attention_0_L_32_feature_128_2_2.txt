start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.6123281121254
time_elpased: 1.787
batch start
#iterations: 1
currently lose_sum: 100.34575468301773
time_elpased: 1.792
batch start
#iterations: 2
currently lose_sum: 100.35893905162811
time_elpased: 1.769
batch start
#iterations: 3
currently lose_sum: 100.05981588363647
time_elpased: 1.752
batch start
#iterations: 4
currently lose_sum: 100.0474261045456
time_elpased: 1.742
batch start
#iterations: 5
currently lose_sum: 99.89433073997498
time_elpased: 1.787
batch start
#iterations: 6
currently lose_sum: 99.81548601388931
time_elpased: 1.789
batch start
#iterations: 7
currently lose_sum: 99.75324535369873
time_elpased: 1.771
batch start
#iterations: 8
currently lose_sum: 99.60071301460266
time_elpased: 1.747
batch start
#iterations: 9
currently lose_sum: 99.65869718790054
time_elpased: 1.75
batch start
#iterations: 10
currently lose_sum: 99.6054317355156
time_elpased: 1.782
batch start
#iterations: 11
currently lose_sum: 99.59806805849075
time_elpased: 1.778
batch start
#iterations: 12
currently lose_sum: 99.50383895635605
time_elpased: 1.779
batch start
#iterations: 13
currently lose_sum: 99.31859290599823
time_elpased: 1.79
batch start
#iterations: 14
currently lose_sum: 99.15933847427368
time_elpased: 1.786
batch start
#iterations: 15
currently lose_sum: 99.1064241528511
time_elpased: 1.776
batch start
#iterations: 16
currently lose_sum: 99.26211559772491
time_elpased: 1.751
batch start
#iterations: 17
currently lose_sum: 99.08463776111603
time_elpased: 1.765
batch start
#iterations: 18
currently lose_sum: 98.93106383085251
time_elpased: 1.766
batch start
#iterations: 19
currently lose_sum: 98.8534449338913
time_elpased: 1.758
start validation test
0.584484536082
0.619239631336
0.442523412576
0.51617549967
0.584733770731
65.051
batch start
#iterations: 20
currently lose_sum: 98.65753030776978
time_elpased: 1.8
batch start
#iterations: 21
currently lose_sum: 98.72665911912918
time_elpased: 1.775
batch start
#iterations: 22
currently lose_sum: 98.62206465005875
time_elpased: 1.77
batch start
#iterations: 23
currently lose_sum: 98.55520737171173
time_elpased: 1.779
batch start
#iterations: 24
currently lose_sum: 98.2400615811348
time_elpased: 1.772
batch start
#iterations: 25
currently lose_sum: 98.42566502094269
time_elpased: 1.781
batch start
#iterations: 26
currently lose_sum: 98.32072269916534
time_elpased: 1.834
batch start
#iterations: 27
currently lose_sum: 98.48175585269928
time_elpased: 1.811
batch start
#iterations: 28
currently lose_sum: 98.0934020280838
time_elpased: 1.769
batch start
#iterations: 29
currently lose_sum: 98.12927275896072
time_elpased: 1.769
batch start
#iterations: 30
currently lose_sum: 98.01240128278732
time_elpased: 1.788
batch start
#iterations: 31
currently lose_sum: 97.8743228316307
time_elpased: 1.767
batch start
#iterations: 32
currently lose_sum: 97.53536313772202
time_elpased: 1.786
batch start
#iterations: 33
currently lose_sum: 97.82872015237808
time_elpased: 1.788
batch start
#iterations: 34
currently lose_sum: 97.76498466730118
time_elpased: 1.803
batch start
#iterations: 35
currently lose_sum: 97.63335299491882
time_elpased: 1.852
batch start
#iterations: 36
currently lose_sum: 97.39874023199081
time_elpased: 1.783
batch start
#iterations: 37
currently lose_sum: 97.55434358119965
time_elpased: 1.812
batch start
#iterations: 38
currently lose_sum: 97.18738353252411
time_elpased: 1.793
batch start
#iterations: 39
currently lose_sum: 97.35410761833191
time_elpased: 1.756
start validation test
0.628917525773
0.6211275736
0.664402593393
0.642036696335
0.628855226264
63.015
batch start
#iterations: 40
currently lose_sum: 97.20709204673767
time_elpased: 1.77
batch start
#iterations: 41
currently lose_sum: 97.08829736709595
time_elpased: 1.738
batch start
#iterations: 42
currently lose_sum: 96.86930531263351
time_elpased: 1.776
batch start
#iterations: 43
currently lose_sum: 97.03524541854858
time_elpased: 1.768
batch start
#iterations: 44
currently lose_sum: 97.18441218137741
time_elpased: 1.775
batch start
#iterations: 45
currently lose_sum: 96.86507153511047
time_elpased: 1.763
batch start
#iterations: 46
currently lose_sum: 96.78002059459686
time_elpased: 1.779
batch start
#iterations: 47
currently lose_sum: 96.58785903453827
time_elpased: 1.787
batch start
#iterations: 48
currently lose_sum: 96.70803022384644
time_elpased: 1.786
batch start
#iterations: 49
currently lose_sum: 96.87723076343536
time_elpased: 1.767
batch start
#iterations: 50
currently lose_sum: 96.94035303592682
time_elpased: 1.753
batch start
#iterations: 51
currently lose_sum: 96.58808130025864
time_elpased: 1.773
batch start
#iterations: 52
currently lose_sum: 96.79775297641754
time_elpased: 1.777
batch start
#iterations: 53
currently lose_sum: 96.3097819685936
time_elpased: 1.767
batch start
#iterations: 54
currently lose_sum: 96.2596788406372
time_elpased: 1.776
batch start
#iterations: 55
currently lose_sum: 96.38785725831985
time_elpased: 1.761
batch start
#iterations: 56
currently lose_sum: 96.73100864887238
time_elpased: 1.785
batch start
#iterations: 57
currently lose_sum: 96.2296125292778
time_elpased: 1.763
batch start
#iterations: 58
currently lose_sum: 96.43356943130493
time_elpased: 1.734
batch start
#iterations: 59
currently lose_sum: 96.4110848903656
time_elpased: 1.767
start validation test
0.643144329897
0.625787862417
0.715241329628
0.667531095423
0.643017752494
61.871
batch start
#iterations: 60
currently lose_sum: 96.15539717674255
time_elpased: 1.751
batch start
#iterations: 61
currently lose_sum: 96.25991225242615
time_elpased: 1.805
batch start
#iterations: 62
currently lose_sum: 96.15838301181793
time_elpased: 1.816
batch start
#iterations: 63
currently lose_sum: 95.8782793879509
time_elpased: 1.755
batch start
#iterations: 64
currently lose_sum: 96.12905341386795
time_elpased: 1.764
batch start
#iterations: 65
currently lose_sum: 95.52874773740768
time_elpased: 1.772
batch start
#iterations: 66
currently lose_sum: 95.88495236635208
time_elpased: 1.825
batch start
#iterations: 67
currently lose_sum: 95.6478967666626
time_elpased: 1.794
batch start
#iterations: 68
currently lose_sum: 95.73171186447144
time_elpased: 1.758
batch start
#iterations: 69
currently lose_sum: 95.4491657614708
time_elpased: 1.772
batch start
#iterations: 70
currently lose_sum: 95.52869713306427
time_elpased: 1.751
batch start
#iterations: 71
currently lose_sum: 95.5756853222847
time_elpased: 1.78
batch start
#iterations: 72
currently lose_sum: 95.53456038236618
time_elpased: 1.82
batch start
#iterations: 73
currently lose_sum: 95.55746293067932
time_elpased: 1.77
batch start
#iterations: 74
currently lose_sum: 95.7008786201477
time_elpased: 1.771
batch start
#iterations: 75
currently lose_sum: 95.24848705530167
time_elpased: 1.791
batch start
#iterations: 76
currently lose_sum: 95.42605656385422
time_elpased: 1.784
batch start
#iterations: 77
currently lose_sum: 95.52420902252197
time_elpased: 1.787
batch start
#iterations: 78
currently lose_sum: 95.7691268324852
time_elpased: 1.771
batch start
#iterations: 79
currently lose_sum: 95.45229494571686
time_elpased: 1.773
start validation test
0.646958762887
0.634799774394
0.694967582587
0.66352247605
0.646874475999
61.303
batch start
#iterations: 80
currently lose_sum: 95.47623062133789
time_elpased: 1.751
batch start
#iterations: 81
currently lose_sum: 95.1872609257698
time_elpased: 1.768
batch start
#iterations: 82
currently lose_sum: 95.15593647956848
time_elpased: 1.797
batch start
#iterations: 83
currently lose_sum: 94.80332911014557
time_elpased: 1.779
batch start
#iterations: 84
currently lose_sum: 95.07493698596954
time_elpased: 1.776
batch start
#iterations: 85
currently lose_sum: 95.40905773639679
time_elpased: 1.769
batch start
#iterations: 86
currently lose_sum: 95.0166323184967
time_elpased: 1.784
batch start
#iterations: 87
currently lose_sum: 95.19450116157532
time_elpased: 1.785
batch start
#iterations: 88
currently lose_sum: 94.91979813575745
time_elpased: 1.801
batch start
#iterations: 89
currently lose_sum: 94.78142613172531
time_elpased: 1.823
batch start
#iterations: 90
currently lose_sum: 95.00004380941391
time_elpased: 1.757
batch start
#iterations: 91
currently lose_sum: 95.38201779127121
time_elpased: 1.759
batch start
#iterations: 92
currently lose_sum: 95.08346444368362
time_elpased: 1.771
batch start
#iterations: 93
currently lose_sum: 94.79695975780487
time_elpased: 1.775
batch start
#iterations: 94
currently lose_sum: 94.56270742416382
time_elpased: 1.78
batch start
#iterations: 95
currently lose_sum: 94.86599564552307
time_elpased: 1.783
batch start
#iterations: 96
currently lose_sum: 94.62071478366852
time_elpased: 1.794
batch start
#iterations: 97
currently lose_sum: 94.8221784234047
time_elpased: 1.758
batch start
#iterations: 98
currently lose_sum: 95.0137534737587
time_elpased: 1.784
batch start
#iterations: 99
currently lose_sum: 94.79346871376038
time_elpased: 1.761
start validation test
0.636030927835
0.636513157895
0.637233714109
0.636873232193
0.636028816158
61.810
batch start
#iterations: 100
currently lose_sum: 95.10404121875763
time_elpased: 1.784
batch start
#iterations: 101
currently lose_sum: 94.88129335641861
time_elpased: 1.841
batch start
#iterations: 102
currently lose_sum: 94.18584340810776
time_elpased: 1.785
batch start
#iterations: 103
currently lose_sum: 94.53835952281952
time_elpased: 1.77
batch start
#iterations: 104
currently lose_sum: 94.52580326795578
time_elpased: 1.77
batch start
#iterations: 105
currently lose_sum: 94.25710445642471
time_elpased: 1.784
batch start
#iterations: 106
currently lose_sum: 94.36939084529877
time_elpased: 1.796
batch start
#iterations: 107
currently lose_sum: 94.77102488279343
time_elpased: 1.759
batch start
#iterations: 108
currently lose_sum: 94.10139536857605
time_elpased: 1.771
batch start
#iterations: 109
currently lose_sum: 94.36406522989273
time_elpased: 1.756
batch start
#iterations: 110
currently lose_sum: 94.46390467882156
time_elpased: 1.798
batch start
#iterations: 111
currently lose_sum: 93.85370188951492
time_elpased: 1.762
batch start
#iterations: 112
currently lose_sum: 94.4462885260582
time_elpased: 1.805
batch start
#iterations: 113
currently lose_sum: 94.50664037466049
time_elpased: 1.773
batch start
#iterations: 114
currently lose_sum: 93.94310188293457
time_elpased: 1.761
batch start
#iterations: 115
currently lose_sum: 94.07895982265472
time_elpased: 1.803
batch start
#iterations: 116
currently lose_sum: 93.87477910518646
time_elpased: 1.773
batch start
#iterations: 117
currently lose_sum: 93.80585616827011
time_elpased: 1.793
batch start
#iterations: 118
currently lose_sum: 93.86423152685165
time_elpased: 1.807
batch start
#iterations: 119
currently lose_sum: 93.80058580636978
time_elpased: 1.773
start validation test
0.636546391753
0.615072513812
0.733251003396
0.66898267687
0.636376611891
61.626
batch start
#iterations: 120
currently lose_sum: 94.03116381168365
time_elpased: 1.784
batch start
#iterations: 121
currently lose_sum: 93.67734014987946
time_elpased: 1.797
batch start
#iterations: 122
currently lose_sum: 93.98568081855774
time_elpased: 1.803
batch start
#iterations: 123
currently lose_sum: 93.83727014064789
time_elpased: 1.784
batch start
#iterations: 124
currently lose_sum: 93.94649076461792
time_elpased: 1.765
batch start
#iterations: 125
currently lose_sum: 94.19922512769699
time_elpased: 1.746
batch start
#iterations: 126
currently lose_sum: 94.32343804836273
time_elpased: 1.776
batch start
#iterations: 127
currently lose_sum: 93.93453496694565
time_elpased: 1.761
batch start
#iterations: 128
currently lose_sum: 93.65453463792801
time_elpased: 1.776
batch start
#iterations: 129
currently lose_sum: 93.90393805503845
time_elpased: 1.758
batch start
#iterations: 130
currently lose_sum: 93.27628993988037
time_elpased: 1.747
batch start
#iterations: 131
currently lose_sum: 93.7692442536354
time_elpased: 1.802
batch start
#iterations: 132
currently lose_sum: 93.2608460187912
time_elpased: 1.763
batch start
#iterations: 133
currently lose_sum: 93.80951207876205
time_elpased: 1.799
batch start
#iterations: 134
currently lose_sum: 93.2023451924324
time_elpased: 1.787
batch start
#iterations: 135
currently lose_sum: 93.3469830751419
time_elpased: 1.783
batch start
#iterations: 136
currently lose_sum: 93.38711166381836
time_elpased: 1.779
batch start
#iterations: 137
currently lose_sum: 93.49756026268005
time_elpased: 1.774
batch start
#iterations: 138
currently lose_sum: 93.36847871541977
time_elpased: 1.84
batch start
#iterations: 139
currently lose_sum: 92.81204253435135
time_elpased: 1.752
start validation test
0.60793814433
0.604930907645
0.626222085006
0.615392394822
0.607906044052
63.015
batch start
#iterations: 140
currently lose_sum: 93.20551401376724
time_elpased: 1.773
batch start
#iterations: 141
currently lose_sum: 93.47283273935318
time_elpased: 1.755
batch start
#iterations: 142
currently lose_sum: 93.24723601341248
time_elpased: 1.78
batch start
#iterations: 143
currently lose_sum: 93.12397533655167
time_elpased: 1.744
batch start
#iterations: 144
currently lose_sum: 93.06925904750824
time_elpased: 1.762
batch start
#iterations: 145
currently lose_sum: 92.95117628574371
time_elpased: 1.813
batch start
#iterations: 146
currently lose_sum: 93.26078486442566
time_elpased: 1.751
batch start
#iterations: 147
currently lose_sum: 93.11616975069046
time_elpased: 1.789
batch start
#iterations: 148
currently lose_sum: 92.48808550834656
time_elpased: 1.781
batch start
#iterations: 149
currently lose_sum: 92.52032727003098
time_elpased: 1.806
batch start
#iterations: 150
currently lose_sum: 93.41667187213898
time_elpased: 1.771
batch start
#iterations: 151
currently lose_sum: 92.91047549247742
time_elpased: 1.809
batch start
#iterations: 152
currently lose_sum: 92.71911895275116
time_elpased: 1.822
batch start
#iterations: 153
currently lose_sum: 92.55689245462418
time_elpased: 1.731
batch start
#iterations: 154
currently lose_sum: 92.63383877277374
time_elpased: 1.811
batch start
#iterations: 155
currently lose_sum: 92.34868371486664
time_elpased: 1.78
batch start
#iterations: 156
currently lose_sum: 92.75047260522842
time_elpased: 1.8
batch start
#iterations: 157
currently lose_sum: 92.25688391923904
time_elpased: 1.789
batch start
#iterations: 158
currently lose_sum: 92.60955518484116
time_elpased: 1.807
batch start
#iterations: 159
currently lose_sum: 92.61048167943954
time_elpased: 1.795
start validation test
0.61618556701
0.63458575323
0.550993104868
0.589842458962
0.616300022433
62.510
batch start
#iterations: 160
currently lose_sum: 92.34108245372772
time_elpased: 1.846
batch start
#iterations: 161
currently lose_sum: 92.65038990974426
time_elpased: 1.778
batch start
#iterations: 162
currently lose_sum: 92.39936709403992
time_elpased: 1.789
batch start
#iterations: 163
currently lose_sum: 92.48493671417236
time_elpased: 1.784
batch start
#iterations: 164
currently lose_sum: 92.33116847276688
time_elpased: 1.802
batch start
#iterations: 165
currently lose_sum: 92.05996572971344
time_elpased: 1.774
batch start
#iterations: 166
currently lose_sum: 92.03238868713379
time_elpased: 1.832
batch start
#iterations: 167
currently lose_sum: 92.40220636129379
time_elpased: 1.786
batch start
#iterations: 168
currently lose_sum: 92.16843354701996
time_elpased: 1.808
batch start
#iterations: 169
currently lose_sum: 92.37943142652512
time_elpased: 1.74
batch start
#iterations: 170
currently lose_sum: 92.47405105829239
time_elpased: 1.806
batch start
#iterations: 171
currently lose_sum: 92.04776632785797
time_elpased: 1.787
batch start
#iterations: 172
currently lose_sum: 92.43650931119919
time_elpased: 1.788
batch start
#iterations: 173
currently lose_sum: 91.98693937063217
time_elpased: 1.742
batch start
#iterations: 174
currently lose_sum: 91.99799472093582
time_elpased: 1.783
batch start
#iterations: 175
currently lose_sum: 91.69535225629807
time_elpased: 1.757
batch start
#iterations: 176
currently lose_sum: 91.43331652879715
time_elpased: 1.766
batch start
#iterations: 177
currently lose_sum: 91.47213470935822
time_elpased: 1.753
batch start
#iterations: 178
currently lose_sum: 92.28597015142441
time_elpased: 1.768
batch start
#iterations: 179
currently lose_sum: 91.96611028909683
time_elpased: 1.75
start validation test
0.622525773196
0.630505887484
0.595142533704
0.612314045211
0.622573848696
62.083
batch start
#iterations: 180
currently lose_sum: 91.9149740934372
time_elpased: 1.774
batch start
#iterations: 181
currently lose_sum: 91.61706900596619
time_elpased: 1.774
batch start
#iterations: 182
currently lose_sum: 91.75415831804276
time_elpased: 1.792
batch start
#iterations: 183
currently lose_sum: 91.55277609825134
time_elpased: 1.758
batch start
#iterations: 184
currently lose_sum: 91.64701926708221
time_elpased: 1.854
batch start
#iterations: 185
currently lose_sum: 91.56257885694504
time_elpased: 1.759
batch start
#iterations: 186
currently lose_sum: 91.87728708982468
time_elpased: 1.789
batch start
#iterations: 187
currently lose_sum: 91.83169639110565
time_elpased: 1.767
batch start
#iterations: 188
currently lose_sum: 91.70486813783646
time_elpased: 1.773
batch start
#iterations: 189
currently lose_sum: 91.35835409164429
time_elpased: 1.746
batch start
#iterations: 190
currently lose_sum: 91.32185560464859
time_elpased: 1.786
batch start
#iterations: 191
currently lose_sum: 91.61984986066818
time_elpased: 1.785
batch start
#iterations: 192
currently lose_sum: 91.61344051361084
time_elpased: 1.804
batch start
#iterations: 193
currently lose_sum: 91.76241743564606
time_elpased: 1.792
batch start
#iterations: 194
currently lose_sum: 91.33318936824799
time_elpased: 1.832
batch start
#iterations: 195
currently lose_sum: 91.68105018138885
time_elpased: 1.795
batch start
#iterations: 196
currently lose_sum: 91.10508173704147
time_elpased: 1.809
batch start
#iterations: 197
currently lose_sum: 91.29312056303024
time_elpased: 1.755
batch start
#iterations: 198
currently lose_sum: 91.08895570039749
time_elpased: 1.838
batch start
#iterations: 199
currently lose_sum: 90.7589066028595
time_elpased: 1.767
start validation test
0.619072164948
0.644695933342
0.533497993208
0.583849532605
0.619222403606
62.751
batch start
#iterations: 200
currently lose_sum: 91.15805906057358
time_elpased: 1.776
batch start
#iterations: 201
currently lose_sum: 91.31408816576004
time_elpased: 1.801
batch start
#iterations: 202
currently lose_sum: 90.76745116710663
time_elpased: 1.805
batch start
#iterations: 203
currently lose_sum: 90.59167468547821
time_elpased: 1.8
batch start
#iterations: 204
currently lose_sum: 90.7411852478981
time_elpased: 1.803
batch start
#iterations: 205
currently lose_sum: 90.79578852653503
time_elpased: 1.783
batch start
#iterations: 206
currently lose_sum: 90.95688438415527
time_elpased: 1.793
batch start
#iterations: 207
currently lose_sum: 90.81980609893799
time_elpased: 1.752
batch start
#iterations: 208
currently lose_sum: 90.35604363679886
time_elpased: 1.778
batch start
#iterations: 209
currently lose_sum: 90.7869501709938
time_elpased: 1.764
batch start
#iterations: 210
currently lose_sum: 90.93020921945572
time_elpased: 1.8
batch start
#iterations: 211
currently lose_sum: 90.73627626895905
time_elpased: 1.822
batch start
#iterations: 212
currently lose_sum: 90.59568685293198
time_elpased: 1.772
batch start
#iterations: 213
currently lose_sum: 90.33533054590225
time_elpased: 1.774
batch start
#iterations: 214
currently lose_sum: 90.47688204050064
time_elpased: 1.757
batch start
#iterations: 215
currently lose_sum: 90.24675661325455
time_elpased: 1.769
batch start
#iterations: 216
currently lose_sum: 90.5391880273819
time_elpased: 1.775
batch start
#iterations: 217
currently lose_sum: 90.00019556283951
time_elpased: 1.783
batch start
#iterations: 218
currently lose_sum: 90.23139673471451
time_elpased: 1.771
batch start
#iterations: 219
currently lose_sum: 90.37818986177444
time_elpased: 1.784
start validation test
0.617783505155
0.621439122178
0.606154162807
0.613701484762
0.617803922259
62.707
batch start
#iterations: 220
currently lose_sum: 90.43558406829834
time_elpased: 1.808
batch start
#iterations: 221
currently lose_sum: 90.51558578014374
time_elpased: 1.749
batch start
#iterations: 222
currently lose_sum: 89.92178958654404
time_elpased: 1.842
batch start
#iterations: 223
currently lose_sum: 90.34435254335403
time_elpased: 1.773
batch start
#iterations: 224
currently lose_sum: 89.97636342048645
time_elpased: 1.796
batch start
#iterations: 225
currently lose_sum: 90.63404589891434
time_elpased: 1.76
batch start
#iterations: 226
currently lose_sum: 90.060577750206
time_elpased: 1.766
batch start
#iterations: 227
currently lose_sum: 89.6522267460823
time_elpased: 1.799
batch start
#iterations: 228
currently lose_sum: 90.2882981300354
time_elpased: 1.827
batch start
#iterations: 229
currently lose_sum: 89.91215932369232
time_elpased: 1.821
batch start
#iterations: 230
currently lose_sum: 90.23888283967972
time_elpased: 1.793
batch start
#iterations: 231
currently lose_sum: 89.90747892856598
time_elpased: 1.798
batch start
#iterations: 232
currently lose_sum: 90.11666810512543
time_elpased: 1.803
batch start
#iterations: 233
currently lose_sum: 89.67335647344589
time_elpased: 1.747
batch start
#iterations: 234
currently lose_sum: 89.69998377561569
time_elpased: 1.822
batch start
#iterations: 235
currently lose_sum: 89.92897737026215
time_elpased: 1.753
batch start
#iterations: 236
currently lose_sum: 89.46660804748535
time_elpased: 1.774
batch start
#iterations: 237
currently lose_sum: 89.47687768936157
time_elpased: 1.773
batch start
#iterations: 238
currently lose_sum: 89.65790778398514
time_elpased: 1.776
batch start
#iterations: 239
currently lose_sum: 90.15483438968658
time_elpased: 1.784
start validation test
0.603505154639
0.628148335654
0.51075434805
0.56340106709
0.603667992986
64.097
batch start
#iterations: 240
currently lose_sum: 89.28110545873642
time_elpased: 1.762
batch start
#iterations: 241
currently lose_sum: 89.4594806432724
time_elpased: 1.785
batch start
#iterations: 242
currently lose_sum: 89.4145855307579
time_elpased: 1.787
batch start
#iterations: 243
currently lose_sum: 89.10427302122116
time_elpased: 1.803
batch start
#iterations: 244
currently lose_sum: 89.6106972694397
time_elpased: 1.769
batch start
#iterations: 245
currently lose_sum: 88.99798423051834
time_elpased: 1.821
batch start
#iterations: 246
currently lose_sum: 88.91670602560043
time_elpased: 1.781
batch start
#iterations: 247
currently lose_sum: 89.01882016658783
time_elpased: 1.757
batch start
#iterations: 248
currently lose_sum: 88.93097019195557
time_elpased: 1.77
batch start
#iterations: 249
currently lose_sum: 89.4269791841507
time_elpased: 1.75
batch start
#iterations: 250
currently lose_sum: 88.90202367305756
time_elpased: 1.782
batch start
#iterations: 251
currently lose_sum: 89.12108355760574
time_elpased: 1.809
batch start
#iterations: 252
currently lose_sum: 89.13557785749435
time_elpased: 1.765
batch start
#iterations: 253
currently lose_sum: 88.6983733177185
time_elpased: 1.772
batch start
#iterations: 254
currently lose_sum: 88.92009335756302
time_elpased: 1.773
batch start
#iterations: 255
currently lose_sum: 88.89470225572586
time_elpased: 1.775
batch start
#iterations: 256
currently lose_sum: 89.04009145498276
time_elpased: 1.8
batch start
#iterations: 257
currently lose_sum: 88.47728729248047
time_elpased: 1.795
batch start
#iterations: 258
currently lose_sum: 88.65066754817963
time_elpased: 1.802
batch start
#iterations: 259
currently lose_sum: 88.67502510547638
time_elpased: 1.789
start validation test
0.612113402062
0.616794543905
0.595657095811
0.606041568504
0.612142293646
63.494
batch start
#iterations: 260
currently lose_sum: 88.69730013608932
time_elpased: 1.776
batch start
#iterations: 261
currently lose_sum: 89.09146046638489
time_elpased: 1.759
batch start
#iterations: 262
currently lose_sum: 88.33269429206848
time_elpased: 1.813
batch start
#iterations: 263
currently lose_sum: 89.01467996835709
time_elpased: 1.763
batch start
#iterations: 264
currently lose_sum: 88.43810945749283
time_elpased: 1.79
batch start
#iterations: 265
currently lose_sum: 88.43367087841034
time_elpased: 1.785
batch start
#iterations: 266
currently lose_sum: 88.58995240926743
time_elpased: 1.776
batch start
#iterations: 267
currently lose_sum: 88.49769282341003
time_elpased: 1.754
batch start
#iterations: 268
currently lose_sum: 88.42734658718109
time_elpased: 1.809
batch start
#iterations: 269
currently lose_sum: 88.58002310991287
time_elpased: 1.816
batch start
#iterations: 270
currently lose_sum: 88.34840720891953
time_elpased: 1.784
batch start
#iterations: 271
currently lose_sum: 88.06606501340866
time_elpased: 1.791
batch start
#iterations: 272
currently lose_sum: 87.91341322660446
time_elpased: 1.788
batch start
#iterations: 273
currently lose_sum: 88.22362154722214
time_elpased: 1.769
batch start
#iterations: 274
currently lose_sum: 88.04377484321594
time_elpased: 1.788
batch start
#iterations: 275
currently lose_sum: 88.75121879577637
time_elpased: 1.787
batch start
#iterations: 276
currently lose_sum: 87.79484623670578
time_elpased: 1.755
batch start
#iterations: 277
currently lose_sum: 87.68715369701385
time_elpased: 1.777
batch start
#iterations: 278
currently lose_sum: 88.09891086816788
time_elpased: 1.789
batch start
#iterations: 279
currently lose_sum: 87.73265612125397
time_elpased: 1.757
start validation test
0.599432989691
0.613218524552
0.542348461459
0.575610288897
0.599533210385
64.588
batch start
#iterations: 280
currently lose_sum: 88.42620080709457
time_elpased: 1.765
batch start
#iterations: 281
currently lose_sum: 88.3309296965599
time_elpased: 1.743
batch start
#iterations: 282
currently lose_sum: 87.35603404045105
time_elpased: 1.786
batch start
#iterations: 283
currently lose_sum: 88.21042066812515
time_elpased: 1.776
batch start
#iterations: 284
currently lose_sum: 87.46365225315094
time_elpased: 1.788
batch start
#iterations: 285
currently lose_sum: 87.35978835821152
time_elpased: 1.769
batch start
#iterations: 286
currently lose_sum: 87.80703729391098
time_elpased: 1.784
batch start
#iterations: 287
currently lose_sum: 87.56087160110474
time_elpased: 1.768
batch start
#iterations: 288
currently lose_sum: 87.80744582414627
time_elpased: 1.826
batch start
#iterations: 289
currently lose_sum: 87.75705289840698
time_elpased: 1.773
batch start
#iterations: 290
currently lose_sum: 87.36031663417816
time_elpased: 1.794
batch start
#iterations: 291
currently lose_sum: 87.58895993232727
time_elpased: 1.789
batch start
#iterations: 292
currently lose_sum: 87.26994448900223
time_elpased: 1.844
batch start
#iterations: 293
currently lose_sum: 87.29891794919968
time_elpased: 1.801
batch start
#iterations: 294
currently lose_sum: 87.49816340208054
time_elpased: 1.806
batch start
#iterations: 295
currently lose_sum: 87.3093051314354
time_elpased: 1.826
batch start
#iterations: 296
currently lose_sum: 87.79271519184113
time_elpased: 1.788
batch start
#iterations: 297
currently lose_sum: 87.434190928936
time_elpased: 1.769
batch start
#iterations: 298
currently lose_sum: 87.86310166120529
time_elpased: 1.804
batch start
#iterations: 299
currently lose_sum: 87.07390516996384
time_elpased: 1.753
start validation test
0.604793814433
0.636230728336
0.492641761861
0.555304216693
0.60499071466
64.750
batch start
#iterations: 300
currently lose_sum: 87.13590902090073
time_elpased: 1.82
batch start
#iterations: 301
currently lose_sum: 87.8078561425209
time_elpased: 1.765
batch start
#iterations: 302
currently lose_sum: 87.19379794597626
time_elpased: 1.806
batch start
#iterations: 303
currently lose_sum: 86.96937501430511
time_elpased: 1.775
batch start
#iterations: 304
currently lose_sum: 87.04215282201767
time_elpased: 1.798
batch start
#iterations: 305
currently lose_sum: 87.16356271505356
time_elpased: 1.759
batch start
#iterations: 306
currently lose_sum: 86.72142499685287
time_elpased: 1.786
batch start
#iterations: 307
currently lose_sum: 87.0797867178917
time_elpased: 1.809
batch start
#iterations: 308
currently lose_sum: 86.95359134674072
time_elpased: 1.813
batch start
#iterations: 309
currently lose_sum: 86.61549913883209
time_elpased: 1.773
batch start
#iterations: 310
currently lose_sum: 87.22220945358276
time_elpased: 1.809
batch start
#iterations: 311
currently lose_sum: 86.65285754203796
time_elpased: 1.813
batch start
#iterations: 312
currently lose_sum: 86.98206895589828
time_elpased: 1.803
batch start
#iterations: 313
currently lose_sum: 86.25696063041687
time_elpased: 1.766
batch start
#iterations: 314
currently lose_sum: 86.85560578107834
time_elpased: 1.782
batch start
#iterations: 315
currently lose_sum: 85.9964485168457
time_elpased: 1.771
batch start
#iterations: 316
currently lose_sum: 86.65976393222809
time_elpased: 1.756
batch start
#iterations: 317
currently lose_sum: 86.41228395700455
time_elpased: 1.77
batch start
#iterations: 318
currently lose_sum: 86.70862996578217
time_elpased: 1.81
batch start
#iterations: 319
currently lose_sum: 86.49453449249268
time_elpased: 1.745
start validation test
0.600824742268
0.615312682642
0.54173098693
0.576182136602
0.60092849047
65.412
batch start
#iterations: 320
currently lose_sum: 85.87623673677444
time_elpased: 1.77
batch start
#iterations: 321
currently lose_sum: 86.73565745353699
time_elpased: 1.789
batch start
#iterations: 322
currently lose_sum: 86.44444531202316
time_elpased: 1.76
batch start
#iterations: 323
currently lose_sum: 86.23251938819885
time_elpased: 1.782
batch start
#iterations: 324
currently lose_sum: 85.90691304206848
time_elpased: 1.753
batch start
#iterations: 325
currently lose_sum: 86.70550441741943
time_elpased: 1.782
batch start
#iterations: 326
currently lose_sum: 86.39979529380798
time_elpased: 1.77
batch start
#iterations: 327
currently lose_sum: 86.28542402386665
time_elpased: 1.771
batch start
#iterations: 328
currently lose_sum: 86.34800797700882
time_elpased: 1.757
batch start
#iterations: 329
currently lose_sum: 86.12330764532089
time_elpased: 1.777
batch start
#iterations: 330
currently lose_sum: 86.30839186906815
time_elpased: 1.799
batch start
#iterations: 331
currently lose_sum: 86.07461524009705
time_elpased: 1.778
batch start
#iterations: 332
currently lose_sum: 85.40559083223343
time_elpased: 1.777
batch start
#iterations: 333
currently lose_sum: 86.03827458620071
time_elpased: 1.789
batch start
#iterations: 334
currently lose_sum: 86.07442682981491
time_elpased: 1.826
batch start
#iterations: 335
currently lose_sum: 86.3434751033783
time_elpased: 1.803
batch start
#iterations: 336
currently lose_sum: 85.89833498001099
time_elpased: 1.821
batch start
#iterations: 337
currently lose_sum: 86.20861202478409
time_elpased: 1.811
batch start
#iterations: 338
currently lose_sum: 86.1185422539711
time_elpased: 1.779
batch start
#iterations: 339
currently lose_sum: 85.67843967676163
time_elpased: 1.779
start validation test
0.593556701031
0.619863909971
0.487496140784
0.545768765482
0.593742906703
65.853
batch start
#iterations: 340
currently lose_sum: 85.75845456123352
time_elpased: 1.795
batch start
#iterations: 341
currently lose_sum: 85.67165446281433
time_elpased: 1.78
batch start
#iterations: 342
currently lose_sum: 85.62500983476639
time_elpased: 1.826
batch start
#iterations: 343
currently lose_sum: 85.63206857442856
time_elpased: 1.8
batch start
#iterations: 344
currently lose_sum: 85.9927049279213
time_elpased: 1.823
batch start
#iterations: 345
currently lose_sum: 86.30206805467606
time_elpased: 1.782
batch start
#iterations: 346
currently lose_sum: 85.56663155555725
time_elpased: 1.77
batch start
#iterations: 347
currently lose_sum: 85.94647204875946
time_elpased: 1.813
batch start
#iterations: 348
currently lose_sum: 84.82057577371597
time_elpased: 1.781
batch start
#iterations: 349
currently lose_sum: 85.5661148428917
time_elpased: 1.748
batch start
#iterations: 350
currently lose_sum: 85.6141037940979
time_elpased: 1.823
batch start
#iterations: 351
currently lose_sum: 85.97787874937057
time_elpased: 1.783
batch start
#iterations: 352
currently lose_sum: 84.45750039815903
time_elpased: 1.807
batch start
#iterations: 353
currently lose_sum: 85.73714929819107
time_elpased: 1.763
batch start
#iterations: 354
currently lose_sum: 85.64383244514465
time_elpased: 1.754
batch start
#iterations: 355
currently lose_sum: 85.21941608190536
time_elpased: 1.809
batch start
#iterations: 356
currently lose_sum: 85.67312324047089
time_elpased: 1.769
batch start
#iterations: 357
currently lose_sum: 85.28501290082932
time_elpased: 1.767
batch start
#iterations: 358
currently lose_sum: 85.02748507261276
time_elpased: 1.8
batch start
#iterations: 359
currently lose_sum: 84.65246415138245
time_elpased: 1.793
start validation test
0.594896907216
0.61376438893
0.515797056705
0.560532349158
0.595035779204
66.578
batch start
#iterations: 360
currently lose_sum: 85.53965270519257
time_elpased: 1.815
batch start
#iterations: 361
currently lose_sum: 84.94610172510147
time_elpased: 1.763
batch start
#iterations: 362
currently lose_sum: 84.99984866380692
time_elpased: 1.783
batch start
#iterations: 363
currently lose_sum: 85.12719225883484
time_elpased: 1.746
batch start
#iterations: 364
currently lose_sum: 84.93140149116516
time_elpased: 1.767
batch start
#iterations: 365
currently lose_sum: 84.74937579035759
time_elpased: 1.733
batch start
#iterations: 366
currently lose_sum: 85.11833029985428
time_elpased: 1.773
batch start
#iterations: 367
currently lose_sum: 84.68725270032883
time_elpased: 1.84
batch start
#iterations: 368
currently lose_sum: 84.80880671739578
time_elpased: 1.876
batch start
#iterations: 369
currently lose_sum: 84.82211291790009
time_elpased: 1.779
batch start
#iterations: 370
currently lose_sum: 85.20721077919006
time_elpased: 1.824
batch start
#iterations: 371
currently lose_sum: 85.09284073114395
time_elpased: 1.789
batch start
#iterations: 372
currently lose_sum: 84.9854097366333
time_elpased: 1.815
batch start
#iterations: 373
currently lose_sum: 84.4449223279953
time_elpased: 1.796
batch start
#iterations: 374
currently lose_sum: 84.86264419555664
time_elpased: 1.803
batch start
#iterations: 375
currently lose_sum: 84.84174317121506
time_elpased: 1.824
batch start
#iterations: 376
currently lose_sum: 84.89168310165405
time_elpased: 1.862
batch start
#iterations: 377
currently lose_sum: 84.69737756252289
time_elpased: 1.785
batch start
#iterations: 378
currently lose_sum: 84.84388667345047
time_elpased: 1.802
batch start
#iterations: 379
currently lose_sum: 84.57504659891129
time_elpased: 1.781
start validation test
0.598917525773
0.615101070155
0.532365956571
0.570750813703
0.599034367318
66.637
batch start
#iterations: 380
currently lose_sum: 84.84682840108871
time_elpased: 1.783
batch start
#iterations: 381
currently lose_sum: 84.5929234623909
time_elpased: 1.818
batch start
#iterations: 382
currently lose_sum: 85.16434639692307
time_elpased: 1.804
batch start
#iterations: 383
currently lose_sum: 84.6035493016243
time_elpased: 1.786
batch start
#iterations: 384
currently lose_sum: 84.03435629606247
time_elpased: 1.806
batch start
#iterations: 385
currently lose_sum: 84.00167459249496
time_elpased: 1.797
batch start
#iterations: 386
currently lose_sum: 83.84896677732468
time_elpased: 1.809
batch start
#iterations: 387
currently lose_sum: 84.07368516921997
time_elpased: 1.804
batch start
#iterations: 388
currently lose_sum: 84.1973466873169
time_elpased: 1.806
batch start
#iterations: 389
currently lose_sum: 84.27841901779175
time_elpased: 1.763
batch start
#iterations: 390
currently lose_sum: 84.45045417547226
time_elpased: 1.768
batch start
#iterations: 391
currently lose_sum: 83.68757265806198
time_elpased: 1.778
batch start
#iterations: 392
currently lose_sum: 84.28935515880585
time_elpased: 1.79
batch start
#iterations: 393
currently lose_sum: 83.57824671268463
time_elpased: 1.775
batch start
#iterations: 394
currently lose_sum: 84.24999678134918
time_elpased: 1.806
batch start
#iterations: 395
currently lose_sum: 83.8094784617424
time_elpased: 1.773
batch start
#iterations: 396
currently lose_sum: 83.68123871088028
time_elpased: 1.784
batch start
#iterations: 397
currently lose_sum: 83.72237253189087
time_elpased: 1.761
batch start
#iterations: 398
currently lose_sum: 84.34191173315048
time_elpased: 1.79
batch start
#iterations: 399
currently lose_sum: 83.75224909186363
time_elpased: 1.778
start validation test
0.593195876289
0.615374889367
0.500874755583
0.552252354476
0.593357960256
67.419
acc: 0.641
pre: 0.629
rec: 0.687
F1: 0.657
auc: 0.641
