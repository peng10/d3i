start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.51090151071548
time_elpased: 2.325
batch start
#iterations: 1
currently lose_sum: 100.37060856819153
time_elpased: 2.325
batch start
#iterations: 2
currently lose_sum: 100.32175523042679
time_elpased: 2.332
batch start
#iterations: 3
currently lose_sum: 100.18229687213898
time_elpased: 2.288
batch start
#iterations: 4
currently lose_sum: 100.21332859992981
time_elpased: 2.297
batch start
#iterations: 5
currently lose_sum: 99.99383991956711
time_elpased: 2.304
batch start
#iterations: 6
currently lose_sum: 99.99863386154175
time_elpased: 2.305
batch start
#iterations: 7
currently lose_sum: 99.65592402219772
time_elpased: 2.264
batch start
#iterations: 8
currently lose_sum: 99.48493564128876
time_elpased: 2.246
batch start
#iterations: 9
currently lose_sum: 99.47343957424164
time_elpased: 2.271
batch start
#iterations: 10
currently lose_sum: 99.3153817653656
time_elpased: 2.278
batch start
#iterations: 11
currently lose_sum: 99.16030472517014
time_elpased: 2.29
batch start
#iterations: 12
currently lose_sum: 98.79094392061234
time_elpased: 2.282
batch start
#iterations: 13
currently lose_sum: 98.91459196805954
time_elpased: 2.315
batch start
#iterations: 14
currently lose_sum: 98.64700561761856
time_elpased: 2.307
batch start
#iterations: 15
currently lose_sum: 98.72560214996338
time_elpased: 2.291
batch start
#iterations: 16
currently lose_sum: 98.6524817943573
time_elpased: 2.269
batch start
#iterations: 17
currently lose_sum: 98.47217738628387
time_elpased: 2.293
batch start
#iterations: 18
currently lose_sum: 98.35094857215881
time_elpased: 2.287
batch start
#iterations: 19
currently lose_sum: 98.3651385307312
time_elpased: 2.261
start validation test
0.618350515464
0.594570283752
0.748276216939
0.662626446733
0.618122410855
63.974
batch start
#iterations: 20
currently lose_sum: 98.54871463775635
time_elpased: 2.285
batch start
#iterations: 21
currently lose_sum: 97.78620111942291
time_elpased: 2.23
batch start
#iterations: 22
currently lose_sum: 97.83719676733017
time_elpased: 2.273
batch start
#iterations: 23
currently lose_sum: 98.02190029621124
time_elpased: 2.269
batch start
#iterations: 24
currently lose_sum: 97.76062166690826
time_elpased: 2.272
batch start
#iterations: 25
currently lose_sum: 97.82497888803482
time_elpased: 2.289
batch start
#iterations: 26
currently lose_sum: 97.90254896879196
time_elpased: 2.304
batch start
#iterations: 27
currently lose_sum: 97.57381349802017
time_elpased: 2.29
batch start
#iterations: 28
currently lose_sum: 97.19609981775284
time_elpased: 2.271
batch start
#iterations: 29
currently lose_sum: 96.8052190542221
time_elpased: 2.274
batch start
#iterations: 30
currently lose_sum: 97.16474974155426
time_elpased: 2.262
batch start
#iterations: 31
currently lose_sum: 97.23240631818771
time_elpased: 2.347
batch start
#iterations: 32
currently lose_sum: 97.21687918901443
time_elpased: 2.286
batch start
#iterations: 33
currently lose_sum: 96.45209938287735
time_elpased: 2.297
batch start
#iterations: 34
currently lose_sum: 96.91413086652756
time_elpased: 2.3
batch start
#iterations: 35
currently lose_sum: 96.65939444303513
time_elpased: 2.255
batch start
#iterations: 36
currently lose_sum: 96.50814133882523
time_elpased: 2.266
batch start
#iterations: 37
currently lose_sum: 96.66707420349121
time_elpased: 2.3
batch start
#iterations: 38
currently lose_sum: 96.59480315446854
time_elpased: 2.264
batch start
#iterations: 39
currently lose_sum: 96.52697575092316
time_elpased: 2.299
start validation test
0.639072164948
0.612088184295
0.762889780797
0.679219351292
0.638854784026
62.416
batch start
#iterations: 40
currently lose_sum: 96.31606042385101
time_elpased: 2.283
batch start
#iterations: 41
currently lose_sum: 96.59170705080032
time_elpased: 2.3
batch start
#iterations: 42
currently lose_sum: 96.28326618671417
time_elpased: 2.284
batch start
#iterations: 43
currently lose_sum: 96.29631346464157
time_elpased: 2.261
batch start
#iterations: 44
currently lose_sum: 96.33275431394577
time_elpased: 2.301
batch start
#iterations: 45
currently lose_sum: 96.57908326387405
time_elpased: 2.276
batch start
#iterations: 46
currently lose_sum: 95.58678358793259
time_elpased: 2.275
batch start
#iterations: 47
currently lose_sum: 96.26444977521896
time_elpased: 2.29
batch start
#iterations: 48
currently lose_sum: 95.89065062999725
time_elpased: 2.262
batch start
#iterations: 49
currently lose_sum: 95.58300644159317
time_elpased: 2.29
batch start
#iterations: 50
currently lose_sum: 95.95929598808289
time_elpased: 2.302
batch start
#iterations: 51
currently lose_sum: 95.54402196407318
time_elpased: 2.313
batch start
#iterations: 52
currently lose_sum: 96.03356009721756
time_elpased: 2.278
batch start
#iterations: 53
currently lose_sum: 95.34801483154297
time_elpased: 2.277
batch start
#iterations: 54
currently lose_sum: 95.344990670681
time_elpased: 2.269
batch start
#iterations: 55
currently lose_sum: 95.24899280071259
time_elpased: 2.306
batch start
#iterations: 56
currently lose_sum: 95.70273357629776
time_elpased: 2.295
batch start
#iterations: 57
currently lose_sum: 95.48156476020813
time_elpased: 2.238
batch start
#iterations: 58
currently lose_sum: 95.47920560836792
time_elpased: 2.22
batch start
#iterations: 59
currently lose_sum: 95.94271236658096
time_elpased: 2.259
start validation test
0.627216494845
0.613047038486
0.693423896264
0.650762990149
0.62710025754
62.087
batch start
#iterations: 60
currently lose_sum: 95.54311752319336
time_elpased: 2.252
batch start
#iterations: 61
currently lose_sum: 95.43762707710266
time_elpased: 2.255
batch start
#iterations: 62
currently lose_sum: 95.50378650426865
time_elpased: 2.237
batch start
#iterations: 63
currently lose_sum: 95.07006788253784
time_elpased: 2.258
batch start
#iterations: 64
currently lose_sum: 95.06179654598236
time_elpased: 2.265
batch start
#iterations: 65
currently lose_sum: 95.412826359272
time_elpased: 2.257
batch start
#iterations: 66
currently lose_sum: 95.16398251056671
time_elpased: 2.277
batch start
#iterations: 67
currently lose_sum: 95.0546737909317
time_elpased: 2.263
batch start
#iterations: 68
currently lose_sum: 94.6557297706604
time_elpased: 2.305
batch start
#iterations: 69
currently lose_sum: 94.76833176612854
time_elpased: 2.28
batch start
#iterations: 70
currently lose_sum: 95.44976031780243
time_elpased: 2.237
batch start
#iterations: 71
currently lose_sum: 95.04944360256195
time_elpased: 2.293
batch start
#iterations: 72
currently lose_sum: 94.54588389396667
time_elpased: 2.239
batch start
#iterations: 73
currently lose_sum: 95.0930346250534
time_elpased: 2.284
batch start
#iterations: 74
currently lose_sum: 94.27860206365585
time_elpased: 2.224
batch start
#iterations: 75
currently lose_sum: 95.11866992712021
time_elpased: 2.246
batch start
#iterations: 76
currently lose_sum: 94.69191336631775
time_elpased: 2.253
batch start
#iterations: 77
currently lose_sum: 94.57658100128174
time_elpased: 2.254
batch start
#iterations: 78
currently lose_sum: 94.92099982500076
time_elpased: 2.304
batch start
#iterations: 79
currently lose_sum: 94.52577203512192
time_elpased: 2.347
start validation test
0.609381443299
0.608766398861
0.616033755274
0.612378516624
0.609369764139
62.876
batch start
#iterations: 80
currently lose_sum: 94.3159955739975
time_elpased: 2.226
batch start
#iterations: 81
currently lose_sum: 94.7320386171341
time_elpased: 2.271
batch start
#iterations: 82
currently lose_sum: 95.2825077176094
time_elpased: 2.288
batch start
#iterations: 83
currently lose_sum: 94.35430353879929
time_elpased: 2.299
batch start
#iterations: 84
currently lose_sum: 94.72380113601685
time_elpased: 2.311
batch start
#iterations: 85
currently lose_sum: 94.53060829639435
time_elpased: 2.291
batch start
#iterations: 86
currently lose_sum: 95.11267930269241
time_elpased: 2.327
batch start
#iterations: 87
currently lose_sum: 94.59594851732254
time_elpased: 2.275
batch start
#iterations: 88
currently lose_sum: 94.46621525287628
time_elpased: 2.28
batch start
#iterations: 89
currently lose_sum: 94.55071836709976
time_elpased: 2.253
batch start
#iterations: 90
currently lose_sum: 94.01319760084152
time_elpased: 2.321
batch start
#iterations: 91
currently lose_sum: 94.6953684091568
time_elpased: 2.315
batch start
#iterations: 92
currently lose_sum: 94.35618108510971
time_elpased: 2.293
batch start
#iterations: 93
currently lose_sum: 94.01786971092224
time_elpased: 2.246
batch start
#iterations: 94
currently lose_sum: 94.15559220314026
time_elpased: 2.243
batch start
#iterations: 95
currently lose_sum: 94.5493313074112
time_elpased: 2.244
batch start
#iterations: 96
currently lose_sum: 94.61780226230621
time_elpased: 2.27
batch start
#iterations: 97
currently lose_sum: 94.5003057718277
time_elpased: 2.306
batch start
#iterations: 98
currently lose_sum: 94.13217979669571
time_elpased: 2.263
batch start
#iterations: 99
currently lose_sum: 94.49858808517456
time_elpased: 2.236
start validation test
0.635412371134
0.642549061893
0.613255119893
0.627560423358
0.635451271606
61.515
batch start
#iterations: 100
currently lose_sum: 94.20773601531982
time_elpased: 2.274
batch start
#iterations: 101
currently lose_sum: 94.16166603565216
time_elpased: 2.259
batch start
#iterations: 102
currently lose_sum: 94.20010477304459
time_elpased: 2.265
batch start
#iterations: 103
currently lose_sum: 94.05786091089249
time_elpased: 2.263
batch start
#iterations: 104
currently lose_sum: 93.95669901371002
time_elpased: 2.308
batch start
#iterations: 105
currently lose_sum: 93.89574146270752
time_elpased: 2.296
batch start
#iterations: 106
currently lose_sum: 93.97229039669037
time_elpased: 2.293
batch start
#iterations: 107
currently lose_sum: 93.91201096773148
time_elpased: 2.259
batch start
#iterations: 108
currently lose_sum: 94.05855321884155
time_elpased: 2.248
batch start
#iterations: 109
currently lose_sum: 94.32883363962173
time_elpased: 2.279
batch start
#iterations: 110
currently lose_sum: 93.84619170427322
time_elpased: 2.308
batch start
#iterations: 111
currently lose_sum: 93.69372725486755
time_elpased: 2.277
batch start
#iterations: 112
currently lose_sum: 93.5212630033493
time_elpased: 2.288
batch start
#iterations: 113
currently lose_sum: 93.52756935358047
time_elpased: 2.284
batch start
#iterations: 114
currently lose_sum: 93.57074809074402
time_elpased: 2.302
batch start
#iterations: 115
currently lose_sum: 93.34092915058136
time_elpased: 2.285
batch start
#iterations: 116
currently lose_sum: 93.68435823917389
time_elpased: 2.297
batch start
#iterations: 117
currently lose_sum: 93.46504431962967
time_elpased: 2.25
batch start
#iterations: 118
currently lose_sum: 94.0733637213707
time_elpased: 2.323
batch start
#iterations: 119
currently lose_sum: 93.25005972385406
time_elpased: 2.291
start validation test
0.632216494845
0.619981412639
0.686528764022
0.651560287151
0.63212114128
61.853
batch start
#iterations: 120
currently lose_sum: 93.20117855072021
time_elpased: 2.255
batch start
#iterations: 121
currently lose_sum: 93.38435679674149
time_elpased: 2.243
batch start
#iterations: 122
currently lose_sum: 93.62897366285324
time_elpased: 2.303
batch start
#iterations: 123
currently lose_sum: 93.71579444408417
time_elpased: 2.306
batch start
#iterations: 124
currently lose_sum: 93.57733756303787
time_elpased: 2.32
batch start
#iterations: 125
currently lose_sum: 93.35495710372925
time_elpased: 2.282
batch start
#iterations: 126
currently lose_sum: 93.31521666049957
time_elpased: 2.272
batch start
#iterations: 127
currently lose_sum: 93.32697695493698
time_elpased: 2.289
batch start
#iterations: 128
currently lose_sum: 93.30807137489319
time_elpased: 2.268
batch start
#iterations: 129
currently lose_sum: 93.40249437093735
time_elpased: 2.289
batch start
#iterations: 130
currently lose_sum: 92.94002467393875
time_elpased: 2.32
batch start
#iterations: 131
currently lose_sum: 92.78339731693268
time_elpased: 2.288
batch start
#iterations: 132
currently lose_sum: 92.77898389101028
time_elpased: 2.262
batch start
#iterations: 133
currently lose_sum: 93.34224218130112
time_elpased: 2.315
batch start
#iterations: 134
currently lose_sum: 93.2546443939209
time_elpased: 2.315
batch start
#iterations: 135
currently lose_sum: 93.30105710029602
time_elpased: 2.26
batch start
#iterations: 136
currently lose_sum: 93.15166848897934
time_elpased: 2.339
batch start
#iterations: 137
currently lose_sum: 93.0227872133255
time_elpased: 2.294
batch start
#iterations: 138
currently lose_sum: 92.79391181468964
time_elpased: 2.237
batch start
#iterations: 139
currently lose_sum: 92.8834497332573
time_elpased: 2.279
start validation test
0.64175257732
0.629214532549
0.693320983843
0.659714061888
0.641662041028
61.090
batch start
#iterations: 140
currently lose_sum: 93.16021704673767
time_elpased: 2.272
batch start
#iterations: 141
currently lose_sum: 92.57297837734222
time_elpased: 2.306
batch start
#iterations: 142
currently lose_sum: 93.15011835098267
time_elpased: 2.245
batch start
#iterations: 143
currently lose_sum: 92.6411263346672
time_elpased: 2.335
batch start
#iterations: 144
currently lose_sum: 93.18759047985077
time_elpased: 2.278
batch start
#iterations: 145
currently lose_sum: 92.31186330318451
time_elpased: 2.273
batch start
#iterations: 146
currently lose_sum: 92.68395340442657
time_elpased: 2.253
batch start
#iterations: 147
currently lose_sum: 92.67231184244156
time_elpased: 2.256
batch start
#iterations: 148
currently lose_sum: 92.86856305599213
time_elpased: 2.282
batch start
#iterations: 149
currently lose_sum: 92.91132742166519
time_elpased: 2.256
batch start
#iterations: 150
currently lose_sum: 92.33821922540665
time_elpased: 2.294
batch start
#iterations: 151
currently lose_sum: 92.32015186548233
time_elpased: 2.306
batch start
#iterations: 152
currently lose_sum: 92.22558599710464
time_elpased: 2.245
batch start
#iterations: 153
currently lose_sum: 92.67523342370987
time_elpased: 2.277
batch start
#iterations: 154
currently lose_sum: 92.59405028820038
time_elpased: 2.298
batch start
#iterations: 155
currently lose_sum: 92.51167887449265
time_elpased: 2.347
batch start
#iterations: 156
currently lose_sum: 92.8229632973671
time_elpased: 2.287
batch start
#iterations: 157
currently lose_sum: 92.44772064685822
time_elpased: 2.281
batch start
#iterations: 158
currently lose_sum: 92.83735531568527
time_elpased: 2.309
batch start
#iterations: 159
currently lose_sum: 92.33499109745026
time_elpased: 2.228
start validation test
0.642525773196
0.642871815941
0.644128846352
0.643499717267
0.642522958754
61.349
batch start
#iterations: 160
currently lose_sum: 92.13179808855057
time_elpased: 2.261
batch start
#iterations: 161
currently lose_sum: 91.85641551017761
time_elpased: 2.264
batch start
#iterations: 162
currently lose_sum: 92.13041627407074
time_elpased: 2.298
batch start
#iterations: 163
currently lose_sum: 91.97668641805649
time_elpased: 2.284
batch start
#iterations: 164
currently lose_sum: 92.53795224428177
time_elpased: 2.293
batch start
#iterations: 165
currently lose_sum: 91.59012401103973
time_elpased: 2.291
batch start
#iterations: 166
currently lose_sum: 91.5341209769249
time_elpased: 2.305
batch start
#iterations: 167
currently lose_sum: 92.05179136991501
time_elpased: 2.325
batch start
#iterations: 168
currently lose_sum: 92.08558374643326
time_elpased: 2.275
batch start
#iterations: 169
currently lose_sum: 91.86872953176498
time_elpased: 2.292
batch start
#iterations: 170
currently lose_sum: 91.66903525590897
time_elpased: 2.277
batch start
#iterations: 171
currently lose_sum: 91.74525463581085
time_elpased: 2.348
batch start
#iterations: 172
currently lose_sum: 92.15981435775757
time_elpased: 2.331
batch start
#iterations: 173
currently lose_sum: 91.6150718331337
time_elpased: 2.304
batch start
#iterations: 174
currently lose_sum: 92.5262941122055
time_elpased: 2.222
batch start
#iterations: 175
currently lose_sum: 91.50671923160553
time_elpased: 2.316
batch start
#iterations: 176
currently lose_sum: 92.09338635206223
time_elpased: 2.245
batch start
#iterations: 177
currently lose_sum: 91.44307029247284
time_elpased: 2.297
batch start
#iterations: 178
currently lose_sum: 91.3945888876915
time_elpased: 2.301
batch start
#iterations: 179
currently lose_sum: 91.47214150428772
time_elpased: 2.322
start validation test
0.636237113402
0.622852392389
0.693938458372
0.656476658716
0.636135809791
61.542
batch start
#iterations: 180
currently lose_sum: 91.40518927574158
time_elpased: 2.329
batch start
#iterations: 181
currently lose_sum: 91.6510272026062
time_elpased: 2.29
batch start
#iterations: 182
currently lose_sum: 90.98366528749466
time_elpased: 2.258
batch start
#iterations: 183
currently lose_sum: 91.11746072769165
time_elpased: 2.255
batch start
#iterations: 184
currently lose_sum: 91.26498979330063
time_elpased: 2.264
batch start
#iterations: 185
currently lose_sum: 91.10633957386017
time_elpased: 2.277
batch start
#iterations: 186
currently lose_sum: 91.09917551279068
time_elpased: 2.262
batch start
#iterations: 187
currently lose_sum: 91.30002957582474
time_elpased: 2.285
batch start
#iterations: 188
currently lose_sum: 90.66305214166641
time_elpased: 2.268
batch start
#iterations: 189
currently lose_sum: 91.10334825515747
time_elpased: 2.296
batch start
#iterations: 190
currently lose_sum: 91.11728709936142
time_elpased: 2.278
batch start
#iterations: 191
currently lose_sum: 90.96317332983017
time_elpased: 2.254
batch start
#iterations: 192
currently lose_sum: 90.82326120138168
time_elpased: 2.325
batch start
#iterations: 193
currently lose_sum: 91.0211718082428
time_elpased: 2.292
batch start
#iterations: 194
currently lose_sum: 90.50609618425369
time_elpased: 2.269
batch start
#iterations: 195
currently lose_sum: 90.51668590307236
time_elpased: 2.245
batch start
#iterations: 196
currently lose_sum: 90.38794964551926
time_elpased: 2.274
batch start
#iterations: 197
currently lose_sum: 90.96127051115036
time_elpased: 2.288
batch start
#iterations: 198
currently lose_sum: 90.86026614904404
time_elpased: 2.34
batch start
#iterations: 199
currently lose_sum: 90.82688760757446
time_elpased: 2.286
start validation test
0.628144329897
0.62208565018
0.656272512092
0.638721955128
0.628094946535
62.725
batch start
#iterations: 200
currently lose_sum: 90.43708825111389
time_elpased: 2.282
batch start
#iterations: 201
currently lose_sum: 90.20342576503754
time_elpased: 2.272
batch start
#iterations: 202
currently lose_sum: 90.09725260734558
time_elpased: 2.309
batch start
#iterations: 203
currently lose_sum: 90.09946477413177
time_elpased: 2.301
batch start
#iterations: 204
currently lose_sum: 90.36822384595871
time_elpased: 2.283
batch start
#iterations: 205
currently lose_sum: 89.89792919158936
time_elpased: 2.269
batch start
#iterations: 206
currently lose_sum: 89.92986565828323
time_elpased: 2.318
batch start
#iterations: 207
currently lose_sum: 89.89764094352722
time_elpased: 2.302
batch start
#iterations: 208
currently lose_sum: 89.93287777900696
time_elpased: 2.291
batch start
#iterations: 209
currently lose_sum: 89.42071825265884
time_elpased: 2.263
batch start
#iterations: 210
currently lose_sum: 89.15870773792267
time_elpased: 2.291
batch start
#iterations: 211
currently lose_sum: 89.52329099178314
time_elpased: 2.292
batch start
#iterations: 212
currently lose_sum: 89.3028079867363
time_elpased: 2.317
batch start
#iterations: 213
currently lose_sum: 89.65775859355927
time_elpased: 2.315
batch start
#iterations: 214
currently lose_sum: 89.59298372268677
time_elpased: 2.338
batch start
#iterations: 215
currently lose_sum: 88.94973331689835
time_elpased: 2.261
batch start
#iterations: 216
currently lose_sum: 89.5165479183197
time_elpased: 2.26
batch start
#iterations: 217
currently lose_sum: 89.34836220741272
time_elpased: 2.27
batch start
#iterations: 218
currently lose_sum: 88.96070581674576
time_elpased: 2.33
batch start
#iterations: 219
currently lose_sum: 89.09133267402649
time_elpased: 2.294
start validation test
0.611546391753
0.616943699732
0.592055161058
0.604243251759
0.611580611614
63.059
batch start
#iterations: 220
currently lose_sum: 88.82558387517929
time_elpased: 2.231
batch start
#iterations: 221
currently lose_sum: 89.2582933306694
time_elpased: 2.308
batch start
#iterations: 222
currently lose_sum: 89.33371990919113
time_elpased: 2.262
batch start
#iterations: 223
currently lose_sum: 88.6687935590744
time_elpased: 2.348
batch start
#iterations: 224
currently lose_sum: 88.57361328601837
time_elpased: 2.293
batch start
#iterations: 225
currently lose_sum: 88.97106486558914
time_elpased: 2.267
batch start
#iterations: 226
currently lose_sum: 88.32025474309921
time_elpased: 2.299
batch start
#iterations: 227
currently lose_sum: 88.64302331209183
time_elpased: 2.309
batch start
#iterations: 228
currently lose_sum: 89.10570937395096
time_elpased: 2.307
batch start
#iterations: 229
currently lose_sum: 88.59387630224228
time_elpased: 2.259
batch start
#iterations: 230
currently lose_sum: 87.77921462059021
time_elpased: 2.269
batch start
#iterations: 231
currently lose_sum: 88.03930735588074
time_elpased: 2.304
batch start
#iterations: 232
currently lose_sum: 88.90728676319122
time_elpased: 2.293
batch start
#iterations: 233
currently lose_sum: 87.9444968700409
time_elpased: 2.299
batch start
#iterations: 234
currently lose_sum: 88.15977531671524
time_elpased: 2.232
batch start
#iterations: 235
currently lose_sum: 88.12650275230408
time_elpased: 2.348
batch start
#iterations: 236
currently lose_sum: 88.04014724493027
time_elpased: 2.299
batch start
#iterations: 237
currently lose_sum: 87.59632378816605
time_elpased: 2.292
batch start
#iterations: 238
currently lose_sum: 88.04758131504059
time_elpased: 2.255
batch start
#iterations: 239
currently lose_sum: 87.61207979917526
time_elpased: 2.295
start validation test
0.614278350515
0.608699883223
0.643717196666
0.625719001651
0.61422666608
63.955
batch start
#iterations: 240
currently lose_sum: 87.83786433935165
time_elpased: 2.248
batch start
#iterations: 241
currently lose_sum: 87.25513309240341
time_elpased: 2.291
batch start
#iterations: 242
currently lose_sum: 87.26491576433182
time_elpased: 2.265
batch start
#iterations: 243
currently lose_sum: 87.01496797800064
time_elpased: 2.326
batch start
#iterations: 244
currently lose_sum: 87.17055928707123
time_elpased: 2.3
batch start
#iterations: 245
currently lose_sum: 87.06512528657913
time_elpased: 2.245
batch start
#iterations: 246
currently lose_sum: 86.53902220726013
time_elpased: 2.264
batch start
#iterations: 247
currently lose_sum: 86.71383535861969
time_elpased: 2.268
batch start
#iterations: 248
currently lose_sum: 87.10385584831238
time_elpased: 2.265
batch start
#iterations: 249
currently lose_sum: 86.68272089958191
time_elpased: 2.29
batch start
#iterations: 250
currently lose_sum: 86.40420752763748
time_elpased: 2.248
batch start
#iterations: 251
currently lose_sum: 86.55445772409439
time_elpased: 2.223
batch start
#iterations: 252
currently lose_sum: 86.51779061555862
time_elpased: 2.264
batch start
#iterations: 253
currently lose_sum: 86.43718457221985
time_elpased: 2.305
batch start
#iterations: 254
currently lose_sum: 86.6719234585762
time_elpased: 2.238
batch start
#iterations: 255
currently lose_sum: 86.41996520757675
time_elpased: 2.376
batch start
#iterations: 256
currently lose_sum: 85.68864983320236
time_elpased: 2.241
batch start
#iterations: 257
currently lose_sum: 85.64749521017075
time_elpased: 2.269
batch start
#iterations: 258
currently lose_sum: 85.45191079378128
time_elpased: 2.26
batch start
#iterations: 259
currently lose_sum: 85.8969544172287
time_elpased: 2.275
start validation test
0.613092783505
0.642700400155
0.512400946794
0.57020155749
0.613269563555
66.077
batch start
#iterations: 260
currently lose_sum: 85.3437131345272
time_elpased: 2.263
batch start
#iterations: 261
currently lose_sum: 85.26161277294159
time_elpased: 2.25
batch start
#iterations: 262
currently lose_sum: 85.63935947418213
time_elpased: 2.314
batch start
#iterations: 263
currently lose_sum: 84.37256729602814
time_elpased: 2.269
batch start
#iterations: 264
currently lose_sum: 85.31703490018845
time_elpased: 2.307
batch start
#iterations: 265
currently lose_sum: 84.89679837226868
time_elpased: 2.268
batch start
#iterations: 266
currently lose_sum: 84.73571461439133
time_elpased: 2.252
batch start
#iterations: 267
currently lose_sum: 85.01221489906311
time_elpased: 2.235
batch start
#iterations: 268
currently lose_sum: 84.81523329019547
time_elpased: 2.319
batch start
#iterations: 269
currently lose_sum: 84.20730650424957
time_elpased: 2.261
batch start
#iterations: 270
currently lose_sum: 84.14198839664459
time_elpased: 2.262
batch start
#iterations: 271
currently lose_sum: 84.15907913446426
time_elpased: 2.292
batch start
#iterations: 272
currently lose_sum: 84.60924619436264
time_elpased: 2.294
batch start
#iterations: 273
currently lose_sum: 83.86666268110275
time_elpased: 2.278
batch start
#iterations: 274
currently lose_sum: 83.73242950439453
time_elpased: 2.271
batch start
#iterations: 275
currently lose_sum: 83.16742873191833
time_elpased: 2.297
batch start
#iterations: 276
currently lose_sum: 83.3752943277359
time_elpased: 2.286
batch start
#iterations: 277
currently lose_sum: 82.7403455376625
time_elpased: 2.305
batch start
#iterations: 278
currently lose_sum: 83.28165480494499
time_elpased: 2.268
batch start
#iterations: 279
currently lose_sum: 82.3592903316021
time_elpased: 2.266
start validation test
0.588711340206
0.625107975813
0.44684573428
0.521154654024
0.588960407159
72.187
batch start
#iterations: 280
currently lose_sum: 82.41832783818245
time_elpased: 2.227
batch start
#iterations: 281
currently lose_sum: 82.98767274618149
time_elpased: 2.286
batch start
#iterations: 282
currently lose_sum: 82.18401581048965
time_elpased: 2.295
batch start
#iterations: 283
currently lose_sum: 82.49644008278847
time_elpased: 2.289
batch start
#iterations: 284
currently lose_sum: 82.51301011443138
time_elpased: 2.239
batch start
#iterations: 285
currently lose_sum: 82.1756294965744
time_elpased: 2.295
batch start
#iterations: 286
currently lose_sum: 81.08769482374191
time_elpased: 2.267
batch start
#iterations: 287
currently lose_sum: 81.7911034822464
time_elpased: 2.258
batch start
#iterations: 288
currently lose_sum: 80.66577732563019
time_elpased: 2.266
batch start
#iterations: 289
currently lose_sum: 81.75304201245308
time_elpased: 2.233
batch start
#iterations: 290
currently lose_sum: 81.41751837730408
time_elpased: 2.286
batch start
#iterations: 291
currently lose_sum: 80.85431635379791
time_elpased: 2.287
batch start
#iterations: 292
currently lose_sum: 81.26082479953766
time_elpased: 2.333
batch start
#iterations: 293
currently lose_sum: 80.4402622282505
time_elpased: 2.287
batch start
#iterations: 294
currently lose_sum: 79.82224103808403
time_elpased: 2.26
batch start
#iterations: 295
currently lose_sum: 80.15950843691826
time_elpased: 2.296
batch start
#iterations: 296
currently lose_sum: 79.00027790665627
time_elpased: 2.261
batch start
#iterations: 297
currently lose_sum: 79.7757519185543
time_elpased: 2.242
batch start
#iterations: 298
currently lose_sum: 78.99927666783333
time_elpased: 2.269
batch start
#iterations: 299
currently lose_sum: 78.59745427966118
time_elpased: 2.289
start validation test
0.584690721649
0.625529340593
0.425645775445
0.506583379264
0.584969949584
77.626
batch start
#iterations: 300
currently lose_sum: 78.58010068535805
time_elpased: 2.231
batch start
#iterations: 301
currently lose_sum: 78.85565432906151
time_elpased: 2.284
batch start
#iterations: 302
currently lose_sum: 79.0145258307457
time_elpased: 2.309
batch start
#iterations: 303
currently lose_sum: 78.51859691739082
time_elpased: 2.287
batch start
#iterations: 304
currently lose_sum: 78.36056181788445
time_elpased: 2.309
batch start
#iterations: 305
currently lose_sum: 78.20124298334122
time_elpased: 2.333
batch start
#iterations: 306
currently lose_sum: 77.97990053892136
time_elpased: 2.218
batch start
#iterations: 307
currently lose_sum: 77.46741959452629
time_elpased: 2.284
batch start
#iterations: 308
currently lose_sum: 76.363101541996
time_elpased: 2.278
batch start
#iterations: 309
currently lose_sum: 76.87924006581306
time_elpased: 2.29
batch start
#iterations: 310
currently lose_sum: 76.7993882894516
time_elpased: 2.308
batch start
#iterations: 311
currently lose_sum: 76.58887302875519
time_elpased: 2.276
batch start
#iterations: 312
currently lose_sum: 76.74237322807312
time_elpased: 2.25
batch start
#iterations: 313
currently lose_sum: 75.61912232637405
time_elpased: 2.287
batch start
#iterations: 314
currently lose_sum: 75.84401616454124
time_elpased: 2.315
batch start
#iterations: 315
currently lose_sum: 76.43111526966095
time_elpased: 2.277
batch start
#iterations: 316
currently lose_sum: 75.75289964675903
time_elpased: 2.27
batch start
#iterations: 317
currently lose_sum: 76.19148549437523
time_elpased: 2.279
batch start
#iterations: 318
currently lose_sum: 75.72471952438354
time_elpased: 2.258
batch start
#iterations: 319
currently lose_sum: 74.52986234426498
time_elpased: 2.232
start validation test
0.57793814433
0.616415410385
0.416589482351
0.497175141243
0.578221416792
85.679
batch start
#iterations: 320
currently lose_sum: 74.76794847846031
time_elpased: 2.275
batch start
#iterations: 321
currently lose_sum: 75.1898600757122
time_elpased: 2.3
batch start
#iterations: 322
currently lose_sum: 74.40324985980988
time_elpased: 2.253
batch start
#iterations: 323
currently lose_sum: 74.7617684006691
time_elpased: 2.302
batch start
#iterations: 324
currently lose_sum: 73.7322733104229
time_elpased: 2.287
batch start
#iterations: 325
currently lose_sum: 73.23421230912209
time_elpased: 2.305
batch start
#iterations: 326
currently lose_sum: 73.71996024250984
time_elpased: 2.293
batch start
#iterations: 327
currently lose_sum: 73.15931937098503
time_elpased: 2.296
batch start
#iterations: 328
currently lose_sum: 73.5673777461052
time_elpased: 2.264
batch start
#iterations: 329
currently lose_sum: 73.09662035107613
time_elpased: 2.271
batch start
#iterations: 330
currently lose_sum: 72.20481351017952
time_elpased: 2.278
batch start
#iterations: 331
currently lose_sum: 72.4247156381607
time_elpased: 2.301
batch start
#iterations: 332
currently lose_sum: 72.1321604847908
time_elpased: 2.287
batch start
#iterations: 333
currently lose_sum: 71.1117792725563
time_elpased: 2.285
batch start
#iterations: 334
currently lose_sum: 71.06292235851288
time_elpased: 2.264
batch start
#iterations: 335
currently lose_sum: 71.09881919622421
time_elpased: 2.292
batch start
#iterations: 336
currently lose_sum: 71.33422580361366
time_elpased: 2.284
batch start
#iterations: 337
currently lose_sum: 70.79300481081009
time_elpased: 2.255
batch start
#iterations: 338
currently lose_sum: 70.93978381156921
time_elpased: 2.262
batch start
#iterations: 339
currently lose_sum: 69.69863721728325
time_elpased: 2.323
start validation test
0.572989690722
0.616447261498
0.390346814861
0.478008821676
0.573310348461
94.436
batch start
#iterations: 340
currently lose_sum: 70.18865609169006
time_elpased: 2.3
batch start
#iterations: 341
currently lose_sum: 70.25333562493324
time_elpased: 2.275
batch start
#iterations: 342
currently lose_sum: 70.21820855140686
time_elpased: 2.306
batch start
#iterations: 343
currently lose_sum: 69.2580156326294
time_elpased: 2.279
batch start
#iterations: 344
currently lose_sum: 69.51797696948051
time_elpased: 2.277
batch start
#iterations: 345
currently lose_sum: 68.72148203849792
time_elpased: 2.275
batch start
#iterations: 346
currently lose_sum: 67.29313579201698
time_elpased: 2.315
batch start
#iterations: 347
currently lose_sum: 68.34568279981613
time_elpased: 2.279
batch start
#iterations: 348
currently lose_sum: 67.9744902253151
time_elpased: 2.322
batch start
#iterations: 349
currently lose_sum: 68.12687736749649
time_elpased: 2.356
batch start
#iterations: 350
currently lose_sum: 67.59037590026855
time_elpased: 2.266
batch start
#iterations: 351
currently lose_sum: 66.64535927772522
time_elpased: 2.298
batch start
#iterations: 352
currently lose_sum: 66.87632611393929
time_elpased: 2.278
batch start
#iterations: 353
currently lose_sum: 66.40781739354134
time_elpased: 2.282
batch start
#iterations: 354
currently lose_sum: 66.71623185276985
time_elpased: 2.273
batch start
#iterations: 355
currently lose_sum: 65.47401341795921
time_elpased: 2.295
batch start
#iterations: 356
currently lose_sum: 65.39999288320541
time_elpased: 2.305
batch start
#iterations: 357
currently lose_sum: 65.36144962906837
time_elpased: 2.309
batch start
#iterations: 358
currently lose_sum: 66.22441282868385
time_elpased: 2.298
batch start
#iterations: 359
currently lose_sum: 64.36823639273643
time_elpased: 2.262
start validation test
0.55912371134
0.614341846758
0.321807142122
0.422367799014
0.559540357181
109.220
batch start
#iterations: 360
currently lose_sum: 65.43358245491982
time_elpased: 2.26
batch start
#iterations: 361
currently lose_sum: 65.17509958148003
time_elpased: 2.325
batch start
#iterations: 362
currently lose_sum: 65.20512780547142
time_elpased: 2.266
batch start
#iterations: 363
currently lose_sum: 64.66130667924881
time_elpased: 2.316
batch start
#iterations: 364
currently lose_sum: 64.21915155649185
time_elpased: 2.246
batch start
#iterations: 365
currently lose_sum: 64.02306681871414
time_elpased: 2.291
batch start
#iterations: 366
currently lose_sum: 64.18401429057121
time_elpased: 2.297
batch start
#iterations: 367
currently lose_sum: 63.134754061698914
time_elpased: 2.273
batch start
#iterations: 368
currently lose_sum: 63.02607265114784
time_elpased: 2.294
batch start
#iterations: 369
currently lose_sum: 63.791726022958755
time_elpased: 2.294
batch start
#iterations: 370
currently lose_sum: 63.08504629135132
time_elpased: 2.294
batch start
#iterations: 371
currently lose_sum: 62.67145326733589
time_elpased: 2.293
batch start
#iterations: 372
currently lose_sum: 62.38479882478714
time_elpased: 2.246
batch start
#iterations: 373
currently lose_sum: 62.22603848576546
time_elpased: 2.273
batch start
#iterations: 374
currently lose_sum: 62.47533771395683
time_elpased: 2.321
batch start
#iterations: 375
currently lose_sum: 61.71297699213028
time_elpased: 2.24
batch start
#iterations: 376
currently lose_sum: 61.54503804445267
time_elpased: 2.31
batch start
#iterations: 377
currently lose_sum: 61.70101135969162
time_elpased: 2.264
batch start
#iterations: 378
currently lose_sum: 61.75886833667755
time_elpased: 2.285
batch start
#iterations: 379
currently lose_sum: 60.394127547740936
time_elpased: 2.227
start validation test
0.558762886598
0.597637130802
0.364412884635
0.452755402122
0.559104098003
125.612
batch start
#iterations: 380
currently lose_sum: 60.44006249308586
time_elpased: 2.265
batch start
#iterations: 381
currently lose_sum: 59.927031457424164
time_elpased: 2.278
batch start
#iterations: 382
currently lose_sum: 61.13609990477562
time_elpased: 2.266
batch start
#iterations: 383
currently lose_sum: 59.850738167762756
time_elpased: 2.3
batch start
#iterations: 384
currently lose_sum: 59.71176999807358
time_elpased: 2.295
batch start
#iterations: 385
currently lose_sum: 58.99506884813309
time_elpased: 2.325
batch start
#iterations: 386
currently lose_sum: 59.94097450375557
time_elpased: 2.228
batch start
#iterations: 387
currently lose_sum: 58.93676897883415
time_elpased: 2.231
batch start
#iterations: 388
currently lose_sum: 59.87181740999222
time_elpased: 2.305
batch start
#iterations: 389
currently lose_sum: 59.271013647317886
time_elpased: 2.312
batch start
#iterations: 390
currently lose_sum: 58.55431976914406
time_elpased: 2.326
batch start
#iterations: 391
currently lose_sum: 58.0769844353199
time_elpased: 2.273
batch start
#iterations: 392
currently lose_sum: 58.965421706438065
time_elpased: 2.249
batch start
#iterations: 393
currently lose_sum: 57.44839811325073
time_elpased: 2.308
batch start
#iterations: 394
currently lose_sum: 57.87938943505287
time_elpased: 2.281
batch start
#iterations: 395
currently lose_sum: 58.368088126182556
time_elpased: 2.296
batch start
#iterations: 396
currently lose_sum: 57.840564876794815
time_elpased: 2.269
batch start
#iterations: 397
currently lose_sum: 58.72181576490402
time_elpased: 2.294
batch start
#iterations: 398
currently lose_sum: 57.91923367977142
time_elpased: 2.287
batch start
#iterations: 399
currently lose_sum: 56.27180537581444
time_elpased: 2.295
start validation test
0.549896907216
0.588119520487
0.338273129567
0.429504769371
0.550268445399
147.587
acc: 0.636
pre: 0.624
rec: 0.687
F1: 0.654
auc: 0.636
