start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.66596627235413
time_elpased: 1.417
batch start
#iterations: 1
currently lose_sum: 100.22859537601471
time_elpased: 1.39
batch start
#iterations: 2
currently lose_sum: 100.16361039876938
time_elpased: 1.413
batch start
#iterations: 3
currently lose_sum: 100.01696008443832
time_elpased: 1.386
batch start
#iterations: 4
currently lose_sum: 99.87173134088516
time_elpased: 1.377
batch start
#iterations: 5
currently lose_sum: 99.82143604755402
time_elpased: 1.408
batch start
#iterations: 6
currently lose_sum: 99.71157139539719
time_elpased: 1.424
batch start
#iterations: 7
currently lose_sum: 99.53408229351044
time_elpased: 1.406
batch start
#iterations: 8
currently lose_sum: 99.50064247846603
time_elpased: 1.379
batch start
#iterations: 9
currently lose_sum: 99.61489099264145
time_elpased: 1.409
batch start
#iterations: 10
currently lose_sum: 99.46709871292114
time_elpased: 1.369
batch start
#iterations: 11
currently lose_sum: 99.25173634290695
time_elpased: 1.392
batch start
#iterations: 12
currently lose_sum: 99.31820380687714
time_elpased: 1.378
batch start
#iterations: 13
currently lose_sum: 99.3304033279419
time_elpased: 1.392
batch start
#iterations: 14
currently lose_sum: 99.16343480348587
time_elpased: 1.372
batch start
#iterations: 15
currently lose_sum: 99.13486295938492
time_elpased: 1.42
batch start
#iterations: 16
currently lose_sum: 99.05353903770447
time_elpased: 1.354
batch start
#iterations: 17
currently lose_sum: 98.97391879558563
time_elpased: 1.4
batch start
#iterations: 18
currently lose_sum: 98.77898889780045
time_elpased: 1.394
batch start
#iterations: 19
currently lose_sum: 98.72952276468277
time_elpased: 1.397
start validation test
0.594742268041
0.616271781371
0.505917464238
0.555668588222
0.594898213685
64.815
batch start
#iterations: 20
currently lose_sum: 98.52053034305573
time_elpased: 1.401
batch start
#iterations: 21
currently lose_sum: 98.50397056341171
time_elpased: 1.451
batch start
#iterations: 22
currently lose_sum: 98.64901828765869
time_elpased: 1.39
batch start
#iterations: 23
currently lose_sum: 98.56095880270004
time_elpased: 1.401
batch start
#iterations: 24
currently lose_sum: 98.40652638673782
time_elpased: 1.385
batch start
#iterations: 25
currently lose_sum: 98.13417267799377
time_elpased: 1.386
batch start
#iterations: 26
currently lose_sum: 98.00600630044937
time_elpased: 1.381
batch start
#iterations: 27
currently lose_sum: 97.9040225148201
time_elpased: 1.421
batch start
#iterations: 28
currently lose_sum: 97.80355083942413
time_elpased: 1.425
batch start
#iterations: 29
currently lose_sum: 98.02575129270554
time_elpased: 1.388
batch start
#iterations: 30
currently lose_sum: 97.4296281337738
time_elpased: 1.409
batch start
#iterations: 31
currently lose_sum: 97.3550996184349
time_elpased: 1.398
batch start
#iterations: 32
currently lose_sum: 97.46995383501053
time_elpased: 1.383
batch start
#iterations: 33
currently lose_sum: 97.27326279878616
time_elpased: 1.388
batch start
#iterations: 34
currently lose_sum: 97.29011982679367
time_elpased: 1.389
batch start
#iterations: 35
currently lose_sum: 97.27594608068466
time_elpased: 1.405
batch start
#iterations: 36
currently lose_sum: 97.4966191649437
time_elpased: 1.377
batch start
#iterations: 37
currently lose_sum: 96.93143999576569
time_elpased: 1.392
batch start
#iterations: 38
currently lose_sum: 97.12522155046463
time_elpased: 1.368
batch start
#iterations: 39
currently lose_sum: 96.81584334373474
time_elpased: 1.379
start validation test
0.634896907216
0.614422241529
0.727796645055
0.666321194705
0.634733807398
62.856
batch start
#iterations: 40
currently lose_sum: 96.82908922433853
time_elpased: 1.396
batch start
#iterations: 41
currently lose_sum: 97.0625330209732
time_elpased: 1.417
batch start
#iterations: 42
currently lose_sum: 96.73964285850525
time_elpased: 1.394
batch start
#iterations: 43
currently lose_sum: 96.64721494913101
time_elpased: 1.397
batch start
#iterations: 44
currently lose_sum: 96.4804584980011
time_elpased: 1.389
batch start
#iterations: 45
currently lose_sum: 96.17531287670135
time_elpased: 1.392
batch start
#iterations: 46
currently lose_sum: 96.24962717294693
time_elpased: 1.389
batch start
#iterations: 47
currently lose_sum: 96.38557147979736
time_elpased: 1.392
batch start
#iterations: 48
currently lose_sum: 96.47727626562119
time_elpased: 1.376
batch start
#iterations: 49
currently lose_sum: 96.24194848537445
time_elpased: 1.421
batch start
#iterations: 50
currently lose_sum: 96.66946083307266
time_elpased: 1.381
batch start
#iterations: 51
currently lose_sum: 95.96890300512314
time_elpased: 1.432
batch start
#iterations: 52
currently lose_sum: 95.97154861688614
time_elpased: 1.363
batch start
#iterations: 53
currently lose_sum: 95.96855473518372
time_elpased: 1.379
batch start
#iterations: 54
currently lose_sum: 95.94027602672577
time_elpased: 1.347
batch start
#iterations: 55
currently lose_sum: 95.80046486854553
time_elpased: 1.377
batch start
#iterations: 56
currently lose_sum: 95.99687141180038
time_elpased: 1.372
batch start
#iterations: 57
currently lose_sum: 95.52668380737305
time_elpased: 1.353
batch start
#iterations: 58
currently lose_sum: 95.92109614610672
time_elpased: 1.38
batch start
#iterations: 59
currently lose_sum: 95.66116964817047
time_elpased: 1.385
start validation test
0.640927835052
0.622670115045
0.718534527117
0.667176301959
0.640791584534
62.086
batch start
#iterations: 60
currently lose_sum: 95.85969823598862
time_elpased: 1.37
batch start
#iterations: 61
currently lose_sum: 95.70749700069427
time_elpased: 1.366
batch start
#iterations: 62
currently lose_sum: 95.69784080982208
time_elpased: 1.374
batch start
#iterations: 63
currently lose_sum: 96.11087000370026
time_elpased: 1.395
batch start
#iterations: 64
currently lose_sum: 95.8677346110344
time_elpased: 1.396
batch start
#iterations: 65
currently lose_sum: 95.57287442684174
time_elpased: 1.417
batch start
#iterations: 66
currently lose_sum: 95.14227110147476
time_elpased: 1.392
batch start
#iterations: 67
currently lose_sum: 95.29827964305878
time_elpased: 1.376
batch start
#iterations: 68
currently lose_sum: 95.51903802156448
time_elpased: 1.392
batch start
#iterations: 69
currently lose_sum: 95.5906873345375
time_elpased: 1.414
batch start
#iterations: 70
currently lose_sum: 95.13183099031448
time_elpased: 1.397
batch start
#iterations: 71
currently lose_sum: 95.17383396625519
time_elpased: 1.358
batch start
#iterations: 72
currently lose_sum: 95.14237707853317
time_elpased: 1.438
batch start
#iterations: 73
currently lose_sum: 94.81054902076721
time_elpased: 1.376
batch start
#iterations: 74
currently lose_sum: 94.90089684724808
time_elpased: 1.406
batch start
#iterations: 75
currently lose_sum: 94.78484189510345
time_elpased: 1.402
batch start
#iterations: 76
currently lose_sum: 94.87326920032501
time_elpased: 1.407
batch start
#iterations: 77
currently lose_sum: 95.27270549535751
time_elpased: 1.401
batch start
#iterations: 78
currently lose_sum: 95.1061162352562
time_elpased: 1.394
batch start
#iterations: 79
currently lose_sum: 95.14092963933945
time_elpased: 1.388
start validation test
0.622783505155
0.613043068514
0.669445302048
0.640003935458
0.622701583173
62.729
batch start
#iterations: 80
currently lose_sum: 94.76161825656891
time_elpased: 1.448
batch start
#iterations: 81
currently lose_sum: 95.51485687494278
time_elpased: 1.378
batch start
#iterations: 82
currently lose_sum: 94.66111040115356
time_elpased: 1.393
batch start
#iterations: 83
currently lose_sum: 95.14907348155975
time_elpased: 1.388
batch start
#iterations: 84
currently lose_sum: 95.10175389051437
time_elpased: 1.444
batch start
#iterations: 85
currently lose_sum: 95.4376653432846
time_elpased: 1.377
batch start
#iterations: 86
currently lose_sum: 94.73203772306442
time_elpased: 1.399
batch start
#iterations: 87
currently lose_sum: 95.01140236854553
time_elpased: 1.42
batch start
#iterations: 88
currently lose_sum: 94.93888932466507
time_elpased: 1.411
batch start
#iterations: 89
currently lose_sum: 94.8009215593338
time_elpased: 1.404
batch start
#iterations: 90
currently lose_sum: 94.98652857542038
time_elpased: 1.418
batch start
#iterations: 91
currently lose_sum: 94.66415697336197
time_elpased: 1.418
batch start
#iterations: 92
currently lose_sum: 94.62055438756943
time_elpased: 1.426
batch start
#iterations: 93
currently lose_sum: 94.55730682611465
time_elpased: 1.409
batch start
#iterations: 94
currently lose_sum: 94.75898325443268
time_elpased: 1.411
batch start
#iterations: 95
currently lose_sum: 94.78878265619278
time_elpased: 1.41
batch start
#iterations: 96
currently lose_sum: 94.59027564525604
time_elpased: 1.43
batch start
#iterations: 97
currently lose_sum: 94.95256918668747
time_elpased: 1.427
batch start
#iterations: 98
currently lose_sum: 94.81680250167847
time_elpased: 1.413
batch start
#iterations: 99
currently lose_sum: 94.4772737622261
time_elpased: 1.442
start validation test
0.635412371134
0.628774595753
0.664299680971
0.646049141771
0.635361655006
61.598
batch start
#iterations: 100
currently lose_sum: 94.50993931293488
time_elpased: 1.437
batch start
#iterations: 101
currently lose_sum: 94.62686932086945
time_elpased: 1.423
batch start
#iterations: 102
currently lose_sum: 94.70685893297195
time_elpased: 1.454
batch start
#iterations: 103
currently lose_sum: 94.52124124765396
time_elpased: 1.428
batch start
#iterations: 104
currently lose_sum: 94.43294644355774
time_elpased: 1.401
batch start
#iterations: 105
currently lose_sum: 94.1968737244606
time_elpased: 1.468
batch start
#iterations: 106
currently lose_sum: 94.38658690452576
time_elpased: 1.395
batch start
#iterations: 107
currently lose_sum: 94.35109728574753
time_elpased: 1.409
batch start
#iterations: 108
currently lose_sum: 93.9984735250473
time_elpased: 1.393
batch start
#iterations: 109
currently lose_sum: 94.2036800980568
time_elpased: 1.464
batch start
#iterations: 110
currently lose_sum: 94.79208165407181
time_elpased: 1.392
batch start
#iterations: 111
currently lose_sum: 94.51100373268127
time_elpased: 1.409
batch start
#iterations: 112
currently lose_sum: 94.23313444852829
time_elpased: 1.41
batch start
#iterations: 113
currently lose_sum: 94.03663402795792
time_elpased: 1.475
batch start
#iterations: 114
currently lose_sum: 94.65104579925537
time_elpased: 1.422
batch start
#iterations: 115
currently lose_sum: 94.05398696660995
time_elpased: 1.4
batch start
#iterations: 116
currently lose_sum: 94.27975088357925
time_elpased: 1.436
batch start
#iterations: 117
currently lose_sum: 94.43218857049942
time_elpased: 1.396
batch start
#iterations: 118
currently lose_sum: 94.23479354381561
time_elpased: 1.419
batch start
#iterations: 119
currently lose_sum: 94.0623106956482
time_elpased: 1.418
start validation test
0.632989690722
0.622719969757
0.678089945456
0.649226524781
0.632910510268
61.632
batch start
#iterations: 120
currently lose_sum: 94.47836887836456
time_elpased: 1.458
batch start
#iterations: 121
currently lose_sum: 94.37603747844696
time_elpased: 1.421
batch start
#iterations: 122
currently lose_sum: 94.00875586271286
time_elpased: 1.393
batch start
#iterations: 123
currently lose_sum: 94.05731451511383
time_elpased: 1.428
batch start
#iterations: 124
currently lose_sum: 94.57064294815063
time_elpased: 1.412
batch start
#iterations: 125
currently lose_sum: 94.39712232351303
time_elpased: 1.421
batch start
#iterations: 126
currently lose_sum: 94.4517514705658
time_elpased: 1.402
batch start
#iterations: 127
currently lose_sum: 94.26672887802124
time_elpased: 1.453
batch start
#iterations: 128
currently lose_sum: 94.25602889060974
time_elpased: 1.438
batch start
#iterations: 129
currently lose_sum: 94.14458173513412
time_elpased: 1.447
batch start
#iterations: 130
currently lose_sum: 93.9855192899704
time_elpased: 1.395
batch start
#iterations: 131
currently lose_sum: 94.12358552217484
time_elpased: 1.402
batch start
#iterations: 132
currently lose_sum: 94.15473765134811
time_elpased: 1.428
batch start
#iterations: 133
currently lose_sum: 94.23157835006714
time_elpased: 1.422
batch start
#iterations: 134
currently lose_sum: 93.92694050073624
time_elpased: 1.409
batch start
#iterations: 135
currently lose_sum: 94.54262322187424
time_elpased: 1.417
batch start
#iterations: 136
currently lose_sum: 94.16131418943405
time_elpased: 1.387
batch start
#iterations: 137
currently lose_sum: 93.67543631792068
time_elpased: 1.395
batch start
#iterations: 138
currently lose_sum: 93.68375849723816
time_elpased: 1.417
batch start
#iterations: 139
currently lose_sum: 94.1033650636673
time_elpased: 1.42
start validation test
0.631804123711
0.625904910976
0.658433672944
0.641757359948
0.63175737143
61.721
batch start
#iterations: 140
currently lose_sum: 94.22115445137024
time_elpased: 1.413
batch start
#iterations: 141
currently lose_sum: 94.2411282658577
time_elpased: 1.412
batch start
#iterations: 142
currently lose_sum: 93.63356512784958
time_elpased: 1.435
batch start
#iterations: 143
currently lose_sum: 94.29015552997589
time_elpased: 1.385
batch start
#iterations: 144
currently lose_sum: 93.45588260889053
time_elpased: 1.44
batch start
#iterations: 145
currently lose_sum: 94.02932232618332
time_elpased: 1.408
batch start
#iterations: 146
currently lose_sum: 93.87318450212479
time_elpased: 1.408
batch start
#iterations: 147
currently lose_sum: 94.05096417665482
time_elpased: 1.455
batch start
#iterations: 148
currently lose_sum: 93.87884056568146
time_elpased: 1.421
batch start
#iterations: 149
currently lose_sum: 94.33562469482422
time_elpased: 1.384
batch start
#iterations: 150
currently lose_sum: 93.65379917621613
time_elpased: 1.426
batch start
#iterations: 151
currently lose_sum: 94.31610548496246
time_elpased: 1.411
batch start
#iterations: 152
currently lose_sum: 93.69049072265625
time_elpased: 1.403
batch start
#iterations: 153
currently lose_sum: 94.08379054069519
time_elpased: 1.414
batch start
#iterations: 154
currently lose_sum: 93.83531892299652
time_elpased: 1.414
batch start
#iterations: 155
currently lose_sum: 94.20762830972672
time_elpased: 1.436
batch start
#iterations: 156
currently lose_sum: 93.2843661904335
time_elpased: 1.405
batch start
#iterations: 157
currently lose_sum: 93.49448710680008
time_elpased: 1.449
batch start
#iterations: 158
currently lose_sum: 94.01516908407211
time_elpased: 1.391
batch start
#iterations: 159
currently lose_sum: 93.98236870765686
time_elpased: 1.406
start validation test
0.64
0.629146583499
0.68508799012
0.655926692285
0.639920841079
61.206
batch start
#iterations: 160
currently lose_sum: 94.07857191562653
time_elpased: 1.44
batch start
#iterations: 161
currently lose_sum: 93.66400575637817
time_elpased: 1.406
batch start
#iterations: 162
currently lose_sum: 93.22143924236298
time_elpased: 1.413
batch start
#iterations: 163
currently lose_sum: 93.6516124010086
time_elpased: 1.445
batch start
#iterations: 164
currently lose_sum: 93.87095880508423
time_elpased: 1.448
batch start
#iterations: 165
currently lose_sum: 93.77151137590408
time_elpased: 1.429
batch start
#iterations: 166
currently lose_sum: 93.86930412054062
time_elpased: 1.409
batch start
#iterations: 167
currently lose_sum: 93.62481790781021
time_elpased: 1.444
batch start
#iterations: 168
currently lose_sum: 93.39630663394928
time_elpased: 1.409
batch start
#iterations: 169
currently lose_sum: 93.64749956130981
time_elpased: 1.436
batch start
#iterations: 170
currently lose_sum: 93.46145009994507
time_elpased: 1.408
batch start
#iterations: 171
currently lose_sum: 93.67324250936508
time_elpased: 1.433
batch start
#iterations: 172
currently lose_sum: 93.41216176748276
time_elpased: 1.414
batch start
#iterations: 173
currently lose_sum: 93.43808776140213
time_elpased: 1.412
batch start
#iterations: 174
currently lose_sum: 93.34872299432755
time_elpased: 1.455
batch start
#iterations: 175
currently lose_sum: 93.70988345146179
time_elpased: 1.401
batch start
#iterations: 176
currently lose_sum: 93.82539826631546
time_elpased: 1.42
batch start
#iterations: 177
currently lose_sum: 93.72141605615616
time_elpased: 1.421
batch start
#iterations: 178
currently lose_sum: 93.51975578069687
time_elpased: 1.418
batch start
#iterations: 179
currently lose_sum: 93.26239424943924
time_elpased: 1.43
start validation test
0.639278350515
0.628097616131
0.686014201914
0.655779636006
0.63919629852
61.348
batch start
#iterations: 180
currently lose_sum: 93.26586788892746
time_elpased: 1.399
batch start
#iterations: 181
currently lose_sum: 93.20676320791245
time_elpased: 1.399
batch start
#iterations: 182
currently lose_sum: 93.93157458305359
time_elpased: 1.417
batch start
#iterations: 183
currently lose_sum: 93.18448096513748
time_elpased: 1.37
batch start
#iterations: 184
currently lose_sum: 93.4682776927948
time_elpased: 1.424
batch start
#iterations: 185
currently lose_sum: 93.10471391677856
time_elpased: 1.419
batch start
#iterations: 186
currently lose_sum: 93.07394659519196
time_elpased: 1.426
batch start
#iterations: 187
currently lose_sum: 93.34090894460678
time_elpased: 1.392
batch start
#iterations: 188
currently lose_sum: 93.33078223466873
time_elpased: 1.428
batch start
#iterations: 189
currently lose_sum: 92.99419212341309
time_elpased: 1.422
batch start
#iterations: 190
currently lose_sum: 93.26995074748993
time_elpased: 1.4
batch start
#iterations: 191
currently lose_sum: 92.86562532186508
time_elpased: 1.394
batch start
#iterations: 192
currently lose_sum: 93.22846883535385
time_elpased: 1.421
batch start
#iterations: 193
currently lose_sum: 93.09695327281952
time_elpased: 1.414
batch start
#iterations: 194
currently lose_sum: 93.15750539302826
time_elpased: 1.405
batch start
#iterations: 195
currently lose_sum: 93.36360055208206
time_elpased: 1.414
batch start
#iterations: 196
currently lose_sum: 93.00260835886002
time_elpased: 1.424
batch start
#iterations: 197
currently lose_sum: 93.13518750667572
time_elpased: 1.434
batch start
#iterations: 198
currently lose_sum: 93.45680403709412
time_elpased: 1.404
batch start
#iterations: 199
currently lose_sum: 93.58432364463806
time_elpased: 1.403
start validation test
0.630309278351
0.639482626329
0.600391067202
0.61932059448
0.630361804385
61.511
batch start
#iterations: 200
currently lose_sum: 93.59951031208038
time_elpased: 1.434
batch start
#iterations: 201
currently lose_sum: 92.66943752765656
time_elpased: 1.406
batch start
#iterations: 202
currently lose_sum: 92.93038058280945
time_elpased: 1.445
batch start
#iterations: 203
currently lose_sum: 93.2456830739975
time_elpased: 1.418
batch start
#iterations: 204
currently lose_sum: 93.1859141588211
time_elpased: 1.428
batch start
#iterations: 205
currently lose_sum: 92.9919468164444
time_elpased: 1.409
batch start
#iterations: 206
currently lose_sum: 93.15220820903778
time_elpased: 1.414
batch start
#iterations: 207
currently lose_sum: 93.0092014670372
time_elpased: 1.438
batch start
#iterations: 208
currently lose_sum: 92.8313860297203
time_elpased: 1.431
batch start
#iterations: 209
currently lose_sum: 92.960757791996
time_elpased: 1.429
batch start
#iterations: 210
currently lose_sum: 93.27221101522446
time_elpased: 1.392
batch start
#iterations: 211
currently lose_sum: 92.99422812461853
time_elpased: 1.43
batch start
#iterations: 212
currently lose_sum: 92.71186172962189
time_elpased: 1.429
batch start
#iterations: 213
currently lose_sum: 92.47881889343262
time_elpased: 1.407
batch start
#iterations: 214
currently lose_sum: 92.50030142068863
time_elpased: 1.463
batch start
#iterations: 215
currently lose_sum: 92.96072095632553
time_elpased: 1.417
batch start
#iterations: 216
currently lose_sum: 93.02459704875946
time_elpased: 1.424
batch start
#iterations: 217
currently lose_sum: 92.73198425769806
time_elpased: 1.422
batch start
#iterations: 218
currently lose_sum: 92.99222892522812
time_elpased: 1.468
batch start
#iterations: 219
currently lose_sum: 93.00841397047043
time_elpased: 1.41
start validation test
0.635670103093
0.643857934181
0.610064834826
0.626506024096
0.635715057091
61.377
batch start
#iterations: 220
currently lose_sum: 93.12787711620331
time_elpased: 1.42
batch start
#iterations: 221
currently lose_sum: 92.79655796289444
time_elpased: 1.398
batch start
#iterations: 222
currently lose_sum: 92.74879080057144
time_elpased: 1.409
batch start
#iterations: 223
currently lose_sum: 92.65333276987076
time_elpased: 1.431
batch start
#iterations: 224
currently lose_sum: 93.18639749288559
time_elpased: 1.452
batch start
#iterations: 225
currently lose_sum: 92.97974449396133
time_elpased: 1.427
batch start
#iterations: 226
currently lose_sum: 92.82610195875168
time_elpased: 1.404
batch start
#iterations: 227
currently lose_sum: 92.80465698242188
time_elpased: 1.431
batch start
#iterations: 228
currently lose_sum: 92.67589563131332
time_elpased: 1.411
batch start
#iterations: 229
currently lose_sum: 92.76379120349884
time_elpased: 1.395
batch start
#iterations: 230
currently lose_sum: 92.5899812579155
time_elpased: 1.42
batch start
#iterations: 231
currently lose_sum: 92.45241010189056
time_elpased: 1.393
batch start
#iterations: 232
currently lose_sum: 92.4166841506958
time_elpased: 1.41
batch start
#iterations: 233
currently lose_sum: 92.58941823244095
time_elpased: 1.415
batch start
#iterations: 234
currently lose_sum: 92.89391553401947
time_elpased: 1.447
batch start
#iterations: 235
currently lose_sum: 92.18615359067917
time_elpased: 1.39
batch start
#iterations: 236
currently lose_sum: 92.84941804409027
time_elpased: 1.432
batch start
#iterations: 237
currently lose_sum: 92.80186587572098
time_elpased: 1.425
batch start
#iterations: 238
currently lose_sum: 92.35848242044449
time_elpased: 1.416
batch start
#iterations: 239
currently lose_sum: 92.51863414049149
time_elpased: 1.403
start validation test
0.619020618557
0.614064338956
0.644334671195
0.628835434139
0.618976175833
62.153
batch start
#iterations: 240
currently lose_sum: 92.58384758234024
time_elpased: 1.393
batch start
#iterations: 241
currently lose_sum: 92.58990371227264
time_elpased: 1.424
batch start
#iterations: 242
currently lose_sum: 92.67647534608841
time_elpased: 1.427
batch start
#iterations: 243
currently lose_sum: 92.75072747468948
time_elpased: 1.406
batch start
#iterations: 244
currently lose_sum: 92.97671520709991
time_elpased: 1.429
batch start
#iterations: 245
currently lose_sum: 92.62099093198776
time_elpased: 1.389
batch start
#iterations: 246
currently lose_sum: 92.81795907020569
time_elpased: 1.422
batch start
#iterations: 247
currently lose_sum: 92.69423532485962
time_elpased: 1.394
batch start
#iterations: 248
currently lose_sum: 92.64275467395782
time_elpased: 1.406
batch start
#iterations: 249
currently lose_sum: 92.43181627988815
time_elpased: 1.428
batch start
#iterations: 250
currently lose_sum: 93.08553558588028
time_elpased: 1.407
batch start
#iterations: 251
currently lose_sum: 92.39642798900604
time_elpased: 1.399
batch start
#iterations: 252
currently lose_sum: 92.35912448167801
time_elpased: 1.419
batch start
#iterations: 253
currently lose_sum: 92.60377502441406
time_elpased: 1.405
batch start
#iterations: 254
currently lose_sum: 92.52714854478836
time_elpased: 1.424
batch start
#iterations: 255
currently lose_sum: 92.55983382463455
time_elpased: 1.432
batch start
#iterations: 256
currently lose_sum: 92.4639344215393
time_elpased: 1.443
batch start
#iterations: 257
currently lose_sum: 92.78495234251022
time_elpased: 1.389
batch start
#iterations: 258
currently lose_sum: 92.35982078313828
time_elpased: 1.399
batch start
#iterations: 259
currently lose_sum: 92.37710630893707
time_elpased: 1.389
start validation test
0.633298969072
0.636439878394
0.624781311104
0.630556709597
0.633313923134
61.228
batch start
#iterations: 260
currently lose_sum: 92.1737272143364
time_elpased: 1.418
batch start
#iterations: 261
currently lose_sum: 92.49664545059204
time_elpased: 1.393
batch start
#iterations: 262
currently lose_sum: 92.60985386371613
time_elpased: 1.401
batch start
#iterations: 263
currently lose_sum: 92.2719396352768
time_elpased: 1.393
batch start
#iterations: 264
currently lose_sum: 92.66481512784958
time_elpased: 1.4
batch start
#iterations: 265
currently lose_sum: 91.83984792232513
time_elpased: 1.406
batch start
#iterations: 266
currently lose_sum: 92.546621799469
time_elpased: 1.404
batch start
#iterations: 267
currently lose_sum: 92.58141374588013
time_elpased: 1.394
batch start
#iterations: 268
currently lose_sum: 92.30664646625519
time_elpased: 1.421
batch start
#iterations: 269
currently lose_sum: 91.93742817640305
time_elpased: 1.409
batch start
#iterations: 270
currently lose_sum: 92.8622362613678
time_elpased: 1.416
batch start
#iterations: 271
currently lose_sum: 92.72000992298126
time_elpased: 1.368
batch start
#iterations: 272
currently lose_sum: 91.75132673978806
time_elpased: 1.419
batch start
#iterations: 273
currently lose_sum: 92.57779031991959
time_elpased: 1.408
batch start
#iterations: 274
currently lose_sum: 92.16748386621475
time_elpased: 1.396
batch start
#iterations: 275
currently lose_sum: 91.99427968263626
time_elpased: 1.46
batch start
#iterations: 276
currently lose_sum: 92.57944202423096
time_elpased: 1.42
batch start
#iterations: 277
currently lose_sum: 92.25797694921494
time_elpased: 1.422
batch start
#iterations: 278
currently lose_sum: 92.54870450496674
time_elpased: 1.398
batch start
#iterations: 279
currently lose_sum: 92.25527453422546
time_elpased: 1.398
start validation test
0.633659793814
0.632192058347
0.642276422764
0.637194343764
0.633644665993
61.152
batch start
#iterations: 280
currently lose_sum: 92.45567309856415
time_elpased: 1.425
batch start
#iterations: 281
currently lose_sum: 92.56510424613953
time_elpased: 1.394
batch start
#iterations: 282
currently lose_sum: 92.15738141536713
time_elpased: 1.424
batch start
#iterations: 283
currently lose_sum: 92.37859272956848
time_elpased: 1.399
batch start
#iterations: 284
currently lose_sum: 92.29714101552963
time_elpased: 1.431
batch start
#iterations: 285
currently lose_sum: 92.15039449930191
time_elpased: 1.394
batch start
#iterations: 286
currently lose_sum: 92.00338983535767
time_elpased: 1.387
batch start
#iterations: 287
currently lose_sum: 92.16980171203613
time_elpased: 1.393
batch start
#iterations: 288
currently lose_sum: 91.98199301958084
time_elpased: 1.392
batch start
#iterations: 289
currently lose_sum: 91.65963071584702
time_elpased: 1.428
batch start
#iterations: 290
currently lose_sum: 92.1327395439148
time_elpased: 1.444
batch start
#iterations: 291
currently lose_sum: 91.88620281219482
time_elpased: 1.388
batch start
#iterations: 292
currently lose_sum: 92.47235476970673
time_elpased: 1.391
batch start
#iterations: 293
currently lose_sum: 91.81060242652893
time_elpased: 1.417
batch start
#iterations: 294
currently lose_sum: 91.70125639438629
time_elpased: 1.377
batch start
#iterations: 295
currently lose_sum: 91.8676969408989
time_elpased: 1.381
batch start
#iterations: 296
currently lose_sum: 91.97159093618393
time_elpased: 1.385
batch start
#iterations: 297
currently lose_sum: 92.08322846889496
time_elpased: 1.401
batch start
#iterations: 298
currently lose_sum: 92.25232112407684
time_elpased: 1.404
batch start
#iterations: 299
currently lose_sum: 91.68669748306274
time_elpased: 1.385
start validation test
0.625309278351
0.647150757394
0.553977565092
0.596950374272
0.625434512175
61.661
batch start
#iterations: 300
currently lose_sum: 92.06098490953445
time_elpased: 1.376
batch start
#iterations: 301
currently lose_sum: 92.08220285177231
time_elpased: 1.37
batch start
#iterations: 302
currently lose_sum: 92.24583125114441
time_elpased: 1.398
batch start
#iterations: 303
currently lose_sum: 92.04073822498322
time_elpased: 1.42
batch start
#iterations: 304
currently lose_sum: 91.78979957103729
time_elpased: 1.404
batch start
#iterations: 305
currently lose_sum: 92.08893251419067
time_elpased: 1.377
batch start
#iterations: 306
currently lose_sum: 92.25407874584198
time_elpased: 1.43
batch start
#iterations: 307
currently lose_sum: 91.65662610530853
time_elpased: 1.432
batch start
#iterations: 308
currently lose_sum: 91.80910038948059
time_elpased: 1.417
batch start
#iterations: 309
currently lose_sum: 91.994249522686
time_elpased: 1.382
batch start
#iterations: 310
currently lose_sum: 91.82704746723175
time_elpased: 1.384
batch start
#iterations: 311
currently lose_sum: 92.0027260184288
time_elpased: 1.403
batch start
#iterations: 312
currently lose_sum: 92.15647655725479
time_elpased: 1.385
batch start
#iterations: 313
currently lose_sum: 91.14286309480667
time_elpased: 1.42
batch start
#iterations: 314
currently lose_sum: 91.91737377643585
time_elpased: 1.389
batch start
#iterations: 315
currently lose_sum: 91.91192102432251
time_elpased: 1.441
batch start
#iterations: 316
currently lose_sum: 92.0635946393013
time_elpased: 1.43
batch start
#iterations: 317
currently lose_sum: 91.76350146532059
time_elpased: 1.385
batch start
#iterations: 318
currently lose_sum: 91.85308277606964
time_elpased: 1.397
batch start
#iterations: 319
currently lose_sum: 91.68654108047485
time_elpased: 1.396
start validation test
0.634381443299
0.637555042986
0.62581043532
0.631628148533
0.634396491025
61.205
batch start
#iterations: 320
currently lose_sum: 92.09652924537659
time_elpased: 1.417
batch start
#iterations: 321
currently lose_sum: 91.72446548938751
time_elpased: 1.391
batch start
#iterations: 322
currently lose_sum: 91.72376209497452
time_elpased: 1.408
batch start
#iterations: 323
currently lose_sum: 92.00917661190033
time_elpased: 1.394
batch start
#iterations: 324
currently lose_sum: 92.30682092905045
time_elpased: 1.435
batch start
#iterations: 325
currently lose_sum: 91.59217482805252
time_elpased: 1.392
batch start
#iterations: 326
currently lose_sum: 91.47161400318146
time_elpased: 1.405
batch start
#iterations: 327
currently lose_sum: 91.97791576385498
time_elpased: 1.411
batch start
#iterations: 328
currently lose_sum: 91.32705277204514
time_elpased: 1.388
batch start
#iterations: 329
currently lose_sum: 91.67553061246872
time_elpased: 1.405
batch start
#iterations: 330
currently lose_sum: 91.49383580684662
time_elpased: 1.411
batch start
#iterations: 331
currently lose_sum: 91.8422030210495
time_elpased: 1.41
batch start
#iterations: 332
currently lose_sum: 91.43967336416245
time_elpased: 1.438
batch start
#iterations: 333
currently lose_sum: 91.38653737306595
time_elpased: 1.398
batch start
#iterations: 334
currently lose_sum: 91.76351225376129
time_elpased: 1.393
batch start
#iterations: 335
currently lose_sum: 91.9867108464241
time_elpased: 1.438
batch start
#iterations: 336
currently lose_sum: 91.49693328142166
time_elpased: 1.418
batch start
#iterations: 337
currently lose_sum: 91.84299319982529
time_elpased: 1.388
batch start
#iterations: 338
currently lose_sum: 91.64965409040451
time_elpased: 1.397
batch start
#iterations: 339
currently lose_sum: 91.30674690008163
time_elpased: 1.438
start validation test
0.636288659794
0.634570648326
0.645672532675
0.640073454397
0.636272184958
61.243
batch start
#iterations: 340
currently lose_sum: 91.47068071365356
time_elpased: 1.429
batch start
#iterations: 341
currently lose_sum: 91.6239207983017
time_elpased: 1.393
batch start
#iterations: 342
currently lose_sum: 91.65760344266891
time_elpased: 1.403
batch start
#iterations: 343
currently lose_sum: 91.87037968635559
time_elpased: 1.417
batch start
#iterations: 344
currently lose_sum: 91.20849591493607
time_elpased: 1.411
batch start
#iterations: 345
currently lose_sum: 91.68137621879578
time_elpased: 1.423
batch start
#iterations: 346
currently lose_sum: 91.2129607796669
time_elpased: 1.436
batch start
#iterations: 347
currently lose_sum: 91.83759993314743
time_elpased: 1.394
batch start
#iterations: 348
currently lose_sum: 91.54331278800964
time_elpased: 1.459
batch start
#iterations: 349
currently lose_sum: 91.78848677873611
time_elpased: 1.42
batch start
#iterations: 350
currently lose_sum: 90.97064220905304
time_elpased: 1.399
batch start
#iterations: 351
currently lose_sum: 91.07606929540634
time_elpased: 1.415
batch start
#iterations: 352
currently lose_sum: 91.48000067472458
time_elpased: 1.419
batch start
#iterations: 353
currently lose_sum: 91.62515860795975
time_elpased: 1.437
batch start
#iterations: 354
currently lose_sum: 91.81495720148087
time_elpased: 1.479
batch start
#iterations: 355
currently lose_sum: 91.56662261486053
time_elpased: 1.407
batch start
#iterations: 356
currently lose_sum: 91.65453028678894
time_elpased: 1.412
batch start
#iterations: 357
currently lose_sum: 91.2438337802887
time_elpased: 1.425
batch start
#iterations: 358
currently lose_sum: 91.64438223838806
time_elpased: 1.412
batch start
#iterations: 359
currently lose_sum: 91.67965215444565
time_elpased: 1.397
start validation test
0.629690721649
0.645190874699
0.579191108367
0.610412147505
0.62977938151
61.565
batch start
#iterations: 360
currently lose_sum: 91.6042228937149
time_elpased: 1.428
batch start
#iterations: 361
currently lose_sum: 91.85114639997482
time_elpased: 1.407
batch start
#iterations: 362
currently lose_sum: 91.1656579375267
time_elpased: 1.42
batch start
#iterations: 363
currently lose_sum: 91.46962398290634
time_elpased: 1.436
batch start
#iterations: 364
currently lose_sum: 91.771355509758
time_elpased: 1.407
batch start
#iterations: 365
currently lose_sum: 91.89678847789764
time_elpased: 1.466
batch start
#iterations: 366
currently lose_sum: 91.18709951639175
time_elpased: 1.393
batch start
#iterations: 367
currently lose_sum: 91.49965649843216
time_elpased: 1.407
batch start
#iterations: 368
currently lose_sum: 91.4872430562973
time_elpased: 1.4
batch start
#iterations: 369
currently lose_sum: 91.31771916151047
time_elpased: 1.413
batch start
#iterations: 370
currently lose_sum: 90.91150140762329
time_elpased: 1.427
batch start
#iterations: 371
currently lose_sum: 91.1148693561554
time_elpased: 1.409
batch start
#iterations: 372
currently lose_sum: 91.05363523960114
time_elpased: 1.46
batch start
#iterations: 373
currently lose_sum: 90.93453484773636
time_elpased: 1.431
batch start
#iterations: 374
currently lose_sum: 90.85704225301743
time_elpased: 1.437
batch start
#iterations: 375
currently lose_sum: 91.18569928407669
time_elpased: 1.411
batch start
#iterations: 376
currently lose_sum: 91.39632368087769
time_elpased: 1.443
batch start
#iterations: 377
currently lose_sum: 91.19539594650269
time_elpased: 1.387
batch start
#iterations: 378
currently lose_sum: 91.182770550251
time_elpased: 1.405
batch start
#iterations: 379
currently lose_sum: 91.21784192323685
time_elpased: 1.409
start validation test
0.631855670103
0.639445467345
0.607594936709
0.623113456464
0.631898263563
61.287
batch start
#iterations: 380
currently lose_sum: 91.04542255401611
time_elpased: 1.396
batch start
#iterations: 381
currently lose_sum: 91.1605293750763
time_elpased: 1.405
batch start
#iterations: 382
currently lose_sum: 90.62814325094223
time_elpased: 1.397
batch start
#iterations: 383
currently lose_sum: 91.35858726501465
time_elpased: 1.399
batch start
#iterations: 384
currently lose_sum: 91.98863518238068
time_elpased: 1.396
batch start
#iterations: 385
currently lose_sum: 90.9420850276947
time_elpased: 1.401
batch start
#iterations: 386
currently lose_sum: 91.46579599380493
time_elpased: 1.409
batch start
#iterations: 387
currently lose_sum: 91.40311819314957
time_elpased: 1.383
batch start
#iterations: 388
currently lose_sum: 91.1222477555275
time_elpased: 1.399
batch start
#iterations: 389
currently lose_sum: 91.2619811296463
time_elpased: 1.371
batch start
#iterations: 390
currently lose_sum: 91.15775102376938
time_elpased: 1.385
batch start
#iterations: 391
currently lose_sum: 91.08983373641968
time_elpased: 1.379
batch start
#iterations: 392
currently lose_sum: 90.88960480690002
time_elpased: 1.412
batch start
#iterations: 393
currently lose_sum: 91.30466908216476
time_elpased: 1.408
batch start
#iterations: 394
currently lose_sum: 90.80604761838913
time_elpased: 1.406
batch start
#iterations: 395
currently lose_sum: 91.2324470281601
time_elpased: 1.393
batch start
#iterations: 396
currently lose_sum: 90.90781652927399
time_elpased: 1.413
batch start
#iterations: 397
currently lose_sum: 90.84135615825653
time_elpased: 1.403
batch start
#iterations: 398
currently lose_sum: 90.63911867141724
time_elpased: 1.444
batch start
#iterations: 399
currently lose_sum: 91.18529677391052
time_elpased: 1.392
start validation test
0.635051546392
0.636844836533
0.631470618504
0.634146341463
0.635057833263
61.308
acc: 0.632
pre: 0.631
rec: 0.639
F1: 0.635
auc: 0.632
