start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.43203794956207
time_elpased: 1.912
batch start
#iterations: 1
currently lose_sum: 100.38347309827805
time_elpased: 1.885
batch start
#iterations: 2
currently lose_sum: 100.26032024621964
time_elpased: 1.87
batch start
#iterations: 3
currently lose_sum: 100.12507629394531
time_elpased: 2.004
batch start
#iterations: 4
currently lose_sum: 100.00043416023254
time_elpased: 1.895
batch start
#iterations: 5
currently lose_sum: 99.93231266736984
time_elpased: 1.911
batch start
#iterations: 6
currently lose_sum: 99.69368767738342
time_elpased: 1.82
batch start
#iterations: 7
currently lose_sum: 99.88168144226074
time_elpased: 1.955
batch start
#iterations: 8
currently lose_sum: 99.3165374994278
time_elpased: 1.833
batch start
#iterations: 9
currently lose_sum: 99.36829173564911
time_elpased: 1.882
batch start
#iterations: 10
currently lose_sum: 99.19445794820786
time_elpased: 1.876
batch start
#iterations: 11
currently lose_sum: 99.12930113077164
time_elpased: 1.942
batch start
#iterations: 12
currently lose_sum: 99.02098876237869
time_elpased: 1.852
batch start
#iterations: 13
currently lose_sum: 98.9030601978302
time_elpased: 1.874
batch start
#iterations: 14
currently lose_sum: 98.7694593667984
time_elpased: 1.854
batch start
#iterations: 15
currently lose_sum: 98.51498728990555
time_elpased: 1.883
batch start
#iterations: 16
currently lose_sum: 98.52196270227432
time_elpased: 1.838
batch start
#iterations: 17
currently lose_sum: 98.30929124355316
time_elpased: 1.882
batch start
#iterations: 18
currently lose_sum: 98.44571447372437
time_elpased: 1.87
batch start
#iterations: 19
currently lose_sum: 98.12004142999649
time_elpased: 1.941
start validation test
0.625309278351
0.595016301816
0.788823711022
0.678348599496
0.625022203544
63.365
batch start
#iterations: 20
currently lose_sum: 97.89181572198868
time_elpased: 1.894
batch start
#iterations: 21
currently lose_sum: 97.96226388216019
time_elpased: 1.854
batch start
#iterations: 22
currently lose_sum: 97.98526954650879
time_elpased: 1.842
batch start
#iterations: 23
currently lose_sum: 97.57444888353348
time_elpased: 1.911
batch start
#iterations: 24
currently lose_sum: 97.52505272626877
time_elpased: 1.856
batch start
#iterations: 25
currently lose_sum: 97.82855707406998
time_elpased: 1.869
batch start
#iterations: 26
currently lose_sum: 97.49861681461334
time_elpased: 1.893
batch start
#iterations: 27
currently lose_sum: 97.39041751623154
time_elpased: 1.901
batch start
#iterations: 28
currently lose_sum: 97.76393032073975
time_elpased: 1.95
batch start
#iterations: 29
currently lose_sum: 97.61831456422806
time_elpased: 1.836
batch start
#iterations: 30
currently lose_sum: 97.60615926980972
time_elpased: 1.904
batch start
#iterations: 31
currently lose_sum: 97.24162858724594
time_elpased: 1.883
batch start
#iterations: 32
currently lose_sum: 96.96276086568832
time_elpased: 1.863
batch start
#iterations: 33
currently lose_sum: 97.25857359170914
time_elpased: 1.901
batch start
#iterations: 34
currently lose_sum: 96.78642892837524
time_elpased: 1.859
batch start
#iterations: 35
currently lose_sum: 97.27529340982437
time_elpased: 1.903
batch start
#iterations: 36
currently lose_sum: 96.56222033500671
time_elpased: 1.877
batch start
#iterations: 37
currently lose_sum: 96.88845402002335
time_elpased: 1.887
batch start
#iterations: 38
currently lose_sum: 96.95752841234207
time_elpased: 1.902
batch start
#iterations: 39
currently lose_sum: 96.75453728437424
time_elpased: 1.918
start validation test
0.639432989691
0.607216007563
0.793248945148
0.687876489224
0.639162942056
62.557
batch start
#iterations: 40
currently lose_sum: 96.39551883935928
time_elpased: 1.866
batch start
#iterations: 41
currently lose_sum: 97.15248209238052
time_elpased: 1.855
batch start
#iterations: 42
currently lose_sum: 96.69116371870041
time_elpased: 1.844
batch start
#iterations: 43
currently lose_sum: 96.373280107975
time_elpased: 1.819
batch start
#iterations: 44
currently lose_sum: 96.45646554231644
time_elpased: 1.965
batch start
#iterations: 45
currently lose_sum: 96.53001815080643
time_elpased: 1.849
batch start
#iterations: 46
currently lose_sum: 96.18923628330231
time_elpased: 1.907
batch start
#iterations: 47
currently lose_sum: 96.28322958946228
time_elpased: 1.881
batch start
#iterations: 48
currently lose_sum: 96.25397843122482
time_elpased: 1.902
batch start
#iterations: 49
currently lose_sum: 96.3859806060791
time_elpased: 1.836
batch start
#iterations: 50
currently lose_sum: 96.25533574819565
time_elpased: 1.888
batch start
#iterations: 51
currently lose_sum: 96.22628927230835
time_elpased: 1.876
batch start
#iterations: 52
currently lose_sum: 96.39841771125793
time_elpased: 1.941
batch start
#iterations: 53
currently lose_sum: 96.11380714178085
time_elpased: 1.93
batch start
#iterations: 54
currently lose_sum: 96.10379493236542
time_elpased: 1.91
batch start
#iterations: 55
currently lose_sum: 96.00956374406815
time_elpased: 1.865
batch start
#iterations: 56
currently lose_sum: 96.10852426290512
time_elpased: 1.851
batch start
#iterations: 57
currently lose_sum: 95.99459755420685
time_elpased: 1.902
batch start
#iterations: 58
currently lose_sum: 95.71447658538818
time_elpased: 1.905
batch start
#iterations: 59
currently lose_sum: 95.97521609067917
time_elpased: 1.891
start validation test
0.645
0.638481111764
0.671400638057
0.654527213444
0.644953649608
61.260
batch start
#iterations: 60
currently lose_sum: 96.20073622465134
time_elpased: 1.847
batch start
#iterations: 61
currently lose_sum: 95.44290286302567
time_elpased: 1.877
batch start
#iterations: 62
currently lose_sum: 95.90109843015671
time_elpased: 1.897
batch start
#iterations: 63
currently lose_sum: 95.88503795862198
time_elpased: 1.895
batch start
#iterations: 64
currently lose_sum: 95.54370647668839
time_elpased: 1.905
batch start
#iterations: 65
currently lose_sum: 95.51733529567719
time_elpased: 1.916
batch start
#iterations: 66
currently lose_sum: 95.32789301872253
time_elpased: 1.853
batch start
#iterations: 67
currently lose_sum: 95.43655973672867
time_elpased: 1.852
batch start
#iterations: 68
currently lose_sum: 94.96789503097534
time_elpased: 1.852
batch start
#iterations: 69
currently lose_sum: 95.30405783653259
time_elpased: 1.95
batch start
#iterations: 70
currently lose_sum: 94.9347665309906
time_elpased: 1.904
batch start
#iterations: 71
currently lose_sum: 95.39307290315628
time_elpased: 1.897
batch start
#iterations: 72
currently lose_sum: 95.38693368434906
time_elpased: 1.865
batch start
#iterations: 73
currently lose_sum: 95.36935687065125
time_elpased: 1.849
batch start
#iterations: 74
currently lose_sum: 95.08657854795456
time_elpased: 1.932
batch start
#iterations: 75
currently lose_sum: 94.97646468877792
time_elpased: 1.861
batch start
#iterations: 76
currently lose_sum: 95.23896366357803
time_elpased: 1.905
batch start
#iterations: 77
currently lose_sum: 94.78164774179459
time_elpased: 1.86
batch start
#iterations: 78
currently lose_sum: 95.22517663240433
time_elpased: 1.883
batch start
#iterations: 79
currently lose_sum: 95.08488166332245
time_elpased: 1.877
start validation test
0.638144329897
0.633448787729
0.658742410209
0.645848047624
0.63810816679
61.555
batch start
#iterations: 80
currently lose_sum: 95.21669048070908
time_elpased: 1.892
batch start
#iterations: 81
currently lose_sum: 95.04422295093536
time_elpased: 1.843
batch start
#iterations: 82
currently lose_sum: 95.45433688163757
time_elpased: 1.843
batch start
#iterations: 83
currently lose_sum: 94.72786217927933
time_elpased: 1.843
batch start
#iterations: 84
currently lose_sum: 94.95517665147781
time_elpased: 1.917
batch start
#iterations: 85
currently lose_sum: 94.94063729047775
time_elpased: 1.887
batch start
#iterations: 86
currently lose_sum: 95.03154581785202
time_elpased: 1.856
batch start
#iterations: 87
currently lose_sum: 94.91491532325745
time_elpased: 1.928
batch start
#iterations: 88
currently lose_sum: 95.22266435623169
time_elpased: 1.865
batch start
#iterations: 89
currently lose_sum: 94.93819046020508
time_elpased: 1.872
batch start
#iterations: 90
currently lose_sum: 94.80570256710052
time_elpased: 1.957
batch start
#iterations: 91
currently lose_sum: 94.74303334951401
time_elpased: 1.88
batch start
#iterations: 92
currently lose_sum: 94.68944656848907
time_elpased: 1.842
batch start
#iterations: 93
currently lose_sum: 95.18452650308609
time_elpased: 1.868
batch start
#iterations: 94
currently lose_sum: 94.69655829668045
time_elpased: 1.875
batch start
#iterations: 95
currently lose_sum: 94.53077614307404
time_elpased: 1.842
batch start
#iterations: 96
currently lose_sum: 94.59851902723312
time_elpased: 1.862
batch start
#iterations: 97
currently lose_sum: 94.4702815413475
time_elpased: 1.901
batch start
#iterations: 98
currently lose_sum: 94.91841697692871
time_elpased: 1.896
batch start
#iterations: 99
currently lose_sum: 94.72442018985748
time_elpased: 1.83
start validation test
0.645154639175
0.658889512058
0.604507564063
0.630528123658
0.645226001385
60.833
batch start
#iterations: 100
currently lose_sum: 94.58944165706635
time_elpased: 1.919
batch start
#iterations: 101
currently lose_sum: 94.58487874269485
time_elpased: 1.865
batch start
#iterations: 102
currently lose_sum: 94.51773452758789
time_elpased: 1.845
batch start
#iterations: 103
currently lose_sum: 94.97764247655869
time_elpased: 1.868
batch start
#iterations: 104
currently lose_sum: 94.62807565927505
time_elpased: 1.933
batch start
#iterations: 105
currently lose_sum: 93.995574593544
time_elpased: 1.861
batch start
#iterations: 106
currently lose_sum: 94.35788887739182
time_elpased: 1.921
batch start
#iterations: 107
currently lose_sum: 94.19665956497192
time_elpased: 1.811
batch start
#iterations: 108
currently lose_sum: 94.52589130401611
time_elpased: 1.853
batch start
#iterations: 109
currently lose_sum: 94.04648369550705
time_elpased: 1.9
batch start
#iterations: 110
currently lose_sum: 94.3853098154068
time_elpased: 1.867
batch start
#iterations: 111
currently lose_sum: 94.15158331394196
time_elpased: 1.922
batch start
#iterations: 112
currently lose_sum: 94.0301878452301
time_elpased: 1.899
batch start
#iterations: 113
currently lose_sum: 94.23137789964676
time_elpased: 1.896
batch start
#iterations: 114
currently lose_sum: 94.22974783182144
time_elpased: 1.908
batch start
#iterations: 115
currently lose_sum: 94.28837990760803
time_elpased: 1.847
batch start
#iterations: 116
currently lose_sum: 93.65135687589645
time_elpased: 1.866
batch start
#iterations: 117
currently lose_sum: 93.62801969051361
time_elpased: 1.929
batch start
#iterations: 118
currently lose_sum: 93.48570275306702
time_elpased: 1.85
batch start
#iterations: 119
currently lose_sum: 93.48621445894241
time_elpased: 1.908
start validation test
0.638092783505
0.621507120966
0.709581146444
0.662630339724
0.637967274658
61.570
batch start
#iterations: 120
currently lose_sum: 93.90783655643463
time_elpased: 1.87
batch start
#iterations: 121
currently lose_sum: 94.24317467212677
time_elpased: 1.892
batch start
#iterations: 122
currently lose_sum: 93.78402727842331
time_elpased: 1.87
batch start
#iterations: 123
currently lose_sum: 93.98104405403137
time_elpased: 1.852
batch start
#iterations: 124
currently lose_sum: 94.1195958852768
time_elpased: 1.864
batch start
#iterations: 125
currently lose_sum: 93.5168844461441
time_elpased: 1.886
batch start
#iterations: 126
currently lose_sum: 94.1696839928627
time_elpased: 1.896
batch start
#iterations: 127
currently lose_sum: 93.71398115158081
time_elpased: 1.869
batch start
#iterations: 128
currently lose_sum: 93.7161768078804
time_elpased: 1.85
batch start
#iterations: 129
currently lose_sum: 94.6773601770401
time_elpased: 1.832
batch start
#iterations: 130
currently lose_sum: 93.49454075098038
time_elpased: 1.857
batch start
#iterations: 131
currently lose_sum: 93.99481397867203
time_elpased: 1.852
batch start
#iterations: 132
currently lose_sum: 93.05566102266312
time_elpased: 1.908
batch start
#iterations: 133
currently lose_sum: 93.49447935819626
time_elpased: 1.85
batch start
#iterations: 134
currently lose_sum: 93.53754830360413
time_elpased: 1.89
batch start
#iterations: 135
currently lose_sum: 93.59167486429214
time_elpased: 1.876
batch start
#iterations: 136
currently lose_sum: 93.69116532802582
time_elpased: 1.829
batch start
#iterations: 137
currently lose_sum: 93.51685267686844
time_elpased: 1.893
batch start
#iterations: 138
currently lose_sum: 93.78861480951309
time_elpased: 1.93
batch start
#iterations: 139
currently lose_sum: 93.33793008327484
time_elpased: 1.874
start validation test
0.630824742268
0.619381366227
0.682103529896
0.649231070624
0.630734714447
61.765
batch start
#iterations: 140
currently lose_sum: 93.40200638771057
time_elpased: 1.848
batch start
#iterations: 141
currently lose_sum: 93.71848809719086
time_elpased: 1.88
batch start
#iterations: 142
currently lose_sum: 93.18317544460297
time_elpased: 1.856
batch start
#iterations: 143
currently lose_sum: 93.49947512149811
time_elpased: 1.864
batch start
#iterations: 144
currently lose_sum: 93.53484106063843
time_elpased: 1.803
batch start
#iterations: 145
currently lose_sum: 93.61892700195312
time_elpased: 1.793
batch start
#iterations: 146
currently lose_sum: 93.11656433343887
time_elpased: 1.869
batch start
#iterations: 147
currently lose_sum: 93.40388894081116
time_elpased: 1.84
batch start
#iterations: 148
currently lose_sum: 93.28358513116837
time_elpased: 1.847
batch start
#iterations: 149
currently lose_sum: 93.27657181024551
time_elpased: 1.88
batch start
#iterations: 150
currently lose_sum: 93.37968230247498
time_elpased: 1.888
batch start
#iterations: 151
currently lose_sum: 92.98757350444794
time_elpased: 1.86
batch start
#iterations: 152
currently lose_sum: 93.5859609246254
time_elpased: 1.855
batch start
#iterations: 153
currently lose_sum: 93.00546216964722
time_elpased: 1.872
batch start
#iterations: 154
currently lose_sum: 93.1406998038292
time_elpased: 1.941
batch start
#iterations: 155
currently lose_sum: 92.64521330595016
time_elpased: 1.876
batch start
#iterations: 156
currently lose_sum: 93.4381610751152
time_elpased: 1.898
batch start
#iterations: 157
currently lose_sum: 92.72119927406311
time_elpased: 1.891
batch start
#iterations: 158
currently lose_sum: 93.32346403598785
time_elpased: 1.842
batch start
#iterations: 159
currently lose_sum: 92.71718353033066
time_elpased: 1.853
start validation test
0.644793814433
0.631173412551
0.699701553978
0.663673190492
0.644697415427
60.952
batch start
#iterations: 160
currently lose_sum: 92.63770735263824
time_elpased: 1.903
batch start
#iterations: 161
currently lose_sum: 92.82346475124359
time_elpased: 1.853
batch start
#iterations: 162
currently lose_sum: 92.9933769106865
time_elpased: 1.886
batch start
#iterations: 163
currently lose_sum: 93.10366326570511
time_elpased: 1.83
batch start
#iterations: 164
currently lose_sum: 92.42773866653442
time_elpased: 1.836
batch start
#iterations: 165
currently lose_sum: 92.67593568563461
time_elpased: 1.886
batch start
#iterations: 166
currently lose_sum: 93.3651077747345
time_elpased: 1.834
batch start
#iterations: 167
currently lose_sum: 92.54327070713043
time_elpased: 1.867
batch start
#iterations: 168
currently lose_sum: 92.78061485290527
time_elpased: 1.834
batch start
#iterations: 169
currently lose_sum: 92.62644785642624
time_elpased: 1.832
batch start
#iterations: 170
currently lose_sum: 92.72769719362259
time_elpased: 1.883
batch start
#iterations: 171
currently lose_sum: 92.91680365800858
time_elpased: 1.86
batch start
#iterations: 172
currently lose_sum: 92.82191127538681
time_elpased: 1.9
batch start
#iterations: 173
currently lose_sum: 92.81326466798782
time_elpased: 1.877
batch start
#iterations: 174
currently lose_sum: 92.66616100072861
time_elpased: 1.81
batch start
#iterations: 175
currently lose_sum: 92.53341633081436
time_elpased: 1.872
batch start
#iterations: 176
currently lose_sum: 92.27889597415924
time_elpased: 1.958
batch start
#iterations: 177
currently lose_sum: 92.09027725458145
time_elpased: 1.821
batch start
#iterations: 178
currently lose_sum: 92.33480072021484
time_elpased: 1.946
batch start
#iterations: 179
currently lose_sum: 92.0434245467186
time_elpased: 1.817
start validation test
0.636134020619
0.648259705489
0.598024081507
0.622129436326
0.636200928495
61.767
batch start
#iterations: 180
currently lose_sum: 92.6539495587349
time_elpased: 1.895
batch start
#iterations: 181
currently lose_sum: 92.38307452201843
time_elpased: 1.861
batch start
#iterations: 182
currently lose_sum: 92.57668250799179
time_elpased: 1.876
batch start
#iterations: 183
currently lose_sum: 92.09279358386993
time_elpased: 1.827
batch start
#iterations: 184
currently lose_sum: 92.40646666288376
time_elpased: 1.883
batch start
#iterations: 185
currently lose_sum: 92.04429167509079
time_elpased: 1.839
batch start
#iterations: 186
currently lose_sum: 92.22122222185135
time_elpased: 1.836
batch start
#iterations: 187
currently lose_sum: 92.29676389694214
time_elpased: 1.867
batch start
#iterations: 188
currently lose_sum: 92.31308305263519
time_elpased: 1.86
batch start
#iterations: 189
currently lose_sum: 92.5733425617218
time_elpased: 1.837
batch start
#iterations: 190
currently lose_sum: 91.98185950517654
time_elpased: 1.844
batch start
#iterations: 191
currently lose_sum: 91.8603248000145
time_elpased: 1.836
batch start
#iterations: 192
currently lose_sum: 92.77455258369446
time_elpased: 1.824
batch start
#iterations: 193
currently lose_sum: 92.17632752656937
time_elpased: 1.854
batch start
#iterations: 194
currently lose_sum: 92.11764055490494
time_elpased: 1.879
batch start
#iterations: 195
currently lose_sum: 92.30114811658859
time_elpased: 1.889
batch start
#iterations: 196
currently lose_sum: 92.12502461671829
time_elpased: 1.863
batch start
#iterations: 197
currently lose_sum: 92.20537221431732
time_elpased: 1.864
batch start
#iterations: 198
currently lose_sum: 91.84840643405914
time_elpased: 1.786
batch start
#iterations: 199
currently lose_sum: 91.7452443242073
time_elpased: 1.886
start validation test
0.631597938144
0.638231497418
0.610579396933
0.624099300479
0.631634839435
62.208
batch start
#iterations: 200
currently lose_sum: 91.8830052614212
time_elpased: 1.847
batch start
#iterations: 201
currently lose_sum: 91.99199402332306
time_elpased: 1.929
batch start
#iterations: 202
currently lose_sum: 91.62659746408463
time_elpased: 1.832
batch start
#iterations: 203
currently lose_sum: 91.50635951757431
time_elpased: 1.818
batch start
#iterations: 204
currently lose_sum: 91.25429821014404
time_elpased: 1.942
batch start
#iterations: 205
currently lose_sum: 91.05051970481873
time_elpased: 1.833
batch start
#iterations: 206
currently lose_sum: 91.99621313810349
time_elpased: 1.863
batch start
#iterations: 207
currently lose_sum: 91.97726112604141
time_elpased: 1.863
batch start
#iterations: 208
currently lose_sum: 91.46297585964203
time_elpased: 1.885
batch start
#iterations: 209
currently lose_sum: 91.60989260673523
time_elpased: 1.889
batch start
#iterations: 210
currently lose_sum: 91.34466189146042
time_elpased: 1.863
batch start
#iterations: 211
currently lose_sum: 91.79887187480927
time_elpased: 1.899
batch start
#iterations: 212
currently lose_sum: 91.62366396188736
time_elpased: 1.857
batch start
#iterations: 213
currently lose_sum: 91.3773598074913
time_elpased: 1.885
batch start
#iterations: 214
currently lose_sum: 91.47407323122025
time_elpased: 1.818
batch start
#iterations: 215
currently lose_sum: 90.87022370100021
time_elpased: 1.861
batch start
#iterations: 216
currently lose_sum: 91.0693569779396
time_elpased: 1.871
batch start
#iterations: 217
currently lose_sum: 91.91780233383179
time_elpased: 1.849
batch start
#iterations: 218
currently lose_sum: 90.97355657815933
time_elpased: 1.842
batch start
#iterations: 219
currently lose_sum: 91.38793796300888
time_elpased: 1.954
start validation test
0.643608247423
0.638337775146
0.66553463003
0.651652559452
0.643569752276
61.510
batch start
#iterations: 220
currently lose_sum: 91.27399563789368
time_elpased: 1.862
batch start
#iterations: 221
currently lose_sum: 91.59923923015594
time_elpased: 1.953
batch start
#iterations: 222
currently lose_sum: 90.87263399362564
time_elpased: 1.859
batch start
#iterations: 223
currently lose_sum: 91.16260921955109
time_elpased: 1.909
batch start
#iterations: 224
currently lose_sum: 90.40432679653168
time_elpased: 1.867
batch start
#iterations: 225
currently lose_sum: 91.42062711715698
time_elpased: 1.932
batch start
#iterations: 226
currently lose_sum: 90.90863382816315
time_elpased: 1.868
batch start
#iterations: 227
currently lose_sum: 90.84153538942337
time_elpased: 1.904
batch start
#iterations: 228
currently lose_sum: 90.97589445114136
time_elpased: 1.858
batch start
#iterations: 229
currently lose_sum: 90.92677623033524
time_elpased: 1.87
batch start
#iterations: 230
currently lose_sum: 90.7825026512146
time_elpased: 1.878
batch start
#iterations: 231
currently lose_sum: 90.94999974966049
time_elpased: 1.903
batch start
#iterations: 232
currently lose_sum: 90.67362040281296
time_elpased: 1.877
batch start
#iterations: 233
currently lose_sum: 90.74941092729568
time_elpased: 1.87
batch start
#iterations: 234
currently lose_sum: 90.84140408039093
time_elpased: 1.882
batch start
#iterations: 235
currently lose_sum: 90.679472386837
time_elpased: 1.927
batch start
#iterations: 236
currently lose_sum: 90.78645086288452
time_elpased: 1.858
batch start
#iterations: 237
currently lose_sum: 90.47140032052994
time_elpased: 1.852
batch start
#iterations: 238
currently lose_sum: 90.53998553752899
time_elpased: 1.863
batch start
#iterations: 239
currently lose_sum: 91.04287630319595
time_elpased: 1.865
start validation test
0.641649484536
0.634288489558
0.672018112586
0.652608434939
0.641596167726
61.319
batch start
#iterations: 240
currently lose_sum: 89.67801600694656
time_elpased: 1.87
batch start
#iterations: 241
currently lose_sum: 90.50700241327286
time_elpased: 1.858
batch start
#iterations: 242
currently lose_sum: 90.60586935281754
time_elpased: 1.867
batch start
#iterations: 243
currently lose_sum: 90.43120950460434
time_elpased: 1.887
batch start
#iterations: 244
currently lose_sum: 90.81164711713791
time_elpased: 1.842
batch start
#iterations: 245
currently lose_sum: 90.0716861486435
time_elpased: 1.879
batch start
#iterations: 246
currently lose_sum: 90.10743534564972
time_elpased: 1.838
batch start
#iterations: 247
currently lose_sum: 89.81619602441788
time_elpased: 1.87
batch start
#iterations: 248
currently lose_sum: 89.93184554576874
time_elpased: 1.857
batch start
#iterations: 249
currently lose_sum: 89.89956450462341
time_elpased: 1.886
batch start
#iterations: 250
currently lose_sum: 89.97955477237701
time_elpased: 1.872
batch start
#iterations: 251
currently lose_sum: 89.93851912021637
time_elpased: 1.931
batch start
#iterations: 252
currently lose_sum: 89.9382209777832
time_elpased: 1.888
batch start
#iterations: 253
currently lose_sum: 89.62246304750443
time_elpased: 1.841
batch start
#iterations: 254
currently lose_sum: 90.01144230365753
time_elpased: 1.887
batch start
#iterations: 255
currently lose_sum: 90.31963700056076
time_elpased: 1.871
batch start
#iterations: 256
currently lose_sum: 89.36813229322433
time_elpased: 1.884
batch start
#iterations: 257
currently lose_sum: 90.09596085548401
time_elpased: 1.879
batch start
#iterations: 258
currently lose_sum: 90.19437140226364
time_elpased: 1.89
batch start
#iterations: 259
currently lose_sum: 89.59158140420914
time_elpased: 1.867
start validation test
0.645515463918
0.636224098235
0.682515179582
0.658557171938
0.64545050521
62.311
batch start
#iterations: 260
currently lose_sum: 89.60395967960358
time_elpased: 1.838
batch start
#iterations: 261
currently lose_sum: 89.13162112236023
time_elpased: 1.881
batch start
#iterations: 262
currently lose_sum: 89.80914813280106
time_elpased: 1.845
batch start
#iterations: 263
currently lose_sum: 89.45579099655151
time_elpased: 1.851
batch start
#iterations: 264
currently lose_sum: 89.34564423561096
time_elpased: 1.891
batch start
#iterations: 265
currently lose_sum: 89.8662656545639
time_elpased: 1.886
batch start
#iterations: 266
currently lose_sum: 89.63432735204697
time_elpased: 1.888
batch start
#iterations: 267
currently lose_sum: 89.0362144112587
time_elpased: 1.885
batch start
#iterations: 268
currently lose_sum: 89.72094774246216
time_elpased: 1.819
batch start
#iterations: 269
currently lose_sum: 89.11633312702179
time_elpased: 1.935
batch start
#iterations: 270
currently lose_sum: 89.56511229276657
time_elpased: 1.821
batch start
#iterations: 271
currently lose_sum: 89.27903032302856
time_elpased: 1.849
batch start
#iterations: 272
currently lose_sum: 88.87304377555847
time_elpased: 1.851
batch start
#iterations: 273
currently lose_sum: 89.06576973199844
time_elpased: 1.884
batch start
#iterations: 274
currently lose_sum: 89.28454959392548
time_elpased: 1.901
batch start
#iterations: 275
currently lose_sum: 88.87897109985352
time_elpased: 1.871
batch start
#iterations: 276
currently lose_sum: 88.89659470319748
time_elpased: 1.918
batch start
#iterations: 277
currently lose_sum: 88.58423960208893
time_elpased: 1.886
batch start
#iterations: 278
currently lose_sum: 88.9443234205246
time_elpased: 1.885
batch start
#iterations: 279
currently lose_sum: 89.00173777341843
time_elpased: 1.906
start validation test
0.625567010309
0.630298523319
0.610579396933
0.620282279143
0.625593323376
63.919
batch start
#iterations: 280
currently lose_sum: 89.29183024168015
time_elpased: 1.848
batch start
#iterations: 281
currently lose_sum: 88.89249920845032
time_elpased: 1.874
batch start
#iterations: 282
currently lose_sum: 87.9827224612236
time_elpased: 1.891
batch start
#iterations: 283
currently lose_sum: 88.50289458036423
time_elpased: 1.854
batch start
#iterations: 284
currently lose_sum: 88.72685778141022
time_elpased: 1.848
batch start
#iterations: 285
currently lose_sum: 88.51883280277252
time_elpased: 1.868
batch start
#iterations: 286
currently lose_sum: 87.74256068468094
time_elpased: 1.866
batch start
#iterations: 287
currently lose_sum: 88.27348220348358
time_elpased: 1.877
batch start
#iterations: 288
currently lose_sum: 88.28380072116852
time_elpased: 1.87
batch start
#iterations: 289
currently lose_sum: 87.95628321170807
time_elpased: 1.869
batch start
#iterations: 290
currently lose_sum: 88.50780707597733
time_elpased: 1.88
batch start
#iterations: 291
currently lose_sum: 88.53675174713135
time_elpased: 1.86
batch start
#iterations: 292
currently lose_sum: 87.87961649894714
time_elpased: 1.875
batch start
#iterations: 293
currently lose_sum: 88.0067726969719
time_elpased: 1.874
batch start
#iterations: 294
currently lose_sum: 88.0798767209053
time_elpased: 1.881
batch start
#iterations: 295
currently lose_sum: 87.85943692922592
time_elpased: 1.854
batch start
#iterations: 296
currently lose_sum: 88.23415768146515
time_elpased: 1.887
batch start
#iterations: 297
currently lose_sum: 87.43612492084503
time_elpased: 1.899
batch start
#iterations: 298
currently lose_sum: 87.87639564275742
time_elpased: 1.854
batch start
#iterations: 299
currently lose_sum: 87.77610087394714
time_elpased: 1.874
start validation test
0.619742268041
0.618469015796
0.628589070701
0.623487980401
0.619726736115
64.199
batch start
#iterations: 300
currently lose_sum: 87.92042738199234
time_elpased: 1.835
batch start
#iterations: 301
currently lose_sum: 87.5998545885086
time_elpased: 1.904
batch start
#iterations: 302
currently lose_sum: 87.77114605903625
time_elpased: 1.841
batch start
#iterations: 303
currently lose_sum: 87.61630779504776
time_elpased: 1.895
batch start
#iterations: 304
currently lose_sum: 87.31050103902817
time_elpased: 1.849
batch start
#iterations: 305
currently lose_sum: 87.23220670223236
time_elpased: 1.839
batch start
#iterations: 306
currently lose_sum: 87.34062552452087
time_elpased: 1.872
batch start
#iterations: 307
currently lose_sum: 87.20180732011795
time_elpased: 1.906
batch start
#iterations: 308
currently lose_sum: 87.01494133472443
time_elpased: 1.885
batch start
#iterations: 309
currently lose_sum: 86.89454293251038
time_elpased: 1.866
batch start
#iterations: 310
currently lose_sum: 86.82940328121185
time_elpased: 1.904
batch start
#iterations: 311
currently lose_sum: 86.64310878515244
time_elpased: 1.872
batch start
#iterations: 312
currently lose_sum: 86.85399705171585
time_elpased: 1.848
batch start
#iterations: 313
currently lose_sum: 87.3335874080658
time_elpased: 1.845
batch start
#iterations: 314
currently lose_sum: 87.03804463148117
time_elpased: 1.801
batch start
#iterations: 315
currently lose_sum: 86.60634130239487
time_elpased: 1.853
batch start
#iterations: 316
currently lose_sum: 86.79316622018814
time_elpased: 1.859
batch start
#iterations: 317
currently lose_sum: 86.73635542392731
time_elpased: 1.852
batch start
#iterations: 318
currently lose_sum: 86.99019855260849
time_elpased: 1.874
batch start
#iterations: 319
currently lose_sum: 86.92932093143463
time_elpased: 1.916
start validation test
0.616855670103
0.620438726007
0.605433775857
0.61284441898
0.616875723
65.108
batch start
#iterations: 320
currently lose_sum: 86.42244070768356
time_elpased: 1.891
batch start
#iterations: 321
currently lose_sum: 86.30220454931259
time_elpased: 1.838
batch start
#iterations: 322
currently lose_sum: 86.47365039587021
time_elpased: 1.866
batch start
#iterations: 323
currently lose_sum: 86.55995082855225
time_elpased: 1.88
batch start
#iterations: 324
currently lose_sum: 86.5481829047203
time_elpased: 1.826
batch start
#iterations: 325
currently lose_sum: 85.86534303426743
time_elpased: 1.871
batch start
#iterations: 326
currently lose_sum: 85.61355894804001
time_elpased: 1.829
batch start
#iterations: 327
currently lose_sum: 85.81724923849106
time_elpased: 1.876
batch start
#iterations: 328
currently lose_sum: 86.27767848968506
time_elpased: 1.88
batch start
#iterations: 329
currently lose_sum: 85.75625956058502
time_elpased: 1.813
batch start
#iterations: 330
currently lose_sum: 86.19136625528336
time_elpased: 1.893
batch start
#iterations: 331
currently lose_sum: 85.24389392137527
time_elpased: 1.825
batch start
#iterations: 332
currently lose_sum: 85.49572837352753
time_elpased: 1.845
batch start
#iterations: 333
currently lose_sum: 85.3611376285553
time_elpased: 1.842
batch start
#iterations: 334
currently lose_sum: 85.48339140415192
time_elpased: 1.826
batch start
#iterations: 335
currently lose_sum: 85.50794100761414
time_elpased: 1.844
batch start
#iterations: 336
currently lose_sum: 85.56844305992126
time_elpased: 1.913
batch start
#iterations: 337
currently lose_sum: 85.83337825536728
time_elpased: 1.913
batch start
#iterations: 338
currently lose_sum: 86.04276877641678
time_elpased: 1.874
batch start
#iterations: 339
currently lose_sum: 85.7776380777359
time_elpased: 1.854
start validation test
0.617319587629
0.628574632724
0.576824122672
0.60158849415
0.617390683664
67.675
batch start
#iterations: 340
currently lose_sum: 85.524478495121
time_elpased: 1.858
batch start
#iterations: 341
currently lose_sum: 85.4376425743103
time_elpased: 1.853
batch start
#iterations: 342
currently lose_sum: 84.57140654325485
time_elpased: 1.859
batch start
#iterations: 343
currently lose_sum: 84.80578196048737
time_elpased: 1.902
batch start
#iterations: 344
currently lose_sum: 85.29378753900528
time_elpased: 1.858
batch start
#iterations: 345
currently lose_sum: 85.48164561390877
time_elpased: 1.931
batch start
#iterations: 346
currently lose_sum: 85.1681382060051
time_elpased: 1.864
batch start
#iterations: 347
currently lose_sum: 84.70232969522476
time_elpased: 1.889
batch start
#iterations: 348
currently lose_sum: 83.9832414984703
time_elpased: 1.879
batch start
#iterations: 349
currently lose_sum: 85.09371888637543
time_elpased: 1.885
batch start
#iterations: 350
currently lose_sum: 84.8798241019249
time_elpased: 1.844
batch start
#iterations: 351
currently lose_sum: 84.3755550980568
time_elpased: 1.886
batch start
#iterations: 352
currently lose_sum: 84.23529994487762
time_elpased: 1.888
batch start
#iterations: 353
currently lose_sum: 83.8677327632904
time_elpased: 1.864
batch start
#iterations: 354
currently lose_sum: 84.0009845495224
time_elpased: 1.84
batch start
#iterations: 355
currently lose_sum: 83.7695677280426
time_elpased: 1.85
batch start
#iterations: 356
currently lose_sum: 83.89578318595886
time_elpased: 1.842
batch start
#iterations: 357
currently lose_sum: 83.19179904460907
time_elpased: 1.834
batch start
#iterations: 358
currently lose_sum: 83.35299116373062
time_elpased: 1.877
batch start
#iterations: 359
currently lose_sum: 84.23975467681885
time_elpased: 1.874
start validation test
0.611804123711
0.620907079646
0.577647422044
0.598496561284
0.611864091069
70.340
batch start
#iterations: 360
currently lose_sum: 83.96725100278854
time_elpased: 1.861
batch start
#iterations: 361
currently lose_sum: 83.91447842121124
time_elpased: 1.818
batch start
#iterations: 362
currently lose_sum: 83.08913725614548
time_elpased: 1.886
batch start
#iterations: 363
currently lose_sum: 83.29574012756348
time_elpased: 1.89
batch start
#iterations: 364
currently lose_sum: 82.63057971000671
time_elpased: 1.866
batch start
#iterations: 365
currently lose_sum: 83.67359906435013
time_elpased: 1.846
batch start
#iterations: 366
currently lose_sum: 83.3451840877533
time_elpased: 1.891
batch start
#iterations: 367
currently lose_sum: 83.17683270573616
time_elpased: 1.895
batch start
#iterations: 368
currently lose_sum: 83.02519252896309
time_elpased: 1.901
batch start
#iterations: 369
currently lose_sum: 82.30135709047318
time_elpased: 1.859
batch start
#iterations: 370
currently lose_sum: 82.70326793193817
time_elpased: 1.847
batch start
#iterations: 371
currently lose_sum: 83.14155113697052
time_elpased: 1.867
batch start
#iterations: 372
currently lose_sum: 82.27923265099525
time_elpased: 1.825
batch start
#iterations: 373
currently lose_sum: 82.95117491483688
time_elpased: 1.872
batch start
#iterations: 374
currently lose_sum: 83.27107864618301
time_elpased: 1.87
batch start
#iterations: 375
currently lose_sum: 82.07901817560196
time_elpased: 1.962
batch start
#iterations: 376
currently lose_sum: 82.99972462654114
time_elpased: 1.858
batch start
#iterations: 377
currently lose_sum: 82.37039342522621
time_elpased: 1.849
batch start
#iterations: 378
currently lose_sum: 82.31734126806259
time_elpased: 1.905
batch start
#iterations: 379
currently lose_sum: 81.62273442745209
time_elpased: 1.897
start validation test
0.599845360825
0.634092780675
0.475455387465
0.543433511733
0.600063746609
71.806
batch start
#iterations: 380
currently lose_sum: 81.88842949271202
time_elpased: 1.823
batch start
#iterations: 381
currently lose_sum: 82.15705889463425
time_elpased: 1.899
batch start
#iterations: 382
currently lose_sum: 82.06860163807869
time_elpased: 1.84
batch start
#iterations: 383
currently lose_sum: 81.75566098093987
time_elpased: 1.891
batch start
#iterations: 384
currently lose_sum: 81.58433929085732
time_elpased: 1.887
batch start
#iterations: 385
currently lose_sum: 81.16713505983353
time_elpased: 1.893
batch start
#iterations: 386
currently lose_sum: 80.44998237490654
time_elpased: 1.842
batch start
#iterations: 387
currently lose_sum: 80.7911411523819
time_elpased: 1.838
batch start
#iterations: 388
currently lose_sum: 80.90271219611168
time_elpased: 1.847
batch start
#iterations: 389
currently lose_sum: 80.77916321158409
time_elpased: 1.859
batch start
#iterations: 390
currently lose_sum: 81.23734229803085
time_elpased: 1.899
batch start
#iterations: 391
currently lose_sum: 80.80896946787834
time_elpased: 1.888
batch start
#iterations: 392
currently lose_sum: 81.32290434837341
time_elpased: 1.846
batch start
#iterations: 393
currently lose_sum: 80.1090706884861
time_elpased: 1.889
batch start
#iterations: 394
currently lose_sum: 80.97199240326881
time_elpased: 1.891
batch start
#iterations: 395
currently lose_sum: 80.03377342224121
time_elpased: 1.89
batch start
#iterations: 396
currently lose_sum: 80.12293639779091
time_elpased: 1.871
batch start
#iterations: 397
currently lose_sum: 80.67262408137321
time_elpased: 1.839
batch start
#iterations: 398
currently lose_sum: 79.88086491823196
time_elpased: 1.855
batch start
#iterations: 399
currently lose_sum: 80.0201658308506
time_elpased: 1.839
start validation test
0.59793814433
0.612539626629
0.536894103118
0.572227706482
0.59804531656
75.641
acc: 0.648
pre: 0.660
rec: 0.612
F1: 0.635
auc: 0.648
