start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.4653657078743
time_elpased: 1.41
batch start
#iterations: 1
currently lose_sum: 100.12718719244003
time_elpased: 1.409
batch start
#iterations: 2
currently lose_sum: 99.98333311080933
time_elpased: 1.42
batch start
#iterations: 3
currently lose_sum: 99.8450939655304
time_elpased: 1.389
batch start
#iterations: 4
currently lose_sum: 99.85240495204926
time_elpased: 1.398
batch start
#iterations: 5
currently lose_sum: 99.67356961965561
time_elpased: 1.432
batch start
#iterations: 6
currently lose_sum: 99.60072374343872
time_elpased: 1.415
batch start
#iterations: 7
currently lose_sum: 99.44477117061615
time_elpased: 1.421
batch start
#iterations: 8
currently lose_sum: 99.49600279331207
time_elpased: 1.42
batch start
#iterations: 9
currently lose_sum: 99.45418065786362
time_elpased: 1.396
batch start
#iterations: 10
currently lose_sum: 99.40100181102753
time_elpased: 1.397
batch start
#iterations: 11
currently lose_sum: 99.18504184484482
time_elpased: 1.442
batch start
#iterations: 12
currently lose_sum: 99.48053234815598
time_elpased: 1.389
batch start
#iterations: 13
currently lose_sum: 99.18146413564682
time_elpased: 1.423
batch start
#iterations: 14
currently lose_sum: 99.23309260606766
time_elpased: 1.424
batch start
#iterations: 15
currently lose_sum: 99.02169942855835
time_elpased: 1.416
batch start
#iterations: 16
currently lose_sum: 99.21643894910812
time_elpased: 1.409
batch start
#iterations: 17
currently lose_sum: 99.01168692111969
time_elpased: 1.461
batch start
#iterations: 18
currently lose_sum: 98.723015666008
time_elpased: 1.383
batch start
#iterations: 19
currently lose_sum: 98.7714581489563
time_elpased: 1.393
start validation test
0.627989690722
0.64374425023
0.576103735721
0.608048661272
0.628080784519
64.014
batch start
#iterations: 20
currently lose_sum: 98.58998781442642
time_elpased: 1.382
batch start
#iterations: 21
currently lose_sum: 98.2939122915268
time_elpased: 1.413
batch start
#iterations: 22
currently lose_sum: 98.16386061906815
time_elpased: 1.374
batch start
#iterations: 23
currently lose_sum: 98.04956644773483
time_elpased: 1.404
batch start
#iterations: 24
currently lose_sum: 97.7124302983284
time_elpased: 1.425
batch start
#iterations: 25
currently lose_sum: 97.37151700258255
time_elpased: 1.412
batch start
#iterations: 26
currently lose_sum: 97.26009523868561
time_elpased: 1.381
batch start
#iterations: 27
currently lose_sum: 97.173923432827
time_elpased: 1.439
batch start
#iterations: 28
currently lose_sum: 97.04470121860504
time_elpased: 1.466
batch start
#iterations: 29
currently lose_sum: 97.0756443142891
time_elpased: 1.444
batch start
#iterations: 30
currently lose_sum: 96.57627713680267
time_elpased: 1.461
batch start
#iterations: 31
currently lose_sum: 96.58327490091324
time_elpased: 1.4
batch start
#iterations: 32
currently lose_sum: 95.99180388450623
time_elpased: 1.403
batch start
#iterations: 33
currently lose_sum: 96.30536806583405
time_elpased: 1.429
batch start
#iterations: 34
currently lose_sum: 96.06515169143677
time_elpased: 1.451
batch start
#iterations: 35
currently lose_sum: 96.29527741670609
time_elpased: 1.442
batch start
#iterations: 36
currently lose_sum: 96.25734686851501
time_elpased: 1.395
batch start
#iterations: 37
currently lose_sum: 96.07477283477783
time_elpased: 1.387
batch start
#iterations: 38
currently lose_sum: 95.71541434526443
time_elpased: 1.41
batch start
#iterations: 39
currently lose_sum: 95.84815186262131
time_elpased: 1.422
start validation test
0.646494845361
0.641380674513
0.667387053617
0.65412547912
0.646458165867
61.595
batch start
#iterations: 40
currently lose_sum: 95.62037700414658
time_elpased: 1.422
batch start
#iterations: 41
currently lose_sum: 95.79641503095627
time_elpased: 1.39
batch start
#iterations: 42
currently lose_sum: 95.48806583881378
time_elpased: 1.392
batch start
#iterations: 43
currently lose_sum: 95.24732333421707
time_elpased: 1.434
batch start
#iterations: 44
currently lose_sum: 95.60590732097626
time_elpased: 1.428
batch start
#iterations: 45
currently lose_sum: 95.18650013208389
time_elpased: 1.398
batch start
#iterations: 46
currently lose_sum: 94.97885674238205
time_elpased: 1.407
batch start
#iterations: 47
currently lose_sum: 94.8653633594513
time_elpased: 1.396
batch start
#iterations: 48
currently lose_sum: 95.39420264959335
time_elpased: 1.443
batch start
#iterations: 49
currently lose_sum: 95.38549089431763
time_elpased: 1.421
batch start
#iterations: 50
currently lose_sum: 95.68237656354904
time_elpased: 1.394
batch start
#iterations: 51
currently lose_sum: 94.76166439056396
time_elpased: 1.404
batch start
#iterations: 52
currently lose_sum: 94.82906132936478
time_elpased: 1.403
batch start
#iterations: 53
currently lose_sum: 94.9579701423645
time_elpased: 1.415
batch start
#iterations: 54
currently lose_sum: 95.33446967601776
time_elpased: 1.43
batch start
#iterations: 55
currently lose_sum: 94.77059084177017
time_elpased: 1.462
batch start
#iterations: 56
currently lose_sum: 95.22237515449524
time_elpased: 1.422
batch start
#iterations: 57
currently lose_sum: 94.48165029287338
time_elpased: 1.415
batch start
#iterations: 58
currently lose_sum: 94.91819894313812
time_elpased: 1.393
batch start
#iterations: 59
currently lose_sum: 94.53657710552216
time_elpased: 1.4
start validation test
0.64912371134
0.638835877863
0.688998662139
0.662969747982
0.649053704714
61.243
batch start
#iterations: 60
currently lose_sum: 95.15159803628922
time_elpased: 1.426
batch start
#iterations: 61
currently lose_sum: 95.04403549432755
time_elpased: 1.388
batch start
#iterations: 62
currently lose_sum: 94.91865068674088
time_elpased: 1.427
batch start
#iterations: 63
currently lose_sum: 94.87310934066772
time_elpased: 1.413
batch start
#iterations: 64
currently lose_sum: 95.15139019489288
time_elpased: 1.413
batch start
#iterations: 65
currently lose_sum: 94.5706142783165
time_elpased: 1.432
batch start
#iterations: 66
currently lose_sum: 94.07522261142731
time_elpased: 1.436
batch start
#iterations: 67
currently lose_sum: 94.0178314447403
time_elpased: 1.408
batch start
#iterations: 68
currently lose_sum: 94.62720662355423
time_elpased: 1.432
batch start
#iterations: 69
currently lose_sum: 94.75662875175476
time_elpased: 1.429
batch start
#iterations: 70
currently lose_sum: 94.33335399627686
time_elpased: 1.409
batch start
#iterations: 71
currently lose_sum: 94.22485107183456
time_elpased: 1.418
batch start
#iterations: 72
currently lose_sum: 93.74990344047546
time_elpased: 1.474
batch start
#iterations: 73
currently lose_sum: 93.84131383895874
time_elpased: 1.435
batch start
#iterations: 74
currently lose_sum: 93.87061393260956
time_elpased: 1.424
batch start
#iterations: 75
currently lose_sum: 94.01944208145142
time_elpased: 1.432
batch start
#iterations: 76
currently lose_sum: 94.24149322509766
time_elpased: 1.443
batch start
#iterations: 77
currently lose_sum: 94.21041762828827
time_elpased: 1.436
batch start
#iterations: 78
currently lose_sum: 93.97005617618561
time_elpased: 1.429
batch start
#iterations: 79
currently lose_sum: 94.38967788219452
time_elpased: 1.45
start validation test
0.649226804124
0.642270861833
0.676443346712
0.658914340133
0.649179021285
60.914
batch start
#iterations: 80
currently lose_sum: 93.73277938365936
time_elpased: 1.416
batch start
#iterations: 81
currently lose_sum: 94.49407809972763
time_elpased: 1.418
batch start
#iterations: 82
currently lose_sum: 93.93945449590683
time_elpased: 1.413
batch start
#iterations: 83
currently lose_sum: 94.49426811933517
time_elpased: 1.434
batch start
#iterations: 84
currently lose_sum: 93.84833037853241
time_elpased: 1.416
batch start
#iterations: 85
currently lose_sum: 94.26861536502838
time_elpased: 1.433
batch start
#iterations: 86
currently lose_sum: 93.91015380620956
time_elpased: 1.413
batch start
#iterations: 87
currently lose_sum: 93.77859979867935
time_elpased: 1.413
batch start
#iterations: 88
currently lose_sum: 93.94626063108444
time_elpased: 1.4
batch start
#iterations: 89
currently lose_sum: 93.6370056271553
time_elpased: 1.417
batch start
#iterations: 90
currently lose_sum: 93.89410275220871
time_elpased: 1.414
batch start
#iterations: 91
currently lose_sum: 93.57353115081787
time_elpased: 1.4
batch start
#iterations: 92
currently lose_sum: 93.48769772052765
time_elpased: 1.405
batch start
#iterations: 93
currently lose_sum: 93.37284088134766
time_elpased: 1.436
batch start
#iterations: 94
currently lose_sum: 93.73766231536865
time_elpased: 1.406
batch start
#iterations: 95
currently lose_sum: 93.83850032091141
time_elpased: 1.397
batch start
#iterations: 96
currently lose_sum: 93.41892617940903
time_elpased: 1.434
batch start
#iterations: 97
currently lose_sum: 93.40686321258545
time_elpased: 1.416
batch start
#iterations: 98
currently lose_sum: 93.46415984630585
time_elpased: 1.402
batch start
#iterations: 99
currently lose_sum: 93.71233612298965
time_elpased: 1.401
start validation test
0.650979381443
0.643315820198
0.680456931152
0.661365341335
0.650927629058
60.580
batch start
#iterations: 100
currently lose_sum: 93.33029222488403
time_elpased: 1.41
batch start
#iterations: 101
currently lose_sum: 93.53423857688904
time_elpased: 1.386
batch start
#iterations: 102
currently lose_sum: 93.58426803350449
time_elpased: 1.396
batch start
#iterations: 103
currently lose_sum: 93.49530678987503
time_elpased: 1.369
batch start
#iterations: 104
currently lose_sum: 92.95927000045776
time_elpased: 1.413
batch start
#iterations: 105
currently lose_sum: 93.1560634970665
time_elpased: 1.378
batch start
#iterations: 106
currently lose_sum: 93.42957949638367
time_elpased: 1.409
batch start
#iterations: 107
currently lose_sum: 93.00668275356293
time_elpased: 1.407
batch start
#iterations: 108
currently lose_sum: 92.91503167152405
time_elpased: 1.457
batch start
#iterations: 109
currently lose_sum: 92.90526807308197
time_elpased: 1.448
batch start
#iterations: 110
currently lose_sum: 93.31057113409042
time_elpased: 1.433
batch start
#iterations: 111
currently lose_sum: 93.49379742145538
time_elpased: 1.426
batch start
#iterations: 112
currently lose_sum: 92.98882043361664
time_elpased: 1.448
batch start
#iterations: 113
currently lose_sum: 92.7451901435852
time_elpased: 1.397
batch start
#iterations: 114
currently lose_sum: 93.17714023590088
time_elpased: 1.398
batch start
#iterations: 115
currently lose_sum: 93.0832691192627
time_elpased: 1.43
batch start
#iterations: 116
currently lose_sum: 92.88636124134064
time_elpased: 1.431
batch start
#iterations: 117
currently lose_sum: 93.0128487944603
time_elpased: 1.389
batch start
#iterations: 118
currently lose_sum: 92.8245159983635
time_elpased: 1.414
batch start
#iterations: 119
currently lose_sum: 92.92457067966461
time_elpased: 1.408
start validation test
0.647783505155
0.642828843106
0.667901615725
0.655125422702
0.647748184709
60.356
batch start
#iterations: 120
currently lose_sum: 92.9301747083664
time_elpased: 1.447
batch start
#iterations: 121
currently lose_sum: 92.86938709020615
time_elpased: 1.395
batch start
#iterations: 122
currently lose_sum: 92.69559794664383
time_elpased: 1.441
batch start
#iterations: 123
currently lose_sum: 92.94092607498169
time_elpased: 1.408
batch start
#iterations: 124
currently lose_sum: 92.98655891418457
time_elpased: 1.412
batch start
#iterations: 125
currently lose_sum: 92.94274747371674
time_elpased: 1.399
batch start
#iterations: 126
currently lose_sum: 92.92978692054749
time_elpased: 1.43
batch start
#iterations: 127
currently lose_sum: 93.05196326971054
time_elpased: 1.42
batch start
#iterations: 128
currently lose_sum: 92.73163139820099
time_elpased: 1.408
batch start
#iterations: 129
currently lose_sum: 92.57979875802994
time_elpased: 1.394
batch start
#iterations: 130
currently lose_sum: 92.42114591598511
time_elpased: 1.447
batch start
#iterations: 131
currently lose_sum: 92.78743463754654
time_elpased: 1.414
batch start
#iterations: 132
currently lose_sum: 92.53095161914825
time_elpased: 1.4
batch start
#iterations: 133
currently lose_sum: 92.75355517864227
time_elpased: 1.41
batch start
#iterations: 134
currently lose_sum: 92.14705294370651
time_elpased: 1.426
batch start
#iterations: 135
currently lose_sum: 92.88843876123428
time_elpased: 1.388
batch start
#iterations: 136
currently lose_sum: 92.77145832777023
time_elpased: 1.41
batch start
#iterations: 137
currently lose_sum: 92.21417278051376
time_elpased: 1.441
batch start
#iterations: 138
currently lose_sum: 92.06996893882751
time_elpased: 1.449
batch start
#iterations: 139
currently lose_sum: 92.73046791553497
time_elpased: 1.411
start validation test
0.642319587629
0.643254950495
0.641864773078
0.642559109875
0.642320386126
60.670
batch start
#iterations: 140
currently lose_sum: 92.77885687351227
time_elpased: 1.433
batch start
#iterations: 141
currently lose_sum: 92.6670588850975
time_elpased: 1.406
batch start
#iterations: 142
currently lose_sum: 92.17802572250366
time_elpased: 1.397
batch start
#iterations: 143
currently lose_sum: 92.41144067049026
time_elpased: 1.404
batch start
#iterations: 144
currently lose_sum: 92.12460279464722
time_elpased: 1.382
batch start
#iterations: 145
currently lose_sum: 92.52327364683151
time_elpased: 1.413
batch start
#iterations: 146
currently lose_sum: 92.40141642093658
time_elpased: 1.398
batch start
#iterations: 147
currently lose_sum: 92.38293290138245
time_elpased: 1.414
batch start
#iterations: 148
currently lose_sum: 92.06356471776962
time_elpased: 1.403
batch start
#iterations: 149
currently lose_sum: 92.82015532255173
time_elpased: 1.405
batch start
#iterations: 150
currently lose_sum: 92.50836688280106
time_elpased: 1.394
batch start
#iterations: 151
currently lose_sum: 92.46866887807846
time_elpased: 1.396
batch start
#iterations: 152
currently lose_sum: 92.15834993124008
time_elpased: 1.4
batch start
#iterations: 153
currently lose_sum: 92.43401747941971
time_elpased: 1.387
batch start
#iterations: 154
currently lose_sum: 92.2393810749054
time_elpased: 1.418
batch start
#iterations: 155
currently lose_sum: 92.12712836265564
time_elpased: 1.439
batch start
#iterations: 156
currently lose_sum: 91.85106635093689
time_elpased: 1.411
batch start
#iterations: 157
currently lose_sum: 91.97566372156143
time_elpased: 1.409
batch start
#iterations: 158
currently lose_sum: 92.53014808893204
time_elpased: 1.417
batch start
#iterations: 159
currently lose_sum: 92.67513424158096
time_elpased: 1.399
start validation test
0.647164948454
0.646980552712
0.650509416487
0.648740185765
0.647159076724
60.260
batch start
#iterations: 160
currently lose_sum: 92.65169650316238
time_elpased: 1.394
batch start
#iterations: 161
currently lose_sum: 91.77498263120651
time_elpased: 1.393
batch start
#iterations: 162
currently lose_sum: 91.8016699552536
time_elpased: 1.399
batch start
#iterations: 163
currently lose_sum: 91.97399079799652
time_elpased: 1.4
batch start
#iterations: 164
currently lose_sum: 92.17576318979263
time_elpased: 1.411
batch start
#iterations: 165
currently lose_sum: 92.11406743526459
time_elpased: 1.447
batch start
#iterations: 166
currently lose_sum: 92.15316194295883
time_elpased: 1.38
batch start
#iterations: 167
currently lose_sum: 91.63770699501038
time_elpased: 1.408
batch start
#iterations: 168
currently lose_sum: 91.86190778017044
time_elpased: 1.396
batch start
#iterations: 169
currently lose_sum: 91.87923407554626
time_elpased: 1.412
batch start
#iterations: 170
currently lose_sum: 91.66258257627487
time_elpased: 1.388
batch start
#iterations: 171
currently lose_sum: 91.59798300266266
time_elpased: 1.449
batch start
#iterations: 172
currently lose_sum: 91.33648401498795
time_elpased: 1.39
batch start
#iterations: 173
currently lose_sum: 91.47742629051208
time_elpased: 1.402
batch start
#iterations: 174
currently lose_sum: 91.54883927106857
time_elpased: 1.439
batch start
#iterations: 175
currently lose_sum: 92.14910215139389
time_elpased: 1.387
batch start
#iterations: 176
currently lose_sum: 91.51918095350266
time_elpased: 1.411
batch start
#iterations: 177
currently lose_sum: 91.91823107004166
time_elpased: 1.397
batch start
#iterations: 178
currently lose_sum: 91.38379848003387
time_elpased: 1.436
batch start
#iterations: 179
currently lose_sum: 91.51801037788391
time_elpased: 1.428
start validation test
0.651855670103
0.652214116922
0.653288051868
0.652750642674
0.651853155336
59.979
batch start
#iterations: 180
currently lose_sum: 91.59245258569717
time_elpased: 1.392
batch start
#iterations: 181
currently lose_sum: 91.11749082803726
time_elpased: 1.382
batch start
#iterations: 182
currently lose_sum: 91.9607789516449
time_elpased: 1.44
batch start
#iterations: 183
currently lose_sum: 91.25690186023712
time_elpased: 1.379
batch start
#iterations: 184
currently lose_sum: 91.54013812541962
time_elpased: 1.445
batch start
#iterations: 185
currently lose_sum: 91.19620209932327
time_elpased: 1.405
batch start
#iterations: 186
currently lose_sum: 90.93231177330017
time_elpased: 1.396
batch start
#iterations: 187
currently lose_sum: 91.39628541469574
time_elpased: 1.404
batch start
#iterations: 188
currently lose_sum: 91.28869307041168
time_elpased: 1.427
batch start
#iterations: 189
currently lose_sum: 91.26986652612686
time_elpased: 1.405
batch start
#iterations: 190
currently lose_sum: 91.24386596679688
time_elpased: 1.406
batch start
#iterations: 191
currently lose_sum: 90.95496499538422
time_elpased: 1.394
batch start
#iterations: 192
currently lose_sum: 90.97284817695618
time_elpased: 1.429
batch start
#iterations: 193
currently lose_sum: 90.85055756568909
time_elpased: 1.431
batch start
#iterations: 194
currently lose_sum: 91.32183176279068
time_elpased: 1.407
batch start
#iterations: 195
currently lose_sum: 91.15627735853195
time_elpased: 1.4
batch start
#iterations: 196
currently lose_sum: 91.22164380550385
time_elpased: 1.461
batch start
#iterations: 197
currently lose_sum: 91.18049430847168
time_elpased: 1.41
batch start
#iterations: 198
currently lose_sum: 91.02890229225159
time_elpased: 1.414
batch start
#iterations: 199
currently lose_sum: 91.39275324344635
time_elpased: 1.402
start validation test
0.638092783505
0.646904969486
0.610888134198
0.628380881808
0.638140545463
60.553
batch start
#iterations: 200
currently lose_sum: 91.37024849653244
time_elpased: 1.408
batch start
#iterations: 201
currently lose_sum: 90.8572325706482
time_elpased: 1.411
batch start
#iterations: 202
currently lose_sum: 91.06089496612549
time_elpased: 1.415
batch start
#iterations: 203
currently lose_sum: 91.49353367090225
time_elpased: 1.409
batch start
#iterations: 204
currently lose_sum: 91.10690921545029
time_elpased: 1.42
batch start
#iterations: 205
currently lose_sum: 90.93962347507477
time_elpased: 1.407
batch start
#iterations: 206
currently lose_sum: 90.93019807338715
time_elpased: 1.42
batch start
#iterations: 207
currently lose_sum: 90.95991587638855
time_elpased: 1.393
batch start
#iterations: 208
currently lose_sum: 90.93709498643875
time_elpased: 1.413
batch start
#iterations: 209
currently lose_sum: 90.90549451112747
time_elpased: 1.432
batch start
#iterations: 210
currently lose_sum: 90.77282798290253
time_elpased: 1.413
batch start
#iterations: 211
currently lose_sum: 90.85566693544388
time_elpased: 1.411
batch start
#iterations: 212
currently lose_sum: 90.8333169221878
time_elpased: 1.449
batch start
#iterations: 213
currently lose_sum: 90.50621592998505
time_elpased: 1.407
batch start
#iterations: 214
currently lose_sum: 90.45771318674088
time_elpased: 1.41
batch start
#iterations: 215
currently lose_sum: 90.88892155885696
time_elpased: 1.414
batch start
#iterations: 216
currently lose_sum: 90.96777808666229
time_elpased: 1.427
batch start
#iterations: 217
currently lose_sum: 90.65284550189972
time_elpased: 1.396
batch start
#iterations: 218
currently lose_sum: 90.79115891456604
time_elpased: 1.394
batch start
#iterations: 219
currently lose_sum: 90.97075098752975
time_elpased: 1.417
start validation test
0.64087628866
0.645656779661
0.627251209221
0.636320927076
0.640900209588
60.243
batch start
#iterations: 220
currently lose_sum: 91.06119322776794
time_elpased: 1.397
batch start
#iterations: 221
currently lose_sum: 90.93644243478775
time_elpased: 1.398
batch start
#iterations: 222
currently lose_sum: 90.56220752000809
time_elpased: 1.426
batch start
#iterations: 223
currently lose_sum: 90.31934720277786
time_elpased: 1.432
batch start
#iterations: 224
currently lose_sum: 90.74557435512543
time_elpased: 1.432
batch start
#iterations: 225
currently lose_sum: 90.97116249799728
time_elpased: 1.404
batch start
#iterations: 226
currently lose_sum: 90.49703669548035
time_elpased: 1.381
batch start
#iterations: 227
currently lose_sum: 90.49612873792648
time_elpased: 1.412
batch start
#iterations: 228
currently lose_sum: 90.14159506559372
time_elpased: 1.399
batch start
#iterations: 229
currently lose_sum: 89.96746706962585
time_elpased: 1.399
batch start
#iterations: 230
currently lose_sum: 89.90841752290726
time_elpased: 1.429
batch start
#iterations: 231
currently lose_sum: 89.82988387346268
time_elpased: 1.417
batch start
#iterations: 232
currently lose_sum: 90.08845937252045
time_elpased: 1.413
batch start
#iterations: 233
currently lose_sum: 90.13869100809097
time_elpased: 1.425
batch start
#iterations: 234
currently lose_sum: 90.5687313079834
time_elpased: 1.403
batch start
#iterations: 235
currently lose_sum: 89.95097535848618
time_elpased: 1.41
batch start
#iterations: 236
currently lose_sum: 90.23311746120453
time_elpased: 1.424
batch start
#iterations: 237
currently lose_sum: 90.12773472070694
time_elpased: 1.422
batch start
#iterations: 238
currently lose_sum: 89.94890373945236
time_elpased: 1.425
batch start
#iterations: 239
currently lose_sum: 89.90034586191177
time_elpased: 1.449
start validation test
0.638762886598
0.642593957259
0.628177421015
0.635303913405
0.638781471015
60.490
batch start
#iterations: 240
currently lose_sum: 89.86888527870178
time_elpased: 1.439
batch start
#iterations: 241
currently lose_sum: 90.01516842842102
time_elpased: 1.404
batch start
#iterations: 242
currently lose_sum: 89.97825914621353
time_elpased: 1.407
batch start
#iterations: 243
currently lose_sum: 90.01108425855637
time_elpased: 1.427
batch start
#iterations: 244
currently lose_sum: 90.5926383137703
time_elpased: 1.425
batch start
#iterations: 245
currently lose_sum: 90.34803205728531
time_elpased: 1.396
batch start
#iterations: 246
currently lose_sum: 90.36469703912735
time_elpased: 1.427
batch start
#iterations: 247
currently lose_sum: 89.91377431154251
time_elpased: 1.421
batch start
#iterations: 248
currently lose_sum: 90.21754878759384
time_elpased: 1.399
batch start
#iterations: 249
currently lose_sum: 89.99759954214096
time_elpased: 1.441
batch start
#iterations: 250
currently lose_sum: 90.41326022148132
time_elpased: 1.43
batch start
#iterations: 251
currently lose_sum: 89.53775453567505
time_elpased: 1.402
batch start
#iterations: 252
currently lose_sum: 90.1117593050003
time_elpased: 1.429
batch start
#iterations: 253
currently lose_sum: 89.85490715503693
time_elpased: 1.436
batch start
#iterations: 254
currently lose_sum: 89.80386763811111
time_elpased: 1.444
batch start
#iterations: 255
currently lose_sum: 89.78394603729248
time_elpased: 1.395
batch start
#iterations: 256
currently lose_sum: 89.76242101192474
time_elpased: 1.42
batch start
#iterations: 257
currently lose_sum: 90.05825144052505
time_elpased: 1.402
batch start
#iterations: 258
currently lose_sum: 89.67055743932724
time_elpased: 1.413
batch start
#iterations: 259
currently lose_sum: 89.158007979393
time_elpased: 1.403
start validation test
0.639896907216
0.642106358622
0.634969640836
0.638518058574
0.639905557793
60.444
batch start
#iterations: 260
currently lose_sum: 89.23435336351395
time_elpased: 1.493
batch start
#iterations: 261
currently lose_sum: 89.69326585531235
time_elpased: 1.409
batch start
#iterations: 262
currently lose_sum: 89.69161105155945
time_elpased: 1.423
batch start
#iterations: 263
currently lose_sum: 89.18697983026505
time_elpased: 1.418
batch start
#iterations: 264
currently lose_sum: 89.6824808716774
time_elpased: 1.409
batch start
#iterations: 265
currently lose_sum: 89.05955851078033
time_elpased: 1.395
batch start
#iterations: 266
currently lose_sum: 89.99554562568665
time_elpased: 1.399
batch start
#iterations: 267
currently lose_sum: 89.63951420783997
time_elpased: 1.415
batch start
#iterations: 268
currently lose_sum: 89.66493046283722
time_elpased: 1.392
batch start
#iterations: 269
currently lose_sum: 89.28160631656647
time_elpased: 1.456
batch start
#iterations: 270
currently lose_sum: 89.77094686031342
time_elpased: 1.406
batch start
#iterations: 271
currently lose_sum: 89.8522065281868
time_elpased: 1.418
batch start
#iterations: 272
currently lose_sum: 89.4435406923294
time_elpased: 1.417
batch start
#iterations: 273
currently lose_sum: 89.48584651947021
time_elpased: 1.418
batch start
#iterations: 274
currently lose_sum: 89.11513429880142
time_elpased: 1.404
batch start
#iterations: 275
currently lose_sum: 88.99260437488556
time_elpased: 1.423
batch start
#iterations: 276
currently lose_sum: 89.7128843665123
time_elpased: 1.4
batch start
#iterations: 277
currently lose_sum: 89.63206225633621
time_elpased: 1.454
batch start
#iterations: 278
currently lose_sum: 89.96215325593948
time_elpased: 1.398
batch start
#iterations: 279
currently lose_sum: 89.37430495023727
time_elpased: 1.424
start validation test
0.642164948454
0.65158964274
0.613769682001
0.632114467409
0.642214800724
60.298
batch start
#iterations: 280
currently lose_sum: 89.49553430080414
time_elpased: 1.422
batch start
#iterations: 281
currently lose_sum: 89.65714764595032
time_elpased: 1.432
batch start
#iterations: 282
currently lose_sum: 89.67359858751297
time_elpased: 1.399
batch start
#iterations: 283
currently lose_sum: 89.4035342335701
time_elpased: 1.408
batch start
#iterations: 284
currently lose_sum: 88.72357559204102
time_elpased: 1.425
batch start
#iterations: 285
currently lose_sum: 88.9496151804924
time_elpased: 1.397
batch start
#iterations: 286
currently lose_sum: 88.80690604448318
time_elpased: 1.431
batch start
#iterations: 287
currently lose_sum: 89.07448410987854
time_elpased: 1.425
batch start
#iterations: 288
currently lose_sum: 89.01739364862442
time_elpased: 1.393
batch start
#iterations: 289
currently lose_sum: 89.1888233423233
time_elpased: 1.395
batch start
#iterations: 290
currently lose_sum: 89.13192021846771
time_elpased: 1.422
batch start
#iterations: 291
currently lose_sum: 88.79400700330734
time_elpased: 1.402
batch start
#iterations: 292
currently lose_sum: 89.05306261777878
time_elpased: 1.45
batch start
#iterations: 293
currently lose_sum: 88.70910263061523
time_elpased: 1.406
batch start
#iterations: 294
currently lose_sum: 88.53950786590576
time_elpased: 1.441
batch start
#iterations: 295
currently lose_sum: 88.76983261108398
time_elpased: 1.388
batch start
#iterations: 296
currently lose_sum: 88.51389294862747
time_elpased: 1.422
batch start
#iterations: 297
currently lose_sum: 88.83179479837418
time_elpased: 1.446
batch start
#iterations: 298
currently lose_sum: 88.86460047960281
time_elpased: 1.44
batch start
#iterations: 299
currently lose_sum: 88.64054322242737
time_elpased: 1.396
start validation test
0.631907216495
0.653296834087
0.564886281774
0.605883326894
0.632024882083
60.927
batch start
#iterations: 300
currently lose_sum: 89.17979407310486
time_elpased: 1.405
batch start
#iterations: 301
currently lose_sum: 89.19855844974518
time_elpased: 1.405
batch start
#iterations: 302
currently lose_sum: 89.2759957909584
time_elpased: 1.438
batch start
#iterations: 303
currently lose_sum: 88.66452759504318
time_elpased: 1.401
batch start
#iterations: 304
currently lose_sum: 88.61279571056366
time_elpased: 1.436
batch start
#iterations: 305
currently lose_sum: 88.58776038885117
time_elpased: 1.408
batch start
#iterations: 306
currently lose_sum: 88.77294725179672
time_elpased: 1.426
batch start
#iterations: 307
currently lose_sum: 88.72018331289291
time_elpased: 1.414
batch start
#iterations: 308
currently lose_sum: 88.72565519809723
time_elpased: 1.413
batch start
#iterations: 309
currently lose_sum: 88.3491587638855
time_elpased: 1.454
batch start
#iterations: 310
currently lose_sum: 88.72042673826218
time_elpased: 1.407
batch start
#iterations: 311
currently lose_sum: 88.833451628685
time_elpased: 1.436
batch start
#iterations: 312
currently lose_sum: 88.34348368644714
time_elpased: 1.431
batch start
#iterations: 313
currently lose_sum: 88.12299710512161
time_elpased: 1.385
batch start
#iterations: 314
currently lose_sum: 88.93551123142242
time_elpased: 1.416
batch start
#iterations: 315
currently lose_sum: 88.98454731702805
time_elpased: 1.403
batch start
#iterations: 316
currently lose_sum: 88.6208848953247
time_elpased: 1.446
batch start
#iterations: 317
currently lose_sum: 88.75251311063766
time_elpased: 1.426
batch start
#iterations: 318
currently lose_sum: 88.47762197256088
time_elpased: 1.392
batch start
#iterations: 319
currently lose_sum: 88.66488355398178
time_elpased: 1.471
start validation test
0.635463917526
0.649081276068
0.592569723166
0.619539487842
0.6355392249
60.776
batch start
#iterations: 320
currently lose_sum: 88.55730026960373
time_elpased: 1.41
batch start
#iterations: 321
currently lose_sum: 88.76620203256607
time_elpased: 1.438
batch start
#iterations: 322
currently lose_sum: 88.44306689500809
time_elpased: 1.392
batch start
#iterations: 323
currently lose_sum: 88.5434547662735
time_elpased: 1.407
batch start
#iterations: 324
currently lose_sum: 88.93520963191986
time_elpased: 1.393
batch start
#iterations: 325
currently lose_sum: 88.60535418987274
time_elpased: 1.428
batch start
#iterations: 326
currently lose_sum: 88.21741825342178
time_elpased: 1.45
batch start
#iterations: 327
currently lose_sum: 88.56223738193512
time_elpased: 1.394
batch start
#iterations: 328
currently lose_sum: 87.94092297554016
time_elpased: 1.409
batch start
#iterations: 329
currently lose_sum: 87.97962045669556
time_elpased: 1.422
batch start
#iterations: 330
currently lose_sum: 88.40937215089798
time_elpased: 1.405
batch start
#iterations: 331
currently lose_sum: 88.38470709323883
time_elpased: 1.42
batch start
#iterations: 332
currently lose_sum: 87.83838206529617
time_elpased: 1.392
batch start
#iterations: 333
currently lose_sum: 87.60496664047241
time_elpased: 1.421
batch start
#iterations: 334
currently lose_sum: 88.31135427951813
time_elpased: 1.422
batch start
#iterations: 335
currently lose_sum: 88.35083109140396
time_elpased: 1.382
batch start
#iterations: 336
currently lose_sum: 88.16558760404587
time_elpased: 1.423
batch start
#iterations: 337
currently lose_sum: 88.3892325758934
time_elpased: 1.411
batch start
#iterations: 338
currently lose_sum: 88.41479271650314
time_elpased: 1.413
batch start
#iterations: 339
currently lose_sum: 87.77106368541718
time_elpased: 1.39
start validation test
0.632525773196
0.647649475126
0.5841309046
0.614252475515
0.632610737852
60.890
batch start
#iterations: 340
currently lose_sum: 87.82964062690735
time_elpased: 1.443
batch start
#iterations: 341
currently lose_sum: 87.99690407514572
time_elpased: 1.414
batch start
#iterations: 342
currently lose_sum: 87.8382078409195
time_elpased: 1.436
batch start
#iterations: 343
currently lose_sum: 88.33919489383698
time_elpased: 1.405
batch start
#iterations: 344
currently lose_sum: 87.74442774057388
time_elpased: 1.428
batch start
#iterations: 345
currently lose_sum: 87.93398922681808
time_elpased: 1.419
batch start
#iterations: 346
currently lose_sum: 87.38682872056961
time_elpased: 1.438
batch start
#iterations: 347
currently lose_sum: 88.4028884768486
time_elpased: 1.418
batch start
#iterations: 348
currently lose_sum: 88.30923759937286
time_elpased: 1.423
batch start
#iterations: 349
currently lose_sum: 88.34843093156815
time_elpased: 1.417
batch start
#iterations: 350
currently lose_sum: 87.94927948713303
time_elpased: 1.418
batch start
#iterations: 351
currently lose_sum: 87.86283177137375
time_elpased: 1.455
batch start
#iterations: 352
currently lose_sum: 87.8133916258812
time_elpased: 1.437
batch start
#iterations: 353
currently lose_sum: 88.18527191877365
time_elpased: 1.403
batch start
#iterations: 354
currently lose_sum: 88.02107298374176
time_elpased: 1.433
batch start
#iterations: 355
currently lose_sum: 88.30314964056015
time_elpased: 1.414
batch start
#iterations: 356
currently lose_sum: 88.1040729880333
time_elpased: 1.455
batch start
#iterations: 357
currently lose_sum: 87.70582509040833
time_elpased: 1.42
batch start
#iterations: 358
currently lose_sum: 88.33806377649307
time_elpased: 1.471
batch start
#iterations: 359
currently lose_sum: 87.49599128961563
time_elpased: 1.421
start validation test
0.633092783505
0.64907651715
0.582278481013
0.613865682977
0.633181995851
61.189
batch start
#iterations: 360
currently lose_sum: 87.67399096488953
time_elpased: 1.424
batch start
#iterations: 361
currently lose_sum: 88.04499500989914
time_elpased: 1.438
batch start
#iterations: 362
currently lose_sum: 87.00886243581772
time_elpased: 1.447
batch start
#iterations: 363
currently lose_sum: 87.47511035203934
time_elpased: 1.415
batch start
#iterations: 364
currently lose_sum: 87.77112835645676
time_elpased: 1.397
batch start
#iterations: 365
currently lose_sum: 87.50334054231644
time_elpased: 1.4
batch start
#iterations: 366
currently lose_sum: 87.09306675195694
time_elpased: 1.39
batch start
#iterations: 367
currently lose_sum: 87.70595610141754
time_elpased: 1.411
batch start
#iterations: 368
currently lose_sum: 87.3517958521843
time_elpased: 1.432
batch start
#iterations: 369
currently lose_sum: 87.6349167227745
time_elpased: 1.384
batch start
#iterations: 370
currently lose_sum: 87.46644282341003
time_elpased: 1.377
batch start
#iterations: 371
currently lose_sum: 87.39972740411758
time_elpased: 1.388
batch start
#iterations: 372
currently lose_sum: 87.33774399757385
time_elpased: 1.407
batch start
#iterations: 373
currently lose_sum: 87.40204858779907
time_elpased: 1.392
batch start
#iterations: 374
currently lose_sum: 87.14601808786392
time_elpased: 1.417
batch start
#iterations: 375
currently lose_sum: 87.73104244470596
time_elpased: 1.405
batch start
#iterations: 376
currently lose_sum: 87.42563563585281
time_elpased: 1.416
batch start
#iterations: 377
currently lose_sum: 87.55043971538544
time_elpased: 1.411
batch start
#iterations: 378
currently lose_sum: 87.73732453584671
time_elpased: 1.41
batch start
#iterations: 379
currently lose_sum: 87.10449409484863
time_elpased: 1.413
start validation test
0.633402061856
0.651612152252
0.576103735721
0.611535940572
0.633502657905
61.159
batch start
#iterations: 380
currently lose_sum: 86.98180538415909
time_elpased: 1.396
batch start
#iterations: 381
currently lose_sum: 86.62489926815033
time_elpased: 1.401
batch start
#iterations: 382
currently lose_sum: 86.99041783809662
time_elpased: 1.428
batch start
#iterations: 383
currently lose_sum: 87.42473685741425
time_elpased: 1.469
batch start
#iterations: 384
currently lose_sum: 87.3726476430893
time_elpased: 1.415
batch start
#iterations: 385
currently lose_sum: 87.12799924612045
time_elpased: 1.423
batch start
#iterations: 386
currently lose_sum: 87.20992904901505
time_elpased: 1.402
batch start
#iterations: 387
currently lose_sum: 87.63035523891449
time_elpased: 1.418
batch start
#iterations: 388
currently lose_sum: 87.37139970064163
time_elpased: 1.499
batch start
#iterations: 389
currently lose_sum: 87.53206932544708
time_elpased: 1.396
batch start
#iterations: 390
currently lose_sum: 86.94948494434357
time_elpased: 1.439
batch start
#iterations: 391
currently lose_sum: 87.56500375270844
time_elpased: 1.438
batch start
#iterations: 392
currently lose_sum: 87.60875505208969
time_elpased: 1.389
batch start
#iterations: 393
currently lose_sum: 87.08080518245697
time_elpased: 1.388
batch start
#iterations: 394
currently lose_sum: 87.2393581867218
time_elpased: 1.389
batch start
#iterations: 395
currently lose_sum: 87.15036731958389
time_elpased: 1.403
batch start
#iterations: 396
currently lose_sum: 86.98435389995575
time_elpased: 1.422
batch start
#iterations: 397
currently lose_sum: 86.47879594564438
time_elpased: 1.393
batch start
#iterations: 398
currently lose_sum: 86.3888293504715
time_elpased: 1.434
batch start
#iterations: 399
currently lose_sum: 86.76419007778168
time_elpased: 1.401
start validation test
0.627474226804
0.643136353185
0.575692086035
0.607548194407
0.627565138339
61.601
acc: 0.651
pre: 0.651
rec: 0.652
F1: 0.652
auc: 0.651
