start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.39915126562119
time_elpased: 1.572
batch start
#iterations: 1
currently lose_sum: 100.1405138373375
time_elpased: 1.553
batch start
#iterations: 2
currently lose_sum: 99.8715483546257
time_elpased: 1.606
batch start
#iterations: 3
currently lose_sum: 99.75496077537537
time_elpased: 1.558
batch start
#iterations: 4
currently lose_sum: 99.68422025442123
time_elpased: 1.529
batch start
#iterations: 5
currently lose_sum: 99.71243649721146
time_elpased: 1.601
batch start
#iterations: 6
currently lose_sum: 99.43611854314804
time_elpased: 1.6
batch start
#iterations: 7
currently lose_sum: 99.4555653333664
time_elpased: 1.571
batch start
#iterations: 8
currently lose_sum: 99.21313518285751
time_elpased: 1.584
batch start
#iterations: 9
currently lose_sum: 99.38904446363449
time_elpased: 1.599
batch start
#iterations: 10
currently lose_sum: 99.20692831277847
time_elpased: 1.544
batch start
#iterations: 11
currently lose_sum: 99.09152466058731
time_elpased: 1.567
batch start
#iterations: 12
currently lose_sum: 99.30241400003433
time_elpased: 1.556
batch start
#iterations: 13
currently lose_sum: 99.25252443552017
time_elpased: 1.569
batch start
#iterations: 14
currently lose_sum: 98.89803498983383
time_elpased: 1.591
batch start
#iterations: 15
currently lose_sum: 99.0949392914772
time_elpased: 1.554
batch start
#iterations: 16
currently lose_sum: 99.07747429609299
time_elpased: 1.531
batch start
#iterations: 17
currently lose_sum: 99.0271829366684
time_elpased: 1.569
batch start
#iterations: 18
currently lose_sum: 98.86634653806686
time_elpased: 1.618
batch start
#iterations: 19
currently lose_sum: 98.66833472251892
time_elpased: 1.555
start validation test
0.632371134021
0.629718875502
0.64553314121
0.637527952836
0.632349387623
63.690
batch start
#iterations: 20
currently lose_sum: 98.32769590616226
time_elpased: 1.547
batch start
#iterations: 21
currently lose_sum: 98.37571614980698
time_elpased: 1.571
batch start
#iterations: 22
currently lose_sum: 97.95216906070709
time_elpased: 1.581
batch start
#iterations: 23
currently lose_sum: 97.73929792642593
time_elpased: 1.558
batch start
#iterations: 24
currently lose_sum: 97.59316128492355
time_elpased: 1.574
batch start
#iterations: 25
currently lose_sum: 97.47140777111053
time_elpased: 1.592
batch start
#iterations: 26
currently lose_sum: 97.39426612854004
time_elpased: 1.587
batch start
#iterations: 27
currently lose_sum: 97.20971357822418
time_elpased: 1.593
batch start
#iterations: 28
currently lose_sum: 96.6879540681839
time_elpased: 1.577
batch start
#iterations: 29
currently lose_sum: 97.30156755447388
time_elpased: 1.587
batch start
#iterations: 30
currently lose_sum: 96.46539115905762
time_elpased: 1.565
batch start
#iterations: 31
currently lose_sum: 96.33880859613419
time_elpased: 1.547
batch start
#iterations: 32
currently lose_sum: 96.08144515752792
time_elpased: 1.55
batch start
#iterations: 33
currently lose_sum: 96.31768137216568
time_elpased: 1.587
batch start
#iterations: 34
currently lose_sum: 96.09423118829727
time_elpased: 1.541
batch start
#iterations: 35
currently lose_sum: 96.39832788705826
time_elpased: 1.541
batch start
#iterations: 36
currently lose_sum: 95.89951813220978
time_elpased: 1.612
batch start
#iterations: 37
currently lose_sum: 95.75807112455368
time_elpased: 1.538
batch start
#iterations: 38
currently lose_sum: 95.55920815467834
time_elpased: 1.605
batch start
#iterations: 39
currently lose_sum: 96.02325308322906
time_elpased: 1.565
start validation test
0.634742268041
0.631842791257
0.648620831618
0.640121889284
0.634719337742
62.279
batch start
#iterations: 40
currently lose_sum: 95.8209143280983
time_elpased: 1.559
batch start
#iterations: 41
currently lose_sum: 95.60691833496094
time_elpased: 1.538
batch start
#iterations: 42
currently lose_sum: 95.50381648540497
time_elpased: 1.57
batch start
#iterations: 43
currently lose_sum: 95.42452037334442
time_elpased: 1.558
batch start
#iterations: 44
currently lose_sum: 95.48046410083771
time_elpased: 1.569
batch start
#iterations: 45
currently lose_sum: 95.40886497497559
time_elpased: 1.579
batch start
#iterations: 46
currently lose_sum: 95.14218789339066
time_elpased: 1.571
batch start
#iterations: 47
currently lose_sum: 95.02986603975296
time_elpased: 1.572
batch start
#iterations: 48
currently lose_sum: 94.68634903430939
time_elpased: 1.584
batch start
#iterations: 49
currently lose_sum: 95.13825350999832
time_elpased: 1.57
batch start
#iterations: 50
currently lose_sum: 95.04442071914673
time_elpased: 1.604
batch start
#iterations: 51
currently lose_sum: 94.76927328109741
time_elpased: 1.539
batch start
#iterations: 52
currently lose_sum: 94.74489223957062
time_elpased: 1.571
batch start
#iterations: 53
currently lose_sum: 94.76849126815796
time_elpased: 1.564
batch start
#iterations: 54
currently lose_sum: 95.01893717050552
time_elpased: 1.555
batch start
#iterations: 55
currently lose_sum: 94.57125955820084
time_elpased: 1.558
batch start
#iterations: 56
currently lose_sum: 94.4648619890213
time_elpased: 1.587
batch start
#iterations: 57
currently lose_sum: 94.47824281454086
time_elpased: 1.563
batch start
#iterations: 58
currently lose_sum: 94.70917195081711
time_elpased: 1.52
batch start
#iterations: 59
currently lose_sum: 94.46677100658417
time_elpased: 1.546
start validation test
0.642835051546
0.641256969083
0.651090983944
0.646136560952
0.642821411014
61.303
batch start
#iterations: 60
currently lose_sum: 93.95317935943604
time_elpased: 1.552
batch start
#iterations: 61
currently lose_sum: 94.81834822893143
time_elpased: 1.58
batch start
#iterations: 62
currently lose_sum: 94.39868986606598
time_elpased: 1.542
batch start
#iterations: 63
currently lose_sum: 94.25231897830963
time_elpased: 1.547
batch start
#iterations: 64
currently lose_sum: 94.18384557962418
time_elpased: 1.575
batch start
#iterations: 65
currently lose_sum: 94.32936102151871
time_elpased: 1.565
batch start
#iterations: 66
currently lose_sum: 94.31941819190979
time_elpased: 1.562
batch start
#iterations: 67
currently lose_sum: 94.25121575593948
time_elpased: 1.54
batch start
#iterations: 68
currently lose_sum: 94.13818550109863
time_elpased: 1.588
batch start
#iterations: 69
currently lose_sum: 93.65091902017593
time_elpased: 1.597
batch start
#iterations: 70
currently lose_sum: 94.11708456277847
time_elpased: 1.601
batch start
#iterations: 71
currently lose_sum: 93.74475485086441
time_elpased: 1.563
batch start
#iterations: 72
currently lose_sum: 93.7886574268341
time_elpased: 1.558
batch start
#iterations: 73
currently lose_sum: 93.65366542339325
time_elpased: 1.606
batch start
#iterations: 74
currently lose_sum: 93.86149942874908
time_elpased: 1.577
batch start
#iterations: 75
currently lose_sum: 93.71019864082336
time_elpased: 1.555
batch start
#iterations: 76
currently lose_sum: 93.89472699165344
time_elpased: 1.597
batch start
#iterations: 77
currently lose_sum: 93.94183248281479
time_elpased: 1.597
batch start
#iterations: 78
currently lose_sum: 94.12752240896225
time_elpased: 1.598
batch start
#iterations: 79
currently lose_sum: 93.81404483318329
time_elpased: 1.552
start validation test
0.649948453608
0.640368557443
0.686702346645
0.662726595481
0.649887728465
60.533
batch start
#iterations: 80
currently lose_sum: 93.76506966352463
time_elpased: 1.533
batch start
#iterations: 81
currently lose_sum: 93.42631924152374
time_elpased: 1.542
batch start
#iterations: 82
currently lose_sum: 94.06567871570587
time_elpased: 1.576
batch start
#iterations: 83
currently lose_sum: 93.62154746055603
time_elpased: 1.597
batch start
#iterations: 84
currently lose_sum: 93.30130487680435
time_elpased: 1.574
batch start
#iterations: 85
currently lose_sum: 93.44360625743866
time_elpased: 1.575
batch start
#iterations: 86
currently lose_sum: 93.22482097148895
time_elpased: 1.55
batch start
#iterations: 87
currently lose_sum: 93.28270417451859
time_elpased: 1.551
batch start
#iterations: 88
currently lose_sum: 93.44722318649292
time_elpased: 1.552
batch start
#iterations: 89
currently lose_sum: 93.80425351858139
time_elpased: 1.537
batch start
#iterations: 90
currently lose_sum: 93.33139562606812
time_elpased: 1.589
batch start
#iterations: 91
currently lose_sum: 92.99898421764374
time_elpased: 1.552
batch start
#iterations: 92
currently lose_sum: 92.96279716491699
time_elpased: 1.594
batch start
#iterations: 93
currently lose_sum: 93.22038835287094
time_elpased: 1.574
batch start
#iterations: 94
currently lose_sum: 93.25646567344666
time_elpased: 1.551
batch start
#iterations: 95
currently lose_sum: 92.8084209561348
time_elpased: 1.535
batch start
#iterations: 96
currently lose_sum: 93.06705319881439
time_elpased: 1.563
batch start
#iterations: 97
currently lose_sum: 92.93307965993881
time_elpased: 1.567
batch start
#iterations: 98
currently lose_sum: 93.01357102394104
time_elpased: 1.562
batch start
#iterations: 99
currently lose_sum: 92.84357839822769
time_elpased: 1.573
start validation test
0.642989690722
0.639166001596
0.659427748044
0.649138804458
0.642962531602
60.896
batch start
#iterations: 100
currently lose_sum: 93.16386866569519
time_elpased: 1.576
batch start
#iterations: 101
currently lose_sum: 93.37008678913116
time_elpased: 1.629
batch start
#iterations: 102
currently lose_sum: 93.06366950273514
time_elpased: 1.585
batch start
#iterations: 103
currently lose_sum: 92.85097253322601
time_elpased: 1.56
batch start
#iterations: 104
currently lose_sum: 92.72002130746841
time_elpased: 1.56
batch start
#iterations: 105
currently lose_sum: 92.69409984350204
time_elpased: 1.556
batch start
#iterations: 106
currently lose_sum: 92.73046839237213
time_elpased: 1.583
batch start
#iterations: 107
currently lose_sum: 92.20080411434174
time_elpased: 1.562
batch start
#iterations: 108
currently lose_sum: 92.71903729438782
time_elpased: 1.564
batch start
#iterations: 109
currently lose_sum: 93.17320096492767
time_elpased: 1.563
batch start
#iterations: 110
currently lose_sum: 92.75087773799896
time_elpased: 1.606
batch start
#iterations: 111
currently lose_sum: 92.91296887397766
time_elpased: 1.573
batch start
#iterations: 112
currently lose_sum: 92.23030710220337
time_elpased: 1.563
batch start
#iterations: 113
currently lose_sum: 92.13598781824112
time_elpased: 1.614
batch start
#iterations: 114
currently lose_sum: 92.80111974477768
time_elpased: 1.584
batch start
#iterations: 115
currently lose_sum: 92.5843032002449
time_elpased: 1.547
batch start
#iterations: 116
currently lose_sum: 92.55910861492157
time_elpased: 1.582
batch start
#iterations: 117
currently lose_sum: 92.3430906534195
time_elpased: 1.578
batch start
#iterations: 118
currently lose_sum: 92.69007694721222
time_elpased: 1.557
batch start
#iterations: 119
currently lose_sum: 92.4247174859047
time_elpased: 1.548
start validation test
0.641701030928
0.638374537083
0.65644298065
0.647282691429
0.641676674134
60.677
batch start
#iterations: 120
currently lose_sum: 92.16724097728729
time_elpased: 1.592
batch start
#iterations: 121
currently lose_sum: 92.33702725172043
time_elpased: 1.607
batch start
#iterations: 122
currently lose_sum: 91.57556241750717
time_elpased: 1.575
batch start
#iterations: 123
currently lose_sum: 92.75423109531403
time_elpased: 1.561
batch start
#iterations: 124
currently lose_sum: 92.4895384311676
time_elpased: 1.542
batch start
#iterations: 125
currently lose_sum: 91.85631734132767
time_elpased: 1.573
batch start
#iterations: 126
currently lose_sum: 91.71954953670502
time_elpased: 1.574
batch start
#iterations: 127
currently lose_sum: 92.02888250350952
time_elpased: 1.54
batch start
#iterations: 128
currently lose_sum: 91.81918728351593
time_elpased: 1.568
batch start
#iterations: 129
currently lose_sum: 91.8171626329422
time_elpased: 1.584
batch start
#iterations: 130
currently lose_sum: 92.12481933832169
time_elpased: 1.596
batch start
#iterations: 131
currently lose_sum: 92.24382269382477
time_elpased: 1.54
batch start
#iterations: 132
currently lose_sum: 91.63663071393967
time_elpased: 1.574
batch start
#iterations: 133
currently lose_sum: 91.71891188621521
time_elpased: 1.589
batch start
#iterations: 134
currently lose_sum: 92.11870300769806
time_elpased: 1.554
batch start
#iterations: 135
currently lose_sum: 91.86015504598618
time_elpased: 1.576
batch start
#iterations: 136
currently lose_sum: 91.98949468135834
time_elpased: 1.584
batch start
#iterations: 137
currently lose_sum: 91.82244402170181
time_elpased: 1.6
batch start
#iterations: 138
currently lose_sum: 91.43954384326935
time_elpased: 1.569
batch start
#iterations: 139
currently lose_sum: 91.66031634807587
time_elpased: 1.552
start validation test
0.642422680412
0.635627135188
0.670234664471
0.652472321026
0.642376729179
60.806
batch start
#iterations: 140
currently lose_sum: 91.41910004615784
time_elpased: 1.597
batch start
#iterations: 141
currently lose_sum: 91.64383488893509
time_elpased: 1.536
batch start
#iterations: 142
currently lose_sum: 91.49884980916977
time_elpased: 1.581
batch start
#iterations: 143
currently lose_sum: 91.76695245504379
time_elpased: 1.589
batch start
#iterations: 144
currently lose_sum: 91.17402291297913
time_elpased: 1.563
batch start
#iterations: 145
currently lose_sum: 91.70667469501495
time_elpased: 1.578
batch start
#iterations: 146
currently lose_sum: 91.39693397283554
time_elpased: 1.563
batch start
#iterations: 147
currently lose_sum: 91.51175254583359
time_elpased: 1.581
batch start
#iterations: 148
currently lose_sum: 91.41205495595932
time_elpased: 1.612
batch start
#iterations: 149
currently lose_sum: 91.66195142269135
time_elpased: 1.57
batch start
#iterations: 150
currently lose_sum: 91.68429976701736
time_elpased: 1.547
batch start
#iterations: 151
currently lose_sum: 91.40891069173813
time_elpased: 1.555
batch start
#iterations: 152
currently lose_sum: 91.58876299858093
time_elpased: 1.567
batch start
#iterations: 153
currently lose_sum: 91.40968811511993
time_elpased: 1.541
batch start
#iterations: 154
currently lose_sum: 91.55115258693695
time_elpased: 1.57
batch start
#iterations: 155
currently lose_sum: 91.22789245843887
time_elpased: 1.56
batch start
#iterations: 156
currently lose_sum: 91.2983438372612
time_elpased: 1.569
batch start
#iterations: 157
currently lose_sum: 90.81429070234299
time_elpased: 1.576
batch start
#iterations: 158
currently lose_sum: 91.22594445943832
time_elpased: 1.546
batch start
#iterations: 159
currently lose_sum: 91.29089200496674
time_elpased: 1.575
start validation test
0.634484536082
0.629706492736
0.655825442569
0.642500630199
0.634449276427
60.977
batch start
#iterations: 160
currently lose_sum: 91.04251670837402
time_elpased: 1.649
batch start
#iterations: 161
currently lose_sum: 91.27080935239792
time_elpased: 1.539
batch start
#iterations: 162
currently lose_sum: 91.40848624706268
time_elpased: 1.608
batch start
#iterations: 163
currently lose_sum: 90.95428556203842
time_elpased: 1.552
batch start
#iterations: 164
currently lose_sum: 91.42624914646149
time_elpased: 1.575
batch start
#iterations: 165
currently lose_sum: 91.0024471282959
time_elpased: 1.643
batch start
#iterations: 166
currently lose_sum: 90.98774582147598
time_elpased: 1.585
batch start
#iterations: 167
currently lose_sum: 90.29730373620987
time_elpased: 1.588
batch start
#iterations: 168
currently lose_sum: 91.05241310596466
time_elpased: 1.574
batch start
#iterations: 169
currently lose_sum: 90.56075417995453
time_elpased: 1.537
batch start
#iterations: 170
currently lose_sum: 90.70899188518524
time_elpased: 1.593
batch start
#iterations: 171
currently lose_sum: 90.75302666425705
time_elpased: 1.55
batch start
#iterations: 172
currently lose_sum: 90.97694635391235
time_elpased: 1.633
batch start
#iterations: 173
currently lose_sum: 90.73290330171585
time_elpased: 1.57
batch start
#iterations: 174
currently lose_sum: 90.3735648393631
time_elpased: 1.595
batch start
#iterations: 175
currently lose_sum: 90.74171137809753
time_elpased: 1.575
batch start
#iterations: 176
currently lose_sum: 91.05566716194153
time_elpased: 1.594
batch start
#iterations: 177
currently lose_sum: 90.91241753101349
time_elpased: 1.583
batch start
#iterations: 178
currently lose_sum: 90.6179992556572
time_elpased: 1.578
batch start
#iterations: 179
currently lose_sum: 90.21898365020752
time_elpased: 1.56
start validation test
0.631804123711
0.643183082916
0.594792095513
0.618041815946
0.631865275348
60.969
batch start
#iterations: 180
currently lose_sum: 90.58456712961197
time_elpased: 1.539
batch start
#iterations: 181
currently lose_sum: 91.01805382966995
time_elpased: 1.558
batch start
#iterations: 182
currently lose_sum: 90.68784242868423
time_elpased: 1.584
batch start
#iterations: 183
currently lose_sum: 90.19899189472198
time_elpased: 1.545
batch start
#iterations: 184
currently lose_sum: 90.71271407604218
time_elpased: 1.586
batch start
#iterations: 185
currently lose_sum: 90.09271031618118
time_elpased: 1.576
batch start
#iterations: 186
currently lose_sum: 90.6414207816124
time_elpased: 1.558
batch start
#iterations: 187
currently lose_sum: 90.12271165847778
time_elpased: 1.558
batch start
#iterations: 188
currently lose_sum: 90.90767347812653
time_elpased: 1.638
batch start
#iterations: 189
currently lose_sum: 90.57279413938522
time_elpased: 1.599
batch start
#iterations: 190
currently lose_sum: 90.33299803733826
time_elpased: 1.57
batch start
#iterations: 191
currently lose_sum: 90.06039869785309
time_elpased: 1.568
batch start
#iterations: 192
currently lose_sum: 90.61199843883514
time_elpased: 1.576
batch start
#iterations: 193
currently lose_sum: 90.25851798057556
time_elpased: 1.562
batch start
#iterations: 194
currently lose_sum: 90.63264560699463
time_elpased: 1.601
batch start
#iterations: 195
currently lose_sum: 90.04260569810867
time_elpased: 1.544
batch start
#iterations: 196
currently lose_sum: 89.97449320554733
time_elpased: 1.603
batch start
#iterations: 197
currently lose_sum: 90.23814034461975
time_elpased: 1.563
batch start
#iterations: 198
currently lose_sum: 89.6654971241951
time_elpased: 1.599
batch start
#iterations: 199
currently lose_sum: 89.85096454620361
time_elpased: 1.635
start validation test
0.621958762887
0.627570694087
0.603025936599
0.615053537686
0.621990043888
61.467
batch start
#iterations: 200
currently lose_sum: 89.7772810459137
time_elpased: 1.564
batch start
#iterations: 201
currently lose_sum: 89.81805646419525
time_elpased: 1.619
batch start
#iterations: 202
currently lose_sum: 90.15387320518494
time_elpased: 1.566
batch start
#iterations: 203
currently lose_sum: 89.99375408887863
time_elpased: 1.572
batch start
#iterations: 204
currently lose_sum: 90.55900198221207
time_elpased: 1.578
batch start
#iterations: 205
currently lose_sum: 89.80472737550735
time_elpased: 1.549
batch start
#iterations: 206
currently lose_sum: 89.27118998765945
time_elpased: 1.547
batch start
#iterations: 207
currently lose_sum: 90.00818556547165
time_elpased: 1.563
batch start
#iterations: 208
currently lose_sum: 89.88833391666412
time_elpased: 1.585
batch start
#iterations: 209
currently lose_sum: 89.62882244586945
time_elpased: 1.572
batch start
#iterations: 210
currently lose_sum: 89.59542572498322
time_elpased: 1.564
batch start
#iterations: 211
currently lose_sum: 89.72032731771469
time_elpased: 1.548
batch start
#iterations: 212
currently lose_sum: 89.75146102905273
time_elpased: 1.594
batch start
#iterations: 213
currently lose_sum: 89.4024185538292
time_elpased: 1.58
batch start
#iterations: 214
currently lose_sum: 89.51545667648315
time_elpased: 1.619
batch start
#iterations: 215
currently lose_sum: 89.51569825410843
time_elpased: 1.573
batch start
#iterations: 216
currently lose_sum: 89.47331726551056
time_elpased: 1.605
batch start
#iterations: 217
currently lose_sum: 89.79268097877502
time_elpased: 1.599
batch start
#iterations: 218
currently lose_sum: 89.797727227211
time_elpased: 1.566
batch start
#iterations: 219
currently lose_sum: 89.18918687105179
time_elpased: 1.576
start validation test
0.631546391753
0.644496961512
0.589440098806
0.615740242985
0.631615960184
61.057
batch start
#iterations: 220
currently lose_sum: 89.44655591249466
time_elpased: 1.606
batch start
#iterations: 221
currently lose_sum: 89.43578964471817
time_elpased: 1.596
batch start
#iterations: 222
currently lose_sum: 89.02768337726593
time_elpased: 1.602
batch start
#iterations: 223
currently lose_sum: 89.17491561174393
time_elpased: 1.563
batch start
#iterations: 224
currently lose_sum: 89.49801433086395
time_elpased: 1.605
batch start
#iterations: 225
currently lose_sum: 88.94128823280334
time_elpased: 1.544
batch start
#iterations: 226
currently lose_sum: 88.67630726099014
time_elpased: 1.627
batch start
#iterations: 227
currently lose_sum: 88.9651130437851
time_elpased: 1.589
batch start
#iterations: 228
currently lose_sum: 88.90699696540833
time_elpased: 1.574
batch start
#iterations: 229
currently lose_sum: 88.43799686431885
time_elpased: 1.562
batch start
#iterations: 230
currently lose_sum: 89.32020610570908
time_elpased: 1.574
batch start
#iterations: 231
currently lose_sum: 89.34304547309875
time_elpased: 1.578
batch start
#iterations: 232
currently lose_sum: 89.52811723947525
time_elpased: 1.623
batch start
#iterations: 233
currently lose_sum: 89.7111205458641
time_elpased: 1.564
batch start
#iterations: 234
currently lose_sum: 89.28858065605164
time_elpased: 1.588
batch start
#iterations: 235
currently lose_sum: 89.36755359172821
time_elpased: 1.574
batch start
#iterations: 236
currently lose_sum: 89.0714083313942
time_elpased: 1.579
batch start
#iterations: 237
currently lose_sum: 89.13805389404297
time_elpased: 1.543
batch start
#iterations: 238
currently lose_sum: 89.27116167545319
time_elpased: 1.605
batch start
#iterations: 239
currently lose_sum: 89.0656265616417
time_elpased: 1.584
start validation test
0.631391752577
0.642452515828
0.59530671058
0.617981729793
0.631451372638
61.327
batch start
#iterations: 240
currently lose_sum: 88.40577733516693
time_elpased: 1.599
batch start
#iterations: 241
currently lose_sum: 88.93865013122559
time_elpased: 1.574
batch start
#iterations: 242
currently lose_sum: 88.56114226579666
time_elpased: 1.627
batch start
#iterations: 243
currently lose_sum: 89.0832189321518
time_elpased: 1.573
batch start
#iterations: 244
currently lose_sum: 88.6455288529396
time_elpased: 1.56
batch start
#iterations: 245
currently lose_sum: 88.98889154195786
time_elpased: 1.573
batch start
#iterations: 246
currently lose_sum: 88.29100638628006
time_elpased: 1.577
batch start
#iterations: 247
currently lose_sum: 88.4901527762413
time_elpased: 1.557
batch start
#iterations: 248
currently lose_sum: 88.21991604566574
time_elpased: 1.569
batch start
#iterations: 249
currently lose_sum: 89.15386080741882
time_elpased: 1.545
batch start
#iterations: 250
currently lose_sum: 88.4906347990036
time_elpased: 1.531
batch start
#iterations: 251
currently lose_sum: 88.39204108715057
time_elpased: 1.568
batch start
#iterations: 252
currently lose_sum: 88.37783819437027
time_elpased: 1.599
batch start
#iterations: 253
currently lose_sum: 88.30298298597336
time_elpased: 1.569
batch start
#iterations: 254
currently lose_sum: 87.80151736736298
time_elpased: 1.605
batch start
#iterations: 255
currently lose_sum: 88.98715263605118
time_elpased: 1.556
batch start
#iterations: 256
currently lose_sum: 88.07554060220718
time_elpased: 1.625
batch start
#iterations: 257
currently lose_sum: 88.36286586523056
time_elpased: 1.58
batch start
#iterations: 258
currently lose_sum: 88.21433699131012
time_elpased: 1.582
batch start
#iterations: 259
currently lose_sum: 88.5003137588501
time_elpased: 1.568
start validation test
0.62293814433
0.639188405797
0.567414573899
0.601166784799
0.623029880919
61.976
batch start
#iterations: 260
currently lose_sum: 88.56766074895859
time_elpased: 1.577
batch start
#iterations: 261
currently lose_sum: 88.14489740133286
time_elpased: 1.59
batch start
#iterations: 262
currently lose_sum: 87.63519531488419
time_elpased: 1.556
batch start
#iterations: 263
currently lose_sum: 88.21537095308304
time_elpased: 1.552
batch start
#iterations: 264
currently lose_sum: 88.30750197172165
time_elpased: 1.581
batch start
#iterations: 265
currently lose_sum: 88.16162818670273
time_elpased: 1.578
batch start
#iterations: 266
currently lose_sum: 87.98568910360336
time_elpased: 1.544
batch start
#iterations: 267
currently lose_sum: 88.19967937469482
time_elpased: 1.582
batch start
#iterations: 268
currently lose_sum: 87.70481687784195
time_elpased: 1.572
batch start
#iterations: 269
currently lose_sum: 87.87739062309265
time_elpased: 1.586
batch start
#iterations: 270
currently lose_sum: 87.54834073781967
time_elpased: 1.606
batch start
#iterations: 271
currently lose_sum: 88.15404206514359
time_elpased: 1.579
batch start
#iterations: 272
currently lose_sum: 87.82672071456909
time_elpased: 1.589
batch start
#iterations: 273
currently lose_sum: 87.59274351596832
time_elpased: 1.546
batch start
#iterations: 274
currently lose_sum: 87.88454693555832
time_elpased: 1.559
batch start
#iterations: 275
currently lose_sum: 87.54360806941986
time_elpased: 1.62
batch start
#iterations: 276
currently lose_sum: 87.1888273358345
time_elpased: 1.572
batch start
#iterations: 277
currently lose_sum: 87.05725163221359
time_elpased: 1.577
batch start
#iterations: 278
currently lose_sum: 87.54786229133606
time_elpased: 1.564
batch start
#iterations: 279
currently lose_sum: 87.61189824342728
time_elpased: 1.569
start validation test
0.61175257732
0.630061934254
0.544462741869
0.584143109541
0.611863754247
62.618
batch start
#iterations: 280
currently lose_sum: 87.12110620737076
time_elpased: 1.581
batch start
#iterations: 281
currently lose_sum: 87.37415730953217
time_elpased: 1.59
batch start
#iterations: 282
currently lose_sum: 87.58422923088074
time_elpased: 1.59
batch start
#iterations: 283
currently lose_sum: 87.80549484491348
time_elpased: 1.582
batch start
#iterations: 284
currently lose_sum: 87.11611264944077
time_elpased: 1.611
batch start
#iterations: 285
currently lose_sum: 87.35379534959793
time_elpased: 1.551
batch start
#iterations: 286
currently lose_sum: 87.43721222877502
time_elpased: 1.606
batch start
#iterations: 287
currently lose_sum: 87.47552877664566
time_elpased: 1.57
batch start
#iterations: 288
currently lose_sum: 88.02858144044876
time_elpased: 1.549
batch start
#iterations: 289
currently lose_sum: 87.50230836868286
time_elpased: 1.557
batch start
#iterations: 290
currently lose_sum: 87.46854627132416
time_elpased: 1.586
batch start
#iterations: 291
currently lose_sum: 87.82989054918289
time_elpased: 1.574
batch start
#iterations: 292
currently lose_sum: 87.14732348918915
time_elpased: 1.566
batch start
#iterations: 293
currently lose_sum: 86.98382091522217
time_elpased: 1.599
batch start
#iterations: 294
currently lose_sum: 87.1548502445221
time_elpased: 1.602
batch start
#iterations: 295
currently lose_sum: 87.36530292034149
time_elpased: 1.584
batch start
#iterations: 296
currently lose_sum: 87.73159581422806
time_elpased: 1.594
batch start
#iterations: 297
currently lose_sum: 87.17078721523285
time_elpased: 1.572
batch start
#iterations: 298
currently lose_sum: 87.01584887504578
time_elpased: 1.556
batch start
#iterations: 299
currently lose_sum: 87.11235272884369
time_elpased: 1.568
start validation test
0.622371134021
0.642228040943
0.555372581309
0.595650734077
0.622481829688
62.397
batch start
#iterations: 300
currently lose_sum: 86.9667598605156
time_elpased: 1.552
batch start
#iterations: 301
currently lose_sum: 86.77647143602371
time_elpased: 1.563
batch start
#iterations: 302
currently lose_sum: 86.28164726495743
time_elpased: 1.585
batch start
#iterations: 303
currently lose_sum: 86.80989116430283
time_elpased: 1.554
batch start
#iterations: 304
currently lose_sum: 86.4153995513916
time_elpased: 1.562
batch start
#iterations: 305
currently lose_sum: 86.81132334470749
time_elpased: 1.547
batch start
#iterations: 306
currently lose_sum: 87.48570138216019
time_elpased: 1.641
batch start
#iterations: 307
currently lose_sum: 86.44902616739273
time_elpased: 1.601
batch start
#iterations: 308
currently lose_sum: 86.61477202177048
time_elpased: 1.59
batch start
#iterations: 309
currently lose_sum: 86.48024666309357
time_elpased: 1.587
batch start
#iterations: 310
currently lose_sum: 86.5426966547966
time_elpased: 1.569
batch start
#iterations: 311
currently lose_sum: 86.33786803483963
time_elpased: 1.601
batch start
#iterations: 312
currently lose_sum: 86.81469839811325
time_elpased: 1.611
batch start
#iterations: 313
currently lose_sum: 86.44017630815506
time_elpased: 1.55
batch start
#iterations: 314
currently lose_sum: 85.97348582744598
time_elpased: 1.569
batch start
#iterations: 315
currently lose_sum: 86.23037642240524
time_elpased: 1.594
batch start
#iterations: 316
currently lose_sum: 86.6792083978653
time_elpased: 1.602
batch start
#iterations: 317
currently lose_sum: 86.22039473056793
time_elpased: 1.592
batch start
#iterations: 318
currently lose_sum: 86.59430754184723
time_elpased: 1.563
batch start
#iterations: 319
currently lose_sum: 87.07839739322662
time_elpased: 1.624
start validation test
0.623865979381
0.641313237528
0.564944421573
0.600711354309
0.623963330158
62.545
batch start
#iterations: 320
currently lose_sum: 86.4643080830574
time_elpased: 1.565
batch start
#iterations: 321
currently lose_sum: 86.56837159395218
time_elpased: 1.592
batch start
#iterations: 322
currently lose_sum: 86.08795881271362
time_elpased: 1.574
batch start
#iterations: 323
currently lose_sum: 86.77477943897247
time_elpased: 1.593
batch start
#iterations: 324
currently lose_sum: 86.19263452291489
time_elpased: 1.625
batch start
#iterations: 325
currently lose_sum: 86.14991927146912
time_elpased: 1.576
batch start
#iterations: 326
currently lose_sum: 85.59076344966888
time_elpased: 1.631
batch start
#iterations: 327
currently lose_sum: 86.00728225708008
time_elpased: 1.543
batch start
#iterations: 328
currently lose_sum: 86.50488293170929
time_elpased: 1.596
batch start
#iterations: 329
currently lose_sum: 86.28314870595932
time_elpased: 1.63
batch start
#iterations: 330
currently lose_sum: 85.99683052301407
time_elpased: 1.555
batch start
#iterations: 331
currently lose_sum: 85.91861021518707
time_elpased: 1.578
batch start
#iterations: 332
currently lose_sum: 86.59994077682495
time_elpased: 1.585
batch start
#iterations: 333
currently lose_sum: 86.02753406763077
time_elpased: 1.578
batch start
#iterations: 334
currently lose_sum: 85.81783944368362
time_elpased: 1.563
batch start
#iterations: 335
currently lose_sum: 85.69021898508072
time_elpased: 1.576
batch start
#iterations: 336
currently lose_sum: 85.91407001018524
time_elpased: 1.573
batch start
#iterations: 337
currently lose_sum: 85.56661385297775
time_elpased: 1.568
batch start
#iterations: 338
currently lose_sum: 85.63285517692566
time_elpased: 1.579
batch start
#iterations: 339
currently lose_sum: 85.62797302007675
time_elpased: 1.563
start validation test
0.626597938144
0.648879788003
0.554446274187
0.597957597958
0.626717147833
62.610
batch start
#iterations: 340
currently lose_sum: 85.61245197057724
time_elpased: 1.589
batch start
#iterations: 341
currently lose_sum: 85.84612661600113
time_elpased: 1.559
batch start
#iterations: 342
currently lose_sum: 85.51272189617157
time_elpased: 1.608
batch start
#iterations: 343
currently lose_sum: 86.21872395277023
time_elpased: 1.608
batch start
#iterations: 344
currently lose_sum: 85.5179957151413
time_elpased: 1.575
batch start
#iterations: 345
currently lose_sum: 85.46812134981155
time_elpased: 1.581
batch start
#iterations: 346
currently lose_sum: 85.16772025823593
time_elpased: 1.603
batch start
#iterations: 347
currently lose_sum: 85.7291812300682
time_elpased: 1.604
batch start
#iterations: 348
currently lose_sum: 85.45803439617157
time_elpased: 1.587
batch start
#iterations: 349
currently lose_sum: 85.20448786020279
time_elpased: 1.554
batch start
#iterations: 350
currently lose_sum: 86.02370649576187
time_elpased: 1.576
batch start
#iterations: 351
currently lose_sum: 85.47428494691849
time_elpased: 1.573
batch start
#iterations: 352
currently lose_sum: 85.74331945180893
time_elpased: 1.548
batch start
#iterations: 353
currently lose_sum: 85.66361820697784
time_elpased: 1.566
batch start
#iterations: 354
currently lose_sum: 85.21684211492538
time_elpased: 1.609
batch start
#iterations: 355
currently lose_sum: 84.78480613231659
time_elpased: 1.583
batch start
#iterations: 356
currently lose_sum: 85.40164542198181
time_elpased: 1.597
batch start
#iterations: 357
currently lose_sum: 84.50943022966385
time_elpased: 1.569
batch start
#iterations: 358
currently lose_sum: 85.42830139398575
time_elpased: 1.602
batch start
#iterations: 359
currently lose_sum: 85.09922504425049
time_elpased: 1.59
start validation test
0.621958762887
0.636802205376
0.57060518732
0.601889045706
0.622043609769
62.875
batch start
#iterations: 360
currently lose_sum: 85.11555260419846
time_elpased: 1.596
batch start
#iterations: 361
currently lose_sum: 84.62925183773041
time_elpased: 1.573
batch start
#iterations: 362
currently lose_sum: 85.51515102386475
time_elpased: 1.575
batch start
#iterations: 363
currently lose_sum: 84.5506392121315
time_elpased: 1.606
batch start
#iterations: 364
currently lose_sum: 85.0344500541687
time_elpased: 1.596
batch start
#iterations: 365
currently lose_sum: 84.56954735517502
time_elpased: 1.553
batch start
#iterations: 366
currently lose_sum: 85.09919530153275
time_elpased: 1.585
batch start
#iterations: 367
currently lose_sum: 84.94086998701096
time_elpased: 1.575
batch start
#iterations: 368
currently lose_sum: 84.91193735599518
time_elpased: 1.591
batch start
#iterations: 369
currently lose_sum: 84.6695174574852
time_elpased: 1.591
batch start
#iterations: 370
currently lose_sum: 84.3140207529068
time_elpased: 1.595
batch start
#iterations: 371
currently lose_sum: 85.27402150630951
time_elpased: 1.569
batch start
#iterations: 372
currently lose_sum: 85.4192401766777
time_elpased: 1.587
batch start
#iterations: 373
currently lose_sum: 84.46522957086563
time_elpased: 1.573
batch start
#iterations: 374
currently lose_sum: 85.24833136796951
time_elpased: 1.588
batch start
#iterations: 375
currently lose_sum: 84.86936861276627
time_elpased: 1.621
batch start
#iterations: 376
currently lose_sum: 84.52293264865875
time_elpased: 1.58
batch start
#iterations: 377
currently lose_sum: 84.73320430517197
time_elpased: 1.587
batch start
#iterations: 378
currently lose_sum: 84.67049765586853
time_elpased: 1.577
batch start
#iterations: 379
currently lose_sum: 84.62710681557655
time_elpased: 1.577
start validation test
0.606134020619
0.630552409714
0.515747221079
0.567400781294
0.606283358577
64.091
batch start
#iterations: 380
currently lose_sum: 84.28678375482559
time_elpased: 1.578
batch start
#iterations: 381
currently lose_sum: 84.52967482805252
time_elpased: 1.552
batch start
#iterations: 382
currently lose_sum: 84.8200534582138
time_elpased: 1.592
batch start
#iterations: 383
currently lose_sum: 84.06034797430038
time_elpased: 1.553
batch start
#iterations: 384
currently lose_sum: 84.46956789493561
time_elpased: 1.599
batch start
#iterations: 385
currently lose_sum: 84.21178710460663
time_elpased: 1.573
batch start
#iterations: 386
currently lose_sum: 84.1676464676857
time_elpased: 1.572
batch start
#iterations: 387
currently lose_sum: 83.71022069454193
time_elpased: 1.57
batch start
#iterations: 388
currently lose_sum: 84.62833148241043
time_elpased: 1.582
batch start
#iterations: 389
currently lose_sum: 83.64975267648697
time_elpased: 1.605
batch start
#iterations: 390
currently lose_sum: 84.67337542772293
time_elpased: 1.59
batch start
#iterations: 391
currently lose_sum: 84.20439314842224
time_elpased: 1.595
batch start
#iterations: 392
currently lose_sum: 84.37628221511841
time_elpased: 1.612
batch start
#iterations: 393
currently lose_sum: 84.12705552577972
time_elpased: 1.563
batch start
#iterations: 394
currently lose_sum: 84.47657972574234
time_elpased: 1.618
batch start
#iterations: 395
currently lose_sum: 84.71234631538391
time_elpased: 1.606
batch start
#iterations: 396
currently lose_sum: 83.74928706884384
time_elpased: 1.59
batch start
#iterations: 397
currently lose_sum: 83.92152971029282
time_elpased: 1.604
batch start
#iterations: 398
currently lose_sum: 84.25000566244125
time_elpased: 1.615
batch start
#iterations: 399
currently lose_sum: 83.91220217943192
time_elpased: 1.552
start validation test
0.612731958763
0.635136793032
0.532832441334
0.579504113729
0.612863969531
64.079
acc: 0.658
pre: 0.647
rec: 0.696
F1: 0.671
auc: 0.658
