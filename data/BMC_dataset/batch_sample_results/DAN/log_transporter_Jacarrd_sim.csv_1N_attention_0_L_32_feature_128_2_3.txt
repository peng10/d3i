start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.62441623210907
time_elpased: 1.818
batch start
#iterations: 1
currently lose_sum: 100.44507014751434
time_elpased: 1.772
batch start
#iterations: 2
currently lose_sum: 100.38543993234634
time_elpased: 1.724
batch start
#iterations: 3
currently lose_sum: 100.26819914579391
time_elpased: 1.754
batch start
#iterations: 4
currently lose_sum: 100.29815250635147
time_elpased: 1.743
batch start
#iterations: 5
currently lose_sum: 100.14208781719208
time_elpased: 1.738
batch start
#iterations: 6
currently lose_sum: 100.29336661100388
time_elpased: 1.767
batch start
#iterations: 7
currently lose_sum: 100.1418371796608
time_elpased: 1.755
batch start
#iterations: 8
currently lose_sum: 100.19607990980148
time_elpased: 1.747
batch start
#iterations: 9
currently lose_sum: 100.18914222717285
time_elpased: 1.761
batch start
#iterations: 10
currently lose_sum: 100.1778957247734
time_elpased: 1.777
batch start
#iterations: 11
currently lose_sum: 100.12735974788666
time_elpased: 1.78
batch start
#iterations: 12
currently lose_sum: 99.96795052289963
time_elpased: 1.771
batch start
#iterations: 13
currently lose_sum: 100.04864972829819
time_elpased: 1.796
batch start
#iterations: 14
currently lose_sum: 99.9574208855629
time_elpased: 1.751
batch start
#iterations: 15
currently lose_sum: 99.96479713916779
time_elpased: 1.776
batch start
#iterations: 16
currently lose_sum: 99.96876728534698
time_elpased: 1.718
batch start
#iterations: 17
currently lose_sum: 99.91418385505676
time_elpased: 1.743
batch start
#iterations: 18
currently lose_sum: 99.83396917581558
time_elpased: 1.774
batch start
#iterations: 19
currently lose_sum: 99.87613320350647
time_elpased: 1.787
start validation test
0.57618556701
0.602892561983
0.450494030465
0.515669180019
0.576393235803
66.055
batch start
#iterations: 20
currently lose_sum: 99.84490042924881
time_elpased: 1.74
batch start
#iterations: 21
currently lose_sum: 99.79710376262665
time_elpased: 1.752
batch start
#iterations: 22
currently lose_sum: 99.62572485208511
time_elpased: 1.719
batch start
#iterations: 23
currently lose_sum: 99.69962787628174
time_elpased: 1.796
batch start
#iterations: 24
currently lose_sum: 99.55641722679138
time_elpased: 1.747
batch start
#iterations: 25
currently lose_sum: 99.7057666182518
time_elpased: 1.781
batch start
#iterations: 26
currently lose_sum: 99.6988188624382
time_elpased: 1.768
batch start
#iterations: 27
currently lose_sum: 99.60051935911179
time_elpased: 1.78
batch start
#iterations: 28
currently lose_sum: 99.6365094780922
time_elpased: 1.756
batch start
#iterations: 29
currently lose_sum: 99.64203554391861
time_elpased: 1.738
batch start
#iterations: 30
currently lose_sum: 99.42603266239166
time_elpased: 1.732
batch start
#iterations: 31
currently lose_sum: 99.63639211654663
time_elpased: 1.737
batch start
#iterations: 32
currently lose_sum: 99.46252870559692
time_elpased: 1.746
batch start
#iterations: 33
currently lose_sum: 99.71794724464417
time_elpased: 1.798
batch start
#iterations: 34
currently lose_sum: 99.43342596292496
time_elpased: 1.745
batch start
#iterations: 35
currently lose_sum: 99.6333618760109
time_elpased: 1.762
batch start
#iterations: 36
currently lose_sum: 99.21848803758621
time_elpased: 1.739
batch start
#iterations: 37
currently lose_sum: 99.37403017282486
time_elpased: 1.743
batch start
#iterations: 38
currently lose_sum: 99.30722016096115
time_elpased: 1.749
batch start
#iterations: 39
currently lose_sum: 99.21794468164444
time_elpased: 1.725
start validation test
0.580051546392
0.591829568067
0.52037875669
0.553809080453
0.580150138362
65.762
batch start
#iterations: 40
currently lose_sum: 99.33520096540451
time_elpased: 1.73
batch start
#iterations: 41
currently lose_sum: 99.4459776878357
time_elpased: 1.739
batch start
#iterations: 42
currently lose_sum: 99.26248413324356
time_elpased: 1.766
batch start
#iterations: 43
currently lose_sum: 99.27244281768799
time_elpased: 1.775
batch start
#iterations: 44
currently lose_sum: 99.28043186664581
time_elpased: 1.729
batch start
#iterations: 45
currently lose_sum: 99.36063987016678
time_elpased: 1.764
batch start
#iterations: 46
currently lose_sum: 99.2142733335495
time_elpased: 1.741
batch start
#iterations: 47
currently lose_sum: 99.33236175775528
time_elpased: 1.744
batch start
#iterations: 48
currently lose_sum: 99.2494449019432
time_elpased: 1.729
batch start
#iterations: 49
currently lose_sum: 99.17333519458771
time_elpased: 1.781
batch start
#iterations: 50
currently lose_sum: 99.15016043186188
time_elpased: 1.727
batch start
#iterations: 51
currently lose_sum: 98.94969815015793
time_elpased: 1.738
batch start
#iterations: 52
currently lose_sum: 99.36538463830948
time_elpased: 1.744
batch start
#iterations: 53
currently lose_sum: 99.09175860881805
time_elpased: 1.751
batch start
#iterations: 54
currently lose_sum: 99.23474764823914
time_elpased: 1.792
batch start
#iterations: 55
currently lose_sum: 99.49788331985474
time_elpased: 1.764
batch start
#iterations: 56
currently lose_sum: 99.08983337879181
time_elpased: 1.777
batch start
#iterations: 57
currently lose_sum: 99.07443326711655
time_elpased: 1.751
batch start
#iterations: 58
currently lose_sum: 99.10171973705292
time_elpased: 1.758
batch start
#iterations: 59
currently lose_sum: 99.18194621801376
time_elpased: 1.784
start validation test
0.59675257732
0.614519056261
0.522745986002
0.564929647962
0.596874851737
65.176
batch start
#iterations: 60
currently lose_sum: 98.93351811170578
time_elpased: 1.727
batch start
#iterations: 61
currently lose_sum: 99.29254549741745
time_elpased: 1.795
batch start
#iterations: 62
currently lose_sum: 99.07219302654266
time_elpased: 1.724
batch start
#iterations: 63
currently lose_sum: 99.0207651257515
time_elpased: 1.742
batch start
#iterations: 64
currently lose_sum: 99.07154089212418
time_elpased: 1.791
batch start
#iterations: 65
currently lose_sum: 99.00028043985367
time_elpased: 1.749
batch start
#iterations: 66
currently lose_sum: 99.111632168293
time_elpased: 1.773
batch start
#iterations: 67
currently lose_sum: 99.05761158466339
time_elpased: 1.723
batch start
#iterations: 68
currently lose_sum: 98.82086372375488
time_elpased: 1.768
batch start
#iterations: 69
currently lose_sum: 99.03941422700882
time_elpased: 1.749
batch start
#iterations: 70
currently lose_sum: 98.987020611763
time_elpased: 1.775
batch start
#iterations: 71
currently lose_sum: 98.90338188409805
time_elpased: 1.78
batch start
#iterations: 72
currently lose_sum: 98.872773706913
time_elpased: 1.81
batch start
#iterations: 73
currently lose_sum: 98.93151921033859
time_elpased: 1.745
batch start
#iterations: 74
currently lose_sum: 99.15114235877991
time_elpased: 1.727
batch start
#iterations: 75
currently lose_sum: 99.16090649366379
time_elpased: 1.732
batch start
#iterations: 76
currently lose_sum: 99.18953227996826
time_elpased: 1.769
batch start
#iterations: 77
currently lose_sum: 98.86887109279633
time_elpased: 1.729
batch start
#iterations: 78
currently lose_sum: 99.03624755144119
time_elpased: 1.739
batch start
#iterations: 79
currently lose_sum: 98.8619926571846
time_elpased: 1.746
start validation test
0.597886597938
0.616159165352
0.522745986002
0.565621693858
0.598010745996
64.955
batch start
#iterations: 80
currently lose_sum: 98.89966183900833
time_elpased: 1.782
batch start
#iterations: 81
currently lose_sum: 98.96908444166183
time_elpased: 1.777
batch start
#iterations: 82
currently lose_sum: 98.87475210428238
time_elpased: 1.771
batch start
#iterations: 83
currently lose_sum: 98.80359095335007
time_elpased: 1.784
batch start
#iterations: 84
currently lose_sum: 98.72976678609848
time_elpased: 1.74
batch start
#iterations: 85
currently lose_sum: 98.7397775053978
time_elpased: 1.756
batch start
#iterations: 86
currently lose_sum: 98.8854192495346
time_elpased: 1.743
batch start
#iterations: 87
currently lose_sum: 98.7147096991539
time_elpased: 1.78
batch start
#iterations: 88
currently lose_sum: 99.01316803693771
time_elpased: 1.747
batch start
#iterations: 89
currently lose_sum: 98.84275245666504
time_elpased: 1.723
batch start
#iterations: 90
currently lose_sum: 98.78156965970993
time_elpased: 1.77
batch start
#iterations: 91
currently lose_sum: 98.84030944108963
time_elpased: 1.748
batch start
#iterations: 92
currently lose_sum: 98.68179720640182
time_elpased: 1.753
batch start
#iterations: 93
currently lose_sum: 98.69242990016937
time_elpased: 1.773
batch start
#iterations: 94
currently lose_sum: 98.98652988672256
time_elpased: 1.736
batch start
#iterations: 95
currently lose_sum: 99.02801382541656
time_elpased: 1.779
batch start
#iterations: 96
currently lose_sum: 98.77317315340042
time_elpased: 1.76
batch start
#iterations: 97
currently lose_sum: 98.76028764247894
time_elpased: 1.749
batch start
#iterations: 98
currently lose_sum: 98.6753261089325
time_elpased: 1.769
batch start
#iterations: 99
currently lose_sum: 98.75250375270844
time_elpased: 1.757
start validation test
0.597268041237
0.617920436237
0.513174145739
0.560697216756
0.597406981998
64.979
batch start
#iterations: 100
currently lose_sum: 98.9640080332756
time_elpased: 1.728
batch start
#iterations: 101
currently lose_sum: 98.7991042137146
time_elpased: 1.748
batch start
#iterations: 102
currently lose_sum: 98.83459377288818
time_elpased: 1.727
batch start
#iterations: 103
currently lose_sum: 98.62798696756363
time_elpased: 1.723
batch start
#iterations: 104
currently lose_sum: 98.57084041833878
time_elpased: 1.827
batch start
#iterations: 105
currently lose_sum: 98.70683413743973
time_elpased: 1.769
batch start
#iterations: 106
currently lose_sum: 98.62789595127106
time_elpased: 1.741
batch start
#iterations: 107
currently lose_sum: 98.73333740234375
time_elpased: 1.784
batch start
#iterations: 108
currently lose_sum: 98.64468562602997
time_elpased: 1.753
batch start
#iterations: 109
currently lose_sum: 98.62318187952042
time_elpased: 1.736
batch start
#iterations: 110
currently lose_sum: 98.48763817548752
time_elpased: 1.826
batch start
#iterations: 111
currently lose_sum: 98.80687808990479
time_elpased: 1.798
batch start
#iterations: 112
currently lose_sum: 98.58960419893265
time_elpased: 1.763
batch start
#iterations: 113
currently lose_sum: 98.71816045045853
time_elpased: 1.756
batch start
#iterations: 114
currently lose_sum: 98.64617305994034
time_elpased: 1.738
batch start
#iterations: 115
currently lose_sum: 98.66171896457672
time_elpased: 1.789
batch start
#iterations: 116
currently lose_sum: 98.65318161249161
time_elpased: 1.768
batch start
#iterations: 117
currently lose_sum: 98.59031987190247
time_elpased: 1.778
batch start
#iterations: 118
currently lose_sum: 98.91123968362808
time_elpased: 1.752
batch start
#iterations: 119
currently lose_sum: 98.70147198438644
time_elpased: 1.758
start validation test
0.57881443299
0.591800356506
0.512556607657
0.549335392422
0.57892390482
65.559
batch start
#iterations: 120
currently lose_sum: 98.58834564685822
time_elpased: 1.739
batch start
#iterations: 121
currently lose_sum: 98.42591029405594
time_elpased: 1.784
batch start
#iterations: 122
currently lose_sum: 98.59361463785172
time_elpased: 1.76
batch start
#iterations: 123
currently lose_sum: 98.65020436048508
time_elpased: 1.748
batch start
#iterations: 124
currently lose_sum: 98.46705138683319
time_elpased: 1.783
batch start
#iterations: 125
currently lose_sum: 98.6020936369896
time_elpased: 1.746
batch start
#iterations: 126
currently lose_sum: 98.31614863872528
time_elpased: 1.767
batch start
#iterations: 127
currently lose_sum: 98.68251067399979
time_elpased: 1.763
batch start
#iterations: 128
currently lose_sum: 98.44736403226852
time_elpased: 1.764
batch start
#iterations: 129
currently lose_sum: 98.48247349262238
time_elpased: 1.751
batch start
#iterations: 130
currently lose_sum: 98.74220472574234
time_elpased: 1.737
batch start
#iterations: 131
currently lose_sum: 98.2635850906372
time_elpased: 1.769
batch start
#iterations: 132
currently lose_sum: 98.58431774377823
time_elpased: 1.786
batch start
#iterations: 133
currently lose_sum: 98.49875855445862
time_elpased: 1.799
batch start
#iterations: 134
currently lose_sum: 98.52452689409256
time_elpased: 1.767
batch start
#iterations: 135
currently lose_sum: 98.40939682722092
time_elpased: 1.776
batch start
#iterations: 136
currently lose_sum: 98.45125532150269
time_elpased: 1.759
batch start
#iterations: 137
currently lose_sum: 98.36068278551102
time_elpased: 1.788
batch start
#iterations: 138
currently lose_sum: 98.53522461652756
time_elpased: 1.753
batch start
#iterations: 139
currently lose_sum: 98.42132490873337
time_elpased: 1.768
start validation test
0.599226804124
0.628526023043
0.488472622478
0.549719117392
0.599409793271
64.850
batch start
#iterations: 140
currently lose_sum: 98.2225387096405
time_elpased: 1.75
batch start
#iterations: 141
currently lose_sum: 98.45667654275894
time_elpased: 1.776
batch start
#iterations: 142
currently lose_sum: 98.24072182178497
time_elpased: 1.728
batch start
#iterations: 143
currently lose_sum: 98.33753168582916
time_elpased: 1.738
batch start
#iterations: 144
currently lose_sum: 98.53447240591049
time_elpased: 1.786
batch start
#iterations: 145
currently lose_sum: 98.25533837080002
time_elpased: 1.816
batch start
#iterations: 146
currently lose_sum: 98.36730521917343
time_elpased: 1.779
batch start
#iterations: 147
currently lose_sum: 98.42029547691345
time_elpased: 1.782
batch start
#iterations: 148
currently lose_sum: 98.03848397731781
time_elpased: 1.781
batch start
#iterations: 149
currently lose_sum: 98.39692509174347
time_elpased: 1.753
batch start
#iterations: 150
currently lose_sum: 98.30729639530182
time_elpased: 1.767
batch start
#iterations: 151
currently lose_sum: 98.54127937555313
time_elpased: 1.736
batch start
#iterations: 152
currently lose_sum: 98.11308300495148
time_elpased: 1.762
batch start
#iterations: 153
currently lose_sum: 98.3401609659195
time_elpased: 1.78
batch start
#iterations: 154
currently lose_sum: 98.32447057962418
time_elpased: 1.732
batch start
#iterations: 155
currently lose_sum: 98.2129173874855
time_elpased: 1.775
batch start
#iterations: 156
currently lose_sum: 97.97736775875092
time_elpased: 1.801
batch start
#iterations: 157
currently lose_sum: 98.21819388866425
time_elpased: 1.801
batch start
#iterations: 158
currently lose_sum: 98.43736290931702
time_elpased: 1.793
batch start
#iterations: 159
currently lose_sum: 98.19655895233154
time_elpased: 1.745
start validation test
0.582268041237
0.595861084681
0.515644298065
0.552858088722
0.582378117641
65.402
batch start
#iterations: 160
currently lose_sum: 98.12358403205872
time_elpased: 1.823
batch start
#iterations: 161
currently lose_sum: 98.27920526266098
time_elpased: 1.787
batch start
#iterations: 162
currently lose_sum: 98.05997627973557
time_elpased: 1.782
batch start
#iterations: 163
currently lose_sum: 98.12385028600693
time_elpased: 1.754
batch start
#iterations: 164
currently lose_sum: 98.21305876970291
time_elpased: 1.757
batch start
#iterations: 165
currently lose_sum: 98.00444775819778
time_elpased: 1.808
batch start
#iterations: 166
currently lose_sum: 98.20747405290604
time_elpased: 1.758
batch start
#iterations: 167
currently lose_sum: 98.22781890630722
time_elpased: 1.768
batch start
#iterations: 168
currently lose_sum: 98.21009957790375
time_elpased: 1.747
batch start
#iterations: 169
currently lose_sum: 98.10701614618301
time_elpased: 1.747
batch start
#iterations: 170
currently lose_sum: 97.95839989185333
time_elpased: 1.799
batch start
#iterations: 171
currently lose_sum: 98.15141266584396
time_elpased: 1.779
batch start
#iterations: 172
currently lose_sum: 98.19025313854218
time_elpased: 1.766
batch start
#iterations: 173
currently lose_sum: 97.96489238739014
time_elpased: 1.828
batch start
#iterations: 174
currently lose_sum: 98.09872472286224
time_elpased: 1.791
batch start
#iterations: 175
currently lose_sum: 97.97574943304062
time_elpased: 1.771
batch start
#iterations: 176
currently lose_sum: 97.88527876138687
time_elpased: 1.745
batch start
#iterations: 177
currently lose_sum: 97.92778646945953
time_elpased: 1.775
batch start
#iterations: 178
currently lose_sum: 98.00762438774109
time_elpased: 1.794
batch start
#iterations: 179
currently lose_sum: 98.24881142377853
time_elpased: 1.806
start validation test
0.587216494845
0.627159023228
0.433511733224
0.512658227848
0.587470447364
65.181
batch start
#iterations: 180
currently lose_sum: 97.90908771753311
time_elpased: 1.782
batch start
#iterations: 181
currently lose_sum: 98.05011588335037
time_elpased: 1.757
batch start
#iterations: 182
currently lose_sum: 98.23233890533447
time_elpased: 1.734
batch start
#iterations: 183
currently lose_sum: 97.86888992786407
time_elpased: 1.764
batch start
#iterations: 184
currently lose_sum: 98.01020044088364
time_elpased: 1.766
batch start
#iterations: 185
currently lose_sum: 97.77230626344681
time_elpased: 1.783
batch start
#iterations: 186
currently lose_sum: 98.00358998775482
time_elpased: 1.776
batch start
#iterations: 187
currently lose_sum: 98.04952120780945
time_elpased: 1.783
batch start
#iterations: 188
currently lose_sum: 97.94689303636551
time_elpased: 1.871
batch start
#iterations: 189
currently lose_sum: 97.86743092536926
time_elpased: 1.799
batch start
#iterations: 190
currently lose_sum: 97.95313447713852
time_elpased: 1.793
batch start
#iterations: 191
currently lose_sum: 98.13607734441757
time_elpased: 1.776
batch start
#iterations: 192
currently lose_sum: 97.95226806402206
time_elpased: 1.73
batch start
#iterations: 193
currently lose_sum: 97.77759557962418
time_elpased: 1.805
batch start
#iterations: 194
currently lose_sum: 97.8452427983284
time_elpased: 1.823
batch start
#iterations: 195
currently lose_sum: 98.0695686340332
time_elpased: 1.793
batch start
#iterations: 196
currently lose_sum: 97.98163115978241
time_elpased: 1.764
batch start
#iterations: 197
currently lose_sum: 97.64287251234055
time_elpased: 1.784
batch start
#iterations: 198
currently lose_sum: 97.74486184120178
time_elpased: 1.756
batch start
#iterations: 199
currently lose_sum: 97.77037632465363
time_elpased: 1.795
start validation test
0.592835051546
0.618556701031
0.487855084397
0.545485931296
0.59300850048
64.914
batch start
#iterations: 200
currently lose_sum: 97.75875699520111
time_elpased: 1.742
batch start
#iterations: 201
currently lose_sum: 97.54814058542252
time_elpased: 1.778
batch start
#iterations: 202
currently lose_sum: 97.83947783708572
time_elpased: 1.734
batch start
#iterations: 203
currently lose_sum: 97.93136340379715
time_elpased: 1.741
batch start
#iterations: 204
currently lose_sum: 97.87995779514313
time_elpased: 1.772
batch start
#iterations: 205
currently lose_sum: 97.90216773748398
time_elpased: 1.803
batch start
#iterations: 206
currently lose_sum: 97.8662856221199
time_elpased: 1.755
batch start
#iterations: 207
currently lose_sum: 97.56208527088165
time_elpased: 1.783
batch start
#iterations: 208
currently lose_sum: 97.50886285305023
time_elpased: 1.761
batch start
#iterations: 209
currently lose_sum: 97.90698790550232
time_elpased: 1.789
batch start
#iterations: 210
currently lose_sum: 97.80013585090637
time_elpased: 1.746
batch start
#iterations: 211
currently lose_sum: 97.49067276716232
time_elpased: 1.761
batch start
#iterations: 212
currently lose_sum: 97.51343858242035
time_elpased: 1.751
batch start
#iterations: 213
currently lose_sum: 97.50985741615295
time_elpased: 1.767
batch start
#iterations: 214
currently lose_sum: 97.63297963142395
time_elpased: 1.785
batch start
#iterations: 215
currently lose_sum: 97.7944433093071
time_elpased: 1.762
batch start
#iterations: 216
currently lose_sum: 97.62305414676666
time_elpased: 1.755
batch start
#iterations: 217
currently lose_sum: 97.6069216132164
time_elpased: 1.795
batch start
#iterations: 218
currently lose_sum: 97.57222831249237
time_elpased: 1.762
batch start
#iterations: 219
currently lose_sum: 97.65507024526596
time_elpased: 1.812
start validation test
0.58412371134
0.627082048118
0.41848497324
0.501975308642
0.584397381292
65.046
batch start
#iterations: 220
currently lose_sum: 97.37573158740997
time_elpased: 1.765
batch start
#iterations: 221
currently lose_sum: 97.23033457994461
time_elpased: 1.756
batch start
#iterations: 222
currently lose_sum: 97.4436320066452
time_elpased: 1.769
batch start
#iterations: 223
currently lose_sum: 97.04963678121567
time_elpased: 1.811
batch start
#iterations: 224
currently lose_sum: 97.62208652496338
time_elpased: 1.771
batch start
#iterations: 225
currently lose_sum: 97.38990938663483
time_elpased: 1.753
batch start
#iterations: 226
currently lose_sum: 97.37907469272614
time_elpased: 1.796
batch start
#iterations: 227
currently lose_sum: 97.49417835474014
time_elpased: 1.76
batch start
#iterations: 228
currently lose_sum: 97.59195238351822
time_elpased: 1.757
batch start
#iterations: 229
currently lose_sum: 97.56080436706543
time_elpased: 1.825
batch start
#iterations: 230
currently lose_sum: 97.58634626865387
time_elpased: 1.798
batch start
#iterations: 231
currently lose_sum: 97.71556651592255
time_elpased: 1.724
batch start
#iterations: 232
currently lose_sum: 97.67575705051422
time_elpased: 1.742
batch start
#iterations: 233
currently lose_sum: 97.47453385591507
time_elpased: 1.789
batch start
#iterations: 234
currently lose_sum: 97.44964677095413
time_elpased: 1.746
batch start
#iterations: 235
currently lose_sum: 97.76969021558762
time_elpased: 1.767
batch start
#iterations: 236
currently lose_sum: 97.55332952737808
time_elpased: 1.763
batch start
#iterations: 237
currently lose_sum: 97.44994956254959
time_elpased: 1.764
batch start
#iterations: 238
currently lose_sum: 97.10555911064148
time_elpased: 1.752
batch start
#iterations: 239
currently lose_sum: 97.59005343914032
time_elpased: 1.753
start validation test
0.571855670103
0.597807991121
0.443495265541
0.509217679036
0.572067748425
65.955
batch start
#iterations: 240
currently lose_sum: 97.52806913852692
time_elpased: 1.771
batch start
#iterations: 241
currently lose_sum: 97.47522509098053
time_elpased: 1.734
batch start
#iterations: 242
currently lose_sum: 97.40915805101395
time_elpased: 1.752
batch start
#iterations: 243
currently lose_sum: 97.55929452180862
time_elpased: 1.78
batch start
#iterations: 244
currently lose_sum: 97.19935059547424
time_elpased: 1.742
batch start
#iterations: 245
currently lose_sum: 97.39196544885635
time_elpased: 1.751
batch start
#iterations: 246
currently lose_sum: 97.38536477088928
time_elpased: 1.764
batch start
#iterations: 247
currently lose_sum: 97.5581163764
time_elpased: 1.743
batch start
#iterations: 248
currently lose_sum: 97.27657508850098
time_elpased: 1.773
batch start
#iterations: 249
currently lose_sum: 97.4867634177208
time_elpased: 1.754
batch start
#iterations: 250
currently lose_sum: 97.25645786523819
time_elpased: 1.778
batch start
#iterations: 251
currently lose_sum: 97.30923169851303
time_elpased: 1.755
batch start
#iterations: 252
currently lose_sum: 97.14681094884872
time_elpased: 1.786
batch start
#iterations: 253
currently lose_sum: 97.26492899656296
time_elpased: 1.783
batch start
#iterations: 254
currently lose_sum: 97.11487567424774
time_elpased: 1.763
batch start
#iterations: 255
currently lose_sum: 97.31798815727234
time_elpased: 1.779
batch start
#iterations: 256
currently lose_sum: 97.23808997869492
time_elpased: 1.737
batch start
#iterations: 257
currently lose_sum: 97.60966950654984
time_elpased: 1.777
batch start
#iterations: 258
currently lose_sum: 97.24431878328323
time_elpased: 1.777
batch start
#iterations: 259
currently lose_sum: 97.14080768823624
time_elpased: 1.749
start validation test
0.587113402062
0.623551564311
0.443083573487
0.518050541516
0.587351369561
65.210
batch start
#iterations: 260
currently lose_sum: 97.18561404943466
time_elpased: 1.822
batch start
#iterations: 261
currently lose_sum: 97.16966009140015
time_elpased: 1.767
batch start
#iterations: 262
currently lose_sum: 97.41785633563995
time_elpased: 1.765
batch start
#iterations: 263
currently lose_sum: 97.1545802950859
time_elpased: 1.782
batch start
#iterations: 264
currently lose_sum: 96.97314262390137
time_elpased: 1.803
batch start
#iterations: 265
currently lose_sum: 97.15837281942368
time_elpased: 1.764
batch start
#iterations: 266
currently lose_sum: 97.1200208067894
time_elpased: 1.796
batch start
#iterations: 267
currently lose_sum: 97.13138461112976
time_elpased: 1.788
batch start
#iterations: 268
currently lose_sum: 96.82732957601547
time_elpased: 1.79
batch start
#iterations: 269
currently lose_sum: 97.04048824310303
time_elpased: 1.753
batch start
#iterations: 270
currently lose_sum: 96.93542528152466
time_elpased: 1.756
batch start
#iterations: 271
currently lose_sum: 96.8670340180397
time_elpased: 1.784
batch start
#iterations: 272
currently lose_sum: 97.014917075634
time_elpased: 1.762
batch start
#iterations: 273
currently lose_sum: 97.10584276914597
time_elpased: 1.783
batch start
#iterations: 274
currently lose_sum: 97.24024868011475
time_elpased: 1.812
batch start
#iterations: 275
currently lose_sum: 96.77174633741379
time_elpased: 1.751
batch start
#iterations: 276
currently lose_sum: 97.16246742010117
time_elpased: 1.783
batch start
#iterations: 277
currently lose_sum: 96.9611833691597
time_elpased: 1.774
batch start
#iterations: 278
currently lose_sum: 97.16301774978638
time_elpased: 1.731
batch start
#iterations: 279
currently lose_sum: 96.87808746099472
time_elpased: 1.769
start validation test
0.58293814433
0.623875590791
0.421160971593
0.502857142857
0.583205434165
65.560
batch start
#iterations: 280
currently lose_sum: 96.92891192436218
time_elpased: 1.75
batch start
#iterations: 281
currently lose_sum: 96.84468054771423
time_elpased: 1.739
batch start
#iterations: 282
currently lose_sum: 96.66612923145294
time_elpased: 1.763
batch start
#iterations: 283
currently lose_sum: 97.0659328699112
time_elpased: 1.791
batch start
#iterations: 284
currently lose_sum: 96.94454091787338
time_elpased: 1.786
batch start
#iterations: 285
currently lose_sum: 96.90303581953049
time_elpased: 1.782
batch start
#iterations: 286
currently lose_sum: 96.98601061105728
time_elpased: 1.761
batch start
#iterations: 287
currently lose_sum: 96.92306911945343
time_elpased: 1.762
batch start
#iterations: 288
currently lose_sum: 97.12122166156769
time_elpased: 1.781
batch start
#iterations: 289
currently lose_sum: 96.9560438990593
time_elpased: 1.764
batch start
#iterations: 290
currently lose_sum: 96.53999549150467
time_elpased: 1.745
batch start
#iterations: 291
currently lose_sum: 96.97151124477386
time_elpased: 1.795
batch start
#iterations: 292
currently lose_sum: 96.52233111858368
time_elpased: 1.766
batch start
#iterations: 293
currently lose_sum: 96.97267991304398
time_elpased: 1.784
batch start
#iterations: 294
currently lose_sum: 96.9730321764946
time_elpased: 1.755
batch start
#iterations: 295
currently lose_sum: 96.81152522563934
time_elpased: 1.737
batch start
#iterations: 296
currently lose_sum: 96.78936356306076
time_elpased: 1.793
batch start
#iterations: 297
currently lose_sum: 96.8972190618515
time_elpased: 1.784
batch start
#iterations: 298
currently lose_sum: 96.61000275611877
time_elpased: 1.772
batch start
#iterations: 299
currently lose_sum: 97.24739468097687
time_elpased: 1.758
start validation test
0.578608247423
0.611844970242
0.433820502264
0.507678410117
0.578847467159
65.696
batch start
#iterations: 300
currently lose_sum: 96.9221960902214
time_elpased: 1.796
batch start
#iterations: 301
currently lose_sum: 96.70333337783813
time_elpased: 1.763
batch start
#iterations: 302
currently lose_sum: 96.87680125236511
time_elpased: 1.755
batch start
#iterations: 303
currently lose_sum: 96.79121840000153
time_elpased: 1.774
batch start
#iterations: 304
currently lose_sum: 96.33629447221756
time_elpased: 1.791
batch start
#iterations: 305
currently lose_sum: 96.68465918302536
time_elpased: 1.809
batch start
#iterations: 306
currently lose_sum: 96.56976568698883
time_elpased: 1.729
batch start
#iterations: 307
currently lose_sum: 96.81863301992416
time_elpased: 1.751
batch start
#iterations: 308
currently lose_sum: 96.80438446998596
time_elpased: 1.773
batch start
#iterations: 309
currently lose_sum: 96.77131527662277
time_elpased: 1.778
batch start
#iterations: 310
currently lose_sum: 97.04851686954498
time_elpased: 1.748
batch start
#iterations: 311
currently lose_sum: 96.55071157217026
time_elpased: 1.716
batch start
#iterations: 312
currently lose_sum: 96.73986220359802
time_elpased: 1.739
batch start
#iterations: 313
currently lose_sum: 96.60484325885773
time_elpased: 1.857
batch start
#iterations: 314
currently lose_sum: 96.57497531175613
time_elpased: 1.807
batch start
#iterations: 315
currently lose_sum: 96.66154676675797
time_elpased: 1.811
batch start
#iterations: 316
currently lose_sum: 96.78105318546295
time_elpased: 1.776
batch start
#iterations: 317
currently lose_sum: 96.6904114484787
time_elpased: 1.761
batch start
#iterations: 318
currently lose_sum: 96.74642443656921
time_elpased: 1.751
batch start
#iterations: 319
currently lose_sum: 96.80304497480392
time_elpased: 1.764
start validation test
0.58087628866
0.604510088356
0.471799094277
0.529972830799
0.581056507073
65.737
batch start
#iterations: 320
currently lose_sum: 96.46249562501907
time_elpased: 1.752
batch start
#iterations: 321
currently lose_sum: 96.57394707202911
time_elpased: 1.731
batch start
#iterations: 322
currently lose_sum: 96.68127846717834
time_elpased: 1.753
batch start
#iterations: 323
currently lose_sum: 96.30293697118759
time_elpased: 1.777
batch start
#iterations: 324
currently lose_sum: 96.70647358894348
time_elpased: 1.744
batch start
#iterations: 325
currently lose_sum: 96.35928601026535
time_elpased: 1.727
batch start
#iterations: 326
currently lose_sum: 96.62267816066742
time_elpased: 1.757
batch start
#iterations: 327
currently lose_sum: 96.55014073848724
time_elpased: 1.744
batch start
#iterations: 328
currently lose_sum: 96.32915532588959
time_elpased: 1.807
batch start
#iterations: 329
currently lose_sum: 96.38889241218567
time_elpased: 1.75
batch start
#iterations: 330
currently lose_sum: 96.52779054641724
time_elpased: 1.774
batch start
#iterations: 331
currently lose_sum: 96.59327924251556
time_elpased: 1.788
batch start
#iterations: 332
currently lose_sum: 96.54354798793793
time_elpased: 1.764
batch start
#iterations: 333
currently lose_sum: 96.55451339483261
time_elpased: 1.773
batch start
#iterations: 334
currently lose_sum: 96.35890138149261
time_elpased: 1.78
batch start
#iterations: 335
currently lose_sum: 96.38497054576874
time_elpased: 1.742
batch start
#iterations: 336
currently lose_sum: 96.21476364135742
time_elpased: 1.773
batch start
#iterations: 337
currently lose_sum: 96.4208710193634
time_elpased: 1.757
batch start
#iterations: 338
currently lose_sum: 96.30593460798264
time_elpased: 1.78
batch start
#iterations: 339
currently lose_sum: 96.3330317735672
time_elpased: 1.788
start validation test
0.574484536082
0.60814211695
0.422807739811
0.498816101026
0.574735137976
66.134
batch start
#iterations: 340
currently lose_sum: 96.2095827460289
time_elpased: 1.774
batch start
#iterations: 341
currently lose_sum: 96.4222019314766
time_elpased: 1.739
batch start
#iterations: 342
currently lose_sum: 96.4444734454155
time_elpased: 1.75
batch start
#iterations: 343
currently lose_sum: 96.49818885326385
time_elpased: 1.818
batch start
#iterations: 344
currently lose_sum: 96.28934425115585
time_elpased: 1.761
batch start
#iterations: 345
currently lose_sum: 96.23169416189194
time_elpased: 1.768
batch start
#iterations: 346
currently lose_sum: 96.27343916893005
time_elpased: 1.779
batch start
#iterations: 347
currently lose_sum: 96.0959712266922
time_elpased: 1.772
batch start
#iterations: 348
currently lose_sum: 96.39240729808807
time_elpased: 1.778
batch start
#iterations: 349
currently lose_sum: 96.40877395868301
time_elpased: 1.755
batch start
#iterations: 350
currently lose_sum: 96.43418610095978
time_elpased: 1.771
batch start
#iterations: 351
currently lose_sum: 96.4235650897026
time_elpased: 1.78
batch start
#iterations: 352
currently lose_sum: 95.9943340420723
time_elpased: 1.837
batch start
#iterations: 353
currently lose_sum: 95.98606365919113
time_elpased: 1.791
batch start
#iterations: 354
currently lose_sum: 96.13885027170181
time_elpased: 1.771
batch start
#iterations: 355
currently lose_sum: 96.24342203140259
time_elpased: 1.782
batch start
#iterations: 356
currently lose_sum: 96.07644879817963
time_elpased: 1.762
batch start
#iterations: 357
currently lose_sum: 96.01671838760376
time_elpased: 1.735
batch start
#iterations: 358
currently lose_sum: 96.19861352443695
time_elpased: 1.746
batch start
#iterations: 359
currently lose_sum: 95.88267767429352
time_elpased: 1.731
start validation test
0.580360824742
0.604678984448
0.468196788802
0.527756830443
0.580546143265
65.860
batch start
#iterations: 360
currently lose_sum: 96.25347888469696
time_elpased: 1.755
batch start
#iterations: 361
currently lose_sum: 96.05917286872864
time_elpased: 1.782
batch start
#iterations: 362
currently lose_sum: 96.0743003487587
time_elpased: 1.748
batch start
#iterations: 363
currently lose_sum: 96.03306341171265
time_elpased: 1.753
batch start
#iterations: 364
currently lose_sum: 96.05481320619583
time_elpased: 1.773
batch start
#iterations: 365
currently lose_sum: 95.97408658266068
time_elpased: 1.794
batch start
#iterations: 366
currently lose_sum: 96.04901820421219
time_elpased: 1.742
batch start
#iterations: 367
currently lose_sum: 95.8243277668953
time_elpased: 1.773
batch start
#iterations: 368
currently lose_sum: 95.56175023317337
time_elpased: 1.755
batch start
#iterations: 369
currently lose_sum: 95.99814021587372
time_elpased: 1.747
batch start
#iterations: 370
currently lose_sum: 95.99131768941879
time_elpased: 1.789
batch start
#iterations: 371
currently lose_sum: 95.85375422239304
time_elpased: 1.763
batch start
#iterations: 372
currently lose_sum: 96.07935154438019
time_elpased: 1.744
batch start
#iterations: 373
currently lose_sum: 95.58653873205185
time_elpased: 1.781
batch start
#iterations: 374
currently lose_sum: 95.83850306272507
time_elpased: 1.76
batch start
#iterations: 375
currently lose_sum: 96.0332579612732
time_elpased: 1.779
batch start
#iterations: 376
currently lose_sum: 95.69586384296417
time_elpased: 1.761
batch start
#iterations: 377
currently lose_sum: 96.06479942798615
time_elpased: 1.737
batch start
#iterations: 378
currently lose_sum: 95.84109950065613
time_elpased: 1.8
batch start
#iterations: 379
currently lose_sum: 95.82136005163193
time_elpased: 1.747
start validation test
0.577783505155
0.621166375338
0.402326060107
0.488350302955
0.57807339767
66.061
batch start
#iterations: 380
currently lose_sum: 95.76228779554367
time_elpased: 1.798
batch start
#iterations: 381
currently lose_sum: 95.69072604179382
time_elpased: 1.759
batch start
#iterations: 382
currently lose_sum: 96.1157398223877
time_elpased: 1.754
batch start
#iterations: 383
currently lose_sum: 95.94834238290787
time_elpased: 1.794
batch start
#iterations: 384
currently lose_sum: 95.82646358013153
time_elpased: 1.746
batch start
#iterations: 385
currently lose_sum: 95.77706915140152
time_elpased: 1.781
batch start
#iterations: 386
currently lose_sum: 96.12325567007065
time_elpased: 1.773
batch start
#iterations: 387
currently lose_sum: 95.6917034983635
time_elpased: 1.769
batch start
#iterations: 388
currently lose_sum: 95.7398442029953
time_elpased: 1.754
batch start
#iterations: 389
currently lose_sum: 95.59912043809891
time_elpased: 1.814
batch start
#iterations: 390
currently lose_sum: 95.49629193544388
time_elpased: 1.787
batch start
#iterations: 391
currently lose_sum: 95.76244276762009
time_elpased: 1.822
batch start
#iterations: 392
currently lose_sum: 95.94081330299377
time_elpased: 1.765
batch start
#iterations: 393
currently lose_sum: 95.75269794464111
time_elpased: 1.763
batch start
#iterations: 394
currently lose_sum: 95.82233840227127
time_elpased: 1.755
batch start
#iterations: 395
currently lose_sum: 95.7988156080246
time_elpased: 1.793
batch start
#iterations: 396
currently lose_sum: 95.90626847743988
time_elpased: 1.749
batch start
#iterations: 397
currently lose_sum: 95.74030834436417
time_elpased: 1.808
batch start
#iterations: 398
currently lose_sum: 95.47898137569427
time_elpased: 1.765
batch start
#iterations: 399
currently lose_sum: 95.66401952505112
time_elpased: 1.764
start validation test
0.574845360825
0.6089344019
0.422293124743
0.498723714598
0.575097409129
66.283
acc: 0.606
pre: 0.637
rec: 0.499
F1: 0.559
auc: 0.607
