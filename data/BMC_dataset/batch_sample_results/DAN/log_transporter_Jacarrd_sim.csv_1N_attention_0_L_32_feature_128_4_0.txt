start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.4216697216034
time_elpased: 2.08
batch start
#iterations: 1
currently lose_sum: 100.43362027406693
time_elpased: 2.044
batch start
#iterations: 2
currently lose_sum: 100.30361586809158
time_elpased: 2.092
batch start
#iterations: 3
currently lose_sum: 100.33273285627365
time_elpased: 2.09
batch start
#iterations: 4
currently lose_sum: 100.36721897125244
time_elpased: 2.103
batch start
#iterations: 5
currently lose_sum: 100.30447453260422
time_elpased: 2.079
batch start
#iterations: 6
currently lose_sum: 100.2779021859169
time_elpased: 2.09
batch start
#iterations: 7
currently lose_sum: 100.23352432250977
time_elpased: 2.104
batch start
#iterations: 8
currently lose_sum: 100.15873903036118
time_elpased: 2.081
batch start
#iterations: 9
currently lose_sum: 100.14829343557358
time_elpased: 2.068
batch start
#iterations: 10
currently lose_sum: 100.16535264253616
time_elpased: 2.025
batch start
#iterations: 11
currently lose_sum: 100.15785431861877
time_elpased: 2.091
batch start
#iterations: 12
currently lose_sum: 100.18722653388977
time_elpased: 2.085
batch start
#iterations: 13
currently lose_sum: 99.9522066116333
time_elpased: 2.066
batch start
#iterations: 14
currently lose_sum: 100.05692487955093
time_elpased: 2.087
batch start
#iterations: 15
currently lose_sum: 99.92645746469498
time_elpased: 2.082
batch start
#iterations: 16
currently lose_sum: 99.87715095281601
time_elpased: 2.059
batch start
#iterations: 17
currently lose_sum: 99.83255624771118
time_elpased: 2.091
batch start
#iterations: 18
currently lose_sum: 99.94269144535065
time_elpased: 2.074
batch start
#iterations: 19
currently lose_sum: 99.8837399482727
time_elpased: 2.068
start validation test
0.576546391753
0.59368762475
0.489760214058
0.536739412395
0.576698758273
65.956
batch start
#iterations: 20
currently lose_sum: 99.87531346082687
time_elpased: 2.075
batch start
#iterations: 21
currently lose_sum: 99.74267536401749
time_elpased: 2.01
batch start
#iterations: 22
currently lose_sum: 99.69065767526627
time_elpased: 2.029
batch start
#iterations: 23
currently lose_sum: 99.73714417219162
time_elpased: 2.061
batch start
#iterations: 24
currently lose_sum: 99.70127087831497
time_elpased: 2.078
batch start
#iterations: 25
currently lose_sum: 99.73732364177704
time_elpased: 2.03
batch start
#iterations: 26
currently lose_sum: 99.65594059228897
time_elpased: 2.066
batch start
#iterations: 27
currently lose_sum: 99.63831907510757
time_elpased: 2.063
batch start
#iterations: 28
currently lose_sum: 99.58991491794586
time_elpased: 2.054
batch start
#iterations: 29
currently lose_sum: 99.5973242521286
time_elpased: 2.079
batch start
#iterations: 30
currently lose_sum: 99.50027996301651
time_elpased: 2.073
batch start
#iterations: 31
currently lose_sum: 99.51518762111664
time_elpased: 2.067
batch start
#iterations: 32
currently lose_sum: 99.25152444839478
time_elpased: 2.098
batch start
#iterations: 33
currently lose_sum: 99.48994708061218
time_elpased: 2.073
batch start
#iterations: 34
currently lose_sum: 99.65562731027603
time_elpased: 2.087
batch start
#iterations: 35
currently lose_sum: 99.49325531721115
time_elpased: 2.054
batch start
#iterations: 36
currently lose_sum: 99.4247555732727
time_elpased: 2.044
batch start
#iterations: 37
currently lose_sum: 99.70738685131073
time_elpased: 2.1
batch start
#iterations: 38
currently lose_sum: 99.43456768989563
time_elpased: 2.1
batch start
#iterations: 39
currently lose_sum: 99.40599739551544
time_elpased: 2.061
start validation test
0.593711340206
0.621154100092
0.484100030874
0.544129554656
0.593903779766
65.735
batch start
#iterations: 40
currently lose_sum: 99.63626998662949
time_elpased: 2.042
batch start
#iterations: 41
currently lose_sum: 99.48479115962982
time_elpased: 2.064
batch start
#iterations: 42
currently lose_sum: 99.32481986284256
time_elpased: 2.07
batch start
#iterations: 43
currently lose_sum: 99.18127286434174
time_elpased: 2.062
batch start
#iterations: 44
currently lose_sum: 99.31843155622482
time_elpased: 2.025
batch start
#iterations: 45
currently lose_sum: 99.08701026439667
time_elpased: 2.059
batch start
#iterations: 46
currently lose_sum: 99.1250792145729
time_elpased: 2.059
batch start
#iterations: 47
currently lose_sum: 99.46457141637802
time_elpased: 2.084
batch start
#iterations: 48
currently lose_sum: 99.36067217588425
time_elpased: 2.066
batch start
#iterations: 49
currently lose_sum: 99.36622768640518
time_elpased: 2.063
batch start
#iterations: 50
currently lose_sum: 99.4559296965599
time_elpased: 2.051
batch start
#iterations: 51
currently lose_sum: 99.30055809020996
time_elpased: 2.046
batch start
#iterations: 52
currently lose_sum: 99.25224190950394
time_elpased: 2.065
batch start
#iterations: 53
currently lose_sum: 99.16379606723785
time_elpased: 2.044
batch start
#iterations: 54
currently lose_sum: 99.29382872581482
time_elpased: 2.078
batch start
#iterations: 55
currently lose_sum: 99.17969125509262
time_elpased: 2.045
batch start
#iterations: 56
currently lose_sum: 99.46768015623093
time_elpased: 2.022
batch start
#iterations: 57
currently lose_sum: 99.23879331350327
time_elpased: 2.027
batch start
#iterations: 58
currently lose_sum: 99.21923649311066
time_elpased: 2.055
batch start
#iterations: 59
currently lose_sum: 99.29377698898315
time_elpased: 2.041
start validation test
0.600567010309
0.623587038433
0.510960172893
0.561683353131
0.600724328933
65.116
batch start
#iterations: 60
currently lose_sum: 99.13504260778427
time_elpased: 2.034
batch start
#iterations: 61
currently lose_sum: 99.38150864839554
time_elpased: 2.061
batch start
#iterations: 62
currently lose_sum: 98.89385378360748
time_elpased: 2.079
batch start
#iterations: 63
currently lose_sum: 99.01300603151321
time_elpased: 2.045
batch start
#iterations: 64
currently lose_sum: 99.16556721925735
time_elpased: 2.051
batch start
#iterations: 65
currently lose_sum: 99.18468081951141
time_elpased: 2.077
batch start
#iterations: 66
currently lose_sum: 99.21064293384552
time_elpased: 2.074
batch start
#iterations: 67
currently lose_sum: 99.13268846273422
time_elpased: 2.093
batch start
#iterations: 68
currently lose_sum: 98.88121563196182
time_elpased: 2.083
batch start
#iterations: 69
currently lose_sum: 98.82169592380524
time_elpased: 2.053
batch start
#iterations: 70
currently lose_sum: 98.91650396585464
time_elpased: 2.064
batch start
#iterations: 71
currently lose_sum: 98.92938059568405
time_elpased: 2.119
batch start
#iterations: 72
currently lose_sum: 98.83279865980148
time_elpased: 2.026
batch start
#iterations: 73
currently lose_sum: 98.67133450508118
time_elpased: 2.138
batch start
#iterations: 74
currently lose_sum: 98.79694348573685
time_elpased: 2.146
batch start
#iterations: 75
currently lose_sum: 99.2054278254509
time_elpased: 2.105
batch start
#iterations: 76
currently lose_sum: 99.07375901937485
time_elpased: 2.06
batch start
#iterations: 77
currently lose_sum: 98.77327185869217
time_elpased: 2.095
batch start
#iterations: 78
currently lose_sum: 99.0124626159668
time_elpased: 2.103
batch start
#iterations: 79
currently lose_sum: 99.03317898511887
time_elpased: 2.062
start validation test
0.588865979381
0.596754473713
0.552536791191
0.573795019771
0.588929760875
65.551
batch start
#iterations: 80
currently lose_sum: 98.94473606348038
time_elpased: 2.049
batch start
#iterations: 81
currently lose_sum: 98.83915084600449
time_elpased: 2.08
batch start
#iterations: 82
currently lose_sum: 98.8706915974617
time_elpased: 2.072
batch start
#iterations: 83
currently lose_sum: 98.89068335294724
time_elpased: 2.113
batch start
#iterations: 84
currently lose_sum: 98.68447434902191
time_elpased: 2.09
batch start
#iterations: 85
currently lose_sum: 98.89051163196564
time_elpased: 2.07
batch start
#iterations: 86
currently lose_sum: 98.99502837657928
time_elpased: 2.073
batch start
#iterations: 87
currently lose_sum: 98.70047241449356
time_elpased: 2.026
batch start
#iterations: 88
currently lose_sum: 99.11342227458954
time_elpased: 2.014
batch start
#iterations: 89
currently lose_sum: 98.59744668006897
time_elpased: 2.054
batch start
#iterations: 90
currently lose_sum: 98.51754611730576
time_elpased: 2.055
batch start
#iterations: 91
currently lose_sum: 98.75090819597244
time_elpased: 2.028
batch start
#iterations: 92
currently lose_sum: 98.72512310743332
time_elpased: 2.054
batch start
#iterations: 93
currently lose_sum: 98.77532601356506
time_elpased: 2.105
batch start
#iterations: 94
currently lose_sum: 98.73549628257751
time_elpased: 2.054
batch start
#iterations: 95
currently lose_sum: 98.85744500160217
time_elpased: 2.058
batch start
#iterations: 96
currently lose_sum: 98.6778564453125
time_elpased: 2.083
batch start
#iterations: 97
currently lose_sum: 98.7324531674385
time_elpased: 2.07
batch start
#iterations: 98
currently lose_sum: 98.943756878376
time_elpased: 2.113
batch start
#iterations: 99
currently lose_sum: 98.70457226037979
time_elpased: 2.087
start validation test
0.585567010309
0.597875569044
0.527117423073
0.560271275432
0.585669627575
65.370
batch start
#iterations: 100
currently lose_sum: 98.76093316078186
time_elpased: 2.047
batch start
#iterations: 101
currently lose_sum: 98.58795917034149
time_elpased: 2.082
batch start
#iterations: 102
currently lose_sum: 98.58852475881577
time_elpased: 2.053
batch start
#iterations: 103
currently lose_sum: 98.80245876312256
time_elpased: 2.096
batch start
#iterations: 104
currently lose_sum: 98.56835913658142
time_elpased: 2.051
batch start
#iterations: 105
currently lose_sum: 98.92640554904938
time_elpased: 2.097
batch start
#iterations: 106
currently lose_sum: 98.85849785804749
time_elpased: 2.063
batch start
#iterations: 107
currently lose_sum: 98.52729421854019
time_elpased: 2.112
batch start
#iterations: 108
currently lose_sum: 98.73391193151474
time_elpased: 2.084
batch start
#iterations: 109
currently lose_sum: 98.47685080766678
time_elpased: 2.116
batch start
#iterations: 110
currently lose_sum: 98.63315773010254
time_elpased: 2.053
batch start
#iterations: 111
currently lose_sum: 98.65215265750885
time_elpased: 2.064
batch start
#iterations: 112
currently lose_sum: 98.81477987766266
time_elpased: 2.056
batch start
#iterations: 113
currently lose_sum: 98.51888537406921
time_elpased: 2.035
batch start
#iterations: 114
currently lose_sum: 98.8704845905304
time_elpased: 2.053
batch start
#iterations: 115
currently lose_sum: 98.84185719490051
time_elpased: 2.106
batch start
#iterations: 116
currently lose_sum: 98.55946409702301
time_elpased: 2.118
batch start
#iterations: 117
currently lose_sum: 98.44868618249893
time_elpased: 2.026
batch start
#iterations: 118
currently lose_sum: 98.43985491991043
time_elpased: 2.046
batch start
#iterations: 119
currently lose_sum: 98.4957212805748
time_elpased: 2.016
start validation test
0.600721649485
0.624134021917
0.509931048678
0.561282283643
0.600881046388
64.894
batch start
#iterations: 120
currently lose_sum: 98.77601653337479
time_elpased: 2.062
batch start
#iterations: 121
currently lose_sum: 98.43623012304306
time_elpased: 2.022
batch start
#iterations: 122
currently lose_sum: 98.61932635307312
time_elpased: 2.057
batch start
#iterations: 123
currently lose_sum: 98.63825488090515
time_elpased: 2.096
batch start
#iterations: 124
currently lose_sum: 98.63826394081116
time_elpased: 2.056
batch start
#iterations: 125
currently lose_sum: 98.5231037735939
time_elpased: 2.032
batch start
#iterations: 126
currently lose_sum: 98.4542680978775
time_elpased: 2.014
batch start
#iterations: 127
currently lose_sum: 98.58757334947586
time_elpased: 2.053
batch start
#iterations: 128
currently lose_sum: 98.63482427597046
time_elpased: 2.046
batch start
#iterations: 129
currently lose_sum: 98.7652952671051
time_elpased: 2.065
batch start
#iterations: 130
currently lose_sum: 98.65667760372162
time_elpased: 2.012
batch start
#iterations: 131
currently lose_sum: 98.27160686254501
time_elpased: 2.075
batch start
#iterations: 132
currently lose_sum: 98.64350068569183
time_elpased: 2.063
batch start
#iterations: 133
currently lose_sum: 98.35078817605972
time_elpased: 2.041
batch start
#iterations: 134
currently lose_sum: 98.17543512582779
time_elpased: 2.108
batch start
#iterations: 135
currently lose_sum: 98.41496431827545
time_elpased: 2.078
batch start
#iterations: 136
currently lose_sum: 98.37168490886688
time_elpased: 2.086
batch start
#iterations: 137
currently lose_sum: 98.58421820402145
time_elpased: 2.073
batch start
#iterations: 138
currently lose_sum: 98.15213131904602
time_elpased: 2.065
batch start
#iterations: 139
currently lose_sum: 98.30594700574875
time_elpased: 2.081
start validation test
0.599845360825
0.627313004952
0.495420397242
0.553619688345
0.600028694955
64.876
batch start
#iterations: 140
currently lose_sum: 98.358407497406
time_elpased: 2.06
batch start
#iterations: 141
currently lose_sum: 98.4431921839714
time_elpased: 2.104
batch start
#iterations: 142
currently lose_sum: 98.6090829372406
time_elpased: 2.024
batch start
#iterations: 143
currently lose_sum: 98.13134509325027
time_elpased: 2.069
batch start
#iterations: 144
currently lose_sum: 98.38054198026657
time_elpased: 2.044
batch start
#iterations: 145
currently lose_sum: 98.48621326684952
time_elpased: 2.094
batch start
#iterations: 146
currently lose_sum: 98.4498445391655
time_elpased: 2.043
batch start
#iterations: 147
currently lose_sum: 98.44929021596909
time_elpased: 2.134
batch start
#iterations: 148
currently lose_sum: 98.71702718734741
time_elpased: 2.101
batch start
#iterations: 149
currently lose_sum: 98.2447983622551
time_elpased: 2.084
batch start
#iterations: 150
currently lose_sum: 98.39350014925003
time_elpased: 2.048
batch start
#iterations: 151
currently lose_sum: 98.34579938650131
time_elpased: 2.099
batch start
#iterations: 152
currently lose_sum: 98.538314640522
time_elpased: 2.052
batch start
#iterations: 153
currently lose_sum: 98.13186645507812
time_elpased: 2.107
batch start
#iterations: 154
currently lose_sum: 98.62093245983124
time_elpased: 2.038
batch start
#iterations: 155
currently lose_sum: 98.29325413703918
time_elpased: 2.057
batch start
#iterations: 156
currently lose_sum: 98.19428062438965
time_elpased: 2.029
batch start
#iterations: 157
currently lose_sum: 98.16915196180344
time_elpased: 2.086
batch start
#iterations: 158
currently lose_sum: 98.11284947395325
time_elpased: 2.051
batch start
#iterations: 159
currently lose_sum: 98.23579603433609
time_elpased: 2.106
start validation test
0.601237113402
0.627166516883
0.502727179171
0.558094367645
0.601410062786
64.640
batch start
#iterations: 160
currently lose_sum: 98.35161197185516
time_elpased: 2.101
batch start
#iterations: 161
currently lose_sum: 98.4957839846611
time_elpased: 2.082
batch start
#iterations: 162
currently lose_sum: 98.19506800174713
time_elpased: 2.132
batch start
#iterations: 163
currently lose_sum: 98.20099127292633
time_elpased: 2.09
batch start
#iterations: 164
currently lose_sum: 98.09706383943558
time_elpased: 2.102
batch start
#iterations: 165
currently lose_sum: 98.44149488210678
time_elpased: 2.065
batch start
#iterations: 166
currently lose_sum: 98.3445126414299
time_elpased: 2.031
batch start
#iterations: 167
currently lose_sum: 98.17108565568924
time_elpased: 2.062
batch start
#iterations: 168
currently lose_sum: 98.21634727716446
time_elpased: 2.056
batch start
#iterations: 169
currently lose_sum: 98.04590117931366
time_elpased: 2.077
batch start
#iterations: 170
currently lose_sum: 98.23901969194412
time_elpased: 2.078
batch start
#iterations: 171
currently lose_sum: 98.2587422132492
time_elpased: 2.133
batch start
#iterations: 172
currently lose_sum: 98.28268605470657
time_elpased: 2.018
batch start
#iterations: 173
currently lose_sum: 97.99682939052582
time_elpased: 2.091
batch start
#iterations: 174
currently lose_sum: 98.5142183303833
time_elpased: 2.085
batch start
#iterations: 175
currently lose_sum: 98.24446141719818
time_elpased: 2.035
batch start
#iterations: 176
currently lose_sum: 98.18272405862808
time_elpased: 2.07
batch start
#iterations: 177
currently lose_sum: 98.0007358789444
time_elpased: 2.101
batch start
#iterations: 178
currently lose_sum: 98.12370240688324
time_elpased: 2.049
batch start
#iterations: 179
currently lose_sum: 98.3357143998146
time_elpased: 2.052
start validation test
0.600309278351
0.634618022219
0.476175774416
0.544096895579
0.600527213863
64.834
batch start
#iterations: 180
currently lose_sum: 98.27661246061325
time_elpased: 2.07
batch start
#iterations: 181
currently lose_sum: 98.02403444051743
time_elpased: 2.114
batch start
#iterations: 182
currently lose_sum: 98.25269079208374
time_elpased: 2.094
batch start
#iterations: 183
currently lose_sum: 98.1050773859024
time_elpased: 2.068
batch start
#iterations: 184
currently lose_sum: 98.11782091856003
time_elpased: 2.089
batch start
#iterations: 185
currently lose_sum: 97.844686627388
time_elpased: 2.058
batch start
#iterations: 186
currently lose_sum: 98.1104565858841
time_elpased: 2.042
batch start
#iterations: 187
currently lose_sum: 97.87860548496246
time_elpased: 2.025
batch start
#iterations: 188
currently lose_sum: 97.9534101486206
time_elpased: 2.08
batch start
#iterations: 189
currently lose_sum: 98.10378986597061
time_elpased: 2.082
batch start
#iterations: 190
currently lose_sum: 98.07298451662064
time_elpased: 2.077
batch start
#iterations: 191
currently lose_sum: 97.73493564128876
time_elpased: 2.061
batch start
#iterations: 192
currently lose_sum: 98.02767932415009
time_elpased: 2.077
batch start
#iterations: 193
currently lose_sum: 98.08386892080307
time_elpased: 2.088
batch start
#iterations: 194
currently lose_sum: 97.97736477851868
time_elpased: 2.098
batch start
#iterations: 195
currently lose_sum: 98.26739293336868
time_elpased: 2.109
batch start
#iterations: 196
currently lose_sum: 97.89786738157272
time_elpased: 2.094
batch start
#iterations: 197
currently lose_sum: 98.25205773115158
time_elpased: 2.086
batch start
#iterations: 198
currently lose_sum: 97.90442824363708
time_elpased: 2.06
batch start
#iterations: 199
currently lose_sum: 98.02947783470154
time_elpased: 2.051
start validation test
0.592525773196
0.623736683966
0.470001029124
0.536064323024
0.592740884282
65.197
batch start
#iterations: 200
currently lose_sum: 98.10259479284286
time_elpased: 2.055
batch start
#iterations: 201
currently lose_sum: 98.04557865858078
time_elpased: 2.064
batch start
#iterations: 202
currently lose_sum: 97.7005078792572
time_elpased: 2.069
batch start
#iterations: 203
currently lose_sum: 98.01123112440109
time_elpased: 2.098
batch start
#iterations: 204
currently lose_sum: 97.97637385129929
time_elpased: 2.042
batch start
#iterations: 205
currently lose_sum: 98.06150722503662
time_elpased: 2.084
batch start
#iterations: 206
currently lose_sum: 97.83570837974548
time_elpased: 2.063
batch start
#iterations: 207
currently lose_sum: 97.89841043949127
time_elpased: 2.071
batch start
#iterations: 208
currently lose_sum: 97.705635368824
time_elpased: 2.065
batch start
#iterations: 209
currently lose_sum: 97.75565665960312
time_elpased: 2.06
batch start
#iterations: 210
currently lose_sum: 97.81016492843628
time_elpased: 2.076
batch start
#iterations: 211
currently lose_sum: 97.63306826353073
time_elpased: 2.091
batch start
#iterations: 212
currently lose_sum: 97.88680303096771
time_elpased: 2.082
batch start
#iterations: 213
currently lose_sum: 97.81427717208862
time_elpased: 2.099
batch start
#iterations: 214
currently lose_sum: 97.94146925210953
time_elpased: 2.066
batch start
#iterations: 215
currently lose_sum: 97.4638352394104
time_elpased: 2.056
batch start
#iterations: 216
currently lose_sum: 97.82222181558609
time_elpased: 2.108
batch start
#iterations: 217
currently lose_sum: 97.32517743110657
time_elpased: 2.088
batch start
#iterations: 218
currently lose_sum: 97.86198753118515
time_elpased: 2.06
batch start
#iterations: 219
currently lose_sum: 97.70993942022324
time_elpased: 2.075
start validation test
0.593969072165
0.628707330722
0.462488422353
0.532938037355
0.594199906725
65.346
batch start
#iterations: 220
currently lose_sum: 97.81040495634079
time_elpased: 2.107
batch start
#iterations: 221
currently lose_sum: 97.9284279346466
time_elpased: 2.079
batch start
#iterations: 222
currently lose_sum: 97.48449331521988
time_elpased: 2.064
batch start
#iterations: 223
currently lose_sum: 97.24998080730438
time_elpased: 2.044
batch start
#iterations: 224
currently lose_sum: 97.63335800170898
time_elpased: 2.068
batch start
#iterations: 225
currently lose_sum: 97.17756599187851
time_elpased: 2.093
batch start
#iterations: 226
currently lose_sum: 97.47832560539246
time_elpased: 2.052
batch start
#iterations: 227
currently lose_sum: 97.75236642360687
time_elpased: 2.04
batch start
#iterations: 228
currently lose_sum: 97.70443284511566
time_elpased: 2.061
batch start
#iterations: 229
currently lose_sum: 97.6610631942749
time_elpased: 2.052
batch start
#iterations: 230
currently lose_sum: 97.21133762598038
time_elpased: 2.063
batch start
#iterations: 231
currently lose_sum: 97.96839326620102
time_elpased: 2.109
batch start
#iterations: 232
currently lose_sum: 97.61020988225937
time_elpased: 2.068
batch start
#iterations: 233
currently lose_sum: 97.20087921619415
time_elpased: 2.058
batch start
#iterations: 234
currently lose_sum: 97.53677350282669
time_elpased: 2.078
batch start
#iterations: 235
currently lose_sum: 97.60539597272873
time_elpased: 2.111
batch start
#iterations: 236
currently lose_sum: 97.35621505975723
time_elpased: 2.066
batch start
#iterations: 237
currently lose_sum: 97.65520423650742
time_elpased: 2.065
batch start
#iterations: 238
currently lose_sum: 97.76266646385193
time_elpased: 2.05
batch start
#iterations: 239
currently lose_sum: 97.51583451032639
time_elpased: 2.052
start validation test
0.591494845361
0.614111054508
0.496243696614
0.54892139564
0.591662073444
65.162
batch start
#iterations: 240
currently lose_sum: 97.50133872032166
time_elpased: 2.091
batch start
#iterations: 241
currently lose_sum: 97.54757964611053
time_elpased: 2.074
batch start
#iterations: 242
currently lose_sum: 97.16281294822693
time_elpased: 2.049
batch start
#iterations: 243
currently lose_sum: 97.57420599460602
time_elpased: 2.093
batch start
#iterations: 244
currently lose_sum: 97.51976001262665
time_elpased: 2.095
batch start
#iterations: 245
currently lose_sum: 97.2412138581276
time_elpased: 2.066
batch start
#iterations: 246
currently lose_sum: 97.472416639328
time_elpased: 2.143
batch start
#iterations: 247
currently lose_sum: 97.6303921341896
time_elpased: 2.088
batch start
#iterations: 248
currently lose_sum: 97.52093452215195
time_elpased: 2.07
batch start
#iterations: 249
currently lose_sum: 97.43350058794022
time_elpased: 2.08
batch start
#iterations: 250
currently lose_sum: 97.66021060943604
time_elpased: 2.075
batch start
#iterations: 251
currently lose_sum: 97.48605400323868
time_elpased: 2.106
batch start
#iterations: 252
currently lose_sum: 97.29307740926743
time_elpased: 2.083
batch start
#iterations: 253
currently lose_sum: 97.3705472946167
time_elpased: 2.055
batch start
#iterations: 254
currently lose_sum: 97.2878789305687
time_elpased: 2.076
batch start
#iterations: 255
currently lose_sum: 97.31787347793579
time_elpased: 2.111
batch start
#iterations: 256
currently lose_sum: 97.16259759664536
time_elpased: 2.049
batch start
#iterations: 257
currently lose_sum: 97.30031734704971
time_elpased: 2.054
batch start
#iterations: 258
currently lose_sum: 97.37597370147705
time_elpased: 2.06
batch start
#iterations: 259
currently lose_sum: 97.2324755191803
time_elpased: 2.081
start validation test
0.598195876289
0.628820375335
0.482762169394
0.546195493975
0.598398537966
64.756
batch start
#iterations: 260
currently lose_sum: 97.14215624332428
time_elpased: 2.064
batch start
#iterations: 261
currently lose_sum: 97.28864496946335
time_elpased: 2.083
batch start
#iterations: 262
currently lose_sum: 96.90562802553177
time_elpased: 2.017
batch start
#iterations: 263
currently lose_sum: 96.95408344268799
time_elpased: 2.093
batch start
#iterations: 264
currently lose_sum: 97.25661343336105
time_elpased: 2.046
batch start
#iterations: 265
currently lose_sum: 97.05621302127838
time_elpased: 2.035
batch start
#iterations: 266
currently lose_sum: 96.79703521728516
time_elpased: 2.065
batch start
#iterations: 267
currently lose_sum: 97.28524202108383
time_elpased: 2.108
batch start
#iterations: 268
currently lose_sum: 97.30838346481323
time_elpased: 2.048
batch start
#iterations: 269
currently lose_sum: 96.99685108661652
time_elpased: 2.045
batch start
#iterations: 270
currently lose_sum: 96.9794493317604
time_elpased: 2.111
batch start
#iterations: 271
currently lose_sum: 97.00916427373886
time_elpased: 2.141
batch start
#iterations: 272
currently lose_sum: 96.98277485370636
time_elpased: 2.096
batch start
#iterations: 273
currently lose_sum: 97.13721829652786
time_elpased: 2.052
batch start
#iterations: 274
currently lose_sum: 96.74081027507782
time_elpased: 2.053
batch start
#iterations: 275
currently lose_sum: 97.4079236984253
time_elpased: 2.091
batch start
#iterations: 276
currently lose_sum: 96.85339856147766
time_elpased: 2.072
batch start
#iterations: 277
currently lose_sum: 97.2329495549202
time_elpased: 2.048
batch start
#iterations: 278
currently lose_sum: 96.70655179023743
time_elpased: 2.047
batch start
#iterations: 279
currently lose_sum: 96.93360781669617
time_elpased: 2.053
start validation test
0.595257731959
0.625962447656
0.476896161367
0.541355140187
0.595465533952
65.384
batch start
#iterations: 280
currently lose_sum: 96.77722585201263
time_elpased: 2.095
batch start
#iterations: 281
currently lose_sum: 96.76614487171173
time_elpased: 2.102
batch start
#iterations: 282
currently lose_sum: 96.62611132860184
time_elpased: 2.077
batch start
#iterations: 283
currently lose_sum: 96.86050897836685
time_elpased: 2.075
batch start
#iterations: 284
currently lose_sum: 96.56902253627777
time_elpased: 2.078
batch start
#iterations: 285
currently lose_sum: 96.82889634370804
time_elpased: 2.045
batch start
#iterations: 286
currently lose_sum: 96.73158490657806
time_elpased: 2.024
batch start
#iterations: 287
currently lose_sum: 96.80945062637329
time_elpased: 2.073
batch start
#iterations: 288
currently lose_sum: 96.5529317855835
time_elpased: 2.045
batch start
#iterations: 289
currently lose_sum: 96.16759592294693
time_elpased: 2.056
batch start
#iterations: 290
currently lose_sum: 96.62953704595566
time_elpased: 2.082
batch start
#iterations: 291
currently lose_sum: 96.47114968299866
time_elpased: 2.033
batch start
#iterations: 292
currently lose_sum: 96.5204616189003
time_elpased: 2.026
batch start
#iterations: 293
currently lose_sum: 95.9935691356659
time_elpased: 2.047
batch start
#iterations: 294
currently lose_sum: 96.82970380783081
time_elpased: 2.063
batch start
#iterations: 295
currently lose_sum: 96.57512134313583
time_elpased: 2.045
batch start
#iterations: 296
currently lose_sum: 96.19520395994186
time_elpased: 2.058
batch start
#iterations: 297
currently lose_sum: 96.41183423995972
time_elpased: 2.06
batch start
#iterations: 298
currently lose_sum: 96.6643842458725
time_elpased: 2.039
batch start
#iterations: 299
currently lose_sum: 96.28971642255783
time_elpased: 2.086
start validation test
0.591237113402
0.624668620064
0.460738911187
0.530324567638
0.591466223124
65.760
batch start
#iterations: 300
currently lose_sum: 96.5631822347641
time_elpased: 2.045
batch start
#iterations: 301
currently lose_sum: 96.46912318468094
time_elpased: 2.066
batch start
#iterations: 302
currently lose_sum: 96.47280436754227
time_elpased: 2.036
batch start
#iterations: 303
currently lose_sum: 96.38759189844131
time_elpased: 2.114
batch start
#iterations: 304
currently lose_sum: 96.50043028593063
time_elpased: 2.119
batch start
#iterations: 305
currently lose_sum: 96.5169637799263
time_elpased: 2.036
batch start
#iterations: 306
currently lose_sum: 96.51032423973083
time_elpased: 2.06
batch start
#iterations: 307
currently lose_sum: 95.82847279310226
time_elpased: 2.087
batch start
#iterations: 308
currently lose_sum: 96.34116685390472
time_elpased: 2.095
batch start
#iterations: 309
currently lose_sum: 96.51960402727127
time_elpased: 2.049
batch start
#iterations: 310
currently lose_sum: 96.50243949890137
time_elpased: 2.081
batch start
#iterations: 311
currently lose_sum: 95.80064004659653
time_elpased: 2.037
batch start
#iterations: 312
currently lose_sum: 95.65069544315338
time_elpased: 2.049
batch start
#iterations: 313
currently lose_sum: 96.22245413064957
time_elpased: 2.048
batch start
#iterations: 314
currently lose_sum: 96.36291354894638
time_elpased: 2.041
batch start
#iterations: 315
currently lose_sum: 96.20971977710724
time_elpased: 2.067
batch start
#iterations: 316
currently lose_sum: 96.29682433605194
time_elpased: 2.05
batch start
#iterations: 317
currently lose_sum: 96.01216125488281
time_elpased: 2.083
batch start
#iterations: 318
currently lose_sum: 95.94105803966522
time_elpased: 2.08
batch start
#iterations: 319
currently lose_sum: 96.09994089603424
time_elpased: 2.089
start validation test
0.595463917526
0.631527093596
0.461768035402
0.533468077518
0.595698641268
65.761
batch start
#iterations: 320
currently lose_sum: 96.13745045661926
time_elpased: 2.055
batch start
#iterations: 321
currently lose_sum: 96.08424079418182
time_elpased: 2.036
batch start
#iterations: 322
currently lose_sum: 95.96293443441391
time_elpased: 2.076
batch start
#iterations: 323
currently lose_sum: 95.7692220211029
time_elpased: 2.103
batch start
#iterations: 324
currently lose_sum: 95.81289899349213
time_elpased: 2.007
batch start
#iterations: 325
currently lose_sum: 95.59489631652832
time_elpased: 2.068
batch start
#iterations: 326
currently lose_sum: 95.61711525917053
time_elpased: 2.085
batch start
#iterations: 327
currently lose_sum: 96.26531314849854
time_elpased: 2.111
batch start
#iterations: 328
currently lose_sum: 95.8629869222641
time_elpased: 2.058
batch start
#iterations: 329
currently lose_sum: 95.57032895088196
time_elpased: 2.077
batch start
#iterations: 330
currently lose_sum: 95.60236489772797
time_elpased: 2.062
batch start
#iterations: 331
currently lose_sum: 95.67523789405823
time_elpased: 2.109
batch start
#iterations: 332
currently lose_sum: 95.644580245018
time_elpased: 2.062
batch start
#iterations: 333
currently lose_sum: 95.66943258047104
time_elpased: 2.046
batch start
#iterations: 334
currently lose_sum: 95.88015240430832
time_elpased: 2.037
batch start
#iterations: 335
currently lose_sum: 95.6312125325203
time_elpased: 2.05
batch start
#iterations: 336
currently lose_sum: 95.47718054056168
time_elpased: 2.021
batch start
#iterations: 337
currently lose_sum: 95.5583907365799
time_elpased: 2.124
batch start
#iterations: 338
currently lose_sum: 95.13271725177765
time_elpased: 2.043
batch start
#iterations: 339
currently lose_sum: 95.66704511642456
time_elpased: 2.105
start validation test
0.583556701031
0.62223880597
0.429041885356
0.507888164707
0.583827975622
66.457
batch start
#iterations: 340
currently lose_sum: 95.60007911920547
time_elpased: 2.073
batch start
#iterations: 341
currently lose_sum: 95.53456151485443
time_elpased: 2.078
batch start
#iterations: 342
currently lose_sum: 95.62412983179092
time_elpased: 2.06
batch start
#iterations: 343
currently lose_sum: 95.34858709573746
time_elpased: 2.072
batch start
#iterations: 344
currently lose_sum: 95.12452292442322
time_elpased: 2.035
batch start
#iterations: 345
currently lose_sum: 95.30520117282867
time_elpased: 2.079
batch start
#iterations: 346
currently lose_sum: 95.2013857960701
time_elpased: 2.095
batch start
#iterations: 347
currently lose_sum: 95.31448268890381
time_elpased: 2.075
batch start
#iterations: 348
currently lose_sum: 95.2647477388382
time_elpased: 2.037
batch start
#iterations: 349
currently lose_sum: 94.80344104766846
time_elpased: 2.104
batch start
#iterations: 350
currently lose_sum: 94.97684222459793
time_elpased: 2.052
batch start
#iterations: 351
currently lose_sum: 94.80215269327164
time_elpased: 2.058
batch start
#iterations: 352
currently lose_sum: 95.34724420309067
time_elpased: 2.065
batch start
#iterations: 353
currently lose_sum: 94.5924745798111
time_elpased: 2.08
batch start
#iterations: 354
currently lose_sum: 95.03624379634857
time_elpased: 2.092
batch start
#iterations: 355
currently lose_sum: 94.41292011737823
time_elpased: 2.045
batch start
#iterations: 356
currently lose_sum: 94.76360714435577
time_elpased: 2.082
batch start
#iterations: 357
currently lose_sum: 94.74909108877182
time_elpased: 2.092
batch start
#iterations: 358
currently lose_sum: 94.7112153172493
time_elpased: 2.124
batch start
#iterations: 359
currently lose_sum: 94.210145175457
time_elpased: 2.06
start validation test
0.584226804124
0.611841213928
0.464752495626
0.528248918002
0.5844365597
67.543
batch start
#iterations: 360
currently lose_sum: 94.46258819103241
time_elpased: 2.074
batch start
#iterations: 361
currently lose_sum: 94.38353097438812
time_elpased: 2.09
batch start
#iterations: 362
currently lose_sum: 94.22876989841461
time_elpased: 2.118
batch start
#iterations: 363
currently lose_sum: 94.29563671350479
time_elpased: 2.136
batch start
#iterations: 364
currently lose_sum: 94.8219085931778
time_elpased: 2.062
batch start
#iterations: 365
currently lose_sum: 94.2854716181755
time_elpased: 2.097
batch start
#iterations: 366
currently lose_sum: 94.15538555383682
time_elpased: 2.06
batch start
#iterations: 367
currently lose_sum: 94.49935966730118
time_elpased: 2.079
batch start
#iterations: 368
currently lose_sum: 93.89101272821426
time_elpased: 2.063
batch start
#iterations: 369
currently lose_sum: 93.87597692012787
time_elpased: 2.081
batch start
#iterations: 370
currently lose_sum: 93.98626691102982
time_elpased: 2.041
batch start
#iterations: 371
currently lose_sum: 94.1262640953064
time_elpased: 2.038
batch start
#iterations: 372
currently lose_sum: 93.62978929281235
time_elpased: 2.057
batch start
#iterations: 373
currently lose_sum: 94.05240523815155
time_elpased: 2.032
batch start
#iterations: 374
currently lose_sum: 94.32853561639786
time_elpased: 2.031
batch start
#iterations: 375
currently lose_sum: 93.81575125455856
time_elpased: 2.043
batch start
#iterations: 376
currently lose_sum: 94.06542736291885
time_elpased: 2.02
batch start
#iterations: 377
currently lose_sum: 93.70466059446335
time_elpased: 2.011
batch start
#iterations: 378
currently lose_sum: 93.79747653007507
time_elpased: 2.042
batch start
#iterations: 379
currently lose_sum: 93.29017794132233
time_elpased: 2.037
start validation test
0.580515463918
0.617502604554
0.426983636925
0.504867364322
0.58078501272
68.969
batch start
#iterations: 380
currently lose_sum: 93.67620122432709
time_elpased: 2.05
batch start
#iterations: 381
currently lose_sum: 93.37137603759766
time_elpased: 2.112
batch start
#iterations: 382
currently lose_sum: 93.52345889806747
time_elpased: 2.073
batch start
#iterations: 383
currently lose_sum: 93.29842036962509
time_elpased: 2.035
batch start
#iterations: 384
currently lose_sum: 93.25343602895737
time_elpased: 2.06
batch start
#iterations: 385
currently lose_sum: 92.84894162416458
time_elpased: 2.05
batch start
#iterations: 386
currently lose_sum: 93.53191810846329
time_elpased: 2.009
batch start
#iterations: 387
currently lose_sum: 93.60095250606537
time_elpased: 2.031
batch start
#iterations: 388
currently lose_sum: 92.86260962486267
time_elpased: 2.079
batch start
#iterations: 389
currently lose_sum: 92.99336904287338
time_elpased: 2.083
batch start
#iterations: 390
currently lose_sum: 93.08841454982758
time_elpased: 2.12
batch start
#iterations: 391
currently lose_sum: 92.98440271615982
time_elpased: 2.05
batch start
#iterations: 392
currently lose_sum: 92.75518482923508
time_elpased: 2.025
batch start
#iterations: 393
currently lose_sum: 92.06617349386215
time_elpased: 2.054
batch start
#iterations: 394
currently lose_sum: 92.9848421216011
time_elpased: 2.025
batch start
#iterations: 395
currently lose_sum: 92.40073531866074
time_elpased: 2.1
batch start
#iterations: 396
currently lose_sum: 92.77912330627441
time_elpased: 2.026
batch start
#iterations: 397
currently lose_sum: 92.62420719861984
time_elpased: 2.037
batch start
#iterations: 398
currently lose_sum: 92.24991571903229
time_elpased: 2.058
batch start
#iterations: 399
currently lose_sum: 92.33455610275269
time_elpased: 2.073
start validation test
0.575051546392
0.606940612749
0.430173921992
0.503493134185
0.575305901407
69.722
acc: 0.600
pre: 0.626
rec: 0.499
F1: 0.555
auc: 0.600
