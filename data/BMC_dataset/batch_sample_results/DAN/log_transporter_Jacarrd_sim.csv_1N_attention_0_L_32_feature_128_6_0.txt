start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.38993698358536
time_elpased: 2.347
batch start
#iterations: 1
currently lose_sum: 100.42835676670074
time_elpased: 2.298
batch start
#iterations: 2
currently lose_sum: 100.2938141822815
time_elpased: 2.328
batch start
#iterations: 3
currently lose_sum: 100.33609968423843
time_elpased: 2.33
batch start
#iterations: 4
currently lose_sum: 100.36673825979233
time_elpased: 2.352
batch start
#iterations: 5
currently lose_sum: 100.30311495065689
time_elpased: 2.325
batch start
#iterations: 6
currently lose_sum: 100.2891098856926
time_elpased: 2.324
batch start
#iterations: 7
currently lose_sum: 100.23842871189117
time_elpased: 2.312
batch start
#iterations: 8
currently lose_sum: 100.18252682685852
time_elpased: 2.297
batch start
#iterations: 9
currently lose_sum: 100.16879665851593
time_elpased: 2.332
batch start
#iterations: 10
currently lose_sum: 100.17587441205978
time_elpased: 2.365
batch start
#iterations: 11
currently lose_sum: 100.19120579957962
time_elpased: 2.378
batch start
#iterations: 12
currently lose_sum: 100.2607706785202
time_elpased: 2.324
batch start
#iterations: 13
currently lose_sum: 100.00141936540604
time_elpased: 2.359
batch start
#iterations: 14
currently lose_sum: 100.13002210855484
time_elpased: 2.289
batch start
#iterations: 15
currently lose_sum: 99.96319085359573
time_elpased: 2.302
batch start
#iterations: 16
currently lose_sum: 99.9404781460762
time_elpased: 2.294
batch start
#iterations: 17
currently lose_sum: 99.89606684446335
time_elpased: 2.323
batch start
#iterations: 18
currently lose_sum: 99.98686802387238
time_elpased: 2.322
batch start
#iterations: 19
currently lose_sum: 99.93260818719864
time_elpased: 2.343
start validation test
0.580051546392
0.599745870394
0.485746629618
0.536760106897
0.580217113219
66.018
batch start
#iterations: 20
currently lose_sum: 99.93145567178726
time_elpased: 2.297
batch start
#iterations: 21
currently lose_sum: 99.80249297618866
time_elpased: 2.29
batch start
#iterations: 22
currently lose_sum: 99.75791043043137
time_elpased: 2.291
batch start
#iterations: 23
currently lose_sum: 99.74200409650803
time_elpased: 2.301
batch start
#iterations: 24
currently lose_sum: 99.715376496315
time_elpased: 2.353
batch start
#iterations: 25
currently lose_sum: 99.7338233590126
time_elpased: 2.284
batch start
#iterations: 26
currently lose_sum: 99.7173290848732
time_elpased: 2.315
batch start
#iterations: 27
currently lose_sum: 99.67039704322815
time_elpased: 2.358
batch start
#iterations: 28
currently lose_sum: 99.62932765483856
time_elpased: 2.288
batch start
#iterations: 29
currently lose_sum: 99.61875289678574
time_elpased: 2.304
batch start
#iterations: 30
currently lose_sum: 99.50838673114777
time_elpased: 2.349
batch start
#iterations: 31
currently lose_sum: 99.5600535273552
time_elpased: 2.273
batch start
#iterations: 32
currently lose_sum: 99.27260076999664
time_elpased: 2.305
batch start
#iterations: 33
currently lose_sum: 99.55098032951355
time_elpased: 2.354
batch start
#iterations: 34
currently lose_sum: 99.68997114896774
time_elpased: 2.313
batch start
#iterations: 35
currently lose_sum: 99.5393455028534
time_elpased: 2.32
batch start
#iterations: 36
currently lose_sum: 99.47428327798843
time_elpased: 2.351
batch start
#iterations: 37
currently lose_sum: 99.70752239227295
time_elpased: 2.328
batch start
#iterations: 38
currently lose_sum: 99.46088403463364
time_elpased: 2.328
batch start
#iterations: 39
currently lose_sum: 99.45494574308395
time_elpased: 2.315
start validation test
0.594278350515
0.625817884406
0.472470927241
0.538438984343
0.594492202235
65.817
batch start
#iterations: 40
currently lose_sum: 99.66122955083847
time_elpased: 2.324
batch start
#iterations: 41
currently lose_sum: 99.54177671670914
time_elpased: 2.331
batch start
#iterations: 42
currently lose_sum: 99.40923029184341
time_elpased: 2.377
batch start
#iterations: 43
currently lose_sum: 99.13845384120941
time_elpased: 2.317
batch start
#iterations: 44
currently lose_sum: 99.35162794589996
time_elpased: 2.329
batch start
#iterations: 45
currently lose_sum: 99.09019070863724
time_elpased: 2.316
batch start
#iterations: 46
currently lose_sum: 99.19932132959366
time_elpased: 2.344
batch start
#iterations: 47
currently lose_sum: 99.47206526994705
time_elpased: 2.333
batch start
#iterations: 48
currently lose_sum: 99.43519729375839
time_elpased: 2.286
batch start
#iterations: 49
currently lose_sum: 99.29553669691086
time_elpased: 2.323
batch start
#iterations: 50
currently lose_sum: 99.43565082550049
time_elpased: 2.307
batch start
#iterations: 51
currently lose_sum: 99.24694204330444
time_elpased: 2.346
batch start
#iterations: 52
currently lose_sum: 99.2581136226654
time_elpased: 2.326
batch start
#iterations: 53
currently lose_sum: 99.21374255418777
time_elpased: 2.34
batch start
#iterations: 54
currently lose_sum: 99.24292576313019
time_elpased: 2.325
batch start
#iterations: 55
currently lose_sum: 99.20759350061417
time_elpased: 2.312
batch start
#iterations: 56
currently lose_sum: 99.37422555685043
time_elpased: 2.349
batch start
#iterations: 57
currently lose_sum: 99.23254716396332
time_elpased: 2.318
batch start
#iterations: 58
currently lose_sum: 99.24935495853424
time_elpased: 2.317
batch start
#iterations: 59
currently lose_sum: 99.31211656332016
time_elpased: 2.277
start validation test
0.602680412371
0.624767109676
0.517649480292
0.566186402521
0.602829697288
65.066
batch start
#iterations: 60
currently lose_sum: 99.19439733028412
time_elpased: 2.272
batch start
#iterations: 61
currently lose_sum: 99.25717878341675
time_elpased: 2.322
batch start
#iterations: 62
currently lose_sum: 98.87710946798325
time_elpased: 2.323
batch start
#iterations: 63
currently lose_sum: 99.02427780628204
time_elpased: 2.273
batch start
#iterations: 64
currently lose_sum: 99.12248086929321
time_elpased: 2.34
batch start
#iterations: 65
currently lose_sum: 99.08005106449127
time_elpased: 2.273
batch start
#iterations: 66
currently lose_sum: 99.18429636955261
time_elpased: 2.311
batch start
#iterations: 67
currently lose_sum: 99.19289582967758
time_elpased: 2.292
batch start
#iterations: 68
currently lose_sum: 98.85158461332321
time_elpased: 2.293
batch start
#iterations: 69
currently lose_sum: 98.74571746587753
time_elpased: 2.286
batch start
#iterations: 70
currently lose_sum: 98.77325904369354
time_elpased: 2.377
batch start
#iterations: 71
currently lose_sum: 98.94983786344528
time_elpased: 2.364
batch start
#iterations: 72
currently lose_sum: 98.82950949668884
time_elpased: 2.356
batch start
#iterations: 73
currently lose_sum: 98.73981839418411
time_elpased: 2.325
batch start
#iterations: 74
currently lose_sum: 98.66686242818832
time_elpased: 2.331
batch start
#iterations: 75
currently lose_sum: 99.11608165502548
time_elpased: 2.291
batch start
#iterations: 76
currently lose_sum: 98.9819341301918
time_elpased: 2.337
batch start
#iterations: 77
currently lose_sum: 98.71729809045792
time_elpased: 2.307
batch start
#iterations: 78
currently lose_sum: 98.96473503112793
time_elpased: 2.307
batch start
#iterations: 79
currently lose_sum: 99.09966123104095
time_elpased: 2.327
start validation test
0.571958762887
0.575521111705
0.554080477514
0.564597315436
0.571990150974
65.998
batch start
#iterations: 80
currently lose_sum: 98.87580066919327
time_elpased: 2.294
batch start
#iterations: 81
currently lose_sum: 98.75404983758926
time_elpased: 2.285
batch start
#iterations: 82
currently lose_sum: 98.78002315759659
time_elpased: 2.334
batch start
#iterations: 83
currently lose_sum: 98.8776398897171
time_elpased: 2.338
batch start
#iterations: 84
currently lose_sum: 98.70420694351196
time_elpased: 2.353
batch start
#iterations: 85
currently lose_sum: 98.85327249765396
time_elpased: 2.325
batch start
#iterations: 86
currently lose_sum: 98.97591179609299
time_elpased: 2.373
batch start
#iterations: 87
currently lose_sum: 98.7217515707016
time_elpased: 2.313
batch start
#iterations: 88
currently lose_sum: 99.04611718654633
time_elpased: 2.344
batch start
#iterations: 89
currently lose_sum: 98.63996118307114
time_elpased: 2.324
batch start
#iterations: 90
currently lose_sum: 98.56152772903442
time_elpased: 2.299
batch start
#iterations: 91
currently lose_sum: 98.74023503065109
time_elpased: 2.324
batch start
#iterations: 92
currently lose_sum: 98.7547916173935
time_elpased: 2.319
batch start
#iterations: 93
currently lose_sum: 98.74919772148132
time_elpased: 2.352
batch start
#iterations: 94
currently lose_sum: 98.7127856016159
time_elpased: 2.331
batch start
#iterations: 95
currently lose_sum: 98.82552200555801
time_elpased: 2.313
batch start
#iterations: 96
currently lose_sum: 98.74482047557831
time_elpased: 2.333
batch start
#iterations: 97
currently lose_sum: 98.70384871959686
time_elpased: 2.271
batch start
#iterations: 98
currently lose_sum: 98.98343002796173
time_elpased: 2.322
batch start
#iterations: 99
currently lose_sum: 98.72993689775467
time_elpased: 2.301
start validation test
0.582474226804
0.594616734933
0.52289801379
0.556456028912
0.582578822035
65.508
batch start
#iterations: 100
currently lose_sum: 98.80100274085999
time_elpased: 2.34
batch start
#iterations: 101
currently lose_sum: 98.63005536794662
time_elpased: 2.3
batch start
#iterations: 102
currently lose_sum: 98.50500804185867
time_elpased: 2.296
batch start
#iterations: 103
currently lose_sum: 98.9228767156601
time_elpased: 2.355
batch start
#iterations: 104
currently lose_sum: 98.53155726194382
time_elpased: 2.353
batch start
#iterations: 105
currently lose_sum: 98.99340444803238
time_elpased: 2.353
batch start
#iterations: 106
currently lose_sum: 98.90047073364258
time_elpased: 2.326
batch start
#iterations: 107
currently lose_sum: 98.47567874193192
time_elpased: 2.352
batch start
#iterations: 108
currently lose_sum: 98.72087478637695
time_elpased: 2.352
batch start
#iterations: 109
currently lose_sum: 98.60746675729752
time_elpased: 2.342
batch start
#iterations: 110
currently lose_sum: 98.61925560235977
time_elpased: 2.31
batch start
#iterations: 111
currently lose_sum: 98.61597084999084
time_elpased: 2.298
batch start
#iterations: 112
currently lose_sum: 98.80378323793411
time_elpased: 2.37
batch start
#iterations: 113
currently lose_sum: 98.52775830030441
time_elpased: 2.316
batch start
#iterations: 114
currently lose_sum: 98.99042129516602
time_elpased: 2.327
batch start
#iterations: 115
currently lose_sum: 98.80548769235611
time_elpased: 2.318
batch start
#iterations: 116
currently lose_sum: 98.62586468458176
time_elpased: 2.359
batch start
#iterations: 117
currently lose_sum: 98.63796210289001
time_elpased: 2.285
batch start
#iterations: 118
currently lose_sum: 98.62887954711914
time_elpased: 2.343
batch start
#iterations: 119
currently lose_sum: 98.62307965755463
time_elpased: 2.323
start validation test
0.598865979381
0.614429331756
0.534630029845
0.571758749725
0.598978755499
65.009
batch start
#iterations: 120
currently lose_sum: 98.79746228456497
time_elpased: 2.295
batch start
#iterations: 121
currently lose_sum: 98.58176779747009
time_elpased: 2.293
batch start
#iterations: 122
currently lose_sum: 98.78822433948517
time_elpased: 2.302
batch start
#iterations: 123
currently lose_sum: 98.71498376131058
time_elpased: 2.284
batch start
#iterations: 124
currently lose_sum: 98.66846889257431
time_elpased: 2.346
batch start
#iterations: 125
currently lose_sum: 98.45507246255875
time_elpased: 2.354
batch start
#iterations: 126
currently lose_sum: 98.50925761461258
time_elpased: 2.3
batch start
#iterations: 127
currently lose_sum: 98.64652526378632
time_elpased: 2.312
batch start
#iterations: 128
currently lose_sum: 98.68285399675369
time_elpased: 2.32
batch start
#iterations: 129
currently lose_sum: 98.79345279932022
time_elpased: 2.317
batch start
#iterations: 130
currently lose_sum: 98.61271065473557
time_elpased: 2.3
batch start
#iterations: 131
currently lose_sum: 98.38219237327576
time_elpased: 2.312
batch start
#iterations: 132
currently lose_sum: 98.67952114343643
time_elpased: 2.346
batch start
#iterations: 133
currently lose_sum: 98.38154637813568
time_elpased: 2.3
batch start
#iterations: 134
currently lose_sum: 98.20101368427277
time_elpased: 2.311
batch start
#iterations: 135
currently lose_sum: 98.48704385757446
time_elpased: 2.322
batch start
#iterations: 136
currently lose_sum: 98.52933531999588
time_elpased: 2.318
batch start
#iterations: 137
currently lose_sum: 98.73645782470703
time_elpased: 2.329
batch start
#iterations: 138
currently lose_sum: 98.26237922906876
time_elpased: 2.3
batch start
#iterations: 139
currently lose_sum: 98.30839055776596
time_elpased: 2.295
start validation test
0.602216494845
0.636612021858
0.479571884326
0.547044667488
0.602431816376
64.957
batch start
#iterations: 140
currently lose_sum: 98.40526795387268
time_elpased: 2.385
batch start
#iterations: 141
currently lose_sum: 98.45852041244507
time_elpased: 2.3
batch start
#iterations: 142
currently lose_sum: 98.7110784649849
time_elpased: 2.285
batch start
#iterations: 143
currently lose_sum: 98.25170344114304
time_elpased: 2.276
batch start
#iterations: 144
currently lose_sum: 98.55520617961884
time_elpased: 2.326
batch start
#iterations: 145
currently lose_sum: 98.5833632349968
time_elpased: 2.312
batch start
#iterations: 146
currently lose_sum: 98.53888177871704
time_elpased: 2.306
batch start
#iterations: 147
currently lose_sum: 98.50938111543655
time_elpased: 2.284
batch start
#iterations: 148
currently lose_sum: 98.68330347537994
time_elpased: 2.289
batch start
#iterations: 149
currently lose_sum: 98.25168246030807
time_elpased: 2.34
batch start
#iterations: 150
currently lose_sum: 98.43244630098343
time_elpased: 2.311
batch start
#iterations: 151
currently lose_sum: 98.48911339044571
time_elpased: 2.351
batch start
#iterations: 152
currently lose_sum: 98.57973408699036
time_elpased: 2.323
batch start
#iterations: 153
currently lose_sum: 98.22585153579712
time_elpased: 2.322
batch start
#iterations: 154
currently lose_sum: 98.68671190738678
time_elpased: 2.291
batch start
#iterations: 155
currently lose_sum: 98.36598587036133
time_elpased: 2.315
batch start
#iterations: 156
currently lose_sum: 98.31298077106476
time_elpased: 2.298
batch start
#iterations: 157
currently lose_sum: 98.27283197641373
time_elpased: 2.286
batch start
#iterations: 158
currently lose_sum: 98.15401846170425
time_elpased: 2.302
batch start
#iterations: 159
currently lose_sum: 98.29223167896271
time_elpased: 2.319
start validation test
0.602164948454
0.632086692216
0.492230112175
0.553459847258
0.602357956015
64.702
batch start
#iterations: 160
currently lose_sum: 98.29493361711502
time_elpased: 2.35
batch start
#iterations: 161
currently lose_sum: 98.54251688718796
time_elpased: 2.331
batch start
#iterations: 162
currently lose_sum: 98.30831700563431
time_elpased: 2.332
batch start
#iterations: 163
currently lose_sum: 98.17543649673462
time_elpased: 2.333
batch start
#iterations: 164
currently lose_sum: 98.22759646177292
time_elpased: 2.32
batch start
#iterations: 165
currently lose_sum: 98.46693009138107
time_elpased: 2.317
batch start
#iterations: 166
currently lose_sum: 98.38151770830154
time_elpased: 2.35
batch start
#iterations: 167
currently lose_sum: 98.21545827388763
time_elpased: 2.283
batch start
#iterations: 168
currently lose_sum: 98.38796073198318
time_elpased: 2.296
batch start
#iterations: 169
currently lose_sum: 98.08976536989212
time_elpased: 2.33
batch start
#iterations: 170
currently lose_sum: 98.46619004011154
time_elpased: 2.339
batch start
#iterations: 171
currently lose_sum: 98.2712442278862
time_elpased: 2.303
batch start
#iterations: 172
currently lose_sum: 98.22306352853775
time_elpased: 2.336
batch start
#iterations: 173
currently lose_sum: 98.18845623731613
time_elpased: 2.269
batch start
#iterations: 174
currently lose_sum: 98.47287333011627
time_elpased: 2.34
batch start
#iterations: 175
currently lose_sum: 98.22928613424301
time_elpased: 2.308
batch start
#iterations: 176
currently lose_sum: 98.1919122338295
time_elpased: 2.315
batch start
#iterations: 177
currently lose_sum: 98.17755424976349
time_elpased: 2.288
batch start
#iterations: 178
currently lose_sum: 98.14354735612869
time_elpased: 2.336
batch start
#iterations: 179
currently lose_sum: 98.30259436368942
time_elpased: 2.277
start validation test
0.593350515464
0.597192683964
0.577956159308
0.5874169761
0.593377542631
65.221
batch start
#iterations: 180
currently lose_sum: 98.35279965400696
time_elpased: 2.302
batch start
#iterations: 181
currently lose_sum: 98.12407422065735
time_elpased: 2.326
batch start
#iterations: 182
currently lose_sum: 98.18042248487473
time_elpased: 2.315
batch start
#iterations: 183
currently lose_sum: 98.24058091640472
time_elpased: 2.318
batch start
#iterations: 184
currently lose_sum: 98.14336907863617
time_elpased: 2.324
batch start
#iterations: 185
currently lose_sum: 97.90141928195953
time_elpased: 2.337
batch start
#iterations: 186
currently lose_sum: 97.98865222930908
time_elpased: 2.366
batch start
#iterations: 187
currently lose_sum: 97.83188313245773
time_elpased: 2.275
batch start
#iterations: 188
currently lose_sum: 97.9988763332367
time_elpased: 2.311
batch start
#iterations: 189
currently lose_sum: 98.0735855102539
time_elpased: 2.299
batch start
#iterations: 190
currently lose_sum: 98.19193667173386
time_elpased: 2.313
batch start
#iterations: 191
currently lose_sum: 97.80329042673111
time_elpased: 2.307
batch start
#iterations: 192
currently lose_sum: 98.08676129579544
time_elpased: 2.344
batch start
#iterations: 193
currently lose_sum: 97.95515036582947
time_elpased: 2.349
batch start
#iterations: 194
currently lose_sum: 98.06222414970398
time_elpased: 2.311
batch start
#iterations: 195
currently lose_sum: 98.33557850122452
time_elpased: 2.287
batch start
#iterations: 196
currently lose_sum: 97.87823188304901
time_elpased: 2.294
batch start
#iterations: 197
currently lose_sum: 98.36083209514618
time_elpased: 2.303
batch start
#iterations: 198
currently lose_sum: 97.90047204494476
time_elpased: 2.342
batch start
#iterations: 199
currently lose_sum: 98.12618774175644
time_elpased: 2.324
start validation test
0.594793814433
0.631855640807
0.457651538541
0.53082661892
0.595034588851
65.160
batch start
#iterations: 200
currently lose_sum: 98.1537858247757
time_elpased: 2.283
batch start
#iterations: 201
currently lose_sum: 98.04287737607956
time_elpased: 2.311
batch start
#iterations: 202
currently lose_sum: 97.66752618551254
time_elpased: 2.298
batch start
#iterations: 203
currently lose_sum: 98.00497996807098
time_elpased: 2.291
batch start
#iterations: 204
currently lose_sum: 97.8873502612114
time_elpased: 2.352
batch start
#iterations: 205
currently lose_sum: 97.92135590314865
time_elpased: 2.316
batch start
#iterations: 206
currently lose_sum: 97.82230144739151
time_elpased: 2.33
batch start
#iterations: 207
currently lose_sum: 97.89182829856873
time_elpased: 2.277
batch start
#iterations: 208
currently lose_sum: 97.61807751655579
time_elpased: 2.321
batch start
#iterations: 209
currently lose_sum: 97.79763889312744
time_elpased: 2.373
batch start
#iterations: 210
currently lose_sum: 97.5922862291336
time_elpased: 2.324
batch start
#iterations: 211
currently lose_sum: 97.45758885145187
time_elpased: 2.308
batch start
#iterations: 212
currently lose_sum: 97.66519284248352
time_elpased: 2.325
batch start
#iterations: 213
currently lose_sum: 97.70355588197708
time_elpased: 2.345
batch start
#iterations: 214
currently lose_sum: 97.56372904777527
time_elpased: 2.288
batch start
#iterations: 215
currently lose_sum: 97.53764778375626
time_elpased: 2.316
batch start
#iterations: 216
currently lose_sum: 97.56903553009033
time_elpased: 2.324
batch start
#iterations: 217
currently lose_sum: 97.14681726694107
time_elpased: 2.367
batch start
#iterations: 218
currently lose_sum: 97.84782689809799
time_elpased: 2.345
batch start
#iterations: 219
currently lose_sum: 97.71334916353226
time_elpased: 2.305
start validation test
0.596804123711
0.619618735008
0.505094164866
0.556525683184
0.596965134689
64.881
batch start
#iterations: 220
currently lose_sum: 97.7315998673439
time_elpased: 2.35
batch start
#iterations: 221
currently lose_sum: 97.82679402828217
time_elpased: 2.32
batch start
#iterations: 222
currently lose_sum: 97.36416453123093
time_elpased: 2.33
batch start
#iterations: 223
currently lose_sum: 97.24174082279205
time_elpased: 2.376
batch start
#iterations: 224
currently lose_sum: 97.45339238643646
time_elpased: 2.353
batch start
#iterations: 225
currently lose_sum: 97.09853994846344
time_elpased: 2.308
batch start
#iterations: 226
currently lose_sum: 97.49999010562897
time_elpased: 2.35
batch start
#iterations: 227
currently lose_sum: 97.59374338388443
time_elpased: 2.291
batch start
#iterations: 228
currently lose_sum: 97.58080381155014
time_elpased: 2.322
batch start
#iterations: 229
currently lose_sum: 97.45233988761902
time_elpased: 2.296
batch start
#iterations: 230
currently lose_sum: 97.155264377594
time_elpased: 2.313
batch start
#iterations: 231
currently lose_sum: 97.41351336240768
time_elpased: 2.358
batch start
#iterations: 232
currently lose_sum: 97.18080812692642
time_elpased: 2.33
batch start
#iterations: 233
currently lose_sum: 96.98333525657654
time_elpased: 2.332
batch start
#iterations: 234
currently lose_sum: 97.10281693935394
time_elpased: 2.32
batch start
#iterations: 235
currently lose_sum: 97.3073468208313
time_elpased: 2.396
batch start
#iterations: 236
currently lose_sum: 96.94186580181122
time_elpased: 2.381
batch start
#iterations: 237
currently lose_sum: 97.19744342565536
time_elpased: 2.28
batch start
#iterations: 238
currently lose_sum: 97.49532437324524
time_elpased: 2.309
batch start
#iterations: 239
currently lose_sum: 97.295907497406
time_elpased: 2.301
start validation test
0.596030927835
0.613746369797
0.521971801997
0.564151048329
0.596160950053
65.361
batch start
#iterations: 240
currently lose_sum: 97.28644222021103
time_elpased: 2.323
batch start
#iterations: 241
currently lose_sum: 97.23319065570831
time_elpased: 2.275
batch start
#iterations: 242
currently lose_sum: 96.8697264790535
time_elpased: 2.303
batch start
#iterations: 243
currently lose_sum: 97.21643960475922
time_elpased: 2.313
batch start
#iterations: 244
currently lose_sum: 97.14337849617004
time_elpased: 2.305
batch start
#iterations: 245
currently lose_sum: 96.86966305971146
time_elpased: 2.319
batch start
#iterations: 246
currently lose_sum: 97.03943330049515
time_elpased: 2.33
batch start
#iterations: 247
currently lose_sum: 97.03626292943954
time_elpased: 2.323
batch start
#iterations: 248
currently lose_sum: 96.96753162145615
time_elpased: 2.292
batch start
#iterations: 249
currently lose_sum: 96.9928485751152
time_elpased: 2.29
batch start
#iterations: 250
currently lose_sum: 96.99622428417206
time_elpased: 2.337
batch start
#iterations: 251
currently lose_sum: 96.87417298555374
time_elpased: 2.329
batch start
#iterations: 252
currently lose_sum: 96.79145324230194
time_elpased: 2.339
batch start
#iterations: 253
currently lose_sum: 96.62403297424316
time_elpased: 2.335
batch start
#iterations: 254
currently lose_sum: 96.75999945402145
time_elpased: 2.335
batch start
#iterations: 255
currently lose_sum: 96.5408433675766
time_elpased: 2.275
batch start
#iterations: 256
currently lose_sum: 96.59321731328964
time_elpased: 2.343
batch start
#iterations: 257
currently lose_sum: 96.84130448102951
time_elpased: 2.354
batch start
#iterations: 258
currently lose_sum: 96.7800344824791
time_elpased: 2.323
batch start
#iterations: 259
currently lose_sum: 96.42334914207458
time_elpased: 2.333
start validation test
0.582010309278
0.605761641673
0.473911701142
0.531785899879
0.582200093058
65.963
batch start
#iterations: 260
currently lose_sum: 96.52554726600647
time_elpased: 2.331
batch start
#iterations: 261
currently lose_sum: 96.55500495433807
time_elpased: 2.349
batch start
#iterations: 262
currently lose_sum: 96.082488656044
time_elpased: 2.288
batch start
#iterations: 263
currently lose_sum: 96.122793674469
time_elpased: 2.287
batch start
#iterations: 264
currently lose_sum: 96.23232769966125
time_elpased: 2.333
batch start
#iterations: 265
currently lose_sum: 96.3055972456932
time_elpased: 2.341
batch start
#iterations: 266
currently lose_sum: 96.2813127040863
time_elpased: 2.318
batch start
#iterations: 267
currently lose_sum: 96.43159836530685
time_elpased: 2.302
batch start
#iterations: 268
currently lose_sum: 96.30013227462769
time_elpased: 2.314
batch start
#iterations: 269
currently lose_sum: 96.38704305887222
time_elpased: 2.3
batch start
#iterations: 270
currently lose_sum: 96.2019784450531
time_elpased: 2.322
batch start
#iterations: 271
currently lose_sum: 96.09453278779984
time_elpased: 2.321
batch start
#iterations: 272
currently lose_sum: 95.97561764717102
time_elpased: 2.354
batch start
#iterations: 273
currently lose_sum: 96.30432814359665
time_elpased: 2.286
batch start
#iterations: 274
currently lose_sum: 95.82726895809174
time_elpased: 2.361
batch start
#iterations: 275
currently lose_sum: 96.46857661008835
time_elpased: 2.337
batch start
#iterations: 276
currently lose_sum: 95.98520493507385
time_elpased: 2.314
batch start
#iterations: 277
currently lose_sum: 95.9947960972786
time_elpased: 2.319
batch start
#iterations: 278
currently lose_sum: 95.76069325208664
time_elpased: 2.311
batch start
#iterations: 279
currently lose_sum: 95.86219125986099
time_elpased: 2.306
start validation test
0.589587628866
0.621285418106
0.462591334774
0.530320906088
0.589810590448
66.757
batch start
#iterations: 280
currently lose_sum: 95.79089218378067
time_elpased: 2.315
batch start
#iterations: 281
currently lose_sum: 95.4995346069336
time_elpased: 2.299
batch start
#iterations: 282
currently lose_sum: 95.50231236219406
time_elpased: 2.363
batch start
#iterations: 283
currently lose_sum: 95.54467195272446
time_elpased: 2.301
batch start
#iterations: 284
currently lose_sum: 95.16938555240631
time_elpased: 2.356
batch start
#iterations: 285
currently lose_sum: 95.40673542022705
time_elpased: 2.342
batch start
#iterations: 286
currently lose_sum: 95.24081379175186
time_elpased: 2.295
batch start
#iterations: 287
currently lose_sum: 95.17633366584778
time_elpased: 2.306
batch start
#iterations: 288
currently lose_sum: 94.95259761810303
time_elpased: 2.382
batch start
#iterations: 289
currently lose_sum: 94.95880496501923
time_elpased: 2.341
batch start
#iterations: 290
currently lose_sum: 95.0257380604744
time_elpased: 2.303
batch start
#iterations: 291
currently lose_sum: 95.04381608963013
time_elpased: 2.324
batch start
#iterations: 292
currently lose_sum: 94.54931235313416
time_elpased: 2.374
batch start
#iterations: 293
currently lose_sum: 94.50426864624023
time_elpased: 2.305
batch start
#iterations: 294
currently lose_sum: 95.08480483293533
time_elpased: 2.368
batch start
#iterations: 295
currently lose_sum: 94.53481602668762
time_elpased: 2.324
batch start
#iterations: 296
currently lose_sum: 94.56527668237686
time_elpased: 2.347
batch start
#iterations: 297
currently lose_sum: 94.25718551874161
time_elpased: 2.326
batch start
#iterations: 298
currently lose_sum: 94.69109439849854
time_elpased: 2.316
batch start
#iterations: 299
currently lose_sum: 94.6483468413353
time_elpased: 2.321
start validation test
0.588092783505
0.63224026969
0.42461665123
0.50803423013
0.58837979107
67.216
batch start
#iterations: 300
currently lose_sum: 94.29863303899765
time_elpased: 2.307
batch start
#iterations: 301
currently lose_sum: 94.39673149585724
time_elpased: 2.307
batch start
#iterations: 302
currently lose_sum: 94.27992022037506
time_elpased: 2.292
batch start
#iterations: 303
currently lose_sum: 94.1695830821991
time_elpased: 2.35
batch start
#iterations: 304
currently lose_sum: 94.23255807161331
time_elpased: 2.313
batch start
#iterations: 305
currently lose_sum: 94.4995087981224
time_elpased: 2.306
batch start
#iterations: 306
currently lose_sum: 94.21608763933182
time_elpased: 2.355
batch start
#iterations: 307
currently lose_sum: 93.69409269094467
time_elpased: 2.305
batch start
#iterations: 308
currently lose_sum: 93.7706543803215
time_elpased: 2.343
batch start
#iterations: 309
currently lose_sum: 94.36786901950836
time_elpased: 2.335
batch start
#iterations: 310
currently lose_sum: 93.57893532514572
time_elpased: 2.403
batch start
#iterations: 311
currently lose_sum: 93.17531806230545
time_elpased: 2.327
batch start
#iterations: 312
currently lose_sum: 93.24332505464554
time_elpased: 2.334
batch start
#iterations: 313
currently lose_sum: 93.16530305147171
time_elpased: 2.33
batch start
#iterations: 314
currently lose_sum: 93.48190367221832
time_elpased: 2.331
batch start
#iterations: 315
currently lose_sum: 92.97309178113937
time_elpased: 2.323
batch start
#iterations: 316
currently lose_sum: 93.45033937692642
time_elpased: 2.325
batch start
#iterations: 317
currently lose_sum: 92.74401134252548
time_elpased: 2.323
batch start
#iterations: 318
currently lose_sum: 93.07157069444656
time_elpased: 2.33
batch start
#iterations: 319
currently lose_sum: 92.98965883255005
time_elpased: 2.27
start validation test
0.582680412371
0.631212562733
0.401255531543
0.49062539323
0.582998931732
70.262
batch start
#iterations: 320
currently lose_sum: 92.8147189617157
time_elpased: 2.335
batch start
#iterations: 321
currently lose_sum: 92.92681437730789
time_elpased: 2.307
batch start
#iterations: 322
currently lose_sum: 92.3658840060234
time_elpased: 2.297
batch start
#iterations: 323
currently lose_sum: 92.20464211702347
time_elpased: 2.328
batch start
#iterations: 324
currently lose_sum: 92.70840376615524
time_elpased: 2.315
batch start
#iterations: 325
currently lose_sum: 92.29159557819366
time_elpased: 2.302
batch start
#iterations: 326
currently lose_sum: 91.91272222995758
time_elpased: 2.332
batch start
#iterations: 327
currently lose_sum: 92.28193473815918
time_elpased: 2.284
batch start
#iterations: 328
currently lose_sum: 92.39076256752014
time_elpased: 2.344
batch start
#iterations: 329
currently lose_sum: 91.40714973211288
time_elpased: 2.339
batch start
#iterations: 330
currently lose_sum: 91.7599316239357
time_elpased: 2.376
batch start
#iterations: 331
currently lose_sum: 92.06482684612274
time_elpased: 2.338
batch start
#iterations: 332
currently lose_sum: 91.97645998001099
time_elpased: 2.34
batch start
#iterations: 333
currently lose_sum: 91.74135375022888
time_elpased: 2.371
batch start
#iterations: 334
currently lose_sum: 91.64306020736694
time_elpased: 2.315
batch start
#iterations: 335
currently lose_sum: 91.39531600475311
time_elpased: 2.342
batch start
#iterations: 336
currently lose_sum: 91.06622350215912
time_elpased: 2.369
batch start
#iterations: 337
currently lose_sum: 91.18878942728043
time_elpased: 2.344
batch start
#iterations: 338
currently lose_sum: 90.70867210626602
time_elpased: 2.365
batch start
#iterations: 339
currently lose_sum: 91.2249944806099
time_elpased: 2.283
start validation test
0.574587628866
0.614018691589
0.405680765668
0.488566648076
0.574884170916
72.835
batch start
#iterations: 340
currently lose_sum: 90.14388781785965
time_elpased: 2.302
batch start
#iterations: 341
currently lose_sum: 90.66296011209488
time_elpased: 2.335
batch start
#iterations: 342
currently lose_sum: 90.90384697914124
time_elpased: 2.363
batch start
#iterations: 343
currently lose_sum: 90.72531694173813
time_elpased: 2.314
batch start
#iterations: 344
currently lose_sum: 90.2061505317688
time_elpased: 2.287
batch start
#iterations: 345
currently lose_sum: 90.10190027952194
time_elpased: 2.328
batch start
#iterations: 346
currently lose_sum: 90.26342183351517
time_elpased: 2.361
batch start
#iterations: 347
currently lose_sum: 90.05571323633194
time_elpased: 2.347
batch start
#iterations: 348
currently lose_sum: 89.69412034749985
time_elpased: 2.354
batch start
#iterations: 349
currently lose_sum: 90.1282668709755
time_elpased: 2.331
batch start
#iterations: 350
currently lose_sum: 89.78252190351486
time_elpased: 2.309
batch start
#iterations: 351
currently lose_sum: 89.1376097202301
time_elpased: 2.324
batch start
#iterations: 352
currently lose_sum: 89.65738755464554
time_elpased: 2.333
batch start
#iterations: 353
currently lose_sum: 89.06830126047134
time_elpased: 2.31
batch start
#iterations: 354
currently lose_sum: 89.55628222227097
time_elpased: 2.32
batch start
#iterations: 355
currently lose_sum: 88.97673094272614
time_elpased: 2.344
batch start
#iterations: 356
currently lose_sum: 88.90670025348663
time_elpased: 2.304
batch start
#iterations: 357
currently lose_sum: 88.32795125246048
time_elpased: 2.312
batch start
#iterations: 358
currently lose_sum: 89.0162661075592
time_elpased: 2.347
batch start
#iterations: 359
currently lose_sum: 88.59484535455704
time_elpased: 2.29
start validation test
0.573298969072
0.626694840641
0.366265308223
0.462327877371
0.573662448597
78.466
batch start
#iterations: 360
currently lose_sum: 88.7519736289978
time_elpased: 2.296
batch start
#iterations: 361
currently lose_sum: 88.16472518444061
time_elpased: 2.311
batch start
#iterations: 362
currently lose_sum: 88.10201013088226
time_elpased: 2.363
batch start
#iterations: 363
currently lose_sum: 87.75736165046692
time_elpased: 2.326
batch start
#iterations: 364
currently lose_sum: 88.27357476949692
time_elpased: 2.384
batch start
#iterations: 365
currently lose_sum: 87.99273365736008
time_elpased: 2.308
batch start
#iterations: 366
currently lose_sum: 88.20551037788391
time_elpased: 2.298
batch start
#iterations: 367
currently lose_sum: 87.63657009601593
time_elpased: 2.26
batch start
#iterations: 368
currently lose_sum: 87.54129683971405
time_elpased: 2.304
batch start
#iterations: 369
currently lose_sum: 86.96812337636948
time_elpased: 2.283
batch start
#iterations: 370
currently lose_sum: 87.20392268896103
time_elpased: 2.351
batch start
#iterations: 371
currently lose_sum: 87.09604382514954
time_elpased: 2.29
batch start
#iterations: 372
currently lose_sum: 86.95910423994064
time_elpased: 2.379
batch start
#iterations: 373
currently lose_sum: 87.35131913423538
time_elpased: 2.311
batch start
#iterations: 374
currently lose_sum: 87.0212294459343
time_elpased: 2.332
batch start
#iterations: 375
currently lose_sum: 86.74731510877609
time_elpased: 2.328
batch start
#iterations: 376
currently lose_sum: 86.49911278486252
time_elpased: 2.313
batch start
#iterations: 377
currently lose_sum: 86.63227254152298
time_elpased: 2.307
batch start
#iterations: 378
currently lose_sum: 86.73074680566788
time_elpased: 2.344
batch start
#iterations: 379
currently lose_sum: 86.46174997091293
time_elpased: 2.318
start validation test
0.572989690722
0.625547573156
0.36739734486
0.462914937759
0.573350639795
82.669
batch start
#iterations: 380
currently lose_sum: 85.80674546957016
time_elpased: 2.343
batch start
#iterations: 381
currently lose_sum: 85.97508472204208
time_elpased: 2.3
batch start
#iterations: 382
currently lose_sum: 86.16173630952835
time_elpased: 2.343
batch start
#iterations: 383
currently lose_sum: 86.09539115428925
time_elpased: 2.272
batch start
#iterations: 384
currently lose_sum: 85.65521848201752
time_elpased: 2.358
batch start
#iterations: 385
currently lose_sum: 85.68127781152725
time_elpased: 2.306
batch start
#iterations: 386
currently lose_sum: 86.42376548051834
time_elpased: 2.369
batch start
#iterations: 387
currently lose_sum: 85.70408171415329
time_elpased: 2.297
batch start
#iterations: 388
currently lose_sum: 84.78523641824722
time_elpased: 2.37
batch start
#iterations: 389
currently lose_sum: 85.43230855464935
time_elpased: 2.298
batch start
#iterations: 390
currently lose_sum: 85.26704186201096
time_elpased: 2.319
batch start
#iterations: 391
currently lose_sum: 85.24535894393921
time_elpased: 2.326
batch start
#iterations: 392
currently lose_sum: 84.94356125593185
time_elpased: 2.302
batch start
#iterations: 393
currently lose_sum: 84.84610134363174
time_elpased: 2.29
batch start
#iterations: 394
currently lose_sum: 85.35132145881653
time_elpased: 2.358
batch start
#iterations: 395
currently lose_sum: 84.95219016075134
time_elpased: 2.331
batch start
#iterations: 396
currently lose_sum: 85.0137369632721
time_elpased: 2.351
batch start
#iterations: 397
currently lose_sum: 84.30342012643814
time_elpased: 2.278
batch start
#iterations: 398
currently lose_sum: 84.01534062623978
time_elpased: 2.361
batch start
#iterations: 399
currently lose_sum: 84.64709305763245
time_elpased: 2.308
start validation test
0.56587628866
0.611120645272
0.366471133066
0.458183221822
0.566226375167
89.562
acc: 0.602
pre: 0.632
rec: 0.493
F1: 0.553
auc: 0.602
