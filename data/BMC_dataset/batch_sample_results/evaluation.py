import sklearn.metrics 
import numpy as np
import argparse
import pdb

parser = argparse.ArgumentParser()
parser.add_argument('--ground_truth', type = str)
parser.add_argument('--prediction_value', type = str)
parser.add_argument('--out_path', type = str)
args = parser.parse_args()

ground_truth = np.loadtxt(args.ground_truth)
prediction_value = np.loadtxt(args.prediction_value)

AUC =  sklearn.metrics.roc_auc_score(ground_truth, prediction_value)

with open(args.out_path, 'w') as f:
	f.write(str(AUC) + '\n')

