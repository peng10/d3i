start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.32588762044907
time_elpased: 2.022
batch start
#iterations: 1
currently lose_sum: 100.41453617811203
time_elpased: 1.996
batch start
#iterations: 2
currently lose_sum: 100.38879042863846
time_elpased: 2.032
batch start
#iterations: 3
currently lose_sum: 100.41196501255035
time_elpased: 2.062
batch start
#iterations: 4
currently lose_sum: 100.3270987868309
time_elpased: 2.014
batch start
#iterations: 5
currently lose_sum: 100.19908213615417
time_elpased: 2.056
batch start
#iterations: 6
currently lose_sum: 100.13076561689377
time_elpased: 2.004
batch start
#iterations: 7
currently lose_sum: 100.16495674848557
time_elpased: 2.019
batch start
#iterations: 8
currently lose_sum: 100.03529989719391
time_elpased: 2.006
batch start
#iterations: 9
currently lose_sum: 100.1124050617218
time_elpased: 2.017
batch start
#iterations: 10
currently lose_sum: 100.05232095718384
time_elpased: 2.025
batch start
#iterations: 11
currently lose_sum: 100.04523545503616
time_elpased: 2.01
batch start
#iterations: 12
currently lose_sum: 99.90219235420227
time_elpased: 2.033
batch start
#iterations: 13
currently lose_sum: 100.0045759677887
time_elpased: 2.048
batch start
#iterations: 14
currently lose_sum: 99.83395683765411
time_elpased: 2.21
batch start
#iterations: 15
currently lose_sum: 99.80383747816086
time_elpased: 2.104
batch start
#iterations: 16
currently lose_sum: 99.81394511461258
time_elpased: 1.971
batch start
#iterations: 17
currently lose_sum: 99.82403790950775
time_elpased: 2.034
batch start
#iterations: 18
currently lose_sum: 99.68650829792023
time_elpased: 2.009
batch start
#iterations: 19
currently lose_sum: 99.76155400276184
time_elpased: 2.026
start validation test
0.589020618557
0.605415860735
0.515385407019
0.556784701762
0.589149896528
65.611
batch start
#iterations: 20
currently lose_sum: 99.72233700752258
time_elpased: 1.993
batch start
#iterations: 21
currently lose_sum: 99.5610983967781
time_elpased: 1.996
batch start
#iterations: 22
currently lose_sum: 99.61049604415894
time_elpased: 1.973
batch start
#iterations: 23
currently lose_sum: 99.67563062906265
time_elpased: 2.037
batch start
#iterations: 24
currently lose_sum: 99.47541844844818
time_elpased: 2.148
batch start
#iterations: 25
currently lose_sum: 99.5811882019043
time_elpased: 2.071
batch start
#iterations: 26
currently lose_sum: 99.47321343421936
time_elpased: 1.998
batch start
#iterations: 27
currently lose_sum: 99.54695558547974
time_elpased: 2.014
batch start
#iterations: 28
currently lose_sum: 99.39580243825912
time_elpased: 2.042
batch start
#iterations: 29
currently lose_sum: 99.70513790845871
time_elpased: 2.029
batch start
#iterations: 30
currently lose_sum: 99.41437113285065
time_elpased: 2.006
batch start
#iterations: 31
currently lose_sum: 99.28932166099548
time_elpased: 1.99
batch start
#iterations: 32
currently lose_sum: 99.43882042169571
time_elpased: 1.99
batch start
#iterations: 33
currently lose_sum: 99.46898925304413
time_elpased: 2.0
batch start
#iterations: 34
currently lose_sum: 99.33478772640228
time_elpased: 1.972
batch start
#iterations: 35
currently lose_sum: 99.27294594049454
time_elpased: 2.082
batch start
#iterations: 36
currently lose_sum: 99.33083891868591
time_elpased: 1.978
batch start
#iterations: 37
currently lose_sum: 99.19780743122101
time_elpased: 2.066
batch start
#iterations: 38
currently lose_sum: 98.96394401788712
time_elpased: 2.123
batch start
#iterations: 39
currently lose_sum: 99.28482991456985
time_elpased: 1.997
start validation test
0.571649484536
0.655813953488
0.304723680148
0.41610455312
0.572118113956
65.169
batch start
#iterations: 40
currently lose_sum: 99.30220299959183
time_elpased: 2.016
batch start
#iterations: 41
currently lose_sum: 98.9946259856224
time_elpased: 2.044
batch start
#iterations: 42
currently lose_sum: 99.32229399681091
time_elpased: 2.004
batch start
#iterations: 43
currently lose_sum: 99.1134449839592
time_elpased: 2.002
batch start
#iterations: 44
currently lose_sum: 99.32423919439316
time_elpased: 2.011
batch start
#iterations: 45
currently lose_sum: 98.9727812409401
time_elpased: 2.012
batch start
#iterations: 46
currently lose_sum: 98.98594868183136
time_elpased: 1.989
batch start
#iterations: 47
currently lose_sum: 99.01696121692657
time_elpased: 2.025
batch start
#iterations: 48
currently lose_sum: 99.10535323619843
time_elpased: 2.012
batch start
#iterations: 49
currently lose_sum: 99.1107707619667
time_elpased: 2.021
batch start
#iterations: 50
currently lose_sum: 99.1928191781044
time_elpased: 2.001
batch start
#iterations: 51
currently lose_sum: 98.98651844263077
time_elpased: 2.061
batch start
#iterations: 52
currently lose_sum: 99.1884372830391
time_elpased: 1.993
batch start
#iterations: 53
currently lose_sum: 99.23759251832962
time_elpased: 2.045
batch start
#iterations: 54
currently lose_sum: 99.09889543056488
time_elpased: 2.039
batch start
#iterations: 55
currently lose_sum: 98.75995981693268
time_elpased: 2.007
batch start
#iterations: 56
currently lose_sum: 98.88521295785904
time_elpased: 2.002
batch start
#iterations: 57
currently lose_sum: 98.87910151481628
time_elpased: 1.993
batch start
#iterations: 58
currently lose_sum: 99.11192589998245
time_elpased: 2.056
batch start
#iterations: 59
currently lose_sum: 99.15003228187561
time_elpased: 2.087
start validation test
0.588195876289
0.675752644426
0.34187506432
0.454042233308
0.588628330466
64.483
batch start
#iterations: 60
currently lose_sum: 98.99391740560532
time_elpased: 2.027
batch start
#iterations: 61
currently lose_sum: 99.31231331825256
time_elpased: 2.025
batch start
#iterations: 62
currently lose_sum: 98.91458022594452
time_elpased: 2.02
batch start
#iterations: 63
currently lose_sum: 99.22465443611145
time_elpased: 2.08
batch start
#iterations: 64
currently lose_sum: 98.91694837808609
time_elpased: 2.011
batch start
#iterations: 65
currently lose_sum: 98.79630845785141
time_elpased: 2.039
batch start
#iterations: 66
currently lose_sum: 98.8630883693695
time_elpased: 2.035
batch start
#iterations: 67
currently lose_sum: 98.91606694459915
time_elpased: 2.014
batch start
#iterations: 68
currently lose_sum: 99.11050766706467
time_elpased: 2.019
batch start
#iterations: 69
currently lose_sum: 98.95334422588348
time_elpased: 2.045
batch start
#iterations: 70
currently lose_sum: 98.90849268436432
time_elpased: 1.998
batch start
#iterations: 71
currently lose_sum: 98.98173904418945
time_elpased: 2.044
batch start
#iterations: 72
currently lose_sum: 98.94832861423492
time_elpased: 1.991
batch start
#iterations: 73
currently lose_sum: 98.89860826730728
time_elpased: 2.018
batch start
#iterations: 74
currently lose_sum: 98.839524269104
time_elpased: 2.036
batch start
#iterations: 75
currently lose_sum: 98.8210541009903
time_elpased: 2.008
batch start
#iterations: 76
currently lose_sum: 98.9124636054039
time_elpased: 1.996
batch start
#iterations: 77
currently lose_sum: 99.02012765407562
time_elpased: 2.035
batch start
#iterations: 78
currently lose_sum: 98.99973249435425
time_elpased: 2.025
batch start
#iterations: 79
currently lose_sum: 98.9608143568039
time_elpased: 2.009
start validation test
0.600670103093
0.662917631492
0.412472985489
0.508532639726
0.60100051216
64.467
batch start
#iterations: 80
currently lose_sum: 98.80183106660843
time_elpased: 2.023
batch start
#iterations: 81
currently lose_sum: 98.87998390197754
time_elpased: 2.009
batch start
#iterations: 82
currently lose_sum: 98.85523772239685
time_elpased: 2.033
batch start
#iterations: 83
currently lose_sum: 98.76424378156662
time_elpased: 2.069
batch start
#iterations: 84
currently lose_sum: 99.06450843811035
time_elpased: 2.046
batch start
#iterations: 85
currently lose_sum: 98.96307849884033
time_elpased: 2.024
batch start
#iterations: 86
currently lose_sum: 98.7471330165863
time_elpased: 2.064
batch start
#iterations: 87
currently lose_sum: 98.78551667928696
time_elpased: 2.055
batch start
#iterations: 88
currently lose_sum: 98.88003528118134
time_elpased: 2.006
batch start
#iterations: 89
currently lose_sum: 98.88847333192825
time_elpased: 2.051
batch start
#iterations: 90
currently lose_sum: 98.94357389211655
time_elpased: 2.046
batch start
#iterations: 91
currently lose_sum: 98.87775206565857
time_elpased: 2.121
batch start
#iterations: 92
currently lose_sum: 98.94655060768127
time_elpased: 2.007
batch start
#iterations: 93
currently lose_sum: 98.77057361602783
time_elpased: 2.01
batch start
#iterations: 94
currently lose_sum: 98.70317804813385
time_elpased: 2.024
batch start
#iterations: 95
currently lose_sum: 98.89060145616531
time_elpased: 2.025
batch start
#iterations: 96
currently lose_sum: 98.92308950424194
time_elpased: 2.024
batch start
#iterations: 97
currently lose_sum: 98.8648971915245
time_elpased: 2.036
batch start
#iterations: 98
currently lose_sum: 98.70167076587677
time_elpased: 2.094
batch start
#iterations: 99
currently lose_sum: 98.58214551210403
time_elpased: 2.019
start validation test
0.650979381443
0.654111738858
0.643408459401
0.648715953307
0.650992673365
63.667
batch start
#iterations: 100
currently lose_sum: 98.8076024055481
time_elpased: 2.016
batch start
#iterations: 101
currently lose_sum: 98.85947328805923
time_elpased: 2.04
batch start
#iterations: 102
currently lose_sum: 98.76121151447296
time_elpased: 2.047
batch start
#iterations: 103
currently lose_sum: 98.73814553022385
time_elpased: 2.032
batch start
#iterations: 104
currently lose_sum: 98.7465609908104
time_elpased: 2.036
batch start
#iterations: 105
currently lose_sum: 98.76818692684174
time_elpased: 2.036
batch start
#iterations: 106
currently lose_sum: 98.82980400323868
time_elpased: 2.045
batch start
#iterations: 107
currently lose_sum: 98.87803918123245
time_elpased: 2.075
batch start
#iterations: 108
currently lose_sum: 98.87377065420151
time_elpased: 2.105
batch start
#iterations: 109
currently lose_sum: 98.75369793176651
time_elpased: 2.022
batch start
#iterations: 110
currently lose_sum: 98.70335972309113
time_elpased: 2.085
batch start
#iterations: 111
currently lose_sum: 98.6662267446518
time_elpased: 2.004
batch start
#iterations: 112
currently lose_sum: 98.7727974653244
time_elpased: 2.03
batch start
#iterations: 113
currently lose_sum: 98.73790925741196
time_elpased: 2.065
batch start
#iterations: 114
currently lose_sum: 98.67090356349945
time_elpased: 2.018
batch start
#iterations: 115
currently lose_sum: 98.8159070611
time_elpased: 2.01
batch start
#iterations: 116
currently lose_sum: 99.01798915863037
time_elpased: 2.073
batch start
#iterations: 117
currently lose_sum: 98.96814584732056
time_elpased: 2.006
batch start
#iterations: 118
currently lose_sum: 98.772165954113
time_elpased: 2.031
batch start
#iterations: 119
currently lose_sum: 98.80256414413452
time_elpased: 2.003
start validation test
0.631391752577
0.671524064171
0.516929093342
0.584171657847
0.63159270943
63.723
batch start
#iterations: 120
currently lose_sum: 98.94856947660446
time_elpased: 2.046
batch start
#iterations: 121
currently lose_sum: 98.6749969124794
time_elpased: 2.07
batch start
#iterations: 122
currently lose_sum: 98.97379714250565
time_elpased: 2.025
batch start
#iterations: 123
currently lose_sum: 98.65281456708908
time_elpased: 2.056
batch start
#iterations: 124
currently lose_sum: 98.75104057788849
time_elpased: 2.07
batch start
#iterations: 125
currently lose_sum: 98.8408272266388
time_elpased: 2.034
batch start
#iterations: 126
currently lose_sum: 98.9686399102211
time_elpased: 2.056
batch start
#iterations: 127
currently lose_sum: 98.78943115472794
time_elpased: 2.047
batch start
#iterations: 128
currently lose_sum: 98.48830544948578
time_elpased: 2.038
batch start
#iterations: 129
currently lose_sum: 98.77855253219604
time_elpased: 2.068
batch start
#iterations: 130
currently lose_sum: 98.58135718107224
time_elpased: 2.074
batch start
#iterations: 131
currently lose_sum: 98.63964635133743
time_elpased: 2.131
batch start
#iterations: 132
currently lose_sum: 98.74242520332336
time_elpased: 2.038
batch start
#iterations: 133
currently lose_sum: 98.6127724647522
time_elpased: 2.031
batch start
#iterations: 134
currently lose_sum: 98.70956826210022
time_elpased: 2.023
batch start
#iterations: 135
currently lose_sum: 98.54031336307526
time_elpased: 2.078
batch start
#iterations: 136
currently lose_sum: 98.74892455339432
time_elpased: 2.052
batch start
#iterations: 137
currently lose_sum: 98.49985712766647
time_elpased: 2.026
batch start
#iterations: 138
currently lose_sum: 98.70660662651062
time_elpased: 2.035
batch start
#iterations: 139
currently lose_sum: 98.80034738779068
time_elpased: 2.016
start validation test
0.626134020619
0.672307692308
0.494700010291
0.5699887354
0.626364773296
63.741
batch start
#iterations: 140
currently lose_sum: 98.97507303953171
time_elpased: 2.025
batch start
#iterations: 141
currently lose_sum: 98.5785408616066
time_elpased: 2.01
batch start
#iterations: 142
currently lose_sum: 98.92728352546692
time_elpased: 2.03
batch start
#iterations: 143
currently lose_sum: 98.72718781232834
time_elpased: 2.029
batch start
#iterations: 144
currently lose_sum: 98.82262361049652
time_elpased: 2.062
batch start
#iterations: 145
currently lose_sum: 98.69360822439194
time_elpased: 2.059
batch start
#iterations: 146
currently lose_sum: 98.65574502944946
time_elpased: 2.024
batch start
#iterations: 147
currently lose_sum: 98.64612120389938
time_elpased: 1.992
batch start
#iterations: 148
currently lose_sum: 98.8049613237381
time_elpased: 2.018
batch start
#iterations: 149
currently lose_sum: 98.47331523895264
time_elpased: 2.003
batch start
#iterations: 150
currently lose_sum: 98.47019910812378
time_elpased: 2.021
batch start
#iterations: 151
currently lose_sum: 98.83992671966553
time_elpased: 2.03
batch start
#iterations: 152
currently lose_sum: 98.93886923789978
time_elpased: 2.011
batch start
#iterations: 153
currently lose_sum: 98.7967997789383
time_elpased: 2.059
batch start
#iterations: 154
currently lose_sum: 98.85732048749924
time_elpased: 2.07
batch start
#iterations: 155
currently lose_sum: 98.74494683742523
time_elpased: 2.019
batch start
#iterations: 156
currently lose_sum: 98.76308035850525
time_elpased: 2.026
batch start
#iterations: 157
currently lose_sum: 98.80855000019073
time_elpased: 2.016
batch start
#iterations: 158
currently lose_sum: 98.87940555810928
time_elpased: 2.013
batch start
#iterations: 159
currently lose_sum: 98.67065417766571
time_elpased: 2.013
start validation test
0.636340206186
0.657105760151
0.572913450653
0.612128209357
0.636451561638
63.681
batch start
#iterations: 160
currently lose_sum: 98.633804500103
time_elpased: 2.002
batch start
#iterations: 161
currently lose_sum: 98.46373164653778
time_elpased: 1.996
batch start
#iterations: 162
currently lose_sum: 98.85716760158539
time_elpased: 2.01
batch start
#iterations: 163
currently lose_sum: 98.54246044158936
time_elpased: 2.076
batch start
#iterations: 164
currently lose_sum: 98.62608110904694
time_elpased: 2.03
batch start
#iterations: 165
currently lose_sum: 98.75416898727417
time_elpased: 2.091
batch start
#iterations: 166
currently lose_sum: 98.62678480148315
time_elpased: 2.029
batch start
#iterations: 167
currently lose_sum: 98.82863467931747
time_elpased: 2.014
batch start
#iterations: 168
currently lose_sum: 98.76010477542877
time_elpased: 2.02
batch start
#iterations: 169
currently lose_sum: 98.88016766309738
time_elpased: 2.002
batch start
#iterations: 170
currently lose_sum: 98.68762946128845
time_elpased: 2.033
batch start
#iterations: 171
currently lose_sum: 98.60006242990494
time_elpased: 2.085
batch start
#iterations: 172
currently lose_sum: 98.95240598917007
time_elpased: 2.074
batch start
#iterations: 173
currently lose_sum: 98.67661774158478
time_elpased: 2.048
batch start
#iterations: 174
currently lose_sum: 98.68949764966965
time_elpased: 2.029
batch start
#iterations: 175
currently lose_sum: 98.53427344560623
time_elpased: 2.029
batch start
#iterations: 176
currently lose_sum: 98.59270745515823
time_elpased: 2.011
batch start
#iterations: 177
currently lose_sum: 98.70965850353241
time_elpased: 2.063
batch start
#iterations: 178
currently lose_sum: 98.6244803071022
time_elpased: 2.057
batch start
#iterations: 179
currently lose_sum: 98.9354459643364
time_elpased: 2.048
start validation test
0.65618556701
0.676269813722
0.601523103839
0.636710239651
0.656281535396
63.460
batch start
#iterations: 180
currently lose_sum: 98.7717188000679
time_elpased: 2.024
batch start
#iterations: 181
currently lose_sum: 98.84251773357391
time_elpased: 2.015
batch start
#iterations: 182
currently lose_sum: 98.6049976348877
time_elpased: 2.087
batch start
#iterations: 183
currently lose_sum: 98.44817513227463
time_elpased: 2.083
batch start
#iterations: 184
currently lose_sum: 98.4873241186142
time_elpased: 2.023
batch start
#iterations: 185
currently lose_sum: 98.51743692159653
time_elpased: 2.073
batch start
#iterations: 186
currently lose_sum: 98.51795983314514
time_elpased: 2.02
batch start
#iterations: 187
currently lose_sum: 98.54281806945801
time_elpased: 2.011
batch start
#iterations: 188
currently lose_sum: 98.71522748470306
time_elpased: 2.072
batch start
#iterations: 189
currently lose_sum: 98.6443607211113
time_elpased: 2.035
batch start
#iterations: 190
currently lose_sum: 98.8487349152565
time_elpased: 2.051
batch start
#iterations: 191
currently lose_sum: 98.56973028182983
time_elpased: 2.036
batch start
#iterations: 192
currently lose_sum: 98.52088934183121
time_elpased: 2.021
batch start
#iterations: 193
currently lose_sum: 98.53721696138382
time_elpased: 2.039
batch start
#iterations: 194
currently lose_sum: 98.98945981264114
time_elpased: 2.065
batch start
#iterations: 195
currently lose_sum: 98.54098749160767
time_elpased: 2.053
batch start
#iterations: 196
currently lose_sum: 98.84750878810883
time_elpased: 2.053
batch start
#iterations: 197
currently lose_sum: 98.69314932823181
time_elpased: 2.027
batch start
#iterations: 198
currently lose_sum: 98.71171337366104
time_elpased: 2.069
batch start
#iterations: 199
currently lose_sum: 98.52493280172348
time_elpased: 2.032
start validation test
0.662319587629
0.639128142028
0.748379129361
0.68945247689
0.66216849683
63.320
batch start
#iterations: 200
currently lose_sum: 98.63005548715591
time_elpased: 2.059
batch start
#iterations: 201
currently lose_sum: 98.35062724351883
time_elpased: 2.015
batch start
#iterations: 202
currently lose_sum: 98.83170610666275
time_elpased: 1.995
batch start
#iterations: 203
currently lose_sum: 98.43140840530396
time_elpased: 1.998
batch start
#iterations: 204
currently lose_sum: 98.65218168497086
time_elpased: 2.048
batch start
#iterations: 205
currently lose_sum: 98.67936050891876
time_elpased: 2.078
batch start
#iterations: 206
currently lose_sum: 98.49094450473785
time_elpased: 2.047
batch start
#iterations: 207
currently lose_sum: 98.61279648542404
time_elpased: 2.045
batch start
#iterations: 208
currently lose_sum: 98.30594515800476
time_elpased: 2.071
batch start
#iterations: 209
currently lose_sum: 98.62526941299438
time_elpased: 2.048
batch start
#iterations: 210
currently lose_sum: 98.3424773812294
time_elpased: 2.042
batch start
#iterations: 211
currently lose_sum: 98.3185413479805
time_elpased: 2.065
batch start
#iterations: 212
currently lose_sum: 98.61003214120865
time_elpased: 2.046
batch start
#iterations: 213
currently lose_sum: 98.7519982457161
time_elpased: 2.031
batch start
#iterations: 214
currently lose_sum: 98.58773630857468
time_elpased: 2.073
batch start
#iterations: 215
currently lose_sum: 98.37557572126389
time_elpased: 2.056
batch start
#iterations: 216
currently lose_sum: 98.3889912366867
time_elpased: 2.022
batch start
#iterations: 217
currently lose_sum: 98.60506856441498
time_elpased: 1.984
batch start
#iterations: 218
currently lose_sum: 98.58689951896667
time_elpased: 2.037
batch start
#iterations: 219
currently lose_sum: 98.87272870540619
time_elpased: 2.046
start validation test
0.644381443299
0.662103083295
0.592260985901
0.625237655495
0.644472948801
63.266
batch start
#iterations: 220
currently lose_sum: 98.45556145906448
time_elpased: 2.024
batch start
#iterations: 221
currently lose_sum: 98.82297360897064
time_elpased: 2.024
batch start
#iterations: 222
currently lose_sum: 98.42346674203873
time_elpased: 2.018
batch start
#iterations: 223
currently lose_sum: 98.53199625015259
time_elpased: 2.07
batch start
#iterations: 224
currently lose_sum: 98.7344080209732
time_elpased: 2.042
batch start
#iterations: 225
currently lose_sum: 98.61662113666534
time_elpased: 2.028
batch start
#iterations: 226
currently lose_sum: 98.50139033794403
time_elpased: 2.016
batch start
#iterations: 227
currently lose_sum: 98.4398255944252
time_elpased: 2.029
batch start
#iterations: 228
currently lose_sum: 98.39282959699631
time_elpased: 2.05
batch start
#iterations: 229
currently lose_sum: 98.6450931429863
time_elpased: 2.083
batch start
#iterations: 230
currently lose_sum: 98.74486595392227
time_elpased: 2.049
batch start
#iterations: 231
currently lose_sum: 98.53980976343155
time_elpased: 2.059
batch start
#iterations: 232
currently lose_sum: 98.48238003253937
time_elpased: 2.018
batch start
#iterations: 233
currently lose_sum: 98.55869668722153
time_elpased: 2.097
batch start
#iterations: 234
currently lose_sum: 98.35550689697266
time_elpased: 2.103
batch start
#iterations: 235
currently lose_sum: 98.41649621725082
time_elpased: 2.059
batch start
#iterations: 236
currently lose_sum: 98.73927509784698
time_elpased: 2.061
batch start
#iterations: 237
currently lose_sum: 98.79080778360367
time_elpased: 2.053
batch start
#iterations: 238
currently lose_sum: 98.59855276346207
time_elpased: 2.008
batch start
#iterations: 239
currently lose_sum: 98.73600995540619
time_elpased: 2.006
start validation test
0.641391752577
0.674418604651
0.54914068128
0.605366158035
0.641553713562
63.481
batch start
#iterations: 240
currently lose_sum: 98.4307296872139
time_elpased: 2.027
batch start
#iterations: 241
currently lose_sum: 98.48760974407196
time_elpased: 2.039
batch start
#iterations: 242
currently lose_sum: 98.62414956092834
time_elpased: 2.068
batch start
#iterations: 243
currently lose_sum: 98.71354448795319
time_elpased: 2.054
batch start
#iterations: 244
currently lose_sum: 98.73270362615585
time_elpased: 2.033
batch start
#iterations: 245
currently lose_sum: 98.77066802978516
time_elpased: 2.069
batch start
#iterations: 246
currently lose_sum: 98.36824589967728
time_elpased: 2.138
batch start
#iterations: 247
currently lose_sum: 98.68110316991806
time_elpased: 2.001
batch start
#iterations: 248
currently lose_sum: 98.59605866670609
time_elpased: 2.04
batch start
#iterations: 249
currently lose_sum: 98.69895702600479
time_elpased: 2.043
batch start
#iterations: 250
currently lose_sum: 98.38748914003372
time_elpased: 2.028
batch start
#iterations: 251
currently lose_sum: 98.61502772569656
time_elpased: 2.057
batch start
#iterations: 252
currently lose_sum: 98.56267178058624
time_elpased: 2.074
batch start
#iterations: 253
currently lose_sum: 98.7133052945137
time_elpased: 2.051
batch start
#iterations: 254
currently lose_sum: 98.38164353370667
time_elpased: 2.031
batch start
#iterations: 255
currently lose_sum: 98.57255309820175
time_elpased: 2.072
batch start
#iterations: 256
currently lose_sum: 98.43245667219162
time_elpased: 2.043
batch start
#iterations: 257
currently lose_sum: 98.53074705600739
time_elpased: 2.031
batch start
#iterations: 258
currently lose_sum: 98.73302555084229
time_elpased: 2.046
batch start
#iterations: 259
currently lose_sum: 98.41847097873688
time_elpased: 2.036
start validation test
0.66087628866
0.630990148606
0.777812081918
0.696750403319
0.660670989839
63.170
batch start
#iterations: 260
currently lose_sum: 98.53964585065842
time_elpased: 2.056
batch start
#iterations: 261
currently lose_sum: 98.42322409152985
time_elpased: 2.077
batch start
#iterations: 262
currently lose_sum: 98.48156076669693
time_elpased: 2.022
batch start
#iterations: 263
currently lose_sum: 98.37119567394257
time_elpased: 2.038
batch start
#iterations: 264
currently lose_sum: 98.57879263162613
time_elpased: 2.033
batch start
#iterations: 265
currently lose_sum: 98.72651940584183
time_elpased: 2.082
batch start
#iterations: 266
currently lose_sum: 98.66881257295609
time_elpased: 2.023
batch start
#iterations: 267
currently lose_sum: 98.53439980745316
time_elpased: 2.025
batch start
#iterations: 268
currently lose_sum: 98.48783046007156
time_elpased: 2.037
batch start
#iterations: 269
currently lose_sum: 98.44610577821732
time_elpased: 2.037
batch start
#iterations: 270
currently lose_sum: 98.5774576663971
time_elpased: 2.034
batch start
#iterations: 271
currently lose_sum: 98.69749003648758
time_elpased: 2.022
batch start
#iterations: 272
currently lose_sum: 98.54961639642715
time_elpased: 2.023
batch start
#iterations: 273
currently lose_sum: 98.41322839260101
time_elpased: 2.037
batch start
#iterations: 274
currently lose_sum: 98.7040051817894
time_elpased: 2.03
batch start
#iterations: 275
currently lose_sum: 98.51682710647583
time_elpased: 2.075
batch start
#iterations: 276
currently lose_sum: 98.4450975060463
time_elpased: 2.005
batch start
#iterations: 277
currently lose_sum: 98.7280986905098
time_elpased: 2.019
batch start
#iterations: 278
currently lose_sum: 98.55197149515152
time_elpased: 2.046
batch start
#iterations: 279
currently lose_sum: 98.23401653766632
time_elpased: 2.024
start validation test
0.620309278351
0.682559403634
0.452300092621
0.544070314434
0.62060424439
63.358
batch start
#iterations: 280
currently lose_sum: 98.39014410972595
time_elpased: 2.15
batch start
#iterations: 281
currently lose_sum: 98.66126871109009
time_elpased: 2.021
batch start
#iterations: 282
currently lose_sum: 98.46989905834198
time_elpased: 2.056
batch start
#iterations: 283
currently lose_sum: 98.63866168260574
time_elpased: 2.055
batch start
#iterations: 284
currently lose_sum: 98.34236788749695
time_elpased: 2.017
batch start
#iterations: 285
currently lose_sum: 98.62131989002228
time_elpased: 2.016
batch start
#iterations: 286
currently lose_sum: 98.2082781791687
time_elpased: 2.016
batch start
#iterations: 287
currently lose_sum: 98.49201911687851
time_elpased: 2.05
batch start
#iterations: 288
currently lose_sum: 98.3937674164772
time_elpased: 2.006
batch start
#iterations: 289
currently lose_sum: 98.69443219900131
time_elpased: 2.027
batch start
#iterations: 290
currently lose_sum: 98.63301104307175
time_elpased: 2.028
batch start
#iterations: 291
currently lose_sum: 98.55005979537964
time_elpased: 2.017
batch start
#iterations: 292
currently lose_sum: 98.4925047159195
time_elpased: 2.009
batch start
#iterations: 293
currently lose_sum: 98.5417650938034
time_elpased: 2.035
batch start
#iterations: 294
currently lose_sum: 98.26102709770203
time_elpased: 2.118
batch start
#iterations: 295
currently lose_sum: 98.68044722080231
time_elpased: 1.99
batch start
#iterations: 296
currently lose_sum: 98.37281668186188
time_elpased: 2.011
batch start
#iterations: 297
currently lose_sum: 98.48863780498505
time_elpased: 2.107
batch start
#iterations: 298
currently lose_sum: 98.63697975873947
time_elpased: 2.025
batch start
#iterations: 299
currently lose_sum: 98.75818657875061
time_elpased: 2.034
start validation test
0.611855670103
0.684494685338
0.417515694144
0.518665302992
0.612196863906
63.420
batch start
#iterations: 300
currently lose_sum: 98.19999831914902
time_elpased: 2.018
batch start
#iterations: 301
currently lose_sum: 98.4793182015419
time_elpased: 2.137
batch start
#iterations: 302
currently lose_sum: 98.47086364030838
time_elpased: 2.089
batch start
#iterations: 303
currently lose_sum: 98.55340510606766
time_elpased: 2.115
batch start
#iterations: 304
currently lose_sum: 98.51944327354431
time_elpased: 2.058
batch start
#iterations: 305
currently lose_sum: 98.67697978019714
time_elpased: 2.047
batch start
#iterations: 306
currently lose_sum: 98.5572834610939
time_elpased: 2.012
batch start
#iterations: 307
currently lose_sum: 98.34642112255096
time_elpased: 2.028
batch start
#iterations: 308
currently lose_sum: 98.41969114542007
time_elpased: 2.029
batch start
#iterations: 309
currently lose_sum: 98.59393894672394
time_elpased: 2.005
batch start
#iterations: 310
currently lose_sum: 98.66476148366928
time_elpased: 2.022
batch start
#iterations: 311
currently lose_sum: 98.16949206590652
time_elpased: 1.996
batch start
#iterations: 312
currently lose_sum: 98.60345512628555
time_elpased: 2.088
batch start
#iterations: 313
currently lose_sum: 98.69846177101135
time_elpased: 2.06
batch start
#iterations: 314
currently lose_sum: 98.19564479589462
time_elpased: 2.012
batch start
#iterations: 315
currently lose_sum: 98.43488055467606
time_elpased: 2.009
batch start
#iterations: 316
currently lose_sum: 98.6134062409401
time_elpased: 2.011
batch start
#iterations: 317
currently lose_sum: 98.30886042118073
time_elpased: 2.007
batch start
#iterations: 318
currently lose_sum: 98.42238664627075
time_elpased: 1.998
batch start
#iterations: 319
currently lose_sum: 98.76041632890701
time_elpased: 2.117
start validation test
0.652886597938
0.668778997397
0.608212411238
0.637059394201
0.652965030363
63.094
batch start
#iterations: 320
currently lose_sum: 98.54573810100555
time_elpased: 2.03
batch start
#iterations: 321
currently lose_sum: 98.41327029466629
time_elpased: 2.018
batch start
#iterations: 322
currently lose_sum: 98.44740760326385
time_elpased: 2.005
batch start
#iterations: 323
currently lose_sum: 98.82226586341858
time_elpased: 2.021
batch start
#iterations: 324
currently lose_sum: 98.68230670690536
time_elpased: 2.078
batch start
#iterations: 325
currently lose_sum: 98.3298117518425
time_elpased: 2.07
batch start
#iterations: 326
currently lose_sum: 98.76515990495682
time_elpased: 2.048
batch start
#iterations: 327
currently lose_sum: 98.39011293649673
time_elpased: 2.042
batch start
#iterations: 328
currently lose_sum: 98.37185299396515
time_elpased: 2.009
batch start
#iterations: 329
currently lose_sum: 98.43722981214523
time_elpased: 2.018
batch start
#iterations: 330
currently lose_sum: 98.34003257751465
time_elpased: 2.036
batch start
#iterations: 331
currently lose_sum: 98.34433102607727
time_elpased: 2.011
batch start
#iterations: 332
currently lose_sum: 98.61046040058136
time_elpased: 2.02
batch start
#iterations: 333
currently lose_sum: 98.61441165208817
time_elpased: 2.024
batch start
#iterations: 334
currently lose_sum: 98.4315397143364
time_elpased: 2.056
batch start
#iterations: 335
currently lose_sum: 98.39865225553513
time_elpased: 2.02
batch start
#iterations: 336
currently lose_sum: 98.58172941207886
time_elpased: 2.056
batch start
#iterations: 337
currently lose_sum: 98.37248682975769
time_elpased: 2.018
batch start
#iterations: 338
currently lose_sum: 98.57839423418045
time_elpased: 2.039
batch start
#iterations: 339
currently lose_sum: 98.42422330379486
time_elpased: 2.034
start validation test
0.655515463918
0.67944168441
0.591128949264
0.63221616862
0.655628504375
63.124
batch start
#iterations: 340
currently lose_sum: 98.5257396697998
time_elpased: 2.031
batch start
#iterations: 341
currently lose_sum: 98.60634511709213
time_elpased: 2.026
batch start
#iterations: 342
currently lose_sum: 98.48249918222427
time_elpased: 2.0
batch start
#iterations: 343
currently lose_sum: 98.51218903064728
time_elpased: 2.022
batch start
#iterations: 344
currently lose_sum: 98.47316187620163
time_elpased: 2.075
batch start
#iterations: 345
currently lose_sum: 98.40319925546646
time_elpased: 2.003
batch start
#iterations: 346
currently lose_sum: 98.43718999624252
time_elpased: 2.066
batch start
#iterations: 347
currently lose_sum: 98.48172783851624
time_elpased: 2.002
batch start
#iterations: 348
currently lose_sum: 98.45138388872147
time_elpased: 2.007
batch start
#iterations: 349
currently lose_sum: 98.53255212306976
time_elpased: 2.003
batch start
#iterations: 350
currently lose_sum: 98.40925461053848
time_elpased: 2.026
batch start
#iterations: 351
currently lose_sum: 98.60120397806168
time_elpased: 2.043
batch start
#iterations: 352
currently lose_sum: 98.52293878793716
time_elpased: 2.024
batch start
#iterations: 353
currently lose_sum: 98.69087028503418
time_elpased: 2.028
batch start
#iterations: 354
currently lose_sum: 98.97665512561798
time_elpased: 2.037
batch start
#iterations: 355
currently lose_sum: 98.60443639755249
time_elpased: 2.015
batch start
#iterations: 356
currently lose_sum: 98.59225726127625
time_elpased: 2.018
batch start
#iterations: 357
currently lose_sum: 98.6324822306633
time_elpased: 2.026
batch start
#iterations: 358
currently lose_sum: 98.57063192129135
time_elpased: 2.001
batch start
#iterations: 359
currently lose_sum: 98.49931061267853
time_elpased: 2.002
start validation test
0.660309278351
0.674089745017
0.623031799938
0.64755588833
0.660374724714
63.079
batch start
#iterations: 360
currently lose_sum: 98.65126866102219
time_elpased: 2.014
batch start
#iterations: 361
currently lose_sum: 98.59611684083939
time_elpased: 2.015
batch start
#iterations: 362
currently lose_sum: 98.36516225337982
time_elpased: 2.037
batch start
#iterations: 363
currently lose_sum: 98.73721033334732
time_elpased: 2.011
batch start
#iterations: 364
currently lose_sum: 98.81347370147705
time_elpased: 2.034
batch start
#iterations: 365
currently lose_sum: 98.65521985292435
time_elpased: 2.036
batch start
#iterations: 366
currently lose_sum: 98.50360316038132
time_elpased: 2.056
batch start
#iterations: 367
currently lose_sum: 98.3882868885994
time_elpased: 2.026
batch start
#iterations: 368
currently lose_sum: 98.46780222654343
time_elpased: 2.056
batch start
#iterations: 369
currently lose_sum: 98.51201504468918
time_elpased: 2.004
batch start
#iterations: 370
currently lose_sum: 98.40735012292862
time_elpased: 2.027
batch start
#iterations: 371
currently lose_sum: 98.48561441898346
time_elpased: 2.003
batch start
#iterations: 372
currently lose_sum: 98.60356670618057
time_elpased: 2.091
batch start
#iterations: 373
currently lose_sum: 98.31824541091919
time_elpased: 2.011
batch start
#iterations: 374
currently lose_sum: 98.45765542984009
time_elpased: 2.031
batch start
#iterations: 375
currently lose_sum: 98.50647503137589
time_elpased: 2.066
batch start
#iterations: 376
currently lose_sum: 98.3912610411644
time_elpased: 2.058
batch start
#iterations: 377
currently lose_sum: 98.60088866949081
time_elpased: 2.033
batch start
#iterations: 378
currently lose_sum: 98.47425800561905
time_elpased: 2.02
batch start
#iterations: 379
currently lose_sum: 98.32916897535324
time_elpased: 2.023
start validation test
0.605979381443
0.687737728672
0.390758464547
0.498359364746
0.606357234958
63.491
batch start
#iterations: 380
currently lose_sum: 98.5091016292572
time_elpased: 2.004
batch start
#iterations: 381
currently lose_sum: 98.4069904088974
time_elpased: 2.036
batch start
#iterations: 382
currently lose_sum: 98.54147446155548
time_elpased: 2.038
batch start
#iterations: 383
currently lose_sum: 98.46443945169449
time_elpased: 2.023
batch start
#iterations: 384
currently lose_sum: 98.58619749546051
time_elpased: 2.06
batch start
#iterations: 385
currently lose_sum: 98.4244658946991
time_elpased: 2.035
batch start
#iterations: 386
currently lose_sum: 98.47958326339722
time_elpased: 2.053
batch start
#iterations: 387
currently lose_sum: 98.3506795167923
time_elpased: 2.057
batch start
#iterations: 388
currently lose_sum: 98.52830547094345
time_elpased: 2.034
batch start
#iterations: 389
currently lose_sum: 98.40113765001297
time_elpased: 2.046
batch start
#iterations: 390
currently lose_sum: 98.32902270555496
time_elpased: 2.016
batch start
#iterations: 391
currently lose_sum: 98.26278686523438
time_elpased: 2.015
batch start
#iterations: 392
currently lose_sum: 98.39842516183853
time_elpased: 2.026
batch start
#iterations: 393
currently lose_sum: 98.37664622068405
time_elpased: 1.987
batch start
#iterations: 394
currently lose_sum: 98.47044110298157
time_elpased: 2.047
batch start
#iterations: 395
currently lose_sum: 98.47654139995575
time_elpased: 2.016
batch start
#iterations: 396
currently lose_sum: 98.17874163389206
time_elpased: 2.062
batch start
#iterations: 397
currently lose_sum: 98.26997190713882
time_elpased: 2.005
batch start
#iterations: 398
currently lose_sum: 98.5366889834404
time_elpased: 2.01
batch start
#iterations: 399
currently lose_sum: 98.61392521858215
time_elpased: 1.99
start validation test
0.588608247423
0.682736842105
0.333744983019
0.448330683625
0.589055699193
63.723
acc: 0.651
pre: 0.665
rec: 0.611
F1: 0.637
auc: 0.651
