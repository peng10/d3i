start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 101.3798001408577
time_elpased: 2.162
batch start
#iterations: 1
currently lose_sum: 100.44381165504456
time_elpased: 1.979
batch start
#iterations: 2
currently lose_sum: 100.4021412730217
time_elpased: 1.979
batch start
#iterations: 3
currently lose_sum: 100.21526032686234
time_elpased: 1.974
batch start
#iterations: 4
currently lose_sum: 100.27895373106003
time_elpased: 1.96
batch start
#iterations: 5
currently lose_sum: 100.21930581331253
time_elpased: 1.971
batch start
#iterations: 6
currently lose_sum: 100.16781330108643
time_elpased: 1.968
batch start
#iterations: 7
currently lose_sum: 100.01000702381134
time_elpased: 1.998
batch start
#iterations: 8
currently lose_sum: 99.92653423547745
time_elpased: 2.015
batch start
#iterations: 9
currently lose_sum: 99.96513837575912
time_elpased: 1.978
batch start
#iterations: 10
currently lose_sum: 99.63817590475082
time_elpased: 1.976
batch start
#iterations: 11
currently lose_sum: 99.66220676898956
time_elpased: 2.004
batch start
#iterations: 12
currently lose_sum: 99.5099269747734
time_elpased: 1.976
batch start
#iterations: 13
currently lose_sum: 99.58061993122101
time_elpased: 1.982
batch start
#iterations: 14
currently lose_sum: 99.24412643909454
time_elpased: 1.959
batch start
#iterations: 15
currently lose_sum: 99.27373385429382
time_elpased: 1.987
batch start
#iterations: 16
currently lose_sum: 98.97922438383102
time_elpased: 1.973
batch start
#iterations: 17
currently lose_sum: 98.74086111783981
time_elpased: 2.008
batch start
#iterations: 18
currently lose_sum: 98.63737004995346
time_elpased: 1.987
batch start
#iterations: 19
currently lose_sum: 98.92879915237427
time_elpased: 1.96
start validation test
0.603917525773
0.646280040294
0.462179685088
0.538941557662
0.604166368414
64.767
batch start
#iterations: 20
currently lose_sum: 99.16050487756729
time_elpased: 1.983
batch start
#iterations: 21
currently lose_sum: 98.27634286880493
time_elpased: 1.965
batch start
#iterations: 22
currently lose_sum: 98.22121697664261
time_elpased: 1.952
batch start
#iterations: 23
currently lose_sum: 98.61015331745148
time_elpased: 1.965
batch start
#iterations: 24
currently lose_sum: 98.40857142210007
time_elpased: 1.977
batch start
#iterations: 25
currently lose_sum: 98.110535800457
time_elpased: 1.952
batch start
#iterations: 26
currently lose_sum: 98.36394602060318
time_elpased: 1.972
batch start
#iterations: 27
currently lose_sum: 97.61309760808945
time_elpased: 1.996
batch start
#iterations: 28
currently lose_sum: 98.31217753887177
time_elpased: 1.986
batch start
#iterations: 29
currently lose_sum: 97.74081987142563
time_elpased: 1.966
batch start
#iterations: 30
currently lose_sum: 97.72535401582718
time_elpased: 1.974
batch start
#iterations: 31
currently lose_sum: 97.89407932758331
time_elpased: 2.015
batch start
#iterations: 32
currently lose_sum: 97.96996092796326
time_elpased: 2.02
batch start
#iterations: 33
currently lose_sum: 97.76256793737411
time_elpased: 1.999
batch start
#iterations: 34
currently lose_sum: 97.64394062757492
time_elpased: 1.978
batch start
#iterations: 35
currently lose_sum: 97.3084145784378
time_elpased: 2.028
batch start
#iterations: 36
currently lose_sum: 97.46312361955643
time_elpased: 1.961
batch start
#iterations: 37
currently lose_sum: 97.36403572559357
time_elpased: 1.977
batch start
#iterations: 38
currently lose_sum: 97.68291878700256
time_elpased: 1.964
batch start
#iterations: 39
currently lose_sum: 96.96516633033752
time_elpased: 1.966
start validation test
0.648556701031
0.619133722364
0.77523927138
0.68844818132
0.648334290239
62.632
batch start
#iterations: 40
currently lose_sum: 97.36896884441376
time_elpased: 2.056
batch start
#iterations: 41
currently lose_sum: 97.22592860460281
time_elpased: 1.991
batch start
#iterations: 42
currently lose_sum: 97.22579634189606
time_elpased: 2.021
batch start
#iterations: 43
currently lose_sum: 97.3183713555336
time_elpased: 1.994
batch start
#iterations: 44
currently lose_sum: 97.04864555597305
time_elpased: 1.983
batch start
#iterations: 45
currently lose_sum: 97.37969714403152
time_elpased: 1.976
batch start
#iterations: 46
currently lose_sum: 97.01008868217468
time_elpased: 1.968
batch start
#iterations: 47
currently lose_sum: 97.24076157808304
time_elpased: 1.987
batch start
#iterations: 48
currently lose_sum: 97.40149265527725
time_elpased: 2.022
batch start
#iterations: 49
currently lose_sum: 97.43656098842621
time_elpased: 1.989
batch start
#iterations: 50
currently lose_sum: 96.84305900335312
time_elpased: 1.989
batch start
#iterations: 51
currently lose_sum: 96.82201182842255
time_elpased: 2.014
batch start
#iterations: 52
currently lose_sum: 97.14046198129654
time_elpased: 2.029
batch start
#iterations: 53
currently lose_sum: 96.73047548532486
time_elpased: 1.95
batch start
#iterations: 54
currently lose_sum: 97.49120312929153
time_elpased: 1.978
batch start
#iterations: 55
currently lose_sum: 96.98080140352249
time_elpased: 2.002
batch start
#iterations: 56
currently lose_sum: 96.86963200569153
time_elpased: 2.029
batch start
#iterations: 57
currently lose_sum: 97.26871663331985
time_elpased: 1.985
batch start
#iterations: 58
currently lose_sum: 97.09347689151764
time_elpased: 1.959
batch start
#iterations: 59
currently lose_sum: 97.41155326366425
time_elpased: 1.983
start validation test
0.650051546392
0.624405166553
0.756200473397
0.684012101466
0.649865185578
62.424
batch start
#iterations: 60
currently lose_sum: 97.12557274103165
time_elpased: 2.011
batch start
#iterations: 61
currently lose_sum: 96.93144363164902
time_elpased: 1.951
batch start
#iterations: 62
currently lose_sum: 97.15624976158142
time_elpased: 1.959
batch start
#iterations: 63
currently lose_sum: 96.68442142009735
time_elpased: 1.991
batch start
#iterations: 64
currently lose_sum: 96.85754978656769
time_elpased: 1.97
batch start
#iterations: 65
currently lose_sum: 96.80798757076263
time_elpased: 1.945
batch start
#iterations: 66
currently lose_sum: 96.97553968429565
time_elpased: 1.947
batch start
#iterations: 67
currently lose_sum: 96.78266835212708
time_elpased: 1.977
batch start
#iterations: 68
currently lose_sum: 96.42010194063187
time_elpased: 1.99
batch start
#iterations: 69
currently lose_sum: 96.60277950763702
time_elpased: 1.998
batch start
#iterations: 70
currently lose_sum: 96.71946823596954
time_elpased: 1.957
batch start
#iterations: 71
currently lose_sum: 96.88234221935272
time_elpased: 1.964
batch start
#iterations: 72
currently lose_sum: 96.59605360031128
time_elpased: 1.991
batch start
#iterations: 73
currently lose_sum: 96.86376440525055
time_elpased: 1.961
batch start
#iterations: 74
currently lose_sum: 96.64412188529968
time_elpased: 1.981
batch start
#iterations: 75
currently lose_sum: 96.61924654245377
time_elpased: 1.981
batch start
#iterations: 76
currently lose_sum: 96.87638992071152
time_elpased: 1.958
batch start
#iterations: 77
currently lose_sum: 96.82105088233948
time_elpased: 2.0
batch start
#iterations: 78
currently lose_sum: 96.6848561167717
time_elpased: 1.982
batch start
#iterations: 79
currently lose_sum: 96.43066656589508
time_elpased: 2.014
start validation test
0.640206185567
0.651131971287
0.606771637337
0.628169614319
0.640264885073
62.181
batch start
#iterations: 80
currently lose_sum: 96.44236898422241
time_elpased: 1.989
batch start
#iterations: 81
currently lose_sum: 96.50078845024109
time_elpased: 1.968
batch start
#iterations: 82
currently lose_sum: 96.92742037773132
time_elpased: 1.982
batch start
#iterations: 83
currently lose_sum: 96.67832332849503
time_elpased: 1.964
batch start
#iterations: 84
currently lose_sum: 96.58853369951248
time_elpased: 1.964
batch start
#iterations: 85
currently lose_sum: 96.73104244470596
time_elpased: 1.987
batch start
#iterations: 86
currently lose_sum: 96.85257470607758
time_elpased: 1.984
batch start
#iterations: 87
currently lose_sum: 96.38881927728653
time_elpased: 2.003
batch start
#iterations: 88
currently lose_sum: 96.49443966150284
time_elpased: 1.994
batch start
#iterations: 89
currently lose_sum: 96.42406415939331
time_elpased: 1.986
batch start
#iterations: 90
currently lose_sum: 96.3011879324913
time_elpased: 1.961
batch start
#iterations: 91
currently lose_sum: 96.80596321821213
time_elpased: 1.972
batch start
#iterations: 92
currently lose_sum: 96.74778842926025
time_elpased: 2.006
batch start
#iterations: 93
currently lose_sum: 96.31957632303238
time_elpased: 1.987
batch start
#iterations: 94
currently lose_sum: 96.20793336629868
time_elpased: 1.996
batch start
#iterations: 95
currently lose_sum: 96.7761579155922
time_elpased: 1.974
batch start
#iterations: 96
currently lose_sum: 96.55950337648392
time_elpased: 2.003
batch start
#iterations: 97
currently lose_sum: 96.69606983661652
time_elpased: 2.005
batch start
#iterations: 98
currently lose_sum: 96.2612515091896
time_elpased: 2.017
batch start
#iterations: 99
currently lose_sum: 96.6581197977066
time_elpased: 1.988
start validation test
0.650103092784
0.627071583514
0.743748070392
0.680444402599
0.649938684582
61.944
batch start
#iterations: 100
currently lose_sum: 96.28609931468964
time_elpased: 1.976
batch start
#iterations: 101
currently lose_sum: 96.53000295162201
time_elpased: 1.957
batch start
#iterations: 102
currently lose_sum: 96.26217514276505
time_elpased: 2.06
batch start
#iterations: 103
currently lose_sum: 96.64465898275375
time_elpased: 1.989
batch start
#iterations: 104
currently lose_sum: 96.3005074262619
time_elpased: 1.967
batch start
#iterations: 105
currently lose_sum: 96.3502025604248
time_elpased: 2.003
batch start
#iterations: 106
currently lose_sum: 96.3458281159401
time_elpased: 1.956
batch start
#iterations: 107
currently lose_sum: 96.26196122169495
time_elpased: 1.981
batch start
#iterations: 108
currently lose_sum: 96.64853924512863
time_elpased: 1.971
batch start
#iterations: 109
currently lose_sum: 96.42252027988434
time_elpased: 1.967
batch start
#iterations: 110
currently lose_sum: 96.1014135479927
time_elpased: 1.984
batch start
#iterations: 111
currently lose_sum: 96.36591809988022
time_elpased: 1.979
batch start
#iterations: 112
currently lose_sum: 96.5198101401329
time_elpased: 1.999
batch start
#iterations: 113
currently lose_sum: 96.1455420255661
time_elpased: 1.958
batch start
#iterations: 114
currently lose_sum: 96.12851643562317
time_elpased: 2.02
batch start
#iterations: 115
currently lose_sum: 96.0138292312622
time_elpased: 1.957
batch start
#iterations: 116
currently lose_sum: 96.4395980834961
time_elpased: 1.972
batch start
#iterations: 117
currently lose_sum: 96.30435144901276
time_elpased: 1.976
batch start
#iterations: 118
currently lose_sum: 96.22211015224457
time_elpased: 1.974
batch start
#iterations: 119
currently lose_sum: 96.12219059467316
time_elpased: 2.015
start validation test
0.649072164948
0.635617715618
0.701553977565
0.666960180022
0.648980025032
61.610
batch start
#iterations: 120
currently lose_sum: 96.08554321527481
time_elpased: 2.006
batch start
#iterations: 121
currently lose_sum: 96.46053516864777
time_elpased: 1.966
batch start
#iterations: 122
currently lose_sum: 96.30815553665161
time_elpased: 2.004
batch start
#iterations: 123
currently lose_sum: 96.46085596084595
time_elpased: 1.958
batch start
#iterations: 124
currently lose_sum: 96.36677980422974
time_elpased: 1.999
batch start
#iterations: 125
currently lose_sum: 95.95218908786774
time_elpased: 1.965
batch start
#iterations: 126
currently lose_sum: 95.95738363265991
time_elpased: 1.978
batch start
#iterations: 127
currently lose_sum: 96.66551607847214
time_elpased: 1.995
batch start
#iterations: 128
currently lose_sum: 96.19024789333344
time_elpased: 1.974
batch start
#iterations: 129
currently lose_sum: 95.90628969669342
time_elpased: 1.985
batch start
#iterations: 130
currently lose_sum: 96.36349457502365
time_elpased: 1.958
batch start
#iterations: 131
currently lose_sum: 96.09193921089172
time_elpased: 1.968
batch start
#iterations: 132
currently lose_sum: 96.10097819566727
time_elpased: 1.995
batch start
#iterations: 133
currently lose_sum: 96.17695438861847
time_elpased: 1.966
batch start
#iterations: 134
currently lose_sum: 96.20098686218262
time_elpased: 1.983
batch start
#iterations: 135
currently lose_sum: 96.510577917099
time_elpased: 1.976
batch start
#iterations: 136
currently lose_sum: 96.16789376735687
time_elpased: 2.004
batch start
#iterations: 137
currently lose_sum: 96.12438386678696
time_elpased: 1.994
batch start
#iterations: 138
currently lose_sum: 96.23329943418503
time_elpased: 1.969
batch start
#iterations: 139
currently lose_sum: 96.36039608716965
time_elpased: 1.948
start validation test
0.644020618557
0.600977081687
0.860862406092
0.707818581824
0.643639919353
62.213
batch start
#iterations: 140
currently lose_sum: 96.1447177529335
time_elpased: 2.024
batch start
#iterations: 141
currently lose_sum: 95.73546463251114
time_elpased: 1.983
batch start
#iterations: 142
currently lose_sum: 96.26590394973755
time_elpased: 2.041
batch start
#iterations: 143
currently lose_sum: 96.02601158618927
time_elpased: 1.992
batch start
#iterations: 144
currently lose_sum: 96.00467485189438
time_elpased: 2.002
batch start
#iterations: 145
currently lose_sum: 96.02865242958069
time_elpased: 1.997
batch start
#iterations: 146
currently lose_sum: 96.13749557733536
time_elpased: 2.018
batch start
#iterations: 147
currently lose_sum: 96.06156802177429
time_elpased: 2.012
batch start
#iterations: 148
currently lose_sum: 95.96797025203705
time_elpased: 2.009
batch start
#iterations: 149
currently lose_sum: 95.851387321949
time_elpased: 1.956
batch start
#iterations: 150
currently lose_sum: 96.03022181987762
time_elpased: 1.962
batch start
#iterations: 151
currently lose_sum: 96.16860949993134
time_elpased: 1.969
batch start
#iterations: 152
currently lose_sum: 95.83338415622711
time_elpased: 1.984
batch start
#iterations: 153
currently lose_sum: 96.07772380113602
time_elpased: 1.962
batch start
#iterations: 154
currently lose_sum: 95.69281208515167
time_elpased: 1.996
batch start
#iterations: 155
currently lose_sum: 95.85085427761078
time_elpased: 1.971
batch start
#iterations: 156
currently lose_sum: 96.3812535405159
time_elpased: 1.979
batch start
#iterations: 157
currently lose_sum: 95.72931104898453
time_elpased: 1.979
batch start
#iterations: 158
currently lose_sum: 96.21619820594788
time_elpased: 1.982
batch start
#iterations: 159
currently lose_sum: 96.24834579229355
time_elpased: 1.997
start validation test
0.631494845361
0.649510945505
0.57404548729
0.60945096968
0.63159570657
62.072
batch start
#iterations: 160
currently lose_sum: 95.83173727989197
time_elpased: 2.033
batch start
#iterations: 161
currently lose_sum: 95.79364615678787
time_elpased: 2.001
batch start
#iterations: 162
currently lose_sum: 95.88397520780563
time_elpased: 1.992
batch start
#iterations: 163
currently lose_sum: 95.97436410188675
time_elpased: 2.016
batch start
#iterations: 164
currently lose_sum: 95.91789096593857
time_elpased: 1.99
batch start
#iterations: 165
currently lose_sum: 95.6462362408638
time_elpased: 1.962
batch start
#iterations: 166
currently lose_sum: 95.88013160228729
time_elpased: 2.017
batch start
#iterations: 167
currently lose_sum: 95.85573953390121
time_elpased: 1.967
batch start
#iterations: 168
currently lose_sum: 96.14049166440964
time_elpased: 2.019
batch start
#iterations: 169
currently lose_sum: 95.93431490659714
time_elpased: 2.027
batch start
#iterations: 170
currently lose_sum: 95.96637535095215
time_elpased: 1.976
batch start
#iterations: 171
currently lose_sum: 95.70028984546661
time_elpased: 1.968
batch start
#iterations: 172
currently lose_sum: 95.77252000570297
time_elpased: 1.963
batch start
#iterations: 173
currently lose_sum: 95.99473255872726
time_elpased: 2.065
batch start
#iterations: 174
currently lose_sum: 96.04827654361725
time_elpased: 1.99
batch start
#iterations: 175
currently lose_sum: 95.83146566152573
time_elpased: 1.964
batch start
#iterations: 176
currently lose_sum: 95.9356135725975
time_elpased: 2.01
batch start
#iterations: 177
currently lose_sum: 95.78441685438156
time_elpased: 1.968
batch start
#iterations: 178
currently lose_sum: 95.77378517389297
time_elpased: 1.973
batch start
#iterations: 179
currently lose_sum: 95.90693163871765
time_elpased: 1.957
start validation test
0.648144329897
0.623473135731
0.751157764742
0.681385362211
0.647963473923
61.638
batch start
#iterations: 180
currently lose_sum: 95.8473693728447
time_elpased: 2.026
batch start
#iterations: 181
currently lose_sum: 96.20498532056808
time_elpased: 1.997
batch start
#iterations: 182
currently lose_sum: 95.8309240937233
time_elpased: 2.039
batch start
#iterations: 183
currently lose_sum: 95.57912540435791
time_elpased: 2.026
batch start
#iterations: 184
currently lose_sum: 95.86733889579773
time_elpased: 2.022
batch start
#iterations: 185
currently lose_sum: 96.22228872776031
time_elpased: 2.019
batch start
#iterations: 186
currently lose_sum: 95.94800007343292
time_elpased: 2.007
batch start
#iterations: 187
currently lose_sum: 95.9370966553688
time_elpased: 1.99
batch start
#iterations: 188
currently lose_sum: 95.405826151371
time_elpased: 1.99
batch start
#iterations: 189
currently lose_sum: 95.419693171978
time_elpased: 1.977
batch start
#iterations: 190
currently lose_sum: 96.25248289108276
time_elpased: 2.006
batch start
#iterations: 191
currently lose_sum: 95.82361072301865
time_elpased: 1.991
batch start
#iterations: 192
currently lose_sum: 95.92967021465302
time_elpased: 1.994
batch start
#iterations: 193
currently lose_sum: 96.06107580661774
time_elpased: 2.008
batch start
#iterations: 194
currently lose_sum: 95.63706928491592
time_elpased: 2.043
batch start
#iterations: 195
currently lose_sum: 95.46123999357224
time_elpased: 1.988
batch start
#iterations: 196
currently lose_sum: 95.38926720619202
time_elpased: 1.988
batch start
#iterations: 197
currently lose_sum: 95.89864337444305
time_elpased: 1.994
batch start
#iterations: 198
currently lose_sum: 95.77525854110718
time_elpased: 1.999
batch start
#iterations: 199
currently lose_sum: 95.78284692764282
time_elpased: 1.976
start validation test
0.645721649485
0.639138943249
0.672223937429
0.655264081858
0.64567512063
61.422
batch start
#iterations: 200
currently lose_sum: 95.79811149835587
time_elpased: 2.032
batch start
#iterations: 201
currently lose_sum: 96.00126475095749
time_elpased: 2.022
batch start
#iterations: 202
currently lose_sum: 95.34709870815277
time_elpased: 1.983
batch start
#iterations: 203
currently lose_sum: 95.98933708667755
time_elpased: 1.999
batch start
#iterations: 204
currently lose_sum: 95.82571744918823
time_elpased: 1.975
batch start
#iterations: 205
currently lose_sum: 95.76319408416748
time_elpased: 1.992
batch start
#iterations: 206
currently lose_sum: 95.66122776269913
time_elpased: 1.992
batch start
#iterations: 207
currently lose_sum: 95.69357538223267
time_elpased: 1.976
batch start
#iterations: 208
currently lose_sum: 95.14953017234802
time_elpased: 1.985
batch start
#iterations: 209
currently lose_sum: 95.41348868608475
time_elpased: 2.01
batch start
#iterations: 210
currently lose_sum: 95.50350111722946
time_elpased: 2.04
batch start
#iterations: 211
currently lose_sum: 95.58119422197342
time_elpased: 1.965
batch start
#iterations: 212
currently lose_sum: 95.58925312757492
time_elpased: 2.045
batch start
#iterations: 213
currently lose_sum: 95.6192877292633
time_elpased: 2.006
batch start
#iterations: 214
currently lose_sum: 95.76804041862488
time_elpased: 1.998
batch start
#iterations: 215
currently lose_sum: 95.86734187602997
time_elpased: 1.975
batch start
#iterations: 216
currently lose_sum: 95.6320561170578
time_elpased: 1.997
batch start
#iterations: 217
currently lose_sum: 95.59446603059769
time_elpased: 1.987
batch start
#iterations: 218
currently lose_sum: 95.2347960472107
time_elpased: 2.079
batch start
#iterations: 219
currently lose_sum: 95.46163046360016
time_elpased: 2.036
start validation test
0.654639175258
0.630798577994
0.748687866626
0.684705882353
0.654474058274
61.237
batch start
#iterations: 220
currently lose_sum: 95.36387264728546
time_elpased: 2.034
batch start
#iterations: 221
currently lose_sum: 95.85737597942352
time_elpased: 1.994
batch start
#iterations: 222
currently lose_sum: 95.68319457769394
time_elpased: 2.001
batch start
#iterations: 223
currently lose_sum: 95.77723902463913
time_elpased: 2.016
batch start
#iterations: 224
currently lose_sum: 95.29914325475693
time_elpased: 2.004
batch start
#iterations: 225
currently lose_sum: 95.35486620664597
time_elpased: 2.025
batch start
#iterations: 226
currently lose_sum: 95.18584698438644
time_elpased: 2.029
batch start
#iterations: 227
currently lose_sum: 95.60681509971619
time_elpased: 1.972
batch start
#iterations: 228
currently lose_sum: 95.76064455509186
time_elpased: 2.042
batch start
#iterations: 229
currently lose_sum: 95.838603079319
time_elpased: 1.981
batch start
#iterations: 230
currently lose_sum: 95.11952519416809
time_elpased: 2.039
batch start
#iterations: 231
currently lose_sum: 95.16517502069473
time_elpased: 1.973
batch start
#iterations: 232
currently lose_sum: 95.71724581718445
time_elpased: 1.993
batch start
#iterations: 233
currently lose_sum: 95.34207409620285
time_elpased: 2.008
batch start
#iterations: 234
currently lose_sum: 95.49778938293457
time_elpased: 1.971
batch start
#iterations: 235
currently lose_sum: 95.72087424993515
time_elpased: 2.011
batch start
#iterations: 236
currently lose_sum: 95.54058569669724
time_elpased: 2.02
batch start
#iterations: 237
currently lose_sum: 95.48855000734329
time_elpased: 1.982
batch start
#iterations: 238
currently lose_sum: 95.27883207798004
time_elpased: 2.03
batch start
#iterations: 239
currently lose_sum: 95.51564991474152
time_elpased: 2.03
start validation test
0.652164948454
0.638183002886
0.705567562005
0.67018572825
0.652071191929
61.464
batch start
#iterations: 240
currently lose_sum: 95.76283401250839
time_elpased: 1.98
batch start
#iterations: 241
currently lose_sum: 95.65572911500931
time_elpased: 1.952
batch start
#iterations: 242
currently lose_sum: 95.48355215787888
time_elpased: 1.974
batch start
#iterations: 243
currently lose_sum: 95.42382681369781
time_elpased: 1.965
batch start
#iterations: 244
currently lose_sum: 95.620561003685
time_elpased: 1.995
batch start
#iterations: 245
currently lose_sum: 95.62984561920166
time_elpased: 2.026
batch start
#iterations: 246
currently lose_sum: 95.33757346868515
time_elpased: 1.985
batch start
#iterations: 247
currently lose_sum: 95.46139425039291
time_elpased: 1.969
batch start
#iterations: 248
currently lose_sum: 95.71972972154617
time_elpased: 2.007
batch start
#iterations: 249
currently lose_sum: 95.29575777053833
time_elpased: 1.994
batch start
#iterations: 250
currently lose_sum: 95.44716304540634
time_elpased: 1.996
batch start
#iterations: 251
currently lose_sum: 95.21385985612869
time_elpased: 2.019
batch start
#iterations: 252
currently lose_sum: 95.33124423027039
time_elpased: 2.003
batch start
#iterations: 253
currently lose_sum: 95.02608025074005
time_elpased: 1.973
batch start
#iterations: 254
currently lose_sum: 95.6156644821167
time_elpased: 1.984
batch start
#iterations: 255
currently lose_sum: 95.62380158901215
time_elpased: 2.023
batch start
#iterations: 256
currently lose_sum: 95.29557830095291
time_elpased: 2.002
batch start
#iterations: 257
currently lose_sum: 95.81307077407837
time_elpased: 1.994
batch start
#iterations: 258
currently lose_sum: 94.76009947061539
time_elpased: 2.014
batch start
#iterations: 259
currently lose_sum: 95.27702748775482
time_elpased: 2.005
start validation test
0.63706185567
0.650337078652
0.595657095811
0.621797282054
0.637134548112
61.707
batch start
#iterations: 260
currently lose_sum: 95.46903729438782
time_elpased: 2.031
batch start
#iterations: 261
currently lose_sum: 94.80399399995804
time_elpased: 1.983
batch start
#iterations: 262
currently lose_sum: 95.70484131574631
time_elpased: 1.999
batch start
#iterations: 263
currently lose_sum: 95.06155759096146
time_elpased: 1.974
batch start
#iterations: 264
currently lose_sum: 95.64823460578918
time_elpased: 2.021
batch start
#iterations: 265
currently lose_sum: 95.50077164173126
time_elpased: 2.007
batch start
#iterations: 266
currently lose_sum: 95.2422958612442
time_elpased: 2.015
batch start
#iterations: 267
currently lose_sum: 95.56862938404083
time_elpased: 1.966
batch start
#iterations: 268
currently lose_sum: 95.52945786714554
time_elpased: 2.026
batch start
#iterations: 269
currently lose_sum: 95.50378209352493
time_elpased: 1.987
batch start
#iterations: 270
currently lose_sum: 95.14975529909134
time_elpased: 1.979
batch start
#iterations: 271
currently lose_sum: 95.37203514575958
time_elpased: 1.987
batch start
#iterations: 272
currently lose_sum: 95.7054500579834
time_elpased: 2.025
batch start
#iterations: 273
currently lose_sum: 94.96058410406113
time_elpased: 1.986
batch start
#iterations: 274
currently lose_sum: 95.67393290996552
time_elpased: 2.05
batch start
#iterations: 275
currently lose_sum: 95.20272445678711
time_elpased: 2.03
batch start
#iterations: 276
currently lose_sum: 95.37831377983093
time_elpased: 2.021
batch start
#iterations: 277
currently lose_sum: 95.29868537187576
time_elpased: 2.044
batch start
#iterations: 278
currently lose_sum: 95.31886893510818
time_elpased: 2.025
batch start
#iterations: 279
currently lose_sum: 95.09740203619003
time_elpased: 2.025
start validation test
0.647010309278
0.605253503559
0.848924565195
0.706673520089
0.646655817659
61.426
batch start
#iterations: 280
currently lose_sum: 95.02125138044357
time_elpased: 2.064
batch start
#iterations: 281
currently lose_sum: 95.36872065067291
time_elpased: 2.014
batch start
#iterations: 282
currently lose_sum: 95.15948867797852
time_elpased: 2.009
batch start
#iterations: 283
currently lose_sum: 95.20257759094238
time_elpased: 1.994
batch start
#iterations: 284
currently lose_sum: 94.95561403036118
time_elpased: 2.022
batch start
#iterations: 285
currently lose_sum: 95.31331557035446
time_elpased: 2.026
batch start
#iterations: 286
currently lose_sum: 95.5178012251854
time_elpased: 2.007
batch start
#iterations: 287
currently lose_sum: 95.0815219283104
time_elpased: 2.019
batch start
#iterations: 288
currently lose_sum: 95.01131385564804
time_elpased: 1.997
batch start
#iterations: 289
currently lose_sum: 95.23348665237427
time_elpased: 2.009
batch start
#iterations: 290
currently lose_sum: 95.5164812207222
time_elpased: 2.055
batch start
#iterations: 291
currently lose_sum: 94.78234046697617
time_elpased: 2.016
batch start
#iterations: 292
currently lose_sum: 95.09792637825012
time_elpased: 2.027
batch start
#iterations: 293
currently lose_sum: 95.46737891435623
time_elpased: 1.983
batch start
#iterations: 294
currently lose_sum: 94.88615477085114
time_elpased: 2.043
batch start
#iterations: 295
currently lose_sum: 95.14143186807632
time_elpased: 1.967
batch start
#iterations: 296
currently lose_sum: 95.0307223200798
time_elpased: 1.991
batch start
#iterations: 297
currently lose_sum: 95.78785938024521
time_elpased: 1.979
batch start
#iterations: 298
currently lose_sum: 94.85036581754684
time_elpased: 1.982
batch start
#iterations: 299
currently lose_sum: 95.67559689283371
time_elpased: 1.978
start validation test
0.64881443299
0.61990090834
0.77256354842
0.687863655106
0.648597172331
61.137
batch start
#iterations: 300
currently lose_sum: 95.17219650745392
time_elpased: 2.036
batch start
#iterations: 301
currently lose_sum: 94.9614000916481
time_elpased: 2.029
batch start
#iterations: 302
currently lose_sum: 95.26172256469727
time_elpased: 2.027
batch start
#iterations: 303
currently lose_sum: 95.5031972527504
time_elpased: 2.026
batch start
#iterations: 304
currently lose_sum: 95.42938101291656
time_elpased: 2.105
batch start
#iterations: 305
currently lose_sum: 95.43639069795609
time_elpased: 2.025
batch start
#iterations: 306
currently lose_sum: 95.10984921455383
time_elpased: 2.082
batch start
#iterations: 307
currently lose_sum: 95.25679326057434
time_elpased: 2.044
batch start
#iterations: 308
currently lose_sum: 95.18974548578262
time_elpased: 2.15
batch start
#iterations: 309
currently lose_sum: 95.29042118787766
time_elpased: 2.012
batch start
#iterations: 310
currently lose_sum: 95.42036533355713
time_elpased: 2.047
batch start
#iterations: 311
currently lose_sum: 94.92264318466187
time_elpased: 2.031
batch start
#iterations: 312
currently lose_sum: 94.79201966524124
time_elpased: 2.002
batch start
#iterations: 313
currently lose_sum: 95.16010963916779
time_elpased: 1.984
batch start
#iterations: 314
currently lose_sum: 95.36176753044128
time_elpased: 2.047
batch start
#iterations: 315
currently lose_sum: 95.04648852348328
time_elpased: 2.008
batch start
#iterations: 316
currently lose_sum: 95.24390816688538
time_elpased: 2.03
batch start
#iterations: 317
currently lose_sum: 94.93334656953812
time_elpased: 1.977
batch start
#iterations: 318
currently lose_sum: 94.91291803121567
time_elpased: 2.036
batch start
#iterations: 319
currently lose_sum: 95.33756828308105
time_elpased: 1.995
start validation test
0.646494845361
0.628102876602
0.721313162499
0.671488790956
0.646363490265
61.262
batch start
#iterations: 320
currently lose_sum: 94.65107488632202
time_elpased: 2.065
batch start
#iterations: 321
currently lose_sum: 94.9893012046814
time_elpased: 2.02
batch start
#iterations: 322
currently lose_sum: 95.59668511152267
time_elpased: 2.012
batch start
#iterations: 323
currently lose_sum: 95.30470257997513
time_elpased: 2.026
batch start
#iterations: 324
currently lose_sum: 95.08244848251343
time_elpased: 2.008
batch start
#iterations: 325
currently lose_sum: 94.85218924283981
time_elpased: 1.969
batch start
#iterations: 326
currently lose_sum: 95.2970917224884
time_elpased: 2.011
batch start
#iterations: 327
currently lose_sum: 95.01034945249557
time_elpased: 1.994
batch start
#iterations: 328
currently lose_sum: 95.51426047086716
time_elpased: 2.003
batch start
#iterations: 329
currently lose_sum: 95.09734523296356
time_elpased: 1.966
batch start
#iterations: 330
currently lose_sum: 94.79218143224716
time_elpased: 2.059
batch start
#iterations: 331
currently lose_sum: 95.32750338315964
time_elpased: 1.978
batch start
#iterations: 332
currently lose_sum: 95.28845191001892
time_elpased: 2.035
batch start
#iterations: 333
currently lose_sum: 94.83132761716843
time_elpased: 2.077
batch start
#iterations: 334
currently lose_sum: 95.58952277898788
time_elpased: 2.035
batch start
#iterations: 335
currently lose_sum: 94.85829645395279
time_elpased: 2.042
batch start
#iterations: 336
currently lose_sum: 95.13079118728638
time_elpased: 1.993
batch start
#iterations: 337
currently lose_sum: 94.7924839258194
time_elpased: 2.005
batch start
#iterations: 338
currently lose_sum: 95.50088143348694
time_elpased: 2.023
batch start
#iterations: 339
currently lose_sum: 95.44654029607773
time_elpased: 1.989
start validation test
0.642628865979
0.632520944402
0.683750128641
0.657138618268
0.64255667126
61.637
batch start
#iterations: 340
currently lose_sum: 95.1648621559143
time_elpased: 2.063
batch start
#iterations: 341
currently lose_sum: 95.440724670887
time_elpased: 2.019
batch start
#iterations: 342
currently lose_sum: 94.94720447063446
time_elpased: 2.018
batch start
#iterations: 343
currently lose_sum: 94.88363498449326
time_elpased: 2.055
batch start
#iterations: 344
currently lose_sum: 94.87121933698654
time_elpased: 2.032
batch start
#iterations: 345
currently lose_sum: 95.35811150074005
time_elpased: 1.968
batch start
#iterations: 346
currently lose_sum: 95.12393355369568
time_elpased: 2.046
batch start
#iterations: 347
currently lose_sum: 94.93165498971939
time_elpased: 1.977
batch start
#iterations: 348
currently lose_sum: 95.40852600336075
time_elpased: 2.029
batch start
#iterations: 349
currently lose_sum: 94.62363749742508
time_elpased: 1.994
batch start
#iterations: 350
currently lose_sum: 95.03596836328506
time_elpased: 2.041
batch start
#iterations: 351
currently lose_sum: 94.77334100008011
time_elpased: 1.996
batch start
#iterations: 352
currently lose_sum: 94.94669824838638
time_elpased: 2.021
batch start
#iterations: 353
currently lose_sum: 95.13155043125153
time_elpased: 2.027
batch start
#iterations: 354
currently lose_sum: 95.04596769809723
time_elpased: 1.987
batch start
#iterations: 355
currently lose_sum: 94.6797850728035
time_elpased: 1.99
batch start
#iterations: 356
currently lose_sum: 95.19539839029312
time_elpased: 1.995
batch start
#iterations: 357
currently lose_sum: 94.99268102645874
time_elpased: 1.969
batch start
#iterations: 358
currently lose_sum: 94.80062127113342
time_elpased: 2.015
batch start
#iterations: 359
currently lose_sum: 95.02607971429825
time_elpased: 1.98
start validation test
0.650412371134
0.623891937526
0.760522795101
0.685465170207
0.650219055301
61.023
batch start
#iterations: 360
currently lose_sum: 95.37478005886078
time_elpased: 2.016
batch start
#iterations: 361
currently lose_sum: 95.46718239784241
time_elpased: 2.024
batch start
#iterations: 362
currently lose_sum: 94.90208995342255
time_elpased: 2.031
batch start
#iterations: 363
currently lose_sum: 95.30627506971359
time_elpased: 2.022
batch start
#iterations: 364
currently lose_sum: 94.92960119247437
time_elpased: 1.976
batch start
#iterations: 365
currently lose_sum: 95.13366919755936
time_elpased: 2.002
batch start
#iterations: 366
currently lose_sum: 95.028315782547
time_elpased: 2.047
batch start
#iterations: 367
currently lose_sum: 94.91502904891968
time_elpased: 1.996
batch start
#iterations: 368
currently lose_sum: 95.14521872997284
time_elpased: 2.046
batch start
#iterations: 369
currently lose_sum: 95.31485724449158
time_elpased: 2.025
batch start
#iterations: 370
currently lose_sum: 94.30505418777466
time_elpased: 2.037
batch start
#iterations: 371
currently lose_sum: 95.10319203138351
time_elpased: 1.967
batch start
#iterations: 372
currently lose_sum: 94.9249963760376
time_elpased: 2.037
batch start
#iterations: 373
currently lose_sum: 95.30508387088776
time_elpased: 1.992
batch start
#iterations: 374
currently lose_sum: 94.993632376194
time_elpased: 2.009
batch start
#iterations: 375
currently lose_sum: 94.9397736787796
time_elpased: 1.973
batch start
#iterations: 376
currently lose_sum: 95.08348572254181
time_elpased: 2.02
batch start
#iterations: 377
currently lose_sum: 95.42791789770126
time_elpased: 1.973
batch start
#iterations: 378
currently lose_sum: 94.37349051237106
time_elpased: 1.97
batch start
#iterations: 379
currently lose_sum: 94.90309798717499
time_elpased: 1.984
start validation test
0.656804123711
0.62817397134
0.771431511783
0.69247113164
0.656602877652
60.904
batch start
#iterations: 380
currently lose_sum: 94.70615690946579
time_elpased: 2.049
batch start
#iterations: 381
currently lose_sum: 95.25872850418091
time_elpased: 1.958
batch start
#iterations: 382
currently lose_sum: 94.99414145946503
time_elpased: 2.021
batch start
#iterations: 383
currently lose_sum: 95.00956171751022
time_elpased: 2.012
batch start
#iterations: 384
currently lose_sum: 94.88729745149612
time_elpased: 2.0
batch start
#iterations: 385
currently lose_sum: 95.0904386639595
time_elpased: 2.01
batch start
#iterations: 386
currently lose_sum: 94.90456408262253
time_elpased: 2.039
batch start
#iterations: 387
currently lose_sum: 94.82842564582825
time_elpased: 2.003
batch start
#iterations: 388
currently lose_sum: 94.98714590072632
time_elpased: 2.02
batch start
#iterations: 389
currently lose_sum: 95.1842890381813
time_elpased: 1.993
batch start
#iterations: 390
currently lose_sum: 95.09490329027176
time_elpased: 2.018
batch start
#iterations: 391
currently lose_sum: 94.883032143116
time_elpased: 2.032
batch start
#iterations: 392
currently lose_sum: 94.52288162708282
time_elpased: 2.05
batch start
#iterations: 393
currently lose_sum: 94.79776298999786
time_elpased: 1.991
batch start
#iterations: 394
currently lose_sum: 95.17098486423492
time_elpased: 2.029
batch start
#iterations: 395
currently lose_sum: 94.87611222267151
time_elpased: 1.991
batch start
#iterations: 396
currently lose_sum: 94.78943055868149
time_elpased: 1.996
batch start
#iterations: 397
currently lose_sum: 95.07529205083847
time_elpased: 2.007
batch start
#iterations: 398
currently lose_sum: 95.06942355632782
time_elpased: 2.084
batch start
#iterations: 399
currently lose_sum: 95.03314876556396
time_elpased: 2.028
start validation test
0.653041237113
0.625504371217
0.765771328599
0.688567066118
0.65284332205
61.111
acc: 0.651
pre: 0.623
rec: 0.768
F1: 0.688
auc: 0.651
