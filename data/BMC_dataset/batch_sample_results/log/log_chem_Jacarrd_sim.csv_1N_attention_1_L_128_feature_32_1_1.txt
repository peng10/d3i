start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.44681441783905
time_elpased: 1.873
batch start
#iterations: 1
currently lose_sum: 100.44965863227844
time_elpased: 1.788
batch start
#iterations: 2
currently lose_sum: 100.36410790681839
time_elpased: 1.821
batch start
#iterations: 3
currently lose_sum: 100.31665432453156
time_elpased: 1.789
batch start
#iterations: 4
currently lose_sum: 100.36301857233047
time_elpased: 1.79
batch start
#iterations: 5
currently lose_sum: 100.2460345029831
time_elpased: 1.795
batch start
#iterations: 6
currently lose_sum: 100.15924972295761
time_elpased: 1.799
batch start
#iterations: 7
currently lose_sum: 100.17265629768372
time_elpased: 1.809
batch start
#iterations: 8
currently lose_sum: 100.11263155937195
time_elpased: 1.802
batch start
#iterations: 9
currently lose_sum: 100.1204081773758
time_elpased: 1.8
batch start
#iterations: 10
currently lose_sum: 99.99015593528748
time_elpased: 1.814
batch start
#iterations: 11
currently lose_sum: 99.83209657669067
time_elpased: 1.829
batch start
#iterations: 12
currently lose_sum: 99.785659968853
time_elpased: 1.851
batch start
#iterations: 13
currently lose_sum: 99.66720497608185
time_elpased: 1.873
batch start
#iterations: 14
currently lose_sum: 99.6218575835228
time_elpased: 1.881
batch start
#iterations: 15
currently lose_sum: 99.41867434978485
time_elpased: 1.863
batch start
#iterations: 16
currently lose_sum: 99.32066482305527
time_elpased: 1.802
batch start
#iterations: 17
currently lose_sum: 99.22936391830444
time_elpased: 1.823
batch start
#iterations: 18
currently lose_sum: 99.01221913099289
time_elpased: 1.81
batch start
#iterations: 19
currently lose_sum: 99.0745941400528
time_elpased: 1.836
start validation test
0.61881443299
0.637689753321
0.553360090563
0.592539533859
0.618929348183
64.767
batch start
#iterations: 20
currently lose_sum: 98.75432604551315
time_elpased: 1.828
batch start
#iterations: 21
currently lose_sum: 98.65228682756424
time_elpased: 1.858
batch start
#iterations: 22
currently lose_sum: 98.70059877634048
time_elpased: 1.829
batch start
#iterations: 23
currently lose_sum: 98.50601154565811
time_elpased: 1.818
batch start
#iterations: 24
currently lose_sum: 98.63558667898178
time_elpased: 1.802
batch start
#iterations: 25
currently lose_sum: 98.23640394210815
time_elpased: 1.819
batch start
#iterations: 26
currently lose_sum: 98.2999918460846
time_elpased: 1.806
batch start
#iterations: 27
currently lose_sum: 98.11337572336197
time_elpased: 1.817
batch start
#iterations: 28
currently lose_sum: 98.17537891864777
time_elpased: 1.788
batch start
#iterations: 29
currently lose_sum: 98.20822823047638
time_elpased: 1.817
batch start
#iterations: 30
currently lose_sum: 98.28341728448868
time_elpased: 1.801
batch start
#iterations: 31
currently lose_sum: 97.84313386678696
time_elpased: 1.829
batch start
#iterations: 32
currently lose_sum: 97.85570573806763
time_elpased: 1.841
batch start
#iterations: 33
currently lose_sum: 97.79838401079178
time_elpased: 1.842
batch start
#iterations: 34
currently lose_sum: 97.83929073810577
time_elpased: 1.872
batch start
#iterations: 35
currently lose_sum: 97.77733188867569
time_elpased: 1.807
batch start
#iterations: 36
currently lose_sum: 97.77645713090897
time_elpased: 1.818
batch start
#iterations: 37
currently lose_sum: 97.95837479829788
time_elpased: 1.803
batch start
#iterations: 38
currently lose_sum: 97.445896089077
time_elpased: 1.846
batch start
#iterations: 39
currently lose_sum: 97.30703806877136
time_elpased: 1.808
start validation test
0.60881443299
0.659951894167
0.451785530514
0.536379742196
0.609090121448
63.752
batch start
#iterations: 40
currently lose_sum: 97.58283722400665
time_elpased: 1.811
batch start
#iterations: 41
currently lose_sum: 97.11826652288437
time_elpased: 1.818
batch start
#iterations: 42
currently lose_sum: 97.50813728570938
time_elpased: 1.818
batch start
#iterations: 43
currently lose_sum: 97.22044879198074
time_elpased: 1.842
batch start
#iterations: 44
currently lose_sum: 97.34756779670715
time_elpased: 1.804
batch start
#iterations: 45
currently lose_sum: 97.6192175745964
time_elpased: 1.812
batch start
#iterations: 46
currently lose_sum: 97.01449769735336
time_elpased: 1.803
batch start
#iterations: 47
currently lose_sum: 97.13576132059097
time_elpased: 1.824
batch start
#iterations: 48
currently lose_sum: 97.19326454401016
time_elpased: 1.806
batch start
#iterations: 49
currently lose_sum: 97.18230128288269
time_elpased: 1.826
batch start
#iterations: 50
currently lose_sum: 97.41469246149063
time_elpased: 1.807
batch start
#iterations: 51
currently lose_sum: 97.16870945692062
time_elpased: 1.789
batch start
#iterations: 52
currently lose_sum: 97.18276977539062
time_elpased: 1.841
batch start
#iterations: 53
currently lose_sum: 96.85688215494156
time_elpased: 1.808
batch start
#iterations: 54
currently lose_sum: 97.32624530792236
time_elpased: 1.805
batch start
#iterations: 55
currently lose_sum: 97.09886467456818
time_elpased: 1.803
batch start
#iterations: 56
currently lose_sum: 96.76577633619308
time_elpased: 1.858
batch start
#iterations: 57
currently lose_sum: 96.77716886997223
time_elpased: 1.8
batch start
#iterations: 58
currently lose_sum: 97.21665239334106
time_elpased: 1.856
batch start
#iterations: 59
currently lose_sum: 96.96438413858414
time_elpased: 1.85
start validation test
0.609381443299
0.650995340957
0.474529175672
0.548928571429
0.609618197254
63.416
batch start
#iterations: 60
currently lose_sum: 97.10345834493637
time_elpased: 1.925
batch start
#iterations: 61
currently lose_sum: 97.12587904930115
time_elpased: 1.791
batch start
#iterations: 62
currently lose_sum: 97.01453721523285
time_elpased: 1.843
batch start
#iterations: 63
currently lose_sum: 97.12330186367035
time_elpased: 1.808
batch start
#iterations: 64
currently lose_sum: 96.9721794128418
time_elpased: 1.795
batch start
#iterations: 65
currently lose_sum: 96.41433948278427
time_elpased: 1.85
batch start
#iterations: 66
currently lose_sum: 96.78129476308823
time_elpased: 1.791
batch start
#iterations: 67
currently lose_sum: 96.91650092601776
time_elpased: 1.793
batch start
#iterations: 68
currently lose_sum: 96.82666450738907
time_elpased: 1.809
batch start
#iterations: 69
currently lose_sum: 96.92170482873917
time_elpased: 1.83
batch start
#iterations: 70
currently lose_sum: 96.82373386621475
time_elpased: 1.831
batch start
#iterations: 71
currently lose_sum: 96.46761286258698
time_elpased: 1.81
batch start
#iterations: 72
currently lose_sum: 96.81859171390533
time_elpased: 1.806
batch start
#iterations: 73
currently lose_sum: 96.6891063451767
time_elpased: 1.797
batch start
#iterations: 74
currently lose_sum: 96.84582817554474
time_elpased: 1.804
batch start
#iterations: 75
currently lose_sum: 96.69437003135681
time_elpased: 1.81
batch start
#iterations: 76
currently lose_sum: 96.13297408819199
time_elpased: 1.798
batch start
#iterations: 77
currently lose_sum: 96.51588469743729
time_elpased: 1.831
batch start
#iterations: 78
currently lose_sum: 96.54430162906647
time_elpased: 1.865
batch start
#iterations: 79
currently lose_sum: 96.71465188264847
time_elpased: 1.783
start validation test
0.652474226804
0.61881939452
0.797262529587
0.696797985249
0.652220028607
62.309
batch start
#iterations: 80
currently lose_sum: 96.85692125558853
time_elpased: 1.825
batch start
#iterations: 81
currently lose_sum: 96.53712600469589
time_elpased: 1.814
batch start
#iterations: 82
currently lose_sum: 96.80206739902496
time_elpased: 1.816
batch start
#iterations: 83
currently lose_sum: 96.47563928365707
time_elpased: 1.819
batch start
#iterations: 84
currently lose_sum: 96.46596419811249
time_elpased: 1.817
batch start
#iterations: 85
currently lose_sum: 96.90759640932083
time_elpased: 1.795
batch start
#iterations: 86
currently lose_sum: 96.67159563302994
time_elpased: 1.809
batch start
#iterations: 87
currently lose_sum: 96.35638892650604
time_elpased: 1.826
batch start
#iterations: 88
currently lose_sum: 96.43268716335297
time_elpased: 1.798
batch start
#iterations: 89
currently lose_sum: 96.16103494167328
time_elpased: 1.791
batch start
#iterations: 90
currently lose_sum: 96.66460508108139
time_elpased: 1.786
batch start
#iterations: 91
currently lose_sum: 96.64671057462692
time_elpased: 1.797
batch start
#iterations: 92
currently lose_sum: 96.48930501937866
time_elpased: 1.802
batch start
#iterations: 93
currently lose_sum: 96.33387291431427
time_elpased: 1.867
batch start
#iterations: 94
currently lose_sum: 96.17184233665466
time_elpased: 1.816
batch start
#iterations: 95
currently lose_sum: 96.26312732696533
time_elpased: 1.863
batch start
#iterations: 96
currently lose_sum: 96.50463098287582
time_elpased: 1.799
batch start
#iterations: 97
currently lose_sum: 96.73483711481094
time_elpased: 1.804
batch start
#iterations: 98
currently lose_sum: 96.35332262516022
time_elpased: 1.806
batch start
#iterations: 99
currently lose_sum: 96.14133554697037
time_elpased: 1.788
start validation test
0.653969072165
0.622812755519
0.783883914789
0.694126759922
0.653740986621
61.788
batch start
#iterations: 100
currently lose_sum: 96.20820850133896
time_elpased: 1.816
batch start
#iterations: 101
currently lose_sum: 96.36506658792496
time_elpased: 1.808
batch start
#iterations: 102
currently lose_sum: 95.9365690946579
time_elpased: 1.807
batch start
#iterations: 103
currently lose_sum: 96.18781632184982
time_elpased: 1.836
batch start
#iterations: 104
currently lose_sum: 96.45558297634125
time_elpased: 1.812
batch start
#iterations: 105
currently lose_sum: 96.12033051252365
time_elpased: 1.802
batch start
#iterations: 106
currently lose_sum: 96.37182033061981
time_elpased: 1.795
batch start
#iterations: 107
currently lose_sum: 96.31771606206894
time_elpased: 1.798
batch start
#iterations: 108
currently lose_sum: 96.1456748843193
time_elpased: 1.792
batch start
#iterations: 109
currently lose_sum: 96.3299845457077
time_elpased: 1.797
batch start
#iterations: 110
currently lose_sum: 95.98896181583405
time_elpased: 1.814
batch start
#iterations: 111
currently lose_sum: 96.26652216911316
time_elpased: 1.792
batch start
#iterations: 112
currently lose_sum: 96.22715002298355
time_elpased: 1.855
batch start
#iterations: 113
currently lose_sum: 95.95100110769272
time_elpased: 1.841
batch start
#iterations: 114
currently lose_sum: 96.12918871641159
time_elpased: 1.845
batch start
#iterations: 115
currently lose_sum: 95.97080290317535
time_elpased: 1.811
batch start
#iterations: 116
currently lose_sum: 96.49471759796143
time_elpased: 1.829
batch start
#iterations: 117
currently lose_sum: 96.3048785328865
time_elpased: 1.829
batch start
#iterations: 118
currently lose_sum: 96.34263473749161
time_elpased: 1.81
batch start
#iterations: 119
currently lose_sum: 96.04737216234207
time_elpased: 1.847
start validation test
0.636443298969
0.647704590818
0.601111454153
0.623538831065
0.636505329472
62.148
batch start
#iterations: 120
currently lose_sum: 95.88405632972717
time_elpased: 1.827
batch start
#iterations: 121
currently lose_sum: 96.3960970044136
time_elpased: 1.849
batch start
#iterations: 122
currently lose_sum: 96.24376404285431
time_elpased: 1.836
batch start
#iterations: 123
currently lose_sum: 95.91847038269043
time_elpased: 1.837
batch start
#iterations: 124
currently lose_sum: 95.89658051729202
time_elpased: 1.824
batch start
#iterations: 125
currently lose_sum: 96.16725385189056
time_elpased: 1.826
batch start
#iterations: 126
currently lose_sum: 96.46743804216385
time_elpased: 1.826
batch start
#iterations: 127
currently lose_sum: 96.22246611118317
time_elpased: 1.79
batch start
#iterations: 128
currently lose_sum: 96.34719222784042
time_elpased: 1.834
batch start
#iterations: 129
currently lose_sum: 95.94040024280548
time_elpased: 1.922
batch start
#iterations: 130
currently lose_sum: 96.06649166345596
time_elpased: 1.912
batch start
#iterations: 131
currently lose_sum: 95.9042791724205
time_elpased: 1.861
batch start
#iterations: 132
currently lose_sum: 96.23330825567245
time_elpased: 1.833
batch start
#iterations: 133
currently lose_sum: 95.89143776893616
time_elpased: 1.845
batch start
#iterations: 134
currently lose_sum: 95.58005541563034
time_elpased: 1.829
batch start
#iterations: 135
currently lose_sum: 96.40327399969101
time_elpased: 1.79
batch start
#iterations: 136
currently lose_sum: 95.3700339794159
time_elpased: 1.92
batch start
#iterations: 137
currently lose_sum: 96.05077910423279
time_elpased: 1.833
batch start
#iterations: 138
currently lose_sum: 95.50038027763367
time_elpased: 1.825
batch start
#iterations: 139
currently lose_sum: 96.16641503572464
time_elpased: 1.836
start validation test
0.653350515464
0.628854435831
0.751363589585
0.684672012004
0.653178438395
61.562
batch start
#iterations: 140
currently lose_sum: 96.0574539899826
time_elpased: 1.871
batch start
#iterations: 141
currently lose_sum: 96.28903257846832
time_elpased: 1.864
batch start
#iterations: 142
currently lose_sum: 95.94659781455994
time_elpased: 1.822
batch start
#iterations: 143
currently lose_sum: 95.97189581394196
time_elpased: 1.805
batch start
#iterations: 144
currently lose_sum: 95.91626787185669
time_elpased: 1.855
batch start
#iterations: 145
currently lose_sum: 96.08493673801422
time_elpased: 1.804
batch start
#iterations: 146
currently lose_sum: 95.56259870529175
time_elpased: 1.814
batch start
#iterations: 147
currently lose_sum: 95.67966204881668
time_elpased: 1.826
batch start
#iterations: 148
currently lose_sum: 96.13599866628647
time_elpased: 1.84
batch start
#iterations: 149
currently lose_sum: 95.89908415079117
time_elpased: 1.816
batch start
#iterations: 150
currently lose_sum: 95.76996010541916
time_elpased: 1.842
batch start
#iterations: 151
currently lose_sum: 95.8826699256897
time_elpased: 1.831
batch start
#iterations: 152
currently lose_sum: 95.95464539527893
time_elpased: 1.83
batch start
#iterations: 153
currently lose_sum: 95.88323110342026
time_elpased: 1.791
batch start
#iterations: 154
currently lose_sum: 96.05142599344254
time_elpased: 1.93
batch start
#iterations: 155
currently lose_sum: 96.08785140514374
time_elpased: 1.855
batch start
#iterations: 156
currently lose_sum: 95.57224798202515
time_elpased: 1.857
batch start
#iterations: 157
currently lose_sum: 96.10442811250687
time_elpased: 1.851
batch start
#iterations: 158
currently lose_sum: 96.03173738718033
time_elpased: 1.861
batch start
#iterations: 159
currently lose_sum: 96.42545795440674
time_elpased: 1.822
start validation test
0.656082474227
0.62581604826
0.779355768241
0.694197451645
0.655866048946
61.666
batch start
#iterations: 160
currently lose_sum: 95.66881954669952
time_elpased: 1.809
batch start
#iterations: 161
currently lose_sum: 95.83040285110474
time_elpased: 1.833
batch start
#iterations: 162
currently lose_sum: 95.68457520008087
time_elpased: 1.814
batch start
#iterations: 163
currently lose_sum: 95.72058010101318
time_elpased: 1.865
batch start
#iterations: 164
currently lose_sum: 95.71126013994217
time_elpased: 1.802
batch start
#iterations: 165
currently lose_sum: 95.68665254116058
time_elpased: 1.829
batch start
#iterations: 166
currently lose_sum: 95.52095550298691
time_elpased: 1.786
batch start
#iterations: 167
currently lose_sum: 95.82924836874008
time_elpased: 1.817
batch start
#iterations: 168
currently lose_sum: 95.33721244335175
time_elpased: 1.85
batch start
#iterations: 169
currently lose_sum: 95.70237523317337
time_elpased: 1.906
batch start
#iterations: 170
currently lose_sum: 95.89210522174835
time_elpased: 1.866
batch start
#iterations: 171
currently lose_sum: 95.82972651720047
time_elpased: 1.904
batch start
#iterations: 172
currently lose_sum: 96.08008235692978
time_elpased: 1.819
batch start
#iterations: 173
currently lose_sum: 95.79882025718689
time_elpased: 1.97
batch start
#iterations: 174
currently lose_sum: 95.6088690161705
time_elpased: 1.803
batch start
#iterations: 175
currently lose_sum: 95.53489309549332
time_elpased: 1.843
batch start
#iterations: 176
currently lose_sum: 95.426229596138
time_elpased: 1.802
batch start
#iterations: 177
currently lose_sum: 95.14311110973358
time_elpased: 1.837
batch start
#iterations: 178
currently lose_sum: 95.3914304971695
time_elpased: 1.803
batch start
#iterations: 179
currently lose_sum: 95.96108758449554
time_elpased: 1.835
start validation test
0.653762886598
0.618352532744
0.806524647525
0.70001339824
0.653494689765
61.653
batch start
#iterations: 180
currently lose_sum: 95.71226835250854
time_elpased: 1.839
batch start
#iterations: 181
currently lose_sum: 95.98430401086807
time_elpased: 1.84
batch start
#iterations: 182
currently lose_sum: 95.39247763156891
time_elpased: 1.841
batch start
#iterations: 183
currently lose_sum: 95.29245191812515
time_elpased: 1.791
batch start
#iterations: 184
currently lose_sum: 95.34919703006744
time_elpased: 1.783
batch start
#iterations: 185
currently lose_sum: 95.64622741937637
time_elpased: 1.829
batch start
#iterations: 186
currently lose_sum: 95.41320699453354
time_elpased: 1.774
batch start
#iterations: 187
currently lose_sum: 95.31015342473984
time_elpased: 1.798
batch start
#iterations: 188
currently lose_sum: 95.60686218738556
time_elpased: 1.772
batch start
#iterations: 189
currently lose_sum: 95.42191034555435
time_elpased: 1.808
batch start
#iterations: 190
currently lose_sum: 95.9878933429718
time_elpased: 1.783
batch start
#iterations: 191
currently lose_sum: 95.50725835561752
time_elpased: 1.821
batch start
#iterations: 192
currently lose_sum: 95.25690108537674
time_elpased: 1.798
batch start
#iterations: 193
currently lose_sum: 95.55480343103409
time_elpased: 1.841
batch start
#iterations: 194
currently lose_sum: 95.48799562454224
time_elpased: 1.825
batch start
#iterations: 195
currently lose_sum: 95.75844931602478
time_elpased: 1.815
batch start
#iterations: 196
currently lose_sum: 95.89447820186615
time_elpased: 1.821
batch start
#iterations: 197
currently lose_sum: 95.66365563869476
time_elpased: 1.824
batch start
#iterations: 198
currently lose_sum: 95.79520922899246
time_elpased: 1.801
batch start
#iterations: 199
currently lose_sum: 95.42322981357574
time_elpased: 1.806
start validation test
0.654896907216
0.64333143616
0.697952042812
0.669529591786
0.654821317285
61.712
batch start
#iterations: 200
currently lose_sum: 95.26354479789734
time_elpased: 1.804
batch start
#iterations: 201
currently lose_sum: 94.94242405891418
time_elpased: 1.787
batch start
#iterations: 202
currently lose_sum: 95.34891349077225
time_elpased: 1.795
batch start
#iterations: 203
currently lose_sum: 95.66767936944962
time_elpased: 1.817
batch start
#iterations: 204
currently lose_sum: 95.44606250524521
time_elpased: 1.811
batch start
#iterations: 205
currently lose_sum: 95.4700146317482
time_elpased: 1.848
batch start
#iterations: 206
currently lose_sum: 95.42771708965302
time_elpased: 1.834
batch start
#iterations: 207
currently lose_sum: 95.29345256090164
time_elpased: 1.827
batch start
#iterations: 208
currently lose_sum: 95.09891641139984
time_elpased: 1.787
batch start
#iterations: 209
currently lose_sum: 95.0090999007225
time_elpased: 1.863
batch start
#iterations: 210
currently lose_sum: 95.47529709339142
time_elpased: 1.802
batch start
#iterations: 211
currently lose_sum: 95.47571325302124
time_elpased: 1.822
batch start
#iterations: 212
currently lose_sum: 95.30626499652863
time_elpased: 1.782
batch start
#iterations: 213
currently lose_sum: 95.23904156684875
time_elpased: 1.836
batch start
#iterations: 214
currently lose_sum: 95.47668659687042
time_elpased: 1.812
batch start
#iterations: 215
currently lose_sum: 95.43759882450104
time_elpased: 1.83
batch start
#iterations: 216
currently lose_sum: 94.84497028589249
time_elpased: 1.809
batch start
#iterations: 217
currently lose_sum: 95.35190516710281
time_elpased: 1.864
batch start
#iterations: 218
currently lose_sum: 95.36184185743332
time_elpased: 1.807
batch start
#iterations: 219
currently lose_sum: 95.65207499265671
time_elpased: 1.818
start validation test
0.660670103093
0.620501384189
0.83040032932
0.710268033977
0.660372115502
61.048
batch start
#iterations: 220
currently lose_sum: 95.61861008405685
time_elpased: 1.862
batch start
#iterations: 221
currently lose_sum: 94.95084351301193
time_elpased: 1.82
batch start
#iterations: 222
currently lose_sum: 95.30594503879547
time_elpased: 1.81
batch start
#iterations: 223
currently lose_sum: 95.08714509010315
time_elpased: 1.804
batch start
#iterations: 224
currently lose_sum: 95.14544582366943
time_elpased: 1.784
batch start
#iterations: 225
currently lose_sum: 95.2703675031662
time_elpased: 1.803
batch start
#iterations: 226
currently lose_sum: 95.09472012519836
time_elpased: 1.8
batch start
#iterations: 227
currently lose_sum: 95.51017355918884
time_elpased: 1.847
batch start
#iterations: 228
currently lose_sum: 95.1808397769928
time_elpased: 1.794
batch start
#iterations: 229
currently lose_sum: 94.7853953242302
time_elpased: 1.795
batch start
#iterations: 230
currently lose_sum: 95.29564082622528
time_elpased: 1.819
batch start
#iterations: 231
currently lose_sum: 95.25618195533752
time_elpased: 1.798
batch start
#iterations: 232
currently lose_sum: 95.29150623083115
time_elpased: 1.777
batch start
#iterations: 233
currently lose_sum: 95.54204761981964
time_elpased: 1.792
batch start
#iterations: 234
currently lose_sum: 94.98709636926651
time_elpased: 1.794
batch start
#iterations: 235
currently lose_sum: 95.29853200912476
time_elpased: 1.807
batch start
#iterations: 236
currently lose_sum: 95.13134127855301
time_elpased: 1.83
batch start
#iterations: 237
currently lose_sum: 95.27983862161636
time_elpased: 1.802
batch start
#iterations: 238
currently lose_sum: 95.37122970819473
time_elpased: 1.8
batch start
#iterations: 239
currently lose_sum: 95.64819139242172
time_elpased: 1.794
start validation test
0.659896907216
0.625877794818
0.797982916538
0.701528996652
0.659654475929
61.072
batch start
#iterations: 240
currently lose_sum: 95.22807669639587
time_elpased: 1.831
batch start
#iterations: 241
currently lose_sum: 95.05337357521057
time_elpased: 1.814
batch start
#iterations: 242
currently lose_sum: 95.63295066356659
time_elpased: 1.859
batch start
#iterations: 243
currently lose_sum: 95.3812266588211
time_elpased: 1.823
batch start
#iterations: 244
currently lose_sum: 95.13491153717041
time_elpased: 1.794
batch start
#iterations: 245
currently lose_sum: 95.65491431951523
time_elpased: 1.848
batch start
#iterations: 246
currently lose_sum: 95.07671427726746
time_elpased: 1.821
batch start
#iterations: 247
currently lose_sum: 95.12884294986725
time_elpased: 1.859
batch start
#iterations: 248
currently lose_sum: 95.230060338974
time_elpased: 1.838
batch start
#iterations: 249
currently lose_sum: 95.07047587633133
time_elpased: 1.831
batch start
#iterations: 250
currently lose_sum: 95.39346998929977
time_elpased: 1.803
batch start
#iterations: 251
currently lose_sum: 95.23509329557419
time_elpased: 1.833
batch start
#iterations: 252
currently lose_sum: 95.31220161914825
time_elpased: 1.807
batch start
#iterations: 253
currently lose_sum: 95.11109471321106
time_elpased: 1.834
batch start
#iterations: 254
currently lose_sum: 95.35412937402725
time_elpased: 1.83
batch start
#iterations: 255
currently lose_sum: 95.28651338815689
time_elpased: 1.797
batch start
#iterations: 256
currently lose_sum: 95.09480106830597
time_elpased: 1.815
batch start
#iterations: 257
currently lose_sum: 94.90878134965897
time_elpased: 1.793
batch start
#iterations: 258
currently lose_sum: 94.7423534989357
time_elpased: 1.791
batch start
#iterations: 259
currently lose_sum: 94.85250341892242
time_elpased: 1.836
start validation test
0.663402061856
0.634415858288
0.774004322322
0.697292786946
0.663207882528
60.906
batch start
#iterations: 260
currently lose_sum: 95.30932807922363
time_elpased: 1.827
batch start
#iterations: 261
currently lose_sum: 94.94860196113586
time_elpased: 1.831
batch start
#iterations: 262
currently lose_sum: 95.0829970240593
time_elpased: 1.823
batch start
#iterations: 263
currently lose_sum: 94.67482364177704
time_elpased: 1.828
batch start
#iterations: 264
currently lose_sum: 94.8581190109253
time_elpased: 1.81
batch start
#iterations: 265
currently lose_sum: 95.05964732170105
time_elpased: 1.828
batch start
#iterations: 266
currently lose_sum: 95.30046612024307
time_elpased: 1.833
batch start
#iterations: 267
currently lose_sum: 95.27568280696869
time_elpased: 1.834
batch start
#iterations: 268
currently lose_sum: 94.88040614128113
time_elpased: 1.82
batch start
#iterations: 269
currently lose_sum: 94.9955860376358
time_elpased: 1.821
batch start
#iterations: 270
currently lose_sum: 95.03714746236801
time_elpased: 1.793
batch start
#iterations: 271
currently lose_sum: 94.98970574140549
time_elpased: 1.94
batch start
#iterations: 272
currently lose_sum: 95.37843006849289
time_elpased: 1.856
batch start
#iterations: 273
currently lose_sum: 95.06426441669464
time_elpased: 1.867
batch start
#iterations: 274
currently lose_sum: 95.22422486543655
time_elpased: 1.862
batch start
#iterations: 275
currently lose_sum: 95.4625512957573
time_elpased: 1.81
batch start
#iterations: 276
currently lose_sum: 94.85709172487259
time_elpased: 1.852
batch start
#iterations: 277
currently lose_sum: 95.18547874689102
time_elpased: 1.856
batch start
#iterations: 278
currently lose_sum: 94.97704672813416
time_elpased: 1.846
batch start
#iterations: 279
currently lose_sum: 95.03984451293945
time_elpased: 1.839
start validation test
0.660206185567
0.635645455335
0.753524750437
0.689583725749
0.660042350433
61.300
batch start
#iterations: 280
currently lose_sum: 94.81719821691513
time_elpased: 1.848
batch start
#iterations: 281
currently lose_sum: 95.14620798826218
time_elpased: 1.822
batch start
#iterations: 282
currently lose_sum: 94.86691629886627
time_elpased: 1.827
batch start
#iterations: 283
currently lose_sum: 94.9367173910141
time_elpased: 1.842
batch start
#iterations: 284
currently lose_sum: 95.39316540956497
time_elpased: 1.826
batch start
#iterations: 285
currently lose_sum: 94.83675318956375
time_elpased: 1.873
batch start
#iterations: 286
currently lose_sum: 94.86314141750336
time_elpased: 1.857
batch start
#iterations: 287
currently lose_sum: 94.616350710392
time_elpased: 1.848
batch start
#iterations: 288
currently lose_sum: 94.4759390950203
time_elpased: 1.792
batch start
#iterations: 289
currently lose_sum: 95.3043407201767
time_elpased: 1.804
batch start
#iterations: 290
currently lose_sum: 95.26822400093079
time_elpased: 1.794
batch start
#iterations: 291
currently lose_sum: 94.89210909605026
time_elpased: 1.851
batch start
#iterations: 292
currently lose_sum: 95.04649692773819
time_elpased: 1.793
batch start
#iterations: 293
currently lose_sum: 95.0361043214798
time_elpased: 1.841
batch start
#iterations: 294
currently lose_sum: 94.65845841169357
time_elpased: 1.803
batch start
#iterations: 295
currently lose_sum: 95.34566766023636
time_elpased: 1.871
batch start
#iterations: 296
currently lose_sum: 94.97774159908295
time_elpased: 1.83
batch start
#iterations: 297
currently lose_sum: 94.90832954645157
time_elpased: 1.826
batch start
#iterations: 298
currently lose_sum: 95.1622411608696
time_elpased: 1.812
batch start
#iterations: 299
currently lose_sum: 94.57831925153732
time_elpased: 1.868
start validation test
0.65675257732
0.646231828615
0.695379232273
0.669905318991
0.656684762269
61.234
batch start
#iterations: 300
currently lose_sum: 94.66138643026352
time_elpased: 1.846
batch start
#iterations: 301
currently lose_sum: 94.68301087617874
time_elpased: 1.875
batch start
#iterations: 302
currently lose_sum: 94.79920566082001
time_elpased: 1.836
batch start
#iterations: 303
currently lose_sum: 95.16906452178955
time_elpased: 1.889
batch start
#iterations: 304
currently lose_sum: 95.18806892633438
time_elpased: 1.868
batch start
#iterations: 305
currently lose_sum: 95.24155861139297
time_elpased: 1.824
batch start
#iterations: 306
currently lose_sum: 95.00875598192215
time_elpased: 1.898
batch start
#iterations: 307
currently lose_sum: 95.12060236930847
time_elpased: 1.831
batch start
#iterations: 308
currently lose_sum: 94.692292034626
time_elpased: 1.925
batch start
#iterations: 309
currently lose_sum: 94.94448101520538
time_elpased: 1.909
batch start
#iterations: 310
currently lose_sum: 95.12938022613525
time_elpased: 1.833
batch start
#iterations: 311
currently lose_sum: 94.50415760278702
time_elpased: 1.86
batch start
#iterations: 312
currently lose_sum: 94.63727504014969
time_elpased: 1.799
batch start
#iterations: 313
currently lose_sum: 95.15703719854355
time_elpased: 1.861
batch start
#iterations: 314
currently lose_sum: 94.88129699230194
time_elpased: 1.825
batch start
#iterations: 315
currently lose_sum: 94.82567101716995
time_elpased: 1.872
batch start
#iterations: 316
currently lose_sum: 94.74974638223648
time_elpased: 1.783
batch start
#iterations: 317
currently lose_sum: 95.3292710185051
time_elpased: 1.855
batch start
#iterations: 318
currently lose_sum: 94.94147402048111
time_elpased: 1.803
batch start
#iterations: 319
currently lose_sum: 94.66526401042938
time_elpased: 1.819
start validation test
0.663659793814
0.635506877229
0.770299475147
0.696441032798
0.663472571406
60.950
batch start
#iterations: 320
currently lose_sum: 95.44000118970871
time_elpased: 1.835
batch start
#iterations: 321
currently lose_sum: 95.300168633461
time_elpased: 1.875
batch start
#iterations: 322
currently lose_sum: 95.02832067012787
time_elpased: 1.909
batch start
#iterations: 323
currently lose_sum: 94.7568798661232
time_elpased: 1.863
batch start
#iterations: 324
currently lose_sum: 95.2317202091217
time_elpased: 1.893
batch start
#iterations: 325
currently lose_sum: 95.18241238594055
time_elpased: 1.91
batch start
#iterations: 326
currently lose_sum: 94.91178321838379
time_elpased: 1.862
batch start
#iterations: 327
currently lose_sum: 94.84014296531677
time_elpased: 1.856
batch start
#iterations: 328
currently lose_sum: 94.79321175813675
time_elpased: 1.792
batch start
#iterations: 329
currently lose_sum: 94.73956167697906
time_elpased: 1.885
batch start
#iterations: 330
currently lose_sum: 94.82118421792984
time_elpased: 1.787
batch start
#iterations: 331
currently lose_sum: 94.97057622671127
time_elpased: 1.869
batch start
#iterations: 332
currently lose_sum: 94.61749267578125
time_elpased: 1.801
batch start
#iterations: 333
currently lose_sum: 94.29664325714111
time_elpased: 1.823
batch start
#iterations: 334
currently lose_sum: 94.84829592704773
time_elpased: 1.877
batch start
#iterations: 335
currently lose_sum: 94.78185051679611
time_elpased: 1.833
batch start
#iterations: 336
currently lose_sum: 94.51082080602646
time_elpased: 1.803
batch start
#iterations: 337
currently lose_sum: 95.20176231861115
time_elpased: 1.865
batch start
#iterations: 338
currently lose_sum: 95.03683537244797
time_elpased: 1.815
batch start
#iterations: 339
currently lose_sum: 94.73295074701309
time_elpased: 1.857
start validation test
0.660979381443
0.639530750089
0.740557785325
0.686346511517
0.660839669281
61.060
batch start
#iterations: 340
currently lose_sum: 94.94351297616959
time_elpased: 1.831
batch start
#iterations: 341
currently lose_sum: 95.30287116765976
time_elpased: 1.853
batch start
#iterations: 342
currently lose_sum: 94.86062759160995
time_elpased: 1.855
batch start
#iterations: 343
currently lose_sum: 94.61285084486008
time_elpased: 1.854
batch start
#iterations: 344
currently lose_sum: 94.66239094734192
time_elpased: 1.865
batch start
#iterations: 345
currently lose_sum: 94.97405695915222
time_elpased: 1.904
batch start
#iterations: 346
currently lose_sum: 94.71220898628235
time_elpased: 1.893
batch start
#iterations: 347
currently lose_sum: 95.25501948595047
time_elpased: 1.827
batch start
#iterations: 348
currently lose_sum: 94.84100311994553
time_elpased: 1.823
batch start
#iterations: 349
currently lose_sum: 94.9980959892273
time_elpased: 1.879
batch start
#iterations: 350
currently lose_sum: 94.7967689037323
time_elpased: 1.828
batch start
#iterations: 351
currently lose_sum: 94.87607192993164
time_elpased: 1.896
batch start
#iterations: 352
currently lose_sum: 94.91281604766846
time_elpased: 1.807
batch start
#iterations: 353
currently lose_sum: 95.28785365819931
time_elpased: 1.813
batch start
#iterations: 354
currently lose_sum: 95.42401057481766
time_elpased: 1.828
batch start
#iterations: 355
currently lose_sum: 95.37373042106628
time_elpased: 1.806
batch start
#iterations: 356
currently lose_sum: 94.89064586162567
time_elpased: 1.828
batch start
#iterations: 357
currently lose_sum: 94.77980595827103
time_elpased: 1.833
batch start
#iterations: 358
currently lose_sum: 95.29796981811523
time_elpased: 1.821
batch start
#iterations: 359
currently lose_sum: 95.01773530244827
time_elpased: 1.827
start validation test
0.64706185567
0.651851851852
0.63394051662
0.642771430062
0.647084892205
61.498
batch start
#iterations: 360
currently lose_sum: 94.82911771535873
time_elpased: 1.846
batch start
#iterations: 361
currently lose_sum: 95.25351589918137
time_elpased: 1.869
batch start
#iterations: 362
currently lose_sum: 94.56526005268097
time_elpased: 1.829
batch start
#iterations: 363
currently lose_sum: 94.74781250953674
time_elpased: 1.833
batch start
#iterations: 364
currently lose_sum: 95.52483683824539
time_elpased: 1.831
batch start
#iterations: 365
currently lose_sum: 94.98381048440933
time_elpased: 1.879
batch start
#iterations: 366
currently lose_sum: 95.17209678888321
time_elpased: 1.84
batch start
#iterations: 367
currently lose_sum: 94.81397968530655
time_elpased: 1.821
batch start
#iterations: 368
currently lose_sum: 94.83563315868378
time_elpased: 1.827
batch start
#iterations: 369
currently lose_sum: 94.68871212005615
time_elpased: 1.838
batch start
#iterations: 370
currently lose_sum: 95.18586480617523
time_elpased: 1.83
batch start
#iterations: 371
currently lose_sum: 94.47148036956787
time_elpased: 1.845
batch start
#iterations: 372
currently lose_sum: 95.11119639873505
time_elpased: 1.853
batch start
#iterations: 373
currently lose_sum: 95.10163998603821
time_elpased: 1.832
batch start
#iterations: 374
currently lose_sum: 94.52631950378418
time_elpased: 1.802
batch start
#iterations: 375
currently lose_sum: 95.28105336427689
time_elpased: 1.848
batch start
#iterations: 376
currently lose_sum: 94.63831859827042
time_elpased: 1.811
batch start
#iterations: 377
currently lose_sum: 94.74409574270248
time_elpased: 1.839
batch start
#iterations: 378
currently lose_sum: 95.26288068294525
time_elpased: 1.906
batch start
#iterations: 379
currently lose_sum: 94.7341718673706
time_elpased: 1.853
start validation test
0.659381443299
0.644295924998
0.714315117835
0.677501220107
0.65928499876
61.104
batch start
#iterations: 380
currently lose_sum: 94.86782121658325
time_elpased: 1.825
batch start
#iterations: 381
currently lose_sum: 94.58631420135498
time_elpased: 1.84
batch start
#iterations: 382
currently lose_sum: 94.8664401769638
time_elpased: 1.823
batch start
#iterations: 383
currently lose_sum: 94.77888292074203
time_elpased: 1.832
batch start
#iterations: 384
currently lose_sum: 94.4459941983223
time_elpased: 1.885
batch start
#iterations: 385
currently lose_sum: 94.94812124967575
time_elpased: 1.832
batch start
#iterations: 386
currently lose_sum: 94.97733974456787
time_elpased: 1.825
batch start
#iterations: 387
currently lose_sum: 94.39933896064758
time_elpased: 1.875
batch start
#iterations: 388
currently lose_sum: 94.70571291446686
time_elpased: 1.807
batch start
#iterations: 389
currently lose_sum: 95.06909078359604
time_elpased: 1.858
batch start
#iterations: 390
currently lose_sum: 94.64073348045349
time_elpased: 1.795
batch start
#iterations: 391
currently lose_sum: 94.57004016637802
time_elpased: 1.815
batch start
#iterations: 392
currently lose_sum: 94.59018069505692
time_elpased: 1.808
batch start
#iterations: 393
currently lose_sum: 94.74049377441406
time_elpased: 1.804
batch start
#iterations: 394
currently lose_sum: 94.49919986724854
time_elpased: 1.795
batch start
#iterations: 395
currently lose_sum: 95.34774649143219
time_elpased: 1.843
batch start
#iterations: 396
currently lose_sum: 94.48055338859558
time_elpased: 1.798
batch start
#iterations: 397
currently lose_sum: 95.06473636627197
time_elpased: 1.811
batch start
#iterations: 398
currently lose_sum: 94.62607103586197
time_elpased: 1.797
batch start
#iterations: 399
currently lose_sum: 94.83182317018509
time_elpased: 1.819
start validation test
0.642680412371
0.650785056849
0.618503653391
0.634233853947
0.642722858401
61.824
acc: 0.662
pre: 0.634
rec: 0.773
F1: 0.696
auc: 0.662
