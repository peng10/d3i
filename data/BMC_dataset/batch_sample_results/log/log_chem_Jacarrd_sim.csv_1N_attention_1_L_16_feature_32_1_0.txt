start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.41580700874329
time_elpased: 1.965
batch start
#iterations: 1
currently lose_sum: 100.44457697868347
time_elpased: 1.926
batch start
#iterations: 2
currently lose_sum: 100.35330539941788
time_elpased: 1.761
batch start
#iterations: 3
currently lose_sum: 100.39933389425278
time_elpased: 1.783
batch start
#iterations: 4
currently lose_sum: 100.32772195339203
time_elpased: 1.774
batch start
#iterations: 5
currently lose_sum: 100.2919288277626
time_elpased: 1.792
batch start
#iterations: 6
currently lose_sum: 100.253327190876
time_elpased: 1.799
batch start
#iterations: 7
currently lose_sum: 100.17872250080109
time_elpased: 1.774
batch start
#iterations: 8
currently lose_sum: 100.1649187207222
time_elpased: 1.819
batch start
#iterations: 9
currently lose_sum: 100.1464581489563
time_elpased: 1.896
batch start
#iterations: 10
currently lose_sum: 100.03780716657639
time_elpased: 1.824
batch start
#iterations: 11
currently lose_sum: 99.93847900629044
time_elpased: 1.759
batch start
#iterations: 12
currently lose_sum: 99.98388254642487
time_elpased: 1.755
batch start
#iterations: 13
currently lose_sum: 99.81928825378418
time_elpased: 1.784
batch start
#iterations: 14
currently lose_sum: 99.68256801366806
time_elpased: 1.805
batch start
#iterations: 15
currently lose_sum: 99.5046923160553
time_elpased: 1.84
batch start
#iterations: 16
currently lose_sum: 99.47059339284897
time_elpased: 1.845
batch start
#iterations: 17
currently lose_sum: 99.29064309597015
time_elpased: 1.778
batch start
#iterations: 18
currently lose_sum: 99.16003787517548
time_elpased: 1.806
batch start
#iterations: 19
currently lose_sum: 99.1527710556984
time_elpased: 1.889
start validation test
0.617783505155
0.643088015912
0.532365956571
0.582512245932
0.617933468836
64.885
batch start
#iterations: 20
currently lose_sum: 99.08725142478943
time_elpased: 1.889
batch start
#iterations: 21
currently lose_sum: 98.74092262983322
time_elpased: 1.846
batch start
#iterations: 22
currently lose_sum: 98.73130393028259
time_elpased: 1.968
batch start
#iterations: 23
currently lose_sum: 98.7018690109253
time_elpased: 1.804
batch start
#iterations: 24
currently lose_sum: 98.36481237411499
time_elpased: 1.855
batch start
#iterations: 25
currently lose_sum: 98.15361738204956
time_elpased: 1.875
batch start
#iterations: 26
currently lose_sum: 98.27755033969879
time_elpased: 1.784
batch start
#iterations: 27
currently lose_sum: 98.3289423584938
time_elpased: 1.789
batch start
#iterations: 28
currently lose_sum: 97.78385895490646
time_elpased: 1.818
batch start
#iterations: 29
currently lose_sum: 98.18459594249725
time_elpased: 1.846
batch start
#iterations: 30
currently lose_sum: 97.86140841245651
time_elpased: 1.818
batch start
#iterations: 31
currently lose_sum: 97.93146067857742
time_elpased: 1.964
batch start
#iterations: 32
currently lose_sum: 97.58893084526062
time_elpased: 1.804
batch start
#iterations: 33
currently lose_sum: 97.72210949659348
time_elpased: 1.829
batch start
#iterations: 34
currently lose_sum: 97.69765675067902
time_elpased: 1.856
batch start
#iterations: 35
currently lose_sum: 97.6974493265152
time_elpased: 1.819
batch start
#iterations: 36
currently lose_sum: 97.69768542051315
time_elpased: 1.846
batch start
#iterations: 37
currently lose_sum: 97.53334730863571
time_elpased: 1.824
batch start
#iterations: 38
currently lose_sum: 97.45638674497604
time_elpased: 1.792
batch start
#iterations: 39
currently lose_sum: 97.39506560564041
time_elpased: 1.906
start validation test
0.647783505155
0.64518727346
0.65946279716
0.652246933686
0.647763000356
62.915
batch start
#iterations: 40
currently lose_sum: 97.49420464038849
time_elpased: 1.912
batch start
#iterations: 41
currently lose_sum: 97.3765863776207
time_elpased: 1.835
batch start
#iterations: 42
currently lose_sum: 97.1648741364479
time_elpased: 1.807
batch start
#iterations: 43
currently lose_sum: 97.25784420967102
time_elpased: 1.887
batch start
#iterations: 44
currently lose_sum: 97.47216254472733
time_elpased: 1.845
batch start
#iterations: 45
currently lose_sum: 97.09154415130615
time_elpased: 1.805
batch start
#iterations: 46
currently lose_sum: 97.11731469631195
time_elpased: 1.916
batch start
#iterations: 47
currently lose_sum: 97.13179087638855
time_elpased: 1.86
batch start
#iterations: 48
currently lose_sum: 97.18243277072906
time_elpased: 1.93
batch start
#iterations: 49
currently lose_sum: 97.23002088069916
time_elpased: 1.816
batch start
#iterations: 50
currently lose_sum: 97.30598211288452
time_elpased: 1.804
batch start
#iterations: 51
currently lose_sum: 96.78859812021255
time_elpased: 1.84
batch start
#iterations: 52
currently lose_sum: 96.86964291334152
time_elpased: 1.789
batch start
#iterations: 53
currently lose_sum: 97.11159300804138
time_elpased: 1.803
batch start
#iterations: 54
currently lose_sum: 97.02628314495087
time_elpased: 1.779
batch start
#iterations: 55
currently lose_sum: 96.77811461687088
time_elpased: 1.951
batch start
#iterations: 56
currently lose_sum: 97.28380453586578
time_elpased: 1.859
batch start
#iterations: 57
currently lose_sum: 96.51426488161087
time_elpased: 1.768
batch start
#iterations: 58
currently lose_sum: 96.96110183000565
time_elpased: 1.838
batch start
#iterations: 59
currently lose_sum: 96.86082929372787
time_elpased: 1.838
start validation test
0.602319587629
0.66228923476
0.420397241947
0.514321687126
0.602638980366
63.315
batch start
#iterations: 60
currently lose_sum: 97.06605911254883
time_elpased: 1.813
batch start
#iterations: 61
currently lose_sum: 96.95927804708481
time_elpased: 1.799
batch start
#iterations: 62
currently lose_sum: 96.82712066173553
time_elpased: 1.851
batch start
#iterations: 63
currently lose_sum: 96.88978892564774
time_elpased: 1.822
batch start
#iterations: 64
currently lose_sum: 97.25848364830017
time_elpased: 1.795
batch start
#iterations: 65
currently lose_sum: 96.69439220428467
time_elpased: 1.868
batch start
#iterations: 66
currently lose_sum: 96.60302823781967
time_elpased: 1.828
batch start
#iterations: 67
currently lose_sum: 96.74406278133392
time_elpased: 1.822
batch start
#iterations: 68
currently lose_sum: 96.71450763940811
time_elpased: 1.909
batch start
#iterations: 69
currently lose_sum: 97.08630883693695
time_elpased: 1.933
batch start
#iterations: 70
currently lose_sum: 96.68923318386078
time_elpased: 1.875
batch start
#iterations: 71
currently lose_sum: 96.65948820114136
time_elpased: 1.809
batch start
#iterations: 72
currently lose_sum: 96.3283976316452
time_elpased: 1.746
batch start
#iterations: 73
currently lose_sum: 95.947967171669
time_elpased: 1.963
batch start
#iterations: 74
currently lose_sum: 96.18914222717285
time_elpased: 1.97
batch start
#iterations: 75
currently lose_sum: 96.56479585170746
time_elpased: 1.856
batch start
#iterations: 76
currently lose_sum: 96.34146761894226
time_elpased: 1.855
batch start
#iterations: 77
currently lose_sum: 96.5582646727562
time_elpased: 1.87
batch start
#iterations: 78
currently lose_sum: 96.45299315452576
time_elpased: 1.893
batch start
#iterations: 79
currently lose_sum: 96.89176112413406
time_elpased: 1.802
start validation test
0.650154639175
0.63933802549
0.69177729752
0.664524739262
0.650081564179
62.305
batch start
#iterations: 80
currently lose_sum: 96.31875091791153
time_elpased: 1.857
batch start
#iterations: 81
currently lose_sum: 96.88252943754196
time_elpased: 1.872
batch start
#iterations: 82
currently lose_sum: 96.31894218921661
time_elpased: 1.902
batch start
#iterations: 83
currently lose_sum: 96.7420643568039
time_elpased: 1.852
batch start
#iterations: 84
currently lose_sum: 96.65344780683517
time_elpased: 1.789
batch start
#iterations: 85
currently lose_sum: 96.96448314189911
time_elpased: 1.84
batch start
#iterations: 86
currently lose_sum: 96.4201192855835
time_elpased: 1.871
batch start
#iterations: 87
currently lose_sum: 96.00671470165253
time_elpased: 1.804
batch start
#iterations: 88
currently lose_sum: 96.37524795532227
time_elpased: 1.791
batch start
#iterations: 89
currently lose_sum: 96.73944890499115
time_elpased: 1.811
batch start
#iterations: 90
currently lose_sum: 96.24443566799164
time_elpased: 1.874
batch start
#iterations: 91
currently lose_sum: 96.24158132076263
time_elpased: 1.815
batch start
#iterations: 92
currently lose_sum: 96.28156983852386
time_elpased: 1.869
batch start
#iterations: 93
currently lose_sum: 96.2607906460762
time_elpased: 1.806
batch start
#iterations: 94
currently lose_sum: 96.33945745229721
time_elpased: 1.803
batch start
#iterations: 95
currently lose_sum: 96.43989092111588
time_elpased: 1.904
batch start
#iterations: 96
currently lose_sum: 96.52145928144455
time_elpased: 1.9
batch start
#iterations: 97
currently lose_sum: 96.37583118677139
time_elpased: 1.823
batch start
#iterations: 98
currently lose_sum: 96.40462565422058
time_elpased: 1.838
batch start
#iterations: 99
currently lose_sum: 96.5578333735466
time_elpased: 1.887
start validation test
0.651134020619
0.639011973225
0.697540393125
0.666994686085
0.651052547074
61.992
batch start
#iterations: 100
currently lose_sum: 96.17215949296951
time_elpased: 1.831
batch start
#iterations: 101
currently lose_sum: 96.52526587247849
time_elpased: 1.812
batch start
#iterations: 102
currently lose_sum: 96.25196152925491
time_elpased: 1.867
batch start
#iterations: 103
currently lose_sum: 96.47427171468735
time_elpased: 1.837
batch start
#iterations: 104
currently lose_sum: 95.99014669656754
time_elpased: 1.845
batch start
#iterations: 105
currently lose_sum: 95.74111706018448
time_elpased: 1.808
batch start
#iterations: 106
currently lose_sum: 96.42735755443573
time_elpased: 1.84
batch start
#iterations: 107
currently lose_sum: 96.15747487545013
time_elpased: 1.891
batch start
#iterations: 108
currently lose_sum: 96.01665645837784
time_elpased: 1.795
batch start
#iterations: 109
currently lose_sum: 95.99769669771194
time_elpased: 1.812
batch start
#iterations: 110
currently lose_sum: 96.39114201068878
time_elpased: 1.802
batch start
#iterations: 111
currently lose_sum: 95.94942373037338
time_elpased: 1.83
batch start
#iterations: 112
currently lose_sum: 96.6416323184967
time_elpased: 1.819
batch start
#iterations: 113
currently lose_sum: 95.8142774105072
time_elpased: 1.815
batch start
#iterations: 114
currently lose_sum: 96.50166404247284
time_elpased: 1.752
batch start
#iterations: 115
currently lose_sum: 96.35120415687561
time_elpased: 1.769
batch start
#iterations: 116
currently lose_sum: 95.8165653347969
time_elpased: 1.906
batch start
#iterations: 117
currently lose_sum: 96.20870453119278
time_elpased: 1.83
batch start
#iterations: 118
currently lose_sum: 96.03719055652618
time_elpased: 1.812
batch start
#iterations: 119
currently lose_sum: 96.14466065168381
time_elpased: 1.752
start validation test
0.647113402062
0.630109670987
0.715447154472
0.670072289157
0.64699343162
62.052
batch start
#iterations: 120
currently lose_sum: 96.12540620565414
time_elpased: 1.818
batch start
#iterations: 121
currently lose_sum: 95.9874951839447
time_elpased: 1.871
batch start
#iterations: 122
currently lose_sum: 96.09971326589584
time_elpased: 1.789
batch start
#iterations: 123
currently lose_sum: 96.05894994735718
time_elpased: 1.839
batch start
#iterations: 124
currently lose_sum: 96.39914345741272
time_elpased: 1.838
batch start
#iterations: 125
currently lose_sum: 96.17389929294586
time_elpased: 1.804
batch start
#iterations: 126
currently lose_sum: 96.19045555591583
time_elpased: 1.813
batch start
#iterations: 127
currently lose_sum: 96.11081802845001
time_elpased: 1.789
batch start
#iterations: 128
currently lose_sum: 96.30256336927414
time_elpased: 1.771
batch start
#iterations: 129
currently lose_sum: 96.24738192558289
time_elpased: 1.812
batch start
#iterations: 130
currently lose_sum: 95.88846808671951
time_elpased: 1.875
batch start
#iterations: 131
currently lose_sum: 96.15576249361038
time_elpased: 1.885
batch start
#iterations: 132
currently lose_sum: 95.9110038280487
time_elpased: 1.792
batch start
#iterations: 133
currently lose_sum: 96.39882761240005
time_elpased: 1.802
batch start
#iterations: 134
currently lose_sum: 95.68269151449203
time_elpased: 1.824
batch start
#iterations: 135
currently lose_sum: 95.93288904428482
time_elpased: 1.898
batch start
#iterations: 136
currently lose_sum: 96.59380221366882
time_elpased: 1.785
batch start
#iterations: 137
currently lose_sum: 95.54714500904083
time_elpased: 1.805
batch start
#iterations: 138
currently lose_sum: 95.72514176368713
time_elpased: 1.859
batch start
#iterations: 139
currently lose_sum: 96.07542395591736
time_elpased: 1.847
start validation test
0.654278350515
0.620728381197
0.796336317794
0.697651354641
0.654028945843
61.598
batch start
#iterations: 140
currently lose_sum: 95.83470958471298
time_elpased: 1.869
batch start
#iterations: 141
currently lose_sum: 96.1465756893158
time_elpased: 1.865
batch start
#iterations: 142
currently lose_sum: 95.69866448640823
time_elpased: 1.879
batch start
#iterations: 143
currently lose_sum: 96.04194045066833
time_elpased: 1.859
batch start
#iterations: 144
currently lose_sum: 95.59158682823181
time_elpased: 1.854
batch start
#iterations: 145
currently lose_sum: 96.21841180324554
time_elpased: 1.78
batch start
#iterations: 146
currently lose_sum: 95.92192190885544
time_elpased: 1.782
batch start
#iterations: 147
currently lose_sum: 95.88194799423218
time_elpased: 1.801
batch start
#iterations: 148
currently lose_sum: 95.51509237289429
time_elpased: 1.856
batch start
#iterations: 149
currently lose_sum: 95.89711713790894
time_elpased: 1.775
batch start
#iterations: 150
currently lose_sum: 95.87910449504852
time_elpased: 1.869
batch start
#iterations: 151
currently lose_sum: 96.0430628657341
time_elpased: 1.822
batch start
#iterations: 152
currently lose_sum: 95.60796135663986
time_elpased: 1.829
batch start
#iterations: 153
currently lose_sum: 96.26335161924362
time_elpased: 1.801
batch start
#iterations: 154
currently lose_sum: 96.08520913124084
time_elpased: 1.975
batch start
#iterations: 155
currently lose_sum: 96.06655436754227
time_elpased: 1.882
batch start
#iterations: 156
currently lose_sum: 95.34670239686966
time_elpased: 1.872
batch start
#iterations: 157
currently lose_sum: 95.60633820295334
time_elpased: 1.767
batch start
#iterations: 158
currently lose_sum: 95.72454369068146
time_elpased: 1.796
batch start
#iterations: 159
currently lose_sum: 95.83048689365387
time_elpased: 1.782
start validation test
0.656082474227
0.634129151617
0.740763610168
0.683311182837
0.655933803431
61.526
batch start
#iterations: 160
currently lose_sum: 95.68625956773758
time_elpased: 1.819
batch start
#iterations: 161
currently lose_sum: 95.88986086845398
time_elpased: 1.764
batch start
#iterations: 162
currently lose_sum: 95.19540029764175
time_elpased: 1.88
batch start
#iterations: 163
currently lose_sum: 95.53712403774261
time_elpased: 1.84
batch start
#iterations: 164
currently lose_sum: 96.18914818763733
time_elpased: 1.887
batch start
#iterations: 165
currently lose_sum: 95.4387743473053
time_elpased: 1.814
batch start
#iterations: 166
currently lose_sum: 95.91860431432724
time_elpased: 1.824
batch start
#iterations: 167
currently lose_sum: 95.66233813762665
time_elpased: 1.824
batch start
#iterations: 168
currently lose_sum: 95.55244708061218
time_elpased: 1.84
batch start
#iterations: 169
currently lose_sum: 95.5530110001564
time_elpased: 1.837
batch start
#iterations: 170
currently lose_sum: 95.69263803958893
time_elpased: 1.814
batch start
#iterations: 171
currently lose_sum: 95.7122911810875
time_elpased: 1.808
batch start
#iterations: 172
currently lose_sum: 95.60424184799194
time_elpased: 1.765
batch start
#iterations: 173
currently lose_sum: 95.4967645406723
time_elpased: 2.003
batch start
#iterations: 174
currently lose_sum: 95.31413877010345
time_elpased: 1.826
batch start
#iterations: 175
currently lose_sum: 96.00072067975998
time_elpased: 1.805
batch start
#iterations: 176
currently lose_sum: 95.61571657657623
time_elpased: 1.811
batch start
#iterations: 177
currently lose_sum: 95.79309350252151
time_elpased: 1.867
batch start
#iterations: 178
currently lose_sum: 95.6211097240448
time_elpased: 1.813
batch start
#iterations: 179
currently lose_sum: 95.36567026376724
time_elpased: 1.811
start validation test
0.65912371134
0.630266912876
0.772769373263
0.694281355462
0.658924188852
61.252
batch start
#iterations: 180
currently lose_sum: 95.40819537639618
time_elpased: 1.931
batch start
#iterations: 181
currently lose_sum: 95.15380448102951
time_elpased: 1.772
batch start
#iterations: 182
currently lose_sum: 95.3177239894867
time_elpased: 1.838
batch start
#iterations: 183
currently lose_sum: 95.12918907403946
time_elpased: 1.783
batch start
#iterations: 184
currently lose_sum: 95.39322996139526
time_elpased: 1.787
batch start
#iterations: 185
currently lose_sum: 95.29905343055725
time_elpased: 1.83
batch start
#iterations: 186
currently lose_sum: 95.00965189933777
time_elpased: 1.864
batch start
#iterations: 187
currently lose_sum: 95.06993341445923
time_elpased: 1.829
batch start
#iterations: 188
currently lose_sum: 95.37598323822021
time_elpased: 1.806
batch start
#iterations: 189
currently lose_sum: 95.41451817750931
time_elpased: 1.853
batch start
#iterations: 190
currently lose_sum: 95.08175003528595
time_elpased: 1.924
batch start
#iterations: 191
currently lose_sum: 95.3892520070076
time_elpased: 1.787
batch start
#iterations: 192
currently lose_sum: 95.03107172250748
time_elpased: 1.864
batch start
#iterations: 193
currently lose_sum: 95.37504369020462
time_elpased: 1.788
batch start
#iterations: 194
currently lose_sum: 95.35366630554199
time_elpased: 1.816
batch start
#iterations: 195
currently lose_sum: 95.63969439268112
time_elpased: 1.859
batch start
#iterations: 196
currently lose_sum: 95.53392839431763
time_elpased: 1.835
batch start
#iterations: 197
currently lose_sum: 95.67715716362
time_elpased: 1.88
batch start
#iterations: 198
currently lose_sum: 95.18220520019531
time_elpased: 1.882
batch start
#iterations: 199
currently lose_sum: 95.75997453927994
time_elpased: 1.812
start validation test
0.654896907216
0.642493398717
0.701142327879
0.670537867231
0.654815716248
61.541
batch start
#iterations: 200
currently lose_sum: 95.45024687051773
time_elpased: 1.836
batch start
#iterations: 201
currently lose_sum: 95.20029884576797
time_elpased: 1.812
batch start
#iterations: 202
currently lose_sum: 95.49219137430191
time_elpased: 1.864
batch start
#iterations: 203
currently lose_sum: 95.45860302448273
time_elpased: 1.809
batch start
#iterations: 204
currently lose_sum: 95.257763504982
time_elpased: 1.808
batch start
#iterations: 205
currently lose_sum: 95.52219867706299
time_elpased: 1.783
batch start
#iterations: 206
currently lose_sum: 95.5795327425003
time_elpased: 1.86
batch start
#iterations: 207
currently lose_sum: 95.18470680713654
time_elpased: 1.861
batch start
#iterations: 208
currently lose_sum: 95.43992328643799
time_elpased: 1.828
batch start
#iterations: 209
currently lose_sum: 95.14397495985031
time_elpased: 1.84
batch start
#iterations: 210
currently lose_sum: 95.271737575531
time_elpased: 1.876
batch start
#iterations: 211
currently lose_sum: 95.27827286720276
time_elpased: 1.828
batch start
#iterations: 212
currently lose_sum: 95.06034404039383
time_elpased: 1.871
batch start
#iterations: 213
currently lose_sum: 94.76354640722275
time_elpased: 1.814
batch start
#iterations: 214
currently lose_sum: 94.97089660167694
time_elpased: 1.794
batch start
#iterations: 215
currently lose_sum: 94.98816758394241
time_elpased: 1.806
batch start
#iterations: 216
currently lose_sum: 95.29869782924652
time_elpased: 1.832
batch start
#iterations: 217
currently lose_sum: 95.2506537437439
time_elpased: 1.854
batch start
#iterations: 218
currently lose_sum: 95.26814639568329
time_elpased: 1.861
batch start
#iterations: 219
currently lose_sum: 95.25226908922195
time_elpased: 1.921
start validation test
0.655927835052
0.615912208505
0.8317381908
0.707736766058
0.655619172855
61.023
batch start
#iterations: 220
currently lose_sum: 95.90024477243423
time_elpased: 1.832
batch start
#iterations: 221
currently lose_sum: 95.49924337863922
time_elpased: 1.927
batch start
#iterations: 222
currently lose_sum: 95.37294340133667
time_elpased: 1.928
batch start
#iterations: 223
currently lose_sum: 95.3998743891716
time_elpased: 1.851
batch start
#iterations: 224
currently lose_sum: 95.25618898868561
time_elpased: 1.862
batch start
#iterations: 225
currently lose_sum: 94.99432677030563
time_elpased: 1.838
batch start
#iterations: 226
currently lose_sum: 95.40615630149841
time_elpased: 1.879
batch start
#iterations: 227
currently lose_sum: 95.18502479791641
time_elpased: 1.844
batch start
#iterations: 228
currently lose_sum: 95.0687290430069
time_elpased: 1.825
batch start
#iterations: 229
currently lose_sum: 95.43318492174149
time_elpased: 1.838
batch start
#iterations: 230
currently lose_sum: 95.28240048885345
time_elpased: 1.863
batch start
#iterations: 231
currently lose_sum: 95.08547896146774
time_elpased: 1.842
batch start
#iterations: 232
currently lose_sum: 95.03298062086105
time_elpased: 1.834
batch start
#iterations: 233
currently lose_sum: 95.2382344007492
time_elpased: 1.789
batch start
#iterations: 234
currently lose_sum: 95.69839829206467
time_elpased: 1.806
batch start
#iterations: 235
currently lose_sum: 95.01181137561798
time_elpased: 1.801
batch start
#iterations: 236
currently lose_sum: 95.2582494020462
time_elpased: 1.87
batch start
#iterations: 237
currently lose_sum: 95.40894359350204
time_elpased: 1.811
batch start
#iterations: 238
currently lose_sum: 95.02635878324509
time_elpased: 1.762
batch start
#iterations: 239
currently lose_sum: 95.11424398422241
time_elpased: 1.886
start validation test
0.656597938144
0.646522781775
0.693629721107
0.669248336809
0.656532923138
61.344
batch start
#iterations: 240
currently lose_sum: 94.66929906606674
time_elpased: 1.906
batch start
#iterations: 241
currently lose_sum: 95.16629558801651
time_elpased: 1.811
batch start
#iterations: 242
currently lose_sum: 94.83920729160309
time_elpased: 1.888
batch start
#iterations: 243
currently lose_sum: 94.82621395587921
time_elpased: 1.805
batch start
#iterations: 244
currently lose_sum: 95.13670766353607
time_elpased: 1.798
batch start
#iterations: 245
currently lose_sum: 95.55187273025513
time_elpased: 1.975
batch start
#iterations: 246
currently lose_sum: 95.31694370508194
time_elpased: 1.824
batch start
#iterations: 247
currently lose_sum: 95.2008449435234
time_elpased: 1.863
batch start
#iterations: 248
currently lose_sum: 95.32508856058121
time_elpased: 1.82
batch start
#iterations: 249
currently lose_sum: 95.1811500787735
time_elpased: 1.831
batch start
#iterations: 250
currently lose_sum: 95.41983878612518
time_elpased: 1.811
batch start
#iterations: 251
currently lose_sum: 95.1324890255928
time_elpased: 1.844
batch start
#iterations: 252
currently lose_sum: 95.2032180428505
time_elpased: 1.883
batch start
#iterations: 253
currently lose_sum: 95.14772909879684
time_elpased: 1.828
batch start
#iterations: 254
currently lose_sum: 95.17442309856415
time_elpased: 1.789
batch start
#iterations: 255
currently lose_sum: 95.16939604282379
time_elpased: 1.784
batch start
#iterations: 256
currently lose_sum: 95.00129467248917
time_elpased: 1.872
batch start
#iterations: 257
currently lose_sum: 95.03402715921402
time_elpased: 1.939
batch start
#iterations: 258
currently lose_sum: 94.84155863523483
time_elpased: 1.807
batch start
#iterations: 259
currently lose_sum: 94.99549669027328
time_elpased: 1.823
start validation test
0.664587628866
0.631816688568
0.791705258825
0.702781711049
0.66436445426
60.858
batch start
#iterations: 260
currently lose_sum: 94.67890554666519
time_elpased: 1.933
batch start
#iterations: 261
currently lose_sum: 95.39134466648102
time_elpased: 1.873
batch start
#iterations: 262
currently lose_sum: 94.9721668958664
time_elpased: 1.848
batch start
#iterations: 263
currently lose_sum: 94.82447254657745
time_elpased: 1.83
batch start
#iterations: 264
currently lose_sum: 95.48759299516678
time_elpased: 1.867
batch start
#iterations: 265
currently lose_sum: 94.8168129324913
time_elpased: 1.857
batch start
#iterations: 266
currently lose_sum: 95.0882648229599
time_elpased: 1.781
batch start
#iterations: 267
currently lose_sum: 94.76662784814835
time_elpased: 1.789
batch start
#iterations: 268
currently lose_sum: 94.60035121440887
time_elpased: 1.868
batch start
#iterations: 269
currently lose_sum: 95.3974557518959
time_elpased: 1.902
batch start
#iterations: 270
currently lose_sum: 95.43796718120575
time_elpased: 1.904
batch start
#iterations: 271
currently lose_sum: 94.9941958785057
time_elpased: 1.763
batch start
#iterations: 272
currently lose_sum: 94.99970507621765
time_elpased: 1.922
batch start
#iterations: 273
currently lose_sum: 95.06712645292282
time_elpased: 1.834
batch start
#iterations: 274
currently lose_sum: 94.55689483880997
time_elpased: 1.859
batch start
#iterations: 275
currently lose_sum: 94.95934826135635
time_elpased: 1.937
batch start
#iterations: 276
currently lose_sum: 95.64150708913803
time_elpased: 1.885
batch start
#iterations: 277
currently lose_sum: 95.37114661931992
time_elpased: 1.769
batch start
#iterations: 278
currently lose_sum: 95.02908146381378
time_elpased: 1.768
batch start
#iterations: 279
currently lose_sum: 94.94573867321014
time_elpased: 1.85
start validation test
0.647731958763
0.640812738107
0.675105485232
0.65751227824
0.647683900315
61.683
batch start
#iterations: 280
currently lose_sum: 95.2694640159607
time_elpased: 1.903
batch start
#iterations: 281
currently lose_sum: 95.24584048986435
time_elpased: 1.832
batch start
#iterations: 282
currently lose_sum: 95.2409039735794
time_elpased: 1.822
batch start
#iterations: 283
currently lose_sum: 95.24766880273819
time_elpased: 1.79
batch start
#iterations: 284
currently lose_sum: 94.77481466531754
time_elpased: 1.879
batch start
#iterations: 285
currently lose_sum: 94.77694457769394
time_elpased: 1.764
batch start
#iterations: 286
currently lose_sum: 94.99013602733612
time_elpased: 1.777
batch start
#iterations: 287
currently lose_sum: 95.14643222093582
time_elpased: 1.782
batch start
#iterations: 288
currently lose_sum: 94.6324434876442
time_elpased: 1.817
batch start
#iterations: 289
currently lose_sum: 95.24872922897339
time_elpased: 1.869
batch start
#iterations: 290
currently lose_sum: 95.17060643434525
time_elpased: 1.902
batch start
#iterations: 291
currently lose_sum: 94.5261926651001
time_elpased: 1.845
batch start
#iterations: 292
currently lose_sum: 94.3316188454628
time_elpased: 1.798
batch start
#iterations: 293
currently lose_sum: 94.74776148796082
time_elpased: 1.839
batch start
#iterations: 294
currently lose_sum: 94.71399867534637
time_elpased: 1.883
batch start
#iterations: 295
currently lose_sum: 94.9072095155716
time_elpased: 1.865
batch start
#iterations: 296
currently lose_sum: 94.87378311157227
time_elpased: 1.905
batch start
#iterations: 297
currently lose_sum: 95.01974809169769
time_elpased: 1.773
batch start
#iterations: 298
currently lose_sum: 95.17460423707962
time_elpased: 1.802
batch start
#iterations: 299
currently lose_sum: 94.3255346417427
time_elpased: 1.867
start validation test
0.663092783505
0.635696612917
0.766800452815
0.695120813509
0.662910708696
60.900
batch start
#iterations: 300
currently lose_sum: 95.11490970849991
time_elpased: 1.898
batch start
#iterations: 301
currently lose_sum: 95.00339078903198
time_elpased: 1.883
batch start
#iterations: 302
currently lose_sum: 94.86508148908615
time_elpased: 1.832
batch start
#iterations: 303
currently lose_sum: 94.67504996061325
time_elpased: 1.891
batch start
#iterations: 304
currently lose_sum: 94.47611778974533
time_elpased: 1.773
batch start
#iterations: 305
currently lose_sum: 94.85766762495041
time_elpased: 1.846
batch start
#iterations: 306
currently lose_sum: 95.30334728956223
time_elpased: 1.8
batch start
#iterations: 307
currently lose_sum: 94.93518048524857
time_elpased: 1.838
batch start
#iterations: 308
currently lose_sum: 94.64006847143173
time_elpased: 1.788
batch start
#iterations: 309
currently lose_sum: 95.0113645195961
time_elpased: 1.842
batch start
#iterations: 310
currently lose_sum: 94.80062925815582
time_elpased: 1.832
batch start
#iterations: 311
currently lose_sum: 95.02075833082199
time_elpased: 1.845
batch start
#iterations: 312
currently lose_sum: 94.8880764245987
time_elpased: 1.804
batch start
#iterations: 313
currently lose_sum: 94.38451999425888
time_elpased: 1.751
batch start
#iterations: 314
currently lose_sum: 94.6889539361
time_elpased: 1.753
batch start
#iterations: 315
currently lose_sum: 94.6318382024765
time_elpased: 1.741
batch start
#iterations: 316
currently lose_sum: 95.27758067846298
time_elpased: 1.848
batch start
#iterations: 317
currently lose_sum: 94.74977022409439
time_elpased: 1.898
batch start
#iterations: 318
currently lose_sum: 94.82947742938995
time_elpased: 1.824
batch start
#iterations: 319
currently lose_sum: 94.64561265707016
time_elpased: 1.852
start validation test
0.660670103093
0.637794583187
0.746423793352
0.687846744748
0.660519549263
60.795
batch start
#iterations: 320
currently lose_sum: 94.77735763788223
time_elpased: 1.86
batch start
#iterations: 321
currently lose_sum: 94.71285629272461
time_elpased: 1.816
batch start
#iterations: 322
currently lose_sum: 95.17308324575424
time_elpased: 1.808
batch start
#iterations: 323
currently lose_sum: 94.54828202724457
time_elpased: 1.903
batch start
#iterations: 324
currently lose_sum: 95.31050133705139
time_elpased: 1.841
batch start
#iterations: 325
currently lose_sum: 95.30596560239792
time_elpased: 1.865
batch start
#iterations: 326
currently lose_sum: 94.3460705280304
time_elpased: 1.869
batch start
#iterations: 327
currently lose_sum: 94.81906253099442
time_elpased: 1.819
batch start
#iterations: 328
currently lose_sum: 94.54904448986053
time_elpased: 1.911
batch start
#iterations: 329
currently lose_sum: 95.04646337032318
time_elpased: 1.816
batch start
#iterations: 330
currently lose_sum: 94.94374489784241
time_elpased: 1.902
batch start
#iterations: 331
currently lose_sum: 94.59678918123245
time_elpased: 1.84
batch start
#iterations: 332
currently lose_sum: 94.49307495355606
time_elpased: 1.875
batch start
#iterations: 333
currently lose_sum: 94.8297888636589
time_elpased: 1.849
batch start
#iterations: 334
currently lose_sum: 94.97183454036713
time_elpased: 1.802
batch start
#iterations: 335
currently lose_sum: 95.09487330913544
time_elpased: 1.925
batch start
#iterations: 336
currently lose_sum: 94.50993186235428
time_elpased: 1.83
batch start
#iterations: 337
currently lose_sum: 95.11830747127533
time_elpased: 1.829
batch start
#iterations: 338
currently lose_sum: 94.76252496242523
time_elpased: 1.879
batch start
#iterations: 339
currently lose_sum: 94.518061876297
time_elpased: 1.84
start validation test
0.664639175258
0.641093241937
0.750746115056
0.691600303375
0.664488001244
60.868
batch start
#iterations: 340
currently lose_sum: 94.64981478452682
time_elpased: 1.77
batch start
#iterations: 341
currently lose_sum: 94.63922107219696
time_elpased: 1.821
batch start
#iterations: 342
currently lose_sum: 94.91513746976852
time_elpased: 1.759
batch start
#iterations: 343
currently lose_sum: 94.82414400577545
time_elpased: 1.766
batch start
#iterations: 344
currently lose_sum: 94.35994982719421
time_elpased: 1.762
batch start
#iterations: 345
currently lose_sum: 94.88923805952072
time_elpased: 1.77
batch start
#iterations: 346
currently lose_sum: 94.56304198503494
time_elpased: 1.756
batch start
#iterations: 347
currently lose_sum: 94.88312059640884
time_elpased: 1.739
batch start
#iterations: 348
currently lose_sum: 94.68638700246811
time_elpased: 1.774
batch start
#iterations: 349
currently lose_sum: 94.93178707361221
time_elpased: 1.762
batch start
#iterations: 350
currently lose_sum: 94.34228444099426
time_elpased: 1.731
batch start
#iterations: 351
currently lose_sum: 94.60643619298935
time_elpased: 1.777
batch start
#iterations: 352
currently lose_sum: 94.5748216509819
time_elpased: 1.757
batch start
#iterations: 353
currently lose_sum: 94.82107186317444
time_elpased: 1.758
batch start
#iterations: 354
currently lose_sum: 94.84133791923523
time_elpased: 1.76
batch start
#iterations: 355
currently lose_sum: 94.69628351926804
time_elpased: 1.769
batch start
#iterations: 356
currently lose_sum: 94.92875093221664
time_elpased: 1.783
batch start
#iterations: 357
currently lose_sum: 94.93079251050949
time_elpased: 1.756
batch start
#iterations: 358
currently lose_sum: 94.94934326410294
time_elpased: 1.767
batch start
#iterations: 359
currently lose_sum: 94.535071849823
time_elpased: 1.749
start validation test
0.657525773196
0.629063418732
0.770711124833
0.692720377393
0.657327058853
61.095
batch start
#iterations: 360
currently lose_sum: 94.63506120443344
time_elpased: 1.764
batch start
#iterations: 361
currently lose_sum: 94.85400980710983
time_elpased: 1.815
batch start
#iterations: 362
currently lose_sum: 94.43407726287842
time_elpased: 1.768
batch start
#iterations: 363
currently lose_sum: 94.52210646867752
time_elpased: 1.779
batch start
#iterations: 364
currently lose_sum: 94.78505140542984
time_elpased: 1.792
batch start
#iterations: 365
currently lose_sum: 95.06938737630844
time_elpased: 1.779
batch start
#iterations: 366
currently lose_sum: 94.66619193553925
time_elpased: 1.746
batch start
#iterations: 367
currently lose_sum: 95.17755055427551
time_elpased: 1.763
batch start
#iterations: 368
currently lose_sum: 95.00547349452972
time_elpased: 1.786
batch start
#iterations: 369
currently lose_sum: 94.90327900648117
time_elpased: 1.746
batch start
#iterations: 370
currently lose_sum: 94.32716810703278
time_elpased: 1.768
batch start
#iterations: 371
currently lose_sum: 94.61531072854996
time_elpased: 1.771
batch start
#iterations: 372
currently lose_sum: 94.65520238876343
time_elpased: 1.739
batch start
#iterations: 373
currently lose_sum: 94.63610488176346
time_elpased: 1.806
batch start
#iterations: 374
currently lose_sum: 94.39218389987946
time_elpased: 1.734
batch start
#iterations: 375
currently lose_sum: 94.73288249969482
time_elpased: 1.752
batch start
#iterations: 376
currently lose_sum: 94.63875472545624
time_elpased: 1.757
batch start
#iterations: 377
currently lose_sum: 94.3995909690857
time_elpased: 1.76
batch start
#iterations: 378
currently lose_sum: 94.59024912118912
time_elpased: 1.76
batch start
#iterations: 379
currently lose_sum: 94.83945453166962
time_elpased: 1.753
start validation test
0.652010309278
0.633893102203
0.722548111557
0.675323426153
0.651886469287
61.195
batch start
#iterations: 380
currently lose_sum: 94.4592587351799
time_elpased: 1.75
batch start
#iterations: 381
currently lose_sum: 94.55223685503006
time_elpased: 1.753
batch start
#iterations: 382
currently lose_sum: 94.64763003587723
time_elpased: 1.75
batch start
#iterations: 383
currently lose_sum: 94.47055339813232
time_elpased: 1.766
batch start
#iterations: 384
currently lose_sum: 94.91632944345474
time_elpased: 1.744
batch start
#iterations: 385
currently lose_sum: 94.4814944267273
time_elpased: 1.784
batch start
#iterations: 386
currently lose_sum: 94.89840298891068
time_elpased: 1.764
batch start
#iterations: 387
currently lose_sum: 94.79668003320694
time_elpased: 1.762
batch start
#iterations: 388
currently lose_sum: 94.4499181509018
time_elpased: 1.778
batch start
#iterations: 389
currently lose_sum: 95.09404844045639
time_elpased: 1.797
batch start
#iterations: 390
currently lose_sum: 94.81537234783173
time_elpased: 1.763
batch start
#iterations: 391
currently lose_sum: 94.71876358985901
time_elpased: 1.74
batch start
#iterations: 392
currently lose_sum: 94.46097779273987
time_elpased: 1.745
batch start
#iterations: 393
currently lose_sum: 94.58110684156418
time_elpased: 1.762
batch start
#iterations: 394
currently lose_sum: 94.5369673371315
time_elpased: 1.742
batch start
#iterations: 395
currently lose_sum: 94.26341366767883
time_elpased: 1.743
batch start
#iterations: 396
currently lose_sum: 94.57753270864487
time_elpased: 1.755
batch start
#iterations: 397
currently lose_sum: 94.25533527135849
time_elpased: 1.74
batch start
#iterations: 398
currently lose_sum: 94.30790972709656
time_elpased: 1.762
batch start
#iterations: 399
currently lose_sum: 94.73496848344803
time_elpased: 1.72
start validation test
0.660824742268
0.639882279497
0.738396624473
0.685618729097
0.660688552864
60.867
acc: 0.662
pre: 0.638
rec: 0.749
F1: 0.689
auc: 0.662
