start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.42471605539322
time_elpased: 2.081
batch start
#iterations: 1
currently lose_sum: 100.47888952493668
time_elpased: 1.989
batch start
#iterations: 2
currently lose_sum: 100.37631541490555
time_elpased: 2.015
batch start
#iterations: 3
currently lose_sum: 100.34610962867737
time_elpased: 1.966
batch start
#iterations: 4
currently lose_sum: 100.30085635185242
time_elpased: 2.018
batch start
#iterations: 5
currently lose_sum: 100.20995968580246
time_elpased: 2.042
batch start
#iterations: 6
currently lose_sum: 100.21961867809296
time_elpased: 1.981
batch start
#iterations: 7
currently lose_sum: 100.04745095968246
time_elpased: 1.984
batch start
#iterations: 8
currently lose_sum: 99.7669392824173
time_elpased: 2.021
batch start
#iterations: 9
currently lose_sum: 99.94018566608429
time_elpased: 2.004
batch start
#iterations: 10
currently lose_sum: 99.78123551607132
time_elpased: 1.97
batch start
#iterations: 11
currently lose_sum: 99.82094043493271
time_elpased: 2.0
batch start
#iterations: 12
currently lose_sum: 99.53900462388992
time_elpased: 2.053
batch start
#iterations: 13
currently lose_sum: 99.46296340227127
time_elpased: 1.994
batch start
#iterations: 14
currently lose_sum: 99.41411745548248
time_elpased: 2.02
batch start
#iterations: 15
currently lose_sum: 99.20347541570663
time_elpased: 2.015
batch start
#iterations: 16
currently lose_sum: 99.09897196292877
time_elpased: 1.978
batch start
#iterations: 17
currently lose_sum: 99.07369011640549
time_elpased: 1.989
batch start
#iterations: 18
currently lose_sum: 98.77033054828644
time_elpased: 1.977
batch start
#iterations: 19
currently lose_sum: 98.83241683244705
time_elpased: 1.995
start validation test
0.630154639175
0.632230545152
0.625398785633
0.628796109473
0.63016298881
64.402
batch start
#iterations: 20
currently lose_sum: 98.9086804986
time_elpased: 2.003
batch start
#iterations: 21
currently lose_sum: 98.35678339004517
time_elpased: 1.988
batch start
#iterations: 22
currently lose_sum: 98.2820456624031
time_elpased: 1.988
batch start
#iterations: 23
currently lose_sum: 98.1746072769165
time_elpased: 1.967
batch start
#iterations: 24
currently lose_sum: 97.74827069044113
time_elpased: 1.981
batch start
#iterations: 25
currently lose_sum: 98.18053084611893
time_elpased: 2.031
batch start
#iterations: 26
currently lose_sum: 98.1436378955841
time_elpased: 1.995
batch start
#iterations: 27
currently lose_sum: 98.00249654054642
time_elpased: 1.972
batch start
#iterations: 28
currently lose_sum: 97.6719297170639
time_elpased: 1.924
batch start
#iterations: 29
currently lose_sum: 97.81300067901611
time_elpased: 1.936
batch start
#iterations: 30
currently lose_sum: 97.66160017251968
time_elpased: 1.939
batch start
#iterations: 31
currently lose_sum: 97.75566804409027
time_elpased: 1.97
batch start
#iterations: 32
currently lose_sum: 97.42580837011337
time_elpased: 1.976
batch start
#iterations: 33
currently lose_sum: 97.6045492887497
time_elpased: 1.956
batch start
#iterations: 34
currently lose_sum: 97.57936227321625
time_elpased: 1.934
batch start
#iterations: 35
currently lose_sum: 97.35319328308105
time_elpased: 1.957
batch start
#iterations: 36
currently lose_sum: 97.55329018831253
time_elpased: 1.904
batch start
#iterations: 37
currently lose_sum: 97.20379662513733
time_elpased: 2.03
batch start
#iterations: 38
currently lose_sum: 97.47975558042526
time_elpased: 1.985
batch start
#iterations: 39
currently lose_sum: 97.43302518129349
time_elpased: 1.998
start validation test
0.624432989691
0.66016602978
0.515591231862
0.578989945684
0.624624078184
63.206
batch start
#iterations: 40
currently lose_sum: 97.71489727497101
time_elpased: 1.997
batch start
#iterations: 41
currently lose_sum: 97.54686385393143
time_elpased: 1.983
batch start
#iterations: 42
currently lose_sum: 97.35503423213959
time_elpased: 2.011
batch start
#iterations: 43
currently lose_sum: 96.638636469841
time_elpased: 2.02
batch start
#iterations: 44
currently lose_sum: 97.35422521829605
time_elpased: 1.978
batch start
#iterations: 45
currently lose_sum: 97.22892755270004
time_elpased: 2.002
batch start
#iterations: 46
currently lose_sum: 97.1256520152092
time_elpased: 1.975
batch start
#iterations: 47
currently lose_sum: 97.163006067276
time_elpased: 2.014
batch start
#iterations: 48
currently lose_sum: 97.05976957082748
time_elpased: 1.979
batch start
#iterations: 49
currently lose_sum: 97.38649189472198
time_elpased: 1.984
batch start
#iterations: 50
currently lose_sum: 97.2352882027626
time_elpased: 1.974
batch start
#iterations: 51
currently lose_sum: 97.01017880439758
time_elpased: 1.949
batch start
#iterations: 52
currently lose_sum: 96.87702697515488
time_elpased: 2.01
batch start
#iterations: 53
currently lose_sum: 97.10601103305817
time_elpased: 1.94
batch start
#iterations: 54
currently lose_sum: 97.14249140024185
time_elpased: 2.039
batch start
#iterations: 55
currently lose_sum: 96.89385676383972
time_elpased: 1.998
batch start
#iterations: 56
currently lose_sum: 96.96288722753525
time_elpased: 1.975
batch start
#iterations: 57
currently lose_sum: 97.12867999076843
time_elpased: 2.019
batch start
#iterations: 58
currently lose_sum: 97.441590487957
time_elpased: 1.98
batch start
#iterations: 59
currently lose_sum: 96.97231930494308
time_elpased: 2.027
start validation test
0.630206185567
0.640048463487
0.598024081507
0.618323047457
0.630262686214
62.783
batch start
#iterations: 60
currently lose_sum: 96.76727098226547
time_elpased: 2.034
batch start
#iterations: 61
currently lose_sum: 97.27200621366501
time_elpased: 2.004
batch start
#iterations: 62
currently lose_sum: 96.80181121826172
time_elpased: 1.997
batch start
#iterations: 63
currently lose_sum: 96.98431867361069
time_elpased: 2.062
batch start
#iterations: 64
currently lose_sum: 97.33754050731659
time_elpased: 2.052
batch start
#iterations: 65
currently lose_sum: 96.77541196346283
time_elpased: 2.017
batch start
#iterations: 66
currently lose_sum: 97.03669792413712
time_elpased: 1.995
batch start
#iterations: 67
currently lose_sum: 96.99956738948822
time_elpased: 1.962
batch start
#iterations: 68
currently lose_sum: 96.88187205791473
time_elpased: 1.997
batch start
#iterations: 69
currently lose_sum: 96.71178358793259
time_elpased: 2.015
batch start
#iterations: 70
currently lose_sum: 96.2836520075798
time_elpased: 2.004
batch start
#iterations: 71
currently lose_sum: 96.38267850875854
time_elpased: 1.981
batch start
#iterations: 72
currently lose_sum: 96.28181767463684
time_elpased: 2.03
batch start
#iterations: 73
currently lose_sum: 96.68329131603241
time_elpased: 1.98
batch start
#iterations: 74
currently lose_sum: 96.3963274359703
time_elpased: 2.026
batch start
#iterations: 75
currently lose_sum: 96.5396483540535
time_elpased: 2.03
batch start
#iterations: 76
currently lose_sum: 96.98058503866196
time_elpased: 2.044
batch start
#iterations: 77
currently lose_sum: 96.7110247015953
time_elpased: 1.941
batch start
#iterations: 78
currently lose_sum: 97.02657598257065
time_elpased: 1.989
batch start
#iterations: 79
currently lose_sum: 96.61504828929901
time_elpased: 1.939
start validation test
0.652989690722
0.636164583523
0.717608315324
0.674436599284
0.652876242759
62.179
batch start
#iterations: 80
currently lose_sum: 96.58828032016754
time_elpased: 2.02
batch start
#iterations: 81
currently lose_sum: 96.84885597229004
time_elpased: 1.935
batch start
#iterations: 82
currently lose_sum: 96.86166173219681
time_elpased: 2.035
batch start
#iterations: 83
currently lose_sum: 96.92935854196548
time_elpased: 1.986
batch start
#iterations: 84
currently lose_sum: 96.88454288244247
time_elpased: 1.953
batch start
#iterations: 85
currently lose_sum: 96.66110026836395
time_elpased: 1.943
batch start
#iterations: 86
currently lose_sum: 96.81466591358185
time_elpased: 1.933
batch start
#iterations: 87
currently lose_sum: 96.36760717630386
time_elpased: 1.944
batch start
#iterations: 88
currently lose_sum: 96.49744886159897
time_elpased: 1.933
batch start
#iterations: 89
currently lose_sum: 96.8128331899643
time_elpased: 1.981
batch start
#iterations: 90
currently lose_sum: 96.41372990608215
time_elpased: 1.952
batch start
#iterations: 91
currently lose_sum: 96.430020570755
time_elpased: 1.992
batch start
#iterations: 92
currently lose_sum: 96.40230256319046
time_elpased: 2.061
batch start
#iterations: 93
currently lose_sum: 96.45156234502792
time_elpased: 2.012
batch start
#iterations: 94
currently lose_sum: 96.36111581325531
time_elpased: 2.009
batch start
#iterations: 95
currently lose_sum: 96.92634481191635
time_elpased: 2.037
batch start
#iterations: 96
currently lose_sum: 96.50908601284027
time_elpased: 2.004
batch start
#iterations: 97
currently lose_sum: 96.15654700994492
time_elpased: 1.972
batch start
#iterations: 98
currently lose_sum: 96.64159363508224
time_elpased: 2.019
batch start
#iterations: 99
currently lose_sum: 96.49406570196152
time_elpased: 1.987
start validation test
0.656134020619
0.633502805049
0.743748070392
0.684213017751
0.655980200641
61.941
batch start
#iterations: 100
currently lose_sum: 96.39487570524216
time_elpased: 2.003
batch start
#iterations: 101
currently lose_sum: 96.1934717297554
time_elpased: 1.997
batch start
#iterations: 102
currently lose_sum: 96.49056047201157
time_elpased: 1.967
batch start
#iterations: 103
currently lose_sum: 96.61332929134369
time_elpased: 1.963
batch start
#iterations: 104
currently lose_sum: 96.18258535861969
time_elpased: 1.984
batch start
#iterations: 105
currently lose_sum: 96.16512304544449
time_elpased: 2.01
batch start
#iterations: 106
currently lose_sum: 96.3286092877388
time_elpased: 1.989
batch start
#iterations: 107
currently lose_sum: 96.32044047117233
time_elpased: 2.024
batch start
#iterations: 108
currently lose_sum: 96.06381320953369
time_elpased: 2.037
batch start
#iterations: 109
currently lose_sum: 95.914810359478
time_elpased: 2.005
batch start
#iterations: 110
currently lose_sum: 96.21443694829941
time_elpased: 1.942
batch start
#iterations: 111
currently lose_sum: 96.28955894708633
time_elpased: 1.968
batch start
#iterations: 112
currently lose_sum: 96.47805881500244
time_elpased: 2.034
batch start
#iterations: 113
currently lose_sum: 96.07134348154068
time_elpased: 2.002
batch start
#iterations: 114
currently lose_sum: 96.30131304264069
time_elpased: 2.052
batch start
#iterations: 115
currently lose_sum: 96.52146661281586
time_elpased: 2.053
batch start
#iterations: 116
currently lose_sum: 96.33056634664536
time_elpased: 2.031
batch start
#iterations: 117
currently lose_sum: 96.18599194288254
time_elpased: 2.045
batch start
#iterations: 118
currently lose_sum: 96.17925697565079
time_elpased: 2.061
batch start
#iterations: 119
currently lose_sum: 96.03975880146027
time_elpased: 1.961
start validation test
0.647680412371
0.646771236504
0.653493876711
0.650115177886
0.647670205938
61.778
batch start
#iterations: 120
currently lose_sum: 96.73844385147095
time_elpased: 1.98
batch start
#iterations: 121
currently lose_sum: 95.91994404792786
time_elpased: 1.943
batch start
#iterations: 122
currently lose_sum: 96.37309682369232
time_elpased: 1.948
batch start
#iterations: 123
currently lose_sum: 95.83916807174683
time_elpased: 1.952
batch start
#iterations: 124
currently lose_sum: 96.5388275384903
time_elpased: 1.939
batch start
#iterations: 125
currently lose_sum: 96.07425320148468
time_elpased: 1.944
batch start
#iterations: 126
currently lose_sum: 95.74133318662643
time_elpased: 1.925
batch start
#iterations: 127
currently lose_sum: 96.12618589401245
time_elpased: 1.953
batch start
#iterations: 128
currently lose_sum: 96.07094866037369
time_elpased: 1.939
batch start
#iterations: 129
currently lose_sum: 96.21126955747604
time_elpased: 1.953
batch start
#iterations: 130
currently lose_sum: 95.69313073158264
time_elpased: 1.943
batch start
#iterations: 131
currently lose_sum: 96.09221851825714
time_elpased: 1.957
batch start
#iterations: 132
currently lose_sum: 96.0107204914093
time_elpased: 1.955
batch start
#iterations: 133
currently lose_sum: 96.41363054513931
time_elpased: 1.931
batch start
#iterations: 134
currently lose_sum: 96.08055758476257
time_elpased: 1.979
batch start
#iterations: 135
currently lose_sum: 95.79955929517746
time_elpased: 1.964
batch start
#iterations: 136
currently lose_sum: 96.30816864967346
time_elpased: 1.952
batch start
#iterations: 137
currently lose_sum: 95.72095185518265
time_elpased: 1.946
batch start
#iterations: 138
currently lose_sum: 95.73643761873245
time_elpased: 1.954
batch start
#iterations: 139
currently lose_sum: 95.96943908929825
time_elpased: 1.99
start validation test
0.656443298969
0.645499618612
0.696717093753
0.670131155655
0.656372592111
61.846
batch start
#iterations: 140
currently lose_sum: 96.2335793375969
time_elpased: 1.976
batch start
#iterations: 141
currently lose_sum: 95.9305767416954
time_elpased: 1.932
batch start
#iterations: 142
currently lose_sum: 95.64594346284866
time_elpased: 1.999
batch start
#iterations: 143
currently lose_sum: 95.91119420528412
time_elpased: 2.006
batch start
#iterations: 144
currently lose_sum: 95.59832715988159
time_elpased: 1.979
batch start
#iterations: 145
currently lose_sum: 96.03816467523575
time_elpased: 1.983
batch start
#iterations: 146
currently lose_sum: 95.97050988674164
time_elpased: 1.987
batch start
#iterations: 147
currently lose_sum: 95.88276559114456
time_elpased: 1.997
batch start
#iterations: 148
currently lose_sum: 95.52548998594284
time_elpased: 1.982
batch start
#iterations: 149
currently lose_sum: 95.79082971811295
time_elpased: 2.019
batch start
#iterations: 150
currently lose_sum: 95.6027540564537
time_elpased: 1.991
batch start
#iterations: 151
currently lose_sum: 95.85043781995773
time_elpased: 1.972
batch start
#iterations: 152
currently lose_sum: 96.02889531850815
time_elpased: 2.031
batch start
#iterations: 153
currently lose_sum: 95.66633450984955
time_elpased: 2.025
batch start
#iterations: 154
currently lose_sum: 95.91219371557236
time_elpased: 2.01
batch start
#iterations: 155
currently lose_sum: 95.86231696605682
time_elpased: 1.996
batch start
#iterations: 156
currently lose_sum: 95.56683826446533
time_elpased: 2.018
batch start
#iterations: 157
currently lose_sum: 95.45215952396393
time_elpased: 1.986
batch start
#iterations: 158
currently lose_sum: 95.76873761415482
time_elpased: 2.035
batch start
#iterations: 159
currently lose_sum: 95.58995419740677
time_elpased: 1.986
start validation test
0.655309278351
0.643656362602
0.698569517341
0.669989636283
0.655233328329
61.801
batch start
#iterations: 160
currently lose_sum: 95.65346866846085
time_elpased: 1.979
batch start
#iterations: 161
currently lose_sum: 95.68057125806808
time_elpased: 1.98
batch start
#iterations: 162
currently lose_sum: 95.73777467012405
time_elpased: 2.009
batch start
#iterations: 163
currently lose_sum: 95.1693167090416
time_elpased: 1.99
batch start
#iterations: 164
currently lose_sum: 95.60804891586304
time_elpased: 1.953
batch start
#iterations: 165
currently lose_sum: 95.3674464225769
time_elpased: 1.961
batch start
#iterations: 166
currently lose_sum: 95.86570465564728
time_elpased: 1.948
batch start
#iterations: 167
currently lose_sum: 95.55806744098663
time_elpased: 1.933
batch start
#iterations: 168
currently lose_sum: 95.85525232553482
time_elpased: 1.945
batch start
#iterations: 169
currently lose_sum: 95.68597966432571
time_elpased: 1.955
batch start
#iterations: 170
currently lose_sum: 95.69791531562805
time_elpased: 1.958
batch start
#iterations: 171
currently lose_sum: 95.78658610582352
time_elpased: 1.962
batch start
#iterations: 172
currently lose_sum: 95.45873075723648
time_elpased: 1.976
batch start
#iterations: 173
currently lose_sum: 95.4381754398346
time_elpased: 2.13
batch start
#iterations: 174
currently lose_sum: 95.79561787843704
time_elpased: 1.962
batch start
#iterations: 175
currently lose_sum: 95.6191446185112
time_elpased: 1.995
batch start
#iterations: 176
currently lose_sum: 95.49305975437164
time_elpased: 1.956
batch start
#iterations: 177
currently lose_sum: 95.57797539234161
time_elpased: 1.984
batch start
#iterations: 178
currently lose_sum: 95.52194184064865
time_elpased: 1.996
batch start
#iterations: 179
currently lose_sum: 95.16938602924347
time_elpased: 2.027
start validation test
0.655463917526
0.621680173313
0.797365442009
0.698647430117
0.655214787513
61.284
batch start
#iterations: 180
currently lose_sum: 95.70021200180054
time_elpased: 2.017
batch start
#iterations: 181
currently lose_sum: 94.81793981790543
time_elpased: 1.967
batch start
#iterations: 182
currently lose_sum: 95.83684813976288
time_elpased: 1.951
batch start
#iterations: 183
currently lose_sum: 95.02085602283478
time_elpased: 2.027
batch start
#iterations: 184
currently lose_sum: 95.71226614713669
time_elpased: 1.963
batch start
#iterations: 185
currently lose_sum: 95.38688331842422
time_elpased: 1.962
batch start
#iterations: 186
currently lose_sum: 94.91780924797058
time_elpased: 1.951
batch start
#iterations: 187
currently lose_sum: 94.92815232276917
time_elpased: 1.969
batch start
#iterations: 188
currently lose_sum: 95.55016082525253
time_elpased: 1.94
batch start
#iterations: 189
currently lose_sum: 95.30578243732452
time_elpased: 1.964
batch start
#iterations: 190
currently lose_sum: 95.4418249130249
time_elpased: 1.95
batch start
#iterations: 191
currently lose_sum: 95.2123841047287
time_elpased: 1.941
batch start
#iterations: 192
currently lose_sum: 95.08577704429626
time_elpased: 1.97
batch start
#iterations: 193
currently lose_sum: 95.35705769062042
time_elpased: 1.997
batch start
#iterations: 194
currently lose_sum: 95.30366080999374
time_elpased: 1.971
batch start
#iterations: 195
currently lose_sum: 95.59535562992096
time_elpased: 1.99
batch start
#iterations: 196
currently lose_sum: 95.54080760478973
time_elpased: 1.939
batch start
#iterations: 197
currently lose_sum: 96.22364622354507
time_elpased: 1.978
batch start
#iterations: 198
currently lose_sum: 95.17080873250961
time_elpased: 1.97
batch start
#iterations: 199
currently lose_sum: 95.67676562070847
time_elpased: 2.01
start validation test
0.661649484536
0.635055255718
0.762889780797
0.693127629734
0.661471741581
61.265
batch start
#iterations: 200
currently lose_sum: 95.44494062662125
time_elpased: 2.006
batch start
#iterations: 201
currently lose_sum: 95.30869174003601
time_elpased: 1.993
batch start
#iterations: 202
currently lose_sum: 94.99453949928284
time_elpased: 2.017
batch start
#iterations: 203
currently lose_sum: 95.3832403421402
time_elpased: 2.002
batch start
#iterations: 204
currently lose_sum: 95.30799734592438
time_elpased: 1.949
batch start
#iterations: 205
currently lose_sum: 95.47446310520172
time_elpased: 1.949
batch start
#iterations: 206
currently lose_sum: 95.41865670681
time_elpased: 1.952
batch start
#iterations: 207
currently lose_sum: 95.09160071611404
time_elpased: 1.941
batch start
#iterations: 208
currently lose_sum: 94.94605338573456
time_elpased: 1.954
batch start
#iterations: 209
currently lose_sum: 95.4077815413475
time_elpased: 1.952
batch start
#iterations: 210
currently lose_sum: 95.01089453697205
time_elpased: 1.942
batch start
#iterations: 211
currently lose_sum: 94.82264560461044
time_elpased: 1.956
batch start
#iterations: 212
currently lose_sum: 95.2658788561821
time_elpased: 2.016
batch start
#iterations: 213
currently lose_sum: 95.33801835775375
time_elpased: 1.976
batch start
#iterations: 214
currently lose_sum: 94.98511064052582
time_elpased: 1.966
batch start
#iterations: 215
currently lose_sum: 95.33450013399124
time_elpased: 2.0
batch start
#iterations: 216
currently lose_sum: 95.61986047029495
time_elpased: 2.022
batch start
#iterations: 217
currently lose_sum: 95.040491938591
time_elpased: 1.99
batch start
#iterations: 218
currently lose_sum: 95.30285197496414
time_elpased: 1.958
batch start
#iterations: 219
currently lose_sum: 95.53488671779633
time_elpased: 1.994
start validation test
0.66293814433
0.625751820196
0.813728517032
0.707466559299
0.662673408573
61.064
batch start
#iterations: 220
currently lose_sum: 96.19872039556503
time_elpased: 2.064
batch start
#iterations: 221
currently lose_sum: 95.2702808380127
time_elpased: 1.994
batch start
#iterations: 222
currently lose_sum: 95.15322881937027
time_elpased: 1.981
batch start
#iterations: 223
currently lose_sum: 95.00391948223114
time_elpased: 1.956
batch start
#iterations: 224
currently lose_sum: 95.43066984415054
time_elpased: 1.973
batch start
#iterations: 225
currently lose_sum: 95.06236469745636
time_elpased: 1.971
batch start
#iterations: 226
currently lose_sum: 95.14071863889694
time_elpased: 1.965
batch start
#iterations: 227
currently lose_sum: 95.55207425355911
time_elpased: 1.978
batch start
#iterations: 228
currently lose_sum: 95.18669986724854
time_elpased: 1.96
batch start
#iterations: 229
currently lose_sum: 95.22033578157425
time_elpased: 1.974
batch start
#iterations: 230
currently lose_sum: 94.5747304558754
time_elpased: 1.972
batch start
#iterations: 231
currently lose_sum: 95.38463765382767
time_elpased: 1.999
batch start
#iterations: 232
currently lose_sum: 95.1724950671196
time_elpased: 1.974
batch start
#iterations: 233
currently lose_sum: 95.08720767498016
time_elpased: 1.992
batch start
#iterations: 234
currently lose_sum: 95.34109377861023
time_elpased: 1.982
batch start
#iterations: 235
currently lose_sum: 95.25658178329468
time_elpased: 1.984
batch start
#iterations: 236
currently lose_sum: 95.40248936414719
time_elpased: 1.97
batch start
#iterations: 237
currently lose_sum: 94.73062562942505
time_elpased: 1.993
batch start
#iterations: 238
currently lose_sum: 95.18071007728577
time_elpased: 1.945
batch start
#iterations: 239
currently lose_sum: 95.09594565629959
time_elpased: 1.958
start validation test
0.622783505155
0.672119385852
0.482041782443
0.561428742659
0.623030598957
62.063
batch start
#iterations: 240
currently lose_sum: 95.09329760074615
time_elpased: 1.987
batch start
#iterations: 241
currently lose_sum: 95.17278403043747
time_elpased: 1.961
batch start
#iterations: 242
currently lose_sum: 94.78845965862274
time_elpased: 1.948
batch start
#iterations: 243
currently lose_sum: 95.07999503612518
time_elpased: 1.95
batch start
#iterations: 244
currently lose_sum: 95.2752256989479
time_elpased: 1.965
batch start
#iterations: 245
currently lose_sum: 95.03036653995514
time_elpased: 1.959
batch start
#iterations: 246
currently lose_sum: 95.34599220752716
time_elpased: 1.948
batch start
#iterations: 247
currently lose_sum: 95.2261523604393
time_elpased: 1.958
batch start
#iterations: 248
currently lose_sum: 95.54476225376129
time_elpased: 1.939
batch start
#iterations: 249
currently lose_sum: 95.04366338253021
time_elpased: 1.952
batch start
#iterations: 250
currently lose_sum: 95.23059469461441
time_elpased: 1.956
batch start
#iterations: 251
currently lose_sum: 95.00156277418137
time_elpased: 1.986
batch start
#iterations: 252
currently lose_sum: 94.79219424724579
time_elpased: 1.964
batch start
#iterations: 253
currently lose_sum: 95.36215275526047
time_elpased: 2.029
batch start
#iterations: 254
currently lose_sum: 95.00889188051224
time_elpased: 1.96
batch start
#iterations: 255
currently lose_sum: 94.77620607614517
time_elpased: 2.011
batch start
#iterations: 256
currently lose_sum: 94.79048103094101
time_elpased: 1.959
batch start
#iterations: 257
currently lose_sum: 95.02563488483429
time_elpased: 2.014
batch start
#iterations: 258
currently lose_sum: 95.15361958742142
time_elpased: 1.98
batch start
#iterations: 259
currently lose_sum: 95.06323426961899
time_elpased: 1.972
start validation test
0.659226804124
0.619756323257
0.827107131831
0.708573947542
0.658932064315
60.942
batch start
#iterations: 260
currently lose_sum: 94.81078028678894
time_elpased: 1.989
batch start
#iterations: 261
currently lose_sum: 95.54192000627518
time_elpased: 1.978
batch start
#iterations: 262
currently lose_sum: 95.01668840646744
time_elpased: 1.981
batch start
#iterations: 263
currently lose_sum: 94.7615697979927
time_elpased: 1.971
batch start
#iterations: 264
currently lose_sum: 95.14809584617615
time_elpased: 1.959
batch start
#iterations: 265
currently lose_sum: 94.87969195842743
time_elpased: 1.973
batch start
#iterations: 266
currently lose_sum: 94.97433811426163
time_elpased: 1.978
batch start
#iterations: 267
currently lose_sum: 95.22635632753372
time_elpased: 2.035
batch start
#iterations: 268
currently lose_sum: 95.11464786529541
time_elpased: 1.933
batch start
#iterations: 269
currently lose_sum: 95.09095132350922
time_elpased: 1.96
batch start
#iterations: 270
currently lose_sum: 95.2380998134613
time_elpased: 1.955
batch start
#iterations: 271
currently lose_sum: 94.98106384277344
time_elpased: 1.954
batch start
#iterations: 272
currently lose_sum: 94.7747997045517
time_elpased: 1.972
batch start
#iterations: 273
currently lose_sum: 95.21915066242218
time_elpased: 1.989
batch start
#iterations: 274
currently lose_sum: 95.28455919027328
time_elpased: 1.986
batch start
#iterations: 275
currently lose_sum: 95.23109644651413
time_elpased: 1.975
batch start
#iterations: 276
currently lose_sum: 95.00119715929031
time_elpased: 1.947
batch start
#iterations: 277
currently lose_sum: 95.44221246242523
time_elpased: 1.969
batch start
#iterations: 278
currently lose_sum: 95.12874734401703
time_elpased: 1.932
batch start
#iterations: 279
currently lose_sum: 95.44348472356796
time_elpased: 1.953
start validation test
0.652628865979
0.650040306328
0.663888031285
0.656891196986
0.652609098778
61.458
batch start
#iterations: 280
currently lose_sum: 94.80059504508972
time_elpased: 1.954
batch start
#iterations: 281
currently lose_sum: 94.76479041576385
time_elpased: 1.954
batch start
#iterations: 282
currently lose_sum: 94.87045556306839
time_elpased: 1.953
batch start
#iterations: 283
currently lose_sum: 95.49466091394424
time_elpased: 1.977
batch start
#iterations: 284
currently lose_sum: 94.6136764883995
time_elpased: 1.952
batch start
#iterations: 285
currently lose_sum: 94.94090366363525
time_elpased: 1.952
batch start
#iterations: 286
currently lose_sum: 94.99545681476593
time_elpased: 1.971
batch start
#iterations: 287
currently lose_sum: 95.06042796373367
time_elpased: 1.989
batch start
#iterations: 288
currently lose_sum: 94.66432791948318
time_elpased: 1.976
batch start
#iterations: 289
currently lose_sum: 94.84795755147934
time_elpased: 1.985
batch start
#iterations: 290
currently lose_sum: 94.84016090631485
time_elpased: 1.943
batch start
#iterations: 291
currently lose_sum: 94.70706814527512
time_elpased: 1.945
batch start
#iterations: 292
currently lose_sum: 94.74954837560654
time_elpased: 1.972
batch start
#iterations: 293
currently lose_sum: 94.92400306463242
time_elpased: 1.967
batch start
#iterations: 294
currently lose_sum: 95.24118250608444
time_elpased: 1.935
batch start
#iterations: 295
currently lose_sum: 94.99578297138214
time_elpased: 1.964
batch start
#iterations: 296
currently lose_sum: 94.80263036489487
time_elpased: 1.933
batch start
#iterations: 297
currently lose_sum: 94.89439141750336
time_elpased: 1.976
batch start
#iterations: 298
currently lose_sum: 95.10805016756058
time_elpased: 1.94
batch start
#iterations: 299
currently lose_sum: 94.48937928676605
time_elpased: 2.062
start validation test
0.662783505155
0.631885021185
0.782751878152
0.699273696791
0.662572882172
60.940
batch start
#iterations: 300
currently lose_sum: 95.19352358579636
time_elpased: 1.965
batch start
#iterations: 301
currently lose_sum: 94.6838709115982
time_elpased: 1.955
batch start
#iterations: 302
currently lose_sum: 94.84119176864624
time_elpased: 1.922
batch start
#iterations: 303
currently lose_sum: 94.99222296476364
time_elpased: 1.981
batch start
#iterations: 304
currently lose_sum: 94.6381573677063
time_elpased: 1.934
batch start
#iterations: 305
currently lose_sum: 94.75900322198868
time_elpased: 1.985
batch start
#iterations: 306
currently lose_sum: 94.9449713230133
time_elpased: 1.946
batch start
#iterations: 307
currently lose_sum: 94.90971046686172
time_elpased: 1.958
batch start
#iterations: 308
currently lose_sum: 94.8153048157692
time_elpased: 1.936
batch start
#iterations: 309
currently lose_sum: 95.16708141565323
time_elpased: 2.012
batch start
#iterations: 310
currently lose_sum: 94.87148714065552
time_elpased: 1.98
batch start
#iterations: 311
currently lose_sum: 94.68718028068542
time_elpased: 1.986
batch start
#iterations: 312
currently lose_sum: 94.80419600009918
time_elpased: 1.968
batch start
#iterations: 313
currently lose_sum: 94.73297822475433
time_elpased: 1.961
batch start
#iterations: 314
currently lose_sum: 94.82447338104248
time_elpased: 1.996
batch start
#iterations: 315
currently lose_sum: 94.7546455860138
time_elpased: 1.961
batch start
#iterations: 316
currently lose_sum: 94.7422536611557
time_elpased: 1.947
batch start
#iterations: 317
currently lose_sum: 94.57121223211288
time_elpased: 1.955
batch start
#iterations: 318
currently lose_sum: 94.864881336689
time_elpased: 1.946
batch start
#iterations: 319
currently lose_sum: 94.94624102115631
time_elpased: 1.987
start validation test
0.62675257732
0.656788247214
0.533703818051
0.588883211264
0.626915938768
62.124
batch start
#iterations: 320
currently lose_sum: 94.97259134054184
time_elpased: 1.94
batch start
#iterations: 321
currently lose_sum: 94.92308795452118
time_elpased: 1.984
batch start
#iterations: 322
currently lose_sum: 95.44706690311432
time_elpased: 1.923
batch start
#iterations: 323
currently lose_sum: 94.71109104156494
time_elpased: 1.968
batch start
#iterations: 324
currently lose_sum: 94.90844452381134
time_elpased: 1.997
batch start
#iterations: 325
currently lose_sum: 94.8348296880722
time_elpased: 1.983
batch start
#iterations: 326
currently lose_sum: 94.51725995540619
time_elpased: 1.925
batch start
#iterations: 327
currently lose_sum: 95.14983350038528
time_elpased: 1.947
batch start
#iterations: 328
currently lose_sum: 94.64560294151306
time_elpased: 1.936
batch start
#iterations: 329
currently lose_sum: 94.59502166509628
time_elpased: 1.997
batch start
#iterations: 330
currently lose_sum: 94.8370509147644
time_elpased: 1.928
batch start
#iterations: 331
currently lose_sum: 94.82800078392029
time_elpased: 1.968
batch start
#iterations: 332
currently lose_sum: 94.80092859268188
time_elpased: 1.934
batch start
#iterations: 333
currently lose_sum: 94.78347831964493
time_elpased: 1.989
batch start
#iterations: 334
currently lose_sum: 95.10998803377151
time_elpased: 1.913
batch start
#iterations: 335
currently lose_sum: 95.13032329082489
time_elpased: 1.949
batch start
#iterations: 336
currently lose_sum: 94.4525255560875
time_elpased: 1.936
batch start
#iterations: 337
currently lose_sum: 94.74482840299606
time_elpased: 1.942
batch start
#iterations: 338
currently lose_sum: 94.81464374065399
time_elpased: 1.933
batch start
#iterations: 339
currently lose_sum: 94.59117364883423
time_elpased: 2.001
start validation test
0.661907216495
0.647901835894
0.711845219718
0.678370028931
0.661819542628
61.012
batch start
#iterations: 340
currently lose_sum: 94.92063117027283
time_elpased: 2.003
batch start
#iterations: 341
currently lose_sum: 94.5289563536644
time_elpased: 1.958
batch start
#iterations: 342
currently lose_sum: 94.87741148471832
time_elpased: 1.946
batch start
#iterations: 343
currently lose_sum: 94.77835619449615
time_elpased: 1.987
batch start
#iterations: 344
currently lose_sum: 94.90817242860794
time_elpased: 1.946
batch start
#iterations: 345
currently lose_sum: 95.00149673223495
time_elpased: 1.954
batch start
#iterations: 346
currently lose_sum: 94.73018127679825
time_elpased: 1.95
batch start
#iterations: 347
currently lose_sum: 94.82357513904572
time_elpased: 1.986
batch start
#iterations: 348
currently lose_sum: 94.62166279554367
time_elpased: 1.944
batch start
#iterations: 349
currently lose_sum: 94.64918422698975
time_elpased: 1.988
batch start
#iterations: 350
currently lose_sum: 94.69208031892776
time_elpased: 1.932
batch start
#iterations: 351
currently lose_sum: 94.95899432897568
time_elpased: 1.987
batch start
#iterations: 352
currently lose_sum: 94.8285026550293
time_elpased: 1.937
batch start
#iterations: 353
currently lose_sum: 94.66982847452164
time_elpased: 2.025
batch start
#iterations: 354
currently lose_sum: 94.85414999723434
time_elpased: 1.957
batch start
#iterations: 355
currently lose_sum: 94.76855289936066
time_elpased: 1.956
batch start
#iterations: 356
currently lose_sum: 95.0511412024498
time_elpased: 1.951
batch start
#iterations: 357
currently lose_sum: 94.83490061759949
time_elpased: 1.955
batch start
#iterations: 358
currently lose_sum: 94.78962630033493
time_elpased: 1.949
batch start
#iterations: 359
currently lose_sum: 94.57861125469208
time_elpased: 1.94
start validation test
0.646030927835
0.64549724321
0.650612328908
0.648044692737
0.646022884479
61.683
batch start
#iterations: 360
currently lose_sum: 95.08056485652924
time_elpased: 2.002
batch start
#iterations: 361
currently lose_sum: 94.79830342531204
time_elpased: 2.012
batch start
#iterations: 362
currently lose_sum: 94.80812740325928
time_elpased: 1.986
batch start
#iterations: 363
currently lose_sum: 94.82394701242447
time_elpased: 1.97
batch start
#iterations: 364
currently lose_sum: 95.12972873449326
time_elpased: 1.993
batch start
#iterations: 365
currently lose_sum: 94.66525059938431
time_elpased: 1.957
batch start
#iterations: 366
currently lose_sum: 94.97492575645447
time_elpased: 1.952
batch start
#iterations: 367
currently lose_sum: 94.84144669771194
time_elpased: 1.971
batch start
#iterations: 368
currently lose_sum: 94.61247617006302
time_elpased: 1.957
batch start
#iterations: 369
currently lose_sum: 94.88121312856674
time_elpased: 1.953
batch start
#iterations: 370
currently lose_sum: 94.44073748588562
time_elpased: 1.978
batch start
#iterations: 371
currently lose_sum: 94.65475958585739
time_elpased: 1.961
batch start
#iterations: 372
currently lose_sum: 94.44291067123413
time_elpased: 1.932
batch start
#iterations: 373
currently lose_sum: 94.63201230764389
time_elpased: 1.992
batch start
#iterations: 374
currently lose_sum: 94.59468019008636
time_elpased: 1.952
batch start
#iterations: 375
currently lose_sum: 94.60049211978912
time_elpased: 1.986
batch start
#iterations: 376
currently lose_sum: 94.68828868865967
time_elpased: 1.969
batch start
#iterations: 377
currently lose_sum: 94.54445987939835
time_elpased: 1.979
batch start
#iterations: 378
currently lose_sum: 94.60491126775742
time_elpased: 1.969
batch start
#iterations: 379
currently lose_sum: 94.63932681083679
time_elpased: 1.98
start validation test
0.658505154639
0.636235459993
0.743027683441
0.685497270354
0.658356762303
61.005
batch start
#iterations: 380
currently lose_sum: 94.48116087913513
time_elpased: 1.966
batch start
#iterations: 381
currently lose_sum: 94.77250528335571
time_elpased: 2.033
batch start
#iterations: 382
currently lose_sum: 94.49340277910233
time_elpased: 2.016
batch start
#iterations: 383
currently lose_sum: 94.87587416172028
time_elpased: 1.975
batch start
#iterations: 384
currently lose_sum: 95.42617321014404
time_elpased: 1.968
batch start
#iterations: 385
currently lose_sum: 94.4681521654129
time_elpased: 2.01
batch start
#iterations: 386
currently lose_sum: 94.59455835819244
time_elpased: 1.96
batch start
#iterations: 387
currently lose_sum: 94.73792457580566
time_elpased: 1.98
batch start
#iterations: 388
currently lose_sum: 94.63433700799942
time_elpased: 1.957
batch start
#iterations: 389
currently lose_sum: 94.79473525285721
time_elpased: 1.975
batch start
#iterations: 390
currently lose_sum: 94.75811207294464
time_elpased: 1.973
batch start
#iterations: 391
currently lose_sum: 94.65217989683151
time_elpased: 2.015
batch start
#iterations: 392
currently lose_sum: 94.32596898078918
time_elpased: 1.939
batch start
#iterations: 393
currently lose_sum: 94.6027991771698
time_elpased: 2.013
batch start
#iterations: 394
currently lose_sum: 94.22753441333771
time_elpased: 1.951
batch start
#iterations: 395
currently lose_sum: 94.20378983020782
time_elpased: 2.036
batch start
#iterations: 396
currently lose_sum: 95.04639685153961
time_elpased: 1.966
batch start
#iterations: 397
currently lose_sum: 94.90175098180771
time_elpased: 1.994
batch start
#iterations: 398
currently lose_sum: 94.49071258306503
time_elpased: 1.967
batch start
#iterations: 399
currently lose_sum: 94.90031468868256
time_elpased: 1.956
start validation test
0.661134020619
0.637021536315
0.751878151693
0.689700745776
0.6609747053
61.080
acc: 0.664
pre: 0.633
rec: 0.783
F1: 0.700
auc: 0.664
