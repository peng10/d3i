start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.42035192251205
time_elpased: 2.089
batch start
#iterations: 1
currently lose_sum: 100.49082016944885
time_elpased: 2.03
batch start
#iterations: 2
currently lose_sum: 100.33861190080643
time_elpased: 2.021
batch start
#iterations: 3
currently lose_sum: 100.2632229924202
time_elpased: 1.986
batch start
#iterations: 4
currently lose_sum: 100.23294049501419
time_elpased: 2.029
batch start
#iterations: 5
currently lose_sum: 100.13533937931061
time_elpased: 2.02
batch start
#iterations: 6
currently lose_sum: 100.02877426147461
time_elpased: 1.988
batch start
#iterations: 7
currently lose_sum: 100.0542174577713
time_elpased: 1.994
batch start
#iterations: 8
currently lose_sum: 99.85159242153168
time_elpased: 2.085
batch start
#iterations: 9
currently lose_sum: 99.94878804683685
time_elpased: 1.995
batch start
#iterations: 10
currently lose_sum: 99.92488497495651
time_elpased: 1.991
batch start
#iterations: 11
currently lose_sum: 99.68421959877014
time_elpased: 2.002
batch start
#iterations: 12
currently lose_sum: 99.49828445911407
time_elpased: 1.986
batch start
#iterations: 13
currently lose_sum: 99.46993863582611
time_elpased: 1.996
batch start
#iterations: 14
currently lose_sum: 99.26437455415726
time_elpased: 1.976
batch start
#iterations: 15
currently lose_sum: 99.24719059467316
time_elpased: 2.014
batch start
#iterations: 16
currently lose_sum: 98.84609401226044
time_elpased: 2.002
batch start
#iterations: 17
currently lose_sum: 98.9021167755127
time_elpased: 2.002
batch start
#iterations: 18
currently lose_sum: 98.88001841306686
time_elpased: 2.01
batch start
#iterations: 19
currently lose_sum: 98.5781718492508
time_elpased: 1.987
start validation test
0.625154639175
0.619723827245
0.651229803437
0.635086310719
0.625108860203
64.449
batch start
#iterations: 20
currently lose_sum: 98.39683854579926
time_elpased: 2.033
batch start
#iterations: 21
currently lose_sum: 98.36621451377869
time_elpased: 2.047
batch start
#iterations: 22
currently lose_sum: 98.29751932621002
time_elpased: 1.961
batch start
#iterations: 23
currently lose_sum: 98.37643277645111
time_elpased: 2.014
batch start
#iterations: 24
currently lose_sum: 97.91546607017517
time_elpased: 2.007
batch start
#iterations: 25
currently lose_sum: 98.17300295829773
time_elpased: 1.99
batch start
#iterations: 26
currently lose_sum: 98.03234386444092
time_elpased: 1.994
batch start
#iterations: 27
currently lose_sum: 98.0739209651947
time_elpased: 2.032
batch start
#iterations: 28
currently lose_sum: 97.8342632651329
time_elpased: 1.981
batch start
#iterations: 29
currently lose_sum: 97.81451445817947
time_elpased: 1.99
batch start
#iterations: 30
currently lose_sum: 97.80095440149307
time_elpased: 2.008
batch start
#iterations: 31
currently lose_sum: 97.66804051399231
time_elpased: 2.02
batch start
#iterations: 32
currently lose_sum: 97.50869572162628
time_elpased: 2.042
batch start
#iterations: 33
currently lose_sum: 97.85590487718582
time_elpased: 2.0
batch start
#iterations: 34
currently lose_sum: 97.40754014253616
time_elpased: 2.019
batch start
#iterations: 35
currently lose_sum: 97.57090520858765
time_elpased: 2.032
batch start
#iterations: 36
currently lose_sum: 97.29204052686691
time_elpased: 1.992
batch start
#iterations: 37
currently lose_sum: 97.60187137126923
time_elpased: 1.995
batch start
#iterations: 38
currently lose_sum: 97.31038385629654
time_elpased: 1.981
batch start
#iterations: 39
currently lose_sum: 97.43002563714981
time_elpased: 1.995
start validation test
0.64381443299
0.622011649135
0.736338376042
0.674363807729
0.643651992937
63.024
batch start
#iterations: 40
currently lose_sum: 97.15927940607071
time_elpased: 2.019
batch start
#iterations: 41
currently lose_sum: 97.30850660800934
time_elpased: 1.994
batch start
#iterations: 42
currently lose_sum: 97.11478769779205
time_elpased: 1.98
batch start
#iterations: 43
currently lose_sum: 97.51059675216675
time_elpased: 2.007
batch start
#iterations: 44
currently lose_sum: 97.37397694587708
time_elpased: 2.019
batch start
#iterations: 45
currently lose_sum: 97.36883080005646
time_elpased: 2.009
batch start
#iterations: 46
currently lose_sum: 97.36240148544312
time_elpased: 2.014
batch start
#iterations: 47
currently lose_sum: 97.11413860321045
time_elpased: 2.031
batch start
#iterations: 48
currently lose_sum: 97.17050528526306
time_elpased: 2.003
batch start
#iterations: 49
currently lose_sum: 96.93169581890106
time_elpased: 2.004
batch start
#iterations: 50
currently lose_sum: 97.02818620204926
time_elpased: 1.995
batch start
#iterations: 51
currently lose_sum: 97.2205461859703
time_elpased: 2.003
batch start
#iterations: 52
currently lose_sum: 97.15680605173111
time_elpased: 2.057
batch start
#iterations: 53
currently lose_sum: 96.92435604333878
time_elpased: 2.003
batch start
#iterations: 54
currently lose_sum: 97.12491482496262
time_elpased: 1.974
batch start
#iterations: 55
currently lose_sum: 96.81424158811569
time_elpased: 1.987
batch start
#iterations: 56
currently lose_sum: 97.02281934022903
time_elpased: 1.993
batch start
#iterations: 57
currently lose_sum: 97.06795209646225
time_elpased: 2.026
batch start
#iterations: 58
currently lose_sum: 97.04825085401535
time_elpased: 1.986
batch start
#iterations: 59
currently lose_sum: 96.83731663227081
time_elpased: 2.055
start validation test
0.633762886598
0.63982869379
0.615004631059
0.627171118224
0.633795819609
62.394
batch start
#iterations: 60
currently lose_sum: 97.12885981798172
time_elpased: 2.023
batch start
#iterations: 61
currently lose_sum: 96.99010717868805
time_elpased: 1.978
batch start
#iterations: 62
currently lose_sum: 97.13774019479752
time_elpased: 1.987
batch start
#iterations: 63
currently lose_sum: 96.68556064367294
time_elpased: 1.982
batch start
#iterations: 64
currently lose_sum: 96.98364686965942
time_elpased: 2.035
batch start
#iterations: 65
currently lose_sum: 97.00444412231445
time_elpased: 2.008
batch start
#iterations: 66
currently lose_sum: 96.7325314283371
time_elpased: 1.993
batch start
#iterations: 67
currently lose_sum: 96.60538727045059
time_elpased: 1.989
batch start
#iterations: 68
currently lose_sum: 96.91454041004181
time_elpased: 1.981
batch start
#iterations: 69
currently lose_sum: 96.66932183504105
time_elpased: 1.966
batch start
#iterations: 70
currently lose_sum: 96.98471456766129
time_elpased: 1.972
batch start
#iterations: 71
currently lose_sum: 96.78761518001556
time_elpased: 2.033
batch start
#iterations: 72
currently lose_sum: 96.6697570681572
time_elpased: 2.033
batch start
#iterations: 73
currently lose_sum: 96.61438995599747
time_elpased: 2.003
batch start
#iterations: 74
currently lose_sum: 96.92464601993561
time_elpased: 2.016
batch start
#iterations: 75
currently lose_sum: 96.79192864894867
time_elpased: 2.007
batch start
#iterations: 76
currently lose_sum: 96.5665915608406
time_elpased: 2.018
batch start
#iterations: 77
currently lose_sum: 97.0335858464241
time_elpased: 2.025
batch start
#iterations: 78
currently lose_sum: 96.97518986463547
time_elpased: 2.039
batch start
#iterations: 79
currently lose_sum: 96.97274965047836
time_elpased: 2.07
start validation test
0.652989690722
0.62995211145
0.744571369764
0.682482784643
0.652828904959
61.903
batch start
#iterations: 80
currently lose_sum: 96.8543803691864
time_elpased: 2.012
batch start
#iterations: 81
currently lose_sum: 96.61020147800446
time_elpased: 1.99
batch start
#iterations: 82
currently lose_sum: 97.18363296985626
time_elpased: 2.025
batch start
#iterations: 83
currently lose_sum: 96.60598075389862
time_elpased: 1.995
batch start
#iterations: 84
currently lose_sum: 96.59453320503235
time_elpased: 1.98
batch start
#iterations: 85
currently lose_sum: 96.55300009250641
time_elpased: 1.982
batch start
#iterations: 86
currently lose_sum: 96.69798719882965
time_elpased: 2.028
batch start
#iterations: 87
currently lose_sum: 96.95141553878784
time_elpased: 1.985
batch start
#iterations: 88
currently lose_sum: 96.5680302977562
time_elpased: 2.028
batch start
#iterations: 89
currently lose_sum: 96.48063957691193
time_elpased: 2.052
batch start
#iterations: 90
currently lose_sum: 96.61150795221329
time_elpased: 1.987
batch start
#iterations: 91
currently lose_sum: 96.78592693805695
time_elpased: 1.994
batch start
#iterations: 92
currently lose_sum: 96.71810299158096
time_elpased: 1.989
batch start
#iterations: 93
currently lose_sum: 96.36097967624664
time_elpased: 1.981
batch start
#iterations: 94
currently lose_sum: 96.71977353096008
time_elpased: 1.969
batch start
#iterations: 95
currently lose_sum: 96.2619097828865
time_elpased: 1.993
batch start
#iterations: 96
currently lose_sum: 96.55912339687347
time_elpased: 2.027
batch start
#iterations: 97
currently lose_sum: 96.43980073928833
time_elpased: 2.004
batch start
#iterations: 98
currently lose_sum: 96.77941358089447
time_elpased: 2.055
batch start
#iterations: 99
currently lose_sum: 96.49964445829391
time_elpased: 1.992
start validation test
0.654896907216
0.621933505487
0.793146032726
0.697182143019
0.654654189553
61.589
batch start
#iterations: 100
currently lose_sum: 96.82899570465088
time_elpased: 2.026
batch start
#iterations: 101
currently lose_sum: 96.20501506328583
time_elpased: 1.989
batch start
#iterations: 102
currently lose_sum: 96.75689643621445
time_elpased: 2.024
batch start
#iterations: 103
currently lose_sum: 96.27193957567215
time_elpased: 1.988
batch start
#iterations: 104
currently lose_sum: 96.54167133569717
time_elpased: 2.052
batch start
#iterations: 105
currently lose_sum: 96.39971262216568
time_elpased: 1.991
batch start
#iterations: 106
currently lose_sum: 96.62214767932892
time_elpased: 2.022
batch start
#iterations: 107
currently lose_sum: 96.79428333044052
time_elpased: 1.992
batch start
#iterations: 108
currently lose_sum: 96.19555521011353
time_elpased: 2.004
batch start
#iterations: 109
currently lose_sum: 96.40221589803696
time_elpased: 1.961
batch start
#iterations: 110
currently lose_sum: 96.28768116235733
time_elpased: 2.003
batch start
#iterations: 111
currently lose_sum: 95.83086556196213
time_elpased: 2.065
batch start
#iterations: 112
currently lose_sum: 96.2326790690422
time_elpased: 2.004
batch start
#iterations: 113
currently lose_sum: 96.50740057229996
time_elpased: 1.988
batch start
#iterations: 114
currently lose_sum: 95.9124693274498
time_elpased: 1.981
batch start
#iterations: 115
currently lose_sum: 96.30362296104431
time_elpased: 2.019
batch start
#iterations: 116
currently lose_sum: 95.84199643135071
time_elpased: 1.988
batch start
#iterations: 117
currently lose_sum: 96.3087460398674
time_elpased: 1.969
batch start
#iterations: 118
currently lose_sum: 96.04972422122955
time_elpased: 2.04
batch start
#iterations: 119
currently lose_sum: 96.13805782794952
time_elpased: 1.989
start validation test
0.658969072165
0.62424873788
0.801687763713
0.701928275365
0.658718507489
61.498
batch start
#iterations: 120
currently lose_sum: 95.97061800956726
time_elpased: 1.996
batch start
#iterations: 121
currently lose_sum: 96.10052317380905
time_elpased: 1.992
batch start
#iterations: 122
currently lose_sum: 96.33292472362518
time_elpased: 1.97
batch start
#iterations: 123
currently lose_sum: 96.10614204406738
time_elpased: 2.019
batch start
#iterations: 124
currently lose_sum: 96.54160964488983
time_elpased: 1.985
batch start
#iterations: 125
currently lose_sum: 96.60966891050339
time_elpased: 2.038
batch start
#iterations: 126
currently lose_sum: 96.31737524271011
time_elpased: 1.992
batch start
#iterations: 127
currently lose_sum: 96.42191284894943
time_elpased: 2.013
batch start
#iterations: 128
currently lose_sum: 95.90171700716019
time_elpased: 2.005
batch start
#iterations: 129
currently lose_sum: 96.67337328195572
time_elpased: 1.976
batch start
#iterations: 130
currently lose_sum: 96.10686719417572
time_elpased: 2.036
batch start
#iterations: 131
currently lose_sum: 95.98386913537979
time_elpased: 2.006
batch start
#iterations: 132
currently lose_sum: 96.1719451546669
time_elpased: 2.015
batch start
#iterations: 133
currently lose_sum: 96.39178401231766
time_elpased: 1.983
batch start
#iterations: 134
currently lose_sum: 96.20519816875458
time_elpased: 1.98
batch start
#iterations: 135
currently lose_sum: 96.1231569647789
time_elpased: 1.991
batch start
#iterations: 136
currently lose_sum: 96.01361805200577
time_elpased: 1.998
batch start
#iterations: 137
currently lose_sum: 95.78858828544617
time_elpased: 1.983
batch start
#iterations: 138
currently lose_sum: 96.49617248773575
time_elpased: 1.984
batch start
#iterations: 139
currently lose_sum: 95.90621739625931
time_elpased: 2.024
start validation test
0.660257731959
0.631322466812
0.773283935371
0.695129284426
0.660059297026
61.108
batch start
#iterations: 140
currently lose_sum: 95.92496109008789
time_elpased: 2.019
batch start
#iterations: 141
currently lose_sum: 96.56530231237411
time_elpased: 1.959
batch start
#iterations: 142
currently lose_sum: 96.17744815349579
time_elpased: 1.992
batch start
#iterations: 143
currently lose_sum: 96.18930399417877
time_elpased: 1.989
batch start
#iterations: 144
currently lose_sum: 96.07231020927429
time_elpased: 2.018
batch start
#iterations: 145
currently lose_sum: 95.7629611492157
time_elpased: 2.021
batch start
#iterations: 146
currently lose_sum: 95.98699516057968
time_elpased: 1.992
batch start
#iterations: 147
currently lose_sum: 95.77984482049942
time_elpased: 2.003
batch start
#iterations: 148
currently lose_sum: 95.97971713542938
time_elpased: 1.973
batch start
#iterations: 149
currently lose_sum: 95.97025936841965
time_elpased: 1.999
batch start
#iterations: 150
currently lose_sum: 96.30113750696182
time_elpased: 2.002
batch start
#iterations: 151
currently lose_sum: 95.84073483943939
time_elpased: 2.018
batch start
#iterations: 152
currently lose_sum: 95.90306347608566
time_elpased: 1.995
batch start
#iterations: 153
currently lose_sum: 95.85779535770416
time_elpased: 1.982
batch start
#iterations: 154
currently lose_sum: 95.95486462116241
time_elpased: 1.976
batch start
#iterations: 155
currently lose_sum: 95.83963000774384
time_elpased: 1.979
batch start
#iterations: 156
currently lose_sum: 95.83072477579117
time_elpased: 1.986
batch start
#iterations: 157
currently lose_sum: 95.54323160648346
time_elpased: 2.0
batch start
#iterations: 158
currently lose_sum: 95.9010181427002
time_elpased: 1.991
batch start
#iterations: 159
currently lose_sum: 96.12153816223145
time_elpased: 2.053
start validation test
0.655515463918
0.647453343701
0.685499639807
0.665933516621
0.655462822072
61.623
batch start
#iterations: 160
currently lose_sum: 95.51229393482208
time_elpased: 2.114
batch start
#iterations: 161
currently lose_sum: 96.12993198633194
time_elpased: 2.009
batch start
#iterations: 162
currently lose_sum: 95.4250019788742
time_elpased: 2.026
batch start
#iterations: 163
currently lose_sum: 96.12054347991943
time_elpased: 2.019
batch start
#iterations: 164
currently lose_sum: 95.62355101108551
time_elpased: 2.018
batch start
#iterations: 165
currently lose_sum: 95.90795266628265
time_elpased: 1.977
batch start
#iterations: 166
currently lose_sum: 95.83598589897156
time_elpased: 2.001
batch start
#iterations: 167
currently lose_sum: 95.77418798208237
time_elpased: 2.057
batch start
#iterations: 168
currently lose_sum: 95.59511256217957
time_elpased: 2.021
batch start
#iterations: 169
currently lose_sum: 95.9204152226448
time_elpased: 2.023
batch start
#iterations: 170
currently lose_sum: 96.03155422210693
time_elpased: 2.041
batch start
#iterations: 171
currently lose_sum: 96.17038333415985
time_elpased: 2.011
batch start
#iterations: 172
currently lose_sum: 95.83486330509186
time_elpased: 2.003
batch start
#iterations: 173
currently lose_sum: 95.88112604618073
time_elpased: 2.077
batch start
#iterations: 174
currently lose_sum: 95.45359241962433
time_elpased: 2.019
batch start
#iterations: 175
currently lose_sum: 95.67064434289932
time_elpased: 2.018
batch start
#iterations: 176
currently lose_sum: 95.52002942562103
time_elpased: 2.077
batch start
#iterations: 177
currently lose_sum: 95.35199809074402
time_elpased: 2.003
batch start
#iterations: 178
currently lose_sum: 96.14874786138535
time_elpased: 2.036
batch start
#iterations: 179
currently lose_sum: 95.93905240297318
time_elpased: 1.999
start validation test
0.657577319588
0.633698677801
0.749716990841
0.686842973648
0.657415554183
61.084
batch start
#iterations: 180
currently lose_sum: 95.43254655599594
time_elpased: 2.065
batch start
#iterations: 181
currently lose_sum: 95.56151854991913
time_elpased: 2.042
batch start
#iterations: 182
currently lose_sum: 95.6908820271492
time_elpased: 2.009
batch start
#iterations: 183
currently lose_sum: 95.42047905921936
time_elpased: 2.013
batch start
#iterations: 184
currently lose_sum: 96.4660713672638
time_elpased: 2.026
batch start
#iterations: 185
currently lose_sum: 95.96159195899963
time_elpased: 2.023
batch start
#iterations: 186
currently lose_sum: 96.03160035610199
time_elpased: 2.023
batch start
#iterations: 187
currently lose_sum: 95.71395790576935
time_elpased: 2.004
batch start
#iterations: 188
currently lose_sum: 95.72615367174149
time_elpased: 2.018
batch start
#iterations: 189
currently lose_sum: 95.71908754110336
time_elpased: 2.057
batch start
#iterations: 190
currently lose_sum: 95.69105786085129
time_elpased: 2.073
batch start
#iterations: 191
currently lose_sum: 95.35199469327927
time_elpased: 1.988
batch start
#iterations: 192
currently lose_sum: 95.57074493169785
time_elpased: 2.022
batch start
#iterations: 193
currently lose_sum: 95.72011184692383
time_elpased: 1.99
batch start
#iterations: 194
currently lose_sum: 95.69464939832687
time_elpased: 2.047
batch start
#iterations: 195
currently lose_sum: 95.90871548652649
time_elpased: 2.03
batch start
#iterations: 196
currently lose_sum: 95.62117964029312
time_elpased: 2.006
batch start
#iterations: 197
currently lose_sum: 95.38343632221222
time_elpased: 2.009
batch start
#iterations: 198
currently lose_sum: 95.56931501626968
time_elpased: 2.025
batch start
#iterations: 199
currently lose_sum: 95.61750280857086
time_elpased: 1.973
start validation test
0.647731958763
0.645268567973
0.658948235052
0.652036659878
0.64771226686
61.322
batch start
#iterations: 200
currently lose_sum: 95.68702006340027
time_elpased: 2.07
batch start
#iterations: 201
currently lose_sum: 95.48501652479172
time_elpased: 2.031
batch start
#iterations: 202
currently lose_sum: 95.57879251241684
time_elpased: 2.004
batch start
#iterations: 203
currently lose_sum: 95.51932644844055
time_elpased: 2.002
batch start
#iterations: 204
currently lose_sum: 95.52456951141357
time_elpased: 1.985
batch start
#iterations: 205
currently lose_sum: 95.74542266130447
time_elpased: 2.017
batch start
#iterations: 206
currently lose_sum: 95.53600931167603
time_elpased: 2.035
batch start
#iterations: 207
currently lose_sum: 95.7535982131958
time_elpased: 2.029
batch start
#iterations: 208
currently lose_sum: 95.30114382505417
time_elpased: 2.003
batch start
#iterations: 209
currently lose_sum: 95.4941640496254
time_elpased: 2.043
batch start
#iterations: 210
currently lose_sum: 95.69108533859253
time_elpased: 2.003
batch start
#iterations: 211
currently lose_sum: 95.66071802377701
time_elpased: 2.021
batch start
#iterations: 212
currently lose_sum: 95.84912759065628
time_elpased: 2.045
batch start
#iterations: 213
currently lose_sum: 95.59587651491165
time_elpased: 1.992
batch start
#iterations: 214
currently lose_sum: 95.46968734264374
time_elpased: 1.987
batch start
#iterations: 215
currently lose_sum: 95.5091952085495
time_elpased: 1.987
batch start
#iterations: 216
currently lose_sum: 95.34920245409012
time_elpased: 2.024
batch start
#iterations: 217
currently lose_sum: 95.62370365858078
time_elpased: 1.999
batch start
#iterations: 218
currently lose_sum: 95.50360333919525
time_elpased: 2.043
batch start
#iterations: 219
currently lose_sum: 95.70394951105118
time_elpased: 2.091
start validation test
0.660051546392
0.641805959302
0.727076258104
0.681785283474
0.659933874172
61.236
batch start
#iterations: 220
currently lose_sum: 95.57721471786499
time_elpased: 2.095
batch start
#iterations: 221
currently lose_sum: 95.34775030612946
time_elpased: 2.009
batch start
#iterations: 222
currently lose_sum: 95.1005425453186
time_elpased: 1.968
batch start
#iterations: 223
currently lose_sum: 95.54212886095047
time_elpased: 2.009
batch start
#iterations: 224
currently lose_sum: 95.31280344724655
time_elpased: 2.048
batch start
#iterations: 225
currently lose_sum: 95.95808988809586
time_elpased: 2.002
batch start
#iterations: 226
currently lose_sum: 95.48904925584793
time_elpased: 2.029
batch start
#iterations: 227
currently lose_sum: 95.26063042879105
time_elpased: 2.04
batch start
#iterations: 228
currently lose_sum: 95.80216079950333
time_elpased: 2.101
batch start
#iterations: 229
currently lose_sum: 95.66850918531418
time_elpased: 2.106
batch start
#iterations: 230
currently lose_sum: 95.15714997053146
time_elpased: 2.005
batch start
#iterations: 231
currently lose_sum: 95.26317191123962
time_elpased: 2.017
batch start
#iterations: 232
currently lose_sum: 95.29981964826584
time_elpased: 2.017
batch start
#iterations: 233
currently lose_sum: 95.60136026144028
time_elpased: 1.998
batch start
#iterations: 234
currently lose_sum: 95.29101777076721
time_elpased: 1.966
batch start
#iterations: 235
currently lose_sum: 95.64170783758163
time_elpased: 1.996
batch start
#iterations: 236
currently lose_sum: 95.54154849052429
time_elpased: 2.04
batch start
#iterations: 237
currently lose_sum: 95.46578598022461
time_elpased: 1.99
batch start
#iterations: 238
currently lose_sum: 95.18587189912796
time_elpased: 1.995
batch start
#iterations: 239
currently lose_sum: 95.66722446680069
time_elpased: 2.014
start validation test
0.62175257732
0.659300924066
0.506637851189
0.572974860335
0.621954678977
62.001
batch start
#iterations: 240
currently lose_sum: 95.35902148485184
time_elpased: 2.016
batch start
#iterations: 241
currently lose_sum: 95.62270534038544
time_elpased: 1.994
batch start
#iterations: 242
currently lose_sum: 95.41140931844711
time_elpased: 2.056
batch start
#iterations: 243
currently lose_sum: 95.1231215596199
time_elpased: 2.035
batch start
#iterations: 244
currently lose_sum: 95.69645941257477
time_elpased: 2.022
batch start
#iterations: 245
currently lose_sum: 95.2367033958435
time_elpased: 2.018
batch start
#iterations: 246
currently lose_sum: 95.3248496055603
time_elpased: 2.026
batch start
#iterations: 247
currently lose_sum: 95.26097226142883
time_elpased: 2.004
batch start
#iterations: 248
currently lose_sum: 95.16002309322357
time_elpased: 1.984
batch start
#iterations: 249
currently lose_sum: 95.42192608118057
time_elpased: 2.048
batch start
#iterations: 250
currently lose_sum: 95.29007613658905
time_elpased: 2.009
batch start
#iterations: 251
currently lose_sum: 95.20436197519302
time_elpased: 2.063
batch start
#iterations: 252
currently lose_sum: 95.30783247947693
time_elpased: 2.034
batch start
#iterations: 253
currently lose_sum: 95.42518043518066
time_elpased: 2.025
batch start
#iterations: 254
currently lose_sum: 95.17599511146545
time_elpased: 2.05
batch start
#iterations: 255
currently lose_sum: 95.33243453502655
time_elpased: 1.983
batch start
#iterations: 256
currently lose_sum: 95.38356137275696
time_elpased: 2.028
batch start
#iterations: 257
currently lose_sum: 95.091228723526
time_elpased: 2.035
batch start
#iterations: 258
currently lose_sum: 95.12137562036514
time_elpased: 1.989
batch start
#iterations: 259
currently lose_sum: 95.15970236063004
time_elpased: 1.994
start validation test
0.660051546392
0.619379014989
0.833487701966
0.710656780591
0.65974705247
60.806
batch start
#iterations: 260
currently lose_sum: 95.53082007169724
time_elpased: 1.978
batch start
#iterations: 261
currently lose_sum: 95.29106992483139
time_elpased: 1.992
batch start
#iterations: 262
currently lose_sum: 95.36374056339264
time_elpased: 2.041
batch start
#iterations: 263
currently lose_sum: 95.61392575502396
time_elpased: 2.043
batch start
#iterations: 264
currently lose_sum: 95.26369643211365
time_elpased: 1.996
batch start
#iterations: 265
currently lose_sum: 95.35575813055038
time_elpased: 2.028
batch start
#iterations: 266
currently lose_sum: 95.1610347032547
time_elpased: 2.019
batch start
#iterations: 267
currently lose_sum: 95.20372039079666
time_elpased: 1.982
batch start
#iterations: 268
currently lose_sum: 95.29192847013474
time_elpased: 1.965
batch start
#iterations: 269
currently lose_sum: 95.57363349199295
time_elpased: 2.025
batch start
#iterations: 270
currently lose_sum: 95.14010310173035
time_elpased: 2.004
batch start
#iterations: 271
currently lose_sum: 95.0064395070076
time_elpased: 2.011
batch start
#iterations: 272
currently lose_sum: 95.09001243114471
time_elpased: 2.072
batch start
#iterations: 273
currently lose_sum: 95.02946150302887
time_elpased: 2.002
batch start
#iterations: 274
currently lose_sum: 95.65609973669052
time_elpased: 2.01
batch start
#iterations: 275
currently lose_sum: 95.41155850887299
time_elpased: 2.035
batch start
#iterations: 276
currently lose_sum: 95.27415245771408
time_elpased: 2.004
batch start
#iterations: 277
currently lose_sum: 95.20074319839478
time_elpased: 2.0
batch start
#iterations: 278
currently lose_sum: 94.84491616487503
time_elpased: 1.998
batch start
#iterations: 279
currently lose_sum: 94.92692905664444
time_elpased: 2.01
start validation test
0.660515463918
0.645994591066
0.712874343933
0.677788649706
0.660423539828
60.952
batch start
#iterations: 280
currently lose_sum: 95.51144188642502
time_elpased: 2.037
batch start
#iterations: 281
currently lose_sum: 95.20821422338486
time_elpased: 2.0
batch start
#iterations: 282
currently lose_sum: 95.08756971359253
time_elpased: 2.048
batch start
#iterations: 283
currently lose_sum: 95.41058403253555
time_elpased: 2.015
batch start
#iterations: 284
currently lose_sum: 95.27310037612915
time_elpased: 2.046
batch start
#iterations: 285
currently lose_sum: 95.1587103009224
time_elpased: 2.038
batch start
#iterations: 286
currently lose_sum: 95.2802078127861
time_elpased: 1.999
batch start
#iterations: 287
currently lose_sum: 95.14494377374649
time_elpased: 2.005
batch start
#iterations: 288
currently lose_sum: 94.99065953493118
time_elpased: 2.008
batch start
#iterations: 289
currently lose_sum: 94.95470088720322
time_elpased: 2.08
batch start
#iterations: 290
currently lose_sum: 94.8055459856987
time_elpased: 2.031
batch start
#iterations: 291
currently lose_sum: 95.1721179485321
time_elpased: 1.994
batch start
#iterations: 292
currently lose_sum: 95.69334083795547
time_elpased: 2.009
batch start
#iterations: 293
currently lose_sum: 95.19897389411926
time_elpased: 1.994
batch start
#iterations: 294
currently lose_sum: 95.2397108078003
time_elpased: 2.024
batch start
#iterations: 295
currently lose_sum: 95.33261317014694
time_elpased: 2.048
batch start
#iterations: 296
currently lose_sum: 95.18728142976761
time_elpased: 2.013
batch start
#iterations: 297
currently lose_sum: 95.23089009523392
time_elpased: 2.035
batch start
#iterations: 298
currently lose_sum: 95.36207562685013
time_elpased: 1.972
batch start
#iterations: 299
currently lose_sum: 95.26378095149994
time_elpased: 1.99
start validation test
0.660309278351
0.616600790514
0.850879901204
0.71503934965
0.659974702229
60.857
batch start
#iterations: 300
currently lose_sum: 94.96618700027466
time_elpased: 1.988
batch start
#iterations: 301
currently lose_sum: 95.36417132616043
time_elpased: 2.039
batch start
#iterations: 302
currently lose_sum: 95.12754762172699
time_elpased: 2.036
batch start
#iterations: 303
currently lose_sum: 95.3825991153717
time_elpased: 2.02
batch start
#iterations: 304
currently lose_sum: 94.80958676338196
time_elpased: 2.003
batch start
#iterations: 305
currently lose_sum: 94.84095484018326
time_elpased: 2.03
batch start
#iterations: 306
currently lose_sum: 94.80833315849304
time_elpased: 2.05
batch start
#iterations: 307
currently lose_sum: 95.23780339956284
time_elpased: 2.048
batch start
#iterations: 308
currently lose_sum: 95.14938032627106
time_elpased: 1.996
batch start
#iterations: 309
currently lose_sum: 95.02608668804169
time_elpased: 2.008
batch start
#iterations: 310
currently lose_sum: 95.3644552230835
time_elpased: 1.999
batch start
#iterations: 311
currently lose_sum: 94.80263614654541
time_elpased: 1.991
batch start
#iterations: 312
currently lose_sum: 94.84070187807083
time_elpased: 2.013
batch start
#iterations: 313
currently lose_sum: 94.8679209947586
time_elpased: 1.978
batch start
#iterations: 314
currently lose_sum: 95.04860007762909
time_elpased: 2.111
batch start
#iterations: 315
currently lose_sum: 94.7770603299141
time_elpased: 2.094
batch start
#iterations: 316
currently lose_sum: 95.19222724437714
time_elpased: 2.031
batch start
#iterations: 317
currently lose_sum: 94.8675525188446
time_elpased: 2.039
batch start
#iterations: 318
currently lose_sum: 95.1569795012474
time_elpased: 2.042
batch start
#iterations: 319
currently lose_sum: 95.50650197267532
time_elpased: 2.059
start validation test
0.661391752577
0.631166666667
0.779458680663
0.697518073399
0.661184467874
60.678
batch start
#iterations: 320
currently lose_sum: 94.68007129430771
time_elpased: 2.039
batch start
#iterations: 321
currently lose_sum: 95.3164199590683
time_elpased: 1.998
batch start
#iterations: 322
currently lose_sum: 95.0129245519638
time_elpased: 1.989
batch start
#iterations: 323
currently lose_sum: 95.03200423717499
time_elpased: 1.994
batch start
#iterations: 324
currently lose_sum: 95.34404695034027
time_elpased: 1.984
batch start
#iterations: 325
currently lose_sum: 95.08738416433334
time_elpased: 2.047
batch start
#iterations: 326
currently lose_sum: 94.97299021482468
time_elpased: 2.012
batch start
#iterations: 327
currently lose_sum: 94.94295620918274
time_elpased: 1.969
batch start
#iterations: 328
currently lose_sum: 95.34862869977951
time_elpased: 1.989
batch start
#iterations: 329
currently lose_sum: 94.60145407915115
time_elpased: 1.968
batch start
#iterations: 330
currently lose_sum: 95.08187687397003
time_elpased: 2.0
batch start
#iterations: 331
currently lose_sum: 94.97279453277588
time_elpased: 2.005
batch start
#iterations: 332
currently lose_sum: 94.4777330160141
time_elpased: 2.004
batch start
#iterations: 333
currently lose_sum: 95.03172594308853
time_elpased: 1.992
batch start
#iterations: 334
currently lose_sum: 95.23702085018158
time_elpased: 2.025
batch start
#iterations: 335
currently lose_sum: 94.71276408433914
time_elpased: 2.013
batch start
#iterations: 336
currently lose_sum: 95.0194720029831
time_elpased: 2.017
batch start
#iterations: 337
currently lose_sum: 95.45556497573853
time_elpased: 2.007
batch start
#iterations: 338
currently lose_sum: 94.96283316612244
time_elpased: 2.002
batch start
#iterations: 339
currently lose_sum: 95.14438647031784
time_elpased: 2.007
start validation test
0.664175257732
0.62950978806
0.800864464341
0.704923230219
0.663935278747
60.621
batch start
#iterations: 340
currently lose_sum: 95.22114342451096
time_elpased: 1.999
batch start
#iterations: 341
currently lose_sum: 94.79000103473663
time_elpased: 1.989
batch start
#iterations: 342
currently lose_sum: 95.04594230651855
time_elpased: 2.022
batch start
#iterations: 343
currently lose_sum: 95.38424950838089
time_elpased: 2.003
batch start
#iterations: 344
currently lose_sum: 94.7401437163353
time_elpased: 1.984
batch start
#iterations: 345
currently lose_sum: 95.27132391929626
time_elpased: 2.001
batch start
#iterations: 346
currently lose_sum: 94.62985283136368
time_elpased: 1.982
batch start
#iterations: 347
currently lose_sum: 95.35636776685715
time_elpased: 2.038
batch start
#iterations: 348
currently lose_sum: 95.08675843477249
time_elpased: 2.001
batch start
#iterations: 349
currently lose_sum: 95.15091979503632
time_elpased: 1.994
batch start
#iterations: 350
currently lose_sum: 95.1690861582756
time_elpased: 2.005
batch start
#iterations: 351
currently lose_sum: 95.06205022335052
time_elpased: 1.986
batch start
#iterations: 352
currently lose_sum: 95.06467008590698
time_elpased: 1.999
batch start
#iterations: 353
currently lose_sum: 94.91913360357285
time_elpased: 1.975
batch start
#iterations: 354
currently lose_sum: 95.57820212841034
time_elpased: 2.03
batch start
#iterations: 355
currently lose_sum: 94.91484731435776
time_elpased: 1.987
batch start
#iterations: 356
currently lose_sum: 95.17753386497498
time_elpased: 1.968
batch start
#iterations: 357
currently lose_sum: 95.07803010940552
time_elpased: 1.981
batch start
#iterations: 358
currently lose_sum: 95.15995770692825
time_elpased: 2.079
batch start
#iterations: 359
currently lose_sum: 95.1196009516716
time_elpased: 2.012
start validation test
0.661030927835
0.637847801281
0.747864567253
0.688488867835
0.660878477988
60.785
batch start
#iterations: 360
currently lose_sum: 94.86027181148529
time_elpased: 2.081
batch start
#iterations: 361
currently lose_sum: 95.02327281236649
time_elpased: 2.067
batch start
#iterations: 362
currently lose_sum: 95.15964359045029
time_elpased: 2.072
batch start
#iterations: 363
currently lose_sum: 95.04071944952011
time_elpased: 2.003
batch start
#iterations: 364
currently lose_sum: 94.98798507452011
time_elpased: 2.069
batch start
#iterations: 365
currently lose_sum: 94.97315901517868
time_elpased: 2.034
batch start
#iterations: 366
currently lose_sum: 95.38539069890976
time_elpased: 2.035
batch start
#iterations: 367
currently lose_sum: 94.93877398967743
time_elpased: 2.074
batch start
#iterations: 368
currently lose_sum: 95.11058533191681
time_elpased: 2.039
batch start
#iterations: 369
currently lose_sum: 95.14651852846146
time_elpased: 2.021
batch start
#iterations: 370
currently lose_sum: 94.63671320676804
time_elpased: 2.016
batch start
#iterations: 371
currently lose_sum: 94.76539474725723
time_elpased: 2.043
batch start
#iterations: 372
currently lose_sum: 95.00013172626495
time_elpased: 2.032
batch start
#iterations: 373
currently lose_sum: 94.76409435272217
time_elpased: 2.071
batch start
#iterations: 374
currently lose_sum: 95.38047808408737
time_elpased: 2.021
batch start
#iterations: 375
currently lose_sum: 95.57261383533478
time_elpased: 2.052
batch start
#iterations: 376
currently lose_sum: 94.75173085927963
time_elpased: 2.113
batch start
#iterations: 377
currently lose_sum: 95.19240647554398
time_elpased: 2.055
batch start
#iterations: 378
currently lose_sum: 95.05164158344269
time_elpased: 2.069
batch start
#iterations: 379
currently lose_sum: 95.00242918729782
time_elpased: 2.056
start validation test
0.640721649485
0.649602439821
0.613769682001
0.631177902424
0.640768967821
61.588
batch start
#iterations: 380
currently lose_sum: 95.47491806745529
time_elpased: 2.015
batch start
#iterations: 381
currently lose_sum: 94.80315274000168
time_elpased: 2.121
batch start
#iterations: 382
currently lose_sum: 95.09708720445633
time_elpased: 2.089
batch start
#iterations: 383
currently lose_sum: 95.24473941326141
time_elpased: 2.019
batch start
#iterations: 384
currently lose_sum: 95.12036329507828
time_elpased: 2.049
batch start
#iterations: 385
currently lose_sum: 95.12344163656235
time_elpased: 2.023
batch start
#iterations: 386
currently lose_sum: 95.23362505435944
time_elpased: 2.054
batch start
#iterations: 387
currently lose_sum: 95.32845741510391
time_elpased: 2.011
batch start
#iterations: 388
currently lose_sum: 94.99879002571106
time_elpased: 2.028
batch start
#iterations: 389
currently lose_sum: 95.31847792863846
time_elpased: 1.976
batch start
#iterations: 390
currently lose_sum: 94.7578741312027
time_elpased: 2.042
batch start
#iterations: 391
currently lose_sum: 94.81191807985306
time_elpased: 1.984
batch start
#iterations: 392
currently lose_sum: 94.6864647269249
time_elpased: 2.065
batch start
#iterations: 393
currently lose_sum: 94.79640251398087
time_elpased: 2.057
batch start
#iterations: 394
currently lose_sum: 95.12032115459442
time_elpased: 2.013
batch start
#iterations: 395
currently lose_sum: 94.98805296421051
time_elpased: 1.989
batch start
#iterations: 396
currently lose_sum: 94.47039115428925
time_elpased: 2.014
batch start
#iterations: 397
currently lose_sum: 95.11347824335098
time_elpased: 2.049
batch start
#iterations: 398
currently lose_sum: 94.69316548109055
time_elpased: 1.979
batch start
#iterations: 399
currently lose_sum: 95.3246146440506
time_elpased: 1.982
start validation test
0.655773195876
0.634433336282
0.737984974786
0.682302568982
0.655628860418
61.023
acc: 0.658
pre: 0.626
rec: 0.789
F1: 0.698
auc: 0.658
