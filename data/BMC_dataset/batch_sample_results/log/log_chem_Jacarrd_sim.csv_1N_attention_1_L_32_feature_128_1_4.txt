start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 101.38379281759262
time_elpased: 2.204
batch start
#iterations: 1
currently lose_sum: 100.44250571727753
time_elpased: 2.028
batch start
#iterations: 2
currently lose_sum: 100.41755622625351
time_elpased: 1.996
batch start
#iterations: 3
currently lose_sum: 100.2127086520195
time_elpased: 1.99
batch start
#iterations: 4
currently lose_sum: 100.27741515636444
time_elpased: 2.02
batch start
#iterations: 5
currently lose_sum: 100.24054890871048
time_elpased: 1.994
batch start
#iterations: 6
currently lose_sum: 100.18763035535812
time_elpased: 1.985
batch start
#iterations: 7
currently lose_sum: 100.0290635228157
time_elpased: 2.013
batch start
#iterations: 8
currently lose_sum: 99.93339145183563
time_elpased: 1.984
batch start
#iterations: 9
currently lose_sum: 99.98198342323303
time_elpased: 2.021
batch start
#iterations: 10
currently lose_sum: 99.67698800563812
time_elpased: 2.002
batch start
#iterations: 11
currently lose_sum: 99.6758439540863
time_elpased: 2.055
batch start
#iterations: 12
currently lose_sum: 99.50979369878769
time_elpased: 1.997
batch start
#iterations: 13
currently lose_sum: 99.6117884516716
time_elpased: 1.986
batch start
#iterations: 14
currently lose_sum: 99.28072667121887
time_elpased: 1.982
batch start
#iterations: 15
currently lose_sum: 99.30924546718597
time_elpased: 2.0
batch start
#iterations: 16
currently lose_sum: 99.01268792152405
time_elpased: 1.994
batch start
#iterations: 17
currently lose_sum: 98.77256083488464
time_elpased: 1.997
batch start
#iterations: 18
currently lose_sum: 98.67039531469345
time_elpased: 1.992
batch start
#iterations: 19
currently lose_sum: 98.983871281147
time_elpased: 1.997
start validation test
0.557989690722
0.649790136411
0.254914068128
0.366176361889
0.558521786723
65.439
batch start
#iterations: 20
currently lose_sum: 99.1310151219368
time_elpased: 2.055
batch start
#iterations: 21
currently lose_sum: 98.30819469690323
time_elpased: 1.98
batch start
#iterations: 22
currently lose_sum: 98.2241240143776
time_elpased: 1.984
batch start
#iterations: 23
currently lose_sum: 98.60980117321014
time_elpased: 2.025
batch start
#iterations: 24
currently lose_sum: 98.41731745004654
time_elpased: 2.01
batch start
#iterations: 25
currently lose_sum: 98.15012395381927
time_elpased: 1.988
batch start
#iterations: 26
currently lose_sum: 98.43121361732483
time_elpased: 2.009
batch start
#iterations: 27
currently lose_sum: 97.6554908156395
time_elpased: 2.013
batch start
#iterations: 28
currently lose_sum: 98.24167573451996
time_elpased: 1.998
batch start
#iterations: 29
currently lose_sum: 97.80175775289536
time_elpased: 1.995
batch start
#iterations: 30
currently lose_sum: 97.78976547718048
time_elpased: 1.977
batch start
#iterations: 31
currently lose_sum: 97.97003054618835
time_elpased: 2.039
batch start
#iterations: 32
currently lose_sum: 97.86460608243942
time_elpased: 2.009
batch start
#iterations: 33
currently lose_sum: 97.67809957265854
time_elpased: 2.021
batch start
#iterations: 34
currently lose_sum: 97.75586169958115
time_elpased: 2.006
batch start
#iterations: 35
currently lose_sum: 97.31374496221542
time_elpased: 2.0
batch start
#iterations: 36
currently lose_sum: 97.50700867176056
time_elpased: 1.998
batch start
#iterations: 37
currently lose_sum: 97.29034173488617
time_elpased: 1.981
batch start
#iterations: 38
currently lose_sum: 97.73405027389526
time_elpased: 2.024
batch start
#iterations: 39
currently lose_sum: 96.93620365858078
time_elpased: 2.009
start validation test
0.647319587629
0.617529229008
0.777297519811
0.688263167487
0.647091391321
62.613
batch start
#iterations: 40
currently lose_sum: 97.4517394900322
time_elpased: 2.084
batch start
#iterations: 41
currently lose_sum: 97.22005039453506
time_elpased: 2.019
batch start
#iterations: 42
currently lose_sum: 97.19916033744812
time_elpased: 2.027
batch start
#iterations: 43
currently lose_sum: 97.32152527570724
time_elpased: 2.007
batch start
#iterations: 44
currently lose_sum: 97.08255404233932
time_elpased: 2.028
batch start
#iterations: 45
currently lose_sum: 97.31729358434677
time_elpased: 2.002
batch start
#iterations: 46
currently lose_sum: 97.0012155175209
time_elpased: 2.012
batch start
#iterations: 47
currently lose_sum: 97.2891036272049
time_elpased: 2.029
batch start
#iterations: 48
currently lose_sum: 97.42256361246109
time_elpased: 2.018
batch start
#iterations: 49
currently lose_sum: 97.49668997526169
time_elpased: 2.075
batch start
#iterations: 50
currently lose_sum: 96.78974312543869
time_elpased: 2.034
batch start
#iterations: 51
currently lose_sum: 96.9593796133995
time_elpased: 2.024
batch start
#iterations: 52
currently lose_sum: 97.06364196538925
time_elpased: 2.058
batch start
#iterations: 53
currently lose_sum: 96.75865763425827
time_elpased: 2.02
batch start
#iterations: 54
currently lose_sum: 97.43633621931076
time_elpased: 2.045
batch start
#iterations: 55
currently lose_sum: 97.02084863185883
time_elpased: 2.018
batch start
#iterations: 56
currently lose_sum: 96.9013774394989
time_elpased: 2.063
batch start
#iterations: 57
currently lose_sum: 97.26305168867111
time_elpased: 2.05
batch start
#iterations: 58
currently lose_sum: 97.18144303560257
time_elpased: 2.068
batch start
#iterations: 59
currently lose_sum: 97.36506402492523
time_elpased: 2.036
start validation test
0.651804123711
0.626149914821
0.756509210662
0.685184322133
0.651620297782
62.383
batch start
#iterations: 60
currently lose_sum: 97.19390147924423
time_elpased: 2.044
batch start
#iterations: 61
currently lose_sum: 97.00774961709976
time_elpased: 2.034
batch start
#iterations: 62
currently lose_sum: 97.10941404104233
time_elpased: 2.017
batch start
#iterations: 63
currently lose_sum: 96.61907088756561
time_elpased: 2.002
batch start
#iterations: 64
currently lose_sum: 96.81279045343399
time_elpased: 2.021
batch start
#iterations: 65
currently lose_sum: 96.77211725711823
time_elpased: 1.997
batch start
#iterations: 66
currently lose_sum: 97.06548416614532
time_elpased: 2.062
batch start
#iterations: 67
currently lose_sum: 96.8016260266304
time_elpased: 2.028
batch start
#iterations: 68
currently lose_sum: 96.39647167921066
time_elpased: 2.022
batch start
#iterations: 69
currently lose_sum: 96.67103236913681
time_elpased: 2.033
batch start
#iterations: 70
currently lose_sum: 96.76256948709488
time_elpased: 2.055
batch start
#iterations: 71
currently lose_sum: 96.84049564599991
time_elpased: 2.009
batch start
#iterations: 72
currently lose_sum: 96.648810505867
time_elpased: 1.992
batch start
#iterations: 73
currently lose_sum: 96.93221592903137
time_elpased: 1.99
batch start
#iterations: 74
currently lose_sum: 96.65080285072327
time_elpased: 1.993
batch start
#iterations: 75
currently lose_sum: 96.68267178535461
time_elpased: 2.013
batch start
#iterations: 76
currently lose_sum: 96.76968723535538
time_elpased: 2.011
batch start
#iterations: 77
currently lose_sum: 96.90411049127579
time_elpased: 1.995
batch start
#iterations: 78
currently lose_sum: 96.63464170694351
time_elpased: 2.034
batch start
#iterations: 79
currently lose_sum: 96.38450980186462
time_elpased: 2.04
start validation test
0.632371134021
0.652184151654
0.570031902851
0.608347062054
0.632480580156
62.435
batch start
#iterations: 80
currently lose_sum: 96.46106570959091
time_elpased: 2.01
batch start
#iterations: 81
currently lose_sum: 96.50224804878235
time_elpased: 2.013
batch start
#iterations: 82
currently lose_sum: 96.95682203769684
time_elpased: 2.084
batch start
#iterations: 83
currently lose_sum: 96.67094677686691
time_elpased: 2.023
batch start
#iterations: 84
currently lose_sum: 96.58966022729874
time_elpased: 2.027
batch start
#iterations: 85
currently lose_sum: 96.66053706407547
time_elpased: 2.054
batch start
#iterations: 86
currently lose_sum: 96.87665164470673
time_elpased: 2.08
batch start
#iterations: 87
currently lose_sum: 96.47117173671722
time_elpased: 2.071
batch start
#iterations: 88
currently lose_sum: 96.6069707274437
time_elpased: 2.022
batch start
#iterations: 89
currently lose_sum: 96.30453616380692
time_elpased: 2.054
batch start
#iterations: 90
currently lose_sum: 96.32079941034317
time_elpased: 2.039
batch start
#iterations: 91
currently lose_sum: 96.91425907611847
time_elpased: 2.026
batch start
#iterations: 92
currently lose_sum: 96.64667427539825
time_elpased: 2.042
batch start
#iterations: 93
currently lose_sum: 96.37836837768555
time_elpased: 2.062
batch start
#iterations: 94
currently lose_sum: 96.20328736305237
time_elpased: 2.011
batch start
#iterations: 95
currently lose_sum: 96.82248049974442
time_elpased: 2.018
batch start
#iterations: 96
currently lose_sum: 96.5418267250061
time_elpased: 2.03
batch start
#iterations: 97
currently lose_sum: 96.69304412603378
time_elpased: 2.049
batch start
#iterations: 98
currently lose_sum: 96.45604693889618
time_elpased: 2.016
batch start
#iterations: 99
currently lose_sum: 96.52533626556396
time_elpased: 2.012
start validation test
0.648711340206
0.632221614726
0.71400638057
0.6706297424
0.648596704692
61.793
batch start
#iterations: 100
currently lose_sum: 96.31425505876541
time_elpased: 2.045
batch start
#iterations: 101
currently lose_sum: 96.61867886781693
time_elpased: 2.011
batch start
#iterations: 102
currently lose_sum: 96.23227518796921
time_elpased: 2.05
batch start
#iterations: 103
currently lose_sum: 96.58908241987228
time_elpased: 2.029
batch start
#iterations: 104
currently lose_sum: 96.21946507692337
time_elpased: 2.037
batch start
#iterations: 105
currently lose_sum: 96.47146481275558
time_elpased: 2.06
batch start
#iterations: 106
currently lose_sum: 96.37594896554947
time_elpased: 2.057
batch start
#iterations: 107
currently lose_sum: 96.28050744533539
time_elpased: 2.05
batch start
#iterations: 108
currently lose_sum: 96.73084616661072
time_elpased: 2.005
batch start
#iterations: 109
currently lose_sum: 96.43743288516998
time_elpased: 2.041
batch start
#iterations: 110
currently lose_sum: 96.09195691347122
time_elpased: 2.004
batch start
#iterations: 111
currently lose_sum: 96.3368010520935
time_elpased: 2.042
batch start
#iterations: 112
currently lose_sum: 96.43738889694214
time_elpased: 2.069
batch start
#iterations: 113
currently lose_sum: 96.23355215787888
time_elpased: 2.028
batch start
#iterations: 114
currently lose_sum: 96.12037026882172
time_elpased: 2.033
batch start
#iterations: 115
currently lose_sum: 96.17318695783615
time_elpased: 2.021
batch start
#iterations: 116
currently lose_sum: 96.39021641016006
time_elpased: 2.014
batch start
#iterations: 117
currently lose_sum: 96.32441526651382
time_elpased: 2.039
batch start
#iterations: 118
currently lose_sum: 96.22260767221451
time_elpased: 2.036
batch start
#iterations: 119
currently lose_sum: 96.38234186172485
time_elpased: 2.062
start validation test
0.653659793814
0.630415869149
0.745703406401
0.683230399321
0.653498197056
61.594
batch start
#iterations: 120
currently lose_sum: 96.02923333644867
time_elpased: 2.008
batch start
#iterations: 121
currently lose_sum: 96.49512511491776
time_elpased: 2.007
batch start
#iterations: 122
currently lose_sum: 96.37531226873398
time_elpased: 2.088
batch start
#iterations: 123
currently lose_sum: 96.46631002426147
time_elpased: 2.021
batch start
#iterations: 124
currently lose_sum: 96.39870250225067
time_elpased: 2.039
batch start
#iterations: 125
currently lose_sum: 95.89104735851288
time_elpased: 2.053
batch start
#iterations: 126
currently lose_sum: 95.90389263629913
time_elpased: 2.031
batch start
#iterations: 127
currently lose_sum: 96.59407240152359
time_elpased: 1.994
batch start
#iterations: 128
currently lose_sum: 96.18058437108994
time_elpased: 2.048
batch start
#iterations: 129
currently lose_sum: 95.91093963384628
time_elpased: 2.073
batch start
#iterations: 130
currently lose_sum: 96.3922746181488
time_elpased: 2.04
batch start
#iterations: 131
currently lose_sum: 96.10565853118896
time_elpased: 2.043
batch start
#iterations: 132
currently lose_sum: 96.15351289510727
time_elpased: 1.999
batch start
#iterations: 133
currently lose_sum: 96.18812841176987
time_elpased: 2.028
batch start
#iterations: 134
currently lose_sum: 96.2495778799057
time_elpased: 1.995
batch start
#iterations: 135
currently lose_sum: 96.39122819900513
time_elpased: 1.994
batch start
#iterations: 136
currently lose_sum: 96.15398746728897
time_elpased: 2.032
batch start
#iterations: 137
currently lose_sum: 96.2203871011734
time_elpased: 2.067
batch start
#iterations: 138
currently lose_sum: 96.27858942747116
time_elpased: 1.993
batch start
#iterations: 139
currently lose_sum: 96.30273169279099
time_elpased: 2.02
start validation test
0.642113402062
0.596803461753
0.880004116497
0.711249740071
0.641695748221
62.447
batch start
#iterations: 140
currently lose_sum: 96.13146215677261
time_elpased: 2.056
batch start
#iterations: 141
currently lose_sum: 95.7875697016716
time_elpased: 2.031
batch start
#iterations: 142
currently lose_sum: 96.21512883901596
time_elpased: 2.017
batch start
#iterations: 143
currently lose_sum: 95.92499870061874
time_elpased: 2.03
batch start
#iterations: 144
currently lose_sum: 96.17257106304169
time_elpased: 2.012
batch start
#iterations: 145
currently lose_sum: 95.97764605283737
time_elpased: 2.049
batch start
#iterations: 146
currently lose_sum: 96.10977071523666
time_elpased: 2.069
batch start
#iterations: 147
currently lose_sum: 96.11885541677475
time_elpased: 2.031
batch start
#iterations: 148
currently lose_sum: 95.96238243579865
time_elpased: 2.066
batch start
#iterations: 149
currently lose_sum: 95.92385631799698
time_elpased: 2.092
batch start
#iterations: 150
currently lose_sum: 95.98561269044876
time_elpased: 2.024
batch start
#iterations: 151
currently lose_sum: 96.06335562467575
time_elpased: 2.072
batch start
#iterations: 152
currently lose_sum: 95.84782391786575
time_elpased: 1.998
batch start
#iterations: 153
currently lose_sum: 96.11881738901138
time_elpased: 2.013
batch start
#iterations: 154
currently lose_sum: 95.67972284555435
time_elpased: 2.112
batch start
#iterations: 155
currently lose_sum: 95.89134818315506
time_elpased: 2.024
batch start
#iterations: 156
currently lose_sum: 96.42286968231201
time_elpased: 2.001
batch start
#iterations: 157
currently lose_sum: 95.6619343161583
time_elpased: 2.003
batch start
#iterations: 158
currently lose_sum: 96.14404320716858
time_elpased: 2.122
batch start
#iterations: 159
currently lose_sum: 96.1524927020073
time_elpased: 2.045
start validation test
0.62175257732
0.654580896686
0.518369867243
0.578566505858
0.621934081613
62.456
batch start
#iterations: 160
currently lose_sum: 96.00033396482468
time_elpased: 2.055
batch start
#iterations: 161
currently lose_sum: 95.79501056671143
time_elpased: 2.07
batch start
#iterations: 162
currently lose_sum: 95.9694619178772
time_elpased: 2.05
batch start
#iterations: 163
currently lose_sum: 95.91858369112015
time_elpased: 2.023
batch start
#iterations: 164
currently lose_sum: 95.91893243789673
time_elpased: 2.02
batch start
#iterations: 165
currently lose_sum: 95.55581432580948
time_elpased: 2.005
batch start
#iterations: 166
currently lose_sum: 95.86164385080338
time_elpased: 2.048
batch start
#iterations: 167
currently lose_sum: 95.72406876087189
time_elpased: 2.038
batch start
#iterations: 168
currently lose_sum: 96.18166488409042
time_elpased: 2.084
batch start
#iterations: 169
currently lose_sum: 95.94485890865326
time_elpased: 2.072
batch start
#iterations: 170
currently lose_sum: 95.9150743484497
time_elpased: 2.037
batch start
#iterations: 171
currently lose_sum: 95.69100654125214
time_elpased: 2.024
batch start
#iterations: 172
currently lose_sum: 95.67777049541473
time_elpased: 2.009
batch start
#iterations: 173
currently lose_sum: 95.93491345643997
time_elpased: 2.154
batch start
#iterations: 174
currently lose_sum: 95.82230299711227
time_elpased: 1.986
batch start
#iterations: 175
currently lose_sum: 95.8725979924202
time_elpased: 2.024
batch start
#iterations: 176
currently lose_sum: 96.01885902881622
time_elpased: 1.995
batch start
#iterations: 177
currently lose_sum: 95.59333032369614
time_elpased: 2.031
batch start
#iterations: 178
currently lose_sum: 95.78344702720642
time_elpased: 2.032
batch start
#iterations: 179
currently lose_sum: 95.96014708280563
time_elpased: 2.098
start validation test
0.644278350515
0.632629992464
0.691159822991
0.660600993459
0.64419604286
61.805
batch start
#iterations: 180
currently lose_sum: 95.64916908740997
time_elpased: 2.047
batch start
#iterations: 181
currently lose_sum: 96.34176820516586
time_elpased: 2.01
batch start
#iterations: 182
currently lose_sum: 95.87837219238281
time_elpased: 2.023
batch start
#iterations: 183
currently lose_sum: 95.60082393884659
time_elpased: 2.002
batch start
#iterations: 184
currently lose_sum: 95.72839230298996
time_elpased: 2.017
batch start
#iterations: 185
currently lose_sum: 96.2669717669487
time_elpased: 2.063
batch start
#iterations: 186
currently lose_sum: 95.94548773765564
time_elpased: 2.027
batch start
#iterations: 187
currently lose_sum: 95.84167462587357
time_elpased: 2.057
batch start
#iterations: 188
currently lose_sum: 95.3986946940422
time_elpased: 2.057
batch start
#iterations: 189
currently lose_sum: 95.52224844694138
time_elpased: 2.032
batch start
#iterations: 190
currently lose_sum: 95.98247754573822
time_elpased: 2.011
batch start
#iterations: 191
currently lose_sum: 95.7317722439766
time_elpased: 1.992
batch start
#iterations: 192
currently lose_sum: 96.09067678451538
time_elpased: 2.014
batch start
#iterations: 193
currently lose_sum: 96.01027399301529
time_elpased: 2.019
batch start
#iterations: 194
currently lose_sum: 95.57053738832474
time_elpased: 2.054
batch start
#iterations: 195
currently lose_sum: 95.46859049797058
time_elpased: 2.06
batch start
#iterations: 196
currently lose_sum: 95.27551919221878
time_elpased: 2.019
batch start
#iterations: 197
currently lose_sum: 95.97118020057678
time_elpased: 2.005
batch start
#iterations: 198
currently lose_sum: 95.71195435523987
time_elpased: 2.037
batch start
#iterations: 199
currently lose_sum: 96.00225549936295
time_elpased: 2.088
start validation test
0.654381443299
0.621451612903
0.793043120305
0.696839535199
0.654138001338
61.378
batch start
#iterations: 200
currently lose_sum: 95.84934616088867
time_elpased: 2.075
batch start
#iterations: 201
currently lose_sum: 95.88910681009293
time_elpased: 2.102
batch start
#iterations: 202
currently lose_sum: 95.24188166856766
time_elpased: 2.047
batch start
#iterations: 203
currently lose_sum: 95.98905050754547
time_elpased: 2.031
batch start
#iterations: 204
currently lose_sum: 95.66957598924637
time_elpased: 2.032
batch start
#iterations: 205
currently lose_sum: 95.72474843263626
time_elpased: 2.037
batch start
#iterations: 206
currently lose_sum: 95.68412309885025
time_elpased: 2.007
batch start
#iterations: 207
currently lose_sum: 95.81323605775833
time_elpased: 2.059
batch start
#iterations: 208
currently lose_sum: 95.24245494604111
time_elpased: 1.972
batch start
#iterations: 209
currently lose_sum: 95.31929361820221
time_elpased: 2.007
batch start
#iterations: 210
currently lose_sum: 95.41657811403275
time_elpased: 2.006
batch start
#iterations: 211
currently lose_sum: 95.60445213317871
time_elpased: 2.023
batch start
#iterations: 212
currently lose_sum: 95.53519254922867
time_elpased: 2.027
batch start
#iterations: 213
currently lose_sum: 95.58671700954437
time_elpased: 2.037
batch start
#iterations: 214
currently lose_sum: 95.70458352565765
time_elpased: 2.01
batch start
#iterations: 215
currently lose_sum: 95.92816686630249
time_elpased: 2.009
batch start
#iterations: 216
currently lose_sum: 95.62106037139893
time_elpased: 2.036
batch start
#iterations: 217
currently lose_sum: 95.67310833930969
time_elpased: 2.03
batch start
#iterations: 218
currently lose_sum: 95.30963999032974
time_elpased: 2.042
batch start
#iterations: 219
currently lose_sum: 95.46868908405304
time_elpased: 2.105
start validation test
0.654948453608
0.62643245504
0.770711124833
0.691122185308
0.654745214384
60.998
batch start
#iterations: 220
currently lose_sum: 95.34387093782425
time_elpased: 1.993
batch start
#iterations: 221
currently lose_sum: 95.75479292869568
time_elpased: 2.035
batch start
#iterations: 222
currently lose_sum: 95.44933623075485
time_elpased: 2.035
batch start
#iterations: 223
currently lose_sum: 95.67028391361237
time_elpased: 2.074
batch start
#iterations: 224
currently lose_sum: 95.30228525400162
time_elpased: 2.035
batch start
#iterations: 225
currently lose_sum: 95.4065733551979
time_elpased: 2.011
batch start
#iterations: 226
currently lose_sum: 95.28575313091278
time_elpased: 2.018
batch start
#iterations: 227
currently lose_sum: 95.62438762187958
time_elpased: 2.006
batch start
#iterations: 228
currently lose_sum: 95.78908616304398
time_elpased: 1.994
batch start
#iterations: 229
currently lose_sum: 95.72190129756927
time_elpased: 2.014
batch start
#iterations: 230
currently lose_sum: 95.27482032775879
time_elpased: 2.012
batch start
#iterations: 231
currently lose_sum: 95.24729186296463
time_elpased: 2.045
batch start
#iterations: 232
currently lose_sum: 95.80742996931076
time_elpased: 1.996
batch start
#iterations: 233
currently lose_sum: 95.30397981405258
time_elpased: 2.063
batch start
#iterations: 234
currently lose_sum: 95.55054461956024
time_elpased: 1.998
batch start
#iterations: 235
currently lose_sum: 95.79481464624405
time_elpased: 2.031
batch start
#iterations: 236
currently lose_sum: 95.49077886343002
time_elpased: 1.979
batch start
#iterations: 237
currently lose_sum: 95.45739990472794
time_elpased: 2.046
batch start
#iterations: 238
currently lose_sum: 95.34710305929184
time_elpased: 2.002
batch start
#iterations: 239
currently lose_sum: 95.39604419469833
time_elpased: 2.02
start validation test
0.648556701031
0.629895151895
0.723371410929
0.673404866833
0.648425352268
61.330
batch start
#iterations: 240
currently lose_sum: 95.75615042448044
time_elpased: 2.017
batch start
#iterations: 241
currently lose_sum: 95.38719993829727
time_elpased: 2.042
batch start
#iterations: 242
currently lose_sum: 95.3417095541954
time_elpased: 1.985
batch start
#iterations: 243
currently lose_sum: 95.417629301548
time_elpased: 2.014
batch start
#iterations: 244
currently lose_sum: 95.76608747243881
time_elpased: 2.004
batch start
#iterations: 245
currently lose_sum: 95.52593207359314
time_elpased: 2.04
batch start
#iterations: 246
currently lose_sum: 95.25579553842545
time_elpased: 2.004
batch start
#iterations: 247
currently lose_sum: 95.37530660629272
time_elpased: 2.072
batch start
#iterations: 248
currently lose_sum: 95.64527022838593
time_elpased: 2.006
batch start
#iterations: 249
currently lose_sum: 95.29841840267181
time_elpased: 2.011
batch start
#iterations: 250
currently lose_sum: 95.47651332616806
time_elpased: 2.016
batch start
#iterations: 251
currently lose_sum: 95.16446459293365
time_elpased: 2.054
batch start
#iterations: 252
currently lose_sum: 95.32107871770859
time_elpased: 1.986
batch start
#iterations: 253
currently lose_sum: 94.984543800354
time_elpased: 1.999
batch start
#iterations: 254
currently lose_sum: 95.5321814417839
time_elpased: 2.023
batch start
#iterations: 255
currently lose_sum: 95.65670990943909
time_elpased: 2.024
batch start
#iterations: 256
currently lose_sum: 95.30121248960495
time_elpased: 2.023
batch start
#iterations: 257
currently lose_sum: 95.82745605707169
time_elpased: 2.062
batch start
#iterations: 258
currently lose_sum: 94.92111110687256
time_elpased: 1.993
batch start
#iterations: 259
currently lose_sum: 95.28240203857422
time_elpased: 2.061
start validation test
0.633608247423
0.650652500289
0.579808582896
0.613191118851
0.633702701032
61.953
batch start
#iterations: 260
currently lose_sum: 95.50543278455734
time_elpased: 2.075
batch start
#iterations: 261
currently lose_sum: 94.76213210821152
time_elpased: 2.014
batch start
#iterations: 262
currently lose_sum: 95.52015906572342
time_elpased: 2.021
batch start
#iterations: 263
currently lose_sum: 95.05479955673218
time_elpased: 2.008
batch start
#iterations: 264
currently lose_sum: 95.55292195081711
time_elpased: 2.035
batch start
#iterations: 265
currently lose_sum: 95.49290323257446
time_elpased: 2.018
batch start
#iterations: 266
currently lose_sum: 95.39595890045166
time_elpased: 2.003
batch start
#iterations: 267
currently lose_sum: 95.57045489549637
time_elpased: 2.03
batch start
#iterations: 268
currently lose_sum: 95.63819950819016
time_elpased: 2.079
batch start
#iterations: 269
currently lose_sum: 95.42897510528564
time_elpased: 2.021
batch start
#iterations: 270
currently lose_sum: 95.07293617725372
time_elpased: 1.966
batch start
#iterations: 271
currently lose_sum: 95.33398002386093
time_elpased: 1.994
batch start
#iterations: 272
currently lose_sum: 95.61461877822876
time_elpased: 1.979
batch start
#iterations: 273
currently lose_sum: 94.89410275220871
time_elpased: 2.031
batch start
#iterations: 274
currently lose_sum: 95.91508823633194
time_elpased: 2.019
batch start
#iterations: 275
currently lose_sum: 95.19411045312881
time_elpased: 2.01
batch start
#iterations: 276
currently lose_sum: 95.37592315673828
time_elpased: 2.014
batch start
#iterations: 277
currently lose_sum: 95.4152193069458
time_elpased: 2.115
batch start
#iterations: 278
currently lose_sum: 95.36267763376236
time_elpased: 1.963
batch start
#iterations: 279
currently lose_sum: 95.13687741756439
time_elpased: 2.001
start validation test
0.649948453608
0.610348468849
0.832767315015
0.704417845484
0.649627486899
61.344
batch start
#iterations: 280
currently lose_sum: 95.0346559882164
time_elpased: 2.035
batch start
#iterations: 281
currently lose_sum: 95.24880331754684
time_elpased: 2.078
batch start
#iterations: 282
currently lose_sum: 95.21368634700775
time_elpased: 2.096
batch start
#iterations: 283
currently lose_sum: 95.31978869438171
time_elpased: 2.065
batch start
#iterations: 284
currently lose_sum: 95.12482237815857
time_elpased: 2.02
batch start
#iterations: 285
currently lose_sum: 95.3076524734497
time_elpased: 2.017
batch start
#iterations: 286
currently lose_sum: 95.51190727949142
time_elpased: 2.006
batch start
#iterations: 287
currently lose_sum: 95.09370440244675
time_elpased: 2.047
batch start
#iterations: 288
currently lose_sum: 95.08617460727692
time_elpased: 1.987
batch start
#iterations: 289
currently lose_sum: 95.17733311653137
time_elpased: 2.022
batch start
#iterations: 290
currently lose_sum: 95.58598697185516
time_elpased: 2.037
batch start
#iterations: 291
currently lose_sum: 94.71035236120224
time_elpased: 2.076
batch start
#iterations: 292
currently lose_sum: 95.23802626132965
time_elpased: 2.034
batch start
#iterations: 293
currently lose_sum: 95.45473992824554
time_elpased: 2.067
batch start
#iterations: 294
currently lose_sum: 94.81427150964737
time_elpased: 2.01
batch start
#iterations: 295
currently lose_sum: 95.10089337825775
time_elpased: 2.008
batch start
#iterations: 296
currently lose_sum: 95.0965445637703
time_elpased: 1.99
batch start
#iterations: 297
currently lose_sum: 95.79138606786728
time_elpased: 2.016
batch start
#iterations: 298
currently lose_sum: 94.9240471124649
time_elpased: 1.989
batch start
#iterations: 299
currently lose_sum: 95.7239357829094
time_elpased: 1.998
start validation test
0.650360824742
0.620049099836
0.779767417927
0.690796371427
0.650133631508
61.189
batch start
#iterations: 300
currently lose_sum: 95.0845918059349
time_elpased: 1.999
batch start
#iterations: 301
currently lose_sum: 95.09234577417374
time_elpased: 2.107
batch start
#iterations: 302
currently lose_sum: 95.30750024318695
time_elpased: 2.193
batch start
#iterations: 303
currently lose_sum: 95.35115051269531
time_elpased: 2.045
batch start
#iterations: 304
currently lose_sum: 95.29971432685852
time_elpased: 2.041
batch start
#iterations: 305
currently lose_sum: 95.44095605611801
time_elpased: 2.08
batch start
#iterations: 306
currently lose_sum: 95.15107017755508
time_elpased: 2.069
batch start
#iterations: 307
currently lose_sum: 95.1855348944664
time_elpased: 2.065
batch start
#iterations: 308
currently lose_sum: 95.14355820417404
time_elpased: 2.015
batch start
#iterations: 309
currently lose_sum: 95.14581543207169
time_elpased: 2.033
batch start
#iterations: 310
currently lose_sum: 95.3542799949646
time_elpased: 2.045
batch start
#iterations: 311
currently lose_sum: 94.90184783935547
time_elpased: 2.005
batch start
#iterations: 312
currently lose_sum: 94.73979914188385
time_elpased: 2.003
batch start
#iterations: 313
currently lose_sum: 95.19371896982193
time_elpased: 2.039
batch start
#iterations: 314
currently lose_sum: 95.37911850214005
time_elpased: 2.023
batch start
#iterations: 315
currently lose_sum: 94.92190873622894
time_elpased: 2.028
batch start
#iterations: 316
currently lose_sum: 95.1610032916069
time_elpased: 2.032
batch start
#iterations: 317
currently lose_sum: 94.88411474227905
time_elpased: 2.051
batch start
#iterations: 318
currently lose_sum: 94.84129214286804
time_elpased: 2.013
batch start
#iterations: 319
currently lose_sum: 95.23697501420975
time_elpased: 2.084
start validation test
0.646391752577
0.631987434168
0.70392096326
0.666017526777
0.646290751175
61.403
batch start
#iterations: 320
currently lose_sum: 94.82565432786942
time_elpased: 2.052
batch start
#iterations: 321
currently lose_sum: 94.8601233959198
time_elpased: 2.076
batch start
#iterations: 322
currently lose_sum: 95.61186373233795
time_elpased: 2.04
batch start
#iterations: 323
currently lose_sum: 95.2408994436264
time_elpased: 2.031
batch start
#iterations: 324
currently lose_sum: 95.11058616638184
time_elpased: 2.048
batch start
#iterations: 325
currently lose_sum: 94.84774821996689
time_elpased: 2.046
batch start
#iterations: 326
currently lose_sum: 95.20979505777359
time_elpased: 1.995
batch start
#iterations: 327
currently lose_sum: 94.92636001110077
time_elpased: 2.043
batch start
#iterations: 328
currently lose_sum: 95.55852895975113
time_elpased: 2.0
batch start
#iterations: 329
currently lose_sum: 95.23375391960144
time_elpased: 2.032
batch start
#iterations: 330
currently lose_sum: 94.76230329275131
time_elpased: 2.038
batch start
#iterations: 331
currently lose_sum: 95.40247458219528
time_elpased: 2.072
batch start
#iterations: 332
currently lose_sum: 95.21641534566879
time_elpased: 2.017
batch start
#iterations: 333
currently lose_sum: 94.93182647228241
time_elpased: 2.114
batch start
#iterations: 334
currently lose_sum: 95.34625911712646
time_elpased: 2.022
batch start
#iterations: 335
currently lose_sum: 94.92104095220566
time_elpased: 2.108
batch start
#iterations: 336
currently lose_sum: 95.05196291208267
time_elpased: 2.134
batch start
#iterations: 337
currently lose_sum: 94.78105878829956
time_elpased: 2.096
batch start
#iterations: 338
currently lose_sum: 95.27476453781128
time_elpased: 2.025
batch start
#iterations: 339
currently lose_sum: 95.46732932329178
time_elpased: 2.038
start validation test
0.646340206186
0.638640776699
0.67695790882
0.657241344857
0.646286452086
61.469
batch start
#iterations: 340
currently lose_sum: 95.13419026136398
time_elpased: 2.085
batch start
#iterations: 341
currently lose_sum: 95.52526372671127
time_elpased: 2.06
batch start
#iterations: 342
currently lose_sum: 94.90198707580566
time_elpased: 2.053
batch start
#iterations: 343
currently lose_sum: 94.8771248459816
time_elpased: 2.068
batch start
#iterations: 344
currently lose_sum: 94.93579477071762
time_elpased: 2.06
batch start
#iterations: 345
currently lose_sum: 95.29076796770096
time_elpased: 2.051
batch start
#iterations: 346
currently lose_sum: 95.07665395736694
time_elpased: 2.04
batch start
#iterations: 347
currently lose_sum: 95.04296916723251
time_elpased: 2.042
batch start
#iterations: 348
currently lose_sum: 95.1809698343277
time_elpased: 2.014
batch start
#iterations: 349
currently lose_sum: 94.62859535217285
time_elpased: 2.064
batch start
#iterations: 350
currently lose_sum: 94.93930023908615
time_elpased: 2.036
batch start
#iterations: 351
currently lose_sum: 94.80398154258728
time_elpased: 2.03
batch start
#iterations: 352
currently lose_sum: 95.0770435333252
time_elpased: 2.085
batch start
#iterations: 353
currently lose_sum: 95.17330235242844
time_elpased: 2.042
batch start
#iterations: 354
currently lose_sum: 94.94357341527939
time_elpased: 2.082
batch start
#iterations: 355
currently lose_sum: 94.73703008890152
time_elpased: 2.078
batch start
#iterations: 356
currently lose_sum: 94.96477800607681
time_elpased: 2.024
batch start
#iterations: 357
currently lose_sum: 95.01643520593643
time_elpased: 2.08
batch start
#iterations: 358
currently lose_sum: 94.73715043067932
time_elpased: 2.011
batch start
#iterations: 359
currently lose_sum: 94.84636896848679
time_elpased: 2.02
start validation test
0.651494845361
0.621466140697
0.778223731604
0.691066940827
0.651272353254
61.095
batch start
#iterations: 360
currently lose_sum: 95.32664966583252
time_elpased: 2.055
batch start
#iterations: 361
currently lose_sum: 95.49432098865509
time_elpased: 2.014
batch start
#iterations: 362
currently lose_sum: 94.98849678039551
time_elpased: 2.074
batch start
#iterations: 363
currently lose_sum: 95.34634947776794
time_elpased: 2.053
batch start
#iterations: 364
currently lose_sum: 95.0411102771759
time_elpased: 2.009
batch start
#iterations: 365
currently lose_sum: 94.95237565040588
time_elpased: 2.035
batch start
#iterations: 366
currently lose_sum: 95.0513231754303
time_elpased: 2.021
batch start
#iterations: 367
currently lose_sum: 94.9345508813858
time_elpased: 2.015
batch start
#iterations: 368
currently lose_sum: 95.06939655542374
time_elpased: 2.014
batch start
#iterations: 369
currently lose_sum: 95.26691806316376
time_elpased: 2.036
batch start
#iterations: 370
currently lose_sum: 94.42938202619553
time_elpased: 2.052
batch start
#iterations: 371
currently lose_sum: 95.05570465326309
time_elpased: 2.058
batch start
#iterations: 372
currently lose_sum: 94.98713862895966
time_elpased: 2.014
batch start
#iterations: 373
currently lose_sum: 95.25805741548538
time_elpased: 2.06
batch start
#iterations: 374
currently lose_sum: 95.03698009252548
time_elpased: 2.018
batch start
#iterations: 375
currently lose_sum: 94.95074474811554
time_elpased: 2.027
batch start
#iterations: 376
currently lose_sum: 95.06675809621811
time_elpased: 2.051
batch start
#iterations: 377
currently lose_sum: 95.46073269844055
time_elpased: 2.065
batch start
#iterations: 378
currently lose_sum: 94.37583410739899
time_elpased: 2.044
batch start
#iterations: 379
currently lose_sum: 94.96000277996063
time_elpased: 2.034
start validation test
0.654793814433
0.626890756303
0.767726664608
0.690197529722
0.654595543396
61.050
batch start
#iterations: 380
currently lose_sum: 94.6056147813797
time_elpased: 2.021
batch start
#iterations: 381
currently lose_sum: 95.30075073242188
time_elpased: 2.043
batch start
#iterations: 382
currently lose_sum: 95.06654262542725
time_elpased: 2.03
batch start
#iterations: 383
currently lose_sum: 94.97279065847397
time_elpased: 2.068
batch start
#iterations: 384
currently lose_sum: 94.90836137533188
time_elpased: 2.041
batch start
#iterations: 385
currently lose_sum: 95.12237805128098
time_elpased: 2.034
batch start
#iterations: 386
currently lose_sum: 94.8288522362709
time_elpased: 2.064
batch start
#iterations: 387
currently lose_sum: 94.69293659925461
time_elpased: 2.027
batch start
#iterations: 388
currently lose_sum: 94.98002552986145
time_elpased: 2.05
batch start
#iterations: 389
currently lose_sum: 95.23893934488297
time_elpased: 2.074
batch start
#iterations: 390
currently lose_sum: 95.11396938562393
time_elpased: 2.048
batch start
#iterations: 391
currently lose_sum: 94.7668554186821
time_elpased: 2.032
batch start
#iterations: 392
currently lose_sum: 94.48637264966965
time_elpased: 2.155
batch start
#iterations: 393
currently lose_sum: 94.807501912117
time_elpased: 2.062
batch start
#iterations: 394
currently lose_sum: 95.0584186911583
time_elpased: 2.056
batch start
#iterations: 395
currently lose_sum: 94.88946777582169
time_elpased: 2.077
batch start
#iterations: 396
currently lose_sum: 94.76620054244995
time_elpased: 2.05
batch start
#iterations: 397
currently lose_sum: 95.21421432495117
time_elpased: 2.011
batch start
#iterations: 398
currently lose_sum: 94.95275509357452
time_elpased: 1.988
batch start
#iterations: 399
currently lose_sum: 94.89549720287323
time_elpased: 2.047
start validation test
0.646340206186
0.631612903226
0.70525882474
0.666407351583
0.646236765463
61.106
acc: 0.651
pre: 0.624
rec: 0.766
F1: 0.688
auc: 0.651
