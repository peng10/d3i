start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 101.2931050658226
time_elpased: 2.142
batch start
#iterations: 1
currently lose_sum: 100.4121727347374
time_elpased: 2.139
batch start
#iterations: 2
currently lose_sum: 100.41239017248154
time_elpased: 2.201
batch start
#iterations: 3
currently lose_sum: 100.33293277025223
time_elpased: 2.232
batch start
#iterations: 4
currently lose_sum: 100.23244994878769
time_elpased: 2.224
batch start
#iterations: 5
currently lose_sum: 100.17126685380936
time_elpased: 2.301
batch start
#iterations: 6
currently lose_sum: 100.19649261236191
time_elpased: 2.308
batch start
#iterations: 7
currently lose_sum: 100.18848186731339
time_elpased: 2.204
batch start
#iterations: 8
currently lose_sum: 99.96047353744507
time_elpased: 2.294
batch start
#iterations: 9
currently lose_sum: 99.84847348928452
time_elpased: 2.307
batch start
#iterations: 10
currently lose_sum: 99.8132563829422
time_elpased: 2.223
batch start
#iterations: 11
currently lose_sum: 99.7225496172905
time_elpased: 2.196
batch start
#iterations: 12
currently lose_sum: 99.58204597234726
time_elpased: 2.232
batch start
#iterations: 13
currently lose_sum: 99.399642765522
time_elpased: 2.108
batch start
#iterations: 14
currently lose_sum: 99.25915265083313
time_elpased: 2.124
batch start
#iterations: 15
currently lose_sum: 99.19799596071243
time_elpased: 2.193
batch start
#iterations: 16
currently lose_sum: 99.22560352087021
time_elpased: 2.254
batch start
#iterations: 17
currently lose_sum: 99.1323219537735
time_elpased: 2.203
batch start
#iterations: 18
currently lose_sum: 98.78156739473343
time_elpased: 2.208
batch start
#iterations: 19
currently lose_sum: 98.75159156322479
time_elpased: 2.17
start validation test
0.629896907216
0.602888672509
0.764717990943
0.674228675136
0.629674154497
64.618
batch start
#iterations: 20
currently lose_sum: 98.71756821870804
time_elpased: 2.202
batch start
#iterations: 21
currently lose_sum: 98.58613932132721
time_elpased: 2.166
batch start
#iterations: 22
currently lose_sum: 98.38460230827332
time_elpased: 2.156
batch start
#iterations: 23
currently lose_sum: 98.04921054840088
time_elpased: 2.158
batch start
#iterations: 24
currently lose_sum: 98.17372691631317
time_elpased: 2.147
batch start
#iterations: 25
currently lose_sum: 98.14386004209518
time_elpased: 2.053
batch start
#iterations: 26
currently lose_sum: 97.75816804170609
time_elpased: 2.108
batch start
#iterations: 27
currently lose_sum: 98.14481145143509
time_elpased: 2.178
batch start
#iterations: 28
currently lose_sum: 97.78562551736832
time_elpased: 2.186
batch start
#iterations: 29
currently lose_sum: 98.17722308635712
time_elpased: 2.192
batch start
#iterations: 30
currently lose_sum: 97.68910801410675
time_elpased: 2.183
batch start
#iterations: 31
currently lose_sum: 97.85957753658295
time_elpased: 2.195
batch start
#iterations: 32
currently lose_sum: 97.57388466596603
time_elpased: 2.155
batch start
#iterations: 33
currently lose_sum: 97.70657557249069
time_elpased: 2.183
batch start
#iterations: 34
currently lose_sum: 97.69221073389053
time_elpased: 2.142
batch start
#iterations: 35
currently lose_sum: 97.90908223390579
time_elpased: 2.167
batch start
#iterations: 36
currently lose_sum: 97.54470235109329
time_elpased: 2.15
batch start
#iterations: 37
currently lose_sum: 97.52721226215363
time_elpased: 2.168
batch start
#iterations: 38
currently lose_sum: 97.26650553941727
time_elpased: 2.07
batch start
#iterations: 39
currently lose_sum: 97.53515791893005
time_elpased: 2.158
start validation test
0.637010309278
0.640766477153
0.626389460683
0.633496408869
0.637027857149
62.987
batch start
#iterations: 40
currently lose_sum: 97.31367975473404
time_elpased: 2.218
batch start
#iterations: 41
currently lose_sum: 97.52160441875458
time_elpased: 2.11
batch start
#iterations: 42
currently lose_sum: 97.3031393289566
time_elpased: 2.103
batch start
#iterations: 43
currently lose_sum: 97.28130728006363
time_elpased: 2.097
batch start
#iterations: 44
currently lose_sum: 97.40696340799332
time_elpased: 2.211
batch start
#iterations: 45
currently lose_sum: 97.26396781206131
time_elpased: 2.188
batch start
#iterations: 46
currently lose_sum: 96.89178627729416
time_elpased: 2.159
batch start
#iterations: 47
currently lose_sum: 97.26274007558823
time_elpased: 2.108
batch start
#iterations: 48
currently lose_sum: 97.00818687677383
time_elpased: 2.126
batch start
#iterations: 49
currently lose_sum: 97.36205011606216
time_elpased: 2.136
batch start
#iterations: 50
currently lose_sum: 97.07078438997269
time_elpased: 2.053
batch start
#iterations: 51
currently lose_sum: 97.03879100084305
time_elpased: 2.06
batch start
#iterations: 52
currently lose_sum: 97.2184407711029
time_elpased: 2.186
batch start
#iterations: 53
currently lose_sum: 96.8963171839714
time_elpased: 2.197
batch start
#iterations: 54
currently lose_sum: 97.29123365879059
time_elpased: 2.188
batch start
#iterations: 55
currently lose_sum: 97.2310642004013
time_elpased: 2.143
batch start
#iterations: 56
currently lose_sum: 97.08111524581909
time_elpased: 2.134
batch start
#iterations: 57
currently lose_sum: 97.18426316976547
time_elpased: 2.138
batch start
#iterations: 58
currently lose_sum: 96.98853886127472
time_elpased: 2.196
batch start
#iterations: 59
currently lose_sum: 97.12172430753708
time_elpased: 2.22
start validation test
0.648453608247
0.634622536259
0.702552490737
0.66686205549
0.648364225542
62.773
batch start
#iterations: 60
currently lose_sum: 96.80318361520767
time_elpased: 2.144
batch start
#iterations: 61
currently lose_sum: 97.09395653009415
time_elpased: 2.101
batch start
#iterations: 62
currently lose_sum: 97.33052039146423
time_elpased: 2.186
batch start
#iterations: 63
currently lose_sum: 96.77863705158234
time_elpased: 2.06
batch start
#iterations: 64
currently lose_sum: 96.97672319412231
time_elpased: 2.057
batch start
#iterations: 65
currently lose_sum: 96.9186230301857
time_elpased: 2.187
batch start
#iterations: 66
currently lose_sum: 97.07616150379181
time_elpased: 2.175
batch start
#iterations: 67
currently lose_sum: 97.29154974222183
time_elpased: 2.15
batch start
#iterations: 68
currently lose_sum: 96.93624186515808
time_elpased: 2.141
batch start
#iterations: 69
currently lose_sum: 96.91970312595367
time_elpased: 2.109
batch start
#iterations: 70
currently lose_sum: 96.83852738142014
time_elpased: 2.161
batch start
#iterations: 71
currently lose_sum: 96.92271792888641
time_elpased: 2.182
batch start
#iterations: 72
currently lose_sum: 96.63482093811035
time_elpased: 2.163
batch start
#iterations: 73
currently lose_sum: 96.87912058830261
time_elpased: 2.162
batch start
#iterations: 74
currently lose_sum: 96.70049315690994
time_elpased: 2.171
batch start
#iterations: 75
currently lose_sum: 96.95083993673325
time_elpased: 2.222
batch start
#iterations: 76
currently lose_sum: 96.64061379432678
time_elpased: 2.063
batch start
#iterations: 77
currently lose_sum: 96.75761288404465
time_elpased: 2.058
batch start
#iterations: 78
currently lose_sum: 96.66692590713501
time_elpased: 2.133
batch start
#iterations: 79
currently lose_sum: 96.63189780712128
time_elpased: 2.157
start validation test
0.631804123711
0.647517486527
0.581206257719
0.61257254434
0.631887722003
62.851
batch start
#iterations: 80
currently lose_sum: 96.77402794361115
time_elpased: 2.134
batch start
#iterations: 81
currently lose_sum: 96.43434435129166
time_elpased: 2.118
batch start
#iterations: 82
currently lose_sum: 96.87804985046387
time_elpased: 2.088
batch start
#iterations: 83
currently lose_sum: 96.43189811706543
time_elpased: 2.135
batch start
#iterations: 84
currently lose_sum: 96.46490925550461
time_elpased: 2.357
batch start
#iterations: 85
currently lose_sum: 96.85516685247421
time_elpased: 2.256
batch start
#iterations: 86
currently lose_sum: 96.38749635219574
time_elpased: 2.053
batch start
#iterations: 87
currently lose_sum: 96.43876683712006
time_elpased: 2.083
batch start
#iterations: 88
currently lose_sum: 96.39591985940933
time_elpased: 2.155
batch start
#iterations: 89
currently lose_sum: 96.8205372095108
time_elpased: 2.07
batch start
#iterations: 90
currently lose_sum: 96.73062986135483
time_elpased: 2.034
batch start
#iterations: 91
currently lose_sum: 96.5370774269104
time_elpased: 2.15
batch start
#iterations: 92
currently lose_sum: 96.38285410404205
time_elpased: 2.118
batch start
#iterations: 93
currently lose_sum: 96.30578982830048
time_elpased: 2.133
batch start
#iterations: 94
currently lose_sum: 96.56168466806412
time_elpased: 2.126
batch start
#iterations: 95
currently lose_sum: 96.62520170211792
time_elpased: 2.037
batch start
#iterations: 96
currently lose_sum: 96.81949752569199
time_elpased: 2.08
batch start
#iterations: 97
currently lose_sum: 96.28352057933807
time_elpased: 2.131
batch start
#iterations: 98
currently lose_sum: 96.66812801361084
time_elpased: 2.141
batch start
#iterations: 99
currently lose_sum: 96.48084115982056
time_elpased: 2.114
start validation test
0.656649484536
0.627749435477
0.772540139975
0.692659068888
0.656458008856
62.029
batch start
#iterations: 100
currently lose_sum: 96.75662046670914
time_elpased: 2.148
batch start
#iterations: 101
currently lose_sum: 96.63319593667984
time_elpased: 2.104
batch start
#iterations: 102
currently lose_sum: 96.72284466028214
time_elpased: 2.11
batch start
#iterations: 103
currently lose_sum: 96.55599462985992
time_elpased: 1.983
batch start
#iterations: 104
currently lose_sum: 96.50781762599945
time_elpased: 2.061
batch start
#iterations: 105
currently lose_sum: 96.43639177083969
time_elpased: 2.156
batch start
#iterations: 106
currently lose_sum: 96.29837989807129
time_elpased: 2.105
batch start
#iterations: 107
currently lose_sum: 96.64199590682983
time_elpased: 2.134
batch start
#iterations: 108
currently lose_sum: 96.28238850831985
time_elpased: 2.123
batch start
#iterations: 109
currently lose_sum: 96.65886056423187
time_elpased: 2.145
batch start
#iterations: 110
currently lose_sum: 96.36191266775131
time_elpased: 2.174
batch start
#iterations: 111
currently lose_sum: 96.0775579214096
time_elpased: 2.115
batch start
#iterations: 112
currently lose_sum: 95.7981099486351
time_elpased: 2.106
batch start
#iterations: 113
currently lose_sum: 96.25806879997253
time_elpased: 2.005
batch start
#iterations: 114
currently lose_sum: 96.47556787729263
time_elpased: 2.12
batch start
#iterations: 115
currently lose_sum: 96.21736532449722
time_elpased: 2.046
batch start
#iterations: 116
currently lose_sum: 96.06307351589203
time_elpased: 2.011
batch start
#iterations: 117
currently lose_sum: 96.17088747024536
time_elpased: 2.016
batch start
#iterations: 118
currently lose_sum: 96.25017416477203
time_elpased: 2.029
batch start
#iterations: 119
currently lose_sum: 96.44662463665009
time_elpased: 2.112
start validation test
0.651288659794
0.627782107907
0.746088925484
0.681841696844
0.651132029863
62.010
batch start
#iterations: 120
currently lose_sum: 96.20297956466675
time_elpased: 2.092
batch start
#iterations: 121
currently lose_sum: 96.09695667028427
time_elpased: 2.059
batch start
#iterations: 122
currently lose_sum: 95.95652514696121
time_elpased: 2.135
batch start
#iterations: 123
currently lose_sum: 96.46459728479385
time_elpased: 2.139
batch start
#iterations: 124
currently lose_sum: 96.47049844264984
time_elpased: 2.122
batch start
#iterations: 125
currently lose_sum: 95.92118191719055
time_elpased: 2.147
batch start
#iterations: 126
currently lose_sum: 95.89171171188354
time_elpased: 2.161
batch start
#iterations: 127
currently lose_sum: 96.18940740823746
time_elpased: 2.174
batch start
#iterations: 128
currently lose_sum: 95.98886334896088
time_elpased: 2.071
batch start
#iterations: 129
currently lose_sum: 96.22104316949844
time_elpased: 2.051
batch start
#iterations: 130
currently lose_sum: 96.31132757663727
time_elpased: 2.096
batch start
#iterations: 131
currently lose_sum: 96.21145963668823
time_elpased: 2.122
batch start
#iterations: 132
currently lose_sum: 95.91625612974167
time_elpased: 2.128
batch start
#iterations: 133
currently lose_sum: 96.11453378200531
time_elpased: 2.102
batch start
#iterations: 134
currently lose_sum: 96.42840385437012
time_elpased: 2.12
batch start
#iterations: 135
currently lose_sum: 96.25307828187943
time_elpased: 2.127
batch start
#iterations: 136
currently lose_sum: 96.29601895809174
time_elpased: 2.141
batch start
#iterations: 137
currently lose_sum: 96.09545958042145
time_elpased: 2.143
batch start
#iterations: 138
currently lose_sum: 96.20882660150528
time_elpased: 2.048
batch start
#iterations: 139
currently lose_sum: 95.91062420606613
time_elpased: 2.098
start validation test
0.645360824742
0.644164294429
0.65212021408
0.648117839607
0.645349656813
62.139
batch start
#iterations: 140
currently lose_sum: 95.98533010482788
time_elpased: 2.097
batch start
#iterations: 141
currently lose_sum: 96.05970650911331
time_elpased: 2.048
batch start
#iterations: 142
currently lose_sum: 96.02699387073517
time_elpased: 2.148
batch start
#iterations: 143
currently lose_sum: 95.98466002941132
time_elpased: 2.153
batch start
#iterations: 144
currently lose_sum: 96.03769278526306
time_elpased: 2.111
batch start
#iterations: 145
currently lose_sum: 95.73618960380554
time_elpased: 2.13
batch start
#iterations: 146
currently lose_sum: 95.96578860282898
time_elpased: 2.106
batch start
#iterations: 147
currently lose_sum: 96.2116904258728
time_elpased: 2.07
batch start
#iterations: 148
currently lose_sum: 96.06098061800003
time_elpased: 2.161
batch start
#iterations: 149
currently lose_sum: 95.9044781923294
time_elpased: 2.096
batch start
#iterations: 150
currently lose_sum: 96.22698301076889
time_elpased: 2.106
batch start
#iterations: 151
currently lose_sum: 95.94790297746658
time_elpased: 2.139
batch start
#iterations: 152
currently lose_sum: 95.98978364467621
time_elpased: 2.126
batch start
#iterations: 153
currently lose_sum: 96.1776134967804
time_elpased: 2.075
batch start
#iterations: 154
currently lose_sum: 95.91310346126556
time_elpased: 2.099
batch start
#iterations: 155
currently lose_sum: 95.90069538354874
time_elpased: 2.098
batch start
#iterations: 156
currently lose_sum: 95.86189949512482
time_elpased: 2.111
batch start
#iterations: 157
currently lose_sum: 95.99606722593307
time_elpased: 2.172
batch start
#iterations: 158
currently lose_sum: 96.2242169380188
time_elpased: 2.116
batch start
#iterations: 159
currently lose_sum: 95.64218872785568
time_elpased: 2.132
start validation test
0.655412371134
0.616014698002
0.828221490325
0.706527942403
0.655126854208
61.716
batch start
#iterations: 160
currently lose_sum: 95.89446663856506
time_elpased: 2.186
batch start
#iterations: 161
currently lose_sum: 95.96277731657028
time_elpased: 2.112
batch start
#iterations: 162
currently lose_sum: 96.00555688142776
time_elpased: 2.105
batch start
#iterations: 163
currently lose_sum: 95.73013639450073
time_elpased: 2.106
batch start
#iterations: 164
currently lose_sum: 96.12517446279526
time_elpased: 2.03
batch start
#iterations: 165
currently lose_sum: 95.76421195268631
time_elpased: 2.009
batch start
#iterations: 166
currently lose_sum: 95.70083767175674
time_elpased: 2.072
batch start
#iterations: 167
currently lose_sum: 95.36598342657089
time_elpased: 2.153
batch start
#iterations: 168
currently lose_sum: 96.18931180238724
time_elpased: 2.131
batch start
#iterations: 169
currently lose_sum: 95.83269995450974
time_elpased: 2.182
batch start
#iterations: 170
currently lose_sum: 95.66833716630936
time_elpased: 2.142
batch start
#iterations: 171
currently lose_sum: 96.00613498687744
time_elpased: 2.126
batch start
#iterations: 172
currently lose_sum: 95.60282576084137
time_elpased: 2.138
batch start
#iterations: 173
currently lose_sum: 95.44529235363007
time_elpased: 2.238
batch start
#iterations: 174
currently lose_sum: 95.77553564310074
time_elpased: 2.061
batch start
#iterations: 175
currently lose_sum: 95.68968665599823
time_elpased: 2.14
batch start
#iterations: 176
currently lose_sum: 96.2655690908432
time_elpased: 2.125
batch start
#iterations: 177
currently lose_sum: 95.73094087839127
time_elpased: 2.132
batch start
#iterations: 178
currently lose_sum: 95.72646999359131
time_elpased: 2.012
batch start
#iterations: 179
currently lose_sum: 95.39810353517532
time_elpased: 2.107
start validation test
0.660773195876
0.628874455315
0.787258130918
0.699209287445
0.660564216223
61.333
batch start
#iterations: 180
currently lose_sum: 95.58932012319565
time_elpased: 2.151
batch start
#iterations: 181
currently lose_sum: 95.97091817855835
time_elpased: 2.105
batch start
#iterations: 182
currently lose_sum: 95.94237285852432
time_elpased: 2.002
batch start
#iterations: 183
currently lose_sum: 95.50471478700638
time_elpased: 2.149
batch start
#iterations: 184
currently lose_sum: 95.81211030483246
time_elpased: 2.117
batch start
#iterations: 185
currently lose_sum: 95.62068611383438
time_elpased: 2.111
batch start
#iterations: 186
currently lose_sum: 95.85390132665634
time_elpased: 2.156
batch start
#iterations: 187
currently lose_sum: 95.78948998451233
time_elpased: 2.139
batch start
#iterations: 188
currently lose_sum: 95.88475501537323
time_elpased: 2.094
batch start
#iterations: 189
currently lose_sum: 95.56808519363403
time_elpased: 2.106
batch start
#iterations: 190
currently lose_sum: 95.79098743200302
time_elpased: 2.006
batch start
#iterations: 191
currently lose_sum: 95.84617239236832
time_elpased: 2.012
batch start
#iterations: 192
currently lose_sum: 95.78470766544342
time_elpased: 2.096
batch start
#iterations: 193
currently lose_sum: 95.30460011959076
time_elpased: 2.129
batch start
#iterations: 194
currently lose_sum: 95.66694432497025
time_elpased: 2.164
batch start
#iterations: 195
currently lose_sum: 95.33806526660919
time_elpased: 2.221
batch start
#iterations: 196
currently lose_sum: 95.42403966188431
time_elpased: 2.204
batch start
#iterations: 197
currently lose_sum: 95.29451811313629
time_elpased: 2.168
batch start
#iterations: 198
currently lose_sum: 95.16624480485916
time_elpased: 2.123
batch start
#iterations: 199
currently lose_sum: 95.56503295898438
time_elpased: 2.232
start validation test
0.658556701031
0.632795052396
0.758233841087
0.689858600993
0.65839201348
61.411
batch start
#iterations: 200
currently lose_sum: 95.54771292209625
time_elpased: 2.104
batch start
#iterations: 201
currently lose_sum: 95.55214500427246
time_elpased: 2.139
batch start
#iterations: 202
currently lose_sum: 95.62345123291016
time_elpased: 2.024
batch start
#iterations: 203
currently lose_sum: 95.99174529314041
time_elpased: 2.117
batch start
#iterations: 204
currently lose_sum: 95.9428403377533
time_elpased: 2.187
batch start
#iterations: 205
currently lose_sum: 95.43690031766891
time_elpased: 2.171
batch start
#iterations: 206
currently lose_sum: 95.48792564868927
time_elpased: 2.156
batch start
#iterations: 207
currently lose_sum: 95.70818972587585
time_elpased: 2.155
batch start
#iterations: 208
currently lose_sum: 95.5630487203598
time_elpased: 2.056
batch start
#iterations: 209
currently lose_sum: 95.35201841592789
time_elpased: 2.121
batch start
#iterations: 210
currently lose_sum: 95.01927483081818
time_elpased: 2.146
batch start
#iterations: 211
currently lose_sum: 94.92034924030304
time_elpased: 2.188
batch start
#iterations: 212
currently lose_sum: 96.01559114456177
time_elpased: 2.134
batch start
#iterations: 213
currently lose_sum: 95.40367013216019
time_elpased: 2.164
batch start
#iterations: 214
currently lose_sum: 95.45416468381882
time_elpased: 2.063
batch start
#iterations: 215
currently lose_sum: 95.65067481994629
time_elpased: 2.097
batch start
#iterations: 216
currently lose_sum: 95.38473719358444
time_elpased: 2.179
batch start
#iterations: 217
currently lose_sum: 95.50830888748169
time_elpased: 2.115
batch start
#iterations: 218
currently lose_sum: 95.44883489608765
time_elpased: 2.137
batch start
#iterations: 219
currently lose_sum: 95.34322088956833
time_elpased: 2.195
start validation test
0.662525773196
0.630874700586
0.786125977769
0.699995417679
0.662321559723
61.018
batch start
#iterations: 220
currently lose_sum: 95.26241290569305
time_elpased: 2.132
batch start
#iterations: 221
currently lose_sum: 95.7732612490654
time_elpased: 2.197
batch start
#iterations: 222
currently lose_sum: 94.98324310779572
time_elpased: 2.14
batch start
#iterations: 223
currently lose_sum: 95.00788760185242
time_elpased: 2.18
batch start
#iterations: 224
currently lose_sum: 95.00407135486603
time_elpased: 2.154
batch start
#iterations: 225
currently lose_sum: 95.38676911592484
time_elpased: 2.107
batch start
#iterations: 226
currently lose_sum: 95.2832180261612
time_elpased: 2.012
batch start
#iterations: 227
currently lose_sum: 95.33432173728943
time_elpased: 2.165
batch start
#iterations: 228
currently lose_sum: 95.11740446090698
time_elpased: 2.161
batch start
#iterations: 229
currently lose_sum: 95.15533512830734
time_elpased: 2.15
batch start
#iterations: 230
currently lose_sum: 95.56819814443588
time_elpased: 2.157
batch start
#iterations: 231
currently lose_sum: 95.6801153421402
time_elpased: 2.225
batch start
#iterations: 232
currently lose_sum: 95.55099886655807
time_elpased: 2.187
batch start
#iterations: 233
currently lose_sum: 95.59887361526489
time_elpased: 2.181
batch start
#iterations: 234
currently lose_sum: 95.55200576782227
time_elpased: 2.121
batch start
#iterations: 235
currently lose_sum: 95.4046214222908
time_elpased: 2.139
batch start
#iterations: 236
currently lose_sum: 95.08710587024689
time_elpased: 2.203
batch start
#iterations: 237
currently lose_sum: 95.44410568475723
time_elpased: 2.183
batch start
#iterations: 238
currently lose_sum: 95.42844468355179
time_elpased: 2.054
batch start
#iterations: 239
currently lose_sum: 95.32726019620895
time_elpased: 2.135
start validation test
0.66206185567
0.640819964349
0.740016467682
0.686855177684
0.661933058294
61.128
batch start
#iterations: 240
currently lose_sum: 95.25432974100113
time_elpased: 2.21
batch start
#iterations: 241
currently lose_sum: 95.18426835536957
time_elpased: 2.21
batch start
#iterations: 242
currently lose_sum: 95.1137729883194
time_elpased: 2.13
batch start
#iterations: 243
currently lose_sum: 95.35579746961594
time_elpased: 2.042
batch start
#iterations: 244
currently lose_sum: 95.46466201543808
time_elpased: 2.159
batch start
#iterations: 245
currently lose_sum: 95.19082701206207
time_elpased: 2.211
batch start
#iterations: 246
currently lose_sum: 95.2265453338623
time_elpased: 2.19
batch start
#iterations: 247
currently lose_sum: 95.72271597385406
time_elpased: 2.167
batch start
#iterations: 248
currently lose_sum: 95.26992112398148
time_elpased: 2.14
batch start
#iterations: 249
currently lose_sum: 95.79410725831985
time_elpased: 2.125
batch start
#iterations: 250
currently lose_sum: 95.3408197760582
time_elpased: 2.091
batch start
#iterations: 251
currently lose_sum: 95.58974134922028
time_elpased: 2.275
batch start
#iterations: 252
currently lose_sum: 95.21583616733551
time_elpased: 2.152
batch start
#iterations: 253
currently lose_sum: 95.23456233739853
time_elpased: 2.189
batch start
#iterations: 254
currently lose_sum: 95.19553691148758
time_elpased: 2.179
batch start
#iterations: 255
currently lose_sum: 95.53069043159485
time_elpased: 2.199
batch start
#iterations: 256
currently lose_sum: 94.59980309009552
time_elpased: 2.163
batch start
#iterations: 257
currently lose_sum: 95.2188059091568
time_elpased: 2.15
batch start
#iterations: 258
currently lose_sum: 95.32651829719543
time_elpased: 2.099
batch start
#iterations: 259
currently lose_sum: 95.33557462692261
time_elpased: 2.186
start validation test
0.660773195876
0.639395286794
0.740016467682
0.686035971566
0.660642269364
61.172
batch start
#iterations: 260
currently lose_sum: 95.21280002593994
time_elpased: 2.09
batch start
#iterations: 261
currently lose_sum: 94.93456614017487
time_elpased: 2.038
batch start
#iterations: 262
currently lose_sum: 94.87308698892593
time_elpased: 2.108
batch start
#iterations: 263
currently lose_sum: 95.11202204227448
time_elpased: 2.194
batch start
#iterations: 264
currently lose_sum: 95.3948312997818
time_elpased: 2.194
batch start
#iterations: 265
currently lose_sum: 95.23545187711716
time_elpased: 2.211
batch start
#iterations: 266
currently lose_sum: 95.05122935771942
time_elpased: 2.165
batch start
#iterations: 267
currently lose_sum: 95.43098247051239
time_elpased: 2.16
batch start
#iterations: 268
currently lose_sum: 94.95631378889084
time_elpased: 2.178
batch start
#iterations: 269
currently lose_sum: 95.5195522904396
time_elpased: 2.105
batch start
#iterations: 270
currently lose_sum: 94.95355862379074
time_elpased: 2.126
batch start
#iterations: 271
currently lose_sum: 95.20321816205978
time_elpased: 2.21
batch start
#iterations: 272
currently lose_sum: 95.01555466651917
time_elpased: 2.137
batch start
#iterations: 273
currently lose_sum: 95.21559923887253
time_elpased: 2.099
batch start
#iterations: 274
currently lose_sum: 95.22265499830246
time_elpased: 2.088
batch start
#iterations: 275
currently lose_sum: 95.34244859218597
time_elpased: 2.123
batch start
#iterations: 276
currently lose_sum: 94.97519701719284
time_elpased: 2.188
batch start
#iterations: 277
currently lose_sum: 94.95857459306717
time_elpased: 2.165
batch start
#iterations: 278
currently lose_sum: 95.01224374771118
time_elpased: 2.236
batch start
#iterations: 279
currently lose_sum: 95.3359626531601
time_elpased: 2.315
start validation test
0.608144329897
0.656918052257
0.455434335117
0.537928519329
0.608396638852
62.680
batch start
#iterations: 280
currently lose_sum: 95.13953107595444
time_elpased: 2.317
batch start
#iterations: 281
currently lose_sum: 94.83152312040329
time_elpased: 2.266
batch start
#iterations: 282
currently lose_sum: 95.54969954490662
time_elpased: 2.21
batch start
#iterations: 283
currently lose_sum: 95.32785087823868
time_elpased: 2.108
batch start
#iterations: 284
currently lose_sum: 94.84468752145767
time_elpased: 2.122
batch start
#iterations: 285
currently lose_sum: 95.39330476522446
time_elpased: 2.193
batch start
#iterations: 286
currently lose_sum: 94.93128889799118
time_elpased: 2.181
batch start
#iterations: 287
currently lose_sum: 94.8415744304657
time_elpased: 2.256
batch start
#iterations: 288
currently lose_sum: 95.11678010225296
time_elpased: 2.303
batch start
#iterations: 289
currently lose_sum: 95.15716820955276
time_elpased: 2.269
batch start
#iterations: 290
currently lose_sum: 94.88295620679855
time_elpased: 2.292
batch start
#iterations: 291
currently lose_sum: 95.1861383318901
time_elpased: 2.212
batch start
#iterations: 292
currently lose_sum: 95.01493287086487
time_elpased: 2.208
batch start
#iterations: 293
currently lose_sum: 94.94409263134003
time_elpased: 2.271
batch start
#iterations: 294
currently lose_sum: 95.2167220711708
time_elpased: 2.094
batch start
#iterations: 295
currently lose_sum: 95.3716532588005
time_elpased: 2.192
batch start
#iterations: 296
currently lose_sum: 95.32603698968887
time_elpased: 2.183
batch start
#iterations: 297
currently lose_sum: 95.15411067008972
time_elpased: 2.139
batch start
#iterations: 298
currently lose_sum: 94.76554679870605
time_elpased: 2.202
batch start
#iterations: 299
currently lose_sum: 95.32637870311737
time_elpased: 2.179
start validation test
0.656443298969
0.646050741982
0.694524495677
0.669411239522
0.656380380842
61.158
batch start
#iterations: 300
currently lose_sum: 95.27646028995514
time_elpased: 2.068
batch start
#iterations: 301
currently lose_sum: 94.71977633237839
time_elpased: 2.114
batch start
#iterations: 302
currently lose_sum: 94.53768247365952
time_elpased: 2.148
batch start
#iterations: 303
currently lose_sum: 94.94804006814957
time_elpased: 2.126
batch start
#iterations: 304
currently lose_sum: 94.57886296510696
time_elpased: 2.043
batch start
#iterations: 305
currently lose_sum: 94.90934300422668
time_elpased: 2.183
batch start
#iterations: 306
currently lose_sum: 95.40351837873459
time_elpased: 2.168
batch start
#iterations: 307
currently lose_sum: 94.98995107412338
time_elpased: 2.146
batch start
#iterations: 308
currently lose_sum: 94.76510399580002
time_elpased: 2.157
batch start
#iterations: 309
currently lose_sum: 94.72424578666687
time_elpased: 2.268
batch start
#iterations: 310
currently lose_sum: 94.92814415693283
time_elpased: 2.241
batch start
#iterations: 311
currently lose_sum: 94.97462272644043
time_elpased: 2.211
batch start
#iterations: 312
currently lose_sum: 95.24576556682587
time_elpased: 2.099
batch start
#iterations: 313
currently lose_sum: 94.88988041877747
time_elpased: 1.972
batch start
#iterations: 314
currently lose_sum: 94.85022097826004
time_elpased: 2.145
batch start
#iterations: 315
currently lose_sum: 94.26374155282974
time_elpased: 2.218
batch start
#iterations: 316
currently lose_sum: 94.70246970653534
time_elpased: 2.142
batch start
#iterations: 317
currently lose_sum: 94.83380252122879
time_elpased: 2.166
batch start
#iterations: 318
currently lose_sum: 94.82957625389099
time_elpased: 2.138
batch start
#iterations: 319
currently lose_sum: 94.84301513433456
time_elpased: 2.159
start validation test
0.660773195876
0.635304272767
0.757513379992
0.691047368668
0.660613360793
60.905
batch start
#iterations: 320
currently lose_sum: 95.22282046079636
time_elpased: 2.179
batch start
#iterations: 321
currently lose_sum: 94.94559276103973
time_elpased: 2.139
batch start
#iterations: 322
currently lose_sum: 95.22985070943832
time_elpased: 2.066
batch start
#iterations: 323
currently lose_sum: 95.05582249164581
time_elpased: 2.147
batch start
#iterations: 324
currently lose_sum: 94.78299868106842
time_elpased: 2.184
batch start
#iterations: 325
currently lose_sum: 94.95847862958908
time_elpased: 2.06
batch start
#iterations: 326
currently lose_sum: 94.76084583997726
time_elpased: 2.134
batch start
#iterations: 327
currently lose_sum: 94.7434070110321
time_elpased: 2.152
batch start
#iterations: 328
currently lose_sum: 95.0017603635788
time_elpased: 2.179
batch start
#iterations: 329
currently lose_sum: 94.89963561296463
time_elpased: 2.108
batch start
#iterations: 330
currently lose_sum: 94.96092796325684
time_elpased: 2.1
batch start
#iterations: 331
currently lose_sum: 94.93985325098038
time_elpased: 2.153
batch start
#iterations: 332
currently lose_sum: 94.68943393230438
time_elpased: 2.166
batch start
#iterations: 333
currently lose_sum: 94.80164790153503
time_elpased: 2.159
batch start
#iterations: 334
currently lose_sum: 94.82227402925491
time_elpased: 2.161
batch start
#iterations: 335
currently lose_sum: 94.7440310716629
time_elpased: 2.175
batch start
#iterations: 336
currently lose_sum: 94.70721244812012
time_elpased: 2.158
batch start
#iterations: 337
currently lose_sum: 94.72342050075531
time_elpased: 2.044
batch start
#iterations: 338
currently lose_sum: 94.89187294244766
time_elpased: 2.116
batch start
#iterations: 339
currently lose_sum: 95.07986849546432
time_elpased: 2.099
start validation test
0.646494845361
0.65004199916
0.63719637711
0.643555093555
0.646510208381
61.291
batch start
#iterations: 340
currently lose_sum: 95.09610885381699
time_elpased: 2.226
batch start
#iterations: 341
currently lose_sum: 94.74147117137909
time_elpased: 2.141
batch start
#iterations: 342
currently lose_sum: 95.40243476629257
time_elpased: 2.167
batch start
#iterations: 343
currently lose_sum: 95.17782658338547
time_elpased: 2.182
batch start
#iterations: 344
currently lose_sum: 94.97768425941467
time_elpased: 2.115
batch start
#iterations: 345
currently lose_sum: 94.77657318115234
time_elpased: 2.184
batch start
#iterations: 346
currently lose_sum: 94.67694073915482
time_elpased: 2.178
batch start
#iterations: 347
currently lose_sum: 95.01923578977585
time_elpased: 2.224
batch start
#iterations: 348
currently lose_sum: 94.91666543483734
time_elpased: 2.12
batch start
#iterations: 349
currently lose_sum: 94.6316157579422
time_elpased: 2.189
batch start
#iterations: 350
currently lose_sum: 95.2210391163826
time_elpased: 2.156
batch start
#iterations: 351
currently lose_sum: 94.92974901199341
time_elpased: 2.095
batch start
#iterations: 352
currently lose_sum: 95.20440071821213
time_elpased: 2.09
batch start
#iterations: 353
currently lose_sum: 95.0757263302803
time_elpased: 2.121
batch start
#iterations: 354
currently lose_sum: 94.70694255828857
time_elpased: 2.1
batch start
#iterations: 355
currently lose_sum: 94.63614928722382
time_elpased: 2.12
batch start
#iterations: 356
currently lose_sum: 95.03424108028412
time_elpased: 2.177
batch start
#iterations: 357
currently lose_sum: 94.82206344604492
time_elpased: 2.215
batch start
#iterations: 358
currently lose_sum: 94.68899357318878
time_elpased: 2.222
batch start
#iterations: 359
currently lose_sum: 94.65391963720322
time_elpased: 2.231
start validation test
0.661597938144
0.628496859962
0.793124742692
0.701278609455
0.661380628265
60.772
batch start
#iterations: 360
currently lose_sum: 94.70150327682495
time_elpased: 2.223
batch start
#iterations: 361
currently lose_sum: 94.97658783197403
time_elpased: 2.204
batch start
#iterations: 362
currently lose_sum: 95.07329332828522
time_elpased: 2.202
batch start
#iterations: 363
currently lose_sum: 94.76214808225632
time_elpased: 2.149
batch start
#iterations: 364
currently lose_sum: 94.6481608748436
time_elpased: 2.162
batch start
#iterations: 365
currently lose_sum: 95.03655737638474
time_elpased: 2.185
batch start
#iterations: 366
currently lose_sum: 95.26200759410858
time_elpased: 2.23
batch start
#iterations: 367
currently lose_sum: 94.81546002626419
time_elpased: 2.216
batch start
#iterations: 368
currently lose_sum: 95.13495230674744
time_elpased: 2.251
batch start
#iterations: 369
currently lose_sum: 94.95316290855408
time_elpased: 2.249
batch start
#iterations: 370
currently lose_sum: 94.77330487966537
time_elpased: 2.246
batch start
#iterations: 371
currently lose_sum: 94.67902791500092
time_elpased: 2.183
batch start
#iterations: 372
currently lose_sum: 94.89472484588623
time_elpased: 2.207
batch start
#iterations: 373
currently lose_sum: 94.57028794288635
time_elpased: 2.198
batch start
#iterations: 374
currently lose_sum: 94.93476110696793
time_elpased: 2.292
batch start
#iterations: 375
currently lose_sum: 94.83646374940872
time_elpased: 2.314
batch start
#iterations: 376
currently lose_sum: 94.48000311851501
time_elpased: 2.259
batch start
#iterations: 377
currently lose_sum: 94.93188560009003
time_elpased: 2.331
batch start
#iterations: 378
currently lose_sum: 94.8418088555336
time_elpased: 2.257
batch start
#iterations: 379
currently lose_sum: 95.04808413982391
time_elpased: 2.223
start validation test
0.663711340206
0.634638096845
0.774289831206
0.697542883635
0.663528641336
60.965
batch start
#iterations: 380
currently lose_sum: 94.5537012219429
time_elpased: 2.303
batch start
#iterations: 381
currently lose_sum: 94.44050943851471
time_elpased: 2.137
batch start
#iterations: 382
currently lose_sum: 94.9705102443695
time_elpased: 2.222
batch start
#iterations: 383
currently lose_sum: 94.3488239645958
time_elpased: 2.28
batch start
#iterations: 384
currently lose_sum: 94.93879330158234
time_elpased: 2.262
batch start
#iterations: 385
currently lose_sum: 95.04801744222641
time_elpased: 2.285
batch start
#iterations: 386
currently lose_sum: 94.82021802663803
time_elpased: 2.229
batch start
#iterations: 387
currently lose_sum: 94.50813829898834
time_elpased: 2.184
batch start
#iterations: 388
currently lose_sum: 95.075015604496
time_elpased: 2.289
batch start
#iterations: 389
currently lose_sum: 94.57514196634293
time_elpased: 2.218
batch start
#iterations: 390
currently lose_sum: 94.82628130912781
time_elpased: 2.191
batch start
#iterations: 391
currently lose_sum: 94.62677985429764
time_elpased: 2.244
batch start
#iterations: 392
currently lose_sum: 94.79784554243088
time_elpased: 2.257
batch start
#iterations: 393
currently lose_sum: 94.51135915517807
time_elpased: 2.243
batch start
#iterations: 394
currently lose_sum: 94.5249815583229
time_elpased: 2.218
batch start
#iterations: 395
currently lose_sum: 94.52684903144836
time_elpased: 2.112
batch start
#iterations: 396
currently lose_sum: 94.28851616382599
time_elpased: 2.21
batch start
#iterations: 397
currently lose_sum: 94.77531898021698
time_elpased: 2.196
batch start
#iterations: 398
currently lose_sum: 94.62756717205048
time_elpased: 2.157
batch start
#iterations: 399
currently lose_sum: 94.56241178512573
time_elpased: 2.099
start validation test
0.666494845361
0.633185622846
0.794153972828
0.704593187837
0.666283925695
60.829
acc: 0.670
pre: 0.636
rec: 0.799
F1: 0.708
auc: 0.670
