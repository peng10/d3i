start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.41164416074753
time_elpased: 2.133
batch start
#iterations: 1
currently lose_sum: 100.49727153778076
time_elpased: 1.935
batch start
#iterations: 2
currently lose_sum: 100.37754970788956
time_elpased: 1.941
batch start
#iterations: 3
currently lose_sum: 100.38767629861832
time_elpased: 1.948
batch start
#iterations: 4
currently lose_sum: 100.35373133420944
time_elpased: 2.005
batch start
#iterations: 5
currently lose_sum: 100.24786096811295
time_elpased: 1.934
batch start
#iterations: 6
currently lose_sum: 100.26290428638458
time_elpased: 2.015
batch start
#iterations: 7
currently lose_sum: 100.1778250336647
time_elpased: 2.008
batch start
#iterations: 8
currently lose_sum: 99.95442229509354
time_elpased: 2.011
batch start
#iterations: 9
currently lose_sum: 100.04500502347946
time_elpased: 1.964
batch start
#iterations: 10
currently lose_sum: 99.90809208154678
time_elpased: 1.973
batch start
#iterations: 11
currently lose_sum: 99.79798048734665
time_elpased: 1.995
batch start
#iterations: 12
currently lose_sum: 99.73527663946152
time_elpased: 1.986
batch start
#iterations: 13
currently lose_sum: 99.57817041873932
time_elpased: 1.987
batch start
#iterations: 14
currently lose_sum: 99.56687837839127
time_elpased: 2.003
batch start
#iterations: 15
currently lose_sum: 99.31769585609436
time_elpased: 1.956
batch start
#iterations: 16
currently lose_sum: 99.26005214452744
time_elpased: 1.939
batch start
#iterations: 17
currently lose_sum: 99.1592487692833
time_elpased: 1.977
batch start
#iterations: 18
currently lose_sum: 98.76383346319199
time_elpased: 2.031
batch start
#iterations: 19
currently lose_sum: 98.77751338481903
time_elpased: 2.068
start validation test
0.625979381443
0.640227920228
0.578161984151
0.607614103396
0.62606333226
64.262
batch start
#iterations: 20
currently lose_sum: 98.88397127389908
time_elpased: 2.066
batch start
#iterations: 21
currently lose_sum: 98.29594576358795
time_elpased: 1.955
batch start
#iterations: 22
currently lose_sum: 98.33242225646973
time_elpased: 1.913
batch start
#iterations: 23
currently lose_sum: 98.21554923057556
time_elpased: 1.969
batch start
#iterations: 24
currently lose_sum: 97.7215228676796
time_elpased: 1.974
batch start
#iterations: 25
currently lose_sum: 98.1518617272377
time_elpased: 1.916
batch start
#iterations: 26
currently lose_sum: 98.07180082798004
time_elpased: 1.914
batch start
#iterations: 27
currently lose_sum: 97.95789194107056
time_elpased: 1.908
batch start
#iterations: 28
currently lose_sum: 97.73205536603928
time_elpased: 1.901
batch start
#iterations: 29
currently lose_sum: 97.69776701927185
time_elpased: 1.905
batch start
#iterations: 30
currently lose_sum: 97.80141097307205
time_elpased: 1.905
batch start
#iterations: 31
currently lose_sum: 97.7394991517067
time_elpased: 1.935
batch start
#iterations: 32
currently lose_sum: 97.44772577285767
time_elpased: 1.923
batch start
#iterations: 33
currently lose_sum: 97.52511674165726
time_elpased: 1.934
batch start
#iterations: 34
currently lose_sum: 97.62457299232483
time_elpased: 1.97
batch start
#iterations: 35
currently lose_sum: 97.29753071069717
time_elpased: 1.898
batch start
#iterations: 36
currently lose_sum: 97.7219033241272
time_elpased: 1.874
batch start
#iterations: 37
currently lose_sum: 97.03574150800705
time_elpased: 1.893
batch start
#iterations: 38
currently lose_sum: 97.3146076798439
time_elpased: 1.915
batch start
#iterations: 39
currently lose_sum: 97.465192258358
time_elpased: 2.004
start validation test
0.640051546392
0.654533122315
0.595862920655
0.623821580563
0.64012912634
62.940
batch start
#iterations: 40
currently lose_sum: 97.6438153386116
time_elpased: 2.045
batch start
#iterations: 41
currently lose_sum: 97.5999014377594
time_elpased: 1.923
batch start
#iterations: 42
currently lose_sum: 97.38774865865707
time_elpased: 2.04
batch start
#iterations: 43
currently lose_sum: 96.6890275478363
time_elpased: 1.901
batch start
#iterations: 44
currently lose_sum: 97.1791883111
time_elpased: 1.904
batch start
#iterations: 45
currently lose_sum: 97.21487653255463
time_elpased: 1.925
batch start
#iterations: 46
currently lose_sum: 97.05140787363052
time_elpased: 1.89
batch start
#iterations: 47
currently lose_sum: 97.19387120008469
time_elpased: 1.909
batch start
#iterations: 48
currently lose_sum: 97.24597817659378
time_elpased: 1.895
batch start
#iterations: 49
currently lose_sum: 97.20493668317795
time_elpased: 1.905
batch start
#iterations: 50
currently lose_sum: 97.35510504245758
time_elpased: 1.91
batch start
#iterations: 51
currently lose_sum: 97.07612150907516
time_elpased: 1.922
batch start
#iterations: 52
currently lose_sum: 96.74400579929352
time_elpased: 1.905
batch start
#iterations: 53
currently lose_sum: 97.0855171084404
time_elpased: 1.904
batch start
#iterations: 54
currently lose_sum: 97.13438898324966
time_elpased: 1.884
batch start
#iterations: 55
currently lose_sum: 97.00209319591522
time_elpased: 1.923
batch start
#iterations: 56
currently lose_sum: 97.02439665794373
time_elpased: 1.915
batch start
#iterations: 57
currently lose_sum: 97.25291162729263
time_elpased: 1.906
batch start
#iterations: 58
currently lose_sum: 97.34118419885635
time_elpased: 1.905
batch start
#iterations: 59
currently lose_sum: 96.96727204322815
time_elpased: 1.887
start validation test
0.635257731959
0.650039768208
0.588761963569
0.617885300788
0.635339362452
62.597
batch start
#iterations: 60
currently lose_sum: 96.80155283212662
time_elpased: 1.919
batch start
#iterations: 61
currently lose_sum: 97.30732929706573
time_elpased: 1.889
batch start
#iterations: 62
currently lose_sum: 96.76354736089706
time_elpased: 1.928
batch start
#iterations: 63
currently lose_sum: 96.6842874288559
time_elpased: 1.968
batch start
#iterations: 64
currently lose_sum: 97.38708382844925
time_elpased: 1.927
batch start
#iterations: 65
currently lose_sum: 96.5057715177536
time_elpased: 1.903
batch start
#iterations: 66
currently lose_sum: 97.03056609630585
time_elpased: 1.944
batch start
#iterations: 67
currently lose_sum: 96.99985736608505
time_elpased: 1.898
batch start
#iterations: 68
currently lose_sum: 96.8636434674263
time_elpased: 1.88
batch start
#iterations: 69
currently lose_sum: 96.69914084672928
time_elpased: 1.904
batch start
#iterations: 70
currently lose_sum: 96.30087345838547
time_elpased: 1.913
batch start
#iterations: 71
currently lose_sum: 96.36812615394592
time_elpased: 1.955
batch start
#iterations: 72
currently lose_sum: 96.39948779344559
time_elpased: 1.939
batch start
#iterations: 73
currently lose_sum: 96.49414736032486
time_elpased: 1.907
batch start
#iterations: 74
currently lose_sum: 96.29776763916016
time_elpased: 1.896
batch start
#iterations: 75
currently lose_sum: 96.51721042394638
time_elpased: 1.912
batch start
#iterations: 76
currently lose_sum: 96.87217706441879
time_elpased: 1.899
batch start
#iterations: 77
currently lose_sum: 96.94453102350235
time_elpased: 1.919
batch start
#iterations: 78
currently lose_sum: 97.0803832411766
time_elpased: 1.906
batch start
#iterations: 79
currently lose_sum: 96.64469689130783
time_elpased: 1.91
start validation test
0.651134020619
0.641629046201
0.687454975816
0.663751987281
0.65107025358
62.209
batch start
#iterations: 80
currently lose_sum: 96.6139788031578
time_elpased: 1.992
batch start
#iterations: 81
currently lose_sum: 96.85226160287857
time_elpased: 1.899
batch start
#iterations: 82
currently lose_sum: 96.87049704790115
time_elpased: 1.886
batch start
#iterations: 83
currently lose_sum: 96.92114681005478
time_elpased: 1.913
batch start
#iterations: 84
currently lose_sum: 96.83444726467133
time_elpased: 1.897
batch start
#iterations: 85
currently lose_sum: 96.67293310165405
time_elpased: 1.913
batch start
#iterations: 86
currently lose_sum: 96.80780386924744
time_elpased: 1.902
batch start
#iterations: 87
currently lose_sum: 96.48212134838104
time_elpased: 1.895
batch start
#iterations: 88
currently lose_sum: 96.50949549674988
time_elpased: 1.886
batch start
#iterations: 89
currently lose_sum: 96.91843873262405
time_elpased: 1.928
batch start
#iterations: 90
currently lose_sum: 96.24458640813828
time_elpased: 1.923
batch start
#iterations: 91
currently lose_sum: 96.56381672620773
time_elpased: 1.911
batch start
#iterations: 92
currently lose_sum: 96.35403108596802
time_elpased: 1.958
batch start
#iterations: 93
currently lose_sum: 96.33155339956284
time_elpased: 1.921
batch start
#iterations: 94
currently lose_sum: 96.36795085668564
time_elpased: 1.906
batch start
#iterations: 95
currently lose_sum: 96.8857125043869
time_elpased: 1.915
batch start
#iterations: 96
currently lose_sum: 96.45447438955307
time_elpased: 1.949
batch start
#iterations: 97
currently lose_sum: 96.06669270992279
time_elpased: 1.915
batch start
#iterations: 98
currently lose_sum: 96.77706182003021
time_elpased: 1.917
batch start
#iterations: 99
currently lose_sum: 96.57632267475128
time_elpased: 1.909
start validation test
0.643608247423
0.644798016324
0.642276422764
0.643534749433
0.643610585646
62.180
batch start
#iterations: 100
currently lose_sum: 96.4927464723587
time_elpased: 1.934
batch start
#iterations: 101
currently lose_sum: 96.06420576572418
time_elpased: 1.899
batch start
#iterations: 102
currently lose_sum: 96.51069122552872
time_elpased: 1.915
batch start
#iterations: 103
currently lose_sum: 96.53857213258743
time_elpased: 1.904
batch start
#iterations: 104
currently lose_sum: 96.28808999061584
time_elpased: 1.911
batch start
#iterations: 105
currently lose_sum: 96.07523792982101
time_elpased: 1.905
batch start
#iterations: 106
currently lose_sum: 96.39858502149582
time_elpased: 1.9
batch start
#iterations: 107
currently lose_sum: 96.26451647281647
time_elpased: 1.942
batch start
#iterations: 108
currently lose_sum: 96.34477454423904
time_elpased: 1.917
batch start
#iterations: 109
currently lose_sum: 96.10782819986343
time_elpased: 1.983
batch start
#iterations: 110
currently lose_sum: 96.2002626657486
time_elpased: 1.988
batch start
#iterations: 111
currently lose_sum: 96.31166732311249
time_elpased: 2.007
batch start
#iterations: 112
currently lose_sum: 96.59395450353622
time_elpased: 2.054
batch start
#iterations: 113
currently lose_sum: 96.04706674814224
time_elpased: 2.012
batch start
#iterations: 114
currently lose_sum: 96.28163433074951
time_elpased: 1.991
batch start
#iterations: 115
currently lose_sum: 96.59830391407013
time_elpased: 2.011
batch start
#iterations: 116
currently lose_sum: 96.25868040323257
time_elpased: 2.018
batch start
#iterations: 117
currently lose_sum: 96.15813970565796
time_elpased: 2.014
batch start
#iterations: 118
currently lose_sum: 96.26517349481583
time_elpased: 1.907
batch start
#iterations: 119
currently lose_sum: 96.09489244222641
time_elpased: 1.894
start validation test
0.651237113402
0.64413402364
0.678604507564
0.660920116267
0.65118906572
61.753
batch start
#iterations: 120
currently lose_sum: 96.64594370126724
time_elpased: 1.946
batch start
#iterations: 121
currently lose_sum: 96.09043633937836
time_elpased: 1.9
batch start
#iterations: 122
currently lose_sum: 96.49357068538666
time_elpased: 1.901
batch start
#iterations: 123
currently lose_sum: 96.0134094953537
time_elpased: 1.905
batch start
#iterations: 124
currently lose_sum: 96.69234120845795
time_elpased: 1.898
batch start
#iterations: 125
currently lose_sum: 96.19321489334106
time_elpased: 1.903
batch start
#iterations: 126
currently lose_sum: 95.9110135436058
time_elpased: 1.886
batch start
#iterations: 127
currently lose_sum: 96.20157784223557
time_elpased: 1.924
batch start
#iterations: 128
currently lose_sum: 96.26459795236588
time_elpased: 1.89
batch start
#iterations: 129
currently lose_sum: 96.26853555440903
time_elpased: 1.94
batch start
#iterations: 130
currently lose_sum: 95.90758156776428
time_elpased: 1.898
batch start
#iterations: 131
currently lose_sum: 96.29654288291931
time_elpased: 1.918
batch start
#iterations: 132
currently lose_sum: 96.17106503248215
time_elpased: 1.893
batch start
#iterations: 133
currently lose_sum: 96.74950218200684
time_elpased: 1.898
batch start
#iterations: 134
currently lose_sum: 96.25061494112015
time_elpased: 1.918
batch start
#iterations: 135
currently lose_sum: 95.95429408550262
time_elpased: 1.915
batch start
#iterations: 136
currently lose_sum: 96.25887668132782
time_elpased: 1.882
batch start
#iterations: 137
currently lose_sum: 95.8256665468216
time_elpased: 1.893
batch start
#iterations: 138
currently lose_sum: 95.77243614196777
time_elpased: 1.911
batch start
#iterations: 139
currently lose_sum: 96.10289055109024
time_elpased: 1.881
start validation test
0.648402061856
0.613160362613
0.807450859319
0.697019499845
0.64812282716
62.153
batch start
#iterations: 140
currently lose_sum: 96.41945719718933
time_elpased: 1.945
batch start
#iterations: 141
currently lose_sum: 96.24717307090759
time_elpased: 1.901
batch start
#iterations: 142
currently lose_sum: 95.86624705791473
time_elpased: 1.913
batch start
#iterations: 143
currently lose_sum: 96.00591659545898
time_elpased: 1.902
batch start
#iterations: 144
currently lose_sum: 95.76957857608795
time_elpased: 1.88
batch start
#iterations: 145
currently lose_sum: 96.1983180642128
time_elpased: 1.912
batch start
#iterations: 146
currently lose_sum: 96.00388562679291
time_elpased: 1.892
batch start
#iterations: 147
currently lose_sum: 95.98512655496597
time_elpased: 1.913
batch start
#iterations: 148
currently lose_sum: 95.76159101724625
time_elpased: 1.88
batch start
#iterations: 149
currently lose_sum: 95.91953843832016
time_elpased: 1.935
batch start
#iterations: 150
currently lose_sum: 95.79303538799286
time_elpased: 1.898
batch start
#iterations: 151
currently lose_sum: 95.95157128572464
time_elpased: 1.893
batch start
#iterations: 152
currently lose_sum: 96.21247589588165
time_elpased: 1.887
batch start
#iterations: 153
currently lose_sum: 95.9637433886528
time_elpased: 1.897
batch start
#iterations: 154
currently lose_sum: 95.93075615167618
time_elpased: 1.883
batch start
#iterations: 155
currently lose_sum: 96.10896068811417
time_elpased: 1.906
batch start
#iterations: 156
currently lose_sum: 95.73614370822906
time_elpased: 1.884
batch start
#iterations: 157
currently lose_sum: 95.70944255590439
time_elpased: 1.892
batch start
#iterations: 158
currently lose_sum: 95.91959637403488
time_elpased: 1.886
batch start
#iterations: 159
currently lose_sum: 95.75903189182281
time_elpased: 1.913
start validation test
0.656237113402
0.631925207756
0.751260677164
0.686445060887
0.65607028488
61.370
batch start
#iterations: 160
currently lose_sum: 95.82530504465103
time_elpased: 1.959
batch start
#iterations: 161
currently lose_sum: 95.83076536655426
time_elpased: 1.942
batch start
#iterations: 162
currently lose_sum: 96.0314798951149
time_elpased: 1.898
batch start
#iterations: 163
currently lose_sum: 95.26936841011047
time_elpased: 1.912
batch start
#iterations: 164
currently lose_sum: 95.90510135889053
time_elpased: 1.908
batch start
#iterations: 165
currently lose_sum: 95.56076437234879
time_elpased: 1.922
batch start
#iterations: 166
currently lose_sum: 95.94424629211426
time_elpased: 1.89
batch start
#iterations: 167
currently lose_sum: 95.64775639772415
time_elpased: 1.927
batch start
#iterations: 168
currently lose_sum: 95.93440175056458
time_elpased: 1.916
batch start
#iterations: 169
currently lose_sum: 95.80671000480652
time_elpased: 1.924
batch start
#iterations: 170
currently lose_sum: 95.73384600877762
time_elpased: 1.968
batch start
#iterations: 171
currently lose_sum: 95.68572914600372
time_elpased: 1.993
batch start
#iterations: 172
currently lose_sum: 95.49320548772812
time_elpased: 1.914
batch start
#iterations: 173
currently lose_sum: 95.63249969482422
time_elpased: 2.072
batch start
#iterations: 174
currently lose_sum: 95.89871597290039
time_elpased: 1.935
batch start
#iterations: 175
currently lose_sum: 95.72345834970474
time_elpased: 1.934
batch start
#iterations: 176
currently lose_sum: 95.5005601644516
time_elpased: 1.918
batch start
#iterations: 177
currently lose_sum: 95.78441751003265
time_elpased: 1.981
batch start
#iterations: 178
currently lose_sum: 95.72917425632477
time_elpased: 2.019
batch start
#iterations: 179
currently lose_sum: 95.43083989620209
time_elpased: 1.975
start validation test
0.655257731959
0.619581523885
0.80755377174
0.701188455008
0.654990352771
61.297
batch start
#iterations: 180
currently lose_sum: 95.71281814575195
time_elpased: 1.954
batch start
#iterations: 181
currently lose_sum: 95.17527574300766
time_elpased: 1.981
batch start
#iterations: 182
currently lose_sum: 95.89396995306015
time_elpased: 1.974
batch start
#iterations: 183
currently lose_sum: 95.15715652704239
time_elpased: 1.993
batch start
#iterations: 184
currently lose_sum: 95.69662058353424
time_elpased: 1.956
batch start
#iterations: 185
currently lose_sum: 95.50070065259933
time_elpased: 1.952
batch start
#iterations: 186
currently lose_sum: 95.12641513347626
time_elpased: 2.017
batch start
#iterations: 187
currently lose_sum: 95.06481438875198
time_elpased: 1.978
batch start
#iterations: 188
currently lose_sum: 95.57775467634201
time_elpased: 1.958
batch start
#iterations: 189
currently lose_sum: 95.47586679458618
time_elpased: 1.919
batch start
#iterations: 190
currently lose_sum: 95.39750677347183
time_elpased: 1.932
batch start
#iterations: 191
currently lose_sum: 95.46627074480057
time_elpased: 1.936
batch start
#iterations: 192
currently lose_sum: 95.20521664619446
time_elpased: 1.903
batch start
#iterations: 193
currently lose_sum: 95.4385775923729
time_elpased: 1.948
batch start
#iterations: 194
currently lose_sum: 95.43079948425293
time_elpased: 1.923
batch start
#iterations: 195
currently lose_sum: 95.82024091482162
time_elpased: 1.939
batch start
#iterations: 196
currently lose_sum: 95.62531089782715
time_elpased: 1.895
batch start
#iterations: 197
currently lose_sum: 96.11686432361603
time_elpased: 1.906
batch start
#iterations: 198
currently lose_sum: 95.31988924741745
time_elpased: 1.908
batch start
#iterations: 199
currently lose_sum: 95.6801946759224
time_elpased: 1.933
start validation test
0.658659793814
0.636620464377
0.742101471648
0.685325983653
0.658513299079
61.555
batch start
#iterations: 200
currently lose_sum: 95.46144562959671
time_elpased: 1.976
batch start
#iterations: 201
currently lose_sum: 95.37237018346786
time_elpased: 2.052
batch start
#iterations: 202
currently lose_sum: 95.12104827165604
time_elpased: 1.945
batch start
#iterations: 203
currently lose_sum: 95.57417720556259
time_elpased: 1.891
batch start
#iterations: 204
currently lose_sum: 95.43351686000824
time_elpased: 1.949
batch start
#iterations: 205
currently lose_sum: 95.58951109647751
time_elpased: 1.903
batch start
#iterations: 206
currently lose_sum: 95.54249155521393
time_elpased: 1.896
batch start
#iterations: 207
currently lose_sum: 95.21755486726761
time_elpased: 1.929
batch start
#iterations: 208
currently lose_sum: 94.99681651592255
time_elpased: 1.89
batch start
#iterations: 209
currently lose_sum: 95.46464544534683
time_elpased: 1.905
batch start
#iterations: 210
currently lose_sum: 95.15420490503311
time_elpased: 1.933
batch start
#iterations: 211
currently lose_sum: 94.90687483549118
time_elpased: 1.939
batch start
#iterations: 212
currently lose_sum: 95.39817571640015
time_elpased: 1.911
batch start
#iterations: 213
currently lose_sum: 95.36895287036896
time_elpased: 1.932
batch start
#iterations: 214
currently lose_sum: 95.1654002070427
time_elpased: 1.926
batch start
#iterations: 215
currently lose_sum: 95.43489599227905
time_elpased: 1.903
batch start
#iterations: 216
currently lose_sum: 95.74721521139145
time_elpased: 1.932
batch start
#iterations: 217
currently lose_sum: 95.10709297657013
time_elpased: 1.967
batch start
#iterations: 218
currently lose_sum: 95.32045668363571
time_elpased: 1.891
batch start
#iterations: 219
currently lose_sum: 95.61070078611374
time_elpased: 1.907
start validation test
0.659381443299
0.630443903667
0.773181022949
0.694554867338
0.659181650585
61.130
batch start
#iterations: 220
currently lose_sum: 96.30977863073349
time_elpased: 1.959
batch start
#iterations: 221
currently lose_sum: 95.32346904277802
time_elpased: 1.918
batch start
#iterations: 222
currently lose_sum: 95.15482020378113
time_elpased: 1.914
batch start
#iterations: 223
currently lose_sum: 95.1015562415123
time_elpased: 1.905
batch start
#iterations: 224
currently lose_sum: 95.580371260643
time_elpased: 1.897
batch start
#iterations: 225
currently lose_sum: 95.28498184680939
time_elpased: 1.941
batch start
#iterations: 226
currently lose_sum: 95.16082155704498
time_elpased: 1.9
batch start
#iterations: 227
currently lose_sum: 95.61406522989273
time_elpased: 1.902
batch start
#iterations: 228
currently lose_sum: 95.1791620850563
time_elpased: 1.888
batch start
#iterations: 229
currently lose_sum: 95.23287630081177
time_elpased: 1.945
batch start
#iterations: 230
currently lose_sum: 94.70021331310272
time_elpased: 1.897
batch start
#iterations: 231
currently lose_sum: 95.31990319490433
time_elpased: 1.931
batch start
#iterations: 232
currently lose_sum: 95.03552007675171
time_elpased: 1.905
batch start
#iterations: 233
currently lose_sum: 95.16578578948975
time_elpased: 1.922
batch start
#iterations: 234
currently lose_sum: 95.37858307361603
time_elpased: 1.955
batch start
#iterations: 235
currently lose_sum: 95.22979801893234
time_elpased: 1.915
batch start
#iterations: 236
currently lose_sum: 95.4288232922554
time_elpased: 1.952
batch start
#iterations: 237
currently lose_sum: 94.82659369707108
time_elpased: 1.925
batch start
#iterations: 238
currently lose_sum: 95.18218952417374
time_elpased: 1.895
batch start
#iterations: 239
currently lose_sum: 95.16257721185684
time_elpased: 1.93
start validation test
0.662474226804
0.639591225443
0.747144180303
0.689196886273
0.662325575641
61.070
batch start
#iterations: 240
currently lose_sum: 95.04539740085602
time_elpased: 1.915
batch start
#iterations: 241
currently lose_sum: 95.21854943037033
time_elpased: 1.995
batch start
#iterations: 242
currently lose_sum: 94.8519988656044
time_elpased: 1.928
batch start
#iterations: 243
currently lose_sum: 95.12277215719223
time_elpased: 1.916
batch start
#iterations: 244
currently lose_sum: 95.37608116865158
time_elpased: 1.896
batch start
#iterations: 245
currently lose_sum: 95.13254636526108
time_elpased: 1.944
batch start
#iterations: 246
currently lose_sum: 95.31542259454727
time_elpased: 1.902
batch start
#iterations: 247
currently lose_sum: 95.36015892028809
time_elpased: 1.912
batch start
#iterations: 248
currently lose_sum: 95.73819309473038
time_elpased: 1.888
batch start
#iterations: 249
currently lose_sum: 94.93576711416245
time_elpased: 1.93
batch start
#iterations: 250
currently lose_sum: 95.20341664552689
time_elpased: 1.922
batch start
#iterations: 251
currently lose_sum: 95.07878130674362
time_elpased: 1.925
batch start
#iterations: 252
currently lose_sum: 94.642369389534
time_elpased: 1.929
batch start
#iterations: 253
currently lose_sum: 95.46167832612991
time_elpased: 1.953
batch start
#iterations: 254
currently lose_sum: 95.12664783000946
time_elpased: 1.89
batch start
#iterations: 255
currently lose_sum: 94.93897527456284
time_elpased: 1.946
batch start
#iterations: 256
currently lose_sum: 94.93291568756104
time_elpased: 1.905
batch start
#iterations: 257
currently lose_sum: 95.04987514019012
time_elpased: 1.992
batch start
#iterations: 258
currently lose_sum: 95.19831478595734
time_elpased: 1.915
batch start
#iterations: 259
currently lose_sum: 94.9547746181488
time_elpased: 1.946
start validation test
0.660412371134
0.629565217391
0.782340228466
0.69768722467
0.660198307974
60.893
batch start
#iterations: 260
currently lose_sum: 94.80665212869644
time_elpased: 1.961
batch start
#iterations: 261
currently lose_sum: 95.4232582449913
time_elpased: 1.921
batch start
#iterations: 262
currently lose_sum: 94.97875106334686
time_elpased: 1.882
batch start
#iterations: 263
currently lose_sum: 94.90188014507294
time_elpased: 1.917
batch start
#iterations: 264
currently lose_sum: 95.06357783079147
time_elpased: 1.895
batch start
#iterations: 265
currently lose_sum: 94.74562048912048
time_elpased: 1.957
batch start
#iterations: 266
currently lose_sum: 95.22878283262253
time_elpased: 1.97
batch start
#iterations: 267
currently lose_sum: 95.30807721614838
time_elpased: 1.944
batch start
#iterations: 268
currently lose_sum: 95.29281508922577
time_elpased: 1.897
batch start
#iterations: 269
currently lose_sum: 95.23992788791656
time_elpased: 1.942
batch start
#iterations: 270
currently lose_sum: 95.39062124490738
time_elpased: 1.91
batch start
#iterations: 271
currently lose_sum: 94.806425511837
time_elpased: 1.938
batch start
#iterations: 272
currently lose_sum: 94.72864127159119
time_elpased: 1.934
batch start
#iterations: 273
currently lose_sum: 95.31312763690948
time_elpased: 1.914
batch start
#iterations: 274
currently lose_sum: 95.30634945631027
time_elpased: 1.995
batch start
#iterations: 275
currently lose_sum: 95.32784658670425
time_elpased: 1.967
batch start
#iterations: 276
currently lose_sum: 94.95810681581497
time_elpased: 1.903
batch start
#iterations: 277
currently lose_sum: 95.61152762174606
time_elpased: 1.93
batch start
#iterations: 278
currently lose_sum: 95.27573823928833
time_elpased: 1.9
batch start
#iterations: 279
currently lose_sum: 95.43993544578552
time_elpased: 1.947
start validation test
0.66293814433
0.649679728711
0.709786971287
0.678404564009
0.662855893988
61.333
batch start
#iterations: 280
currently lose_sum: 94.94000780582428
time_elpased: 1.942
batch start
#iterations: 281
currently lose_sum: 94.7728961110115
time_elpased: 1.955
batch start
#iterations: 282
currently lose_sum: 94.92904371023178
time_elpased: 1.913
batch start
#iterations: 283
currently lose_sum: 95.60503578186035
time_elpased: 1.902
batch start
#iterations: 284
currently lose_sum: 94.60568857192993
time_elpased: 1.899
batch start
#iterations: 285
currently lose_sum: 95.02231734991074
time_elpased: 1.896
batch start
#iterations: 286
currently lose_sum: 94.85944133996964
time_elpased: 1.917
batch start
#iterations: 287
currently lose_sum: 95.00363427400589
time_elpased: 1.93
batch start
#iterations: 288
currently lose_sum: 94.68525123596191
time_elpased: 1.905
batch start
#iterations: 289
currently lose_sum: 94.95227938890457
time_elpased: 1.893
batch start
#iterations: 290
currently lose_sum: 94.74670332670212
time_elpased: 1.908
batch start
#iterations: 291
currently lose_sum: 94.684255361557
time_elpased: 1.902
batch start
#iterations: 292
currently lose_sum: 94.86102253198624
time_elpased: 1.9
batch start
#iterations: 293
currently lose_sum: 94.99592077732086
time_elpased: 1.905
batch start
#iterations: 294
currently lose_sum: 95.34737420082092
time_elpased: 1.89
batch start
#iterations: 295
currently lose_sum: 95.01516389846802
time_elpased: 1.928
batch start
#iterations: 296
currently lose_sum: 94.93980211019516
time_elpased: 1.882
batch start
#iterations: 297
currently lose_sum: 94.95861768722534
time_elpased: 1.894
batch start
#iterations: 298
currently lose_sum: 95.11520910263062
time_elpased: 1.888
batch start
#iterations: 299
currently lose_sum: 94.71462565660477
time_elpased: 1.924
start validation test
0.663041237113
0.632345596804
0.781825666358
0.699185495375
0.662832692727
60.848
batch start
#iterations: 300
currently lose_sum: 95.15151512622833
time_elpased: 1.938
batch start
#iterations: 301
currently lose_sum: 94.77001988887787
time_elpased: 1.905
batch start
#iterations: 302
currently lose_sum: 94.95562332868576
time_elpased: 1.887
batch start
#iterations: 303
currently lose_sum: 95.01050055027008
time_elpased: 1.905
batch start
#iterations: 304
currently lose_sum: 94.67018914222717
time_elpased: 1.913
batch start
#iterations: 305
currently lose_sum: 94.70965212583542
time_elpased: 1.906
batch start
#iterations: 306
currently lose_sum: 95.028036236763
time_elpased: 1.89
batch start
#iterations: 307
currently lose_sum: 94.96047675609589
time_elpased: 1.906
batch start
#iterations: 308
currently lose_sum: 94.77202343940735
time_elpased: 1.906
batch start
#iterations: 309
currently lose_sum: 95.30637884140015
time_elpased: 1.924
batch start
#iterations: 310
currently lose_sum: 94.95602458715439
time_elpased: 1.9
batch start
#iterations: 311
currently lose_sum: 94.69350683689117
time_elpased: 1.903
batch start
#iterations: 312
currently lose_sum: 94.81518685817719
time_elpased: 1.922
batch start
#iterations: 313
currently lose_sum: 94.72622656822205
time_elpased: 1.934
batch start
#iterations: 314
currently lose_sum: 94.9750867486
time_elpased: 1.925
batch start
#iterations: 315
currently lose_sum: 94.76631963253021
time_elpased: 1.927
batch start
#iterations: 316
currently lose_sum: 94.75751912593842
time_elpased: 1.949
batch start
#iterations: 317
currently lose_sum: 94.66443705558777
time_elpased: 1.954
batch start
#iterations: 318
currently lose_sum: 94.87531155347824
time_elpased: 1.928
batch start
#iterations: 319
currently lose_sum: 95.04430723190308
time_elpased: 1.929
start validation test
0.617731958763
0.661564387024
0.484820417824
0.559567644613
0.617965305473
62.106
batch start
#iterations: 320
currently lose_sum: 95.11117333173752
time_elpased: 1.932
batch start
#iterations: 321
currently lose_sum: 94.95624709129333
time_elpased: 1.927
batch start
#iterations: 322
currently lose_sum: 95.56271004676819
time_elpased: 1.898
batch start
#iterations: 323
currently lose_sum: 94.73151797056198
time_elpased: 1.964
batch start
#iterations: 324
currently lose_sum: 95.00420433282852
time_elpased: 1.932
batch start
#iterations: 325
currently lose_sum: 94.77783674001694
time_elpased: 1.92
batch start
#iterations: 326
currently lose_sum: 94.4463638663292
time_elpased: 1.902
batch start
#iterations: 327
currently lose_sum: 95.33917224407196
time_elpased: 1.911
batch start
#iterations: 328
currently lose_sum: 94.78506362438202
time_elpased: 1.916
batch start
#iterations: 329
currently lose_sum: 94.7713274359703
time_elpased: 1.922
batch start
#iterations: 330
currently lose_sum: 94.92598557472229
time_elpased: 1.899
batch start
#iterations: 331
currently lose_sum: 94.81114691495895
time_elpased: 1.913
batch start
#iterations: 332
currently lose_sum: 94.79855072498322
time_elpased: 1.891
batch start
#iterations: 333
currently lose_sum: 94.71822100877762
time_elpased: 1.949
batch start
#iterations: 334
currently lose_sum: 95.13045805692673
time_elpased: 1.869
batch start
#iterations: 335
currently lose_sum: 95.20138329267502
time_elpased: 1.929
batch start
#iterations: 336
currently lose_sum: 94.5006639957428
time_elpased: 1.942
batch start
#iterations: 337
currently lose_sum: 94.7873386144638
time_elpased: 1.931
batch start
#iterations: 338
currently lose_sum: 94.73273903131485
time_elpased: 1.919
batch start
#iterations: 339
currently lose_sum: 94.67009997367859
time_elpased: 1.933
start validation test
0.658505154639
0.644621141254
0.709169496758
0.67535649532
0.658416205572
61.210
batch start
#iterations: 340
currently lose_sum: 95.09881240129471
time_elpased: 1.967
batch start
#iterations: 341
currently lose_sum: 94.5889840722084
time_elpased: 1.937
batch start
#iterations: 342
currently lose_sum: 94.82103818655014
time_elpased: 1.925
batch start
#iterations: 343
currently lose_sum: 94.94574666023254
time_elpased: 1.985
batch start
#iterations: 344
currently lose_sum: 94.94202649593353
time_elpased: 1.92
batch start
#iterations: 345
currently lose_sum: 95.07080411911011
time_elpased: 1.955
batch start
#iterations: 346
currently lose_sum: 94.70949137210846
time_elpased: 1.976
batch start
#iterations: 347
currently lose_sum: 94.89312589168549
time_elpased: 1.93
batch start
#iterations: 348
currently lose_sum: 94.53619354963303
time_elpased: 1.919
batch start
#iterations: 349
currently lose_sum: 94.76380199193954
time_elpased: 1.941
batch start
#iterations: 350
currently lose_sum: 94.6287550330162
time_elpased: 1.915
batch start
#iterations: 351
currently lose_sum: 94.92379260063171
time_elpased: 1.958
batch start
#iterations: 352
currently lose_sum: 94.85310679674149
time_elpased: 1.906
batch start
#iterations: 353
currently lose_sum: 94.66649121046066
time_elpased: 1.97
batch start
#iterations: 354
currently lose_sum: 94.84155082702637
time_elpased: 1.988
batch start
#iterations: 355
currently lose_sum: 94.71062248945236
time_elpased: 1.942
batch start
#iterations: 356
currently lose_sum: 95.09973359107971
time_elpased: 1.947
batch start
#iterations: 357
currently lose_sum: 94.81843036413193
time_elpased: 1.945
batch start
#iterations: 358
currently lose_sum: 94.76464509963989
time_elpased: 1.934
batch start
#iterations: 359
currently lose_sum: 94.69600820541382
time_elpased: 1.897
start validation test
0.659793814433
0.633216514232
0.762375218689
0.691819200598
0.659613716956
61.361
batch start
#iterations: 360
currently lose_sum: 95.08211261034012
time_elpased: 1.946
batch start
#iterations: 361
currently lose_sum: 94.80883133411407
time_elpased: 1.946
batch start
#iterations: 362
currently lose_sum: 94.72020363807678
time_elpased: 1.945
batch start
#iterations: 363
currently lose_sum: 94.770647585392
time_elpased: 1.921
batch start
#iterations: 364
currently lose_sum: 95.21724599599838
time_elpased: 1.944
batch start
#iterations: 365
currently lose_sum: 94.70931845903397
time_elpased: 1.917
batch start
#iterations: 366
currently lose_sum: 95.09571766853333
time_elpased: 1.926
batch start
#iterations: 367
currently lose_sum: 94.93944227695465
time_elpased: 1.928
batch start
#iterations: 368
currently lose_sum: 94.51075792312622
time_elpased: 1.914
batch start
#iterations: 369
currently lose_sum: 95.06253403425217
time_elpased: 1.933
batch start
#iterations: 370
currently lose_sum: 94.45188069343567
time_elpased: 1.943
batch start
#iterations: 371
currently lose_sum: 94.56677609682083
time_elpased: 1.959
batch start
#iterations: 372
currently lose_sum: 94.40905952453613
time_elpased: 1.92
batch start
#iterations: 373
currently lose_sum: 94.63233286142349
time_elpased: 1.913
batch start
#iterations: 374
currently lose_sum: 94.68280416727066
time_elpased: 1.928
batch start
#iterations: 375
currently lose_sum: 94.61012625694275
time_elpased: 1.91
batch start
#iterations: 376
currently lose_sum: 94.58447986841202
time_elpased: 1.917
batch start
#iterations: 377
currently lose_sum: 94.54128241539001
time_elpased: 1.909
batch start
#iterations: 378
currently lose_sum: 94.64493483304977
time_elpased: 1.888
batch start
#iterations: 379
currently lose_sum: 94.79253417253494
time_elpased: 1.912
start validation test
0.658608247423
0.642738512641
0.716887928373
0.677791291657
0.658505928454
60.946
batch start
#iterations: 380
currently lose_sum: 94.60023421049118
time_elpased: 1.924
batch start
#iterations: 381
currently lose_sum: 94.57386392354965
time_elpased: 1.958
batch start
#iterations: 382
currently lose_sum: 94.68375557661057
time_elpased: 1.929
batch start
#iterations: 383
currently lose_sum: 94.76152956485748
time_elpased: 1.974
batch start
#iterations: 384
currently lose_sum: 95.43185478448868
time_elpased: 1.904
batch start
#iterations: 385
currently lose_sum: 94.34053122997284
time_elpased: 1.95
batch start
#iterations: 386
currently lose_sum: 94.60091120004654
time_elpased: 1.92
batch start
#iterations: 387
currently lose_sum: 94.82728242874146
time_elpased: 1.952
batch start
#iterations: 388
currently lose_sum: 94.72450691461563
time_elpased: 1.942
batch start
#iterations: 389
currently lose_sum: 94.95249271392822
time_elpased: 1.948
batch start
#iterations: 390
currently lose_sum: 94.63498085737228
time_elpased: 1.91
batch start
#iterations: 391
currently lose_sum: 94.77844488620758
time_elpased: 1.943
batch start
#iterations: 392
currently lose_sum: 94.335966527462
time_elpased: 1.935
batch start
#iterations: 393
currently lose_sum: 94.4927042722702
time_elpased: 1.963
batch start
#iterations: 394
currently lose_sum: 94.25177246332169
time_elpased: 1.937
batch start
#iterations: 395
currently lose_sum: 94.33340901136398
time_elpased: 1.942
batch start
#iterations: 396
currently lose_sum: 95.00781083106995
time_elpased: 1.909
batch start
#iterations: 397
currently lose_sum: 94.83119994401932
time_elpased: 1.929
batch start
#iterations: 398
currently lose_sum: 94.6152411699295
time_elpased: 1.889
batch start
#iterations: 399
currently lose_sum: 95.05085289478302
time_elpased: 1.935
start validation test
0.665206185567
0.638783597519
0.76309560564
0.695427901524
0.665034325592
60.718
acc: 0.665
pre: 0.639
rec: 0.762
F1: 0.695
auc: 0.665
