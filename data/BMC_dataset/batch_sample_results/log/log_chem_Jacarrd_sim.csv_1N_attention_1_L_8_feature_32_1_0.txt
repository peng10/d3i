start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.41565734148026
time_elpased: 1.852
batch start
#iterations: 1
currently lose_sum: 100.44232642650604
time_elpased: 1.885
batch start
#iterations: 2
currently lose_sum: 100.34839987754822
time_elpased: 1.777
batch start
#iterations: 3
currently lose_sum: 100.40258193016052
time_elpased: 1.788
batch start
#iterations: 4
currently lose_sum: 100.33095389604568
time_elpased: 1.814
batch start
#iterations: 5
currently lose_sum: 100.29276043176651
time_elpased: 1.807
batch start
#iterations: 6
currently lose_sum: 100.25300043821335
time_elpased: 1.908
batch start
#iterations: 7
currently lose_sum: 100.1860864162445
time_elpased: 1.8
batch start
#iterations: 8
currently lose_sum: 100.1814848780632
time_elpased: 1.815
batch start
#iterations: 9
currently lose_sum: 100.15974944829941
time_elpased: 1.781
batch start
#iterations: 10
currently lose_sum: 100.0379006266594
time_elpased: 1.816
batch start
#iterations: 11
currently lose_sum: 99.91307663917542
time_elpased: 1.842
batch start
#iterations: 12
currently lose_sum: 100.0303190946579
time_elpased: 1.843
batch start
#iterations: 13
currently lose_sum: 99.78886824846268
time_elpased: 1.876
batch start
#iterations: 14
currently lose_sum: 99.68752020597458
time_elpased: 1.884
batch start
#iterations: 15
currently lose_sum: 99.51794844865799
time_elpased: 1.852
batch start
#iterations: 16
currently lose_sum: 99.49339324235916
time_elpased: 1.799
batch start
#iterations: 17
currently lose_sum: 99.2760711312294
time_elpased: 1.883
batch start
#iterations: 18
currently lose_sum: 99.18734222650528
time_elpased: 1.77
batch start
#iterations: 19
currently lose_sum: 99.22315216064453
time_elpased: 1.778
start validation test
0.62293814433
0.62980977086
0.599670680251
0.614370815541
0.622978993952
64.922
batch start
#iterations: 20
currently lose_sum: 99.09939283132553
time_elpased: 1.769
batch start
#iterations: 21
currently lose_sum: 98.78101986646652
time_elpased: 1.771
batch start
#iterations: 22
currently lose_sum: 98.73739749193192
time_elpased: 1.79
batch start
#iterations: 23
currently lose_sum: 98.72801959514618
time_elpased: 1.777
batch start
#iterations: 24
currently lose_sum: 98.39571863412857
time_elpased: 1.762
batch start
#iterations: 25
currently lose_sum: 98.19782322645187
time_elpased: 1.811
batch start
#iterations: 26
currently lose_sum: 98.23548686504364
time_elpased: 1.759
batch start
#iterations: 27
currently lose_sum: 98.35610800981522
time_elpased: 1.759
batch start
#iterations: 28
currently lose_sum: 97.80046725273132
time_elpased: 1.785
batch start
#iterations: 29
currently lose_sum: 98.13947606086731
time_elpased: 1.788
batch start
#iterations: 30
currently lose_sum: 97.89207130670547
time_elpased: 1.77
batch start
#iterations: 31
currently lose_sum: 97.99436742067337
time_elpased: 1.752
batch start
#iterations: 32
currently lose_sum: 97.57494223117828
time_elpased: 1.778
batch start
#iterations: 33
currently lose_sum: 97.71843272447586
time_elpased: 1.751
batch start
#iterations: 34
currently lose_sum: 97.72870463132858
time_elpased: 1.776
batch start
#iterations: 35
currently lose_sum: 97.75804424285889
time_elpased: 1.747
batch start
#iterations: 36
currently lose_sum: 97.70484620332718
time_elpased: 1.761
batch start
#iterations: 37
currently lose_sum: 97.5916234254837
time_elpased: 1.756
batch start
#iterations: 38
currently lose_sum: 97.54794937372208
time_elpased: 1.757
batch start
#iterations: 39
currently lose_sum: 97.39238458871841
time_elpased: 1.749
start validation test
0.648195876289
0.643566322478
0.667078316353
0.655111425539
0.648162725253
62.909
batch start
#iterations: 40
currently lose_sum: 97.58757466077805
time_elpased: 1.807
batch start
#iterations: 41
currently lose_sum: 97.28826802968979
time_elpased: 1.768
batch start
#iterations: 42
currently lose_sum: 97.29181009531021
time_elpased: 1.832
batch start
#iterations: 43
currently lose_sum: 97.30985927581787
time_elpased: 1.845
batch start
#iterations: 44
currently lose_sum: 97.45590376853943
time_elpased: 1.876
batch start
#iterations: 45
currently lose_sum: 97.0831995010376
time_elpased: 1.838
batch start
#iterations: 46
currently lose_sum: 97.17510837316513
time_elpased: 1.945
batch start
#iterations: 47
currently lose_sum: 97.03084045648575
time_elpased: 1.812
batch start
#iterations: 48
currently lose_sum: 97.2082856297493
time_elpased: 1.903
batch start
#iterations: 49
currently lose_sum: 97.31246590614319
time_elpased: 1.856
batch start
#iterations: 50
currently lose_sum: 97.48078459501266
time_elpased: 1.778
batch start
#iterations: 51
currently lose_sum: 96.82899928092957
time_elpased: 1.865
batch start
#iterations: 52
currently lose_sum: 96.87708681821823
time_elpased: 1.84
batch start
#iterations: 53
currently lose_sum: 97.17240542173386
time_elpased: 1.79
batch start
#iterations: 54
currently lose_sum: 97.08788269758224
time_elpased: 1.798
batch start
#iterations: 55
currently lose_sum: 96.80493170022964
time_elpased: 1.81
batch start
#iterations: 56
currently lose_sum: 97.31148606538773
time_elpased: 1.868
batch start
#iterations: 57
currently lose_sum: 96.58897989988327
time_elpased: 1.876
batch start
#iterations: 58
currently lose_sum: 96.91500157117844
time_elpased: 1.753
batch start
#iterations: 59
currently lose_sum: 96.85905337333679
time_elpased: 1.863
start validation test
0.617010309278
0.65500881117
0.497272820829
0.565344565345
0.617220526908
63.025
batch start
#iterations: 60
currently lose_sum: 97.15443694591522
time_elpased: 1.843
batch start
#iterations: 61
currently lose_sum: 96.97895580530167
time_elpased: 1.801
batch start
#iterations: 62
currently lose_sum: 96.88117438554764
time_elpased: 1.819
batch start
#iterations: 63
currently lose_sum: 96.90389311313629
time_elpased: 1.757
batch start
#iterations: 64
currently lose_sum: 97.30090981721878
time_elpased: 1.788
batch start
#iterations: 65
currently lose_sum: 96.61918097734451
time_elpased: 1.746
batch start
#iterations: 66
currently lose_sum: 96.5804215669632
time_elpased: 1.751
batch start
#iterations: 67
currently lose_sum: 96.70956587791443
time_elpased: 1.762
batch start
#iterations: 68
currently lose_sum: 96.71833211183548
time_elpased: 1.745
batch start
#iterations: 69
currently lose_sum: 97.18261754512787
time_elpased: 1.766
batch start
#iterations: 70
currently lose_sum: 96.68865120410919
time_elpased: 1.765
batch start
#iterations: 71
currently lose_sum: 96.60921138525009
time_elpased: 1.756
batch start
#iterations: 72
currently lose_sum: 96.30947059392929
time_elpased: 1.746
batch start
#iterations: 73
currently lose_sum: 95.95427268743515
time_elpased: 1.814
batch start
#iterations: 74
currently lose_sum: 96.20478147268295
time_elpased: 1.81
batch start
#iterations: 75
currently lose_sum: 96.63486206531525
time_elpased: 1.77
batch start
#iterations: 76
currently lose_sum: 96.40073627233505
time_elpased: 1.772
batch start
#iterations: 77
currently lose_sum: 96.62763118743896
time_elpased: 1.751
batch start
#iterations: 78
currently lose_sum: 96.44920474290848
time_elpased: 1.773
batch start
#iterations: 79
currently lose_sum: 96.87614107131958
time_elpased: 1.773
start validation test
0.652835051546
0.642379679144
0.692291859627
0.666402496409
0.652765779034
62.333
batch start
#iterations: 80
currently lose_sum: 96.35345327854156
time_elpased: 1.864
batch start
#iterations: 81
currently lose_sum: 96.80924993753433
time_elpased: 1.753
batch start
#iterations: 82
currently lose_sum: 96.33839303255081
time_elpased: 1.773
batch start
#iterations: 83
currently lose_sum: 96.69743806123734
time_elpased: 1.758
batch start
#iterations: 84
currently lose_sum: 96.56580531597137
time_elpased: 1.781
batch start
#iterations: 85
currently lose_sum: 96.95037370920181
time_elpased: 1.76
batch start
#iterations: 86
currently lose_sum: 96.4240061044693
time_elpased: 1.762
batch start
#iterations: 87
currently lose_sum: 96.02853679656982
time_elpased: 1.763
batch start
#iterations: 88
currently lose_sum: 96.37269711494446
time_elpased: 1.759
batch start
#iterations: 89
currently lose_sum: 96.72977656126022
time_elpased: 1.756
batch start
#iterations: 90
currently lose_sum: 96.20768511295319
time_elpased: 1.759
batch start
#iterations: 91
currently lose_sum: 96.18192112445831
time_elpased: 1.761
batch start
#iterations: 92
currently lose_sum: 96.41342604160309
time_elpased: 1.75
batch start
#iterations: 93
currently lose_sum: 96.35024255514145
time_elpased: 1.767
batch start
#iterations: 94
currently lose_sum: 96.30515325069427
time_elpased: 1.773
batch start
#iterations: 95
currently lose_sum: 96.43628466129303
time_elpased: 1.752
batch start
#iterations: 96
currently lose_sum: 96.55847334861755
time_elpased: 1.759
batch start
#iterations: 97
currently lose_sum: 96.3097568154335
time_elpased: 1.738
batch start
#iterations: 98
currently lose_sum: 96.43200147151947
time_elpased: 1.757
batch start
#iterations: 99
currently lose_sum: 96.63480406999588
time_elpased: 1.795
start validation test
0.657164948454
0.638758146271
0.726252958732
0.67970142066
0.657043653795
61.862
batch start
#iterations: 100
currently lose_sum: 96.17794215679169
time_elpased: 1.785
batch start
#iterations: 101
currently lose_sum: 96.519580245018
time_elpased: 1.753
batch start
#iterations: 102
currently lose_sum: 96.14541494846344
time_elpased: 1.747
batch start
#iterations: 103
currently lose_sum: 96.49771857261658
time_elpased: 1.77
batch start
#iterations: 104
currently lose_sum: 95.95551109313965
time_elpased: 1.879
batch start
#iterations: 105
currently lose_sum: 95.81064480543137
time_elpased: 1.755
batch start
#iterations: 106
currently lose_sum: 96.35112249851227
time_elpased: 1.758
batch start
#iterations: 107
currently lose_sum: 96.12230378389359
time_elpased: 1.846
batch start
#iterations: 108
currently lose_sum: 96.09795498847961
time_elpased: 1.816
batch start
#iterations: 109
currently lose_sum: 95.94830030202866
time_elpased: 1.791
batch start
#iterations: 110
currently lose_sum: 96.46929037570953
time_elpased: 1.748
batch start
#iterations: 111
currently lose_sum: 95.99395430088043
time_elpased: 1.783
batch start
#iterations: 112
currently lose_sum: 96.51663738489151
time_elpased: 1.755
batch start
#iterations: 113
currently lose_sum: 95.74834531545639
time_elpased: 1.767
batch start
#iterations: 114
currently lose_sum: 96.51434773206711
time_elpased: 1.742
batch start
#iterations: 115
currently lose_sum: 96.2877476811409
time_elpased: 1.758
batch start
#iterations: 116
currently lose_sum: 95.77181839942932
time_elpased: 1.763
batch start
#iterations: 117
currently lose_sum: 96.24451851844788
time_elpased: 1.776
batch start
#iterations: 118
currently lose_sum: 96.09572368860245
time_elpased: 1.751
batch start
#iterations: 119
currently lose_sum: 96.17247134447098
time_elpased: 1.784
start validation test
0.645051546392
0.619988132576
0.752701451065
0.679929348331
0.64486255038
62.079
batch start
#iterations: 120
currently lose_sum: 96.00867730379105
time_elpased: 1.79
batch start
#iterations: 121
currently lose_sum: 95.9712695479393
time_elpased: 1.742
batch start
#iterations: 122
currently lose_sum: 96.1682054400444
time_elpased: 1.755
batch start
#iterations: 123
currently lose_sum: 96.08499109745026
time_elpased: 1.738
batch start
#iterations: 124
currently lose_sum: 96.36979073286057
time_elpased: 1.722
batch start
#iterations: 125
currently lose_sum: 96.18972623348236
time_elpased: 1.763
batch start
#iterations: 126
currently lose_sum: 96.07631087303162
time_elpased: 1.78
batch start
#iterations: 127
currently lose_sum: 96.17456513643265
time_elpased: 1.753
batch start
#iterations: 128
currently lose_sum: 96.15901923179626
time_elpased: 1.765
batch start
#iterations: 129
currently lose_sum: 96.20644807815552
time_elpased: 1.758
batch start
#iterations: 130
currently lose_sum: 95.74261647462845
time_elpased: 1.734
batch start
#iterations: 131
currently lose_sum: 96.32574188709259
time_elpased: 1.738
batch start
#iterations: 132
currently lose_sum: 95.93079733848572
time_elpased: 1.752
batch start
#iterations: 133
currently lose_sum: 96.15575397014618
time_elpased: 1.768
batch start
#iterations: 134
currently lose_sum: 95.7082988023758
time_elpased: 1.784
batch start
#iterations: 135
currently lose_sum: 95.83669859170914
time_elpased: 1.74
batch start
#iterations: 136
currently lose_sum: 96.36214208602905
time_elpased: 1.777
batch start
#iterations: 137
currently lose_sum: 95.54219573736191
time_elpased: 1.788
batch start
#iterations: 138
currently lose_sum: 95.78309279680252
time_elpased: 1.765
batch start
#iterations: 139
currently lose_sum: 96.11930721998215
time_elpased: 1.75
start validation test
0.650309278351
0.614009173599
0.812802305238
0.699557130204
0.650023996779
61.738
batch start
#iterations: 140
currently lose_sum: 95.87046229839325
time_elpased: 1.751
batch start
#iterations: 141
currently lose_sum: 96.23371976613998
time_elpased: 1.782
batch start
#iterations: 142
currently lose_sum: 95.70641446113586
time_elpased: 1.792
batch start
#iterations: 143
currently lose_sum: 96.09912246465683
time_elpased: 1.776
batch start
#iterations: 144
currently lose_sum: 95.47456288337708
time_elpased: 1.759
batch start
#iterations: 145
currently lose_sum: 96.15133380889893
time_elpased: 1.765
batch start
#iterations: 146
currently lose_sum: 95.80291479825974
time_elpased: 1.765
batch start
#iterations: 147
currently lose_sum: 95.75478512048721
time_elpased: 1.763
batch start
#iterations: 148
currently lose_sum: 95.58956050872803
time_elpased: 1.788
batch start
#iterations: 149
currently lose_sum: 95.89157426357269
time_elpased: 1.76
batch start
#iterations: 150
currently lose_sum: 95.94445782899857
time_elpased: 1.78
batch start
#iterations: 151
currently lose_sum: 96.14416408538818
time_elpased: 1.75
batch start
#iterations: 152
currently lose_sum: 95.50898200273514
time_elpased: 1.77
batch start
#iterations: 153
currently lose_sum: 96.24111491441727
time_elpased: 1.744
batch start
#iterations: 154
currently lose_sum: 96.05228644609451
time_elpased: 1.754
batch start
#iterations: 155
currently lose_sum: 96.10046064853668
time_elpased: 1.801
batch start
#iterations: 156
currently lose_sum: 95.2780413031578
time_elpased: 1.782
batch start
#iterations: 157
currently lose_sum: 95.62704282999039
time_elpased: 1.759
batch start
#iterations: 158
currently lose_sum: 95.69432646036148
time_elpased: 1.762
batch start
#iterations: 159
currently lose_sum: 95.86934268474579
time_elpased: 1.767
start validation test
0.656082474227
0.634342186535
0.739837398374
0.683040380048
0.655935429539
61.533
batch start
#iterations: 160
currently lose_sum: 95.67156285047531
time_elpased: 1.822
batch start
#iterations: 161
currently lose_sum: 95.79885029792786
time_elpased: 1.75
batch start
#iterations: 162
currently lose_sum: 95.30862146615982
time_elpased: 1.77
batch start
#iterations: 163
currently lose_sum: 95.58391016721725
time_elpased: 1.764
batch start
#iterations: 164
currently lose_sum: 96.39515191316605
time_elpased: 1.776
batch start
#iterations: 165
currently lose_sum: 95.53424513339996
time_elpased: 1.773
batch start
#iterations: 166
currently lose_sum: 95.83192479610443
time_elpased: 1.762
batch start
#iterations: 167
currently lose_sum: 95.65946906805038
time_elpased: 1.756
batch start
#iterations: 168
currently lose_sum: 95.5330440402031
time_elpased: 1.768
batch start
#iterations: 169
currently lose_sum: 95.52629536390305
time_elpased: 1.79
batch start
#iterations: 170
currently lose_sum: 95.77515852451324
time_elpased: 1.745
batch start
#iterations: 171
currently lose_sum: 95.79100096225739
time_elpased: 1.756
batch start
#iterations: 172
currently lose_sum: 95.52800440788269
time_elpased: 1.781
batch start
#iterations: 173
currently lose_sum: 95.59239637851715
time_elpased: 1.871
batch start
#iterations: 174
currently lose_sum: 95.31341880559921
time_elpased: 1.766
batch start
#iterations: 175
currently lose_sum: 95.97575187683105
time_elpased: 1.753
batch start
#iterations: 176
currently lose_sum: 95.62554335594177
time_elpased: 1.871
batch start
#iterations: 177
currently lose_sum: 95.81057643890381
time_elpased: 1.874
batch start
#iterations: 178
currently lose_sum: 95.77434641122818
time_elpased: 1.777
batch start
#iterations: 179
currently lose_sum: 95.27331644296646
time_elpased: 1.762
start validation test
0.658041237113
0.646321784528
0.700730678193
0.672427414576
0.657966289215
61.495
batch start
#iterations: 180
currently lose_sum: 95.37935829162598
time_elpased: 1.755
batch start
#iterations: 181
currently lose_sum: 95.13357138633728
time_elpased: 1.76
batch start
#iterations: 182
currently lose_sum: 95.29073375463486
time_elpased: 1.756
batch start
#iterations: 183
currently lose_sum: 95.1802990436554
time_elpased: 1.742
batch start
#iterations: 184
currently lose_sum: 95.41289710998535
time_elpased: 1.769
batch start
#iterations: 185
currently lose_sum: 95.28651612997055
time_elpased: 1.765
batch start
#iterations: 186
currently lose_sum: 95.03687369823456
time_elpased: 1.758
batch start
#iterations: 187
currently lose_sum: 95.07411152124405
time_elpased: 1.818
batch start
#iterations: 188
currently lose_sum: 95.35477322340012
time_elpased: 1.751
batch start
#iterations: 189
currently lose_sum: 95.47379422187805
time_elpased: 1.764
batch start
#iterations: 190
currently lose_sum: 95.16609525680542
time_elpased: 1.811
batch start
#iterations: 191
currently lose_sum: 95.29097747802734
time_elpased: 1.765
batch start
#iterations: 192
currently lose_sum: 95.0568243265152
time_elpased: 1.776
batch start
#iterations: 193
currently lose_sum: 95.38252133131027
time_elpased: 1.756
batch start
#iterations: 194
currently lose_sum: 95.2534516453743
time_elpased: 1.761
batch start
#iterations: 195
currently lose_sum: 95.75516802072525
time_elpased: 1.775
batch start
#iterations: 196
currently lose_sum: 95.36907863616943
time_elpased: 1.843
batch start
#iterations: 197
currently lose_sum: 95.68899005651474
time_elpased: 1.842
batch start
#iterations: 198
currently lose_sum: 95.17110878229141
time_elpased: 1.86
batch start
#iterations: 199
currently lose_sum: 95.83951085805893
time_elpased: 1.876
start validation test
0.65381443299
0.636297574712
0.720901512813
0.675962559105
0.653696651274
61.422
batch start
#iterations: 200
currently lose_sum: 95.55206191539764
time_elpased: 1.947
batch start
#iterations: 201
currently lose_sum: 95.15050089359283
time_elpased: 1.84
batch start
#iterations: 202
currently lose_sum: 95.49222272634506
time_elpased: 1.938
batch start
#iterations: 203
currently lose_sum: 95.51397287845612
time_elpased: 1.866
batch start
#iterations: 204
currently lose_sum: 95.25662499666214
time_elpased: 1.879
batch start
#iterations: 205
currently lose_sum: 95.50503206253052
time_elpased: 1.843
batch start
#iterations: 206
currently lose_sum: 95.64273631572723
time_elpased: 1.751
batch start
#iterations: 207
currently lose_sum: 95.26697957515717
time_elpased: 1.728
batch start
#iterations: 208
currently lose_sum: 95.46505337953568
time_elpased: 1.763
batch start
#iterations: 209
currently lose_sum: 95.120276927948
time_elpased: 1.77
batch start
#iterations: 210
currently lose_sum: 95.19240880012512
time_elpased: 1.795
batch start
#iterations: 211
currently lose_sum: 95.27916669845581
time_elpased: 1.825
batch start
#iterations: 212
currently lose_sum: 94.96460103988647
time_elpased: 1.763
batch start
#iterations: 213
currently lose_sum: 94.63977789878845
time_elpased: 1.79
batch start
#iterations: 214
currently lose_sum: 94.99999576807022
time_elpased: 1.763
batch start
#iterations: 215
currently lose_sum: 95.10896188020706
time_elpased: 1.754
batch start
#iterations: 216
currently lose_sum: 95.24528187513351
time_elpased: 1.754
batch start
#iterations: 217
currently lose_sum: 95.31488823890686
time_elpased: 1.797
batch start
#iterations: 218
currently lose_sum: 95.26201725006104
time_elpased: 1.787
batch start
#iterations: 219
currently lose_sum: 95.371013879776
time_elpased: 1.776
start validation test
0.657989690722
0.616919575114
0.836780899455
0.710224046818
0.657675795178
61.076
batch start
#iterations: 220
currently lose_sum: 95.82275485992432
time_elpased: 1.789
batch start
#iterations: 221
currently lose_sum: 95.50381129980087
time_elpased: 1.742
batch start
#iterations: 222
currently lose_sum: 95.31944978237152
time_elpased: 1.776
batch start
#iterations: 223
currently lose_sum: 95.4896222949028
time_elpased: 1.76
batch start
#iterations: 224
currently lose_sum: 95.42671304941177
time_elpased: 1.761
batch start
#iterations: 225
currently lose_sum: 95.02694082260132
time_elpased: 1.793
batch start
#iterations: 226
currently lose_sum: 95.41754430532455
time_elpased: 1.793
batch start
#iterations: 227
currently lose_sum: 95.18997794389725
time_elpased: 1.739
batch start
#iterations: 228
currently lose_sum: 95.14556121826172
time_elpased: 1.766
batch start
#iterations: 229
currently lose_sum: 95.65746533870697
time_elpased: 1.77
batch start
#iterations: 230
currently lose_sum: 95.21878153085709
time_elpased: 1.764
batch start
#iterations: 231
currently lose_sum: 95.11459124088287
time_elpased: 1.76
batch start
#iterations: 232
currently lose_sum: 94.9578332901001
time_elpased: 1.774
batch start
#iterations: 233
currently lose_sum: 95.10433965921402
time_elpased: 1.787
batch start
#iterations: 234
currently lose_sum: 95.67983162403107
time_elpased: 1.836
batch start
#iterations: 235
currently lose_sum: 95.11063545942307
time_elpased: 1.85
batch start
#iterations: 236
currently lose_sum: 95.16804206371307
time_elpased: 1.869
batch start
#iterations: 237
currently lose_sum: 95.39294421672821
time_elpased: 1.825
batch start
#iterations: 238
currently lose_sum: 94.98683232069016
time_elpased: 1.9
batch start
#iterations: 239
currently lose_sum: 95.22938460111618
time_elpased: 1.841
start validation test
0.655206185567
0.64687621265
0.686220026757
0.665967540574
0.655151735985
61.510
batch start
#iterations: 240
currently lose_sum: 94.72557187080383
time_elpased: 1.864
batch start
#iterations: 241
currently lose_sum: 95.19218689203262
time_elpased: 1.838
batch start
#iterations: 242
currently lose_sum: 94.86125963926315
time_elpased: 1.842
batch start
#iterations: 243
currently lose_sum: 94.7444611787796
time_elpased: 1.816
batch start
#iterations: 244
currently lose_sum: 95.0959797501564
time_elpased: 1.905
batch start
#iterations: 245
currently lose_sum: 95.47084307670593
time_elpased: 1.797
batch start
#iterations: 246
currently lose_sum: 95.35848665237427
time_elpased: 1.852
batch start
#iterations: 247
currently lose_sum: 95.16064435243607
time_elpased: 1.847
batch start
#iterations: 248
currently lose_sum: 95.32485860586166
time_elpased: 1.776
batch start
#iterations: 249
currently lose_sum: 95.29940289258957
time_elpased: 1.797
batch start
#iterations: 250
currently lose_sum: 95.2504615187645
time_elpased: 1.843
batch start
#iterations: 251
currently lose_sum: 95.24766850471497
time_elpased: 1.81
batch start
#iterations: 252
currently lose_sum: 95.1904531121254
time_elpased: 1.894
batch start
#iterations: 253
currently lose_sum: 95.14446949958801
time_elpased: 1.877
batch start
#iterations: 254
currently lose_sum: 95.06528329849243
time_elpased: 1.86
batch start
#iterations: 255
currently lose_sum: 95.1818385720253
time_elpased: 1.745
batch start
#iterations: 256
currently lose_sum: 94.93617969751358
time_elpased: 1.844
batch start
#iterations: 257
currently lose_sum: 95.13155859708786
time_elpased: 1.79
batch start
#iterations: 258
currently lose_sum: 94.82903558015823
time_elpased: 1.816
batch start
#iterations: 259
currently lose_sum: 95.08726418018341
time_elpased: 1.846
start validation test
0.662577319588
0.630290081354
0.78933827313
0.700904687928
0.662354771182
60.978
batch start
#iterations: 260
currently lose_sum: 94.73382645845413
time_elpased: 1.824
batch start
#iterations: 261
currently lose_sum: 95.28114855289459
time_elpased: 1.792
batch start
#iterations: 262
currently lose_sum: 95.04163110256195
time_elpased: 1.802
batch start
#iterations: 263
currently lose_sum: 94.76905232667923
time_elpased: 1.834
batch start
#iterations: 264
currently lose_sum: 95.5595645904541
time_elpased: 1.85
batch start
#iterations: 265
currently lose_sum: 94.74563366174698
time_elpased: 1.8
batch start
#iterations: 266
currently lose_sum: 95.09412997961044
time_elpased: 1.868
batch start
#iterations: 267
currently lose_sum: 94.84507489204407
time_elpased: 1.851
batch start
#iterations: 268
currently lose_sum: 94.64434599876404
time_elpased: 1.853
batch start
#iterations: 269
currently lose_sum: 95.44822663068771
time_elpased: 1.847
batch start
#iterations: 270
currently lose_sum: 95.3101897239685
time_elpased: 1.831
batch start
#iterations: 271
currently lose_sum: 94.95600378513336
time_elpased: 1.82
batch start
#iterations: 272
currently lose_sum: 94.95198899507523
time_elpased: 1.887
batch start
#iterations: 273
currently lose_sum: 95.11372119188309
time_elpased: 1.794
batch start
#iterations: 274
currently lose_sum: 94.63188380002975
time_elpased: 1.796
batch start
#iterations: 275
currently lose_sum: 94.94090312719345
time_elpased: 1.842
batch start
#iterations: 276
currently lose_sum: 95.56430643796921
time_elpased: 1.822
batch start
#iterations: 277
currently lose_sum: 95.42806655168533
time_elpased: 1.817
batch start
#iterations: 278
currently lose_sum: 95.08745014667511
time_elpased: 1.772
batch start
#iterations: 279
currently lose_sum: 95.02158284187317
time_elpased: 1.858
start validation test
0.660824742268
0.62921163193
0.786045075641
0.698938506589
0.660604898659
61.170
batch start
#iterations: 280
currently lose_sum: 95.38639146089554
time_elpased: 1.83
batch start
#iterations: 281
currently lose_sum: 95.22073483467102
time_elpased: 1.779
batch start
#iterations: 282
currently lose_sum: 95.14066553115845
time_elpased: 1.801
batch start
#iterations: 283
currently lose_sum: 95.13525259494781
time_elpased: 1.785
batch start
#iterations: 284
currently lose_sum: 94.66953027248383
time_elpased: 1.785
batch start
#iterations: 285
currently lose_sum: 94.81255131959915
time_elpased: 1.781
batch start
#iterations: 286
currently lose_sum: 94.98592209815979
time_elpased: 1.783
batch start
#iterations: 287
currently lose_sum: 95.13250994682312
time_elpased: 1.783
batch start
#iterations: 288
currently lose_sum: 94.68327409029007
time_elpased: 1.79
batch start
#iterations: 289
currently lose_sum: 95.23174595832825
time_elpased: 1.799
batch start
#iterations: 290
currently lose_sum: 95.19560778141022
time_elpased: 1.907
batch start
#iterations: 291
currently lose_sum: 94.54895430803299
time_elpased: 1.822
batch start
#iterations: 292
currently lose_sum: 94.4256534576416
time_elpased: 1.797
batch start
#iterations: 293
currently lose_sum: 94.81296610832214
time_elpased: 1.816
batch start
#iterations: 294
currently lose_sum: 94.72580701112747
time_elpased: 1.796
batch start
#iterations: 295
currently lose_sum: 94.91884398460388
time_elpased: 1.824
batch start
#iterations: 296
currently lose_sum: 94.7851642370224
time_elpased: 1.784
batch start
#iterations: 297
currently lose_sum: 95.14352822303772
time_elpased: 1.8
batch start
#iterations: 298
currently lose_sum: 95.23638188838959
time_elpased: 1.838
batch start
#iterations: 299
currently lose_sum: 94.46025860309601
time_elpased: 1.786
start validation test
0.665463917526
0.63605700312
0.776268395595
0.699202817946
0.665269383174
60.854
batch start
#iterations: 300
currently lose_sum: 95.14691072702408
time_elpased: 1.827
batch start
#iterations: 301
currently lose_sum: 95.1321011185646
time_elpased: 1.812
batch start
#iterations: 302
currently lose_sum: 94.87172955274582
time_elpased: 1.86
batch start
#iterations: 303
currently lose_sum: 94.64640486240387
time_elpased: 1.807
batch start
#iterations: 304
currently lose_sum: 94.46298444271088
time_elpased: 1.796
batch start
#iterations: 305
currently lose_sum: 94.95631045103073
time_elpased: 1.801
batch start
#iterations: 306
currently lose_sum: 95.18020069599152
time_elpased: 1.812
batch start
#iterations: 307
currently lose_sum: 94.9138388633728
time_elpased: 1.906
batch start
#iterations: 308
currently lose_sum: 94.74461776018143
time_elpased: 1.894
batch start
#iterations: 309
currently lose_sum: 95.0192003250122
time_elpased: 1.953
batch start
#iterations: 310
currently lose_sum: 94.8177347779274
time_elpased: 1.864
batch start
#iterations: 311
currently lose_sum: 94.99731719493866
time_elpased: 1.837
batch start
#iterations: 312
currently lose_sum: 94.79412907361984
time_elpased: 1.841
batch start
#iterations: 313
currently lose_sum: 94.38445019721985
time_elpased: 1.775
batch start
#iterations: 314
currently lose_sum: 94.65975016355515
time_elpased: 1.898
batch start
#iterations: 315
currently lose_sum: 94.78498721122742
time_elpased: 1.835
batch start
#iterations: 316
currently lose_sum: 95.27012133598328
time_elpased: 1.852
batch start
#iterations: 317
currently lose_sum: 94.79763960838318
time_elpased: 1.798
batch start
#iterations: 318
currently lose_sum: 94.78071284294128
time_elpased: 1.782
batch start
#iterations: 319
currently lose_sum: 94.6097885966301
time_elpased: 1.831
start validation test
0.660051546392
0.645969702637
0.710919007924
0.676889912302
0.659962240717
61.162
batch start
#iterations: 320
currently lose_sum: 94.84723895788193
time_elpased: 1.954
batch start
#iterations: 321
currently lose_sum: 94.66531133651733
time_elpased: 1.834
batch start
#iterations: 322
currently lose_sum: 95.18651306629181
time_elpased: 1.803
batch start
#iterations: 323
currently lose_sum: 94.58651912212372
time_elpased: 1.795
batch start
#iterations: 324
currently lose_sum: 95.42517453432083
time_elpased: 1.793
batch start
#iterations: 325
currently lose_sum: 95.35772424936295
time_elpased: 1.821
batch start
#iterations: 326
currently lose_sum: 94.38654619455338
time_elpased: 1.824
batch start
#iterations: 327
currently lose_sum: 94.72617137432098
time_elpased: 1.872
batch start
#iterations: 328
currently lose_sum: 94.66383135318756
time_elpased: 1.793
batch start
#iterations: 329
currently lose_sum: 95.14124095439911
time_elpased: 1.895
batch start
#iterations: 330
currently lose_sum: 94.97930508852005
time_elpased: 1.831
batch start
#iterations: 331
currently lose_sum: 94.76224619150162
time_elpased: 1.841
batch start
#iterations: 332
currently lose_sum: 94.61527627706528
time_elpased: 1.804
batch start
#iterations: 333
currently lose_sum: 94.93979543447495
time_elpased: 1.861
batch start
#iterations: 334
currently lose_sum: 95.00078547000885
time_elpased: 1.847
batch start
#iterations: 335
currently lose_sum: 95.04438251256943
time_elpased: 1.805
batch start
#iterations: 336
currently lose_sum: 94.47866529226303
time_elpased: 1.801
batch start
#iterations: 337
currently lose_sum: 95.15078663825989
time_elpased: 1.778
batch start
#iterations: 338
currently lose_sum: 94.78055334091187
time_elpased: 1.814
batch start
#iterations: 339
currently lose_sum: 94.47123610973358
time_elpased: 1.888
start validation test
0.662577319588
0.642568114378
0.735412164248
0.68586236683
0.662449446784
60.938
batch start
#iterations: 340
currently lose_sum: 94.65344601869583
time_elpased: 1.868
batch start
#iterations: 341
currently lose_sum: 94.73847144842148
time_elpased: 1.837
batch start
#iterations: 342
currently lose_sum: 95.07479494810104
time_elpased: 1.817
batch start
#iterations: 343
currently lose_sum: 94.74352544546127
time_elpased: 1.899
batch start
#iterations: 344
currently lose_sum: 94.50509536266327
time_elpased: 1.868
batch start
#iterations: 345
currently lose_sum: 94.88461619615555
time_elpased: 1.851
batch start
#iterations: 346
currently lose_sum: 94.62060582637787
time_elpased: 1.827
batch start
#iterations: 347
currently lose_sum: 94.79603058099747
time_elpased: 1.805
batch start
#iterations: 348
currently lose_sum: 94.7674663066864
time_elpased: 1.827
batch start
#iterations: 349
currently lose_sum: 94.96098148822784
time_elpased: 1.833
batch start
#iterations: 350
currently lose_sum: 94.46002572774887
time_elpased: 1.821
batch start
#iterations: 351
currently lose_sum: 94.71784061193466
time_elpased: 1.815
batch start
#iterations: 352
currently lose_sum: 94.63705033063889
time_elpased: 1.841
batch start
#iterations: 353
currently lose_sum: 94.79804509878159
time_elpased: 1.847
batch start
#iterations: 354
currently lose_sum: 94.87730956077576
time_elpased: 1.793
batch start
#iterations: 355
currently lose_sum: 94.7534145116806
time_elpased: 1.843
batch start
#iterations: 356
currently lose_sum: 94.88866353034973
time_elpased: 1.831
batch start
#iterations: 357
currently lose_sum: 94.91881710290909
time_elpased: 1.842
batch start
#iterations: 358
currently lose_sum: 94.93280655145645
time_elpased: 1.824
batch start
#iterations: 359
currently lose_sum: 94.586330473423
time_elpased: 1.797
start validation test
0.657422680412
0.639047360319
0.726252958732
0.679865125241
0.657301838242
61.257
batch start
#iterations: 360
currently lose_sum: 94.78989797830582
time_elpased: 1.857
batch start
#iterations: 361
currently lose_sum: 94.79780477285385
time_elpased: 1.821
batch start
#iterations: 362
currently lose_sum: 94.34743010997772
time_elpased: 1.77
batch start
#iterations: 363
currently lose_sum: 94.65286242961884
time_elpased: 1.811
batch start
#iterations: 364
currently lose_sum: 94.74812877178192
time_elpased: 1.827
batch start
#iterations: 365
currently lose_sum: 95.02478259801865
time_elpased: 1.786
batch start
#iterations: 366
currently lose_sum: 94.67078852653503
time_elpased: 1.772
batch start
#iterations: 367
currently lose_sum: 95.17858016490936
time_elpased: 1.835
batch start
#iterations: 368
currently lose_sum: 94.97072106599808
time_elpased: 1.785
batch start
#iterations: 369
currently lose_sum: 94.83447766304016
time_elpased: 1.812
batch start
#iterations: 370
currently lose_sum: 94.3438555598259
time_elpased: 1.805
batch start
#iterations: 371
currently lose_sum: 94.75318777561188
time_elpased: 1.838
batch start
#iterations: 372
currently lose_sum: 94.72031855583191
time_elpased: 1.809
batch start
#iterations: 373
currently lose_sum: 94.6560692191124
time_elpased: 1.807
batch start
#iterations: 374
currently lose_sum: 94.54827505350113
time_elpased: 1.787
batch start
#iterations: 375
currently lose_sum: 94.81784862279892
time_elpased: 1.901
batch start
#iterations: 376
currently lose_sum: 94.7458798289299
time_elpased: 1.832
batch start
#iterations: 377
currently lose_sum: 94.51737093925476
time_elpased: 1.858
batch start
#iterations: 378
currently lose_sum: 94.49644380807877
time_elpased: 1.927
batch start
#iterations: 379
currently lose_sum: 94.90176594257355
time_elpased: 1.838
start validation test
0.656391752577
0.635732716434
0.735309251827
0.681904943692
0.656253200735
61.458
batch start
#iterations: 380
currently lose_sum: 94.68063926696777
time_elpased: 1.851
batch start
#iterations: 381
currently lose_sum: 94.67698848247528
time_elpased: 1.801
batch start
#iterations: 382
currently lose_sum: 94.64057117700577
time_elpased: 1.788
batch start
#iterations: 383
currently lose_sum: 94.50950717926025
time_elpased: 1.815
batch start
#iterations: 384
currently lose_sum: 95.06455045938492
time_elpased: 1.898
batch start
#iterations: 385
currently lose_sum: 94.56070858240128
time_elpased: 1.814
batch start
#iterations: 386
currently lose_sum: 94.87285888195038
time_elpased: 1.924
batch start
#iterations: 387
currently lose_sum: 94.9424814581871
time_elpased: 1.786
batch start
#iterations: 388
currently lose_sum: 94.49220287799835
time_elpased: 1.843
batch start
#iterations: 389
currently lose_sum: 95.10589951276779
time_elpased: 1.779
batch start
#iterations: 390
currently lose_sum: 94.76336079835892
time_elpased: 1.812
batch start
#iterations: 391
currently lose_sum: 94.75098198652267
time_elpased: 1.948
batch start
#iterations: 392
currently lose_sum: 94.54985111951828
time_elpased: 1.801
batch start
#iterations: 393
currently lose_sum: 94.63812589645386
time_elpased: 1.796
batch start
#iterations: 394
currently lose_sum: 94.55981862545013
time_elpased: 1.81
batch start
#iterations: 395
currently lose_sum: 94.18460994958878
time_elpased: 1.889
batch start
#iterations: 396
currently lose_sum: 94.61052906513214
time_elpased: 1.853
batch start
#iterations: 397
currently lose_sum: 94.2955556511879
time_elpased: 1.831
batch start
#iterations: 398
currently lose_sum: 94.32100045681
time_elpased: 1.856
batch start
#iterations: 399
currently lose_sum: 94.79619497060776
time_elpased: 1.781
start validation test
0.65618556701
0.644147979941
0.700627765771
0.671201814059
0.656107541876
61.026
acc: 0.661
pre: 0.632
rec: 0.777
F1: 0.697
auc: 0.661
