start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.30877649784088
time_elpased: 1.98
batch start
#iterations: 1
currently lose_sum: 100.48427307605743
time_elpased: 1.858
batch start
#iterations: 2
currently lose_sum: 100.37102794647217
time_elpased: 1.849
batch start
#iterations: 3
currently lose_sum: 100.33179610967636
time_elpased: 1.854
batch start
#iterations: 4
currently lose_sum: 100.24887669086456
time_elpased: 1.878
batch start
#iterations: 5
currently lose_sum: 100.20402836799622
time_elpased: 1.842
batch start
#iterations: 6
currently lose_sum: 100.09367197751999
time_elpased: 1.848
batch start
#iterations: 7
currently lose_sum: 100.05254828929901
time_elpased: 1.871
batch start
#iterations: 8
currently lose_sum: 99.87622159719467
time_elpased: 1.835
batch start
#iterations: 9
currently lose_sum: 99.98633754253387
time_elpased: 1.857
batch start
#iterations: 10
currently lose_sum: 99.85734206438065
time_elpased: 1.84
batch start
#iterations: 11
currently lose_sum: 99.75822186470032
time_elpased: 1.84
batch start
#iterations: 12
currently lose_sum: 99.76972395181656
time_elpased: 1.853
batch start
#iterations: 13
currently lose_sum: 99.4142050743103
time_elpased: 1.834
batch start
#iterations: 14
currently lose_sum: 99.3734022974968
time_elpased: 1.815
batch start
#iterations: 15
currently lose_sum: 99.38344585895538
time_elpased: 1.843
batch start
#iterations: 16
currently lose_sum: 99.23093038797379
time_elpased: 1.83
batch start
#iterations: 17
currently lose_sum: 99.09052610397339
time_elpased: 1.841
batch start
#iterations: 18
currently lose_sum: 98.89641451835632
time_elpased: 1.84
batch start
#iterations: 19
currently lose_sum: 98.89997661113739
time_elpased: 1.849
start validation test
0.629948453608
0.615510649918
0.695893794381
0.653238661064
0.629832676391
64.554
batch start
#iterations: 20
currently lose_sum: 98.70305877923965
time_elpased: 1.886
batch start
#iterations: 21
currently lose_sum: 98.56340807676315
time_elpased: 1.829
batch start
#iterations: 22
currently lose_sum: 98.42473703622818
time_elpased: 1.816
batch start
#iterations: 23
currently lose_sum: 98.15323293209076
time_elpased: 1.827
batch start
#iterations: 24
currently lose_sum: 98.25148206949234
time_elpased: 1.837
batch start
#iterations: 25
currently lose_sum: 98.13166511058807
time_elpased: 1.842
batch start
#iterations: 26
currently lose_sum: 97.91807615756989
time_elpased: 1.83
batch start
#iterations: 27
currently lose_sum: 98.3012804389
time_elpased: 1.835
batch start
#iterations: 28
currently lose_sum: 98.04165351390839
time_elpased: 1.85
batch start
#iterations: 29
currently lose_sum: 97.932452917099
time_elpased: 1.831
batch start
#iterations: 30
currently lose_sum: 97.84488308429718
time_elpased: 1.864
batch start
#iterations: 31
currently lose_sum: 97.79978877305984
time_elpased: 1.83
batch start
#iterations: 32
currently lose_sum: 97.67055571079254
time_elpased: 1.84
batch start
#iterations: 33
currently lose_sum: 97.82639455795288
time_elpased: 1.835
batch start
#iterations: 34
currently lose_sum: 97.75624203681946
time_elpased: 1.824
batch start
#iterations: 35
currently lose_sum: 97.71716797351837
time_elpased: 1.842
batch start
#iterations: 36
currently lose_sum: 97.45085269212723
time_elpased: 1.857
batch start
#iterations: 37
currently lose_sum: 97.62165969610214
time_elpased: 1.838
batch start
#iterations: 38
currently lose_sum: 97.5333212018013
time_elpased: 1.876
batch start
#iterations: 39
currently lose_sum: 97.40545052289963
time_elpased: 1.846
start validation test
0.646855670103
0.620663523072
0.758567459092
0.682721252258
0.646659542827
63.033
batch start
#iterations: 40
currently lose_sum: 97.39884543418884
time_elpased: 1.873
batch start
#iterations: 41
currently lose_sum: 97.23745566606522
time_elpased: 1.829
batch start
#iterations: 42
currently lose_sum: 97.1682317852974
time_elpased: 1.826
batch start
#iterations: 43
currently lose_sum: 97.14521193504333
time_elpased: 1.845
batch start
#iterations: 44
currently lose_sum: 97.28372204303741
time_elpased: 1.871
batch start
#iterations: 45
currently lose_sum: 97.42091166973114
time_elpased: 1.821
batch start
#iterations: 46
currently lose_sum: 97.41367572546005
time_elpased: 1.846
batch start
#iterations: 47
currently lose_sum: 97.45314925909042
time_elpased: 1.865
batch start
#iterations: 48
currently lose_sum: 97.43320494890213
time_elpased: 1.852
batch start
#iterations: 49
currently lose_sum: 97.27330476045609
time_elpased: 1.893
batch start
#iterations: 50
currently lose_sum: 97.00315976142883
time_elpased: 1.881
batch start
#iterations: 51
currently lose_sum: 97.09678709506989
time_elpased: 1.841
batch start
#iterations: 52
currently lose_sum: 97.0813974738121
time_elpased: 1.852
batch start
#iterations: 53
currently lose_sum: 96.96902519464493
time_elpased: 1.92
batch start
#iterations: 54
currently lose_sum: 96.92748183012009
time_elpased: 1.846
batch start
#iterations: 55
currently lose_sum: 97.02231860160828
time_elpased: 1.818
batch start
#iterations: 56
currently lose_sum: 97.11215096712112
time_elpased: 1.812
batch start
#iterations: 57
currently lose_sum: 97.14307647943497
time_elpased: 1.848
batch start
#iterations: 58
currently lose_sum: 96.88675183057785
time_elpased: 1.844
batch start
#iterations: 59
currently lose_sum: 96.83108818531036
time_elpased: 1.849
start validation test
0.623917525773
0.655172413793
0.525985386436
0.583514099783
0.624089460749
62.835
batch start
#iterations: 60
currently lose_sum: 97.09412515163422
time_elpased: 1.881
batch start
#iterations: 61
currently lose_sum: 96.78329342603683
time_elpased: 1.833
batch start
#iterations: 62
currently lose_sum: 97.41611188650131
time_elpased: 1.838
batch start
#iterations: 63
currently lose_sum: 96.88181239366531
time_elpased: 1.855
batch start
#iterations: 64
currently lose_sum: 96.91378384828568
time_elpased: 1.834
batch start
#iterations: 65
currently lose_sum: 96.66451334953308
time_elpased: 1.84
batch start
#iterations: 66
currently lose_sum: 96.77231353521347
time_elpased: 1.864
batch start
#iterations: 67
currently lose_sum: 96.75111246109009
time_elpased: 1.839
batch start
#iterations: 68
currently lose_sum: 96.88600701093674
time_elpased: 1.833
batch start
#iterations: 69
currently lose_sum: 96.74219882488251
time_elpased: 1.852
batch start
#iterations: 70
currently lose_sum: 96.59473025798798
time_elpased: 1.84
batch start
#iterations: 71
currently lose_sum: 97.18444776535034
time_elpased: 1.937
batch start
#iterations: 72
currently lose_sum: 96.6067830324173
time_elpased: 1.829
batch start
#iterations: 73
currently lose_sum: 96.73444920778275
time_elpased: 1.849
batch start
#iterations: 74
currently lose_sum: 96.77586936950684
time_elpased: 1.862
batch start
#iterations: 75
currently lose_sum: 96.66031390428543
time_elpased: 1.837
batch start
#iterations: 76
currently lose_sum: 96.6873077750206
time_elpased: 1.852
batch start
#iterations: 77
currently lose_sum: 96.73070424795151
time_elpased: 1.837
batch start
#iterations: 78
currently lose_sum: 96.69872009754181
time_elpased: 1.832
batch start
#iterations: 79
currently lose_sum: 97.12501698732376
time_elpased: 1.858
start validation test
0.646649484536
0.637675582067
0.682103529896
0.659141763214
0.646587239491
62.337
batch start
#iterations: 80
currently lose_sum: 97.00919342041016
time_elpased: 1.897
batch start
#iterations: 81
currently lose_sum: 96.79737132787704
time_elpased: 1.862
batch start
#iterations: 82
currently lose_sum: 96.66320925951004
time_elpased: 1.848
batch start
#iterations: 83
currently lose_sum: 96.45698517560959
time_elpased: 1.814
batch start
#iterations: 84
currently lose_sum: 96.81833201646805
time_elpased: 1.839
batch start
#iterations: 85
currently lose_sum: 96.56464821100235
time_elpased: 1.853
batch start
#iterations: 86
currently lose_sum: 96.62264561653137
time_elpased: 1.866
batch start
#iterations: 87
currently lose_sum: 96.9158074259758
time_elpased: 1.822
batch start
#iterations: 88
currently lose_sum: 96.51871860027313
time_elpased: 1.836
batch start
#iterations: 89
currently lose_sum: 96.58410632610321
time_elpased: 1.846
batch start
#iterations: 90
currently lose_sum: 96.59670633077621
time_elpased: 1.865
batch start
#iterations: 91
currently lose_sum: 96.89185053110123
time_elpased: 1.862
batch start
#iterations: 92
currently lose_sum: 96.68841284513474
time_elpased: 1.882
batch start
#iterations: 93
currently lose_sum: 96.7119911313057
time_elpased: 1.836
batch start
#iterations: 94
currently lose_sum: 96.71547704935074
time_elpased: 1.844
batch start
#iterations: 95
currently lose_sum: 96.06935334205627
time_elpased: 1.88
batch start
#iterations: 96
currently lose_sum: 96.59143137931824
time_elpased: 1.876
batch start
#iterations: 97
currently lose_sum: 96.2732048034668
time_elpased: 1.845
batch start
#iterations: 98
currently lose_sum: 96.69637131690979
time_elpased: 1.825
batch start
#iterations: 99
currently lose_sum: 96.3000762462616
time_elpased: 1.829
start validation test
0.653350515464
0.62658656287
0.762066481424
0.687717668911
0.653159647817
61.656
batch start
#iterations: 100
currently lose_sum: 96.7436534166336
time_elpased: 1.875
batch start
#iterations: 101
currently lose_sum: 96.2513455748558
time_elpased: 1.852
batch start
#iterations: 102
currently lose_sum: 96.33835029602051
time_elpased: 1.85
batch start
#iterations: 103
currently lose_sum: 96.5495622754097
time_elpased: 1.832
batch start
#iterations: 104
currently lose_sum: 96.6180328130722
time_elpased: 1.879
batch start
#iterations: 105
currently lose_sum: 96.05929660797119
time_elpased: 1.873
batch start
#iterations: 106
currently lose_sum: 96.36671179533005
time_elpased: 1.848
batch start
#iterations: 107
currently lose_sum: 96.66041469573975
time_elpased: 1.851
batch start
#iterations: 108
currently lose_sum: 96.34109342098236
time_elpased: 1.821
batch start
#iterations: 109
currently lose_sum: 96.43807178735733
time_elpased: 1.866
batch start
#iterations: 110
currently lose_sum: 96.60293179750443
time_elpased: 1.845
batch start
#iterations: 111
currently lose_sum: 95.9439348578453
time_elpased: 1.825
batch start
#iterations: 112
currently lose_sum: 96.30551517009735
time_elpased: 1.858
batch start
#iterations: 113
currently lose_sum: 96.08866447210312
time_elpased: 1.846
batch start
#iterations: 114
currently lose_sum: 96.10637301206589
time_elpased: 1.852
batch start
#iterations: 115
currently lose_sum: 96.38409405946732
time_elpased: 1.858
batch start
#iterations: 116
currently lose_sum: 95.91676980257034
time_elpased: 1.843
batch start
#iterations: 117
currently lose_sum: 96.08649605512619
time_elpased: 1.857
batch start
#iterations: 118
currently lose_sum: 95.75222700834274
time_elpased: 1.845
batch start
#iterations: 119
currently lose_sum: 96.17205029726028
time_elpased: 1.83
start validation test
0.653298969072
0.637593154844
0.713183081198
0.673273098222
0.653193833277
61.400
batch start
#iterations: 120
currently lose_sum: 96.08291286230087
time_elpased: 1.855
batch start
#iterations: 121
currently lose_sum: 96.20507401227951
time_elpased: 1.865
batch start
#iterations: 122
currently lose_sum: 95.96224999427795
time_elpased: 1.829
batch start
#iterations: 123
currently lose_sum: 96.19649964570999
time_elpased: 1.847
batch start
#iterations: 124
currently lose_sum: 96.30793392658234
time_elpased: 1.834
batch start
#iterations: 125
currently lose_sum: 96.05871456861496
time_elpased: 1.888
batch start
#iterations: 126
currently lose_sum: 96.49053621292114
time_elpased: 1.846
batch start
#iterations: 127
currently lose_sum: 96.26411134004593
time_elpased: 1.866
batch start
#iterations: 128
currently lose_sum: 95.92815840244293
time_elpased: 1.881
batch start
#iterations: 129
currently lose_sum: 96.65782231092453
time_elpased: 1.855
batch start
#iterations: 130
currently lose_sum: 95.80501860380173
time_elpased: 1.846
batch start
#iterations: 131
currently lose_sum: 96.47264283895493
time_elpased: 1.854
batch start
#iterations: 132
currently lose_sum: 96.14744144678116
time_elpased: 1.852
batch start
#iterations: 133
currently lose_sum: 96.07503432035446
time_elpased: 1.872
batch start
#iterations: 134
currently lose_sum: 96.25540518760681
time_elpased: 1.936
batch start
#iterations: 135
currently lose_sum: 96.17984700202942
time_elpased: 1.861
batch start
#iterations: 136
currently lose_sum: 96.05926787853241
time_elpased: 1.829
batch start
#iterations: 137
currently lose_sum: 95.85182923078537
time_elpased: 1.846
batch start
#iterations: 138
currently lose_sum: 96.34793436527252
time_elpased: 1.844
batch start
#iterations: 139
currently lose_sum: 95.8451509475708
time_elpased: 1.851
start validation test
0.653092783505
0.635859183117
0.71935782649
0.675036214389
0.652976445002
61.141
batch start
#iterations: 140
currently lose_sum: 96.03186076879501
time_elpased: 1.897
batch start
#iterations: 141
currently lose_sum: 96.11805701255798
time_elpased: 1.854
batch start
#iterations: 142
currently lose_sum: 95.9480232000351
time_elpased: 1.862
batch start
#iterations: 143
currently lose_sum: 96.04199862480164
time_elpased: 1.855
batch start
#iterations: 144
currently lose_sum: 96.29190719127655
time_elpased: 1.894
batch start
#iterations: 145
currently lose_sum: 95.85792291164398
time_elpased: 1.808
batch start
#iterations: 146
currently lose_sum: 95.82626819610596
time_elpased: 1.848
batch start
#iterations: 147
currently lose_sum: 95.94323670864105
time_elpased: 1.847
batch start
#iterations: 148
currently lose_sum: 95.71600395441055
time_elpased: 1.864
batch start
#iterations: 149
currently lose_sum: 95.64599406719208
time_elpased: 1.926
batch start
#iterations: 150
currently lose_sum: 96.13611036539078
time_elpased: 1.849
batch start
#iterations: 151
currently lose_sum: 95.68423187732697
time_elpased: 1.892
batch start
#iterations: 152
currently lose_sum: 95.84611976146698
time_elpased: 1.838
batch start
#iterations: 153
currently lose_sum: 96.01253491640091
time_elpased: 1.867
batch start
#iterations: 154
currently lose_sum: 95.89083182811737
time_elpased: 1.854
batch start
#iterations: 155
currently lose_sum: 95.9475594162941
time_elpased: 1.87
batch start
#iterations: 156
currently lose_sum: 95.96716541051865
time_elpased: 1.837
batch start
#iterations: 157
currently lose_sum: 95.5317754149437
time_elpased: 1.822
batch start
#iterations: 158
currently lose_sum: 95.62720680236816
time_elpased: 1.825
batch start
#iterations: 159
currently lose_sum: 95.79501444101334
time_elpased: 1.889
start validation test
0.650773195876
0.645384463333
0.672018112586
0.658432064532
0.650735897148
61.297
batch start
#iterations: 160
currently lose_sum: 95.3630263209343
time_elpased: 1.896
batch start
#iterations: 161
currently lose_sum: 96.08892905712128
time_elpased: 1.838
batch start
#iterations: 162
currently lose_sum: 95.94239777326584
time_elpased: 1.853
batch start
#iterations: 163
currently lose_sum: 95.83342814445496
time_elpased: 1.867
batch start
#iterations: 164
currently lose_sum: 95.43206477165222
time_elpased: 1.859
batch start
#iterations: 165
currently lose_sum: 96.09757244586945
time_elpased: 1.852
batch start
#iterations: 166
currently lose_sum: 96.13632255792618
time_elpased: 1.839
batch start
#iterations: 167
currently lose_sum: 95.73257946968079
time_elpased: 1.875
batch start
#iterations: 168
currently lose_sum: 95.51213669776917
time_elpased: 1.843
batch start
#iterations: 169
currently lose_sum: 95.8423398733139
time_elpased: 1.849
batch start
#iterations: 170
currently lose_sum: 96.04260647296906
time_elpased: 1.888
batch start
#iterations: 171
currently lose_sum: 95.86710542440414
time_elpased: 1.937
batch start
#iterations: 172
currently lose_sum: 96.34714645147324
time_elpased: 1.852
batch start
#iterations: 173
currently lose_sum: 95.94373035430908
time_elpased: 2.007
batch start
#iterations: 174
currently lose_sum: 95.17337661981583
time_elpased: 1.912
batch start
#iterations: 175
currently lose_sum: 95.57291978597641
time_elpased: 1.867
batch start
#iterations: 176
currently lose_sum: 95.37155330181122
time_elpased: 1.847
batch start
#iterations: 177
currently lose_sum: 95.6159166097641
time_elpased: 1.861
batch start
#iterations: 178
currently lose_sum: 95.66772043704987
time_elpased: 1.94
batch start
#iterations: 179
currently lose_sum: 95.6576999425888
time_elpased: 1.899
start validation test
0.662371134021
0.634342920166
0.769476175774
0.695405505952
0.6621830946
60.839
batch start
#iterations: 180
currently lose_sum: 95.52238404750824
time_elpased: 1.868
batch start
#iterations: 181
currently lose_sum: 95.67867535352707
time_elpased: 1.869
batch start
#iterations: 182
currently lose_sum: 95.84335994720459
time_elpased: 1.839
batch start
#iterations: 183
currently lose_sum: 95.37874019145966
time_elpased: 1.871
batch start
#iterations: 184
currently lose_sum: 96.11129152774811
time_elpased: 1.895
batch start
#iterations: 185
currently lose_sum: 95.50439351797104
time_elpased: 1.873
batch start
#iterations: 186
currently lose_sum: 95.9279934167862
time_elpased: 1.819
batch start
#iterations: 187
currently lose_sum: 95.78016406297684
time_elpased: 1.845
batch start
#iterations: 188
currently lose_sum: 95.94645845890045
time_elpased: 1.863
batch start
#iterations: 189
currently lose_sum: 95.99382841587067
time_elpased: 1.879
batch start
#iterations: 190
currently lose_sum: 95.32897764444351
time_elpased: 1.868
batch start
#iterations: 191
currently lose_sum: 95.6581357717514
time_elpased: 1.835
batch start
#iterations: 192
currently lose_sum: 95.84500014781952
time_elpased: 1.823
batch start
#iterations: 193
currently lose_sum: 95.36903148889542
time_elpased: 1.854
batch start
#iterations: 194
currently lose_sum: 95.23538202047348
time_elpased: 1.855
batch start
#iterations: 195
currently lose_sum: 95.82859247922897
time_elpased: 1.849
batch start
#iterations: 196
currently lose_sum: 95.58769249916077
time_elpased: 1.842
batch start
#iterations: 197
currently lose_sum: 95.72420287132263
time_elpased: 1.871
batch start
#iterations: 198
currently lose_sum: 95.60741358995438
time_elpased: 1.871
batch start
#iterations: 199
currently lose_sum: 95.41730749607086
time_elpased: 1.888
start validation test
0.649175257732
0.648233017619
0.655037563034
0.651617526618
0.649164965551
61.210
batch start
#iterations: 200
currently lose_sum: 95.62190020084381
time_elpased: 1.871
batch start
#iterations: 201
currently lose_sum: 95.5665533542633
time_elpased: 1.905
batch start
#iterations: 202
currently lose_sum: 95.47400891780853
time_elpased: 1.842
batch start
#iterations: 203
currently lose_sum: 95.64067232608795
time_elpased: 1.937
batch start
#iterations: 204
currently lose_sum: 95.35759997367859
time_elpased: 1.873
batch start
#iterations: 205
currently lose_sum: 95.36422723531723
time_elpased: 1.86
batch start
#iterations: 206
currently lose_sum: 95.4116479754448
time_elpased: 1.808
batch start
#iterations: 207
currently lose_sum: 95.63786840438843
time_elpased: 1.838
batch start
#iterations: 208
currently lose_sum: 95.63119649887085
time_elpased: 1.845
batch start
#iterations: 209
currently lose_sum: 95.1819097995758
time_elpased: 1.868
batch start
#iterations: 210
currently lose_sum: 95.15406954288483
time_elpased: 1.876
batch start
#iterations: 211
currently lose_sum: 95.77712571620941
time_elpased: 1.839
batch start
#iterations: 212
currently lose_sum: 95.67725110054016
time_elpased: 1.848
batch start
#iterations: 213
currently lose_sum: 95.73168230056763
time_elpased: 1.822
batch start
#iterations: 214
currently lose_sum: 95.35049802064896
time_elpased: 1.89
batch start
#iterations: 215
currently lose_sum: 95.55066901445389
time_elpased: 1.878
batch start
#iterations: 216
currently lose_sum: 95.27316933870316
time_elpased: 1.84
batch start
#iterations: 217
currently lose_sum: 95.49765717983246
time_elpased: 1.85
batch start
#iterations: 218
currently lose_sum: 95.16671127080917
time_elpased: 1.853
batch start
#iterations: 219
currently lose_sum: 95.80301028490067
time_elpased: 1.864
start validation test
0.627525773196
0.654126964485
0.543995060204
0.593999325767
0.627672424246
61.746
batch start
#iterations: 220
currently lose_sum: 95.48143327236176
time_elpased: 1.826
batch start
#iterations: 221
currently lose_sum: 95.45120358467102
time_elpased: 1.885
batch start
#iterations: 222
currently lose_sum: 95.37236350774765
time_elpased: 1.865
batch start
#iterations: 223
currently lose_sum: 95.0426635146141
time_elpased: 1.839
batch start
#iterations: 224
currently lose_sum: 95.20244014263153
time_elpased: 1.837
batch start
#iterations: 225
currently lose_sum: 95.79135501384735
time_elpased: 1.832
batch start
#iterations: 226
currently lose_sum: 95.23092663288116
time_elpased: 1.891
batch start
#iterations: 227
currently lose_sum: 95.63125383853912
time_elpased: 1.857
batch start
#iterations: 228
currently lose_sum: 95.5842234492302
time_elpased: 1.842
batch start
#iterations: 229
currently lose_sum: 95.37744498252869
time_elpased: 1.836
batch start
#iterations: 230
currently lose_sum: 95.39072483778
time_elpased: 1.861
batch start
#iterations: 231
currently lose_sum: 95.02045327425003
time_elpased: 1.832
batch start
#iterations: 232
currently lose_sum: 95.47635394334793
time_elpased: 1.836
batch start
#iterations: 233
currently lose_sum: 95.5279358625412
time_elpased: 1.875
batch start
#iterations: 234
currently lose_sum: 95.20587909221649
time_elpased: 1.827
batch start
#iterations: 235
currently lose_sum: 95.08226954936981
time_elpased: 1.834
batch start
#iterations: 236
currently lose_sum: 95.55567908287048
time_elpased: 1.865
batch start
#iterations: 237
currently lose_sum: 95.44528651237488
time_elpased: 1.892
batch start
#iterations: 238
currently lose_sum: 95.25289899110794
time_elpased: 1.865
batch start
#iterations: 239
currently lose_sum: 95.32642883062363
time_elpased: 1.862
start validation test
0.661237113402
0.620120693606
0.835443037975
0.711855489302
0.660931268032
60.852
batch start
#iterations: 240
currently lose_sum: 95.20358097553253
time_elpased: 1.88
batch start
#iterations: 241
currently lose_sum: 95.42436522245407
time_elpased: 1.83
batch start
#iterations: 242
currently lose_sum: 95.31374269723892
time_elpased: 1.825
batch start
#iterations: 243
currently lose_sum: 95.46848553419113
time_elpased: 1.851
batch start
#iterations: 244
currently lose_sum: 95.52034443616867
time_elpased: 1.827
batch start
#iterations: 245
currently lose_sum: 95.35675090551376
time_elpased: 1.873
batch start
#iterations: 246
currently lose_sum: 95.5038810968399
time_elpased: 1.854
batch start
#iterations: 247
currently lose_sum: 94.89030861854553
time_elpased: 1.837
batch start
#iterations: 248
currently lose_sum: 95.18710219860077
time_elpased: 1.875
batch start
#iterations: 249
currently lose_sum: 95.48763465881348
time_elpased: 1.878
batch start
#iterations: 250
currently lose_sum: 94.83981502056122
time_elpased: 1.848
batch start
#iterations: 251
currently lose_sum: 95.15011405944824
time_elpased: 1.864
batch start
#iterations: 252
currently lose_sum: 95.44219970703125
time_elpased: 1.828
batch start
#iterations: 253
currently lose_sum: 95.13265919685364
time_elpased: 1.826
batch start
#iterations: 254
currently lose_sum: 95.41433292627335
time_elpased: 1.866
batch start
#iterations: 255
currently lose_sum: 95.42762887477875
time_elpased: 1.909
batch start
#iterations: 256
currently lose_sum: 94.98132956027985
time_elpased: 1.839
batch start
#iterations: 257
currently lose_sum: 95.00306057929993
time_elpased: 1.904
batch start
#iterations: 258
currently lose_sum: 95.22707623243332
time_elpased: 1.863
batch start
#iterations: 259
currently lose_sum: 95.11969351768494
time_elpased: 1.848
start validation test
0.663298969072
0.637914609855
0.758052896985
0.692814145974
0.663132613937
60.702
batch start
#iterations: 260
currently lose_sum: 95.32516461610794
time_elpased: 1.817
batch start
#iterations: 261
currently lose_sum: 94.7967255115509
time_elpased: 1.929
batch start
#iterations: 262
currently lose_sum: 95.36923784017563
time_elpased: 1.814
batch start
#iterations: 263
currently lose_sum: 95.58176070451736
time_elpased: 1.861
batch start
#iterations: 264
currently lose_sum: 95.21272993087769
time_elpased: 1.847
batch start
#iterations: 265
currently lose_sum: 95.12672591209412
time_elpased: 1.834
batch start
#iterations: 266
currently lose_sum: 94.97724848985672
time_elpased: 1.872
batch start
#iterations: 267
currently lose_sum: 95.12202596664429
time_elpased: 1.902
batch start
#iterations: 268
currently lose_sum: 95.42141062021255
time_elpased: 1.811
batch start
#iterations: 269
currently lose_sum: 95.30704110860825
time_elpased: 1.851
batch start
#iterations: 270
currently lose_sum: 95.09107053279877
time_elpased: 1.837
batch start
#iterations: 271
currently lose_sum: 95.29553145170212
time_elpased: 1.851
batch start
#iterations: 272
currently lose_sum: 94.87410569190979
time_elpased: 1.819
batch start
#iterations: 273
currently lose_sum: 94.96971344947815
time_elpased: 1.851
batch start
#iterations: 274
currently lose_sum: 95.48201787471771
time_elpased: 1.842
batch start
#iterations: 275
currently lose_sum: 95.27949798107147
time_elpased: 1.843
batch start
#iterations: 276
currently lose_sum: 95.27924126386642
time_elpased: 1.93
batch start
#iterations: 277
currently lose_sum: 95.01032096147537
time_elpased: 1.835
batch start
#iterations: 278
currently lose_sum: 95.144890666008
time_elpased: 1.85
batch start
#iterations: 279
currently lose_sum: 95.22022420167923
time_elpased: 1.944
start validation test
0.658505154639
0.639909502262
0.727693732634
0.680984253864
0.658383683419
60.893
batch start
#iterations: 280
currently lose_sum: 95.11432784795761
time_elpased: 1.914
batch start
#iterations: 281
currently lose_sum: 94.92224472761154
time_elpased: 1.831
batch start
#iterations: 282
currently lose_sum: 95.33629316091537
time_elpased: 1.833
batch start
#iterations: 283
currently lose_sum: 95.42938393354416
time_elpased: 1.847
batch start
#iterations: 284
currently lose_sum: 95.01314198970795
time_elpased: 1.891
batch start
#iterations: 285
currently lose_sum: 95.15130341053009
time_elpased: 1.894
batch start
#iterations: 286
currently lose_sum: 95.03114551305771
time_elpased: 1.879
batch start
#iterations: 287
currently lose_sum: 95.00996792316437
time_elpased: 1.943
batch start
#iterations: 288
currently lose_sum: 95.31801927089691
time_elpased: 1.924
batch start
#iterations: 289
currently lose_sum: 94.96436488628387
time_elpased: 1.897
batch start
#iterations: 290
currently lose_sum: 94.82910937070847
time_elpased: 1.893
batch start
#iterations: 291
currently lose_sum: 95.51373249292374
time_elpased: 1.857
batch start
#iterations: 292
currently lose_sum: 95.11966264247894
time_elpased: 1.86
batch start
#iterations: 293
currently lose_sum: 95.28222876787186
time_elpased: 1.859
batch start
#iterations: 294
currently lose_sum: 95.26550197601318
time_elpased: 1.885
batch start
#iterations: 295
currently lose_sum: 95.41922348737717
time_elpased: 1.854
batch start
#iterations: 296
currently lose_sum: 94.90158379077911
time_elpased: 1.818
batch start
#iterations: 297
currently lose_sum: 95.34740000963211
time_elpased: 1.842
batch start
#iterations: 298
currently lose_sum: 95.13674587011337
time_elpased: 1.837
batch start
#iterations: 299
currently lose_sum: 94.92303121089935
time_elpased: 1.918
start validation test
0.664484536082
0.634789915966
0.777400432232
0.698894388676
0.664286294811
60.525
batch start
#iterations: 300
currently lose_sum: 94.95191437005997
time_elpased: 1.963
batch start
#iterations: 301
currently lose_sum: 95.3312047123909
time_elpased: 1.827
batch start
#iterations: 302
currently lose_sum: 95.33654719591141
time_elpased: 1.863
batch start
#iterations: 303
currently lose_sum: 95.10679411888123
time_elpased: 1.814
batch start
#iterations: 304
currently lose_sum: 95.04537242650986
time_elpased: 1.927
batch start
#iterations: 305
currently lose_sum: 94.8903620839119
time_elpased: 1.82
batch start
#iterations: 306
currently lose_sum: 94.78764921426773
time_elpased: 1.855
batch start
#iterations: 307
currently lose_sum: 95.08941698074341
time_elpased: 1.829
batch start
#iterations: 308
currently lose_sum: 94.64293199777603
time_elpased: 1.871
batch start
#iterations: 309
currently lose_sum: 94.65397721529007
time_elpased: 1.855
batch start
#iterations: 310
currently lose_sum: 95.51868331432343
time_elpased: 1.854
batch start
#iterations: 311
currently lose_sum: 94.87257862091064
time_elpased: 1.909
batch start
#iterations: 312
currently lose_sum: 94.91096824407578
time_elpased: 1.806
batch start
#iterations: 313
currently lose_sum: 95.17738419771194
time_elpased: 1.849
batch start
#iterations: 314
currently lose_sum: 94.82573395967484
time_elpased: 1.857
batch start
#iterations: 315
currently lose_sum: 94.62312060594559
time_elpased: 1.804
batch start
#iterations: 316
currently lose_sum: 94.9350037574768
time_elpased: 1.84
batch start
#iterations: 317
currently lose_sum: 94.78764200210571
time_elpased: 1.922
batch start
#iterations: 318
currently lose_sum: 95.11709362268448
time_elpased: 1.904
batch start
#iterations: 319
currently lose_sum: 95.45759391784668
time_elpased: 1.904
start validation test
0.663711340206
0.621879532789
0.838324585778
0.714060308555
0.663404779721
60.642
batch start
#iterations: 320
currently lose_sum: 94.68638163805008
time_elpased: 1.887
batch start
#iterations: 321
currently lose_sum: 95.012864112854
time_elpased: 1.815
batch start
#iterations: 322
currently lose_sum: 95.05179619789124
time_elpased: 1.854
batch start
#iterations: 323
currently lose_sum: 95.00971573591232
time_elpased: 1.865
batch start
#iterations: 324
currently lose_sum: 94.80786311626434
time_elpased: 1.816
batch start
#iterations: 325
currently lose_sum: 95.06370621919632
time_elpased: 1.818
batch start
#iterations: 326
currently lose_sum: 94.90192383527756
time_elpased: 1.839
batch start
#iterations: 327
currently lose_sum: 95.20958667993546
time_elpased: 1.817
batch start
#iterations: 328
currently lose_sum: 95.44114315509796
time_elpased: 1.88
batch start
#iterations: 329
currently lose_sum: 94.62844824790955
time_elpased: 1.81
batch start
#iterations: 330
currently lose_sum: 95.43229007720947
time_elpased: 1.817
batch start
#iterations: 331
currently lose_sum: 94.83337146043777
time_elpased: 1.84
batch start
#iterations: 332
currently lose_sum: 94.57673591375351
time_elpased: 1.844
batch start
#iterations: 333
currently lose_sum: 95.10793590545654
time_elpased: 1.805
batch start
#iterations: 334
currently lose_sum: 94.85785961151123
time_elpased: 1.826
batch start
#iterations: 335
currently lose_sum: 94.70249980688095
time_elpased: 1.823
batch start
#iterations: 336
currently lose_sum: 95.06595116853714
time_elpased: 1.827
batch start
#iterations: 337
currently lose_sum: 95.31641858816147
time_elpased: 1.826
batch start
#iterations: 338
currently lose_sum: 95.17336934804916
time_elpased: 1.836
batch start
#iterations: 339
currently lose_sum: 95.10671359300613
time_elpased: 1.933
start validation test
0.662268041237
0.62785812394
0.799732427704
0.703448900154
0.662026701304
60.664
batch start
#iterations: 340
currently lose_sum: 95.31648379564285
time_elpased: 1.869
batch start
#iterations: 341
currently lose_sum: 95.14687287807465
time_elpased: 1.815
batch start
#iterations: 342
currently lose_sum: 95.18775743246078
time_elpased: 1.818
batch start
#iterations: 343
currently lose_sum: 95.09722727537155
time_elpased: 1.823
batch start
#iterations: 344
currently lose_sum: 94.74726283550262
time_elpased: 1.855
batch start
#iterations: 345
currently lose_sum: 95.1014010310173
time_elpased: 1.823
batch start
#iterations: 346
currently lose_sum: 94.6401264667511
time_elpased: 1.843
batch start
#iterations: 347
currently lose_sum: 95.09371519088745
time_elpased: 1.832
batch start
#iterations: 348
currently lose_sum: 94.79175573587418
time_elpased: 1.875
batch start
#iterations: 349
currently lose_sum: 95.59740060567856
time_elpased: 1.815
batch start
#iterations: 350
currently lose_sum: 94.97906166315079
time_elpased: 1.833
batch start
#iterations: 351
currently lose_sum: 95.07465648651123
time_elpased: 1.834
batch start
#iterations: 352
currently lose_sum: 94.77093076705933
time_elpased: 1.889
batch start
#iterations: 353
currently lose_sum: 94.77207887172699
time_elpased: 1.814
batch start
#iterations: 354
currently lose_sum: 95.17008692026138
time_elpased: 1.844
batch start
#iterations: 355
currently lose_sum: 94.86138552427292
time_elpased: 1.869
batch start
#iterations: 356
currently lose_sum: 95.25132596492767
time_elpased: 1.843
batch start
#iterations: 357
currently lose_sum: 94.96212387084961
time_elpased: 1.849
batch start
#iterations: 358
currently lose_sum: 94.74288249015808
time_elpased: 1.82
batch start
#iterations: 359
currently lose_sum: 94.87126791477203
time_elpased: 1.811
start validation test
0.652422680412
0.639127994012
0.702994751467
0.669541778976
0.652333893341
61.248
batch start
#iterations: 360
currently lose_sum: 95.23412108421326
time_elpased: 1.88
batch start
#iterations: 361
currently lose_sum: 94.94455474615097
time_elpased: 1.832
batch start
#iterations: 362
currently lose_sum: 94.99105560779572
time_elpased: 1.838
batch start
#iterations: 363
currently lose_sum: 94.6294778585434
time_elpased: 1.885
batch start
#iterations: 364
currently lose_sum: 94.97531616687775
time_elpased: 1.885
batch start
#iterations: 365
currently lose_sum: 95.26910895109177
time_elpased: 1.845
batch start
#iterations: 366
currently lose_sum: 94.83356928825378
time_elpased: 1.872
batch start
#iterations: 367
currently lose_sum: 94.9085910320282
time_elpased: 1.856
batch start
#iterations: 368
currently lose_sum: 95.12483018636703
time_elpased: 1.884
batch start
#iterations: 369
currently lose_sum: 95.281190097332
time_elpased: 1.825
batch start
#iterations: 370
currently lose_sum: 94.38069289922714
time_elpased: 1.888
batch start
#iterations: 371
currently lose_sum: 95.20610058307648
time_elpased: 1.864
batch start
#iterations: 372
currently lose_sum: 94.69871789216995
time_elpased: 1.836
batch start
#iterations: 373
currently lose_sum: 95.04500234127045
time_elpased: 1.804
batch start
#iterations: 374
currently lose_sum: 95.29128837585449
time_elpased: 1.828
batch start
#iterations: 375
currently lose_sum: 94.5317325592041
time_elpased: 1.889
batch start
#iterations: 376
currently lose_sum: 95.14847195148468
time_elpased: 1.831
batch start
#iterations: 377
currently lose_sum: 95.30231684446335
time_elpased: 1.875
batch start
#iterations: 378
currently lose_sum: 94.88402044773102
time_elpased: 1.901
batch start
#iterations: 379
currently lose_sum: 95.03556662797928
time_elpased: 1.832
start validation test
0.659690721649
0.63917433652
0.736132551199
0.684235699254
0.659556516227
60.765
batch start
#iterations: 380
currently lose_sum: 95.34760117530823
time_elpased: 1.859
batch start
#iterations: 381
currently lose_sum: 94.67564141750336
time_elpased: 1.835
batch start
#iterations: 382
currently lose_sum: 94.9662572145462
time_elpased: 1.82
batch start
#iterations: 383
currently lose_sum: 94.83982753753662
time_elpased: 1.845
batch start
#iterations: 384
currently lose_sum: 95.40882807970047
time_elpased: 1.854
batch start
#iterations: 385
currently lose_sum: 95.36956977844238
time_elpased: 1.845
batch start
#iterations: 386
currently lose_sum: 94.86115026473999
time_elpased: 1.81
batch start
#iterations: 387
currently lose_sum: 95.28896355628967
time_elpased: 1.853
batch start
#iterations: 388
currently lose_sum: 94.94060724973679
time_elpased: 1.818
batch start
#iterations: 389
currently lose_sum: 94.98154544830322
time_elpased: 1.926
batch start
#iterations: 390
currently lose_sum: 94.96347379684448
time_elpased: 1.909
batch start
#iterations: 391
currently lose_sum: 95.13300710916519
time_elpased: 1.824
batch start
#iterations: 392
currently lose_sum: 94.94886326789856
time_elpased: 1.856
batch start
#iterations: 393
currently lose_sum: 94.93765217065811
time_elpased: 1.818
batch start
#iterations: 394
currently lose_sum: 94.70761007070541
time_elpased: 1.818
batch start
#iterations: 395
currently lose_sum: 94.62039709091187
time_elpased: 1.858
batch start
#iterations: 396
currently lose_sum: 94.61577409505844
time_elpased: 1.836
batch start
#iterations: 397
currently lose_sum: 94.92144852876663
time_elpased: 1.863
batch start
#iterations: 398
currently lose_sum: 94.56702798604965
time_elpased: 1.836
batch start
#iterations: 399
currently lose_sum: 95.14070010185242
time_elpased: 1.825
start validation test
0.656597938144
0.643900141309
0.703406401153
0.672339169782
0.656515758668
60.809
acc: 0.658
pre: 0.631
rec: 0.767
F1: 0.692
auc: 0.658
