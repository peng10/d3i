start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.53962242603302
time_elpased: 1.92
batch start
#iterations: 1
currently lose_sum: 100.22040510177612
time_elpased: 1.913
batch start
#iterations: 2
currently lose_sum: 100.25385016202927
time_elpased: 1.915
batch start
#iterations: 3
currently lose_sum: 100.07033789157867
time_elpased: 1.892
batch start
#iterations: 4
currently lose_sum: 100.05221402645111
time_elpased: 1.934
batch start
#iterations: 5
currently lose_sum: 99.86863070726395
time_elpased: 1.932
batch start
#iterations: 6
currently lose_sum: 99.69241970777512
time_elpased: 1.925
batch start
#iterations: 7
currently lose_sum: 99.88240951299667
time_elpased: 1.947
batch start
#iterations: 8
currently lose_sum: 99.76331615447998
time_elpased: 1.935
batch start
#iterations: 9
currently lose_sum: 99.75075286626816
time_elpased: 1.913
batch start
#iterations: 10
currently lose_sum: 99.73701453208923
time_elpased: 1.934
batch start
#iterations: 11
currently lose_sum: 99.69535380601883
time_elpased: 2.029
batch start
#iterations: 12
currently lose_sum: 99.80868554115295
time_elpased: 1.975
batch start
#iterations: 13
currently lose_sum: 99.60124427080154
time_elpased: 1.905
batch start
#iterations: 14
currently lose_sum: 99.55732131004333
time_elpased: 1.936
batch start
#iterations: 15
currently lose_sum: 99.38963681459427
time_elpased: 1.924
batch start
#iterations: 16
currently lose_sum: 99.51336079835892
time_elpased: 1.909
batch start
#iterations: 17
currently lose_sum: 99.65382212400436
time_elpased: 1.945
batch start
#iterations: 18
currently lose_sum: 99.35474067926407
time_elpased: 1.914
batch start
#iterations: 19
currently lose_sum: 99.62336230278015
time_elpased: 1.924
start validation test
0.585309278351
0.635494327391
0.403519604816
0.493611128596
0.585628438162
65.546
batch start
#iterations: 20
currently lose_sum: 99.50337505340576
time_elpased: 1.912
batch start
#iterations: 21
currently lose_sum: 99.31065094470978
time_elpased: 1.942
batch start
#iterations: 22
currently lose_sum: 99.46714144945145
time_elpased: 1.916
batch start
#iterations: 23
currently lose_sum: 99.56858432292938
time_elpased: 1.922
batch start
#iterations: 24
currently lose_sum: 99.45000821352005
time_elpased: 1.933
batch start
#iterations: 25
currently lose_sum: 99.41823619604111
time_elpased: 1.945
batch start
#iterations: 26
currently lose_sum: 99.48484963178635
time_elpased: 1.927
batch start
#iterations: 27
currently lose_sum: 99.41998744010925
time_elpased: 1.921
batch start
#iterations: 28
currently lose_sum: 99.4158143401146
time_elpased: 1.907
batch start
#iterations: 29
currently lose_sum: 99.5646880865097
time_elpased: 1.929
batch start
#iterations: 30
currently lose_sum: 99.46630895137787
time_elpased: 1.892
batch start
#iterations: 31
currently lose_sum: 99.30282127857208
time_elpased: 1.92
batch start
#iterations: 32
currently lose_sum: 99.40251785516739
time_elpased: 1.993
batch start
#iterations: 33
currently lose_sum: 99.48288702964783
time_elpased: 1.951
batch start
#iterations: 34
currently lose_sum: 99.28912925720215
time_elpased: 1.938
batch start
#iterations: 35
currently lose_sum: 99.52514266967773
time_elpased: 1.942
batch start
#iterations: 36
currently lose_sum: 99.37823075056076
time_elpased: 1.93
batch start
#iterations: 37
currently lose_sum: 99.24237126111984
time_elpased: 1.917
batch start
#iterations: 38
currently lose_sum: 99.20145010948181
time_elpased: 1.92
batch start
#iterations: 39
currently lose_sum: 99.35610944032669
time_elpased: 1.942
start validation test
0.610773195876
0.618516086671
0.581661006483
0.599522673031
0.610824306815
64.906
batch start
#iterations: 40
currently lose_sum: 99.31685948371887
time_elpased: 1.932
batch start
#iterations: 41
currently lose_sum: 99.26478677988052
time_elpased: 1.949
batch start
#iterations: 42
currently lose_sum: 99.35268622636795
time_elpased: 1.931
batch start
#iterations: 43
currently lose_sum: 99.3064050078392
time_elpased: 1.931
batch start
#iterations: 44
currently lose_sum: 99.30853372812271
time_elpased: 1.932
batch start
#iterations: 45
currently lose_sum: 99.14170390367508
time_elpased: 1.926
batch start
#iterations: 46
currently lose_sum: 99.20394814014435
time_elpased: 1.946
batch start
#iterations: 47
currently lose_sum: 99.17231684923172
time_elpased: 1.977
batch start
#iterations: 48
currently lose_sum: 99.41740995645523
time_elpased: 1.928
batch start
#iterations: 49
currently lose_sum: 99.17506957054138
time_elpased: 1.929
batch start
#iterations: 50
currently lose_sum: 99.3709197640419
time_elpased: 1.874
batch start
#iterations: 51
currently lose_sum: 99.17315721511841
time_elpased: 1.896
batch start
#iterations: 52
currently lose_sum: 99.33518886566162
time_elpased: 1.917
batch start
#iterations: 53
currently lose_sum: 99.48139256238937
time_elpased: 1.967
batch start
#iterations: 54
currently lose_sum: 99.3780043721199
time_elpased: 1.958
batch start
#iterations: 55
currently lose_sum: 99.25798445940018
time_elpased: 1.971
batch start
#iterations: 56
currently lose_sum: 99.19823509454727
time_elpased: 1.927
batch start
#iterations: 57
currently lose_sum: 99.33530527353287
time_elpased: 1.944
batch start
#iterations: 58
currently lose_sum: 99.37990921735764
time_elpased: 1.918
batch start
#iterations: 59
currently lose_sum: 99.25247186422348
time_elpased: 1.952
start validation test
0.61118556701
0.627223782772
0.551507666975
0.586933902853
0.611290340769
64.844
batch start
#iterations: 60
currently lose_sum: 99.22604775428772
time_elpased: 1.945
batch start
#iterations: 61
currently lose_sum: 99.380326628685
time_elpased: 1.924
batch start
#iterations: 62
currently lose_sum: 99.21742218732834
time_elpased: 1.925
batch start
#iterations: 63
currently lose_sum: 99.33957898616791
time_elpased: 1.931
batch start
#iterations: 64
currently lose_sum: 99.28812968730927
time_elpased: 1.933
batch start
#iterations: 65
currently lose_sum: 99.11342525482178
time_elpased: 1.928
batch start
#iterations: 66
currently lose_sum: 99.0136188864708
time_elpased: 1.91
batch start
#iterations: 67
currently lose_sum: 99.24407893419266
time_elpased: 1.941
batch start
#iterations: 68
currently lose_sum: 99.3742362856865
time_elpased: 2.023
batch start
#iterations: 69
currently lose_sum: 99.3462238907814
time_elpased: 1.958
batch start
#iterations: 70
currently lose_sum: 99.29796230792999
time_elpased: 1.983
batch start
#iterations: 71
currently lose_sum: 99.19299960136414
time_elpased: 1.987
batch start
#iterations: 72
currently lose_sum: 99.3238714337349
time_elpased: 1.947
batch start
#iterations: 73
currently lose_sum: 99.14732271432877
time_elpased: 1.945
batch start
#iterations: 74
currently lose_sum: 99.09308391809464
time_elpased: 1.881
batch start
#iterations: 75
currently lose_sum: 99.13321644067764
time_elpased: 1.89
batch start
#iterations: 76
currently lose_sum: 99.0493221282959
time_elpased: 1.887
batch start
#iterations: 77
currently lose_sum: 99.3212822675705
time_elpased: 1.893
batch start
#iterations: 78
currently lose_sum: 99.31106823682785
time_elpased: 1.887
batch start
#iterations: 79
currently lose_sum: 99.30353850126266
time_elpased: 1.872
start validation test
0.602835051546
0.638224787029
0.478028198003
0.546631362165
0.603054169228
65.000
batch start
#iterations: 80
currently lose_sum: 99.18546569347382
time_elpased: 1.915
batch start
#iterations: 81
currently lose_sum: 99.13089472055435
time_elpased: 1.957
batch start
#iterations: 82
currently lose_sum: 99.10176521539688
time_elpased: 1.922
batch start
#iterations: 83
currently lose_sum: 99.00749999284744
time_elpased: 1.891
batch start
#iterations: 84
currently lose_sum: 99.16796004772186
time_elpased: 1.872
batch start
#iterations: 85
currently lose_sum: 99.25059574842453
time_elpased: 1.875
batch start
#iterations: 86
currently lose_sum: 99.05908012390137
time_elpased: 1.88
batch start
#iterations: 87
currently lose_sum: 99.03206473588943
time_elpased: 1.881
batch start
#iterations: 88
currently lose_sum: 99.22960138320923
time_elpased: 1.875
batch start
#iterations: 89
currently lose_sum: 99.12405574321747
time_elpased: 1.878
batch start
#iterations: 90
currently lose_sum: 99.28548645973206
time_elpased: 1.875
batch start
#iterations: 91
currently lose_sum: 99.17017239332199
time_elpased: 1.878
batch start
#iterations: 92
currently lose_sum: 98.97002685070038
time_elpased: 1.872
batch start
#iterations: 93
currently lose_sum: 99.22683757543564
time_elpased: 1.869
batch start
#iterations: 94
currently lose_sum: 99.06843918561935
time_elpased: 1.882
batch start
#iterations: 95
currently lose_sum: 99.2527112364769
time_elpased: 1.891
batch start
#iterations: 96
currently lose_sum: 99.29020446538925
time_elpased: 1.876
batch start
#iterations: 97
currently lose_sum: 99.11061680316925
time_elpased: 1.88
batch start
#iterations: 98
currently lose_sum: 99.04098188877106
time_elpased: 1.873
batch start
#iterations: 99
currently lose_sum: 98.9341054558754
time_elpased: 1.873
start validation test
0.596546391753
0.635736857225
0.455490377689
0.530727261826
0.596794037342
64.735
batch start
#iterations: 100
currently lose_sum: 99.22583949565887
time_elpased: 1.868
batch start
#iterations: 101
currently lose_sum: 99.26778948307037
time_elpased: 1.883
batch start
#iterations: 102
currently lose_sum: 98.96435970067978
time_elpased: 1.89
batch start
#iterations: 103
currently lose_sum: 99.11912781000137
time_elpased: 1.882
batch start
#iterations: 104
currently lose_sum: 99.07378000020981
time_elpased: 1.886
batch start
#iterations: 105
currently lose_sum: 99.22253966331482
time_elpased: 1.924
batch start
#iterations: 106
currently lose_sum: 99.10923713445663
time_elpased: 1.874
batch start
#iterations: 107
currently lose_sum: 99.23355585336685
time_elpased: 1.881
batch start
#iterations: 108
currently lose_sum: 99.27711290121078
time_elpased: 1.89
batch start
#iterations: 109
currently lose_sum: 99.18167001008987
time_elpased: 1.894
batch start
#iterations: 110
currently lose_sum: 99.05348151922226
time_elpased: 1.89
batch start
#iterations: 111
currently lose_sum: 99.07961910963058
time_elpased: 1.89
batch start
#iterations: 112
currently lose_sum: 99.15156191587448
time_elpased: 1.938
batch start
#iterations: 113
currently lose_sum: 99.02553278207779
time_elpased: 1.9
batch start
#iterations: 114
currently lose_sum: 98.9825159907341
time_elpased: 1.891
batch start
#iterations: 115
currently lose_sum: 99.15754383802414
time_elpased: 1.888
batch start
#iterations: 116
currently lose_sum: 99.21842533349991
time_elpased: 1.964
batch start
#iterations: 117
currently lose_sum: 99.38447564840317
time_elpased: 1.888
batch start
#iterations: 118
currently lose_sum: 99.0796326994896
time_elpased: 1.895
batch start
#iterations: 119
currently lose_sum: 99.19340819120407
time_elpased: 1.916
start validation test
0.607113402062
0.64894070809
0.46969229186
0.544955223881
0.607354666017
64.749
batch start
#iterations: 120
currently lose_sum: 99.24742066860199
time_elpased: 1.893
batch start
#iterations: 121
currently lose_sum: 98.91404193639755
time_elpased: 1.929
batch start
#iterations: 122
currently lose_sum: 99.25921714305878
time_elpased: 1.93
batch start
#iterations: 123
currently lose_sum: 99.07027494907379
time_elpased: 1.9
batch start
#iterations: 124
currently lose_sum: 99.03749811649323
time_elpased: 1.886
batch start
#iterations: 125
currently lose_sum: 99.27503538131714
time_elpased: 1.888
batch start
#iterations: 126
currently lose_sum: 99.14640110731125
time_elpased: 1.891
batch start
#iterations: 127
currently lose_sum: 99.35223293304443
time_elpased: 1.902
batch start
#iterations: 128
currently lose_sum: 99.0460746884346
time_elpased: 1.877
batch start
#iterations: 129
currently lose_sum: 99.18685752153397
time_elpased: 1.898
batch start
#iterations: 130
currently lose_sum: 99.09455609321594
time_elpased: 1.896
batch start
#iterations: 131
currently lose_sum: 99.12415504455566
time_elpased: 1.896
batch start
#iterations: 132
currently lose_sum: 99.11294722557068
time_elpased: 1.896
batch start
#iterations: 133
currently lose_sum: 98.98099809885025
time_elpased: 1.893
batch start
#iterations: 134
currently lose_sum: 99.06359368562698
time_elpased: 1.89
batch start
#iterations: 135
currently lose_sum: 99.01056110858917
time_elpased: 1.886
batch start
#iterations: 136
currently lose_sum: 99.19997847080231
time_elpased: 1.901
batch start
#iterations: 137
currently lose_sum: 99.08276188373566
time_elpased: 1.892
batch start
#iterations: 138
currently lose_sum: 99.09122198820114
time_elpased: 1.883
batch start
#iterations: 139
currently lose_sum: 99.13594752550125
time_elpased: 1.88
start validation test
0.616134020619
0.635635755258
0.547391170114
0.58822228366
0.616254709295
64.547
batch start
#iterations: 140
currently lose_sum: 99.24475556612015
time_elpased: 1.886
batch start
#iterations: 141
currently lose_sum: 99.0364745259285
time_elpased: 1.869
batch start
#iterations: 142
currently lose_sum: 99.1988468170166
time_elpased: 1.882
batch start
#iterations: 143
currently lose_sum: 99.22511750459671
time_elpased: 1.888
batch start
#iterations: 144
currently lose_sum: 99.19567638635635
time_elpased: 1.883
batch start
#iterations: 145
currently lose_sum: 99.14376932382584
time_elpased: 1.902
batch start
#iterations: 146
currently lose_sum: 99.07740050554276
time_elpased: 1.89
batch start
#iterations: 147
currently lose_sum: 99.0083184838295
time_elpased: 1.864
batch start
#iterations: 148
currently lose_sum: 99.22228693962097
time_elpased: 1.89
batch start
#iterations: 149
currently lose_sum: 98.97068357467651
time_elpased: 1.893
batch start
#iterations: 150
currently lose_sum: 98.99861228466034
time_elpased: 1.933
batch start
#iterations: 151
currently lose_sum: 99.18823701143265
time_elpased: 1.932
batch start
#iterations: 152
currently lose_sum: 99.2066645026207
time_elpased: 1.885
batch start
#iterations: 153
currently lose_sum: 99.23540788888931
time_elpased: 1.88
batch start
#iterations: 154
currently lose_sum: 99.09425121545792
time_elpased: 1.897
batch start
#iterations: 155
currently lose_sum: 99.0920432806015
time_elpased: 1.877
batch start
#iterations: 156
currently lose_sum: 99.1107469201088
time_elpased: 1.88
batch start
#iterations: 157
currently lose_sum: 99.2415663599968
time_elpased: 1.879
batch start
#iterations: 158
currently lose_sum: 99.36365884542465
time_elpased: 1.877
batch start
#iterations: 159
currently lose_sum: 99.19025099277496
time_elpased: 1.879
start validation test
0.598659793814
0.648469936952
0.433981681589
0.519975339088
0.59894891164
64.796
batch start
#iterations: 160
currently lose_sum: 99.04465037584305
time_elpased: 1.876
batch start
#iterations: 161
currently lose_sum: 98.9690346121788
time_elpased: 1.869
batch start
#iterations: 162
currently lose_sum: 99.16783285140991
time_elpased: 1.899
batch start
#iterations: 163
currently lose_sum: 99.06156557798386
time_elpased: 1.887
batch start
#iterations: 164
currently lose_sum: 99.06746435165405
time_elpased: 1.904
batch start
#iterations: 165
currently lose_sum: 99.1926873922348
time_elpased: 1.876
batch start
#iterations: 166
currently lose_sum: 98.98481065034866
time_elpased: 1.944
batch start
#iterations: 167
currently lose_sum: 99.32078862190247
time_elpased: 1.939
batch start
#iterations: 168
currently lose_sum: 98.90783613920212
time_elpased: 1.905
batch start
#iterations: 169
currently lose_sum: 99.18328195810318
time_elpased: 1.88
batch start
#iterations: 170
currently lose_sum: 99.10852724313736
time_elpased: 1.882
batch start
#iterations: 171
currently lose_sum: 98.96617192029953
time_elpased: 1.897
batch start
#iterations: 172
currently lose_sum: 99.26990723609924
time_elpased: 1.922
batch start
#iterations: 173
currently lose_sum: 99.13214921951294
time_elpased: 1.886
batch start
#iterations: 174
currently lose_sum: 99.17293471097946
time_elpased: 1.901
batch start
#iterations: 175
currently lose_sum: 98.95630812644958
time_elpased: 1.893
batch start
#iterations: 176
currently lose_sum: 99.07351684570312
time_elpased: 1.887
batch start
#iterations: 177
currently lose_sum: 99.08539307117462
time_elpased: 1.878
batch start
#iterations: 178
currently lose_sum: 99.03667318820953
time_elpased: 1.901
batch start
#iterations: 179
currently lose_sum: 99.31317591667175
time_elpased: 1.894
start validation test
0.622628865979
0.634335052702
0.582175568591
0.607137107593
0.622699887982
64.521
batch start
#iterations: 180
currently lose_sum: 99.24156707525253
time_elpased: 1.892
batch start
#iterations: 181
currently lose_sum: 99.31281012296677
time_elpased: 1.874
batch start
#iterations: 182
currently lose_sum: 99.07261914014816
time_elpased: 1.893
batch start
#iterations: 183
currently lose_sum: 98.95579606294632
time_elpased: 1.887
batch start
#iterations: 184
currently lose_sum: 98.8862773180008
time_elpased: 1.887
batch start
#iterations: 185
currently lose_sum: 99.09556317329407
time_elpased: 1.882
batch start
#iterations: 186
currently lose_sum: 98.99558174610138
time_elpased: 1.893
batch start
#iterations: 187
currently lose_sum: 99.08380901813507
time_elpased: 1.88
batch start
#iterations: 188
currently lose_sum: 99.17509061098099
time_elpased: 1.91
batch start
#iterations: 189
currently lose_sum: 99.2484878897667
time_elpased: 1.899
batch start
#iterations: 190
currently lose_sum: 99.22470343112946
time_elpased: 1.882
batch start
#iterations: 191
currently lose_sum: 99.00270617008209
time_elpased: 1.887
batch start
#iterations: 192
currently lose_sum: 98.98220998048782
time_elpased: 1.899
batch start
#iterations: 193
currently lose_sum: 99.05741757154465
time_elpased: 1.883
batch start
#iterations: 194
currently lose_sum: 99.27142268419266
time_elpased: 1.894
batch start
#iterations: 195
currently lose_sum: 98.87742513418198
time_elpased: 1.899
batch start
#iterations: 196
currently lose_sum: 99.28095269203186
time_elpased: 1.902
batch start
#iterations: 197
currently lose_sum: 99.14709997177124
time_elpased: 1.881
batch start
#iterations: 198
currently lose_sum: 99.15328431129456
time_elpased: 1.886
batch start
#iterations: 199
currently lose_sum: 99.01780301332474
time_elpased: 1.879
start validation test
0.619793814433
0.645205309515
0.535247504374
0.585105186185
0.619942248521
64.478
batch start
#iterations: 200
currently lose_sum: 99.11154168844223
time_elpased: 1.894
batch start
#iterations: 201
currently lose_sum: 98.92716771364212
time_elpased: 1.872
batch start
#iterations: 202
currently lose_sum: 99.14211946725845
time_elpased: 1.885
batch start
#iterations: 203
currently lose_sum: 98.97925466299057
time_elpased: 1.882
batch start
#iterations: 204
currently lose_sum: 98.9829358458519
time_elpased: 1.897
batch start
#iterations: 205
currently lose_sum: 99.09353023767471
time_elpased: 1.881
batch start
#iterations: 206
currently lose_sum: 99.10190808773041
time_elpased: 1.905
batch start
#iterations: 207
currently lose_sum: 99.04650789499283
time_elpased: 1.884
batch start
#iterations: 208
currently lose_sum: 98.81094342470169
time_elpased: 1.887
batch start
#iterations: 209
currently lose_sum: 99.07522612810135
time_elpased: 1.913
batch start
#iterations: 210
currently lose_sum: 98.99292379617691
time_elpased: 1.885
batch start
#iterations: 211
currently lose_sum: 98.79828625917435
time_elpased: 1.875
batch start
#iterations: 212
currently lose_sum: 98.91574084758759
time_elpased: 1.889
batch start
#iterations: 213
currently lose_sum: 99.13078761100769
time_elpased: 1.885
batch start
#iterations: 214
currently lose_sum: 99.17049723863602
time_elpased: 1.897
batch start
#iterations: 215
currently lose_sum: 98.87217682600021
time_elpased: 1.937
batch start
#iterations: 216
currently lose_sum: 99.00728350877762
time_elpased: 1.903
batch start
#iterations: 217
currently lose_sum: 99.00640106201172
time_elpased: 1.881
batch start
#iterations: 218
currently lose_sum: 99.0016497373581
time_elpased: 1.89
batch start
#iterations: 219
currently lose_sum: 99.28273594379425
time_elpased: 1.88
start validation test
0.616958762887
0.645308924485
0.522383451683
0.577375874424
0.617124804433
64.540
batch start
#iterations: 220
currently lose_sum: 99.05018174648285
time_elpased: 2.11
batch start
#iterations: 221
currently lose_sum: 99.21422761678696
time_elpased: 1.901
batch start
#iterations: 222
currently lose_sum: 98.93350058794022
time_elpased: 1.897
batch start
#iterations: 223
currently lose_sum: 99.00404441356659
time_elpased: 1.904
batch start
#iterations: 224
currently lose_sum: 99.1390752196312
time_elpased: 1.91
batch start
#iterations: 225
currently lose_sum: 99.11335372924805
time_elpased: 1.895
batch start
#iterations: 226
currently lose_sum: 98.97872585058212
time_elpased: 1.894
batch start
#iterations: 227
currently lose_sum: 99.0219714641571
time_elpased: 1.953
batch start
#iterations: 228
currently lose_sum: 98.86566698551178
time_elpased: 1.907
batch start
#iterations: 229
currently lose_sum: 99.12644708156586
time_elpased: 1.903
batch start
#iterations: 230
currently lose_sum: 99.1780726313591
time_elpased: 1.911
batch start
#iterations: 231
currently lose_sum: 99.0985032916069
time_elpased: 1.888
batch start
#iterations: 232
currently lose_sum: 99.02556419372559
time_elpased: 1.887
batch start
#iterations: 233
currently lose_sum: 99.12476807832718
time_elpased: 1.907
batch start
#iterations: 234
currently lose_sum: 98.90017652511597
time_elpased: 1.905
batch start
#iterations: 235
currently lose_sum: 99.04474955797195
time_elpased: 1.918
batch start
#iterations: 236
currently lose_sum: 99.14950913190842
time_elpased: 1.937
batch start
#iterations: 237
currently lose_sum: 99.17644333839417
time_elpased: 1.902
batch start
#iterations: 238
currently lose_sum: 99.10295528173447
time_elpased: 1.894
batch start
#iterations: 239
currently lose_sum: 98.98515003919601
time_elpased: 1.887
start validation test
0.609793814433
0.646632973637
0.48718740352
0.555699025707
0.610009068898
64.523
batch start
#iterations: 240
currently lose_sum: 98.93012511730194
time_elpased: 1.884
batch start
#iterations: 241
currently lose_sum: 99.01214307546616
time_elpased: 1.982
batch start
#iterations: 242
currently lose_sum: 99.04710054397583
time_elpased: 1.894
batch start
#iterations: 243
currently lose_sum: 99.21170860528946
time_elpased: 1.901
batch start
#iterations: 244
currently lose_sum: 99.19536221027374
time_elpased: 1.936
batch start
#iterations: 245
currently lose_sum: 99.16447424888611
time_elpased: 1.884
batch start
#iterations: 246
currently lose_sum: 98.7829042673111
time_elpased: 1.884
batch start
#iterations: 247
currently lose_sum: 99.1533774137497
time_elpased: 1.887
batch start
#iterations: 248
currently lose_sum: 99.10422432422638
time_elpased: 1.892
batch start
#iterations: 249
currently lose_sum: 99.25762212276459
time_elpased: 1.898
batch start
#iterations: 250
currently lose_sum: 98.97713768482208
time_elpased: 1.898
batch start
#iterations: 251
currently lose_sum: 99.20366501808167
time_elpased: 1.9
batch start
#iterations: 252
currently lose_sum: 99.05188083648682
time_elpased: 1.915
batch start
#iterations: 253
currently lose_sum: 99.13938969373703
time_elpased: 1.878
batch start
#iterations: 254
currently lose_sum: 99.1186134815216
time_elpased: 1.88
batch start
#iterations: 255
currently lose_sum: 99.12478363513947
time_elpased: 1.92
batch start
#iterations: 256
currently lose_sum: 98.97642940282822
time_elpased: 1.887
batch start
#iterations: 257
currently lose_sum: 98.98157739639282
time_elpased: 1.882
batch start
#iterations: 258
currently lose_sum: 99.03509718179703
time_elpased: 1.905
batch start
#iterations: 259
currently lose_sum: 99.02458560466766
time_elpased: 1.894
start validation test
0.609896907216
0.646091094494
0.489039827107
0.556701030928
0.610109090462
64.577
batch start
#iterations: 260
currently lose_sum: 98.93452960252762
time_elpased: 1.896
batch start
#iterations: 261
currently lose_sum: 98.94008338451385
time_elpased: 1.894
batch start
#iterations: 262
currently lose_sum: 98.9273431301117
time_elpased: 1.91
batch start
#iterations: 263
currently lose_sum: 98.99269872903824
time_elpased: 1.898
batch start
#iterations: 264
currently lose_sum: 98.94448924064636
time_elpased: 1.946
batch start
#iterations: 265
currently lose_sum: 99.35187643766403
time_elpased: 1.9
batch start
#iterations: 266
currently lose_sum: 98.98659610748291
time_elpased: 1.899
batch start
#iterations: 267
currently lose_sum: 98.98257726430893
time_elpased: 1.898
batch start
#iterations: 268
currently lose_sum: 99.08061689138412
time_elpased: 1.901
batch start
#iterations: 269
currently lose_sum: 99.04602593183517
time_elpased: 1.931
batch start
#iterations: 270
currently lose_sum: 99.00549268722534
time_elpased: 1.901
batch start
#iterations: 271
currently lose_sum: 99.17344242334366
time_elpased: 1.937
batch start
#iterations: 272
currently lose_sum: 99.05909073352814
time_elpased: 1.911
batch start
#iterations: 273
currently lose_sum: 98.77879351377487
time_elpased: 1.895
batch start
#iterations: 274
currently lose_sum: 99.14861071109772
time_elpased: 1.895
batch start
#iterations: 275
currently lose_sum: 99.2020874619484
time_elpased: 1.886
batch start
#iterations: 276
currently lose_sum: 98.95388734340668
time_elpased: 1.889
batch start
#iterations: 277
currently lose_sum: 99.12524235248566
time_elpased: 1.892
batch start
#iterations: 278
currently lose_sum: 99.05857962369919
time_elpased: 1.887
batch start
#iterations: 279
currently lose_sum: 98.72599440813065
time_elpased: 1.887
start validation test
0.614226804124
0.641132600177
0.521971801997
0.575448150669
0.614388772009
64.433
batch start
#iterations: 280
currently lose_sum: 98.8933562040329
time_elpased: 1.899
batch start
#iterations: 281
currently lose_sum: 99.09884470701218
time_elpased: 1.901
batch start
#iterations: 282
currently lose_sum: 98.98497980833054
time_elpased: 1.899
batch start
#iterations: 283
currently lose_sum: 99.19726759195328
time_elpased: 1.902
batch start
#iterations: 284
currently lose_sum: 98.91928499937057
time_elpased: 1.902
batch start
#iterations: 285
currently lose_sum: 99.11220234632492
time_elpased: 1.891
batch start
#iterations: 286
currently lose_sum: 98.6230000257492
time_elpased: 1.904
batch start
#iterations: 287
currently lose_sum: 99.09484905004501
time_elpased: 1.903
batch start
#iterations: 288
currently lose_sum: 98.91553902626038
time_elpased: 1.904
batch start
#iterations: 289
currently lose_sum: 99.28698670864105
time_elpased: 1.9
batch start
#iterations: 290
currently lose_sum: 99.08069175481796
time_elpased: 1.914
batch start
#iterations: 291
currently lose_sum: 99.11775559186935
time_elpased: 1.89
batch start
#iterations: 292
currently lose_sum: 99.20868766307831
time_elpased: 1.915
batch start
#iterations: 293
currently lose_sum: 99.0340347290039
time_elpased: 1.899
batch start
#iterations: 294
currently lose_sum: 98.87320387363434
time_elpased: 1.901
batch start
#iterations: 295
currently lose_sum: 99.12390649318695
time_elpased: 1.89
batch start
#iterations: 296
currently lose_sum: 98.88623827695847
time_elpased: 1.905
batch start
#iterations: 297
currently lose_sum: 99.12952196598053
time_elpased: 1.905
batch start
#iterations: 298
currently lose_sum: 99.07623964548111
time_elpased: 1.901
batch start
#iterations: 299
currently lose_sum: 98.99147999286652
time_elpased: 1.937
start validation test
0.606855670103
0.648564117145
0.469486467017
0.544683899469
0.607096842927
64.555
batch start
#iterations: 300
currently lose_sum: 98.68938452005386
time_elpased: 1.911
batch start
#iterations: 301
currently lose_sum: 99.01192051172256
time_elpased: 1.941
batch start
#iterations: 302
currently lose_sum: 98.93872046470642
time_elpased: 1.911
batch start
#iterations: 303
currently lose_sum: 99.11398941278458
time_elpased: 1.91
batch start
#iterations: 304
currently lose_sum: 99.02960121631622
time_elpased: 1.908
batch start
#iterations: 305
currently lose_sum: 99.11857116222382
time_elpased: 1.9
batch start
#iterations: 306
currently lose_sum: 99.1741035580635
time_elpased: 1.91
batch start
#iterations: 307
currently lose_sum: 98.910136282444
time_elpased: 1.904
batch start
#iterations: 308
currently lose_sum: 98.8727605342865
time_elpased: 1.894
batch start
#iterations: 309
currently lose_sum: 99.02559411525726
time_elpased: 1.887
batch start
#iterations: 310
currently lose_sum: 98.9845899939537
time_elpased: 1.89
batch start
#iterations: 311
currently lose_sum: 98.95639133453369
time_elpased: 1.899
batch start
#iterations: 312
currently lose_sum: 99.069280564785
time_elpased: 1.898
batch start
#iterations: 313
currently lose_sum: 98.95209562778473
time_elpased: 1.895
batch start
#iterations: 314
currently lose_sum: 98.82552868127823
time_elpased: 1.932
batch start
#iterations: 315
currently lose_sum: 98.92820060253143
time_elpased: 1.889
batch start
#iterations: 316
currently lose_sum: 99.09602808952332
time_elpased: 1.894
batch start
#iterations: 317
currently lose_sum: 98.99550259113312
time_elpased: 1.884
batch start
#iterations: 318
currently lose_sum: 98.97292524576187
time_elpased: 1.889
batch start
#iterations: 319
currently lose_sum: 99.12867498397827
time_elpased: 1.887
start validation test
0.615257731959
0.637027125654
0.538952351549
0.583900100346
0.615391697823
64.467
batch start
#iterations: 320
currently lose_sum: 99.1288549900055
time_elpased: 1.906
batch start
#iterations: 321
currently lose_sum: 98.95159238576889
time_elpased: 1.89
batch start
#iterations: 322
currently lose_sum: 98.88473683595657
time_elpased: 1.9
batch start
#iterations: 323
currently lose_sum: 99.26554143428802
time_elpased: 1.88
batch start
#iterations: 324
currently lose_sum: 99.15595334768295
time_elpased: 1.909
batch start
#iterations: 325
currently lose_sum: 99.01706677675247
time_elpased: 1.895
batch start
#iterations: 326
currently lose_sum: 99.17216485738754
time_elpased: 1.901
batch start
#iterations: 327
currently lose_sum: 99.02048355340958
time_elpased: 1.882
batch start
#iterations: 328
currently lose_sum: 98.9684471487999
time_elpased: 1.884
batch start
#iterations: 329
currently lose_sum: 98.98389077186584
time_elpased: 1.89
batch start
#iterations: 330
currently lose_sum: 98.91847318410873
time_elpased: 1.888
batch start
#iterations: 331
currently lose_sum: 98.91159355640411
time_elpased: 1.884
batch start
#iterations: 332
currently lose_sum: 99.01015263795853
time_elpased: 1.891
batch start
#iterations: 333
currently lose_sum: 99.01407724618912
time_elpased: 1.879
batch start
#iterations: 334
currently lose_sum: 98.85614174604416
time_elpased: 1.905
batch start
#iterations: 335
currently lose_sum: 99.06735914945602
time_elpased: 1.893
batch start
#iterations: 336
currently lose_sum: 98.93393272161484
time_elpased: 1.889
batch start
#iterations: 337
currently lose_sum: 99.00214785337448
time_elpased: 1.892
batch start
#iterations: 338
currently lose_sum: 98.98210036754608
time_elpased: 1.915
batch start
#iterations: 339
currently lose_sum: 98.9466552734375
time_elpased: 1.899
start validation test
0.617010309278
0.645464953568
0.52217762684
0.577312549778
0.617176802679
64.358
batch start
#iterations: 340
currently lose_sum: 99.12659817934036
time_elpased: 1.898
batch start
#iterations: 341
currently lose_sum: 99.14959120750427
time_elpased: 1.9
batch start
#iterations: 342
currently lose_sum: 99.07657760381699
time_elpased: 1.898
batch start
#iterations: 343
currently lose_sum: 99.11788898706436
time_elpased: 1.899
batch start
#iterations: 344
currently lose_sum: 99.0417246222496
time_elpased: 1.908
batch start
#iterations: 345
currently lose_sum: 98.88477778434753
time_elpased: 1.9
batch start
#iterations: 346
currently lose_sum: 99.13354414701462
time_elpased: 1.904
batch start
#iterations: 347
currently lose_sum: 98.84204232692719
time_elpased: 1.951
batch start
#iterations: 348
currently lose_sum: 99.0626305937767
time_elpased: 1.893
batch start
#iterations: 349
currently lose_sum: 99.09537452459335
time_elpased: 1.888
batch start
#iterations: 350
currently lose_sum: 99.05536597967148
time_elpased: 1.897
batch start
#iterations: 351
currently lose_sum: 99.07519370317459
time_elpased: 1.936
batch start
#iterations: 352
currently lose_sum: 98.9968433380127
time_elpased: 1.902
batch start
#iterations: 353
currently lose_sum: 99.20138728618622
time_elpased: 1.892
batch start
#iterations: 354
currently lose_sum: 99.49821072816849
time_elpased: 1.908
batch start
#iterations: 355
currently lose_sum: 99.21982878446579
time_elpased: 1.887
batch start
#iterations: 356
currently lose_sum: 99.08109617233276
time_elpased: 1.912
batch start
#iterations: 357
currently lose_sum: 99.1244398355484
time_elpased: 1.893
batch start
#iterations: 358
currently lose_sum: 99.23553001880646
time_elpased: 1.898
batch start
#iterations: 359
currently lose_sum: 99.14466619491577
time_elpased: 1.945
start validation test
0.617422680412
0.643276314147
0.530204795719
0.581293015909
0.617575804861
64.513
batch start
#iterations: 360
currently lose_sum: 99.21107310056686
time_elpased: 1.906
batch start
#iterations: 361
currently lose_sum: 99.11546957492828
time_elpased: 1.893
batch start
#iterations: 362
currently lose_sum: 98.8500075340271
time_elpased: 1.896
batch start
#iterations: 363
currently lose_sum: 99.1571329832077
time_elpased: 1.893
batch start
#iterations: 364
currently lose_sum: 99.12367391586304
time_elpased: 1.909
batch start
#iterations: 365
currently lose_sum: 99.12481135129929
time_elpased: 1.905
batch start
#iterations: 366
currently lose_sum: 99.08527773618698
time_elpased: 1.888
batch start
#iterations: 367
currently lose_sum: 98.90979224443436
time_elpased: 1.947
batch start
#iterations: 368
currently lose_sum: 99.07130944728851
time_elpased: 1.888
batch start
#iterations: 369
currently lose_sum: 99.07712662220001
time_elpased: 1.901
batch start
#iterations: 370
currently lose_sum: 98.99887013435364
time_elpased: 1.879
batch start
#iterations: 371
currently lose_sum: 99.0341911315918
time_elpased: 1.897
batch start
#iterations: 372
currently lose_sum: 99.17194271087646
time_elpased: 1.891
batch start
#iterations: 373
currently lose_sum: 98.95563226938248
time_elpased: 1.903
batch start
#iterations: 374
currently lose_sum: 99.09080493450165
time_elpased: 1.878
batch start
#iterations: 375
currently lose_sum: 98.90967959165573
time_elpased: 1.92
batch start
#iterations: 376
currently lose_sum: 99.10151249170303
time_elpased: 1.874
batch start
#iterations: 377
currently lose_sum: 99.18844306468964
time_elpased: 1.921
batch start
#iterations: 378
currently lose_sum: 98.799076795578
time_elpased: 1.904
batch start
#iterations: 379
currently lose_sum: 98.86758023500443
time_elpased: 1.91
start validation test
0.618865979381
0.649005772931
0.520633940517
0.577775239836
0.619038440877
64.377
batch start
#iterations: 380
currently lose_sum: 99.04452073574066
time_elpased: 1.926
batch start
#iterations: 381
currently lose_sum: 99.04792386293411
time_elpased: 1.948
batch start
#iterations: 382
currently lose_sum: 98.9234099984169
time_elpased: 1.882
batch start
#iterations: 383
currently lose_sum: 98.99664610624313
time_elpased: 1.903
batch start
#iterations: 384
currently lose_sum: 99.04819965362549
time_elpased: 1.878
batch start
#iterations: 385
currently lose_sum: 98.9718411564827
time_elpased: 1.89
batch start
#iterations: 386
currently lose_sum: 98.88888162374496
time_elpased: 1.876
batch start
#iterations: 387
currently lose_sum: 98.92930990457535
time_elpased: 1.893
batch start
#iterations: 388
currently lose_sum: 99.19697660207748
time_elpased: 1.876
batch start
#iterations: 389
currently lose_sum: 98.94410717487335
time_elpased: 1.898
batch start
#iterations: 390
currently lose_sum: 98.87777119874954
time_elpased: 1.869
batch start
#iterations: 391
currently lose_sum: 98.986931681633
time_elpased: 1.898
batch start
#iterations: 392
currently lose_sum: 99.0170174241066
time_elpased: 1.877
batch start
#iterations: 393
currently lose_sum: 98.97382354736328
time_elpased: 1.894
batch start
#iterations: 394
currently lose_sum: 99.05748808383942
time_elpased: 1.885
batch start
#iterations: 395
currently lose_sum: 99.04891043901443
time_elpased: 1.893
batch start
#iterations: 396
currently lose_sum: 98.91833329200745
time_elpased: 1.875
batch start
#iterations: 397
currently lose_sum: 98.98459482192993
time_elpased: 1.896
batch start
#iterations: 398
currently lose_sum: 99.04055052995682
time_elpased: 1.891
batch start
#iterations: 399
currently lose_sum: 99.03528183698654
time_elpased: 1.899
start validation test
0.617422680412
0.64541883158
0.524132962849
0.57848705134
0.617586464901
64.428
acc: 0.618
pre: 0.646
rec: 0.523
F1: 0.578
auc: 0.618
