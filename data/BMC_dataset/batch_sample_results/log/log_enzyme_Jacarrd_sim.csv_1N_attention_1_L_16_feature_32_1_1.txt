start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.78931260108948
time_elpased: 1.95
batch start
#iterations: 1
currently lose_sum: 100.34885716438293
time_elpased: 1.775
batch start
#iterations: 2
currently lose_sum: 100.02719020843506
time_elpased: 1.802
batch start
#iterations: 3
currently lose_sum: 99.88237500190735
time_elpased: 1.806
batch start
#iterations: 4
currently lose_sum: 99.79310154914856
time_elpased: 1.858
batch start
#iterations: 5
currently lose_sum: 99.60535836219788
time_elpased: 1.794
batch start
#iterations: 6
currently lose_sum: 99.57795530557632
time_elpased: 1.859
batch start
#iterations: 7
currently lose_sum: 99.24150508642197
time_elpased: 1.8
batch start
#iterations: 8
currently lose_sum: 99.35067290067673
time_elpased: 1.785
batch start
#iterations: 9
currently lose_sum: 99.34587395191193
time_elpased: 1.807
batch start
#iterations: 10
currently lose_sum: 99.03825724124908
time_elpased: 1.768
batch start
#iterations: 11
currently lose_sum: 99.19209790229797
time_elpased: 1.817
batch start
#iterations: 12
currently lose_sum: 99.09148162603378
time_elpased: 1.827
batch start
#iterations: 13
currently lose_sum: 98.90736693143845
time_elpased: 1.798
batch start
#iterations: 14
currently lose_sum: 99.07804852724075
time_elpased: 1.807
batch start
#iterations: 15
currently lose_sum: 98.5575168132782
time_elpased: 1.789
batch start
#iterations: 16
currently lose_sum: 98.57527428865433
time_elpased: 1.78
batch start
#iterations: 17
currently lose_sum: 98.60593438148499
time_elpased: 1.804
batch start
#iterations: 18
currently lose_sum: 98.52912271022797
time_elpased: 1.822
batch start
#iterations: 19
currently lose_sum: 98.68049627542496
time_elpased: 1.771
start validation test
0.617474226804
0.630662417482
0.570237727694
0.598929903259
0.617557157764
64.463
batch start
#iterations: 20
currently lose_sum: 98.48367995023727
time_elpased: 1.791
batch start
#iterations: 21
currently lose_sum: 98.29724752902985
time_elpased: 1.772
batch start
#iterations: 22
currently lose_sum: 98.61162531375885
time_elpased: 1.786
batch start
#iterations: 23
currently lose_sum: 98.43805211782455
time_elpased: 1.775
batch start
#iterations: 24
currently lose_sum: 98.59145706892014
time_elpased: 1.75
batch start
#iterations: 25
currently lose_sum: 98.52268600463867
time_elpased: 1.781
batch start
#iterations: 26
currently lose_sum: 98.34658318758011
time_elpased: 1.766
batch start
#iterations: 27
currently lose_sum: 98.38220202922821
time_elpased: 1.764
batch start
#iterations: 28
currently lose_sum: 98.30096834897995
time_elpased: 1.765
batch start
#iterations: 29
currently lose_sum: 98.43362838029861
time_elpased: 1.806
batch start
#iterations: 30
currently lose_sum: 98.24717044830322
time_elpased: 1.775
batch start
#iterations: 31
currently lose_sum: 98.23835176229477
time_elpased: 1.756
batch start
#iterations: 32
currently lose_sum: 98.13628894090652
time_elpased: 1.755
batch start
#iterations: 33
currently lose_sum: 98.44510239362717
time_elpased: 1.768
batch start
#iterations: 34
currently lose_sum: 98.1499735713005
time_elpased: 1.759
batch start
#iterations: 35
currently lose_sum: 98.15905100107193
time_elpased: 1.802
batch start
#iterations: 36
currently lose_sum: 98.22548860311508
time_elpased: 1.8
batch start
#iterations: 37
currently lose_sum: 98.35751593112946
time_elpased: 1.75
batch start
#iterations: 38
currently lose_sum: 98.16288441419601
time_elpased: 1.796
batch start
#iterations: 39
currently lose_sum: 98.02890700101852
time_elpased: 1.849
start validation test
0.616391752577
0.635529608007
0.548934856437
0.589066813915
0.616510183563
64.131
batch start
#iterations: 40
currently lose_sum: 98.05729138851166
time_elpased: 1.865
batch start
#iterations: 41
currently lose_sum: 98.12009280920029
time_elpased: 1.779
batch start
#iterations: 42
currently lose_sum: 97.8737200498581
time_elpased: 1.741
batch start
#iterations: 43
currently lose_sum: 98.1110246181488
time_elpased: 1.764
batch start
#iterations: 44
currently lose_sum: 97.99129778146744
time_elpased: 1.77
batch start
#iterations: 45
currently lose_sum: 97.94227284193039
time_elpased: 1.785
batch start
#iterations: 46
currently lose_sum: 97.72095030546188
time_elpased: 1.769
batch start
#iterations: 47
currently lose_sum: 97.91905474662781
time_elpased: 1.742
batch start
#iterations: 48
currently lose_sum: 98.13508230447769
time_elpased: 1.79
batch start
#iterations: 49
currently lose_sum: 98.08044862747192
time_elpased: 1.813
batch start
#iterations: 50
currently lose_sum: 98.10001176595688
time_elpased: 1.828
batch start
#iterations: 51
currently lose_sum: 97.95237201452255
time_elpased: 1.783
batch start
#iterations: 52
currently lose_sum: 98.06596970558167
time_elpased: 1.841
batch start
#iterations: 53
currently lose_sum: 98.04415100812912
time_elpased: 1.758
batch start
#iterations: 54
currently lose_sum: 98.2263355255127
time_elpased: 1.775
batch start
#iterations: 55
currently lose_sum: 98.21432089805603
time_elpased: 1.816
batch start
#iterations: 56
currently lose_sum: 97.76307904720306
time_elpased: 1.783
batch start
#iterations: 57
currently lose_sum: 97.98757469654083
time_elpased: 1.776
batch start
#iterations: 58
currently lose_sum: 98.12999576330185
time_elpased: 1.802
batch start
#iterations: 59
currently lose_sum: 98.10369300842285
time_elpased: 1.77
start validation test
0.622989690722
0.632835820896
0.589070700834
0.610169491525
0.62304924074
63.855
batch start
#iterations: 60
currently lose_sum: 98.14416867494583
time_elpased: 1.797
batch start
#iterations: 61
currently lose_sum: 97.78105938434601
time_elpased: 1.794
batch start
#iterations: 62
currently lose_sum: 97.97612518072128
time_elpased: 1.733
batch start
#iterations: 63
currently lose_sum: 98.06389266252518
time_elpased: 1.84
batch start
#iterations: 64
currently lose_sum: 97.8788052201271
time_elpased: 1.799
batch start
#iterations: 65
currently lose_sum: 97.73076474666595
time_elpased: 1.831
batch start
#iterations: 66
currently lose_sum: 97.62477850914001
time_elpased: 1.815
batch start
#iterations: 67
currently lose_sum: 97.93036890029907
time_elpased: 1.823
batch start
#iterations: 68
currently lose_sum: 97.85905998945236
time_elpased: 1.798
batch start
#iterations: 69
currently lose_sum: 98.07097047567368
time_elpased: 1.765
batch start
#iterations: 70
currently lose_sum: 97.84125131368637
time_elpased: 1.796
batch start
#iterations: 71
currently lose_sum: 97.751658141613
time_elpased: 1.834
batch start
#iterations: 72
currently lose_sum: 97.75653141736984
time_elpased: 1.81
batch start
#iterations: 73
currently lose_sum: 97.6874834895134
time_elpased: 1.774
batch start
#iterations: 74
currently lose_sum: 97.72703117132187
time_elpased: 1.756
batch start
#iterations: 75
currently lose_sum: 97.75327324867249
time_elpased: 1.817
batch start
#iterations: 76
currently lose_sum: 97.56039118766785
time_elpased: 1.81
batch start
#iterations: 77
currently lose_sum: 97.85498768091202
time_elpased: 1.783
batch start
#iterations: 78
currently lose_sum: 97.69616085290909
time_elpased: 1.769
batch start
#iterations: 79
currently lose_sum: 97.93891406059265
time_elpased: 1.784
start validation test
0.620618556701
0.636101166416
0.566841617783
0.599477579451
0.620712970412
63.818
batch start
#iterations: 80
currently lose_sum: 97.8206856250763
time_elpased: 1.81
batch start
#iterations: 81
currently lose_sum: 97.67427092790604
time_elpased: 1.774
batch start
#iterations: 82
currently lose_sum: 97.84351712465286
time_elpased: 1.776
batch start
#iterations: 83
currently lose_sum: 97.47829556465149
time_elpased: 1.866
batch start
#iterations: 84
currently lose_sum: 97.5732324719429
time_elpased: 1.793
batch start
#iterations: 85
currently lose_sum: 97.64869314432144
time_elpased: 1.756
batch start
#iterations: 86
currently lose_sum: 97.78323513269424
time_elpased: 1.733
batch start
#iterations: 87
currently lose_sum: 97.43770182132721
time_elpased: 1.823
batch start
#iterations: 88
currently lose_sum: 97.49710696935654
time_elpased: 1.831
batch start
#iterations: 89
currently lose_sum: 97.38120222091675
time_elpased: 1.79
batch start
#iterations: 90
currently lose_sum: 97.81105029582977
time_elpased: 1.79
batch start
#iterations: 91
currently lose_sum: 97.68061709403992
time_elpased: 1.798
batch start
#iterations: 92
currently lose_sum: 97.46207118034363
time_elpased: 1.769
batch start
#iterations: 93
currently lose_sum: 97.7180517911911
time_elpased: 1.764
batch start
#iterations: 94
currently lose_sum: 97.45103561878204
time_elpased: 1.768
batch start
#iterations: 95
currently lose_sum: 97.7109934091568
time_elpased: 1.771
batch start
#iterations: 96
currently lose_sum: 97.70011693239212
time_elpased: 1.779
batch start
#iterations: 97
currently lose_sum: 97.40927445888519
time_elpased: 1.77
batch start
#iterations: 98
currently lose_sum: 97.72723418474197
time_elpased: 1.76
batch start
#iterations: 99
currently lose_sum: 97.35203891992569
time_elpased: 1.812
start validation test
0.623092783505
0.642594568955
0.557682412267
0.597134986226
0.623207621501
63.742
batch start
#iterations: 100
currently lose_sum: 97.87794548273087
time_elpased: 1.865
batch start
#iterations: 101
currently lose_sum: 97.63205146789551
time_elpased: 1.798
batch start
#iterations: 102
currently lose_sum: 97.26205217838287
time_elpased: 1.806
batch start
#iterations: 103
currently lose_sum: 97.43380832672119
time_elpased: 1.805
batch start
#iterations: 104
currently lose_sum: 97.66884142160416
time_elpased: 1.794
batch start
#iterations: 105
currently lose_sum: 97.61973565816879
time_elpased: 1.755
batch start
#iterations: 106
currently lose_sum: 97.64070522785187
time_elpased: 1.796
batch start
#iterations: 107
currently lose_sum: 97.48533511161804
time_elpased: 1.745
batch start
#iterations: 108
currently lose_sum: 97.50632828474045
time_elpased: 1.759
batch start
#iterations: 109
currently lose_sum: 97.8349831700325
time_elpased: 1.772
batch start
#iterations: 110
currently lose_sum: 97.30086410045624
time_elpased: 1.805
batch start
#iterations: 111
currently lose_sum: 97.50809162855148
time_elpased: 1.777
batch start
#iterations: 112
currently lose_sum: 97.44172692298889
time_elpased: 1.77
batch start
#iterations: 113
currently lose_sum: 97.51313108205795
time_elpased: 1.836
batch start
#iterations: 114
currently lose_sum: 97.29550921916962
time_elpased: 1.817
batch start
#iterations: 115
currently lose_sum: 97.37742120027542
time_elpased: 1.76
batch start
#iterations: 116
currently lose_sum: 97.78547948598862
time_elpased: 1.786
batch start
#iterations: 117
currently lose_sum: 97.54033368825912
time_elpased: 1.755
batch start
#iterations: 118
currently lose_sum: 97.6427634358406
time_elpased: 1.84
batch start
#iterations: 119
currently lose_sum: 97.4470831155777
time_elpased: 1.795
start validation test
0.628144329897
0.64190951355
0.582587218277
0.610811394044
0.628224312433
63.482
batch start
#iterations: 120
currently lose_sum: 97.61198765039444
time_elpased: 1.777
batch start
#iterations: 121
currently lose_sum: 97.5355988740921
time_elpased: 1.782
batch start
#iterations: 122
currently lose_sum: 97.68879359960556
time_elpased: 1.788
batch start
#iterations: 123
currently lose_sum: 97.3126038312912
time_elpased: 1.777
batch start
#iterations: 124
currently lose_sum: 97.07764679193497
time_elpased: 1.834
batch start
#iterations: 125
currently lose_sum: 97.47961270809174
time_elpased: 1.748
batch start
#iterations: 126
currently lose_sum: 97.70772343873978
time_elpased: 1.803
batch start
#iterations: 127
currently lose_sum: 97.67008155584335
time_elpased: 1.801
batch start
#iterations: 128
currently lose_sum: 97.69478142261505
time_elpased: 1.841
batch start
#iterations: 129
currently lose_sum: 97.23668789863586
time_elpased: 1.827
batch start
#iterations: 130
currently lose_sum: 97.5453509092331
time_elpased: 1.889
batch start
#iterations: 131
currently lose_sum: 97.33451986312866
time_elpased: 1.79
batch start
#iterations: 132
currently lose_sum: 97.28524827957153
time_elpased: 1.832
batch start
#iterations: 133
currently lose_sum: 97.31983602046967
time_elpased: 1.865
batch start
#iterations: 134
currently lose_sum: 97.34319311380386
time_elpased: 1.804
batch start
#iterations: 135
currently lose_sum: 97.614881336689
time_elpased: 1.774
batch start
#iterations: 136
currently lose_sum: 97.19321173429489
time_elpased: 1.779
batch start
#iterations: 137
currently lose_sum: 97.54071629047394
time_elpased: 1.757
batch start
#iterations: 138
currently lose_sum: 97.20278090238571
time_elpased: 1.761
batch start
#iterations: 139
currently lose_sum: 97.32225054502487
time_elpased: 1.774
start validation test
0.628350515464
0.637249534655
0.5989502933
0.6175066313
0.628402132089
63.443
batch start
#iterations: 140
currently lose_sum: 97.56552141904831
time_elpased: 1.808
batch start
#iterations: 141
currently lose_sum: 97.61303120851517
time_elpased: 1.733
batch start
#iterations: 142
currently lose_sum: 97.52731668949127
time_elpased: 1.781
batch start
#iterations: 143
currently lose_sum: 97.45265084505081
time_elpased: 1.756
batch start
#iterations: 144
currently lose_sum: 97.50229334831238
time_elpased: 1.795
batch start
#iterations: 145
currently lose_sum: 97.69838547706604
time_elpased: 1.727
batch start
#iterations: 146
currently lose_sum: 97.09943962097168
time_elpased: 1.769
batch start
#iterations: 147
currently lose_sum: 97.31921577453613
time_elpased: 1.741
batch start
#iterations: 148
currently lose_sum: 97.69636255502701
time_elpased: 1.719
batch start
#iterations: 149
currently lose_sum: 97.3984626531601
time_elpased: 1.732
batch start
#iterations: 150
currently lose_sum: 97.24582439661026
time_elpased: 1.758
batch start
#iterations: 151
currently lose_sum: 97.25247198343277
time_elpased: 1.732
batch start
#iterations: 152
currently lose_sum: 97.38780462741852
time_elpased: 1.737
batch start
#iterations: 153
currently lose_sum: 97.6459412574768
time_elpased: 1.75
batch start
#iterations: 154
currently lose_sum: 97.54073578119278
time_elpased: 1.734
batch start
#iterations: 155
currently lose_sum: 97.28655290603638
time_elpased: 1.754
batch start
#iterations: 156
currently lose_sum: 97.32842963933945
time_elpased: 1.745
batch start
#iterations: 157
currently lose_sum: 97.72648823261261
time_elpased: 1.732
batch start
#iterations: 158
currently lose_sum: 97.64703011512756
time_elpased: 1.741
batch start
#iterations: 159
currently lose_sum: 97.81392401456833
time_elpased: 1.733
start validation test
0.626340206186
0.644428838951
0.56663579294
0.603033787854
0.626445026492
63.781
batch start
#iterations: 160
currently lose_sum: 97.3361701965332
time_elpased: 1.752
batch start
#iterations: 161
currently lose_sum: 97.36602079868317
time_elpased: 1.768
batch start
#iterations: 162
currently lose_sum: 97.22980523109436
time_elpased: 1.739
batch start
#iterations: 163
currently lose_sum: 97.3143898844719
time_elpased: 1.74
batch start
#iterations: 164
currently lose_sum: 97.29978406429291
time_elpased: 1.843
batch start
#iterations: 165
currently lose_sum: 97.54181665182114
time_elpased: 1.824
batch start
#iterations: 166
currently lose_sum: 97.33156192302704
time_elpased: 1.725
batch start
#iterations: 167
currently lose_sum: 97.371511220932
time_elpased: 1.771
batch start
#iterations: 168
currently lose_sum: 97.07831919193268
time_elpased: 1.757
batch start
#iterations: 169
currently lose_sum: 97.29485273361206
time_elpased: 1.808
batch start
#iterations: 170
currently lose_sum: 97.24106013774872
time_elpased: 1.842
batch start
#iterations: 171
currently lose_sum: 97.40197622776031
time_elpased: 1.746
batch start
#iterations: 172
currently lose_sum: 97.65546369552612
time_elpased: 1.779
batch start
#iterations: 173
currently lose_sum: 97.31986010074615
time_elpased: 1.89
batch start
#iterations: 174
currently lose_sum: 97.36225014925003
time_elpased: 1.767
batch start
#iterations: 175
currently lose_sum: 97.24400591850281
time_elpased: 1.754
batch start
#iterations: 176
currently lose_sum: 97.3827292919159
time_elpased: 1.813
batch start
#iterations: 177
currently lose_sum: 97.33615797758102
time_elpased: 1.821
batch start
#iterations: 178
currently lose_sum: 97.04447001218796
time_elpased: 1.727
batch start
#iterations: 179
currently lose_sum: 97.56230652332306
time_elpased: 1.753
start validation test
0.626288659794
0.63619299989
0.592981372852
0.613827633962
0.626347135873
63.664
batch start
#iterations: 180
currently lose_sum: 97.35338789224625
time_elpased: 1.771
batch start
#iterations: 181
currently lose_sum: 97.5698111653328
time_elpased: 1.739
batch start
#iterations: 182
currently lose_sum: 97.48551678657532
time_elpased: 1.744
batch start
#iterations: 183
currently lose_sum: 97.016794860363
time_elpased: 1.757
batch start
#iterations: 184
currently lose_sum: 97.1400356888771
time_elpased: 1.753
batch start
#iterations: 185
currently lose_sum: 97.3215880393982
time_elpased: 1.779
batch start
#iterations: 186
currently lose_sum: 97.27991592884064
time_elpased: 1.715
batch start
#iterations: 187
currently lose_sum: 96.74789607524872
time_elpased: 1.745
batch start
#iterations: 188
currently lose_sum: 97.64397352933884
time_elpased: 1.73
batch start
#iterations: 189
currently lose_sum: 97.24729710817337
time_elpased: 1.755
batch start
#iterations: 190
currently lose_sum: 97.57009780406952
time_elpased: 1.737
batch start
#iterations: 191
currently lose_sum: 97.24359011650085
time_elpased: 1.743
batch start
#iterations: 192
currently lose_sum: 97.0864867568016
time_elpased: 1.733
batch start
#iterations: 193
currently lose_sum: 97.27824866771698
time_elpased: 1.772
batch start
#iterations: 194
currently lose_sum: 97.31806123256683
time_elpased: 1.732
batch start
#iterations: 195
currently lose_sum: 97.24335956573486
time_elpased: 1.781
batch start
#iterations: 196
currently lose_sum: 97.40903729200363
time_elpased: 1.794
batch start
#iterations: 197
currently lose_sum: 97.29453551769257
time_elpased: 1.781
batch start
#iterations: 198
currently lose_sum: 97.34666681289673
time_elpased: 1.731
batch start
#iterations: 199
currently lose_sum: 97.2348860502243
time_elpased: 1.729
start validation test
0.624536082474
0.64539261384
0.555727076258
0.597213005972
0.624656887298
63.595
batch start
#iterations: 200
currently lose_sum: 97.1426312327385
time_elpased: 1.752
batch start
#iterations: 201
currently lose_sum: 97.30450236797333
time_elpased: 1.758
batch start
#iterations: 202
currently lose_sum: 96.97521811723709
time_elpased: 1.744
batch start
#iterations: 203
currently lose_sum: 97.28056108951569
time_elpased: 1.752
batch start
#iterations: 204
currently lose_sum: 96.93022900819778
time_elpased: 1.746
batch start
#iterations: 205
currently lose_sum: 97.15211510658264
time_elpased: 1.77
batch start
#iterations: 206
currently lose_sum: 97.36567467451096
time_elpased: 1.767
batch start
#iterations: 207
currently lose_sum: 97.16893696784973
time_elpased: 1.87
batch start
#iterations: 208
currently lose_sum: 96.9377304315567
time_elpased: 1.804
batch start
#iterations: 209
currently lose_sum: 97.09665775299072
time_elpased: 1.802
batch start
#iterations: 210
currently lose_sum: 97.03721410036087
time_elpased: 1.794
batch start
#iterations: 211
currently lose_sum: 97.20857554674149
time_elpased: 1.812
batch start
#iterations: 212
currently lose_sum: 96.95205050706863
time_elpased: 1.786
batch start
#iterations: 213
currently lose_sum: 97.25640791654587
time_elpased: 1.795
batch start
#iterations: 214
currently lose_sum: 97.2950946688652
time_elpased: 1.773
batch start
#iterations: 215
currently lose_sum: 97.21233141422272
time_elpased: 1.742
batch start
#iterations: 216
currently lose_sum: 96.80183988809586
time_elpased: 1.757
batch start
#iterations: 217
currently lose_sum: 97.34766417741776
time_elpased: 1.775
batch start
#iterations: 218
currently lose_sum: 97.3221081495285
time_elpased: 1.779
batch start
#iterations: 219
currently lose_sum: 97.09909129142761
time_elpased: 1.819
start validation test
0.627680412371
0.643995381062
0.573942574869
0.606954345105
0.627774757433
63.638
batch start
#iterations: 220
currently lose_sum: 97.33974039554596
time_elpased: 1.775
batch start
#iterations: 221
currently lose_sum: 97.1488026380539
time_elpased: 1.808
batch start
#iterations: 222
currently lose_sum: 97.12337678670883
time_elpased: 1.728
batch start
#iterations: 223
currently lose_sum: 97.03819185495377
time_elpased: 1.753
batch start
#iterations: 224
currently lose_sum: 97.08468228578568
time_elpased: 1.745
batch start
#iterations: 225
currently lose_sum: 97.15800720453262
time_elpased: 1.761
batch start
#iterations: 226
currently lose_sum: 97.19837468862534
time_elpased: 1.788
batch start
#iterations: 227
currently lose_sum: 97.10604399442673
time_elpased: 1.759
batch start
#iterations: 228
currently lose_sum: 96.93081945180893
time_elpased: 1.766
batch start
#iterations: 229
currently lose_sum: 97.05714166164398
time_elpased: 1.766
batch start
#iterations: 230
currently lose_sum: 97.22759813070297
time_elpased: 1.731
batch start
#iterations: 231
currently lose_sum: 97.20487296581268
time_elpased: 1.786
batch start
#iterations: 232
currently lose_sum: 97.21793031692505
time_elpased: 1.763
batch start
#iterations: 233
currently lose_sum: 97.17024344205856
time_elpased: 1.747
batch start
#iterations: 234
currently lose_sum: 97.13359194993973
time_elpased: 1.78
batch start
#iterations: 235
currently lose_sum: 97.05753654241562
time_elpased: 1.795
batch start
#iterations: 236
currently lose_sum: 97.34822368621826
time_elpased: 1.774
batch start
#iterations: 237
currently lose_sum: 97.30569714307785
time_elpased: 1.913
batch start
#iterations: 238
currently lose_sum: 97.31091499328613
time_elpased: 1.813
batch start
#iterations: 239
currently lose_sum: 97.0783063173294
time_elpased: 1.87
start validation test
0.621546391753
0.647680636737
0.535967891324
0.5865525397
0.62169663801
63.753
batch start
#iterations: 240
currently lose_sum: 96.8651881814003
time_elpased: 1.786
batch start
#iterations: 241
currently lose_sum: 97.09042292833328
time_elpased: 1.865
batch start
#iterations: 242
currently lose_sum: 97.2116630077362
time_elpased: 1.821
batch start
#iterations: 243
currently lose_sum: 97.22184246778488
time_elpased: 1.76
batch start
#iterations: 244
currently lose_sum: 97.30541080236435
time_elpased: 1.753
batch start
#iterations: 245
currently lose_sum: 97.48337066173553
time_elpased: 1.868
batch start
#iterations: 246
currently lose_sum: 96.88252645730972
time_elpased: 1.784
batch start
#iterations: 247
currently lose_sum: 96.9566957950592
time_elpased: 1.782
batch start
#iterations: 248
currently lose_sum: 97.11628669500351
time_elpased: 1.739
batch start
#iterations: 249
currently lose_sum: 97.30574637651443
time_elpased: 1.768
batch start
#iterations: 250
currently lose_sum: 97.03414905071259
time_elpased: 1.742
batch start
#iterations: 251
currently lose_sum: 97.12274241447449
time_elpased: 1.749
batch start
#iterations: 252
currently lose_sum: 97.2916225194931
time_elpased: 1.811
batch start
#iterations: 253
currently lose_sum: 97.14652597904205
time_elpased: 1.812
batch start
#iterations: 254
currently lose_sum: 97.05774992704391
time_elpased: 1.74
batch start
#iterations: 255
currently lose_sum: 97.17953211069107
time_elpased: 1.769
batch start
#iterations: 256
currently lose_sum: 97.05654728412628
time_elpased: 1.768
batch start
#iterations: 257
currently lose_sum: 96.8647346496582
time_elpased: 1.731
batch start
#iterations: 258
currently lose_sum: 97.13757717609406
time_elpased: 1.827
batch start
#iterations: 259
currently lose_sum: 97.00611644983292
time_elpased: 1.825
start validation test
0.62587628866
0.640305831336
0.577441597201
0.607251082251
0.625961323231
63.544
batch start
#iterations: 260
currently lose_sum: 97.04516661167145
time_elpased: 1.774
batch start
#iterations: 261
currently lose_sum: 96.92363208532333
time_elpased: 1.804
batch start
#iterations: 262
currently lose_sum: 96.82895517349243
time_elpased: 1.775
batch start
#iterations: 263
currently lose_sum: 97.05448424816132
time_elpased: 1.827
batch start
#iterations: 264
currently lose_sum: 96.83151423931122
time_elpased: 1.8
batch start
#iterations: 265
currently lose_sum: 97.41834765672684
time_elpased: 1.745
batch start
#iterations: 266
currently lose_sum: 97.15907210111618
time_elpased: 1.853
batch start
#iterations: 267
currently lose_sum: 97.08281695842743
time_elpased: 1.736
batch start
#iterations: 268
currently lose_sum: 96.84813058376312
time_elpased: 1.74
batch start
#iterations: 269
currently lose_sum: 97.18877726793289
time_elpased: 1.78
batch start
#iterations: 270
currently lose_sum: 96.99946093559265
time_elpased: 1.76
batch start
#iterations: 271
currently lose_sum: 97.05946755409241
time_elpased: 1.775
batch start
#iterations: 272
currently lose_sum: 97.26104491949081
time_elpased: 1.892
batch start
#iterations: 273
currently lose_sum: 97.04292154312134
time_elpased: 1.769
batch start
#iterations: 274
currently lose_sum: 97.04280096292496
time_elpased: 1.752
batch start
#iterations: 275
currently lose_sum: 97.28322160243988
time_elpased: 1.764
batch start
#iterations: 276
currently lose_sum: 96.99502265453339
time_elpased: 1.784
batch start
#iterations: 277
currently lose_sum: 97.07864117622375
time_elpased: 1.809
batch start
#iterations: 278
currently lose_sum: 96.84043526649475
time_elpased: 1.787
batch start
#iterations: 279
currently lose_sum: 97.09597551822662
time_elpased: 1.728
start validation test
0.62793814433
0.637625289129
0.595760008233
0.615982123856
0.627994638011
63.428
batch start
#iterations: 280
currently lose_sum: 96.86709892749786
time_elpased: 1.845
batch start
#iterations: 281
currently lose_sum: 97.20372068881989
time_elpased: 1.759
batch start
#iterations: 282
currently lose_sum: 97.08562016487122
time_elpased: 1.785
batch start
#iterations: 283
currently lose_sum: 97.03498411178589
time_elpased: 1.823
batch start
#iterations: 284
currently lose_sum: 97.08911216259003
time_elpased: 1.846
batch start
#iterations: 285
currently lose_sum: 96.73374027013779
time_elpased: 1.781
batch start
#iterations: 286
currently lose_sum: 97.06910711526871
time_elpased: 1.78
batch start
#iterations: 287
currently lose_sum: 96.93564641475677
time_elpased: 1.757
batch start
#iterations: 288
currently lose_sum: 96.77810859680176
time_elpased: 1.885
batch start
#iterations: 289
currently lose_sum: 97.28687769174576
time_elpased: 1.796
batch start
#iterations: 290
currently lose_sum: 97.35809510946274
time_elpased: 1.813
batch start
#iterations: 291
currently lose_sum: 96.90352368354797
time_elpased: 1.796
batch start
#iterations: 292
currently lose_sum: 97.08123230934143
time_elpased: 1.81
batch start
#iterations: 293
currently lose_sum: 97.10920602083206
time_elpased: 1.77
batch start
#iterations: 294
currently lose_sum: 97.01372528076172
time_elpased: 1.86
batch start
#iterations: 295
currently lose_sum: 96.89136797189713
time_elpased: 1.787
batch start
#iterations: 296
currently lose_sum: 97.15088790655136
time_elpased: 1.758
batch start
#iterations: 297
currently lose_sum: 97.043630361557
time_elpased: 1.819
batch start
#iterations: 298
currently lose_sum: 97.1732206940651
time_elpased: 1.831
batch start
#iterations: 299
currently lose_sum: 96.84873253107071
time_elpased: 1.76
start validation test
0.625567010309
0.636535678504
0.588453226304
0.611550802139
0.625632169282
63.565
batch start
#iterations: 300
currently lose_sum: 97.02298313379288
time_elpased: 1.863
batch start
#iterations: 301
currently lose_sum: 96.75461310148239
time_elpased: 1.792
batch start
#iterations: 302
currently lose_sum: 97.06966346502304
time_elpased: 1.796
batch start
#iterations: 303
currently lose_sum: 96.82444751262665
time_elpased: 1.84
batch start
#iterations: 304
currently lose_sum: 97.16332441568375
time_elpased: 1.784
batch start
#iterations: 305
currently lose_sum: 97.26059621572495
time_elpased: 1.798
batch start
#iterations: 306
currently lose_sum: 96.99016463756561
time_elpased: 1.794
batch start
#iterations: 307
currently lose_sum: 97.05736345052719
time_elpased: 1.747
batch start
#iterations: 308
currently lose_sum: 96.73236989974976
time_elpased: 1.776
batch start
#iterations: 309
currently lose_sum: 97.09426862001419
time_elpased: 1.854
batch start
#iterations: 310
currently lose_sum: 96.95132154226303
time_elpased: 1.786
batch start
#iterations: 311
currently lose_sum: 97.00760269165039
time_elpased: 1.724
batch start
#iterations: 312
currently lose_sum: 97.14536517858505
time_elpased: 1.763
batch start
#iterations: 313
currently lose_sum: 97.07703238725662
time_elpased: 1.761
batch start
#iterations: 314
currently lose_sum: 96.91278940439224
time_elpased: 1.815
batch start
#iterations: 315
currently lose_sum: 96.90483945608139
time_elpased: 1.733
batch start
#iterations: 316
currently lose_sum: 96.94798350334167
time_elpased: 1.832
batch start
#iterations: 317
currently lose_sum: 97.23449337482452
time_elpased: 1.733
batch start
#iterations: 318
currently lose_sum: 97.11671084165573
time_elpased: 1.724
batch start
#iterations: 319
currently lose_sum: 96.7950781583786
time_elpased: 1.766
start validation test
0.627835051546
0.639108635097
0.590305649892
0.613738497753
0.6279009402
63.484
batch start
#iterations: 320
currently lose_sum: 97.2622646689415
time_elpased: 1.766
batch start
#iterations: 321
currently lose_sum: 96.91014295816422
time_elpased: 1.786
batch start
#iterations: 322
currently lose_sum: 96.92957520484924
time_elpased: 1.815
batch start
#iterations: 323
currently lose_sum: 96.93952345848083
time_elpased: 1.749
batch start
#iterations: 324
currently lose_sum: 97.17687433958054
time_elpased: 1.731
batch start
#iterations: 325
currently lose_sum: 97.11472278833389
time_elpased: 1.79
batch start
#iterations: 326
currently lose_sum: 96.96892523765564
time_elpased: 1.753
batch start
#iterations: 327
currently lose_sum: 96.90728467702866
time_elpased: 1.771
batch start
#iterations: 328
currently lose_sum: 97.15011769533157
time_elpased: 1.75
batch start
#iterations: 329
currently lose_sum: 96.81334608793259
time_elpased: 1.792
batch start
#iterations: 330
currently lose_sum: 96.88237619400024
time_elpased: 1.777
batch start
#iterations: 331
currently lose_sum: 97.01200640201569
time_elpased: 1.819
batch start
#iterations: 332
currently lose_sum: 96.84860181808472
time_elpased: 1.795
batch start
#iterations: 333
currently lose_sum: 96.64489620923996
time_elpased: 1.882
batch start
#iterations: 334
currently lose_sum: 96.85246855020523
time_elpased: 1.866
batch start
#iterations: 335
currently lose_sum: 96.80938106775284
time_elpased: 1.866
batch start
#iterations: 336
currently lose_sum: 96.71813982725143
time_elpased: 1.837
batch start
#iterations: 337
currently lose_sum: 97.07560557126999
time_elpased: 1.748
batch start
#iterations: 338
currently lose_sum: 96.77287811040878
time_elpased: 1.819
batch start
#iterations: 339
currently lose_sum: 96.99564409255981
time_elpased: 1.837
start validation test
0.627989690722
0.635222847252
0.60430173922
0.619376615157
0.628031278574
63.541
batch start
#iterations: 340
currently lose_sum: 97.06473791599274
time_elpased: 1.778
batch start
#iterations: 341
currently lose_sum: 97.22501522302628
time_elpased: 1.802
batch start
#iterations: 342
currently lose_sum: 96.88644540309906
time_elpased: 1.764
batch start
#iterations: 343
currently lose_sum: 97.06676286458969
time_elpased: 1.786
batch start
#iterations: 344
currently lose_sum: 97.00360852479935
time_elpased: 1.775
batch start
#iterations: 345
currently lose_sum: 96.99570769071579
time_elpased: 1.761
batch start
#iterations: 346
currently lose_sum: 96.80269402265549
time_elpased: 1.779
batch start
#iterations: 347
currently lose_sum: 96.8711769580841
time_elpased: 1.745
batch start
#iterations: 348
currently lose_sum: 96.98377168178558
time_elpased: 1.752
batch start
#iterations: 349
currently lose_sum: 97.08093136548996
time_elpased: 1.774
batch start
#iterations: 350
currently lose_sum: 97.15449333190918
time_elpased: 1.791
batch start
#iterations: 351
currently lose_sum: 96.96063858270645
time_elpased: 1.784
batch start
#iterations: 352
currently lose_sum: 97.03224128484726
time_elpased: 1.777
batch start
#iterations: 353
currently lose_sum: 97.24259704351425
time_elpased: 1.771
batch start
#iterations: 354
currently lose_sum: 97.55950260162354
time_elpased: 1.762
batch start
#iterations: 355
currently lose_sum: 97.41432499885559
time_elpased: 1.81
batch start
#iterations: 356
currently lose_sum: 97.0117917060852
time_elpased: 1.744
batch start
#iterations: 357
currently lose_sum: 96.86783045530319
time_elpased: 1.825
batch start
#iterations: 358
currently lose_sum: 97.43313521146774
time_elpased: 1.773
batch start
#iterations: 359
currently lose_sum: 97.07556116580963
time_elpased: 1.828
start validation test
0.623969072165
0.632378662003
0.595348358547
0.613305062285
0.624019320242
63.484
batch start
#iterations: 360
currently lose_sum: 97.01773554086685
time_elpased: 1.796
batch start
#iterations: 361
currently lose_sum: 97.28225082159042
time_elpased: 1.782
batch start
#iterations: 362
currently lose_sum: 96.94280898571014
time_elpased: 1.755
batch start
#iterations: 363
currently lose_sum: 96.76016759872437
time_elpased: 1.829
batch start
#iterations: 364
currently lose_sum: 97.42280900478363
time_elpased: 1.738
batch start
#iterations: 365
currently lose_sum: 96.95459806919098
time_elpased: 1.746
batch start
#iterations: 366
currently lose_sum: 97.06022149324417
time_elpased: 1.808
batch start
#iterations: 367
currently lose_sum: 96.82893097400665
time_elpased: 1.805
batch start
#iterations: 368
currently lose_sum: 97.19281673431396
time_elpased: 1.867
batch start
#iterations: 369
currently lose_sum: 96.84781432151794
time_elpased: 1.811
batch start
#iterations: 370
currently lose_sum: 96.95295304059982
time_elpased: 1.821
batch start
#iterations: 371
currently lose_sum: 96.83552539348602
time_elpased: 1.771
batch start
#iterations: 372
currently lose_sum: 97.19515681266785
time_elpased: 1.861
batch start
#iterations: 373
currently lose_sum: 97.06675374507904
time_elpased: 1.769
batch start
#iterations: 374
currently lose_sum: 96.93728911876678
time_elpased: 1.783
batch start
#iterations: 375
currently lose_sum: 97.11748385429382
time_elpased: 1.8
batch start
#iterations: 376
currently lose_sum: 96.7213271856308
time_elpased: 1.83
batch start
#iterations: 377
currently lose_sum: 97.17746502161026
time_elpased: 1.731
batch start
#iterations: 378
currently lose_sum: 96.91542148590088
time_elpased: 1.826
batch start
#iterations: 379
currently lose_sum: 96.74001026153564
time_elpased: 1.772
start validation test
0.628969072165
0.638696178835
0.59689204487
0.617086924141
0.629025388334
63.530
batch start
#iterations: 380
currently lose_sum: 96.94515526294708
time_elpased: 1.775
batch start
#iterations: 381
currently lose_sum: 96.89387255907059
time_elpased: 1.748
batch start
#iterations: 382
currently lose_sum: 96.90936297178268
time_elpased: 1.764
batch start
#iterations: 383
currently lose_sum: 96.90773040056229
time_elpased: 1.743
batch start
#iterations: 384
currently lose_sum: 96.78783863782883
time_elpased: 1.731
batch start
#iterations: 385
currently lose_sum: 96.91954559087753
time_elpased: 1.807
batch start
#iterations: 386
currently lose_sum: 96.98434692621231
time_elpased: 1.807
batch start
#iterations: 387
currently lose_sum: 96.61990016698837
time_elpased: 1.775
batch start
#iterations: 388
currently lose_sum: 96.92430329322815
time_elpased: 1.789
batch start
#iterations: 389
currently lose_sum: 96.92695260047913
time_elpased: 1.802
batch start
#iterations: 390
currently lose_sum: 97.08787035942078
time_elpased: 1.775
batch start
#iterations: 391
currently lose_sum: 96.7991972565651
time_elpased: 1.804
batch start
#iterations: 392
currently lose_sum: 96.74280035495758
time_elpased: 1.819
batch start
#iterations: 393
currently lose_sum: 96.87672090530396
time_elpased: 1.733
batch start
#iterations: 394
currently lose_sum: 96.77004706859589
time_elpased: 1.731
batch start
#iterations: 395
currently lose_sum: 97.1298998594284
time_elpased: 1.756
batch start
#iterations: 396
currently lose_sum: 96.74906224012375
time_elpased: 1.856
batch start
#iterations: 397
currently lose_sum: 96.87540501356125
time_elpased: 1.73
batch start
#iterations: 398
currently lose_sum: 96.77825492620468
time_elpased: 1.751
batch start
#iterations: 399
currently lose_sum: 96.94353657960892
time_elpased: 1.796
start validation test
0.628608247423
0.640965207632
0.587732839354
0.613195898427
0.628680010506
63.591
acc: 0.632
pre: 0.641
rec: 0.602
F1: 0.621
auc: 0.632
