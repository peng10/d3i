start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.74593436717987
time_elpased: 2.502
batch start
#iterations: 1
currently lose_sum: 100.50130730867386
time_elpased: 2.407
batch start
#iterations: 2
currently lose_sum: 100.27729040384293
time_elpased: 2.379
batch start
#iterations: 3
currently lose_sum: 100.02605736255646
time_elpased: 2.48
batch start
#iterations: 4
currently lose_sum: 99.9723652601242
time_elpased: 2.381
batch start
#iterations: 5
currently lose_sum: 99.73861157894135
time_elpased: 2.48
batch start
#iterations: 6
currently lose_sum: 99.77332264184952
time_elpased: 2.443
batch start
#iterations: 7
currently lose_sum: 99.53614038228989
time_elpased: 2.467
batch start
#iterations: 8
currently lose_sum: 99.36767095327377
time_elpased: 2.494
batch start
#iterations: 9
currently lose_sum: 99.00226944684982
time_elpased: 2.588
batch start
#iterations: 10
currently lose_sum: 99.36722695827484
time_elpased: 2.461
batch start
#iterations: 11
currently lose_sum: 99.13019931316376
time_elpased: 2.592
batch start
#iterations: 12
currently lose_sum: 98.94753497838974
time_elpased: 2.536
batch start
#iterations: 13
currently lose_sum: 99.33796578645706
time_elpased: 2.611
batch start
#iterations: 14
currently lose_sum: 98.96117025613785
time_elpased: 2.602
batch start
#iterations: 15
currently lose_sum: 99.0313007235527
time_elpased: 2.612
batch start
#iterations: 16
currently lose_sum: 98.92538976669312
time_elpased: 2.629
batch start
#iterations: 17
currently lose_sum: 99.07934945821762
time_elpased: 2.6
batch start
#iterations: 18
currently lose_sum: 98.91226726770401
time_elpased: 2.459
batch start
#iterations: 19
currently lose_sum: 98.74403548240662
time_elpased: 2.587
start validation test
0.617989690722
0.627672537942
0.583161794977
0.604599050312
0.618047233713
64.447
batch start
#iterations: 20
currently lose_sum: 98.80017638206482
time_elpased: 2.553
batch start
#iterations: 21
currently lose_sum: 98.3538920879364
time_elpased: 2.567
batch start
#iterations: 22
currently lose_sum: 98.62940490245819
time_elpased: 2.526
batch start
#iterations: 23
currently lose_sum: 98.82394343614578
time_elpased: 2.614
batch start
#iterations: 24
currently lose_sum: 98.5247813463211
time_elpased: 2.545
batch start
#iterations: 25
currently lose_sum: 98.64971250295639
time_elpased: 2.557
batch start
#iterations: 26
currently lose_sum: 98.4913467168808
time_elpased: 2.559
batch start
#iterations: 27
currently lose_sum: 98.69005650281906
time_elpased: 2.593
batch start
#iterations: 28
currently lose_sum: 98.78174692392349
time_elpased: 2.368
batch start
#iterations: 29
currently lose_sum: 98.70811039209366
time_elpased: 2.358
batch start
#iterations: 30
currently lose_sum: 98.5053214430809
time_elpased: 2.386
batch start
#iterations: 31
currently lose_sum: 98.53322094678879
time_elpased: 2.348
batch start
#iterations: 32
currently lose_sum: 98.49567395448685
time_elpased: 2.256
batch start
#iterations: 33
currently lose_sum: 98.3625739812851
time_elpased: 2.301
batch start
#iterations: 34
currently lose_sum: 98.22968953847885
time_elpased: 2.261
batch start
#iterations: 35
currently lose_sum: 98.51929193735123
time_elpased: 2.157
batch start
#iterations: 36
currently lose_sum: 98.31529831886292
time_elpased: 2.401
batch start
#iterations: 37
currently lose_sum: 98.45249497890472
time_elpased: 2.448
batch start
#iterations: 38
currently lose_sum: 98.4106593132019
time_elpased: 2.325
batch start
#iterations: 39
currently lose_sum: 98.45022892951965
time_elpased: 2.374
start validation test
0.621494845361
0.619210288355
0.63431453273
0.626671411866
0.621473664547
63.886
batch start
#iterations: 40
currently lose_sum: 98.24754679203033
time_elpased: 2.425
batch start
#iterations: 41
currently lose_sum: 98.25922620296478
time_elpased: 2.449
batch start
#iterations: 42
currently lose_sum: 98.18594926595688
time_elpased: 2.411
batch start
#iterations: 43
currently lose_sum: 98.13957977294922
time_elpased: 2.426
batch start
#iterations: 44
currently lose_sum: 98.38786262273788
time_elpased: 2.56
batch start
#iterations: 45
currently lose_sum: 98.3286172747612
time_elpased: 2.836
batch start
#iterations: 46
currently lose_sum: 98.41327238082886
time_elpased: 3.068
batch start
#iterations: 47
currently lose_sum: 98.20094382762909
time_elpased: 2.874
batch start
#iterations: 48
currently lose_sum: 98.35387450456619
time_elpased: 2.523
batch start
#iterations: 49
currently lose_sum: 98.12620866298676
time_elpased: 2.529
batch start
#iterations: 50
currently lose_sum: 98.26691174507141
time_elpased: 2.444
batch start
#iterations: 51
currently lose_sum: 98.15492641925812
time_elpased: 2.793
batch start
#iterations: 52
currently lose_sum: 98.14858824014664
time_elpased: 2.66
batch start
#iterations: 53
currently lose_sum: 98.19673883914948
time_elpased: 2.65
batch start
#iterations: 54
currently lose_sum: 98.12206780910492
time_elpased: 3.063
batch start
#iterations: 55
currently lose_sum: 98.22064113616943
time_elpased: 3.061
batch start
#iterations: 56
currently lose_sum: 98.28959399461746
time_elpased: 3.022
batch start
#iterations: 57
currently lose_sum: 98.0545483827591
time_elpased: 2.868
batch start
#iterations: 58
currently lose_sum: 98.11084097623825
time_elpased: 2.623
batch start
#iterations: 59
currently lose_sum: 98.238001704216
time_elpased: 2.389
start validation test
0.624639175258
0.646309208944
0.553314121037
0.596207164245
0.624757019213
63.975
batch start
#iterations: 60
currently lose_sum: 98.07546103000641
time_elpased: 2.555
batch start
#iterations: 61
currently lose_sum: 98.34020173549652
time_elpased: 2.617
batch start
#iterations: 62
currently lose_sum: 98.19760620594025
time_elpased: 2.501
batch start
#iterations: 63
currently lose_sum: 98.0578293800354
time_elpased: 2.443
batch start
#iterations: 64
currently lose_sum: 98.00737822055817
time_elpased: 2.665
batch start
#iterations: 65
currently lose_sum: 98.11603379249573
time_elpased: 2.567
batch start
#iterations: 66
currently lose_sum: 98.18872004747391
time_elpased: 2.366
batch start
#iterations: 67
currently lose_sum: 98.16954833269119
time_elpased: 2.376
batch start
#iterations: 68
currently lose_sum: 97.87409120798111
time_elpased: 2.522
batch start
#iterations: 69
currently lose_sum: 98.14625763893127
time_elpased: 2.5
batch start
#iterations: 70
currently lose_sum: 98.02884936332703
time_elpased: 2.725
batch start
#iterations: 71
currently lose_sum: 97.82649821043015
time_elpased: 2.613
batch start
#iterations: 72
currently lose_sum: 97.90582370758057
time_elpased: 2.621
batch start
#iterations: 73
currently lose_sum: 97.98935979604721
time_elpased: 2.693
batch start
#iterations: 74
currently lose_sum: 98.0255075097084
time_elpased: 2.56
batch start
#iterations: 75
currently lose_sum: 98.15351694822311
time_elpased: 2.459
batch start
#iterations: 76
currently lose_sum: 97.95535010099411
time_elpased: 2.48
batch start
#iterations: 77
currently lose_sum: 97.8650621175766
time_elpased: 2.585
batch start
#iterations: 78
currently lose_sum: 98.18692469596863
time_elpased: 2.526
batch start
#iterations: 79
currently lose_sum: 97.77818441390991
time_elpased: 2.561
start validation test
0.63
0.624901574803
0.653458213256
0.638860937814
0.629961242109
63.478
batch start
#iterations: 80
currently lose_sum: 98.11485993862152
time_elpased: 2.566
batch start
#iterations: 81
currently lose_sum: 98.25888735055923
time_elpased: 2.681
batch start
#iterations: 82
currently lose_sum: 97.6109521985054
time_elpased: 2.619
batch start
#iterations: 83
currently lose_sum: 97.91694462299347
time_elpased: 2.629
batch start
#iterations: 84
currently lose_sum: 97.72020065784454
time_elpased: 2.761
batch start
#iterations: 85
currently lose_sum: 97.70952278375626
time_elpased: 2.602
batch start
#iterations: 86
currently lose_sum: 98.07970005273819
time_elpased: 2.739
batch start
#iterations: 87
currently lose_sum: 97.70772683620453
time_elpased: 2.661
batch start
#iterations: 88
currently lose_sum: 97.95800137519836
time_elpased: 2.641
batch start
#iterations: 89
currently lose_sum: 97.96809148788452
time_elpased: 2.551
batch start
#iterations: 90
currently lose_sum: 97.7604169845581
time_elpased: 2.559
batch start
#iterations: 91
currently lose_sum: 97.93380212783813
time_elpased: 2.425
batch start
#iterations: 92
currently lose_sum: 97.6753665804863
time_elpased: 2.541
batch start
#iterations: 93
currently lose_sum: 97.8107482790947
time_elpased: 2.585
batch start
#iterations: 94
currently lose_sum: 97.73771286010742
time_elpased: 2.444
batch start
#iterations: 95
currently lose_sum: 98.02798449993134
time_elpased: 2.474
batch start
#iterations: 96
currently lose_sum: 98.04504197835922
time_elpased: 2.582
batch start
#iterations: 97
currently lose_sum: 97.77700698375702
time_elpased: 2.538
batch start
#iterations: 98
currently lose_sum: 97.89361506700516
time_elpased: 2.454
batch start
#iterations: 99
currently lose_sum: 97.83375304937363
time_elpased: 2.438
start validation test
0.626958762887
0.642585988727
0.57492795389
0.606877070998
0.627044728701
63.752
batch start
#iterations: 100
currently lose_sum: 97.78185254335403
time_elpased: 2.655
batch start
#iterations: 101
currently lose_sum: 97.74075919389725
time_elpased: 2.552
batch start
#iterations: 102
currently lose_sum: 97.92019087076187
time_elpased: 2.606
batch start
#iterations: 103
currently lose_sum: 98.06102681159973
time_elpased: 2.627
batch start
#iterations: 104
currently lose_sum: 97.79786115884781
time_elpased: 2.558
batch start
#iterations: 105
currently lose_sum: 97.8249740600586
time_elpased: 2.58
batch start
#iterations: 106
currently lose_sum: 97.70496219396591
time_elpased: 2.581
batch start
#iterations: 107
currently lose_sum: 98.08600288629532
time_elpased: 2.751
batch start
#iterations: 108
currently lose_sum: 97.74782830476761
time_elpased: 2.759
batch start
#iterations: 109
currently lose_sum: 97.59335881471634
time_elpased: 2.68
batch start
#iterations: 110
currently lose_sum: 97.7157940864563
time_elpased: 2.764
batch start
#iterations: 111
currently lose_sum: 97.89000099897385
time_elpased: 2.728
batch start
#iterations: 112
currently lose_sum: 97.68933033943176
time_elpased: 2.843
batch start
#iterations: 113
currently lose_sum: 98.08085280656815
time_elpased: 2.924
batch start
#iterations: 114
currently lose_sum: 97.95452231168747
time_elpased: 2.812
batch start
#iterations: 115
currently lose_sum: 97.55091899633408
time_elpased: 2.816
batch start
#iterations: 116
currently lose_sum: 98.04134130477905
time_elpased: 2.916
batch start
#iterations: 117
currently lose_sum: 97.47264093160629
time_elpased: 3.0
batch start
#iterations: 118
currently lose_sum: 98.14069002866745
time_elpased: 2.739
batch start
#iterations: 119
currently lose_sum: 97.74569875001907
time_elpased: 2.511
start validation test
0.631443298969
0.634373690825
0.623404693289
0.628841362126
0.631456580432
63.572
batch start
#iterations: 120
currently lose_sum: 97.48795783519745
time_elpased: 2.552
batch start
#iterations: 121
currently lose_sum: 97.87965703010559
time_elpased: 2.577
batch start
#iterations: 122
currently lose_sum: 97.72068703174591
time_elpased: 2.617
batch start
#iterations: 123
currently lose_sum: 97.6703292131424
time_elpased: 2.631
batch start
#iterations: 124
currently lose_sum: 97.8198589682579
time_elpased: 2.616
batch start
#iterations: 125
currently lose_sum: 97.84211230278015
time_elpased: 2.627
batch start
#iterations: 126
currently lose_sum: 97.67946457862854
time_elpased: 2.714
batch start
#iterations: 127
currently lose_sum: 98.04699343442917
time_elpased: 2.71
batch start
#iterations: 128
currently lose_sum: 97.26224863529205
time_elpased: 2.766
batch start
#iterations: 129
currently lose_sum: 97.65796446800232
time_elpased: 2.753
batch start
#iterations: 130
currently lose_sum: 98.04715621471405
time_elpased: 2.822
batch start
#iterations: 131
currently lose_sum: 97.535504758358
time_elpased: 2.814
batch start
#iterations: 132
currently lose_sum: 97.65186214447021
time_elpased: 2.706
batch start
#iterations: 133
currently lose_sum: 97.93326395750046
time_elpased: 2.683
batch start
#iterations: 134
currently lose_sum: 97.61681813001633
time_elpased: 2.65
batch start
#iterations: 135
currently lose_sum: 97.68504023551941
time_elpased: 2.718
batch start
#iterations: 136
currently lose_sum: 97.7692596912384
time_elpased: 2.729
batch start
#iterations: 137
currently lose_sum: 97.62694323062897
time_elpased: 2.742
batch start
#iterations: 138
currently lose_sum: 97.81816899776459
time_elpased: 2.623
batch start
#iterations: 139
currently lose_sum: 97.70143580436707
time_elpased: 2.626
start validation test
0.630773195876
0.626549023496
0.650473445862
0.638287128213
0.63074064693
63.501
batch start
#iterations: 140
currently lose_sum: 97.2877745628357
time_elpased: 2.694
batch start
#iterations: 141
currently lose_sum: 97.58304023742676
time_elpased: 2.769
batch start
#iterations: 142
currently lose_sum: 97.73548430204391
time_elpased: 2.733
batch start
#iterations: 143
currently lose_sum: 97.38507145643234
time_elpased: 2.715
batch start
#iterations: 144
currently lose_sum: 97.87002086639404
time_elpased: 2.676
batch start
#iterations: 145
currently lose_sum: 97.8134908080101
time_elpased: 2.674
batch start
#iterations: 146
currently lose_sum: 97.51113045215607
time_elpased: 2.703
batch start
#iterations: 147
currently lose_sum: 97.82043212652206
time_elpased: 2.713
batch start
#iterations: 148
currently lose_sum: 97.76592081785202
time_elpased: 2.793
batch start
#iterations: 149
currently lose_sum: 97.61477053165436
time_elpased: 2.786
batch start
#iterations: 150
currently lose_sum: 97.50519585609436
time_elpased: 2.779
batch start
#iterations: 151
currently lose_sum: 98.00637918710709
time_elpased: 2.769
batch start
#iterations: 152
currently lose_sum: 97.64024466276169
time_elpased: 2.741
batch start
#iterations: 153
currently lose_sum: 97.65414971113205
time_elpased: 2.794
batch start
#iterations: 154
currently lose_sum: 97.69484436511993
time_elpased: 2.761
batch start
#iterations: 155
currently lose_sum: 97.49153792858124
time_elpased: 2.771
batch start
#iterations: 156
currently lose_sum: 97.72213131189346
time_elpased: 2.714
batch start
#iterations: 157
currently lose_sum: 97.69427788257599
time_elpased: 2.758
batch start
#iterations: 158
currently lose_sum: 97.5779727101326
time_elpased: 2.774
batch start
#iterations: 159
currently lose_sum: 97.78092241287231
time_elpased: 2.765
start validation test
0.632371134021
0.640404259943
0.606525319061
0.623004545935
0.63241383673
63.570
batch start
#iterations: 160
currently lose_sum: 97.60183596611023
time_elpased: 2.843
batch start
#iterations: 161
currently lose_sum: 97.58003622293472
time_elpased: 2.765
batch start
#iterations: 162
currently lose_sum: 97.51541316509247
time_elpased: 2.81
batch start
#iterations: 163
currently lose_sum: 97.58181017637253
time_elpased: 2.394
batch start
#iterations: 164
currently lose_sum: 97.67269444465637
time_elpased: 2.302
batch start
#iterations: 165
currently lose_sum: 97.64690744876862
time_elpased: 2.669
batch start
#iterations: 166
currently lose_sum: 97.59745794534683
time_elpased: 2.703
batch start
#iterations: 167
currently lose_sum: 97.6511659026146
time_elpased: 2.728
batch start
#iterations: 168
currently lose_sum: 97.8084009885788
time_elpased: 2.723
batch start
#iterations: 169
currently lose_sum: 97.51684784889221
time_elpased: 2.654
batch start
#iterations: 170
currently lose_sum: 97.21361607313156
time_elpased: 2.765
batch start
#iterations: 171
currently lose_sum: 97.51546543836594
time_elpased: 2.742
batch start
#iterations: 172
currently lose_sum: 97.48918008804321
time_elpased: 2.552
batch start
#iterations: 173
currently lose_sum: 97.47019612789154
time_elpased: 2.245
batch start
#iterations: 174
currently lose_sum: 97.6546089053154
time_elpased: 2.236
batch start
#iterations: 175
currently lose_sum: 97.64025872945786
time_elpased: 2.218
batch start
#iterations: 176
currently lose_sum: 97.40459597110748
time_elpased: 2.115
batch start
#iterations: 177
currently lose_sum: 97.76626884937286
time_elpased: 2.21
batch start
#iterations: 178
currently lose_sum: 97.62885415554047
time_elpased: 2.128
batch start
#iterations: 179
currently lose_sum: 97.57022655010223
time_elpased: 2.122
start validation test
0.627422680412
0.642693278275
0.576677645121
0.607898448519
0.627506521859
63.686
batch start
#iterations: 180
currently lose_sum: 97.40645468235016
time_elpased: 2.113
batch start
#iterations: 181
currently lose_sum: 97.65567004680634
time_elpased: 2.143
batch start
#iterations: 182
currently lose_sum: 97.70173037052155
time_elpased: 2.159
batch start
#iterations: 183
currently lose_sum: 97.39097464084625
time_elpased: 2.125
batch start
#iterations: 184
currently lose_sum: 97.34415227174759
time_elpased: 2.141
batch start
#iterations: 185
currently lose_sum: 97.25785720348358
time_elpased: 2.205
batch start
#iterations: 186
currently lose_sum: 97.63748854398727
time_elpased: 2.183
batch start
#iterations: 187
currently lose_sum: 97.49882048368454
time_elpased: 2.188
batch start
#iterations: 188
currently lose_sum: 97.79707086086273
time_elpased: 2.154
batch start
#iterations: 189
currently lose_sum: 97.56747567653656
time_elpased: 2.144
batch start
#iterations: 190
currently lose_sum: 97.71738821268082
time_elpased: 2.135
batch start
#iterations: 191
currently lose_sum: 97.45265632867813
time_elpased: 2.166
batch start
#iterations: 192
currently lose_sum: 97.55878645181656
time_elpased: 2.147
batch start
#iterations: 193
currently lose_sum: 97.26011848449707
time_elpased: 2.154
batch start
#iterations: 194
currently lose_sum: 97.3823053240776
time_elpased: 2.238
batch start
#iterations: 195
currently lose_sum: 97.26645815372467
time_elpased: 2.148
batch start
#iterations: 196
currently lose_sum: 97.38707530498505
time_elpased: 2.119
batch start
#iterations: 197
currently lose_sum: 97.3456661105156
time_elpased: 2.251
batch start
#iterations: 198
currently lose_sum: 97.57223641872406
time_elpased: 2.2
batch start
#iterations: 199
currently lose_sum: 97.4494423866272
time_elpased: 2.195
start validation test
0.634742268041
0.633611054664
0.641827912721
0.637693015646
0.634730561069
63.347
batch start
#iterations: 200
currently lose_sum: 97.5296540260315
time_elpased: 2.175
batch start
#iterations: 201
currently lose_sum: 97.13620489835739
time_elpased: 2.196
batch start
#iterations: 202
currently lose_sum: 97.75708651542664
time_elpased: 2.139
batch start
#iterations: 203
currently lose_sum: 97.62597393989563
time_elpased: 2.158
batch start
#iterations: 204
currently lose_sum: 97.42732799053192
time_elpased: 2.188
batch start
#iterations: 205
currently lose_sum: 97.62197363376617
time_elpased: 2.183
batch start
#iterations: 206
currently lose_sum: 97.53051775693893
time_elpased: 2.236
batch start
#iterations: 207
currently lose_sum: 97.30707597732544
time_elpased: 2.222
batch start
#iterations: 208
currently lose_sum: 97.21567326784134
time_elpased: 2.248
batch start
#iterations: 209
currently lose_sum: 97.26826441287994
time_elpased: 2.228
batch start
#iterations: 210
currently lose_sum: 97.65038549900055
time_elpased: 2.184
batch start
#iterations: 211
currently lose_sum: 97.30457431077957
time_elpased: 2.189
batch start
#iterations: 212
currently lose_sum: 97.62161982059479
time_elpased: 2.196
batch start
#iterations: 213
currently lose_sum: 97.37298440933228
time_elpased: 2.198
batch start
#iterations: 214
currently lose_sum: 97.4685930609703
time_elpased: 2.283
batch start
#iterations: 215
currently lose_sum: 97.70702004432678
time_elpased: 2.227
batch start
#iterations: 216
currently lose_sum: 97.23424518108368
time_elpased: 2.158
batch start
#iterations: 217
currently lose_sum: 97.36478191614151
time_elpased: 2.189
batch start
#iterations: 218
currently lose_sum: 97.52094340324402
time_elpased: 2.288
batch start
#iterations: 219
currently lose_sum: 97.34826749563217
time_elpased: 2.261
start validation test
0.619536082474
0.647355799571
0.527892136682
0.581552242191
0.619687497502
63.799
batch start
#iterations: 220
currently lose_sum: 97.41571831703186
time_elpased: 2.219
batch start
#iterations: 221
currently lose_sum: 97.51379418373108
time_elpased: 2.187
batch start
#iterations: 222
currently lose_sum: 97.11882084608078
time_elpased: 2.157
batch start
#iterations: 223
currently lose_sum: 97.2002118229866
time_elpased: 2.24
batch start
#iterations: 224
currently lose_sum: 97.3771151304245
time_elpased: 2.237
batch start
#iterations: 225
currently lose_sum: 97.39273542165756
time_elpased: 2.273
batch start
#iterations: 226
currently lose_sum: 97.25429552793503
time_elpased: 2.24
batch start
#iterations: 227
currently lose_sum: 97.49746257066727
time_elpased: 2.232
batch start
#iterations: 228
currently lose_sum: 97.57815092802048
time_elpased: 2.264
batch start
#iterations: 229
currently lose_sum: 97.36905252933502
time_elpased: 2.255
batch start
#iterations: 230
currently lose_sum: 97.55036008358002
time_elpased: 2.229
batch start
#iterations: 231
currently lose_sum: 97.35175454616547
time_elpased: 2.276
batch start
#iterations: 232
currently lose_sum: 97.61315870285034
time_elpased: 2.23
batch start
#iterations: 233
currently lose_sum: 97.48856991529465
time_elpased: 2.232
batch start
#iterations: 234
currently lose_sum: 97.48315250873566
time_elpased: 2.206
batch start
#iterations: 235
currently lose_sum: 97.52142423391342
time_elpased: 2.169
batch start
#iterations: 236
currently lose_sum: 97.45326805114746
time_elpased: 2.23
batch start
#iterations: 237
currently lose_sum: 97.19180220365524
time_elpased: 2.201
batch start
#iterations: 238
currently lose_sum: 97.14134472608566
time_elpased: 2.216
batch start
#iterations: 239
currently lose_sum: 97.40750896930695
time_elpased: 2.212
start validation test
0.632989690722
0.638202725724
0.616920543434
0.627381201591
0.633016240325
63.514
batch start
#iterations: 240
currently lose_sum: 97.48388504981995
time_elpased: 2.195
batch start
#iterations: 241
currently lose_sum: 97.37425518035889
time_elpased: 2.202
batch start
#iterations: 242
currently lose_sum: 97.42478972673416
time_elpased: 2.176
batch start
#iterations: 243
currently lose_sum: 97.71919059753418
time_elpased: 2.219
batch start
#iterations: 244
currently lose_sum: 97.20871812105179
time_elpased: 2.25
batch start
#iterations: 245
currently lose_sum: 97.66603416204453
time_elpased: 2.213
batch start
#iterations: 246
currently lose_sum: 97.64207583665848
time_elpased: 2.202
batch start
#iterations: 247
currently lose_sum: 97.32450526952744
time_elpased: 2.239
batch start
#iterations: 248
currently lose_sum: 97.57213747501373
time_elpased: 2.226
batch start
#iterations: 249
currently lose_sum: 97.89527583122253
time_elpased: 2.206
batch start
#iterations: 250
currently lose_sum: 97.44313377141953
time_elpased: 2.23
batch start
#iterations: 251
currently lose_sum: 97.52382522821426
time_elpased: 2.22
batch start
#iterations: 252
currently lose_sum: 97.1721755862236
time_elpased: 2.22
batch start
#iterations: 253
currently lose_sum: 97.4537456035614
time_elpased: 2.25
batch start
#iterations: 254
currently lose_sum: 97.41853868961334
time_elpased: 2.213
batch start
#iterations: 255
currently lose_sum: 97.17358666658401
time_elpased: 2.163
batch start
#iterations: 256
currently lose_sum: 97.0795401930809
time_elpased: 2.229
batch start
#iterations: 257
currently lose_sum: 97.63830643892288
time_elpased: 2.229
batch start
#iterations: 258
currently lose_sum: 97.33233040571213
time_elpased: 2.172
batch start
#iterations: 259
currently lose_sum: 97.31676030158997
time_elpased: 2.184
start validation test
0.633453608247
0.639260130439
0.61537669823
0.627091090251
0.633483475096
63.637
batch start
#iterations: 260
currently lose_sum: 97.34994524717331
time_elpased: 2.181
batch start
#iterations: 261
currently lose_sum: 97.32631021738052
time_elpased: 2.242
batch start
#iterations: 262
currently lose_sum: 97.44189840555191
time_elpased: 2.262
batch start
#iterations: 263
currently lose_sum: 97.32942134141922
time_elpased: 2.288
batch start
#iterations: 264
currently lose_sum: 97.47167003154755
time_elpased: 2.191
batch start
#iterations: 265
currently lose_sum: 97.34093654155731
time_elpased: 2.292
batch start
#iterations: 266
currently lose_sum: 97.38771122694016
time_elpased: 2.208
batch start
#iterations: 267
currently lose_sum: 97.40928900241852
time_elpased: 2.279
batch start
#iterations: 268
currently lose_sum: 97.36652183532715
time_elpased: 2.231
batch start
#iterations: 269
currently lose_sum: 97.61032122373581
time_elpased: 2.275
batch start
#iterations: 270
currently lose_sum: 97.09487223625183
time_elpased: 2.203
batch start
#iterations: 271
currently lose_sum: 97.27073580026627
time_elpased: 2.227
batch start
#iterations: 272
currently lose_sum: 97.27309852838516
time_elpased: 2.235
batch start
#iterations: 273
currently lose_sum: 97.63520658016205
time_elpased: 2.211
batch start
#iterations: 274
currently lose_sum: 97.41631853580475
time_elpased: 2.197
batch start
#iterations: 275
currently lose_sum: 97.4574025273323
time_elpased: 2.274
batch start
#iterations: 276
currently lose_sum: 97.28027331829071
time_elpased: 2.205
batch start
#iterations: 277
currently lose_sum: 97.34499424695969
time_elpased: 2.188
batch start
#iterations: 278
currently lose_sum: 97.43667548894882
time_elpased: 2.183
batch start
#iterations: 279
currently lose_sum: 97.47990417480469
time_elpased: 2.111
start validation test
0.63087628866
0.640554516448
0.599217785097
0.619197022069
0.630928595151
63.571
batch start
#iterations: 280
currently lose_sum: 97.61570346355438
time_elpased: 2.198
batch start
#iterations: 281
currently lose_sum: 97.2463396191597
time_elpased: 2.229
batch start
#iterations: 282
currently lose_sum: 97.20911169052124
time_elpased: 2.242
batch start
#iterations: 283
currently lose_sum: 97.3599066734314
time_elpased: 2.22
batch start
#iterations: 284
currently lose_sum: 97.37602162361145
time_elpased: 2.285
batch start
#iterations: 285
currently lose_sum: 97.1909692287445
time_elpased: 2.248
batch start
#iterations: 286
currently lose_sum: 97.24575084447861
time_elpased: 2.212
batch start
#iterations: 287
currently lose_sum: 97.38751220703125
time_elpased: 2.209
batch start
#iterations: 288
currently lose_sum: 97.50577008724213
time_elpased: 2.163
batch start
#iterations: 289
currently lose_sum: 97.48226696252823
time_elpased: 2.187
batch start
#iterations: 290
currently lose_sum: 97.27274966239929
time_elpased: 2.224
batch start
#iterations: 291
currently lose_sum: 97.21976864337921
time_elpased: 2.204
batch start
#iterations: 292
currently lose_sum: 97.01436495780945
time_elpased: 2.162
batch start
#iterations: 293
currently lose_sum: 97.41366517543793
time_elpased: 2.306
batch start
#iterations: 294
currently lose_sum: 97.47260445356369
time_elpased: 2.282
batch start
#iterations: 295
currently lose_sum: 97.18691110610962
time_elpased: 2.242
batch start
#iterations: 296
currently lose_sum: 97.25626683235168
time_elpased: 2.154
batch start
#iterations: 297
currently lose_sum: 97.490143597126
time_elpased: 2.192
batch start
#iterations: 298
currently lose_sum: 97.27163529396057
time_elpased: 2.213
batch start
#iterations: 299
currently lose_sum: 97.35198706388474
time_elpased: 2.212
start validation test
0.634536082474
0.635165740169
0.635034993825
0.635100360268
0.634535258168
63.429
batch start
#iterations: 300
currently lose_sum: 97.32984656095505
time_elpased: 2.293
batch start
#iterations: 301
currently lose_sum: 97.19952845573425
time_elpased: 2.295
batch start
#iterations: 302
currently lose_sum: 97.47877103090286
time_elpased: 2.231
batch start
#iterations: 303
currently lose_sum: 97.09090274572372
time_elpased: 2.189
batch start
#iterations: 304
currently lose_sum: 97.00388294458389
time_elpased: 2.222
batch start
#iterations: 305
currently lose_sum: 97.22148978710175
time_elpased: 2.33
batch start
#iterations: 306
currently lose_sum: 97.19594645500183
time_elpased: 2.268
batch start
#iterations: 307
currently lose_sum: 97.22273248434067
time_elpased: 2.284
batch start
#iterations: 308
currently lose_sum: 97.04145961999893
time_elpased: 2.286
batch start
#iterations: 309
currently lose_sum: 97.2653107047081
time_elpased: 2.24
batch start
#iterations: 310
currently lose_sum: 97.44838285446167
time_elpased: 2.197
batch start
#iterations: 311
currently lose_sum: 97.15448951721191
time_elpased: 2.257
batch start
#iterations: 312
currently lose_sum: 97.33488166332245
time_elpased: 2.278
batch start
#iterations: 313
currently lose_sum: 97.247587621212
time_elpased: 2.269
batch start
#iterations: 314
currently lose_sum: 97.40246194601059
time_elpased: 2.281
batch start
#iterations: 315
currently lose_sum: 97.042595744133
time_elpased: 2.262
batch start
#iterations: 316
currently lose_sum: 97.31544333696365
time_elpased: 2.19
batch start
#iterations: 317
currently lose_sum: 97.27866405248642
time_elpased: 2.207
batch start
#iterations: 318
currently lose_sum: 96.90360987186432
time_elpased: 2.225
batch start
#iterations: 319
currently lose_sum: 97.43843758106232
time_elpased: 2.3
start validation test
0.63175257732
0.638936905791
0.608686702347
0.623445076956
0.631790686985
63.469
batch start
#iterations: 320
currently lose_sum: 97.43339818716049
time_elpased: 2.259
batch start
#iterations: 321
currently lose_sum: 97.48748433589935
time_elpased: 2.224
batch start
#iterations: 322
currently lose_sum: 97.21423381567001
time_elpased: 2.209
batch start
#iterations: 323
currently lose_sum: 97.34795469045639
time_elpased: 2.277
batch start
#iterations: 324
currently lose_sum: 97.61154413223267
time_elpased: 2.247
batch start
#iterations: 325
currently lose_sum: 97.20246541500092
time_elpased: 2.323
batch start
#iterations: 326
currently lose_sum: 97.200159907341
time_elpased: 2.239
batch start
#iterations: 327
currently lose_sum: 97.0382336974144
time_elpased: 2.203
batch start
#iterations: 328
currently lose_sum: 97.0892550945282
time_elpased: 2.148
batch start
#iterations: 329
currently lose_sum: 97.44427651166916
time_elpased: 2.238
batch start
#iterations: 330
currently lose_sum: 97.38574808835983
time_elpased: 2.114
batch start
#iterations: 331
currently lose_sum: 97.4173880815506
time_elpased: 2.157
batch start
#iterations: 332
currently lose_sum: 97.02328813076019
time_elpased: 2.168
batch start
#iterations: 333
currently lose_sum: 97.44186598062515
time_elpased: 2.145
batch start
#iterations: 334
currently lose_sum: 97.27663320302963
time_elpased: 2.124
batch start
#iterations: 335
currently lose_sum: 97.46135014295578
time_elpased: 2.217
batch start
#iterations: 336
currently lose_sum: 97.30770528316498
time_elpased: 2.181
batch start
#iterations: 337
currently lose_sum: 97.50497663021088
time_elpased: 2.194
batch start
#iterations: 338
currently lose_sum: 97.04869091510773
time_elpased: 2.214
batch start
#iterations: 339
currently lose_sum: 97.07484763860703
time_elpased: 2.202
start validation test
0.634072164948
0.632613763049
0.642445450803
0.637491701986
0.634058330523
63.483
batch start
#iterations: 340
currently lose_sum: 97.27488321065903
time_elpased: 2.298
batch start
#iterations: 341
currently lose_sum: 97.07455945014954
time_elpased: 2.284
batch start
#iterations: 342
currently lose_sum: 97.35844218730927
time_elpased: 2.319
batch start
#iterations: 343
currently lose_sum: 97.4714726805687
time_elpased: 2.257
batch start
#iterations: 344
currently lose_sum: 97.12488031387329
time_elpased: 2.147
batch start
#iterations: 345
currently lose_sum: 97.47239822149277
time_elpased: 2.231
batch start
#iterations: 346
currently lose_sum: 97.45347142219543
time_elpased: 2.263
batch start
#iterations: 347
currently lose_sum: 97.24284362792969
time_elpased: 2.255
batch start
#iterations: 348
currently lose_sum: 97.21895664930344
time_elpased: 2.247
batch start
#iterations: 349
currently lose_sum: 97.1737430691719
time_elpased: 2.245
batch start
#iterations: 350
currently lose_sum: 97.48153257369995
time_elpased: 2.247
batch start
#iterations: 351
currently lose_sum: 97.54656565189362
time_elpased: 2.353
batch start
#iterations: 352
currently lose_sum: 97.08825677633286
time_elpased: 2.097
batch start
#iterations: 353
currently lose_sum: 97.53408247232437
time_elpased: 2.289
batch start
#iterations: 354
currently lose_sum: 97.33366537094116
time_elpased: 2.246
batch start
#iterations: 355
currently lose_sum: 97.32691097259521
time_elpased: 2.234
batch start
#iterations: 356
currently lose_sum: 97.15360671281815
time_elpased: 2.283
batch start
#iterations: 357
currently lose_sum: 97.45963627099991
time_elpased: 2.278
batch start
#iterations: 358
currently lose_sum: 97.32251328229904
time_elpased: 2.23
batch start
#iterations: 359
currently lose_sum: 96.90918916463852
time_elpased: 2.262
start validation test
0.63118556701
0.631104740453
0.634417455743
0.632756762306
0.631180227252
63.392
batch start
#iterations: 360
currently lose_sum: 97.36521810293198
time_elpased: 2.342
batch start
#iterations: 361
currently lose_sum: 97.252281665802
time_elpased: 2.263
batch start
#iterations: 362
currently lose_sum: 97.30916666984558
time_elpased: 2.296
batch start
#iterations: 363
currently lose_sum: 97.10245257616043
time_elpased: 2.274
batch start
#iterations: 364
currently lose_sum: 97.33089208602905
time_elpased: 2.209
batch start
#iterations: 365
currently lose_sum: 97.37531507015228
time_elpased: 2.225
batch start
#iterations: 366
currently lose_sum: 97.38910615444183
time_elpased: 2.323
batch start
#iterations: 367
currently lose_sum: 97.27600735425949
time_elpased: 2.217
batch start
#iterations: 368
currently lose_sum: 97.0580632686615
time_elpased: 2.182
batch start
#iterations: 369
currently lose_sum: 97.3002672791481
time_elpased: 2.259
batch start
#iterations: 370
currently lose_sum: 97.26322358846664
time_elpased: 2.194
batch start
#iterations: 371
currently lose_sum: 97.13636040687561
time_elpased: 2.154
batch start
#iterations: 372
currently lose_sum: 97.11147946119308
time_elpased: 2.337
batch start
#iterations: 373
currently lose_sum: 97.13728016614914
time_elpased: 2.197
batch start
#iterations: 374
currently lose_sum: 97.0554484128952
time_elpased: 2.229
batch start
#iterations: 375
currently lose_sum: 97.35695332288742
time_elpased: 2.254
batch start
#iterations: 376
currently lose_sum: 97.11540311574936
time_elpased: 2.22
batch start
#iterations: 377
currently lose_sum: 97.10256105661392
time_elpased: 2.238
batch start
#iterations: 378
currently lose_sum: 97.33874523639679
time_elpased: 2.168
batch start
#iterations: 379
currently lose_sum: 97.55385667085648
time_elpased: 2.225
start validation test
0.632835051546
0.633097217945
0.634726224784
0.633910674821
0.632831926931
63.587
batch start
#iterations: 380
currently lose_sum: 97.23065942525864
time_elpased: 2.262
batch start
#iterations: 381
currently lose_sum: 96.83901852369308
time_elpased: 2.276
batch start
#iterations: 382
currently lose_sum: 97.47601240873337
time_elpased: 2.297
batch start
#iterations: 383
currently lose_sum: 97.36939233541489
time_elpased: 2.267
batch start
#iterations: 384
currently lose_sum: 97.1683087348938
time_elpased: 2.139
batch start
#iterations: 385
currently lose_sum: 97.10741990804672
time_elpased: 2.184
batch start
#iterations: 386
currently lose_sum: 97.09063047170639
time_elpased: 2.239
batch start
#iterations: 387
currently lose_sum: 97.5421895980835
time_elpased: 2.387
batch start
#iterations: 388
currently lose_sum: 97.352958381176
time_elpased: 2.342
batch start
#iterations: 389
currently lose_sum: 97.15335416793823
time_elpased: 2.551
batch start
#iterations: 390
currently lose_sum: 97.33660566806793
time_elpased: 2.76
batch start
#iterations: 391
currently lose_sum: 96.86173921823502
time_elpased: 2.654
batch start
#iterations: 392
currently lose_sum: 97.04405903816223
time_elpased: 2.452
batch start
#iterations: 393
currently lose_sum: 97.06493419408798
time_elpased: 2.463
batch start
#iterations: 394
currently lose_sum: 97.02865833044052
time_elpased: 2.463
batch start
#iterations: 395
currently lose_sum: 97.18306410312653
time_elpased: 2.482
batch start
#iterations: 396
currently lose_sum: 97.15823715925217
time_elpased: 2.298
batch start
#iterations: 397
currently lose_sum: 97.27139073610306
time_elpased: 2.444
batch start
#iterations: 398
currently lose_sum: 96.98048138618469
time_elpased: 2.386
batch start
#iterations: 399
currently lose_sum: 97.31373733282089
time_elpased: 2.31
start validation test
0.632525773196
0.633226902874
0.632770687526
0.632998712999
0.632525368546
63.515
acc: 0.633
pre: 0.633
rec: 0.638
F1: 0.636
auc: 0.633
