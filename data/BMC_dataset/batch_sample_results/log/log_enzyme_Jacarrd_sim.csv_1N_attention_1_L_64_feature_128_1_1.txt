start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.73015636205673
time_elpased: 2.034
batch start
#iterations: 1
currently lose_sum: 100.39486640691757
time_elpased: 1.917
batch start
#iterations: 2
currently lose_sum: 100.0723614692688
time_elpased: 1.915
batch start
#iterations: 3
currently lose_sum: 99.9940214753151
time_elpased: 1.929
batch start
#iterations: 4
currently lose_sum: 99.64198476076126
time_elpased: 1.907
batch start
#iterations: 5
currently lose_sum: 99.60709542036057
time_elpased: 1.923
batch start
#iterations: 6
currently lose_sum: 99.67376655340195
time_elpased: 1.913
batch start
#iterations: 7
currently lose_sum: 99.50097078084946
time_elpased: 1.901
batch start
#iterations: 8
currently lose_sum: 99.16865581274033
time_elpased: 1.918
batch start
#iterations: 9
currently lose_sum: 99.28486263751984
time_elpased: 1.917
batch start
#iterations: 10
currently lose_sum: 99.16797298192978
time_elpased: 1.93
batch start
#iterations: 11
currently lose_sum: 99.22439527511597
time_elpased: 1.928
batch start
#iterations: 12
currently lose_sum: 99.22427624464035
time_elpased: 1.919
batch start
#iterations: 13
currently lose_sum: 98.76138043403625
time_elpased: 1.894
batch start
#iterations: 14
currently lose_sum: 98.86362677812576
time_elpased: 1.899
batch start
#iterations: 15
currently lose_sum: 98.87461203336716
time_elpased: 1.913
batch start
#iterations: 16
currently lose_sum: 98.78196513652802
time_elpased: 1.923
batch start
#iterations: 17
currently lose_sum: 98.88163697719574
time_elpased: 1.921
batch start
#iterations: 18
currently lose_sum: 98.26785212755203
time_elpased: 1.931
batch start
#iterations: 19
currently lose_sum: 98.7298355102539
time_elpased: 1.923
start validation test
0.613298969072
0.6139051733
0.614284244108
0.614094650206
0.61329723927
64.756
batch start
#iterations: 20
currently lose_sum: 98.5819571018219
time_elpased: 1.975
batch start
#iterations: 21
currently lose_sum: 98.45214521884918
time_elpased: 1.882
batch start
#iterations: 22
currently lose_sum: 98.72792625427246
time_elpased: 1.897
batch start
#iterations: 23
currently lose_sum: 98.44081753492355
time_elpased: 1.904
batch start
#iterations: 24
currently lose_sum: 98.48886126279831
time_elpased: 1.885
batch start
#iterations: 25
currently lose_sum: 98.71657013893127
time_elpased: 1.886
batch start
#iterations: 26
currently lose_sum: 98.61766368150711
time_elpased: 1.891
batch start
#iterations: 27
currently lose_sum: 98.3870854973793
time_elpased: 1.908
batch start
#iterations: 28
currently lose_sum: 98.38286405801773
time_elpased: 1.895
batch start
#iterations: 29
currently lose_sum: 98.49062472581863
time_elpased: 1.886
batch start
#iterations: 30
currently lose_sum: 98.3235451579094
time_elpased: 1.895
batch start
#iterations: 31
currently lose_sum: 98.47804909944534
time_elpased: 1.899
batch start
#iterations: 32
currently lose_sum: 98.54922831058502
time_elpased: 1.904
batch start
#iterations: 33
currently lose_sum: 98.52156466245651
time_elpased: 1.901
batch start
#iterations: 34
currently lose_sum: 98.30777674913406
time_elpased: 1.904
batch start
#iterations: 35
currently lose_sum: 98.27743554115295
time_elpased: 1.9
batch start
#iterations: 36
currently lose_sum: 98.12310659885406
time_elpased: 1.903
batch start
#iterations: 37
currently lose_sum: 98.29992258548737
time_elpased: 1.926
batch start
#iterations: 38
currently lose_sum: 98.38829171657562
time_elpased: 1.951
batch start
#iterations: 39
currently lose_sum: 98.16557741165161
time_elpased: 1.92
start validation test
0.616701030928
0.624034801523
0.590511474735
0.606810490694
0.616747010733
64.032
batch start
#iterations: 40
currently lose_sum: 98.08690655231476
time_elpased: 1.948
batch start
#iterations: 41
currently lose_sum: 98.15310829877853
time_elpased: 1.923
batch start
#iterations: 42
currently lose_sum: 98.10137921571732
time_elpased: 1.913
batch start
#iterations: 43
currently lose_sum: 98.13335818052292
time_elpased: 1.912
batch start
#iterations: 44
currently lose_sum: 98.08076411485672
time_elpased: 1.96
batch start
#iterations: 45
currently lose_sum: 98.03996050357819
time_elpased: 1.959
batch start
#iterations: 46
currently lose_sum: 98.26238197088242
time_elpased: 1.925
batch start
#iterations: 47
currently lose_sum: 98.04629218578339
time_elpased: 1.915
batch start
#iterations: 48
currently lose_sum: 98.17675870656967
time_elpased: 1.923
batch start
#iterations: 49
currently lose_sum: 98.15180557966232
time_elpased: 1.928
batch start
#iterations: 50
currently lose_sum: 98.10683649778366
time_elpased: 1.979
batch start
#iterations: 51
currently lose_sum: 98.16010636091232
time_elpased: 1.986
batch start
#iterations: 52
currently lose_sum: 98.33327335119247
time_elpased: 1.925
batch start
#iterations: 53
currently lose_sum: 98.0005596280098
time_elpased: 1.962
batch start
#iterations: 54
currently lose_sum: 98.07379704713821
time_elpased: 1.923
batch start
#iterations: 55
currently lose_sum: 98.28246700763702
time_elpased: 1.936
batch start
#iterations: 56
currently lose_sum: 98.01304072141647
time_elpased: 1.922
batch start
#iterations: 57
currently lose_sum: 98.2888109087944
time_elpased: 1.915
batch start
#iterations: 58
currently lose_sum: 98.06571006774902
time_elpased: 1.933
batch start
#iterations: 59
currently lose_sum: 98.14838302135468
time_elpased: 1.903
start validation test
0.623195876289
0.628702812533
0.605022126171
0.616635200336
0.62322778311
64.105
batch start
#iterations: 60
currently lose_sum: 98.07503092288971
time_elpased: 1.933
batch start
#iterations: 61
currently lose_sum: 98.11232310533524
time_elpased: 1.912
batch start
#iterations: 62
currently lose_sum: 97.97283697128296
time_elpased: 1.905
batch start
#iterations: 63
currently lose_sum: 98.08872485160828
time_elpased: 1.898
batch start
#iterations: 64
currently lose_sum: 98.01194763183594
time_elpased: 1.891
batch start
#iterations: 65
currently lose_sum: 98.0994046330452
time_elpased: 1.925
batch start
#iterations: 66
currently lose_sum: 98.059725522995
time_elpased: 1.921
batch start
#iterations: 67
currently lose_sum: 98.04288512468338
time_elpased: 1.891
batch start
#iterations: 68
currently lose_sum: 97.91791051626205
time_elpased: 1.901
batch start
#iterations: 69
currently lose_sum: 97.88454985618591
time_elpased: 1.941
batch start
#iterations: 70
currently lose_sum: 97.65253251791
time_elpased: 1.889
batch start
#iterations: 71
currently lose_sum: 97.75532793998718
time_elpased: 1.898
batch start
#iterations: 72
currently lose_sum: 97.87674105167389
time_elpased: 1.945
batch start
#iterations: 73
currently lose_sum: 97.53841990232468
time_elpased: 1.907
batch start
#iterations: 74
currently lose_sum: 98.00856912136078
time_elpased: 1.903
batch start
#iterations: 75
currently lose_sum: 98.08264034986496
time_elpased: 1.921
batch start
#iterations: 76
currently lose_sum: 98.00035256147385
time_elpased: 1.92
batch start
#iterations: 77
currently lose_sum: 98.19795978069305
time_elpased: 1.914
batch start
#iterations: 78
currently lose_sum: 97.87712055444717
time_elpased: 1.92
batch start
#iterations: 79
currently lose_sum: 97.79381918907166
time_elpased: 1.925
start validation test
0.621701030928
0.639423076923
0.561181434599
0.597752808989
0.621807282414
63.811
batch start
#iterations: 80
currently lose_sum: 97.68461763858795
time_elpased: 1.979
batch start
#iterations: 81
currently lose_sum: 97.87990540266037
time_elpased: 1.932
batch start
#iterations: 82
currently lose_sum: 97.8070832490921
time_elpased: 1.914
batch start
#iterations: 83
currently lose_sum: 97.86872380971909
time_elpased: 1.935
batch start
#iterations: 84
currently lose_sum: 97.49093616008759
time_elpased: 1.924
batch start
#iterations: 85
currently lose_sum: 97.6145721077919
time_elpased: 1.94
batch start
#iterations: 86
currently lose_sum: 97.90825831890106
time_elpased: 1.909
batch start
#iterations: 87
currently lose_sum: 97.79198032617569
time_elpased: 1.926
batch start
#iterations: 88
currently lose_sum: 97.8413627743721
time_elpased: 1.956
batch start
#iterations: 89
currently lose_sum: 97.29705440998077
time_elpased: 1.917
batch start
#iterations: 90
currently lose_sum: 97.48339629173279
time_elpased: 1.923
batch start
#iterations: 91
currently lose_sum: 97.7686460018158
time_elpased: 1.92
batch start
#iterations: 92
currently lose_sum: 97.62725096940994
time_elpased: 1.911
batch start
#iterations: 93
currently lose_sum: 98.02073460817337
time_elpased: 1.925
batch start
#iterations: 94
currently lose_sum: 97.76351749897003
time_elpased: 1.896
batch start
#iterations: 95
currently lose_sum: 97.73652476072311
time_elpased: 1.913
batch start
#iterations: 96
currently lose_sum: 97.73816913366318
time_elpased: 1.937
batch start
#iterations: 97
currently lose_sum: 97.43765783309937
time_elpased: 1.96
batch start
#iterations: 98
currently lose_sum: 97.61278545856476
time_elpased: 1.912
batch start
#iterations: 99
currently lose_sum: 97.51140719652176
time_elpased: 1.943
start validation test
0.60881443299
0.63187902826
0.524647524956
0.573292100084
0.608962200979
64.231
batch start
#iterations: 100
currently lose_sum: 97.84604632854462
time_elpased: 1.983
batch start
#iterations: 101
currently lose_sum: 97.80810695886612
time_elpased: 1.965
batch start
#iterations: 102
currently lose_sum: 97.48418694734573
time_elpased: 1.919
batch start
#iterations: 103
currently lose_sum: 97.79819679260254
time_elpased: 1.965
batch start
#iterations: 104
currently lose_sum: 97.65953373908997
time_elpased: 1.91
batch start
#iterations: 105
currently lose_sum: 97.58205389976501
time_elpased: 1.92
batch start
#iterations: 106
currently lose_sum: 97.78408205509186
time_elpased: 1.906
batch start
#iterations: 107
currently lose_sum: 97.76181942224503
time_elpased: 1.908
batch start
#iterations: 108
currently lose_sum: 97.32574313879013
time_elpased: 1.964
batch start
#iterations: 109
currently lose_sum: 97.70445418357849
time_elpased: 1.914
batch start
#iterations: 110
currently lose_sum: 97.4339047074318
time_elpased: 1.922
batch start
#iterations: 111
currently lose_sum: 97.56625682115555
time_elpased: 1.941
batch start
#iterations: 112
currently lose_sum: 97.55838960409164
time_elpased: 1.942
batch start
#iterations: 113
currently lose_sum: 97.66851729154587
time_elpased: 1.933
batch start
#iterations: 114
currently lose_sum: 97.38710361719131
time_elpased: 1.965
batch start
#iterations: 115
currently lose_sum: 97.62920826673508
time_elpased: 1.95
batch start
#iterations: 116
currently lose_sum: 97.78330653905869
time_elpased: 2.029
batch start
#iterations: 117
currently lose_sum: 97.8309651017189
time_elpased: 1.923
batch start
#iterations: 118
currently lose_sum: 97.51075118780136
time_elpased: 1.91
batch start
#iterations: 119
currently lose_sum: 97.44485676288605
time_elpased: 1.961
start validation test
0.61618556701
0.644300419367
0.521765977153
0.576595018765
0.616351335164
63.914
batch start
#iterations: 120
currently lose_sum: 97.55362892150879
time_elpased: 2.041
batch start
#iterations: 121
currently lose_sum: 97.47998285293579
time_elpased: 1.966
batch start
#iterations: 122
currently lose_sum: 97.77348840236664
time_elpased: 1.933
batch start
#iterations: 123
currently lose_sum: 97.56231665611267
time_elpased: 1.923
batch start
#iterations: 124
currently lose_sum: 97.61760437488556
time_elpased: 1.912
batch start
#iterations: 125
currently lose_sum: 97.70143443346024
time_elpased: 1.961
batch start
#iterations: 126
currently lose_sum: 97.42654395103455
time_elpased: 1.926
batch start
#iterations: 127
currently lose_sum: 97.4913130402565
time_elpased: 1.954
batch start
#iterations: 128
currently lose_sum: 97.45499163866043
time_elpased: 1.948
batch start
#iterations: 129
currently lose_sum: 97.32761514186859
time_elpased: 1.948
batch start
#iterations: 130
currently lose_sum: 97.63694447278976
time_elpased: 1.941
batch start
#iterations: 131
currently lose_sum: 97.50126141309738
time_elpased: 1.917
batch start
#iterations: 132
currently lose_sum: 97.46902912855148
time_elpased: 1.934
batch start
#iterations: 133
currently lose_sum: 97.11471021175385
time_elpased: 1.915
batch start
#iterations: 134
currently lose_sum: 97.3462198972702
time_elpased: 1.969
batch start
#iterations: 135
currently lose_sum: 97.62692934274673
time_elpased: 1.928
batch start
#iterations: 136
currently lose_sum: 97.66190558671951
time_elpased: 1.937
batch start
#iterations: 137
currently lose_sum: 97.3576180934906
time_elpased: 1.965
batch start
#iterations: 138
currently lose_sum: 97.52301073074341
time_elpased: 1.981
batch start
#iterations: 139
currently lose_sum: 97.45821642875671
time_elpased: 1.948
start validation test
0.619381443299
0.647602176389
0.526705773387
0.580930760499
0.619544149732
63.894
batch start
#iterations: 140
currently lose_sum: 97.58335065841675
time_elpased: 1.957
batch start
#iterations: 141
currently lose_sum: 97.22166174650192
time_elpased: 1.924
batch start
#iterations: 142
currently lose_sum: 97.87025535106659
time_elpased: 1.97
batch start
#iterations: 143
currently lose_sum: 97.23847556114197
time_elpased: 1.966
batch start
#iterations: 144
currently lose_sum: 97.46507400274277
time_elpased: 1.975
batch start
#iterations: 145
currently lose_sum: 97.74516642093658
time_elpased: 1.932
batch start
#iterations: 146
currently lose_sum: 97.35665673017502
time_elpased: 1.923
batch start
#iterations: 147
currently lose_sum: 97.27681201696396
time_elpased: 1.943
batch start
#iterations: 148
currently lose_sum: 97.28677409887314
time_elpased: 1.913
batch start
#iterations: 149
currently lose_sum: 97.53172105550766
time_elpased: 1.94
batch start
#iterations: 150
currently lose_sum: 97.2994612455368
time_elpased: 1.933
batch start
#iterations: 151
currently lose_sum: 97.76125276088715
time_elpased: 1.902
batch start
#iterations: 152
currently lose_sum: 97.24064671993256
time_elpased: 1.935
batch start
#iterations: 153
currently lose_sum: 97.34543037414551
time_elpased: 1.904
batch start
#iterations: 154
currently lose_sum: 97.63251912593842
time_elpased: 1.956
batch start
#iterations: 155
currently lose_sum: 97.37828171253204
time_elpased: 1.934
batch start
#iterations: 156
currently lose_sum: 97.58341526985168
time_elpased: 1.933
batch start
#iterations: 157
currently lose_sum: 97.7498950958252
time_elpased: 1.933
batch start
#iterations: 158
currently lose_sum: 97.73817574977875
time_elpased: 1.936
batch start
#iterations: 159
currently lose_sum: 97.56187653541565
time_elpased: 1.919
start validation test
0.617886597938
0.645859711319
0.524956262221
0.579165483963
0.618049751476
63.982
batch start
#iterations: 160
currently lose_sum: 97.50059819221497
time_elpased: 1.957
batch start
#iterations: 161
currently lose_sum: 97.27863788604736
time_elpased: 1.933
batch start
#iterations: 162
currently lose_sum: 97.42723709344864
time_elpased: 1.941
batch start
#iterations: 163
currently lose_sum: 97.2508435845375
time_elpased: 1.904
batch start
#iterations: 164
currently lose_sum: 97.62297570705414
time_elpased: 1.908
batch start
#iterations: 165
currently lose_sum: 97.41253346204758
time_elpased: 1.926
batch start
#iterations: 166
currently lose_sum: 97.43174076080322
time_elpased: 1.926
batch start
#iterations: 167
currently lose_sum: 97.4659413099289
time_elpased: 1.921
batch start
#iterations: 168
currently lose_sum: 97.21021497249603
time_elpased: 1.923
batch start
#iterations: 169
currently lose_sum: 97.39819943904877
time_elpased: 1.944
batch start
#iterations: 170
currently lose_sum: 97.19422906637192
time_elpased: 1.949
batch start
#iterations: 171
currently lose_sum: 97.470183968544
time_elpased: 1.922
batch start
#iterations: 172
currently lose_sum: 97.64522278308868
time_elpased: 2.08
batch start
#iterations: 173
currently lose_sum: 97.21062344312668
time_elpased: 1.933
batch start
#iterations: 174
currently lose_sum: 97.57625877857208
time_elpased: 1.932
batch start
#iterations: 175
currently lose_sum: 96.97456723451614
time_elpased: 1.964
batch start
#iterations: 176
currently lose_sum: 97.35225927829742
time_elpased: 1.928
batch start
#iterations: 177
currently lose_sum: 97.591950237751
time_elpased: 1.948
batch start
#iterations: 178
currently lose_sum: 97.44395864009857
time_elpased: 1.943
batch start
#iterations: 179
currently lose_sum: 97.49617058038712
time_elpased: 1.924
start validation test
0.629381443299
0.636021100226
0.608006586395
0.621698411028
0.629418970157
63.571
batch start
#iterations: 180
currently lose_sum: 97.22720539569855
time_elpased: 1.96
batch start
#iterations: 181
currently lose_sum: 97.31201362609863
time_elpased: 1.954
batch start
#iterations: 182
currently lose_sum: 97.44705384969711
time_elpased: 1.935
batch start
#iterations: 183
currently lose_sum: 97.14366120100021
time_elpased: 1.996
batch start
#iterations: 184
currently lose_sum: 97.19323796033859
time_elpased: 1.952
batch start
#iterations: 185
currently lose_sum: 97.44669914245605
time_elpased: 1.932
batch start
#iterations: 186
currently lose_sum: 97.12087666988373
time_elpased: 1.952
batch start
#iterations: 187
currently lose_sum: 97.12343448400497
time_elpased: 1.958
batch start
#iterations: 188
currently lose_sum: 97.54096508026123
time_elpased: 1.914
batch start
#iterations: 189
currently lose_sum: 97.28847146034241
time_elpased: 1.978
batch start
#iterations: 190
currently lose_sum: 97.31578832864761
time_elpased: 1.914
batch start
#iterations: 191
currently lose_sum: 97.29138052463531
time_elpased: 1.966
batch start
#iterations: 192
currently lose_sum: 97.32718282938004
time_elpased: 1.947
batch start
#iterations: 193
currently lose_sum: 97.33155220746994
time_elpased: 1.947
batch start
#iterations: 194
currently lose_sum: 97.33815985918045
time_elpased: 1.939
batch start
#iterations: 195
currently lose_sum: 97.25547569990158
time_elpased: 1.928
batch start
#iterations: 196
currently lose_sum: 97.44216054677963
time_elpased: 1.957
batch start
#iterations: 197
currently lose_sum: 97.18966376781464
time_elpased: 1.951
batch start
#iterations: 198
currently lose_sum: 97.17400133609772
time_elpased: 1.924
batch start
#iterations: 199
currently lose_sum: 97.13982993364334
time_elpased: 1.915
start validation test
0.628453608247
0.637389114007
0.5989502933
0.617572156197
0.628505405867
63.483
batch start
#iterations: 200
currently lose_sum: 97.40193998813629
time_elpased: 1.965
batch start
#iterations: 201
currently lose_sum: 97.08558243513107
time_elpased: 1.946
batch start
#iterations: 202
currently lose_sum: 97.00341695547104
time_elpased: 1.946
batch start
#iterations: 203
currently lose_sum: 97.25279438495636
time_elpased: 1.961
batch start
#iterations: 204
currently lose_sum: 97.3147400021553
time_elpased: 1.929
batch start
#iterations: 205
currently lose_sum: 97.28990751504898
time_elpased: 1.941
batch start
#iterations: 206
currently lose_sum: 97.0297502875328
time_elpased: 1.914
batch start
#iterations: 207
currently lose_sum: 97.14753925800323
time_elpased: 1.93
batch start
#iterations: 208
currently lose_sum: 97.14327961206436
time_elpased: 1.916
batch start
#iterations: 209
currently lose_sum: 97.03049165010452
time_elpased: 1.948
batch start
#iterations: 210
currently lose_sum: 96.96727585792542
time_elpased: 1.981
batch start
#iterations: 211
currently lose_sum: 97.04598772525787
time_elpased: 1.981
batch start
#iterations: 212
currently lose_sum: 97.099833548069
time_elpased: 1.957
batch start
#iterations: 213
currently lose_sum: 97.44394612312317
time_elpased: 1.93
batch start
#iterations: 214
currently lose_sum: 97.30305677652359
time_elpased: 1.952
batch start
#iterations: 215
currently lose_sum: 96.97027742862701
time_elpased: 1.911
batch start
#iterations: 216
currently lose_sum: 97.04572403430939
time_elpased: 1.942
batch start
#iterations: 217
currently lose_sum: 97.57403194904327
time_elpased: 1.958
batch start
#iterations: 218
currently lose_sum: 97.17090421915054
time_elpased: 1.942
batch start
#iterations: 219
currently lose_sum: 97.24378800392151
time_elpased: 1.971
start validation test
0.628402061856
0.640362659503
0.588761963569
0.613479170018
0.628471656162
63.473
batch start
#iterations: 220
currently lose_sum: 97.17181205749512
time_elpased: 1.991
batch start
#iterations: 221
currently lose_sum: 97.30651926994324
time_elpased: 1.958
batch start
#iterations: 222
currently lose_sum: 97.19596695899963
time_elpased: 1.959
batch start
#iterations: 223
currently lose_sum: 97.22114366292953
time_elpased: 1.95
batch start
#iterations: 224
currently lose_sum: 96.8760484457016
time_elpased: 1.988
batch start
#iterations: 225
currently lose_sum: 97.12443190813065
time_elpased: 1.961
batch start
#iterations: 226
currently lose_sum: 96.92785775661469
time_elpased: 1.945
batch start
#iterations: 227
currently lose_sum: 97.30620986223221
time_elpased: 1.953
batch start
#iterations: 228
currently lose_sum: 96.9641660451889
time_elpased: 1.934
batch start
#iterations: 229
currently lose_sum: 97.17016625404358
time_elpased: 1.939
batch start
#iterations: 230
currently lose_sum: 97.11820495128632
time_elpased: 1.959
batch start
#iterations: 231
currently lose_sum: 97.31600165367126
time_elpased: 1.967
batch start
#iterations: 232
currently lose_sum: 97.18329983949661
time_elpased: 1.932
batch start
#iterations: 233
currently lose_sum: 96.96363830566406
time_elpased: 1.992
batch start
#iterations: 234
currently lose_sum: 96.90042513608932
time_elpased: 1.948
batch start
#iterations: 235
currently lose_sum: 97.11330932378769
time_elpased: 1.962
batch start
#iterations: 236
currently lose_sum: 97.64122426509857
time_elpased: 1.909
batch start
#iterations: 237
currently lose_sum: 97.16094321012497
time_elpased: 1.956
batch start
#iterations: 238
currently lose_sum: 97.13804495334625
time_elpased: 1.966
batch start
#iterations: 239
currently lose_sum: 97.29241925477982
time_elpased: 1.988
start validation test
0.622577319588
0.63929277655
0.565606668725
0.600196570929
0.622677340352
63.637
batch start
#iterations: 240
currently lose_sum: 96.98571312427521
time_elpased: 1.957
batch start
#iterations: 241
currently lose_sum: 97.2391647696495
time_elpased: 1.972
batch start
#iterations: 242
currently lose_sum: 97.19033509492874
time_elpased: 1.933
batch start
#iterations: 243
currently lose_sum: 97.33642625808716
time_elpased: 1.965
batch start
#iterations: 244
currently lose_sum: 96.99454659223557
time_elpased: 1.966
batch start
#iterations: 245
currently lose_sum: 97.19426959753036
time_elpased: 1.929
batch start
#iterations: 246
currently lose_sum: 97.24164652824402
time_elpased: 1.945
batch start
#iterations: 247
currently lose_sum: 97.18529570102692
time_elpased: 1.962
batch start
#iterations: 248
currently lose_sum: 97.04119330644608
time_elpased: 1.979
batch start
#iterations: 249
currently lose_sum: 97.30529022216797
time_elpased: 1.937
batch start
#iterations: 250
currently lose_sum: 97.20873707532883
time_elpased: 1.986
batch start
#iterations: 251
currently lose_sum: 97.06067144870758
time_elpased: 1.946
batch start
#iterations: 252
currently lose_sum: 97.04355126619339
time_elpased: 1.938
batch start
#iterations: 253
currently lose_sum: 97.40510672330856
time_elpased: 1.955
batch start
#iterations: 254
currently lose_sum: 96.83239990472794
time_elpased: 1.92
batch start
#iterations: 255
currently lose_sum: 97.09580832719803
time_elpased: 1.929
batch start
#iterations: 256
currently lose_sum: 97.00692248344421
time_elpased: 1.942
batch start
#iterations: 257
currently lose_sum: 97.19355154037476
time_elpased: 1.976
batch start
#iterations: 258
currently lose_sum: 97.06078827381134
time_elpased: 1.943
batch start
#iterations: 259
currently lose_sum: 96.79415845870972
time_elpased: 2.001
start validation test
0.629329896907
0.630718277789
0.627148296799
0.62892822127
0.629333727043
63.526
batch start
#iterations: 260
currently lose_sum: 96.57162338495255
time_elpased: 1.953
batch start
#iterations: 261
currently lose_sum: 96.98197054862976
time_elpased: 1.951
batch start
#iterations: 262
currently lose_sum: 97.05689752101898
time_elpased: 1.964
batch start
#iterations: 263
currently lose_sum: 97.2012385725975
time_elpased: 1.964
batch start
#iterations: 264
currently lose_sum: 97.03842508792877
time_elpased: 1.94
batch start
#iterations: 265
currently lose_sum: 97.12952154874802
time_elpased: 1.955
batch start
#iterations: 266
currently lose_sum: 97.13178116083145
time_elpased: 1.939
batch start
#iterations: 267
currently lose_sum: 97.02423512935638
time_elpased: 1.95
batch start
#iterations: 268
currently lose_sum: 97.06695401668549
time_elpased: 1.932
batch start
#iterations: 269
currently lose_sum: 97.43712663650513
time_elpased: 1.917
batch start
#iterations: 270
currently lose_sum: 97.1105005145073
time_elpased: 1.945
batch start
#iterations: 271
currently lose_sum: 96.84430944919586
time_elpased: 1.943
batch start
#iterations: 272
currently lose_sum: 96.92819893360138
time_elpased: 1.932
batch start
#iterations: 273
currently lose_sum: 97.14641159772873
time_elpased: 1.93
batch start
#iterations: 274
currently lose_sum: 97.0153266787529
time_elpased: 1.937
batch start
#iterations: 275
currently lose_sum: 97.36790335178375
time_elpased: 1.957
batch start
#iterations: 276
currently lose_sum: 96.93130522966385
time_elpased: 1.947
batch start
#iterations: 277
currently lose_sum: 97.08780473470688
time_elpased: 1.949
batch start
#iterations: 278
currently lose_sum: 96.59715974330902
time_elpased: 1.935
batch start
#iterations: 279
currently lose_sum: 97.25017142295837
time_elpased: 1.961
start validation test
0.628505154639
0.635910764566
0.60430173922
0.619703445728
0.628547647468
63.502
batch start
#iterations: 280
currently lose_sum: 96.98204869031906
time_elpased: 2.0
batch start
#iterations: 281
currently lose_sum: 97.11282753944397
time_elpased: 1.962
batch start
#iterations: 282
currently lose_sum: 96.86428618431091
time_elpased: 1.981
batch start
#iterations: 283
currently lose_sum: 97.47181284427643
time_elpased: 1.934
batch start
#iterations: 284
currently lose_sum: 96.9412190914154
time_elpased: 1.934
batch start
#iterations: 285
currently lose_sum: 96.5866402387619
time_elpased: 1.944
batch start
#iterations: 286
currently lose_sum: 96.90431922674179
time_elpased: 1.946
batch start
#iterations: 287
currently lose_sum: 97.04774034023285
time_elpased: 1.925
batch start
#iterations: 288
currently lose_sum: 96.8364018201828
time_elpased: 1.946
batch start
#iterations: 289
currently lose_sum: 97.41310226917267
time_elpased: 1.962
batch start
#iterations: 290
currently lose_sum: 97.31259787082672
time_elpased: 1.968
batch start
#iterations: 291
currently lose_sum: 96.96761667728424
time_elpased: 1.968
batch start
#iterations: 292
currently lose_sum: 97.0031543970108
time_elpased: 1.94
batch start
#iterations: 293
currently lose_sum: 97.00094026327133
time_elpased: 1.925
batch start
#iterations: 294
currently lose_sum: 97.11027979850769
time_elpased: 1.922
batch start
#iterations: 295
currently lose_sum: 97.26784479618073
time_elpased: 1.949
batch start
#iterations: 296
currently lose_sum: 97.08905178308487
time_elpased: 1.964
batch start
#iterations: 297
currently lose_sum: 97.06583696603775
time_elpased: 1.918
batch start
#iterations: 298
currently lose_sum: 96.60681712627411
time_elpased: 1.944
batch start
#iterations: 299
currently lose_sum: 96.9713796377182
time_elpased: 1.987
start validation test
0.62793814433
0.63465890721
0.606051250386
0.620025268478
0.627976570148
63.532
batch start
#iterations: 300
currently lose_sum: 96.95188164710999
time_elpased: 1.944
batch start
#iterations: 301
currently lose_sum: 97.07832425832748
time_elpased: 1.969
batch start
#iterations: 302
currently lose_sum: 97.00455713272095
time_elpased: 1.966
batch start
#iterations: 303
currently lose_sum: 96.99494957923889
time_elpased: 1.941
batch start
#iterations: 304
currently lose_sum: 96.9247899055481
time_elpased: 1.947
batch start
#iterations: 305
currently lose_sum: 96.86519086360931
time_elpased: 1.986
batch start
#iterations: 306
currently lose_sum: 97.35469394922256
time_elpased: 1.969
batch start
#iterations: 307
currently lose_sum: 97.11667191982269
time_elpased: 1.931
batch start
#iterations: 308
currently lose_sum: 96.93735772371292
time_elpased: 1.922
batch start
#iterations: 309
currently lose_sum: 97.1660607457161
time_elpased: 1.925
batch start
#iterations: 310
currently lose_sum: 96.94833207130432
time_elpased: 1.925
batch start
#iterations: 311
currently lose_sum: 97.07121193408966
time_elpased: 1.944
batch start
#iterations: 312
currently lose_sum: 97.0748301744461
time_elpased: 1.959
batch start
#iterations: 313
currently lose_sum: 96.99682635068893
time_elpased: 1.945
batch start
#iterations: 314
currently lose_sum: 97.00863575935364
time_elpased: 1.951
batch start
#iterations: 315
currently lose_sum: 96.9863675236702
time_elpased: 1.97
batch start
#iterations: 316
currently lose_sum: 96.73100513219833
time_elpased: 1.959
batch start
#iterations: 317
currently lose_sum: 97.16699379682541
time_elpased: 1.962
batch start
#iterations: 318
currently lose_sum: 97.12267899513245
time_elpased: 1.949
batch start
#iterations: 319
currently lose_sum: 97.16294652223587
time_elpased: 2.006
start validation test
0.626443298969
0.638081395349
0.587321189668
0.611649965168
0.626511983866
63.619
batch start
#iterations: 320
currently lose_sum: 97.06480598449707
time_elpased: 1.981
batch start
#iterations: 321
currently lose_sum: 97.0317085981369
time_elpased: 1.982
batch start
#iterations: 322
currently lose_sum: 96.97188949584961
time_elpased: 1.953
batch start
#iterations: 323
currently lose_sum: 97.24874502420425
time_elpased: 1.944
batch start
#iterations: 324
currently lose_sum: 96.95590323209763
time_elpased: 1.994
batch start
#iterations: 325
currently lose_sum: 96.93635749816895
time_elpased: 1.931
batch start
#iterations: 326
currently lose_sum: 96.90827411413193
time_elpased: 1.935
batch start
#iterations: 327
currently lose_sum: 96.73376196622849
time_elpased: 1.984
batch start
#iterations: 328
currently lose_sum: 96.97586500644684
time_elpased: 1.91
batch start
#iterations: 329
currently lose_sum: 96.67766535282135
time_elpased: 1.939
batch start
#iterations: 330
currently lose_sum: 96.83357912302017
time_elpased: 1.909
batch start
#iterations: 331
currently lose_sum: 97.05984151363373
time_elpased: 1.959
batch start
#iterations: 332
currently lose_sum: 97.09197390079498
time_elpased: 1.947
batch start
#iterations: 333
currently lose_sum: 96.69596856832504
time_elpased: 1.969
batch start
#iterations: 334
currently lose_sum: 97.02123951911926
time_elpased: 1.94
batch start
#iterations: 335
currently lose_sum: 96.93692076206207
time_elpased: 1.977
batch start
#iterations: 336
currently lose_sum: 96.99329072237015
time_elpased: 1.978
batch start
#iterations: 337
currently lose_sum: 96.88100492954254
time_elpased: 2.0
batch start
#iterations: 338
currently lose_sum: 96.77387738227844
time_elpased: 1.962
batch start
#iterations: 339
currently lose_sum: 96.86247426271439
time_elpased: 1.939
start validation test
0.616597938144
0.644258766933
0.523721313162
0.577770208901
0.616760997385
63.758
batch start
#iterations: 340
currently lose_sum: 97.1325159072876
time_elpased: 1.978
batch start
#iterations: 341
currently lose_sum: 97.11412769556046
time_elpased: 1.998
batch start
#iterations: 342
currently lose_sum: 96.87775194644928
time_elpased: 1.973
batch start
#iterations: 343
currently lose_sum: 97.00716257095337
time_elpased: 1.968
batch start
#iterations: 344
currently lose_sum: 96.82839393615723
time_elpased: 1.967
batch start
#iterations: 345
currently lose_sum: 97.0209134221077
time_elpased: 1.94
batch start
#iterations: 346
currently lose_sum: 97.0371390581131
time_elpased: 1.974
batch start
#iterations: 347
currently lose_sum: 96.93650430440903
time_elpased: 1.941
batch start
#iterations: 348
currently lose_sum: 97.12807828187943
time_elpased: 1.93
batch start
#iterations: 349
currently lose_sum: 96.9405089020729
time_elpased: 1.989
batch start
#iterations: 350
currently lose_sum: 97.18914878368378
time_elpased: 1.974
batch start
#iterations: 351
currently lose_sum: 97.1460165977478
time_elpased: 1.95
batch start
#iterations: 352
currently lose_sum: 97.15265262126923
time_elpased: 2.006
batch start
#iterations: 353
currently lose_sum: 97.49135893583298
time_elpased: 1.934
batch start
#iterations: 354
currently lose_sum: 97.01233923435211
time_elpased: 1.994
batch start
#iterations: 355
currently lose_sum: 97.40973711013794
time_elpased: 1.947
batch start
#iterations: 356
currently lose_sum: 97.14444762468338
time_elpased: 2.006
batch start
#iterations: 357
currently lose_sum: 96.85137343406677
time_elpased: 1.942
batch start
#iterations: 358
currently lose_sum: 97.26144659519196
time_elpased: 1.946
batch start
#iterations: 359
currently lose_sum: 96.94883489608765
time_elpased: 1.955
start validation test
0.627525773196
0.634692332648
0.603993001955
0.618962244252
0.627567088605
63.558
batch start
#iterations: 360
currently lose_sum: 96.66589206457138
time_elpased: 2.003
batch start
#iterations: 361
currently lose_sum: 97.35746294260025
time_elpased: 1.942
batch start
#iterations: 362
currently lose_sum: 96.74073112010956
time_elpased: 1.937
batch start
#iterations: 363
currently lose_sum: 96.9117636680603
time_elpased: 1.968
batch start
#iterations: 364
currently lose_sum: 97.46349674463272
time_elpased: 1.969
batch start
#iterations: 365
currently lose_sum: 96.75769490003586
time_elpased: 1.955
batch start
#iterations: 366
currently lose_sum: 96.67920875549316
time_elpased: 1.994
batch start
#iterations: 367
currently lose_sum: 96.69382429122925
time_elpased: 1.974
batch start
#iterations: 368
currently lose_sum: 97.19176226854324
time_elpased: 2.011
batch start
#iterations: 369
currently lose_sum: 96.83656346797943
time_elpased: 1.981
batch start
#iterations: 370
currently lose_sum: 96.92546486854553
time_elpased: 1.98
batch start
#iterations: 371
currently lose_sum: 97.14211040735245
time_elpased: 2.021
batch start
#iterations: 372
currently lose_sum: 97.07019382715225
time_elpased: 1.932
batch start
#iterations: 373
currently lose_sum: 97.02117270231247
time_elpased: 2.015
batch start
#iterations: 374
currently lose_sum: 96.77229869365692
time_elpased: 1.954
batch start
#iterations: 375
currently lose_sum: 97.14574521780014
time_elpased: 1.959
batch start
#iterations: 376
currently lose_sum: 96.79081803560257
time_elpased: 1.953
batch start
#iterations: 377
currently lose_sum: 96.89988195896149
time_elpased: 2.008
batch start
#iterations: 378
currently lose_sum: 96.82354348897934
time_elpased: 1.96
batch start
#iterations: 379
currently lose_sum: 96.83300185203552
time_elpased: 1.971
start validation test
0.62706185567
0.639032041228
0.587012452403
0.611918682615
0.627132168575
63.558
batch start
#iterations: 380
currently lose_sum: 96.89947235584259
time_elpased: 1.952
batch start
#iterations: 381
currently lose_sum: 96.78369510173798
time_elpased: 1.934
batch start
#iterations: 382
currently lose_sum: 96.91261720657349
time_elpased: 1.96
batch start
#iterations: 383
currently lose_sum: 97.03754442930222
time_elpased: 1.976
batch start
#iterations: 384
currently lose_sum: 96.64883905649185
time_elpased: 1.915
batch start
#iterations: 385
currently lose_sum: 96.71774327754974
time_elpased: 2.032
batch start
#iterations: 386
currently lose_sum: 96.9870011806488
time_elpased: 1.937
batch start
#iterations: 387
currently lose_sum: 96.9107306599617
time_elpased: 1.976
batch start
#iterations: 388
currently lose_sum: 96.88694435358047
time_elpased: 1.985
batch start
#iterations: 389
currently lose_sum: 96.86333477497101
time_elpased: 1.996
batch start
#iterations: 390
currently lose_sum: 97.08282774686813
time_elpased: 1.978
batch start
#iterations: 391
currently lose_sum: 96.89786744117737
time_elpased: 1.959
batch start
#iterations: 392
currently lose_sum: 96.68663567304611
time_elpased: 2.01
batch start
#iterations: 393
currently lose_sum: 96.99856674671173
time_elpased: 1.994
batch start
#iterations: 394
currently lose_sum: 96.57315665483475
time_elpased: 1.923
batch start
#iterations: 395
currently lose_sum: 96.69630920886993
time_elpased: 1.959
batch start
#iterations: 396
currently lose_sum: 96.69568020105362
time_elpased: 1.998
batch start
#iterations: 397
currently lose_sum: 96.68913048505783
time_elpased: 1.991
batch start
#iterations: 398
currently lose_sum: 96.90356379747391
time_elpased: 1.924
batch start
#iterations: 399
currently lose_sum: 96.8202612400055
time_elpased: 2.019
start validation test
0.627783505155
0.632259431963
0.613975506844
0.622983344646
0.627807747225
63.383
acc: 0.634
pre: 0.637
rec: 0.624
F1: 0.630
auc: 0.634
