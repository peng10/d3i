start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.73734563589096
time_elpased: 1.973
batch start
#iterations: 1
currently lose_sum: 100.30075657367706
time_elpased: 1.832
batch start
#iterations: 2
currently lose_sum: 99.97160589694977
time_elpased: 1.858
batch start
#iterations: 3
currently lose_sum: 99.98268473148346
time_elpased: 1.825
batch start
#iterations: 4
currently lose_sum: 99.75367683172226
time_elpased: 1.849
batch start
#iterations: 5
currently lose_sum: 99.74980288743973
time_elpased: 1.821
batch start
#iterations: 6
currently lose_sum: 99.62174248695374
time_elpased: 1.826
batch start
#iterations: 7
currently lose_sum: 99.37703490257263
time_elpased: 1.83
batch start
#iterations: 8
currently lose_sum: 99.22936242818832
time_elpased: 1.882
batch start
#iterations: 9
currently lose_sum: 99.34503442049026
time_elpased: 1.818
batch start
#iterations: 10
currently lose_sum: 99.20967495441437
time_elpased: 1.822
batch start
#iterations: 11
currently lose_sum: 99.25086492300034
time_elpased: 1.831
batch start
#iterations: 12
currently lose_sum: 99.35822129249573
time_elpased: 1.779
batch start
#iterations: 13
currently lose_sum: 98.93318301439285
time_elpased: 1.842
batch start
#iterations: 14
currently lose_sum: 99.00728476047516
time_elpased: 1.82
batch start
#iterations: 15
currently lose_sum: 98.5904552936554
time_elpased: 1.83
batch start
#iterations: 16
currently lose_sum: 98.96455186605453
time_elpased: 1.788
batch start
#iterations: 17
currently lose_sum: 98.97707587480545
time_elpased: 1.815
batch start
#iterations: 18
currently lose_sum: 98.75391441583633
time_elpased: 1.817
batch start
#iterations: 19
currently lose_sum: 98.927769780159
time_elpased: 1.799
start validation test
0.602783505155
0.648369485023
0.4521971802
0.53279980599
0.603047882674
64.813
batch start
#iterations: 20
currently lose_sum: 98.74316203594208
time_elpased: 1.86
batch start
#iterations: 21
currently lose_sum: 98.59111714363098
time_elpased: 1.826
batch start
#iterations: 22
currently lose_sum: 98.78836220502853
time_elpased: 1.81
batch start
#iterations: 23
currently lose_sum: 98.45796328783035
time_elpased: 1.791
batch start
#iterations: 24
currently lose_sum: 98.53925240039825
time_elpased: 1.823
batch start
#iterations: 25
currently lose_sum: 98.35288882255554
time_elpased: 1.825
batch start
#iterations: 26
currently lose_sum: 98.43972432613373
time_elpased: 1.827
batch start
#iterations: 27
currently lose_sum: 98.2982731461525
time_elpased: 1.812
batch start
#iterations: 28
currently lose_sum: 98.39074140787125
time_elpased: 1.791
batch start
#iterations: 29
currently lose_sum: 98.50118106603622
time_elpased: 1.854
batch start
#iterations: 30
currently lose_sum: 98.39948517084122
time_elpased: 1.829
batch start
#iterations: 31
currently lose_sum: 98.468659222126
time_elpased: 1.812
batch start
#iterations: 32
currently lose_sum: 97.94943332672119
time_elpased: 1.886
batch start
#iterations: 33
currently lose_sum: 98.47489470243454
time_elpased: 1.835
batch start
#iterations: 34
currently lose_sum: 98.17879503965378
time_elpased: 1.823
batch start
#iterations: 35
currently lose_sum: 98.18065613508224
time_elpased: 1.794
batch start
#iterations: 36
currently lose_sum: 98.6342950463295
time_elpased: 2.051
batch start
#iterations: 37
currently lose_sum: 98.26851612329483
time_elpased: 1.857
batch start
#iterations: 38
currently lose_sum: 98.29284358024597
time_elpased: 1.791
batch start
#iterations: 39
currently lose_sum: 98.44949632883072
time_elpased: 1.829
start validation test
0.622783505155
0.629914437344
0.598538643614
0.61382585752
0.622826070749
63.987
batch start
#iterations: 40
currently lose_sum: 98.34004372358322
time_elpased: 1.855
batch start
#iterations: 41
currently lose_sum: 98.24110263586044
time_elpased: 1.833
batch start
#iterations: 42
currently lose_sum: 98.23843282461166
time_elpased: 1.824
batch start
#iterations: 43
currently lose_sum: 98.31308996677399
time_elpased: 1.813
batch start
#iterations: 44
currently lose_sum: 98.23727840185165
time_elpased: 1.811
batch start
#iterations: 45
currently lose_sum: 98.0229440331459
time_elpased: 1.851
batch start
#iterations: 46
currently lose_sum: 98.17643731832504
time_elpased: 1.839
batch start
#iterations: 47
currently lose_sum: 98.11936271190643
time_elpased: 1.798
batch start
#iterations: 48
currently lose_sum: 98.36852270364761
time_elpased: 1.797
batch start
#iterations: 49
currently lose_sum: 98.27449959516525
time_elpased: 1.788
batch start
#iterations: 50
currently lose_sum: 98.52317756414413
time_elpased: 1.828
batch start
#iterations: 51
currently lose_sum: 97.9057006239891
time_elpased: 1.86
batch start
#iterations: 52
currently lose_sum: 97.8410010933876
time_elpased: 1.811
batch start
#iterations: 53
currently lose_sum: 98.35553139448166
time_elpased: 1.809
batch start
#iterations: 54
currently lose_sum: 98.19019562005997
time_elpased: 1.809
batch start
#iterations: 55
currently lose_sum: 98.07721883058548
time_elpased: 1.812
batch start
#iterations: 56
currently lose_sum: 98.2702004313469
time_elpased: 1.879
batch start
#iterations: 57
currently lose_sum: 97.8569233417511
time_elpased: 1.821
batch start
#iterations: 58
currently lose_sum: 98.19908088445663
time_elpased: 1.823
batch start
#iterations: 59
currently lose_sum: 98.04048895835876
time_elpased: 1.876
start validation test
0.620103092784
0.628265384195
0.59154059895
0.609350153716
0.620153238647
63.902
batch start
#iterations: 60
currently lose_sum: 98.32893794775009
time_elpased: 1.856
batch start
#iterations: 61
currently lose_sum: 98.2261073589325
time_elpased: 1.815
batch start
#iterations: 62
currently lose_sum: 98.06558626890182
time_elpased: 1.779
batch start
#iterations: 63
currently lose_sum: 98.36271822452545
time_elpased: 1.832
batch start
#iterations: 64
currently lose_sum: 98.2754767537117
time_elpased: 1.793
batch start
#iterations: 65
currently lose_sum: 97.80094647407532
time_elpased: 1.793
batch start
#iterations: 66
currently lose_sum: 97.62781208753586
time_elpased: 1.806
batch start
#iterations: 67
currently lose_sum: 97.89582169055939
time_elpased: 1.841
batch start
#iterations: 68
currently lose_sum: 98.06799077987671
time_elpased: 1.807
batch start
#iterations: 69
currently lose_sum: 98.1043114066124
time_elpased: 1.797
batch start
#iterations: 70
currently lose_sum: 97.74672639369965
time_elpased: 1.829
batch start
#iterations: 71
currently lose_sum: 97.68850803375244
time_elpased: 1.872
batch start
#iterations: 72
currently lose_sum: 97.60200357437134
time_elpased: 1.789
batch start
#iterations: 73
currently lose_sum: 97.75624978542328
time_elpased: 1.883
batch start
#iterations: 74
currently lose_sum: 97.88035213947296
time_elpased: 1.835
batch start
#iterations: 75
currently lose_sum: 97.78790467977524
time_elpased: 1.835
batch start
#iterations: 76
currently lose_sum: 97.79247856140137
time_elpased: 1.858
batch start
#iterations: 77
currently lose_sum: 97.66746288537979
time_elpased: 1.785
batch start
#iterations: 78
currently lose_sum: 97.87215322256088
time_elpased: 1.806
batch start
#iterations: 79
currently lose_sum: 98.09617549180984
time_elpased: 1.857
start validation test
0.627474226804
0.635061835539
0.602449315632
0.618325851598
0.627518161896
63.703
batch start
#iterations: 80
currently lose_sum: 97.95148694515228
time_elpased: 1.882
batch start
#iterations: 81
currently lose_sum: 98.19339019060135
time_elpased: 1.781
batch start
#iterations: 82
currently lose_sum: 97.83256316184998
time_elpased: 1.813
batch start
#iterations: 83
currently lose_sum: 98.14878678321838
time_elpased: 1.794
batch start
#iterations: 84
currently lose_sum: 97.84016990661621
time_elpased: 1.818
batch start
#iterations: 85
currently lose_sum: 97.96910637617111
time_elpased: 1.84
batch start
#iterations: 86
currently lose_sum: 97.86607766151428
time_elpased: 1.82
batch start
#iterations: 87
currently lose_sum: 97.81359320878983
time_elpased: 1.832
batch start
#iterations: 88
currently lose_sum: 97.70188230276108
time_elpased: 1.791
batch start
#iterations: 89
currently lose_sum: 97.85822343826294
time_elpased: 1.842
batch start
#iterations: 90
currently lose_sum: 97.78960329294205
time_elpased: 1.813
batch start
#iterations: 91
currently lose_sum: 97.82233566045761
time_elpased: 1.828
batch start
#iterations: 92
currently lose_sum: 97.84796458482742
time_elpased: 1.786
batch start
#iterations: 93
currently lose_sum: 97.68289649486542
time_elpased: 1.811
batch start
#iterations: 94
currently lose_sum: 97.77871799468994
time_elpased: 1.837
batch start
#iterations: 95
currently lose_sum: 97.82961702346802
time_elpased: 1.835
batch start
#iterations: 96
currently lose_sum: 97.87503558397293
time_elpased: 1.864
batch start
#iterations: 97
currently lose_sum: 97.91677397489548
time_elpased: 1.8
batch start
#iterations: 98
currently lose_sum: 97.9544786810875
time_elpased: 1.824
batch start
#iterations: 99
currently lose_sum: 97.69669425487518
time_elpased: 1.818
start validation test
0.622474226804
0.634877691354
0.579602758053
0.605982354207
0.62254949428
63.580
batch start
#iterations: 100
currently lose_sum: 97.46748781204224
time_elpased: 1.906
batch start
#iterations: 101
currently lose_sum: 97.90430945158005
time_elpased: 1.821
batch start
#iterations: 102
currently lose_sum: 97.62021243572235
time_elpased: 1.822
batch start
#iterations: 103
currently lose_sum: 97.68368995189667
time_elpased: 1.83
batch start
#iterations: 104
currently lose_sum: 97.74190264940262
time_elpased: 1.887
batch start
#iterations: 105
currently lose_sum: 97.37684416770935
time_elpased: 1.846
batch start
#iterations: 106
currently lose_sum: 98.01797538995743
time_elpased: 1.943
batch start
#iterations: 107
currently lose_sum: 97.51324021816254
time_elpased: 1.83
batch start
#iterations: 108
currently lose_sum: 97.6387545466423
time_elpased: 1.85
batch start
#iterations: 109
currently lose_sum: 97.70972609519958
time_elpased: 1.802
batch start
#iterations: 110
currently lose_sum: 97.90341609716415
time_elpased: 1.847
batch start
#iterations: 111
currently lose_sum: 97.65644091367722
time_elpased: 1.822
batch start
#iterations: 112
currently lose_sum: 97.73119843006134
time_elpased: 1.787
batch start
#iterations: 113
currently lose_sum: 97.44491136074066
time_elpased: 1.841
batch start
#iterations: 114
currently lose_sum: 97.75558280944824
time_elpased: 1.811
batch start
#iterations: 115
currently lose_sum: 97.62503731250763
time_elpased: 1.827
batch start
#iterations: 116
currently lose_sum: 97.53883588314056
time_elpased: 1.819
batch start
#iterations: 117
currently lose_sum: 97.83152341842651
time_elpased: 1.859
batch start
#iterations: 118
currently lose_sum: 97.64102464914322
time_elpased: 1.809
batch start
#iterations: 119
currently lose_sum: 97.53782910108566
time_elpased: 1.848
start validation test
0.624639175258
0.638210920649
0.578573633838
0.606930799957
0.624720050421
63.595
batch start
#iterations: 120
currently lose_sum: 97.69274199008942
time_elpased: 1.815
batch start
#iterations: 121
currently lose_sum: 97.59352034330368
time_elpased: 1.773
batch start
#iterations: 122
currently lose_sum: 97.65491890907288
time_elpased: 1.777
batch start
#iterations: 123
currently lose_sum: 97.77094006538391
time_elpased: 1.831
batch start
#iterations: 124
currently lose_sum: 97.69780796766281
time_elpased: 1.764
batch start
#iterations: 125
currently lose_sum: 97.54236173629761
time_elpased: 1.794
batch start
#iterations: 126
currently lose_sum: 97.5473011136055
time_elpased: 1.821
batch start
#iterations: 127
currently lose_sum: 97.76767653226852
time_elpased: 1.843
batch start
#iterations: 128
currently lose_sum: 97.626689016819
time_elpased: 1.817
batch start
#iterations: 129
currently lose_sum: 97.75503706932068
time_elpased: 1.775
batch start
#iterations: 130
currently lose_sum: 97.52956712245941
time_elpased: 1.839
batch start
#iterations: 131
currently lose_sum: 97.88688164949417
time_elpased: 1.871
batch start
#iterations: 132
currently lose_sum: 97.59692758321762
time_elpased: 1.882
batch start
#iterations: 133
currently lose_sum: 97.51796007156372
time_elpased: 1.83
batch start
#iterations: 134
currently lose_sum: 97.41288822889328
time_elpased: 1.793
batch start
#iterations: 135
currently lose_sum: 97.73028790950775
time_elpased: 1.806
batch start
#iterations: 136
currently lose_sum: 97.68111878633499
time_elpased: 1.835
batch start
#iterations: 137
currently lose_sum: 97.42773020267487
time_elpased: 1.843
batch start
#iterations: 138
currently lose_sum: 97.50386017560959
time_elpased: 1.838
batch start
#iterations: 139
currently lose_sum: 97.75067073106766
time_elpased: 1.834
start validation test
0.622216494845
0.639258222533
0.564062982402
0.599311136625
0.622318592306
63.799
batch start
#iterations: 140
currently lose_sum: 97.59262067079544
time_elpased: 1.837
batch start
#iterations: 141
currently lose_sum: 97.70310360193253
time_elpased: 1.822
batch start
#iterations: 142
currently lose_sum: 97.37777584791183
time_elpased: 1.846
batch start
#iterations: 143
currently lose_sum: 97.77557355165482
time_elpased: 1.805
batch start
#iterations: 144
currently lose_sum: 97.54270994663239
time_elpased: 1.835
batch start
#iterations: 145
currently lose_sum: 97.4316036105156
time_elpased: 1.797
batch start
#iterations: 146
currently lose_sum: 97.52816700935364
time_elpased: 1.807
batch start
#iterations: 147
currently lose_sum: 98.02357876300812
time_elpased: 1.863
batch start
#iterations: 148
currently lose_sum: 97.52952480316162
time_elpased: 1.795
batch start
#iterations: 149
currently lose_sum: 97.7669665813446
time_elpased: 1.832
batch start
#iterations: 150
currently lose_sum: 97.77975314855576
time_elpased: 1.776
batch start
#iterations: 151
currently lose_sum: 97.44527810811996
time_elpased: 1.775
batch start
#iterations: 152
currently lose_sum: 97.62503206729889
time_elpased: 1.85
batch start
#iterations: 153
currently lose_sum: 97.68624347448349
time_elpased: 1.797
batch start
#iterations: 154
currently lose_sum: 97.76674509048462
time_elpased: 1.783
batch start
#iterations: 155
currently lose_sum: 97.73820942640305
time_elpased: 1.796
batch start
#iterations: 156
currently lose_sum: 97.30954396724701
time_elpased: 1.849
batch start
#iterations: 157
currently lose_sum: 97.65169143676758
time_elpased: 1.875
batch start
#iterations: 158
currently lose_sum: 97.63881480693817
time_elpased: 1.782
batch start
#iterations: 159
currently lose_sum: 97.78339862823486
time_elpased: 1.847
start validation test
0.62793814433
0.629307668426
0.62581043532
0.627554179567
0.627941879851
63.590
batch start
#iterations: 160
currently lose_sum: 97.58134061098099
time_elpased: 1.845
batch start
#iterations: 161
currently lose_sum: 97.50164532661438
time_elpased: 1.769
batch start
#iterations: 162
currently lose_sum: 97.41858911514282
time_elpased: 1.828
batch start
#iterations: 163
currently lose_sum: 97.65417057275772
time_elpased: 1.808
batch start
#iterations: 164
currently lose_sum: 97.94254797697067
time_elpased: 1.803
batch start
#iterations: 165
currently lose_sum: 97.81019186973572
time_elpased: 1.815
batch start
#iterations: 166
currently lose_sum: 97.4031069278717
time_elpased: 1.79
batch start
#iterations: 167
currently lose_sum: 97.5096218585968
time_elpased: 1.784
batch start
#iterations: 168
currently lose_sum: 97.39053505659103
time_elpased: 1.814
batch start
#iterations: 169
currently lose_sum: 97.31371515989304
time_elpased: 1.785
batch start
#iterations: 170
currently lose_sum: 97.54312062263489
time_elpased: 1.748
batch start
#iterations: 171
currently lose_sum: 97.37408554553986
time_elpased: 1.852
batch start
#iterations: 172
currently lose_sum: 97.52748876810074
time_elpased: 1.819
batch start
#iterations: 173
currently lose_sum: 97.54876297712326
time_elpased: 1.958
batch start
#iterations: 174
currently lose_sum: 97.38846909999847
time_elpased: 1.835
batch start
#iterations: 175
currently lose_sum: 97.57622373104095
time_elpased: 1.779
batch start
#iterations: 176
currently lose_sum: 97.27074933052063
time_elpased: 1.81
batch start
#iterations: 177
currently lose_sum: 97.44773310422897
time_elpased: 1.876
batch start
#iterations: 178
currently lose_sum: 97.49033737182617
time_elpased: 1.789
batch start
#iterations: 179
currently lose_sum: 97.24373906850815
time_elpased: 1.809
start validation test
0.624896907216
0.635645986213
0.588350313883
0.611084388862
0.624961070398
63.490
batch start
#iterations: 180
currently lose_sum: 97.59508073329926
time_elpased: 1.823
batch start
#iterations: 181
currently lose_sum: 97.46596503257751
time_elpased: 1.826
batch start
#iterations: 182
currently lose_sum: 97.78009456396103
time_elpased: 1.873
batch start
#iterations: 183
currently lose_sum: 97.41192173957825
time_elpased: 1.804
batch start
#iterations: 184
currently lose_sum: 97.01529771089554
time_elpased: 1.755
batch start
#iterations: 185
currently lose_sum: 97.24550318717957
time_elpased: 1.851
batch start
#iterations: 186
currently lose_sum: 97.19052118062973
time_elpased: 1.807
batch start
#iterations: 187
currently lose_sum: 97.19086265563965
time_elpased: 1.817
batch start
#iterations: 188
currently lose_sum: 97.3017235994339
time_elpased: 1.787
batch start
#iterations: 189
currently lose_sum: 97.4247258901596
time_elpased: 1.86
batch start
#iterations: 190
currently lose_sum: 97.39831173419952
time_elpased: 1.826
batch start
#iterations: 191
currently lose_sum: 97.1654942035675
time_elpased: 1.815
batch start
#iterations: 192
currently lose_sum: 97.19816625118256
time_elpased: 1.813
batch start
#iterations: 193
currently lose_sum: 97.23069804906845
time_elpased: 1.814
batch start
#iterations: 194
currently lose_sum: 97.08997601270676
time_elpased: 1.792
batch start
#iterations: 195
currently lose_sum: 97.55836719274521
time_elpased: 1.825
batch start
#iterations: 196
currently lose_sum: 97.28989762067795
time_elpased: 1.866
batch start
#iterations: 197
currently lose_sum: 97.66881281137466
time_elpased: 1.81
batch start
#iterations: 198
currently lose_sum: 97.34557157754898
time_elpased: 1.78
batch start
#iterations: 199
currently lose_sum: 97.46736079454422
time_elpased: 1.818
start validation test
0.623505154639
0.633123689727
0.590511474735
0.611075612354
0.623563080133
63.621
batch start
#iterations: 200
currently lose_sum: 97.39569580554962
time_elpased: 1.777
batch start
#iterations: 201
currently lose_sum: 97.05703520774841
time_elpased: 1.83
batch start
#iterations: 202
currently lose_sum: 97.34015876054764
time_elpased: 1.841
batch start
#iterations: 203
currently lose_sum: 97.32914990186691
time_elpased: 1.85
batch start
#iterations: 204
currently lose_sum: 97.48357445001602
time_elpased: 1.825
batch start
#iterations: 205
currently lose_sum: 97.29503172636032
time_elpased: 1.859
batch start
#iterations: 206
currently lose_sum: 97.17311155796051
time_elpased: 1.798
batch start
#iterations: 207
currently lose_sum: 97.18192040920258
time_elpased: 1.766
batch start
#iterations: 208
currently lose_sum: 97.0996395945549
time_elpased: 1.842
batch start
#iterations: 209
currently lose_sum: 97.04568922519684
time_elpased: 1.792
batch start
#iterations: 210
currently lose_sum: 97.28718727827072
time_elpased: 1.794
batch start
#iterations: 211
currently lose_sum: 97.33554339408875
time_elpased: 1.879
batch start
#iterations: 212
currently lose_sum: 97.03593754768372
time_elpased: 1.844
batch start
#iterations: 213
currently lose_sum: 96.87806415557861
time_elpased: 1.817
batch start
#iterations: 214
currently lose_sum: 97.02674335241318
time_elpased: 1.778
batch start
#iterations: 215
currently lose_sum: 97.13241785764694
time_elpased: 1.831
batch start
#iterations: 216
currently lose_sum: 97.2440465092659
time_elpased: 1.883
batch start
#iterations: 217
currently lose_sum: 97.0755500793457
time_elpased: 1.829
batch start
#iterations: 218
currently lose_sum: 97.21466386318207
time_elpased: 1.796
batch start
#iterations: 219
currently lose_sum: 97.44982177019119
time_elpased: 1.807
start validation test
0.626855670103
0.632854385589
0.607389111866
0.619860316127
0.626889846649
63.473
batch start
#iterations: 220
currently lose_sum: 97.4212361574173
time_elpased: 1.882
batch start
#iterations: 221
currently lose_sum: 97.60457366704941
time_elpased: 1.867
batch start
#iterations: 222
currently lose_sum: 97.19951140880585
time_elpased: 1.847
batch start
#iterations: 223
currently lose_sum: 97.10594511032104
time_elpased: 1.837
batch start
#iterations: 224
currently lose_sum: 97.25049686431885
time_elpased: 1.873
batch start
#iterations: 225
currently lose_sum: 97.27289664745331
time_elpased: 1.81
batch start
#iterations: 226
currently lose_sum: 97.42482602596283
time_elpased: 1.824
batch start
#iterations: 227
currently lose_sum: 97.26227754354477
time_elpased: 1.847
batch start
#iterations: 228
currently lose_sum: 97.15931111574173
time_elpased: 1.858
batch start
#iterations: 229
currently lose_sum: 97.53024595975876
time_elpased: 1.798
batch start
#iterations: 230
currently lose_sum: 97.28463464975357
time_elpased: 1.783
batch start
#iterations: 231
currently lose_sum: 97.07912409305573
time_elpased: 1.84
batch start
#iterations: 232
currently lose_sum: 97.27023786306381
time_elpased: 1.759
batch start
#iterations: 233
currently lose_sum: 97.16533201932907
time_elpased: 1.857
batch start
#iterations: 234
currently lose_sum: 97.28209066390991
time_elpased: 1.811
batch start
#iterations: 235
currently lose_sum: 97.14052963256836
time_elpased: 1.828
batch start
#iterations: 236
currently lose_sum: 97.1527538895607
time_elpased: 1.794
batch start
#iterations: 237
currently lose_sum: 97.24811804294586
time_elpased: 1.946
batch start
#iterations: 238
currently lose_sum: 97.39327847957611
time_elpased: 1.814
batch start
#iterations: 239
currently lose_sum: 97.04598450660706
time_elpased: 1.826
start validation test
0.628505154639
0.632105263158
0.617989091283
0.624967476713
0.62852361721
63.340
batch start
#iterations: 240
currently lose_sum: 97.1406484246254
time_elpased: 1.841
batch start
#iterations: 241
currently lose_sum: 97.01557183265686
time_elpased: 1.821
batch start
#iterations: 242
currently lose_sum: 97.4215195775032
time_elpased: 1.793
batch start
#iterations: 243
currently lose_sum: 97.18232882022858
time_elpased: 1.835
batch start
#iterations: 244
currently lose_sum: 97.33255159854889
time_elpased: 1.842
batch start
#iterations: 245
currently lose_sum: 97.504310131073
time_elpased: 1.805
batch start
#iterations: 246
currently lose_sum: 97.33308273553848
time_elpased: 1.808
batch start
#iterations: 247
currently lose_sum: 97.21887618303299
time_elpased: 1.776
batch start
#iterations: 248
currently lose_sum: 97.24103969335556
time_elpased: 1.853
batch start
#iterations: 249
currently lose_sum: 97.26799011230469
time_elpased: 1.81
batch start
#iterations: 250
currently lose_sum: 97.39927518367767
time_elpased: 1.816
batch start
#iterations: 251
currently lose_sum: 97.17518752813339
time_elpased: 1.835
batch start
#iterations: 252
currently lose_sum: 97.3004823923111
time_elpased: 1.808
batch start
#iterations: 253
currently lose_sum: 97.24613755941391
time_elpased: 1.822
batch start
#iterations: 254
currently lose_sum: 97.33150565624237
time_elpased: 1.852
batch start
#iterations: 255
currently lose_sum: 96.91339838504791
time_elpased: 1.813
batch start
#iterations: 256
currently lose_sum: 97.171950340271
time_elpased: 1.767
batch start
#iterations: 257
currently lose_sum: 97.4146431684494
time_elpased: 1.847
batch start
#iterations: 258
currently lose_sum: 97.2601627111435
time_elpased: 1.767
batch start
#iterations: 259
currently lose_sum: 97.21690768003464
time_elpased: 1.902
start validation test
0.624587628866
0.63382449967
0.593187197695
0.61283291691
0.624642757166
63.345
batch start
#iterations: 260
currently lose_sum: 96.84392648935318
time_elpased: 1.92
batch start
#iterations: 261
currently lose_sum: 97.37450158596039
time_elpased: 1.831
batch start
#iterations: 262
currently lose_sum: 97.11330217123032
time_elpased: 1.841
batch start
#iterations: 263
currently lose_sum: 97.29011845588684
time_elpased: 1.823
batch start
#iterations: 264
currently lose_sum: 97.35548657178879
time_elpased: 1.836
batch start
#iterations: 265
currently lose_sum: 96.92471837997437
time_elpased: 1.787
batch start
#iterations: 266
currently lose_sum: 97.2120264172554
time_elpased: 1.782
batch start
#iterations: 267
currently lose_sum: 97.14445000886917
time_elpased: 1.844
batch start
#iterations: 268
currently lose_sum: 97.1689692735672
time_elpased: 1.857
batch start
#iterations: 269
currently lose_sum: 97.07945078611374
time_elpased: 1.873
batch start
#iterations: 270
currently lose_sum: 97.5208398103714
time_elpased: 1.803
batch start
#iterations: 271
currently lose_sum: 97.32670426368713
time_elpased: 1.862
batch start
#iterations: 272
currently lose_sum: 97.1991925239563
time_elpased: 1.847
batch start
#iterations: 273
currently lose_sum: 97.41621553897858
time_elpased: 1.818
batch start
#iterations: 274
currently lose_sum: 97.00634998083115
time_elpased: 1.76
batch start
#iterations: 275
currently lose_sum: 97.03115993738174
time_elpased: 1.826
batch start
#iterations: 276
currently lose_sum: 97.52527630329132
time_elpased: 1.792
batch start
#iterations: 277
currently lose_sum: 97.54102611541748
time_elpased: 1.847
batch start
#iterations: 278
currently lose_sum: 97.25206738710403
time_elpased: 1.885
batch start
#iterations: 279
currently lose_sum: 97.2311162352562
time_elpased: 1.798
start validation test
0.628298969072
0.633724653148
0.611093959041
0.622203594069
0.628329175121
63.405
batch start
#iterations: 280
currently lose_sum: 97.23032695055008
time_elpased: 1.893
batch start
#iterations: 281
currently lose_sum: 97.24709922075272
time_elpased: 1.832
batch start
#iterations: 282
currently lose_sum: 97.34731894731522
time_elpased: 1.839
batch start
#iterations: 283
currently lose_sum: 97.41074085235596
time_elpased: 1.812
batch start
#iterations: 284
currently lose_sum: 97.1465739607811
time_elpased: 1.841
batch start
#iterations: 285
currently lose_sum: 97.02741158008575
time_elpased: 1.875
batch start
#iterations: 286
currently lose_sum: 97.38528674840927
time_elpased: 1.839
batch start
#iterations: 287
currently lose_sum: 97.1819874048233
time_elpased: 1.844
batch start
#iterations: 288
currently lose_sum: 96.93041133880615
time_elpased: 1.8
batch start
#iterations: 289
currently lose_sum: 97.05844575166702
time_elpased: 1.81
batch start
#iterations: 290
currently lose_sum: 97.25867581367493
time_elpased: 1.843
batch start
#iterations: 291
currently lose_sum: 96.94768935441971
time_elpased: 1.849
batch start
#iterations: 292
currently lose_sum: 96.97829639911652
time_elpased: 1.79
batch start
#iterations: 293
currently lose_sum: 96.99359530210495
time_elpased: 1.918
batch start
#iterations: 294
currently lose_sum: 97.1016017794609
time_elpased: 1.777
batch start
#iterations: 295
currently lose_sum: 96.86988097429276
time_elpased: 1.901
batch start
#iterations: 296
currently lose_sum: 97.06150740385056
time_elpased: 1.81
batch start
#iterations: 297
currently lose_sum: 97.31064009666443
time_elpased: 1.866
batch start
#iterations: 298
currently lose_sum: 97.38273686170578
time_elpased: 1.86
batch start
#iterations: 299
currently lose_sum: 96.84987169504166
time_elpased: 1.815
start validation test
0.624536082474
0.633813661863
0.592981372852
0.612717992344
0.624591481634
63.419
batch start
#iterations: 300
currently lose_sum: 97.40861713886261
time_elpased: 1.797
batch start
#iterations: 301
currently lose_sum: 97.2906551361084
time_elpased: 1.807
batch start
#iterations: 302
currently lose_sum: 97.3578120470047
time_elpased: 1.817
batch start
#iterations: 303
currently lose_sum: 97.1945651769638
time_elpased: 1.871
batch start
#iterations: 304
currently lose_sum: 97.12759643793106
time_elpased: 1.814
batch start
#iterations: 305
currently lose_sum: 97.10717016458511
time_elpased: 1.846
batch start
#iterations: 306
currently lose_sum: 97.30739259719849
time_elpased: 1.798
batch start
#iterations: 307
currently lose_sum: 97.19219183921814
time_elpased: 1.812
batch start
#iterations: 308
currently lose_sum: 97.0859984755516
time_elpased: 1.797
batch start
#iterations: 309
currently lose_sum: 97.3576208949089
time_elpased: 1.836
batch start
#iterations: 310
currently lose_sum: 97.19388800859451
time_elpased: 1.875
batch start
#iterations: 311
currently lose_sum: 97.17196035385132
time_elpased: 1.832
batch start
#iterations: 312
currently lose_sum: 97.33500319719315
time_elpased: 1.793
batch start
#iterations: 313
currently lose_sum: 96.98292410373688
time_elpased: 1.837
batch start
#iterations: 314
currently lose_sum: 96.95996063947678
time_elpased: 1.786
batch start
#iterations: 315
currently lose_sum: 97.03143018484116
time_elpased: 1.801
batch start
#iterations: 316
currently lose_sum: 97.3505175113678
time_elpased: 1.816
batch start
#iterations: 317
currently lose_sum: 97.13619339466095
time_elpased: 1.839
batch start
#iterations: 318
currently lose_sum: 97.16452223062515
time_elpased: 1.792
batch start
#iterations: 319
currently lose_sum: 97.10926192998886
time_elpased: 1.818
start validation test
0.625773195876
0.633634286957
0.599464855408
0.616076150185
0.625819384226
63.504
batch start
#iterations: 320
currently lose_sum: 96.98867511749268
time_elpased: 1.775
batch start
#iterations: 321
currently lose_sum: 96.86878269910812
time_elpased: 1.857
batch start
#iterations: 322
currently lose_sum: 97.3420250415802
time_elpased: 1.815
batch start
#iterations: 323
currently lose_sum: 97.2233716249466
time_elpased: 1.792
batch start
#iterations: 324
currently lose_sum: 97.36836767196655
time_elpased: 1.761
batch start
#iterations: 325
currently lose_sum: 97.46014595031738
time_elpased: 1.827
batch start
#iterations: 326
currently lose_sum: 97.14119112491608
time_elpased: 1.936
batch start
#iterations: 327
currently lose_sum: 97.301493704319
time_elpased: 1.815
batch start
#iterations: 328
currently lose_sum: 96.93157535791397
time_elpased: 1.824
batch start
#iterations: 329
currently lose_sum: 97.16299551725388
time_elpased: 1.904
batch start
#iterations: 330
currently lose_sum: 97.00969183444977
time_elpased: 1.832
batch start
#iterations: 331
currently lose_sum: 97.2252989411354
time_elpased: 1.875
batch start
#iterations: 332
currently lose_sum: 97.01959127187729
time_elpased: 1.888
batch start
#iterations: 333
currently lose_sum: 97.2368894815445
time_elpased: 1.88
batch start
#iterations: 334
currently lose_sum: 97.19979500770569
time_elpased: 1.826
batch start
#iterations: 335
currently lose_sum: 97.34471333026886
time_elpased: 1.842
batch start
#iterations: 336
currently lose_sum: 96.9943659901619
time_elpased: 1.863
batch start
#iterations: 337
currently lose_sum: 97.40957725048065
time_elpased: 1.875
batch start
#iterations: 338
currently lose_sum: 97.05289942026138
time_elpased: 1.792
batch start
#iterations: 339
currently lose_sum: 97.05583107471466
time_elpased: 1.824
start validation test
0.627577319588
0.636323851204
0.598538643614
0.616853157978
0.627628301462
63.494
batch start
#iterations: 340
currently lose_sum: 96.9087103009224
time_elpased: 1.899
batch start
#iterations: 341
currently lose_sum: 96.91366815567017
time_elpased: 1.891
batch start
#iterations: 342
currently lose_sum: 97.20031094551086
time_elpased: 1.892
batch start
#iterations: 343
currently lose_sum: 97.29749673604965
time_elpased: 1.893
batch start
#iterations: 344
currently lose_sum: 97.03607940673828
time_elpased: 1.867
batch start
#iterations: 345
currently lose_sum: 97.06025177240372
time_elpased: 1.826
batch start
#iterations: 346
currently lose_sum: 97.14153188467026
time_elpased: 1.793
batch start
#iterations: 347
currently lose_sum: 97.12317842245102
time_elpased: 1.907
batch start
#iterations: 348
currently lose_sum: 97.01338714361191
time_elpased: 1.832
batch start
#iterations: 349
currently lose_sum: 97.5562275648117
time_elpased: 1.862
batch start
#iterations: 350
currently lose_sum: 96.97841203212738
time_elpased: 1.942
batch start
#iterations: 351
currently lose_sum: 97.0024784207344
time_elpased: 1.894
batch start
#iterations: 352
currently lose_sum: 96.97650545835495
time_elpased: 1.882
batch start
#iterations: 353
currently lose_sum: 97.1407300233841
time_elpased: 1.901
batch start
#iterations: 354
currently lose_sum: 97.14385950565338
time_elpased: 1.857
batch start
#iterations: 355
currently lose_sum: 97.45562982559204
time_elpased: 1.852
batch start
#iterations: 356
currently lose_sum: 97.16564559936523
time_elpased: 1.875
batch start
#iterations: 357
currently lose_sum: 97.06076240539551
time_elpased: 1.873
batch start
#iterations: 358
currently lose_sum: 97.3435623049736
time_elpased: 1.791
batch start
#iterations: 359
currently lose_sum: 97.0710261464119
time_elpased: 1.891
start validation test
0.628298969072
0.638300220751
0.595142533704
0.615966341801
0.628357180308
63.422
batch start
#iterations: 360
currently lose_sum: 97.4680797457695
time_elpased: 1.807
batch start
#iterations: 361
currently lose_sum: 97.38822150230408
time_elpased: 1.838
batch start
#iterations: 362
currently lose_sum: 97.01136821508408
time_elpased: 1.79
batch start
#iterations: 363
currently lose_sum: 97.04510343074799
time_elpased: 1.852
batch start
#iterations: 364
currently lose_sum: 97.30592572689056
time_elpased: 1.843
batch start
#iterations: 365
currently lose_sum: 97.4253380894661
time_elpased: 1.933
batch start
#iterations: 366
currently lose_sum: 96.91010063886642
time_elpased: 1.86
batch start
#iterations: 367
currently lose_sum: 97.41637879610062
time_elpased: 1.838
batch start
#iterations: 368
currently lose_sum: 97.01088118553162
time_elpased: 1.828
batch start
#iterations: 369
currently lose_sum: 97.11664015054703
time_elpased: 1.884
batch start
#iterations: 370
currently lose_sum: 97.27816843986511
time_elpased: 1.815
batch start
#iterations: 371
currently lose_sum: 96.92660993337631
time_elpased: 1.867
batch start
#iterations: 372
currently lose_sum: 96.9847058057785
time_elpased: 1.83
batch start
#iterations: 373
currently lose_sum: 96.9753766655922
time_elpased: 1.834
batch start
#iterations: 374
currently lose_sum: 97.12845730781555
time_elpased: 1.798
batch start
#iterations: 375
currently lose_sum: 97.41102385520935
time_elpased: 1.842
batch start
#iterations: 376
currently lose_sum: 97.01769030094147
time_elpased: 1.866
batch start
#iterations: 377
currently lose_sum: 97.20470297336578
time_elpased: 1.839
batch start
#iterations: 378
currently lose_sum: 97.0964789390564
time_elpased: 1.855
batch start
#iterations: 379
currently lose_sum: 96.91750591993332
time_elpased: 1.825
start validation test
0.626288659794
0.636797160918
0.590923124421
0.613003095975
0.626350749446
63.524
batch start
#iterations: 380
currently lose_sum: 97.18547546863556
time_elpased: 1.805
batch start
#iterations: 381
currently lose_sum: 96.81798207759857
time_elpased: 1.839
batch start
#iterations: 382
currently lose_sum: 97.0977429151535
time_elpased: 1.871
batch start
#iterations: 383
currently lose_sum: 96.81415700912476
time_elpased: 1.863
batch start
#iterations: 384
currently lose_sum: 97.37421798706055
time_elpased: 1.95
batch start
#iterations: 385
currently lose_sum: 97.21367555856705
time_elpased: 1.828
batch start
#iterations: 386
currently lose_sum: 97.13590055704117
time_elpased: 1.811
batch start
#iterations: 387
currently lose_sum: 97.23486179113388
time_elpased: 1.928
batch start
#iterations: 388
currently lose_sum: 97.1383096575737
time_elpased: 1.846
batch start
#iterations: 389
currently lose_sum: 97.10548454523087
time_elpased: 1.897
batch start
#iterations: 390
currently lose_sum: 96.93874645233154
time_elpased: 1.782
batch start
#iterations: 391
currently lose_sum: 97.18555855751038
time_elpased: 1.874
batch start
#iterations: 392
currently lose_sum: 96.98371595144272
time_elpased: 1.798
batch start
#iterations: 393
currently lose_sum: 97.17608904838562
time_elpased: 1.83
batch start
#iterations: 394
currently lose_sum: 97.14657664299011
time_elpased: 1.856
batch start
#iterations: 395
currently lose_sum: 96.83317112922668
time_elpased: 1.878
batch start
#iterations: 396
currently lose_sum: 97.26727652549744
time_elpased: 1.869
batch start
#iterations: 397
currently lose_sum: 96.71234530210495
time_elpased: 1.854
batch start
#iterations: 398
currently lose_sum: 96.57147014141083
time_elpased: 1.848
batch start
#iterations: 399
currently lose_sum: 97.13129621744156
time_elpased: 1.814
start validation test
0.62381443299
0.634823319585
0.586086240609
0.609482020548
0.623880670651
63.507
acc: 0.629
pre: 0.631
rec: 0.623
F1: 0.627
auc: 0.629
