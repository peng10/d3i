start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.74364250898361
time_elpased: 1.91
batch start
#iterations: 1
currently lose_sum: 100.42633545398712
time_elpased: 1.786
batch start
#iterations: 2
currently lose_sum: 100.11218720674515
time_elpased: 1.805
batch start
#iterations: 3
currently lose_sum: 99.95655852556229
time_elpased: 1.801
batch start
#iterations: 4
currently lose_sum: 99.849274456501
time_elpased: 1.822
batch start
#iterations: 5
currently lose_sum: 99.81940573453903
time_elpased: 1.802
batch start
#iterations: 6
currently lose_sum: 99.51459336280823
time_elpased: 1.787
batch start
#iterations: 7
currently lose_sum: 99.45345062017441
time_elpased: 1.822
batch start
#iterations: 8
currently lose_sum: 99.49409383535385
time_elpased: 1.793
batch start
#iterations: 9
currently lose_sum: 99.09699332714081
time_elpased: 1.817
batch start
#iterations: 10
currently lose_sum: 99.17566907405853
time_elpased: 1.857
batch start
#iterations: 11
currently lose_sum: 99.30472779273987
time_elpased: 1.792
batch start
#iterations: 12
currently lose_sum: 99.0976397395134
time_elpased: 1.787
batch start
#iterations: 13
currently lose_sum: 99.16672760248184
time_elpased: 1.786
batch start
#iterations: 14
currently lose_sum: 98.85895866155624
time_elpased: 1.834
batch start
#iterations: 15
currently lose_sum: 98.90315079689026
time_elpased: 1.831
batch start
#iterations: 16
currently lose_sum: 98.94332242012024
time_elpased: 1.764
batch start
#iterations: 17
currently lose_sum: 98.99997395277023
time_elpased: 1.772
batch start
#iterations: 18
currently lose_sum: 98.67142564058304
time_elpased: 1.785
batch start
#iterations: 19
currently lose_sum: 98.86328393220901
time_elpased: 1.803
start validation test
0.617164948454
0.627250500111
0.580837707111
0.603152551429
0.617228726529
64.399
batch start
#iterations: 20
currently lose_sum: 98.84624880552292
time_elpased: 1.892
batch start
#iterations: 21
currently lose_sum: 98.6566652059555
time_elpased: 1.811
batch start
#iterations: 22
currently lose_sum: 98.73585826158524
time_elpased: 1.899
batch start
#iterations: 23
currently lose_sum: 98.95037168264389
time_elpased: 1.841
batch start
#iterations: 24
currently lose_sum: 98.5323616862297
time_elpased: 1.79
batch start
#iterations: 25
currently lose_sum: 98.61901384592056
time_elpased: 1.794
batch start
#iterations: 26
currently lose_sum: 98.70396447181702
time_elpased: 1.831
batch start
#iterations: 27
currently lose_sum: 98.48890060186386
time_elpased: 1.784
batch start
#iterations: 28
currently lose_sum: 98.43573647737503
time_elpased: 1.861
batch start
#iterations: 29
currently lose_sum: 98.34521496295929
time_elpased: 1.808
batch start
#iterations: 30
currently lose_sum: 98.59338390827179
time_elpased: 1.792
batch start
#iterations: 31
currently lose_sum: 98.35624754428864
time_elpased: 1.801
batch start
#iterations: 32
currently lose_sum: 98.56811988353729
time_elpased: 1.824
batch start
#iterations: 33
currently lose_sum: 98.261787712574
time_elpased: 1.817
batch start
#iterations: 34
currently lose_sum: 98.37823820114136
time_elpased: 1.818
batch start
#iterations: 35
currently lose_sum: 98.1771799325943
time_elpased: 1.81
batch start
#iterations: 36
currently lose_sum: 98.36378866434097
time_elpased: 1.787
batch start
#iterations: 37
currently lose_sum: 98.36772483587265
time_elpased: 1.852
batch start
#iterations: 38
currently lose_sum: 98.1470240354538
time_elpased: 1.805
batch start
#iterations: 39
currently lose_sum: 98.18722993135452
time_elpased: 1.751
start validation test
0.610927835052
0.637330631886
0.517958217557
0.571477234018
0.611091057555
64.212
batch start
#iterations: 40
currently lose_sum: 98.53372889757156
time_elpased: 1.874
batch start
#iterations: 41
currently lose_sum: 98.24239683151245
time_elpased: 1.82
batch start
#iterations: 42
currently lose_sum: 98.3804841041565
time_elpased: 1.825
batch start
#iterations: 43
currently lose_sum: 98.28167283535004
time_elpased: 1.805
batch start
#iterations: 44
currently lose_sum: 98.14415657520294
time_elpased: 1.796
batch start
#iterations: 45
currently lose_sum: 97.98475128412247
time_elpased: 1.879
batch start
#iterations: 46
currently lose_sum: 98.36066281795502
time_elpased: 1.81
batch start
#iterations: 47
currently lose_sum: 98.4975848197937
time_elpased: 1.843
batch start
#iterations: 48
currently lose_sum: 98.38853621482849
time_elpased: 1.766
batch start
#iterations: 49
currently lose_sum: 98.04189920425415
time_elpased: 1.778
batch start
#iterations: 50
currently lose_sum: 98.10339319705963
time_elpased: 1.822
batch start
#iterations: 51
currently lose_sum: 97.9887033700943
time_elpased: 1.873
batch start
#iterations: 52
currently lose_sum: 98.37216877937317
time_elpased: 1.783
batch start
#iterations: 53
currently lose_sum: 97.93470573425293
time_elpased: 1.861
batch start
#iterations: 54
currently lose_sum: 98.07775855064392
time_elpased: 1.807
batch start
#iterations: 55
currently lose_sum: 98.10335755348206
time_elpased: 1.794
batch start
#iterations: 56
currently lose_sum: 98.16655522584915
time_elpased: 1.79
batch start
#iterations: 57
currently lose_sum: 98.07484889030457
time_elpased: 1.869
batch start
#iterations: 58
currently lose_sum: 98.15754574537277
time_elpased: 1.778
batch start
#iterations: 59
currently lose_sum: 98.27949547767639
time_elpased: 1.83
start validation test
0.609020618557
0.646912899669
0.483070906658
0.553113768927
0.609241742702
64.369
batch start
#iterations: 60
currently lose_sum: 98.07472795248032
time_elpased: 1.833
batch start
#iterations: 61
currently lose_sum: 98.3198812007904
time_elpased: 1.795
batch start
#iterations: 62
currently lose_sum: 98.11118322610855
time_elpased: 1.782
batch start
#iterations: 63
currently lose_sum: 98.05902087688446
time_elpased: 1.806
batch start
#iterations: 64
currently lose_sum: 98.37004113197327
time_elpased: 1.766
batch start
#iterations: 65
currently lose_sum: 98.08258771896362
time_elpased: 1.797
batch start
#iterations: 66
currently lose_sum: 98.01810854673386
time_elpased: 1.846
batch start
#iterations: 67
currently lose_sum: 97.83883452415466
time_elpased: 1.803
batch start
#iterations: 68
currently lose_sum: 97.74775558710098
time_elpased: 1.779
batch start
#iterations: 69
currently lose_sum: 98.39578491449356
time_elpased: 1.82
batch start
#iterations: 70
currently lose_sum: 98.06007021665573
time_elpased: 1.773
batch start
#iterations: 71
currently lose_sum: 97.98880195617676
time_elpased: 1.803
batch start
#iterations: 72
currently lose_sum: 97.85527491569519
time_elpased: 1.797
batch start
#iterations: 73
currently lose_sum: 98.07499861717224
time_elpased: 1.828
batch start
#iterations: 74
currently lose_sum: 97.92799025774002
time_elpased: 1.819
batch start
#iterations: 75
currently lose_sum: 97.97722619771957
time_elpased: 1.755
batch start
#iterations: 76
currently lose_sum: 97.84463554620743
time_elpased: 1.794
batch start
#iterations: 77
currently lose_sum: 98.05720633268356
time_elpased: 1.788
batch start
#iterations: 78
currently lose_sum: 98.03070867061615
time_elpased: 1.81
batch start
#iterations: 79
currently lose_sum: 98.0776417851448
time_elpased: 1.794
start validation test
0.624381443299
0.634432396548
0.590099825049
0.611463609704
0.624441629967
63.677
batch start
#iterations: 80
currently lose_sum: 98.00764137506485
time_elpased: 1.829
batch start
#iterations: 81
currently lose_sum: 98.1205062866211
time_elpased: 1.843
batch start
#iterations: 82
currently lose_sum: 98.02679860591888
time_elpased: 1.75
batch start
#iterations: 83
currently lose_sum: 97.78134787082672
time_elpased: 1.747
batch start
#iterations: 84
currently lose_sum: 97.9429360628128
time_elpased: 1.806
batch start
#iterations: 85
currently lose_sum: 98.089286506176
time_elpased: 1.838
batch start
#iterations: 86
currently lose_sum: 97.9582206606865
time_elpased: 1.788
batch start
#iterations: 87
currently lose_sum: 98.08274555206299
time_elpased: 1.763
batch start
#iterations: 88
currently lose_sum: 97.84048879146576
time_elpased: 1.795
batch start
#iterations: 89
currently lose_sum: 98.12984901666641
time_elpased: 1.943
batch start
#iterations: 90
currently lose_sum: 97.85806375741959
time_elpased: 1.81
batch start
#iterations: 91
currently lose_sum: 98.0437303185463
time_elpased: 1.768
batch start
#iterations: 92
currently lose_sum: 97.81678259372711
time_elpased: 1.802
batch start
#iterations: 93
currently lose_sum: 97.59905791282654
time_elpased: 1.783
batch start
#iterations: 94
currently lose_sum: 98.04959601163864
time_elpased: 1.788
batch start
#iterations: 95
currently lose_sum: 98.00463026762009
time_elpased: 1.787
batch start
#iterations: 96
currently lose_sum: 97.90745431184769
time_elpased: 1.857
batch start
#iterations: 97
currently lose_sum: 97.99723571538925
time_elpased: 1.81
batch start
#iterations: 98
currently lose_sum: 98.11183202266693
time_elpased: 1.814
batch start
#iterations: 99
currently lose_sum: 98.0675197839737
time_elpased: 1.856
start validation test
0.618041237113
0.639564428312
0.543995060204
0.587921254588
0.618171236598
63.844
batch start
#iterations: 100
currently lose_sum: 97.89742487668991
time_elpased: 1.795
batch start
#iterations: 101
currently lose_sum: 97.77759540081024
time_elpased: 1.792
batch start
#iterations: 102
currently lose_sum: 97.81896185874939
time_elpased: 1.842
batch start
#iterations: 103
currently lose_sum: 97.81229305267334
time_elpased: 1.844
batch start
#iterations: 104
currently lose_sum: 97.70778995752335
time_elpased: 1.813
batch start
#iterations: 105
currently lose_sum: 97.85592991113663
time_elpased: 1.848
batch start
#iterations: 106
currently lose_sum: 97.86736553907394
time_elpased: 1.798
batch start
#iterations: 107
currently lose_sum: 98.0315552353859
time_elpased: 1.857
batch start
#iterations: 108
currently lose_sum: 97.87872177362442
time_elpased: 1.778
batch start
#iterations: 109
currently lose_sum: 97.81480002403259
time_elpased: 1.778
batch start
#iterations: 110
currently lose_sum: 97.89045917987823
time_elpased: 1.839
batch start
#iterations: 111
currently lose_sum: 97.96430110931396
time_elpased: 1.852
batch start
#iterations: 112
currently lose_sum: 97.91525280475616
time_elpased: 1.798
batch start
#iterations: 113
currently lose_sum: 97.55597418546677
time_elpased: 1.799
batch start
#iterations: 114
currently lose_sum: 97.88355588912964
time_elpased: 1.801
batch start
#iterations: 115
currently lose_sum: 97.85566008090973
time_elpased: 1.798
batch start
#iterations: 116
currently lose_sum: 97.85209268331528
time_elpased: 1.778
batch start
#iterations: 117
currently lose_sum: 97.70402842760086
time_elpased: 1.79
batch start
#iterations: 118
currently lose_sum: 97.70837205648422
time_elpased: 1.792
batch start
#iterations: 119
currently lose_sum: 97.76150047779083
time_elpased: 1.769
start validation test
0.621391752577
0.632307005801
0.583307605228
0.606819763396
0.621458615172
63.625
batch start
#iterations: 120
currently lose_sum: 97.59145390987396
time_elpased: 1.864
batch start
#iterations: 121
currently lose_sum: 97.7557098865509
time_elpased: 1.766
batch start
#iterations: 122
currently lose_sum: 97.71456289291382
time_elpased: 1.807
batch start
#iterations: 123
currently lose_sum: 97.52137267589569
time_elpased: 1.832
batch start
#iterations: 124
currently lose_sum: 97.63197231292725
time_elpased: 1.795
batch start
#iterations: 125
currently lose_sum: 97.74392127990723
time_elpased: 1.803
batch start
#iterations: 126
currently lose_sum: 97.82997423410416
time_elpased: 1.842
batch start
#iterations: 127
currently lose_sum: 97.93860161304474
time_elpased: 1.816
batch start
#iterations: 128
currently lose_sum: 97.62938964366913
time_elpased: 1.816
batch start
#iterations: 129
currently lose_sum: 97.50155431032181
time_elpased: 1.779
batch start
#iterations: 130
currently lose_sum: 97.70745515823364
time_elpased: 1.82
batch start
#iterations: 131
currently lose_sum: 97.6196448802948
time_elpased: 1.809
batch start
#iterations: 132
currently lose_sum: 97.78649365901947
time_elpased: 1.784
batch start
#iterations: 133
currently lose_sum: 97.70049351453781
time_elpased: 1.772
batch start
#iterations: 134
currently lose_sum: 97.67801350355148
time_elpased: 1.793
batch start
#iterations: 135
currently lose_sum: 98.00000715255737
time_elpased: 1.754
batch start
#iterations: 136
currently lose_sum: 97.59409755468369
time_elpased: 1.821
batch start
#iterations: 137
currently lose_sum: 97.78064864873886
time_elpased: 1.756
batch start
#iterations: 138
currently lose_sum: 97.98061740398407
time_elpased: 1.819
batch start
#iterations: 139
currently lose_sum: 97.61125993728638
time_elpased: 1.776
start validation test
0.625257731959
0.631913746631
0.603169702583
0.617207245156
0.625296510901
63.556
batch start
#iterations: 140
currently lose_sum: 97.5636556148529
time_elpased: 1.902
batch start
#iterations: 141
currently lose_sum: 97.60859733819962
time_elpased: 1.889
batch start
#iterations: 142
currently lose_sum: 97.65082830190659
time_elpased: 1.8
batch start
#iterations: 143
currently lose_sum: 97.63933479785919
time_elpased: 1.884
batch start
#iterations: 144
currently lose_sum: 97.680166721344
time_elpased: 1.885
batch start
#iterations: 145
currently lose_sum: 97.99375921487808
time_elpased: 1.866
batch start
#iterations: 146
currently lose_sum: 97.68350923061371
time_elpased: 1.881
batch start
#iterations: 147
currently lose_sum: 97.8276596069336
time_elpased: 1.885
batch start
#iterations: 148
currently lose_sum: 97.6576737165451
time_elpased: 1.825
batch start
#iterations: 149
currently lose_sum: 97.70652478933334
time_elpased: 1.907
batch start
#iterations: 150
currently lose_sum: 97.53943425416946
time_elpased: 1.8
batch start
#iterations: 151
currently lose_sum: 97.78314518928528
time_elpased: 1.811
batch start
#iterations: 152
currently lose_sum: 97.47712987661362
time_elpased: 1.81
batch start
#iterations: 153
currently lose_sum: 97.68357712030411
time_elpased: 1.839
batch start
#iterations: 154
currently lose_sum: 97.592624604702
time_elpased: 1.832
batch start
#iterations: 155
currently lose_sum: 97.79363936185837
time_elpased: 1.831
batch start
#iterations: 156
currently lose_sum: 97.79903584718704
time_elpased: 1.989
batch start
#iterations: 157
currently lose_sum: 97.50106507539749
time_elpased: 1.8
batch start
#iterations: 158
currently lose_sum: 97.67800176143646
time_elpased: 1.812
batch start
#iterations: 159
currently lose_sum: 97.80711060762405
time_elpased: 1.847
start validation test
0.626958762887
0.638238573021
0.589173613255
0.612725424092
0.627025100545
63.556
batch start
#iterations: 160
currently lose_sum: 97.9195014834404
time_elpased: 1.801
batch start
#iterations: 161
currently lose_sum: 97.84387969970703
time_elpased: 1.8
batch start
#iterations: 162
currently lose_sum: 97.54544997215271
time_elpased: 1.773
batch start
#iterations: 163
currently lose_sum: 97.3318458199501
time_elpased: 1.818
batch start
#iterations: 164
currently lose_sum: 97.44282650947571
time_elpased: 1.787
batch start
#iterations: 165
currently lose_sum: 97.75863164663315
time_elpased: 1.789
batch start
#iterations: 166
currently lose_sum: 97.5711944103241
time_elpased: 1.786
batch start
#iterations: 167
currently lose_sum: 97.81918400526047
time_elpased: 1.791
batch start
#iterations: 168
currently lose_sum: 97.68390148878098
time_elpased: 1.805
batch start
#iterations: 169
currently lose_sum: 97.52041983604431
time_elpased: 1.811
batch start
#iterations: 170
currently lose_sum: 97.31445610523224
time_elpased: 1.792
batch start
#iterations: 171
currently lose_sum: 97.59013348817825
time_elpased: 1.822
batch start
#iterations: 172
currently lose_sum: 97.49752008914948
time_elpased: 1.904
batch start
#iterations: 173
currently lose_sum: 97.47855949401855
time_elpased: 1.85
batch start
#iterations: 174
currently lose_sum: 97.56485092639923
time_elpased: 1.781
batch start
#iterations: 175
currently lose_sum: 97.83360350131989
time_elpased: 1.796
batch start
#iterations: 176
currently lose_sum: 97.4289436340332
time_elpased: 1.808
batch start
#iterations: 177
currently lose_sum: 97.57413500547409
time_elpased: 1.825
batch start
#iterations: 178
currently lose_sum: 97.4644324183464
time_elpased: 1.81
batch start
#iterations: 179
currently lose_sum: 97.54720067977905
time_elpased: 1.762
start validation test
0.625
0.637037037037
0.5841309046
0.609437912707
0.625071752001
63.533
batch start
#iterations: 180
currently lose_sum: 97.63590884208679
time_elpased: 1.875
batch start
#iterations: 181
currently lose_sum: 97.62367278337479
time_elpased: 1.859
batch start
#iterations: 182
currently lose_sum: 97.588185608387
time_elpased: 1.835
batch start
#iterations: 183
currently lose_sum: 97.48661035299301
time_elpased: 1.836
batch start
#iterations: 184
currently lose_sum: 97.44547563791275
time_elpased: 1.853
batch start
#iterations: 185
currently lose_sum: 97.45844238996506
time_elpased: 1.817
batch start
#iterations: 186
currently lose_sum: 97.47263842821121
time_elpased: 1.756
batch start
#iterations: 187
currently lose_sum: 97.61041581630707
time_elpased: 1.825
batch start
#iterations: 188
currently lose_sum: 97.5002071261406
time_elpased: 1.948
batch start
#iterations: 189
currently lose_sum: 97.61719340085983
time_elpased: 1.82
batch start
#iterations: 190
currently lose_sum: 97.48929131031036
time_elpased: 1.81
batch start
#iterations: 191
currently lose_sum: 97.51738768815994
time_elpased: 1.783
batch start
#iterations: 192
currently lose_sum: 97.56566834449768
time_elpased: 1.776
batch start
#iterations: 193
currently lose_sum: 97.45988422632217
time_elpased: 1.815
batch start
#iterations: 194
currently lose_sum: 97.52011162042618
time_elpased: 1.84
batch start
#iterations: 195
currently lose_sum: 97.34969317913055
time_elpased: 1.794
batch start
#iterations: 196
currently lose_sum: 97.32352232933044
time_elpased: 1.83
batch start
#iterations: 197
currently lose_sum: 97.8860074877739
time_elpased: 1.799
batch start
#iterations: 198
currently lose_sum: 97.06984823942184
time_elpased: 1.799
batch start
#iterations: 199
currently lose_sum: 97.30855298042297
time_elpased: 1.825
start validation test
0.625103092784
0.640492067142
0.57332510034
0.60505023079
0.625193997035
63.630
batch start
#iterations: 200
currently lose_sum: 97.37888270616531
time_elpased: 1.919
batch start
#iterations: 201
currently lose_sum: 97.53250312805176
time_elpased: 1.825
batch start
#iterations: 202
currently lose_sum: 97.28771889209747
time_elpased: 1.814
batch start
#iterations: 203
currently lose_sum: 97.61077219247818
time_elpased: 1.923
batch start
#iterations: 204
currently lose_sum: 97.4820077419281
time_elpased: 1.801
batch start
#iterations: 205
currently lose_sum: 97.3454801440239
time_elpased: 1.913
batch start
#iterations: 206
currently lose_sum: 97.4530034661293
time_elpased: 1.859
batch start
#iterations: 207
currently lose_sum: 97.71545326709747
time_elpased: 1.794
batch start
#iterations: 208
currently lose_sum: 97.34196615219116
time_elpased: 1.865
batch start
#iterations: 209
currently lose_sum: 97.1696954369545
time_elpased: 1.87
batch start
#iterations: 210
currently lose_sum: 97.4491406083107
time_elpased: 1.792
batch start
#iterations: 211
currently lose_sum: 97.47447335720062
time_elpased: 1.774
batch start
#iterations: 212
currently lose_sum: 97.46021658182144
time_elpased: 1.773
batch start
#iterations: 213
currently lose_sum: 97.36803418397903
time_elpased: 1.804
batch start
#iterations: 214
currently lose_sum: 97.51418572664261
time_elpased: 1.782
batch start
#iterations: 215
currently lose_sum: 97.18096596002579
time_elpased: 1.775
batch start
#iterations: 216
currently lose_sum: 97.42882108688354
time_elpased: 1.865
batch start
#iterations: 217
currently lose_sum: 97.43929374217987
time_elpased: 1.816
batch start
#iterations: 218
currently lose_sum: 97.37992888689041
time_elpased: 1.953
batch start
#iterations: 219
currently lose_sum: 97.43302434682846
time_elpased: 1.827
start validation test
0.627835051546
0.640422899561
0.585983328188
0.611994840929
0.627908528702
63.640
batch start
#iterations: 220
currently lose_sum: 97.32295978069305
time_elpased: 1.881
batch start
#iterations: 221
currently lose_sum: 97.28870558738708
time_elpased: 1.837
batch start
#iterations: 222
currently lose_sum: 97.42255610227585
time_elpased: 1.798
batch start
#iterations: 223
currently lose_sum: 97.66129386425018
time_elpased: 1.846
batch start
#iterations: 224
currently lose_sum: 97.671965777874
time_elpased: 1.802
batch start
#iterations: 225
currently lose_sum: 97.38152039051056
time_elpased: 1.795
batch start
#iterations: 226
currently lose_sum: 97.22614979743958
time_elpased: 1.797
batch start
#iterations: 227
currently lose_sum: 97.16811293363571
time_elpased: 1.889
batch start
#iterations: 228
currently lose_sum: 97.48969572782516
time_elpased: 1.798
batch start
#iterations: 229
currently lose_sum: 97.402900993824
time_elpased: 1.821
batch start
#iterations: 230
currently lose_sum: 97.49532747268677
time_elpased: 1.819
batch start
#iterations: 231
currently lose_sum: 97.40753972530365
time_elpased: 1.793
batch start
#iterations: 232
currently lose_sum: 97.64644038677216
time_elpased: 1.832
batch start
#iterations: 233
currently lose_sum: 97.50748687982559
time_elpased: 1.776
batch start
#iterations: 234
currently lose_sum: 97.04409778118134
time_elpased: 1.832
batch start
#iterations: 235
currently lose_sum: 97.78322339057922
time_elpased: 1.83
batch start
#iterations: 236
currently lose_sum: 97.27260392904282
time_elpased: 1.901
batch start
#iterations: 237
currently lose_sum: 97.38540595769882
time_elpased: 1.785
batch start
#iterations: 238
currently lose_sum: 97.37583965063095
time_elpased: 1.869
batch start
#iterations: 239
currently lose_sum: 97.24147689342499
time_elpased: 1.783
start validation test
0.630103092784
0.642352941176
0.589996912627
0.615062761506
0.630173505369
63.519
batch start
#iterations: 240
currently lose_sum: 97.61453646421432
time_elpased: 1.86
batch start
#iterations: 241
currently lose_sum: 97.7239933013916
time_elpased: 1.792
batch start
#iterations: 242
currently lose_sum: 97.31535792350769
time_elpased: 1.81
batch start
#iterations: 243
currently lose_sum: 97.79629856348038
time_elpased: 1.767
batch start
#iterations: 244
currently lose_sum: 97.64660757780075
time_elpased: 1.819
batch start
#iterations: 245
currently lose_sum: 97.35980540513992
time_elpased: 1.836
batch start
#iterations: 246
currently lose_sum: 97.81898581981659
time_elpased: 1.847
batch start
#iterations: 247
currently lose_sum: 97.2662405371666
time_elpased: 1.847
batch start
#iterations: 248
currently lose_sum: 97.57395386695862
time_elpased: 1.819
batch start
#iterations: 249
currently lose_sum: 97.22964286804199
time_elpased: 1.778
batch start
#iterations: 250
currently lose_sum: 97.11914831399918
time_elpased: 1.85
batch start
#iterations: 251
currently lose_sum: 97.32559591531754
time_elpased: 1.834
batch start
#iterations: 252
currently lose_sum: 97.27583861351013
time_elpased: 1.865
batch start
#iterations: 253
currently lose_sum: 97.46354985237122
time_elpased: 1.902
batch start
#iterations: 254
currently lose_sum: 97.40674251317978
time_elpased: 1.808
batch start
#iterations: 255
currently lose_sum: 97.41961014270782
time_elpased: 1.847
batch start
#iterations: 256
currently lose_sum: 97.05706214904785
time_elpased: 1.761
batch start
#iterations: 257
currently lose_sum: 97.40015345811844
time_elpased: 1.824
batch start
#iterations: 258
currently lose_sum: 97.19312238693237
time_elpased: 1.836
batch start
#iterations: 259
currently lose_sum: 97.3089474439621
time_elpased: 1.807
start validation test
0.625979381443
0.64074116436
0.576515385407
0.606933911159
0.626066223118
63.637
batch start
#iterations: 260
currently lose_sum: 97.23913311958313
time_elpased: 1.816
batch start
#iterations: 261
currently lose_sum: 97.32379221916199
time_elpased: 1.858
batch start
#iterations: 262
currently lose_sum: 97.23969489336014
time_elpased: 1.795
batch start
#iterations: 263
currently lose_sum: 97.30700618028641
time_elpased: 1.817
batch start
#iterations: 264
currently lose_sum: 97.56290543079376
time_elpased: 1.85
batch start
#iterations: 265
currently lose_sum: 97.61509162187576
time_elpased: 1.897
batch start
#iterations: 266
currently lose_sum: 97.31278932094574
time_elpased: 1.824
batch start
#iterations: 267
currently lose_sum: 97.30216836929321
time_elpased: 1.839
batch start
#iterations: 268
currently lose_sum: 97.26394063234329
time_elpased: 1.834
batch start
#iterations: 269
currently lose_sum: 97.66050273180008
time_elpased: 1.795
batch start
#iterations: 270
currently lose_sum: 97.4055568575859
time_elpased: 1.872
batch start
#iterations: 271
currently lose_sum: 97.4476118683815
time_elpased: 1.822
batch start
#iterations: 272
currently lose_sum: 97.4588378071785
time_elpased: 1.891
batch start
#iterations: 273
currently lose_sum: 97.43526411056519
time_elpased: 1.842
batch start
#iterations: 274
currently lose_sum: 97.51779961585999
time_elpased: 1.817
batch start
#iterations: 275
currently lose_sum: 97.52720183134079
time_elpased: 1.781
batch start
#iterations: 276
currently lose_sum: 97.32230013608932
time_elpased: 1.815
batch start
#iterations: 277
currently lose_sum: 97.72844392061234
time_elpased: 1.87
batch start
#iterations: 278
currently lose_sum: 97.2694399356842
time_elpased: 1.802
batch start
#iterations: 279
currently lose_sum: 97.35565149784088
time_elpased: 1.868
start validation test
0.627371134021
0.641685649203
0.579808582896
0.609179867005
0.627454637415
63.713
batch start
#iterations: 280
currently lose_sum: 97.581651866436
time_elpased: 1.84
batch start
#iterations: 281
currently lose_sum: 97.00862383842468
time_elpased: 1.804
batch start
#iterations: 282
currently lose_sum: 97.3275973200798
time_elpased: 1.836
batch start
#iterations: 283
currently lose_sum: 97.35668158531189
time_elpased: 1.803
batch start
#iterations: 284
currently lose_sum: 97.30095648765564
time_elpased: 1.871
batch start
#iterations: 285
currently lose_sum: 97.21502882242203
time_elpased: 1.87
batch start
#iterations: 286
currently lose_sum: 97.4308939576149
time_elpased: 1.944
batch start
#iterations: 287
currently lose_sum: 97.20014643669128
time_elpased: 1.853
batch start
#iterations: 288
currently lose_sum: 97.30443638563156
time_elpased: 1.82
batch start
#iterations: 289
currently lose_sum: 97.2775656580925
time_elpased: 1.763
batch start
#iterations: 290
currently lose_sum: 97.32467877864838
time_elpased: 1.827
batch start
#iterations: 291
currently lose_sum: 97.15816432237625
time_elpased: 1.864
batch start
#iterations: 292
currently lose_sum: 97.39069885015488
time_elpased: 1.827
batch start
#iterations: 293
currently lose_sum: 97.29925328493118
time_elpased: 1.775
batch start
#iterations: 294
currently lose_sum: 97.27247315645218
time_elpased: 1.85
batch start
#iterations: 295
currently lose_sum: 97.28281491994858
time_elpased: 1.764
batch start
#iterations: 296
currently lose_sum: 97.23690974712372
time_elpased: 1.775
batch start
#iterations: 297
currently lose_sum: 97.7287049293518
time_elpased: 1.751
batch start
#iterations: 298
currently lose_sum: 97.28909891843796
time_elpased: 1.782
batch start
#iterations: 299
currently lose_sum: 97.2258728146553
time_elpased: 1.813
start validation test
0.630103092784
0.63080407701
0.63054440671
0.630674215131
0.630102317989
63.322
batch start
#iterations: 300
currently lose_sum: 97.15207010507584
time_elpased: 2.04
batch start
#iterations: 301
currently lose_sum: 97.2759085893631
time_elpased: 1.906
batch start
#iterations: 302
currently lose_sum: 97.53034937381744
time_elpased: 1.856
batch start
#iterations: 303
currently lose_sum: 97.27692383527756
time_elpased: 1.896
batch start
#iterations: 304
currently lose_sum: 97.27087366580963
time_elpased: 1.87
batch start
#iterations: 305
currently lose_sum: 97.22303289175034
time_elpased: 1.867
batch start
#iterations: 306
currently lose_sum: 97.22068721055984
time_elpased: 1.855
batch start
#iterations: 307
currently lose_sum: 97.01273518800735
time_elpased: 1.854
batch start
#iterations: 308
currently lose_sum: 97.56690698862076
time_elpased: 1.822
batch start
#iterations: 309
currently lose_sum: 97.45133274793625
time_elpased: 1.863
batch start
#iterations: 310
currently lose_sum: 97.3848015666008
time_elpased: 1.819
batch start
#iterations: 311
currently lose_sum: 97.24001222848892
time_elpased: 1.828
batch start
#iterations: 312
currently lose_sum: 97.45999956130981
time_elpased: 1.85
batch start
#iterations: 313
currently lose_sum: 97.26104217767715
time_elpased: 1.868
batch start
#iterations: 314
currently lose_sum: 97.14382308721542
time_elpased: 1.844
batch start
#iterations: 315
currently lose_sum: 97.17375195026398
time_elpased: 1.772
batch start
#iterations: 316
currently lose_sum: 96.9240557551384
time_elpased: 1.812
batch start
#iterations: 317
currently lose_sum: 97.30947864055634
time_elpased: 1.825
batch start
#iterations: 318
currently lose_sum: 97.36862301826477
time_elpased: 1.816
batch start
#iterations: 319
currently lose_sum: 97.31439250707626
time_elpased: 1.796
start validation test
0.62881443299
0.635181603267
0.60831532366
0.621458234768
0.628850422338
63.486
batch start
#iterations: 320
currently lose_sum: 97.34231734275818
time_elpased: 1.82
batch start
#iterations: 321
currently lose_sum: 97.04297661781311
time_elpased: 1.848
batch start
#iterations: 322
currently lose_sum: 97.43078780174255
time_elpased: 1.908
batch start
#iterations: 323
currently lose_sum: 96.86065226793289
time_elpased: 1.804
batch start
#iterations: 324
currently lose_sum: 97.4244299530983
time_elpased: 1.865
batch start
#iterations: 325
currently lose_sum: 97.37013375759125
time_elpased: 1.804
batch start
#iterations: 326
currently lose_sum: 97.51714044809341
time_elpased: 1.842
batch start
#iterations: 327
currently lose_sum: 97.00462090969086
time_elpased: 1.781
batch start
#iterations: 328
currently lose_sum: 97.32232797145844
time_elpased: 1.831
batch start
#iterations: 329
currently lose_sum: 97.15470266342163
time_elpased: 1.816
batch start
#iterations: 330
currently lose_sum: 97.14819520711899
time_elpased: 1.846
batch start
#iterations: 331
currently lose_sum: 97.2614312171936
time_elpased: 1.815
batch start
#iterations: 332
currently lose_sum: 97.32350957393646
time_elpased: 1.858
batch start
#iterations: 333
currently lose_sum: 97.4122177362442
time_elpased: 1.869
batch start
#iterations: 334
currently lose_sum: 97.54405385255814
time_elpased: 1.849
batch start
#iterations: 335
currently lose_sum: 97.16539466381073
time_elpased: 1.846
batch start
#iterations: 336
currently lose_sum: 97.41562610864639
time_elpased: 1.799
batch start
#iterations: 337
currently lose_sum: 97.40538990497589
time_elpased: 1.804
batch start
#iterations: 338
currently lose_sum: 97.36469340324402
time_elpased: 1.884
batch start
#iterations: 339
currently lose_sum: 97.04842215776443
time_elpased: 1.813
start validation test
0.631082474227
0.635507092949
0.61778326644
0.626519855972
0.631105823037
63.413
batch start
#iterations: 340
currently lose_sum: 97.20379549264908
time_elpased: 1.872
batch start
#iterations: 341
currently lose_sum: 97.65583181381226
time_elpased: 1.807
batch start
#iterations: 342
currently lose_sum: 97.26953810453415
time_elpased: 1.815
batch start
#iterations: 343
currently lose_sum: 97.27505493164062
time_elpased: 1.784
batch start
#iterations: 344
currently lose_sum: 97.21057337522507
time_elpased: 1.814
batch start
#iterations: 345
currently lose_sum: 97.35324513912201
time_elpased: 1.895
batch start
#iterations: 346
currently lose_sum: 97.24552375078201
time_elpased: 1.77
batch start
#iterations: 347
currently lose_sum: 96.93661916255951
time_elpased: 1.843
batch start
#iterations: 348
currently lose_sum: 97.62968146800995
time_elpased: 1.794
batch start
#iterations: 349
currently lose_sum: 97.27404004335403
time_elpased: 1.78
batch start
#iterations: 350
currently lose_sum: 97.21257984638214
time_elpased: 1.791
batch start
#iterations: 351
currently lose_sum: 97.26983785629272
time_elpased: 1.845
batch start
#iterations: 352
currently lose_sum: 97.08269959688187
time_elpased: 1.81
batch start
#iterations: 353
currently lose_sum: 97.2765845656395
time_elpased: 1.912
batch start
#iterations: 354
currently lose_sum: 97.22206395864487
time_elpased: 1.907
batch start
#iterations: 355
currently lose_sum: 97.05928242206573
time_elpased: 1.87
batch start
#iterations: 356
currently lose_sum: 97.29784822463989
time_elpased: 1.834
batch start
#iterations: 357
currently lose_sum: 97.32076066732407
time_elpased: 1.819
batch start
#iterations: 358
currently lose_sum: 97.22537899017334
time_elpased: 1.902
batch start
#iterations: 359
currently lose_sum: 97.29787069559097
time_elpased: 1.861
start validation test
0.623762886598
0.640712290503
0.566532880519
0.601343601507
0.623863362701
63.636
batch start
#iterations: 360
currently lose_sum: 97.16471475362778
time_elpased: 1.822
batch start
#iterations: 361
currently lose_sum: 97.49863183498383
time_elpased: 1.772
batch start
#iterations: 362
currently lose_sum: 97.43937242031097
time_elpased: 1.875
batch start
#iterations: 363
currently lose_sum: 97.25827252864838
time_elpased: 1.797
batch start
#iterations: 364
currently lose_sum: 96.9213205575943
time_elpased: 1.795
batch start
#iterations: 365
currently lose_sum: 97.24923354387283
time_elpased: 1.8
batch start
#iterations: 366
currently lose_sum: 97.3330197930336
time_elpased: 1.838
batch start
#iterations: 367
currently lose_sum: 97.21168220043182
time_elpased: 1.77
batch start
#iterations: 368
currently lose_sum: 97.34042286872864
time_elpased: 1.803
batch start
#iterations: 369
currently lose_sum: 97.34156095981598
time_elpased: 1.831
batch start
#iterations: 370
currently lose_sum: 97.21726387739182
time_elpased: 1.864
batch start
#iterations: 371
currently lose_sum: 97.14082372188568
time_elpased: 1.813
batch start
#iterations: 372
currently lose_sum: 97.21629756689072
time_elpased: 1.829
batch start
#iterations: 373
currently lose_sum: 97.28653764724731
time_elpased: 1.798
batch start
#iterations: 374
currently lose_sum: 97.21264898777008
time_elpased: 1.814
batch start
#iterations: 375
currently lose_sum: 97.02979648113251
time_elpased: 1.828
batch start
#iterations: 376
currently lose_sum: 97.14875626564026
time_elpased: 1.851
batch start
#iterations: 377
currently lose_sum: 97.33445793390274
time_elpased: 1.81
batch start
#iterations: 378
currently lose_sum: 97.23937946557999
time_elpased: 1.795
batch start
#iterations: 379
currently lose_sum: 97.51191675662994
time_elpased: 1.901
start validation test
0.630051546392
0.63870685889
0.601831841103
0.619721294972
0.630101090437
63.586
batch start
#iterations: 380
currently lose_sum: 97.39009922742844
time_elpased: 1.882
batch start
#iterations: 381
currently lose_sum: 97.15999692678452
time_elpased: 1.849
batch start
#iterations: 382
currently lose_sum: 96.901602268219
time_elpased: 1.876
batch start
#iterations: 383
currently lose_sum: 96.97820502519608
time_elpased: 1.843
batch start
#iterations: 384
currently lose_sum: 97.15773469209671
time_elpased: 1.853
batch start
#iterations: 385
currently lose_sum: 97.7120081782341
time_elpased: 1.797
batch start
#iterations: 386
currently lose_sum: 97.3990997672081
time_elpased: 1.774
batch start
#iterations: 387
currently lose_sum: 97.18296301364899
time_elpased: 1.882
batch start
#iterations: 388
currently lose_sum: 97.36486089229584
time_elpased: 1.852
batch start
#iterations: 389
currently lose_sum: 97.12131029367447
time_elpased: 1.917
batch start
#iterations: 390
currently lose_sum: 97.35437154769897
time_elpased: 1.91
batch start
#iterations: 391
currently lose_sum: 97.4248057603836
time_elpased: 1.853
batch start
#iterations: 392
currently lose_sum: 97.28334558010101
time_elpased: 1.864
batch start
#iterations: 393
currently lose_sum: 97.24409258365631
time_elpased: 1.83
batch start
#iterations: 394
currently lose_sum: 97.05116945505142
time_elpased: 1.77
batch start
#iterations: 395
currently lose_sum: 96.96685516834259
time_elpased: 1.806
batch start
#iterations: 396
currently lose_sum: 97.18702554702759
time_elpased: 1.853
batch start
#iterations: 397
currently lose_sum: 97.54253953695297
time_elpased: 1.898
batch start
#iterations: 398
currently lose_sum: 97.15524250268936
time_elpased: 1.77
batch start
#iterations: 399
currently lose_sum: 97.265480697155
time_elpased: 1.778
start validation test
0.622989690722
0.644846292948
0.55047854276
0.593937375083
0.623116995226
63.716
acc: 0.632
pre: 0.633
rec: 0.633
F1: 0.633
auc: 0.632
