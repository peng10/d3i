start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.73307263851166
time_elpased: 1.816
batch start
#iterations: 1
currently lose_sum: 100.43890923261642
time_elpased: 1.763
batch start
#iterations: 2
currently lose_sum: 100.14708912372589
time_elpased: 1.804
batch start
#iterations: 3
currently lose_sum: 99.99286204576492
time_elpased: 1.785
batch start
#iterations: 4
currently lose_sum: 99.8724821805954
time_elpased: 1.728
batch start
#iterations: 5
currently lose_sum: 99.87960970401764
time_elpased: 1.758
batch start
#iterations: 6
currently lose_sum: 99.60644316673279
time_elpased: 1.767
batch start
#iterations: 7
currently lose_sum: 99.55147808790207
time_elpased: 1.752
batch start
#iterations: 8
currently lose_sum: 99.59816616773605
time_elpased: 1.761
batch start
#iterations: 9
currently lose_sum: 99.23536539077759
time_elpased: 1.742
batch start
#iterations: 10
currently lose_sum: 99.29259210824966
time_elpased: 1.795
batch start
#iterations: 11
currently lose_sum: 99.37367987632751
time_elpased: 1.746
batch start
#iterations: 12
currently lose_sum: 99.15776860713959
time_elpased: 1.76
batch start
#iterations: 13
currently lose_sum: 99.23826944828033
time_elpased: 1.733
batch start
#iterations: 14
currently lose_sum: 98.91643464565277
time_elpased: 1.777
batch start
#iterations: 15
currently lose_sum: 98.9820026755333
time_elpased: 1.751
batch start
#iterations: 16
currently lose_sum: 99.01298642158508
time_elpased: 1.781
batch start
#iterations: 17
currently lose_sum: 98.99518638849258
time_elpased: 1.743
batch start
#iterations: 18
currently lose_sum: 98.73541963100433
time_elpased: 1.758
batch start
#iterations: 19
currently lose_sum: 98.87795716524124
time_elpased: 1.76
start validation test
0.618041237113
0.627107438017
0.585674590923
0.605683269476
0.618098061753
64.418
batch start
#iterations: 20
currently lose_sum: 98.85695213079453
time_elpased: 1.827
batch start
#iterations: 21
currently lose_sum: 98.69331860542297
time_elpased: 1.782
batch start
#iterations: 22
currently lose_sum: 98.77967059612274
time_elpased: 1.799
batch start
#iterations: 23
currently lose_sum: 98.96901035308838
time_elpased: 1.733
batch start
#iterations: 24
currently lose_sum: 98.55147802829742
time_elpased: 1.731
batch start
#iterations: 25
currently lose_sum: 98.66228145360947
time_elpased: 1.755
batch start
#iterations: 26
currently lose_sum: 98.71185439825058
time_elpased: 1.736
batch start
#iterations: 27
currently lose_sum: 98.51031613349915
time_elpased: 1.771
batch start
#iterations: 28
currently lose_sum: 98.48009610176086
time_elpased: 1.741
batch start
#iterations: 29
currently lose_sum: 98.39991420507431
time_elpased: 1.78
batch start
#iterations: 30
currently lose_sum: 98.59684509038925
time_elpased: 1.8
batch start
#iterations: 31
currently lose_sum: 98.3375568985939
time_elpased: 1.761
batch start
#iterations: 32
currently lose_sum: 98.55580139160156
time_elpased: 1.738
batch start
#iterations: 33
currently lose_sum: 98.28676325082779
time_elpased: 1.761
batch start
#iterations: 34
currently lose_sum: 98.33770912885666
time_elpased: 1.82
batch start
#iterations: 35
currently lose_sum: 98.18305152654648
time_elpased: 1.754
batch start
#iterations: 36
currently lose_sum: 98.40577667951584
time_elpased: 1.795
batch start
#iterations: 37
currently lose_sum: 98.39615190029144
time_elpased: 1.826
batch start
#iterations: 38
currently lose_sum: 98.17832064628601
time_elpased: 1.739
batch start
#iterations: 39
currently lose_sum: 98.193818628788
time_elpased: 1.793
start validation test
0.619793814433
0.639129917984
0.553360090563
0.593160507446
0.619910449082
64.003
batch start
#iterations: 40
currently lose_sum: 98.55073434114456
time_elpased: 1.823
batch start
#iterations: 41
currently lose_sum: 98.24588429927826
time_elpased: 1.792
batch start
#iterations: 42
currently lose_sum: 98.35563284158707
time_elpased: 1.746
batch start
#iterations: 43
currently lose_sum: 98.35385990142822
time_elpased: 1.761
batch start
#iterations: 44
currently lose_sum: 98.18280124664307
time_elpased: 1.8
batch start
#iterations: 45
currently lose_sum: 97.98385107517242
time_elpased: 1.753
batch start
#iterations: 46
currently lose_sum: 98.35453540086746
time_elpased: 1.767
batch start
#iterations: 47
currently lose_sum: 98.49654370546341
time_elpased: 1.751
batch start
#iterations: 48
currently lose_sum: 98.34569674730301
time_elpased: 1.792
batch start
#iterations: 49
currently lose_sum: 98.08903855085373
time_elpased: 1.766
batch start
#iterations: 50
currently lose_sum: 98.10970330238342
time_elpased: 1.747
batch start
#iterations: 51
currently lose_sum: 97.94921684265137
time_elpased: 1.749
batch start
#iterations: 52
currently lose_sum: 98.35436427593231
time_elpased: 1.777
batch start
#iterations: 53
currently lose_sum: 97.91987657546997
time_elpased: 1.836
batch start
#iterations: 54
currently lose_sum: 98.09702426195145
time_elpased: 1.804
batch start
#iterations: 55
currently lose_sum: 98.08721953630447
time_elpased: 1.808
batch start
#iterations: 56
currently lose_sum: 98.20055836439133
time_elpased: 1.783
batch start
#iterations: 57
currently lose_sum: 98.02266997098923
time_elpased: 1.763
batch start
#iterations: 58
currently lose_sum: 98.1344239115715
time_elpased: 1.767
batch start
#iterations: 59
currently lose_sum: 98.2437893152237
time_elpased: 1.765
start validation test
0.608711340206
0.647638888889
0.479880621591
0.551279777738
0.608937522403
64.244
batch start
#iterations: 60
currently lose_sum: 98.08327668905258
time_elpased: 1.811
batch start
#iterations: 61
currently lose_sum: 98.3187073469162
time_elpased: 1.755
batch start
#iterations: 62
currently lose_sum: 98.09739315509796
time_elpased: 1.768
batch start
#iterations: 63
currently lose_sum: 97.9981330037117
time_elpased: 1.772
batch start
#iterations: 64
currently lose_sum: 98.34585922956467
time_elpased: 1.759
batch start
#iterations: 65
currently lose_sum: 98.04117584228516
time_elpased: 1.835
batch start
#iterations: 66
currently lose_sum: 98.04484504461288
time_elpased: 1.758
batch start
#iterations: 67
currently lose_sum: 97.75614148378372
time_elpased: 1.773
batch start
#iterations: 68
currently lose_sum: 97.74163740873337
time_elpased: 1.748
batch start
#iterations: 69
currently lose_sum: 98.43372708559036
time_elpased: 1.913
batch start
#iterations: 70
currently lose_sum: 98.0610288977623
time_elpased: 1.787
batch start
#iterations: 71
currently lose_sum: 98.0076647400856
time_elpased: 1.773
batch start
#iterations: 72
currently lose_sum: 97.84247386455536
time_elpased: 1.753
batch start
#iterations: 73
currently lose_sum: 98.0374926328659
time_elpased: 1.769
batch start
#iterations: 74
currently lose_sum: 97.90898180007935
time_elpased: 1.767
batch start
#iterations: 75
currently lose_sum: 97.98847949504852
time_elpased: 1.709
batch start
#iterations: 76
currently lose_sum: 97.84762370586395
time_elpased: 1.811
batch start
#iterations: 77
currently lose_sum: 98.10520428419113
time_elpased: 1.738
batch start
#iterations: 78
currently lose_sum: 98.04352855682373
time_elpased: 1.789
batch start
#iterations: 79
currently lose_sum: 98.04612332582474
time_elpased: 1.805
start validation test
0.626134020619
0.63411713477
0.599464855408
0.616304290324
0.626180842452
63.549
batch start
#iterations: 80
currently lose_sum: 98.03172361850739
time_elpased: 1.812
batch start
#iterations: 81
currently lose_sum: 98.0861446261406
time_elpased: 1.82
batch start
#iterations: 82
currently lose_sum: 97.98527729511261
time_elpased: 1.78
batch start
#iterations: 83
currently lose_sum: 97.757029235363
time_elpased: 1.788
batch start
#iterations: 84
currently lose_sum: 97.97378987073898
time_elpased: 1.763
batch start
#iterations: 85
currently lose_sum: 98.08577871322632
time_elpased: 1.734
batch start
#iterations: 86
currently lose_sum: 97.88798993825912
time_elpased: 1.754
batch start
#iterations: 87
currently lose_sum: 98.09189623594284
time_elpased: 1.879
batch start
#iterations: 88
currently lose_sum: 97.77417206764221
time_elpased: 1.76
batch start
#iterations: 89
currently lose_sum: 98.05274373292923
time_elpased: 1.741
batch start
#iterations: 90
currently lose_sum: 97.83324563503265
time_elpased: 1.77
batch start
#iterations: 91
currently lose_sum: 98.03384631872177
time_elpased: 1.763
batch start
#iterations: 92
currently lose_sum: 97.73812311887741
time_elpased: 1.769
batch start
#iterations: 93
currently lose_sum: 97.59231376647949
time_elpased: 1.774
batch start
#iterations: 94
currently lose_sum: 98.09092670679092
time_elpased: 1.718
batch start
#iterations: 95
currently lose_sum: 97.9989904165268
time_elpased: 1.771
batch start
#iterations: 96
currently lose_sum: 97.88202548027039
time_elpased: 1.757
batch start
#iterations: 97
currently lose_sum: 97.9523383975029
time_elpased: 1.761
batch start
#iterations: 98
currently lose_sum: 98.12853902578354
time_elpased: 1.758
batch start
#iterations: 99
currently lose_sum: 98.02931571006775
time_elpased: 1.846
start validation test
0.619381443299
0.642970952323
0.539878563343
0.586932199597
0.619521022867
63.766
batch start
#iterations: 100
currently lose_sum: 97.81439107656479
time_elpased: 1.782
batch start
#iterations: 101
currently lose_sum: 97.71357321739197
time_elpased: 1.762
batch start
#iterations: 102
currently lose_sum: 97.79651767015457
time_elpased: 1.76
batch start
#iterations: 103
currently lose_sum: 97.79437762498856
time_elpased: 1.781
batch start
#iterations: 104
currently lose_sum: 97.70208877325058
time_elpased: 1.765
batch start
#iterations: 105
currently lose_sum: 97.79335027933121
time_elpased: 1.798
batch start
#iterations: 106
currently lose_sum: 97.84724056720734
time_elpased: 1.754
batch start
#iterations: 107
currently lose_sum: 97.98779720067978
time_elpased: 1.725
batch start
#iterations: 108
currently lose_sum: 97.91938680410385
time_elpased: 1.762
batch start
#iterations: 109
currently lose_sum: 97.78270882368088
time_elpased: 1.79
batch start
#iterations: 110
currently lose_sum: 97.78545159101486
time_elpased: 1.773
batch start
#iterations: 111
currently lose_sum: 97.96813982725143
time_elpased: 1.758
batch start
#iterations: 112
currently lose_sum: 97.84503376483917
time_elpased: 1.749
batch start
#iterations: 113
currently lose_sum: 97.53726357221603
time_elpased: 1.763
batch start
#iterations: 114
currently lose_sum: 97.77112078666687
time_elpased: 1.767
batch start
#iterations: 115
currently lose_sum: 97.81224656105042
time_elpased: 1.734
batch start
#iterations: 116
currently lose_sum: 97.80299085378647
time_elpased: 1.752
batch start
#iterations: 117
currently lose_sum: 97.60755831003189
time_elpased: 1.764
batch start
#iterations: 118
currently lose_sum: 97.65897285938263
time_elpased: 1.787
batch start
#iterations: 119
currently lose_sum: 97.70580822229385
time_elpased: 1.745
start validation test
0.623865979381
0.635377041844
0.584439641865
0.608844813723
0.623935198398
63.490
batch start
#iterations: 120
currently lose_sum: 97.5643864274025
time_elpased: 1.794
batch start
#iterations: 121
currently lose_sum: 97.74315869808197
time_elpased: 1.776
batch start
#iterations: 122
currently lose_sum: 97.69425135850906
time_elpased: 1.732
batch start
#iterations: 123
currently lose_sum: 97.4737401008606
time_elpased: 1.777
batch start
#iterations: 124
currently lose_sum: 97.61776852607727
time_elpased: 1.83
batch start
#iterations: 125
currently lose_sum: 97.63830405473709
time_elpased: 1.729
batch start
#iterations: 126
currently lose_sum: 97.76499837636948
time_elpased: 1.831
batch start
#iterations: 127
currently lose_sum: 97.9193611741066
time_elpased: 1.815
batch start
#iterations: 128
currently lose_sum: 97.60920238494873
time_elpased: 1.782
batch start
#iterations: 129
currently lose_sum: 97.45658564567566
time_elpased: 1.754
batch start
#iterations: 130
currently lose_sum: 97.75193041563034
time_elpased: 1.768
batch start
#iterations: 131
currently lose_sum: 97.55510437488556
time_elpased: 1.783
batch start
#iterations: 132
currently lose_sum: 97.78258806467056
time_elpased: 1.783
batch start
#iterations: 133
currently lose_sum: 97.68704015016556
time_elpased: 1.761
batch start
#iterations: 134
currently lose_sum: 97.63969141244888
time_elpased: 1.803
batch start
#iterations: 135
currently lose_sum: 97.92036360502243
time_elpased: 1.852
batch start
#iterations: 136
currently lose_sum: 97.5623368024826
time_elpased: 1.798
batch start
#iterations: 137
currently lose_sum: 97.83376389741898
time_elpased: 1.817
batch start
#iterations: 138
currently lose_sum: 97.90703904628754
time_elpased: 1.772
batch start
#iterations: 139
currently lose_sum: 97.54172736406326
time_elpased: 1.783
start validation test
0.629020618557
0.636897001304
0.603272615005
0.619628983669
0.629065823148
63.402
batch start
#iterations: 140
currently lose_sum: 97.52971231937408
time_elpased: 1.853
batch start
#iterations: 141
currently lose_sum: 97.61434108018875
time_elpased: 1.804
batch start
#iterations: 142
currently lose_sum: 97.63079595565796
time_elpased: 1.747
batch start
#iterations: 143
currently lose_sum: 97.61506634950638
time_elpased: 1.786
batch start
#iterations: 144
currently lose_sum: 97.63362520933151
time_elpased: 1.796
batch start
#iterations: 145
currently lose_sum: 98.0127934217453
time_elpased: 1.891
batch start
#iterations: 146
currently lose_sum: 97.64171177148819
time_elpased: 1.753
batch start
#iterations: 147
currently lose_sum: 97.76412057876587
time_elpased: 1.739
batch start
#iterations: 148
currently lose_sum: 97.61846572160721
time_elpased: 1.778
batch start
#iterations: 149
currently lose_sum: 97.68713819980621
time_elpased: 1.782
batch start
#iterations: 150
currently lose_sum: 97.42133444547653
time_elpased: 1.749
batch start
#iterations: 151
currently lose_sum: 97.65510350465775
time_elpased: 1.737
batch start
#iterations: 152
currently lose_sum: 97.38731455802917
time_elpased: 1.739
batch start
#iterations: 153
currently lose_sum: 97.65226662158966
time_elpased: 1.781
batch start
#iterations: 154
currently lose_sum: 97.59059995412827
time_elpased: 1.765
batch start
#iterations: 155
currently lose_sum: 97.66470927000046
time_elpased: 1.788
batch start
#iterations: 156
currently lose_sum: 97.72131901979446
time_elpased: 1.722
batch start
#iterations: 157
currently lose_sum: 97.49721795320511
time_elpased: 1.757
batch start
#iterations: 158
currently lose_sum: 97.64795577526093
time_elpased: 1.736
batch start
#iterations: 159
currently lose_sum: 97.7483361363411
time_elpased: 1.742
start validation test
0.62912371134
0.639986678508
0.593290110116
0.615754339119
0.629186622754
63.412
batch start
#iterations: 160
currently lose_sum: 97.80318593978882
time_elpased: 1.79
batch start
#iterations: 161
currently lose_sum: 97.80063563585281
time_elpased: 1.755
batch start
#iterations: 162
currently lose_sum: 97.5373540520668
time_elpased: 1.764
batch start
#iterations: 163
currently lose_sum: 97.30298030376434
time_elpased: 1.758
batch start
#iterations: 164
currently lose_sum: 97.42767822742462
time_elpased: 1.774
batch start
#iterations: 165
currently lose_sum: 97.65049737691879
time_elpased: 1.779
batch start
#iterations: 166
currently lose_sum: 97.46249985694885
time_elpased: 1.787
batch start
#iterations: 167
currently lose_sum: 97.76091861724854
time_elpased: 1.77
batch start
#iterations: 168
currently lose_sum: 97.71094506978989
time_elpased: 1.831
batch start
#iterations: 169
currently lose_sum: 97.48354905843735
time_elpased: 1.776
batch start
#iterations: 170
currently lose_sum: 97.31025564670563
time_elpased: 1.756
batch start
#iterations: 171
currently lose_sum: 97.6652649641037
time_elpased: 1.788
batch start
#iterations: 172
currently lose_sum: 97.4230045080185
time_elpased: 1.882
batch start
#iterations: 173
currently lose_sum: 97.4874324798584
time_elpased: 1.761
batch start
#iterations: 174
currently lose_sum: 97.52699488401413
time_elpased: 1.775
batch start
#iterations: 175
currently lose_sum: 97.70249009132385
time_elpased: 1.773
batch start
#iterations: 176
currently lose_sum: 97.37094646692276
time_elpased: 1.735
batch start
#iterations: 177
currently lose_sum: 97.65083682537079
time_elpased: 1.753
batch start
#iterations: 178
currently lose_sum: 97.40452247858047
time_elpased: 1.762
batch start
#iterations: 179
currently lose_sum: 97.52114993333817
time_elpased: 1.741
start validation test
0.628298969072
0.638759689922
0.593598847381
0.61535178962
0.628359890488
63.387
batch start
#iterations: 180
currently lose_sum: 97.56132525205612
time_elpased: 1.781
batch start
#iterations: 181
currently lose_sum: 97.61916589736938
time_elpased: 1.768
batch start
#iterations: 182
currently lose_sum: 97.65238434076309
time_elpased: 1.747
batch start
#iterations: 183
currently lose_sum: 97.45587396621704
time_elpased: 1.734
batch start
#iterations: 184
currently lose_sum: 97.41496354341507
time_elpased: 1.794
batch start
#iterations: 185
currently lose_sum: 97.39867389202118
time_elpased: 1.73
batch start
#iterations: 186
currently lose_sum: 97.48314297199249
time_elpased: 1.751
batch start
#iterations: 187
currently lose_sum: 97.53075993061066
time_elpased: 1.782
batch start
#iterations: 188
currently lose_sum: 97.49134135246277
time_elpased: 1.761
batch start
#iterations: 189
currently lose_sum: 97.56911534070969
time_elpased: 1.753
batch start
#iterations: 190
currently lose_sum: 97.4489278793335
time_elpased: 1.733
batch start
#iterations: 191
currently lose_sum: 97.47884392738342
time_elpased: 1.739
batch start
#iterations: 192
currently lose_sum: 97.55522048473358
time_elpased: 1.803
batch start
#iterations: 193
currently lose_sum: 97.39910644292831
time_elpased: 1.736
batch start
#iterations: 194
currently lose_sum: 97.47681546211243
time_elpased: 1.749
batch start
#iterations: 195
currently lose_sum: 97.2760865688324
time_elpased: 1.783
batch start
#iterations: 196
currently lose_sum: 97.2542667388916
time_elpased: 1.792
batch start
#iterations: 197
currently lose_sum: 97.8941091299057
time_elpased: 1.829
batch start
#iterations: 198
currently lose_sum: 97.05370342731476
time_elpased: 1.774
batch start
#iterations: 199
currently lose_sum: 97.29495334625244
time_elpased: 1.762
start validation test
0.628298969072
0.644321584888
0.575692086035
0.608076525898
0.628391328569
63.494
batch start
#iterations: 200
currently lose_sum: 97.29047739505768
time_elpased: 1.838
batch start
#iterations: 201
currently lose_sum: 97.45561337471008
time_elpased: 1.719
batch start
#iterations: 202
currently lose_sum: 97.26962733268738
time_elpased: 1.769
batch start
#iterations: 203
currently lose_sum: 97.66728276014328
time_elpased: 1.777
batch start
#iterations: 204
currently lose_sum: 97.47942793369293
time_elpased: 1.73
batch start
#iterations: 205
currently lose_sum: 97.31782341003418
time_elpased: 1.777
batch start
#iterations: 206
currently lose_sum: 97.40128326416016
time_elpased: 1.804
batch start
#iterations: 207
currently lose_sum: 97.71039378643036
time_elpased: 1.774
batch start
#iterations: 208
currently lose_sum: 97.33770430088043
time_elpased: 1.769
batch start
#iterations: 209
currently lose_sum: 97.19454109668732
time_elpased: 1.805
batch start
#iterations: 210
currently lose_sum: 97.48741340637207
time_elpased: 1.768
batch start
#iterations: 211
currently lose_sum: 97.52092969417572
time_elpased: 1.764
batch start
#iterations: 212
currently lose_sum: 97.4596415758133
time_elpased: 1.78
batch start
#iterations: 213
currently lose_sum: 97.3368291258812
time_elpased: 1.772
batch start
#iterations: 214
currently lose_sum: 97.42924875020981
time_elpased: 1.877
batch start
#iterations: 215
currently lose_sum: 97.1397106051445
time_elpased: 1.742
batch start
#iterations: 216
currently lose_sum: 97.33023554086685
time_elpased: 1.757
batch start
#iterations: 217
currently lose_sum: 97.43173497915268
time_elpased: 1.753
batch start
#iterations: 218
currently lose_sum: 97.28590959310532
time_elpased: 1.81
batch start
#iterations: 219
currently lose_sum: 97.36509299278259
time_elpased: 1.747
start validation test
0.627577319588
0.642497712717
0.578161984151
0.608634418504
0.627664075831
63.414
batch start
#iterations: 220
currently lose_sum: 97.2373229265213
time_elpased: 1.753
batch start
#iterations: 221
currently lose_sum: 97.34916943311691
time_elpased: 1.738
batch start
#iterations: 222
currently lose_sum: 97.32971638441086
time_elpased: 1.755
batch start
#iterations: 223
currently lose_sum: 97.5812970995903
time_elpased: 1.742
batch start
#iterations: 224
currently lose_sum: 97.62861275672913
time_elpased: 1.848
batch start
#iterations: 225
currently lose_sum: 97.47273087501526
time_elpased: 1.79
batch start
#iterations: 226
currently lose_sum: 97.16448283195496
time_elpased: 1.847
batch start
#iterations: 227
currently lose_sum: 97.13127660751343
time_elpased: 1.765
batch start
#iterations: 228
currently lose_sum: 97.4374617934227
time_elpased: 1.846
batch start
#iterations: 229
currently lose_sum: 97.40179949998856
time_elpased: 1.805
batch start
#iterations: 230
currently lose_sum: 97.51034420728683
time_elpased: 1.813
batch start
#iterations: 231
currently lose_sum: 97.3345594406128
time_elpased: 1.763
batch start
#iterations: 232
currently lose_sum: 97.62815618515015
time_elpased: 1.753
batch start
#iterations: 233
currently lose_sum: 97.41785860061646
time_elpased: 1.766
batch start
#iterations: 234
currently lose_sum: 97.20211786031723
time_elpased: 1.8
batch start
#iterations: 235
currently lose_sum: 97.66722464561462
time_elpased: 1.759
batch start
#iterations: 236
currently lose_sum: 97.21637833118439
time_elpased: 1.788
batch start
#iterations: 237
currently lose_sum: 97.36962848901749
time_elpased: 1.76
batch start
#iterations: 238
currently lose_sum: 97.30148267745972
time_elpased: 1.766
batch start
#iterations: 239
currently lose_sum: 97.20681411027908
time_elpased: 1.754
start validation test
0.630927835052
0.641756292272
0.595657095811
0.617847993168
0.630989758274
63.301
batch start
#iterations: 240
currently lose_sum: 97.61660730838776
time_elpased: 1.846
batch start
#iterations: 241
currently lose_sum: 97.69719558954239
time_elpased: 1.806
batch start
#iterations: 242
currently lose_sum: 97.23316156864166
time_elpased: 1.781
batch start
#iterations: 243
currently lose_sum: 97.8137703537941
time_elpased: 1.829
batch start
#iterations: 244
currently lose_sum: 97.56996834278107
time_elpased: 1.814
batch start
#iterations: 245
currently lose_sum: 97.31622737646103
time_elpased: 1.85
batch start
#iterations: 246
currently lose_sum: 97.83121770620346
time_elpased: 1.742
batch start
#iterations: 247
currently lose_sum: 97.19713985919952
time_elpased: 1.788
batch start
#iterations: 248
currently lose_sum: 97.47217953205109
time_elpased: 1.766
batch start
#iterations: 249
currently lose_sum: 97.18107110261917
time_elpased: 1.727
batch start
#iterations: 250
currently lose_sum: 97.09463030099869
time_elpased: 1.778
batch start
#iterations: 251
currently lose_sum: 97.3430352807045
time_elpased: 1.8
batch start
#iterations: 252
currently lose_sum: 97.26302218437195
time_elpased: 1.757
batch start
#iterations: 253
currently lose_sum: 97.48915213346481
time_elpased: 1.789
batch start
#iterations: 254
currently lose_sum: 97.23653662204742
time_elpased: 1.729
batch start
#iterations: 255
currently lose_sum: 97.48069566488266
time_elpased: 1.737
batch start
#iterations: 256
currently lose_sum: 97.0046404004097
time_elpased: 1.797
batch start
#iterations: 257
currently lose_sum: 97.40295779705048
time_elpased: 1.737
batch start
#iterations: 258
currently lose_sum: 97.11605155467987
time_elpased: 1.809
batch start
#iterations: 259
currently lose_sum: 97.34118378162384
time_elpased: 1.737
start validation test
0.624381443299
0.640397504044
0.570340640115
0.603342224158
0.624476320264
63.495
batch start
#iterations: 260
currently lose_sum: 97.19249749183655
time_elpased: 1.81
batch start
#iterations: 261
currently lose_sum: 97.322316467762
time_elpased: 1.756
batch start
#iterations: 262
currently lose_sum: 97.24862790107727
time_elpased: 1.745
batch start
#iterations: 263
currently lose_sum: 97.24573713541031
time_elpased: 1.793
batch start
#iterations: 264
currently lose_sum: 97.54099172353745
time_elpased: 1.762
batch start
#iterations: 265
currently lose_sum: 97.5500493645668
time_elpased: 1.709
batch start
#iterations: 266
currently lose_sum: 97.26437956094742
time_elpased: 1.782
batch start
#iterations: 267
currently lose_sum: 97.29665207862854
time_elpased: 1.865
batch start
#iterations: 268
currently lose_sum: 97.21121889352798
time_elpased: 1.741
batch start
#iterations: 269
currently lose_sum: 97.5438221693039
time_elpased: 1.803
batch start
#iterations: 270
currently lose_sum: 97.42713981866837
time_elpased: 1.784
batch start
#iterations: 271
currently lose_sum: 97.44260507822037
time_elpased: 1.728
batch start
#iterations: 272
currently lose_sum: 97.41250044107437
time_elpased: 1.773
batch start
#iterations: 273
currently lose_sum: 97.4461418390274
time_elpased: 1.713
batch start
#iterations: 274
currently lose_sum: 97.49223440885544
time_elpased: 1.777
batch start
#iterations: 275
currently lose_sum: 97.40883845090866
time_elpased: 1.767
batch start
#iterations: 276
currently lose_sum: 97.29068607091904
time_elpased: 1.767
batch start
#iterations: 277
currently lose_sum: 97.68403381109238
time_elpased: 1.744
batch start
#iterations: 278
currently lose_sum: 97.28606903553009
time_elpased: 1.776
batch start
#iterations: 279
currently lose_sum: 97.30203872919083
time_elpased: 1.747
start validation test
0.626701030928
0.642029151842
0.575692086035
0.607053716766
0.626790584998
63.549
batch start
#iterations: 280
currently lose_sum: 97.52517706155777
time_elpased: 1.813
batch start
#iterations: 281
currently lose_sum: 97.13522291183472
time_elpased: 1.756
batch start
#iterations: 282
currently lose_sum: 97.34712618589401
time_elpased: 1.726
batch start
#iterations: 283
currently lose_sum: 97.39392483234406
time_elpased: 1.723
batch start
#iterations: 284
currently lose_sum: 97.23835778236389
time_elpased: 1.748
batch start
#iterations: 285
currently lose_sum: 97.17312633991241
time_elpased: 1.775
batch start
#iterations: 286
currently lose_sum: 97.4276739358902
time_elpased: 1.757
batch start
#iterations: 287
currently lose_sum: 97.09160536527634
time_elpased: 1.757
batch start
#iterations: 288
currently lose_sum: 97.23544442653656
time_elpased: 1.764
batch start
#iterations: 289
currently lose_sum: 97.22651648521423
time_elpased: 1.759
batch start
#iterations: 290
currently lose_sum: 97.19422435760498
time_elpased: 1.804
batch start
#iterations: 291
currently lose_sum: 97.16298657655716
time_elpased: 1.762
batch start
#iterations: 292
currently lose_sum: 97.33375412225723
time_elpased: 1.717
batch start
#iterations: 293
currently lose_sum: 97.41416776180267
time_elpased: 1.732
batch start
#iterations: 294
currently lose_sum: 97.31536448001862
time_elpased: 1.758
batch start
#iterations: 295
currently lose_sum: 97.18146270513535
time_elpased: 1.769
batch start
#iterations: 296
currently lose_sum: 97.12284481525421
time_elpased: 1.76
batch start
#iterations: 297
currently lose_sum: 97.77746266126633
time_elpased: 1.732
batch start
#iterations: 298
currently lose_sum: 97.28375542163849
time_elpased: 1.765
batch start
#iterations: 299
currently lose_sum: 97.32016211748123
time_elpased: 1.707
start validation test
0.63175257732
0.633773526048
0.627251209221
0.630495500155
0.631760480166
63.211
batch start
#iterations: 300
currently lose_sum: 97.05749088525772
time_elpased: 1.841
batch start
#iterations: 301
currently lose_sum: 97.2127240896225
time_elpased: 1.747
batch start
#iterations: 302
currently lose_sum: 97.52136981487274
time_elpased: 1.754
batch start
#iterations: 303
currently lose_sum: 97.1296404004097
time_elpased: 1.742
batch start
#iterations: 304
currently lose_sum: 97.32867586612701
time_elpased: 1.762
batch start
#iterations: 305
currently lose_sum: 97.2189753651619
time_elpased: 1.744
batch start
#iterations: 306
currently lose_sum: 97.18993777036667
time_elpased: 1.74
batch start
#iterations: 307
currently lose_sum: 97.14354765415192
time_elpased: 1.751
batch start
#iterations: 308
currently lose_sum: 97.51365333795547
time_elpased: 1.713
batch start
#iterations: 309
currently lose_sum: 97.31883281469345
time_elpased: 1.765
batch start
#iterations: 310
currently lose_sum: 97.3358244895935
time_elpased: 1.792
batch start
#iterations: 311
currently lose_sum: 97.24432641267776
time_elpased: 1.757
batch start
#iterations: 312
currently lose_sum: 97.47661709785461
time_elpased: 1.787
batch start
#iterations: 313
currently lose_sum: 97.21117061376572
time_elpased: 1.768
batch start
#iterations: 314
currently lose_sum: 96.94014942646027
time_elpased: 1.763
batch start
#iterations: 315
currently lose_sum: 97.18141257762909
time_elpased: 1.748
batch start
#iterations: 316
currently lose_sum: 96.8222633600235
time_elpased: 1.76
batch start
#iterations: 317
currently lose_sum: 97.20411258935928
time_elpased: 1.761
batch start
#iterations: 318
currently lose_sum: 97.42280727624893
time_elpased: 1.777
batch start
#iterations: 319
currently lose_sum: 97.3425641655922
time_elpased: 1.769
start validation test
0.629639175258
0.634853003835
0.613358032315
0.623920439675
0.629667759315
63.238
batch start
#iterations: 320
currently lose_sum: 97.28026765584946
time_elpased: 1.779
batch start
#iterations: 321
currently lose_sum: 97.00741189718246
time_elpased: 1.728
batch start
#iterations: 322
currently lose_sum: 97.33414167165756
time_elpased: 1.713
batch start
#iterations: 323
currently lose_sum: 96.80384528636932
time_elpased: 1.781
batch start
#iterations: 324
currently lose_sum: 97.34592115879059
time_elpased: 1.785
batch start
#iterations: 325
currently lose_sum: 97.32104188203812
time_elpased: 1.749
batch start
#iterations: 326
currently lose_sum: 97.54272133111954
time_elpased: 1.839
batch start
#iterations: 327
currently lose_sum: 97.00171285867691
time_elpased: 1.767
batch start
#iterations: 328
currently lose_sum: 97.27766287326813
time_elpased: 1.788
batch start
#iterations: 329
currently lose_sum: 97.09731006622314
time_elpased: 1.728
batch start
#iterations: 330
currently lose_sum: 97.12053608894348
time_elpased: 1.803
batch start
#iterations: 331
currently lose_sum: 97.17365038394928
time_elpased: 1.72
batch start
#iterations: 332
currently lose_sum: 97.38512909412384
time_elpased: 1.818
batch start
#iterations: 333
currently lose_sum: 97.30493414402008
time_elpased: 1.829
batch start
#iterations: 334
currently lose_sum: 97.52926027774811
time_elpased: 1.758
batch start
#iterations: 335
currently lose_sum: 97.11939877271652
time_elpased: 1.776
batch start
#iterations: 336
currently lose_sum: 97.37837207317352
time_elpased: 1.795
batch start
#iterations: 337
currently lose_sum: 97.2903163433075
time_elpased: 1.746
batch start
#iterations: 338
currently lose_sum: 97.26100766658783
time_elpased: 1.808
batch start
#iterations: 339
currently lose_sum: 96.97078913450241
time_elpased: 1.801
start validation test
0.632319587629
0.638122728245
0.614284244108
0.625976613707
0.632351251456
63.244
batch start
#iterations: 340
currently lose_sum: 97.19072622060776
time_elpased: 1.771
batch start
#iterations: 341
currently lose_sum: 97.69338911771774
time_elpased: 1.764
batch start
#iterations: 342
currently lose_sum: 97.24865621328354
time_elpased: 1.763
batch start
#iterations: 343
currently lose_sum: 97.19381999969482
time_elpased: 1.741
batch start
#iterations: 344
currently lose_sum: 97.2888098359108
time_elpased: 1.873
batch start
#iterations: 345
currently lose_sum: 97.39994311332703
time_elpased: 1.75
batch start
#iterations: 346
currently lose_sum: 97.2505874633789
time_elpased: 1.808
batch start
#iterations: 347
currently lose_sum: 96.9155382514
time_elpased: 1.785
batch start
#iterations: 348
currently lose_sum: 97.59922105073929
time_elpased: 1.8
batch start
#iterations: 349
currently lose_sum: 97.34335744380951
time_elpased: 1.742
batch start
#iterations: 350
currently lose_sum: 97.33656531572342
time_elpased: 1.873
batch start
#iterations: 351
currently lose_sum: 97.31026339530945
time_elpased: 1.774
batch start
#iterations: 352
currently lose_sum: 97.09287148714066
time_elpased: 1.754
batch start
#iterations: 353
currently lose_sum: 97.23160129785538
time_elpased: 1.832
batch start
#iterations: 354
currently lose_sum: 97.18865090608597
time_elpased: 1.753
batch start
#iterations: 355
currently lose_sum: 97.08131891489029
time_elpased: 1.915
batch start
#iterations: 356
currently lose_sum: 97.16533708572388
time_elpased: 1.74
batch start
#iterations: 357
currently lose_sum: 97.31651598215103
time_elpased: 1.766
batch start
#iterations: 358
currently lose_sum: 97.15446531772614
time_elpased: 1.796
batch start
#iterations: 359
currently lose_sum: 97.28494203090668
time_elpased: 1.772
start validation test
0.62175257732
0.640023543261
0.559534835855
0.59707884911
0.621861810161
63.392
batch start
#iterations: 360
currently lose_sum: 97.29108810424805
time_elpased: 1.848
batch start
#iterations: 361
currently lose_sum: 97.49305361509323
time_elpased: 1.808
batch start
#iterations: 362
currently lose_sum: 97.48801690340042
time_elpased: 1.744
batch start
#iterations: 363
currently lose_sum: 97.24483060836792
time_elpased: 1.814
batch start
#iterations: 364
currently lose_sum: 97.00784397125244
time_elpased: 1.852
batch start
#iterations: 365
currently lose_sum: 97.16738730669022
time_elpased: 1.793
batch start
#iterations: 366
currently lose_sum: 97.34512823820114
time_elpased: 1.759
batch start
#iterations: 367
currently lose_sum: 97.29398190975189
time_elpased: 1.743
batch start
#iterations: 368
currently lose_sum: 97.30373764038086
time_elpased: 1.767
batch start
#iterations: 369
currently lose_sum: 97.4044777750969
time_elpased: 1.757
batch start
#iterations: 370
currently lose_sum: 97.25269031524658
time_elpased: 1.788
batch start
#iterations: 371
currently lose_sum: 97.23244178295135
time_elpased: 1.786
batch start
#iterations: 372
currently lose_sum: 97.20910614728928
time_elpased: 1.749
batch start
#iterations: 373
currently lose_sum: 97.1752250790596
time_elpased: 1.723
batch start
#iterations: 374
currently lose_sum: 97.19521206617355
time_elpased: 1.782
batch start
#iterations: 375
currently lose_sum: 97.1397026181221
time_elpased: 1.783
batch start
#iterations: 376
currently lose_sum: 97.1272121667862
time_elpased: 1.771
batch start
#iterations: 377
currently lose_sum: 97.29603487253189
time_elpased: 1.815
batch start
#iterations: 378
currently lose_sum: 97.23153948783875
time_elpased: 1.772
batch start
#iterations: 379
currently lose_sum: 97.54286462068558
time_elpased: 1.728
start validation test
0.630412371134
0.638228590036
0.605125038592
0.621236133122
0.630456766947
63.451
batch start
#iterations: 380
currently lose_sum: 97.38988322019577
time_elpased: 1.753
batch start
#iterations: 381
currently lose_sum: 97.21155977249146
time_elpased: 1.758
batch start
#iterations: 382
currently lose_sum: 96.96810692548752
time_elpased: 1.789
batch start
#iterations: 383
currently lose_sum: 97.0366040468216
time_elpased: 1.741
batch start
#iterations: 384
currently lose_sum: 97.08802479505539
time_elpased: 1.786
batch start
#iterations: 385
currently lose_sum: 97.69452846050262
time_elpased: 1.822
batch start
#iterations: 386
currently lose_sum: 97.35868972539902
time_elpased: 1.799
batch start
#iterations: 387
currently lose_sum: 97.10608667135239
time_elpased: 1.717
batch start
#iterations: 388
currently lose_sum: 97.33739966154099
time_elpased: 1.803
batch start
#iterations: 389
currently lose_sum: 97.05822163820267
time_elpased: 1.778
batch start
#iterations: 390
currently lose_sum: 97.28478389978409
time_elpased: 1.805
batch start
#iterations: 391
currently lose_sum: 97.42227643728256
time_elpased: 1.792
batch start
#iterations: 392
currently lose_sum: 97.13687533140182
time_elpased: 1.796
batch start
#iterations: 393
currently lose_sum: 97.17147439718246
time_elpased: 1.76
batch start
#iterations: 394
currently lose_sum: 96.8987205028534
time_elpased: 1.787
batch start
#iterations: 395
currently lose_sum: 96.86515659093857
time_elpased: 1.814
batch start
#iterations: 396
currently lose_sum: 97.20236104726791
time_elpased: 1.746
batch start
#iterations: 397
currently lose_sum: 97.56858468055725
time_elpased: 1.771
batch start
#iterations: 398
currently lose_sum: 97.2423152923584
time_elpased: 1.818
batch start
#iterations: 399
currently lose_sum: 96.98229962587357
time_elpased: 1.749
start validation test
0.625670103093
0.646672242801
0.556962025316
0.598473957757
0.625790730721
63.525
acc: 0.631
pre: 0.633
rec: 0.628
F1: 0.630
auc: 0.631
