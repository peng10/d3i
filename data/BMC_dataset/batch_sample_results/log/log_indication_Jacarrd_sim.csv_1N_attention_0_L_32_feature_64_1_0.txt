start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.12152171134949
time_elpased: 1.963
batch start
#iterations: 1
currently lose_sum: 99.6455494761467
time_elpased: 1.915
batch start
#iterations: 2
currently lose_sum: 99.6513694524765
time_elpased: 1.936
batch start
#iterations: 3
currently lose_sum: 99.34198808670044
time_elpased: 1.918
batch start
#iterations: 4
currently lose_sum: 99.3852527141571
time_elpased: 1.951
batch start
#iterations: 5
currently lose_sum: 99.38957417011261
time_elpased: 1.954
batch start
#iterations: 6
currently lose_sum: 99.1671541929245
time_elpased: 1.945
batch start
#iterations: 7
currently lose_sum: 99.2868527173996
time_elpased: 1.941
batch start
#iterations: 8
currently lose_sum: 99.20701104402542
time_elpased: 1.928
batch start
#iterations: 9
currently lose_sum: 99.19557881355286
time_elpased: 1.971
batch start
#iterations: 10
currently lose_sum: 99.01342505216599
time_elpased: 1.922
batch start
#iterations: 11
currently lose_sum: 99.25585436820984
time_elpased: 1.916
batch start
#iterations: 12
currently lose_sum: 99.26882034540176
time_elpased: 1.921
batch start
#iterations: 13
currently lose_sum: 99.0321597456932
time_elpased: 1.936
batch start
#iterations: 14
currently lose_sum: 99.09182739257812
time_elpased: 1.946
batch start
#iterations: 15
currently lose_sum: 99.16267329454422
time_elpased: 1.926
batch start
#iterations: 16
currently lose_sum: 98.9989721775055
time_elpased: 1.928
batch start
#iterations: 17
currently lose_sum: 98.9961576461792
time_elpased: 1.926
batch start
#iterations: 18
currently lose_sum: 98.98066449165344
time_elpased: 1.929
batch start
#iterations: 19
currently lose_sum: 99.0424085855484
time_elpased: 1.919
start validation test
0.622010309278
0.647524752475
0.538437789441
0.58796426364
0.622157033727
64.083
batch start
#iterations: 20
currently lose_sum: 99.01525521278381
time_elpased: 1.941
batch start
#iterations: 21
currently lose_sum: 98.97313183546066
time_elpased: 1.918
batch start
#iterations: 22
currently lose_sum: 98.89114171266556
time_elpased: 1.923
batch start
#iterations: 23
currently lose_sum: 98.8888111114502
time_elpased: 1.928
batch start
#iterations: 24
currently lose_sum: 98.67728060483932
time_elpased: 1.949
batch start
#iterations: 25
currently lose_sum: 98.88688826560974
time_elpased: 1.904
batch start
#iterations: 26
currently lose_sum: 98.84093701839447
time_elpased: 1.928
batch start
#iterations: 27
currently lose_sum: 98.87049335241318
time_elpased: 1.934
batch start
#iterations: 28
currently lose_sum: 98.85285240411758
time_elpased: 1.932
batch start
#iterations: 29
currently lose_sum: 98.77711445093155
time_elpased: 1.968
batch start
#iterations: 30
currently lose_sum: 98.65498995780945
time_elpased: 1.924
batch start
#iterations: 31
currently lose_sum: 98.80527877807617
time_elpased: 1.934
batch start
#iterations: 32
currently lose_sum: 98.56246292591095
time_elpased: 1.967
batch start
#iterations: 33
currently lose_sum: 98.75495427846909
time_elpased: 1.932
batch start
#iterations: 34
currently lose_sum: 98.79208379983902
time_elpased: 1.925
batch start
#iterations: 35
currently lose_sum: 98.89903539419174
time_elpased: 1.965
batch start
#iterations: 36
currently lose_sum: 98.9626801609993
time_elpased: 1.944
batch start
#iterations: 37
currently lose_sum: 98.89642858505249
time_elpased: 1.935
batch start
#iterations: 38
currently lose_sum: 98.69502866268158
time_elpased: 1.92
batch start
#iterations: 39
currently lose_sum: 98.78215944766998
time_elpased: 1.918
start validation test
0.624742268041
0.659218607082
0.519193166615
0.580886586068
0.624927575769
63.931
batch start
#iterations: 40
currently lose_sum: 98.68032789230347
time_elpased: 1.948
batch start
#iterations: 41
currently lose_sum: 98.84337645769119
time_elpased: 1.922
batch start
#iterations: 42
currently lose_sum: 98.91904878616333
time_elpased: 1.926
batch start
#iterations: 43
currently lose_sum: 98.71800726652145
time_elpased: 1.926
batch start
#iterations: 44
currently lose_sum: 98.95894747972488
time_elpased: 1.938
batch start
#iterations: 45
currently lose_sum: 98.65458077192307
time_elpased: 1.945
batch start
#iterations: 46
currently lose_sum: 98.53314524888992
time_elpased: 1.931
batch start
#iterations: 47
currently lose_sum: 98.7680515050888
time_elpased: 1.963
batch start
#iterations: 48
currently lose_sum: 98.82406455278397
time_elpased: 1.928
batch start
#iterations: 49
currently lose_sum: 98.92446023225784
time_elpased: 1.982
batch start
#iterations: 50
currently lose_sum: 98.64365863800049
time_elpased: 1.942
batch start
#iterations: 51
currently lose_sum: 98.61503905057907
time_elpased: 1.934
batch start
#iterations: 52
currently lose_sum: 98.84198808670044
time_elpased: 1.953
batch start
#iterations: 53
currently lose_sum: 98.75094020366669
time_elpased: 1.915
batch start
#iterations: 54
currently lose_sum: 98.5955770611763
time_elpased: 1.94
batch start
#iterations: 55
currently lose_sum: 98.79314404726028
time_elpased: 1.923
batch start
#iterations: 56
currently lose_sum: 98.63261634111404
time_elpased: 1.93
batch start
#iterations: 57
currently lose_sum: 98.6033546924591
time_elpased: 1.937
batch start
#iterations: 58
currently lose_sum: 98.56206315755844
time_elpased: 1.976
batch start
#iterations: 59
currently lose_sum: 98.93178904056549
time_elpased: 1.927
start validation test
0.636340206186
0.654947613504
0.578985283524
0.614628284263
0.636440901599
63.815
batch start
#iterations: 60
currently lose_sum: 98.57701426744461
time_elpased: 1.919
batch start
#iterations: 61
currently lose_sum: 98.7836172580719
time_elpased: 1.933
batch start
#iterations: 62
currently lose_sum: 98.8921446800232
time_elpased: 1.92
batch start
#iterations: 63
currently lose_sum: 98.83571988344193
time_elpased: 1.927
batch start
#iterations: 64
currently lose_sum: 98.87639248371124
time_elpased: 1.975
batch start
#iterations: 65
currently lose_sum: 98.62080150842667
time_elpased: 1.968
batch start
#iterations: 66
currently lose_sum: 98.30543524026871
time_elpased: 1.928
batch start
#iterations: 67
currently lose_sum: 98.68200588226318
time_elpased: 1.934
batch start
#iterations: 68
currently lose_sum: 98.72606581449509
time_elpased: 1.935
batch start
#iterations: 69
currently lose_sum: 98.64297807216644
time_elpased: 1.919
batch start
#iterations: 70
currently lose_sum: 98.50805002450943
time_elpased: 1.935
batch start
#iterations: 71
currently lose_sum: 98.44862151145935
time_elpased: 1.928
batch start
#iterations: 72
currently lose_sum: 98.3784950375557
time_elpased: 1.987
batch start
#iterations: 73
currently lose_sum: 98.40700095891953
time_elpased: 1.948
batch start
#iterations: 74
currently lose_sum: 98.29985076189041
time_elpased: 1.914
batch start
#iterations: 75
currently lose_sum: 98.36433529853821
time_elpased: 1.912
batch start
#iterations: 76
currently lose_sum: 98.302110850811
time_elpased: 1.907
batch start
#iterations: 77
currently lose_sum: 98.47099262475967
time_elpased: 1.933
batch start
#iterations: 78
currently lose_sum: 98.53920716047287
time_elpased: 1.932
batch start
#iterations: 79
currently lose_sum: 98.58519858121872
time_elpased: 1.926
start validation test
0.636030927835
0.660619254959
0.562107646393
0.607395051432
0.636160711558
63.603
batch start
#iterations: 80
currently lose_sum: 98.68832540512085
time_elpased: 1.931
batch start
#iterations: 81
currently lose_sum: 98.56389838457108
time_elpased: 1.93
batch start
#iterations: 82
currently lose_sum: 98.59554761648178
time_elpased: 1.987
batch start
#iterations: 83
currently lose_sum: 98.710957467556
time_elpased: 1.97
batch start
#iterations: 84
currently lose_sum: 98.6132663488388
time_elpased: 1.938
batch start
#iterations: 85
currently lose_sum: 98.45967960357666
time_elpased: 1.926
batch start
#iterations: 86
currently lose_sum: 98.61954003572464
time_elpased: 1.96
batch start
#iterations: 87
currently lose_sum: 98.32279622554779
time_elpased: 1.948
batch start
#iterations: 88
currently lose_sum: 98.38325649499893
time_elpased: 1.98
batch start
#iterations: 89
currently lose_sum: 98.73167222738266
time_elpased: 1.929
batch start
#iterations: 90
currently lose_sum: 98.44829535484314
time_elpased: 1.94
batch start
#iterations: 91
currently lose_sum: 98.2768326997757
time_elpased: 1.927
batch start
#iterations: 92
currently lose_sum: 98.46441632509232
time_elpased: 1.943
batch start
#iterations: 93
currently lose_sum: 98.44838207960129
time_elpased: 1.933
batch start
#iterations: 94
currently lose_sum: 98.49847388267517
time_elpased: 1.944
batch start
#iterations: 95
currently lose_sum: 98.34948414564133
time_elpased: 1.95
batch start
#iterations: 96
currently lose_sum: 98.61577498912811
time_elpased: 1.938
batch start
#iterations: 97
currently lose_sum: 98.37735897302628
time_elpased: 1.943
batch start
#iterations: 98
currently lose_sum: 98.54122811555862
time_elpased: 1.949
batch start
#iterations: 99
currently lose_sum: 98.4213497042656
time_elpased: 1.938
start validation test
0.640412371134
0.662362279351
0.57538334877
0.615816719903
0.640526539613
63.330
batch start
#iterations: 100
currently lose_sum: 98.44462198019028
time_elpased: 1.982
batch start
#iterations: 101
currently lose_sum: 98.42164093255997
time_elpased: 1.999
batch start
#iterations: 102
currently lose_sum: 98.48580229282379
time_elpased: 1.985
batch start
#iterations: 103
currently lose_sum: 98.32713901996613
time_elpased: 1.941
batch start
#iterations: 104
currently lose_sum: 98.29160016775131
time_elpased: 1.915
batch start
#iterations: 105
currently lose_sum: 98.5906291604042
time_elpased: 1.911
batch start
#iterations: 106
currently lose_sum: 98.63528823852539
time_elpased: 1.966
batch start
#iterations: 107
currently lose_sum: 98.4197142124176
time_elpased: 1.944
batch start
#iterations: 108
currently lose_sum: 98.24452477693558
time_elpased: 1.941
batch start
#iterations: 109
currently lose_sum: 98.57953864336014
time_elpased: 1.943
batch start
#iterations: 110
currently lose_sum: 98.39833980798721
time_elpased: 1.985
batch start
#iterations: 111
currently lose_sum: 98.3325423002243
time_elpased: 1.937
batch start
#iterations: 112
currently lose_sum: 98.40019834041595
time_elpased: 1.994
batch start
#iterations: 113
currently lose_sum: 98.65249741077423
time_elpased: 1.932
batch start
#iterations: 114
currently lose_sum: 98.5093160867691
time_elpased: 1.944
batch start
#iterations: 115
currently lose_sum: 98.23864299058914
time_elpased: 1.937
batch start
#iterations: 116
currently lose_sum: 98.29773795604706
time_elpased: 1.934
batch start
#iterations: 117
currently lose_sum: 98.40340375900269
time_elpased: 1.943
batch start
#iterations: 118
currently lose_sum: 98.50008201599121
time_elpased: 1.957
batch start
#iterations: 119
currently lose_sum: 98.49244439601898
time_elpased: 1.958
start validation test
0.631907216495
0.676196990424
0.508696099619
0.580607270805
0.632123532615
63.371
batch start
#iterations: 120
currently lose_sum: 98.43211960792542
time_elpased: 1.954
batch start
#iterations: 121
currently lose_sum: 98.48172289133072
time_elpased: 1.937
batch start
#iterations: 122
currently lose_sum: 98.52280789613724
time_elpased: 1.916
batch start
#iterations: 123
currently lose_sum: 98.36916947364807
time_elpased: 1.911
batch start
#iterations: 124
currently lose_sum: 98.589659512043
time_elpased: 1.941
batch start
#iterations: 125
currently lose_sum: 98.47530561685562
time_elpased: 1.927
batch start
#iterations: 126
currently lose_sum: 98.45440924167633
time_elpased: 1.943
batch start
#iterations: 127
currently lose_sum: 98.47548401355743
time_elpased: 1.924
batch start
#iterations: 128
currently lose_sum: 98.51061862707138
time_elpased: 1.941
batch start
#iterations: 129
currently lose_sum: 98.52024292945862
time_elpased: 1.928
batch start
#iterations: 130
currently lose_sum: 98.32414031028748
time_elpased: 1.939
batch start
#iterations: 131
currently lose_sum: 98.4638786315918
time_elpased: 1.985
batch start
#iterations: 132
currently lose_sum: 98.44642037153244
time_elpased: 1.923
batch start
#iterations: 133
currently lose_sum: 98.35231077671051
time_elpased: 1.938
batch start
#iterations: 134
currently lose_sum: 98.37243461608887
time_elpased: 1.926
batch start
#iterations: 135
currently lose_sum: 98.62130063772202
time_elpased: 1.925
batch start
#iterations: 136
currently lose_sum: 98.53528267145157
time_elpased: 1.931
batch start
#iterations: 137
currently lose_sum: 98.33211398124695
time_elpased: 1.938
batch start
#iterations: 138
currently lose_sum: 98.41604548692703
time_elpased: 1.952
batch start
#iterations: 139
currently lose_sum: 98.44392889738083
time_elpased: 1.914
start validation test
0.632731958763
0.667398605012
0.531748482042
0.591901025259
0.632909250832
63.344
batch start
#iterations: 140
currently lose_sum: 98.30685329437256
time_elpased: 1.964
batch start
#iterations: 141
currently lose_sum: 98.42857140302658
time_elpased: 1.94
batch start
#iterations: 142
currently lose_sum: 98.38465118408203
time_elpased: 1.937
batch start
#iterations: 143
currently lose_sum: 98.45906120538712
time_elpased: 1.92
batch start
#iterations: 144
currently lose_sum: 98.486004114151
time_elpased: 1.944
batch start
#iterations: 145
currently lose_sum: 98.28940814733505
time_elpased: 1.93
batch start
#iterations: 146
currently lose_sum: 98.59836614131927
time_elpased: 1.952
batch start
#iterations: 147
currently lose_sum: 98.5883840918541
time_elpased: 1.988
batch start
#iterations: 148
currently lose_sum: 98.32558619976044
time_elpased: 1.93
batch start
#iterations: 149
currently lose_sum: 98.39389210939407
time_elpased: 1.935
batch start
#iterations: 150
currently lose_sum: 98.44437688589096
time_elpased: 1.948
batch start
#iterations: 151
currently lose_sum: 98.2370640039444
time_elpased: 1.931
batch start
#iterations: 152
currently lose_sum: 98.42788261175156
time_elpased: 1.932
batch start
#iterations: 153
currently lose_sum: 98.45433920621872
time_elpased: 1.901
batch start
#iterations: 154
currently lose_sum: 98.20688754320145
time_elpased: 1.942
batch start
#iterations: 155
currently lose_sum: 98.48080486059189
time_elpased: 1.93
batch start
#iterations: 156
currently lose_sum: 98.42488753795624
time_elpased: 1.975
batch start
#iterations: 157
currently lose_sum: 98.50100708007812
time_elpased: 1.91
batch start
#iterations: 158
currently lose_sum: 98.27503633499146
time_elpased: 1.969
batch start
#iterations: 159
currently lose_sum: 98.4560803771019
time_elpased: 1.959
start validation test
0.639381443299
0.661368758154
0.573839662447
0.614502975534
0.639496512004
63.258
batch start
#iterations: 160
currently lose_sum: 98.49720638990402
time_elpased: 1.934
batch start
#iterations: 161
currently lose_sum: 98.39303356409073
time_elpased: 1.908
batch start
#iterations: 162
currently lose_sum: 98.27739590406418
time_elpased: 1.927
batch start
#iterations: 163
currently lose_sum: 98.54905140399933
time_elpased: 1.934
batch start
#iterations: 164
currently lose_sum: 98.36532765626907
time_elpased: 1.965
batch start
#iterations: 165
currently lose_sum: 98.23729825019836
time_elpased: 1.934
batch start
#iterations: 166
currently lose_sum: 98.51178133487701
time_elpased: 1.96
batch start
#iterations: 167
currently lose_sum: 98.28932958841324
time_elpased: 1.94
batch start
#iterations: 168
currently lose_sum: 98.36194783449173
time_elpased: 1.943
batch start
#iterations: 169
currently lose_sum: 98.39007759094238
time_elpased: 1.934
batch start
#iterations: 170
currently lose_sum: 98.5502063035965
time_elpased: 1.963
batch start
#iterations: 171
currently lose_sum: 98.34057891368866
time_elpased: 1.937
batch start
#iterations: 172
currently lose_sum: 98.51718628406525
time_elpased: 1.953
batch start
#iterations: 173
currently lose_sum: 98.05211687088013
time_elpased: 1.945
batch start
#iterations: 174
currently lose_sum: 98.34854620695114
time_elpased: 1.957
batch start
#iterations: 175
currently lose_sum: 98.34762054681778
time_elpased: 1.942
batch start
#iterations: 176
currently lose_sum: 98.54893219470978
time_elpased: 1.954
batch start
#iterations: 177
currently lose_sum: 98.30399698019028
time_elpased: 1.931
batch start
#iterations: 178
currently lose_sum: 98.27887523174286
time_elpased: 1.94
batch start
#iterations: 179
currently lose_sum: 98.16512739658356
time_elpased: 1.973
start validation test
0.642422680412
0.67262791853
0.557373675003
0.609600990489
0.642571997059
63.091
batch start
#iterations: 180
currently lose_sum: 98.51808667182922
time_elpased: 1.937
batch start
#iterations: 181
currently lose_sum: 98.21557253599167
time_elpased: 1.906
batch start
#iterations: 182
currently lose_sum: 98.37351149320602
time_elpased: 1.923
batch start
#iterations: 183
currently lose_sum: 97.97541427612305
time_elpased: 1.916
batch start
#iterations: 184
currently lose_sum: 98.33209031820297
time_elpased: 1.939
batch start
#iterations: 185
currently lose_sum: 98.266905605793
time_elpased: 1.925
batch start
#iterations: 186
currently lose_sum: 98.33625167608261
time_elpased: 1.939
batch start
#iterations: 187
currently lose_sum: 98.04359090328217
time_elpased: 1.925
batch start
#iterations: 188
currently lose_sum: 98.26805365085602
time_elpased: 1.946
batch start
#iterations: 189
currently lose_sum: 98.20646476745605
time_elpased: 1.934
batch start
#iterations: 190
currently lose_sum: 98.36727213859558
time_elpased: 1.972
batch start
#iterations: 191
currently lose_sum: 98.52992528676987
time_elpased: 1.93
batch start
#iterations: 192
currently lose_sum: 98.29254877567291
time_elpased: 1.944
batch start
#iterations: 193
currently lose_sum: 98.18916201591492
time_elpased: 1.941
batch start
#iterations: 194
currently lose_sum: 98.2106260061264
time_elpased: 1.95
batch start
#iterations: 195
currently lose_sum: 98.35970169305801
time_elpased: 1.932
batch start
#iterations: 196
currently lose_sum: 98.30978745222092
time_elpased: 1.957
batch start
#iterations: 197
currently lose_sum: 98.42947059869766
time_elpased: 1.918
batch start
#iterations: 198
currently lose_sum: 98.41951441764832
time_elpased: 1.974
batch start
#iterations: 199
currently lose_sum: 98.58259570598602
time_elpased: 1.984
start validation test
0.645721649485
0.673330082886
0.568488216528
0.616483455164
0.645857244689
63.085
batch start
#iterations: 200
currently lose_sum: 98.4129576086998
time_elpased: 1.952
batch start
#iterations: 201
currently lose_sum: 98.38482338190079
time_elpased: 1.945
batch start
#iterations: 202
currently lose_sum: 98.2642473578453
time_elpased: 1.941
batch start
#iterations: 203
currently lose_sum: 98.40527772903442
time_elpased: 1.94
batch start
#iterations: 204
currently lose_sum: 98.40442603826523
time_elpased: 1.979
batch start
#iterations: 205
currently lose_sum: 98.21335083246231
time_elpased: 1.939
batch start
#iterations: 206
currently lose_sum: 98.40464103221893
time_elpased: 1.929
batch start
#iterations: 207
currently lose_sum: 98.38702809810638
time_elpased: 1.938
batch start
#iterations: 208
currently lose_sum: 98.13983452320099
time_elpased: 1.991
batch start
#iterations: 209
currently lose_sum: 98.34358251094818
time_elpased: 1.918
batch start
#iterations: 210
currently lose_sum: 98.05861175060272
time_elpased: 1.967
batch start
#iterations: 211
currently lose_sum: 98.00714701414108
time_elpased: 1.93
batch start
#iterations: 212
currently lose_sum: 98.14428037405014
time_elpased: 1.946
batch start
#iterations: 213
currently lose_sum: 98.13297301530838
time_elpased: 1.933
batch start
#iterations: 214
currently lose_sum: 98.16294795274734
time_elpased: 1.943
batch start
#iterations: 215
currently lose_sum: 98.36541730165482
time_elpased: 1.94
batch start
#iterations: 216
currently lose_sum: 98.13393247127533
time_elpased: 1.951
batch start
#iterations: 217
currently lose_sum: 98.09305942058563
time_elpased: 1.973
batch start
#iterations: 218
currently lose_sum: 98.47671180963516
time_elpased: 1.934
batch start
#iterations: 219
currently lose_sum: 98.42507517337799
time_elpased: 1.925
start validation test
0.637577319588
0.675143453312
0.532777606257
0.595570894449
0.637761311649
63.132
batch start
#iterations: 220
currently lose_sum: 98.49786186218262
time_elpased: 2.096
batch start
#iterations: 221
currently lose_sum: 98.427914083004
time_elpased: 1.941
batch start
#iterations: 222
currently lose_sum: 98.24292051792145
time_elpased: 1.957
batch start
#iterations: 223
currently lose_sum: 98.20190578699112
time_elpased: 1.94
batch start
#iterations: 224
currently lose_sum: 98.27410823106766
time_elpased: 1.953
batch start
#iterations: 225
currently lose_sum: 98.11826461553574
time_elpased: 1.973
batch start
#iterations: 226
currently lose_sum: 98.50171607732773
time_elpased: 1.962
batch start
#iterations: 227
currently lose_sum: 98.24774616956711
time_elpased: 2.016
batch start
#iterations: 228
currently lose_sum: 98.09609097242355
time_elpased: 1.935
batch start
#iterations: 229
currently lose_sum: 98.50712543725967
time_elpased: 1.921
batch start
#iterations: 230
currently lose_sum: 98.0830414891243
time_elpased: 1.957
batch start
#iterations: 231
currently lose_sum: 98.12469029426575
time_elpased: 1.917
batch start
#iterations: 232
currently lose_sum: 98.27287006378174
time_elpased: 1.954
batch start
#iterations: 233
currently lose_sum: 98.5188724398613
time_elpased: 1.993
batch start
#iterations: 234
currently lose_sum: 98.15187513828278
time_elpased: 1.942
batch start
#iterations: 235
currently lose_sum: 98.11420476436615
time_elpased: 1.935
batch start
#iterations: 236
currently lose_sum: 98.32684433460236
time_elpased: 1.963
batch start
#iterations: 237
currently lose_sum: 98.24448549747467
time_elpased: 1.973
batch start
#iterations: 238
currently lose_sum: 98.50067019462585
time_elpased: 1.968
batch start
#iterations: 239
currently lose_sum: 98.2600308060646
time_elpased: 1.927
start validation test
0.655567010309
0.676638342451
0.59822990635
0.635022940791
0.655667674439
62.941
batch start
#iterations: 240
currently lose_sum: 98.22341328859329
time_elpased: 1.957
batch start
#iterations: 241
currently lose_sum: 98.36859279870987
time_elpased: 1.945
batch start
#iterations: 242
currently lose_sum: 98.12365370988846
time_elpased: 1.954
batch start
#iterations: 243
currently lose_sum: 98.35403037071228
time_elpased: 1.927
batch start
#iterations: 244
currently lose_sum: 98.39979499578476
time_elpased: 1.957
batch start
#iterations: 245
currently lose_sum: 98.43992578983307
time_elpased: 2.0
batch start
#iterations: 246
currently lose_sum: 98.44707649946213
time_elpased: 1.946
batch start
#iterations: 247
currently lose_sum: 98.1658011674881
time_elpased: 1.929
batch start
#iterations: 248
currently lose_sum: 98.57257753610611
time_elpased: 1.956
batch start
#iterations: 249
currently lose_sum: 98.48181754350662
time_elpased: 1.939
batch start
#iterations: 250
currently lose_sum: 98.3360744714737
time_elpased: 1.959
batch start
#iterations: 251
currently lose_sum: 98.11570513248444
time_elpased: 1.951
batch start
#iterations: 252
currently lose_sum: 98.30816739797592
time_elpased: 1.957
batch start
#iterations: 253
currently lose_sum: 98.5164909362793
time_elpased: 1.936
batch start
#iterations: 254
currently lose_sum: 98.01755720376968
time_elpased: 1.943
batch start
#iterations: 255
currently lose_sum: 98.35949009656906
time_elpased: 1.923
batch start
#iterations: 256
currently lose_sum: 98.08547055721283
time_elpased: 1.952
batch start
#iterations: 257
currently lose_sum: 98.10281246900558
time_elpased: 1.947
batch start
#iterations: 258
currently lose_sum: 98.3551579117775
time_elpased: 1.971
batch start
#iterations: 259
currently lose_sum: 98.30491149425507
time_elpased: 1.957
start validation test
0.653041237113
0.678845232391
0.583204692806
0.627401051758
0.653163845938
62.849
batch start
#iterations: 260
currently lose_sum: 98.23707395792007
time_elpased: 1.958
batch start
#iterations: 261
currently lose_sum: 98.46602034568787
time_elpased: 1.939
batch start
#iterations: 262
currently lose_sum: 98.24030673503876
time_elpased: 1.95
batch start
#iterations: 263
currently lose_sum: 98.37861567735672
time_elpased: 1.919
batch start
#iterations: 264
currently lose_sum: 98.36811715364456
time_elpased: 1.949
batch start
#iterations: 265
currently lose_sum: 98.26242715120316
time_elpased: 1.945
batch start
#iterations: 266
currently lose_sum: 98.28072363138199
time_elpased: 1.96
batch start
#iterations: 267
currently lose_sum: 98.25085973739624
time_elpased: 1.94
batch start
#iterations: 268
currently lose_sum: 98.13677054643631
time_elpased: 1.956
batch start
#iterations: 269
currently lose_sum: 98.42409181594849
time_elpased: 2.017
batch start
#iterations: 270
currently lose_sum: 98.53214538097382
time_elpased: 1.932
batch start
#iterations: 271
currently lose_sum: 98.0822274684906
time_elpased: 1.931
batch start
#iterations: 272
currently lose_sum: 98.37050431966782
time_elpased: 1.939
batch start
#iterations: 273
currently lose_sum: 98.05062365531921
time_elpased: 1.923
batch start
#iterations: 274
currently lose_sum: 98.07416886091232
time_elpased: 1.965
batch start
#iterations: 275
currently lose_sum: 98.48357385396957
time_elpased: 1.909
batch start
#iterations: 276
currently lose_sum: 98.59257119894028
time_elpased: 1.939
batch start
#iterations: 277
currently lose_sum: 98.24295270442963
time_elpased: 1.926
batch start
#iterations: 278
currently lose_sum: 98.35300505161285
time_elpased: 1.933
batch start
#iterations: 279
currently lose_sum: 98.50147384405136
time_elpased: 1.922
start validation test
0.64912371134
0.679896142433
0.56591540599
0.617691659646
0.649269796354
62.896
batch start
#iterations: 280
currently lose_sum: 98.32775914669037
time_elpased: 1.942
batch start
#iterations: 281
currently lose_sum: 98.3474298119545
time_elpased: 1.935
batch start
#iterations: 282
currently lose_sum: 98.5175895690918
time_elpased: 1.969
batch start
#iterations: 283
currently lose_sum: 98.38254189491272
time_elpased: 1.937
batch start
#iterations: 284
currently lose_sum: 98.11534631252289
time_elpased: 1.952
batch start
#iterations: 285
currently lose_sum: 98.47638589143753
time_elpased: 1.937
batch start
#iterations: 286
currently lose_sum: 98.2661749124527
time_elpased: 1.965
batch start
#iterations: 287
currently lose_sum: 98.08926749229431
time_elpased: 1.927
batch start
#iterations: 288
currently lose_sum: 98.29611611366272
time_elpased: 1.941
batch start
#iterations: 289
currently lose_sum: 98.28865647315979
time_elpased: 1.936
batch start
#iterations: 290
currently lose_sum: 98.28416383266449
time_elpased: 1.953
batch start
#iterations: 291
currently lose_sum: 97.96566307544708
time_elpased: 1.966
batch start
#iterations: 292
currently lose_sum: 98.07177251577377
time_elpased: 1.983
batch start
#iterations: 293
currently lose_sum: 98.20714372396469
time_elpased: 1.915
batch start
#iterations: 294
currently lose_sum: 98.21974450349808
time_elpased: 1.928
batch start
#iterations: 295
currently lose_sum: 98.26806777715683
time_elpased: 1.926
batch start
#iterations: 296
currently lose_sum: 98.23287731409073
time_elpased: 1.954
batch start
#iterations: 297
currently lose_sum: 98.14782428741455
time_elpased: 1.92
batch start
#iterations: 298
currently lose_sum: 98.4319257736206
time_elpased: 1.939
batch start
#iterations: 299
currently lose_sum: 98.25700396299362
time_elpased: 1.977
start validation test
0.650670103093
0.67458432304
0.584542554286
0.626343937807
0.650786200204
62.893
batch start
#iterations: 300
currently lose_sum: 98.4035434126854
time_elpased: 1.94
batch start
#iterations: 301
currently lose_sum: 98.3976441025734
time_elpased: 1.925
batch start
#iterations: 302
currently lose_sum: 98.34984427690506
time_elpased: 1.972
batch start
#iterations: 303
currently lose_sum: 98.113321185112
time_elpased: 1.939
batch start
#iterations: 304
currently lose_sum: 98.21526724100113
time_elpased: 1.984
batch start
#iterations: 305
currently lose_sum: 98.41412848234177
time_elpased: 1.997
batch start
#iterations: 306
currently lose_sum: 98.292924284935
time_elpased: 1.939
batch start
#iterations: 307
currently lose_sum: 98.12406194210052
time_elpased: 1.933
batch start
#iterations: 308
currently lose_sum: 98.33795833587646
time_elpased: 1.956
batch start
#iterations: 309
currently lose_sum: 98.1717289686203
time_elpased: 1.924
batch start
#iterations: 310
currently lose_sum: 98.39365684986115
time_elpased: 1.94
batch start
#iterations: 311
currently lose_sum: 98.27611124515533
time_elpased: 1.913
batch start
#iterations: 312
currently lose_sum: 98.28093791007996
time_elpased: 1.937
batch start
#iterations: 313
currently lose_sum: 98.22081792354584
time_elpased: 1.912
batch start
#iterations: 314
currently lose_sum: 98.16506445407867
time_elpased: 1.931
batch start
#iterations: 315
currently lose_sum: 98.36843758821487
time_elpased: 1.942
batch start
#iterations: 316
currently lose_sum: 98.49226075410843
time_elpased: 1.957
batch start
#iterations: 317
currently lose_sum: 98.11164671182632
time_elpased: 1.971
batch start
#iterations: 318
currently lose_sum: 98.28293216228485
time_elpased: 1.962
batch start
#iterations: 319
currently lose_sum: 98.19568282365799
time_elpased: 1.946
start validation test
0.651340206186
0.68275776705
0.567664917156
0.619914587548
0.651487111061
62.827
batch start
#iterations: 320
currently lose_sum: 98.21591705083847
time_elpased: 1.954
batch start
#iterations: 321
currently lose_sum: 98.31650567054749
time_elpased: 1.931
batch start
#iterations: 322
currently lose_sum: 98.39187204837799
time_elpased: 1.947
batch start
#iterations: 323
currently lose_sum: 98.2873626947403
time_elpased: 1.936
batch start
#iterations: 324
currently lose_sum: 98.55285316705704
time_elpased: 1.964
batch start
#iterations: 325
currently lose_sum: 98.36178177595139
time_elpased: 1.945
batch start
#iterations: 326
currently lose_sum: 98.16857951879501
time_elpased: 1.957
batch start
#iterations: 327
currently lose_sum: 98.1879677772522
time_elpased: 1.937
batch start
#iterations: 328
currently lose_sum: 98.19822239875793
time_elpased: 1.97
batch start
#iterations: 329
currently lose_sum: 98.38045024871826
time_elpased: 1.909
batch start
#iterations: 330
currently lose_sum: 98.15020996332169
time_elpased: 1.943
batch start
#iterations: 331
currently lose_sum: 98.20318615436554
time_elpased: 1.938
batch start
#iterations: 332
currently lose_sum: 98.27049052715302
time_elpased: 1.949
batch start
#iterations: 333
currently lose_sum: 98.1415981054306
time_elpased: 1.939
batch start
#iterations: 334
currently lose_sum: 98.39731228351593
time_elpased: 1.931
batch start
#iterations: 335
currently lose_sum: 98.2174905538559
time_elpased: 1.942
batch start
#iterations: 336
currently lose_sum: 98.24506562948227
time_elpased: 1.947
batch start
#iterations: 337
currently lose_sum: 98.07318162918091
time_elpased: 1.934
batch start
#iterations: 338
currently lose_sum: 98.06737720966339
time_elpased: 1.95
batch start
#iterations: 339
currently lose_sum: 98.11324000358582
time_elpased: 1.919
start validation test
0.654536082474
0.682395644283
0.580426057425
0.627293960627
0.654666194054
62.679
batch start
#iterations: 340
currently lose_sum: 98.27822506427765
time_elpased: 1.954
batch start
#iterations: 341
currently lose_sum: 98.27746737003326
time_elpased: 1.93
batch start
#iterations: 342
currently lose_sum: 98.1530590057373
time_elpased: 1.947
batch start
#iterations: 343
currently lose_sum: 98.16546887159348
time_elpased: 1.965
batch start
#iterations: 344
currently lose_sum: 98.25556379556656
time_elpased: 1.948
batch start
#iterations: 345
currently lose_sum: 98.28750514984131
time_elpased: 1.929
batch start
#iterations: 346
currently lose_sum: 98.34948146343231
time_elpased: 1.93
batch start
#iterations: 347
currently lose_sum: 98.1740717291832
time_elpased: 1.92
batch start
#iterations: 348
currently lose_sum: 98.27585870027542
time_elpased: 1.977
batch start
#iterations: 349
currently lose_sum: 98.38940292596817
time_elpased: 1.928
batch start
#iterations: 350
currently lose_sum: 98.22018176317215
time_elpased: 1.946
batch start
#iterations: 351
currently lose_sum: 98.142402946949
time_elpased: 1.935
batch start
#iterations: 352
currently lose_sum: 98.20348060131073
time_elpased: 1.927
batch start
#iterations: 353
currently lose_sum: 98.55673640966415
time_elpased: 1.927
batch start
#iterations: 354
currently lose_sum: 98.3909861445427
time_elpased: 1.979
batch start
#iterations: 355
currently lose_sum: 98.28207725286484
time_elpased: 1.906
batch start
#iterations: 356
currently lose_sum: 98.20468258857727
time_elpased: 1.997
batch start
#iterations: 357
currently lose_sum: 98.2556466460228
time_elpased: 1.916
batch start
#iterations: 358
currently lose_sum: 98.12191158533096
time_elpased: 2.042
batch start
#iterations: 359
currently lose_sum: 98.01903957128525
time_elpased: 1.916
start validation test
0.639587628866
0.681642447674
0.526191211279
0.593913346498
0.639786713767
62.783
batch start
#iterations: 360
currently lose_sum: 98.52878034114838
time_elpased: 1.937
batch start
#iterations: 361
currently lose_sum: 98.36005485057831
time_elpased: 1.909
batch start
#iterations: 362
currently lose_sum: 98.13541328907013
time_elpased: 1.934
batch start
#iterations: 363
currently lose_sum: 98.2400838136673
time_elpased: 1.936
batch start
#iterations: 364
currently lose_sum: 98.65501302480698
time_elpased: 1.95
batch start
#iterations: 365
currently lose_sum: 98.22425270080566
time_elpased: 1.938
batch start
#iterations: 366
currently lose_sum: 98.44573420286179
time_elpased: 1.97
batch start
#iterations: 367
currently lose_sum: 98.43767940998077
time_elpased: 1.906
batch start
#iterations: 368
currently lose_sum: 98.40194410085678
time_elpased: 1.942
batch start
#iterations: 369
currently lose_sum: 98.26912546157837
time_elpased: 1.931
batch start
#iterations: 370
currently lose_sum: 97.98711848258972
time_elpased: 1.941
batch start
#iterations: 371
currently lose_sum: 98.30375015735626
time_elpased: 1.928
batch start
#iterations: 372
currently lose_sum: 98.2219814658165
time_elpased: 1.951
batch start
#iterations: 373
currently lose_sum: 98.01590746641159
time_elpased: 1.981
batch start
#iterations: 374
currently lose_sum: 98.2047889828682
time_elpased: 1.947
batch start
#iterations: 375
currently lose_sum: 98.38537335395813
time_elpased: 1.935
batch start
#iterations: 376
currently lose_sum: 98.06807923316956
time_elpased: 1.938
batch start
#iterations: 377
currently lose_sum: 98.33297175168991
time_elpased: 1.913
batch start
#iterations: 378
currently lose_sum: 98.26460891962051
time_elpased: 1.928
batch start
#iterations: 379
currently lose_sum: 98.34755223989487
time_elpased: 1.915
start validation test
0.649072164948
0.682932964407
0.558814448904
0.614670590899
0.64923062629
62.841
batch start
#iterations: 380
currently lose_sum: 98.25867027044296
time_elpased: 1.948
batch start
#iterations: 381
currently lose_sum: 98.27911823987961
time_elpased: 1.913
batch start
#iterations: 382
currently lose_sum: 98.14985942840576
time_elpased: 1.941
batch start
#iterations: 383
currently lose_sum: 98.1110251545906
time_elpased: 1.924
batch start
#iterations: 384
currently lose_sum: 98.39631539583206
time_elpased: 1.938
batch start
#iterations: 385
currently lose_sum: 98.37061876058578
time_elpased: 1.93
batch start
#iterations: 386
currently lose_sum: 98.20426362752914
time_elpased: 1.97
batch start
#iterations: 387
currently lose_sum: 98.28298860788345
time_elpased: 1.987
batch start
#iterations: 388
currently lose_sum: 98.22205781936646
time_elpased: 1.95
batch start
#iterations: 389
currently lose_sum: 98.33626866340637
time_elpased: 1.929
batch start
#iterations: 390
currently lose_sum: 98.3706670999527
time_elpased: 1.944
batch start
#iterations: 391
currently lose_sum: 98.29417335987091
time_elpased: 1.93
batch start
#iterations: 392
currently lose_sum: 98.29050129652023
time_elpased: 1.945
batch start
#iterations: 393
currently lose_sum: 98.34707391262054
time_elpased: 1.93
batch start
#iterations: 394
currently lose_sum: 98.11876738071442
time_elpased: 1.954
batch start
#iterations: 395
currently lose_sum: 97.91336423158646
time_elpased: 1.944
batch start
#iterations: 396
currently lose_sum: 98.13047540187836
time_elpased: 1.955
batch start
#iterations: 397
currently lose_sum: 98.14533150196075
time_elpased: 1.939
batch start
#iterations: 398
currently lose_sum: 98.1663190126419
time_elpased: 1.923
batch start
#iterations: 399
currently lose_sum: 98.41980230808258
time_elpased: 1.914
start validation test
0.645360824742
0.680908047443
0.549449418545
0.608155826404
0.645529212009
62.960
acc: 0.652
pre: 0.679
rec: 0.578
F1: 0.624
auc: 0.652
