start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.09611874818802
time_elpased: 2.131
batch start
#iterations: 1
currently lose_sum: 99.35693371295929
time_elpased: 1.97
batch start
#iterations: 2
currently lose_sum: 99.07592225074768
time_elpased: 1.971
batch start
#iterations: 3
currently lose_sum: 98.774687230587
time_elpased: 1.973
batch start
#iterations: 4
currently lose_sum: 98.38624733686447
time_elpased: 1.954
batch start
#iterations: 5
currently lose_sum: 97.83159101009369
time_elpased: 1.973
batch start
#iterations: 6
currently lose_sum: 97.82417672872543
time_elpased: 1.981
batch start
#iterations: 7
currently lose_sum: 97.56200671195984
time_elpased: 1.982
batch start
#iterations: 8
currently lose_sum: 97.4828137755394
time_elpased: 2.051
batch start
#iterations: 9
currently lose_sum: 97.31852048635483
time_elpased: 2.03
batch start
#iterations: 10
currently lose_sum: 97.30098080635071
time_elpased: 2.004
batch start
#iterations: 11
currently lose_sum: 97.21965658664703
time_elpased: 1.986
batch start
#iterations: 12
currently lose_sum: 96.97445929050446
time_elpased: 1.982
batch start
#iterations: 13
currently lose_sum: 96.90855592489243
time_elpased: 1.969
batch start
#iterations: 14
currently lose_sum: 96.7367684841156
time_elpased: 1.981
batch start
#iterations: 15
currently lose_sum: 96.87602692842484
time_elpased: 1.983
batch start
#iterations: 16
currently lose_sum: 96.7825146317482
time_elpased: 1.979
batch start
#iterations: 17
currently lose_sum: 96.73512405157089
time_elpased: 1.985
batch start
#iterations: 18
currently lose_sum: 96.3498540520668
time_elpased: 1.982
batch start
#iterations: 19
currently lose_sum: 96.67145442962646
time_elpased: 1.973
start validation test
0.641649484536
0.632486823191
0.679221982093
0.655021834061
0.641583520221
62.278
batch start
#iterations: 20
currently lose_sum: 96.47251850366592
time_elpased: 1.985
batch start
#iterations: 21
currently lose_sum: 96.25882077217102
time_elpased: 1.965
batch start
#iterations: 22
currently lose_sum: 96.65867763757706
time_elpased: 1.963
batch start
#iterations: 23
currently lose_sum: 96.00344878435135
time_elpased: 1.976
batch start
#iterations: 24
currently lose_sum: 95.9574276804924
time_elpased: 1.949
batch start
#iterations: 25
currently lose_sum: 96.20830744504929
time_elpased: 1.963
batch start
#iterations: 26
currently lose_sum: 96.22801232337952
time_elpased: 2.01
batch start
#iterations: 27
currently lose_sum: 96.09728956222534
time_elpased: 2.007
batch start
#iterations: 28
currently lose_sum: 95.97737729549408
time_elpased: 1.985
batch start
#iterations: 29
currently lose_sum: 95.94436800479889
time_elpased: 1.997
batch start
#iterations: 30
currently lose_sum: 95.89825457334518
time_elpased: 1.949
batch start
#iterations: 31
currently lose_sum: 96.16312396526337
time_elpased: 1.964
batch start
#iterations: 32
currently lose_sum: 95.72993099689484
time_elpased: 1.953
batch start
#iterations: 33
currently lose_sum: 96.20795875787735
time_elpased: 1.976
batch start
#iterations: 34
currently lose_sum: 95.91683799028397
time_elpased: 1.971
batch start
#iterations: 35
currently lose_sum: 95.63141423463821
time_elpased: 1.97
batch start
#iterations: 36
currently lose_sum: 95.34098941087723
time_elpased: 1.975
batch start
#iterations: 37
currently lose_sum: 95.89733684062958
time_elpased: 1.983
batch start
#iterations: 38
currently lose_sum: 95.66199326515198
time_elpased: 1.973
batch start
#iterations: 39
currently lose_sum: 95.49672424793243
time_elpased: 1.993
start validation test
0.657422680412
0.63388264016
0.748173304518
0.686302275087
0.657263353694
61.335
batch start
#iterations: 40
currently lose_sum: 95.79851353168488
time_elpased: 2.004
batch start
#iterations: 41
currently lose_sum: 95.34180068969727
time_elpased: 1.982
batch start
#iterations: 42
currently lose_sum: 95.34204405546188
time_elpased: 1.971
batch start
#iterations: 43
currently lose_sum: 95.34380227327347
time_elpased: 1.972
batch start
#iterations: 44
currently lose_sum: 95.74964064359665
time_elpased: 1.957
batch start
#iterations: 45
currently lose_sum: 95.32797360420227
time_elpased: 1.981
batch start
#iterations: 46
currently lose_sum: 95.43532860279083
time_elpased: 1.97
batch start
#iterations: 47
currently lose_sum: 95.66506391763687
time_elpased: 1.969
batch start
#iterations: 48
currently lose_sum: 95.7923995256424
time_elpased: 1.979
batch start
#iterations: 49
currently lose_sum: 95.45397877693176
time_elpased: 1.975
batch start
#iterations: 50
currently lose_sum: 95.44216710329056
time_elpased: 1.983
batch start
#iterations: 51
currently lose_sum: 95.52830070257187
time_elpased: 2.088
batch start
#iterations: 52
currently lose_sum: 95.71270525455475
time_elpased: 2.031
batch start
#iterations: 53
currently lose_sum: 95.03182190656662
time_elpased: 2.019
batch start
#iterations: 54
currently lose_sum: 95.27499210834503
time_elpased: 1.991
batch start
#iterations: 55
currently lose_sum: 95.70315170288086
time_elpased: 1.996
batch start
#iterations: 56
currently lose_sum: 95.09338134527206
time_elpased: 2.037
batch start
#iterations: 57
currently lose_sum: 95.14573168754578
time_elpased: 2.023
batch start
#iterations: 58
currently lose_sum: 95.22899478673935
time_elpased: 2.012
batch start
#iterations: 59
currently lose_sum: 95.41723072528839
time_elpased: 2.017
start validation test
0.656855670103
0.633975481611
0.745085931872
0.685054643516
0.65670076827
61.181
batch start
#iterations: 60
currently lose_sum: 95.16670775413513
time_elpased: 2.047
batch start
#iterations: 61
currently lose_sum: 95.30484861135483
time_elpased: 2.089
batch start
#iterations: 62
currently lose_sum: 95.07644706964493
time_elpased: 2.054
batch start
#iterations: 63
currently lose_sum: 95.11108481884003
time_elpased: 2.017
batch start
#iterations: 64
currently lose_sum: 95.18823617696762
time_elpased: 2.104
batch start
#iterations: 65
currently lose_sum: 95.15117251873016
time_elpased: 2.027
batch start
#iterations: 66
currently lose_sum: 95.48443275690079
time_elpased: 2.011
batch start
#iterations: 67
currently lose_sum: 95.35661667585373
time_elpased: 2.042
batch start
#iterations: 68
currently lose_sum: 95.1490672826767
time_elpased: 2.015
batch start
#iterations: 69
currently lose_sum: 94.9066731929779
time_elpased: 2.018
batch start
#iterations: 70
currently lose_sum: 94.86002004146576
time_elpased: 2.016
batch start
#iterations: 71
currently lose_sum: 95.01453137397766
time_elpased: 1.997
batch start
#iterations: 72
currently lose_sum: 95.18917447328568
time_elpased: 2.057
batch start
#iterations: 73
currently lose_sum: 94.57399886846542
time_elpased: 2.126
batch start
#iterations: 74
currently lose_sum: 95.22534370422363
time_elpased: 2.006
batch start
#iterations: 75
currently lose_sum: 95.209687769413
time_elpased: 2.063
batch start
#iterations: 76
currently lose_sum: 95.1458967924118
time_elpased: 2.012
batch start
#iterations: 77
currently lose_sum: 95.04269105195999
time_elpased: 2.014
batch start
#iterations: 78
currently lose_sum: 95.22135007381439
time_elpased: 2.054
batch start
#iterations: 79
currently lose_sum: 94.72123742103577
time_elpased: 2.021
start validation test
0.661134020619
0.634029850746
0.765050941649
0.693405465908
0.660951578436
60.973
batch start
#iterations: 80
currently lose_sum: 94.83526635169983
time_elpased: 2.049
batch start
#iterations: 81
currently lose_sum: 95.15822339057922
time_elpased: 2.003
batch start
#iterations: 82
currently lose_sum: 94.80119329690933
time_elpased: 1.961
batch start
#iterations: 83
currently lose_sum: 95.08320552110672
time_elpased: 2.011
batch start
#iterations: 84
currently lose_sum: 94.73011755943298
time_elpased: 2.027
batch start
#iterations: 85
currently lose_sum: 95.02158421278
time_elpased: 2.057
batch start
#iterations: 86
currently lose_sum: 95.19096332788467
time_elpased: 2.009
batch start
#iterations: 87
currently lose_sum: 94.81865245103836
time_elpased: 1.997
batch start
#iterations: 88
currently lose_sum: 95.09618163108826
time_elpased: 2.095
batch start
#iterations: 89
currently lose_sum: 94.52019327878952
time_elpased: 1.988
batch start
#iterations: 90
currently lose_sum: 94.29902869462967
time_elpased: 2.008
batch start
#iterations: 91
currently lose_sum: 94.84690868854523
time_elpased: 2.004
batch start
#iterations: 92
currently lose_sum: 94.61203420162201
time_elpased: 2.016
batch start
#iterations: 93
currently lose_sum: 95.15203446149826
time_elpased: 2.017
batch start
#iterations: 94
currently lose_sum: 94.77280408143997
time_elpased: 2.043
batch start
#iterations: 95
currently lose_sum: 94.8402755856514
time_elpased: 2.025
batch start
#iterations: 96
currently lose_sum: 95.12594276666641
time_elpased: 2.019
batch start
#iterations: 97
currently lose_sum: 94.2911427617073
time_elpased: 2.01
batch start
#iterations: 98
currently lose_sum: 94.00453424453735
time_elpased: 1.99
batch start
#iterations: 99
currently lose_sum: 94.41468852758408
time_elpased: 2.007
start validation test
0.646288659794
0.637484349417
0.681177318102
0.658606965174
0.646227407373
61.304
batch start
#iterations: 100
currently lose_sum: 94.84076815843582
time_elpased: 2.047
batch start
#iterations: 101
currently lose_sum: 94.62773549556732
time_elpased: 2.016
batch start
#iterations: 102
currently lose_sum: 94.36416923999786
time_elpased: 2.011
batch start
#iterations: 103
currently lose_sum: 94.68680155277252
time_elpased: 2.033
batch start
#iterations: 104
currently lose_sum: 94.68214815855026
time_elpased: 1.994
batch start
#iterations: 105
currently lose_sum: 94.40701425075531
time_elpased: 2.023
batch start
#iterations: 106
currently lose_sum: 94.97715592384338
time_elpased: 1.969
batch start
#iterations: 107
currently lose_sum: 95.0020312666893
time_elpased: 2.018
batch start
#iterations: 108
currently lose_sum: 94.35014915466309
time_elpased: 2.033
batch start
#iterations: 109
currently lose_sum: 94.2372516989708
time_elpased: 2.021
batch start
#iterations: 110
currently lose_sum: 94.35984373092651
time_elpased: 2.04
batch start
#iterations: 111
currently lose_sum: 94.60407334566116
time_elpased: 2.006
batch start
#iterations: 112
currently lose_sum: 94.89077717065811
time_elpased: 2.011
batch start
#iterations: 113
currently lose_sum: 94.69691371917725
time_elpased: 2.008
batch start
#iterations: 114
currently lose_sum: 94.50875335931778
time_elpased: 2.031
batch start
#iterations: 115
currently lose_sum: 94.08228123188019
time_elpased: 2.052
batch start
#iterations: 116
currently lose_sum: 94.92195349931717
time_elpased: 2.011
batch start
#iterations: 117
currently lose_sum: 94.87679475545883
time_elpased: 1.987
batch start
#iterations: 118
currently lose_sum: 94.37486046552658
time_elpased: 2.009
batch start
#iterations: 119
currently lose_sum: 94.63163304328918
time_elpased: 2.002
start validation test
0.65175257732
0.635937930401
0.712771431512
0.672166149068
0.65164544931
61.415
batch start
#iterations: 120
currently lose_sum: 94.32186931371689
time_elpased: 2.103
batch start
#iterations: 121
currently lose_sum: 94.56761074066162
time_elpased: 2.032
batch start
#iterations: 122
currently lose_sum: 94.60115021467209
time_elpased: 1.994
batch start
#iterations: 123
currently lose_sum: 94.56471198797226
time_elpased: 2.028
batch start
#iterations: 124
currently lose_sum: 94.41147363185883
time_elpased: 1.988
batch start
#iterations: 125
currently lose_sum: 94.7429581284523
time_elpased: 2.035
batch start
#iterations: 126
currently lose_sum: 94.38111817836761
time_elpased: 2.044
batch start
#iterations: 127
currently lose_sum: 94.26779472827911
time_elpased: 2.018
batch start
#iterations: 128
currently lose_sum: 94.46086090803146
time_elpased: 1.993
batch start
#iterations: 129
currently lose_sum: 94.24797004461288
time_elpased: 1.999
batch start
#iterations: 130
currently lose_sum: 94.7196216583252
time_elpased: 2.006
batch start
#iterations: 131
currently lose_sum: 94.06342834234238
time_elpased: 2.084
batch start
#iterations: 132
currently lose_sum: 94.40977817773819
time_elpased: 2.056
batch start
#iterations: 133
currently lose_sum: 93.890953540802
time_elpased: 2.014
batch start
#iterations: 134
currently lose_sum: 94.13921755552292
time_elpased: 2.032
batch start
#iterations: 135
currently lose_sum: 94.40879714488983
time_elpased: 2.018
batch start
#iterations: 136
currently lose_sum: 94.44172012805939
time_elpased: 2.007
batch start
#iterations: 137
currently lose_sum: 94.33583170175552
time_elpased: 2.027
batch start
#iterations: 138
currently lose_sum: 94.81425446271896
time_elpased: 1.995
batch start
#iterations: 139
currently lose_sum: 94.07559448480606
time_elpased: 1.987
start validation test
0.653298969072
0.62811616551
0.754553874653
0.685553997195
0.653121200468
60.988
batch start
#iterations: 140
currently lose_sum: 94.40542232990265
time_elpased: 2.03
batch start
#iterations: 141
currently lose_sum: 94.07689428329468
time_elpased: 2.032
batch start
#iterations: 142
currently lose_sum: 94.76376503705978
time_elpased: 2.034
batch start
#iterations: 143
currently lose_sum: 94.1421000957489
time_elpased: 2.031
batch start
#iterations: 144
currently lose_sum: 94.1619284749031
time_elpased: 2.01
batch start
#iterations: 145
currently lose_sum: 94.35097616910934
time_elpased: 1.978
batch start
#iterations: 146
currently lose_sum: 94.23047083616257
time_elpased: 2.015
batch start
#iterations: 147
currently lose_sum: 93.7765965461731
time_elpased: 2.007
batch start
#iterations: 148
currently lose_sum: 94.25226187705994
time_elpased: 2.035
batch start
#iterations: 149
currently lose_sum: 94.27901047468185
time_elpased: 2.019
batch start
#iterations: 150
currently lose_sum: 94.1916275024414
time_elpased: 2.014
batch start
#iterations: 151
currently lose_sum: 94.48755919933319
time_elpased: 1.968
batch start
#iterations: 152
currently lose_sum: 94.44605177640915
time_elpased: 2.008
batch start
#iterations: 153
currently lose_sum: 93.87437045574188
time_elpased: 1.99
batch start
#iterations: 154
currently lose_sum: 94.60438114404678
time_elpased: 2.013
batch start
#iterations: 155
currently lose_sum: 94.72007662057877
time_elpased: 1.992
batch start
#iterations: 156
currently lose_sum: 94.67147397994995
time_elpased: 2.012
batch start
#iterations: 157
currently lose_sum: 94.6966056227684
time_elpased: 1.966
batch start
#iterations: 158
currently lose_sum: 94.4774461388588
time_elpased: 2.058
batch start
#iterations: 159
currently lose_sum: 94.52718412876129
time_elpased: 1.992
start validation test
0.653144329897
0.639940052454
0.703097663888
0.67003383514
0.653056629114
60.886
batch start
#iterations: 160
currently lose_sum: 94.32619041204453
time_elpased: 2.06
batch start
#iterations: 161
currently lose_sum: 94.21508151292801
time_elpased: 2.007
batch start
#iterations: 162
currently lose_sum: 94.29404842853546
time_elpased: 1.987
batch start
#iterations: 163
currently lose_sum: 94.2402133345604
time_elpased: 2.015
batch start
#iterations: 164
currently lose_sum: 94.43291687965393
time_elpased: 2.034
batch start
#iterations: 165
currently lose_sum: 94.34655004739761
time_elpased: 2.021
batch start
#iterations: 166
currently lose_sum: 94.17383801937103
time_elpased: 2.048
batch start
#iterations: 167
currently lose_sum: 94.2390211224556
time_elpased: 1.99
batch start
#iterations: 168
currently lose_sum: 94.15224969387054
time_elpased: 1.975
batch start
#iterations: 169
currently lose_sum: 94.1792584657669
time_elpased: 2.042
batch start
#iterations: 170
currently lose_sum: 94.13620984554291
time_elpased: 2.041
batch start
#iterations: 171
currently lose_sum: 94.40493315458298
time_elpased: 2.076
batch start
#iterations: 172
currently lose_sum: 94.42545485496521
time_elpased: 2.018
batch start
#iterations: 173
currently lose_sum: 94.23669630289078
time_elpased: 2.132
batch start
#iterations: 174
currently lose_sum: 93.77275329828262
time_elpased: 2.027
batch start
#iterations: 175
currently lose_sum: 94.25913327932358
time_elpased: 2.023
batch start
#iterations: 176
currently lose_sum: 94.14943879842758
time_elpased: 2.053
batch start
#iterations: 177
currently lose_sum: 94.574737906456
time_elpased: 2.03
batch start
#iterations: 178
currently lose_sum: 94.30180287361145
time_elpased: 2.035
batch start
#iterations: 179
currently lose_sum: 94.40474581718445
time_elpased: 2.014
start validation test
0.653865979381
0.631321084864
0.742616033755
0.682460869154
0.653710164972
60.920
batch start
#iterations: 180
currently lose_sum: 93.93731337785721
time_elpased: 2.061
batch start
#iterations: 181
currently lose_sum: 93.80674922466278
time_elpased: 2.011
batch start
#iterations: 182
currently lose_sum: 94.12406182289124
time_elpased: 2.023
batch start
#iterations: 183
currently lose_sum: 93.9596244096756
time_elpased: 2.024
batch start
#iterations: 184
currently lose_sum: 94.04406189918518
time_elpased: 1.993
batch start
#iterations: 185
currently lose_sum: 94.11343932151794
time_elpased: 2.027
batch start
#iterations: 186
currently lose_sum: 94.15474605560303
time_elpased: 2.065
batch start
#iterations: 187
currently lose_sum: 93.85454952716827
time_elpased: 2.012
batch start
#iterations: 188
currently lose_sum: 94.2048122882843
time_elpased: 2.0
batch start
#iterations: 189
currently lose_sum: 94.07923579216003
time_elpased: 1.999
batch start
#iterations: 190
currently lose_sum: 94.37131637334824
time_elpased: 2.036
batch start
#iterations: 191
currently lose_sum: 94.46737051010132
time_elpased: 2.055
batch start
#iterations: 192
currently lose_sum: 94.44961941242218
time_elpased: 2.053
batch start
#iterations: 193
currently lose_sum: 94.24012219905853
time_elpased: 2.042
batch start
#iterations: 194
currently lose_sum: 93.8417741060257
time_elpased: 2.042
batch start
#iterations: 195
currently lose_sum: 94.09350204467773
time_elpased: 1.992
batch start
#iterations: 196
currently lose_sum: 94.54143065214157
time_elpased: 2.006
batch start
#iterations: 197
currently lose_sum: 93.94803547859192
time_elpased: 1.98
batch start
#iterations: 198
currently lose_sum: 94.05556684732437
time_elpased: 2.084
batch start
#iterations: 199
currently lose_sum: 94.08785915374756
time_elpased: 1.987
start validation test
0.657319587629
0.630141633449
0.764639291963
0.690905709503
0.657131171335
60.673
batch start
#iterations: 200
currently lose_sum: 94.48766601085663
time_elpased: 2.075
batch start
#iterations: 201
currently lose_sum: 93.59523832798004
time_elpased: 2.004
batch start
#iterations: 202
currently lose_sum: 93.61088448762894
time_elpased: 2.013
batch start
#iterations: 203
currently lose_sum: 94.07252144813538
time_elpased: 1.996
batch start
#iterations: 204
currently lose_sum: 94.14850300550461
time_elpased: 2.003
batch start
#iterations: 205
currently lose_sum: 93.94064664840698
time_elpased: 1.98
batch start
#iterations: 206
currently lose_sum: 94.10441172122955
time_elpased: 2.022
batch start
#iterations: 207
currently lose_sum: 94.13571816682816
time_elpased: 2.004
batch start
#iterations: 208
currently lose_sum: 93.87506306171417
time_elpased: 1.998
batch start
#iterations: 209
currently lose_sum: 93.93402022123337
time_elpased: 2.019
batch start
#iterations: 210
currently lose_sum: 93.8616469502449
time_elpased: 2.06
batch start
#iterations: 211
currently lose_sum: 93.9409304857254
time_elpased: 2.073
batch start
#iterations: 212
currently lose_sum: 94.02263647317886
time_elpased: 2.061
batch start
#iterations: 213
currently lose_sum: 93.95809978246689
time_elpased: 2.023
batch start
#iterations: 214
currently lose_sum: 94.04486095905304
time_elpased: 2.07
batch start
#iterations: 215
currently lose_sum: 93.98703277111053
time_elpased: 2.017
batch start
#iterations: 216
currently lose_sum: 93.96561592817307
time_elpased: 2.098
batch start
#iterations: 217
currently lose_sum: 94.40591418743134
time_elpased: 2.023
batch start
#iterations: 218
currently lose_sum: 93.95162177085876
time_elpased: 1.997
batch start
#iterations: 219
currently lose_sum: 94.18794751167297
time_elpased: 2.061
start validation test
0.656804123711
0.631887557127
0.754142224967
0.687623158487
0.656633231661
60.680
batch start
#iterations: 220
currently lose_sum: 93.97388410568237
time_elpased: 2.051
batch start
#iterations: 221
currently lose_sum: 93.9935889840126
time_elpased: 2.044
batch start
#iterations: 222
currently lose_sum: 94.28462851047516
time_elpased: 2.041
batch start
#iterations: 223
currently lose_sum: 93.87359869480133
time_elpased: 2.025
batch start
#iterations: 224
currently lose_sum: 93.65522867441177
time_elpased: 1.988
batch start
#iterations: 225
currently lose_sum: 93.92107737064362
time_elpased: 2.052
batch start
#iterations: 226
currently lose_sum: 94.03842920064926
time_elpased: 2.024
batch start
#iterations: 227
currently lose_sum: 94.00279080867767
time_elpased: 1.992
batch start
#iterations: 228
currently lose_sum: 93.67336452007294
time_elpased: 2.018
batch start
#iterations: 229
currently lose_sum: 93.88691085577011
time_elpased: 1.984
batch start
#iterations: 230
currently lose_sum: 93.56810110807419
time_elpased: 2.079
batch start
#iterations: 231
currently lose_sum: 94.04341387748718
time_elpased: 2.034
batch start
#iterations: 232
currently lose_sum: 93.99745553731918
time_elpased: 2.064
batch start
#iterations: 233
currently lose_sum: 93.52744901180267
time_elpased: 2.036
batch start
#iterations: 234
currently lose_sum: 93.76313257217407
time_elpased: 2.036
batch start
#iterations: 235
currently lose_sum: 94.07784336805344
time_elpased: 2.062
batch start
#iterations: 236
currently lose_sum: 94.26559770107269
time_elpased: 2.033
batch start
#iterations: 237
currently lose_sum: 93.94698423147202
time_elpased: 2.021
batch start
#iterations: 238
currently lose_sum: 93.80305176973343
time_elpased: 1.986
batch start
#iterations: 239
currently lose_sum: 94.46212935447693
time_elpased: 2.056
start validation test
0.655567010309
0.627746443303
0.767417927344
0.690590850157
0.655370638773
60.739
batch start
#iterations: 240
currently lose_sum: 93.58099591732025
time_elpased: 2.074
batch start
#iterations: 241
currently lose_sum: 93.93781232833862
time_elpased: 2.064
batch start
#iterations: 242
currently lose_sum: 93.99599725008011
time_elpased: 1.993
batch start
#iterations: 243
currently lose_sum: 94.28102821111679
time_elpased: 1.998
batch start
#iterations: 244
currently lose_sum: 93.9026535153389
time_elpased: 1.998
batch start
#iterations: 245
currently lose_sum: 94.15089392662048
time_elpased: 2.009
batch start
#iterations: 246
currently lose_sum: 93.65343832969666
time_elpased: 2.054
batch start
#iterations: 247
currently lose_sum: 94.12501364946365
time_elpased: 2.022
batch start
#iterations: 248
currently lose_sum: 93.48659819364548
time_elpased: 1.982
batch start
#iterations: 249
currently lose_sum: 93.91188442707062
time_elpased: 2.054
batch start
#iterations: 250
currently lose_sum: 94.00712966918945
time_elpased: 2.003
batch start
#iterations: 251
currently lose_sum: 93.93773955106735
time_elpased: 2.054
batch start
#iterations: 252
currently lose_sum: 93.50044202804565
time_elpased: 1.982
batch start
#iterations: 253
currently lose_sum: 94.08508855104446
time_elpased: 2.03
batch start
#iterations: 254
currently lose_sum: 93.57620447874069
time_elpased: 1.999
batch start
#iterations: 255
currently lose_sum: 93.88805693387985
time_elpased: 1.999
batch start
#iterations: 256
currently lose_sum: 93.72389096021652
time_elpased: 2.013
batch start
#iterations: 257
currently lose_sum: 93.77104562520981
time_elpased: 2.007
batch start
#iterations: 258
currently lose_sum: 93.72645574808121
time_elpased: 2.0
batch start
#iterations: 259
currently lose_sum: 93.68616342544556
time_elpased: 2.001
start validation test
0.657422680412
0.633533350726
0.749716990841
0.686745852187
0.657260643515
60.647
batch start
#iterations: 260
currently lose_sum: 93.52405488491058
time_elpased: 2.075
batch start
#iterations: 261
currently lose_sum: 93.5511891245842
time_elpased: 2.036
batch start
#iterations: 262
currently lose_sum: 93.81625717878342
time_elpased: 2.061
batch start
#iterations: 263
currently lose_sum: 93.67558419704437
time_elpased: 2.062
batch start
#iterations: 264
currently lose_sum: 94.01538556814194
time_elpased: 2.029
batch start
#iterations: 265
currently lose_sum: 93.6134005188942
time_elpased: 2.011
batch start
#iterations: 266
currently lose_sum: 93.8707405924797
time_elpased: 1.997
batch start
#iterations: 267
currently lose_sum: 93.66199177503586
time_elpased: 2.032
batch start
#iterations: 268
currently lose_sum: 93.75566774606705
time_elpased: 2.015
batch start
#iterations: 269
currently lose_sum: 93.95127195119858
time_elpased: 2.002
batch start
#iterations: 270
currently lose_sum: 93.95254427194595
time_elpased: 2.027
batch start
#iterations: 271
currently lose_sum: 93.49683058261871
time_elpased: 2.018
batch start
#iterations: 272
currently lose_sum: 93.95630300045013
time_elpased: 2.099
batch start
#iterations: 273
currently lose_sum: 93.91739225387573
time_elpased: 2.033
batch start
#iterations: 274
currently lose_sum: 93.80745148658752
time_elpased: 2.026
batch start
#iterations: 275
currently lose_sum: 94.29732584953308
time_elpased: 2.06
batch start
#iterations: 276
currently lose_sum: 93.30290579795837
time_elpased: 1.994
batch start
#iterations: 277
currently lose_sum: 93.80258631706238
time_elpased: 1.997
batch start
#iterations: 278
currently lose_sum: 93.42120009660721
time_elpased: 2.044
batch start
#iterations: 279
currently lose_sum: 93.76233685016632
time_elpased: 2.114
start validation test
0.658041237113
0.634829003761
0.74693835546
0.6863356974
0.657885164511
60.654
batch start
#iterations: 280
currently lose_sum: 93.2196878194809
time_elpased: 2.134
batch start
#iterations: 281
currently lose_sum: 93.73727470636368
time_elpased: 2.016
batch start
#iterations: 282
currently lose_sum: 93.89782047271729
time_elpased: 2.038
batch start
#iterations: 283
currently lose_sum: 93.99151456356049
time_elpased: 2.041
batch start
#iterations: 284
currently lose_sum: 93.24391061067581
time_elpased: 2.082
batch start
#iterations: 285
currently lose_sum: 93.44673711061478
time_elpased: 2.022
batch start
#iterations: 286
currently lose_sum: 93.665791451931
time_elpased: 2.024
batch start
#iterations: 287
currently lose_sum: 93.74833053350449
time_elpased: 2.041
batch start
#iterations: 288
currently lose_sum: 93.72781360149384
time_elpased: 2.041
batch start
#iterations: 289
currently lose_sum: 93.96408468484879
time_elpased: 2.001
batch start
#iterations: 290
currently lose_sum: 94.0212984085083
time_elpased: 1.993
batch start
#iterations: 291
currently lose_sum: 93.50647449493408
time_elpased: 2.018
batch start
#iterations: 292
currently lose_sum: 93.585764169693
time_elpased: 2.045
batch start
#iterations: 293
currently lose_sum: 93.64478665590286
time_elpased: 1.984
batch start
#iterations: 294
currently lose_sum: 93.52049976587296
time_elpased: 2.015
batch start
#iterations: 295
currently lose_sum: 94.13701266050339
time_elpased: 1.998
batch start
#iterations: 296
currently lose_sum: 93.73032784461975
time_elpased: 2.038
batch start
#iterations: 297
currently lose_sum: 93.91146206855774
time_elpased: 2.02
batch start
#iterations: 298
currently lose_sum: 93.50471067428589
time_elpased: 2.023
batch start
#iterations: 299
currently lose_sum: 93.44716602563858
time_elpased: 2.064
start validation test
0.654536082474
0.63427451679
0.73283935371
0.68000381971
0.654398609004
60.821
batch start
#iterations: 300
currently lose_sum: 93.56561410427094
time_elpased: 2.05
batch start
#iterations: 301
currently lose_sum: 93.79814952611923
time_elpased: 2.009
batch start
#iterations: 302
currently lose_sum: 93.28798425197601
time_elpased: 2.028
batch start
#iterations: 303
currently lose_sum: 93.92250925302505
time_elpased: 2.02
batch start
#iterations: 304
currently lose_sum: 93.55833929777145
time_elpased: 2.01
batch start
#iterations: 305
currently lose_sum: 93.64789533615112
time_elpased: 2.09
batch start
#iterations: 306
currently lose_sum: 93.98951321840286
time_elpased: 2.02
batch start
#iterations: 307
currently lose_sum: 93.74342715740204
time_elpased: 2.064
batch start
#iterations: 308
currently lose_sum: 93.46082407236099
time_elpased: 1.997
batch start
#iterations: 309
currently lose_sum: 93.63700538873672
time_elpased: 2.045
batch start
#iterations: 310
currently lose_sum: 93.74466490745544
time_elpased: 2.029
batch start
#iterations: 311
currently lose_sum: 93.82152485847473
time_elpased: 2.064
batch start
#iterations: 312
currently lose_sum: 93.32466554641724
time_elpased: 2.012
batch start
#iterations: 313
currently lose_sum: 93.49984115362167
time_elpased: 2.018
batch start
#iterations: 314
currently lose_sum: 93.61572200059891
time_elpased: 2.064
batch start
#iterations: 315
currently lose_sum: 93.49368834495544
time_elpased: 2.035
batch start
#iterations: 316
currently lose_sum: 93.55146700143814
time_elpased: 2.019
batch start
#iterations: 317
currently lose_sum: 94.00871276855469
time_elpased: 2.008
batch start
#iterations: 318
currently lose_sum: 93.94956356287003
time_elpased: 2.059
batch start
#iterations: 319
currently lose_sum: 93.874027967453
time_elpased: 2.069
start validation test
0.656237113402
0.634676564157
0.739117011423
0.682926829268
0.656091604958
60.900
batch start
#iterations: 320
currently lose_sum: 93.27009224891663
time_elpased: 2.061
batch start
#iterations: 321
currently lose_sum: 94.22770518064499
time_elpased: 2.0
batch start
#iterations: 322
currently lose_sum: 93.67622292041779
time_elpased: 2.018
batch start
#iterations: 323
currently lose_sum: 93.83301943540573
time_elpased: 2.058
batch start
#iterations: 324
currently lose_sum: 93.49128895998001
time_elpased: 2.075
batch start
#iterations: 325
currently lose_sum: 93.60862648487091
time_elpased: 2.05
batch start
#iterations: 326
currently lose_sum: 93.79684793949127
time_elpased: 1.999
batch start
#iterations: 327
currently lose_sum: 93.13809090852737
time_elpased: 2.021
batch start
#iterations: 328
currently lose_sum: 93.59318917989731
time_elpased: 2.023
batch start
#iterations: 329
currently lose_sum: 93.77887272834778
time_elpased: 2.027
batch start
#iterations: 330
currently lose_sum: 93.2916830778122
time_elpased: 2.022
batch start
#iterations: 331
currently lose_sum: 93.43224030733109
time_elpased: 2.068
batch start
#iterations: 332
currently lose_sum: 93.69081115722656
time_elpased: 2.031
batch start
#iterations: 333
currently lose_sum: 93.16109508275986
time_elpased: 2.031
batch start
#iterations: 334
currently lose_sum: 93.47346919775009
time_elpased: 2.037
batch start
#iterations: 335
currently lose_sum: 93.31093031167984
time_elpased: 2.03
batch start
#iterations: 336
currently lose_sum: 93.50687128305435
time_elpased: 2.021
batch start
#iterations: 337
currently lose_sum: 93.8244851231575
time_elpased: 2.05
batch start
#iterations: 338
currently lose_sum: 93.65102797746658
time_elpased: 2.02
batch start
#iterations: 339
currently lose_sum: 93.73721659183502
time_elpased: 2.068
start validation test
0.658195876289
0.633523710627
0.753421838016
0.688290321064
0.658028692425
60.696
batch start
#iterations: 340
currently lose_sum: 93.69676172733307
time_elpased: 2.057
batch start
#iterations: 341
currently lose_sum: 93.75762850046158
time_elpased: 2.06
batch start
#iterations: 342
currently lose_sum: 93.41939789056778
time_elpased: 2.058
batch start
#iterations: 343
currently lose_sum: 93.39019709825516
time_elpased: 2.023
batch start
#iterations: 344
currently lose_sum: 93.73786306381226
time_elpased: 2.079
batch start
#iterations: 345
currently lose_sum: 93.54208904504776
time_elpased: 2.032
batch start
#iterations: 346
currently lose_sum: 93.70263236761093
time_elpased: 2.01
batch start
#iterations: 347
currently lose_sum: 93.89770221710205
time_elpased: 2.037
batch start
#iterations: 348
currently lose_sum: 93.71517163515091
time_elpased: 2.042
batch start
#iterations: 349
currently lose_sum: 93.63254004716873
time_elpased: 2.025
batch start
#iterations: 350
currently lose_sum: 93.77526617050171
time_elpased: 2.067
batch start
#iterations: 351
currently lose_sum: 93.7971920967102
time_elpased: 2.086
batch start
#iterations: 352
currently lose_sum: 93.5887348651886
time_elpased: 2.092
batch start
#iterations: 353
currently lose_sum: 94.39035373926163
time_elpased: 2.005
batch start
#iterations: 354
currently lose_sum: 93.37968784570694
time_elpased: 2.088
batch start
#iterations: 355
currently lose_sum: 93.73074167966843
time_elpased: 2.07
batch start
#iterations: 356
currently lose_sum: 93.8891487121582
time_elpased: 2.034
batch start
#iterations: 357
currently lose_sum: 93.68471950292587
time_elpased: 2.014
batch start
#iterations: 358
currently lose_sum: 93.9814458489418
time_elpased: 2.04
batch start
#iterations: 359
currently lose_sum: 93.82979595661163
time_elpased: 2.021
start validation test
0.655515463918
0.634057971014
0.738396624473
0.682261208577
0.655369953257
60.935
batch start
#iterations: 360
currently lose_sum: 93.43458545207977
time_elpased: 2.053
batch start
#iterations: 361
currently lose_sum: 93.84886264801025
time_elpased: 2.05
batch start
#iterations: 362
currently lose_sum: 93.61404359340668
time_elpased: 2.094
batch start
#iterations: 363
currently lose_sum: 93.74155902862549
time_elpased: 2.051
batch start
#iterations: 364
currently lose_sum: 94.09491300582886
time_elpased: 2.036
batch start
#iterations: 365
currently lose_sum: 93.36475664377213
time_elpased: 2.018
batch start
#iterations: 366
currently lose_sum: 93.24205565452576
time_elpased: 2.066
batch start
#iterations: 367
currently lose_sum: 93.05578464269638
time_elpased: 2.011
batch start
#iterations: 368
currently lose_sum: 93.51246625185013
time_elpased: 2.046
batch start
#iterations: 369
currently lose_sum: 93.70633089542389
time_elpased: 2.001
batch start
#iterations: 370
currently lose_sum: 93.68094515800476
time_elpased: 2.022
batch start
#iterations: 371
currently lose_sum: 93.51269370317459
time_elpased: 2.071
batch start
#iterations: 372
currently lose_sum: 93.6112899184227
time_elpased: 2.061
batch start
#iterations: 373
currently lose_sum: 93.60913467407227
time_elpased: 2.061
batch start
#iterations: 374
currently lose_sum: 93.08764344453812
time_elpased: 2.075
batch start
#iterations: 375
currently lose_sum: 94.0608788728714
time_elpased: 2.017
batch start
#iterations: 376
currently lose_sum: 93.23242521286011
time_elpased: 2.007
batch start
#iterations: 377
currently lose_sum: 93.65120279788971
time_elpased: 2.031
batch start
#iterations: 378
currently lose_sum: 93.31881713867188
time_elpased: 2.016
batch start
#iterations: 379
currently lose_sum: 93.33085596561432
time_elpased: 1.994
start validation test
0.657989690722
0.630548966452
0.765977153442
0.691696482505
0.657800102075
60.768
batch start
#iterations: 380
currently lose_sum: 93.87563407421112
time_elpased: 2.008
batch start
#iterations: 381
currently lose_sum: 93.23953396081924
time_elpased: 2.029
batch start
#iterations: 382
currently lose_sum: 93.8642590045929
time_elpased: 2.073
batch start
#iterations: 383
currently lose_sum: 93.59617644548416
time_elpased: 1.995
batch start
#iterations: 384
currently lose_sum: 93.61899918317795
time_elpased: 2.054
batch start
#iterations: 385
currently lose_sum: 93.35399967432022
time_elpased: 2.023
batch start
#iterations: 386
currently lose_sum: 93.42640054225922
time_elpased: 2.004
batch start
#iterations: 387
currently lose_sum: 93.6994172334671
time_elpased: 2.075
batch start
#iterations: 388
currently lose_sum: 93.43881440162659
time_elpased: 2.079
batch start
#iterations: 389
currently lose_sum: 93.30324983596802
time_elpased: 1.983
batch start
#iterations: 390
currently lose_sum: 93.39242196083069
time_elpased: 2.036
batch start
#iterations: 391
currently lose_sum: 93.44256967306137
time_elpased: 2.086
batch start
#iterations: 392
currently lose_sum: 93.39326828718185
time_elpased: 2.035
batch start
#iterations: 393
currently lose_sum: 93.65513968467712
time_elpased: 2.039
batch start
#iterations: 394
currently lose_sum: 93.39364808797836
time_elpased: 2.011
batch start
#iterations: 395
currently lose_sum: 93.62887042760849
time_elpased: 2.017
batch start
#iterations: 396
currently lose_sum: 93.75908023118973
time_elpased: 2.039
batch start
#iterations: 397
currently lose_sum: 93.39029490947723
time_elpased: 2.004
batch start
#iterations: 398
currently lose_sum: 93.82065737247467
time_elpased: 1.994
batch start
#iterations: 399
currently lose_sum: 93.45909577608109
time_elpased: 2.014
start validation test
0.656288659794
0.627094622759
0.774107234743
0.692888725129
0.656081811113
60.706
acc: 0.654
pre: 0.630
rec: 0.748
F1: 0.684
auc: 0.653
