start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.07768142223358
time_elpased: 2.848
batch start
#iterations: 1
currently lose_sum: 99.48098462820053
time_elpased: 2.464
batch start
#iterations: 2
currently lose_sum: 99.02739375829697
time_elpased: 2.479
batch start
#iterations: 3
currently lose_sum: 98.2211166024208
time_elpased: 2.426
batch start
#iterations: 4
currently lose_sum: 98.16959458589554
time_elpased: 2.344
batch start
#iterations: 5
currently lose_sum: 98.04027116298676
time_elpased: 2.516
batch start
#iterations: 6
currently lose_sum: 97.50697821378708
time_elpased: 2.702
batch start
#iterations: 7
currently lose_sum: 97.63010966777802
time_elpased: 2.795
batch start
#iterations: 8
currently lose_sum: 97.29307997226715
time_elpased: 2.716
batch start
#iterations: 9
currently lose_sum: 97.57346868515015
time_elpased: 2.601
batch start
#iterations: 10
currently lose_sum: 97.04051971435547
time_elpased: 2.412
batch start
#iterations: 11
currently lose_sum: 97.10542166233063
time_elpased: 2.458
batch start
#iterations: 12
currently lose_sum: 97.15615379810333
time_elpased: 2.276
batch start
#iterations: 13
currently lose_sum: 96.7012991309166
time_elpased: 2.379
batch start
#iterations: 14
currently lose_sum: 96.38500952720642
time_elpased: 2.395
batch start
#iterations: 15
currently lose_sum: 96.68771332502365
time_elpased: 2.328
batch start
#iterations: 16
currently lose_sum: 96.44982212781906
time_elpased: 2.303
batch start
#iterations: 17
currently lose_sum: 96.32488477230072
time_elpased: 2.423
batch start
#iterations: 18
currently lose_sum: 96.27907490730286
time_elpased: 2.444
batch start
#iterations: 19
currently lose_sum: 96.11671203374863
time_elpased: 2.396
start validation test
0.643144329897
0.630414488424
0.694967582587
0.661118997504
0.643053346184
62.047
batch start
#iterations: 20
currently lose_sum: 96.57880133390427
time_elpased: 2.299
batch start
#iterations: 21
currently lose_sum: 96.11530143022537
time_elpased: 2.372
batch start
#iterations: 22
currently lose_sum: 96.10464715957642
time_elpased: 2.428
batch start
#iterations: 23
currently lose_sum: 95.69280368089676
time_elpased: 2.493
batch start
#iterations: 24
currently lose_sum: 95.76917344331741
time_elpased: 2.454
batch start
#iterations: 25
currently lose_sum: 96.08804887533188
time_elpased: 2.457
batch start
#iterations: 26
currently lose_sum: 95.68959814310074
time_elpased: 2.413
batch start
#iterations: 27
currently lose_sum: 95.72557210922241
time_elpased: 2.314
batch start
#iterations: 28
currently lose_sum: 95.54943931102753
time_elpased: 2.516
batch start
#iterations: 29
currently lose_sum: 96.31498229503632
time_elpased: 2.415
batch start
#iterations: 30
currently lose_sum: 95.51336741447449
time_elpased: 2.335
batch start
#iterations: 31
currently lose_sum: 95.53809458017349
time_elpased: 2.328
batch start
#iterations: 32
currently lose_sum: 95.53762447834015
time_elpased: 2.357
batch start
#iterations: 33
currently lose_sum: 95.69172137975693
time_elpased: 2.362
batch start
#iterations: 34
currently lose_sum: 95.6760880947113
time_elpased: 2.439
batch start
#iterations: 35
currently lose_sum: 95.7847118973732
time_elpased: 2.47
batch start
#iterations: 36
currently lose_sum: 95.18146485090256
time_elpased: 2.542
batch start
#iterations: 37
currently lose_sum: 95.40562576055527
time_elpased: 2.639
batch start
#iterations: 38
currently lose_sum: 95.45765727758408
time_elpased: 2.564
batch start
#iterations: 39
currently lose_sum: 95.4385437965393
time_elpased: 2.479
start validation test
0.655412371134
0.637718023256
0.722445199136
0.677442702051
0.655294684665
61.306
batch start
#iterations: 40
currently lose_sum: 95.27783125638962
time_elpased: 2.592
batch start
#iterations: 41
currently lose_sum: 95.37172734737396
time_elpased: 2.576
batch start
#iterations: 42
currently lose_sum: 95.1456943154335
time_elpased: 2.509
batch start
#iterations: 43
currently lose_sum: 95.46079051494598
time_elpased: 2.44
batch start
#iterations: 44
currently lose_sum: 95.44554930925369
time_elpased: 2.592
batch start
#iterations: 45
currently lose_sum: 95.24493771791458
time_elpased: 2.484
batch start
#iterations: 46
currently lose_sum: 95.12216693162918
time_elpased: 2.5
batch start
#iterations: 47
currently lose_sum: 95.32217001914978
time_elpased: 2.553
batch start
#iterations: 48
currently lose_sum: 95.41625249385834
time_elpased: 2.522
batch start
#iterations: 49
currently lose_sum: 95.36575645208359
time_elpased: 2.529
batch start
#iterations: 50
currently lose_sum: 95.05813348293304
time_elpased: 2.458
batch start
#iterations: 51
currently lose_sum: 95.31367522478104
time_elpased: 2.415
batch start
#iterations: 52
currently lose_sum: 95.03004467487335
time_elpased: 2.352
batch start
#iterations: 53
currently lose_sum: 95.01489520072937
time_elpased: 2.441
batch start
#iterations: 54
currently lose_sum: 94.97715026140213
time_elpased: 2.389
batch start
#iterations: 55
currently lose_sum: 95.17331635951996
time_elpased: 2.584
batch start
#iterations: 56
currently lose_sum: 95.43418371677399
time_elpased: 2.474
batch start
#iterations: 57
currently lose_sum: 95.28233796358109
time_elpased: 2.447
batch start
#iterations: 58
currently lose_sum: 95.11827731132507
time_elpased: 2.484
batch start
#iterations: 59
currently lose_sum: 95.11836391687393
time_elpased: 2.397
start validation test
0.667835051546
0.646837146703
0.741895646805
0.691113028473
0.667705026748
60.863
batch start
#iterations: 60
currently lose_sum: 94.93300819396973
time_elpased: 2.522
batch start
#iterations: 61
currently lose_sum: 94.95147168636322
time_elpased: 2.539
batch start
#iterations: 62
currently lose_sum: 95.46537327766418
time_elpased: 2.527
batch start
#iterations: 63
currently lose_sum: 95.12589037418365
time_elpased: 2.514
batch start
#iterations: 64
currently lose_sum: 94.97341668605804
time_elpased: 2.534
batch start
#iterations: 65
currently lose_sum: 94.85959845781326
time_elpased: 2.438
batch start
#iterations: 66
currently lose_sum: 94.92417985200882
time_elpased: 2.477
batch start
#iterations: 67
currently lose_sum: 94.91392058134079
time_elpased: 2.525
batch start
#iterations: 68
currently lose_sum: 95.00463831424713
time_elpased: 2.616
batch start
#iterations: 69
currently lose_sum: 94.66232007741928
time_elpased: 2.655
batch start
#iterations: 70
currently lose_sum: 94.80745047330856
time_elpased: 2.495
batch start
#iterations: 71
currently lose_sum: 94.9963749051094
time_elpased: 2.526
batch start
#iterations: 72
currently lose_sum: 94.85636991262436
time_elpased: 2.646
batch start
#iterations: 73
currently lose_sum: 95.172392308712
time_elpased: 2.434
batch start
#iterations: 74
currently lose_sum: 95.05183237791061
time_elpased: 2.632
batch start
#iterations: 75
currently lose_sum: 95.19626069068909
time_elpased: 2.697
batch start
#iterations: 76
currently lose_sum: 94.63376516103745
time_elpased: 2.615
batch start
#iterations: 77
currently lose_sum: 94.9364487528801
time_elpased: 2.609
batch start
#iterations: 78
currently lose_sum: 94.99734020233154
time_elpased: 2.682
batch start
#iterations: 79
currently lose_sum: 95.04528683423996
time_elpased: 2.639
start validation test
0.661340206186
0.647207409486
0.71194813214
0.678035871802
0.661251356166
61.008
batch start
#iterations: 80
currently lose_sum: 95.19410824775696
time_elpased: 2.375
batch start
#iterations: 81
currently lose_sum: 95.01453191041946
time_elpased: 2.433
batch start
#iterations: 82
currently lose_sum: 95.00578010082245
time_elpased: 2.315
batch start
#iterations: 83
currently lose_sum: 94.8425629734993
time_elpased: 2.215
batch start
#iterations: 84
currently lose_sum: 94.97225123643875
time_elpased: 2.137
batch start
#iterations: 85
currently lose_sum: 94.85277676582336
time_elpased: 2.245
batch start
#iterations: 86
currently lose_sum: 94.58898735046387
time_elpased: 2.289
batch start
#iterations: 87
currently lose_sum: 94.75190180540085
time_elpased: 2.389
batch start
#iterations: 88
currently lose_sum: 94.62548005580902
time_elpased: 2.434
batch start
#iterations: 89
currently lose_sum: 95.01317429542542
time_elpased: 2.375
batch start
#iterations: 90
currently lose_sum: 95.04601973295212
time_elpased: 2.426
batch start
#iterations: 91
currently lose_sum: 94.78416323661804
time_elpased: 2.467
batch start
#iterations: 92
currently lose_sum: 94.70870131254196
time_elpased: 2.475
batch start
#iterations: 93
currently lose_sum: 94.89899188280106
time_elpased: 2.564
batch start
#iterations: 94
currently lose_sum: 94.70758587121964
time_elpased: 2.49
batch start
#iterations: 95
currently lose_sum: 94.29898345470428
time_elpased: 2.481
batch start
#iterations: 96
currently lose_sum: 94.49827349185944
time_elpased: 2.415
batch start
#iterations: 97
currently lose_sum: 94.6840050816536
time_elpased: 2.439
batch start
#iterations: 98
currently lose_sum: 94.97438889741898
time_elpased: 2.465
batch start
#iterations: 99
currently lose_sum: 94.70620143413544
time_elpased: 2.562
start validation test
0.663969072165
0.641179586791
0.747350005146
0.690205769139
0.663822684076
60.662
batch start
#iterations: 100
currently lose_sum: 94.96639561653137
time_elpased: 2.546
batch start
#iterations: 101
currently lose_sum: 94.79271405935287
time_elpased: 2.526
batch start
#iterations: 102
currently lose_sum: 94.56048321723938
time_elpased: 2.494
batch start
#iterations: 103
currently lose_sum: 94.74602025747299
time_elpased: 2.538
batch start
#iterations: 104
currently lose_sum: 94.67268013954163
time_elpased: 2.574
batch start
#iterations: 105
currently lose_sum: 94.38990533351898
time_elpased: 2.547
batch start
#iterations: 106
currently lose_sum: 94.80134958028793
time_elpased: 2.532
batch start
#iterations: 107
currently lose_sum: 94.57637417316437
time_elpased: 2.568
batch start
#iterations: 108
currently lose_sum: 94.47216033935547
time_elpased: 2.5
batch start
#iterations: 109
currently lose_sum: 94.61338651180267
time_elpased: 2.57
batch start
#iterations: 110
currently lose_sum: 94.50423592329025
time_elpased: 2.584
batch start
#iterations: 111
currently lose_sum: 94.30627423524857
time_elpased: 2.508
batch start
#iterations: 112
currently lose_sum: 94.21547871828079
time_elpased: 2.486
batch start
#iterations: 113
currently lose_sum: 94.24563544988632
time_elpased: 2.452
batch start
#iterations: 114
currently lose_sum: 94.3014680147171
time_elpased: 2.461
batch start
#iterations: 115
currently lose_sum: 94.6693828701973
time_elpased: 2.51
batch start
#iterations: 116
currently lose_sum: 94.16882979869843
time_elpased: 2.453
batch start
#iterations: 117
currently lose_sum: 94.28749912977219
time_elpased: 2.393
batch start
#iterations: 118
currently lose_sum: 94.10915964841843
time_elpased: 2.415
batch start
#iterations: 119
currently lose_sum: 94.53678900003433
time_elpased: 2.463
start validation test
0.661804123711
0.645036764706
0.722239374292
0.681458464825
0.661698020307
60.693
batch start
#iterations: 120
currently lose_sum: 94.27683371305466
time_elpased: 2.345
batch start
#iterations: 121
currently lose_sum: 94.40378713607788
time_elpased: 2.455
batch start
#iterations: 122
currently lose_sum: 94.30737727880478
time_elpased: 2.439
batch start
#iterations: 123
currently lose_sum: 94.69846987724304
time_elpased: 2.468
batch start
#iterations: 124
currently lose_sum: 94.47961097955704
time_elpased: 2.429
batch start
#iterations: 125
currently lose_sum: 94.35829317569733
time_elpased: 2.468
batch start
#iterations: 126
currently lose_sum: 94.74270731210709
time_elpased: 2.455
batch start
#iterations: 127
currently lose_sum: 94.90727084875107
time_elpased: 2.483
batch start
#iterations: 128
currently lose_sum: 94.36957681179047
time_elpased: 2.475
batch start
#iterations: 129
currently lose_sum: 94.7401510477066
time_elpased: 2.314
batch start
#iterations: 130
currently lose_sum: 94.00427502393723
time_elpased: 2.483
batch start
#iterations: 131
currently lose_sum: 95.00928509235382
time_elpased: 2.526
batch start
#iterations: 132
currently lose_sum: 94.22230160236359
time_elpased: 2.45
batch start
#iterations: 133
currently lose_sum: 94.28443652391434
time_elpased: 2.483
batch start
#iterations: 134
currently lose_sum: 94.29645055532455
time_elpased: 2.46
batch start
#iterations: 135
currently lose_sum: 94.32983911037445
time_elpased: 2.447
batch start
#iterations: 136
currently lose_sum: 94.30850446224213
time_elpased: 2.416
batch start
#iterations: 137
currently lose_sum: 94.23658329248428
time_elpased: 2.408
batch start
#iterations: 138
currently lose_sum: 94.32750529050827
time_elpased: 2.473
batch start
#iterations: 139
currently lose_sum: 94.13042950630188
time_elpased: 2.455
start validation test
0.665463917526
0.642296498809
0.749511165998
0.691774316109
0.665316359618
60.478
batch start
#iterations: 140
currently lose_sum: 94.28813648223877
time_elpased: 2.576
batch start
#iterations: 141
currently lose_sum: 94.42163240909576
time_elpased: 2.541
batch start
#iterations: 142
currently lose_sum: 94.12353754043579
time_elpased: 2.508
batch start
#iterations: 143
currently lose_sum: 94.42653912305832
time_elpased: 2.43
batch start
#iterations: 144
currently lose_sum: 94.56645041704178
time_elpased: 2.412
batch start
#iterations: 145
currently lose_sum: 94.174531519413
time_elpased: 2.455
batch start
#iterations: 146
currently lose_sum: 94.04134172201157
time_elpased: 2.335
batch start
#iterations: 147
currently lose_sum: 94.28339731693268
time_elpased: 2.374
batch start
#iterations: 148
currently lose_sum: 94.14341694116592
time_elpased: 2.301
batch start
#iterations: 149
currently lose_sum: 94.11295521259308
time_elpased: 2.405
batch start
#iterations: 150
currently lose_sum: 94.37620586156845
time_elpased: 2.336
batch start
#iterations: 151
currently lose_sum: 94.07764947414398
time_elpased: 2.25
batch start
#iterations: 152
currently lose_sum: 94.34820890426636
time_elpased: 2.344
batch start
#iterations: 153
currently lose_sum: 94.53551632165909
time_elpased: 2.336
batch start
#iterations: 154
currently lose_sum: 94.1129167675972
time_elpased: 2.343
batch start
#iterations: 155
currently lose_sum: 94.31157130002975
time_elpased: 2.345
batch start
#iterations: 156
currently lose_sum: 94.31741851568222
time_elpased: 2.261
batch start
#iterations: 157
currently lose_sum: 93.57503014802933
time_elpased: 2.286
batch start
#iterations: 158
currently lose_sum: 94.22139900922775
time_elpased: 2.236
batch start
#iterations: 159
currently lose_sum: 94.16223138570786
time_elpased: 2.236
start validation test
0.666030927835
0.640807096886
0.758258721828
0.694602875324
0.665869007718
60.490
batch start
#iterations: 160
currently lose_sum: 94.0511337518692
time_elpased: 2.437
batch start
#iterations: 161
currently lose_sum: 94.6586172580719
time_elpased: 2.371
batch start
#iterations: 162
currently lose_sum: 94.49173206090927
time_elpased: 2.373
batch start
#iterations: 163
currently lose_sum: 94.39730876684189
time_elpased: 2.367
batch start
#iterations: 164
currently lose_sum: 94.1541697382927
time_elpased: 2.319
batch start
#iterations: 165
currently lose_sum: 94.31200188398361
time_elpased: 2.245
batch start
#iterations: 166
currently lose_sum: 94.34300428628922
time_elpased: 2.287
batch start
#iterations: 167
currently lose_sum: 94.29672265052795
time_elpased: 2.432
batch start
#iterations: 168
currently lose_sum: 94.40772825479507
time_elpased: 2.414
batch start
#iterations: 169
currently lose_sum: 94.08377754688263
time_elpased: 2.319
batch start
#iterations: 170
currently lose_sum: 94.4860200881958
time_elpased: 2.363
batch start
#iterations: 171
currently lose_sum: 94.44549906253815
time_elpased: 2.421
batch start
#iterations: 172
currently lose_sum: 94.37379276752472
time_elpased: 2.416
batch start
#iterations: 173
currently lose_sum: 94.36143732070923
time_elpased: 2.672
batch start
#iterations: 174
currently lose_sum: 93.76762944459915
time_elpased: 2.469
batch start
#iterations: 175
currently lose_sum: 94.05850476026535
time_elpased: 2.455
batch start
#iterations: 176
currently lose_sum: 93.73358297348022
time_elpased: 2.488
batch start
#iterations: 177
currently lose_sum: 94.08614701032639
time_elpased: 2.346
batch start
#iterations: 178
currently lose_sum: 93.86537730693817
time_elpased: 2.471
batch start
#iterations: 179
currently lose_sum: 93.94640672206879
time_elpased: 2.435
start validation test
0.670515463918
0.642765135251
0.770299475147
0.700777080798
0.670340277695
60.462
batch start
#iterations: 180
currently lose_sum: 94.01343810558319
time_elpased: 2.366
batch start
#iterations: 181
currently lose_sum: 94.06482261419296
time_elpased: 2.422
batch start
#iterations: 182
currently lose_sum: 94.15164679288864
time_elpased: 2.381
batch start
#iterations: 183
currently lose_sum: 93.99843239784241
time_elpased: 2.432
batch start
#iterations: 184
currently lose_sum: 94.53908556699753
time_elpased: 2.421
batch start
#iterations: 185
currently lose_sum: 93.93517822027206
time_elpased: 2.473
batch start
#iterations: 186
currently lose_sum: 94.48973971605301
time_elpased: 2.439
batch start
#iterations: 187
currently lose_sum: 94.13879317045212
time_elpased: 2.438
batch start
#iterations: 188
currently lose_sum: 94.37184697389603
time_elpased: 2.445
batch start
#iterations: 189
currently lose_sum: 94.47625863552094
time_elpased: 2.396
batch start
#iterations: 190
currently lose_sum: 93.98198586702347
time_elpased: 2.449
batch start
#iterations: 191
currently lose_sum: 94.35440254211426
time_elpased: 2.456
batch start
#iterations: 192
currently lose_sum: 94.07530075311661
time_elpased: 2.442
batch start
#iterations: 193
currently lose_sum: 94.12178474664688
time_elpased: 2.421
batch start
#iterations: 194
currently lose_sum: 93.89423793554306
time_elpased: 2.42
batch start
#iterations: 195
currently lose_sum: 94.33778017759323
time_elpased: 2.372
batch start
#iterations: 196
currently lose_sum: 94.05741226673126
time_elpased: 2.467
batch start
#iterations: 197
currently lose_sum: 94.29337000846863
time_elpased: 2.387
batch start
#iterations: 198
currently lose_sum: 94.10548776388168
time_elpased: 2.395
batch start
#iterations: 199
currently lose_sum: 93.74242359399796
time_elpased: 2.398
start validation test
0.670412371134
0.645045831515
0.76041988268
0.697997354997
0.670254349065
60.197
batch start
#iterations: 200
currently lose_sum: 94.0918840765953
time_elpased: 2.355
batch start
#iterations: 201
currently lose_sum: 93.90521258115768
time_elpased: 2.34
batch start
#iterations: 202
currently lose_sum: 93.76416605710983
time_elpased: 2.407
batch start
#iterations: 203
currently lose_sum: 94.07045912742615
time_elpased: 2.381
batch start
#iterations: 204
currently lose_sum: 94.11322975158691
time_elpased: 2.407
batch start
#iterations: 205
currently lose_sum: 93.67445987462997
time_elpased: 2.271
batch start
#iterations: 206
currently lose_sum: 93.83961749076843
time_elpased: 2.268
batch start
#iterations: 207
currently lose_sum: 94.12938433885574
time_elpased: 2.378
batch start
#iterations: 208
currently lose_sum: 94.03832465410233
time_elpased: 2.323
batch start
#iterations: 209
currently lose_sum: 93.86809039115906
time_elpased: 2.295
batch start
#iterations: 210
currently lose_sum: 93.78194046020508
time_elpased: 2.368
batch start
#iterations: 211
currently lose_sum: 94.36432099342346
time_elpased: 2.358
batch start
#iterations: 212
currently lose_sum: 94.25782245397568
time_elpased: 2.456
batch start
#iterations: 213
currently lose_sum: 94.06794464588165
time_elpased: 2.478
batch start
#iterations: 214
currently lose_sum: 93.8754597902298
time_elpased: 2.502
batch start
#iterations: 215
currently lose_sum: 93.99747782945633
time_elpased: 2.497
batch start
#iterations: 216
currently lose_sum: 93.9521404504776
time_elpased: 2.318
batch start
#iterations: 217
currently lose_sum: 94.08450561761856
time_elpased: 2.426
batch start
#iterations: 218
currently lose_sum: 93.59240514039993
time_elpased: 2.447
batch start
#iterations: 219
currently lose_sum: 94.33975774049759
time_elpased: 2.483
start validation test
0.670051546392
0.644752924742
0.760008232994
0.697652449105
0.669893613554
60.510
batch start
#iterations: 220
currently lose_sum: 94.0117859840393
time_elpased: 2.442
batch start
#iterations: 221
currently lose_sum: 94.2565558552742
time_elpased: 2.467
batch start
#iterations: 222
currently lose_sum: 93.76314699649811
time_elpased: 2.354
batch start
#iterations: 223
currently lose_sum: 93.51245510578156
time_elpased: 2.398
batch start
#iterations: 224
currently lose_sum: 93.68347430229187
time_elpased: 2.37
batch start
#iterations: 225
currently lose_sum: 94.31008350849152
time_elpased: 2.397
batch start
#iterations: 226
currently lose_sum: 93.87069010734558
time_elpased: 2.369
batch start
#iterations: 227
currently lose_sum: 94.09738063812256
time_elpased: 2.314
batch start
#iterations: 228
currently lose_sum: 93.87644499540329
time_elpased: 2.294
batch start
#iterations: 229
currently lose_sum: 94.02642560005188
time_elpased: 2.349
batch start
#iterations: 230
currently lose_sum: 94.09572213888168
time_elpased: 2.374
batch start
#iterations: 231
currently lose_sum: 93.56509560346603
time_elpased: 2.404
batch start
#iterations: 232
currently lose_sum: 93.98793411254883
time_elpased: 2.373
batch start
#iterations: 233
currently lose_sum: 94.20140767097473
time_elpased: 2.295
batch start
#iterations: 234
currently lose_sum: 93.91046386957169
time_elpased: 2.296
batch start
#iterations: 235
currently lose_sum: 93.67467629909515
time_elpased: 2.313
batch start
#iterations: 236
currently lose_sum: 94.18099558353424
time_elpased: 2.33
batch start
#iterations: 237
currently lose_sum: 94.0125966668129
time_elpased: 2.34
batch start
#iterations: 238
currently lose_sum: 93.94627511501312
time_elpased: 2.241
batch start
#iterations: 239
currently lose_sum: 93.91995936632156
time_elpased: 2.258
start validation test
0.666958762887
0.644454303461
0.747452917567
0.692142755039
0.666817442983
60.384
batch start
#iterations: 240
currently lose_sum: 93.66612529754639
time_elpased: 2.459
batch start
#iterations: 241
currently lose_sum: 94.0916645526886
time_elpased: 2.441
batch start
#iterations: 242
currently lose_sum: 93.99039208889008
time_elpased: 2.414
batch start
#iterations: 243
currently lose_sum: 94.03096479177475
time_elpased: 2.337
batch start
#iterations: 244
currently lose_sum: 94.01743614673615
time_elpased: 2.343
batch start
#iterations: 245
currently lose_sum: 93.712546646595
time_elpased: 2.439
batch start
#iterations: 246
currently lose_sum: 94.05780237913132
time_elpased: 2.393
batch start
#iterations: 247
currently lose_sum: 93.5773851275444
time_elpased: 2.428
batch start
#iterations: 248
currently lose_sum: 93.79132670164108
time_elpased: 2.372
batch start
#iterations: 249
currently lose_sum: 93.88359636068344
time_elpased: 2.385
batch start
#iterations: 250
currently lose_sum: 93.67727756500244
time_elpased: 2.394
batch start
#iterations: 251
currently lose_sum: 93.78794395923615
time_elpased: 2.488
batch start
#iterations: 252
currently lose_sum: 93.89139479398727
time_elpased: 2.383
batch start
#iterations: 253
currently lose_sum: 93.72976297140121
time_elpased: 2.337
batch start
#iterations: 254
currently lose_sum: 94.0956249833107
time_elpased: 2.211
batch start
#iterations: 255
currently lose_sum: 94.13734805583954
time_elpased: 2.317
batch start
#iterations: 256
currently lose_sum: 93.62945431470871
time_elpased: 2.405
batch start
#iterations: 257
currently lose_sum: 93.91765773296356
time_elpased: 2.315
batch start
#iterations: 258
currently lose_sum: 93.99841290712357
time_elpased: 2.328
batch start
#iterations: 259
currently lose_sum: 93.76603543758392
time_elpased: 2.285
start validation test
0.669072164948
0.645537211971
0.752495626222
0.694924919217
0.668925702195
60.078
batch start
#iterations: 260
currently lose_sum: 93.56575435400009
time_elpased: 2.173
batch start
#iterations: 261
currently lose_sum: 93.21522504091263
time_elpased: 2.283
batch start
#iterations: 262
currently lose_sum: 94.11772984266281
time_elpased: 2.343
batch start
#iterations: 263
currently lose_sum: 94.03591269254684
time_elpased: 2.383
batch start
#iterations: 264
currently lose_sum: 93.81763404607773
time_elpased: 2.291
batch start
#iterations: 265
currently lose_sum: 93.86652493476868
time_elpased: 2.225
batch start
#iterations: 266
currently lose_sum: 93.50311821699142
time_elpased: 2.306
batch start
#iterations: 267
currently lose_sum: 93.91909199953079
time_elpased: 2.333
batch start
#iterations: 268
currently lose_sum: 93.7909020781517
time_elpased: 2.445
batch start
#iterations: 269
currently lose_sum: 93.66721022129059
time_elpased: 2.368
batch start
#iterations: 270
currently lose_sum: 93.77130138874054
time_elpased: 2.296
batch start
#iterations: 271
currently lose_sum: 93.87060612440109
time_elpased: 2.222
batch start
#iterations: 272
currently lose_sum: 93.61158680915833
time_elpased: 2.358
batch start
#iterations: 273
currently lose_sum: 93.43006813526154
time_elpased: 2.359
batch start
#iterations: 274
currently lose_sum: 94.13707602024078
time_elpased: 2.337
batch start
#iterations: 275
currently lose_sum: 93.94863945245743
time_elpased: 2.259
batch start
#iterations: 276
currently lose_sum: 93.93153870105743
time_elpased: 2.282
batch start
#iterations: 277
currently lose_sum: 93.55879271030426
time_elpased: 2.291
batch start
#iterations: 278
currently lose_sum: 93.53749310970306
time_elpased: 2.293
batch start
#iterations: 279
currently lose_sum: 93.93458533287048
time_elpased: 2.307
start validation test
0.669381443299
0.642481235441
0.766388803129
0.698986296227
0.669211131916
60.349
batch start
#iterations: 280
currently lose_sum: 93.87082231044769
time_elpased: 2.332
batch start
#iterations: 281
currently lose_sum: 93.71493089199066
time_elpased: 2.287
batch start
#iterations: 282
currently lose_sum: 93.84493798017502
time_elpased: 2.189
batch start
#iterations: 283
currently lose_sum: 93.84375071525574
time_elpased: 2.275
batch start
#iterations: 284
currently lose_sum: 93.55260252952576
time_elpased: 2.319
batch start
#iterations: 285
currently lose_sum: 93.67736673355103
time_elpased: 2.328
batch start
#iterations: 286
currently lose_sum: 93.61201453208923
time_elpased: 2.276
batch start
#iterations: 287
currently lose_sum: 93.58684545755386
time_elpased: 2.268
batch start
#iterations: 288
currently lose_sum: 93.98630970716476
time_elpased: 2.308
batch start
#iterations: 289
currently lose_sum: 93.33578222990036
time_elpased: 2.307
batch start
#iterations: 290
currently lose_sum: 93.77607858181
time_elpased: 2.307
batch start
#iterations: 291
currently lose_sum: 93.9522288441658
time_elpased: 2.352
batch start
#iterations: 292
currently lose_sum: 93.71737790107727
time_elpased: 2.292
batch start
#iterations: 293
currently lose_sum: 93.50621116161346
time_elpased: 2.213
batch start
#iterations: 294
currently lose_sum: 93.67583173513412
time_elpased: 2.235
batch start
#iterations: 295
currently lose_sum: 94.19082820415497
time_elpased: 2.353
batch start
#iterations: 296
currently lose_sum: 93.73245471715927
time_elpased: 2.299
batch start
#iterations: 297
currently lose_sum: 93.94297313690186
time_elpased: 2.218
batch start
#iterations: 298
currently lose_sum: 93.96779298782349
time_elpased: 2.26
batch start
#iterations: 299
currently lose_sum: 93.66856473684311
time_elpased: 2.269
start validation test
0.668608247423
0.646315414738
0.747350005146
0.693170429056
0.668470004122
60.294
batch start
#iterations: 300
currently lose_sum: 93.73211508989334
time_elpased: 2.291
batch start
#iterations: 301
currently lose_sum: 94.02334189414978
time_elpased: 2.326
batch start
#iterations: 302
currently lose_sum: 93.574498295784
time_elpased: 2.316
batch start
#iterations: 303
currently lose_sum: 93.75202983617783
time_elpased: 2.301
batch start
#iterations: 304
currently lose_sum: 93.7748510837555
time_elpased: 2.188
batch start
#iterations: 305
currently lose_sum: 93.5203185081482
time_elpased: 2.205
batch start
#iterations: 306
currently lose_sum: 93.63857644796371
time_elpased: 2.314
batch start
#iterations: 307
currently lose_sum: 94.18220865726471
time_elpased: 2.243
batch start
#iterations: 308
currently lose_sum: 93.26891630887985
time_elpased: 2.235
batch start
#iterations: 309
currently lose_sum: 93.60065668821335
time_elpased: 2.295
batch start
#iterations: 310
currently lose_sum: 94.01318347454071
time_elpased: 2.308
batch start
#iterations: 311
currently lose_sum: 93.6837706565857
time_elpased: 2.307
batch start
#iterations: 312
currently lose_sum: 93.51830279827118
time_elpased: 2.307
batch start
#iterations: 313
currently lose_sum: 93.9917620420456
time_elpased: 2.297
batch start
#iterations: 314
currently lose_sum: 93.78667312860489
time_elpased: 2.341
batch start
#iterations: 315
currently lose_sum: 93.57383197546005
time_elpased: 2.259
batch start
#iterations: 316
currently lose_sum: 93.68527698516846
time_elpased: 2.155
batch start
#iterations: 317
currently lose_sum: 93.50516837835312
time_elpased: 2.281
batch start
#iterations: 318
currently lose_sum: 93.76157468557358
time_elpased: 2.24
batch start
#iterations: 319
currently lose_sum: 93.88575851917267
time_elpased: 2.244
start validation test
0.667783505155
0.647069399497
0.740763610168
0.690753802601
0.667655377324
60.379
batch start
#iterations: 320
currently lose_sum: 93.44644850492477
time_elpased: 2.344
batch start
#iterations: 321
currently lose_sum: 93.68178880214691
time_elpased: 2.386
batch start
#iterations: 322
currently lose_sum: 93.94208353757858
time_elpased: 2.293
batch start
#iterations: 323
currently lose_sum: 93.88880169391632
time_elpased: 2.307
batch start
#iterations: 324
currently lose_sum: 93.56086927652359
time_elpased: 2.305
batch start
#iterations: 325
currently lose_sum: 93.85460549592972
time_elpased: 2.342
batch start
#iterations: 326
currently lose_sum: 93.46886730194092
time_elpased: 2.291
batch start
#iterations: 327
currently lose_sum: 93.61717140674591
time_elpased: 2.154
batch start
#iterations: 328
currently lose_sum: 93.63714629411697
time_elpased: 2.272
batch start
#iterations: 329
currently lose_sum: 93.49352043867111
time_elpased: 2.166
batch start
#iterations: 330
currently lose_sum: 94.10979348421097
time_elpased: 2.329
batch start
#iterations: 331
currently lose_sum: 93.495765209198
time_elpased: 2.287
batch start
#iterations: 332
currently lose_sum: 93.1319095492363
time_elpased: 2.306
batch start
#iterations: 333
currently lose_sum: 93.76023197174072
time_elpased: 2.284
batch start
#iterations: 334
currently lose_sum: 93.35907089710236
time_elpased: 2.308
batch start
#iterations: 335
currently lose_sum: 93.60111624002457
time_elpased: 2.283
batch start
#iterations: 336
currently lose_sum: 93.5331215262413
time_elpased: 2.325
batch start
#iterations: 337
currently lose_sum: 94.11397582292557
time_elpased: 2.303
batch start
#iterations: 338
currently lose_sum: 93.61056131124496
time_elpased: 2.222
batch start
#iterations: 339
currently lose_sum: 93.77096420526505
time_elpased: 2.24
start validation test
0.670360824742
0.639720726783
0.78264896573
0.704003702847
0.670163685592
60.211
batch start
#iterations: 340
currently lose_sum: 93.83798295259476
time_elpased: 2.224
batch start
#iterations: 341
currently lose_sum: 93.84829330444336
time_elpased: 2.27
batch start
#iterations: 342
currently lose_sum: 94.03766876459122
time_elpased: 2.346
batch start
#iterations: 343
currently lose_sum: 93.55447387695312
time_elpased: 2.281
batch start
#iterations: 344
currently lose_sum: 93.55972284078598
time_elpased: 2.289
batch start
#iterations: 345
currently lose_sum: 93.75249254703522
time_elpased: 2.254
batch start
#iterations: 346
currently lose_sum: 93.46570718288422
time_elpased: 2.3
batch start
#iterations: 347
currently lose_sum: 93.98639392852783
time_elpased: 2.301
batch start
#iterations: 348
currently lose_sum: 93.61323970556259
time_elpased: 2.304
batch start
#iterations: 349
currently lose_sum: 94.3022552728653
time_elpased: 2.189
batch start
#iterations: 350
currently lose_sum: 93.50414168834686
time_elpased: 2.195
batch start
#iterations: 351
currently lose_sum: 93.62973767518997
time_elpased: 2.27
batch start
#iterations: 352
currently lose_sum: 93.57970696687698
time_elpased: 2.309
batch start
#iterations: 353
currently lose_sum: 93.40177589654922
time_elpased: 2.322
batch start
#iterations: 354
currently lose_sum: 93.87881290912628
time_elpased: 2.285
batch start
#iterations: 355
currently lose_sum: 93.45455372333527
time_elpased: 2.318
batch start
#iterations: 356
currently lose_sum: 94.02731543779373
time_elpased: 2.33
batch start
#iterations: 357
currently lose_sum: 93.71985340118408
time_elpased: 2.284
batch start
#iterations: 358
currently lose_sum: 93.52094680070877
time_elpased: 2.275
batch start
#iterations: 359
currently lose_sum: 93.54677599668503
time_elpased: 2.275
start validation test
0.670154639175
0.643115942029
0.767212102501
0.699704350275
0.669984239828
60.239
batch start
#iterations: 360
currently lose_sum: 93.94281989336014
time_elpased: 2.29
batch start
#iterations: 361
currently lose_sum: 94.00413084030151
time_elpased: 2.155
batch start
#iterations: 362
currently lose_sum: 93.59076756238937
time_elpased: 2.233
batch start
#iterations: 363
currently lose_sum: 93.51506334543228
time_elpased: 2.292
batch start
#iterations: 364
currently lose_sum: 93.52336978912354
time_elpased: 2.303
batch start
#iterations: 365
currently lose_sum: 94.17280578613281
time_elpased: 2.258
batch start
#iterations: 366
currently lose_sum: 93.61960005760193
time_elpased: 2.282
batch start
#iterations: 367
currently lose_sum: 93.70869809389114
time_elpased: 2.37
batch start
#iterations: 368
currently lose_sum: 93.74844390153885
time_elpased: 2.321
batch start
#iterations: 369
currently lose_sum: 94.0521023273468
time_elpased: 2.277
batch start
#iterations: 370
currently lose_sum: 93.44191294908524
time_elpased: 2.292
batch start
#iterations: 371
currently lose_sum: 93.47243523597717
time_elpased: 2.344
batch start
#iterations: 372
currently lose_sum: 93.12110662460327
time_elpased: 2.173
batch start
#iterations: 373
currently lose_sum: 93.98932409286499
time_elpased: 2.193
batch start
#iterations: 374
currently lose_sum: 93.9383088350296
time_elpased: 2.259
batch start
#iterations: 375
currently lose_sum: 93.1258345246315
time_elpased: 2.342
batch start
#iterations: 376
currently lose_sum: 94.0183442234993
time_elpased: 2.298
batch start
#iterations: 377
currently lose_sum: 93.88807988166809
time_elpased: 2.281
batch start
#iterations: 378
currently lose_sum: 93.63955295085907
time_elpased: 2.31
batch start
#iterations: 379
currently lose_sum: 93.5722461938858
time_elpased: 2.283
start validation test
0.672010309278
0.642311608961
0.778944118555
0.704060276266
0.671822570483
60.145
batch start
#iterations: 380
currently lose_sum: 94.02673053741455
time_elpased: 2.338
batch start
#iterations: 381
currently lose_sum: 93.49598342180252
time_elpased: 2.302
batch start
#iterations: 382
currently lose_sum: 93.97201657295227
time_elpased: 2.318
batch start
#iterations: 383
currently lose_sum: 93.77999269962311
time_elpased: 2.225
batch start
#iterations: 384
currently lose_sum: 94.09918349981308
time_elpased: 2.165
batch start
#iterations: 385
currently lose_sum: 93.78940153121948
time_elpased: 2.244
batch start
#iterations: 386
currently lose_sum: 93.6354348063469
time_elpased: 2.284
batch start
#iterations: 387
currently lose_sum: 94.16999226808548
time_elpased: 2.353
batch start
#iterations: 388
currently lose_sum: 93.85022842884064
time_elpased: 2.264
batch start
#iterations: 389
currently lose_sum: 93.83515727519989
time_elpased: 2.333
batch start
#iterations: 390
currently lose_sum: 93.8792832493782
time_elpased: 2.319
batch start
#iterations: 391
currently lose_sum: 93.79937320947647
time_elpased: 2.309
batch start
#iterations: 392
currently lose_sum: 93.52997589111328
time_elpased: 2.332
batch start
#iterations: 393
currently lose_sum: 93.54287856817245
time_elpased: 2.356
batch start
#iterations: 394
currently lose_sum: 93.58486366271973
time_elpased: 2.328
batch start
#iterations: 395
currently lose_sum: 93.4544329047203
time_elpased: 2.179
batch start
#iterations: 396
currently lose_sum: 93.27079600095749
time_elpased: 2.215
batch start
#iterations: 397
currently lose_sum: 93.68355149030685
time_elpased: 2.335
batch start
#iterations: 398
currently lose_sum: 93.46078377962112
time_elpased: 2.336
batch start
#iterations: 399
currently lose_sum: 93.9420907497406
time_elpased: 2.221
start validation test
0.668556701031
0.642603036876
0.762169393846
0.697297806233
0.66839234951
60.332
acc: 0.668
pre: 0.646
rec: 0.746
F1: 0.693
auc: 0.668
