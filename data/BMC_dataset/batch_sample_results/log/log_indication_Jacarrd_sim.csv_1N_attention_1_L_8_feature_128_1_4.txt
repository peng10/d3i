start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.13668859004974
time_elpased: 2.027
batch start
#iterations: 1
currently lose_sum: 99.3181546330452
time_elpased: 1.883
batch start
#iterations: 2
currently lose_sum: 99.28550499677658
time_elpased: 1.917
batch start
#iterations: 3
currently lose_sum: 98.6380785703659
time_elpased: 1.9
batch start
#iterations: 4
currently lose_sum: 98.5704118013382
time_elpased: 1.91
batch start
#iterations: 5
currently lose_sum: 98.05701577663422
time_elpased: 1.975
batch start
#iterations: 6
currently lose_sum: 97.98031461238861
time_elpased: 1.891
batch start
#iterations: 7
currently lose_sum: 97.55616277456284
time_elpased: 1.942
batch start
#iterations: 8
currently lose_sum: 97.44865107536316
time_elpased: 1.952
batch start
#iterations: 9
currently lose_sum: 97.1448096036911
time_elpased: 1.897
batch start
#iterations: 10
currently lose_sum: 97.06073015928268
time_elpased: 1.894
batch start
#iterations: 11
currently lose_sum: 97.18547803163528
time_elpased: 1.96
batch start
#iterations: 12
currently lose_sum: 96.8744952082634
time_elpased: 1.904
batch start
#iterations: 13
currently lose_sum: 96.94154518842697
time_elpased: 1.935
batch start
#iterations: 14
currently lose_sum: 96.47904914617538
time_elpased: 1.938
batch start
#iterations: 15
currently lose_sum: 96.63909089565277
time_elpased: 1.922
batch start
#iterations: 16
currently lose_sum: 96.84721034765244
time_elpased: 1.907
batch start
#iterations: 17
currently lose_sum: 96.49202263355255
time_elpased: 1.893
batch start
#iterations: 18
currently lose_sum: 96.13269048929214
time_elpased: 1.919
batch start
#iterations: 19
currently lose_sum: 96.5231192111969
time_elpased: 1.902
start validation test
0.661958762887
0.643656207367
0.728311207163
0.683371958285
0.661842270937
61.731
batch start
#iterations: 20
currently lose_sum: 96.61558222770691
time_elpased: 1.921
batch start
#iterations: 21
currently lose_sum: 95.74409985542297
time_elpased: 1.909
batch start
#iterations: 22
currently lose_sum: 96.42253410816193
time_elpased: 1.891
batch start
#iterations: 23
currently lose_sum: 96.499626994133
time_elpased: 1.92
batch start
#iterations: 24
currently lose_sum: 95.81168472766876
time_elpased: 1.89
batch start
#iterations: 25
currently lose_sum: 95.89447313547134
time_elpased: 1.879
batch start
#iterations: 26
currently lose_sum: 95.94625014066696
time_elpased: 1.959
batch start
#iterations: 27
currently lose_sum: 95.47102558612823
time_elpased: 1.907
batch start
#iterations: 28
currently lose_sum: 95.81242686510086
time_elpased: 1.909
batch start
#iterations: 29
currently lose_sum: 95.55157554149628
time_elpased: 1.908
batch start
#iterations: 30
currently lose_sum: 95.63608568906784
time_elpased: 1.895
batch start
#iterations: 31
currently lose_sum: 95.76434135437012
time_elpased: 1.908
batch start
#iterations: 32
currently lose_sum: 95.55966204404831
time_elpased: 1.927
batch start
#iterations: 33
currently lose_sum: 95.48962610960007
time_elpased: 1.912
batch start
#iterations: 34
currently lose_sum: 95.70743095874786
time_elpased: 1.917
batch start
#iterations: 35
currently lose_sum: 95.2827776670456
time_elpased: 1.913
batch start
#iterations: 36
currently lose_sum: 95.76124501228333
time_elpased: 1.906
batch start
#iterations: 37
currently lose_sum: 95.24337810277939
time_elpased: 1.898
batch start
#iterations: 38
currently lose_sum: 95.2997704744339
time_elpased: 1.918
batch start
#iterations: 39
currently lose_sum: 95.14204758405685
time_elpased: 1.886
start validation test
0.671907216495
0.649991050653
0.747452917567
0.695323344981
0.671774584364
60.481
batch start
#iterations: 40
currently lose_sum: 95.61530357599258
time_elpased: 1.943
batch start
#iterations: 41
currently lose_sum: 95.27903372049332
time_elpased: 1.93
batch start
#iterations: 42
currently lose_sum: 95.30105632543564
time_elpased: 1.912
batch start
#iterations: 43
currently lose_sum: 95.42500066757202
time_elpased: 1.897
batch start
#iterations: 44
currently lose_sum: 94.95450967550278
time_elpased: 1.909
batch start
#iterations: 45
currently lose_sum: 95.22484093904495
time_elpased: 1.892
batch start
#iterations: 46
currently lose_sum: 95.12234872579575
time_elpased: 1.885
batch start
#iterations: 47
currently lose_sum: 95.3384661078453
time_elpased: 1.907
batch start
#iterations: 48
currently lose_sum: 95.24751883745193
time_elpased: 1.901
batch start
#iterations: 49
currently lose_sum: 95.08480721712112
time_elpased: 1.906
batch start
#iterations: 50
currently lose_sum: 94.78577822446823
time_elpased: 1.964
batch start
#iterations: 51
currently lose_sum: 95.12066298723221
time_elpased: 1.92
batch start
#iterations: 52
currently lose_sum: 95.28359842300415
time_elpased: 1.92
batch start
#iterations: 53
currently lose_sum: 94.86482268571854
time_elpased: 1.919
batch start
#iterations: 54
currently lose_sum: 94.94194155931473
time_elpased: 1.923
batch start
#iterations: 55
currently lose_sum: 94.84831315279007
time_elpased: 1.953
batch start
#iterations: 56
currently lose_sum: 94.91664564609528
time_elpased: 1.911
batch start
#iterations: 57
currently lose_sum: 94.97250366210938
time_elpased: 1.919
batch start
#iterations: 58
currently lose_sum: 95.07059723138809
time_elpased: 1.938
batch start
#iterations: 59
currently lose_sum: 95.35113620758057
time_elpased: 1.92
start validation test
0.659690721649
0.653827160494
0.681280230524
0.667271444411
0.659652817937
60.536
batch start
#iterations: 60
currently lose_sum: 95.25761389732361
time_elpased: 1.959
batch start
#iterations: 61
currently lose_sum: 95.07276737689972
time_elpased: 1.926
batch start
#iterations: 62
currently lose_sum: 94.97085690498352
time_elpased: 1.929
batch start
#iterations: 63
currently lose_sum: 94.82844114303589
time_elpased: 1.949
batch start
#iterations: 64
currently lose_sum: 95.21064966917038
time_elpased: 1.929
batch start
#iterations: 65
currently lose_sum: 94.95911902189255
time_elpased: 1.894
batch start
#iterations: 66
currently lose_sum: 95.02865010499954
time_elpased: 1.932
batch start
#iterations: 67
currently lose_sum: 94.84687304496765
time_elpased: 1.914
batch start
#iterations: 68
currently lose_sum: 94.63673251867294
time_elpased: 1.89
batch start
#iterations: 69
currently lose_sum: 94.97506481409073
time_elpased: 1.897
batch start
#iterations: 70
currently lose_sum: 94.92271876335144
time_elpased: 1.914
batch start
#iterations: 71
currently lose_sum: 94.97891116142273
time_elpased: 1.9
batch start
#iterations: 72
currently lose_sum: 94.44357430934906
time_elpased: 1.917
batch start
#iterations: 73
currently lose_sum: 94.81448060274124
time_elpased: 1.943
batch start
#iterations: 74
currently lose_sum: 94.3628026843071
time_elpased: 2.023
batch start
#iterations: 75
currently lose_sum: 95.214060485363
time_elpased: 1.923
batch start
#iterations: 76
currently lose_sum: 94.52269738912582
time_elpased: 1.897
batch start
#iterations: 77
currently lose_sum: 94.75071305036545
time_elpased: 1.878
batch start
#iterations: 78
currently lose_sum: 94.99939692020416
time_elpased: 1.928
batch start
#iterations: 79
currently lose_sum: 94.43005007505417
time_elpased: 1.898
start validation test
0.668402061856
0.65172796156
0.725841309046
0.686790983008
0.668301218398
60.066
batch start
#iterations: 80
currently lose_sum: 94.34352594614029
time_elpased: 1.924
batch start
#iterations: 81
currently lose_sum: 94.5018315911293
time_elpased: 1.89
batch start
#iterations: 82
currently lose_sum: 95.14459228515625
time_elpased: 1.938
batch start
#iterations: 83
currently lose_sum: 94.6815687417984
time_elpased: 1.926
batch start
#iterations: 84
currently lose_sum: 94.79645907878876
time_elpased: 1.914
batch start
#iterations: 85
currently lose_sum: 94.78247863054276
time_elpased: 1.939
batch start
#iterations: 86
currently lose_sum: 95.13660687208176
time_elpased: 1.936
batch start
#iterations: 87
currently lose_sum: 94.3538190126419
time_elpased: 1.905
batch start
#iterations: 88
currently lose_sum: 94.55572974681854
time_elpased: 1.907
batch start
#iterations: 89
currently lose_sum: 94.43112128973007
time_elpased: 1.936
batch start
#iterations: 90
currently lose_sum: 94.07039350271225
time_elpased: 1.919
batch start
#iterations: 91
currently lose_sum: 94.96914184093475
time_elpased: 1.932
batch start
#iterations: 92
currently lose_sum: 94.75016140937805
time_elpased: 1.932
batch start
#iterations: 93
currently lose_sum: 94.01238995790482
time_elpased: 1.944
batch start
#iterations: 94
currently lose_sum: 94.47975689172745
time_elpased: 1.963
batch start
#iterations: 95
currently lose_sum: 94.49121975898743
time_elpased: 1.939
batch start
#iterations: 96
currently lose_sum: 94.52317994832993
time_elpased: 1.985
batch start
#iterations: 97
currently lose_sum: 94.59933167695999
time_elpased: 1.991
batch start
#iterations: 98
currently lose_sum: 94.67502272129059
time_elpased: 1.962
batch start
#iterations: 99
currently lose_sum: 94.72427690029144
time_elpased: 1.934
start validation test
0.673144329897
0.647423580786
0.762889780797
0.70042991449
0.672986767916
60.071
batch start
#iterations: 100
currently lose_sum: 94.32052618265152
time_elpased: 1.997
batch start
#iterations: 101
currently lose_sum: 94.36753123998642
time_elpased: 1.954
batch start
#iterations: 102
currently lose_sum: 94.25712984800339
time_elpased: 1.939
batch start
#iterations: 103
currently lose_sum: 94.62017035484314
time_elpased: 1.95
batch start
#iterations: 104
currently lose_sum: 94.19384998083115
time_elpased: 2.0
batch start
#iterations: 105
currently lose_sum: 94.49590128660202
time_elpased: 1.938
batch start
#iterations: 106
currently lose_sum: 94.66730850934982
time_elpased: 1.941
batch start
#iterations: 107
currently lose_sum: 94.25984317064285
time_elpased: 1.963
batch start
#iterations: 108
currently lose_sum: 94.78120166063309
time_elpased: 1.98
batch start
#iterations: 109
currently lose_sum: 94.79786068201065
time_elpased: 1.931
batch start
#iterations: 110
currently lose_sum: 94.17854255437851
time_elpased: 1.917
batch start
#iterations: 111
currently lose_sum: 94.14372563362122
time_elpased: 1.924
batch start
#iterations: 112
currently lose_sum: 94.3051415681839
time_elpased: 1.926
batch start
#iterations: 113
currently lose_sum: 94.11330062150955
time_elpased: 1.972
batch start
#iterations: 114
currently lose_sum: 94.38229846954346
time_elpased: 1.97
batch start
#iterations: 115
currently lose_sum: 93.94476932287216
time_elpased: 1.968
batch start
#iterations: 116
currently lose_sum: 94.36441105604172
time_elpased: 1.948
batch start
#iterations: 117
currently lose_sum: 94.40192830562592
time_elpased: 1.912
batch start
#iterations: 118
currently lose_sum: 94.54934567213058
time_elpased: 1.939
batch start
#iterations: 119
currently lose_sum: 93.89825683832169
time_elpased: 1.961
start validation test
0.676237113402
0.653201355449
0.753833487702
0.699918780756
0.676100880999
59.807
batch start
#iterations: 120
currently lose_sum: 94.19048923254013
time_elpased: 1.948
batch start
#iterations: 121
currently lose_sum: 94.56654769182205
time_elpased: 1.962
batch start
#iterations: 122
currently lose_sum: 94.65135687589645
time_elpased: 1.925
batch start
#iterations: 123
currently lose_sum: 94.29941815137863
time_elpased: 1.956
batch start
#iterations: 124
currently lose_sum: 94.33187067508698
time_elpased: 1.98
batch start
#iterations: 125
currently lose_sum: 93.96561080217361
time_elpased: 1.963
batch start
#iterations: 126
currently lose_sum: 94.03504890203476
time_elpased: 1.975
batch start
#iterations: 127
currently lose_sum: 94.27962589263916
time_elpased: 1.957
batch start
#iterations: 128
currently lose_sum: 94.04993134737015
time_elpased: 1.925
batch start
#iterations: 129
currently lose_sum: 94.06986105442047
time_elpased: 1.956
batch start
#iterations: 130
currently lose_sum: 94.2437635064125
time_elpased: 1.943
batch start
#iterations: 131
currently lose_sum: 94.32145392894745
time_elpased: 1.969
batch start
#iterations: 132
currently lose_sum: 94.60871982574463
time_elpased: 1.942
batch start
#iterations: 133
currently lose_sum: 94.12651097774506
time_elpased: 1.948
batch start
#iterations: 134
currently lose_sum: 94.53813087940216
time_elpased: 1.963
batch start
#iterations: 135
currently lose_sum: 94.3639788031578
time_elpased: 1.923
batch start
#iterations: 136
currently lose_sum: 93.73502683639526
time_elpased: 1.948
batch start
#iterations: 137
currently lose_sum: 94.31968885660172
time_elpased: 1.948
batch start
#iterations: 138
currently lose_sum: 94.31607550382614
time_elpased: 1.902
batch start
#iterations: 139
currently lose_sum: 94.47011226415634
time_elpased: 1.955
start validation test
0.677113402062
0.649700858406
0.771122774519
0.705223529412
0.676948354109
59.752
batch start
#iterations: 140
currently lose_sum: 94.43779468536377
time_elpased: 1.966
batch start
#iterations: 141
currently lose_sum: 94.1527704000473
time_elpased: 1.991
batch start
#iterations: 142
currently lose_sum: 94.4460837841034
time_elpased: 1.927
batch start
#iterations: 143
currently lose_sum: 93.84355944395065
time_elpased: 1.925
batch start
#iterations: 144
currently lose_sum: 94.18563461303711
time_elpased: 1.968
batch start
#iterations: 145
currently lose_sum: 94.21206837892532
time_elpased: 1.98
batch start
#iterations: 146
currently lose_sum: 93.90141350030899
time_elpased: 1.97
batch start
#iterations: 147
currently lose_sum: 94.51171296834946
time_elpased: 1.955
batch start
#iterations: 148
currently lose_sum: 94.39569932222366
time_elpased: 1.948
batch start
#iterations: 149
currently lose_sum: 94.15427720546722
time_elpased: 1.983
batch start
#iterations: 150
currently lose_sum: 93.87766379117966
time_elpased: 1.936
batch start
#iterations: 151
currently lose_sum: 94.33450841903687
time_elpased: 1.957
batch start
#iterations: 152
currently lose_sum: 93.91284269094467
time_elpased: 1.947
batch start
#iterations: 153
currently lose_sum: 94.36188113689423
time_elpased: 1.933
batch start
#iterations: 154
currently lose_sum: 93.76833999156952
time_elpased: 1.954
batch start
#iterations: 155
currently lose_sum: 93.74468797445297
time_elpased: 1.926
batch start
#iterations: 156
currently lose_sum: 94.62764072418213
time_elpased: 1.931
batch start
#iterations: 157
currently lose_sum: 94.27079600095749
time_elpased: 1.903
batch start
#iterations: 158
currently lose_sum: 94.50242525339127
time_elpased: 1.936
batch start
#iterations: 159
currently lose_sum: 94.2451514005661
time_elpased: 1.928
start validation test
0.676082474227
0.652972105873
0.754039312545
0.699875823861
0.675945608973
59.874
batch start
#iterations: 160
currently lose_sum: 93.96926951408386
time_elpased: 1.972
batch start
#iterations: 161
currently lose_sum: 93.88035809993744
time_elpased: 1.926
batch start
#iterations: 162
currently lose_sum: 94.03814089298248
time_elpased: 1.916
batch start
#iterations: 163
currently lose_sum: 94.04544061422348
time_elpased: 1.926
batch start
#iterations: 164
currently lose_sum: 94.5123382806778
time_elpased: 1.96
batch start
#iterations: 165
currently lose_sum: 93.8902171254158
time_elpased: 1.925
batch start
#iterations: 166
currently lose_sum: 93.90423959493637
time_elpased: 1.962
batch start
#iterations: 167
currently lose_sum: 93.94488769769669
time_elpased: 1.922
batch start
#iterations: 168
currently lose_sum: 94.49971836805344
time_elpased: 1.942
batch start
#iterations: 169
currently lose_sum: 94.164863884449
time_elpased: 1.935
batch start
#iterations: 170
currently lose_sum: 93.52706187963486
time_elpased: 1.945
batch start
#iterations: 171
currently lose_sum: 93.78447538614273
time_elpased: 1.977
batch start
#iterations: 172
currently lose_sum: 93.94632261991501
time_elpased: 2.075
batch start
#iterations: 173
currently lose_sum: 94.01933282613754
time_elpased: 1.923
batch start
#iterations: 174
currently lose_sum: 94.18613243103027
time_elpased: 1.958
batch start
#iterations: 175
currently lose_sum: 94.27305936813354
time_elpased: 1.998
batch start
#iterations: 176
currently lose_sum: 94.23012697696686
time_elpased: 1.921
batch start
#iterations: 177
currently lose_sum: 94.07626301050186
time_elpased: 1.96
batch start
#iterations: 178
currently lose_sum: 93.88601499795914
time_elpased: 1.98
batch start
#iterations: 179
currently lose_sum: 93.98416966199875
time_elpased: 1.924
start validation test
0.676494845361
0.653602356932
0.753421838016
0.699971316569
0.67635978816
59.766
batch start
#iterations: 180
currently lose_sum: 93.88934993743896
time_elpased: 1.998
batch start
#iterations: 181
currently lose_sum: 93.9967411160469
time_elpased: 1.917
batch start
#iterations: 182
currently lose_sum: 93.67495507001877
time_elpased: 1.891
batch start
#iterations: 183
currently lose_sum: 93.72260743379593
time_elpased: 1.937
batch start
#iterations: 184
currently lose_sum: 93.85294508934021
time_elpased: 1.951
batch start
#iterations: 185
currently lose_sum: 94.03521823883057
time_elpased: 2.0
batch start
#iterations: 186
currently lose_sum: 93.70358443260193
time_elpased: 1.971
batch start
#iterations: 187
currently lose_sum: 93.97519981861115
time_elpased: 2.0
batch start
#iterations: 188
currently lose_sum: 93.62622475624084
time_elpased: 1.935
batch start
#iterations: 189
currently lose_sum: 93.83684086799622
time_elpased: 1.936
batch start
#iterations: 190
currently lose_sum: 93.72494202852249
time_elpased: 1.911
batch start
#iterations: 191
currently lose_sum: 93.99828881025314
time_elpased: 1.931
batch start
#iterations: 192
currently lose_sum: 94.1488419175148
time_elpased: 1.913
batch start
#iterations: 193
currently lose_sum: 94.40078854560852
time_elpased: 1.926
batch start
#iterations: 194
currently lose_sum: 93.6601591706276
time_elpased: 1.96
batch start
#iterations: 195
currently lose_sum: 93.83225113153458
time_elpased: 1.943
batch start
#iterations: 196
currently lose_sum: 93.30570548772812
time_elpased: 1.952
batch start
#iterations: 197
currently lose_sum: 93.90233498811722
time_elpased: 1.999
batch start
#iterations: 198
currently lose_sum: 93.85972201824188
time_elpased: 1.992
batch start
#iterations: 199
currently lose_sum: 94.11066114902496
time_elpased: 1.96
start validation test
0.676288659794
0.647447447447
0.77657713286
0.706157589369
0.676112587911
59.984
batch start
#iterations: 200
currently lose_sum: 93.99350625276566
time_elpased: 1.957
batch start
#iterations: 201
currently lose_sum: 93.83508813381195
time_elpased: 1.946
batch start
#iterations: 202
currently lose_sum: 93.50748372077942
time_elpased: 1.918
batch start
#iterations: 203
currently lose_sum: 94.06405556201935
time_elpased: 1.933
batch start
#iterations: 204
currently lose_sum: 93.67113941907883
time_elpased: 1.962
batch start
#iterations: 205
currently lose_sum: 93.77180516719818
time_elpased: 1.93
batch start
#iterations: 206
currently lose_sum: 94.26502829790115
time_elpased: 1.922
batch start
#iterations: 207
currently lose_sum: 94.03647977113724
time_elpased: 1.941
batch start
#iterations: 208
currently lose_sum: 94.07403653860092
time_elpased: 1.947
batch start
#iterations: 209
currently lose_sum: 93.60025036334991
time_elpased: 1.978
batch start
#iterations: 210
currently lose_sum: 93.73636603355408
time_elpased: 1.962
batch start
#iterations: 211
currently lose_sum: 93.41694349050522
time_elpased: 1.952
batch start
#iterations: 212
currently lose_sum: 93.68966209888458
time_elpased: 1.924
batch start
#iterations: 213
currently lose_sum: 93.53996855020523
time_elpased: 1.947
batch start
#iterations: 214
currently lose_sum: 93.98083710670471
time_elpased: 1.979
batch start
#iterations: 215
currently lose_sum: 93.89970874786377
time_elpased: 1.978
batch start
#iterations: 216
currently lose_sum: 93.82393270730972
time_elpased: 1.996
batch start
#iterations: 217
currently lose_sum: 93.68298804759979
time_elpased: 1.945
batch start
#iterations: 218
currently lose_sum: 93.5838937163353
time_elpased: 1.949
batch start
#iterations: 219
currently lose_sum: 93.80317634344101
time_elpased: 2.05
start validation test
0.672628865979
0.651348920863
0.745394669137
0.695205643807
0.672501114389
59.835
batch start
#iterations: 220
currently lose_sum: 93.76627188920975
time_elpased: 1.954
batch start
#iterations: 221
currently lose_sum: 94.15450412034988
time_elpased: 1.954
batch start
#iterations: 222
currently lose_sum: 93.62950730323792
time_elpased: 2.014
batch start
#iterations: 223
currently lose_sum: 93.94681710004807
time_elpased: 2.017
batch start
#iterations: 224
currently lose_sum: 93.86431646347046
time_elpased: 1.904
batch start
#iterations: 225
currently lose_sum: 93.84725970029831
time_elpased: 1.966
batch start
#iterations: 226
currently lose_sum: 93.57440775632858
time_elpased: 2.024
batch start
#iterations: 227
currently lose_sum: 93.7660077214241
time_elpased: 2.008
batch start
#iterations: 228
currently lose_sum: 94.04974818229675
time_elpased: 1.963
batch start
#iterations: 229
currently lose_sum: 94.11436706781387
time_elpased: 1.917
batch start
#iterations: 230
currently lose_sum: 93.70333343744278
time_elpased: 1.966
batch start
#iterations: 231
currently lose_sum: 93.65519416332245
time_elpased: 1.976
batch start
#iterations: 232
currently lose_sum: 94.11035841703415
time_elpased: 1.938
batch start
#iterations: 233
currently lose_sum: 93.77145951986313
time_elpased: 2.004
batch start
#iterations: 234
currently lose_sum: 93.80865412950516
time_elpased: 1.928
batch start
#iterations: 235
currently lose_sum: 94.03244268894196
time_elpased: 1.95
batch start
#iterations: 236
currently lose_sum: 93.75401616096497
time_elpased: 1.949
batch start
#iterations: 237
currently lose_sum: 93.78181207180023
time_elpased: 1.954
batch start
#iterations: 238
currently lose_sum: 93.93661427497864
time_elpased: 1.9
batch start
#iterations: 239
currently lose_sum: 93.88066476583481
time_elpased: 2.025
start validation test
0.67675257732
0.655057595392
0.749099516312
0.69892937731
0.676625561109
59.701
batch start
#iterations: 240
currently lose_sum: 93.92062592506409
time_elpased: 1.965
batch start
#iterations: 241
currently lose_sum: 93.94140774011612
time_elpased: 1.966
batch start
#iterations: 242
currently lose_sum: 93.54420214891434
time_elpased: 1.908
batch start
#iterations: 243
currently lose_sum: 93.82179480791092
time_elpased: 1.94
batch start
#iterations: 244
currently lose_sum: 94.22436428070068
time_elpased: 1.929
batch start
#iterations: 245
currently lose_sum: 93.8426074385643
time_elpased: 1.928
batch start
#iterations: 246
currently lose_sum: 93.99199199676514
time_elpased: 1.921
batch start
#iterations: 247
currently lose_sum: 93.59845954179764
time_elpased: 1.978
batch start
#iterations: 248
currently lose_sum: 94.00634574890137
time_elpased: 1.978
batch start
#iterations: 249
currently lose_sum: 93.71030408143997
time_elpased: 1.963
batch start
#iterations: 250
currently lose_sum: 93.70225292444229
time_elpased: 1.91
batch start
#iterations: 251
currently lose_sum: 93.56356966495514
time_elpased: 1.975
batch start
#iterations: 252
currently lose_sum: 93.8555845618248
time_elpased: 1.909
batch start
#iterations: 253
currently lose_sum: 93.42806482315063
time_elpased: 1.954
batch start
#iterations: 254
currently lose_sum: 93.62620204687119
time_elpased: 1.921
batch start
#iterations: 255
currently lose_sum: 93.99422931671143
time_elpased: 1.958
batch start
#iterations: 256
currently lose_sum: 93.56177258491516
time_elpased: 1.899
batch start
#iterations: 257
currently lose_sum: 93.95177209377289
time_elpased: 2.009
batch start
#iterations: 258
currently lose_sum: 93.50612407922745
time_elpased: 1.951
batch start
#iterations: 259
currently lose_sum: 93.4761792421341
time_elpased: 1.957
start validation test
0.680103092784
0.650518734459
0.780796542143
0.709728718428
0.679926309902
59.804
batch start
#iterations: 260
currently lose_sum: 94.02427595853806
time_elpased: 1.94
batch start
#iterations: 261
currently lose_sum: 93.33507663011551
time_elpased: 1.949
batch start
#iterations: 262
currently lose_sum: 93.64419376850128
time_elpased: 1.936
batch start
#iterations: 263
currently lose_sum: 93.58458340167999
time_elpased: 1.938
batch start
#iterations: 264
currently lose_sum: 94.146180331707
time_elpased: 1.926
batch start
#iterations: 265
currently lose_sum: 93.54391849040985
time_elpased: 1.952
batch start
#iterations: 266
currently lose_sum: 93.80672043561935
time_elpased: 1.922
batch start
#iterations: 267
currently lose_sum: 93.94464427232742
time_elpased: 1.939
batch start
#iterations: 268
currently lose_sum: 94.04484689235687
time_elpased: 1.958
batch start
#iterations: 269
currently lose_sum: 93.50263321399689
time_elpased: 1.93
batch start
#iterations: 270
currently lose_sum: 93.51745349168777
time_elpased: 1.928
batch start
#iterations: 271
currently lose_sum: 93.52230155467987
time_elpased: 1.94
batch start
#iterations: 272
currently lose_sum: 93.98404175043106
time_elpased: 1.908
batch start
#iterations: 273
currently lose_sum: 93.53177917003632
time_elpased: 1.965
batch start
#iterations: 274
currently lose_sum: 94.16114747524261
time_elpased: 1.941
batch start
#iterations: 275
currently lose_sum: 93.80407786369324
time_elpased: 1.981
batch start
#iterations: 276
currently lose_sum: 93.75672793388367
time_elpased: 1.922
batch start
#iterations: 277
currently lose_sum: 94.02279931306839
time_elpased: 1.953
batch start
#iterations: 278
currently lose_sum: 93.98779159784317
time_elpased: 1.916
batch start
#iterations: 279
currently lose_sum: 93.72075670957565
time_elpased: 1.943
start validation test
0.677989690722
0.655828992276
0.751466502007
0.700398062443
0.677860690846
60.043
batch start
#iterations: 280
currently lose_sum: 93.91208708286285
time_elpased: 1.924
batch start
#iterations: 281
currently lose_sum: 93.46534413099289
time_elpased: 1.954
batch start
#iterations: 282
currently lose_sum: 93.41568344831467
time_elpased: 1.937
batch start
#iterations: 283
currently lose_sum: 93.53556150197983
time_elpased: 1.977
batch start
#iterations: 284
currently lose_sum: 93.42923957109451
time_elpased: 1.933
batch start
#iterations: 285
currently lose_sum: 93.50581192970276
time_elpased: 1.943
batch start
#iterations: 286
currently lose_sum: 93.74044495820999
time_elpased: 1.931
batch start
#iterations: 287
currently lose_sum: 93.33180916309357
time_elpased: 1.968
batch start
#iterations: 288
currently lose_sum: 93.52628123760223
time_elpased: 1.919
batch start
#iterations: 289
currently lose_sum: 93.58243709802628
time_elpased: 1.953
batch start
#iterations: 290
currently lose_sum: 93.74318212270737
time_elpased: 1.917
batch start
#iterations: 291
currently lose_sum: 92.94699883460999
time_elpased: 1.93
batch start
#iterations: 292
currently lose_sum: 93.2936846613884
time_elpased: 1.943
batch start
#iterations: 293
currently lose_sum: 93.73112732172012
time_elpased: 1.941
batch start
#iterations: 294
currently lose_sum: 93.01557332277298
time_elpased: 1.924
batch start
#iterations: 295
currently lose_sum: 93.50610506534576
time_elpased: 1.951
batch start
#iterations: 296
currently lose_sum: 93.34754192829132
time_elpased: 1.897
batch start
#iterations: 297
currently lose_sum: 94.06682527065277
time_elpased: 1.936
batch start
#iterations: 298
currently lose_sum: 93.33102709054947
time_elpased: 1.966
batch start
#iterations: 299
currently lose_sum: 93.76091367006302
time_elpased: 1.936
start validation test
0.677835051546
0.654514662626
0.755685911289
0.701471150172
0.677698372354
59.978
batch start
#iterations: 300
currently lose_sum: 93.55421137809753
time_elpased: 1.93
batch start
#iterations: 301
currently lose_sum: 93.26485389471054
time_elpased: 1.975
batch start
#iterations: 302
currently lose_sum: 93.2674064040184
time_elpased: 1.984
batch start
#iterations: 303
currently lose_sum: 93.42807668447495
time_elpased: 1.937
batch start
#iterations: 304
currently lose_sum: 93.56703698635101
time_elpased: 1.941
batch start
#iterations: 305
currently lose_sum: 93.47103023529053
time_elpased: 2.029
batch start
#iterations: 306
currently lose_sum: 93.25889068841934
time_elpased: 1.963
batch start
#iterations: 307
currently lose_sum: 93.32658523321152
time_elpased: 1.972
batch start
#iterations: 308
currently lose_sum: 93.72554928064346
time_elpased: 1.955
batch start
#iterations: 309
currently lose_sum: 93.6085045337677
time_elpased: 2.045
batch start
#iterations: 310
currently lose_sum: 93.93883007764816
time_elpased: 2.026
batch start
#iterations: 311
currently lose_sum: 93.358791410923
time_elpased: 1.992
batch start
#iterations: 312
currently lose_sum: 93.47948151826859
time_elpased: 2.016
batch start
#iterations: 313
currently lose_sum: 93.21393698453903
time_elpased: 1.984
batch start
#iterations: 314
currently lose_sum: 93.81844925880432
time_elpased: 1.942
batch start
#iterations: 315
currently lose_sum: 93.3269853591919
time_elpased: 1.956
batch start
#iterations: 316
currently lose_sum: 93.73281973600388
time_elpased: 1.961
batch start
#iterations: 317
currently lose_sum: 93.82712757587433
time_elpased: 1.98
batch start
#iterations: 318
currently lose_sum: 93.23046481609344
time_elpased: 1.969
batch start
#iterations: 319
currently lose_sum: 93.58971428871155
time_elpased: 1.963
start validation test
0.677731958763
0.649572649573
0.774313059586
0.706478873239
0.677562395744
59.713
batch start
#iterations: 320
currently lose_sum: 93.32431042194366
time_elpased: 1.954
batch start
#iterations: 321
currently lose_sum: 93.32586896419525
time_elpased: 1.953
batch start
#iterations: 322
currently lose_sum: 94.10472095012665
time_elpased: 1.959
batch start
#iterations: 323
currently lose_sum: 94.01567256450653
time_elpased: 1.949
batch start
#iterations: 324
currently lose_sum: 93.38753587007523
time_elpased: 2.003
batch start
#iterations: 325
currently lose_sum: 93.5257499217987
time_elpased: 1.976
batch start
#iterations: 326
currently lose_sum: 93.42020177841187
time_elpased: 1.948
batch start
#iterations: 327
currently lose_sum: 93.44207292795181
time_elpased: 1.958
batch start
#iterations: 328
currently lose_sum: 93.71751606464386
time_elpased: 1.951
batch start
#iterations: 329
currently lose_sum: 93.5937756896019
time_elpased: 1.958
batch start
#iterations: 330
currently lose_sum: 93.16348844766617
time_elpased: 2.049
batch start
#iterations: 331
currently lose_sum: 93.73472034931183
time_elpased: 2.008
batch start
#iterations: 332
currently lose_sum: 93.36646175384521
time_elpased: 1.952
batch start
#iterations: 333
currently lose_sum: 93.27534449100494
time_elpased: 1.993
batch start
#iterations: 334
currently lose_sum: 94.2740238904953
time_elpased: 1.894
batch start
#iterations: 335
currently lose_sum: 93.30118989944458
time_elpased: 1.964
batch start
#iterations: 336
currently lose_sum: 93.43224304914474
time_elpased: 1.985
batch start
#iterations: 337
currently lose_sum: 93.14685159921646
time_elpased: 1.968
batch start
#iterations: 338
currently lose_sum: 93.42193883657455
time_elpased: 1.951
batch start
#iterations: 339
currently lose_sum: 93.5278405547142
time_elpased: 1.958
start validation test
0.67824742268
0.649205667669
0.778017906761
0.707798895235
0.678072260207
59.800
batch start
#iterations: 340
currently lose_sum: 93.53712010383606
time_elpased: 2.003
batch start
#iterations: 341
currently lose_sum: 94.01809310913086
time_elpased: 1.983
batch start
#iterations: 342
currently lose_sum: 93.31910115480423
time_elpased: 1.944
batch start
#iterations: 343
currently lose_sum: 93.2892462015152
time_elpased: 1.957
batch start
#iterations: 344
currently lose_sum: 93.623211145401
time_elpased: 1.969
batch start
#iterations: 345
currently lose_sum: 93.45969754457474
time_elpased: 1.935
batch start
#iterations: 346
currently lose_sum: 93.8622385263443
time_elpased: 1.973
batch start
#iterations: 347
currently lose_sum: 93.4546389579773
time_elpased: 2.094
batch start
#iterations: 348
currently lose_sum: 93.50793462991714
time_elpased: 1.991
batch start
#iterations: 349
currently lose_sum: 93.22441017627716
time_elpased: 1.995
batch start
#iterations: 350
currently lose_sum: 93.87441074848175
time_elpased: 1.961
batch start
#iterations: 351
currently lose_sum: 93.3875418305397
time_elpased: 1.99
batch start
#iterations: 352
currently lose_sum: 93.50952023267746
time_elpased: 1.971
batch start
#iterations: 353
currently lose_sum: 93.68631452322006
time_elpased: 1.981
batch start
#iterations: 354
currently lose_sum: 93.45742475986481
time_elpased: 1.954
batch start
#iterations: 355
currently lose_sum: 92.99964988231659
time_elpased: 2.018
batch start
#iterations: 356
currently lose_sum: 93.18472927808762
time_elpased: 1.999
batch start
#iterations: 357
currently lose_sum: 93.58106905221939
time_elpased: 2.018
batch start
#iterations: 358
currently lose_sum: 93.03322207927704
time_elpased: 1.953
batch start
#iterations: 359
currently lose_sum: 93.69346392154694
time_elpased: 1.982
start validation test
0.677886597938
0.648028000683
0.781208191829
0.708413046521
0.677705200944
59.838
batch start
#iterations: 360
currently lose_sum: 93.89008712768555
time_elpased: 2.018
batch start
#iterations: 361
currently lose_sum: 93.62726026773453
time_elpased: 2.034
batch start
#iterations: 362
currently lose_sum: 93.2618066072464
time_elpased: 1.985
batch start
#iterations: 363
currently lose_sum: 93.54630672931671
time_elpased: 1.976
batch start
#iterations: 364
currently lose_sum: 93.78351014852524
time_elpased: 1.926
batch start
#iterations: 365
currently lose_sum: 93.44510078430176
time_elpased: 1.994
batch start
#iterations: 366
currently lose_sum: 93.36617076396942
time_elpased: 1.93
batch start
#iterations: 367
currently lose_sum: 93.55968421697617
time_elpased: 1.994
batch start
#iterations: 368
currently lose_sum: 93.84725350141525
time_elpased: 2.017
batch start
#iterations: 369
currently lose_sum: 93.64979672431946
time_elpased: 2.012
batch start
#iterations: 370
currently lose_sum: 93.44182431697845
time_elpased: 1.994
batch start
#iterations: 371
currently lose_sum: 93.45664757490158
time_elpased: 1.999
batch start
#iterations: 372
currently lose_sum: 93.50747722387314
time_elpased: 1.973
batch start
#iterations: 373
currently lose_sum: 93.6536693572998
time_elpased: 2.027
batch start
#iterations: 374
currently lose_sum: 93.15079456567764
time_elpased: 2.023
batch start
#iterations: 375
currently lose_sum: 93.53428184986115
time_elpased: 1.975
batch start
#iterations: 376
currently lose_sum: 93.76902109384537
time_elpased: 1.926
batch start
#iterations: 377
currently lose_sum: 93.76427829265594
time_elpased: 2.007
batch start
#iterations: 378
currently lose_sum: 93.3172436952591
time_elpased: 1.96
batch start
#iterations: 379
currently lose_sum: 93.65598195791245
time_elpased: 2.058
start validation test
0.678298969072
0.646444219751
0.789544097973
0.710864025944
0.67810366109
59.845
batch start
#iterations: 380
currently lose_sum: 93.40907853841782
time_elpased: 1.937
batch start
#iterations: 381
currently lose_sum: 93.85750913619995
time_elpased: 2.006
batch start
#iterations: 382
currently lose_sum: 93.52974450588226
time_elpased: 1.975
batch start
#iterations: 383
currently lose_sum: 93.45281219482422
time_elpased: 1.954
batch start
#iterations: 384
currently lose_sum: 93.52880775928497
time_elpased: 2.001
batch start
#iterations: 385
currently lose_sum: 94.00724482536316
time_elpased: 2.014
batch start
#iterations: 386
currently lose_sum: 93.53099071979523
time_elpased: 1.977
batch start
#iterations: 387
currently lose_sum: 93.49684870243073
time_elpased: 1.994
batch start
#iterations: 388
currently lose_sum: 93.67785835266113
time_elpased: 2.001
batch start
#iterations: 389
currently lose_sum: 93.93116617202759
time_elpased: 2.037
batch start
#iterations: 390
currently lose_sum: 93.68015956878662
time_elpased: 1.937
batch start
#iterations: 391
currently lose_sum: 93.35891532897949
time_elpased: 1.994
batch start
#iterations: 392
currently lose_sum: 93.23715752363205
time_elpased: 1.961
batch start
#iterations: 393
currently lose_sum: 92.98005747795105
time_elpased: 2.0
batch start
#iterations: 394
currently lose_sum: 93.47698247432709
time_elpased: 2.008
batch start
#iterations: 395
currently lose_sum: 93.97866076231003
time_elpased: 1.996
batch start
#iterations: 396
currently lose_sum: 93.32939499616623
time_elpased: 1.973
batch start
#iterations: 397
currently lose_sum: 93.55135959386826
time_elpased: 1.941
batch start
#iterations: 398
currently lose_sum: 93.44779014587402
time_elpased: 2.026
batch start
#iterations: 399
currently lose_sum: 93.1406198143959
time_elpased: 2.035
start validation test
0.675515463918
0.647780272931
0.77184316147
0.704390702043
0.675346345787
59.895
acc: 0.668
pre: 0.647
rec: 0.741
F1: 0.691
auc: 0.668
