start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.07025134563446
time_elpased: 1.976
batch start
#iterations: 1
currently lose_sum: 99.38970905542374
time_elpased: 1.86
batch start
#iterations: 2
currently lose_sum: 99.2899090051651
time_elpased: 1.872
batch start
#iterations: 3
currently lose_sum: 98.7153040766716
time_elpased: 1.828
batch start
#iterations: 4
currently lose_sum: 98.68965584039688
time_elpased: 1.846
batch start
#iterations: 5
currently lose_sum: 98.12644505500793
time_elpased: 1.852
batch start
#iterations: 6
currently lose_sum: 97.6724385023117
time_elpased: 1.866
batch start
#iterations: 7
currently lose_sum: 97.73579525947571
time_elpased: 1.874
batch start
#iterations: 8
currently lose_sum: 97.61028951406479
time_elpased: 1.852
batch start
#iterations: 9
currently lose_sum: 97.61505621671677
time_elpased: 1.825
batch start
#iterations: 10
currently lose_sum: 97.3560301065445
time_elpased: 1.853
batch start
#iterations: 11
currently lose_sum: 97.15577805042267
time_elpased: 1.856
batch start
#iterations: 12
currently lose_sum: 97.01869398355484
time_elpased: 1.859
batch start
#iterations: 13
currently lose_sum: 96.91090619564056
time_elpased: 1.886
batch start
#iterations: 14
currently lose_sum: 96.77410817146301
time_elpased: 1.878
batch start
#iterations: 15
currently lose_sum: 96.97970217466354
time_elpased: 1.85
batch start
#iterations: 16
currently lose_sum: 96.77889049053192
time_elpased: 1.83
batch start
#iterations: 17
currently lose_sum: 96.78603678941727
time_elpased: 1.877
batch start
#iterations: 18
currently lose_sum: 96.20142960548401
time_elpased: 1.841
batch start
#iterations: 19
currently lose_sum: 96.58966219425201
time_elpased: 1.885
start validation test
0.658453608247
0.640845712203
0.723680148194
0.679748670855
0.658339092996
61.832
batch start
#iterations: 20
currently lose_sum: 96.497418820858
time_elpased: 1.898
batch start
#iterations: 21
currently lose_sum: 96.25085842609406
time_elpased: 1.887
batch start
#iterations: 22
currently lose_sum: 96.61637836694717
time_elpased: 1.976
batch start
#iterations: 23
currently lose_sum: 96.48755782842636
time_elpased: 1.857
batch start
#iterations: 24
currently lose_sum: 96.20330882072449
time_elpased: 1.875
batch start
#iterations: 25
currently lose_sum: 96.45063835382462
time_elpased: 1.856
batch start
#iterations: 26
currently lose_sum: 96.07998567819595
time_elpased: 1.865
batch start
#iterations: 27
currently lose_sum: 96.16667234897614
time_elpased: 1.826
batch start
#iterations: 28
currently lose_sum: 96.09656757116318
time_elpased: 1.869
batch start
#iterations: 29
currently lose_sum: 96.60236811637878
time_elpased: 1.879
batch start
#iterations: 30
currently lose_sum: 95.74520295858383
time_elpased: 1.854
batch start
#iterations: 31
currently lose_sum: 95.94213271141052
time_elpased: 1.887
batch start
#iterations: 32
currently lose_sum: 96.05227029323578
time_elpased: 1.805
batch start
#iterations: 33
currently lose_sum: 96.08076804876328
time_elpased: 1.829
batch start
#iterations: 34
currently lose_sum: 95.86691790819168
time_elpased: 1.816
batch start
#iterations: 35
currently lose_sum: 95.92525792121887
time_elpased: 1.868
batch start
#iterations: 36
currently lose_sum: 95.81198680400848
time_elpased: 1.836
batch start
#iterations: 37
currently lose_sum: 95.78783470392227
time_elpased: 1.821
batch start
#iterations: 38
currently lose_sum: 95.48400819301605
time_elpased: 1.794
batch start
#iterations: 39
currently lose_sum: 95.87080156803131
time_elpased: 1.849
start validation test
0.658195876289
0.634571777429
0.748790779047
0.686965963272
0.658036822964
61.400
batch start
#iterations: 40
currently lose_sum: 95.71051919460297
time_elpased: 1.864
batch start
#iterations: 41
currently lose_sum: 95.62013751268387
time_elpased: 1.845
batch start
#iterations: 42
currently lose_sum: 95.37441295385361
time_elpased: 1.865
batch start
#iterations: 43
currently lose_sum: 95.4807698726654
time_elpased: 1.865
batch start
#iterations: 44
currently lose_sum: 95.62176865339279
time_elpased: 1.849
batch start
#iterations: 45
currently lose_sum: 95.20672327280045
time_elpased: 1.862
batch start
#iterations: 46
currently lose_sum: 95.43602693080902
time_elpased: 1.851
batch start
#iterations: 47
currently lose_sum: 95.58942610025406
time_elpased: 1.877
batch start
#iterations: 48
currently lose_sum: 95.56219440698624
time_elpased: 1.847
batch start
#iterations: 49
currently lose_sum: 95.47160148620605
time_elpased: 1.889
batch start
#iterations: 50
currently lose_sum: 95.4146339893341
time_elpased: 1.851
batch start
#iterations: 51
currently lose_sum: 95.396120429039
time_elpased: 1.927
batch start
#iterations: 52
currently lose_sum: 95.46143144369125
time_elpased: 1.849
batch start
#iterations: 53
currently lose_sum: 95.64690339565277
time_elpased: 1.872
batch start
#iterations: 54
currently lose_sum: 95.49690479040146
time_elpased: 1.823
batch start
#iterations: 55
currently lose_sum: 95.06698673963547
time_elpased: 1.878
batch start
#iterations: 56
currently lose_sum: 95.25107842683792
time_elpased: 1.869
batch start
#iterations: 57
currently lose_sum: 95.15642416477203
time_elpased: 1.859
batch start
#iterations: 58
currently lose_sum: 95.31343019008636
time_elpased: 1.823
batch start
#iterations: 59
currently lose_sum: 95.04342371225357
time_elpased: 1.875
start validation test
0.665618556701
0.640630442355
0.757126685191
0.694023866799
0.665457900067
61.025
batch start
#iterations: 60
currently lose_sum: 94.99263262748718
time_elpased: 1.953
batch start
#iterations: 61
currently lose_sum: 95.29285657405853
time_elpased: 1.819
batch start
#iterations: 62
currently lose_sum: 95.1072028875351
time_elpased: 1.807
batch start
#iterations: 63
currently lose_sum: 95.41027069091797
time_elpased: 1.831
batch start
#iterations: 64
currently lose_sum: 95.07069462537766
time_elpased: 1.827
batch start
#iterations: 65
currently lose_sum: 94.96479618549347
time_elpased: 1.808
batch start
#iterations: 66
currently lose_sum: 95.1970397233963
time_elpased: 1.809
batch start
#iterations: 67
currently lose_sum: 95.03980040550232
time_elpased: 1.83
batch start
#iterations: 68
currently lose_sum: 95.2480074763298
time_elpased: 1.795
batch start
#iterations: 69
currently lose_sum: 95.20783424377441
time_elpased: 1.841
batch start
#iterations: 70
currently lose_sum: 94.90095019340515
time_elpased: 1.816
batch start
#iterations: 71
currently lose_sum: 94.92608547210693
time_elpased: 1.841
batch start
#iterations: 72
currently lose_sum: 95.16701948642731
time_elpased: 1.851
batch start
#iterations: 73
currently lose_sum: 94.88250017166138
time_elpased: 1.866
batch start
#iterations: 74
currently lose_sum: 95.00741803646088
time_elpased: 1.821
batch start
#iterations: 75
currently lose_sum: 94.70882844924927
time_elpased: 1.843
batch start
#iterations: 76
currently lose_sum: 95.01499086618423
time_elpased: 1.826
batch start
#iterations: 77
currently lose_sum: 94.93628162145615
time_elpased: 1.859
batch start
#iterations: 78
currently lose_sum: 95.29553282260895
time_elpased: 1.803
batch start
#iterations: 79
currently lose_sum: 94.75672763586044
time_elpased: 1.817
start validation test
0.662164948454
0.637533698582
0.754450962231
0.691082202112
0.662002926122
61.090
batch start
#iterations: 80
currently lose_sum: 94.58252489566803
time_elpased: 1.838
batch start
#iterations: 81
currently lose_sum: 95.33534735441208
time_elpased: 1.844
batch start
#iterations: 82
currently lose_sum: 94.95815563201904
time_elpased: 1.851
batch start
#iterations: 83
currently lose_sum: 94.74941819906235
time_elpased: 1.84
batch start
#iterations: 84
currently lose_sum: 94.82988661527634
time_elpased: 1.804
batch start
#iterations: 85
currently lose_sum: 94.94958770275116
time_elpased: 1.814
batch start
#iterations: 86
currently lose_sum: 94.85282385349274
time_elpased: 1.808
batch start
#iterations: 87
currently lose_sum: 94.63832902908325
time_elpased: 1.828
batch start
#iterations: 88
currently lose_sum: 94.7604888677597
time_elpased: 1.811
batch start
#iterations: 89
currently lose_sum: 94.69475489854813
time_elpased: 1.81
batch start
#iterations: 90
currently lose_sum: 94.94585055112839
time_elpased: 1.863
batch start
#iterations: 91
currently lose_sum: 94.7347736954689
time_elpased: 1.826
batch start
#iterations: 92
currently lose_sum: 94.46702057123184
time_elpased: 1.823
batch start
#iterations: 93
currently lose_sum: 94.60176438093185
time_elpased: 1.828
batch start
#iterations: 94
currently lose_sum: 94.68183189630508
time_elpased: 1.856
batch start
#iterations: 95
currently lose_sum: 94.88149642944336
time_elpased: 1.827
batch start
#iterations: 96
currently lose_sum: 95.15616661310196
time_elpased: 1.818
batch start
#iterations: 97
currently lose_sum: 94.69169563055038
time_elpased: 1.876
batch start
#iterations: 98
currently lose_sum: 94.48034036159515
time_elpased: 1.833
batch start
#iterations: 99
currently lose_sum: 94.4866514801979
time_elpased: 1.843
start validation test
0.664484536082
0.63604749788
0.771740249048
0.697354349747
0.664296232135
60.667
batch start
#iterations: 100
currently lose_sum: 94.1533704996109
time_elpased: 1.955
batch start
#iterations: 101
currently lose_sum: 94.72073239088058
time_elpased: 1.857
batch start
#iterations: 102
currently lose_sum: 94.38826441764832
time_elpased: 1.807
batch start
#iterations: 103
currently lose_sum: 94.65976732969284
time_elpased: 1.812
batch start
#iterations: 104
currently lose_sum: 94.85398435592651
time_elpased: 1.836
batch start
#iterations: 105
currently lose_sum: 94.76103866100311
time_elpased: 1.864
batch start
#iterations: 106
currently lose_sum: 94.9212406873703
time_elpased: 1.861
batch start
#iterations: 107
currently lose_sum: 94.85011929273605
time_elpased: 1.82
batch start
#iterations: 108
currently lose_sum: 94.5748205780983
time_elpased: 1.859
batch start
#iterations: 109
currently lose_sum: 94.47398108243942
time_elpased: 1.901
batch start
#iterations: 110
currently lose_sum: 94.27292329072952
time_elpased: 1.784
batch start
#iterations: 111
currently lose_sum: 94.2463949918747
time_elpased: 1.854
batch start
#iterations: 112
currently lose_sum: 94.45252066850662
time_elpased: 1.934
batch start
#iterations: 113
currently lose_sum: 94.3900226354599
time_elpased: 1.876
batch start
#iterations: 114
currently lose_sum: 94.08296275138855
time_elpased: 1.847
batch start
#iterations: 115
currently lose_sum: 94.35038459300995
time_elpased: 1.882
batch start
#iterations: 116
currently lose_sum: 94.34090101718903
time_elpased: 1.782
batch start
#iterations: 117
currently lose_sum: 94.73663240671158
time_elpased: 1.825
batch start
#iterations: 118
currently lose_sum: 94.50556313991547
time_elpased: 1.841
batch start
#iterations: 119
currently lose_sum: 94.6958692073822
time_elpased: 1.911
start validation test
0.66587628866
0.637390639599
0.772254811156
0.698371335505
0.665689524756
60.733
batch start
#iterations: 120
currently lose_sum: 94.60598433017731
time_elpased: 1.922
batch start
#iterations: 121
currently lose_sum: 94.61623811721802
time_elpased: 1.821
batch start
#iterations: 122
currently lose_sum: 94.46203923225403
time_elpased: 1.803
batch start
#iterations: 123
currently lose_sum: 94.11980754137039
time_elpased: 1.833
batch start
#iterations: 124
currently lose_sum: 94.55098444223404
time_elpased: 1.819
batch start
#iterations: 125
currently lose_sum: 94.70872110128403
time_elpased: 1.854
batch start
#iterations: 126
currently lose_sum: 94.44973570108414
time_elpased: 1.871
batch start
#iterations: 127
currently lose_sum: 94.74247461557388
time_elpased: 1.931
batch start
#iterations: 128
currently lose_sum: 94.07881444692612
time_elpased: 1.808
batch start
#iterations: 129
currently lose_sum: 94.23511791229248
time_elpased: 1.821
batch start
#iterations: 130
currently lose_sum: 94.15632629394531
time_elpased: 1.799
batch start
#iterations: 131
currently lose_sum: 94.2064266204834
time_elpased: 1.897
batch start
#iterations: 132
currently lose_sum: 94.44372951984406
time_elpased: 1.825
batch start
#iterations: 133
currently lose_sum: 94.13042372465134
time_elpased: 1.86
batch start
#iterations: 134
currently lose_sum: 94.49416118860245
time_elpased: 1.88
batch start
#iterations: 135
currently lose_sum: 93.95627021789551
time_elpased: 1.824
batch start
#iterations: 136
currently lose_sum: 94.03570604324341
time_elpased: 1.838
batch start
#iterations: 137
currently lose_sum: 93.96463704109192
time_elpased: 1.832
batch start
#iterations: 138
currently lose_sum: 94.15363490581512
time_elpased: 1.844
batch start
#iterations: 139
currently lose_sum: 94.24183571338654
time_elpased: 1.934
start validation test
0.664536082474
0.630224819414
0.799114953175
0.704691895816
0.664299808509
60.556
batch start
#iterations: 140
currently lose_sum: 94.58436125516891
time_elpased: 1.903
batch start
#iterations: 141
currently lose_sum: 94.25674468278885
time_elpased: 1.81
batch start
#iterations: 142
currently lose_sum: 94.32060104608536
time_elpased: 1.842
batch start
#iterations: 143
currently lose_sum: 94.27496999502182
time_elpased: 1.857
batch start
#iterations: 144
currently lose_sum: 94.73097598552704
time_elpased: 1.898
batch start
#iterations: 145
currently lose_sum: 94.00045615434647
time_elpased: 1.881
batch start
#iterations: 146
currently lose_sum: 94.29646700620651
time_elpased: 1.884
batch start
#iterations: 147
currently lose_sum: 94.1085233092308
time_elpased: 1.845
batch start
#iterations: 148
currently lose_sum: 94.61016428470612
time_elpased: 1.87
batch start
#iterations: 149
currently lose_sum: 94.03886967897415
time_elpased: 1.892
batch start
#iterations: 150
currently lose_sum: 93.65927404165268
time_elpased: 1.838
batch start
#iterations: 151
currently lose_sum: 94.26798605918884
time_elpased: 1.824
batch start
#iterations: 152
currently lose_sum: 94.57612317800522
time_elpased: 1.859
batch start
#iterations: 153
currently lose_sum: 94.34777814149857
time_elpased: 1.828
batch start
#iterations: 154
currently lose_sum: 94.26655948162079
time_elpased: 1.855
batch start
#iterations: 155
currently lose_sum: 94.44732373952866
time_elpased: 1.827
batch start
#iterations: 156
currently lose_sum: 94.33001494407654
time_elpased: 1.852
batch start
#iterations: 157
currently lose_sum: 94.46884447336197
time_elpased: 1.863
batch start
#iterations: 158
currently lose_sum: 94.87998020648956
time_elpased: 1.84
batch start
#iterations: 159
currently lose_sum: 94.43449664115906
time_elpased: 1.842
start validation test
0.662835051546
0.63505698248
0.768447051559
0.695413271246
0.662649633391
60.604
batch start
#iterations: 160
currently lose_sum: 94.04034072160721
time_elpased: 1.917
batch start
#iterations: 161
currently lose_sum: 94.30397003889084
time_elpased: 1.85
batch start
#iterations: 162
currently lose_sum: 94.23058295249939
time_elpased: 1.83
batch start
#iterations: 163
currently lose_sum: 94.05429947376251
time_elpased: 1.838
batch start
#iterations: 164
currently lose_sum: 94.30389106273651
time_elpased: 1.886
batch start
#iterations: 165
currently lose_sum: 94.36787790060043
time_elpased: 1.837
batch start
#iterations: 166
currently lose_sum: 94.11355268955231
time_elpased: 1.876
batch start
#iterations: 167
currently lose_sum: 94.04904264211655
time_elpased: 1.886
batch start
#iterations: 168
currently lose_sum: 94.00533103942871
time_elpased: 1.932
batch start
#iterations: 169
currently lose_sum: 94.10111093521118
time_elpased: 1.852
batch start
#iterations: 170
currently lose_sum: 94.12382447719574
time_elpased: 1.878
batch start
#iterations: 171
currently lose_sum: 94.13287383317947
time_elpased: 1.881
batch start
#iterations: 172
currently lose_sum: 94.7016350030899
time_elpased: 1.922
batch start
#iterations: 173
currently lose_sum: 94.12269043922424
time_elpased: 1.954
batch start
#iterations: 174
currently lose_sum: 93.95804286003113
time_elpased: 1.814
batch start
#iterations: 175
currently lose_sum: 94.02433532476425
time_elpased: 1.794
batch start
#iterations: 176
currently lose_sum: 93.96606534719467
time_elpased: 1.819
batch start
#iterations: 177
currently lose_sum: 93.84826594591141
time_elpased: 1.826
batch start
#iterations: 178
currently lose_sum: 93.65437304973602
time_elpased: 1.818
batch start
#iterations: 179
currently lose_sum: 94.82850480079651
time_elpased: 1.835
start validation test
0.666804123711
0.638626097332
0.771122774519
0.698648018648
0.66662097623
60.462
batch start
#iterations: 180
currently lose_sum: 94.24441188573837
time_elpased: 1.879
batch start
#iterations: 181
currently lose_sum: 94.14136749505997
time_elpased: 1.807
batch start
#iterations: 182
currently lose_sum: 94.0495136976242
time_elpased: 1.807
batch start
#iterations: 183
currently lose_sum: 93.69811260700226
time_elpased: 1.892
batch start
#iterations: 184
currently lose_sum: 94.12837421894073
time_elpased: 1.843
batch start
#iterations: 185
currently lose_sum: 93.95073860883713
time_elpased: 1.824
batch start
#iterations: 186
currently lose_sum: 93.74415612220764
time_elpased: 1.883
batch start
#iterations: 187
currently lose_sum: 93.98909968137741
time_elpased: 1.814
batch start
#iterations: 188
currently lose_sum: 94.28229427337646
time_elpased: 1.865
batch start
#iterations: 189
currently lose_sum: 93.87738412618637
time_elpased: 1.83
batch start
#iterations: 190
currently lose_sum: 94.2331753373146
time_elpased: 1.832
batch start
#iterations: 191
currently lose_sum: 94.05274653434753
time_elpased: 1.89
batch start
#iterations: 192
currently lose_sum: 93.975301861763
time_elpased: 1.818
batch start
#iterations: 193
currently lose_sum: 93.96085768938065
time_elpased: 1.909
batch start
#iterations: 194
currently lose_sum: 94.18814820051193
time_elpased: 1.881
batch start
#iterations: 195
currently lose_sum: 94.02740669250488
time_elpased: 1.868
batch start
#iterations: 196
currently lose_sum: 94.51248329877853
time_elpased: 1.868
batch start
#iterations: 197
currently lose_sum: 93.74024504423141
time_elpased: 1.883
batch start
#iterations: 198
currently lose_sum: 93.90493083000183
time_elpased: 1.844
batch start
#iterations: 199
currently lose_sum: 93.78178650140762
time_elpased: 1.848
start validation test
0.664381443299
0.638811915483
0.759184933621
0.693816129791
0.664215001149
60.437
batch start
#iterations: 200
currently lose_sum: 93.75437432527542
time_elpased: 1.893
batch start
#iterations: 201
currently lose_sum: 93.86906343698502
time_elpased: 1.852
batch start
#iterations: 202
currently lose_sum: 94.01984512805939
time_elpased: 1.878
batch start
#iterations: 203
currently lose_sum: 93.88895207643509
time_elpased: 1.816
batch start
#iterations: 204
currently lose_sum: 93.70085310935974
time_elpased: 1.863
batch start
#iterations: 205
currently lose_sum: 94.04518836736679
time_elpased: 1.809
batch start
#iterations: 206
currently lose_sum: 93.89377403259277
time_elpased: 1.933
batch start
#iterations: 207
currently lose_sum: 94.00248175859451
time_elpased: 1.812
batch start
#iterations: 208
currently lose_sum: 93.59930443763733
time_elpased: 1.885
batch start
#iterations: 209
currently lose_sum: 93.9886884689331
time_elpased: 1.879
batch start
#iterations: 210
currently lose_sum: 93.86278384923935
time_elpased: 1.888
batch start
#iterations: 211
currently lose_sum: 93.6373639702797
time_elpased: 1.828
batch start
#iterations: 212
currently lose_sum: 94.09363055229187
time_elpased: 1.887
batch start
#iterations: 213
currently lose_sum: 93.99664777517319
time_elpased: 1.86
batch start
#iterations: 214
currently lose_sum: 94.16658473014832
time_elpased: 1.863
batch start
#iterations: 215
currently lose_sum: 93.52394306659698
time_elpased: 1.887
batch start
#iterations: 216
currently lose_sum: 93.57132828235626
time_elpased: 1.855
batch start
#iterations: 217
currently lose_sum: 94.05603730678558
time_elpased: 1.8
batch start
#iterations: 218
currently lose_sum: 94.30005383491516
time_elpased: 1.865
batch start
#iterations: 219
currently lose_sum: 94.34276360273361
time_elpased: 1.815
start validation test
0.669690721649
0.639045297924
0.782546053309
0.70355292376
0.669492586708
60.385
batch start
#iterations: 220
currently lose_sum: 93.76137763261795
time_elpased: 1.955
batch start
#iterations: 221
currently lose_sum: 94.05954486131668
time_elpased: 1.865
batch start
#iterations: 222
currently lose_sum: 93.62450248003006
time_elpased: 1.828
batch start
#iterations: 223
currently lose_sum: 94.02786785364151
time_elpased: 1.923
batch start
#iterations: 224
currently lose_sum: 94.05622863769531
time_elpased: 1.852
batch start
#iterations: 225
currently lose_sum: 93.90672725439072
time_elpased: 2.026
batch start
#iterations: 226
currently lose_sum: 93.80954790115356
time_elpased: 1.842
batch start
#iterations: 227
currently lose_sum: 93.69851005077362
time_elpased: 1.885
batch start
#iterations: 228
currently lose_sum: 93.66796958446503
time_elpased: 1.849
batch start
#iterations: 229
currently lose_sum: 93.80998939275742
time_elpased: 1.864
batch start
#iterations: 230
currently lose_sum: 93.854032933712
time_elpased: 1.869
batch start
#iterations: 231
currently lose_sum: 94.1990270614624
time_elpased: 1.828
batch start
#iterations: 232
currently lose_sum: 93.89946126937866
time_elpased: 1.903
batch start
#iterations: 233
currently lose_sum: 94.14071774482727
time_elpased: 1.895
batch start
#iterations: 234
currently lose_sum: 93.87790524959564
time_elpased: 1.845
batch start
#iterations: 235
currently lose_sum: 93.78814280033112
time_elpased: 1.854
batch start
#iterations: 236
currently lose_sum: 94.00749510526657
time_elpased: 1.8
batch start
#iterations: 237
currently lose_sum: 94.15624153614044
time_elpased: 1.818
batch start
#iterations: 238
currently lose_sum: 94.19042330980301
time_elpased: 1.857
batch start
#iterations: 239
currently lose_sum: 93.8616344332695
time_elpased: 1.861
start validation test
0.667268041237
0.637521079258
0.778120819183
0.700838856189
0.667073422088
60.367
batch start
#iterations: 240
currently lose_sum: 93.93816143274307
time_elpased: 1.949
batch start
#iterations: 241
currently lose_sum: 93.85720747709274
time_elpased: 1.857
batch start
#iterations: 242
currently lose_sum: 93.87581068277359
time_elpased: 1.843
batch start
#iterations: 243
currently lose_sum: 94.1922715306282
time_elpased: 1.844
batch start
#iterations: 244
currently lose_sum: 93.98706638813019
time_elpased: 1.846
batch start
#iterations: 245
currently lose_sum: 94.07049000263214
time_elpased: 1.828
batch start
#iterations: 246
currently lose_sum: 93.50733387470245
time_elpased: 1.804
batch start
#iterations: 247
currently lose_sum: 93.80019515752792
time_elpased: 1.789
batch start
#iterations: 248
currently lose_sum: 93.81900602579117
time_elpased: 1.854
batch start
#iterations: 249
currently lose_sum: 94.2273582816124
time_elpased: 1.826
batch start
#iterations: 250
currently lose_sum: 93.57995694875717
time_elpased: 1.866
batch start
#iterations: 251
currently lose_sum: 94.02908486127853
time_elpased: 1.828
batch start
#iterations: 252
currently lose_sum: 93.7588050365448
time_elpased: 1.834
batch start
#iterations: 253
currently lose_sum: 93.99514126777649
time_elpased: 1.841
batch start
#iterations: 254
currently lose_sum: 93.9155758023262
time_elpased: 1.878
batch start
#iterations: 255
currently lose_sum: 93.71580529212952
time_elpased: 1.854
batch start
#iterations: 256
currently lose_sum: 93.51822918653488
time_elpased: 1.844
batch start
#iterations: 257
currently lose_sum: 93.49063050746918
time_elpased: 1.851
batch start
#iterations: 258
currently lose_sum: 93.7775444984436
time_elpased: 1.847
batch start
#iterations: 259
currently lose_sum: 94.03355866670609
time_elpased: 1.856
start validation test
0.665051546392
0.635582512004
0.776474220438
0.698999444136
0.664855926702
60.330
batch start
#iterations: 260
currently lose_sum: 93.49975687265396
time_elpased: 1.975
batch start
#iterations: 261
currently lose_sum: 93.85959315299988
time_elpased: 1.814
batch start
#iterations: 262
currently lose_sum: 93.83655893802643
time_elpased: 1.922
batch start
#iterations: 263
currently lose_sum: 93.5481184720993
time_elpased: 1.866
batch start
#iterations: 264
currently lose_sum: 94.01861548423767
time_elpased: 1.886
batch start
#iterations: 265
currently lose_sum: 93.8713721036911
time_elpased: 1.853
batch start
#iterations: 266
currently lose_sum: 94.44103348255157
time_elpased: 1.835
batch start
#iterations: 267
currently lose_sum: 93.84975010156631
time_elpased: 1.871
batch start
#iterations: 268
currently lose_sum: 93.73849588632584
time_elpased: 1.868
batch start
#iterations: 269
currently lose_sum: 93.67451119422913
time_elpased: 1.835
batch start
#iterations: 270
currently lose_sum: 93.85036253929138
time_elpased: 1.861
batch start
#iterations: 271
currently lose_sum: 93.94400489330292
time_elpased: 1.822
batch start
#iterations: 272
currently lose_sum: 93.80638593435287
time_elpased: 1.825
batch start
#iterations: 273
currently lose_sum: 93.51879167556763
time_elpased: 1.827
batch start
#iterations: 274
currently lose_sum: 94.3895623087883
time_elpased: 1.809
batch start
#iterations: 275
currently lose_sum: 94.13494098186493
time_elpased: 1.825
batch start
#iterations: 276
currently lose_sum: 93.51267904043198
time_elpased: 1.815
batch start
#iterations: 277
currently lose_sum: 93.90473514795303
time_elpased: 1.792
batch start
#iterations: 278
currently lose_sum: 93.79811525344849
time_elpased: 1.815
batch start
#iterations: 279
currently lose_sum: 93.30530196428299
time_elpased: 1.829
start validation test
0.668041237113
0.634844868735
0.793866419677
0.705505761844
0.667820331598
60.377
batch start
#iterations: 280
currently lose_sum: 93.57138311862946
time_elpased: 1.894
batch start
#iterations: 281
currently lose_sum: 93.98349565267563
time_elpased: 1.852
batch start
#iterations: 282
currently lose_sum: 93.31964993476868
time_elpased: 1.845
batch start
#iterations: 283
currently lose_sum: 93.96321719884872
time_elpased: 1.887
batch start
#iterations: 284
currently lose_sum: 93.74331283569336
time_elpased: 1.852
batch start
#iterations: 285
currently lose_sum: 93.74437111616135
time_elpased: 1.836
batch start
#iterations: 286
currently lose_sum: 93.53779751062393
time_elpased: 1.913
batch start
#iterations: 287
currently lose_sum: 93.58509397506714
time_elpased: 1.856
batch start
#iterations: 288
currently lose_sum: 93.27722811698914
time_elpased: 1.898
batch start
#iterations: 289
currently lose_sum: 94.23466682434082
time_elpased: 1.961
batch start
#iterations: 290
currently lose_sum: 93.83251190185547
time_elpased: 1.893
batch start
#iterations: 291
currently lose_sum: 93.4664226770401
time_elpased: 1.849
batch start
#iterations: 292
currently lose_sum: 94.0231419801712
time_elpased: 1.856
batch start
#iterations: 293
currently lose_sum: 93.66705107688904
time_elpased: 1.846
batch start
#iterations: 294
currently lose_sum: 93.72565042972565
time_elpased: 1.949
batch start
#iterations: 295
currently lose_sum: 94.1750990152359
time_elpased: 1.843
batch start
#iterations: 296
currently lose_sum: 93.39519238471985
time_elpased: 1.88
batch start
#iterations: 297
currently lose_sum: 93.55778956413269
time_elpased: 1.848
batch start
#iterations: 298
currently lose_sum: 93.75409030914307
time_elpased: 1.852
batch start
#iterations: 299
currently lose_sum: 93.75672268867493
time_elpased: 1.853
start validation test
0.667113402062
0.634214644593
0.792425645775
0.704547534084
0.66689339709
60.302
batch start
#iterations: 300
currently lose_sum: 93.10828256607056
time_elpased: 1.943
batch start
#iterations: 301
currently lose_sum: 93.58459120988846
time_elpased: 1.879
batch start
#iterations: 302
currently lose_sum: 93.65124475955963
time_elpased: 1.877
batch start
#iterations: 303
currently lose_sum: 93.90335720777512
time_elpased: 1.898
batch start
#iterations: 304
currently lose_sum: 93.65664213895798
time_elpased: 1.894
batch start
#iterations: 305
currently lose_sum: 94.16523832082748
time_elpased: 1.847
batch start
#iterations: 306
currently lose_sum: 93.68316859006882
time_elpased: 1.863
batch start
#iterations: 307
currently lose_sum: 93.74323505163193
time_elpased: 1.826
batch start
#iterations: 308
currently lose_sum: 93.64057266712189
time_elpased: 1.869
batch start
#iterations: 309
currently lose_sum: 93.72746813297272
time_elpased: 1.794
batch start
#iterations: 310
currently lose_sum: 93.67643773555756
time_elpased: 1.87
batch start
#iterations: 311
currently lose_sum: 93.36381387710571
time_elpased: 1.881
batch start
#iterations: 312
currently lose_sum: 93.32993644475937
time_elpased: 1.841
batch start
#iterations: 313
currently lose_sum: 94.1128351688385
time_elpased: 1.88
batch start
#iterations: 314
currently lose_sum: 93.57026982307434
time_elpased: 1.842
batch start
#iterations: 315
currently lose_sum: 93.78354543447495
time_elpased: 1.946
batch start
#iterations: 316
currently lose_sum: 93.69042330980301
time_elpased: 1.896
batch start
#iterations: 317
currently lose_sum: 93.77309024333954
time_elpased: 1.883
batch start
#iterations: 318
currently lose_sum: 93.58002585172653
time_elpased: 1.93
batch start
#iterations: 319
currently lose_sum: 94.23127698898315
time_elpased: 1.858
start validation test
0.662268041237
0.642889390519
0.732736441288
0.684878799538
0.662144323092
60.558
batch start
#iterations: 320
currently lose_sum: 93.9770901799202
time_elpased: 1.87
batch start
#iterations: 321
currently lose_sum: 93.70726835727692
time_elpased: 1.873
batch start
#iterations: 322
currently lose_sum: 93.54628425836563
time_elpased: 1.857
batch start
#iterations: 323
currently lose_sum: 94.24300676584244
time_elpased: 1.846
batch start
#iterations: 324
currently lose_sum: 94.05579626560211
time_elpased: 1.911
batch start
#iterations: 325
currently lose_sum: 93.72921293973923
time_elpased: 1.833
batch start
#iterations: 326
currently lose_sum: 93.92388671636581
time_elpased: 1.994
batch start
#iterations: 327
currently lose_sum: 93.48214691877365
time_elpased: 1.92
batch start
#iterations: 328
currently lose_sum: 93.45796626806259
time_elpased: 1.872
batch start
#iterations: 329
currently lose_sum: 93.68227833509445
time_elpased: 1.856
batch start
#iterations: 330
currently lose_sum: 93.7892604470253
time_elpased: 1.877
batch start
#iterations: 331
currently lose_sum: 93.16439455747604
time_elpased: 1.874
batch start
#iterations: 332
currently lose_sum: 93.57100588083267
time_elpased: 2.005
batch start
#iterations: 333
currently lose_sum: 93.29284203052521
time_elpased: 1.854
batch start
#iterations: 334
currently lose_sum: 93.66029018163681
time_elpased: 1.958
batch start
#iterations: 335
currently lose_sum: 93.7273091673851
time_elpased: 1.869
batch start
#iterations: 336
currently lose_sum: 93.92798966169357
time_elpased: 1.893
batch start
#iterations: 337
currently lose_sum: 93.48515456914902
time_elpased: 1.898
batch start
#iterations: 338
currently lose_sum: 93.68175083398819
time_elpased: 1.932
batch start
#iterations: 339
currently lose_sum: 93.7077721953392
time_elpased: 1.824
start validation test
0.669381443299
0.636004282303
0.794792631471
0.706587374199
0.669161264615
60.261
batch start
#iterations: 340
currently lose_sum: 93.69297283887863
time_elpased: 1.897
batch start
#iterations: 341
currently lose_sum: 93.90363049507141
time_elpased: 1.916
batch start
#iterations: 342
currently lose_sum: 93.89319759607315
time_elpased: 1.86
batch start
#iterations: 343
currently lose_sum: 93.396260201931
time_elpased: 1.887
batch start
#iterations: 344
currently lose_sum: 93.74044114351273
time_elpased: 1.872
batch start
#iterations: 345
currently lose_sum: 93.456722676754
time_elpased: 1.844
batch start
#iterations: 346
currently lose_sum: 93.96073371171951
time_elpased: 1.922
batch start
#iterations: 347
currently lose_sum: 93.8708027601242
time_elpased: 1.861
batch start
#iterations: 348
currently lose_sum: 93.98988389968872
time_elpased: 1.83
batch start
#iterations: 349
currently lose_sum: 93.87704539299011
time_elpased: 1.836
batch start
#iterations: 350
currently lose_sum: 93.64378035068512
time_elpased: 1.851
batch start
#iterations: 351
currently lose_sum: 93.98430633544922
time_elpased: 1.858
batch start
#iterations: 352
currently lose_sum: 93.53904849290848
time_elpased: 1.845
batch start
#iterations: 353
currently lose_sum: 94.11125212907791
time_elpased: 1.832
batch start
#iterations: 354
currently lose_sum: 94.32241880893707
time_elpased: 1.865
batch start
#iterations: 355
currently lose_sum: 93.39310932159424
time_elpased: 1.831
batch start
#iterations: 356
currently lose_sum: 93.61763471364975
time_elpased: 1.891
batch start
#iterations: 357
currently lose_sum: 94.12403041124344
time_elpased: 1.859
batch start
#iterations: 358
currently lose_sum: 94.02609658241272
time_elpased: 1.866
batch start
#iterations: 359
currently lose_sum: 93.74358081817627
time_elpased: 1.914
start validation test
0.667731958763
0.635917892462
0.787485849542
0.703632183908
0.667521712337
60.492
batch start
#iterations: 360
currently lose_sum: 94.12193608283997
time_elpased: 1.875
batch start
#iterations: 361
currently lose_sum: 94.26978981494904
time_elpased: 1.832
batch start
#iterations: 362
currently lose_sum: 93.22603261470795
time_elpased: 1.917
batch start
#iterations: 363
currently lose_sum: 94.013667345047
time_elpased: 1.869
batch start
#iterations: 364
currently lose_sum: 94.22279858589172
time_elpased: 1.898
batch start
#iterations: 365
currently lose_sum: 93.99246883392334
time_elpased: 1.939
batch start
#iterations: 366
currently lose_sum: 93.79006522893906
time_elpased: 1.883
batch start
#iterations: 367
currently lose_sum: 93.31125593185425
time_elpased: 1.827
batch start
#iterations: 368
currently lose_sum: 93.90829491615295
time_elpased: 1.838
batch start
#iterations: 369
currently lose_sum: 93.70319640636444
time_elpased: 1.905
batch start
#iterations: 370
currently lose_sum: 93.71645820140839
time_elpased: 1.924
batch start
#iterations: 371
currently lose_sum: 94.00138413906097
time_elpased: 1.825
batch start
#iterations: 372
currently lose_sum: 93.70000964403152
time_elpased: 1.877
batch start
#iterations: 373
currently lose_sum: 93.66508841514587
time_elpased: 1.872
batch start
#iterations: 374
currently lose_sum: 93.63974022865295
time_elpased: 1.911
batch start
#iterations: 375
currently lose_sum: 93.63721567392349
time_elpased: 1.876
batch start
#iterations: 376
currently lose_sum: 93.34597891569138
time_elpased: 1.858
batch start
#iterations: 377
currently lose_sum: 93.97962790727615
time_elpased: 1.946
batch start
#iterations: 378
currently lose_sum: 93.34702956676483
time_elpased: 1.873
batch start
#iterations: 379
currently lose_sum: 93.80226993560791
time_elpased: 1.833
start validation test
0.665412371134
0.636787652646
0.772769373263
0.69821935004
0.665223889358
60.467
batch start
#iterations: 380
currently lose_sum: 93.68446004390717
time_elpased: 1.831
batch start
#iterations: 381
currently lose_sum: 93.55323177576065
time_elpased: 1.863
batch start
#iterations: 382
currently lose_sum: 93.69963538646698
time_elpased: 1.824
batch start
#iterations: 383
currently lose_sum: 93.90404587984085
time_elpased: 1.888
batch start
#iterations: 384
currently lose_sum: 93.95215255022049
time_elpased: 1.869
batch start
#iterations: 385
currently lose_sum: 93.46145820617676
time_elpased: 1.929
batch start
#iterations: 386
currently lose_sum: 93.48167288303375
time_elpased: 1.864
batch start
#iterations: 387
currently lose_sum: 93.47665417194366
time_elpased: 1.871
batch start
#iterations: 388
currently lose_sum: 93.71272575855255
time_elpased: 1.873
batch start
#iterations: 389
currently lose_sum: 93.68485933542252
time_elpased: 1.859
batch start
#iterations: 390
currently lose_sum: 93.52746087312698
time_elpased: 1.88
batch start
#iterations: 391
currently lose_sum: 93.37548345327377
time_elpased: 1.85
batch start
#iterations: 392
currently lose_sum: 93.24381572008133
time_elpased: 1.834
batch start
#iterations: 393
currently lose_sum: 93.38302171230316
time_elpased: 1.871
batch start
#iterations: 394
currently lose_sum: 93.62385332584381
time_elpased: 1.869
batch start
#iterations: 395
currently lose_sum: 93.88294780254364
time_elpased: 1.892
batch start
#iterations: 396
currently lose_sum: 93.2533289194107
time_elpased: 1.871
batch start
#iterations: 397
currently lose_sum: 93.27435106039047
time_elpased: 1.844
batch start
#iterations: 398
currently lose_sum: 93.96065104007721
time_elpased: 1.863
batch start
#iterations: 399
currently lose_sum: 93.34325683116913
time_elpased: 1.801
start validation test
0.668350515464
0.638488146461
0.778841206134
0.701715345387
0.668156532014
60.400
acc: 0.667
pre: 0.635
rec: 0.791
F1: 0.704
auc: 0.667
