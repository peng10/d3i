start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.7992200255394
time_elpased: 1.972
batch start
#iterations: 1
currently lose_sum: 100.01164364814758
time_elpased: 1.85
batch start
#iterations: 2
currently lose_sum: 99.75880038738251
time_elpased: 1.838
batch start
#iterations: 3
currently lose_sum: 99.60510319471359
time_elpased: 1.871
batch start
#iterations: 4
currently lose_sum: 99.47273355722427
time_elpased: 1.853
batch start
#iterations: 5
currently lose_sum: 98.99661028385162
time_elpased: 1.838
batch start
#iterations: 6
currently lose_sum: 98.89705342054367
time_elpased: 1.834
batch start
#iterations: 7
currently lose_sum: 98.7595004439354
time_elpased: 1.854
batch start
#iterations: 8
currently lose_sum: 98.49358308315277
time_elpased: 1.874
batch start
#iterations: 9
currently lose_sum: 98.38462352752686
time_elpased: 1.852
batch start
#iterations: 10
currently lose_sum: 98.14939266443253
time_elpased: 1.839
batch start
#iterations: 11
currently lose_sum: 97.93065774440765
time_elpased: 1.876
batch start
#iterations: 12
currently lose_sum: 98.11017203330994
time_elpased: 1.835
batch start
#iterations: 13
currently lose_sum: 97.68398743867874
time_elpased: 1.85
batch start
#iterations: 14
currently lose_sum: 97.78369724750519
time_elpased: 1.856
batch start
#iterations: 15
currently lose_sum: 97.38144969940186
time_elpased: 1.859
batch start
#iterations: 16
currently lose_sum: 97.51562410593033
time_elpased: 1.816
batch start
#iterations: 17
currently lose_sum: 97.35266321897507
time_elpased: 1.849
batch start
#iterations: 18
currently lose_sum: 96.91884744167328
time_elpased: 1.848
batch start
#iterations: 19
currently lose_sum: 97.56705319881439
time_elpased: 1.868
start validation test
0.648608247423
0.626770414408
0.737779149943
0.677759394942
0.64845169415
62.298
batch start
#iterations: 20
currently lose_sum: 97.3990433216095
time_elpased: 1.875
batch start
#iterations: 21
currently lose_sum: 96.82662272453308
time_elpased: 1.869
batch start
#iterations: 22
currently lose_sum: 97.0474705696106
time_elpased: 1.842
batch start
#iterations: 23
currently lose_sum: 96.93439823389053
time_elpased: 1.853
batch start
#iterations: 24
currently lose_sum: 96.73220813274384
time_elpased: 1.823
batch start
#iterations: 25
currently lose_sum: 96.52782064676285
time_elpased: 1.854
batch start
#iterations: 26
currently lose_sum: 96.75438243150711
time_elpased: 1.852
batch start
#iterations: 27
currently lose_sum: 96.48422944545746
time_elpased: 1.851
batch start
#iterations: 28
currently lose_sum: 96.33493387699127
time_elpased: 1.818
batch start
#iterations: 29
currently lose_sum: 96.79562020301819
time_elpased: 1.87
batch start
#iterations: 30
currently lose_sum: 96.7577275633812
time_elpased: 1.847
batch start
#iterations: 31
currently lose_sum: 96.50965768098831
time_elpased: 1.893
batch start
#iterations: 32
currently lose_sum: 95.87436670064926
time_elpased: 1.831
batch start
#iterations: 33
currently lose_sum: 96.33159905672073
time_elpased: 1.962
batch start
#iterations: 34
currently lose_sum: 96.12566787004471
time_elpased: 1.884
batch start
#iterations: 35
currently lose_sum: 96.52911627292633
time_elpased: 1.857
batch start
#iterations: 36
currently lose_sum: 96.35934191942215
time_elpased: 1.92
batch start
#iterations: 37
currently lose_sum: 96.38670206069946
time_elpased: 1.927
batch start
#iterations: 38
currently lose_sum: 96.05162793397903
time_elpased: 1.883
batch start
#iterations: 39
currently lose_sum: 96.47054982185364
time_elpased: 1.917
start validation test
0.650721649485
0.661824584571
0.618915303077
0.639651138056
0.650777490431
61.819
batch start
#iterations: 40
currently lose_sum: 96.09602344036102
time_elpased: 1.871
batch start
#iterations: 41
currently lose_sum: 96.15182453393936
time_elpased: 1.868
batch start
#iterations: 42
currently lose_sum: 95.98755568265915
time_elpased: 1.829
batch start
#iterations: 43
currently lose_sum: 96.04620260000229
time_elpased: 1.847
batch start
#iterations: 44
currently lose_sum: 96.25781840085983
time_elpased: 1.873
batch start
#iterations: 45
currently lose_sum: 96.02165389060974
time_elpased: 1.828
batch start
#iterations: 46
currently lose_sum: 96.03376770019531
time_elpased: 1.887
batch start
#iterations: 47
currently lose_sum: 95.8457424044609
time_elpased: 1.898
batch start
#iterations: 48
currently lose_sum: 96.03231596946716
time_elpased: 1.864
batch start
#iterations: 49
currently lose_sum: 96.2276958823204
time_elpased: 1.848
batch start
#iterations: 50
currently lose_sum: 96.13726657629013
time_elpased: 1.869
batch start
#iterations: 51
currently lose_sum: 95.93439626693726
time_elpased: 1.879
batch start
#iterations: 52
currently lose_sum: 95.75145637989044
time_elpased: 1.828
batch start
#iterations: 53
currently lose_sum: 95.89356583356857
time_elpased: 1.893
batch start
#iterations: 54
currently lose_sum: 96.13645309209824
time_elpased: 1.839
batch start
#iterations: 55
currently lose_sum: 95.9863953590393
time_elpased: 1.846
batch start
#iterations: 56
currently lose_sum: 96.01209634542465
time_elpased: 1.861
batch start
#iterations: 57
currently lose_sum: 95.56913328170776
time_elpased: 1.823
batch start
#iterations: 58
currently lose_sum: 95.99340814352036
time_elpased: 1.847
batch start
#iterations: 59
currently lose_sum: 95.75343239307404
time_elpased: 1.836
start validation test
0.651958762887
0.645757545964
0.675928784604
0.660498793242
0.651916679816
61.199
batch start
#iterations: 60
currently lose_sum: 96.19290643930435
time_elpased: 1.85
batch start
#iterations: 61
currently lose_sum: 96.07288861274719
time_elpased: 1.904
batch start
#iterations: 62
currently lose_sum: 96.12519532442093
time_elpased: 1.872
batch start
#iterations: 63
currently lose_sum: 95.93552893400192
time_elpased: 1.811
batch start
#iterations: 64
currently lose_sum: 96.10265529155731
time_elpased: 1.848
batch start
#iterations: 65
currently lose_sum: 95.47989654541016
time_elpased: 1.867
batch start
#iterations: 66
currently lose_sum: 95.64150154590607
time_elpased: 1.847
batch start
#iterations: 67
currently lose_sum: 95.63642799854279
time_elpased: 1.849
batch start
#iterations: 68
currently lose_sum: 95.69430810213089
time_elpased: 1.871
batch start
#iterations: 69
currently lose_sum: 96.17271137237549
time_elpased: 1.922
batch start
#iterations: 70
currently lose_sum: 95.50753623247147
time_elpased: 1.853
batch start
#iterations: 71
currently lose_sum: 95.40315818786621
time_elpased: 1.853
batch start
#iterations: 72
currently lose_sum: 95.388352394104
time_elpased: 1.837
batch start
#iterations: 73
currently lose_sum: 95.09791427850723
time_elpased: 1.917
batch start
#iterations: 74
currently lose_sum: 95.48303234577179
time_elpased: 1.913
batch start
#iterations: 75
currently lose_sum: 95.4714595079422
time_elpased: 1.831
batch start
#iterations: 76
currently lose_sum: 95.49398618936539
time_elpased: 1.872
batch start
#iterations: 77
currently lose_sum: 95.36000800132751
time_elpased: 1.856
batch start
#iterations: 78
currently lose_sum: 95.63134747743607
time_elpased: 1.853
batch start
#iterations: 79
currently lose_sum: 95.46843206882477
time_elpased: 1.83
start validation test
0.65793814433
0.625601304525
0.789647010394
0.698116640888
0.657706909101
61.441
batch start
#iterations: 80
currently lose_sum: 95.45797109603882
time_elpased: 1.892
batch start
#iterations: 81
currently lose_sum: 95.78312927484512
time_elpased: 1.837
batch start
#iterations: 82
currently lose_sum: 95.43985766172409
time_elpased: 1.861
batch start
#iterations: 83
currently lose_sum: 95.56190592050552
time_elpased: 1.831
batch start
#iterations: 84
currently lose_sum: 95.74285155534744
time_elpased: 1.844
batch start
#iterations: 85
currently lose_sum: 95.97672867774963
time_elpased: 1.827
batch start
#iterations: 86
currently lose_sum: 95.6316630244255
time_elpased: 1.825
batch start
#iterations: 87
currently lose_sum: 95.5248441696167
time_elpased: 1.843
batch start
#iterations: 88
currently lose_sum: 95.36140525341034
time_elpased: 1.846
batch start
#iterations: 89
currently lose_sum: 95.67807573080063
time_elpased: 1.843
batch start
#iterations: 90
currently lose_sum: 95.6773624420166
time_elpased: 1.867
batch start
#iterations: 91
currently lose_sum: 95.32762777805328
time_elpased: 1.909
batch start
#iterations: 92
currently lose_sum: 95.14332246780396
time_elpased: 1.884
batch start
#iterations: 93
currently lose_sum: 94.99893873929977
time_elpased: 1.867
batch start
#iterations: 94
currently lose_sum: 95.3648442029953
time_elpased: 1.932
batch start
#iterations: 95
currently lose_sum: 95.72944450378418
time_elpased: 1.846
batch start
#iterations: 96
currently lose_sum: 95.49283963441849
time_elpased: 1.869
batch start
#iterations: 97
currently lose_sum: 95.3167832493782
time_elpased: 1.893
batch start
#iterations: 98
currently lose_sum: 95.53234720230103
time_elpased: 1.924
batch start
#iterations: 99
currently lose_sum: 95.68501710891724
time_elpased: 1.872
start validation test
0.65381443299
0.655347344446
0.65143562828
0.653385631709
0.653818609348
61.553
batch start
#iterations: 100
currently lose_sum: 95.28991001844406
time_elpased: 1.866
batch start
#iterations: 101
currently lose_sum: 95.78913825750351
time_elpased: 1.921
batch start
#iterations: 102
currently lose_sum: 95.21180909872055
time_elpased: 1.851
batch start
#iterations: 103
currently lose_sum: 95.38582974672318
time_elpased: 1.831
batch start
#iterations: 104
currently lose_sum: 95.00754189491272
time_elpased: 1.895
batch start
#iterations: 105
currently lose_sum: 95.00840640068054
time_elpased: 1.812
batch start
#iterations: 106
currently lose_sum: 95.29048663377762
time_elpased: 1.856
batch start
#iterations: 107
currently lose_sum: 95.14551293849945
time_elpased: 1.88
batch start
#iterations: 108
currently lose_sum: 95.41522258520126
time_elpased: 1.85
batch start
#iterations: 109
currently lose_sum: 95.12094694375992
time_elpased: 1.89
batch start
#iterations: 110
currently lose_sum: 95.55286180973053
time_elpased: 1.834
batch start
#iterations: 111
currently lose_sum: 95.21989434957504
time_elpased: 1.833
batch start
#iterations: 112
currently lose_sum: 95.35665565729141
time_elpased: 1.832
batch start
#iterations: 113
currently lose_sum: 95.06027126312256
time_elpased: 1.887
batch start
#iterations: 114
currently lose_sum: 95.46084052324295
time_elpased: 1.954
batch start
#iterations: 115
currently lose_sum: 95.46378546953201
time_elpased: 1.859
batch start
#iterations: 116
currently lose_sum: 94.78278023004532
time_elpased: 1.927
batch start
#iterations: 117
currently lose_sum: 95.42991989850998
time_elpased: 1.891
batch start
#iterations: 118
currently lose_sum: 95.14511156082153
time_elpased: 1.867
batch start
#iterations: 119
currently lose_sum: 95.50018501281738
time_elpased: 1.856
start validation test
0.664587628866
0.65279893374
0.705670474426
0.678205825627
0.664515501594
60.878
batch start
#iterations: 120
currently lose_sum: 95.28111463785172
time_elpased: 1.929
batch start
#iterations: 121
currently lose_sum: 95.3169915676117
time_elpased: 1.904
batch start
#iterations: 122
currently lose_sum: 95.13636445999146
time_elpased: 1.879
batch start
#iterations: 123
currently lose_sum: 95.0029765367508
time_elpased: 1.855
batch start
#iterations: 124
currently lose_sum: 95.53605139255524
time_elpased: 1.801
batch start
#iterations: 125
currently lose_sum: 95.44999182224274
time_elpased: 1.895
batch start
#iterations: 126
currently lose_sum: 95.15828543901443
time_elpased: 1.853
batch start
#iterations: 127
currently lose_sum: 95.17532205581665
time_elpased: 1.871
batch start
#iterations: 128
currently lose_sum: 95.47647362947464
time_elpased: 1.843
batch start
#iterations: 129
currently lose_sum: 95.45588308572769
time_elpased: 1.874
batch start
#iterations: 130
currently lose_sum: 94.97373336553574
time_elpased: 1.836
batch start
#iterations: 131
currently lose_sum: 95.35053980350494
time_elpased: 1.833
batch start
#iterations: 132
currently lose_sum: 95.3158466219902
time_elpased: 1.85
batch start
#iterations: 133
currently lose_sum: 95.42924958467484
time_elpased: 1.901
batch start
#iterations: 134
currently lose_sum: 95.00925827026367
time_elpased: 1.886
batch start
#iterations: 135
currently lose_sum: 95.18117278814316
time_elpased: 1.861
batch start
#iterations: 136
currently lose_sum: 95.39828497171402
time_elpased: 1.855
batch start
#iterations: 137
currently lose_sum: 94.8959686756134
time_elpased: 1.862
batch start
#iterations: 138
currently lose_sum: 95.27530354261398
time_elpased: 1.895
batch start
#iterations: 139
currently lose_sum: 95.50171202421188
time_elpased: 1.883
start validation test
0.666288659794
0.645256651438
0.741278172275
0.689942528736
0.666157004138
60.925
batch start
#iterations: 140
currently lose_sum: 95.23638838529587
time_elpased: 1.85
batch start
#iterations: 141
currently lose_sum: 95.18052065372467
time_elpased: 1.856
batch start
#iterations: 142
currently lose_sum: 94.94929295778275
time_elpased: 1.889
batch start
#iterations: 143
currently lose_sum: 95.35476797819138
time_elpased: 1.831
batch start
#iterations: 144
currently lose_sum: 95.02569484710693
time_elpased: 1.824
batch start
#iterations: 145
currently lose_sum: 95.34510731697083
time_elpased: 1.889
batch start
#iterations: 146
currently lose_sum: 94.98727583885193
time_elpased: 1.859
batch start
#iterations: 147
currently lose_sum: 95.58299142122269
time_elpased: 1.859
batch start
#iterations: 148
currently lose_sum: 94.79408901929855
time_elpased: 1.883
batch start
#iterations: 149
currently lose_sum: 95.49960887432098
time_elpased: 1.864
batch start
#iterations: 150
currently lose_sum: 95.41184222698212
time_elpased: 1.855
batch start
#iterations: 151
currently lose_sum: 95.58609813451767
time_elpased: 1.846
batch start
#iterations: 152
currently lose_sum: 94.98791408538818
time_elpased: 1.861
batch start
#iterations: 153
currently lose_sum: 95.37381166219711
time_elpased: 1.829
batch start
#iterations: 154
currently lose_sum: 95.33751809597015
time_elpased: 1.854
batch start
#iterations: 155
currently lose_sum: 95.39851522445679
time_elpased: 1.883
batch start
#iterations: 156
currently lose_sum: 94.86323970556259
time_elpased: 1.959
batch start
#iterations: 157
currently lose_sum: 94.92385679483414
time_elpased: 1.871
batch start
#iterations: 158
currently lose_sum: 95.12091982364655
time_elpased: 1.839
batch start
#iterations: 159
currently lose_sum: 95.10922193527222
time_elpased: 1.905
start validation test
0.659484536082
0.6464
0.706802511063
0.67525317078
0.659401462079
61.143
batch start
#iterations: 160
currently lose_sum: 95.0774554014206
time_elpased: 1.883
batch start
#iterations: 161
currently lose_sum: 95.15229594707489
time_elpased: 1.87
batch start
#iterations: 162
currently lose_sum: 94.69365924596786
time_elpased: 1.878
batch start
#iterations: 163
currently lose_sum: 94.91774094104767
time_elpased: 1.896
batch start
#iterations: 164
currently lose_sum: 95.30589812994003
time_elpased: 1.853
batch start
#iterations: 165
currently lose_sum: 94.73337614536285
time_elpased: 1.877
batch start
#iterations: 166
currently lose_sum: 95.3295162320137
time_elpased: 1.86
batch start
#iterations: 167
currently lose_sum: 95.06514352560043
time_elpased: 1.903
batch start
#iterations: 168
currently lose_sum: 94.80427688360214
time_elpased: 1.837
batch start
#iterations: 169
currently lose_sum: 94.87690776586533
time_elpased: 1.87
batch start
#iterations: 170
currently lose_sum: 95.0768369436264
time_elpased: 1.831
batch start
#iterations: 171
currently lose_sum: 94.95818024873734
time_elpased: 1.879
batch start
#iterations: 172
currently lose_sum: 94.52551352977753
time_elpased: 1.849
batch start
#iterations: 173
currently lose_sum: 95.11738985776901
time_elpased: 2.023
batch start
#iterations: 174
currently lose_sum: 94.70782381296158
time_elpased: 1.858
batch start
#iterations: 175
currently lose_sum: 95.28678488731384
time_elpased: 1.876
batch start
#iterations: 176
currently lose_sum: 94.856998026371
time_elpased: 1.849
batch start
#iterations: 177
currently lose_sum: 95.28919261693954
time_elpased: 1.872
batch start
#iterations: 178
currently lose_sum: 95.18952453136444
time_elpased: 1.845
batch start
#iterations: 179
currently lose_sum: 94.5644103884697
time_elpased: 1.863
start validation test
0.643505154639
0.662943571844
0.586394977874
0.622324159021
0.643605420363
61.424
batch start
#iterations: 180
currently lose_sum: 95.08602648973465
time_elpased: 1.868
batch start
#iterations: 181
currently lose_sum: 94.4930591583252
time_elpased: 1.88
batch start
#iterations: 182
currently lose_sum: 94.94211918115616
time_elpased: 1.885
batch start
#iterations: 183
currently lose_sum: 94.6079494357109
time_elpased: 1.854
batch start
#iterations: 184
currently lose_sum: 94.62636238336563
time_elpased: 1.861
batch start
#iterations: 185
currently lose_sum: 94.88732779026031
time_elpased: 1.869
batch start
#iterations: 186
currently lose_sum: 94.30942767858505
time_elpased: 1.83
batch start
#iterations: 187
currently lose_sum: 94.48504424095154
time_elpased: 1.882
batch start
#iterations: 188
currently lose_sum: 94.81698954105377
time_elpased: 1.85
batch start
#iterations: 189
currently lose_sum: 94.95238095521927
time_elpased: 1.934
batch start
#iterations: 190
currently lose_sum: 94.84241753816605
time_elpased: 1.955
batch start
#iterations: 191
currently lose_sum: 94.81095373630524
time_elpased: 1.863
batch start
#iterations: 192
currently lose_sum: 94.97913634777069
time_elpased: 1.869
batch start
#iterations: 193
currently lose_sum: 94.875452876091
time_elpased: 1.877
batch start
#iterations: 194
currently lose_sum: 94.68602156639099
time_elpased: 1.857
batch start
#iterations: 195
currently lose_sum: 94.82350009679794
time_elpased: 1.863
batch start
#iterations: 196
currently lose_sum: 94.72651183605194
time_elpased: 1.856
batch start
#iterations: 197
currently lose_sum: 95.06309658288956
time_elpased: 1.875
batch start
#iterations: 198
currently lose_sum: 94.51306682825089
time_elpased: 1.837
batch start
#iterations: 199
currently lose_sum: 95.07086712121964
time_elpased: 1.878
start validation test
0.662628865979
0.648836336336
0.711536482453
0.678741471555
0.662543001115
60.781
batch start
#iterations: 200
currently lose_sum: 95.00037854909897
time_elpased: 1.913
batch start
#iterations: 201
currently lose_sum: 94.63929891586304
time_elpased: 1.881
batch start
#iterations: 202
currently lose_sum: 95.09962117671967
time_elpased: 1.859
batch start
#iterations: 203
currently lose_sum: 95.12092453241348
time_elpased: 1.852
batch start
#iterations: 204
currently lose_sum: 94.70406860113144
time_elpased: 1.846
batch start
#iterations: 205
currently lose_sum: 94.67510682344437
time_elpased: 1.828
batch start
#iterations: 206
currently lose_sum: 94.90023881196976
time_elpased: 1.849
batch start
#iterations: 207
currently lose_sum: 94.59054255485535
time_elpased: 1.829
batch start
#iterations: 208
currently lose_sum: 95.053681910038
time_elpased: 1.862
batch start
#iterations: 209
currently lose_sum: 94.93614172935486
time_elpased: 1.926
batch start
#iterations: 210
currently lose_sum: 94.70553767681122
time_elpased: 1.874
batch start
#iterations: 211
currently lose_sum: 94.65448379516602
time_elpased: 2.002
batch start
#iterations: 212
currently lose_sum: 94.6432374715805
time_elpased: 1.877
batch start
#iterations: 213
currently lose_sum: 94.1462795138359
time_elpased: 1.968
batch start
#iterations: 214
currently lose_sum: 94.31815242767334
time_elpased: 1.877
batch start
#iterations: 215
currently lose_sum: 94.36929839849472
time_elpased: 1.916
batch start
#iterations: 216
currently lose_sum: 94.98382830619812
time_elpased: 1.899
batch start
#iterations: 217
currently lose_sum: 94.28087514638901
time_elpased: 1.937
batch start
#iterations: 218
currently lose_sum: 95.00322204828262
time_elpased: 1.958
batch start
#iterations: 219
currently lose_sum: 94.9203839302063
time_elpased: 1.898
start validation test
0.665670103093
0.636756116143
0.774107234743
0.698745935903
0.665479724983
60.286
batch start
#iterations: 220
currently lose_sum: 95.13336044549942
time_elpased: 1.889
batch start
#iterations: 221
currently lose_sum: 94.90805608034134
time_elpased: 1.912
batch start
#iterations: 222
currently lose_sum: 94.9910609126091
time_elpased: 1.919
batch start
#iterations: 223
currently lose_sum: 94.90493679046631
time_elpased: 1.891
batch start
#iterations: 224
currently lose_sum: 94.92822170257568
time_elpased: 1.919
batch start
#iterations: 225
currently lose_sum: 94.79853504896164
time_elpased: 1.919
batch start
#iterations: 226
currently lose_sum: 94.55273133516312
time_elpased: 1.868
batch start
#iterations: 227
currently lose_sum: 94.82473009824753
time_elpased: 1.897
batch start
#iterations: 228
currently lose_sum: 94.54582476615906
time_elpased: 1.872
batch start
#iterations: 229
currently lose_sum: 94.82976269721985
time_elpased: 1.861
batch start
#iterations: 230
currently lose_sum: 94.81255030632019
time_elpased: 1.864
batch start
#iterations: 231
currently lose_sum: 94.44339692592621
time_elpased: 1.864
batch start
#iterations: 232
currently lose_sum: 94.471266746521
time_elpased: 1.833
batch start
#iterations: 233
currently lose_sum: 94.76463109254837
time_elpased: 1.942
batch start
#iterations: 234
currently lose_sum: 95.18249493837357
time_elpased: 1.867
batch start
#iterations: 235
currently lose_sum: 94.47990995645523
time_elpased: 1.902
batch start
#iterations: 236
currently lose_sum: 94.74332964420319
time_elpased: 1.87
batch start
#iterations: 237
currently lose_sum: 94.44509172439575
time_elpased: 1.9
batch start
#iterations: 238
currently lose_sum: 94.61203181743622
time_elpased: 1.856
batch start
#iterations: 239
currently lose_sum: 94.62227213382721
time_elpased: 1.872
start validation test
0.651391752577
0.651611578731
0.653288051868
0.652448738373
0.651388423331
61.076
batch start
#iterations: 240
currently lose_sum: 94.1017387509346
time_elpased: 1.899
batch start
#iterations: 241
currently lose_sum: 94.99471914768219
time_elpased: 1.872
batch start
#iterations: 242
currently lose_sum: 94.49440503120422
time_elpased: 1.855
batch start
#iterations: 243
currently lose_sum: 94.55144661664963
time_elpased: 1.878
batch start
#iterations: 244
currently lose_sum: 94.7201908826828
time_elpased: 1.847
batch start
#iterations: 245
currently lose_sum: 95.0060014128685
time_elpased: 1.894
batch start
#iterations: 246
currently lose_sum: 94.91188877820969
time_elpased: 1.845
batch start
#iterations: 247
currently lose_sum: 94.88728082180023
time_elpased: 1.929
batch start
#iterations: 248
currently lose_sum: 94.7367154955864
time_elpased: 1.907
batch start
#iterations: 249
currently lose_sum: 94.74729186296463
time_elpased: 1.885
batch start
#iterations: 250
currently lose_sum: 94.83374160528183
time_elpased: 1.885
batch start
#iterations: 251
currently lose_sum: 94.46901297569275
time_elpased: 1.939
batch start
#iterations: 252
currently lose_sum: 94.42031729221344
time_elpased: 1.89
batch start
#iterations: 253
currently lose_sum: 94.68563038110733
time_elpased: 1.9
batch start
#iterations: 254
currently lose_sum: 94.73611801862717
time_elpased: 1.874
batch start
#iterations: 255
currently lose_sum: 94.25844115018845
time_elpased: 1.909
batch start
#iterations: 256
currently lose_sum: 94.46362721920013
time_elpased: 1.879
batch start
#iterations: 257
currently lose_sum: 94.55754524469376
time_elpased: 1.905
batch start
#iterations: 258
currently lose_sum: 94.28061777353287
time_elpased: 1.851
batch start
#iterations: 259
currently lose_sum: 94.784836769104
time_elpased: 1.917
start validation test
0.663350515464
0.632155301145
0.784192652053
0.700013779799
0.663138358455
60.620
batch start
#iterations: 260
currently lose_sum: 94.44294333457947
time_elpased: 1.927
batch start
#iterations: 261
currently lose_sum: 94.74904960393906
time_elpased: 1.892
batch start
#iterations: 262
currently lose_sum: 94.72019052505493
time_elpased: 1.915
batch start
#iterations: 263
currently lose_sum: 94.40346419811249
time_elpased: 1.918
batch start
#iterations: 264
currently lose_sum: 94.83969086408615
time_elpased: 1.99
batch start
#iterations: 265
currently lose_sum: 94.64397042989731
time_elpased: 1.876
batch start
#iterations: 266
currently lose_sum: 94.76113110780716
time_elpased: 1.902
batch start
#iterations: 267
currently lose_sum: 94.25141990184784
time_elpased: 1.904
batch start
#iterations: 268
currently lose_sum: 94.41944801807404
time_elpased: 1.871
batch start
#iterations: 269
currently lose_sum: 94.75011503696442
time_elpased: 1.877
batch start
#iterations: 270
currently lose_sum: 94.9212167263031
time_elpased: 1.868
batch start
#iterations: 271
currently lose_sum: 94.3529401421547
time_elpased: 1.885
batch start
#iterations: 272
currently lose_sum: 94.69111180305481
time_elpased: 1.852
batch start
#iterations: 273
currently lose_sum: 94.14951580762863
time_elpased: 1.873
batch start
#iterations: 274
currently lose_sum: 94.17199331521988
time_elpased: 1.857
batch start
#iterations: 275
currently lose_sum: 94.60065996646881
time_elpased: 1.926
batch start
#iterations: 276
currently lose_sum: 94.80071526765823
time_elpased: 1.893
batch start
#iterations: 277
currently lose_sum: 94.96034270524979
time_elpased: 1.934
batch start
#iterations: 278
currently lose_sum: 94.87302160263062
time_elpased: 1.878
batch start
#iterations: 279
currently lose_sum: 94.55151998996735
time_elpased: 1.917
start validation test
0.665670103093
0.623954576843
0.836883811876
0.714901098901
0.665369511019
60.408
batch start
#iterations: 280
currently lose_sum: 94.64446073770523
time_elpased: 1.861
batch start
#iterations: 281
currently lose_sum: 94.67137932777405
time_elpased: 1.871
batch start
#iterations: 282
currently lose_sum: 94.87693059444427
time_elpased: 1.853
batch start
#iterations: 283
currently lose_sum: 94.82291632890701
time_elpased: 1.929
batch start
#iterations: 284
currently lose_sum: 94.24199104309082
time_elpased: 1.981
batch start
#iterations: 285
currently lose_sum: 94.47845816612244
time_elpased: 1.942
batch start
#iterations: 286
currently lose_sum: 94.53562915325165
time_elpased: 1.891
batch start
#iterations: 287
currently lose_sum: 94.764777302742
time_elpased: 1.935
batch start
#iterations: 288
currently lose_sum: 94.49118852615356
time_elpased: 1.906
batch start
#iterations: 289
currently lose_sum: 94.59851413965225
time_elpased: 1.952
batch start
#iterations: 290
currently lose_sum: 94.76512616872787
time_elpased: 1.856
batch start
#iterations: 291
currently lose_sum: 94.39022767543793
time_elpased: 2.012
batch start
#iterations: 292
currently lose_sum: 94.2441229224205
time_elpased: 1.906
batch start
#iterations: 293
currently lose_sum: 94.2877168059349
time_elpased: 1.908
batch start
#iterations: 294
currently lose_sum: 94.35151469707489
time_elpased: 1.855
batch start
#iterations: 295
currently lose_sum: 94.43733948469162
time_elpased: 2.072
batch start
#iterations: 296
currently lose_sum: 94.08770489692688
time_elpased: 1.909
batch start
#iterations: 297
currently lose_sum: 94.75681930780411
time_elpased: 1.929
batch start
#iterations: 298
currently lose_sum: 94.7001046538353
time_elpased: 1.891
batch start
#iterations: 299
currently lose_sum: 93.91804069280624
time_elpased: 1.858
start validation test
0.660979381443
0.643117593437
0.726047133889
0.682070865761
0.660865144968
60.671
batch start
#iterations: 300
currently lose_sum: 94.62989819049835
time_elpased: 1.909
batch start
#iterations: 301
currently lose_sum: 94.46804308891296
time_elpased: 1.864
batch start
#iterations: 302
currently lose_sum: 94.536572098732
time_elpased: 1.89
batch start
#iterations: 303
currently lose_sum: 94.36289495229721
time_elpased: 1.904
batch start
#iterations: 304
currently lose_sum: 94.01197427511215
time_elpased: 1.922
batch start
#iterations: 305
currently lose_sum: 94.49287128448486
time_elpased: 1.932
batch start
#iterations: 306
currently lose_sum: 94.96745270490646
time_elpased: 1.923
batch start
#iterations: 307
currently lose_sum: 94.38476586341858
time_elpased: 1.843
batch start
#iterations: 308
currently lose_sum: 94.18284398317337
time_elpased: 1.945
batch start
#iterations: 309
currently lose_sum: 94.55181896686554
time_elpased: 1.894
batch start
#iterations: 310
currently lose_sum: 94.42515432834625
time_elpased: 1.915
batch start
#iterations: 311
currently lose_sum: 94.42765092849731
time_elpased: 1.88
batch start
#iterations: 312
currently lose_sum: 94.52910703420639
time_elpased: 1.981
batch start
#iterations: 313
currently lose_sum: 94.0035873055458
time_elpased: 1.845
batch start
#iterations: 314
currently lose_sum: 94.4857559800148
time_elpased: 1.826
batch start
#iterations: 315
currently lose_sum: 94.47678035497665
time_elpased: 1.846
batch start
#iterations: 316
currently lose_sum: 94.88484579324722
time_elpased: 1.869
batch start
#iterations: 317
currently lose_sum: 94.44307696819305
time_elpased: 1.907
batch start
#iterations: 318
currently lose_sum: 94.44260466098785
time_elpased: 1.834
batch start
#iterations: 319
currently lose_sum: 94.29133653640747
time_elpased: 1.915
start validation test
0.66675257732
0.637540179327
0.775650921066
0.699846789545
0.666561389481
60.435
batch start
#iterations: 320
currently lose_sum: 94.36693620681763
time_elpased: 1.902
batch start
#iterations: 321
currently lose_sum: 94.20503294467926
time_elpased: 1.906
batch start
#iterations: 322
currently lose_sum: 94.67344361543655
time_elpased: 1.852
batch start
#iterations: 323
currently lose_sum: 94.138975918293
time_elpased: 1.876
batch start
#iterations: 324
currently lose_sum: 94.87278181314468
time_elpased: 1.823
batch start
#iterations: 325
currently lose_sum: 94.68345445394516
time_elpased: 1.934
batch start
#iterations: 326
currently lose_sum: 94.29675334692001
time_elpased: 1.976
batch start
#iterations: 327
currently lose_sum: 94.39612853527069
time_elpased: 1.855
batch start
#iterations: 328
currently lose_sum: 94.04442328214645
time_elpased: 1.855
batch start
#iterations: 329
currently lose_sum: 94.8516880273819
time_elpased: 1.9
batch start
#iterations: 330
currently lose_sum: 94.36903685331345
time_elpased: 1.863
batch start
#iterations: 331
currently lose_sum: 94.49984031915665
time_elpased: 1.906
batch start
#iterations: 332
currently lose_sum: 94.11003631353378
time_elpased: 1.947
batch start
#iterations: 333
currently lose_sum: 94.57098031044006
time_elpased: 1.905
batch start
#iterations: 334
currently lose_sum: 94.57122868299484
time_elpased: 1.842
batch start
#iterations: 335
currently lose_sum: 94.67936724424362
time_elpased: 1.881
batch start
#iterations: 336
currently lose_sum: 94.32147806882858
time_elpased: 1.863
batch start
#iterations: 337
currently lose_sum: 94.65243971347809
time_elpased: 1.882
batch start
#iterations: 338
currently lose_sum: 94.23718190193176
time_elpased: 1.863
batch start
#iterations: 339
currently lose_sum: 94.19401615858078
time_elpased: 1.892
start validation test
0.662525773196
0.650751379114
0.704126788103
0.676387721813
0.662452736198
60.742
batch start
#iterations: 340
currently lose_sum: 94.20778012275696
time_elpased: 1.912
batch start
#iterations: 341
currently lose_sum: 94.44110614061356
time_elpased: 1.894
batch start
#iterations: 342
currently lose_sum: 94.24628609418869
time_elpased: 1.855
batch start
#iterations: 343
currently lose_sum: 94.79366040229797
time_elpased: 1.87
batch start
#iterations: 344
currently lose_sum: 94.19893050193787
time_elpased: 1.916
batch start
#iterations: 345
currently lose_sum: 94.65483105182648
time_elpased: 1.918
batch start
#iterations: 346
currently lose_sum: 94.21069657802582
time_elpased: 1.847
batch start
#iterations: 347
currently lose_sum: 94.78352737426758
time_elpased: 1.87
batch start
#iterations: 348
currently lose_sum: 94.48064821958542
time_elpased: 1.897
batch start
#iterations: 349
currently lose_sum: 94.73125356435776
time_elpased: 1.877
batch start
#iterations: 350
currently lose_sum: 94.32976859807968
time_elpased: 1.921
batch start
#iterations: 351
currently lose_sum: 94.11150926351547
time_elpased: 1.91
batch start
#iterations: 352
currently lose_sum: 94.33369994163513
time_elpased: 1.863
batch start
#iterations: 353
currently lose_sum: 94.47105264663696
time_elpased: 1.9
batch start
#iterations: 354
currently lose_sum: 94.69238412380219
time_elpased: 1.919
batch start
#iterations: 355
currently lose_sum: 94.39528715610504
time_elpased: 1.869
batch start
#iterations: 356
currently lose_sum: 94.75610011816025
time_elpased: 1.886
batch start
#iterations: 357
currently lose_sum: 94.47119843959808
time_elpased: 1.984
batch start
#iterations: 358
currently lose_sum: 94.55604481697083
time_elpased: 1.912
batch start
#iterations: 359
currently lose_sum: 94.51245903968811
time_elpased: 1.935
start validation test
0.624639175258
0.665353796007
0.504167953072
0.573653395785
0.62485068107
61.878
batch start
#iterations: 360
currently lose_sum: 94.51258254051208
time_elpased: 1.88
batch start
#iterations: 361
currently lose_sum: 94.65384125709534
time_elpased: 1.896
batch start
#iterations: 362
currently lose_sum: 94.14691984653473
time_elpased: 1.908
batch start
#iterations: 363
currently lose_sum: 94.55535924434662
time_elpased: 1.929
batch start
#iterations: 364
currently lose_sum: 94.32513028383255
time_elpased: 1.865
batch start
#iterations: 365
currently lose_sum: 94.61430412530899
time_elpased: 1.936
batch start
#iterations: 366
currently lose_sum: 94.15664726495743
time_elpased: 1.921
batch start
#iterations: 367
currently lose_sum: 94.66025882959366
time_elpased: 1.896
batch start
#iterations: 368
currently lose_sum: 94.6270279288292
time_elpased: 1.883
batch start
#iterations: 369
currently lose_sum: 94.56226241588593
time_elpased: 1.942
batch start
#iterations: 370
currently lose_sum: 93.80434948205948
time_elpased: 1.883
batch start
#iterations: 371
currently lose_sum: 94.20924425125122
time_elpased: 2.018
batch start
#iterations: 372
currently lose_sum: 94.25147676467896
time_elpased: 1.913
batch start
#iterations: 373
currently lose_sum: 94.09515994787216
time_elpased: 1.888
batch start
#iterations: 374
currently lose_sum: 94.28122991323471
time_elpased: 1.85
batch start
#iterations: 375
currently lose_sum: 94.79022026062012
time_elpased: 1.905
batch start
#iterations: 376
currently lose_sum: 94.35332429409027
time_elpased: 1.865
batch start
#iterations: 377
currently lose_sum: 94.11046886444092
time_elpased: 1.855
batch start
#iterations: 378
currently lose_sum: 94.4917277097702
time_elpased: 1.923
batch start
#iterations: 379
currently lose_sum: 94.307668030262
time_elpased: 1.848
start validation test
0.664329896907
0.637282618007
0.765565503756
0.695558672277
0.664152162185
60.459
batch start
#iterations: 380
currently lose_sum: 94.3471828699112
time_elpased: 1.916
batch start
#iterations: 381
currently lose_sum: 94.41053986549377
time_elpased: 1.902
batch start
#iterations: 382
currently lose_sum: 94.22861248254776
time_elpased: 1.91
batch start
#iterations: 383
currently lose_sum: 94.17449694871902
time_elpased: 1.991
batch start
#iterations: 384
currently lose_sum: 94.74274909496307
time_elpased: 1.855
batch start
#iterations: 385
currently lose_sum: 94.24947106838226
time_elpased: 1.934
batch start
#iterations: 386
currently lose_sum: 94.30343437194824
time_elpased: 1.87
batch start
#iterations: 387
currently lose_sum: 94.43507105112076
time_elpased: 1.876
batch start
#iterations: 388
currently lose_sum: 94.11431807279587
time_elpased: 1.889
batch start
#iterations: 389
currently lose_sum: 94.72951376438141
time_elpased: 1.886
batch start
#iterations: 390
currently lose_sum: 94.54041999578476
time_elpased: 1.907
batch start
#iterations: 391
currently lose_sum: 94.52711981534958
time_elpased: 1.821
batch start
#iterations: 392
currently lose_sum: 94.14338493347168
time_elpased: 1.951
batch start
#iterations: 393
currently lose_sum: 94.47815161943436
time_elpased: 1.945
batch start
#iterations: 394
currently lose_sum: 94.11855953931808
time_elpased: 1.862
batch start
#iterations: 395
currently lose_sum: 94.05364614725113
time_elpased: 1.917
batch start
#iterations: 396
currently lose_sum: 94.27863627672195
time_elpased: 2.003
batch start
#iterations: 397
currently lose_sum: 93.9480174779892
time_elpased: 1.941
batch start
#iterations: 398
currently lose_sum: 93.99712777137756
time_elpased: 1.937
batch start
#iterations: 399
currently lose_sum: 94.39787411689758
time_elpased: 1.988
start validation test
0.667216494845
0.635954306679
0.784913039004
0.702625518194
0.667009860409
60.287
acc: 0.665
pre: 0.637
rec: 0.773
F1: 0.698
auc: 0.665
