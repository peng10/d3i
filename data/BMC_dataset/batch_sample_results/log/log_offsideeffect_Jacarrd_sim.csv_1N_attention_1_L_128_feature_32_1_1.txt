start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.75577807426453
time_elpased: 1.94
batch start
#iterations: 1
currently lose_sum: 99.99449425935745
time_elpased: 1.806
batch start
#iterations: 2
currently lose_sum: 99.84665763378143
time_elpased: 1.849
batch start
#iterations: 3
currently lose_sum: 99.62192523479462
time_elpased: 1.793
batch start
#iterations: 4
currently lose_sum: 99.52919012308121
time_elpased: 1.803
batch start
#iterations: 5
currently lose_sum: 99.23105961084366
time_elpased: 1.823
batch start
#iterations: 6
currently lose_sum: 98.9652368426323
time_elpased: 1.791
batch start
#iterations: 7
currently lose_sum: 98.47416657209396
time_elpased: 1.821
batch start
#iterations: 8
currently lose_sum: 98.71688234806061
time_elpased: 1.813
batch start
#iterations: 9
currently lose_sum: 98.40565019845963
time_elpased: 1.791
batch start
#iterations: 10
currently lose_sum: 98.15326917171478
time_elpased: 1.804
batch start
#iterations: 11
currently lose_sum: 98.06069165468216
time_elpased: 1.827
batch start
#iterations: 12
currently lose_sum: 97.69149243831635
time_elpased: 1.836
batch start
#iterations: 13
currently lose_sum: 97.76654255390167
time_elpased: 1.793
batch start
#iterations: 14
currently lose_sum: 97.4801869392395
time_elpased: 1.816
batch start
#iterations: 15
currently lose_sum: 97.4517428278923
time_elpased: 1.808
batch start
#iterations: 16
currently lose_sum: 97.41816407442093
time_elpased: 1.782
batch start
#iterations: 17
currently lose_sum: 97.09185838699341
time_elpased: 1.807
batch start
#iterations: 18
currently lose_sum: 97.1585704088211
time_elpased: 1.82
batch start
#iterations: 19
currently lose_sum: 97.0326834321022
time_elpased: 1.797
start validation test
0.641546391753
0.659840333218
0.586909539981
0.621241830065
0.641642315173
62.826
batch start
#iterations: 20
currently lose_sum: 97.22685724496841
time_elpased: 1.835
batch start
#iterations: 21
currently lose_sum: 96.84138798713684
time_elpased: 1.845
batch start
#iterations: 22
currently lose_sum: 97.0190059542656
time_elpased: 1.801
batch start
#iterations: 23
currently lose_sum: 96.6337560415268
time_elpased: 1.811
batch start
#iterations: 24
currently lose_sum: 97.02343934774399
time_elpased: 1.799
batch start
#iterations: 25
currently lose_sum: 96.61577528715134
time_elpased: 1.892
batch start
#iterations: 26
currently lose_sum: 96.64821094274521
time_elpased: 1.895
batch start
#iterations: 27
currently lose_sum: 96.65120023488998
time_elpased: 1.837
batch start
#iterations: 28
currently lose_sum: 96.44870740175247
time_elpased: 1.857
batch start
#iterations: 29
currently lose_sum: 96.72425323724747
time_elpased: 1.838
batch start
#iterations: 30
currently lose_sum: 96.48249554634094
time_elpased: 1.859
batch start
#iterations: 31
currently lose_sum: 96.22920382022858
time_elpased: 1.828
batch start
#iterations: 32
currently lose_sum: 96.16895306110382
time_elpased: 1.845
batch start
#iterations: 33
currently lose_sum: 96.37877637147903
time_elpased: 1.832
batch start
#iterations: 34
currently lose_sum: 96.3155826330185
time_elpased: 1.811
batch start
#iterations: 35
currently lose_sum: 96.65583336353302
time_elpased: 1.843
batch start
#iterations: 36
currently lose_sum: 96.412995159626
time_elpased: 1.826
batch start
#iterations: 37
currently lose_sum: 96.51297533512115
time_elpased: 1.862
batch start
#iterations: 38
currently lose_sum: 96.14595746994019
time_elpased: 1.817
batch start
#iterations: 39
currently lose_sum: 95.91077667474747
time_elpased: 1.866
start validation test
0.654896907216
0.639391143911
0.713285993619
0.674320182906
0.654794396169
61.427
batch start
#iterations: 40
currently lose_sum: 96.36399871110916
time_elpased: 1.903
batch start
#iterations: 41
currently lose_sum: 96.1178480386734
time_elpased: 1.842
batch start
#iterations: 42
currently lose_sum: 96.1420641541481
time_elpased: 1.814
batch start
#iterations: 43
currently lose_sum: 96.08741450309753
time_elpased: 1.834
batch start
#iterations: 44
currently lose_sum: 95.96377331018448
time_elpased: 1.81
batch start
#iterations: 45
currently lose_sum: 96.28489756584167
time_elpased: 1.85
batch start
#iterations: 46
currently lose_sum: 95.63799303770065
time_elpased: 1.813
batch start
#iterations: 47
currently lose_sum: 96.05230361223221
time_elpased: 1.823
batch start
#iterations: 48
currently lose_sum: 96.24348258972168
time_elpased: 1.791
batch start
#iterations: 49
currently lose_sum: 96.01929008960724
time_elpased: 1.812
batch start
#iterations: 50
currently lose_sum: 96.09763938188553
time_elpased: 1.839
batch start
#iterations: 51
currently lose_sum: 96.12467288970947
time_elpased: 1.806
batch start
#iterations: 52
currently lose_sum: 96.31986719369888
time_elpased: 1.818
batch start
#iterations: 53
currently lose_sum: 95.73759937286377
time_elpased: 1.805
batch start
#iterations: 54
currently lose_sum: 96.31183868646622
time_elpased: 1.809
batch start
#iterations: 55
currently lose_sum: 95.99182987213135
time_elpased: 1.846
batch start
#iterations: 56
currently lose_sum: 95.51207876205444
time_elpased: 1.83
batch start
#iterations: 57
currently lose_sum: 95.65742713212967
time_elpased: 1.801
batch start
#iterations: 58
currently lose_sum: 95.82652246952057
time_elpased: 1.857
batch start
#iterations: 59
currently lose_sum: 95.82034850120544
time_elpased: 1.84
start validation test
0.652680412371
0.663447821793
0.622208500566
0.642166755178
0.652733910512
61.666
batch start
#iterations: 60
currently lose_sum: 95.8518875837326
time_elpased: 1.863
batch start
#iterations: 61
currently lose_sum: 95.76979279518127
time_elpased: 1.832
batch start
#iterations: 62
currently lose_sum: 95.67100423574448
time_elpased: 1.792
batch start
#iterations: 63
currently lose_sum: 96.2147645354271
time_elpased: 1.809
batch start
#iterations: 64
currently lose_sum: 95.82537311315536
time_elpased: 1.838
batch start
#iterations: 65
currently lose_sum: 95.37544536590576
time_elpased: 1.819
batch start
#iterations: 66
currently lose_sum: 95.54374545812607
time_elpased: 1.833
batch start
#iterations: 67
currently lose_sum: 95.82069164514542
time_elpased: 1.794
batch start
#iterations: 68
currently lose_sum: 95.84635680913925
time_elpased: 1.804
batch start
#iterations: 69
currently lose_sum: 95.89220356941223
time_elpased: 1.821
batch start
#iterations: 70
currently lose_sum: 95.91459995508194
time_elpased: 1.818
batch start
#iterations: 71
currently lose_sum: 95.55995559692383
time_elpased: 1.819
batch start
#iterations: 72
currently lose_sum: 95.6908768415451
time_elpased: 1.783
batch start
#iterations: 73
currently lose_sum: 95.51334863901138
time_elpased: 1.8
batch start
#iterations: 74
currently lose_sum: 95.58553004264832
time_elpased: 1.787
batch start
#iterations: 75
currently lose_sum: 95.42610436677933
time_elpased: 1.809
batch start
#iterations: 76
currently lose_sum: 95.2898678779602
time_elpased: 1.796
batch start
#iterations: 77
currently lose_sum: 95.60194712877274
time_elpased: 1.825
batch start
#iterations: 78
currently lose_sum: 95.46095424890518
time_elpased: 1.789
batch start
#iterations: 79
currently lose_sum: 95.87397593259811
time_elpased: 1.797
start validation test
0.657835051546
0.648043081065
0.693526808686
0.670013919268
0.657772389162
61.548
batch start
#iterations: 80
currently lose_sum: 95.73385399580002
time_elpased: 1.819
batch start
#iterations: 81
currently lose_sum: 95.61782747507095
time_elpased: 1.808
batch start
#iterations: 82
currently lose_sum: 95.81131613254547
time_elpased: 1.797
batch start
#iterations: 83
currently lose_sum: 95.73079770803452
time_elpased: 1.811
batch start
#iterations: 84
currently lose_sum: 95.51822936534882
time_elpased: 1.793
batch start
#iterations: 85
currently lose_sum: 95.81764328479767
time_elpased: 1.828
batch start
#iterations: 86
currently lose_sum: 95.7130988240242
time_elpased: 1.836
batch start
#iterations: 87
currently lose_sum: 95.14612942934036
time_elpased: 1.836
batch start
#iterations: 88
currently lose_sum: 95.52637547254562
time_elpased: 1.789
batch start
#iterations: 89
currently lose_sum: 95.06479758024216
time_elpased: 1.806
batch start
#iterations: 90
currently lose_sum: 95.6347388625145
time_elpased: 1.789
batch start
#iterations: 91
currently lose_sum: 95.56387680768967
time_elpased: 1.835
batch start
#iterations: 92
currently lose_sum: 95.29540550708771
time_elpased: 1.801
batch start
#iterations: 93
currently lose_sum: 95.61018860340118
time_elpased: 1.79
batch start
#iterations: 94
currently lose_sum: 95.23732364177704
time_elpased: 1.781
batch start
#iterations: 95
currently lose_sum: 95.47858518362045
time_elpased: 1.821
batch start
#iterations: 96
currently lose_sum: 95.61783283948898
time_elpased: 1.782
batch start
#iterations: 97
currently lose_sum: 95.37139320373535
time_elpased: 1.79
batch start
#iterations: 98
currently lose_sum: 95.47484856843948
time_elpased: 1.813
batch start
#iterations: 99
currently lose_sum: 95.04089814424515
time_elpased: 1.813
start validation test
0.663505154639
0.638543748371
0.756303385819
0.692452652407
0.663342233031
61.253
batch start
#iterations: 100
currently lose_sum: 95.40098136663437
time_elpased: 1.812
batch start
#iterations: 101
currently lose_sum: 95.50977957248688
time_elpased: 1.796
batch start
#iterations: 102
currently lose_sum: 94.79329532384872
time_elpased: 1.792
batch start
#iterations: 103
currently lose_sum: 95.17719429731369
time_elpased: 1.797
batch start
#iterations: 104
currently lose_sum: 95.57366049289703
time_elpased: 1.838
batch start
#iterations: 105
currently lose_sum: 95.50989556312561
time_elpased: 1.836
batch start
#iterations: 106
currently lose_sum: 95.35834348201752
time_elpased: 1.784
batch start
#iterations: 107
currently lose_sum: 95.60434675216675
time_elpased: 1.786
batch start
#iterations: 108
currently lose_sum: 95.77179992198944
time_elpased: 1.808
batch start
#iterations: 109
currently lose_sum: 95.32439959049225
time_elpased: 1.786
batch start
#iterations: 110
currently lose_sum: 95.223069190979
time_elpased: 1.834
batch start
#iterations: 111
currently lose_sum: 95.18121010065079
time_elpased: 1.784
batch start
#iterations: 112
currently lose_sum: 95.16254162788391
time_elpased: 1.786
batch start
#iterations: 113
currently lose_sum: 95.21027094125748
time_elpased: 1.797
batch start
#iterations: 114
currently lose_sum: 94.92613923549652
time_elpased: 1.818
batch start
#iterations: 115
currently lose_sum: 95.16426259279251
time_elpased: 1.797
batch start
#iterations: 116
currently lose_sum: 95.36658865213394
time_elpased: 1.78
batch start
#iterations: 117
currently lose_sum: 95.25348567962646
time_elpased: 1.783
batch start
#iterations: 118
currently lose_sum: 95.49360048770905
time_elpased: 1.785
batch start
#iterations: 119
currently lose_sum: 95.29508912563324
time_elpased: 1.794
start validation test
0.648298969072
0.666820382753
0.595245446125
0.62900331684
0.648392112715
61.824
batch start
#iterations: 120
currently lose_sum: 95.09909975528717
time_elpased: 1.804
batch start
#iterations: 121
currently lose_sum: 95.30246376991272
time_elpased: 1.793
batch start
#iterations: 122
currently lose_sum: 95.32611674070358
time_elpased: 1.796
batch start
#iterations: 123
currently lose_sum: 94.94988483190536
time_elpased: 1.793
batch start
#iterations: 124
currently lose_sum: 95.14653700590134
time_elpased: 1.815
batch start
#iterations: 125
currently lose_sum: 95.35129505395889
time_elpased: 1.792
batch start
#iterations: 126
currently lose_sum: 95.44183826446533
time_elpased: 1.803
batch start
#iterations: 127
currently lose_sum: 95.43096095323563
time_elpased: 1.822
batch start
#iterations: 128
currently lose_sum: 95.19339656829834
time_elpased: 1.793
batch start
#iterations: 129
currently lose_sum: 94.77684479951859
time_elpased: 1.797
batch start
#iterations: 130
currently lose_sum: 95.17251282930374
time_elpased: 1.802
batch start
#iterations: 131
currently lose_sum: 94.86558413505554
time_elpased: 1.809
batch start
#iterations: 132
currently lose_sum: 95.07845115661621
time_elpased: 1.786
batch start
#iterations: 133
currently lose_sum: 95.10226845741272
time_elpased: 1.842
batch start
#iterations: 134
currently lose_sum: 95.24210035800934
time_elpased: 1.803
batch start
#iterations: 135
currently lose_sum: 95.44028574228287
time_elpased: 1.818
batch start
#iterations: 136
currently lose_sum: 94.61773872375488
time_elpased: 1.816
batch start
#iterations: 137
currently lose_sum: 95.38942837715149
time_elpased: 1.828
batch start
#iterations: 138
currently lose_sum: 94.8308225274086
time_elpased: 1.809
batch start
#iterations: 139
currently lose_sum: 95.39215904474258
time_elpased: 1.78
start validation test
0.668608247423
0.632816286961
0.806112997839
0.709029192125
0.668366836625
61.033
batch start
#iterations: 140
currently lose_sum: 95.22066402435303
time_elpased: 1.862
batch start
#iterations: 141
currently lose_sum: 95.48610955476761
time_elpased: 1.819
batch start
#iterations: 142
currently lose_sum: 94.75063019990921
time_elpased: 1.79
batch start
#iterations: 143
currently lose_sum: 95.1249783039093
time_elpased: 1.81
batch start
#iterations: 144
currently lose_sum: 95.16053587198257
time_elpased: 1.859
batch start
#iterations: 145
currently lose_sum: 95.39370226860046
time_elpased: 1.782
batch start
#iterations: 146
currently lose_sum: 95.0169620513916
time_elpased: 1.813
batch start
#iterations: 147
currently lose_sum: 94.96787923574448
time_elpased: 1.801
batch start
#iterations: 148
currently lose_sum: 95.53823125362396
time_elpased: 1.771
batch start
#iterations: 149
currently lose_sum: 95.03074425458908
time_elpased: 1.805
batch start
#iterations: 150
currently lose_sum: 95.00806772708893
time_elpased: 1.795
batch start
#iterations: 151
currently lose_sum: 94.82711708545685
time_elpased: 1.795
batch start
#iterations: 152
currently lose_sum: 95.36108380556107
time_elpased: 1.82
batch start
#iterations: 153
currently lose_sum: 95.23899412155151
time_elpased: 1.79
batch start
#iterations: 154
currently lose_sum: 95.21641570329666
time_elpased: 1.799
batch start
#iterations: 155
currently lose_sum: 95.19857275485992
time_elpased: 1.801
batch start
#iterations: 156
currently lose_sum: 94.98947137594223
time_elpased: 1.8
batch start
#iterations: 157
currently lose_sum: 95.40446788072586
time_elpased: 1.806
batch start
#iterations: 158
currently lose_sum: 95.63272047042847
time_elpased: 1.795
batch start
#iterations: 159
currently lose_sum: 95.72269076108932
time_elpased: 1.792
start validation test
0.664587628866
0.641359873172
0.749408253576
0.691186939395
0.664438713177
61.184
batch start
#iterations: 160
currently lose_sum: 95.10216850042343
time_elpased: 1.858
batch start
#iterations: 161
currently lose_sum: 95.18511486053467
time_elpased: 1.826
batch start
#iterations: 162
currently lose_sum: 94.80029231309891
time_elpased: 1.814
batch start
#iterations: 163
currently lose_sum: 94.7887396812439
time_elpased: 1.812
batch start
#iterations: 164
currently lose_sum: 94.88143503665924
time_elpased: 1.826
batch start
#iterations: 165
currently lose_sum: 95.24579638242722
time_elpased: 1.83
batch start
#iterations: 166
currently lose_sum: 94.99192428588867
time_elpased: 1.842
batch start
#iterations: 167
currently lose_sum: 95.22089725732803
time_elpased: 1.817
batch start
#iterations: 168
currently lose_sum: 94.73989689350128
time_elpased: 1.835
batch start
#iterations: 169
currently lose_sum: 94.82789868116379
time_elpased: 1.793
batch start
#iterations: 170
currently lose_sum: 94.95608729124069
time_elpased: 1.808
batch start
#iterations: 171
currently lose_sum: 95.18744671344757
time_elpased: 1.79
batch start
#iterations: 172
currently lose_sum: 95.18790864944458
time_elpased: 1.97
batch start
#iterations: 173
currently lose_sum: 94.94799906015396
time_elpased: 1.806
batch start
#iterations: 174
currently lose_sum: 95.03822255134583
time_elpased: 1.821
batch start
#iterations: 175
currently lose_sum: 94.7952830195427
time_elpased: 1.786
batch start
#iterations: 176
currently lose_sum: 94.76107388734818
time_elpased: 1.784
batch start
#iterations: 177
currently lose_sum: 94.72937005758286
time_elpased: 1.788
batch start
#iterations: 178
currently lose_sum: 95.18307507038116
time_elpased: 1.791
batch start
#iterations: 179
currently lose_sum: 95.13845300674438
time_elpased: 1.792
start validation test
0.663762886598
0.636100221578
0.768138314295
0.695911612512
0.663579639436
61.036
batch start
#iterations: 180
currently lose_sum: 94.98527401685715
time_elpased: 1.846
batch start
#iterations: 181
currently lose_sum: 95.21966177225113
time_elpased: 1.812
batch start
#iterations: 182
currently lose_sum: 94.89832854270935
time_elpased: 1.781
batch start
#iterations: 183
currently lose_sum: 94.38159185647964
time_elpased: 1.773
batch start
#iterations: 184
currently lose_sum: 94.71936255693436
time_elpased: 1.796
batch start
#iterations: 185
currently lose_sum: 95.03991562128067
time_elpased: 1.801
batch start
#iterations: 186
currently lose_sum: 94.62684315443039
time_elpased: 1.79
batch start
#iterations: 187
currently lose_sum: 94.5857572555542
time_elpased: 1.776
batch start
#iterations: 188
currently lose_sum: 94.9167730808258
time_elpased: 1.772
batch start
#iterations: 189
currently lose_sum: 94.98369294404984
time_elpased: 1.79
batch start
#iterations: 190
currently lose_sum: 95.0517110824585
time_elpased: 1.861
batch start
#iterations: 191
currently lose_sum: 94.92303866147995
time_elpased: 1.827
batch start
#iterations: 192
currently lose_sum: 94.89980924129486
time_elpased: 1.799
batch start
#iterations: 193
currently lose_sum: 95.02040982246399
time_elpased: 1.783
batch start
#iterations: 194
currently lose_sum: 94.87760055065155
time_elpased: 1.801
batch start
#iterations: 195
currently lose_sum: 95.03845179080963
time_elpased: 1.792
batch start
#iterations: 196
currently lose_sum: 95.39783841371536
time_elpased: 1.808
batch start
#iterations: 197
currently lose_sum: 94.75224196910858
time_elpased: 1.796
batch start
#iterations: 198
currently lose_sum: 94.99075508117676
time_elpased: 1.791
batch start
#iterations: 199
currently lose_sum: 94.96158105134964
time_elpased: 1.805
start validation test
0.666340206186
0.640287147552
0.761860656581
0.695803374219
0.666172505302
61.048
batch start
#iterations: 200
currently lose_sum: 94.63728535175323
time_elpased: 1.831
batch start
#iterations: 201
currently lose_sum: 94.5040374994278
time_elpased: 1.778
batch start
#iterations: 202
currently lose_sum: 94.83324193954468
time_elpased: 1.792
batch start
#iterations: 203
currently lose_sum: 94.77998316287994
time_elpased: 1.808
batch start
#iterations: 204
currently lose_sum: 94.87131828069687
time_elpased: 1.798
batch start
#iterations: 205
currently lose_sum: 94.56053000688553
time_elpased: 1.793
batch start
#iterations: 206
currently lose_sum: 94.93416905403137
time_elpased: 1.789
batch start
#iterations: 207
currently lose_sum: 95.10179531574249
time_elpased: 1.844
batch start
#iterations: 208
currently lose_sum: 94.58710163831711
time_elpased: 1.807
batch start
#iterations: 209
currently lose_sum: 94.69540417194366
time_elpased: 1.788
batch start
#iterations: 210
currently lose_sum: 94.87454748153687
time_elpased: 1.854
batch start
#iterations: 211
currently lose_sum: 94.75866276025772
time_elpased: 1.854
batch start
#iterations: 212
currently lose_sum: 94.52948236465454
time_elpased: 1.869
batch start
#iterations: 213
currently lose_sum: 94.84313017129898
time_elpased: 1.809
batch start
#iterations: 214
currently lose_sum: 94.54149436950684
time_elpased: 1.855
batch start
#iterations: 215
currently lose_sum: 94.89079713821411
time_elpased: 1.809
batch start
#iterations: 216
currently lose_sum: 94.35849720239639
time_elpased: 1.827
batch start
#iterations: 217
currently lose_sum: 94.86536860466003
time_elpased: 1.846
batch start
#iterations: 218
currently lose_sum: 95.08615851402283
time_elpased: 1.81
batch start
#iterations: 219
currently lose_sum: 94.89226722717285
time_elpased: 1.804
start validation test
0.661443298969
0.630544730951
0.78264896573
0.698411240702
0.661230503726
61.065
batch start
#iterations: 220
currently lose_sum: 94.98807311058044
time_elpased: 1.837
batch start
#iterations: 221
currently lose_sum: 94.54412055015564
time_elpased: 1.782
batch start
#iterations: 222
currently lose_sum: 94.87698197364807
time_elpased: 1.799
batch start
#iterations: 223
currently lose_sum: 94.47605609893799
time_elpased: 1.8
batch start
#iterations: 224
currently lose_sum: 94.55762845277786
time_elpased: 1.797
batch start
#iterations: 225
currently lose_sum: 94.9458509683609
time_elpased: 1.803
batch start
#iterations: 226
currently lose_sum: 94.64700698852539
time_elpased: 1.797
batch start
#iterations: 227
currently lose_sum: 95.0102276802063
time_elpased: 1.807
batch start
#iterations: 228
currently lose_sum: 94.50745648145676
time_elpased: 1.843
batch start
#iterations: 229
currently lose_sum: 94.25821977853775
time_elpased: 2.014
batch start
#iterations: 230
currently lose_sum: 94.83028078079224
time_elpased: 1.908
batch start
#iterations: 231
currently lose_sum: 94.73008531332016
time_elpased: 1.903
batch start
#iterations: 232
currently lose_sum: 94.63035851716995
time_elpased: 1.802
batch start
#iterations: 233
currently lose_sum: 94.89950162172318
time_elpased: 1.843
batch start
#iterations: 234
currently lose_sum: 94.53701883554459
time_elpased: 1.815
batch start
#iterations: 235
currently lose_sum: 95.05294448137283
time_elpased: 1.821
batch start
#iterations: 236
currently lose_sum: 94.8070769906044
time_elpased: 1.879
batch start
#iterations: 237
currently lose_sum: 94.93395835161209
time_elpased: 1.884
batch start
#iterations: 238
currently lose_sum: 95.01432394981384
time_elpased: 1.901
batch start
#iterations: 239
currently lose_sum: 95.04315829277039
time_elpased: 1.904
start validation test
0.663659793814
0.64039408867
0.749202428733
0.690538297368
0.663509610525
60.682
batch start
#iterations: 240
currently lose_sum: 94.6915675997734
time_elpased: 1.898
batch start
#iterations: 241
currently lose_sum: 94.6783674955368
time_elpased: 1.813
batch start
#iterations: 242
currently lose_sum: 95.03413647413254
time_elpased: 1.921
batch start
#iterations: 243
currently lose_sum: 94.60316914319992
time_elpased: 1.871
batch start
#iterations: 244
currently lose_sum: 94.93088042736053
time_elpased: 1.867
batch start
#iterations: 245
currently lose_sum: 94.9085014462471
time_elpased: 1.836
batch start
#iterations: 246
currently lose_sum: 94.62019270658493
time_elpased: 1.843
batch start
#iterations: 247
currently lose_sum: 94.93110620975494
time_elpased: 1.813
batch start
#iterations: 248
currently lose_sum: 94.62456101179123
time_elpased: 1.83
batch start
#iterations: 249
currently lose_sum: 94.58758002519608
time_elpased: 1.834
batch start
#iterations: 250
currently lose_sum: 94.78133058547974
time_elpased: 1.818
batch start
#iterations: 251
currently lose_sum: 94.73490422964096
time_elpased: 1.847
batch start
#iterations: 252
currently lose_sum: 94.96169936656952
time_elpased: 1.827
batch start
#iterations: 253
currently lose_sum: 94.6685808300972
time_elpased: 1.818
batch start
#iterations: 254
currently lose_sum: 94.91975563764572
time_elpased: 1.83
batch start
#iterations: 255
currently lose_sum: 94.79859191179276
time_elpased: 1.789
batch start
#iterations: 256
currently lose_sum: 94.4417741894722
time_elpased: 1.859
batch start
#iterations: 257
currently lose_sum: 94.37360489368439
time_elpased: 1.845
batch start
#iterations: 258
currently lose_sum: 94.2052743434906
time_elpased: 1.83
batch start
#iterations: 259
currently lose_sum: 94.3449274301529
time_elpased: 1.829
start validation test
0.665979381443
0.644547646691
0.742718946177
0.690159701635
0.665844653301
60.857
batch start
#iterations: 260
currently lose_sum: 94.51776766777039
time_elpased: 1.877
batch start
#iterations: 261
currently lose_sum: 94.37191784381866
time_elpased: 1.833
batch start
#iterations: 262
currently lose_sum: 94.79523086547852
time_elpased: 1.841
batch start
#iterations: 263
currently lose_sum: 94.47203361988068
time_elpased: 1.892
batch start
#iterations: 264
currently lose_sum: 94.76872324943542
time_elpased: 1.807
batch start
#iterations: 265
currently lose_sum: 94.85516375303268
time_elpased: 1.8
batch start
#iterations: 266
currently lose_sum: 94.71438378095627
time_elpased: 1.833
batch start
#iterations: 267
currently lose_sum: 94.99414855241776
time_elpased: 1.783
batch start
#iterations: 268
currently lose_sum: 94.37934565544128
time_elpased: 1.838
batch start
#iterations: 269
currently lose_sum: 94.61909079551697
time_elpased: 1.823
batch start
#iterations: 270
currently lose_sum: 94.37588495016098
time_elpased: 1.84
batch start
#iterations: 271
currently lose_sum: 94.32969850301743
time_elpased: 1.8
batch start
#iterations: 272
currently lose_sum: 94.92465031147003
time_elpased: 1.834
batch start
#iterations: 273
currently lose_sum: 94.75470048189163
time_elpased: 1.824
batch start
#iterations: 274
currently lose_sum: 94.87753331661224
time_elpased: 1.83
batch start
#iterations: 275
currently lose_sum: 94.80311185121536
time_elpased: 1.791
batch start
#iterations: 276
currently lose_sum: 94.2864625453949
time_elpased: 1.809
batch start
#iterations: 277
currently lose_sum: 94.60669475793839
time_elpased: 1.795
batch start
#iterations: 278
currently lose_sum: 94.56302577257156
time_elpased: 1.807
batch start
#iterations: 279
currently lose_sum: 94.62914252281189
time_elpased: 1.854
start validation test
0.665979381443
0.644858140159
0.741483997118
0.689803733844
0.665846821445
60.820
batch start
#iterations: 280
currently lose_sum: 94.38761758804321
time_elpased: 1.862
batch start
#iterations: 281
currently lose_sum: 94.57992839813232
time_elpased: 1.852
batch start
#iterations: 282
currently lose_sum: 94.45856058597565
time_elpased: 1.883
batch start
#iterations: 283
currently lose_sum: 94.50380527973175
time_elpased: 1.797
batch start
#iterations: 284
currently lose_sum: 94.71589314937592
time_elpased: 1.798
batch start
#iterations: 285
currently lose_sum: 94.69828408956528
time_elpased: 1.785
batch start
#iterations: 286
currently lose_sum: 94.31524574756622
time_elpased: 1.825
batch start
#iterations: 287
currently lose_sum: 94.36778265237808
time_elpased: 1.791
batch start
#iterations: 288
currently lose_sum: 93.95345962047577
time_elpased: 1.851
batch start
#iterations: 289
currently lose_sum: 94.81675493717194
time_elpased: 1.78
batch start
#iterations: 290
currently lose_sum: 94.96995729207993
time_elpased: 1.821
batch start
#iterations: 291
currently lose_sum: 94.6672894358635
time_elpased: 1.859
batch start
#iterations: 292
currently lose_sum: 94.36196511983871
time_elpased: 1.839
batch start
#iterations: 293
currently lose_sum: 94.91482031345367
time_elpased: 1.791
batch start
#iterations: 294
currently lose_sum: 94.40449726581573
time_elpased: 1.81
batch start
#iterations: 295
currently lose_sum: 94.8277867436409
time_elpased: 1.791
batch start
#iterations: 296
currently lose_sum: 94.25974786281586
time_elpased: 1.861
batch start
#iterations: 297
currently lose_sum: 94.56090515851974
time_elpased: 1.813
batch start
#iterations: 298
currently lose_sum: 94.87731748819351
time_elpased: 1.794
batch start
#iterations: 299
currently lose_sum: 94.37753850221634
time_elpased: 1.826
start validation test
0.669536082474
0.638511814982
0.784192652053
0.703893584592
0.669334785182
60.636
batch start
#iterations: 300
currently lose_sum: 94.43930149078369
time_elpased: 1.802
batch start
#iterations: 301
currently lose_sum: 94.295256793499
time_elpased: 1.8
batch start
#iterations: 302
currently lose_sum: 94.48486089706421
time_elpased: 1.801
batch start
#iterations: 303
currently lose_sum: 94.45117604732513
time_elpased: 1.813
batch start
#iterations: 304
currently lose_sum: 94.6921883225441
time_elpased: 1.873
batch start
#iterations: 305
currently lose_sum: 94.84696060419083
time_elpased: 1.82
batch start
#iterations: 306
currently lose_sum: 94.54280745983124
time_elpased: 1.818
batch start
#iterations: 307
currently lose_sum: 94.83778184652328
time_elpased: 1.788
batch start
#iterations: 308
currently lose_sum: 94.34134691953659
time_elpased: 1.823
batch start
#iterations: 309
currently lose_sum: 94.48974734544754
time_elpased: 1.803
batch start
#iterations: 310
currently lose_sum: 94.63786113262177
time_elpased: 1.78
batch start
#iterations: 311
currently lose_sum: 94.15262246131897
time_elpased: 1.815
batch start
#iterations: 312
currently lose_sum: 94.19551819562912
time_elpased: 1.791
batch start
#iterations: 313
currently lose_sum: 94.56803530454636
time_elpased: 1.843
batch start
#iterations: 314
currently lose_sum: 94.98581033945084
time_elpased: 1.846
batch start
#iterations: 315
currently lose_sum: 94.17633134126663
time_elpased: 1.833
batch start
#iterations: 316
currently lose_sum: 94.54028820991516
time_elpased: 1.833
batch start
#iterations: 317
currently lose_sum: 94.76200062036514
time_elpased: 1.871
batch start
#iterations: 318
currently lose_sum: 94.48020017147064
time_elpased: 1.854
batch start
#iterations: 319
currently lose_sum: 94.53441858291626
time_elpased: 1.825
start validation test
0.659278350515
0.657220929056
0.668313265411
0.662720685784
0.659262488329
61.129
batch start
#iterations: 320
currently lose_sum: 94.58634150028229
time_elpased: 1.833
batch start
#iterations: 321
currently lose_sum: 94.79921835660934
time_elpased: 1.828
batch start
#iterations: 322
currently lose_sum: 94.90042817592621
time_elpased: 1.86
batch start
#iterations: 323
currently lose_sum: 94.57207888364792
time_elpased: 1.788
batch start
#iterations: 324
currently lose_sum: 95.1059462428093
time_elpased: 1.807
batch start
#iterations: 325
currently lose_sum: 94.89173829555511
time_elpased: 1.82
batch start
#iterations: 326
currently lose_sum: 94.59445959329605
time_elpased: 1.971
batch start
#iterations: 327
currently lose_sum: 94.27873814105988
time_elpased: 1.857
batch start
#iterations: 328
currently lose_sum: 94.44162929058075
time_elpased: 1.801
batch start
#iterations: 329
currently lose_sum: 94.45497339963913
time_elpased: 1.806
batch start
#iterations: 330
currently lose_sum: 94.56989294290543
time_elpased: 1.803
batch start
#iterations: 331
currently lose_sum: 94.21999126672745
time_elpased: 1.819
batch start
#iterations: 332
currently lose_sum: 94.1387031674385
time_elpased: 1.84
batch start
#iterations: 333
currently lose_sum: 94.06824517250061
time_elpased: 1.845
batch start
#iterations: 334
currently lose_sum: 94.51359874010086
time_elpased: 1.843
batch start
#iterations: 335
currently lose_sum: 94.28400760889053
time_elpased: 1.871
batch start
#iterations: 336
currently lose_sum: 93.99503326416016
time_elpased: 1.862
batch start
#iterations: 337
currently lose_sum: 94.32465434074402
time_elpased: 1.813
batch start
#iterations: 338
currently lose_sum: 94.19698840379715
time_elpased: 1.891
batch start
#iterations: 339
currently lose_sum: 94.65098643302917
time_elpased: 1.863
start validation test
0.670721649485
0.645638288564
0.759390758465
0.697909770169
0.670565977187
60.751
batch start
#iterations: 340
currently lose_sum: 94.55527985095978
time_elpased: 1.824
batch start
#iterations: 341
currently lose_sum: 94.68429148197174
time_elpased: 1.838
batch start
#iterations: 342
currently lose_sum: 94.35130268335342
time_elpased: 1.85
batch start
#iterations: 343
currently lose_sum: 94.22025090456009
time_elpased: 1.795
batch start
#iterations: 344
currently lose_sum: 94.4781802892685
time_elpased: 1.835
batch start
#iterations: 345
currently lose_sum: 94.52865248918533
time_elpased: 1.807
batch start
#iterations: 346
currently lose_sum: 94.31117594242096
time_elpased: 1.814
batch start
#iterations: 347
currently lose_sum: 94.85053461790085
time_elpased: 1.832
batch start
#iterations: 348
currently lose_sum: 94.57655370235443
time_elpased: 1.828
batch start
#iterations: 349
currently lose_sum: 94.42199832201004
time_elpased: 1.801
batch start
#iterations: 350
currently lose_sum: 94.66711395978928
time_elpased: 1.831
batch start
#iterations: 351
currently lose_sum: 94.58923006057739
time_elpased: 1.879
batch start
#iterations: 352
currently lose_sum: 94.31258165836334
time_elpased: 1.813
batch start
#iterations: 353
currently lose_sum: 94.8661921620369
time_elpased: 1.834
batch start
#iterations: 354
currently lose_sum: 94.92653614282608
time_elpased: 1.795
batch start
#iterations: 355
currently lose_sum: 94.89954602718353
time_elpased: 1.828
batch start
#iterations: 356
currently lose_sum: 94.1737397313118
time_elpased: 1.804
batch start
#iterations: 357
currently lose_sum: 94.65195232629776
time_elpased: 1.888
batch start
#iterations: 358
currently lose_sum: 94.90706866979599
time_elpased: 1.852
batch start
#iterations: 359
currently lose_sum: 94.54835027456284
time_elpased: 1.851
start validation test
0.665979381443
0.656361704183
0.69918699187
0.677097867251
0.665921080361
60.846
batch start
#iterations: 360
currently lose_sum: 94.8272876739502
time_elpased: 1.879
batch start
#iterations: 361
currently lose_sum: 94.77696973085403
time_elpased: 1.88
batch start
#iterations: 362
currently lose_sum: 94.15634453296661
time_elpased: 1.868
batch start
#iterations: 363
currently lose_sum: 94.36119502782822
time_elpased: 1.835
batch start
#iterations: 364
currently lose_sum: 94.99609410762787
time_elpased: 1.87
batch start
#iterations: 365
currently lose_sum: 94.48656564950943
time_elpased: 1.822
batch start
#iterations: 366
currently lose_sum: 94.66657245159149
time_elpased: 1.828
batch start
#iterations: 367
currently lose_sum: 94.192538022995
time_elpased: 1.851
batch start
#iterations: 368
currently lose_sum: 94.51086330413818
time_elpased: 1.814
batch start
#iterations: 369
currently lose_sum: 94.3252072930336
time_elpased: 1.923
batch start
#iterations: 370
currently lose_sum: 94.60349428653717
time_elpased: 1.888
batch start
#iterations: 371
currently lose_sum: 94.13320815563202
time_elpased: 1.824
batch start
#iterations: 372
currently lose_sum: 94.64425337314606
time_elpased: 1.845
batch start
#iterations: 373
currently lose_sum: 94.58365833759308
time_elpased: 1.859
batch start
#iterations: 374
currently lose_sum: 94.30856841802597
time_elpased: 1.84
batch start
#iterations: 375
currently lose_sum: 94.73131042718887
time_elpased: 1.847
batch start
#iterations: 376
currently lose_sum: 94.09251528978348
time_elpased: 1.842
batch start
#iterations: 377
currently lose_sum: 94.16568696498871
time_elpased: 1.822
batch start
#iterations: 378
currently lose_sum: 94.89335972070694
time_elpased: 1.867
batch start
#iterations: 379
currently lose_sum: 94.17924755811691
time_elpased: 1.803
start validation test
0.668092783505
0.63018268467
0.816507152413
0.711346214193
0.667832219189
60.469
batch start
#iterations: 380
currently lose_sum: 94.63341689109802
time_elpased: 1.82
batch start
#iterations: 381
currently lose_sum: 94.22380024194717
time_elpased: 1.796
batch start
#iterations: 382
currently lose_sum: 94.4747086763382
time_elpased: 1.864
batch start
#iterations: 383
currently lose_sum: 94.22742718458176
time_elpased: 1.867
batch start
#iterations: 384
currently lose_sum: 94.25024384260178
time_elpased: 1.793
batch start
#iterations: 385
currently lose_sum: 94.48256969451904
time_elpased: 1.925
batch start
#iterations: 386
currently lose_sum: 94.48689484596252
time_elpased: 1.821
batch start
#iterations: 387
currently lose_sum: 94.12072825431824
time_elpased: 1.84
batch start
#iterations: 388
currently lose_sum: 94.4928126335144
time_elpased: 1.805
batch start
#iterations: 389
currently lose_sum: 94.54542231559753
time_elpased: 1.831
batch start
#iterations: 390
currently lose_sum: 94.36180520057678
time_elpased: 1.796
batch start
#iterations: 391
currently lose_sum: 94.56291508674622
time_elpased: 1.815
batch start
#iterations: 392
currently lose_sum: 94.17093259096146
time_elpased: 1.83
batch start
#iterations: 393
currently lose_sum: 94.53099572658539
time_elpased: 1.817
batch start
#iterations: 394
currently lose_sum: 93.99696385860443
time_elpased: 1.799
batch start
#iterations: 395
currently lose_sum: 94.81987410783768
time_elpased: 1.815
batch start
#iterations: 396
currently lose_sum: 94.22097820043564
time_elpased: 1.856
batch start
#iterations: 397
currently lose_sum: 94.14715254306793
time_elpased: 1.824
batch start
#iterations: 398
currently lose_sum: 94.10803979635239
time_elpased: 1.814
batch start
#iterations: 399
currently lose_sum: 94.56596457958221
time_elpased: 1.85
start validation test
0.666443298969
0.645717363979
0.740146135639
0.689714696715
0.666313902271
60.815
acc: 0.669
pre: 0.631
rec: 0.817
F1: 0.712
auc: 0.669
