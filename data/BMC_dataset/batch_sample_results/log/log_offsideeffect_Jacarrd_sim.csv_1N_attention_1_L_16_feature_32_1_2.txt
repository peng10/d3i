start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.78591334819794
time_elpased: 2.251
batch start
#iterations: 1
currently lose_sum: 100.01358115673065
time_elpased: 2.013
batch start
#iterations: 2
currently lose_sum: 99.88040274381638
time_elpased: 2.11
batch start
#iterations: 3
currently lose_sum: 99.61695098876953
time_elpased: 2.189
batch start
#iterations: 4
currently lose_sum: 99.25216996669769
time_elpased: 2.132
batch start
#iterations: 5
currently lose_sum: 99.09235417842865
time_elpased: 2.112
batch start
#iterations: 6
currently lose_sum: 98.75960618257523
time_elpased: 2.197
batch start
#iterations: 7
currently lose_sum: 98.73752158880234
time_elpased: 2.182
batch start
#iterations: 8
currently lose_sum: 98.77993988990784
time_elpased: 2.128
batch start
#iterations: 9
currently lose_sum: 98.52493190765381
time_elpased: 2.2
batch start
#iterations: 10
currently lose_sum: 98.09499698877335
time_elpased: 2.178
batch start
#iterations: 11
currently lose_sum: 98.30115067958832
time_elpased: 2.213
batch start
#iterations: 12
currently lose_sum: 97.88986325263977
time_elpased: 2.272
batch start
#iterations: 13
currently lose_sum: 98.12756317853928
time_elpased: 2.145
batch start
#iterations: 14
currently lose_sum: 97.40534257888794
time_elpased: 2.358
batch start
#iterations: 15
currently lose_sum: 97.73056411743164
time_elpased: 2.287
batch start
#iterations: 16
currently lose_sum: 97.1816440820694
time_elpased: 2.211
batch start
#iterations: 17
currently lose_sum: 97.45981699228287
time_elpased: 2.363
batch start
#iterations: 18
currently lose_sum: 97.24655055999756
time_elpased: 2.313
batch start
#iterations: 19
currently lose_sum: 96.9746522307396
time_elpased: 2.226
start validation test
0.652010309278
0.631916029176
0.731089842544
0.677894937736
0.651871472961
62.438
batch start
#iterations: 20
currently lose_sum: 97.11555284261703
time_elpased: 2.303
batch start
#iterations: 21
currently lose_sum: 96.80424731969833
time_elpased: 2.199
batch start
#iterations: 22
currently lose_sum: 97.16643744707108
time_elpased: 2.243
batch start
#iterations: 23
currently lose_sum: 96.72953307628632
time_elpased: 2.251
batch start
#iterations: 24
currently lose_sum: 96.67565089464188
time_elpased: 2.108
batch start
#iterations: 25
currently lose_sum: 96.49288189411163
time_elpased: 2.164
batch start
#iterations: 26
currently lose_sum: 96.67353039979935
time_elpased: 2.157
batch start
#iterations: 27
currently lose_sum: 96.60423624515533
time_elpased: 2.212
batch start
#iterations: 28
currently lose_sum: 96.38886404037476
time_elpased: 2.235
batch start
#iterations: 29
currently lose_sum: 96.71491819620132
time_elpased: 2.248
batch start
#iterations: 30
currently lose_sum: 96.62922477722168
time_elpased: 2.261
batch start
#iterations: 31
currently lose_sum: 96.25747746229172
time_elpased: 2.215
batch start
#iterations: 32
currently lose_sum: 96.21486586332321
time_elpased: 2.256
batch start
#iterations: 33
currently lose_sum: 96.48040801286697
time_elpased: 2.277
batch start
#iterations: 34
currently lose_sum: 96.37935817241669
time_elpased: 2.246
batch start
#iterations: 35
currently lose_sum: 96.17437773942947
time_elpased: 2.226
batch start
#iterations: 36
currently lose_sum: 96.38737004995346
time_elpased: 2.097
batch start
#iterations: 37
currently lose_sum: 96.21263384819031
time_elpased: 2.117
batch start
#iterations: 38
currently lose_sum: 96.13302665948868
time_elpased: 2.166
batch start
#iterations: 39
currently lose_sum: 96.15323162078857
time_elpased: 2.254
start validation test
0.659690721649
0.637019442245
0.745188844294
0.686871561374
0.659540616508
61.284
batch start
#iterations: 40
currently lose_sum: 96.12963163852692
time_elpased: 2.246
batch start
#iterations: 41
currently lose_sum: 96.07178741693497
time_elpased: 2.239
batch start
#iterations: 42
currently lose_sum: 95.9986497759819
time_elpased: 2.214
batch start
#iterations: 43
currently lose_sum: 95.99332344532013
time_elpased: 2.241
batch start
#iterations: 44
currently lose_sum: 96.28809994459152
time_elpased: 2.292
batch start
#iterations: 45
currently lose_sum: 96.1714591383934
time_elpased: 2.229
batch start
#iterations: 46
currently lose_sum: 95.83425003290176
time_elpased: 2.181
batch start
#iterations: 47
currently lose_sum: 96.14027488231659
time_elpased: 2.112
batch start
#iterations: 48
currently lose_sum: 96.25708419084549
time_elpased: 2.136
batch start
#iterations: 49
currently lose_sum: 96.1477438211441
time_elpased: 2.214
batch start
#iterations: 50
currently lose_sum: 95.84543865919113
time_elpased: 2.249
batch start
#iterations: 51
currently lose_sum: 95.8928307890892
time_elpased: 2.218
batch start
#iterations: 52
currently lose_sum: 95.75910931825638
time_elpased: 2.251
batch start
#iterations: 53
currently lose_sum: 95.5115676522255
time_elpased: 2.282
batch start
#iterations: 54
currently lose_sum: 96.02198451757431
time_elpased: 2.243
batch start
#iterations: 55
currently lose_sum: 95.98047077655792
time_elpased: 2.243
batch start
#iterations: 56
currently lose_sum: 96.03921568393707
time_elpased: 2.263
batch start
#iterations: 57
currently lose_sum: 95.68688410520554
time_elpased: 2.27
batch start
#iterations: 58
currently lose_sum: 96.00719511508942
time_elpased: 2.173
batch start
#iterations: 59
currently lose_sum: 95.92985606193542
time_elpased: 2.099
start validation test
0.664484536082
0.644089112469
0.737882062365
0.687802772315
0.664355675404
61.002
batch start
#iterations: 60
currently lose_sum: 95.79135143756866
time_elpased: 2.126
batch start
#iterations: 61
currently lose_sum: 96.00057911872864
time_elpased: 2.252
batch start
#iterations: 62
currently lose_sum: 95.95777994394302
time_elpased: 2.221
batch start
#iterations: 63
currently lose_sum: 95.82375574111938
time_elpased: 2.226
batch start
#iterations: 64
currently lose_sum: 95.6822247505188
time_elpased: 2.225
batch start
#iterations: 65
currently lose_sum: 95.97943872213364
time_elpased: 2.246
batch start
#iterations: 66
currently lose_sum: 95.49702227115631
time_elpased: 2.264
batch start
#iterations: 67
currently lose_sum: 95.62322109937668
time_elpased: 2.262
batch start
#iterations: 68
currently lose_sum: 95.70721572637558
time_elpased: 2.251
batch start
#iterations: 69
currently lose_sum: 95.73252999782562
time_elpased: 2.224
batch start
#iterations: 70
currently lose_sum: 95.55147939920425
time_elpased: 2.116
batch start
#iterations: 71
currently lose_sum: 95.70066088438034
time_elpased: 2.08
batch start
#iterations: 72
currently lose_sum: 95.57533168792725
time_elpased: 2.226
batch start
#iterations: 73
currently lose_sum: 95.93432611227036
time_elpased: 2.246
batch start
#iterations: 74
currently lose_sum: 95.70763325691223
time_elpased: 2.231
batch start
#iterations: 75
currently lose_sum: 95.65401911735535
time_elpased: 2.276
batch start
#iterations: 76
currently lose_sum: 95.77861875295639
time_elpased: 2.258
batch start
#iterations: 77
currently lose_sum: 95.73261845111847
time_elpased: 2.239
batch start
#iterations: 78
currently lose_sum: 95.39381211996078
time_elpased: 2.254
batch start
#iterations: 79
currently lose_sum: 96.31738698482513
time_elpased: 2.261
start validation test
0.667164948454
0.639268626111
0.769990737882
0.698566826946
0.66698442192
60.935
batch start
#iterations: 80
currently lose_sum: 95.68235260248184
time_elpased: 2.315
batch start
#iterations: 81
currently lose_sum: 95.75490313768387
time_elpased: 2.146
batch start
#iterations: 82
currently lose_sum: 95.92260199785233
time_elpased: 2.136
batch start
#iterations: 83
currently lose_sum: 95.4029039144516
time_elpased: 2.17
batch start
#iterations: 84
currently lose_sum: 95.46581870317459
time_elpased: 2.217
batch start
#iterations: 85
currently lose_sum: 95.64197570085526
time_elpased: 2.231
batch start
#iterations: 86
currently lose_sum: 95.78517979383469
time_elpased: 2.219
batch start
#iterations: 87
currently lose_sum: 95.59981793165207
time_elpased: 2.271
batch start
#iterations: 88
currently lose_sum: 95.66296499967575
time_elpased: 2.302
batch start
#iterations: 89
currently lose_sum: 95.49421817064285
time_elpased: 2.224
batch start
#iterations: 90
currently lose_sum: 95.83386343717575
time_elpased: 2.269
batch start
#iterations: 91
currently lose_sum: 95.63847976922989
time_elpased: 2.247
batch start
#iterations: 92
currently lose_sum: 95.64836132526398
time_elpased: 2.227
batch start
#iterations: 93
currently lose_sum: 95.55930787324905
time_elpased: 2.136
batch start
#iterations: 94
currently lose_sum: 95.5512672662735
time_elpased: 2.125
batch start
#iterations: 95
currently lose_sum: 95.70907539129257
time_elpased: 2.211
batch start
#iterations: 96
currently lose_sum: 95.22094696760178
time_elpased: 2.233
batch start
#iterations: 97
currently lose_sum: 95.43273907899857
time_elpased: 2.26
batch start
#iterations: 98
currently lose_sum: 95.55756741762161
time_elpased: 2.259
batch start
#iterations: 99
currently lose_sum: 95.60469043254852
time_elpased: 2.302
start validation test
0.661855670103
0.664787556112
0.655346300298
0.660033167496
0.661867098306
61.371
batch start
#iterations: 100
currently lose_sum: 95.46533942222595
time_elpased: 2.224
batch start
#iterations: 101
currently lose_sum: 95.41414362192154
time_elpased: 2.285
batch start
#iterations: 102
currently lose_sum: 95.79225242137909
time_elpased: 2.232
batch start
#iterations: 103
currently lose_sum: 95.33835023641586
time_elpased: 2.215
batch start
#iterations: 104
currently lose_sum: 95.64365994930267
time_elpased: 2.096
batch start
#iterations: 105
currently lose_sum: 95.20609974861145
time_elpased: 2.072
batch start
#iterations: 106
currently lose_sum: 95.20475363731384
time_elpased: 2.19
batch start
#iterations: 107
currently lose_sum: 95.58574718236923
time_elpased: 2.232
batch start
#iterations: 108
currently lose_sum: 95.53219938278198
time_elpased: 2.262
batch start
#iterations: 109
currently lose_sum: 95.30547547340393
time_elpased: 2.233
batch start
#iterations: 110
currently lose_sum: 95.42667245864868
time_elpased: 2.245
batch start
#iterations: 111
currently lose_sum: 94.95590102672577
time_elpased: 2.274
batch start
#iterations: 112
currently lose_sum: 95.58645951747894
time_elpased: 2.234
batch start
#iterations: 113
currently lose_sum: 94.94720083475113
time_elpased: 2.257
batch start
#iterations: 114
currently lose_sum: 95.39675348997116
time_elpased: 2.302
batch start
#iterations: 115
currently lose_sum: 95.17560237646103
time_elpased: 2.175
batch start
#iterations: 116
currently lose_sum: 95.13271123170853
time_elpased: 2.149
batch start
#iterations: 117
currently lose_sum: 94.86098980903625
time_elpased: 2.091
batch start
#iterations: 118
currently lose_sum: 95.45748627185822
time_elpased: 2.26
batch start
#iterations: 119
currently lose_sum: 95.07784408330917
time_elpased: 2.238
start validation test
0.668917525773
0.632098171319
0.811052794072
0.710480054091
0.668667985387
60.625
batch start
#iterations: 120
currently lose_sum: 95.23407572507858
time_elpased: 2.296
batch start
#iterations: 121
currently lose_sum: 94.87131398916245
time_elpased: 2.283
batch start
#iterations: 122
currently lose_sum: 95.34379488229752
time_elpased: 2.281
batch start
#iterations: 123
currently lose_sum: 95.3376111984253
time_elpased: 2.254
batch start
#iterations: 124
currently lose_sum: 95.60438424348831
time_elpased: 2.232
batch start
#iterations: 125
currently lose_sum: 95.27775889635086
time_elpased: 2.283
batch start
#iterations: 126
currently lose_sum: 95.27860128879547
time_elpased: 2.221
batch start
#iterations: 127
currently lose_sum: 95.55152422189713
time_elpased: 2.217
batch start
#iterations: 128
currently lose_sum: 95.35634142160416
time_elpased: 2.048
batch start
#iterations: 129
currently lose_sum: 95.38727432489395
time_elpased: 2.225
batch start
#iterations: 130
currently lose_sum: 95.10765570402145
time_elpased: 2.282
batch start
#iterations: 131
currently lose_sum: 95.59934824705124
time_elpased: 2.283
batch start
#iterations: 132
currently lose_sum: 95.41769504547119
time_elpased: 2.29
batch start
#iterations: 133
currently lose_sum: 95.09513139724731
time_elpased: 2.245
batch start
#iterations: 134
currently lose_sum: 95.09397149085999
time_elpased: 2.25
batch start
#iterations: 135
currently lose_sum: 95.09254693984985
time_elpased: 2.303
batch start
#iterations: 136
currently lose_sum: 95.16824126243591
time_elpased: 2.293
batch start
#iterations: 137
currently lose_sum: 95.30417311191559
time_elpased: 2.22
batch start
#iterations: 138
currently lose_sum: 95.29352295398712
time_elpased: 2.205
batch start
#iterations: 139
currently lose_sum: 95.04350507259369
time_elpased: 2.125
start validation test
0.668402061856
0.637428858386
0.783781002367
0.703069466882
0.66819949633
60.702
batch start
#iterations: 140
currently lose_sum: 94.97872310876846
time_elpased: 2.191
batch start
#iterations: 141
currently lose_sum: 95.49586397409439
time_elpased: 2.275
batch start
#iterations: 142
currently lose_sum: 95.07496428489685
time_elpased: 2.247
batch start
#iterations: 143
currently lose_sum: 94.99558186531067
time_elpased: 2.258
batch start
#iterations: 144
currently lose_sum: 95.33462035655975
time_elpased: 2.236
batch start
#iterations: 145
currently lose_sum: 95.22225093841553
time_elpased: 2.27
batch start
#iterations: 146
currently lose_sum: 95.22997134923935
time_elpased: 2.253
batch start
#iterations: 147
currently lose_sum: 95.17584490776062
time_elpased: 2.256
batch start
#iterations: 148
currently lose_sum: 95.02663785219193
time_elpased: 2.256
batch start
#iterations: 149
currently lose_sum: 95.36391973495483
time_elpased: 2.197
batch start
#iterations: 150
currently lose_sum: 95.22275853157043
time_elpased: 2.199
batch start
#iterations: 151
currently lose_sum: 95.07118827104568
time_elpased: 2.065
batch start
#iterations: 152
currently lose_sum: 95.20296621322632
time_elpased: 2.235
batch start
#iterations: 153
currently lose_sum: 95.40371173620224
time_elpased: 2.24
batch start
#iterations: 154
currently lose_sum: 95.32831585407257
time_elpased: 2.261
batch start
#iterations: 155
currently lose_sum: 95.10594946146011
time_elpased: 2.269
batch start
#iterations: 156
currently lose_sum: 95.2121741771698
time_elpased: 2.257
batch start
#iterations: 157
currently lose_sum: 94.73378491401672
time_elpased: 2.237
batch start
#iterations: 158
currently lose_sum: 94.81187516450882
time_elpased: 2.282
batch start
#iterations: 159
currently lose_sum: 95.05905991792679
time_elpased: 2.239
start validation test
0.650515463918
0.665502084977
0.60769784913
0.635287789134
0.650590636844
61.637
batch start
#iterations: 160
currently lose_sum: 94.88257628679276
time_elpased: 2.208
batch start
#iterations: 161
currently lose_sum: 95.1783886551857
time_elpased: 2.287
batch start
#iterations: 162
currently lose_sum: 95.53645157814026
time_elpased: 2.124
batch start
#iterations: 163
currently lose_sum: 95.2237902879715
time_elpased: 2.189
batch start
#iterations: 164
currently lose_sum: 95.23403072357178
time_elpased: 2.248
batch start
#iterations: 165
currently lose_sum: 94.87267285585403
time_elpased: 2.281
batch start
#iterations: 166
currently lose_sum: 95.11989849805832
time_elpased: 2.266
batch start
#iterations: 167
currently lose_sum: 94.97860819101334
time_elpased: 2.217
batch start
#iterations: 168
currently lose_sum: 95.1265863776207
time_elpased: 2.238
batch start
#iterations: 169
currently lose_sum: 95.31130957603455
time_elpased: 2.315
batch start
#iterations: 170
currently lose_sum: 94.92085337638855
time_elpased: 2.29
batch start
#iterations: 171
currently lose_sum: 95.35851716995239
time_elpased: 2.172
batch start
#iterations: 172
currently lose_sum: 95.47400689125061
time_elpased: 2.229
batch start
#iterations: 173
currently lose_sum: 95.08650434017181
time_elpased: 2.399
batch start
#iterations: 174
currently lose_sum: 94.69453883171082
time_elpased: 2.132
batch start
#iterations: 175
currently lose_sum: 94.74743115901947
time_elpased: 2.272
batch start
#iterations: 176
currently lose_sum: 94.869857609272
time_elpased: 2.291
batch start
#iterations: 177
currently lose_sum: 95.22062575817108
time_elpased: 2.288
batch start
#iterations: 178
currently lose_sum: 95.15484791994095
time_elpased: 2.244
batch start
#iterations: 179
currently lose_sum: 94.68412816524506
time_elpased: 2.287
start validation test
0.669793814433
0.6481696948
0.745291756715
0.693346098612
0.669661266151
60.407
batch start
#iterations: 180
currently lose_sum: 94.64435595273972
time_elpased: 2.318
batch start
#iterations: 181
currently lose_sum: 94.84112989902496
time_elpased: 2.25
batch start
#iterations: 182
currently lose_sum: 94.79007744789124
time_elpased: 2.169
batch start
#iterations: 183
currently lose_sum: 94.81025493144989
time_elpased: 2.229
batch start
#iterations: 184
currently lose_sum: 95.2758395075798
time_elpased: 2.159
batch start
#iterations: 185
currently lose_sum: 95.03915518522263
time_elpased: 2.11
batch start
#iterations: 186
currently lose_sum: 94.68219381570816
time_elpased: 2.25
batch start
#iterations: 187
currently lose_sum: 95.17697894573212
time_elpased: 2.271
batch start
#iterations: 188
currently lose_sum: 95.28530871868134
time_elpased: 2.227
batch start
#iterations: 189
currently lose_sum: 94.92493963241577
time_elpased: 2.221
batch start
#iterations: 190
currently lose_sum: 95.2650756239891
time_elpased: 2.218
batch start
#iterations: 191
currently lose_sum: 95.08807635307312
time_elpased: 2.248
batch start
#iterations: 192
currently lose_sum: 94.78793185949326
time_elpased: 2.24
batch start
#iterations: 193
currently lose_sum: 95.23883581161499
time_elpased: 2.158
batch start
#iterations: 194
currently lose_sum: 94.81752413511276
time_elpased: 2.285
batch start
#iterations: 195
currently lose_sum: 94.64345222711563
time_elpased: 2.283
batch start
#iterations: 196
currently lose_sum: 95.15626728534698
time_elpased: 2.067
batch start
#iterations: 197
currently lose_sum: 95.01099753379822
time_elpased: 2.223
batch start
#iterations: 198
currently lose_sum: 95.07533067464828
time_elpased: 2.244
batch start
#iterations: 199
currently lose_sum: 94.84026271104813
time_elpased: 2.293
start validation test
0.662989690722
0.657049698646
0.68436760317
0.670430486944
0.662952158499
60.945
batch start
#iterations: 200
currently lose_sum: 94.8987489938736
time_elpased: 2.25
batch start
#iterations: 201
currently lose_sum: 94.91909378767014
time_elpased: 2.303
batch start
#iterations: 202
currently lose_sum: 94.86583399772644
time_elpased: 2.23
batch start
#iterations: 203
currently lose_sum: 94.77476507425308
time_elpased: 2.276
batch start
#iterations: 204
currently lose_sum: 94.97583359479904
time_elpased: 2.167
batch start
#iterations: 205
currently lose_sum: 94.84331411123276
time_elpased: 2.263
batch start
#iterations: 206
currently lose_sum: 94.62987864017487
time_elpased: 2.235
batch start
#iterations: 207
currently lose_sum: 94.93984305858612
time_elpased: 2.12
batch start
#iterations: 208
currently lose_sum: 95.18074977397919
time_elpased: 2.126
batch start
#iterations: 209
currently lose_sum: 95.02784788608551
time_elpased: 2.261
batch start
#iterations: 210
currently lose_sum: 94.69738584756851
time_elpased: 2.229
batch start
#iterations: 211
currently lose_sum: 94.96566885709763
time_elpased: 2.235
batch start
#iterations: 212
currently lose_sum: 94.93693971633911
time_elpased: 2.211
batch start
#iterations: 213
currently lose_sum: 94.63805556297302
time_elpased: 2.264
batch start
#iterations: 214
currently lose_sum: 94.82508915662766
time_elpased: 2.262
batch start
#iterations: 215
currently lose_sum: 94.81927835941315
time_elpased: 2.154
batch start
#iterations: 216
currently lose_sum: 94.72170931100845
time_elpased: 2.255
batch start
#iterations: 217
currently lose_sum: 94.99026012420654
time_elpased: 2.224
batch start
#iterations: 218
currently lose_sum: 94.59292417764664
time_elpased: 2.199
batch start
#iterations: 219
currently lose_sum: 94.91294836997986
time_elpased: 2.106
start validation test
0.662731958763
0.647847959754
0.715652979315
0.680064544521
0.662639047749
60.886
batch start
#iterations: 220
currently lose_sum: 95.00295215845108
time_elpased: 2.279
batch start
#iterations: 221
currently lose_sum: 94.97525125741959
time_elpased: 2.294
batch start
#iterations: 222
currently lose_sum: 95.06637650728226
time_elpased: 2.256
batch start
#iterations: 223
currently lose_sum: 94.65960258245468
time_elpased: 2.268
batch start
#iterations: 224
currently lose_sum: 94.64585983753204
time_elpased: 2.259
batch start
#iterations: 225
currently lose_sum: 94.45055204629898
time_elpased: 2.21
batch start
#iterations: 226
currently lose_sum: 94.93836051225662
time_elpased: 2.178
batch start
#iterations: 227
currently lose_sum: 94.73439568281174
time_elpased: 2.227
batch start
#iterations: 228
currently lose_sum: 94.93962931632996
time_elpased: 2.255
batch start
#iterations: 229
currently lose_sum: 94.79244184494019
time_elpased: 2.214
batch start
#iterations: 230
currently lose_sum: 94.80264854431152
time_elpased: 2.054
batch start
#iterations: 231
currently lose_sum: 94.65663814544678
time_elpased: 2.2
batch start
#iterations: 232
currently lose_sum: 94.54159766435623
time_elpased: 2.241
batch start
#iterations: 233
currently lose_sum: 95.1180072426796
time_elpased: 2.225
batch start
#iterations: 234
currently lose_sum: 94.72256553173065
time_elpased: 2.225
batch start
#iterations: 235
currently lose_sum: 94.84647685289383
time_elpased: 2.239
batch start
#iterations: 236
currently lose_sum: 94.72433453798294
time_elpased: 2.199
batch start
#iterations: 237
currently lose_sum: 95.01240521669388
time_elpased: 2.16
batch start
#iterations: 238
currently lose_sum: 95.05406910181046
time_elpased: 2.233
batch start
#iterations: 239
currently lose_sum: 95.04784071445465
time_elpased: 2.258
start validation test
0.66618556701
0.625125472936
0.833178964701
0.714310922887
0.665892384344
60.530
batch start
#iterations: 240
currently lose_sum: 94.52790957689285
time_elpased: 2.26
batch start
#iterations: 241
currently lose_sum: 94.90813875198364
time_elpased: 2.137
batch start
#iterations: 242
currently lose_sum: 94.85645085573196
time_elpased: 2.184
batch start
#iterations: 243
currently lose_sum: 94.58260208368301
time_elpased: 2.294
batch start
#iterations: 244
currently lose_sum: 95.0752632021904
time_elpased: 2.273
batch start
#iterations: 245
currently lose_sum: 94.8905701637268
time_elpased: 2.268
batch start
#iterations: 246
currently lose_sum: 95.03273570537567
time_elpased: 2.253
batch start
#iterations: 247
currently lose_sum: 94.48780333995819
time_elpased: 2.251
batch start
#iterations: 248
currently lose_sum: 94.7664304971695
time_elpased: 2.156
batch start
#iterations: 249
currently lose_sum: 94.59352415800095
time_elpased: 2.259
batch start
#iterations: 250
currently lose_sum: 94.57312947511673
time_elpased: 2.222
batch start
#iterations: 251
currently lose_sum: 94.63008588552475
time_elpased: 2.269
batch start
#iterations: 252
currently lose_sum: 94.70653265714645
time_elpased: 2.253
batch start
#iterations: 253
currently lose_sum: 94.85123664140701
time_elpased: 2.112
batch start
#iterations: 254
currently lose_sum: 94.59443640708923
time_elpased: 2.215
batch start
#iterations: 255
currently lose_sum: 94.65894550085068
time_elpased: 2.253
batch start
#iterations: 256
currently lose_sum: 94.83316779136658
time_elpased: 2.318
batch start
#iterations: 257
currently lose_sum: 94.46395760774612
time_elpased: 2.17
batch start
#iterations: 258
currently lose_sum: 94.71317625045776
time_elpased: 2.171
batch start
#iterations: 259
currently lose_sum: 94.81387156248093
time_elpased: 2.178
start validation test
0.666804123711
0.640300181144
0.763918905012
0.696668230878
0.666633623734
60.489
batch start
#iterations: 260
currently lose_sum: 94.44479554891586
time_elpased: 2.211
batch start
#iterations: 261
currently lose_sum: 94.36677527427673
time_elpased: 2.245
batch start
#iterations: 262
currently lose_sum: 94.36972892284393
time_elpased: 2.243
batch start
#iterations: 263
currently lose_sum: 94.86111795902252
time_elpased: 2.223
batch start
#iterations: 264
currently lose_sum: 94.97390526533127
time_elpased: 2.071
batch start
#iterations: 265
currently lose_sum: 94.58475583791733
time_elpased: 2.249
batch start
#iterations: 266
currently lose_sum: 94.44532179832458
time_elpased: 2.232
batch start
#iterations: 267
currently lose_sum: 94.81469398736954
time_elpased: 2.265
batch start
#iterations: 268
currently lose_sum: 94.49545961618423
time_elpased: 2.264
batch start
#iterations: 269
currently lose_sum: 94.90844374895096
time_elpased: 2.236
batch start
#iterations: 270
currently lose_sum: 94.62754511833191
time_elpased: 2.235
batch start
#iterations: 271
currently lose_sum: 94.57863211631775
time_elpased: 2.297
batch start
#iterations: 272
currently lose_sum: 94.49805051088333
time_elpased: 2.231
batch start
#iterations: 273
currently lose_sum: 94.34608602523804
time_elpased: 2.282
batch start
#iterations: 274
currently lose_sum: 94.6679230928421
time_elpased: 2.217
batch start
#iterations: 275
currently lose_sum: 94.74867349863052
time_elpased: 2.194
batch start
#iterations: 276
currently lose_sum: 94.52602481842041
time_elpased: 2.114
batch start
#iterations: 277
currently lose_sum: 94.77183473110199
time_elpased: 2.294
batch start
#iterations: 278
currently lose_sum: 94.20420742034912
time_elpased: 2.273
batch start
#iterations: 279
currently lose_sum: 94.7874431014061
time_elpased: 2.283
start validation test
0.668556701031
0.636855691565
0.787074199856
0.70404124091
0.668348625282
60.345
batch start
#iterations: 280
currently lose_sum: 94.68088507652283
time_elpased: 2.196
batch start
#iterations: 281
currently lose_sum: 94.2869284749031
time_elpased: 2.29
batch start
#iterations: 282
currently lose_sum: 94.54193967580795
time_elpased: 2.293
batch start
#iterations: 283
currently lose_sum: 94.94136828184128
time_elpased: 2.254
batch start
#iterations: 284
currently lose_sum: 94.35315078496933
time_elpased: 2.211
batch start
#iterations: 285
currently lose_sum: 94.62388855218887
time_elpased: 2.248
batch start
#iterations: 286
currently lose_sum: 94.44180673360825
time_elpased: 2.204
batch start
#iterations: 287
currently lose_sum: 94.33362877368927
time_elpased: 2.074
batch start
#iterations: 288
currently lose_sum: 94.44059205055237
time_elpased: 2.22
batch start
#iterations: 289
currently lose_sum: 94.61524111032486
time_elpased: 2.258
batch start
#iterations: 290
currently lose_sum: 94.27377760410309
time_elpased: 2.247
batch start
#iterations: 291
currently lose_sum: 94.78389644622803
time_elpased: 2.181
batch start
#iterations: 292
currently lose_sum: 95.08188450336456
time_elpased: 2.248
batch start
#iterations: 293
currently lose_sum: 94.21373689174652
time_elpased: 2.291
batch start
#iterations: 294
currently lose_sum: 94.76561254262924
time_elpased: 2.217
batch start
#iterations: 295
currently lose_sum: 94.89228123426437
time_elpased: 2.259
batch start
#iterations: 296
currently lose_sum: 94.78335136175156
time_elpased: 2.235
batch start
#iterations: 297
currently lose_sum: 94.52160030603409
time_elpased: 2.235
batch start
#iterations: 298
currently lose_sum: 94.78461766242981
time_elpased: 2.154
batch start
#iterations: 299
currently lose_sum: 94.48388087749481
time_elpased: 2.138
start validation test
0.670463917526
0.640490278952
0.779767417927
0.703299763308
0.670272018372
60.185
batch start
#iterations: 300
currently lose_sum: 94.36803096532822
time_elpased: 2.313
batch start
#iterations: 301
currently lose_sum: 94.94608223438263
time_elpased: 2.243
batch start
#iterations: 302
currently lose_sum: 94.623750269413
time_elpased: 2.188
batch start
#iterations: 303
currently lose_sum: 94.49859237670898
time_elpased: 2.246
batch start
#iterations: 304
currently lose_sum: 94.25491732358932
time_elpased: 2.23
batch start
#iterations: 305
currently lose_sum: 94.38157427310944
time_elpased: 2.249
batch start
#iterations: 306
currently lose_sum: 94.57015562057495
time_elpased: 2.274
batch start
#iterations: 307
currently lose_sum: 94.77534240484238
time_elpased: 2.218
batch start
#iterations: 308
currently lose_sum: 94.32903927564621
time_elpased: 2.221
batch start
#iterations: 309
currently lose_sum: 94.12139785289764
time_elpased: 2.299
batch start
#iterations: 310
currently lose_sum: 94.77912366390228
time_elpased: 2.041
batch start
#iterations: 311
currently lose_sum: 94.62889713048935
time_elpased: 2.263
batch start
#iterations: 312
currently lose_sum: 94.60022193193436
time_elpased: 2.244
batch start
#iterations: 313
currently lose_sum: 94.26307106018066
time_elpased: 2.058
batch start
#iterations: 314
currently lose_sum: 94.89815676212311
time_elpased: 2.214
batch start
#iterations: 315
currently lose_sum: 94.57090508937836
time_elpased: 2.157
batch start
#iterations: 316
currently lose_sum: 94.2564668059349
time_elpased: 2.129
batch start
#iterations: 317
currently lose_sum: 94.43726170063019
time_elpased: 2.169
batch start
#iterations: 318
currently lose_sum: 94.48121672868729
time_elpased: 2.206
batch start
#iterations: 319
currently lose_sum: 94.74416214227676
time_elpased: 2.232
start validation test
0.665257731959
0.641347250241
0.752495626222
0.692489819112
0.66510457238
60.583
batch start
#iterations: 320
currently lose_sum: 94.4243049621582
time_elpased: 2.17
batch start
#iterations: 321
currently lose_sum: 94.49338585138321
time_elpased: 2.253
batch start
#iterations: 322
currently lose_sum: 94.83767080307007
time_elpased: 2.216
batch start
#iterations: 323
currently lose_sum: 94.39786875247955
time_elpased: 2.021
batch start
#iterations: 324
currently lose_sum: 94.83285933732986
time_elpased: 2.182
batch start
#iterations: 325
currently lose_sum: 94.37682473659515
time_elpased: 2.198
batch start
#iterations: 326
currently lose_sum: 94.45401555299759
time_elpased: 2.216
batch start
#iterations: 327
currently lose_sum: 94.41050148010254
time_elpased: 2.226
batch start
#iterations: 328
currently lose_sum: 94.50865370035172
time_elpased: 2.188
batch start
#iterations: 329
currently lose_sum: 94.50023901462555
time_elpased: 2.222
batch start
#iterations: 330
currently lose_sum: 94.6543060541153
time_elpased: 2.158
batch start
#iterations: 331
currently lose_sum: 94.64066791534424
time_elpased: 2.167
batch start
#iterations: 332
currently lose_sum: 94.23004430532455
time_elpased: 2.21
batch start
#iterations: 333
currently lose_sum: 94.36905860900879
time_elpased: 2.224
batch start
#iterations: 334
currently lose_sum: 94.07403880357742
time_elpased: 2.142
batch start
#iterations: 335
currently lose_sum: 94.18472558259964
time_elpased: 2.082
batch start
#iterations: 336
currently lose_sum: 94.14150774478912
time_elpased: 2.202
batch start
#iterations: 337
currently lose_sum: 94.80653244256973
time_elpased: 2.215
batch start
#iterations: 338
currently lose_sum: 94.22634661197662
time_elpased: 2.231
batch start
#iterations: 339
currently lose_sum: 94.53708332777023
time_elpased: 2.229
start validation test
0.669948453608
0.632666132906
0.813213954924
0.711667492232
0.669696928924
60.262
batch start
#iterations: 340
currently lose_sum: 94.96468901634216
time_elpased: 2.272
batch start
#iterations: 341
currently lose_sum: 94.58312290906906
time_elpased: 2.252
batch start
#iterations: 342
currently lose_sum: 94.78023809194565
time_elpased: 2.105
batch start
#iterations: 343
currently lose_sum: 94.67844837903976
time_elpased: 2.228
batch start
#iterations: 344
currently lose_sum: 94.04878747463226
time_elpased: 2.188
batch start
#iterations: 345
currently lose_sum: 94.69261741638184
time_elpased: 2.229
batch start
#iterations: 346
currently lose_sum: 94.30641090869904
time_elpased: 2.038
batch start
#iterations: 347
currently lose_sum: 94.38892501592636
time_elpased: 2.198
batch start
#iterations: 348
currently lose_sum: 94.84016299247742
time_elpased: 2.198
batch start
#iterations: 349
currently lose_sum: 94.73970657587051
time_elpased: 2.21
batch start
#iterations: 350
currently lose_sum: 94.76257610321045
time_elpased: 2.18
batch start
#iterations: 351
currently lose_sum: 94.27216124534607
time_elpased: 2.207
batch start
#iterations: 352
currently lose_sum: 94.81065082550049
time_elpased: 2.192
batch start
#iterations: 353
currently lose_sum: 94.18846505880356
time_elpased: 2.154
batch start
#iterations: 354
currently lose_sum: 94.46201246976852
time_elpased: 2.18
batch start
#iterations: 355
currently lose_sum: 94.26083999872208
time_elpased: 2.221
batch start
#iterations: 356
currently lose_sum: 94.72287195920944
time_elpased: 2.217
batch start
#iterations: 357
currently lose_sum: 94.78561168909073
time_elpased: 2.149
batch start
#iterations: 358
currently lose_sum: 94.56196093559265
time_elpased: 2.022
batch start
#iterations: 359
currently lose_sum: 94.33979904651642
time_elpased: 2.229
start validation test
0.670567010309
0.644810170672
0.762066481424
0.698551955096
0.670406368875
60.275
batch start
#iterations: 360
currently lose_sum: 94.66035723686218
time_elpased: 2.222
batch start
#iterations: 361
currently lose_sum: 94.86133754253387
time_elpased: 2.245
batch start
#iterations: 362
currently lose_sum: 94.48233944177628
time_elpased: 2.232
batch start
#iterations: 363
currently lose_sum: 94.23775380849838
time_elpased: 2.221
batch start
#iterations: 364
currently lose_sum: 94.39359533786774
time_elpased: 2.13
batch start
#iterations: 365
currently lose_sum: 94.58575528860092
time_elpased: 2.169
batch start
#iterations: 366
currently lose_sum: 95.00230818986893
time_elpased: 2.231
batch start
#iterations: 367
currently lose_sum: 94.38050645589828
time_elpased: 2.244
batch start
#iterations: 368
currently lose_sum: 94.75725561380386
time_elpased: 2.177
batch start
#iterations: 369
currently lose_sum: 94.73074144124985
time_elpased: 2.083
batch start
#iterations: 370
currently lose_sum: 94.32226818799973
time_elpased: 2.161
batch start
#iterations: 371
currently lose_sum: 94.53684931993484
time_elpased: 2.222
batch start
#iterations: 372
currently lose_sum: 94.37092661857605
time_elpased: 2.238
batch start
#iterations: 373
currently lose_sum: 94.29928380250931
time_elpased: 2.241
batch start
#iterations: 374
currently lose_sum: 94.67256718873978
time_elpased: 2.18
batch start
#iterations: 375
currently lose_sum: 94.41609209775925
time_elpased: 2.159
batch start
#iterations: 376
currently lose_sum: 94.35955613851547
time_elpased: 2.158
batch start
#iterations: 377
currently lose_sum: 94.81973564624786
time_elpased: 2.237
batch start
#iterations: 378
currently lose_sum: 94.61417293548584
time_elpased: 2.211
batch start
#iterations: 379
currently lose_sum: 94.61513537168503
time_elpased: 2.217
start validation test
0.670721649485
0.643726793887
0.767212102501
0.700065733872
0.670552245612
60.391
batch start
#iterations: 380
currently lose_sum: 94.53321850299835
time_elpased: 2.075
batch start
#iterations: 381
currently lose_sum: 94.54441636800766
time_elpased: 2.177
batch start
#iterations: 382
currently lose_sum: 94.37290251255035
time_elpased: 2.208
batch start
#iterations: 383
currently lose_sum: 94.75293797254562
time_elpased: 2.212
batch start
#iterations: 384
currently lose_sum: 94.4373123049736
time_elpased: 2.206
batch start
#iterations: 385
currently lose_sum: 94.79856914281845
time_elpased: 2.231
batch start
#iterations: 386
currently lose_sum: 94.80096709728241
time_elpased: 2.115
batch start
#iterations: 387
currently lose_sum: 94.77546715736389
time_elpased: 2.204
batch start
#iterations: 388
currently lose_sum: 94.80693125724792
time_elpased: 2.198
batch start
#iterations: 389
currently lose_sum: 94.30742806196213
time_elpased: 2.181
batch start
#iterations: 390
currently lose_sum: 94.50333648920059
time_elpased: 2.248
batch start
#iterations: 391
currently lose_sum: 94.53949946165085
time_elpased: 2.172
batch start
#iterations: 392
currently lose_sum: 94.70250219106674
time_elpased: 2.066
batch start
#iterations: 393
currently lose_sum: 94.37382936477661
time_elpased: 2.167
batch start
#iterations: 394
currently lose_sum: 94.68453830480576
time_elpased: 2.21
batch start
#iterations: 395
currently lose_sum: 94.15824836492538
time_elpased: 2.245
batch start
#iterations: 396
currently lose_sum: 94.07969254255295
time_elpased: 2.2
batch start
#iterations: 397
currently lose_sum: 94.41095101833344
time_elpased: 2.19
batch start
#iterations: 398
currently lose_sum: 94.45756042003632
time_elpased: 2.178
batch start
#iterations: 399
currently lose_sum: 94.40430146455765
time_elpased: 2.269
start validation test
0.666134020619
0.650278293135
0.72141607492
0.684002536957
0.666036964446
60.466
acc: 0.667
pre: 0.638
rec: 0.775
F1: 0.700
auc: 0.667
