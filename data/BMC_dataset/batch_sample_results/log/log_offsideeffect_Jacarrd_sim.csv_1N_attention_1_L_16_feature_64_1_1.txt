start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.32191455364227
time_elpased: 1.953
batch start
#iterations: 1
currently lose_sum: 99.75361281633377
time_elpased: 1.829
batch start
#iterations: 2
currently lose_sum: 99.77202677726746
time_elpased: 1.836
batch start
#iterations: 3
currently lose_sum: 99.44298654794693
time_elpased: 1.831
batch start
#iterations: 4
currently lose_sum: 99.35756182670593
time_elpased: 1.821
batch start
#iterations: 5
currently lose_sum: 98.94893616437912
time_elpased: 1.836
batch start
#iterations: 6
currently lose_sum: 98.62862211465836
time_elpased: 1.829
batch start
#iterations: 7
currently lose_sum: 98.5323960185051
time_elpased: 1.85
batch start
#iterations: 8
currently lose_sum: 98.46802777051926
time_elpased: 1.898
batch start
#iterations: 9
currently lose_sum: 98.07877510786057
time_elpased: 1.855
batch start
#iterations: 10
currently lose_sum: 97.83692359924316
time_elpased: 1.902
batch start
#iterations: 11
currently lose_sum: 97.855364382267
time_elpased: 1.838
batch start
#iterations: 12
currently lose_sum: 97.70855391025543
time_elpased: 1.851
batch start
#iterations: 13
currently lose_sum: 97.6012914776802
time_elpased: 1.856
batch start
#iterations: 14
currently lose_sum: 97.29936534166336
time_elpased: 1.883
batch start
#iterations: 15
currently lose_sum: 97.39596003293991
time_elpased: 1.867
batch start
#iterations: 16
currently lose_sum: 97.38957065343857
time_elpased: 1.874
batch start
#iterations: 17
currently lose_sum: 97.0622085928917
time_elpased: 1.873
batch start
#iterations: 18
currently lose_sum: 97.07804173231125
time_elpased: 1.875
batch start
#iterations: 19
currently lose_sum: 97.19378620386124
time_elpased: 1.862
start validation test
0.653556701031
0.644566685968
0.687352063394
0.665272174909
0.65349736806
62.427
batch start
#iterations: 20
currently lose_sum: 97.04063701629639
time_elpased: 1.907
batch start
#iterations: 21
currently lose_sum: 96.79145133495331
time_elpased: 1.859
batch start
#iterations: 22
currently lose_sum: 96.80285555124283
time_elpased: 1.868
batch start
#iterations: 23
currently lose_sum: 97.17555058002472
time_elpased: 1.838
batch start
#iterations: 24
currently lose_sum: 96.90155392885208
time_elpased: 1.85
batch start
#iterations: 25
currently lose_sum: 96.63379591703415
time_elpased: 1.856
batch start
#iterations: 26
currently lose_sum: 96.57674515247345
time_elpased: 1.837
batch start
#iterations: 27
currently lose_sum: 96.58669978380203
time_elpased: 1.825
batch start
#iterations: 28
currently lose_sum: 96.39937084913254
time_elpased: 1.867
batch start
#iterations: 29
currently lose_sum: 96.91815310716629
time_elpased: 1.866
batch start
#iterations: 30
currently lose_sum: 96.3400307893753
time_elpased: 1.888
batch start
#iterations: 31
currently lose_sum: 96.02598142623901
time_elpased: 1.834
batch start
#iterations: 32
currently lose_sum: 96.29666662216187
time_elpased: 1.846
batch start
#iterations: 33
currently lose_sum: 96.40067088603973
time_elpased: 1.87
batch start
#iterations: 34
currently lose_sum: 96.14439743757248
time_elpased: 1.82
batch start
#iterations: 35
currently lose_sum: 96.48849618434906
time_elpased: 1.84
batch start
#iterations: 36
currently lose_sum: 96.51786786317825
time_elpased: 1.82
batch start
#iterations: 37
currently lose_sum: 96.29945969581604
time_elpased: 1.823
batch start
#iterations: 38
currently lose_sum: 95.87046331167221
time_elpased: 1.819
batch start
#iterations: 39
currently lose_sum: 96.2735242843628
time_elpased: 1.883
start validation test
0.654587628866
0.649188761377
0.675311310075
0.661992433796
0.654551245247
61.590
batch start
#iterations: 40
currently lose_sum: 96.46465915441513
time_elpased: 1.894
batch start
#iterations: 41
currently lose_sum: 96.10443878173828
time_elpased: 1.843
batch start
#iterations: 42
currently lose_sum: 96.02214121818542
time_elpased: 1.857
batch start
#iterations: 43
currently lose_sum: 96.21710938215256
time_elpased: 1.838
batch start
#iterations: 44
currently lose_sum: 95.86604881286621
time_elpased: 1.825
batch start
#iterations: 45
currently lose_sum: 95.80073028802872
time_elpased: 1.84
batch start
#iterations: 46
currently lose_sum: 95.61454164981842
time_elpased: 1.798
batch start
#iterations: 47
currently lose_sum: 96.17325174808502
time_elpased: 1.852
batch start
#iterations: 48
currently lose_sum: 95.99585944414139
time_elpased: 1.861
batch start
#iterations: 49
currently lose_sum: 96.13380062580109
time_elpased: 1.844
batch start
#iterations: 50
currently lose_sum: 96.39467656612396
time_elpased: 1.849
batch start
#iterations: 51
currently lose_sum: 95.96568250656128
time_elpased: 1.842
batch start
#iterations: 52
currently lose_sum: 96.19271731376648
time_elpased: 1.816
batch start
#iterations: 53
currently lose_sum: 96.09580022096634
time_elpased: 1.855
batch start
#iterations: 54
currently lose_sum: 96.00817161798477
time_elpased: 1.836
batch start
#iterations: 55
currently lose_sum: 95.66337937116623
time_elpased: 1.868
batch start
#iterations: 56
currently lose_sum: 95.7610524892807
time_elpased: 1.836
batch start
#iterations: 57
currently lose_sum: 95.70416390895844
time_elpased: 1.911
batch start
#iterations: 58
currently lose_sum: 95.96125543117523
time_elpased: 1.841
batch start
#iterations: 59
currently lose_sum: 95.73915374279022
time_elpased: 1.867
start validation test
0.658453608247
0.647176459385
0.699392816713
0.672272232664
0.658381733153
61.208
batch start
#iterations: 60
currently lose_sum: 95.44268697500229
time_elpased: 1.912
batch start
#iterations: 61
currently lose_sum: 96.19020038843155
time_elpased: 1.854
batch start
#iterations: 62
currently lose_sum: 95.73448795080185
time_elpased: 1.858
batch start
#iterations: 63
currently lose_sum: 96.21156239509583
time_elpased: 1.928
batch start
#iterations: 64
currently lose_sum: 95.52930176258087
time_elpased: 1.841
batch start
#iterations: 65
currently lose_sum: 95.41474711894989
time_elpased: 1.905
batch start
#iterations: 66
currently lose_sum: 95.65009546279907
time_elpased: 1.837
batch start
#iterations: 67
currently lose_sum: 95.79815340042114
time_elpased: 1.898
batch start
#iterations: 68
currently lose_sum: 96.02416622638702
time_elpased: 1.856
batch start
#iterations: 69
currently lose_sum: 95.89593029022217
time_elpased: 1.829
batch start
#iterations: 70
currently lose_sum: 95.49652695655823
time_elpased: 1.815
batch start
#iterations: 71
currently lose_sum: 95.54433727264404
time_elpased: 1.836
batch start
#iterations: 72
currently lose_sum: 95.82836163043976
time_elpased: 1.866
batch start
#iterations: 73
currently lose_sum: 95.42668187618256
time_elpased: 1.843
batch start
#iterations: 74
currently lose_sum: 95.60147094726562
time_elpased: 1.849
batch start
#iterations: 75
currently lose_sum: 95.55852800607681
time_elpased: 1.82
batch start
#iterations: 76
currently lose_sum: 95.46433782577515
time_elpased: 1.833
batch start
#iterations: 77
currently lose_sum: 95.5633534193039
time_elpased: 1.831
batch start
#iterations: 78
currently lose_sum: 95.83439511060715
time_elpased: 1.846
batch start
#iterations: 79
currently lose_sum: 95.56661415100098
time_elpased: 1.827
start validation test
0.629381443299
0.667417516894
0.518369867243
0.583526413346
0.629576341243
62.398
batch start
#iterations: 80
currently lose_sum: 95.69447267055511
time_elpased: 1.848
batch start
#iterations: 81
currently lose_sum: 95.65852707624435
time_elpased: 1.843
batch start
#iterations: 82
currently lose_sum: 95.7337594628334
time_elpased: 1.892
batch start
#iterations: 83
currently lose_sum: 95.49485939741135
time_elpased: 1.812
batch start
#iterations: 84
currently lose_sum: 95.58316087722778
time_elpased: 1.862
batch start
#iterations: 85
currently lose_sum: 95.77638137340546
time_elpased: 1.85
batch start
#iterations: 86
currently lose_sum: 95.63097310066223
time_elpased: 1.831
batch start
#iterations: 87
currently lose_sum: 95.1249263882637
time_elpased: 1.869
batch start
#iterations: 88
currently lose_sum: 95.54189068078995
time_elpased: 1.941
batch start
#iterations: 89
currently lose_sum: 95.39882296323776
time_elpased: 1.88
batch start
#iterations: 90
currently lose_sum: 95.62213498353958
time_elpased: 1.822
batch start
#iterations: 91
currently lose_sum: 95.2584758400917
time_elpased: 1.828
batch start
#iterations: 92
currently lose_sum: 95.41622394323349
time_elpased: 1.854
batch start
#iterations: 93
currently lose_sum: 95.23381584882736
time_elpased: 1.877
batch start
#iterations: 94
currently lose_sum: 95.54472327232361
time_elpased: 1.842
batch start
#iterations: 95
currently lose_sum: 95.62635958194733
time_elpased: 1.842
batch start
#iterations: 96
currently lose_sum: 95.71378374099731
time_elpased: 1.848
batch start
#iterations: 97
currently lose_sum: 95.45027786493301
time_elpased: 1.84
batch start
#iterations: 98
currently lose_sum: 95.58541107177734
time_elpased: 1.852
batch start
#iterations: 99
currently lose_sum: 94.94865149259567
time_elpased: 1.848
start validation test
0.667371134021
0.629462160876
0.816610064835
0.710925950813
0.667109122059
60.839
batch start
#iterations: 100
currently lose_sum: 95.34452384710312
time_elpased: 1.915
batch start
#iterations: 101
currently lose_sum: 95.40258699655533
time_elpased: 1.856
batch start
#iterations: 102
currently lose_sum: 95.02337682247162
time_elpased: 1.878
batch start
#iterations: 103
currently lose_sum: 95.29085779190063
time_elpased: 1.904
batch start
#iterations: 104
currently lose_sum: 95.53706777095795
time_elpased: 1.834
batch start
#iterations: 105
currently lose_sum: 95.40043443441391
time_elpased: 1.852
batch start
#iterations: 106
currently lose_sum: 95.57038694620132
time_elpased: 1.818
batch start
#iterations: 107
currently lose_sum: 95.94860470294952
time_elpased: 1.83
batch start
#iterations: 108
currently lose_sum: 95.21832978725433
time_elpased: 2.033
batch start
#iterations: 109
currently lose_sum: 95.2775291800499
time_elpased: 1.886
batch start
#iterations: 110
currently lose_sum: 95.21905690431595
time_elpased: 1.861
batch start
#iterations: 111
currently lose_sum: 95.08960694074631
time_elpased: 1.856
batch start
#iterations: 112
currently lose_sum: 95.31292682886124
time_elpased: 1.885
batch start
#iterations: 113
currently lose_sum: 95.21351021528244
time_elpased: 1.875
batch start
#iterations: 114
currently lose_sum: 94.89077013731003
time_elpased: 2.004
batch start
#iterations: 115
currently lose_sum: 95.1518257856369
time_elpased: 1.845
batch start
#iterations: 116
currently lose_sum: 95.42307245731354
time_elpased: 1.85
batch start
#iterations: 117
currently lose_sum: 95.38699895143509
time_elpased: 1.86
batch start
#iterations: 118
currently lose_sum: 95.37456268072128
time_elpased: 1.832
batch start
#iterations: 119
currently lose_sum: 95.40571248531342
time_elpased: 1.867
start validation test
0.660567010309
0.663567996658
0.653802613976
0.658649110984
0.66057888625
61.291
batch start
#iterations: 120
currently lose_sum: 95.09104490280151
time_elpased: 1.875
batch start
#iterations: 121
currently lose_sum: 95.32098788022995
time_elpased: 1.849
batch start
#iterations: 122
currently lose_sum: 95.36047124862671
time_elpased: 1.857
batch start
#iterations: 123
currently lose_sum: 95.06709808111191
time_elpased: 1.859
batch start
#iterations: 124
currently lose_sum: 95.25920814275742
time_elpased: 1.872
batch start
#iterations: 125
currently lose_sum: 95.35098254680634
time_elpased: 1.837
batch start
#iterations: 126
currently lose_sum: 95.45873671770096
time_elpased: 1.862
batch start
#iterations: 127
currently lose_sum: 95.26626801490784
time_elpased: 1.906
batch start
#iterations: 128
currently lose_sum: 94.8490839600563
time_elpased: 1.836
batch start
#iterations: 129
currently lose_sum: 94.90858560800552
time_elpased: 1.884
batch start
#iterations: 130
currently lose_sum: 95.28239506483078
time_elpased: 1.888
batch start
#iterations: 131
currently lose_sum: 95.24774903059006
time_elpased: 1.904
batch start
#iterations: 132
currently lose_sum: 95.14531642198563
time_elpased: 1.858
batch start
#iterations: 133
currently lose_sum: 95.00836968421936
time_elpased: 1.855
batch start
#iterations: 134
currently lose_sum: 95.24973595142365
time_elpased: 1.848
batch start
#iterations: 135
currently lose_sum: 94.94267076253891
time_elpased: 1.852
batch start
#iterations: 136
currently lose_sum: 95.20033174753189
time_elpased: 1.866
batch start
#iterations: 137
currently lose_sum: 94.94986492395401
time_elpased: 1.85
batch start
#iterations: 138
currently lose_sum: 94.84780365228653
time_elpased: 1.892
batch start
#iterations: 139
currently lose_sum: 95.42053961753845
time_elpased: 1.824
start validation test
0.66206185567
0.633567142736
0.771534424205
0.695777262181
0.661869659691
60.985
batch start
#iterations: 140
currently lose_sum: 95.59109115600586
time_elpased: 1.954
batch start
#iterations: 141
currently lose_sum: 94.85768377780914
time_elpased: 1.919
batch start
#iterations: 142
currently lose_sum: 95.15104699134827
time_elpased: 1.867
batch start
#iterations: 143
currently lose_sum: 95.30328166484833
time_elpased: 1.853
batch start
#iterations: 144
currently lose_sum: 95.36280107498169
time_elpased: 1.864
batch start
#iterations: 145
currently lose_sum: 95.13078838586807
time_elpased: 1.867
batch start
#iterations: 146
currently lose_sum: 95.01391822099686
time_elpased: 1.838
batch start
#iterations: 147
currently lose_sum: 95.24336069822311
time_elpased: 1.826
batch start
#iterations: 148
currently lose_sum: 95.38086664676666
time_elpased: 1.877
batch start
#iterations: 149
currently lose_sum: 94.9634125828743
time_elpased: 1.854
batch start
#iterations: 150
currently lose_sum: 94.88321089744568
time_elpased: 1.873
batch start
#iterations: 151
currently lose_sum: 95.2679848074913
time_elpased: 1.908
batch start
#iterations: 152
currently lose_sum: 95.51714205741882
time_elpased: 2.04
batch start
#iterations: 153
currently lose_sum: 95.20807856321335
time_elpased: 1.85
batch start
#iterations: 154
currently lose_sum: 95.23560869693756
time_elpased: 1.889
batch start
#iterations: 155
currently lose_sum: 95.05161428451538
time_elpased: 1.847
batch start
#iterations: 156
currently lose_sum: 95.21316796541214
time_elpased: 1.86
batch start
#iterations: 157
currently lose_sum: 95.4041975736618
time_elpased: 1.849
batch start
#iterations: 158
currently lose_sum: 95.76562029123306
time_elpased: 1.867
batch start
#iterations: 159
currently lose_sum: 95.53715968132019
time_elpased: 1.889
start validation test
0.665515463918
0.659297275957
0.687454975816
0.673081767343
0.66547694572
61.105
batch start
#iterations: 160
currently lose_sum: 95.00368338823318
time_elpased: 1.857
batch start
#iterations: 161
currently lose_sum: 94.91187685728073
time_elpased: 1.864
batch start
#iterations: 162
currently lose_sum: 94.89035558700562
time_elpased: 1.932
batch start
#iterations: 163
currently lose_sum: 94.85347723960876
time_elpased: 1.834
batch start
#iterations: 164
currently lose_sum: 95.064881503582
time_elpased: 1.936
batch start
#iterations: 165
currently lose_sum: 95.19615244865417
time_elpased: 1.83
batch start
#iterations: 166
currently lose_sum: 95.02828049659729
time_elpased: 1.852
batch start
#iterations: 167
currently lose_sum: 95.08511221408844
time_elpased: 1.869
batch start
#iterations: 168
currently lose_sum: 94.87631559371948
time_elpased: 1.916
batch start
#iterations: 169
currently lose_sum: 94.87765949964523
time_elpased: 1.857
batch start
#iterations: 170
currently lose_sum: 95.09066498279572
time_elpased: 1.841
batch start
#iterations: 171
currently lose_sum: 95.1152451634407
time_elpased: 1.887
batch start
#iterations: 172
currently lose_sum: 95.20993781089783
time_elpased: 1.944
batch start
#iterations: 173
currently lose_sum: 95.02845376729965
time_elpased: 2.015
batch start
#iterations: 174
currently lose_sum: 94.99743980169296
time_elpased: 1.863
batch start
#iterations: 175
currently lose_sum: 94.73124432563782
time_elpased: 1.818
batch start
#iterations: 176
currently lose_sum: 94.93836832046509
time_elpased: 1.854
batch start
#iterations: 177
currently lose_sum: 94.83696013689041
time_elpased: 1.853
batch start
#iterations: 178
currently lose_sum: 94.96111035346985
time_elpased: 1.84
batch start
#iterations: 179
currently lose_sum: 95.35834485292435
time_elpased: 1.875
start validation test
0.663711340206
0.63336396291
0.780281980035
0.699188491332
0.663506682468
60.799
batch start
#iterations: 180
currently lose_sum: 95.00581294298172
time_elpased: 1.898
batch start
#iterations: 181
currently lose_sum: 94.92385649681091
time_elpased: 1.88
batch start
#iterations: 182
currently lose_sum: 94.68325859308243
time_elpased: 1.896
batch start
#iterations: 183
currently lose_sum: 94.52472877502441
time_elpased: 1.86
batch start
#iterations: 184
currently lose_sum: 94.95103305578232
time_elpased: 1.865
batch start
#iterations: 185
currently lose_sum: 94.96730226278305
time_elpased: 1.851
batch start
#iterations: 186
currently lose_sum: 94.54378473758698
time_elpased: 1.853
batch start
#iterations: 187
currently lose_sum: 94.95835530757904
time_elpased: 1.925
batch start
#iterations: 188
currently lose_sum: 94.82964080572128
time_elpased: 1.918
batch start
#iterations: 189
currently lose_sum: 95.03022146224976
time_elpased: 1.983
batch start
#iterations: 190
currently lose_sum: 95.03754210472107
time_elpased: 1.821
batch start
#iterations: 191
currently lose_sum: 94.96113234758377
time_elpased: 1.872
batch start
#iterations: 192
currently lose_sum: 94.811243891716
time_elpased: 1.852
batch start
#iterations: 193
currently lose_sum: 94.93084490299225
time_elpased: 1.867
batch start
#iterations: 194
currently lose_sum: 95.17060667276382
time_elpased: 1.847
batch start
#iterations: 195
currently lose_sum: 94.9859362244606
time_elpased: 1.861
batch start
#iterations: 196
currently lose_sum: 95.53605258464813
time_elpased: 1.835
batch start
#iterations: 197
currently lose_sum: 94.76873481273651
time_elpased: 1.884
batch start
#iterations: 198
currently lose_sum: 94.87073928117752
time_elpased: 1.898
batch start
#iterations: 199
currently lose_sum: 94.96046876907349
time_elpased: 1.887
start validation test
0.661649484536
0.638301605404
0.748790779047
0.689145671529
0.661496494553
60.938
batch start
#iterations: 200
currently lose_sum: 94.76994824409485
time_elpased: 1.872
batch start
#iterations: 201
currently lose_sum: 94.5269627571106
time_elpased: 1.851
batch start
#iterations: 202
currently lose_sum: 95.1085969209671
time_elpased: 1.841
batch start
#iterations: 203
currently lose_sum: 94.46938693523407
time_elpased: 1.839
batch start
#iterations: 204
currently lose_sum: 94.60210365056992
time_elpased: 1.859
batch start
#iterations: 205
currently lose_sum: 94.9667683839798
time_elpased: 1.834
batch start
#iterations: 206
currently lose_sum: 94.81090945005417
time_elpased: 1.84
batch start
#iterations: 207
currently lose_sum: 95.10016477108002
time_elpased: 1.852
batch start
#iterations: 208
currently lose_sum: 94.44334000349045
time_elpased: 1.849
batch start
#iterations: 209
currently lose_sum: 94.86276251077652
time_elpased: 1.933
batch start
#iterations: 210
currently lose_sum: 94.66925913095474
time_elpased: 1.899
batch start
#iterations: 211
currently lose_sum: 94.24454760551453
time_elpased: 1.882
batch start
#iterations: 212
currently lose_sum: 94.85575598478317
time_elpased: 1.863
batch start
#iterations: 213
currently lose_sum: 94.84036159515381
time_elpased: 1.884
batch start
#iterations: 214
currently lose_sum: 94.80323302745819
time_elpased: 1.891
batch start
#iterations: 215
currently lose_sum: 94.59540927410126
time_elpased: 1.896
batch start
#iterations: 216
currently lose_sum: 94.62043243646622
time_elpased: 1.873
batch start
#iterations: 217
currently lose_sum: 95.11676806211472
time_elpased: 1.89
batch start
#iterations: 218
currently lose_sum: 94.9429179430008
time_elpased: 1.903
batch start
#iterations: 219
currently lose_sum: 95.0155748128891
time_elpased: 1.915
start validation test
0.670360824742
0.646861184792
0.752907275908
0.695867218338
0.670215901715
60.726
batch start
#iterations: 220
currently lose_sum: 94.4861233830452
time_elpased: 1.983
batch start
#iterations: 221
currently lose_sum: 94.92797780036926
time_elpased: 1.988
batch start
#iterations: 222
currently lose_sum: 94.73279976844788
time_elpased: 1.864
batch start
#iterations: 223
currently lose_sum: 94.75199383497238
time_elpased: 1.875
batch start
#iterations: 224
currently lose_sum: 94.83320826292038
time_elpased: 1.88
batch start
#iterations: 225
currently lose_sum: 94.70829784870148
time_elpased: 1.846
batch start
#iterations: 226
currently lose_sum: 94.47375667095184
time_elpased: 1.847
batch start
#iterations: 227
currently lose_sum: 94.78811055421829
time_elpased: 1.952
batch start
#iterations: 228
currently lose_sum: 94.55210381746292
time_elpased: 1.895
batch start
#iterations: 229
currently lose_sum: 94.4830652475357
time_elpased: 1.875
batch start
#iterations: 230
currently lose_sum: 94.89038825035095
time_elpased: 1.965
batch start
#iterations: 231
currently lose_sum: 94.65157961845398
time_elpased: 1.841
batch start
#iterations: 232
currently lose_sum: 94.67086738348007
time_elpased: 1.889
batch start
#iterations: 233
currently lose_sum: 94.86752408742905
time_elpased: 1.927
batch start
#iterations: 234
currently lose_sum: 94.87477660179138
time_elpased: 1.859
batch start
#iterations: 235
currently lose_sum: 94.89923799037933
time_elpased: 1.893
batch start
#iterations: 236
currently lose_sum: 94.82827377319336
time_elpased: 1.883
batch start
#iterations: 237
currently lose_sum: 95.00234282016754
time_elpased: 2.035
batch start
#iterations: 238
currently lose_sum: 95.01600635051727
time_elpased: 1.885
batch start
#iterations: 239
currently lose_sum: 94.73106598854065
time_elpased: 1.836
start validation test
0.661340206186
0.64120972808
0.735309251827
0.685043144775
0.661210342117
60.761
batch start
#iterations: 240
currently lose_sum: 94.71079552173615
time_elpased: 1.872
batch start
#iterations: 241
currently lose_sum: 94.69524645805359
time_elpased: 1.917
batch start
#iterations: 242
currently lose_sum: 94.68298238515854
time_elpased: 1.899
batch start
#iterations: 243
currently lose_sum: 95.32971775531769
time_elpased: 1.902
batch start
#iterations: 244
currently lose_sum: 94.7746524810791
time_elpased: 1.854
batch start
#iterations: 245
currently lose_sum: 94.80290776491165
time_elpased: 1.942
batch start
#iterations: 246
currently lose_sum: 94.6102643609047
time_elpased: 1.886
batch start
#iterations: 247
currently lose_sum: 94.89297318458557
time_elpased: 1.931
batch start
#iterations: 248
currently lose_sum: 94.69888937473297
time_elpased: 1.875
batch start
#iterations: 249
currently lose_sum: 94.64652794599533
time_elpased: 1.891
batch start
#iterations: 250
currently lose_sum: 94.57038497924805
time_elpased: 1.866
batch start
#iterations: 251
currently lose_sum: 94.99395978450775
time_elpased: 1.885
batch start
#iterations: 252
currently lose_sum: 94.63560098409653
time_elpased: 1.955
batch start
#iterations: 253
currently lose_sum: 95.08715337514877
time_elpased: 1.858
batch start
#iterations: 254
currently lose_sum: 94.51558440923691
time_elpased: 1.89
batch start
#iterations: 255
currently lose_sum: 94.83875715732574
time_elpased: 1.859
batch start
#iterations: 256
currently lose_sum: 94.39295679330826
time_elpased: 1.832
batch start
#iterations: 257
currently lose_sum: 94.35460430383682
time_elpased: 1.896
batch start
#iterations: 258
currently lose_sum: 94.41034537553787
time_elpased: 1.883
batch start
#iterations: 259
currently lose_sum: 94.43929266929626
time_elpased: 1.924
start validation test
0.665670103093
0.633899709905
0.787074199856
0.702231200073
0.665456959476
60.494
batch start
#iterations: 260
currently lose_sum: 94.3654677271843
time_elpased: 1.935
batch start
#iterations: 261
currently lose_sum: 94.720578789711
time_elpased: 1.872
batch start
#iterations: 262
currently lose_sum: 94.49756383895874
time_elpased: 1.932
batch start
#iterations: 263
currently lose_sum: 94.40442645549774
time_elpased: 1.869
batch start
#iterations: 264
currently lose_sum: 94.84104013442993
time_elpased: 1.894
batch start
#iterations: 265
currently lose_sum: 94.92628687620163
time_elpased: 1.876
batch start
#iterations: 266
currently lose_sum: 94.78317415714264
time_elpased: 1.918
batch start
#iterations: 267
currently lose_sum: 94.61701399087906
time_elpased: 1.922
batch start
#iterations: 268
currently lose_sum: 94.53758174180984
time_elpased: 1.872
batch start
#iterations: 269
currently lose_sum: 94.55520439147949
time_elpased: 1.846
batch start
#iterations: 270
currently lose_sum: 94.36000180244446
time_elpased: 1.839
batch start
#iterations: 271
currently lose_sum: 94.503069460392
time_elpased: 1.965
batch start
#iterations: 272
currently lose_sum: 94.7359848022461
time_elpased: 1.905
batch start
#iterations: 273
currently lose_sum: 94.57242661714554
time_elpased: 1.853
batch start
#iterations: 274
currently lose_sum: 95.22737002372742
time_elpased: 1.846
batch start
#iterations: 275
currently lose_sum: 94.71236777305603
time_elpased: 1.849
batch start
#iterations: 276
currently lose_sum: 94.34712225198746
time_elpased: 1.918
batch start
#iterations: 277
currently lose_sum: 94.84436738491058
time_elpased: 1.838
batch start
#iterations: 278
currently lose_sum: 94.45726180076599
time_elpased: 1.879
batch start
#iterations: 279
currently lose_sum: 94.24542045593262
time_elpased: 1.871
start validation test
0.661855670103
0.651269765213
0.699392816713
0.674473997618
0.661789767853
60.982
batch start
#iterations: 280
currently lose_sum: 94.56525337696075
time_elpased: 1.874
batch start
#iterations: 281
currently lose_sum: 94.72939890623093
time_elpased: 1.843
batch start
#iterations: 282
currently lose_sum: 94.18762129545212
time_elpased: 1.887
batch start
#iterations: 283
currently lose_sum: 94.85373854637146
time_elpased: 1.862
batch start
#iterations: 284
currently lose_sum: 94.53832203149796
time_elpased: 1.93
batch start
#iterations: 285
currently lose_sum: 94.58362311124802
time_elpased: 1.874
batch start
#iterations: 286
currently lose_sum: 93.98227262496948
time_elpased: 1.859
batch start
#iterations: 287
currently lose_sum: 94.38994574546814
time_elpased: 1.885
batch start
#iterations: 288
currently lose_sum: 94.06004816293716
time_elpased: 1.873
batch start
#iterations: 289
currently lose_sum: 95.2847467660904
time_elpased: 1.945
batch start
#iterations: 290
currently lose_sum: 94.84714424610138
time_elpased: 1.937
batch start
#iterations: 291
currently lose_sum: 94.34177595376968
time_elpased: 1.85
batch start
#iterations: 292
currently lose_sum: 94.72663503885269
time_elpased: 1.875
batch start
#iterations: 293
currently lose_sum: 94.73525142669678
time_elpased: 1.849
batch start
#iterations: 294
currently lose_sum: 94.67060226202011
time_elpased: 1.875
batch start
#iterations: 295
currently lose_sum: 94.76401329040527
time_elpased: 1.901
batch start
#iterations: 296
currently lose_sum: 94.25313884019852
time_elpased: 1.902
batch start
#iterations: 297
currently lose_sum: 94.69867134094238
time_elpased: 1.855
batch start
#iterations: 298
currently lose_sum: 94.57289588451385
time_elpased: 1.89
batch start
#iterations: 299
currently lose_sum: 94.41668605804443
time_elpased: 1.907
start validation test
0.669175257732
0.632628447375
0.809714932592
0.710300622912
0.668928518656
60.274
batch start
#iterations: 300
currently lose_sum: 94.03265815973282
time_elpased: 1.954
batch start
#iterations: 301
currently lose_sum: 94.71451598405838
time_elpased: 1.883
batch start
#iterations: 302
currently lose_sum: 94.56728649139404
time_elpased: 1.843
batch start
#iterations: 303
currently lose_sum: 94.63328289985657
time_elpased: 1.856
batch start
#iterations: 304
currently lose_sum: 94.54454308748245
time_elpased: 1.86
batch start
#iterations: 305
currently lose_sum: 94.79279667139053
time_elpased: 1.893
batch start
#iterations: 306
currently lose_sum: 94.71666985750198
time_elpased: 1.89
batch start
#iterations: 307
currently lose_sum: 94.5425974726677
time_elpased: 1.841
batch start
#iterations: 308
currently lose_sum: 94.38468199968338
time_elpased: 1.858
batch start
#iterations: 309
currently lose_sum: 94.52536225318909
time_elpased: 1.815
batch start
#iterations: 310
currently lose_sum: 94.8639788031578
time_elpased: 1.846
batch start
#iterations: 311
currently lose_sum: 94.07894790172577
time_elpased: 1.857
batch start
#iterations: 312
currently lose_sum: 94.30811500549316
time_elpased: 1.869
batch start
#iterations: 313
currently lose_sum: 95.03019338846207
time_elpased: 1.828
batch start
#iterations: 314
currently lose_sum: 94.33658772706985
time_elpased: 1.888
batch start
#iterations: 315
currently lose_sum: 94.55957996845245
time_elpased: 1.857
batch start
#iterations: 316
currently lose_sum: 94.47572094202042
time_elpased: 1.862
batch start
#iterations: 317
currently lose_sum: 94.41901761293411
time_elpased: 1.835
batch start
#iterations: 318
currently lose_sum: 94.51022750139236
time_elpased: 1.859
batch start
#iterations: 319
currently lose_sum: 95.01754170656204
time_elpased: 1.888
start validation test
0.653041237113
0.663813912662
0.622620150252
0.642557484998
0.653094646024
61.358
batch start
#iterations: 320
currently lose_sum: 94.61286973953247
time_elpased: 1.864
batch start
#iterations: 321
currently lose_sum: 94.44213104248047
time_elpased: 1.9
batch start
#iterations: 322
currently lose_sum: 94.65093350410461
time_elpased: 1.851
batch start
#iterations: 323
currently lose_sum: 95.02526378631592
time_elpased: 1.887
batch start
#iterations: 324
currently lose_sum: 94.79235315322876
time_elpased: 1.94
batch start
#iterations: 325
currently lose_sum: 94.49912643432617
time_elpased: 1.87
batch start
#iterations: 326
currently lose_sum: 94.81019574403763
time_elpased: 1.855
batch start
#iterations: 327
currently lose_sum: 94.11277693510056
time_elpased: 1.934
batch start
#iterations: 328
currently lose_sum: 94.71674537658691
time_elpased: 1.879
batch start
#iterations: 329
currently lose_sum: 94.21058177947998
time_elpased: 1.872
batch start
#iterations: 330
currently lose_sum: 94.45187056064606
time_elpased: 1.879
batch start
#iterations: 331
currently lose_sum: 94.06719106435776
time_elpased: 1.857
batch start
#iterations: 332
currently lose_sum: 94.33820307254791
time_elpased: 1.858
batch start
#iterations: 333
currently lose_sum: 94.3650888800621
time_elpased: 1.829
batch start
#iterations: 334
currently lose_sum: 94.49635690450668
time_elpased: 1.842
batch start
#iterations: 335
currently lose_sum: 94.29038071632385
time_elpased: 1.85
batch start
#iterations: 336
currently lose_sum: 94.4440204501152
time_elpased: 1.893
batch start
#iterations: 337
currently lose_sum: 94.33554273843765
time_elpased: 1.813
batch start
#iterations: 338
currently lose_sum: 94.31703978776932
time_elpased: 1.957
batch start
#iterations: 339
currently lose_sum: 94.4950487613678
time_elpased: 1.883
start validation test
0.668608247423
0.649291681802
0.735823813934
0.689854792802
0.668490240128
60.591
batch start
#iterations: 340
currently lose_sum: 94.76097351312637
time_elpased: 2.027
batch start
#iterations: 341
currently lose_sum: 94.56527280807495
time_elpased: 1.88
batch start
#iterations: 342
currently lose_sum: 94.43186855316162
time_elpased: 1.93
batch start
#iterations: 343
currently lose_sum: 94.34921139478683
time_elpased: 1.86
batch start
#iterations: 344
currently lose_sum: 94.42399907112122
time_elpased: 2.047
batch start
#iterations: 345
currently lose_sum: 94.46344751119614
time_elpased: 1.902
batch start
#iterations: 346
currently lose_sum: 94.99473518133163
time_elpased: 1.917
batch start
#iterations: 347
currently lose_sum: 94.510427236557
time_elpased: 1.84
batch start
#iterations: 348
currently lose_sum: 94.76164472103119
time_elpased: 1.86
batch start
#iterations: 349
currently lose_sum: 94.43152105808258
time_elpased: 1.824
batch start
#iterations: 350
currently lose_sum: 94.54454678297043
time_elpased: 1.833
batch start
#iterations: 351
currently lose_sum: 94.53805160522461
time_elpased: 1.846
batch start
#iterations: 352
currently lose_sum: 94.59598314762115
time_elpased: 1.84
batch start
#iterations: 353
currently lose_sum: 94.90030026435852
time_elpased: 1.877
batch start
#iterations: 354
currently lose_sum: 94.96273493766785
time_elpased: 1.867
batch start
#iterations: 355
currently lose_sum: 94.55006337165833
time_elpased: 1.861
batch start
#iterations: 356
currently lose_sum: 94.42682778835297
time_elpased: 1.911
batch start
#iterations: 357
currently lose_sum: 94.78232473134995
time_elpased: 1.86
batch start
#iterations: 358
currently lose_sum: 94.75241535902023
time_elpased: 1.914
batch start
#iterations: 359
currently lose_sum: 94.38219326734543
time_elpased: 1.892
start validation test
0.663969072165
0.635946267642
0.769784913039
0.696494250198
0.663783296135
60.818
batch start
#iterations: 360
currently lose_sum: 94.8636400103569
time_elpased: 1.932
batch start
#iterations: 361
currently lose_sum: 94.63977879285812
time_elpased: 1.924
batch start
#iterations: 362
currently lose_sum: 93.88506555557251
time_elpased: 1.834
batch start
#iterations: 363
currently lose_sum: 94.85561037063599
time_elpased: 1.836
batch start
#iterations: 364
currently lose_sum: 94.76598995923996
time_elpased: 1.875
batch start
#iterations: 365
currently lose_sum: 94.61651474237442
time_elpased: 1.897
batch start
#iterations: 366
currently lose_sum: 94.52868157625198
time_elpased: 1.878
batch start
#iterations: 367
currently lose_sum: 94.05850327014923
time_elpased: 1.889
batch start
#iterations: 368
currently lose_sum: 94.83927065134048
time_elpased: 1.924
batch start
#iterations: 369
currently lose_sum: 94.39696353673935
time_elpased: 1.854
batch start
#iterations: 370
currently lose_sum: 94.47690898180008
time_elpased: 1.938
batch start
#iterations: 371
currently lose_sum: 94.63893043994904
time_elpased: 1.887
batch start
#iterations: 372
currently lose_sum: 94.71224111318588
time_elpased: 1.918
batch start
#iterations: 373
currently lose_sum: 94.33078861236572
time_elpased: 1.905
batch start
#iterations: 374
currently lose_sum: 94.45020061731339
time_elpased: 1.909
batch start
#iterations: 375
currently lose_sum: 94.4297541975975
time_elpased: 2.008
batch start
#iterations: 376
currently lose_sum: 94.1515491604805
time_elpased: 1.86
batch start
#iterations: 377
currently lose_sum: 94.49218130111694
time_elpased: 1.85
batch start
#iterations: 378
currently lose_sum: 94.46124941110611
time_elpased: 1.958
batch start
#iterations: 379
currently lose_sum: 94.53352171182632
time_elpased: 1.924
start validation test
0.666701030928
0.636792055878
0.778738293712
0.700648148148
0.666504332232
60.633
batch start
#iterations: 380
currently lose_sum: 94.50066500902176
time_elpased: 1.867
batch start
#iterations: 381
currently lose_sum: 94.25230544805527
time_elpased: 1.855
batch start
#iterations: 382
currently lose_sum: 94.26293140649796
time_elpased: 1.899
batch start
#iterations: 383
currently lose_sum: 94.10408818721771
time_elpased: 1.901
batch start
#iterations: 384
currently lose_sum: 94.7668468952179
time_elpased: 1.879
batch start
#iterations: 385
currently lose_sum: 94.44823521375656
time_elpased: 1.876
batch start
#iterations: 386
currently lose_sum: 94.14201521873474
time_elpased: 1.852
batch start
#iterations: 387
currently lose_sum: 94.27132868766785
time_elpased: 1.877
batch start
#iterations: 388
currently lose_sum: 94.64827990531921
time_elpased: 1.909
batch start
#iterations: 389
currently lose_sum: 94.44042760133743
time_elpased: 1.86
batch start
#iterations: 390
currently lose_sum: 94.53697609901428
time_elpased: 1.89
batch start
#iterations: 391
currently lose_sum: 94.48579287528992
time_elpased: 1.901
batch start
#iterations: 392
currently lose_sum: 94.0418763756752
time_elpased: 1.843
batch start
#iterations: 393
currently lose_sum: 94.10314548015594
time_elpased: 1.871
batch start
#iterations: 394
currently lose_sum: 94.65665227174759
time_elpased: 1.832
batch start
#iterations: 395
currently lose_sum: 94.45614904165268
time_elpased: 1.889
batch start
#iterations: 396
currently lose_sum: 94.12933045625687
time_elpased: 1.834
batch start
#iterations: 397
currently lose_sum: 94.11897587776184
time_elpased: 1.913
batch start
#iterations: 398
currently lose_sum: 94.54610657691956
time_elpased: 1.875
batch start
#iterations: 399
currently lose_sum: 94.42166018486023
time_elpased: 1.889
start validation test
0.662835051546
0.641963168246
0.739014099002
0.687078409798
0.662701307479
60.865
acc: 0.669
pre: 0.633
rec: 0.808
F1: 0.710
auc: 0.669
