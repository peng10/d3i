start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.40705871582031
time_elpased: 2.198
batch start
#iterations: 1
currently lose_sum: 99.93065541982651
time_elpased: 1.957
batch start
#iterations: 2
currently lose_sum: 99.66118878126144
time_elpased: 1.938
batch start
#iterations: 3
currently lose_sum: 99.27652037143707
time_elpased: 1.939
batch start
#iterations: 4
currently lose_sum: 98.87913328409195
time_elpased: 1.9
batch start
#iterations: 5
currently lose_sum: 98.87360501289368
time_elpased: 1.932
batch start
#iterations: 6
currently lose_sum: 98.4389808177948
time_elpased: 2.075
batch start
#iterations: 7
currently lose_sum: 98.53924536705017
time_elpased: 1.993
batch start
#iterations: 8
currently lose_sum: 98.41096407175064
time_elpased: 1.948
batch start
#iterations: 9
currently lose_sum: 98.18094789981842
time_elpased: 1.925
batch start
#iterations: 10
currently lose_sum: 97.99128496646881
time_elpased: 1.91
batch start
#iterations: 11
currently lose_sum: 97.87690621614456
time_elpased: 1.899
batch start
#iterations: 12
currently lose_sum: 98.15268331766129
time_elpased: 1.936
batch start
#iterations: 13
currently lose_sum: 97.32966601848602
time_elpased: 1.964
batch start
#iterations: 14
currently lose_sum: 97.52376401424408
time_elpased: 1.926
batch start
#iterations: 15
currently lose_sum: 97.42449462413788
time_elpased: 2.013
batch start
#iterations: 16
currently lose_sum: 97.2421435713768
time_elpased: 2.037
batch start
#iterations: 17
currently lose_sum: 96.9850224852562
time_elpased: 2.038
batch start
#iterations: 18
currently lose_sum: 97.00613355636597
time_elpased: 2.024
batch start
#iterations: 19
currently lose_sum: 97.11793959140778
time_elpased: 2.008
start validation test
0.658298969072
0.637439914545
0.736955850571
0.683595055129
0.658160874785
62.233
batch start
#iterations: 20
currently lose_sum: 96.96704083681107
time_elpased: 2.036
batch start
#iterations: 21
currently lose_sum: 96.76939463615417
time_elpased: 2.035
batch start
#iterations: 22
currently lose_sum: 96.80008906126022
time_elpased: 2.005
batch start
#iterations: 23
currently lose_sum: 96.5647537112236
time_elpased: 1.945
batch start
#iterations: 24
currently lose_sum: 96.63421338796616
time_elpased: 1.93
batch start
#iterations: 25
currently lose_sum: 96.71136891841888
time_elpased: 1.905
batch start
#iterations: 26
currently lose_sum: 96.43120914697647
time_elpased: 1.943
batch start
#iterations: 27
currently lose_sum: 96.5715326666832
time_elpased: 1.921
batch start
#iterations: 28
currently lose_sum: 96.44110041856766
time_elpased: 1.956
batch start
#iterations: 29
currently lose_sum: 96.77192050218582
time_elpased: 1.97
batch start
#iterations: 30
currently lose_sum: 96.25907099246979
time_elpased: 1.92
batch start
#iterations: 31
currently lose_sum: 96.54560178518295
time_elpased: 1.92
batch start
#iterations: 32
currently lose_sum: 96.07798647880554
time_elpased: 1.879
batch start
#iterations: 33
currently lose_sum: 96.51308107376099
time_elpased: 1.892
batch start
#iterations: 34
currently lose_sum: 96.37226241827011
time_elpased: 1.898
batch start
#iterations: 35
currently lose_sum: 96.40696090459824
time_elpased: 1.91
batch start
#iterations: 36
currently lose_sum: 96.05972063541412
time_elpased: 1.899
batch start
#iterations: 37
currently lose_sum: 96.06452292203903
time_elpased: 1.891
batch start
#iterations: 38
currently lose_sum: 96.17142707109451
time_elpased: 1.899
batch start
#iterations: 39
currently lose_sum: 96.07641530036926
time_elpased: 1.857
start validation test
0.634175257732
0.658097996621
0.561181434599
0.605787924235
0.634303409647
62.020
batch start
#iterations: 40
currently lose_sum: 96.02622121572495
time_elpased: 1.869
batch start
#iterations: 41
currently lose_sum: 96.01002556085587
time_elpased: 1.879
batch start
#iterations: 42
currently lose_sum: 95.92784720659256
time_elpased: 1.86
batch start
#iterations: 43
currently lose_sum: 95.8755516409874
time_elpased: 1.861
batch start
#iterations: 44
currently lose_sum: 96.20137286186218
time_elpased: 1.918
batch start
#iterations: 45
currently lose_sum: 96.13963615894318
time_elpased: 2.016
batch start
#iterations: 46
currently lose_sum: 95.87423449754715
time_elpased: 2.018
batch start
#iterations: 47
currently lose_sum: 96.11009734869003
time_elpased: 2.126
batch start
#iterations: 48
currently lose_sum: 96.08690416812897
time_elpased: 2.402
batch start
#iterations: 49
currently lose_sum: 96.06477725505829
time_elpased: 2.426
batch start
#iterations: 50
currently lose_sum: 95.75480854511261
time_elpased: 2.367
batch start
#iterations: 51
currently lose_sum: 96.00319117307663
time_elpased: 2.437
batch start
#iterations: 52
currently lose_sum: 95.74496060609818
time_elpased: 2.422
batch start
#iterations: 53
currently lose_sum: 95.88501769304276
time_elpased: 2.423
batch start
#iterations: 54
currently lose_sum: 95.79191756248474
time_elpased: 2.388
batch start
#iterations: 55
currently lose_sum: 95.92641085386276
time_elpased: 2.394
batch start
#iterations: 56
currently lose_sum: 95.93786334991455
time_elpased: 2.415
batch start
#iterations: 57
currently lose_sum: 95.90253102779388
time_elpased: 2.259
batch start
#iterations: 58
currently lose_sum: 95.74720644950867
time_elpased: 1.901
batch start
#iterations: 59
currently lose_sum: 95.98403680324554
time_elpased: 1.908
start validation test
0.663298969072
0.644000361696
0.732942266132
0.685598767809
0.663176699522
61.319
batch start
#iterations: 60
currently lose_sum: 95.9823911190033
time_elpased: 1.991
batch start
#iterations: 61
currently lose_sum: 95.87225389480591
time_elpased: 1.904
batch start
#iterations: 62
currently lose_sum: 96.22721260786057
time_elpased: 1.9
batch start
#iterations: 63
currently lose_sum: 95.65713047981262
time_elpased: 1.922
batch start
#iterations: 64
currently lose_sum: 95.69274455308914
time_elpased: 1.955
batch start
#iterations: 65
currently lose_sum: 95.71362417936325
time_elpased: 1.923
batch start
#iterations: 66
currently lose_sum: 95.72741800546646
time_elpased: 1.913
batch start
#iterations: 67
currently lose_sum: 95.81413596868515
time_elpased: 1.914
batch start
#iterations: 68
currently lose_sum: 95.65336793661118
time_elpased: 1.893
batch start
#iterations: 69
currently lose_sum: 95.8466020822525
time_elpased: 1.918
batch start
#iterations: 70
currently lose_sum: 95.37853044271469
time_elpased: 1.9
batch start
#iterations: 71
currently lose_sum: 95.7268979549408
time_elpased: 1.889
batch start
#iterations: 72
currently lose_sum: 95.60600954294205
time_elpased: 1.896
batch start
#iterations: 73
currently lose_sum: 95.76613384485245
time_elpased: 1.934
batch start
#iterations: 74
currently lose_sum: 95.87285214662552
time_elpased: 1.882
batch start
#iterations: 75
currently lose_sum: 95.68305051326752
time_elpased: 1.907
batch start
#iterations: 76
currently lose_sum: 95.6989796757698
time_elpased: 1.913
batch start
#iterations: 77
currently lose_sum: 95.81198382377625
time_elpased: 1.882
batch start
#iterations: 78
currently lose_sum: 95.61457008123398
time_elpased: 1.891
batch start
#iterations: 79
currently lose_sum: 95.90539407730103
time_elpased: 1.924
start validation test
0.665979381443
0.630576845502
0.804363486673
0.706946454414
0.665736426802
61.026
batch start
#iterations: 80
currently lose_sum: 95.80462092161179
time_elpased: 1.955
batch start
#iterations: 81
currently lose_sum: 95.78249222040176
time_elpased: 1.916
batch start
#iterations: 82
currently lose_sum: 95.7443026304245
time_elpased: 1.933
batch start
#iterations: 83
currently lose_sum: 95.43500423431396
time_elpased: 1.922
batch start
#iterations: 84
currently lose_sum: 95.69406223297119
time_elpased: 1.915
batch start
#iterations: 85
currently lose_sum: 95.60548257827759
time_elpased: 1.927
batch start
#iterations: 86
currently lose_sum: 95.47324031591415
time_elpased: 1.902
batch start
#iterations: 87
currently lose_sum: 95.89637696743011
time_elpased: 1.907
batch start
#iterations: 88
currently lose_sum: 95.67887806892395
time_elpased: 1.904
batch start
#iterations: 89
currently lose_sum: 95.67356395721436
time_elpased: 1.866
batch start
#iterations: 90
currently lose_sum: 95.6662050485611
time_elpased: 1.874
batch start
#iterations: 91
currently lose_sum: 95.70694410800934
time_elpased: 1.882
batch start
#iterations: 92
currently lose_sum: 95.67125964164734
time_elpased: 1.863
batch start
#iterations: 93
currently lose_sum: 95.6083979010582
time_elpased: 1.874
batch start
#iterations: 94
currently lose_sum: 95.60251760482788
time_elpased: 1.849
batch start
#iterations: 95
currently lose_sum: 95.1548565030098
time_elpased: 1.87
batch start
#iterations: 96
currently lose_sum: 95.50824856758118
time_elpased: 1.893
batch start
#iterations: 97
currently lose_sum: 95.25826984643936
time_elpased: 1.867
batch start
#iterations: 98
currently lose_sum: 95.65772992372513
time_elpased: 1.872
batch start
#iterations: 99
currently lose_sum: 95.56782960891724
time_elpased: 1.868
start validation test
0.662268041237
0.653805034503
0.692291859627
0.672498250525
0.662215329793
60.950
batch start
#iterations: 100
currently lose_sum: 95.68975001573563
time_elpased: 1.926
batch start
#iterations: 101
currently lose_sum: 95.36466825008392
time_elpased: 1.897
batch start
#iterations: 102
currently lose_sum: 95.56284475326538
time_elpased: 1.881
batch start
#iterations: 103
currently lose_sum: 95.60358101129532
time_elpased: 1.867
batch start
#iterations: 104
currently lose_sum: 95.32072484493256
time_elpased: 1.908
batch start
#iterations: 105
currently lose_sum: 95.19747686386108
time_elpased: 1.887
batch start
#iterations: 106
currently lose_sum: 95.38135206699371
time_elpased: 1.897
batch start
#iterations: 107
currently lose_sum: 95.53853982686996
time_elpased: 1.922
batch start
#iterations: 108
currently lose_sum: 95.44566756486893
time_elpased: 1.913
batch start
#iterations: 109
currently lose_sum: 95.409787774086
time_elpased: 1.867
batch start
#iterations: 110
currently lose_sum: 95.34627032279968
time_elpased: 1.912
batch start
#iterations: 111
currently lose_sum: 95.25223106145859
time_elpased: 1.834
batch start
#iterations: 112
currently lose_sum: 95.27994656562805
time_elpased: 1.88
batch start
#iterations: 113
currently lose_sum: 94.95043051242828
time_elpased: 1.879
batch start
#iterations: 114
currently lose_sum: 95.1984965801239
time_elpased: 1.914
batch start
#iterations: 115
currently lose_sum: 95.3281072974205
time_elpased: 1.904
batch start
#iterations: 116
currently lose_sum: 95.0796988606453
time_elpased: 1.867
batch start
#iterations: 117
currently lose_sum: 95.12089258432388
time_elpased: 1.875
batch start
#iterations: 118
currently lose_sum: 95.01298350095749
time_elpased: 1.927
batch start
#iterations: 119
currently lose_sum: 95.32470142841339
time_elpased: 2.028
start validation test
0.669175257732
0.64848321181
0.741381084697
0.691827523288
0.669048489266
60.723
batch start
#iterations: 120
currently lose_sum: 95.07467764616013
time_elpased: 1.929
batch start
#iterations: 121
currently lose_sum: 95.3214425444603
time_elpased: 1.868
batch start
#iterations: 122
currently lose_sum: 95.21219104528427
time_elpased: 1.861
batch start
#iterations: 123
currently lose_sum: 95.5476343035698
time_elpased: 1.85
batch start
#iterations: 124
currently lose_sum: 95.28275442123413
time_elpased: 1.867
batch start
#iterations: 125
currently lose_sum: 95.4253436923027
time_elpased: 1.854
batch start
#iterations: 126
currently lose_sum: 95.55555802583694
time_elpased: 1.843
batch start
#iterations: 127
currently lose_sum: 95.63159656524658
time_elpased: 1.87
batch start
#iterations: 128
currently lose_sum: 95.05245023965836
time_elpased: 1.881
batch start
#iterations: 129
currently lose_sum: 95.64777356386185
time_elpased: 1.87
batch start
#iterations: 130
currently lose_sum: 94.9779394865036
time_elpased: 1.902
batch start
#iterations: 131
currently lose_sum: 95.92039912939072
time_elpased: 1.895
batch start
#iterations: 132
currently lose_sum: 95.2152333855629
time_elpased: 1.891
batch start
#iterations: 133
currently lose_sum: 94.93316012620926
time_elpased: 1.883
batch start
#iterations: 134
currently lose_sum: 95.08458298444748
time_elpased: 1.892
batch start
#iterations: 135
currently lose_sum: 95.12739449739456
time_elpased: 2.016
batch start
#iterations: 136
currently lose_sum: 95.35570138692856
time_elpased: 1.9
batch start
#iterations: 137
currently lose_sum: 95.02937024831772
time_elpased: 1.924
batch start
#iterations: 138
currently lose_sum: 95.39060521125793
time_elpased: 1.885
batch start
#iterations: 139
currently lose_sum: 95.27471357584
time_elpased: 1.919
start validation test
0.664226804124
0.639564270153
0.755274261603
0.69261985655
0.664066956269
60.953
batch start
#iterations: 140
currently lose_sum: 95.20614719390869
time_elpased: 1.953
batch start
#iterations: 141
currently lose_sum: 95.28253346681595
time_elpased: 1.921
batch start
#iterations: 142
currently lose_sum: 95.01977205276489
time_elpased: 1.889
batch start
#iterations: 143
currently lose_sum: 95.21816372871399
time_elpased: 1.908
batch start
#iterations: 144
currently lose_sum: 95.33271920681
time_elpased: 1.952
batch start
#iterations: 145
currently lose_sum: 95.32032197713852
time_elpased: 1.946
batch start
#iterations: 146
currently lose_sum: 94.92431342601776
time_elpased: 1.951
batch start
#iterations: 147
currently lose_sum: 95.42063617706299
time_elpased: 1.935
batch start
#iterations: 148
currently lose_sum: 95.30431044101715
time_elpased: 1.945
batch start
#iterations: 149
currently lose_sum: 95.19987219572067
time_elpased: 1.922
batch start
#iterations: 150
currently lose_sum: 95.10596603155136
time_elpased: 1.944
batch start
#iterations: 151
currently lose_sum: 95.18698859214783
time_elpased: 1.986
batch start
#iterations: 152
currently lose_sum: 95.20990252494812
time_elpased: 1.941
batch start
#iterations: 153
currently lose_sum: 95.53121733665466
time_elpased: 1.969
batch start
#iterations: 154
currently lose_sum: 95.03755968809128
time_elpased: 1.921
batch start
#iterations: 155
currently lose_sum: 95.16214793920517
time_elpased: 1.912
batch start
#iterations: 156
currently lose_sum: 95.15651512145996
time_elpased: 2.033
batch start
#iterations: 157
currently lose_sum: 94.67021387815475
time_elpased: 2.026
batch start
#iterations: 158
currently lose_sum: 94.92651379108429
time_elpased: 1.958
batch start
#iterations: 159
currently lose_sum: 95.0214409828186
time_elpased: 2.011
start validation test
0.667371134021
0.64188836724
0.759802408151
0.69588576276
0.667208856662
60.839
batch start
#iterations: 160
currently lose_sum: 94.78962242603302
time_elpased: 1.999
batch start
#iterations: 161
currently lose_sum: 95.25632286071777
time_elpased: 1.943
batch start
#iterations: 162
currently lose_sum: 95.42756342887878
time_elpased: 1.959
batch start
#iterations: 163
currently lose_sum: 95.33777689933777
time_elpased: 2.018
batch start
#iterations: 164
currently lose_sum: 94.98746645450592
time_elpased: 2.011
batch start
#iterations: 165
currently lose_sum: 95.30182266235352
time_elpased: 2.02
batch start
#iterations: 166
currently lose_sum: 95.00689256191254
time_elpased: 2.039
batch start
#iterations: 167
currently lose_sum: 95.1045201420784
time_elpased: 2.029
batch start
#iterations: 168
currently lose_sum: 95.20262205600739
time_elpased: 2.017
batch start
#iterations: 169
currently lose_sum: 94.80254966020584
time_elpased: 2.012
batch start
#iterations: 170
currently lose_sum: 95.31301909685135
time_elpased: 1.939
batch start
#iterations: 171
currently lose_sum: 95.37819409370422
time_elpased: 2.075
batch start
#iterations: 172
currently lose_sum: 95.32748609781265
time_elpased: 1.995
batch start
#iterations: 173
currently lose_sum: 95.28771030902863
time_elpased: 2.117
batch start
#iterations: 174
currently lose_sum: 94.78177154064178
time_elpased: 1.989
batch start
#iterations: 175
currently lose_sum: 94.82769829034805
time_elpased: 2.017
batch start
#iterations: 176
currently lose_sum: 94.84634017944336
time_elpased: 1.993
batch start
#iterations: 177
currently lose_sum: 95.13580697774887
time_elpased: 1.893
batch start
#iterations: 178
currently lose_sum: 94.72602665424347
time_elpased: 1.964
batch start
#iterations: 179
currently lose_sum: 94.81476390361786
time_elpased: 2.02
start validation test
0.659845360825
0.659146590445
0.664505505815
0.661815200123
0.659837179221
60.823
batch start
#iterations: 180
currently lose_sum: 94.95762187242508
time_elpased: 1.979
batch start
#iterations: 181
currently lose_sum: 94.43362474441528
time_elpased: 1.99
batch start
#iterations: 182
currently lose_sum: 95.19438207149506
time_elpased: 1.943
batch start
#iterations: 183
currently lose_sum: 94.7293558716774
time_elpased: 1.921
batch start
#iterations: 184
currently lose_sum: 95.23617017269135
time_elpased: 1.989
batch start
#iterations: 185
currently lose_sum: 94.58837604522705
time_elpased: 1.986
batch start
#iterations: 186
currently lose_sum: 95.40271323919296
time_elpased: 2.029
batch start
#iterations: 187
currently lose_sum: 95.13432371616364
time_elpased: 1.994
batch start
#iterations: 188
currently lose_sum: 95.06183463335037
time_elpased: 2.027
batch start
#iterations: 189
currently lose_sum: 94.95498603582382
time_elpased: 2.057
batch start
#iterations: 190
currently lose_sum: 95.08167678117752
time_elpased: 2.01
batch start
#iterations: 191
currently lose_sum: 95.18327504396439
time_elpased: 1.998
batch start
#iterations: 192
currently lose_sum: 95.0511702299118
time_elpased: 1.925
batch start
#iterations: 193
currently lose_sum: 94.8924994468689
time_elpased: 2.025
batch start
#iterations: 194
currently lose_sum: 94.44583034515381
time_elpased: 2.003
batch start
#iterations: 195
currently lose_sum: 95.07874989509583
time_elpased: 1.926
batch start
#iterations: 196
currently lose_sum: 94.96552276611328
time_elpased: 1.877
batch start
#iterations: 197
currently lose_sum: 95.26448911428452
time_elpased: 1.834
batch start
#iterations: 198
currently lose_sum: 94.96719962358475
time_elpased: 1.88
batch start
#iterations: 199
currently lose_sum: 94.72678565979004
time_elpased: 1.886
start validation test
0.668350515464
0.645124215366
0.750951939899
0.694027011604
0.668205495923
60.525
batch start
#iterations: 200
currently lose_sum: 95.05611497163773
time_elpased: 1.907
batch start
#iterations: 201
currently lose_sum: 94.95122742652893
time_elpased: 1.882
batch start
#iterations: 202
currently lose_sum: 94.74154394865036
time_elpased: 1.914
batch start
#iterations: 203
currently lose_sum: 95.03272515535355
time_elpased: 1.926
batch start
#iterations: 204
currently lose_sum: 94.95075154304504
time_elpased: 1.892
batch start
#iterations: 205
currently lose_sum: 94.68606179952621
time_elpased: 1.959
batch start
#iterations: 206
currently lose_sum: 94.90297019481659
time_elpased: 1.984
batch start
#iterations: 207
currently lose_sum: 95.01050686836243
time_elpased: 2.008
batch start
#iterations: 208
currently lose_sum: 95.06900990009308
time_elpased: 1.943
batch start
#iterations: 209
currently lose_sum: 94.9187262058258
time_elpased: 1.994
batch start
#iterations: 210
currently lose_sum: 94.60436153411865
time_elpased: 1.987
batch start
#iterations: 211
currently lose_sum: 95.10623288154602
time_elpased: 1.921
batch start
#iterations: 212
currently lose_sum: 94.88199466466904
time_elpased: 1.824
batch start
#iterations: 213
currently lose_sum: 94.90307021141052
time_elpased: 1.858
batch start
#iterations: 214
currently lose_sum: 94.64698123931885
time_elpased: 1.888
batch start
#iterations: 215
currently lose_sum: 94.6137832403183
time_elpased: 1.931
batch start
#iterations: 216
currently lose_sum: 94.7389862537384
time_elpased: 1.998
batch start
#iterations: 217
currently lose_sum: 94.9920625090599
time_elpased: 1.965
batch start
#iterations: 218
currently lose_sum: 94.46656930446625
time_elpased: 1.984
batch start
#iterations: 219
currently lose_sum: 95.25361400842667
time_elpased: 1.939
start validation test
0.660463917526
0.655257936508
0.679736544201
0.667272819114
0.660430081457
60.917
batch start
#iterations: 220
currently lose_sum: 94.8687915802002
time_elpased: 1.937
batch start
#iterations: 221
currently lose_sum: 94.94601595401764
time_elpased: 1.926
batch start
#iterations: 222
currently lose_sum: 94.87973219156265
time_elpased: 1.974
batch start
#iterations: 223
currently lose_sum: 94.48365759849548
time_elpased: 2.022
batch start
#iterations: 224
currently lose_sum: 94.24284267425537
time_elpased: 1.98
batch start
#iterations: 225
currently lose_sum: 94.88637316226959
time_elpased: 1.977
batch start
#iterations: 226
currently lose_sum: 94.8491957783699
time_elpased: 1.977
batch start
#iterations: 227
currently lose_sum: 94.94147139787674
time_elpased: 1.931
batch start
#iterations: 228
currently lose_sum: 94.8085025548935
time_elpased: 1.938
batch start
#iterations: 229
currently lose_sum: 94.91145658493042
time_elpased: 1.969
batch start
#iterations: 230
currently lose_sum: 94.77643233537674
time_elpased: 1.952
batch start
#iterations: 231
currently lose_sum: 94.5528239607811
time_elpased: 1.967
batch start
#iterations: 232
currently lose_sum: 94.56688368320465
time_elpased: 1.93
batch start
#iterations: 233
currently lose_sum: 95.04255717992783
time_elpased: 1.973
batch start
#iterations: 234
currently lose_sum: 94.82798683643341
time_elpased: 1.929
batch start
#iterations: 235
currently lose_sum: 94.52447807788849
time_elpased: 1.964
batch start
#iterations: 236
currently lose_sum: 95.012919485569
time_elpased: 1.978
batch start
#iterations: 237
currently lose_sum: 94.88386380672455
time_elpased: 2.03
batch start
#iterations: 238
currently lose_sum: 94.91968405246735
time_elpased: 1.942
batch start
#iterations: 239
currently lose_sum: 95.12728238105774
time_elpased: 1.976
start validation test
0.668453608247
0.63602484472
0.790367397345
0.704845814978
0.668239569787
60.434
batch start
#iterations: 240
currently lose_sum: 94.50063771009445
time_elpased: 1.942
batch start
#iterations: 241
currently lose_sum: 95.066122174263
time_elpased: 1.944
batch start
#iterations: 242
currently lose_sum: 94.6279827952385
time_elpased: 1.955
batch start
#iterations: 243
currently lose_sum: 94.7854853272438
time_elpased: 1.997
batch start
#iterations: 244
currently lose_sum: 94.95853316783905
time_elpased: 1.978
batch start
#iterations: 245
currently lose_sum: 94.62130904197693
time_elpased: 1.996
batch start
#iterations: 246
currently lose_sum: 95.0112983584404
time_elpased: 2.01
batch start
#iterations: 247
currently lose_sum: 94.3244309425354
time_elpased: 1.92
batch start
#iterations: 248
currently lose_sum: 94.70562314987183
time_elpased: 1.97
batch start
#iterations: 249
currently lose_sum: 94.64279425144196
time_elpased: 1.93
batch start
#iterations: 250
currently lose_sum: 94.32042270898819
time_elpased: 1.945
batch start
#iterations: 251
currently lose_sum: 94.61518663167953
time_elpased: 1.974
batch start
#iterations: 252
currently lose_sum: 94.98343735933304
time_elpased: 1.956
batch start
#iterations: 253
currently lose_sum: 94.49278712272644
time_elpased: 1.984
batch start
#iterations: 254
currently lose_sum: 94.59003907442093
time_elpased: 1.93
batch start
#iterations: 255
currently lose_sum: 94.87537091970444
time_elpased: 1.992
batch start
#iterations: 256
currently lose_sum: 94.50604218244553
time_elpased: 1.967
batch start
#iterations: 257
currently lose_sum: 94.72473514080048
time_elpased: 1.953
batch start
#iterations: 258
currently lose_sum: 94.79386061429977
time_elpased: 1.968
batch start
#iterations: 259
currently lose_sum: 94.57919812202454
time_elpased: 1.982
start validation test
0.670670103093
0.645226042939
0.760831532366
0.698276269185
0.670511810797
60.226
batch start
#iterations: 260
currently lose_sum: 94.48828583955765
time_elpased: 1.925
batch start
#iterations: 261
currently lose_sum: 93.91918230056763
time_elpased: 1.941
batch start
#iterations: 262
currently lose_sum: 95.0086464881897
time_elpased: 1.96
batch start
#iterations: 263
currently lose_sum: 94.92128926515579
time_elpased: 1.977
batch start
#iterations: 264
currently lose_sum: 94.70019847154617
time_elpased: 1.965
batch start
#iterations: 265
currently lose_sum: 94.6192781329155
time_elpased: 1.915
batch start
#iterations: 266
currently lose_sum: 94.70328539609909
time_elpased: 1.906
batch start
#iterations: 267
currently lose_sum: 94.71777456998825
time_elpased: 1.901
batch start
#iterations: 268
currently lose_sum: 94.76319587230682
time_elpased: 1.934
batch start
#iterations: 269
currently lose_sum: 94.5142794251442
time_elpased: 1.954
batch start
#iterations: 270
currently lose_sum: 94.62302368879318
time_elpased: 1.933
batch start
#iterations: 271
currently lose_sum: 94.71434915065765
time_elpased: 1.982
batch start
#iterations: 272
currently lose_sum: 94.3390445113182
time_elpased: 1.988
batch start
#iterations: 273
currently lose_sum: 94.61079144477844
time_elpased: 1.902
batch start
#iterations: 274
currently lose_sum: 94.67343163490295
time_elpased: 1.927
batch start
#iterations: 275
currently lose_sum: 94.41800558567047
time_elpased: 1.909
batch start
#iterations: 276
currently lose_sum: 94.80712789297104
time_elpased: 1.895
batch start
#iterations: 277
currently lose_sum: 94.33817797899246
time_elpased: 1.864
batch start
#iterations: 278
currently lose_sum: 94.43927770853043
time_elpased: 1.943
batch start
#iterations: 279
currently lose_sum: 94.79126274585724
time_elpased: 1.909
start validation test
0.669793814433
0.632112361344
0.815169290933
0.712064005753
0.669538585361
60.324
batch start
#iterations: 280
currently lose_sum: 94.55195671319962
time_elpased: 1.867
batch start
#iterations: 281
currently lose_sum: 94.43882495164871
time_elpased: 1.856
batch start
#iterations: 282
currently lose_sum: 94.69313889741898
time_elpased: 1.855
batch start
#iterations: 283
currently lose_sum: 94.84277212619781
time_elpased: 1.9
batch start
#iterations: 284
currently lose_sum: 94.49380457401276
time_elpased: 1.879
batch start
#iterations: 285
currently lose_sum: 94.4625386595726
time_elpased: 1.84
batch start
#iterations: 286
currently lose_sum: 94.46567279100418
time_elpased: 1.858
batch start
#iterations: 287
currently lose_sum: 94.43184560537338
time_elpased: 1.891
batch start
#iterations: 288
currently lose_sum: 94.64784568548203
time_elpased: 1.829
batch start
#iterations: 289
currently lose_sum: 94.1803428530693
time_elpased: 1.826
batch start
#iterations: 290
currently lose_sum: 94.60950750112534
time_elpased: 1.864
batch start
#iterations: 291
currently lose_sum: 94.86359822750092
time_elpased: 1.822
batch start
#iterations: 292
currently lose_sum: 94.7725203037262
time_elpased: 1.867
batch start
#iterations: 293
currently lose_sum: 94.40223664045334
time_elpased: 1.83
batch start
#iterations: 294
currently lose_sum: 94.67654371261597
time_elpased: 1.828
batch start
#iterations: 295
currently lose_sum: 94.92024421691895
time_elpased: 1.866
batch start
#iterations: 296
currently lose_sum: 94.73563796281815
time_elpased: 1.876
batch start
#iterations: 297
currently lose_sum: 94.72171777486801
time_elpased: 1.877
batch start
#iterations: 298
currently lose_sum: 94.80339306592941
time_elpased: 1.888
batch start
#iterations: 299
currently lose_sum: 94.62565767765045
time_elpased: 1.944
start validation test
0.667835051546
0.644656589764
0.750540290213
0.693580599144
0.667689849744
60.607
batch start
#iterations: 300
currently lose_sum: 94.39494556188583
time_elpased: 2.022
batch start
#iterations: 301
currently lose_sum: 94.98460745811462
time_elpased: 1.98
batch start
#iterations: 302
currently lose_sum: 94.25401532649994
time_elpased: 1.861
batch start
#iterations: 303
currently lose_sum: 94.53080171346664
time_elpased: 1.944
batch start
#iterations: 304
currently lose_sum: 94.52232277393341
time_elpased: 1.993
batch start
#iterations: 305
currently lose_sum: 94.3107260465622
time_elpased: 1.966
batch start
#iterations: 306
currently lose_sum: 94.46752792596817
time_elpased: 1.877
batch start
#iterations: 307
currently lose_sum: 94.76758259534836
time_elpased: 1.872
batch start
#iterations: 308
currently lose_sum: 94.08471357822418
time_elpased: 1.871
batch start
#iterations: 309
currently lose_sum: 94.41385823488235
time_elpased: 1.951
batch start
#iterations: 310
currently lose_sum: 94.95057314634323
time_elpased: 1.996
batch start
#iterations: 311
currently lose_sum: 94.48915940523148
time_elpased: 2.024
batch start
#iterations: 312
currently lose_sum: 94.3783273100853
time_elpased: 1.964
batch start
#iterations: 313
currently lose_sum: 94.60257929563522
time_elpased: 2.079
batch start
#iterations: 314
currently lose_sum: 94.54501050710678
time_elpased: 2.206
batch start
#iterations: 315
currently lose_sum: 94.25644046068192
time_elpased: 2.145
batch start
#iterations: 316
currently lose_sum: 94.36087703704834
time_elpased: 1.932
batch start
#iterations: 317
currently lose_sum: 94.28262507915497
time_elpased: 1.973
batch start
#iterations: 318
currently lose_sum: 94.64018487930298
time_elpased: 1.998
batch start
#iterations: 319
currently lose_sum: 94.8977620601654
time_elpased: 2.001
start validation test
0.667216494845
0.64448382809
0.748482041782
0.692600704695
0.667073820643
60.730
batch start
#iterations: 320
currently lose_sum: 94.2370263338089
time_elpased: 2.005
batch start
#iterations: 321
currently lose_sum: 94.52503579854965
time_elpased: 1.981
batch start
#iterations: 322
currently lose_sum: 94.77136904001236
time_elpased: 1.93
batch start
#iterations: 323
currently lose_sum: 94.76241505146027
time_elpased: 1.925
batch start
#iterations: 324
currently lose_sum: 94.36796629428864
time_elpased: 1.968
batch start
#iterations: 325
currently lose_sum: 94.62465715408325
time_elpased: 1.925
batch start
#iterations: 326
currently lose_sum: 94.30616456270218
time_elpased: 1.983
batch start
#iterations: 327
currently lose_sum: 94.64632672071457
time_elpased: 1.958
batch start
#iterations: 328
currently lose_sum: 94.5109993815422
time_elpased: 2.001
batch start
#iterations: 329
currently lose_sum: 94.14187335968018
time_elpased: 1.875
batch start
#iterations: 330
currently lose_sum: 94.89973837137222
time_elpased: 1.961
batch start
#iterations: 331
currently lose_sum: 94.35955536365509
time_elpased: 1.954
batch start
#iterations: 332
currently lose_sum: 94.0166944861412
time_elpased: 1.956
batch start
#iterations: 333
currently lose_sum: 94.46430683135986
time_elpased: 1.959
batch start
#iterations: 334
currently lose_sum: 94.02157473564148
time_elpased: 2.004
batch start
#iterations: 335
currently lose_sum: 94.4646133184433
time_elpased: 1.993
batch start
#iterations: 336
currently lose_sum: 94.33387100696564
time_elpased: 1.924
batch start
#iterations: 337
currently lose_sum: 94.70431280136108
time_elpased: 2.004
batch start
#iterations: 338
currently lose_sum: 94.38206833600998
time_elpased: 2.023
batch start
#iterations: 339
currently lose_sum: 94.63766437768936
time_elpased: 1.98
start validation test
0.665051546392
0.643257676903
0.743748070392
0.689862542955
0.664913382506
60.576
batch start
#iterations: 340
currently lose_sum: 94.79724299907684
time_elpased: 2.009
batch start
#iterations: 341
currently lose_sum: 94.65845847129822
time_elpased: 1.956
batch start
#iterations: 342
currently lose_sum: 94.802510201931
time_elpased: 1.972
batch start
#iterations: 343
currently lose_sum: 94.49154043197632
time_elpased: 1.88
batch start
#iterations: 344
currently lose_sum: 94.12729549407959
time_elpased: 1.92
batch start
#iterations: 345
currently lose_sum: 94.60040092468262
time_elpased: 1.822
batch start
#iterations: 346
currently lose_sum: 94.25511449575424
time_elpased: 1.879
batch start
#iterations: 347
currently lose_sum: 94.65113961696625
time_elpased: 1.882
batch start
#iterations: 348
currently lose_sum: 94.62253475189209
time_elpased: 1.88
batch start
#iterations: 349
currently lose_sum: 94.97743064165115
time_elpased: 1.926
batch start
#iterations: 350
currently lose_sum: 94.43347871303558
time_elpased: 1.913
batch start
#iterations: 351
currently lose_sum: 94.42391854524612
time_elpased: 1.867
batch start
#iterations: 352
currently lose_sum: 94.5573341846466
time_elpased: 1.886
batch start
#iterations: 353
currently lose_sum: 94.06806010007858
time_elpased: 1.979
batch start
#iterations: 354
currently lose_sum: 94.62476259469986
time_elpased: 1.99
batch start
#iterations: 355
currently lose_sum: 94.29797661304474
time_elpased: 1.913
batch start
#iterations: 356
currently lose_sum: 94.93504917621613
time_elpased: 1.904
batch start
#iterations: 357
currently lose_sum: 94.48162978887558
time_elpased: 2.015
batch start
#iterations: 358
currently lose_sum: 94.59116166830063
time_elpased: 2.002
batch start
#iterations: 359
currently lose_sum: 94.3841683268547
time_elpased: 1.952
start validation test
0.664896907216
0.646554866934
0.730060718329
0.685775049543
0.664782502095
60.614
batch start
#iterations: 360
currently lose_sum: 94.78825795650482
time_elpased: 1.944
batch start
#iterations: 361
currently lose_sum: 94.65735685825348
time_elpased: 1.929
batch start
#iterations: 362
currently lose_sum: 94.51701837778091
time_elpased: 1.953
batch start
#iterations: 363
currently lose_sum: 94.22052246332169
time_elpased: 1.973
batch start
#iterations: 364
currently lose_sum: 94.35693317651749
time_elpased: 1.972
batch start
#iterations: 365
currently lose_sum: 94.96921414136887
time_elpased: 1.999
batch start
#iterations: 366
currently lose_sum: 94.57615715265274
time_elpased: 1.894
batch start
#iterations: 367
currently lose_sum: 94.50469207763672
time_elpased: 2.003
batch start
#iterations: 368
currently lose_sum: 94.57517486810684
time_elpased: 1.903
batch start
#iterations: 369
currently lose_sum: 94.72615492343903
time_elpased: 1.848
batch start
#iterations: 370
currently lose_sum: 94.50883775949478
time_elpased: 1.969
batch start
#iterations: 371
currently lose_sum: 94.47106313705444
time_elpased: 1.977
batch start
#iterations: 372
currently lose_sum: 93.81969046592712
time_elpased: 2.116
batch start
#iterations: 373
currently lose_sum: 94.78293186426163
time_elpased: 1.932
batch start
#iterations: 374
currently lose_sum: 94.9074097275734
time_elpased: 1.951
batch start
#iterations: 375
currently lose_sum: 94.01890975236893
time_elpased: 1.975
batch start
#iterations: 376
currently lose_sum: 94.77253222465515
time_elpased: 1.971
batch start
#iterations: 377
currently lose_sum: 94.64712333679199
time_elpased: 1.971
batch start
#iterations: 378
currently lose_sum: 94.56315648555756
time_elpased: 2.024
batch start
#iterations: 379
currently lose_sum: 94.54254615306854
time_elpased: 2.033
start validation test
0.666391752577
0.654863033311
0.706082124112
0.67950876498
0.666322070008
60.742
batch start
#iterations: 380
currently lose_sum: 94.76405054330826
time_elpased: 1.959
batch start
#iterations: 381
currently lose_sum: 94.21542674303055
time_elpased: 1.93
batch start
#iterations: 382
currently lose_sum: 94.70102351903915
time_elpased: 1.915
batch start
#iterations: 383
currently lose_sum: 94.36289095878601
time_elpased: 1.981
batch start
#iterations: 384
currently lose_sum: 94.77675276994705
time_elpased: 2.04
batch start
#iterations: 385
currently lose_sum: 94.73018169403076
time_elpased: 1.986
batch start
#iterations: 386
currently lose_sum: 94.61467653512955
time_elpased: 1.951
batch start
#iterations: 387
currently lose_sum: 94.9161022901535
time_elpased: 1.939
batch start
#iterations: 388
currently lose_sum: 94.73105269670486
time_elpased: 1.939
batch start
#iterations: 389
currently lose_sum: 94.3436958193779
time_elpased: 1.899
batch start
#iterations: 390
currently lose_sum: 94.4860211610794
time_elpased: 1.977
batch start
#iterations: 391
currently lose_sum: 94.66860514879227
time_elpased: 1.956
batch start
#iterations: 392
currently lose_sum: 94.46618396043777
time_elpased: 2.17
batch start
#iterations: 393
currently lose_sum: 94.33536505699158
time_elpased: 1.958
batch start
#iterations: 394
currently lose_sum: 94.3374382853508
time_elpased: 2.005
batch start
#iterations: 395
currently lose_sum: 94.32598495483398
time_elpased: 1.93
batch start
#iterations: 396
currently lose_sum: 94.08422762155533
time_elpased: 1.865
batch start
#iterations: 397
currently lose_sum: 94.4404291510582
time_elpased: 1.912
batch start
#iterations: 398
currently lose_sum: 94.3995954990387
time_elpased: 1.866
batch start
#iterations: 399
currently lose_sum: 94.63722759485245
time_elpased: 1.832
start validation test
0.664639175258
0.650850324157
0.712874343933
0.680451866405
0.664554490979
60.589
acc: 0.669
pre: 0.645
rec: 0.755
F1: 0.696
auc: 0.669
