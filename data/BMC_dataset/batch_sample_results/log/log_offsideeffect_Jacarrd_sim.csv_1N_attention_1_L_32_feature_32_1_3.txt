start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.79422265291214
time_elpased: 2.08
batch start
#iterations: 1
currently lose_sum: 100.01970851421356
time_elpased: 1.915
batch start
#iterations: 2
currently lose_sum: 99.63312774896622
time_elpased: 1.918
batch start
#iterations: 3
currently lose_sum: 99.42929935455322
time_elpased: 1.866
batch start
#iterations: 4
currently lose_sum: 99.21240103244781
time_elpased: 1.805
batch start
#iterations: 5
currently lose_sum: 99.05327415466309
time_elpased: 1.848
batch start
#iterations: 6
currently lose_sum: 98.72241282463074
time_elpased: 1.907
batch start
#iterations: 7
currently lose_sum: 98.51456326246262
time_elpased: 1.913
batch start
#iterations: 8
currently lose_sum: 98.5091278553009
time_elpased: 1.87
batch start
#iterations: 9
currently lose_sum: 98.31527262926102
time_elpased: 1.878
batch start
#iterations: 10
currently lose_sum: 98.05257451534271
time_elpased: 1.906
batch start
#iterations: 11
currently lose_sum: 98.01435941457748
time_elpased: 1.914
batch start
#iterations: 12
currently lose_sum: 97.67581564188004
time_elpased: 1.842
batch start
#iterations: 13
currently lose_sum: 97.90889900922775
time_elpased: 1.877
batch start
#iterations: 14
currently lose_sum: 97.28711700439453
time_elpased: 1.857
batch start
#iterations: 15
currently lose_sum: 97.5699268579483
time_elpased: 1.895
batch start
#iterations: 16
currently lose_sum: 97.35621058940887
time_elpased: 1.85
batch start
#iterations: 17
currently lose_sum: 97.39754980802536
time_elpased: 1.865
batch start
#iterations: 18
currently lose_sum: 97.17123585939407
time_elpased: 1.873
batch start
#iterations: 19
currently lose_sum: 97.11857581138611
time_elpased: 1.864
start validation test
0.639381443299
0.652534768955
0.598806093042
0.624516960069
0.639448482292
62.675
batch start
#iterations: 20
currently lose_sum: 97.02207046747208
time_elpased: 1.925
batch start
#iterations: 21
currently lose_sum: 96.77067708969116
time_elpased: 1.917
batch start
#iterations: 22
currently lose_sum: 96.85703015327454
time_elpased: 1.88
batch start
#iterations: 23
currently lose_sum: 96.70501691102982
time_elpased: 1.872
batch start
#iterations: 24
currently lose_sum: 96.7608608007431
time_elpased: 1.904
batch start
#iterations: 25
currently lose_sum: 96.5192523598671
time_elpased: 1.839
batch start
#iterations: 26
currently lose_sum: 96.71967160701752
time_elpased: 1.884
batch start
#iterations: 27
currently lose_sum: 96.68777585029602
time_elpased: 1.88
batch start
#iterations: 28
currently lose_sum: 96.50097620487213
time_elpased: 1.877
batch start
#iterations: 29
currently lose_sum: 96.70314347743988
time_elpased: 1.894
batch start
#iterations: 30
currently lose_sum: 96.58772218227386
time_elpased: 1.889
batch start
#iterations: 31
currently lose_sum: 96.26765394210815
time_elpased: 1.895
batch start
#iterations: 32
currently lose_sum: 96.41250985860825
time_elpased: 1.868
batch start
#iterations: 33
currently lose_sum: 96.35828465223312
time_elpased: 1.947
batch start
#iterations: 34
currently lose_sum: 96.49135839939117
time_elpased: 1.911
batch start
#iterations: 35
currently lose_sum: 96.42946517467499
time_elpased: 1.953
batch start
#iterations: 36
currently lose_sum: 96.32947570085526
time_elpased: 1.838
batch start
#iterations: 37
currently lose_sum: 96.31095814704895
time_elpased: 1.864
batch start
#iterations: 38
currently lose_sum: 96.39522302150726
time_elpased: 1.841
batch start
#iterations: 39
currently lose_sum: 96.2855110168457
time_elpased: 1.807
start validation test
0.657886597938
0.640018190086
0.724269246604
0.679542272222
0.657776919873
61.919
batch start
#iterations: 40
currently lose_sum: 96.17581290006638
time_elpased: 1.849
batch start
#iterations: 41
currently lose_sum: 96.41988861560822
time_elpased: 1.854
batch start
#iterations: 42
currently lose_sum: 96.20729380846024
time_elpased: 1.878
batch start
#iterations: 43
currently lose_sum: 96.19778114557266
time_elpased: 1.86
batch start
#iterations: 44
currently lose_sum: 96.38813400268555
time_elpased: 1.854
batch start
#iterations: 45
currently lose_sum: 96.26649743318558
time_elpased: 1.861
batch start
#iterations: 46
currently lose_sum: 95.73269724845886
time_elpased: 1.801
batch start
#iterations: 47
currently lose_sum: 96.14354622364044
time_elpased: 1.849
batch start
#iterations: 48
currently lose_sum: 96.02085262537003
time_elpased: 1.839
batch start
#iterations: 49
currently lose_sum: 96.09777194261551
time_elpased: 1.868
batch start
#iterations: 50
currently lose_sum: 96.13965147733688
time_elpased: 1.854
batch start
#iterations: 51
currently lose_sum: 95.9232587814331
time_elpased: 1.863
batch start
#iterations: 52
currently lose_sum: 95.94113516807556
time_elpased: 1.822
batch start
#iterations: 53
currently lose_sum: 95.46244484186172
time_elpased: 1.87
batch start
#iterations: 54
currently lose_sum: 96.03282153606415
time_elpased: 1.906
batch start
#iterations: 55
currently lose_sum: 96.02122402191162
time_elpased: 1.935
batch start
#iterations: 56
currently lose_sum: 96.32283937931061
time_elpased: 1.847
batch start
#iterations: 57
currently lose_sum: 95.83908653259277
time_elpased: 1.857
batch start
#iterations: 58
currently lose_sum: 96.1978629231453
time_elpased: 1.835
batch start
#iterations: 59
currently lose_sum: 95.87305760383606
time_elpased: 1.842
start validation test
0.628608247423
0.664397014535
0.522231370935
0.584798017634
0.628784004344
62.381
batch start
#iterations: 60
currently lose_sum: 95.64505285024643
time_elpased: 1.814
batch start
#iterations: 61
currently lose_sum: 95.9188306927681
time_elpased: 1.861
batch start
#iterations: 62
currently lose_sum: 96.09789907932281
time_elpased: 1.855
batch start
#iterations: 63
currently lose_sum: 95.89117634296417
time_elpased: 1.902
batch start
#iterations: 64
currently lose_sum: 95.85817801952362
time_elpased: 1.934
batch start
#iterations: 65
currently lose_sum: 95.71411806344986
time_elpased: 1.915
batch start
#iterations: 66
currently lose_sum: 95.77725428342819
time_elpased: 1.904
batch start
#iterations: 67
currently lose_sum: 95.78326714038849
time_elpased: 1.861
batch start
#iterations: 68
currently lose_sum: 95.79919975996017
time_elpased: 1.9
batch start
#iterations: 69
currently lose_sum: 95.6907788515091
time_elpased: 1.963
batch start
#iterations: 70
currently lose_sum: 96.01247149705887
time_elpased: 1.912
batch start
#iterations: 71
currently lose_sum: 95.59797406196594
time_elpased: 1.887
batch start
#iterations: 72
currently lose_sum: 95.80506128072739
time_elpased: 1.926
batch start
#iterations: 73
currently lose_sum: 95.68304127454758
time_elpased: 1.894
batch start
#iterations: 74
currently lose_sum: 95.69359630346298
time_elpased: 1.839
batch start
#iterations: 75
currently lose_sum: 95.54223722219467
time_elpased: 1.865
batch start
#iterations: 76
currently lose_sum: 95.66216385364532
time_elpased: 1.801
batch start
#iterations: 77
currently lose_sum: 95.48030030727386
time_elpased: 1.811
batch start
#iterations: 78
currently lose_sum: 96.01924043893814
time_elpased: 1.782
batch start
#iterations: 79
currently lose_sum: 95.50789326429367
time_elpased: 1.787
start validation test
0.66412371134
0.635938827528
0.77037875669
0.696732756213
0.66394815571
61.144
batch start
#iterations: 80
currently lose_sum: 95.50920379161835
time_elpased: 1.903
batch start
#iterations: 81
currently lose_sum: 95.71250468492508
time_elpased: 1.889
batch start
#iterations: 82
currently lose_sum: 95.46370476484299
time_elpased: 1.827
batch start
#iterations: 83
currently lose_sum: 95.83727890253067
time_elpased: 1.861
batch start
#iterations: 84
currently lose_sum: 95.70931541919708
time_elpased: 1.899
batch start
#iterations: 85
currently lose_sum: 95.60456508398056
time_elpased: 1.862
batch start
#iterations: 86
currently lose_sum: 95.75475919246674
time_elpased: 1.817
batch start
#iterations: 87
currently lose_sum: 95.05516290664673
time_elpased: 1.794
batch start
#iterations: 88
currently lose_sum: 95.53166103363037
time_elpased: 1.775
batch start
#iterations: 89
currently lose_sum: 95.58695882558823
time_elpased: 1.864
batch start
#iterations: 90
currently lose_sum: 95.65694987773895
time_elpased: 1.808
batch start
#iterations: 91
currently lose_sum: 95.59235459566116
time_elpased: 1.773
batch start
#iterations: 92
currently lose_sum: 95.7035413980484
time_elpased: 1.877
batch start
#iterations: 93
currently lose_sum: 95.3623663187027
time_elpased: 1.982
batch start
#iterations: 94
currently lose_sum: 95.83780497312546
time_elpased: 1.892
batch start
#iterations: 95
currently lose_sum: 95.62260901927948
time_elpased: 1.84
batch start
#iterations: 96
currently lose_sum: 95.35326027870178
time_elpased: 1.846
batch start
#iterations: 97
currently lose_sum: 95.54041582345963
time_elpased: 1.917
batch start
#iterations: 98
currently lose_sum: 95.3319149017334
time_elpased: 1.933
batch start
#iterations: 99
currently lose_sum: 95.4262643456459
time_elpased: 1.957
start validation test
0.662371134021
0.654953014879
0.688657883903
0.671382701184
0.662327702794
61.361
batch start
#iterations: 100
currently lose_sum: 95.62143522500992
time_elpased: 2.01
batch start
#iterations: 101
currently lose_sum: 95.59649062156677
time_elpased: 1.841
batch start
#iterations: 102
currently lose_sum: 95.65594798326492
time_elpased: 1.851
batch start
#iterations: 103
currently lose_sum: 95.6509895324707
time_elpased: 1.901
batch start
#iterations: 104
currently lose_sum: 95.3119609951973
time_elpased: 1.792
batch start
#iterations: 105
currently lose_sum: 95.2578039765358
time_elpased: 1.825
batch start
#iterations: 106
currently lose_sum: 95.41843181848526
time_elpased: 1.822
batch start
#iterations: 107
currently lose_sum: 95.30061382055283
time_elpased: 1.795
batch start
#iterations: 108
currently lose_sum: 95.45480853319168
time_elpased: 1.816
batch start
#iterations: 109
currently lose_sum: 95.31325197219849
time_elpased: 1.833
batch start
#iterations: 110
currently lose_sum: 95.74861282110214
time_elpased: 1.817
batch start
#iterations: 111
currently lose_sum: 94.98268842697144
time_elpased: 2.0
batch start
#iterations: 112
currently lose_sum: 95.36955398321152
time_elpased: 2.178
batch start
#iterations: 113
currently lose_sum: 94.85042959451675
time_elpased: 2.208
batch start
#iterations: 114
currently lose_sum: 95.62961959838867
time_elpased: 2.19
batch start
#iterations: 115
currently lose_sum: 95.2387466430664
time_elpased: 2.184
batch start
#iterations: 116
currently lose_sum: 95.68342012166977
time_elpased: 2.443
batch start
#iterations: 117
currently lose_sum: 95.04710382223129
time_elpased: 2.42
batch start
#iterations: 118
currently lose_sum: 95.65125781297684
time_elpased: 2.511
batch start
#iterations: 119
currently lose_sum: 95.41516470909119
time_elpased: 2.477
start validation test
0.658969072165
0.637460092231
0.739810621655
0.684832317073
0.658835504962
61.415
batch start
#iterations: 120
currently lose_sum: 95.16978669166565
time_elpased: 2.525
batch start
#iterations: 121
currently lose_sum: 94.92225581407547
time_elpased: 2.358
batch start
#iterations: 122
currently lose_sum: 95.34947323799133
time_elpased: 2.326
batch start
#iterations: 123
currently lose_sum: 95.43892765045166
time_elpased: 2.351
batch start
#iterations: 124
currently lose_sum: 95.46232330799103
time_elpased: 2.42
batch start
#iterations: 125
currently lose_sum: 95.38893949985504
time_elpased: 2.452
batch start
#iterations: 126
currently lose_sum: 94.94457918405533
time_elpased: 2.333
batch start
#iterations: 127
currently lose_sum: 95.40127557516098
time_elpased: 2.464
batch start
#iterations: 128
currently lose_sum: 95.25047850608826
time_elpased: 2.507
batch start
#iterations: 129
currently lose_sum: 95.13106644153595
time_elpased: 2.501
batch start
#iterations: 130
currently lose_sum: 95.49382537603378
time_elpased: 2.429
batch start
#iterations: 131
currently lose_sum: 95.01805794239044
time_elpased: 2.506
batch start
#iterations: 132
currently lose_sum: 95.47529691457748
time_elpased: 2.518
batch start
#iterations: 133
currently lose_sum: 95.17315644025803
time_elpased: 2.4
batch start
#iterations: 134
currently lose_sum: 95.062490940094
time_elpased: 2.531
batch start
#iterations: 135
currently lose_sum: 95.86522722244263
time_elpased: 2.474
batch start
#iterations: 136
currently lose_sum: 95.38755410909653
time_elpased: 2.533
batch start
#iterations: 137
currently lose_sum: 95.43369603157043
time_elpased: 2.547
batch start
#iterations: 138
currently lose_sum: 95.09560257196426
time_elpased: 2.126
batch start
#iterations: 139
currently lose_sum: 94.87830144166946
time_elpased: 1.852
start validation test
0.658402061856
0.636331538529
0.74197200494
0.685103349964
0.658263986774
60.994
batch start
#iterations: 140
currently lose_sum: 95.20768249034882
time_elpased: 2.007
batch start
#iterations: 141
currently lose_sum: 95.2738538980484
time_elpased: 1.897
batch start
#iterations: 142
currently lose_sum: 95.33022558689117
time_elpased: 1.818
batch start
#iterations: 143
currently lose_sum: 95.40020841360092
time_elpased: 1.806
batch start
#iterations: 144
currently lose_sum: 95.07052886486053
time_elpased: 1.772
batch start
#iterations: 145
currently lose_sum: 95.05391079187393
time_elpased: 1.778
batch start
#iterations: 146
currently lose_sum: 94.83684319257736
time_elpased: 1.757
batch start
#iterations: 147
currently lose_sum: 95.3081818819046
time_elpased: 1.792
batch start
#iterations: 148
currently lose_sum: 95.52078074216843
time_elpased: 1.799
batch start
#iterations: 149
currently lose_sum: 95.13945585489273
time_elpased: 1.785
batch start
#iterations: 150
currently lose_sum: 95.28441888093948
time_elpased: 1.797
batch start
#iterations: 151
currently lose_sum: 95.31011021137238
time_elpased: 1.763
batch start
#iterations: 152
currently lose_sum: 95.21142381429672
time_elpased: 1.788
batch start
#iterations: 153
currently lose_sum: 95.21674507856369
time_elpased: 1.821
batch start
#iterations: 154
currently lose_sum: 95.39102870225906
time_elpased: 1.816
batch start
#iterations: 155
currently lose_sum: 94.74110877513885
time_elpased: 1.796
batch start
#iterations: 156
currently lose_sum: 95.24210947751999
time_elpased: 1.79
batch start
#iterations: 157
currently lose_sum: 94.9200484752655
time_elpased: 1.791
batch start
#iterations: 158
currently lose_sum: 95.44339209794998
time_elpased: 1.796
batch start
#iterations: 159
currently lose_sum: 94.84670233726501
time_elpased: 1.796
start validation test
0.665206185567
0.636703166115
0.772025524907
0.697864818347
0.665029697604
60.927
batch start
#iterations: 160
currently lose_sum: 94.93614757061005
time_elpased: 1.832
batch start
#iterations: 161
currently lose_sum: 95.46762174367905
time_elpased: 1.804
batch start
#iterations: 162
currently lose_sum: 95.27453678846359
time_elpased: 1.775
batch start
#iterations: 163
currently lose_sum: 95.28358942270279
time_elpased: 1.775
batch start
#iterations: 164
currently lose_sum: 95.37653094530106
time_elpased: 1.771
batch start
#iterations: 165
currently lose_sum: 94.9618353843689
time_elpased: 1.796
batch start
#iterations: 166
currently lose_sum: 95.16485041379929
time_elpased: 1.791
batch start
#iterations: 167
currently lose_sum: 94.95569658279419
time_elpased: 1.774
batch start
#iterations: 168
currently lose_sum: 95.355213701725
time_elpased: 1.857
batch start
#iterations: 169
currently lose_sum: 94.98283898830414
time_elpased: 1.778
batch start
#iterations: 170
currently lose_sum: 94.82458186149597
time_elpased: 1.798
batch start
#iterations: 171
currently lose_sum: 94.99135154485703
time_elpased: 1.784
batch start
#iterations: 172
currently lose_sum: 94.78264784812927
time_elpased: 1.792
batch start
#iterations: 173
currently lose_sum: 94.93350464105606
time_elpased: 1.972
batch start
#iterations: 174
currently lose_sum: 95.09980529546738
time_elpased: 1.799
batch start
#iterations: 175
currently lose_sum: 94.88415402173996
time_elpased: 1.799
batch start
#iterations: 176
currently lose_sum: 95.25042760372162
time_elpased: 1.799
batch start
#iterations: 177
currently lose_sum: 95.29835867881775
time_elpased: 1.846
batch start
#iterations: 178
currently lose_sum: 94.93405789136887
time_elpased: 1.795
batch start
#iterations: 179
currently lose_sum: 94.82037484645844
time_elpased: 1.789
start validation test
0.668092783505
0.634778317019
0.794256895842
0.705618799433
0.667884333918
60.674
batch start
#iterations: 180
currently lose_sum: 94.89843475818634
time_elpased: 1.869
batch start
#iterations: 181
currently lose_sum: 95.12373226881027
time_elpased: 1.808
batch start
#iterations: 182
currently lose_sum: 95.30564051866531
time_elpased: 1.808
batch start
#iterations: 183
currently lose_sum: 94.98425894975662
time_elpased: 1.809
batch start
#iterations: 184
currently lose_sum: 94.85668724775314
time_elpased: 1.801
batch start
#iterations: 185
currently lose_sum: 95.1997013092041
time_elpased: 1.802
batch start
#iterations: 186
currently lose_sum: 94.7573504447937
time_elpased: 1.78
batch start
#iterations: 187
currently lose_sum: 95.11023688316345
time_elpased: 1.802
batch start
#iterations: 188
currently lose_sum: 95.11773014068604
time_elpased: 1.797
batch start
#iterations: 189
currently lose_sum: 95.0378093123436
time_elpased: 1.806
batch start
#iterations: 190
currently lose_sum: 95.13720142841339
time_elpased: 1.786
batch start
#iterations: 191
currently lose_sum: 94.88436150550842
time_elpased: 1.821
batch start
#iterations: 192
currently lose_sum: 94.82774329185486
time_elpased: 1.831
batch start
#iterations: 193
currently lose_sum: 94.95919191837311
time_elpased: 1.79
batch start
#iterations: 194
currently lose_sum: 94.91344088315964
time_elpased: 1.786
batch start
#iterations: 195
currently lose_sum: 95.03644347190857
time_elpased: 1.833
batch start
#iterations: 196
currently lose_sum: 94.4243369102478
time_elpased: 1.832
batch start
#iterations: 197
currently lose_sum: 94.99972242116928
time_elpased: 1.821
batch start
#iterations: 198
currently lose_sum: 94.80136978626251
time_elpased: 1.784
batch start
#iterations: 199
currently lose_sum: 94.81271696090698
time_elpased: 1.798
start validation test
0.661340206186
0.639524569807
0.742074927954
0.686993806575
0.661206815485
60.961
batch start
#iterations: 200
currently lose_sum: 95.02086937427521
time_elpased: 1.79
batch start
#iterations: 201
currently lose_sum: 94.64054614305496
time_elpased: 1.793
batch start
#iterations: 202
currently lose_sum: 94.8971249461174
time_elpased: 1.799
batch start
#iterations: 203
currently lose_sum: 94.97891718149185
time_elpased: 1.791
batch start
#iterations: 204
currently lose_sum: 95.2650038599968
time_elpased: 1.814
batch start
#iterations: 205
currently lose_sum: 95.45160043239594
time_elpased: 1.785
batch start
#iterations: 206
currently lose_sum: 94.86754775047302
time_elpased: 1.813
batch start
#iterations: 207
currently lose_sum: 94.95481926202774
time_elpased: 1.793
batch start
#iterations: 208
currently lose_sum: 95.01882404088974
time_elpased: 1.807
batch start
#iterations: 209
currently lose_sum: 95.01161903142929
time_elpased: 1.762
batch start
#iterations: 210
currently lose_sum: 94.81789827346802
time_elpased: 1.736
batch start
#iterations: 211
currently lose_sum: 94.11422353982925
time_elpased: 1.756
batch start
#iterations: 212
currently lose_sum: 94.75665766000748
time_elpased: 1.809
batch start
#iterations: 213
currently lose_sum: 94.99205493927002
time_elpased: 1.737
batch start
#iterations: 214
currently lose_sum: 94.94657278060913
time_elpased: 1.77
batch start
#iterations: 215
currently lose_sum: 94.85183703899384
time_elpased: 1.831
batch start
#iterations: 216
currently lose_sum: 94.44550508260727
time_elpased: 1.808
batch start
#iterations: 217
currently lose_sum: 94.69287461042404
time_elpased: 1.823
batch start
#iterations: 218
currently lose_sum: 94.8159408569336
time_elpased: 1.785
batch start
#iterations: 219
currently lose_sum: 95.10759460926056
time_elpased: 1.769
start validation test
0.66824742268
0.634448270208
0.796521202141
0.706306470749
0.668035487481
60.638
batch start
#iterations: 220
currently lose_sum: 94.96666711568832
time_elpased: 1.822
batch start
#iterations: 221
currently lose_sum: 94.67991733551025
time_elpased: 1.79
batch start
#iterations: 222
currently lose_sum: 94.72390758991241
time_elpased: 1.83
batch start
#iterations: 223
currently lose_sum: 94.17431926727295
time_elpased: 1.743
batch start
#iterations: 224
currently lose_sum: 94.61003196239471
time_elpased: 1.788
batch start
#iterations: 225
currently lose_sum: 94.73205280303955
time_elpased: 1.789
batch start
#iterations: 226
currently lose_sum: 94.79853951931
time_elpased: 1.814
batch start
#iterations: 227
currently lose_sum: 94.52032214403152
time_elpased: 1.762
batch start
#iterations: 228
currently lose_sum: 94.91045266389847
time_elpased: 1.78
batch start
#iterations: 229
currently lose_sum: 94.47058218717575
time_elpased: 1.78
batch start
#iterations: 230
currently lose_sum: 94.82597708702087
time_elpased: 1.8
batch start
#iterations: 231
currently lose_sum: 94.6251066327095
time_elpased: 1.797
batch start
#iterations: 232
currently lose_sum: 95.19234889745712
time_elpased: 1.794
batch start
#iterations: 233
currently lose_sum: 95.08766263723373
time_elpased: 1.794
batch start
#iterations: 234
currently lose_sum: 94.69629752635956
time_elpased: 1.787
batch start
#iterations: 235
currently lose_sum: 95.27535539865494
time_elpased: 1.793
batch start
#iterations: 236
currently lose_sum: 94.89274382591248
time_elpased: 1.807
batch start
#iterations: 237
currently lose_sum: 94.56753408908844
time_elpased: 1.831
batch start
#iterations: 238
currently lose_sum: 94.90603214502335
time_elpased: 1.866
batch start
#iterations: 239
currently lose_sum: 94.96558851003647
time_elpased: 1.935
start validation test
0.665463917526
0.640873362445
0.755249073693
0.693376169328
0.665315573608
60.972
batch start
#iterations: 240
currently lose_sum: 94.87841719388962
time_elpased: 1.892
batch start
#iterations: 241
currently lose_sum: 94.41959911584854
time_elpased: 1.892
batch start
#iterations: 242
currently lose_sum: 94.74255031347275
time_elpased: 1.777
batch start
#iterations: 243
currently lose_sum: 94.63295018672943
time_elpased: 1.791
batch start
#iterations: 244
currently lose_sum: 94.56534212827682
time_elpased: 1.814
batch start
#iterations: 245
currently lose_sum: 95.11453729867935
time_elpased: 1.779
batch start
#iterations: 246
currently lose_sum: 94.82403153181076
time_elpased: 1.8
batch start
#iterations: 247
currently lose_sum: 94.59326958656311
time_elpased: 1.82
batch start
#iterations: 248
currently lose_sum: 94.88889026641846
time_elpased: 1.798
batch start
#iterations: 249
currently lose_sum: 94.72801697254181
time_elpased: 1.767
batch start
#iterations: 250
currently lose_sum: 94.95329636335373
time_elpased: 1.753
batch start
#iterations: 251
currently lose_sum: 94.80252647399902
time_elpased: 1.751
batch start
#iterations: 252
currently lose_sum: 94.85375010967255
time_elpased: 1.789
batch start
#iterations: 253
currently lose_sum: 94.69454890489578
time_elpased: 1.753
batch start
#iterations: 254
currently lose_sum: 94.92321538925171
time_elpased: 1.758
batch start
#iterations: 255
currently lose_sum: 95.02446818351746
time_elpased: 1.793
batch start
#iterations: 256
currently lose_sum: 94.47459328174591
time_elpased: 1.838
batch start
#iterations: 257
currently lose_sum: 94.51069098711014
time_elpased: 1.787
batch start
#iterations: 258
currently lose_sum: 94.67489302158356
time_elpased: 1.79
batch start
#iterations: 259
currently lose_sum: 94.62366020679474
time_elpased: 1.761
start validation test
0.660567010309
0.653766820548
0.685055578427
0.669045584762
0.660526550056
60.913
batch start
#iterations: 260
currently lose_sum: 94.72161030769348
time_elpased: 1.826
batch start
#iterations: 261
currently lose_sum: 94.68713909387589
time_elpased: 1.792
batch start
#iterations: 262
currently lose_sum: 94.66303914785385
time_elpased: 1.834
batch start
#iterations: 263
currently lose_sum: 94.6470205783844
time_elpased: 1.768
batch start
#iterations: 264
currently lose_sum: 94.70194864273071
time_elpased: 1.796
batch start
#iterations: 265
currently lose_sum: 94.91254842281342
time_elpased: 1.81
batch start
#iterations: 266
currently lose_sum: 94.4119034409523
time_elpased: 1.8
batch start
#iterations: 267
currently lose_sum: 94.72254002094269
time_elpased: 1.765
batch start
#iterations: 268
currently lose_sum: 94.70949256420135
time_elpased: 1.764
batch start
#iterations: 269
currently lose_sum: 94.77424770593643
time_elpased: 1.788
batch start
#iterations: 270
currently lose_sum: 94.45621407032013
time_elpased: 1.775
batch start
#iterations: 271
currently lose_sum: 94.5611042380333
time_elpased: 1.779
batch start
#iterations: 272
currently lose_sum: 94.76858258247375
time_elpased: 1.819
batch start
#iterations: 273
currently lose_sum: 94.72656309604645
time_elpased: 1.891
batch start
#iterations: 274
currently lose_sum: 94.77801465988159
time_elpased: 1.864
batch start
#iterations: 275
currently lose_sum: 94.5394127368927
time_elpased: 1.809
batch start
#iterations: 276
currently lose_sum: 94.8145701289177
time_elpased: 1.811
batch start
#iterations: 277
currently lose_sum: 94.19392132759094
time_elpased: 1.771
batch start
#iterations: 278
currently lose_sum: 94.71011424064636
time_elpased: 1.783
batch start
#iterations: 279
currently lose_sum: 94.8439217209816
time_elpased: 1.813
start validation test
0.660515463918
0.645041705283
0.716344174557
0.678825709548
0.660423223173
61.177
batch start
#iterations: 280
currently lose_sum: 94.68020045757294
time_elpased: 1.791
batch start
#iterations: 281
currently lose_sum: 94.28447586297989
time_elpased: 1.791
batch start
#iterations: 282
currently lose_sum: 94.82408833503723
time_elpased: 1.782
batch start
#iterations: 283
currently lose_sum: 94.85823410749435
time_elpased: 1.74
batch start
#iterations: 284
currently lose_sum: 94.05819380283356
time_elpased: 1.777
batch start
#iterations: 285
currently lose_sum: 94.36768037080765
time_elpased: 1.747
batch start
#iterations: 286
currently lose_sum: 94.36211967468262
time_elpased: 1.795
batch start
#iterations: 287
currently lose_sum: 94.5829513669014
time_elpased: 1.771
batch start
#iterations: 288
currently lose_sum: 94.55471748113632
time_elpased: 1.778
batch start
#iterations: 289
currently lose_sum: 94.56194484233856
time_elpased: 1.75
batch start
#iterations: 290
currently lose_sum: 94.73621487617493
time_elpased: 1.79
batch start
#iterations: 291
currently lose_sum: 94.59139752388
time_elpased: 1.784
batch start
#iterations: 292
currently lose_sum: 94.66225701570511
time_elpased: 1.773
batch start
#iterations: 293
currently lose_sum: 94.55350184440613
time_elpased: 1.76
batch start
#iterations: 294
currently lose_sum: 94.63416928052902
time_elpased: 1.788
batch start
#iterations: 295
currently lose_sum: 94.59616607427597
time_elpased: 1.796
batch start
#iterations: 296
currently lose_sum: 94.72846388816833
time_elpased: 1.804
batch start
#iterations: 297
currently lose_sum: 95.07488948106766
time_elpased: 1.799
batch start
#iterations: 298
currently lose_sum: 94.5362948179245
time_elpased: 1.86
batch start
#iterations: 299
currently lose_sum: 94.67418599128723
time_elpased: 1.788
start validation test
0.666391752577
0.629243027888
0.812783038287
0.709332614749
0.666149883456
60.494
batch start
#iterations: 300
currently lose_sum: 94.77028369903564
time_elpased: 1.762
batch start
#iterations: 301
currently lose_sum: 94.41184324026108
time_elpased: 1.755
batch start
#iterations: 302
currently lose_sum: 94.44804257154465
time_elpased: 1.914
batch start
#iterations: 303
currently lose_sum: 94.06336569786072
time_elpased: 1.808
batch start
#iterations: 304
currently lose_sum: 94.00996536016464
time_elpased: 1.782
batch start
#iterations: 305
currently lose_sum: 94.32200837135315
time_elpased: 1.781
batch start
#iterations: 306
currently lose_sum: 94.64152073860168
time_elpased: 1.79
batch start
#iterations: 307
currently lose_sum: 95.02472394704819
time_elpased: 1.775
batch start
#iterations: 308
currently lose_sum: 94.52544748783112
time_elpased: 1.795
batch start
#iterations: 309
currently lose_sum: 94.25076991319656
time_elpased: 1.788
batch start
#iterations: 310
currently lose_sum: 94.40693891048431
time_elpased: 1.813
batch start
#iterations: 311
currently lose_sum: 94.29888170957565
time_elpased: 1.78
batch start
#iterations: 312
currently lose_sum: 94.891341984272
time_elpased: 1.789
batch start
#iterations: 313
currently lose_sum: 94.4347620010376
time_elpased: 1.816
batch start
#iterations: 314
currently lose_sum: 94.57726645469666
time_elpased: 1.789
batch start
#iterations: 315
currently lose_sum: 94.0327257514
time_elpased: 1.824
batch start
#iterations: 316
currently lose_sum: 94.47341865301132
time_elpased: 1.832
batch start
#iterations: 317
currently lose_sum: 94.33205610513687
time_elpased: 1.841
batch start
#iterations: 318
currently lose_sum: 94.26480168104172
time_elpased: 1.829
batch start
#iterations: 319
currently lose_sum: 94.29947525262833
time_elpased: 1.78
start validation test
0.663144329897
0.638533228813
0.754528612598
0.691701655895
0.662993343887
60.618
batch start
#iterations: 320
currently lose_sum: 94.84887999296188
time_elpased: 1.803
batch start
#iterations: 321
currently lose_sum: 94.58321988582611
time_elpased: 1.837
batch start
#iterations: 322
currently lose_sum: 94.42499953508377
time_elpased: 1.79
batch start
#iterations: 323
currently lose_sum: 94.55844855308533
time_elpased: 1.794
batch start
#iterations: 324
currently lose_sum: 94.43473827838898
time_elpased: 1.79
batch start
#iterations: 325
currently lose_sum: 94.47755998373032
time_elpased: 1.799
batch start
#iterations: 326
currently lose_sum: 94.31570243835449
time_elpased: 1.792
batch start
#iterations: 327
currently lose_sum: 94.62891286611557
time_elpased: 1.846
batch start
#iterations: 328
currently lose_sum: 94.45512360334396
time_elpased: 1.812
batch start
#iterations: 329
currently lose_sum: 94.90825879573822
time_elpased: 1.817
batch start
#iterations: 330
currently lose_sum: 94.71702486276627
time_elpased: 1.857
batch start
#iterations: 331
currently lose_sum: 94.60265272855759
time_elpased: 1.77
batch start
#iterations: 332
currently lose_sum: 94.36044692993164
time_elpased: 1.856
batch start
#iterations: 333
currently lose_sum: 94.55743545293808
time_elpased: 1.8
batch start
#iterations: 334
currently lose_sum: 94.63589012622833
time_elpased: 1.837
batch start
#iterations: 335
currently lose_sum: 94.14398640394211
time_elpased: 1.841
batch start
#iterations: 336
currently lose_sum: 94.42423969507217
time_elpased: 1.806
batch start
#iterations: 337
currently lose_sum: 94.5661523938179
time_elpased: 1.836
batch start
#iterations: 338
currently lose_sum: 94.21687173843384
time_elpased: 1.861
batch start
#iterations: 339
currently lose_sum: 94.48320388793945
time_elpased: 1.807
start validation test
0.667525773196
0.638742565845
0.773775216138
0.699804523876
0.667350226822
60.570
batch start
#iterations: 340
currently lose_sum: 95.19187700748444
time_elpased: 1.911
batch start
#iterations: 341
currently lose_sum: 94.22982233762741
time_elpased: 1.891
batch start
#iterations: 342
currently lose_sum: 94.60270869731903
time_elpased: 1.899
batch start
#iterations: 343
currently lose_sum: 95.0680593252182
time_elpased: 1.826
batch start
#iterations: 344
currently lose_sum: 94.54910510778427
time_elpased: 1.78
batch start
#iterations: 345
currently lose_sum: 94.60609298944473
time_elpased: 1.772
batch start
#iterations: 346
currently lose_sum: 94.29032552242279
time_elpased: 1.768
batch start
#iterations: 347
currently lose_sum: 94.49239367246628
time_elpased: 1.748
batch start
#iterations: 348
currently lose_sum: 94.6182233095169
time_elpased: 1.777
batch start
#iterations: 349
currently lose_sum: 94.72580128908157
time_elpased: 1.753
batch start
#iterations: 350
currently lose_sum: 94.60168850421906
time_elpased: 1.78
batch start
#iterations: 351
currently lose_sum: 94.55886602401733
time_elpased: 1.843
batch start
#iterations: 352
currently lose_sum: 94.60727381706238
time_elpased: 1.824
batch start
#iterations: 353
currently lose_sum: 94.90918946266174
time_elpased: 1.775
batch start
#iterations: 354
currently lose_sum: 94.3609407544136
time_elpased: 1.839
batch start
#iterations: 355
currently lose_sum: 94.19598770141602
time_elpased: 1.816
batch start
#iterations: 356
currently lose_sum: 94.29259723424911
time_elpased: 1.798
batch start
#iterations: 357
currently lose_sum: 94.54384034872055
time_elpased: 1.794
batch start
#iterations: 358
currently lose_sum: 94.49862360954285
time_elpased: 1.806
batch start
#iterations: 359
currently lose_sum: 94.09345388412476
time_elpased: 1.799
start validation test
0.662010309278
0.652505551801
0.695553725813
0.673342300603
0.661954888516
60.679
batch start
#iterations: 360
currently lose_sum: 94.5537161231041
time_elpased: 1.827
batch start
#iterations: 361
currently lose_sum: 94.58803814649582
time_elpased: 1.787
batch start
#iterations: 362
currently lose_sum: 94.57357776165009
time_elpased: 1.819
batch start
#iterations: 363
currently lose_sum: 94.64944273233414
time_elpased: 1.767
batch start
#iterations: 364
currently lose_sum: 94.22613352537155
time_elpased: 1.788
batch start
#iterations: 365
currently lose_sum: 94.78047621250153
time_elpased: 1.795
batch start
#iterations: 366
currently lose_sum: 94.71044790744781
time_elpased: 1.873
batch start
#iterations: 367
currently lose_sum: 94.72768127918243
time_elpased: 1.87
batch start
#iterations: 368
currently lose_sum: 94.57154935598373
time_elpased: 1.875
batch start
#iterations: 369
currently lose_sum: 94.7011524438858
time_elpased: 1.859
batch start
#iterations: 370
currently lose_sum: 94.4722216129303
time_elpased: 1.806
batch start
#iterations: 371
currently lose_sum: 94.3919026851654
time_elpased: 1.907
batch start
#iterations: 372
currently lose_sum: 94.1290311217308
time_elpased: 1.818
batch start
#iterations: 373
currently lose_sum: 94.49394923448563
time_elpased: 1.815
batch start
#iterations: 374
currently lose_sum: 94.36885243654251
time_elpased: 1.827
batch start
#iterations: 375
currently lose_sum: 94.61193889379501
time_elpased: 1.792
batch start
#iterations: 376
currently lose_sum: 94.23996156454086
time_elpased: 1.817
batch start
#iterations: 377
currently lose_sum: 94.06189125776291
time_elpased: 1.761
batch start
#iterations: 378
currently lose_sum: 94.65718603134155
time_elpased: 1.842
batch start
#iterations: 379
currently lose_sum: 94.50527602434158
time_elpased: 1.805
start validation test
0.657422680412
0.661510942761
0.647076986414
0.654214360042
0.65743977367
61.175
batch start
#iterations: 380
currently lose_sum: 94.4627388715744
time_elpased: 1.814
batch start
#iterations: 381
currently lose_sum: 94.25413531064987
time_elpased: 1.826
batch start
#iterations: 382
currently lose_sum: 94.4876338839531
time_elpased: 1.826
batch start
#iterations: 383
currently lose_sum: 94.33487451076508
time_elpased: 1.783
batch start
#iterations: 384
currently lose_sum: 94.25108033418655
time_elpased: 1.813
batch start
#iterations: 385
currently lose_sum: 94.53687357902527
time_elpased: 1.844
batch start
#iterations: 386
currently lose_sum: 94.64869916439056
time_elpased: 1.838
batch start
#iterations: 387
currently lose_sum: 94.18089604377747
time_elpased: 1.779
batch start
#iterations: 388
currently lose_sum: 94.43198138475418
time_elpased: 1.82
batch start
#iterations: 389
currently lose_sum: 93.99460011720657
time_elpased: 1.803
batch start
#iterations: 390
currently lose_sum: 94.47807145118713
time_elpased: 1.855
batch start
#iterations: 391
currently lose_sum: 93.8484577536583
time_elpased: 1.814
batch start
#iterations: 392
currently lose_sum: 94.43931549787521
time_elpased: 1.839
batch start
#iterations: 393
currently lose_sum: 94.09268945455551
time_elpased: 1.788
batch start
#iterations: 394
currently lose_sum: 94.18474531173706
time_elpased: 1.808
batch start
#iterations: 395
currently lose_sum: 94.41917324066162
time_elpased: 1.798
batch start
#iterations: 396
currently lose_sum: 94.23169279098511
time_elpased: 1.809
batch start
#iterations: 397
currently lose_sum: 94.3918474316597
time_elpased: 1.788
batch start
#iterations: 398
currently lose_sum: 94.50229513645172
time_elpased: 1.806
batch start
#iterations: 399
currently lose_sum: 94.37639236450195
time_elpased: 1.78
start validation test
0.658865979381
0.653214638971
0.679703581721
0.666195904368
0.65883155129
61.035
acc: 0.674
pre: 0.636
rec: 0.821
F1: 0.716
auc: 0.674
