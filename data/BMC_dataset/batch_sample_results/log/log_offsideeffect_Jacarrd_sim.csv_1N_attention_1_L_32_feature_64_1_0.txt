start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.43234533071518
time_elpased: 1.923
batch start
#iterations: 1
currently lose_sum: 99.81510639190674
time_elpased: 1.826
batch start
#iterations: 2
currently lose_sum: 99.67966747283936
time_elpased: 1.754
batch start
#iterations: 3
currently lose_sum: 99.46764409542084
time_elpased: 1.74
batch start
#iterations: 4
currently lose_sum: 99.10954999923706
time_elpased: 1.759
batch start
#iterations: 5
currently lose_sum: 98.88722282648087
time_elpased: 1.781
batch start
#iterations: 6
currently lose_sum: 98.62117993831635
time_elpased: 1.81
batch start
#iterations: 7
currently lose_sum: 98.58995747566223
time_elpased: 1.774
batch start
#iterations: 8
currently lose_sum: 98.40316569805145
time_elpased: 1.765
batch start
#iterations: 9
currently lose_sum: 98.17734432220459
time_elpased: 1.746
batch start
#iterations: 10
currently lose_sum: 97.94963616132736
time_elpased: 1.773
batch start
#iterations: 11
currently lose_sum: 97.90034002065659
time_elpased: 1.801
batch start
#iterations: 12
currently lose_sum: 98.11505395174026
time_elpased: 1.753
batch start
#iterations: 13
currently lose_sum: 97.51596772670746
time_elpased: 1.757
batch start
#iterations: 14
currently lose_sum: 97.41783004999161
time_elpased: 1.755
batch start
#iterations: 15
currently lose_sum: 97.60323762893677
time_elpased: 1.746
batch start
#iterations: 16
currently lose_sum: 97.29055899381638
time_elpased: 1.734
batch start
#iterations: 17
currently lose_sum: 97.07371473312378
time_elpased: 1.782
batch start
#iterations: 18
currently lose_sum: 97.21235638856888
time_elpased: 1.755
batch start
#iterations: 19
currently lose_sum: 97.32515335083008
time_elpased: 1.757
start validation test
0.642164948454
0.648506903564
0.623546362046
0.635781741868
0.642197636254
62.668
batch start
#iterations: 20
currently lose_sum: 97.35380643606186
time_elpased: 1.815
batch start
#iterations: 21
currently lose_sum: 96.87328058481216
time_elpased: 1.757
batch start
#iterations: 22
currently lose_sum: 97.06160712242126
time_elpased: 1.764
batch start
#iterations: 23
currently lose_sum: 96.63215553760529
time_elpased: 1.751
batch start
#iterations: 24
currently lose_sum: 96.70914137363434
time_elpased: 1.829
batch start
#iterations: 25
currently lose_sum: 96.72488528490067
time_elpased: 1.809
batch start
#iterations: 26
currently lose_sum: 96.62718689441681
time_elpased: 1.742
batch start
#iterations: 27
currently lose_sum: 96.49222999811172
time_elpased: 1.795
batch start
#iterations: 28
currently lose_sum: 96.64649790525436
time_elpased: 1.84
batch start
#iterations: 29
currently lose_sum: 96.33226317167282
time_elpased: 1.835
batch start
#iterations: 30
currently lose_sum: 96.90415090322495
time_elpased: 1.732
batch start
#iterations: 31
currently lose_sum: 96.24066025018692
time_elpased: 1.729
batch start
#iterations: 32
currently lose_sum: 96.21347945928574
time_elpased: 1.759
batch start
#iterations: 33
currently lose_sum: 96.34673184156418
time_elpased: 1.781
batch start
#iterations: 34
currently lose_sum: 96.38927739858627
time_elpased: 1.793
batch start
#iterations: 35
currently lose_sum: 96.54411262273788
time_elpased: 1.799
batch start
#iterations: 36
currently lose_sum: 96.51512426137924
time_elpased: 1.728
batch start
#iterations: 37
currently lose_sum: 96.09727054834366
time_elpased: 1.748
batch start
#iterations: 38
currently lose_sum: 96.34673541784286
time_elpased: 1.741
batch start
#iterations: 39
currently lose_sum: 96.33354806900024
time_elpased: 1.742
start validation test
0.639484536082
0.661986912552
0.572604713389
0.61406025825
0.639601953927
61.904
batch start
#iterations: 40
currently lose_sum: 96.14486700296402
time_elpased: 1.768
batch start
#iterations: 41
currently lose_sum: 96.24493718147278
time_elpased: 1.734
batch start
#iterations: 42
currently lose_sum: 96.37336474657059
time_elpased: 1.733
batch start
#iterations: 43
currently lose_sum: 96.0454490184784
time_elpased: 1.756
batch start
#iterations: 44
currently lose_sum: 96.22791129350662
time_elpased: 1.782
batch start
#iterations: 45
currently lose_sum: 95.7598757147789
time_elpased: 1.846
batch start
#iterations: 46
currently lose_sum: 95.94067353010178
time_elpased: 1.737
batch start
#iterations: 47
currently lose_sum: 95.955601811409
time_elpased: 1.73
batch start
#iterations: 48
currently lose_sum: 95.97847080230713
time_elpased: 1.727
batch start
#iterations: 49
currently lose_sum: 96.55632323026657
time_elpased: 1.75
batch start
#iterations: 50
currently lose_sum: 95.794713139534
time_elpased: 1.759
batch start
#iterations: 51
currently lose_sum: 96.00765597820282
time_elpased: 1.757
batch start
#iterations: 52
currently lose_sum: 95.80132699012756
time_elpased: 1.751
batch start
#iterations: 53
currently lose_sum: 95.97643500566483
time_elpased: 1.747
batch start
#iterations: 54
currently lose_sum: 96.085051715374
time_elpased: 1.762
batch start
#iterations: 55
currently lose_sum: 95.919129550457
time_elpased: 1.801
batch start
#iterations: 56
currently lose_sum: 95.73698967695236
time_elpased: 1.754
batch start
#iterations: 57
currently lose_sum: 95.71116596460342
time_elpased: 1.773
batch start
#iterations: 58
currently lose_sum: 95.88382923603058
time_elpased: 1.782
batch start
#iterations: 59
currently lose_sum: 96.36057710647583
time_elpased: 1.76
start validation test
0.656082474227
0.64251614715
0.706390861377
0.672941176471
0.655994150094
61.496
batch start
#iterations: 60
currently lose_sum: 95.970887362957
time_elpased: 1.809
batch start
#iterations: 61
currently lose_sum: 96.19114404916763
time_elpased: 1.763
batch start
#iterations: 62
currently lose_sum: 96.16396683454514
time_elpased: 1.777
batch start
#iterations: 63
currently lose_sum: 95.96730715036392
time_elpased: 1.849
batch start
#iterations: 64
currently lose_sum: 96.08330291509628
time_elpased: 1.748
batch start
#iterations: 65
currently lose_sum: 95.35014986991882
time_elpased: 1.746
batch start
#iterations: 66
currently lose_sum: 95.51716148853302
time_elpased: 1.754
batch start
#iterations: 67
currently lose_sum: 95.64230132102966
time_elpased: 1.834
batch start
#iterations: 68
currently lose_sum: 96.24790638685226
time_elpased: 1.768
batch start
#iterations: 69
currently lose_sum: 95.63819938898087
time_elpased: 1.734
batch start
#iterations: 70
currently lose_sum: 95.32542502880096
time_elpased: 1.733
batch start
#iterations: 71
currently lose_sum: 95.44763779640198
time_elpased: 1.759
batch start
#iterations: 72
currently lose_sum: 95.28349912166595
time_elpased: 1.744
batch start
#iterations: 73
currently lose_sum: 95.13260823488235
time_elpased: 1.771
batch start
#iterations: 74
currently lose_sum: 95.42811661958694
time_elpased: 1.768
batch start
#iterations: 75
currently lose_sum: 95.39423775672913
time_elpased: 1.821
batch start
#iterations: 76
currently lose_sum: 95.4754991531372
time_elpased: 1.745
batch start
#iterations: 77
currently lose_sum: 95.57026261091232
time_elpased: 1.778
batch start
#iterations: 78
currently lose_sum: 95.47736519575119
time_elpased: 1.766
batch start
#iterations: 79
currently lose_sum: 95.50227373838425
time_elpased: 1.768
start validation test
0.660463917526
0.646672914714
0.710095708552
0.676901947319
0.660376781261
61.584
batch start
#iterations: 80
currently lose_sum: 95.6425855755806
time_elpased: 1.757
batch start
#iterations: 81
currently lose_sum: 95.09449595212936
time_elpased: 1.754
batch start
#iterations: 82
currently lose_sum: 95.62357711791992
time_elpased: 1.755
batch start
#iterations: 83
currently lose_sum: 95.75220519304276
time_elpased: 1.735
batch start
#iterations: 84
currently lose_sum: 95.83584105968475
time_elpased: 1.739
batch start
#iterations: 85
currently lose_sum: 95.4668915271759
time_elpased: 1.764
batch start
#iterations: 86
currently lose_sum: 95.7894674539566
time_elpased: 1.768
batch start
#iterations: 87
currently lose_sum: 95.33123505115509
time_elpased: 1.734
batch start
#iterations: 88
currently lose_sum: 95.42354077100754
time_elpased: 1.738
batch start
#iterations: 89
currently lose_sum: 95.76156318187714
time_elpased: 1.73
batch start
#iterations: 90
currently lose_sum: 95.54728788137436
time_elpased: 1.751
batch start
#iterations: 91
currently lose_sum: 95.06824195384979
time_elpased: 1.753
batch start
#iterations: 92
currently lose_sum: 95.25782483816147
time_elpased: 1.75
batch start
#iterations: 93
currently lose_sum: 95.16581976413727
time_elpased: 1.758
batch start
#iterations: 94
currently lose_sum: 95.58168119192123
time_elpased: 1.765
batch start
#iterations: 95
currently lose_sum: 95.29735136032104
time_elpased: 1.766
batch start
#iterations: 96
currently lose_sum: 95.46057093143463
time_elpased: 1.755
batch start
#iterations: 97
currently lose_sum: 95.3690619468689
time_elpased: 1.746
batch start
#iterations: 98
currently lose_sum: 95.46381115913391
time_elpased: 1.787
batch start
#iterations: 99
currently lose_sum: 95.59680688381195
time_elpased: 1.747
start validation test
0.653917525773
0.652143074273
0.662344344962
0.657204125396
0.653902731192
61.369
batch start
#iterations: 100
currently lose_sum: 95.439219892025
time_elpased: 1.767
batch start
#iterations: 101
currently lose_sum: 95.76731050014496
time_elpased: 1.752
batch start
#iterations: 102
currently lose_sum: 95.27897250652313
time_elpased: 1.723
batch start
#iterations: 103
currently lose_sum: 95.03279280662537
time_elpased: 1.78
batch start
#iterations: 104
currently lose_sum: 94.95003682374954
time_elpased: 1.725
batch start
#iterations: 105
currently lose_sum: 95.14878857135773
time_elpased: 1.739
batch start
#iterations: 106
currently lose_sum: 95.36726033687592
time_elpased: 1.779
batch start
#iterations: 107
currently lose_sum: 95.29521065950394
time_elpased: 1.759
batch start
#iterations: 108
currently lose_sum: 95.0532540678978
time_elpased: 1.748
batch start
#iterations: 109
currently lose_sum: 95.51133739948273
time_elpased: 1.765
batch start
#iterations: 110
currently lose_sum: 95.23288917541504
time_elpased: 1.772
batch start
#iterations: 111
currently lose_sum: 95.02483314275742
time_elpased: 1.769
batch start
#iterations: 112
currently lose_sum: 95.33947771787643
time_elpased: 1.763
batch start
#iterations: 113
currently lose_sum: 95.13692557811737
time_elpased: 1.769
batch start
#iterations: 114
currently lose_sum: 95.3784658908844
time_elpased: 1.779
batch start
#iterations: 115
currently lose_sum: 95.33437794446945
time_elpased: 1.761
batch start
#iterations: 116
currently lose_sum: 94.95101225376129
time_elpased: 1.751
batch start
#iterations: 117
currently lose_sum: 95.25227779150009
time_elpased: 1.752
batch start
#iterations: 118
currently lose_sum: 95.33132880926132
time_elpased: 1.755
batch start
#iterations: 119
currently lose_sum: 95.12860983610153
time_elpased: 1.751
start validation test
0.671134020619
0.63485007678
0.808377071112
0.711181530104
0.670893069275
60.502
batch start
#iterations: 120
currently lose_sum: 95.37771970033646
time_elpased: 1.758
batch start
#iterations: 121
currently lose_sum: 95.39012598991394
time_elpased: 1.754
batch start
#iterations: 122
currently lose_sum: 95.09031772613525
time_elpased: 1.753
batch start
#iterations: 123
currently lose_sum: 95.13164830207825
time_elpased: 1.913
batch start
#iterations: 124
currently lose_sum: 95.41610908508301
time_elpased: 2.045
batch start
#iterations: 125
currently lose_sum: 95.25803881883621
time_elpased: 2.013
batch start
#iterations: 126
currently lose_sum: 95.13142919540405
time_elpased: 1.982
batch start
#iterations: 127
currently lose_sum: 95.18473279476166
time_elpased: 1.758
batch start
#iterations: 128
currently lose_sum: 95.79567909240723
time_elpased: 1.761
batch start
#iterations: 129
currently lose_sum: 95.3069948554039
time_elpased: 1.757
batch start
#iterations: 130
currently lose_sum: 94.81949830055237
time_elpased: 1.781
batch start
#iterations: 131
currently lose_sum: 95.28272068500519
time_elpased: 1.813
batch start
#iterations: 132
currently lose_sum: 95.14754867553711
time_elpased: 1.736
batch start
#iterations: 133
currently lose_sum: 95.13304454088211
time_elpased: 1.781
batch start
#iterations: 134
currently lose_sum: 95.05545043945312
time_elpased: 1.806
batch start
#iterations: 135
currently lose_sum: 95.29549407958984
time_elpased: 1.823
batch start
#iterations: 136
currently lose_sum: 95.14726483821869
time_elpased: 1.832
batch start
#iterations: 137
currently lose_sum: 94.65166276693344
time_elpased: 1.814
batch start
#iterations: 138
currently lose_sum: 95.34661787748337
time_elpased: 1.753
batch start
#iterations: 139
currently lose_sum: 95.57431048154831
time_elpased: 1.749
start validation test
0.66793814433
0.637848303729
0.779767417927
0.701704019263
0.667741810792
60.868
batch start
#iterations: 140
currently lose_sum: 95.14644420146942
time_elpased: 1.814
batch start
#iterations: 141
currently lose_sum: 95.25407522916794
time_elpased: 1.78
batch start
#iterations: 142
currently lose_sum: 95.2378261089325
time_elpased: 1.727
batch start
#iterations: 143
currently lose_sum: 94.89567857980728
time_elpased: 1.75
batch start
#iterations: 144
currently lose_sum: 95.05823540687561
time_elpased: 1.751
batch start
#iterations: 145
currently lose_sum: 94.84687215089798
time_elpased: 1.762
batch start
#iterations: 146
currently lose_sum: 95.29462844133377
time_elpased: 1.756
batch start
#iterations: 147
currently lose_sum: 95.18836998939514
time_elpased: 1.781
batch start
#iterations: 148
currently lose_sum: 94.92147535085678
time_elpased: 1.761
batch start
#iterations: 149
currently lose_sum: 95.32250308990479
time_elpased: 1.765
batch start
#iterations: 150
currently lose_sum: 95.1631789803505
time_elpased: 1.777
batch start
#iterations: 151
currently lose_sum: 95.08809524774551
time_elpased: 1.753
batch start
#iterations: 152
currently lose_sum: 95.1897542476654
time_elpased: 1.752
batch start
#iterations: 153
currently lose_sum: 95.21095651388168
time_elpased: 1.797
batch start
#iterations: 154
currently lose_sum: 95.22459542751312
time_elpased: 1.744
batch start
#iterations: 155
currently lose_sum: 95.0103303194046
time_elpased: 1.794
batch start
#iterations: 156
currently lose_sum: 94.97318083047867
time_elpased: 1.748
batch start
#iterations: 157
currently lose_sum: 94.98224627971649
time_elpased: 1.739
batch start
#iterations: 158
currently lose_sum: 94.82601946592331
time_elpased: 1.743
batch start
#iterations: 159
currently lose_sum: 95.10021102428436
time_elpased: 1.769
start validation test
0.664587628866
0.635580334516
0.774313059586
0.698121085595
0.664394988948
60.873
batch start
#iterations: 160
currently lose_sum: 95.2587080001831
time_elpased: 1.768
batch start
#iterations: 161
currently lose_sum: 94.60232031345367
time_elpased: 1.749
batch start
#iterations: 162
currently lose_sum: 94.97093892097473
time_elpased: 1.751
batch start
#iterations: 163
currently lose_sum: 95.27113515138626
time_elpased: 1.777
batch start
#iterations: 164
currently lose_sum: 94.7475318312645
time_elpased: 1.76
batch start
#iterations: 165
currently lose_sum: 94.90502959489822
time_elpased: 1.764
batch start
#iterations: 166
currently lose_sum: 95.31194615364075
time_elpased: 1.752
batch start
#iterations: 167
currently lose_sum: 94.64485383033752
time_elpased: 1.81
batch start
#iterations: 168
currently lose_sum: 94.76956111192703
time_elpased: 1.844
batch start
#iterations: 169
currently lose_sum: 95.10651642084122
time_elpased: 1.82
batch start
#iterations: 170
currently lose_sum: 94.79018640518188
time_elpased: 1.828
batch start
#iterations: 171
currently lose_sum: 94.71921223402023
time_elpased: 1.882
batch start
#iterations: 172
currently lose_sum: 94.9657706618309
time_elpased: 1.816
batch start
#iterations: 173
currently lose_sum: 94.55949848890305
time_elpased: 1.921
batch start
#iterations: 174
currently lose_sum: 94.95508474111557
time_elpased: 1.791
batch start
#iterations: 175
currently lose_sum: 94.7529444694519
time_elpased: 1.858
batch start
#iterations: 176
currently lose_sum: 95.13453757762909
time_elpased: 1.878
batch start
#iterations: 177
currently lose_sum: 95.04281693696976
time_elpased: 1.945
batch start
#iterations: 178
currently lose_sum: 95.11409771442413
time_elpased: 1.856
batch start
#iterations: 179
currently lose_sum: 94.67911756038666
time_elpased: 1.867
start validation test
0.660309278351
0.637281587497
0.74693835546
0.687766511892
0.660157187644
60.617
batch start
#iterations: 180
currently lose_sum: 94.89465034008026
time_elpased: 1.87
batch start
#iterations: 181
currently lose_sum: 94.46859961748123
time_elpased: 1.882
batch start
#iterations: 182
currently lose_sum: 94.81446808576584
time_elpased: 1.888
batch start
#iterations: 183
currently lose_sum: 94.39144742488861
time_elpased: 1.856
batch start
#iterations: 184
currently lose_sum: 94.89895308017731
time_elpased: 1.831
batch start
#iterations: 185
currently lose_sum: 94.60967952013016
time_elpased: 1.918
batch start
#iterations: 186
currently lose_sum: 94.4758220911026
time_elpased: 1.877
batch start
#iterations: 187
currently lose_sum: 94.67512792348862
time_elpased: 1.833
batch start
#iterations: 188
currently lose_sum: 94.6010999083519
time_elpased: 1.813
batch start
#iterations: 189
currently lose_sum: 94.84396207332611
time_elpased: 1.849
batch start
#iterations: 190
currently lose_sum: 94.61902409791946
time_elpased: 1.834
batch start
#iterations: 191
currently lose_sum: 94.96759045124054
time_elpased: 1.838
batch start
#iterations: 192
currently lose_sum: 94.6077800989151
time_elpased: 1.832
batch start
#iterations: 193
currently lose_sum: 94.66187554597855
time_elpased: 1.852
batch start
#iterations: 194
currently lose_sum: 94.42696261405945
time_elpased: 1.899
batch start
#iterations: 195
currently lose_sum: 94.80944353342056
time_elpased: 1.904
batch start
#iterations: 196
currently lose_sum: 94.60091769695282
time_elpased: 1.954
batch start
#iterations: 197
currently lose_sum: 94.92512935400009
time_elpased: 1.927
batch start
#iterations: 198
currently lose_sum: 94.8053429722786
time_elpased: 1.882
batch start
#iterations: 199
currently lose_sum: 95.07893776893616
time_elpased: 1.893
start validation test
0.666443298969
0.637963277797
0.772357723577
0.698757041106
0.66625734986
60.495
batch start
#iterations: 200
currently lose_sum: 94.71695071458817
time_elpased: 1.923
batch start
#iterations: 201
currently lose_sum: 94.79265087842941
time_elpased: 1.793
batch start
#iterations: 202
currently lose_sum: 95.03086465597153
time_elpased: 1.906
batch start
#iterations: 203
currently lose_sum: 94.72335463762283
time_elpased: 1.839
batch start
#iterations: 204
currently lose_sum: 94.54090183973312
time_elpased: 1.928
batch start
#iterations: 205
currently lose_sum: 94.74083524942398
time_elpased: 1.846
batch start
#iterations: 206
currently lose_sum: 94.5569714307785
time_elpased: 1.793
batch start
#iterations: 207
currently lose_sum: 95.05171406269073
time_elpased: 1.813
batch start
#iterations: 208
currently lose_sum: 94.58337253332138
time_elpased: 1.84
batch start
#iterations: 209
currently lose_sum: 94.97778069972992
time_elpased: 1.79
batch start
#iterations: 210
currently lose_sum: 94.44194507598877
time_elpased: 1.81
batch start
#iterations: 211
currently lose_sum: 94.56712770462036
time_elpased: 1.87
batch start
#iterations: 212
currently lose_sum: 94.06305629014969
time_elpased: 1.841
batch start
#iterations: 213
currently lose_sum: 94.350701212883
time_elpased: 1.853
batch start
#iterations: 214
currently lose_sum: 94.322032392025
time_elpased: 1.867
batch start
#iterations: 215
currently lose_sum: 94.51351928710938
time_elpased: 1.82
batch start
#iterations: 216
currently lose_sum: 94.54050606489182
time_elpased: 1.856
batch start
#iterations: 217
currently lose_sum: 94.46876603364944
time_elpased: 1.87
batch start
#iterations: 218
currently lose_sum: 94.95895326137543
time_elpased: 1.807
batch start
#iterations: 219
currently lose_sum: 95.0214251279831
time_elpased: 1.906
start validation test
0.66587628866
0.642649263603
0.749922815684
0.692154255319
0.665728732018
60.467
batch start
#iterations: 220
currently lose_sum: 94.9439617395401
time_elpased: 1.819
batch start
#iterations: 221
currently lose_sum: 94.94285094738007
time_elpased: 1.825
batch start
#iterations: 222
currently lose_sum: 94.92581862211227
time_elpased: 1.814
batch start
#iterations: 223
currently lose_sum: 94.7383604645729
time_elpased: 1.784
batch start
#iterations: 224
currently lose_sum: 94.75973618030548
time_elpased: 1.817
batch start
#iterations: 225
currently lose_sum: 94.39992719888687
time_elpased: 1.872
batch start
#iterations: 226
currently lose_sum: 94.63803571462631
time_elpased: 1.816
batch start
#iterations: 227
currently lose_sum: 94.64160597324371
time_elpased: 1.813
batch start
#iterations: 228
currently lose_sum: 94.44826608896255
time_elpased: 1.822
batch start
#iterations: 229
currently lose_sum: 95.0897051692009
time_elpased: 1.831
batch start
#iterations: 230
currently lose_sum: 94.29745090007782
time_elpased: 1.802
batch start
#iterations: 231
currently lose_sum: 94.29182010889053
time_elpased: 1.866
batch start
#iterations: 232
currently lose_sum: 94.66461342573166
time_elpased: 1.804
batch start
#iterations: 233
currently lose_sum: 94.89785087108612
time_elpased: 1.806
batch start
#iterations: 234
currently lose_sum: 94.52307146787643
time_elpased: 1.765
batch start
#iterations: 235
currently lose_sum: 94.31398510932922
time_elpased: 1.806
batch start
#iterations: 236
currently lose_sum: 94.83789801597595
time_elpased: 1.815
batch start
#iterations: 237
currently lose_sum: 94.32392913103104
time_elpased: 1.784
batch start
#iterations: 238
currently lose_sum: 94.39955937862396
time_elpased: 1.833
batch start
#iterations: 239
currently lose_sum: 94.17928320169449
time_elpased: 1.798
start validation test
0.664536082474
0.637784456848
0.764330554698
0.695346877633
0.664360877886
60.334
batch start
#iterations: 240
currently lose_sum: 94.48417323827744
time_elpased: 1.842
batch start
#iterations: 241
currently lose_sum: 94.65821176767349
time_elpased: 1.789
batch start
#iterations: 242
currently lose_sum: 94.21018373966217
time_elpased: 1.832
batch start
#iterations: 243
currently lose_sum: 94.47557771205902
time_elpased: 1.766
batch start
#iterations: 244
currently lose_sum: 94.89835441112518
time_elpased: 1.833
batch start
#iterations: 245
currently lose_sum: 95.26531732082367
time_elpased: 1.809
batch start
#iterations: 246
currently lose_sum: 94.55678862333298
time_elpased: 1.783
batch start
#iterations: 247
currently lose_sum: 94.61668282747269
time_elpased: 1.791
batch start
#iterations: 248
currently lose_sum: 95.08040529489517
time_elpased: 1.908
batch start
#iterations: 249
currently lose_sum: 94.64555931091309
time_elpased: 1.844
batch start
#iterations: 250
currently lose_sum: 94.59855711460114
time_elpased: 1.862
batch start
#iterations: 251
currently lose_sum: 94.18635350465775
time_elpased: 1.854
batch start
#iterations: 252
currently lose_sum: 94.39708268642426
time_elpased: 1.817
batch start
#iterations: 253
currently lose_sum: 94.76344323158264
time_elpased: 1.802
batch start
#iterations: 254
currently lose_sum: 94.49254727363586
time_elpased: 1.782
batch start
#iterations: 255
currently lose_sum: 94.23893719911575
time_elpased: 1.807
batch start
#iterations: 256
currently lose_sum: 94.5028777718544
time_elpased: 1.79
batch start
#iterations: 257
currently lose_sum: 94.13254129886627
time_elpased: 1.804
batch start
#iterations: 258
currently lose_sum: 94.54939025640488
time_elpased: 1.812
batch start
#iterations: 259
currently lose_sum: 94.494500041008
time_elpased: 1.808
start validation test
0.665360824742
0.648521691075
0.724606359988
0.684456109653
0.665256810067
60.504
batch start
#iterations: 260
currently lose_sum: 94.48588466644287
time_elpased: 1.803
batch start
#iterations: 261
currently lose_sum: 94.63042533397675
time_elpased: 1.809
batch start
#iterations: 262
currently lose_sum: 94.14660453796387
time_elpased: 1.907
batch start
#iterations: 263
currently lose_sum: 94.42904007434845
time_elpased: 1.809
batch start
#iterations: 264
currently lose_sum: 94.88622188568115
time_elpased: 1.755
batch start
#iterations: 265
currently lose_sum: 94.69943428039551
time_elpased: 1.778
batch start
#iterations: 266
currently lose_sum: 94.33231699466705
time_elpased: 1.791
batch start
#iterations: 267
currently lose_sum: 94.29375278949738
time_elpased: 1.79
batch start
#iterations: 268
currently lose_sum: 94.71037673950195
time_elpased: 1.787
batch start
#iterations: 269
currently lose_sum: 94.61278945207596
time_elpased: 1.833
batch start
#iterations: 270
currently lose_sum: 94.88332188129425
time_elpased: 1.764
batch start
#iterations: 271
currently lose_sum: 94.16416239738464
time_elpased: 1.772
batch start
#iterations: 272
currently lose_sum: 94.49959790706635
time_elpased: 1.79
batch start
#iterations: 273
currently lose_sum: 93.99525076150894
time_elpased: 1.772
batch start
#iterations: 274
currently lose_sum: 94.305295586586
time_elpased: 1.78
batch start
#iterations: 275
currently lose_sum: 94.37363052368164
time_elpased: 1.759
batch start
#iterations: 276
currently lose_sum: 94.94063538312912
time_elpased: 1.774
batch start
#iterations: 277
currently lose_sum: 94.99253934621811
time_elpased: 1.769
batch start
#iterations: 278
currently lose_sum: 94.30196005105972
time_elpased: 1.848
batch start
#iterations: 279
currently lose_sum: 94.94759321212769
time_elpased: 1.744
start validation test
0.661340206186
0.646631255242
0.714109292992
0.678697183099
0.661247561915
60.663
batch start
#iterations: 280
currently lose_sum: 94.380715072155
time_elpased: 1.815
batch start
#iterations: 281
currently lose_sum: 94.48620045185089
time_elpased: 1.778
batch start
#iterations: 282
currently lose_sum: 94.65098696947098
time_elpased: 1.775
batch start
#iterations: 283
currently lose_sum: 94.6957613825798
time_elpased: 1.815
batch start
#iterations: 284
currently lose_sum: 93.9321563243866
time_elpased: 1.774
batch start
#iterations: 285
currently lose_sum: 94.73719781637192
time_elpased: 1.865
batch start
#iterations: 286
currently lose_sum: 94.47862708568573
time_elpased: 1.785
batch start
#iterations: 287
currently lose_sum: 94.54566967487335
time_elpased: 1.795
batch start
#iterations: 288
currently lose_sum: 94.60969805717468
time_elpased: 1.795
batch start
#iterations: 289
currently lose_sum: 94.54261428117752
time_elpased: 1.791
batch start
#iterations: 290
currently lose_sum: 94.53362691402435
time_elpased: 1.801
batch start
#iterations: 291
currently lose_sum: 94.23182654380798
time_elpased: 1.8
batch start
#iterations: 292
currently lose_sum: 94.23141580820084
time_elpased: 1.811
batch start
#iterations: 293
currently lose_sum: 94.20736289024353
time_elpased: 1.772
batch start
#iterations: 294
currently lose_sum: 94.31711739301682
time_elpased: 1.774
batch start
#iterations: 295
currently lose_sum: 94.29429060220718
time_elpased: 1.782
batch start
#iterations: 296
currently lose_sum: 94.55611270666122
time_elpased: 1.767
batch start
#iterations: 297
currently lose_sum: 94.2841392159462
time_elpased: 1.759
batch start
#iterations: 298
currently lose_sum: 94.63534897565842
time_elpased: 1.821
batch start
#iterations: 299
currently lose_sum: 94.26194733381271
time_elpased: 1.766
start validation test
0.663505154639
0.640645673459
0.747452917567
0.68994015389
0.663357771393
60.576
batch start
#iterations: 300
currently lose_sum: 94.32354724407196
time_elpased: 1.777
batch start
#iterations: 301
currently lose_sum: 94.49921762943268
time_elpased: 1.799
batch start
#iterations: 302
currently lose_sum: 94.5310589671135
time_elpased: 1.743
batch start
#iterations: 303
currently lose_sum: 94.15430307388306
time_elpased: 1.764
batch start
#iterations: 304
currently lose_sum: 94.11537021398544
time_elpased: 1.773
batch start
#iterations: 305
currently lose_sum: 94.42386323213577
time_elpased: 1.768
batch start
#iterations: 306
currently lose_sum: 94.47660332918167
time_elpased: 1.771
batch start
#iterations: 307
currently lose_sum: 94.45600700378418
time_elpased: 1.797
batch start
#iterations: 308
currently lose_sum: 94.18193173408508
time_elpased: 1.843
batch start
#iterations: 309
currently lose_sum: 94.43279618024826
time_elpased: 1.783
batch start
#iterations: 310
currently lose_sum: 94.55056375265121
time_elpased: 1.785
batch start
#iterations: 311
currently lose_sum: 94.27444368600845
time_elpased: 1.769
batch start
#iterations: 312
currently lose_sum: 94.27020102739334
time_elpased: 1.778
batch start
#iterations: 313
currently lose_sum: 94.18502825498581
time_elpased: 1.774
batch start
#iterations: 314
currently lose_sum: 94.31353163719177
time_elpased: 1.748
batch start
#iterations: 315
currently lose_sum: 94.79395371675491
time_elpased: 1.774
batch start
#iterations: 316
currently lose_sum: 94.52631890773773
time_elpased: 1.768
batch start
#iterations: 317
currently lose_sum: 94.27353006601334
time_elpased: 1.793
batch start
#iterations: 318
currently lose_sum: 94.17986977100372
time_elpased: 1.809
batch start
#iterations: 319
currently lose_sum: 94.28711926937103
time_elpased: 1.842
start validation test
0.664278350515
0.645821955216
0.73016363075
0.685407911897
0.664162678744
60.461
batch start
#iterations: 320
currently lose_sum: 94.4722695350647
time_elpased: 1.808
batch start
#iterations: 321
currently lose_sum: 94.48958241939545
time_elpased: 1.763
batch start
#iterations: 322
currently lose_sum: 94.1973562836647
time_elpased: 1.762
batch start
#iterations: 323
currently lose_sum: 94.4051906466484
time_elpased: 1.802
batch start
#iterations: 324
currently lose_sum: 94.86237967014313
time_elpased: 1.762
batch start
#iterations: 325
currently lose_sum: 94.44971704483032
time_elpased: 1.788
batch start
#iterations: 326
currently lose_sum: 93.91210943460464
time_elpased: 1.806
batch start
#iterations: 327
currently lose_sum: 94.17973911762238
time_elpased: 1.766
batch start
#iterations: 328
currently lose_sum: 94.18026304244995
time_elpased: 1.793
batch start
#iterations: 329
currently lose_sum: 94.93663895130157
time_elpased: 1.771
batch start
#iterations: 330
currently lose_sum: 94.09659123420715
time_elpased: 1.768
batch start
#iterations: 331
currently lose_sum: 94.2728590965271
time_elpased: 1.757
batch start
#iterations: 332
currently lose_sum: 94.2924587726593
time_elpased: 1.777
batch start
#iterations: 333
currently lose_sum: 94.33613049983978
time_elpased: 1.791
batch start
#iterations: 334
currently lose_sum: 94.71113979816437
time_elpased: 1.736
batch start
#iterations: 335
currently lose_sum: 94.37795734405518
time_elpased: 1.809
batch start
#iterations: 336
currently lose_sum: 94.50279170274734
time_elpased: 1.766
batch start
#iterations: 337
currently lose_sum: 94.5246866941452
time_elpased: 1.748
batch start
#iterations: 338
currently lose_sum: 94.18089735507965
time_elpased: 1.829
batch start
#iterations: 339
currently lose_sum: 93.93360620737076
time_elpased: 1.748
start validation test
0.662783505155
0.647688157038
0.716476278687
0.680347894068
0.662689239209
60.808
batch start
#iterations: 340
currently lose_sum: 94.23665434122086
time_elpased: 1.821
batch start
#iterations: 341
currently lose_sum: 94.31133770942688
time_elpased: 1.771
batch start
#iterations: 342
currently lose_sum: 94.53672432899475
time_elpased: 1.791
batch start
#iterations: 343
currently lose_sum: 94.5438751578331
time_elpased: 1.78
batch start
#iterations: 344
currently lose_sum: 94.19102776050568
time_elpased: 1.772
batch start
#iterations: 345
currently lose_sum: 94.48032450675964
time_elpased: 1.816
batch start
#iterations: 346
currently lose_sum: 94.20203256607056
time_elpased: 1.777
batch start
#iterations: 347
currently lose_sum: 94.40551400184631
time_elpased: 1.829
batch start
#iterations: 348
currently lose_sum: 94.56973540782928
time_elpased: 1.884
batch start
#iterations: 349
currently lose_sum: 94.42272019386292
time_elpased: 1.783
batch start
#iterations: 350
currently lose_sum: 94.05889987945557
time_elpased: 1.753
batch start
#iterations: 351
currently lose_sum: 94.29337793588638
time_elpased: 1.737
batch start
#iterations: 352
currently lose_sum: 94.33081078529358
time_elpased: 1.841
batch start
#iterations: 353
currently lose_sum: 94.49679166078568
time_elpased: 1.765
batch start
#iterations: 354
currently lose_sum: 94.46249574422836
time_elpased: 1.779
batch start
#iterations: 355
currently lose_sum: 94.29875272512436
time_elpased: 1.775
batch start
#iterations: 356
currently lose_sum: 94.46970611810684
time_elpased: 1.785
batch start
#iterations: 357
currently lose_sum: 94.75107198953629
time_elpased: 1.841
batch start
#iterations: 358
currently lose_sum: 94.40122348070145
time_elpased: 1.762
batch start
#iterations: 359
currently lose_sum: 94.4399603009224
time_elpased: 1.751
start validation test
0.66618556701
0.650730164636
0.719975301019
0.683603674028
0.666091130836
60.643
batch start
#iterations: 360
currently lose_sum: 94.59931015968323
time_elpased: 1.77
batch start
#iterations: 361
currently lose_sum: 94.3976901769638
time_elpased: 1.787
batch start
#iterations: 362
currently lose_sum: 94.17315983772278
time_elpased: 1.762
batch start
#iterations: 363
currently lose_sum: 94.36053967475891
time_elpased: 1.764
batch start
#iterations: 364
currently lose_sum: 94.81997513771057
time_elpased: 1.755
batch start
#iterations: 365
currently lose_sum: 93.77255022525787
time_elpased: 1.759
batch start
#iterations: 366
currently lose_sum: 94.56669038534164
time_elpased: 1.781
batch start
#iterations: 367
currently lose_sum: 94.30699002742767
time_elpased: 1.752
batch start
#iterations: 368
currently lose_sum: 94.85590147972107
time_elpased: 1.759
batch start
#iterations: 369
currently lose_sum: 94.15381920337677
time_elpased: 1.795
batch start
#iterations: 370
currently lose_sum: 93.82948994636536
time_elpased: 1.76
batch start
#iterations: 371
currently lose_sum: 94.39393037557602
time_elpased: 1.756
batch start
#iterations: 372
currently lose_sum: 94.25874865055084
time_elpased: 1.771
batch start
#iterations: 373
currently lose_sum: 93.95362550020218
time_elpased: 1.822
batch start
#iterations: 374
currently lose_sum: 94.2253270149231
time_elpased: 1.789
batch start
#iterations: 375
currently lose_sum: 94.54865086078644
time_elpased: 1.763
batch start
#iterations: 376
currently lose_sum: 93.86671251058578
time_elpased: 1.745
batch start
#iterations: 377
currently lose_sum: 94.3126031756401
time_elpased: 1.789
batch start
#iterations: 378
currently lose_sum: 94.13050866127014
time_elpased: 1.734
batch start
#iterations: 379
currently lose_sum: 94.40127390623093
time_elpased: 1.73
start validation test
0.664896907216
0.648503878833
0.722651023979
0.683572645413
0.664795510957
60.480
batch start
#iterations: 380
currently lose_sum: 94.03646928071976
time_elpased: 1.789
batch start
#iterations: 381
currently lose_sum: 94.32601398229599
time_elpased: 1.768
batch start
#iterations: 382
currently lose_sum: 94.22939330339432
time_elpased: 1.734
batch start
#iterations: 383
currently lose_sum: 94.09406816959381
time_elpased: 1.752
batch start
#iterations: 384
currently lose_sum: 94.73867040872574
time_elpased: 1.753
batch start
#iterations: 385
currently lose_sum: 94.3932614326477
time_elpased: 1.773
batch start
#iterations: 386
currently lose_sum: 94.07632964849472
time_elpased: 1.752
batch start
#iterations: 387
currently lose_sum: 94.20164895057678
time_elpased: 1.784
batch start
#iterations: 388
currently lose_sum: 94.53394919633865
time_elpased: 1.756
batch start
#iterations: 389
currently lose_sum: 94.34519249200821
time_elpased: 1.803
batch start
#iterations: 390
currently lose_sum: 94.57074612379074
time_elpased: 1.795
batch start
#iterations: 391
currently lose_sum: 94.28859478235245
time_elpased: 1.752
batch start
#iterations: 392
currently lose_sum: 94.14552628993988
time_elpased: 1.818
batch start
#iterations: 393
currently lose_sum: 94.3754632472992
time_elpased: 1.792
batch start
#iterations: 394
currently lose_sum: 94.0999528169632
time_elpased: 1.73
batch start
#iterations: 395
currently lose_sum: 93.85568118095398
time_elpased: 1.781
batch start
#iterations: 396
currently lose_sum: 94.21095621585846
time_elpased: 1.78
batch start
#iterations: 397
currently lose_sum: 94.07689076662064
time_elpased: 1.802
batch start
#iterations: 398
currently lose_sum: 94.37025028467178
time_elpased: 1.862
batch start
#iterations: 399
currently lose_sum: 94.44560760259628
time_elpased: 1.753
start validation test
0.664020618557
0.642367601246
0.742718946177
0.688907980145
0.663882451504
60.644
acc: 0.669
pre: 0.642
rec: 0.767
F1: 0.699
auc: 0.669
