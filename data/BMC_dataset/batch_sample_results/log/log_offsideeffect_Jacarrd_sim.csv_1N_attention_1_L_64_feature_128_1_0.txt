start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.3204556107521
time_elpased: 1.988
batch start
#iterations: 1
currently lose_sum: 99.84454262256622
time_elpased: 1.883
batch start
#iterations: 2
currently lose_sum: 99.46927553415298
time_elpased: 1.921
batch start
#iterations: 3
currently lose_sum: 99.04606968164444
time_elpased: 1.928
batch start
#iterations: 4
currently lose_sum: 98.99435412883759
time_elpased: 1.898
batch start
#iterations: 5
currently lose_sum: 98.5944516658783
time_elpased: 1.885
batch start
#iterations: 6
currently lose_sum: 98.65303522348404
time_elpased: 1.887
batch start
#iterations: 7
currently lose_sum: 98.38647097349167
time_elpased: 1.887
batch start
#iterations: 8
currently lose_sum: 97.95765775442123
time_elpased: 1.876
batch start
#iterations: 9
currently lose_sum: 98.14708709716797
time_elpased: 1.883
batch start
#iterations: 10
currently lose_sum: 98.01608353853226
time_elpased: 1.856
batch start
#iterations: 11
currently lose_sum: 97.91323006153107
time_elpased: 1.897
batch start
#iterations: 12
currently lose_sum: 97.86855268478394
time_elpased: 1.916
batch start
#iterations: 13
currently lose_sum: 97.4473152756691
time_elpased: 1.925
batch start
#iterations: 14
currently lose_sum: 97.86745965480804
time_elpased: 1.883
batch start
#iterations: 15
currently lose_sum: 97.28851705789566
time_elpased: 1.905
batch start
#iterations: 16
currently lose_sum: 97.402558863163
time_elpased: 1.869
batch start
#iterations: 17
currently lose_sum: 97.39295655488968
time_elpased: 1.913
batch start
#iterations: 18
currently lose_sum: 97.03344529867172
time_elpased: 1.897
batch start
#iterations: 19
currently lose_sum: 97.43634271621704
time_elpased: 1.891
start validation test
0.618092783505
0.662352279122
0.48451168056
0.559643387816
0.618327305735
63.228
batch start
#iterations: 20
currently lose_sum: 97.35925936698914
time_elpased: 1.894
batch start
#iterations: 21
currently lose_sum: 96.5110713839531
time_elpased: 1.914
batch start
#iterations: 22
currently lose_sum: 96.9292181134224
time_elpased: 1.897
batch start
#iterations: 23
currently lose_sum: 96.81623357534409
time_elpased: 1.925
batch start
#iterations: 24
currently lose_sum: 96.66471934318542
time_elpased: 1.908
batch start
#iterations: 25
currently lose_sum: 96.49006676673889
time_elpased: 1.902
batch start
#iterations: 26
currently lose_sum: 96.85089933872223
time_elpased: 1.882
batch start
#iterations: 27
currently lose_sum: 96.49878352880478
time_elpased: 1.889
batch start
#iterations: 28
currently lose_sum: 96.52906584739685
time_elpased: 1.91
batch start
#iterations: 29
currently lose_sum: 96.55975496768951
time_elpased: 1.881
batch start
#iterations: 30
currently lose_sum: 96.59100878238678
time_elpased: 1.879
batch start
#iterations: 31
currently lose_sum: 96.44564545154572
time_elpased: 1.888
batch start
#iterations: 32
currently lose_sum: 96.17149996757507
time_elpased: 1.887
batch start
#iterations: 33
currently lose_sum: 96.22597342729568
time_elpased: 1.888
batch start
#iterations: 34
currently lose_sum: 96.47326177358627
time_elpased: 1.869
batch start
#iterations: 35
currently lose_sum: 96.51203340291977
time_elpased: 1.87
batch start
#iterations: 36
currently lose_sum: 96.2972599864006
time_elpased: 1.86
batch start
#iterations: 37
currently lose_sum: 96.19256108999252
time_elpased: 1.892
batch start
#iterations: 38
currently lose_sum: 96.18757635354996
time_elpased: 1.908
batch start
#iterations: 39
currently lose_sum: 96.322967171669
time_elpased: 1.901
start validation test
0.660309278351
0.641480409013
0.729546156221
0.682684899846
0.660187722332
61.395
batch start
#iterations: 40
currently lose_sum: 96.68552601337433
time_elpased: 1.926
batch start
#iterations: 41
currently lose_sum: 96.42282527685165
time_elpased: 1.881
batch start
#iterations: 42
currently lose_sum: 96.11806970834732
time_elpased: 1.939
batch start
#iterations: 43
currently lose_sum: 95.63512349128723
time_elpased: 1.905
batch start
#iterations: 44
currently lose_sum: 96.02406048774719
time_elpased: 1.901
batch start
#iterations: 45
currently lose_sum: 95.9099212884903
time_elpased: 1.907
batch start
#iterations: 46
currently lose_sum: 96.11073017120361
time_elpased: 1.864
batch start
#iterations: 47
currently lose_sum: 96.02672010660172
time_elpased: 1.881
batch start
#iterations: 48
currently lose_sum: 96.00681364536285
time_elpased: 1.883
batch start
#iterations: 49
currently lose_sum: 96.26267045736313
time_elpased: 1.886
batch start
#iterations: 50
currently lose_sum: 95.90988320112228
time_elpased: 1.94
batch start
#iterations: 51
currently lose_sum: 96.01420038938522
time_elpased: 1.883
batch start
#iterations: 52
currently lose_sum: 95.97639685869217
time_elpased: 1.914
batch start
#iterations: 53
currently lose_sum: 95.91470754146576
time_elpased: 1.889
batch start
#iterations: 54
currently lose_sum: 96.13314723968506
time_elpased: 1.888
batch start
#iterations: 55
currently lose_sum: 95.84031850099564
time_elpased: 1.899
batch start
#iterations: 56
currently lose_sum: 95.78621029853821
time_elpased: 1.891
batch start
#iterations: 57
currently lose_sum: 95.7894914150238
time_elpased: 1.888
batch start
#iterations: 58
currently lose_sum: 96.2544395327568
time_elpased: 1.895
batch start
#iterations: 59
currently lose_sum: 95.97979706525803
time_elpased: 1.892
start validation test
0.651958762887
0.649340183338
0.663373469178
0.656281816331
0.651938722609
61.728
batch start
#iterations: 60
currently lose_sum: 96.02745771408081
time_elpased: 1.89
batch start
#iterations: 61
currently lose_sum: 96.20291417837143
time_elpased: 1.885
batch start
#iterations: 62
currently lose_sum: 95.89412540197372
time_elpased: 1.881
batch start
#iterations: 63
currently lose_sum: 95.93904459476471
time_elpased: 1.921
batch start
#iterations: 64
currently lose_sum: 96.00374960899353
time_elpased: 1.893
batch start
#iterations: 65
currently lose_sum: 95.31478375196457
time_elpased: 1.877
batch start
#iterations: 66
currently lose_sum: 96.08365219831467
time_elpased: 1.898
batch start
#iterations: 67
currently lose_sum: 96.0184018611908
time_elpased: 1.916
batch start
#iterations: 68
currently lose_sum: 95.34969437122345
time_elpased: 1.897
batch start
#iterations: 69
currently lose_sum: 95.45692420005798
time_elpased: 1.895
batch start
#iterations: 70
currently lose_sum: 95.40532743930817
time_elpased: 1.901
batch start
#iterations: 71
currently lose_sum: 95.51227736473083
time_elpased: 1.934
batch start
#iterations: 72
currently lose_sum: 95.31724989414215
time_elpased: 1.901
batch start
#iterations: 73
currently lose_sum: 95.25184887647629
time_elpased: 1.892
batch start
#iterations: 74
currently lose_sum: 95.26048737764359
time_elpased: 1.869
batch start
#iterations: 75
currently lose_sum: 95.36047399044037
time_elpased: 1.899
batch start
#iterations: 76
currently lose_sum: 95.7466681599617
time_elpased: 1.904
batch start
#iterations: 77
currently lose_sum: 95.44888758659363
time_elpased: 1.881
batch start
#iterations: 78
currently lose_sum: 95.58551239967346
time_elpased: 1.873
batch start
#iterations: 79
currently lose_sum: 95.26809906959534
time_elpased: 1.905
start validation test
0.633608247423
0.654983961031
0.567356179891
0.608029116577
0.633724563146
62.136
batch start
#iterations: 80
currently lose_sum: 95.62292367219925
time_elpased: 1.906
batch start
#iterations: 81
currently lose_sum: 95.7410877943039
time_elpased: 1.882
batch start
#iterations: 82
currently lose_sum: 95.58955025672913
time_elpased: 1.88
batch start
#iterations: 83
currently lose_sum: 95.61358612775803
time_elpased: 1.889
batch start
#iterations: 84
currently lose_sum: 95.55623894929886
time_elpased: 1.929
batch start
#iterations: 85
currently lose_sum: 95.6914929151535
time_elpased: 1.888
batch start
#iterations: 86
currently lose_sum: 95.74740755558014
time_elpased: 1.901
batch start
#iterations: 87
currently lose_sum: 95.36835843324661
time_elpased: 1.918
batch start
#iterations: 88
currently lose_sum: 95.48413640260696
time_elpased: 1.894
batch start
#iterations: 89
currently lose_sum: 95.52751606702805
time_elpased: 1.883
batch start
#iterations: 90
currently lose_sum: 95.06305658817291
time_elpased: 1.913
batch start
#iterations: 91
currently lose_sum: 95.5846775174141
time_elpased: 1.888
batch start
#iterations: 92
currently lose_sum: 95.38006836175919
time_elpased: 1.889
batch start
#iterations: 93
currently lose_sum: 95.34101587533951
time_elpased: 1.913
batch start
#iterations: 94
currently lose_sum: 95.14956003427505
time_elpased: 1.899
batch start
#iterations: 95
currently lose_sum: 95.44532054662704
time_elpased: 1.892
batch start
#iterations: 96
currently lose_sum: 95.55825370550156
time_elpased: 1.883
batch start
#iterations: 97
currently lose_sum: 95.36547428369522
time_elpased: 1.895
batch start
#iterations: 98
currently lose_sum: 95.85768228769302
time_elpased: 1.892
batch start
#iterations: 99
currently lose_sum: 95.63954001665115
time_elpased: 1.92
start validation test
0.665154639175
0.643295666874
0.744056807657
0.690017178851
0.665016114249
61.009
batch start
#iterations: 100
currently lose_sum: 95.35025590658188
time_elpased: 1.905
batch start
#iterations: 101
currently lose_sum: 95.11149060726166
time_elpased: 1.9
batch start
#iterations: 102
currently lose_sum: 95.25065475702286
time_elpased: 1.912
batch start
#iterations: 103
currently lose_sum: 95.1792864203453
time_elpased: 1.886
batch start
#iterations: 104
currently lose_sum: 94.75730192661285
time_elpased: 1.921
batch start
#iterations: 105
currently lose_sum: 95.4053190946579
time_elpased: 1.934
batch start
#iterations: 106
currently lose_sum: 95.32201647758484
time_elpased: 1.902
batch start
#iterations: 107
currently lose_sum: 95.13868713378906
time_elpased: 1.889
batch start
#iterations: 108
currently lose_sum: 95.28926879167557
time_elpased: 1.9
batch start
#iterations: 109
currently lose_sum: 95.12013399600983
time_elpased: 1.901
batch start
#iterations: 110
currently lose_sum: 95.06591993570328
time_elpased: 1.941
batch start
#iterations: 111
currently lose_sum: 95.24950700998306
time_elpased: 1.893
batch start
#iterations: 112
currently lose_sum: 95.54427021741867
time_elpased: 1.881
batch start
#iterations: 113
currently lose_sum: 95.07360589504242
time_elpased: 1.89
batch start
#iterations: 114
currently lose_sum: 95.4357602596283
time_elpased: 1.882
batch start
#iterations: 115
currently lose_sum: 95.40223830938339
time_elpased: 1.899
batch start
#iterations: 116
currently lose_sum: 94.92754757404327
time_elpased: 1.886
batch start
#iterations: 117
currently lose_sum: 95.22757560014725
time_elpased: 1.91
batch start
#iterations: 118
currently lose_sum: 95.1570338010788
time_elpased: 1.911
batch start
#iterations: 119
currently lose_sum: 95.4064701795578
time_elpased: 1.868
start validation test
0.666649484536
0.630606011895
0.807450859319
0.708154700122
0.666402286006
60.655
batch start
#iterations: 120
currently lose_sum: 95.47648841142654
time_elpased: 1.902
batch start
#iterations: 121
currently lose_sum: 95.1957221031189
time_elpased: 1.871
batch start
#iterations: 122
currently lose_sum: 95.45159459114075
time_elpased: 1.866
batch start
#iterations: 123
currently lose_sum: 94.81816506385803
time_elpased: 1.891
batch start
#iterations: 124
currently lose_sum: 95.74738156795502
time_elpased: 1.928
batch start
#iterations: 125
currently lose_sum: 95.01262438297272
time_elpased: 1.924
batch start
#iterations: 126
currently lose_sum: 95.30113184452057
time_elpased: 1.871
batch start
#iterations: 127
currently lose_sum: 95.16783899068832
time_elpased: 1.891
batch start
#iterations: 128
currently lose_sum: 95.24507158994675
time_elpased: 1.907
batch start
#iterations: 129
currently lose_sum: 95.08919256925583
time_elpased: 1.901
batch start
#iterations: 130
currently lose_sum: 95.02527332305908
time_elpased: 1.897
batch start
#iterations: 131
currently lose_sum: 95.144950568676
time_elpased: 1.904
batch start
#iterations: 132
currently lose_sum: 95.11708158254623
time_elpased: 1.884
batch start
#iterations: 133
currently lose_sum: 95.5395113825798
time_elpased: 1.925
batch start
#iterations: 134
currently lose_sum: 95.01406693458557
time_elpased: 1.891
batch start
#iterations: 135
currently lose_sum: 95.03371578454971
time_elpased: 1.918
batch start
#iterations: 136
currently lose_sum: 95.42797422409058
time_elpased: 1.976
batch start
#iterations: 137
currently lose_sum: 94.91688197851181
time_elpased: 1.876
batch start
#iterations: 138
currently lose_sum: 95.33463180065155
time_elpased: 1.921
batch start
#iterations: 139
currently lose_sum: 95.38098925352097
time_elpased: 1.881
start validation test
0.66
0.64187653423
0.726561695997
0.68159876424
0.659883140676
61.229
batch start
#iterations: 140
currently lose_sum: 95.32883822917938
time_elpased: 1.935
batch start
#iterations: 141
currently lose_sum: 95.03191071748734
time_elpased: 1.877
batch start
#iterations: 142
currently lose_sum: 95.14534378051758
time_elpased: 1.927
batch start
#iterations: 143
currently lose_sum: 95.12909889221191
time_elpased: 1.926
batch start
#iterations: 144
currently lose_sum: 94.81179928779602
time_elpased: 1.856
batch start
#iterations: 145
currently lose_sum: 95.1083772778511
time_elpased: 1.91
batch start
#iterations: 146
currently lose_sum: 94.887619972229
time_elpased: 1.877
batch start
#iterations: 147
currently lose_sum: 95.44503253698349
time_elpased: 1.864
batch start
#iterations: 148
currently lose_sum: 94.86821764707565
time_elpased: 1.878
batch start
#iterations: 149
currently lose_sum: 95.30395042896271
time_elpased: 1.888
batch start
#iterations: 150
currently lose_sum: 95.11718195676804
time_elpased: 1.882
batch start
#iterations: 151
currently lose_sum: 95.1488224864006
time_elpased: 1.86
batch start
#iterations: 152
currently lose_sum: 95.23814231157303
time_elpased: 1.868
batch start
#iterations: 153
currently lose_sum: 95.08054941892624
time_elpased: 1.896
batch start
#iterations: 154
currently lose_sum: 95.36171239614487
time_elpased: 1.875
batch start
#iterations: 155
currently lose_sum: 95.11598229408264
time_elpased: 1.859
batch start
#iterations: 156
currently lose_sum: 95.03752064704895
time_elpased: 1.865
batch start
#iterations: 157
currently lose_sum: 95.00955039262772
time_elpased: 1.913
batch start
#iterations: 158
currently lose_sum: 95.02702295780182
time_elpased: 1.899
batch start
#iterations: 159
currently lose_sum: 94.93255537748337
time_elpased: 1.874
start validation test
0.668711340206
0.644146512443
0.756509210662
0.69582090965
0.668557197503
60.675
batch start
#iterations: 160
currently lose_sum: 94.901322722435
time_elpased: 1.934
batch start
#iterations: 161
currently lose_sum: 95.14828342199326
time_elpased: 1.9
batch start
#iterations: 162
currently lose_sum: 94.95862454175949
time_elpased: 1.901
batch start
#iterations: 163
currently lose_sum: 94.75029557943344
time_elpased: 1.921
batch start
#iterations: 164
currently lose_sum: 95.03332775831223
time_elpased: 1.883
batch start
#iterations: 165
currently lose_sum: 95.00363713502884
time_elpased: 1.88
batch start
#iterations: 166
currently lose_sum: 94.93974882364273
time_elpased: 1.871
batch start
#iterations: 167
currently lose_sum: 94.75977343320847
time_elpased: 1.861
batch start
#iterations: 168
currently lose_sum: 94.74738627672195
time_elpased: 1.881
batch start
#iterations: 169
currently lose_sum: 94.78757745027542
time_elpased: 1.914
batch start
#iterations: 170
currently lose_sum: 95.09468340873718
time_elpased: 1.908
batch start
#iterations: 171
currently lose_sum: 94.8780187368393
time_elpased: 1.885
batch start
#iterations: 172
currently lose_sum: 94.7370463013649
time_elpased: 2.005
batch start
#iterations: 173
currently lose_sum: 94.72511303424835
time_elpased: 1.9
batch start
#iterations: 174
currently lose_sum: 95.00471127033234
time_elpased: 1.921
batch start
#iterations: 175
currently lose_sum: 95.09467798471451
time_elpased: 1.934
batch start
#iterations: 176
currently lose_sum: 94.93337911367416
time_elpased: 1.897
batch start
#iterations: 177
currently lose_sum: 94.79047006368637
time_elpased: 1.89
batch start
#iterations: 178
currently lose_sum: 94.86022138595581
time_elpased: 1.959
batch start
#iterations: 179
currently lose_sum: 94.68729728460312
time_elpased: 1.899
start validation test
0.661804123711
0.646165246388
0.717917052588
0.680154048652
0.66170560881
60.579
batch start
#iterations: 180
currently lose_sum: 95.07244825363159
time_elpased: 1.944
batch start
#iterations: 181
currently lose_sum: 94.31648987531662
time_elpased: 1.925
batch start
#iterations: 182
currently lose_sum: 95.08133429288864
time_elpased: 1.961
batch start
#iterations: 183
currently lose_sum: 94.69909918308258
time_elpased: 1.929
batch start
#iterations: 184
currently lose_sum: 94.63393211364746
time_elpased: 1.906
batch start
#iterations: 185
currently lose_sum: 94.4432253241539
time_elpased: 1.902
batch start
#iterations: 186
currently lose_sum: 94.40265601873398
time_elpased: 1.965
batch start
#iterations: 187
currently lose_sum: 94.49879264831543
time_elpased: 1.907
batch start
#iterations: 188
currently lose_sum: 94.64828830957413
time_elpased: 1.935
batch start
#iterations: 189
currently lose_sum: 94.86186188459396
time_elpased: 1.913
batch start
#iterations: 190
currently lose_sum: 94.75822329521179
time_elpased: 1.944
batch start
#iterations: 191
currently lose_sum: 94.82293170690536
time_elpased: 1.904
batch start
#iterations: 192
currently lose_sum: 94.25735276937485
time_elpased: 1.9
batch start
#iterations: 193
currently lose_sum: 94.6805117726326
time_elpased: 1.927
batch start
#iterations: 194
currently lose_sum: 94.36926746368408
time_elpased: 1.91
batch start
#iterations: 195
currently lose_sum: 94.69985496997833
time_elpased: 1.905
batch start
#iterations: 196
currently lose_sum: 94.92420989274979
time_elpased: 1.923
batch start
#iterations: 197
currently lose_sum: 95.42200821638107
time_elpased: 1.918
batch start
#iterations: 198
currently lose_sum: 94.63277089595795
time_elpased: 1.934
batch start
#iterations: 199
currently lose_sum: 94.7901519536972
time_elpased: 1.906
start validation test
0.647371134021
0.650733752621
0.638880312854
0.644752557512
0.647386040967
61.084
batch start
#iterations: 200
currently lose_sum: 94.72863584756851
time_elpased: 1.946
batch start
#iterations: 201
currently lose_sum: 94.91591030359268
time_elpased: 1.918
batch start
#iterations: 202
currently lose_sum: 94.47050589323044
time_elpased: 1.906
batch start
#iterations: 203
currently lose_sum: 94.95102047920227
time_elpased: 1.885
batch start
#iterations: 204
currently lose_sum: 94.63286846876144
time_elpased: 1.931
batch start
#iterations: 205
currently lose_sum: 94.74744147062302
time_elpased: 1.894
batch start
#iterations: 206
currently lose_sum: 94.76684200763702
time_elpased: 1.906
batch start
#iterations: 207
currently lose_sum: 94.69004935026169
time_elpased: 1.891
batch start
#iterations: 208
currently lose_sum: 94.27298331260681
time_elpased: 1.907
batch start
#iterations: 209
currently lose_sum: 94.6341763138771
time_elpased: 1.913
batch start
#iterations: 210
currently lose_sum: 94.23450779914856
time_elpased: 1.907
batch start
#iterations: 211
currently lose_sum: 94.43095886707306
time_elpased: 1.944
batch start
#iterations: 212
currently lose_sum: 94.20653700828552
time_elpased: 1.943
batch start
#iterations: 213
currently lose_sum: 94.5914049744606
time_elpased: 1.921
batch start
#iterations: 214
currently lose_sum: 94.1840113401413
time_elpased: 1.89
batch start
#iterations: 215
currently lose_sum: 94.5178257226944
time_elpased: 1.886
batch start
#iterations: 216
currently lose_sum: 94.96351861953735
time_elpased: 1.922
batch start
#iterations: 217
currently lose_sum: 94.54042738676071
time_elpased: 1.907
batch start
#iterations: 218
currently lose_sum: 94.98636251688004
time_elpased: 1.897
batch start
#iterations: 219
currently lose_sum: 94.85025411844254
time_elpased: 1.884
start validation test
0.659690721649
0.618884054652
0.834413913759
0.710667017267
0.659383968137
60.597
batch start
#iterations: 220
currently lose_sum: 95.28386914730072
time_elpased: 1.977
batch start
#iterations: 221
currently lose_sum: 94.79179918766022
time_elpased: 1.877
batch start
#iterations: 222
currently lose_sum: 94.84887540340424
time_elpased: 1.918
batch start
#iterations: 223
currently lose_sum: 94.52661317586899
time_elpased: 1.88
batch start
#iterations: 224
currently lose_sum: 94.63612258434296
time_elpased: 1.967
batch start
#iterations: 225
currently lose_sum: 94.54164934158325
time_elpased: 1.879
batch start
#iterations: 226
currently lose_sum: 94.50725215673447
time_elpased: 1.902
batch start
#iterations: 227
currently lose_sum: 94.93650704622269
time_elpased: 1.916
batch start
#iterations: 228
currently lose_sum: 94.68176627159119
time_elpased: 1.901
batch start
#iterations: 229
currently lose_sum: 94.44462531805038
time_elpased: 1.876
batch start
#iterations: 230
currently lose_sum: 94.20274251699448
time_elpased: 1.978
batch start
#iterations: 231
currently lose_sum: 94.70960360765457
time_elpased: 1.898
batch start
#iterations: 232
currently lose_sum: 94.73025894165039
time_elpased: 1.891
batch start
#iterations: 233
currently lose_sum: 94.58510446548462
time_elpased: 1.912
batch start
#iterations: 234
currently lose_sum: 94.82104712724686
time_elpased: 1.908
batch start
#iterations: 235
currently lose_sum: 94.50532793998718
time_elpased: 1.876
batch start
#iterations: 236
currently lose_sum: 94.69490993022919
time_elpased: 1.964
batch start
#iterations: 237
currently lose_sum: 94.18168288469315
time_elpased: 1.931
batch start
#iterations: 238
currently lose_sum: 94.75812464952469
time_elpased: 1.943
batch start
#iterations: 239
currently lose_sum: 94.46000450849533
time_elpased: 1.946
start validation test
0.625309278351
0.668085690744
0.50066893074
0.572386610977
0.625528103705
61.586
batch start
#iterations: 240
currently lose_sum: 94.4639801979065
time_elpased: 1.955
batch start
#iterations: 241
currently lose_sum: 94.65524834394455
time_elpased: 1.91
batch start
#iterations: 242
currently lose_sum: 94.54984498023987
time_elpased: 1.925
batch start
#iterations: 243
currently lose_sum: 94.61370009183884
time_elpased: 1.873
batch start
#iterations: 244
currently lose_sum: 94.90855699777603
time_elpased: 1.882
batch start
#iterations: 245
currently lose_sum: 94.74137037992477
time_elpased: 1.896
batch start
#iterations: 246
currently lose_sum: 94.77219605445862
time_elpased: 1.918
batch start
#iterations: 247
currently lose_sum: 94.78881293535233
time_elpased: 1.906
batch start
#iterations: 248
currently lose_sum: 95.08804666996002
time_elpased: 1.948
batch start
#iterations: 249
currently lose_sum: 94.31902974843979
time_elpased: 1.899
batch start
#iterations: 250
currently lose_sum: 94.6288440823555
time_elpased: 1.995
batch start
#iterations: 251
currently lose_sum: 94.26914489269257
time_elpased: 1.91
batch start
#iterations: 252
currently lose_sum: 94.06848913431168
time_elpased: 1.898
batch start
#iterations: 253
currently lose_sum: 94.45147716999054
time_elpased: 1.883
batch start
#iterations: 254
currently lose_sum: 94.59184062480927
time_elpased: 1.873
batch start
#iterations: 255
currently lose_sum: 93.89310473203659
time_elpased: 1.896
batch start
#iterations: 256
currently lose_sum: 94.45642578601837
time_elpased: 1.887
batch start
#iterations: 257
currently lose_sum: 94.25610685348511
time_elpased: 1.9
batch start
#iterations: 258
currently lose_sum: 94.47350227832794
time_elpased: 1.885
batch start
#iterations: 259
currently lose_sum: 94.63016366958618
time_elpased: 1.951
start validation test
0.649484536082
0.634263094909
0.709066584337
0.669582118562
0.649379930607
60.663
batch start
#iterations: 260
currently lose_sum: 94.25961518287659
time_elpased: 1.941
batch start
#iterations: 261
currently lose_sum: 94.50688344240189
time_elpased: 1.942
batch start
#iterations: 262
currently lose_sum: 94.31424885988235
time_elpased: 1.925
batch start
#iterations: 263
currently lose_sum: 94.56620955467224
time_elpased: 1.906
batch start
#iterations: 264
currently lose_sum: 94.53749394416809
time_elpased: 1.897
batch start
#iterations: 265
currently lose_sum: 94.56935495138168
time_elpased: 1.935
batch start
#iterations: 266
currently lose_sum: 94.6899082660675
time_elpased: 1.918
batch start
#iterations: 267
currently lose_sum: 94.42907249927521
time_elpased: 1.92
batch start
#iterations: 268
currently lose_sum: 94.50683146715164
time_elpased: 1.88
batch start
#iterations: 269
currently lose_sum: 94.44588476419449
time_elpased: 1.907
batch start
#iterations: 270
currently lose_sum: 94.68928968906403
time_elpased: 1.883
batch start
#iterations: 271
currently lose_sum: 94.12417209148407
time_elpased: 1.896
batch start
#iterations: 272
currently lose_sum: 94.28120863437653
time_elpased: 1.905
batch start
#iterations: 273
currently lose_sum: 94.50238943099976
time_elpased: 1.885
batch start
#iterations: 274
currently lose_sum: 94.65260195732117
time_elpased: 1.904
batch start
#iterations: 275
currently lose_sum: 94.91393965482712
time_elpased: 1.906
batch start
#iterations: 276
currently lose_sum: 94.47356802225113
time_elpased: 1.92
batch start
#iterations: 277
currently lose_sum: 94.76595151424408
time_elpased: 1.92
batch start
#iterations: 278
currently lose_sum: 94.58405834436417
time_elpased: 1.912
batch start
#iterations: 279
currently lose_sum: 94.70520555973053
time_elpased: 1.918
start validation test
0.667886597938
0.632916531341
0.802202325821
0.707575001135
0.667650785961
60.495
batch start
#iterations: 280
currently lose_sum: 94.29314517974854
time_elpased: 1.973
batch start
#iterations: 281
currently lose_sum: 94.35534781217575
time_elpased: 1.888
batch start
#iterations: 282
currently lose_sum: 94.48519867658615
time_elpased: 1.899
batch start
#iterations: 283
currently lose_sum: 94.86481386423111
time_elpased: 1.873
batch start
#iterations: 284
currently lose_sum: 94.11728721857071
time_elpased: 1.878
batch start
#iterations: 285
currently lose_sum: 94.56669986248016
time_elpased: 1.863
batch start
#iterations: 286
currently lose_sum: 94.37426829338074
time_elpased: 1.873
batch start
#iterations: 287
currently lose_sum: 94.56413930654526
time_elpased: 1.886
batch start
#iterations: 288
currently lose_sum: 94.38219147920609
time_elpased: 1.897
batch start
#iterations: 289
currently lose_sum: 94.52941977977753
time_elpased: 1.855
batch start
#iterations: 290
currently lose_sum: 94.39006567001343
time_elpased: 1.905
batch start
#iterations: 291
currently lose_sum: 94.09456431865692
time_elpased: 1.869
batch start
#iterations: 292
currently lose_sum: 94.1361648440361
time_elpased: 1.913
batch start
#iterations: 293
currently lose_sum: 94.03377562761307
time_elpased: 1.915
batch start
#iterations: 294
currently lose_sum: 94.59247487783432
time_elpased: 1.92
batch start
#iterations: 295
currently lose_sum: 94.39912801980972
time_elpased: 1.897
batch start
#iterations: 296
currently lose_sum: 94.31280773878098
time_elpased: 1.874
batch start
#iterations: 297
currently lose_sum: 94.2641931772232
time_elpased: 1.882
batch start
#iterations: 298
currently lose_sum: 94.68022531270981
time_elpased: 1.901
batch start
#iterations: 299
currently lose_sum: 94.21713882684708
time_elpased: 1.936
start validation test
0.66618556701
0.634581845362
0.786353812905
0.702362349481
0.66597459312
60.333
batch start
#iterations: 300
currently lose_sum: 94.57105773687363
time_elpased: 1.969
batch start
#iterations: 301
currently lose_sum: 94.18314105272293
time_elpased: 1.919
batch start
#iterations: 302
currently lose_sum: 94.33534407615662
time_elpased: 1.874
batch start
#iterations: 303
currently lose_sum: 94.48407119512558
time_elpased: 1.912
batch start
#iterations: 304
currently lose_sum: 94.14429897069931
time_elpased: 1.886
batch start
#iterations: 305
currently lose_sum: 94.25747907161713
time_elpased: 1.907
batch start
#iterations: 306
currently lose_sum: 94.37158274650574
time_elpased: 1.979
batch start
#iterations: 307
currently lose_sum: 94.22931784391403
time_elpased: 1.914
batch start
#iterations: 308
currently lose_sum: 94.31004995107651
time_elpased: 1.895
batch start
#iterations: 309
currently lose_sum: 94.77915906906128
time_elpased: 1.942
batch start
#iterations: 310
currently lose_sum: 94.13822048902512
time_elpased: 1.908
batch start
#iterations: 311
currently lose_sum: 94.24454218149185
time_elpased: 1.898
batch start
#iterations: 312
currently lose_sum: 94.44184523820877
time_elpased: 1.9
batch start
#iterations: 313
currently lose_sum: 94.4363352060318
time_elpased: 1.867
batch start
#iterations: 314
currently lose_sum: 94.63691502809525
time_elpased: 1.908
batch start
#iterations: 315
currently lose_sum: 94.46330493688583
time_elpased: 1.911
batch start
#iterations: 316
currently lose_sum: 94.19896620512009
time_elpased: 1.94
batch start
#iterations: 317
currently lose_sum: 94.13232260942459
time_elpased: 1.905
batch start
#iterations: 318
currently lose_sum: 94.40853387117386
time_elpased: 1.913
batch start
#iterations: 319
currently lose_sum: 94.48926764726639
time_elpased: 1.889
start validation test
0.662989690722
0.639908458762
0.748173304518
0.689818768384
0.662840137749
60.339
batch start
#iterations: 320
currently lose_sum: 94.31528621912003
time_elpased: 1.895
batch start
#iterations: 321
currently lose_sum: 94.27187240123749
time_elpased: 1.945
batch start
#iterations: 322
currently lose_sum: 94.82716113328934
time_elpased: 1.899
batch start
#iterations: 323
currently lose_sum: 94.36388427019119
time_elpased: 1.912
batch start
#iterations: 324
currently lose_sum: 94.34614115953445
time_elpased: 1.898
batch start
#iterations: 325
currently lose_sum: 94.25412291288376
time_elpased: 1.885
batch start
#iterations: 326
currently lose_sum: 93.93041467666626
time_elpased: 1.93
batch start
#iterations: 327
currently lose_sum: 94.81073993444443
time_elpased: 1.912
batch start
#iterations: 328
currently lose_sum: 94.19520592689514
time_elpased: 1.924
batch start
#iterations: 329
currently lose_sum: 94.59726458787918
time_elpased: 1.946
batch start
#iterations: 330
currently lose_sum: 94.21804118156433
time_elpased: 1.97
batch start
#iterations: 331
currently lose_sum: 94.44572132825851
time_elpased: 1.908
batch start
#iterations: 332
currently lose_sum: 94.09279751777649
time_elpased: 1.875
batch start
#iterations: 333
currently lose_sum: 94.38756734132767
time_elpased: 1.926
batch start
#iterations: 334
currently lose_sum: 94.53243392705917
time_elpased: 1.925
batch start
#iterations: 335
currently lose_sum: 94.44467431306839
time_elpased: 1.925
batch start
#iterations: 336
currently lose_sum: 94.09560990333557
time_elpased: 1.891
batch start
#iterations: 337
currently lose_sum: 94.25535947084427
time_elpased: 1.92
batch start
#iterations: 338
currently lose_sum: 94.07144892215729
time_elpased: 1.894
batch start
#iterations: 339
currently lose_sum: 94.12277299165726
time_elpased: 1.897
start validation test
0.666701030928
0.643633471768
0.749614078419
0.692592944756
0.666555464285
60.299
batch start
#iterations: 340
currently lose_sum: 94.35225015878677
time_elpased: 1.896
batch start
#iterations: 341
currently lose_sum: 94.20316487550735
time_elpased: 1.879
batch start
#iterations: 342
currently lose_sum: 94.35227888822556
time_elpased: 1.958
batch start
#iterations: 343
currently lose_sum: 94.60664027929306
time_elpased: 1.94
batch start
#iterations: 344
currently lose_sum: 94.28799802064896
time_elpased: 1.924
batch start
#iterations: 345
currently lose_sum: 94.71599918603897
time_elpased: 1.903
batch start
#iterations: 346
currently lose_sum: 94.16756427288055
time_elpased: 1.957
batch start
#iterations: 347
currently lose_sum: 94.39280652999878
time_elpased: 1.91
batch start
#iterations: 348
currently lose_sum: 94.13471162319183
time_elpased: 1.92
batch start
#iterations: 349
currently lose_sum: 94.4201871752739
time_elpased: 1.903
batch start
#iterations: 350
currently lose_sum: 94.43181449174881
time_elpased: 1.914
batch start
#iterations: 351
currently lose_sum: 94.35319995880127
time_elpased: 1.928
batch start
#iterations: 352
currently lose_sum: 94.29703956842422
time_elpased: 1.941
batch start
#iterations: 353
currently lose_sum: 94.09442287683487
time_elpased: 1.91
batch start
#iterations: 354
currently lose_sum: 94.49114120006561
time_elpased: 1.944
batch start
#iterations: 355
currently lose_sum: 94.35370200872421
time_elpased: 1.921
batch start
#iterations: 356
currently lose_sum: 94.59687292575836
time_elpased: 1.942
batch start
#iterations: 357
currently lose_sum: 94.34022396802902
time_elpased: 1.901
batch start
#iterations: 358
currently lose_sum: 94.52623867988586
time_elpased: 1.912
batch start
#iterations: 359
currently lose_sum: 94.38245767354965
time_elpased: 1.907
start validation test
0.661030927835
0.650734235531
0.697746217969
0.673420738975
0.66096646848
61.072
batch start
#iterations: 360
currently lose_sum: 94.43204391002655
time_elpased: 1.951
batch start
#iterations: 361
currently lose_sum: 94.0906171798706
time_elpased: 1.886
batch start
#iterations: 362
currently lose_sum: 94.3558977842331
time_elpased: 1.9
batch start
#iterations: 363
currently lose_sum: 94.23817420005798
time_elpased: 1.93
batch start
#iterations: 364
currently lose_sum: 94.11972576379776
time_elpased: 1.902
batch start
#iterations: 365
currently lose_sum: 94.19608241319656
time_elpased: 1.872
batch start
#iterations: 366
currently lose_sum: 94.2974973320961
time_elpased: 1.932
batch start
#iterations: 367
currently lose_sum: 94.29085922241211
time_elpased: 1.898
batch start
#iterations: 368
currently lose_sum: 94.22373604774475
time_elpased: 1.884
batch start
#iterations: 369
currently lose_sum: 94.60856115818024
time_elpased: 1.924
batch start
#iterations: 370
currently lose_sum: 94.01344537734985
time_elpased: 1.926
batch start
#iterations: 371
currently lose_sum: 94.28291940689087
time_elpased: 1.915
batch start
#iterations: 372
currently lose_sum: 93.9768203496933
time_elpased: 1.915
batch start
#iterations: 373
currently lose_sum: 94.11333453655243
time_elpased: 1.91
batch start
#iterations: 374
currently lose_sum: 94.27706253528595
time_elpased: 1.915
batch start
#iterations: 375
currently lose_sum: 94.41248625516891
time_elpased: 1.908
batch start
#iterations: 376
currently lose_sum: 94.04828941822052
time_elpased: 1.895
batch start
#iterations: 377
currently lose_sum: 94.3718581199646
time_elpased: 1.908
batch start
#iterations: 378
currently lose_sum: 94.23748689889908
time_elpased: 1.887
batch start
#iterations: 379
currently lose_sum: 93.9574522972107
time_elpased: 1.882
start validation test
0.667268041237
0.637220259128
0.779458680663
0.701198907559
0.667071073265
60.290
batch start
#iterations: 380
currently lose_sum: 94.30331939458847
time_elpased: 1.906
batch start
#iterations: 381
currently lose_sum: 94.12821847200394
time_elpased: 1.883
batch start
#iterations: 382
currently lose_sum: 94.35642212629318
time_elpased: 1.905
batch start
#iterations: 383
currently lose_sum: 94.3405414223671
time_elpased: 1.89
batch start
#iterations: 384
currently lose_sum: 95.04124581813812
time_elpased: 1.924
batch start
#iterations: 385
currently lose_sum: 93.7701461315155
time_elpased: 1.914
batch start
#iterations: 386
currently lose_sum: 94.12649351358414
time_elpased: 1.931
batch start
#iterations: 387
currently lose_sum: 94.40133118629456
time_elpased: 1.897
batch start
#iterations: 388
currently lose_sum: 94.15997874736786
time_elpased: 1.915
batch start
#iterations: 389
currently lose_sum: 94.56303781270981
time_elpased: 1.91
batch start
#iterations: 390
currently lose_sum: 94.33180886507034
time_elpased: 1.881
batch start
#iterations: 391
currently lose_sum: 94.23882114887238
time_elpased: 1.902
batch start
#iterations: 392
currently lose_sum: 93.93589377403259
time_elpased: 1.938
batch start
#iterations: 393
currently lose_sum: 94.1138282418251
time_elpased: 1.914
batch start
#iterations: 394
currently lose_sum: 93.65505480766296
time_elpased: 1.907
batch start
#iterations: 395
currently lose_sum: 93.839719414711
time_elpased: 1.94
batch start
#iterations: 396
currently lose_sum: 94.66659367084503
time_elpased: 1.909
batch start
#iterations: 397
currently lose_sum: 94.48767977952957
time_elpased: 1.932
batch start
#iterations: 398
currently lose_sum: 94.0801008939743
time_elpased: 1.96
batch start
#iterations: 399
currently lose_sum: 94.42296677827835
time_elpased: 1.934
start validation test
0.667525773196
0.643151345193
0.755274261603
0.694717909883
0.66737171719
60.396
acc: 0.668
pre: 0.638
rec: 0.779
F1: 0.702
auc: 0.668
