start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.40943771600723
time_elpased: 2.225
batch start
#iterations: 1
currently lose_sum: 99.94665521383286
time_elpased: 2.264
batch start
#iterations: 2
currently lose_sum: 99.69082468748093
time_elpased: 2.208
batch start
#iterations: 3
currently lose_sum: 99.31278663873672
time_elpased: 2.252
batch start
#iterations: 4
currently lose_sum: 98.91297084093094
time_elpased: 2.236
batch start
#iterations: 5
currently lose_sum: 98.88051742315292
time_elpased: 2.111
batch start
#iterations: 6
currently lose_sum: 98.45306926965714
time_elpased: 2.123
batch start
#iterations: 7
currently lose_sum: 98.55245876312256
time_elpased: 2.051
batch start
#iterations: 8
currently lose_sum: 98.41577696800232
time_elpased: 2.161
batch start
#iterations: 9
currently lose_sum: 98.1718539595604
time_elpased: 2.303
batch start
#iterations: 10
currently lose_sum: 97.99460023641586
time_elpased: 2.232
batch start
#iterations: 11
currently lose_sum: 97.85232198238373
time_elpased: 2.256
batch start
#iterations: 12
currently lose_sum: 98.14173227548599
time_elpased: 2.207
batch start
#iterations: 13
currently lose_sum: 97.33980637788773
time_elpased: 2.247
batch start
#iterations: 14
currently lose_sum: 97.53841841220856
time_elpased: 2.239
batch start
#iterations: 15
currently lose_sum: 97.42120349407196
time_elpased: 2.244
batch start
#iterations: 16
currently lose_sum: 97.20537900924683
time_elpased: 2.072
batch start
#iterations: 17
currently lose_sum: 96.98813211917877
time_elpased: 2.166
batch start
#iterations: 18
currently lose_sum: 96.99876290559769
time_elpased: 2.253
batch start
#iterations: 19
currently lose_sum: 97.131766974926
time_elpased: 2.249
start validation test
0.656030927835
0.636306645173
0.731192754966
0.680457788632
0.655898969655
62.306
batch start
#iterations: 20
currently lose_sum: 96.9960607290268
time_elpased: 2.268
batch start
#iterations: 21
currently lose_sum: 96.79019570350647
time_elpased: 2.223
batch start
#iterations: 22
currently lose_sum: 96.82154506444931
time_elpased: 2.217
batch start
#iterations: 23
currently lose_sum: 96.48872405290604
time_elpased: 2.286
batch start
#iterations: 24
currently lose_sum: 96.63830065727234
time_elpased: 2.188
batch start
#iterations: 25
currently lose_sum: 96.75157350301743
time_elpased: 2.132
batch start
#iterations: 26
currently lose_sum: 96.47163897752762
time_elpased: 2.27
batch start
#iterations: 27
currently lose_sum: 96.63846826553345
time_elpased: 2.251
batch start
#iterations: 28
currently lose_sum: 96.49628376960754
time_elpased: 2.195
batch start
#iterations: 29
currently lose_sum: 96.82676529884338
time_elpased: 2.229
batch start
#iterations: 30
currently lose_sum: 96.2766854763031
time_elpased: 2.181
batch start
#iterations: 31
currently lose_sum: 96.64073771238327
time_elpased: 2.183
batch start
#iterations: 32
currently lose_sum: 96.0226160287857
time_elpased: 2.158
batch start
#iterations: 33
currently lose_sum: 96.55064326524734
time_elpased: 2.139
batch start
#iterations: 34
currently lose_sum: 96.30975812673569
time_elpased: 2.12
batch start
#iterations: 35
currently lose_sum: 96.45622128248215
time_elpased: 2.306
batch start
#iterations: 36
currently lose_sum: 96.10190320014954
time_elpased: 2.147
batch start
#iterations: 37
currently lose_sum: 96.1040295958519
time_elpased: 2.187
batch start
#iterations: 38
currently lose_sum: 96.29113030433655
time_elpased: 2.19
batch start
#iterations: 39
currently lose_sum: 96.16753214597702
time_elpased: 2.172
start validation test
0.634536082474
0.661462814997
0.553771740249
0.602845619538
0.634677876734
62.074
batch start
#iterations: 40
currently lose_sum: 96.01301109790802
time_elpased: 2.206
batch start
#iterations: 41
currently lose_sum: 96.04620379209518
time_elpased: 2.002
batch start
#iterations: 42
currently lose_sum: 96.0002436041832
time_elpased: 1.917
batch start
#iterations: 43
currently lose_sum: 95.96446293592453
time_elpased: 1.989
batch start
#iterations: 44
currently lose_sum: 96.34456211328506
time_elpased: 1.955
batch start
#iterations: 45
currently lose_sum: 96.21869385242462
time_elpased: 1.974
batch start
#iterations: 46
currently lose_sum: 95.83856439590454
time_elpased: 1.996
batch start
#iterations: 47
currently lose_sum: 96.0918317437172
time_elpased: 1.985
batch start
#iterations: 48
currently lose_sum: 96.12305134534836
time_elpased: 2.009
batch start
#iterations: 49
currently lose_sum: 96.14544290304184
time_elpased: 1.953
batch start
#iterations: 50
currently lose_sum: 95.82720863819122
time_elpased: 1.959
batch start
#iterations: 51
currently lose_sum: 96.12387615442276
time_elpased: 1.931
batch start
#iterations: 52
currently lose_sum: 95.77117383480072
time_elpased: 1.951
batch start
#iterations: 53
currently lose_sum: 95.94564294815063
time_elpased: 1.956
batch start
#iterations: 54
currently lose_sum: 95.89794319868088
time_elpased: 1.956
batch start
#iterations: 55
currently lose_sum: 95.95839166641235
time_elpased: 1.93
batch start
#iterations: 56
currently lose_sum: 95.94801819324493
time_elpased: 1.873
batch start
#iterations: 57
currently lose_sum: 95.92346513271332
time_elpased: 1.909
batch start
#iterations: 58
currently lose_sum: 95.766237616539
time_elpased: 1.918
batch start
#iterations: 59
currently lose_sum: 96.01140767335892
time_elpased: 1.904
start validation test
0.661597938144
0.636049723757
0.758258721828
0.691798507112
0.66142823523
61.300
batch start
#iterations: 60
currently lose_sum: 95.96483880281448
time_elpased: 1.984
batch start
#iterations: 61
currently lose_sum: 95.87477684020996
time_elpased: 1.949
batch start
#iterations: 62
currently lose_sum: 96.2641726732254
time_elpased: 1.985
batch start
#iterations: 63
currently lose_sum: 95.65183562040329
time_elpased: 2.051
batch start
#iterations: 64
currently lose_sum: 95.68905371427536
time_elpased: 1.979
batch start
#iterations: 65
currently lose_sum: 95.7387342453003
time_elpased: 1.936
batch start
#iterations: 66
currently lose_sum: 95.76192235946655
time_elpased: 1.907
batch start
#iterations: 67
currently lose_sum: 95.80121755599976
time_elpased: 1.929
batch start
#iterations: 68
currently lose_sum: 95.63458156585693
time_elpased: 1.953
batch start
#iterations: 69
currently lose_sum: 95.87684088945389
time_elpased: 1.97
batch start
#iterations: 70
currently lose_sum: 95.46616023778915
time_elpased: 1.927
batch start
#iterations: 71
currently lose_sum: 95.7744887471199
time_elpased: 2.044
batch start
#iterations: 72
currently lose_sum: 95.53253668546677
time_elpased: 1.984
batch start
#iterations: 73
currently lose_sum: 95.79756504297256
time_elpased: 2.024
batch start
#iterations: 74
currently lose_sum: 95.93405503034592
time_elpased: 2.017
batch start
#iterations: 75
currently lose_sum: 95.75715899467468
time_elpased: 1.999
batch start
#iterations: 76
currently lose_sum: 95.77826577425003
time_elpased: 1.977
batch start
#iterations: 77
currently lose_sum: 95.82788842916489
time_elpased: 1.954
batch start
#iterations: 78
currently lose_sum: 95.67893040180206
time_elpased: 1.977
batch start
#iterations: 79
currently lose_sum: 96.06515961885452
time_elpased: 2.003
start validation test
0.663865979381
0.62932988022
0.800246989812
0.704571195578
0.663626541482
61.120
batch start
#iterations: 80
currently lose_sum: 95.7787538766861
time_elpased: 2.034
batch start
#iterations: 81
currently lose_sum: 95.76892286539078
time_elpased: 1.989
batch start
#iterations: 82
currently lose_sum: 95.74542516469955
time_elpased: 2.055
batch start
#iterations: 83
currently lose_sum: 95.44963669776917
time_elpased: 1.974
batch start
#iterations: 84
currently lose_sum: 95.7509161233902
time_elpased: 2.013
batch start
#iterations: 85
currently lose_sum: 95.68417930603027
time_elpased: 1.909
batch start
#iterations: 86
currently lose_sum: 95.47956657409668
time_elpased: 2.01
batch start
#iterations: 87
currently lose_sum: 95.89871555566788
time_elpased: 1.991
batch start
#iterations: 88
currently lose_sum: 95.5697523355484
time_elpased: 2.055
batch start
#iterations: 89
currently lose_sum: 95.67590248584747
time_elpased: 2.05
batch start
#iterations: 90
currently lose_sum: 95.6136925816536
time_elpased: 2.063
batch start
#iterations: 91
currently lose_sum: 95.74971359968185
time_elpased: 2.006
batch start
#iterations: 92
currently lose_sum: 95.65757691860199
time_elpased: 1.923
batch start
#iterations: 93
currently lose_sum: 95.6383084654808
time_elpased: 2.027
batch start
#iterations: 94
currently lose_sum: 95.62994354963303
time_elpased: 2.038
batch start
#iterations: 95
currently lose_sum: 95.27679407596588
time_elpased: 2.002
batch start
#iterations: 96
currently lose_sum: 95.49652725458145
time_elpased: 2.036
batch start
#iterations: 97
currently lose_sum: 95.32710987329483
time_elpased: 2.037
batch start
#iterations: 98
currently lose_sum: 95.7855486869812
time_elpased: 1.972
batch start
#iterations: 99
currently lose_sum: 95.59195971488953
time_elpased: 1.966
start validation test
0.659484536082
0.65329654085
0.682206442318
0.66743858236
0.659444644271
61.114
batch start
#iterations: 100
currently lose_sum: 95.73814100027084
time_elpased: 1.979
batch start
#iterations: 101
currently lose_sum: 95.43723452091217
time_elpased: 2.017
batch start
#iterations: 102
currently lose_sum: 95.59306859970093
time_elpased: 1.972
batch start
#iterations: 103
currently lose_sum: 95.59832608699799
time_elpased: 1.979
batch start
#iterations: 104
currently lose_sum: 95.35582160949707
time_elpased: 2.024
batch start
#iterations: 105
currently lose_sum: 95.20787757635117
time_elpased: 1.976
batch start
#iterations: 106
currently lose_sum: 95.41359460353851
time_elpased: 1.994
batch start
#iterations: 107
currently lose_sum: 95.54668551683426
time_elpased: 1.919
batch start
#iterations: 108
currently lose_sum: 95.46509993076324
time_elpased: 1.975
batch start
#iterations: 109
currently lose_sum: 95.41160464286804
time_elpased: 1.896
batch start
#iterations: 110
currently lose_sum: 95.34592354297638
time_elpased: 1.888
batch start
#iterations: 111
currently lose_sum: 95.20950585603714
time_elpased: 1.86
batch start
#iterations: 112
currently lose_sum: 95.27921682596207
time_elpased: 1.883
batch start
#iterations: 113
currently lose_sum: 94.96269136667252
time_elpased: 1.941
batch start
#iterations: 114
currently lose_sum: 95.20444542169571
time_elpased: 2.012
batch start
#iterations: 115
currently lose_sum: 95.37184149026871
time_elpased: 2.007
batch start
#iterations: 116
currently lose_sum: 95.04930198192596
time_elpased: 2.022
batch start
#iterations: 117
currently lose_sum: 95.08311700820923
time_elpased: 1.959
batch start
#iterations: 118
currently lose_sum: 95.02903801202774
time_elpased: 1.994
batch start
#iterations: 119
currently lose_sum: 95.36215770244598
time_elpased: 2.015
start validation test
0.667577319588
0.643610476358
0.753627662859
0.694287745911
0.667426244938
60.714
batch start
#iterations: 120
currently lose_sum: 95.02682733535767
time_elpased: 2.033
batch start
#iterations: 121
currently lose_sum: 95.2746474146843
time_elpased: 2.024
batch start
#iterations: 122
currently lose_sum: 95.14095914363861
time_elpased: 2.041
batch start
#iterations: 123
currently lose_sum: 95.59462481737137
time_elpased: 2.074
batch start
#iterations: 124
currently lose_sum: 95.30823367834091
time_elpased: 2.028
batch start
#iterations: 125
currently lose_sum: 95.46551114320755
time_elpased: 2.064
batch start
#iterations: 126
currently lose_sum: 95.59600418806076
time_elpased: 2.094
batch start
#iterations: 127
currently lose_sum: 95.62136924266815
time_elpased: 2.053
batch start
#iterations: 128
currently lose_sum: 95.02707880735397
time_elpased: 2.093
batch start
#iterations: 129
currently lose_sum: 95.65678095817566
time_elpased: 2.063
batch start
#iterations: 130
currently lose_sum: 95.01806366443634
time_elpased: 2.091
batch start
#iterations: 131
currently lose_sum: 95.8715226650238
time_elpased: 2.041
batch start
#iterations: 132
currently lose_sum: 95.16063284873962
time_elpased: 2.094
batch start
#iterations: 133
currently lose_sum: 94.93545144796371
time_elpased: 2.048
batch start
#iterations: 134
currently lose_sum: 95.08110451698303
time_elpased: 2.045
batch start
#iterations: 135
currently lose_sum: 95.14698439836502
time_elpased: 2.001
batch start
#iterations: 136
currently lose_sum: 95.42511105537415
time_elpased: 2.006
batch start
#iterations: 137
currently lose_sum: 95.05430328845978
time_elpased: 2.066
batch start
#iterations: 138
currently lose_sum: 95.40788930654526
time_elpased: 2.057
batch start
#iterations: 139
currently lose_sum: 95.17914754152298
time_elpased: 2.167
start validation test
0.665051546392
0.641097571667
0.752598538644
0.69238780534
0.664897844144
60.836
batch start
#iterations: 140
currently lose_sum: 95.17224436998367
time_elpased: 2.048
batch start
#iterations: 141
currently lose_sum: 95.31206119060516
time_elpased: 2.055
batch start
#iterations: 142
currently lose_sum: 95.1299290060997
time_elpased: 1.996
batch start
#iterations: 143
currently lose_sum: 95.22707670927048
time_elpased: 2.008
batch start
#iterations: 144
currently lose_sum: 95.3293085694313
time_elpased: 2.054
batch start
#iterations: 145
currently lose_sum: 95.34143215417862
time_elpased: 2.011
batch start
#iterations: 146
currently lose_sum: 94.95667511224747
time_elpased: 2.049
batch start
#iterations: 147
currently lose_sum: 95.40259653329849
time_elpased: 1.993
batch start
#iterations: 148
currently lose_sum: 95.25097924470901
time_elpased: 2.071
batch start
#iterations: 149
currently lose_sum: 95.25184524059296
time_elpased: 2.031
batch start
#iterations: 150
currently lose_sum: 95.09931606054306
time_elpased: 2.084
batch start
#iterations: 151
currently lose_sum: 95.22315609455109
time_elpased: 1.981
batch start
#iterations: 152
currently lose_sum: 95.14516490697861
time_elpased: 1.962
batch start
#iterations: 153
currently lose_sum: 95.54624700546265
time_elpased: 2.024
batch start
#iterations: 154
currently lose_sum: 95.09956121444702
time_elpased: 1.987
batch start
#iterations: 155
currently lose_sum: 95.13140803575516
time_elpased: 1.925
batch start
#iterations: 156
currently lose_sum: 95.22195065021515
time_elpased: 1.919
batch start
#iterations: 157
currently lose_sum: 94.75322759151459
time_elpased: 2.019
batch start
#iterations: 158
currently lose_sum: 94.87173146009445
time_elpased: 1.985
batch start
#iterations: 159
currently lose_sum: 95.01196998357773
time_elpased: 2.047
start validation test
0.668917525773
0.638706417383
0.780487804878
0.702514936779
0.66872164694
60.713
batch start
#iterations: 160
currently lose_sum: 94.74583971500397
time_elpased: 2.076
batch start
#iterations: 161
currently lose_sum: 95.30380910634995
time_elpased: 2.009
batch start
#iterations: 162
currently lose_sum: 95.43333995342255
time_elpased: 1.99
batch start
#iterations: 163
currently lose_sum: 95.31661832332611
time_elpased: 2.013
batch start
#iterations: 164
currently lose_sum: 94.95083451271057
time_elpased: 1.976
batch start
#iterations: 165
currently lose_sum: 95.24048733711243
time_elpased: 2.01
batch start
#iterations: 166
currently lose_sum: 95.01524716615677
time_elpased: 2.06
batch start
#iterations: 167
currently lose_sum: 95.11989849805832
time_elpased: 2.016
batch start
#iterations: 168
currently lose_sum: 95.20410925149918
time_elpased: 2.078
batch start
#iterations: 169
currently lose_sum: 94.8137389421463
time_elpased: 2.005
batch start
#iterations: 170
currently lose_sum: 95.34008187055588
time_elpased: 2.035
batch start
#iterations: 171
currently lose_sum: 95.4241554737091
time_elpased: 1.996
batch start
#iterations: 172
currently lose_sum: 95.32810711860657
time_elpased: 1.997
batch start
#iterations: 173
currently lose_sum: 95.27476000785828
time_elpased: 2.104
batch start
#iterations: 174
currently lose_sum: 94.78343665599823
time_elpased: 1.939
batch start
#iterations: 175
currently lose_sum: 94.84556645154953
time_elpased: 1.947
batch start
#iterations: 176
currently lose_sum: 94.88176691532135
time_elpased: 1.993
batch start
#iterations: 177
currently lose_sum: 95.08550721406937
time_elpased: 2.008
batch start
#iterations: 178
currently lose_sum: 94.71508800983429
time_elpased: 2.038
batch start
#iterations: 179
currently lose_sum: 94.869475543499
time_elpased: 2.013
start validation test
0.657422680412
0.655588205492
0.665843367294
0.660675993056
0.657407896598
60.905
batch start
#iterations: 180
currently lose_sum: 94.96989673376083
time_elpased: 1.959
batch start
#iterations: 181
currently lose_sum: 94.52730238437653
time_elpased: 2.008
batch start
#iterations: 182
currently lose_sum: 95.19605040550232
time_elpased: 1.916
batch start
#iterations: 183
currently lose_sum: 94.729312479496
time_elpased: 1.914
batch start
#iterations: 184
currently lose_sum: 95.26087760925293
time_elpased: 1.917
batch start
#iterations: 185
currently lose_sum: 94.58039021492004
time_elpased: 1.917
batch start
#iterations: 186
currently lose_sum: 95.36203038692474
time_elpased: 1.933
batch start
#iterations: 187
currently lose_sum: 95.11688286066055
time_elpased: 1.938
batch start
#iterations: 188
currently lose_sum: 95.12837564945221
time_elpased: 1.892
batch start
#iterations: 189
currently lose_sum: 94.94344252347946
time_elpased: 1.883
batch start
#iterations: 190
currently lose_sum: 95.08038312196732
time_elpased: 1.837
batch start
#iterations: 191
currently lose_sum: 95.18925708532333
time_elpased: 1.907
batch start
#iterations: 192
currently lose_sum: 95.03557229042053
time_elpased: 1.865
batch start
#iterations: 193
currently lose_sum: 94.94352686405182
time_elpased: 1.919
batch start
#iterations: 194
currently lose_sum: 94.48082965612411
time_elpased: 1.932
batch start
#iterations: 195
currently lose_sum: 95.0899453163147
time_elpased: 1.966
batch start
#iterations: 196
currently lose_sum: 94.92316722869873
time_elpased: 2.017
batch start
#iterations: 197
currently lose_sum: 95.27729260921478
time_elpased: 2.094
batch start
#iterations: 198
currently lose_sum: 94.98545479774475
time_elpased: 2.072
batch start
#iterations: 199
currently lose_sum: 94.6594750881195
time_elpased: 1.943
start validation test
0.669020618557
0.638139145013
0.783472265102
0.70337691135
0.668819681038
60.476
batch start
#iterations: 200
currently lose_sum: 94.98992270231247
time_elpased: 1.92
batch start
#iterations: 201
currently lose_sum: 94.99323040246964
time_elpased: 1.931
batch start
#iterations: 202
currently lose_sum: 94.81275546550751
time_elpased: 1.994
batch start
#iterations: 203
currently lose_sum: 95.04940325021744
time_elpased: 2.013
batch start
#iterations: 204
currently lose_sum: 94.93034541606903
time_elpased: 2.005
batch start
#iterations: 205
currently lose_sum: 94.6614596247673
time_elpased: 2.043
batch start
#iterations: 206
currently lose_sum: 94.84372395277023
time_elpased: 2.014
batch start
#iterations: 207
currently lose_sum: 95.03537398576736
time_elpased: 2.008
batch start
#iterations: 208
currently lose_sum: 95.00837695598602
time_elpased: 1.979
batch start
#iterations: 209
currently lose_sum: 94.90379858016968
time_elpased: 1.951
batch start
#iterations: 210
currently lose_sum: 94.57870346307755
time_elpased: 1.977
batch start
#iterations: 211
currently lose_sum: 95.0538409948349
time_elpased: 1.976
batch start
#iterations: 212
currently lose_sum: 94.88961237668991
time_elpased: 2.002
batch start
#iterations: 213
currently lose_sum: 94.87781703472137
time_elpased: 2.014
batch start
#iterations: 214
currently lose_sum: 94.65653151273727
time_elpased: 2.028
batch start
#iterations: 215
currently lose_sum: 94.71665006875992
time_elpased: 1.976
batch start
#iterations: 216
currently lose_sum: 94.70273643732071
time_elpased: 1.974
batch start
#iterations: 217
currently lose_sum: 94.97273886203766
time_elpased: 2.047
batch start
#iterations: 218
currently lose_sum: 94.51796025037766
time_elpased: 2.03
batch start
#iterations: 219
currently lose_sum: 95.33765703439713
time_elpased: 2.019
start validation test
0.666340206186
0.647938708501
0.731089842544
0.687007398095
0.666226528212
60.647
batch start
#iterations: 220
currently lose_sum: 94.79506969451904
time_elpased: 2.049
batch start
#iterations: 221
currently lose_sum: 94.9628871679306
time_elpased: 2.042
batch start
#iterations: 222
currently lose_sum: 94.82430183887482
time_elpased: 1.938
batch start
#iterations: 223
currently lose_sum: 94.49456489086151
time_elpased: 2.063
batch start
#iterations: 224
currently lose_sum: 94.24944120645523
time_elpased: 2.011
batch start
#iterations: 225
currently lose_sum: 94.91074597835541
time_elpased: 1.993
batch start
#iterations: 226
currently lose_sum: 94.82176625728607
time_elpased: 2.003
batch start
#iterations: 227
currently lose_sum: 94.97392570972443
time_elpased: 2.064
batch start
#iterations: 228
currently lose_sum: 94.85074752569199
time_elpased: 1.99
batch start
#iterations: 229
currently lose_sum: 94.94752633571625
time_elpased: 1.971
batch start
#iterations: 230
currently lose_sum: 94.71520131826401
time_elpased: 1.989
batch start
#iterations: 231
currently lose_sum: 94.60388022661209
time_elpased: 2.054
batch start
#iterations: 232
currently lose_sum: 94.624646961689
time_elpased: 2.034
batch start
#iterations: 233
currently lose_sum: 95.06270724534988
time_elpased: 2.061
batch start
#iterations: 234
currently lose_sum: 94.87549006938934
time_elpased: 2.004
batch start
#iterations: 235
currently lose_sum: 94.63474994897842
time_elpased: 1.973
batch start
#iterations: 236
currently lose_sum: 94.98053616285324
time_elpased: 1.957
batch start
#iterations: 237
currently lose_sum: 94.9339115023613
time_elpased: 1.983
batch start
#iterations: 238
currently lose_sum: 94.90441250801086
time_elpased: 1.883
batch start
#iterations: 239
currently lose_sum: 95.07599794864655
time_elpased: 1.986
start validation test
0.665103092784
0.638126286891
0.765462591335
0.696018340897
0.664926896204
60.558
batch start
#iterations: 240
currently lose_sum: 94.51857477426529
time_elpased: 1.892
batch start
#iterations: 241
currently lose_sum: 95.11222922801971
time_elpased: 1.98
batch start
#iterations: 242
currently lose_sum: 94.56253337860107
time_elpased: 1.976
batch start
#iterations: 243
currently lose_sum: 94.75806301832199
time_elpased: 1.972
batch start
#iterations: 244
currently lose_sum: 94.91122132539749
time_elpased: 1.908
batch start
#iterations: 245
currently lose_sum: 94.68189609050751
time_elpased: 1.959
batch start
#iterations: 246
currently lose_sum: 95.0748239159584
time_elpased: 1.9
batch start
#iterations: 247
currently lose_sum: 94.27895623445511
time_elpased: 1.866
batch start
#iterations: 248
currently lose_sum: 94.73484361171722
time_elpased: 1.882
batch start
#iterations: 249
currently lose_sum: 94.57212710380554
time_elpased: 1.894
batch start
#iterations: 250
currently lose_sum: 94.37120741605759
time_elpased: 1.877
batch start
#iterations: 251
currently lose_sum: 94.59294092655182
time_elpased: 1.96
batch start
#iterations: 252
currently lose_sum: 94.9876457452774
time_elpased: 1.945
batch start
#iterations: 253
currently lose_sum: 94.50311172008514
time_elpased: 1.998
batch start
#iterations: 254
currently lose_sum: 94.55980342626572
time_elpased: 1.966
batch start
#iterations: 255
currently lose_sum: 94.84915482997894
time_elpased: 2.079
batch start
#iterations: 256
currently lose_sum: 94.48417562246323
time_elpased: 1.997
batch start
#iterations: 257
currently lose_sum: 94.66296660900116
time_elpased: 2.001
batch start
#iterations: 258
currently lose_sum: 94.75439256429672
time_elpased: 2.074
batch start
#iterations: 259
currently lose_sum: 94.54065018892288
time_elpased: 1.979
start validation test
0.669587628866
0.644726477024
0.758052896985
0.69681203292
0.669432314443
60.384
batch start
#iterations: 260
currently lose_sum: 94.52302849292755
time_elpased: 1.955
batch start
#iterations: 261
currently lose_sum: 93.94552636146545
time_elpased: 1.938
batch start
#iterations: 262
currently lose_sum: 94.94083744287491
time_elpased: 1.983
batch start
#iterations: 263
currently lose_sum: 94.92166543006897
time_elpased: 1.985
batch start
#iterations: 264
currently lose_sum: 94.69909399747849
time_elpased: 2.003
batch start
#iterations: 265
currently lose_sum: 94.58366537094116
time_elpased: 1.981
batch start
#iterations: 266
currently lose_sum: 94.66967815160751
time_elpased: 1.991
batch start
#iterations: 267
currently lose_sum: 94.60868293046951
time_elpased: 1.999
batch start
#iterations: 268
currently lose_sum: 94.72264969348907
time_elpased: 1.962
batch start
#iterations: 269
currently lose_sum: 94.48004788160324
time_elpased: 2.012
batch start
#iterations: 270
currently lose_sum: 94.66446942090988
time_elpased: 1.953
batch start
#iterations: 271
currently lose_sum: 94.75788474082947
time_elpased: 2.021
batch start
#iterations: 272
currently lose_sum: 94.32685542106628
time_elpased: 2.023
batch start
#iterations: 273
currently lose_sum: 94.59998524188995
time_elpased: 2.034
batch start
#iterations: 274
currently lose_sum: 94.65823096036911
time_elpased: 2.046
batch start
#iterations: 275
currently lose_sum: 94.4634747505188
time_elpased: 2.003
batch start
#iterations: 276
currently lose_sum: 94.85490316152573
time_elpased: 2.053
batch start
#iterations: 277
currently lose_sum: 94.25871247053146
time_elpased: 2.047
batch start
#iterations: 278
currently lose_sum: 94.37455999851227
time_elpased: 2.047
batch start
#iterations: 279
currently lose_sum: 94.75374329090118
time_elpased: 2.052
start validation test
0.66824742268
0.629878869448
0.818771225687
0.712010023268
0.667983154928
60.393
batch start
#iterations: 280
currently lose_sum: 94.66718631982803
time_elpased: 2.071
batch start
#iterations: 281
currently lose_sum: 94.45588564872742
time_elpased: 2.052
batch start
#iterations: 282
currently lose_sum: 94.69374883174896
time_elpased: 2.057
batch start
#iterations: 283
currently lose_sum: 94.79994434118271
time_elpased: 1.967
batch start
#iterations: 284
currently lose_sum: 94.50102323293686
time_elpased: 1.967
batch start
#iterations: 285
currently lose_sum: 94.42399853467941
time_elpased: 1.992
batch start
#iterations: 286
currently lose_sum: 94.43721824884415
time_elpased: 2.02
batch start
#iterations: 287
currently lose_sum: 94.41928124427795
time_elpased: 1.988
batch start
#iterations: 288
currently lose_sum: 94.61881005764008
time_elpased: 1.948
batch start
#iterations: 289
currently lose_sum: 94.11384069919586
time_elpased: 1.998
batch start
#iterations: 290
currently lose_sum: 94.55743074417114
time_elpased: 2.006
batch start
#iterations: 291
currently lose_sum: 94.85056930780411
time_elpased: 2.039
batch start
#iterations: 292
currently lose_sum: 94.81267648935318
time_elpased: 2.02
batch start
#iterations: 293
currently lose_sum: 94.3115718960762
time_elpased: 2.03
batch start
#iterations: 294
currently lose_sum: 94.6622365117073
time_elpased: 2.01
batch start
#iterations: 295
currently lose_sum: 94.91695725917816
time_elpased: 2.005
batch start
#iterations: 296
currently lose_sum: 94.63234144449234
time_elpased: 1.987
batch start
#iterations: 297
currently lose_sum: 94.7507244348526
time_elpased: 1.985
batch start
#iterations: 298
currently lose_sum: 94.7518116235733
time_elpased: 1.893
batch start
#iterations: 299
currently lose_sum: 94.5572674870491
time_elpased: 1.931
start validation test
0.667216494845
0.645282010158
0.745291756715
0.691690544413
0.667079421681
60.537
batch start
#iterations: 300
currently lose_sum: 94.37085777521133
time_elpased: 1.946
batch start
#iterations: 301
currently lose_sum: 95.00206279754639
time_elpased: 2.014
batch start
#iterations: 302
currently lose_sum: 94.28966873884201
time_elpased: 1.861
batch start
#iterations: 303
currently lose_sum: 94.56987792253494
time_elpased: 1.941
batch start
#iterations: 304
currently lose_sum: 94.49271339178085
time_elpased: 1.896
batch start
#iterations: 305
currently lose_sum: 94.2330214381218
time_elpased: 1.865
batch start
#iterations: 306
currently lose_sum: 94.45554381608963
time_elpased: 1.878
batch start
#iterations: 307
currently lose_sum: 94.6834324002266
time_elpased: 2.017
batch start
#iterations: 308
currently lose_sum: 94.1010085940361
time_elpased: 2.045
batch start
#iterations: 309
currently lose_sum: 94.43903493881226
time_elpased: 1.961
batch start
#iterations: 310
currently lose_sum: 94.90921133756638
time_elpased: 1.979
batch start
#iterations: 311
currently lose_sum: 94.41996264457703
time_elpased: 2.037
batch start
#iterations: 312
currently lose_sum: 94.32252794504166
time_elpased: 2.062
batch start
#iterations: 313
currently lose_sum: 94.6555324792862
time_elpased: 2.063
batch start
#iterations: 314
currently lose_sum: 94.48979091644287
time_elpased: 2.049
batch start
#iterations: 315
currently lose_sum: 94.23219555616379
time_elpased: 2.001
batch start
#iterations: 316
currently lose_sum: 94.45443594455719
time_elpased: 2.077
batch start
#iterations: 317
currently lose_sum: 94.24001860618591
time_elpased: 2.079
batch start
#iterations: 318
currently lose_sum: 94.59874880313873
time_elpased: 2.088
batch start
#iterations: 319
currently lose_sum: 94.94042909145355
time_elpased: 2.092
start validation test
0.667525773196
0.646253021757
0.742821858598
0.691180695203
0.667393579304
60.796
batch start
#iterations: 320
currently lose_sum: 94.21388983726501
time_elpased: 2.11
batch start
#iterations: 321
currently lose_sum: 94.49768000841141
time_elpased: 2.08
batch start
#iterations: 322
currently lose_sum: 94.82393318414688
time_elpased: 1.961
batch start
#iterations: 323
currently lose_sum: 94.6798021197319
time_elpased: 2.034
batch start
#iterations: 324
currently lose_sum: 94.36186462640762
time_elpased: 1.985
batch start
#iterations: 325
currently lose_sum: 94.69859343767166
time_elpased: 1.978
batch start
#iterations: 326
currently lose_sum: 94.26970446109772
time_elpased: 2.061
batch start
#iterations: 327
currently lose_sum: 94.61001074314117
time_elpased: 2.048
batch start
#iterations: 328
currently lose_sum: 94.63528567552567
time_elpased: 2.053
batch start
#iterations: 329
currently lose_sum: 94.16857373714447
time_elpased: 2.019
batch start
#iterations: 330
currently lose_sum: 94.78358352184296
time_elpased: 2.003
batch start
#iterations: 331
currently lose_sum: 94.33822005987167
time_elpased: 2.097
batch start
#iterations: 332
currently lose_sum: 94.05851012468338
time_elpased: 2.083
batch start
#iterations: 333
currently lose_sum: 94.37932336330414
time_elpased: 2.044
batch start
#iterations: 334
currently lose_sum: 93.99427539110184
time_elpased: 2.083
batch start
#iterations: 335
currently lose_sum: 94.47678446769714
time_elpased: 2.039
batch start
#iterations: 336
currently lose_sum: 94.18810844421387
time_elpased: 2.089
batch start
#iterations: 337
currently lose_sum: 94.6215273141861
time_elpased: 2.054
batch start
#iterations: 338
currently lose_sum: 94.37030845880508
time_elpased: 2.035
batch start
#iterations: 339
currently lose_sum: 94.66984641551971
time_elpased: 2.127
start validation test
0.657113402062
0.642757335817
0.710095708552
0.674750635635
0.657020383451
60.867
batch start
#iterations: 340
currently lose_sum: 94.78711366653442
time_elpased: 2.077
batch start
#iterations: 341
currently lose_sum: 94.67052328586578
time_elpased: 2.038
batch start
#iterations: 342
currently lose_sum: 94.75362819433212
time_elpased: 2.048
batch start
#iterations: 343
currently lose_sum: 94.40446346998215
time_elpased: 2.017
batch start
#iterations: 344
currently lose_sum: 94.13242709636688
time_elpased: 2.069
batch start
#iterations: 345
currently lose_sum: 94.69228267669678
time_elpased: 1.979
batch start
#iterations: 346
currently lose_sum: 94.2669245004654
time_elpased: 1.989
batch start
#iterations: 347
currently lose_sum: 94.53413712978363
time_elpased: 2.079
batch start
#iterations: 348
currently lose_sum: 94.62051498889923
time_elpased: 2.082
batch start
#iterations: 349
currently lose_sum: 95.05010831356049
time_elpased: 2.068
batch start
#iterations: 350
currently lose_sum: 94.34820181131363
time_elpased: 2.084
batch start
#iterations: 351
currently lose_sum: 94.46416163444519
time_elpased: 2.107
batch start
#iterations: 352
currently lose_sum: 94.50504177808762
time_elpased: 2.111
batch start
#iterations: 353
currently lose_sum: 94.09914374351501
time_elpased: 2.002
batch start
#iterations: 354
currently lose_sum: 94.61876481771469
time_elpased: 2.093
batch start
#iterations: 355
currently lose_sum: 94.29787087440491
time_elpased: 1.977
batch start
#iterations: 356
currently lose_sum: 94.85040539503098
time_elpased: 1.929
batch start
#iterations: 357
currently lose_sum: 94.42288690805435
time_elpased: 1.947
batch start
#iterations: 358
currently lose_sum: 94.6063888669014
time_elpased: 1.952
batch start
#iterations: 359
currently lose_sum: 94.30842679738998
time_elpased: 1.959
start validation test
0.666958762887
0.645773638968
0.742204384069
0.690639214747
0.666826657593
60.495
batch start
#iterations: 360
currently lose_sum: 94.81100642681122
time_elpased: 1.931
batch start
#iterations: 361
currently lose_sum: 94.7145225405693
time_elpased: 1.959
batch start
#iterations: 362
currently lose_sum: 94.36923038959503
time_elpased: 1.883
batch start
#iterations: 363
currently lose_sum: 94.19776910543442
time_elpased: 1.89
batch start
#iterations: 364
currently lose_sum: 94.231789290905
time_elpased: 1.879
batch start
#iterations: 365
currently lose_sum: 94.99874520301819
time_elpased: 2.05
batch start
#iterations: 366
currently lose_sum: 94.52398407459259
time_elpased: 1.997
batch start
#iterations: 367
currently lose_sum: 94.47876173257828
time_elpased: 1.943
batch start
#iterations: 368
currently lose_sum: 94.48745787143707
time_elpased: 1.973
batch start
#iterations: 369
currently lose_sum: 94.72225505113602
time_elpased: 1.927
batch start
#iterations: 370
currently lose_sum: 94.49381238222122
time_elpased: 1.944
batch start
#iterations: 371
currently lose_sum: 94.43907248973846
time_elpased: 1.944
batch start
#iterations: 372
currently lose_sum: 93.7720273733139
time_elpased: 1.897
batch start
#iterations: 373
currently lose_sum: 94.73307919502258
time_elpased: 1.978
batch start
#iterations: 374
currently lose_sum: 94.8959721326828
time_elpased: 1.95
batch start
#iterations: 375
currently lose_sum: 94.03813070058823
time_elpased: 1.954
batch start
#iterations: 376
currently lose_sum: 94.7101338505745
time_elpased: 1.978
batch start
#iterations: 377
currently lose_sum: 94.66796135902405
time_elpased: 1.931
batch start
#iterations: 378
currently lose_sum: 94.55699634552002
time_elpased: 1.927
batch start
#iterations: 379
currently lose_sum: 94.47968828678131
time_elpased: 1.907
start validation test
0.661907216495
0.655199528209
0.686014201914
0.670252878186
0.661864892964
60.965
batch start
#iterations: 380
currently lose_sum: 94.73839068412781
time_elpased: 1.931
batch start
#iterations: 381
currently lose_sum: 94.16145676374435
time_elpased: 1.886
batch start
#iterations: 382
currently lose_sum: 94.67176133394241
time_elpased: 1.935
batch start
#iterations: 383
currently lose_sum: 94.29184192419052
time_elpased: 1.936
batch start
#iterations: 384
currently lose_sum: 94.78089642524719
time_elpased: 1.914
batch start
#iterations: 385
currently lose_sum: 94.6415023803711
time_elpased: 1.926
batch start
#iterations: 386
currently lose_sum: 94.71252942085266
time_elpased: 1.92
batch start
#iterations: 387
currently lose_sum: 94.88324725627899
time_elpased: 1.943
batch start
#iterations: 388
currently lose_sum: 94.62174344062805
time_elpased: 1.898
batch start
#iterations: 389
currently lose_sum: 94.38776206970215
time_elpased: 1.941
batch start
#iterations: 390
currently lose_sum: 94.49144995212555
time_elpased: 1.952
batch start
#iterations: 391
currently lose_sum: 94.58722364902496
time_elpased: 1.926
batch start
#iterations: 392
currently lose_sum: 94.41414350271225
time_elpased: 1.928
batch start
#iterations: 393
currently lose_sum: 94.42920881509781
time_elpased: 1.943
batch start
#iterations: 394
currently lose_sum: 94.34588623046875
time_elpased: 1.963
batch start
#iterations: 395
currently lose_sum: 94.23457419872284
time_elpased: 1.908
batch start
#iterations: 396
currently lose_sum: 94.04029405117035
time_elpased: 1.958
batch start
#iterations: 397
currently lose_sum: 94.41900885105133
time_elpased: 1.914
batch start
#iterations: 398
currently lose_sum: 94.31591629981995
time_elpased: 1.951
batch start
#iterations: 399
currently lose_sum: 94.55737733840942
time_elpased: 1.951
start validation test
0.665154639175
0.647901552025
0.726047133889
0.684752013977
0.665047733009
60.582
acc: 0.666
pre: 0.643
rec: 0.753
F1: 0.693
auc: 0.666
