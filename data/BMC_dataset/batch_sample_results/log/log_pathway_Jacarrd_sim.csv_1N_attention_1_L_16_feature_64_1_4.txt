start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.7401162981987
time_elpased: 2.001
batch start
#iterations: 1
currently lose_sum: 100.32437270879745
time_elpased: 1.9
batch start
#iterations: 2
currently lose_sum: 100.17213106155396
time_elpased: 1.896
batch start
#iterations: 3
currently lose_sum: 99.84375661611557
time_elpased: 1.896
batch start
#iterations: 4
currently lose_sum: 99.99533116817474
time_elpased: 1.903
batch start
#iterations: 5
currently lose_sum: 99.6308805346489
time_elpased: 1.9
batch start
#iterations: 6
currently lose_sum: 99.48242396116257
time_elpased: 1.914
batch start
#iterations: 7
currently lose_sum: 99.44950813055038
time_elpased: 1.892
batch start
#iterations: 8
currently lose_sum: 99.32475656270981
time_elpased: 1.906
batch start
#iterations: 9
currently lose_sum: 98.99182462692261
time_elpased: 1.897
batch start
#iterations: 10
currently lose_sum: 98.90965801477432
time_elpased: 1.96
batch start
#iterations: 11
currently lose_sum: 98.77440315485
time_elpased: 1.907
batch start
#iterations: 12
currently lose_sum: 98.64844220876694
time_elpased: 1.962
batch start
#iterations: 13
currently lose_sum: 98.81793826818466
time_elpased: 1.969
batch start
#iterations: 14
currently lose_sum: 98.48738586902618
time_elpased: 1.893
batch start
#iterations: 15
currently lose_sum: 98.391881108284
time_elpased: 1.916
batch start
#iterations: 16
currently lose_sum: 98.29088014364243
time_elpased: 1.885
batch start
#iterations: 17
currently lose_sum: 97.78122788667679
time_elpased: 2.011
batch start
#iterations: 18
currently lose_sum: 98.06662648916245
time_elpased: 1.893
batch start
#iterations: 19
currently lose_sum: 97.8728579878807
time_elpased: 1.949
start validation test
0.625824742268
0.622045680238
0.644643408459
0.633142972659
0.625791703197
63.604
batch start
#iterations: 20
currently lose_sum: 98.15313637256622
time_elpased: 2.011
batch start
#iterations: 21
currently lose_sum: 97.73409730195999
time_elpased: 1.938
batch start
#iterations: 22
currently lose_sum: 97.89005821943283
time_elpased: 1.971
batch start
#iterations: 23
currently lose_sum: 97.78971672058105
time_elpased: 1.89
batch start
#iterations: 24
currently lose_sum: 97.6330658197403
time_elpased: 1.91
batch start
#iterations: 25
currently lose_sum: 97.4388056397438
time_elpased: 1.907
batch start
#iterations: 26
currently lose_sum: 97.73574286699295
time_elpased: 1.92
batch start
#iterations: 27
currently lose_sum: 97.4050520658493
time_elpased: 1.891
batch start
#iterations: 28
currently lose_sum: 97.71232175827026
time_elpased: 1.933
batch start
#iterations: 29
currently lose_sum: 96.90372115373611
time_elpased: 1.92
batch start
#iterations: 30
currently lose_sum: 97.02760994434357
time_elpased: 1.895
batch start
#iterations: 31
currently lose_sum: 97.19024896621704
time_elpased: 1.897
batch start
#iterations: 32
currently lose_sum: 97.41701048612595
time_elpased: 1.921
batch start
#iterations: 33
currently lose_sum: 97.35188311338425
time_elpased: 1.919
batch start
#iterations: 34
currently lose_sum: 96.97795134782791
time_elpased: 1.941
batch start
#iterations: 35
currently lose_sum: 97.09568959474564
time_elpased: 1.924
batch start
#iterations: 36
currently lose_sum: 97.27256083488464
time_elpased: 1.99
batch start
#iterations: 37
currently lose_sum: 96.9935861825943
time_elpased: 1.925
batch start
#iterations: 38
currently lose_sum: 96.88968098163605
time_elpased: 1.913
batch start
#iterations: 39
currently lose_sum: 97.31451201438904
time_elpased: 1.895
start validation test
0.640463917526
0.61654199252
0.746423793352
0.675294446255
0.640277888621
62.613
batch start
#iterations: 40
currently lose_sum: 96.98813319206238
time_elpased: 1.942
batch start
#iterations: 41
currently lose_sum: 96.81790095567703
time_elpased: 1.964
batch start
#iterations: 42
currently lose_sum: 97.18795567750931
time_elpased: 1.942
batch start
#iterations: 43
currently lose_sum: 97.1020770072937
time_elpased: 1.92
batch start
#iterations: 44
currently lose_sum: 97.17276573181152
time_elpased: 1.911
batch start
#iterations: 45
currently lose_sum: 97.16382873058319
time_elpased: 1.92
batch start
#iterations: 46
currently lose_sum: 96.8447670340538
time_elpased: 2.005
batch start
#iterations: 47
currently lose_sum: 97.20380860567093
time_elpased: 1.928
batch start
#iterations: 48
currently lose_sum: 97.00554883480072
time_elpased: 1.911
batch start
#iterations: 49
currently lose_sum: 96.88945043087006
time_elpased: 1.94
batch start
#iterations: 50
currently lose_sum: 96.84792482852936
time_elpased: 1.909
batch start
#iterations: 51
currently lose_sum: 96.67467838525772
time_elpased: 1.918
batch start
#iterations: 52
currently lose_sum: 96.67635560035706
time_elpased: 1.893
batch start
#iterations: 53
currently lose_sum: 96.5842592716217
time_elpased: 1.943
batch start
#iterations: 54
currently lose_sum: 96.85717403888702
time_elpased: 2.004
batch start
#iterations: 55
currently lose_sum: 96.38878434896469
time_elpased: 1.921
batch start
#iterations: 56
currently lose_sum: 96.90078383684158
time_elpased: 1.886
batch start
#iterations: 57
currently lose_sum: 96.75917220115662
time_elpased: 1.91
batch start
#iterations: 58
currently lose_sum: 96.69290292263031
time_elpased: 1.961
batch start
#iterations: 59
currently lose_sum: 97.21813321113586
time_elpased: 1.926
start validation test
0.638092783505
0.61768814388
0.72810538232
0.668367105947
0.637934752504
62.589
batch start
#iterations: 60
currently lose_sum: 96.49687850475311
time_elpased: 1.904
batch start
#iterations: 61
currently lose_sum: 97.15455949306488
time_elpased: 1.923
batch start
#iterations: 62
currently lose_sum: 96.66993290185928
time_elpased: 1.91
batch start
#iterations: 63
currently lose_sum: 96.72690516710281
time_elpased: 1.916
batch start
#iterations: 64
currently lose_sum: 97.02419084310532
time_elpased: 1.899
batch start
#iterations: 65
currently lose_sum: 96.73728728294373
time_elpased: 1.923
batch start
#iterations: 66
currently lose_sum: 96.59182840585709
time_elpased: 1.924
batch start
#iterations: 67
currently lose_sum: 96.46887594461441
time_elpased: 1.928
batch start
#iterations: 68
currently lose_sum: 96.45417749881744
time_elpased: 1.926
batch start
#iterations: 69
currently lose_sum: 96.64126706123352
time_elpased: 1.924
batch start
#iterations: 70
currently lose_sum: 96.69333052635193
time_elpased: 1.885
batch start
#iterations: 71
currently lose_sum: 96.72103923559189
time_elpased: 1.943
batch start
#iterations: 72
currently lose_sum: 96.29231882095337
time_elpased: 1.91
batch start
#iterations: 73
currently lose_sum: 96.53242009878159
time_elpased: 1.896
batch start
#iterations: 74
currently lose_sum: 96.49058067798615
time_elpased: 1.876
batch start
#iterations: 75
currently lose_sum: 96.49685281515121
time_elpased: 1.902
batch start
#iterations: 76
currently lose_sum: 96.51295167207718
time_elpased: 1.889
batch start
#iterations: 77
currently lose_sum: 96.69966262578964
time_elpased: 1.939
batch start
#iterations: 78
currently lose_sum: 96.62618565559387
time_elpased: 1.956
batch start
#iterations: 79
currently lose_sum: 96.30476289987564
time_elpased: 1.91
start validation test
0.644536082474
0.613722486495
0.783369352681
0.688245931284
0.644292339255
62.152
batch start
#iterations: 80
currently lose_sum: 96.50941997766495
time_elpased: 1.974
batch start
#iterations: 81
currently lose_sum: 96.41336786746979
time_elpased: 1.896
batch start
#iterations: 82
currently lose_sum: 96.50670832395554
time_elpased: 1.921
batch start
#iterations: 83
currently lose_sum: 96.40734159946442
time_elpased: 1.893
batch start
#iterations: 84
currently lose_sum: 96.90462321043015
time_elpased: 1.913
batch start
#iterations: 85
currently lose_sum: 96.6257866024971
time_elpased: 1.914
batch start
#iterations: 86
currently lose_sum: 96.3075338602066
time_elpased: 1.928
batch start
#iterations: 87
currently lose_sum: 96.44290643930435
time_elpased: 1.902
batch start
#iterations: 88
currently lose_sum: 96.59727430343628
time_elpased: 1.893
batch start
#iterations: 89
currently lose_sum: 96.76619052886963
time_elpased: 1.936
batch start
#iterations: 90
currently lose_sum: 96.30265235900879
time_elpased: 1.925
batch start
#iterations: 91
currently lose_sum: 96.41090762615204
time_elpased: 1.937
batch start
#iterations: 92
currently lose_sum: 96.15182465314865
time_elpased: 1.887
batch start
#iterations: 93
currently lose_sum: 96.40187001228333
time_elpased: 1.943
batch start
#iterations: 94
currently lose_sum: 96.61483764648438
time_elpased: 1.897
batch start
#iterations: 95
currently lose_sum: 96.41890662908554
time_elpased: 1.924
batch start
#iterations: 96
currently lose_sum: 96.53220707178116
time_elpased: 1.894
batch start
#iterations: 97
currently lose_sum: 96.3399566411972
time_elpased: 1.93
batch start
#iterations: 98
currently lose_sum: 96.44777953624725
time_elpased: 1.917
batch start
#iterations: 99
currently lose_sum: 96.66527056694031
time_elpased: 1.919
start validation test
0.626855670103
0.612370760022
0.694864670166
0.651014800174
0.626736269814
62.567
batch start
#iterations: 100
currently lose_sum: 96.58427572250366
time_elpased: 2.002
batch start
#iterations: 101
currently lose_sum: 96.37448364496231
time_elpased: 1.89
batch start
#iterations: 102
currently lose_sum: 96.25372689962387
time_elpased: 1.898
batch start
#iterations: 103
currently lose_sum: 96.38309574127197
time_elpased: 1.926
batch start
#iterations: 104
currently lose_sum: 95.97518473863602
time_elpased: 1.883
batch start
#iterations: 105
currently lose_sum: 96.31838244199753
time_elpased: 1.915
batch start
#iterations: 106
currently lose_sum: 96.5447313785553
time_elpased: 1.886
batch start
#iterations: 107
currently lose_sum: 96.28494608402252
time_elpased: 1.907
batch start
#iterations: 108
currently lose_sum: 96.20990985631943
time_elpased: 1.894
batch start
#iterations: 109
currently lose_sum: 96.27277535200119
time_elpased: 1.927
batch start
#iterations: 110
currently lose_sum: 96.4616824388504
time_elpased: 1.904
batch start
#iterations: 111
currently lose_sum: 96.58744502067566
time_elpased: 1.92
batch start
#iterations: 112
currently lose_sum: 96.33341085910797
time_elpased: 1.906
batch start
#iterations: 113
currently lose_sum: 96.02746349573135
time_elpased: 1.895
batch start
#iterations: 114
currently lose_sum: 96.43088406324387
time_elpased: 1.933
batch start
#iterations: 115
currently lose_sum: 96.22828477621078
time_elpased: 1.972
batch start
#iterations: 116
currently lose_sum: 96.1353366971016
time_elpased: 1.926
batch start
#iterations: 117
currently lose_sum: 96.03928977251053
time_elpased: 1.911
batch start
#iterations: 118
currently lose_sum: 96.35838931798935
time_elpased: 1.915
batch start
#iterations: 119
currently lose_sum: 96.01721954345703
time_elpased: 1.935
start validation test
0.633969072165
0.620242691671
0.694350108058
0.655207574654
0.633863063943
62.180
batch start
#iterations: 120
currently lose_sum: 96.31290477514267
time_elpased: 1.926
batch start
#iterations: 121
currently lose_sum: 96.25304442644119
time_elpased: 1.93
batch start
#iterations: 122
currently lose_sum: 96.28222984075546
time_elpased: 1.959
batch start
#iterations: 123
currently lose_sum: 95.937524497509
time_elpased: 1.903
batch start
#iterations: 124
currently lose_sum: 96.23354780673981
time_elpased: 1.892
batch start
#iterations: 125
currently lose_sum: 96.30123472213745
time_elpased: 1.927
batch start
#iterations: 126
currently lose_sum: 96.28193247318268
time_elpased: 1.91
batch start
#iterations: 127
currently lose_sum: 96.23835170269012
time_elpased: 1.913
batch start
#iterations: 128
currently lose_sum: 96.14353823661804
time_elpased: 1.884
batch start
#iterations: 129
currently lose_sum: 96.09163892269135
time_elpased: 1.896
batch start
#iterations: 130
currently lose_sum: 96.16274493932724
time_elpased: 1.911
batch start
#iterations: 131
currently lose_sum: 96.39985048770905
time_elpased: 1.944
batch start
#iterations: 132
currently lose_sum: 96.2176364660263
time_elpased: 1.947
batch start
#iterations: 133
currently lose_sum: 95.62815773487091
time_elpased: 1.923
batch start
#iterations: 134
currently lose_sum: 96.11092311143875
time_elpased: 1.908
batch start
#iterations: 135
currently lose_sum: 96.28502595424652
time_elpased: 1.892
batch start
#iterations: 136
currently lose_sum: 96.21898716688156
time_elpased: 1.911
batch start
#iterations: 137
currently lose_sum: 96.36071592569351
time_elpased: 1.896
batch start
#iterations: 138
currently lose_sum: 96.46582144498825
time_elpased: 1.906
batch start
#iterations: 139
currently lose_sum: 96.1562112569809
time_elpased: 1.897
start validation test
0.621804123711
0.618219749652
0.640423999177
0.629126017288
0.621771433648
62.613
batch start
#iterations: 140
currently lose_sum: 95.80758595466614
time_elpased: 1.913
batch start
#iterations: 141
currently lose_sum: 96.61389321088791
time_elpased: 1.9
batch start
#iterations: 142
currently lose_sum: 96.05477023124695
time_elpased: 1.927
batch start
#iterations: 143
currently lose_sum: 96.5058713555336
time_elpased: 1.907
batch start
#iterations: 144
currently lose_sum: 96.07582664489746
time_elpased: 1.97
batch start
#iterations: 145
currently lose_sum: 96.12902915477753
time_elpased: 1.99
batch start
#iterations: 146
currently lose_sum: 96.17937499284744
time_elpased: 1.971
batch start
#iterations: 147
currently lose_sum: 95.98905020952225
time_elpased: 1.984
batch start
#iterations: 148
currently lose_sum: 96.20235860347748
time_elpased: 1.944
batch start
#iterations: 149
currently lose_sum: 96.2343281507492
time_elpased: 1.989
batch start
#iterations: 150
currently lose_sum: 96.1652626991272
time_elpased: 1.937
batch start
#iterations: 151
currently lose_sum: 95.78296250104904
time_elpased: 1.929
batch start
#iterations: 152
currently lose_sum: 95.84827256202698
time_elpased: 1.922
batch start
#iterations: 153
currently lose_sum: 96.2001702785492
time_elpased: 1.956
batch start
#iterations: 154
currently lose_sum: 96.40761607885361
time_elpased: 1.98
batch start
#iterations: 155
currently lose_sum: 96.13630598783493
time_elpased: 1.933
batch start
#iterations: 156
currently lose_sum: 96.18546772003174
time_elpased: 1.953
batch start
#iterations: 157
currently lose_sum: 96.21934241056442
time_elpased: 2.034
batch start
#iterations: 158
currently lose_sum: 96.3646969795227
time_elpased: 1.925
batch start
#iterations: 159
currently lose_sum: 96.14133882522583
time_elpased: 1.914
start validation test
0.637371134021
0.616872930103
0.728414119584
0.668019442216
0.637211294017
62.181
batch start
#iterations: 160
currently lose_sum: 96.51454508304596
time_elpased: 1.98
batch start
#iterations: 161
currently lose_sum: 96.07219582796097
time_elpased: 1.949
batch start
#iterations: 162
currently lose_sum: 95.89621943235397
time_elpased: 1.947
batch start
#iterations: 163
currently lose_sum: 96.1980870962143
time_elpased: 1.968
batch start
#iterations: 164
currently lose_sum: 96.3435350060463
time_elpased: 1.973
batch start
#iterations: 165
currently lose_sum: 96.03157162666321
time_elpased: 1.945
batch start
#iterations: 166
currently lose_sum: 96.06287670135498
time_elpased: 2.029
batch start
#iterations: 167
currently lose_sum: 96.17650485038757
time_elpased: 1.929
batch start
#iterations: 168
currently lose_sum: 96.13937586545944
time_elpased: 2.014
batch start
#iterations: 169
currently lose_sum: 96.0702223777771
time_elpased: 1.919
batch start
#iterations: 170
currently lose_sum: 96.1973968744278
time_elpased: 1.999
batch start
#iterations: 171
currently lose_sum: 95.95797336101532
time_elpased: 1.937
batch start
#iterations: 172
currently lose_sum: 96.19952553510666
time_elpased: 1.935
batch start
#iterations: 173
currently lose_sum: 95.89009964466095
time_elpased: 2.177
batch start
#iterations: 174
currently lose_sum: 96.2774937748909
time_elpased: 1.972
batch start
#iterations: 175
currently lose_sum: 96.0408251285553
time_elpased: 1.901
batch start
#iterations: 176
currently lose_sum: 96.31357914209366
time_elpased: 1.952
batch start
#iterations: 177
currently lose_sum: 95.81136983633041
time_elpased: 2.094
batch start
#iterations: 178
currently lose_sum: 95.99786978960037
time_elpased: 1.947
batch start
#iterations: 179
currently lose_sum: 96.27994519472122
time_elpased: 1.98
start validation test
0.622268041237
0.626415493703
0.609138623032
0.617656266305
0.622291091956
62.589
batch start
#iterations: 180
currently lose_sum: 95.905393242836
time_elpased: 2.013
batch start
#iterations: 181
currently lose_sum: 96.02853435277939
time_elpased: 1.979
batch start
#iterations: 182
currently lose_sum: 96.13095259666443
time_elpased: 1.992
batch start
#iterations: 183
currently lose_sum: 95.83059638738632
time_elpased: 1.997
batch start
#iterations: 184
currently lose_sum: 95.7996888756752
time_elpased: 2.005
batch start
#iterations: 185
currently lose_sum: 95.91419124603271
time_elpased: 1.931
batch start
#iterations: 186
currently lose_sum: 95.92876386642456
time_elpased: 1.945
batch start
#iterations: 187
currently lose_sum: 96.05279189348221
time_elpased: 1.944
batch start
#iterations: 188
currently lose_sum: 96.25357395410538
time_elpased: 1.973
batch start
#iterations: 189
currently lose_sum: 95.97125345468521
time_elpased: 2.097
batch start
#iterations: 190
currently lose_sum: 96.08942919969559
time_elpased: 2.01
batch start
#iterations: 191
currently lose_sum: 95.98415893316269
time_elpased: 2.007
batch start
#iterations: 192
currently lose_sum: 95.87623006105423
time_elpased: 2.059
batch start
#iterations: 193
currently lose_sum: 96.16982448101044
time_elpased: 1.956
batch start
#iterations: 194
currently lose_sum: 96.08974212408066
time_elpased: 1.949
batch start
#iterations: 195
currently lose_sum: 95.92185151576996
time_elpased: 1.974
batch start
#iterations: 196
currently lose_sum: 96.0005452632904
time_elpased: 1.986
batch start
#iterations: 197
currently lose_sum: 96.00154858827591
time_elpased: 1.966
batch start
#iterations: 198
currently lose_sum: 95.54051315784454
time_elpased: 1.984
batch start
#iterations: 199
currently lose_sum: 96.07891035079956
time_elpased: 1.917
start validation test
0.638298969072
0.614581565099
0.745188844294
0.673612726173
0.63811130741
62.172
batch start
#iterations: 200
currently lose_sum: 96.29217338562012
time_elpased: 2.047
batch start
#iterations: 201
currently lose_sum: 95.91374206542969
time_elpased: 1.976
batch start
#iterations: 202
currently lose_sum: 95.86634880304337
time_elpased: 1.975
batch start
#iterations: 203
currently lose_sum: 96.01849937438965
time_elpased: 1.955
batch start
#iterations: 204
currently lose_sum: 96.01002132892609
time_elpased: 1.993
batch start
#iterations: 205
currently lose_sum: 96.0950465798378
time_elpased: 1.977
batch start
#iterations: 206
currently lose_sum: 95.78942757844925
time_elpased: 1.982
batch start
#iterations: 207
currently lose_sum: 95.75707745552063
time_elpased: 1.969
batch start
#iterations: 208
currently lose_sum: 95.62849801778793
time_elpased: 1.996
batch start
#iterations: 209
currently lose_sum: 95.6900617480278
time_elpased: 1.94
batch start
#iterations: 210
currently lose_sum: 95.90240758657455
time_elpased: 1.976
batch start
#iterations: 211
currently lose_sum: 96.01196873188019
time_elpased: 2.036
batch start
#iterations: 212
currently lose_sum: 95.63280254602432
time_elpased: 2.137
batch start
#iterations: 213
currently lose_sum: 95.70668691396713
time_elpased: 2.029
batch start
#iterations: 214
currently lose_sum: 95.97756457328796
time_elpased: 1.998
batch start
#iterations: 215
currently lose_sum: 95.81415444612503
time_elpased: 2.021
batch start
#iterations: 216
currently lose_sum: 95.72325706481934
time_elpased: 1.935
batch start
#iterations: 217
currently lose_sum: 96.14626944065094
time_elpased: 1.981
batch start
#iterations: 218
currently lose_sum: 95.59235310554504
time_elpased: 2.015
batch start
#iterations: 219
currently lose_sum: 95.69976603984833
time_elpased: 1.909
start validation test
0.638917525773
0.62363238512
0.70392096326
0.661348803481
0.638803402213
62.102
batch start
#iterations: 220
currently lose_sum: 95.92975014448166
time_elpased: 2.029
batch start
#iterations: 221
currently lose_sum: 95.84596866369247
time_elpased: 1.992
batch start
#iterations: 222
currently lose_sum: 95.8987466096878
time_elpased: 1.903
batch start
#iterations: 223
currently lose_sum: 95.86257666349411
time_elpased: 1.944
batch start
#iterations: 224
currently lose_sum: 95.97275358438492
time_elpased: 1.988
batch start
#iterations: 225
currently lose_sum: 95.97903764247894
time_elpased: 2.003
batch start
#iterations: 226
currently lose_sum: 95.72829157114029
time_elpased: 1.975
batch start
#iterations: 227
currently lose_sum: 95.83502542972565
time_elpased: 2.004
batch start
#iterations: 228
currently lose_sum: 95.5761046409607
time_elpased: 1.97
batch start
#iterations: 229
currently lose_sum: 96.13087975978851
time_elpased: 2.003
batch start
#iterations: 230
currently lose_sum: 95.8006951212883
time_elpased: 1.919
batch start
#iterations: 231
currently lose_sum: 95.98879110813141
time_elpased: 1.933
batch start
#iterations: 232
currently lose_sum: 95.956507563591
time_elpased: 1.918
batch start
#iterations: 233
currently lose_sum: 95.7919253706932
time_elpased: 1.994
batch start
#iterations: 234
currently lose_sum: 95.92392063140869
time_elpased: 1.98
batch start
#iterations: 235
currently lose_sum: 95.87920147180557
time_elpased: 1.937
batch start
#iterations: 236
currently lose_sum: 95.95868331193924
time_elpased: 1.95
batch start
#iterations: 237
currently lose_sum: 96.05035984516144
time_elpased: 1.983
batch start
#iterations: 238
currently lose_sum: 95.74971663951874
time_elpased: 1.955
batch start
#iterations: 239
currently lose_sum: 95.74478328227997
time_elpased: 1.93
start validation test
0.630206185567
0.615433499773
0.697643305547
0.653964885202
0.630087789301
62.290
batch start
#iterations: 240
currently lose_sum: 95.96653825044632
time_elpased: 1.919
batch start
#iterations: 241
currently lose_sum: 95.95495969057083
time_elpased: 1.916
batch start
#iterations: 242
currently lose_sum: 95.95538657903671
time_elpased: 1.937
batch start
#iterations: 243
currently lose_sum: 96.08500105142593
time_elpased: 1.899
batch start
#iterations: 244
currently lose_sum: 95.94701534509659
time_elpased: 2.006
batch start
#iterations: 245
currently lose_sum: 95.84646904468536
time_elpased: 1.953
batch start
#iterations: 246
currently lose_sum: 96.25270199775696
time_elpased: 1.974
batch start
#iterations: 247
currently lose_sum: 95.53791129589081
time_elpased: 1.964
batch start
#iterations: 248
currently lose_sum: 95.84974646568298
time_elpased: 2.013
batch start
#iterations: 249
currently lose_sum: 95.63337504863739
time_elpased: 1.952
batch start
#iterations: 250
currently lose_sum: 95.63010919094086
time_elpased: 1.997
batch start
#iterations: 251
currently lose_sum: 95.50520414113998
time_elpased: 1.927
batch start
#iterations: 252
currently lose_sum: 96.21055960655212
time_elpased: 1.928
batch start
#iterations: 253
currently lose_sum: 95.50193983316422
time_elpased: 1.963
batch start
#iterations: 254
currently lose_sum: 95.90596032142639
time_elpased: 2.004
batch start
#iterations: 255
currently lose_sum: 95.87737065553665
time_elpased: 1.894
batch start
#iterations: 256
currently lose_sum: 95.72531235218048
time_elpased: 1.908
batch start
#iterations: 257
currently lose_sum: 96.02418810129166
time_elpased: 1.954
batch start
#iterations: 258
currently lose_sum: 95.34944587945938
time_elpased: 1.936
batch start
#iterations: 259
currently lose_sum: 95.84422129392624
time_elpased: 1.983
start validation test
0.642268041237
0.613615907045
0.771740249048
0.683653933813
0.642040732807
62.129
batch start
#iterations: 260
currently lose_sum: 95.89976471662521
time_elpased: 2.017
batch start
#iterations: 261
currently lose_sum: 95.71403777599335
time_elpased: 1.937
batch start
#iterations: 262
currently lose_sum: 95.72876781225204
time_elpased: 1.954
batch start
#iterations: 263
currently lose_sum: 95.57238799333572
time_elpased: 1.991
batch start
#iterations: 264
currently lose_sum: 96.25455701351166
time_elpased: 1.928
batch start
#iterations: 265
currently lose_sum: 95.63321673870087
time_elpased: 1.971
batch start
#iterations: 266
currently lose_sum: 95.91298395395279
time_elpased: 2.006
batch start
#iterations: 267
currently lose_sum: 96.0987138748169
time_elpased: 1.955
batch start
#iterations: 268
currently lose_sum: 95.9786987900734
time_elpased: 1.918
batch start
#iterations: 269
currently lose_sum: 96.03783637285233
time_elpased: 1.93
batch start
#iterations: 270
currently lose_sum: 95.72596514225006
time_elpased: 1.93
batch start
#iterations: 271
currently lose_sum: 95.66817796230316
time_elpased: 1.927
batch start
#iterations: 272
currently lose_sum: 95.92282462120056
time_elpased: 1.925
batch start
#iterations: 273
currently lose_sum: 95.6308000087738
time_elpased: 1.917
batch start
#iterations: 274
currently lose_sum: 96.12345814704895
time_elpased: 1.913
batch start
#iterations: 275
currently lose_sum: 95.67242681980133
time_elpased: 1.952
batch start
#iterations: 276
currently lose_sum: 96.1160541176796
time_elpased: 1.916
batch start
#iterations: 277
currently lose_sum: 95.7086306810379
time_elpased: 1.953
batch start
#iterations: 278
currently lose_sum: 95.67513984441757
time_elpased: 1.964
batch start
#iterations: 279
currently lose_sum: 95.59167808294296
time_elpased: 1.925
start validation test
0.639536082474
0.61953659821
0.726458783575
0.668750888163
0.639383476266
62.167
batch start
#iterations: 280
currently lose_sum: 96.21198338270187
time_elpased: 1.94
batch start
#iterations: 281
currently lose_sum: 95.76456201076508
time_elpased: 1.925
batch start
#iterations: 282
currently lose_sum: 95.50190901756287
time_elpased: 1.986
batch start
#iterations: 283
currently lose_sum: 95.6734316945076
time_elpased: 2.031
batch start
#iterations: 284
currently lose_sum: 95.52374857664108
time_elpased: 1.991
batch start
#iterations: 285
currently lose_sum: 95.79054266214371
time_elpased: 1.936
batch start
#iterations: 286
currently lose_sum: 95.87802785634995
time_elpased: 1.935
batch start
#iterations: 287
currently lose_sum: 95.8773677945137
time_elpased: 2.045
batch start
#iterations: 288
currently lose_sum: 95.48402374982834
time_elpased: 1.948
batch start
#iterations: 289
currently lose_sum: 95.76023143529892
time_elpased: 1.986
batch start
#iterations: 290
currently lose_sum: 95.7826287150383
time_elpased: 1.913
batch start
#iterations: 291
currently lose_sum: 95.5384441614151
time_elpased: 1.932
batch start
#iterations: 292
currently lose_sum: 95.91554296016693
time_elpased: 1.948
batch start
#iterations: 293
currently lose_sum: 95.47308230400085
time_elpased: 1.988
batch start
#iterations: 294
currently lose_sum: 95.42129862308502
time_elpased: 1.983
batch start
#iterations: 295
currently lose_sum: 95.78845083713531
time_elpased: 2.012
batch start
#iterations: 296
currently lose_sum: 95.83694589138031
time_elpased: 1.935
batch start
#iterations: 297
currently lose_sum: 96.03487122058868
time_elpased: 1.964
batch start
#iterations: 298
currently lose_sum: 95.43424814939499
time_elpased: 2.029
batch start
#iterations: 299
currently lose_sum: 95.54003965854645
time_elpased: 1.968
start validation test
0.642886597938
0.615086242469
0.767006277658
0.682696711551
0.642668686696
61.884
batch start
#iterations: 300
currently lose_sum: 95.39308631420135
time_elpased: 2.055
batch start
#iterations: 301
currently lose_sum: 95.86952930688858
time_elpased: 1.892
batch start
#iterations: 302
currently lose_sum: 95.97517919540405
time_elpased: 1.946
batch start
#iterations: 303
currently lose_sum: 95.57513242959976
time_elpased: 1.993
batch start
#iterations: 304
currently lose_sum: 95.60712838172913
time_elpased: 1.924
batch start
#iterations: 305
currently lose_sum: 95.70864748954773
time_elpased: 2.003
batch start
#iterations: 306
currently lose_sum: 95.52738982439041
time_elpased: 1.973
batch start
#iterations: 307
currently lose_sum: 95.44813942909241
time_elpased: 1.96
batch start
#iterations: 308
currently lose_sum: 95.63721764087677
time_elpased: 1.925
batch start
#iterations: 309
currently lose_sum: 95.6493411064148
time_elpased: 1.941
batch start
#iterations: 310
currently lose_sum: 95.78563570976257
time_elpased: 1.944
batch start
#iterations: 311
currently lose_sum: 95.72778016328812
time_elpased: 1.912
batch start
#iterations: 312
currently lose_sum: 95.62586361169815
time_elpased: 1.957
batch start
#iterations: 313
currently lose_sum: 95.78313207626343
time_elpased: 1.928
batch start
#iterations: 314
currently lose_sum: 95.17964452505112
time_elpased: 1.954
batch start
#iterations: 315
currently lose_sum: 95.43504464626312
time_elpased: 1.898
batch start
#iterations: 316
currently lose_sum: 96.09235274791718
time_elpased: 1.939
batch start
#iterations: 317
currently lose_sum: 96.01040327548981
time_elpased: 1.993
batch start
#iterations: 318
currently lose_sum: 95.71488362550735
time_elpased: 1.935
batch start
#iterations: 319
currently lose_sum: 95.57288813591003
time_elpased: 1.941
start validation test
0.638865979381
0.621493232948
0.713594730884
0.664367155313
0.638734781532
62.021
batch start
#iterations: 320
currently lose_sum: 95.60388523340225
time_elpased: 1.97
batch start
#iterations: 321
currently lose_sum: 95.44101387262344
time_elpased: 1.96
batch start
#iterations: 322
currently lose_sum: 95.85649710893631
time_elpased: 1.953
batch start
#iterations: 323
currently lose_sum: 96.05347681045532
time_elpased: 1.918
batch start
#iterations: 324
currently lose_sum: 95.93630313873291
time_elpased: 1.943
batch start
#iterations: 325
currently lose_sum: 95.7744928598404
time_elpased: 1.932
batch start
#iterations: 326
currently lose_sum: 95.47738349437714
time_elpased: 1.956
batch start
#iterations: 327
currently lose_sum: 95.85784924030304
time_elpased: 1.916
batch start
#iterations: 328
currently lose_sum: 95.70300537347794
time_elpased: 1.954
batch start
#iterations: 329
currently lose_sum: 95.4272243976593
time_elpased: 1.937
batch start
#iterations: 330
currently lose_sum: 95.61274194717407
time_elpased: 1.943
batch start
#iterations: 331
currently lose_sum: 95.75394105911255
time_elpased: 1.977
batch start
#iterations: 332
currently lose_sum: 95.60231071710587
time_elpased: 1.939
batch start
#iterations: 333
currently lose_sum: 95.55453300476074
time_elpased: 2.077
batch start
#iterations: 334
currently lose_sum: 95.81348156929016
time_elpased: 1.936
batch start
#iterations: 335
currently lose_sum: 95.33305394649506
time_elpased: 1.932
batch start
#iterations: 336
currently lose_sum: 95.99272525310516
time_elpased: 1.972
batch start
#iterations: 337
currently lose_sum: 95.27495104074478
time_elpased: 1.934
batch start
#iterations: 338
currently lose_sum: 95.4581738114357
time_elpased: 1.932
batch start
#iterations: 339
currently lose_sum: 95.81257951259613
time_elpased: 1.921
start validation test
0.638711340206
0.615214431586
0.744056807657
0.673529274768
0.63852638999
62.102
batch start
#iterations: 340
currently lose_sum: 95.50601154565811
time_elpased: 1.935
batch start
#iterations: 341
currently lose_sum: 96.00825035572052
time_elpased: 1.941
batch start
#iterations: 342
currently lose_sum: 95.84012478590012
time_elpased: 2.0
batch start
#iterations: 343
currently lose_sum: 95.74349015951157
time_elpased: 2.015
batch start
#iterations: 344
currently lose_sum: 95.49583530426025
time_elpased: 1.954
batch start
#iterations: 345
currently lose_sum: 95.73449951410294
time_elpased: 1.979
batch start
#iterations: 346
currently lose_sum: 95.6032475233078
time_elpased: 1.981
batch start
#iterations: 347
currently lose_sum: 95.61371821165085
time_elpased: 1.965
batch start
#iterations: 348
currently lose_sum: 95.81280267238617
time_elpased: 1.981
batch start
#iterations: 349
currently lose_sum: 95.59306401014328
time_elpased: 1.969
batch start
#iterations: 350
currently lose_sum: 95.76795262098312
time_elpased: 1.923
batch start
#iterations: 351
currently lose_sum: 95.50941216945648
time_elpased: 1.951
batch start
#iterations: 352
currently lose_sum: 95.4043989777565
time_elpased: 1.927
batch start
#iterations: 353
currently lose_sum: 95.41155290603638
time_elpased: 1.922
batch start
#iterations: 354
currently lose_sum: 95.47448080778122
time_elpased: 2.017
batch start
#iterations: 355
currently lose_sum: 95.7026858329773
time_elpased: 1.957
batch start
#iterations: 356
currently lose_sum: 95.67303824424744
time_elpased: 1.892
batch start
#iterations: 357
currently lose_sum: 95.54470854997635
time_elpased: 1.921
batch start
#iterations: 358
currently lose_sum: 95.30179697275162
time_elpased: 1.919
batch start
#iterations: 359
currently lose_sum: 95.63707572221756
time_elpased: 1.931
start validation test
0.642680412371
0.612162706404
0.782134403623
0.686788360745
0.64243557938
61.856
batch start
#iterations: 360
currently lose_sum: 95.64018738269806
time_elpased: 1.963
batch start
#iterations: 361
currently lose_sum: 95.83655148744583
time_elpased: 1.975
batch start
#iterations: 362
currently lose_sum: 95.79623585939407
time_elpased: 1.946
batch start
#iterations: 363
currently lose_sum: 95.8513736128807
time_elpased: 1.922
batch start
#iterations: 364
currently lose_sum: 95.3233493566513
time_elpased: 2.034
batch start
#iterations: 365
currently lose_sum: 95.86597549915314
time_elpased: 1.937
batch start
#iterations: 366
currently lose_sum: 95.77173739671707
time_elpased: 1.994
batch start
#iterations: 367
currently lose_sum: 95.67541301250458
time_elpased: 1.966
batch start
#iterations: 368
currently lose_sum: 95.57599592208862
time_elpased: 1.994
batch start
#iterations: 369
currently lose_sum: 95.78292673826218
time_elpased: 1.991
batch start
#iterations: 370
currently lose_sum: 95.63293933868408
time_elpased: 1.944
batch start
#iterations: 371
currently lose_sum: 95.4494600892067
time_elpased: 1.991
batch start
#iterations: 372
currently lose_sum: 95.55663365125656
time_elpased: 2.019
batch start
#iterations: 373
currently lose_sum: 95.62109243869781
time_elpased: 2.086
batch start
#iterations: 374
currently lose_sum: 95.55427664518356
time_elpased: 1.924
batch start
#iterations: 375
currently lose_sum: 95.68835723400116
time_elpased: 1.906
batch start
#iterations: 376
currently lose_sum: 95.61000245809555
time_elpased: 1.96
batch start
#iterations: 377
currently lose_sum: 95.6432763338089
time_elpased: 1.936
batch start
#iterations: 378
currently lose_sum: 95.66222870349884
time_elpased: 2.016
batch start
#iterations: 379
currently lose_sum: 95.88200032711029
time_elpased: 1.926
start validation test
0.633092783505
0.613443910956
0.723165586086
0.66380124693
0.632934646807
62.084
batch start
#iterations: 380
currently lose_sum: 95.52869910001755
time_elpased: 1.953
batch start
#iterations: 381
currently lose_sum: 95.36280727386475
time_elpased: 1.907
batch start
#iterations: 382
currently lose_sum: 95.45972907543182
time_elpased: 1.929
batch start
#iterations: 383
currently lose_sum: 95.34231066703796
time_elpased: 1.934
batch start
#iterations: 384
currently lose_sum: 95.63445621728897
time_elpased: 2.028
batch start
#iterations: 385
currently lose_sum: 95.7581696510315
time_elpased: 2.035
batch start
#iterations: 386
currently lose_sum: 95.9009650349617
time_elpased: 1.98
batch start
#iterations: 387
currently lose_sum: 95.68349415063858
time_elpased: 1.987
batch start
#iterations: 388
currently lose_sum: 95.5456770658493
time_elpased: 1.95
batch start
#iterations: 389
currently lose_sum: 95.72898602485657
time_elpased: 1.997
batch start
#iterations: 390
currently lose_sum: 95.66833817958832
time_elpased: 2.026
batch start
#iterations: 391
currently lose_sum: 95.85408347845078
time_elpased: 1.948
batch start
#iterations: 392
currently lose_sum: 95.52274829149246
time_elpased: 1.932
batch start
#iterations: 393
currently lose_sum: 95.55608838796616
time_elpased: 1.963
batch start
#iterations: 394
currently lose_sum: 95.5011779665947
time_elpased: 1.923
batch start
#iterations: 395
currently lose_sum: 95.47725647687912
time_elpased: 1.952
batch start
#iterations: 396
currently lose_sum: 95.87447088956833
time_elpased: 1.935
batch start
#iterations: 397
currently lose_sum: 95.46976011991501
time_elpased: 2.122
batch start
#iterations: 398
currently lose_sum: 95.52172404527664
time_elpased: 1.97
batch start
#iterations: 399
currently lose_sum: 95.63035386800766
time_elpased: 1.967
start validation test
0.635721649485
0.618748879728
0.710507358238
0.661461077844
0.635590351638
62.244
acc: 0.634
pre: 0.605
rec: 0.775
F1: 0.679
auc: 0.633
