start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.85925054550171
time_elpased: 2.761
batch start
#iterations: 1
currently lose_sum: 100.31009662151337
time_elpased: 2.407
batch start
#iterations: 2
currently lose_sum: 99.97021889686584
time_elpased: 2.627
batch start
#iterations: 3
currently lose_sum: 99.82193499803543
time_elpased: 2.538
batch start
#iterations: 4
currently lose_sum: 99.83719789981842
time_elpased: 2.507
batch start
#iterations: 5
currently lose_sum: 99.67171055078506
time_elpased: 2.43
batch start
#iterations: 6
currently lose_sum: 99.46285665035248
time_elpased: 2.361
batch start
#iterations: 7
currently lose_sum: 99.147332072258
time_elpased: 2.369
batch start
#iterations: 8
currently lose_sum: 99.14372992515564
time_elpased: 2.362
batch start
#iterations: 9
currently lose_sum: 98.90148341655731
time_elpased: 2.21
batch start
#iterations: 10
currently lose_sum: 98.84982931613922
time_elpased: 2.318
batch start
#iterations: 11
currently lose_sum: 98.93242824077606
time_elpased: 2.404
batch start
#iterations: 12
currently lose_sum: 98.41903412342072
time_elpased: 2.414
batch start
#iterations: 13
currently lose_sum: 98.52037847042084
time_elpased: 2.482
batch start
#iterations: 14
currently lose_sum: 98.42906761169434
time_elpased: 2.443
batch start
#iterations: 15
currently lose_sum: 98.33679366111755
time_elpased: 2.363
batch start
#iterations: 16
currently lose_sum: 98.40761756896973
time_elpased: 2.417
batch start
#iterations: 17
currently lose_sum: 98.21692878007889
time_elpased: 2.353
batch start
#iterations: 18
currently lose_sum: 98.25149440765381
time_elpased: 2.402
batch start
#iterations: 19
currently lose_sum: 97.98879009485245
time_elpased: 2.429
start validation test
0.624432989691
0.612271299205
0.68196788802
0.645242964261
0.624337929966
63.616
batch start
#iterations: 20
currently lose_sum: 97.90696877241135
time_elpased: 2.399
batch start
#iterations: 21
currently lose_sum: 97.91778606176376
time_elpased: 2.392
batch start
#iterations: 22
currently lose_sum: 97.7854243516922
time_elpased: 2.276
batch start
#iterations: 23
currently lose_sum: 97.79222232103348
time_elpased: 2.41
batch start
#iterations: 24
currently lose_sum: 97.63756310939789
time_elpased: 2.333
batch start
#iterations: 25
currently lose_sum: 97.4523144364357
time_elpased: 2.317
batch start
#iterations: 26
currently lose_sum: 97.71160525083542
time_elpased: 2.46
batch start
#iterations: 27
currently lose_sum: 97.6044129729271
time_elpased: 2.457
batch start
#iterations: 28
currently lose_sum: 97.51140451431274
time_elpased: 2.393
batch start
#iterations: 29
currently lose_sum: 97.6883116364479
time_elpased: 2.425
batch start
#iterations: 30
currently lose_sum: 97.34464424848557
time_elpased: 2.416
batch start
#iterations: 31
currently lose_sum: 97.57506096363068
time_elpased: 2.495
batch start
#iterations: 32
currently lose_sum: 97.3380321264267
time_elpased: 2.444
batch start
#iterations: 33
currently lose_sum: 97.3791851401329
time_elpased: 2.351
batch start
#iterations: 34
currently lose_sum: 97.04812896251678
time_elpased: 2.527
batch start
#iterations: 35
currently lose_sum: 97.3259385228157
time_elpased: 2.484
batch start
#iterations: 36
currently lose_sum: 97.0100399851799
time_elpased: 2.448
batch start
#iterations: 37
currently lose_sum: 97.24457484483719
time_elpased: 2.493
batch start
#iterations: 38
currently lose_sum: 97.14486795663834
time_elpased: 2.511
batch start
#iterations: 39
currently lose_sum: 97.19851940870285
time_elpased: 2.497
start validation test
0.625618556701
0.619670211728
0.653664059284
0.636213373403
0.625572219646
63.108
batch start
#iterations: 40
currently lose_sum: 96.98787701129913
time_elpased: 2.488
batch start
#iterations: 41
currently lose_sum: 97.07969284057617
time_elpased: 2.548
batch start
#iterations: 42
currently lose_sum: 97.01433461904526
time_elpased: 2.541
batch start
#iterations: 43
currently lose_sum: 96.96622896194458
time_elpased: 2.563
batch start
#iterations: 44
currently lose_sum: 97.12671947479248
time_elpased: 2.56
batch start
#iterations: 45
currently lose_sum: 97.2198035120964
time_elpased: 2.462
batch start
#iterations: 46
currently lose_sum: 97.14462327957153
time_elpased: 2.456
batch start
#iterations: 47
currently lose_sum: 96.76453161239624
time_elpased: 2.445
batch start
#iterations: 48
currently lose_sum: 96.92824935913086
time_elpased: 2.434
batch start
#iterations: 49
currently lose_sum: 96.99463987350464
time_elpased: 2.438
batch start
#iterations: 50
currently lose_sum: 97.18477195501328
time_elpased: 2.456
batch start
#iterations: 51
currently lose_sum: 96.8688451051712
time_elpased: 2.497
batch start
#iterations: 52
currently lose_sum: 97.21391594409943
time_elpased: 2.501
batch start
#iterations: 53
currently lose_sum: 96.85087311267853
time_elpased: 2.503
batch start
#iterations: 54
currently lose_sum: 96.91093510389328
time_elpased: 2.401
batch start
#iterations: 55
currently lose_sum: 96.67851126194
time_elpased: 2.537
batch start
#iterations: 56
currently lose_sum: 97.10516625642776
time_elpased: 2.517
batch start
#iterations: 57
currently lose_sum: 96.85076600313187
time_elpased: 2.45
batch start
#iterations: 58
currently lose_sum: 96.67711865901947
time_elpased: 2.428
batch start
#iterations: 59
currently lose_sum: 96.89633238315582
time_elpased: 2.355
start validation test
0.634226804124
0.609973136333
0.747838616715
0.671906787498
0.63403909357
62.632
batch start
#iterations: 60
currently lose_sum: 96.84479343891144
time_elpased: 2.423
batch start
#iterations: 61
currently lose_sum: 97.20165967941284
time_elpased: 2.452
batch start
#iterations: 62
currently lose_sum: 97.27919614315033
time_elpased: 2.458
batch start
#iterations: 63
currently lose_sum: 96.71042174100876
time_elpased: 2.481
batch start
#iterations: 64
currently lose_sum: 96.82504343986511
time_elpased: 2.419
batch start
#iterations: 65
currently lose_sum: 96.86506497859955
time_elpased: 2.354
batch start
#iterations: 66
currently lose_sum: 96.79232788085938
time_elpased: 2.309
batch start
#iterations: 67
currently lose_sum: 96.78375566005707
time_elpased: 2.318
batch start
#iterations: 68
currently lose_sum: 96.84606748819351
time_elpased: 2.227
batch start
#iterations: 69
currently lose_sum: 96.76236432790756
time_elpased: 2.3
batch start
#iterations: 70
currently lose_sum: 96.9038535952568
time_elpased: 2.47
batch start
#iterations: 71
currently lose_sum: 96.51852637529373
time_elpased: 2.577
batch start
#iterations: 72
currently lose_sum: 96.52032768726349
time_elpased: 2.563
batch start
#iterations: 73
currently lose_sum: 96.61163640022278
time_elpased: 2.485
batch start
#iterations: 74
currently lose_sum: 96.98033821582794
time_elpased: 2.392
batch start
#iterations: 75
currently lose_sum: 96.98530894517899
time_elpased: 2.407
batch start
#iterations: 76
currently lose_sum: 96.9916924238205
time_elpased: 2.425
batch start
#iterations: 77
currently lose_sum: 96.81118017435074
time_elpased: 2.347
batch start
#iterations: 78
currently lose_sum: 96.98624563217163
time_elpased: 2.345
batch start
#iterations: 79
currently lose_sum: 96.53797471523285
time_elpased: 2.35
start validation test
0.625515463918
0.625396500563
0.629065459037
0.627225614449
0.625509598581
62.737
batch start
#iterations: 80
currently lose_sum: 96.80445069074631
time_elpased: 2.343
batch start
#iterations: 81
currently lose_sum: 96.63656640052795
time_elpased: 2.202
batch start
#iterations: 82
currently lose_sum: 96.47177851200104
time_elpased: 2.415
batch start
#iterations: 83
currently lose_sum: 96.7101702094078
time_elpased: 2.419
batch start
#iterations: 84
currently lose_sum: 96.54423868656158
time_elpased: 2.189
batch start
#iterations: 85
currently lose_sum: 96.37203055620193
time_elpased: 2.235
batch start
#iterations: 86
currently lose_sum: 96.62488824129105
time_elpased: 2.287
batch start
#iterations: 87
currently lose_sum: 96.37019062042236
time_elpased: 2.306
batch start
#iterations: 88
currently lose_sum: 96.87405455112457
time_elpased: 2.387
batch start
#iterations: 89
currently lose_sum: 96.7333357334137
time_elpased: 2.462
batch start
#iterations: 90
currently lose_sum: 96.5237655043602
time_elpased: 2.439
batch start
#iterations: 91
currently lose_sum: 96.77020418643951
time_elpased: 2.421
batch start
#iterations: 92
currently lose_sum: 96.3168193101883
time_elpased: 2.368
batch start
#iterations: 93
currently lose_sum: 96.62511783838272
time_elpased: 2.262
batch start
#iterations: 94
currently lose_sum: 96.53704386949539
time_elpased: 2.298
batch start
#iterations: 95
currently lose_sum: 96.91101384162903
time_elpased: 2.387
batch start
#iterations: 96
currently lose_sum: 96.28660750389099
time_elpased: 2.239
batch start
#iterations: 97
currently lose_sum: 96.24317073822021
time_elpased: 2.223
batch start
#iterations: 98
currently lose_sum: 96.46496653556824
time_elpased: 2.224
batch start
#iterations: 99
currently lose_sum: 96.5607437491417
time_elpased: 2.283
start validation test
0.621340206186
0.614537019138
0.654384520379
0.633835111155
0.621285610045
62.732
batch start
#iterations: 100
currently lose_sum: 96.67159044742584
time_elpased: 2.345
batch start
#iterations: 101
currently lose_sum: 96.60668617486954
time_elpased: 2.258
batch start
#iterations: 102
currently lose_sum: 96.66716933250427
time_elpased: 2.271
batch start
#iterations: 103
currently lose_sum: 96.60038161277771
time_elpased: 2.229
batch start
#iterations: 104
currently lose_sum: 96.60165810585022
time_elpased: 2.186
batch start
#iterations: 105
currently lose_sum: 96.43849778175354
time_elpased: 2.199
batch start
#iterations: 106
currently lose_sum: 96.4741268157959
time_elpased: 2.193
batch start
#iterations: 107
currently lose_sum: 96.95872801542282
time_elpased: 2.236
batch start
#iterations: 108
currently lose_sum: 96.41271704435349
time_elpased: 2.204
batch start
#iterations: 109
currently lose_sum: 96.3246260881424
time_elpased: 2.168
batch start
#iterations: 110
currently lose_sum: 96.41944360733032
time_elpased: 2.221
batch start
#iterations: 111
currently lose_sum: 96.59756416082382
time_elpased: 2.192
batch start
#iterations: 112
currently lose_sum: 96.33825767040253
time_elpased: 2.228
batch start
#iterations: 113
currently lose_sum: 96.58069151639938
time_elpased: 2.198
batch start
#iterations: 114
currently lose_sum: 96.26802599430084
time_elpased: 2.205
batch start
#iterations: 115
currently lose_sum: 96.43205106258392
time_elpased: 2.188
batch start
#iterations: 116
currently lose_sum: 96.73274451494217
time_elpased: 2.169
batch start
#iterations: 117
currently lose_sum: 96.39946293830872
time_elpased: 2.17
batch start
#iterations: 118
currently lose_sum: 96.99676072597504
time_elpased: 2.188
batch start
#iterations: 119
currently lose_sum: 96.47933489084244
time_elpased: 2.208
start validation test
0.636494845361
0.617792713123
0.719020172911
0.664573820396
0.636358496203
62.466
batch start
#iterations: 120
currently lose_sum: 96.23004442453384
time_elpased: 2.236
batch start
#iterations: 121
currently lose_sum: 96.4346661567688
time_elpased: 2.202
batch start
#iterations: 122
currently lose_sum: 96.62287557125092
time_elpased: 2.165
batch start
#iterations: 123
currently lose_sum: 96.53542464971542
time_elpased: 2.18
batch start
#iterations: 124
currently lose_sum: 96.65587455034256
time_elpased: 2.145
batch start
#iterations: 125
currently lose_sum: 96.91975390911102
time_elpased: 2.214
batch start
#iterations: 126
currently lose_sum: 96.40335184335709
time_elpased: 2.164
batch start
#iterations: 127
currently lose_sum: 96.7683322429657
time_elpased: 2.239
batch start
#iterations: 128
currently lose_sum: 96.42097353935242
time_elpased: 2.171
batch start
#iterations: 129
currently lose_sum: 96.28877609968185
time_elpased: 2.133
batch start
#iterations: 130
currently lose_sum: 96.32524508237839
time_elpased: 2.236
batch start
#iterations: 131
currently lose_sum: 96.28420734405518
time_elpased: 2.236
batch start
#iterations: 132
currently lose_sum: 96.3477076292038
time_elpased: 2.209
batch start
#iterations: 133
currently lose_sum: 96.42799413204193
time_elpased: 2.226
batch start
#iterations: 134
currently lose_sum: 96.44533962011337
time_elpased: 2.202
batch start
#iterations: 135
currently lose_sum: 96.30635637044907
time_elpased: 2.209
batch start
#iterations: 136
currently lose_sum: 96.41831493377686
time_elpased: 2.192
batch start
#iterations: 137
currently lose_sum: 96.00290882587433
time_elpased: 2.267
batch start
#iterations: 138
currently lose_sum: 96.51287764310837
time_elpased: 2.183
batch start
#iterations: 139
currently lose_sum: 96.31197273731232
time_elpased: 2.133
start validation test
0.633659793814
0.610279820779
0.743001235076
0.670132281272
0.63347913881
62.364
batch start
#iterations: 140
currently lose_sum: 95.93660312891006
time_elpased: 2.149
batch start
#iterations: 141
currently lose_sum: 96.55808931589127
time_elpased: 2.155
batch start
#iterations: 142
currently lose_sum: 96.33867758512497
time_elpased: 2.175
batch start
#iterations: 143
currently lose_sum: 96.26119655370712
time_elpased: 2.176
batch start
#iterations: 144
currently lose_sum: 96.6189278960228
time_elpased: 2.165
batch start
#iterations: 145
currently lose_sum: 96.27029705047607
time_elpased: 2.158
batch start
#iterations: 146
currently lose_sum: 96.06096971035004
time_elpased: 2.188
batch start
#iterations: 147
currently lose_sum: 96.35275667905807
time_elpased: 2.18
batch start
#iterations: 148
currently lose_sum: 96.85339719057083
time_elpased: 2.153
batch start
#iterations: 149
currently lose_sum: 96.7265328168869
time_elpased: 2.201
batch start
#iterations: 150
currently lose_sum: 96.17524856328964
time_elpased: 2.164
batch start
#iterations: 151
currently lose_sum: 96.55999326705933
time_elpased: 2.134
batch start
#iterations: 152
currently lose_sum: 96.24085801839828
time_elpased: 2.146
batch start
#iterations: 153
currently lose_sum: 96.4306812286377
time_elpased: 2.153
batch start
#iterations: 154
currently lose_sum: 96.27306818962097
time_elpased: 2.153
batch start
#iterations: 155
currently lose_sum: 96.39341354370117
time_elpased: 2.123
batch start
#iterations: 156
currently lose_sum: 96.3639640212059
time_elpased: 2.171
batch start
#iterations: 157
currently lose_sum: 96.30720221996307
time_elpased: 2.196
batch start
#iterations: 158
currently lose_sum: 96.37426352500916
time_elpased: 2.208
batch start
#iterations: 159
currently lose_sum: 96.28212672472
time_elpased: 2.282
start validation test
0.631237113402
0.609431060994
0.734252778921
0.666044253571
0.631066909907
62.451
batch start
#iterations: 160
currently lose_sum: 96.23834186792374
time_elpased: 2.191
batch start
#iterations: 161
currently lose_sum: 96.53991687297821
time_elpased: 2.163
batch start
#iterations: 162
currently lose_sum: 96.1438307762146
time_elpased: 2.227
batch start
#iterations: 163
currently lose_sum: 96.32249808311462
time_elpased: 2.159
batch start
#iterations: 164
currently lose_sum: 96.41110330820084
time_elpased: 2.18
batch start
#iterations: 165
currently lose_sum: 96.27500528097153
time_elpased: 2.238
batch start
#iterations: 166
currently lose_sum: 96.31700575351715
time_elpased: 2.147
batch start
#iterations: 167
currently lose_sum: 96.68153095245361
time_elpased: 2.162
batch start
#iterations: 168
currently lose_sum: 96.09825444221497
time_elpased: 2.185
batch start
#iterations: 169
currently lose_sum: 96.05854213237762
time_elpased: 2.197
batch start
#iterations: 170
currently lose_sum: 95.88212567567825
time_elpased: 2.158
batch start
#iterations: 171
currently lose_sum: 96.28346717357635
time_elpased: 2.139
batch start
#iterations: 172
currently lose_sum: 96.2011130452156
time_elpased: 2.156
batch start
#iterations: 173
currently lose_sum: 96.17302668094635
time_elpased: 2.281
batch start
#iterations: 174
currently lose_sum: 96.6182359457016
time_elpased: 2.194
batch start
#iterations: 175
currently lose_sum: 96.17179137468338
time_elpased: 2.169
batch start
#iterations: 176
currently lose_sum: 96.02089864015579
time_elpased: 2.195
batch start
#iterations: 177
currently lose_sum: 96.25812220573425
time_elpased: 2.222
batch start
#iterations: 178
currently lose_sum: 96.40812593698502
time_elpased: 2.168
batch start
#iterations: 179
currently lose_sum: 96.19459158182144
time_elpased: 2.227
start validation test
0.625824742268
0.622299651568
0.643371757925
0.632660290471
0.625795750916
62.646
batch start
#iterations: 180
currently lose_sum: 96.02858585119247
time_elpased: 2.179
batch start
#iterations: 181
currently lose_sum: 96.1048641204834
time_elpased: 2.129
batch start
#iterations: 182
currently lose_sum: 96.40626454353333
time_elpased: 2.111
batch start
#iterations: 183
currently lose_sum: 96.08397936820984
time_elpased: 2.102
batch start
#iterations: 184
currently lose_sum: 96.36764180660248
time_elpased: 2.105
batch start
#iterations: 185
currently lose_sum: 96.13781237602234
time_elpased: 2.167
batch start
#iterations: 186
currently lose_sum: 95.94823807477951
time_elpased: 2.148
batch start
#iterations: 187
currently lose_sum: 96.3658636212349
time_elpased: 2.12
batch start
#iterations: 188
currently lose_sum: 96.73235356807709
time_elpased: 2.122
batch start
#iterations: 189
currently lose_sum: 96.01178377866745
time_elpased: 2.172
batch start
#iterations: 190
currently lose_sum: 96.57284253835678
time_elpased: 2.188
batch start
#iterations: 191
currently lose_sum: 96.30271792411804
time_elpased: 2.134
batch start
#iterations: 192
currently lose_sum: 96.31919747591019
time_elpased: 2.141
batch start
#iterations: 193
currently lose_sum: 96.00240081548691
time_elpased: 2.172
batch start
#iterations: 194
currently lose_sum: 96.25742310285568
time_elpased: 2.156
batch start
#iterations: 195
currently lose_sum: 95.98904949426651
time_elpased: 2.221
batch start
#iterations: 196
currently lose_sum: 96.05483567714691
time_elpased: 2.165
batch start
#iterations: 197
currently lose_sum: 96.17123866081238
time_elpased: 2.171
batch start
#iterations: 198
currently lose_sum: 96.21064031124115
time_elpased: 2.167
batch start
#iterations: 199
currently lose_sum: 95.96433067321777
time_elpased: 2.195
start validation test
0.637989690722
0.612876184089
0.752470152326
0.675537075537
0.637800544978
62.434
batch start
#iterations: 200
currently lose_sum: 96.58937180042267
time_elpased: 2.241
batch start
#iterations: 201
currently lose_sum: 95.93956971168518
time_elpased: 2.157
batch start
#iterations: 202
currently lose_sum: 96.53328257799149
time_elpased: 2.217
batch start
#iterations: 203
currently lose_sum: 96.3135696053505
time_elpased: 2.297
batch start
#iterations: 204
currently lose_sum: 96.4747302532196
time_elpased: 2.218
batch start
#iterations: 205
currently lose_sum: 96.27947795391083
time_elpased: 2.191
batch start
#iterations: 206
currently lose_sum: 96.2958642244339
time_elpased: 2.252
batch start
#iterations: 207
currently lose_sum: 95.86073583364487
time_elpased: 2.259
batch start
#iterations: 208
currently lose_sum: 95.74769866466522
time_elpased: 2.329
batch start
#iterations: 209
currently lose_sum: 95.99931752681732
time_elpased: 2.313
batch start
#iterations: 210
currently lose_sum: 96.37283480167389
time_elpased: 2.293
batch start
#iterations: 211
currently lose_sum: 96.24585193395615
time_elpased: 2.235
batch start
#iterations: 212
currently lose_sum: 96.16188907623291
time_elpased: 2.181
batch start
#iterations: 213
currently lose_sum: 95.89216619729996
time_elpased: 2.25
batch start
#iterations: 214
currently lose_sum: 96.09130525588989
time_elpased: 2.219
batch start
#iterations: 215
currently lose_sum: 96.26139676570892
time_elpased: 2.241
batch start
#iterations: 216
currently lose_sum: 95.77848243713379
time_elpased: 2.246
batch start
#iterations: 217
currently lose_sum: 96.28758734464645
time_elpased: 2.229
batch start
#iterations: 218
currently lose_sum: 96.43565148115158
time_elpased: 2.236
batch start
#iterations: 219
currently lose_sum: 95.97915703058243
time_elpased: 2.157
start validation test
0.637371134021
0.614114241934
0.742486620008
0.67222662256
0.637197461181
62.323
batch start
#iterations: 220
currently lose_sum: 96.0452390909195
time_elpased: 2.262
batch start
#iterations: 221
currently lose_sum: 96.05915051698685
time_elpased: 2.196
batch start
#iterations: 222
currently lose_sum: 95.76844274997711
time_elpased: 2.183
batch start
#iterations: 223
currently lose_sum: 95.89870935678482
time_elpased: 2.146
batch start
#iterations: 224
currently lose_sum: 96.1011843085289
time_elpased: 2.235
batch start
#iterations: 225
currently lose_sum: 96.19396859407425
time_elpased: 2.234
batch start
#iterations: 226
currently lose_sum: 95.79386699199677
time_elpased: 2.229
batch start
#iterations: 227
currently lose_sum: 96.07363849878311
time_elpased: 2.201
batch start
#iterations: 228
currently lose_sum: 96.15880703926086
time_elpased: 2.22
batch start
#iterations: 229
currently lose_sum: 96.04161238670349
time_elpased: 2.247
batch start
#iterations: 230
currently lose_sum: 96.10424906015396
time_elpased: 2.257
batch start
#iterations: 231
currently lose_sum: 96.28291374444962
time_elpased: 2.295
batch start
#iterations: 232
currently lose_sum: 95.95330703258514
time_elpased: 2.26
batch start
#iterations: 233
currently lose_sum: 95.9395740032196
time_elpased: 2.311
batch start
#iterations: 234
currently lose_sum: 96.07817870378494
time_elpased: 2.245
batch start
#iterations: 235
currently lose_sum: 96.19026952981949
time_elpased: 2.347
batch start
#iterations: 236
currently lose_sum: 96.33917534351349
time_elpased: 2.385
batch start
#iterations: 237
currently lose_sum: 96.3226090669632
time_elpased: 2.338
batch start
#iterations: 238
currently lose_sum: 95.78022307157516
time_elpased: 2.232
batch start
#iterations: 239
currently lose_sum: 96.14044463634491
time_elpased: 2.263
start validation test
0.63675257732
0.614657616634
0.736311239193
0.670007024116
0.63658808552
62.281
batch start
#iterations: 240
currently lose_sum: 96.07773888111115
time_elpased: 2.23
batch start
#iterations: 241
currently lose_sum: 95.71995741128922
time_elpased: 2.16
batch start
#iterations: 242
currently lose_sum: 96.08621233701706
time_elpased: 2.228
batch start
#iterations: 243
currently lose_sum: 96.49939686059952
time_elpased: 2.221
batch start
#iterations: 244
currently lose_sum: 95.66154772043228
time_elpased: 2.147
batch start
#iterations: 245
currently lose_sum: 96.19421422481537
time_elpased: 2.249
batch start
#iterations: 246
currently lose_sum: 95.78480100631714
time_elpased: 2.177
batch start
#iterations: 247
currently lose_sum: 95.93926191329956
time_elpased: 2.157
batch start
#iterations: 248
currently lose_sum: 96.22780573368073
time_elpased: 2.275
batch start
#iterations: 249
currently lose_sum: 96.55364918708801
time_elpased: 2.306
batch start
#iterations: 250
currently lose_sum: 96.1122995018959
time_elpased: 2.291
batch start
#iterations: 251
currently lose_sum: 95.88317084312439
time_elpased: 2.199
batch start
#iterations: 252
currently lose_sum: 95.88678401708603
time_elpased: 2.24
batch start
#iterations: 253
currently lose_sum: 96.30242073535919
time_elpased: 2.249
batch start
#iterations: 254
currently lose_sum: 96.02096247673035
time_elpased: 2.271
batch start
#iterations: 255
currently lose_sum: 95.9650958776474
time_elpased: 2.237
batch start
#iterations: 256
currently lose_sum: 95.94400197267532
time_elpased: 2.179
batch start
#iterations: 257
currently lose_sum: 96.38218313455582
time_elpased: 2.204
batch start
#iterations: 258
currently lose_sum: 96.0944030880928
time_elpased: 2.167
batch start
#iterations: 259
currently lose_sum: 96.220223903656
time_elpased: 2.146
start validation test
0.634896907216
0.619540542995
0.702243721696
0.65830479039
0.634785636148
62.574
batch start
#iterations: 260
currently lose_sum: 96.13869369029999
time_elpased: 2.237
batch start
#iterations: 261
currently lose_sum: 96.20161408185959
time_elpased: 2.275
batch start
#iterations: 262
currently lose_sum: 95.79916560649872
time_elpased: 2.203
batch start
#iterations: 263
currently lose_sum: 95.94887214899063
time_elpased: 2.211
batch start
#iterations: 264
currently lose_sum: 96.0439423918724
time_elpased: 2.15
batch start
#iterations: 265
currently lose_sum: 96.02470153570175
time_elpased: 2.101
batch start
#iterations: 266
currently lose_sum: 95.89939105510712
time_elpased: 2.253
batch start
#iterations: 267
currently lose_sum: 96.06713879108429
time_elpased: 2.19
batch start
#iterations: 268
currently lose_sum: 95.90025281906128
time_elpased: 2.118
batch start
#iterations: 269
currently lose_sum: 96.24766474962234
time_elpased: 2.117
batch start
#iterations: 270
currently lose_sum: 95.51275849342346
time_elpased: 2.112
batch start
#iterations: 271
currently lose_sum: 95.74931621551514
time_elpased: 2.16
batch start
#iterations: 272
currently lose_sum: 95.81707853078842
time_elpased: 2.162
batch start
#iterations: 273
currently lose_sum: 96.05323678255081
time_elpased: 2.55
batch start
#iterations: 274
currently lose_sum: 95.92801451683044
time_elpased: 2.558
batch start
#iterations: 275
currently lose_sum: 96.03598445653915
time_elpased: 2.689
batch start
#iterations: 276
currently lose_sum: 95.91853332519531
time_elpased: 2.559
batch start
#iterations: 277
currently lose_sum: 95.86598539352417
time_elpased: 2.151
batch start
#iterations: 278
currently lose_sum: 96.1605275273323
time_elpased: 2.162
batch start
#iterations: 279
currently lose_sum: 95.96554505825043
time_elpased: 2.205
start validation test
0.639587628866
0.615914893617
0.744853849321
0.674275598621
0.639413706982
62.421
batch start
#iterations: 280
currently lose_sum: 96.14632016420364
time_elpased: 2.225
batch start
#iterations: 281
currently lose_sum: 96.06508338451385
time_elpased: 2.196
batch start
#iterations: 282
currently lose_sum: 95.77086156606674
time_elpased: 2.203
batch start
#iterations: 283
currently lose_sum: 96.23598921298981
time_elpased: 2.212
batch start
#iterations: 284
currently lose_sum: 96.15223157405853
time_elpased: 2.279
batch start
#iterations: 285
currently lose_sum: 95.98777240514755
time_elpased: 2.255
batch start
#iterations: 286
currently lose_sum: 95.49113655090332
time_elpased: 2.289
batch start
#iterations: 287
currently lose_sum: 96.2014547586441
time_elpased: 2.19
batch start
#iterations: 288
currently lose_sum: 96.02738732099533
time_elpased: 2.226
batch start
#iterations: 289
currently lose_sum: 96.05718171596527
time_elpased: 2.281
batch start
#iterations: 290
currently lose_sum: 95.73660624027252
time_elpased: 2.317
batch start
#iterations: 291
currently lose_sum: 95.94756841659546
time_elpased: 2.229
batch start
#iterations: 292
currently lose_sum: 95.6319265961647
time_elpased: 2.229
batch start
#iterations: 293
currently lose_sum: 96.13222980499268
time_elpased: 2.247
batch start
#iterations: 294
currently lose_sum: 95.88087344169617
time_elpased: 2.318
batch start
#iterations: 295
currently lose_sum: 96.14939564466476
time_elpased: 2.281
batch start
#iterations: 296
currently lose_sum: 95.74882584810257
time_elpased: 2.289
batch start
#iterations: 297
currently lose_sum: 95.95507484674454
time_elpased: 2.233
batch start
#iterations: 298
currently lose_sum: 96.00672489404678
time_elpased: 2.226
batch start
#iterations: 299
currently lose_sum: 95.79110443592072
time_elpased: 2.205
start validation test
0.631958762887
0.619104864065
0.689069575957
0.652216268875
0.63186440384
62.499
batch start
#iterations: 300
currently lose_sum: 95.94414168596268
time_elpased: 2.175
batch start
#iterations: 301
currently lose_sum: 95.69363403320312
time_elpased: 2.223
batch start
#iterations: 302
currently lose_sum: 96.05088531970978
time_elpased: 2.164
batch start
#iterations: 303
currently lose_sum: 95.8974592089653
time_elpased: 2.19
batch start
#iterations: 304
currently lose_sum: 95.8388220667839
time_elpased: 2.197
batch start
#iterations: 305
currently lose_sum: 96.04700511693954
time_elpased: 2.215
batch start
#iterations: 306
currently lose_sum: 95.61257952451706
time_elpased: 2.198
batch start
#iterations: 307
currently lose_sum: 96.02578592300415
time_elpased: 2.248
batch start
#iterations: 308
currently lose_sum: 95.82481038570404
time_elpased: 2.233
batch start
#iterations: 309
currently lose_sum: 95.72194528579712
time_elpased: 2.362
batch start
#iterations: 310
currently lose_sum: 95.96340650320053
time_elpased: 2.343
batch start
#iterations: 311
currently lose_sum: 95.64250040054321
time_elpased: 2.309
batch start
#iterations: 312
currently lose_sum: 95.99014991521835
time_elpased: 2.246
batch start
#iterations: 313
currently lose_sum: 96.03243112564087
time_elpased: 2.254
batch start
#iterations: 314
currently lose_sum: 95.77862030267715
time_elpased: 2.252
batch start
#iterations: 315
currently lose_sum: 95.64519333839417
time_elpased: 2.291
batch start
#iterations: 316
currently lose_sum: 95.94606852531433
time_elpased: 2.315
batch start
#iterations: 317
currently lose_sum: 95.70902174711227
time_elpased: 2.259
batch start
#iterations: 318
currently lose_sum: 95.69653409719467
time_elpased: 2.279
batch start
#iterations: 319
currently lose_sum: 96.31613862514496
time_elpased: 2.478
start validation test
0.635824742268
0.619965607747
0.705022643063
0.659764026005
0.635710412816
62.334
batch start
#iterations: 320
currently lose_sum: 95.91609901189804
time_elpased: 2.358
batch start
#iterations: 321
currently lose_sum: 96.28131145238876
time_elpased: 2.295
batch start
#iterations: 322
currently lose_sum: 95.88618701696396
time_elpased: 2.659
batch start
#iterations: 323
currently lose_sum: 95.7931877374649
time_elpased: 2.459
batch start
#iterations: 324
currently lose_sum: 96.11169695854187
time_elpased: 2.343
batch start
#iterations: 325
currently lose_sum: 95.37571215629578
time_elpased: 2.42
batch start
#iterations: 326
currently lose_sum: 95.97787666320801
time_elpased: 2.366
batch start
#iterations: 327
currently lose_sum: 95.93936043977737
time_elpased: 2.432
batch start
#iterations: 328
currently lose_sum: 95.65106493234634
time_elpased: 2.45
batch start
#iterations: 329
currently lose_sum: 95.49552339315414
time_elpased: 2.411
batch start
#iterations: 330
currently lose_sum: 95.93810331821442
time_elpased: 2.422
batch start
#iterations: 331
currently lose_sum: 95.91248095035553
time_elpased: 2.447
batch start
#iterations: 332
currently lose_sum: 95.72988897562027
time_elpased: 2.351
batch start
#iterations: 333
currently lose_sum: 95.69297879934311
time_elpased: 2.422
batch start
#iterations: 334
currently lose_sum: 95.75467228889465
time_elpased: 2.474
batch start
#iterations: 335
currently lose_sum: 95.78680658340454
time_elpased: 2.512
batch start
#iterations: 336
currently lose_sum: 96.01383078098297
time_elpased: 2.474
batch start
#iterations: 337
currently lose_sum: 95.87428689002991
time_elpased: 2.378
batch start
#iterations: 338
currently lose_sum: 95.577372610569
time_elpased: 2.486
batch start
#iterations: 339
currently lose_sum: 95.76708799600601
time_elpased: 2.409
start validation test
0.637216494845
0.610442098317
0.761733223549
0.677747252747
0.637010767082
62.216
batch start
#iterations: 340
currently lose_sum: 95.90963238477707
time_elpased: 2.513
batch start
#iterations: 341
currently lose_sum: 95.76629495620728
time_elpased: 2.304
batch start
#iterations: 342
currently lose_sum: 95.8681463599205
time_elpased: 2.332
batch start
#iterations: 343
currently lose_sum: 96.0041715502739
time_elpased: 2.268
batch start
#iterations: 344
currently lose_sum: 96.08221566677094
time_elpased: 2.314
batch start
#iterations: 345
currently lose_sum: 95.73447704315186
time_elpased: 2.382
batch start
#iterations: 346
currently lose_sum: 96.13165354728699
time_elpased: 2.49
batch start
#iterations: 347
currently lose_sum: 95.7678484916687
time_elpased: 2.473
batch start
#iterations: 348
currently lose_sum: 96.0278531908989
time_elpased: 2.478
batch start
#iterations: 349
currently lose_sum: 95.72527128458023
time_elpased: 2.351
batch start
#iterations: 350
currently lose_sum: 96.14895212650299
time_elpased: 2.351
batch start
#iterations: 351
currently lose_sum: 96.08719265460968
time_elpased: 2.284
batch start
#iterations: 352
currently lose_sum: 95.8086239695549
time_elpased: 2.286
batch start
#iterations: 353
currently lose_sum: 96.15645450353622
time_elpased: 2.249
batch start
#iterations: 354
currently lose_sum: 95.9226233959198
time_elpased: 2.276
batch start
#iterations: 355
currently lose_sum: 95.71120780706406
time_elpased: 2.328
batch start
#iterations: 356
currently lose_sum: 95.62406831979752
time_elpased: 2.327
batch start
#iterations: 357
currently lose_sum: 95.96760982275009
time_elpased: 2.329
batch start
#iterations: 358
currently lose_sum: 96.02206963300705
time_elpased: 2.454
batch start
#iterations: 359
currently lose_sum: 95.4709278345108
time_elpased: 2.504
start validation test
0.63412371134
0.604586129754
0.778818443804
0.680730478589
0.633884645281
62.267
batch start
#iterations: 360
currently lose_sum: 96.00221121311188
time_elpased: 2.504
batch start
#iterations: 361
currently lose_sum: 95.89232355356216
time_elpased: 2.579
batch start
#iterations: 362
currently lose_sum: 95.88223373889923
time_elpased: 2.528
batch start
#iterations: 363
currently lose_sum: 95.94477075338364
time_elpased: 2.579
batch start
#iterations: 364
currently lose_sum: 95.95837432146072
time_elpased: 2.769
batch start
#iterations: 365
currently lose_sum: 96.17383128404617
time_elpased: 2.846
batch start
#iterations: 366
currently lose_sum: 96.1359286904335
time_elpased: 2.642
batch start
#iterations: 367
currently lose_sum: 95.58004152774811
time_elpased: 2.686
batch start
#iterations: 368
currently lose_sum: 95.59114933013916
time_elpased: 2.722
batch start
#iterations: 369
currently lose_sum: 95.92174297571182
time_elpased: 2.515
batch start
#iterations: 370
currently lose_sum: 95.90019637346268
time_elpased: 2.363
batch start
#iterations: 371
currently lose_sum: 95.90325570106506
time_elpased: 2.466
batch start
#iterations: 372
currently lose_sum: 96.01989883184433
time_elpased: 2.491
batch start
#iterations: 373
currently lose_sum: 95.81360918283463
time_elpased: 2.518
batch start
#iterations: 374
currently lose_sum: 95.66895067691803
time_elpased: 2.487
batch start
#iterations: 375
currently lose_sum: 96.09591609239578
time_elpased: 2.488
batch start
#iterations: 376
currently lose_sum: 95.64763081073761
time_elpased: 2.628
batch start
#iterations: 377
currently lose_sum: 95.81015712022781
time_elpased: 2.618
batch start
#iterations: 378
currently lose_sum: 95.91620409488678
time_elpased: 2.729
batch start
#iterations: 379
currently lose_sum: 96.08501505851746
time_elpased: 2.497
start validation test
0.636340206186
0.608232327341
0.769555372581
0.679449316189
0.636120106778
62.282
batch start
#iterations: 380
currently lose_sum: 95.87276846170425
time_elpased: 2.428
batch start
#iterations: 381
currently lose_sum: 95.76190775632858
time_elpased: 2.388
batch start
#iterations: 382
currently lose_sum: 96.0527001619339
time_elpased: 2.421
batch start
#iterations: 383
currently lose_sum: 96.04512059688568
time_elpased: 2.459
batch start
#iterations: 384
currently lose_sum: 95.69590753316879
time_elpased: 2.498
batch start
#iterations: 385
currently lose_sum: 95.43010431528091
time_elpased: 2.488
batch start
#iterations: 386
currently lose_sum: 95.72324562072754
time_elpased: 2.589
batch start
#iterations: 387
currently lose_sum: 95.68745106458664
time_elpased: 2.492
batch start
#iterations: 388
currently lose_sum: 95.77536177635193
time_elpased: 2.519
batch start
#iterations: 389
currently lose_sum: 95.71855998039246
time_elpased: 2.46
batch start
#iterations: 390
currently lose_sum: 95.58339202404022
time_elpased: 2.587
batch start
#iterations: 391
currently lose_sum: 95.37634640932083
time_elpased: 2.578
batch start
#iterations: 392
currently lose_sum: 95.69120836257935
time_elpased: 2.481
batch start
#iterations: 393
currently lose_sum: 95.70875722169876
time_elpased: 2.402
batch start
#iterations: 394
currently lose_sum: 95.79815036058426
time_elpased: 2.52
batch start
#iterations: 395
currently lose_sum: 95.73732817173004
time_elpased: 2.435
batch start
#iterations: 396
currently lose_sum: 95.84546369314194
time_elpased: 2.419
batch start
#iterations: 397
currently lose_sum: 95.78245145082474
time_elpased: 2.474
batch start
#iterations: 398
currently lose_sum: 95.21895849704742
time_elpased: 2.463
batch start
#iterations: 399
currently lose_sum: 95.71172589063644
time_elpased: 2.456
start validation test
0.637577319588
0.611958969227
0.755249073693
0.676095268807
0.637382901159
62.436
acc: 0.640
pre: 0.613
rec: 0.766
F1: 0.681
auc: 0.640
