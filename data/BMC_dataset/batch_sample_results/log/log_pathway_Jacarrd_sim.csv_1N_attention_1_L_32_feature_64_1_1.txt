start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.8740023970604
time_elpased: 1.975
batch start
#iterations: 1
currently lose_sum: 100.3856794834137
time_elpased: 1.862
batch start
#iterations: 2
currently lose_sum: 100.24791795015335
time_elpased: 1.899
batch start
#iterations: 3
currently lose_sum: 100.18924504518509
time_elpased: 1.822
batch start
#iterations: 4
currently lose_sum: 99.90378522872925
time_elpased: 1.857
batch start
#iterations: 5
currently lose_sum: 99.71444231271744
time_elpased: 1.887
batch start
#iterations: 6
currently lose_sum: 99.48259627819061
time_elpased: 1.85
batch start
#iterations: 7
currently lose_sum: 99.45347183942795
time_elpased: 1.851
batch start
#iterations: 8
currently lose_sum: 99.0806679725647
time_elpased: 1.905
batch start
#iterations: 9
currently lose_sum: 99.24055671691895
time_elpased: 1.817
batch start
#iterations: 10
currently lose_sum: 99.20510357618332
time_elpased: 1.826
batch start
#iterations: 11
currently lose_sum: 98.87280207872391
time_elpased: 1.85
batch start
#iterations: 12
currently lose_sum: 99.01119512319565
time_elpased: 1.838
batch start
#iterations: 13
currently lose_sum: 98.76070469617844
time_elpased: 1.856
batch start
#iterations: 14
currently lose_sum: 98.53969079256058
time_elpased: 1.883
batch start
#iterations: 15
currently lose_sum: 98.53528118133545
time_elpased: 1.837
batch start
#iterations: 16
currently lose_sum: 98.33035182952881
time_elpased: 1.87
batch start
#iterations: 17
currently lose_sum: 98.5301998257637
time_elpased: 1.819
batch start
#iterations: 18
currently lose_sum: 98.13657170534134
time_elpased: 1.871
batch start
#iterations: 19
currently lose_sum: 98.10633587837219
time_elpased: 1.92
start validation test
0.620360824742
0.640535372849
0.551610579397
0.592756427979
0.620481526402
63.710
batch start
#iterations: 20
currently lose_sum: 98.2044506072998
time_elpased: 1.961
batch start
#iterations: 21
currently lose_sum: 97.83653527498245
time_elpased: 1.909
batch start
#iterations: 22
currently lose_sum: 98.11235147714615
time_elpased: 1.833
batch start
#iterations: 23
currently lose_sum: 98.22846704721451
time_elpased: 1.843
batch start
#iterations: 24
currently lose_sum: 97.70201003551483
time_elpased: 1.889
batch start
#iterations: 25
currently lose_sum: 97.92556023597717
time_elpased: 1.842
batch start
#iterations: 26
currently lose_sum: 97.71590781211853
time_elpased: 1.862
batch start
#iterations: 27
currently lose_sum: 97.9617183804512
time_elpased: 1.857
batch start
#iterations: 28
currently lose_sum: 97.57095444202423
time_elpased: 1.871
batch start
#iterations: 29
currently lose_sum: 98.08181917667389
time_elpased: 1.87
batch start
#iterations: 30
currently lose_sum: 97.41878336668015
time_elpased: 1.854
batch start
#iterations: 31
currently lose_sum: 97.61124837398529
time_elpased: 1.866
batch start
#iterations: 32
currently lose_sum: 97.53961026668549
time_elpased: 1.819
batch start
#iterations: 33
currently lose_sum: 97.56332737207413
time_elpased: 1.834
batch start
#iterations: 34
currently lose_sum: 97.41424471139908
time_elpased: 1.847
batch start
#iterations: 35
currently lose_sum: 97.60584545135498
time_elpased: 1.823
batch start
#iterations: 36
currently lose_sum: 97.35462075471878
time_elpased: 1.829
batch start
#iterations: 37
currently lose_sum: 97.11559897661209
time_elpased: 1.864
batch start
#iterations: 38
currently lose_sum: 97.18268698453903
time_elpased: 1.829
batch start
#iterations: 39
currently lose_sum: 97.25416368246078
time_elpased: 1.903
start validation test
0.624587628866
0.633005464481
0.596068745498
0.613982085122
0.624637698164
62.984
batch start
#iterations: 40
currently lose_sum: 97.22939640283585
time_elpased: 1.913
batch start
#iterations: 41
currently lose_sum: 97.10634273290634
time_elpased: 1.882
batch start
#iterations: 42
currently lose_sum: 97.3114909529686
time_elpased: 1.836
batch start
#iterations: 43
currently lose_sum: 97.03478181362152
time_elpased: 1.886
batch start
#iterations: 44
currently lose_sum: 97.04649257659912
time_elpased: 1.858
batch start
#iterations: 45
currently lose_sum: 96.90947926044464
time_elpased: 1.861
batch start
#iterations: 46
currently lose_sum: 96.8387525677681
time_elpased: 1.847
batch start
#iterations: 47
currently lose_sum: 97.21880793571472
time_elpased: 1.848
batch start
#iterations: 48
currently lose_sum: 97.2563066482544
time_elpased: 1.819
batch start
#iterations: 49
currently lose_sum: 97.07521957159042
time_elpased: 1.875
batch start
#iterations: 50
currently lose_sum: 97.45771378278732
time_elpased: 1.832
batch start
#iterations: 51
currently lose_sum: 97.03409045934677
time_elpased: 1.832
batch start
#iterations: 52
currently lose_sum: 97.15155893564224
time_elpased: 1.83
batch start
#iterations: 53
currently lose_sum: 97.16156220436096
time_elpased: 1.844
batch start
#iterations: 54
currently lose_sum: 96.95241916179657
time_elpased: 1.844
batch start
#iterations: 55
currently lose_sum: 96.7485961318016
time_elpased: 1.857
batch start
#iterations: 56
currently lose_sum: 96.99091809988022
time_elpased: 1.83
batch start
#iterations: 57
currently lose_sum: 96.87021434307098
time_elpased: 1.829
batch start
#iterations: 58
currently lose_sum: 96.9190593957901
time_elpased: 1.85
batch start
#iterations: 59
currently lose_sum: 97.04599332809448
time_elpased: 1.893
start validation test
0.640618556701
0.620532185826
0.727179170526
0.669636087945
0.640466586192
62.597
batch start
#iterations: 60
currently lose_sum: 96.72386515140533
time_elpased: 1.862
batch start
#iterations: 61
currently lose_sum: 97.06988769769669
time_elpased: 1.856
batch start
#iterations: 62
currently lose_sum: 96.71365070343018
time_elpased: 1.811
batch start
#iterations: 63
currently lose_sum: 96.88525748252869
time_elpased: 1.902
batch start
#iterations: 64
currently lose_sum: 96.77754706144333
time_elpased: 1.829
batch start
#iterations: 65
currently lose_sum: 96.69653934240341
time_elpased: 1.843
batch start
#iterations: 66
currently lose_sum: 96.88392007350922
time_elpased: 1.883
batch start
#iterations: 67
currently lose_sum: 96.75470316410065
time_elpased: 1.91
batch start
#iterations: 68
currently lose_sum: 97.22310447692871
time_elpased: 1.922
batch start
#iterations: 69
currently lose_sum: 96.79862385988235
time_elpased: 1.845
batch start
#iterations: 70
currently lose_sum: 96.52201896905899
time_elpased: 1.952
batch start
#iterations: 71
currently lose_sum: 96.47100079059601
time_elpased: 1.944
batch start
#iterations: 72
currently lose_sum: 96.86320698261261
time_elpased: 1.843
batch start
#iterations: 73
currently lose_sum: 96.48310834169388
time_elpased: 1.844
batch start
#iterations: 74
currently lose_sum: 96.66844379901886
time_elpased: 1.823
batch start
#iterations: 75
currently lose_sum: 96.49541157484055
time_elpased: 1.874
batch start
#iterations: 76
currently lose_sum: 96.47051900625229
time_elpased: 1.925
batch start
#iterations: 77
currently lose_sum: 96.66302913427353
time_elpased: 1.917
batch start
#iterations: 78
currently lose_sum: 96.88053560256958
time_elpased: 1.842
batch start
#iterations: 79
currently lose_sum: 96.52046382427216
time_elpased: 1.864
start validation test
0.640773195876
0.6125491481
0.769579088196
0.682143671608
0.640547057265
62.361
batch start
#iterations: 80
currently lose_sum: 96.66545820236206
time_elpased: 1.862
batch start
#iterations: 81
currently lose_sum: 96.88659209012985
time_elpased: 1.872
batch start
#iterations: 82
currently lose_sum: 96.78591519594193
time_elpased: 1.86
batch start
#iterations: 83
currently lose_sum: 96.60195410251617
time_elpased: 1.831
batch start
#iterations: 84
currently lose_sum: 96.58065813779831
time_elpased: 1.855
batch start
#iterations: 85
currently lose_sum: 96.65809214115143
time_elpased: 1.926
batch start
#iterations: 86
currently lose_sum: 96.78368920087814
time_elpased: 1.892
batch start
#iterations: 87
currently lose_sum: 96.55812883377075
time_elpased: 1.892
batch start
#iterations: 88
currently lose_sum: 96.6322768330574
time_elpased: 1.833
batch start
#iterations: 89
currently lose_sum: 96.41779047250748
time_elpased: 1.846
batch start
#iterations: 90
currently lose_sum: 96.62361478805542
time_elpased: 1.883
batch start
#iterations: 91
currently lose_sum: 96.56007957458496
time_elpased: 1.9
batch start
#iterations: 92
currently lose_sum: 96.54675590991974
time_elpased: 1.86
batch start
#iterations: 93
currently lose_sum: 96.70111328363419
time_elpased: 1.877
batch start
#iterations: 94
currently lose_sum: 96.59872508049011
time_elpased: 1.834
batch start
#iterations: 95
currently lose_sum: 96.35328483581543
time_elpased: 1.841
batch start
#iterations: 96
currently lose_sum: 96.98321044445038
time_elpased: 2.02
batch start
#iterations: 97
currently lose_sum: 96.40472149848938
time_elpased: 1.868
batch start
#iterations: 98
currently lose_sum: 96.22419410943985
time_elpased: 1.846
batch start
#iterations: 99
currently lose_sum: 96.33982586860657
time_elpased: 1.882
start validation test
0.640309278351
0.621765804214
0.719666563754
0.667143674871
0.640169954396
62.270
batch start
#iterations: 100
currently lose_sum: 96.36239850521088
time_elpased: 1.97
batch start
#iterations: 101
currently lose_sum: 96.46318286657333
time_elpased: 1.833
batch start
#iterations: 102
currently lose_sum: 96.29254275560379
time_elpased: 1.856
batch start
#iterations: 103
currently lose_sum: 96.59690034389496
time_elpased: 1.852
batch start
#iterations: 104
currently lose_sum: 96.64556586742401
time_elpased: 1.876
batch start
#iterations: 105
currently lose_sum: 96.56728833913803
time_elpased: 1.878
batch start
#iterations: 106
currently lose_sum: 96.4630925655365
time_elpased: 1.88
batch start
#iterations: 107
currently lose_sum: 96.60389339923859
time_elpased: 1.873
batch start
#iterations: 108
currently lose_sum: 96.45103704929352
time_elpased: 1.835
batch start
#iterations: 109
currently lose_sum: 96.42940431833267
time_elpased: 1.878
batch start
#iterations: 110
currently lose_sum: 96.15987479686737
time_elpased: 1.83
batch start
#iterations: 111
currently lose_sum: 96.42097413539886
time_elpased: 1.824
batch start
#iterations: 112
currently lose_sum: 96.36408060789108
time_elpased: 1.833
batch start
#iterations: 113
currently lose_sum: 96.26871132850647
time_elpased: 1.857
batch start
#iterations: 114
currently lose_sum: 96.25593429803848
time_elpased: 1.868
batch start
#iterations: 115
currently lose_sum: 96.48270916938782
time_elpased: 1.839
batch start
#iterations: 116
currently lose_sum: 96.54185551404953
time_elpased: 1.85
batch start
#iterations: 117
currently lose_sum: 96.39730948209763
time_elpased: 1.938
batch start
#iterations: 118
currently lose_sum: 96.1450406908989
time_elpased: 1.914
batch start
#iterations: 119
currently lose_sum: 96.43264400959015
time_elpased: 1.856
start validation test
0.642010309278
0.61375574524
0.769579088196
0.682891192183
0.641786342611
62.170
batch start
#iterations: 120
currently lose_sum: 96.5613266825676
time_elpased: 1.882
batch start
#iterations: 121
currently lose_sum: 96.34255808591843
time_elpased: 1.868
batch start
#iterations: 122
currently lose_sum: 96.25162267684937
time_elpased: 1.884
batch start
#iterations: 123
currently lose_sum: 96.11618459224701
time_elpased: 1.85
batch start
#iterations: 124
currently lose_sum: 96.55558955669403
time_elpased: 1.871
batch start
#iterations: 125
currently lose_sum: 96.33034366369247
time_elpased: 1.883
batch start
#iterations: 126
currently lose_sum: 96.41239356994629
time_elpased: 1.862
batch start
#iterations: 127
currently lose_sum: 96.45329493284225
time_elpased: 1.887
batch start
#iterations: 128
currently lose_sum: 96.19377428293228
time_elpased: 1.885
batch start
#iterations: 129
currently lose_sum: 96.23921072483063
time_elpased: 1.907
batch start
#iterations: 130
currently lose_sum: 96.13617354631424
time_elpased: 1.907
batch start
#iterations: 131
currently lose_sum: 96.3063794374466
time_elpased: 1.9
batch start
#iterations: 132
currently lose_sum: 96.31126111745834
time_elpased: 1.908
batch start
#iterations: 133
currently lose_sum: 95.80888789892197
time_elpased: 1.887
batch start
#iterations: 134
currently lose_sum: 96.70707666873932
time_elpased: 1.922
batch start
#iterations: 135
currently lose_sum: 96.1495754122734
time_elpased: 1.882
batch start
#iterations: 136
currently lose_sum: 96.23203212022781
time_elpased: 1.888
batch start
#iterations: 137
currently lose_sum: 95.88662129640579
time_elpased: 1.895
batch start
#iterations: 138
currently lose_sum: 96.12949925661087
time_elpased: 1.854
batch start
#iterations: 139
currently lose_sum: 96.37624216079712
time_elpased: 1.858
start validation test
0.641597938144
0.615590498494
0.757435422455
0.679186084068
0.641394567574
62.186
batch start
#iterations: 140
currently lose_sum: 96.63516443967819
time_elpased: 1.904
batch start
#iterations: 141
currently lose_sum: 96.19091862440109
time_elpased: 1.844
batch start
#iterations: 142
currently lose_sum: 96.35061800479889
time_elpased: 1.933
batch start
#iterations: 143
currently lose_sum: 96.14405083656311
time_elpased: 1.912
batch start
#iterations: 144
currently lose_sum: 96.4980531334877
time_elpased: 1.844
batch start
#iterations: 145
currently lose_sum: 96.0120747089386
time_elpased: 1.905
batch start
#iterations: 146
currently lose_sum: 95.87855350971222
time_elpased: 1.845
batch start
#iterations: 147
currently lose_sum: 96.12142872810364
time_elpased: 1.845
batch start
#iterations: 148
currently lose_sum: 96.2485693693161
time_elpased: 1.878
batch start
#iterations: 149
currently lose_sum: 96.19079184532166
time_elpased: 1.882
batch start
#iterations: 150
currently lose_sum: 96.12104612588882
time_elpased: 1.902
batch start
#iterations: 151
currently lose_sum: 96.52569234371185
time_elpased: 1.911
batch start
#iterations: 152
currently lose_sum: 96.42859750986099
time_elpased: 1.848
batch start
#iterations: 153
currently lose_sum: 96.03041344881058
time_elpased: 1.851
batch start
#iterations: 154
currently lose_sum: 96.41668099164963
time_elpased: 1.893
batch start
#iterations: 155
currently lose_sum: 96.13235008716583
time_elpased: 1.883
batch start
#iterations: 156
currently lose_sum: 96.46304428577423
time_elpased: 1.945
batch start
#iterations: 157
currently lose_sum: 96.41562277078629
time_elpased: 1.88
batch start
#iterations: 158
currently lose_sum: 96.87769657373428
time_elpased: 1.861
batch start
#iterations: 159
currently lose_sum: 96.34055989980698
time_elpased: 1.868
start validation test
0.638092783505
0.619588360539
0.71874035196
0.665491447901
0.63795119426
62.302
batch start
#iterations: 160
currently lose_sum: 96.06473994255066
time_elpased: 1.89
batch start
#iterations: 161
currently lose_sum: 95.93823325634003
time_elpased: 1.884
batch start
#iterations: 162
currently lose_sum: 96.12434023618698
time_elpased: 1.882
batch start
#iterations: 163
currently lose_sum: 96.04438227415085
time_elpased: 1.916
batch start
#iterations: 164
currently lose_sum: 96.29751336574554
time_elpased: 1.851
batch start
#iterations: 165
currently lose_sum: 96.13025885820389
time_elpased: 1.828
batch start
#iterations: 166
currently lose_sum: 96.05081814527512
time_elpased: 1.934
batch start
#iterations: 167
currently lose_sum: 96.00279879570007
time_elpased: 1.868
batch start
#iterations: 168
currently lose_sum: 96.12961596250534
time_elpased: 1.878
batch start
#iterations: 169
currently lose_sum: 96.10619044303894
time_elpased: 1.856
batch start
#iterations: 170
currently lose_sum: 96.00774097442627
time_elpased: 1.859
batch start
#iterations: 171
currently lose_sum: 96.1507299542427
time_elpased: 1.873
batch start
#iterations: 172
currently lose_sum: 96.6139400601387
time_elpased: 1.87
batch start
#iterations: 173
currently lose_sum: 96.23652935028076
time_elpased: 1.986
batch start
#iterations: 174
currently lose_sum: 95.84660077095032
time_elpased: 1.872
batch start
#iterations: 175
currently lose_sum: 95.8870861530304
time_elpased: 1.872
batch start
#iterations: 176
currently lose_sum: 95.96859586238861
time_elpased: 1.905
batch start
#iterations: 177
currently lose_sum: 95.79514253139496
time_elpased: 1.873
batch start
#iterations: 178
currently lose_sum: 95.64635181427002
time_elpased: 1.878
batch start
#iterations: 179
currently lose_sum: 96.62466299533844
time_elpased: 1.931
start validation test
0.644896907216
0.616532058678
0.769887825461
0.684728387717
0.644677466381
62.292
batch start
#iterations: 180
currently lose_sum: 96.19901490211487
time_elpased: 1.922
batch start
#iterations: 181
currently lose_sum: 96.14681416749954
time_elpased: 1.913
batch start
#iterations: 182
currently lose_sum: 95.94476115703583
time_elpased: 1.92
batch start
#iterations: 183
currently lose_sum: 95.9655727148056
time_elpased: 1.851
batch start
#iterations: 184
currently lose_sum: 96.14989840984344
time_elpased: 1.844
batch start
#iterations: 185
currently lose_sum: 95.89666086435318
time_elpased: 1.879
batch start
#iterations: 186
currently lose_sum: 95.91447019577026
time_elpased: 1.878
batch start
#iterations: 187
currently lose_sum: 96.09242159128189
time_elpased: 1.857
batch start
#iterations: 188
currently lose_sum: 96.03003519773483
time_elpased: 1.886
batch start
#iterations: 189
currently lose_sum: 96.23578190803528
time_elpased: 1.988
batch start
#iterations: 190
currently lose_sum: 96.64791077375412
time_elpased: 1.893
batch start
#iterations: 191
currently lose_sum: 96.15106201171875
time_elpased: 1.872
batch start
#iterations: 192
currently lose_sum: 95.98654639720917
time_elpased: 1.887
batch start
#iterations: 193
currently lose_sum: 96.01707887649536
time_elpased: 1.927
batch start
#iterations: 194
currently lose_sum: 96.33492910861969
time_elpased: 1.88
batch start
#iterations: 195
currently lose_sum: 96.07402849197388
time_elpased: 1.911
batch start
#iterations: 196
currently lose_sum: 96.41263389587402
time_elpased: 1.925
batch start
#iterations: 197
currently lose_sum: 95.95926946401596
time_elpased: 1.907
batch start
#iterations: 198
currently lose_sum: 95.92770206928253
time_elpased: 1.902
batch start
#iterations: 199
currently lose_sum: 95.96866112947464
time_elpased: 1.854
start validation test
0.63881443299
0.621394015409
0.713800555727
0.664399635998
0.638682783285
62.207
batch start
#iterations: 200
currently lose_sum: 95.8848802447319
time_elpased: 1.867
batch start
#iterations: 201
currently lose_sum: 95.99317717552185
time_elpased: 1.87
batch start
#iterations: 202
currently lose_sum: 96.22466081380844
time_elpased: 1.836
batch start
#iterations: 203
currently lose_sum: 96.0831418633461
time_elpased: 1.927
batch start
#iterations: 204
currently lose_sum: 95.72531867027283
time_elpased: 1.901
batch start
#iterations: 205
currently lose_sum: 96.35815823078156
time_elpased: 1.992
batch start
#iterations: 206
currently lose_sum: 96.13588511943817
time_elpased: 1.887
batch start
#iterations: 207
currently lose_sum: 96.10272085666656
time_elpased: 1.902
batch start
#iterations: 208
currently lose_sum: 95.70402884483337
time_elpased: 1.841
batch start
#iterations: 209
currently lose_sum: 96.03291743993759
time_elpased: 1.883
batch start
#iterations: 210
currently lose_sum: 95.68187880516052
time_elpased: 1.92
batch start
#iterations: 211
currently lose_sum: 95.52866142988205
time_elpased: 1.893
batch start
#iterations: 212
currently lose_sum: 96.0195152759552
time_elpased: 1.874
batch start
#iterations: 213
currently lose_sum: 95.92205822467804
time_elpased: 1.83
batch start
#iterations: 214
currently lose_sum: 96.21073317527771
time_elpased: 1.839
batch start
#iterations: 215
currently lose_sum: 96.05443674325943
time_elpased: 1.875
batch start
#iterations: 216
currently lose_sum: 96.28472447395325
time_elpased: 1.848
batch start
#iterations: 217
currently lose_sum: 95.97805488109589
time_elpased: 1.856
batch start
#iterations: 218
currently lose_sum: 96.22969657182693
time_elpased: 1.869
batch start
#iterations: 219
currently lose_sum: 96.43149822950363
time_elpased: 1.862
start validation test
0.64706185567
0.619205848148
0.767109190079
0.685267754539
0.646851094059
62.094
batch start
#iterations: 220
currently lose_sum: 95.73941570520401
time_elpased: 2.014
batch start
#iterations: 221
currently lose_sum: 96.03520691394806
time_elpased: 1.911
batch start
#iterations: 222
currently lose_sum: 95.88300895690918
time_elpased: 1.904
batch start
#iterations: 223
currently lose_sum: 96.05256927013397
time_elpased: 1.913
batch start
#iterations: 224
currently lose_sum: 95.96037137508392
time_elpased: 1.856
batch start
#iterations: 225
currently lose_sum: 96.2109403014183
time_elpased: 1.907
batch start
#iterations: 226
currently lose_sum: 95.80913603305817
time_elpased: 1.809
batch start
#iterations: 227
currently lose_sum: 95.68199306726456
time_elpased: 1.884
batch start
#iterations: 228
currently lose_sum: 95.76446771621704
time_elpased: 1.886
batch start
#iterations: 229
currently lose_sum: 96.030994951725
time_elpased: 1.909
batch start
#iterations: 230
currently lose_sum: 96.0116251707077
time_elpased: 1.835
batch start
#iterations: 231
currently lose_sum: 96.05367702245712
time_elpased: 1.847
batch start
#iterations: 232
currently lose_sum: 95.90522342920303
time_elpased: 1.875
batch start
#iterations: 233
currently lose_sum: 95.96331268548965
time_elpased: 1.877
batch start
#iterations: 234
currently lose_sum: 96.19480335712433
time_elpased: 1.843
batch start
#iterations: 235
currently lose_sum: 95.83070516586304
time_elpased: 1.934
batch start
#iterations: 236
currently lose_sum: 95.74457031488419
time_elpased: 1.881
batch start
#iterations: 237
currently lose_sum: 95.96639144420624
time_elpased: 1.868
batch start
#iterations: 238
currently lose_sum: 96.27406936883926
time_elpased: 1.896
batch start
#iterations: 239
currently lose_sum: 95.99240100383759
time_elpased: 1.859
start validation test
0.64206185567
0.611571578016
0.782134403623
0.686416184971
0.641815936707
62.235
batch start
#iterations: 240
currently lose_sum: 95.74676072597504
time_elpased: 1.871
batch start
#iterations: 241
currently lose_sum: 95.94765520095825
time_elpased: 1.898
batch start
#iterations: 242
currently lose_sum: 95.76706916093826
time_elpased: 1.839
batch start
#iterations: 243
currently lose_sum: 96.57294976711273
time_elpased: 1.869
batch start
#iterations: 244
currently lose_sum: 96.14490449428558
time_elpased: 1.923
batch start
#iterations: 245
currently lose_sum: 96.31362718343735
time_elpased: 1.863
batch start
#iterations: 246
currently lose_sum: 95.3883445262909
time_elpased: 1.892
batch start
#iterations: 247
currently lose_sum: 96.09352195262909
time_elpased: 1.874
batch start
#iterations: 248
currently lose_sum: 95.89646172523499
time_elpased: 1.844
batch start
#iterations: 249
currently lose_sum: 96.02759599685669
time_elpased: 1.847
batch start
#iterations: 250
currently lose_sum: 95.93340909481049
time_elpased: 1.908
batch start
#iterations: 251
currently lose_sum: 96.0628473162651
time_elpased: 1.872
batch start
#iterations: 252
currently lose_sum: 95.79316717386246
time_elpased: 1.865
batch start
#iterations: 253
currently lose_sum: 95.97617328166962
time_elpased: 1.971
batch start
#iterations: 254
currently lose_sum: 95.76454496383667
time_elpased: 1.875
batch start
#iterations: 255
currently lose_sum: 96.01977300643921
time_elpased: 1.901
batch start
#iterations: 256
currently lose_sum: 95.67745912075043
time_elpased: 1.918
batch start
#iterations: 257
currently lose_sum: 95.99035316705704
time_elpased: 1.888
batch start
#iterations: 258
currently lose_sum: 95.71244949102402
time_elpased: 1.858
batch start
#iterations: 259
currently lose_sum: 95.67045551538467
time_elpased: 1.894
start validation test
0.635927835052
0.621010395769
0.700833590614
0.658511821303
0.635813882987
62.380
batch start
#iterations: 260
currently lose_sum: 95.76017487049103
time_elpased: 1.921
batch start
#iterations: 261
currently lose_sum: 95.9795692563057
time_elpased: 1.955
batch start
#iterations: 262
currently lose_sum: 95.56488192081451
time_elpased: 1.96
batch start
#iterations: 263
currently lose_sum: 95.73074144124985
time_elpased: 1.961
batch start
#iterations: 264
currently lose_sum: 95.88957703113556
time_elpased: 1.936
batch start
#iterations: 265
currently lose_sum: 95.9978763461113
time_elpased: 1.88
batch start
#iterations: 266
currently lose_sum: 96.13016647100449
time_elpased: 1.826
batch start
#iterations: 267
currently lose_sum: 95.92090553045273
time_elpased: 1.876
batch start
#iterations: 268
currently lose_sum: 95.82567584514618
time_elpased: 1.916
batch start
#iterations: 269
currently lose_sum: 95.84581995010376
time_elpased: 1.875
batch start
#iterations: 270
currently lose_sum: 95.98644441366196
time_elpased: 1.839
batch start
#iterations: 271
currently lose_sum: 95.95837432146072
time_elpased: 1.871
batch start
#iterations: 272
currently lose_sum: 95.8725129365921
time_elpased: 1.901
batch start
#iterations: 273
currently lose_sum: 95.37557083368301
time_elpased: 1.901
batch start
#iterations: 274
currently lose_sum: 95.94359582662582
time_elpased: 1.878
batch start
#iterations: 275
currently lose_sum: 95.93345218896866
time_elpased: 1.861
batch start
#iterations: 276
currently lose_sum: 95.50089263916016
time_elpased: 1.839
batch start
#iterations: 277
currently lose_sum: 95.9383197426796
time_elpased: 1.859
batch start
#iterations: 278
currently lose_sum: 96.02233237028122
time_elpased: 1.877
batch start
#iterations: 279
currently lose_sum: 95.77473986148834
time_elpased: 1.855
start validation test
0.645412371134
0.622054016859
0.7442626325
0.677692920395
0.645238824254
62.170
batch start
#iterations: 280
currently lose_sum: 95.7539712190628
time_elpased: 1.901
batch start
#iterations: 281
currently lose_sum: 95.7932785153389
time_elpased: 1.856
batch start
#iterations: 282
currently lose_sum: 95.73873698711395
time_elpased: 1.885
batch start
#iterations: 283
currently lose_sum: 96.1288030743599
time_elpased: 1.861
batch start
#iterations: 284
currently lose_sum: 95.64121925830841
time_elpased: 1.963
batch start
#iterations: 285
currently lose_sum: 95.84065115451813
time_elpased: 1.89
batch start
#iterations: 286
currently lose_sum: 95.38177478313446
time_elpased: 1.957
batch start
#iterations: 287
currently lose_sum: 95.72002679109573
time_elpased: 1.917
batch start
#iterations: 288
currently lose_sum: 95.58901619911194
time_elpased: 1.889
batch start
#iterations: 289
currently lose_sum: 96.14847093820572
time_elpased: 1.907
batch start
#iterations: 290
currently lose_sum: 95.85539788007736
time_elpased: 1.883
batch start
#iterations: 291
currently lose_sum: 95.80900144577026
time_elpased: 1.87
batch start
#iterations: 292
currently lose_sum: 95.9771620631218
time_elpased: 1.893
batch start
#iterations: 293
currently lose_sum: 95.86192315816879
time_elpased: 1.882
batch start
#iterations: 294
currently lose_sum: 95.92692202329636
time_elpased: 1.888
batch start
#iterations: 295
currently lose_sum: 96.02159714698792
time_elpased: 1.84
batch start
#iterations: 296
currently lose_sum: 95.85969179868698
time_elpased: 1.844
batch start
#iterations: 297
currently lose_sum: 95.77124392986298
time_elpased: 1.947
batch start
#iterations: 298
currently lose_sum: 96.11495262384415
time_elpased: 1.9
batch start
#iterations: 299
currently lose_sum: 95.83002555370331
time_elpased: 1.889
start validation test
0.637319587629
0.614975555365
0.737882062365
0.670845808383
0.637143034694
62.175
batch start
#iterations: 300
currently lose_sum: 95.35497546195984
time_elpased: 1.91
batch start
#iterations: 301
currently lose_sum: 95.66021710634232
time_elpased: 1.924
batch start
#iterations: 302
currently lose_sum: 95.62401217222214
time_elpased: 1.924
batch start
#iterations: 303
currently lose_sum: 95.7742308974266
time_elpased: 1.879
batch start
#iterations: 304
currently lose_sum: 95.52712279558182
time_elpased: 1.868
batch start
#iterations: 305
currently lose_sum: 95.92093753814697
time_elpased: 1.908
batch start
#iterations: 306
currently lose_sum: 96.01522868871689
time_elpased: 1.952
batch start
#iterations: 307
currently lose_sum: 95.63633263111115
time_elpased: 1.911
batch start
#iterations: 308
currently lose_sum: 95.81074041128159
time_elpased: 1.836
batch start
#iterations: 309
currently lose_sum: 95.973552942276
time_elpased: 1.85
batch start
#iterations: 310
currently lose_sum: 95.666119992733
time_elpased: 1.871
batch start
#iterations: 311
currently lose_sum: 95.74698179960251
time_elpased: 1.858
batch start
#iterations: 312
currently lose_sum: 95.52509313821793
time_elpased: 1.866
batch start
#iterations: 313
currently lose_sum: 95.96854192018509
time_elpased: 1.893
batch start
#iterations: 314
currently lose_sum: 95.6149433851242
time_elpased: 1.864
batch start
#iterations: 315
currently lose_sum: 95.86016422510147
time_elpased: 1.86
batch start
#iterations: 316
currently lose_sum: 95.79433923959732
time_elpased: 1.856
batch start
#iterations: 317
currently lose_sum: 95.79098784923553
time_elpased: 1.876
batch start
#iterations: 318
currently lose_sum: 95.62756663560867
time_elpased: 1.847
batch start
#iterations: 319
currently lose_sum: 96.16451841592789
time_elpased: 1.823
start validation test
0.642371134021
0.614202350621
0.769064526088
0.682964723085
0.64214870423
62.224
batch start
#iterations: 320
currently lose_sum: 96.18392926454544
time_elpased: 1.926
batch start
#iterations: 321
currently lose_sum: 95.91818541288376
time_elpased: 1.893
batch start
#iterations: 322
currently lose_sum: 95.69069212675095
time_elpased: 1.857
batch start
#iterations: 323
currently lose_sum: 96.23374092578888
time_elpased: 1.855
batch start
#iterations: 324
currently lose_sum: 95.93339306116104
time_elpased: 1.845
batch start
#iterations: 325
currently lose_sum: 95.75853532552719
time_elpased: 1.862
batch start
#iterations: 326
currently lose_sum: 96.26096349954605
time_elpased: 1.925
batch start
#iterations: 327
currently lose_sum: 95.89067089557648
time_elpased: 1.893
batch start
#iterations: 328
currently lose_sum: 95.47544997930527
time_elpased: 1.833
batch start
#iterations: 329
currently lose_sum: 95.71693563461304
time_elpased: 1.925
batch start
#iterations: 330
currently lose_sum: 95.69328397512436
time_elpased: 1.863
batch start
#iterations: 331
currently lose_sum: 95.52462857961655
time_elpased: 1.951
batch start
#iterations: 332
currently lose_sum: 95.62064623832703
time_elpased: 1.892
batch start
#iterations: 333
currently lose_sum: 95.50056880712509
time_elpased: 1.953
batch start
#iterations: 334
currently lose_sum: 95.6029782295227
time_elpased: 1.863
batch start
#iterations: 335
currently lose_sum: 95.63284659385681
time_elpased: 1.875
batch start
#iterations: 336
currently lose_sum: 95.68989789485931
time_elpased: 1.85
batch start
#iterations: 337
currently lose_sum: 95.50969099998474
time_elpased: 1.891
batch start
#iterations: 338
currently lose_sum: 95.81700330972672
time_elpased: 1.941
batch start
#iterations: 339
currently lose_sum: 95.9075493812561
time_elpased: 1.884
start validation test
0.648041237113
0.618333742934
0.776782957703
0.688560481664
0.647815211166
61.974
batch start
#iterations: 340
currently lose_sum: 95.74808460474014
time_elpased: 1.857
batch start
#iterations: 341
currently lose_sum: 96.01113533973694
time_elpased: 1.869
batch start
#iterations: 342
currently lose_sum: 95.84438520669937
time_elpased: 1.826
batch start
#iterations: 343
currently lose_sum: 95.58032357692719
time_elpased: 1.87
batch start
#iterations: 344
currently lose_sum: 95.89729112386703
time_elpased: 1.915
batch start
#iterations: 345
currently lose_sum: 95.57637935876846
time_elpased: 1.873
batch start
#iterations: 346
currently lose_sum: 96.14590042829514
time_elpased: 1.864
batch start
#iterations: 347
currently lose_sum: 95.70126301050186
time_elpased: 1.887
batch start
#iterations: 348
currently lose_sum: 95.95023638010025
time_elpased: 1.887
batch start
#iterations: 349
currently lose_sum: 95.83831751346588
time_elpased: 1.841
batch start
#iterations: 350
currently lose_sum: 95.54344713687897
time_elpased: 1.937
batch start
#iterations: 351
currently lose_sum: 95.87571239471436
time_elpased: 1.879
batch start
#iterations: 352
currently lose_sum: 95.61314821243286
time_elpased: 1.869
batch start
#iterations: 353
currently lose_sum: 96.08428621292114
time_elpased: 1.891
batch start
#iterations: 354
currently lose_sum: 96.19420063495636
time_elpased: 1.909
batch start
#iterations: 355
currently lose_sum: 95.86102306842804
time_elpased: 1.896
batch start
#iterations: 356
currently lose_sum: 95.904367685318
time_elpased: 1.949
batch start
#iterations: 357
currently lose_sum: 95.93180084228516
time_elpased: 1.939
batch start
#iterations: 358
currently lose_sum: 96.0053962469101
time_elpased: 1.891
batch start
#iterations: 359
currently lose_sum: 95.80962640047073
time_elpased: 1.88
start validation test
0.647113402062
0.61521791476
0.788823711022
0.691287878788
0.646864607757
62.061
batch start
#iterations: 360
currently lose_sum: 95.94684809446335
time_elpased: 1.901
batch start
#iterations: 361
currently lose_sum: 96.14222502708435
time_elpased: 1.929
batch start
#iterations: 362
currently lose_sum: 95.35508561134338
time_elpased: 1.873
batch start
#iterations: 363
currently lose_sum: 95.97048807144165
time_elpased: 1.853
batch start
#iterations: 364
currently lose_sum: 96.13782799243927
time_elpased: 1.89
batch start
#iterations: 365
currently lose_sum: 95.77896922826767
time_elpased: 1.845
batch start
#iterations: 366
currently lose_sum: 95.78137916326523
time_elpased: 1.864
batch start
#iterations: 367
currently lose_sum: 95.30578565597534
time_elpased: 1.883
batch start
#iterations: 368
currently lose_sum: 95.88762891292572
time_elpased: 1.918
batch start
#iterations: 369
currently lose_sum: 95.88689106702805
time_elpased: 1.907
batch start
#iterations: 370
currently lose_sum: 95.48156476020813
time_elpased: 1.894
batch start
#iterations: 371
currently lose_sum: 95.73027735948563
time_elpased: 1.835
batch start
#iterations: 372
currently lose_sum: 96.01035910844803
time_elpased: 1.838
batch start
#iterations: 373
currently lose_sum: 95.53469282388687
time_elpased: 1.902
batch start
#iterations: 374
currently lose_sum: 95.70975852012634
time_elpased: 1.83
batch start
#iterations: 375
currently lose_sum: 95.38271397352219
time_elpased: 1.943
batch start
#iterations: 376
currently lose_sum: 95.45502293109894
time_elpased: 1.909
batch start
#iterations: 377
currently lose_sum: 95.74318104982376
time_elpased: 1.887
batch start
#iterations: 378
currently lose_sum: 95.61981803178787
time_elpased: 1.827
batch start
#iterations: 379
currently lose_sum: 95.71301102638245
time_elpased: 1.858
start validation test
0.643711340206
0.624500665779
0.723988885458
0.670574778381
0.643570400594
62.252
batch start
#iterations: 380
currently lose_sum: 95.73107719421387
time_elpased: 1.903
batch start
#iterations: 381
currently lose_sum: 95.61393237113953
time_elpased: 1.938
batch start
#iterations: 382
currently lose_sum: 95.74917382001877
time_elpased: 1.931
batch start
#iterations: 383
currently lose_sum: 95.51453566551208
time_elpased: 1.89
batch start
#iterations: 384
currently lose_sum: 95.79207926988602
time_elpased: 1.863
batch start
#iterations: 385
currently lose_sum: 95.58056497573853
time_elpased: 1.865
batch start
#iterations: 386
currently lose_sum: 95.55829614400864
time_elpased: 1.907
batch start
#iterations: 387
currently lose_sum: 95.84051007032394
time_elpased: 1.921
batch start
#iterations: 388
currently lose_sum: 95.95779716968536
time_elpased: 1.917
batch start
#iterations: 389
currently lose_sum: 95.7845367193222
time_elpased: 1.934
batch start
#iterations: 390
currently lose_sum: 95.53519642353058
time_elpased: 1.855
batch start
#iterations: 391
currently lose_sum: 95.53515362739563
time_elpased: 1.893
batch start
#iterations: 392
currently lose_sum: 95.7090824842453
time_elpased: 1.875
batch start
#iterations: 393
currently lose_sum: 95.63598841428757
time_elpased: 1.87
batch start
#iterations: 394
currently lose_sum: 95.78087943792343
time_elpased: 1.898
batch start
#iterations: 395
currently lose_sum: 95.97742521762848
time_elpased: 1.849
batch start
#iterations: 396
currently lose_sum: 95.51863491535187
time_elpased: 1.845
batch start
#iterations: 397
currently lose_sum: 95.39457088708878
time_elpased: 1.816
batch start
#iterations: 398
currently lose_sum: 95.76431268453598
time_elpased: 1.828
batch start
#iterations: 399
currently lose_sum: 95.63793057203293
time_elpased: 1.815
start validation test
0.645567010309
0.619259508018
0.7590820212
0.682078786758
0.645367717199
62.237
acc: 0.642
pre: 0.613
rec: 0.771
F1: 0.683
auc: 0.641
