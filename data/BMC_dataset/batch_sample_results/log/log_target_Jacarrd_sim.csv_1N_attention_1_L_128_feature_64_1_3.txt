start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.35489296913147
time_elpased: 2.341
batch start
#iterations: 1
currently lose_sum: 100.0353901386261
time_elpased: 2.261
batch start
#iterations: 2
currently lose_sum: 99.63239401578903
time_elpased: 2.208
batch start
#iterations: 3
currently lose_sum: 99.40842664241791
time_elpased: 2.206
batch start
#iterations: 4
currently lose_sum: 99.03987830877304
time_elpased: 2.26
batch start
#iterations: 5
currently lose_sum: 99.05068725347519
time_elpased: 2.315
batch start
#iterations: 6
currently lose_sum: 98.6270079612732
time_elpased: 2.423
batch start
#iterations: 7
currently lose_sum: 98.44091314077377
time_elpased: 2.445
batch start
#iterations: 8
currently lose_sum: 98.14366096258163
time_elpased: 2.387
batch start
#iterations: 9
currently lose_sum: 98.13355511426926
time_elpased: 2.379
batch start
#iterations: 10
currently lose_sum: 97.91257655620575
time_elpased: 2.31
batch start
#iterations: 11
currently lose_sum: 97.52208691835403
time_elpased: 2.27
batch start
#iterations: 12
currently lose_sum: 97.85037016868591
time_elpased: 2.252
batch start
#iterations: 13
currently lose_sum: 97.96484106779099
time_elpased: 2.265
batch start
#iterations: 14
currently lose_sum: 97.45686286687851
time_elpased: 2.389
batch start
#iterations: 15
currently lose_sum: 97.61553925275803
time_elpased: 2.37
batch start
#iterations: 16
currently lose_sum: 97.5217068195343
time_elpased: 2.356
batch start
#iterations: 17
currently lose_sum: 97.4694253206253
time_elpased: 2.42
batch start
#iterations: 18
currently lose_sum: 97.42618346214294
time_elpased: 2.479
batch start
#iterations: 19
currently lose_sum: 97.07808995246887
time_elpased: 2.44
start validation test
0.641597938144
0.630367085024
0.687525730753
0.657706887215
0.641522055794
62.849
batch start
#iterations: 20
currently lose_sum: 96.97231537103653
time_elpased: 2.447
batch start
#iterations: 21
currently lose_sum: 97.04537695646286
time_elpased: 2.409
batch start
#iterations: 22
currently lose_sum: 97.00327658653259
time_elpased: 2.267
batch start
#iterations: 23
currently lose_sum: 96.89723175764084
time_elpased: 2.319
batch start
#iterations: 24
currently lose_sum: 96.87990093231201
time_elpased: 2.253
batch start
#iterations: 25
currently lose_sum: 96.89956337213516
time_elpased: 2.363
batch start
#iterations: 26
currently lose_sum: 96.80032759904861
time_elpased: 2.434
batch start
#iterations: 27
currently lose_sum: 96.76755678653717
time_elpased: 2.386
batch start
#iterations: 28
currently lose_sum: 96.6657378077507
time_elpased: 2.382
batch start
#iterations: 29
currently lose_sum: 97.13912934064865
time_elpased: 2.392
batch start
#iterations: 30
currently lose_sum: 96.56780368089676
time_elpased: 2.422
batch start
#iterations: 31
currently lose_sum: 96.58849984407425
time_elpased: 2.369
batch start
#iterations: 32
currently lose_sum: 96.61378210783005
time_elpased: 2.385
batch start
#iterations: 33
currently lose_sum: 96.54539972543716
time_elpased: 2.256
batch start
#iterations: 34
currently lose_sum: 96.61922121047974
time_elpased: 2.28
batch start
#iterations: 35
currently lose_sum: 96.80362927913666
time_elpased: 2.379
batch start
#iterations: 36
currently lose_sum: 96.44305688142776
time_elpased: 2.472
batch start
#iterations: 37
currently lose_sum: 96.5525751709938
time_elpased: 2.421
batch start
#iterations: 38
currently lose_sum: 96.24354857206345
time_elpased: 2.441
batch start
#iterations: 39
currently lose_sum: 96.49725753068924
time_elpased: 2.443
start validation test
0.636649484536
0.633416708354
0.651605599012
0.642382426057
0.636624773897
62.758
batch start
#iterations: 40
currently lose_sum: 96.54963517189026
time_elpased: 2.535
batch start
#iterations: 41
currently lose_sum: 96.6734631061554
time_elpased: 2.541
batch start
#iterations: 42
currently lose_sum: 96.38681429624557
time_elpased: 2.523
batch start
#iterations: 43
currently lose_sum: 96.29245948791504
time_elpased: 2.424
batch start
#iterations: 44
currently lose_sum: 96.43577778339386
time_elpased: 2.334
batch start
#iterations: 45
currently lose_sum: 96.37145805358887
time_elpased: 2.45
batch start
#iterations: 46
currently lose_sum: 96.24112468957901
time_elpased: 2.48
batch start
#iterations: 47
currently lose_sum: 96.19118863344193
time_elpased: 2.36
batch start
#iterations: 48
currently lose_sum: 96.05545002222061
time_elpased: 2.405
batch start
#iterations: 49
currently lose_sum: 96.2731339931488
time_elpased: 2.436
batch start
#iterations: 50
currently lose_sum: 96.44565844535828
time_elpased: 2.377
batch start
#iterations: 51
currently lose_sum: 96.01692593097687
time_elpased: 2.324
batch start
#iterations: 52
currently lose_sum: 96.33875942230225
time_elpased: 2.299
batch start
#iterations: 53
currently lose_sum: 96.20182865858078
time_elpased: 2.342
batch start
#iterations: 54
currently lose_sum: 96.28779298067093
time_elpased: 2.237
batch start
#iterations: 55
currently lose_sum: 96.1680616736412
time_elpased: 2.235
batch start
#iterations: 56
currently lose_sum: 96.23697596788406
time_elpased: 2.34
batch start
#iterations: 57
currently lose_sum: 96.30186450481415
time_elpased: 2.35
batch start
#iterations: 58
currently lose_sum: 96.25101852416992
time_elpased: 2.346
batch start
#iterations: 59
currently lose_sum: 96.1748052239418
time_elpased: 2.365
start validation test
0.643350515464
0.637363716727
0.667867435159
0.652259134543
0.643310008368
62.447
batch start
#iterations: 60
currently lose_sum: 95.91446709632874
time_elpased: 2.309
batch start
#iterations: 61
currently lose_sum: 96.42120087146759
time_elpased: 2.321
batch start
#iterations: 62
currently lose_sum: 96.40275049209595
time_elpased: 2.327
batch start
#iterations: 63
currently lose_sum: 95.91558521986008
time_elpased: 2.355
batch start
#iterations: 64
currently lose_sum: 96.15958255529404
time_elpased: 2.282
batch start
#iterations: 65
currently lose_sum: 96.17718923091888
time_elpased: 2.319
batch start
#iterations: 66
currently lose_sum: 96.23320227861404
time_elpased: 2.259
batch start
#iterations: 67
currently lose_sum: 96.16714495420456
time_elpased: 2.323
batch start
#iterations: 68
currently lose_sum: 95.89573466777802
time_elpased: 2.383
batch start
#iterations: 69
currently lose_sum: 95.91612589359283
time_elpased: 2.38
batch start
#iterations: 70
currently lose_sum: 96.14729803800583
time_elpased: 2.441
batch start
#iterations: 71
currently lose_sum: 96.11342149972916
time_elpased: 2.43
batch start
#iterations: 72
currently lose_sum: 95.83702182769775
time_elpased: 2.37
batch start
#iterations: 73
currently lose_sum: 95.8562942147255
time_elpased: 2.383
batch start
#iterations: 74
currently lose_sum: 95.78857523202896
time_elpased: 2.41
batch start
#iterations: 75
currently lose_sum: 95.94934225082397
time_elpased: 2.599
batch start
#iterations: 76
currently lose_sum: 95.89090520143509
time_elpased: 2.56
batch start
#iterations: 77
currently lose_sum: 95.97634035348892
time_elpased: 2.481
batch start
#iterations: 78
currently lose_sum: 96.15334868431091
time_elpased: 2.503
batch start
#iterations: 79
currently lose_sum: 95.93734014034271
time_elpased: 2.568
start validation test
0.653711340206
0.638233124308
0.712330177028
0.673249027237
0.653614489588
62.072
batch start
#iterations: 80
currently lose_sum: 95.98581314086914
time_elpased: 2.627
batch start
#iterations: 81
currently lose_sum: 95.7041449546814
time_elpased: 2.514
batch start
#iterations: 82
currently lose_sum: 96.14954078197479
time_elpased: 2.554
batch start
#iterations: 83
currently lose_sum: 95.66872185468674
time_elpased: 2.491
batch start
#iterations: 84
currently lose_sum: 95.62374693155289
time_elpased: 2.57
batch start
#iterations: 85
currently lose_sum: 95.90035396814346
time_elpased: 2.532
batch start
#iterations: 86
currently lose_sum: 95.63572704792023
time_elpased: 2.492
batch start
#iterations: 87
currently lose_sum: 95.55660504102707
time_elpased: 2.455
batch start
#iterations: 88
currently lose_sum: 95.71265053749084
time_elpased: 2.504
batch start
#iterations: 89
currently lose_sum: 96.11137974262238
time_elpased: 2.529
batch start
#iterations: 90
currently lose_sum: 95.81300282478333
time_elpased: 2.437
batch start
#iterations: 91
currently lose_sum: 95.83907216787338
time_elpased: 2.567
batch start
#iterations: 92
currently lose_sum: 95.71283757686615
time_elpased: 2.501
batch start
#iterations: 93
currently lose_sum: 95.445159137249
time_elpased: 2.548
batch start
#iterations: 94
currently lose_sum: 95.5862085223198
time_elpased: 2.587
batch start
#iterations: 95
currently lose_sum: 95.73731487989426
time_elpased: 2.455
batch start
#iterations: 96
currently lose_sum: 95.84057152271271
time_elpased: 2.539
batch start
#iterations: 97
currently lose_sum: 95.52839106321335
time_elpased: 2.525
batch start
#iterations: 98
currently lose_sum: 95.70949900150299
time_elpased: 2.582
batch start
#iterations: 99
currently lose_sum: 95.8855008482933
time_elpased: 2.606
start validation test
0.651288659794
0.641209685137
0.689584191025
0.664517728738
0.651225387541
62.083
batch start
#iterations: 100
currently lose_sum: 95.90304613113403
time_elpased: 2.489
batch start
#iterations: 101
currently lose_sum: 95.81467133760452
time_elpased: 2.586
batch start
#iterations: 102
currently lose_sum: 96.11826831102371
time_elpased: 2.518
batch start
#iterations: 103
currently lose_sum: 95.73329567909241
time_elpased: 2.481
batch start
#iterations: 104
currently lose_sum: 95.59477210044861
time_elpased: 2.556
batch start
#iterations: 105
currently lose_sum: 95.76458138227463
time_elpased: 2.578
batch start
#iterations: 106
currently lose_sum: 95.59373033046722
time_elpased: 2.487
batch start
#iterations: 107
currently lose_sum: 95.75400739908218
time_elpased: 2.556
batch start
#iterations: 108
currently lose_sum: 95.42083954811096
time_elpased: 2.546
batch start
#iterations: 109
currently lose_sum: 95.91255730390549
time_elpased: 2.473
batch start
#iterations: 110
currently lose_sum: 95.59087097644806
time_elpased: 2.581
batch start
#iterations: 111
currently lose_sum: 95.73853522539139
time_elpased: 2.662
batch start
#iterations: 112
currently lose_sum: 95.52872014045715
time_elpased: 2.532
batch start
#iterations: 113
currently lose_sum: 95.6834152340889
time_elpased: 2.427
batch start
#iterations: 114
currently lose_sum: 95.89575463533401
time_elpased: 2.595
batch start
#iterations: 115
currently lose_sum: 95.68539553880692
time_elpased: 2.516
batch start
#iterations: 116
currently lose_sum: 95.72450017929077
time_elpased: 2.358
batch start
#iterations: 117
currently lose_sum: 95.46848618984222
time_elpased: 2.469
batch start
#iterations: 118
currently lose_sum: 95.54285877943039
time_elpased: 2.48
batch start
#iterations: 119
currently lose_sum: 95.8690133690834
time_elpased: 2.453
start validation test
0.647319587629
0.634349289454
0.69833264718
0.664805016657
0.64723530335
62.044
batch start
#iterations: 120
currently lose_sum: 95.17328089475632
time_elpased: 2.39
batch start
#iterations: 121
currently lose_sum: 95.69578194618225
time_elpased: 2.479
batch start
#iterations: 122
currently lose_sum: 95.31887698173523
time_elpased: 2.486
batch start
#iterations: 123
currently lose_sum: 95.91975092887878
time_elpased: 2.465
batch start
#iterations: 124
currently lose_sum: 95.83643943071365
time_elpased: 2.513
batch start
#iterations: 125
currently lose_sum: 95.18955773115158
time_elpased: 2.475
batch start
#iterations: 126
currently lose_sum: 95.47079002857208
time_elpased: 2.528
batch start
#iterations: 127
currently lose_sum: 95.66801488399506
time_elpased: 2.586
batch start
#iterations: 128
currently lose_sum: 95.44457274675369
time_elpased: 2.514
batch start
#iterations: 129
currently lose_sum: 95.51618909835815
time_elpased: 2.477
batch start
#iterations: 130
currently lose_sum: 95.47779041528702
time_elpased: 2.54
batch start
#iterations: 131
currently lose_sum: 95.55818176269531
time_elpased: 2.577
batch start
#iterations: 132
currently lose_sum: 95.35082787275314
time_elpased: 2.483
batch start
#iterations: 133
currently lose_sum: 95.63050609827042
time_elpased: 2.638
batch start
#iterations: 134
currently lose_sum: 95.65721917152405
time_elpased: 2.593
batch start
#iterations: 135
currently lose_sum: 95.54933911561966
time_elpased: 2.593
batch start
#iterations: 136
currently lose_sum: 95.51599264144897
time_elpased: 2.558
batch start
#iterations: 137
currently lose_sum: 95.52910727262497
time_elpased: 2.573
batch start
#iterations: 138
currently lose_sum: 95.480189204216
time_elpased: 2.533
batch start
#iterations: 139
currently lose_sum: 95.18287742137909
time_elpased: 2.5
start validation test
0.651494845361
0.640460119783
0.693392342528
0.665875957499
0.651425621904
61.925
batch start
#iterations: 140
currently lose_sum: 95.34148275852203
time_elpased: 2.624
batch start
#iterations: 141
currently lose_sum: 95.37933266162872
time_elpased: 2.522
batch start
#iterations: 142
currently lose_sum: 95.66891062259674
time_elpased: 2.475
batch start
#iterations: 143
currently lose_sum: 95.50153082609177
time_elpased: 2.545
batch start
#iterations: 144
currently lose_sum: 95.63444596529007
time_elpased: 2.439
batch start
#iterations: 145
currently lose_sum: 94.9925497174263
time_elpased: 2.366
batch start
#iterations: 146
currently lose_sum: 95.45377039909363
time_elpased: 2.352
batch start
#iterations: 147
currently lose_sum: 95.66513901948929
time_elpased: 2.455
batch start
#iterations: 148
currently lose_sum: 95.3106010556221
time_elpased: 2.543
batch start
#iterations: 149
currently lose_sum: 95.46362572908401
time_elpased: 2.288
batch start
#iterations: 150
currently lose_sum: 95.65453308820724
time_elpased: 2.249
batch start
#iterations: 151
currently lose_sum: 95.57641005516052
time_elpased: 2.203
batch start
#iterations: 152
currently lose_sum: 95.48541247844696
time_elpased: 2.02
batch start
#iterations: 153
currently lose_sum: 95.95719718933105
time_elpased: 2.216
batch start
#iterations: 154
currently lose_sum: 95.55890345573425
time_elpased: 2.278
batch start
#iterations: 155
currently lose_sum: 95.40279030799866
time_elpased: 2.293
batch start
#iterations: 156
currently lose_sum: 95.42196130752563
time_elpased: 2.246
batch start
#iterations: 157
currently lose_sum: 95.20241326093674
time_elpased: 2.228
batch start
#iterations: 158
currently lose_sum: 95.73922526836395
time_elpased: 2.235
batch start
#iterations: 159
currently lose_sum: 95.21653324365616
time_elpased: 2.219
start validation test
0.652268041237
0.63765294772
0.708007410457
0.670991026141
0.652175948103
61.966
batch start
#iterations: 160
currently lose_sum: 95.562340259552
time_elpased: 2.19
batch start
#iterations: 161
currently lose_sum: 95.42474526166916
time_elpased: 2.146
batch start
#iterations: 162
currently lose_sum: 95.60403126478195
time_elpased: 2.283
batch start
#iterations: 163
currently lose_sum: 95.21305274963379
time_elpased: 2.248
batch start
#iterations: 164
currently lose_sum: 95.77432614564896
time_elpased: 2.245
batch start
#iterations: 165
currently lose_sum: 95.32896721363068
time_elpased: 2.271
batch start
#iterations: 166
currently lose_sum: 95.02533316612244
time_elpased: 2.278
batch start
#iterations: 167
currently lose_sum: 95.19333136081696
time_elpased: 2.237
batch start
#iterations: 168
currently lose_sum: 95.6087720990181
time_elpased: 2.213
batch start
#iterations: 169
currently lose_sum: 95.2694354057312
time_elpased: 2.186
batch start
#iterations: 170
currently lose_sum: 95.17428523302078
time_elpased: 2.244
batch start
#iterations: 171
currently lose_sum: 95.41204714775085
time_elpased: 2.249
batch start
#iterations: 172
currently lose_sum: 95.32627636194229
time_elpased: 2.246
batch start
#iterations: 173
currently lose_sum: 95.02868431806564
time_elpased: 2.35
batch start
#iterations: 174
currently lose_sum: 95.02655041217804
time_elpased: 2.268
batch start
#iterations: 175
currently lose_sum: 95.09940099716187
time_elpased: 2.252
batch start
#iterations: 176
currently lose_sum: 95.60631704330444
time_elpased: 2.226
batch start
#iterations: 177
currently lose_sum: 95.31523954868317
time_elpased: 2.279
batch start
#iterations: 178
currently lose_sum: 95.1905722618103
time_elpased: 2.18
batch start
#iterations: 179
currently lose_sum: 95.19349950551987
time_elpased: 2.281
start validation test
0.654793814433
0.636916099773
0.7227254014
0.677112964659
0.654681577197
61.837
batch start
#iterations: 180
currently lose_sum: 95.17021572589874
time_elpased: 2.317
batch start
#iterations: 181
currently lose_sum: 95.62533682584763
time_elpased: 2.237
batch start
#iterations: 182
currently lose_sum: 95.41578263044357
time_elpased: 2.292
batch start
#iterations: 183
currently lose_sum: 95.19642382860184
time_elpased: 2.172
batch start
#iterations: 184
currently lose_sum: 95.6618122458458
time_elpased: 2.248
batch start
#iterations: 185
currently lose_sum: 95.14121788740158
time_elpased: 2.251
batch start
#iterations: 186
currently lose_sum: 95.1219722032547
time_elpased: 2.247
batch start
#iterations: 187
currently lose_sum: 95.09441143274307
time_elpased: 2.228
batch start
#iterations: 188
currently lose_sum: 95.57104742527008
time_elpased: 2.222
batch start
#iterations: 189
currently lose_sum: 95.4168553352356
time_elpased: 2.265
batch start
#iterations: 190
currently lose_sum: 95.5798544883728
time_elpased: 2.254
batch start
#iterations: 191
currently lose_sum: 95.30740576982498
time_elpased: 2.184
batch start
#iterations: 192
currently lose_sum: 95.24563187360764
time_elpased: 2.254
batch start
#iterations: 193
currently lose_sum: 95.16898882389069
time_elpased: 2.238
batch start
#iterations: 194
currently lose_sum: 95.47029024362564
time_elpased: 2.267
batch start
#iterations: 195
currently lose_sum: 94.89312392473221
time_elpased: 2.143
batch start
#iterations: 196
currently lose_sum: 95.14699518680573
time_elpased: 2.29
batch start
#iterations: 197
currently lose_sum: 94.87746465206146
time_elpased: 2.273
batch start
#iterations: 198
currently lose_sum: 94.80403703451157
time_elpased: 2.167
batch start
#iterations: 199
currently lose_sum: 95.18438404798508
time_elpased: 2.227
start validation test
0.659793814433
0.643726937269
0.718196788802
0.678925861062
0.659697320465
61.797
batch start
#iterations: 200
currently lose_sum: 95.23772966861725
time_elpased: 2.304
batch start
#iterations: 201
currently lose_sum: 95.12543570995331
time_elpased: 2.233
batch start
#iterations: 202
currently lose_sum: 95.09157192707062
time_elpased: 2.198
batch start
#iterations: 203
currently lose_sum: 95.5653355717659
time_elpased: 2.158
batch start
#iterations: 204
currently lose_sum: 95.7438388466835
time_elpased: 2.165
batch start
#iterations: 205
currently lose_sum: 95.35149246454239
time_elpased: 2.191
batch start
#iterations: 206
currently lose_sum: 94.95318704843521
time_elpased: 2.126
batch start
#iterations: 207
currently lose_sum: 95.39468663930893
time_elpased: 2.263
batch start
#iterations: 208
currently lose_sum: 94.99746024608612
time_elpased: 2.295
batch start
#iterations: 209
currently lose_sum: 94.85202819108963
time_elpased: 2.248
batch start
#iterations: 210
currently lose_sum: 94.99433010816574
time_elpased: 2.225
batch start
#iterations: 211
currently lose_sum: 94.76144260168076
time_elpased: 2.242
batch start
#iterations: 212
currently lose_sum: 95.33239156007767
time_elpased: 2.18
batch start
#iterations: 213
currently lose_sum: 95.29709720611572
time_elpased: 2.191
batch start
#iterations: 214
currently lose_sum: 94.83917105197906
time_elpased: 2.169
batch start
#iterations: 215
currently lose_sum: 94.85155856609344
time_elpased: 2.252
batch start
#iterations: 216
currently lose_sum: 95.02647393941879
time_elpased: 2.239
batch start
#iterations: 217
currently lose_sum: 95.1962782740593
time_elpased: 2.221
batch start
#iterations: 218
currently lose_sum: 95.31501024961472
time_elpased: 2.206
batch start
#iterations: 219
currently lose_sum: 95.06537127494812
time_elpased: 2.222
start validation test
0.658556701031
0.638159070599
0.734973240016
0.683153161772
0.658430444874
61.646
batch start
#iterations: 220
currently lose_sum: 95.24688398838043
time_elpased: 2.215
batch start
#iterations: 221
currently lose_sum: 95.18777537345886
time_elpased: 2.168
batch start
#iterations: 222
currently lose_sum: 94.96420073509216
time_elpased: 2.168
batch start
#iterations: 223
currently lose_sum: 95.09848868846893
time_elpased: 2.262
batch start
#iterations: 224
currently lose_sum: 94.81148284673691
time_elpased: 2.215
batch start
#iterations: 225
currently lose_sum: 94.85281217098236
time_elpased: 2.2
batch start
#iterations: 226
currently lose_sum: 95.1205039024353
time_elpased: 2.248
batch start
#iterations: 227
currently lose_sum: 95.17490965127945
time_elpased: 2.23
batch start
#iterations: 228
currently lose_sum: 94.78931140899658
time_elpased: 2.232
batch start
#iterations: 229
currently lose_sum: 94.89380151033401
time_elpased: 2.219
batch start
#iterations: 230
currently lose_sum: 95.42666274309158
time_elpased: 2.092
batch start
#iterations: 231
currently lose_sum: 95.28913462162018
time_elpased: 2.292
batch start
#iterations: 232
currently lose_sum: 95.45331227779388
time_elpased: 2.246
batch start
#iterations: 233
currently lose_sum: 95.44501942396164
time_elpased: 2.186
batch start
#iterations: 234
currently lose_sum: 95.33640855550766
time_elpased: 2.176
batch start
#iterations: 235
currently lose_sum: 95.15481042861938
time_elpased: 2.237
batch start
#iterations: 236
currently lose_sum: 95.17604410648346
time_elpased: 2.23
batch start
#iterations: 237
currently lose_sum: 95.08437389135361
time_elpased: 2.262
batch start
#iterations: 238
currently lose_sum: 95.40795266628265
time_elpased: 2.231
batch start
#iterations: 239
currently lose_sum: 95.07970267534256
time_elpased: 2.224
start validation test
0.65675257732
0.638790520294
0.724063400576
0.678759226205
0.656641365716
61.851
batch start
#iterations: 240
currently lose_sum: 95.06788951158524
time_elpased: 2.275
batch start
#iterations: 241
currently lose_sum: 94.87630885839462
time_elpased: 2.238
batch start
#iterations: 242
currently lose_sum: 95.0312567949295
time_elpased: 2.246
batch start
#iterations: 243
currently lose_sum: 95.15813761949539
time_elpased: 2.264
batch start
#iterations: 244
currently lose_sum: 95.4165511727333
time_elpased: 2.346
batch start
#iterations: 245
currently lose_sum: 95.3233215212822
time_elpased: 2.278
batch start
#iterations: 246
currently lose_sum: 94.56895327568054
time_elpased: 2.364
batch start
#iterations: 247
currently lose_sum: 95.3012884259224
time_elpased: 2.09
batch start
#iterations: 248
currently lose_sum: 94.87445783615112
time_elpased: 2.108
batch start
#iterations: 249
currently lose_sum: 95.12442898750305
time_elpased: 2.123
batch start
#iterations: 250
currently lose_sum: 95.0865975022316
time_elpased: 2.137
batch start
#iterations: 251
currently lose_sum: 95.24385964870453
time_elpased: 2.214
batch start
#iterations: 252
currently lose_sum: 95.1728001832962
time_elpased: 2.242
batch start
#iterations: 253
currently lose_sum: 94.94214761257172
time_elpased: 2.269
batch start
#iterations: 254
currently lose_sum: 94.91071832180023
time_elpased: 2.251
batch start
#iterations: 255
currently lose_sum: 95.3816442489624
time_elpased: 2.14
batch start
#iterations: 256
currently lose_sum: 94.52008944749832
time_elpased: 2.24
batch start
#iterations: 257
currently lose_sum: 94.89748430252075
time_elpased: 2.212
batch start
#iterations: 258
currently lose_sum: 95.2760608792305
time_elpased: 2.269
batch start
#iterations: 259
currently lose_sum: 95.20974922180176
time_elpased: 2.215
start validation test
0.650154639175
0.636321325514
0.703581720873
0.66826335598
0.650066366426
61.997
batch start
#iterations: 260
currently lose_sum: 94.81886774301529
time_elpased: 2.316
batch start
#iterations: 261
currently lose_sum: 95.05733972787857
time_elpased: 2.273
batch start
#iterations: 262
currently lose_sum: 94.67147254943848
time_elpased: 2.311
batch start
#iterations: 263
currently lose_sum: 95.03359651565552
time_elpased: 2.214
batch start
#iterations: 264
currently lose_sum: 95.21649849414825
time_elpased: 2.199
batch start
#iterations: 265
currently lose_sum: 95.02698940038681
time_elpased: 2.262
batch start
#iterations: 266
currently lose_sum: 95.04900699853897
time_elpased: 2.292
batch start
#iterations: 267
currently lose_sum: 95.05846410989761
time_elpased: 2.302
batch start
#iterations: 268
currently lose_sum: 94.61870312690735
time_elpased: 2.289
batch start
#iterations: 269
currently lose_sum: 95.1198194026947
time_elpased: 2.283
batch start
#iterations: 270
currently lose_sum: 94.5644588470459
time_elpased: 2.286
batch start
#iterations: 271
currently lose_sum: 95.10911911725998
time_elpased: 2.188
batch start
#iterations: 272
currently lose_sum: 94.96522271633148
time_elpased: 2.183
batch start
#iterations: 273
currently lose_sum: 94.91008245944977
time_elpased: 2.231
batch start
#iterations: 274
currently lose_sum: 95.2338849902153
time_elpased: 2.241
batch start
#iterations: 275
currently lose_sum: 95.14896076917648
time_elpased: 2.225
batch start
#iterations: 276
currently lose_sum: 94.82569175958633
time_elpased: 2.323
batch start
#iterations: 277
currently lose_sum: 94.63516283035278
time_elpased: 2.284
batch start
#iterations: 278
currently lose_sum: 95.10038608312607
time_elpased: 2.203
batch start
#iterations: 279
currently lose_sum: 95.17912459373474
time_elpased: 2.228
start validation test
0.658711340206
0.640413755558
0.726430629889
0.680715629069
0.65859945373
61.881
batch start
#iterations: 280
currently lose_sum: 95.14654856920242
time_elpased: 2.234
batch start
#iterations: 281
currently lose_sum: 94.70389223098755
time_elpased: 2.204
batch start
#iterations: 282
currently lose_sum: 95.33625292778015
time_elpased: 2.293
batch start
#iterations: 283
currently lose_sum: 94.91058319807053
time_elpased: 2.291
batch start
#iterations: 284
currently lose_sum: 94.7140519618988
time_elpased: 2.274
batch start
#iterations: 285
currently lose_sum: 95.04221910238266
time_elpased: 2.301
batch start
#iterations: 286
currently lose_sum: 94.80592930316925
time_elpased: 2.168
batch start
#iterations: 287
currently lose_sum: 94.88322359323502
time_elpased: 2.27
batch start
#iterations: 288
currently lose_sum: 94.80689853429794
time_elpased: 2.266
batch start
#iterations: 289
currently lose_sum: 94.89488738775253
time_elpased: 2.189
batch start
#iterations: 290
currently lose_sum: 94.83245658874512
time_elpased: 2.243
batch start
#iterations: 291
currently lose_sum: 94.98331272602081
time_elpased: 2.285
batch start
#iterations: 292
currently lose_sum: 94.72811049222946
time_elpased: 2.221
batch start
#iterations: 293
currently lose_sum: 94.72803258895874
time_elpased: 2.234
batch start
#iterations: 294
currently lose_sum: 95.03425121307373
time_elpased: 2.237
batch start
#iterations: 295
currently lose_sum: 95.1280819773674
time_elpased: 2.26
batch start
#iterations: 296
currently lose_sum: 94.90132027864456
time_elpased: 2.253
batch start
#iterations: 297
currently lose_sum: 94.91112869977951
time_elpased: 2.257
batch start
#iterations: 298
currently lose_sum: 94.8608986735344
time_elpased: 2.17
batch start
#iterations: 299
currently lose_sum: 95.18998384475708
time_elpased: 2.211
start validation test
0.649896907216
0.638499431603
0.693701111569
0.664956590371
0.649824533479
62.013
batch start
#iterations: 300
currently lose_sum: 95.09628999233246
time_elpased: 2.248
batch start
#iterations: 301
currently lose_sum: 94.75526624917984
time_elpased: 2.176
batch start
#iterations: 302
currently lose_sum: 94.5045895576477
time_elpased: 2.288
batch start
#iterations: 303
currently lose_sum: 94.75169229507446
time_elpased: 2.278
batch start
#iterations: 304
currently lose_sum: 94.5070772767067
time_elpased: 2.274
batch start
#iterations: 305
currently lose_sum: 94.87007415294647
time_elpased: 2.23
batch start
#iterations: 306
currently lose_sum: 95.28034204244614
time_elpased: 2.167
batch start
#iterations: 307
currently lose_sum: 94.75286948680878
time_elpased: 2.194
batch start
#iterations: 308
currently lose_sum: 94.78674203157425
time_elpased: 2.291
batch start
#iterations: 309
currently lose_sum: 94.72795188426971
time_elpased: 2.1
batch start
#iterations: 310
currently lose_sum: 94.59752553701401
time_elpased: 2.098
batch start
#iterations: 311
currently lose_sum: 94.82747519016266
time_elpased: 2.088
batch start
#iterations: 312
currently lose_sum: 95.00608110427856
time_elpased: 2.114
batch start
#iterations: 313
currently lose_sum: 94.82973378896713
time_elpased: 2.037
batch start
#iterations: 314
currently lose_sum: 94.81120938062668
time_elpased: 2.09
batch start
#iterations: 315
currently lose_sum: 94.3721045255661
time_elpased: 2.109
batch start
#iterations: 316
currently lose_sum: 94.74057751893997
time_elpased: 2.093
batch start
#iterations: 317
currently lose_sum: 94.77442747354507
time_elpased: 2.154
batch start
#iterations: 318
currently lose_sum: 94.68485778570175
time_elpased: 2.13
batch start
#iterations: 319
currently lose_sum: 94.74692577123642
time_elpased: 2.104
start validation test
0.653453608247
0.637029576046
0.716035405517
0.674225904928
0.653350209987
61.746
batch start
#iterations: 320
currently lose_sum: 94.96753197908401
time_elpased: 2.1
batch start
#iterations: 321
currently lose_sum: 94.74613964557648
time_elpased: 2.148
batch start
#iterations: 322
currently lose_sum: 95.05162298679352
time_elpased: 2.172
batch start
#iterations: 323
currently lose_sum: 95.09910589456558
time_elpased: 2.121
batch start
#iterations: 324
currently lose_sum: 94.6094878911972
time_elpased: 2.179
batch start
#iterations: 325
currently lose_sum: 94.8296412229538
time_elpased: 2.144
batch start
#iterations: 326
currently lose_sum: 94.46813958883286
time_elpased: 2.299
batch start
#iterations: 327
currently lose_sum: 94.59034460783005
time_elpased: 2.282
batch start
#iterations: 328
currently lose_sum: 95.05753648281097
time_elpased: 2.14
batch start
#iterations: 329
currently lose_sum: 94.69327735900879
time_elpased: 2.116
batch start
#iterations: 330
currently lose_sum: 94.97466796636581
time_elpased: 2.234
batch start
#iterations: 331
currently lose_sum: 94.77948081493378
time_elpased: 2.253
batch start
#iterations: 332
currently lose_sum: 94.94410383701324
time_elpased: 2.265
batch start
#iterations: 333
currently lose_sum: 95.01187175512314
time_elpased: 2.221
batch start
#iterations: 334
currently lose_sum: 94.84654253721237
time_elpased: 2.181
batch start
#iterations: 335
currently lose_sum: 94.32062393426895
time_elpased: 2.208
batch start
#iterations: 336
currently lose_sum: 94.6971954703331
time_elpased: 2.157
batch start
#iterations: 337
currently lose_sum: 94.44303530454636
time_elpased: 2.215
batch start
#iterations: 338
currently lose_sum: 94.59799122810364
time_elpased: 2.217
batch start
#iterations: 339
currently lose_sum: 94.80483496189117
time_elpased: 2.288
start validation test
0.658556701031
0.639329488104
0.730135858378
0.681722083413
0.658438437243
61.677
batch start
#iterations: 340
currently lose_sum: 94.73281794786453
time_elpased: 2.266
batch start
#iterations: 341
currently lose_sum: 94.60595464706421
time_elpased: 2.275
batch start
#iterations: 342
currently lose_sum: 95.15409797430038
time_elpased: 2.299
batch start
#iterations: 343
currently lose_sum: 95.16316378116608
time_elpased: 2.279
batch start
#iterations: 344
currently lose_sum: 94.96512067317963
time_elpased: 2.271
batch start
#iterations: 345
currently lose_sum: 94.85158133506775
time_elpased: 2.143
batch start
#iterations: 346
currently lose_sum: 94.63981944322586
time_elpased: 2.122
batch start
#iterations: 347
currently lose_sum: 94.96460646390915
time_elpased: 2.277
batch start
#iterations: 348
currently lose_sum: 95.00336748361588
time_elpased: 2.278
batch start
#iterations: 349
currently lose_sum: 94.67329430580139
time_elpased: 2.272
batch start
#iterations: 350
currently lose_sum: 95.14120662212372
time_elpased: 2.29
batch start
#iterations: 351
currently lose_sum: 94.71646291017532
time_elpased: 2.218
batch start
#iterations: 352
currently lose_sum: 95.07362097501755
time_elpased: 2.268
batch start
#iterations: 353
currently lose_sum: 94.83394622802734
time_elpased: 2.265
batch start
#iterations: 354
currently lose_sum: 94.84890502691269
time_elpased: 2.123
batch start
#iterations: 355
currently lose_sum: 94.69698202610016
time_elpased: 2.263
batch start
#iterations: 356
currently lose_sum: 94.8481297492981
time_elpased: 2.259
batch start
#iterations: 357
currently lose_sum: 94.39410477876663
time_elpased: 2.275
batch start
#iterations: 358
currently lose_sum: 94.8499283194542
time_elpased: 2.272
batch start
#iterations: 359
currently lose_sum: 94.760304749012
time_elpased: 2.239
start validation test
0.656804123711
0.637376460018
0.730135858378
0.680610189005
0.656682964298
61.715
batch start
#iterations: 360
currently lose_sum: 94.97451692819595
time_elpased: 2.247
batch start
#iterations: 361
currently lose_sum: 94.6179609298706
time_elpased: 2.286
batch start
#iterations: 362
currently lose_sum: 95.32577896118164
time_elpased: 2.154
batch start
#iterations: 363
currently lose_sum: 94.5901580452919
time_elpased: 2.202
batch start
#iterations: 364
currently lose_sum: 94.8069599866867
time_elpased: 2.281
batch start
#iterations: 365
currently lose_sum: 95.1215271949768
time_elpased: 2.273
batch start
#iterations: 366
currently lose_sum: 94.73035913705826
time_elpased: 2.255
batch start
#iterations: 367
currently lose_sum: 94.44714951515198
time_elpased: 2.309
batch start
#iterations: 368
currently lose_sum: 95.09725737571716
time_elpased: 2.281
batch start
#iterations: 369
currently lose_sum: 94.42723101377487
time_elpased: 2.249
batch start
#iterations: 370
currently lose_sum: 94.55687648057938
time_elpased: 2.234
batch start
#iterations: 371
currently lose_sum: 94.59573197364807
time_elpased: 2.052
batch start
#iterations: 372
currently lose_sum: 95.20073330402374
time_elpased: 2.119
batch start
#iterations: 373
currently lose_sum: 94.68414831161499
time_elpased: 2.078
batch start
#iterations: 374
currently lose_sum: 94.88459318876266
time_elpased: 2.068
batch start
#iterations: 375
currently lose_sum: 94.70702582597733
time_elpased: 2.075
batch start
#iterations: 376
currently lose_sum: 94.51246011257172
time_elpased: 2.107
batch start
#iterations: 377
currently lose_sum: 94.65217196941376
time_elpased: 2.175
batch start
#iterations: 378
currently lose_sum: 94.48600775003433
time_elpased: 2.128
batch start
#iterations: 379
currently lose_sum: 94.87495768070221
time_elpased: 2.132
start validation test
0.656340206186
0.638452456634
0.723548785508
0.678342258889
0.65622916351
61.788
batch start
#iterations: 380
currently lose_sum: 94.5942844748497
time_elpased: 2.272
batch start
#iterations: 381
currently lose_sum: 94.79812049865723
time_elpased: 2.282
batch start
#iterations: 382
currently lose_sum: 94.91921955347061
time_elpased: 2.219
batch start
#iterations: 383
currently lose_sum: 94.53543365001678
time_elpased: 2.235
batch start
#iterations: 384
currently lose_sum: 94.86074310541153
time_elpased: 2.275
batch start
#iterations: 385
currently lose_sum: 94.76994919776917
time_elpased: 2.284
batch start
#iterations: 386
currently lose_sum: 94.7412496805191
time_elpased: 2.217
batch start
#iterations: 387
currently lose_sum: 94.30475622415543
time_elpased: 2.203
batch start
#iterations: 388
currently lose_sum: 95.19516909122467
time_elpased: 2.179
batch start
#iterations: 389
currently lose_sum: 94.28053879737854
time_elpased: 2.144
batch start
#iterations: 390
currently lose_sum: 94.69669252634048
time_elpased: 2.169
batch start
#iterations: 391
currently lose_sum: 94.52288746833801
time_elpased: 2.191
batch start
#iterations: 392
currently lose_sum: 94.68418592214584
time_elpased: 2.249
batch start
#iterations: 393
currently lose_sum: 94.59954410791397
time_elpased: 2.285
batch start
#iterations: 394
currently lose_sum: 94.44489043951035
time_elpased: 2.177
batch start
#iterations: 395
currently lose_sum: 94.63209527730942
time_elpased: 2.335
batch start
#iterations: 396
currently lose_sum: 94.48102408647537
time_elpased: 2.242
batch start
#iterations: 397
currently lose_sum: 94.90105044841766
time_elpased: 2.274
batch start
#iterations: 398
currently lose_sum: 94.73546487092972
time_elpased: 2.189
batch start
#iterations: 399
currently lose_sum: 94.56554728746414
time_elpased: 2.237
start validation test
0.65675257732
0.639601790118
0.720769864142
0.677764335834
0.656646807329
61.748
acc: 0.666
pre: 0.644
rec: 0.744
F1: 0.691
auc: 0.666
