start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.52145332098007
time_elpased: 2.24
batch start
#iterations: 1
currently lose_sum: 100.16905492544174
time_elpased: 2.064
batch start
#iterations: 2
currently lose_sum: 100.0194485783577
time_elpased: 2.09
batch start
#iterations: 3
currently lose_sum: 99.6149337887764
time_elpased: 2.106
batch start
#iterations: 4
currently lose_sum: 99.37549483776093
time_elpased: 2.123
batch start
#iterations: 5
currently lose_sum: 99.14389669895172
time_elpased: 2.117
batch start
#iterations: 6
currently lose_sum: 98.92059051990509
time_elpased: 2.014
batch start
#iterations: 7
currently lose_sum: 98.64779257774353
time_elpased: 2.073
batch start
#iterations: 8
currently lose_sum: 98.64947891235352
time_elpased: 2.09
batch start
#iterations: 9
currently lose_sum: 98.43211096525192
time_elpased: 2.052
batch start
#iterations: 10
currently lose_sum: 98.09592878818512
time_elpased: 2.03
batch start
#iterations: 11
currently lose_sum: 98.13617372512817
time_elpased: 2.096
batch start
#iterations: 12
currently lose_sum: 97.91512370109558
time_elpased: 2.089
batch start
#iterations: 13
currently lose_sum: 98.13148093223572
time_elpased: 2.118
batch start
#iterations: 14
currently lose_sum: 97.45925641059875
time_elpased: 2.107
batch start
#iterations: 15
currently lose_sum: 97.8452844619751
time_elpased: 2.006
batch start
#iterations: 16
currently lose_sum: 97.3912963271141
time_elpased: 2.086
batch start
#iterations: 17
currently lose_sum: 97.39245945215225
time_elpased: 2.102
batch start
#iterations: 18
currently lose_sum: 97.31331932544708
time_elpased: 2.028
batch start
#iterations: 19
currently lose_sum: 97.12840038537979
time_elpased: 2.06
start validation test
0.646546391753
0.629881925522
0.713697643306
0.669175471607
0.646428497373
62.741
batch start
#iterations: 20
currently lose_sum: 97.35341936349869
time_elpased: 2.14
batch start
#iterations: 21
currently lose_sum: 97.1690884232521
time_elpased: 2.082
batch start
#iterations: 22
currently lose_sum: 97.09219312667847
time_elpased: 2.02
batch start
#iterations: 23
currently lose_sum: 96.89819538593292
time_elpased: 1.938
batch start
#iterations: 24
currently lose_sum: 96.87836396694183
time_elpased: 2.017
batch start
#iterations: 25
currently lose_sum: 96.90331691503525
time_elpased: 2.078
batch start
#iterations: 26
currently lose_sum: 97.09194886684418
time_elpased: 2.115
batch start
#iterations: 27
currently lose_sum: 96.86537081003189
time_elpased: 2.068
batch start
#iterations: 28
currently lose_sum: 96.52939188480377
time_elpased: 2.1
batch start
#iterations: 29
currently lose_sum: 96.92796981334686
time_elpased: 2.016
batch start
#iterations: 30
currently lose_sum: 96.84326392412186
time_elpased: 2.121
batch start
#iterations: 31
currently lose_sum: 96.27400726079941
time_elpased: 2.068
batch start
#iterations: 32
currently lose_sum: 96.4424369931221
time_elpased: 2.14
batch start
#iterations: 33
currently lose_sum: 96.66215801239014
time_elpased: 1.999
batch start
#iterations: 34
currently lose_sum: 96.50269496440887
time_elpased: 1.95
batch start
#iterations: 35
currently lose_sum: 96.55451476573944
time_elpased: 1.967
batch start
#iterations: 36
currently lose_sum: 96.62213152647018
time_elpased: 1.943
batch start
#iterations: 37
currently lose_sum: 96.2548925280571
time_elpased: 1.928
batch start
#iterations: 38
currently lose_sum: 96.38940107822418
time_elpased: 1.925
batch start
#iterations: 39
currently lose_sum: 96.34607464075089
time_elpased: 1.977
start validation test
0.644948453608
0.638121277219
0.672532674694
0.654875238
0.644900025254
62.390
batch start
#iterations: 40
currently lose_sum: 96.67577916383743
time_elpased: 2.12
batch start
#iterations: 41
currently lose_sum: 96.24407440423965
time_elpased: 2.017
batch start
#iterations: 42
currently lose_sum: 96.27837681770325
time_elpased: 2.119
batch start
#iterations: 43
currently lose_sum: 96.22560274600983
time_elpased: 1.992
batch start
#iterations: 44
currently lose_sum: 96.2867865562439
time_elpased: 1.985
batch start
#iterations: 45
currently lose_sum: 96.35209131240845
time_elpased: 2.082
batch start
#iterations: 46
currently lose_sum: 95.95814424753189
time_elpased: 2.017
batch start
#iterations: 47
currently lose_sum: 96.01355546712875
time_elpased: 2.082
batch start
#iterations: 48
currently lose_sum: 96.56212788820267
time_elpased: 2.176
batch start
#iterations: 49
currently lose_sum: 96.2168185710907
time_elpased: 2.037
batch start
#iterations: 50
currently lose_sum: 96.14857459068298
time_elpased: 2.071
batch start
#iterations: 51
currently lose_sum: 95.9919655919075
time_elpased: 2.013
batch start
#iterations: 52
currently lose_sum: 95.9251691699028
time_elpased: 2.017
batch start
#iterations: 53
currently lose_sum: 95.96453964710236
time_elpased: 2.082
batch start
#iterations: 54
currently lose_sum: 96.39621424674988
time_elpased: 2.054
batch start
#iterations: 55
currently lose_sum: 95.83014982938766
time_elpased: 2.101
batch start
#iterations: 56
currently lose_sum: 96.20926904678345
time_elpased: 2.105
batch start
#iterations: 57
currently lose_sum: 96.02236729860306
time_elpased: 2.091
batch start
#iterations: 58
currently lose_sum: 95.78758549690247
time_elpased: 2.098
batch start
#iterations: 59
currently lose_sum: 96.06598645448685
time_elpased: 2.134
start validation test
0.65118556701
0.638915049915
0.698157867655
0.667223998033
0.651103099892
61.896
batch start
#iterations: 60
currently lose_sum: 95.8537785410881
time_elpased: 2.069
batch start
#iterations: 61
currently lose_sum: 96.01946979761124
time_elpased: 2.035
batch start
#iterations: 62
currently lose_sum: 95.96499407291412
time_elpased: 2.039
batch start
#iterations: 63
currently lose_sum: 96.04027235507965
time_elpased: 2.009
batch start
#iterations: 64
currently lose_sum: 96.24043065309525
time_elpased: 2.087
batch start
#iterations: 65
currently lose_sum: 96.13626950979233
time_elpased: 2.095
batch start
#iterations: 66
currently lose_sum: 95.65471488237381
time_elpased: 2.104
batch start
#iterations: 67
currently lose_sum: 95.90772515535355
time_elpased: 2.128
batch start
#iterations: 68
currently lose_sum: 95.85718315839767
time_elpased: 2.084
batch start
#iterations: 69
currently lose_sum: 95.77806347608566
time_elpased: 2.098
batch start
#iterations: 70
currently lose_sum: 95.67215776443481
time_elpased: 1.99
batch start
#iterations: 71
currently lose_sum: 95.74031710624695
time_elpased: 2.059
batch start
#iterations: 72
currently lose_sum: 95.8908212184906
time_elpased: 2.035
batch start
#iterations: 73
currently lose_sum: 96.15688180923462
time_elpased: 2.073
batch start
#iterations: 74
currently lose_sum: 96.0124699473381
time_elpased: 2.07
batch start
#iterations: 75
currently lose_sum: 95.84870487451553
time_elpased: 1.976
batch start
#iterations: 76
currently lose_sum: 95.93573331832886
time_elpased: 2.064
batch start
#iterations: 77
currently lose_sum: 95.67462486028671
time_elpased: 2.083
batch start
#iterations: 78
currently lose_sum: 95.62571150064468
time_elpased: 2.02
batch start
#iterations: 79
currently lose_sum: 96.10536378622055
time_elpased: 2.061
start validation test
0.651391752577
0.642540050183
0.685190902542
0.663180437273
0.651332412956
61.817
batch start
#iterations: 80
currently lose_sum: 95.88454723358154
time_elpased: 2.057
batch start
#iterations: 81
currently lose_sum: 95.80150002241135
time_elpased: 2.147
batch start
#iterations: 82
currently lose_sum: 96.1092637181282
time_elpased: 2.141
batch start
#iterations: 83
currently lose_sum: 95.65752387046814
time_elpased: 2.117
batch start
#iterations: 84
currently lose_sum: 95.7425885796547
time_elpased: 2.112
batch start
#iterations: 85
currently lose_sum: 95.93447232246399
time_elpased: 2.131
batch start
#iterations: 86
currently lose_sum: 95.83293706178665
time_elpased: 2.123
batch start
#iterations: 87
currently lose_sum: 95.73027151823044
time_elpased: 2.039
batch start
#iterations: 88
currently lose_sum: 95.73769313097
time_elpased: 1.948
batch start
#iterations: 89
currently lose_sum: 95.48278051614761
time_elpased: 1.912
batch start
#iterations: 90
currently lose_sum: 96.03797972202301
time_elpased: 2.011
batch start
#iterations: 91
currently lose_sum: 95.76443481445312
time_elpased: 2.245
batch start
#iterations: 92
currently lose_sum: 95.59475177526474
time_elpased: 2.116
batch start
#iterations: 93
currently lose_sum: 95.59521174430847
time_elpased: 2.092
batch start
#iterations: 94
currently lose_sum: 95.87124109268188
time_elpased: 2.113
batch start
#iterations: 95
currently lose_sum: 95.6581203341484
time_elpased: 2.104
batch start
#iterations: 96
currently lose_sum: 95.32497316598892
time_elpased: 1.929
batch start
#iterations: 97
currently lose_sum: 95.4653052687645
time_elpased: 2.027
batch start
#iterations: 98
currently lose_sum: 95.66885727643967
time_elpased: 2.143
batch start
#iterations: 99
currently lose_sum: 95.78622817993164
time_elpased: 2.066
start validation test
0.657268041237
0.63970856102
0.722856848822
0.678745711939
0.657152889969
61.650
batch start
#iterations: 100
currently lose_sum: 95.79750090837479
time_elpased: 2.106
batch start
#iterations: 101
currently lose_sum: 95.73931515216827
time_elpased: 2.114
batch start
#iterations: 102
currently lose_sum: 95.81240993738174
time_elpased: 2.11
batch start
#iterations: 103
currently lose_sum: 95.61139851808548
time_elpased: 2.081
batch start
#iterations: 104
currently lose_sum: 95.8124640583992
time_elpased: 2.121
batch start
#iterations: 105
currently lose_sum: 95.34753930568695
time_elpased: 2.028
batch start
#iterations: 106
currently lose_sum: 95.68063408136368
time_elpased: 2.09
batch start
#iterations: 107
currently lose_sum: 95.8222246170044
time_elpased: 2.077
batch start
#iterations: 108
currently lose_sum: 95.6663510799408
time_elpased: 2.009
batch start
#iterations: 109
currently lose_sum: 95.57501447200775
time_elpased: 2.091
batch start
#iterations: 110
currently lose_sum: 96.0061137676239
time_elpased: 2.098
batch start
#iterations: 111
currently lose_sum: 95.43342316150665
time_elpased: 2.107
batch start
#iterations: 112
currently lose_sum: 95.60818874835968
time_elpased: 2.108
batch start
#iterations: 113
currently lose_sum: 95.12122201919556
time_elpased: 2.132
batch start
#iterations: 114
currently lose_sum: 95.40745681524277
time_elpased: 2.009
batch start
#iterations: 115
currently lose_sum: 95.50086832046509
time_elpased: 2.123
batch start
#iterations: 116
currently lose_sum: 95.80067878961563
time_elpased: 2.07
batch start
#iterations: 117
currently lose_sum: 94.98631483316422
time_elpased: 2.075
batch start
#iterations: 118
currently lose_sum: 95.35125893354416
time_elpased: 2.125
batch start
#iterations: 119
currently lose_sum: 94.97074568271637
time_elpased: 2.122
start validation test
0.653917525773
0.641932129691
0.698878254605
0.66919590067
0.653838590279
61.487
batch start
#iterations: 120
currently lose_sum: 96.00521445274353
time_elpased: 2.145
batch start
#iterations: 121
currently lose_sum: 95.21935957670212
time_elpased: 2.147
batch start
#iterations: 122
currently lose_sum: 95.44507551193237
time_elpased: 2.086
batch start
#iterations: 123
currently lose_sum: 95.22951769828796
time_elpased: 2.049
batch start
#iterations: 124
currently lose_sum: 95.6850283741951
time_elpased: 2.112
batch start
#iterations: 125
currently lose_sum: 95.7371392250061
time_elpased: 2.043
batch start
#iterations: 126
currently lose_sum: 95.13106840848923
time_elpased: 2.113
batch start
#iterations: 127
currently lose_sum: 95.69655030965805
time_elpased: 2.145
batch start
#iterations: 128
currently lose_sum: 95.36064183712006
time_elpased: 2.166
batch start
#iterations: 129
currently lose_sum: 95.9170732498169
time_elpased: 2.186
batch start
#iterations: 130
currently lose_sum: 95.47583907842636
time_elpased: 2.158
batch start
#iterations: 131
currently lose_sum: 95.6806715130806
time_elpased: 2.072
batch start
#iterations: 132
currently lose_sum: 95.65120112895966
time_elpased: 2.08
batch start
#iterations: 133
currently lose_sum: 95.503666639328
time_elpased: 2.096
batch start
#iterations: 134
currently lose_sum: 95.16068863868713
time_elpased: 2.08
batch start
#iterations: 135
currently lose_sum: 95.1836296916008
time_elpased: 2.136
batch start
#iterations: 136
currently lose_sum: 95.15498143434525
time_elpased: 2.119
batch start
#iterations: 137
currently lose_sum: 95.43180048465729
time_elpased: 2.118
batch start
#iterations: 138
currently lose_sum: 95.68789368867874
time_elpased: 2.116
batch start
#iterations: 139
currently lose_sum: 95.04904466867447
time_elpased: 2.152
start validation test
0.658092783505
0.640590809628
0.723062673665
0.679332849891
0.657978718842
61.382
batch start
#iterations: 140
currently lose_sum: 95.3016009926796
time_elpased: 2.045
batch start
#iterations: 141
currently lose_sum: 95.90973645448685
time_elpased: 2.103
batch start
#iterations: 142
currently lose_sum: 95.22595459222794
time_elpased: 2.046
batch start
#iterations: 143
currently lose_sum: 95.36955720186234
time_elpased: 2.158
batch start
#iterations: 144
currently lose_sum: 95.3776091337204
time_elpased: 2.094
batch start
#iterations: 145
currently lose_sum: 95.45590072870255
time_elpased: 2.179
batch start
#iterations: 146
currently lose_sum: 95.31895065307617
time_elpased: 2.098
batch start
#iterations: 147
currently lose_sum: 95.29933452606201
time_elpased: 2.11
batch start
#iterations: 148
currently lose_sum: 95.16404795646667
time_elpased: 2.142
batch start
#iterations: 149
currently lose_sum: 95.33167231082916
time_elpased: 2.078
batch start
#iterations: 150
currently lose_sum: 95.36627447605133
time_elpased: 2.197
batch start
#iterations: 151
currently lose_sum: 95.50789839029312
time_elpased: 2.07
batch start
#iterations: 152
currently lose_sum: 95.23714411258698
time_elpased: 2.129
batch start
#iterations: 153
currently lose_sum: 95.51145303249359
time_elpased: 2.157
batch start
#iterations: 154
currently lose_sum: 95.43260860443115
time_elpased: 2.154
batch start
#iterations: 155
currently lose_sum: 95.37626504898071
time_elpased: 2.154
batch start
#iterations: 156
currently lose_sum: 95.44890415668488
time_elpased: 2.196
batch start
#iterations: 157
currently lose_sum: 94.93105417490005
time_elpased: 2.095
batch start
#iterations: 158
currently lose_sum: 95.25820308923721
time_elpased: 2.019
batch start
#iterations: 159
currently lose_sum: 95.38937985897064
time_elpased: 2.077
start validation test
0.657113402062
0.635296194932
0.740557785325
0.683900399164
0.656966902577
61.201
batch start
#iterations: 160
currently lose_sum: 95.08751636743546
time_elpased: 2.137
batch start
#iterations: 161
currently lose_sum: 95.55217599868774
time_elpased: 2.123
batch start
#iterations: 162
currently lose_sum: 95.66840589046478
time_elpased: 2.141
batch start
#iterations: 163
currently lose_sum: 95.48890960216522
time_elpased: 2.17
batch start
#iterations: 164
currently lose_sum: 95.31196534633636
time_elpased: 2.194
batch start
#iterations: 165
currently lose_sum: 95.27650761604309
time_elpased: 2.136
batch start
#iterations: 166
currently lose_sum: 95.17843180894852
time_elpased: 2.061
batch start
#iterations: 167
currently lose_sum: 95.21851432323456
time_elpased: 2.061
batch start
#iterations: 168
currently lose_sum: 95.0032679438591
time_elpased: 2.076
batch start
#iterations: 169
currently lose_sum: 95.36533069610596
time_elpased: 2.131
batch start
#iterations: 170
currently lose_sum: 95.31659972667694
time_elpased: 2.142
batch start
#iterations: 171
currently lose_sum: 95.53376370668411
time_elpased: 2.134
batch start
#iterations: 172
currently lose_sum: 95.66744619607925
time_elpased: 2.123
batch start
#iterations: 173
currently lose_sum: 95.38494962453842
time_elpased: 2.301
batch start
#iterations: 174
currently lose_sum: 95.13889908790588
time_elpased: 2.153
batch start
#iterations: 175
currently lose_sum: 94.85381382703781
time_elpased: 2.021
batch start
#iterations: 176
currently lose_sum: 94.96374261379242
time_elpased: 2.106
batch start
#iterations: 177
currently lose_sum: 95.26721203327179
time_elpased: 2.092
batch start
#iterations: 178
currently lose_sum: 95.72502321004868
time_elpased: 2.142
batch start
#iterations: 179
currently lose_sum: 95.17560756206512
time_elpased: 2.143
start validation test
0.65706185567
0.638994737797
0.724812184831
0.679203433145
0.656942909517
61.217
batch start
#iterations: 180
currently lose_sum: 94.99872070550919
time_elpased: 2.141
batch start
#iterations: 181
currently lose_sum: 95.14076668024063
time_elpased: 2.085
batch start
#iterations: 182
currently lose_sum: 95.31149077415466
time_elpased: 2.159
batch start
#iterations: 183
currently lose_sum: 95.18972891569138
time_elpased: 2.104
batch start
#iterations: 184
currently lose_sum: 95.58781117200851
time_elpased: 2.048
batch start
#iterations: 185
currently lose_sum: 95.29063230752945
time_elpased: 2.038
batch start
#iterations: 186
currently lose_sum: 95.13049232959747
time_elpased: 2.153
batch start
#iterations: 187
currently lose_sum: 95.2408818602562
time_elpased: 2.098
batch start
#iterations: 188
currently lose_sum: 95.36141407489777
time_elpased: 2.153
batch start
#iterations: 189
currently lose_sum: 95.1095324754715
time_elpased: 2.124
batch start
#iterations: 190
currently lose_sum: 94.93636053800583
time_elpased: 2.151
batch start
#iterations: 191
currently lose_sum: 95.17315059900284
time_elpased: 2.109
batch start
#iterations: 192
currently lose_sum: 95.06982272863388
time_elpased: 2.047
batch start
#iterations: 193
currently lose_sum: 95.34503984451294
time_elpased: 2.057
batch start
#iterations: 194
currently lose_sum: 95.0085671544075
time_elpased: 2.102
batch start
#iterations: 195
currently lose_sum: 94.95272082090378
time_elpased: 2.145
batch start
#iterations: 196
currently lose_sum: 95.17274767160416
time_elpased: 2.145
batch start
#iterations: 197
currently lose_sum: 95.26764810085297
time_elpased: 2.148
batch start
#iterations: 198
currently lose_sum: 95.17311745882034
time_elpased: 2.159
batch start
#iterations: 199
currently lose_sum: 94.95905041694641
time_elpased: 2.182
start validation test
0.654020618557
0.644207697476
0.690748173305
0.666666666667
0.65395613767
61.275
batch start
#iterations: 200
currently lose_sum: 95.01790660619736
time_elpased: 2.164
batch start
#iterations: 201
currently lose_sum: 95.21836125850677
time_elpased: 2.067
batch start
#iterations: 202
currently lose_sum: 94.90245568752289
time_elpased: 2.079
batch start
#iterations: 203
currently lose_sum: 95.00279599428177
time_elpased: 2.141
batch start
#iterations: 204
currently lose_sum: 95.06144827604294
time_elpased: 2.157
batch start
#iterations: 205
currently lose_sum: 94.86675089597702
time_elpased: 2.14
batch start
#iterations: 206
currently lose_sum: 94.71844083070755
time_elpased: 2.142
batch start
#iterations: 207
currently lose_sum: 95.06736040115356
time_elpased: 2.113
batch start
#iterations: 208
currently lose_sum: 95.25079208612442
time_elpased: 2.181
batch start
#iterations: 209
currently lose_sum: 94.6459373831749
time_elpased: 2.16
batch start
#iterations: 210
currently lose_sum: 95.00102388858795
time_elpased: 2.097
batch start
#iterations: 211
currently lose_sum: 95.13389760255814
time_elpased: 2.042
batch start
#iterations: 212
currently lose_sum: 95.45534497499466
time_elpased: 2.128
batch start
#iterations: 213
currently lose_sum: 94.96764415502548
time_elpased: 2.188
batch start
#iterations: 214
currently lose_sum: 95.38054674863815
time_elpased: 2.158
batch start
#iterations: 215
currently lose_sum: 95.02563494443893
time_elpased: 2.192
batch start
#iterations: 216
currently lose_sum: 95.08046495914459
time_elpased: 2.193
batch start
#iterations: 217
currently lose_sum: 95.14676475524902
time_elpased: 2.159
batch start
#iterations: 218
currently lose_sum: 94.76144468784332
time_elpased: 2.161
batch start
#iterations: 219
currently lose_sum: 95.0129731297493
time_elpased: 2.071
start validation test
0.657835051546
0.64255949625
0.714109292992
0.676447650614
0.657736253436
61.433
batch start
#iterations: 220
currently lose_sum: 95.25245463848114
time_elpased: 2.058
batch start
#iterations: 221
currently lose_sum: 94.99541783332825
time_elpased: 2.131
batch start
#iterations: 222
currently lose_sum: 95.08377015590668
time_elpased: 2.166
batch start
#iterations: 223
currently lose_sum: 94.88818329572678
time_elpased: 2.164
batch start
#iterations: 224
currently lose_sum: 94.94681799411774
time_elpased: 2.18
batch start
#iterations: 225
currently lose_sum: 95.13066387176514
time_elpased: 2.149
batch start
#iterations: 226
currently lose_sum: 95.11923289299011
time_elpased: 2.145
batch start
#iterations: 227
currently lose_sum: 94.6392560005188
time_elpased: 2.218
batch start
#iterations: 228
currently lose_sum: 94.94571381807327
time_elpased: 2.047
batch start
#iterations: 229
currently lose_sum: 95.06106162071228
time_elpased: 2.098
batch start
#iterations: 230
currently lose_sum: 94.99895197153091
time_elpased: 2.138
batch start
#iterations: 231
currently lose_sum: 95.00811541080475
time_elpased: 2.157
batch start
#iterations: 232
currently lose_sum: 94.66584730148315
time_elpased: 2.17
batch start
#iterations: 233
currently lose_sum: 95.23874390125275
time_elpased: 2.138
batch start
#iterations: 234
currently lose_sum: 95.0172376036644
time_elpased: 2.181
batch start
#iterations: 235
currently lose_sum: 94.93103849887848
time_elpased: 2.152
batch start
#iterations: 236
currently lose_sum: 95.02082240581512
time_elpased: 2.127
batch start
#iterations: 237
currently lose_sum: 95.00137233734131
time_elpased: 2.041
batch start
#iterations: 238
currently lose_sum: 95.0860111117363
time_elpased: 2.077
batch start
#iterations: 239
currently lose_sum: 95.32699602842331
time_elpased: 2.133
start validation test
0.65587628866
0.64020285846
0.714520942678
0.675323412119
0.655773328924
61.228
batch start
#iterations: 240
currently lose_sum: 94.84344696998596
time_elpased: 2.144
batch start
#iterations: 241
currently lose_sum: 94.84754782915115
time_elpased: 2.129
batch start
#iterations: 242
currently lose_sum: 95.31806010007858
time_elpased: 2.165
batch start
#iterations: 243
currently lose_sum: 94.76072549819946
time_elpased: 2.151
batch start
#iterations: 244
currently lose_sum: 95.16431480646133
time_elpased: 2.164
batch start
#iterations: 245
currently lose_sum: 95.03154462575912
time_elpased: 2.155
batch start
#iterations: 246
currently lose_sum: 95.39450079202652
time_elpased: 2.054
batch start
#iterations: 247
currently lose_sum: 94.98518115282059
time_elpased: 2.071
batch start
#iterations: 248
currently lose_sum: 94.72425699234009
time_elpased: 2.13
batch start
#iterations: 249
currently lose_sum: 95.06539958715439
time_elpased: 2.178
batch start
#iterations: 250
currently lose_sum: 95.00730282068253
time_elpased: 2.156
batch start
#iterations: 251
currently lose_sum: 94.86925381422043
time_elpased: 2.136
batch start
#iterations: 252
currently lose_sum: 95.07353603839874
time_elpased: 2.183
batch start
#iterations: 253
currently lose_sum: 95.13094353675842
time_elpased: 2.171
batch start
#iterations: 254
currently lose_sum: 94.84916496276855
time_elpased: 2.13
batch start
#iterations: 255
currently lose_sum: 95.07517397403717
time_elpased: 2.06
batch start
#iterations: 256
currently lose_sum: 94.75400477647781
time_elpased: 2.046
batch start
#iterations: 257
currently lose_sum: 94.60884612798691
time_elpased: 2.106
batch start
#iterations: 258
currently lose_sum: 95.13131684064865
time_elpased: 2.144
batch start
#iterations: 259
currently lose_sum: 94.69727158546448
time_elpased: 2.122
start validation test
0.662216494845
0.639113612381
0.747967479675
0.689269287306
0.662065945765
61.002
batch start
#iterations: 260
currently lose_sum: 94.6140228509903
time_elpased: 2.2
batch start
#iterations: 261
currently lose_sum: 94.81298196315765
time_elpased: 2.129
batch start
#iterations: 262
currently lose_sum: 94.67002004384995
time_elpased: 2.132
batch start
#iterations: 263
currently lose_sum: 95.36461180448532
time_elpased: 2.322
batch start
#iterations: 264
currently lose_sum: 94.80427432060242
time_elpased: 2.105
batch start
#iterations: 265
currently lose_sum: 94.94793999195099
time_elpased: 2.016
batch start
#iterations: 266
currently lose_sum: 94.72685331106186
time_elpased: 2.12
batch start
#iterations: 267
currently lose_sum: 94.9237630367279
time_elpased: 2.135
batch start
#iterations: 268
currently lose_sum: 94.80319303274155
time_elpased: 2.114
batch start
#iterations: 269
currently lose_sum: 95.2724460363388
time_elpased: 2.106
batch start
#iterations: 270
currently lose_sum: 94.8342969417572
time_elpased: 2.109
batch start
#iterations: 271
currently lose_sum: 94.8102313876152
time_elpased: 2.116
batch start
#iterations: 272
currently lose_sum: 94.86225521564484
time_elpased: 2.126
batch start
#iterations: 273
currently lose_sum: 94.50866329669952
time_elpased: 2.137
batch start
#iterations: 274
currently lose_sum: 94.87373578548431
time_elpased: 1.957
batch start
#iterations: 275
currently lose_sum: 95.36314082145691
time_elpased: 2.124
batch start
#iterations: 276
currently lose_sum: 94.54772764444351
time_elpased: 2.141
batch start
#iterations: 277
currently lose_sum: 94.95851111412048
time_elpased: 2.137
batch start
#iterations: 278
currently lose_sum: 94.45261824131012
time_elpased: 2.163
batch start
#iterations: 279
currently lose_sum: 95.04776418209076
time_elpased: 2.091
start validation test
0.662216494845
0.642573900505
0.733765565504
0.685148705136
0.662090879416
61.183
batch start
#iterations: 280
currently lose_sum: 94.95039343833923
time_elpased: 2.12
batch start
#iterations: 281
currently lose_sum: 94.77135509252548
time_elpased: 2.114
batch start
#iterations: 282
currently lose_sum: 94.72028785943985
time_elpased: 1.989
batch start
#iterations: 283
currently lose_sum: 94.87448126077652
time_elpased: 1.96
batch start
#iterations: 284
currently lose_sum: 94.77116960287094
time_elpased: 1.984
batch start
#iterations: 285
currently lose_sum: 94.8220956325531
time_elpased: 1.956
batch start
#iterations: 286
currently lose_sum: 94.95498096942902
time_elpased: 1.99
batch start
#iterations: 287
currently lose_sum: 94.53125381469727
time_elpased: 2.099
batch start
#iterations: 288
currently lose_sum: 94.74463587999344
time_elpased: 2.113
batch start
#iterations: 289
currently lose_sum: 95.09807527065277
time_elpased: 2.193
batch start
#iterations: 290
currently lose_sum: 94.41309654712677
time_elpased: 2.106
batch start
#iterations: 291
currently lose_sum: 95.10403901338577
time_elpased: 2.141
batch start
#iterations: 292
currently lose_sum: 94.98217022418976
time_elpased: 2.177
batch start
#iterations: 293
currently lose_sum: 94.82746040821075
time_elpased: 2.179
batch start
#iterations: 294
currently lose_sum: 95.13990676403046
time_elpased: 2.081
batch start
#iterations: 295
currently lose_sum: 95.2757448554039
time_elpased: 2.154
batch start
#iterations: 296
currently lose_sum: 95.15353953838348
time_elpased: 2.175
batch start
#iterations: 297
currently lose_sum: 94.58281886577606
time_elpased: 2.123
batch start
#iterations: 298
currently lose_sum: 95.06438475847244
time_elpased: 2.159
batch start
#iterations: 299
currently lose_sum: 94.87548673152924
time_elpased: 2.045
start validation test
0.657989690722
0.641454011382
0.719152001647
0.678084517976
0.657882310851
61.108
batch start
#iterations: 300
currently lose_sum: 94.76513528823853
time_elpased: 2.166
batch start
#iterations: 301
currently lose_sum: 95.28554838895798
time_elpased: 2.099
batch start
#iterations: 302
currently lose_sum: 94.65375936031342
time_elpased: 2.093
batch start
#iterations: 303
currently lose_sum: 94.90053921937943
time_elpased: 2.068
batch start
#iterations: 304
currently lose_sum: 95.07166051864624
time_elpased: 2.125
batch start
#iterations: 305
currently lose_sum: 95.17245024442673
time_elpased: 2.12
batch start
#iterations: 306
currently lose_sum: 94.64483714103699
time_elpased: 2.139
batch start
#iterations: 307
currently lose_sum: 94.85427874326706
time_elpased: 2.127
batch start
#iterations: 308
currently lose_sum: 94.49089711904526
time_elpased: 2.1
batch start
#iterations: 309
currently lose_sum: 94.34734350442886
time_elpased: 2.124
batch start
#iterations: 310
currently lose_sum: 95.05920386314392
time_elpased: 2.169
batch start
#iterations: 311
currently lose_sum: 95.02771264314651
time_elpased: 2.032
batch start
#iterations: 312
currently lose_sum: 94.94482696056366
time_elpased: 2.174
batch start
#iterations: 313
currently lose_sum: 94.70887994766235
time_elpased: 2.12
batch start
#iterations: 314
currently lose_sum: 94.8971539735794
time_elpased: 2.195
batch start
#iterations: 315
currently lose_sum: 94.74515807628632
time_elpased: 2.162
batch start
#iterations: 316
currently lose_sum: 94.60768222808838
time_elpased: 2.072
batch start
#iterations: 317
currently lose_sum: 94.83870422840118
time_elpased: 2.056
batch start
#iterations: 318
currently lose_sum: 94.69372510910034
time_elpased: 2.133
batch start
#iterations: 319
currently lose_sum: 94.87488251924515
time_elpased: 2.105
start validation test
0.661082474227
0.644419930134
0.72141607492
0.680747754309
0.660976549285
61.139
batch start
#iterations: 320
currently lose_sum: 94.35956716537476
time_elpased: 2.191
batch start
#iterations: 321
currently lose_sum: 94.81603968143463
time_elpased: 2.121
batch start
#iterations: 322
currently lose_sum: 94.97785425186157
time_elpased: 2.145
batch start
#iterations: 323
currently lose_sum: 94.79612863063812
time_elpased: 2.116
batch start
#iterations: 324
currently lose_sum: 94.59661251306534
time_elpased: 2.193
batch start
#iterations: 325
currently lose_sum: 94.82312971353531
time_elpased: 2.007
batch start
#iterations: 326
currently lose_sum: 94.57377791404724
time_elpased: 2.124
batch start
#iterations: 327
currently lose_sum: 94.93044024705887
time_elpased: 2.117
batch start
#iterations: 328
currently lose_sum: 94.6676242351532
time_elpased: 2.081
batch start
#iterations: 329
currently lose_sum: 94.92447406053543
time_elpased: 2.162
batch start
#iterations: 330
currently lose_sum: 94.66686773300171
time_elpased: 2.125
batch start
#iterations: 331
currently lose_sum: 95.11840581893921
time_elpased: 2.141
batch start
#iterations: 332
currently lose_sum: 94.62318974733353
time_elpased: 2.137
batch start
#iterations: 333
currently lose_sum: 94.76412063837051
time_elpased: 2.107
batch start
#iterations: 334
currently lose_sum: 94.72495901584625
time_elpased: 2.044
batch start
#iterations: 335
currently lose_sum: 94.4168228507042
time_elpased: 2.15
batch start
#iterations: 336
currently lose_sum: 94.61041975021362
time_elpased: 2.096
batch start
#iterations: 337
currently lose_sum: 95.4715861082077
time_elpased: 2.035
batch start
#iterations: 338
currently lose_sum: 94.60613113641739
time_elpased: 2.051
batch start
#iterations: 339
currently lose_sum: 94.7387843132019
time_elpased: 1.991
start validation test
0.662474226804
0.638857243011
0.750231552948
0.690079515335
0.662320155282
61.189
batch start
#iterations: 340
currently lose_sum: 94.5965067744255
time_elpased: 2.049
batch start
#iterations: 341
currently lose_sum: 94.85382068157196
time_elpased: 1.982
batch start
#iterations: 342
currently lose_sum: 95.06346118450165
time_elpased: 2.092
batch start
#iterations: 343
currently lose_sum: 94.85201114416122
time_elpased: 2.082
batch start
#iterations: 344
currently lose_sum: 94.43695414066315
time_elpased: 2.11
batch start
#iterations: 345
currently lose_sum: 94.86081546545029
time_elpased: 2.138
batch start
#iterations: 346
currently lose_sum: 94.71544694900513
time_elpased: 2.126
batch start
#iterations: 347
currently lose_sum: 94.78831255435944
time_elpased: 2.022
batch start
#iterations: 348
currently lose_sum: 94.75548243522644
time_elpased: 2.175
batch start
#iterations: 349
currently lose_sum: 95.01137483119965
time_elpased: 2.113
batch start
#iterations: 350
currently lose_sum: 95.12596040964127
time_elpased: 2.086
batch start
#iterations: 351
currently lose_sum: 94.63219928741455
time_elpased: 2.145
batch start
#iterations: 352
currently lose_sum: 94.95900845527649
time_elpased: 2.132
batch start
#iterations: 353
currently lose_sum: 94.78346955776215
time_elpased: 2.136
batch start
#iterations: 354
currently lose_sum: 94.94535356760025
time_elpased: 2.127
batch start
#iterations: 355
currently lose_sum: 94.56942230463028
time_elpased: 2.124
batch start
#iterations: 356
currently lose_sum: 94.61505889892578
time_elpased: 2.12
batch start
#iterations: 357
currently lose_sum: 94.99069797992706
time_elpased: 2.113
batch start
#iterations: 358
currently lose_sum: 94.5819456577301
time_elpased: 2.102
batch start
#iterations: 359
currently lose_sum: 94.6072404384613
time_elpased: 2.113
start validation test
0.660154639175
0.644388981327
0.717402490481
0.678938397857
0.660054131742
61.215
batch start
#iterations: 360
currently lose_sum: 94.84834319353104
time_elpased: 2.068
batch start
#iterations: 361
currently lose_sum: 95.1195439696312
time_elpased: 2.118
batch start
#iterations: 362
currently lose_sum: 94.66837972402573
time_elpased: 2.214
batch start
#iterations: 363
currently lose_sum: 94.72341138124466
time_elpased: 2.112
batch start
#iterations: 364
currently lose_sum: 94.62269401550293
time_elpased: 2.222
batch start
#iterations: 365
currently lose_sum: 94.93430680036545
time_elpased: 2.069
batch start
#iterations: 366
currently lose_sum: 94.94244182109833
time_elpased: 2.107
batch start
#iterations: 367
currently lose_sum: 94.72874999046326
time_elpased: 2.09
batch start
#iterations: 368
currently lose_sum: 95.10125571489334
time_elpased: 2.209
batch start
#iterations: 369
currently lose_sum: 94.81061285734177
time_elpased: 2.118
batch start
#iterations: 370
currently lose_sum: 94.41686606407166
time_elpased: 2.191
batch start
#iterations: 371
currently lose_sum: 94.78693968057632
time_elpased: 2.169
batch start
#iterations: 372
currently lose_sum: 94.6697855591774
time_elpased: 2.152
batch start
#iterations: 373
currently lose_sum: 94.5914631485939
time_elpased: 2.121
batch start
#iterations: 374
currently lose_sum: 95.09649217128754
time_elpased: 2.063
batch start
#iterations: 375
currently lose_sum: 94.85962224006653
time_elpased: 2.065
batch start
#iterations: 376
currently lose_sum: 94.42895531654358
time_elpased: 2.15
batch start
#iterations: 377
currently lose_sum: 95.08415204286575
time_elpased: 2.122
batch start
#iterations: 378
currently lose_sum: 94.94069063663483
time_elpased: 2.175
batch start
#iterations: 379
currently lose_sum: 94.75290828943253
time_elpased: 2.158
start validation test
0.662422680412
0.640975436098
0.741175259854
0.687443325538
0.662284418112
61.159
batch start
#iterations: 380
currently lose_sum: 94.8873388171196
time_elpased: 2.209
batch start
#iterations: 381
currently lose_sum: 94.73882722854614
time_elpased: 2.173
batch start
#iterations: 382
currently lose_sum: 94.84413635730743
time_elpased: 2.072
batch start
#iterations: 383
currently lose_sum: 94.84365743398666
time_elpased: 2.067
batch start
#iterations: 384
currently lose_sum: 95.16112500429153
time_elpased: 2.072
batch start
#iterations: 385
currently lose_sum: 95.05553340911865
time_elpased: 2.159
batch start
#iterations: 386
currently lose_sum: 94.69681829214096
time_elpased: 2.141
batch start
#iterations: 387
currently lose_sum: 94.9026547074318
time_elpased: 2.12
batch start
#iterations: 388
currently lose_sum: 95.13279634714127
time_elpased: 2.164
batch start
#iterations: 389
currently lose_sum: 94.71383827924728
time_elpased: 2.127
batch start
#iterations: 390
currently lose_sum: 94.78995138406754
time_elpased: 2.196
batch start
#iterations: 391
currently lose_sum: 94.67177003622055
time_elpased: 2.105
batch start
#iterations: 392
currently lose_sum: 94.95781773328781
time_elpased: 2.099
batch start
#iterations: 393
currently lose_sum: 94.67735171318054
time_elpased: 2.084
batch start
#iterations: 394
currently lose_sum: 94.71713781356812
time_elpased: 2.217
batch start
#iterations: 395
currently lose_sum: 94.46625685691833
time_elpased: 2.15
batch start
#iterations: 396
currently lose_sum: 94.66790568828583
time_elpased: 2.177
batch start
#iterations: 397
currently lose_sum: 94.76011508703232
time_elpased: 2.143
batch start
#iterations: 398
currently lose_sum: 94.80412125587463
time_elpased: 2.172
batch start
#iterations: 399
currently lose_sum: 94.6074435710907
time_elpased: 2.125
start validation test
0.658659793814
0.640261035077
0.726973345683
0.68086746988
0.658539858837
61.199
acc: 0.655
pre: 0.634
rec: 0.737
F1: 0.682
auc: 0.655
