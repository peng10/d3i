start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.35667806863785
time_elpased: 2.004
batch start
#iterations: 1
currently lose_sum: 99.93907821178436
time_elpased: 1.848
batch start
#iterations: 2
currently lose_sum: 99.83587551116943
time_elpased: 1.897
batch start
#iterations: 3
currently lose_sum: 99.33876019716263
time_elpased: 1.888
batch start
#iterations: 4
currently lose_sum: 99.32548940181732
time_elpased: 1.878
batch start
#iterations: 5
currently lose_sum: 99.08250045776367
time_elpased: 1.853
batch start
#iterations: 6
currently lose_sum: 98.71758508682251
time_elpased: 1.904
batch start
#iterations: 7
currently lose_sum: 98.61919552087784
time_elpased: 1.872
batch start
#iterations: 8
currently lose_sum: 98.44082146883011
time_elpased: 1.85
batch start
#iterations: 9
currently lose_sum: 98.17679023742676
time_elpased: 1.877
batch start
#iterations: 10
currently lose_sum: 98.00973004102707
time_elpased: 1.886
batch start
#iterations: 11
currently lose_sum: 98.12477177381516
time_elpased: 1.877
batch start
#iterations: 12
currently lose_sum: 98.16806960105896
time_elpased: 1.874
batch start
#iterations: 13
currently lose_sum: 97.62585008144379
time_elpased: 1.871
batch start
#iterations: 14
currently lose_sum: 97.6733461022377
time_elpased: 1.962
batch start
#iterations: 15
currently lose_sum: 97.90533673763275
time_elpased: 1.888
batch start
#iterations: 16
currently lose_sum: 97.61097705364227
time_elpased: 1.873
batch start
#iterations: 17
currently lose_sum: 97.49420577287674
time_elpased: 1.864
batch start
#iterations: 18
currently lose_sum: 97.57684856653214
time_elpased: 1.861
batch start
#iterations: 19
currently lose_sum: 97.43266439437866
time_elpased: 1.876
start validation test
0.642474226804
0.640810126582
0.651229803437
0.645977950184
0.642458855039
63.030
batch start
#iterations: 20
currently lose_sum: 97.4713848233223
time_elpased: 1.922
batch start
#iterations: 21
currently lose_sum: 97.11753952503204
time_elpased: 1.877
batch start
#iterations: 22
currently lose_sum: 96.99831068515778
time_elpased: 1.851
batch start
#iterations: 23
currently lose_sum: 97.12977403402328
time_elpased: 1.861
batch start
#iterations: 24
currently lose_sum: 96.88853079080582
time_elpased: 1.839
batch start
#iterations: 25
currently lose_sum: 96.9571241736412
time_elpased: 1.853
batch start
#iterations: 26
currently lose_sum: 96.73712104558945
time_elpased: 1.848
batch start
#iterations: 27
currently lose_sum: 96.70386409759521
time_elpased: 1.838
batch start
#iterations: 28
currently lose_sum: 96.95360624790192
time_elpased: 1.853
batch start
#iterations: 29
currently lose_sum: 96.73929619789124
time_elpased: 1.867
batch start
#iterations: 30
currently lose_sum: 96.8013773560524
time_elpased: 1.842
batch start
#iterations: 31
currently lose_sum: 96.46950256824493
time_elpased: 1.861
batch start
#iterations: 32
currently lose_sum: 96.39896249771118
time_elpased: 1.858
batch start
#iterations: 33
currently lose_sum: 96.76266032457352
time_elpased: 1.901
batch start
#iterations: 34
currently lose_sum: 96.74714642763138
time_elpased: 1.835
batch start
#iterations: 35
currently lose_sum: 96.52240777015686
time_elpased: 1.867
batch start
#iterations: 36
currently lose_sum: 96.83206230401993
time_elpased: 1.878
batch start
#iterations: 37
currently lose_sum: 96.25548738241196
time_elpased: 1.852
batch start
#iterations: 38
currently lose_sum: 96.3871585726738
time_elpased: 1.914
batch start
#iterations: 39
currently lose_sum: 96.44522005319595
time_elpased: 1.869
start validation test
0.641030927835
0.63433199961
0.66893073994
0.651172109798
0.640981945412
62.390
batch start
#iterations: 40
currently lose_sum: 96.4159637093544
time_elpased: 1.969
batch start
#iterations: 41
currently lose_sum: 96.30421447753906
time_elpased: 1.875
batch start
#iterations: 42
currently lose_sum: 96.48079013824463
time_elpased: 1.865
batch start
#iterations: 43
currently lose_sum: 96.23256438970566
time_elpased: 1.9
batch start
#iterations: 44
currently lose_sum: 96.53802609443665
time_elpased: 1.872
batch start
#iterations: 45
currently lose_sum: 96.34092634916306
time_elpased: 1.916
batch start
#iterations: 46
currently lose_sum: 96.01442587375641
time_elpased: 1.872
batch start
#iterations: 47
currently lose_sum: 95.9048792719841
time_elpased: 1.838
batch start
#iterations: 48
currently lose_sum: 96.37829852104187
time_elpased: 1.876
batch start
#iterations: 49
currently lose_sum: 96.65770876407623
time_elpased: 1.884
batch start
#iterations: 50
currently lose_sum: 96.08621227741241
time_elpased: 1.912
batch start
#iterations: 51
currently lose_sum: 96.05017340183258
time_elpased: 1.985
batch start
#iterations: 52
currently lose_sum: 96.02322906255722
time_elpased: 1.916
batch start
#iterations: 53
currently lose_sum: 96.26416230201721
time_elpased: 2.078
batch start
#iterations: 54
currently lose_sum: 96.25548231601715
time_elpased: 1.893
batch start
#iterations: 55
currently lose_sum: 96.17550253868103
time_elpased: 1.867
batch start
#iterations: 56
currently lose_sum: 96.18988126516342
time_elpased: 1.903
batch start
#iterations: 57
currently lose_sum: 95.83723551034927
time_elpased: 1.951
batch start
#iterations: 58
currently lose_sum: 96.14910864830017
time_elpased: 1.892
batch start
#iterations: 59
currently lose_sum: 96.24559861421585
time_elpased: 1.855
start validation test
0.647886597938
0.633167220377
0.706082124112
0.667639760619
0.647784426716
62.262
batch start
#iterations: 60
currently lose_sum: 96.21432721614838
time_elpased: 1.89
batch start
#iterations: 61
currently lose_sum: 96.29594057798386
time_elpased: 1.857
batch start
#iterations: 62
currently lose_sum: 96.13899278640747
time_elpased: 1.861
batch start
#iterations: 63
currently lose_sum: 96.09792119264603
time_elpased: 1.905
batch start
#iterations: 64
currently lose_sum: 96.29464972019196
time_elpased: 1.857
batch start
#iterations: 65
currently lose_sum: 95.66776406764984
time_elpased: 1.876
batch start
#iterations: 66
currently lose_sum: 95.58867597579956
time_elpased: 1.872
batch start
#iterations: 67
currently lose_sum: 95.95431196689606
time_elpased: 1.85
batch start
#iterations: 68
currently lose_sum: 96.51715821027756
time_elpased: 1.868
batch start
#iterations: 69
currently lose_sum: 95.69230860471725
time_elpased: 1.869
batch start
#iterations: 70
currently lose_sum: 95.71769618988037
time_elpased: 1.882
batch start
#iterations: 71
currently lose_sum: 95.6338460445404
time_elpased: 1.878
batch start
#iterations: 72
currently lose_sum: 95.33739131689072
time_elpased: 1.869
batch start
#iterations: 73
currently lose_sum: 95.50291538238525
time_elpased: 1.894
batch start
#iterations: 74
currently lose_sum: 95.69654136896133
time_elpased: 1.844
batch start
#iterations: 75
currently lose_sum: 95.57836425304413
time_elpased: 1.859
batch start
#iterations: 76
currently lose_sum: 95.53699773550034
time_elpased: 1.872
batch start
#iterations: 77
currently lose_sum: 95.77332180738449
time_elpased: 1.89
batch start
#iterations: 78
currently lose_sum: 95.69094461202621
time_elpased: 1.885
batch start
#iterations: 79
currently lose_sum: 95.70163786411285
time_elpased: 1.86
start validation test
0.644587628866
0.638333333333
0.670062776577
0.6538133253
0.644542903315
62.019
batch start
#iterations: 80
currently lose_sum: 95.90761715173721
time_elpased: 1.891
batch start
#iterations: 81
currently lose_sum: 95.57184684276581
time_elpased: 1.874
batch start
#iterations: 82
currently lose_sum: 95.80643355846405
time_elpased: 1.877
batch start
#iterations: 83
currently lose_sum: 96.08035308122635
time_elpased: 1.851
batch start
#iterations: 84
currently lose_sum: 95.8552730679512
time_elpased: 1.856
batch start
#iterations: 85
currently lose_sum: 95.6156997680664
time_elpased: 1.895
batch start
#iterations: 86
currently lose_sum: 95.8036716580391
time_elpased: 1.9
batch start
#iterations: 87
currently lose_sum: 95.37146353721619
time_elpased: 1.887
batch start
#iterations: 88
currently lose_sum: 95.52753406763077
time_elpased: 1.864
batch start
#iterations: 89
currently lose_sum: 95.68479877710342
time_elpased: 1.917
batch start
#iterations: 90
currently lose_sum: 95.89736646413803
time_elpased: 1.914
batch start
#iterations: 91
currently lose_sum: 95.39427649974823
time_elpased: 1.907
batch start
#iterations: 92
currently lose_sum: 95.70605731010437
time_elpased: 1.843
batch start
#iterations: 93
currently lose_sum: 95.54018378257751
time_elpased: 1.862
batch start
#iterations: 94
currently lose_sum: 95.76925045251846
time_elpased: 1.896
batch start
#iterations: 95
currently lose_sum: 95.34505939483643
time_elpased: 1.94
batch start
#iterations: 96
currently lose_sum: 95.7214115858078
time_elpased: 1.927
batch start
#iterations: 97
currently lose_sum: 95.54882681369781
time_elpased: 1.967
batch start
#iterations: 98
currently lose_sum: 95.40282380580902
time_elpased: 1.948
batch start
#iterations: 99
currently lose_sum: 95.71981769800186
time_elpased: 1.858
start validation test
0.646030927835
0.63496874408
0.689924873932
0.66130702836
0.645953865243
61.927
batch start
#iterations: 100
currently lose_sum: 95.66374331712723
time_elpased: 1.939
batch start
#iterations: 101
currently lose_sum: 95.4604179263115
time_elpased: 1.889
batch start
#iterations: 102
currently lose_sum: 95.49391424655914
time_elpased: 1.854
batch start
#iterations: 103
currently lose_sum: 95.23717081546783
time_elpased: 1.899
batch start
#iterations: 104
currently lose_sum: 95.23165065050125
time_elpased: 1.885
batch start
#iterations: 105
currently lose_sum: 95.54610359668732
time_elpased: 1.894
batch start
#iterations: 106
currently lose_sum: 95.82581949234009
time_elpased: 1.855
batch start
#iterations: 107
currently lose_sum: 95.2285185456276
time_elpased: 1.901
batch start
#iterations: 108
currently lose_sum: 95.2655656337738
time_elpased: 1.887
batch start
#iterations: 109
currently lose_sum: 95.7433602809906
time_elpased: 1.891
batch start
#iterations: 110
currently lose_sum: 95.21414840221405
time_elpased: 1.869
batch start
#iterations: 111
currently lose_sum: 95.16428756713867
time_elpased: 1.858
batch start
#iterations: 112
currently lose_sum: 95.2473309636116
time_elpased: 1.957
batch start
#iterations: 113
currently lose_sum: 95.14128684997559
time_elpased: 1.85
batch start
#iterations: 114
currently lose_sum: 95.57437032461166
time_elpased: 1.888
batch start
#iterations: 115
currently lose_sum: 95.47444343566895
time_elpased: 1.885
batch start
#iterations: 116
currently lose_sum: 95.38384884595871
time_elpased: 1.896
batch start
#iterations: 117
currently lose_sum: 95.41639703512192
time_elpased: 1.892
batch start
#iterations: 118
currently lose_sum: 95.47987675666809
time_elpased: 1.879
batch start
#iterations: 119
currently lose_sum: 95.31154042482376
time_elpased: 1.917
start validation test
0.651237113402
0.642877892902
0.683235566533
0.662442626222
0.651180935182
61.933
batch start
#iterations: 120
currently lose_sum: 95.52565103769302
time_elpased: 1.972
batch start
#iterations: 121
currently lose_sum: 95.43631780147552
time_elpased: 1.911
batch start
#iterations: 122
currently lose_sum: 95.32835984230042
time_elpased: 1.897
batch start
#iterations: 123
currently lose_sum: 95.51655000448227
time_elpased: 1.847
batch start
#iterations: 124
currently lose_sum: 95.79906380176544
time_elpased: 1.889
batch start
#iterations: 125
currently lose_sum: 95.45067977905273
time_elpased: 1.876
batch start
#iterations: 126
currently lose_sum: 95.43277657032013
time_elpased: 1.847
batch start
#iterations: 127
currently lose_sum: 95.4336302280426
time_elpased: 1.861
batch start
#iterations: 128
currently lose_sum: 95.63301700353622
time_elpased: 1.875
batch start
#iterations: 129
currently lose_sum: 95.29622846841812
time_elpased: 1.856
batch start
#iterations: 130
currently lose_sum: 95.05303931236267
time_elpased: 1.911
batch start
#iterations: 131
currently lose_sum: 95.52744251489639
time_elpased: 1.913
batch start
#iterations: 132
currently lose_sum: 95.38032573461533
time_elpased: 1.885
batch start
#iterations: 133
currently lose_sum: 95.26542216539383
time_elpased: 1.865
batch start
#iterations: 134
currently lose_sum: 95.19026291370392
time_elpased: 1.939
batch start
#iterations: 135
currently lose_sum: 95.73870176076889
time_elpased: 1.923
batch start
#iterations: 136
currently lose_sum: 95.22248804569244
time_elpased: 1.887
batch start
#iterations: 137
currently lose_sum: 94.72791028022766
time_elpased: 1.897
batch start
#iterations: 138
currently lose_sum: 95.56050163507462
time_elpased: 1.917
batch start
#iterations: 139
currently lose_sum: 95.46162486076355
time_elpased: 1.906
start validation test
0.653556701031
0.643184859491
0.692497684471
0.666930967838
0.653488334128
61.594
batch start
#iterations: 140
currently lose_sum: 95.44166445732117
time_elpased: 1.961
batch start
#iterations: 141
currently lose_sum: 95.2202672958374
time_elpased: 1.879
batch start
#iterations: 142
currently lose_sum: 95.20709764957428
time_elpased: 1.875
batch start
#iterations: 143
currently lose_sum: 95.13645780086517
time_elpased: 1.851
batch start
#iterations: 144
currently lose_sum: 95.19818753004074
time_elpased: 1.904
batch start
#iterations: 145
currently lose_sum: 95.26200026273727
time_elpased: 1.935
batch start
#iterations: 146
currently lose_sum: 95.50026226043701
time_elpased: 1.83
batch start
#iterations: 147
currently lose_sum: 95.32243943214417
time_elpased: 1.863
batch start
#iterations: 148
currently lose_sum: 95.28911256790161
time_elpased: 1.884
batch start
#iterations: 149
currently lose_sum: 95.28056049346924
time_elpased: 1.921
batch start
#iterations: 150
currently lose_sum: 95.3970330953598
time_elpased: 1.942
batch start
#iterations: 151
currently lose_sum: 95.11773145198822
time_elpased: 1.93
batch start
#iterations: 152
currently lose_sum: 95.23082858324051
time_elpased: 1.966
batch start
#iterations: 153
currently lose_sum: 95.44545894861221
time_elpased: 1.875
batch start
#iterations: 154
currently lose_sum: 95.16629260778427
time_elpased: 1.861
batch start
#iterations: 155
currently lose_sum: 95.33734506368637
time_elpased: 1.9
batch start
#iterations: 156
currently lose_sum: 95.1956433057785
time_elpased: 1.906
batch start
#iterations: 157
currently lose_sum: 95.31781595945358
time_elpased: 1.873
batch start
#iterations: 158
currently lose_sum: 95.18126660585403
time_elpased: 1.883
batch start
#iterations: 159
currently lose_sum: 95.28060799837112
time_elpased: 1.881
start validation test
0.649845360825
0.63784650198
0.696202531646
0.665748167101
0.649763973661
61.674
batch start
#iterations: 160
currently lose_sum: 95.37169426679611
time_elpased: 1.975
batch start
#iterations: 161
currently lose_sum: 95.0078597664833
time_elpased: 1.853
batch start
#iterations: 162
currently lose_sum: 95.21702855825424
time_elpased: 1.878
batch start
#iterations: 163
currently lose_sum: 95.38192760944366
time_elpased: 1.91
batch start
#iterations: 164
currently lose_sum: 95.0976951122284
time_elpased: 1.889
batch start
#iterations: 165
currently lose_sum: 95.08866763114929
time_elpased: 1.898
batch start
#iterations: 166
currently lose_sum: 95.4897792339325
time_elpased: 1.904
batch start
#iterations: 167
currently lose_sum: 95.10073280334473
time_elpased: 1.858
batch start
#iterations: 168
currently lose_sum: 94.92322218418121
time_elpased: 1.867
batch start
#iterations: 169
currently lose_sum: 95.18642276525497
time_elpased: 1.85
batch start
#iterations: 170
currently lose_sum: 95.08184844255447
time_elpased: 1.914
batch start
#iterations: 171
currently lose_sum: 95.01022070646286
time_elpased: 1.879
batch start
#iterations: 172
currently lose_sum: 94.85583424568176
time_elpased: 1.911
batch start
#iterations: 173
currently lose_sum: 94.84277391433716
time_elpased: 1.972
batch start
#iterations: 174
currently lose_sum: 95.22791743278503
time_elpased: 1.872
batch start
#iterations: 175
currently lose_sum: 95.10679012537003
time_elpased: 1.871
batch start
#iterations: 176
currently lose_sum: 95.57015293836594
time_elpased: 1.909
batch start
#iterations: 177
currently lose_sum: 95.23401468992233
time_elpased: 1.858
batch start
#iterations: 178
currently lose_sum: 94.94484555721283
time_elpased: 1.84
batch start
#iterations: 179
currently lose_sum: 94.8996519446373
time_elpased: 1.865
start validation test
0.653144329897
0.639861449167
0.703406401153
0.670130888769
0.653056087078
61.756
batch start
#iterations: 180
currently lose_sum: 95.06429249048233
time_elpased: 1.853
batch start
#iterations: 181
currently lose_sum: 95.02894413471222
time_elpased: 1.849
batch start
#iterations: 182
currently lose_sum: 95.2195953130722
time_elpased: 1.832
batch start
#iterations: 183
currently lose_sum: 94.50653177499771
time_elpased: 1.859
batch start
#iterations: 184
currently lose_sum: 95.10417157411575
time_elpased: 1.884
batch start
#iterations: 185
currently lose_sum: 94.94947892427444
time_elpased: 1.879
batch start
#iterations: 186
currently lose_sum: 94.97317051887512
time_elpased: 1.915
batch start
#iterations: 187
currently lose_sum: 94.78530299663544
time_elpased: 1.839
batch start
#iterations: 188
currently lose_sum: 94.82114398479462
time_elpased: 1.868
batch start
#iterations: 189
currently lose_sum: 94.9050921201706
time_elpased: 1.873
batch start
#iterations: 190
currently lose_sum: 94.84942692518234
time_elpased: 1.867
batch start
#iterations: 191
currently lose_sum: 95.18225318193436
time_elpased: 1.875
batch start
#iterations: 192
currently lose_sum: 94.57695317268372
time_elpased: 1.885
batch start
#iterations: 193
currently lose_sum: 94.84522408246994
time_elpased: 1.963
batch start
#iterations: 194
currently lose_sum: 94.71708983182907
time_elpased: 1.929
batch start
#iterations: 195
currently lose_sum: 95.08507716655731
time_elpased: 1.982
batch start
#iterations: 196
currently lose_sum: 94.95388472080231
time_elpased: 1.948
batch start
#iterations: 197
currently lose_sum: 95.19192343950272
time_elpased: 1.862
batch start
#iterations: 198
currently lose_sum: 95.2013977766037
time_elpased: 1.888
batch start
#iterations: 199
currently lose_sum: 95.36723345518112
time_elpased: 1.941
start validation test
0.65381443299
0.641436516166
0.700319028507
0.669585752239
0.653732786999
61.823
batch start
#iterations: 200
currently lose_sum: 94.9641410112381
time_elpased: 1.924
batch start
#iterations: 201
currently lose_sum: 94.99060654640198
time_elpased: 1.93
batch start
#iterations: 202
currently lose_sum: 95.13765186071396
time_elpased: 1.903
batch start
#iterations: 203
currently lose_sum: 95.13986307382584
time_elpased: 1.902
batch start
#iterations: 204
currently lose_sum: 94.80670112371445
time_elpased: 1.943
batch start
#iterations: 205
currently lose_sum: 94.8103586435318
time_elpased: 1.873
batch start
#iterations: 206
currently lose_sum: 94.76018959283829
time_elpased: 1.884
batch start
#iterations: 207
currently lose_sum: 94.95892691612244
time_elpased: 1.888
batch start
#iterations: 208
currently lose_sum: 94.86038464307785
time_elpased: 1.871
batch start
#iterations: 209
currently lose_sum: 95.08710718154907
time_elpased: 1.842
batch start
#iterations: 210
currently lose_sum: 94.71589350700378
time_elpased: 1.937
batch start
#iterations: 211
currently lose_sum: 94.77536123991013
time_elpased: 1.876
batch start
#iterations: 212
currently lose_sum: 94.6445837020874
time_elpased: 1.904
batch start
#iterations: 213
currently lose_sum: 94.7206563949585
time_elpased: 1.89
batch start
#iterations: 214
currently lose_sum: 95.10422194004059
time_elpased: 1.913
batch start
#iterations: 215
currently lose_sum: 95.04861253499985
time_elpased: 1.892
batch start
#iterations: 216
currently lose_sum: 95.00700837373734
time_elpased: 1.866
batch start
#iterations: 217
currently lose_sum: 94.7296804189682
time_elpased: 1.964
batch start
#iterations: 218
currently lose_sum: 95.30702126026154
time_elpased: 1.872
batch start
#iterations: 219
currently lose_sum: 95.08961468935013
time_elpased: 1.869
start validation test
0.653711340206
0.642823126012
0.694555932901
0.66768895924
0.653639631224
61.602
batch start
#iterations: 220
currently lose_sum: 95.11324208974838
time_elpased: 1.876
batch start
#iterations: 221
currently lose_sum: 94.87074482440948
time_elpased: 1.914
batch start
#iterations: 222
currently lose_sum: 95.0216954946518
time_elpased: 1.865
batch start
#iterations: 223
currently lose_sum: 95.18025875091553
time_elpased: 1.858
batch start
#iterations: 224
currently lose_sum: 95.09798675775528
time_elpased: 1.882
batch start
#iterations: 225
currently lose_sum: 94.73541420698166
time_elpased: 1.886
batch start
#iterations: 226
currently lose_sum: 94.88450229167938
time_elpased: 1.892
batch start
#iterations: 227
currently lose_sum: 94.90322810411453
time_elpased: 1.907
batch start
#iterations: 228
currently lose_sum: 94.7988874912262
time_elpased: 1.851
batch start
#iterations: 229
currently lose_sum: 95.26712775230408
time_elpased: 1.867
batch start
#iterations: 230
currently lose_sum: 94.23984998464584
time_elpased: 1.893
batch start
#iterations: 231
currently lose_sum: 94.47467106580734
time_elpased: 1.858
batch start
#iterations: 232
currently lose_sum: 94.94109857082367
time_elpased: 1.856
batch start
#iterations: 233
currently lose_sum: 95.36919689178467
time_elpased: 1.863
batch start
#iterations: 234
currently lose_sum: 94.86793196201324
time_elpased: 1.861
batch start
#iterations: 235
currently lose_sum: 94.97149497270584
time_elpased: 1.885
batch start
#iterations: 236
currently lose_sum: 95.21007543802261
time_elpased: 1.883
batch start
#iterations: 237
currently lose_sum: 94.67655998468399
time_elpased: 1.898
batch start
#iterations: 238
currently lose_sum: 95.06003135442734
time_elpased: 1.877
batch start
#iterations: 239
currently lose_sum: 94.70379024744034
time_elpased: 1.872
start validation test
0.652989690722
0.64438425075
0.685499639807
0.664306372793
0.652932614492
61.536
batch start
#iterations: 240
currently lose_sum: 94.9187119603157
time_elpased: 1.969
batch start
#iterations: 241
currently lose_sum: 95.00078737735748
time_elpased: 1.834
batch start
#iterations: 242
currently lose_sum: 94.51419907808304
time_elpased: 1.917
batch start
#iterations: 243
currently lose_sum: 94.73140352964401
time_elpased: 1.832
batch start
#iterations: 244
currently lose_sum: 95.09514355659485
time_elpased: 1.922
batch start
#iterations: 245
currently lose_sum: 95.33924204111099
time_elpased: 1.87
batch start
#iterations: 246
currently lose_sum: 94.87845534086227
time_elpased: 1.951
batch start
#iterations: 247
currently lose_sum: 94.88108325004578
time_elpased: 1.874
batch start
#iterations: 248
currently lose_sum: 95.41344451904297
time_elpased: 1.875
batch start
#iterations: 249
currently lose_sum: 95.02464163303375
time_elpased: 1.869
batch start
#iterations: 250
currently lose_sum: 94.88033974170685
time_elpased: 1.927
batch start
#iterations: 251
currently lose_sum: 94.69213455915451
time_elpased: 1.915
batch start
#iterations: 252
currently lose_sum: 94.95236724615097
time_elpased: 1.927
batch start
#iterations: 253
currently lose_sum: 95.20501339435577
time_elpased: 1.995
batch start
#iterations: 254
currently lose_sum: 94.67101067304611
time_elpased: 1.892
batch start
#iterations: 255
currently lose_sum: 94.66415721178055
time_elpased: 1.867
batch start
#iterations: 256
currently lose_sum: 94.53011691570282
time_elpased: 1.879
batch start
#iterations: 257
currently lose_sum: 94.77553176879883
time_elpased: 1.846
batch start
#iterations: 258
currently lose_sum: 94.74914038181305
time_elpased: 1.925
batch start
#iterations: 259
currently lose_sum: 94.82964384555817
time_elpased: 1.874
start validation test
0.652628865979
0.639733483483
0.701553977565
0.669219064448
0.6525429704
61.633
batch start
#iterations: 260
currently lose_sum: 94.85735380649567
time_elpased: 1.929
batch start
#iterations: 261
currently lose_sum: 94.77885752916336
time_elpased: 1.839
batch start
#iterations: 262
currently lose_sum: 94.5578624010086
time_elpased: 1.861
batch start
#iterations: 263
currently lose_sum: 94.84422594308853
time_elpased: 1.849
batch start
#iterations: 264
currently lose_sum: 94.74349790811539
time_elpased: 1.871
batch start
#iterations: 265
currently lose_sum: 94.91418069601059
time_elpased: 1.908
batch start
#iterations: 266
currently lose_sum: 94.71704351902008
time_elpased: 1.908
batch start
#iterations: 267
currently lose_sum: 94.60572642087936
time_elpased: 1.879
batch start
#iterations: 268
currently lose_sum: 94.58212071657181
time_elpased: 1.959
batch start
#iterations: 269
currently lose_sum: 94.92163789272308
time_elpased: 1.876
batch start
#iterations: 270
currently lose_sum: 95.24918919801712
time_elpased: 1.867
batch start
#iterations: 271
currently lose_sum: 94.71068060398102
time_elpased: 1.879
batch start
#iterations: 272
currently lose_sum: 95.0170007944107
time_elpased: 1.94
batch start
#iterations: 273
currently lose_sum: 94.53111898899078
time_elpased: 1.841
batch start
#iterations: 274
currently lose_sum: 94.74566048383713
time_elpased: 1.921
batch start
#iterations: 275
currently lose_sum: 94.97788989543915
time_elpased: 1.915
batch start
#iterations: 276
currently lose_sum: 95.42114359140396
time_elpased: 1.897
batch start
#iterations: 277
currently lose_sum: 95.18542438745499
time_elpased: 1.873
batch start
#iterations: 278
currently lose_sum: 94.67644512653351
time_elpased: 1.87
batch start
#iterations: 279
currently lose_sum: 95.14386290311813
time_elpased: 1.88
start validation test
0.643969072165
0.635801275855
0.67695790882
0.655734436525
0.643911155174
61.708
batch start
#iterations: 280
currently lose_sum: 94.71023273468018
time_elpased: 1.939
batch start
#iterations: 281
currently lose_sum: 95.00938510894775
time_elpased: 1.869
batch start
#iterations: 282
currently lose_sum: 94.80455017089844
time_elpased: 1.875
batch start
#iterations: 283
currently lose_sum: 95.01942074298859
time_elpased: 1.87
batch start
#iterations: 284
currently lose_sum: 94.56789571046829
time_elpased: 1.956
batch start
#iterations: 285
currently lose_sum: 95.04190146923065
time_elpased: 1.841
batch start
#iterations: 286
currently lose_sum: 94.88623207807541
time_elpased: 1.866
batch start
#iterations: 287
currently lose_sum: 94.6232460141182
time_elpased: 1.864
batch start
#iterations: 288
currently lose_sum: 94.89770048856735
time_elpased: 1.86
batch start
#iterations: 289
currently lose_sum: 94.71377885341644
time_elpased: 1.863
batch start
#iterations: 290
currently lose_sum: 94.70363992452621
time_elpased: 1.845
batch start
#iterations: 291
currently lose_sum: 94.65230029821396
time_elpased: 1.874
batch start
#iterations: 292
currently lose_sum: 94.60823154449463
time_elpased: 1.867
batch start
#iterations: 293
currently lose_sum: 94.59550160169601
time_elpased: 1.892
batch start
#iterations: 294
currently lose_sum: 94.69267696142197
time_elpased: 1.851
batch start
#iterations: 295
currently lose_sum: 94.46913808584213
time_elpased: 1.879
batch start
#iterations: 296
currently lose_sum: 94.77761906385422
time_elpased: 1.907
batch start
#iterations: 297
currently lose_sum: 94.74297171831131
time_elpased: 1.862
batch start
#iterations: 298
currently lose_sum: 94.77118647098541
time_elpased: 1.915
batch start
#iterations: 299
currently lose_sum: 94.27019739151001
time_elpased: 1.895
start validation test
0.656237113402
0.638797814208
0.721827724606
0.677779388317
0.656121958967
61.546
batch start
#iterations: 300
currently lose_sum: 94.64550042152405
time_elpased: 1.908
batch start
#iterations: 301
currently lose_sum: 95.1615886092186
time_elpased: 1.859
batch start
#iterations: 302
currently lose_sum: 94.66955626010895
time_elpased: 1.89
batch start
#iterations: 303
currently lose_sum: 94.51633512973785
time_elpased: 1.848
batch start
#iterations: 304
currently lose_sum: 94.61196529865265
time_elpased: 1.877
batch start
#iterations: 305
currently lose_sum: 94.92407435178757
time_elpased: 1.872
batch start
#iterations: 306
currently lose_sum: 94.90676444768906
time_elpased: 1.918
batch start
#iterations: 307
currently lose_sum: 94.49296951293945
time_elpased: 1.877
batch start
#iterations: 308
currently lose_sum: 94.632149040699
time_elpased: 2.013
batch start
#iterations: 309
currently lose_sum: 94.4678755402565
time_elpased: 1.893
batch start
#iterations: 310
currently lose_sum: 94.95754826068878
time_elpased: 1.9
batch start
#iterations: 311
currently lose_sum: 94.65018022060394
time_elpased: 1.974
batch start
#iterations: 312
currently lose_sum: 94.52260428667068
time_elpased: 1.877
batch start
#iterations: 313
currently lose_sum: 94.42652547359467
time_elpased: 1.857
batch start
#iterations: 314
currently lose_sum: 94.64031380414963
time_elpased: 1.903
batch start
#iterations: 315
currently lose_sum: 94.97175294160843
time_elpased: 1.857
batch start
#iterations: 316
currently lose_sum: 95.00439411401749
time_elpased: 1.904
batch start
#iterations: 317
currently lose_sum: 94.81758683919907
time_elpased: 1.886
batch start
#iterations: 318
currently lose_sum: 94.72659850120544
time_elpased: 1.878
batch start
#iterations: 319
currently lose_sum: 94.66855150461197
time_elpased: 1.855
start validation test
0.652886597938
0.642877670275
0.690645260883
0.665905933717
0.652820306781
61.583
batch start
#iterations: 320
currently lose_sum: 94.50747913122177
time_elpased: 1.926
batch start
#iterations: 321
currently lose_sum: 94.84643012285233
time_elpased: 1.875
batch start
#iterations: 322
currently lose_sum: 95.0109111070633
time_elpased: 1.909
batch start
#iterations: 323
currently lose_sum: 94.82428377866745
time_elpased: 1.874
batch start
#iterations: 324
currently lose_sum: 95.10346788167953
time_elpased: 1.885
batch start
#iterations: 325
currently lose_sum: 94.72989064455032
time_elpased: 1.913
batch start
#iterations: 326
currently lose_sum: 94.62854129076004
time_elpased: 1.915
batch start
#iterations: 327
currently lose_sum: 94.60853952169418
time_elpased: 1.9
batch start
#iterations: 328
currently lose_sum: 94.22961127758026
time_elpased: 1.906
batch start
#iterations: 329
currently lose_sum: 95.04548186063766
time_elpased: 1.846
batch start
#iterations: 330
currently lose_sum: 94.4708142876625
time_elpased: 1.869
batch start
#iterations: 331
currently lose_sum: 94.37997847795486
time_elpased: 1.872
batch start
#iterations: 332
currently lose_sum: 94.53911018371582
time_elpased: 1.865
batch start
#iterations: 333
currently lose_sum: 94.36355137825012
time_elpased: 1.874
batch start
#iterations: 334
currently lose_sum: 95.02333927154541
time_elpased: 1.871
batch start
#iterations: 335
currently lose_sum: 94.7152447104454
time_elpased: 1.884
batch start
#iterations: 336
currently lose_sum: 94.8430849313736
time_elpased: 1.843
batch start
#iterations: 337
currently lose_sum: 94.84779405593872
time_elpased: 1.834
batch start
#iterations: 338
currently lose_sum: 94.5392736196518
time_elpased: 1.904
batch start
#iterations: 339
currently lose_sum: 94.36769580841064
time_elpased: 1.875
start validation test
0.657371134021
0.644456992283
0.704744262632
0.673253699061
0.657287963187
61.476
batch start
#iterations: 340
currently lose_sum: 94.655781686306
time_elpased: 2.003
batch start
#iterations: 341
currently lose_sum: 94.58070123195648
time_elpased: 1.875
batch start
#iterations: 342
currently lose_sum: 94.71447044610977
time_elpased: 1.908
batch start
#iterations: 343
currently lose_sum: 94.61418378353119
time_elpased: 1.842
batch start
#iterations: 344
currently lose_sum: 94.56770330667496
time_elpased: 1.878
batch start
#iterations: 345
currently lose_sum: 94.74439573287964
time_elpased: 1.883
batch start
#iterations: 346
currently lose_sum: 94.87057566642761
time_elpased: 1.927
batch start
#iterations: 347
currently lose_sum: 94.91946464776993
time_elpased: 1.874
batch start
#iterations: 348
currently lose_sum: 94.7516719698906
time_elpased: 1.962
batch start
#iterations: 349
currently lose_sum: 94.64494013786316
time_elpased: 1.856
batch start
#iterations: 350
currently lose_sum: 94.71697950363159
time_elpased: 1.864
batch start
#iterations: 351
currently lose_sum: 94.69009631872177
time_elpased: 1.856
batch start
#iterations: 352
currently lose_sum: 94.65412503480911
time_elpased: 1.894
batch start
#iterations: 353
currently lose_sum: 94.88588154315948
time_elpased: 1.86
batch start
#iterations: 354
currently lose_sum: 94.70943140983582
time_elpased: 1.974
batch start
#iterations: 355
currently lose_sum: 94.76306712627411
time_elpased: 1.911
batch start
#iterations: 356
currently lose_sum: 94.5029296875
time_elpased: 1.975
batch start
#iterations: 357
currently lose_sum: 95.02406108379364
time_elpased: 1.83
batch start
#iterations: 358
currently lose_sum: 94.5713557600975
time_elpased: 1.898
batch start
#iterations: 359
currently lose_sum: 94.70267468690872
time_elpased: 1.881
start validation test
0.654072164948
0.638960798817
0.711227745189
0.673160278576
0.653971819512
61.677
batch start
#iterations: 360
currently lose_sum: 94.84854084253311
time_elpased: 1.906
batch start
#iterations: 361
currently lose_sum: 94.7442512512207
time_elpased: 1.849
batch start
#iterations: 362
currently lose_sum: 94.60073816776276
time_elpased: 1.878
batch start
#iterations: 363
currently lose_sum: 94.67167043685913
time_elpased: 1.857
batch start
#iterations: 364
currently lose_sum: 95.57194298505783
time_elpased: 1.913
batch start
#iterations: 365
currently lose_sum: 94.46377491950989
time_elpased: 1.855
batch start
#iterations: 366
currently lose_sum: 94.76246297359467
time_elpased: 1.872
batch start
#iterations: 367
currently lose_sum: 94.88739520311356
time_elpased: 1.907
batch start
#iterations: 368
currently lose_sum: 94.96190196275711
time_elpased: 1.888
batch start
#iterations: 369
currently lose_sum: 94.48689514398575
time_elpased: 1.838
batch start
#iterations: 370
currently lose_sum: 94.3624119758606
time_elpased: 1.875
batch start
#iterations: 371
currently lose_sum: 94.66877466440201
time_elpased: 1.86
batch start
#iterations: 372
currently lose_sum: 94.96555936336517
time_elpased: 1.9
batch start
#iterations: 373
currently lose_sum: 94.32730466127396
time_elpased: 1.902
batch start
#iterations: 374
currently lose_sum: 94.49166864156723
time_elpased: 1.853
batch start
#iterations: 375
currently lose_sum: 94.91779261827469
time_elpased: 1.89
batch start
#iterations: 376
currently lose_sum: 94.30494517087936
time_elpased: 1.886
batch start
#iterations: 377
currently lose_sum: 94.60304468870163
time_elpased: 1.855
batch start
#iterations: 378
currently lose_sum: 94.55978208780289
time_elpased: 1.886
batch start
#iterations: 379
currently lose_sum: 94.77463567256927
time_elpased: 1.846
start validation test
0.651288659794
0.640732265446
0.691571472677
0.665181885672
0.651217937103
61.763
batch start
#iterations: 380
currently lose_sum: 94.32631021738052
time_elpased: 1.921
batch start
#iterations: 381
currently lose_sum: 94.63377052545547
time_elpased: 1.898
batch start
#iterations: 382
currently lose_sum: 94.45937502384186
time_elpased: 1.875
batch start
#iterations: 383
currently lose_sum: 94.25250029563904
time_elpased: 1.93
batch start
#iterations: 384
currently lose_sum: 94.92032241821289
time_elpased: 1.916
batch start
#iterations: 385
currently lose_sum: 94.73653328418732
time_elpased: 1.931
batch start
#iterations: 386
currently lose_sum: 94.2511796951294
time_elpased: 1.932
batch start
#iterations: 387
currently lose_sum: 94.38739287853241
time_elpased: 1.927
batch start
#iterations: 388
currently lose_sum: 94.93661558628082
time_elpased: 1.951
batch start
#iterations: 389
currently lose_sum: 95.0650846362114
time_elpased: 1.861
batch start
#iterations: 390
currently lose_sum: 94.86013132333755
time_elpased: 1.853
batch start
#iterations: 391
currently lose_sum: 94.82813590765
time_elpased: 1.869
batch start
#iterations: 392
currently lose_sum: 94.43551397323608
time_elpased: 1.881
batch start
#iterations: 393
currently lose_sum: 94.75549191236496
time_elpased: 1.844
batch start
#iterations: 394
currently lose_sum: 94.11194694042206
time_elpased: 1.885
batch start
#iterations: 395
currently lose_sum: 94.2760238647461
time_elpased: 1.871
batch start
#iterations: 396
currently lose_sum: 94.35513657331467
time_elpased: 1.896
batch start
#iterations: 397
currently lose_sum: 94.48910957574844
time_elpased: 1.862
batch start
#iterations: 398
currently lose_sum: 94.58977681398392
time_elpased: 1.871
batch start
#iterations: 399
currently lose_sum: 94.64996367692947
time_elpased: 1.881
start validation test
0.650206185567
0.642212518195
0.681074405681
0.661072819898
0.650151991646
61.764
acc: 0.659
pre: 0.644
rec: 0.712
F1: 0.677
auc: 0.659
