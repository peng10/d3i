start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.58154851198196
time_elpased: 1.869
batch start
#iterations: 1
currently lose_sum: 100.0498235821724
time_elpased: 1.753
batch start
#iterations: 2
currently lose_sum: 99.98141437768936
time_elpased: 1.785
batch start
#iterations: 3
currently lose_sum: 99.66734218597412
time_elpased: 1.772
batch start
#iterations: 4
currently lose_sum: 99.50892966985703
time_elpased: 1.764
batch start
#iterations: 5
currently lose_sum: 98.99103713035583
time_elpased: 1.832
batch start
#iterations: 6
currently lose_sum: 98.83920878171921
time_elpased: 1.895
batch start
#iterations: 7
currently lose_sum: 98.48111736774445
time_elpased: 1.837
batch start
#iterations: 8
currently lose_sum: 98.68556708097458
time_elpased: 1.81
batch start
#iterations: 9
currently lose_sum: 98.17404991388321
time_elpased: 1.779
batch start
#iterations: 10
currently lose_sum: 98.11692202091217
time_elpased: 1.78
batch start
#iterations: 11
currently lose_sum: 98.04897886514664
time_elpased: 1.815
batch start
#iterations: 12
currently lose_sum: 97.80735522508621
time_elpased: 1.78
batch start
#iterations: 13
currently lose_sum: 97.9307290315628
time_elpased: 1.82
batch start
#iterations: 14
currently lose_sum: 97.69332206249237
time_elpased: 1.818
batch start
#iterations: 15
currently lose_sum: 97.5304616689682
time_elpased: 1.794
batch start
#iterations: 16
currently lose_sum: 97.6344204545021
time_elpased: 1.904
batch start
#iterations: 17
currently lose_sum: 97.50590258836746
time_elpased: 1.767
batch start
#iterations: 18
currently lose_sum: 97.2159451842308
time_elpased: 1.801
batch start
#iterations: 19
currently lose_sum: 97.23702943325043
time_elpased: 1.847
start validation test
0.638917525773
0.635762915499
0.653493876711
0.644506470439
0.638891934741
62.854
batch start
#iterations: 20
currently lose_sum: 97.24515759944916
time_elpased: 1.79
batch start
#iterations: 21
currently lose_sum: 96.87549132108688
time_elpased: 1.807
batch start
#iterations: 22
currently lose_sum: 97.2879701256752
time_elpased: 1.844
batch start
#iterations: 23
currently lose_sum: 96.87423980236053
time_elpased: 1.911
batch start
#iterations: 24
currently lose_sum: 97.260717689991
time_elpased: 1.783
batch start
#iterations: 25
currently lose_sum: 96.88423204421997
time_elpased: 1.838
batch start
#iterations: 26
currently lose_sum: 97.0073469877243
time_elpased: 1.825
batch start
#iterations: 27
currently lose_sum: 97.03191590309143
time_elpased: 1.794
batch start
#iterations: 28
currently lose_sum: 96.61911046504974
time_elpased: 1.751
batch start
#iterations: 29
currently lose_sum: 96.97455912828445
time_elpased: 1.783
batch start
#iterations: 30
currently lose_sum: 96.94496130943298
time_elpased: 1.81
batch start
#iterations: 31
currently lose_sum: 96.47900944948196
time_elpased: 1.783
batch start
#iterations: 32
currently lose_sum: 96.61448282003403
time_elpased: 1.768
batch start
#iterations: 33
currently lose_sum: 96.58285892009735
time_elpased: 1.795
batch start
#iterations: 34
currently lose_sum: 96.81151807308197
time_elpased: 1.822
batch start
#iterations: 35
currently lose_sum: 96.46572428941727
time_elpased: 1.781
batch start
#iterations: 36
currently lose_sum: 96.5442852973938
time_elpased: 1.89
batch start
#iterations: 37
currently lose_sum: 96.75060838460922
time_elpased: 1.778
batch start
#iterations: 38
currently lose_sum: 96.28836762905121
time_elpased: 1.802
batch start
#iterations: 39
currently lose_sum: 96.04602313041687
time_elpased: 1.82
start validation test
0.647886597938
0.634558000746
0.700319028507
0.665818697715
0.647794544719
62.329
batch start
#iterations: 40
currently lose_sum: 96.41660690307617
time_elpased: 1.858
batch start
#iterations: 41
currently lose_sum: 96.53852343559265
time_elpased: 1.861
batch start
#iterations: 42
currently lose_sum: 96.32632839679718
time_elpased: 1.828
batch start
#iterations: 43
currently lose_sum: 96.1860722899437
time_elpased: 1.997
batch start
#iterations: 44
currently lose_sum: 96.12159264087677
time_elpased: 1.793
batch start
#iterations: 45
currently lose_sum: 96.2917987704277
time_elpased: 1.779
batch start
#iterations: 46
currently lose_sum: 95.7268773317337
time_elpased: 1.852
batch start
#iterations: 47
currently lose_sum: 96.2536090016365
time_elpased: 1.775
batch start
#iterations: 48
currently lose_sum: 96.30005717277527
time_elpased: 1.841
batch start
#iterations: 49
currently lose_sum: 96.318667948246
time_elpased: 1.793
batch start
#iterations: 50
currently lose_sum: 96.31467753648758
time_elpased: 1.848
batch start
#iterations: 51
currently lose_sum: 96.18442958593369
time_elpased: 1.776
batch start
#iterations: 52
currently lose_sum: 96.11048805713654
time_elpased: 1.809
batch start
#iterations: 53
currently lose_sum: 96.18392610549927
time_elpased: 1.789
batch start
#iterations: 54
currently lose_sum: 96.51011097431183
time_elpased: 1.835
batch start
#iterations: 55
currently lose_sum: 96.09343999624252
time_elpased: 1.806
batch start
#iterations: 56
currently lose_sum: 95.79442644119263
time_elpased: 1.834
batch start
#iterations: 57
currently lose_sum: 95.8721034526825
time_elpased: 1.82
batch start
#iterations: 58
currently lose_sum: 96.2585541009903
time_elpased: 1.775
batch start
#iterations: 59
currently lose_sum: 96.13842833042145
time_elpased: 1.822
start validation test
0.656288659794
0.637826597957
0.726047133889
0.679083646164
0.656166188033
61.790
batch start
#iterations: 60
currently lose_sum: 95.9911053776741
time_elpased: 1.907
batch start
#iterations: 61
currently lose_sum: 96.06697422266006
time_elpased: 1.878
batch start
#iterations: 62
currently lose_sum: 95.85479819774628
time_elpased: 1.858
batch start
#iterations: 63
currently lose_sum: 96.18880242109299
time_elpased: 1.832
batch start
#iterations: 64
currently lose_sum: 96.09792625904083
time_elpased: 1.889
batch start
#iterations: 65
currently lose_sum: 95.45491820573807
time_elpased: 1.815
batch start
#iterations: 66
currently lose_sum: 95.99896693229675
time_elpased: 1.835
batch start
#iterations: 67
currently lose_sum: 95.99130856990814
time_elpased: 1.79
batch start
#iterations: 68
currently lose_sum: 95.99698209762573
time_elpased: 1.838
batch start
#iterations: 69
currently lose_sum: 96.02323561906815
time_elpased: 1.791
batch start
#iterations: 70
currently lose_sum: 95.87750548124313
time_elpased: 1.818
batch start
#iterations: 71
currently lose_sum: 95.68432241678238
time_elpased: 1.835
batch start
#iterations: 72
currently lose_sum: 95.98985856771469
time_elpased: 1.803
batch start
#iterations: 73
currently lose_sum: 95.77679806947708
time_elpased: 1.796
batch start
#iterations: 74
currently lose_sum: 95.77408975362778
time_elpased: 1.917
batch start
#iterations: 75
currently lose_sum: 95.5334050655365
time_elpased: 1.793
batch start
#iterations: 76
currently lose_sum: 95.3145038485527
time_elpased: 1.797
batch start
#iterations: 77
currently lose_sum: 95.92060321569443
time_elpased: 1.854
batch start
#iterations: 78
currently lose_sum: 95.75248610973358
time_elpased: 1.856
batch start
#iterations: 79
currently lose_sum: 96.08899122476578
time_elpased: 1.893
start validation test
0.644329896907
0.6370002918
0.673973448595
0.65496549655
0.64427785308
62.175
batch start
#iterations: 80
currently lose_sum: 96.01067316532135
time_elpased: 1.861
batch start
#iterations: 81
currently lose_sum: 95.90857023000717
time_elpased: 1.801
batch start
#iterations: 82
currently lose_sum: 96.11035168170929
time_elpased: 1.818
batch start
#iterations: 83
currently lose_sum: 95.78914922475815
time_elpased: 1.912
batch start
#iterations: 84
currently lose_sum: 95.72602164745331
time_elpased: 1.81
batch start
#iterations: 85
currently lose_sum: 96.01969242095947
time_elpased: 1.808
batch start
#iterations: 86
currently lose_sum: 96.03184312582016
time_elpased: 2.027
batch start
#iterations: 87
currently lose_sum: 95.47896379232407
time_elpased: 1.791
batch start
#iterations: 88
currently lose_sum: 95.38279807567596
time_elpased: 1.912
batch start
#iterations: 89
currently lose_sum: 95.25692677497864
time_elpased: 1.773
batch start
#iterations: 90
currently lose_sum: 95.948845744133
time_elpased: 1.828
batch start
#iterations: 91
currently lose_sum: 95.96504300832748
time_elpased: 1.772
batch start
#iterations: 92
currently lose_sum: 95.45018321275711
time_elpased: 1.785
batch start
#iterations: 93
currently lose_sum: 96.081980407238
time_elpased: 1.79
batch start
#iterations: 94
currently lose_sum: 95.46793305873871
time_elpased: 1.856
batch start
#iterations: 95
currently lose_sum: 95.68538719415665
time_elpased: 1.819
batch start
#iterations: 96
currently lose_sum: 95.94007503986359
time_elpased: 1.822
batch start
#iterations: 97
currently lose_sum: 95.44448059797287
time_elpased: 1.818
batch start
#iterations: 98
currently lose_sum: 95.52769780158997
time_elpased: 1.811
batch start
#iterations: 99
currently lose_sum: 95.29530447721481
time_elpased: 1.812
start validation test
0.653402061856
0.642428856953
0.694658845323
0.667523734177
0.653329629209
61.680
batch start
#iterations: 100
currently lose_sum: 95.61417073011398
time_elpased: 1.857
batch start
#iterations: 101
currently lose_sum: 95.42384999990463
time_elpased: 1.792
batch start
#iterations: 102
currently lose_sum: 95.26325690746307
time_elpased: 1.819
batch start
#iterations: 103
currently lose_sum: 95.48846876621246
time_elpased: 1.839
batch start
#iterations: 104
currently lose_sum: 95.88267350196838
time_elpased: 1.843
batch start
#iterations: 105
currently lose_sum: 95.54683530330658
time_elpased: 1.852
batch start
#iterations: 106
currently lose_sum: 95.73019343614578
time_elpased: 1.809
batch start
#iterations: 107
currently lose_sum: 95.50994676351547
time_elpased: 1.833
batch start
#iterations: 108
currently lose_sum: 95.54992681741714
time_elpased: 1.873
batch start
#iterations: 109
currently lose_sum: 95.46033000946045
time_elpased: 1.856
batch start
#iterations: 110
currently lose_sum: 95.33002877235413
time_elpased: 1.882
batch start
#iterations: 111
currently lose_sum: 95.62391966581345
time_elpased: 1.825
batch start
#iterations: 112
currently lose_sum: 95.36268424987793
time_elpased: 1.866
batch start
#iterations: 113
currently lose_sum: 95.46017944812775
time_elpased: 1.809
batch start
#iterations: 114
currently lose_sum: 95.30806612968445
time_elpased: 1.845
batch start
#iterations: 115
currently lose_sum: 95.39585965871811
time_elpased: 1.877
batch start
#iterations: 116
currently lose_sum: 95.72701036930084
time_elpased: 1.833
batch start
#iterations: 117
currently lose_sum: 95.4045038819313
time_elpased: 1.866
batch start
#iterations: 118
currently lose_sum: 95.53886848688126
time_elpased: 1.902
batch start
#iterations: 119
currently lose_sum: 95.55494517087936
time_elpased: 1.839
start validation test
0.65824742268
0.642087821044
0.717814140167
0.677842565598
0.65814284412
61.591
batch start
#iterations: 120
currently lose_sum: 95.39188200235367
time_elpased: 1.843
batch start
#iterations: 121
currently lose_sum: 95.34722572565079
time_elpased: 1.866
batch start
#iterations: 122
currently lose_sum: 95.58541375398636
time_elpased: 1.825
batch start
#iterations: 123
currently lose_sum: 95.42457151412964
time_elpased: 1.847
batch start
#iterations: 124
currently lose_sum: 95.08044850826263
time_elpased: 1.867
batch start
#iterations: 125
currently lose_sum: 95.73727536201477
time_elpased: 1.843
batch start
#iterations: 126
currently lose_sum: 95.64830285310745
time_elpased: 1.821
batch start
#iterations: 127
currently lose_sum: 95.56256192922592
time_elpased: 1.841
batch start
#iterations: 128
currently lose_sum: 95.79699498414993
time_elpased: 1.829
batch start
#iterations: 129
currently lose_sum: 95.29632431268692
time_elpased: 1.829
batch start
#iterations: 130
currently lose_sum: 95.29138648509979
time_elpased: 1.849
batch start
#iterations: 131
currently lose_sum: 95.17644453048706
time_elpased: 1.866
batch start
#iterations: 132
currently lose_sum: 95.51340037584305
time_elpased: 1.863
batch start
#iterations: 133
currently lose_sum: 95.47359204292297
time_elpased: 1.862
batch start
#iterations: 134
currently lose_sum: 95.08122324943542
time_elpased: 1.792
batch start
#iterations: 135
currently lose_sum: 95.86426055431366
time_elpased: 1.887
batch start
#iterations: 136
currently lose_sum: 94.87104761600494
time_elpased: 1.847
batch start
#iterations: 137
currently lose_sum: 95.11343443393707
time_elpased: 1.936
batch start
#iterations: 138
currently lose_sum: 95.17265713214874
time_elpased: 1.906
batch start
#iterations: 139
currently lose_sum: 95.41229093074799
time_elpased: 1.81
start validation test
0.654278350515
0.636125180897
0.723783060615
0.677128965484
0.654156324277
61.609
batch start
#iterations: 140
currently lose_sum: 95.60183393955231
time_elpased: 1.824
batch start
#iterations: 141
currently lose_sum: 95.80402171611786
time_elpased: 1.811
batch start
#iterations: 142
currently lose_sum: 95.42353194952011
time_elpased: 1.866
batch start
#iterations: 143
currently lose_sum: 95.36558723449707
time_elpased: 1.855
batch start
#iterations: 144
currently lose_sum: 95.48142921924591
time_elpased: 1.85
batch start
#iterations: 145
currently lose_sum: 95.42010015249252
time_elpased: 1.79
batch start
#iterations: 146
currently lose_sum: 95.04863315820694
time_elpased: 1.875
batch start
#iterations: 147
currently lose_sum: 95.19570165872574
time_elpased: 1.819
batch start
#iterations: 148
currently lose_sum: 95.62953990697861
time_elpased: 1.822
batch start
#iterations: 149
currently lose_sum: 95.30400764942169
time_elpased: 1.944
batch start
#iterations: 150
currently lose_sum: 95.09102660417557
time_elpased: 1.882
batch start
#iterations: 151
currently lose_sum: 94.68669921159744
time_elpased: 1.844
batch start
#iterations: 152
currently lose_sum: 95.36153542995453
time_elpased: 1.887
batch start
#iterations: 153
currently lose_sum: 95.20783412456512
time_elpased: 1.853
batch start
#iterations: 154
currently lose_sum: 95.58539402484894
time_elpased: 1.833
batch start
#iterations: 155
currently lose_sum: 95.46818035840988
time_elpased: 1.905
batch start
#iterations: 156
currently lose_sum: 94.98031014204025
time_elpased: 1.863
batch start
#iterations: 157
currently lose_sum: 95.61018323898315
time_elpased: 1.829
batch start
#iterations: 158
currently lose_sum: 95.75289034843445
time_elpased: 1.853
batch start
#iterations: 159
currently lose_sum: 95.97232604026794
time_elpased: 1.813
start validation test
0.654587628866
0.636668479246
0.722959761243
0.677075803576
0.654467591042
61.647
batch start
#iterations: 160
currently lose_sum: 95.27183896303177
time_elpased: 1.869
batch start
#iterations: 161
currently lose_sum: 95.13925004005432
time_elpased: 1.891
batch start
#iterations: 162
currently lose_sum: 95.1432756781578
time_elpased: 1.873
batch start
#iterations: 163
currently lose_sum: 95.13992375135422
time_elpased: 1.854
batch start
#iterations: 164
currently lose_sum: 95.00076270103455
time_elpased: 1.906
batch start
#iterations: 165
currently lose_sum: 95.26607954502106
time_elpased: 1.867
batch start
#iterations: 166
currently lose_sum: 95.24039608240128
time_elpased: 1.817
batch start
#iterations: 167
currently lose_sum: 95.23503696918488
time_elpased: 1.841
batch start
#iterations: 168
currently lose_sum: 95.21196264028549
time_elpased: 1.818
batch start
#iterations: 169
currently lose_sum: 95.12781894207001
time_elpased: 1.928
batch start
#iterations: 170
currently lose_sum: 95.12376934289932
time_elpased: 1.856
batch start
#iterations: 171
currently lose_sum: 95.36856591701508
time_elpased: 1.886
batch start
#iterations: 172
currently lose_sum: 95.47270512580872
time_elpased: 1.802
batch start
#iterations: 173
currently lose_sum: 95.23249107599258
time_elpased: 1.996
batch start
#iterations: 174
currently lose_sum: 94.90119034051895
time_elpased: 1.841
batch start
#iterations: 175
currently lose_sum: 95.03626209497452
time_elpased: 1.875
batch start
#iterations: 176
currently lose_sum: 95.38642656803131
time_elpased: 1.881
batch start
#iterations: 177
currently lose_sum: 94.93625450134277
time_elpased: 1.86
batch start
#iterations: 178
currently lose_sum: 94.92523485422134
time_elpased: 1.817
batch start
#iterations: 179
currently lose_sum: 95.23796856403351
time_elpased: 1.858
start validation test
0.656391752577
0.637668080498
0.727179170526
0.679488412347
0.656267474347
61.586
batch start
#iterations: 180
currently lose_sum: 95.12833672761917
time_elpased: 1.869
batch start
#iterations: 181
currently lose_sum: 95.43770724534988
time_elpased: 1.897
batch start
#iterations: 182
currently lose_sum: 95.26939404010773
time_elpased: 1.923
batch start
#iterations: 183
currently lose_sum: 94.9123260974884
time_elpased: 1.829
batch start
#iterations: 184
currently lose_sum: 95.25612652301788
time_elpased: 1.892
batch start
#iterations: 185
currently lose_sum: 95.08218264579773
time_elpased: 1.827
batch start
#iterations: 186
currently lose_sum: 95.01054918766022
time_elpased: 1.786
batch start
#iterations: 187
currently lose_sum: 94.78418231010437
time_elpased: 1.921
batch start
#iterations: 188
currently lose_sum: 95.23487919569016
time_elpased: 1.829
batch start
#iterations: 189
currently lose_sum: 95.34294384717941
time_elpased: 1.914
batch start
#iterations: 190
currently lose_sum: 95.4764797091484
time_elpased: 1.849
batch start
#iterations: 191
currently lose_sum: 95.24876481294632
time_elpased: 1.877
batch start
#iterations: 192
currently lose_sum: 95.01549577713013
time_elpased: 1.824
batch start
#iterations: 193
currently lose_sum: 94.94916582107544
time_elpased: 1.837
batch start
#iterations: 194
currently lose_sum: 95.03793919086456
time_elpased: 1.835
batch start
#iterations: 195
currently lose_sum: 95.25006538629532
time_elpased: 1.844
batch start
#iterations: 196
currently lose_sum: 95.54176187515259
time_elpased: 1.896
batch start
#iterations: 197
currently lose_sum: 95.27742123603821
time_elpased: 1.863
batch start
#iterations: 198
currently lose_sum: 95.49275934696198
time_elpased: 1.925
batch start
#iterations: 199
currently lose_sum: 95.14241790771484
time_elpased: 1.894
start validation test
0.654226804124
0.639940470654
0.708037460121
0.672268907563
0.654132331217
61.522
batch start
#iterations: 200
currently lose_sum: 95.02309280633926
time_elpased: 1.888
batch start
#iterations: 201
currently lose_sum: 94.98868781328201
time_elpased: 1.864
batch start
#iterations: 202
currently lose_sum: 94.91812866926193
time_elpased: 1.853
batch start
#iterations: 203
currently lose_sum: 95.08383792638779
time_elpased: 1.881
batch start
#iterations: 204
currently lose_sum: 94.84838420152664
time_elpased: 1.866
batch start
#iterations: 205
currently lose_sum: 94.85784524679184
time_elpased: 1.884
batch start
#iterations: 206
currently lose_sum: 95.2057471871376
time_elpased: 1.865
batch start
#iterations: 207
currently lose_sum: 95.17513090372086
time_elpased: 1.985
batch start
#iterations: 208
currently lose_sum: 94.87195962667465
time_elpased: 1.875
batch start
#iterations: 209
currently lose_sum: 94.65014350414276
time_elpased: 1.842
batch start
#iterations: 210
currently lose_sum: 94.79529386758804
time_elpased: 1.873
batch start
#iterations: 211
currently lose_sum: 94.88838160037994
time_elpased: 1.86
batch start
#iterations: 212
currently lose_sum: 94.81897985935211
time_elpased: 1.928
batch start
#iterations: 213
currently lose_sum: 94.98006397485733
time_elpased: 1.891
batch start
#iterations: 214
currently lose_sum: 95.05297517776489
time_elpased: 1.823
batch start
#iterations: 215
currently lose_sum: 94.97625708580017
time_elpased: 1.829
batch start
#iterations: 216
currently lose_sum: 94.62083166837692
time_elpased: 1.902
batch start
#iterations: 217
currently lose_sum: 94.96649777889252
time_elpased: 1.824
batch start
#iterations: 218
currently lose_sum: 95.14470326900482
time_elpased: 1.838
batch start
#iterations: 219
currently lose_sum: 95.1965326666832
time_elpased: 1.813
start validation test
0.656958762887
0.642154131848
0.711742307296
0.675159857471
0.656862581925
61.382
batch start
#iterations: 220
currently lose_sum: 95.2203027009964
time_elpased: 1.914
batch start
#iterations: 221
currently lose_sum: 94.57396286725998
time_elpased: 1.87
batch start
#iterations: 222
currently lose_sum: 95.23548078536987
time_elpased: 1.891
batch start
#iterations: 223
currently lose_sum: 95.01572734117508
time_elpased: 1.864
batch start
#iterations: 224
currently lose_sum: 95.05447345972061
time_elpased: 1.817
batch start
#iterations: 225
currently lose_sum: 95.0198672413826
time_elpased: 1.828
batch start
#iterations: 226
currently lose_sum: 94.9700191617012
time_elpased: 1.878
batch start
#iterations: 227
currently lose_sum: 95.19345945119858
time_elpased: 1.817
batch start
#iterations: 228
currently lose_sum: 95.04807013273239
time_elpased: 1.855
batch start
#iterations: 229
currently lose_sum: 94.49961882829666
time_elpased: 1.845
batch start
#iterations: 230
currently lose_sum: 95.12032067775726
time_elpased: 1.82
batch start
#iterations: 231
currently lose_sum: 94.9894135594368
time_elpased: 1.849
batch start
#iterations: 232
currently lose_sum: 94.72334003448486
time_elpased: 1.818
batch start
#iterations: 233
currently lose_sum: 95.11162662506104
time_elpased: 1.813
batch start
#iterations: 234
currently lose_sum: 94.73304110765457
time_elpased: 1.894
batch start
#iterations: 235
currently lose_sum: 95.31212246417999
time_elpased: 1.867
batch start
#iterations: 236
currently lose_sum: 95.24528121948242
time_elpased: 1.904
batch start
#iterations: 237
currently lose_sum: 94.9473826289177
time_elpased: 1.848
batch start
#iterations: 238
currently lose_sum: 95.44414532184601
time_elpased: 1.82
batch start
#iterations: 239
currently lose_sum: 95.070780813694
time_elpased: 1.807
start validation test
0.649175257732
0.639375658336
0.687146238551
0.662400793651
0.649108593819
61.669
batch start
#iterations: 240
currently lose_sum: 94.91557550430298
time_elpased: 1.839
batch start
#iterations: 241
currently lose_sum: 94.63561248779297
time_elpased: 1.828
batch start
#iterations: 242
currently lose_sum: 95.14595139026642
time_elpased: 1.862
batch start
#iterations: 243
currently lose_sum: 95.08708703517914
time_elpased: 1.834
batch start
#iterations: 244
currently lose_sum: 94.92415481805801
time_elpased: 1.883
batch start
#iterations: 245
currently lose_sum: 95.33011347055435
time_elpased: 1.84
batch start
#iterations: 246
currently lose_sum: 94.81498855352402
time_elpased: 1.818
batch start
#iterations: 247
currently lose_sum: 95.00595164299011
time_elpased: 1.854
batch start
#iterations: 248
currently lose_sum: 94.99047195911407
time_elpased: 1.812
batch start
#iterations: 249
currently lose_sum: 95.08258885145187
time_elpased: 1.862
batch start
#iterations: 250
currently lose_sum: 94.87046611309052
time_elpased: 1.822
batch start
#iterations: 251
currently lose_sum: 94.98722910881042
time_elpased: 1.844
batch start
#iterations: 252
currently lose_sum: 95.2146942615509
time_elpased: 1.823
batch start
#iterations: 253
currently lose_sum: 94.97734814882278
time_elpased: 1.841
batch start
#iterations: 254
currently lose_sum: 95.06121617555618
time_elpased: 1.849
batch start
#iterations: 255
currently lose_sum: 94.92702102661133
time_elpased: 1.821
batch start
#iterations: 256
currently lose_sum: 94.77647203207016
time_elpased: 1.836
batch start
#iterations: 257
currently lose_sum: 94.86936503648758
time_elpased: 1.874
batch start
#iterations: 258
currently lose_sum: 94.78126972913742
time_elpased: 1.889
batch start
#iterations: 259
currently lose_sum: 94.56005680561066
time_elpased: 1.826
start validation test
0.656443298969
0.640205806689
0.717093753216
0.676472015922
0.656336817742
61.486
batch start
#iterations: 260
currently lose_sum: 94.95635998249054
time_elpased: 1.861
batch start
#iterations: 261
currently lose_sum: 94.81117141246796
time_elpased: 1.925
batch start
#iterations: 262
currently lose_sum: 94.77322316169739
time_elpased: 1.901
batch start
#iterations: 263
currently lose_sum: 94.73050260543823
time_elpased: 1.84
batch start
#iterations: 264
currently lose_sum: 94.63710963726044
time_elpased: 1.83
batch start
#iterations: 265
currently lose_sum: 95.06741327047348
time_elpased: 1.836
batch start
#iterations: 266
currently lose_sum: 95.22698402404785
time_elpased: 1.823
batch start
#iterations: 267
currently lose_sum: 95.2101913690567
time_elpased: 1.835
batch start
#iterations: 268
currently lose_sum: 94.87545305490494
time_elpased: 1.841
batch start
#iterations: 269
currently lose_sum: 94.89618790149689
time_elpased: 1.786
batch start
#iterations: 270
currently lose_sum: 95.0124517083168
time_elpased: 1.809
batch start
#iterations: 271
currently lose_sum: 94.81399518251419
time_elpased: 1.818
batch start
#iterations: 272
currently lose_sum: 95.13300758600235
time_elpased: 1.909
batch start
#iterations: 273
currently lose_sum: 94.9729483127594
time_elpased: 1.775
batch start
#iterations: 274
currently lose_sum: 95.09795600175858
time_elpased: 1.829
batch start
#iterations: 275
currently lose_sum: 95.21954917907715
time_elpased: 1.827
batch start
#iterations: 276
currently lose_sum: 94.77383106946945
time_elpased: 1.895
batch start
#iterations: 277
currently lose_sum: 94.73230236768723
time_elpased: 1.833
batch start
#iterations: 278
currently lose_sum: 94.9588171839714
time_elpased: 1.875
batch start
#iterations: 279
currently lose_sum: 94.80047172307968
time_elpased: 1.932
start validation test
0.658144329897
0.637809345126
0.734691777298
0.682831181253
0.658009939046
61.368
batch start
#iterations: 280
currently lose_sum: 94.76881396770477
time_elpased: 1.892
batch start
#iterations: 281
currently lose_sum: 94.90695852041245
time_elpased: 1.778
batch start
#iterations: 282
currently lose_sum: 95.1392914056778
time_elpased: 1.9
batch start
#iterations: 283
currently lose_sum: 94.88945508003235
time_elpased: 1.841
batch start
#iterations: 284
currently lose_sum: 94.900106549263
time_elpased: 1.833
batch start
#iterations: 285
currently lose_sum: 94.6952633857727
time_elpased: 1.788
batch start
#iterations: 286
currently lose_sum: 94.93249773979187
time_elpased: 1.818
batch start
#iterations: 287
currently lose_sum: 94.62190067768097
time_elpased: 1.798
batch start
#iterations: 288
currently lose_sum: 94.39811587333679
time_elpased: 1.853
batch start
#iterations: 289
currently lose_sum: 94.94368076324463
time_elpased: 1.872
batch start
#iterations: 290
currently lose_sum: 95.06722784042358
time_elpased: 1.932
batch start
#iterations: 291
currently lose_sum: 94.68939507007599
time_elpased: 1.848
batch start
#iterations: 292
currently lose_sum: 94.9194620847702
time_elpased: 1.797
batch start
#iterations: 293
currently lose_sum: 95.04073643684387
time_elpased: 1.818
batch start
#iterations: 294
currently lose_sum: 94.79438960552216
time_elpased: 1.833
batch start
#iterations: 295
currently lose_sum: 95.13361567258835
time_elpased: 1.859
batch start
#iterations: 296
currently lose_sum: 94.4249861240387
time_elpased: 1.9
batch start
#iterations: 297
currently lose_sum: 94.95802181959152
time_elpased: 1.878
batch start
#iterations: 298
currently lose_sum: 94.6160404086113
time_elpased: 1.858
batch start
#iterations: 299
currently lose_sum: 94.69370800256729
time_elpased: 1.76
start validation test
0.656907216495
0.637597770386
0.729854893486
0.680614203455
0.656779145596
61.533
batch start
#iterations: 300
currently lose_sum: 94.53852653503418
time_elpased: 1.825
batch start
#iterations: 301
currently lose_sum: 94.80419558286667
time_elpased: 1.838
batch start
#iterations: 302
currently lose_sum: 94.78464794158936
time_elpased: 1.831
batch start
#iterations: 303
currently lose_sum: 94.89474147558212
time_elpased: 1.796
batch start
#iterations: 304
currently lose_sum: 95.15252876281738
time_elpased: 1.88
batch start
#iterations: 305
currently lose_sum: 94.96125066280365
time_elpased: 1.8
batch start
#iterations: 306
currently lose_sum: 94.68587356805801
time_elpased: 1.784
batch start
#iterations: 307
currently lose_sum: 95.06181287765503
time_elpased: 1.81
batch start
#iterations: 308
currently lose_sum: 94.56373047828674
time_elpased: 1.827
batch start
#iterations: 309
currently lose_sum: 94.9116603732109
time_elpased: 1.863
batch start
#iterations: 310
currently lose_sum: 94.86178642511368
time_elpased: 1.859
batch start
#iterations: 311
currently lose_sum: 94.52407956123352
time_elpased: 1.8
batch start
#iterations: 312
currently lose_sum: 94.60516804456711
time_elpased: 1.904
batch start
#iterations: 313
currently lose_sum: 94.78951787948608
time_elpased: 1.856
batch start
#iterations: 314
currently lose_sum: 95.07697665691376
time_elpased: 1.855
batch start
#iterations: 315
currently lose_sum: 94.7795837521553
time_elpased: 1.773
batch start
#iterations: 316
currently lose_sum: 94.42971670627594
time_elpased: 1.836
batch start
#iterations: 317
currently lose_sum: 95.30431592464447
time_elpased: 1.785
batch start
#iterations: 318
currently lose_sum: 94.73115080595016
time_elpased: 1.894
batch start
#iterations: 319
currently lose_sum: 94.86915761232376
time_elpased: 1.813
start validation test
0.658092783505
0.638196809464
0.73283935371
0.682251497006
0.657961554372
61.485
batch start
#iterations: 320
currently lose_sum: 94.88931256532669
time_elpased: 1.8
batch start
#iterations: 321
currently lose_sum: 94.96578460931778
time_elpased: 1.89
batch start
#iterations: 322
currently lose_sum: 94.84891355037689
time_elpased: 1.815
batch start
#iterations: 323
currently lose_sum: 94.62846213579178
time_elpased: 1.817
batch start
#iterations: 324
currently lose_sum: 95.03408235311508
time_elpased: 1.828
batch start
#iterations: 325
currently lose_sum: 95.24550652503967
time_elpased: 1.803
batch start
#iterations: 326
currently lose_sum: 94.8292281627655
time_elpased: 1.89
batch start
#iterations: 327
currently lose_sum: 94.67853885889053
time_elpased: 1.775
batch start
#iterations: 328
currently lose_sum: 94.6402273774147
time_elpased: 1.8
batch start
#iterations: 329
currently lose_sum: 94.95253676176071
time_elpased: 1.897
batch start
#iterations: 330
currently lose_sum: 94.71052122116089
time_elpased: 1.789
batch start
#iterations: 331
currently lose_sum: 94.48141551017761
time_elpased: 1.817
batch start
#iterations: 332
currently lose_sum: 94.29459303617477
time_elpased: 1.848
batch start
#iterations: 333
currently lose_sum: 94.25931429862976
time_elpased: 1.891
batch start
#iterations: 334
currently lose_sum: 94.72197479009628
time_elpased: 1.774
batch start
#iterations: 335
currently lose_sum: 94.58435153961182
time_elpased: 2.006
batch start
#iterations: 336
currently lose_sum: 94.47470831871033
time_elpased: 1.813
batch start
#iterations: 337
currently lose_sum: 94.94686967134476
time_elpased: 1.775
batch start
#iterations: 338
currently lose_sum: 94.6414806842804
time_elpased: 1.943
batch start
#iterations: 339
currently lose_sum: 94.56502914428711
time_elpased: 1.923
start validation test
0.656288659794
0.63777677361
0.726252958732
0.679145414301
0.656165826676
61.519
batch start
#iterations: 340
currently lose_sum: 94.84944725036621
time_elpased: 1.844
batch start
#iterations: 341
currently lose_sum: 95.16834843158722
time_elpased: 1.888
batch start
#iterations: 342
currently lose_sum: 94.69887828826904
time_elpased: 1.824
batch start
#iterations: 343
currently lose_sum: 94.64243948459625
time_elpased: 1.838
batch start
#iterations: 344
currently lose_sum: 94.32030063867569
time_elpased: 1.863
batch start
#iterations: 345
currently lose_sum: 95.00173854827881
time_elpased: 1.788
batch start
#iterations: 346
currently lose_sum: 94.41235560178757
time_elpased: 1.757
batch start
#iterations: 347
currently lose_sum: 95.05380153656006
time_elpased: 1.797
batch start
#iterations: 348
currently lose_sum: 94.67573702335358
time_elpased: 1.828
batch start
#iterations: 349
currently lose_sum: 94.52185779809952
time_elpased: 1.803
batch start
#iterations: 350
currently lose_sum: 94.95812141895294
time_elpased: 1.815
batch start
#iterations: 351
currently lose_sum: 94.7598397731781
time_elpased: 1.86
batch start
#iterations: 352
currently lose_sum: 94.87526935338974
time_elpased: 1.842
batch start
#iterations: 353
currently lose_sum: 95.27176290750504
time_elpased: 1.885
batch start
#iterations: 354
currently lose_sum: 95.27492040395737
time_elpased: 1.866
batch start
#iterations: 355
currently lose_sum: 95.31430572271347
time_elpased: 1.931
batch start
#iterations: 356
currently lose_sum: 94.67254257202148
time_elpased: 1.892
batch start
#iterations: 357
currently lose_sum: 94.5676839351654
time_elpased: 1.905
batch start
#iterations: 358
currently lose_sum: 95.36133968830109
time_elpased: 1.858
batch start
#iterations: 359
currently lose_sum: 94.96495187282562
time_elpased: 1.936
start validation test
0.654845360825
0.638896551724
0.715035504785
0.674825174825
0.654739687743
61.507
batch start
#iterations: 360
currently lose_sum: 94.92780870199203
time_elpased: 1.81
batch start
#iterations: 361
currently lose_sum: 95.21672886610031
time_elpased: 1.794
batch start
#iterations: 362
currently lose_sum: 94.47276318073273
time_elpased: 1.877
batch start
#iterations: 363
currently lose_sum: 94.4226570725441
time_elpased: 1.833
batch start
#iterations: 364
currently lose_sum: 95.13908404111862
time_elpased: 1.853
batch start
#iterations: 365
currently lose_sum: 95.06930547952652
time_elpased: 1.779
batch start
#iterations: 366
currently lose_sum: 95.06873369216919
time_elpased: 1.832
batch start
#iterations: 367
currently lose_sum: 94.73120486736298
time_elpased: 1.824
batch start
#iterations: 368
currently lose_sum: 94.78111374378204
time_elpased: 1.79
batch start
#iterations: 369
currently lose_sum: 94.47828429937363
time_elpased: 1.91
batch start
#iterations: 370
currently lose_sum: 94.7659832239151
time_elpased: 1.813
batch start
#iterations: 371
currently lose_sum: 94.46121710538864
time_elpased: 1.8
batch start
#iterations: 372
currently lose_sum: 94.94649124145508
time_elpased: 1.844
batch start
#iterations: 373
currently lose_sum: 94.99540013074875
time_elpased: 1.875
batch start
#iterations: 374
currently lose_sum: 94.58696722984314
time_elpased: 1.839
batch start
#iterations: 375
currently lose_sum: 94.96426856517792
time_elpased: 1.785
batch start
#iterations: 376
currently lose_sum: 94.20049548149109
time_elpased: 1.845
batch start
#iterations: 377
currently lose_sum: 94.89435017108917
time_elpased: 1.811
batch start
#iterations: 378
currently lose_sum: 94.87645262479782
time_elpased: 1.855
batch start
#iterations: 379
currently lose_sum: 94.53269731998444
time_elpased: 1.827
start validation test
0.655979381443
0.638104747209
0.723474323351
0.678113243947
0.655860883662
61.408
batch start
#iterations: 380
currently lose_sum: 95.08527100086212
time_elpased: 1.854
batch start
#iterations: 381
currently lose_sum: 94.34717452526093
time_elpased: 1.829
batch start
#iterations: 382
currently lose_sum: 94.83636862039566
time_elpased: 1.865
batch start
#iterations: 383
currently lose_sum: 94.60126209259033
time_elpased: 1.88
batch start
#iterations: 384
currently lose_sum: 94.40650397539139
time_elpased: 1.847
batch start
#iterations: 385
currently lose_sum: 94.805349111557
time_elpased: 1.933
batch start
#iterations: 386
currently lose_sum: 94.93390625715256
time_elpased: 1.814
batch start
#iterations: 387
currently lose_sum: 94.28153216838837
time_elpased: 1.836
batch start
#iterations: 388
currently lose_sum: 94.63287776708603
time_elpased: 1.846
batch start
#iterations: 389
currently lose_sum: 94.86719012260437
time_elpased: 1.794
batch start
#iterations: 390
currently lose_sum: 94.75904822349548
time_elpased: 1.791
batch start
#iterations: 391
currently lose_sum: 94.43773192167282
time_elpased: 1.814
batch start
#iterations: 392
currently lose_sum: 94.47535902261734
time_elpased: 1.769
batch start
#iterations: 393
currently lose_sum: 94.3702672123909
time_elpased: 1.809
batch start
#iterations: 394
currently lose_sum: 94.64790576696396
time_elpased: 1.785
batch start
#iterations: 395
currently lose_sum: 94.96424788236618
time_elpased: 1.785
batch start
#iterations: 396
currently lose_sum: 94.51040142774582
time_elpased: 1.809
batch start
#iterations: 397
currently lose_sum: 94.40965622663498
time_elpased: 1.811
batch start
#iterations: 398
currently lose_sum: 94.64173966646194
time_elpased: 1.765
batch start
#iterations: 399
currently lose_sum: 94.84878665208817
time_elpased: 1.84
start validation test
0.655618556701
0.639701822198
0.71534424205
0.675411747559
0.655513699048
61.534
acc: 0.659
pre: 0.639
rec: 0.731
F1: 0.682
auc: 0.659
