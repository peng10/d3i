start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.5034818649292
time_elpased: 2.197
batch start
#iterations: 1
currently lose_sum: 100.16199171543121
time_elpased: 2.144
batch start
#iterations: 2
currently lose_sum: 99.9723151922226
time_elpased: 2.083
batch start
#iterations: 3
currently lose_sum: 99.53913778066635
time_elpased: 2.071
batch start
#iterations: 4
currently lose_sum: 99.30332314968109
time_elpased: 2.1
batch start
#iterations: 5
currently lose_sum: 99.00013077259064
time_elpased: 2.052
batch start
#iterations: 6
currently lose_sum: 98.74698078632355
time_elpased: 2.17
batch start
#iterations: 7
currently lose_sum: 98.49756997823715
time_elpased: 2.18
batch start
#iterations: 8
currently lose_sum: 98.52062809467316
time_elpased: 2.181
batch start
#iterations: 9
currently lose_sum: 98.3430986404419
time_elpased: 2.156
batch start
#iterations: 10
currently lose_sum: 98.00867307186127
time_elpased: 2.195
batch start
#iterations: 11
currently lose_sum: 98.05413031578064
time_elpased: 2.158
batch start
#iterations: 12
currently lose_sum: 97.82098311185837
time_elpased: 2.088
batch start
#iterations: 13
currently lose_sum: 98.07068783044815
time_elpased: 2.174
batch start
#iterations: 14
currently lose_sum: 97.35547631978989
time_elpased: 2.091
batch start
#iterations: 15
currently lose_sum: 97.77585941553116
time_elpased: 2.178
batch start
#iterations: 16
currently lose_sum: 97.3259626030922
time_elpased: 2.171
batch start
#iterations: 17
currently lose_sum: 97.31794840097427
time_elpased: 2.21
batch start
#iterations: 18
currently lose_sum: 97.2664675116539
time_elpased: 2.086
batch start
#iterations: 19
currently lose_sum: 97.0477237701416
time_elpased: 2.083
start validation test
0.646134020619
0.632159406858
0.701965627251
0.665236260789
0.646035999622
62.706
batch start
#iterations: 20
currently lose_sum: 97.27753400802612
time_elpased: 2.105
batch start
#iterations: 21
currently lose_sum: 97.11061203479767
time_elpased: 2.096
batch start
#iterations: 22
currently lose_sum: 97.05220246315002
time_elpased: 2.079
batch start
#iterations: 23
currently lose_sum: 96.84164291620255
time_elpased: 2.196
batch start
#iterations: 24
currently lose_sum: 96.83607828617096
time_elpased: 2.183
batch start
#iterations: 25
currently lose_sum: 96.86638861894608
time_elpased: 2.204
batch start
#iterations: 26
currently lose_sum: 97.07537806034088
time_elpased: 2.055
batch start
#iterations: 27
currently lose_sum: 96.81215351819992
time_elpased: 2.19
batch start
#iterations: 28
currently lose_sum: 96.51323372125626
time_elpased: 2.191
batch start
#iterations: 29
currently lose_sum: 96.88745814561844
time_elpased: 2.174
batch start
#iterations: 30
currently lose_sum: 96.84002953767776
time_elpased: 2.188
batch start
#iterations: 31
currently lose_sum: 96.24340003728867
time_elpased: 2.132
batch start
#iterations: 32
currently lose_sum: 96.4244242310524
time_elpased: 2.071
batch start
#iterations: 33
currently lose_sum: 96.62507569789886
time_elpased: 2.162
batch start
#iterations: 34
currently lose_sum: 96.47545635700226
time_elpased: 2.192
batch start
#iterations: 35
currently lose_sum: 96.54103815555573
time_elpased: 2.096
batch start
#iterations: 36
currently lose_sum: 96.668805539608
time_elpased: 2.178
batch start
#iterations: 37
currently lose_sum: 96.2741858959198
time_elpased: 2.211
batch start
#iterations: 38
currently lose_sum: 96.378830909729
time_elpased: 2.204
batch start
#iterations: 39
currently lose_sum: 96.38561797142029
time_elpased: 2.231
start validation test
0.641494845361
0.636247040253
0.663682206442
0.649675112074
0.641455892026
62.476
batch start
#iterations: 40
currently lose_sum: 96.65038132667542
time_elpased: 2.15
batch start
#iterations: 41
currently lose_sum: 96.23041874170303
time_elpased: 2.203
batch start
#iterations: 42
currently lose_sum: 96.29187422990799
time_elpased: 2.15
batch start
#iterations: 43
currently lose_sum: 96.21068000793457
time_elpased: 2.081
batch start
#iterations: 44
currently lose_sum: 96.25509649515152
time_elpased: 2.152
batch start
#iterations: 45
currently lose_sum: 96.36675691604614
time_elpased: 2.197
batch start
#iterations: 46
currently lose_sum: 95.93089801073074
time_elpased: 2.154
batch start
#iterations: 47
currently lose_sum: 96.02073192596436
time_elpased: 2.159
batch start
#iterations: 48
currently lose_sum: 96.62427139282227
time_elpased: 2.1
batch start
#iterations: 49
currently lose_sum: 96.1945995092392
time_elpased: 2.012
batch start
#iterations: 50
currently lose_sum: 96.15606021881104
time_elpased: 2.072
batch start
#iterations: 51
currently lose_sum: 96.02057284116745
time_elpased: 2.047
batch start
#iterations: 52
currently lose_sum: 95.94277721643448
time_elpased: 2.019
batch start
#iterations: 53
currently lose_sum: 95.93993955850601
time_elpased: 2.102
batch start
#iterations: 54
currently lose_sum: 96.41613894701004
time_elpased: 2.092
batch start
#iterations: 55
currently lose_sum: 95.78210008144379
time_elpased: 2.08
batch start
#iterations: 56
currently lose_sum: 96.1867526769638
time_elpased: 2.094
batch start
#iterations: 57
currently lose_sum: 96.00376760959625
time_elpased: 2.06
batch start
#iterations: 58
currently lose_sum: 95.80608397722244
time_elpased: 1.98
batch start
#iterations: 59
currently lose_sum: 96.04734897613525
time_elpased: 2.056
start validation test
0.65324742268
0.637332353482
0.71400638057
0.673494151337
0.653140750958
61.937
batch start
#iterations: 60
currently lose_sum: 95.7986034154892
time_elpased: 2.004
batch start
#iterations: 61
currently lose_sum: 96.0137328505516
time_elpased: 2.045
batch start
#iterations: 62
currently lose_sum: 95.93175953626633
time_elpased: 2.194
batch start
#iterations: 63
currently lose_sum: 96.05504596233368
time_elpased: 2.197
batch start
#iterations: 64
currently lose_sum: 96.22745269536972
time_elpased: 2.137
batch start
#iterations: 65
currently lose_sum: 96.12915325164795
time_elpased: 2.215
batch start
#iterations: 66
currently lose_sum: 95.66668713092804
time_elpased: 2.159
batch start
#iterations: 67
currently lose_sum: 95.86161237955093
time_elpased: 2.098
batch start
#iterations: 68
currently lose_sum: 95.90547800064087
time_elpased: 2.163
batch start
#iterations: 69
currently lose_sum: 95.77067452669144
time_elpased: 1.984
batch start
#iterations: 70
currently lose_sum: 95.67153429985046
time_elpased: 2.08
batch start
#iterations: 71
currently lose_sum: 95.78768163919449
time_elpased: 2.087
batch start
#iterations: 72
currently lose_sum: 95.87799894809723
time_elpased: 2.107
batch start
#iterations: 73
currently lose_sum: 96.16142624616623
time_elpased: 2.082
batch start
#iterations: 74
currently lose_sum: 96.05318111181259
time_elpased: 2.107
batch start
#iterations: 75
currently lose_sum: 95.89338105916977
time_elpased: 2.089
batch start
#iterations: 76
currently lose_sum: 95.96222895383835
time_elpased: 2.058
batch start
#iterations: 77
currently lose_sum: 95.65809470415115
time_elpased: 1.972
batch start
#iterations: 78
currently lose_sum: 95.62488627433777
time_elpased: 2.029
batch start
#iterations: 79
currently lose_sum: 96.08162546157837
time_elpased: 2.121
start validation test
0.646597938144
0.639601834683
0.674488010703
0.656581847325
0.64654897282
61.918
batch start
#iterations: 80
currently lose_sum: 95.91531097888947
time_elpased: 2.099
batch start
#iterations: 81
currently lose_sum: 95.83239197731018
time_elpased: 2.078
batch start
#iterations: 82
currently lose_sum: 96.12066060304642
time_elpased: 2.113
batch start
#iterations: 83
currently lose_sum: 95.6479395031929
time_elpased: 2.112
batch start
#iterations: 84
currently lose_sum: 95.73107969760895
time_elpased: 2.096
batch start
#iterations: 85
currently lose_sum: 95.9234471321106
time_elpased: 2.088
batch start
#iterations: 86
currently lose_sum: 95.83336818218231
time_elpased: 1.965
batch start
#iterations: 87
currently lose_sum: 95.75534385442734
time_elpased: 2.044
batch start
#iterations: 88
currently lose_sum: 95.72561085224152
time_elpased: 2.098
batch start
#iterations: 89
currently lose_sum: 95.50016885995865
time_elpased: 2.112
batch start
#iterations: 90
currently lose_sum: 96.07015472650528
time_elpased: 2.142
batch start
#iterations: 91
currently lose_sum: 95.76645946502686
time_elpased: 2.097
batch start
#iterations: 92
currently lose_sum: 95.6988587975502
time_elpased: 2.092
batch start
#iterations: 93
currently lose_sum: 95.66030871868134
time_elpased: 2.093
batch start
#iterations: 94
currently lose_sum: 95.8393177986145
time_elpased: 2.126
batch start
#iterations: 95
currently lose_sum: 95.69690626859665
time_elpased: 1.998
batch start
#iterations: 96
currently lose_sum: 95.40080785751343
time_elpased: 2.037
batch start
#iterations: 97
currently lose_sum: 95.47560387849808
time_elpased: 2.139
batch start
#iterations: 98
currently lose_sum: 95.72143185138702
time_elpased: 2.106
batch start
#iterations: 99
currently lose_sum: 95.86871737241745
time_elpased: 2.102
start validation test
0.654381443299
0.636413043478
0.723062673665
0.676976441682
0.654260862806
61.816
batch start
#iterations: 100
currently lose_sum: 95.84703689813614
time_elpased: 2.143
batch start
#iterations: 101
currently lose_sum: 95.78190803527832
time_elpased: 2.116
batch start
#iterations: 102
currently lose_sum: 95.79270082712173
time_elpased: 2.123
batch start
#iterations: 103
currently lose_sum: 95.62308222055435
time_elpased: 2.068
batch start
#iterations: 104
currently lose_sum: 95.80654084682465
time_elpased: 1.989
batch start
#iterations: 105
currently lose_sum: 95.39659756422043
time_elpased: 2.081
batch start
#iterations: 106
currently lose_sum: 95.78341329097748
time_elpased: 2.083
batch start
#iterations: 107
currently lose_sum: 95.86623245477676
time_elpased: 2.115
batch start
#iterations: 108
currently lose_sum: 95.68343383073807
time_elpased: 2.089
batch start
#iterations: 109
currently lose_sum: 95.58319336175919
time_elpased: 2.114
batch start
#iterations: 110
currently lose_sum: 96.0518838763237
time_elpased: 2.14
batch start
#iterations: 111
currently lose_sum: 95.47077995538712
time_elpased: 2.109
batch start
#iterations: 112
currently lose_sum: 95.6728727221489
time_elpased: 2.045
batch start
#iterations: 113
currently lose_sum: 95.1628201007843
time_elpased: 1.986
batch start
#iterations: 114
currently lose_sum: 95.39301192760468
time_elpased: 2.115
batch start
#iterations: 115
currently lose_sum: 95.57940375804901
time_elpased: 2.121
batch start
#iterations: 116
currently lose_sum: 95.78626388311386
time_elpased: 2.12
batch start
#iterations: 117
currently lose_sum: 95.04418289661407
time_elpased: 2.115
batch start
#iterations: 118
currently lose_sum: 95.44531893730164
time_elpased: 2.112
batch start
#iterations: 119
currently lose_sum: 95.07396012544632
time_elpased: 2.119
start validation test
0.649381443299
0.638243384236
0.692497684471
0.664264560711
0.649305746087
61.633
batch start
#iterations: 120
currently lose_sum: 96.00317442417145
time_elpased: 2.111
batch start
#iterations: 121
currently lose_sum: 95.29557466506958
time_elpased: 2.048
batch start
#iterations: 122
currently lose_sum: 95.50319927930832
time_elpased: 1.989
batch start
#iterations: 123
currently lose_sum: 95.29375576972961
time_elpased: 2.109
batch start
#iterations: 124
currently lose_sum: 95.72317999601364
time_elpased: 2.132
batch start
#iterations: 125
currently lose_sum: 95.77378302812576
time_elpased: 2.098
batch start
#iterations: 126
currently lose_sum: 95.19838750362396
time_elpased: 2.121
batch start
#iterations: 127
currently lose_sum: 95.78044956922531
time_elpased: 2.117
batch start
#iterations: 128
currently lose_sum: 95.50602239370346
time_elpased: 2.09
batch start
#iterations: 129
currently lose_sum: 95.92630416154861
time_elpased: 2.102
batch start
#iterations: 130
currently lose_sum: 95.47429913282394
time_elpased: 2.061
batch start
#iterations: 131
currently lose_sum: 95.76112151145935
time_elpased: 2.017
batch start
#iterations: 132
currently lose_sum: 95.70465689897537
time_elpased: 2.114
batch start
#iterations: 133
currently lose_sum: 95.62824010848999
time_elpased: 2.107
batch start
#iterations: 134
currently lose_sum: 95.2350994348526
time_elpased: 2.123
batch start
#iterations: 135
currently lose_sum: 95.27253419160843
time_elpased: 2.115
batch start
#iterations: 136
currently lose_sum: 95.23567718267441
time_elpased: 2.108
batch start
#iterations: 137
currently lose_sum: 95.51058250665665
time_elpased: 2.104
batch start
#iterations: 138
currently lose_sum: 95.79677802324295
time_elpased: 2.092
batch start
#iterations: 139
currently lose_sum: 95.0650863647461
time_elpased: 1.993
start validation test
0.656237113402
0.639228942079
0.72007821344
0.677249189372
0.656125030504
61.564
batch start
#iterations: 140
currently lose_sum: 95.35818761587143
time_elpased: 2.102
batch start
#iterations: 141
currently lose_sum: 95.94494354724884
time_elpased: 2.119
batch start
#iterations: 142
currently lose_sum: 95.31174021959305
time_elpased: 2.125
batch start
#iterations: 143
currently lose_sum: 95.48162740468979
time_elpased: 2.115
batch start
#iterations: 144
currently lose_sum: 95.39757311344147
time_elpased: 2.118
batch start
#iterations: 145
currently lose_sum: 95.53092175722122
time_elpased: 2.137
batch start
#iterations: 146
currently lose_sum: 95.39885622262955
time_elpased: 2.128
batch start
#iterations: 147
currently lose_sum: 95.37536984682083
time_elpased: 2.109
batch start
#iterations: 148
currently lose_sum: 95.27528828382492
time_elpased: 2.083
batch start
#iterations: 149
currently lose_sum: 95.42249983549118
time_elpased: 2.056
batch start
#iterations: 150
currently lose_sum: 95.48217004537582
time_elpased: 2.207
batch start
#iterations: 151
currently lose_sum: 95.58357208967209
time_elpased: 2.137
batch start
#iterations: 152
currently lose_sum: 95.28600418567657
time_elpased: 2.129
batch start
#iterations: 153
currently lose_sum: 95.55928653478622
time_elpased: 2.114
batch start
#iterations: 154
currently lose_sum: 95.46851515769958
time_elpased: 2.119
batch start
#iterations: 155
currently lose_sum: 95.37754756212234
time_elpased: 2.188
batch start
#iterations: 156
currently lose_sum: 95.4277406334877
time_elpased: 2.039
batch start
#iterations: 157
currently lose_sum: 95.0341961979866
time_elpased: 1.979
batch start
#iterations: 158
currently lose_sum: 95.30132329463959
time_elpased: 2.115
batch start
#iterations: 159
currently lose_sum: 95.48076540231705
time_elpased: 2.12
start validation test
0.655824742268
0.634370579915
0.738499536894
0.682486090637
0.655679593914
61.319
batch start
#iterations: 160
currently lose_sum: 95.24890887737274
time_elpased: 2.149
batch start
#iterations: 161
currently lose_sum: 95.65353894233704
time_elpased: 2.125
batch start
#iterations: 162
currently lose_sum: 95.71913647651672
time_elpased: 2.121
batch start
#iterations: 163
currently lose_sum: 95.5891963839531
time_elpased: 2.128
batch start
#iterations: 164
currently lose_sum: 95.37201499938965
time_elpased: 2.156
batch start
#iterations: 165
currently lose_sum: 95.35662883520126
time_elpased: 2.061
batch start
#iterations: 166
currently lose_sum: 95.30089247226715
time_elpased: 2.014
batch start
#iterations: 167
currently lose_sum: 95.24279797077179
time_elpased: 2.108
batch start
#iterations: 168
currently lose_sum: 95.07558703422546
time_elpased: 2.119
batch start
#iterations: 169
currently lose_sum: 95.42067915201187
time_elpased: 2.175
batch start
#iterations: 170
currently lose_sum: 95.32618176937103
time_elpased: 2.163
batch start
#iterations: 171
currently lose_sum: 95.60282295942307
time_elpased: 2.112
batch start
#iterations: 172
currently lose_sum: 95.84243470430374
time_elpased: 2.17
batch start
#iterations: 173
currently lose_sum: 95.466313123703
time_elpased: 2.25
batch start
#iterations: 174
currently lose_sum: 95.23033738136292
time_elpased: 2.029
batch start
#iterations: 175
currently lose_sum: 94.97266709804535
time_elpased: 2.033
batch start
#iterations: 176
currently lose_sum: 95.03459048271179
time_elpased: 2.115
batch start
#iterations: 177
currently lose_sum: 95.28631108999252
time_elpased: 2.145
batch start
#iterations: 178
currently lose_sum: 95.7620689868927
time_elpased: 2.165
batch start
#iterations: 179
currently lose_sum: 95.27761399745941
time_elpased: 2.133
start validation test
0.656597938144
0.636811464398
0.731707317073
0.680969255818
0.656466072045
61.337
batch start
#iterations: 180
currently lose_sum: 95.06568449735641
time_elpased: 2.158
batch start
#iterations: 181
currently lose_sum: 95.18372106552124
time_elpased: 2.129
batch start
#iterations: 182
currently lose_sum: 95.4896011352539
time_elpased: 2.105
batch start
#iterations: 183
currently lose_sum: 95.23527228832245
time_elpased: 1.979
batch start
#iterations: 184
currently lose_sum: 95.63183224201202
time_elpased: 2.06
batch start
#iterations: 185
currently lose_sum: 95.34249639511108
time_elpased: 2.159
batch start
#iterations: 186
currently lose_sum: 95.20062339305878
time_elpased: 2.144
batch start
#iterations: 187
currently lose_sum: 95.2757858633995
time_elpased: 2.126
batch start
#iterations: 188
currently lose_sum: 95.45150130987167
time_elpased: 2.201
batch start
#iterations: 189
currently lose_sum: 95.11320281028748
time_elpased: 2.092
batch start
#iterations: 190
currently lose_sum: 95.06197029352188
time_elpased: 2.166
batch start
#iterations: 191
currently lose_sum: 95.19928342103958
time_elpased: 2.06
batch start
#iterations: 192
currently lose_sum: 95.15692901611328
time_elpased: 2.007
batch start
#iterations: 193
currently lose_sum: 95.39782547950745
time_elpased: 2.056
batch start
#iterations: 194
currently lose_sum: 95.06824141740799
time_elpased: 2.151
batch start
#iterations: 195
currently lose_sum: 95.06059432029724
time_elpased: 2.115
batch start
#iterations: 196
currently lose_sum: 95.26716566085815
time_elpased: 2.139
batch start
#iterations: 197
currently lose_sum: 95.37755054235458
time_elpased: 2.12
batch start
#iterations: 198
currently lose_sum: 95.24565070867538
time_elpased: 2.102
batch start
#iterations: 199
currently lose_sum: 95.06480610370636
time_elpased: 2.147
start validation test
0.653298969072
0.64048849225
0.701656889987
0.669678813476
0.653214069283
61.401
batch start
#iterations: 200
currently lose_sum: 95.10914844274521
time_elpased: 2.045
batch start
#iterations: 201
currently lose_sum: 95.46896630525589
time_elpased: 2.001
batch start
#iterations: 202
currently lose_sum: 94.96816742420197
time_elpased: 2.121
batch start
#iterations: 203
currently lose_sum: 95.14093887805939
time_elpased: 2.165
batch start
#iterations: 204
currently lose_sum: 95.17983955144882
time_elpased: 2.149
batch start
#iterations: 205
currently lose_sum: 95.06627827882767
time_elpased: 2.165
batch start
#iterations: 206
currently lose_sum: 94.85038489103317
time_elpased: 2.143
batch start
#iterations: 207
currently lose_sum: 95.17084556818008
time_elpased: 2.151
batch start
#iterations: 208
currently lose_sum: 95.4153060913086
time_elpased: 2.132
batch start
#iterations: 209
currently lose_sum: 94.7220870256424
time_elpased: 2.01
batch start
#iterations: 210
currently lose_sum: 95.08295863866806
time_elpased: 2.101
batch start
#iterations: 211
currently lose_sum: 95.16948735713959
time_elpased: 2.131
batch start
#iterations: 212
currently lose_sum: 95.52305674552917
time_elpased: 2.117
batch start
#iterations: 213
currently lose_sum: 95.03482073545456
time_elpased: 2.094
batch start
#iterations: 214
currently lose_sum: 95.52807366847992
time_elpased: 2.143
batch start
#iterations: 215
currently lose_sum: 95.12573438882828
time_elpased: 2.122
batch start
#iterations: 216
currently lose_sum: 95.14507615566254
time_elpased: 2.139
batch start
#iterations: 217
currently lose_sum: 95.13120049238205
time_elpased: 2.087
batch start
#iterations: 218
currently lose_sum: 94.91327768564224
time_elpased: 2.02
batch start
#iterations: 219
currently lose_sum: 95.11729496717453
time_elpased: 2.11
start validation test
0.657268041237
0.640347666972
0.720284038283
0.677967743498
0.657157406935
61.469
batch start
#iterations: 220
currently lose_sum: 95.3377730846405
time_elpased: 2.18
batch start
#iterations: 221
currently lose_sum: 95.10628664493561
time_elpased: 2.12
batch start
#iterations: 222
currently lose_sum: 95.16465330123901
time_elpased: 2.112
batch start
#iterations: 223
currently lose_sum: 94.92476665973663
time_elpased: 2.138
batch start
#iterations: 224
currently lose_sum: 95.08304554224014
time_elpased: 2.178
batch start
#iterations: 225
currently lose_sum: 95.20329540967941
time_elpased: 2.13
batch start
#iterations: 226
currently lose_sum: 95.17262601852417
time_elpased: 2.084
batch start
#iterations: 227
currently lose_sum: 94.69564813375473
time_elpased: 2.045
batch start
#iterations: 228
currently lose_sum: 94.98033094406128
time_elpased: 2.124
batch start
#iterations: 229
currently lose_sum: 95.21804136037827
time_elpased: 2.16
batch start
#iterations: 230
currently lose_sum: 95.02665686607361
time_elpased: 2.157
batch start
#iterations: 231
currently lose_sum: 95.0973750948906
time_elpased: 2.14
batch start
#iterations: 232
currently lose_sum: 94.85725700855255
time_elpased: 2.146
batch start
#iterations: 233
currently lose_sum: 95.37082386016846
time_elpased: 2.174
batch start
#iterations: 234
currently lose_sum: 95.10957151651382
time_elpased: 2.166
batch start
#iterations: 235
currently lose_sum: 94.99946451187134
time_elpased: 2.016
batch start
#iterations: 236
currently lose_sum: 95.16837728023529
time_elpased: 2.043
batch start
#iterations: 237
currently lose_sum: 95.16136002540588
time_elpased: 2.134
batch start
#iterations: 238
currently lose_sum: 95.23965430259705
time_elpased: 2.15
batch start
#iterations: 239
currently lose_sum: 95.4717646241188
time_elpased: 2.113
start validation test
0.654020618557
0.637101925358
0.718534527117
0.675372412459
0.65390735444
61.433
batch start
#iterations: 240
currently lose_sum: 94.99163401126862
time_elpased: 2.122
batch start
#iterations: 241
currently lose_sum: 94.92971432209015
time_elpased: 2.137
batch start
#iterations: 242
currently lose_sum: 95.41818016767502
time_elpased: 2.135
batch start
#iterations: 243
currently lose_sum: 94.79149556159973
time_elpased: 2.116
batch start
#iterations: 244
currently lose_sum: 95.20889633893967
time_elpased: 2.022
batch start
#iterations: 245
currently lose_sum: 95.03584671020508
time_elpased: 2.039
batch start
#iterations: 246
currently lose_sum: 95.51213729381561
time_elpased: 2.129
batch start
#iterations: 247
currently lose_sum: 95.2188350558281
time_elpased: 2.143
batch start
#iterations: 248
currently lose_sum: 94.78254729509354
time_elpased: 2.138
batch start
#iterations: 249
currently lose_sum: 95.09888410568237
time_elpased: 2.148
batch start
#iterations: 250
currently lose_sum: 95.15662401914597
time_elpased: 2.137
batch start
#iterations: 251
currently lose_sum: 94.98661482334137
time_elpased: 2.128
batch start
#iterations: 252
currently lose_sum: 95.25126326084137
time_elpased: 2.115
batch start
#iterations: 253
currently lose_sum: 95.26134079694748
time_elpased: 2.002
batch start
#iterations: 254
currently lose_sum: 95.04523986577988
time_elpased: 2.097
batch start
#iterations: 255
currently lose_sum: 95.1572135090828
time_elpased: 2.181
batch start
#iterations: 256
currently lose_sum: 94.8465901017189
time_elpased: 2.139
batch start
#iterations: 257
currently lose_sum: 94.78001719713211
time_elpased: 2.112
batch start
#iterations: 258
currently lose_sum: 95.16705304384232
time_elpased: 2.138
batch start
#iterations: 259
currently lose_sum: 94.79079657793045
time_elpased: 2.117
start validation test
0.660979381443
0.638790664781
0.743645157971
0.687241428503
0.660834248922
61.149
batch start
#iterations: 260
currently lose_sum: 94.69825541973114
time_elpased: 2.148
batch start
#iterations: 261
currently lose_sum: 94.97670525312424
time_elpased: 2.057
batch start
#iterations: 262
currently lose_sum: 94.77506148815155
time_elpased: 2.003
batch start
#iterations: 263
currently lose_sum: 95.29657274484634
time_elpased: 2.077
batch start
#iterations: 264
currently lose_sum: 94.96535527706146
time_elpased: 2.118
batch start
#iterations: 265
currently lose_sum: 95.04962468147278
time_elpased: 2.139
batch start
#iterations: 266
currently lose_sum: 94.82824921607971
time_elpased: 2.106
batch start
#iterations: 267
currently lose_sum: 95.01266956329346
time_elpased: 2.169
batch start
#iterations: 268
currently lose_sum: 94.98165756464005
time_elpased: 2.138
batch start
#iterations: 269
currently lose_sum: 95.37652468681335
time_elpased: 2.13
batch start
#iterations: 270
currently lose_sum: 94.95048868656158
time_elpased: 2.062
batch start
#iterations: 271
currently lose_sum: 95.01799607276917
time_elpased: 2.013
batch start
#iterations: 272
currently lose_sum: 94.86386877298355
time_elpased: 1.965
batch start
#iterations: 273
currently lose_sum: 94.70706099271774
time_elpased: 1.958
batch start
#iterations: 274
currently lose_sum: 94.93671435117722
time_elpased: 2.106
batch start
#iterations: 275
currently lose_sum: 95.41268390417099
time_elpased: 2.142
batch start
#iterations: 276
currently lose_sum: 94.72922748327255
time_elpased: 2.183
batch start
#iterations: 277
currently lose_sum: 95.07187098264694
time_elpased: 2.199
batch start
#iterations: 278
currently lose_sum: 94.68886470794678
time_elpased: 2.249
batch start
#iterations: 279
currently lose_sum: 95.11699765920639
time_elpased: 2.148
start validation test
0.658865979381
0.642482758621
0.719049089225
0.678613053613
0.65876031865
61.324
batch start
#iterations: 280
currently lose_sum: 95.08282315731049
time_elpased: 2.104
batch start
#iterations: 281
currently lose_sum: 94.87636744976044
time_elpased: 2.198
batch start
#iterations: 282
currently lose_sum: 94.94819855690002
time_elpased: 2.194
batch start
#iterations: 283
currently lose_sum: 94.88062411546707
time_elpased: 2.206
batch start
#iterations: 284
currently lose_sum: 94.98460030555725
time_elpased: 2.165
batch start
#iterations: 285
currently lose_sum: 94.85416400432587
time_elpased: 2.214
batch start
#iterations: 286
currently lose_sum: 95.08888500928879
time_elpased: 2.195
batch start
#iterations: 287
currently lose_sum: 94.61459195613861
time_elpased: 2.192
batch start
#iterations: 288
currently lose_sum: 94.87887865304947
time_elpased: 2.107
batch start
#iterations: 289
currently lose_sum: 95.27181077003479
time_elpased: 2.008
batch start
#iterations: 290
currently lose_sum: 94.47529286146164
time_elpased: 2.182
batch start
#iterations: 291
currently lose_sum: 95.08288371562958
time_elpased: 2.065
batch start
#iterations: 292
currently lose_sum: 95.05597370862961
time_elpased: 2.201
batch start
#iterations: 293
currently lose_sum: 94.90315192937851
time_elpased: 2.172
batch start
#iterations: 294
currently lose_sum: 95.23641180992126
time_elpased: 2.173
batch start
#iterations: 295
currently lose_sum: 95.33080065250397
time_elpased: 2.186
batch start
#iterations: 296
currently lose_sum: 95.23378700017929
time_elpased: 2.208
batch start
#iterations: 297
currently lose_sum: 94.65017110109329
time_elpased: 2.118
batch start
#iterations: 298
currently lose_sum: 95.16560304164886
time_elpased: 2.048
batch start
#iterations: 299
currently lose_sum: 95.00187766551971
time_elpased: 2.133
start validation test
0.654329896907
0.643121969769
0.696202531646
0.668610397312
0.654256383039
61.269
batch start
#iterations: 300
currently lose_sum: 94.92262864112854
time_elpased: 2.203
batch start
#iterations: 301
currently lose_sum: 95.28006023168564
time_elpased: 2.183
batch start
#iterations: 302
currently lose_sum: 94.79714798927307
time_elpased: 2.182
batch start
#iterations: 303
currently lose_sum: 95.04140222072601
time_elpased: 2.165
batch start
#iterations: 304
currently lose_sum: 95.11297577619553
time_elpased: 2.176
batch start
#iterations: 305
currently lose_sum: 95.24917739629745
time_elpased: 2.176
batch start
#iterations: 306
currently lose_sum: 94.76203447580338
time_elpased: 2.113
batch start
#iterations: 307
currently lose_sum: 95.02686727046967
time_elpased: 2.079
batch start
#iterations: 308
currently lose_sum: 94.65637791156769
time_elpased: 2.187
batch start
#iterations: 309
currently lose_sum: 94.39086526632309
time_elpased: 2.232
batch start
#iterations: 310
currently lose_sum: 95.13714998960495
time_elpased: 2.156
batch start
#iterations: 311
currently lose_sum: 95.13295787572861
time_elpased: 2.155
batch start
#iterations: 312
currently lose_sum: 94.97929358482361
time_elpased: 2.115
batch start
#iterations: 313
currently lose_sum: 94.76391589641571
time_elpased: 2.127
batch start
#iterations: 314
currently lose_sum: 94.91469639539719
time_elpased: 2.124
batch start
#iterations: 315
currently lose_sum: 94.91298085451126
time_elpased: 2.086
batch start
#iterations: 316
currently lose_sum: 94.76679414510727
time_elpased: 2.003
batch start
#iterations: 317
currently lose_sum: 94.94724583625793
time_elpased: 2.122
batch start
#iterations: 318
currently lose_sum: 94.91284382343292
time_elpased: 2.129
batch start
#iterations: 319
currently lose_sum: 94.98132109642029
time_elpased: 2.168
start validation test
0.65824742268
0.643808813938
0.711124832767
0.675794621027
0.658154588231
61.323
batch start
#iterations: 320
currently lose_sum: 94.51034033298492
time_elpased: 2.192
batch start
#iterations: 321
currently lose_sum: 94.8610138297081
time_elpased: 2.147
batch start
#iterations: 322
currently lose_sum: 95.1213470697403
time_elpased: 2.115
batch start
#iterations: 323
currently lose_sum: 94.761470079422
time_elpased: 2.146
batch start
#iterations: 324
currently lose_sum: 94.72063320875168
time_elpased: 2.044
batch start
#iterations: 325
currently lose_sum: 94.82467138767242
time_elpased: 2.029
batch start
#iterations: 326
currently lose_sum: 94.72122305631638
time_elpased: 2.125
batch start
#iterations: 327
currently lose_sum: 94.94716364145279
time_elpased: 2.114
batch start
#iterations: 328
currently lose_sum: 94.86837321519852
time_elpased: 2.106
batch start
#iterations: 329
currently lose_sum: 94.98860305547714
time_elpased: 2.131
batch start
#iterations: 330
currently lose_sum: 94.77141362428665
time_elpased: 2.096
batch start
#iterations: 331
currently lose_sum: 95.14748704433441
time_elpased: 2.1
batch start
#iterations: 332
currently lose_sum: 94.65262585878372
time_elpased: 2.156
batch start
#iterations: 333
currently lose_sum: 94.76281297206879
time_elpased: 2.121
batch start
#iterations: 334
currently lose_sum: 94.7398870587349
time_elpased: 2.015
batch start
#iterations: 335
currently lose_sum: 94.58553624153137
time_elpased: 2.158
batch start
#iterations: 336
currently lose_sum: 94.61177825927734
time_elpased: 2.088
batch start
#iterations: 337
currently lose_sum: 95.62020564079285
time_elpased: 2.133
batch start
#iterations: 338
currently lose_sum: 94.58701956272125
time_elpased: 2.121
batch start
#iterations: 339
currently lose_sum: 94.90238958597183
time_elpased: 2.194
start validation test
0.661082474227
0.637301171124
0.750437377791
0.689257526348
0.660925597912
61.370
batch start
#iterations: 340
currently lose_sum: 94.74251568317413
time_elpased: 2.173
batch start
#iterations: 341
currently lose_sum: 94.93176847696304
time_elpased: 2.133
batch start
#iterations: 342
currently lose_sum: 95.25948637723923
time_elpased: 2.059
batch start
#iterations: 343
currently lose_sum: 94.93946951627731
time_elpased: 2.027
batch start
#iterations: 344
currently lose_sum: 94.42635554075241
time_elpased: 2.106
batch start
#iterations: 345
currently lose_sum: 94.8729636669159
time_elpased: 2.164
batch start
#iterations: 346
currently lose_sum: 94.82619202136993
time_elpased: 2.081
batch start
#iterations: 347
currently lose_sum: 94.80111336708069
time_elpased: 2.139
batch start
#iterations: 348
currently lose_sum: 94.7681764960289
time_elpased: 2.176
batch start
#iterations: 349
currently lose_sum: 94.9632083773613
time_elpased: 2.158
batch start
#iterations: 350
currently lose_sum: 95.242555975914
time_elpased: 2.14
batch start
#iterations: 351
currently lose_sum: 94.69211274385452
time_elpased: 2.139
batch start
#iterations: 352
currently lose_sum: 94.96369218826294
time_elpased: 1.974
batch start
#iterations: 353
currently lose_sum: 94.82527565956116
time_elpased: 2.138
batch start
#iterations: 354
currently lose_sum: 95.05445474386215
time_elpased: 2.156
batch start
#iterations: 355
currently lose_sum: 94.72747427225113
time_elpased: 2.137
batch start
#iterations: 356
currently lose_sum: 94.78357195854187
time_elpased: 2.112
batch start
#iterations: 357
currently lose_sum: 95.09842437505722
time_elpased: 2.112
batch start
#iterations: 358
currently lose_sum: 94.60519641637802
time_elpased: 2.104
batch start
#iterations: 359
currently lose_sum: 94.68214601278305
time_elpased: 2.119
start validation test
0.655360824742
0.639330697803
0.715652979315
0.675342332718
0.655254972565
61.442
batch start
#iterations: 360
currently lose_sum: 95.01059752702713
time_elpased: 2.102
batch start
#iterations: 361
currently lose_sum: 95.20449995994568
time_elpased: 1.927
batch start
#iterations: 362
currently lose_sum: 94.66180509328842
time_elpased: 2.074
batch start
#iterations: 363
currently lose_sum: 94.78694188594818
time_elpased: 2.092
batch start
#iterations: 364
currently lose_sum: 94.6496752500534
time_elpased: 2.122
batch start
#iterations: 365
currently lose_sum: 94.99249196052551
time_elpased: 2.073
batch start
#iterations: 366
currently lose_sum: 95.06414675712585
time_elpased: 2.131
batch start
#iterations: 367
currently lose_sum: 94.7549660205841
time_elpased: 2.08
batch start
#iterations: 368
currently lose_sum: 95.1354483962059
time_elpased: 2.097
batch start
#iterations: 369
currently lose_sum: 94.80195462703705
time_elpased: 2.119
batch start
#iterations: 370
currently lose_sum: 94.49203461408615
time_elpased: 2.026
batch start
#iterations: 371
currently lose_sum: 94.84038066864014
time_elpased: 1.964
batch start
#iterations: 372
currently lose_sum: 94.67293971776962
time_elpased: 2.125
batch start
#iterations: 373
currently lose_sum: 94.73998361825943
time_elpased: 2.091
batch start
#iterations: 374
currently lose_sum: 95.18679529428482
time_elpased: 2.117
batch start
#iterations: 375
currently lose_sum: 94.86887353658676
time_elpased: 2.144
batch start
#iterations: 376
currently lose_sum: 94.5481749176979
time_elpased: 2.138
batch start
#iterations: 377
currently lose_sum: 95.14345973730087
time_elpased: 2.117
batch start
#iterations: 378
currently lose_sum: 94.96377539634705
time_elpased: 2.102
batch start
#iterations: 379
currently lose_sum: 94.80613869428635
time_elpased: 2.124
start validation test
0.661082474227
0.637517507003
0.749511165998
0.688992952084
0.660927224019
61.320
batch start
#iterations: 380
currently lose_sum: 94.99290269613266
time_elpased: 1.925
batch start
#iterations: 381
currently lose_sum: 94.80402135848999
time_elpased: 2.151
batch start
#iterations: 382
currently lose_sum: 94.96339458227158
time_elpased: 2.113
batch start
#iterations: 383
currently lose_sum: 94.90490084886551
time_elpased: 2.13
batch start
#iterations: 384
currently lose_sum: 95.14848613739014
time_elpased: 2.143
batch start
#iterations: 385
currently lose_sum: 95.13351601362228
time_elpased: 2.105
batch start
#iterations: 386
currently lose_sum: 94.81354111433029
time_elpased: 2.118
batch start
#iterations: 387
currently lose_sum: 94.91260278224945
time_elpased: 2.124
batch start
#iterations: 388
currently lose_sum: 95.23112332820892
time_elpased: 2.138
batch start
#iterations: 389
currently lose_sum: 94.69506239891052
time_elpased: 1.973
batch start
#iterations: 390
currently lose_sum: 94.87578982114792
time_elpased: 2.056
batch start
#iterations: 391
currently lose_sum: 94.67864322662354
time_elpased: 2.15
batch start
#iterations: 392
currently lose_sum: 94.99820786714554
time_elpased: 2.13
batch start
#iterations: 393
currently lose_sum: 94.75547587871552
time_elpased: 2.128
batch start
#iterations: 394
currently lose_sum: 94.72438168525696
time_elpased: 2.176
batch start
#iterations: 395
currently lose_sum: 94.57050025463104
time_elpased: 2.161
batch start
#iterations: 396
currently lose_sum: 94.67277705669403
time_elpased: 2.143
batch start
#iterations: 397
currently lose_sum: 94.79543727636337
time_elpased: 2.117
batch start
#iterations: 398
currently lose_sum: 94.8607228398323
time_elpased: 2.034
batch start
#iterations: 399
currently lose_sum: 94.62643992900848
time_elpased: 2.054
start validation test
0.657422680412
0.64078114972
0.719254914068
0.677754072925
0.657314124389
61.371
acc: 0.655
pre: 0.635
rec: 0.734
F1: 0.680
auc: 0.655
