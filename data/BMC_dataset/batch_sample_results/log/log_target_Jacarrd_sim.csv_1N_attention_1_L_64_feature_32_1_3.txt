start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.54605484008789
time_elpased: 2.41
batch start
#iterations: 1
currently lose_sum: 100.07852685451508
time_elpased: 2.29
batch start
#iterations: 2
currently lose_sum: 99.85354548692703
time_elpased: 2.386
batch start
#iterations: 3
currently lose_sum: 99.44686603546143
time_elpased: 2.402
batch start
#iterations: 4
currently lose_sum: 99.31816852092743
time_elpased: 2.299
batch start
#iterations: 5
currently lose_sum: 98.91891396045685
time_elpased: 2.247
batch start
#iterations: 6
currently lose_sum: 98.80824553966522
time_elpased: 2.265
batch start
#iterations: 7
currently lose_sum: 98.46821957826614
time_elpased: 2.367
batch start
#iterations: 8
currently lose_sum: 98.40098679065704
time_elpased: 2.316
batch start
#iterations: 9
currently lose_sum: 98.18377095460892
time_elpased: 2.417
batch start
#iterations: 10
currently lose_sum: 98.02474993467331
time_elpased: 2.345
batch start
#iterations: 11
currently lose_sum: 97.84219539165497
time_elpased: 2.25
batch start
#iterations: 12
currently lose_sum: 97.66037046909332
time_elpased: 2.326
batch start
#iterations: 13
currently lose_sum: 97.84370046854019
time_elpased: 2.444
batch start
#iterations: 14
currently lose_sum: 97.63203155994415
time_elpased: 2.4
batch start
#iterations: 15
currently lose_sum: 97.49149286746979
time_elpased: 2.42
batch start
#iterations: 16
currently lose_sum: 97.51439654827118
time_elpased: 2.381
batch start
#iterations: 17
currently lose_sum: 97.46255803108215
time_elpased: 2.325
batch start
#iterations: 18
currently lose_sum: 97.24546629190445
time_elpased: 2.31
batch start
#iterations: 19
currently lose_sum: 97.11393815279007
time_elpased: 2.266
start validation test
0.633711340206
0.62862211709
0.65644298065
0.642231396637
0.633673782766
63.024
batch start
#iterations: 20
currently lose_sum: 96.95223379135132
time_elpased: 2.173
batch start
#iterations: 21
currently lose_sum: 96.93005782365799
time_elpased: 2.242
batch start
#iterations: 22
currently lose_sum: 96.95287096500397
time_elpased: 2.219
batch start
#iterations: 23
currently lose_sum: 96.94063901901245
time_elpased: 2.102
batch start
#iterations: 24
currently lose_sum: 96.97709083557129
time_elpased: 2.155
batch start
#iterations: 25
currently lose_sum: 96.7650243639946
time_elpased: 2.182
batch start
#iterations: 26
currently lose_sum: 96.92725610733032
time_elpased: 2.159
batch start
#iterations: 27
currently lose_sum: 96.88224750757217
time_elpased: 2.154
batch start
#iterations: 28
currently lose_sum: 96.73936235904694
time_elpased: 2.118
batch start
#iterations: 29
currently lose_sum: 97.05387848615646
time_elpased: 2.173
batch start
#iterations: 30
currently lose_sum: 96.79756337404251
time_elpased: 2.169
batch start
#iterations: 31
currently lose_sum: 96.64045172929764
time_elpased: 2.184
batch start
#iterations: 32
currently lose_sum: 96.50212144851685
time_elpased: 2.189
batch start
#iterations: 33
currently lose_sum: 96.63326263427734
time_elpased: 2.146
batch start
#iterations: 34
currently lose_sum: 96.5909343957901
time_elpased: 2.168
batch start
#iterations: 35
currently lose_sum: 96.6925602555275
time_elpased: 2.177
batch start
#iterations: 36
currently lose_sum: 96.63266229629517
time_elpased: 2.037
batch start
#iterations: 37
currently lose_sum: 96.42623317241669
time_elpased: 2.148
batch start
#iterations: 38
currently lose_sum: 96.39191544055939
time_elpased: 2.133
batch start
#iterations: 39
currently lose_sum: 96.34564924240112
time_elpased: 2.16
start validation test
0.646134020619
0.637027780448
0.682070811033
0.658780257468
0.6460746455
62.455
batch start
#iterations: 40
currently lose_sum: 96.37794631719589
time_elpased: 2.188
batch start
#iterations: 41
currently lose_sum: 96.71650636196136
time_elpased: 2.161
batch start
#iterations: 42
currently lose_sum: 96.51577597856522
time_elpased: 2.014
batch start
#iterations: 43
currently lose_sum: 96.32820093631744
time_elpased: 2.071
batch start
#iterations: 44
currently lose_sum: 96.34830313920975
time_elpased: 2.117
batch start
#iterations: 45
currently lose_sum: 96.68448638916016
time_elpased: 2.034
batch start
#iterations: 46
currently lose_sum: 95.9766840338707
time_elpased: 2.062
batch start
#iterations: 47
currently lose_sum: 96.07756477594376
time_elpased: 2.022
batch start
#iterations: 48
currently lose_sum: 96.2641282081604
time_elpased: 2.054
batch start
#iterations: 49
currently lose_sum: 95.99592107534409
time_elpased: 2.048
batch start
#iterations: 50
currently lose_sum: 96.21470355987549
time_elpased: 2.062
batch start
#iterations: 51
currently lose_sum: 96.21579211950302
time_elpased: 2.036
batch start
#iterations: 52
currently lose_sum: 96.37655872106552
time_elpased: 1.929
batch start
#iterations: 53
currently lose_sum: 95.95110249519348
time_elpased: 2.08
batch start
#iterations: 54
currently lose_sum: 96.29625338315964
time_elpased: 2.047
batch start
#iterations: 55
currently lose_sum: 95.95716524124146
time_elpased: 2.06
batch start
#iterations: 56
currently lose_sum: 96.38201129436493
time_elpased: 2.017
batch start
#iterations: 57
currently lose_sum: 95.89946007728577
time_elpased: 2.047
batch start
#iterations: 58
currently lose_sum: 96.29978394508362
time_elpased: 2.069
batch start
#iterations: 59
currently lose_sum: 96.00925368070602
time_elpased: 1.993
start validation test
0.64912371134
0.640926266835
0.68083573487
0.660278484803
0.649071316423
62.279
batch start
#iterations: 60
currently lose_sum: 95.99537718296051
time_elpased: 2.072
batch start
#iterations: 61
currently lose_sum: 96.16235721111298
time_elpased: 2.01
batch start
#iterations: 62
currently lose_sum: 96.31786155700684
time_elpased: 2.005
batch start
#iterations: 63
currently lose_sum: 96.02512180805206
time_elpased: 2.055
batch start
#iterations: 64
currently lose_sum: 96.13417476415634
time_elpased: 2.002
batch start
#iterations: 65
currently lose_sum: 96.1644879579544
time_elpased: 2.082
batch start
#iterations: 66
currently lose_sum: 96.18006139993668
time_elpased: 1.994
batch start
#iterations: 67
currently lose_sum: 96.01416492462158
time_elpased: 2.026
batch start
#iterations: 68
currently lose_sum: 95.94138944149017
time_elpased: 2.006
batch start
#iterations: 69
currently lose_sum: 95.52125144004822
time_elpased: 2.024
batch start
#iterations: 70
currently lose_sum: 96.332610309124
time_elpased: 2.034
batch start
#iterations: 71
currently lose_sum: 95.87227576971054
time_elpased: 2.011
batch start
#iterations: 72
currently lose_sum: 96.01537281274796
time_elpased: 1.991
batch start
#iterations: 73
currently lose_sum: 95.67433595657349
time_elpased: 1.966
batch start
#iterations: 74
currently lose_sum: 96.02687710523605
time_elpased: 1.954
batch start
#iterations: 75
currently lose_sum: 95.70993912220001
time_elpased: 2.017
batch start
#iterations: 76
currently lose_sum: 95.95311230421066
time_elpased: 2.061
batch start
#iterations: 77
currently lose_sum: 95.60452741384506
time_elpased: 2.025
batch start
#iterations: 78
currently lose_sum: 96.12844288349152
time_elpased: 2.059
batch start
#iterations: 79
currently lose_sum: 95.8809524178505
time_elpased: 2.028
start validation test
0.650206185567
0.642150203765
0.681144503911
0.661072819898
0.650155068973
61.943
batch start
#iterations: 80
currently lose_sum: 95.73196268081665
time_elpased: 2.059
batch start
#iterations: 81
currently lose_sum: 96.0105749964714
time_elpased: 2.002
batch start
#iterations: 82
currently lose_sum: 95.82644176483154
time_elpased: 2.058
batch start
#iterations: 83
currently lose_sum: 95.9351561665535
time_elpased: 2.065
batch start
#iterations: 84
currently lose_sum: 95.67815244197845
time_elpased: 2.016
batch start
#iterations: 85
currently lose_sum: 95.40559059381485
time_elpased: 2.047
batch start
#iterations: 86
currently lose_sum: 95.96685546636581
time_elpased: 1.987
batch start
#iterations: 87
currently lose_sum: 95.20488303899765
time_elpased: 2.013
batch start
#iterations: 88
currently lose_sum: 95.8084711432457
time_elpased: 1.94
batch start
#iterations: 89
currently lose_sum: 95.74800825119019
time_elpased: 2.022
batch start
#iterations: 90
currently lose_sum: 95.94507253170013
time_elpased: 2.022
batch start
#iterations: 91
currently lose_sum: 95.66210240125656
time_elpased: 1.991
batch start
#iterations: 92
currently lose_sum: 95.85548388957977
time_elpased: 2.056
batch start
#iterations: 93
currently lose_sum: 95.2443500161171
time_elpased: 1.965
batch start
#iterations: 94
currently lose_sum: 95.62238371372223
time_elpased: 1.999
batch start
#iterations: 95
currently lose_sum: 95.80844980478287
time_elpased: 1.956
batch start
#iterations: 96
currently lose_sum: 95.49079596996307
time_elpased: 1.972
batch start
#iterations: 97
currently lose_sum: 95.5016862154007
time_elpased: 2.025
batch start
#iterations: 98
currently lose_sum: 95.6170557141304
time_elpased: 2.023
batch start
#iterations: 99
currently lose_sum: 95.53165638446808
time_elpased: 2.005
start validation test
0.647886597938
0.639036144578
0.682379580074
0.659997013588
0.647829608294
62.101
batch start
#iterations: 100
currently lose_sum: 95.96130907535553
time_elpased: 2.005
batch start
#iterations: 101
currently lose_sum: 95.87487268447876
time_elpased: 1.98
batch start
#iterations: 102
currently lose_sum: 96.14399552345276
time_elpased: 1.959
batch start
#iterations: 103
currently lose_sum: 95.8403844833374
time_elpased: 2.001
batch start
#iterations: 104
currently lose_sum: 95.60674035549164
time_elpased: 2.043
batch start
#iterations: 105
currently lose_sum: 95.78919553756714
time_elpased: 2.086
batch start
#iterations: 106
currently lose_sum: 95.44220674037933
time_elpased: 2.036
batch start
#iterations: 107
currently lose_sum: 95.81428653001785
time_elpased: 2.091
batch start
#iterations: 108
currently lose_sum: 95.43980211019516
time_elpased: 2.027
batch start
#iterations: 109
currently lose_sum: 95.43261831998825
time_elpased: 1.99
batch start
#iterations: 110
currently lose_sum: 95.92696583271027
time_elpased: 1.958
batch start
#iterations: 111
currently lose_sum: 95.43738985061646
time_elpased: 2.021
batch start
#iterations: 112
currently lose_sum: 95.662533223629
time_elpased: 2.043
batch start
#iterations: 113
currently lose_sum: 95.49148160219193
time_elpased: 2.042
batch start
#iterations: 114
currently lose_sum: 95.64117342233658
time_elpased: 2.04
batch start
#iterations: 115
currently lose_sum: 95.5724213719368
time_elpased: 2.036
batch start
#iterations: 116
currently lose_sum: 95.86914891004562
time_elpased: 1.989
batch start
#iterations: 117
currently lose_sum: 95.31042909622192
time_elpased: 1.958
batch start
#iterations: 118
currently lose_sum: 95.79061686992645
time_elpased: 1.916
batch start
#iterations: 119
currently lose_sum: 95.82620507478714
time_elpased: 1.996
start validation test
0.657422680412
0.636153982615
0.738163853438
0.683373034778
0.657289279053
61.880
batch start
#iterations: 120
currently lose_sum: 95.18815690279007
time_elpased: 2.047
batch start
#iterations: 121
currently lose_sum: 95.47153890132904
time_elpased: 2.028
batch start
#iterations: 122
currently lose_sum: 95.67661851644516
time_elpased: 2.023
batch start
#iterations: 123
currently lose_sum: 95.41577106714249
time_elpased: 1.984
batch start
#iterations: 124
currently lose_sum: 95.70809924602509
time_elpased: 2.027
batch start
#iterations: 125
currently lose_sum: 95.54939842224121
time_elpased: 1.959
batch start
#iterations: 126
currently lose_sum: 95.10795974731445
time_elpased: 2.008
batch start
#iterations: 127
currently lose_sum: 95.46926015615463
time_elpased: 2.104
batch start
#iterations: 128
currently lose_sum: 95.57744854688644
time_elpased: 2.03
batch start
#iterations: 129
currently lose_sum: 95.20324218273163
time_elpased: 2.091
batch start
#iterations: 130
currently lose_sum: 95.74705630540848
time_elpased: 2.068
batch start
#iterations: 131
currently lose_sum: 95.2855293750763
time_elpased: 2.054
batch start
#iterations: 132
currently lose_sum: 95.46396511793137
time_elpased: 2.032
batch start
#iterations: 133
currently lose_sum: 95.60730069875717
time_elpased: 2.028
batch start
#iterations: 134
currently lose_sum: 95.3308892250061
time_elpased: 2.094
batch start
#iterations: 135
currently lose_sum: 95.61663734912872
time_elpased: 2.027
batch start
#iterations: 136
currently lose_sum: 95.59599339962006
time_elpased: 1.998
batch start
#iterations: 137
currently lose_sum: 95.73235082626343
time_elpased: 2.062
batch start
#iterations: 138
currently lose_sum: 95.59400397539139
time_elpased: 1.998
batch start
#iterations: 139
currently lose_sum: 95.17399299144745
time_elpased: 2.094
start validation test
0.652268041237
0.636564281773
0.712433100041
0.672365225838
0.652168635935
61.642
batch start
#iterations: 140
currently lose_sum: 95.19161993265152
time_elpased: 1.971
batch start
#iterations: 141
currently lose_sum: 95.36239975690842
time_elpased: 2.102
batch start
#iterations: 142
currently lose_sum: 95.54736918210983
time_elpased: 2.063
batch start
#iterations: 143
currently lose_sum: 95.53128963708878
time_elpased: 2.087
batch start
#iterations: 144
currently lose_sum: 95.45258861780167
time_elpased: 2.095
batch start
#iterations: 145
currently lose_sum: 95.2825887799263
time_elpased: 2.03
batch start
#iterations: 146
currently lose_sum: 95.10963445901871
time_elpased: 2.015
batch start
#iterations: 147
currently lose_sum: 95.36086225509644
time_elpased: 2.069
batch start
#iterations: 148
currently lose_sum: 95.61929285526276
time_elpased: 2.044
batch start
#iterations: 149
currently lose_sum: 95.25012093782425
time_elpased: 2.09
batch start
#iterations: 150
currently lose_sum: 95.47260338068008
time_elpased: 2.071
batch start
#iterations: 151
currently lose_sum: 95.59950143098831
time_elpased: 2.029
batch start
#iterations: 152
currently lose_sum: 95.47210425138474
time_elpased: 2.087
batch start
#iterations: 153
currently lose_sum: 95.51986515522003
time_elpased: 2.003
batch start
#iterations: 154
currently lose_sum: 95.78086721897125
time_elpased: 2.034
batch start
#iterations: 155
currently lose_sum: 95.17575305700302
time_elpased: 2.007
batch start
#iterations: 156
currently lose_sum: 95.48631620407104
time_elpased: 2.008
batch start
#iterations: 157
currently lose_sum: 95.25049269199371
time_elpased: 1.894
batch start
#iterations: 158
currently lose_sum: 95.40807908773422
time_elpased: 1.934
batch start
#iterations: 159
currently lose_sum: 95.41218209266663
time_elpased: 1.977
start validation test
0.657577319588
0.635338677002
0.742383696995
0.684702643695
0.657437201657
61.860
batch start
#iterations: 160
currently lose_sum: 95.29142820835114
time_elpased: 2.004
batch start
#iterations: 161
currently lose_sum: 95.5720551609993
time_elpased: 2.013
batch start
#iterations: 162
currently lose_sum: 95.52341705560684
time_elpased: 1.967
batch start
#iterations: 163
currently lose_sum: 95.41002798080444
time_elpased: 1.913
batch start
#iterations: 164
currently lose_sum: 95.43974488973618
time_elpased: 1.98
batch start
#iterations: 165
currently lose_sum: 95.3525812625885
time_elpased: 1.993
batch start
#iterations: 166
currently lose_sum: 95.0835337638855
time_elpased: 2.11
batch start
#iterations: 167
currently lose_sum: 94.95130574703217
time_elpased: 2.03
batch start
#iterations: 168
currently lose_sum: 95.45136201381683
time_elpased: 2.025
batch start
#iterations: 169
currently lose_sum: 95.51290971040726
time_elpased: 1.994
batch start
#iterations: 170
currently lose_sum: 95.08401364088058
time_elpased: 2.025
batch start
#iterations: 171
currently lose_sum: 95.5692852139473
time_elpased: 1.96
batch start
#iterations: 172
currently lose_sum: 95.18762999773026
time_elpased: 2.064
batch start
#iterations: 173
currently lose_sum: 95.33374571800232
time_elpased: 2.109
batch start
#iterations: 174
currently lose_sum: 94.8327567577362
time_elpased: 2.097
batch start
#iterations: 175
currently lose_sum: 95.01275557279587
time_elpased: 2.181
batch start
#iterations: 176
currently lose_sum: 95.45419436693192
time_elpased: 2.17
batch start
#iterations: 177
currently lose_sum: 95.53952813148499
time_elpased: 2.063
batch start
#iterations: 178
currently lose_sum: 95.07715147733688
time_elpased: 2.128
batch start
#iterations: 179
currently lose_sum: 95.16953778266907
time_elpased: 1.976
start validation test
0.657113402062
0.634645807699
0.743207081103
0.684649663411
0.656971157239
61.706
batch start
#iterations: 180
currently lose_sum: 94.99597662687302
time_elpased: 2.134
batch start
#iterations: 181
currently lose_sum: 95.56856203079224
time_elpased: 2.151
batch start
#iterations: 182
currently lose_sum: 95.485087454319
time_elpased: 2.158
batch start
#iterations: 183
currently lose_sum: 95.14164263010025
time_elpased: 2.129
batch start
#iterations: 184
currently lose_sum: 95.26589524745941
time_elpased: 2.18
batch start
#iterations: 185
currently lose_sum: 95.24478828907013
time_elpased: 2.099
batch start
#iterations: 186
currently lose_sum: 94.91400426626205
time_elpased: 2.099
batch start
#iterations: 187
currently lose_sum: 95.52630090713501
time_elpased: 2.194
batch start
#iterations: 188
currently lose_sum: 95.33579689264297
time_elpased: 2.137
batch start
#iterations: 189
currently lose_sum: 95.50244051218033
time_elpased: 2.138
batch start
#iterations: 190
currently lose_sum: 95.4810379743576
time_elpased: 2.186
batch start
#iterations: 191
currently lose_sum: 95.30613940954208
time_elpased: 2.146
batch start
#iterations: 192
currently lose_sum: 95.18731677532196
time_elpased: 2.15
batch start
#iterations: 193
currently lose_sum: 95.00534880161285
time_elpased: 2.145
batch start
#iterations: 194
currently lose_sum: 95.20265239477158
time_elpased: 2.126
batch start
#iterations: 195
currently lose_sum: 95.4746168255806
time_elpased: 2.117
batch start
#iterations: 196
currently lose_sum: 94.97768813371658
time_elpased: 2.213
batch start
#iterations: 197
currently lose_sum: 94.99976271390915
time_elpased: 2.202
batch start
#iterations: 198
currently lose_sum: 94.94759726524353
time_elpased: 2.14
batch start
#iterations: 199
currently lose_sum: 94.76369655132294
time_elpased: 2.185
start validation test
0.657886597938
0.641797918394
0.717167558666
0.677392699169
0.657788653352
61.705
batch start
#iterations: 200
currently lose_sum: 95.48133605718613
time_elpased: 2.203
batch start
#iterations: 201
currently lose_sum: 94.99781024456024
time_elpased: 2.232
batch start
#iterations: 202
currently lose_sum: 95.28523224592209
time_elpased: 2.325
batch start
#iterations: 203
currently lose_sum: 95.26627314090729
time_elpased: 2.24
batch start
#iterations: 204
currently lose_sum: 95.43368190526962
time_elpased: 2.353
batch start
#iterations: 205
currently lose_sum: 95.48519426584244
time_elpased: 2.357
batch start
#iterations: 206
currently lose_sum: 95.09412509202957
time_elpased: 2.397
batch start
#iterations: 207
currently lose_sum: 94.96571052074432
time_elpased: 2.392
batch start
#iterations: 208
currently lose_sum: 95.21140396595001
time_elpased: 2.42
batch start
#iterations: 209
currently lose_sum: 94.94731545448303
time_elpased: 2.308
batch start
#iterations: 210
currently lose_sum: 95.17131841182709
time_elpased: 2.356
batch start
#iterations: 211
currently lose_sum: 94.61967122554779
time_elpased: 2.289
batch start
#iterations: 212
currently lose_sum: 95.20409125089645
time_elpased: 2.247
batch start
#iterations: 213
currently lose_sum: 95.26966780424118
time_elpased: 2.203
batch start
#iterations: 214
currently lose_sum: 95.27475500106812
time_elpased: 2.226
batch start
#iterations: 215
currently lose_sum: 94.73571008443832
time_elpased: 2.331
batch start
#iterations: 216
currently lose_sum: 94.67203056812286
time_elpased: 2.347
batch start
#iterations: 217
currently lose_sum: 95.3121754527092
time_elpased: 2.371
batch start
#iterations: 218
currently lose_sum: 95.10986840724945
time_elpased: 2.375
batch start
#iterations: 219
currently lose_sum: 95.27045428752899
time_elpased: 2.316
start validation test
0.656443298969
0.638241957408
0.724886784685
0.678810659727
0.656330215969
61.646
batch start
#iterations: 220
currently lose_sum: 95.17484998703003
time_elpased: 2.397
batch start
#iterations: 221
currently lose_sum: 94.9936311841011
time_elpased: 2.242
batch start
#iterations: 222
currently lose_sum: 95.03457522392273
time_elpased: 2.373
batch start
#iterations: 223
currently lose_sum: 94.83918142318726
time_elpased: 2.242
batch start
#iterations: 224
currently lose_sum: 94.94752049446106
time_elpased: 2.269
batch start
#iterations: 225
currently lose_sum: 95.09775811433792
time_elpased: 2.317
batch start
#iterations: 226
currently lose_sum: 95.07386338710785
time_elpased: 2.234
batch start
#iterations: 227
currently lose_sum: 95.07319939136505
time_elpased: 2.218
batch start
#iterations: 228
currently lose_sum: 94.96304649114609
time_elpased: 2.226
batch start
#iterations: 229
currently lose_sum: 94.79062056541443
time_elpased: 2.227
batch start
#iterations: 230
currently lose_sum: 94.99423444271088
time_elpased: 2.174
batch start
#iterations: 231
currently lose_sum: 95.22544586658478
time_elpased: 2.075
batch start
#iterations: 232
currently lose_sum: 95.64105135202408
time_elpased: 2.15
batch start
#iterations: 233
currently lose_sum: 95.32187885046005
time_elpased: 2.224
batch start
#iterations: 234
currently lose_sum: 95.2680150270462
time_elpased: 2.189
batch start
#iterations: 235
currently lose_sum: 95.33000886440277
time_elpased: 2.301
batch start
#iterations: 236
currently lose_sum: 95.0722668170929
time_elpased: 2.277
batch start
#iterations: 237
currently lose_sum: 94.81752920150757
time_elpased: 2.141
batch start
#iterations: 238
currently lose_sum: 95.23325192928314
time_elpased: 2.191
batch start
#iterations: 239
currently lose_sum: 95.3037394285202
time_elpased: 2.208
start validation test
0.657628865979
0.641894387001
0.715623713462
0.676756861982
0.657533046322
61.653
batch start
#iterations: 240
currently lose_sum: 95.03947222232819
time_elpased: 2.107
batch start
#iterations: 241
currently lose_sum: 94.73946744203568
time_elpased: 2.203
batch start
#iterations: 242
currently lose_sum: 95.10974723100662
time_elpased: 2.176
batch start
#iterations: 243
currently lose_sum: 95.21582448482513
time_elpased: 2.188
batch start
#iterations: 244
currently lose_sum: 95.03111499547958
time_elpased: 2.221
batch start
#iterations: 245
currently lose_sum: 95.41774266958237
time_elpased: 2.204
batch start
#iterations: 246
currently lose_sum: 95.09642791748047
time_elpased: 2.168
batch start
#iterations: 247
currently lose_sum: 95.05377531051636
time_elpased: 2.152
batch start
#iterations: 248
currently lose_sum: 95.1377717256546
time_elpased: 2.125
batch start
#iterations: 249
currently lose_sum: 94.86434179544449
time_elpased: 2.027
batch start
#iterations: 250
currently lose_sum: 95.04274713993073
time_elpased: 2.217
batch start
#iterations: 251
currently lose_sum: 94.81730055809021
time_elpased: 2.185
batch start
#iterations: 252
currently lose_sum: 95.43520748615265
time_elpased: 2.218
batch start
#iterations: 253
currently lose_sum: 95.00188761949539
time_elpased: 2.198
batch start
#iterations: 254
currently lose_sum: 95.09740787744522
time_elpased: 2.216
batch start
#iterations: 255
currently lose_sum: 94.77376627922058
time_elpased: 2.221
batch start
#iterations: 256
currently lose_sum: 95.00335830450058
time_elpased: 2.261
batch start
#iterations: 257
currently lose_sum: 94.97749954462051
time_elpased: 2.067
batch start
#iterations: 258
currently lose_sum: 94.95188242197037
time_elpased: 2.056
batch start
#iterations: 259
currently lose_sum: 95.12950164079666
time_elpased: 2.026
start validation test
0.651804123711
0.642561386615
0.686805269658
0.66394706731
0.651746294474
61.749
batch start
#iterations: 260
currently lose_sum: 95.01401084661484
time_elpased: 2.048
batch start
#iterations: 261
currently lose_sum: 95.00092166662216
time_elpased: 2.146
batch start
#iterations: 262
currently lose_sum: 95.0010712146759
time_elpased: 2.137
batch start
#iterations: 263
currently lose_sum: 94.66235929727554
time_elpased: 2.039
batch start
#iterations: 264
currently lose_sum: 95.212317943573
time_elpased: 2.052
batch start
#iterations: 265
currently lose_sum: 95.20093148946762
time_elpased: 1.999
batch start
#iterations: 266
currently lose_sum: 95.13472056388855
time_elpased: 2.081
batch start
#iterations: 267
currently lose_sum: 94.97493726015091
time_elpased: 2.043
batch start
#iterations: 268
currently lose_sum: 94.93019670248032
time_elpased: 2.052
batch start
#iterations: 269
currently lose_sum: 95.10456717014313
time_elpased: 2.075
batch start
#iterations: 270
currently lose_sum: 94.68878614902496
time_elpased: 2.126
batch start
#iterations: 271
currently lose_sum: 94.96130955219269
time_elpased: 2.064
batch start
#iterations: 272
currently lose_sum: 95.29363131523132
time_elpased: 2.003
batch start
#iterations: 273
currently lose_sum: 94.91873693466187
time_elpased: 1.941
batch start
#iterations: 274
currently lose_sum: 95.17156994342804
time_elpased: 2.062
batch start
#iterations: 275
currently lose_sum: 95.16136187314987
time_elpased: 2.039
batch start
#iterations: 276
currently lose_sum: 94.96146982908249
time_elpased: 2.039
batch start
#iterations: 277
currently lose_sum: 94.74505120515823
time_elpased: 2.155
batch start
#iterations: 278
currently lose_sum: 95.11903834342957
time_elpased: 2.19
batch start
#iterations: 279
currently lose_sum: 95.06605660915375
time_elpased: 2.147
start validation test
0.658402061856
0.636572641259
0.740942774804
0.684803804994
0.658265687278
61.611
batch start
#iterations: 280
currently lose_sum: 94.89283937215805
time_elpased: 2.0
batch start
#iterations: 281
currently lose_sum: 94.85466837882996
time_elpased: 2.063
batch start
#iterations: 282
currently lose_sum: 95.05274379253387
time_elpased: 2.04
batch start
#iterations: 283
currently lose_sum: 95.26061588525772
time_elpased: 2.015
batch start
#iterations: 284
currently lose_sum: 94.72127109766006
time_elpased: 2.057
batch start
#iterations: 285
currently lose_sum: 94.9689809679985
time_elpased: 1.992
batch start
#iterations: 286
currently lose_sum: 94.75336533784866
time_elpased: 2.048
batch start
#iterations: 287
currently lose_sum: 95.0326880812645
time_elpased: 1.985
batch start
#iterations: 288
currently lose_sum: 94.89237177371979
time_elpased: 2.012
batch start
#iterations: 289
currently lose_sum: 94.80102932453156
time_elpased: 2.05
batch start
#iterations: 290
currently lose_sum: 94.73601478338242
time_elpased: 2.107
batch start
#iterations: 291
currently lose_sum: 95.09352093935013
time_elpased: 2.059
batch start
#iterations: 292
currently lose_sum: 94.76409631967545
time_elpased: 2.001
batch start
#iterations: 293
currently lose_sum: 94.92464649677277
time_elpased: 1.984
batch start
#iterations: 294
currently lose_sum: 95.1493211388588
time_elpased: 2.007
batch start
#iterations: 295
currently lose_sum: 94.85669231414795
time_elpased: 2.014
batch start
#iterations: 296
currently lose_sum: 95.17835861444473
time_elpased: 2.047
batch start
#iterations: 297
currently lose_sum: 95.21008789539337
time_elpased: 2.091
batch start
#iterations: 298
currently lose_sum: 94.74256896972656
time_elpased: 1.983
batch start
#iterations: 299
currently lose_sum: 94.87994140386581
time_elpased: 2.047
start validation test
0.655618556701
0.635261609769
0.733532317826
0.680869357535
0.655489826819
61.695
batch start
#iterations: 300
currently lose_sum: 95.16840279102325
time_elpased: 2.037
batch start
#iterations: 301
currently lose_sum: 94.95083194971085
time_elpased: 2.017
batch start
#iterations: 302
currently lose_sum: 94.65784823894501
time_elpased: 1.962
batch start
#iterations: 303
currently lose_sum: 94.58999300003052
time_elpased: 2.016
batch start
#iterations: 304
currently lose_sum: 94.51090830564499
time_elpased: 2.051
batch start
#iterations: 305
currently lose_sum: 95.05202567577362
time_elpased: 2.003
batch start
#iterations: 306
currently lose_sum: 95.1001855134964
time_elpased: 2.005
batch start
#iterations: 307
currently lose_sum: 95.20112240314484
time_elpased: 2.013
batch start
#iterations: 308
currently lose_sum: 94.83665877580643
time_elpased: 2.022
batch start
#iterations: 309
currently lose_sum: 94.60607725381851
time_elpased: 1.948
batch start
#iterations: 310
currently lose_sum: 94.74324655532837
time_elpased: 2.057
batch start
#iterations: 311
currently lose_sum: 94.55120807886124
time_elpased: 1.999
batch start
#iterations: 312
currently lose_sum: 95.08815962076187
time_elpased: 2.013
batch start
#iterations: 313
currently lose_sum: 94.72554367780685
time_elpased: 2.093
batch start
#iterations: 314
currently lose_sum: 94.9305567741394
time_elpased: 2.021
batch start
#iterations: 315
currently lose_sum: 94.44200509786606
time_elpased: 2.043
batch start
#iterations: 316
currently lose_sum: 94.74768602848053
time_elpased: 1.964
batch start
#iterations: 317
currently lose_sum: 94.60617744922638
time_elpased: 2.034
batch start
#iterations: 318
currently lose_sum: 94.53525793552399
time_elpased: 2.058
batch start
#iterations: 319
currently lose_sum: 94.74109560251236
time_elpased: 2.025
start validation test
0.656082474227
0.641423527225
0.710477562783
0.674186932318
0.655992602127
61.570
batch start
#iterations: 320
currently lose_sum: 94.99701029062271
time_elpased: 2.036
batch start
#iterations: 321
currently lose_sum: 94.79133039712906
time_elpased: 2.056
batch start
#iterations: 322
currently lose_sum: 94.82741475105286
time_elpased: 2.027
batch start
#iterations: 323
currently lose_sum: 95.07187104225159
time_elpased: 1.936
batch start
#iterations: 324
currently lose_sum: 95.00260770320892
time_elpased: 2.026
batch start
#iterations: 325
currently lose_sum: 94.7369155883789
time_elpased: 2.095
batch start
#iterations: 326
currently lose_sum: 94.34389936923981
time_elpased: 2.056
batch start
#iterations: 327
currently lose_sum: 95.1029264330864
time_elpased: 2.04
batch start
#iterations: 328
currently lose_sum: 94.73340576887131
time_elpased: 2.029
batch start
#iterations: 329
currently lose_sum: 95.02528792619705
time_elpased: 2.019
batch start
#iterations: 330
currently lose_sum: 94.7299929857254
time_elpased: 1.949
batch start
#iterations: 331
currently lose_sum: 94.98019552230835
time_elpased: 2.032
batch start
#iterations: 332
currently lose_sum: 94.92822754383087
time_elpased: 2.027
batch start
#iterations: 333
currently lose_sum: 94.77842891216278
time_elpased: 2.049
batch start
#iterations: 334
currently lose_sum: 94.8860799074173
time_elpased: 2.049
batch start
#iterations: 335
currently lose_sum: 94.60908561944962
time_elpased: 2.064
batch start
#iterations: 336
currently lose_sum: 94.59818518161774
time_elpased: 2.01
batch start
#iterations: 337
currently lose_sum: 94.84485548734665
time_elpased: 2.033
batch start
#iterations: 338
currently lose_sum: 94.54790306091309
time_elpased: 1.973
batch start
#iterations: 339
currently lose_sum: 94.72053337097168
time_elpased: 2.033
start validation test
0.65675257732
0.637789597043
0.72818032112
0.679994233264
0.656634563699
61.642
batch start
#iterations: 340
currently lose_sum: 95.1559357047081
time_elpased: 2.027
batch start
#iterations: 341
currently lose_sum: 94.63866049051285
time_elpased: 2.044
batch start
#iterations: 342
currently lose_sum: 94.92572337388992
time_elpased: 2.037
batch start
#iterations: 343
currently lose_sum: 95.38799649477005
time_elpased: 2.093
batch start
#iterations: 344
currently lose_sum: 95.10382509231567
time_elpased: 2.047
batch start
#iterations: 345
currently lose_sum: 95.04869335889816
time_elpased: 1.983
batch start
#iterations: 346
currently lose_sum: 94.61590152978897
time_elpased: 2.029
batch start
#iterations: 347
currently lose_sum: 94.87200450897217
time_elpased: 2.037
batch start
#iterations: 348
currently lose_sum: 94.9747862815857
time_elpased: 2.112
batch start
#iterations: 349
currently lose_sum: 94.9660450220108
time_elpased: 2.035
batch start
#iterations: 350
currently lose_sum: 94.68922609090805
time_elpased: 2.088
batch start
#iterations: 351
currently lose_sum: 95.08373373746872
time_elpased: 2.031
batch start
#iterations: 352
currently lose_sum: 94.84532535076141
time_elpased: 2.041
batch start
#iterations: 353
currently lose_sum: 95.3486739397049
time_elpased: 2.026
batch start
#iterations: 354
currently lose_sum: 94.69741141796112
time_elpased: 2.07
batch start
#iterations: 355
currently lose_sum: 94.54326641559601
time_elpased: 1.978
batch start
#iterations: 356
currently lose_sum: 94.75347018241882
time_elpased: 2.071
batch start
#iterations: 357
currently lose_sum: 94.7102182507515
time_elpased: 1.994
batch start
#iterations: 358
currently lose_sum: 94.74585562944412
time_elpased: 1.996
batch start
#iterations: 359
currently lose_sum: 94.6253707408905
time_elpased: 1.944
start validation test
0.65881443299
0.639316239316
0.731370934541
0.68225241227
0.658694554425
61.613
batch start
#iterations: 360
currently lose_sum: 95.16875940561295
time_elpased: 2.087
batch start
#iterations: 361
currently lose_sum: 94.89332711696625
time_elpased: 2.066
batch start
#iterations: 362
currently lose_sum: 94.96173292398453
time_elpased: 2.039
batch start
#iterations: 363
currently lose_sum: 94.76111382246017
time_elpased: 2.013
batch start
#iterations: 364
currently lose_sum: 94.49474501609802
time_elpased: 2.028
batch start
#iterations: 365
currently lose_sum: 95.08532238006592
time_elpased: 2.049
batch start
#iterations: 366
currently lose_sum: 94.8882703781128
time_elpased: 1.964
batch start
#iterations: 367
currently lose_sum: 94.64033633470535
time_elpased: 2.066
batch start
#iterations: 368
currently lose_sum: 94.89708995819092
time_elpased: 2.067
batch start
#iterations: 369
currently lose_sum: 94.82315462827682
time_elpased: 2.018
batch start
#iterations: 370
currently lose_sum: 94.83855539560318
time_elpased: 2.052
batch start
#iterations: 371
currently lose_sum: 94.37571036815643
time_elpased: 2.017
batch start
#iterations: 372
currently lose_sum: 94.75208246707916
time_elpased: 2.05
batch start
#iterations: 373
currently lose_sum: 94.86054366827011
time_elpased: 1.969
batch start
#iterations: 374
currently lose_sum: 94.85449057817459
time_elpased: 1.98
batch start
#iterations: 375
currently lose_sum: 94.62391126155853
time_elpased: 2.054
batch start
#iterations: 376
currently lose_sum: 94.43869316577911
time_elpased: 2.041
batch start
#iterations: 377
currently lose_sum: 94.60827732086182
time_elpased: 2.021
batch start
#iterations: 378
currently lose_sum: 94.90208864212036
time_elpased: 2.045
batch start
#iterations: 379
currently lose_sum: 94.6063015460968
time_elpased: 2.028
start validation test
0.654432989691
0.638904261206
0.712947715109
0.673898239128
0.654336311086
61.733
batch start
#iterations: 380
currently lose_sum: 94.70102655887604
time_elpased: 2.046
batch start
#iterations: 381
currently lose_sum: 94.70566684007645
time_elpased: 1.942
batch start
#iterations: 382
currently lose_sum: 95.01276022195816
time_elpased: 2.144
batch start
#iterations: 383
currently lose_sum: 94.68695431947708
time_elpased: 1.977
batch start
#iterations: 384
currently lose_sum: 94.51771837472916
time_elpased: 2.062
batch start
#iterations: 385
currently lose_sum: 94.82603704929352
time_elpased: 2.017
batch start
#iterations: 386
currently lose_sum: 95.07783019542694
time_elpased: 2.056
batch start
#iterations: 387
currently lose_sum: 94.46932309865952
time_elpased: 1.943
batch start
#iterations: 388
currently lose_sum: 94.67464578151703
time_elpased: 1.98
batch start
#iterations: 389
currently lose_sum: 94.54192006587982
time_elpased: 2.076
batch start
#iterations: 390
currently lose_sum: 94.67491281032562
time_elpased: 2.045
batch start
#iterations: 391
currently lose_sum: 94.23647624254227
time_elpased: 1.988
batch start
#iterations: 392
currently lose_sum: 94.91576272249222
time_elpased: 2.066
batch start
#iterations: 393
currently lose_sum: 94.70760482549667
time_elpased: 1.984
batch start
#iterations: 394
currently lose_sum: 94.53997194766998
time_elpased: 2.033
batch start
#iterations: 395
currently lose_sum: 94.45538187026978
time_elpased: 2.014
batch start
#iterations: 396
currently lose_sum: 94.48284143209457
time_elpased: 2.097
batch start
#iterations: 397
currently lose_sum: 94.66901749372482
time_elpased: 2.013
batch start
#iterations: 398
currently lose_sum: 94.79862475395203
time_elpased: 2.015
batch start
#iterations: 399
currently lose_sum: 94.92720299959183
time_elpased: 2.044
start validation test
0.657474226804
0.635824856258
0.739810621655
0.68388754103
0.657338189803
61.663
acc: 0.665
pre: 0.648
rec: 0.725
F1: 0.684
auc: 0.664
