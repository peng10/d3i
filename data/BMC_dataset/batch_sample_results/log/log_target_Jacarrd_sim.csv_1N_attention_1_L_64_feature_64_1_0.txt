start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.35021907091141
time_elpased: 1.963
batch start
#iterations: 1
currently lose_sum: 99.92460536956787
time_elpased: 1.821
batch start
#iterations: 2
currently lose_sum: 99.84131968021393
time_elpased: 1.872
batch start
#iterations: 3
currently lose_sum: 99.31030309200287
time_elpased: 1.835
batch start
#iterations: 4
currently lose_sum: 99.29401278495789
time_elpased: 1.873
batch start
#iterations: 5
currently lose_sum: 99.0044646859169
time_elpased: 1.845
batch start
#iterations: 6
currently lose_sum: 98.61832720041275
time_elpased: 1.824
batch start
#iterations: 7
currently lose_sum: 98.52197802066803
time_elpased: 1.84
batch start
#iterations: 8
currently lose_sum: 98.41683661937714
time_elpased: 1.868
batch start
#iterations: 9
currently lose_sum: 98.12617528438568
time_elpased: 1.833
batch start
#iterations: 10
currently lose_sum: 97.96789133548737
time_elpased: 1.851
batch start
#iterations: 11
currently lose_sum: 98.07629877328873
time_elpased: 1.828
batch start
#iterations: 12
currently lose_sum: 98.11781561374664
time_elpased: 1.838
batch start
#iterations: 13
currently lose_sum: 97.58783954381943
time_elpased: 1.817
batch start
#iterations: 14
currently lose_sum: 97.64784163236618
time_elpased: 1.86
batch start
#iterations: 15
currently lose_sum: 98.0056904554367
time_elpased: 1.83
batch start
#iterations: 16
currently lose_sum: 97.5733328461647
time_elpased: 1.823
batch start
#iterations: 17
currently lose_sum: 97.52374136447906
time_elpased: 1.845
batch start
#iterations: 18
currently lose_sum: 97.58916026353836
time_elpased: 1.799
batch start
#iterations: 19
currently lose_sum: 97.41025960445404
time_elpased: 1.832
start validation test
0.641546391753
0.638412984671
0.655757949985
0.646969235455
0.64152144117
63.052
batch start
#iterations: 20
currently lose_sum: 97.4992526769638
time_elpased: 1.878
batch start
#iterations: 21
currently lose_sum: 97.1509313583374
time_elpased: 1.864
batch start
#iterations: 22
currently lose_sum: 97.05948001146317
time_elpased: 1.879
batch start
#iterations: 23
currently lose_sum: 97.10210585594177
time_elpased: 1.853
batch start
#iterations: 24
currently lose_sum: 96.93479585647583
time_elpased: 1.855
batch start
#iterations: 25
currently lose_sum: 97.13731080293655
time_elpased: 1.807
batch start
#iterations: 26
currently lose_sum: 96.84683150053024
time_elpased: 1.811
batch start
#iterations: 27
currently lose_sum: 96.75858902931213
time_elpased: 1.846
batch start
#iterations: 28
currently lose_sum: 96.99557727575302
time_elpased: 1.854
batch start
#iterations: 29
currently lose_sum: 96.78614294528961
time_elpased: 1.855
batch start
#iterations: 30
currently lose_sum: 96.85504853725433
time_elpased: 1.84
batch start
#iterations: 31
currently lose_sum: 96.52140718698502
time_elpased: 1.82
batch start
#iterations: 32
currently lose_sum: 96.52506721019745
time_elpased: 1.834
batch start
#iterations: 33
currently lose_sum: 96.85758477449417
time_elpased: 1.837
batch start
#iterations: 34
currently lose_sum: 96.84506493806839
time_elpased: 1.825
batch start
#iterations: 35
currently lose_sum: 96.65514278411865
time_elpased: 1.857
batch start
#iterations: 36
currently lose_sum: 96.92216503620148
time_elpased: 1.819
batch start
#iterations: 37
currently lose_sum: 96.37144428491592
time_elpased: 1.831
batch start
#iterations: 38
currently lose_sum: 96.54536944627762
time_elpased: 1.859
batch start
#iterations: 39
currently lose_sum: 96.55550110340118
time_elpased: 1.843
start validation test
0.641134020619
0.634560906516
0.668519090254
0.651097524306
0.641085941905
62.406
batch start
#iterations: 40
currently lose_sum: 96.51005977392197
time_elpased: 1.93
batch start
#iterations: 41
currently lose_sum: 96.40115058422089
time_elpased: 1.815
batch start
#iterations: 42
currently lose_sum: 96.58490312099457
time_elpased: 1.837
batch start
#iterations: 43
currently lose_sum: 96.36101871728897
time_elpased: 1.843
batch start
#iterations: 44
currently lose_sum: 96.66681754589081
time_elpased: 1.836
batch start
#iterations: 45
currently lose_sum: 96.3780403137207
time_elpased: 1.855
batch start
#iterations: 46
currently lose_sum: 96.07672476768494
time_elpased: 1.849
batch start
#iterations: 47
currently lose_sum: 95.97299212217331
time_elpased: 1.833
batch start
#iterations: 48
currently lose_sum: 96.40123862028122
time_elpased: 1.843
batch start
#iterations: 49
currently lose_sum: 96.71847265958786
time_elpased: 1.873
batch start
#iterations: 50
currently lose_sum: 96.13570535182953
time_elpased: 1.813
batch start
#iterations: 51
currently lose_sum: 96.1188332438469
time_elpased: 1.845
batch start
#iterations: 52
currently lose_sum: 96.05203193426132
time_elpased: 1.888
batch start
#iterations: 53
currently lose_sum: 96.34195744991302
time_elpased: 1.815
batch start
#iterations: 54
currently lose_sum: 96.28508847951889
time_elpased: 1.818
batch start
#iterations: 55
currently lose_sum: 96.23045283555984
time_elpased: 1.83
batch start
#iterations: 56
currently lose_sum: 96.2508829832077
time_elpased: 1.813
batch start
#iterations: 57
currently lose_sum: 95.91169691085815
time_elpased: 1.831
batch start
#iterations: 58
currently lose_sum: 96.23456293344498
time_elpased: 1.837
batch start
#iterations: 59
currently lose_sum: 96.28564071655273
time_elpased: 1.834
start validation test
0.647835051546
0.632327309421
0.709375321601
0.668639053254
0.64772700811
62.305
batch start
#iterations: 60
currently lose_sum: 96.2146081328392
time_elpased: 1.851
batch start
#iterations: 61
currently lose_sum: 96.30974996089935
time_elpased: 1.821
batch start
#iterations: 62
currently lose_sum: 96.23948246240616
time_elpased: 1.832
batch start
#iterations: 63
currently lose_sum: 96.21511095762253
time_elpased: 1.845
batch start
#iterations: 64
currently lose_sum: 96.30147361755371
time_elpased: 1.833
batch start
#iterations: 65
currently lose_sum: 95.76280778646469
time_elpased: 1.854
batch start
#iterations: 66
currently lose_sum: 95.6285988688469
time_elpased: 1.836
batch start
#iterations: 67
currently lose_sum: 95.96434193849564
time_elpased: 1.833
batch start
#iterations: 68
currently lose_sum: 96.51738649606705
time_elpased: 1.844
batch start
#iterations: 69
currently lose_sum: 95.7229700088501
time_elpased: 1.879
batch start
#iterations: 70
currently lose_sum: 95.7568621635437
time_elpased: 1.858
batch start
#iterations: 71
currently lose_sum: 95.66851735115051
time_elpased: 1.839
batch start
#iterations: 72
currently lose_sum: 95.3742601275444
time_elpased: 1.824
batch start
#iterations: 73
currently lose_sum: 95.54946023225784
time_elpased: 1.867
batch start
#iterations: 74
currently lose_sum: 95.72731059789658
time_elpased: 1.826
batch start
#iterations: 75
currently lose_sum: 95.563323199749
time_elpased: 1.829
batch start
#iterations: 76
currently lose_sum: 95.64780288934708
time_elpased: 1.811
batch start
#iterations: 77
currently lose_sum: 95.79353779554367
time_elpased: 1.889
batch start
#iterations: 78
currently lose_sum: 95.70578533411026
time_elpased: 1.838
batch start
#iterations: 79
currently lose_sum: 95.7529234290123
time_elpased: 1.849
start validation test
0.648865979381
0.639141680238
0.686631676443
0.662036118277
0.648799675875
61.972
batch start
#iterations: 80
currently lose_sum: 95.95759576559067
time_elpased: 1.842
batch start
#iterations: 81
currently lose_sum: 95.5716683268547
time_elpased: 1.862
batch start
#iterations: 82
currently lose_sum: 95.7771544456482
time_elpased: 1.886
batch start
#iterations: 83
currently lose_sum: 96.11990934610367
time_elpased: 1.825
batch start
#iterations: 84
currently lose_sum: 95.88846629858017
time_elpased: 1.86
batch start
#iterations: 85
currently lose_sum: 95.65787136554718
time_elpased: 1.846
batch start
#iterations: 86
currently lose_sum: 95.88600790500641
time_elpased: 1.827
batch start
#iterations: 87
currently lose_sum: 95.42668461799622
time_elpased: 1.812
batch start
#iterations: 88
currently lose_sum: 95.58125585317612
time_elpased: 1.856
batch start
#iterations: 89
currently lose_sum: 95.74526292085648
time_elpased: 1.855
batch start
#iterations: 90
currently lose_sum: 95.9391296505928
time_elpased: 1.852
batch start
#iterations: 91
currently lose_sum: 95.51468712091446
time_elpased: 1.817
batch start
#iterations: 92
currently lose_sum: 95.72765147686005
time_elpased: 1.851
batch start
#iterations: 93
currently lose_sum: 95.6093413233757
time_elpased: 1.862
batch start
#iterations: 94
currently lose_sum: 95.86141657829285
time_elpased: 1.835
batch start
#iterations: 95
currently lose_sum: 95.28009235858917
time_elpased: 1.86
batch start
#iterations: 96
currently lose_sum: 95.76208460330963
time_elpased: 1.849
batch start
#iterations: 97
currently lose_sum: 95.58401095867157
time_elpased: 1.854
batch start
#iterations: 98
currently lose_sum: 95.42289596796036
time_elpased: 1.821
batch start
#iterations: 99
currently lose_sum: 95.77486020326614
time_elpased: 1.811
start validation test
0.647474226804
0.634763064244
0.697540393125
0.6646727139
0.647386327927
61.894
batch start
#iterations: 100
currently lose_sum: 95.73723405599594
time_elpased: 1.847
batch start
#iterations: 101
currently lose_sum: 95.53626084327698
time_elpased: 1.833
batch start
#iterations: 102
currently lose_sum: 95.54494249820709
time_elpased: 1.849
batch start
#iterations: 103
currently lose_sum: 95.30986052751541
time_elpased: 1.838
batch start
#iterations: 104
currently lose_sum: 95.23086893558502
time_elpased: 1.828
batch start
#iterations: 105
currently lose_sum: 95.48851299285889
time_elpased: 1.847
batch start
#iterations: 106
currently lose_sum: 95.8522356748581
time_elpased: 1.859
batch start
#iterations: 107
currently lose_sum: 95.24755042791367
time_elpased: 1.836
batch start
#iterations: 108
currently lose_sum: 95.28615897893906
time_elpased: 1.812
batch start
#iterations: 109
currently lose_sum: 95.75292003154755
time_elpased: 1.849
batch start
#iterations: 110
currently lose_sum: 95.32197672128677
time_elpased: 1.838
batch start
#iterations: 111
currently lose_sum: 95.19741016626358
time_elpased: 1.817
batch start
#iterations: 112
currently lose_sum: 95.29026752710342
time_elpased: 1.873
batch start
#iterations: 113
currently lose_sum: 95.18232691287994
time_elpased: 1.836
batch start
#iterations: 114
currently lose_sum: 95.6717923283577
time_elpased: 1.854
batch start
#iterations: 115
currently lose_sum: 95.45049101114273
time_elpased: 1.852
batch start
#iterations: 116
currently lose_sum: 95.52384161949158
time_elpased: 1.857
batch start
#iterations: 117
currently lose_sum: 95.48674321174622
time_elpased: 1.842
batch start
#iterations: 118
currently lose_sum: 95.44159692525864
time_elpased: 1.833
batch start
#iterations: 119
currently lose_sum: 95.37617665529251
time_elpased: 1.831
start validation test
0.652216494845
0.641132864474
0.694247195637
0.666633726963
0.652142703467
61.870
batch start
#iterations: 120
currently lose_sum: 95.53468984365463
time_elpased: 1.842
batch start
#iterations: 121
currently lose_sum: 95.49085932970047
time_elpased: 1.835
batch start
#iterations: 122
currently lose_sum: 95.2672210931778
time_elpased: 1.839
batch start
#iterations: 123
currently lose_sum: 95.60421061515808
time_elpased: 1.822
batch start
#iterations: 124
currently lose_sum: 95.83852237462997
time_elpased: 1.855
batch start
#iterations: 125
currently lose_sum: 95.50915569067001
time_elpased: 1.866
batch start
#iterations: 126
currently lose_sum: 95.54982674121857
time_elpased: 1.807
batch start
#iterations: 127
currently lose_sum: 95.50242495536804
time_elpased: 1.82
batch start
#iterations: 128
currently lose_sum: 95.68107599020004
time_elpased: 1.813
batch start
#iterations: 129
currently lose_sum: 95.2418103814125
time_elpased: 1.804
batch start
#iterations: 130
currently lose_sum: 95.10495781898499
time_elpased: 1.816
batch start
#iterations: 131
currently lose_sum: 95.57184451818466
time_elpased: 1.826
batch start
#iterations: 132
currently lose_sum: 95.28549438714981
time_elpased: 1.823
batch start
#iterations: 133
currently lose_sum: 95.36747699975967
time_elpased: 1.818
batch start
#iterations: 134
currently lose_sum: 95.23664885759354
time_elpased: 1.839
batch start
#iterations: 135
currently lose_sum: 95.713185608387
time_elpased: 1.826
batch start
#iterations: 136
currently lose_sum: 95.26117461919785
time_elpased: 1.808
batch start
#iterations: 137
currently lose_sum: 94.84136962890625
time_elpased: 1.843
batch start
#iterations: 138
currently lose_sum: 95.62886565923691
time_elpased: 1.82
batch start
#iterations: 139
currently lose_sum: 95.47937101125717
time_elpased: 1.792
start validation test
0.651340206186
0.636801630687
0.707317073171
0.670209653827
0.651241930162
61.662
batch start
#iterations: 140
currently lose_sum: 95.4723938703537
time_elpased: 1.963
batch start
#iterations: 141
currently lose_sum: 95.25352507829666
time_elpased: 1.813
batch start
#iterations: 142
currently lose_sum: 95.27163183689117
time_elpased: 1.818
batch start
#iterations: 143
currently lose_sum: 95.14950680732727
time_elpased: 1.82
batch start
#iterations: 144
currently lose_sum: 95.25225311517715
time_elpased: 1.856
batch start
#iterations: 145
currently lose_sum: 95.30626767873764
time_elpased: 1.815
batch start
#iterations: 146
currently lose_sum: 95.60383236408234
time_elpased: 1.831
batch start
#iterations: 147
currently lose_sum: 95.38819599151611
time_elpased: 1.821
batch start
#iterations: 148
currently lose_sum: 95.4551129937172
time_elpased: 1.816
batch start
#iterations: 149
currently lose_sum: 95.26023125648499
time_elpased: 1.81
batch start
#iterations: 150
currently lose_sum: 95.45586997270584
time_elpased: 1.846
batch start
#iterations: 151
currently lose_sum: 95.14173018932343
time_elpased: 1.852
batch start
#iterations: 152
currently lose_sum: 95.32323288917542
time_elpased: 1.849
batch start
#iterations: 153
currently lose_sum: 95.52459162473679
time_elpased: 1.801
batch start
#iterations: 154
currently lose_sum: 95.26428681612015
time_elpased: 1.831
batch start
#iterations: 155
currently lose_sum: 95.37644076347351
time_elpased: 1.821
batch start
#iterations: 156
currently lose_sum: 95.3210060596466
time_elpased: 1.816
batch start
#iterations: 157
currently lose_sum: 95.34315764904022
time_elpased: 1.806
batch start
#iterations: 158
currently lose_sum: 95.32509469985962
time_elpased: 1.813
batch start
#iterations: 159
currently lose_sum: 95.24883991479874
time_elpased: 1.856
start validation test
0.651082474227
0.639504069657
0.695379232273
0.666272247695
0.651004704436
61.691
batch start
#iterations: 160
currently lose_sum: 95.48938244581223
time_elpased: 1.863
batch start
#iterations: 161
currently lose_sum: 95.14107912778854
time_elpased: 1.815
batch start
#iterations: 162
currently lose_sum: 95.30763906240463
time_elpased: 1.817
batch start
#iterations: 163
currently lose_sum: 95.48238414525986
time_elpased: 1.843
batch start
#iterations: 164
currently lose_sum: 95.11094403266907
time_elpased: 1.833
batch start
#iterations: 165
currently lose_sum: 95.12246018648148
time_elpased: 1.813
batch start
#iterations: 166
currently lose_sum: 95.56078553199768
time_elpased: 1.818
batch start
#iterations: 167
currently lose_sum: 95.14641481637955
time_elpased: 1.813
batch start
#iterations: 168
currently lose_sum: 95.1007929444313
time_elpased: 1.819
batch start
#iterations: 169
currently lose_sum: 95.2634488940239
time_elpased: 1.802
batch start
#iterations: 170
currently lose_sum: 94.99278384447098
time_elpased: 1.817
batch start
#iterations: 171
currently lose_sum: 95.03059875965118
time_elpased: 1.828
batch start
#iterations: 172
currently lose_sum: 94.9395781159401
time_elpased: 1.818
batch start
#iterations: 173
currently lose_sum: 94.8754146695137
time_elpased: 1.98
batch start
#iterations: 174
currently lose_sum: 95.13474541902542
time_elpased: 1.828
batch start
#iterations: 175
currently lose_sum: 95.1556208729744
time_elpased: 1.829
batch start
#iterations: 176
currently lose_sum: 95.6824221611023
time_elpased: 1.839
batch start
#iterations: 177
currently lose_sum: 95.22577661275864
time_elpased: 1.878
batch start
#iterations: 178
currently lose_sum: 94.92151415348053
time_elpased: 1.836
batch start
#iterations: 179
currently lose_sum: 95.01932990550995
time_elpased: 1.831
start validation test
0.649690721649
0.640879714479
0.683750128641
0.661621190998
0.649630925107
61.698
batch start
#iterations: 180
currently lose_sum: 95.05363500118256
time_elpased: 1.892
batch start
#iterations: 181
currently lose_sum: 95.03725039958954
time_elpased: 1.84
batch start
#iterations: 182
currently lose_sum: 95.29575800895691
time_elpased: 1.801
batch start
#iterations: 183
currently lose_sum: 94.64025545120239
time_elpased: 1.825
batch start
#iterations: 184
currently lose_sum: 95.09868425130844
time_elpased: 1.826
batch start
#iterations: 185
currently lose_sum: 94.95425033569336
time_elpased: 1.832
batch start
#iterations: 186
currently lose_sum: 94.99091988801956
time_elpased: 1.872
batch start
#iterations: 187
currently lose_sum: 94.78267639875412
time_elpased: 1.864
batch start
#iterations: 188
currently lose_sum: 94.87212055921555
time_elpased: 1.851
batch start
#iterations: 189
currently lose_sum: 94.98374390602112
time_elpased: 1.858
batch start
#iterations: 190
currently lose_sum: 94.91680181026459
time_elpased: 1.823
batch start
#iterations: 191
currently lose_sum: 95.13160693645477
time_elpased: 1.857
batch start
#iterations: 192
currently lose_sum: 94.66264992952347
time_elpased: 1.863
batch start
#iterations: 193
currently lose_sum: 94.90211927890778
time_elpased: 1.852
batch start
#iterations: 194
currently lose_sum: 94.81239473819733
time_elpased: 1.841
batch start
#iterations: 195
currently lose_sum: 95.12732428312302
time_elpased: 1.835
batch start
#iterations: 196
currently lose_sum: 94.96445685625076
time_elpased: 1.858
batch start
#iterations: 197
currently lose_sum: 95.26563757658005
time_elpased: 1.818
batch start
#iterations: 198
currently lose_sum: 95.2251740694046
time_elpased: 1.827
batch start
#iterations: 199
currently lose_sum: 95.46800655126572
time_elpased: 1.84
start validation test
0.653402061856
0.639403819283
0.706390861377
0.671230197536
0.653309031845
61.779
batch start
#iterations: 200
currently lose_sum: 95.0716644525528
time_elpased: 1.851
batch start
#iterations: 201
currently lose_sum: 95.09566700458527
time_elpased: 1.838
batch start
#iterations: 202
currently lose_sum: 95.15267086029053
time_elpased: 1.828
batch start
#iterations: 203
currently lose_sum: 95.25151413679123
time_elpased: 1.829
batch start
#iterations: 204
currently lose_sum: 94.89049637317657
time_elpased: 1.834
batch start
#iterations: 205
currently lose_sum: 94.88437336683273
time_elpased: 1.834
batch start
#iterations: 206
currently lose_sum: 94.77937710285187
time_elpased: 1.822
batch start
#iterations: 207
currently lose_sum: 95.05721020698547
time_elpased: 1.823
batch start
#iterations: 208
currently lose_sum: 94.91306734085083
time_elpased: 1.83
batch start
#iterations: 209
currently lose_sum: 95.120596408844
time_elpased: 1.821
batch start
#iterations: 210
currently lose_sum: 94.69038873910904
time_elpased: 1.841
batch start
#iterations: 211
currently lose_sum: 94.85746878385544
time_elpased: 1.813
batch start
#iterations: 212
currently lose_sum: 94.77663642168045
time_elpased: 1.835
batch start
#iterations: 213
currently lose_sum: 94.8498545885086
time_elpased: 1.832
batch start
#iterations: 214
currently lose_sum: 95.06662690639496
time_elpased: 1.838
batch start
#iterations: 215
currently lose_sum: 95.06753498315811
time_elpased: 1.846
batch start
#iterations: 216
currently lose_sum: 94.9974564909935
time_elpased: 1.839
batch start
#iterations: 217
currently lose_sum: 94.76364779472351
time_elpased: 1.822
batch start
#iterations: 218
currently lose_sum: 95.2708066701889
time_elpased: 1.846
batch start
#iterations: 219
currently lose_sum: 95.32035356760025
time_elpased: 1.819
start validation test
0.65381443299
0.641596678305
0.699701553978
0.669390568081
0.653733871071
61.617
batch start
#iterations: 220
currently lose_sum: 95.0692109465599
time_elpased: 1.855
batch start
#iterations: 221
currently lose_sum: 94.96953111886978
time_elpased: 1.881
batch start
#iterations: 222
currently lose_sum: 94.97985708713531
time_elpased: 1.837
batch start
#iterations: 223
currently lose_sum: 95.09501826763153
time_elpased: 1.834
batch start
#iterations: 224
currently lose_sum: 95.07175266742706
time_elpased: 1.836
batch start
#iterations: 225
currently lose_sum: 94.85675489902496
time_elpased: 1.823
batch start
#iterations: 226
currently lose_sum: 95.02032697200775
time_elpased: 1.827
batch start
#iterations: 227
currently lose_sum: 94.90186983346939
time_elpased: 1.826
batch start
#iterations: 228
currently lose_sum: 94.81349730491638
time_elpased: 1.811
batch start
#iterations: 229
currently lose_sum: 95.17202454805374
time_elpased: 1.836
batch start
#iterations: 230
currently lose_sum: 94.31017053127289
time_elpased: 1.842
batch start
#iterations: 231
currently lose_sum: 94.54637801647186
time_elpased: 1.839
batch start
#iterations: 232
currently lose_sum: 94.97300243377686
time_elpased: 1.853
batch start
#iterations: 233
currently lose_sum: 95.36898773908615
time_elpased: 1.84
batch start
#iterations: 234
currently lose_sum: 94.81796377897263
time_elpased: 1.823
batch start
#iterations: 235
currently lose_sum: 94.86158436536789
time_elpased: 1.832
batch start
#iterations: 236
currently lose_sum: 95.15480470657349
time_elpased: 1.852
batch start
#iterations: 237
currently lose_sum: 94.72552335262299
time_elpased: 1.82
batch start
#iterations: 238
currently lose_sum: 95.10331726074219
time_elpased: 1.918
batch start
#iterations: 239
currently lose_sum: 94.62612181901932
time_elpased: 1.84
start validation test
0.656134020619
0.645853284811
0.694041370793
0.66908080758
0.656067468419
61.492
batch start
#iterations: 240
currently lose_sum: 94.94162595272064
time_elpased: 1.864
batch start
#iterations: 241
currently lose_sum: 95.07314503192902
time_elpased: 1.835
batch start
#iterations: 242
currently lose_sum: 94.45909786224365
time_elpased: 1.836
batch start
#iterations: 243
currently lose_sum: 94.77227532863617
time_elpased: 1.84
batch start
#iterations: 244
currently lose_sum: 95.13852572441101
time_elpased: 1.828
batch start
#iterations: 245
currently lose_sum: 95.3470795750618
time_elpased: 1.863
batch start
#iterations: 246
currently lose_sum: 94.943319439888
time_elpased: 1.825
batch start
#iterations: 247
currently lose_sum: 94.89286476373672
time_elpased: 1.829
batch start
#iterations: 248
currently lose_sum: 95.46801233291626
time_elpased: 1.872
batch start
#iterations: 249
currently lose_sum: 95.00069332122803
time_elpased: 1.843
batch start
#iterations: 250
currently lose_sum: 94.94200325012207
time_elpased: 1.844
batch start
#iterations: 251
currently lose_sum: 94.73495727777481
time_elpased: 1.864
batch start
#iterations: 252
currently lose_sum: 95.02275800704956
time_elpased: 1.839
batch start
#iterations: 253
currently lose_sum: 95.20248311758041
time_elpased: 1.916
batch start
#iterations: 254
currently lose_sum: 94.62865960597992
time_elpased: 1.828
batch start
#iterations: 255
currently lose_sum: 94.62406796216965
time_elpased: 1.815
batch start
#iterations: 256
currently lose_sum: 94.47763782739639
time_elpased: 1.817
batch start
#iterations: 257
currently lose_sum: 94.74662339687347
time_elpased: 1.865
batch start
#iterations: 258
currently lose_sum: 94.66701656579971
time_elpased: 1.836
batch start
#iterations: 259
currently lose_sum: 94.79036051034927
time_elpased: 1.838
start validation test
0.651804123711
0.636522861357
0.71061027066
0.67152929735
0.65170088045
61.593
batch start
#iterations: 260
currently lose_sum: 94.90455025434494
time_elpased: 1.867
batch start
#iterations: 261
currently lose_sum: 94.88487029075623
time_elpased: 1.818
batch start
#iterations: 262
currently lose_sum: 94.59954315423965
time_elpased: 1.869
batch start
#iterations: 263
currently lose_sum: 94.89890050888062
time_elpased: 1.798
batch start
#iterations: 264
currently lose_sum: 94.79204601049423
time_elpased: 1.884
batch start
#iterations: 265
currently lose_sum: 95.01111418008804
time_elpased: 1.834
batch start
#iterations: 266
currently lose_sum: 94.7539764046669
time_elpased: 1.861
batch start
#iterations: 267
currently lose_sum: 94.65233713388443
time_elpased: 1.849
batch start
#iterations: 268
currently lose_sum: 94.60033041238785
time_elpased: 1.836
batch start
#iterations: 269
currently lose_sum: 94.96402406692505
time_elpased: 1.841
batch start
#iterations: 270
currently lose_sum: 95.31336671113968
time_elpased: 1.826
batch start
#iterations: 271
currently lose_sum: 94.86535984277725
time_elpased: 1.847
batch start
#iterations: 272
currently lose_sum: 95.0672824382782
time_elpased: 1.84
batch start
#iterations: 273
currently lose_sum: 94.53861576318741
time_elpased: 1.818
batch start
#iterations: 274
currently lose_sum: 94.87943434715271
time_elpased: 1.864
batch start
#iterations: 275
currently lose_sum: 94.93711423873901
time_elpased: 1.833
batch start
#iterations: 276
currently lose_sum: 95.37802678346634
time_elpased: 1.864
batch start
#iterations: 277
currently lose_sum: 94.99450236558914
time_elpased: 1.84
batch start
#iterations: 278
currently lose_sum: 94.80922418832779
time_elpased: 1.826
batch start
#iterations: 279
currently lose_sum: 95.12599182128906
time_elpased: 1.801
start validation test
0.646546391753
0.636268343816
0.687146238551
0.660729305824
0.646475112459
61.583
batch start
#iterations: 280
currently lose_sum: 94.79412460327148
time_elpased: 1.848
batch start
#iterations: 281
currently lose_sum: 95.06332129240036
time_elpased: 1.8
batch start
#iterations: 282
currently lose_sum: 94.89131224155426
time_elpased: 1.83
batch start
#iterations: 283
currently lose_sum: 94.98111361265182
time_elpased: 1.821
batch start
#iterations: 284
currently lose_sum: 94.70685249567032
time_elpased: 1.837
batch start
#iterations: 285
currently lose_sum: 95.04399347305298
time_elpased: 1.818
batch start
#iterations: 286
currently lose_sum: 94.90528291463852
time_elpased: 1.823
batch start
#iterations: 287
currently lose_sum: 94.59704005718231
time_elpased: 1.799
batch start
#iterations: 288
currently lose_sum: 94.92481100559235
time_elpased: 1.82
batch start
#iterations: 289
currently lose_sum: 94.7625213265419
time_elpased: 1.833
batch start
#iterations: 290
currently lose_sum: 94.82969254255295
time_elpased: 1.824
batch start
#iterations: 291
currently lose_sum: 94.79024422168732
time_elpased: 1.866
batch start
#iterations: 292
currently lose_sum: 94.63941663503647
time_elpased: 1.84
batch start
#iterations: 293
currently lose_sum: 94.68283176422119
time_elpased: 1.811
batch start
#iterations: 294
currently lose_sum: 94.64227449893951
time_elpased: 1.848
batch start
#iterations: 295
currently lose_sum: 94.35277569293976
time_elpased: 1.868
batch start
#iterations: 296
currently lose_sum: 94.71683692932129
time_elpased: 1.844
batch start
#iterations: 297
currently lose_sum: 94.69693797826767
time_elpased: 1.843
batch start
#iterations: 298
currently lose_sum: 94.67600905895233
time_elpased: 1.811
batch start
#iterations: 299
currently lose_sum: 94.3400656580925
time_elpased: 1.828
start validation test
0.658298969072
0.640773158279
0.723268498508
0.679526226734
0.658184905042
61.468
batch start
#iterations: 300
currently lose_sum: 94.68886250257492
time_elpased: 1.84
batch start
#iterations: 301
currently lose_sum: 95.09760534763336
time_elpased: 1.839
batch start
#iterations: 302
currently lose_sum: 94.78823184967041
time_elpased: 1.81
batch start
#iterations: 303
currently lose_sum: 94.56658154726028
time_elpased: 1.821
batch start
#iterations: 304
currently lose_sum: 94.54713958501816
time_elpased: 1.856
batch start
#iterations: 305
currently lose_sum: 94.93994164466858
time_elpased: 1.848
batch start
#iterations: 306
currently lose_sum: 94.8652572631836
time_elpased: 1.819
batch start
#iterations: 307
currently lose_sum: 94.60613119602203
time_elpased: 1.868
batch start
#iterations: 308
currently lose_sum: 94.68633991479874
time_elpased: 1.831
batch start
#iterations: 309
currently lose_sum: 94.45614796876907
time_elpased: 1.815
batch start
#iterations: 310
currently lose_sum: 94.91013938188553
time_elpased: 1.845
batch start
#iterations: 311
currently lose_sum: 94.71370816230774
time_elpased: 1.822
batch start
#iterations: 312
currently lose_sum: 94.5387801527977
time_elpased: 1.816
batch start
#iterations: 313
currently lose_sum: 94.54534792900085
time_elpased: 1.797
batch start
#iterations: 314
currently lose_sum: 94.64470428228378
time_elpased: 1.861
batch start
#iterations: 315
currently lose_sum: 94.8924732208252
time_elpased: 1.796
batch start
#iterations: 316
currently lose_sum: 94.94318640232086
time_elpased: 1.822
batch start
#iterations: 317
currently lose_sum: 94.96780866384506
time_elpased: 1.804
batch start
#iterations: 318
currently lose_sum: 94.75900757312775
time_elpased: 1.829
batch start
#iterations: 319
currently lose_sum: 94.71286278963089
time_elpased: 1.809
start validation test
0.651134020619
0.64562962963
0.672738499537
0.658905352283
0.651096090624
61.641
batch start
#iterations: 320
currently lose_sum: 94.58299052715302
time_elpased: 1.883
batch start
#iterations: 321
currently lose_sum: 94.83244270086288
time_elpased: 1.805
batch start
#iterations: 322
currently lose_sum: 94.93056666851044
time_elpased: 1.807
batch start
#iterations: 323
currently lose_sum: 94.85810619592667
time_elpased: 1.817
batch start
#iterations: 324
currently lose_sum: 95.15119087696075
time_elpased: 1.831
batch start
#iterations: 325
currently lose_sum: 94.81019765138626
time_elpased: 1.838
batch start
#iterations: 326
currently lose_sum: 94.58690792322159
time_elpased: 1.824
batch start
#iterations: 327
currently lose_sum: 94.57579255104065
time_elpased: 1.824
batch start
#iterations: 328
currently lose_sum: 94.24356532096863
time_elpased: 1.837
batch start
#iterations: 329
currently lose_sum: 94.98484420776367
time_elpased: 1.799
batch start
#iterations: 330
currently lose_sum: 94.51180505752563
time_elpased: 1.833
batch start
#iterations: 331
currently lose_sum: 94.42673635482788
time_elpased: 1.816
batch start
#iterations: 332
currently lose_sum: 94.56448811292648
time_elpased: 1.848
batch start
#iterations: 333
currently lose_sum: 94.34629154205322
time_elpased: 1.822
batch start
#iterations: 334
currently lose_sum: 95.10915398597717
time_elpased: 1.825
batch start
#iterations: 335
currently lose_sum: 94.64578378200531
time_elpased: 1.828
batch start
#iterations: 336
currently lose_sum: 94.71915245056152
time_elpased: 1.806
batch start
#iterations: 337
currently lose_sum: 94.93257611989975
time_elpased: 1.807
batch start
#iterations: 338
currently lose_sum: 94.74893206357956
time_elpased: 1.878
batch start
#iterations: 339
currently lose_sum: 94.4736538529396
time_elpased: 1.812
start validation test
0.658092783505
0.643148904567
0.712977256355
0.676265313095
0.657996425348
61.420
batch start
#iterations: 340
currently lose_sum: 94.62324410676956
time_elpased: 1.849
batch start
#iterations: 341
currently lose_sum: 94.67318910360336
time_elpased: 1.814
batch start
#iterations: 342
currently lose_sum: 94.74026519060135
time_elpased: 1.816
batch start
#iterations: 343
currently lose_sum: 94.63519817590714
time_elpased: 1.818
batch start
#iterations: 344
currently lose_sum: 94.63288855552673
time_elpased: 1.823
batch start
#iterations: 345
currently lose_sum: 94.75343626737595
time_elpased: 1.859
batch start
#iterations: 346
currently lose_sum: 94.88883578777313
time_elpased: 1.832
batch start
#iterations: 347
currently lose_sum: 94.91879260540009
time_elpased: 1.826
batch start
#iterations: 348
currently lose_sum: 94.81036794185638
time_elpased: 1.841
batch start
#iterations: 349
currently lose_sum: 94.53182679414749
time_elpased: 1.813
batch start
#iterations: 350
currently lose_sum: 94.73305410146713
time_elpased: 1.834
batch start
#iterations: 351
currently lose_sum: 94.65841895341873
time_elpased: 1.822
batch start
#iterations: 352
currently lose_sum: 94.63945281505585
time_elpased: 1.859
batch start
#iterations: 353
currently lose_sum: 94.88915467262268
time_elpased: 1.822
batch start
#iterations: 354
currently lose_sum: 94.7250303030014
time_elpased: 1.815
batch start
#iterations: 355
currently lose_sum: 94.88139724731445
time_elpased: 1.809
batch start
#iterations: 356
currently lose_sum: 94.48814469575882
time_elpased: 1.873
batch start
#iterations: 357
currently lose_sum: 94.97380089759827
time_elpased: 1.808
batch start
#iterations: 358
currently lose_sum: 94.52544492483139
time_elpased: 1.801
batch start
#iterations: 359
currently lose_sum: 94.59875810146332
time_elpased: 1.832
start validation test
0.654793814433
0.640858208955
0.707008335906
0.672310025933
0.654702143787
61.644
batch start
#iterations: 360
currently lose_sum: 94.8463631272316
time_elpased: 1.877
batch start
#iterations: 361
currently lose_sum: 94.84451258182526
time_elpased: 1.823
batch start
#iterations: 362
currently lose_sum: 94.58936548233032
time_elpased: 1.842
batch start
#iterations: 363
currently lose_sum: 94.63012659549713
time_elpased: 1.845
batch start
#iterations: 364
currently lose_sum: 95.41762417554855
time_elpased: 1.825
batch start
#iterations: 365
currently lose_sum: 94.52162998914719
time_elpased: 1.798
batch start
#iterations: 366
currently lose_sum: 94.87190133333206
time_elpased: 1.815
batch start
#iterations: 367
currently lose_sum: 94.98561328649521
time_elpased: 1.794
batch start
#iterations: 368
currently lose_sum: 94.96068531274796
time_elpased: 1.81
batch start
#iterations: 369
currently lose_sum: 94.58059471845627
time_elpased: 1.811
batch start
#iterations: 370
currently lose_sum: 94.37319606542587
time_elpased: 1.804
batch start
#iterations: 371
currently lose_sum: 94.7400375008583
time_elpased: 1.809
batch start
#iterations: 372
currently lose_sum: 94.95832526683807
time_elpased: 1.952
batch start
#iterations: 373
currently lose_sum: 94.34989601373672
time_elpased: 1.818
batch start
#iterations: 374
currently lose_sum: 94.43852895498276
time_elpased: 1.795
batch start
#iterations: 375
currently lose_sum: 94.95037627220154
time_elpased: 1.82
batch start
#iterations: 376
currently lose_sum: 94.4506283402443
time_elpased: 1.853
batch start
#iterations: 377
currently lose_sum: 94.6152595281601
time_elpased: 1.851
batch start
#iterations: 378
currently lose_sum: 94.7171158194542
time_elpased: 1.824
batch start
#iterations: 379
currently lose_sum: 94.7533084154129
time_elpased: 1.787
start validation test
0.655721649485
0.638468550593
0.720798600391
0.677140232997
0.65560739686
61.682
batch start
#iterations: 380
currently lose_sum: 94.35096573829651
time_elpased: 1.845
batch start
#iterations: 381
currently lose_sum: 94.74002677202225
time_elpased: 1.832
batch start
#iterations: 382
currently lose_sum: 94.49503648281097
time_elpased: 1.884
batch start
#iterations: 383
currently lose_sum: 94.3242661356926
time_elpased: 1.822
batch start
#iterations: 384
currently lose_sum: 94.96578592061996
time_elpased: 1.809
batch start
#iterations: 385
currently lose_sum: 94.77871197462082
time_elpased: 1.826
batch start
#iterations: 386
currently lose_sum: 94.39965236186981
time_elpased: 1.826
batch start
#iterations: 387
currently lose_sum: 94.36392384767532
time_elpased: 1.818
batch start
#iterations: 388
currently lose_sum: 94.94003683328629
time_elpased: 1.838
batch start
#iterations: 389
currently lose_sum: 95.15963959693909
time_elpased: 1.832
batch start
#iterations: 390
currently lose_sum: 94.87424063682556
time_elpased: 1.832
batch start
#iterations: 391
currently lose_sum: 94.93184906244278
time_elpased: 1.802
batch start
#iterations: 392
currently lose_sum: 94.5386158823967
time_elpased: 1.811
batch start
#iterations: 393
currently lose_sum: 94.71237975358963
time_elpased: 1.877
batch start
#iterations: 394
currently lose_sum: 94.17376661300659
time_elpased: 1.845
batch start
#iterations: 395
currently lose_sum: 94.34878814220428
time_elpased: 1.88
batch start
#iterations: 396
currently lose_sum: 94.3924126625061
time_elpased: 1.813
batch start
#iterations: 397
currently lose_sum: 94.58198529481888
time_elpased: 1.863
batch start
#iterations: 398
currently lose_sum: 94.46789664030075
time_elpased: 1.805
batch start
#iterations: 399
currently lose_sum: 94.62702596187592
time_elpased: 1.826
start validation test
0.651443298969
0.642822619623
0.68436760317
0.6629448709
0.651385495275
61.643
acc: 0.658
pre: 0.643
rec: 0.716
F1: 0.677
auc: 0.658
