start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.41396939754486
time_elpased: 2.071
batch start
#iterations: 1
currently lose_sum: 99.96781361103058
time_elpased: 1.88
batch start
#iterations: 2
currently lose_sum: 99.78018945455551
time_elpased: 1.876
batch start
#iterations: 3
currently lose_sum: 99.3619601726532
time_elpased: 1.919
batch start
#iterations: 4
currently lose_sum: 99.41351878643036
time_elpased: 1.905
batch start
#iterations: 5
currently lose_sum: 99.04102432727814
time_elpased: 1.868
batch start
#iterations: 6
currently lose_sum: 99.03396755456924
time_elpased: 1.931
batch start
#iterations: 7
currently lose_sum: 98.69787639379501
time_elpased: 1.87
batch start
#iterations: 8
currently lose_sum: 98.13024932146072
time_elpased: 1.903
batch start
#iterations: 9
currently lose_sum: 98.27834618091583
time_elpased: 1.879
batch start
#iterations: 10
currently lose_sum: 98.4580363035202
time_elpased: 1.87
batch start
#iterations: 11
currently lose_sum: 98.34533447027206
time_elpased: 1.965
batch start
#iterations: 12
currently lose_sum: 98.00045955181122
time_elpased: 1.921
batch start
#iterations: 13
currently lose_sum: 97.73432117700577
time_elpased: 1.886
batch start
#iterations: 14
currently lose_sum: 97.98453855514526
time_elpased: 1.918
batch start
#iterations: 15
currently lose_sum: 97.47913616895676
time_elpased: 1.917
batch start
#iterations: 16
currently lose_sum: 97.70493394136429
time_elpased: 1.893
batch start
#iterations: 17
currently lose_sum: 97.682581782341
time_elpased: 1.883
batch start
#iterations: 18
currently lose_sum: 97.47491824626923
time_elpased: 1.923
batch start
#iterations: 19
currently lose_sum: 97.49801081418991
time_elpased: 1.907
start validation test
0.634072164948
0.626497873985
0.667181228774
0.646199850486
0.63401403688
63.297
batch start
#iterations: 20
currently lose_sum: 97.43380951881409
time_elpased: 1.91
batch start
#iterations: 21
currently lose_sum: 96.907929956913
time_elpased: 1.912
batch start
#iterations: 22
currently lose_sum: 97.09790527820587
time_elpased: 1.906
batch start
#iterations: 23
currently lose_sum: 97.35824173688889
time_elpased: 1.942
batch start
#iterations: 24
currently lose_sum: 96.8929853439331
time_elpased: 1.904
batch start
#iterations: 25
currently lose_sum: 96.75481456518173
time_elpased: 1.882
batch start
#iterations: 26
currently lose_sum: 97.02266454696655
time_elpased: 1.909
batch start
#iterations: 27
currently lose_sum: 96.82861870527267
time_elpased: 1.943
batch start
#iterations: 28
currently lose_sum: 96.75198060274124
time_elpased: 1.887
batch start
#iterations: 29
currently lose_sum: 96.59876990318298
time_elpased: 1.9
batch start
#iterations: 30
currently lose_sum: 96.81129342317581
time_elpased: 1.906
batch start
#iterations: 31
currently lose_sum: 96.67306560277939
time_elpased: 1.955
batch start
#iterations: 32
currently lose_sum: 96.50647968053818
time_elpased: 1.893
batch start
#iterations: 33
currently lose_sum: 96.54043674468994
time_elpased: 1.906
batch start
#iterations: 34
currently lose_sum: 96.83919334411621
time_elpased: 1.903
batch start
#iterations: 35
currently lose_sum: 96.38553076982498
time_elpased: 1.963
batch start
#iterations: 36
currently lose_sum: 96.41504311561584
time_elpased: 1.955
batch start
#iterations: 37
currently lose_sum: 96.50879287719727
time_elpased: 1.891
batch start
#iterations: 38
currently lose_sum: 96.42362362146378
time_elpased: 1.916
batch start
#iterations: 39
currently lose_sum: 96.33155107498169
time_elpased: 1.915
start validation test
0.637319587629
0.636409891116
0.643614284244
0.639991813344
0.637308536318
62.385
batch start
#iterations: 40
currently lose_sum: 96.75347286462784
time_elpased: 1.886
batch start
#iterations: 41
currently lose_sum: 96.64098304510117
time_elpased: 1.918
batch start
#iterations: 42
currently lose_sum: 96.50451683998108
time_elpased: 1.929
batch start
#iterations: 43
currently lose_sum: 95.98440080881119
time_elpased: 1.941
batch start
#iterations: 44
currently lose_sum: 96.3528636097908
time_elpased: 1.933
batch start
#iterations: 45
currently lose_sum: 96.02224063873291
time_elpased: 1.902
batch start
#iterations: 46
currently lose_sum: 96.05240565538406
time_elpased: 1.917
batch start
#iterations: 47
currently lose_sum: 96.23000997304916
time_elpased: 1.908
batch start
#iterations: 48
currently lose_sum: 96.13340151309967
time_elpased: 1.921
batch start
#iterations: 49
currently lose_sum: 96.32348316907883
time_elpased: 1.887
batch start
#iterations: 50
currently lose_sum: 96.09787386655807
time_elpased: 1.915
batch start
#iterations: 51
currently lose_sum: 96.1239600777626
time_elpased: 1.866
batch start
#iterations: 52
currently lose_sum: 95.94734972715378
time_elpased: 1.903
batch start
#iterations: 53
currently lose_sum: 96.23699361085892
time_elpased: 1.892
batch start
#iterations: 54
currently lose_sum: 96.32189530134201
time_elpased: 1.89
batch start
#iterations: 55
currently lose_sum: 96.06124603748322
time_elpased: 1.879
batch start
#iterations: 56
currently lose_sum: 95.98730826377869
time_elpased: 1.886
batch start
#iterations: 57
currently lose_sum: 95.8790134191513
time_elpased: 1.932
batch start
#iterations: 58
currently lose_sum: 96.3480094075203
time_elpased: 1.884
batch start
#iterations: 59
currently lose_sum: 96.21001946926117
time_elpased: 1.924
start validation test
0.643195876289
0.636621370613
0.670165688999
0.6529630001
0.643148526623
62.046
batch start
#iterations: 60
currently lose_sum: 96.24638932943344
time_elpased: 1.94
batch start
#iterations: 61
currently lose_sum: 96.35317146778107
time_elpased: 1.954
batch start
#iterations: 62
currently lose_sum: 95.7651014328003
time_elpased: 1.919
batch start
#iterations: 63
currently lose_sum: 95.66234540939331
time_elpased: 1.985
batch start
#iterations: 64
currently lose_sum: 96.16674035787582
time_elpased: 1.94
batch start
#iterations: 65
currently lose_sum: 95.58146125078201
time_elpased: 1.951
batch start
#iterations: 66
currently lose_sum: 95.9433861374855
time_elpased: 1.933
batch start
#iterations: 67
currently lose_sum: 96.05193668603897
time_elpased: 1.936
batch start
#iterations: 68
currently lose_sum: 95.86859333515167
time_elpased: 1.879
batch start
#iterations: 69
currently lose_sum: 95.67865943908691
time_elpased: 1.902
batch start
#iterations: 70
currently lose_sum: 95.27376168966293
time_elpased: 1.922
batch start
#iterations: 71
currently lose_sum: 95.66707479953766
time_elpased: 1.919
batch start
#iterations: 72
currently lose_sum: 95.22102999687195
time_elpased: 1.888
batch start
#iterations: 73
currently lose_sum: 95.57158172130585
time_elpased: 1.898
batch start
#iterations: 74
currently lose_sum: 95.5474904179573
time_elpased: 1.893
batch start
#iterations: 75
currently lose_sum: 95.37731748819351
time_elpased: 1.925
batch start
#iterations: 76
currently lose_sum: 95.66452991962433
time_elpased: 1.903
batch start
#iterations: 77
currently lose_sum: 95.6753825545311
time_elpased: 1.894
batch start
#iterations: 78
currently lose_sum: 95.89170801639557
time_elpased: 1.899
batch start
#iterations: 79
currently lose_sum: 95.60079008340836
time_elpased: 1.883
start validation test
0.654432989691
0.636743215031
0.721930637028
0.676666345134
0.65431448716
61.656
batch start
#iterations: 80
currently lose_sum: 95.63535922765732
time_elpased: 1.981
batch start
#iterations: 81
currently lose_sum: 95.99987530708313
time_elpased: 1.9
batch start
#iterations: 82
currently lose_sum: 95.8026996254921
time_elpased: 1.896
batch start
#iterations: 83
currently lose_sum: 95.97666347026825
time_elpased: 1.884
batch start
#iterations: 84
currently lose_sum: 95.41072815656662
time_elpased: 1.918
batch start
#iterations: 85
currently lose_sum: 95.72267776727676
time_elpased: 1.935
batch start
#iterations: 86
currently lose_sum: 95.74258428812027
time_elpased: 1.892
batch start
#iterations: 87
currently lose_sum: 95.21922993659973
time_elpased: 1.915
batch start
#iterations: 88
currently lose_sum: 95.68517827987671
time_elpased: 1.866
batch start
#iterations: 89
currently lose_sum: 95.64343738555908
time_elpased: 1.912
batch start
#iterations: 90
currently lose_sum: 95.72569108009338
time_elpased: 1.954
batch start
#iterations: 91
currently lose_sum: 95.7273742556572
time_elpased: 1.879
batch start
#iterations: 92
currently lose_sum: 95.69613033533096
time_elpased: 1.96
batch start
#iterations: 93
currently lose_sum: 95.47959119081497
time_elpased: 1.966
batch start
#iterations: 94
currently lose_sum: 95.50310361385345
time_elpased: 1.906
batch start
#iterations: 95
currently lose_sum: 95.51435244083405
time_elpased: 1.921
batch start
#iterations: 96
currently lose_sum: 95.60822248458862
time_elpased: 1.925
batch start
#iterations: 97
currently lose_sum: 95.48698657751083
time_elpased: 1.889
batch start
#iterations: 98
currently lose_sum: 95.56294733285904
time_elpased: 1.919
batch start
#iterations: 99
currently lose_sum: 95.73051190376282
time_elpased: 1.898
start validation test
0.647268041237
0.634701912261
0.696820006175
0.664311994113
0.64718104512
61.757
batch start
#iterations: 100
currently lose_sum: 95.38036155700684
time_elpased: 1.93
batch start
#iterations: 101
currently lose_sum: 95.02793079614639
time_elpased: 1.898
batch start
#iterations: 102
currently lose_sum: 95.55204731225967
time_elpased: 1.898
batch start
#iterations: 103
currently lose_sum: 95.54468470811844
time_elpased: 1.903
batch start
#iterations: 104
currently lose_sum: 95.25778704881668
time_elpased: 1.906
batch start
#iterations: 105
currently lose_sum: 95.35213512182236
time_elpased: 1.88
batch start
#iterations: 106
currently lose_sum: 95.76253134012222
time_elpased: 1.92
batch start
#iterations: 107
currently lose_sum: 95.45275670289993
time_elpased: 1.951
batch start
#iterations: 108
currently lose_sum: 95.34095466136932
time_elpased: 1.897
batch start
#iterations: 109
currently lose_sum: 95.34475833177567
time_elpased: 1.875
batch start
#iterations: 110
currently lose_sum: 95.02614802122116
time_elpased: 1.935
batch start
#iterations: 111
currently lose_sum: 95.13066482543945
time_elpased: 1.93
batch start
#iterations: 112
currently lose_sum: 95.70830476284027
time_elpased: 1.867
batch start
#iterations: 113
currently lose_sum: 95.10667544603348
time_elpased: 1.911
batch start
#iterations: 114
currently lose_sum: 95.45402586460114
time_elpased: 1.879
batch start
#iterations: 115
currently lose_sum: 95.56810122728348
time_elpased: 1.901
batch start
#iterations: 116
currently lose_sum: 95.36454421281815
time_elpased: 1.896
batch start
#iterations: 117
currently lose_sum: 95.45737481117249
time_elpased: 1.889
batch start
#iterations: 118
currently lose_sum: 95.32944881916046
time_elpased: 1.889
batch start
#iterations: 119
currently lose_sum: 95.42379456758499
time_elpased: 1.907
start validation test
0.650773195876
0.644187414233
0.67634043429
0.659872483558
0.650728308646
61.676
batch start
#iterations: 120
currently lose_sum: 95.56952458620071
time_elpased: 2.002
batch start
#iterations: 121
currently lose_sum: 95.47084826231003
time_elpased: 1.871
batch start
#iterations: 122
currently lose_sum: 95.74201136827469
time_elpased: 1.877
batch start
#iterations: 123
currently lose_sum: 95.06052577495575
time_elpased: 1.908
batch start
#iterations: 124
currently lose_sum: 95.91099745035172
time_elpased: 1.928
batch start
#iterations: 125
currently lose_sum: 95.33606272935867
time_elpased: 1.959
batch start
#iterations: 126
currently lose_sum: 95.39458411931992
time_elpased: 1.902
batch start
#iterations: 127
currently lose_sum: 95.32818067073822
time_elpased: 1.921
batch start
#iterations: 128
currently lose_sum: 95.47695744037628
time_elpased: 1.901
batch start
#iterations: 129
currently lose_sum: 95.12611526250839
time_elpased: 1.959
batch start
#iterations: 130
currently lose_sum: 95.06835412979126
time_elpased: 1.933
batch start
#iterations: 131
currently lose_sum: 95.39242947101593
time_elpased: 1.907
batch start
#iterations: 132
currently lose_sum: 95.26124632358551
time_elpased: 1.899
batch start
#iterations: 133
currently lose_sum: 95.71486121416092
time_elpased: 1.863
batch start
#iterations: 134
currently lose_sum: 95.17052471637726
time_elpased: 1.911
batch start
#iterations: 135
currently lose_sum: 95.25433123111725
time_elpased: 1.919
batch start
#iterations: 136
currently lose_sum: 95.4727703332901
time_elpased: 1.886
batch start
#iterations: 137
currently lose_sum: 95.05419564247131
time_elpased: 1.898
batch start
#iterations: 138
currently lose_sum: 95.20550847053528
time_elpased: 1.898
batch start
#iterations: 139
currently lose_sum: 95.11941283941269
time_elpased: 1.882
start validation test
0.654175257732
0.642911440517
0.696305444067
0.668544044267
0.654101291692
61.529
batch start
#iterations: 140
currently lose_sum: 95.67753714323044
time_elpased: 1.952
batch start
#iterations: 141
currently lose_sum: 95.11786442995071
time_elpased: 1.868
batch start
#iterations: 142
currently lose_sum: 95.33559215068817
time_elpased: 1.909
batch start
#iterations: 143
currently lose_sum: 95.3925232887268
time_elpased: 1.939
batch start
#iterations: 144
currently lose_sum: 95.0442646741867
time_elpased: 1.876
batch start
#iterations: 145
currently lose_sum: 95.46758586168289
time_elpased: 1.896
batch start
#iterations: 146
currently lose_sum: 95.4403138756752
time_elpased: 1.891
batch start
#iterations: 147
currently lose_sum: 95.45927304029465
time_elpased: 1.891
batch start
#iterations: 148
currently lose_sum: 95.03112691640854
time_elpased: 1.884
batch start
#iterations: 149
currently lose_sum: 95.03814661502838
time_elpased: 1.881
batch start
#iterations: 150
currently lose_sum: 95.41779744625092
time_elpased: 1.867
batch start
#iterations: 151
currently lose_sum: 95.32336354255676
time_elpased: 1.878
batch start
#iterations: 152
currently lose_sum: 95.29748111963272
time_elpased: 1.896
batch start
#iterations: 153
currently lose_sum: 95.40644550323486
time_elpased: 1.935
batch start
#iterations: 154
currently lose_sum: 95.21602910757065
time_elpased: 1.898
batch start
#iterations: 155
currently lose_sum: 95.47750353813171
time_elpased: 1.863
batch start
#iterations: 156
currently lose_sum: 95.08972400426865
time_elpased: 1.911
batch start
#iterations: 157
currently lose_sum: 95.23110818862915
time_elpased: 1.891
batch start
#iterations: 158
currently lose_sum: 95.15980583429337
time_elpased: 1.873
batch start
#iterations: 159
currently lose_sum: 95.02746725082397
time_elpased: 1.905
start validation test
0.65206185567
0.641758241758
0.691159822991
0.665543553662
0.651993213158
61.724
batch start
#iterations: 160
currently lose_sum: 95.38802444934845
time_elpased: 1.956
batch start
#iterations: 161
currently lose_sum: 95.32858365774155
time_elpased: 1.959
batch start
#iterations: 162
currently lose_sum: 95.34999585151672
time_elpased: 1.943
batch start
#iterations: 163
currently lose_sum: 95.02184027433395
time_elpased: 1.966
batch start
#iterations: 164
currently lose_sum: 95.36413013935089
time_elpased: 1.916
batch start
#iterations: 165
currently lose_sum: 95.0159809589386
time_elpased: 1.89
batch start
#iterations: 166
currently lose_sum: 95.29154741764069
time_elpased: 1.884
batch start
#iterations: 167
currently lose_sum: 95.11913406848907
time_elpased: 1.903
batch start
#iterations: 168
currently lose_sum: 94.94012433290482
time_elpased: 1.9
batch start
#iterations: 169
currently lose_sum: 94.83040767908096
time_elpased: 1.909
batch start
#iterations: 170
currently lose_sum: 95.13430732488632
time_elpased: 1.93
batch start
#iterations: 171
currently lose_sum: 94.85986089706421
time_elpased: 1.884
batch start
#iterations: 172
currently lose_sum: 94.74319392442703
time_elpased: 1.919
batch start
#iterations: 173
currently lose_sum: 94.92672425508499
time_elpased: 2.023
batch start
#iterations: 174
currently lose_sum: 95.53388887643814
time_elpased: 1.875
batch start
#iterations: 175
currently lose_sum: 95.35304635763168
time_elpased: 1.938
batch start
#iterations: 176
currently lose_sum: 95.18824940919876
time_elpased: 1.892
batch start
#iterations: 177
currently lose_sum: 94.96328043937683
time_elpased: 1.912
batch start
#iterations: 178
currently lose_sum: 95.23837387561798
time_elpased: 1.934
batch start
#iterations: 179
currently lose_sum: 94.95604223012924
time_elpased: 1.894
start validation test
0.65587628866
0.640487849949
0.713388906041
0.674975657254
0.655775316389
61.581
batch start
#iterations: 180
currently lose_sum: 95.04456317424774
time_elpased: 1.983
batch start
#iterations: 181
currently lose_sum: 94.63279831409454
time_elpased: 1.894
batch start
#iterations: 182
currently lose_sum: 95.48577892780304
time_elpased: 1.899
batch start
#iterations: 183
currently lose_sum: 94.53597432374954
time_elpased: 1.902
batch start
#iterations: 184
currently lose_sum: 95.10911756753922
time_elpased: 1.899
batch start
#iterations: 185
currently lose_sum: 94.84805184602737
time_elpased: 1.958
batch start
#iterations: 186
currently lose_sum: 94.74231427907944
time_elpased: 1.904
batch start
#iterations: 187
currently lose_sum: 94.84524440765381
time_elpased: 1.888
batch start
#iterations: 188
currently lose_sum: 94.80078238248825
time_elpased: 1.903
batch start
#iterations: 189
currently lose_sum: 95.1200025677681
time_elpased: 1.944
batch start
#iterations: 190
currently lose_sum: 94.89383751153946
time_elpased: 1.885
batch start
#iterations: 191
currently lose_sum: 94.89568078517914
time_elpased: 1.885
batch start
#iterations: 192
currently lose_sum: 94.41629099845886
time_elpased: 1.89
batch start
#iterations: 193
currently lose_sum: 94.88283610343933
time_elpased: 1.89
batch start
#iterations: 194
currently lose_sum: 94.94770681858063
time_elpased: 1.938
batch start
#iterations: 195
currently lose_sum: 95.17984348535538
time_elpased: 1.884
batch start
#iterations: 196
currently lose_sum: 95.03457635641098
time_elpased: 1.9
batch start
#iterations: 197
currently lose_sum: 95.69078969955444
time_elpased: 1.876
batch start
#iterations: 198
currently lose_sum: 94.94657474756241
time_elpased: 1.897
batch start
#iterations: 199
currently lose_sum: 95.33117014169693
time_elpased: 1.911
start validation test
0.650824742268
0.63684553148
0.704744262632
0.669076697606
0.650730078234
61.873
batch start
#iterations: 200
currently lose_sum: 95.02316963672638
time_elpased: 1.911
batch start
#iterations: 201
currently lose_sum: 95.02888751029968
time_elpased: 1.941
batch start
#iterations: 202
currently lose_sum: 94.81717509031296
time_elpased: 1.879
batch start
#iterations: 203
currently lose_sum: 95.09979337453842
time_elpased: 1.903
batch start
#iterations: 204
currently lose_sum: 94.70134353637695
time_elpased: 1.932
batch start
#iterations: 205
currently lose_sum: 94.9855141043663
time_elpased: 1.908
batch start
#iterations: 206
currently lose_sum: 94.87821561098099
time_elpased: 1.911
batch start
#iterations: 207
currently lose_sum: 94.80028104782104
time_elpased: 1.873
batch start
#iterations: 208
currently lose_sum: 94.83886003494263
time_elpased: 1.936
batch start
#iterations: 209
currently lose_sum: 95.08653795719147
time_elpased: 1.937
batch start
#iterations: 210
currently lose_sum: 94.56558459997177
time_elpased: 1.986
batch start
#iterations: 211
currently lose_sum: 94.53347712755203
time_elpased: 1.91
batch start
#iterations: 212
currently lose_sum: 95.10255873203278
time_elpased: 1.918
batch start
#iterations: 213
currently lose_sum: 95.2596645951271
time_elpased: 1.93
batch start
#iterations: 214
currently lose_sum: 95.01462852954865
time_elpased: 1.955
batch start
#iterations: 215
currently lose_sum: 95.04246532917023
time_elpased: 1.888
batch start
#iterations: 216
currently lose_sum: 95.23793053627014
time_elpased: 1.946
batch start
#iterations: 217
currently lose_sum: 94.9148337841034
time_elpased: 1.947
batch start
#iterations: 218
currently lose_sum: 95.16104280948639
time_elpased: 1.918
batch start
#iterations: 219
currently lose_sum: 95.08495795726776
time_elpased: 1.905
start validation test
0.650721649485
0.63995431617
0.691983122363
0.664952531646
0.650649208605
61.768
batch start
#iterations: 220
currently lose_sum: 95.45843154191971
time_elpased: 1.96
batch start
#iterations: 221
currently lose_sum: 94.94087368249893
time_elpased: 1.909
batch start
#iterations: 222
currently lose_sum: 94.94399511814117
time_elpased: 1.899
batch start
#iterations: 223
currently lose_sum: 94.95565617084503
time_elpased: 1.882
batch start
#iterations: 224
currently lose_sum: 94.99334871768951
time_elpased: 1.944
batch start
#iterations: 225
currently lose_sum: 94.7865030169487
time_elpased: 1.912
batch start
#iterations: 226
currently lose_sum: 94.91748410463333
time_elpased: 1.951
batch start
#iterations: 227
currently lose_sum: 95.22046703100204
time_elpased: 2.006
batch start
#iterations: 228
currently lose_sum: 94.73474985361099
time_elpased: 1.904
batch start
#iterations: 229
currently lose_sum: 94.57474678754807
time_elpased: 1.883
batch start
#iterations: 230
currently lose_sum: 94.37811195850372
time_elpased: 1.895
batch start
#iterations: 231
currently lose_sum: 95.08692669868469
time_elpased: 1.95
batch start
#iterations: 232
currently lose_sum: 94.99823045730591
time_elpased: 1.88
batch start
#iterations: 233
currently lose_sum: 95.06847429275513
time_elpased: 1.978
batch start
#iterations: 234
currently lose_sum: 95.19909304380417
time_elpased: 1.935
batch start
#iterations: 235
currently lose_sum: 95.0049632191658
time_elpased: 1.961
batch start
#iterations: 236
currently lose_sum: 95.1094781756401
time_elpased: 1.96
batch start
#iterations: 237
currently lose_sum: 94.813731610775
time_elpased: 1.925
batch start
#iterations: 238
currently lose_sum: 95.05406898260117
time_elpased: 1.908
batch start
#iterations: 239
currently lose_sum: 94.78457683324814
time_elpased: 1.929
start validation test
0.653350515464
0.643707973103
0.689616136668
0.665871714612
0.653286845572
61.622
batch start
#iterations: 240
currently lose_sum: 94.93391227722168
time_elpased: 1.953
batch start
#iterations: 241
currently lose_sum: 94.96870642900467
time_elpased: 1.907
batch start
#iterations: 242
currently lose_sum: 94.67021131515503
time_elpased: 1.964
batch start
#iterations: 243
currently lose_sum: 94.8794493675232
time_elpased: 1.919
batch start
#iterations: 244
currently lose_sum: 95.26692909002304
time_elpased: 1.927
batch start
#iterations: 245
currently lose_sum: 94.99192321300507
time_elpased: 1.89
batch start
#iterations: 246
currently lose_sum: 95.13990616798401
time_elpased: 1.903
batch start
#iterations: 247
currently lose_sum: 95.03614574670792
time_elpased: 1.869
batch start
#iterations: 248
currently lose_sum: 95.41259598731995
time_elpased: 1.908
batch start
#iterations: 249
currently lose_sum: 94.65122133493423
time_elpased: 1.931
batch start
#iterations: 250
currently lose_sum: 95.17654639482498
time_elpased: 1.901
batch start
#iterations: 251
currently lose_sum: 94.79367184638977
time_elpased: 1.9
batch start
#iterations: 252
currently lose_sum: 94.67663770914078
time_elpased: 1.913
batch start
#iterations: 253
currently lose_sum: 95.10726165771484
time_elpased: 1.888
batch start
#iterations: 254
currently lose_sum: 94.76939934492111
time_elpased: 1.894
batch start
#iterations: 255
currently lose_sum: 94.51074421405792
time_elpased: 1.982
batch start
#iterations: 256
currently lose_sum: 94.53416174650192
time_elpased: 1.909
batch start
#iterations: 257
currently lose_sum: 94.87176007032394
time_elpased: 1.947
batch start
#iterations: 258
currently lose_sum: 95.06474202871323
time_elpased: 1.893
batch start
#iterations: 259
currently lose_sum: 94.97092288732529
time_elpased: 1.905
start validation test
0.652989690722
0.639083030472
0.705773386848
0.670774647887
0.652897020802
61.663
batch start
#iterations: 260
currently lose_sum: 94.50582665205002
time_elpased: 1.933
batch start
#iterations: 261
currently lose_sum: 94.86366355419159
time_elpased: 1.958
batch start
#iterations: 262
currently lose_sum: 94.70837169885635
time_elpased: 1.869
batch start
#iterations: 263
currently lose_sum: 94.9461385011673
time_elpased: 1.931
batch start
#iterations: 264
currently lose_sum: 94.83810079097748
time_elpased: 1.93
batch start
#iterations: 265
currently lose_sum: 94.56215196847916
time_elpased: 1.905
batch start
#iterations: 266
currently lose_sum: 94.80064386129379
time_elpased: 1.919
batch start
#iterations: 267
currently lose_sum: 94.95634078979492
time_elpased: 1.951
batch start
#iterations: 268
currently lose_sum: 95.02367424964905
time_elpased: 1.96
batch start
#iterations: 269
currently lose_sum: 94.86609363555908
time_elpased: 1.854
batch start
#iterations: 270
currently lose_sum: 95.13515293598175
time_elpased: 1.91
batch start
#iterations: 271
currently lose_sum: 94.69139951467514
time_elpased: 1.923
batch start
#iterations: 272
currently lose_sum: 94.787433385849
time_elpased: 1.972
batch start
#iterations: 273
currently lose_sum: 95.00224566459656
time_elpased: 1.893
batch start
#iterations: 274
currently lose_sum: 94.95249837636948
time_elpased: 1.943
batch start
#iterations: 275
currently lose_sum: 95.20651859045029
time_elpased: 1.966
batch start
#iterations: 276
currently lose_sum: 95.04601871967316
time_elpased: 1.898
batch start
#iterations: 277
currently lose_sum: 95.03216242790222
time_elpased: 1.975
batch start
#iterations: 278
currently lose_sum: 95.03695774078369
time_elpased: 1.925
batch start
#iterations: 279
currently lose_sum: 95.01411938667297
time_elpased: 1.888
start validation test
0.645567010309
0.645826917154
0.647422043841
0.646623496762
0.645563753512
61.959
batch start
#iterations: 280
currently lose_sum: 94.73257499933243
time_elpased: 1.98
batch start
#iterations: 281
currently lose_sum: 94.81526374816895
time_elpased: 2.017
batch start
#iterations: 282
currently lose_sum: 95.02959728240967
time_elpased: 1.933
batch start
#iterations: 283
currently lose_sum: 95.21595120429993
time_elpased: 1.934
batch start
#iterations: 284
currently lose_sum: 94.85023647546768
time_elpased: 1.935
batch start
#iterations: 285
currently lose_sum: 94.90391659736633
time_elpased: 1.923
batch start
#iterations: 286
currently lose_sum: 94.91797703504562
time_elpased: 1.897
batch start
#iterations: 287
currently lose_sum: 94.77941811084747
time_elpased: 1.914
batch start
#iterations: 288
currently lose_sum: 94.78660470247269
time_elpased: 1.956
batch start
#iterations: 289
currently lose_sum: 94.7249761223793
time_elpased: 1.902
batch start
#iterations: 290
currently lose_sum: 94.66185057163239
time_elpased: 1.934
batch start
#iterations: 291
currently lose_sum: 94.68975132703781
time_elpased: 1.934
batch start
#iterations: 292
currently lose_sum: 94.68958061933517
time_elpased: 1.995
batch start
#iterations: 293
currently lose_sum: 94.60273677110672
time_elpased: 1.924
batch start
#iterations: 294
currently lose_sum: 95.14094585180283
time_elpased: 1.891
batch start
#iterations: 295
currently lose_sum: 94.51995033025742
time_elpased: 1.897
batch start
#iterations: 296
currently lose_sum: 94.7385561466217
time_elpased: 1.948
batch start
#iterations: 297
currently lose_sum: 94.46165174245834
time_elpased: 1.91
batch start
#iterations: 298
currently lose_sum: 94.88574212789536
time_elpased: 1.909
batch start
#iterations: 299
currently lose_sum: 94.66282761096954
time_elpased: 1.925
start validation test
0.654639175258
0.641099990646
0.705361737162
0.671697373579
0.654550123977
61.591
batch start
#iterations: 300
currently lose_sum: 95.13598674535751
time_elpased: 1.904
batch start
#iterations: 301
currently lose_sum: 94.69519072771072
time_elpased: 1.871
batch start
#iterations: 302
currently lose_sum: 94.64947271347046
time_elpased: 1.964
batch start
#iterations: 303
currently lose_sum: 95.04662537574768
time_elpased: 1.979
batch start
#iterations: 304
currently lose_sum: 94.67987203598022
time_elpased: 1.97
batch start
#iterations: 305
currently lose_sum: 94.54276752471924
time_elpased: 1.929
batch start
#iterations: 306
currently lose_sum: 94.70898389816284
time_elpased: 1.922
batch start
#iterations: 307
currently lose_sum: 94.63807314634323
time_elpased: 1.921
batch start
#iterations: 308
currently lose_sum: 94.7007247209549
time_elpased: 1.921
batch start
#iterations: 309
currently lose_sum: 94.91226762533188
time_elpased: 1.915
batch start
#iterations: 310
currently lose_sum: 94.71197092533112
time_elpased: 1.987
batch start
#iterations: 311
currently lose_sum: 94.54981642961502
time_elpased: 1.906
batch start
#iterations: 312
currently lose_sum: 94.636887550354
time_elpased: 1.938
batch start
#iterations: 313
currently lose_sum: 94.87349498271942
time_elpased: 1.888
batch start
#iterations: 314
currently lose_sum: 94.853855073452
time_elpased: 1.898
batch start
#iterations: 315
currently lose_sum: 94.86768919229507
time_elpased: 1.896
batch start
#iterations: 316
currently lose_sum: 94.73261368274689
time_elpased: 1.964
batch start
#iterations: 317
currently lose_sum: 94.77217835187912
time_elpased: 1.878
batch start
#iterations: 318
currently lose_sum: 94.82253235578537
time_elpased: 1.935
batch start
#iterations: 319
currently lose_sum: 94.90983295440674
time_elpased: 1.962
start validation test
0.653144329897
0.641638225256
0.69651126891
0.667949666913
0.653068192547
61.756
batch start
#iterations: 320
currently lose_sum: 95.02876102924347
time_elpased: 1.989
batch start
#iterations: 321
currently lose_sum: 94.77098399400711
time_elpased: 1.934
batch start
#iterations: 322
currently lose_sum: 95.33468890190125
time_elpased: 1.992
batch start
#iterations: 323
currently lose_sum: 94.68323600292206
time_elpased: 1.97
batch start
#iterations: 324
currently lose_sum: 94.95196807384491
time_elpased: 1.954
batch start
#iterations: 325
currently lose_sum: 94.91722679138184
time_elpased: 1.916
batch start
#iterations: 326
currently lose_sum: 94.49728709459305
time_elpased: 1.939
batch start
#iterations: 327
currently lose_sum: 94.8104732632637
time_elpased: 1.913
batch start
#iterations: 328
currently lose_sum: 94.43759334087372
time_elpased: 1.932
batch start
#iterations: 329
currently lose_sum: 94.75464516878128
time_elpased: 1.939
batch start
#iterations: 330
currently lose_sum: 94.63528525829315
time_elpased: 1.934
batch start
#iterations: 331
currently lose_sum: 94.58328056335449
time_elpased: 1.972
batch start
#iterations: 332
currently lose_sum: 94.57076162099838
time_elpased: 1.952
batch start
#iterations: 333
currently lose_sum: 94.74572288990021
time_elpased: 1.918
batch start
#iterations: 334
currently lose_sum: 94.9193417429924
time_elpased: 1.946
batch start
#iterations: 335
currently lose_sum: 94.9431419968605
time_elpased: 1.903
batch start
#iterations: 336
currently lose_sum: 94.5652939081192
time_elpased: 1.898
batch start
#iterations: 337
currently lose_sum: 94.89386808872223
time_elpased: 1.92
batch start
#iterations: 338
currently lose_sum: 94.5372274518013
time_elpased: 1.919
batch start
#iterations: 339
currently lose_sum: 94.51781010627747
time_elpased: 1.925
start validation test
0.655360824742
0.644154855893
0.696922918596
0.669500741473
0.655287856076
61.667
batch start
#iterations: 340
currently lose_sum: 94.82100033760071
time_elpased: 1.998
batch start
#iterations: 341
currently lose_sum: 94.52566063404083
time_elpased: 1.877
batch start
#iterations: 342
currently lose_sum: 94.7934039235115
time_elpased: 1.902
batch start
#iterations: 343
currently lose_sum: 94.97093778848648
time_elpased: 1.956
batch start
#iterations: 344
currently lose_sum: 94.82206028699875
time_elpased: 1.935
batch start
#iterations: 345
currently lose_sum: 94.95510965585709
time_elpased: 1.938
batch start
#iterations: 346
currently lose_sum: 95.09781819581985
time_elpased: 1.947
batch start
#iterations: 347
currently lose_sum: 94.77985292673111
time_elpased: 1.943
batch start
#iterations: 348
currently lose_sum: 94.53400045633316
time_elpased: 1.961
batch start
#iterations: 349
currently lose_sum: 94.74434691667557
time_elpased: 1.934
batch start
#iterations: 350
currently lose_sum: 94.7043063044548
time_elpased: 1.91
batch start
#iterations: 351
currently lose_sum: 94.99329298734665
time_elpased: 1.978
batch start
#iterations: 352
currently lose_sum: 94.89403748512268
time_elpased: 2.0
batch start
#iterations: 353
currently lose_sum: 94.97487908601761
time_elpased: 1.902
batch start
#iterations: 354
currently lose_sum: 94.69736295938492
time_elpased: 1.944
batch start
#iterations: 355
currently lose_sum: 94.81250464916229
time_elpased: 1.918
batch start
#iterations: 356
currently lose_sum: 94.7062115073204
time_elpased: 1.906
batch start
#iterations: 357
currently lose_sum: 94.83598309755325
time_elpased: 1.972
batch start
#iterations: 358
currently lose_sum: 94.76247310638428
time_elpased: 1.962
batch start
#iterations: 359
currently lose_sum: 94.7114183306694
time_elpased: 1.948
start validation test
0.653144329897
0.638950892857
0.707008335906
0.67125897699
0.653049763326
61.616
batch start
#iterations: 360
currently lose_sum: 94.83841150999069
time_elpased: 1.94
batch start
#iterations: 361
currently lose_sum: 94.74040281772614
time_elpased: 1.943
batch start
#iterations: 362
currently lose_sum: 94.75489264726639
time_elpased: 1.953
batch start
#iterations: 363
currently lose_sum: 94.90572780370712
time_elpased: 1.892
batch start
#iterations: 364
currently lose_sum: 95.25649702548981
time_elpased: 1.952
batch start
#iterations: 365
currently lose_sum: 94.81714904308319
time_elpased: 1.892
batch start
#iterations: 366
currently lose_sum: 94.6246309876442
time_elpased: 1.937
batch start
#iterations: 367
currently lose_sum: 94.83085083961487
time_elpased: 1.932
batch start
#iterations: 368
currently lose_sum: 94.6464883685112
time_elpased: 1.922
batch start
#iterations: 369
currently lose_sum: 94.90016829967499
time_elpased: 1.944
batch start
#iterations: 370
currently lose_sum: 94.57115709781647
time_elpased: 1.981
batch start
#iterations: 371
currently lose_sum: 94.74562537670135
time_elpased: 1.946
batch start
#iterations: 372
currently lose_sum: 94.76685881614685
time_elpased: 1.949
batch start
#iterations: 373
currently lose_sum: 94.58643645048141
time_elpased: 1.905
batch start
#iterations: 374
currently lose_sum: 94.57682889699936
time_elpased: 1.902
batch start
#iterations: 375
currently lose_sum: 94.91400718688965
time_elpased: 1.958
batch start
#iterations: 376
currently lose_sum: 94.80605298280716
time_elpased: 1.943
batch start
#iterations: 377
currently lose_sum: 94.52970564365387
time_elpased: 1.903
batch start
#iterations: 378
currently lose_sum: 94.83345782756805
time_elpased: 1.901
batch start
#iterations: 379
currently lose_sum: 94.73881256580353
time_elpased: 1.917
start validation test
0.654175257732
0.639001848429
0.711536482453
0.673321322491
0.654074551255
61.685
batch start
#iterations: 380
currently lose_sum: 94.56215089559555
time_elpased: 1.955
batch start
#iterations: 381
currently lose_sum: 94.50232768058777
time_elpased: 1.913
batch start
#iterations: 382
currently lose_sum: 94.62659817934036
time_elpased: 1.966
batch start
#iterations: 383
currently lose_sum: 94.53499746322632
time_elpased: 1.921
batch start
#iterations: 384
currently lose_sum: 95.37926679849625
time_elpased: 1.921
batch start
#iterations: 385
currently lose_sum: 94.22617608308792
time_elpased: 1.93
batch start
#iterations: 386
currently lose_sum: 94.70234471559525
time_elpased: 1.979
batch start
#iterations: 387
currently lose_sum: 94.92880916595459
time_elpased: 1.892
batch start
#iterations: 388
currently lose_sum: 94.9361584186554
time_elpased: 1.937
batch start
#iterations: 389
currently lose_sum: 95.37499111890793
time_elpased: 1.916
batch start
#iterations: 390
currently lose_sum: 94.89697962999344
time_elpased: 1.956
batch start
#iterations: 391
currently lose_sum: 94.8371154665947
time_elpased: 1.965
batch start
#iterations: 392
currently lose_sum: 94.4449348449707
time_elpased: 1.921
batch start
#iterations: 393
currently lose_sum: 94.52950924634933
time_elpased: 1.927
batch start
#iterations: 394
currently lose_sum: 94.1485425233841
time_elpased: 1.937
batch start
#iterations: 395
currently lose_sum: 94.13785326480865
time_elpased: 1.935
batch start
#iterations: 396
currently lose_sum: 94.91390383243561
time_elpased: 1.959
batch start
#iterations: 397
currently lose_sum: 95.02782559394836
time_elpased: 1.919
batch start
#iterations: 398
currently lose_sum: 94.54671972990036
time_elpased: 1.995
batch start
#iterations: 399
currently lose_sum: 94.70036387443542
time_elpased: 1.896
start validation test
0.650979381443
0.641146032963
0.688587012452
0.664020245125
0.650913355446
61.741
acc: 0.654
pre: 0.642
rec: 0.699
F1: 0.669
auc: 0.654
