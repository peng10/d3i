start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.44130194187164
time_elpased: 2.463
batch start
#iterations: 1
currently lose_sum: 99.97319328784943
time_elpased: 2.385
batch start
#iterations: 2
currently lose_sum: 99.69653689861298
time_elpased: 2.309
batch start
#iterations: 3
currently lose_sum: 99.43776881694794
time_elpased: 2.406
batch start
#iterations: 4
currently lose_sum: 99.2866839170456
time_elpased: 2.435
batch start
#iterations: 5
currently lose_sum: 99.17265683412552
time_elpased: 2.334
batch start
#iterations: 6
currently lose_sum: 98.82473731040955
time_elpased: 2.358
batch start
#iterations: 7
currently lose_sum: 98.61436396837234
time_elpased: 2.332
batch start
#iterations: 8
currently lose_sum: 98.46724677085876
time_elpased: 2.433
batch start
#iterations: 9
currently lose_sum: 98.09713822603226
time_elpased: 2.421
batch start
#iterations: 10
currently lose_sum: 98.08659660816193
time_elpased: 2.515
batch start
#iterations: 11
currently lose_sum: 98.11667811870575
time_elpased: 2.44
batch start
#iterations: 12
currently lose_sum: 97.6411429643631
time_elpased: 2.468
batch start
#iterations: 13
currently lose_sum: 97.92243349552155
time_elpased: 2.486
batch start
#iterations: 14
currently lose_sum: 97.63770627975464
time_elpased: 2.448
batch start
#iterations: 15
currently lose_sum: 97.67171424627304
time_elpased: 2.463
batch start
#iterations: 16
currently lose_sum: 97.5942971110344
time_elpased: 2.335
batch start
#iterations: 17
currently lose_sum: 97.49359679222107
time_elpased: 2.475
batch start
#iterations: 18
currently lose_sum: 97.30274379253387
time_elpased: 2.445
batch start
#iterations: 19
currently lose_sum: 97.2076364159584
time_elpased: 2.357
start validation test
0.641391752577
0.634624768225
0.669308357349
0.651505284777
0.641345628488
62.887
batch start
#iterations: 20
currently lose_sum: 97.09208434820175
time_elpased: 2.268
batch start
#iterations: 21
currently lose_sum: 97.01162469387054
time_elpased: 2.208
batch start
#iterations: 22
currently lose_sum: 96.97763079404831
time_elpased: 2.14
batch start
#iterations: 23
currently lose_sum: 97.10359585285187
time_elpased: 2.14
batch start
#iterations: 24
currently lose_sum: 97.08574217557907
time_elpased: 2.243
batch start
#iterations: 25
currently lose_sum: 96.84497278928757
time_elpased: 2.328
batch start
#iterations: 26
currently lose_sum: 96.95872122049332
time_elpased: 2.297
batch start
#iterations: 27
currently lose_sum: 97.01083415746689
time_elpased: 2.36
batch start
#iterations: 28
currently lose_sum: 96.61609262228012
time_elpased: 2.311
batch start
#iterations: 29
currently lose_sum: 97.0303755402565
time_elpased: 2.323
batch start
#iterations: 30
currently lose_sum: 96.59325814247131
time_elpased: 2.338
batch start
#iterations: 31
currently lose_sum: 96.73849332332611
time_elpased: 2.357
batch start
#iterations: 32
currently lose_sum: 96.66420477628708
time_elpased: 2.394
batch start
#iterations: 33
currently lose_sum: 96.76225244998932
time_elpased: 2.403
batch start
#iterations: 34
currently lose_sum: 96.4922416806221
time_elpased: 2.447
batch start
#iterations: 35
currently lose_sum: 96.82339775562286
time_elpased: 2.369
batch start
#iterations: 36
currently lose_sum: 96.24432027339935
time_elpased: 2.298
batch start
#iterations: 37
currently lose_sum: 96.61654728651047
time_elpased: 2.405
batch start
#iterations: 38
currently lose_sum: 96.42587423324585
time_elpased: 2.263
batch start
#iterations: 39
currently lose_sum: 96.47160184383392
time_elpased: 2.261
start validation test
0.651597938144
0.634323612247
0.718608480856
0.673840660136
0.651487222667
62.379
batch start
#iterations: 40
currently lose_sum: 96.5018275976181
time_elpased: 2.355
batch start
#iterations: 41
currently lose_sum: 96.52299851179123
time_elpased: 2.347
batch start
#iterations: 42
currently lose_sum: 96.06656092405319
time_elpased: 2.415
batch start
#iterations: 43
currently lose_sum: 96.41301739215851
time_elpased: 2.352
batch start
#iterations: 44
currently lose_sum: 96.54404282569885
time_elpased: 2.341
batch start
#iterations: 45
currently lose_sum: 96.43901515007019
time_elpased: 2.308
batch start
#iterations: 46
currently lose_sum: 96.35101985931396
time_elpased: 2.358
batch start
#iterations: 47
currently lose_sum: 96.20286458730698
time_elpased: 2.367
batch start
#iterations: 48
currently lose_sum: 96.19796371459961
time_elpased: 2.403
batch start
#iterations: 49
currently lose_sum: 96.28251564502716
time_elpased: 2.37
batch start
#iterations: 50
currently lose_sum: 96.31196200847626
time_elpased: 2.363
batch start
#iterations: 51
currently lose_sum: 95.90192860364914
time_elpased: 2.366
batch start
#iterations: 52
currently lose_sum: 96.58124434947968
time_elpased: 2.271
batch start
#iterations: 53
currently lose_sum: 96.05597740411758
time_elpased: 2.205
batch start
#iterations: 54
currently lose_sum: 96.11167961359024
time_elpased: 2.335
batch start
#iterations: 55
currently lose_sum: 96.19033640623093
time_elpased: 2.259
batch start
#iterations: 56
currently lose_sum: 96.29396444559097
time_elpased: 2.408
batch start
#iterations: 57
currently lose_sum: 96.24534958600998
time_elpased: 2.311
batch start
#iterations: 58
currently lose_sum: 96.02182775735855
time_elpased: 2.352
batch start
#iterations: 59
currently lose_sum: 95.85030430555344
time_elpased: 2.331
start validation test
0.646855670103
0.63564056434
0.690922190202
0.662129506337
0.646782862965
62.179
batch start
#iterations: 60
currently lose_sum: 95.99252563714981
time_elpased: 2.383
batch start
#iterations: 61
currently lose_sum: 96.33785218000412
time_elpased: 2.27
batch start
#iterations: 62
currently lose_sum: 96.42384535074234
time_elpased: 2.318
batch start
#iterations: 63
currently lose_sum: 95.71404272317886
time_elpased: 2.249
batch start
#iterations: 64
currently lose_sum: 96.06001335382462
time_elpased: 2.303
batch start
#iterations: 65
currently lose_sum: 96.03373092412949
time_elpased: 2.394
batch start
#iterations: 66
currently lose_sum: 96.18274629116058
time_elpased: 2.357
batch start
#iterations: 67
currently lose_sum: 95.88027024269104
time_elpased: 2.344
batch start
#iterations: 68
currently lose_sum: 95.79131495952606
time_elpased: 2.381
batch start
#iterations: 69
currently lose_sum: 96.06444382667542
time_elpased: 2.295
batch start
#iterations: 70
currently lose_sum: 96.00762021541595
time_elpased: 2.321
batch start
#iterations: 71
currently lose_sum: 95.82655626535416
time_elpased: 2.314
batch start
#iterations: 72
currently lose_sum: 95.73372238874435
time_elpased: 2.38
batch start
#iterations: 73
currently lose_sum: 95.72641253471375
time_elpased: 2.379
batch start
#iterations: 74
currently lose_sum: 95.83816409111023
time_elpased: 2.359
batch start
#iterations: 75
currently lose_sum: 95.8024759888649
time_elpased: 2.392
batch start
#iterations: 76
currently lose_sum: 96.02853846549988
time_elpased: 2.36
batch start
#iterations: 77
currently lose_sum: 95.67590522766113
time_elpased: 2.286
batch start
#iterations: 78
currently lose_sum: 96.17652404308319
time_elpased: 2.211
batch start
#iterations: 79
currently lose_sum: 95.55218577384949
time_elpased: 2.151
start validation test
0.654896907216
0.643026228577
0.698950185261
0.669822952113
0.654824121957
61.925
batch start
#iterations: 80
currently lose_sum: 96.02936559915543
time_elpased: 2.142
batch start
#iterations: 81
currently lose_sum: 95.92306244373322
time_elpased: 2.212
batch start
#iterations: 82
currently lose_sum: 95.48709088563919
time_elpased: 2.194
batch start
#iterations: 83
currently lose_sum: 95.67057597637177
time_elpased: 2.246
batch start
#iterations: 84
currently lose_sum: 95.5038251876831
time_elpased: 2.219
batch start
#iterations: 85
currently lose_sum: 95.54367315769196
time_elpased: 2.231
batch start
#iterations: 86
currently lose_sum: 95.67500621080399
time_elpased: 2.213
batch start
#iterations: 87
currently lose_sum: 95.44492506980896
time_elpased: 2.165
batch start
#iterations: 88
currently lose_sum: 95.94626235961914
time_elpased: 2.194
batch start
#iterations: 89
currently lose_sum: 95.9830852150917
time_elpased: 2.218
batch start
#iterations: 90
currently lose_sum: 95.69332605600357
time_elpased: 2.311
batch start
#iterations: 91
currently lose_sum: 95.57474172115326
time_elpased: 2.337
batch start
#iterations: 92
currently lose_sum: 95.43132323026657
time_elpased: 2.319
batch start
#iterations: 93
currently lose_sum: 95.29124337434769
time_elpased: 2.32
batch start
#iterations: 94
currently lose_sum: 95.72178834676743
time_elpased: 2.309
batch start
#iterations: 95
currently lose_sum: 95.94712418317795
time_elpased: 2.386
batch start
#iterations: 96
currently lose_sum: 95.70954942703247
time_elpased: 2.366
batch start
#iterations: 97
currently lose_sum: 95.61770993471146
time_elpased: 2.174
batch start
#iterations: 98
currently lose_sum: 95.73160570859909
time_elpased: 2.164
batch start
#iterations: 99
currently lose_sum: 95.76193273067474
time_elpased: 2.143
start validation test
0.650360824742
0.638728597105
0.694936187732
0.66564795189
0.650287176889
62.003
batch start
#iterations: 100
currently lose_sum: 95.77199798822403
time_elpased: 2.134
batch start
#iterations: 101
currently lose_sum: 95.68496894836426
time_elpased: 2.225
batch start
#iterations: 102
currently lose_sum: 96.0922766327858
time_elpased: 2.304
batch start
#iterations: 103
currently lose_sum: 95.37084686756134
time_elpased: 2.29
batch start
#iterations: 104
currently lose_sum: 95.48878335952759
time_elpased: 2.24
batch start
#iterations: 105
currently lose_sum: 95.71552324295044
time_elpased: 2.304
batch start
#iterations: 106
currently lose_sum: 95.21334666013718
time_elpased: 2.217
batch start
#iterations: 107
currently lose_sum: 95.92496645450592
time_elpased: 2.151
batch start
#iterations: 108
currently lose_sum: 95.27979725599289
time_elpased: 2.352
batch start
#iterations: 109
currently lose_sum: 95.63394445180893
time_elpased: 2.422
batch start
#iterations: 110
currently lose_sum: 95.62673115730286
time_elpased: 2.366
batch start
#iterations: 111
currently lose_sum: 95.73714315891266
time_elpased: 2.259
batch start
#iterations: 112
currently lose_sum: 95.49660551548004
time_elpased: 2.313
batch start
#iterations: 113
currently lose_sum: 95.74142509698868
time_elpased: 2.342
batch start
#iterations: 114
currently lose_sum: 95.59033244848251
time_elpased: 2.207
batch start
#iterations: 115
currently lose_sum: 95.57140743732452
time_elpased: 2.133
batch start
#iterations: 116
currently lose_sum: 95.63807034492493
time_elpased: 2.159
batch start
#iterations: 117
currently lose_sum: 95.44475018978119
time_elpased: 2.212
batch start
#iterations: 118
currently lose_sum: 95.47902941703796
time_elpased: 2.195
batch start
#iterations: 119
currently lose_sum: 95.61406743526459
time_elpased: 2.198
start validation test
0.655154639175
0.638123060069
0.719431864965
0.676342525399
0.655048439711
61.854
batch start
#iterations: 120
currently lose_sum: 95.0365582704544
time_elpased: 2.241
batch start
#iterations: 121
currently lose_sum: 95.38179302215576
time_elpased: 2.125
batch start
#iterations: 122
currently lose_sum: 95.62223100662231
time_elpased: 2.178
batch start
#iterations: 123
currently lose_sum: 95.38616198301315
time_elpased: 2.166
batch start
#iterations: 124
currently lose_sum: 95.46991240978241
time_elpased: 2.194
batch start
#iterations: 125
currently lose_sum: 95.54683405160904
time_elpased: 2.154
batch start
#iterations: 126
currently lose_sum: 95.26024597883224
time_elpased: 2.171
batch start
#iterations: 127
currently lose_sum: 95.54967147111893
time_elpased: 2.246
batch start
#iterations: 128
currently lose_sum: 95.13760364055634
time_elpased: 2.249
batch start
#iterations: 129
currently lose_sum: 95.61172050237656
time_elpased: 2.175
batch start
#iterations: 130
currently lose_sum: 95.59319591522217
time_elpased: 2.212
batch start
#iterations: 131
currently lose_sum: 94.86821037530899
time_elpased: 2.191
batch start
#iterations: 132
currently lose_sum: 95.48470294475555
time_elpased: 2.187
batch start
#iterations: 133
currently lose_sum: 95.80794888734818
time_elpased: 2.154
batch start
#iterations: 134
currently lose_sum: 95.36634457111359
time_elpased: 2.067
batch start
#iterations: 135
currently lose_sum: 95.44123136997223
time_elpased: 2.163
batch start
#iterations: 136
currently lose_sum: 95.41507863998413
time_elpased: 2.151
batch start
#iterations: 137
currently lose_sum: 95.29662585258484
time_elpased: 2.179
batch start
#iterations: 138
currently lose_sum: 95.65884095430374
time_elpased: 2.239
batch start
#iterations: 139
currently lose_sum: 95.31979483366013
time_elpased: 2.177
start validation test
0.658144329897
0.639648614381
0.726945244957
0.68050871953
0.658030656349
61.711
batch start
#iterations: 140
currently lose_sum: 94.99001485109329
time_elpased: 2.181
batch start
#iterations: 141
currently lose_sum: 95.23089623451233
time_elpased: 2.107
batch start
#iterations: 142
currently lose_sum: 95.64876365661621
time_elpased: 2.178
batch start
#iterations: 143
currently lose_sum: 95.26990777254105
time_elpased: 2.177
batch start
#iterations: 144
currently lose_sum: 95.41345393657684
time_elpased: 2.234
batch start
#iterations: 145
currently lose_sum: 95.56457030773163
time_elpased: 2.201
batch start
#iterations: 146
currently lose_sum: 94.79080522060394
time_elpased: 2.217
batch start
#iterations: 147
currently lose_sum: 95.4647588133812
time_elpased: 2.174
batch start
#iterations: 148
currently lose_sum: 95.40801560878754
time_elpased: 2.17
batch start
#iterations: 149
currently lose_sum: 95.54729092121124
time_elpased: 2.18
batch start
#iterations: 150
currently lose_sum: 95.28636127710342
time_elpased: 2.25
batch start
#iterations: 151
currently lose_sum: 95.82231116294861
time_elpased: 2.243
batch start
#iterations: 152
currently lose_sum: 95.30005013942719
time_elpased: 2.289
batch start
#iterations: 153
currently lose_sum: 95.65318995714188
time_elpased: 2.211
batch start
#iterations: 154
currently lose_sum: 95.3823914527893
time_elpased: 2.101
batch start
#iterations: 155
currently lose_sum: 95.09793055057526
time_elpased: 2.225
batch start
#iterations: 156
currently lose_sum: 95.49919122457504
time_elpased: 2.155
batch start
#iterations: 157
currently lose_sum: 95.08836251497269
time_elpased: 2.23
batch start
#iterations: 158
currently lose_sum: 95.50775676965714
time_elpased: 2.178
batch start
#iterations: 159
currently lose_sum: 95.48060774803162
time_elpased: 2.214
start validation test
0.65293814433
0.643151933967
0.689687114039
0.665607151726
0.652877427321
61.765
batch start
#iterations: 160
currently lose_sum: 95.15641736984253
time_elpased: 2.165
batch start
#iterations: 161
currently lose_sum: 95.32290226221085
time_elpased: 2.2
batch start
#iterations: 162
currently lose_sum: 95.49138653278351
time_elpased: 2.209
batch start
#iterations: 163
currently lose_sum: 95.23224145174026
time_elpased: 2.199
batch start
#iterations: 164
currently lose_sum: 95.68241357803345
time_elpased: 2.211
batch start
#iterations: 165
currently lose_sum: 95.31266939640045
time_elpased: 2.191
batch start
#iterations: 166
currently lose_sum: 95.24577713012695
time_elpased: 2.165
batch start
#iterations: 167
currently lose_sum: 95.32330185174942
time_elpased: 2.148
batch start
#iterations: 168
currently lose_sum: 95.31246703863144
time_elpased: 2.204
batch start
#iterations: 169
currently lose_sum: 95.29664069414139
time_elpased: 2.18
batch start
#iterations: 170
currently lose_sum: 95.11182689666748
time_elpased: 2.19
batch start
#iterations: 171
currently lose_sum: 95.14414012432098
time_elpased: 2.207
batch start
#iterations: 172
currently lose_sum: 94.8213022351265
time_elpased: 2.182
batch start
#iterations: 173
currently lose_sum: 95.21322512626648
time_elpased: 2.232
batch start
#iterations: 174
currently lose_sum: 95.45814698934555
time_elpased: 2.133
batch start
#iterations: 175
currently lose_sum: 95.11334604024887
time_elpased: 2.203
batch start
#iterations: 176
currently lose_sum: 95.23831641674042
time_elpased: 2.153
batch start
#iterations: 177
currently lose_sum: 95.22303307056427
time_elpased: 2.193
batch start
#iterations: 178
currently lose_sum: 95.20726102590561
time_elpased: 2.188
batch start
#iterations: 179
currently lose_sum: 95.30287861824036
time_elpased: 2.183
start validation test
0.657319587629
0.637013219007
0.734046932894
0.682096403979
0.657192817955
61.549
batch start
#iterations: 180
currently lose_sum: 94.97060108184814
time_elpased: 2.202
batch start
#iterations: 181
currently lose_sum: 95.33767366409302
time_elpased: 2.212
batch start
#iterations: 182
currently lose_sum: 95.63733088970184
time_elpased: 2.194
batch start
#iterations: 183
currently lose_sum: 95.14431130886078
time_elpased: 2.158
batch start
#iterations: 184
currently lose_sum: 95.22724896669388
time_elpased: 2.173
batch start
#iterations: 185
currently lose_sum: 95.18346267938614
time_elpased: 2.19
batch start
#iterations: 186
currently lose_sum: 95.05929124355316
time_elpased: 2.195
batch start
#iterations: 187
currently lose_sum: 95.12700748443604
time_elpased: 2.135
batch start
#iterations: 188
currently lose_sum: 95.55135941505432
time_elpased: 2.195
batch start
#iterations: 189
currently lose_sum: 95.1396319270134
time_elpased: 2.15
batch start
#iterations: 190
currently lose_sum: 95.56239432096481
time_elpased: 2.205
batch start
#iterations: 191
currently lose_sum: 95.34364134073257
time_elpased: 2.203
batch start
#iterations: 192
currently lose_sum: 95.27800422906876
time_elpased: 2.245
batch start
#iterations: 193
currently lose_sum: 94.6752462387085
time_elpased: 2.207
batch start
#iterations: 194
currently lose_sum: 95.20909810066223
time_elpased: 2.25
batch start
#iterations: 195
currently lose_sum: 94.99541759490967
time_elpased: 2.277
batch start
#iterations: 196
currently lose_sum: 94.9406071305275
time_elpased: 2.214
batch start
#iterations: 197
currently lose_sum: 94.91381418704987
time_elpased: 2.258
batch start
#iterations: 198
currently lose_sum: 94.87448179721832
time_elpased: 2.26
batch start
#iterations: 199
currently lose_sum: 95.20968806743622
time_elpased: 2.214
start validation test
0.651958762887
0.641008563273
0.693392342528
0.666172253535
0.651890305919
61.764
batch start
#iterations: 200
currently lose_sum: 95.21409475803375
time_elpased: 2.194
batch start
#iterations: 201
currently lose_sum: 94.75610840320587
time_elpased: 2.171
batch start
#iterations: 202
currently lose_sum: 95.4504126906395
time_elpased: 2.258
batch start
#iterations: 203
currently lose_sum: 95.40696507692337
time_elpased: 2.239
batch start
#iterations: 204
currently lose_sum: 95.4071455001831
time_elpased: 2.312
batch start
#iterations: 205
currently lose_sum: 95.48101097345352
time_elpased: 2.257
batch start
#iterations: 206
currently lose_sum: 95.26624321937561
time_elpased: 2.242
batch start
#iterations: 207
currently lose_sum: 94.68679201602936
time_elpased: 2.267
batch start
#iterations: 208
currently lose_sum: 94.7319266796112
time_elpased: 2.249
batch start
#iterations: 209
currently lose_sum: 94.67750835418701
time_elpased: 2.307
batch start
#iterations: 210
currently lose_sum: 95.20280915498734
time_elpased: 2.234
batch start
#iterations: 211
currently lose_sum: 95.03668290376663
time_elpased: 2.163
batch start
#iterations: 212
currently lose_sum: 94.95472013950348
time_elpased: 2.252
batch start
#iterations: 213
currently lose_sum: 94.74525171518326
time_elpased: 2.201
batch start
#iterations: 214
currently lose_sum: 95.26380640268326
time_elpased: 2.253
batch start
#iterations: 215
currently lose_sum: 95.16266864538193
time_elpased: 2.256
batch start
#iterations: 216
currently lose_sum: 94.82877689599991
time_elpased: 2.216
batch start
#iterations: 217
currently lose_sum: 95.20961475372314
time_elpased: 2.186
batch start
#iterations: 218
currently lose_sum: 95.26555395126343
time_elpased: 2.263
batch start
#iterations: 219
currently lose_sum: 95.12094926834106
time_elpased: 2.246
start validation test
0.657113402062
0.641720629047
0.713976945245
0.675923219332
0.657019451557
61.299
batch start
#iterations: 220
currently lose_sum: 95.2801861166954
time_elpased: 2.269
batch start
#iterations: 221
currently lose_sum: 94.868437230587
time_elpased: 2.312
batch start
#iterations: 222
currently lose_sum: 94.95523798465729
time_elpased: 2.342
batch start
#iterations: 223
currently lose_sum: 94.5022731423378
time_elpased: 2.263
batch start
#iterations: 224
currently lose_sum: 95.15317785739899
time_elpased: 2.265
batch start
#iterations: 225
currently lose_sum: 94.95007759332657
time_elpased: 2.234
batch start
#iterations: 226
currently lose_sum: 94.73028039932251
time_elpased: 2.238
batch start
#iterations: 227
currently lose_sum: 95.04766809940338
time_elpased: 2.221
batch start
#iterations: 228
currently lose_sum: 95.23925215005875
time_elpased: 2.364
batch start
#iterations: 229
currently lose_sum: 95.27974855899811
time_elpased: 2.396
batch start
#iterations: 230
currently lose_sum: 94.86149042844772
time_elpased: 2.402
batch start
#iterations: 231
currently lose_sum: 95.39363688230515
time_elpased: 2.351
batch start
#iterations: 232
currently lose_sum: 95.41935116052628
time_elpased: 2.38
batch start
#iterations: 233
currently lose_sum: 95.22818553447723
time_elpased: 2.367
batch start
#iterations: 234
currently lose_sum: 95.16379511356354
time_elpased: 2.362
batch start
#iterations: 235
currently lose_sum: 95.19694888591766
time_elpased: 2.293
batch start
#iterations: 236
currently lose_sum: 95.43210035562515
time_elpased: 2.33
batch start
#iterations: 237
currently lose_sum: 94.78538179397583
time_elpased: 2.452
batch start
#iterations: 238
currently lose_sum: 94.96244126558304
time_elpased: 2.34
batch start
#iterations: 239
currently lose_sum: 95.05130356550217
time_elpased: 2.371
start validation test
0.654278350515
0.638701945238
0.713050638123
0.673831639352
0.654181246364
61.594
batch start
#iterations: 240
currently lose_sum: 95.35933685302734
time_elpased: 2.415
batch start
#iterations: 241
currently lose_sum: 94.8615049123764
time_elpased: 2.334
batch start
#iterations: 242
currently lose_sum: 95.07369422912598
time_elpased: 2.345
batch start
#iterations: 243
currently lose_sum: 95.41226238012314
time_elpased: 2.301
batch start
#iterations: 244
currently lose_sum: 94.8618038892746
time_elpased: 2.269
batch start
#iterations: 245
currently lose_sum: 95.14727532863617
time_elpased: 2.298
batch start
#iterations: 246
currently lose_sum: 94.5833575129509
time_elpased: 2.35
batch start
#iterations: 247
currently lose_sum: 94.81013053655624
time_elpased: 2.397
batch start
#iterations: 248
currently lose_sum: 95.02037024497986
time_elpased: 2.457
batch start
#iterations: 249
currently lose_sum: 95.48088955879211
time_elpased: 2.466
batch start
#iterations: 250
currently lose_sum: 94.93992006778717
time_elpased: 2.411
batch start
#iterations: 251
currently lose_sum: 95.05991286039352
time_elpased: 2.453
batch start
#iterations: 252
currently lose_sum: 94.96722483634949
time_elpased: 2.409
batch start
#iterations: 253
currently lose_sum: 95.07918053865433
time_elpased: 2.244
batch start
#iterations: 254
currently lose_sum: 94.87524896860123
time_elpased: 2.36
batch start
#iterations: 255
currently lose_sum: 94.58491390943527
time_elpased: 2.373
batch start
#iterations: 256
currently lose_sum: 94.84480756521225
time_elpased: 2.363
batch start
#iterations: 257
currently lose_sum: 95.19359558820724
time_elpased: 2.368
batch start
#iterations: 258
currently lose_sum: 94.8577641248703
time_elpased: 2.402
batch start
#iterations: 259
currently lose_sum: 95.12538009881973
time_elpased: 2.405
start validation test
0.647989690722
0.642455343926
0.670028818444
0.655952440929
0.647953277458
61.784
batch start
#iterations: 260
currently lose_sum: 94.6531657576561
time_elpased: 2.406
batch start
#iterations: 261
currently lose_sum: 95.09814327955246
time_elpased: 2.281
batch start
#iterations: 262
currently lose_sum: 95.16106033325195
time_elpased: 2.258
batch start
#iterations: 263
currently lose_sum: 94.79145640134811
time_elpased: 2.386
batch start
#iterations: 264
currently lose_sum: 95.10122895240784
time_elpased: 2.348
batch start
#iterations: 265
currently lose_sum: 95.27145618200302
time_elpased: 2.33
batch start
#iterations: 266
currently lose_sum: 94.8435589671135
time_elpased: 2.339
batch start
#iterations: 267
currently lose_sum: 94.97644698619843
time_elpased: 2.348
batch start
#iterations: 268
currently lose_sum: 94.73216438293457
time_elpased: 2.384
batch start
#iterations: 269
currently lose_sum: 95.36317974328995
time_elpased: 2.327
batch start
#iterations: 270
currently lose_sum: 94.57504153251648
time_elpased: 2.211
batch start
#iterations: 271
currently lose_sum: 94.84703147411346
time_elpased: 2.243
batch start
#iterations: 272
currently lose_sum: 94.89954870939255
time_elpased: 2.297
batch start
#iterations: 273
currently lose_sum: 95.16608357429504
time_elpased: 2.224
batch start
#iterations: 274
currently lose_sum: 94.9545134305954
time_elpased: 2.165
batch start
#iterations: 275
currently lose_sum: 94.75413435697556
time_elpased: 2.19
batch start
#iterations: 276
currently lose_sum: 94.62479877471924
time_elpased: 2.226
batch start
#iterations: 277
currently lose_sum: 94.96280270814896
time_elpased: 2.153
batch start
#iterations: 278
currently lose_sum: 95.27432650327682
time_elpased: 2.181
batch start
#iterations: 279
currently lose_sum: 95.15019619464874
time_elpased: 2.173
start validation test
0.656649484536
0.638548752834
0.724578015644
0.678848657249
0.656537252349
61.539
batch start
#iterations: 280
currently lose_sum: 95.1447337269783
time_elpased: 2.236
batch start
#iterations: 281
currently lose_sum: 94.66501247882843
time_elpased: 2.171
batch start
#iterations: 282
currently lose_sum: 94.95555037260056
time_elpased: 2.246
batch start
#iterations: 283
currently lose_sum: 95.1783287525177
time_elpased: 2.189
batch start
#iterations: 284
currently lose_sum: 95.00136524438858
time_elpased: 2.261
batch start
#iterations: 285
currently lose_sum: 94.68168866634369
time_elpased: 2.188
batch start
#iterations: 286
currently lose_sum: 94.35427337884903
time_elpased: 2.265
batch start
#iterations: 287
currently lose_sum: 94.94925355911255
time_elpased: 2.208
batch start
#iterations: 288
currently lose_sum: 94.96021288633347
time_elpased: 2.205
batch start
#iterations: 289
currently lose_sum: 94.9809895157814
time_elpased: 2.178
batch start
#iterations: 290
currently lose_sum: 94.55007249116898
time_elpased: 2.21
batch start
#iterations: 291
currently lose_sum: 94.81759816408157
time_elpased: 2.264
batch start
#iterations: 292
currently lose_sum: 94.67718952894211
time_elpased: 2.257
batch start
#iterations: 293
currently lose_sum: 94.93202435970306
time_elpased: 2.191
batch start
#iterations: 294
currently lose_sum: 95.17338609695435
time_elpased: 2.17
batch start
#iterations: 295
currently lose_sum: 94.64530420303345
time_elpased: 2.204
batch start
#iterations: 296
currently lose_sum: 94.83384120464325
time_elpased: 2.13
batch start
#iterations: 297
currently lose_sum: 95.18648660182953
time_elpased: 2.243
batch start
#iterations: 298
currently lose_sum: 94.87199372053146
time_elpased: 2.213
batch start
#iterations: 299
currently lose_sum: 94.79514187574387
time_elpased: 2.19
start validation test
0.657268041237
0.637793153024
0.730547550432
0.68102662509
0.657146968112
61.609
batch start
#iterations: 300
currently lose_sum: 94.92123371362686
time_elpased: 2.245
batch start
#iterations: 301
currently lose_sum: 94.67069745063782
time_elpased: 2.214
batch start
#iterations: 302
currently lose_sum: 95.01023721694946
time_elpased: 2.16
batch start
#iterations: 303
currently lose_sum: 94.63714098930359
time_elpased: 2.201
batch start
#iterations: 304
currently lose_sum: 94.5005071759224
time_elpased: 2.251
batch start
#iterations: 305
currently lose_sum: 95.03468042612076
time_elpased: 2.215
batch start
#iterations: 306
currently lose_sum: 94.84092807769775
time_elpased: 2.215
batch start
#iterations: 307
currently lose_sum: 94.69100785255432
time_elpased: 2.225
batch start
#iterations: 308
currently lose_sum: 94.77714967727661
time_elpased: 2.176
batch start
#iterations: 309
currently lose_sum: 94.89439159631729
time_elpased: 2.166
batch start
#iterations: 310
currently lose_sum: 95.03556084632874
time_elpased: 2.205
batch start
#iterations: 311
currently lose_sum: 94.6690371632576
time_elpased: 2.24
batch start
#iterations: 312
currently lose_sum: 94.82384395599365
time_elpased: 2.136
batch start
#iterations: 313
currently lose_sum: 94.69616478681564
time_elpased: 2.15
batch start
#iterations: 314
currently lose_sum: 94.93903350830078
time_elpased: 2.169
batch start
#iterations: 315
currently lose_sum: 94.49445027112961
time_elpased: 2.117
batch start
#iterations: 316
currently lose_sum: 94.84778249263763
time_elpased: 2.163
batch start
#iterations: 317
currently lose_sum: 94.84908229112625
time_elpased: 2.174
batch start
#iterations: 318
currently lose_sum: 94.64046633243561
time_elpased: 2.18
batch start
#iterations: 319
currently lose_sum: 94.78532326221466
time_elpased: 2.187
start validation test
0.655515463918
0.641848283603
0.706257719226
0.672514333317
0.655431627064
61.573
batch start
#iterations: 320
currently lose_sum: 95.03588795661926
time_elpased: 2.265
batch start
#iterations: 321
currently lose_sum: 95.07419973611832
time_elpased: 2.194
batch start
#iterations: 322
currently lose_sum: 94.65908366441727
time_elpased: 2.117
batch start
#iterations: 323
currently lose_sum: 94.74829941987991
time_elpased: 2.315
batch start
#iterations: 324
currently lose_sum: 95.10995036363602
time_elpased: 2.176
batch start
#iterations: 325
currently lose_sum: 94.52795654535294
time_elpased: 2.223
batch start
#iterations: 326
currently lose_sum: 94.61078315973282
time_elpased: 2.198
batch start
#iterations: 327
currently lose_sum: 94.81415712833405
time_elpased: 2.181
batch start
#iterations: 328
currently lose_sum: 94.61435294151306
time_elpased: 2.155
batch start
#iterations: 329
currently lose_sum: 94.81776893138885
time_elpased: 2.13
batch start
#iterations: 330
currently lose_sum: 94.9821349978447
time_elpased: 2.22
batch start
#iterations: 331
currently lose_sum: 94.95386004447937
time_elpased: 2.128
batch start
#iterations: 332
currently lose_sum: 94.97323721647263
time_elpased: 2.193
batch start
#iterations: 333
currently lose_sum: 94.96893870830536
time_elpased: 2.151
batch start
#iterations: 334
currently lose_sum: 94.76312011480331
time_elpased: 2.255
batch start
#iterations: 335
currently lose_sum: 94.70447969436646
time_elpased: 2.092
batch start
#iterations: 336
currently lose_sum: 94.61426705121994
time_elpased: 2.185
batch start
#iterations: 337
currently lose_sum: 94.89359927177429
time_elpased: 2.23
batch start
#iterations: 338
currently lose_sum: 94.51378005743027
time_elpased: 2.21
batch start
#iterations: 339
currently lose_sum: 94.54711878299713
time_elpased: 2.219
start validation test
0.656855670103
0.638679844048
0.724989707699
0.679103398409
0.656743098376
61.472
batch start
#iterations: 340
currently lose_sum: 95.25580668449402
time_elpased: 2.234
batch start
#iterations: 341
currently lose_sum: 94.72563081979752
time_elpased: 2.179
batch start
#iterations: 342
currently lose_sum: 94.76503562927246
time_elpased: 2.202
batch start
#iterations: 343
currently lose_sum: 94.93027329444885
time_elpased: 2.223
batch start
#iterations: 344
currently lose_sum: 94.70895838737488
time_elpased: 2.256
batch start
#iterations: 345
currently lose_sum: 95.02236479520798
time_elpased: 2.25
batch start
#iterations: 346
currently lose_sum: 94.99803274869919
time_elpased: 2.223
batch start
#iterations: 347
currently lose_sum: 94.66902577877045
time_elpased: 2.233
batch start
#iterations: 348
currently lose_sum: 95.11934489011765
time_elpased: 2.115
batch start
#iterations: 349
currently lose_sum: 94.76832967996597
time_elpased: 2.171
batch start
#iterations: 350
currently lose_sum: 95.1665351986885
time_elpased: 2.202
batch start
#iterations: 351
currently lose_sum: 94.96154999732971
time_elpased: 2.216
batch start
#iterations: 352
currently lose_sum: 94.69564545154572
time_elpased: 2.185
batch start
#iterations: 353
currently lose_sum: 95.0317000746727
time_elpased: 2.193
batch start
#iterations: 354
currently lose_sum: 94.98649430274963
time_elpased: 2.154
batch start
#iterations: 355
currently lose_sum: 94.57989114522934
time_elpased: 2.16
batch start
#iterations: 356
currently lose_sum: 94.43694978952408
time_elpased: 2.12
batch start
#iterations: 357
currently lose_sum: 94.63717019557953
time_elpased: 2.225
batch start
#iterations: 358
currently lose_sum: 95.09102314710617
time_elpased: 2.197
batch start
#iterations: 359
currently lose_sum: 94.46183168888092
time_elpased: 2.17
start validation test
0.651546391753
0.63818249813
0.702552490737
0.668822261415
0.651462118974
61.656
batch start
#iterations: 360
currently lose_sum: 95.26321649551392
time_elpased: 2.247
batch start
#iterations: 361
currently lose_sum: 94.63234782218933
time_elpased: 2.156
batch start
#iterations: 362
currently lose_sum: 95.17146474123001
time_elpased: 2.249
batch start
#iterations: 363
currently lose_sum: 94.59493374824524
time_elpased: 2.227
batch start
#iterations: 364
currently lose_sum: 95.11066710948944
time_elpased: 2.224
batch start
#iterations: 365
currently lose_sum: 94.97145295143127
time_elpased: 2.204
batch start
#iterations: 366
currently lose_sum: 94.69458562135696
time_elpased: 2.195
batch start
#iterations: 367
currently lose_sum: 94.68833774328232
time_elpased: 2.144
batch start
#iterations: 368
currently lose_sum: 94.63331341743469
time_elpased: 2.235
batch start
#iterations: 369
currently lose_sum: 94.64280647039413
time_elpased: 2.206
batch start
#iterations: 370
currently lose_sum: 95.23089617490768
time_elpased: 2.181
batch start
#iterations: 371
currently lose_sum: 94.77316808700562
time_elpased: 2.16
batch start
#iterations: 372
currently lose_sum: 94.86244267225266
time_elpased: 2.186
batch start
#iterations: 373
currently lose_sum: 94.58105665445328
time_elpased: 2.216
batch start
#iterations: 374
currently lose_sum: 94.50667095184326
time_elpased: 2.134
batch start
#iterations: 375
currently lose_sum: 95.04976779222488
time_elpased: 2.225
batch start
#iterations: 376
currently lose_sum: 94.55439078807831
time_elpased: 2.165
batch start
#iterations: 377
currently lose_sum: 94.57850533723831
time_elpased: 2.17
batch start
#iterations: 378
currently lose_sum: 94.72646236419678
time_elpased: 2.184
batch start
#iterations: 379
currently lose_sum: 95.12451994419098
time_elpased: 2.218
start validation test
0.653195876289
0.636388533869
0.717476327707
0.67450411224
0.653089671495
61.687
batch start
#iterations: 380
currently lose_sum: 95.03825384378433
time_elpased: 2.16
batch start
#iterations: 381
currently lose_sum: 94.34349250793457
time_elpased: 2.129
batch start
#iterations: 382
currently lose_sum: 94.92149698734283
time_elpased: 2.22
batch start
#iterations: 383
currently lose_sum: 94.98544782400131
time_elpased: 2.188
batch start
#iterations: 384
currently lose_sum: 94.56709265708923
time_elpased: 2.268
batch start
#iterations: 385
currently lose_sum: 94.69011324644089
time_elpased: 2.21
batch start
#iterations: 386
currently lose_sum: 94.80853939056396
time_elpased: 2.267
batch start
#iterations: 387
currently lose_sum: 94.61605489253998
time_elpased: 2.183
batch start
#iterations: 388
currently lose_sum: 94.79249107837677
time_elpased: 2.207
batch start
#iterations: 389
currently lose_sum: 94.3422549366951
time_elpased: 2.216
batch start
#iterations: 390
currently lose_sum: 94.97460788488388
time_elpased: 2.207
batch start
#iterations: 391
currently lose_sum: 94.33905071020126
time_elpased: 2.431
batch start
#iterations: 392
currently lose_sum: 94.36362719535828
time_elpased: 2.338
batch start
#iterations: 393
currently lose_sum: 94.6663048863411
time_elpased: 2.376
batch start
#iterations: 394
currently lose_sum: 94.76877444982529
time_elpased: 2.328
batch start
#iterations: 395
currently lose_sum: 94.7882748246193
time_elpased: 2.263
batch start
#iterations: 396
currently lose_sum: 94.81726098060608
time_elpased: 2.453
batch start
#iterations: 397
currently lose_sum: 94.85301810503006
time_elpased: 2.456
batch start
#iterations: 398
currently lose_sum: 94.61868184804916
time_elpased: 2.408
batch start
#iterations: 399
currently lose_sum: 95.00026828050613
time_elpased: 2.397
start validation test
0.652319587629
0.635082295171
0.718814326883
0.674359098151
0.652209724367
61.700
acc: 0.663
pre: 0.647
rec: 0.722
F1: 0.682
auc: 0.663
