start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.45480632781982
time_elpased: 2.151
batch start
#iterations: 1
currently lose_sum: 99.90978825092316
time_elpased: 1.949
batch start
#iterations: 2
currently lose_sum: 99.90942007303238
time_elpased: 1.924
batch start
#iterations: 3
currently lose_sum: 99.46541875600815
time_elpased: 1.964
batch start
#iterations: 4
currently lose_sum: 99.5048640370369
time_elpased: 1.955
batch start
#iterations: 5
currently lose_sum: 99.06241184473038
time_elpased: 1.95
batch start
#iterations: 6
currently lose_sum: 98.99217593669891
time_elpased: 1.976
batch start
#iterations: 7
currently lose_sum: 98.55453777313232
time_elpased: 1.967
batch start
#iterations: 8
currently lose_sum: 98.5086442232132
time_elpased: 1.922
batch start
#iterations: 9
currently lose_sum: 98.22164589166641
time_elpased: 1.96
batch start
#iterations: 10
currently lose_sum: 98.03574776649475
time_elpased: 1.931
batch start
#iterations: 11
currently lose_sum: 98.00345915555954
time_elpased: 1.955
batch start
#iterations: 12
currently lose_sum: 97.85578215122223
time_elpased: 1.986
batch start
#iterations: 13
currently lose_sum: 97.80969226360321
time_elpased: 1.986
batch start
#iterations: 14
currently lose_sum: 97.65039128065109
time_elpased: 1.909
batch start
#iterations: 15
currently lose_sum: 97.67637544870377
time_elpased: 1.944
batch start
#iterations: 16
currently lose_sum: 97.6505977511406
time_elpased: 1.971
batch start
#iterations: 17
currently lose_sum: 97.35470682382584
time_elpased: 1.906
batch start
#iterations: 18
currently lose_sum: 97.36865538358688
time_elpased: 1.932
batch start
#iterations: 19
currently lose_sum: 97.45762532949448
time_elpased: 1.928
start validation test
0.640257731959
0.641308835673
0.639394874961
0.640350425148
0.640259246837
62.935
batch start
#iterations: 20
currently lose_sum: 97.66760569810867
time_elpased: 1.956
batch start
#iterations: 21
currently lose_sum: 96.98841309547424
time_elpased: 1.979
batch start
#iterations: 22
currently lose_sum: 97.16499245166779
time_elpased: 1.933
batch start
#iterations: 23
currently lose_sum: 97.39917850494385
time_elpased: 1.956
batch start
#iterations: 24
currently lose_sum: 97.09857058525085
time_elpased: 1.955
batch start
#iterations: 25
currently lose_sum: 97.02552735805511
time_elpased: 1.926
batch start
#iterations: 26
currently lose_sum: 97.25382709503174
time_elpased: 1.929
batch start
#iterations: 27
currently lose_sum: 96.65315610170364
time_elpased: 1.925
batch start
#iterations: 28
currently lose_sum: 96.65014934539795
time_elpased: 1.961
batch start
#iterations: 29
currently lose_sum: 96.68779331445694
time_elpased: 1.933
batch start
#iterations: 30
currently lose_sum: 96.86952072381973
time_elpased: 1.926
batch start
#iterations: 31
currently lose_sum: 96.66316574811935
time_elpased: 1.932
batch start
#iterations: 32
currently lose_sum: 96.7902347445488
time_elpased: 1.991
batch start
#iterations: 33
currently lose_sum: 96.60344922542572
time_elpased: 1.958
batch start
#iterations: 34
currently lose_sum: 96.85404425859451
time_elpased: 1.937
batch start
#iterations: 35
currently lose_sum: 96.21744507551193
time_elpased: 1.945
batch start
#iterations: 36
currently lose_sum: 96.63069212436676
time_elpased: 1.934
batch start
#iterations: 37
currently lose_sum: 96.46181970834732
time_elpased: 1.965
batch start
#iterations: 38
currently lose_sum: 96.60513722896576
time_elpased: 1.964
batch start
#iterations: 39
currently lose_sum: 96.55354458093643
time_elpased: 1.956
start validation test
0.647628865979
0.644093227968
0.662653082227
0.653241351324
0.64760248865
62.097
batch start
#iterations: 40
currently lose_sum: 96.7819391489029
time_elpased: 1.976
batch start
#iterations: 41
currently lose_sum: 96.35620927810669
time_elpased: 1.953
batch start
#iterations: 42
currently lose_sum: 96.30817192792892
time_elpased: 1.946
batch start
#iterations: 43
currently lose_sum: 96.42182922363281
time_elpased: 1.953
batch start
#iterations: 44
currently lose_sum: 96.34149980545044
time_elpased: 1.961
batch start
#iterations: 45
currently lose_sum: 96.33329606056213
time_elpased: 1.944
batch start
#iterations: 46
currently lose_sum: 96.30374890565872
time_elpased: 1.947
batch start
#iterations: 47
currently lose_sum: 96.47915834188461
time_elpased: 1.936
batch start
#iterations: 48
currently lose_sum: 96.40804672241211
time_elpased: 1.927
batch start
#iterations: 49
currently lose_sum: 96.29723483324051
time_elpased: 1.957
batch start
#iterations: 50
currently lose_sum: 96.00435972213745
time_elpased: 1.94
batch start
#iterations: 51
currently lose_sum: 95.86201810836792
time_elpased: 1.924
batch start
#iterations: 52
currently lose_sum: 96.18373084068298
time_elpased: 1.943
batch start
#iterations: 53
currently lose_sum: 95.7835932970047
time_elpased: 1.931
batch start
#iterations: 54
currently lose_sum: 96.22818952798843
time_elpased: 1.916
batch start
#iterations: 55
currently lose_sum: 95.91881996393204
time_elpased: 1.89
batch start
#iterations: 56
currently lose_sum: 96.09245508909225
time_elpased: 1.929
batch start
#iterations: 57
currently lose_sum: 96.03776276111603
time_elpased: 1.922
batch start
#iterations: 58
currently lose_sum: 96.12140679359436
time_elpased: 1.963
batch start
#iterations: 59
currently lose_sum: 96.40064108371735
time_elpased: 1.924
start validation test
0.640412371134
0.640867509508
0.641658948235
0.641262984676
0.640410182576
61.880
batch start
#iterations: 60
currently lose_sum: 96.37652391195297
time_elpased: 2.03
batch start
#iterations: 61
currently lose_sum: 96.20934623479843
time_elpased: 1.939
batch start
#iterations: 62
currently lose_sum: 95.87730437517166
time_elpased: 1.969
batch start
#iterations: 63
currently lose_sum: 95.86672532558441
time_elpased: 1.939
batch start
#iterations: 64
currently lose_sum: 95.97491508722305
time_elpased: 1.925
batch start
#iterations: 65
currently lose_sum: 95.95734614133835
time_elpased: 1.936
batch start
#iterations: 66
currently lose_sum: 96.10872375965118
time_elpased: 1.956
batch start
#iterations: 67
currently lose_sum: 95.96701449155807
time_elpased: 1.942
batch start
#iterations: 68
currently lose_sum: 95.59480571746826
time_elpased: 1.945
batch start
#iterations: 69
currently lose_sum: 95.73725259304047
time_elpased: 1.945
batch start
#iterations: 70
currently lose_sum: 96.0131967663765
time_elpased: 1.951
batch start
#iterations: 71
currently lose_sum: 95.95064663887024
time_elpased: 1.941
batch start
#iterations: 72
currently lose_sum: 95.53652894496918
time_elpased: 1.917
batch start
#iterations: 73
currently lose_sum: 95.88093274831772
time_elpased: 1.933
batch start
#iterations: 74
currently lose_sum: 95.6321468949318
time_elpased: 1.979
batch start
#iterations: 75
currently lose_sum: 95.92794156074524
time_elpased: 1.927
batch start
#iterations: 76
currently lose_sum: 95.65158557891846
time_elpased: 1.956
batch start
#iterations: 77
currently lose_sum: 95.9304832816124
time_elpased: 1.953
batch start
#iterations: 78
currently lose_sum: 96.16547185182571
time_elpased: 1.943
batch start
#iterations: 79
currently lose_sum: 95.55975997447968
time_elpased: 1.942
start validation test
0.659278350515
0.640550076902
0.728619944427
0.681752527684
0.659156610652
61.392
batch start
#iterations: 80
currently lose_sum: 95.36426323652267
time_elpased: 1.931
batch start
#iterations: 81
currently lose_sum: 95.56277012825012
time_elpased: 1.9
batch start
#iterations: 82
currently lose_sum: 96.06889897584915
time_elpased: 1.977
batch start
#iterations: 83
currently lose_sum: 95.4247208237648
time_elpased: 1.936
batch start
#iterations: 84
currently lose_sum: 95.59822088479996
time_elpased: 1.942
batch start
#iterations: 85
currently lose_sum: 95.81892251968384
time_elpased: 2.006
batch start
#iterations: 86
currently lose_sum: 96.10067504644394
time_elpased: 1.978
batch start
#iterations: 87
currently lose_sum: 95.67349445819855
time_elpased: 1.911
batch start
#iterations: 88
currently lose_sum: 95.6838948726654
time_elpased: 1.965
batch start
#iterations: 89
currently lose_sum: 95.62872672080994
time_elpased: 1.924
batch start
#iterations: 90
currently lose_sum: 95.53637719154358
time_elpased: 1.932
batch start
#iterations: 91
currently lose_sum: 96.16250717639923
time_elpased: 1.913
batch start
#iterations: 92
currently lose_sum: 95.60573875904083
time_elpased: 1.945
batch start
#iterations: 93
currently lose_sum: 95.45577371120453
time_elpased: 1.917
batch start
#iterations: 94
currently lose_sum: 95.21567100286484
time_elpased: 1.924
batch start
#iterations: 95
currently lose_sum: 95.86164140701294
time_elpased: 1.928
batch start
#iterations: 96
currently lose_sum: 95.64444518089294
time_elpased: 1.961
batch start
#iterations: 97
currently lose_sum: 95.80244493484497
time_elpased: 1.924
batch start
#iterations: 98
currently lose_sum: 95.80772906541824
time_elpased: 1.966
batch start
#iterations: 99
currently lose_sum: 95.9540786743164
time_elpased: 1.946
start validation test
0.652216494845
0.640491958373
0.696717093753
0.667422487307
0.65213836718
61.422
batch start
#iterations: 100
currently lose_sum: 95.48043936491013
time_elpased: 2.02
batch start
#iterations: 101
currently lose_sum: 95.59007632732391
time_elpased: 1.945
batch start
#iterations: 102
currently lose_sum: 95.32771825790405
time_elpased: 2.009
batch start
#iterations: 103
currently lose_sum: 95.5493232011795
time_elpased: 1.943
batch start
#iterations: 104
currently lose_sum: 95.50949376821518
time_elpased: 1.932
batch start
#iterations: 105
currently lose_sum: 95.65489387512207
time_elpased: 1.899
batch start
#iterations: 106
currently lose_sum: 95.7779353260994
time_elpased: 1.928
batch start
#iterations: 107
currently lose_sum: 95.52826964855194
time_elpased: 1.942
batch start
#iterations: 108
currently lose_sum: 95.70206600427628
time_elpased: 1.943
batch start
#iterations: 109
currently lose_sum: 95.96419107913971
time_elpased: 1.964
batch start
#iterations: 110
currently lose_sum: 95.52367174625397
time_elpased: 1.936
batch start
#iterations: 111
currently lose_sum: 95.44582533836365
time_elpased: 1.937
batch start
#iterations: 112
currently lose_sum: 95.58505940437317
time_elpased: 1.962
batch start
#iterations: 113
currently lose_sum: 95.1554644703865
time_elpased: 1.969
batch start
#iterations: 114
currently lose_sum: 95.77662587165833
time_elpased: 1.933
batch start
#iterations: 115
currently lose_sum: 95.26633477210999
time_elpased: 1.929
batch start
#iterations: 116
currently lose_sum: 95.38279247283936
time_elpased: 1.961
batch start
#iterations: 117
currently lose_sum: 95.34380531311035
time_elpased: 1.949
batch start
#iterations: 118
currently lose_sum: 95.58626973628998
time_elpased: 2.01
batch start
#iterations: 119
currently lose_sum: 95.2730120420456
time_elpased: 1.963
start validation test
0.656494845361
0.644815482402
0.699495729135
0.671043538355
0.656419350677
61.165
batch start
#iterations: 120
currently lose_sum: 95.14163535833359
time_elpased: 2.012
batch start
#iterations: 121
currently lose_sum: 95.68780326843262
time_elpased: 1.944
batch start
#iterations: 122
currently lose_sum: 95.7384541630745
time_elpased: 1.939
batch start
#iterations: 123
currently lose_sum: 95.303680062294
time_elpased: 1.895
batch start
#iterations: 124
currently lose_sum: 95.42408555746078
time_elpased: 1.989
batch start
#iterations: 125
currently lose_sum: 95.20098495483398
time_elpased: 1.919
batch start
#iterations: 126
currently lose_sum: 95.07799971103668
time_elpased: 1.919
batch start
#iterations: 127
currently lose_sum: 95.47155523300171
time_elpased: 1.956
batch start
#iterations: 128
currently lose_sum: 95.18418997526169
time_elpased: 1.935
batch start
#iterations: 129
currently lose_sum: 95.12655138969421
time_elpased: 1.988
batch start
#iterations: 130
currently lose_sum: 95.35680478811264
time_elpased: 1.969
batch start
#iterations: 131
currently lose_sum: 95.4162608385086
time_elpased: 1.893
batch start
#iterations: 132
currently lose_sum: 95.56018948554993
time_elpased: 1.948
batch start
#iterations: 133
currently lose_sum: 95.64196157455444
time_elpased: 1.911
batch start
#iterations: 134
currently lose_sum: 95.5484117269516
time_elpased: 1.996
batch start
#iterations: 135
currently lose_sum: 95.47843009233475
time_elpased: 1.952
batch start
#iterations: 136
currently lose_sum: 95.17146408557892
time_elpased: 1.936
batch start
#iterations: 137
currently lose_sum: 95.50565600395203
time_elpased: 1.932
batch start
#iterations: 138
currently lose_sum: 95.30952072143555
time_elpased: 1.923
batch start
#iterations: 139
currently lose_sum: 95.63537567853928
time_elpased: 1.934
start validation test
0.652422680412
0.640283018868
0.698466604919
0.668110449377
0.652341843201
61.438
batch start
#iterations: 140
currently lose_sum: 95.4810203909874
time_elpased: 2.017
batch start
#iterations: 141
currently lose_sum: 95.33275294303894
time_elpased: 1.966
batch start
#iterations: 142
currently lose_sum: 95.58645886182785
time_elpased: 1.954
batch start
#iterations: 143
currently lose_sum: 95.3660044670105
time_elpased: 1.961
batch start
#iterations: 144
currently lose_sum: 95.2660664319992
time_elpased: 1.945
batch start
#iterations: 145
currently lose_sum: 95.49370986223221
time_elpased: 1.94
batch start
#iterations: 146
currently lose_sum: 95.07579064369202
time_elpased: 1.932
batch start
#iterations: 147
currently lose_sum: 95.48455864191055
time_elpased: 1.931
batch start
#iterations: 148
currently lose_sum: 95.5578585267067
time_elpased: 2.03
batch start
#iterations: 149
currently lose_sum: 95.3541647195816
time_elpased: 1.961
batch start
#iterations: 150
currently lose_sum: 95.14696544408798
time_elpased: 1.977
batch start
#iterations: 151
currently lose_sum: 95.60995107889175
time_elpased: 2.01
batch start
#iterations: 152
currently lose_sum: 94.9966197013855
time_elpased: 1.927
batch start
#iterations: 153
currently lose_sum: 95.44091212749481
time_elpased: 1.914
batch start
#iterations: 154
currently lose_sum: 95.00984954833984
time_elpased: 1.92
batch start
#iterations: 155
currently lose_sum: 95.22173601388931
time_elpased: 1.945
batch start
#iterations: 156
currently lose_sum: 95.72087270021439
time_elpased: 1.937
batch start
#iterations: 157
currently lose_sum: 95.51298505067825
time_elpased: 1.916
batch start
#iterations: 158
currently lose_sum: 95.49052917957306
time_elpased: 1.913
batch start
#iterations: 159
currently lose_sum: 95.43681317567825
time_elpased: 1.948
start validation test
0.655979381443
0.643578371237
0.70186271483
0.671458107709
0.655898826175
61.364
batch start
#iterations: 160
currently lose_sum: 95.26195973157883
time_elpased: 2.064
batch start
#iterations: 161
currently lose_sum: 95.33274275064468
time_elpased: 1.951
batch start
#iterations: 162
currently lose_sum: 95.3063285946846
time_elpased: 1.934
batch start
#iterations: 163
currently lose_sum: 95.33528804779053
time_elpased: 1.923
batch start
#iterations: 164
currently lose_sum: 95.73401343822479
time_elpased: 1.989
batch start
#iterations: 165
currently lose_sum: 95.18893575668335
time_elpased: 1.928
batch start
#iterations: 166
currently lose_sum: 95.13698899745941
time_elpased: 1.984
batch start
#iterations: 167
currently lose_sum: 95.25291168689728
time_elpased: 1.969
batch start
#iterations: 168
currently lose_sum: 95.56606411933899
time_elpased: 1.953
batch start
#iterations: 169
currently lose_sum: 94.99481356143951
time_elpased: 2.006
batch start
#iterations: 170
currently lose_sum: 94.84317582845688
time_elpased: 1.919
batch start
#iterations: 171
currently lose_sum: 94.95426297187805
time_elpased: 2.032
batch start
#iterations: 172
currently lose_sum: 95.1822681427002
time_elpased: 1.969
batch start
#iterations: 173
currently lose_sum: 95.27223294973373
time_elpased: 2.051
batch start
#iterations: 174
currently lose_sum: 95.43499237298965
time_elpased: 1.926
batch start
#iterations: 175
currently lose_sum: 95.36654984951019
time_elpased: 1.939
batch start
#iterations: 176
currently lose_sum: 95.4288878440857
time_elpased: 2.005
batch start
#iterations: 177
currently lose_sum: 95.25177109241486
time_elpased: 1.921
batch start
#iterations: 178
currently lose_sum: 95.23982977867126
time_elpased: 1.968
batch start
#iterations: 179
currently lose_sum: 95.26299566030502
time_elpased: 1.958
start validation test
0.657371134021
0.642737586015
0.71133065761
0.675296761272
0.657276399754
61.129
batch start
#iterations: 180
currently lose_sum: 95.2065099477768
time_elpased: 1.958
batch start
#iterations: 181
currently lose_sum: 95.41686844825745
time_elpased: 1.918
batch start
#iterations: 182
currently lose_sum: 94.74636137485504
time_elpased: 1.969
batch start
#iterations: 183
currently lose_sum: 94.9658350944519
time_elpased: 1.936
batch start
#iterations: 184
currently lose_sum: 95.1681376695633
time_elpased: 1.985
batch start
#iterations: 185
currently lose_sum: 95.17788243293762
time_elpased: 1.967
batch start
#iterations: 186
currently lose_sum: 95.07086408138275
time_elpased: 1.978
batch start
#iterations: 187
currently lose_sum: 95.3034610748291
time_elpased: 1.924
batch start
#iterations: 188
currently lose_sum: 95.08827525377274
time_elpased: 1.931
batch start
#iterations: 189
currently lose_sum: 94.86891162395477
time_elpased: 1.93
batch start
#iterations: 190
currently lose_sum: 95.15720629692078
time_elpased: 1.965
batch start
#iterations: 191
currently lose_sum: 95.30857408046722
time_elpased: 1.978
batch start
#iterations: 192
currently lose_sum: 95.40091729164124
time_elpased: 1.957
batch start
#iterations: 193
currently lose_sum: 95.11304491758347
time_elpased: 1.957
batch start
#iterations: 194
currently lose_sum: 94.83350312709808
time_elpased: 1.945
batch start
#iterations: 195
currently lose_sum: 94.92268860340118
time_elpased: 1.921
batch start
#iterations: 196
currently lose_sum: 94.86364513635635
time_elpased: 1.922
batch start
#iterations: 197
currently lose_sum: 95.17098999023438
time_elpased: 1.948
batch start
#iterations: 198
currently lose_sum: 95.27314502000809
time_elpased: 1.934
batch start
#iterations: 199
currently lose_sum: 95.20781654119492
time_elpased: 2.002
start validation test
0.655824742268
0.647946272143
0.68508799012
0.665999699865
0.655773366123
61.428
batch start
#iterations: 200
currently lose_sum: 95.1327133178711
time_elpased: 1.996
batch start
#iterations: 201
currently lose_sum: 95.0347820520401
time_elpased: 1.972
batch start
#iterations: 202
currently lose_sum: 94.8498712182045
time_elpased: 1.945
batch start
#iterations: 203
currently lose_sum: 95.12704753875732
time_elpased: 2.004
batch start
#iterations: 204
currently lose_sum: 94.87523281574249
time_elpased: 1.941
batch start
#iterations: 205
currently lose_sum: 95.08825188875198
time_elpased: 1.935
batch start
#iterations: 206
currently lose_sum: 95.1958019733429
time_elpased: 1.945
batch start
#iterations: 207
currently lose_sum: 95.06528782844543
time_elpased: 1.97
batch start
#iterations: 208
currently lose_sum: 94.95327389240265
time_elpased: 1.948
batch start
#iterations: 209
currently lose_sum: 94.78105628490448
time_elpased: 1.945
batch start
#iterations: 210
currently lose_sum: 94.78536880016327
time_elpased: 1.949
batch start
#iterations: 211
currently lose_sum: 94.66633009910583
time_elpased: 1.988
batch start
#iterations: 212
currently lose_sum: 94.75488138198853
time_elpased: 1.929
batch start
#iterations: 213
currently lose_sum: 94.9262912273407
time_elpased: 1.948
batch start
#iterations: 214
currently lose_sum: 95.24110275506973
time_elpased: 1.959
batch start
#iterations: 215
currently lose_sum: 95.03544056415558
time_elpased: 1.91
batch start
#iterations: 216
currently lose_sum: 94.77614492177963
time_elpased: 1.923
batch start
#iterations: 217
currently lose_sum: 94.99537897109985
time_elpased: 1.921
batch start
#iterations: 218
currently lose_sum: 95.0080498456955
time_elpased: 1.945
batch start
#iterations: 219
currently lose_sum: 94.97414809465408
time_elpased: 2.009
start validation test
0.657216494845
0.644193700047
0.705052999897
0.673250786164
0.657132510482
61.199
batch start
#iterations: 220
currently lose_sum: 94.95566236972809
time_elpased: 1.966
batch start
#iterations: 221
currently lose_sum: 95.42836964130402
time_elpased: 1.921
batch start
#iterations: 222
currently lose_sum: 95.07286036014557
time_elpased: 1.979
batch start
#iterations: 223
currently lose_sum: 95.36104744672775
time_elpased: 1.969
batch start
#iterations: 224
currently lose_sum: 95.10650157928467
time_elpased: 1.934
batch start
#iterations: 225
currently lose_sum: 94.83586025238037
time_elpased: 1.966
batch start
#iterations: 226
currently lose_sum: 94.75997769832611
time_elpased: 1.911
batch start
#iterations: 227
currently lose_sum: 94.96141946315765
time_elpased: 1.963
batch start
#iterations: 228
currently lose_sum: 95.19324719905853
time_elpased: 1.96
batch start
#iterations: 229
currently lose_sum: 95.11989492177963
time_elpased: 1.931
batch start
#iterations: 230
currently lose_sum: 94.99069601297379
time_elpased: 1.922
batch start
#iterations: 231
currently lose_sum: 94.92022097110748
time_elpased: 1.954
batch start
#iterations: 232
currently lose_sum: 95.1600970029831
time_elpased: 1.907
batch start
#iterations: 233
currently lose_sum: 94.72536510229111
time_elpased: 1.945
batch start
#iterations: 234
currently lose_sum: 95.19618731737137
time_elpased: 1.916
batch start
#iterations: 235
currently lose_sum: 95.02761143445969
time_elpased: 1.95
batch start
#iterations: 236
currently lose_sum: 95.05280071496964
time_elpased: 1.945
batch start
#iterations: 237
currently lose_sum: 95.02530270814896
time_elpased: 1.952
batch start
#iterations: 238
currently lose_sum: 95.00907635688782
time_elpased: 1.963
batch start
#iterations: 239
currently lose_sum: 95.04977852106094
time_elpased: 1.998
start validation test
0.648453608247
0.641856821075
0.674488010703
0.657767964673
0.648407900838
61.248
batch start
#iterations: 240
currently lose_sum: 95.0799845457077
time_elpased: 2.003
batch start
#iterations: 241
currently lose_sum: 95.05605465173721
time_elpased: 1.95
batch start
#iterations: 242
currently lose_sum: 95.07201033830643
time_elpased: 2.013
batch start
#iterations: 243
currently lose_sum: 94.99871343374252
time_elpased: 1.935
batch start
#iterations: 244
currently lose_sum: 95.36929965019226
time_elpased: 1.993
batch start
#iterations: 245
currently lose_sum: 95.13996827602386
time_elpased: 1.95
batch start
#iterations: 246
currently lose_sum: 95.17643314599991
time_elpased: 1.947
batch start
#iterations: 247
currently lose_sum: 94.83064925670624
time_elpased: 1.969
batch start
#iterations: 248
currently lose_sum: 95.21805745363235
time_elpased: 1.929
batch start
#iterations: 249
currently lose_sum: 95.02615308761597
time_elpased: 1.92
batch start
#iterations: 250
currently lose_sum: 94.92054706811905
time_elpased: 1.936
batch start
#iterations: 251
currently lose_sum: 94.81338858604431
time_elpased: 1.949
batch start
#iterations: 252
currently lose_sum: 95.12615412473679
time_elpased: 1.938
batch start
#iterations: 253
currently lose_sum: 94.76591145992279
time_elpased: 1.92
batch start
#iterations: 254
currently lose_sum: 94.59248208999634
time_elpased: 1.918
batch start
#iterations: 255
currently lose_sum: 95.42413055896759
time_elpased: 1.92
batch start
#iterations: 256
currently lose_sum: 94.92072427272797
time_elpased: 1.918
batch start
#iterations: 257
currently lose_sum: 95.2443236708641
time_elpased: 1.938
batch start
#iterations: 258
currently lose_sum: 94.75247895717621
time_elpased: 1.944
batch start
#iterations: 259
currently lose_sum: 94.67442238330841
time_elpased: 1.929
start validation test
0.658556701031
0.638339744163
0.734383040033
0.683001531394
0.658423576197
61.134
batch start
#iterations: 260
currently lose_sum: 95.54000675678253
time_elpased: 1.967
batch start
#iterations: 261
currently lose_sum: 94.34980005025864
time_elpased: 1.956
batch start
#iterations: 262
currently lose_sum: 94.96610301733017
time_elpased: 1.925
batch start
#iterations: 263
currently lose_sum: 94.92129844427109
time_elpased: 1.955
batch start
#iterations: 264
currently lose_sum: 95.37645518779755
time_elpased: 1.962
batch start
#iterations: 265
currently lose_sum: 94.92022722959518
time_elpased: 1.941
batch start
#iterations: 266
currently lose_sum: 95.08612632751465
time_elpased: 1.924
batch start
#iterations: 267
currently lose_sum: 94.98597115278244
time_elpased: 1.908
batch start
#iterations: 268
currently lose_sum: 95.16160249710083
time_elpased: 1.929
batch start
#iterations: 269
currently lose_sum: 95.0271703004837
time_elpased: 1.948
batch start
#iterations: 270
currently lose_sum: 95.00921076536179
time_elpased: 1.937
batch start
#iterations: 271
currently lose_sum: 94.58496570587158
time_elpased: 1.95
batch start
#iterations: 272
currently lose_sum: 95.45050531625748
time_elpased: 1.917
batch start
#iterations: 273
currently lose_sum: 94.97209030389786
time_elpased: 1.988
batch start
#iterations: 274
currently lose_sum: 95.41871070861816
time_elpased: 1.934
batch start
#iterations: 275
currently lose_sum: 94.91052901744843
time_elpased: 1.995
batch start
#iterations: 276
currently lose_sum: 94.95140254497528
time_elpased: 1.943
batch start
#iterations: 277
currently lose_sum: 95.07835972309113
time_elpased: 2.011
batch start
#iterations: 278
currently lose_sum: 95.31853985786438
time_elpased: 1.943
batch start
#iterations: 279
currently lose_sum: 94.63134235143661
time_elpased: 1.938
start validation test
0.652989690722
0.645425314236
0.68169188021
0.663063063063
0.652939299601
61.409
batch start
#iterations: 280
currently lose_sum: 95.04677361249924
time_elpased: 1.928
batch start
#iterations: 281
currently lose_sum: 94.78957611322403
time_elpased: 1.992
batch start
#iterations: 282
currently lose_sum: 94.74091267585754
time_elpased: 1.958
batch start
#iterations: 283
currently lose_sum: 94.9684082865715
time_elpased: 2.052
batch start
#iterations: 284
currently lose_sum: 94.82608968019485
time_elpased: 1.932
batch start
#iterations: 285
currently lose_sum: 94.90967810153961
time_elpased: 1.979
batch start
#iterations: 286
currently lose_sum: 95.03289425373077
time_elpased: 1.94
batch start
#iterations: 287
currently lose_sum: 94.54417008161545
time_elpased: 1.976
batch start
#iterations: 288
currently lose_sum: 94.64541083574295
time_elpased: 1.947
batch start
#iterations: 289
currently lose_sum: 94.7949880361557
time_elpased: 2.032
batch start
#iterations: 290
currently lose_sum: 94.96408253908157
time_elpased: 1.946
batch start
#iterations: 291
currently lose_sum: 94.67789840698242
time_elpased: 2.001
batch start
#iterations: 292
currently lose_sum: 94.6697615981102
time_elpased: 1.933
batch start
#iterations: 293
currently lose_sum: 94.84491765499115
time_elpased: 1.992
batch start
#iterations: 294
currently lose_sum: 94.40689587593079
time_elpased: 1.88
batch start
#iterations: 295
currently lose_sum: 94.86995929479599
time_elpased: 1.97
batch start
#iterations: 296
currently lose_sum: 94.55473858118057
time_elpased: 1.918
batch start
#iterations: 297
currently lose_sum: 95.23344850540161
time_elpased: 1.912
batch start
#iterations: 298
currently lose_sum: 94.48926520347595
time_elpased: 1.925
batch start
#iterations: 299
currently lose_sum: 94.8841000199318
time_elpased: 1.941
start validation test
0.660567010309
0.641693811075
0.729854893486
0.682940921566
0.660445364743
61.153
batch start
#iterations: 300
currently lose_sum: 94.76357513666153
time_elpased: 1.958
batch start
#iterations: 301
currently lose_sum: 94.70151007175446
time_elpased: 1.968
batch start
#iterations: 302
currently lose_sum: 94.87429684400558
time_elpased: 1.989
batch start
#iterations: 303
currently lose_sum: 94.5654307603836
time_elpased: 1.971
batch start
#iterations: 304
currently lose_sum: 94.92631667852402
time_elpased: 1.962
batch start
#iterations: 305
currently lose_sum: 94.64942556619644
time_elpased: 1.94
batch start
#iterations: 306
currently lose_sum: 94.75683706998825
time_elpased: 1.963
batch start
#iterations: 307
currently lose_sum: 94.40526294708252
time_elpased: 2.1
batch start
#iterations: 308
currently lose_sum: 94.70999401807785
time_elpased: 1.966
batch start
#iterations: 309
currently lose_sum: 94.70851528644562
time_elpased: 1.973
batch start
#iterations: 310
currently lose_sum: 95.25569266080856
time_elpased: 1.922
batch start
#iterations: 311
currently lose_sum: 94.61019206047058
time_elpased: 1.999
batch start
#iterations: 312
currently lose_sum: 94.66560125350952
time_elpased: 1.987
batch start
#iterations: 313
currently lose_sum: 94.324358522892
time_elpased: 1.967
batch start
#iterations: 314
currently lose_sum: 95.18137508630753
time_elpased: 1.957
batch start
#iterations: 315
currently lose_sum: 94.6269668340683
time_elpased: 1.947
batch start
#iterations: 316
currently lose_sum: 95.1683618426323
time_elpased: 1.964
batch start
#iterations: 317
currently lose_sum: 94.94392532110214
time_elpased: 1.943
batch start
#iterations: 318
currently lose_sum: 94.5633556842804
time_elpased: 1.978
batch start
#iterations: 319
currently lose_sum: 94.7592636346817
time_elpased: 1.936
start validation test
0.651134020619
0.636997119762
0.705567562005
0.66953125
0.651038454141
61.230
batch start
#iterations: 320
currently lose_sum: 94.5361550450325
time_elpased: 1.978
batch start
#iterations: 321
currently lose_sum: 94.87814790010452
time_elpased: 1.987
batch start
#iterations: 322
currently lose_sum: 95.2399109005928
time_elpased: 1.94
batch start
#iterations: 323
currently lose_sum: 95.10010069608688
time_elpased: 2.001
batch start
#iterations: 324
currently lose_sum: 95.01245504617691
time_elpased: 1.954
batch start
#iterations: 325
currently lose_sum: 94.68499946594238
time_elpased: 1.93
batch start
#iterations: 326
currently lose_sum: 94.6926811337471
time_elpased: 1.935
batch start
#iterations: 327
currently lose_sum: 94.64778870344162
time_elpased: 1.942
batch start
#iterations: 328
currently lose_sum: 94.67925524711609
time_elpased: 1.97
batch start
#iterations: 329
currently lose_sum: 94.87440574169159
time_elpased: 1.947
batch start
#iterations: 330
currently lose_sum: 94.29576194286346
time_elpased: 2.017
batch start
#iterations: 331
currently lose_sum: 94.71884816884995
time_elpased: 1.911
batch start
#iterations: 332
currently lose_sum: 94.76984566450119
time_elpased: 1.974
batch start
#iterations: 333
currently lose_sum: 94.71784293651581
time_elpased: 1.94
batch start
#iterations: 334
currently lose_sum: 95.57655292749405
time_elpased: 1.916
batch start
#iterations: 335
currently lose_sum: 94.46452528238297
time_elpased: 1.924
batch start
#iterations: 336
currently lose_sum: 94.74127048254013
time_elpased: 1.929
batch start
#iterations: 337
currently lose_sum: 94.45674192905426
time_elpased: 1.992
batch start
#iterations: 338
currently lose_sum: 94.86818432807922
time_elpased: 1.957
batch start
#iterations: 339
currently lose_sum: 94.9287731051445
time_elpased: 1.977
start validation test
0.657989690722
0.637246170289
0.736338376042
0.683217951778
0.65785213752
61.191
batch start
#iterations: 340
currently lose_sum: 94.68045598268509
time_elpased: 1.977
batch start
#iterations: 341
currently lose_sum: 95.1535974740982
time_elpased: 2.001
batch start
#iterations: 342
currently lose_sum: 94.41915839910507
time_elpased: 1.97
batch start
#iterations: 343
currently lose_sum: 94.55691426992416
time_elpased: 1.983
batch start
#iterations: 344
currently lose_sum: 94.8182402253151
time_elpased: 1.967
batch start
#iterations: 345
currently lose_sum: 94.68600022792816
time_elpased: 1.977
batch start
#iterations: 346
currently lose_sum: 94.98693418502808
time_elpased: 1.941
batch start
#iterations: 347
currently lose_sum: 94.47300225496292
time_elpased: 1.92
batch start
#iterations: 348
currently lose_sum: 94.96968549489975
time_elpased: 1.942
batch start
#iterations: 349
currently lose_sum: 94.54541355371475
time_elpased: 1.922
batch start
#iterations: 350
currently lose_sum: 94.88095217943192
time_elpased: 1.964
batch start
#iterations: 351
currently lose_sum: 94.57851076126099
time_elpased: 1.934
batch start
#iterations: 352
currently lose_sum: 94.91546905040741
time_elpased: 1.937
batch start
#iterations: 353
currently lose_sum: 94.92217218875885
time_elpased: 1.915
batch start
#iterations: 354
currently lose_sum: 94.8440882563591
time_elpased: 1.955
batch start
#iterations: 355
currently lose_sum: 94.50466930866241
time_elpased: 1.947
batch start
#iterations: 356
currently lose_sum: 94.58631473779678
time_elpased: 1.948
batch start
#iterations: 357
currently lose_sum: 94.77896338701248
time_elpased: 1.935
batch start
#iterations: 358
currently lose_sum: 94.49667006731033
time_elpased: 1.938
batch start
#iterations: 359
currently lose_sum: 95.04990541934967
time_elpased: 1.989
start validation test
0.653556701031
0.635811423391
0.721724812185
0.676049549332
0.653437021398
61.182
batch start
#iterations: 360
currently lose_sum: 94.89272052049637
time_elpased: 2.051
batch start
#iterations: 361
currently lose_sum: 95.02795457839966
time_elpased: 1.991
batch start
#iterations: 362
currently lose_sum: 94.62374246120453
time_elpased: 1.954
batch start
#iterations: 363
currently lose_sum: 94.80773401260376
time_elpased: 1.985
batch start
#iterations: 364
currently lose_sum: 94.65781956911087
time_elpased: 1.902
batch start
#iterations: 365
currently lose_sum: 95.12736970186234
time_elpased: 2.007
batch start
#iterations: 366
currently lose_sum: 94.70852369070053
time_elpased: 1.932
batch start
#iterations: 367
currently lose_sum: 94.76910930871964
time_elpased: 1.976
batch start
#iterations: 368
currently lose_sum: 94.80660563707352
time_elpased: 1.989
batch start
#iterations: 369
currently lose_sum: 95.02457624673843
time_elpased: 1.981
batch start
#iterations: 370
currently lose_sum: 94.57964324951172
time_elpased: 2.002
batch start
#iterations: 371
currently lose_sum: 94.82697820663452
time_elpased: 1.916
batch start
#iterations: 372
currently lose_sum: 94.99221360683441
time_elpased: 1.959
batch start
#iterations: 373
currently lose_sum: 94.69886404275894
time_elpased: 1.921
batch start
#iterations: 374
currently lose_sum: 94.69087713956833
time_elpased: 1.974
batch start
#iterations: 375
currently lose_sum: 94.94381231069565
time_elpased: 1.94
batch start
#iterations: 376
currently lose_sum: 95.18608009815216
time_elpased: 1.944
batch start
#iterations: 377
currently lose_sum: 94.85687011480331
time_elpased: 2.001
batch start
#iterations: 378
currently lose_sum: 94.56211823225021
time_elpased: 1.956
batch start
#iterations: 379
currently lose_sum: 94.73560416698456
time_elpased: 1.953
start validation test
0.658608247423
0.636131643787
0.743953895235
0.685830842939
0.658458409974
61.136
batch start
#iterations: 380
currently lose_sum: 94.72243392467499
time_elpased: 1.986
batch start
#iterations: 381
currently lose_sum: 94.98804813623428
time_elpased: 1.945
batch start
#iterations: 382
currently lose_sum: 94.69040960073471
time_elpased: 1.989
batch start
#iterations: 383
currently lose_sum: 94.67719447612762
time_elpased: 2.028
batch start
#iterations: 384
currently lose_sum: 94.73148667812347
time_elpased: 1.966
batch start
#iterations: 385
currently lose_sum: 95.17422097921371
time_elpased: 1.988
batch start
#iterations: 386
currently lose_sum: 94.6285554766655
time_elpased: 1.961
batch start
#iterations: 387
currently lose_sum: 94.78756308555603
time_elpased: 1.94
batch start
#iterations: 388
currently lose_sum: 95.17836678028107
time_elpased: 2.027
batch start
#iterations: 389
currently lose_sum: 94.98991000652313
time_elpased: 1.94
batch start
#iterations: 390
currently lose_sum: 95.1464530825615
time_elpased: 1.92
batch start
#iterations: 391
currently lose_sum: 94.66130757331848
time_elpased: 1.975
batch start
#iterations: 392
currently lose_sum: 94.59352737665176
time_elpased: 1.974
batch start
#iterations: 393
currently lose_sum: 94.34030568599701
time_elpased: 2.008
batch start
#iterations: 394
currently lose_sum: 94.89998996257782
time_elpased: 1.975
batch start
#iterations: 395
currently lose_sum: 95.04806709289551
time_elpased: 1.948
batch start
#iterations: 396
currently lose_sum: 94.36108064651489
time_elpased: 1.968
batch start
#iterations: 397
currently lose_sum: 95.10807973146439
time_elpased: 1.98
batch start
#iterations: 398
currently lose_sum: 94.85603952407837
time_elpased: 1.971
batch start
#iterations: 399
currently lose_sum: 94.41554170846939
time_elpased: 1.999
start validation test
0.660824742268
0.637672254893
0.74765874241
0.688299384178
0.660672291788
61.116
acc: 0.659
pre: 0.636
rec: 0.746
F1: 0.687
auc: 0.659
