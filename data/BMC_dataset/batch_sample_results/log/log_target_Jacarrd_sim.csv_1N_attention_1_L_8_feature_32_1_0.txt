start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.48215341567993
time_elpased: 1.868
batch start
#iterations: 1
currently lose_sum: 99.99351918697357
time_elpased: 1.764
batch start
#iterations: 2
currently lose_sum: 99.84497308731079
time_elpased: 1.788
batch start
#iterations: 3
currently lose_sum: 99.52774614095688
time_elpased: 1.85
batch start
#iterations: 4
currently lose_sum: 99.44601482152939
time_elpased: 1.802
batch start
#iterations: 5
currently lose_sum: 99.11478209495544
time_elpased: 1.794
batch start
#iterations: 6
currently lose_sum: 98.9479997754097
time_elpased: 1.831
batch start
#iterations: 7
currently lose_sum: 98.58243471384048
time_elpased: 1.837
batch start
#iterations: 8
currently lose_sum: 98.58054023981094
time_elpased: 1.828
batch start
#iterations: 9
currently lose_sum: 98.3632248044014
time_elpased: 1.834
batch start
#iterations: 10
currently lose_sum: 98.34519535303116
time_elpased: 1.84
batch start
#iterations: 11
currently lose_sum: 97.94541186094284
time_elpased: 1.757
batch start
#iterations: 12
currently lose_sum: 98.18546974658966
time_elpased: 1.758
batch start
#iterations: 13
currently lose_sum: 97.92056572437286
time_elpased: 1.749
batch start
#iterations: 14
currently lose_sum: 97.92264395952225
time_elpased: 1.773
batch start
#iterations: 15
currently lose_sum: 97.64073354005814
time_elpased: 1.752
batch start
#iterations: 16
currently lose_sum: 97.80301505327225
time_elpased: 1.715
batch start
#iterations: 17
currently lose_sum: 97.67533499002457
time_elpased: 1.746
batch start
#iterations: 18
currently lose_sum: 97.40356159210205
time_elpased: 1.742
batch start
#iterations: 19
currently lose_sum: 97.53399097919464
time_elpased: 1.762
start validation test
0.642731958763
0.643726784977
0.642070597921
0.642897624813
0.642733119884
63.016
batch start
#iterations: 20
currently lose_sum: 97.43305617570877
time_elpased: 1.74
batch start
#iterations: 21
currently lose_sum: 97.08976918458939
time_elpased: 1.741
batch start
#iterations: 22
currently lose_sum: 97.24784725904465
time_elpased: 1.779
batch start
#iterations: 23
currently lose_sum: 97.07711791992188
time_elpased: 1.783
batch start
#iterations: 24
currently lose_sum: 96.99724566936493
time_elpased: 1.726
batch start
#iterations: 25
currently lose_sum: 96.74327111244202
time_elpased: 1.767
batch start
#iterations: 26
currently lose_sum: 96.8058630824089
time_elpased: 1.779
batch start
#iterations: 27
currently lose_sum: 96.75604492425919
time_elpased: 1.767
batch start
#iterations: 28
currently lose_sum: 96.52073937654495
time_elpased: 1.774
batch start
#iterations: 29
currently lose_sum: 96.8741460442543
time_elpased: 1.747
batch start
#iterations: 30
currently lose_sum: 96.65413159132004
time_elpased: 1.73
batch start
#iterations: 31
currently lose_sum: 96.58673310279846
time_elpased: 1.774
batch start
#iterations: 32
currently lose_sum: 96.0912549495697
time_elpased: 1.737
batch start
#iterations: 33
currently lose_sum: 96.60423994064331
time_elpased: 1.733
batch start
#iterations: 34
currently lose_sum: 96.66175156831741
time_elpased: 1.76
batch start
#iterations: 35
currently lose_sum: 96.5528131723404
time_elpased: 1.722
batch start
#iterations: 36
currently lose_sum: 96.7136042714119
time_elpased: 1.765
batch start
#iterations: 37
currently lose_sum: 96.71275645494461
time_elpased: 1.744
batch start
#iterations: 38
currently lose_sum: 96.27550500631332
time_elpased: 1.731
batch start
#iterations: 39
currently lose_sum: 96.41042119264603
time_elpased: 1.736
start validation test
0.652783505155
0.63794539565
0.709375321601
0.671766884319
0.652684149492
62.089
batch start
#iterations: 40
currently lose_sum: 96.45119827985764
time_elpased: 1.827
batch start
#iterations: 41
currently lose_sum: 96.24996745586395
time_elpased: 1.746
batch start
#iterations: 42
currently lose_sum: 96.16287952661514
time_elpased: 1.733
batch start
#iterations: 43
currently lose_sum: 96.25957727432251
time_elpased: 1.746
batch start
#iterations: 44
currently lose_sum: 96.41423976421356
time_elpased: 1.731
batch start
#iterations: 45
currently lose_sum: 96.34781116247177
time_elpased: 1.75
batch start
#iterations: 46
currently lose_sum: 96.18303453922272
time_elpased: 1.721
batch start
#iterations: 47
currently lose_sum: 95.8956019282341
time_elpased: 1.744
batch start
#iterations: 48
currently lose_sum: 96.12097507715225
time_elpased: 1.724
batch start
#iterations: 49
currently lose_sum: 96.49364668130875
time_elpased: 1.741
batch start
#iterations: 50
currently lose_sum: 96.47190308570862
time_elpased: 1.746
batch start
#iterations: 51
currently lose_sum: 95.94976383447647
time_elpased: 1.83
batch start
#iterations: 52
currently lose_sum: 95.75245952606201
time_elpased: 1.779
batch start
#iterations: 53
currently lose_sum: 96.07273459434509
time_elpased: 1.737
batch start
#iterations: 54
currently lose_sum: 96.46858221292496
time_elpased: 1.761
batch start
#iterations: 55
currently lose_sum: 95.97277736663818
time_elpased: 1.718
batch start
#iterations: 56
currently lose_sum: 96.37037539482117
time_elpased: 1.725
batch start
#iterations: 57
currently lose_sum: 95.70596635341644
time_elpased: 1.733
batch start
#iterations: 58
currently lose_sum: 96.18124270439148
time_elpased: 1.767
batch start
#iterations: 59
currently lose_sum: 95.88925755023956
time_elpased: 1.764
start validation test
0.650773195876
0.632022976126
0.724709272409
0.675200153411
0.65064338969
61.922
batch start
#iterations: 60
currently lose_sum: 96.33281028270721
time_elpased: 1.826
batch start
#iterations: 61
currently lose_sum: 96.07926988601685
time_elpased: 1.742
batch start
#iterations: 62
currently lose_sum: 95.97481089830399
time_elpased: 1.725
batch start
#iterations: 63
currently lose_sum: 96.12675243616104
time_elpased: 1.733
batch start
#iterations: 64
currently lose_sum: 96.41714191436768
time_elpased: 1.753
batch start
#iterations: 65
currently lose_sum: 95.7545502781868
time_elpased: 1.747
batch start
#iterations: 66
currently lose_sum: 95.4205556511879
time_elpased: 1.77
batch start
#iterations: 67
currently lose_sum: 95.77366369962692
time_elpased: 1.732
batch start
#iterations: 68
currently lose_sum: 95.95996290445328
time_elpased: 1.718
batch start
#iterations: 69
currently lose_sum: 96.1823890209198
time_elpased: 1.75
batch start
#iterations: 70
currently lose_sum: 95.63774555921555
time_elpased: 1.737
batch start
#iterations: 71
currently lose_sum: 95.62622606754303
time_elpased: 1.799
batch start
#iterations: 72
currently lose_sum: 95.51382946968079
time_elpased: 1.809
batch start
#iterations: 73
currently lose_sum: 95.34337252378464
time_elpased: 1.774
batch start
#iterations: 74
currently lose_sum: 95.5629056096077
time_elpased: 1.725
batch start
#iterations: 75
currently lose_sum: 95.34952521324158
time_elpased: 1.725
batch start
#iterations: 76
currently lose_sum: 95.69407719373703
time_elpased: 1.76
batch start
#iterations: 77
currently lose_sum: 95.70993095636368
time_elpased: 1.727
batch start
#iterations: 78
currently lose_sum: 95.42297106981277
time_elpased: 1.727
batch start
#iterations: 79
currently lose_sum: 95.80625367164612
time_elpased: 1.746
start validation test
0.649432989691
0.633516483516
0.71194813214
0.670446285797
0.649323234716
61.745
batch start
#iterations: 80
currently lose_sum: 95.42160165309906
time_elpased: 1.769
batch start
#iterations: 81
currently lose_sum: 96.05309009552002
time_elpased: 1.718
batch start
#iterations: 82
currently lose_sum: 95.45834368467331
time_elpased: 1.729
batch start
#iterations: 83
currently lose_sum: 95.89718729257584
time_elpased: 1.708
batch start
#iterations: 84
currently lose_sum: 95.68032950162888
time_elpased: 1.694
batch start
#iterations: 85
currently lose_sum: 95.94808745384216
time_elpased: 1.735
batch start
#iterations: 86
currently lose_sum: 95.68325680494308
time_elpased: 1.763
batch start
#iterations: 87
currently lose_sum: 95.2869610786438
time_elpased: 1.719
batch start
#iterations: 88
currently lose_sum: 95.60840743780136
time_elpased: 1.757
batch start
#iterations: 89
currently lose_sum: 95.73054790496826
time_elpased: 1.699
batch start
#iterations: 90
currently lose_sum: 95.83488202095032
time_elpased: 1.712
batch start
#iterations: 91
currently lose_sum: 95.57380270957947
time_elpased: 1.697
batch start
#iterations: 92
currently lose_sum: 95.44286924600601
time_elpased: 1.785
batch start
#iterations: 93
currently lose_sum: 95.38238489627838
time_elpased: 1.729
batch start
#iterations: 94
currently lose_sum: 95.74184066057205
time_elpased: 1.686
batch start
#iterations: 95
currently lose_sum: 95.54014068841934
time_elpased: 1.71
batch start
#iterations: 96
currently lose_sum: 95.6193545460701
time_elpased: 1.729
batch start
#iterations: 97
currently lose_sum: 95.52575600147247
time_elpased: 1.72
batch start
#iterations: 98
currently lose_sum: 95.35570245981216
time_elpased: 1.701
batch start
#iterations: 99
currently lose_sum: 95.70496708154678
time_elpased: 1.74
start validation test
0.648298969072
0.64026754556
0.679736544201
0.659411970249
0.64824377556
61.799
batch start
#iterations: 100
currently lose_sum: 95.24022215604782
time_elpased: 1.761
batch start
#iterations: 101
currently lose_sum: 95.7760859131813
time_elpased: 1.771
batch start
#iterations: 102
currently lose_sum: 95.32624715566635
time_elpased: 1.71
batch start
#iterations: 103
currently lose_sum: 95.4508525133133
time_elpased: 1.765
batch start
#iterations: 104
currently lose_sum: 95.2460395693779
time_elpased: 1.713
batch start
#iterations: 105
currently lose_sum: 95.1250376701355
time_elpased: 1.805
batch start
#iterations: 106
currently lose_sum: 95.7968248128891
time_elpased: 1.707
batch start
#iterations: 107
currently lose_sum: 95.37225258350372
time_elpased: 1.715
batch start
#iterations: 108
currently lose_sum: 95.3146014213562
time_elpased: 1.71
batch start
#iterations: 109
currently lose_sum: 95.27347445487976
time_elpased: 1.739
batch start
#iterations: 110
currently lose_sum: 95.44771879911423
time_elpased: 1.738
batch start
#iterations: 111
currently lose_sum: 95.12066859006882
time_elpased: 1.726
batch start
#iterations: 112
currently lose_sum: 95.4620498418808
time_elpased: 1.741
batch start
#iterations: 113
currently lose_sum: 94.84792762994766
time_elpased: 1.752
batch start
#iterations: 114
currently lose_sum: 95.5586786866188
time_elpased: 1.712
batch start
#iterations: 115
currently lose_sum: 95.58099138736725
time_elpased: 1.718
batch start
#iterations: 116
currently lose_sum: 95.2817451953888
time_elpased: 1.751
batch start
#iterations: 117
currently lose_sum: 95.49911201000214
time_elpased: 1.781
batch start
#iterations: 118
currently lose_sum: 95.26651555299759
time_elpased: 1.793
batch start
#iterations: 119
currently lose_sum: 95.42481768131256
time_elpased: 1.714
start validation test
0.648556701031
0.647441765843
0.655037563034
0.651217515858
0.648545322878
61.775
batch start
#iterations: 120
currently lose_sum: 95.44526433944702
time_elpased: 1.73
batch start
#iterations: 121
currently lose_sum: 95.40929633378983
time_elpased: 1.716
batch start
#iterations: 122
currently lose_sum: 95.28863596916199
time_elpased: 1.828
batch start
#iterations: 123
currently lose_sum: 95.21409457921982
time_elpased: 1.696
batch start
#iterations: 124
currently lose_sum: 95.69143587350845
time_elpased: 1.673
batch start
#iterations: 125
currently lose_sum: 95.67736107110977
time_elpased: 1.761
batch start
#iterations: 126
currently lose_sum: 95.29012954235077
time_elpased: 1.801
batch start
#iterations: 127
currently lose_sum: 95.51516026258469
time_elpased: 1.709
batch start
#iterations: 128
currently lose_sum: 95.65104079246521
time_elpased: 1.685
batch start
#iterations: 129
currently lose_sum: 95.30908274650574
time_elpased: 1.794
batch start
#iterations: 130
currently lose_sum: 94.98976302146912
time_elpased: 1.695
batch start
#iterations: 131
currently lose_sum: 95.26665610074997
time_elpased: 1.685
batch start
#iterations: 132
currently lose_sum: 95.35099452733994
time_elpased: 1.724
batch start
#iterations: 133
currently lose_sum: 95.42829930782318
time_elpased: 1.771
batch start
#iterations: 134
currently lose_sum: 95.17732274532318
time_elpased: 1.706
batch start
#iterations: 135
currently lose_sum: 95.25756919384003
time_elpased: 1.752
batch start
#iterations: 136
currently lose_sum: 95.60053217411041
time_elpased: 1.7
batch start
#iterations: 137
currently lose_sum: 94.90922552347183
time_elpased: 1.761
batch start
#iterations: 138
currently lose_sum: 95.22524243593216
time_elpased: 1.756
batch start
#iterations: 139
currently lose_sum: 95.54595345258713
time_elpased: 1.745
start validation test
0.653917525773
0.63979145331
0.707214160749
0.671815426728
0.65382395531
61.639
batch start
#iterations: 140
currently lose_sum: 95.61566090583801
time_elpased: 1.787
batch start
#iterations: 141
currently lose_sum: 95.17331123352051
time_elpased: 1.852
batch start
#iterations: 142
currently lose_sum: 95.00258696079254
time_elpased: 1.817
batch start
#iterations: 143
currently lose_sum: 95.28808379173279
time_elpased: 1.723
batch start
#iterations: 144
currently lose_sum: 94.94794726371765
time_elpased: 1.761
batch start
#iterations: 145
currently lose_sum: 95.45684748888016
time_elpased: 1.817
batch start
#iterations: 146
currently lose_sum: 95.32811832427979
time_elpased: 1.803
batch start
#iterations: 147
currently lose_sum: 95.39954555034637
time_elpased: 1.781
batch start
#iterations: 148
currently lose_sum: 95.00369763374329
time_elpased: 1.793
batch start
#iterations: 149
currently lose_sum: 95.57851380109787
time_elpased: 1.845
batch start
#iterations: 150
currently lose_sum: 95.38818174600601
time_elpased: 1.857
batch start
#iterations: 151
currently lose_sum: 95.45480036735535
time_elpased: 1.8
batch start
#iterations: 152
currently lose_sum: 94.90371239185333
time_elpased: 1.765
batch start
#iterations: 153
currently lose_sum: 95.59767580032349
time_elpased: 1.745
batch start
#iterations: 154
currently lose_sum: 95.28157901763916
time_elpased: 1.799
batch start
#iterations: 155
currently lose_sum: 95.43285477161407
time_elpased: 1.829
batch start
#iterations: 156
currently lose_sum: 95.02807968854904
time_elpased: 1.755
batch start
#iterations: 157
currently lose_sum: 95.0619306564331
time_elpased: 1.794
batch start
#iterations: 158
currently lose_sum: 95.35458171367645
time_elpased: 1.76
batch start
#iterations: 159
currently lose_sum: 95.23530602455139
time_elpased: 1.797
start validation test
0.653041237113
0.637883265608
0.710816095503
0.672377707471
0.652939804438
61.731
batch start
#iterations: 160
currently lose_sum: 95.28710752725601
time_elpased: 1.726
batch start
#iterations: 161
currently lose_sum: 95.22890025377274
time_elpased: 1.71
batch start
#iterations: 162
currently lose_sum: 94.97699075937271
time_elpased: 1.731
batch start
#iterations: 163
currently lose_sum: 95.15599173307419
time_elpased: 1.802
batch start
#iterations: 164
currently lose_sum: 95.51045745611191
time_elpased: 1.789
batch start
#iterations: 165
currently lose_sum: 95.03116875886917
time_elpased: 1.738
batch start
#iterations: 166
currently lose_sum: 95.3792833685875
time_elpased: 1.729
batch start
#iterations: 167
currently lose_sum: 95.154993891716
time_elpased: 1.806
batch start
#iterations: 168
currently lose_sum: 95.11278307437897
time_elpased: 1.696
batch start
#iterations: 169
currently lose_sum: 95.06331688165665
time_elpased: 1.833
batch start
#iterations: 170
currently lose_sum: 95.28958857059479
time_elpased: 1.69
batch start
#iterations: 171
currently lose_sum: 94.89583998918533
time_elpased: 1.737
batch start
#iterations: 172
currently lose_sum: 94.68006557226181
time_elpased: 1.741
batch start
#iterations: 173
currently lose_sum: 95.20624625682831
time_elpased: 1.86
batch start
#iterations: 174
currently lose_sum: 94.81415790319443
time_elpased: 1.776
batch start
#iterations: 175
currently lose_sum: 95.22508424520493
time_elpased: 1.689
batch start
#iterations: 176
currently lose_sum: 95.32899922132492
time_elpased: 1.723
batch start
#iterations: 177
currently lose_sum: 95.450344145298
time_elpased: 1.746
batch start
#iterations: 178
currently lose_sum: 95.26373553276062
time_elpased: 1.726
batch start
#iterations: 179
currently lose_sum: 94.5458277463913
time_elpased: 1.753
start validation test
0.651855670103
0.641756769687
0.690233611197
0.665113050377
0.651788291708
61.700
batch start
#iterations: 180
currently lose_sum: 95.24146538972855
time_elpased: 1.715
batch start
#iterations: 181
currently lose_sum: 94.93149596452713
time_elpased: 1.744
batch start
#iterations: 182
currently lose_sum: 95.37909460067749
time_elpased: 1.711
batch start
#iterations: 183
currently lose_sum: 94.47772288322449
time_elpased: 1.722
batch start
#iterations: 184
currently lose_sum: 94.95402306318283
time_elpased: 1.705
batch start
#iterations: 185
currently lose_sum: 94.85957145690918
time_elpased: 1.725
batch start
#iterations: 186
currently lose_sum: 94.86208939552307
time_elpased: 1.712
batch start
#iterations: 187
currently lose_sum: 94.68980729579926
time_elpased: 1.745
batch start
#iterations: 188
currently lose_sum: 94.94882959127426
time_elpased: 1.705
batch start
#iterations: 189
currently lose_sum: 94.88493609428406
time_elpased: 1.697
batch start
#iterations: 190
currently lose_sum: 94.85006058216095
time_elpased: 1.694
batch start
#iterations: 191
currently lose_sum: 95.05631047487259
time_elpased: 1.716
batch start
#iterations: 192
currently lose_sum: 94.73486173152924
time_elpased: 1.751
batch start
#iterations: 193
currently lose_sum: 94.8589419722557
time_elpased: 1.73
batch start
#iterations: 194
currently lose_sum: 94.80001711845398
time_elpased: 1.744
batch start
#iterations: 195
currently lose_sum: 95.05979979038239
time_elpased: 1.699
batch start
#iterations: 196
currently lose_sum: 94.85731226205826
time_elpased: 1.706
batch start
#iterations: 197
currently lose_sum: 95.25044280290604
time_elpased: 1.78
batch start
#iterations: 198
currently lose_sum: 94.81368064880371
time_elpased: 1.703
batch start
#iterations: 199
currently lose_sum: 95.356296479702
time_elpased: 1.769
start validation test
0.652628865979
0.642324603326
0.691571472677
0.666038951385
0.652560496227
61.708
batch start
#iterations: 200
currently lose_sum: 95.29560321569443
time_elpased: 1.716
batch start
#iterations: 201
currently lose_sum: 94.81563621759415
time_elpased: 1.719
batch start
#iterations: 202
currently lose_sum: 95.12383830547333
time_elpased: 1.722
batch start
#iterations: 203
currently lose_sum: 95.2226248383522
time_elpased: 1.723
batch start
#iterations: 204
currently lose_sum: 94.91297429800034
time_elpased: 1.706
batch start
#iterations: 205
currently lose_sum: 94.74317181110382
time_elpased: 1.739
batch start
#iterations: 206
currently lose_sum: 94.92097520828247
time_elpased: 1.759
batch start
#iterations: 207
currently lose_sum: 94.57633745670319
time_elpased: 1.71
batch start
#iterations: 208
currently lose_sum: 95.22941476106644
time_elpased: 1.781
batch start
#iterations: 209
currently lose_sum: 95.01409864425659
time_elpased: 1.787
batch start
#iterations: 210
currently lose_sum: 94.77945977449417
time_elpased: 1.737
batch start
#iterations: 211
currently lose_sum: 94.94355118274689
time_elpased: 1.796
batch start
#iterations: 212
currently lose_sum: 95.03457409143448
time_elpased: 1.725
batch start
#iterations: 213
currently lose_sum: 94.5633152127266
time_elpased: 1.721
batch start
#iterations: 214
currently lose_sum: 94.79808658361435
time_elpased: 1.723
batch start
#iterations: 215
currently lose_sum: 94.79269969463348
time_elpased: 1.761
batch start
#iterations: 216
currently lose_sum: 95.29194861650467
time_elpased: 1.775
batch start
#iterations: 217
currently lose_sum: 94.86009359359741
time_elpased: 1.737
batch start
#iterations: 218
currently lose_sum: 95.06649643182755
time_elpased: 1.711
batch start
#iterations: 219
currently lose_sum: 95.17040878534317
time_elpased: 1.723
start validation test
0.656443298969
0.64352896915
0.704126788103
0.672465477419
0.656359583249
61.663
batch start
#iterations: 220
currently lose_sum: 95.14344894886017
time_elpased: 1.797
batch start
#iterations: 221
currently lose_sum: 95.03831559419632
time_elpased: 1.718
batch start
#iterations: 222
currently lose_sum: 94.8482369184494
time_elpased: 1.766
batch start
#iterations: 223
currently lose_sum: 94.99811547994614
time_elpased: 1.748
batch start
#iterations: 224
currently lose_sum: 95.09686744213104
time_elpased: 1.732
batch start
#iterations: 225
currently lose_sum: 95.06679064035416
time_elpased: 1.724
batch start
#iterations: 226
currently lose_sum: 95.06850755214691
time_elpased: 1.773
batch start
#iterations: 227
currently lose_sum: 94.90457612276077
time_elpased: 1.716
batch start
#iterations: 228
currently lose_sum: 94.82421684265137
time_elpased: 1.699
batch start
#iterations: 229
currently lose_sum: 94.93575316667557
time_elpased: 1.711
batch start
#iterations: 230
currently lose_sum: 94.81785434484482
time_elpased: 1.717
batch start
#iterations: 231
currently lose_sum: 94.36384439468384
time_elpased: 1.708
batch start
#iterations: 232
currently lose_sum: 94.64224183559418
time_elpased: 1.753
batch start
#iterations: 233
currently lose_sum: 95.09607070684433
time_elpased: 1.807
batch start
#iterations: 234
currently lose_sum: 95.27054572105408
time_elpased: 1.726
batch start
#iterations: 235
currently lose_sum: 94.80723416805267
time_elpased: 1.737
batch start
#iterations: 236
currently lose_sum: 95.05522441864014
time_elpased: 1.724
batch start
#iterations: 237
currently lose_sum: 94.81306517124176
time_elpased: 1.722
batch start
#iterations: 238
currently lose_sum: 95.03856003284454
time_elpased: 1.742
batch start
#iterations: 239
currently lose_sum: 94.79205232858658
time_elpased: 1.773
start validation test
0.653556701031
0.641882932374
0.697437480704
0.668508014797
0.653479661554
61.621
batch start
#iterations: 240
currently lose_sum: 94.75513458251953
time_elpased: 1.739
batch start
#iterations: 241
currently lose_sum: 95.04151564836502
time_elpased: 1.753
batch start
#iterations: 242
currently lose_sum: 94.71666651964188
time_elpased: 1.727
batch start
#iterations: 243
currently lose_sum: 94.5565612912178
time_elpased: 1.72
batch start
#iterations: 244
currently lose_sum: 95.12577521800995
time_elpased: 1.723
batch start
#iterations: 245
currently lose_sum: 95.302958548069
time_elpased: 1.735
batch start
#iterations: 246
currently lose_sum: 95.05981427431107
time_elpased: 1.747
batch start
#iterations: 247
currently lose_sum: 95.14824825525284
time_elpased: 1.704
batch start
#iterations: 248
currently lose_sum: 95.13409548997879
time_elpased: 1.736
batch start
#iterations: 249
currently lose_sum: 95.10121083259583
time_elpased: 1.743
batch start
#iterations: 250
currently lose_sum: 95.11794072389603
time_elpased: 1.707
batch start
#iterations: 251
currently lose_sum: 94.7269611954689
time_elpased: 1.738
batch start
#iterations: 252
currently lose_sum: 94.7512109875679
time_elpased: 1.748
batch start
#iterations: 253
currently lose_sum: 95.11998724937439
time_elpased: 1.783
batch start
#iterations: 254
currently lose_sum: 94.8961107134819
time_elpased: 1.787
batch start
#iterations: 255
currently lose_sum: 94.40946823358536
time_elpased: 1.733
batch start
#iterations: 256
currently lose_sum: 94.5221197605133
time_elpased: 1.728
batch start
#iterations: 257
currently lose_sum: 94.75622349977493
time_elpased: 1.734
batch start
#iterations: 258
currently lose_sum: 94.7933486700058
time_elpased: 1.727
batch start
#iterations: 259
currently lose_sum: 94.67398834228516
time_elpased: 1.743
start validation test
0.653969072165
0.638178472861
0.713903468149
0.673920435226
0.653863848089
61.597
batch start
#iterations: 260
currently lose_sum: 94.63156002759933
time_elpased: 1.816
batch start
#iterations: 261
currently lose_sum: 94.95870453119278
time_elpased: 1.763
batch start
#iterations: 262
currently lose_sum: 94.85557478666306
time_elpased: 1.732
batch start
#iterations: 263
currently lose_sum: 94.63465809822083
time_elpased: 1.737
batch start
#iterations: 264
currently lose_sum: 94.96943378448486
time_elpased: 1.696
batch start
#iterations: 265
currently lose_sum: 94.4246090054512
time_elpased: 1.698
batch start
#iterations: 266
currently lose_sum: 95.16002178192139
time_elpased: 1.72
batch start
#iterations: 267
currently lose_sum: 94.64807105064392
time_elpased: 1.718
batch start
#iterations: 268
currently lose_sum: 94.6003749370575
time_elpased: 1.726
batch start
#iterations: 269
currently lose_sum: 94.7684736251831
time_elpased: 1.72
batch start
#iterations: 270
currently lose_sum: 95.10848039388657
time_elpased: 1.717
batch start
#iterations: 271
currently lose_sum: 94.83270460367203
time_elpased: 1.755
batch start
#iterations: 272
currently lose_sum: 95.10389065742493
time_elpased: 1.737
batch start
#iterations: 273
currently lose_sum: 94.68650102615356
time_elpased: 1.761
batch start
#iterations: 274
currently lose_sum: 94.61188477277756
time_elpased: 1.725
batch start
#iterations: 275
currently lose_sum: 94.95334184169769
time_elpased: 1.764
batch start
#iterations: 276
currently lose_sum: 95.22573590278625
time_elpased: 1.894
batch start
#iterations: 277
currently lose_sum: 95.00221765041351
time_elpased: 1.74
batch start
#iterations: 278
currently lose_sum: 95.02639228105545
time_elpased: 1.736
batch start
#iterations: 279
currently lose_sum: 94.8964262008667
time_elpased: 1.739
start validation test
0.656855670103
0.639930492043
0.72007821344
0.677642729166
0.656744673177
61.396
batch start
#iterations: 280
currently lose_sum: 94.86938446760178
time_elpased: 1.734
batch start
#iterations: 281
currently lose_sum: 94.91422802209854
time_elpased: 1.756
batch start
#iterations: 282
currently lose_sum: 95.14803045988083
time_elpased: 1.739
batch start
#iterations: 283
currently lose_sum: 94.91961866617203
time_elpased: 1.776
batch start
#iterations: 284
currently lose_sum: 94.58900344371796
time_elpased: 1.719
batch start
#iterations: 285
currently lose_sum: 94.9529356956482
time_elpased: 1.756
batch start
#iterations: 286
currently lose_sum: 94.99588507413864
time_elpased: 1.705
batch start
#iterations: 287
currently lose_sum: 94.9478080868721
time_elpased: 1.784
batch start
#iterations: 288
currently lose_sum: 94.54730021953583
time_elpased: 1.708
batch start
#iterations: 289
currently lose_sum: 94.78934043645859
time_elpased: 1.731
batch start
#iterations: 290
currently lose_sum: 94.86360108852386
time_elpased: 1.738
batch start
#iterations: 291
currently lose_sum: 94.93776148557663
time_elpased: 1.792
batch start
#iterations: 292
currently lose_sum: 94.63776397705078
time_elpased: 1.732
batch start
#iterations: 293
currently lose_sum: 94.6679083108902
time_elpased: 1.719
batch start
#iterations: 294
currently lose_sum: 94.61556845903397
time_elpased: 1.733
batch start
#iterations: 295
currently lose_sum: 94.43359518051147
time_elpased: 1.798
batch start
#iterations: 296
currently lose_sum: 94.5973424911499
time_elpased: 1.803
batch start
#iterations: 297
currently lose_sum: 94.72233664989471
time_elpased: 1.743
batch start
#iterations: 298
currently lose_sum: 94.87368661165237
time_elpased: 1.728
batch start
#iterations: 299
currently lose_sum: 94.2377519607544
time_elpased: 1.794
start validation test
0.656855670103
0.641850547005
0.712462694247
0.675315807443
0.656758043395
61.500
batch start
#iterations: 300
currently lose_sum: 94.53215050697327
time_elpased: 1.733
batch start
#iterations: 301
currently lose_sum: 94.88942086696625
time_elpased: 1.726
batch start
#iterations: 302
currently lose_sum: 94.95434927940369
time_elpased: 1.753
batch start
#iterations: 303
currently lose_sum: 94.64967775344849
time_elpased: 1.715
batch start
#iterations: 304
currently lose_sum: 94.45533627271652
time_elpased: 1.719
batch start
#iterations: 305
currently lose_sum: 94.9198562502861
time_elpased: 1.721
batch start
#iterations: 306
currently lose_sum: 95.2045333981514
time_elpased: 1.729
batch start
#iterations: 307
currently lose_sum: 94.67558819055557
time_elpased: 1.706
batch start
#iterations: 308
currently lose_sum: 94.48830056190491
time_elpased: 1.72
batch start
#iterations: 309
currently lose_sum: 94.56204652786255
time_elpased: 1.788
batch start
#iterations: 310
currently lose_sum: 94.71462571620941
time_elpased: 1.707
batch start
#iterations: 311
currently lose_sum: 94.69095546007156
time_elpased: 1.748
batch start
#iterations: 312
currently lose_sum: 94.75365579128265
time_elpased: 1.757
batch start
#iterations: 313
currently lose_sum: 94.31434345245361
time_elpased: 1.823
batch start
#iterations: 314
currently lose_sum: 94.71807914972305
time_elpased: 1.763
batch start
#iterations: 315
currently lose_sum: 94.76606625318527
time_elpased: 1.704
batch start
#iterations: 316
currently lose_sum: 94.96504598855972
time_elpased: 1.724
batch start
#iterations: 317
currently lose_sum: 94.99353790283203
time_elpased: 1.791
batch start
#iterations: 318
currently lose_sum: 94.89414668083191
time_elpased: 1.714
batch start
#iterations: 319
currently lose_sum: 94.76268965005875
time_elpased: 1.728
start validation test
0.653556701031
0.643927747886
0.689719049089
0.666037267081
0.653493212451
61.646
batch start
#iterations: 320
currently lose_sum: 94.66677814722061
time_elpased: 1.767
batch start
#iterations: 321
currently lose_sum: 94.55797016620636
time_elpased: 1.781
batch start
#iterations: 322
currently lose_sum: 94.939748108387
time_elpased: 1.737
batch start
#iterations: 323
currently lose_sum: 94.58532398939133
time_elpased: 1.735
batch start
#iterations: 324
currently lose_sum: 95.30381351709366
time_elpased: 1.753
batch start
#iterations: 325
currently lose_sum: 95.08555287122726
time_elpased: 1.716
batch start
#iterations: 326
currently lose_sum: 94.63356226682663
time_elpased: 1.755
batch start
#iterations: 327
currently lose_sum: 94.56818252801895
time_elpased: 1.736
batch start
#iterations: 328
currently lose_sum: 94.38355851173401
time_elpased: 1.745
batch start
#iterations: 329
currently lose_sum: 94.71947860717773
time_elpased: 1.715
batch start
#iterations: 330
currently lose_sum: 94.6618001461029
time_elpased: 1.715
batch start
#iterations: 331
currently lose_sum: 94.70565044879913
time_elpased: 1.713
batch start
#iterations: 332
currently lose_sum: 94.36635488271713
time_elpased: 1.747
batch start
#iterations: 333
currently lose_sum: 94.60494858026505
time_elpased: 1.761
batch start
#iterations: 334
currently lose_sum: 94.74255102872849
time_elpased: 1.713
batch start
#iterations: 335
currently lose_sum: 94.91315644979477
time_elpased: 1.748
batch start
#iterations: 336
currently lose_sum: 94.49859619140625
time_elpased: 1.754
batch start
#iterations: 337
currently lose_sum: 95.25826579332352
time_elpased: 1.734
batch start
#iterations: 338
currently lose_sum: 94.5779139995575
time_elpased: 1.8
batch start
#iterations: 339
currently lose_sum: 94.42159527540207
time_elpased: 1.74
start validation test
0.65706185567
0.644120413923
0.704641350211
0.673023050081
0.656978322529
61.413
batch start
#iterations: 340
currently lose_sum: 94.59040331840515
time_elpased: 1.721
batch start
#iterations: 341
currently lose_sum: 94.60109949111938
time_elpased: 1.744
batch start
#iterations: 342
currently lose_sum: 94.69754737615585
time_elpased: 1.733
batch start
#iterations: 343
currently lose_sum: 94.83190357685089
time_elpased: 1.825
batch start
#iterations: 344
currently lose_sum: 94.45280224084854
time_elpased: 1.741
batch start
#iterations: 345
currently lose_sum: 94.76737403869629
time_elpased: 1.745
batch start
#iterations: 346
currently lose_sum: 94.6451404094696
time_elpased: 1.754
batch start
#iterations: 347
currently lose_sum: 94.92330759763718
time_elpased: 1.839
batch start
#iterations: 348
currently lose_sum: 94.78482872247696
time_elpased: 1.75
batch start
#iterations: 349
currently lose_sum: 94.79605269432068
time_elpased: 1.742
batch start
#iterations: 350
currently lose_sum: 94.73218059539795
time_elpased: 1.709
batch start
#iterations: 351
currently lose_sum: 94.64509224891663
time_elpased: 1.737
batch start
#iterations: 352
currently lose_sum: 94.63130259513855
time_elpased: 1.78
batch start
#iterations: 353
currently lose_sum: 94.80218362808228
time_elpased: 1.761
batch start
#iterations: 354
currently lose_sum: 94.88770490884781
time_elpased: 1.763
batch start
#iterations: 355
currently lose_sum: 94.95104020833969
time_elpased: 1.747
batch start
#iterations: 356
currently lose_sum: 94.80276942253113
time_elpased: 1.697
batch start
#iterations: 357
currently lose_sum: 94.66636556386948
time_elpased: 1.72
batch start
#iterations: 358
currently lose_sum: 94.85006695985794
time_elpased: 1.717
batch start
#iterations: 359
currently lose_sum: 94.6352516412735
time_elpased: 1.743
start validation test
0.654536082474
0.639518741323
0.711124832767
0.67342364292
0.654436732195
61.589
batch start
#iterations: 360
currently lose_sum: 94.88291954994202
time_elpased: 1.744
batch start
#iterations: 361
currently lose_sum: 94.74376374483109
time_elpased: 1.775
batch start
#iterations: 362
currently lose_sum: 94.61235761642456
time_elpased: 1.75
batch start
#iterations: 363
currently lose_sum: 94.85443949699402
time_elpased: 1.765
batch start
#iterations: 364
currently lose_sum: 95.06951594352722
time_elpased: 1.718
batch start
#iterations: 365
currently lose_sum: 94.92455089092255
time_elpased: 1.766
batch start
#iterations: 366
currently lose_sum: 94.54158353805542
time_elpased: 1.711
batch start
#iterations: 367
currently lose_sum: 94.99854844808578
time_elpased: 1.801
batch start
#iterations: 368
currently lose_sum: 94.88119572401047
time_elpased: 1.831
batch start
#iterations: 369
currently lose_sum: 94.94367492198944
time_elpased: 1.784
batch start
#iterations: 370
currently lose_sum: 94.42477136850357
time_elpased: 1.883
batch start
#iterations: 371
currently lose_sum: 94.5203155875206
time_elpased: 1.762
batch start
#iterations: 372
currently lose_sum: 94.90118235349655
time_elpased: 1.713
batch start
#iterations: 373
currently lose_sum: 94.62200820446014
time_elpased: 1.782
batch start
#iterations: 374
currently lose_sum: 94.4513349533081
time_elpased: 1.735
batch start
#iterations: 375
currently lose_sum: 94.82757842540741
time_elpased: 1.736
batch start
#iterations: 376
currently lose_sum: 94.52291357517242
time_elpased: 1.739
batch start
#iterations: 377
currently lose_sum: 94.40767347812653
time_elpased: 1.719
batch start
#iterations: 378
currently lose_sum: 94.70973283052444
time_elpased: 1.73
batch start
#iterations: 379
currently lose_sum: 94.88810294866562
time_elpased: 1.741
start validation test
0.654329896907
0.642417935862
0.698878254605
0.66945977918
0.654251685394
61.548
batch start
#iterations: 380
currently lose_sum: 94.37934678792953
time_elpased: 1.764
batch start
#iterations: 381
currently lose_sum: 94.6308017373085
time_elpased: 1.718
batch start
#iterations: 382
currently lose_sum: 94.62564235925674
time_elpased: 1.701
batch start
#iterations: 383
currently lose_sum: 94.39102691411972
time_elpased: 1.724
batch start
#iterations: 384
currently lose_sum: 94.93185317516327
time_elpased: 1.774
batch start
#iterations: 385
currently lose_sum: 94.5296561717987
time_elpased: 1.837
batch start
#iterations: 386
currently lose_sum: 94.51592445373535
time_elpased: 1.884
batch start
#iterations: 387
currently lose_sum: 94.51313745975494
time_elpased: 1.739
batch start
#iterations: 388
currently lose_sum: 94.57624757289886
time_elpased: 1.792
batch start
#iterations: 389
currently lose_sum: 95.27514737844467
time_elpased: 1.741
batch start
#iterations: 390
currently lose_sum: 94.89557147026062
time_elpased: 1.776
batch start
#iterations: 391
currently lose_sum: 95.02767145633698
time_elpased: 1.743
batch start
#iterations: 392
currently lose_sum: 94.56330919265747
time_elpased: 1.733
batch start
#iterations: 393
currently lose_sum: 94.78106582164764
time_elpased: 1.706
batch start
#iterations: 394
currently lose_sum: 94.31070500612259
time_elpased: 1.749
batch start
#iterations: 395
currently lose_sum: 94.19023728370667
time_elpased: 1.753
batch start
#iterations: 396
currently lose_sum: 94.54569047689438
time_elpased: 1.796
batch start
#iterations: 397
currently lose_sum: 94.4348972439766
time_elpased: 1.75
batch start
#iterations: 398
currently lose_sum: 94.37478387355804
time_elpased: 1.818
batch start
#iterations: 399
currently lose_sum: 94.45630919933319
time_elpased: 1.726
start validation test
0.652577319588
0.634329031676
0.723371410929
0.675930377921
0.652453029641
61.586
acc: 0.659
pre: 0.640
rec: 0.728
F1: 0.681
auc: 0.659
