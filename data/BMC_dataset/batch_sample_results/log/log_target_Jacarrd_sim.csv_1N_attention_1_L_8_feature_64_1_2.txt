start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.28804099559784
time_elpased: 2.292
batch start
#iterations: 1
currently lose_sum: 100.04534071683884
time_elpased: 2.281
batch start
#iterations: 2
currently lose_sum: 99.77999645471573
time_elpased: 2.258
batch start
#iterations: 3
currently lose_sum: 99.34907817840576
time_elpased: 2.27
batch start
#iterations: 4
currently lose_sum: 99.15396237373352
time_elpased: 2.26
batch start
#iterations: 5
currently lose_sum: 99.06470465660095
time_elpased: 2.175
batch start
#iterations: 6
currently lose_sum: 98.59639620780945
time_elpased: 2.202
batch start
#iterations: 7
currently lose_sum: 98.47928756475449
time_elpased: 2.279
batch start
#iterations: 8
currently lose_sum: 98.33467864990234
time_elpased: 2.314
batch start
#iterations: 9
currently lose_sum: 98.29944318532944
time_elpased: 2.285
batch start
#iterations: 10
currently lose_sum: 98.17296600341797
time_elpased: 2.293
batch start
#iterations: 11
currently lose_sum: 97.90213006734848
time_elpased: 2.279
batch start
#iterations: 12
currently lose_sum: 98.15403175354004
time_elpased: 2.273
batch start
#iterations: 13
currently lose_sum: 97.88398534059525
time_elpased: 2.237
batch start
#iterations: 14
currently lose_sum: 97.57220470905304
time_elpased: 2.163
batch start
#iterations: 15
currently lose_sum: 97.7722858786583
time_elpased: 2.277
batch start
#iterations: 16
currently lose_sum: 97.49267655611038
time_elpased: 2.228
batch start
#iterations: 17
currently lose_sum: 97.29247087240219
time_elpased: 2.213
batch start
#iterations: 18
currently lose_sum: 97.23616242408752
time_elpased: 2.309
batch start
#iterations: 19
currently lose_sum: 97.27800238132477
time_elpased: 2.26
start validation test
0.639536082474
0.631340405014
0.673767623752
0.651864389904
0.639475983724
62.876
batch start
#iterations: 20
currently lose_sum: 97.38675701618195
time_elpased: 2.255
batch start
#iterations: 21
currently lose_sum: 97.23148322105408
time_elpased: 2.271
batch start
#iterations: 22
currently lose_sum: 97.07922911643982
time_elpased: 2.285
batch start
#iterations: 23
currently lose_sum: 96.8845716714859
time_elpased: 2.28
batch start
#iterations: 24
currently lose_sum: 96.8599813580513
time_elpased: 2.269
batch start
#iterations: 25
currently lose_sum: 97.25547873973846
time_elpased: 2.334
batch start
#iterations: 26
currently lose_sum: 97.03064090013504
time_elpased: 2.347
batch start
#iterations: 27
currently lose_sum: 96.85643553733826
time_elpased: 2.323
batch start
#iterations: 28
currently lose_sum: 96.82580351829529
time_elpased: 2.37
batch start
#iterations: 29
currently lose_sum: 97.10084480047226
time_elpased: 2.342
batch start
#iterations: 30
currently lose_sum: 96.6857470870018
time_elpased: 2.374
batch start
#iterations: 31
currently lose_sum: 96.42230767011642
time_elpased: 2.402
batch start
#iterations: 32
currently lose_sum: 96.50746947526932
time_elpased: 2.398
batch start
#iterations: 33
currently lose_sum: 96.82702511548996
time_elpased: 2.415
batch start
#iterations: 34
currently lose_sum: 96.59702396392822
time_elpased: 2.411
batch start
#iterations: 35
currently lose_sum: 96.74712139368057
time_elpased: 2.436
batch start
#iterations: 36
currently lose_sum: 96.16279375553131
time_elpased: 2.362
batch start
#iterations: 37
currently lose_sum: 96.56746125221252
time_elpased: 2.336
batch start
#iterations: 38
currently lose_sum: 96.53161841630936
time_elpased: 2.379
batch start
#iterations: 39
currently lose_sum: 96.46912997961044
time_elpased: 2.375
start validation test
0.645721649485
0.640347414133
0.667695790882
0.653735704569
0.64568307049
62.286
batch start
#iterations: 40
currently lose_sum: 96.67502677440643
time_elpased: 2.326
batch start
#iterations: 41
currently lose_sum: 96.35970240831375
time_elpased: 2.379
batch start
#iterations: 42
currently lose_sum: 96.46109771728516
time_elpased: 2.377
batch start
#iterations: 43
currently lose_sum: 96.19310820102692
time_elpased: 2.423
batch start
#iterations: 44
currently lose_sum: 96.35807764530182
time_elpased: 2.367
batch start
#iterations: 45
currently lose_sum: 96.3125239610672
time_elpased: 2.398
batch start
#iterations: 46
currently lose_sum: 96.1592630147934
time_elpased: 2.403
batch start
#iterations: 47
currently lose_sum: 96.42189329862595
time_elpased: 2.42
batch start
#iterations: 48
currently lose_sum: 96.45222800970078
time_elpased: 2.415
batch start
#iterations: 49
currently lose_sum: 96.20643955469131
time_elpased: 2.425
batch start
#iterations: 50
currently lose_sum: 96.23782914876938
time_elpased: 2.22
batch start
#iterations: 51
currently lose_sum: 96.00681442022324
time_elpased: 2.344
batch start
#iterations: 52
currently lose_sum: 96.06433403491974
time_elpased: 2.321
batch start
#iterations: 53
currently lose_sum: 96.3084711432457
time_elpased: 2.324
batch start
#iterations: 54
currently lose_sum: 96.05677163600922
time_elpased: 2.334
batch start
#iterations: 55
currently lose_sum: 96.07173103094101
time_elpased: 2.388
batch start
#iterations: 56
currently lose_sum: 96.18990361690521
time_elpased: 2.381
batch start
#iterations: 57
currently lose_sum: 95.97657358646393
time_elpased: 2.401
batch start
#iterations: 58
currently lose_sum: 95.82942026853561
time_elpased: 2.384
batch start
#iterations: 59
currently lose_sum: 96.1578757762909
time_elpased: 2.382
start validation test
0.652371134021
0.637549736282
0.709066584337
0.67140908205
0.652271596413
61.855
batch start
#iterations: 60
currently lose_sum: 96.05652117729187
time_elpased: 2.383
batch start
#iterations: 61
currently lose_sum: 95.82496762275696
time_elpased: 2.375
batch start
#iterations: 62
currently lose_sum: 96.22514414787292
time_elpased: 2.417
batch start
#iterations: 63
currently lose_sum: 95.91072016954422
time_elpased: 2.31
batch start
#iterations: 64
currently lose_sum: 96.30645442008972
time_elpased: 2.331
batch start
#iterations: 65
currently lose_sum: 95.79458564519882
time_elpased: 2.408
batch start
#iterations: 66
currently lose_sum: 96.22880417108536
time_elpased: 2.274
batch start
#iterations: 67
currently lose_sum: 95.76416939496994
time_elpased: 2.393
batch start
#iterations: 68
currently lose_sum: 95.83277922868729
time_elpased: 2.399
batch start
#iterations: 69
currently lose_sum: 95.80472069978714
time_elpased: 2.399
batch start
#iterations: 70
currently lose_sum: 95.7161226272583
time_elpased: 2.376
batch start
#iterations: 71
currently lose_sum: 95.96952050924301
time_elpased: 2.391
batch start
#iterations: 72
currently lose_sum: 95.90842664241791
time_elpased: 2.394
batch start
#iterations: 73
currently lose_sum: 96.11508864164352
time_elpased: 2.413
batch start
#iterations: 74
currently lose_sum: 96.08863228559494
time_elpased: 2.403
batch start
#iterations: 75
currently lose_sum: 96.03520268201828
time_elpased: 2.398
batch start
#iterations: 76
currently lose_sum: 95.5139729976654
time_elpased: 2.385
batch start
#iterations: 77
currently lose_sum: 95.65905612707138
time_elpased: 2.315
batch start
#iterations: 78
currently lose_sum: 96.10987561941147
time_elpased: 2.391
batch start
#iterations: 79
currently lose_sum: 95.80409741401672
time_elpased: 2.367
start validation test
0.653608247423
0.640929182733
0.701348152722
0.669778869779
0.653524432656
61.657
batch start
#iterations: 80
currently lose_sum: 96.00738400220871
time_elpased: 2.316
batch start
#iterations: 81
currently lose_sum: 95.78811323642731
time_elpased: 2.397
batch start
#iterations: 82
currently lose_sum: 96.00820833444595
time_elpased: 2.404
batch start
#iterations: 83
currently lose_sum: 95.64423769712448
time_elpased: 2.374
batch start
#iterations: 84
currently lose_sum: 95.91122955083847
time_elpased: 2.378
batch start
#iterations: 85
currently lose_sum: 95.95479536056519
time_elpased: 2.395
batch start
#iterations: 86
currently lose_sum: 95.60836964845657
time_elpased: 2.376
batch start
#iterations: 87
currently lose_sum: 95.92102581262589
time_elpased: 2.401
batch start
#iterations: 88
currently lose_sum: 95.48822373151779
time_elpased: 2.354
batch start
#iterations: 89
currently lose_sum: 95.86916905641556
time_elpased: 2.455
batch start
#iterations: 90
currently lose_sum: 95.80580765008926
time_elpased: 2.3
batch start
#iterations: 91
currently lose_sum: 95.81620109081268
time_elpased: 2.323
batch start
#iterations: 92
currently lose_sum: 95.60412681102753
time_elpased: 2.373
batch start
#iterations: 93
currently lose_sum: 95.92612588405609
time_elpased: 2.29
batch start
#iterations: 94
currently lose_sum: 95.67458671331406
time_elpased: 2.403
batch start
#iterations: 95
currently lose_sum: 95.54383051395416
time_elpased: 2.38
batch start
#iterations: 96
currently lose_sum: 95.456045627594
time_elpased: 2.382
batch start
#iterations: 97
currently lose_sum: 95.5428147315979
time_elpased: 2.43
batch start
#iterations: 98
currently lose_sum: 95.7280905842781
time_elpased: 2.375
batch start
#iterations: 99
currently lose_sum: 95.80418890714645
time_elpased: 2.407
start validation test
0.655360824742
0.639922444834
0.713285993619
0.674615534359
0.655259128174
61.526
batch start
#iterations: 100
currently lose_sum: 95.95152229070663
time_elpased: 2.453
batch start
#iterations: 101
currently lose_sum: 95.42595851421356
time_elpased: 2.421
batch start
#iterations: 102
currently lose_sum: 95.72505015134811
time_elpased: 2.403
batch start
#iterations: 103
currently lose_sum: 95.93160486221313
time_elpased: 2.347
batch start
#iterations: 104
currently lose_sum: 95.65652471780777
time_elpased: 2.32
batch start
#iterations: 105
currently lose_sum: 95.45017349720001
time_elpased: 2.379
batch start
#iterations: 106
currently lose_sum: 95.83960956335068
time_elpased: 2.314
batch start
#iterations: 107
currently lose_sum: 95.98273211717606
time_elpased: 2.347
batch start
#iterations: 108
currently lose_sum: 95.42904514074326
time_elpased: 2.406
batch start
#iterations: 109
currently lose_sum: 95.97253465652466
time_elpased: 2.395
batch start
#iterations: 110
currently lose_sum: 95.68446058034897
time_elpased: 2.383
batch start
#iterations: 111
currently lose_sum: 95.48162549734116
time_elpased: 2.39
batch start
#iterations: 112
currently lose_sum: 95.46622514724731
time_elpased: 2.375
batch start
#iterations: 113
currently lose_sum: 95.4016142487526
time_elpased: 2.399
batch start
#iterations: 114
currently lose_sum: 95.3701663017273
time_elpased: 2.421
batch start
#iterations: 115
currently lose_sum: 95.7879928946495
time_elpased: 2.414
batch start
#iterations: 116
currently lose_sum: 95.16028648614883
time_elpased: 2.44
batch start
#iterations: 117
currently lose_sum: 95.5803849697113
time_elpased: 2.286
batch start
#iterations: 118
currently lose_sum: 95.08947217464447
time_elpased: 2.377
batch start
#iterations: 119
currently lose_sum: 95.43168497085571
time_elpased: 2.352
start validation test
0.654432989691
0.641362484752
0.703406401153
0.670953175616
0.654347009313
61.482
batch start
#iterations: 120
currently lose_sum: 95.49060791730881
time_elpased: 2.365
batch start
#iterations: 121
currently lose_sum: 95.20518165826797
time_elpased: 2.378
batch start
#iterations: 122
currently lose_sum: 95.17782181501389
time_elpased: 2.396
batch start
#iterations: 123
currently lose_sum: 95.50762927532196
time_elpased: 2.389
batch start
#iterations: 124
currently lose_sum: 95.66380792856216
time_elpased: 2.358
batch start
#iterations: 125
currently lose_sum: 95.40602570772171
time_elpased: 2.43
batch start
#iterations: 126
currently lose_sum: 95.65640938282013
time_elpased: 2.446
batch start
#iterations: 127
currently lose_sum: 95.64371705055237
time_elpased: 2.412
batch start
#iterations: 128
currently lose_sum: 95.28364205360413
time_elpased: 2.414
batch start
#iterations: 129
currently lose_sum: 96.04467380046844
time_elpased: 2.414
batch start
#iterations: 130
currently lose_sum: 95.14813834428787
time_elpased: 2.332
batch start
#iterations: 131
currently lose_sum: 95.90562301874161
time_elpased: 2.332
batch start
#iterations: 132
currently lose_sum: 95.32859975099564
time_elpased: 2.336
batch start
#iterations: 133
currently lose_sum: 95.42179328203201
time_elpased: 2.408
batch start
#iterations: 134
currently lose_sum: 95.03414052724838
time_elpased: 2.332
batch start
#iterations: 135
currently lose_sum: 95.3771590590477
time_elpased: 2.384
batch start
#iterations: 136
currently lose_sum: 95.24929308891296
time_elpased: 2.404
batch start
#iterations: 137
currently lose_sum: 95.21041637659073
time_elpased: 2.403
batch start
#iterations: 138
currently lose_sum: 95.5444267988205
time_elpased: 2.419
batch start
#iterations: 139
currently lose_sum: 95.1304240822792
time_elpased: 2.405
start validation test
0.651494845361
0.642940038685
0.684161778327
0.662910704492
0.651437493521
61.525
batch start
#iterations: 140
currently lose_sum: 95.52267283201218
time_elpased: 2.403
batch start
#iterations: 141
currently lose_sum: 95.56624883413315
time_elpased: 2.423
batch start
#iterations: 142
currently lose_sum: 95.1793863773346
time_elpased: 2.4
batch start
#iterations: 143
currently lose_sum: 95.32394814491272
time_elpased: 2.378
batch start
#iterations: 144
currently lose_sum: 95.51664501428604
time_elpased: 2.309
batch start
#iterations: 145
currently lose_sum: 95.33974820375443
time_elpased: 2.335
batch start
#iterations: 146
currently lose_sum: 95.40012729167938
time_elpased: 2.37
batch start
#iterations: 147
currently lose_sum: 95.44509637355804
time_elpased: 2.371
batch start
#iterations: 148
currently lose_sum: 95.15339201688766
time_elpased: 2.422
batch start
#iterations: 149
currently lose_sum: 95.41441512107849
time_elpased: 2.407
batch start
#iterations: 150
currently lose_sum: 95.26654726266861
time_elpased: 2.393
batch start
#iterations: 151
currently lose_sum: 95.32776123285294
time_elpased: 2.393
batch start
#iterations: 152
currently lose_sum: 95.37592035531998
time_elpased: 2.405
batch start
#iterations: 153
currently lose_sum: 95.67044007778168
time_elpased: 2.364
batch start
#iterations: 154
currently lose_sum: 95.24757796525955
time_elpased: 2.397
batch start
#iterations: 155
currently lose_sum: 95.3059298992157
time_elpased: 2.392
batch start
#iterations: 156
currently lose_sum: 95.64874994754791
time_elpased: 2.39
batch start
#iterations: 157
currently lose_sum: 94.83530789613724
time_elpased: 2.274
batch start
#iterations: 158
currently lose_sum: 95.25974148511887
time_elpased: 2.271
batch start
#iterations: 159
currently lose_sum: 95.19272845983505
time_elpased: 2.356
start validation test
0.655927835052
0.638071895425
0.723371410929
0.678049486326
0.655809427452
61.292
batch start
#iterations: 160
currently lose_sum: 95.35381883382797
time_elpased: 2.368
batch start
#iterations: 161
currently lose_sum: 95.80901491641998
time_elpased: 2.313
batch start
#iterations: 162
currently lose_sum: 95.53677493333817
time_elpased: 2.396
batch start
#iterations: 163
currently lose_sum: 95.18570280075073
time_elpased: 2.375
batch start
#iterations: 164
currently lose_sum: 95.21688950061798
time_elpased: 2.328
batch start
#iterations: 165
currently lose_sum: 95.33173084259033
time_elpased: 2.371
batch start
#iterations: 166
currently lose_sum: 95.40942233800888
time_elpased: 2.384
batch start
#iterations: 167
currently lose_sum: 95.17185032367706
time_elpased: 2.394
batch start
#iterations: 168
currently lose_sum: 95.24529802799225
time_elpased: 2.387
batch start
#iterations: 169
currently lose_sum: 95.24877923727036
time_elpased: 2.4
batch start
#iterations: 170
currently lose_sum: 95.70562678575516
time_elpased: 2.362
batch start
#iterations: 171
currently lose_sum: 95.44950067996979
time_elpased: 2.209
batch start
#iterations: 172
currently lose_sum: 95.52884995937347
time_elpased: 2.321
batch start
#iterations: 173
currently lose_sum: 95.39597553014755
time_elpased: 2.596
batch start
#iterations: 174
currently lose_sum: 94.86863052845001
time_elpased: 2.349
batch start
#iterations: 175
currently lose_sum: 95.01561695337296
time_elpased: 2.375
batch start
#iterations: 176
currently lose_sum: 95.17269682884216
time_elpased: 2.444
batch start
#iterations: 177
currently lose_sum: 95.24301254749298
time_elpased: 2.388
batch start
#iterations: 178
currently lose_sum: 95.29925668239594
time_elpased: 2.44
batch start
#iterations: 179
currently lose_sum: 95.44506722688675
time_elpased: 2.363
start validation test
0.659226804124
0.637726144023
0.740043223217
0.685085504692
0.659084918435
61.254
batch start
#iterations: 180
currently lose_sum: 95.1040118932724
time_elpased: 2.41
batch start
#iterations: 181
currently lose_sum: 95.12506371736526
time_elpased: 2.418
batch start
#iterations: 182
currently lose_sum: 95.23222541809082
time_elpased: 2.415
batch start
#iterations: 183
currently lose_sum: 95.2179337143898
time_elpased: 2.373
batch start
#iterations: 184
currently lose_sum: 95.6888815164566
time_elpased: 2.279
batch start
#iterations: 185
currently lose_sum: 94.9034760594368
time_elpased: 2.311
batch start
#iterations: 186
currently lose_sum: 95.51717740297318
time_elpased: 2.373
batch start
#iterations: 187
currently lose_sum: 95.26366329193115
time_elpased: 2.444
batch start
#iterations: 188
currently lose_sum: 95.33688175678253
time_elpased: 2.387
batch start
#iterations: 189
currently lose_sum: 95.2789859175682
time_elpased: 2.463
batch start
#iterations: 190
currently lose_sum: 94.76028883457184
time_elpased: 2.212
batch start
#iterations: 191
currently lose_sum: 95.12692600488663
time_elpased: 2.233
batch start
#iterations: 192
currently lose_sum: 95.25337702035904
time_elpased: 2.205
batch start
#iterations: 193
currently lose_sum: 95.11646908521652
time_elpased: 2.212
batch start
#iterations: 194
currently lose_sum: 94.86460971832275
time_elpased: 2.168
batch start
#iterations: 195
currently lose_sum: 95.3359699845314
time_elpased: 2.153
batch start
#iterations: 196
currently lose_sum: 95.04615157842636
time_elpased: 2.175
batch start
#iterations: 197
currently lose_sum: 95.39978379011154
time_elpased: 2.169
batch start
#iterations: 198
currently lose_sum: 95.08834093809128
time_elpased: 2.09
batch start
#iterations: 199
currently lose_sum: 94.98105227947235
time_elpased: 2.106
start validation test
0.653865979381
0.640806754221
0.702994751467
0.670461795161
0.653779726244
61.351
batch start
#iterations: 200
currently lose_sum: 95.23713612556458
time_elpased: 2.176
batch start
#iterations: 201
currently lose_sum: 95.16292017698288
time_elpased: 2.138
batch start
#iterations: 202
currently lose_sum: 94.75821352005005
time_elpased: 2.149
batch start
#iterations: 203
currently lose_sum: 95.41887044906616
time_elpased: 2.138
batch start
#iterations: 204
currently lose_sum: 94.84973305463791
time_elpased: 2.202
batch start
#iterations: 205
currently lose_sum: 94.90974187850952
time_elpased: 2.185
batch start
#iterations: 206
currently lose_sum: 94.844069480896
time_elpased: 2.125
batch start
#iterations: 207
currently lose_sum: 95.37595850229263
time_elpased: 2.14
batch start
#iterations: 208
currently lose_sum: 94.92073559761047
time_elpased: 2.249
batch start
#iterations: 209
currently lose_sum: 95.03061872720718
time_elpased: 2.211
batch start
#iterations: 210
currently lose_sum: 94.9658060669899
time_elpased: 2.166
batch start
#iterations: 211
currently lose_sum: 95.28240007162094
time_elpased: 2.154
batch start
#iterations: 212
currently lose_sum: 95.54182147979736
time_elpased: 2.211
batch start
#iterations: 213
currently lose_sum: 95.34716492891312
time_elpased: 2.13
batch start
#iterations: 214
currently lose_sum: 94.96139305830002
time_elpased: 2.165
batch start
#iterations: 215
currently lose_sum: 95.02782422304153
time_elpased: 2.122
batch start
#iterations: 216
currently lose_sum: 94.99977993965149
time_elpased: 2.166
batch start
#iterations: 217
currently lose_sum: 95.21106368303299
time_elpased: 2.151
batch start
#iterations: 218
currently lose_sum: 94.72251147031784
time_elpased: 2.215
batch start
#iterations: 219
currently lose_sum: 95.3770701289177
time_elpased: 2.12
start validation test
0.658556701031
0.640527033167
0.72542965936
0.680339735547
0.658439295238
61.408
batch start
#iterations: 220
currently lose_sum: 95.10671699047089
time_elpased: 2.269
batch start
#iterations: 221
currently lose_sum: 95.21875923871994
time_elpased: 2.184
batch start
#iterations: 222
currently lose_sum: 95.01019209623337
time_elpased: 2.2
batch start
#iterations: 223
currently lose_sum: 94.65166866779327
time_elpased: 2.147
batch start
#iterations: 224
currently lose_sum: 95.0414234995842
time_elpased: 2.196
batch start
#iterations: 225
currently lose_sum: 95.56131064891815
time_elpased: 2.339
batch start
#iterations: 226
currently lose_sum: 94.65198647975922
time_elpased: 2.37
batch start
#iterations: 227
currently lose_sum: 94.98319071531296
time_elpased: 2.285
batch start
#iterations: 228
currently lose_sum: 94.8858585357666
time_elpased: 2.218
batch start
#iterations: 229
currently lose_sum: 95.05385434627533
time_elpased: 2.209
batch start
#iterations: 230
currently lose_sum: 95.21418648958206
time_elpased: 2.151
batch start
#iterations: 231
currently lose_sum: 94.74397563934326
time_elpased: 2.11
batch start
#iterations: 232
currently lose_sum: 94.9970765709877
time_elpased: 2.232
batch start
#iterations: 233
currently lose_sum: 95.26084804534912
time_elpased: 2.167
batch start
#iterations: 234
currently lose_sum: 95.14259576797485
time_elpased: 2.195
batch start
#iterations: 235
currently lose_sum: 94.75443589687347
time_elpased: 2.077
batch start
#iterations: 236
currently lose_sum: 95.21061873435974
time_elpased: 2.152
batch start
#iterations: 237
currently lose_sum: 95.1951510310173
time_elpased: 2.187
batch start
#iterations: 238
currently lose_sum: 95.22621309757233
time_elpased: 2.206
batch start
#iterations: 239
currently lose_sum: 95.10744893550873
time_elpased: 2.054
start validation test
0.651494845361
0.637182105068
0.706493773798
0.67005026597
0.651398286259
61.287
batch start
#iterations: 240
currently lose_sum: 94.9133511185646
time_elpased: 2.219
batch start
#iterations: 241
currently lose_sum: 95.08045780658722
time_elpased: 2.181
batch start
#iterations: 242
currently lose_sum: 95.2456026673317
time_elpased: 2.137
batch start
#iterations: 243
currently lose_sum: 95.13148087263107
time_elpased: 2.113
batch start
#iterations: 244
currently lose_sum: 95.03494024276733
time_elpased: 2.091
batch start
#iterations: 245
currently lose_sum: 95.18078351020813
time_elpased: 2.168
batch start
#iterations: 246
currently lose_sum: 95.28457033634186
time_elpased: 2.21
batch start
#iterations: 247
currently lose_sum: 94.72348934412003
time_elpased: 2.082
batch start
#iterations: 248
currently lose_sum: 94.96178621053696
time_elpased: 2.209
batch start
#iterations: 249
currently lose_sum: 95.19291204214096
time_elpased: 2.173
batch start
#iterations: 250
currently lose_sum: 94.8289725780487
time_elpased: 2.17
batch start
#iterations: 251
currently lose_sum: 95.01610690355301
time_elpased: 2.19
batch start
#iterations: 252
currently lose_sum: 95.3167359828949
time_elpased: 2.075
batch start
#iterations: 253
currently lose_sum: 94.73686903715134
time_elpased: 2.178
batch start
#iterations: 254
currently lose_sum: 95.10346442461014
time_elpased: 2.232
batch start
#iterations: 255
currently lose_sum: 94.96755838394165
time_elpased: 2.118
batch start
#iterations: 256
currently lose_sum: 94.61932575702667
time_elpased: 2.137
batch start
#iterations: 257
currently lose_sum: 95.0267162322998
time_elpased: 2.151
batch start
#iterations: 258
currently lose_sum: 95.06448864936829
time_elpased: 2.162
batch start
#iterations: 259
currently lose_sum: 94.61560213565826
time_elpased: 2.161
start validation test
0.663917525773
0.64169843099
0.74498301945
0.689494237546
0.663775202796
61.026
batch start
#iterations: 260
currently lose_sum: 94.68196892738342
time_elpased: 2.115
batch start
#iterations: 261
currently lose_sum: 94.59461867809296
time_elpased: 2.198
batch start
#iterations: 262
currently lose_sum: 95.26003855466843
time_elpased: 2.162
batch start
#iterations: 263
currently lose_sum: 95.40609711408615
time_elpased: 2.159
batch start
#iterations: 264
currently lose_sum: 94.97414267063141
time_elpased: 2.149
batch start
#iterations: 265
currently lose_sum: 94.89339369535446
time_elpased: 2.165
batch start
#iterations: 266
currently lose_sum: 94.80387967824936
time_elpased: 2.196
batch start
#iterations: 267
currently lose_sum: 94.92378866672516
time_elpased: 2.188
batch start
#iterations: 268
currently lose_sum: 94.9483796954155
time_elpased: 2.177
batch start
#iterations: 269
currently lose_sum: 95.14875000715256
time_elpased: 2.083
batch start
#iterations: 270
currently lose_sum: 95.01056981086731
time_elpased: 2.217
batch start
#iterations: 271
currently lose_sum: 94.8937965631485
time_elpased: 2.273
batch start
#iterations: 272
currently lose_sum: 94.80234497785568
time_elpased: 2.262
batch start
#iterations: 273
currently lose_sum: 94.76609110832214
time_elpased: 2.368
batch start
#iterations: 274
currently lose_sum: 95.18027776479721
time_elpased: 2.39
batch start
#iterations: 275
currently lose_sum: 95.02055460214615
time_elpased: 2.408
batch start
#iterations: 276
currently lose_sum: 94.83427011966705
time_elpased: 2.4
batch start
#iterations: 277
currently lose_sum: 94.75414550304413
time_elpased: 2.427
batch start
#iterations: 278
currently lose_sum: 94.74358248710632
time_elpased: 2.442
batch start
#iterations: 279
currently lose_sum: 95.03011792898178
time_elpased: 2.392
start validation test
0.66118556701
0.640457469621
0.737676237522
0.685637763642
0.66105127584
61.134
batch start
#iterations: 280
currently lose_sum: 95.03272014856339
time_elpased: 2.456
batch start
#iterations: 281
currently lose_sum: 94.93766641616821
time_elpased: 2.44
batch start
#iterations: 282
currently lose_sum: 94.79615503549576
time_elpased: 2.453
batch start
#iterations: 283
currently lose_sum: 94.93763214349747
time_elpased: 2.416
batch start
#iterations: 284
currently lose_sum: 94.85460829734802
time_elpased: 2.484
batch start
#iterations: 285
currently lose_sum: 94.83327370882034
time_elpased: 2.347
batch start
#iterations: 286
currently lose_sum: 94.69861394166946
time_elpased: 2.307
batch start
#iterations: 287
currently lose_sum: 94.73740446567535
time_elpased: 2.419
batch start
#iterations: 288
currently lose_sum: 95.07775038480759
time_elpased: 2.396
batch start
#iterations: 289
currently lose_sum: 94.84427988529205
time_elpased: 2.481
batch start
#iterations: 290
currently lose_sum: 94.69598758220673
time_elpased: 2.428
batch start
#iterations: 291
currently lose_sum: 95.08211416006088
time_elpased: 2.432
batch start
#iterations: 292
currently lose_sum: 95.2254456281662
time_elpased: 2.448
batch start
#iterations: 293
currently lose_sum: 95.01835072040558
time_elpased: 2.41
batch start
#iterations: 294
currently lose_sum: 95.15121245384216
time_elpased: 2.449
batch start
#iterations: 295
currently lose_sum: 95.24201452732086
time_elpased: 2.443
batch start
#iterations: 296
currently lose_sum: 94.93404483795166
time_elpased: 2.471
batch start
#iterations: 297
currently lose_sum: 94.96988135576248
time_elpased: 2.416
batch start
#iterations: 298
currently lose_sum: 95.21565854549408
time_elpased: 2.402
batch start
#iterations: 299
currently lose_sum: 94.87697237730026
time_elpased: 2.291
start validation test
0.660927835052
0.636111352008
0.754862611917
0.690417921687
0.660762918062
61.158
batch start
#iterations: 300
currently lose_sum: 95.00931268930435
time_elpased: 2.391
batch start
#iterations: 301
currently lose_sum: 95.10619300603867
time_elpased: 2.355
batch start
#iterations: 302
currently lose_sum: 94.73667120933533
time_elpased: 2.438
batch start
#iterations: 303
currently lose_sum: 95.06330221891403
time_elpased: 2.456
batch start
#iterations: 304
currently lose_sum: 95.31755459308624
time_elpased: 2.43
batch start
#iterations: 305
currently lose_sum: 94.8886050581932
time_elpased: 2.438
batch start
#iterations: 306
currently lose_sum: 94.82685399055481
time_elpased: 2.476
batch start
#iterations: 307
currently lose_sum: 94.75750917196274
time_elpased: 2.422
batch start
#iterations: 308
currently lose_sum: 94.25155299901962
time_elpased: 2.494
batch start
#iterations: 309
currently lose_sum: 94.74220991134644
time_elpased: 2.454
batch start
#iterations: 310
currently lose_sum: 95.27912348508835
time_elpased: 2.463
batch start
#iterations: 311
currently lose_sum: 94.87206542491913
time_elpased: 2.402
batch start
#iterations: 312
currently lose_sum: 94.7587497830391
time_elpased: 2.391
batch start
#iterations: 313
currently lose_sum: 94.89325976371765
time_elpased: 2.312
batch start
#iterations: 314
currently lose_sum: 94.81712287664413
time_elpased: 2.422
batch start
#iterations: 315
currently lose_sum: 94.66524714231491
time_elpased: 2.363
batch start
#iterations: 316
currently lose_sum: 94.85454905033112
time_elpased: 2.446
batch start
#iterations: 317
currently lose_sum: 94.75670403242111
time_elpased: 2.422
batch start
#iterations: 318
currently lose_sum: 94.8520724773407
time_elpased: 2.426
batch start
#iterations: 319
currently lose_sum: 94.78901320695877
time_elpased: 2.48
start validation test
0.657886597938
0.643389199255
0.711124832767
0.675563376839
0.657793130005
61.193
batch start
#iterations: 320
currently lose_sum: 94.61673176288605
time_elpased: 2.535
batch start
#iterations: 321
currently lose_sum: 95.03388822078705
time_elpased: 2.433
batch start
#iterations: 322
currently lose_sum: 95.22503983974457
time_elpased: 2.467
batch start
#iterations: 323
currently lose_sum: 95.06146198511124
time_elpased: 2.464
batch start
#iterations: 324
currently lose_sum: 94.27482837438583
time_elpased: 2.463
batch start
#iterations: 325
currently lose_sum: 95.16474336385727
time_elpased: 2.359
batch start
#iterations: 326
currently lose_sum: 94.41122078895569
time_elpased: 2.327
batch start
#iterations: 327
currently lose_sum: 95.0407002568245
time_elpased: 2.419
batch start
#iterations: 328
currently lose_sum: 94.88615149259567
time_elpased: 2.445
batch start
#iterations: 329
currently lose_sum: 94.64111292362213
time_elpased: 2.489
batch start
#iterations: 330
currently lose_sum: 95.11511939764023
time_elpased: 2.482
batch start
#iterations: 331
currently lose_sum: 94.80106276273727
time_elpased: 2.469
batch start
#iterations: 332
currently lose_sum: 94.71595358848572
time_elpased: 2.53
batch start
#iterations: 333
currently lose_sum: 94.79700565338135
time_elpased: 2.447
batch start
#iterations: 334
currently lose_sum: 94.60981982946396
time_elpased: 2.477
batch start
#iterations: 335
currently lose_sum: 94.79972398281097
time_elpased: 2.446
batch start
#iterations: 336
currently lose_sum: 94.88428980112076
time_elpased: 2.509
batch start
#iterations: 337
currently lose_sum: 95.36117231845856
time_elpased: 2.445
batch start
#iterations: 338
currently lose_sum: 94.78459364175797
time_elpased: 2.47
batch start
#iterations: 339
currently lose_sum: 94.75331622362137
time_elpased: 2.308
start validation test
0.657577319588
0.640058319665
0.722856848822
0.678942535402
0.657462711305
61.231
batch start
#iterations: 340
currently lose_sum: 94.719218313694
time_elpased: 2.514
batch start
#iterations: 341
currently lose_sum: 94.83658510446548
time_elpased: 2.371
batch start
#iterations: 342
currently lose_sum: 95.17466902732849
time_elpased: 2.508
batch start
#iterations: 343
currently lose_sum: 94.76679408550262
time_elpased: 2.468
batch start
#iterations: 344
currently lose_sum: 94.62250930070877
time_elpased: 2.511
batch start
#iterations: 345
currently lose_sum: 95.00418835878372
time_elpased: 2.459
batch start
#iterations: 346
currently lose_sum: 94.88051408529282
time_elpased: 2.468
batch start
#iterations: 347
currently lose_sum: 94.81398725509644
time_elpased: 2.504
batch start
#iterations: 348
currently lose_sum: 94.72093403339386
time_elpased: 2.475
batch start
#iterations: 349
currently lose_sum: 95.50613164901733
time_elpased: 2.468
batch start
#iterations: 350
currently lose_sum: 94.67833018302917
time_elpased: 2.432
batch start
#iterations: 351
currently lose_sum: 94.91546022891998
time_elpased: 2.405
batch start
#iterations: 352
currently lose_sum: 94.83178400993347
time_elpased: 2.335
batch start
#iterations: 353
currently lose_sum: 94.53288412094116
time_elpased: 2.356
batch start
#iterations: 354
currently lose_sum: 95.30576694011688
time_elpased: 2.399
batch start
#iterations: 355
currently lose_sum: 94.76343357563019
time_elpased: 2.376
batch start
#iterations: 356
currently lose_sum: 94.83488953113556
time_elpased: 2.456
batch start
#iterations: 357
currently lose_sum: 94.84858125448227
time_elpased: 2.458
batch start
#iterations: 358
currently lose_sum: 94.80251437425613
time_elpased: 2.443
batch start
#iterations: 359
currently lose_sum: 94.78965908288956
time_elpased: 2.435
start validation test
0.659742268041
0.640715317919
0.730060718329
0.682476309587
0.659618813156
61.234
batch start
#iterations: 360
currently lose_sum: 95.07742810249329
time_elpased: 2.448
batch start
#iterations: 361
currently lose_sum: 95.07187205553055
time_elpased: 2.435
batch start
#iterations: 362
currently lose_sum: 94.68544173240662
time_elpased: 2.414
batch start
#iterations: 363
currently lose_sum: 94.92651736736298
time_elpased: 2.394
batch start
#iterations: 364
currently lose_sum: 94.86193585395813
time_elpased: 2.474
batch start
#iterations: 365
currently lose_sum: 95.25070291757584
time_elpased: 2.358
batch start
#iterations: 366
currently lose_sum: 94.65814018249512
time_elpased: 2.393
batch start
#iterations: 367
currently lose_sum: 95.0185763835907
time_elpased: 2.417
batch start
#iterations: 368
currently lose_sum: 95.00658470392227
time_elpased: 2.412
batch start
#iterations: 369
currently lose_sum: 94.89442777633667
time_elpased: 2.395
batch start
#iterations: 370
currently lose_sum: 94.60933393239975
time_elpased: 2.467
batch start
#iterations: 371
currently lose_sum: 95.10224914550781
time_elpased: 2.483
batch start
#iterations: 372
currently lose_sum: 94.46272832155228
time_elpased: 2.546
batch start
#iterations: 373
currently lose_sum: 95.04919505119324
time_elpased: 2.513
batch start
#iterations: 374
currently lose_sum: 95.10731786489487
time_elpased: 2.43
batch start
#iterations: 375
currently lose_sum: 94.43177843093872
time_elpased: 2.43
batch start
#iterations: 376
currently lose_sum: 95.05816173553467
time_elpased: 2.394
batch start
#iterations: 377
currently lose_sum: 95.0804368853569
time_elpased: 2.394
batch start
#iterations: 378
currently lose_sum: 94.90598946809769
time_elpased: 2.43
batch start
#iterations: 379
currently lose_sum: 94.6335374712944
time_elpased: 2.286
start validation test
0.660824742268
0.637575651259
0.748070392096
0.688417463775
0.660671569073
61.076
batch start
#iterations: 380
currently lose_sum: 95.17064636945724
time_elpased: 2.44
batch start
#iterations: 381
currently lose_sum: 94.81319165229797
time_elpased: 2.336
batch start
#iterations: 382
currently lose_sum: 94.79993283748627
time_elpased: 2.44
batch start
#iterations: 383
currently lose_sum: 94.85753619670868
time_elpased: 2.437
batch start
#iterations: 384
currently lose_sum: 95.40524184703827
time_elpased: 2.437
batch start
#iterations: 385
currently lose_sum: 95.06425869464874
time_elpased: 2.441
batch start
#iterations: 386
currently lose_sum: 94.62978166341782
time_elpased: 2.501
batch start
#iterations: 387
currently lose_sum: 95.32494539022446
time_elpased: 2.451
batch start
#iterations: 388
currently lose_sum: 94.95437949895859
time_elpased: 2.416
batch start
#iterations: 389
currently lose_sum: 95.0539323091507
time_elpased: 2.421
batch start
#iterations: 390
currently lose_sum: 94.90989202260971
time_elpased: 2.478
batch start
#iterations: 391
currently lose_sum: 94.91309124231339
time_elpased: 2.394
batch start
#iterations: 392
currently lose_sum: 94.74871724843979
time_elpased: 2.285
batch start
#iterations: 393
currently lose_sum: 94.59148329496384
time_elpased: 2.432
batch start
#iterations: 394
currently lose_sum: 94.78492099046707
time_elpased: 2.437
batch start
#iterations: 395
currently lose_sum: 94.55309128761292
time_elpased: 2.404
batch start
#iterations: 396
currently lose_sum: 94.72811037302017
time_elpased: 2.468
batch start
#iterations: 397
currently lose_sum: 94.76576209068298
time_elpased: 2.468
batch start
#iterations: 398
currently lose_sum: 94.73960596323013
time_elpased: 2.45
batch start
#iterations: 399
currently lose_sum: 94.88903480768204
time_elpased: 2.414
start validation test
0.658092783505
0.642434878995
0.715755891736
0.677116292654
0.657991547025
61.242
acc: 0.661
pre: 0.640
rec: 0.738
F1: 0.686
auc: 0.661
