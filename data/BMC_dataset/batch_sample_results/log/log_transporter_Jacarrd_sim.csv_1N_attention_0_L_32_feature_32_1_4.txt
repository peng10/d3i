start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.58948922157288
time_elpased: 1.95
batch start
#iterations: 1
currently lose_sum: 100.36730897426605
time_elpased: 1.931
batch start
#iterations: 2
currently lose_sum: 100.2757049202919
time_elpased: 1.903
batch start
#iterations: 3
currently lose_sum: 100.2140901684761
time_elpased: 1.899
batch start
#iterations: 4
currently lose_sum: 100.24940520524979
time_elpased: 1.968
batch start
#iterations: 5
currently lose_sum: 100.15914899110794
time_elpased: 1.977
batch start
#iterations: 6
currently lose_sum: 100.06810408830643
time_elpased: 1.93
batch start
#iterations: 7
currently lose_sum: 100.05887746810913
time_elpased: 1.887
batch start
#iterations: 8
currently lose_sum: 100.13742220401764
time_elpased: 1.923
batch start
#iterations: 9
currently lose_sum: 100.23012810945511
time_elpased: 1.953
batch start
#iterations: 10
currently lose_sum: 99.9907882809639
time_elpased: 1.91
batch start
#iterations: 11
currently lose_sum: 100.00206172466278
time_elpased: 1.919
batch start
#iterations: 12
currently lose_sum: 99.99658769369125
time_elpased: 1.905
batch start
#iterations: 13
currently lose_sum: 100.19757252931595
time_elpased: 1.925
batch start
#iterations: 14
currently lose_sum: 99.92610085010529
time_elpased: 1.927
batch start
#iterations: 15
currently lose_sum: 100.09479278326035
time_elpased: 1.994
batch start
#iterations: 16
currently lose_sum: 100.00786298513412
time_elpased: 1.921
batch start
#iterations: 17
currently lose_sum: 100.01801723241806
time_elpased: 1.982
batch start
#iterations: 18
currently lose_sum: 99.85693019628525
time_elpased: 1.936
batch start
#iterations: 19
currently lose_sum: 99.92564851045609
time_elpased: 1.924
start validation test
0.577422680412
0.597359312909
0.479571884326
0.532024203676
0.577594472577
66.011
batch start
#iterations: 20
currently lose_sum: 99.96851855516434
time_elpased: 1.956
batch start
#iterations: 21
currently lose_sum: 99.89721620082855
time_elpased: 1.951
batch start
#iterations: 22
currently lose_sum: 99.77918499708176
time_elpased: 1.922
batch start
#iterations: 23
currently lose_sum: 100.02487647533417
time_elpased: 1.955
batch start
#iterations: 24
currently lose_sum: 99.87921386957169
time_elpased: 1.931
batch start
#iterations: 25
currently lose_sum: 99.76472342014313
time_elpased: 1.96
batch start
#iterations: 26
currently lose_sum: 99.90070563554764
time_elpased: 1.926
batch start
#iterations: 27
currently lose_sum: 99.78516507148743
time_elpased: 2.15
batch start
#iterations: 28
currently lose_sum: 99.88090485334396
time_elpased: 1.956
batch start
#iterations: 29
currently lose_sum: 99.83934277296066
time_elpased: 1.929
batch start
#iterations: 30
currently lose_sum: 99.82402163743973
time_elpased: 1.973
batch start
#iterations: 31
currently lose_sum: 99.72316771745682
time_elpased: 1.927
batch start
#iterations: 32
currently lose_sum: 99.88546305894852
time_elpased: 1.959
batch start
#iterations: 33
currently lose_sum: 99.78740900754929
time_elpased: 1.983
batch start
#iterations: 34
currently lose_sum: 99.71423590183258
time_elpased: 1.946
batch start
#iterations: 35
currently lose_sum: 99.67195773124695
time_elpased: 1.934
batch start
#iterations: 36
currently lose_sum: 99.86870807409286
time_elpased: 1.925
batch start
#iterations: 37
currently lose_sum: 99.83750396966934
time_elpased: 1.927
batch start
#iterations: 38
currently lose_sum: 99.76633900403976
time_elpased: 1.957
batch start
#iterations: 39
currently lose_sum: 99.5971155166626
time_elpased: 1.92
start validation test
0.589329896907
0.63277693475
0.429144797777
0.51143680628
0.589611126556
65.744
batch start
#iterations: 40
currently lose_sum: 99.78860980272293
time_elpased: 1.921
batch start
#iterations: 41
currently lose_sum: 99.63158643245697
time_elpased: 1.937
batch start
#iterations: 42
currently lose_sum: 99.73512881994247
time_elpased: 1.953
batch start
#iterations: 43
currently lose_sum: 99.88669753074646
time_elpased: 1.927
batch start
#iterations: 44
currently lose_sum: 99.66004991531372
time_elpased: 1.898
batch start
#iterations: 45
currently lose_sum: 99.60071587562561
time_elpased: 1.908
batch start
#iterations: 46
currently lose_sum: 99.78033638000488
time_elpased: 1.903
batch start
#iterations: 47
currently lose_sum: 99.76530450582504
time_elpased: 1.915
batch start
#iterations: 48
currently lose_sum: 99.67689836025238
time_elpased: 1.905
batch start
#iterations: 49
currently lose_sum: 99.70735263824463
time_elpased: 1.942
batch start
#iterations: 50
currently lose_sum: 99.67606008052826
time_elpased: 1.947
batch start
#iterations: 51
currently lose_sum: 99.70780164003372
time_elpased: 1.96
batch start
#iterations: 52
currently lose_sum: 99.73743253946304
time_elpased: 2.004
batch start
#iterations: 53
currently lose_sum: 99.79544603824615
time_elpased: 1.963
batch start
#iterations: 54
currently lose_sum: 99.81348967552185
time_elpased: 1.923
batch start
#iterations: 55
currently lose_sum: 99.66489607095718
time_elpased: 1.972
batch start
#iterations: 56
currently lose_sum: 99.67219805717468
time_elpased: 1.922
batch start
#iterations: 57
currently lose_sum: 99.72913801670074
time_elpased: 1.907
batch start
#iterations: 58
currently lose_sum: 99.69584929943085
time_elpased: 1.935
batch start
#iterations: 59
currently lose_sum: 99.65338051319122
time_elpased: 1.947
start validation test
0.587525773196
0.645659928656
0.391170114233
0.487182773648
0.587870505841
65.765
batch start
#iterations: 60
currently lose_sum: 99.65029680728912
time_elpased: 1.903
batch start
#iterations: 61
currently lose_sum: 99.78600430488586
time_elpased: 1.975
batch start
#iterations: 62
currently lose_sum: 99.70513617992401
time_elpased: 1.943
batch start
#iterations: 63
currently lose_sum: 99.74270039796829
time_elpased: 1.945
batch start
#iterations: 64
currently lose_sum: 99.74396574497223
time_elpased: 1.932
batch start
#iterations: 65
currently lose_sum: 99.60001838207245
time_elpased: 1.986
batch start
#iterations: 66
currently lose_sum: 99.82033091783524
time_elpased: 1.979
batch start
#iterations: 67
currently lose_sum: 99.70062220096588
time_elpased: 1.956
batch start
#iterations: 68
currently lose_sum: 99.65758466720581
time_elpased: 1.975
batch start
#iterations: 69
currently lose_sum: 99.79076671600342
time_elpased: 2.019
batch start
#iterations: 70
currently lose_sum: 99.65619748830795
time_elpased: 1.918
batch start
#iterations: 71
currently lose_sum: 99.65447747707367
time_elpased: 1.951
batch start
#iterations: 72
currently lose_sum: 99.7017553448677
time_elpased: 2.002
batch start
#iterations: 73
currently lose_sum: 99.65116059780121
time_elpased: 1.975
batch start
#iterations: 74
currently lose_sum: 99.54365074634552
time_elpased: 1.937
batch start
#iterations: 75
currently lose_sum: 99.6988377571106
time_elpased: 1.962
batch start
#iterations: 76
currently lose_sum: 99.49447667598724
time_elpased: 1.916
batch start
#iterations: 77
currently lose_sum: 99.66246235370636
time_elpased: 1.94
batch start
#iterations: 78
currently lose_sum: 99.66924649477005
time_elpased: 1.988
batch start
#iterations: 79
currently lose_sum: 99.736787378788
time_elpased: 1.938
start validation test
0.597422680412
0.632596300932
0.468148605537
0.538088478827
0.59764964099
65.420
batch start
#iterations: 80
currently lose_sum: 99.6186380982399
time_elpased: 1.942
batch start
#iterations: 81
currently lose_sum: 99.66663867235184
time_elpased: 1.956
batch start
#iterations: 82
currently lose_sum: 99.66313362121582
time_elpased: 1.95
batch start
#iterations: 83
currently lose_sum: 99.54582434892654
time_elpased: 1.973
batch start
#iterations: 84
currently lose_sum: 99.59849941730499
time_elpased: 1.95
batch start
#iterations: 85
currently lose_sum: 99.78153789043427
time_elpased: 1.93
batch start
#iterations: 86
currently lose_sum: 99.79102021455765
time_elpased: 1.947
batch start
#iterations: 87
currently lose_sum: 99.49809503555298
time_elpased: 1.97
batch start
#iterations: 88
currently lose_sum: 99.54964226484299
time_elpased: 1.982
batch start
#iterations: 89
currently lose_sum: 99.75057607889175
time_elpased: 1.962
batch start
#iterations: 90
currently lose_sum: 99.45635259151459
time_elpased: 1.951
batch start
#iterations: 91
currently lose_sum: 99.7744123339653
time_elpased: 1.919
batch start
#iterations: 92
currently lose_sum: 99.56122410297394
time_elpased: 1.965
batch start
#iterations: 93
currently lose_sum: 99.56039673089981
time_elpased: 1.938
batch start
#iterations: 94
currently lose_sum: 99.6193556189537
time_elpased: 1.996
batch start
#iterations: 95
currently lose_sum: 99.63670647144318
time_elpased: 1.987
batch start
#iterations: 96
currently lose_sum: 99.84327709674835
time_elpased: 1.944
batch start
#iterations: 97
currently lose_sum: 99.64892238378525
time_elpased: 1.959
batch start
#iterations: 98
currently lose_sum: 99.72851014137268
time_elpased: 1.959
batch start
#iterations: 99
currently lose_sum: 99.6762883067131
time_elpased: 1.979
start validation test
0.599587628866
0.616497310221
0.530719357826
0.570401504258
0.599708537738
65.373
batch start
#iterations: 100
currently lose_sum: 99.53805011510849
time_elpased: 1.949
batch start
#iterations: 101
currently lose_sum: 99.60516154766083
time_elpased: 1.932
batch start
#iterations: 102
currently lose_sum: 99.50860124826431
time_elpased: 1.928
batch start
#iterations: 103
currently lose_sum: 99.6555945277214
time_elpased: 1.925
batch start
#iterations: 104
currently lose_sum: 99.56362807750702
time_elpased: 1.943
batch start
#iterations: 105
currently lose_sum: 99.58270132541656
time_elpased: 1.943
batch start
#iterations: 106
currently lose_sum: 99.55640375614166
time_elpased: 1.961
batch start
#iterations: 107
currently lose_sum: 99.64025640487671
time_elpased: 1.914
batch start
#iterations: 108
currently lose_sum: 99.77663284540176
time_elpased: 1.935
batch start
#iterations: 109
currently lose_sum: 99.51751238107681
time_elpased: 2.001
batch start
#iterations: 110
currently lose_sum: 99.5724880695343
time_elpased: 1.955
batch start
#iterations: 111
currently lose_sum: 99.76553076505661
time_elpased: 1.935
batch start
#iterations: 112
currently lose_sum: 99.61187446117401
time_elpased: 1.962
batch start
#iterations: 113
currently lose_sum: 99.43219262361526
time_elpased: 2.001
batch start
#iterations: 114
currently lose_sum: 99.75146228075027
time_elpased: 1.977
batch start
#iterations: 115
currently lose_sum: 99.60649991035461
time_elpased: 1.932
batch start
#iterations: 116
currently lose_sum: 99.62661319971085
time_elpased: 2.008
batch start
#iterations: 117
currently lose_sum: 99.51338666677475
time_elpased: 1.928
batch start
#iterations: 118
currently lose_sum: 99.60605680942535
time_elpased: 1.938
batch start
#iterations: 119
currently lose_sum: 99.70334148406982
time_elpased: 1.912
start validation test
0.601030927835
0.634654679199
0.479468971905
0.546253957088
0.601244348598
65.321
batch start
#iterations: 120
currently lose_sum: 99.60121238231659
time_elpased: 1.969
batch start
#iterations: 121
currently lose_sum: 99.65639585256577
time_elpased: 1.984
batch start
#iterations: 122
currently lose_sum: 99.70176720619202
time_elpased: 1.959
batch start
#iterations: 123
currently lose_sum: 99.59334951639175
time_elpased: 1.952
batch start
#iterations: 124
currently lose_sum: 99.63512969017029
time_elpased: 1.95
batch start
#iterations: 125
currently lose_sum: 99.46945172548294
time_elpased: 1.99
batch start
#iterations: 126
currently lose_sum: 99.7124274969101
time_elpased: 1.962
batch start
#iterations: 127
currently lose_sum: 99.56190538406372
time_elpased: 1.972
batch start
#iterations: 128
currently lose_sum: 99.4762796163559
time_elpased: 1.988
batch start
#iterations: 129
currently lose_sum: 99.53021335601807
time_elpased: 1.937
batch start
#iterations: 130
currently lose_sum: 99.59191542863846
time_elpased: 1.919
batch start
#iterations: 131
currently lose_sum: 99.69405996799469
time_elpased: 1.936
batch start
#iterations: 132
currently lose_sum: 99.69792222976685
time_elpased: 1.95
batch start
#iterations: 133
currently lose_sum: 99.5335682630539
time_elpased: 1.981
batch start
#iterations: 134
currently lose_sum: 99.46248942613602
time_elpased: 1.987
batch start
#iterations: 135
currently lose_sum: 99.56325817108154
time_elpased: 2.007
batch start
#iterations: 136
currently lose_sum: 99.55594271421432
time_elpased: 1.972
batch start
#iterations: 137
currently lose_sum: 99.67378997802734
time_elpased: 1.938
batch start
#iterations: 138
currently lose_sum: 99.75968849658966
time_elpased: 1.945
batch start
#iterations: 139
currently lose_sum: 99.52052080631256
time_elpased: 1.966
start validation test
0.593298969072
0.649094173331
0.409282700422
0.502019691997
0.593622038014
65.433
batch start
#iterations: 140
currently lose_sum: 99.52998566627502
time_elpased: 1.995
batch start
#iterations: 141
currently lose_sum: 99.6329516172409
time_elpased: 1.937
batch start
#iterations: 142
currently lose_sum: 99.7962087392807
time_elpased: 1.926
batch start
#iterations: 143
currently lose_sum: 99.67005681991577
time_elpased: 2.025
batch start
#iterations: 144
currently lose_sum: 99.56585502624512
time_elpased: 2.004
batch start
#iterations: 145
currently lose_sum: 99.67158740758896
time_elpased: 1.955
batch start
#iterations: 146
currently lose_sum: 99.45634132623672
time_elpased: 1.972
batch start
#iterations: 147
currently lose_sum: 99.45156782865524
time_elpased: 1.976
batch start
#iterations: 148
currently lose_sum: 99.5291239619255
time_elpased: 1.979
batch start
#iterations: 149
currently lose_sum: 99.67821806669235
time_elpased: 1.996
batch start
#iterations: 150
currently lose_sum: 99.46604269742966
time_elpased: 1.926
batch start
#iterations: 151
currently lose_sum: 99.43353056907654
time_elpased: 1.993
batch start
#iterations: 152
currently lose_sum: 99.30188530683517
time_elpased: 1.94
batch start
#iterations: 153
currently lose_sum: 99.67957711219788
time_elpased: 2.013
batch start
#iterations: 154
currently lose_sum: 99.56727665662766
time_elpased: 1.937
batch start
#iterations: 155
currently lose_sum: 99.36369639635086
time_elpased: 1.999
batch start
#iterations: 156
currently lose_sum: 99.77028197050095
time_elpased: 2.019
batch start
#iterations: 157
currently lose_sum: 99.52629762887955
time_elpased: 2.013
batch start
#iterations: 158
currently lose_sum: 99.54593032598495
time_elpased: 1.938
batch start
#iterations: 159
currently lose_sum: 99.61541813611984
time_elpased: 1.932
start validation test
0.601597938144
0.635496183206
0.479777709169
0.546765964933
0.601811812346
65.290
batch start
#iterations: 160
currently lose_sum: 99.68095815181732
time_elpased: 1.941
batch start
#iterations: 161
currently lose_sum: 99.5810626745224
time_elpased: 1.934
batch start
#iterations: 162
currently lose_sum: 99.48139625787735
time_elpased: 1.953
batch start
#iterations: 163
currently lose_sum: 99.59612560272217
time_elpased: 1.972
batch start
#iterations: 164
currently lose_sum: 99.70170873403549
time_elpased: 1.96
batch start
#iterations: 165
currently lose_sum: 99.42782777547836
time_elpased: 1.949
batch start
#iterations: 166
currently lose_sum: 99.62945592403412
time_elpased: 1.946
batch start
#iterations: 167
currently lose_sum: 99.80226510763168
time_elpased: 1.977
batch start
#iterations: 168
currently lose_sum: 99.72328114509583
time_elpased: 1.954
batch start
#iterations: 169
currently lose_sum: 99.64494949579239
time_elpased: 1.941
batch start
#iterations: 170
currently lose_sum: 99.45129853487015
time_elpased: 1.972
batch start
#iterations: 171
currently lose_sum: 99.58459061384201
time_elpased: 1.945
batch start
#iterations: 172
currently lose_sum: 99.50639486312866
time_elpased: 1.963
batch start
#iterations: 173
currently lose_sum: 99.50881099700928
time_elpased: 1.972
batch start
#iterations: 174
currently lose_sum: 99.51280635595322
time_elpased: 1.991
batch start
#iterations: 175
currently lose_sum: 99.4879447221756
time_elpased: 1.94
batch start
#iterations: 176
currently lose_sum: 99.59960025548935
time_elpased: 1.942
batch start
#iterations: 177
currently lose_sum: 99.5595805644989
time_elpased: 1.955
batch start
#iterations: 178
currently lose_sum: 99.51826477050781
time_elpased: 1.971
batch start
#iterations: 179
currently lose_sum: 99.46332186460495
time_elpased: 1.963
start validation test
0.601958762887
0.637605186922
0.475661212308
0.544854414712
0.602180497717
65.236
batch start
#iterations: 180
currently lose_sum: 99.56118214130402
time_elpased: 2.017
batch start
#iterations: 181
currently lose_sum: 99.69192808866501
time_elpased: 1.958
batch start
#iterations: 182
currently lose_sum: 99.60225200653076
time_elpased: 2.024
batch start
#iterations: 183
currently lose_sum: 99.39157855510712
time_elpased: 1.97
batch start
#iterations: 184
currently lose_sum: 99.58444184064865
time_elpased: 1.953
batch start
#iterations: 185
currently lose_sum: 99.48739767074585
time_elpased: 1.944
batch start
#iterations: 186
currently lose_sum: 99.53483492136002
time_elpased: 1.958
batch start
#iterations: 187
currently lose_sum: 99.6364478468895
time_elpased: 1.935
batch start
#iterations: 188
currently lose_sum: 99.59184008836746
time_elpased: 2.003
batch start
#iterations: 189
currently lose_sum: 99.74596333503723
time_elpased: 1.977
batch start
#iterations: 190
currently lose_sum: 99.5614692568779
time_elpased: 1.953
batch start
#iterations: 191
currently lose_sum: 99.4169071316719
time_elpased: 1.973
batch start
#iterations: 192
currently lose_sum: 99.56899172067642
time_elpased: 1.968
batch start
#iterations: 193
currently lose_sum: 99.53903633356094
time_elpased: 1.941
batch start
#iterations: 194
currently lose_sum: 99.53831523656845
time_elpased: 1.953
batch start
#iterations: 195
currently lose_sum: 99.57293635606766
time_elpased: 1.935
batch start
#iterations: 196
currently lose_sum: 99.6741561293602
time_elpased: 2.005
batch start
#iterations: 197
currently lose_sum: 99.6838054060936
time_elpased: 1.971
batch start
#iterations: 198
currently lose_sum: 99.71149945259094
time_elpased: 1.925
batch start
#iterations: 199
currently lose_sum: 99.45638859272003
time_elpased: 1.974
start validation test
0.60087628866
0.640718562874
0.462488422353
0.53720638336
0.601119249904
65.259
batch start
#iterations: 200
currently lose_sum: 99.52519065141678
time_elpased: 1.978
batch start
#iterations: 201
currently lose_sum: 99.45548474788666
time_elpased: 1.944
batch start
#iterations: 202
currently lose_sum: 99.59862577915192
time_elpased: 1.955
batch start
#iterations: 203
currently lose_sum: 99.74635398387909
time_elpased: 1.956
batch start
#iterations: 204
currently lose_sum: 99.58610939979553
time_elpased: 1.968
batch start
#iterations: 205
currently lose_sum: 99.39725363254547
time_elpased: 1.981
batch start
#iterations: 206
currently lose_sum: 99.61097943782806
time_elpased: 1.943
batch start
#iterations: 207
currently lose_sum: 99.46365761756897
time_elpased: 1.971
batch start
#iterations: 208
currently lose_sum: 99.42226600646973
time_elpased: 1.927
batch start
#iterations: 209
currently lose_sum: 99.61645287275314
time_elpased: 1.951
batch start
#iterations: 210
currently lose_sum: 99.52108359336853
time_elpased: 1.918
batch start
#iterations: 211
currently lose_sum: 99.51963275671005
time_elpased: 1.922
batch start
#iterations: 212
currently lose_sum: 99.5951731801033
time_elpased: 1.979
batch start
#iterations: 213
currently lose_sum: 99.46521866321564
time_elpased: 1.985
batch start
#iterations: 214
currently lose_sum: 99.60835826396942
time_elpased: 1.996
batch start
#iterations: 215
currently lose_sum: 99.3995161652565
time_elpased: 1.987
batch start
#iterations: 216
currently lose_sum: 99.5007494688034
time_elpased: 1.999
batch start
#iterations: 217
currently lose_sum: 99.4799234867096
time_elpased: 2.013
batch start
#iterations: 218
currently lose_sum: 99.45184302330017
time_elpased: 1.954
batch start
#iterations: 219
currently lose_sum: 99.41921854019165
time_elpased: 2.01
start validation test
0.600309278351
0.652052672347
0.433158382217
0.520529309918
0.60060273753
65.253
batch start
#iterations: 220
currently lose_sum: 99.42022287845612
time_elpased: 2.038
batch start
#iterations: 221
currently lose_sum: 99.42295527458191
time_elpased: 1.965
batch start
#iterations: 222
currently lose_sum: 99.51479631662369
time_elpased: 1.958
batch start
#iterations: 223
currently lose_sum: 99.71423542499542
time_elpased: 1.991
batch start
#iterations: 224
currently lose_sum: 99.65412598848343
time_elpased: 1.935
batch start
#iterations: 225
currently lose_sum: 99.47186976671219
time_elpased: 1.949
batch start
#iterations: 226
currently lose_sum: 99.39532387256622
time_elpased: 1.963
batch start
#iterations: 227
currently lose_sum: 99.4026762843132
time_elpased: 1.999
batch start
#iterations: 228
currently lose_sum: 99.70495474338531
time_elpased: 1.973
batch start
#iterations: 229
currently lose_sum: 99.6238699555397
time_elpased: 1.957
batch start
#iterations: 230
currently lose_sum: 99.52497416734695
time_elpased: 1.968
batch start
#iterations: 231
currently lose_sum: 99.68806558847427
time_elpased: 1.959
batch start
#iterations: 232
currently lose_sum: 99.52394306659698
time_elpased: 1.932
batch start
#iterations: 233
currently lose_sum: 99.55409544706345
time_elpased: 1.918
batch start
#iterations: 234
currently lose_sum: 99.46117049455643
time_elpased: 1.961
batch start
#iterations: 235
currently lose_sum: 99.54152292013168
time_elpased: 1.969
batch start
#iterations: 236
currently lose_sum: 99.37935662269592
time_elpased: 1.946
batch start
#iterations: 237
currently lose_sum: 99.6606285572052
time_elpased: 1.955
batch start
#iterations: 238
currently lose_sum: 99.41663557291031
time_elpased: 1.987
batch start
#iterations: 239
currently lose_sum: 99.4075910449028
time_elpased: 1.97
start validation test
0.605360824742
0.638675817521
0.488422352578
0.553533939818
0.605566128267
65.132
batch start
#iterations: 240
currently lose_sum: 99.66873127222061
time_elpased: 1.97
batch start
#iterations: 241
currently lose_sum: 99.58171540498734
time_elpased: 1.961
batch start
#iterations: 242
currently lose_sum: 99.4742739200592
time_elpased: 1.935
batch start
#iterations: 243
currently lose_sum: 99.49326705932617
time_elpased: 2.015
batch start
#iterations: 244
currently lose_sum: 99.57486432790756
time_elpased: 1.968
batch start
#iterations: 245
currently lose_sum: 99.43562096357346
time_elpased: 1.989
batch start
#iterations: 246
currently lose_sum: 99.6893105506897
time_elpased: 1.985
batch start
#iterations: 247
currently lose_sum: 99.51934516429901
time_elpased: 1.965
batch start
#iterations: 248
currently lose_sum: 99.63481003046036
time_elpased: 1.957
batch start
#iterations: 249
currently lose_sum: 99.45993810892105
time_elpased: 1.952
batch start
#iterations: 250
currently lose_sum: 99.54081177711487
time_elpased: 1.977
batch start
#iterations: 251
currently lose_sum: 99.62716990709305
time_elpased: 1.977
batch start
#iterations: 252
currently lose_sum: 99.55831116437912
time_elpased: 1.93
batch start
#iterations: 253
currently lose_sum: 99.53903388977051
time_elpased: 1.963
batch start
#iterations: 254
currently lose_sum: 99.44510626792908
time_elpased: 1.968
batch start
#iterations: 255
currently lose_sum: 99.42328876256943
time_elpased: 1.932
batch start
#iterations: 256
currently lose_sum: 99.31094127893448
time_elpased: 1.939
batch start
#iterations: 257
currently lose_sum: 99.5981782078743
time_elpased: 1.935
batch start
#iterations: 258
currently lose_sum: 99.51504290103912
time_elpased: 1.961
batch start
#iterations: 259
currently lose_sum: 99.44954633712769
time_elpased: 1.958
start validation test
0.606855670103
0.643347050754
0.482659256972
0.551537602164
0.607073716062
65.193
batch start
#iterations: 260
currently lose_sum: 99.5822623372078
time_elpased: 1.959
batch start
#iterations: 261
currently lose_sum: 99.58967036008835
time_elpased: 1.981
batch start
#iterations: 262
currently lose_sum: 99.52396309375763
time_elpased: 1.952
batch start
#iterations: 263
currently lose_sum: 99.56038624048233
time_elpased: 1.941
batch start
#iterations: 264
currently lose_sum: 99.57662600278854
time_elpased: 1.957
batch start
#iterations: 265
currently lose_sum: 99.62496024370193
time_elpased: 1.923
batch start
#iterations: 266
currently lose_sum: 99.41776728630066
time_elpased: 1.928
batch start
#iterations: 267
currently lose_sum: 99.5235680937767
time_elpased: 1.932
batch start
#iterations: 268
currently lose_sum: 99.38589960336685
time_elpased: 1.926
batch start
#iterations: 269
currently lose_sum: 99.62590169906616
time_elpased: 1.908
batch start
#iterations: 270
currently lose_sum: 99.6487187743187
time_elpased: 1.931
batch start
#iterations: 271
currently lose_sum: 99.3889349102974
time_elpased: 1.932
batch start
#iterations: 272
currently lose_sum: 99.73740410804749
time_elpased: 1.932
batch start
#iterations: 273
currently lose_sum: 99.59143251180649
time_elpased: 1.913
batch start
#iterations: 274
currently lose_sum: 99.63411742448807
time_elpased: 1.963
batch start
#iterations: 275
currently lose_sum: 99.55843323469162
time_elpased: 1.94
batch start
#iterations: 276
currently lose_sum: 99.6377882361412
time_elpased: 2.013
batch start
#iterations: 277
currently lose_sum: 99.67445600032806
time_elpased: 1.935
batch start
#iterations: 278
currently lose_sum: 99.52406841516495
time_elpased: 1.929
batch start
#iterations: 279
currently lose_sum: 99.40975725650787
time_elpased: 1.946
start validation test
0.608144329897
0.628916250152
0.53092518267
0.57578125
0.60827990002
65.157
batch start
#iterations: 280
currently lose_sum: 99.63242083787918
time_elpased: 1.953
batch start
#iterations: 281
currently lose_sum: 99.55855351686478
time_elpased: 1.955
batch start
#iterations: 282
currently lose_sum: 99.564482152462
time_elpased: 1.963
batch start
#iterations: 283
currently lose_sum: 99.488500893116
time_elpased: 1.959
batch start
#iterations: 284
currently lose_sum: 99.65280020236969
time_elpased: 1.965
batch start
#iterations: 285
currently lose_sum: 99.49512475728989
time_elpased: 1.976
batch start
#iterations: 286
currently lose_sum: 99.42034381628036
time_elpased: 1.937
batch start
#iterations: 287
currently lose_sum: 99.50246679782867
time_elpased: 1.917
batch start
#iterations: 288
currently lose_sum: 99.48988884687424
time_elpased: 1.959
batch start
#iterations: 289
currently lose_sum: 99.51772540807724
time_elpased: 1.918
batch start
#iterations: 290
currently lose_sum: 99.57349264621735
time_elpased: 1.936
batch start
#iterations: 291
currently lose_sum: 99.30626022815704
time_elpased: 1.953
batch start
#iterations: 292
currently lose_sum: 99.47916847467422
time_elpased: 1.962
batch start
#iterations: 293
currently lose_sum: 99.52151840925217
time_elpased: 1.934
batch start
#iterations: 294
currently lose_sum: 99.39576584100723
time_elpased: 1.935
batch start
#iterations: 295
currently lose_sum: 99.58437597751617
time_elpased: 1.946
batch start
#iterations: 296
currently lose_sum: 99.39438962936401
time_elpased: 1.949
batch start
#iterations: 297
currently lose_sum: 99.69889950752258
time_elpased: 1.927
batch start
#iterations: 298
currently lose_sum: 99.63278615474701
time_elpased: 2.006
batch start
#iterations: 299
currently lose_sum: 99.49421906471252
time_elpased: 1.896
start validation test
0.608144329897
0.635942923255
0.509107749305
0.565500685871
0.608318203889
65.162
batch start
#iterations: 300
currently lose_sum: 99.40440744161606
time_elpased: 1.924
batch start
#iterations: 301
currently lose_sum: 99.48742246627808
time_elpased: 1.969
batch start
#iterations: 302
currently lose_sum: 99.57240468263626
time_elpased: 1.922
batch start
#iterations: 303
currently lose_sum: 99.37660366296768
time_elpased: 1.913
batch start
#iterations: 304
currently lose_sum: 99.53156650066376
time_elpased: 2.029
batch start
#iterations: 305
currently lose_sum: 99.46420061588287
time_elpased: 1.978
batch start
#iterations: 306
currently lose_sum: 99.4531187415123
time_elpased: 1.987
batch start
#iterations: 307
currently lose_sum: 99.46040868759155
time_elpased: 1.963
batch start
#iterations: 308
currently lose_sum: 99.74563360214233
time_elpased: 1.97
batch start
#iterations: 309
currently lose_sum: 99.63582217693329
time_elpased: 1.964
batch start
#iterations: 310
currently lose_sum: 99.62044095993042
time_elpased: 1.972
batch start
#iterations: 311
currently lose_sum: 99.43662917613983
time_elpased: 1.973
batch start
#iterations: 312
currently lose_sum: 99.60822141170502
time_elpased: 1.988
batch start
#iterations: 313
currently lose_sum: 99.48960202932358
time_elpased: 2.015
batch start
#iterations: 314
currently lose_sum: 99.49067163467407
time_elpased: 1.994
batch start
#iterations: 315
currently lose_sum: 99.32092761993408
time_elpased: 1.928
batch start
#iterations: 316
currently lose_sum: 99.3814799785614
time_elpased: 1.934
batch start
#iterations: 317
currently lose_sum: 99.4665657877922
time_elpased: 1.958
batch start
#iterations: 318
currently lose_sum: 99.4840440750122
time_elpased: 1.984
batch start
#iterations: 319
currently lose_sum: 99.50409680604935
time_elpased: 1.982
start validation test
0.603298969072
0.648319389403
0.454564165895
0.534422262553
0.60356009596
65.193
batch start
#iterations: 320
currently lose_sum: 99.555251121521
time_elpased: 1.981
batch start
#iterations: 321
currently lose_sum: 99.59927695989609
time_elpased: 1.988
batch start
#iterations: 322
currently lose_sum: 99.63566142320633
time_elpased: 1.967
batch start
#iterations: 323
currently lose_sum: 99.41041040420532
time_elpased: 1.956
batch start
#iterations: 324
currently lose_sum: 99.49213308095932
time_elpased: 1.954
batch start
#iterations: 325
currently lose_sum: 99.47956377267838
time_elpased: 1.953
batch start
#iterations: 326
currently lose_sum: 99.66245913505554
time_elpased: 1.947
batch start
#iterations: 327
currently lose_sum: 99.54218935966492
time_elpased: 1.97
batch start
#iterations: 328
currently lose_sum: 99.57298529148102
time_elpased: 1.97
batch start
#iterations: 329
currently lose_sum: 99.47650700807571
time_elpased: 1.973
batch start
#iterations: 330
currently lose_sum: 99.39278167486191
time_elpased: 1.982
batch start
#iterations: 331
currently lose_sum: 99.56548196077347
time_elpased: 1.92
batch start
#iterations: 332
currently lose_sum: 99.42607909440994
time_elpased: 2.013
batch start
#iterations: 333
currently lose_sum: 99.57681095600128
time_elpased: 1.96
batch start
#iterations: 334
currently lose_sum: 99.73409765958786
time_elpased: 1.962
batch start
#iterations: 335
currently lose_sum: 99.32956230640411
time_elpased: 1.976
batch start
#iterations: 336
currently lose_sum: 99.56668829917908
time_elpased: 1.955
batch start
#iterations: 337
currently lose_sum: 99.67682880163193
time_elpased: 1.926
batch start
#iterations: 338
currently lose_sum: 99.54560387134552
time_elpased: 1.984
batch start
#iterations: 339
currently lose_sum: 99.42155802249908
time_elpased: 1.941
start validation test
0.603969072165
0.629257752923
0.509519398991
0.563093545635
0.604134893134
65.172
batch start
#iterations: 340
currently lose_sum: 99.37057322263718
time_elpased: 1.963
batch start
#iterations: 341
currently lose_sum: 99.73552739620209
time_elpased: 1.92
batch start
#iterations: 342
currently lose_sum: 99.61242485046387
time_elpased: 1.964
batch start
#iterations: 343
currently lose_sum: 99.48527890443802
time_elpased: 1.935
batch start
#iterations: 344
currently lose_sum: 99.56710922718048
time_elpased: 1.969
batch start
#iterations: 345
currently lose_sum: 99.43901669979095
time_elpased: 1.977
batch start
#iterations: 346
currently lose_sum: 99.57346713542938
time_elpased: 1.949
batch start
#iterations: 347
currently lose_sum: 99.39500725269318
time_elpased: 1.961
batch start
#iterations: 348
currently lose_sum: 99.54912185668945
time_elpased: 1.946
batch start
#iterations: 349
currently lose_sum: 99.49630457162857
time_elpased: 1.928
batch start
#iterations: 350
currently lose_sum: 99.66047829389572
time_elpased: 1.945
batch start
#iterations: 351
currently lose_sum: 99.50663667917252
time_elpased: 1.933
batch start
#iterations: 352
currently lose_sum: 99.36091619729996
time_elpased: 1.96
batch start
#iterations: 353
currently lose_sum: 99.54858732223511
time_elpased: 1.967
batch start
#iterations: 354
currently lose_sum: 99.60252332687378
time_elpased: 1.941
batch start
#iterations: 355
currently lose_sum: 99.49441343545914
time_elpased: 1.978
batch start
#iterations: 356
currently lose_sum: 99.40548259019852
time_elpased: 1.948
batch start
#iterations: 357
currently lose_sum: 99.38499069213867
time_elpased: 1.943
batch start
#iterations: 358
currently lose_sum: 99.39558577537537
time_elpased: 1.989
batch start
#iterations: 359
currently lose_sum: 99.34237229824066
time_elpased: 1.93
start validation test
0.610360824742
0.639875550946
0.507975712669
0.56634731226
0.610540577598
65.058
batch start
#iterations: 360
currently lose_sum: 99.4721063375473
time_elpased: 1.957
batch start
#iterations: 361
currently lose_sum: 99.49265855550766
time_elpased: 1.964
batch start
#iterations: 362
currently lose_sum: 99.63252067565918
time_elpased: 1.929
batch start
#iterations: 363
currently lose_sum: 99.47887343168259
time_elpased: 1.957
batch start
#iterations: 364
currently lose_sum: 99.5287458896637
time_elpased: 1.971
batch start
#iterations: 365
currently lose_sum: 99.42596572637558
time_elpased: 1.95
batch start
#iterations: 366
currently lose_sum: 99.47041529417038
time_elpased: 1.938
batch start
#iterations: 367
currently lose_sum: 99.46748423576355
time_elpased: 1.938
batch start
#iterations: 368
currently lose_sum: 99.61561393737793
time_elpased: 1.995
batch start
#iterations: 369
currently lose_sum: 99.52300119400024
time_elpased: 1.946
batch start
#iterations: 370
currently lose_sum: 99.46132534742355
time_elpased: 1.988
batch start
#iterations: 371
currently lose_sum: 99.50771003961563
time_elpased: 1.982
batch start
#iterations: 372
currently lose_sum: 99.4859983921051
time_elpased: 1.966
batch start
#iterations: 373
currently lose_sum: 99.5652527809143
time_elpased: 1.951
batch start
#iterations: 374
currently lose_sum: 99.5261070728302
time_elpased: 1.958
batch start
#iterations: 375
currently lose_sum: 99.37591832876205
time_elpased: 1.926
batch start
#iterations: 376
currently lose_sum: 99.49932032823563
time_elpased: 2.013
batch start
#iterations: 377
currently lose_sum: 99.42677074670792
time_elpased: 1.955
batch start
#iterations: 378
currently lose_sum: 99.55584460496902
time_elpased: 1.939
batch start
#iterations: 379
currently lose_sum: 99.39426302909851
time_elpased: 1.943
start validation test
0.605721649485
0.644252232143
0.475249562622
0.546994373704
0.605950713357
65.213
batch start
#iterations: 380
currently lose_sum: 99.70095586776733
time_elpased: 1.926
batch start
#iterations: 381
currently lose_sum: 99.60079902410507
time_elpased: 1.932
batch start
#iterations: 382
currently lose_sum: 99.51724201440811
time_elpased: 1.918
batch start
#iterations: 383
currently lose_sum: 99.45586204528809
time_elpased: 1.934
batch start
#iterations: 384
currently lose_sum: 99.4378497004509
time_elpased: 1.975
batch start
#iterations: 385
currently lose_sum: 99.71207916736603
time_elpased: 1.937
batch start
#iterations: 386
currently lose_sum: 99.62098830938339
time_elpased: 2.035
batch start
#iterations: 387
currently lose_sum: 99.53988415002823
time_elpased: 1.972
batch start
#iterations: 388
currently lose_sum: 99.67562276124954
time_elpased: 1.992
batch start
#iterations: 389
currently lose_sum: 99.45369523763657
time_elpased: 1.964
batch start
#iterations: 390
currently lose_sum: 99.45925623178482
time_elpased: 1.976
batch start
#iterations: 391
currently lose_sum: 99.57555091381073
time_elpased: 1.992
batch start
#iterations: 392
currently lose_sum: 99.5663868188858
time_elpased: 1.971
batch start
#iterations: 393
currently lose_sum: 99.51409095525742
time_elpased: 1.926
batch start
#iterations: 394
currently lose_sum: 99.40759140253067
time_elpased: 1.965
batch start
#iterations: 395
currently lose_sum: 99.3753861784935
time_elpased: 1.975
batch start
#iterations: 396
currently lose_sum: 99.37181901931763
time_elpased: 1.97
batch start
#iterations: 397
currently lose_sum: 99.55167865753174
time_elpased: 1.959
batch start
#iterations: 398
currently lose_sum: 99.3831295967102
time_elpased: 1.957
batch start
#iterations: 399
currently lose_sum: 99.36713629961014
time_elpased: 1.931
start validation test
0.605051546392
0.635571975195
0.495729134507
0.557007400555
0.605243478748
65.071
acc: 0.606
pre: 0.635
rec: 0.502
F1: 0.561
auc: 0.607
