start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.68639999628067
time_elpased: 1.858
batch start
#iterations: 1
currently lose_sum: 100.43573081493378
time_elpased: 1.792
batch start
#iterations: 2
currently lose_sum: 100.30754959583282
time_elpased: 1.774
batch start
#iterations: 3
currently lose_sum: 100.1404196023941
time_elpased: 1.77
batch start
#iterations: 4
currently lose_sum: 100.06572622060776
time_elpased: 1.782
batch start
#iterations: 5
currently lose_sum: 99.91166615486145
time_elpased: 1.761
batch start
#iterations: 6
currently lose_sum: 99.93469715118408
time_elpased: 1.761
batch start
#iterations: 7
currently lose_sum: 99.71780514717102
time_elpased: 1.788
batch start
#iterations: 8
currently lose_sum: 99.94917798042297
time_elpased: 1.767
batch start
#iterations: 9
currently lose_sum: 99.96334683895111
time_elpased: 1.774
batch start
#iterations: 10
currently lose_sum: 99.69416588544846
time_elpased: 1.774
batch start
#iterations: 11
currently lose_sum: 99.68470048904419
time_elpased: 1.767
batch start
#iterations: 12
currently lose_sum: 99.68082869052887
time_elpased: 1.79
batch start
#iterations: 13
currently lose_sum: 99.9086098074913
time_elpased: 1.812
batch start
#iterations: 14
currently lose_sum: 99.62210923433304
time_elpased: 1.764
batch start
#iterations: 15
currently lose_sum: 99.68825501203537
time_elpased: 1.778
batch start
#iterations: 16
currently lose_sum: 99.74324351549149
time_elpased: 1.769
batch start
#iterations: 17
currently lose_sum: 99.70290386676788
time_elpased: 1.787
batch start
#iterations: 18
currently lose_sum: 99.53891557455063
time_elpased: 1.765
batch start
#iterations: 19
currently lose_sum: 99.48942786455154
time_elpased: 1.775
start validation test
0.581546391753
0.641616448068
0.372581309181
0.4714155489
0.581891645916
65.777
batch start
#iterations: 20
currently lose_sum: 99.58587288856506
time_elpased: 1.795
batch start
#iterations: 21
currently lose_sum: 99.59163075685501
time_elpased: 1.765
batch start
#iterations: 22
currently lose_sum: 99.58845949172974
time_elpased: 1.755
batch start
#iterations: 23
currently lose_sum: 99.42554295063019
time_elpased: 1.779
batch start
#iterations: 24
currently lose_sum: 99.25471156835556
time_elpased: 1.765
batch start
#iterations: 25
currently lose_sum: 99.32839620113373
time_elpased: 1.777
batch start
#iterations: 26
currently lose_sum: 99.47979468107224
time_elpased: 1.777
batch start
#iterations: 27
currently lose_sum: 99.35621708631516
time_elpased: 1.78
batch start
#iterations: 28
currently lose_sum: 99.42883116006851
time_elpased: 1.77
batch start
#iterations: 29
currently lose_sum: 99.44985777139664
time_elpased: 1.788
batch start
#iterations: 30
currently lose_sum: 99.12897038459778
time_elpased: 1.763
batch start
#iterations: 31
currently lose_sum: 99.31953006982803
time_elpased: 1.77
batch start
#iterations: 32
currently lose_sum: 99.39837270975113
time_elpased: 1.755
batch start
#iterations: 33
currently lose_sum: 99.2521665096283
time_elpased: 1.803
batch start
#iterations: 34
currently lose_sum: 99.39608037471771
time_elpased: 1.77
batch start
#iterations: 35
currently lose_sum: 99.34181427955627
time_elpased: 1.781
batch start
#iterations: 36
currently lose_sum: 99.21871519088745
time_elpased: 1.767
batch start
#iterations: 37
currently lose_sum: 99.23506450653076
time_elpased: 1.77
batch start
#iterations: 38
currently lose_sum: 99.23112726211548
time_elpased: 1.761
batch start
#iterations: 39
currently lose_sum: 99.04060274362564
time_elpased: 1.777
start validation test
0.596237113402
0.632400506258
0.462844792096
0.534498128009
0.596457505507
65.265
batch start
#iterations: 40
currently lose_sum: 99.16473543643951
time_elpased: 1.777
batch start
#iterations: 41
currently lose_sum: 99.24152547121048
time_elpased: 1.773
batch start
#iterations: 42
currently lose_sum: 99.25693053007126
time_elpased: 1.766
batch start
#iterations: 43
currently lose_sum: 99.14934319257736
time_elpased: 1.753
batch start
#iterations: 44
currently lose_sum: 99.06906795501709
time_elpased: 1.765
batch start
#iterations: 45
currently lose_sum: 99.33080780506134
time_elpased: 1.786
batch start
#iterations: 46
currently lose_sum: 98.9967822432518
time_elpased: 1.779
batch start
#iterations: 47
currently lose_sum: 99.14110660552979
time_elpased: 1.775
batch start
#iterations: 48
currently lose_sum: 99.10239142179489
time_elpased: 1.765
batch start
#iterations: 49
currently lose_sum: 99.1477901339531
time_elpased: 1.781
batch start
#iterations: 50
currently lose_sum: 99.22303432226181
time_elpased: 1.819
batch start
#iterations: 51
currently lose_sum: 99.06898754835129
time_elpased: 1.754
batch start
#iterations: 52
currently lose_sum: 99.00443685054779
time_elpased: 1.778
batch start
#iterations: 53
currently lose_sum: 98.98374146223068
time_elpased: 1.781
batch start
#iterations: 54
currently lose_sum: 99.1584757566452
time_elpased: 1.792
batch start
#iterations: 55
currently lose_sum: 99.14277136325836
time_elpased: 1.776
batch start
#iterations: 56
currently lose_sum: 99.03997284173965
time_elpased: 1.761
batch start
#iterations: 57
currently lose_sum: 99.0496654510498
time_elpased: 1.769
batch start
#iterations: 58
currently lose_sum: 99.172014772892
time_elpased: 1.784
batch start
#iterations: 59
currently lose_sum: 99.0172056555748
time_elpased: 1.751
start validation test
0.580927835052
0.629914809961
0.395738987238
0.486093552465
0.581233805886
65.462
batch start
#iterations: 60
currently lose_sum: 98.97600853443146
time_elpased: 1.781
batch start
#iterations: 61
currently lose_sum: 99.16645050048828
time_elpased: 1.757
batch start
#iterations: 62
currently lose_sum: 99.16895526647568
time_elpased: 1.771
batch start
#iterations: 63
currently lose_sum: 99.02437299489975
time_elpased: 1.774
batch start
#iterations: 64
currently lose_sum: 99.06291842460632
time_elpased: 1.777
batch start
#iterations: 65
currently lose_sum: 98.84477108716965
time_elpased: 1.761
batch start
#iterations: 66
currently lose_sum: 99.17603212594986
time_elpased: 1.771
batch start
#iterations: 67
currently lose_sum: 98.92709523439407
time_elpased: 1.77
batch start
#iterations: 68
currently lose_sum: 99.0657764673233
time_elpased: 1.766
batch start
#iterations: 69
currently lose_sum: 98.9450591802597
time_elpased: 1.796
batch start
#iterations: 70
currently lose_sum: 99.00219559669495
time_elpased: 1.791
batch start
#iterations: 71
currently lose_sum: 98.9492437839508
time_elpased: 1.775
batch start
#iterations: 72
currently lose_sum: 98.8834742307663
time_elpased: 1.763
batch start
#iterations: 73
currently lose_sum: 98.96355420351028
time_elpased: 1.755
batch start
#iterations: 74
currently lose_sum: 99.04287838935852
time_elpased: 1.772
batch start
#iterations: 75
currently lose_sum: 99.14678055047989
time_elpased: 1.766
batch start
#iterations: 76
currently lose_sum: 98.99654549360275
time_elpased: 1.798
batch start
#iterations: 77
currently lose_sum: 98.86085742712021
time_elpased: 1.783
batch start
#iterations: 78
currently lose_sum: 99.09063231945038
time_elpased: 1.772
batch start
#iterations: 79
currently lose_sum: 98.94364541769028
time_elpased: 1.783
start validation test
0.594381443299
0.624276678778
0.477459860025
0.54108590424
0.594574622288
65.105
batch start
#iterations: 80
currently lose_sum: 98.95339119434357
time_elpased: 1.786
batch start
#iterations: 81
currently lose_sum: 99.0073891878128
time_elpased: 1.772
batch start
#iterations: 82
currently lose_sum: 98.99728018045425
time_elpased: 1.947
batch start
#iterations: 83
currently lose_sum: 98.8015855550766
time_elpased: 2.036
batch start
#iterations: 84
currently lose_sum: 98.96547245979309
time_elpased: 1.929
batch start
#iterations: 85
currently lose_sum: 98.98882305622101
time_elpased: 1.925
batch start
#iterations: 86
currently lose_sum: 98.94862771034241
time_elpased: 1.81
batch start
#iterations: 87
currently lose_sum: 98.75692975521088
time_elpased: 1.75
batch start
#iterations: 88
currently lose_sum: 98.87185478210449
time_elpased: 1.77
batch start
#iterations: 89
currently lose_sum: 98.75518888235092
time_elpased: 1.78
batch start
#iterations: 90
currently lose_sum: 98.91667741537094
time_elpased: 1.769
batch start
#iterations: 91
currently lose_sum: 99.10554993152618
time_elpased: 1.784
batch start
#iterations: 92
currently lose_sum: 98.84321320056915
time_elpased: 1.764
batch start
#iterations: 93
currently lose_sum: 98.55579668283463
time_elpased: 1.814
batch start
#iterations: 94
currently lose_sum: 99.08733665943146
time_elpased: 1.791
batch start
#iterations: 95
currently lose_sum: 98.95812141895294
time_elpased: 1.752
batch start
#iterations: 96
currently lose_sum: 98.98410350084305
time_elpased: 1.758
batch start
#iterations: 97
currently lose_sum: 98.89010775089264
time_elpased: 1.762
batch start
#iterations: 98
currently lose_sum: 98.81395542621613
time_elpased: 1.769
batch start
#iterations: 99
currently lose_sum: 98.94841438531876
time_elpased: 1.761
start validation test
0.597371134021
0.62932790224
0.47704816797
0.542708272349
0.597569932808
65.084
batch start
#iterations: 100
currently lose_sum: 99.0229389667511
time_elpased: 1.829
batch start
#iterations: 101
currently lose_sum: 99.02236449718475
time_elpased: 1.77
batch start
#iterations: 102
currently lose_sum: 99.03472274541855
time_elpased: 1.758
batch start
#iterations: 103
currently lose_sum: 98.92228591442108
time_elpased: 1.763
batch start
#iterations: 104
currently lose_sum: 99.02109336853027
time_elpased: 1.782
batch start
#iterations: 105
currently lose_sum: 98.87874400615692
time_elpased: 1.802
batch start
#iterations: 106
currently lose_sum: 98.8691269159317
time_elpased: 1.767
batch start
#iterations: 107
currently lose_sum: 98.79973888397217
time_elpased: 1.782
batch start
#iterations: 108
currently lose_sum: 98.84694665670395
time_elpased: 1.774
batch start
#iterations: 109
currently lose_sum: 98.87312233448029
time_elpased: 1.763
batch start
#iterations: 110
currently lose_sum: 98.96656078100204
time_elpased: 1.754
batch start
#iterations: 111
currently lose_sum: 98.888696372509
time_elpased: 1.778
batch start
#iterations: 112
currently lose_sum: 98.8227926492691
time_elpased: 1.768
batch start
#iterations: 113
currently lose_sum: 98.85923331975937
time_elpased: 1.765
batch start
#iterations: 114
currently lose_sum: 99.11748999357224
time_elpased: 1.906
batch start
#iterations: 115
currently lose_sum: 98.89600336551666
time_elpased: 1.852
batch start
#iterations: 116
currently lose_sum: 98.97897362709045
time_elpased: 1.78
batch start
#iterations: 117
currently lose_sum: 98.79629284143448
time_elpased: 1.757
batch start
#iterations: 118
currently lose_sum: 99.2177563905716
time_elpased: 1.783
batch start
#iterations: 119
currently lose_sum: 98.9832701086998
time_elpased: 1.773
start validation test
0.602577319588
0.617474818459
0.542610127625
0.577626821519
0.602676397972
64.964
batch start
#iterations: 120
currently lose_sum: 98.75230270624161
time_elpased: 1.828
batch start
#iterations: 121
currently lose_sum: 98.95607554912567
time_elpased: 1.812
batch start
#iterations: 122
currently lose_sum: 99.01213759183884
time_elpased: 1.76
batch start
#iterations: 123
currently lose_sum: 99.00928199291229
time_elpased: 1.772
batch start
#iterations: 124
currently lose_sum: 98.89822345972061
time_elpased: 1.76
batch start
#iterations: 125
currently lose_sum: 98.85753268003464
time_elpased: 1.764
batch start
#iterations: 126
currently lose_sum: 98.72209143638611
time_elpased: 1.768
batch start
#iterations: 127
currently lose_sum: 98.88665723800659
time_elpased: 1.779
batch start
#iterations: 128
currently lose_sum: 98.87596487998962
time_elpased: 1.758
batch start
#iterations: 129
currently lose_sum: 98.67463076114655
time_elpased: 1.764
batch start
#iterations: 130
currently lose_sum: 98.81782937049866
time_elpased: 1.761
batch start
#iterations: 131
currently lose_sum: 98.86624890565872
time_elpased: 1.791
batch start
#iterations: 132
currently lose_sum: 98.79276591539383
time_elpased: 1.76
batch start
#iterations: 133
currently lose_sum: 98.92549282312393
time_elpased: 1.773
batch start
#iterations: 134
currently lose_sum: 98.73714780807495
time_elpased: 1.758
batch start
#iterations: 135
currently lose_sum: 98.93391335010529
time_elpased: 1.761
batch start
#iterations: 136
currently lose_sum: 98.86263257265091
time_elpased: 1.747
batch start
#iterations: 137
currently lose_sum: 98.89749431610107
time_elpased: 1.765
batch start
#iterations: 138
currently lose_sum: 98.76001608371735
time_elpased: 1.753
batch start
#iterations: 139
currently lose_sum: 98.69669163227081
time_elpased: 1.782
start validation test
0.597989690722
0.628813331541
0.481576780568
0.545433350819
0.598182029276
65.077
batch start
#iterations: 140
currently lose_sum: 98.83463764190674
time_elpased: 1.82
batch start
#iterations: 141
currently lose_sum: 98.94420492649078
time_elpased: 1.784
batch start
#iterations: 142
currently lose_sum: 99.02079111337662
time_elpased: 1.742
batch start
#iterations: 143
currently lose_sum: 98.75464171171188
time_elpased: 1.769
batch start
#iterations: 144
currently lose_sum: 98.69926029443741
time_elpased: 1.746
batch start
#iterations: 145
currently lose_sum: 98.76249343156815
time_elpased: 1.778
batch start
#iterations: 146
currently lose_sum: 98.86850243806839
time_elpased: 1.738
batch start
#iterations: 147
currently lose_sum: 98.87158477306366
time_elpased: 1.791
batch start
#iterations: 148
currently lose_sum: 98.82340079545975
time_elpased: 1.769
batch start
#iterations: 149
currently lose_sum: 98.77576154470444
time_elpased: 1.786
batch start
#iterations: 150
currently lose_sum: 98.84081429243088
time_elpased: 1.757
batch start
#iterations: 151
currently lose_sum: 98.78260225057602
time_elpased: 1.754
batch start
#iterations: 152
currently lose_sum: 98.65045446157455
time_elpased: 1.765
batch start
#iterations: 153
currently lose_sum: 98.7401015162468
time_elpased: 1.773
batch start
#iterations: 154
currently lose_sum: 98.91227495670319
time_elpased: 1.755
batch start
#iterations: 155
currently lose_sum: 98.7146663069725
time_elpased: 1.77
batch start
#iterations: 156
currently lose_sum: 98.59495359659195
time_elpased: 1.767
batch start
#iterations: 157
currently lose_sum: 98.64489531517029
time_elpased: 1.795
batch start
#iterations: 158
currently lose_sum: 99.00869661569595
time_elpased: 1.772
batch start
#iterations: 159
currently lose_sum: 98.60971361398697
time_elpased: 1.768
start validation test
0.60324742268
0.632916392363
0.494750926307
0.555369418289
0.603426681658
65.006
batch start
#iterations: 160
currently lose_sum: 98.55983018875122
time_elpased: 1.813
batch start
#iterations: 161
currently lose_sum: 98.92634207010269
time_elpased: 1.855
batch start
#iterations: 162
currently lose_sum: 98.72740745544434
time_elpased: 1.758
batch start
#iterations: 163
currently lose_sum: 98.77866989374161
time_elpased: 1.768
batch start
#iterations: 164
currently lose_sum: 98.76330929994583
time_elpased: 1.771
batch start
#iterations: 165
currently lose_sum: 98.73913270235062
time_elpased: 1.995
batch start
#iterations: 166
currently lose_sum: 98.80413496494293
time_elpased: 1.966
batch start
#iterations: 167
currently lose_sum: 98.80259066820145
time_elpased: 1.906
batch start
#iterations: 168
currently lose_sum: 99.08996200561523
time_elpased: 1.765
batch start
#iterations: 169
currently lose_sum: 98.7055459022522
time_elpased: 1.776
batch start
#iterations: 170
currently lose_sum: 98.69322854280472
time_elpased: 1.773
batch start
#iterations: 171
currently lose_sum: 98.87258493900299
time_elpased: 1.774
batch start
#iterations: 172
currently lose_sum: 98.86470609903336
time_elpased: 1.881
batch start
#iterations: 173
currently lose_sum: 98.62142300605774
time_elpased: 1.781
batch start
#iterations: 174
currently lose_sum: 98.68580031394958
time_elpased: 1.783
batch start
#iterations: 175
currently lose_sum: 98.70041263103485
time_elpased: 1.806
batch start
#iterations: 176
currently lose_sum: 98.65549397468567
time_elpased: 1.78
batch start
#iterations: 177
currently lose_sum: 98.86807215213776
time_elpased: 1.81
batch start
#iterations: 178
currently lose_sum: 98.75996547937393
time_elpased: 1.781
batch start
#iterations: 179
currently lose_sum: 98.55475330352783
time_elpased: 1.77
start validation test
0.601958762887
0.62998696219
0.497324001647
0.555849534108
0.602131641468
64.919
batch start
#iterations: 180
currently lose_sum: 98.5776504278183
time_elpased: 1.782
batch start
#iterations: 181
currently lose_sum: 98.84975326061249
time_elpased: 1.77
batch start
#iterations: 182
currently lose_sum: 98.76522743701935
time_elpased: 1.769
batch start
#iterations: 183
currently lose_sum: 98.68349248170853
time_elpased: 1.78
batch start
#iterations: 184
currently lose_sum: 98.5768758058548
time_elpased: 1.809
batch start
#iterations: 185
currently lose_sum: 98.86879432201385
time_elpased: 1.769
batch start
#iterations: 186
currently lose_sum: 98.70212781429291
time_elpased: 1.758
batch start
#iterations: 187
currently lose_sum: 98.64861172437668
time_elpased: 1.776
batch start
#iterations: 188
currently lose_sum: 98.72783082723618
time_elpased: 1.767
batch start
#iterations: 189
currently lose_sum: 98.8673512339592
time_elpased: 1.776
batch start
#iterations: 190
currently lose_sum: 98.88638985157013
time_elpased: 1.767
batch start
#iterations: 191
currently lose_sum: 98.5515769124031
time_elpased: 1.778
batch start
#iterations: 192
currently lose_sum: 98.75030833482742
time_elpased: 1.754
batch start
#iterations: 193
currently lose_sum: 98.63574773073196
time_elpased: 1.789
batch start
#iterations: 194
currently lose_sum: 98.75378119945526
time_elpased: 1.763
batch start
#iterations: 195
currently lose_sum: 98.76367652416229
time_elpased: 1.766
batch start
#iterations: 196
currently lose_sum: 98.681975543499
time_elpased: 1.757
batch start
#iterations: 197
currently lose_sum: 98.47300386428833
time_elpased: 1.786
batch start
#iterations: 198
currently lose_sum: 98.6623198390007
time_elpased: 1.753
batch start
#iterations: 199
currently lose_sum: 98.58294826745987
time_elpased: 1.763
start validation test
0.602164948454
0.634563577586
0.484870317003
0.549708284714
0.602358743797
65.007
batch start
#iterations: 200
currently lose_sum: 98.90338969230652
time_elpased: 1.792
batch start
#iterations: 201
currently lose_sum: 98.63437032699585
time_elpased: 1.765
batch start
#iterations: 202
currently lose_sum: 98.54032927751541
time_elpased: 1.758
batch start
#iterations: 203
currently lose_sum: 98.68132716417313
time_elpased: 1.759
batch start
#iterations: 204
currently lose_sum: 98.75936156511307
time_elpased: 1.784
batch start
#iterations: 205
currently lose_sum: 98.6806001663208
time_elpased: 1.819
batch start
#iterations: 206
currently lose_sum: 98.67456889152527
time_elpased: 1.775
batch start
#iterations: 207
currently lose_sum: 98.59181439876556
time_elpased: 1.794
batch start
#iterations: 208
currently lose_sum: 98.88377195596695
time_elpased: 1.767
batch start
#iterations: 209
currently lose_sum: 98.75561445951462
time_elpased: 1.778
batch start
#iterations: 210
currently lose_sum: 98.80736500024796
time_elpased: 1.746
batch start
#iterations: 211
currently lose_sum: 98.12308752536774
time_elpased: 1.767
batch start
#iterations: 212
currently lose_sum: 98.64543187618256
time_elpased: 1.766
batch start
#iterations: 213
currently lose_sum: 98.70917689800262
time_elpased: 1.771
batch start
#iterations: 214
currently lose_sum: 98.69712150096893
time_elpased: 1.763
batch start
#iterations: 215
currently lose_sum: 98.48534172773361
time_elpased: 1.789
batch start
#iterations: 216
currently lose_sum: 98.75266563892365
time_elpased: 1.757
batch start
#iterations: 217
currently lose_sum: 98.46343612670898
time_elpased: 1.803
batch start
#iterations: 218
currently lose_sum: 98.59094935655594
time_elpased: 1.75
batch start
#iterations: 219
currently lose_sum: 98.48139244318008
time_elpased: 1.77
start validation test
0.595721649485
0.625016686691
0.481885549609
0.544197129075
0.595909730608
64.944
batch start
#iterations: 220
currently lose_sum: 98.51685374975204
time_elpased: 1.814
batch start
#iterations: 221
currently lose_sum: 98.40698754787445
time_elpased: 1.768
batch start
#iterations: 222
currently lose_sum: 98.67875254154205
time_elpased: 1.77
batch start
#iterations: 223
currently lose_sum: 98.44471859931946
time_elpased: 1.769
batch start
#iterations: 224
currently lose_sum: 98.43773818016052
time_elpased: 1.756
batch start
#iterations: 225
currently lose_sum: 98.42267090082169
time_elpased: 1.786
batch start
#iterations: 226
currently lose_sum: 98.63388335704803
time_elpased: 1.756
batch start
#iterations: 227
currently lose_sum: 98.61096000671387
time_elpased: 1.783
batch start
#iterations: 228
currently lose_sum: 98.61298090219498
time_elpased: 1.78
batch start
#iterations: 229
currently lose_sum: 98.42367124557495
time_elpased: 1.781
batch start
#iterations: 230
currently lose_sum: 98.53297561407089
time_elpased: 1.752
batch start
#iterations: 231
currently lose_sum: 98.65119814872742
time_elpased: 1.784
batch start
#iterations: 232
currently lose_sum: 98.75321227312088
time_elpased: 1.772
batch start
#iterations: 233
currently lose_sum: 98.51936638355255
time_elpased: 1.783
batch start
#iterations: 234
currently lose_sum: 98.6441633105278
time_elpased: 1.757
batch start
#iterations: 235
currently lose_sum: 98.95010471343994
time_elpased: 1.792
batch start
#iterations: 236
currently lose_sum: 98.79276883602142
time_elpased: 1.798
batch start
#iterations: 237
currently lose_sum: 98.43360728025436
time_elpased: 1.768
batch start
#iterations: 238
currently lose_sum: 98.55768495798111
time_elpased: 1.771
batch start
#iterations: 239
currently lose_sum: 98.88356506824493
time_elpased: 1.79
start validation test
0.598762886598
0.636982416336
0.462330177028
0.535782442748
0.598988302062
65.111
batch start
#iterations: 240
currently lose_sum: 98.54063045978546
time_elpased: 1.815
batch start
#iterations: 241
currently lose_sum: 98.67004299163818
time_elpased: 1.786
batch start
#iterations: 242
currently lose_sum: 98.59062546491623
time_elpased: 1.765
batch start
#iterations: 243
currently lose_sum: 98.87846052646637
time_elpased: 1.785
batch start
#iterations: 244
currently lose_sum: 98.51676517724991
time_elpased: 1.763
batch start
#iterations: 245
currently lose_sum: 98.62859052419662
time_elpased: 1.797
batch start
#iterations: 246
currently lose_sum: 98.81315767765045
time_elpased: 1.776
batch start
#iterations: 247
currently lose_sum: 98.55485045909882
time_elpased: 1.79
batch start
#iterations: 248
currently lose_sum: 98.70583695173264
time_elpased: 1.759
batch start
#iterations: 249
currently lose_sum: 98.40374344587326
time_elpased: 1.782
batch start
#iterations: 250
currently lose_sum: 98.72611993551254
time_elpased: 1.758
batch start
#iterations: 251
currently lose_sum: 98.51159888505936
time_elpased: 1.793
batch start
#iterations: 252
currently lose_sum: 98.69604277610779
time_elpased: 1.773
batch start
#iterations: 253
currently lose_sum: 98.60747009515762
time_elpased: 1.839
batch start
#iterations: 254
currently lose_sum: 98.50386345386505
time_elpased: 1.792
batch start
#iterations: 255
currently lose_sum: 98.5128875374794
time_elpased: 1.819
batch start
#iterations: 256
currently lose_sum: 98.3239775300026
time_elpased: 1.767
batch start
#iterations: 257
currently lose_sum: 98.78675025701523
time_elpased: 1.792
batch start
#iterations: 258
currently lose_sum: 98.64943045377731
time_elpased: 1.806
batch start
#iterations: 259
currently lose_sum: 98.43629288673401
time_elpased: 1.782
start validation test
0.597731958763
0.638110372725
0.454610951009
0.530953239572
0.597968424699
64.966
batch start
#iterations: 260
currently lose_sum: 98.61682683229446
time_elpased: 1.777
batch start
#iterations: 261
currently lose_sum: 98.61300039291382
time_elpased: 1.782
batch start
#iterations: 262
currently lose_sum: 98.55504965782166
time_elpased: 1.769
batch start
#iterations: 263
currently lose_sum: 98.69663846492767
time_elpased: 1.789
batch start
#iterations: 264
currently lose_sum: 98.5682680606842
time_elpased: 1.78
batch start
#iterations: 265
currently lose_sum: 98.51609879732132
time_elpased: 1.805
batch start
#iterations: 266
currently lose_sum: 98.62482124567032
time_elpased: 1.766
batch start
#iterations: 267
currently lose_sum: 98.59118455648422
time_elpased: 1.78
batch start
#iterations: 268
currently lose_sum: 98.56971311569214
time_elpased: 1.757
batch start
#iterations: 269
currently lose_sum: 98.320336997509
time_elpased: 1.791
batch start
#iterations: 270
currently lose_sum: 98.48691517114639
time_elpased: 1.771
batch start
#iterations: 271
currently lose_sum: 98.47810983657837
time_elpased: 1.821
batch start
#iterations: 272
currently lose_sum: 98.65700829029083
time_elpased: 1.778
batch start
#iterations: 273
currently lose_sum: 98.63652265071869
time_elpased: 1.836
batch start
#iterations: 274
currently lose_sum: 98.6699447631836
time_elpased: 1.77
batch start
#iterations: 275
currently lose_sum: 98.52857482433319
time_elpased: 1.786
batch start
#iterations: 276
currently lose_sum: 98.80218458175659
time_elpased: 1.76
batch start
#iterations: 277
currently lose_sum: 98.47953015565872
time_elpased: 1.819
batch start
#iterations: 278
currently lose_sum: 98.44999408721924
time_elpased: 1.77
batch start
#iterations: 279
currently lose_sum: 98.62695395946503
time_elpased: 1.849
start validation test
0.59706185567
0.640396273843
0.44575957184
0.525638691668
0.597311838791
65.023
batch start
#iterations: 280
currently lose_sum: 98.68236815929413
time_elpased: 1.788
batch start
#iterations: 281
currently lose_sum: 98.47138738632202
time_elpased: 1.785
batch start
#iterations: 282
currently lose_sum: 98.83511745929718
time_elpased: 1.796
batch start
#iterations: 283
currently lose_sum: 98.62617844343185
time_elpased: 1.783
batch start
#iterations: 284
currently lose_sum: 98.27415639162064
time_elpased: 1.788
batch start
#iterations: 285
currently lose_sum: 98.45531964302063
time_elpased: 1.778
batch start
#iterations: 286
currently lose_sum: 98.54466480016708
time_elpased: 1.76
batch start
#iterations: 287
currently lose_sum: 98.63187110424042
time_elpased: 1.803
batch start
#iterations: 288
currently lose_sum: 98.51640403270721
time_elpased: 1.76
batch start
#iterations: 289
currently lose_sum: 98.27168184518814
time_elpased: 1.786
batch start
#iterations: 290
currently lose_sum: 98.30698132514954
time_elpased: 1.775
batch start
#iterations: 291
currently lose_sum: 98.3059818148613
time_elpased: 1.785
batch start
#iterations: 292
currently lose_sum: 98.34159731864929
time_elpased: 1.759
batch start
#iterations: 293
currently lose_sum: 98.63669699430466
time_elpased: 1.785
batch start
#iterations: 294
currently lose_sum: 98.78679573535919
time_elpased: 1.796
batch start
#iterations: 295
currently lose_sum: 98.52648550271988
time_elpased: 1.799
batch start
#iterations: 296
currently lose_sum: 98.71711069345474
time_elpased: 1.775
batch start
#iterations: 297
currently lose_sum: 98.70319312810898
time_elpased: 1.795
batch start
#iterations: 298
currently lose_sum: 98.42251628637314
time_elpased: 1.767
batch start
#iterations: 299
currently lose_sum: 98.66713619232178
time_elpased: 1.78
start validation test
0.596237113402
0.619585926585
0.502058460272
0.554664847348
0.596392716299
64.943
batch start
#iterations: 300
currently lose_sum: 98.57095831632614
time_elpased: 1.821
batch start
#iterations: 301
currently lose_sum: 98.61222285032272
time_elpased: 1.766
batch start
#iterations: 302
currently lose_sum: 98.55187410116196
time_elpased: 1.765
batch start
#iterations: 303
currently lose_sum: 98.4215230345726
time_elpased: 1.803
batch start
#iterations: 304
currently lose_sum: 98.28693842887878
time_elpased: 1.767
batch start
#iterations: 305
currently lose_sum: 98.56672137975693
time_elpased: 1.786
batch start
#iterations: 306
currently lose_sum: 98.58077472448349
time_elpased: 1.796
batch start
#iterations: 307
currently lose_sum: 98.71153771877289
time_elpased: 1.792
batch start
#iterations: 308
currently lose_sum: 98.46476465463638
time_elpased: 1.793
batch start
#iterations: 309
currently lose_sum: 98.53253334760666
time_elpased: 1.778
batch start
#iterations: 310
currently lose_sum: 98.68591725826263
time_elpased: 1.813
batch start
#iterations: 311
currently lose_sum: 98.26436161994934
time_elpased: 1.79
batch start
#iterations: 312
currently lose_sum: 98.58476555347443
time_elpased: 1.76
batch start
#iterations: 313
currently lose_sum: 98.59679019451141
time_elpased: 1.778
batch start
#iterations: 314
currently lose_sum: 98.70718961954117
time_elpased: 1.773
batch start
#iterations: 315
currently lose_sum: 98.50695562362671
time_elpased: 1.78
batch start
#iterations: 316
currently lose_sum: 98.39094442129135
time_elpased: 1.816
batch start
#iterations: 317
currently lose_sum: 98.63922840356827
time_elpased: 1.785
batch start
#iterations: 318
currently lose_sum: 98.34427553415298
time_elpased: 1.789
batch start
#iterations: 319
currently lose_sum: 98.74477481842041
time_elpased: 1.79
start validation test
0.603711340206
0.627483027408
0.513688760807
0.564912280702
0.603860076397
64.822
batch start
#iterations: 320
currently lose_sum: 98.67382830381393
time_elpased: 1.802
batch start
#iterations: 321
currently lose_sum: 98.54571950435638
time_elpased: 1.79
batch start
#iterations: 322
currently lose_sum: 98.51825922727585
time_elpased: 1.766
batch start
#iterations: 323
currently lose_sum: 98.39099985361099
time_elpased: 1.779
batch start
#iterations: 324
currently lose_sum: 98.6021100282669
time_elpased: 1.78
batch start
#iterations: 325
currently lose_sum: 98.48142284154892
time_elpased: 1.814
batch start
#iterations: 326
currently lose_sum: 98.56140339374542
time_elpased: 1.789
batch start
#iterations: 327
currently lose_sum: 98.59180176258087
time_elpased: 1.773
batch start
#iterations: 328
currently lose_sum: 98.41453498601913
time_elpased: 1.771
batch start
#iterations: 329
currently lose_sum: 98.46855211257935
time_elpased: 1.792
batch start
#iterations: 330
currently lose_sum: 98.53567665815353
time_elpased: 1.762
batch start
#iterations: 331
currently lose_sum: 98.65364080667496
time_elpased: 1.776
batch start
#iterations: 332
currently lose_sum: 98.23022139072418
time_elpased: 1.774
batch start
#iterations: 333
currently lose_sum: 98.62030303478241
time_elpased: 1.779
batch start
#iterations: 334
currently lose_sum: 98.53571671247482
time_elpased: 1.773
batch start
#iterations: 335
currently lose_sum: 98.41139554977417
time_elpased: 1.781
batch start
#iterations: 336
currently lose_sum: 98.20073449611664
time_elpased: 1.816
batch start
#iterations: 337
currently lose_sum: 98.46973073482513
time_elpased: 1.793
batch start
#iterations: 338
currently lose_sum: 98.6193311214447
time_elpased: 1.775
batch start
#iterations: 339
currently lose_sum: 98.34588766098022
time_elpased: 1.786
start validation test
0.601907216495
0.636413415469
0.478489090161
0.546266376829
0.602111129136
64.869
batch start
#iterations: 340
currently lose_sum: 98.52494621276855
time_elpased: 1.812
batch start
#iterations: 341
currently lose_sum: 98.52637094259262
time_elpased: 1.8
batch start
#iterations: 342
currently lose_sum: 98.65393966436386
time_elpased: 1.871
batch start
#iterations: 343
currently lose_sum: 98.798863530159
time_elpased: 1.831
batch start
#iterations: 344
currently lose_sum: 98.49661320447922
time_elpased: 1.787
batch start
#iterations: 345
currently lose_sum: 98.37209856510162
time_elpased: 1.773
batch start
#iterations: 346
currently lose_sum: 98.39888870716095
time_elpased: 1.761
batch start
#iterations: 347
currently lose_sum: 98.51342821121216
time_elpased: 1.773
batch start
#iterations: 348
currently lose_sum: 98.60092347860336
time_elpased: 1.78
batch start
#iterations: 349
currently lose_sum: 98.63838005065918
time_elpased: 1.777
batch start
#iterations: 350
currently lose_sum: 98.55824291706085
time_elpased: 1.786
batch start
#iterations: 351
currently lose_sum: 98.72424983978271
time_elpased: 1.813
batch start
#iterations: 352
currently lose_sum: 98.59028708934784
time_elpased: 1.77
batch start
#iterations: 353
currently lose_sum: 98.74604332447052
time_elpased: 1.789
batch start
#iterations: 354
currently lose_sum: 98.51780217885971
time_elpased: 1.783
batch start
#iterations: 355
currently lose_sum: 98.49080896377563
time_elpased: 1.833
batch start
#iterations: 356
currently lose_sum: 98.63490432500839
time_elpased: 1.769
batch start
#iterations: 357
currently lose_sum: 98.43365097045898
time_elpased: 1.785
batch start
#iterations: 358
currently lose_sum: 98.31348830461502
time_elpased: 1.774
batch start
#iterations: 359
currently lose_sum: 98.4108299612999
time_elpased: 1.802
start validation test
0.598917525773
0.627185487051
0.491045697818
0.550828378456
0.599095752668
64.859
batch start
#iterations: 360
currently lose_sum: 98.42443311214447
time_elpased: 1.797
batch start
#iterations: 361
currently lose_sum: 98.51143074035645
time_elpased: 1.787
batch start
#iterations: 362
currently lose_sum: 98.63649809360504
time_elpased: 1.774
batch start
#iterations: 363
currently lose_sum: 98.45846319198608
time_elpased: 1.798
batch start
#iterations: 364
currently lose_sum: 98.23938834667206
time_elpased: 1.78
batch start
#iterations: 365
currently lose_sum: 98.47223150730133
time_elpased: 1.818
batch start
#iterations: 366
currently lose_sum: 98.52444350719452
time_elpased: 1.775
batch start
#iterations: 367
currently lose_sum: 98.42133390903473
time_elpased: 1.818
batch start
#iterations: 368
currently lose_sum: 98.48463612794876
time_elpased: 1.773
batch start
#iterations: 369
currently lose_sum: 98.31004726886749
time_elpased: 1.796
batch start
#iterations: 370
currently lose_sum: 98.46546906232834
time_elpased: 1.858
batch start
#iterations: 371
currently lose_sum: 98.37651801109314
time_elpased: 1.824
batch start
#iterations: 372
currently lose_sum: 98.47550350427628
time_elpased: 1.827
batch start
#iterations: 373
currently lose_sum: 98.38540476560593
time_elpased: 1.779
batch start
#iterations: 374
currently lose_sum: 98.33031111955643
time_elpased: 1.775
batch start
#iterations: 375
currently lose_sum: 98.2712345123291
time_elpased: 1.798
batch start
#iterations: 376
currently lose_sum: 98.4073703289032
time_elpased: 1.773
batch start
#iterations: 377
currently lose_sum: 98.34333205223083
time_elpased: 1.977
batch start
#iterations: 378
currently lose_sum: 98.67908734083176
time_elpased: 2.078
batch start
#iterations: 379
currently lose_sum: 98.3514176607132
time_elpased: 2.067
start validation test
0.593865979381
0.635252540127
0.444009880609
0.522687344763
0.594113573101
65.133
batch start
#iterations: 380
currently lose_sum: 98.5269695520401
time_elpased: 1.965
batch start
#iterations: 381
currently lose_sum: 98.17573791742325
time_elpased: 1.96
batch start
#iterations: 382
currently lose_sum: 98.48818463087082
time_elpased: 1.857
batch start
#iterations: 383
currently lose_sum: 98.5451568365097
time_elpased: 1.789
batch start
#iterations: 384
currently lose_sum: 98.40286302566528
time_elpased: 1.795
batch start
#iterations: 385
currently lose_sum: 98.83906364440918
time_elpased: 1.822
batch start
#iterations: 386
currently lose_sum: 98.75461548566818
time_elpased: 1.82
batch start
#iterations: 387
currently lose_sum: 98.41312944889069
time_elpased: 1.826
batch start
#iterations: 388
currently lose_sum: 98.59997743368149
time_elpased: 1.798
batch start
#iterations: 389
currently lose_sum: 98.4789589047432
time_elpased: 1.793
batch start
#iterations: 390
currently lose_sum: 98.48846882581711
time_elpased: 1.81
batch start
#iterations: 391
currently lose_sum: 98.54652959108353
time_elpased: 1.779
batch start
#iterations: 392
currently lose_sum: 98.5025492310524
time_elpased: 1.761
batch start
#iterations: 393
currently lose_sum: 98.48782867193222
time_elpased: 1.772
batch start
#iterations: 394
currently lose_sum: 98.47932749986649
time_elpased: 1.772
batch start
#iterations: 395
currently lose_sum: 98.28465688228607
time_elpased: 1.77
batch start
#iterations: 396
currently lose_sum: 98.3360253572464
time_elpased: 1.776
batch start
#iterations: 397
currently lose_sum: 98.73806393146515
time_elpased: 1.788
batch start
#iterations: 398
currently lose_sum: 98.47074472904205
time_elpased: 1.771
batch start
#iterations: 399
currently lose_sum: 98.53399002552032
time_elpased: 1.813
start validation test
0.59793814433
0.642305407011
0.445039110745
0.525778210117
0.598190765616
65.117
acc: 0.605
pre: 0.628
rec: 0.519
F1: 0.568
auc: 0.605
