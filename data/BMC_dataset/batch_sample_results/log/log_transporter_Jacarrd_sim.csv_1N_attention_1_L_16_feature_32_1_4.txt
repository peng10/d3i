start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.63798755407333
time_elpased: 1.945
batch start
#iterations: 1
currently lose_sum: 100.37531346082687
time_elpased: 1.742
batch start
#iterations: 2
currently lose_sum: 100.29287004470825
time_elpased: 1.764
batch start
#iterations: 3
currently lose_sum: 100.17896908521652
time_elpased: 1.752
batch start
#iterations: 4
currently lose_sum: 100.18736308813095
time_elpased: 1.736
batch start
#iterations: 5
currently lose_sum: 100.09365433454514
time_elpased: 1.73
batch start
#iterations: 6
currently lose_sum: 99.9810500741005
time_elpased: 1.734
batch start
#iterations: 7
currently lose_sum: 99.96745842695236
time_elpased: 1.725
batch start
#iterations: 8
currently lose_sum: 99.96848982572556
time_elpased: 1.775
batch start
#iterations: 9
currently lose_sum: 100.08224362134933
time_elpased: 1.727
batch start
#iterations: 10
currently lose_sum: 99.81364691257477
time_elpased: 1.741
batch start
#iterations: 11
currently lose_sum: 99.83366268873215
time_elpased: 1.753
batch start
#iterations: 12
currently lose_sum: 99.74454349279404
time_elpased: 1.763
batch start
#iterations: 13
currently lose_sum: 100.01972830295563
time_elpased: 1.731
batch start
#iterations: 14
currently lose_sum: 99.60259658098221
time_elpased: 1.761
batch start
#iterations: 15
currently lose_sum: 99.85091751813889
time_elpased: 1.751
batch start
#iterations: 16
currently lose_sum: 99.70268124341965
time_elpased: 1.88
batch start
#iterations: 17
currently lose_sum: 99.80485892295837
time_elpased: 1.779
batch start
#iterations: 18
currently lose_sum: 99.58259254693985
time_elpased: 1.782
batch start
#iterations: 19
currently lose_sum: 99.66694432497025
time_elpased: 1.794
start validation test
0.589020618557
0.607098992876
0.508696099619
0.553558429923
0.589161640639
65.582
batch start
#iterations: 20
currently lose_sum: 99.62535226345062
time_elpased: 1.865
batch start
#iterations: 21
currently lose_sum: 99.64299893379211
time_elpased: 1.834
batch start
#iterations: 22
currently lose_sum: 99.54223716259003
time_elpased: 1.771
batch start
#iterations: 23
currently lose_sum: 99.6422209739685
time_elpased: 1.771
batch start
#iterations: 24
currently lose_sum: 99.49638032913208
time_elpased: 1.83
batch start
#iterations: 25
currently lose_sum: 99.38133096694946
time_elpased: 1.825
batch start
#iterations: 26
currently lose_sum: 99.5525878071785
time_elpased: 1.766
batch start
#iterations: 27
currently lose_sum: 99.39648807048798
time_elpased: 1.742
batch start
#iterations: 28
currently lose_sum: 99.42016577720642
time_elpased: 1.786
batch start
#iterations: 29
currently lose_sum: 99.52223706245422
time_elpased: 1.811
batch start
#iterations: 30
currently lose_sum: 99.37733680009842
time_elpased: 1.766
batch start
#iterations: 31
currently lose_sum: 99.28001528978348
time_elpased: 1.756
batch start
#iterations: 32
currently lose_sum: 99.49352723360062
time_elpased: 1.767
batch start
#iterations: 33
currently lose_sum: 99.34791868925095
time_elpased: 1.827
batch start
#iterations: 34
currently lose_sum: 99.22176378965378
time_elpased: 1.735
batch start
#iterations: 35
currently lose_sum: 99.18475133180618
time_elpased: 1.765
batch start
#iterations: 36
currently lose_sum: 99.37335288524628
time_elpased: 1.745
batch start
#iterations: 37
currently lose_sum: 99.38111382722855
time_elpased: 1.734
batch start
#iterations: 38
currently lose_sum: 99.14856469631195
time_elpased: 1.771
batch start
#iterations: 39
currently lose_sum: 99.08858251571655
time_elpased: 1.728
start validation test
0.599587628866
0.630054717737
0.48584954204
0.548634514817
0.59978731362
65.151
batch start
#iterations: 40
currently lose_sum: 99.3059424161911
time_elpased: 1.735
batch start
#iterations: 41
currently lose_sum: 99.10373491048813
time_elpased: 1.752
batch start
#iterations: 42
currently lose_sum: 99.1494128704071
time_elpased: 1.795
batch start
#iterations: 43
currently lose_sum: 99.33707278966904
time_elpased: 1.734
batch start
#iterations: 44
currently lose_sum: 99.17971158027649
time_elpased: 1.754
batch start
#iterations: 45
currently lose_sum: 99.15401458740234
time_elpased: 1.791
batch start
#iterations: 46
currently lose_sum: 99.24128866195679
time_elpased: 1.861
batch start
#iterations: 47
currently lose_sum: 99.18149954080582
time_elpased: 1.805
batch start
#iterations: 48
currently lose_sum: 99.2348119020462
time_elpased: 1.805
batch start
#iterations: 49
currently lose_sum: 99.09986650943756
time_elpased: 1.747
batch start
#iterations: 50
currently lose_sum: 99.1493929028511
time_elpased: 1.811
batch start
#iterations: 51
currently lose_sum: 99.11483645439148
time_elpased: 1.732
batch start
#iterations: 52
currently lose_sum: 99.16012984514236
time_elpased: 1.727
batch start
#iterations: 53
currently lose_sum: 99.16630637645721
time_elpased: 1.743
batch start
#iterations: 54
currently lose_sum: 99.25475388765335
time_elpased: 1.765
batch start
#iterations: 55
currently lose_sum: 99.11733275651932
time_elpased: 1.805
batch start
#iterations: 56
currently lose_sum: 99.02628469467163
time_elpased: 1.745
batch start
#iterations: 57
currently lose_sum: 99.08354473114014
time_elpased: 1.742
batch start
#iterations: 58
currently lose_sum: 99.17049133777618
time_elpased: 1.789
batch start
#iterations: 59
currently lose_sum: 99.03160756826401
time_elpased: 1.74
start validation test
0.588969072165
0.651961639058
0.384789544098
0.483950297696
0.589327540819
65.480
batch start
#iterations: 60
currently lose_sum: 99.08685982227325
time_elpased: 1.75
batch start
#iterations: 61
currently lose_sum: 99.1485658288002
time_elpased: 1.734
batch start
#iterations: 62
currently lose_sum: 99.13124257326126
time_elpased: 1.774
batch start
#iterations: 63
currently lose_sum: 99.20366948843002
time_elpased: 1.736
batch start
#iterations: 64
currently lose_sum: 99.27663224935532
time_elpased: 1.74
batch start
#iterations: 65
currently lose_sum: 98.94831055402756
time_elpased: 1.728
batch start
#iterations: 66
currently lose_sum: 99.17428779602051
time_elpased: 1.775
batch start
#iterations: 67
currently lose_sum: 99.04172444343567
time_elpased: 1.727
batch start
#iterations: 68
currently lose_sum: 98.9971336722374
time_elpased: 1.746
batch start
#iterations: 69
currently lose_sum: 99.19596493244171
time_elpased: 1.754
batch start
#iterations: 70
currently lose_sum: 99.03460961580276
time_elpased: 1.852
batch start
#iterations: 71
currently lose_sum: 98.96418637037277
time_elpased: 1.736
batch start
#iterations: 72
currently lose_sum: 98.98994904756546
time_elpased: 1.721
batch start
#iterations: 73
currently lose_sum: 98.97804856300354
time_elpased: 1.749
batch start
#iterations: 74
currently lose_sum: 98.91499245166779
time_elpased: 1.723
batch start
#iterations: 75
currently lose_sum: 99.15830338001251
time_elpased: 1.749
batch start
#iterations: 76
currently lose_sum: 98.88046991825104
time_elpased: 1.755
batch start
#iterations: 77
currently lose_sum: 98.91229432821274
time_elpased: 1.719
batch start
#iterations: 78
currently lose_sum: 99.12131762504578
time_elpased: 1.756
batch start
#iterations: 79
currently lose_sum: 99.02739065885544
time_elpased: 1.747
start validation test
0.61
0.634825122226
0.521148502624
0.572397422855
0.610155992508
64.832
batch start
#iterations: 80
currently lose_sum: 98.95815026760101
time_elpased: 1.723
batch start
#iterations: 81
currently lose_sum: 99.00240010023117
time_elpased: 1.782
batch start
#iterations: 82
currently lose_sum: 99.0354453921318
time_elpased: 1.76
batch start
#iterations: 83
currently lose_sum: 98.80499815940857
time_elpased: 1.712
batch start
#iterations: 84
currently lose_sum: 99.02190846204758
time_elpased: 1.764
batch start
#iterations: 85
currently lose_sum: 99.07713824510574
time_elpased: 1.728
batch start
#iterations: 86
currently lose_sum: 99.102519094944
time_elpased: 1.807
batch start
#iterations: 87
currently lose_sum: 98.9320764541626
time_elpased: 1.727
batch start
#iterations: 88
currently lose_sum: 98.81808567047119
time_elpased: 1.738
batch start
#iterations: 89
currently lose_sum: 99.17913454771042
time_elpased: 1.764
batch start
#iterations: 90
currently lose_sum: 98.72281712293625
time_elpased: 1.753
batch start
#iterations: 91
currently lose_sum: 99.10594606399536
time_elpased: 1.742
batch start
#iterations: 92
currently lose_sum: 98.93268299102783
time_elpased: 1.763
batch start
#iterations: 93
currently lose_sum: 98.85528188943863
time_elpased: 1.824
batch start
#iterations: 94
currently lose_sum: 98.9627560377121
time_elpased: 1.834
batch start
#iterations: 95
currently lose_sum: 99.02466142177582
time_elpased: 1.719
batch start
#iterations: 96
currently lose_sum: 99.17138206958771
time_elpased: 1.755
batch start
#iterations: 97
currently lose_sum: 98.99041622877121
time_elpased: 1.725
batch start
#iterations: 98
currently lose_sum: 99.097128033638
time_elpased: 1.717
batch start
#iterations: 99
currently lose_sum: 99.02086424827576
time_elpased: 1.712
start validation test
0.607628865979
0.627036813518
0.534630029845
0.577158093545
0.607757026696
64.749
batch start
#iterations: 100
currently lose_sum: 98.96840792894363
time_elpased: 1.777
batch start
#iterations: 101
currently lose_sum: 98.91780734062195
time_elpased: 1.72
batch start
#iterations: 102
currently lose_sum: 98.80294543504715
time_elpased: 1.732
batch start
#iterations: 103
currently lose_sum: 99.00269788503647
time_elpased: 1.705
batch start
#iterations: 104
currently lose_sum: 98.75813680887222
time_elpased: 1.753
batch start
#iterations: 105
currently lose_sum: 98.82406359910965
time_elpased: 1.73
batch start
#iterations: 106
currently lose_sum: 98.98449373245239
time_elpased: 1.792
batch start
#iterations: 107
currently lose_sum: 99.01875150203705
time_elpased: 1.798
batch start
#iterations: 108
currently lose_sum: 99.17939686775208
time_elpased: 1.789
batch start
#iterations: 109
currently lose_sum: 98.82553654909134
time_elpased: 1.744
batch start
#iterations: 110
currently lose_sum: 98.91885191202164
time_elpased: 1.749
batch start
#iterations: 111
currently lose_sum: 99.30263870954514
time_elpased: 1.761
batch start
#iterations: 112
currently lose_sum: 98.96853595972061
time_elpased: 1.81
batch start
#iterations: 113
currently lose_sum: 98.7881868481636
time_elpased: 1.756
batch start
#iterations: 114
currently lose_sum: 99.10249584913254
time_elpased: 1.742
batch start
#iterations: 115
currently lose_sum: 98.81642550230026
time_elpased: 1.704
batch start
#iterations: 116
currently lose_sum: 98.96013402938843
time_elpased: 1.724
batch start
#iterations: 117
currently lose_sum: 98.79108852148056
time_elpased: 1.749
batch start
#iterations: 118
currently lose_sum: 98.88554745912552
time_elpased: 1.779
batch start
#iterations: 119
currently lose_sum: 98.92517632246017
time_elpased: 1.926
start validation test
0.601134020619
0.631915744567
0.487804878049
0.550586595423
0.601332987408
64.846
batch start
#iterations: 120
currently lose_sum: 98.96957617998123
time_elpased: 1.828
batch start
#iterations: 121
currently lose_sum: 99.03273123502731
time_elpased: 1.825
batch start
#iterations: 122
currently lose_sum: 99.03333282470703
time_elpased: 1.749
batch start
#iterations: 123
currently lose_sum: 98.82437074184418
time_elpased: 1.745
batch start
#iterations: 124
currently lose_sum: 98.9483505487442
time_elpased: 1.809
batch start
#iterations: 125
currently lose_sum: 98.73783528804779
time_elpased: 1.72
batch start
#iterations: 126
currently lose_sum: 98.945925116539
time_elpased: 1.729
batch start
#iterations: 127
currently lose_sum: 98.84751176834106
time_elpased: 1.736
batch start
#iterations: 128
currently lose_sum: 98.79251343011856
time_elpased: 1.755
batch start
#iterations: 129
currently lose_sum: 98.84615224599838
time_elpased: 1.7
batch start
#iterations: 130
currently lose_sum: 98.79687315225601
time_elpased: 1.698
batch start
#iterations: 131
currently lose_sum: 98.97985917329788
time_elpased: 1.743
batch start
#iterations: 132
currently lose_sum: 99.0918562412262
time_elpased: 1.731
batch start
#iterations: 133
currently lose_sum: 98.76879501342773
time_elpased: 1.745
batch start
#iterations: 134
currently lose_sum: 98.80466991662979
time_elpased: 1.822
batch start
#iterations: 135
currently lose_sum: 98.70122188329697
time_elpased: 1.778
batch start
#iterations: 136
currently lose_sum: 98.86060863733292
time_elpased: 1.755
batch start
#iterations: 137
currently lose_sum: 98.99514454603195
time_elpased: 1.734
batch start
#iterations: 138
currently lose_sum: 98.99115151166916
time_elpased: 1.75
batch start
#iterations: 139
currently lose_sum: 98.64997392892838
time_elpased: 1.723
start validation test
0.604845360825
0.639923591213
0.482659256972
0.55027572451
0.605059877376
64.861
batch start
#iterations: 140
currently lose_sum: 98.92421251535416
time_elpased: 1.78
batch start
#iterations: 141
currently lose_sum: 98.88968968391418
time_elpased: 1.732
batch start
#iterations: 142
currently lose_sum: 99.11432832479477
time_elpased: 1.734
batch start
#iterations: 143
currently lose_sum: 98.8782029747963
time_elpased: 1.755
batch start
#iterations: 144
currently lose_sum: 98.74300020933151
time_elpased: 1.747
batch start
#iterations: 145
currently lose_sum: 98.96991181373596
time_elpased: 1.734
batch start
#iterations: 146
currently lose_sum: 98.76512598991394
time_elpased: 1.721
batch start
#iterations: 147
currently lose_sum: 98.68111044168472
time_elpased: 1.774
batch start
#iterations: 148
currently lose_sum: 98.74636399745941
time_elpased: 1.748
batch start
#iterations: 149
currently lose_sum: 98.9581663608551
time_elpased: 1.773
batch start
#iterations: 150
currently lose_sum: 98.80793708562851
time_elpased: 1.77
batch start
#iterations: 151
currently lose_sum: 98.65090763568878
time_elpased: 1.758
batch start
#iterations: 152
currently lose_sum: 98.56061667203903
time_elpased: 1.76
batch start
#iterations: 153
currently lose_sum: 98.89458805322647
time_elpased: 1.768
batch start
#iterations: 154
currently lose_sum: 98.79042488336563
time_elpased: 1.782
batch start
#iterations: 155
currently lose_sum: 98.5856065750122
time_elpased: 1.715
batch start
#iterations: 156
currently lose_sum: 99.11335641145706
time_elpased: 1.701
batch start
#iterations: 157
currently lose_sum: 98.91430449485779
time_elpased: 1.734
batch start
#iterations: 158
currently lose_sum: 98.70597279071808
time_elpased: 1.746
batch start
#iterations: 159
currently lose_sum: 98.91496831178665
time_elpased: 1.854
start validation test
0.607577319588
0.640980970249
0.492230112175
0.556842656732
0.607779829401
64.805
batch start
#iterations: 160
currently lose_sum: 98.78600978851318
time_elpased: 1.775
batch start
#iterations: 161
currently lose_sum: 98.83598738908768
time_elpased: 1.792
batch start
#iterations: 162
currently lose_sum: 98.76269555091858
time_elpased: 1.761
batch start
#iterations: 163
currently lose_sum: 98.84946322441101
time_elpased: 1.753
batch start
#iterations: 164
currently lose_sum: 98.93939983844757
time_elpased: 1.751
batch start
#iterations: 165
currently lose_sum: 98.71794319152832
time_elpased: 1.808
batch start
#iterations: 166
currently lose_sum: 98.8982315659523
time_elpased: 1.737
batch start
#iterations: 167
currently lose_sum: 99.11976563930511
time_elpased: 1.826
batch start
#iterations: 168
currently lose_sum: 98.88589215278625
time_elpased: 1.75
batch start
#iterations: 169
currently lose_sum: 98.86350011825562
time_elpased: 1.8
batch start
#iterations: 170
currently lose_sum: 98.62315219640732
time_elpased: 1.756
batch start
#iterations: 171
currently lose_sum: 98.88494008779526
time_elpased: 1.812
batch start
#iterations: 172
currently lose_sum: 98.66320431232452
time_elpased: 1.972
batch start
#iterations: 173
currently lose_sum: 98.61548233032227
time_elpased: 1.798
batch start
#iterations: 174
currently lose_sum: 98.81521087884903
time_elpased: 1.735
batch start
#iterations: 175
currently lose_sum: 98.70170569419861
time_elpased: 1.794
batch start
#iterations: 176
currently lose_sum: 98.90075469017029
time_elpased: 1.8
batch start
#iterations: 177
currently lose_sum: 98.82104539871216
time_elpased: 1.751
batch start
#iterations: 178
currently lose_sum: 98.69627666473389
time_elpased: 1.791
batch start
#iterations: 179
currently lose_sum: 98.48125940561295
time_elpased: 1.784
start validation test
0.606855670103
0.642138193689
0.48584954204
0.553166559259
0.607068115025
64.844
batch start
#iterations: 180
currently lose_sum: 98.73192548751831
time_elpased: 1.769
batch start
#iterations: 181
currently lose_sum: 98.88313031196594
time_elpased: 1.75
batch start
#iterations: 182
currently lose_sum: 98.7846947312355
time_elpased: 1.792
batch start
#iterations: 183
currently lose_sum: 98.66379672288895
time_elpased: 1.779
batch start
#iterations: 184
currently lose_sum: 98.77912771701813
time_elpased: 1.784
batch start
#iterations: 185
currently lose_sum: 98.76900935173035
time_elpased: 1.788
batch start
#iterations: 186
currently lose_sum: 98.73977482318878
time_elpased: 1.764
batch start
#iterations: 187
currently lose_sum: 98.86165302991867
time_elpased: 1.765
batch start
#iterations: 188
currently lose_sum: 98.74497944116592
time_elpased: 1.752
batch start
#iterations: 189
currently lose_sum: 99.00844955444336
time_elpased: 1.758
batch start
#iterations: 190
currently lose_sum: 98.71039581298828
time_elpased: 1.827
batch start
#iterations: 191
currently lose_sum: 98.64386743307114
time_elpased: 1.74
batch start
#iterations: 192
currently lose_sum: 98.76055926084518
time_elpased: 1.766
batch start
#iterations: 193
currently lose_sum: 98.73169195652008
time_elpased: 1.745
batch start
#iterations: 194
currently lose_sum: 98.86308151483536
time_elpased: 1.829
batch start
#iterations: 195
currently lose_sum: 98.846994638443
time_elpased: 1.81
batch start
#iterations: 196
currently lose_sum: 98.88926321268082
time_elpased: 1.804
batch start
#iterations: 197
currently lose_sum: 98.90687519311905
time_elpased: 1.807
batch start
#iterations: 198
currently lose_sum: 98.82419866323471
time_elpased: 1.838
batch start
#iterations: 199
currently lose_sum: 98.70884764194489
time_elpased: 1.718
start validation test
0.606494845361
0.645807083858
0.474735000515
0.547212336892
0.606726170091
64.848
batch start
#iterations: 200
currently lose_sum: 98.6030945777893
time_elpased: 1.743
batch start
#iterations: 201
currently lose_sum: 98.82771182060242
time_elpased: 1.747
batch start
#iterations: 202
currently lose_sum: 98.72368347644806
time_elpased: 1.761
batch start
#iterations: 203
currently lose_sum: 99.0354129076004
time_elpased: 1.745
batch start
#iterations: 204
currently lose_sum: 98.77265614271164
time_elpased: 1.755
batch start
#iterations: 205
currently lose_sum: 98.55316334962845
time_elpased: 1.755
batch start
#iterations: 206
currently lose_sum: 98.87548595666885
time_elpased: 1.765
batch start
#iterations: 207
currently lose_sum: 98.74282157421112
time_elpased: 1.81
batch start
#iterations: 208
currently lose_sum: 98.59084761142731
time_elpased: 1.74
batch start
#iterations: 209
currently lose_sum: 98.8135855793953
time_elpased: 1.768
batch start
#iterations: 210
currently lose_sum: 98.68436652421951
time_elpased: 1.728
batch start
#iterations: 211
currently lose_sum: 98.66186881065369
time_elpased: 1.776
batch start
#iterations: 212
currently lose_sum: 98.72904586791992
time_elpased: 1.733
batch start
#iterations: 213
currently lose_sum: 98.62661463022232
time_elpased: 1.741
batch start
#iterations: 214
currently lose_sum: 98.77633607387543
time_elpased: 1.78
batch start
#iterations: 215
currently lose_sum: 98.53102946281433
time_elpased: 1.763
batch start
#iterations: 216
currently lose_sum: 98.71106088161469
time_elpased: 1.761
batch start
#iterations: 217
currently lose_sum: 98.74554216861725
time_elpased: 1.772
batch start
#iterations: 218
currently lose_sum: 98.59318119287491
time_elpased: 1.745
batch start
#iterations: 219
currently lose_sum: 98.4672019481659
time_elpased: 1.793
start validation test
0.609742268041
0.641818662437
0.499742718946
0.5619394781
0.609935389216
64.703
batch start
#iterations: 220
currently lose_sum: 98.58430808782578
time_elpased: 1.742
batch start
#iterations: 221
currently lose_sum: 98.64946699142456
time_elpased: 1.768
batch start
#iterations: 222
currently lose_sum: 98.710255920887
time_elpased: 1.725
batch start
#iterations: 223
currently lose_sum: 98.77600610256195
time_elpased: 1.785
batch start
#iterations: 224
currently lose_sum: 98.94313645362854
time_elpased: 1.738
batch start
#iterations: 225
currently lose_sum: 98.65770781040192
time_elpased: 1.794
batch start
#iterations: 226
currently lose_sum: 98.58511275053024
time_elpased: 1.778
batch start
#iterations: 227
currently lose_sum: 98.49514943361282
time_elpased: 1.756
batch start
#iterations: 228
currently lose_sum: 98.7207909822464
time_elpased: 1.736
batch start
#iterations: 229
currently lose_sum: 98.8754951953888
time_elpased: 1.729
batch start
#iterations: 230
currently lose_sum: 98.76308339834213
time_elpased: 1.76
batch start
#iterations: 231
currently lose_sum: 98.89459639787674
time_elpased: 1.727
batch start
#iterations: 232
currently lose_sum: 98.69918060302734
time_elpased: 1.75
batch start
#iterations: 233
currently lose_sum: 98.74923431873322
time_elpased: 1.721
batch start
#iterations: 234
currently lose_sum: 98.61914092302322
time_elpased: 1.772
batch start
#iterations: 235
currently lose_sum: 98.64691466093063
time_elpased: 1.771
batch start
#iterations: 236
currently lose_sum: 98.54173094034195
time_elpased: 1.789
batch start
#iterations: 237
currently lose_sum: 98.82204699516296
time_elpased: 1.742
batch start
#iterations: 238
currently lose_sum: 98.68653571605682
time_elpased: 1.766
batch start
#iterations: 239
currently lose_sum: 98.44437164068222
time_elpased: 1.765
start validation test
0.607268041237
0.635459710744
0.506432026346
0.563656147987
0.607445074414
64.576
batch start
#iterations: 240
currently lose_sum: 98.76536679267883
time_elpased: 1.849
batch start
#iterations: 241
currently lose_sum: 98.7783899307251
time_elpased: 1.859
batch start
#iterations: 242
currently lose_sum: 98.56932216882706
time_elpased: 1.843
batch start
#iterations: 243
currently lose_sum: 98.71605581045151
time_elpased: 1.831
batch start
#iterations: 244
currently lose_sum: 98.67389345169067
time_elpased: 1.792
batch start
#iterations: 245
currently lose_sum: 98.68000543117523
time_elpased: 1.859
batch start
#iterations: 246
currently lose_sum: 98.70835363864899
time_elpased: 1.877
batch start
#iterations: 247
currently lose_sum: 98.7530208826065
time_elpased: 1.841
batch start
#iterations: 248
currently lose_sum: 98.82409828901291
time_elpased: 1.807
batch start
#iterations: 249
currently lose_sum: 98.59192258119583
time_elpased: 1.773
batch start
#iterations: 250
currently lose_sum: 98.70319449901581
time_elpased: 1.8
batch start
#iterations: 251
currently lose_sum: 98.82045274972916
time_elpased: 1.842
batch start
#iterations: 252
currently lose_sum: 98.64920151233673
time_elpased: 1.804
batch start
#iterations: 253
currently lose_sum: 98.66239130496979
time_elpased: 1.791
batch start
#iterations: 254
currently lose_sum: 98.57147866487503
time_elpased: 1.815
batch start
#iterations: 255
currently lose_sum: 98.62151730060577
time_elpased: 1.837
batch start
#iterations: 256
currently lose_sum: 98.39024931192398
time_elpased: 1.752
batch start
#iterations: 257
currently lose_sum: 98.83568024635315
time_elpased: 1.774
batch start
#iterations: 258
currently lose_sum: 98.75568038225174
time_elpased: 1.748
batch start
#iterations: 259
currently lose_sum: 98.49725842475891
time_elpased: 1.742
start validation test
0.606546391753
0.635500650195
0.502933004014
0.561498247831
0.606728301036
64.826
batch start
#iterations: 260
currently lose_sum: 98.64763414859772
time_elpased: 1.806
batch start
#iterations: 261
currently lose_sum: 98.68209117650986
time_elpased: 1.808
batch start
#iterations: 262
currently lose_sum: 98.5840094089508
time_elpased: 1.761
batch start
#iterations: 263
currently lose_sum: 98.6174721121788
time_elpased: 1.751
batch start
#iterations: 264
currently lose_sum: 98.71445494890213
time_elpased: 1.74
batch start
#iterations: 265
currently lose_sum: 98.83832681179047
time_elpased: 1.788
batch start
#iterations: 266
currently lose_sum: 98.52718603610992
time_elpased: 1.79
batch start
#iterations: 267
currently lose_sum: 98.70693010091782
time_elpased: 1.749
batch start
#iterations: 268
currently lose_sum: 98.4065210223198
time_elpased: 1.752
batch start
#iterations: 269
currently lose_sum: 98.80602079629898
time_elpased: 1.744
batch start
#iterations: 270
currently lose_sum: 98.68580842018127
time_elpased: 1.782
batch start
#iterations: 271
currently lose_sum: 98.47713601589203
time_elpased: 1.793
batch start
#iterations: 272
currently lose_sum: 98.8686910867691
time_elpased: 1.781
batch start
#iterations: 273
currently lose_sum: 98.7999318242073
time_elpased: 1.774
batch start
#iterations: 274
currently lose_sum: 98.62906801700592
time_elpased: 1.786
batch start
#iterations: 275
currently lose_sum: 98.74481916427612
time_elpased: 1.876
batch start
#iterations: 276
currently lose_sum: 98.78245556354523
time_elpased: 1.775
batch start
#iterations: 277
currently lose_sum: 98.80699783563614
time_elpased: 1.834
batch start
#iterations: 278
currently lose_sum: 98.6873499751091
time_elpased: 1.845
batch start
#iterations: 279
currently lose_sum: 98.53615689277649
time_elpased: 1.832
start validation test
0.610257731959
0.63505387121
0.521663064732
0.572800723205
0.610413273562
64.590
batch start
#iterations: 280
currently lose_sum: 98.74123638868332
time_elpased: 1.776
batch start
#iterations: 281
currently lose_sum: 98.69682788848877
time_elpased: 1.822
batch start
#iterations: 282
currently lose_sum: 98.64634442329407
time_elpased: 1.752
batch start
#iterations: 283
currently lose_sum: 98.50865244865417
time_elpased: 1.808
batch start
#iterations: 284
currently lose_sum: 98.71475487947464
time_elpased: 1.797
batch start
#iterations: 285
currently lose_sum: 98.66052156686783
time_elpased: 1.8
batch start
#iterations: 286
currently lose_sum: 98.4723613858223
time_elpased: 1.757
batch start
#iterations: 287
currently lose_sum: 98.57537293434143
time_elpased: 1.768
batch start
#iterations: 288
currently lose_sum: 98.62581646442413
time_elpased: 1.761
batch start
#iterations: 289
currently lose_sum: 98.60111337900162
time_elpased: 1.766
batch start
#iterations: 290
currently lose_sum: 98.45794641971588
time_elpased: 1.715
batch start
#iterations: 291
currently lose_sum: 98.32212871313095
time_elpased: 1.761
batch start
#iterations: 292
currently lose_sum: 98.68999487161636
time_elpased: 1.766
batch start
#iterations: 293
currently lose_sum: 98.640889108181
time_elpased: 1.761
batch start
#iterations: 294
currently lose_sum: 98.55581587553024
time_elpased: 1.743
batch start
#iterations: 295
currently lose_sum: 98.61702185869217
time_elpased: 1.761
batch start
#iterations: 296
currently lose_sum: 98.43103981018066
time_elpased: 1.749
batch start
#iterations: 297
currently lose_sum: 98.7853701710701
time_elpased: 1.748
batch start
#iterations: 298
currently lose_sum: 98.65110212564468
time_elpased: 1.804
batch start
#iterations: 299
currently lose_sum: 98.44709819555283
time_elpased: 1.739
start validation test
0.610206185567
0.63634063014
0.517546567871
0.570828603859
0.610368863818
64.612
batch start
#iterations: 300
currently lose_sum: 98.35321640968323
time_elpased: 1.757
batch start
#iterations: 301
currently lose_sum: 98.41738641262054
time_elpased: 1.848
batch start
#iterations: 302
currently lose_sum: 98.62939727306366
time_elpased: 1.816
batch start
#iterations: 303
currently lose_sum: 98.46476554870605
time_elpased: 1.762
batch start
#iterations: 304
currently lose_sum: 98.60292166471481
time_elpased: 1.75
batch start
#iterations: 305
currently lose_sum: 98.66385734081268
time_elpased: 1.75
batch start
#iterations: 306
currently lose_sum: 98.58084607124329
time_elpased: 1.794
batch start
#iterations: 307
currently lose_sum: 98.54949003458023
time_elpased: 1.796
batch start
#iterations: 308
currently lose_sum: 98.77402877807617
time_elpased: 1.751
batch start
#iterations: 309
currently lose_sum: 98.73994034528732
time_elpased: 1.822
batch start
#iterations: 310
currently lose_sum: 98.75578159093857
time_elpased: 1.799
batch start
#iterations: 311
currently lose_sum: 98.43326836824417
time_elpased: 1.76
batch start
#iterations: 312
currently lose_sum: 98.71027201414108
time_elpased: 1.784
batch start
#iterations: 313
currently lose_sum: 98.52038711309433
time_elpased: 1.843
batch start
#iterations: 314
currently lose_sum: 98.6220390200615
time_elpased: 1.77
batch start
#iterations: 315
currently lose_sum: 98.41029393672943
time_elpased: 1.736
batch start
#iterations: 316
currently lose_sum: 98.53764623403549
time_elpased: 1.818
batch start
#iterations: 317
currently lose_sum: 98.38814228773117
time_elpased: 1.789
batch start
#iterations: 318
currently lose_sum: 98.55684036016464
time_elpased: 1.762
batch start
#iterations: 319
currently lose_sum: 98.55477052927017
time_elpased: 1.78
start validation test
0.60881443299
0.642703862661
0.493156323968
0.558085366564
0.609017488639
64.721
batch start
#iterations: 320
currently lose_sum: 98.7217703461647
time_elpased: 1.742
batch start
#iterations: 321
currently lose_sum: 98.73388910293579
time_elpased: 1.753
batch start
#iterations: 322
currently lose_sum: 98.81594336032867
time_elpased: 1.768
batch start
#iterations: 323
currently lose_sum: 98.39234626293182
time_elpased: 1.752
batch start
#iterations: 324
currently lose_sum: 98.52268236875534
time_elpased: 1.794
batch start
#iterations: 325
currently lose_sum: 98.53916418552399
time_elpased: 1.762
batch start
#iterations: 326
currently lose_sum: 98.68171375989914
time_elpased: 1.78
batch start
#iterations: 327
currently lose_sum: 98.65220242738724
time_elpased: 1.756
batch start
#iterations: 328
currently lose_sum: 98.5530691742897
time_elpased: 1.763
batch start
#iterations: 329
currently lose_sum: 98.6407989859581
time_elpased: 1.74
batch start
#iterations: 330
currently lose_sum: 98.39682674407959
time_elpased: 1.735
batch start
#iterations: 331
currently lose_sum: 98.67683666944504
time_elpased: 1.74
batch start
#iterations: 332
currently lose_sum: 98.38168317079544
time_elpased: 1.774
batch start
#iterations: 333
currently lose_sum: 98.574731528759
time_elpased: 1.771
batch start
#iterations: 334
currently lose_sum: 98.83156079053879
time_elpased: 1.751
batch start
#iterations: 335
currently lose_sum: 98.44965612888336
time_elpased: 1.742
batch start
#iterations: 336
currently lose_sum: 98.54965656995773
time_elpased: 1.777
batch start
#iterations: 337
currently lose_sum: 98.68551021814346
time_elpased: 1.741
batch start
#iterations: 338
currently lose_sum: 98.60673540830612
time_elpased: 1.733
batch start
#iterations: 339
currently lose_sum: 98.53996181488037
time_elpased: 1.826
start validation test
0.60881443299
0.636972193615
0.509210661727
0.565970832142
0.608989302773
64.670
batch start
#iterations: 340
currently lose_sum: 98.45824432373047
time_elpased: 1.764
batch start
#iterations: 341
currently lose_sum: 98.86481362581253
time_elpased: 1.763
batch start
#iterations: 342
currently lose_sum: 98.62056344747543
time_elpased: 1.762
batch start
#iterations: 343
currently lose_sum: 98.53317034244537
time_elpased: 1.754
batch start
#iterations: 344
currently lose_sum: 98.57974314689636
time_elpased: 1.756
batch start
#iterations: 345
currently lose_sum: 98.51188683509827
time_elpased: 1.745
batch start
#iterations: 346
currently lose_sum: 98.59088915586472
time_elpased: 1.759
batch start
#iterations: 347
currently lose_sum: 98.45912975072861
time_elpased: 1.803
batch start
#iterations: 348
currently lose_sum: 98.6456533074379
time_elpased: 1.811
batch start
#iterations: 349
currently lose_sum: 98.57556933164597
time_elpased: 1.745
batch start
#iterations: 350
currently lose_sum: 98.67995148897171
time_elpased: 1.743
batch start
#iterations: 351
currently lose_sum: 98.50333172082901
time_elpased: 1.761
batch start
#iterations: 352
currently lose_sum: 98.4065243601799
time_elpased: 1.821
batch start
#iterations: 353
currently lose_sum: 98.63932609558105
time_elpased: 1.763
batch start
#iterations: 354
currently lose_sum: 98.61631894111633
time_elpased: 1.806
batch start
#iterations: 355
currently lose_sum: 98.46772515773773
time_elpased: 1.761
batch start
#iterations: 356
currently lose_sum: 98.50883281230927
time_elpased: 1.747
batch start
#iterations: 357
currently lose_sum: 98.48189318180084
time_elpased: 1.798
batch start
#iterations: 358
currently lose_sum: 98.39067697525024
time_elpased: 1.751
batch start
#iterations: 359
currently lose_sum: 98.3604028224945
time_elpased: 1.801
start validation test
0.608350515464
0.635607321131
0.511063085314
0.566571591557
0.608521318553
64.607
batch start
#iterations: 360
currently lose_sum: 98.6028504371643
time_elpased: 1.832
batch start
#iterations: 361
currently lose_sum: 98.6298657655716
time_elpased: 1.759
batch start
#iterations: 362
currently lose_sum: 98.67150342464447
time_elpased: 1.762
batch start
#iterations: 363
currently lose_sum: 98.48837316036224
time_elpased: 1.726
batch start
#iterations: 364
currently lose_sum: 98.50545805692673
time_elpased: 1.739
batch start
#iterations: 365
currently lose_sum: 98.42676901817322
time_elpased: 1.759
batch start
#iterations: 366
currently lose_sum: 98.42036962509155
time_elpased: 1.756
batch start
#iterations: 367
currently lose_sum: 98.4564425945282
time_elpased: 1.772
batch start
#iterations: 368
currently lose_sum: 98.54219245910645
time_elpased: 1.741
batch start
#iterations: 369
currently lose_sum: 98.61966943740845
time_elpased: 1.818
batch start
#iterations: 370
currently lose_sum: 98.40903204679489
time_elpased: 1.745
batch start
#iterations: 371
currently lose_sum: 98.32961523532867
time_elpased: 1.75
batch start
#iterations: 372
currently lose_sum: 98.52729070186615
time_elpased: 1.787
batch start
#iterations: 373
currently lose_sum: 98.54939550161362
time_elpased: 1.799
batch start
#iterations: 374
currently lose_sum: 98.54911720752716
time_elpased: 1.853
batch start
#iterations: 375
currently lose_sum: 98.3983496427536
time_elpased: 1.788
batch start
#iterations: 376
currently lose_sum: 98.60331666469574
time_elpased: 1.766
batch start
#iterations: 377
currently lose_sum: 98.58622699975967
time_elpased: 1.776
batch start
#iterations: 378
currently lose_sum: 98.6698579788208
time_elpased: 1.767
batch start
#iterations: 379
currently lose_sum: 98.44690948724747
time_elpased: 1.773
start validation test
0.60618556701
0.636806744829
0.497478645673
0.558585625144
0.606376418778
64.780
batch start
#iterations: 380
currently lose_sum: 98.89023983478546
time_elpased: 1.801
batch start
#iterations: 381
currently lose_sum: 98.81524121761322
time_elpased: 1.759
batch start
#iterations: 382
currently lose_sum: 98.47195374965668
time_elpased: 1.791
batch start
#iterations: 383
currently lose_sum: 98.46956098079681
time_elpased: 1.771
batch start
#iterations: 384
currently lose_sum: 98.44631832838058
time_elpased: 1.77
batch start
#iterations: 385
currently lose_sum: 98.76313751935959
time_elpased: 1.815
batch start
#iterations: 386
currently lose_sum: 98.66879189014435
time_elpased: 1.829
batch start
#iterations: 387
currently lose_sum: 98.49205768108368
time_elpased: 1.834
batch start
#iterations: 388
currently lose_sum: 98.71217972040176
time_elpased: 1.778
batch start
#iterations: 389
currently lose_sum: 98.4573490023613
time_elpased: 1.797
batch start
#iterations: 390
currently lose_sum: 98.51155626773834
time_elpased: 1.764
batch start
#iterations: 391
currently lose_sum: 98.57750511169434
time_elpased: 1.797
batch start
#iterations: 392
currently lose_sum: 98.56247401237488
time_elpased: 1.842
batch start
#iterations: 393
currently lose_sum: 98.67277950048447
time_elpased: 1.785
batch start
#iterations: 394
currently lose_sum: 98.44874602556229
time_elpased: 1.819
batch start
#iterations: 395
currently lose_sum: 98.26903873682022
time_elpased: 1.768
batch start
#iterations: 396
currently lose_sum: 98.41808980703354
time_elpased: 1.801
batch start
#iterations: 397
currently lose_sum: 98.5110250711441
time_elpased: 1.82
batch start
#iterations: 398
currently lose_sum: 98.32564878463745
time_elpased: 1.754
batch start
#iterations: 399
currently lose_sum: 98.30970633029938
time_elpased: 1.791
start validation test
0.604793814433
0.630873340143
0.508490274776
0.56310900906
0.604962890151
64.641
acc: 0.605
pre: 0.634
rec: 0.502
F1: 0.560
auc: 0.606
