start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.65904897451401
time_elpased: 1.893
batch start
#iterations: 1
currently lose_sum: 100.43198716640472
time_elpased: 1.799
batch start
#iterations: 2
currently lose_sum: 100.40223151445389
time_elpased: 1.862
batch start
#iterations: 3
currently lose_sum: 100.22825247049332
time_elpased: 1.789
batch start
#iterations: 4
currently lose_sum: 100.39082223176956
time_elpased: 1.805
batch start
#iterations: 5
currently lose_sum: 100.14178615808487
time_elpased: 1.853
batch start
#iterations: 6
currently lose_sum: 100.0577734708786
time_elpased: 1.801
batch start
#iterations: 7
currently lose_sum: 100.0529021024704
time_elpased: 1.82
batch start
#iterations: 8
currently lose_sum: 100.1545757651329
time_elpased: 1.8
batch start
#iterations: 9
currently lose_sum: 99.85840845108032
time_elpased: 1.805
batch start
#iterations: 10
currently lose_sum: 99.94065082073212
time_elpased: 1.808
batch start
#iterations: 11
currently lose_sum: 99.90721195936203
time_elpased: 1.802
batch start
#iterations: 12
currently lose_sum: 99.8941535949707
time_elpased: 1.837
batch start
#iterations: 13
currently lose_sum: 99.8391941189766
time_elpased: 1.83
batch start
#iterations: 14
currently lose_sum: 99.87495362758636
time_elpased: 1.825
batch start
#iterations: 15
currently lose_sum: 99.87461858987808
time_elpased: 1.802
batch start
#iterations: 16
currently lose_sum: 99.74987614154816
time_elpased: 1.8
batch start
#iterations: 17
currently lose_sum: 99.69390189647675
time_elpased: 1.854
batch start
#iterations: 18
currently lose_sum: 99.8344983458519
time_elpased: 1.807
batch start
#iterations: 19
currently lose_sum: 99.72813659906387
time_elpased: 1.889
start validation test
0.587680412371
0.60035046729
0.528866934239
0.562346118072
0.587783668503
65.620
batch start
#iterations: 20
currently lose_sum: 99.71898037195206
time_elpased: 1.811
batch start
#iterations: 21
currently lose_sum: 99.56121861934662
time_elpased: 1.8
batch start
#iterations: 22
currently lose_sum: 99.60508930683136
time_elpased: 1.796
batch start
#iterations: 23
currently lose_sum: 99.52224671840668
time_elpased: 1.827
batch start
#iterations: 24
currently lose_sum: 99.53734111785889
time_elpased: 1.794
batch start
#iterations: 25
currently lose_sum: 99.51355534791946
time_elpased: 1.838
batch start
#iterations: 26
currently lose_sum: 99.53560143709183
time_elpased: 1.783
batch start
#iterations: 27
currently lose_sum: 99.48180955648422
time_elpased: 1.788
batch start
#iterations: 28
currently lose_sum: 99.43183416128159
time_elpased: 1.861
batch start
#iterations: 29
currently lose_sum: 99.50129234790802
time_elpased: 1.843
batch start
#iterations: 30
currently lose_sum: 99.32522314786911
time_elpased: 1.834
batch start
#iterations: 31
currently lose_sum: 99.33713102340698
time_elpased: 1.811
batch start
#iterations: 32
currently lose_sum: 99.46440535783768
time_elpased: 1.797
batch start
#iterations: 33
currently lose_sum: 99.38043409585953
time_elpased: 1.816
batch start
#iterations: 34
currently lose_sum: 99.26146900653839
time_elpased: 1.818
batch start
#iterations: 35
currently lose_sum: 99.38572585582733
time_elpased: 1.814
batch start
#iterations: 36
currently lose_sum: 99.4354317188263
time_elpased: 1.808
batch start
#iterations: 37
currently lose_sum: 99.23518943786621
time_elpased: 1.83
batch start
#iterations: 38
currently lose_sum: 99.12522941827774
time_elpased: 1.816
batch start
#iterations: 39
currently lose_sum: 99.34206837415695
time_elpased: 1.825
start validation test
0.599845360825
0.624522049452
0.504270865493
0.557991231566
0.600013156593
65.274
batch start
#iterations: 40
currently lose_sum: 99.2873620390892
time_elpased: 1.824
batch start
#iterations: 41
currently lose_sum: 99.14888101816177
time_elpased: 1.823
batch start
#iterations: 42
currently lose_sum: 99.18926286697388
time_elpased: 1.803
batch start
#iterations: 43
currently lose_sum: 99.34494644403458
time_elpased: 1.824
batch start
#iterations: 44
currently lose_sum: 99.290312230587
time_elpased: 1.781
batch start
#iterations: 45
currently lose_sum: 99.26267647743225
time_elpased: 1.824
batch start
#iterations: 46
currently lose_sum: 99.2006539106369
time_elpased: 1.802
batch start
#iterations: 47
currently lose_sum: 99.29863584041595
time_elpased: 1.823
batch start
#iterations: 48
currently lose_sum: 99.21265441179276
time_elpased: 1.827
batch start
#iterations: 49
currently lose_sum: 99.21084678173065
time_elpased: 1.812
batch start
#iterations: 50
currently lose_sum: 99.35245633125305
time_elpased: 1.844
batch start
#iterations: 51
currently lose_sum: 99.09325975179672
time_elpased: 1.811
batch start
#iterations: 52
currently lose_sum: 99.26007825136185
time_elpased: 1.859
batch start
#iterations: 53
currently lose_sum: 99.23908507823944
time_elpased: 1.861
batch start
#iterations: 54
currently lose_sum: 99.27571147680283
time_elpased: 1.849
batch start
#iterations: 55
currently lose_sum: 98.99995112419128
time_elpased: 1.79
batch start
#iterations: 56
currently lose_sum: 99.18571209907532
time_elpased: 1.806
batch start
#iterations: 57
currently lose_sum: 99.21849751472473
time_elpased: 1.863
batch start
#iterations: 58
currently lose_sum: 99.09956288337708
time_elpased: 1.818
batch start
#iterations: 59
currently lose_sum: 99.19263517856598
time_elpased: 1.848
start validation test
0.591134020619
0.650785605677
0.396418647731
0.492709132771
0.591475873488
65.543
batch start
#iterations: 60
currently lose_sum: 99.22948879003525
time_elpased: 1.838
batch start
#iterations: 61
currently lose_sum: 99.29996228218079
time_elpased: 1.835
batch start
#iterations: 62
currently lose_sum: 99.22895282506943
time_elpased: 1.802
batch start
#iterations: 63
currently lose_sum: 99.21566796302795
time_elpased: 1.806
batch start
#iterations: 64
currently lose_sum: 99.25008183717728
time_elpased: 1.843
batch start
#iterations: 65
currently lose_sum: 99.01995939016342
time_elpased: 1.805
batch start
#iterations: 66
currently lose_sum: 99.30900555849075
time_elpased: 1.791
batch start
#iterations: 67
currently lose_sum: 99.02815532684326
time_elpased: 1.793
batch start
#iterations: 68
currently lose_sum: 99.12811666727066
time_elpased: 1.777
batch start
#iterations: 69
currently lose_sum: 99.1495012640953
time_elpased: 1.796
batch start
#iterations: 70
currently lose_sum: 98.99074810743332
time_elpased: 1.779
batch start
#iterations: 71
currently lose_sum: 99.0412250161171
time_elpased: 1.824
batch start
#iterations: 72
currently lose_sum: 98.99311339855194
time_elpased: 1.803
batch start
#iterations: 73
currently lose_sum: 98.9398273229599
time_elpased: 1.795
batch start
#iterations: 74
currently lose_sum: 99.03603839874268
time_elpased: 1.795
batch start
#iterations: 75
currently lose_sum: 99.11857372522354
time_elpased: 1.813
batch start
#iterations: 76
currently lose_sum: 98.85961520671844
time_elpased: 1.807
batch start
#iterations: 77
currently lose_sum: 99.28711426258087
time_elpased: 1.801
batch start
#iterations: 78
currently lose_sum: 98.98936420679092
time_elpased: 1.846
batch start
#iterations: 79
currently lose_sum: 99.10192519426346
time_elpased: 1.853
start validation test
0.605721649485
0.62362505978
0.536791190697
0.57695923898
0.605842667537
64.935
batch start
#iterations: 80
currently lose_sum: 99.05239748954773
time_elpased: 1.876
batch start
#iterations: 81
currently lose_sum: 98.90768527984619
time_elpased: 1.819
batch start
#iterations: 82
currently lose_sum: 99.07386702299118
time_elpased: 1.801
batch start
#iterations: 83
currently lose_sum: 98.8005303144455
time_elpased: 1.799
batch start
#iterations: 84
currently lose_sum: 99.31163489818573
time_elpased: 1.819
batch start
#iterations: 85
currently lose_sum: 99.0498543381691
time_elpased: 1.836
batch start
#iterations: 86
currently lose_sum: 99.09884691238403
time_elpased: 1.818
batch start
#iterations: 87
currently lose_sum: 98.92921382188797
time_elpased: 1.812
batch start
#iterations: 88
currently lose_sum: 99.04791188240051
time_elpased: 1.916
batch start
#iterations: 89
currently lose_sum: 99.02457976341248
time_elpased: 1.848
batch start
#iterations: 90
currently lose_sum: 99.0307297706604
time_elpased: 1.821
batch start
#iterations: 91
currently lose_sum: 99.07021600008011
time_elpased: 1.835
batch start
#iterations: 92
currently lose_sum: 99.03379726409912
time_elpased: 1.82
batch start
#iterations: 93
currently lose_sum: 99.04934620857239
time_elpased: 1.814
batch start
#iterations: 94
currently lose_sum: 98.91965925693512
time_elpased: 1.793
batch start
#iterations: 95
currently lose_sum: 99.19701498746872
time_elpased: 1.785
batch start
#iterations: 96
currently lose_sum: 99.00541359186172
time_elpased: 1.858
batch start
#iterations: 97
currently lose_sum: 99.11008310317993
time_elpased: 1.8
batch start
#iterations: 98
currently lose_sum: 99.01799136400223
time_elpased: 1.803
batch start
#iterations: 99
currently lose_sum: 99.12014263868332
time_elpased: 1.835
start validation test
0.605360824742
0.617919670443
0.555727076258
0.585175552666
0.605447964443
64.882
batch start
#iterations: 100
currently lose_sum: 98.83560341596603
time_elpased: 1.869
batch start
#iterations: 101
currently lose_sum: 98.9341750741005
time_elpased: 1.883
batch start
#iterations: 102
currently lose_sum: 99.04978328943253
time_elpased: 1.799
batch start
#iterations: 103
currently lose_sum: 98.9151941537857
time_elpased: 1.838
batch start
#iterations: 104
currently lose_sum: 98.85329860448837
time_elpased: 1.864
batch start
#iterations: 105
currently lose_sum: 98.90061014890671
time_elpased: 1.826
batch start
#iterations: 106
currently lose_sum: 99.02871710062027
time_elpased: 1.798
batch start
#iterations: 107
currently lose_sum: 98.97069251537323
time_elpased: 1.88
batch start
#iterations: 108
currently lose_sum: 99.11369907855988
time_elpased: 1.817
batch start
#iterations: 109
currently lose_sum: 98.99544787406921
time_elpased: 1.818
batch start
#iterations: 110
currently lose_sum: 98.88274097442627
time_elpased: 1.828
batch start
#iterations: 111
currently lose_sum: 99.33855867385864
time_elpased: 1.806
batch start
#iterations: 112
currently lose_sum: 99.01537346839905
time_elpased: 1.822
batch start
#iterations: 113
currently lose_sum: 98.84521049261093
time_elpased: 1.882
batch start
#iterations: 114
currently lose_sum: 99.15901809930801
time_elpased: 1.907
batch start
#iterations: 115
currently lose_sum: 98.9252792596817
time_elpased: 1.819
batch start
#iterations: 116
currently lose_sum: 98.84772765636444
time_elpased: 1.839
batch start
#iterations: 117
currently lose_sum: 98.91981089115143
time_elpased: 1.859
batch start
#iterations: 118
currently lose_sum: 98.91833460330963
time_elpased: 1.804
batch start
#iterations: 119
currently lose_sum: 99.05268466472626
time_elpased: 1.848
start validation test
0.599536082474
0.634456101601
0.472985489349
0.541949177525
0.59975826156
65.046
batch start
#iterations: 120
currently lose_sum: 98.94786757230759
time_elpased: 1.879
batch start
#iterations: 121
currently lose_sum: 99.10614621639252
time_elpased: 1.831
batch start
#iterations: 122
currently lose_sum: 99.070421397686
time_elpased: 1.905
batch start
#iterations: 123
currently lose_sum: 98.88410198688507
time_elpased: 1.833
batch start
#iterations: 124
currently lose_sum: 98.86793977022171
time_elpased: 1.857
batch start
#iterations: 125
currently lose_sum: 98.85803920030594
time_elpased: 1.798
batch start
#iterations: 126
currently lose_sum: 98.9297890663147
time_elpased: 1.824
batch start
#iterations: 127
currently lose_sum: 98.73963141441345
time_elpased: 1.827
batch start
#iterations: 128
currently lose_sum: 98.93445074558258
time_elpased: 1.799
batch start
#iterations: 129
currently lose_sum: 98.83333104848862
time_elpased: 1.838
batch start
#iterations: 130
currently lose_sum: 98.8867576122284
time_elpased: 1.836
batch start
#iterations: 131
currently lose_sum: 99.03726696968079
time_elpased: 1.828
batch start
#iterations: 132
currently lose_sum: 99.16242390871048
time_elpased: 1.831
batch start
#iterations: 133
currently lose_sum: 98.6671491265297
time_elpased: 1.821
batch start
#iterations: 134
currently lose_sum: 98.92838352918625
time_elpased: 1.844
batch start
#iterations: 135
currently lose_sum: 98.76773643493652
time_elpased: 1.799
batch start
#iterations: 136
currently lose_sum: 98.96783941984177
time_elpased: 1.817
batch start
#iterations: 137
currently lose_sum: 99.04593187570572
time_elpased: 1.849
batch start
#iterations: 138
currently lose_sum: 98.96179604530334
time_elpased: 1.839
batch start
#iterations: 139
currently lose_sum: 98.78565084934235
time_elpased: 1.798
start validation test
0.605154639175
0.633554083885
0.502109704641
0.560225054541
0.605335550451
64.921
batch start
#iterations: 140
currently lose_sum: 98.88203173875809
time_elpased: 1.812
batch start
#iterations: 141
currently lose_sum: 99.09930735826492
time_elpased: 1.795
batch start
#iterations: 142
currently lose_sum: 99.01871931552887
time_elpased: 1.818
batch start
#iterations: 143
currently lose_sum: 98.96550917625427
time_elpased: 1.843
batch start
#iterations: 144
currently lose_sum: 98.94420373439789
time_elpased: 1.943
batch start
#iterations: 145
currently lose_sum: 98.93448609113693
time_elpased: 1.789
batch start
#iterations: 146
currently lose_sum: 98.67805635929108
time_elpased: 1.807
batch start
#iterations: 147
currently lose_sum: 98.83395230770111
time_elpased: 1.863
batch start
#iterations: 148
currently lose_sum: 98.74839240312576
time_elpased: 1.842
batch start
#iterations: 149
currently lose_sum: 98.91537648439407
time_elpased: 1.851
batch start
#iterations: 150
currently lose_sum: 98.90362960100174
time_elpased: 1.854
batch start
#iterations: 151
currently lose_sum: 98.57725578546524
time_elpased: 1.87
batch start
#iterations: 152
currently lose_sum: 98.65373104810715
time_elpased: 1.846
batch start
#iterations: 153
currently lose_sum: 98.97077906131744
time_elpased: 1.855
batch start
#iterations: 154
currently lose_sum: 98.75027549266815
time_elpased: 1.818
batch start
#iterations: 155
currently lose_sum: 98.66577214002609
time_elpased: 1.823
batch start
#iterations: 156
currently lose_sum: 99.04822528362274
time_elpased: 1.834
batch start
#iterations: 157
currently lose_sum: 98.93343365192413
time_elpased: 1.797
batch start
#iterations: 158
currently lose_sum: 98.8332799077034
time_elpased: 1.817
batch start
#iterations: 159
currently lose_sum: 98.74414157867432
time_elpased: 1.8
start validation test
0.605979381443
0.632663509535
0.508696099619
0.56394751854
0.606150177249
64.802
batch start
#iterations: 160
currently lose_sum: 98.95925033092499
time_elpased: 1.841
batch start
#iterations: 161
currently lose_sum: 98.8286172747612
time_elpased: 1.798
batch start
#iterations: 162
currently lose_sum: 98.81260585784912
time_elpased: 1.83
batch start
#iterations: 163
currently lose_sum: 98.94776231050491
time_elpased: 1.833
batch start
#iterations: 164
currently lose_sum: 98.85412681102753
time_elpased: 1.828
batch start
#iterations: 165
currently lose_sum: 98.94717222452164
time_elpased: 1.883
batch start
#iterations: 166
currently lose_sum: 98.85969859361649
time_elpased: 1.851
batch start
#iterations: 167
currently lose_sum: 99.03708136081696
time_elpased: 1.836
batch start
#iterations: 168
currently lose_sum: 98.94823372364044
time_elpased: 1.795
batch start
#iterations: 169
currently lose_sum: 98.83848929405212
time_elpased: 1.852
batch start
#iterations: 170
currently lose_sum: 98.83404678106308
time_elpased: 1.847
batch start
#iterations: 171
currently lose_sum: 98.69288915395737
time_elpased: 1.81
batch start
#iterations: 172
currently lose_sum: 98.77721655368805
time_elpased: 1.822
batch start
#iterations: 173
currently lose_sum: 98.67472767829895
time_elpased: 1.907
batch start
#iterations: 174
currently lose_sum: 98.87094730138779
time_elpased: 1.799
batch start
#iterations: 175
currently lose_sum: 98.80426985025406
time_elpased: 1.805
batch start
#iterations: 176
currently lose_sum: 99.14248412847519
time_elpased: 1.803
batch start
#iterations: 177
currently lose_sum: 98.65111249685287
time_elpased: 1.828
batch start
#iterations: 178
currently lose_sum: 98.72268968820572
time_elpased: 1.814
batch start
#iterations: 179
currently lose_sum: 98.82327270507812
time_elpased: 1.841
start validation test
0.598350515464
0.647419206617
0.435010805804
0.520374245968
0.598637283518
65.178
batch start
#iterations: 180
currently lose_sum: 98.85106366872787
time_elpased: 1.862
batch start
#iterations: 181
currently lose_sum: 99.01961642503738
time_elpased: 1.849
batch start
#iterations: 182
currently lose_sum: 98.7809636592865
time_elpased: 1.836
batch start
#iterations: 183
currently lose_sum: 98.625334918499
time_elpased: 1.812
batch start
#iterations: 184
currently lose_sum: 98.88371086120605
time_elpased: 1.857
batch start
#iterations: 185
currently lose_sum: 98.79076731204987
time_elpased: 1.835
batch start
#iterations: 186
currently lose_sum: 98.80516934394836
time_elpased: 1.825
batch start
#iterations: 187
currently lose_sum: 98.87301975488663
time_elpased: 1.837
batch start
#iterations: 188
currently lose_sum: 98.8373452425003
time_elpased: 1.833
batch start
#iterations: 189
currently lose_sum: 98.98354828357697
time_elpased: 1.843
batch start
#iterations: 190
currently lose_sum: 98.70061272382736
time_elpased: 1.82
batch start
#iterations: 191
currently lose_sum: 98.66765826940536
time_elpased: 1.803
batch start
#iterations: 192
currently lose_sum: 98.86015677452087
time_elpased: 1.814
batch start
#iterations: 193
currently lose_sum: 98.86730033159256
time_elpased: 1.784
batch start
#iterations: 194
currently lose_sum: 98.91832566261292
time_elpased: 1.785
batch start
#iterations: 195
currently lose_sum: 98.83220565319061
time_elpased: 1.812
batch start
#iterations: 196
currently lose_sum: 98.93064272403717
time_elpased: 1.811
batch start
#iterations: 197
currently lose_sum: 98.87187504768372
time_elpased: 1.79
batch start
#iterations: 198
currently lose_sum: 98.82352840900421
time_elpased: 1.818
batch start
#iterations: 199
currently lose_sum: 98.782142162323
time_elpased: 1.801
start validation test
0.607268041237
0.641756756757
0.488731089843
0.55488695449
0.607476151138
64.962
batch start
#iterations: 200
currently lose_sum: 98.61994910240173
time_elpased: 1.838
batch start
#iterations: 201
currently lose_sum: 98.8095029592514
time_elpased: 1.843
batch start
#iterations: 202
currently lose_sum: 98.95594346523285
time_elpased: 1.819
batch start
#iterations: 203
currently lose_sum: 98.97351384162903
time_elpased: 1.825
batch start
#iterations: 204
currently lose_sum: 98.75690406560898
time_elpased: 1.855
batch start
#iterations: 205
currently lose_sum: 98.75423270463943
time_elpased: 1.875
batch start
#iterations: 206
currently lose_sum: 98.5913497209549
time_elpased: 1.812
batch start
#iterations: 207
currently lose_sum: 98.89646536111832
time_elpased: 1.825
batch start
#iterations: 208
currently lose_sum: 98.75433993339539
time_elpased: 1.82
batch start
#iterations: 209
currently lose_sum: 98.75167483091354
time_elpased: 1.847
batch start
#iterations: 210
currently lose_sum: 98.6917622089386
time_elpased: 1.82
batch start
#iterations: 211
currently lose_sum: 98.66365784406662
time_elpased: 1.854
batch start
#iterations: 212
currently lose_sum: 98.67519003152847
time_elpased: 1.867
batch start
#iterations: 213
currently lose_sum: 98.78461235761642
time_elpased: 1.876
batch start
#iterations: 214
currently lose_sum: 98.77038669586182
time_elpased: 1.839
batch start
#iterations: 215
currently lose_sum: 98.61883521080017
time_elpased: 1.817
batch start
#iterations: 216
currently lose_sum: 98.8568531870842
time_elpased: 1.787
batch start
#iterations: 217
currently lose_sum: 98.77935433387756
time_elpased: 1.855
batch start
#iterations: 218
currently lose_sum: 98.51718038320541
time_elpased: 1.855
batch start
#iterations: 219
currently lose_sum: 98.4790969491005
time_elpased: 1.781
start validation test
0.605773195876
0.638914999329
0.489657301636
0.554416219995
0.605977055238
64.879
batch start
#iterations: 220
currently lose_sum: 98.63888478279114
time_elpased: 1.879
batch start
#iterations: 221
currently lose_sum: 98.83620434999466
time_elpased: 1.823
batch start
#iterations: 222
currently lose_sum: 98.69482553005219
time_elpased: 1.839
batch start
#iterations: 223
currently lose_sum: 98.94438129663467
time_elpased: 1.776
batch start
#iterations: 224
currently lose_sum: 98.72118109464645
time_elpased: 1.843
batch start
#iterations: 225
currently lose_sum: 98.73664343357086
time_elpased: 1.833
batch start
#iterations: 226
currently lose_sum: 98.45159530639648
time_elpased: 1.834
batch start
#iterations: 227
currently lose_sum: 98.64014434814453
time_elpased: 1.838
batch start
#iterations: 228
currently lose_sum: 98.8792205452919
time_elpased: 1.812
batch start
#iterations: 229
currently lose_sum: 98.99332016706467
time_elpased: 1.784
batch start
#iterations: 230
currently lose_sum: 98.8145586848259
time_elpased: 1.81
batch start
#iterations: 231
currently lose_sum: 98.77300477027893
time_elpased: 1.805
batch start
#iterations: 232
currently lose_sum: 98.79423534870148
time_elpased: 1.795
batch start
#iterations: 233
currently lose_sum: 98.65689474344254
time_elpased: 1.814
batch start
#iterations: 234
currently lose_sum: 98.6884298324585
time_elpased: 1.846
batch start
#iterations: 235
currently lose_sum: 98.54949325323105
time_elpased: 1.83
batch start
#iterations: 236
currently lose_sum: 98.81702333688736
time_elpased: 1.819
batch start
#iterations: 237
currently lose_sum: 98.7502806186676
time_elpased: 1.788
batch start
#iterations: 238
currently lose_sum: 98.610471367836
time_elpased: 1.803
batch start
#iterations: 239
currently lose_sum: 98.68397802114487
time_elpased: 1.799
start validation test
0.605773195876
0.640043319345
0.48656992899
0.55285313377
0.605982475597
64.821
batch start
#iterations: 240
currently lose_sum: 98.77479773759842
time_elpased: 1.862
batch start
#iterations: 241
currently lose_sum: 98.68710595369339
time_elpased: 1.838
batch start
#iterations: 242
currently lose_sum: 98.58208733797073
time_elpased: 1.821
batch start
#iterations: 243
currently lose_sum: 98.73158705234528
time_elpased: 1.792
batch start
#iterations: 244
currently lose_sum: 98.73130571842194
time_elpased: 1.822
batch start
#iterations: 245
currently lose_sum: 98.74841034412384
time_elpased: 1.792
batch start
#iterations: 246
currently lose_sum: 98.65868747234344
time_elpased: 1.815
batch start
#iterations: 247
currently lose_sum: 98.82701700925827
time_elpased: 1.795
batch start
#iterations: 248
currently lose_sum: 98.69344830513
time_elpased: 1.833
batch start
#iterations: 249
currently lose_sum: 98.66110932826996
time_elpased: 1.834
batch start
#iterations: 250
currently lose_sum: 98.8106740117073
time_elpased: 1.817
batch start
#iterations: 251
currently lose_sum: 98.78986340761185
time_elpased: 1.803
batch start
#iterations: 252
currently lose_sum: 98.86383354663849
time_elpased: 1.827
batch start
#iterations: 253
currently lose_sum: 98.6381339430809
time_elpased: 1.822
batch start
#iterations: 254
currently lose_sum: 98.454520881176
time_elpased: 1.871
batch start
#iterations: 255
currently lose_sum: 98.50352323055267
time_elpased: 1.805
batch start
#iterations: 256
currently lose_sum: 98.61166948080063
time_elpased: 1.801
batch start
#iterations: 257
currently lose_sum: 98.92934387922287
time_elpased: 1.82
batch start
#iterations: 258
currently lose_sum: 98.58276462554932
time_elpased: 1.824
batch start
#iterations: 259
currently lose_sum: 98.66000360250473
time_elpased: 1.868
start validation test
0.606907216495
0.632998346266
0.51209220953
0.566162248265
0.607073678864
64.857
batch start
#iterations: 260
currently lose_sum: 98.57728362083435
time_elpased: 1.882
batch start
#iterations: 261
currently lose_sum: 98.78111231327057
time_elpased: 1.814
batch start
#iterations: 262
currently lose_sum: 98.54113644361496
time_elpased: 1.833
batch start
#iterations: 263
currently lose_sum: 98.6037015914917
time_elpased: 1.82
batch start
#iterations: 264
currently lose_sum: 98.83777540922165
time_elpased: 1.833
batch start
#iterations: 265
currently lose_sum: 98.66915994882584
time_elpased: 1.773
batch start
#iterations: 266
currently lose_sum: 98.72380143404007
time_elpased: 1.834
batch start
#iterations: 267
currently lose_sum: 98.59301435947418
time_elpased: 1.791
batch start
#iterations: 268
currently lose_sum: 98.6084920167923
time_elpased: 1.832
batch start
#iterations: 269
currently lose_sum: 98.89467686414719
time_elpased: 1.843
batch start
#iterations: 270
currently lose_sum: 98.49717658758163
time_elpased: 1.82
batch start
#iterations: 271
currently lose_sum: 98.6086818575859
time_elpased: 1.816
batch start
#iterations: 272
currently lose_sum: 98.90303617715836
time_elpased: 1.804
batch start
#iterations: 273
currently lose_sum: 98.82183170318604
time_elpased: 1.837
batch start
#iterations: 274
currently lose_sum: 98.724400639534
time_elpased: 1.861
batch start
#iterations: 275
currently lose_sum: 98.83426690101624
time_elpased: 1.834
batch start
#iterations: 276
currently lose_sum: 98.85481888055801
time_elpased: 1.859
batch start
#iterations: 277
currently lose_sum: 98.67956882715225
time_elpased: 1.84
batch start
#iterations: 278
currently lose_sum: 98.74182003736496
time_elpased: 1.87
batch start
#iterations: 279
currently lose_sum: 98.4749606847763
time_elpased: 1.822
start validation test
0.606134020619
0.638881455713
0.491406812802
0.555523238904
0.606335441927
64.804
batch start
#iterations: 280
currently lose_sum: 98.92466133832932
time_elpased: 1.836
batch start
#iterations: 281
currently lose_sum: 98.73095655441284
time_elpased: 1.818
batch start
#iterations: 282
currently lose_sum: 98.66290318965912
time_elpased: 1.924
batch start
#iterations: 283
currently lose_sum: 98.50350594520569
time_elpased: 1.865
batch start
#iterations: 284
currently lose_sum: 98.71920013427734
time_elpased: 1.856
batch start
#iterations: 285
currently lose_sum: 98.45466607809067
time_elpased: 1.817
batch start
#iterations: 286
currently lose_sum: 98.62168258428574
time_elpased: 1.823
batch start
#iterations: 287
currently lose_sum: 98.56501740217209
time_elpased: 1.834
batch start
#iterations: 288
currently lose_sum: 98.62912786006927
time_elpased: 1.837
batch start
#iterations: 289
currently lose_sum: 98.64612525701523
time_elpased: 1.84
batch start
#iterations: 290
currently lose_sum: 98.52468758821487
time_elpased: 1.856
batch start
#iterations: 291
currently lose_sum: 98.39138060808182
time_elpased: 1.824
batch start
#iterations: 292
currently lose_sum: 98.705786049366
time_elpased: 1.905
batch start
#iterations: 293
currently lose_sum: 98.58502840995789
time_elpased: 1.776
batch start
#iterations: 294
currently lose_sum: 98.63095337152481
time_elpased: 1.828
batch start
#iterations: 295
currently lose_sum: 98.63902240991592
time_elpased: 1.861
batch start
#iterations: 296
currently lose_sum: 98.43476116657257
time_elpased: 1.793
batch start
#iterations: 297
currently lose_sum: 99.01546114683151
time_elpased: 1.838
batch start
#iterations: 298
currently lose_sum: 98.5008932352066
time_elpased: 1.891
batch start
#iterations: 299
currently lose_sum: 98.3844393491745
time_elpased: 1.862
start validation test
0.60706185567
0.646269907795
0.476072861994
0.548266666667
0.607291827052
64.853
batch start
#iterations: 300
currently lose_sum: 98.44501686096191
time_elpased: 1.886
batch start
#iterations: 301
currently lose_sum: 98.55550855398178
time_elpased: 1.846
batch start
#iterations: 302
currently lose_sum: 98.61169755458832
time_elpased: 1.847
batch start
#iterations: 303
currently lose_sum: 98.51890701055527
time_elpased: 1.845
batch start
#iterations: 304
currently lose_sum: 98.6103333234787
time_elpased: 1.834
batch start
#iterations: 305
currently lose_sum: 98.68412256240845
time_elpased: 1.841
batch start
#iterations: 306
currently lose_sum: 98.6167938709259
time_elpased: 1.85
batch start
#iterations: 307
currently lose_sum: 98.58144021034241
time_elpased: 1.801
batch start
#iterations: 308
currently lose_sum: 98.64162564277649
time_elpased: 1.924
batch start
#iterations: 309
currently lose_sum: 98.75120383501053
time_elpased: 1.855
batch start
#iterations: 310
currently lose_sum: 98.64957618713379
time_elpased: 1.853
batch start
#iterations: 311
currently lose_sum: 98.65814912319183
time_elpased: 1.802
batch start
#iterations: 312
currently lose_sum: 98.63093394041061
time_elpased: 1.863
batch start
#iterations: 313
currently lose_sum: 98.67778933048248
time_elpased: 1.821
batch start
#iterations: 314
currently lose_sum: 98.33845978975296
time_elpased: 1.838
batch start
#iterations: 315
currently lose_sum: 98.37304520606995
time_elpased: 1.793
batch start
#iterations: 316
currently lose_sum: 98.70832985639572
time_elpased: 1.906
batch start
#iterations: 317
currently lose_sum: 98.51163470745087
time_elpased: 1.783
batch start
#iterations: 318
currently lose_sum: 98.40567219257355
time_elpased: 1.817
batch start
#iterations: 319
currently lose_sum: 98.800168633461
time_elpased: 1.856
start validation test
0.606391752577
0.645260365769
0.475661212308
0.547630331754
0.606621270205
64.895
batch start
#iterations: 320
currently lose_sum: 98.53301435709
time_elpased: 1.868
batch start
#iterations: 321
currently lose_sum: 98.79781973361969
time_elpased: 1.808
batch start
#iterations: 322
currently lose_sum: 98.58095240592957
time_elpased: 1.884
batch start
#iterations: 323
currently lose_sum: 98.38242948055267
time_elpased: 1.832
batch start
#iterations: 324
currently lose_sum: 98.61270612478256
time_elpased: 1.887
batch start
#iterations: 325
currently lose_sum: 98.64153599739075
time_elpased: 1.802
batch start
#iterations: 326
currently lose_sum: 98.72671908140182
time_elpased: 1.826
batch start
#iterations: 327
currently lose_sum: 98.5027916431427
time_elpased: 1.827
batch start
#iterations: 328
currently lose_sum: 98.48162364959717
time_elpased: 1.828
batch start
#iterations: 329
currently lose_sum: 98.55131793022156
time_elpased: 1.794
batch start
#iterations: 330
currently lose_sum: 98.61389899253845
time_elpased: 1.838
batch start
#iterations: 331
currently lose_sum: 98.50838720798492
time_elpased: 1.793
batch start
#iterations: 332
currently lose_sum: 98.44114488363266
time_elpased: 1.852
batch start
#iterations: 333
currently lose_sum: 98.55974900722504
time_elpased: 1.83
batch start
#iterations: 334
currently lose_sum: 98.91170179843903
time_elpased: 1.88
batch start
#iterations: 335
currently lose_sum: 98.3531374335289
time_elpased: 1.824
batch start
#iterations: 336
currently lose_sum: 98.7226459980011
time_elpased: 1.811
batch start
#iterations: 337
currently lose_sum: 98.58236360549927
time_elpased: 1.807
batch start
#iterations: 338
currently lose_sum: 98.54571551084518
time_elpased: 1.861
batch start
#iterations: 339
currently lose_sum: 98.67562210559845
time_elpased: 1.823
start validation test
0.607113402062
0.638247327438
0.497684470516
0.559269110674
0.607305521429
64.729
batch start
#iterations: 340
currently lose_sum: 98.66410160064697
time_elpased: 1.89
batch start
#iterations: 341
currently lose_sum: 98.80897378921509
time_elpased: 1.792
batch start
#iterations: 342
currently lose_sum: 98.53287422657013
time_elpased: 1.829
batch start
#iterations: 343
currently lose_sum: 98.76126194000244
time_elpased: 1.816
batch start
#iterations: 344
currently lose_sum: 98.38874953985214
time_elpased: 1.895
batch start
#iterations: 345
currently lose_sum: 98.711934030056
time_elpased: 1.847
batch start
#iterations: 346
currently lose_sum: 98.71833175420761
time_elpased: 1.835
batch start
#iterations: 347
currently lose_sum: 98.43666368722916
time_elpased: 1.811
batch start
#iterations: 348
currently lose_sum: 98.72217482328415
time_elpased: 1.87
batch start
#iterations: 349
currently lose_sum: 98.43152779340744
time_elpased: 1.801
batch start
#iterations: 350
currently lose_sum: 98.68224024772644
time_elpased: 1.802
batch start
#iterations: 351
currently lose_sum: 98.48107367753983
time_elpased: 1.84
batch start
#iterations: 352
currently lose_sum: 98.60398054122925
time_elpased: 1.892
batch start
#iterations: 353
currently lose_sum: 98.69100022315979
time_elpased: 1.783
batch start
#iterations: 354
currently lose_sum: 98.44051826000214
time_elpased: 1.836
batch start
#iterations: 355
currently lose_sum: 98.43901306390762
time_elpased: 1.891
batch start
#iterations: 356
currently lose_sum: 98.43652528524399
time_elpased: 1.875
batch start
#iterations: 357
currently lose_sum: 98.49132043123245
time_elpased: 1.881
batch start
#iterations: 358
currently lose_sum: 98.47586697340012
time_elpased: 1.874
batch start
#iterations: 359
currently lose_sum: 98.36449253559113
time_elpased: 1.863
start validation test
0.60793814433
0.641393168118
0.492744674282
0.557327435689
0.608140384234
64.712
batch start
#iterations: 360
currently lose_sum: 98.61542057991028
time_elpased: 1.912
batch start
#iterations: 361
currently lose_sum: 98.73397761583328
time_elpased: 1.887
batch start
#iterations: 362
currently lose_sum: 98.62104642391205
time_elpased: 1.938
batch start
#iterations: 363
currently lose_sum: 98.49994468688965
time_elpased: 1.885
batch start
#iterations: 364
currently lose_sum: 98.42586433887482
time_elpased: 1.912
batch start
#iterations: 365
currently lose_sum: 98.53897047042847
time_elpased: 1.84
batch start
#iterations: 366
currently lose_sum: 98.29133409261703
time_elpased: 1.849
batch start
#iterations: 367
currently lose_sum: 98.65955066680908
time_elpased: 1.83
batch start
#iterations: 368
currently lose_sum: 98.46107190847397
time_elpased: 1.841
batch start
#iterations: 369
currently lose_sum: 98.6370113492012
time_elpased: 1.856
batch start
#iterations: 370
currently lose_sum: 98.43094950914383
time_elpased: 1.903
batch start
#iterations: 371
currently lose_sum: 98.36285299062729
time_elpased: 1.803
batch start
#iterations: 372
currently lose_sum: 98.58258819580078
time_elpased: 1.885
batch start
#iterations: 373
currently lose_sum: 98.38087511062622
time_elpased: 1.866
batch start
#iterations: 374
currently lose_sum: 98.52241319417953
time_elpased: 1.847
batch start
#iterations: 375
currently lose_sum: 98.68347704410553
time_elpased: 1.819
batch start
#iterations: 376
currently lose_sum: 98.632652759552
time_elpased: 1.879
batch start
#iterations: 377
currently lose_sum: 98.54351752996445
time_elpased: 1.828
batch start
#iterations: 378
currently lose_sum: 98.53443652391434
time_elpased: 1.839
batch start
#iterations: 379
currently lose_sum: 98.71048825979233
time_elpased: 1.786
start validation test
0.6
0.648730810154
0.439230215087
0.52380952381
0.600282256154
65.092
batch start
#iterations: 380
currently lose_sum: 98.68690705299377
time_elpased: 1.935
batch start
#iterations: 381
currently lose_sum: 98.73911917209625
time_elpased: 1.83
batch start
#iterations: 382
currently lose_sum: 98.37343561649323
time_elpased: 1.826
batch start
#iterations: 383
currently lose_sum: 98.5637977719307
time_elpased: 1.823
batch start
#iterations: 384
currently lose_sum: 98.49889141321182
time_elpased: 1.808
batch start
#iterations: 385
currently lose_sum: 98.74107509851456
time_elpased: 1.815
batch start
#iterations: 386
currently lose_sum: 98.74940001964569
time_elpased: 1.845
batch start
#iterations: 387
currently lose_sum: 98.46290445327759
time_elpased: 1.86
batch start
#iterations: 388
currently lose_sum: 98.54116374254227
time_elpased: 1.86
batch start
#iterations: 389
currently lose_sum: 98.58313941955566
time_elpased: 1.81
batch start
#iterations: 390
currently lose_sum: 98.7435553073883
time_elpased: 1.813
batch start
#iterations: 391
currently lose_sum: 98.55953478813171
time_elpased: 1.834
batch start
#iterations: 392
currently lose_sum: 98.5986099243164
time_elpased: 1.897
batch start
#iterations: 393
currently lose_sum: 98.41756093502045
time_elpased: 1.846
batch start
#iterations: 394
currently lose_sum: 98.39466726779938
time_elpased: 1.889
batch start
#iterations: 395
currently lose_sum: 98.38847559690475
time_elpased: 1.896
batch start
#iterations: 396
currently lose_sum: 98.62631070613861
time_elpased: 1.859
batch start
#iterations: 397
currently lose_sum: 98.29552435874939
time_elpased: 1.836
batch start
#iterations: 398
currently lose_sum: 98.52767556905746
time_elpased: 1.866
batch start
#iterations: 399
currently lose_sum: 98.47029203176498
time_elpased: 1.871
start validation test
0.606237113402
0.638866613205
0.49192137491
0.555846270132
0.606437812313
64.727
acc: 0.605
pre: 0.638
rec: 0.488
F1: 0.553
auc: 0.605
