start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.66248774528503
time_elpased: 2.004
batch start
#iterations: 1
currently lose_sum: 100.27233672142029
time_elpased: 1.864
batch start
#iterations: 2
currently lose_sum: 100.2736086845398
time_elpased: 1.894
batch start
#iterations: 3
currently lose_sum: 100.28833281993866
time_elpased: 1.887
batch start
#iterations: 4
currently lose_sum: 100.11311852931976
time_elpased: 1.876
batch start
#iterations: 5
currently lose_sum: 100.1410346031189
time_elpased: 1.908
batch start
#iterations: 6
currently lose_sum: 100.14852738380432
time_elpased: 1.916
batch start
#iterations: 7
currently lose_sum: 99.87651860713959
time_elpased: 1.88
batch start
#iterations: 8
currently lose_sum: 100.02428060770035
time_elpased: 1.859
batch start
#iterations: 9
currently lose_sum: 99.87479519844055
time_elpased: 1.908
batch start
#iterations: 10
currently lose_sum: 99.76740252971649
time_elpased: 1.858
batch start
#iterations: 11
currently lose_sum: 99.78751403093338
time_elpased: 1.875
batch start
#iterations: 12
currently lose_sum: 99.79349946975708
time_elpased: 1.822
batch start
#iterations: 13
currently lose_sum: 99.85939091444016
time_elpased: 1.873
batch start
#iterations: 14
currently lose_sum: 99.78627771139145
time_elpased: 1.864
batch start
#iterations: 15
currently lose_sum: 99.71139717102051
time_elpased: 1.866
batch start
#iterations: 16
currently lose_sum: 99.54094165563583
time_elpased: 1.829
batch start
#iterations: 17
currently lose_sum: 99.60944443941116
time_elpased: 1.827
batch start
#iterations: 18
currently lose_sum: 99.7295857667923
time_elpased: 1.875
batch start
#iterations: 19
currently lose_sum: 99.53853851556778
time_elpased: 1.903
start validation test
0.590309278351
0.632232022724
0.435216630647
0.51554309399
0.590581567415
65.666
batch start
#iterations: 20
currently lose_sum: 99.56760931015015
time_elpased: 1.919
batch start
#iterations: 21
currently lose_sum: 99.435795545578
time_elpased: 1.943
batch start
#iterations: 22
currently lose_sum: 99.4623818397522
time_elpased: 1.853
batch start
#iterations: 23
currently lose_sum: 99.37505739927292
time_elpased: 1.876
batch start
#iterations: 24
currently lose_sum: 99.56598341464996
time_elpased: 1.887
batch start
#iterations: 25
currently lose_sum: 99.5497967004776
time_elpased: 1.862
batch start
#iterations: 26
currently lose_sum: 99.48424643278122
time_elpased: 1.85
batch start
#iterations: 27
currently lose_sum: 99.27692663669586
time_elpased: 1.853
batch start
#iterations: 28
currently lose_sum: 99.31897914409637
time_elpased: 1.855
batch start
#iterations: 29
currently lose_sum: 99.29656314849854
time_elpased: 1.876
batch start
#iterations: 30
currently lose_sum: 99.4034234881401
time_elpased: 1.895
batch start
#iterations: 31
currently lose_sum: 99.27637594938278
time_elpased: 1.858
batch start
#iterations: 32
currently lose_sum: 99.3589893579483
time_elpased: 1.847
batch start
#iterations: 33
currently lose_sum: 99.27967059612274
time_elpased: 1.879
batch start
#iterations: 34
currently lose_sum: 99.33821511268616
time_elpased: 1.837
batch start
#iterations: 35
currently lose_sum: 99.32488977909088
time_elpased: 1.844
batch start
#iterations: 36
currently lose_sum: 99.09625059366226
time_elpased: 1.833
batch start
#iterations: 37
currently lose_sum: 99.52014762163162
time_elpased: 1.837
batch start
#iterations: 38
currently lose_sum: 99.0902601480484
time_elpased: 1.885
batch start
#iterations: 39
currently lose_sum: 99.22074955701828
time_elpased: 1.861
start validation test
0.602783505155
0.621013359008
0.531028095091
0.572506379674
0.602909482845
65.187
batch start
#iterations: 40
currently lose_sum: 99.31141662597656
time_elpased: 1.841
batch start
#iterations: 41
currently lose_sum: 99.28318905830383
time_elpased: 1.865
batch start
#iterations: 42
currently lose_sum: 99.16550594568253
time_elpased: 1.87
batch start
#iterations: 43
currently lose_sum: 99.07515728473663
time_elpased: 1.847
batch start
#iterations: 44
currently lose_sum: 99.3835541009903
time_elpased: 1.851
batch start
#iterations: 45
currently lose_sum: 99.14780765771866
time_elpased: 1.878
batch start
#iterations: 46
currently lose_sum: 98.9305494427681
time_elpased: 1.891
batch start
#iterations: 47
currently lose_sum: 99.124462723732
time_elpased: 1.842
batch start
#iterations: 48
currently lose_sum: 99.24490451812744
time_elpased: 1.891
batch start
#iterations: 49
currently lose_sum: 99.36284416913986
time_elpased: 1.857
batch start
#iterations: 50
currently lose_sum: 99.13055330514908
time_elpased: 1.857
batch start
#iterations: 51
currently lose_sum: 99.2037644982338
time_elpased: 1.894
batch start
#iterations: 52
currently lose_sum: 99.13760006427765
time_elpased: 1.861
batch start
#iterations: 53
currently lose_sum: 99.07965141534805
time_elpased: 1.893
batch start
#iterations: 54
currently lose_sum: 99.18080139160156
time_elpased: 1.866
batch start
#iterations: 55
currently lose_sum: 99.38528668880463
time_elpased: 1.883
batch start
#iterations: 56
currently lose_sum: 99.09663760662079
time_elpased: 1.858
batch start
#iterations: 57
currently lose_sum: 99.11099696159363
time_elpased: 1.836
batch start
#iterations: 58
currently lose_sum: 99.04218047857285
time_elpased: 1.832
batch start
#iterations: 59
currently lose_sum: 99.07575607299805
time_elpased: 1.843
start validation test
0.602525773196
0.623127915541
0.522383451683
0.568325589207
0.602666475402
65.061
batch start
#iterations: 60
currently lose_sum: 99.04224294424057
time_elpased: 1.916
batch start
#iterations: 61
currently lose_sum: 99.2071180343628
time_elpased: 1.851
batch start
#iterations: 62
currently lose_sum: 98.93404453992844
time_elpased: 1.891
batch start
#iterations: 63
currently lose_sum: 99.28987669944763
time_elpased: 1.9
batch start
#iterations: 64
currently lose_sum: 99.18693870306015
time_elpased: 1.875
batch start
#iterations: 65
currently lose_sum: 98.81462389230728
time_elpased: 1.877
batch start
#iterations: 66
currently lose_sum: 99.00011551380157
time_elpased: 1.861
batch start
#iterations: 67
currently lose_sum: 99.01718938350677
time_elpased: 1.854
batch start
#iterations: 68
currently lose_sum: 98.9805246591568
time_elpased: 1.852
batch start
#iterations: 69
currently lose_sum: 99.13770806789398
time_elpased: 1.846
batch start
#iterations: 70
currently lose_sum: 99.04951864480972
time_elpased: 1.905
batch start
#iterations: 71
currently lose_sum: 99.24455058574677
time_elpased: 1.862
batch start
#iterations: 72
currently lose_sum: 99.07274103164673
time_elpased: 1.848
batch start
#iterations: 73
currently lose_sum: 99.01081776618958
time_elpased: 1.863
batch start
#iterations: 74
currently lose_sum: 98.85223573446274
time_elpased: 1.864
batch start
#iterations: 75
currently lose_sum: 98.91213011741638
time_elpased: 1.831
batch start
#iterations: 76
currently lose_sum: 98.71187669038773
time_elpased: 1.871
batch start
#iterations: 77
currently lose_sum: 99.0238966345787
time_elpased: 1.85
batch start
#iterations: 78
currently lose_sum: 98.86624020338058
time_elpased: 1.857
batch start
#iterations: 79
currently lose_sum: 99.1405171751976
time_elpased: 1.877
start validation test
0.597525773196
0.62426767348
0.493465061233
0.551212783079
0.597708467826
65.152
batch start
#iterations: 80
currently lose_sum: 98.92650026082993
time_elpased: 1.883
batch start
#iterations: 81
currently lose_sum: 98.73856556415558
time_elpased: 1.908
batch start
#iterations: 82
currently lose_sum: 98.84822469949722
time_elpased: 1.842
batch start
#iterations: 83
currently lose_sum: 98.82157546281815
time_elpased: 1.87
batch start
#iterations: 84
currently lose_sum: 98.91081839799881
time_elpased: 1.837
batch start
#iterations: 85
currently lose_sum: 99.0635941028595
time_elpased: 1.829
batch start
#iterations: 86
currently lose_sum: 99.178562104702
time_elpased: 1.848
batch start
#iterations: 87
currently lose_sum: 98.99385416507721
time_elpased: 1.873
batch start
#iterations: 88
currently lose_sum: 98.74369215965271
time_elpased: 1.837
batch start
#iterations: 89
currently lose_sum: 98.60918736457825
time_elpased: 1.977
batch start
#iterations: 90
currently lose_sum: 99.08753436803818
time_elpased: 1.831
batch start
#iterations: 91
currently lose_sum: 99.066810131073
time_elpased: 1.851
batch start
#iterations: 92
currently lose_sum: 98.76347017288208
time_elpased: 1.88
batch start
#iterations: 93
currently lose_sum: 98.85278654098511
time_elpased: 1.86
batch start
#iterations: 94
currently lose_sum: 98.8077991604805
time_elpased: 1.836
batch start
#iterations: 95
currently lose_sum: 98.88926881551743
time_elpased: 1.85
batch start
#iterations: 96
currently lose_sum: 98.72982281446457
time_elpased: 1.836
batch start
#iterations: 97
currently lose_sum: 98.959104180336
time_elpased: 1.88
batch start
#iterations: 98
currently lose_sum: 98.92158192396164
time_elpased: 1.884
batch start
#iterations: 99
currently lose_sum: 98.7971743941307
time_elpased: 1.895
start validation test
0.603144329897
0.628600560795
0.507564062982
0.561635255936
0.603312135798
64.942
batch start
#iterations: 100
currently lose_sum: 98.7885389328003
time_elpased: 1.888
batch start
#iterations: 101
currently lose_sum: 98.7627632021904
time_elpased: 1.802
batch start
#iterations: 102
currently lose_sum: 98.70661109685898
time_elpased: 1.836
batch start
#iterations: 103
currently lose_sum: 99.11176139116287
time_elpased: 1.837
batch start
#iterations: 104
currently lose_sum: 99.04837340116501
time_elpased: 1.829
batch start
#iterations: 105
currently lose_sum: 98.79970908164978
time_elpased: 1.861
batch start
#iterations: 106
currently lose_sum: 98.74513334035873
time_elpased: 1.834
batch start
#iterations: 107
currently lose_sum: 99.07298028469086
time_elpased: 1.829
batch start
#iterations: 108
currently lose_sum: 99.03022575378418
time_elpased: 1.819
batch start
#iterations: 109
currently lose_sum: 98.8089804649353
time_elpased: 1.86
batch start
#iterations: 110
currently lose_sum: 98.86648833751678
time_elpased: 1.846
batch start
#iterations: 111
currently lose_sum: 98.82827723026276
time_elpased: 1.84
batch start
#iterations: 112
currently lose_sum: 98.8762698173523
time_elpased: 1.84
batch start
#iterations: 113
currently lose_sum: 98.87458020448685
time_elpased: 1.856
batch start
#iterations: 114
currently lose_sum: 98.84556120634079
time_elpased: 1.879
batch start
#iterations: 115
currently lose_sum: 98.7720336318016
time_elpased: 1.82
batch start
#iterations: 116
currently lose_sum: 99.01586467027664
time_elpased: 1.877
batch start
#iterations: 117
currently lose_sum: 98.7740650177002
time_elpased: 1.848
batch start
#iterations: 118
currently lose_sum: 99.07482153177261
time_elpased: 1.88
batch start
#iterations: 119
currently lose_sum: 99.00937527418137
time_elpased: 1.896
start validation test
0.603298969072
0.625512358713
0.518266954821
0.566861773976
0.603448255888
64.962
batch start
#iterations: 120
currently lose_sum: 98.94179683923721
time_elpased: 1.864
batch start
#iterations: 121
currently lose_sum: 98.83421474695206
time_elpased: 1.861
batch start
#iterations: 122
currently lose_sum: 98.87793111801147
time_elpased: 1.836
batch start
#iterations: 123
currently lose_sum: 98.81723880767822
time_elpased: 1.895
batch start
#iterations: 124
currently lose_sum: 98.60902571678162
time_elpased: 1.887
batch start
#iterations: 125
currently lose_sum: 99.01976239681244
time_elpased: 1.868
batch start
#iterations: 126
currently lose_sum: 98.83940333127975
time_elpased: 1.86
batch start
#iterations: 127
currently lose_sum: 98.9422807097435
time_elpased: 1.831
batch start
#iterations: 128
currently lose_sum: 98.97957164049149
time_elpased: 1.857
batch start
#iterations: 129
currently lose_sum: 98.72847551107407
time_elpased: 1.831
batch start
#iterations: 130
currently lose_sum: 98.84484362602234
time_elpased: 1.892
batch start
#iterations: 131
currently lose_sum: 98.69320964813232
time_elpased: 1.855
batch start
#iterations: 132
currently lose_sum: 98.72972375154495
time_elpased: 1.865
batch start
#iterations: 133
currently lose_sum: 98.83335256576538
time_elpased: 1.892
batch start
#iterations: 134
currently lose_sum: 98.70518338680267
time_elpased: 1.883
batch start
#iterations: 135
currently lose_sum: 98.89031434059143
time_elpased: 1.85
batch start
#iterations: 136
currently lose_sum: 98.59230124950409
time_elpased: 1.859
batch start
#iterations: 137
currently lose_sum: 98.79214155673981
time_elpased: 1.861
batch start
#iterations: 138
currently lose_sum: 98.71534478664398
time_elpased: 1.869
batch start
#iterations: 139
currently lose_sum: 98.90580981969833
time_elpased: 1.863
start validation test
0.608041237113
0.628387410378
0.532160131728
0.576284408782
0.608174458098
64.813
batch start
#iterations: 140
currently lose_sum: 98.8610765337944
time_elpased: 1.889
batch start
#iterations: 141
currently lose_sum: 98.9507971405983
time_elpased: 1.851
batch start
#iterations: 142
currently lose_sum: 98.94604963064194
time_elpased: 1.845
batch start
#iterations: 143
currently lose_sum: 98.91413778066635
time_elpased: 1.982
batch start
#iterations: 144
currently lose_sum: 98.70637774467468
time_elpased: 1.976
batch start
#iterations: 145
currently lose_sum: 98.8204647898674
time_elpased: 1.876
batch start
#iterations: 146
currently lose_sum: 98.66203618049622
time_elpased: 1.852
batch start
#iterations: 147
currently lose_sum: 98.87786847352982
time_elpased: 1.873
batch start
#iterations: 148
currently lose_sum: 99.06256860494614
time_elpased: 1.839
batch start
#iterations: 149
currently lose_sum: 98.71476727724075
time_elpased: 1.871
batch start
#iterations: 150
currently lose_sum: 98.67100077867508
time_elpased: 1.885
batch start
#iterations: 151
currently lose_sum: 98.79796743392944
time_elpased: 1.869
batch start
#iterations: 152
currently lose_sum: 99.02258521318436
time_elpased: 1.938
batch start
#iterations: 153
currently lose_sum: 98.87859547138214
time_elpased: 1.874
batch start
#iterations: 154
currently lose_sum: 98.9371719956398
time_elpased: 1.849
batch start
#iterations: 155
currently lose_sum: 98.7016881108284
time_elpased: 1.89
batch start
#iterations: 156
currently lose_sum: 98.77488350868225
time_elpased: 1.848
batch start
#iterations: 157
currently lose_sum: 98.9331973195076
time_elpased: 1.869
batch start
#iterations: 158
currently lose_sum: 98.9766657948494
time_elpased: 1.852
batch start
#iterations: 159
currently lose_sum: 99.08249872922897
time_elpased: 1.862
start validation test
0.604432989691
0.626910175177
0.519296079037
0.568051334009
0.604582460669
65.016
batch start
#iterations: 160
currently lose_sum: 98.8664682507515
time_elpased: 1.903
batch start
#iterations: 161
currently lose_sum: 98.76620519161224
time_elpased: 1.832
batch start
#iterations: 162
currently lose_sum: 98.64826738834381
time_elpased: 1.84
batch start
#iterations: 163
currently lose_sum: 98.85413002967834
time_elpased: 1.891
batch start
#iterations: 164
currently lose_sum: 98.737788438797
time_elpased: 1.92
batch start
#iterations: 165
currently lose_sum: 98.77977812290192
time_elpased: 1.9
batch start
#iterations: 166
currently lose_sum: 98.68826353549957
time_elpased: 1.899
batch start
#iterations: 167
currently lose_sum: 99.06773239374161
time_elpased: 1.866
batch start
#iterations: 168
currently lose_sum: 98.60767006874084
time_elpased: 1.845
batch start
#iterations: 169
currently lose_sum: 98.77459216117859
time_elpased: 1.847
batch start
#iterations: 170
currently lose_sum: 98.74814265966415
time_elpased: 1.852
batch start
#iterations: 171
currently lose_sum: 98.68887281417847
time_elpased: 1.861
batch start
#iterations: 172
currently lose_sum: 98.89513999223709
time_elpased: 1.856
batch start
#iterations: 173
currently lose_sum: 98.70365643501282
time_elpased: 2.029
batch start
#iterations: 174
currently lose_sum: 98.8017749786377
time_elpased: 1.857
batch start
#iterations: 175
currently lose_sum: 98.5183424949646
time_elpased: 1.826
batch start
#iterations: 176
currently lose_sum: 98.61035060882568
time_elpased: 1.849
batch start
#iterations: 177
currently lose_sum: 98.61299759149551
time_elpased: 1.865
batch start
#iterations: 178
currently lose_sum: 98.63133281469345
time_elpased: 1.858
batch start
#iterations: 179
currently lose_sum: 98.82370060682297
time_elpased: 1.85
start validation test
0.602010309278
0.613331819214
0.55582998868
0.583166873617
0.602091385954
64.915
batch start
#iterations: 180
currently lose_sum: 98.60223150253296
time_elpased: 1.932
batch start
#iterations: 181
currently lose_sum: 98.9795600771904
time_elpased: 2.073
batch start
#iterations: 182
currently lose_sum: 98.86877924203873
time_elpased: 1.859
batch start
#iterations: 183
currently lose_sum: 98.7738271355629
time_elpased: 1.864
batch start
#iterations: 184
currently lose_sum: 98.72151869535446
time_elpased: 1.852
batch start
#iterations: 185
currently lose_sum: 98.78019994497299
time_elpased: 1.847
batch start
#iterations: 186
currently lose_sum: 98.80881726741791
time_elpased: 1.856
batch start
#iterations: 187
currently lose_sum: 98.49441659450531
time_elpased: 1.903
batch start
#iterations: 188
currently lose_sum: 98.84537440538406
time_elpased: 1.84
batch start
#iterations: 189
currently lose_sum: 98.68390417098999
time_elpased: 1.861
batch start
#iterations: 190
currently lose_sum: 98.93275135755539
time_elpased: 1.878
batch start
#iterations: 191
currently lose_sum: 98.85697954893112
time_elpased: 1.886
batch start
#iterations: 192
currently lose_sum: 98.61252772808075
time_elpased: 1.848
batch start
#iterations: 193
currently lose_sum: 98.83028674125671
time_elpased: 1.868
batch start
#iterations: 194
currently lose_sum: 98.80335372686386
time_elpased: 1.838
batch start
#iterations: 195
currently lose_sum: 98.82325679063797
time_elpased: 1.882
batch start
#iterations: 196
currently lose_sum: 99.05470818281174
time_elpased: 1.866
batch start
#iterations: 197
currently lose_sum: 98.66469049453735
time_elpased: 1.909
batch start
#iterations: 198
currently lose_sum: 98.73018634319305
time_elpased: 1.862
batch start
#iterations: 199
currently lose_sum: 98.6239640712738
time_elpased: 1.878
start validation test
0.604845360825
0.639164065681
0.484717505403
0.551328573101
0.605056263803
64.903
batch start
#iterations: 200
currently lose_sum: 98.62490051984787
time_elpased: 1.93
batch start
#iterations: 201
currently lose_sum: 98.59669351577759
time_elpased: 1.94
batch start
#iterations: 202
currently lose_sum: 98.6380387544632
time_elpased: 1.906
batch start
#iterations: 203
currently lose_sum: 98.72107380628586
time_elpased: 1.876
batch start
#iterations: 204
currently lose_sum: 98.6497454047203
time_elpased: 1.842
batch start
#iterations: 205
currently lose_sum: 98.60312342643738
time_elpased: 1.872
batch start
#iterations: 206
currently lose_sum: 98.6157893538475
time_elpased: 1.883
batch start
#iterations: 207
currently lose_sum: 98.71875774860382
time_elpased: 1.899
batch start
#iterations: 208
currently lose_sum: 98.57237261533737
time_elpased: 1.879
batch start
#iterations: 209
currently lose_sum: 98.49293982982635
time_elpased: 1.851
batch start
#iterations: 210
currently lose_sum: 98.5324205160141
time_elpased: 1.862
batch start
#iterations: 211
currently lose_sum: 98.61653584241867
time_elpased: 1.862
batch start
#iterations: 212
currently lose_sum: 98.53631311655045
time_elpased: 1.888
batch start
#iterations: 213
currently lose_sum: 98.89250785112381
time_elpased: 1.918
batch start
#iterations: 214
currently lose_sum: 98.83681410551071
time_elpased: 1.891
batch start
#iterations: 215
currently lose_sum: 98.74125248193741
time_elpased: 1.895
batch start
#iterations: 216
currently lose_sum: 98.36416220664978
time_elpased: 1.87
batch start
#iterations: 217
currently lose_sum: 98.7790452837944
time_elpased: 1.95
batch start
#iterations: 218
currently lose_sum: 98.56663405895233
time_elpased: 1.916
batch start
#iterations: 219
currently lose_sum: 98.91128128767014
time_elpased: 1.974
start validation test
0.597216494845
0.640070660975
0.447463208809
0.526711084191
0.597479409837
64.906
batch start
#iterations: 220
currently lose_sum: 98.68709748983383
time_elpased: 1.908
batch start
#iterations: 221
currently lose_sum: 98.55847322940826
time_elpased: 1.898
batch start
#iterations: 222
currently lose_sum: 98.65585947036743
time_elpased: 1.867
batch start
#iterations: 223
currently lose_sum: 98.38446062803268
time_elpased: 1.906
batch start
#iterations: 224
currently lose_sum: 98.57635867595673
time_elpased: 1.853
batch start
#iterations: 225
currently lose_sum: 98.73141419887543
time_elpased: 1.849
batch start
#iterations: 226
currently lose_sum: 98.6161293387413
time_elpased: 1.892
batch start
#iterations: 227
currently lose_sum: 98.80750435590744
time_elpased: 1.845
batch start
#iterations: 228
currently lose_sum: 98.58886325359344
time_elpased: 1.873
batch start
#iterations: 229
currently lose_sum: 98.7301282286644
time_elpased: 1.86
batch start
#iterations: 230
currently lose_sum: 98.86935424804688
time_elpased: 1.862
batch start
#iterations: 231
currently lose_sum: 98.657619535923
time_elpased: 1.852
batch start
#iterations: 232
currently lose_sum: 98.61961644887924
time_elpased: 1.83
batch start
#iterations: 233
currently lose_sum: 98.8033476471901
time_elpased: 1.85
batch start
#iterations: 234
currently lose_sum: 98.54585844278336
time_elpased: 1.847
batch start
#iterations: 235
currently lose_sum: 98.78396421670914
time_elpased: 1.836
batch start
#iterations: 236
currently lose_sum: 98.83520364761353
time_elpased: 1.845
batch start
#iterations: 237
currently lose_sum: 98.70464998483658
time_elpased: 1.855
batch start
#iterations: 238
currently lose_sum: 98.60652965307236
time_elpased: 1.844
batch start
#iterations: 239
currently lose_sum: 98.83800321817398
time_elpased: 1.852
start validation test
0.597268041237
0.631929046563
0.469280642174
0.538593279395
0.597492742857
65.060
batch start
#iterations: 240
currently lose_sum: 98.45192348957062
time_elpased: 1.929
batch start
#iterations: 241
currently lose_sum: 98.70538419485092
time_elpased: 1.828
batch start
#iterations: 242
currently lose_sum: 98.6471004486084
time_elpased: 1.874
batch start
#iterations: 243
currently lose_sum: 98.75840139389038
time_elpased: 1.899
batch start
#iterations: 244
currently lose_sum: 98.59726375341415
time_elpased: 1.887
batch start
#iterations: 245
currently lose_sum: 98.86290150880814
time_elpased: 1.887
batch start
#iterations: 246
currently lose_sum: 98.77843695878983
time_elpased: 1.838
batch start
#iterations: 247
currently lose_sum: 98.63605600595474
time_elpased: 1.845
batch start
#iterations: 248
currently lose_sum: 98.47316765785217
time_elpased: 1.923
batch start
#iterations: 249
currently lose_sum: 98.61182951927185
time_elpased: 1.878
batch start
#iterations: 250
currently lose_sum: 98.6747761964798
time_elpased: 1.884
batch start
#iterations: 251
currently lose_sum: 98.5842649936676
time_elpased: 1.957
batch start
#iterations: 252
currently lose_sum: 98.63254106044769
time_elpased: 1.844
batch start
#iterations: 253
currently lose_sum: 98.71734350919724
time_elpased: 1.878
batch start
#iterations: 254
currently lose_sum: 98.80571550130844
time_elpased: 1.837
batch start
#iterations: 255
currently lose_sum: 98.55909955501556
time_elpased: 1.819
batch start
#iterations: 256
currently lose_sum: 98.50687396526337
time_elpased: 1.855
batch start
#iterations: 257
currently lose_sum: 98.35454148054123
time_elpased: 1.821
batch start
#iterations: 258
currently lose_sum: 98.4485901594162
time_elpased: 1.843
batch start
#iterations: 259
currently lose_sum: 98.6501053571701
time_elpased: 1.882
start validation test
0.606855670103
0.631512710798
0.516414531234
0.56819339863
0.607014453472
64.777
batch start
#iterations: 260
currently lose_sum: 98.66318565607071
time_elpased: 1.904
batch start
#iterations: 261
currently lose_sum: 98.55379068851471
time_elpased: 1.865
batch start
#iterations: 262
currently lose_sum: 98.33094656467438
time_elpased: 1.886
batch start
#iterations: 263
currently lose_sum: 98.49609410762787
time_elpased: 1.917
batch start
#iterations: 264
currently lose_sum: 98.63950234651566
time_elpased: 1.902
batch start
#iterations: 265
currently lose_sum: 98.6591050028801
time_elpased: 1.927
batch start
#iterations: 266
currently lose_sum: 98.81633847951889
time_elpased: 1.886
batch start
#iterations: 267
currently lose_sum: 98.65090775489807
time_elpased: 1.926
batch start
#iterations: 268
currently lose_sum: 98.62986201047897
time_elpased: 1.886
batch start
#iterations: 269
currently lose_sum: 98.80591630935669
time_elpased: 1.878
batch start
#iterations: 270
currently lose_sum: 98.38944154977798
time_elpased: 1.862
batch start
#iterations: 271
currently lose_sum: 98.52306914329529
time_elpased: 1.931
batch start
#iterations: 272
currently lose_sum: 98.9634400010109
time_elpased: 1.838
batch start
#iterations: 273
currently lose_sum: 98.61828261613846
time_elpased: 1.88
batch start
#iterations: 274
currently lose_sum: 98.51013284921646
time_elpased: 1.872
batch start
#iterations: 275
currently lose_sum: 98.60659420490265
time_elpased: 1.851
batch start
#iterations: 276
currently lose_sum: 98.76552242040634
time_elpased: 1.91
batch start
#iterations: 277
currently lose_sum: 98.63381034135818
time_elpased: 1.924
batch start
#iterations: 278
currently lose_sum: 98.74968993663788
time_elpased: 1.83
batch start
#iterations: 279
currently lose_sum: 98.65170001983643
time_elpased: 1.868
start validation test
0.606340206186
0.624016217505
0.538540701863
0.578136220516
0.606459238673
64.771
batch start
#iterations: 280
currently lose_sum: 98.57333391904831
time_elpased: 1.926
batch start
#iterations: 281
currently lose_sum: 98.51081877946854
time_elpased: 1.914
batch start
#iterations: 282
currently lose_sum: 98.66767144203186
time_elpased: 1.862
batch start
#iterations: 283
currently lose_sum: 98.55254501104355
time_elpased: 1.861
batch start
#iterations: 284
currently lose_sum: 98.53213620185852
time_elpased: 1.884
batch start
#iterations: 285
currently lose_sum: 98.75338155031204
time_elpased: 1.859
batch start
#iterations: 286
currently lose_sum: 98.50940209627151
time_elpased: 1.88
batch start
#iterations: 287
currently lose_sum: 98.44582617282867
time_elpased: 1.9
batch start
#iterations: 288
currently lose_sum: 98.34102928638458
time_elpased: 1.841
batch start
#iterations: 289
currently lose_sum: 98.62411099672318
time_elpased: 1.828
batch start
#iterations: 290
currently lose_sum: 98.75928741693497
time_elpased: 1.845
batch start
#iterations: 291
currently lose_sum: 98.36604648828506
time_elpased: 1.931
batch start
#iterations: 292
currently lose_sum: 98.44296264648438
time_elpased: 1.861
batch start
#iterations: 293
currently lose_sum: 98.76707315444946
time_elpased: 1.883
batch start
#iterations: 294
currently lose_sum: 98.51546204090118
time_elpased: 1.848
batch start
#iterations: 295
currently lose_sum: 98.68842911720276
time_elpased: 1.881
batch start
#iterations: 296
currently lose_sum: 98.61300647258759
time_elpased: 1.906
batch start
#iterations: 297
currently lose_sum: 98.53739780187607
time_elpased: 1.873
batch start
#iterations: 298
currently lose_sum: 98.71440726518631
time_elpased: 1.881
batch start
#iterations: 299
currently lose_sum: 98.73376047611237
time_elpased: 1.853
start validation test
0.605
0.628825890617
0.515899969126
0.566792922155
0.605156428847
64.929
batch start
#iterations: 300
currently lose_sum: 98.52128547430038
time_elpased: 1.862
batch start
#iterations: 301
currently lose_sum: 98.43492668867111
time_elpased: 1.963
batch start
#iterations: 302
currently lose_sum: 98.58124053478241
time_elpased: 2.038
batch start
#iterations: 303
currently lose_sum: 98.75426626205444
time_elpased: 1.993
batch start
#iterations: 304
currently lose_sum: 98.47772312164307
time_elpased: 1.901
batch start
#iterations: 305
currently lose_sum: 98.68792581558228
time_elpased: 1.88
batch start
#iterations: 306
currently lose_sum: 98.4641175866127
time_elpased: 1.903
batch start
#iterations: 307
currently lose_sum: 98.87406152486801
time_elpased: 1.852
batch start
#iterations: 308
currently lose_sum: 98.30587935447693
time_elpased: 1.848
batch start
#iterations: 309
currently lose_sum: 98.6102364063263
time_elpased: 1.917
batch start
#iterations: 310
currently lose_sum: 98.81608963012695
time_elpased: 1.836
batch start
#iterations: 311
currently lose_sum: 98.43893319368362
time_elpased: 1.84
batch start
#iterations: 312
currently lose_sum: 98.61616253852844
time_elpased: 1.868
batch start
#iterations: 313
currently lose_sum: 98.39627003669739
time_elpased: 1.882
batch start
#iterations: 314
currently lose_sum: 98.7993836402893
time_elpased: 1.967
batch start
#iterations: 315
currently lose_sum: 98.57488232851028
time_elpased: 2.0
batch start
#iterations: 316
currently lose_sum: 98.61159837245941
time_elpased: 1.889
batch start
#iterations: 317
currently lose_sum: 98.61968177556992
time_elpased: 1.986
batch start
#iterations: 318
currently lose_sum: 98.67369401454926
time_elpased: 1.943
batch start
#iterations: 319
currently lose_sum: 98.75445032119751
time_elpased: 1.987
start validation test
0.607577319588
0.627887186968
0.531542657199
0.575711976815
0.607710810165
64.811
batch start
#iterations: 320
currently lose_sum: 98.62626683712006
time_elpased: 2.002
batch start
#iterations: 321
currently lose_sum: 98.63540548086166
time_elpased: 1.982
batch start
#iterations: 322
currently lose_sum: 98.78945553302765
time_elpased: 1.979
batch start
#iterations: 323
currently lose_sum: 98.50113731622696
time_elpased: 1.904
batch start
#iterations: 324
currently lose_sum: 98.9542868733406
time_elpased: 1.916
batch start
#iterations: 325
currently lose_sum: 98.73807376623154
time_elpased: 1.948
batch start
#iterations: 326
currently lose_sum: 98.64088296890259
time_elpased: 1.917
batch start
#iterations: 327
currently lose_sum: 98.71272271871567
time_elpased: 1.904
batch start
#iterations: 328
currently lose_sum: 98.72248530387878
time_elpased: 1.851
batch start
#iterations: 329
currently lose_sum: 98.57857996225357
time_elpased: 1.86
batch start
#iterations: 330
currently lose_sum: 98.79683589935303
time_elpased: 1.913
batch start
#iterations: 331
currently lose_sum: 98.55906891822815
time_elpased: 1.879
batch start
#iterations: 332
currently lose_sum: 98.39991867542267
time_elpased: 1.905
batch start
#iterations: 333
currently lose_sum: 98.42817395925522
time_elpased: 1.946
batch start
#iterations: 334
currently lose_sum: 98.59625083208084
time_elpased: 1.972
batch start
#iterations: 335
currently lose_sum: 98.57579737901688
time_elpased: 1.891
batch start
#iterations: 336
currently lose_sum: 98.56309819221497
time_elpased: 1.866
batch start
#iterations: 337
currently lose_sum: 98.51409196853638
time_elpased: 1.921
batch start
#iterations: 338
currently lose_sum: 98.45966696739197
time_elpased: 1.935
batch start
#iterations: 339
currently lose_sum: 98.71064406633377
time_elpased: 1.931
start validation test
0.606597938144
0.624507345038
0.538129052177
0.578109452736
0.606718145834
64.709
batch start
#iterations: 340
currently lose_sum: 98.74646651744843
time_elpased: 1.896
batch start
#iterations: 341
currently lose_sum: 99.06141799688339
time_elpased: 1.919
batch start
#iterations: 342
currently lose_sum: 98.59196573495865
time_elpased: 1.956
batch start
#iterations: 343
currently lose_sum: 98.69195425510406
time_elpased: 1.933
batch start
#iterations: 344
currently lose_sum: 98.68755996227264
time_elpased: 1.965
batch start
#iterations: 345
currently lose_sum: 98.71610134840012
time_elpased: 1.924
batch start
#iterations: 346
currently lose_sum: 98.53065353631973
time_elpased: 1.972
batch start
#iterations: 347
currently lose_sum: 98.7088451385498
time_elpased: 1.897
batch start
#iterations: 348
currently lose_sum: 98.74281096458435
time_elpased: 1.854
batch start
#iterations: 349
currently lose_sum: 98.51483780145645
time_elpased: 1.869
batch start
#iterations: 350
currently lose_sum: 98.68776494264603
time_elpased: 2.013
batch start
#iterations: 351
currently lose_sum: 98.86435151100159
time_elpased: 1.889
batch start
#iterations: 352
currently lose_sum: 98.60548549890518
time_elpased: 1.96
batch start
#iterations: 353
currently lose_sum: 98.63270890712738
time_elpased: 1.928
batch start
#iterations: 354
currently lose_sum: 98.65749323368073
time_elpased: 1.905
batch start
#iterations: 355
currently lose_sum: 98.6931893825531
time_elpased: 1.989
batch start
#iterations: 356
currently lose_sum: 98.48086112737656
time_elpased: 1.86
batch start
#iterations: 357
currently lose_sum: 98.61134654283524
time_elpased: 1.944
batch start
#iterations: 358
currently lose_sum: 98.6077088713646
time_elpased: 1.921
batch start
#iterations: 359
currently lose_sum: 98.83996778726578
time_elpased: 1.931
start validation test
0.603762886598
0.629563441409
0.507564062982
0.562019258162
0.60393177847
64.890
batch start
#iterations: 360
currently lose_sum: 98.8203861117363
time_elpased: 1.886
batch start
#iterations: 361
currently lose_sum: 98.83504354953766
time_elpased: 1.913
batch start
#iterations: 362
currently lose_sum: 98.59103518724442
time_elpased: 1.905
batch start
#iterations: 363
currently lose_sum: 98.3420302271843
time_elpased: 1.91
batch start
#iterations: 364
currently lose_sum: 98.84797376394272
time_elpased: 1.964
batch start
#iterations: 365
currently lose_sum: 98.67845022678375
time_elpased: 1.909
batch start
#iterations: 366
currently lose_sum: 98.90204095840454
time_elpased: 1.875
batch start
#iterations: 367
currently lose_sum: 98.31833815574646
time_elpased: 1.921
batch start
#iterations: 368
currently lose_sum: 98.62910228967667
time_elpased: 1.856
batch start
#iterations: 369
currently lose_sum: 98.41995793581009
time_elpased: 1.899
batch start
#iterations: 370
currently lose_sum: 98.7465318441391
time_elpased: 1.925
batch start
#iterations: 371
currently lose_sum: 98.5341677069664
time_elpased: 1.974
batch start
#iterations: 372
currently lose_sum: 98.64505022764206
time_elpased: 1.879
batch start
#iterations: 373
currently lose_sum: 98.70916903018951
time_elpased: 1.834
batch start
#iterations: 374
currently lose_sum: 98.54987454414368
time_elpased: 1.895
batch start
#iterations: 375
currently lose_sum: 98.71555829048157
time_elpased: 1.846
batch start
#iterations: 376
currently lose_sum: 98.54369294643402
time_elpased: 1.855
batch start
#iterations: 377
currently lose_sum: 98.53299188613892
time_elpased: 1.826
batch start
#iterations: 378
currently lose_sum: 98.72693520784378
time_elpased: 1.943
batch start
#iterations: 379
currently lose_sum: 98.41964316368103
time_elpased: 1.866
start validation test
0.606546391753
0.631333501386
0.51548831944
0.56755991162
0.606706258243
64.733
batch start
#iterations: 380
currently lose_sum: 98.763656437397
time_elpased: 1.851
batch start
#iterations: 381
currently lose_sum: 98.50763326883316
time_elpased: 1.834
batch start
#iterations: 382
currently lose_sum: 98.67531681060791
time_elpased: 1.877
batch start
#iterations: 383
currently lose_sum: 98.48839890956879
time_elpased: 1.863
batch start
#iterations: 384
currently lose_sum: 98.48124676942825
time_elpased: 1.867
batch start
#iterations: 385
currently lose_sum: 98.66269904375076
time_elpased: 1.93
batch start
#iterations: 386
currently lose_sum: 98.56825774908066
time_elpased: 1.851
batch start
#iterations: 387
currently lose_sum: 98.35139453411102
time_elpased: 1.879
batch start
#iterations: 388
currently lose_sum: 98.63626313209534
time_elpased: 1.972
batch start
#iterations: 389
currently lose_sum: 98.57485330104828
time_elpased: 1.901
batch start
#iterations: 390
currently lose_sum: 98.7418263554573
time_elpased: 1.908
batch start
#iterations: 391
currently lose_sum: 98.49290800094604
time_elpased: 1.842
batch start
#iterations: 392
currently lose_sum: 98.43474501371384
time_elpased: 1.837
batch start
#iterations: 393
currently lose_sum: 98.52585178613663
time_elpased: 1.866
batch start
#iterations: 394
currently lose_sum: 98.47416096925735
time_elpased: 1.907
batch start
#iterations: 395
currently lose_sum: 98.77611577510834
time_elpased: 1.853
batch start
#iterations: 396
currently lose_sum: 98.4104545712471
time_elpased: 1.879
batch start
#iterations: 397
currently lose_sum: 98.52570623159409
time_elpased: 1.893
batch start
#iterations: 398
currently lose_sum: 98.5045428276062
time_elpased: 1.852
batch start
#iterations: 399
currently lose_sum: 98.75083321332932
time_elpased: 1.96
start validation test
0.606237113402
0.632255600815
0.511165997736
0.565299038297
0.606404025409
64.801
acc: 0.603
pre: 0.619
rec: 0.536
F1: 0.575
auc: 0.603
