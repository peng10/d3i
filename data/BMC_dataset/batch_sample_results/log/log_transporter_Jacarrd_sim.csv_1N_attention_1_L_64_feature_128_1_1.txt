start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.73765748739243
time_elpased: 2.077
batch start
#iterations: 1
currently lose_sum: 100.4209896326065
time_elpased: 1.939
batch start
#iterations: 2
currently lose_sum: 100.421990275383
time_elpased: 1.964
batch start
#iterations: 3
currently lose_sum: 100.42668813467026
time_elpased: 1.956
batch start
#iterations: 4
currently lose_sum: 100.24193441867828
time_elpased: 1.933
batch start
#iterations: 5
currently lose_sum: 100.37169474363327
time_elpased: 1.962
batch start
#iterations: 6
currently lose_sum: 100.14789032936096
time_elpased: 1.941
batch start
#iterations: 7
currently lose_sum: 100.11495131254196
time_elpased: 1.921
batch start
#iterations: 8
currently lose_sum: 100.03088647127151
time_elpased: 1.96
batch start
#iterations: 9
currently lose_sum: 99.95201927423477
time_elpased: 1.94
batch start
#iterations: 10
currently lose_sum: 99.98960256576538
time_elpased: 1.954
batch start
#iterations: 11
currently lose_sum: 99.84712380170822
time_elpased: 1.968
batch start
#iterations: 12
currently lose_sum: 99.90715157985687
time_elpased: 1.96
batch start
#iterations: 13
currently lose_sum: 99.7713235616684
time_elpased: 1.952
batch start
#iterations: 14
currently lose_sum: 99.62768465280533
time_elpased: 1.97
batch start
#iterations: 15
currently lose_sum: 99.98215526342392
time_elpased: 1.938
batch start
#iterations: 16
currently lose_sum: 99.70918369293213
time_elpased: 1.951
batch start
#iterations: 17
currently lose_sum: 99.79699581861496
time_elpased: 1.952
batch start
#iterations: 18
currently lose_sum: 99.51946306228638
time_elpased: 1.942
batch start
#iterations: 19
currently lose_sum: 99.55675691366196
time_elpased: 1.947
start validation test
0.589278350515
0.600876687046
0.536070803746
0.566626781247
0.589371764571
65.688
batch start
#iterations: 20
currently lose_sum: 99.52297741174698
time_elpased: 1.976
batch start
#iterations: 21
currently lose_sum: 99.58728820085526
time_elpased: 1.921
batch start
#iterations: 22
currently lose_sum: 99.53866237401962
time_elpased: 1.946
batch start
#iterations: 23
currently lose_sum: 99.51482105255127
time_elpased: 1.958
batch start
#iterations: 24
currently lose_sum: 99.39165729284286
time_elpased: 1.926
batch start
#iterations: 25
currently lose_sum: 99.57812434434891
time_elpased: 1.929
batch start
#iterations: 26
currently lose_sum: 99.40351730585098
time_elpased: 1.932
batch start
#iterations: 27
currently lose_sum: 99.43759971857071
time_elpased: 1.981
batch start
#iterations: 28
currently lose_sum: 99.39314246177673
time_elpased: 1.935
batch start
#iterations: 29
currently lose_sum: 99.38610517978668
time_elpased: 1.956
batch start
#iterations: 30
currently lose_sum: 99.34713935852051
time_elpased: 1.932
batch start
#iterations: 31
currently lose_sum: 99.43026101589203
time_elpased: 1.951
batch start
#iterations: 32
currently lose_sum: 99.3765252828598
time_elpased: 1.94
batch start
#iterations: 33
currently lose_sum: 99.38122880458832
time_elpased: 1.959
batch start
#iterations: 34
currently lose_sum: 99.32784003019333
time_elpased: 1.92
batch start
#iterations: 35
currently lose_sum: 99.42204523086548
time_elpased: 1.935
batch start
#iterations: 36
currently lose_sum: 99.01260703802109
time_elpased: 1.935
batch start
#iterations: 37
currently lose_sum: 99.55953997373581
time_elpased: 1.981
batch start
#iterations: 38
currently lose_sum: 99.12228411436081
time_elpased: 1.96
batch start
#iterations: 39
currently lose_sum: 99.26971232891083
time_elpased: 1.981
start validation test
0.601030927835
0.63070210234
0.490892250695
0.552083333333
0.60122429327
65.308
batch start
#iterations: 40
currently lose_sum: 99.37551993131638
time_elpased: 1.981
batch start
#iterations: 41
currently lose_sum: 99.3467897772789
time_elpased: 1.997
batch start
#iterations: 42
currently lose_sum: 99.2917617559433
time_elpased: 1.979
batch start
#iterations: 43
currently lose_sum: 99.03249275684357
time_elpased: 1.979
batch start
#iterations: 44
currently lose_sum: 99.2971225976944
time_elpased: 1.942
batch start
#iterations: 45
currently lose_sum: 99.18293207883835
time_elpased: 1.966
batch start
#iterations: 46
currently lose_sum: 99.10350733995438
time_elpased: 1.953
batch start
#iterations: 47
currently lose_sum: 99.34229153394699
time_elpased: 1.967
batch start
#iterations: 48
currently lose_sum: 99.24610722064972
time_elpased: 1.967
batch start
#iterations: 49
currently lose_sum: 99.27336537837982
time_elpased: 1.936
batch start
#iterations: 50
currently lose_sum: 99.2112153172493
time_elpased: 1.95
batch start
#iterations: 51
currently lose_sum: 99.27685111761093
time_elpased: 1.936
batch start
#iterations: 52
currently lose_sum: 99.43372130393982
time_elpased: 1.967
batch start
#iterations: 53
currently lose_sum: 99.00528794527054
time_elpased: 1.944
batch start
#iterations: 54
currently lose_sum: 99.12957793474197
time_elpased: 1.989
batch start
#iterations: 55
currently lose_sum: 99.3265123963356
time_elpased: 1.921
batch start
#iterations: 56
currently lose_sum: 99.2334936261177
time_elpased: 1.946
batch start
#iterations: 57
currently lose_sum: 98.98313277959824
time_elpased: 1.937
batch start
#iterations: 58
currently lose_sum: 99.10294234752655
time_elpased: 1.952
batch start
#iterations: 59
currently lose_sum: 99.02982610464096
time_elpased: 1.949
start validation test
0.596907216495
0.638730437326
0.449418544818
0.527606620756
0.597166155607
65.241
batch start
#iterations: 60
currently lose_sum: 99.01417237520218
time_elpased: 1.961
batch start
#iterations: 61
currently lose_sum: 99.38162821531296
time_elpased: 1.936
batch start
#iterations: 62
currently lose_sum: 98.99709779024124
time_elpased: 1.985
batch start
#iterations: 63
currently lose_sum: 99.28779417276382
time_elpased: 1.968
batch start
#iterations: 64
currently lose_sum: 99.10252606868744
time_elpased: 1.965
batch start
#iterations: 65
currently lose_sum: 98.945845246315
time_elpased: 1.943
batch start
#iterations: 66
currently lose_sum: 99.07596057653427
time_elpased: 1.946
batch start
#iterations: 67
currently lose_sum: 99.14546144008636
time_elpased: 1.971
batch start
#iterations: 68
currently lose_sum: 99.02481496334076
time_elpased: 1.989
batch start
#iterations: 69
currently lose_sum: 99.02160638570786
time_elpased: 1.955
batch start
#iterations: 70
currently lose_sum: 99.02355206012726
time_elpased: 1.953
batch start
#iterations: 71
currently lose_sum: 99.06556385755539
time_elpased: 1.947
batch start
#iterations: 72
currently lose_sum: 99.17329955101013
time_elpased: 1.983
batch start
#iterations: 73
currently lose_sum: 98.843845307827
time_elpased: 1.942
batch start
#iterations: 74
currently lose_sum: 99.04965060949326
time_elpased: 1.979
batch start
#iterations: 75
currently lose_sum: 99.06165897846222
time_elpased: 2.058
batch start
#iterations: 76
currently lose_sum: 99.02207654714584
time_elpased: 2.015
batch start
#iterations: 77
currently lose_sum: 99.05445259809494
time_elpased: 1.956
batch start
#iterations: 78
currently lose_sum: 98.88822263479233
time_elpased: 1.923
batch start
#iterations: 79
currently lose_sum: 98.96257984638214
time_elpased: 1.916
start validation test
0.603144329897
0.628240976106
0.508696099619
0.562183679272
0.603310148333
64.981
batch start
#iterations: 80
currently lose_sum: 98.78570026159286
time_elpased: 1.996
batch start
#iterations: 81
currently lose_sum: 98.84741473197937
time_elpased: 1.933
batch start
#iterations: 82
currently lose_sum: 98.95499378442764
time_elpased: 1.954
batch start
#iterations: 83
currently lose_sum: 99.0620247721672
time_elpased: 1.952
batch start
#iterations: 84
currently lose_sum: 98.88173055648804
time_elpased: 1.937
batch start
#iterations: 85
currently lose_sum: 98.88078570365906
time_elpased: 1.927
batch start
#iterations: 86
currently lose_sum: 99.09488600492477
time_elpased: 1.939
batch start
#iterations: 87
currently lose_sum: 99.06932014226913
time_elpased: 1.937
batch start
#iterations: 88
currently lose_sum: 99.02309268712997
time_elpased: 1.933
batch start
#iterations: 89
currently lose_sum: 98.73344814777374
time_elpased: 1.942
batch start
#iterations: 90
currently lose_sum: 98.86742353439331
time_elpased: 1.93
batch start
#iterations: 91
currently lose_sum: 98.92812281847
time_elpased: 1.929
batch start
#iterations: 92
currently lose_sum: 98.79682290554047
time_elpased: 1.935
batch start
#iterations: 93
currently lose_sum: 98.92309701442719
time_elpased: 2.076
batch start
#iterations: 94
currently lose_sum: 98.92486363649368
time_elpased: 1.96
batch start
#iterations: 95
currently lose_sum: 98.94253653287888
time_elpased: 1.947
batch start
#iterations: 96
currently lose_sum: 98.87609696388245
time_elpased: 1.929
batch start
#iterations: 97
currently lose_sum: 98.87058913707733
time_elpased: 1.93
batch start
#iterations: 98
currently lose_sum: 98.58528417348862
time_elpased: 1.914
batch start
#iterations: 99
currently lose_sum: 98.79621911048889
time_elpased: 1.965
start validation test
0.595979381443
0.635005029458
0.454769990738
0.529983209403
0.596227296309
65.102
batch start
#iterations: 100
currently lose_sum: 98.91578298807144
time_elpased: 1.986
batch start
#iterations: 101
currently lose_sum: 99.027004301548
time_elpased: 1.947
batch start
#iterations: 102
currently lose_sum: 98.71416366100311
time_elpased: 1.965
batch start
#iterations: 103
currently lose_sum: 98.9835911989212
time_elpased: 1.961
batch start
#iterations: 104
currently lose_sum: 99.04491800069809
time_elpased: 1.931
batch start
#iterations: 105
currently lose_sum: 98.93017679452896
time_elpased: 1.95
batch start
#iterations: 106
currently lose_sum: 98.86872053146362
time_elpased: 1.931
batch start
#iterations: 107
currently lose_sum: 99.08025485277176
time_elpased: 1.957
batch start
#iterations: 108
currently lose_sum: 98.94210374355316
time_elpased: 1.936
batch start
#iterations: 109
currently lose_sum: 98.73801612854004
time_elpased: 1.939
batch start
#iterations: 110
currently lose_sum: 98.8543609380722
time_elpased: 1.945
batch start
#iterations: 111
currently lose_sum: 98.93082106113434
time_elpased: 1.987
batch start
#iterations: 112
currently lose_sum: 99.02745938301086
time_elpased: 1.941
batch start
#iterations: 113
currently lose_sum: 98.89683431386948
time_elpased: 1.952
batch start
#iterations: 114
currently lose_sum: 98.71740978956223
time_elpased: 1.934
batch start
#iterations: 115
currently lose_sum: 98.79436242580414
time_elpased: 1.963
batch start
#iterations: 116
currently lose_sum: 99.00237262248993
time_elpased: 1.943
batch start
#iterations: 117
currently lose_sum: 99.12724381685257
time_elpased: 1.944
batch start
#iterations: 118
currently lose_sum: 99.00411081314087
time_elpased: 1.931
batch start
#iterations: 119
currently lose_sum: 98.97664391994476
time_elpased: 1.938
start validation test
0.601494845361
0.636363636364
0.476896161367
0.545208541679
0.601713597569
65.028
batch start
#iterations: 120
currently lose_sum: 98.70208859443665
time_elpased: 1.951
batch start
#iterations: 121
currently lose_sum: 98.84653037786484
time_elpased: 1.95
batch start
#iterations: 122
currently lose_sum: 98.93585884571075
time_elpased: 1.951
batch start
#iterations: 123
currently lose_sum: 98.85248225927353
time_elpased: 1.949
batch start
#iterations: 124
currently lose_sum: 98.96725046634674
time_elpased: 1.913
batch start
#iterations: 125
currently lose_sum: 99.0252217054367
time_elpased: 1.955
batch start
#iterations: 126
currently lose_sum: 98.77090436220169
time_elpased: 1.985
batch start
#iterations: 127
currently lose_sum: 98.82249236106873
time_elpased: 1.959
batch start
#iterations: 128
currently lose_sum: 98.66678184270859
time_elpased: 1.989
batch start
#iterations: 129
currently lose_sum: 98.63358414173126
time_elpased: 1.925
batch start
#iterations: 130
currently lose_sum: 99.06023865938187
time_elpased: 1.955
batch start
#iterations: 131
currently lose_sum: 98.692327439785
time_elpased: 1.951
batch start
#iterations: 132
currently lose_sum: 98.95201110839844
time_elpased: 1.943
batch start
#iterations: 133
currently lose_sum: 98.59999519586563
time_elpased: 1.91
batch start
#iterations: 134
currently lose_sum: 98.77728831768036
time_elpased: 1.954
batch start
#iterations: 135
currently lose_sum: 98.7185406088829
time_elpased: 2.023
batch start
#iterations: 136
currently lose_sum: 98.96307873725891
time_elpased: 1.973
batch start
#iterations: 137
currently lose_sum: 98.79464483261108
time_elpased: 1.933
batch start
#iterations: 138
currently lose_sum: 98.96234714984894
time_elpased: 1.952
batch start
#iterations: 139
currently lose_sum: 98.88161969184875
time_elpased: 1.997
start validation test
0.604432989691
0.628506730406
0.51415045796
0.565606249292
0.6045914946
64.899
batch start
#iterations: 140
currently lose_sum: 98.96898227930069
time_elpased: 2.092
batch start
#iterations: 141
currently lose_sum: 98.54220712184906
time_elpased: 1.925
batch start
#iterations: 142
currently lose_sum: 99.01737153530121
time_elpased: 1.923
batch start
#iterations: 143
currently lose_sum: 98.63730561733246
time_elpased: 1.954
batch start
#iterations: 144
currently lose_sum: 98.930135846138
time_elpased: 1.951
batch start
#iterations: 145
currently lose_sum: 98.76975518465042
time_elpased: 1.992
batch start
#iterations: 146
currently lose_sum: 98.91922360658646
time_elpased: 1.987
batch start
#iterations: 147
currently lose_sum: 98.75432527065277
time_elpased: 1.974
batch start
#iterations: 148
currently lose_sum: 98.81714099645615
time_elpased: 2.019
batch start
#iterations: 149
currently lose_sum: 98.81351339817047
time_elpased: 1.997
batch start
#iterations: 150
currently lose_sum: 98.78539377450943
time_elpased: 1.934
batch start
#iterations: 151
currently lose_sum: 99.047223508358
time_elpased: 1.99
batch start
#iterations: 152
currently lose_sum: 99.01327973604202
time_elpased: 1.967
batch start
#iterations: 153
currently lose_sum: 98.63821065425873
time_elpased: 1.924
batch start
#iterations: 154
currently lose_sum: 98.91214215755463
time_elpased: 1.947
batch start
#iterations: 155
currently lose_sum: 98.76336348056793
time_elpased: 1.941
batch start
#iterations: 156
currently lose_sum: 98.92358237504959
time_elpased: 1.953
batch start
#iterations: 157
currently lose_sum: 98.97250765562057
time_elpased: 1.947
batch start
#iterations: 158
currently lose_sum: 98.93305724859238
time_elpased: 1.912
batch start
#iterations: 159
currently lose_sum: 99.07494127750397
time_elpased: 1.935
start validation test
0.605051546392
0.629620285102
0.513635895853
0.565744729086
0.605212040666
64.919
batch start
#iterations: 160
currently lose_sum: 98.81408035755157
time_elpased: 2.002
batch start
#iterations: 161
currently lose_sum: 98.87104725837708
time_elpased: 1.941
batch start
#iterations: 162
currently lose_sum: 98.63438099622726
time_elpased: 1.948
batch start
#iterations: 163
currently lose_sum: 98.84452509880066
time_elpased: 1.955
batch start
#iterations: 164
currently lose_sum: 98.768514752388
time_elpased: 1.951
batch start
#iterations: 165
currently lose_sum: 98.98330909013748
time_elpased: 1.95
batch start
#iterations: 166
currently lose_sum: 98.54163432121277
time_elpased: 2.006
batch start
#iterations: 167
currently lose_sum: 98.87213218212128
time_elpased: 1.934
batch start
#iterations: 168
currently lose_sum: 98.78335601091385
time_elpased: 1.923
batch start
#iterations: 169
currently lose_sum: 98.86533463001251
time_elpased: 2.005
batch start
#iterations: 170
currently lose_sum: 98.7123532295227
time_elpased: 1.991
batch start
#iterations: 171
currently lose_sum: 98.80828708410263
time_elpased: 2.021
batch start
#iterations: 172
currently lose_sum: 98.82697021961212
time_elpased: 1.97
batch start
#iterations: 173
currently lose_sum: 98.63132929801941
time_elpased: 2.111
batch start
#iterations: 174
currently lose_sum: 98.91929787397385
time_elpased: 1.912
batch start
#iterations: 175
currently lose_sum: 98.44907265901566
time_elpased: 1.937
batch start
#iterations: 176
currently lose_sum: 98.6391744017601
time_elpased: 1.926
batch start
#iterations: 177
currently lose_sum: 98.86750411987305
time_elpased: 1.927
batch start
#iterations: 178
currently lose_sum: 98.79776376485825
time_elpased: 1.921
batch start
#iterations: 179
currently lose_sum: 98.71759331226349
time_elpased: 1.912
start validation test
0.602113402062
0.625313597592
0.513018421323
0.563627112895
0.602269822042
65.034
batch start
#iterations: 180
currently lose_sum: 98.76179707050323
time_elpased: 1.917
batch start
#iterations: 181
currently lose_sum: 98.91583728790283
time_elpased: 1.927
batch start
#iterations: 182
currently lose_sum: 98.84645771980286
time_elpased: 1.936
batch start
#iterations: 183
currently lose_sum: 98.6461837887764
time_elpased: 1.933
batch start
#iterations: 184
currently lose_sum: 98.62788891792297
time_elpased: 1.909
batch start
#iterations: 185
currently lose_sum: 98.84384149312973
time_elpased: 1.933
batch start
#iterations: 186
currently lose_sum: 98.75153052806854
time_elpased: 1.95
batch start
#iterations: 187
currently lose_sum: 98.65991991758347
time_elpased: 1.98
batch start
#iterations: 188
currently lose_sum: 98.9258713722229
time_elpased: 1.915
batch start
#iterations: 189
currently lose_sum: 98.68306851387024
time_elpased: 1.916
batch start
#iterations: 190
currently lose_sum: 98.88200676441193
time_elpased: 1.919
batch start
#iterations: 191
currently lose_sum: 98.82117682695389
time_elpased: 1.915
batch start
#iterations: 192
currently lose_sum: 98.86249035596848
time_elpased: 1.912
batch start
#iterations: 193
currently lose_sum: 98.84495097398758
time_elpased: 1.938
batch start
#iterations: 194
currently lose_sum: 98.7260952591896
time_elpased: 1.937
batch start
#iterations: 195
currently lose_sum: 98.65204626321793
time_elpased: 1.906
batch start
#iterations: 196
currently lose_sum: 98.69953328371048
time_elpased: 1.963
batch start
#iterations: 197
currently lose_sum: 98.58245813846588
time_elpased: 1.916
batch start
#iterations: 198
currently lose_sum: 98.71336090564728
time_elpased: 1.922
batch start
#iterations: 199
currently lose_sum: 98.47349029779434
time_elpased: 1.923
start validation test
0.605463917526
0.632702946095
0.506123289081
0.562378502001
0.605638325321
64.788
batch start
#iterations: 200
currently lose_sum: 98.8571942448616
time_elpased: 1.94
batch start
#iterations: 201
currently lose_sum: 98.59868961572647
time_elpased: 1.902
batch start
#iterations: 202
currently lose_sum: 98.69578814506531
time_elpased: 1.916
batch start
#iterations: 203
currently lose_sum: 98.48440772294998
time_elpased: 1.916
batch start
#iterations: 204
currently lose_sum: 98.89485681056976
time_elpased: 1.914
batch start
#iterations: 205
currently lose_sum: 98.80073481798172
time_elpased: 1.981
batch start
#iterations: 206
currently lose_sum: 98.64047491550446
time_elpased: 1.968
batch start
#iterations: 207
currently lose_sum: 98.63195025920868
time_elpased: 1.983
batch start
#iterations: 208
currently lose_sum: 98.61230301856995
time_elpased: 1.999
batch start
#iterations: 209
currently lose_sum: 98.53900229930878
time_elpased: 1.995
batch start
#iterations: 210
currently lose_sum: 98.6086477637291
time_elpased: 2.029
batch start
#iterations: 211
currently lose_sum: 98.69150656461716
time_elpased: 2.033
batch start
#iterations: 212
currently lose_sum: 98.69874793291092
time_elpased: 2.015
batch start
#iterations: 213
currently lose_sum: 98.78612321615219
time_elpased: 1.998
batch start
#iterations: 214
currently lose_sum: 98.60094797611237
time_elpased: 2.032
batch start
#iterations: 215
currently lose_sum: 98.54864114522934
time_elpased: 2.041
batch start
#iterations: 216
currently lose_sum: 98.52634942531586
time_elpased: 1.951
batch start
#iterations: 217
currently lose_sum: 98.89927345514297
time_elpased: 1.934
batch start
#iterations: 218
currently lose_sum: 98.53822124004364
time_elpased: 1.928
batch start
#iterations: 219
currently lose_sum: 98.9899954199791
time_elpased: 1.935
start validation test
0.603195876289
0.633620119126
0.492641761861
0.554307549792
0.603389971088
64.972
batch start
#iterations: 220
currently lose_sum: 98.5623607635498
time_elpased: 1.925
batch start
#iterations: 221
currently lose_sum: 98.52706462144852
time_elpased: 1.924
batch start
#iterations: 222
currently lose_sum: 98.64798617362976
time_elpased: 1.962
batch start
#iterations: 223
currently lose_sum: 98.4609044790268
time_elpased: 1.973
batch start
#iterations: 224
currently lose_sum: 98.68997120857239
time_elpased: 1.996
batch start
#iterations: 225
currently lose_sum: 98.85012495517731
time_elpased: 2.096
batch start
#iterations: 226
currently lose_sum: 98.57379734516144
time_elpased: 2.041
batch start
#iterations: 227
currently lose_sum: 98.77978163957596
time_elpased: 2.001
batch start
#iterations: 228
currently lose_sum: 98.78810584545135
time_elpased: 1.98
batch start
#iterations: 229
currently lose_sum: 98.62252974510193
time_elpased: 1.971
batch start
#iterations: 230
currently lose_sum: 98.73869651556015
time_elpased: 1.927
batch start
#iterations: 231
currently lose_sum: 98.55567121505737
time_elpased: 1.962
batch start
#iterations: 232
currently lose_sum: 98.77663868665695
time_elpased: 1.976
batch start
#iterations: 233
currently lose_sum: 98.67235606908798
time_elpased: 1.97
batch start
#iterations: 234
currently lose_sum: 98.48766309022903
time_elpased: 1.948
batch start
#iterations: 235
currently lose_sum: 98.7525327205658
time_elpased: 1.958
batch start
#iterations: 236
currently lose_sum: 98.89253598451614
time_elpased: 1.977
batch start
#iterations: 237
currently lose_sum: 98.86061018705368
time_elpased: 1.955
batch start
#iterations: 238
currently lose_sum: 98.73833006620407
time_elpased: 1.938
batch start
#iterations: 239
currently lose_sum: 98.78425651788712
time_elpased: 1.957
start validation test
0.60381443299
0.630108904548
0.506123289081
0.561351443899
0.603985944861
64.980
batch start
#iterations: 240
currently lose_sum: 98.43701148033142
time_elpased: 1.984
batch start
#iterations: 241
currently lose_sum: 98.77263009548187
time_elpased: 1.968
batch start
#iterations: 242
currently lose_sum: 98.74375981092453
time_elpased: 1.967
batch start
#iterations: 243
currently lose_sum: 98.93441659212112
time_elpased: 1.958
batch start
#iterations: 244
currently lose_sum: 98.54562544822693
time_elpased: 1.942
batch start
#iterations: 245
currently lose_sum: 98.73476016521454
time_elpased: 1.998
batch start
#iterations: 246
currently lose_sum: 98.72441977262497
time_elpased: 1.948
batch start
#iterations: 247
currently lose_sum: 98.59933638572693
time_elpased: 2.022
batch start
#iterations: 248
currently lose_sum: 98.5531160235405
time_elpased: 1.963
batch start
#iterations: 249
currently lose_sum: 98.72877764701843
time_elpased: 1.971
batch start
#iterations: 250
currently lose_sum: 98.63476824760437
time_elpased: 2.012
batch start
#iterations: 251
currently lose_sum: 98.74272334575653
time_elpased: 2.011
batch start
#iterations: 252
currently lose_sum: 98.34143882989883
time_elpased: 1.952
batch start
#iterations: 253
currently lose_sum: 98.831578373909
time_elpased: 2.023
batch start
#iterations: 254
currently lose_sum: 98.39527535438538
time_elpased: 1.928
batch start
#iterations: 255
currently lose_sum: 98.54802024364471
time_elpased: 1.947
batch start
#iterations: 256
currently lose_sum: 98.55770969390869
time_elpased: 1.957
batch start
#iterations: 257
currently lose_sum: 98.60560983419418
time_elpased: 1.993
batch start
#iterations: 258
currently lose_sum: 98.54792273044586
time_elpased: 1.951
batch start
#iterations: 259
currently lose_sum: 98.38193899393082
time_elpased: 1.966
start validation test
0.605154639175
0.621128253445
0.542760111145
0.579305799649
0.605264182393
64.744
batch start
#iterations: 260
currently lose_sum: 98.75231230258942
time_elpased: 1.948
batch start
#iterations: 261
currently lose_sum: 98.51198881864548
time_elpased: 1.976
batch start
#iterations: 262
currently lose_sum: 98.31363302469254
time_elpased: 2.029
batch start
#iterations: 263
currently lose_sum: 98.61478853225708
time_elpased: 1.945
batch start
#iterations: 264
currently lose_sum: 98.85905289649963
time_elpased: 1.974
batch start
#iterations: 265
currently lose_sum: 98.83468401432037
time_elpased: 1.941
batch start
#iterations: 266
currently lose_sum: 98.69461071491241
time_elpased: 1.979
batch start
#iterations: 267
currently lose_sum: 98.44364809989929
time_elpased: 2.051
batch start
#iterations: 268
currently lose_sum: 98.61710220575333
time_elpased: 1.938
batch start
#iterations: 269
currently lose_sum: 98.71499347686768
time_elpased: 1.934
batch start
#iterations: 270
currently lose_sum: 98.64812457561493
time_elpased: 1.958
batch start
#iterations: 271
currently lose_sum: 98.6002865433693
time_elpased: 1.943
batch start
#iterations: 272
currently lose_sum: 98.62918275594711
time_elpased: 1.942
batch start
#iterations: 273
currently lose_sum: 98.60635018348694
time_elpased: 1.928
batch start
#iterations: 274
currently lose_sum: 98.59765070676804
time_elpased: 1.942
batch start
#iterations: 275
currently lose_sum: 98.81299203634262
time_elpased: 1.993
batch start
#iterations: 276
currently lose_sum: 98.69099962711334
time_elpased: 1.926
batch start
#iterations: 277
currently lose_sum: 98.49846982955933
time_elpased: 1.964
batch start
#iterations: 278
currently lose_sum: 98.47890317440033
time_elpased: 1.908
batch start
#iterations: 279
currently lose_sum: 98.63620465993881
time_elpased: 1.964
start validation test
0.607268041237
0.632717611336
0.514665020068
0.56761818285
0.607430620124
64.731
batch start
#iterations: 280
currently lose_sum: 98.52201217412949
time_elpased: 1.992
batch start
#iterations: 281
currently lose_sum: 98.58047592639923
time_elpased: 1.944
batch start
#iterations: 282
currently lose_sum: 98.72243410348892
time_elpased: 1.958
batch start
#iterations: 283
currently lose_sum: 98.7440635561943
time_elpased: 1.982
batch start
#iterations: 284
currently lose_sum: 98.43156123161316
time_elpased: 1.953
batch start
#iterations: 285
currently lose_sum: 98.52631652355194
time_elpased: 1.952
batch start
#iterations: 286
currently lose_sum: 98.49741101264954
time_elpased: 1.949
batch start
#iterations: 287
currently lose_sum: 98.56180316209793
time_elpased: 1.963
batch start
#iterations: 288
currently lose_sum: 98.38368493318558
time_elpased: 1.932
batch start
#iterations: 289
currently lose_sum: 98.52260398864746
time_elpased: 1.994
batch start
#iterations: 290
currently lose_sum: 98.8239620923996
time_elpased: 1.989
batch start
#iterations: 291
currently lose_sum: 98.55342483520508
time_elpased: 1.967
batch start
#iterations: 292
currently lose_sum: 98.26334416866302
time_elpased: 1.975
batch start
#iterations: 293
currently lose_sum: 98.71328675746918
time_elpased: 1.979
batch start
#iterations: 294
currently lose_sum: 98.62599617242813
time_elpased: 1.932
batch start
#iterations: 295
currently lose_sum: 98.57144671678543
time_elpased: 2.003
batch start
#iterations: 296
currently lose_sum: 98.68680113554001
time_elpased: 1.979
batch start
#iterations: 297
currently lose_sum: 98.59592801332474
time_elpased: 2.007
batch start
#iterations: 298
currently lose_sum: 98.41948807239532
time_elpased: 2.02
batch start
#iterations: 299
currently lose_sum: 98.69931763410568
time_elpased: 1.987
start validation test
0.606907216495
0.636221498371
0.502521354327
0.561522539098
0.607090481976
64.832
batch start
#iterations: 300
currently lose_sum: 98.47204864025116
time_elpased: 1.949
batch start
#iterations: 301
currently lose_sum: 98.57958775758743
time_elpased: 1.952
batch start
#iterations: 302
currently lose_sum: 98.5056272149086
time_elpased: 1.953
batch start
#iterations: 303
currently lose_sum: 98.54533195495605
time_elpased: 1.937
batch start
#iterations: 304
currently lose_sum: 98.54602432250977
time_elpased: 1.973
batch start
#iterations: 305
currently lose_sum: 98.74202048778534
time_elpased: 1.95
batch start
#iterations: 306
currently lose_sum: 98.35877329111099
time_elpased: 1.975
batch start
#iterations: 307
currently lose_sum: 98.71894943714142
time_elpased: 1.942
batch start
#iterations: 308
currently lose_sum: 98.40120935440063
time_elpased: 1.925
batch start
#iterations: 309
currently lose_sum: 98.77712613344193
time_elpased: 1.925
batch start
#iterations: 310
currently lose_sum: 98.57041472196579
time_elpased: 1.947
batch start
#iterations: 311
currently lose_sum: 98.52631276845932
time_elpased: 1.92
batch start
#iterations: 312
currently lose_sum: 98.6013126373291
time_elpased: 1.951
batch start
#iterations: 313
currently lose_sum: 98.46259397268295
time_elpased: 1.924
batch start
#iterations: 314
currently lose_sum: 98.66568571329117
time_elpased: 1.943
batch start
#iterations: 315
currently lose_sum: 98.61629122495651
time_elpased: 1.939
batch start
#iterations: 316
currently lose_sum: 98.5016753077507
time_elpased: 2.036
batch start
#iterations: 317
currently lose_sum: 98.79422986507416
time_elpased: 1.955
batch start
#iterations: 318
currently lose_sum: 98.6925458908081
time_elpased: 1.957
batch start
#iterations: 319
currently lose_sum: 98.85492438077927
time_elpased: 1.966
start validation test
0.606597938144
0.62821301193
0.525676649172
0.5723890632
0.606740007948
64.851
batch start
#iterations: 320
currently lose_sum: 98.46829915046692
time_elpased: 1.965
batch start
#iterations: 321
currently lose_sum: 98.67978554964066
time_elpased: 1.947
batch start
#iterations: 322
currently lose_sum: 98.72803008556366
time_elpased: 1.952
batch start
#iterations: 323
currently lose_sum: 98.64988559484482
time_elpased: 1.936
batch start
#iterations: 324
currently lose_sum: 98.67535382509232
time_elpased: 1.993
batch start
#iterations: 325
currently lose_sum: 98.77049791812897
time_elpased: 1.949
batch start
#iterations: 326
currently lose_sum: 98.64478981494904
time_elpased: 1.992
batch start
#iterations: 327
currently lose_sum: 98.604871571064
time_elpased: 1.961
batch start
#iterations: 328
currently lose_sum: 98.49250835180283
time_elpased: 1.911
batch start
#iterations: 329
currently lose_sum: 98.42333751916885
time_elpased: 1.946
batch start
#iterations: 330
currently lose_sum: 98.52506351470947
time_elpased: 1.962
batch start
#iterations: 331
currently lose_sum: 98.55084085464478
time_elpased: 1.949
batch start
#iterations: 332
currently lose_sum: 98.39000535011292
time_elpased: 1.968
batch start
#iterations: 333
currently lose_sum: 98.49853092432022
time_elpased: 1.951
batch start
#iterations: 334
currently lose_sum: 98.62127155065536
time_elpased: 1.928
batch start
#iterations: 335
currently lose_sum: 98.57996839284897
time_elpased: 1.961
batch start
#iterations: 336
currently lose_sum: 98.58049267530441
time_elpased: 1.95
batch start
#iterations: 337
currently lose_sum: 98.3378164768219
time_elpased: 1.981
batch start
#iterations: 338
currently lose_sum: 98.58896172046661
time_elpased: 1.963
batch start
#iterations: 339
currently lose_sum: 98.69176030158997
time_elpased: 1.953
start validation test
0.602525773196
0.638459414688
0.475969949573
0.545368787218
0.602747961464
65.015
batch start
#iterations: 340
currently lose_sum: 98.85048139095306
time_elpased: 1.945
batch start
#iterations: 341
currently lose_sum: 98.91336834430695
time_elpased: 1.968
batch start
#iterations: 342
currently lose_sum: 98.48416656255722
time_elpased: 1.946
batch start
#iterations: 343
currently lose_sum: 98.61444306373596
time_elpased: 1.956
batch start
#iterations: 344
currently lose_sum: 98.67759191989899
time_elpased: 1.947
batch start
#iterations: 345
currently lose_sum: 98.64592534303665
time_elpased: 1.945
batch start
#iterations: 346
currently lose_sum: 98.47108095884323
time_elpased: 1.955
batch start
#iterations: 347
currently lose_sum: 98.72651135921478
time_elpased: 1.907
batch start
#iterations: 348
currently lose_sum: 98.74350309371948
time_elpased: 1.993
batch start
#iterations: 349
currently lose_sum: 98.6146297454834
time_elpased: 1.98
batch start
#iterations: 350
currently lose_sum: 98.69172859191895
time_elpased: 1.95
batch start
#iterations: 351
currently lose_sum: 98.7506291270256
time_elpased: 1.96
batch start
#iterations: 352
currently lose_sum: 98.56278985738754
time_elpased: 1.96
batch start
#iterations: 353
currently lose_sum: 98.63169556856155
time_elpased: 1.94
batch start
#iterations: 354
currently lose_sum: 98.80587941408157
time_elpased: 1.991
batch start
#iterations: 355
currently lose_sum: 98.68735820055008
time_elpased: 1.921
batch start
#iterations: 356
currently lose_sum: 98.67082649469376
time_elpased: 1.937
batch start
#iterations: 357
currently lose_sum: 98.72686296701431
time_elpased: 1.942
batch start
#iterations: 358
currently lose_sum: 98.66419011354446
time_elpased: 1.904
batch start
#iterations: 359
currently lose_sum: 98.79700070619583
time_elpased: 1.939
start validation test
0.608711340206
0.631040433925
0.526808685808
0.574232991194
0.608855132949
64.803
batch start
#iterations: 360
currently lose_sum: 98.68076831102371
time_elpased: 1.969
batch start
#iterations: 361
currently lose_sum: 98.8419982790947
time_elpased: 1.93
batch start
#iterations: 362
currently lose_sum: 98.53581482172012
time_elpased: 1.959
batch start
#iterations: 363
currently lose_sum: 98.39922213554382
time_elpased: 1.957
batch start
#iterations: 364
currently lose_sum: 98.94114691019058
time_elpased: 1.976
batch start
#iterations: 365
currently lose_sum: 98.52809524536133
time_elpased: 1.961
batch start
#iterations: 366
currently lose_sum: 98.51089215278625
time_elpased: 2.017
batch start
#iterations: 367
currently lose_sum: 98.32804536819458
time_elpased: 2.007
batch start
#iterations: 368
currently lose_sum: 98.55515080690384
time_elpased: 1.982
batch start
#iterations: 369
currently lose_sum: 98.54849869012833
time_elpased: 1.98
batch start
#iterations: 370
currently lose_sum: 98.66591513156891
time_elpased: 1.94
batch start
#iterations: 371
currently lose_sum: 98.63981014490128
time_elpased: 1.926
batch start
#iterations: 372
currently lose_sum: 98.57475113868713
time_elpased: 1.918
batch start
#iterations: 373
currently lose_sum: 98.55723857879639
time_elpased: 1.944
batch start
#iterations: 374
currently lose_sum: 98.6104092001915
time_elpased: 1.907
batch start
#iterations: 375
currently lose_sum: 98.79324823617935
time_elpased: 1.915
batch start
#iterations: 376
currently lose_sum: 98.60144209861755
time_elpased: 1.933
batch start
#iterations: 377
currently lose_sum: 98.44639879465103
time_elpased: 1.959
batch start
#iterations: 378
currently lose_sum: 98.59740871191025
time_elpased: 1.939
batch start
#iterations: 379
currently lose_sum: 98.58134061098099
time_elpased: 1.9
start validation test
0.602010309278
0.630559916274
0.496037871771
0.555267553712
0.602196360238
64.815
batch start
#iterations: 380
currently lose_sum: 98.5431621670723
time_elpased: 1.944
batch start
#iterations: 381
currently lose_sum: 98.4189019203186
time_elpased: 1.937
batch start
#iterations: 382
currently lose_sum: 98.54659962654114
time_elpased: 1.95
batch start
#iterations: 383
currently lose_sum: 98.5321695804596
time_elpased: 1.97
batch start
#iterations: 384
currently lose_sum: 98.43309736251831
time_elpased: 1.926
batch start
#iterations: 385
currently lose_sum: 98.5587465763092
time_elpased: 1.942
batch start
#iterations: 386
currently lose_sum: 98.45690608024597
time_elpased: 1.948
batch start
#iterations: 387
currently lose_sum: 98.61869543790817
time_elpased: 1.957
batch start
#iterations: 388
currently lose_sum: 98.61719644069672
time_elpased: 2.013
batch start
#iterations: 389
currently lose_sum: 98.49044591188431
time_elpased: 1.905
batch start
#iterations: 390
currently lose_sum: 98.5645923614502
time_elpased: 1.957
batch start
#iterations: 391
currently lose_sum: 98.46348786354065
time_elpased: 1.943
batch start
#iterations: 392
currently lose_sum: 98.46839493513107
time_elpased: 1.964
batch start
#iterations: 393
currently lose_sum: 98.60303473472595
time_elpased: 1.958
batch start
#iterations: 394
currently lose_sum: 98.30326515436172
time_elpased: 1.928
batch start
#iterations: 395
currently lose_sum: 98.58723425865173
time_elpased: 1.951
batch start
#iterations: 396
currently lose_sum: 98.67872995138168
time_elpased: 1.993
batch start
#iterations: 397
currently lose_sum: 98.50693494081497
time_elpased: 1.962
batch start
#iterations: 398
currently lose_sum: 98.64588010311127
time_elpased: 1.912
batch start
#iterations: 399
currently lose_sum: 98.711574614048
time_elpased: 1.959
start validation test
0.605670103093
0.621402560789
0.54440670989
0.580362040592
0.605777660429
64.744
acc: 0.603
pre: 0.627
rec: 0.509
F1: 0.562
auc: 0.603
