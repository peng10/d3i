start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.77455508708954
time_elpased: 2.88
batch start
#iterations: 1
currently lose_sum: 100.5237809419632
time_elpased: 2.666
batch start
#iterations: 2
currently lose_sum: 100.4371457695961
time_elpased: 2.617
batch start
#iterations: 3
currently lose_sum: 100.45961421728134
time_elpased: 2.63
batch start
#iterations: 4
currently lose_sum: 100.25519102811813
time_elpased: 2.535
batch start
#iterations: 5
currently lose_sum: 100.19462954998016
time_elpased: 2.306
batch start
#iterations: 6
currently lose_sum: 100.05918377637863
time_elpased: 2.278
batch start
#iterations: 7
currently lose_sum: 100.08741873502731
time_elpased: 2.355
batch start
#iterations: 8
currently lose_sum: 100.16201514005661
time_elpased: 2.381
batch start
#iterations: 9
currently lose_sum: 99.91421961784363
time_elpased: 2.394
batch start
#iterations: 10
currently lose_sum: 99.89737230539322
time_elpased: 2.405
batch start
#iterations: 11
currently lose_sum: 100.00185370445251
time_elpased: 2.382
batch start
#iterations: 12
currently lose_sum: 99.9677307009697
time_elpased: 2.35
batch start
#iterations: 13
currently lose_sum: 99.86952805519104
time_elpased: 2.402
batch start
#iterations: 14
currently lose_sum: 99.73035085201263
time_elpased: 2.313
batch start
#iterations: 15
currently lose_sum: 99.81309312582016
time_elpased: 2.466
batch start
#iterations: 16
currently lose_sum: 99.70817989110947
time_elpased: 2.424
batch start
#iterations: 17
currently lose_sum: 99.79154413938522
time_elpased: 2.417
batch start
#iterations: 18
currently lose_sum: 99.74211031198502
time_elpased: 2.38
batch start
#iterations: 19
currently lose_sum: 99.62935674190521
time_elpased: 2.369
start validation test
0.587164948454
0.588387497413
0.585057116394
0.586717580887
0.587168649078
65.558
batch start
#iterations: 20
currently lose_sum: 99.61791551113129
time_elpased: 2.306
batch start
#iterations: 21
currently lose_sum: 99.57820743322372
time_elpased: 2.342
batch start
#iterations: 22
currently lose_sum: 99.4971421957016
time_elpased: 2.439
batch start
#iterations: 23
currently lose_sum: 99.62462037801743
time_elpased: 2.505
batch start
#iterations: 24
currently lose_sum: 99.60819101333618
time_elpased: 2.39
batch start
#iterations: 25
currently lose_sum: 99.56075811386108
time_elpased: 2.345
batch start
#iterations: 26
currently lose_sum: 99.34362006187439
time_elpased: 2.306
batch start
#iterations: 27
currently lose_sum: 99.42980515956879
time_elpased: 2.343
batch start
#iterations: 28
currently lose_sum: 99.30263298749924
time_elpased: 2.425
batch start
#iterations: 29
currently lose_sum: 99.2754779458046
time_elpased: 2.362
batch start
#iterations: 30
currently lose_sum: 99.33015543222427
time_elpased: 2.271
batch start
#iterations: 31
currently lose_sum: 99.42038244009018
time_elpased: 2.312
batch start
#iterations: 32
currently lose_sum: 99.3978641629219
time_elpased: 2.314
batch start
#iterations: 33
currently lose_sum: 99.43962961435318
time_elpased: 2.31
batch start
#iterations: 34
currently lose_sum: 99.2142602801323
time_elpased: 2.267
batch start
#iterations: 35
currently lose_sum: 99.30461764335632
time_elpased: 2.31
batch start
#iterations: 36
currently lose_sum: 99.3065789937973
time_elpased: 2.331
batch start
#iterations: 37
currently lose_sum: 99.43839067220688
time_elpased: 2.382
batch start
#iterations: 38
currently lose_sum: 99.04107999801636
time_elpased: 2.364
batch start
#iterations: 39
currently lose_sum: 99.26016384363174
time_elpased: 2.289
start validation test
0.600154639175
0.608551174125
0.565400843882
0.586182982129
0.600215654823
65.150
batch start
#iterations: 40
currently lose_sum: 99.13030880689621
time_elpased: 2.353
batch start
#iterations: 41
currently lose_sum: 99.14875334501266
time_elpased: 2.357
batch start
#iterations: 42
currently lose_sum: 99.25405538082123
time_elpased: 2.383
batch start
#iterations: 43
currently lose_sum: 99.16595429182053
time_elpased: 2.367
batch start
#iterations: 44
currently lose_sum: 99.30041033029556
time_elpased: 2.442
batch start
#iterations: 45
currently lose_sum: 99.24436098337173
time_elpased: 2.328
batch start
#iterations: 46
currently lose_sum: 98.91993486881256
time_elpased: 2.324
batch start
#iterations: 47
currently lose_sum: 99.20462316274643
time_elpased: 2.373
batch start
#iterations: 48
currently lose_sum: 99.04249054193497
time_elpased: 2.196
batch start
#iterations: 49
currently lose_sum: 98.97559505701065
time_elpased: 2.22
batch start
#iterations: 50
currently lose_sum: 99.11114716529846
time_elpased: 2.247
batch start
#iterations: 51
currently lose_sum: 99.23834127187729
time_elpased: 2.253
batch start
#iterations: 52
currently lose_sum: 99.1887486577034
time_elpased: 2.27
batch start
#iterations: 53
currently lose_sum: 99.0140683054924
time_elpased: 2.299
batch start
#iterations: 54
currently lose_sum: 99.08752471208572
time_elpased: 2.264
batch start
#iterations: 55
currently lose_sum: 99.32743245363235
time_elpased: 2.299
batch start
#iterations: 56
currently lose_sum: 99.02878844738007
time_elpased: 2.247
batch start
#iterations: 57
currently lose_sum: 99.03040635585785
time_elpased: 2.19
batch start
#iterations: 58
currently lose_sum: 99.13486814498901
time_elpased: 2.244
batch start
#iterations: 59
currently lose_sum: 98.95559632778168
time_elpased: 2.242
start validation test
0.598711340206
0.634765625
0.468251517958
0.538939887474
0.598940382546
65.135
batch start
#iterations: 60
currently lose_sum: 98.85784775018692
time_elpased: 2.231
batch start
#iterations: 61
currently lose_sum: 99.24440932273865
time_elpased: 2.145
batch start
#iterations: 62
currently lose_sum: 98.9944241642952
time_elpased: 2.192
batch start
#iterations: 63
currently lose_sum: 99.03843933343887
time_elpased: 2.2
batch start
#iterations: 64
currently lose_sum: 98.92853665351868
time_elpased: 2.211
batch start
#iterations: 65
currently lose_sum: 99.12605875730515
time_elpased: 2.23
batch start
#iterations: 66
currently lose_sum: 99.01958763599396
time_elpased: 2.242
batch start
#iterations: 67
currently lose_sum: 98.8571497797966
time_elpased: 2.256
batch start
#iterations: 68
currently lose_sum: 98.97340512275696
time_elpased: 2.204
batch start
#iterations: 69
currently lose_sum: 99.03734183311462
time_elpased: 2.229
batch start
#iterations: 70
currently lose_sum: 98.9629921913147
time_elpased: 2.202
batch start
#iterations: 71
currently lose_sum: 98.91516000032425
time_elpased: 2.185
batch start
#iterations: 72
currently lose_sum: 99.12187474966049
time_elpased: 2.155
batch start
#iterations: 73
currently lose_sum: 98.89731949567795
time_elpased: 2.142
batch start
#iterations: 74
currently lose_sum: 99.15817308425903
time_elpased: 2.136
batch start
#iterations: 75
currently lose_sum: 99.00354599952698
time_elpased: 2.185
batch start
#iterations: 76
currently lose_sum: 99.00607681274414
time_elpased: 2.13
batch start
#iterations: 77
currently lose_sum: 99.06014090776443
time_elpased: 2.069
batch start
#iterations: 78
currently lose_sum: 98.97393602132797
time_elpased: 2.09
batch start
#iterations: 79
currently lose_sum: 99.20680278539658
time_elpased: 2.096
start validation test
0.592319587629
0.628555176337
0.454872903159
0.527792704042
0.592560896483
65.059
batch start
#iterations: 80
currently lose_sum: 99.05454444885254
time_elpased: 2.125
batch start
#iterations: 81
currently lose_sum: 98.74360167980194
time_elpased: 2.14
batch start
#iterations: 82
currently lose_sum: 98.9335458278656
time_elpased: 2.113
batch start
#iterations: 83
currently lose_sum: 98.842817902565
time_elpased: 2.152
batch start
#iterations: 84
currently lose_sum: 98.70289099216461
time_elpased: 2.139
batch start
#iterations: 85
currently lose_sum: 98.99985802173615
time_elpased: 2.118
batch start
#iterations: 86
currently lose_sum: 99.00130087137222
time_elpased: 2.123
batch start
#iterations: 87
currently lose_sum: 99.18310803174973
time_elpased: 2.185
batch start
#iterations: 88
currently lose_sum: 98.85215866565704
time_elpased: 2.154
batch start
#iterations: 89
currently lose_sum: 99.00757431983948
time_elpased: 2.136
batch start
#iterations: 90
currently lose_sum: 98.83998906612396
time_elpased: 2.17
batch start
#iterations: 91
currently lose_sum: 99.19188344478607
time_elpased: 2.173
batch start
#iterations: 92
currently lose_sum: 98.79939419031143
time_elpased: 2.159
batch start
#iterations: 93
currently lose_sum: 98.82172679901123
time_elpased: 2.108
batch start
#iterations: 94
currently lose_sum: 98.84015852212906
time_elpased: 2.089
batch start
#iterations: 95
currently lose_sum: 98.87929552793503
time_elpased: 2.114
batch start
#iterations: 96
currently lose_sum: 98.77393513917923
time_elpased: 2.129
batch start
#iterations: 97
currently lose_sum: 98.82752448320389
time_elpased: 2.127
batch start
#iterations: 98
currently lose_sum: 98.88955926895142
time_elpased: 2.16
batch start
#iterations: 99
currently lose_sum: 98.78388714790344
time_elpased: 2.161
start validation test
0.603144329897
0.625092982891
0.518884429351
0.567058426587
0.603292261149
64.803
batch start
#iterations: 100
currently lose_sum: 98.88337403535843
time_elpased: 2.164
batch start
#iterations: 101
currently lose_sum: 98.94489645957947
time_elpased: 2.151
batch start
#iterations: 102
currently lose_sum: 99.00656336545944
time_elpased: 2.129
batch start
#iterations: 103
currently lose_sum: 98.85072487592697
time_elpased: 2.152
batch start
#iterations: 104
currently lose_sum: 98.9062876701355
time_elpased: 2.127
batch start
#iterations: 105
currently lose_sum: 98.7506971359253
time_elpased: 2.108
batch start
#iterations: 106
currently lose_sum: 98.81354576349258
time_elpased: 2.127
batch start
#iterations: 107
currently lose_sum: 98.94547981023788
time_elpased: 2.114
batch start
#iterations: 108
currently lose_sum: 98.8281602859497
time_elpased: 2.099
batch start
#iterations: 109
currently lose_sum: 98.85852831602097
time_elpased: 2.101
batch start
#iterations: 110
currently lose_sum: 98.84092104434967
time_elpased: 2.152
batch start
#iterations: 111
currently lose_sum: 98.84332537651062
time_elpased: 2.147
batch start
#iterations: 112
currently lose_sum: 98.85295927524567
time_elpased: 2.156
batch start
#iterations: 113
currently lose_sum: 98.76302689313889
time_elpased: 2.127
batch start
#iterations: 114
currently lose_sum: 98.58836877346039
time_elpased: 2.16
batch start
#iterations: 115
currently lose_sum: 98.95300149917603
time_elpased: 2.165
batch start
#iterations: 116
currently lose_sum: 98.90080440044403
time_elpased: 2.101
batch start
#iterations: 117
currently lose_sum: 98.59027141332626
time_elpased: 2.118
batch start
#iterations: 118
currently lose_sum: 98.63738852739334
time_elpased: 2.146
batch start
#iterations: 119
currently lose_sum: 98.9965677857399
time_elpased: 2.159
start validation test
0.603762886598
0.624662245149
0.523412575898
0.569572764432
0.603903953962
64.687
batch start
#iterations: 120
currently lose_sum: 98.79914021492004
time_elpased: 2.125
batch start
#iterations: 121
currently lose_sum: 98.86622428894043
time_elpased: 2.138
batch start
#iterations: 122
currently lose_sum: 99.01579928398132
time_elpased: 2.141
batch start
#iterations: 123
currently lose_sum: 98.99840927124023
time_elpased: 2.177
batch start
#iterations: 124
currently lose_sum: 98.99429386854172
time_elpased: 2.125
batch start
#iterations: 125
currently lose_sum: 98.99682825803757
time_elpased: 2.149
batch start
#iterations: 126
currently lose_sum: 98.7387415766716
time_elpased: 2.217
batch start
#iterations: 127
currently lose_sum: 98.87392175197601
time_elpased: 2.17
batch start
#iterations: 128
currently lose_sum: 98.85713583230972
time_elpased: 2.14
batch start
#iterations: 129
currently lose_sum: 99.1215591430664
time_elpased: 2.166
batch start
#iterations: 130
currently lose_sum: 98.88533914089203
time_elpased: 2.165
batch start
#iterations: 131
currently lose_sum: 98.86360615491867
time_elpased: 2.126
batch start
#iterations: 132
currently lose_sum: 98.79666930437088
time_elpased: 2.159
batch start
#iterations: 133
currently lose_sum: 98.75861746072769
time_elpased: 2.167
batch start
#iterations: 134
currently lose_sum: 98.99190336465836
time_elpased: 2.105
batch start
#iterations: 135
currently lose_sum: 98.67543005943298
time_elpased: 2.245
batch start
#iterations: 136
currently lose_sum: 98.58626699447632
time_elpased: 2.177
batch start
#iterations: 137
currently lose_sum: 98.78514862060547
time_elpased: 2.193
batch start
#iterations: 138
currently lose_sum: 98.97637468576431
time_elpased: 2.146
batch start
#iterations: 139
currently lose_sum: 98.91818362474442
time_elpased: 2.158
start validation test
0.603608247423
0.62554192989
0.519707728723
0.567734682406
0.603755547724
64.670
batch start
#iterations: 140
currently lose_sum: 98.7220509648323
time_elpased: 2.268
batch start
#iterations: 141
currently lose_sum: 98.8602010011673
time_elpased: 2.159
batch start
#iterations: 142
currently lose_sum: 98.97009658813477
time_elpased: 2.23
batch start
#iterations: 143
currently lose_sum: 98.79443919658661
time_elpased: 2.216
batch start
#iterations: 144
currently lose_sum: 98.73494869470596
time_elpased: 2.138
batch start
#iterations: 145
currently lose_sum: 98.60764998197556
time_elpased: 2.168
batch start
#iterations: 146
currently lose_sum: 98.7024398446083
time_elpased: 2.159
batch start
#iterations: 147
currently lose_sum: 98.65586066246033
time_elpased: 2.116
batch start
#iterations: 148
currently lose_sum: 98.77358829975128
time_elpased: 2.145
batch start
#iterations: 149
currently lose_sum: 98.61656826734543
time_elpased: 2.105
batch start
#iterations: 150
currently lose_sum: 98.93856531381607
time_elpased: 2.156
batch start
#iterations: 151
currently lose_sum: 98.78533154726028
time_elpased: 2.128
batch start
#iterations: 152
currently lose_sum: 98.88352608680725
time_elpased: 2.164
batch start
#iterations: 153
currently lose_sum: 98.72320878505707
time_elpased: 2.207
batch start
#iterations: 154
currently lose_sum: 98.88579207658768
time_elpased: 2.121
batch start
#iterations: 155
currently lose_sum: 98.66186332702637
time_elpased: 2.124
batch start
#iterations: 156
currently lose_sum: 98.63417136669159
time_elpased: 2.087
batch start
#iterations: 157
currently lose_sum: 98.5785915851593
time_elpased: 2.157
batch start
#iterations: 158
currently lose_sum: 98.64375114440918
time_elpased: 2.132
batch start
#iterations: 159
currently lose_sum: 98.91180694103241
time_elpased: 2.159
start validation test
0.605051546392
0.617040665224
0.557476587424
0.585748269896
0.60513507157
64.662
batch start
#iterations: 160
currently lose_sum: 98.79106199741364
time_elpased: 2.153
batch start
#iterations: 161
currently lose_sum: 98.87558430433273
time_elpased: 2.158
batch start
#iterations: 162
currently lose_sum: 98.64455765485764
time_elpased: 2.192
batch start
#iterations: 163
currently lose_sum: 98.84528529644012
time_elpased: 2.154
batch start
#iterations: 164
currently lose_sum: 98.72704541683197
time_elpased: 2.13
batch start
#iterations: 165
currently lose_sum: 98.82610446214676
time_elpased: 2.156
batch start
#iterations: 166
currently lose_sum: 98.55323612689972
time_elpased: 2.286
batch start
#iterations: 167
currently lose_sum: 98.83866238594055
time_elpased: 2.351
batch start
#iterations: 168
currently lose_sum: 98.72903853654861
time_elpased: 2.27
batch start
#iterations: 169
currently lose_sum: 98.7996296286583
time_elpased: 2.151
batch start
#iterations: 170
currently lose_sum: 98.93445670604706
time_elpased: 2.19
batch start
#iterations: 171
currently lose_sum: 98.66147249937057
time_elpased: 2.187
batch start
#iterations: 172
currently lose_sum: 98.77489924430847
time_elpased: 2.156
batch start
#iterations: 173
currently lose_sum: 98.80743390321732
time_elpased: 2.257
batch start
#iterations: 174
currently lose_sum: 98.63800036907196
time_elpased: 2.2
batch start
#iterations: 175
currently lose_sum: 98.63971865177155
time_elpased: 2.158
batch start
#iterations: 176
currently lose_sum: 98.52300435304642
time_elpased: 2.166
batch start
#iterations: 177
currently lose_sum: 98.48522490262985
time_elpased: 2.157
batch start
#iterations: 178
currently lose_sum: 98.98960834741592
time_elpased: 2.164
batch start
#iterations: 179
currently lose_sum: 98.69943016767502
time_elpased: 2.141
start validation test
0.6
0.631962238705
0.482247607286
0.54704646276
0.600206732487
64.844
batch start
#iterations: 180
currently lose_sum: 98.6484312415123
time_elpased: 2.2
batch start
#iterations: 181
currently lose_sum: 98.72609031200409
time_elpased: 2.149
batch start
#iterations: 182
currently lose_sum: 98.84402596950531
time_elpased: 2.15
batch start
#iterations: 183
currently lose_sum: 98.53742790222168
time_elpased: 2.188
batch start
#iterations: 184
currently lose_sum: 98.98836022615433
time_elpased: 2.184
batch start
#iterations: 185
currently lose_sum: 98.71304476261139
time_elpased: 2.174
batch start
#iterations: 186
currently lose_sum: 98.91114330291748
time_elpased: 2.197
batch start
#iterations: 187
currently lose_sum: 98.54862713813782
time_elpased: 2.199
batch start
#iterations: 188
currently lose_sum: 98.77729034423828
time_elpased: 2.154
batch start
#iterations: 189
currently lose_sum: 98.69848775863647
time_elpased: 2.165
batch start
#iterations: 190
currently lose_sum: 98.71099972724915
time_elpased: 2.15
batch start
#iterations: 191
currently lose_sum: 98.79959809780121
time_elpased: 2.179
batch start
#iterations: 192
currently lose_sum: 98.5825006365776
time_elpased: 2.173
batch start
#iterations: 193
currently lose_sum: 98.67044937610626
time_elpased: 2.171
batch start
#iterations: 194
currently lose_sum: 98.467460334301
time_elpased: 2.155
batch start
#iterations: 195
currently lose_sum: 98.77289074659348
time_elpased: 2.162
batch start
#iterations: 196
currently lose_sum: 98.72977191209793
time_elpased: 2.167
batch start
#iterations: 197
currently lose_sum: 98.65789818763733
time_elpased: 2.173
batch start
#iterations: 198
currently lose_sum: 98.70072311162949
time_elpased: 2.124
batch start
#iterations: 199
currently lose_sum: 98.67975533008575
time_elpased: 2.147
start validation test
0.603144329897
0.616890639481
0.548008644643
0.580413101531
0.603241129096
64.600
batch start
#iterations: 200
currently lose_sum: 98.74316865205765
time_elpased: 2.166
batch start
#iterations: 201
currently lose_sum: 98.63747507333755
time_elpased: 2.119
batch start
#iterations: 202
currently lose_sum: 98.58881771564484
time_elpased: 2.159
batch start
#iterations: 203
currently lose_sum: 98.65425914525986
time_elpased: 2.187
batch start
#iterations: 204
currently lose_sum: 98.50668680667877
time_elpased: 2.144
batch start
#iterations: 205
currently lose_sum: 98.86222183704376
time_elpased: 2.143
batch start
#iterations: 206
currently lose_sum: 98.58727091550827
time_elpased: 2.156
batch start
#iterations: 207
currently lose_sum: 98.70755785703659
time_elpased: 2.16
batch start
#iterations: 208
currently lose_sum: 98.5210235118866
time_elpased: 2.138
batch start
#iterations: 209
currently lose_sum: 98.65434008836746
time_elpased: 2.038
batch start
#iterations: 210
currently lose_sum: 98.6213943362236
time_elpased: 2.043
batch start
#iterations: 211
currently lose_sum: 98.85592490434647
time_elpased: 2.097
batch start
#iterations: 212
currently lose_sum: 98.56872963905334
time_elpased: 2.112
batch start
#iterations: 213
currently lose_sum: 98.6128038764
time_elpased: 2.155
batch start
#iterations: 214
currently lose_sum: 98.68071675300598
time_elpased: 2.066
batch start
#iterations: 215
currently lose_sum: 98.57330518960953
time_elpased: 2.056
batch start
#iterations: 216
currently lose_sum: 98.44675827026367
time_elpased: 2.144
batch start
#iterations: 217
currently lose_sum: 98.48004984855652
time_elpased: 2.081
batch start
#iterations: 218
currently lose_sum: 98.57690960168839
time_elpased: 2.148
batch start
#iterations: 219
currently lose_sum: 98.67696779966354
time_elpased: 2.169
start validation test
0.600515463918
0.637802998459
0.468457342801
0.540168505993
0.600747312318
64.808
batch start
#iterations: 220
currently lose_sum: 98.74565416574478
time_elpased: 2.187
batch start
#iterations: 221
currently lose_sum: 98.47698885202408
time_elpased: 2.137
batch start
#iterations: 222
currently lose_sum: 98.3862134218216
time_elpased: 2.206
batch start
#iterations: 223
currently lose_sum: 98.63243669271469
time_elpased: 2.136
batch start
#iterations: 224
currently lose_sum: 98.61436188220978
time_elpased: 2.175
batch start
#iterations: 225
currently lose_sum: 98.73886626958847
time_elpased: 2.173
batch start
#iterations: 226
currently lose_sum: 98.81793707609177
time_elpased: 2.158
batch start
#iterations: 227
currently lose_sum: 98.38142156600952
time_elpased: 2.148
batch start
#iterations: 228
currently lose_sum: 98.8125981092453
time_elpased: 2.155
batch start
#iterations: 229
currently lose_sum: 98.75438970327377
time_elpased: 2.155
batch start
#iterations: 230
currently lose_sum: 98.36992502212524
time_elpased: 2.191
batch start
#iterations: 231
currently lose_sum: 98.70255017280579
time_elpased: 2.184
batch start
#iterations: 232
currently lose_sum: 98.69988137483597
time_elpased: 2.213
batch start
#iterations: 233
currently lose_sum: 98.74551343917847
time_elpased: 2.166
batch start
#iterations: 234
currently lose_sum: 98.50692665576935
time_elpased: 2.144
batch start
#iterations: 235
currently lose_sum: 98.70937436819077
time_elpased: 2.153
batch start
#iterations: 236
currently lose_sum: 98.92601209878922
time_elpased: 2.132
batch start
#iterations: 237
currently lose_sum: 98.61447709798813
time_elpased: 2.077
batch start
#iterations: 238
currently lose_sum: 98.64324676990509
time_elpased: 2.137
batch start
#iterations: 239
currently lose_sum: 98.55524027347565
time_elpased: 2.212
start validation test
0.602525773196
0.631973684211
0.494288360605
0.554715019923
0.602715800668
64.696
batch start
#iterations: 240
currently lose_sum: 98.7076421380043
time_elpased: 2.209
batch start
#iterations: 241
currently lose_sum: 98.75096583366394
time_elpased: 2.215
batch start
#iterations: 242
currently lose_sum: 98.72039771080017
time_elpased: 2.135
batch start
#iterations: 243
currently lose_sum: 98.63084578514099
time_elpased: 2.163
batch start
#iterations: 244
currently lose_sum: 98.5604567527771
time_elpased: 2.145
batch start
#iterations: 245
currently lose_sum: 98.4501543045044
time_elpased: 2.17
batch start
#iterations: 246
currently lose_sum: 98.70222157239914
time_elpased: 2.153
batch start
#iterations: 247
currently lose_sum: 98.24080085754395
time_elpased: 2.144
batch start
#iterations: 248
currently lose_sum: 98.6916943192482
time_elpased: 2.168
batch start
#iterations: 249
currently lose_sum: 98.64152586460114
time_elpased: 2.149
batch start
#iterations: 250
currently lose_sum: 98.5504161119461
time_elpased: 2.172
batch start
#iterations: 251
currently lose_sum: 98.51939314603806
time_elpased: 2.163
batch start
#iterations: 252
currently lose_sum: 98.74310326576233
time_elpased: 2.149
batch start
#iterations: 253
currently lose_sum: 98.58912968635559
time_elpased: 2.125
batch start
#iterations: 254
currently lose_sum: 98.50188171863556
time_elpased: 2.116
batch start
#iterations: 255
currently lose_sum: 98.48388975858688
time_elpased: 2.145
batch start
#iterations: 256
currently lose_sum: 98.63488614559174
time_elpased: 2.189
batch start
#iterations: 257
currently lose_sum: 98.58811765909195
time_elpased: 2.073
batch start
#iterations: 258
currently lose_sum: 98.4559942483902
time_elpased: 2.044
batch start
#iterations: 259
currently lose_sum: 98.19671094417572
time_elpased: 2.32
start validation test
0.604020618557
0.624831309042
0.524132962849
0.570069397806
0.604160873658
64.430
batch start
#iterations: 260
currently lose_sum: 98.6925390958786
time_elpased: 2.18
batch start
#iterations: 261
currently lose_sum: 98.5303789973259
time_elpased: 2.213
batch start
#iterations: 262
currently lose_sum: 98.52907687425613
time_elpased: 2.195
batch start
#iterations: 263
currently lose_sum: 98.7738618850708
time_elpased: 2.173
batch start
#iterations: 264
currently lose_sum: 98.4867354631424
time_elpased: 2.206
batch start
#iterations: 265
currently lose_sum: 98.6800320148468
time_elpased: 2.253
batch start
#iterations: 266
currently lose_sum: 98.35049057006836
time_elpased: 2.259
batch start
#iterations: 267
currently lose_sum: 98.62590593099594
time_elpased: 2.181
batch start
#iterations: 268
currently lose_sum: 98.59353113174438
time_elpased: 2.22
batch start
#iterations: 269
currently lose_sum: 98.71267992258072
time_elpased: 2.213
batch start
#iterations: 270
currently lose_sum: 98.63506388664246
time_elpased: 2.163
batch start
#iterations: 271
currently lose_sum: 98.55884230136871
time_elpased: 2.222
batch start
#iterations: 272
currently lose_sum: 98.31553655862808
time_elpased: 2.224
batch start
#iterations: 273
currently lose_sum: 98.40510654449463
time_elpased: 2.207
batch start
#iterations: 274
currently lose_sum: 98.72132533788681
time_elpased: 2.209
batch start
#iterations: 275
currently lose_sum: 98.61353427171707
time_elpased: 2.184
batch start
#iterations: 276
currently lose_sum: 98.71598470211029
time_elpased: 2.175
batch start
#iterations: 277
currently lose_sum: 98.54390102624893
time_elpased: 2.227
batch start
#iterations: 278
currently lose_sum: 98.678187251091
time_elpased: 2.145
batch start
#iterations: 279
currently lose_sum: 98.64283961057663
time_elpased: 2.119
start validation test
0.603453608247
0.636978884678
0.484305855717
0.550248465361
0.603662790504
64.699
batch start
#iterations: 280
currently lose_sum: 98.53921329975128
time_elpased: 2.16
batch start
#iterations: 281
currently lose_sum: 98.49764263629913
time_elpased: 2.159
batch start
#iterations: 282
currently lose_sum: 98.60780400037766
time_elpased: 2.15
batch start
#iterations: 283
currently lose_sum: 98.63104772567749
time_elpased: 2.174
batch start
#iterations: 284
currently lose_sum: 98.4939376115799
time_elpased: 2.137
batch start
#iterations: 285
currently lose_sum: 98.48617279529572
time_elpased: 2.214
batch start
#iterations: 286
currently lose_sum: 98.67030030488968
time_elpased: 2.168
batch start
#iterations: 287
currently lose_sum: 98.56949418783188
time_elpased: 2.184
batch start
#iterations: 288
currently lose_sum: 98.45985877513885
time_elpased: 2.147
batch start
#iterations: 289
currently lose_sum: 98.6248477101326
time_elpased: 2.16
batch start
#iterations: 290
currently lose_sum: 98.55497193336487
time_elpased: 2.204
batch start
#iterations: 291
currently lose_sum: 98.59704506397247
time_elpased: 2.165
batch start
#iterations: 292
currently lose_sum: 98.48248732089996
time_elpased: 2.178
batch start
#iterations: 293
currently lose_sum: 98.6627750992775
time_elpased: 2.168
batch start
#iterations: 294
currently lose_sum: 98.63504260778427
time_elpased: 2.178
batch start
#iterations: 295
currently lose_sum: 98.65755224227905
time_elpased: 2.198
batch start
#iterations: 296
currently lose_sum: 98.43873232603073
time_elpased: 2.096
batch start
#iterations: 297
currently lose_sum: 98.51822859048843
time_elpased: 2.194
batch start
#iterations: 298
currently lose_sum: 98.38745850324631
time_elpased: 2.089
batch start
#iterations: 299
currently lose_sum: 98.69038617610931
time_elpased: 2.078
start validation test
0.600103092784
0.63121232418
0.484923330246
0.548480968455
0.600305308622
64.698
batch start
#iterations: 300
currently lose_sum: 98.53370815515518
time_elpased: 2.258
batch start
#iterations: 301
currently lose_sum: 98.74428808689117
time_elpased: 2.185
batch start
#iterations: 302
currently lose_sum: 98.50910085439682
time_elpased: 2.177
batch start
#iterations: 303
currently lose_sum: 98.46583771705627
time_elpased: 2.169
batch start
#iterations: 304
currently lose_sum: 98.45946449041367
time_elpased: 2.167
batch start
#iterations: 305
currently lose_sum: 98.47640550136566
time_elpased: 2.198
batch start
#iterations: 306
currently lose_sum: 98.47613161802292
time_elpased: 2.157
batch start
#iterations: 307
currently lose_sum: 98.71163100004196
time_elpased: 2.169
batch start
#iterations: 308
currently lose_sum: 98.61067080497742
time_elpased: 2.221
batch start
#iterations: 309
currently lose_sum: 98.43997120857239
time_elpased: 2.184
batch start
#iterations: 310
currently lose_sum: 98.63249510526657
time_elpased: 2.172
batch start
#iterations: 311
currently lose_sum: 98.44697159528732
time_elpased: 2.161
batch start
#iterations: 312
currently lose_sum: 98.46962761878967
time_elpased: 2.197
batch start
#iterations: 313
currently lose_sum: 98.45072895288467
time_elpased: 2.147
batch start
#iterations: 314
currently lose_sum: 98.63676518201828
time_elpased: 2.167
batch start
#iterations: 315
currently lose_sum: 98.46230620145798
time_elpased: 2.206
batch start
#iterations: 316
currently lose_sum: 98.45356744527817
time_elpased: 2.174
batch start
#iterations: 317
currently lose_sum: 98.4654204249382
time_elpased: 2.193
batch start
#iterations: 318
currently lose_sum: 98.5945748090744
time_elpased: 2.203
batch start
#iterations: 319
currently lose_sum: 98.7387587428093
time_elpased: 2.181
start validation test
0.600979381443
0.630860927152
0.490171863744
0.551688191348
0.601173921132
64.720
batch start
#iterations: 320
currently lose_sum: 98.47952437400818
time_elpased: 2.135
batch start
#iterations: 321
currently lose_sum: 98.84641289710999
time_elpased: 2.131
batch start
#iterations: 322
currently lose_sum: 98.57843148708344
time_elpased: 2.193
batch start
#iterations: 323
currently lose_sum: 98.85510832071304
time_elpased: 2.13
batch start
#iterations: 324
currently lose_sum: 98.51866269111633
time_elpased: 2.232
batch start
#iterations: 325
currently lose_sum: 98.58333820104599
time_elpased: 2.149
batch start
#iterations: 326
currently lose_sum: 98.44072788953781
time_elpased: 2.208
batch start
#iterations: 327
currently lose_sum: 98.32412928342819
time_elpased: 2.192
batch start
#iterations: 328
currently lose_sum: 98.639368891716
time_elpased: 2.167
batch start
#iterations: 329
currently lose_sum: 98.4388335943222
time_elpased: 2.202
batch start
#iterations: 330
currently lose_sum: 98.47322028875351
time_elpased: 2.209
batch start
#iterations: 331
currently lose_sum: 98.51329851150513
time_elpased: 2.149
batch start
#iterations: 332
currently lose_sum: 98.27682912349701
time_elpased: 2.175
batch start
#iterations: 333
currently lose_sum: 98.51282805204391
time_elpased: 2.166
batch start
#iterations: 334
currently lose_sum: 98.76794248819351
time_elpased: 2.132
batch start
#iterations: 335
currently lose_sum: 98.579092502594
time_elpased: 2.188
batch start
#iterations: 336
currently lose_sum: 98.51271486282349
time_elpased: 2.154
batch start
#iterations: 337
currently lose_sum: 98.90567708015442
time_elpased: 2.117
batch start
#iterations: 338
currently lose_sum: 98.60555976629257
time_elpased: 2.192
batch start
#iterations: 339
currently lose_sum: 98.52462941408157
time_elpased: 2.145
start validation test
0.604639175258
0.618037135279
0.551507666975
0.582880139221
0.604732455815
64.509
batch start
#iterations: 340
currently lose_sum: 98.43703627586365
time_elpased: 2.176
batch start
#iterations: 341
currently lose_sum: 98.43336111307144
time_elpased: 2.125
batch start
#iterations: 342
currently lose_sum: 98.61658030748367
time_elpased: 2.132
batch start
#iterations: 343
currently lose_sum: 98.56718969345093
time_elpased: 2.118
batch start
#iterations: 344
currently lose_sum: 98.3278324007988
time_elpased: 2.09
batch start
#iterations: 345
currently lose_sum: 98.67133104801178
time_elpased: 2.113
batch start
#iterations: 346
currently lose_sum: 98.50972592830658
time_elpased: 2.113
batch start
#iterations: 347
currently lose_sum: 98.61171954870224
time_elpased: 2.287
batch start
#iterations: 348
currently lose_sum: 98.66652721166611
time_elpased: 2.14
batch start
#iterations: 349
currently lose_sum: 98.4301929473877
time_elpased: 2.168
batch start
#iterations: 350
currently lose_sum: 98.71344596147537
time_elpased: 2.121
batch start
#iterations: 351
currently lose_sum: 98.56605362892151
time_elpased: 2.146
batch start
#iterations: 352
currently lose_sum: 98.4153026342392
time_elpased: 2.191
batch start
#iterations: 353
currently lose_sum: 98.51246839761734
time_elpased: 2.151
batch start
#iterations: 354
currently lose_sum: 98.6818916797638
time_elpased: 2.176
batch start
#iterations: 355
currently lose_sum: 98.52033841609955
time_elpased: 2.139
batch start
#iterations: 356
currently lose_sum: 98.69569158554077
time_elpased: 2.154
batch start
#iterations: 357
currently lose_sum: 98.57064288854599
time_elpased: 2.183
batch start
#iterations: 358
currently lose_sum: 98.60104238986969
time_elpased: 2.166
batch start
#iterations: 359
currently lose_sum: 98.56086045503616
time_elpased: 2.095
start validation test
0.603711340206
0.631804599194
0.500463105897
0.558516136442
0.603892608406
64.650
batch start
#iterations: 360
currently lose_sum: 98.55897730588913
time_elpased: 2.186
batch start
#iterations: 361
currently lose_sum: 98.6321160197258
time_elpased: 2.189
batch start
#iterations: 362
currently lose_sum: 98.52917176485062
time_elpased: 2.204
batch start
#iterations: 363
currently lose_sum: 98.46495342254639
time_elpased: 2.168
batch start
#iterations: 364
currently lose_sum: 98.44612711668015
time_elpased: 2.223
batch start
#iterations: 365
currently lose_sum: 98.53050512075424
time_elpased: 2.223
batch start
#iterations: 366
currently lose_sum: 98.68358737230301
time_elpased: 2.173
batch start
#iterations: 367
currently lose_sum: 98.48155397176743
time_elpased: 2.179
batch start
#iterations: 368
currently lose_sum: 98.41353076696396
time_elpased: 2.186
batch start
#iterations: 369
currently lose_sum: 98.6328501701355
time_elpased: 2.176
batch start
#iterations: 370
currently lose_sum: 98.51562976837158
time_elpased: 2.196
batch start
#iterations: 371
currently lose_sum: 98.42226773500443
time_elpased: 2.112
batch start
#iterations: 372
currently lose_sum: 98.32335197925568
time_elpased: 2.223
batch start
#iterations: 373
currently lose_sum: 98.22916030883789
time_elpased: 2.23
batch start
#iterations: 374
currently lose_sum: 98.456099152565
time_elpased: 2.152
batch start
#iterations: 375
currently lose_sum: 98.62042737007141
time_elpased: 2.152
batch start
#iterations: 376
currently lose_sum: 98.5031224489212
time_elpased: 2.165
batch start
#iterations: 377
currently lose_sum: 98.19344490766525
time_elpased: 2.149
batch start
#iterations: 378
currently lose_sum: 98.5919383764267
time_elpased: 2.161
batch start
#iterations: 379
currently lose_sum: 98.49106973409653
time_elpased: 2.122
start validation test
0.604845360825
0.6223893066
0.536688278275
0.576370468612
0.604965021096
64.514
batch start
#iterations: 380
currently lose_sum: 98.61331707239151
time_elpased: 2.162
batch start
#iterations: 381
currently lose_sum: 98.42837536334991
time_elpased: 2.147
batch start
#iterations: 382
currently lose_sum: 98.66154289245605
time_elpased: 2.137
batch start
#iterations: 383
currently lose_sum: 98.62260085344315
time_elpased: 2.154
batch start
#iterations: 384
currently lose_sum: 98.65267735719681
time_elpased: 2.192
batch start
#iterations: 385
currently lose_sum: 98.65861803293228
time_elpased: 2.171
batch start
#iterations: 386
currently lose_sum: 98.6985257267952
time_elpased: 2.257
batch start
#iterations: 387
currently lose_sum: 98.64551359415054
time_elpased: 2.329
batch start
#iterations: 388
currently lose_sum: 98.25294679403305
time_elpased: 2.267
batch start
#iterations: 389
currently lose_sum: 98.73135715723038
time_elpased: 2.246
batch start
#iterations: 390
currently lose_sum: 98.46172612905502
time_elpased: 2.216
batch start
#iterations: 391
currently lose_sum: 98.66494357585907
time_elpased: 2.148
batch start
#iterations: 392
currently lose_sum: 98.5454825758934
time_elpased: 2.242
batch start
#iterations: 393
currently lose_sum: 98.4612484574318
time_elpased: 2.246
batch start
#iterations: 394
currently lose_sum: 98.5601612329483
time_elpased: 2.295
batch start
#iterations: 395
currently lose_sum: 98.45711660385132
time_elpased: 2.279
batch start
#iterations: 396
currently lose_sum: 98.43587619066238
time_elpased: 2.301
batch start
#iterations: 397
currently lose_sum: 98.58482706546783
time_elpased: 2.304
batch start
#iterations: 398
currently lose_sum: 98.71208465099335
time_elpased: 2.266
batch start
#iterations: 399
currently lose_sum: 98.53216433525085
time_elpased: 2.253
start validation test
0.604020618557
0.625663826108
0.521354327467
0.568766138992
0.604165751981
64.546
acc: 0.605
pre: 0.628
rec: 0.517
F1: 0.567
auc: 0.605
