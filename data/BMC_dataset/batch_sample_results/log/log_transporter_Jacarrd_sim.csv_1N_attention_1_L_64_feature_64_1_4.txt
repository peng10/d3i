start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.66456991434097
time_elpased: 1.967
batch start
#iterations: 1
currently lose_sum: 100.42007595300674
time_elpased: 1.797
batch start
#iterations: 2
currently lose_sum: 100.40581923723221
time_elpased: 1.831
batch start
#iterations: 3
currently lose_sum: 100.21562576293945
time_elpased: 1.821
batch start
#iterations: 4
currently lose_sum: 100.39038634300232
time_elpased: 1.806
batch start
#iterations: 5
currently lose_sum: 100.12571293115616
time_elpased: 1.794
batch start
#iterations: 6
currently lose_sum: 100.0423344373703
time_elpased: 1.793
batch start
#iterations: 7
currently lose_sum: 100.04790884256363
time_elpased: 1.826
batch start
#iterations: 8
currently lose_sum: 100.16529709100723
time_elpased: 1.817
batch start
#iterations: 9
currently lose_sum: 99.84945559501648
time_elpased: 1.83
batch start
#iterations: 10
currently lose_sum: 99.9407114982605
time_elpased: 1.838
batch start
#iterations: 11
currently lose_sum: 99.90471988916397
time_elpased: 1.82
batch start
#iterations: 12
currently lose_sum: 99.89502322673798
time_elpased: 1.816
batch start
#iterations: 13
currently lose_sum: 99.83464109897614
time_elpased: 1.834
batch start
#iterations: 14
currently lose_sum: 99.86485439538956
time_elpased: 1.77
batch start
#iterations: 15
currently lose_sum: 99.88171303272247
time_elpased: 1.82
batch start
#iterations: 16
currently lose_sum: 99.74156385660172
time_elpased: 1.79
batch start
#iterations: 17
currently lose_sum: 99.68661189079285
time_elpased: 1.82
batch start
#iterations: 18
currently lose_sum: 99.8287205696106
time_elpased: 1.793
batch start
#iterations: 19
currently lose_sum: 99.70703762769699
time_elpased: 1.822
start validation test
0.590670103093
0.603400093153
0.533292168365
0.566184102704
0.590770838907
65.608
batch start
#iterations: 20
currently lose_sum: 99.7134759426117
time_elpased: 1.841
batch start
#iterations: 21
currently lose_sum: 99.55302888154984
time_elpased: 1.792
batch start
#iterations: 22
currently lose_sum: 99.59365808963776
time_elpased: 1.806
batch start
#iterations: 23
currently lose_sum: 99.5041247010231
time_elpased: 1.801
batch start
#iterations: 24
currently lose_sum: 99.52069193124771
time_elpased: 1.805
batch start
#iterations: 25
currently lose_sum: 99.5022332072258
time_elpased: 1.813
batch start
#iterations: 26
currently lose_sum: 99.53866279125214
time_elpased: 1.807
batch start
#iterations: 27
currently lose_sum: 99.46331888437271
time_elpased: 1.812
batch start
#iterations: 28
currently lose_sum: 99.42710876464844
time_elpased: 1.804
batch start
#iterations: 29
currently lose_sum: 99.49820530414581
time_elpased: 1.824
batch start
#iterations: 30
currently lose_sum: 99.31643480062485
time_elpased: 1.813
batch start
#iterations: 31
currently lose_sum: 99.32855504751205
time_elpased: 1.812
batch start
#iterations: 32
currently lose_sum: 99.43881106376648
time_elpased: 1.825
batch start
#iterations: 33
currently lose_sum: 99.36208194494247
time_elpased: 1.811
batch start
#iterations: 34
currently lose_sum: 99.2132106423378
time_elpased: 1.803
batch start
#iterations: 35
currently lose_sum: 99.38509380817413
time_elpased: 1.797
batch start
#iterations: 36
currently lose_sum: 99.42058700323105
time_elpased: 1.822
batch start
#iterations: 37
currently lose_sum: 99.22213155031204
time_elpased: 1.821
batch start
#iterations: 38
currently lose_sum: 99.1326841711998
time_elpased: 1.808
batch start
#iterations: 39
currently lose_sum: 99.33136403560638
time_elpased: 1.802
start validation test
0.60175257732
0.6271229728
0.50540290213
0.55972190563
0.601921734035
65.303
batch start
#iterations: 40
currently lose_sum: 99.28111666440964
time_elpased: 1.857
batch start
#iterations: 41
currently lose_sum: 99.1617625951767
time_elpased: 1.815
batch start
#iterations: 42
currently lose_sum: 99.19060450792313
time_elpased: 1.804
batch start
#iterations: 43
currently lose_sum: 99.33329612016678
time_elpased: 1.889
batch start
#iterations: 44
currently lose_sum: 99.27861779928207
time_elpased: 1.806
batch start
#iterations: 45
currently lose_sum: 99.26117587089539
time_elpased: 1.781
batch start
#iterations: 46
currently lose_sum: 99.19664818048477
time_elpased: 1.846
batch start
#iterations: 47
currently lose_sum: 99.29985135793686
time_elpased: 1.8
batch start
#iterations: 48
currently lose_sum: 99.2018632888794
time_elpased: 1.822
batch start
#iterations: 49
currently lose_sum: 99.19683861732483
time_elpased: 1.817
batch start
#iterations: 50
currently lose_sum: 99.33146297931671
time_elpased: 1.849
batch start
#iterations: 51
currently lose_sum: 99.07554906606674
time_elpased: 1.829
batch start
#iterations: 52
currently lose_sum: 99.25290298461914
time_elpased: 1.804
batch start
#iterations: 53
currently lose_sum: 99.22772520780563
time_elpased: 1.854
batch start
#iterations: 54
currently lose_sum: 99.2746661901474
time_elpased: 1.823
batch start
#iterations: 55
currently lose_sum: 98.98889982700348
time_elpased: 1.793
batch start
#iterations: 56
currently lose_sum: 99.19077891111374
time_elpased: 1.84
batch start
#iterations: 57
currently lose_sum: 99.19633942842484
time_elpased: 1.809
batch start
#iterations: 58
currently lose_sum: 99.0990498661995
time_elpased: 1.863
batch start
#iterations: 59
currently lose_sum: 99.18851554393768
time_elpased: 1.805
start validation test
0.593453608247
0.652703604806
0.402490480601
0.497931122287
0.593788873472
65.490
batch start
#iterations: 60
currently lose_sum: 99.21986573934555
time_elpased: 1.812
batch start
#iterations: 61
currently lose_sum: 99.28308320045471
time_elpased: 1.831
batch start
#iterations: 62
currently lose_sum: 99.21387392282486
time_elpased: 1.813
batch start
#iterations: 63
currently lose_sum: 99.24011278152466
time_elpased: 1.793
batch start
#iterations: 64
currently lose_sum: 99.23687970638275
time_elpased: 1.792
batch start
#iterations: 65
currently lose_sum: 99.02987253665924
time_elpased: 1.798
batch start
#iterations: 66
currently lose_sum: 99.29905146360397
time_elpased: 1.806
batch start
#iterations: 67
currently lose_sum: 99.04634153842926
time_elpased: 1.79
batch start
#iterations: 68
currently lose_sum: 99.10327231884003
time_elpased: 1.798
batch start
#iterations: 69
currently lose_sum: 99.13335371017456
time_elpased: 1.816
batch start
#iterations: 70
currently lose_sum: 99.00929749011993
time_elpased: 1.8
batch start
#iterations: 71
currently lose_sum: 99.04051756858826
time_elpased: 1.787
batch start
#iterations: 72
currently lose_sum: 99.0154345035553
time_elpased: 1.795
batch start
#iterations: 73
currently lose_sum: 98.96278822422028
time_elpased: 1.806
batch start
#iterations: 74
currently lose_sum: 99.02571576833725
time_elpased: 1.792
batch start
#iterations: 75
currently lose_sum: 99.13935297727585
time_elpased: 1.785
batch start
#iterations: 76
currently lose_sum: 98.84194934368134
time_elpased: 1.803
batch start
#iterations: 77
currently lose_sum: 99.25799894332886
time_elpased: 1.81
batch start
#iterations: 78
currently lose_sum: 98.9735038280487
time_elpased: 1.825
batch start
#iterations: 79
currently lose_sum: 99.09309035539627
time_elpased: 1.835
start validation test
0.606701030928
0.621520903692
0.549243593702
0.583151223776
0.606801906321
64.946
batch start
#iterations: 80
currently lose_sum: 99.0446047782898
time_elpased: 1.812
batch start
#iterations: 81
currently lose_sum: 98.94512236118317
time_elpased: 1.802
batch start
#iterations: 82
currently lose_sum: 99.11589932441711
time_elpased: 1.813
batch start
#iterations: 83
currently lose_sum: 98.78879302740097
time_elpased: 1.786
batch start
#iterations: 84
currently lose_sum: 99.33741772174835
time_elpased: 1.845
batch start
#iterations: 85
currently lose_sum: 99.0383910536766
time_elpased: 1.806
batch start
#iterations: 86
currently lose_sum: 99.12341523170471
time_elpased: 1.807
batch start
#iterations: 87
currently lose_sum: 98.9429343342781
time_elpased: 1.8
batch start
#iterations: 88
currently lose_sum: 99.05511337518692
time_elpased: 1.81
batch start
#iterations: 89
currently lose_sum: 99.02423214912415
time_elpased: 1.817
batch start
#iterations: 90
currently lose_sum: 99.04072403907776
time_elpased: 1.784
batch start
#iterations: 91
currently lose_sum: 99.07419377565384
time_elpased: 1.787
batch start
#iterations: 92
currently lose_sum: 98.99977630376816
time_elpased: 1.827
batch start
#iterations: 93
currently lose_sum: 99.0418268442154
time_elpased: 1.819
batch start
#iterations: 94
currently lose_sum: 98.91492742300034
time_elpased: 1.789
batch start
#iterations: 95
currently lose_sum: 99.173124730587
time_elpased: 1.802
batch start
#iterations: 96
currently lose_sum: 98.9937036037445
time_elpased: 1.853
batch start
#iterations: 97
currently lose_sum: 99.11333018541336
time_elpased: 1.833
batch start
#iterations: 98
currently lose_sum: 99.07460451126099
time_elpased: 1.808
batch start
#iterations: 99
currently lose_sum: 99.11314350366592
time_elpased: 1.847
start validation test
0.604742268041
0.618125216188
0.551713491818
0.583034257749
0.604835368237
64.934
batch start
#iterations: 100
currently lose_sum: 98.81226551532745
time_elpased: 1.836
batch start
#iterations: 101
currently lose_sum: 98.95071578025818
time_elpased: 1.826
batch start
#iterations: 102
currently lose_sum: 99.04248172044754
time_elpased: 1.831
batch start
#iterations: 103
currently lose_sum: 98.92164605855942
time_elpased: 1.829
batch start
#iterations: 104
currently lose_sum: 98.836121737957
time_elpased: 1.824
batch start
#iterations: 105
currently lose_sum: 98.92916572093964
time_elpased: 1.84
batch start
#iterations: 106
currently lose_sum: 99.05540037155151
time_elpased: 1.818
batch start
#iterations: 107
currently lose_sum: 98.96809136867523
time_elpased: 1.81
batch start
#iterations: 108
currently lose_sum: 99.13805890083313
time_elpased: 1.782
batch start
#iterations: 109
currently lose_sum: 98.95868301391602
time_elpased: 1.829
batch start
#iterations: 110
currently lose_sum: 98.92973524332047
time_elpased: 1.832
batch start
#iterations: 111
currently lose_sum: 99.32813513278961
time_elpased: 1.795
batch start
#iterations: 112
currently lose_sum: 98.99685245752335
time_elpased: 1.84
batch start
#iterations: 113
currently lose_sum: 98.85794013738632
time_elpased: 1.821
batch start
#iterations: 114
currently lose_sum: 99.13572931289673
time_elpased: 1.873
batch start
#iterations: 115
currently lose_sum: 98.91787219047546
time_elpased: 1.817
batch start
#iterations: 116
currently lose_sum: 98.88700598478317
time_elpased: 1.862
batch start
#iterations: 117
currently lose_sum: 98.92196452617645
time_elpased: 1.816
batch start
#iterations: 118
currently lose_sum: 98.90654838085175
time_elpased: 1.814
batch start
#iterations: 119
currently lose_sum: 99.01240146160126
time_elpased: 1.817
start validation test
0.602010309278
0.632115435531
0.491406812802
0.552950031845
0.602204490776
65.005
batch start
#iterations: 120
currently lose_sum: 98.97671276330948
time_elpased: 1.853
batch start
#iterations: 121
currently lose_sum: 99.09867149591446
time_elpased: 1.807
batch start
#iterations: 122
currently lose_sum: 99.07369405031204
time_elpased: 1.826
batch start
#iterations: 123
currently lose_sum: 98.91087585687637
time_elpased: 1.834
batch start
#iterations: 124
currently lose_sum: 98.85726273059845
time_elpased: 1.822
batch start
#iterations: 125
currently lose_sum: 98.88251399993896
time_elpased: 1.815
batch start
#iterations: 126
currently lose_sum: 98.9397314786911
time_elpased: 1.858
batch start
#iterations: 127
currently lose_sum: 98.77584600448608
time_elpased: 1.855
batch start
#iterations: 128
currently lose_sum: 98.98131191730499
time_elpased: 1.805
batch start
#iterations: 129
currently lose_sum: 98.80719524621964
time_elpased: 1.785
batch start
#iterations: 130
currently lose_sum: 98.90418022871017
time_elpased: 1.847
batch start
#iterations: 131
currently lose_sum: 99.03576093912125
time_elpased: 1.819
batch start
#iterations: 132
currently lose_sum: 99.22027403116226
time_elpased: 1.84
batch start
#iterations: 133
currently lose_sum: 98.71556335687637
time_elpased: 1.797
batch start
#iterations: 134
currently lose_sum: 98.94021141529083
time_elpased: 1.819
batch start
#iterations: 135
currently lose_sum: 98.83211499452591
time_elpased: 1.822
batch start
#iterations: 136
currently lose_sum: 98.9343169927597
time_elpased: 1.834
batch start
#iterations: 137
currently lose_sum: 99.04831522703171
time_elpased: 1.811
batch start
#iterations: 138
currently lose_sum: 98.97477853298187
time_elpased: 1.821
batch start
#iterations: 139
currently lose_sum: 98.8166190981865
time_elpased: 1.848
start validation test
0.602371134021
0.63344437042
0.48924565195
0.552084543026
0.602569743253
64.974
batch start
#iterations: 140
currently lose_sum: 98.85627895593643
time_elpased: 1.905
batch start
#iterations: 141
currently lose_sum: 99.11816251277924
time_elpased: 1.795
batch start
#iterations: 142
currently lose_sum: 99.05288207530975
time_elpased: 1.822
batch start
#iterations: 143
currently lose_sum: 98.94907027482986
time_elpased: 1.822
batch start
#iterations: 144
currently lose_sum: 98.9267852306366
time_elpased: 1.818
batch start
#iterations: 145
currently lose_sum: 98.95364344120026
time_elpased: 1.826
batch start
#iterations: 146
currently lose_sum: 98.72455155849457
time_elpased: 1.815
batch start
#iterations: 147
currently lose_sum: 98.8575907945633
time_elpased: 1.823
batch start
#iterations: 148
currently lose_sum: 98.79684382677078
time_elpased: 1.887
batch start
#iterations: 149
currently lose_sum: 98.93886858224869
time_elpased: 1.806
batch start
#iterations: 150
currently lose_sum: 98.92148524522781
time_elpased: 1.846
batch start
#iterations: 151
currently lose_sum: 98.59500700235367
time_elpased: 1.808
batch start
#iterations: 152
currently lose_sum: 98.68007922172546
time_elpased: 1.828
batch start
#iterations: 153
currently lose_sum: 99.02932727336884
time_elpased: 1.825
batch start
#iterations: 154
currently lose_sum: 98.75120329856873
time_elpased: 1.86
batch start
#iterations: 155
currently lose_sum: 98.71591955423355
time_elpased: 1.844
batch start
#iterations: 156
currently lose_sum: 99.08199948072433
time_elpased: 1.809
batch start
#iterations: 157
currently lose_sum: 98.96795892715454
time_elpased: 1.836
batch start
#iterations: 158
currently lose_sum: 98.82082170248032
time_elpased: 1.814
batch start
#iterations: 159
currently lose_sum: 98.7591615319252
time_elpased: 1.813
start validation test
0.604690721649
0.6343832021
0.497478645673
0.557651266078
0.604878948985
64.889
batch start
#iterations: 160
currently lose_sum: 98.98443442583084
time_elpased: 1.834
batch start
#iterations: 161
currently lose_sum: 98.88768005371094
time_elpased: 1.792
batch start
#iterations: 162
currently lose_sum: 98.84687739610672
time_elpased: 1.78
batch start
#iterations: 163
currently lose_sum: 99.00656920671463
time_elpased: 1.859
batch start
#iterations: 164
currently lose_sum: 98.89525997638702
time_elpased: 1.809
batch start
#iterations: 165
currently lose_sum: 98.99010437726974
time_elpased: 1.806
batch start
#iterations: 166
currently lose_sum: 98.9182910323143
time_elpased: 1.782
batch start
#iterations: 167
currently lose_sum: 99.0977612733841
time_elpased: 1.788
batch start
#iterations: 168
currently lose_sum: 98.98238241672516
time_elpased: 1.817
batch start
#iterations: 169
currently lose_sum: 98.84295457601547
time_elpased: 1.838
batch start
#iterations: 170
currently lose_sum: 98.94512331485748
time_elpased: 1.782
batch start
#iterations: 171
currently lose_sum: 98.69046902656555
time_elpased: 1.796
batch start
#iterations: 172
currently lose_sum: 98.80037027597427
time_elpased: 1.812
batch start
#iterations: 173
currently lose_sum: 98.69196218252182
time_elpased: 1.923
batch start
#iterations: 174
currently lose_sum: 98.85923808813095
time_elpased: 1.832
batch start
#iterations: 175
currently lose_sum: 98.87315726280212
time_elpased: 1.81
batch start
#iterations: 176
currently lose_sum: 99.21761709451675
time_elpased: 1.804
batch start
#iterations: 177
currently lose_sum: 98.67667818069458
time_elpased: 1.839
batch start
#iterations: 178
currently lose_sum: 98.72885566949844
time_elpased: 1.823
batch start
#iterations: 179
currently lose_sum: 98.83073371648788
time_elpased: 1.808
start validation test
0.598969072165
0.646764661312
0.439230215087
0.523167443001
0.599249518367
65.149
batch start
#iterations: 180
currently lose_sum: 98.85417377948761
time_elpased: 1.863
batch start
#iterations: 181
currently lose_sum: 99.06265932321548
time_elpased: 1.8
batch start
#iterations: 182
currently lose_sum: 98.8298978805542
time_elpased: 1.821
batch start
#iterations: 183
currently lose_sum: 98.67425709962845
time_elpased: 1.898
batch start
#iterations: 184
currently lose_sum: 98.8554738163948
time_elpased: 1.807
batch start
#iterations: 185
currently lose_sum: 98.8212366104126
time_elpased: 1.785
batch start
#iterations: 186
currently lose_sum: 98.79888564348221
time_elpased: 1.795
batch start
#iterations: 187
currently lose_sum: 98.92287009954453
time_elpased: 1.812
batch start
#iterations: 188
currently lose_sum: 98.8701222538948
time_elpased: 1.812
batch start
#iterations: 189
currently lose_sum: 98.96848648786545
time_elpased: 1.795
batch start
#iterations: 190
currently lose_sum: 98.77223759889603
time_elpased: 1.8
batch start
#iterations: 191
currently lose_sum: 98.73320192098618
time_elpased: 1.809
batch start
#iterations: 192
currently lose_sum: 98.91145515441895
time_elpased: 1.81
batch start
#iterations: 193
currently lose_sum: 98.90874457359314
time_elpased: 1.831
batch start
#iterations: 194
currently lose_sum: 98.92242461442947
time_elpased: 1.798
batch start
#iterations: 195
currently lose_sum: 98.85227608680725
time_elpased: 1.799
batch start
#iterations: 196
currently lose_sum: 98.97023493051529
time_elpased: 1.839
batch start
#iterations: 197
currently lose_sum: 98.91242462396622
time_elpased: 1.812
batch start
#iterations: 198
currently lose_sum: 98.86758875846863
time_elpased: 1.807
batch start
#iterations: 199
currently lose_sum: 98.81685137748718
time_elpased: 1.824
start validation test
0.606597938144
0.64025292614
0.489760214058
0.554985422741
0.60680306479
64.975
batch start
#iterations: 200
currently lose_sum: 98.65967667102814
time_elpased: 1.875
batch start
#iterations: 201
currently lose_sum: 98.87930554151535
time_elpased: 1.821
batch start
#iterations: 202
currently lose_sum: 99.02447718381882
time_elpased: 1.839
batch start
#iterations: 203
currently lose_sum: 99.00779616832733
time_elpased: 1.813
batch start
#iterations: 204
currently lose_sum: 98.75505638122559
time_elpased: 1.801
batch start
#iterations: 205
currently lose_sum: 98.79276829957962
time_elpased: 1.804
batch start
#iterations: 206
currently lose_sum: 98.6128140091896
time_elpased: 1.843
batch start
#iterations: 207
currently lose_sum: 98.90639168024063
time_elpased: 1.797
batch start
#iterations: 208
currently lose_sum: 98.80528968572617
time_elpased: 1.84
batch start
#iterations: 209
currently lose_sum: 98.79557514190674
time_elpased: 1.792
batch start
#iterations: 210
currently lose_sum: 98.79736572504044
time_elpased: 1.804
batch start
#iterations: 211
currently lose_sum: 98.72952675819397
time_elpased: 1.8
batch start
#iterations: 212
currently lose_sum: 98.72251385450363
time_elpased: 1.821
batch start
#iterations: 213
currently lose_sum: 98.77884310483932
time_elpased: 1.801
batch start
#iterations: 214
currently lose_sum: 98.83208125829697
time_elpased: 1.845
batch start
#iterations: 215
currently lose_sum: 98.63163846731186
time_elpased: 1.803
batch start
#iterations: 216
currently lose_sum: 98.8713189959526
time_elpased: 1.809
batch start
#iterations: 217
currently lose_sum: 98.83694624900818
time_elpased: 1.819
batch start
#iterations: 218
currently lose_sum: 98.55113399028778
time_elpased: 1.797
batch start
#iterations: 219
currently lose_sum: 98.50846117734909
time_elpased: 1.792
start validation test
0.601907216495
0.640938648572
0.466604919214
0.540051217914
0.602144760547
64.918
batch start
#iterations: 220
currently lose_sum: 98.78547888994217
time_elpased: 1.836
batch start
#iterations: 221
currently lose_sum: 98.88993030786514
time_elpased: 1.792
batch start
#iterations: 222
currently lose_sum: 98.7174796462059
time_elpased: 1.799
batch start
#iterations: 223
currently lose_sum: 99.00315356254578
time_elpased: 1.795
batch start
#iterations: 224
currently lose_sum: 98.8295521736145
time_elpased: 1.8
batch start
#iterations: 225
currently lose_sum: 98.79169452190399
time_elpased: 1.841
batch start
#iterations: 226
currently lose_sum: 98.52627789974213
time_elpased: 1.79
batch start
#iterations: 227
currently lose_sum: 98.6791861653328
time_elpased: 1.799
batch start
#iterations: 228
currently lose_sum: 98.9183965921402
time_elpased: 1.809
batch start
#iterations: 229
currently lose_sum: 99.03447544574738
time_elpased: 1.808
batch start
#iterations: 230
currently lose_sum: 98.86651980876923
time_elpased: 1.83
batch start
#iterations: 231
currently lose_sum: 98.83190554380417
time_elpased: 1.784
batch start
#iterations: 232
currently lose_sum: 98.78366953134537
time_elpased: 1.829
batch start
#iterations: 233
currently lose_sum: 98.75242078304291
time_elpased: 1.808
batch start
#iterations: 234
currently lose_sum: 98.76302760839462
time_elpased: 1.845
batch start
#iterations: 235
currently lose_sum: 98.55512583255768
time_elpased: 1.808
batch start
#iterations: 236
currently lose_sum: 98.74626272916794
time_elpased: 1.834
batch start
#iterations: 237
currently lose_sum: 98.79148614406586
time_elpased: 1.833
batch start
#iterations: 238
currently lose_sum: 98.6151927113533
time_elpased: 1.805
batch start
#iterations: 239
currently lose_sum: 98.73962181806564
time_elpased: 1.849
start validation test
0.606701030928
0.63529106703
0.504270865493
0.562248995984
0.606880862882
64.842
batch start
#iterations: 240
currently lose_sum: 98.78191423416138
time_elpased: 1.883
batch start
#iterations: 241
currently lose_sum: 98.71234649419785
time_elpased: 1.835
batch start
#iterations: 242
currently lose_sum: 98.69950354099274
time_elpased: 1.789
batch start
#iterations: 243
currently lose_sum: 98.77267706394196
time_elpased: 1.79
batch start
#iterations: 244
currently lose_sum: 98.78096318244934
time_elpased: 1.812
batch start
#iterations: 245
currently lose_sum: 98.82573771476746
time_elpased: 1.81
batch start
#iterations: 246
currently lose_sum: 98.71193718910217
time_elpased: 1.833
batch start
#iterations: 247
currently lose_sum: 98.85220497846603
time_elpased: 1.82
batch start
#iterations: 248
currently lose_sum: 98.74800229072571
time_elpased: 1.804
batch start
#iterations: 249
currently lose_sum: 98.69005006551743
time_elpased: 1.893
batch start
#iterations: 250
currently lose_sum: 98.86347532272339
time_elpased: 1.851
batch start
#iterations: 251
currently lose_sum: 98.84034711122513
time_elpased: 1.83
batch start
#iterations: 252
currently lose_sum: 98.92532503604889
time_elpased: 1.826
batch start
#iterations: 253
currently lose_sum: 98.57567942142487
time_elpased: 1.798
batch start
#iterations: 254
currently lose_sum: 98.45333850383759
time_elpased: 1.868
batch start
#iterations: 255
currently lose_sum: 98.51821780204773
time_elpased: 1.796
batch start
#iterations: 256
currently lose_sum: 98.69653224945068
time_elpased: 1.836
batch start
#iterations: 257
currently lose_sum: 98.96993166208267
time_elpased: 1.811
batch start
#iterations: 258
currently lose_sum: 98.67651677131653
time_elpased: 1.826
batch start
#iterations: 259
currently lose_sum: 98.68596911430359
time_elpased: 1.809
start validation test
0.604020618557
0.624161073826
0.526397036122
0.571125502456
0.604156898728
64.908
batch start
#iterations: 260
currently lose_sum: 98.68501496315002
time_elpased: 1.885
batch start
#iterations: 261
currently lose_sum: 98.86823332309723
time_elpased: 1.824
batch start
#iterations: 262
currently lose_sum: 98.62086343765259
time_elpased: 1.793
batch start
#iterations: 263
currently lose_sum: 98.63874304294586
time_elpased: 1.833
batch start
#iterations: 264
currently lose_sum: 98.90761893987656
time_elpased: 1.792
batch start
#iterations: 265
currently lose_sum: 98.66118949651718
time_elpased: 1.818
batch start
#iterations: 266
currently lose_sum: 98.72761607170105
time_elpased: 1.834
batch start
#iterations: 267
currently lose_sum: 98.6302000284195
time_elpased: 1.828
batch start
#iterations: 268
currently lose_sum: 98.71618050336838
time_elpased: 1.823
batch start
#iterations: 269
currently lose_sum: 98.90094691514969
time_elpased: 1.815
batch start
#iterations: 270
currently lose_sum: 98.58665597438812
time_elpased: 1.821
batch start
#iterations: 271
currently lose_sum: 98.68367421627045
time_elpased: 1.798
batch start
#iterations: 272
currently lose_sum: 98.93044263124466
time_elpased: 1.828
batch start
#iterations: 273
currently lose_sum: 98.82537823915482
time_elpased: 1.797
batch start
#iterations: 274
currently lose_sum: 98.7679802775383
time_elpased: 1.812
batch start
#iterations: 275
currently lose_sum: 98.90511918067932
time_elpased: 1.832
batch start
#iterations: 276
currently lose_sum: 98.87338423728943
time_elpased: 1.799
batch start
#iterations: 277
currently lose_sum: 98.68095970153809
time_elpased: 1.813
batch start
#iterations: 278
currently lose_sum: 98.8106940984726
time_elpased: 1.872
batch start
#iterations: 279
currently lose_sum: 98.60388201475143
time_elpased: 1.833
start validation test
0.605773195876
0.63556545669
0.499125244417
0.559142264238
0.605960432804
64.852
batch start
#iterations: 280
currently lose_sum: 98.98705798387527
time_elpased: 1.814
batch start
#iterations: 281
currently lose_sum: 98.7924861907959
time_elpased: 1.803
batch start
#iterations: 282
currently lose_sum: 98.72528153657913
time_elpased: 1.839
batch start
#iterations: 283
currently lose_sum: 98.57244890928268
time_elpased: 1.881
batch start
#iterations: 284
currently lose_sum: 98.7903904914856
time_elpased: 1.87
batch start
#iterations: 285
currently lose_sum: 98.50494003295898
time_elpased: 1.845
batch start
#iterations: 286
currently lose_sum: 98.68796008825302
time_elpased: 1.802
batch start
#iterations: 287
currently lose_sum: 98.59644049406052
time_elpased: 1.79
batch start
#iterations: 288
currently lose_sum: 98.73277550935745
time_elpased: 1.803
batch start
#iterations: 289
currently lose_sum: 98.65439999103546
time_elpased: 1.79
batch start
#iterations: 290
currently lose_sum: 98.67256319522858
time_elpased: 1.805
batch start
#iterations: 291
currently lose_sum: 98.46318227052689
time_elpased: 1.823
batch start
#iterations: 292
currently lose_sum: 98.79399502277374
time_elpased: 1.879
batch start
#iterations: 293
currently lose_sum: 98.68258112668991
time_elpased: 1.808
batch start
#iterations: 294
currently lose_sum: 98.77485752105713
time_elpased: 1.804
batch start
#iterations: 295
currently lose_sum: 98.6050174832344
time_elpased: 1.835
batch start
#iterations: 296
currently lose_sum: 98.47781383991241
time_elpased: 1.824
batch start
#iterations: 297
currently lose_sum: 99.03728145360947
time_elpased: 1.835
batch start
#iterations: 298
currently lose_sum: 98.5737339258194
time_elpased: 1.808
batch start
#iterations: 299
currently lose_sum: 98.40556275844574
time_elpased: 1.782
start validation test
0.607525773196
0.643549488055
0.485129155089
0.553221452881
0.607740659337
64.845
batch start
#iterations: 300
currently lose_sum: 98.54083234071732
time_elpased: 1.847
batch start
#iterations: 301
currently lose_sum: 98.58851605653763
time_elpased: 1.797
batch start
#iterations: 302
currently lose_sum: 98.62807887792587
time_elpased: 1.807
batch start
#iterations: 303
currently lose_sum: 98.68037867546082
time_elpased: 1.909
batch start
#iterations: 304
currently lose_sum: 98.56757575273514
time_elpased: 1.863
batch start
#iterations: 305
currently lose_sum: 98.67317003011703
time_elpased: 1.826
batch start
#iterations: 306
currently lose_sum: 98.63669884204865
time_elpased: 1.815
batch start
#iterations: 307
currently lose_sum: 98.67354327440262
time_elpased: 1.83
batch start
#iterations: 308
currently lose_sum: 98.66803485155106
time_elpased: 1.802
batch start
#iterations: 309
currently lose_sum: 98.80778396129608
time_elpased: 1.852
batch start
#iterations: 310
currently lose_sum: 98.70228803157806
time_elpased: 1.837
batch start
#iterations: 311
currently lose_sum: 98.6922253370285
time_elpased: 1.842
batch start
#iterations: 312
currently lose_sum: 98.72592234611511
time_elpased: 1.867
batch start
#iterations: 313
currently lose_sum: 98.70212012529373
time_elpased: 1.838
batch start
#iterations: 314
currently lose_sum: 98.41324126720428
time_elpased: 1.814
batch start
#iterations: 315
currently lose_sum: 98.3381980061531
time_elpased: 1.836
batch start
#iterations: 316
currently lose_sum: 98.74854969978333
time_elpased: 1.824
batch start
#iterations: 317
currently lose_sum: 98.62776273488998
time_elpased: 1.796
batch start
#iterations: 318
currently lose_sum: 98.49547404050827
time_elpased: 1.802
batch start
#iterations: 319
currently lose_sum: 98.84282797574997
time_elpased: 1.795
start validation test
0.604329896907
0.642787183434
0.472779664506
0.544829222011
0.60456085363
64.945
batch start
#iterations: 320
currently lose_sum: 98.57880824804306
time_elpased: 1.907
batch start
#iterations: 321
currently lose_sum: 98.79689902067184
time_elpased: 1.807
batch start
#iterations: 322
currently lose_sum: 98.72298926115036
time_elpased: 1.894
batch start
#iterations: 323
currently lose_sum: 98.48849606513977
time_elpased: 1.842
batch start
#iterations: 324
currently lose_sum: 98.66281539201736
time_elpased: 1.8
batch start
#iterations: 325
currently lose_sum: 98.61599081754684
time_elpased: 1.85
batch start
#iterations: 326
currently lose_sum: 98.80622309446335
time_elpased: 1.821
batch start
#iterations: 327
currently lose_sum: 98.56956297159195
time_elpased: 1.823
batch start
#iterations: 328
currently lose_sum: 98.54964691400528
time_elpased: 1.83
batch start
#iterations: 329
currently lose_sum: 98.64404910802841
time_elpased: 1.851
batch start
#iterations: 330
currently lose_sum: 98.69427442550659
time_elpased: 1.817
batch start
#iterations: 331
currently lose_sum: 98.58224350214005
time_elpased: 1.863
batch start
#iterations: 332
currently lose_sum: 98.48899114131927
time_elpased: 1.826
batch start
#iterations: 333
currently lose_sum: 98.60652834177017
time_elpased: 1.794
batch start
#iterations: 334
currently lose_sum: 98.93379729986191
time_elpased: 1.825
batch start
#iterations: 335
currently lose_sum: 98.44715571403503
time_elpased: 1.88
batch start
#iterations: 336
currently lose_sum: 98.74984282255173
time_elpased: 1.804
batch start
#iterations: 337
currently lose_sum: 98.64020240306854
time_elpased: 1.82
batch start
#iterations: 338
currently lose_sum: 98.67543292045593
time_elpased: 1.871
batch start
#iterations: 339
currently lose_sum: 98.71795099973679
time_elpased: 1.825
start validation test
0.601288659794
0.639068200954
0.468663167644
0.54075877219
0.601521504301
64.881
batch start
#iterations: 340
currently lose_sum: 98.72740632295609
time_elpased: 1.81
batch start
#iterations: 341
currently lose_sum: 98.8544494509697
time_elpased: 1.814
batch start
#iterations: 342
currently lose_sum: 98.54007762670517
time_elpased: 1.789
batch start
#iterations: 343
currently lose_sum: 98.83448648452759
time_elpased: 1.866
batch start
#iterations: 344
currently lose_sum: 98.4538756608963
time_elpased: 1.864
batch start
#iterations: 345
currently lose_sum: 98.70599430799484
time_elpased: 1.851
batch start
#iterations: 346
currently lose_sum: 98.79428917169571
time_elpased: 1.822
batch start
#iterations: 347
currently lose_sum: 98.46836018562317
time_elpased: 1.816
batch start
#iterations: 348
currently lose_sum: 98.77198725938797
time_elpased: 1.906
batch start
#iterations: 349
currently lose_sum: 98.50507432222366
time_elpased: 1.829
batch start
#iterations: 350
currently lose_sum: 98.79409289360046
time_elpased: 1.815
batch start
#iterations: 351
currently lose_sum: 98.53410077095032
time_elpased: 1.874
batch start
#iterations: 352
currently lose_sum: 98.60985946655273
time_elpased: 1.856
batch start
#iterations: 353
currently lose_sum: 98.66140115261078
time_elpased: 1.832
batch start
#iterations: 354
currently lose_sum: 98.4761472940445
time_elpased: 1.83
batch start
#iterations: 355
currently lose_sum: 98.59004110097885
time_elpased: 1.815
batch start
#iterations: 356
currently lose_sum: 98.45175385475159
time_elpased: 1.854
batch start
#iterations: 357
currently lose_sum: 98.50982069969177
time_elpased: 1.886
batch start
#iterations: 358
currently lose_sum: 98.56956827640533
time_elpased: 1.885
batch start
#iterations: 359
currently lose_sum: 98.43588668107986
time_elpased: 1.825
start validation test
0.609175257732
0.635486736896
0.515282494597
0.56910661514
0.60934010096
64.744
batch start
#iterations: 360
currently lose_sum: 98.6659991145134
time_elpased: 1.95
batch start
#iterations: 361
currently lose_sum: 98.79254949092865
time_elpased: 1.816
batch start
#iterations: 362
currently lose_sum: 98.75215500593185
time_elpased: 1.814
batch start
#iterations: 363
currently lose_sum: 98.50132018327713
time_elpased: 1.787
batch start
#iterations: 364
currently lose_sum: 98.42812579870224
time_elpased: 1.81
batch start
#iterations: 365
currently lose_sum: 98.66448992490768
time_elpased: 1.833
batch start
#iterations: 366
currently lose_sum: 98.35152769088745
time_elpased: 1.807
batch start
#iterations: 367
currently lose_sum: 98.6531126499176
time_elpased: 1.795
batch start
#iterations: 368
currently lose_sum: 98.4611223936081
time_elpased: 1.785
batch start
#iterations: 369
currently lose_sum: 98.6822372674942
time_elpased: 1.826
batch start
#iterations: 370
currently lose_sum: 98.568907558918
time_elpased: 1.821
batch start
#iterations: 371
currently lose_sum: 98.44398248195648
time_elpased: 1.825
batch start
#iterations: 372
currently lose_sum: 98.65952461957932
time_elpased: 1.812
batch start
#iterations: 373
currently lose_sum: 98.5029930472374
time_elpased: 1.838
batch start
#iterations: 374
currently lose_sum: 98.55749529600143
time_elpased: 1.964
batch start
#iterations: 375
currently lose_sum: 98.69845771789551
time_elpased: 1.807
batch start
#iterations: 376
currently lose_sum: 98.66757071018219
time_elpased: 1.853
batch start
#iterations: 377
currently lose_sum: 98.55171144008636
time_elpased: 1.823
batch start
#iterations: 378
currently lose_sum: 98.62756556272507
time_elpased: 1.859
batch start
#iterations: 379
currently lose_sum: 98.7735909819603
time_elpased: 1.82
start validation test
0.602010309278
0.652786282915
0.438818565401
0.524832297372
0.602296817555
65.115
batch start
#iterations: 380
currently lose_sum: 98.77997434139252
time_elpased: 1.829
batch start
#iterations: 381
currently lose_sum: 98.78744679689407
time_elpased: 1.791
batch start
#iterations: 382
currently lose_sum: 98.44693684577942
time_elpased: 1.822
batch start
#iterations: 383
currently lose_sum: 98.67401576042175
time_elpased: 1.881
batch start
#iterations: 384
currently lose_sum: 98.58003556728363
time_elpased: 1.875
batch start
#iterations: 385
currently lose_sum: 98.78585809469223
time_elpased: 1.872
batch start
#iterations: 386
currently lose_sum: 98.76059395074844
time_elpased: 1.862
batch start
#iterations: 387
currently lose_sum: 98.51778614521027
time_elpased: 1.863
batch start
#iterations: 388
currently lose_sum: 98.56997889280319
time_elpased: 1.814
batch start
#iterations: 389
currently lose_sum: 98.66173946857452
time_elpased: 1.84
batch start
#iterations: 390
currently lose_sum: 98.76279151439667
time_elpased: 1.832
batch start
#iterations: 391
currently lose_sum: 98.55795282125473
time_elpased: 1.798
batch start
#iterations: 392
currently lose_sum: 98.54172748327255
time_elpased: 1.882
batch start
#iterations: 393
currently lose_sum: 98.47772288322449
time_elpased: 1.872
batch start
#iterations: 394
currently lose_sum: 98.44159507751465
time_elpased: 1.844
batch start
#iterations: 395
currently lose_sum: 98.39315348863602
time_elpased: 1.878
batch start
#iterations: 396
currently lose_sum: 98.64541602134705
time_elpased: 1.843
batch start
#iterations: 397
currently lose_sum: 98.44130104780197
time_elpased: 1.846
batch start
#iterations: 398
currently lose_sum: 98.58075219392776
time_elpased: 1.84
batch start
#iterations: 399
currently lose_sum: 98.5407183766365
time_elpased: 1.84
start validation test
0.607731958763
0.634942999872
0.510136873521
0.565738415887
0.607903301988
64.769
acc: 0.603
pre: 0.629
rec: 0.507
F1: 0.561
auc: 0.604
