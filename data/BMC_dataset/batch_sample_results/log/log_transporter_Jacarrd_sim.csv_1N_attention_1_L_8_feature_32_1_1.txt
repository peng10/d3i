start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.66253185272217
time_elpased: 1.904
batch start
#iterations: 1
currently lose_sum: 100.25730752944946
time_elpased: 1.822
batch start
#iterations: 2
currently lose_sum: 100.26224881410599
time_elpased: 1.809
batch start
#iterations: 3
currently lose_sum: 100.26699411869049
time_elpased: 1.832
batch start
#iterations: 4
currently lose_sum: 100.10764509439468
time_elpased: 1.801
batch start
#iterations: 5
currently lose_sum: 100.12715911865234
time_elpased: 1.843
batch start
#iterations: 6
currently lose_sum: 100.14511704444885
time_elpased: 1.831
batch start
#iterations: 7
currently lose_sum: 99.88282752037048
time_elpased: 1.789
batch start
#iterations: 8
currently lose_sum: 100.016366481781
time_elpased: 1.784
batch start
#iterations: 9
currently lose_sum: 99.88551312685013
time_elpased: 1.798
batch start
#iterations: 10
currently lose_sum: 99.80103349685669
time_elpased: 1.804
batch start
#iterations: 11
currently lose_sum: 99.811838388443
time_elpased: 1.815
batch start
#iterations: 12
currently lose_sum: 99.84117114543915
time_elpased: 1.791
batch start
#iterations: 13
currently lose_sum: 99.8910756111145
time_elpased: 1.846
batch start
#iterations: 14
currently lose_sum: 99.82020729780197
time_elpased: 1.84
batch start
#iterations: 15
currently lose_sum: 99.7407774925232
time_elpased: 1.809
batch start
#iterations: 16
currently lose_sum: 99.5657969713211
time_elpased: 1.789
batch start
#iterations: 17
currently lose_sum: 99.65215021371841
time_elpased: 1.794
batch start
#iterations: 18
currently lose_sum: 99.7570350766182
time_elpased: 1.823
batch start
#iterations: 19
currently lose_sum: 99.58198654651642
time_elpased: 1.815
start validation test
0.591237113402
0.633817582747
0.435525367912
0.516286446261
0.591510489388
65.669
batch start
#iterations: 20
currently lose_sum: 99.61353594064713
time_elpased: 1.805
batch start
#iterations: 21
currently lose_sum: 99.49352371692657
time_elpased: 1.83
batch start
#iterations: 22
currently lose_sum: 99.49815607070923
time_elpased: 1.848
batch start
#iterations: 23
currently lose_sum: 99.41022789478302
time_elpased: 1.829
batch start
#iterations: 24
currently lose_sum: 99.60993093252182
time_elpased: 1.777
batch start
#iterations: 25
currently lose_sum: 99.57571810483932
time_elpased: 1.801
batch start
#iterations: 26
currently lose_sum: 99.51813018321991
time_elpased: 1.769
batch start
#iterations: 27
currently lose_sum: 99.31777381896973
time_elpased: 1.803
batch start
#iterations: 28
currently lose_sum: 99.33933055400848
time_elpased: 1.782
batch start
#iterations: 29
currently lose_sum: 99.31943261623383
time_elpased: 1.841
batch start
#iterations: 30
currently lose_sum: 99.43034887313843
time_elpased: 1.818
batch start
#iterations: 31
currently lose_sum: 99.30148929357529
time_elpased: 1.829
batch start
#iterations: 32
currently lose_sum: 99.37108302116394
time_elpased: 1.816
batch start
#iterations: 33
currently lose_sum: 99.30818504095078
time_elpased: 1.814
batch start
#iterations: 34
currently lose_sum: 99.35173290967941
time_elpased: 1.812
batch start
#iterations: 35
currently lose_sum: 99.35366779565811
time_elpased: 1.816
batch start
#iterations: 36
currently lose_sum: 99.10777109861374
time_elpased: 1.891
batch start
#iterations: 37
currently lose_sum: 99.52500778436661
time_elpased: 1.78
batch start
#iterations: 38
currently lose_sum: 99.11512207984924
time_elpased: 1.79
batch start
#iterations: 39
currently lose_sum: 99.22937250137329
time_elpased: 1.829
start validation test
0.602525773196
0.620033508856
0.533189255943
0.57334144857
0.602647504146
65.144
batch start
#iterations: 40
currently lose_sum: 99.30946677923203
time_elpased: 1.888
batch start
#iterations: 41
currently lose_sum: 99.29184931516647
time_elpased: 1.797
batch start
#iterations: 42
currently lose_sum: 99.20037931203842
time_elpased: 1.828
batch start
#iterations: 43
currently lose_sum: 99.0909441113472
time_elpased: 1.825
batch start
#iterations: 44
currently lose_sum: 99.377481341362
time_elpased: 1.794
batch start
#iterations: 45
currently lose_sum: 99.15422075986862
time_elpased: 1.81
batch start
#iterations: 46
currently lose_sum: 98.9545624256134
time_elpased: 1.801
batch start
#iterations: 47
currently lose_sum: 99.12757819890976
time_elpased: 1.763
batch start
#iterations: 48
currently lose_sum: 99.25606948137283
time_elpased: 1.815
batch start
#iterations: 49
currently lose_sum: 99.38073700666428
time_elpased: 1.784
batch start
#iterations: 50
currently lose_sum: 99.15867644548416
time_elpased: 1.825
batch start
#iterations: 51
currently lose_sum: 99.19869530200958
time_elpased: 1.817
batch start
#iterations: 52
currently lose_sum: 99.12483620643616
time_elpased: 1.838
batch start
#iterations: 53
currently lose_sum: 99.09575861692429
time_elpased: 1.794
batch start
#iterations: 54
currently lose_sum: 99.17095965147018
time_elpased: 1.805
batch start
#iterations: 55
currently lose_sum: 99.39776736497879
time_elpased: 1.845
batch start
#iterations: 56
currently lose_sum: 99.0983675122261
time_elpased: 1.79
batch start
#iterations: 57
currently lose_sum: 99.12696987390518
time_elpased: 1.798
batch start
#iterations: 58
currently lose_sum: 99.0615103840828
time_elpased: 1.81
batch start
#iterations: 59
currently lose_sum: 99.0982004404068
time_elpased: 1.798
start validation test
0.603608247423
0.622418166445
0.53030770814
0.572682818404
0.603736937825
65.018
batch start
#iterations: 60
currently lose_sum: 99.05146127939224
time_elpased: 1.872
batch start
#iterations: 61
currently lose_sum: 99.23227822780609
time_elpased: 1.794
batch start
#iterations: 62
currently lose_sum: 98.97990053892136
time_elpased: 1.795
batch start
#iterations: 63
currently lose_sum: 99.28863370418549
time_elpased: 1.801
batch start
#iterations: 64
currently lose_sum: 99.20671778917313
time_elpased: 1.818
batch start
#iterations: 65
currently lose_sum: 98.814832508564
time_elpased: 1.772
batch start
#iterations: 66
currently lose_sum: 99.032928109169
time_elpased: 1.825
batch start
#iterations: 67
currently lose_sum: 99.0204187631607
time_elpased: 1.822
batch start
#iterations: 68
currently lose_sum: 98.98152875900269
time_elpased: 1.906
batch start
#iterations: 69
currently lose_sum: 99.15232414007187
time_elpased: 1.755
batch start
#iterations: 70
currently lose_sum: 99.07403475046158
time_elpased: 1.788
batch start
#iterations: 71
currently lose_sum: 99.25981312990189
time_elpased: 1.784
batch start
#iterations: 72
currently lose_sum: 99.09899598360062
time_elpased: 1.839
batch start
#iterations: 73
currently lose_sum: 99.00416016578674
time_elpased: 1.792
batch start
#iterations: 74
currently lose_sum: 98.87367928028107
time_elpased: 1.786
batch start
#iterations: 75
currently lose_sum: 98.90581053495407
time_elpased: 1.816
batch start
#iterations: 76
currently lose_sum: 98.73439288139343
time_elpased: 1.797
batch start
#iterations: 77
currently lose_sum: 98.99201720952988
time_elpased: 1.779
batch start
#iterations: 78
currently lose_sum: 98.86929476261139
time_elpased: 1.819
batch start
#iterations: 79
currently lose_sum: 99.13331240415573
time_elpased: 1.821
start validation test
0.598041237113
0.625277451364
0.492847586704
0.551220073665
0.598225920792
65.133
batch start
#iterations: 80
currently lose_sum: 98.95668071508408
time_elpased: 1.901
batch start
#iterations: 81
currently lose_sum: 98.78517031669617
time_elpased: 1.843
batch start
#iterations: 82
currently lose_sum: 98.84731203317642
time_elpased: 1.819
batch start
#iterations: 83
currently lose_sum: 98.82220417261124
time_elpased: 1.839
batch start
#iterations: 84
currently lose_sum: 98.9212241768837
time_elpased: 1.827
batch start
#iterations: 85
currently lose_sum: 99.02387446165085
time_elpased: 1.762
batch start
#iterations: 86
currently lose_sum: 99.17224025726318
time_elpased: 1.8
batch start
#iterations: 87
currently lose_sum: 98.97865968942642
time_elpased: 1.801
batch start
#iterations: 88
currently lose_sum: 98.75528436899185
time_elpased: 1.852
batch start
#iterations: 89
currently lose_sum: 98.58274227380753
time_elpased: 1.824
batch start
#iterations: 90
currently lose_sum: 99.0993801355362
time_elpased: 1.757
batch start
#iterations: 91
currently lose_sum: 99.07151782512665
time_elpased: 1.819
batch start
#iterations: 92
currently lose_sum: 98.72366148233414
time_elpased: 1.84
batch start
#iterations: 93
currently lose_sum: 98.86791652441025
time_elpased: 1.807
batch start
#iterations: 94
currently lose_sum: 98.79826706647873
time_elpased: 1.855
batch start
#iterations: 95
currently lose_sum: 98.8858739733696
time_elpased: 1.826
batch start
#iterations: 96
currently lose_sum: 98.74222308397293
time_elpased: 1.795
batch start
#iterations: 97
currently lose_sum: 98.97682958841324
time_elpased: 1.849
batch start
#iterations: 98
currently lose_sum: 98.89471596479416
time_elpased: 1.84
batch start
#iterations: 99
currently lose_sum: 98.80293548107147
time_elpased: 1.79
start validation test
0.605773195876
0.629978640533
0.516002881548
0.567322923738
0.605930801509
64.854
batch start
#iterations: 100
currently lose_sum: 98.76083558797836
time_elpased: 1.859
batch start
#iterations: 101
currently lose_sum: 98.73180192708969
time_elpased: 1.831
batch start
#iterations: 102
currently lose_sum: 98.67015314102173
time_elpased: 1.809
batch start
#iterations: 103
currently lose_sum: 99.08475786447525
time_elpased: 1.802
batch start
#iterations: 104
currently lose_sum: 99.06627994775772
time_elpased: 1.822
batch start
#iterations: 105
currently lose_sum: 98.79260283708572
time_elpased: 1.813
batch start
#iterations: 106
currently lose_sum: 98.7647956609726
time_elpased: 1.8
batch start
#iterations: 107
currently lose_sum: 99.07236516475677
time_elpased: 1.782
batch start
#iterations: 108
currently lose_sum: 99.03082782030106
time_elpased: 1.811
batch start
#iterations: 109
currently lose_sum: 98.80884677171707
time_elpased: 1.804
batch start
#iterations: 110
currently lose_sum: 98.88877195119858
time_elpased: 1.842
batch start
#iterations: 111
currently lose_sum: 98.8175418972969
time_elpased: 1.874
batch start
#iterations: 112
currently lose_sum: 98.83412444591522
time_elpased: 1.785
batch start
#iterations: 113
currently lose_sum: 98.87945973873138
time_elpased: 1.807
batch start
#iterations: 114
currently lose_sum: 98.7978281378746
time_elpased: 1.851
batch start
#iterations: 115
currently lose_sum: 98.7307865023613
time_elpased: 1.815
batch start
#iterations: 116
currently lose_sum: 98.95593297481537
time_elpased: 1.806
batch start
#iterations: 117
currently lose_sum: 98.81607246398926
time_elpased: 1.826
batch start
#iterations: 118
currently lose_sum: 99.01140636205673
time_elpased: 1.821
batch start
#iterations: 119
currently lose_sum: 98.95504599809647
time_elpased: 1.774
start validation test
0.604845360825
0.626620570441
0.522280539261
0.569712617872
0.604990316104
64.903
batch start
#iterations: 120
currently lose_sum: 98.91147357225418
time_elpased: 1.867
batch start
#iterations: 121
currently lose_sum: 98.81257474422455
time_elpased: 1.821
batch start
#iterations: 122
currently lose_sum: 98.87478643655777
time_elpased: 1.848
batch start
#iterations: 123
currently lose_sum: 98.75548082590103
time_elpased: 1.827
batch start
#iterations: 124
currently lose_sum: 98.61871433258057
time_elpased: 1.794
batch start
#iterations: 125
currently lose_sum: 98.99382972717285
time_elpased: 1.781
batch start
#iterations: 126
currently lose_sum: 98.86614614725113
time_elpased: 1.789
batch start
#iterations: 127
currently lose_sum: 98.92843222618103
time_elpased: 1.794
batch start
#iterations: 128
currently lose_sum: 98.9799354672432
time_elpased: 1.8
batch start
#iterations: 129
currently lose_sum: 98.71730178594589
time_elpased: 1.84
batch start
#iterations: 130
currently lose_sum: 98.88662832975388
time_elpased: 1.784
batch start
#iterations: 131
currently lose_sum: 98.69945287704468
time_elpased: 1.781
batch start
#iterations: 132
currently lose_sum: 98.73729807138443
time_elpased: 1.817
batch start
#iterations: 133
currently lose_sum: 98.81428825855255
time_elpased: 1.841
batch start
#iterations: 134
currently lose_sum: 98.73926794528961
time_elpased: 1.793
batch start
#iterations: 135
currently lose_sum: 98.91603219509125
time_elpased: 1.791
batch start
#iterations: 136
currently lose_sum: 98.63603037595749
time_elpased: 1.846
batch start
#iterations: 137
currently lose_sum: 98.79489022493362
time_elpased: 1.836
batch start
#iterations: 138
currently lose_sum: 98.7082307934761
time_elpased: 1.8
batch start
#iterations: 139
currently lose_sum: 98.95165228843689
time_elpased: 1.857
start validation test
0.605824742268
0.622688477952
0.540598950293
0.578747314493
0.605939256206
64.741
batch start
#iterations: 140
currently lose_sum: 98.85084295272827
time_elpased: 1.862
batch start
#iterations: 141
currently lose_sum: 98.8770580291748
time_elpased: 1.824
batch start
#iterations: 142
currently lose_sum: 98.92045152187347
time_elpased: 1.857
batch start
#iterations: 143
currently lose_sum: 98.91451531648636
time_elpased: 1.787
batch start
#iterations: 144
currently lose_sum: 98.68793839216232
time_elpased: 1.812
batch start
#iterations: 145
currently lose_sum: 98.80893552303314
time_elpased: 1.791
batch start
#iterations: 146
currently lose_sum: 98.67477214336395
time_elpased: 1.802
batch start
#iterations: 147
currently lose_sum: 98.85699182748795
time_elpased: 1.795
batch start
#iterations: 148
currently lose_sum: 99.02987945079803
time_elpased: 1.77
batch start
#iterations: 149
currently lose_sum: 98.76497334241867
time_elpased: 1.856
batch start
#iterations: 150
currently lose_sum: 98.67278373241425
time_elpased: 1.875
batch start
#iterations: 151
currently lose_sum: 98.78950864076614
time_elpased: 1.797
batch start
#iterations: 152
currently lose_sum: 98.96809089183807
time_elpased: 1.802
batch start
#iterations: 153
currently lose_sum: 98.85946196317673
time_elpased: 1.756
batch start
#iterations: 154
currently lose_sum: 98.91740721464157
time_elpased: 1.791
batch start
#iterations: 155
currently lose_sum: 98.7070569396019
time_elpased: 1.825
batch start
#iterations: 156
currently lose_sum: 98.72418600320816
time_elpased: 1.778
batch start
#iterations: 157
currently lose_sum: 98.89378297328949
time_elpased: 1.805
batch start
#iterations: 158
currently lose_sum: 98.96086984872818
time_elpased: 1.838
batch start
#iterations: 159
currently lose_sum: 99.12119942903519
time_elpased: 1.779
start validation test
0.602783505155
0.633905979491
0.489863126479
0.552652966446
0.602981754296
65.018
batch start
#iterations: 160
currently lose_sum: 98.83462619781494
time_elpased: 1.867
batch start
#iterations: 161
currently lose_sum: 98.75425124168396
time_elpased: 1.821
batch start
#iterations: 162
currently lose_sum: 98.63264447450638
time_elpased: 1.761
batch start
#iterations: 163
currently lose_sum: 98.87385141849518
time_elpased: 1.776
batch start
#iterations: 164
currently lose_sum: 98.70754182338715
time_elpased: 1.819
batch start
#iterations: 165
currently lose_sum: 98.76755875349045
time_elpased: 1.885
batch start
#iterations: 166
currently lose_sum: 98.65711516141891
time_elpased: 1.765
batch start
#iterations: 167
currently lose_sum: 99.05547672510147
time_elpased: 1.789
batch start
#iterations: 168
currently lose_sum: 98.61000484228134
time_elpased: 1.801
batch start
#iterations: 169
currently lose_sum: 98.7360053062439
time_elpased: 1.785
batch start
#iterations: 170
currently lose_sum: 98.69795036315918
time_elpased: 1.787
batch start
#iterations: 171
currently lose_sum: 98.66750341653824
time_elpased: 1.864
batch start
#iterations: 172
currently lose_sum: 98.87104916572571
time_elpased: 1.847
batch start
#iterations: 173
currently lose_sum: 98.72535115480423
time_elpased: 1.942
batch start
#iterations: 174
currently lose_sum: 98.82869398593903
time_elpased: 1.808
batch start
#iterations: 175
currently lose_sum: 98.55644690990448
time_elpased: 1.775
batch start
#iterations: 176
currently lose_sum: 98.60992014408112
time_elpased: 1.803
batch start
#iterations: 177
currently lose_sum: 98.65175300836563
time_elpased: 1.818
batch start
#iterations: 178
currently lose_sum: 98.64256352186203
time_elpased: 1.769
batch start
#iterations: 179
currently lose_sum: 98.88379007577896
time_elpased: 1.814
start validation test
0.604432989691
0.617238608975
0.553463002984
0.583613673359
0.604522475364
64.815
batch start
#iterations: 180
currently lose_sum: 98.57393592596054
time_elpased: 1.829
batch start
#iterations: 181
currently lose_sum: 98.99790042638779
time_elpased: 1.821
batch start
#iterations: 182
currently lose_sum: 98.83575111627579
time_elpased: 1.816
batch start
#iterations: 183
currently lose_sum: 98.75302493572235
time_elpased: 1.753
batch start
#iterations: 184
currently lose_sum: 98.6654976606369
time_elpased: 1.803
batch start
#iterations: 185
currently lose_sum: 98.77379262447357
time_elpased: 1.879
batch start
#iterations: 186
currently lose_sum: 98.76732432842255
time_elpased: 1.836
batch start
#iterations: 187
currently lose_sum: 98.47029107809067
time_elpased: 1.835
batch start
#iterations: 188
currently lose_sum: 98.89960300922394
time_elpased: 1.788
batch start
#iterations: 189
currently lose_sum: 98.68012881278992
time_elpased: 1.786
batch start
#iterations: 190
currently lose_sum: 98.8754061460495
time_elpased: 1.791
batch start
#iterations: 191
currently lose_sum: 98.75344634056091
time_elpased: 1.788
batch start
#iterations: 192
currently lose_sum: 98.59699213504791
time_elpased: 1.801
batch start
#iterations: 193
currently lose_sum: 98.77009093761444
time_elpased: 1.808
batch start
#iterations: 194
currently lose_sum: 98.7454246878624
time_elpased: 1.787
batch start
#iterations: 195
currently lose_sum: 98.81581920385361
time_elpased: 1.828
batch start
#iterations: 196
currently lose_sum: 99.00222831964493
time_elpased: 1.794
batch start
#iterations: 197
currently lose_sum: 98.60609114170074
time_elpased: 1.825
batch start
#iterations: 198
currently lose_sum: 98.73737519979477
time_elpased: 1.805
batch start
#iterations: 199
currently lose_sum: 98.62128186225891
time_elpased: 1.803
start validation test
0.603969072165
0.634701986755
0.493156323968
0.555047199861
0.604163621036
64.860
batch start
#iterations: 200
currently lose_sum: 98.64764076471329
time_elpased: 1.919
batch start
#iterations: 201
currently lose_sum: 98.5856003165245
time_elpased: 1.869
batch start
#iterations: 202
currently lose_sum: 98.60267639160156
time_elpased: 1.893
batch start
#iterations: 203
currently lose_sum: 98.69689732789993
time_elpased: 1.814
batch start
#iterations: 204
currently lose_sum: 98.65512865781784
time_elpased: 1.813
batch start
#iterations: 205
currently lose_sum: 98.62368136644363
time_elpased: 1.778
batch start
#iterations: 206
currently lose_sum: 98.61304259300232
time_elpased: 1.838
batch start
#iterations: 207
currently lose_sum: 98.67254436016083
time_elpased: 1.778
batch start
#iterations: 208
currently lose_sum: 98.53475219011307
time_elpased: 1.766
batch start
#iterations: 209
currently lose_sum: 98.44278383255005
time_elpased: 1.81
batch start
#iterations: 210
currently lose_sum: 98.48347246646881
time_elpased: 1.765
batch start
#iterations: 211
currently lose_sum: 98.63772815465927
time_elpased: 1.794
batch start
#iterations: 212
currently lose_sum: 98.52127665281296
time_elpased: 1.807
batch start
#iterations: 213
currently lose_sum: 98.84824788570404
time_elpased: 1.8
batch start
#iterations: 214
currently lose_sum: 98.82935470342636
time_elpased: 1.826
batch start
#iterations: 215
currently lose_sum: 98.75394177436829
time_elpased: 1.83
batch start
#iterations: 216
currently lose_sum: 98.40469652414322
time_elpased: 1.814
batch start
#iterations: 217
currently lose_sum: 98.78644853830338
time_elpased: 1.871
batch start
#iterations: 218
currently lose_sum: 98.60951691865921
time_elpased: 1.771
batch start
#iterations: 219
currently lose_sum: 98.91528517007828
time_elpased: 1.862
start validation test
0.598556701031
0.638836908018
0.456725326747
0.532645223236
0.598805707884
64.901
batch start
#iterations: 220
currently lose_sum: 98.6486263871193
time_elpased: 1.821
batch start
#iterations: 221
currently lose_sum: 98.45652616024017
time_elpased: 1.792
batch start
#iterations: 222
currently lose_sum: 98.57149124145508
time_elpased: 1.822
batch start
#iterations: 223
currently lose_sum: 98.40063685178757
time_elpased: 1.856
batch start
#iterations: 224
currently lose_sum: 98.55237954854965
time_elpased: 1.786
batch start
#iterations: 225
currently lose_sum: 98.70217317342758
time_elpased: 1.824
batch start
#iterations: 226
currently lose_sum: 98.63820391893387
time_elpased: 1.84
batch start
#iterations: 227
currently lose_sum: 98.81492060422897
time_elpased: 1.79
batch start
#iterations: 228
currently lose_sum: 98.5477728843689
time_elpased: 1.822
batch start
#iterations: 229
currently lose_sum: 98.71287161111832
time_elpased: 1.809
batch start
#iterations: 230
currently lose_sum: 98.85722053050995
time_elpased: 1.885
batch start
#iterations: 231
currently lose_sum: 98.62591242790222
time_elpased: 1.803
batch start
#iterations: 232
currently lose_sum: 98.614836871624
time_elpased: 1.754
batch start
#iterations: 233
currently lose_sum: 98.7735515832901
time_elpased: 1.776
batch start
#iterations: 234
currently lose_sum: 98.51798957586288
time_elpased: 1.793
batch start
#iterations: 235
currently lose_sum: 98.73612523078918
time_elpased: 1.835
batch start
#iterations: 236
currently lose_sum: 98.81002140045166
time_elpased: 1.847
batch start
#iterations: 237
currently lose_sum: 98.66902899742126
time_elpased: 1.803
batch start
#iterations: 238
currently lose_sum: 98.59046924114227
time_elpased: 1.822
batch start
#iterations: 239
currently lose_sum: 98.88753664493561
time_elpased: 1.773
start validation test
0.601649484536
0.633579583613
0.485437892354
0.549702831838
0.601853511911
64.976
batch start
#iterations: 240
currently lose_sum: 98.43956685066223
time_elpased: 1.814
batch start
#iterations: 241
currently lose_sum: 98.7326448559761
time_elpased: 1.842
batch start
#iterations: 242
currently lose_sum: 98.71793109178543
time_elpased: 1.788
batch start
#iterations: 243
currently lose_sum: 98.70914334058762
time_elpased: 1.817
batch start
#iterations: 244
currently lose_sum: 98.60559356212616
time_elpased: 1.822
batch start
#iterations: 245
currently lose_sum: 98.79834812879562
time_elpased: 1.916
batch start
#iterations: 246
currently lose_sum: 98.72174912691116
time_elpased: 1.781
batch start
#iterations: 247
currently lose_sum: 98.66639220714569
time_elpased: 1.783
batch start
#iterations: 248
currently lose_sum: 98.488077044487
time_elpased: 1.858
batch start
#iterations: 249
currently lose_sum: 98.60155874490738
time_elpased: 1.841
batch start
#iterations: 250
currently lose_sum: 98.64163827896118
time_elpased: 1.836
batch start
#iterations: 251
currently lose_sum: 98.52652269601822
time_elpased: 1.838
batch start
#iterations: 252
currently lose_sum: 98.57643282413483
time_elpased: 1.797
batch start
#iterations: 253
currently lose_sum: 98.68008244037628
time_elpased: 1.826
batch start
#iterations: 254
currently lose_sum: 98.79515981674194
time_elpased: 1.852
batch start
#iterations: 255
currently lose_sum: 98.52035218477249
time_elpased: 1.793
batch start
#iterations: 256
currently lose_sum: 98.47563004493713
time_elpased: 1.814
batch start
#iterations: 257
currently lose_sum: 98.37322801351547
time_elpased: 1.8
batch start
#iterations: 258
currently lose_sum: 98.48020792007446
time_elpased: 1.797
batch start
#iterations: 259
currently lose_sum: 98.60159689188004
time_elpased: 1.815
start validation test
0.606907216495
0.632795630636
0.512709684059
0.566458214895
0.607072594792
64.671
batch start
#iterations: 260
currently lose_sum: 98.61685979366302
time_elpased: 1.814
batch start
#iterations: 261
currently lose_sum: 98.50374925136566
time_elpased: 1.833
batch start
#iterations: 262
currently lose_sum: 98.31308680772781
time_elpased: 1.782
batch start
#iterations: 263
currently lose_sum: 98.51313823461533
time_elpased: 1.857
batch start
#iterations: 264
currently lose_sum: 98.64171177148819
time_elpased: 1.813
batch start
#iterations: 265
currently lose_sum: 98.64685201644897
time_elpased: 1.798
batch start
#iterations: 266
currently lose_sum: 98.77359050512314
time_elpased: 1.842
batch start
#iterations: 267
currently lose_sum: 98.64575290679932
time_elpased: 1.908
batch start
#iterations: 268
currently lose_sum: 98.61766707897186
time_elpased: 1.803
batch start
#iterations: 269
currently lose_sum: 98.73718702793121
time_elpased: 1.777
batch start
#iterations: 270
currently lose_sum: 98.36909401416779
time_elpased: 1.821
batch start
#iterations: 271
currently lose_sum: 98.56609147787094
time_elpased: 1.781
batch start
#iterations: 272
currently lose_sum: 98.90566885471344
time_elpased: 1.873
batch start
#iterations: 273
currently lose_sum: 98.62350821495056
time_elpased: 1.82
batch start
#iterations: 274
currently lose_sum: 98.50952988862991
time_elpased: 1.8
batch start
#iterations: 275
currently lose_sum: 98.65582567453384
time_elpased: 1.837
batch start
#iterations: 276
currently lose_sum: 98.75098407268524
time_elpased: 1.812
batch start
#iterations: 277
currently lose_sum: 98.53241270780563
time_elpased: 1.828
batch start
#iterations: 278
currently lose_sum: 98.6793863773346
time_elpased: 1.789
batch start
#iterations: 279
currently lose_sum: 98.556680560112
time_elpased: 1.836
start validation test
0.60824742268
0.627915407855
0.534732942266
0.577590040018
0.60837648869
64.623
batch start
#iterations: 280
currently lose_sum: 98.49818122386932
time_elpased: 1.816
batch start
#iterations: 281
currently lose_sum: 98.50106728076935
time_elpased: 1.791
batch start
#iterations: 282
currently lose_sum: 98.64429944753647
time_elpased: 1.838
batch start
#iterations: 283
currently lose_sum: 98.46454030275345
time_elpased: 1.845
batch start
#iterations: 284
currently lose_sum: 98.46649903059006
time_elpased: 1.906
batch start
#iterations: 285
currently lose_sum: 98.64614206552505
time_elpased: 1.829
batch start
#iterations: 286
currently lose_sum: 98.48497468233109
time_elpased: 1.87
batch start
#iterations: 287
currently lose_sum: 98.46975022554398
time_elpased: 1.822
batch start
#iterations: 288
currently lose_sum: 98.24812257289886
time_elpased: 1.828
batch start
#iterations: 289
currently lose_sum: 98.61755800247192
time_elpased: 1.833
batch start
#iterations: 290
currently lose_sum: 98.76214152574539
time_elpased: 1.823
batch start
#iterations: 291
currently lose_sum: 98.35327237844467
time_elpased: 1.83
batch start
#iterations: 292
currently lose_sum: 98.409592628479
time_elpased: 1.791
batch start
#iterations: 293
currently lose_sum: 98.72026878595352
time_elpased: 1.829
batch start
#iterations: 294
currently lose_sum: 98.46029967069626
time_elpased: 1.845
batch start
#iterations: 295
currently lose_sum: 98.64866179227829
time_elpased: 1.867
batch start
#iterations: 296
currently lose_sum: 98.57057362794876
time_elpased: 1.837
batch start
#iterations: 297
currently lose_sum: 98.4340842962265
time_elpased: 1.875
batch start
#iterations: 298
currently lose_sum: 98.69748413562775
time_elpased: 1.797
batch start
#iterations: 299
currently lose_sum: 98.70429974794388
time_elpased: 1.797
start validation test
0.606855670103
0.63029925187
0.520222290831
0.569994925861
0.607007768363
64.774
batch start
#iterations: 300
currently lose_sum: 98.43736040592194
time_elpased: 1.807
batch start
#iterations: 301
currently lose_sum: 98.34011566638947
time_elpased: 1.8
batch start
#iterations: 302
currently lose_sum: 98.56926327943802
time_elpased: 1.798
batch start
#iterations: 303
currently lose_sum: 98.7546159029007
time_elpased: 1.809
batch start
#iterations: 304
currently lose_sum: 98.51655840873718
time_elpased: 1.803
batch start
#iterations: 305
currently lose_sum: 98.62079375982285
time_elpased: 1.828
batch start
#iterations: 306
currently lose_sum: 98.47542583942413
time_elpased: 1.884
batch start
#iterations: 307
currently lose_sum: 98.79924786090851
time_elpased: 1.867
batch start
#iterations: 308
currently lose_sum: 98.3152249455452
time_elpased: 1.842
batch start
#iterations: 309
currently lose_sum: 98.54436135292053
time_elpased: 1.81
batch start
#iterations: 310
currently lose_sum: 98.7782432436943
time_elpased: 1.778
batch start
#iterations: 311
currently lose_sum: 98.40254843235016
time_elpased: 1.796
batch start
#iterations: 312
currently lose_sum: 98.57219588756561
time_elpased: 1.813
batch start
#iterations: 313
currently lose_sum: 98.4205179810524
time_elpased: 1.827
batch start
#iterations: 314
currently lose_sum: 98.79528325796127
time_elpased: 1.789
batch start
#iterations: 315
currently lose_sum: 98.4750868678093
time_elpased: 1.761
batch start
#iterations: 316
currently lose_sum: 98.61514180898666
time_elpased: 1.776
batch start
#iterations: 317
currently lose_sum: 98.63194561004639
time_elpased: 1.771
batch start
#iterations: 318
currently lose_sum: 98.60431689023972
time_elpased: 1.772
batch start
#iterations: 319
currently lose_sum: 98.73858684301376
time_elpased: 1.763
start validation test
0.605515463918
0.625547445255
0.529175671504
0.573340023415
0.605649490198
64.782
batch start
#iterations: 320
currently lose_sum: 98.57835102081299
time_elpased: 1.789
batch start
#iterations: 321
currently lose_sum: 98.58280432224274
time_elpased: 1.842
batch start
#iterations: 322
currently lose_sum: 98.72414249181747
time_elpased: 1.775
batch start
#iterations: 323
currently lose_sum: 98.50706309080124
time_elpased: 1.796
batch start
#iterations: 324
currently lose_sum: 98.92335277795792
time_elpased: 1.82
batch start
#iterations: 325
currently lose_sum: 98.70817989110947
time_elpased: 1.836
batch start
#iterations: 326
currently lose_sum: 98.60232281684875
time_elpased: 1.775
batch start
#iterations: 327
currently lose_sum: 98.62017667293549
time_elpased: 1.781
batch start
#iterations: 328
currently lose_sum: 98.6316921710968
time_elpased: 1.754
batch start
#iterations: 329
currently lose_sum: 98.53110802173615
time_elpased: 1.775
batch start
#iterations: 330
currently lose_sum: 98.7559683918953
time_elpased: 1.766
batch start
#iterations: 331
currently lose_sum: 98.49373358488083
time_elpased: 1.801
batch start
#iterations: 332
currently lose_sum: 98.28904610872269
time_elpased: 1.811
batch start
#iterations: 333
currently lose_sum: 98.39327329397202
time_elpased: 1.784
batch start
#iterations: 334
currently lose_sum: 98.55290794372559
time_elpased: 1.81
batch start
#iterations: 335
currently lose_sum: 98.49959200620651
time_elpased: 1.81
batch start
#iterations: 336
currently lose_sum: 98.49220150709152
time_elpased: 1.818
batch start
#iterations: 337
currently lose_sum: 98.44395476579666
time_elpased: 1.81
batch start
#iterations: 338
currently lose_sum: 98.426260471344
time_elpased: 1.788
batch start
#iterations: 339
currently lose_sum: 98.68143552541733
time_elpased: 1.784
start validation test
0.606597938144
0.62243100411
0.545435834105
0.581395348837
0.606705317652
64.561
batch start
#iterations: 340
currently lose_sum: 98.61881399154663
time_elpased: 1.795
batch start
#iterations: 341
currently lose_sum: 99.0119234919548
time_elpased: 1.781
batch start
#iterations: 342
currently lose_sum: 98.53347510099411
time_elpased: 1.803
batch start
#iterations: 343
currently lose_sum: 98.57938635349274
time_elpased: 1.846
batch start
#iterations: 344
currently lose_sum: 98.62747263908386
time_elpased: 1.787
batch start
#iterations: 345
currently lose_sum: 98.71519255638123
time_elpased: 1.797
batch start
#iterations: 346
currently lose_sum: 98.48347759246826
time_elpased: 1.962
batch start
#iterations: 347
currently lose_sum: 98.66904377937317
time_elpased: 1.821
batch start
#iterations: 348
currently lose_sum: 98.64395868778229
time_elpased: 1.842
batch start
#iterations: 349
currently lose_sum: 98.51838910579681
time_elpased: 1.858
batch start
#iterations: 350
currently lose_sum: 98.63409262895584
time_elpased: 1.78
batch start
#iterations: 351
currently lose_sum: 98.784843146801
time_elpased: 1.904
batch start
#iterations: 352
currently lose_sum: 98.55689042806625
time_elpased: 1.958
batch start
#iterations: 353
currently lose_sum: 98.63541615009308
time_elpased: 1.85
batch start
#iterations: 354
currently lose_sum: 98.64824783802032
time_elpased: 1.809
batch start
#iterations: 355
currently lose_sum: 98.7445559501648
time_elpased: 1.82
batch start
#iterations: 356
currently lose_sum: 98.4489756822586
time_elpased: 1.914
batch start
#iterations: 357
currently lose_sum: 98.56101733446121
time_elpased: 1.863
batch start
#iterations: 358
currently lose_sum: 98.59769123792648
time_elpased: 1.92
batch start
#iterations: 359
currently lose_sum: 98.7654596567154
time_elpased: 1.98
start validation test
0.603298969072
0.6246453682
0.521148502624
0.568222621185
0.603443196887
64.821
batch start
#iterations: 360
currently lose_sum: 98.74857461452484
time_elpased: 1.774
batch start
#iterations: 361
currently lose_sum: 98.83245027065277
time_elpased: 1.807
batch start
#iterations: 362
currently lose_sum: 98.5692595243454
time_elpased: 1.766
batch start
#iterations: 363
currently lose_sum: 98.36547309160233
time_elpased: 1.766
batch start
#iterations: 364
currently lose_sum: 98.74729979038239
time_elpased: 1.757
batch start
#iterations: 365
currently lose_sum: 98.62204337120056
time_elpased: 1.82
batch start
#iterations: 366
currently lose_sum: 98.76639956235886
time_elpased: 1.803
batch start
#iterations: 367
currently lose_sum: 98.2741105556488
time_elpased: 1.818
batch start
#iterations: 368
currently lose_sum: 98.56651145219803
time_elpased: 1.818
batch start
#iterations: 369
currently lose_sum: 98.42043882608414
time_elpased: 1.827
batch start
#iterations: 370
currently lose_sum: 98.69663280248642
time_elpased: 1.788
batch start
#iterations: 371
currently lose_sum: 98.44649565219879
time_elpased: 1.823
batch start
#iterations: 372
currently lose_sum: 98.58277010917664
time_elpased: 1.789
batch start
#iterations: 373
currently lose_sum: 98.63345324993134
time_elpased: 1.768
batch start
#iterations: 374
currently lose_sum: 98.50574314594269
time_elpased: 1.797
batch start
#iterations: 375
currently lose_sum: 98.64692497253418
time_elpased: 1.809
batch start
#iterations: 376
currently lose_sum: 98.50845146179199
time_elpased: 1.824
batch start
#iterations: 377
currently lose_sum: 98.42510026693344
time_elpased: 1.772
batch start
#iterations: 378
currently lose_sum: 98.74350500106812
time_elpased: 1.817
batch start
#iterations: 379
currently lose_sum: 98.37610226869583
time_elpased: 1.776
start validation test
0.604896907216
0.631403688525
0.507358238139
0.562624821683
0.605068151394
64.718
batch start
#iterations: 380
currently lose_sum: 98.71473580598831
time_elpased: 1.842
batch start
#iterations: 381
currently lose_sum: 98.44729280471802
time_elpased: 1.778
batch start
#iterations: 382
currently lose_sum: 98.65196323394775
time_elpased: 1.811
batch start
#iterations: 383
currently lose_sum: 98.45031118392944
time_elpased: 1.787
batch start
#iterations: 384
currently lose_sum: 98.43948584794998
time_elpased: 1.789
batch start
#iterations: 385
currently lose_sum: 98.66020685434341
time_elpased: 1.795
batch start
#iterations: 386
currently lose_sum: 98.57577139139175
time_elpased: 1.796
batch start
#iterations: 387
currently lose_sum: 98.3077204823494
time_elpased: 1.819
batch start
#iterations: 388
currently lose_sum: 98.53674709796906
time_elpased: 1.777
batch start
#iterations: 389
currently lose_sum: 98.55993735790253
time_elpased: 1.829
batch start
#iterations: 390
currently lose_sum: 98.64732027053833
time_elpased: 1.805
batch start
#iterations: 391
currently lose_sum: 98.38352179527283
time_elpased: 1.769
batch start
#iterations: 392
currently lose_sum: 98.366903424263
time_elpased: 1.853
batch start
#iterations: 393
currently lose_sum: 98.47812050580978
time_elpased: 1.871
batch start
#iterations: 394
currently lose_sum: 98.38471013307571
time_elpased: 1.872
batch start
#iterations: 395
currently lose_sum: 98.68777650594711
time_elpased: 1.884
batch start
#iterations: 396
currently lose_sum: 98.33471882343292
time_elpased: 1.903
batch start
#iterations: 397
currently lose_sum: 98.42103636264801
time_elpased: 1.782
batch start
#iterations: 398
currently lose_sum: 98.46222996711731
time_elpased: 1.773
batch start
#iterations: 399
currently lose_sum: 98.66270792484283
time_elpased: 1.786
start validation test
0.607474226804
0.63596377749
0.505917464238
0.563535278271
0.607652525365
64.688
acc: 0.605
pre: 0.619
rec: 0.547
F1: 0.581
auc: 0.605
