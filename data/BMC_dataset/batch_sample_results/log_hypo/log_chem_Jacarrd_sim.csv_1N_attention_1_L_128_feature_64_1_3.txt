start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 101.30015939474106
time_elpased: 2.52
batch start
#iterations: 1
currently lose_sum: 100.4035832285881
time_elpased: 2.661
batch start
#iterations: 2
currently lose_sum: 100.39932882785797
time_elpased: 2.62
batch start
#iterations: 3
currently lose_sum: 100.32106757164001
time_elpased: 2.671
batch start
#iterations: 4
currently lose_sum: 100.21061044931412
time_elpased: 2.709
batch start
#iterations: 5
currently lose_sum: 100.1519809961319
time_elpased: 2.7
batch start
#iterations: 6
currently lose_sum: 100.17308831214905
time_elpased: 2.595
batch start
#iterations: 7
currently lose_sum: 100.14862966537476
time_elpased: 2.737
batch start
#iterations: 8
currently lose_sum: 99.91722440719604
time_elpased: 2.622
batch start
#iterations: 9
currently lose_sum: 99.79260051250458
time_elpased: 2.497
batch start
#iterations: 10
currently lose_sum: 99.80366855859756
time_elpased: 2.734
batch start
#iterations: 11
currently lose_sum: 99.64902967214584
time_elpased: 2.647
batch start
#iterations: 12
currently lose_sum: 99.5314290523529
time_elpased: 2.721
batch start
#iterations: 13
currently lose_sum: 99.37761783599854
time_elpased: 2.523
batch start
#iterations: 14
currently lose_sum: 99.16138035058975
time_elpased: 2.649
batch start
#iterations: 15
currently lose_sum: 99.18860167264938
time_elpased: 2.543
batch start
#iterations: 16
currently lose_sum: 99.18940931558609
time_elpased: 2.276
batch start
#iterations: 17
currently lose_sum: 99.09003990888596
time_elpased: 2.568
batch start
#iterations: 18
currently lose_sum: 98.77702569961548
time_elpased: 2.631
batch start
#iterations: 19
currently lose_sum: 98.6850135922432
time_elpased: 2.536
start validation test
0.627577319588
0.596483073824
0.792507204611
0.680662983425
0.62730482081
64.528
batch start
#iterations: 20
currently lose_sum: 98.6279154419899
time_elpased: 2.693
batch start
#iterations: 21
currently lose_sum: 98.54369068145752
time_elpased: 2.777
batch start
#iterations: 22
currently lose_sum: 98.38833636045456
time_elpased: 2.606
batch start
#iterations: 23
currently lose_sum: 98.01331865787506
time_elpased: 2.744
batch start
#iterations: 24
currently lose_sum: 98.11114943027496
time_elpased: 2.477
batch start
#iterations: 25
currently lose_sum: 98.1467906832695
time_elpased: 2.567
batch start
#iterations: 26
currently lose_sum: 97.72147190570831
time_elpased: 2.754
batch start
#iterations: 27
currently lose_sum: 98.21442198753357
time_elpased: 2.667
batch start
#iterations: 28
currently lose_sum: 97.78001719713211
time_elpased: 2.711
batch start
#iterations: 29
currently lose_sum: 98.17151546478271
time_elpased: 2.626
batch start
#iterations: 30
currently lose_sum: 97.66062653064728
time_elpased: 2.684
batch start
#iterations: 31
currently lose_sum: 97.83188724517822
time_elpased: 2.837
batch start
#iterations: 32
currently lose_sum: 97.5205630660057
time_elpased: 2.777
batch start
#iterations: 33
currently lose_sum: 97.69266521930695
time_elpased: 2.726
batch start
#iterations: 34
currently lose_sum: 97.5721669793129
time_elpased: 2.783
batch start
#iterations: 35
currently lose_sum: 97.90121924877167
time_elpased: 2.544
batch start
#iterations: 36
currently lose_sum: 97.59263861179352
time_elpased: 2.759
batch start
#iterations: 37
currently lose_sum: 97.51855087280273
time_elpased: 2.686
batch start
#iterations: 38
currently lose_sum: 97.24537128210068
time_elpased: 2.794
batch start
#iterations: 39
currently lose_sum: 97.58702486753464
time_elpased: 2.568
start validation test
0.651443298969
0.632442611191
0.725916014821
0.67596319724
0.651320254416
62.851
batch start
#iterations: 40
currently lose_sum: 97.40485519170761
time_elpased: 2.636
batch start
#iterations: 41
currently lose_sum: 97.49645501375198
time_elpased: 2.612
batch start
#iterations: 42
currently lose_sum: 97.27824407815933
time_elpased: 2.663
batch start
#iterations: 43
currently lose_sum: 97.2579836845398
time_elpased: 2.516
batch start
#iterations: 44
currently lose_sum: 97.38080894947052
time_elpased: 2.815
batch start
#iterations: 45
currently lose_sum: 97.23784118890762
time_elpased: 2.635
batch start
#iterations: 46
currently lose_sum: 96.87005460262299
time_elpased: 2.67
batch start
#iterations: 47
currently lose_sum: 97.28731375932693
time_elpased: 2.542
batch start
#iterations: 48
currently lose_sum: 96.99379849433899
time_elpased: 2.627
batch start
#iterations: 49
currently lose_sum: 97.33650660514832
time_elpased: 2.76
batch start
#iterations: 50
currently lose_sum: 97.04714059829712
time_elpased: 2.666
batch start
#iterations: 51
currently lose_sum: 97.05509066581726
time_elpased: 2.541
batch start
#iterations: 52
currently lose_sum: 97.12818336486816
time_elpased: 2.613
batch start
#iterations: 53
currently lose_sum: 96.93362581729889
time_elpased: 2.737
batch start
#iterations: 54
currently lose_sum: 97.35890966653824
time_elpased: 2.685
batch start
#iterations: 55
currently lose_sum: 97.10272514820099
time_elpased: 2.462
batch start
#iterations: 56
currently lose_sum: 97.10373967885971
time_elpased: 2.666
batch start
#iterations: 57
currently lose_sum: 97.17504251003265
time_elpased: 2.642
batch start
#iterations: 58
currently lose_sum: 96.9231795668602
time_elpased: 2.427
batch start
#iterations: 59
currently lose_sum: 97.06223958730698
time_elpased: 2.415
start validation test
0.633917525773
0.638131473261
0.621449156031
0.629679841485
0.633938126136
62.880
batch start
#iterations: 60
currently lose_sum: 96.85402250289917
time_elpased: 2.448
batch start
#iterations: 61
currently lose_sum: 97.15116941928864
time_elpased: 2.186
batch start
#iterations: 62
currently lose_sum: 97.29444044828415
time_elpased: 2.372
batch start
#iterations: 63
currently lose_sum: 96.72193276882172
time_elpased: 2.358
batch start
#iterations: 64
currently lose_sum: 97.04966431856155
time_elpased: 2.674
batch start
#iterations: 65
currently lose_sum: 96.93414151668549
time_elpased: 2.691
batch start
#iterations: 66
currently lose_sum: 96.85986793041229
time_elpased: 2.347
batch start
#iterations: 67
currently lose_sum: 97.26254463195801
time_elpased: 2.587
batch start
#iterations: 68
currently lose_sum: 96.92033237218857
time_elpased: 2.652
batch start
#iterations: 69
currently lose_sum: 96.84407538175583
time_elpased: 2.793
batch start
#iterations: 70
currently lose_sum: 96.87994921207428
time_elpased: 2.631
batch start
#iterations: 71
currently lose_sum: 96.80661940574646
time_elpased: 2.761
batch start
#iterations: 72
currently lose_sum: 96.6355881690979
time_elpased: 2.612
batch start
#iterations: 73
currently lose_sum: 96.81193941831589
time_elpased: 2.414
batch start
#iterations: 74
currently lose_sum: 96.61351960897446
time_elpased: 2.546
batch start
#iterations: 75
currently lose_sum: 96.88027465343475
time_elpased: 2.948
batch start
#iterations: 76
currently lose_sum: 96.62972432374954
time_elpased: 2.538
batch start
#iterations: 77
currently lose_sum: 96.61130833625793
time_elpased: 2.609
batch start
#iterations: 78
currently lose_sum: 96.70557403564453
time_elpased: 2.927
batch start
#iterations: 79
currently lose_sum: 96.69048970937729
time_elpased: 2.762
start validation test
0.645154639175
0.641826923077
0.659530671058
0.650558375635
0.645130886954
62.508
batch start
#iterations: 80
currently lose_sum: 96.7618339061737
time_elpased: 2.6
batch start
#iterations: 81
currently lose_sum: 96.51798421144485
time_elpased: 2.327
batch start
#iterations: 82
currently lose_sum: 96.83132576942444
time_elpased: 2.194
batch start
#iterations: 83
currently lose_sum: 96.46209269762039
time_elpased: 2.319
batch start
#iterations: 84
currently lose_sum: 96.45875406265259
time_elpased: 2.66
batch start
#iterations: 85
currently lose_sum: 96.77820229530334
time_elpased: 2.722
batch start
#iterations: 86
currently lose_sum: 96.39568710327148
time_elpased: 2.765
batch start
#iterations: 87
currently lose_sum: 96.33461833000183
time_elpased: 2.837
batch start
#iterations: 88
currently lose_sum: 96.30140334367752
time_elpased: 2.696
batch start
#iterations: 89
currently lose_sum: 96.82252568006516
time_elpased: 2.593
batch start
#iterations: 90
currently lose_sum: 96.67039322853088
time_elpased: 2.538
batch start
#iterations: 91
currently lose_sum: 96.52827888727188
time_elpased: 2.599
batch start
#iterations: 92
currently lose_sum: 96.456871509552
time_elpased: 2.818
batch start
#iterations: 93
currently lose_sum: 96.29969614744186
time_elpased: 2.752
batch start
#iterations: 94
currently lose_sum: 96.5800604224205
time_elpased: 2.654
batch start
#iterations: 95
currently lose_sum: 96.54476952552795
time_elpased: 2.633
batch start
#iterations: 96
currently lose_sum: 96.76999872922897
time_elpased: 2.267
batch start
#iterations: 97
currently lose_sum: 96.27405643463135
time_elpased: 2.588
batch start
#iterations: 98
currently lose_sum: 96.58337169885635
time_elpased: 2.778
batch start
#iterations: 99
currently lose_sum: 96.5979877114296
time_elpased: 2.545
start validation test
0.655515463918
0.623262618873
0.789213668176
0.696489395522
0.65529456643
61.995
batch start
#iterations: 100
currently lose_sum: 96.69817304611206
time_elpased: 2.8
batch start
#iterations: 101
currently lose_sum: 96.69466012716293
time_elpased: 2.752
batch start
#iterations: 102
currently lose_sum: 96.75490999221802
time_elpased: 3.0
batch start
#iterations: 103
currently lose_sum: 96.59149831533432
time_elpased: 2.745
batch start
#iterations: 104
currently lose_sum: 96.48032236099243
time_elpased: 2.757
batch start
#iterations: 105
currently lose_sum: 96.3402693271637
time_elpased: 2.615
batch start
#iterations: 106
currently lose_sum: 96.33293169736862
time_elpased: 2.437
batch start
#iterations: 107
currently lose_sum: 96.58974933624268
time_elpased: 2.48
batch start
#iterations: 108
currently lose_sum: 96.22637808322906
time_elpased: 2.376
batch start
#iterations: 109
currently lose_sum: 96.64500892162323
time_elpased: 2.772
batch start
#iterations: 110
currently lose_sum: 96.30000811815262
time_elpased: 2.735
batch start
#iterations: 111
currently lose_sum: 96.07880628108978
time_elpased: 2.725
batch start
#iterations: 112
currently lose_sum: 95.71571362018585
time_elpased: 2.811
batch start
#iterations: 113
currently lose_sum: 96.33492857217789
time_elpased: 2.75
batch start
#iterations: 114
currently lose_sum: 96.50305312871933
time_elpased: 2.732
batch start
#iterations: 115
currently lose_sum: 96.31774061918259
time_elpased: 2.489
batch start
#iterations: 116
currently lose_sum: 96.05063563585281
time_elpased: 2.679
batch start
#iterations: 117
currently lose_sum: 96.12435114383698
time_elpased: 2.806
batch start
#iterations: 118
currently lose_sum: 96.42710894346237
time_elpased: 3.03
batch start
#iterations: 119
currently lose_sum: 96.44988167285919
time_elpased: 3.019
start validation test
0.65087628866
0.623437631071
0.76492383697
0.686971391598
0.650687858179
61.925
batch start
#iterations: 120
currently lose_sum: 96.0327262878418
time_elpased: 2.667
batch start
#iterations: 121
currently lose_sum: 96.13033360242844
time_elpased: 2.598
batch start
#iterations: 122
currently lose_sum: 95.97875046730042
time_elpased: 2.743
batch start
#iterations: 123
currently lose_sum: 96.5522398352623
time_elpased: 2.979
batch start
#iterations: 124
currently lose_sum: 96.3733931183815
time_elpased: 2.586
batch start
#iterations: 125
currently lose_sum: 95.84447276592255
time_elpased: 2.728
batch start
#iterations: 126
currently lose_sum: 95.88501065969467
time_elpased: 2.871
batch start
#iterations: 127
currently lose_sum: 96.18851315975189
time_elpased: 2.557
batch start
#iterations: 128
currently lose_sum: 95.95472794771194
time_elpased: 2.614
batch start
#iterations: 129
currently lose_sum: 96.17415577173233
time_elpased: 2.652
batch start
#iterations: 130
currently lose_sum: 96.56695032119751
time_elpased: 2.705
batch start
#iterations: 131
currently lose_sum: 96.13094711303711
time_elpased: 2.58
batch start
#iterations: 132
currently lose_sum: 95.97868221998215
time_elpased: 2.848
batch start
#iterations: 133
currently lose_sum: 96.10899472236633
time_elpased: 2.588
batch start
#iterations: 134
currently lose_sum: 96.38615548610687
time_elpased: 3.11
batch start
#iterations: 135
currently lose_sum: 96.41521507501602
time_elpased: 2.825
batch start
#iterations: 136
currently lose_sum: 96.21636873483658
time_elpased: 2.835
batch start
#iterations: 137
currently lose_sum: 96.06665623188019
time_elpased: 2.621
batch start
#iterations: 138
currently lose_sum: 96.1623802781105
time_elpased: 2.507
batch start
#iterations: 139
currently lose_sum: 95.9293903708458
time_elpased: 2.763
start validation test
0.645670103093
0.635333333333
0.686599423631
0.659972299169
0.645602479267
62.103
batch start
#iterations: 140
currently lose_sum: 96.00066995620728
time_elpased: 2.773
batch start
#iterations: 141
currently lose_sum: 96.05646228790283
time_elpased: 2.795
batch start
#iterations: 142
currently lose_sum: 96.05286282300949
time_elpased: 2.823
batch start
#iterations: 143
currently lose_sum: 96.00495529174805
time_elpased: 2.9
batch start
#iterations: 144
currently lose_sum: 96.15618842840195
time_elpased: 2.353
batch start
#iterations: 145
currently lose_sum: 95.75711447000504
time_elpased: 2.687
batch start
#iterations: 146
currently lose_sum: 95.92863017320633
time_elpased: 2.91
batch start
#iterations: 147
currently lose_sum: 96.21374136209488
time_elpased: 2.487
batch start
#iterations: 148
currently lose_sum: 96.08483684062958
time_elpased: 2.824
batch start
#iterations: 149
currently lose_sum: 95.8212366104126
time_elpased: 2.883
batch start
#iterations: 150
currently lose_sum: 96.20583552122116
time_elpased: 2.655
batch start
#iterations: 151
currently lose_sum: 96.03022128343582
time_elpased: 2.495
batch start
#iterations: 152
currently lose_sum: 95.86559128761292
time_elpased: 2.458
batch start
#iterations: 153
currently lose_sum: 96.28801047801971
time_elpased: 2.584
batch start
#iterations: 154
currently lose_sum: 95.84957957267761
time_elpased: 2.766
batch start
#iterations: 155
currently lose_sum: 95.93837052583694
time_elpased: 2.545
batch start
#iterations: 156
currently lose_sum: 95.93834829330444
time_elpased: 2.751
batch start
#iterations: 157
currently lose_sum: 95.97669649124146
time_elpased: 2.824
batch start
#iterations: 158
currently lose_sum: 96.24483925104141
time_elpased: 2.76
batch start
#iterations: 159
currently lose_sum: 95.76368272304535
time_elpased: 2.932
start validation test
0.652474226804
0.61283958112
0.831206257719
0.705512361317
0.652178923986
61.987
batch start
#iterations: 160
currently lose_sum: 95.98947149515152
time_elpased: 2.594
batch start
#iterations: 161
currently lose_sum: 96.04531747102737
time_elpased: 2.44
batch start
#iterations: 162
currently lose_sum: 96.06801462173462
time_elpased: 2.639
batch start
#iterations: 163
currently lose_sum: 95.75655168294907
time_elpased: 2.621
batch start
#iterations: 164
currently lose_sum: 96.21694999933243
time_elpased: 2.796
batch start
#iterations: 165
currently lose_sum: 95.97657251358032
time_elpased: 2.571
batch start
#iterations: 166
currently lose_sum: 95.8906199336052
time_elpased: 2.634
batch start
#iterations: 167
currently lose_sum: 95.50024896860123
time_elpased: 2.766
batch start
#iterations: 168
currently lose_sum: 96.1919093132019
time_elpased: 2.767
batch start
#iterations: 169
currently lose_sum: 96.02874392271042
time_elpased: 2.752
batch start
#iterations: 170
currently lose_sum: 95.67214637994766
time_elpased: 2.439
batch start
#iterations: 171
currently lose_sum: 95.96758151054382
time_elpased: 2.663
batch start
#iterations: 172
currently lose_sum: 95.8485723733902
time_elpased: 2.772
batch start
#iterations: 173
currently lose_sum: 95.4921543598175
time_elpased: 2.746
batch start
#iterations: 174
currently lose_sum: 95.66305416822433
time_elpased: 2.785
batch start
#iterations: 175
currently lose_sum: 95.82508438825607
time_elpased: 2.477
batch start
#iterations: 176
currently lose_sum: 96.19062596559525
time_elpased: 2.983
batch start
#iterations: 177
currently lose_sum: 95.80567044019699
time_elpased: 2.543
batch start
#iterations: 178
currently lose_sum: 95.8007128238678
time_elpased: 2.565
batch start
#iterations: 179
currently lose_sum: 95.42410838603973
time_elpased: 2.796
start validation test
0.65824742268
0.623856156686
0.799917661589
0.701001172544
0.658013353719
61.391
batch start
#iterations: 180
currently lose_sum: 95.6067607998848
time_elpased: 2.592
batch start
#iterations: 181
currently lose_sum: 95.95215088129044
time_elpased: 2.942
batch start
#iterations: 182
currently lose_sum: 95.95776325464249
time_elpased: 2.617
batch start
#iterations: 183
currently lose_sum: 95.66197645664215
time_elpased: 2.616
batch start
#iterations: 184
currently lose_sum: 95.89671677350998
time_elpased: 2.913
batch start
#iterations: 185
currently lose_sum: 95.6223983168602
time_elpased: 2.63
batch start
#iterations: 186
currently lose_sum: 95.96226263046265
time_elpased: 2.717
batch start
#iterations: 187
currently lose_sum: 95.95511186122894
time_elpased: 2.811
batch start
#iterations: 188
currently lose_sum: 96.03589862585068
time_elpased: 2.315
batch start
#iterations: 189
currently lose_sum: 95.76574456691742
time_elpased: 2.484
batch start
#iterations: 190
currently lose_sum: 95.88174533843994
time_elpased: 2.602
batch start
#iterations: 191
currently lose_sum: 96.0325140953064
time_elpased: 2.972
batch start
#iterations: 192
currently lose_sum: 95.70110702514648
time_elpased: 3.054
batch start
#iterations: 193
currently lose_sum: 95.33739972114563
time_elpased: 2.865
batch start
#iterations: 194
currently lose_sum: 95.87442862987518
time_elpased: 2.697
batch start
#iterations: 195
currently lose_sum: 95.37493062019348
time_elpased: 2.804
batch start
#iterations: 196
currently lose_sum: 95.4997810125351
time_elpased: 2.705
batch start
#iterations: 197
currently lose_sum: 95.40394139289856
time_elpased: 2.614
batch start
#iterations: 198
currently lose_sum: 95.33136081695557
time_elpased: 2.687
batch start
#iterations: 199
currently lose_sum: 95.71003890037537
time_elpased: 2.532
start validation test
0.654226804124
0.640508221226
0.705640181145
0.671498530852
0.654141858437
61.561
batch start
#iterations: 200
currently lose_sum: 95.61187493801117
time_elpased: 2.648
batch start
#iterations: 201
currently lose_sum: 95.64388608932495
time_elpased: 2.727
batch start
#iterations: 202
currently lose_sum: 95.9071010351181
time_elpased: 2.52
batch start
#iterations: 203
currently lose_sum: 96.08935636281967
time_elpased: 2.564
batch start
#iterations: 204
currently lose_sum: 95.89846414327621
time_elpased: 2.776
batch start
#iterations: 205
currently lose_sum: 95.50340753793716
time_elpased: 2.606
batch start
#iterations: 206
currently lose_sum: 95.47527396678925
time_elpased: 2.714
batch start
#iterations: 207
currently lose_sum: 95.80023342370987
time_elpased: 2.741
batch start
#iterations: 208
currently lose_sum: 95.69618773460388
time_elpased: 2.601
batch start
#iterations: 209
currently lose_sum: 95.58298689126968
time_elpased: 2.553
batch start
#iterations: 210
currently lose_sum: 95.08400774002075
time_elpased: 2.68
batch start
#iterations: 211
currently lose_sum: 95.13442087173462
time_elpased: 2.657
batch start
#iterations: 212
currently lose_sum: 96.1400101184845
time_elpased: 2.759
batch start
#iterations: 213
currently lose_sum: 95.50626868009567
time_elpased: 2.776
batch start
#iterations: 214
currently lose_sum: 95.27024203538895
time_elpased: 2.413
batch start
#iterations: 215
currently lose_sum: 95.62018936872482
time_elpased: 2.617
batch start
#iterations: 216
currently lose_sum: 95.51692366600037
time_elpased: 2.723
batch start
#iterations: 217
currently lose_sum: 95.52004504203796
time_elpased: 2.698
batch start
#iterations: 218
currently lose_sum: 95.55770808458328
time_elpased: 2.442
batch start
#iterations: 219
currently lose_sum: 95.3303456902504
time_elpased: 2.619
start validation test
0.658505154639
0.633198310782
0.756175380815
0.689244336038
0.658343782931
61.229
batch start
#iterations: 220
currently lose_sum: 95.27607369422913
time_elpased: 2.799
batch start
#iterations: 221
currently lose_sum: 95.96776115894318
time_elpased: 2.666
batch start
#iterations: 222
currently lose_sum: 95.07382214069366
time_elpased: 2.488
batch start
#iterations: 223
currently lose_sum: 95.1113618016243
time_elpased: 2.592
batch start
#iterations: 224
currently lose_sum: 95.06855475902557
time_elpased: 2.533
batch start
#iterations: 225
currently lose_sum: 95.40054631233215
time_elpased: 2.808
batch start
#iterations: 226
currently lose_sum: 95.35871315002441
time_elpased: 2.674
batch start
#iterations: 227
currently lose_sum: 95.474276304245
time_elpased: 2.701
batch start
#iterations: 228
currently lose_sum: 95.19539177417755
time_elpased: 2.855
batch start
#iterations: 229
currently lose_sum: 95.18810623884201
time_elpased: 2.907
batch start
#iterations: 230
currently lose_sum: 95.67068845033646
time_elpased: 2.852
batch start
#iterations: 231
currently lose_sum: 95.71301805973053
time_elpased: 2.665
batch start
#iterations: 232
currently lose_sum: 95.5966369509697
time_elpased: 2.707
batch start
#iterations: 233
currently lose_sum: 95.82942289113998
time_elpased: 2.744
batch start
#iterations: 234
currently lose_sum: 95.5119771361351
time_elpased: 2.843
batch start
#iterations: 235
currently lose_sum: 95.55868524312973
time_elpased: 2.67
batch start
#iterations: 236
currently lose_sum: 95.20722097158432
time_elpased: 2.859
batch start
#iterations: 237
currently lose_sum: 95.46018648147583
time_elpased: 2.443
batch start
#iterations: 238
currently lose_sum: 95.46849197149277
time_elpased: 2.821
batch start
#iterations: 239
currently lose_sum: 95.3174854516983
time_elpased: 2.76
start validation test
0.660824742268
0.63744740533
0.748456154796
0.688505964779
0.660679956787
61.291
batch start
#iterations: 240
currently lose_sum: 95.3143208026886
time_elpased: 2.693
batch start
#iterations: 241
currently lose_sum: 95.35169237852097
time_elpased: 2.727
batch start
#iterations: 242
currently lose_sum: 95.31650751829147
time_elpased: 2.719
batch start
#iterations: 243
currently lose_sum: 95.40992385149002
time_elpased: 2.675
batch start
#iterations: 244
currently lose_sum: 95.54918730258942
time_elpased: 2.691
batch start
#iterations: 245
currently lose_sum: 95.31870490312576
time_elpased: 2.828
batch start
#iterations: 246
currently lose_sum: 95.19020503759384
time_elpased: 2.887
batch start
#iterations: 247
currently lose_sum: 95.78628915548325
time_elpased: 2.674
batch start
#iterations: 248
currently lose_sum: 95.30620205402374
time_elpased: 2.474
batch start
#iterations: 249
currently lose_sum: 95.9158530831337
time_elpased: 2.797
batch start
#iterations: 250
currently lose_sum: 95.4136198759079
time_elpased: 2.699
batch start
#iterations: 251
currently lose_sum: 95.66023373603821
time_elpased: 2.725
batch start
#iterations: 252
currently lose_sum: 95.24336433410645
time_elpased: 2.416
batch start
#iterations: 253
currently lose_sum: 95.31223905086517
time_elpased: 2.489
batch start
#iterations: 254
currently lose_sum: 95.36926007270813
time_elpased: 2.672
batch start
#iterations: 255
currently lose_sum: 95.60893714427948
time_elpased: 2.886
batch start
#iterations: 256
currently lose_sum: 94.8691338300705
time_elpased: 2.486
batch start
#iterations: 257
currently lose_sum: 95.3574213385582
time_elpased: 2.466
batch start
#iterations: 258
currently lose_sum: 95.42728906869888
time_elpased: 2.23
batch start
#iterations: 259
currently lose_sum: 95.62597244977951
time_elpased: 2.936
start validation test
0.658350515464
0.635414839502
0.745677233429
0.686144521261
0.658206233402
61.371
batch start
#iterations: 260
currently lose_sum: 95.40367293357849
time_elpased: 2.459
batch start
#iterations: 261
currently lose_sum: 95.00709933042526
time_elpased: 2.86
batch start
#iterations: 262
currently lose_sum: 94.95043730735779
time_elpased: 2.644
batch start
#iterations: 263
currently lose_sum: 95.1293950676918
time_elpased: 2.277
batch start
#iterations: 264
currently lose_sum: 95.6004006266594
time_elpased: 2.436
batch start
#iterations: 265
currently lose_sum: 95.19984948635101
time_elpased: 2.879
batch start
#iterations: 266
currently lose_sum: 95.09626805782318
time_elpased: 2.643
batch start
#iterations: 267
currently lose_sum: 95.58672165870667
time_elpased: 2.384
batch start
#iterations: 268
currently lose_sum: 95.15960985422134
time_elpased: 2.696
batch start
#iterations: 269
currently lose_sum: 95.49689120054245
time_elpased: 2.888
batch start
#iterations: 270
currently lose_sum: 95.01566255092621
time_elpased: 2.833
batch start
#iterations: 271
currently lose_sum: 95.301706969738
time_elpased: 2.677
batch start
#iterations: 272
currently lose_sum: 95.0825184583664
time_elpased: 2.866
batch start
#iterations: 273
currently lose_sum: 95.27046728134155
time_elpased: 2.607
batch start
#iterations: 274
currently lose_sum: 95.31784200668335
time_elpased: 2.567
batch start
#iterations: 275
currently lose_sum: 95.44434028863907
time_elpased: 2.707
batch start
#iterations: 276
currently lose_sum: 95.1268435716629
time_elpased: 2.583
batch start
#iterations: 277
currently lose_sum: 95.0306311249733
time_elpased: 2.638
batch start
#iterations: 278
currently lose_sum: 95.1967476606369
time_elpased: 2.574
batch start
#iterations: 279
currently lose_sum: 95.37823015451431
time_elpased: 2.705
start validation test
0.631597938144
0.650603822253
0.571119802388
0.608276240066
0.631697860715
62.145
batch start
#iterations: 280
currently lose_sum: 95.28884971141815
time_elpased: 2.736
batch start
#iterations: 281
currently lose_sum: 95.05802112817764
time_elpased: 2.26
batch start
#iterations: 282
currently lose_sum: 95.54857689142227
time_elpased: 2.212
batch start
#iterations: 283
currently lose_sum: 95.3188396692276
time_elpased: 2.467
batch start
#iterations: 284
currently lose_sum: 95.01120030879974
time_elpased: 2.451
batch start
#iterations: 285
currently lose_sum: 95.24987572431564
time_elpased: 2.506
batch start
#iterations: 286
currently lose_sum: 94.95970076322556
time_elpased: 2.254
batch start
#iterations: 287
currently lose_sum: 94.9461640715599
time_elpased: 2.31
batch start
#iterations: 288
currently lose_sum: 95.33138352632523
time_elpased: 2.301
batch start
#iterations: 289
currently lose_sum: 95.20694428682327
time_elpased: 2.239
batch start
#iterations: 290
currently lose_sum: 95.20157051086426
time_elpased: 2.223
batch start
#iterations: 291
currently lose_sum: 95.2488312125206
time_elpased: 2.751
batch start
#iterations: 292
currently lose_sum: 95.13629752397537
time_elpased: 2.904
batch start
#iterations: 293
currently lose_sum: 95.02619123458862
time_elpased: 2.727
batch start
#iterations: 294
currently lose_sum: 95.2852753996849
time_elpased: 2.683
batch start
#iterations: 295
currently lose_sum: 95.44499409198761
time_elpased: 2.735
batch start
#iterations: 296
currently lose_sum: 95.47636091709137
time_elpased: 2.366
batch start
#iterations: 297
currently lose_sum: 95.30880242586136
time_elpased: 2.059
batch start
#iterations: 298
currently lose_sum: 94.89589685201645
time_elpased: 1.872
batch start
#iterations: 299
currently lose_sum: 95.47632825374603
time_elpased: 1.907
start validation test
0.649432989691
0.641849148418
0.678777274599
0.659796908609
0.649384506775
61.407
batch start
#iterations: 300
currently lose_sum: 95.52666336297989
time_elpased: 1.839
batch start
#iterations: 301
currently lose_sum: 94.90658676624298
time_elpased: 2.479
batch start
#iterations: 302
currently lose_sum: 94.67481768131256
time_elpased: 2.555
batch start
#iterations: 303
currently lose_sum: 94.99563539028168
time_elpased: 2.604
batch start
#iterations: 304
currently lose_sum: 94.66131204366684
time_elpased: 2.776
batch start
#iterations: 305
currently lose_sum: 94.99887079000473
time_elpased: 2.607
batch start
#iterations: 306
currently lose_sum: 95.47749960422516
time_elpased: 2.601
batch start
#iterations: 307
currently lose_sum: 95.05463820695877
time_elpased: 2.753
batch start
#iterations: 308
currently lose_sum: 94.95638412237167
time_elpased: 2.626
batch start
#iterations: 309
currently lose_sum: 94.80405008792877
time_elpased: 2.479
batch start
#iterations: 310
currently lose_sum: 95.0203230381012
time_elpased: 2.635
batch start
#iterations: 311
currently lose_sum: 95.02703094482422
time_elpased: 2.679
batch start
#iterations: 312
currently lose_sum: 95.21414798498154
time_elpased: 2.938
batch start
#iterations: 313
currently lose_sum: 95.08630669116974
time_elpased: 2.568
batch start
#iterations: 314
currently lose_sum: 94.88537216186523
time_elpased: 2.397
batch start
#iterations: 315
currently lose_sum: 94.32586687803268
time_elpased: 2.951
batch start
#iterations: 316
currently lose_sum: 94.87400841712952
time_elpased: 2.55
batch start
#iterations: 317
currently lose_sum: 94.99019235372543
time_elpased: 2.472
batch start
#iterations: 318
currently lose_sum: 95.06023359298706
time_elpased: 2.619
batch start
#iterations: 319
currently lose_sum: 95.00698119401932
time_elpased: 2.928
start validation test
0.657525773196
0.629401853412
0.7689378345
0.692207912536
0.657341697093
61.217
batch start
#iterations: 320
currently lose_sum: 95.19812405109406
time_elpased: 2.683
batch start
#iterations: 321
currently lose_sum: 94.87011700868607
time_elpased: 2.831
batch start
#iterations: 322
currently lose_sum: 95.28111344575882
time_elpased: 2.812
batch start
#iterations: 323
currently lose_sum: 95.09812450408936
time_elpased: 2.593
batch start
#iterations: 324
currently lose_sum: 94.88654857873917
time_elpased: 2.81
batch start
#iterations: 325
currently lose_sum: 95.02828693389893
time_elpased: 2.503
batch start
#iterations: 326
currently lose_sum: 94.8503560423851
time_elpased: 2.82
batch start
#iterations: 327
currently lose_sum: 94.9514474272728
time_elpased: 2.77
batch start
#iterations: 328
currently lose_sum: 95.09406977891922
time_elpased: 2.574
batch start
#iterations: 329
currently lose_sum: 95.12038308382034
time_elpased: 2.423
batch start
#iterations: 330
currently lose_sum: 95.01299446821213
time_elpased: 2.763
batch start
#iterations: 331
currently lose_sum: 94.85002392530441
time_elpased: 2.666
batch start
#iterations: 332
currently lose_sum: 94.84811246395111
time_elpased: 2.466
batch start
#iterations: 333
currently lose_sum: 94.85324782133102
time_elpased: 2.629
batch start
#iterations: 334
currently lose_sum: 95.05794543027878
time_elpased: 2.641
batch start
#iterations: 335
currently lose_sum: 94.83341324329376
time_elpased: 2.653
batch start
#iterations: 336
currently lose_sum: 95.03878432512283
time_elpased: 2.508
batch start
#iterations: 337
currently lose_sum: 94.77472251653671
time_elpased: 2.842
batch start
#iterations: 338
currently lose_sum: 94.85592830181122
time_elpased: 2.835
batch start
#iterations: 339
currently lose_sum: 95.13949066400528
time_elpased: 2.719
start validation test
0.655154639175
0.642843655589
0.700802799506
0.670573173134
0.655079218836
61.270
batch start
#iterations: 340
currently lose_sum: 95.28625476360321
time_elpased: 2.841
batch start
#iterations: 341
currently lose_sum: 94.87042963504791
time_elpased: 2.677
batch start
#iterations: 342
currently lose_sum: 95.44514983892441
time_elpased: 2.542
batch start
#iterations: 343
currently lose_sum: 95.37379056215286
time_elpased: 2.446
batch start
#iterations: 344
currently lose_sum: 95.02190399169922
time_elpased: 2.722
batch start
#iterations: 345
currently lose_sum: 94.83903008699417
time_elpased: 2.907
batch start
#iterations: 346
currently lose_sum: 94.75302952528
time_elpased: 2.76
batch start
#iterations: 347
currently lose_sum: 94.95681238174438
time_elpased: 2.678
batch start
#iterations: 348
currently lose_sum: 94.96478354930878
time_elpased: 2.586
batch start
#iterations: 349
currently lose_sum: 94.77815675735474
time_elpased: 2.343
batch start
#iterations: 350
currently lose_sum: 95.29252660274506
time_elpased: 2.057
batch start
#iterations: 351
currently lose_sum: 94.9703232049942
time_elpased: 2.225
batch start
#iterations: 352
currently lose_sum: 95.23491644859314
time_elpased: 2.771
batch start
#iterations: 353
currently lose_sum: 95.17028570175171
time_elpased: 2.82
batch start
#iterations: 354
currently lose_sum: 94.81695353984833
time_elpased: 2.547
batch start
#iterations: 355
currently lose_sum: 94.70318633317947
time_elpased: 2.546
batch start
#iterations: 356
currently lose_sum: 95.10513091087341
time_elpased: 2.583
batch start
#iterations: 357
currently lose_sum: 94.99949985742569
time_elpased: 2.836
batch start
#iterations: 358
currently lose_sum: 94.8814867734909
time_elpased: 2.865
batch start
#iterations: 359
currently lose_sum: 94.66718345880508
time_elpased: 2.905
start validation test
0.660721649485
0.629077429984
0.786023054755
0.698846998536
0.660514625271
60.906
batch start
#iterations: 360
currently lose_sum: 94.80195158720016
time_elpased: 2.472
batch start
#iterations: 361
currently lose_sum: 94.98574155569077
time_elpased: 2.654
batch start
#iterations: 362
currently lose_sum: 95.12985014915466
time_elpased: 2.752
batch start
#iterations: 363
currently lose_sum: 94.96722799539566
time_elpased: 2.606
batch start
#iterations: 364
currently lose_sum: 94.74219357967377
time_elpased: 2.569
batch start
#iterations: 365
currently lose_sum: 95.11830973625183
time_elpased: 2.222
batch start
#iterations: 366
currently lose_sum: 95.4665333032608
time_elpased: 2.374
batch start
#iterations: 367
currently lose_sum: 94.77825385332108
time_elpased: 2.345
batch start
#iterations: 368
currently lose_sum: 95.14847111701965
time_elpased: 2.379
batch start
#iterations: 369
currently lose_sum: 94.91438949108124
time_elpased: 2.622
batch start
#iterations: 370
currently lose_sum: 94.74576359987259
time_elpased: 2.602
batch start
#iterations: 371
currently lose_sum: 94.69500118494034
time_elpased: 2.778
batch start
#iterations: 372
currently lose_sum: 95.16236984729767
time_elpased: 2.772
batch start
#iterations: 373
currently lose_sum: 94.7100522518158
time_elpased: 2.819
batch start
#iterations: 374
currently lose_sum: 95.01215815544128
time_elpased: 2.527
batch start
#iterations: 375
currently lose_sum: 94.94516807794571
time_elpased: 2.689
batch start
#iterations: 376
currently lose_sum: 94.5320885181427
time_elpased: 2.775
batch start
#iterations: 377
currently lose_sum: 95.1597483754158
time_elpased: 2.586
batch start
#iterations: 378
currently lose_sum: 94.93506562709808
time_elpased: 2.535
batch start
#iterations: 379
currently lose_sum: 95.14826267957687
time_elpased: 2.453
start validation test
0.66587628866
0.632911392405
0.792507204611
0.703774792067
0.665667067816
60.892
batch start
#iterations: 380
currently lose_sum: 94.5845792889595
time_elpased: 2.558
batch start
#iterations: 381
currently lose_sum: 94.52897757291794
time_elpased: 2.978
batch start
#iterations: 382
currently lose_sum: 95.17624545097351
time_elpased: 2.651
batch start
#iterations: 383
currently lose_sum: 94.3741163611412
time_elpased: 2.807
batch start
#iterations: 384
currently lose_sum: 94.99125957489014
time_elpased: 2.673
batch start
#iterations: 385
currently lose_sum: 95.17873555421829
time_elpased: 2.626
batch start
#iterations: 386
currently lose_sum: 94.98488825559616
time_elpased: 2.818
batch start
#iterations: 387
currently lose_sum: 94.70881015062332
time_elpased: 2.643
batch start
#iterations: 388
currently lose_sum: 95.20189905166626
time_elpased: 2.562
batch start
#iterations: 389
currently lose_sum: 94.70696353912354
time_elpased: 2.686
batch start
#iterations: 390
currently lose_sum: 94.88944345712662
time_elpased: 2.768
batch start
#iterations: 391
currently lose_sum: 94.81865453720093
time_elpased: 2.817
batch start
#iterations: 392
currently lose_sum: 94.92979001998901
time_elpased: 2.734
batch start
#iterations: 393
currently lose_sum: 94.58454650640488
time_elpased: 2.885
batch start
#iterations: 394
currently lose_sum: 94.65789884328842
time_elpased: 2.714
batch start
#iterations: 395
currently lose_sum: 94.71447998285294
time_elpased: 2.596
batch start
#iterations: 396
currently lose_sum: 94.3641402721405
time_elpased: 2.343
batch start
#iterations: 397
currently lose_sum: 94.93163108825684
time_elpased: 2.445
batch start
#iterations: 398
currently lose_sum: 94.71357536315918
time_elpased: 2.623
batch start
#iterations: 399
currently lose_sum: 94.67673426866531
time_elpased: 2.545
start validation test
0.659587628866
0.628914664457
0.78128859613
0.696869549252
0.659386553331
60.976
acc: 0.667
pre: 0.634
rec: 0.791
F1: 0.704
auc: 0.707
