start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.43090510368347
time_elpased: 3.034
batch start
#iterations: 1
currently lose_sum: 100.48466449975967
time_elpased: 2.728
batch start
#iterations: 2
currently lose_sum: 100.33899104595184
time_elpased: 3.059
batch start
#iterations: 3
currently lose_sum: 100.26174610853195
time_elpased: 2.817
batch start
#iterations: 4
currently lose_sum: 100.22526127099991
time_elpased: 3.093
batch start
#iterations: 5
currently lose_sum: 100.12883603572845
time_elpased: 3.016
batch start
#iterations: 6
currently lose_sum: 100.0231945514679
time_elpased: 3.04
batch start
#iterations: 7
currently lose_sum: 100.05921828746796
time_elpased: 3.077
batch start
#iterations: 8
currently lose_sum: 99.8240196108818
time_elpased: 2.959
batch start
#iterations: 9
currently lose_sum: 99.92129743099213
time_elpased: 2.848
batch start
#iterations: 10
currently lose_sum: 99.91157102584839
time_elpased: 3.09
batch start
#iterations: 11
currently lose_sum: 99.66097629070282
time_elpased: 3.055
batch start
#iterations: 12
currently lose_sum: 99.469935297966
time_elpased: 2.941
batch start
#iterations: 13
currently lose_sum: 99.43490141630173
time_elpased: 2.706
batch start
#iterations: 14
currently lose_sum: 99.23666471242905
time_elpased: 2.749
batch start
#iterations: 15
currently lose_sum: 99.21029311418533
time_elpased: 2.413
batch start
#iterations: 16
currently lose_sum: 98.82207351922989
time_elpased: 2.882
batch start
#iterations: 17
currently lose_sum: 98.86293095350266
time_elpased: 3.021
batch start
#iterations: 18
currently lose_sum: 98.81912463903427
time_elpased: 2.982
batch start
#iterations: 19
currently lose_sum: 98.52884668111801
time_elpased: 2.841
start validation test
0.62793814433
0.624415015434
0.64536379541
0.63471659919
0.627907550912
64.388
batch start
#iterations: 20
currently lose_sum: 98.39424121379852
time_elpased: 2.541
batch start
#iterations: 21
currently lose_sum: 98.3219895362854
time_elpased: 2.831
batch start
#iterations: 22
currently lose_sum: 98.28983676433563
time_elpased: 2.814
batch start
#iterations: 23
currently lose_sum: 98.36689376831055
time_elpased: 3.241
batch start
#iterations: 24
currently lose_sum: 97.88454061746597
time_elpased: 2.66
batch start
#iterations: 25
currently lose_sum: 98.25352627038956
time_elpased: 3.487
batch start
#iterations: 26
currently lose_sum: 97.97917032241821
time_elpased: 3.061
batch start
#iterations: 27
currently lose_sum: 98.07697075605392
time_elpased: 3.036
batch start
#iterations: 28
currently lose_sum: 97.85398906469345
time_elpased: 3.082
batch start
#iterations: 29
currently lose_sum: 97.80783212184906
time_elpased: 3.177
batch start
#iterations: 30
currently lose_sum: 97.9031485915184
time_elpased: 3.117
batch start
#iterations: 31
currently lose_sum: 97.68550217151642
time_elpased: 2.844
batch start
#iterations: 32
currently lose_sum: 97.52767360210419
time_elpased: 2.883
batch start
#iterations: 33
currently lose_sum: 97.90483689308167
time_elpased: 2.572
batch start
#iterations: 34
currently lose_sum: 97.3833270072937
time_elpased: 2.609
batch start
#iterations: 35
currently lose_sum: 97.52708774805069
time_elpased: 2.85
batch start
#iterations: 36
currently lose_sum: 97.31444066762924
time_elpased: 3.072
batch start
#iterations: 37
currently lose_sum: 97.62669062614441
time_elpased: 3.326
batch start
#iterations: 38
currently lose_sum: 97.29906058311462
time_elpased: 3.081
batch start
#iterations: 39
currently lose_sum: 97.42077249288559
time_elpased: 3.158
start validation test
0.643402061856
0.633501860155
0.683441391376
0.657524752475
0.643331766637
62.913
batch start
#iterations: 40
currently lose_sum: 97.18504559993744
time_elpased: 2.971
batch start
#iterations: 41
currently lose_sum: 97.31538558006287
time_elpased: 3.106
batch start
#iterations: 42
currently lose_sum: 97.09131318330765
time_elpased: 3.306
batch start
#iterations: 43
currently lose_sum: 97.48388302326202
time_elpased: 2.93
batch start
#iterations: 44
currently lose_sum: 97.40805023908615
time_elpased: 3.113
batch start
#iterations: 45
currently lose_sum: 97.43303644657135
time_elpased: 2.884
batch start
#iterations: 46
currently lose_sum: 97.39165031909943
time_elpased: 2.961
batch start
#iterations: 47
currently lose_sum: 97.18087500333786
time_elpased: 3.031
batch start
#iterations: 48
currently lose_sum: 97.19934898614883
time_elpased: 2.994
batch start
#iterations: 49
currently lose_sum: 96.90747964382172
time_elpased: 2.867
batch start
#iterations: 50
currently lose_sum: 97.10559970140457
time_elpased: 2.782
batch start
#iterations: 51
currently lose_sum: 97.2395179271698
time_elpased: 3.02
batch start
#iterations: 52
currently lose_sum: 97.10540413856506
time_elpased: 3.232
batch start
#iterations: 53
currently lose_sum: 96.99033719301224
time_elpased: 2.806
batch start
#iterations: 54
currently lose_sum: 97.17623621225357
time_elpased: 2.659
batch start
#iterations: 55
currently lose_sum: 97.00472581386566
time_elpased: 2.638
batch start
#iterations: 56
currently lose_sum: 97.05159294605255
time_elpased: 2.644
batch start
#iterations: 57
currently lose_sum: 97.19774681329727
time_elpased: 2.677
batch start
#iterations: 58
currently lose_sum: 97.05592447519302
time_elpased: 2.834
batch start
#iterations: 59
currently lose_sum: 96.98777478933334
time_elpased: 3.229
start validation test
0.636701030928
0.644755396464
0.61171143357
0.627798901563
0.63674490402
62.569
batch start
#iterations: 60
currently lose_sum: 97.06644541025162
time_elpased: 3.055
batch start
#iterations: 61
currently lose_sum: 97.05406564474106
time_elpased: 3.224
batch start
#iterations: 62
currently lose_sum: 97.18231159448624
time_elpased: 3.438
batch start
#iterations: 63
currently lose_sum: 96.7882205247879
time_elpased: 3.08
batch start
#iterations: 64
currently lose_sum: 97.06403464078903
time_elpased: 2.659
batch start
#iterations: 65
currently lose_sum: 96.9900730252266
time_elpased: 3.233
batch start
#iterations: 66
currently lose_sum: 96.83389019966125
time_elpased: 3.184
batch start
#iterations: 67
currently lose_sum: 96.67868441343307
time_elpased: 2.928
batch start
#iterations: 68
currently lose_sum: 97.02439028024673
time_elpased: 3.317
batch start
#iterations: 69
currently lose_sum: 96.842609167099
time_elpased: 3.196
batch start
#iterations: 70
currently lose_sum: 96.93333381414413
time_elpased: 3.114
batch start
#iterations: 71
currently lose_sum: 96.64849799871445
time_elpased: 2.838
batch start
#iterations: 72
currently lose_sum: 96.65707230567932
time_elpased: 3.201
batch start
#iterations: 73
currently lose_sum: 96.7099911570549
time_elpased: 3.38
batch start
#iterations: 74
currently lose_sum: 97.06948059797287
time_elpased: 2.86
batch start
#iterations: 75
currently lose_sum: 96.74731981754303
time_elpased: 3.129
batch start
#iterations: 76
currently lose_sum: 96.6191241145134
time_elpased: 3.078
batch start
#iterations: 77
currently lose_sum: 97.0671820640564
time_elpased: 2.717
batch start
#iterations: 78
currently lose_sum: 96.85984718799591
time_elpased: 3.034
batch start
#iterations: 79
currently lose_sum: 96.85823476314545
time_elpased: 2.682
start validation test
0.651958762887
0.631789492399
0.731398579809
0.677954783936
0.651819294035
61.922
batch start
#iterations: 80
currently lose_sum: 96.95534783601761
time_elpased: 3.085
batch start
#iterations: 81
currently lose_sum: 96.64125287532806
time_elpased: 3.441
batch start
#iterations: 82
currently lose_sum: 97.1712651848793
time_elpased: 3.389
batch start
#iterations: 83
currently lose_sum: 96.55835074186325
time_elpased: 3.157
batch start
#iterations: 84
currently lose_sum: 96.55405682325363
time_elpased: 3.216
batch start
#iterations: 85
currently lose_sum: 96.58423560857773
time_elpased: 3.376
batch start
#iterations: 86
currently lose_sum: 96.74901258945465
time_elpased: 3.339
batch start
#iterations: 87
currently lose_sum: 96.89548379182816
time_elpased: 3.187
batch start
#iterations: 88
currently lose_sum: 96.76427632570267
time_elpased: 3.329
batch start
#iterations: 89
currently lose_sum: 96.54609102010727
time_elpased: 3.278
batch start
#iterations: 90
currently lose_sum: 96.49936628341675
time_elpased: 3.122
batch start
#iterations: 91
currently lose_sum: 96.86443740129471
time_elpased: 3.205
batch start
#iterations: 92
currently lose_sum: 96.71741145849228
time_elpased: 3.077
batch start
#iterations: 93
currently lose_sum: 96.4112805724144
time_elpased: 3.022
batch start
#iterations: 94
currently lose_sum: 96.76437568664551
time_elpased: 3.182
batch start
#iterations: 95
currently lose_sum: 96.24476826190948
time_elpased: 3.084
batch start
#iterations: 96
currently lose_sum: 96.38137131929398
time_elpased: 3.032
batch start
#iterations: 97
currently lose_sum: 96.41704672574997
time_elpased: 3.08
batch start
#iterations: 98
currently lose_sum: 96.82386040687561
time_elpased: 3.23
batch start
#iterations: 99
currently lose_sum: 96.51545917987823
time_elpased: 3.242
start validation test
0.653865979381
0.626966672306
0.762786868375
0.688239936859
0.653674751961
61.762
batch start
#iterations: 100
currently lose_sum: 96.82833087444305
time_elpased: 2.869
batch start
#iterations: 101
currently lose_sum: 96.29426658153534
time_elpased: 3.077
batch start
#iterations: 102
currently lose_sum: 96.80639570951462
time_elpased: 3.27
batch start
#iterations: 103
currently lose_sum: 96.32097226381302
time_elpased: 3.307
batch start
#iterations: 104
currently lose_sum: 96.54696077108383
time_elpased: 2.846
batch start
#iterations: 105
currently lose_sum: 96.35411787033081
time_elpased: 2.757
batch start
#iterations: 106
currently lose_sum: 96.65977549552917
time_elpased: 3.257
batch start
#iterations: 107
currently lose_sum: 96.82198929786682
time_elpased: 3.105
batch start
#iterations: 108
currently lose_sum: 96.21104735136032
time_elpased: 3.144
batch start
#iterations: 109
currently lose_sum: 96.46993577480316
time_elpased: 3.208
batch start
#iterations: 110
currently lose_sum: 96.41166186332703
time_elpased: 2.826
batch start
#iterations: 111
currently lose_sum: 95.98563158512115
time_elpased: 2.918
batch start
#iterations: 112
currently lose_sum: 96.2609304189682
time_elpased: 3.078
batch start
#iterations: 113
currently lose_sum: 96.56427085399628
time_elpased: 3.193
batch start
#iterations: 114
currently lose_sum: 95.95099610090256
time_elpased: 2.922
batch start
#iterations: 115
currently lose_sum: 96.27843934297562
time_elpased: 3.085
batch start
#iterations: 116
currently lose_sum: 95.97664779424667
time_elpased: 3.24
batch start
#iterations: 117
currently lose_sum: 96.34389209747314
time_elpased: 2.898
batch start
#iterations: 118
currently lose_sum: 95.94886720180511
time_elpased: 2.97
batch start
#iterations: 119
currently lose_sum: 96.10036140680313
time_elpased: 2.892
start validation test
0.654587628866
0.620389589654
0.799732427704
0.698736681203
0.654332804785
61.651
batch start
#iterations: 120
currently lose_sum: 95.98962450027466
time_elpased: 3.036
batch start
#iterations: 121
currently lose_sum: 96.14143604040146
time_elpased: 3.039
batch start
#iterations: 122
currently lose_sum: 96.44221442937851
time_elpased: 3.103
batch start
#iterations: 123
currently lose_sum: 96.04608911275864
time_elpased: 3.018
batch start
#iterations: 124
currently lose_sum: 96.5869910120964
time_elpased: 3.086
batch start
#iterations: 125
currently lose_sum: 96.57131689786911
time_elpased: 3.349
batch start
#iterations: 126
currently lose_sum: 96.2244724035263
time_elpased: 3.205
batch start
#iterations: 127
currently lose_sum: 96.27713513374329
time_elpased: 3.054
batch start
#iterations: 128
currently lose_sum: 96.0436127781868
time_elpased: 3.144
batch start
#iterations: 129
currently lose_sum: 96.69056421518326
time_elpased: 2.874
batch start
#iterations: 130
currently lose_sum: 96.11092185974121
time_elpased: 3.389
batch start
#iterations: 131
currently lose_sum: 96.09925639629364
time_elpased: 3.334
batch start
#iterations: 132
currently lose_sum: 96.21216815710068
time_elpased: 3.171
batch start
#iterations: 133
currently lose_sum: 96.3809609413147
time_elpased: 3.164
batch start
#iterations: 134
currently lose_sum: 96.23398417234421
time_elpased: 3.047
batch start
#iterations: 135
currently lose_sum: 96.13768112659454
time_elpased: 3.022
batch start
#iterations: 136
currently lose_sum: 95.98359680175781
time_elpased: 3.175
batch start
#iterations: 137
currently lose_sum: 95.86926299333572
time_elpased: 2.932
batch start
#iterations: 138
currently lose_sum: 96.50245499610901
time_elpased: 3.059
batch start
#iterations: 139
currently lose_sum: 95.81862998008728
time_elpased: 3.068
start validation test
0.655927835052
0.626497005988
0.77523927138
0.692976404029
0.655718365423
61.322
batch start
#iterations: 140
currently lose_sum: 95.90517848730087
time_elpased: 3.158
batch start
#iterations: 141
currently lose_sum: 96.55923813581467
time_elpased: 2.873
batch start
#iterations: 142
currently lose_sum: 96.13731920719147
time_elpased: 3.197
batch start
#iterations: 143
currently lose_sum: 96.0319727063179
time_elpased: 3.225
batch start
#iterations: 144
currently lose_sum: 96.13587439060211
time_elpased: 3.05
batch start
#iterations: 145
currently lose_sum: 95.81240624189377
time_elpased: 2.912
batch start
#iterations: 146
currently lose_sum: 95.90710580348969
time_elpased: 2.927
batch start
#iterations: 147
currently lose_sum: 95.8767482638359
time_elpased: 3.137
batch start
#iterations: 148
currently lose_sum: 95.99628973007202
time_elpased: 3.054
batch start
#iterations: 149
currently lose_sum: 96.01370632648468
time_elpased: 2.853
batch start
#iterations: 150
currently lose_sum: 96.38013315200806
time_elpased: 3.105
batch start
#iterations: 151
currently lose_sum: 95.86419695615768
time_elpased: 3.018
batch start
#iterations: 152
currently lose_sum: 95.8826305270195
time_elpased: 3.217
batch start
#iterations: 153
currently lose_sum: 95.99112164974213
time_elpased: 3.176
batch start
#iterations: 154
currently lose_sum: 95.93659782409668
time_elpased: 3.151
batch start
#iterations: 155
currently lose_sum: 95.93806231021881
time_elpased: 2.772
batch start
#iterations: 156
currently lose_sum: 95.73225849866867
time_elpased: 3.213
batch start
#iterations: 157
currently lose_sum: 95.5467563867569
time_elpased: 3.256
batch start
#iterations: 158
currently lose_sum: 95.83560746908188
time_elpased: 3.056
batch start
#iterations: 159
currently lose_sum: 96.1807701587677
time_elpased: 2.997
start validation test
0.655618556701
0.63832695462
0.720901512813
0.677105988111
0.655503942402
61.589
batch start
#iterations: 160
currently lose_sum: 95.613396525383
time_elpased: 3.237
batch start
#iterations: 161
currently lose_sum: 96.1316705942154
time_elpased: 3.156
batch start
#iterations: 162
currently lose_sum: 95.46011161804199
time_elpased: 3.26
batch start
#iterations: 163
currently lose_sum: 96.21044129133224
time_elpased: 3.025
batch start
#iterations: 164
currently lose_sum: 95.77617090940475
time_elpased: 2.97
batch start
#iterations: 165
currently lose_sum: 95.87979704141617
time_elpased: 3.237
batch start
#iterations: 166
currently lose_sum: 95.82514250278473
time_elpased: 3.375
batch start
#iterations: 167
currently lose_sum: 95.69634073972702
time_elpased: 3.176
batch start
#iterations: 168
currently lose_sum: 95.73245692253113
time_elpased: 3.062
batch start
#iterations: 169
currently lose_sum: 95.95277416706085
time_elpased: 3.304
batch start
#iterations: 170
currently lose_sum: 96.06034117937088
time_elpased: 3.032
batch start
#iterations: 171
currently lose_sum: 96.1378293633461
time_elpased: 2.923
batch start
#iterations: 172
currently lose_sum: 95.88386446237564
time_elpased: 3.029
batch start
#iterations: 173
currently lose_sum: 95.77498948574066
time_elpased: 3.174
batch start
#iterations: 174
currently lose_sum: 95.53983479738235
time_elpased: 3.088
batch start
#iterations: 175
currently lose_sum: 95.57328236103058
time_elpased: 3.041
batch start
#iterations: 176
currently lose_sum: 95.4846767783165
time_elpased: 3.235
batch start
#iterations: 177
currently lose_sum: 95.34967559576035
time_elpased: 3.169
batch start
#iterations: 178
currently lose_sum: 96.13309466838837
time_elpased: 3.035
batch start
#iterations: 179
currently lose_sum: 95.81343966722488
time_elpased: 3.124
start validation test
0.654639175258
0.628876548484
0.757538334877
0.687237419475
0.654458519912
61.168
batch start
#iterations: 180
currently lose_sum: 95.64497363567352
time_elpased: 2.924
batch start
#iterations: 181
currently lose_sum: 95.53879487514496
time_elpased: 3.155
batch start
#iterations: 182
currently lose_sum: 95.64286363124847
time_elpased: 3.326
batch start
#iterations: 183
currently lose_sum: 95.38226747512817
time_elpased: 3.288
batch start
#iterations: 184
currently lose_sum: 96.36503106355667
time_elpased: 2.843
batch start
#iterations: 185
currently lose_sum: 95.98464465141296
time_elpased: 3.121
batch start
#iterations: 186
currently lose_sum: 96.02513593435287
time_elpased: 2.924
batch start
#iterations: 187
currently lose_sum: 95.66364979743958
time_elpased: 3.213
batch start
#iterations: 188
currently lose_sum: 95.73541736602783
time_elpased: 3.424
batch start
#iterations: 189
currently lose_sum: 95.83159512281418
time_elpased: 3.044
batch start
#iterations: 190
currently lose_sum: 95.58332300186157
time_elpased: 3.046
batch start
#iterations: 191
currently lose_sum: 95.40250194072723
time_elpased: 3.087
batch start
#iterations: 192
currently lose_sum: 95.61153793334961
time_elpased: 3.184
batch start
#iterations: 193
currently lose_sum: 95.62804818153381
time_elpased: 2.718
batch start
#iterations: 194
currently lose_sum: 95.71579611301422
time_elpased: 3.091
batch start
#iterations: 195
currently lose_sum: 95.99892491102219
time_elpased: 3.165
batch start
#iterations: 196
currently lose_sum: 95.52955877780914
time_elpased: 2.999
batch start
#iterations: 197
currently lose_sum: 95.49354583024979
time_elpased: 3.313
batch start
#iterations: 198
currently lose_sum: 95.62915676832199
time_elpased: 3.594
batch start
#iterations: 199
currently lose_sum: 95.60521996021271
time_elpased: 3.039
start validation test
0.65175257732
0.641363506159
0.691262735412
0.665378900446
0.651683211143
61.083
batch start
#iterations: 200
currently lose_sum: 95.6700833439827
time_elpased: 3.141
batch start
#iterations: 201
currently lose_sum: 95.4992139339447
time_elpased: 2.723
batch start
#iterations: 202
currently lose_sum: 95.4996063709259
time_elpased: 3.275
batch start
#iterations: 203
currently lose_sum: 95.54790097475052
time_elpased: 2.988
batch start
#iterations: 204
currently lose_sum: 95.52722495794296
time_elpased: 2.962
batch start
#iterations: 205
currently lose_sum: 95.7403547167778
time_elpased: 2.942
batch start
#iterations: 206
currently lose_sum: 95.49928349256516
time_elpased: 3.602
batch start
#iterations: 207
currently lose_sum: 95.66771912574768
time_elpased: 3.006
batch start
#iterations: 208
currently lose_sum: 95.34299808740616
time_elpased: 3.47
batch start
#iterations: 209
currently lose_sum: 95.47294867038727
time_elpased: 3.242
batch start
#iterations: 210
currently lose_sum: 95.715651512146
time_elpased: 3.063
batch start
#iterations: 211
currently lose_sum: 95.68074798583984
time_elpased: 2.735
batch start
#iterations: 212
currently lose_sum: 95.91498959064484
time_elpased: 3.417
batch start
#iterations: 213
currently lose_sum: 95.61335462331772
time_elpased: 3.051
batch start
#iterations: 214
currently lose_sum: 95.51796299219131
time_elpased: 3.143
batch start
#iterations: 215
currently lose_sum: 95.4059379696846
time_elpased: 3.168
batch start
#iterations: 216
currently lose_sum: 95.16480672359467
time_elpased: 2.673
batch start
#iterations: 217
currently lose_sum: 95.62030935287476
time_elpased: 3.12
batch start
#iterations: 218
currently lose_sum: 95.35121309757233
time_elpased: 3.266
batch start
#iterations: 219
currently lose_sum: 95.71648705005646
time_elpased: 3.368
start validation test
0.657628865979
0.642903615578
0.711845219718
0.675620238328
0.657533680808
61.298
batch start
#iterations: 220
currently lose_sum: 95.53686970472336
time_elpased: 3.445
batch start
#iterations: 221
currently lose_sum: 95.36263805627823
time_elpased: 2.78
batch start
#iterations: 222
currently lose_sum: 95.23214101791382
time_elpased: 2.918
batch start
#iterations: 223
currently lose_sum: 95.46190625429153
time_elpased: 3.051
batch start
#iterations: 224
currently lose_sum: 95.26187515258789
time_elpased: 3.035
batch start
#iterations: 225
currently lose_sum: 95.8426725268364
time_elpased: 2.843
batch start
#iterations: 226
currently lose_sum: 95.47080832719803
time_elpased: 3.206
batch start
#iterations: 227
currently lose_sum: 95.17312800884247
time_elpased: 3.105
batch start
#iterations: 228
currently lose_sum: 95.77676862478256
time_elpased: 2.86
batch start
#iterations: 229
currently lose_sum: 95.57346159219742
time_elpased: 3.116
batch start
#iterations: 230
currently lose_sum: 95.2094178199768
time_elpased: 3.153
batch start
#iterations: 231
currently lose_sum: 95.23893266916275
time_elpased: 3.452
batch start
#iterations: 232
currently lose_sum: 95.37716311216354
time_elpased: 3.319
batch start
#iterations: 233
currently lose_sum: 95.59542590379715
time_elpased: 3.223
batch start
#iterations: 234
currently lose_sum: 95.27847146987915
time_elpased: 3.239
batch start
#iterations: 235
currently lose_sum: 95.57413506507874
time_elpased: 3.172
batch start
#iterations: 236
currently lose_sum: 95.51301366090775
time_elpased: 2.833
batch start
#iterations: 237
currently lose_sum: 95.4004961848259
time_elpased: 3.077
batch start
#iterations: 238
currently lose_sum: 95.0389387011528
time_elpased: 3.151
batch start
#iterations: 239
currently lose_sum: 95.77888369560242
time_elpased: 2.895
start validation test
0.636701030928
0.654223968566
0.582587218277
0.616330974415
0.636796036072
61.670
batch start
#iterations: 240
currently lose_sum: 95.40464550256729
time_elpased: 2.749
batch start
#iterations: 241
currently lose_sum: 95.53843158483505
time_elpased: 3.271
batch start
#iterations: 242
currently lose_sum: 95.43015748262405
time_elpased: 2.857
batch start
#iterations: 243
currently lose_sum: 95.07663971185684
time_elpased: 2.712
batch start
#iterations: 244
currently lose_sum: 95.59262877702713
time_elpased: 2.899
batch start
#iterations: 245
currently lose_sum: 95.27980250120163
time_elpased: 3.243
batch start
#iterations: 246
currently lose_sum: 95.31987679004669
time_elpased: 3.049
batch start
#iterations: 247
currently lose_sum: 95.19626539945602
time_elpased: 3.111
batch start
#iterations: 248
currently lose_sum: 95.232676923275
time_elpased: 2.935
batch start
#iterations: 249
currently lose_sum: 95.44965016841888
time_elpased: 3.041
batch start
#iterations: 250
currently lose_sum: 95.29531252384186
time_elpased: 2.95
batch start
#iterations: 251
currently lose_sum: 95.23398840427399
time_elpased: 2.685
batch start
#iterations: 252
currently lose_sum: 95.23578828573227
time_elpased: 3.126
batch start
#iterations: 253
currently lose_sum: 95.31864356994629
time_elpased: 3.287
batch start
#iterations: 254
currently lose_sum: 95.12783968448639
time_elpased: 3.169
batch start
#iterations: 255
currently lose_sum: 95.32643955945969
time_elpased: 3.158
batch start
#iterations: 256
currently lose_sum: 95.39525723457336
time_elpased: 3.017
batch start
#iterations: 257
currently lose_sum: 94.9912269115448
time_elpased: 2.836
batch start
#iterations: 258
currently lose_sum: 95.1465100646019
time_elpased: 2.834
batch start
#iterations: 259
currently lose_sum: 95.05821031332016
time_elpased: 3.248
start validation test
0.660721649485
0.621880102636
0.823093547391
0.708477278767
0.660436580574
60.721
batch start
#iterations: 260
currently lose_sum: 95.34856051206589
time_elpased: 3.05
batch start
#iterations: 261
currently lose_sum: 95.23453277349472
time_elpased: 3.263
batch start
#iterations: 262
currently lose_sum: 95.43213325738907
time_elpased: 3.214
batch start
#iterations: 263
currently lose_sum: 95.67380410432816
time_elpased: 3.087
batch start
#iterations: 264
currently lose_sum: 95.12473458051682
time_elpased: 3.093
batch start
#iterations: 265
currently lose_sum: 95.42218065261841
time_elpased: 3.155
batch start
#iterations: 266
currently lose_sum: 95.02573478221893
time_elpased: 2.858
batch start
#iterations: 267
currently lose_sum: 95.30965995788574
time_elpased: 2.999
batch start
#iterations: 268
currently lose_sum: 95.31938397884369
time_elpased: 3.065
batch start
#iterations: 269
currently lose_sum: 95.49362421035767
time_elpased: 2.873
batch start
#iterations: 270
currently lose_sum: 95.07647728919983
time_elpased: 3.454
batch start
#iterations: 271
currently lose_sum: 95.14630419015884
time_elpased: 3.323
batch start
#iterations: 272
currently lose_sum: 95.08548903465271
time_elpased: 3.024
batch start
#iterations: 273
currently lose_sum: 95.11952584981918
time_elpased: 2.54
batch start
#iterations: 274
currently lose_sum: 95.56482535600662
time_elpased: 2.576
batch start
#iterations: 275
currently lose_sum: 95.45408922433853
time_elpased: 2.791
batch start
#iterations: 276
currently lose_sum: 95.22689193487167
time_elpased: 3.026
batch start
#iterations: 277
currently lose_sum: 95.13795220851898
time_elpased: 2.693
batch start
#iterations: 278
currently lose_sum: 94.76374739408493
time_elpased: 3.101
batch start
#iterations: 279
currently lose_sum: 94.79044783115387
time_elpased: 3.395
start validation test
0.659793814433
0.642134062927
0.724606359988
0.680881926313
0.659680026013
60.914
batch start
#iterations: 280
currently lose_sum: 95.52682703733444
time_elpased: 3.568
batch start
#iterations: 281
currently lose_sum: 95.19505375623703
time_elpased: 3.395
batch start
#iterations: 282
currently lose_sum: 95.09122145175934
time_elpased: 3.061
batch start
#iterations: 283
currently lose_sum: 95.41547286510468
time_elpased: 3.507
batch start
#iterations: 284
currently lose_sum: 95.30844694375992
time_elpased: 3.046
batch start
#iterations: 285
currently lose_sum: 95.08031171560287
time_elpased: 2.878
batch start
#iterations: 286
currently lose_sum: 95.21078252792358
time_elpased: 2.894
batch start
#iterations: 287
currently lose_sum: 95.1854659318924
time_elpased: 2.805
batch start
#iterations: 288
currently lose_sum: 95.00019943714142
time_elpased: 3.308
batch start
#iterations: 289
currently lose_sum: 94.99315637350082
time_elpased: 3.416
batch start
#iterations: 290
currently lose_sum: 94.8556159734726
time_elpased: 3.176
batch start
#iterations: 291
currently lose_sum: 95.2491637468338
time_elpased: 2.911
batch start
#iterations: 292
currently lose_sum: 95.62308919429779
time_elpased: 2.74
batch start
#iterations: 293
currently lose_sum: 95.13840526342392
time_elpased: 3.044
batch start
#iterations: 294
currently lose_sum: 95.25701755285263
time_elpased: 2.913
batch start
#iterations: 295
currently lose_sum: 95.26333516836166
time_elpased: 3.275
batch start
#iterations: 296
currently lose_sum: 95.19313097000122
time_elpased: 2.965
batch start
#iterations: 297
currently lose_sum: 95.20611649751663
time_elpased: 2.774
batch start
#iterations: 298
currently lose_sum: 95.36237353086472
time_elpased: 3.083
batch start
#iterations: 299
currently lose_sum: 95.25772029161453
time_elpased: 2.896
start validation test
0.659690721649
0.619284674887
0.832149840486
0.710108017915
0.659387943066
60.800
batch start
#iterations: 300
currently lose_sum: 94.8495067358017
time_elpased: 3.073
batch start
#iterations: 301
currently lose_sum: 95.50979340076447
time_elpased: 2.915
batch start
#iterations: 302
currently lose_sum: 95.13445872068405
time_elpased: 3.274
batch start
#iterations: 303
currently lose_sum: 95.29530137777328
time_elpased: 3.134
batch start
#iterations: 304
currently lose_sum: 94.83024972677231
time_elpased: 3.172
batch start
#iterations: 305
currently lose_sum: 95.00540387630463
time_elpased: 3.204
batch start
#iterations: 306
currently lose_sum: 94.74572968482971
time_elpased: 3.252
batch start
#iterations: 307
currently lose_sum: 95.30762982368469
time_elpased: 3.108
batch start
#iterations: 308
currently lose_sum: 95.18897646665573
time_elpased: 3.196
batch start
#iterations: 309
currently lose_sum: 95.05284237861633
time_elpased: 2.993
batch start
#iterations: 310
currently lose_sum: 95.54092514514923
time_elpased: 3.163
batch start
#iterations: 311
currently lose_sum: 94.89427274465561
time_elpased: 3.205
batch start
#iterations: 312
currently lose_sum: 94.86709326505661
time_elpased: 2.994
batch start
#iterations: 313
currently lose_sum: 94.79917711019516
time_elpased: 3.035
batch start
#iterations: 314
currently lose_sum: 94.98680084943771
time_elpased: 3.406
batch start
#iterations: 315
currently lose_sum: 94.75193256139755
time_elpased: 3.164
batch start
#iterations: 316
currently lose_sum: 95.03636527061462
time_elpased: 3.063
batch start
#iterations: 317
currently lose_sum: 94.90287882089615
time_elpased: 3.146
batch start
#iterations: 318
currently lose_sum: 95.34645038843155
time_elpased: 2.795
batch start
#iterations: 319
currently lose_sum: 95.42554551362991
time_elpased: 3.02
start validation test
0.660103092784
0.62393840781
0.808994545642
0.704516938519
0.659841690873
60.749
batch start
#iterations: 320
currently lose_sum: 94.7485221028328
time_elpased: 2.957
batch start
#iterations: 321
currently lose_sum: 95.36303871870041
time_elpased: 3.133
batch start
#iterations: 322
currently lose_sum: 95.03268057107925
time_elpased: 3.129
batch start
#iterations: 323
currently lose_sum: 95.00826007127762
time_elpased: 2.897
batch start
#iterations: 324
currently lose_sum: 95.25901037454605
time_elpased: 3.016
batch start
#iterations: 325
currently lose_sum: 95.00497508049011
time_elpased: 2.92
batch start
#iterations: 326
currently lose_sum: 94.88996434211731
time_elpased: 3.297
batch start
#iterations: 327
currently lose_sum: 95.02356290817261
time_elpased: 3.236
batch start
#iterations: 328
currently lose_sum: 95.32132679224014
time_elpased: 3.167
batch start
#iterations: 329
currently lose_sum: 94.61133223772049
time_elpased: 3.214
batch start
#iterations: 330
currently lose_sum: 94.99908709526062
time_elpased: 2.79
batch start
#iterations: 331
currently lose_sum: 94.9760674238205
time_elpased: 3.149
batch start
#iterations: 332
currently lose_sum: 94.50209993124008
time_elpased: 3.1
batch start
#iterations: 333
currently lose_sum: 95.03974121809006
time_elpased: 3.165
batch start
#iterations: 334
currently lose_sum: 95.26190966367722
time_elpased: 3.173
batch start
#iterations: 335
currently lose_sum: 94.77213203907013
time_elpased: 2.999
batch start
#iterations: 336
currently lose_sum: 94.93614363670349
time_elpased: 2.549
batch start
#iterations: 337
currently lose_sum: 95.46766322851181
time_elpased: 2.763
batch start
#iterations: 338
currently lose_sum: 95.08036756515503
time_elpased: 3.084
batch start
#iterations: 339
currently lose_sum: 95.15472263097763
time_elpased: 3.371
start validation test
0.662371134021
0.625069109865
0.814448903983
0.707301814282
0.662104138039
60.804
batch start
#iterations: 340
currently lose_sum: 95.2137376666069
time_elpased: 3.249
batch start
#iterations: 341
currently lose_sum: 94.80709564685822
time_elpased: 3.117
batch start
#iterations: 342
currently lose_sum: 95.10181802511215
time_elpased: 3.109
batch start
#iterations: 343
currently lose_sum: 95.35891252756119
time_elpased: 2.974
batch start
#iterations: 344
currently lose_sum: 94.70605289936066
time_elpased: 2.81
batch start
#iterations: 345
currently lose_sum: 95.22974973917007
time_elpased: 2.626
batch start
#iterations: 346
currently lose_sum: 94.71303832530975
time_elpased: 3.128
batch start
#iterations: 347
currently lose_sum: 95.4335076212883
time_elpased: 3.055
batch start
#iterations: 348
currently lose_sum: 95.06258916854858
time_elpased: 3.121
batch start
#iterations: 349
currently lose_sum: 95.14824795722961
time_elpased: 3.004
batch start
#iterations: 350
currently lose_sum: 95.23495072126389
time_elpased: 3.045
batch start
#iterations: 351
currently lose_sum: 94.95509147644043
time_elpased: 3.132
batch start
#iterations: 352
currently lose_sum: 95.08050632476807
time_elpased: 3.364
batch start
#iterations: 353
currently lose_sum: 94.96586436033249
time_elpased: 2.81
batch start
#iterations: 354
currently lose_sum: 95.6399941444397
time_elpased: 2.332
batch start
#iterations: 355
currently lose_sum: 94.92778158187866
time_elpased: 2.721
batch start
#iterations: 356
currently lose_sum: 95.1285240650177
time_elpased: 2.963
batch start
#iterations: 357
currently lose_sum: 94.99770373106003
time_elpased: 3.37
batch start
#iterations: 358
currently lose_sum: 95.15776765346527
time_elpased: 3.125
batch start
#iterations: 359
currently lose_sum: 95.0179876089096
time_elpased: 3.168
start validation test
0.646958762887
0.652164685908
0.632499742719
0.642181704195
0.646984147927
61.382
batch start
#iterations: 360
currently lose_sum: 94.79764503240585
time_elpased: 3.093
batch start
#iterations: 361
currently lose_sum: 95.24982303380966
time_elpased: 2.652
batch start
#iterations: 362
currently lose_sum: 95.06488400697708
time_elpased: 3.013
batch start
#iterations: 363
currently lose_sum: 95.11627566814423
time_elpased: 2.847
batch start
#iterations: 364
currently lose_sum: 94.9740344285965
time_elpased: 2.981
batch start
#iterations: 365
currently lose_sum: 95.09341841936111
time_elpased: 3.479
batch start
#iterations: 366
currently lose_sum: 95.31040477752686
time_elpased: 3.664
batch start
#iterations: 367
currently lose_sum: 94.96541154384613
time_elpased: 3.005
batch start
#iterations: 368
currently lose_sum: 94.99808937311172
time_elpased: 3.26
batch start
#iterations: 369
currently lose_sum: 95.0464089512825
time_elpased: 3.301
batch start
#iterations: 370
currently lose_sum: 94.68315720558167
time_elpased: 3.34
batch start
#iterations: 371
currently lose_sum: 94.82606965303421
time_elpased: 2.945
batch start
#iterations: 372
currently lose_sum: 94.96218836307526
time_elpased: 3.046
batch start
#iterations: 373
currently lose_sum: 94.80283761024475
time_elpased: 3.103
batch start
#iterations: 374
currently lose_sum: 95.36461424827576
time_elpased: 3.099
batch start
#iterations: 375
currently lose_sum: 95.47671794891357
time_elpased: 3.291
batch start
#iterations: 376
currently lose_sum: 94.73204642534256
time_elpased: 3.091
batch start
#iterations: 377
currently lose_sum: 95.15635055303574
time_elpased: 3.084
batch start
#iterations: 378
currently lose_sum: 95.06428265571594
time_elpased: 3.259
batch start
#iterations: 379
currently lose_sum: 94.97036671638489
time_elpased: 2.987
start validation test
0.641597938144
0.649761595145
0.61706287949
0.63299023489
0.641641013224
61.444
batch start
#iterations: 380
currently lose_sum: 95.34757405519485
time_elpased: 3.137
batch start
#iterations: 381
currently lose_sum: 94.87578254938126
time_elpased: 2.85
batch start
#iterations: 382
currently lose_sum: 95.13668274879456
time_elpased: 2.904
batch start
#iterations: 383
currently lose_sum: 95.22208726406097
time_elpased: 2.986
batch start
#iterations: 384
currently lose_sum: 95.04256284236908
time_elpased: 2.931
batch start
#iterations: 385
currently lose_sum: 95.04942589998245
time_elpased: 3.122
batch start
#iterations: 386
currently lose_sum: 95.11106038093567
time_elpased: 3.261
batch start
#iterations: 387
currently lose_sum: 95.26091396808624
time_elpased: 2.794
batch start
#iterations: 388
currently lose_sum: 94.9223707318306
time_elpased: 3.0
batch start
#iterations: 389
currently lose_sum: 95.22363698482513
time_elpased: 2.886
batch start
#iterations: 390
currently lose_sum: 94.87341046333313
time_elpased: 3.425
batch start
#iterations: 391
currently lose_sum: 94.87991398572922
time_elpased: 3.546
batch start
#iterations: 392
currently lose_sum: 94.79334104061127
time_elpased: 2.492
batch start
#iterations: 393
currently lose_sum: 94.71165812015533
time_elpased: 2.813
batch start
#iterations: 394
currently lose_sum: 95.15510076284409
time_elpased: 2.826
batch start
#iterations: 395
currently lose_sum: 94.96129560470581
time_elpased: 2.643
batch start
#iterations: 396
currently lose_sum: 94.56000971794128
time_elpased: 2.569
batch start
#iterations: 397
currently lose_sum: 95.12561666965485
time_elpased: 2.507
batch start
#iterations: 398
currently lose_sum: 94.68615227937698
time_elpased: 2.989
batch start
#iterations: 399
currently lose_sum: 95.35895538330078
time_elpased: 2.924
start validation test
0.659381443299
0.621778300039
0.816815889678
0.706075971889
0.659105042846
60.940
acc: 0.659
pre: 0.621
rec: 0.819
F1: 0.706
auc: 0.696
