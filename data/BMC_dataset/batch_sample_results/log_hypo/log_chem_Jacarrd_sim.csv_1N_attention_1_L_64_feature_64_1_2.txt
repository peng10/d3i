start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.31023353338242
time_elpased: 2.261
batch start
#iterations: 1
currently lose_sum: 100.47573781013489
time_elpased: 2.52
batch start
#iterations: 2
currently lose_sum: 100.3540433049202
time_elpased: 2.648
batch start
#iterations: 3
currently lose_sum: 100.31887632608414
time_elpased: 2.518
batch start
#iterations: 4
currently lose_sum: 100.22036463022232
time_elpased: 2.518
batch start
#iterations: 5
currently lose_sum: 100.17078870534897
time_elpased: 2.513
batch start
#iterations: 6
currently lose_sum: 100.0401701927185
time_elpased: 2.674
batch start
#iterations: 7
currently lose_sum: 100.0401371717453
time_elpased: 2.267
batch start
#iterations: 8
currently lose_sum: 99.81531715393066
time_elpased: 2.338
batch start
#iterations: 9
currently lose_sum: 99.94012850522995
time_elpased: 2.472
batch start
#iterations: 10
currently lose_sum: 99.7529661655426
time_elpased: 2.316
batch start
#iterations: 11
currently lose_sum: 99.68624955415726
time_elpased: 2.605
batch start
#iterations: 12
currently lose_sum: 99.65058088302612
time_elpased: 2.578
batch start
#iterations: 13
currently lose_sum: 99.32641637325287
time_elpased: 2.557
batch start
#iterations: 14
currently lose_sum: 99.32918220758438
time_elpased: 2.357
batch start
#iterations: 15
currently lose_sum: 99.27796053886414
time_elpased: 2.427
batch start
#iterations: 16
currently lose_sum: 99.15878546237946
time_elpased: 2.413
batch start
#iterations: 17
currently lose_sum: 98.92301028966904
time_elpased: 2.501
batch start
#iterations: 18
currently lose_sum: 98.74084329605103
time_elpased: 2.49
batch start
#iterations: 19
currently lose_sum: 98.85047280788422
time_elpased: 2.528
start validation test
0.63
0.610708991018
0.72069568797
0.661159365559
0.629840769731
64.504
batch start
#iterations: 20
currently lose_sum: 98.65158140659332
time_elpased: 2.69
batch start
#iterations: 21
currently lose_sum: 98.40509021282196
time_elpased: 2.643
batch start
#iterations: 22
currently lose_sum: 98.35083550214767
time_elpased: 2.762
batch start
#iterations: 23
currently lose_sum: 98.10241144895554
time_elpased: 2.213
batch start
#iterations: 24
currently lose_sum: 98.1017427444458
time_elpased: 2.503
batch start
#iterations: 25
currently lose_sum: 98.06467872858047
time_elpased: 2.479
batch start
#iterations: 26
currently lose_sum: 97.81714725494385
time_elpased: 2.723
batch start
#iterations: 27
currently lose_sum: 98.09426718950272
time_elpased: 2.818
batch start
#iterations: 28
currently lose_sum: 97.99720948934555
time_elpased: 2.59
batch start
#iterations: 29
currently lose_sum: 97.97111922502518
time_elpased: 2.443
batch start
#iterations: 30
currently lose_sum: 97.85582602024078
time_elpased: 2.672
batch start
#iterations: 31
currently lose_sum: 97.76731026172638
time_elpased: 2.569
batch start
#iterations: 32
currently lose_sum: 97.57328587770462
time_elpased: 2.553
batch start
#iterations: 33
currently lose_sum: 97.65369719266891
time_elpased: 2.473
batch start
#iterations: 34
currently lose_sum: 97.58666127920151
time_elpased: 2.514
batch start
#iterations: 35
currently lose_sum: 97.8176582455635
time_elpased: 2.528
batch start
#iterations: 36
currently lose_sum: 97.55690532922745
time_elpased: 2.546
batch start
#iterations: 37
currently lose_sum: 97.63252294063568
time_elpased: 2.389
batch start
#iterations: 38
currently lose_sum: 97.4613116979599
time_elpased: 2.283
batch start
#iterations: 39
currently lose_sum: 97.39465683698654
time_elpased: 2.354
start validation test
0.643608247423
0.62659199711
0.713903468149
0.667404271695
0.64348483332
62.874
batch start
#iterations: 40
currently lose_sum: 97.19623792171478
time_elpased: 2.241
batch start
#iterations: 41
currently lose_sum: 97.42974704504013
time_elpased: 2.199
batch start
#iterations: 42
currently lose_sum: 97.23560458421707
time_elpased: 2.206
batch start
#iterations: 43
currently lose_sum: 97.15429848432541
time_elpased: 2.398
batch start
#iterations: 44
currently lose_sum: 97.29744094610214
time_elpased: 2.257
batch start
#iterations: 45
currently lose_sum: 97.51028650999069
time_elpased: 2.152
batch start
#iterations: 46
currently lose_sum: 97.38952696323395
time_elpased: 2.258
batch start
#iterations: 47
currently lose_sum: 97.32623142004013
time_elpased: 2.517
batch start
#iterations: 48
currently lose_sum: 97.38985413312912
time_elpased: 2.571
batch start
#iterations: 49
currently lose_sum: 97.40489780902863
time_elpased: 2.591
batch start
#iterations: 50
currently lose_sum: 97.07923972606659
time_elpased: 2.616
batch start
#iterations: 51
currently lose_sum: 97.0922299027443
time_elpased: 2.47
batch start
#iterations: 52
currently lose_sum: 97.17297112941742
time_elpased: 2.39
batch start
#iterations: 53
currently lose_sum: 97.00956517457962
time_elpased: 2.474
batch start
#iterations: 54
currently lose_sum: 96.88742178678513
time_elpased: 2.472
batch start
#iterations: 55
currently lose_sum: 97.09509938955307
time_elpased: 2.482
batch start
#iterations: 56
currently lose_sum: 97.15555113554001
time_elpased: 2.55
batch start
#iterations: 57
currently lose_sum: 97.13914757966995
time_elpased: 2.69
batch start
#iterations: 58
currently lose_sum: 97.02244079113007
time_elpased: 2.553
batch start
#iterations: 59
currently lose_sum: 97.03713858127594
time_elpased: 2.505
start validation test
0.643608247423
0.633718156664
0.683544303797
0.657688880087
0.643538133516
62.373
batch start
#iterations: 60
currently lose_sum: 97.15041756629944
time_elpased: 2.579
batch start
#iterations: 61
currently lose_sum: 96.9686079621315
time_elpased: 2.115
batch start
#iterations: 62
currently lose_sum: 97.24297791719437
time_elpased: 2.493
batch start
#iterations: 63
currently lose_sum: 97.14405363798141
time_elpased: 2.519
batch start
#iterations: 64
currently lose_sum: 96.93438708782196
time_elpased: 2.562
batch start
#iterations: 65
currently lose_sum: 96.97956889867783
time_elpased: 2.612
batch start
#iterations: 66
currently lose_sum: 96.7955127954483
time_elpased: 2.657
batch start
#iterations: 67
currently lose_sum: 96.71465766429901
time_elpased: 2.64
batch start
#iterations: 68
currently lose_sum: 96.98969161510468
time_elpased: 2.571
batch start
#iterations: 69
currently lose_sum: 96.8619812130928
time_elpased: 2.495
batch start
#iterations: 70
currently lose_sum: 96.67721980810165
time_elpased: 2.578
batch start
#iterations: 71
currently lose_sum: 97.19726449251175
time_elpased: 2.721
batch start
#iterations: 72
currently lose_sum: 96.76705312728882
time_elpased: 2.325
batch start
#iterations: 73
currently lose_sum: 96.91647452116013
time_elpased: 2.549
batch start
#iterations: 74
currently lose_sum: 96.94367909431458
time_elpased: 2.656
batch start
#iterations: 75
currently lose_sum: 96.73497653007507
time_elpased: 2.427
batch start
#iterations: 76
currently lose_sum: 96.8926253914833
time_elpased: 2.591
batch start
#iterations: 77
currently lose_sum: 96.8557778596878
time_elpased: 2.609
batch start
#iterations: 78
currently lose_sum: 96.77178978919983
time_elpased: 2.739
batch start
#iterations: 79
currently lose_sum: 97.07687401771545
time_elpased: 2.588
start validation test
0.648556701031
0.613446035846
0.806627559946
0.696896950298
0.648279183257
62.464
batch start
#iterations: 80
currently lose_sum: 97.05240815877914
time_elpased: 2.252
batch start
#iterations: 81
currently lose_sum: 96.92979127168655
time_elpased: 2.594
batch start
#iterations: 82
currently lose_sum: 96.6928203701973
time_elpased: 2.586
batch start
#iterations: 83
currently lose_sum: 96.57326704263687
time_elpased: 2.459
batch start
#iterations: 84
currently lose_sum: 96.8600247502327
time_elpased: 2.628
batch start
#iterations: 85
currently lose_sum: 96.73197901248932
time_elpased: 2.667
batch start
#iterations: 86
currently lose_sum: 96.8398169875145
time_elpased: 2.388
batch start
#iterations: 87
currently lose_sum: 96.8402995467186
time_elpased: 2.388
batch start
#iterations: 88
currently lose_sum: 96.70132964849472
time_elpased: 2.614
batch start
#iterations: 89
currently lose_sum: 96.66870296001434
time_elpased: 2.562
batch start
#iterations: 90
currently lose_sum: 96.83688050508499
time_elpased: 2.521
batch start
#iterations: 91
currently lose_sum: 96.98243254423141
time_elpased: 2.607
batch start
#iterations: 92
currently lose_sum: 96.9308152794838
time_elpased: 2.674
batch start
#iterations: 93
currently lose_sum: 96.72482120990753
time_elpased: 2.445
batch start
#iterations: 94
currently lose_sum: 97.07718926668167
time_elpased: 2.73
batch start
#iterations: 95
currently lose_sum: 96.17722803354263
time_elpased: 2.544
batch start
#iterations: 96
currently lose_sum: 96.91752195358276
time_elpased: 2.441
batch start
#iterations: 97
currently lose_sum: 96.3179617524147
time_elpased: 2.526
batch start
#iterations: 98
currently lose_sum: 96.91393399238586
time_elpased: 2.646
batch start
#iterations: 99
currently lose_sum: 96.64625489711761
time_elpased: 2.721
start validation test
0.632886597938
0.633914748684
0.632088093033
0.633000103061
0.632887999837
62.062
batch start
#iterations: 100
currently lose_sum: 96.96513164043427
time_elpased: 2.652
batch start
#iterations: 101
currently lose_sum: 96.59037679433823
time_elpased: 2.582
batch start
#iterations: 102
currently lose_sum: 96.44000059366226
time_elpased: 2.483
batch start
#iterations: 103
currently lose_sum: 96.66405540704727
time_elpased: 2.429
batch start
#iterations: 104
currently lose_sum: 96.7588317990303
time_elpased: 2.465
batch start
#iterations: 105
currently lose_sum: 96.46605229377747
time_elpased: 2.351
batch start
#iterations: 106
currently lose_sum: 96.46399062871933
time_elpased: 2.247
batch start
#iterations: 107
currently lose_sum: 96.79456663131714
time_elpased: 2.515
batch start
#iterations: 108
currently lose_sum: 96.51898217201233
time_elpased: 2.769
batch start
#iterations: 109
currently lose_sum: 96.67955946922302
time_elpased: 2.522
batch start
#iterations: 110
currently lose_sum: 96.74222922325134
time_elpased: 2.698
batch start
#iterations: 111
currently lose_sum: 96.09025830030441
time_elpased: 2.612
batch start
#iterations: 112
currently lose_sum: 96.42703431844711
time_elpased: 2.33
batch start
#iterations: 113
currently lose_sum: 96.58234173059464
time_elpased: 2.614
batch start
#iterations: 114
currently lose_sum: 96.46408921480179
time_elpased: 2.63
batch start
#iterations: 115
currently lose_sum: 96.73964887857437
time_elpased: 2.427
batch start
#iterations: 116
currently lose_sum: 96.3873872756958
time_elpased: 2.71
batch start
#iterations: 117
currently lose_sum: 96.61441403627396
time_elpased: 2.398
batch start
#iterations: 118
currently lose_sum: 96.18212014436722
time_elpased: 2.352
batch start
#iterations: 119
currently lose_sum: 96.36345767974854
time_elpased: 2.46
start validation test
0.653195876289
0.626727719834
0.760625707523
0.687215248722
0.65300726665
61.818
batch start
#iterations: 120
currently lose_sum: 96.42586499452591
time_elpased: 2.641
batch start
#iterations: 121
currently lose_sum: 96.46200996637344
time_elpased: 2.446
batch start
#iterations: 122
currently lose_sum: 96.56895285844803
time_elpased: 2.688
batch start
#iterations: 123
currently lose_sum: 96.51299387216568
time_elpased: 2.515
batch start
#iterations: 124
currently lose_sum: 96.43961226940155
time_elpased: 2.682
batch start
#iterations: 125
currently lose_sum: 96.40877223014832
time_elpased: 2.442
batch start
#iterations: 126
currently lose_sum: 96.71164155006409
time_elpased: 2.603
batch start
#iterations: 127
currently lose_sum: 96.69381070137024
time_elpased: 2.525
batch start
#iterations: 128
currently lose_sum: 96.27478337287903
time_elpased: 2.806
batch start
#iterations: 129
currently lose_sum: 96.99346709251404
time_elpased: 2.462
batch start
#iterations: 130
currently lose_sum: 96.36037266254425
time_elpased: 2.558
batch start
#iterations: 131
currently lose_sum: 96.89742535352707
time_elpased: 2.472
batch start
#iterations: 132
currently lose_sum: 96.41838800907135
time_elpased: 2.691
batch start
#iterations: 133
currently lose_sum: 96.40542101860046
time_elpased: 2.571
batch start
#iterations: 134
currently lose_sum: 96.5844224691391
time_elpased: 2.474
batch start
#iterations: 135
currently lose_sum: 96.40307551622391
time_elpased: 2.472
batch start
#iterations: 136
currently lose_sum: 96.44417017698288
time_elpased: 2.598
batch start
#iterations: 137
currently lose_sum: 96.34201377630234
time_elpased: 2.51
batch start
#iterations: 138
currently lose_sum: 96.48773872852325
time_elpased: 2.476
batch start
#iterations: 139
currently lose_sum: 96.43808662891388
time_elpased: 2.645
start validation test
0.651701030928
0.624056999162
0.766182978285
0.687855130041
0.651500040212
61.718
batch start
#iterations: 140
currently lose_sum: 96.33328878879547
time_elpased: 2.51
batch start
#iterations: 141
currently lose_sum: 96.6006047129631
time_elpased: 2.478
batch start
#iterations: 142
currently lose_sum: 96.22508609294891
time_elpased: 2.445
batch start
#iterations: 143
currently lose_sum: 96.27065861225128
time_elpased: 2.426
batch start
#iterations: 144
currently lose_sum: 96.5773177742958
time_elpased: 2.219
batch start
#iterations: 145
currently lose_sum: 96.25858426094055
time_elpased: 2.602
batch start
#iterations: 146
currently lose_sum: 96.32441705465317
time_elpased: 2.419
batch start
#iterations: 147
currently lose_sum: 96.25710886716843
time_elpased: 2.495
batch start
#iterations: 148
currently lose_sum: 96.04014313220978
time_elpased: 2.462
batch start
#iterations: 149
currently lose_sum: 96.11432355642319
time_elpased: 2.551
batch start
#iterations: 150
currently lose_sum: 96.37790375947952
time_elpased: 2.731
batch start
#iterations: 151
currently lose_sum: 96.40138185024261
time_elpased: 2.686
batch start
#iterations: 152
currently lose_sum: 96.38437992334366
time_elpased: 2.666
batch start
#iterations: 153
currently lose_sum: 96.49076867103577
time_elpased: 2.477
batch start
#iterations: 154
currently lose_sum: 96.38645488023758
time_elpased: 2.618
batch start
#iterations: 155
currently lose_sum: 96.48434895277023
time_elpased: 2.562
batch start
#iterations: 156
currently lose_sum: 96.3696556687355
time_elpased: 2.459
batch start
#iterations: 157
currently lose_sum: 96.13096123933792
time_elpased: 2.536
batch start
#iterations: 158
currently lose_sum: 96.29636853933334
time_elpased: 2.502
batch start
#iterations: 159
currently lose_sum: 96.21397721767426
time_elpased: 2.39
start validation test
0.649587628866
0.640539239287
0.684573428013
0.66182469406
0.649526205899
61.885
batch start
#iterations: 160
currently lose_sum: 95.7712128162384
time_elpased: 2.849
batch start
#iterations: 161
currently lose_sum: 96.42858511209488
time_elpased: 2.442
batch start
#iterations: 162
currently lose_sum: 96.49107164144516
time_elpased: 2.621
batch start
#iterations: 163
currently lose_sum: 96.40275675058365
time_elpased: 2.447
batch start
#iterations: 164
currently lose_sum: 95.62981450557709
time_elpased: 2.191
batch start
#iterations: 165
currently lose_sum: 96.17135947942734
time_elpased: 2.433
batch start
#iterations: 166
currently lose_sum: 96.272725045681
time_elpased: 2.59
batch start
#iterations: 167
currently lose_sum: 96.21565556526184
time_elpased: 2.657
batch start
#iterations: 168
currently lose_sum: 96.0798150897026
time_elpased: 2.88
batch start
#iterations: 169
currently lose_sum: 96.10398125648499
time_elpased: 2.512
batch start
#iterations: 170
currently lose_sum: 96.28548187017441
time_elpased: 2.494
batch start
#iterations: 171
currently lose_sum: 96.26168537139893
time_elpased: 2.259
batch start
#iterations: 172
currently lose_sum: 96.84647762775421
time_elpased: 2.489
batch start
#iterations: 173
currently lose_sum: 96.1616678237915
time_elpased: 2.682
batch start
#iterations: 174
currently lose_sum: 95.52707755565643
time_elpased: 2.312
batch start
#iterations: 175
currently lose_sum: 96.03318148851395
time_elpased: 2.519
batch start
#iterations: 176
currently lose_sum: 96.06990230083466
time_elpased: 2.682
batch start
#iterations: 177
currently lose_sum: 96.23933166265488
time_elpased: 2.643
batch start
#iterations: 178
currently lose_sum: 96.17768955230713
time_elpased: 2.638
batch start
#iterations: 179
currently lose_sum: 96.05967366695404
time_elpased: 2.641
start validation test
0.654484536082
0.627776835679
0.761963569003
0.688391985496
0.654295840063
61.296
batch start
#iterations: 180
currently lose_sum: 95.87817543745041
time_elpased: 2.669
batch start
#iterations: 181
currently lose_sum: 96.14310473203659
time_elpased: 2.947
batch start
#iterations: 182
currently lose_sum: 96.11263763904572
time_elpased: 2.872
batch start
#iterations: 183
currently lose_sum: 95.80822068452835
time_elpased: 2.741
batch start
#iterations: 184
currently lose_sum: 96.39032047986984
time_elpased: 2.658
batch start
#iterations: 185
currently lose_sum: 96.08262866735458
time_elpased: 2.347
batch start
#iterations: 186
currently lose_sum: 96.03532809019089
time_elpased: 2.471
batch start
#iterations: 187
currently lose_sum: 96.20282930135727
time_elpased: 2.526
batch start
#iterations: 188
currently lose_sum: 96.2405863404274
time_elpased: 2.62
batch start
#iterations: 189
currently lose_sum: 96.25964403152466
time_elpased: 2.71
batch start
#iterations: 190
currently lose_sum: 96.13258957862854
time_elpased: 2.322
batch start
#iterations: 191
currently lose_sum: 95.99939972162247
time_elpased: 2.525
batch start
#iterations: 192
currently lose_sum: 96.32902824878693
time_elpased: 2.602
batch start
#iterations: 193
currently lose_sum: 95.80417490005493
time_elpased: 2.648
batch start
#iterations: 194
currently lose_sum: 95.82969677448273
time_elpased: 2.579
batch start
#iterations: 195
currently lose_sum: 96.06740295886993
time_elpased: 2.529
batch start
#iterations: 196
currently lose_sum: 95.92847901582718
time_elpased: 2.603
batch start
#iterations: 197
currently lose_sum: 96.30469971895218
time_elpased: 2.646
batch start
#iterations: 198
currently lose_sum: 95.94812262058258
time_elpased: 2.456
batch start
#iterations: 199
currently lose_sum: 96.18662315607071
time_elpased: 2.55
start validation test
0.655463917526
0.627576343905
0.767726664608
0.690612849472
0.655266822958
61.445
batch start
#iterations: 200
currently lose_sum: 96.02919089794159
time_elpased: 2.528
batch start
#iterations: 201
currently lose_sum: 96.04220068454742
time_elpased: 2.42
batch start
#iterations: 202
currently lose_sum: 95.94495725631714
time_elpased: 2.657
batch start
#iterations: 203
currently lose_sum: 95.88802695274353
time_elpased: 2.445
batch start
#iterations: 204
currently lose_sum: 95.94951045513153
time_elpased: 2.577
batch start
#iterations: 205
currently lose_sum: 95.74018424749374
time_elpased: 2.469
batch start
#iterations: 206
currently lose_sum: 95.86828887462616
time_elpased: 2.523
batch start
#iterations: 207
currently lose_sum: 96.0365139245987
time_elpased: 2.787
batch start
#iterations: 208
currently lose_sum: 96.1126481294632
time_elpased: 2.488
batch start
#iterations: 209
currently lose_sum: 95.77959263324738
time_elpased: 2.252
batch start
#iterations: 210
currently lose_sum: 95.92547887563705
time_elpased: 2.57
batch start
#iterations: 211
currently lose_sum: 96.22035622596741
time_elpased: 2.426
batch start
#iterations: 212
currently lose_sum: 96.08415567874908
time_elpased: 2.592
batch start
#iterations: 213
currently lose_sum: 96.30491071939468
time_elpased: 2.604
batch start
#iterations: 214
currently lose_sum: 95.99249637126923
time_elpased: 2.644
batch start
#iterations: 215
currently lose_sum: 95.89070743322372
time_elpased: 2.557
batch start
#iterations: 216
currently lose_sum: 95.96379208564758
time_elpased: 2.481
batch start
#iterations: 217
currently lose_sum: 95.73245233297348
time_elpased: 2.566
batch start
#iterations: 218
currently lose_sum: 95.80887705087662
time_elpased: 2.563
batch start
#iterations: 219
currently lose_sum: 96.17427015304565
time_elpased: 2.847
start validation test
0.631494845361
0.644888287068
0.58814448904
0.615210721783
0.631570953598
61.918
batch start
#iterations: 220
currently lose_sum: 95.8651533126831
time_elpased: 2.657
batch start
#iterations: 221
currently lose_sum: 95.93991708755493
time_elpased: 2.482
batch start
#iterations: 222
currently lose_sum: 95.65606504678726
time_elpased: 2.391
batch start
#iterations: 223
currently lose_sum: 95.46584802865982
time_elpased: 2.276
batch start
#iterations: 224
currently lose_sum: 95.8124275803566
time_elpased: 2.382
batch start
#iterations: 225
currently lose_sum: 96.28717464208603
time_elpased: 2.352
batch start
#iterations: 226
currently lose_sum: 95.54922211170197
time_elpased: 2.677
batch start
#iterations: 227
currently lose_sum: 95.84546393156052
time_elpased: 2.501
batch start
#iterations: 228
currently lose_sum: 95.85446655750275
time_elpased: 2.554
batch start
#iterations: 229
currently lose_sum: 95.7012552022934
time_elpased: 2.625
batch start
#iterations: 230
currently lose_sum: 95.79722988605499
time_elpased: 2.508
batch start
#iterations: 231
currently lose_sum: 95.49271559715271
time_elpased: 2.544
batch start
#iterations: 232
currently lose_sum: 96.07162874937057
time_elpased: 2.586
batch start
#iterations: 233
currently lose_sum: 96.04152321815491
time_elpased: 2.668
batch start
#iterations: 234
currently lose_sum: 95.61443680524826
time_elpased: 2.157
batch start
#iterations: 235
currently lose_sum: 95.56031620502472
time_elpased: 2.706
batch start
#iterations: 236
currently lose_sum: 96.15634155273438
time_elpased: 2.206
batch start
#iterations: 237
currently lose_sum: 96.02379900217056
time_elpased: 2.421
batch start
#iterations: 238
currently lose_sum: 95.84804481267929
time_elpased: 2.679
batch start
#iterations: 239
currently lose_sum: 95.82407069206238
time_elpased: 2.327
start validation test
0.651494845361
0.612721171446
0.826798394566
0.703841604976
0.651187072941
61.486
batch start
#iterations: 240
currently lose_sum: 95.62976723909378
time_elpased: 2.733
batch start
#iterations: 241
currently lose_sum: 96.05690199136734
time_elpased: 2.536
batch start
#iterations: 242
currently lose_sum: 95.81541383266449
time_elpased: 2.387
batch start
#iterations: 243
currently lose_sum: 95.79834830760956
time_elpased: 2.356
batch start
#iterations: 244
currently lose_sum: 96.09468442201614
time_elpased: 2.079
batch start
#iterations: 245
currently lose_sum: 95.70303320884705
time_elpased: 2.224
batch start
#iterations: 246
currently lose_sum: 95.9276293516159
time_elpased: 2.217
batch start
#iterations: 247
currently lose_sum: 95.24690061807632
time_elpased: 2.208
batch start
#iterations: 248
currently lose_sum: 95.75957721471786
time_elpased: 2.34
batch start
#iterations: 249
currently lose_sum: 95.855852663517
time_elpased: 2.718
batch start
#iterations: 250
currently lose_sum: 95.39156472682953
time_elpased: 2.562
batch start
#iterations: 251
currently lose_sum: 95.83198654651642
time_elpased: 2.552
batch start
#iterations: 252
currently lose_sum: 95.86337000131607
time_elpased: 2.561
batch start
#iterations: 253
currently lose_sum: 95.66691434383392
time_elpased: 2.327
batch start
#iterations: 254
currently lose_sum: 95.75040024518967
time_elpased: 2.673
batch start
#iterations: 255
currently lose_sum: 95.87750506401062
time_elpased: 2.808
batch start
#iterations: 256
currently lose_sum: 95.34283691644669
time_elpased: 2.783
batch start
#iterations: 257
currently lose_sum: 95.84147930145264
time_elpased: 2.695
batch start
#iterations: 258
currently lose_sum: 95.83778357505798
time_elpased: 2.58
batch start
#iterations: 259
currently lose_sum: 95.44718354940414
time_elpased: 2.628
start validation test
0.639278350515
0.654155799977
0.593701759802
0.622464393612
0.63935836725
61.693
batch start
#iterations: 260
currently lose_sum: 95.90655273199081
time_elpased: 2.56
batch start
#iterations: 261
currently lose_sum: 95.48204350471497
time_elpased: 2.649
batch start
#iterations: 262
currently lose_sum: 95.95466500520706
time_elpased: 2.591
batch start
#iterations: 263
currently lose_sum: 96.07421284914017
time_elpased: 2.582
batch start
#iterations: 264
currently lose_sum: 95.66033214330673
time_elpased: 2.631
batch start
#iterations: 265
currently lose_sum: 95.94447559118271
time_elpased: 2.425
batch start
#iterations: 266
currently lose_sum: 95.4701641201973
time_elpased: 2.607
batch start
#iterations: 267
currently lose_sum: 95.65327769517899
time_elpased: 2.647
batch start
#iterations: 268
currently lose_sum: 95.74018555879593
time_elpased: 2.65
batch start
#iterations: 269
currently lose_sum: 95.8671286702156
time_elpased: 2.653
batch start
#iterations: 270
currently lose_sum: 95.50650084018707
time_elpased: 2.682
batch start
#iterations: 271
currently lose_sum: 95.75166845321655
time_elpased: 2.565
batch start
#iterations: 272
currently lose_sum: 95.36200159788132
time_elpased: 2.469
batch start
#iterations: 273
currently lose_sum: 95.23611915111542
time_elpased: 2.173
batch start
#iterations: 274
currently lose_sum: 95.55185121297836
time_elpased: 2.464
batch start
#iterations: 275
currently lose_sum: 95.7968630194664
time_elpased: 2.585
batch start
#iterations: 276
currently lose_sum: 95.66563493013382
time_elpased: 2.604
batch start
#iterations: 277
currently lose_sum: 95.37837702035904
time_elpased: 2.501
batch start
#iterations: 278
currently lose_sum: 95.67307901382446
time_elpased: 2.41
batch start
#iterations: 279
currently lose_sum: 95.60323023796082
time_elpased: 2.438
start validation test
0.619690721649
0.649763093866
0.52217762684
0.579025447906
0.619861920928
62.074
batch start
#iterations: 280
currently lose_sum: 95.75799244642258
time_elpased: 2.638
batch start
#iterations: 281
currently lose_sum: 95.55596178770065
time_elpased: 2.539
batch start
#iterations: 282
currently lose_sum: 95.97254544496536
time_elpased: 2.569
batch start
#iterations: 283
currently lose_sum: 95.59619188308716
time_elpased: 2.55
batch start
#iterations: 284
currently lose_sum: 95.73563385009766
time_elpased: 2.5
batch start
#iterations: 285
currently lose_sum: 95.67295384407043
time_elpased: 2.284
batch start
#iterations: 286
currently lose_sum: 95.31647074222565
time_elpased: 2.247
batch start
#iterations: 287
currently lose_sum: 95.40759545564651
time_elpased: 2.558
batch start
#iterations: 288
currently lose_sum: 95.60561847686768
time_elpased: 2.621
batch start
#iterations: 289
currently lose_sum: 95.2913807630539
time_elpased: 2.212
batch start
#iterations: 290
currently lose_sum: 95.38862752914429
time_elpased: 2.463
batch start
#iterations: 291
currently lose_sum: 95.92256128787994
time_elpased: 2.925
batch start
#iterations: 292
currently lose_sum: 95.51027363538742
time_elpased: 2.571
batch start
#iterations: 293
currently lose_sum: 95.52587759494781
time_elpased: 2.668
batch start
#iterations: 294
currently lose_sum: 95.69924312829971
time_elpased: 2.429
batch start
#iterations: 295
currently lose_sum: 95.99015420675278
time_elpased: 2.581
batch start
#iterations: 296
currently lose_sum: 95.69576865434647
time_elpased: 2.576
batch start
#iterations: 297
currently lose_sum: 95.64687293767929
time_elpased: 2.365
batch start
#iterations: 298
currently lose_sum: 95.46111863851547
time_elpased: 2.107
batch start
#iterations: 299
currently lose_sum: 95.68749165534973
time_elpased: 2.436
start validation test
0.652783505155
0.622503493055
0.779458680663
0.692195211113
0.652561107346
61.105
batch start
#iterations: 300
currently lose_sum: 95.28940540552139
time_elpased: 2.435
batch start
#iterations: 301
currently lose_sum: 95.76054286956787
time_elpased: 2.534
batch start
#iterations: 302
currently lose_sum: 95.58348280191422
time_elpased: 2.33
batch start
#iterations: 303
currently lose_sum: 95.96999901533127
time_elpased: 2.635
batch start
#iterations: 304
currently lose_sum: 95.82637876272202
time_elpased: 2.506
batch start
#iterations: 305
currently lose_sum: 95.43783956766129
time_elpased: 2.656
batch start
#iterations: 306
currently lose_sum: 95.45024055242538
time_elpased: 2.562
batch start
#iterations: 307
currently lose_sum: 95.51390326023102
time_elpased: 2.314
batch start
#iterations: 308
currently lose_sum: 95.43819242715836
time_elpased: 2.348
batch start
#iterations: 309
currently lose_sum: 95.0374881029129
time_elpased: 2.011
batch start
#iterations: 310
currently lose_sum: 95.97838282585144
time_elpased: 2.495
batch start
#iterations: 311
currently lose_sum: 95.35183250904083
time_elpased: 2.411
batch start
#iterations: 312
currently lose_sum: 95.30438685417175
time_elpased: 2.388
batch start
#iterations: 313
currently lose_sum: 95.42419475317001
time_elpased: 2.398
batch start
#iterations: 314
currently lose_sum: 95.5010330080986
time_elpased: 2.206
batch start
#iterations: 315
currently lose_sum: 95.26267832517624
time_elpased: 2.619
batch start
#iterations: 316
currently lose_sum: 95.77724206447601
time_elpased: 3.048
batch start
#iterations: 317
currently lose_sum: 95.48212057352066
time_elpased: 2.466
batch start
#iterations: 318
currently lose_sum: 95.52718895673752
time_elpased: 2.751
batch start
#iterations: 319
currently lose_sum: 95.7989770770073
time_elpased: 2.637
start validation test
0.653917525773
0.608922742111
0.863846866317
0.714322185346
0.653548962436
61.237
batch start
#iterations: 320
currently lose_sum: 95.40001940727234
time_elpased: 2.51
batch start
#iterations: 321
currently lose_sum: 95.80578935146332
time_elpased: 2.658
batch start
#iterations: 322
currently lose_sum: 95.8486921787262
time_elpased: 2.655
batch start
#iterations: 323
currently lose_sum: 95.74694287776947
time_elpased: 2.741
batch start
#iterations: 324
currently lose_sum: 95.49777048826218
time_elpased: 2.736
batch start
#iterations: 325
currently lose_sum: 95.72013282775879
time_elpased: 2.578
batch start
#iterations: 326
currently lose_sum: 95.12880331277847
time_elpased: 2.511
batch start
#iterations: 327
currently lose_sum: 95.68862301111221
time_elpased: 2.468
batch start
#iterations: 328
currently lose_sum: 95.68862253427505
time_elpased: 2.552
batch start
#iterations: 329
currently lose_sum: 95.3396412730217
time_elpased: 2.491
batch start
#iterations: 330
currently lose_sum: 95.74062037467957
time_elpased: 2.505
batch start
#iterations: 331
currently lose_sum: 95.68188446760178
time_elpased: 2.775
batch start
#iterations: 332
currently lose_sum: 95.33939784765244
time_elpased: 2.775
batch start
#iterations: 333
currently lose_sum: 95.7595751285553
time_elpased: 2.44
batch start
#iterations: 334
currently lose_sum: 95.05606031417847
time_elpased: 2.473
batch start
#iterations: 335
currently lose_sum: 95.31307590007782
time_elpased: 2.077
batch start
#iterations: 336
currently lose_sum: 95.55376356840134
time_elpased: 2.721
batch start
#iterations: 337
currently lose_sum: 95.8084380030632
time_elpased: 2.439
batch start
#iterations: 338
currently lose_sum: 95.52530914545059
time_elpased: 2.601
batch start
#iterations: 339
currently lose_sum: 95.61645185947418
time_elpased: 2.708
start validation test
0.653556701031
0.625313702526
0.769270350931
0.689862027594
0.653353547871
60.993
batch start
#iterations: 340
currently lose_sum: 95.51542395353317
time_elpased: 2.567
batch start
#iterations: 341
currently lose_sum: 95.66509526968002
time_elpased: 2.465
batch start
#iterations: 342
currently lose_sum: 95.95312756299973
time_elpased: 2.574
batch start
#iterations: 343
currently lose_sum: 95.49752032756805
time_elpased: 2.547
batch start
#iterations: 344
currently lose_sum: 95.44642299413681
time_elpased: 2.612
batch start
#iterations: 345
currently lose_sum: 95.46109837293625
time_elpased: 2.607
batch start
#iterations: 346
currently lose_sum: 95.31673979759216
time_elpased: 2.57
batch start
#iterations: 347
currently lose_sum: 95.55682122707367
time_elpased: 2.745
batch start
#iterations: 348
currently lose_sum: 95.2499178647995
time_elpased: 2.55
batch start
#iterations: 349
currently lose_sum: 95.826446890831
time_elpased: 2.362
batch start
#iterations: 350
currently lose_sum: 95.32450729608536
time_elpased: 2.637
batch start
#iterations: 351
currently lose_sum: 95.64141297340393
time_elpased: 2.706
batch start
#iterations: 352
currently lose_sum: 95.53820598125458
time_elpased: 2.412
batch start
#iterations: 353
currently lose_sum: 95.13186472654343
time_elpased: 2.613
batch start
#iterations: 354
currently lose_sum: 95.62868189811707
time_elpased: 2.598
batch start
#iterations: 355
currently lose_sum: 95.20679652690887
time_elpased: 2.568
batch start
#iterations: 356
currently lose_sum: 95.62423604726791
time_elpased: 2.638
batch start
#iterations: 357
currently lose_sum: 95.59428668022156
time_elpased: 2.635
batch start
#iterations: 358
currently lose_sum: 95.57635259628296
time_elpased: 2.625
batch start
#iterations: 359
currently lose_sum: 95.23687952756882
time_elpased: 2.519
start validation test
0.656288659794
0.61762209706
0.823813934342
0.70597054414
0.655994543335
61.004
batch start
#iterations: 360
currently lose_sum: 95.751005589962
time_elpased: 2.678
batch start
#iterations: 361
currently lose_sum: 95.63876932859421
time_elpased: 2.583
batch start
#iterations: 362
currently lose_sum: 95.49498075246811
time_elpased: 2.667
batch start
#iterations: 363
currently lose_sum: 95.25735527276993
time_elpased: 2.634
batch start
#iterations: 364
currently lose_sum: 95.44001054763794
time_elpased: 2.617
batch start
#iterations: 365
currently lose_sum: 95.79170793294907
time_elpased: 2.5
batch start
#iterations: 366
currently lose_sum: 95.33540940284729
time_elpased: 2.5
batch start
#iterations: 367
currently lose_sum: 95.15067547559738
time_elpased: 2.37
batch start
#iterations: 368
currently lose_sum: 95.61498445272446
time_elpased: 2.27
batch start
#iterations: 369
currently lose_sum: 95.78999292850494
time_elpased: 2.482
batch start
#iterations: 370
currently lose_sum: 95.3547460436821
time_elpased: 2.581
batch start
#iterations: 371
currently lose_sum: 95.45276147127151
time_elpased: 2.67
batch start
#iterations: 372
currently lose_sum: 95.28232508897781
time_elpased: 2.63
batch start
#iterations: 373
currently lose_sum: 95.46797597408295
time_elpased: 2.521
batch start
#iterations: 374
currently lose_sum: 95.90008568763733
time_elpased: 2.543
batch start
#iterations: 375
currently lose_sum: 95.06238597631454
time_elpased: 2.491
batch start
#iterations: 376
currently lose_sum: 95.64628267288208
time_elpased: 2.693
batch start
#iterations: 377
currently lose_sum: 95.78208023309708
time_elpased: 2.44
batch start
#iterations: 378
currently lose_sum: 95.43338358402252
time_elpased: 2.532
batch start
#iterations: 379
currently lose_sum: 95.21008318662643
time_elpased: 2.545
start validation test
0.655773195876
0.622748202601
0.793351857569
0.697773352643
0.655531655316
60.966
batch start
#iterations: 380
currently lose_sum: 95.85575902462006
time_elpased: 2.652
batch start
#iterations: 381
currently lose_sum: 95.18312925100327
time_elpased: 2.476
batch start
#iterations: 382
currently lose_sum: 95.46095514297485
time_elpased: 2.497
batch start
#iterations: 383
currently lose_sum: 95.43860256671906
time_elpased: 2.279
batch start
#iterations: 384
currently lose_sum: 95.77218735218048
time_elpased: 2.43
batch start
#iterations: 385
currently lose_sum: 95.64090234041214
time_elpased: 2.642
batch start
#iterations: 386
currently lose_sum: 95.66824096441269
time_elpased: 2.635
batch start
#iterations: 387
currently lose_sum: 95.90181320905685
time_elpased: 2.448
batch start
#iterations: 388
currently lose_sum: 95.45420622825623
time_elpased: 2.589
batch start
#iterations: 389
currently lose_sum: 95.53920674324036
time_elpased: 2.675
batch start
#iterations: 390
currently lose_sum: 95.35768365859985
time_elpased: 2.668
batch start
#iterations: 391
currently lose_sum: 95.6617146730423
time_elpased: 2.464
batch start
#iterations: 392
currently lose_sum: 95.3835169672966
time_elpased: 2.689
batch start
#iterations: 393
currently lose_sum: 95.35635250806808
time_elpased: 2.555
batch start
#iterations: 394
currently lose_sum: 95.33259892463684
time_elpased: 2.539
batch start
#iterations: 395
currently lose_sum: 95.11411613225937
time_elpased: 2.517
batch start
#iterations: 396
currently lose_sum: 95.22979038953781
time_elpased: 2.634
batch start
#iterations: 397
currently lose_sum: 95.68931365013123
time_elpased: 2.673
batch start
#iterations: 398
currently lose_sum: 95.33348870277405
time_elpased: 2.683
batch start
#iterations: 399
currently lose_sum: 95.6200864315033
time_elpased: 2.557
start validation test
0.649948453608
0.62438360823
0.755788823711
0.683830718376
0.649762634514
61.391
acc: 0.650
pre: 0.619
rec: 0.784
F1: 0.692
auc: 0.689
