start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 101.32454204559326
time_elpased: 5.106
batch start
#iterations: 1
currently lose_sum: 100.41497325897217
time_elpased: 4.627
batch start
#iterations: 2
currently lose_sum: 100.40571826696396
time_elpased: 5.466
batch start
#iterations: 3
currently lose_sum: 100.27302092313766
time_elpased: 4.634
batch start
#iterations: 4
currently lose_sum: 100.32931607961655
time_elpased: 3.416
batch start
#iterations: 5
currently lose_sum: 100.2280016541481
time_elpased: 3.728
batch start
#iterations: 6
currently lose_sum: 100.06776863336563
time_elpased: 4.36
batch start
#iterations: 7
currently lose_sum: 100.12797355651855
time_elpased: 4.26
batch start
#iterations: 8
currently lose_sum: 100.02370035648346
time_elpased: 4.66
batch start
#iterations: 9
currently lose_sum: 99.93343168497086
time_elpased: 5.336
batch start
#iterations: 10
currently lose_sum: 99.92553961277008
time_elpased: 4.9
batch start
#iterations: 11
currently lose_sum: 99.80708104372025
time_elpased: 4.408
batch start
#iterations: 12
currently lose_sum: 99.46804529428482
time_elpased: 4.735
batch start
#iterations: 13
currently lose_sum: 99.59851485490799
time_elpased: 3.468
batch start
#iterations: 14
currently lose_sum: 99.16915309429169
time_elpased: 3.553
batch start
#iterations: 15
currently lose_sum: 99.12970525026321
time_elpased: 4.525
batch start
#iterations: 16
currently lose_sum: 99.22597968578339
time_elpased: 3.772
batch start
#iterations: 17
currently lose_sum: 98.86053550243378
time_elpased: 5.198
batch start
#iterations: 18
currently lose_sum: 98.83023589849472
time_elpased: 5.848
batch start
#iterations: 19
currently lose_sum: 98.93618983030319
time_elpased: 5.542
start validation test
0.553041237113
0.644634955752
0.239888854585
0.349658741469
0.553591024421
65.526
batch start
#iterations: 20
currently lose_sum: 98.82084786891937
time_elpased: 5.887
batch start
#iterations: 21
currently lose_sum: 98.56838423013687
time_elpased: 5.164
batch start
#iterations: 22
currently lose_sum: 98.48336952924728
time_elpased: 5.192
batch start
#iterations: 23
currently lose_sum: 98.49111276865005
time_elpased: 4.268
batch start
#iterations: 24
currently lose_sum: 98.32814502716064
time_elpased: 4.748
batch start
#iterations: 25
currently lose_sum: 98.21476191282272
time_elpased: 5.548
batch start
#iterations: 26
currently lose_sum: 98.36508029699326
time_elpased: 4.088
batch start
#iterations: 27
currently lose_sum: 97.81100934743881
time_elpased: 5.192
batch start
#iterations: 28
currently lose_sum: 98.01517468690872
time_elpased: 4.104
batch start
#iterations: 29
currently lose_sum: 97.88614130020142
time_elpased: 4.184
batch start
#iterations: 30
currently lose_sum: 98.00442481040955
time_elpased: 5.306
batch start
#iterations: 31
currently lose_sum: 97.8733047246933
time_elpased: 3.508
batch start
#iterations: 32
currently lose_sum: 97.97543734312057
time_elpased: 3.089
batch start
#iterations: 33
currently lose_sum: 97.82052582502365
time_elpased: 3.269
batch start
#iterations: 34
currently lose_sum: 97.67432534694672
time_elpased: 3.404
batch start
#iterations: 35
currently lose_sum: 97.6438500881195
time_elpased: 4.477
batch start
#iterations: 36
currently lose_sum: 97.52112376689911
time_elpased: 2.946
batch start
#iterations: 37
currently lose_sum: 97.67405790090561
time_elpased: 3.069
batch start
#iterations: 38
currently lose_sum: 97.41749405860901
time_elpased: 3.875
batch start
#iterations: 39
currently lose_sum: 97.5262199640274
time_elpased: 9.494
start validation test
0.64587628866
0.63211136891
0.700936503036
0.664747218427
0.645779621961
62.880
batch start
#iterations: 40
currently lose_sum: 97.4288986325264
time_elpased: 2.96
batch start
#iterations: 41
currently lose_sum: 97.05765801668167
time_elpased: 3.256
batch start
#iterations: 42
currently lose_sum: 97.42451459169388
time_elpased: 3.73
batch start
#iterations: 43
currently lose_sum: 97.41550928354263
time_elpased: 4.841
batch start
#iterations: 44
currently lose_sum: 97.22773081064224
time_elpased: 3.313
batch start
#iterations: 45
currently lose_sum: 97.51150894165039
time_elpased: 3.863
batch start
#iterations: 46
currently lose_sum: 97.15501302480698
time_elpased: 5.11
batch start
#iterations: 47
currently lose_sum: 97.36672949790955
time_elpased: 4.205
batch start
#iterations: 48
currently lose_sum: 97.30085867643356
time_elpased: 3.993
batch start
#iterations: 49
currently lose_sum: 97.12003529071808
time_elpased: 2.94
batch start
#iterations: 50
currently lose_sum: 97.16678267717361
time_elpased: 2.747
batch start
#iterations: 51
currently lose_sum: 97.13874566555023
time_elpased: 2.586
batch start
#iterations: 52
currently lose_sum: 96.95789635181427
time_elpased: 2.626
batch start
#iterations: 53
currently lose_sum: 96.66166412830353
time_elpased: 2.683
batch start
#iterations: 54
currently lose_sum: 97.25059694051743
time_elpased: 2.431
batch start
#iterations: 55
currently lose_sum: 97.1869387626648
time_elpased: 2.416
batch start
#iterations: 56
currently lose_sum: 97.0473917722702
time_elpased: 2.341
batch start
#iterations: 57
currently lose_sum: 97.15232229232788
time_elpased: 2.048
batch start
#iterations: 58
currently lose_sum: 96.98937314748764
time_elpased: 1.987
batch start
#iterations: 59
currently lose_sum: 97.51316499710083
time_elpased: 2.043
start validation test
0.634845360825
0.640967983724
0.616033755274
0.62825356843
0.6348783875
62.876
batch start
#iterations: 60
currently lose_sum: 96.97943931818008
time_elpased: 2.407
batch start
#iterations: 61
currently lose_sum: 97.3242409825325
time_elpased: 2.215
batch start
#iterations: 62
currently lose_sum: 96.86090177297592
time_elpased: 2.355
batch start
#iterations: 63
currently lose_sum: 96.9593363404274
time_elpased: 2.52
batch start
#iterations: 64
currently lose_sum: 97.17145204544067
time_elpased: 2.406
batch start
#iterations: 65
currently lose_sum: 96.42445212602615
time_elpased: 3.033
batch start
#iterations: 66
currently lose_sum: 96.97606265544891
time_elpased: 2.239
batch start
#iterations: 67
currently lose_sum: 96.55515104532242
time_elpased: 2.43
batch start
#iterations: 68
currently lose_sum: 96.75837177038193
time_elpased: 2.601
batch start
#iterations: 69
currently lose_sum: 96.84155517816544
time_elpased: 2.398
batch start
#iterations: 70
currently lose_sum: 96.97482514381409
time_elpased: 2.6
batch start
#iterations: 71
currently lose_sum: 96.83969420194626
time_elpased: 2.55
batch start
#iterations: 72
currently lose_sum: 96.77267169952393
time_elpased: 2.61
batch start
#iterations: 73
currently lose_sum: 96.62776237726212
time_elpased: 2.53
batch start
#iterations: 74
currently lose_sum: 96.68349826335907
time_elpased: 2.539
batch start
#iterations: 75
currently lose_sum: 96.76158875226974
time_elpased: 2.581
batch start
#iterations: 76
currently lose_sum: 96.79048335552216
time_elpased: 2.448
batch start
#iterations: 77
currently lose_sum: 96.88964486122131
time_elpased: 2.443
batch start
#iterations: 78
currently lose_sum: 96.64255046844482
time_elpased: 2.38
batch start
#iterations: 79
currently lose_sum: 96.70915883779526
time_elpased: 2.232
start validation test
0.64881443299
0.634320074006
0.705670474426
0.668095678862
0.64871461344
62.247
batch start
#iterations: 80
currently lose_sum: 96.84373164176941
time_elpased: 2.157
batch start
#iterations: 81
currently lose_sum: 96.29799610376358
time_elpased: 2.101
batch start
#iterations: 82
currently lose_sum: 96.63624286651611
time_elpased: 2.15
batch start
#iterations: 83
currently lose_sum: 96.69721776247025
time_elpased: 2.264
batch start
#iterations: 84
currently lose_sum: 96.77774858474731
time_elpased: 2.364
batch start
#iterations: 85
currently lose_sum: 96.73995852470398
time_elpased: 2.174
batch start
#iterations: 86
currently lose_sum: 96.5749563574791
time_elpased: 2.268
batch start
#iterations: 87
currently lose_sum: 96.55693244934082
time_elpased: 2.481
batch start
#iterations: 88
currently lose_sum: 96.43749988079071
time_elpased: 2.272
batch start
#iterations: 89
currently lose_sum: 96.473184466362
time_elpased: 2.2
batch start
#iterations: 90
currently lose_sum: 96.34022790193558
time_elpased: 2.477
batch start
#iterations: 91
currently lose_sum: 96.56119745969772
time_elpased: 2.302
batch start
#iterations: 92
currently lose_sum: 96.42400074005127
time_elpased: 2.168
batch start
#iterations: 93
currently lose_sum: 96.52876901626587
time_elpased: 2.156
batch start
#iterations: 94
currently lose_sum: 96.55986374616623
time_elpased: 2.238
batch start
#iterations: 95
currently lose_sum: 96.65343713760376
time_elpased: 2.495
batch start
#iterations: 96
currently lose_sum: 96.68395274877548
time_elpased: 2.724
batch start
#iterations: 97
currently lose_sum: 96.62589281797409
time_elpased: 3.155
batch start
#iterations: 98
currently lose_sum: 96.62759029865265
time_elpased: 3.186
batch start
#iterations: 99
currently lose_sum: 96.41317510604858
time_elpased: 3.047
start validation test
0.652113402062
0.625698797222
0.760214057837
0.686428471867
0.651923614687
61.931
batch start
#iterations: 100
currently lose_sum: 96.54557192325592
time_elpased: 2.947
batch start
#iterations: 101
currently lose_sum: 96.67571705579758
time_elpased: 3.056
batch start
#iterations: 102
currently lose_sum: 96.2697182893753
time_elpased: 2.82
batch start
#iterations: 103
currently lose_sum: 96.31674993038177
time_elpased: 2.8
batch start
#iterations: 104
currently lose_sum: 96.20877021551132
time_elpased: 2.869
batch start
#iterations: 105
currently lose_sum: 96.49956405162811
time_elpased: 3.347
batch start
#iterations: 106
currently lose_sum: 96.71555083990097
time_elpased: 3.338
batch start
#iterations: 107
currently lose_sum: 96.15724754333496
time_elpased: 3.505
batch start
#iterations: 108
currently lose_sum: 96.56984877586365
time_elpased: 3.729
batch start
#iterations: 109
currently lose_sum: 96.4218635559082
time_elpased: 3.097
batch start
#iterations: 110
currently lose_sum: 96.60923743247986
time_elpased: 3.494
batch start
#iterations: 111
currently lose_sum: 96.53003007173538
time_elpased: 3.295
batch start
#iterations: 112
currently lose_sum: 96.25711303949356
time_elpased: 2.875
batch start
#iterations: 113
currently lose_sum: 95.81175565719604
time_elpased: 3.073
batch start
#iterations: 114
currently lose_sum: 96.64196348190308
time_elpased: 2.964
batch start
#iterations: 115
currently lose_sum: 96.27296698093414
time_elpased: 2.68
batch start
#iterations: 116
currently lose_sum: 96.1698585152626
time_elpased: 2.723
batch start
#iterations: 117
currently lose_sum: 96.2465346455574
time_elpased: 3.081
batch start
#iterations: 118
currently lose_sum: 96.4492295384407
time_elpased: 2.904
batch start
#iterations: 119
currently lose_sum: 96.10260498523712
time_elpased: 2.833
start validation test
0.652628865979
0.619598393574
0.793866419677
0.695989533992
0.652380901669
61.716
batch start
#iterations: 120
currently lose_sum: 96.17505621910095
time_elpased: 3.141
batch start
#iterations: 121
currently lose_sum: 96.15476679801941
time_elpased: 2.79
batch start
#iterations: 122
currently lose_sum: 96.255506336689
time_elpased: 2.356
batch start
#iterations: 123
currently lose_sum: 96.48756992816925
time_elpased: 5.052
batch start
#iterations: 124
currently lose_sum: 96.1713296175003
time_elpased: 4.118
batch start
#iterations: 125
currently lose_sum: 96.30925130844116
time_elpased: 4.167
batch start
#iterations: 126
currently lose_sum: 96.1971286535263
time_elpased: 3.038
batch start
#iterations: 127
currently lose_sum: 96.37929159402847
time_elpased: 3.162
batch start
#iterations: 128
currently lose_sum: 96.11635196208954
time_elpased: 2.947
batch start
#iterations: 129
currently lose_sum: 95.92200273275375
time_elpased: 7.89
batch start
#iterations: 130
currently lose_sum: 96.38351660966873
time_elpased: 3.167
batch start
#iterations: 131
currently lose_sum: 96.30645161867142
time_elpased: 4.031
batch start
#iterations: 132
currently lose_sum: 96.35217928886414
time_elpased: 2.625
batch start
#iterations: 133
currently lose_sum: 95.79765295982361
time_elpased: 2.621
batch start
#iterations: 134
currently lose_sum: 96.15572553873062
time_elpased: 2.588
batch start
#iterations: 135
currently lose_sum: 96.3404552936554
time_elpased: 2.843
batch start
#iterations: 136
currently lose_sum: 96.36466079950333
time_elpased: 2.533
batch start
#iterations: 137
currently lose_sum: 96.28864395618439
time_elpased: 2.561
batch start
#iterations: 138
currently lose_sum: 96.16902309656143
time_elpased: 2.582
batch start
#iterations: 139
currently lose_sum: 95.92969536781311
time_elpased: 2.669
start validation test
0.652525773196
0.621192376609
0.784913039004
0.693521254831
0.652293346931
61.481
batch start
#iterations: 140
currently lose_sum: 95.54486393928528
time_elpased: 2.655
batch start
#iterations: 141
currently lose_sum: 96.58829164505005
time_elpased: 2.521
batch start
#iterations: 142
currently lose_sum: 95.94030219316483
time_elpased: 2.715
batch start
#iterations: 143
currently lose_sum: 96.05898696184158
time_elpased: 2.491
batch start
#iterations: 144
currently lose_sum: 95.93282794952393
time_elpased: 2.273
batch start
#iterations: 145
currently lose_sum: 95.84280532598495
time_elpased: 2.191
batch start
#iterations: 146
currently lose_sum: 96.01658314466476
time_elpased: 2.129
batch start
#iterations: 147
currently lose_sum: 95.91763210296631
time_elpased: 2.02
batch start
#iterations: 148
currently lose_sum: 95.99434852600098
time_elpased: 2.262
batch start
#iterations: 149
currently lose_sum: 95.69602829217911
time_elpased: 2.235
batch start
#iterations: 150
currently lose_sum: 96.15477162599564
time_elpased: 2.113
batch start
#iterations: 151
currently lose_sum: 95.83370941877365
time_elpased: 2.269
batch start
#iterations: 152
currently lose_sum: 95.89763134717941
time_elpased: 2.315
batch start
#iterations: 153
currently lose_sum: 95.87895286083221
time_elpased: 2.256
batch start
#iterations: 154
currently lose_sum: 96.0445704460144
time_elpased: 2.293
batch start
#iterations: 155
currently lose_sum: 96.0300424695015
time_elpased: 2.229
batch start
#iterations: 156
currently lose_sum: 95.95805758237839
time_elpased: 2.106
batch start
#iterations: 157
currently lose_sum: 96.15545922517776
time_elpased: 2.029
batch start
#iterations: 158
currently lose_sum: 96.0453662276268
time_elpased: 2.08
batch start
#iterations: 159
currently lose_sum: 95.90587520599365
time_elpased: 2.107
start validation test
0.653556701031
0.624646363788
0.77256354842
0.690775247297
0.653347766155
61.354
batch start
#iterations: 160
currently lose_sum: 96.0260192155838
time_elpased: 2.169
batch start
#iterations: 161
currently lose_sum: 95.80236738920212
time_elpased: 2.347
batch start
#iterations: 162
currently lose_sum: 95.74493718147278
time_elpased: 2.468
batch start
#iterations: 163
currently lose_sum: 95.7918296456337
time_elpased: 2.591
batch start
#iterations: 164
currently lose_sum: 95.93352049589157
time_elpased: 2.522
batch start
#iterations: 165
currently lose_sum: 95.64276844263077
time_elpased: 2.633
batch start
#iterations: 166
currently lose_sum: 95.64441043138504
time_elpased: 2.825
batch start
#iterations: 167
currently lose_sum: 96.00693494081497
time_elpased: 2.65
batch start
#iterations: 168
currently lose_sum: 95.9712108373642
time_elpased: 2.718
batch start
#iterations: 169
currently lose_sum: 95.89608013629913
time_elpased: 2.701
batch start
#iterations: 170
currently lose_sum: 95.72924202680588
time_elpased: 2.582
batch start
#iterations: 171
currently lose_sum: 95.41263365745544
time_elpased: 2.612
batch start
#iterations: 172
currently lose_sum: 95.77924406528473
time_elpased: 2.562
batch start
#iterations: 173
currently lose_sum: 95.74782717227936
time_elpased: 62.547
batch start
#iterations: 174
currently lose_sum: 95.83844685554504
time_elpased: 2.754
batch start
#iterations: 175
currently lose_sum: 95.93330472707748
time_elpased: 2.9
batch start
#iterations: 176
currently lose_sum: 95.94139009714127
time_elpased: 2.913
batch start
#iterations: 177
currently lose_sum: 95.64186710119247
time_elpased: 3.05
batch start
#iterations: 178
currently lose_sum: 95.61996281147003
time_elpased: 3.169
batch start
#iterations: 179
currently lose_sum: 95.8920396566391
time_elpased: 3.311
start validation test
0.652319587629
0.615033286887
0.81763918905
0.702010161255
0.65202934357
61.473
batch start
#iterations: 180
currently lose_sum: 95.61840718984604
time_elpased: 3.184
batch start
#iterations: 181
currently lose_sum: 95.78971081972122
time_elpased: 4.428
batch start
#iterations: 182
currently lose_sum: 95.90425312519073
time_elpased: 4.228
batch start
#iterations: 183
currently lose_sum: 95.42584466934204
time_elpased: 3.612
batch start
#iterations: 184
currently lose_sum: 95.66038906574249
time_elpased: 3.606
batch start
#iterations: 185
currently lose_sum: 95.64282202720642
time_elpased: 4.7
batch start
#iterations: 186
currently lose_sum: 95.77397400140762
time_elpased: 4.656
batch start
#iterations: 187
currently lose_sum: 96.06063544750214
time_elpased: 4.344
batch start
#iterations: 188
currently lose_sum: 95.81750524044037
time_elpased: 4.252
batch start
#iterations: 189
currently lose_sum: 95.58960956335068
time_elpased: 4.21
batch start
#iterations: 190
currently lose_sum: 95.58667916059494
time_elpased: 4.348
batch start
#iterations: 191
currently lose_sum: 95.60750955343246
time_elpased: 3.712
batch start
#iterations: 192
currently lose_sum: 96.00610017776489
time_elpased: 5.425
batch start
#iterations: 193
currently lose_sum: 95.67204213142395
time_elpased: 5.318
batch start
#iterations: 194
currently lose_sum: 95.49788320064545
time_elpased: 5.33
batch start
#iterations: 195
currently lose_sum: 95.72178417444229
time_elpased: 5.691
batch start
#iterations: 196
currently lose_sum: 95.73868334293365
time_elpased: 4.585
batch start
#iterations: 197
currently lose_sum: 95.67466133832932
time_elpased: 3.619
batch start
#iterations: 198
currently lose_sum: 95.19745570421219
time_elpased: 3.44
batch start
#iterations: 199
currently lose_sum: 95.82284933328629
time_elpased: 3.567
start validation test
0.650773195876
0.636128076994
0.707419985592
0.669882570774
0.6506737437
61.541
batch start
#iterations: 200
currently lose_sum: 95.85212302207947
time_elpased: 4.552
batch start
#iterations: 201
currently lose_sum: 95.90446150302887
time_elpased: 5.144
batch start
#iterations: 202
currently lose_sum: 95.1899721622467
time_elpased: 4.229
batch start
#iterations: 203
currently lose_sum: 95.83158975839615
time_elpased: 4.897
batch start
#iterations: 204
currently lose_sum: 95.53968966007233
time_elpased: 4.275
batch start
#iterations: 205
currently lose_sum: 95.57121968269348
time_elpased: 5.525
batch start
#iterations: 206
currently lose_sum: 95.60262060165405
time_elpased: 4.566
batch start
#iterations: 207
currently lose_sum: 95.61692863702774
time_elpased: 5.382
batch start
#iterations: 208
currently lose_sum: 95.23455768823624
time_elpased: 5.496
batch start
#iterations: 209
currently lose_sum: 95.43232023715973
time_elpased: 5.575
batch start
#iterations: 210
currently lose_sum: 95.3351821899414
time_elpased: 5.127
batch start
#iterations: 211
currently lose_sum: 95.4888688325882
time_elpased: 5.602
batch start
#iterations: 212
currently lose_sum: 95.19766557216644
time_elpased: 3.802
batch start
#iterations: 213
currently lose_sum: 95.60753691196442
time_elpased: 3.874
batch start
#iterations: 214
currently lose_sum: 95.43632566928864
time_elpased: 4.934
batch start
#iterations: 215
currently lose_sum: 95.61770164966583
time_elpased: 3.994
batch start
#iterations: 216
currently lose_sum: 95.35021322965622
time_elpased: 5.012
batch start
#iterations: 217
currently lose_sum: 95.58759462833405
time_elpased: 6.352
batch start
#iterations: 218
currently lose_sum: 95.42818856239319
time_elpased: 4.771
batch start
#iterations: 219
currently lose_sum: 95.49481385946274
time_elpased: 4.58
start validation test
0.655721649485
0.623275442298
0.790367397345
0.696946322428
0.655485258106
61.175
batch start
#iterations: 220
currently lose_sum: 95.42781591415405
time_elpased: 7.499
batch start
#iterations: 221
currently lose_sum: 95.31689208745956
time_elpased: 2.753
batch start
#iterations: 222
currently lose_sum: 95.45663249492645
time_elpased: 2.743
batch start
#iterations: 223
currently lose_sum: 95.547616481781
time_elpased: 2.973
batch start
#iterations: 224
currently lose_sum: 95.53557682037354
time_elpased: 3.588
batch start
#iterations: 225
currently lose_sum: 95.47337543964386
time_elpased: 3.334
batch start
#iterations: 226
currently lose_sum: 95.08030545711517
time_elpased: 3.849
batch start
#iterations: 227
currently lose_sum: 95.53755247592926
time_elpased: 3.865
batch start
#iterations: 228
currently lose_sum: 95.13756960630417
time_elpased: 4.621
batch start
#iterations: 229
currently lose_sum: 95.61344057321548
time_elpased: 4.032
batch start
#iterations: 230
currently lose_sum: 95.5806674361229
time_elpased: 3.736
batch start
#iterations: 231
currently lose_sum: 95.28732889890671
time_elpased: 4.038
batch start
#iterations: 232
currently lose_sum: 95.34665781259537
time_elpased: 3.906
batch start
#iterations: 233
currently lose_sum: 94.92487698793411
time_elpased: 3.106
batch start
#iterations: 234
currently lose_sum: 95.68406909704208
time_elpased: 3.334
batch start
#iterations: 235
currently lose_sum: 95.29645198583603
time_elpased: 3.553
batch start
#iterations: 236
currently lose_sum: 95.36138409376144
time_elpased: 3.284
batch start
#iterations: 237
currently lose_sum: 95.49351251125336
time_elpased: 4.415
batch start
#iterations: 238
currently lose_sum: 95.15212732553482
time_elpased: 4.1
batch start
#iterations: 239
currently lose_sum: 95.29996544122696
time_elpased: 3.24
start validation test
0.654690721649
0.623749384943
0.782751878152
0.694263155493
0.654465890537
61.235
batch start
#iterations: 240
currently lose_sum: 95.61173403263092
time_elpased: 3.672
batch start
#iterations: 241
currently lose_sum: 95.29022133350372
time_elpased: 3.175
batch start
#iterations: 242
currently lose_sum: 95.14133512973785
time_elpased: 2.951
batch start
#iterations: 243
currently lose_sum: 95.41365146636963
time_elpased: 4.325
batch start
#iterations: 244
currently lose_sum: 95.61353451013565
time_elpased: 4.828
batch start
#iterations: 245
currently lose_sum: 95.39242869615555
time_elpased: 4.943
batch start
#iterations: 246
currently lose_sum: 95.67162454128265
time_elpased: 5.35
batch start
#iterations: 247
currently lose_sum: 95.17179137468338
time_elpased: 5.265
batch start
#iterations: 248
currently lose_sum: 95.48172217607498
time_elpased: 5.491
batch start
#iterations: 249
currently lose_sum: 95.40497976541519
time_elpased: 5.441
batch start
#iterations: 250
currently lose_sum: 95.19363582134247
time_elpased: 4.309
batch start
#iterations: 251
currently lose_sum: 95.08979523181915
time_elpased: 4.224
batch start
#iterations: 252
currently lose_sum: 95.30612713098526
time_elpased: 3.927
batch start
#iterations: 253
currently lose_sum: 94.88078546524048
time_elpased: 3.753
batch start
#iterations: 254
currently lose_sum: 95.24564003944397
time_elpased: 3.865
batch start
#iterations: 255
currently lose_sum: 95.11966639757156
time_elpased: 3.795
batch start
#iterations: 256
currently lose_sum: 95.11469215154648
time_elpased: 5.696
batch start
#iterations: 257
currently lose_sum: 95.58376026153564
time_elpased: 5.091
batch start
#iterations: 258
currently lose_sum: 94.84819322824478
time_elpased: 4.218
batch start
#iterations: 259
currently lose_sum: 95.4276990890503
time_elpased: 5.282
start validation test
0.649381443299
0.610626185958
0.827930431203
0.702865629914
0.649067973011
61.029
batch start
#iterations: 260
currently lose_sum: 95.48224425315857
time_elpased: 4.831
batch start
#iterations: 261
currently lose_sum: 95.08513778448105
time_elpased: 4.414
batch start
#iterations: 262
currently lose_sum: 95.05746775865555
time_elpased: 3.409
batch start
#iterations: 263
currently lose_sum: 95.13967609405518
time_elpased: 4.112
batch start
#iterations: 264
currently lose_sum: 95.43098646402359
time_elpased: 3.937
batch start
#iterations: 265
currently lose_sum: 95.16184985637665
time_elpased: 4.178
batch start
#iterations: 266
currently lose_sum: 95.16008216142654
time_elpased: 4.168
batch start
#iterations: 267
currently lose_sum: 95.31532806158066
time_elpased: 5.05
batch start
#iterations: 268
currently lose_sum: 95.51788938045502
time_elpased: 4.692
batch start
#iterations: 269
currently lose_sum: 95.42441016435623
time_elpased: 4.745
batch start
#iterations: 270
currently lose_sum: 95.01649451255798
time_elpased: 5.062
batch start
#iterations: 271
currently lose_sum: 95.16418892145157
time_elpased: 3.658
batch start
#iterations: 272
currently lose_sum: 95.42597860097885
time_elpased: 5.356
batch start
#iterations: 273
currently lose_sum: 95.06242138147354
time_elpased: 5.088
batch start
#iterations: 274
currently lose_sum: 95.20760160684586
time_elpased: 5.304
batch start
#iterations: 275
currently lose_sum: 95.04635506868362
time_elpased: 5.391
batch start
#iterations: 276
currently lose_sum: 95.49086582660675
time_elpased: 5.655
batch start
#iterations: 277
currently lose_sum: 95.22490471601486
time_elpased: 5.128
batch start
#iterations: 278
currently lose_sum: 95.43345433473587
time_elpased: 5.302
batch start
#iterations: 279
currently lose_sum: 95.05124014616013
time_elpased: 4.053
start validation test
0.652216494845
0.618553408909
0.797365442009
0.696668614845
0.651961663481
61.268
batch start
#iterations: 280
currently lose_sum: 95.22791028022766
time_elpased: 5.153
batch start
#iterations: 281
currently lose_sum: 94.94017833471298
time_elpased: 4.076
batch start
#iterations: 282
currently lose_sum: 94.97135764360428
time_elpased: 3.555
batch start
#iterations: 283
currently lose_sum: 94.87116652727127
time_elpased: 4.945
batch start
#iterations: 284
currently lose_sum: 94.93665838241577
time_elpased: 4.767
batch start
#iterations: 285
currently lose_sum: 95.10739177465439
time_elpased: 4.08
batch start
#iterations: 286
currently lose_sum: 95.04419058561325
time_elpased: 3.722
batch start
#iterations: 287
currently lose_sum: 95.02135556936264
time_elpased: 4.179
batch start
#iterations: 288
currently lose_sum: 95.1320880651474
time_elpased: 3.949
batch start
#iterations: 289
currently lose_sum: 95.10058706998825
time_elpased: 4.137
batch start
#iterations: 290
currently lose_sum: 95.20380002260208
time_elpased: 3.918
batch start
#iterations: 291
currently lose_sum: 94.63552558422089
time_elpased: 4.644
batch start
#iterations: 292
currently lose_sum: 95.30217534303665
time_elpased: 6.222
batch start
#iterations: 293
currently lose_sum: 94.8439593911171
time_elpased: 4.34
batch start
#iterations: 294
currently lose_sum: 94.68883395195007
time_elpased: 6.145
batch start
#iterations: 295
currently lose_sum: 95.02445393800735
time_elpased: 4.947
batch start
#iterations: 296
currently lose_sum: 95.15240550041199
time_elpased: 3.548
batch start
#iterations: 297
currently lose_sum: 95.5646516084671
time_elpased: 3.726
batch start
#iterations: 298
currently lose_sum: 94.7536838054657
time_elpased: 3.532
batch start
#iterations: 299
currently lose_sum: 95.27881461381912
time_elpased: 3.194
start validation test
0.645360824742
0.600581436574
0.871668210353
0.711167086482
0.644963507221
61.777
batch start
#iterations: 300
currently lose_sum: 95.17879062891006
time_elpased: 3.787
batch start
#iterations: 301
currently lose_sum: 95.45645362138748
time_elpased: 3.328
batch start
#iterations: 302
currently lose_sum: 94.96928280591965
time_elpased: 3.816
batch start
#iterations: 303
currently lose_sum: 95.12996280193329
time_elpased: 3.003
batch start
#iterations: 304
currently lose_sum: 95.13013124465942
time_elpased: 3.046
batch start
#iterations: 305
currently lose_sum: 95.41600263118744
time_elpased: 3.803
batch start
#iterations: 306
currently lose_sum: 94.80596649646759
time_elpased: 3.266
batch start
#iterations: 307
currently lose_sum: 95.00417840480804
time_elpased: 3.687
batch start
#iterations: 308
currently lose_sum: 94.99549287557602
time_elpased: 3.551
batch start
#iterations: 309
currently lose_sum: 94.99937999248505
time_elpased: 3.511
batch start
#iterations: 310
currently lose_sum: 95.19107753038406
time_elpased: 4.288
batch start
#iterations: 311
currently lose_sum: 95.1499633193016
time_elpased: 3.364
batch start
#iterations: 312
currently lose_sum: 94.97414201498032
time_elpased: 3.618
batch start
#iterations: 313
currently lose_sum: 95.35187059640884
time_elpased: 3.918
batch start
#iterations: 314
currently lose_sum: 94.59188961982727
time_elpased: 3.182
batch start
#iterations: 315
currently lose_sum: 94.61545914411545
time_elpased: 4.199
batch start
#iterations: 316
currently lose_sum: 95.3198733329773
time_elpased: 4.998
batch start
#iterations: 317
currently lose_sum: 94.93655008077621
time_elpased: 5.008
batch start
#iterations: 318
currently lose_sum: 94.59679591655731
time_elpased: 5.124
batch start
#iterations: 319
currently lose_sum: 95.22703170776367
time_elpased: 4.804
start validation test
0.655257731959
0.621539202311
0.797056704744
0.698439895392
0.655008781991
61.001
batch start
#iterations: 320
currently lose_sum: 94.74771201610565
time_elpased: 5.387
batch start
#iterations: 321
currently lose_sum: 94.83166664838791
time_elpased: 5.383
batch start
#iterations: 322
currently lose_sum: 94.9827212691307
time_elpased: 4.44
batch start
#iterations: 323
currently lose_sum: 95.11796486377716
time_elpased: 5.374
batch start
#iterations: 324
currently lose_sum: 95.02518856525421
time_elpased: 3.987
batch start
#iterations: 325
currently lose_sum: 95.0234808921814
time_elpased: 4.646
batch start
#iterations: 326
currently lose_sum: 95.1095632314682
time_elpased: 5.671
batch start
#iterations: 327
currently lose_sum: 95.00768303871155
time_elpased: 5.449
batch start
#iterations: 328
currently lose_sum: 94.80383884906769
time_elpased: 4.808
batch start
#iterations: 329
currently lose_sum: 95.21302115917206
time_elpased: 4.884
batch start
#iterations: 330
currently lose_sum: 95.13748908042908
time_elpased: 4.922
batch start
#iterations: 331
currently lose_sum: 95.11838465929031
time_elpased: 5.737
batch start
#iterations: 332
currently lose_sum: 94.8017578125
time_elpased: 5.736
batch start
#iterations: 333
currently lose_sum: 95.04570376873016
time_elpased: 5.805
batch start
#iterations: 334
currently lose_sum: 95.09562003612518
time_elpased: 4.05
batch start
#iterations: 335
currently lose_sum: 94.72602832317352
time_elpased: 4.31
batch start
#iterations: 336
currently lose_sum: 95.18159115314484
time_elpased: 4.246
batch start
#iterations: 337
currently lose_sum: 94.6358671784401
time_elpased: 5.237
batch start
#iterations: 338
currently lose_sum: 95.16785699129105
time_elpased: 5.379
batch start
#iterations: 339
currently lose_sum: 94.92532932758331
time_elpased: 5.227
start validation test
0.642422680412
0.633474169387
0.678913244829
0.655407083602
0.642358615598
61.316
batch start
#iterations: 340
currently lose_sum: 94.88571590185165
time_elpased: 6.069
batch start
#iterations: 341
currently lose_sum: 95.38027673959732
time_elpased: 4.728
batch start
#iterations: 342
currently lose_sum: 95.04913783073425
time_elpased: 3.436
batch start
#iterations: 343
currently lose_sum: 95.03234273195267
time_elpased: 3.924
batch start
#iterations: 344
currently lose_sum: 94.79505634307861
time_elpased: 4.991
batch start
#iterations: 345
currently lose_sum: 95.0765917301178
time_elpased: 5.297
batch start
#iterations: 346
currently lose_sum: 94.5565036535263
time_elpased: 5.007
batch start
#iterations: 347
currently lose_sum: 95.03141671419144
time_elpased: 4.038
batch start
#iterations: 348
currently lose_sum: 94.87434357404709
time_elpased: 4.665
batch start
#iterations: 349
currently lose_sum: 94.73126965761185
time_elpased: 4.422
batch start
#iterations: 350
currently lose_sum: 95.08859837055206
time_elpased: 3.732
batch start
#iterations: 351
currently lose_sum: 94.73615169525146
time_elpased: 3.386
batch start
#iterations: 352
currently lose_sum: 94.8924451470375
time_elpased: 3.392
batch start
#iterations: 353
currently lose_sum: 94.67411142587662
time_elpased: 3.478
batch start
#iterations: 354
currently lose_sum: 94.80051070451736
time_elpased: 3.985
batch start
#iterations: 355
currently lose_sum: 94.86078333854675
time_elpased: 4.336
batch start
#iterations: 356
currently lose_sum: 95.04565870761871
time_elpased: 4.651
batch start
#iterations: 357
currently lose_sum: 94.50906205177307
time_elpased: 4.369
batch start
#iterations: 358
currently lose_sum: 94.35734939575195
time_elpased: 4.947
batch start
#iterations: 359
currently lose_sum: 94.9583540558815
time_elpased: 5.689
start validation test
0.651494845361
0.629831342235
0.737882062365
0.679588645088
0.651343179277
61.122
batch start
#iterations: 360
currently lose_sum: 94.9565948843956
time_elpased: 4.49
batch start
#iterations: 361
currently lose_sum: 95.09696173667908
time_elpased: 4.728
batch start
#iterations: 362
currently lose_sum: 95.13253992795944
time_elpased: 5.156
batch start
#iterations: 363
currently lose_sum: 95.16603153944016
time_elpased: 4.152
batch start
#iterations: 364
currently lose_sum: 94.78736078739166
time_elpased: 4.311
batch start
#iterations: 365
currently lose_sum: 94.95482724905014
time_elpased: 5.182
batch start
#iterations: 366
currently lose_sum: 95.02315747737885
time_elpased: 5.118
batch start
#iterations: 367
currently lose_sum: 94.763208091259
time_elpased: 5.63
batch start
#iterations: 368
currently lose_sum: 94.81871950626373
time_elpased: 5.387
batch start
#iterations: 369
currently lose_sum: 94.98666495084763
time_elpased: 4.892
batch start
#iterations: 370
currently lose_sum: 94.6834664940834
time_elpased: 4.438
batch start
#iterations: 371
currently lose_sum: 95.07696431875229
time_elpased: 5.642
batch start
#iterations: 372
currently lose_sum: 94.66978877782822
time_elpased: 4.798
batch start
#iterations: 373
currently lose_sum: 95.29999828338623
time_elpased: 5.764
batch start
#iterations: 374
currently lose_sum: 94.91452610492706
time_elpased: 6.28
batch start
#iterations: 375
currently lose_sum: 94.79988020658493
time_elpased: 4.728
batch start
#iterations: 376
currently lose_sum: 94.90021109580994
time_elpased: 4.26
batch start
#iterations: 377
currently lose_sum: 95.15717154741287
time_elpased: 4.254
batch start
#iterations: 378
currently lose_sum: 94.71700459718704
time_elpased: 4.097
batch start
#iterations: 379
currently lose_sum: 95.25746244192123
time_elpased: 3.381
start validation test
0.62881443299
0.64453125
0.577338684779
0.609087454536
0.628904806605
61.539
batch start
#iterations: 380
currently lose_sum: 94.63313484191895
time_elpased: 3.78
batch start
#iterations: 381
currently lose_sum: 94.67370820045471
time_elpased: 5.239
batch start
#iterations: 382
currently lose_sum: 94.56400161981583
time_elpased: 5.368
batch start
#iterations: 383
currently lose_sum: 94.78101122379303
time_elpased: 5.526
batch start
#iterations: 384
currently lose_sum: 94.73880636692047
time_elpased: 5.504
batch start
#iterations: 385
currently lose_sum: 94.85611969232559
time_elpased: 4.344
batch start
#iterations: 386
currently lose_sum: 95.1402558684349
time_elpased: 4.147
batch start
#iterations: 387
currently lose_sum: 94.93789583444595
time_elpased: 4.264
batch start
#iterations: 388
currently lose_sum: 94.67592698335648
time_elpased: 4.497
batch start
#iterations: 389
currently lose_sum: 94.80795532464981
time_elpased: 5.14
batch start
#iterations: 390
currently lose_sum: 94.84564733505249
time_elpased: 5.496
batch start
#iterations: 391
currently lose_sum: 95.18723636865616
time_elpased: 5.46
batch start
#iterations: 392
currently lose_sum: 94.7947336435318
time_elpased: 5.158
batch start
#iterations: 393
currently lose_sum: 94.44689106941223
time_elpased: 4.622
batch start
#iterations: 394
currently lose_sum: 94.79676324129105
time_elpased: 3.515
batch start
#iterations: 395
currently lose_sum: 94.38239222764969
time_elpased: 4.022
batch start
#iterations: 396
currently lose_sum: 94.77525699138641
time_elpased: 3.918
batch start
#iterations: 397
currently lose_sum: 94.81149536371231
time_elpased: 4.045
batch start
#iterations: 398
currently lose_sum: 94.72382140159607
time_elpased: 4.592
batch start
#iterations: 399
currently lose_sum: 95.19381487369537
time_elpased: 3.922
start validation test
0.655360824742
0.622306512792
0.793557682412
0.697575538267
0.655118198843
60.950
acc: 0.650
pre: 0.618
rec: 0.789
F1: 0.693
auc: 0.689
