start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.75689315795898
time_elpased: 2.413
batch start
#iterations: 1
currently lose_sum: 100.29103702306747
time_elpased: 2.393
batch start
#iterations: 2
currently lose_sum: 99.96156448125839
time_elpased: 2.36
batch start
#iterations: 3
currently lose_sum: 99.98359435796738
time_elpased: 2.499
batch start
#iterations: 4
currently lose_sum: 99.74743282794952
time_elpased: 2.396
batch start
#iterations: 5
currently lose_sum: 99.73840761184692
time_elpased: 2.358
batch start
#iterations: 6
currently lose_sum: 99.59805625677109
time_elpased: 2.353
batch start
#iterations: 7
currently lose_sum: 99.36464393138885
time_elpased: 2.415
batch start
#iterations: 8
currently lose_sum: 99.21387749910355
time_elpased: 2.346
batch start
#iterations: 9
currently lose_sum: 99.33819317817688
time_elpased: 2.22
batch start
#iterations: 10
currently lose_sum: 99.1868160367012
time_elpased: 2.103
batch start
#iterations: 11
currently lose_sum: 99.23992794752121
time_elpased: 2.311
batch start
#iterations: 12
currently lose_sum: 99.33852112293243
time_elpased: 2.429
batch start
#iterations: 13
currently lose_sum: 98.93012630939484
time_elpased: 2.426
batch start
#iterations: 14
currently lose_sum: 99.00760889053345
time_elpased: 2.207
batch start
#iterations: 15
currently lose_sum: 98.59776365756989
time_elpased: 2.471
batch start
#iterations: 16
currently lose_sum: 98.95339423418045
time_elpased: 2.369
batch start
#iterations: 17
currently lose_sum: 98.97039234638214
time_elpased: 2.338
batch start
#iterations: 18
currently lose_sum: 98.74825882911682
time_elpased: 2.433
batch start
#iterations: 19
currently lose_sum: 98.91757261753082
time_elpased: 2.431
start validation test
0.603505154639
0.651912978245
0.447154471545
0.530460261262
0.603779652379
64.864
batch start
#iterations: 20
currently lose_sum: 98.74223536252975
time_elpased: 2.401
batch start
#iterations: 21
currently lose_sum: 98.58896851539612
time_elpased: 2.427
batch start
#iterations: 22
currently lose_sum: 98.78115516901016
time_elpased: 2.453
batch start
#iterations: 23
currently lose_sum: 98.47011744976044
time_elpased: 2.209
batch start
#iterations: 24
currently lose_sum: 98.5482428073883
time_elpased: 2.347
batch start
#iterations: 25
currently lose_sum: 98.37489175796509
time_elpased: 2.35
batch start
#iterations: 26
currently lose_sum: 98.43735605478287
time_elpased: 2.259
batch start
#iterations: 27
currently lose_sum: 98.30716675519943
time_elpased: 2.44
batch start
#iterations: 28
currently lose_sum: 98.39372104406357
time_elpased: 2.398
batch start
#iterations: 29
currently lose_sum: 98.50261795520782
time_elpased: 2.439
batch start
#iterations: 30
currently lose_sum: 98.41151636838913
time_elpased: 2.439
batch start
#iterations: 31
currently lose_sum: 98.48544561862946
time_elpased: 2.377
batch start
#iterations: 32
currently lose_sum: 97.96913200616837
time_elpased: 2.349
batch start
#iterations: 33
currently lose_sum: 98.48123556375504
time_elpased: 2.408
batch start
#iterations: 34
currently lose_sum: 98.20514792203903
time_elpased: 2.176
batch start
#iterations: 35
currently lose_sum: 98.20346653461456
time_elpased: 2.229
batch start
#iterations: 36
currently lose_sum: 98.65293830633163
time_elpased: 2.288
batch start
#iterations: 37
currently lose_sum: 98.30083096027374
time_elpased: 2.423
batch start
#iterations: 38
currently lose_sum: 98.29341876506805
time_elpased: 2.233
batch start
#iterations: 39
currently lose_sum: 98.47427207231522
time_elpased: 2.314
start validation test
0.622886597938
0.629406057993
0.600905629309
0.614825734442
0.622925188919
64.031
batch start
#iterations: 40
currently lose_sum: 98.36321312189102
time_elpased: 2.176
batch start
#iterations: 41
currently lose_sum: 98.26128947734833
time_elpased: 2.614
batch start
#iterations: 42
currently lose_sum: 98.26901412010193
time_elpased: 2.225
batch start
#iterations: 43
currently lose_sum: 98.31863784790039
time_elpased: 1.879
batch start
#iterations: 44
currently lose_sum: 98.28026735782623
time_elpased: 2.464
batch start
#iterations: 45
currently lose_sum: 98.0341928601265
time_elpased: 2.534
batch start
#iterations: 46
currently lose_sum: 98.18293035030365
time_elpased: 2.324
batch start
#iterations: 47
currently lose_sum: 98.12738251686096
time_elpased: 2.405
batch start
#iterations: 48
currently lose_sum: 98.39835494756699
time_elpased: 2.403
batch start
#iterations: 49
currently lose_sum: 98.29530292749405
time_elpased: 2.492
batch start
#iterations: 50
currently lose_sum: 98.5414165854454
time_elpased: 2.231
batch start
#iterations: 51
currently lose_sum: 97.93362033367157
time_elpased: 2.112
batch start
#iterations: 52
currently lose_sum: 97.82114428281784
time_elpased: 2.268
batch start
#iterations: 53
currently lose_sum: 98.37513083219528
time_elpased: 2.019
batch start
#iterations: 54
currently lose_sum: 98.22954678535461
time_elpased: 2.425
batch start
#iterations: 55
currently lose_sum: 98.12185740470886
time_elpased: 2.295
batch start
#iterations: 56
currently lose_sum: 98.30042201280594
time_elpased: 2.445
batch start
#iterations: 57
currently lose_sum: 97.8778846859932
time_elpased: 2.678
batch start
#iterations: 58
currently lose_sum: 98.23132437467575
time_elpased: 2.078
batch start
#iterations: 59
currently lose_sum: 98.05498230457306
time_elpased: 2.299
start validation test
0.621288659794
0.630191166777
0.590305649892
0.609596684202
0.621343055246
63.919
batch start
#iterations: 60
currently lose_sum: 98.40480130910873
time_elpased: 2.585
batch start
#iterations: 61
currently lose_sum: 98.24501341581345
time_elpased: 2.313
batch start
#iterations: 62
currently lose_sum: 98.08870452642441
time_elpased: 2.329
batch start
#iterations: 63
currently lose_sum: 98.38146561384201
time_elpased: 2.377
batch start
#iterations: 64
currently lose_sum: 98.30926024913788
time_elpased: 2.309
batch start
#iterations: 65
currently lose_sum: 97.81928944587708
time_elpased: 2.451
batch start
#iterations: 66
currently lose_sum: 97.65400815010071
time_elpased: 2.371
batch start
#iterations: 67
currently lose_sum: 97.8585302233696
time_elpased: 2.375
batch start
#iterations: 68
currently lose_sum: 98.07728445529938
time_elpased: 2.479
batch start
#iterations: 69
currently lose_sum: 98.11272829771042
time_elpased: 2.525
batch start
#iterations: 70
currently lose_sum: 97.75989663600922
time_elpased: 2.395
batch start
#iterations: 71
currently lose_sum: 97.71160674095154
time_elpased: 2.302
batch start
#iterations: 72
currently lose_sum: 97.65149879455566
time_elpased: 2.383
batch start
#iterations: 73
currently lose_sum: 97.76734668016434
time_elpased: 2.374
batch start
#iterations: 74
currently lose_sum: 97.89659810066223
time_elpased: 2.441
batch start
#iterations: 75
currently lose_sum: 97.81537854671478
time_elpased: 2.375
batch start
#iterations: 76
currently lose_sum: 97.8219045996666
time_elpased: 2.323
batch start
#iterations: 77
currently lose_sum: 97.63980555534363
time_elpased: 2.382
batch start
#iterations: 78
currently lose_sum: 97.95940721035004
time_elpased: 2.325
batch start
#iterations: 79
currently lose_sum: 98.10085690021515
time_elpased: 2.151
start validation test
0.622371134021
0.63528346724
0.577750334465
0.605152527757
0.622449472717
63.839
batch start
#iterations: 80
currently lose_sum: 97.93388670682907
time_elpased: 2.404
batch start
#iterations: 81
currently lose_sum: 98.25745284557343
time_elpased: 2.332
batch start
#iterations: 82
currently lose_sum: 97.82728683948517
time_elpased: 2.363
batch start
#iterations: 83
currently lose_sum: 98.1821722984314
time_elpased: 2.457
batch start
#iterations: 84
currently lose_sum: 97.85642594099045
time_elpased: 2.42
batch start
#iterations: 85
currently lose_sum: 98.0412603020668
time_elpased: 2.527
batch start
#iterations: 86
currently lose_sum: 97.89491558074951
time_elpased: 2.235
batch start
#iterations: 87
currently lose_sum: 97.78543943166733
time_elpased: 2.355
batch start
#iterations: 88
currently lose_sum: 97.71440291404724
time_elpased: 2.313
batch start
#iterations: 89
currently lose_sum: 97.89980655908585
time_elpased: 2.035
batch start
#iterations: 90
currently lose_sum: 97.85943073034286
time_elpased: 2.26
batch start
#iterations: 91
currently lose_sum: 97.87020230293274
time_elpased: 2.334
batch start
#iterations: 92
currently lose_sum: 97.86021226644516
time_elpased: 2.198
batch start
#iterations: 93
currently lose_sum: 97.74990206956863
time_elpased: 2.237
batch start
#iterations: 94
currently lose_sum: 97.8526845574379
time_elpased: 2.369
batch start
#iterations: 95
currently lose_sum: 97.78980660438538
time_elpased: 2.315
batch start
#iterations: 96
currently lose_sum: 97.90710437297821
time_elpased: 2.423
batch start
#iterations: 97
currently lose_sum: 97.93730580806732
time_elpased: 2.324
batch start
#iterations: 98
currently lose_sum: 98.00188779830933
time_elpased: 2.427
batch start
#iterations: 99
currently lose_sum: 97.72089886665344
time_elpased: 2.21
start validation test
0.622474226804
0.634908106889
0.579499845631
0.605939954805
0.622549674959
63.679
batch start
#iterations: 100
currently lose_sum: 97.524041056633
time_elpased: 1.926
batch start
#iterations: 101
currently lose_sum: 97.94647455215454
time_elpased: 2.181
batch start
#iterations: 102
currently lose_sum: 97.5991108417511
time_elpased: 1.956
batch start
#iterations: 103
currently lose_sum: 97.66186022758484
time_elpased: 1.931
batch start
#iterations: 104
currently lose_sum: 97.72475785017014
time_elpased: 2.042
batch start
#iterations: 105
currently lose_sum: 97.44097024202347
time_elpased: 2.386
batch start
#iterations: 106
currently lose_sum: 98.06230968236923
time_elpased: 2.678
batch start
#iterations: 107
currently lose_sum: 97.50891518592834
time_elpased: 2.072
batch start
#iterations: 108
currently lose_sum: 97.6757447719574
time_elpased: 2.447
batch start
#iterations: 109
currently lose_sum: 97.77030789852142
time_elpased: 2.266
batch start
#iterations: 110
currently lose_sum: 97.8835881948471
time_elpased: 2.383
batch start
#iterations: 111
currently lose_sum: 97.73089861869812
time_elpased: 2.376
batch start
#iterations: 112
currently lose_sum: 97.69844847917557
time_elpased: 2.403
batch start
#iterations: 113
currently lose_sum: 97.42749339342117
time_elpased: 2.261
batch start
#iterations: 114
currently lose_sum: 97.82931935787201
time_elpased: 2.361
batch start
#iterations: 115
currently lose_sum: 97.62099754810333
time_elpased: 2.56
batch start
#iterations: 116
currently lose_sum: 97.57156693935394
time_elpased: 2.581
batch start
#iterations: 117
currently lose_sum: 97.85434031486511
time_elpased: 2.425
batch start
#iterations: 118
currently lose_sum: 97.67156141996384
time_elpased: 2.576
batch start
#iterations: 119
currently lose_sum: 97.48661673069
time_elpased: 2.009
start validation test
0.625
0.638686960473
0.578676546259
0.607202634847
0.625081327968
63.694
batch start
#iterations: 120
currently lose_sum: 97.73318636417389
time_elpased: 2.274
batch start
#iterations: 121
currently lose_sum: 97.59480732679367
time_elpased: 2.368
batch start
#iterations: 122
currently lose_sum: 97.62435030937195
time_elpased: 2.116
batch start
#iterations: 123
currently lose_sum: 97.82803678512573
time_elpased: 2.319
batch start
#iterations: 124
currently lose_sum: 97.69551229476929
time_elpased: 2.286
batch start
#iterations: 125
currently lose_sum: 97.59574860334396
time_elpased: 2.279
batch start
#iterations: 126
currently lose_sum: 97.56062400341034
time_elpased: 2.469
batch start
#iterations: 127
currently lose_sum: 97.72460341453552
time_elpased: 2.417
batch start
#iterations: 128
currently lose_sum: 97.6668610572815
time_elpased: 2.359
batch start
#iterations: 129
currently lose_sum: 97.71330732107162
time_elpased: 2.385
batch start
#iterations: 130
currently lose_sum: 97.49934077262878
time_elpased: 2.327
batch start
#iterations: 131
currently lose_sum: 97.86266577243805
time_elpased: 2.408
batch start
#iterations: 132
currently lose_sum: 97.59071493148804
time_elpased: 2.136
batch start
#iterations: 133
currently lose_sum: 97.58314567804337
time_elpased: 2.271
batch start
#iterations: 134
currently lose_sum: 97.52534979581833
time_elpased: 2.104
batch start
#iterations: 135
currently lose_sum: 97.78369390964508
time_elpased: 1.903
batch start
#iterations: 136
currently lose_sum: 97.66542088985443
time_elpased: 2.003
batch start
#iterations: 137
currently lose_sum: 97.44542133808136
time_elpased: 2.072
batch start
#iterations: 138
currently lose_sum: 97.46009111404419
time_elpased: 1.876
batch start
#iterations: 139
currently lose_sum: 97.75359725952148
time_elpased: 1.922
start validation test
0.617010309278
0.642137973897
0.53164556962
0.581691250985
0.617160180245
63.909
batch start
#iterations: 140
currently lose_sum: 97.621033847332
time_elpased: 2.035
batch start
#iterations: 141
currently lose_sum: 97.693050801754
time_elpased: 2.193
batch start
#iterations: 142
currently lose_sum: 97.40939420461655
time_elpased: 2.495
batch start
#iterations: 143
currently lose_sum: 97.80469846725464
time_elpased: 2.236
batch start
#iterations: 144
currently lose_sum: 97.52839934825897
time_elpased: 2.101
batch start
#iterations: 145
currently lose_sum: 97.50622045993805
time_elpased: 2.346
batch start
#iterations: 146
currently lose_sum: 97.55634140968323
time_elpased: 2.333
batch start
#iterations: 147
currently lose_sum: 98.0738263130188
time_elpased: 2.292
batch start
#iterations: 148
currently lose_sum: 97.60769265890121
time_elpased: 2.221
batch start
#iterations: 149
currently lose_sum: 97.76381570100784
time_elpased: 2.173
batch start
#iterations: 150
currently lose_sum: 97.80242168903351
time_elpased: 2.19
batch start
#iterations: 151
currently lose_sum: 97.43803876638412
time_elpased: 2.348
batch start
#iterations: 152
currently lose_sum: 97.68681353330612
time_elpased: 2.333
batch start
#iterations: 153
currently lose_sum: 97.68723344802856
time_elpased: 2.188
batch start
#iterations: 154
currently lose_sum: 97.80981540679932
time_elpased: 2.183
batch start
#iterations: 155
currently lose_sum: 97.75692754983902
time_elpased: 2.727
batch start
#iterations: 156
currently lose_sum: 97.30945724248886
time_elpased: 2.334
batch start
#iterations: 157
currently lose_sum: 97.67054343223572
time_elpased: 2.189
batch start
#iterations: 158
currently lose_sum: 97.67217028141022
time_elpased: 2.364
batch start
#iterations: 159
currently lose_sum: 97.79344272613525
time_elpased: 2.423
start validation test
0.624639175258
0.629369886303
0.609550272718
0.61930154747
0.624665666153
63.631
batch start
#iterations: 160
currently lose_sum: 97.64710932970047
time_elpased: 2.529
batch start
#iterations: 161
currently lose_sum: 97.54978251457214
time_elpased: 2.535
batch start
#iterations: 162
currently lose_sum: 97.41759330034256
time_elpased: 2.361
batch start
#iterations: 163
currently lose_sum: 97.63924407958984
time_elpased: 2.506
batch start
#iterations: 164
currently lose_sum: 97.90630918741226
time_elpased: 1.844
batch start
#iterations: 165
currently lose_sum: 97.85508722066879
time_elpased: 2.467
batch start
#iterations: 166
currently lose_sum: 97.39625835418701
time_elpased: 2.593
batch start
#iterations: 167
currently lose_sum: 97.5408770442009
time_elpased: 2.36
batch start
#iterations: 168
currently lose_sum: 97.51642835140228
time_elpased: 2.575
batch start
#iterations: 169
currently lose_sum: 97.36788403987885
time_elpased: 2.706
batch start
#iterations: 170
currently lose_sum: 97.46695500612259
time_elpased: 2.452
batch start
#iterations: 171
currently lose_sum: 97.53259390592575
time_elpased: 2.572
batch start
#iterations: 172
currently lose_sum: 97.5369479060173
time_elpased: 2.486
batch start
#iterations: 173
currently lose_sum: 97.59764587879181
time_elpased: 2.358
batch start
#iterations: 174
currently lose_sum: 97.41780537366867
time_elpased: 2.456
batch start
#iterations: 175
currently lose_sum: 97.62182676792145
time_elpased: 2.357
batch start
#iterations: 176
currently lose_sum: 97.32402569055557
time_elpased: 2.332
batch start
#iterations: 177
currently lose_sum: 97.55488693714142
time_elpased: 2.307
batch start
#iterations: 178
currently lose_sum: 97.56819158792496
time_elpased: 2.343
batch start
#iterations: 179
currently lose_sum: 97.24971514940262
time_elpased: 2.437
start validation test
0.623659793814
0.633539686049
0.589791087784
0.610883121036
0.623719255552
63.523
batch start
#iterations: 180
currently lose_sum: 97.66058933734894
time_elpased: 2.387
batch start
#iterations: 181
currently lose_sum: 97.46291249990463
time_elpased: 2.31
batch start
#iterations: 182
currently lose_sum: 97.74101161956787
time_elpased: 2.368
batch start
#iterations: 183
currently lose_sum: 97.33621644973755
time_elpased: 2.491
batch start
#iterations: 184
currently lose_sum: 96.99699330329895
time_elpased: 2.177
batch start
#iterations: 185
currently lose_sum: 97.35204654932022
time_elpased: 2.153
batch start
#iterations: 186
currently lose_sum: 97.24541079998016
time_elpased: 2.383
batch start
#iterations: 187
currently lose_sum: 97.25927865505219
time_elpased: 2.447
batch start
#iterations: 188
currently lose_sum: 97.35233080387115
time_elpased: 2.098
batch start
#iterations: 189
currently lose_sum: 97.5477249622345
time_elpased: 2.349
batch start
#iterations: 190
currently lose_sum: 97.4546719789505
time_elpased: 2.442
batch start
#iterations: 191
currently lose_sum: 97.14195734262466
time_elpased: 2.393
batch start
#iterations: 192
currently lose_sum: 97.26100301742554
time_elpased: 2.323
batch start
#iterations: 193
currently lose_sum: 97.25735312700272
time_elpased: 2.369
batch start
#iterations: 194
currently lose_sum: 97.18081682920456
time_elpased: 2.275
batch start
#iterations: 195
currently lose_sum: 97.55999428033829
time_elpased: 2.303
batch start
#iterations: 196
currently lose_sum: 97.33422350883484
time_elpased: 2.253
batch start
#iterations: 197
currently lose_sum: 97.69489097595215
time_elpased: 2.049
batch start
#iterations: 198
currently lose_sum: 97.3339935541153
time_elpased: 2.24
batch start
#iterations: 199
currently lose_sum: 97.48552948236465
time_elpased: 2.405
start validation test
0.621958762887
0.635659797336
0.574560049398
0.603567567568
0.622041978639
63.674
batch start
#iterations: 200
currently lose_sum: 97.43403881788254
time_elpased: 2.449
batch start
#iterations: 201
currently lose_sum: 97.09292823076248
time_elpased: 2.418
batch start
#iterations: 202
currently lose_sum: 97.48298925161362
time_elpased: 2.309
batch start
#iterations: 203
currently lose_sum: 97.35352551937103
time_elpased: 2.147
batch start
#iterations: 204
currently lose_sum: 97.57811504602432
time_elpased: 2.216
batch start
#iterations: 205
currently lose_sum: 97.4015326499939
time_elpased: 2.432
batch start
#iterations: 206
currently lose_sum: 97.2365306019783
time_elpased: 2.66
batch start
#iterations: 207
currently lose_sum: 97.23513567447662
time_elpased: 2.414
batch start
#iterations: 208
currently lose_sum: 97.17361330986023
time_elpased: 2.208
batch start
#iterations: 209
currently lose_sum: 97.1447103023529
time_elpased: 2.499
batch start
#iterations: 210
currently lose_sum: 97.31533652544022
time_elpased: 2.186
batch start
#iterations: 211
currently lose_sum: 97.41976308822632
time_elpased: 2.186
batch start
#iterations: 212
currently lose_sum: 97.09674847126007
time_elpased: 2.057
batch start
#iterations: 213
currently lose_sum: 96.94339168071747
time_elpased: 2.431
batch start
#iterations: 214
currently lose_sum: 97.0778426527977
time_elpased: 2.437
batch start
#iterations: 215
currently lose_sum: 97.12955605983734
time_elpased: 2.494
batch start
#iterations: 216
currently lose_sum: 97.38506644964218
time_elpased: 2.311
batch start
#iterations: 217
currently lose_sum: 97.1240137219429
time_elpased: 2.542
batch start
#iterations: 218
currently lose_sum: 97.29167264699936
time_elpased: 2.183
batch start
#iterations: 219
currently lose_sum: 97.4501023888588
time_elpased: 2.464
start validation test
0.627525773196
0.632598743745
0.611505608727
0.621873364731
0.627553899066
63.460
batch start
#iterations: 220
currently lose_sum: 97.5153374671936
time_elpased: 2.405
batch start
#iterations: 221
currently lose_sum: 97.64698427915573
time_elpased: 2.173
batch start
#iterations: 222
currently lose_sum: 97.26342928409576
time_elpased: 2.555
batch start
#iterations: 223
currently lose_sum: 97.18761497735977
time_elpased: 2.352
batch start
#iterations: 224
currently lose_sum: 97.31215530633926
time_elpased: 2.52
batch start
#iterations: 225
currently lose_sum: 97.31127953529358
time_elpased: 2.441
batch start
#iterations: 226
currently lose_sum: 97.46511614322662
time_elpased: 2.368
batch start
#iterations: 227
currently lose_sum: 97.2708432674408
time_elpased: 2.411
batch start
#iterations: 228
currently lose_sum: 97.21714913845062
time_elpased: 2.452
batch start
#iterations: 229
currently lose_sum: 97.49005836248398
time_elpased: 2.108
batch start
#iterations: 230
currently lose_sum: 97.29931056499481
time_elpased: 2.52
batch start
#iterations: 231
currently lose_sum: 97.08738702535629
time_elpased: 2.563
batch start
#iterations: 232
currently lose_sum: 97.33985024690628
time_elpased: 2.381
batch start
#iterations: 233
currently lose_sum: 97.20055341720581
time_elpased: 2.311
batch start
#iterations: 234
currently lose_sum: 97.33388406038284
time_elpased: 2.428
batch start
#iterations: 235
currently lose_sum: 97.15918958187103
time_elpased: 2.41
batch start
#iterations: 236
currently lose_sum: 97.28894513845444
time_elpased: 2.309
batch start
#iterations: 237
currently lose_sum: 97.25767630338669
time_elpased: 2.336
batch start
#iterations: 238
currently lose_sum: 97.46150237321854
time_elpased: 2.492
batch start
#iterations: 239
currently lose_sum: 97.06373393535614
time_elpased: 2.346
start validation test
0.625824742268
0.631023454158
0.609138623032
0.619887940514
0.625854037324
63.423
batch start
#iterations: 240
currently lose_sum: 97.173654794693
time_elpased: 2.417
batch start
#iterations: 241
currently lose_sum: 97.02637600898743
time_elpased: 2.407
batch start
#iterations: 242
currently lose_sum: 97.42590123414993
time_elpased: 2.421
batch start
#iterations: 243
currently lose_sum: 97.12132668495178
time_elpased: 2.019
batch start
#iterations: 244
currently lose_sum: 97.32411754131317
time_elpased: 2.294
batch start
#iterations: 245
currently lose_sum: 97.47286611795425
time_elpased: 2.471
batch start
#iterations: 246
currently lose_sum: 97.29978996515274
time_elpased: 2.434
batch start
#iterations: 247
currently lose_sum: 97.26694250106812
time_elpased: 2.319
batch start
#iterations: 248
currently lose_sum: 97.2623103260994
time_elpased: 2.171
batch start
#iterations: 249
currently lose_sum: 97.26430559158325
time_elpased: 2.287
batch start
#iterations: 250
currently lose_sum: 97.32413411140442
time_elpased: 2.099
batch start
#iterations: 251
currently lose_sum: 97.19192558526993
time_elpased: 2.446
batch start
#iterations: 252
currently lose_sum: 97.30874049663544
time_elpased: 2.393
batch start
#iterations: 253
currently lose_sum: 97.26432204246521
time_elpased: 2.306
batch start
#iterations: 254
currently lose_sum: 97.24444139003754
time_elpased: 2.424
batch start
#iterations: 255
currently lose_sum: 97.01509177684784
time_elpased: 2.258
batch start
#iterations: 256
currently lose_sum: 97.18037676811218
time_elpased: 2.389
batch start
#iterations: 257
currently lose_sum: 97.42526912689209
time_elpased: 2.327
batch start
#iterations: 258
currently lose_sum: 97.2985121011734
time_elpased: 2.464
batch start
#iterations: 259
currently lose_sum: 97.25474715232849
time_elpased: 2.128
start validation test
0.624175257732
0.630542402066
0.60296387774
0.616444841917
0.624212497581
63.458
batch start
#iterations: 260
currently lose_sum: 96.92482405900955
time_elpased: 2.506
batch start
#iterations: 261
currently lose_sum: 97.30812728404999
time_elpased: 2.384
batch start
#iterations: 262
currently lose_sum: 97.13116067647934
time_elpased: 2.338
batch start
#iterations: 263
currently lose_sum: 97.30380439758301
time_elpased: 2.206
batch start
#iterations: 264
currently lose_sum: 97.35740232467651
time_elpased: 2.471
batch start
#iterations: 265
currently lose_sum: 97.01626563072205
time_elpased: 2.269
batch start
#iterations: 266
currently lose_sum: 97.22998839616776
time_elpased: 2.354
batch start
#iterations: 267
currently lose_sum: 97.13692545890808
time_elpased: 2.392
batch start
#iterations: 268
currently lose_sum: 97.13609200716019
time_elpased: 2.463
batch start
#iterations: 269
currently lose_sum: 97.10020536184311
time_elpased: 2.428
batch start
#iterations: 270
currently lose_sum: 97.48869836330414
time_elpased: 2.301
batch start
#iterations: 271
currently lose_sum: 97.3505933880806
time_elpased: 2.181
batch start
#iterations: 272
currently lose_sum: 97.19653338193893
time_elpased: 2.439
batch start
#iterations: 273
currently lose_sum: 97.32807868719101
time_elpased: 2.406
batch start
#iterations: 274
currently lose_sum: 97.0593067407608
time_elpased: 2.365
batch start
#iterations: 275
currently lose_sum: 97.13685482740402
time_elpased: 1.979
batch start
#iterations: 276
currently lose_sum: 97.54347652196884
time_elpased: 2.312
batch start
#iterations: 277
currently lose_sum: 97.57747769355774
time_elpased: 2.464
batch start
#iterations: 278
currently lose_sum: 97.23068732023239
time_elpased: 2.297
batch start
#iterations: 279
currently lose_sum: 97.11814147233963
time_elpased: 2.53
start validation test
0.626649484536
0.631763953984
0.61037357209
0.620884585187
0.626678059411
63.408
batch start
#iterations: 280
currently lose_sum: 97.23599600791931
time_elpased: 2.352
batch start
#iterations: 281
currently lose_sum: 97.27782130241394
time_elpased: 2.308
batch start
#iterations: 282
currently lose_sum: 97.38358986377716
time_elpased: 2.391
batch start
#iterations: 283
currently lose_sum: 97.44815403223038
time_elpased: 2.251
batch start
#iterations: 284
currently lose_sum: 97.13256776332855
time_elpased: 1.886
batch start
#iterations: 285
currently lose_sum: 97.04845005273819
time_elpased: 2.282
batch start
#iterations: 286
currently lose_sum: 97.40823459625244
time_elpased: 2.107
batch start
#iterations: 287
currently lose_sum: 97.19765657186508
time_elpased: 2.143
batch start
#iterations: 288
currently lose_sum: 96.97706770896912
time_elpased: 2.423
batch start
#iterations: 289
currently lose_sum: 97.02410280704498
time_elpased: 2.435
batch start
#iterations: 290
currently lose_sum: 97.27316975593567
time_elpased: 2.314
batch start
#iterations: 291
currently lose_sum: 96.9436205625534
time_elpased: 2.381
batch start
#iterations: 292
currently lose_sum: 96.95431935787201
time_elpased: 2.403
batch start
#iterations: 293
currently lose_sum: 96.99497318267822
time_elpased: 2.309
batch start
#iterations: 294
currently lose_sum: 97.11810314655304
time_elpased: 2.262
batch start
#iterations: 295
currently lose_sum: 96.9332726597786
time_elpased: 2.332
batch start
#iterations: 296
currently lose_sum: 97.13294523954391
time_elpased: 2.195
batch start
#iterations: 297
currently lose_sum: 97.29285365343094
time_elpased: 2.307
batch start
#iterations: 298
currently lose_sum: 97.3399453163147
time_elpased: 2.191
batch start
#iterations: 299
currently lose_sum: 96.86828947067261
time_elpased: 2.181
start validation test
0.62293814433
0.634580905424
0.58279304312
0.607585429966
0.623008625247
63.531
batch start
#iterations: 300
currently lose_sum: 97.44918429851532
time_elpased: 2.089
batch start
#iterations: 301
currently lose_sum: 97.21681594848633
time_elpased: 2.259
batch start
#iterations: 302
currently lose_sum: 97.45458859205246
time_elpased: 2.314
batch start
#iterations: 303
currently lose_sum: 97.06876617670059
time_elpased: 2.418
batch start
#iterations: 304
currently lose_sum: 97.17607295513153
time_elpased: 2.277
batch start
#iterations: 305
currently lose_sum: 97.10244333744049
time_elpased: 2.362
batch start
#iterations: 306
currently lose_sum: 97.37838786840439
time_elpased: 2.475
batch start
#iterations: 307
currently lose_sum: 97.21432560682297
time_elpased: 2.376
batch start
#iterations: 308
currently lose_sum: 97.12783199548721
time_elpased: 2.562
batch start
#iterations: 309
currently lose_sum: 97.33569133281708
time_elpased: 2.415
batch start
#iterations: 310
currently lose_sum: 97.15541487932205
time_elpased: 1.984
batch start
#iterations: 311
currently lose_sum: 97.17628085613251
time_elpased: 1.955
batch start
#iterations: 312
currently lose_sum: 97.44901597499847
time_elpased: 2.274
batch start
#iterations: 313
currently lose_sum: 97.05375653505325
time_elpased: 2.409
batch start
#iterations: 314
currently lose_sum: 97.00136590003967
time_elpased: 2.361
batch start
#iterations: 315
currently lose_sum: 97.0877435207367
time_elpased: 2.447
batch start
#iterations: 316
currently lose_sum: 97.35349249839783
time_elpased: 2.502
batch start
#iterations: 317
currently lose_sum: 97.06434798240662
time_elpased: 2.447
batch start
#iterations: 318
currently lose_sum: 97.17761319875717
time_elpased: 2.095
batch start
#iterations: 319
currently lose_sum: 97.12971383333206
time_elpased: 2.411
start validation test
0.625206185567
0.632244809689
0.601728928682
0.616609543897
0.625247403513
63.544
batch start
#iterations: 320
currently lose_sum: 97.0131379365921
time_elpased: 2.622
batch start
#iterations: 321
currently lose_sum: 96.889599442482
time_elpased: 2.649
batch start
#iterations: 322
currently lose_sum: 97.36241972446442
time_elpased: 2.477
batch start
#iterations: 323
currently lose_sum: 97.18323755264282
time_elpased: 2.486
batch start
#iterations: 324
currently lose_sum: 97.39255177974701
time_elpased: 2.33
batch start
#iterations: 325
currently lose_sum: 97.38656717538834
time_elpased: 2.452
batch start
#iterations: 326
currently lose_sum: 97.1485612988472
time_elpased: 2.318
batch start
#iterations: 327
currently lose_sum: 97.25949800014496
time_elpased: 2.381
batch start
#iterations: 328
currently lose_sum: 96.95304203033447
time_elpased: 2.398
batch start
#iterations: 329
currently lose_sum: 97.18640208244324
time_elpased: 2.462
batch start
#iterations: 330
currently lose_sum: 96.96740627288818
time_elpased: 2.286
batch start
#iterations: 331
currently lose_sum: 97.32664227485657
time_elpased: 2.411
batch start
#iterations: 332
currently lose_sum: 97.05669403076172
time_elpased: 2.571
batch start
#iterations: 333
currently lose_sum: 97.27442222833633
time_elpased: 2.367
batch start
#iterations: 334
currently lose_sum: 97.2952048778534
time_elpased: 2.463
batch start
#iterations: 335
currently lose_sum: 97.36304998397827
time_elpased: 2.431
batch start
#iterations: 336
currently lose_sum: 97.06065553426743
time_elpased: 2.149
batch start
#iterations: 337
currently lose_sum: 97.42482715845108
time_elpased: 2.27
batch start
#iterations: 338
currently lose_sum: 97.14208424091339
time_elpased: 2.504
batch start
#iterations: 339
currently lose_sum: 97.09093248844147
time_elpased: 2.186
start validation test
0.624742268041
0.634329180906
0.592158073479
0.612518628912
0.624799474621
63.501
batch start
#iterations: 340
currently lose_sum: 96.97496056556702
time_elpased: 2.239
batch start
#iterations: 341
currently lose_sum: 96.97736287117004
time_elpased: 2.385
batch start
#iterations: 342
currently lose_sum: 97.23548465967178
time_elpased: 2.365
batch start
#iterations: 343
currently lose_sum: 97.30713677406311
time_elpased: 2.244
batch start
#iterations: 344
currently lose_sum: 97.01155197620392
time_elpased: 2.389
batch start
#iterations: 345
currently lose_sum: 97.12319523096085
time_elpased: 2.201
batch start
#iterations: 346
currently lose_sum: 97.22205913066864
time_elpased: 2.24
batch start
#iterations: 347
currently lose_sum: 97.10234254598618
time_elpased: 2.065
batch start
#iterations: 348
currently lose_sum: 96.99058032035828
time_elpased: 2.207
batch start
#iterations: 349
currently lose_sum: 97.5372925400734
time_elpased: 2.324
batch start
#iterations: 350
currently lose_sum: 97.0516704916954
time_elpased: 2.734
batch start
#iterations: 351
currently lose_sum: 96.9589758515358
time_elpased: 2.675
batch start
#iterations: 352
currently lose_sum: 96.94011169672012
time_elpased: 2.485
batch start
#iterations: 353
currently lose_sum: 97.16223394870758
time_elpased: 2.438
batch start
#iterations: 354
currently lose_sum: 97.14667403697968
time_elpased: 2.413
batch start
#iterations: 355
currently lose_sum: 97.4653959274292
time_elpased: 2.102
batch start
#iterations: 356
currently lose_sum: 97.1381528377533
time_elpased: 2.252
batch start
#iterations: 357
currently lose_sum: 96.95407801866531
time_elpased: 2.457
batch start
#iterations: 358
currently lose_sum: 97.41302734613419
time_elpased: 2.256
batch start
#iterations: 359
currently lose_sum: 97.04795503616333
time_elpased: 2.232
start validation test
0.624690721649
0.639072847682
0.576000823299
0.605899864682
0.624776204276
63.535
batch start
#iterations: 360
currently lose_sum: 97.47312051057816
time_elpased: 2.417
batch start
#iterations: 361
currently lose_sum: 97.41745227575302
time_elpased: 2.531
batch start
#iterations: 362
currently lose_sum: 97.04929721355438
time_elpased: 2.345
batch start
#iterations: 363
currently lose_sum: 97.04426181316376
time_elpased: 2.248
batch start
#iterations: 364
currently lose_sum: 97.31888318061829
time_elpased: 2.155
batch start
#iterations: 365
currently lose_sum: 97.36993426084518
time_elpased: 2.338
batch start
#iterations: 366
currently lose_sum: 96.99921238422394
time_elpased: 2.444
batch start
#iterations: 367
currently lose_sum: 97.35377955436707
time_elpased: 2.186
batch start
#iterations: 368
currently lose_sum: 96.98869842290878
time_elpased: 2.257
batch start
#iterations: 369
currently lose_sum: 97.11991775035858
time_elpased: 1.982
batch start
#iterations: 370
currently lose_sum: 97.29399317502975
time_elpased: 1.995
batch start
#iterations: 371
currently lose_sum: 96.98099422454834
time_elpased: 1.925
batch start
#iterations: 372
currently lose_sum: 97.05973982810974
time_elpased: 2.052
batch start
#iterations: 373
currently lose_sum: 96.93460363149643
time_elpased: 2.111
batch start
#iterations: 374
currently lose_sum: 97.05645781755447
time_elpased: 2.194
batch start
#iterations: 375
currently lose_sum: 97.43379610776901
time_elpased: 2.052
batch start
#iterations: 376
currently lose_sum: 96.9368108510971
time_elpased: 2.317
batch start
#iterations: 377
currently lose_sum: 97.17008399963379
time_elpased: 2.26
batch start
#iterations: 378
currently lose_sum: 97.10392099618912
time_elpased: 2.372
batch start
#iterations: 379
currently lose_sum: 96.91945493221283
time_elpased: 2.12
start validation test
0.622989690722
0.636024000906
0.578161984151
0.605714285714
0.623068392675
63.650
batch start
#iterations: 380
currently lose_sum: 97.20919394493103
time_elpased: 2.317
batch start
#iterations: 381
currently lose_sum: 96.74925893545151
time_elpased: 2.378
batch start
#iterations: 382
currently lose_sum: 97.18643808364868
time_elpased: 2.419
batch start
#iterations: 383
currently lose_sum: 96.85175985097885
time_elpased: 2.69
batch start
#iterations: 384
currently lose_sum: 97.48135846853256
time_elpased: 2.332
batch start
#iterations: 385
currently lose_sum: 97.20059990882874
time_elpased: 2.297
batch start
#iterations: 386
currently lose_sum: 97.1620591878891
time_elpased: 2.306
batch start
#iterations: 387
currently lose_sum: 97.27382791042328
time_elpased: 2.421
batch start
#iterations: 388
currently lose_sum: 97.17076689004898
time_elpased: 2.29
batch start
#iterations: 389
currently lose_sum: 97.1182918548584
time_elpased: 2.502
batch start
#iterations: 390
currently lose_sum: 96.82957196235657
time_elpased: 2.54
batch start
#iterations: 391
currently lose_sum: 97.25311976671219
time_elpased: 2.239
batch start
#iterations: 392
currently lose_sum: 97.00837928056717
time_elpased: 2.24
batch start
#iterations: 393
currently lose_sum: 97.20899993181229
time_elpased: 2.472
batch start
#iterations: 394
currently lose_sum: 97.15069764852524
time_elpased: 2.311
batch start
#iterations: 395
currently lose_sum: 96.9264349937439
time_elpased: 2.417
batch start
#iterations: 396
currently lose_sum: 97.26511138677597
time_elpased: 2.448
batch start
#iterations: 397
currently lose_sum: 96.7215883731842
time_elpased: 2.297
batch start
#iterations: 398
currently lose_sum: 96.65020221471786
time_elpased: 2.513
batch start
#iterations: 399
currently lose_sum: 97.24244529008865
time_elpased: 2.327
start validation test
0.620618556701
0.627529488151
0.596789132448
0.611773393818
0.620660392931
63.614
acc: 0.625
pre: 0.629
rec: 0.612
F1: 0.620
auc: 0.664
