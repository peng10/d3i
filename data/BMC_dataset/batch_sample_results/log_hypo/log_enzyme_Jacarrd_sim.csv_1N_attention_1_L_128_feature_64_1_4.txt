start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.6075769662857
time_elpased: 1.668
batch start
#iterations: 1
currently lose_sum: 100.3573830127716
time_elpased: 1.652
batch start
#iterations: 2
currently lose_sum: 100.17890501022339
time_elpased: 1.677
batch start
#iterations: 3
currently lose_sum: 100.10662919282913
time_elpased: 1.636
batch start
#iterations: 4
currently lose_sum: 99.91944456100464
time_elpased: 1.615
batch start
#iterations: 5
currently lose_sum: 99.84836035966873
time_elpased: 1.637
batch start
#iterations: 6
currently lose_sum: 99.52470350265503
time_elpased: 1.677
batch start
#iterations: 7
currently lose_sum: 99.47790533304214
time_elpased: 1.63
batch start
#iterations: 8
currently lose_sum: 99.52211385965347
time_elpased: 1.671
batch start
#iterations: 9
currently lose_sum: 99.0341522693634
time_elpased: 1.634
batch start
#iterations: 10
currently lose_sum: 99.54844558238983
time_elpased: 1.675
batch start
#iterations: 11
currently lose_sum: 99.2368375658989
time_elpased: 1.615
batch start
#iterations: 12
currently lose_sum: 99.01687151193619
time_elpased: 1.619
batch start
#iterations: 13
currently lose_sum: 99.19966304302216
time_elpased: 1.686
batch start
#iterations: 14
currently lose_sum: 98.92738044261932
time_elpased: 1.61
batch start
#iterations: 15
currently lose_sum: 98.96117728948593
time_elpased: 1.591
batch start
#iterations: 16
currently lose_sum: 98.96450936794281
time_elpased: 1.662
batch start
#iterations: 17
currently lose_sum: 98.78625601530075
time_elpased: 1.64
batch start
#iterations: 18
currently lose_sum: 98.8966127038002
time_elpased: 1.693
batch start
#iterations: 19
currently lose_sum: 98.88316649198532
time_elpased: 1.698
start validation test
0.612628865979
0.605479977007
0.650406504065
0.627139667576
0.612562541509
64.353
batch start
#iterations: 20
currently lose_sum: 98.71657663583755
time_elpased: 1.66
batch start
#iterations: 21
currently lose_sum: 98.9552715420723
time_elpased: 1.644
batch start
#iterations: 22
currently lose_sum: 98.79100435972214
time_elpased: 1.66
batch start
#iterations: 23
currently lose_sum: 98.66394913196564
time_elpased: 1.652
batch start
#iterations: 24
currently lose_sum: 98.67455834150314
time_elpased: 1.698
batch start
#iterations: 25
currently lose_sum: 98.80449521541595
time_elpased: 1.63
batch start
#iterations: 26
currently lose_sum: 98.5717334151268
time_elpased: 1.637
batch start
#iterations: 27
currently lose_sum: 98.504603266716
time_elpased: 1.677
batch start
#iterations: 28
currently lose_sum: 98.6739599108696
time_elpased: 1.693
batch start
#iterations: 29
currently lose_sum: 98.29499262571335
time_elpased: 1.628
batch start
#iterations: 30
currently lose_sum: 98.56428915262222
time_elpased: 1.63
batch start
#iterations: 31
currently lose_sum: 98.35120415687561
time_elpased: 1.637
batch start
#iterations: 32
currently lose_sum: 98.45838469266891
time_elpased: 1.676
batch start
#iterations: 33
currently lose_sum: 98.49283319711685
time_elpased: 1.622
batch start
#iterations: 34
currently lose_sum: 98.35084944963455
time_elpased: 1.689
batch start
#iterations: 35
currently lose_sum: 98.48780137300491
time_elpased: 1.643
batch start
#iterations: 36
currently lose_sum: 98.56216090917587
time_elpased: 1.668
batch start
#iterations: 37
currently lose_sum: 98.37753397226334
time_elpased: 1.789
batch start
#iterations: 38
currently lose_sum: 98.25501942634583
time_elpased: 1.62
batch start
#iterations: 39
currently lose_sum: 98.5380466580391
time_elpased: 1.618
start validation test
0.611494845361
0.635842472582
0.525059174642
0.57516487233
0.611646596513
64.297
batch start
#iterations: 40
currently lose_sum: 98.33121246099472
time_elpased: 1.642
batch start
#iterations: 41
currently lose_sum: 98.21254199743271
time_elpased: 1.591
batch start
#iterations: 42
currently lose_sum: 98.53312230110168
time_elpased: 1.678
batch start
#iterations: 43
currently lose_sum: 98.37327963113785
time_elpased: 1.644
batch start
#iterations: 44
currently lose_sum: 98.21865117549896
time_elpased: 1.644
batch start
#iterations: 45
currently lose_sum: 98.18889099359512
time_elpased: 1.634
batch start
#iterations: 46
currently lose_sum: 98.17916423082352
time_elpased: 1.663
batch start
#iterations: 47
currently lose_sum: 98.6564559340477
time_elpased: 1.652
batch start
#iterations: 48
currently lose_sum: 98.37220537662506
time_elpased: 1.621
batch start
#iterations: 49
currently lose_sum: 98.12296068668365
time_elpased: 1.665
batch start
#iterations: 50
currently lose_sum: 98.18579924106598
time_elpased: 1.649
batch start
#iterations: 51
currently lose_sum: 98.1808477640152
time_elpased: 1.642
batch start
#iterations: 52
currently lose_sum: 98.46347337961197
time_elpased: 1.621
batch start
#iterations: 53
currently lose_sum: 97.96524626016617
time_elpased: 1.651
batch start
#iterations: 54
currently lose_sum: 98.17600268125534
time_elpased: 1.621
batch start
#iterations: 55
currently lose_sum: 98.04001891613007
time_elpased: 1.673
batch start
#iterations: 56
currently lose_sum: 98.12917804718018
time_elpased: 1.638
batch start
#iterations: 57
currently lose_sum: 98.37227725982666
time_elpased: 1.675
batch start
#iterations: 58
currently lose_sum: 98.139229118824
time_elpased: 1.577
batch start
#iterations: 59
currently lose_sum: 98.38822782039642
time_elpased: 1.658
start validation test
0.600412371134
0.646488743104
0.446228259751
0.528007793473
0.600683065123
64.607
batch start
#iterations: 60
currently lose_sum: 98.16966331005096
time_elpased: 1.615
batch start
#iterations: 61
currently lose_sum: 98.29947119951248
time_elpased: 1.633
batch start
#iterations: 62
currently lose_sum: 98.06000357866287
time_elpased: 1.643
batch start
#iterations: 63
currently lose_sum: 98.07593184709549
time_elpased: 1.648
batch start
#iterations: 64
currently lose_sum: 98.37701892852783
time_elpased: 1.658
batch start
#iterations: 65
currently lose_sum: 97.99902826547623
time_elpased: 1.634
batch start
#iterations: 66
currently lose_sum: 98.30483675003052
time_elpased: 1.663
batch start
#iterations: 67
currently lose_sum: 97.76857304573059
time_elpased: 1.621
batch start
#iterations: 68
currently lose_sum: 97.98986631631851
time_elpased: 1.624
batch start
#iterations: 69
currently lose_sum: 98.19532364606857
time_elpased: 1.596
batch start
#iterations: 70
currently lose_sum: 98.21857386827469
time_elpased: 1.658
batch start
#iterations: 71
currently lose_sum: 98.10226929187775
time_elpased: 1.627
batch start
#iterations: 72
currently lose_sum: 97.80504232645035
time_elpased: 1.607
batch start
#iterations: 73
currently lose_sum: 98.12674486637115
time_elpased: 1.649
batch start
#iterations: 74
currently lose_sum: 97.99966806173325
time_elpased: 1.628
batch start
#iterations: 75
currently lose_sum: 97.92110258340836
time_elpased: 1.706
batch start
#iterations: 76
currently lose_sum: 97.96069186925888
time_elpased: 1.681
batch start
#iterations: 77
currently lose_sum: 98.33272421360016
time_elpased: 1.609
batch start
#iterations: 78
currently lose_sum: 98.01674658060074
time_elpased: 1.644
batch start
#iterations: 79
currently lose_sum: 98.02223068475723
time_elpased: 1.654
start validation test
0.600515463918
0.652409731908
0.433261294638
0.52071737786
0.600809104409
64.194
batch start
#iterations: 80
currently lose_sum: 98.22072750329971
time_elpased: 1.612
batch start
#iterations: 81
currently lose_sum: 98.08815026283264
time_elpased: 1.67
batch start
#iterations: 82
currently lose_sum: 98.1301554441452
time_elpased: 1.624
batch start
#iterations: 83
currently lose_sum: 97.719711124897
time_elpased: 1.714
batch start
#iterations: 84
currently lose_sum: 98.19500344991684
time_elpased: 1.648
batch start
#iterations: 85
currently lose_sum: 98.17921495437622
time_elpased: 1.671
batch start
#iterations: 86
currently lose_sum: 97.84191179275513
time_elpased: 1.64
batch start
#iterations: 87
currently lose_sum: 98.12095111608505
time_elpased: 1.659
batch start
#iterations: 88
currently lose_sum: 98.02579128742218
time_elpased: 1.688
batch start
#iterations: 89
currently lose_sum: 98.03238433599472
time_elpased: 1.662
batch start
#iterations: 90
currently lose_sum: 97.96589058637619
time_elpased: 1.624
batch start
#iterations: 91
currently lose_sum: 98.07449096441269
time_elpased: 1.7
batch start
#iterations: 92
currently lose_sum: 97.7899460196495
time_elpased: 1.679
batch start
#iterations: 93
currently lose_sum: 97.94134449958801
time_elpased: 1.619
batch start
#iterations: 94
currently lose_sum: 97.97965121269226
time_elpased: 1.688
batch start
#iterations: 95
currently lose_sum: 97.88495713472366
time_elpased: 1.602
batch start
#iterations: 96
currently lose_sum: 97.99994367361069
time_elpased: 1.602
batch start
#iterations: 97
currently lose_sum: 98.04618096351624
time_elpased: 1.679
batch start
#iterations: 98
currently lose_sum: 98.13156414031982
time_elpased: 1.636
batch start
#iterations: 99
currently lose_sum: 98.08356422185898
time_elpased: 1.656
start validation test
0.618762886598
0.636867555136
0.555727076258
0.593537041108
0.618873555686
63.804
batch start
#iterations: 100
currently lose_sum: 97.8628990650177
time_elpased: 1.651
batch start
#iterations: 101
currently lose_sum: 97.91611129045486
time_elpased: 1.698
batch start
#iterations: 102
currently lose_sum: 97.8416593670845
time_elpased: 1.612
batch start
#iterations: 103
currently lose_sum: 98.02791684865952
time_elpased: 1.651
batch start
#iterations: 104
currently lose_sum: 97.65926802158356
time_elpased: 1.66
batch start
#iterations: 105
currently lose_sum: 97.77550184726715
time_elpased: 1.677
batch start
#iterations: 106
currently lose_sum: 98.11267882585526
time_elpased: 1.682
batch start
#iterations: 107
currently lose_sum: 97.91961765289307
time_elpased: 1.668
batch start
#iterations: 108
currently lose_sum: 97.78703093528748
time_elpased: 1.655
batch start
#iterations: 109
currently lose_sum: 97.81705659627914
time_elpased: 1.664
batch start
#iterations: 110
currently lose_sum: 97.87950426340103
time_elpased: 1.648
batch start
#iterations: 111
currently lose_sum: 98.0753573179245
time_elpased: 1.646
batch start
#iterations: 112
currently lose_sum: 97.67454624176025
time_elpased: 1.616
batch start
#iterations: 113
currently lose_sum: 97.59687691926956
time_elpased: 1.674
batch start
#iterations: 114
currently lose_sum: 97.92787307500839
time_elpased: 1.612
batch start
#iterations: 115
currently lose_sum: 97.79457241296768
time_elpased: 1.695
batch start
#iterations: 116
currently lose_sum: 97.69764006137848
time_elpased: 1.629
batch start
#iterations: 117
currently lose_sum: 97.79572468996048
time_elpased: 1.68
batch start
#iterations: 118
currently lose_sum: 97.80433064699173
time_elpased: 1.685
batch start
#iterations: 119
currently lose_sum: 97.74879080057144
time_elpased: 1.637
start validation test
0.630567010309
0.644328729907
0.585777503345
0.61365964099
0.630645645197
63.595
batch start
#iterations: 120
currently lose_sum: 97.71197199821472
time_elpased: 1.632
batch start
#iterations: 121
currently lose_sum: 97.6575620174408
time_elpased: 1.633
batch start
#iterations: 122
currently lose_sum: 97.89852917194366
time_elpased: 1.667
batch start
#iterations: 123
currently lose_sum: 97.75829994678497
time_elpased: 1.676
batch start
#iterations: 124
currently lose_sum: 97.69201987981796
time_elpased: 1.601
batch start
#iterations: 125
currently lose_sum: 97.89923775196075
time_elpased: 1.686
batch start
#iterations: 126
currently lose_sum: 97.89853018522263
time_elpased: 1.619
batch start
#iterations: 127
currently lose_sum: 97.82515782117844
time_elpased: 1.694
batch start
#iterations: 128
currently lose_sum: 97.67042899131775
time_elpased: 1.593
batch start
#iterations: 129
currently lose_sum: 97.64755284786224
time_elpased: 1.645
batch start
#iterations: 130
currently lose_sum: 97.63722574710846
time_elpased: 1.66
batch start
#iterations: 131
currently lose_sum: 97.83713042736053
time_elpased: 1.659
batch start
#iterations: 132
currently lose_sum: 97.8086222410202
time_elpased: 1.69
batch start
#iterations: 133
currently lose_sum: 97.60362386703491
time_elpased: 1.653
batch start
#iterations: 134
currently lose_sum: 97.80710709095001
time_elpased: 1.665
batch start
#iterations: 135
currently lose_sum: 97.85190284252167
time_elpased: 1.626
batch start
#iterations: 136
currently lose_sum: 97.72458785772324
time_elpased: 1.663
batch start
#iterations: 137
currently lose_sum: 97.86303246021271
time_elpased: 1.67
batch start
#iterations: 138
currently lose_sum: 97.75250309705734
time_elpased: 1.662
batch start
#iterations: 139
currently lose_sum: 97.60117244720459
time_elpased: 1.69
start validation test
0.623711340206
0.639276247551
0.570855202223
0.60313145591
0.623804137309
63.558
batch start
#iterations: 140
currently lose_sum: 97.49883711338043
time_elpased: 1.641
batch start
#iterations: 141
currently lose_sum: 97.89920574426651
time_elpased: 1.658
batch start
#iterations: 142
currently lose_sum: 97.73276937007904
time_elpased: 1.611
batch start
#iterations: 143
currently lose_sum: 97.75262749195099
time_elpased: 1.68
batch start
#iterations: 144
currently lose_sum: 97.7624146938324
time_elpased: 1.623
batch start
#iterations: 145
currently lose_sum: 97.67840373516083
time_elpased: 1.622
batch start
#iterations: 146
currently lose_sum: 97.89311903715134
time_elpased: 1.676
batch start
#iterations: 147
currently lose_sum: 97.77803945541382
time_elpased: 1.638
batch start
#iterations: 148
currently lose_sum: 97.69672787189484
time_elpased: 1.686
batch start
#iterations: 149
currently lose_sum: 97.81089788675308
time_elpased: 1.715
batch start
#iterations: 150
currently lose_sum: 97.50981575250626
time_elpased: 1.677
batch start
#iterations: 151
currently lose_sum: 97.53386926651001
time_elpased: 1.667
batch start
#iterations: 152
currently lose_sum: 97.59932655096054
time_elpased: 1.688
batch start
#iterations: 153
currently lose_sum: 97.8787077665329
time_elpased: 1.686
batch start
#iterations: 154
currently lose_sum: 97.83237773180008
time_elpased: 1.611
batch start
#iterations: 155
currently lose_sum: 97.52664124965668
time_elpased: 1.707
batch start
#iterations: 156
currently lose_sum: 97.75391817092896
time_elpased: 1.669
batch start
#iterations: 157
currently lose_sum: 97.71515345573425
time_elpased: 1.694
batch start
#iterations: 158
currently lose_sum: 97.77439230680466
time_elpased: 1.674
batch start
#iterations: 159
currently lose_sum: 97.79237478971481
time_elpased: 1.639
start validation test
0.630824742268
0.634205273663
0.621282288772
0.627677271782
0.630841495517
63.317
batch start
#iterations: 160
currently lose_sum: 97.97455275058746
time_elpased: 1.609
batch start
#iterations: 161
currently lose_sum: 97.70697456598282
time_elpased: 1.741
batch start
#iterations: 162
currently lose_sum: 97.53167194128036
time_elpased: 1.626
batch start
#iterations: 163
currently lose_sum: 97.67333102226257
time_elpased: 1.64
batch start
#iterations: 164
currently lose_sum: 97.71758502721786
time_elpased: 1.637
batch start
#iterations: 165
currently lose_sum: 97.68512719869614
time_elpased: 1.604
batch start
#iterations: 166
currently lose_sum: 97.51524668931961
time_elpased: 1.644
batch start
#iterations: 167
currently lose_sum: 97.89213389158249
time_elpased: 1.644
batch start
#iterations: 168
currently lose_sum: 97.66965818405151
time_elpased: 1.655
batch start
#iterations: 169
currently lose_sum: 97.5370267033577
time_elpased: 1.638
batch start
#iterations: 170
currently lose_sum: 97.56334191560745
time_elpased: 1.657
batch start
#iterations: 171
currently lose_sum: 97.57642632722855
time_elpased: 1.713
batch start
#iterations: 172
currently lose_sum: 97.46516346931458
time_elpased: 1.627
batch start
#iterations: 173
currently lose_sum: 97.4831674695015
time_elpased: 1.705
batch start
#iterations: 174
currently lose_sum: 97.6796402335167
time_elpased: 1.632
batch start
#iterations: 175
currently lose_sum: 97.46691691875458
time_elpased: 1.629
batch start
#iterations: 176
currently lose_sum: 97.65160071849823
time_elpased: 1.629
batch start
#iterations: 177
currently lose_sum: 97.5478744506836
time_elpased: 1.629
batch start
#iterations: 178
currently lose_sum: 97.3007003068924
time_elpased: 1.628
batch start
#iterations: 179
currently lose_sum: 97.73475849628448
time_elpased: 1.642
start validation test
0.626340206186
0.636112949482
0.593495934959
0.614065910664
0.62639786937
63.593
batch start
#iterations: 180
currently lose_sum: 97.74360543489456
time_elpased: 1.724
batch start
#iterations: 181
currently lose_sum: 97.64287739992142
time_elpased: 1.667
batch start
#iterations: 182
currently lose_sum: 97.5273329615593
time_elpased: 1.66
batch start
#iterations: 183
currently lose_sum: 97.40789872407913
time_elpased: 1.597
batch start
#iterations: 184
currently lose_sum: 97.52088415622711
time_elpased: 1.644
batch start
#iterations: 185
currently lose_sum: 97.35359412431717
time_elpased: 1.61
batch start
#iterations: 186
currently lose_sum: 97.49095928668976
time_elpased: 1.653
batch start
#iterations: 187
currently lose_sum: 97.75102680921555
time_elpased: 1.649
batch start
#iterations: 188
currently lose_sum: 97.5223816037178
time_elpased: 1.639
batch start
#iterations: 189
currently lose_sum: 97.37385737895966
time_elpased: 1.615
batch start
#iterations: 190
currently lose_sum: 97.5188125371933
time_elpased: 1.677
batch start
#iterations: 191
currently lose_sum: 97.4941675066948
time_elpased: 1.661
batch start
#iterations: 192
currently lose_sum: 97.78006708621979
time_elpased: 1.628
batch start
#iterations: 193
currently lose_sum: 97.5800147652626
time_elpased: 1.657
batch start
#iterations: 194
currently lose_sum: 97.29353684186935
time_elpased: 1.626
batch start
#iterations: 195
currently lose_sum: 97.2939201593399
time_elpased: 1.632
batch start
#iterations: 196
currently lose_sum: 97.53506809473038
time_elpased: 1.658
batch start
#iterations: 197
currently lose_sum: 97.29358196258545
time_elpased: 1.622
batch start
#iterations: 198
currently lose_sum: 97.01048225164413
time_elpased: 1.634
batch start
#iterations: 199
currently lose_sum: 97.49714207649231
time_elpased: 1.677
start validation test
0.621907216495
0.642022418316
0.554080477514
0.594818538364
0.622026296797
63.584
batch start
#iterations: 200
currently lose_sum: 97.35502135753632
time_elpased: 1.671
batch start
#iterations: 201
currently lose_sum: 97.40528357028961
time_elpased: 1.676
batch start
#iterations: 202
currently lose_sum: 97.3590812087059
time_elpased: 1.665
batch start
#iterations: 203
currently lose_sum: 97.55880612134933
time_elpased: 1.641
batch start
#iterations: 204
currently lose_sum: 97.46366268396378
time_elpased: 1.614
batch start
#iterations: 205
currently lose_sum: 97.53311032056808
time_elpased: 1.654
batch start
#iterations: 206
currently lose_sum: 97.44488650560379
time_elpased: 1.646
batch start
#iterations: 207
currently lose_sum: 97.46416306495667
time_elpased: 1.611
batch start
#iterations: 208
currently lose_sum: 97.43747639656067
time_elpased: 1.674
batch start
#iterations: 209
currently lose_sum: 97.02703946828842
time_elpased: 1.589
batch start
#iterations: 210
currently lose_sum: 97.47720682621002
time_elpased: 1.619
batch start
#iterations: 211
currently lose_sum: 97.64179050922394
time_elpased: 1.614
batch start
#iterations: 212
currently lose_sum: 97.20967525243759
time_elpased: 1.635
batch start
#iterations: 213
currently lose_sum: 97.29386299848557
time_elpased: 1.641
batch start
#iterations: 214
currently lose_sum: 97.49219274520874
time_elpased: 1.669
batch start
#iterations: 215
currently lose_sum: 97.24869257211685
time_elpased: 1.583
batch start
#iterations: 216
currently lose_sum: 97.3481108546257
time_elpased: 1.631
batch start
#iterations: 217
currently lose_sum: 97.44952768087387
time_elpased: 1.666
batch start
#iterations: 218
currently lose_sum: 97.25779539346695
time_elpased: 1.68
batch start
#iterations: 219
currently lose_sum: 97.29200959205627
time_elpased: 1.661
start validation test
0.628969072165
0.645859872611
0.573942574869
0.607781168265
0.629065679668
63.436
batch start
#iterations: 220
currently lose_sum: 97.47757285833359
time_elpased: 1.641
batch start
#iterations: 221
currently lose_sum: 97.27292758226395
time_elpased: 1.608
batch start
#iterations: 222
currently lose_sum: 97.44487076997757
time_elpased: 1.672
batch start
#iterations: 223
currently lose_sum: 97.77116978168488
time_elpased: 1.64
batch start
#iterations: 224
currently lose_sum: 97.61899292469025
time_elpased: 1.655
batch start
#iterations: 225
currently lose_sum: 97.13172590732574
time_elpased: 1.692
batch start
#iterations: 226
currently lose_sum: 97.12808948755264
time_elpased: 1.612
batch start
#iterations: 227
currently lose_sum: 97.08231157064438
time_elpased: 1.628
batch start
#iterations: 228
currently lose_sum: 97.19911307096481
time_elpased: 1.634
batch start
#iterations: 229
currently lose_sum: 97.73920542001724
time_elpased: 1.649
batch start
#iterations: 230
currently lose_sum: 97.53015434741974
time_elpased: 1.582
batch start
#iterations: 231
currently lose_sum: 97.37565279006958
time_elpased: 1.588
batch start
#iterations: 232
currently lose_sum: 97.35115057229996
time_elpased: 1.613
batch start
#iterations: 233
currently lose_sum: 97.41373687982559
time_elpased: 1.637
batch start
#iterations: 234
currently lose_sum: 97.44587254524231
time_elpased: 1.658
batch start
#iterations: 235
currently lose_sum: 97.32587832212448
time_elpased: 1.638
batch start
#iterations: 236
currently lose_sum: 97.19477963447571
time_elpased: 1.644
batch start
#iterations: 237
currently lose_sum: 97.4249438047409
time_elpased: 1.63
batch start
#iterations: 238
currently lose_sum: 97.13045561313629
time_elpased: 1.664
batch start
#iterations: 239
currently lose_sum: 97.2972941994667
time_elpased: 1.633
start validation test
0.628505154639
0.636620944916
0.601831841103
0.618737766492
0.628551983755
63.371
batch start
#iterations: 240
currently lose_sum: 97.69794142246246
time_elpased: 1.676
batch start
#iterations: 241
currently lose_sum: 97.38952475786209
time_elpased: 1.619
batch start
#iterations: 242
currently lose_sum: 97.26110899448395
time_elpased: 1.673
batch start
#iterations: 243
currently lose_sum: 97.70963168144226
time_elpased: 1.599
batch start
#iterations: 244
currently lose_sum: 97.59146958589554
time_elpased: 1.659
batch start
#iterations: 245
currently lose_sum: 97.4167777299881
time_elpased: 1.659
batch start
#iterations: 246
currently lose_sum: 97.55537617206573
time_elpased: 1.607
batch start
#iterations: 247
currently lose_sum: 97.12026971578598
time_elpased: 1.67
batch start
#iterations: 248
currently lose_sum: 97.25568914413452
time_elpased: 1.692
batch start
#iterations: 249
currently lose_sum: 97.34227573871613
time_elpased: 1.647
batch start
#iterations: 250
currently lose_sum: 97.31531512737274
time_elpased: 1.687
batch start
#iterations: 251
currently lose_sum: 97.18215125799179
time_elpased: 1.647
batch start
#iterations: 252
currently lose_sum: 97.47607177495956
time_elpased: 1.658
batch start
#iterations: 253
currently lose_sum: 97.27568370103836
time_elpased: 1.589
batch start
#iterations: 254
currently lose_sum: 97.32864046096802
time_elpased: 1.659
batch start
#iterations: 255
currently lose_sum: 97.25163805484772
time_elpased: 1.734
batch start
#iterations: 256
currently lose_sum: 96.9437969326973
time_elpased: 1.658
batch start
#iterations: 257
currently lose_sum: 97.55437576770782
time_elpased: 1.629
batch start
#iterations: 258
currently lose_sum: 97.06825256347656
time_elpased: 1.591
batch start
#iterations: 259
currently lose_sum: 97.26006281375885
time_elpased: 1.655
start validation test
0.629948453608
0.634513461946
0.616033755274
0.62513706856
0.629972883007
63.294
batch start
#iterations: 260
currently lose_sum: 97.14402574300766
time_elpased: 1.656
batch start
#iterations: 261
currently lose_sum: 97.2851402759552
time_elpased: 1.626
batch start
#iterations: 262
currently lose_sum: 97.27692091464996
time_elpased: 1.64
batch start
#iterations: 263
currently lose_sum: 97.15293592214584
time_elpased: 1.641
batch start
#iterations: 264
currently lose_sum: 97.60385543107986
time_elpased: 1.622
batch start
#iterations: 265
currently lose_sum: 97.45739835500717
time_elpased: 1.646
batch start
#iterations: 266
currently lose_sum: 97.23525297641754
time_elpased: 1.663
batch start
#iterations: 267
currently lose_sum: 97.0715708732605
time_elpased: 1.621
batch start
#iterations: 268
currently lose_sum: 97.58360230922699
time_elpased: 1.65
batch start
#iterations: 269
currently lose_sum: 97.4421734213829
time_elpased: 1.63
batch start
#iterations: 270
currently lose_sum: 97.33801174163818
time_elpased: 1.643
batch start
#iterations: 271
currently lose_sum: 97.31219899654388
time_elpased: 1.65
batch start
#iterations: 272
currently lose_sum: 97.32266056537628
time_elpased: 1.663
batch start
#iterations: 273
currently lose_sum: 97.54313498735428
time_elpased: 1.625
batch start
#iterations: 274
currently lose_sum: 97.59543526172638
time_elpased: 1.662
batch start
#iterations: 275
currently lose_sum: 97.43666213750839
time_elpased: 1.616
batch start
#iterations: 276
currently lose_sum: 97.36487293243408
time_elpased: 1.649
batch start
#iterations: 277
currently lose_sum: 97.31959283351898
time_elpased: 1.644
batch start
#iterations: 278
currently lose_sum: 97.34012466669083
time_elpased: 1.658
batch start
#iterations: 279
currently lose_sum: 97.12393420934677
time_elpased: 1.647
start validation test
0.632113402062
0.640369967356
0.6056396007
0.622520759507
0.632159880903
63.413
batch start
#iterations: 280
currently lose_sum: 97.4493932723999
time_elpased: 1.575
batch start
#iterations: 281
currently lose_sum: 97.19589805603027
time_elpased: 1.702
batch start
#iterations: 282
currently lose_sum: 97.33818525075912
time_elpased: 1.62
batch start
#iterations: 283
currently lose_sum: 97.18443465232849
time_elpased: 1.669
batch start
#iterations: 284
currently lose_sum: 96.9753190279007
time_elpased: 1.735
batch start
#iterations: 285
currently lose_sum: 97.09685730934143
time_elpased: 1.647
batch start
#iterations: 286
currently lose_sum: 97.49898117780685
time_elpased: 1.664
batch start
#iterations: 287
currently lose_sum: 97.14310002326965
time_elpased: 1.601
batch start
#iterations: 288
currently lose_sum: 97.25000393390656
time_elpased: 1.665
batch start
#iterations: 289
currently lose_sum: 97.10197311639786
time_elpased: 1.688
batch start
#iterations: 290
currently lose_sum: 97.27521115541458
time_elpased: 1.674
batch start
#iterations: 291
currently lose_sum: 96.99226778745651
time_elpased: 1.632
batch start
#iterations: 292
currently lose_sum: 97.40565806627274
time_elpased: 1.628
batch start
#iterations: 293
currently lose_sum: 97.23997443914413
time_elpased: 1.632
batch start
#iterations: 294
currently lose_sum: 96.92506664991379
time_elpased: 1.677
batch start
#iterations: 295
currently lose_sum: 97.05124819278717
time_elpased: 1.616
batch start
#iterations: 296
currently lose_sum: 97.2571548819542
time_elpased: 1.606
batch start
#iterations: 297
currently lose_sum: 97.58925503492355
time_elpased: 1.715
batch start
#iterations: 298
currently lose_sum: 97.10331761837006
time_elpased: 1.607
batch start
#iterations: 299
currently lose_sum: 97.01385551691055
time_elpased: 1.643
start validation test
0.628711340206
0.64158594278
0.586189153031
0.612637805862
0.628785994464
63.413
batch start
#iterations: 300
currently lose_sum: 97.22923451662064
time_elpased: 1.63
batch start
#iterations: 301
currently lose_sum: 97.31912994384766
time_elpased: 1.647
batch start
#iterations: 302
currently lose_sum: 97.38088721036911
time_elpased: 1.651
batch start
#iterations: 303
currently lose_sum: 97.01004910469055
time_elpased: 1.674
batch start
#iterations: 304
currently lose_sum: 97.1359099149704
time_elpased: 1.654
batch start
#iterations: 305
currently lose_sum: 97.16921359300613
time_elpased: 1.616
batch start
#iterations: 306
currently lose_sum: 97.09634047746658
time_elpased: 1.669
batch start
#iterations: 307
currently lose_sum: 97.25170826911926
time_elpased: 1.66
batch start
#iterations: 308
currently lose_sum: 97.38777846097946
time_elpased: 1.62
batch start
#iterations: 309
currently lose_sum: 97.1523362994194
time_elpased: 1.613
batch start
#iterations: 310
currently lose_sum: 97.41890394687653
time_elpased: 1.616
batch start
#iterations: 311
currently lose_sum: 97.2397700548172
time_elpased: 1.688
batch start
#iterations: 312
currently lose_sum: 97.27852654457092
time_elpased: 1.664
batch start
#iterations: 313
currently lose_sum: 97.15690124034882
time_elpased: 1.642
batch start
#iterations: 314
currently lose_sum: 96.91449308395386
time_elpased: 1.66
batch start
#iterations: 315
currently lose_sum: 96.9167063832283
time_elpased: 1.59
batch start
#iterations: 316
currently lose_sum: 97.13409328460693
time_elpased: 1.616
batch start
#iterations: 317
currently lose_sum: 97.33182173967361
time_elpased: 1.627
batch start
#iterations: 318
currently lose_sum: 97.11964905261993
time_elpased: 1.62
batch start
#iterations: 319
currently lose_sum: 97.38768434524536
time_elpased: 1.682
start validation test
0.629432989691
0.639145750771
0.597509519399
0.617626721983
0.629489036267
63.385
batch start
#iterations: 320
currently lose_sum: 97.01346224546432
time_elpased: 1.676
batch start
#iterations: 321
currently lose_sum: 96.9275820851326
time_elpased: 1.655
batch start
#iterations: 322
currently lose_sum: 96.92692297697067
time_elpased: 1.654
batch start
#iterations: 323
currently lose_sum: 96.9900529384613
time_elpased: 1.653
batch start
#iterations: 324
currently lose_sum: 97.1710597872734
time_elpased: 1.649
batch start
#iterations: 325
currently lose_sum: 97.49520814418793
time_elpased: 1.695
batch start
#iterations: 326
currently lose_sum: 97.06886720657349
time_elpased: 1.581
batch start
#iterations: 327
currently lose_sum: 97.10751342773438
time_elpased: 1.666
batch start
#iterations: 328
currently lose_sum: 97.19804513454437
time_elpased: 1.663
batch start
#iterations: 329
currently lose_sum: 96.9764713048935
time_elpased: 1.655
batch start
#iterations: 330
currently lose_sum: 97.34173732995987
time_elpased: 1.679
batch start
#iterations: 331
currently lose_sum: 97.19059604406357
time_elpased: 1.616
batch start
#iterations: 332
currently lose_sum: 97.1591324210167
time_elpased: 1.712
batch start
#iterations: 333
currently lose_sum: 97.39572334289551
time_elpased: 1.653
batch start
#iterations: 334
currently lose_sum: 97.29781436920166
time_elpased: 1.638
batch start
#iterations: 335
currently lose_sum: 97.04520577192307
time_elpased: 1.67
batch start
#iterations: 336
currently lose_sum: 97.47292625904083
time_elpased: 1.62
batch start
#iterations: 337
currently lose_sum: 97.0339183807373
time_elpased: 1.628
batch start
#iterations: 338
currently lose_sum: 97.06629818677902
time_elpased: 1.615
batch start
#iterations: 339
currently lose_sum: 97.19034552574158
time_elpased: 1.608
start validation test
0.629484536082
0.64054684895
0.593084285273
0.615902532863
0.629548442337
63.317
batch start
#iterations: 340
currently lose_sum: 97.22140550613403
time_elpased: 1.575
batch start
#iterations: 341
currently lose_sum: 97.42816001176834
time_elpased: 1.691
batch start
#iterations: 342
currently lose_sum: 97.31921857595444
time_elpased: 1.599
batch start
#iterations: 343
currently lose_sum: 97.16789638996124
time_elpased: 1.649
batch start
#iterations: 344
currently lose_sum: 97.0748918056488
time_elpased: 1.617
batch start
#iterations: 345
currently lose_sum: 97.19018185138702
time_elpased: 1.605
batch start
#iterations: 346
currently lose_sum: 97.0486689209938
time_elpased: 1.636
batch start
#iterations: 347
currently lose_sum: 97.22914093732834
time_elpased: 1.672
batch start
#iterations: 348
currently lose_sum: 97.21490031480789
time_elpased: 1.643
batch start
#iterations: 349
currently lose_sum: 97.10938835144043
time_elpased: 1.596
batch start
#iterations: 350
currently lose_sum: 97.36334818601608
time_elpased: 1.646
batch start
#iterations: 351
currently lose_sum: 97.0197856426239
time_elpased: 1.628
batch start
#iterations: 352
currently lose_sum: 97.21974921226501
time_elpased: 1.707
batch start
#iterations: 353
currently lose_sum: 97.28767091035843
time_elpased: 1.645
batch start
#iterations: 354
currently lose_sum: 96.93021565675735
time_elpased: 1.627
batch start
#iterations: 355
currently lose_sum: 97.38474768400192
time_elpased: 1.605
batch start
#iterations: 356
currently lose_sum: 97.05684554576874
time_elpased: 1.645
batch start
#iterations: 357
currently lose_sum: 97.34624975919724
time_elpased: 1.609
batch start
#iterations: 358
currently lose_sum: 97.03530019521713
time_elpased: 1.686
batch start
#iterations: 359
currently lose_sum: 97.15604436397552
time_elpased: 1.597
start validation test
0.626134020619
0.642263279446
0.572398888546
0.605321869728
0.626228360931
63.492
batch start
#iterations: 360
currently lose_sum: 97.20523643493652
time_elpased: 1.666
batch start
#iterations: 361
currently lose_sum: 97.4118914604187
time_elpased: 1.627
batch start
#iterations: 362
currently lose_sum: 97.26014196872711
time_elpased: 1.673
batch start
#iterations: 363
currently lose_sum: 96.96676939725876
time_elpased: 1.67
batch start
#iterations: 364
currently lose_sum: 96.78144347667694
time_elpased: 1.679
batch start
#iterations: 365
currently lose_sum: 97.48053085803986
time_elpased: 1.641
batch start
#iterations: 366
currently lose_sum: 97.29505032300949
time_elpased: 1.634
batch start
#iterations: 367
currently lose_sum: 97.11318475008011
time_elpased: 1.588
batch start
#iterations: 368
currently lose_sum: 97.28614085912704
time_elpased: 1.618
batch start
#iterations: 369
currently lose_sum: 97.23846524953842
time_elpased: 1.643
batch start
#iterations: 370
currently lose_sum: 97.02416259050369
time_elpased: 1.613
batch start
#iterations: 371
currently lose_sum: 97.05382877588272
time_elpased: 1.639
batch start
#iterations: 372
currently lose_sum: 97.21537762880325
time_elpased: 1.661
batch start
#iterations: 373
currently lose_sum: 97.00655204057693
time_elpased: 1.639
batch start
#iterations: 374
currently lose_sum: 97.12623071670532
time_elpased: 1.664
batch start
#iterations: 375
currently lose_sum: 97.2016174197197
time_elpased: 1.614
batch start
#iterations: 376
currently lose_sum: 97.08332985639572
time_elpased: 1.664
batch start
#iterations: 377
currently lose_sum: 97.13530403375626
time_elpased: 1.668
batch start
#iterations: 378
currently lose_sum: 97.03223073482513
time_elpased: 1.689
batch start
#iterations: 379
currently lose_sum: 97.49761837720871
time_elpased: 1.64
start validation test
0.623402061856
0.64442314604
0.553565915406
0.595549158547
0.623524669982
63.628
batch start
#iterations: 380
currently lose_sum: 97.17709892988205
time_elpased: 1.62
batch start
#iterations: 381
currently lose_sum: 97.196513235569
time_elpased: 1.614
batch start
#iterations: 382
currently lose_sum: 96.99176847934723
time_elpased: 1.623
batch start
#iterations: 383
currently lose_sum: 96.90153419971466
time_elpased: 1.618
batch start
#iterations: 384
currently lose_sum: 97.08300334215164
time_elpased: 1.627
batch start
#iterations: 385
currently lose_sum: 97.47049587965012
time_elpased: 1.61
batch start
#iterations: 386
currently lose_sum: 97.29146015644073
time_elpased: 1.667
batch start
#iterations: 387
currently lose_sum: 97.12482643127441
time_elpased: 1.615
batch start
#iterations: 388
currently lose_sum: 96.81716060638428
time_elpased: 1.656
batch start
#iterations: 389
currently lose_sum: 97.05211669206619
time_elpased: 1.633
batch start
#iterations: 390
currently lose_sum: 97.4494132399559
time_elpased: 1.653
batch start
#iterations: 391
currently lose_sum: 97.22090822458267
time_elpased: 1.679
batch start
#iterations: 392
currently lose_sum: 97.2286930680275
time_elpased: 1.634
batch start
#iterations: 393
currently lose_sum: 96.73559391498566
time_elpased: 1.724
batch start
#iterations: 394
currently lose_sum: 96.95222306251526
time_elpased: 1.682
batch start
#iterations: 395
currently lose_sum: 97.05203247070312
time_elpased: 1.611
batch start
#iterations: 396
currently lose_sum: 97.31094670295715
time_elpased: 1.681
batch start
#iterations: 397
currently lose_sum: 97.15596383810043
time_elpased: 1.697
batch start
#iterations: 398
currently lose_sum: 97.27543693780899
time_elpased: 1.605
batch start
#iterations: 399
currently lose_sum: 97.0205888748169
time_elpased: 1.672
start validation test
0.628092783505
0.641997729852
0.58207265617
0.610568359691
0.628173578937
63.458
acc: 0.634
pre: 0.639
rec: 0.621
F1: 0.630
auc: 0.669
