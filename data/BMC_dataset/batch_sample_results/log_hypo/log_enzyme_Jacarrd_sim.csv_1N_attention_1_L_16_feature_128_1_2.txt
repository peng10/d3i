start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.67102432250977
time_elpased: 1.794
batch start
#iterations: 1
currently lose_sum: 100.29894733428955
time_elpased: 1.751
batch start
#iterations: 2
currently lose_sum: 100.04216665029526
time_elpased: 1.759
batch start
#iterations: 3
currently lose_sum: 99.86264151334763
time_elpased: 1.798
batch start
#iterations: 4
currently lose_sum: 99.71434217691422
time_elpased: 1.752
batch start
#iterations: 5
currently lose_sum: 99.5826672911644
time_elpased: 1.714
batch start
#iterations: 6
currently lose_sum: 99.39366871118546
time_elpased: 1.785
batch start
#iterations: 7
currently lose_sum: 99.45087969303131
time_elpased: 1.752
batch start
#iterations: 8
currently lose_sum: 99.44223827123642
time_elpased: 1.736
batch start
#iterations: 9
currently lose_sum: 99.27859568595886
time_elpased: 1.763
batch start
#iterations: 10
currently lose_sum: 99.24408113956451
time_elpased: 1.719
batch start
#iterations: 11
currently lose_sum: 99.29140239953995
time_elpased: 1.756
batch start
#iterations: 12
currently lose_sum: 99.09605699777603
time_elpased: 1.788
batch start
#iterations: 13
currently lose_sum: 99.2038624882698
time_elpased: 1.738
batch start
#iterations: 14
currently lose_sum: 98.88456219434738
time_elpased: 1.745
batch start
#iterations: 15
currently lose_sum: 98.97435516119003
time_elpased: 1.818
batch start
#iterations: 16
currently lose_sum: 98.74989885091782
time_elpased: 1.739
batch start
#iterations: 17
currently lose_sum: 98.85500460863113
time_elpased: 1.744
batch start
#iterations: 18
currently lose_sum: 98.88948088884354
time_elpased: 1.755
batch start
#iterations: 19
currently lose_sum: 98.73284870386124
time_elpased: 1.823
start validation test
0.617422680412
0.619518800125
0.612123083256
0.615798736929
0.617431984673
64.373
batch start
#iterations: 20
currently lose_sum: 98.61084240674973
time_elpased: 1.754
batch start
#iterations: 21
currently lose_sum: 98.55156260728836
time_elpased: 1.76
batch start
#iterations: 22
currently lose_sum: 98.62859719991684
time_elpased: 1.765
batch start
#iterations: 23
currently lose_sum: 98.73758751153946
time_elpased: 1.719
batch start
#iterations: 24
currently lose_sum: 98.54778814315796
time_elpased: 1.741
batch start
#iterations: 25
currently lose_sum: 98.7285915017128
time_elpased: 1.804
batch start
#iterations: 26
currently lose_sum: 98.48373264074326
time_elpased: 1.757
batch start
#iterations: 27
currently lose_sum: 98.5643640756607
time_elpased: 1.73
batch start
#iterations: 28
currently lose_sum: 98.44985949993134
time_elpased: 1.753
batch start
#iterations: 29
currently lose_sum: 98.33043193817139
time_elpased: 1.744
batch start
#iterations: 30
currently lose_sum: 98.4693813920021
time_elpased: 1.793
batch start
#iterations: 31
currently lose_sum: 98.38882493972778
time_elpased: 1.748
batch start
#iterations: 32
currently lose_sum: 98.61164253950119
time_elpased: 1.756
batch start
#iterations: 33
currently lose_sum: 98.74732005596161
time_elpased: 1.733
batch start
#iterations: 34
currently lose_sum: 98.18481850624084
time_elpased: 1.787
batch start
#iterations: 35
currently lose_sum: 98.49297821521759
time_elpased: 1.765
batch start
#iterations: 36
currently lose_sum: 98.27146995067596
time_elpased: 1.778
batch start
#iterations: 37
currently lose_sum: 98.41201883554459
time_elpased: 1.821
batch start
#iterations: 38
currently lose_sum: 98.11742734909058
time_elpased: 1.745
batch start
#iterations: 39
currently lose_sum: 98.22427713871002
time_elpased: 1.778
start validation test
0.622474226804
0.633791792463
0.583307605228
0.607502679528
0.622542989849
63.941
batch start
#iterations: 40
currently lose_sum: 98.12423026561737
time_elpased: 1.772
batch start
#iterations: 41
currently lose_sum: 98.24584811925888
time_elpased: 1.791
batch start
#iterations: 42
currently lose_sum: 98.07027024030685
time_elpased: 1.728
batch start
#iterations: 43
currently lose_sum: 98.19578284025192
time_elpased: 1.758
batch start
#iterations: 44
currently lose_sum: 98.27299338579178
time_elpased: 1.732
batch start
#iterations: 45
currently lose_sum: 98.24110037088394
time_elpased: 1.765
batch start
#iterations: 46
currently lose_sum: 98.07399946451187
time_elpased: 1.806
batch start
#iterations: 47
currently lose_sum: 98.27019596099854
time_elpased: 1.793
batch start
#iterations: 48
currently lose_sum: 98.20418190956116
time_elpased: 1.787
batch start
#iterations: 49
currently lose_sum: 98.14797961711884
time_elpased: 1.754
batch start
#iterations: 50
currently lose_sum: 98.12707716226578
time_elpased: 1.792
batch start
#iterations: 51
currently lose_sum: 98.13505285978317
time_elpased: 1.74
batch start
#iterations: 52
currently lose_sum: 98.02925479412079
time_elpased: 1.748
batch start
#iterations: 53
currently lose_sum: 97.85912573337555
time_elpased: 1.76
batch start
#iterations: 54
currently lose_sum: 98.00171548128128
time_elpased: 1.744
batch start
#iterations: 55
currently lose_sum: 98.21659821271896
time_elpased: 1.771
batch start
#iterations: 56
currently lose_sum: 97.99897736310959
time_elpased: 1.77
batch start
#iterations: 57
currently lose_sum: 98.11763733625412
time_elpased: 1.764
batch start
#iterations: 58
currently lose_sum: 98.04210239648819
time_elpased: 1.735
batch start
#iterations: 59
currently lose_sum: 98.19923621416092
time_elpased: 1.785
start validation test
0.612731958763
0.64511456413
0.504167953072
0.565998498065
0.61292255962
63.990
batch start
#iterations: 60
currently lose_sum: 98.00824707746506
time_elpased: 1.804
batch start
#iterations: 61
currently lose_sum: 98.20298856496811
time_elpased: 1.746
batch start
#iterations: 62
currently lose_sum: 98.25592547655106
time_elpased: 1.74
batch start
#iterations: 63
currently lose_sum: 97.77236014604568
time_elpased: 1.777
batch start
#iterations: 64
currently lose_sum: 97.85532605648041
time_elpased: 1.724
batch start
#iterations: 65
currently lose_sum: 98.07040345668793
time_elpased: 1.793
batch start
#iterations: 66
currently lose_sum: 98.13735681772232
time_elpased: 1.747
batch start
#iterations: 67
currently lose_sum: 97.7108251452446
time_elpased: 1.759
batch start
#iterations: 68
currently lose_sum: 98.0957316160202
time_elpased: 1.712
batch start
#iterations: 69
currently lose_sum: 97.90616714954376
time_elpased: 1.722
batch start
#iterations: 70
currently lose_sum: 98.02593785524368
time_elpased: 1.757
batch start
#iterations: 71
currently lose_sum: 97.82531803846359
time_elpased: 1.74
batch start
#iterations: 72
currently lose_sum: 98.04234117269516
time_elpased: 1.778
batch start
#iterations: 73
currently lose_sum: 97.95239114761353
time_elpased: 1.758
batch start
#iterations: 74
currently lose_sum: 98.39520746469498
time_elpased: 1.745
batch start
#iterations: 75
currently lose_sum: 97.93355309963226
time_elpased: 1.794
batch start
#iterations: 76
currently lose_sum: 98.02823621034622
time_elpased: 1.767
batch start
#iterations: 77
currently lose_sum: 98.06027460098267
time_elpased: 1.783
batch start
#iterations: 78
currently lose_sum: 98.21729099750519
time_elpased: 1.773
batch start
#iterations: 79
currently lose_sum: 97.9279272556305
time_elpased: 1.746
start validation test
0.62675257732
0.643819702602
0.570340640115
0.60485675307
0.626851617176
63.497
batch start
#iterations: 80
currently lose_sum: 98.00641506910324
time_elpased: 1.779
batch start
#iterations: 81
currently lose_sum: 97.74227231740952
time_elpased: 1.793
batch start
#iterations: 82
currently lose_sum: 97.94709265232086
time_elpased: 1.753
batch start
#iterations: 83
currently lose_sum: 97.98414289951324
time_elpased: 1.765
batch start
#iterations: 84
currently lose_sum: 97.63555425405502
time_elpased: 1.728
batch start
#iterations: 85
currently lose_sum: 97.90784746408463
time_elpased: 1.751
batch start
#iterations: 86
currently lose_sum: 97.90674263238907
time_elpased: 1.776
batch start
#iterations: 87
currently lose_sum: 98.02939081192017
time_elpased: 1.761
batch start
#iterations: 88
currently lose_sum: 97.75612777471542
time_elpased: 1.736
batch start
#iterations: 89
currently lose_sum: 98.1225478053093
time_elpased: 1.752
batch start
#iterations: 90
currently lose_sum: 97.80273306369781
time_elpased: 1.728
batch start
#iterations: 91
currently lose_sum: 98.08109825849533
time_elpased: 1.772
batch start
#iterations: 92
currently lose_sum: 97.66854590177536
time_elpased: 1.781
batch start
#iterations: 93
currently lose_sum: 97.87988412380219
time_elpased: 1.753
batch start
#iterations: 94
currently lose_sum: 97.90469533205032
time_elpased: 1.749
batch start
#iterations: 95
currently lose_sum: 97.8680032491684
time_elpased: 1.777
batch start
#iterations: 96
currently lose_sum: 97.97610580921173
time_elpased: 1.754
batch start
#iterations: 97
currently lose_sum: 97.6498253941536
time_elpased: 1.759
batch start
#iterations: 98
currently lose_sum: 98.20676761865616
time_elpased: 1.787
batch start
#iterations: 99
currently lose_sum: 97.78928971290588
time_elpased: 1.741
start validation test
0.627474226804
0.636006117544
0.599156118143
0.617031423878
0.627523943612
63.507
batch start
#iterations: 100
currently lose_sum: 97.80195474624634
time_elpased: 1.775
batch start
#iterations: 101
currently lose_sum: 97.91340273618698
time_elpased: 1.734
batch start
#iterations: 102
currently lose_sum: 97.8421779870987
time_elpased: 1.753
batch start
#iterations: 103
currently lose_sum: 97.95073235034943
time_elpased: 1.797
batch start
#iterations: 104
currently lose_sum: 97.92127674818039
time_elpased: 1.774
batch start
#iterations: 105
currently lose_sum: 97.66731345653534
time_elpased: 1.723
batch start
#iterations: 106
currently lose_sum: 97.83088302612305
time_elpased: 1.751
batch start
#iterations: 107
currently lose_sum: 98.06252139806747
time_elpased: 1.765
batch start
#iterations: 108
currently lose_sum: 97.95527386665344
time_elpased: 1.744
batch start
#iterations: 109
currently lose_sum: 97.62043184041977
time_elpased: 1.778
batch start
#iterations: 110
currently lose_sum: 97.68466192483902
time_elpased: 1.762
batch start
#iterations: 111
currently lose_sum: 97.61984831094742
time_elpased: 1.774
batch start
#iterations: 112
currently lose_sum: 97.75011849403381
time_elpased: 1.776
batch start
#iterations: 113
currently lose_sum: 97.73790937662125
time_elpased: 1.794
batch start
#iterations: 114
currently lose_sum: 97.47166514396667
time_elpased: 1.738
batch start
#iterations: 115
currently lose_sum: 97.70959532260895
time_elpased: 1.826
batch start
#iterations: 116
currently lose_sum: 97.76343232393265
time_elpased: 1.796
batch start
#iterations: 117
currently lose_sum: 97.61489933729172
time_elpased: 1.736
batch start
#iterations: 118
currently lose_sum: 97.81634610891342
time_elpased: 1.737
batch start
#iterations: 119
currently lose_sum: 97.70218062400818
time_elpased: 1.733
start validation test
0.623092783505
0.63845710996
0.57064937738
0.602651885665
0.623184855993
63.548
batch start
#iterations: 120
currently lose_sum: 97.50858581066132
time_elpased: 1.741
batch start
#iterations: 121
currently lose_sum: 97.52811849117279
time_elpased: 1.803
batch start
#iterations: 122
currently lose_sum: 97.99523568153381
time_elpased: 1.752
batch start
#iterations: 123
currently lose_sum: 97.80513000488281
time_elpased: 1.759
batch start
#iterations: 124
currently lose_sum: 97.79596638679504
time_elpased: 1.773
batch start
#iterations: 125
currently lose_sum: 98.08959871530533
time_elpased: 1.782
batch start
#iterations: 126
currently lose_sum: 97.77122575044632
time_elpased: 1.736
batch start
#iterations: 127
currently lose_sum: 97.65324461460114
time_elpased: 1.736
batch start
#iterations: 128
currently lose_sum: 97.45670711994171
time_elpased: 1.774
batch start
#iterations: 129
currently lose_sum: 98.12785303592682
time_elpased: 1.712
batch start
#iterations: 130
currently lose_sum: 97.71777039766312
time_elpased: 1.761
batch start
#iterations: 131
currently lose_sum: 97.89868915081024
time_elpased: 1.741
batch start
#iterations: 132
currently lose_sum: 97.73334181308746
time_elpased: 1.777
batch start
#iterations: 133
currently lose_sum: 97.7748139500618
time_elpased: 1.746
batch start
#iterations: 134
currently lose_sum: 97.85339277982712
time_elpased: 1.726
batch start
#iterations: 135
currently lose_sum: 97.74686580896378
time_elpased: 1.773
batch start
#iterations: 136
currently lose_sum: 97.2540642619133
time_elpased: 1.773
batch start
#iterations: 137
currently lose_sum: 97.42418015003204
time_elpased: 1.763
batch start
#iterations: 138
currently lose_sum: 97.84908431768417
time_elpased: 1.772
batch start
#iterations: 139
currently lose_sum: 97.61107814311981
time_elpased: 1.784
start validation test
0.628917525773
0.64558279371
0.574560049398
0.608004356112
0.629012958707
63.542
batch start
#iterations: 140
currently lose_sum: 97.63250696659088
time_elpased: 1.75
batch start
#iterations: 141
currently lose_sum: 97.75975733995438
time_elpased: 1.741
batch start
#iterations: 142
currently lose_sum: 97.83058124780655
time_elpased: 1.749
batch start
#iterations: 143
currently lose_sum: 97.8202583193779
time_elpased: 1.734
batch start
#iterations: 144
currently lose_sum: 97.76867133378983
time_elpased: 1.751
batch start
#iterations: 145
currently lose_sum: 97.79333573579788
time_elpased: 1.758
batch start
#iterations: 146
currently lose_sum: 97.54828125238419
time_elpased: 1.771
batch start
#iterations: 147
currently lose_sum: 97.46457082033157
time_elpased: 1.809
batch start
#iterations: 148
currently lose_sum: 97.59206449985504
time_elpased: 1.76
batch start
#iterations: 149
currently lose_sum: 97.7088098526001
time_elpased: 1.74
batch start
#iterations: 150
currently lose_sum: 97.67764848470688
time_elpased: 1.761
batch start
#iterations: 151
currently lose_sum: 97.67409926652908
time_elpased: 1.789
batch start
#iterations: 152
currently lose_sum: 97.48677086830139
time_elpased: 1.773
batch start
#iterations: 153
currently lose_sum: 97.86782866716385
time_elpased: 1.72
batch start
#iterations: 154
currently lose_sum: 97.79693537950516
time_elpased: 1.756
batch start
#iterations: 155
currently lose_sum: 97.70056122541428
time_elpased: 1.742
batch start
#iterations: 156
currently lose_sum: 97.52514237165451
time_elpased: 1.71
batch start
#iterations: 157
currently lose_sum: 97.27279597520828
time_elpased: 1.732
batch start
#iterations: 158
currently lose_sum: 97.69651180505753
time_elpased: 1.774
batch start
#iterations: 159
currently lose_sum: 97.73743808269501
time_elpased: 1.795
start validation test
0.626340206186
0.642956441149
0.571163939487
0.604937598779
0.626437076632
63.433
batch start
#iterations: 160
currently lose_sum: 97.48803353309631
time_elpased: 1.761
batch start
#iterations: 161
currently lose_sum: 97.83891201019287
time_elpased: 1.793
batch start
#iterations: 162
currently lose_sum: 97.59542399644852
time_elpased: 1.765
batch start
#iterations: 163
currently lose_sum: 97.93846499919891
time_elpased: 1.748
batch start
#iterations: 164
currently lose_sum: 97.45686888694763
time_elpased: 1.772
batch start
#iterations: 165
currently lose_sum: 97.80472314357758
time_elpased: 1.763
batch start
#iterations: 166
currently lose_sum: 97.34869062900543
time_elpased: 1.784
batch start
#iterations: 167
currently lose_sum: 97.66800689697266
time_elpased: 1.75
batch start
#iterations: 168
currently lose_sum: 97.4767375588417
time_elpased: 1.724
batch start
#iterations: 169
currently lose_sum: 97.66982245445251
time_elpased: 1.821
batch start
#iterations: 170
currently lose_sum: 97.94825720787048
time_elpased: 1.747
batch start
#iterations: 171
currently lose_sum: 97.82193511724472
time_elpased: 1.775
batch start
#iterations: 172
currently lose_sum: 97.66940253973007
time_elpased: 1.748
batch start
#iterations: 173
currently lose_sum: 97.86146360635757
time_elpased: 1.806
batch start
#iterations: 174
currently lose_sum: 97.33601182699203
time_elpased: 1.819
batch start
#iterations: 175
currently lose_sum: 97.48351842164993
time_elpased: 1.739
batch start
#iterations: 176
currently lose_sum: 97.4439508318901
time_elpased: 1.771
batch start
#iterations: 177
currently lose_sum: 97.56529188156128
time_elpased: 1.824
batch start
#iterations: 178
currently lose_sum: 97.8994756937027
time_elpased: 1.751
batch start
#iterations: 179
currently lose_sum: 97.77444541454315
time_elpased: 1.753
start validation test
0.628762886598
0.640549904996
0.589791087784
0.614123446207
0.628831307602
63.588
batch start
#iterations: 180
currently lose_sum: 97.67058634757996
time_elpased: 1.764
batch start
#iterations: 181
currently lose_sum: 97.6905665397644
time_elpased: 1.728
batch start
#iterations: 182
currently lose_sum: 97.31372851133347
time_elpased: 1.785
batch start
#iterations: 183
currently lose_sum: 97.49500489234924
time_elpased: 1.812
batch start
#iterations: 184
currently lose_sum: 97.62562263011932
time_elpased: 1.754
batch start
#iterations: 185
currently lose_sum: 97.82804697751999
time_elpased: 1.78
batch start
#iterations: 186
currently lose_sum: 97.72900021076202
time_elpased: 1.753
batch start
#iterations: 187
currently lose_sum: 97.39133149385452
time_elpased: 1.749
batch start
#iterations: 188
currently lose_sum: 97.83020609617233
time_elpased: 1.741
batch start
#iterations: 189
currently lose_sum: 97.47467303276062
time_elpased: 1.76
batch start
#iterations: 190
currently lose_sum: 97.41215282678604
time_elpased: 1.741
batch start
#iterations: 191
currently lose_sum: 97.57667398452759
time_elpased: 1.771
batch start
#iterations: 192
currently lose_sum: 97.32861441373825
time_elpased: 1.773
batch start
#iterations: 193
currently lose_sum: 97.44853383302689
time_elpased: 1.749
batch start
#iterations: 194
currently lose_sum: 97.62797635793686
time_elpased: 1.78
batch start
#iterations: 195
currently lose_sum: 97.66851717233658
time_elpased: 1.746
batch start
#iterations: 196
currently lose_sum: 97.44542771577835
time_elpased: 1.764
batch start
#iterations: 197
currently lose_sum: 97.60940128564835
time_elpased: 1.785
batch start
#iterations: 198
currently lose_sum: 97.81000554561615
time_elpased: 1.723
batch start
#iterations: 199
currently lose_sum: 97.18418091535568
time_elpased: 1.761
start validation test
0.631597938144
0.631068951448
0.636719152002
0.633881460991
0.631588947064
63.163
batch start
#iterations: 200
currently lose_sum: 97.33907186985016
time_elpased: 1.8
batch start
#iterations: 201
currently lose_sum: 97.4338373541832
time_elpased: 1.794
batch start
#iterations: 202
currently lose_sum: 97.31372654438019
time_elpased: 1.763
batch start
#iterations: 203
currently lose_sum: 97.32831299304962
time_elpased: 1.751
batch start
#iterations: 204
currently lose_sum: 97.23213005065918
time_elpased: 1.742
batch start
#iterations: 205
currently lose_sum: 97.44756132364273
time_elpased: 1.735
batch start
#iterations: 206
currently lose_sum: 97.33829808235168
time_elpased: 1.78
batch start
#iterations: 207
currently lose_sum: 97.36552828550339
time_elpased: 1.796
batch start
#iterations: 208
currently lose_sum: 97.23881757259369
time_elpased: 1.74
batch start
#iterations: 209
currently lose_sum: 97.51731431484222
time_elpased: 1.729
batch start
#iterations: 210
currently lose_sum: 97.41511797904968
time_elpased: 1.796
batch start
#iterations: 211
currently lose_sum: 97.68243902921677
time_elpased: 1.758
batch start
#iterations: 212
currently lose_sum: 97.60778707265854
time_elpased: 1.762
batch start
#iterations: 213
currently lose_sum: 97.34721946716309
time_elpased: 1.795
batch start
#iterations: 214
currently lose_sum: 97.59132021665573
time_elpased: 1.749
batch start
#iterations: 215
currently lose_sum: 97.31401002407074
time_elpased: 1.786
batch start
#iterations: 216
currently lose_sum: 97.1997942328453
time_elpased: 1.728
batch start
#iterations: 217
currently lose_sum: 97.32591342926025
time_elpased: 1.756
batch start
#iterations: 218
currently lose_sum: 97.36747908592224
time_elpased: 1.794
batch start
#iterations: 219
currently lose_sum: 97.59864008426666
time_elpased: 1.787
start validation test
0.620567010309
0.648624779208
0.529072759082
0.582780706229
0.620727642579
63.668
batch start
#iterations: 220
currently lose_sum: 97.44676238298416
time_elpased: 1.742
batch start
#iterations: 221
currently lose_sum: 97.44744628667831
time_elpased: 1.743
batch start
#iterations: 222
currently lose_sum: 97.22482413053513
time_elpased: 1.728
batch start
#iterations: 223
currently lose_sum: 97.40456175804138
time_elpased: 1.715
batch start
#iterations: 224
currently lose_sum: 97.43938535451889
time_elpased: 1.736
batch start
#iterations: 225
currently lose_sum: 97.70995408296585
time_elpased: 1.763
batch start
#iterations: 226
currently lose_sum: 97.37846279144287
time_elpased: 1.744
batch start
#iterations: 227
currently lose_sum: 97.41034525632858
time_elpased: 1.745
batch start
#iterations: 228
currently lose_sum: 97.44040936231613
time_elpased: 1.759
batch start
#iterations: 229
currently lose_sum: 97.46932423114777
time_elpased: 1.781
batch start
#iterations: 230
currently lose_sum: 97.29752790927887
time_elpased: 1.766
batch start
#iterations: 231
currently lose_sum: 97.3852441906929
time_elpased: 1.754
batch start
#iterations: 232
currently lose_sum: 97.5013445019722
time_elpased: 1.742
batch start
#iterations: 233
currently lose_sum: 97.63167810440063
time_elpased: 1.761
batch start
#iterations: 234
currently lose_sum: 97.31413453817368
time_elpased: 1.752
batch start
#iterations: 235
currently lose_sum: 97.43767112493515
time_elpased: 1.772
batch start
#iterations: 236
currently lose_sum: 97.49320185184479
time_elpased: 1.742
batch start
#iterations: 237
currently lose_sum: 97.52976232767105
time_elpased: 1.745
batch start
#iterations: 238
currently lose_sum: 97.07109665870667
time_elpased: 1.77
batch start
#iterations: 239
currently lose_sum: 97.49812173843384
time_elpased: 1.743
start validation test
0.628144329897
0.646528509542
0.568282391685
0.604885529631
0.628249426763
63.612
batch start
#iterations: 240
currently lose_sum: 97.26339775323868
time_elpased: 1.771
batch start
#iterations: 241
currently lose_sum: 97.43765276670456
time_elpased: 1.783
batch start
#iterations: 242
currently lose_sum: 97.54260176420212
time_elpased: 1.743
batch start
#iterations: 243
currently lose_sum: 97.34470409154892
time_elpased: 1.786
batch start
#iterations: 244
currently lose_sum: 97.47128874063492
time_elpased: 1.753
batch start
#iterations: 245
currently lose_sum: 97.45526254177094
time_elpased: 1.802
batch start
#iterations: 246
currently lose_sum: 97.40352976322174
time_elpased: 1.806
batch start
#iterations: 247
currently lose_sum: 97.39352226257324
time_elpased: 1.76
batch start
#iterations: 248
currently lose_sum: 97.22840887308121
time_elpased: 1.794
batch start
#iterations: 249
currently lose_sum: 97.44376629590988
time_elpased: 1.788
batch start
#iterations: 250
currently lose_sum: 97.48374783992767
time_elpased: 1.734
batch start
#iterations: 251
currently lose_sum: 97.06732481718063
time_elpased: 1.805
batch start
#iterations: 252
currently lose_sum: 97.11895108222961
time_elpased: 1.753
batch start
#iterations: 253
currently lose_sum: 97.39920789003372
time_elpased: 1.797
batch start
#iterations: 254
currently lose_sum: 97.1393592953682
time_elpased: 1.755
batch start
#iterations: 255
currently lose_sum: 97.57983148097992
time_elpased: 1.762
batch start
#iterations: 256
currently lose_sum: 97.6364683508873
time_elpased: 1.766
batch start
#iterations: 257
currently lose_sum: 97.36306250095367
time_elpased: 1.763
batch start
#iterations: 258
currently lose_sum: 97.02992868423462
time_elpased: 1.769
batch start
#iterations: 259
currently lose_sum: 97.0473667383194
time_elpased: 1.744
start validation test
0.632010309278
0.638960758948
0.609961922404
0.624124677513
0.632049018622
63.135
batch start
#iterations: 260
currently lose_sum: 97.57565873861313
time_elpased: 1.744
batch start
#iterations: 261
currently lose_sum: 97.1963678598404
time_elpased: 1.749
batch start
#iterations: 262
currently lose_sum: 97.2056006193161
time_elpased: 1.793
batch start
#iterations: 263
currently lose_sum: 97.52575838565826
time_elpased: 1.746
batch start
#iterations: 264
currently lose_sum: 97.02118211984634
time_elpased: 1.787
batch start
#iterations: 265
currently lose_sum: 97.46463000774384
time_elpased: 1.774
batch start
#iterations: 266
currently lose_sum: 97.17075324058533
time_elpased: 1.759
batch start
#iterations: 267
currently lose_sum: 97.14098554849625
time_elpased: 1.736
batch start
#iterations: 268
currently lose_sum: 97.17844051122665
time_elpased: 1.758
batch start
#iterations: 269
currently lose_sum: 97.68300873041153
time_elpased: 1.739
batch start
#iterations: 270
currently lose_sum: 97.33034664392471
time_elpased: 1.764
batch start
#iterations: 271
currently lose_sum: 97.29408240318298
time_elpased: 1.759
batch start
#iterations: 272
currently lose_sum: 97.3153315782547
time_elpased: 1.791
batch start
#iterations: 273
currently lose_sum: 97.41404420137405
time_elpased: 1.763
batch start
#iterations: 274
currently lose_sum: 97.42433339357376
time_elpased: 1.811
batch start
#iterations: 275
currently lose_sum: 97.43916028738022
time_elpased: 1.753
batch start
#iterations: 276
currently lose_sum: 97.42973083257675
time_elpased: 1.816
batch start
#iterations: 277
currently lose_sum: 97.39463102817535
time_elpased: 1.766
batch start
#iterations: 278
currently lose_sum: 97.31368267536163
time_elpased: 1.772
batch start
#iterations: 279
currently lose_sum: 97.16029858589172
time_elpased: 1.78
start validation test
0.629020618557
0.637885751806
0.599876505094
0.618297533811
0.629071785543
63.207
batch start
#iterations: 280
currently lose_sum: 97.38941323757172
time_elpased: 1.792
batch start
#iterations: 281
currently lose_sum: 97.4648015499115
time_elpased: 1.733
batch start
#iterations: 282
currently lose_sum: 97.31668055057526
time_elpased: 1.761
batch start
#iterations: 283
currently lose_sum: 97.39094752073288
time_elpased: 1.793
batch start
#iterations: 284
currently lose_sum: 97.10305136442184
time_elpased: 1.761
batch start
#iterations: 285
currently lose_sum: 97.02774739265442
time_elpased: 1.747
batch start
#iterations: 286
currently lose_sum: 97.5813809633255
time_elpased: 1.752
batch start
#iterations: 287
currently lose_sum: 97.28462398052216
time_elpased: 1.781
batch start
#iterations: 288
currently lose_sum: 97.4320684671402
time_elpased: 1.783
batch start
#iterations: 289
currently lose_sum: 97.28073579072952
time_elpased: 1.76
batch start
#iterations: 290
currently lose_sum: 97.01055574417114
time_elpased: 1.737
batch start
#iterations: 291
currently lose_sum: 97.45123648643494
time_elpased: 1.768
batch start
#iterations: 292
currently lose_sum: 97.54238259792328
time_elpased: 1.768
batch start
#iterations: 293
currently lose_sum: 97.19282829761505
time_elpased: 1.781
batch start
#iterations: 294
currently lose_sum: 97.27756017446518
time_elpased: 1.757
batch start
#iterations: 295
currently lose_sum: 97.26073378324509
time_elpased: 1.761
batch start
#iterations: 296
currently lose_sum: 97.21252852678299
time_elpased: 1.787
batch start
#iterations: 297
currently lose_sum: 97.19419783353806
time_elpased: 1.797
batch start
#iterations: 298
currently lose_sum: 97.24045062065125
time_elpased: 1.753
batch start
#iterations: 299
currently lose_sum: 97.33623272180557
time_elpased: 1.753
start validation test
0.621855670103
0.650031505986
0.530822270248
0.584409698618
0.622015493277
63.626
batch start
#iterations: 300
currently lose_sum: 97.13267570734024
time_elpased: 1.77
batch start
#iterations: 301
currently lose_sum: 97.36679434776306
time_elpased: 1.743
batch start
#iterations: 302
currently lose_sum: 97.54354816675186
time_elpased: 1.808
batch start
#iterations: 303
currently lose_sum: 97.35765957832336
time_elpased: 1.718
batch start
#iterations: 304
currently lose_sum: 97.1473263502121
time_elpased: 1.787
batch start
#iterations: 305
currently lose_sum: 97.1856192946434
time_elpased: 1.755
batch start
#iterations: 306
currently lose_sum: 97.08254754543304
time_elpased: 1.757
batch start
#iterations: 307
currently lose_sum: 97.15388202667236
time_elpased: 1.764
batch start
#iterations: 308
currently lose_sum: 97.28240597248077
time_elpased: 1.778
batch start
#iterations: 309
currently lose_sum: 97.11811047792435
time_elpased: 1.727
batch start
#iterations: 310
currently lose_sum: 97.55523639917374
time_elpased: 1.799
batch start
#iterations: 311
currently lose_sum: 97.24553555250168
time_elpased: 1.759
batch start
#iterations: 312
currently lose_sum: 97.02328145503998
time_elpased: 1.789
batch start
#iterations: 313
currently lose_sum: 97.10496091842651
time_elpased: 1.73
batch start
#iterations: 314
currently lose_sum: 97.23844224214554
time_elpased: 1.742
batch start
#iterations: 315
currently lose_sum: 97.16201025247574
time_elpased: 1.734
batch start
#iterations: 316
currently lose_sum: 97.19066989421844
time_elpased: 1.778
batch start
#iterations: 317
currently lose_sum: 97.0500363111496
time_elpased: 1.729
batch start
#iterations: 318
currently lose_sum: 97.41537952423096
time_elpased: 1.748
batch start
#iterations: 319
currently lose_sum: 97.50420725345612
time_elpased: 1.719
start validation test
0.628505154639
0.632272344013
0.617371616754
0.624733142411
0.628524701282
63.298
batch start
#iterations: 320
currently lose_sum: 97.08726596832275
time_elpased: 1.754
batch start
#iterations: 321
currently lose_sum: 97.28298509120941
time_elpased: 1.817
batch start
#iterations: 322
currently lose_sum: 97.13358187675476
time_elpased: 1.782
batch start
#iterations: 323
currently lose_sum: 97.17771178483963
time_elpased: 1.775
batch start
#iterations: 324
currently lose_sum: 97.3697030544281
time_elpased: 1.765
batch start
#iterations: 325
currently lose_sum: 97.23844480514526
time_elpased: 1.711
batch start
#iterations: 326
currently lose_sum: 97.01030445098877
time_elpased: 1.779
batch start
#iterations: 327
currently lose_sum: 97.04183262586594
time_elpased: 1.783
batch start
#iterations: 328
currently lose_sum: 97.28800064325333
time_elpased: 1.829
batch start
#iterations: 329
currently lose_sum: 97.47736930847168
time_elpased: 1.756
batch start
#iterations: 330
currently lose_sum: 97.09633034467697
time_elpased: 1.772
batch start
#iterations: 331
currently lose_sum: 97.3185446858406
time_elpased: 1.747
batch start
#iterations: 332
currently lose_sum: 96.87225085496902
time_elpased: 1.776
batch start
#iterations: 333
currently lose_sum: 97.04372572898865
time_elpased: 1.741
batch start
#iterations: 334
currently lose_sum: 97.52344363927841
time_elpased: 1.776
batch start
#iterations: 335
currently lose_sum: 97.12571358680725
time_elpased: 1.734
batch start
#iterations: 336
currently lose_sum: 97.3536366224289
time_elpased: 1.776
batch start
#iterations: 337
currently lose_sum: 97.43671208620071
time_elpased: 1.769
batch start
#iterations: 338
currently lose_sum: 97.41248947381973
time_elpased: 1.798
batch start
#iterations: 339
currently lose_sum: 97.20852714776993
time_elpased: 1.767
start validation test
0.63087628866
0.635870720817
0.615519193167
0.625529467134
0.63090325041
63.201
batch start
#iterations: 340
currently lose_sum: 97.22544431686401
time_elpased: 1.747
batch start
#iterations: 341
currently lose_sum: 97.11216825246811
time_elpased: 1.777
batch start
#iterations: 342
currently lose_sum: 97.20791471004486
time_elpased: 1.756
batch start
#iterations: 343
currently lose_sum: 97.57227522134781
time_elpased: 1.75
batch start
#iterations: 344
currently lose_sum: 97.04235082864761
time_elpased: 1.761
batch start
#iterations: 345
currently lose_sum: 97.60478466749191
time_elpased: 1.781
batch start
#iterations: 346
currently lose_sum: 96.9776456952095
time_elpased: 1.787
batch start
#iterations: 347
currently lose_sum: 97.60245060920715
time_elpased: 1.734
batch start
#iterations: 348
currently lose_sum: 97.26024305820465
time_elpased: 1.733
batch start
#iterations: 349
currently lose_sum: 97.39431887865067
time_elpased: 1.718
batch start
#iterations: 350
currently lose_sum: 97.31883043050766
time_elpased: 1.776
batch start
#iterations: 351
currently lose_sum: 97.16542607545853
time_elpased: 1.76
batch start
#iterations: 352
currently lose_sum: 97.35999375581741
time_elpased: 1.759
batch start
#iterations: 353
currently lose_sum: 97.20621651411057
time_elpased: 1.747
batch start
#iterations: 354
currently lose_sum: 97.54605734348297
time_elpased: 1.759
batch start
#iterations: 355
currently lose_sum: 97.15412098169327
time_elpased: 1.713
batch start
#iterations: 356
currently lose_sum: 97.44471353292465
time_elpased: 1.765
batch start
#iterations: 357
currently lose_sum: 97.20212197303772
time_elpased: 1.743
batch start
#iterations: 358
currently lose_sum: 97.50947034358978
time_elpased: 1.767
batch start
#iterations: 359
currently lose_sum: 97.16650694608688
time_elpased: 1.765
start validation test
0.626134020619
0.637931034483
0.586394977874
0.611078342002
0.626203788637
63.429
batch start
#iterations: 360
currently lose_sum: 97.02693492174149
time_elpased: 1.835
batch start
#iterations: 361
currently lose_sum: 97.26330548524857
time_elpased: 1.806
batch start
#iterations: 362
currently lose_sum: 97.46313709020615
time_elpased: 1.794
batch start
#iterations: 363
currently lose_sum: 97.008709192276
time_elpased: 1.778
batch start
#iterations: 364
currently lose_sum: 97.06438952684402
time_elpased: 1.759
batch start
#iterations: 365
currently lose_sum: 97.32503575086594
time_elpased: 1.766
batch start
#iterations: 366
currently lose_sum: 97.1291298866272
time_elpased: 1.787
batch start
#iterations: 367
currently lose_sum: 97.42638951539993
time_elpased: 1.798
batch start
#iterations: 368
currently lose_sum: 97.0851536989212
time_elpased: 1.794
batch start
#iterations: 369
currently lose_sum: 97.1750962138176
time_elpased: 1.768
batch start
#iterations: 370
currently lose_sum: 97.10033196210861
time_elpased: 1.795
batch start
#iterations: 371
currently lose_sum: 97.16277009248734
time_elpased: 1.762
batch start
#iterations: 372
currently lose_sum: 97.31774115562439
time_elpased: 1.751
batch start
#iterations: 373
currently lose_sum: 97.01688402891159
time_elpased: 1.763
batch start
#iterations: 374
currently lose_sum: 97.52510070800781
time_elpased: 1.749
batch start
#iterations: 375
currently lose_sum: 97.28752034902573
time_elpased: 1.741
batch start
#iterations: 376
currently lose_sum: 97.1445181965828
time_elpased: 1.785
batch start
#iterations: 377
currently lose_sum: 97.32559645175934
time_elpased: 1.787
batch start
#iterations: 378
currently lose_sum: 97.29569149017334
time_elpased: 1.754
batch start
#iterations: 379
currently lose_sum: 97.16912084817886
time_elpased: 1.789
start validation test
0.629639175258
0.632509943479
0.621899763301
0.627159981319
0.629652762989
63.185
batch start
#iterations: 380
currently lose_sum: 97.43263417482376
time_elpased: 1.803
batch start
#iterations: 381
currently lose_sum: 97.06387943029404
time_elpased: 1.778
batch start
#iterations: 382
currently lose_sum: 97.30018776655197
time_elpased: 1.762
batch start
#iterations: 383
currently lose_sum: 97.56021863222122
time_elpased: 1.765
batch start
#iterations: 384
currently lose_sum: 97.16707956790924
time_elpased: 1.774
batch start
#iterations: 385
currently lose_sum: 97.33242464065552
time_elpased: 1.747
batch start
#iterations: 386
currently lose_sum: 97.32369941473007
time_elpased: 1.775
batch start
#iterations: 387
currently lose_sum: 97.4825987815857
time_elpased: 1.744
batch start
#iterations: 388
currently lose_sum: 97.56113475561142
time_elpased: 1.767
batch start
#iterations: 389
currently lose_sum: 97.70512735843658
time_elpased: 1.83
batch start
#iterations: 390
currently lose_sum: 97.21819162368774
time_elpased: 1.787
batch start
#iterations: 391
currently lose_sum: 97.0815132856369
time_elpased: 1.745
batch start
#iterations: 392
currently lose_sum: 97.09821200370789
time_elpased: 1.758
batch start
#iterations: 393
currently lose_sum: 97.1192524433136
time_elpased: 1.756
batch start
#iterations: 394
currently lose_sum: 97.30731731653214
time_elpased: 1.756
batch start
#iterations: 395
currently lose_sum: 97.39022308588028
time_elpased: 1.749
batch start
#iterations: 396
currently lose_sum: 96.89582204818726
time_elpased: 1.79
batch start
#iterations: 397
currently lose_sum: 97.37834465503693
time_elpased: 1.766
batch start
#iterations: 398
currently lose_sum: 96.97527086734772
time_elpased: 1.79
batch start
#iterations: 399
currently lose_sum: 97.31708008050919
time_elpased: 1.739
start validation test
0.62706185567
0.64473991136
0.568899866214
0.604450276092
0.627163968013
63.415
acc: 0.620
pre: 0.629
rec: 0.590
F1: 0.609
auc: 0.661
