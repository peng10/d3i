start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.74453961849213
time_elpased: 2.4
batch start
#iterations: 1
currently lose_sum: 100.4339730143547
time_elpased: 2.347
batch start
#iterations: 2
currently lose_sum: 100.13049900531769
time_elpased: 2.329
batch start
#iterations: 3
currently lose_sum: 99.98874574899673
time_elpased: 2.285
batch start
#iterations: 4
currently lose_sum: 99.8744141459465
time_elpased: 2.285
batch start
#iterations: 5
currently lose_sum: 99.86483842134476
time_elpased: 2.168
batch start
#iterations: 6
currently lose_sum: 99.58865821361542
time_elpased: 2.508
batch start
#iterations: 7
currently lose_sum: 99.52368831634521
time_elpased: 2.648
batch start
#iterations: 8
currently lose_sum: 99.56393158435822
time_elpased: 2.891
batch start
#iterations: 9
currently lose_sum: 99.18831217288971
time_elpased: 2.628
batch start
#iterations: 10
currently lose_sum: 99.22544133663177
time_elpased: 2.601
batch start
#iterations: 11
currently lose_sum: 99.34135323762894
time_elpased: 2.43
batch start
#iterations: 12
currently lose_sum: 99.13635867834091
time_elpased: 2.158
batch start
#iterations: 13
currently lose_sum: 99.20590144395828
time_elpased: 3.181
batch start
#iterations: 14
currently lose_sum: 98.91180855035782
time_elpased: 2.802
batch start
#iterations: 15
currently lose_sum: 98.93294936418533
time_elpased: 2.893
batch start
#iterations: 16
currently lose_sum: 98.96558195352554
time_elpased: 2.717
batch start
#iterations: 17
currently lose_sum: 99.02374511957169
time_elpased: 2.654
batch start
#iterations: 18
currently lose_sum: 98.72307658195496
time_elpased: 2.525
batch start
#iterations: 19
currently lose_sum: 98.90133965015411
time_elpased: 2.139
start validation test
0.617216494845
0.62512288367
0.588967788412
0.606506994489
0.617266089807
64.459
batch start
#iterations: 20
currently lose_sum: 98.86424350738525
time_elpased: 2.346
batch start
#iterations: 21
currently lose_sum: 98.68992358446121
time_elpased: 2.438
batch start
#iterations: 22
currently lose_sum: 98.7633336186409
time_elpased: 2.426
batch start
#iterations: 23
currently lose_sum: 98.95430260896683
time_elpased: 2.491
batch start
#iterations: 24
currently lose_sum: 98.5480849146843
time_elpased: 2.513
batch start
#iterations: 25
currently lose_sum: 98.64860910177231
time_elpased: 2.999
batch start
#iterations: 26
currently lose_sum: 98.74171364307404
time_elpased: 2.882
batch start
#iterations: 27
currently lose_sum: 98.49536770582199
time_elpased: 3.013
batch start
#iterations: 28
currently lose_sum: 98.47477954626083
time_elpased: 2.911
batch start
#iterations: 29
currently lose_sum: 98.3509264588356
time_elpased: 2.994
batch start
#iterations: 30
currently lose_sum: 98.60018926858902
time_elpased: 2.784
batch start
#iterations: 31
currently lose_sum: 98.3497104048729
time_elpased: 2.713
batch start
#iterations: 32
currently lose_sum: 98.590507209301
time_elpased: 5.571
batch start
#iterations: 33
currently lose_sum: 98.27961993217468
time_elpased: 3.101
batch start
#iterations: 34
currently lose_sum: 98.39108675718307
time_elpased: 3.082
batch start
#iterations: 35
currently lose_sum: 98.15513718128204
time_elpased: 2.808
batch start
#iterations: 36
currently lose_sum: 98.39182674884796
time_elpased: 2.233
batch start
#iterations: 37
currently lose_sum: 98.3757975101471
time_elpased: 2.901
batch start
#iterations: 38
currently lose_sum: 98.16393810510635
time_elpased: 4.032
batch start
#iterations: 39
currently lose_sum: 98.20995616912842
time_elpased: 2.682
start validation test
0.612731958763
0.639989837398
0.518472779665
0.572858036273
0.61289744529
64.130
batch start
#iterations: 40
currently lose_sum: 98.56310606002808
time_elpased: 2.579
batch start
#iterations: 41
currently lose_sum: 98.21807742118835
time_elpased: 2.715
batch start
#iterations: 42
currently lose_sum: 98.33596658706665
time_elpased: 2.777
batch start
#iterations: 43
currently lose_sum: 98.3061448931694
time_elpased: 2.734
batch start
#iterations: 44
currently lose_sum: 98.15702819824219
time_elpased: 2.439
batch start
#iterations: 45
currently lose_sum: 97.98152577877045
time_elpased: 2.598
batch start
#iterations: 46
currently lose_sum: 98.32641416788101
time_elpased: 2.434
batch start
#iterations: 47
currently lose_sum: 98.47162336111069
time_elpased: 2.098
batch start
#iterations: 48
currently lose_sum: 98.3501728773117
time_elpased: 2.081
batch start
#iterations: 49
currently lose_sum: 98.04299718141556
time_elpased: 2.395
batch start
#iterations: 50
currently lose_sum: 98.13106858730316
time_elpased: 2.768
batch start
#iterations: 51
currently lose_sum: 97.94346511363983
time_elpased: 2.838
batch start
#iterations: 52
currently lose_sum: 98.35410350561142
time_elpased: 3.18
batch start
#iterations: 53
currently lose_sum: 97.8993930220604
time_elpased: 2.446
batch start
#iterations: 54
currently lose_sum: 98.12326061725616
time_elpased: 2.407
batch start
#iterations: 55
currently lose_sum: 98.07238924503326
time_elpased: 2.208
batch start
#iterations: 56
currently lose_sum: 98.20312637090683
time_elpased: 2.623
batch start
#iterations: 57
currently lose_sum: 98.02687507867813
time_elpased: 2.61
batch start
#iterations: 58
currently lose_sum: 98.12235128879547
time_elpased: 2.723
batch start
#iterations: 59
currently lose_sum: 98.22183936834335
time_elpased: 2.507
start validation test
0.611597938144
0.649780340472
0.487084491098
0.556790777013
0.611816540705
64.295
batch start
#iterations: 60
currently lose_sum: 98.02768987417221
time_elpased: 2.387
batch start
#iterations: 61
currently lose_sum: 98.31426531076431
time_elpased: 2.49
batch start
#iterations: 62
currently lose_sum: 98.06165635585785
time_elpased: 2.294
batch start
#iterations: 63
currently lose_sum: 97.98059570789337
time_elpased: 2.698
batch start
#iterations: 64
currently lose_sum: 98.38760006427765
time_elpased: 2.376
batch start
#iterations: 65
currently lose_sum: 98.05967152118683
time_elpased: 2.224
batch start
#iterations: 66
currently lose_sum: 97.9996822476387
time_elpased: 2.261
batch start
#iterations: 67
currently lose_sum: 97.83314317464828
time_elpased: 2.514
batch start
#iterations: 68
currently lose_sum: 97.71451514959335
time_elpased: 2.474
batch start
#iterations: 69
currently lose_sum: 98.39676249027252
time_elpased: 2.209
batch start
#iterations: 70
currently lose_sum: 98.03864794969559
time_elpased: 2.836
batch start
#iterations: 71
currently lose_sum: 97.9932998418808
time_elpased: 2.773
batch start
#iterations: 72
currently lose_sum: 97.78634232282639
time_elpased: 2.315
batch start
#iterations: 73
currently lose_sum: 98.04899257421494
time_elpased: 2.151
batch start
#iterations: 74
currently lose_sum: 97.8954381942749
time_elpased: 2.232
batch start
#iterations: 75
currently lose_sum: 97.93148583173752
time_elpased: 2.268
batch start
#iterations: 76
currently lose_sum: 97.79149544239044
time_elpased: 2.314
batch start
#iterations: 77
currently lose_sum: 98.14210551977158
time_elpased: 2.335
batch start
#iterations: 78
currently lose_sum: 97.98820912837982
time_elpased: 2.5
batch start
#iterations: 79
currently lose_sum: 98.08976590633392
time_elpased: 2.712
start validation test
0.624484536082
0.631630222992
0.600493979623
0.615668689
0.624526655205
63.613
batch start
#iterations: 80
currently lose_sum: 97.99246221780777
time_elpased: 2.619
batch start
#iterations: 81
currently lose_sum: 98.1266657114029
time_elpased: 2.977
batch start
#iterations: 82
currently lose_sum: 97.94943058490753
time_elpased: 2.91
batch start
#iterations: 83
currently lose_sum: 97.72168773412704
time_elpased: 2.991
batch start
#iterations: 84
currently lose_sum: 97.9624297618866
time_elpased: 2.846
batch start
#iterations: 85
currently lose_sum: 98.10699373483658
time_elpased: 2.807
batch start
#iterations: 86
currently lose_sum: 97.8976292014122
time_elpased: 2.735
batch start
#iterations: 87
currently lose_sum: 98.05392110347748
time_elpased: 2.768
batch start
#iterations: 88
currently lose_sum: 97.82828843593597
time_elpased: 2.862
batch start
#iterations: 89
currently lose_sum: 98.04730021953583
time_elpased: 2.921
batch start
#iterations: 90
currently lose_sum: 97.8203399181366
time_elpased: 2.615
batch start
#iterations: 91
currently lose_sum: 98.0336976647377
time_elpased: 2.741
batch start
#iterations: 92
currently lose_sum: 97.76403015851974
time_elpased: 2.968
batch start
#iterations: 93
currently lose_sum: 97.56148505210876
time_elpased: 3.286
batch start
#iterations: 94
currently lose_sum: 98.02743607759476
time_elpased: 3.578
batch start
#iterations: 95
currently lose_sum: 97.99693727493286
time_elpased: 3.455
batch start
#iterations: 96
currently lose_sum: 97.88704997301102
time_elpased: 3.206
batch start
#iterations: 97
currently lose_sum: 97.92311251163483
time_elpased: 3.736
batch start
#iterations: 98
currently lose_sum: 98.09165197610855
time_elpased: 3.338
batch start
#iterations: 99
currently lose_sum: 98.03534585237503
time_elpased: 3.668
start validation test
0.619793814433
0.640095751047
0.550375630339
0.59185480301
0.619915688762
63.803
batch start
#iterations: 100
currently lose_sum: 97.861483335495
time_elpased: 3.422
batch start
#iterations: 101
currently lose_sum: 97.68010103702545
time_elpased: 3.647
batch start
#iterations: 102
currently lose_sum: 97.76140183210373
time_elpased: 3.723
batch start
#iterations: 103
currently lose_sum: 97.8216198682785
time_elpased: 3.854
batch start
#iterations: 104
currently lose_sum: 97.69929629564285
time_elpased: 4.104
batch start
#iterations: 105
currently lose_sum: 97.82361245155334
time_elpased: 3.644
batch start
#iterations: 106
currently lose_sum: 97.8054940700531
time_elpased: 3.174
batch start
#iterations: 107
currently lose_sum: 98.00713193416595
time_elpased: 3.622
batch start
#iterations: 108
currently lose_sum: 97.89408922195435
time_elpased: 3.685
batch start
#iterations: 109
currently lose_sum: 97.79102903604507
time_elpased: 4.015
batch start
#iterations: 110
currently lose_sum: 97.86399161815643
time_elpased: 2.915
batch start
#iterations: 111
currently lose_sum: 97.96880978345871
time_elpased: 2.992
batch start
#iterations: 112
currently lose_sum: 97.81546205282211
time_elpased: 3.33
batch start
#iterations: 113
currently lose_sum: 97.51045024394989
time_elpased: 3.288
batch start
#iterations: 114
currently lose_sum: 97.76504909992218
time_elpased: 3.311
batch start
#iterations: 115
currently lose_sum: 97.80410772562027
time_elpased: 2.896
batch start
#iterations: 116
currently lose_sum: 97.81221264600754
time_elpased: 9.269
batch start
#iterations: 117
currently lose_sum: 97.65873277187347
time_elpased: 2.823
batch start
#iterations: 118
currently lose_sum: 97.66788244247437
time_elpased: 2.89
batch start
#iterations: 119
currently lose_sum: 97.70797258615494
time_elpased: 2.796
start validation test
0.624639175258
0.635775621724
0.586703715138
0.610254763434
0.624705776809
63.489
batch start
#iterations: 120
currently lose_sum: 97.58545798063278
time_elpased: 3.262
batch start
#iterations: 121
currently lose_sum: 97.73307597637177
time_elpased: 3.392
batch start
#iterations: 122
currently lose_sum: 97.69271463155746
time_elpased: 3.64
batch start
#iterations: 123
currently lose_sum: 97.53312915563583
time_elpased: 4.128
batch start
#iterations: 124
currently lose_sum: 97.58444035053253
time_elpased: 3.77
batch start
#iterations: 125
currently lose_sum: 97.73523586988449
time_elpased: 3.439
batch start
#iterations: 126
currently lose_sum: 97.82950466871262
time_elpased: 3.865
batch start
#iterations: 127
currently lose_sum: 97.8952631354332
time_elpased: 3.481
batch start
#iterations: 128
currently lose_sum: 97.58580368757248
time_elpased: 3.363
batch start
#iterations: 129
currently lose_sum: 97.51631820201874
time_elpased: 3.294
batch start
#iterations: 130
currently lose_sum: 97.65914332866669
time_elpased: 3.339
batch start
#iterations: 131
currently lose_sum: 97.60723567008972
time_elpased: 3.747
batch start
#iterations: 132
currently lose_sum: 97.77721190452576
time_elpased: 3.373
batch start
#iterations: 133
currently lose_sum: 97.66334962844849
time_elpased: 3.622
batch start
#iterations: 134
currently lose_sum: 97.65866315364838
time_elpased: 3.494
batch start
#iterations: 135
currently lose_sum: 97.85975253582001
time_elpased: 3.752
batch start
#iterations: 136
currently lose_sum: 97.60542523860931
time_elpased: 3.605
batch start
#iterations: 137
currently lose_sum: 97.80003309249878
time_elpased: 3.95
batch start
#iterations: 138
currently lose_sum: 97.91318732500076
time_elpased: 3.118
batch start
#iterations: 139
currently lose_sum: 97.63709366321564
time_elpased: 2.948
start validation test
0.62618556701
0.631578947368
0.608829885767
0.619995808007
0.626216037586
63.478
batch start
#iterations: 140
currently lose_sum: 97.57852256298065
time_elpased: 3.916
batch start
#iterations: 141
currently lose_sum: 97.5958263874054
time_elpased: 3.91
batch start
#iterations: 142
currently lose_sum: 97.60209119319916
time_elpased: 3.958
batch start
#iterations: 143
currently lose_sum: 97.63383311033249
time_elpased: 4.174
batch start
#iterations: 144
currently lose_sum: 97.68151122331619
time_elpased: 3.658
batch start
#iterations: 145
currently lose_sum: 97.97963231801987
time_elpased: 3.646
batch start
#iterations: 146
currently lose_sum: 97.66041606664658
time_elpased: 2.907
batch start
#iterations: 147
currently lose_sum: 97.72417479753494
time_elpased: 9.279
batch start
#iterations: 148
currently lose_sum: 97.65067422389984
time_elpased: 3.52
batch start
#iterations: 149
currently lose_sum: 97.71007680892944
time_elpased: 4.176
batch start
#iterations: 150
currently lose_sum: 97.51128107309341
time_elpased: 3.638
batch start
#iterations: 151
currently lose_sum: 97.77565866708755
time_elpased: 2.593
batch start
#iterations: 152
currently lose_sum: 97.47856628894806
time_elpased: 2.908
batch start
#iterations: 153
currently lose_sum: 97.66850858926773
time_elpased: 2.984
batch start
#iterations: 154
currently lose_sum: 97.6097601056099
time_elpased: 3.845
batch start
#iterations: 155
currently lose_sum: 97.72402161359787
time_elpased: 2.803
batch start
#iterations: 156
currently lose_sum: 97.77696043252945
time_elpased: 3.086
batch start
#iterations: 157
currently lose_sum: 97.51772165298462
time_elpased: 3.171
batch start
#iterations: 158
currently lose_sum: 97.65112209320068
time_elpased: 3.246
batch start
#iterations: 159
currently lose_sum: 97.74584323167801
time_elpased: 3.691
start validation test
0.630309278351
0.639421496658
0.600596892045
0.619401400976
0.630361443027
63.467
batch start
#iterations: 160
currently lose_sum: 97.85612112283707
time_elpased: 3.601
batch start
#iterations: 161
currently lose_sum: 97.80753546953201
time_elpased: 3.383
batch start
#iterations: 162
currently lose_sum: 97.57866752147675
time_elpased: 3.042
batch start
#iterations: 163
currently lose_sum: 97.25834250450134
time_elpased: 2.93
batch start
#iterations: 164
currently lose_sum: 97.45808583498001
time_elpased: 2.761
batch start
#iterations: 165
currently lose_sum: 97.70762699842453
time_elpased: 2.614
batch start
#iterations: 166
currently lose_sum: 97.52778631448746
time_elpased: 2.219
batch start
#iterations: 167
currently lose_sum: 97.78727620840073
time_elpased: 2.282
batch start
#iterations: 168
currently lose_sum: 97.66200345754623
time_elpased: 2.206
batch start
#iterations: 169
currently lose_sum: 97.48474818468094
time_elpased: 2.231
batch start
#iterations: 170
currently lose_sum: 97.31618642807007
time_elpased: 2.364
batch start
#iterations: 171
currently lose_sum: 97.61557525396347
time_elpased: 2.461
batch start
#iterations: 172
currently lose_sum: 97.49356007575989
time_elpased: 2.598
batch start
#iterations: 173
currently lose_sum: 97.51790469884872
time_elpased: 25.128
batch start
#iterations: 174
currently lose_sum: 97.56860101222992
time_elpased: 2.718
batch start
#iterations: 175
currently lose_sum: 97.78690993785858
time_elpased: 2.44
batch start
#iterations: 176
currently lose_sum: 97.41178339719772
time_elpased: 2.715
batch start
#iterations: 177
currently lose_sum: 97.54233402013779
time_elpased: 2.078
batch start
#iterations: 178
currently lose_sum: 97.42683327198029
time_elpased: 2.093
batch start
#iterations: 179
currently lose_sum: 97.51865249872208
time_elpased: 2.342
start validation test
0.628505154639
0.638003078953
0.597097869713
0.616873106161
0.628560294972
63.397
batch start
#iterations: 180
currently lose_sum: 97.66321736574173
time_elpased: 2.523
batch start
#iterations: 181
currently lose_sum: 97.60474997758865
time_elpased: 2.139
batch start
#iterations: 182
currently lose_sum: 97.6329402923584
time_elpased: 2.071
batch start
#iterations: 183
currently lose_sum: 97.49808490276337
time_elpased: 2.045
batch start
#iterations: 184
currently lose_sum: 97.39003074169159
time_elpased: 2.433
batch start
#iterations: 185
currently lose_sum: 97.39883178472519
time_elpased: 2.394
batch start
#iterations: 186
currently lose_sum: 97.48994708061218
time_elpased: 2.341
batch start
#iterations: 187
currently lose_sum: 97.53911817073822
time_elpased: 2.113
batch start
#iterations: 188
currently lose_sum: 97.49151974916458
time_elpased: 2.398
batch start
#iterations: 189
currently lose_sum: 97.63249510526657
time_elpased: 2.544
batch start
#iterations: 190
currently lose_sum: 97.51362109184265
time_elpased: 2.546
batch start
#iterations: 191
currently lose_sum: 97.57249987125397
time_elpased: 2.685
batch start
#iterations: 192
currently lose_sum: 97.58404159545898
time_elpased: 2.494
batch start
#iterations: 193
currently lose_sum: 97.39619809389114
time_elpased: 2.443
batch start
#iterations: 194
currently lose_sum: 97.55409210920334
time_elpased: 2.535
batch start
#iterations: 195
currently lose_sum: 97.33102387189865
time_elpased: 2.514
batch start
#iterations: 196
currently lose_sum: 97.289126932621
time_elpased: 3.964
batch start
#iterations: 197
currently lose_sum: 97.85443139076233
time_elpased: 2.717
batch start
#iterations: 198
currently lose_sum: 97.07885253429413
time_elpased: 2.571
batch start
#iterations: 199
currently lose_sum: 97.24789363145828
time_elpased: 2.755
start validation test
0.62675257732
0.644020474639
0.569723165586
0.604597826681
0.626852701248
63.559
batch start
#iterations: 200
currently lose_sum: 97.28982490301132
time_elpased: 2.758
batch start
#iterations: 201
currently lose_sum: 97.46880024671555
time_elpased: 2.977
batch start
#iterations: 202
currently lose_sum: 97.29767137765884
time_elpased: 2.769
batch start
#iterations: 203
currently lose_sum: 97.63676464557648
time_elpased: 2.164
batch start
#iterations: 204
currently lose_sum: 97.48197817802429
time_elpased: 6.984
batch start
#iterations: 205
currently lose_sum: 97.36635196208954
time_elpased: 2.756
batch start
#iterations: 206
currently lose_sum: 97.49499213695526
time_elpased: 2.52
batch start
#iterations: 207
currently lose_sum: 97.67105549573898
time_elpased: 2.032
batch start
#iterations: 208
currently lose_sum: 97.3691366314888
time_elpased: 2.245
batch start
#iterations: 209
currently lose_sum: 97.23226684331894
time_elpased: 2.431
batch start
#iterations: 210
currently lose_sum: 97.43719381093979
time_elpased: 2.016
batch start
#iterations: 211
currently lose_sum: 97.4729534983635
time_elpased: 3.105
batch start
#iterations: 212
currently lose_sum: 97.5138527750969
time_elpased: 3.584
batch start
#iterations: 213
currently lose_sum: 97.37460565567017
time_elpased: 2.559
batch start
#iterations: 214
currently lose_sum: 97.46037435531616
time_elpased: 2.223
batch start
#iterations: 215
currently lose_sum: 97.16159915924072
time_elpased: 2.493
batch start
#iterations: 216
currently lose_sum: 97.39593803882599
time_elpased: 2.084
batch start
#iterations: 217
currently lose_sum: 97.46722620725632
time_elpased: 2.657
batch start
#iterations: 218
currently lose_sum: 97.3564327955246
time_elpased: 2.909
batch start
#iterations: 219
currently lose_sum: 97.40330046415329
time_elpased: 2.422
start validation test
0.627525773196
0.647413895136
0.562930945765
0.602223934823
0.627639179378
63.580
batch start
#iterations: 220
currently lose_sum: 97.31693786382675
time_elpased: 2.5
batch start
#iterations: 221
currently lose_sum: 97.25747889280319
time_elpased: 4.762
batch start
#iterations: 222
currently lose_sum: 97.3744530081749
time_elpased: 2.496
batch start
#iterations: 223
currently lose_sum: 97.65188312530518
time_elpased: 2.205
batch start
#iterations: 224
currently lose_sum: 97.66853129863739
time_elpased: 2.577
batch start
#iterations: 225
currently lose_sum: 97.41733741760254
time_elpased: 2.682
batch start
#iterations: 226
currently lose_sum: 97.21665692329407
time_elpased: 3.238
batch start
#iterations: 227
currently lose_sum: 97.11420530080795
time_elpased: 2.209
batch start
#iterations: 228
currently lose_sum: 97.50281918048859
time_elpased: 2.306
batch start
#iterations: 229
currently lose_sum: 97.40001511573792
time_elpased: 2.346
batch start
#iterations: 230
currently lose_sum: 97.53027522563934
time_elpased: 3.554
batch start
#iterations: 231
currently lose_sum: 97.32022559642792
time_elpased: 2.756
batch start
#iterations: 232
currently lose_sum: 97.6015276312828
time_elpased: 2.672
batch start
#iterations: 233
currently lose_sum: 97.45518118143082
time_elpased: 2.497
batch start
#iterations: 234
currently lose_sum: 97.13734823465347
time_elpased: 2.429
batch start
#iterations: 235
currently lose_sum: 97.73198229074478
time_elpased: 2.315
batch start
#iterations: 236
currently lose_sum: 97.22632563114166
time_elpased: 2.377
batch start
#iterations: 237
currently lose_sum: 97.38784599304199
time_elpased: 2.311
batch start
#iterations: 238
currently lose_sum: 97.34110420942307
time_elpased: 2.326
batch start
#iterations: 239
currently lose_sum: 97.23597776889801
time_elpased: 1.889
start validation test
0.630360824742
0.641413019329
0.59421632191
0.61691329665
0.630424281992
63.451
batch start
#iterations: 240
currently lose_sum: 97.60135501623154
time_elpased: 2.096
batch start
#iterations: 241
currently lose_sum: 97.7657459974289
time_elpased: 2.019
batch start
#iterations: 242
currently lose_sum: 97.23851120471954
time_elpased: 1.947
batch start
#iterations: 243
currently lose_sum: 97.85771083831787
time_elpased: 1.57
batch start
#iterations: 244
currently lose_sum: 97.62449902296066
time_elpased: 1.5
batch start
#iterations: 245
currently lose_sum: 97.30634719133377
time_elpased: 1.429
batch start
#iterations: 246
currently lose_sum: 97.83945679664612
time_elpased: 1.451
batch start
#iterations: 247
currently lose_sum: 97.23314309120178
time_elpased: 1.475
batch start
#iterations: 248
currently lose_sum: 97.49758470058441
time_elpased: 1.532
batch start
#iterations: 249
currently lose_sum: 97.24718791246414
time_elpased: 1.437
batch start
#iterations: 250
currently lose_sum: 97.13965004682541
time_elpased: 1.441
batch start
#iterations: 251
currently lose_sum: 97.33844190835953
time_elpased: 1.486
batch start
#iterations: 252
currently lose_sum: 97.27269995212555
time_elpased: 1.399
batch start
#iterations: 253
currently lose_sum: 97.48847830295563
time_elpased: 1.499
batch start
#iterations: 254
currently lose_sum: 97.40405076742172
time_elpased: 1.433
batch start
#iterations: 255
currently lose_sum: 97.45480608940125
time_elpased: 1.501
batch start
#iterations: 256
currently lose_sum: 96.99873447418213
time_elpased: 1.392
batch start
#iterations: 257
currently lose_sum: 97.38201838731766
time_elpased: 1.439
batch start
#iterations: 258
currently lose_sum: 97.0775585770607
time_elpased: 1.391
batch start
#iterations: 259
currently lose_sum: 97.37577885389328
time_elpased: 1.404
start validation test
0.624742268041
0.644320739074
0.559843573119
0.599118942731
0.62485620771
63.638
batch start
#iterations: 260
currently lose_sum: 97.21517711877823
time_elpased: 1.434
batch start
#iterations: 261
currently lose_sum: 97.33630293607712
time_elpased: 1.397
batch start
#iterations: 262
currently lose_sum: 97.25375705957413
time_elpased: 1.468
batch start
#iterations: 263
currently lose_sum: 97.2802015542984
time_elpased: 1.404
batch start
#iterations: 264
currently lose_sum: 97.52779537439346
time_elpased: 1.417
batch start
#iterations: 265
currently lose_sum: 97.62487328052521
time_elpased: 1.451
batch start
#iterations: 266
currently lose_sum: 97.21161526441574
time_elpased: 1.358
batch start
#iterations: 267
currently lose_sum: 97.29195392131805
time_elpased: 1.397
batch start
#iterations: 268
currently lose_sum: 97.23850816488266
time_elpased: 1.405
batch start
#iterations: 269
currently lose_sum: 97.69872605800629
time_elpased: 1.425
batch start
#iterations: 270
currently lose_sum: 97.4629562497139
time_elpased: 1.387
batch start
#iterations: 271
currently lose_sum: 97.49694430828094
time_elpased: 1.572
batch start
#iterations: 272
currently lose_sum: 97.44697338342667
time_elpased: 1.51
batch start
#iterations: 273
currently lose_sum: 97.44172155857086
time_elpased: 1.392
batch start
#iterations: 274
currently lose_sum: 97.54071098566055
time_elpased: 2.196
batch start
#iterations: 275
currently lose_sum: 97.5474464893341
time_elpased: 1.617
batch start
#iterations: 276
currently lose_sum: 97.34112173318863
time_elpased: 2.36
batch start
#iterations: 277
currently lose_sum: 97.69867599010468
time_elpased: 2.639
batch start
#iterations: 278
currently lose_sum: 97.30596256256104
time_elpased: 2.292
batch start
#iterations: 279
currently lose_sum: 97.34732127189636
time_elpased: 2.441
start validation test
0.625773195876
0.646197786505
0.558814448904
0.599337748344
0.625890752284
63.708
batch start
#iterations: 280
currently lose_sum: 97.5388548374176
time_elpased: 2.204
batch start
#iterations: 281
currently lose_sum: 97.10463887453079
time_elpased: 2.235
batch start
#iterations: 282
currently lose_sum: 97.4415043592453
time_elpased: 2.495
batch start
#iterations: 283
currently lose_sum: 97.36546128988266
time_elpased: 2.331
batch start
#iterations: 284
currently lose_sum: 97.34184175729752
time_elpased: 2.284
batch start
#iterations: 285
currently lose_sum: 97.13379418849945
time_elpased: 2.149
batch start
#iterations: 286
currently lose_sum: 97.42273020744324
time_elpased: 2.256
batch start
#iterations: 287
currently lose_sum: 97.15539622306824
time_elpased: 2.432
batch start
#iterations: 288
currently lose_sum: 97.21717059612274
time_elpased: 2.095
batch start
#iterations: 289
currently lose_sum: 97.17639845609665
time_elpased: 2.286
batch start
#iterations: 290
currently lose_sum: 97.24111491441727
time_elpased: 2.349
batch start
#iterations: 291
currently lose_sum: 97.1598334312439
time_elpased: 2.583
batch start
#iterations: 292
currently lose_sum: 97.37675005197525
time_elpased: 2.154
batch start
#iterations: 293
currently lose_sum: 97.33788353204727
time_elpased: 1.99
batch start
#iterations: 294
currently lose_sum: 97.29340690374374
time_elpased: 2.722
batch start
#iterations: 295
currently lose_sum: 97.20203506946564
time_elpased: 2.64
batch start
#iterations: 296
currently lose_sum: 97.17993307113647
time_elpased: 2.449
batch start
#iterations: 297
currently lose_sum: 97.79114013910294
time_elpased: 2.522
batch start
#iterations: 298
currently lose_sum: 97.25703781843185
time_elpased: 2.862
batch start
#iterations: 299
currently lose_sum: 97.2002387046814
time_elpased: 2.408
start validation test
0.630618556701
0.628565668783
0.641761860657
0.635095223546
0.630598992912
63.208
batch start
#iterations: 300
currently lose_sum: 97.0992037653923
time_elpased: 2.393
batch start
#iterations: 301
currently lose_sum: 97.23992830514908
time_elpased: 2.45
batch start
#iterations: 302
currently lose_sum: 97.52307552099228
time_elpased: 2.602
batch start
#iterations: 303
currently lose_sum: 97.22736477851868
time_elpased: 2.733
batch start
#iterations: 304
currently lose_sum: 97.23474699258804
time_elpased: 2.613
batch start
#iterations: 305
currently lose_sum: 97.20382672548294
time_elpased: 2.383
batch start
#iterations: 306
currently lose_sum: 97.15320163965225
time_elpased: 2.635
batch start
#iterations: 307
currently lose_sum: 97.0906417965889
time_elpased: 2.967
batch start
#iterations: 308
currently lose_sum: 97.57504826784134
time_elpased: 2.873
batch start
#iterations: 309
currently lose_sum: 97.40823632478714
time_elpased: 2.802
batch start
#iterations: 310
currently lose_sum: 97.49870771169662
time_elpased: 2.919
batch start
#iterations: 311
currently lose_sum: 97.24386340379715
time_elpased: 2.651
batch start
#iterations: 312
currently lose_sum: 97.43725717067719
time_elpased: 3.151
batch start
#iterations: 313
currently lose_sum: 97.28312563896179
time_elpased: 3.388
batch start
#iterations: 314
currently lose_sum: 97.15878522396088
time_elpased: 2.986
batch start
#iterations: 315
currently lose_sum: 97.12779062986374
time_elpased: 3.456
batch start
#iterations: 316
currently lose_sum: 96.89805018901825
time_elpased: 4.05
batch start
#iterations: 317
currently lose_sum: 97.2862069606781
time_elpased: 3.572
batch start
#iterations: 318
currently lose_sum: 97.3698405623436
time_elpased: 3.496
batch start
#iterations: 319
currently lose_sum: 97.32555437088013
time_elpased: 3.856
start validation test
0.631237113402
0.632619269378
0.629103632808
0.630856553148
0.631240859056
63.272
batch start
#iterations: 320
currently lose_sum: 97.35654044151306
time_elpased: 3.491
batch start
#iterations: 321
currently lose_sum: 97.01348841190338
time_elpased: 3.764
batch start
#iterations: 322
currently lose_sum: 97.42982321977615
time_elpased: 3.456
batch start
#iterations: 323
currently lose_sum: 96.85213124752045
time_elpased: 3.264
batch start
#iterations: 324
currently lose_sum: 97.37311500310898
time_elpased: 3.63
batch start
#iterations: 325
currently lose_sum: 97.31550335884094
time_elpased: 3.474
batch start
#iterations: 326
currently lose_sum: 97.53467893600464
time_elpased: 3.588
batch start
#iterations: 327
currently lose_sum: 97.07594156265259
time_elpased: 3.765
batch start
#iterations: 328
currently lose_sum: 97.38894093036652
time_elpased: 3.698
batch start
#iterations: 329
currently lose_sum: 97.08414226770401
time_elpased: 3.138
batch start
#iterations: 330
currently lose_sum: 97.16471672058105
time_elpased: 3.669
batch start
#iterations: 331
currently lose_sum: 97.21633160114288
time_elpased: 3.722
batch start
#iterations: 332
currently lose_sum: 97.37217372655869
time_elpased: 3.963
batch start
#iterations: 333
currently lose_sum: 97.39116883277893
time_elpased: 3.252
batch start
#iterations: 334
currently lose_sum: 97.53337293863297
time_elpased: 3.248
batch start
#iterations: 335
currently lose_sum: 97.14996707439423
time_elpased: 3.643
batch start
#iterations: 336
currently lose_sum: 97.37534230947495
time_elpased: 3.236
batch start
#iterations: 337
currently lose_sum: 97.276746571064
time_elpased: 3.102
batch start
#iterations: 338
currently lose_sum: 97.23624885082245
time_elpased: 3.254
batch start
#iterations: 339
currently lose_sum: 97.01561087369919
time_elpased: 3.148
start validation test
0.631340206186
0.639145058045
0.606257075229
0.622266821591
0.631384243491
63.373
batch start
#iterations: 340
currently lose_sum: 97.22214376926422
time_elpased: 3.959
batch start
#iterations: 341
currently lose_sum: 97.639927983284
time_elpased: 3.89
batch start
#iterations: 342
currently lose_sum: 97.21357375383377
time_elpased: 3.628
batch start
#iterations: 343
currently lose_sum: 97.20038431882858
time_elpased: 3.23
batch start
#iterations: 344
currently lose_sum: 97.25429421663284
time_elpased: 5.434
batch start
#iterations: 345
currently lose_sum: 97.38775193691254
time_elpased: 7.321
batch start
#iterations: 346
currently lose_sum: 97.19755357503891
time_elpased: 3.168
batch start
#iterations: 347
currently lose_sum: 96.94003957509995
time_elpased: 2.879
batch start
#iterations: 348
currently lose_sum: 97.54883253574371
time_elpased: 2.745
batch start
#iterations: 349
currently lose_sum: 97.34119027853012
time_elpased: 2.823
batch start
#iterations: 350
currently lose_sum: 97.22033542394638
time_elpased: 2.72
batch start
#iterations: 351
currently lose_sum: 97.3009769320488
time_elpased: 2.782
batch start
#iterations: 352
currently lose_sum: 97.05565059185028
time_elpased: 3.194
batch start
#iterations: 353
currently lose_sum: 97.22554397583008
time_elpased: 2.863
batch start
#iterations: 354
currently lose_sum: 97.17163038253784
time_elpased: 2.62
batch start
#iterations: 355
currently lose_sum: 97.11943727731705
time_elpased: 2.923
batch start
#iterations: 356
currently lose_sum: 97.24974727630615
time_elpased: 3.444
batch start
#iterations: 357
currently lose_sum: 97.34387773275375
time_elpased: 3.09
batch start
#iterations: 358
currently lose_sum: 97.18518352508545
time_elpased: 3.22
batch start
#iterations: 359
currently lose_sum: 97.30043303966522
time_elpased: 3.096
start validation test
0.626804123711
0.641332876869
0.578367808995
0.608225108225
0.626889161133
63.542
batch start
#iterations: 360
currently lose_sum: 97.23979878425598
time_elpased: 3.357
batch start
#iterations: 361
currently lose_sum: 97.43138003349304
time_elpased: 3.925
batch start
#iterations: 362
currently lose_sum: 97.46851688623428
time_elpased: 2.78
batch start
#iterations: 363
currently lose_sum: 97.20870769023895
time_elpased: 3.158
batch start
#iterations: 364
currently lose_sum: 97.01108253002167
time_elpased: 3.132
batch start
#iterations: 365
currently lose_sum: 97.28104096651077
time_elpased: 3.124
batch start
#iterations: 366
currently lose_sum: 97.32279235124588
time_elpased: 3.003
batch start
#iterations: 367
currently lose_sum: 97.23829793930054
time_elpased: 2.71
batch start
#iterations: 368
currently lose_sum: 97.33313518762589
time_elpased: 2.706
batch start
#iterations: 369
currently lose_sum: 97.35638880729675
time_elpased: 3.58
batch start
#iterations: 370
currently lose_sum: 97.17898952960968
time_elpased: 3.767
batch start
#iterations: 371
currently lose_sum: 97.16996216773987
time_elpased: 2.469
batch start
#iterations: 372
currently lose_sum: 97.1971333026886
time_elpased: 2.261
batch start
#iterations: 373
currently lose_sum: 97.2008808851242
time_elpased: 2.296
batch start
#iterations: 374
currently lose_sum: 97.29130917787552
time_elpased: 2.276
batch start
#iterations: 375
currently lose_sum: 97.14334738254547
time_elpased: 2.447
batch start
#iterations: 376
currently lose_sum: 97.16627925634384
time_elpased: 3.53
batch start
#iterations: 377
currently lose_sum: 97.29570859670639
time_elpased: 2.311
batch start
#iterations: 378
currently lose_sum: 97.2582738995552
time_elpased: 2.829
batch start
#iterations: 379
currently lose_sum: 97.52780872583389
time_elpased: 2.877
start validation test
0.631340206186
0.635671215487
0.618400740969
0.626917057903
0.631362923413
63.446
batch start
#iterations: 380
currently lose_sum: 97.41073328256607
time_elpased: 2.852
batch start
#iterations: 381
currently lose_sum: 97.2321959733963
time_elpased: 2.794
batch start
#iterations: 382
currently lose_sum: 96.96824777126312
time_elpased: 2.657
batch start
#iterations: 383
currently lose_sum: 97.06422954797745
time_elpased: 2.214
batch start
#iterations: 384
currently lose_sum: 97.13482916355133
time_elpased: 2.836
batch start
#iterations: 385
currently lose_sum: 97.66675305366516
time_elpased: 2.755
batch start
#iterations: 386
currently lose_sum: 97.39853972196579
time_elpased: 2.422
batch start
#iterations: 387
currently lose_sum: 97.15026497840881
time_elpased: 2.846
batch start
#iterations: 388
currently lose_sum: 97.31574165821075
time_elpased: 2.659
batch start
#iterations: 389
currently lose_sum: 97.148532807827
time_elpased: 2.296
batch start
#iterations: 390
currently lose_sum: 97.36006253957748
time_elpased: 2.846
batch start
#iterations: 391
currently lose_sum: 97.52135026454926
time_elpased: 2.754
batch start
#iterations: 392
currently lose_sum: 97.20171236991882
time_elpased: 2.48
batch start
#iterations: 393
currently lose_sum: 97.24729460477829
time_elpased: 2.608
batch start
#iterations: 394
currently lose_sum: 96.97303378582001
time_elpased: 2.654
batch start
#iterations: 395
currently lose_sum: 96.8607639670372
time_elpased: 2.879
batch start
#iterations: 396
currently lose_sum: 97.23026472330093
time_elpased: 2.561
batch start
#iterations: 397
currently lose_sum: 97.52586245536804
time_elpased: 3.278
batch start
#iterations: 398
currently lose_sum: 97.17812365293503
time_elpased: 3.023
batch start
#iterations: 399
currently lose_sum: 97.00520783662796
time_elpased: 3.031
start validation test
0.622113402062
0.643873613121
0.549449418545
0.592925759343
0.622240974893
63.580
acc: 0.633
pre: 0.631
rec: 0.646
F1: 0.639
auc: 0.671
