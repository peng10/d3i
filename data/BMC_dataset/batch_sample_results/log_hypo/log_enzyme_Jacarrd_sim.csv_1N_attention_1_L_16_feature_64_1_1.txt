start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.53071373701096
time_elpased: 2.508
batch start
#iterations: 1
currently lose_sum: 100.19923919439316
time_elpased: 2.464
batch start
#iterations: 2
currently lose_sum: 100.25241732597351
time_elpased: 2.274
batch start
#iterations: 3
currently lose_sum: 99.96392405033112
time_elpased: 2.374
batch start
#iterations: 4
currently lose_sum: 99.82995754480362
time_elpased: 2.634
batch start
#iterations: 5
currently lose_sum: 99.50713872909546
time_elpased: 2.613
batch start
#iterations: 6
currently lose_sum: 99.42366963624954
time_elpased: 2.202
batch start
#iterations: 7
currently lose_sum: 99.55998367071152
time_elpased: 2.488
batch start
#iterations: 8
currently lose_sum: 99.36019921302795
time_elpased: 2.259
batch start
#iterations: 9
currently lose_sum: 99.26634579896927
time_elpased: 2.138
batch start
#iterations: 10
currently lose_sum: 99.18157988786697
time_elpased: 2.319
batch start
#iterations: 11
currently lose_sum: 99.02897423505783
time_elpased: 2.208
batch start
#iterations: 12
currently lose_sum: 99.19023215770721
time_elpased: 2.127
batch start
#iterations: 13
currently lose_sum: 99.0170311331749
time_elpased: 2.395
batch start
#iterations: 14
currently lose_sum: 98.81991797685623
time_elpased: 2.427
batch start
#iterations: 15
currently lose_sum: 98.59619981050491
time_elpased: 2.569
batch start
#iterations: 16
currently lose_sum: 98.71259474754333
time_elpased: 2.501
batch start
#iterations: 17
currently lose_sum: 98.7855994105339
time_elpased: 2.469
batch start
#iterations: 18
currently lose_sum: 98.38482093811035
time_elpased: 2.599
batch start
#iterations: 19
currently lose_sum: 98.76468199491501
time_elpased: 2.551
start validation test
0.606597938144
0.642476424764
0.483791293609
0.551954913702
0.60681354415
64.830
batch start
#iterations: 20
currently lose_sum: 98.42592597007751
time_elpased: 2.475
batch start
#iterations: 21
currently lose_sum: 98.36801022291183
time_elpased: 2.323
batch start
#iterations: 22
currently lose_sum: 98.53249704837799
time_elpased: 2.44
batch start
#iterations: 23
currently lose_sum: 98.61108654737473
time_elpased: 2.756
batch start
#iterations: 24
currently lose_sum: 98.46809530258179
time_elpased: 2.253
batch start
#iterations: 25
currently lose_sum: 98.3874568939209
time_elpased: 2.568
batch start
#iterations: 26
currently lose_sum: 98.49050909280777
time_elpased: 2.525
batch start
#iterations: 27
currently lose_sum: 98.3400850892067
time_elpased: 2.596
batch start
#iterations: 28
currently lose_sum: 98.29465931653976
time_elpased: 2.865
batch start
#iterations: 29
currently lose_sum: 98.41940659284592
time_elpased: 2.446
batch start
#iterations: 30
currently lose_sum: 98.28273743391037
time_elpased: 2.472
batch start
#iterations: 31
currently lose_sum: 98.03285920619965
time_elpased: 2.467
batch start
#iterations: 32
currently lose_sum: 98.45805871486664
time_elpased: 2.529
batch start
#iterations: 33
currently lose_sum: 98.49695736169815
time_elpased: 2.587
batch start
#iterations: 34
currently lose_sum: 98.04098010063171
time_elpased: 2.575
batch start
#iterations: 35
currently lose_sum: 98.2963016629219
time_elpased: 2.024
batch start
#iterations: 36
currently lose_sum: 98.28963619470596
time_elpased: 2.563
batch start
#iterations: 37
currently lose_sum: 98.14681321382523
time_elpased: 2.289
batch start
#iterations: 38
currently lose_sum: 98.06269830465317
time_elpased: 2.351
batch start
#iterations: 39
currently lose_sum: 98.24075067043304
time_elpased: 2.65
start validation test
0.624381443299
0.626378198461
0.619738602449
0.62304071181
0.624389594522
63.904
batch start
#iterations: 40
currently lose_sum: 98.11667859554291
time_elpased: 2.757
batch start
#iterations: 41
currently lose_sum: 98.02279722690582
time_elpased: 2.719
batch start
#iterations: 42
currently lose_sum: 97.91182607412338
time_elpased: 2.384
batch start
#iterations: 43
currently lose_sum: 98.11720710992813
time_elpased: 2.484
batch start
#iterations: 44
currently lose_sum: 97.94893723726273
time_elpased: 2.502
batch start
#iterations: 45
currently lose_sum: 97.80752098560333
time_elpased: 2.381
batch start
#iterations: 46
currently lose_sum: 97.85304963588715
time_elpased: 2.419
batch start
#iterations: 47
currently lose_sum: 98.11281210184097
time_elpased: 2.365
batch start
#iterations: 48
currently lose_sum: 98.22819030284882
time_elpased: 2.467
batch start
#iterations: 49
currently lose_sum: 97.94506365060806
time_elpased: 2.353
batch start
#iterations: 50
currently lose_sum: 98.05580770969391
time_elpased: 2.715
batch start
#iterations: 51
currently lose_sum: 97.91983437538147
time_elpased: 2.733
batch start
#iterations: 52
currently lose_sum: 98.17562144994736
time_elpased: 2.494
batch start
#iterations: 53
currently lose_sum: 98.16743093729019
time_elpased: 2.551
batch start
#iterations: 54
currently lose_sum: 98.13655853271484
time_elpased: 2.547
batch start
#iterations: 55
currently lose_sum: 97.90705114603043
time_elpased: 2.557
batch start
#iterations: 56
currently lose_sum: 98.00450778007507
time_elpased: 2.563
batch start
#iterations: 57
currently lose_sum: 98.13087773323059
time_elpased: 2.501
batch start
#iterations: 58
currently lose_sum: 98.10942935943604
time_elpased: 2.669
batch start
#iterations: 59
currently lose_sum: 98.13913768529892
time_elpased: 2.369
start validation test
0.620103092784
0.629539684292
0.586909539981
0.607477631018
0.620161369185
63.878
batch start
#iterations: 60
currently lose_sum: 97.84411132335663
time_elpased: 2.53
batch start
#iterations: 61
currently lose_sum: 98.13009285926819
time_elpased: 2.6
batch start
#iterations: 62
currently lose_sum: 97.79423898458481
time_elpased: 2.367
batch start
#iterations: 63
currently lose_sum: 97.97994661331177
time_elpased: 2.518
batch start
#iterations: 64
currently lose_sum: 97.77075338363647
time_elpased: 2.381
batch start
#iterations: 65
currently lose_sum: 97.82424908876419
time_elpased: 2.249
batch start
#iterations: 66
currently lose_sum: 97.68576538562775
time_elpased: 2.482
batch start
#iterations: 67
currently lose_sum: 97.80168682336807
time_elpased: 2.51
batch start
#iterations: 68
currently lose_sum: 98.10547947883606
time_elpased: 2.455
batch start
#iterations: 69
currently lose_sum: 98.04906004667282
time_elpased: 2.57
batch start
#iterations: 70
currently lose_sum: 97.59297293424606
time_elpased: 2.414
batch start
#iterations: 71
currently lose_sum: 97.61928331851959
time_elpased: 2.543
batch start
#iterations: 72
currently lose_sum: 97.9196874499321
time_elpased: 2.449
batch start
#iterations: 73
currently lose_sum: 97.75517874956131
time_elpased: 2.281
batch start
#iterations: 74
currently lose_sum: 97.65030789375305
time_elpased: 2.532
batch start
#iterations: 75
currently lose_sum: 97.76737332344055
time_elpased: 2.435
batch start
#iterations: 76
currently lose_sum: 97.7758292555809
time_elpased: 1.998
batch start
#iterations: 77
currently lose_sum: 97.84446531534195
time_elpased: 2.045
batch start
#iterations: 78
currently lose_sum: 97.95118451118469
time_elpased: 2.118
batch start
#iterations: 79
currently lose_sum: 97.92392289638519
time_elpased: 2.463
start validation test
0.618917525773
0.64
0.546670783164
0.58966531609
0.619044366073
63.902
batch start
#iterations: 80
currently lose_sum: 97.6287373304367
time_elpased: 2.15
batch start
#iterations: 81
currently lose_sum: 97.68604171276093
time_elpased: 2.265
batch start
#iterations: 82
currently lose_sum: 97.78577148914337
time_elpased: 2.53
batch start
#iterations: 83
currently lose_sum: 97.44050425291061
time_elpased: 2.668
batch start
#iterations: 84
currently lose_sum: 97.58564758300781
time_elpased: 2.602
batch start
#iterations: 85
currently lose_sum: 97.76932340860367
time_elpased: 2.54
batch start
#iterations: 86
currently lose_sum: 97.60589718818665
time_elpased: 2.567
batch start
#iterations: 87
currently lose_sum: 97.39057397842407
time_elpased: 2.593
batch start
#iterations: 88
currently lose_sum: 97.66837257146835
time_elpased: 2.66
batch start
#iterations: 89
currently lose_sum: 97.49290442466736
time_elpased: 2.463
batch start
#iterations: 90
currently lose_sum: 97.64912474155426
time_elpased: 2.48
batch start
#iterations: 91
currently lose_sum: 97.27838224172592
time_elpased: 2.353
batch start
#iterations: 92
currently lose_sum: 97.5794193148613
time_elpased: 2.445
batch start
#iterations: 93
currently lose_sum: 97.7146605849266
time_elpased: 2.629
batch start
#iterations: 94
currently lose_sum: 97.52007782459259
time_elpased: 2.318
batch start
#iterations: 95
currently lose_sum: 97.66002607345581
time_elpased: 2.499
batch start
#iterations: 96
currently lose_sum: 97.66993486881256
time_elpased: 2.357
batch start
#iterations: 97
currently lose_sum: 97.40215331315994
time_elpased: 2.408
batch start
#iterations: 98
currently lose_sum: 97.3822135925293
time_elpased: 2.426
batch start
#iterations: 99
currently lose_sum: 97.57544112205505
time_elpased: 2.445
start validation test
0.619690721649
0.63370298388
0.570443552537
0.600411611785
0.619777182651
63.764
batch start
#iterations: 100
currently lose_sum: 97.58456927537918
time_elpased: 2.432
batch start
#iterations: 101
currently lose_sum: 97.6842086315155
time_elpased: 2.522
batch start
#iterations: 102
currently lose_sum: 97.26191937923431
time_elpased: 2.618
batch start
#iterations: 103
currently lose_sum: 97.50402837991714
time_elpased: 2.552
batch start
#iterations: 104
currently lose_sum: 97.58811640739441
time_elpased: 2.576
batch start
#iterations: 105
currently lose_sum: 97.62425428628922
time_elpased: 2.625
batch start
#iterations: 106
currently lose_sum: 97.51138186454773
time_elpased: 2.397
batch start
#iterations: 107
currently lose_sum: 97.5864474773407
time_elpased: 2.487
batch start
#iterations: 108
currently lose_sum: 97.74758857488632
time_elpased: 2.507
batch start
#iterations: 109
currently lose_sum: 97.53573000431061
time_elpased: 2.33
batch start
#iterations: 110
currently lose_sum: 97.13203662633896
time_elpased: 2.474
batch start
#iterations: 111
currently lose_sum: 97.56987565755844
time_elpased: 2.857
batch start
#iterations: 112
currently lose_sum: 97.56268364191055
time_elpased: 2.524
batch start
#iterations: 113
currently lose_sum: 97.38743370771408
time_elpased: 2.513
batch start
#iterations: 114
currently lose_sum: 97.25132083892822
time_elpased: 2.546
batch start
#iterations: 115
currently lose_sum: 97.54988235235214
time_elpased: 2.265
batch start
#iterations: 116
currently lose_sum: 97.68758052587509
time_elpased: 2.465
batch start
#iterations: 117
currently lose_sum: 97.61644512414932
time_elpased: 2.582
batch start
#iterations: 118
currently lose_sum: 97.33333522081375
time_elpased: 2.413
batch start
#iterations: 119
currently lose_sum: 97.54188740253448
time_elpased: 2.456
start validation test
0.622216494845
0.644902912621
0.546876608007
0.591858328229
0.622348765637
63.781
batch start
#iterations: 120
currently lose_sum: 97.49339592456818
time_elpased: 2.466
batch start
#iterations: 121
currently lose_sum: 97.33055865764618
time_elpased: 2.463
batch start
#iterations: 122
currently lose_sum: 97.6411269903183
time_elpased: 2.163
batch start
#iterations: 123
currently lose_sum: 97.24024713039398
time_elpased: 2.452
batch start
#iterations: 124
currently lose_sum: 97.24205487966537
time_elpased: 2.609
batch start
#iterations: 125
currently lose_sum: 97.63896214962006
time_elpased: 2.648
batch start
#iterations: 126
currently lose_sum: 97.57388597726822
time_elpased: 2.478
batch start
#iterations: 127
currently lose_sum: 97.73981434106827
time_elpased: 2.563
batch start
#iterations: 128
currently lose_sum: 97.1527887582779
time_elpased: 2.613
batch start
#iterations: 129
currently lose_sum: 97.55654489994049
time_elpased: 2.455
batch start
#iterations: 130
currently lose_sum: 97.22540336847305
time_elpased: 2.361
batch start
#iterations: 131
currently lose_sum: 97.4340386390686
time_elpased: 2.479
batch start
#iterations: 132
currently lose_sum: 97.30496799945831
time_elpased: 2.371
batch start
#iterations: 133
currently lose_sum: 97.19165164232254
time_elpased: 2.596
batch start
#iterations: 134
currently lose_sum: 97.34485477209091
time_elpased: 2.626
batch start
#iterations: 135
currently lose_sum: 97.38324666023254
time_elpased: 2.482
batch start
#iterations: 136
currently lose_sum: 97.41330641508102
time_elpased: 2.466
batch start
#iterations: 137
currently lose_sum: 97.2627632021904
time_elpased: 2.501
batch start
#iterations: 138
currently lose_sum: 97.18448424339294
time_elpased: 2.558
batch start
#iterations: 139
currently lose_sum: 97.43903893232346
time_elpased: 2.497
start validation test
0.627783505155
0.634395864743
0.606257075229
0.620007367258
0.627821298123
63.468
batch start
#iterations: 140
currently lose_sum: 97.68021541833878
time_elpased: 2.329
batch start
#iterations: 141
currently lose_sum: 97.26028054952621
time_elpased: 2.35
batch start
#iterations: 142
currently lose_sum: 97.5211312174797
time_elpased: 2.503
batch start
#iterations: 143
currently lose_sum: 97.26882594823837
time_elpased: 2.423
batch start
#iterations: 144
currently lose_sum: 97.62485182285309
time_elpased: 2.706
batch start
#iterations: 145
currently lose_sum: 97.39473730325699
time_elpased: 2.461
batch start
#iterations: 146
currently lose_sum: 97.29842311143875
time_elpased: 2.536
batch start
#iterations: 147
currently lose_sum: 97.35260665416718
time_elpased: 2.515
batch start
#iterations: 148
currently lose_sum: 97.48050969839096
time_elpased: 2.288
batch start
#iterations: 149
currently lose_sum: 97.11364740133286
time_elpased: 2.319
batch start
#iterations: 150
currently lose_sum: 96.99152278900146
time_elpased: 2.472
batch start
#iterations: 151
currently lose_sum: 97.50529044866562
time_elpased: 2.68
batch start
#iterations: 152
currently lose_sum: 97.45981156826019
time_elpased: 2.613
batch start
#iterations: 153
currently lose_sum: 97.65615236759186
time_elpased: 2.39
batch start
#iterations: 154
currently lose_sum: 97.33168494701385
time_elpased: 2.45
batch start
#iterations: 155
currently lose_sum: 97.18706226348877
time_elpased: 2.235
batch start
#iterations: 156
currently lose_sum: 97.47416007518768
time_elpased: 2.603
batch start
#iterations: 157
currently lose_sum: 97.52740621566772
time_elpased: 2.574
batch start
#iterations: 158
currently lose_sum: 97.75392884016037
time_elpased: 2.589
batch start
#iterations: 159
currently lose_sum: 97.60824728012085
time_elpased: 2.946
start validation test
0.617216494845
0.652835223482
0.503550478543
0.568556820823
0.617416053069
63.997
batch start
#iterations: 160
currently lose_sum: 97.43942296504974
time_elpased: 2.634
batch start
#iterations: 161
currently lose_sum: 97.05176132917404
time_elpased: 2.224
batch start
#iterations: 162
currently lose_sum: 97.37741029262543
time_elpased: 2.73
batch start
#iterations: 163
currently lose_sum: 97.14367026090622
time_elpased: 2.656
batch start
#iterations: 164
currently lose_sum: 97.37549179792404
time_elpased: 2.49
batch start
#iterations: 165
currently lose_sum: 97.4247213602066
time_elpased: 2.275
batch start
#iterations: 166
currently lose_sum: 97.27843606472015
time_elpased: 2.427
batch start
#iterations: 167
currently lose_sum: 97.40394294261932
time_elpased: 2.56
batch start
#iterations: 168
currently lose_sum: 97.03253191709518
time_elpased: 2.636
batch start
#iterations: 169
currently lose_sum: 97.21604204177856
time_elpased: 2.61
batch start
#iterations: 170
currently lose_sum: 97.2255979180336
time_elpased: 2.209
batch start
#iterations: 171
currently lose_sum: 97.25213503837585
time_elpased: 2.31
batch start
#iterations: 172
currently lose_sum: 97.57739555835724
time_elpased: 2.583
batch start
#iterations: 173
currently lose_sum: 97.33106678724289
time_elpased: 2.239
batch start
#iterations: 174
currently lose_sum: 97.43724244832993
time_elpased: 2.653
batch start
#iterations: 175
currently lose_sum: 96.98410815000534
time_elpased: 2.372
batch start
#iterations: 176
currently lose_sum: 97.29083639383316
time_elpased: 2.497
batch start
#iterations: 177
currently lose_sum: 97.12837332487106
time_elpased: 2.531
batch start
#iterations: 178
currently lose_sum: 97.15260702371597
time_elpased: 2.516
batch start
#iterations: 179
currently lose_sum: 97.68231457471848
time_elpased: 2.521
start validation test
0.629381443299
0.635379834994
0.610270659669
0.622572178478
0.629414995227
63.453
batch start
#iterations: 180
currently lose_sum: 97.41649222373962
time_elpased: 2.504
batch start
#iterations: 181
currently lose_sum: 97.45118427276611
time_elpased: 2.209
batch start
#iterations: 182
currently lose_sum: 97.23598301410675
time_elpased: 2.256
batch start
#iterations: 183
currently lose_sum: 96.94327789545059
time_elpased: 2.433
batch start
#iterations: 184
currently lose_sum: 97.14372110366821
time_elpased: 2.415
batch start
#iterations: 185
currently lose_sum: 97.48299658298492
time_elpased: 2.527
batch start
#iterations: 186
currently lose_sum: 96.91763371229172
time_elpased: 2.37
batch start
#iterations: 187
currently lose_sum: 97.1002624630928
time_elpased: 2.621
batch start
#iterations: 188
currently lose_sum: 97.23938411474228
time_elpased: 2.492
batch start
#iterations: 189
currently lose_sum: 97.35596323013306
time_elpased: 2.475
batch start
#iterations: 190
currently lose_sum: 97.51094806194305
time_elpased: 2.763
batch start
#iterations: 191
currently lose_sum: 97.04204624891281
time_elpased: 2.33
batch start
#iterations: 192
currently lose_sum: 97.0289266705513
time_elpased: 2.428
batch start
#iterations: 193
currently lose_sum: 97.33765321969986
time_elpased: 2.274
batch start
#iterations: 194
currently lose_sum: 97.40122163295746
time_elpased: 2.499
batch start
#iterations: 195
currently lose_sum: 96.94848889112473
time_elpased: 2.427
batch start
#iterations: 196
currently lose_sum: 97.43932437896729
time_elpased: 2.522
batch start
#iterations: 197
currently lose_sum: 97.23864948749542
time_elpased: 2.497
batch start
#iterations: 198
currently lose_sum: 97.24590355157852
time_elpased: 2.495
batch start
#iterations: 199
currently lose_sum: 96.97778475284576
time_elpased: 2.686
start validation test
0.618453608247
0.646019931878
0.527014510651
0.58048061664
0.618614143687
63.710
batch start
#iterations: 200
currently lose_sum: 97.18735909461975
time_elpased: 2.265
batch start
#iterations: 201
currently lose_sum: 97.053482234478
time_elpased: 2.37
batch start
#iterations: 202
currently lose_sum: 97.25629806518555
time_elpased: 2.528
batch start
#iterations: 203
currently lose_sum: 96.95110112428665
time_elpased: 2.408
batch start
#iterations: 204
currently lose_sum: 96.99404013156891
time_elpased: 2.598
batch start
#iterations: 205
currently lose_sum: 97.35748463869095
time_elpased: 2.417
batch start
#iterations: 206
currently lose_sum: 97.15710771083832
time_elpased: 2.208
batch start
#iterations: 207
currently lose_sum: 97.05928075313568
time_elpased: 2.418
batch start
#iterations: 208
currently lose_sum: 96.7949720621109
time_elpased: 2.246
batch start
#iterations: 209
currently lose_sum: 97.08028602600098
time_elpased: 1.928
batch start
#iterations: 210
currently lose_sum: 97.08631736040115
time_elpased: 1.852
batch start
#iterations: 211
currently lose_sum: 96.66215199232101
time_elpased: 1.942
batch start
#iterations: 212
currently lose_sum: 97.16319280862808
time_elpased: 1.893
batch start
#iterations: 213
currently lose_sum: 97.1990817785263
time_elpased: 1.865
batch start
#iterations: 214
currently lose_sum: 97.38749206066132
time_elpased: 1.985
batch start
#iterations: 215
currently lose_sum: 96.93598186969757
time_elpased: 2.754
batch start
#iterations: 216
currently lose_sum: 96.95980244874954
time_elpased: 2.326
batch start
#iterations: 217
currently lose_sum: 97.06628465652466
time_elpased: 2.534
batch start
#iterations: 218
currently lose_sum: 97.20938640832901
time_elpased: 2.733
batch start
#iterations: 219
currently lose_sum: 97.24399590492249
time_elpased: 2.576
start validation test
0.624793814433
0.640179392824
0.572913450653
0.604681475045
0.624884898414
63.521
batch start
#iterations: 220
currently lose_sum: 96.9911458492279
time_elpased: 2.398
batch start
#iterations: 221
currently lose_sum: 97.36352503299713
time_elpased: 2.299
batch start
#iterations: 222
currently lose_sum: 96.9233387708664
time_elpased: 2.476
batch start
#iterations: 223
currently lose_sum: 97.01985198259354
time_elpased: 2.309
batch start
#iterations: 224
currently lose_sum: 97.09274834394455
time_elpased: 2.575
batch start
#iterations: 225
currently lose_sum: 97.28083276748657
time_elpased: 2.562
batch start
#iterations: 226
currently lose_sum: 96.90717166662216
time_elpased: 2.474
batch start
#iterations: 227
currently lose_sum: 96.99786120653152
time_elpased: 2.437
batch start
#iterations: 228
currently lose_sum: 96.94861549139023
time_elpased: 2.553
batch start
#iterations: 229
currently lose_sum: 97.09833514690399
time_elpased: 2.408
batch start
#iterations: 230
currently lose_sum: 97.05523401498795
time_elpased: 2.367
batch start
#iterations: 231
currently lose_sum: 97.2341518998146
time_elpased: 2.672
batch start
#iterations: 232
currently lose_sum: 97.21843427419662
time_elpased: 2.476
batch start
#iterations: 233
currently lose_sum: 97.01513069868088
time_elpased: 2.588
batch start
#iterations: 234
currently lose_sum: 97.02521634101868
time_elpased: 2.434
batch start
#iterations: 235
currently lose_sum: 96.98327040672302
time_elpased: 2.515
batch start
#iterations: 236
currently lose_sum: 97.27204579114914
time_elpased: 2.522
batch start
#iterations: 237
currently lose_sum: 97.26428061723709
time_elpased: 2.508
batch start
#iterations: 238
currently lose_sum: 97.36147612333298
time_elpased: 2.535
batch start
#iterations: 239
currently lose_sum: 96.70781999826431
time_elpased: 2.528
start validation test
0.623453608247
0.646217264791
0.548523206751
0.593376008906
0.623585160124
63.661
batch start
#iterations: 240
currently lose_sum: 97.05465477705002
time_elpased: 2.552
batch start
#iterations: 241
currently lose_sum: 97.03853231668472
time_elpased: 2.68
batch start
#iterations: 242
currently lose_sum: 97.03157651424408
time_elpased: 2.441
batch start
#iterations: 243
currently lose_sum: 97.29428768157959
time_elpased: 2.584
batch start
#iterations: 244
currently lose_sum: 97.17375522851944
time_elpased: 2.424
batch start
#iterations: 245
currently lose_sum: 97.19300639629364
time_elpased: 2.627
batch start
#iterations: 246
currently lose_sum: 96.80677646398544
time_elpased: 2.807
batch start
#iterations: 247
currently lose_sum: 97.1086533665657
time_elpased: 2.936
batch start
#iterations: 248
currently lose_sum: 97.08817058801651
time_elpased: 2.749
batch start
#iterations: 249
currently lose_sum: 97.2772496342659
time_elpased: 2.442
batch start
#iterations: 250
currently lose_sum: 96.88434588909149
time_elpased: 2.637
batch start
#iterations: 251
currently lose_sum: 97.30966275930405
time_elpased: 2.651
batch start
#iterations: 252
currently lose_sum: 97.00912284851074
time_elpased: 2.582
batch start
#iterations: 253
currently lose_sum: 97.0809400677681
time_elpased: 2.64
batch start
#iterations: 254
currently lose_sum: 96.94525438547134
time_elpased: 2.414
batch start
#iterations: 255
currently lose_sum: 97.1387727856636
time_elpased: 2.53
batch start
#iterations: 256
currently lose_sum: 96.86622846126556
time_elpased: 2.708
batch start
#iterations: 257
currently lose_sum: 97.02931916713715
time_elpased: 2.55
batch start
#iterations: 258
currently lose_sum: 97.00917601585388
time_elpased: 2.56
batch start
#iterations: 259
currently lose_sum: 97.03028839826584
time_elpased: 2.411
start validation test
0.625154639175
0.635818242418
0.588967788412
0.611496954803
0.625218170773
63.527
batch start
#iterations: 260
currently lose_sum: 96.75686275959015
time_elpased: 2.565
batch start
#iterations: 261
currently lose_sum: 96.89202153682709
time_elpased: 2.553
batch start
#iterations: 262
currently lose_sum: 96.62581533193588
time_elpased: 2.617
batch start
#iterations: 263
currently lose_sum: 96.9497007727623
time_elpased: 2.448
batch start
#iterations: 264
currently lose_sum: 97.00437527894974
time_elpased: 2.614
batch start
#iterations: 265
currently lose_sum: 97.202388048172
time_elpased: 2.526
batch start
#iterations: 266
currently lose_sum: 96.98610305786133
time_elpased: 2.345
batch start
#iterations: 267
currently lose_sum: 96.86137330532074
time_elpased: 2.193
batch start
#iterations: 268
currently lose_sum: 96.90426868200302
time_elpased: 2.328
batch start
#iterations: 269
currently lose_sum: 97.12784618139267
time_elpased: 2.192
batch start
#iterations: 270
currently lose_sum: 96.97682249546051
time_elpased: 2.463
batch start
#iterations: 271
currently lose_sum: 97.12257373332977
time_elpased: 2.5
batch start
#iterations: 272
currently lose_sum: 96.99299454689026
time_elpased: 2.547
batch start
#iterations: 273
currently lose_sum: 96.7573544383049
time_elpased: 2.477
batch start
#iterations: 274
currently lose_sum: 97.24596434831619
time_elpased: 2.439
batch start
#iterations: 275
currently lose_sum: 97.20478785037994
time_elpased: 2.558
batch start
#iterations: 276
currently lose_sum: 96.80614078044891
time_elpased: 2.532
batch start
#iterations: 277
currently lose_sum: 97.05664700269699
time_elpased: 2.368
batch start
#iterations: 278
currently lose_sum: 96.86743903160095
time_elpased: 2.32
batch start
#iterations: 279
currently lose_sum: 96.64273208379745
time_elpased: 2.134
start validation test
0.625463917526
0.641529044924
0.571678501595
0.604592947323
0.625558346119
63.635
batch start
#iterations: 280
currently lose_sum: 96.97901612520218
time_elpased: 2.14
batch start
#iterations: 281
currently lose_sum: 97.3535680770874
time_elpased: 2.345
batch start
#iterations: 282
currently lose_sum: 96.84748667478561
time_elpased: 2.2
batch start
#iterations: 283
currently lose_sum: 97.15391081571579
time_elpased: 2.133
batch start
#iterations: 284
currently lose_sum: 96.97225844860077
time_elpased: 2.338
batch start
#iterations: 285
currently lose_sum: 96.90252709388733
time_elpased: 2.658
batch start
#iterations: 286
currently lose_sum: 96.52998495101929
time_elpased: 2.463
batch start
#iterations: 287
currently lose_sum: 96.83725428581238
time_elpased: 2.534
batch start
#iterations: 288
currently lose_sum: 96.75285959243774
time_elpased: 2.63
batch start
#iterations: 289
currently lose_sum: 97.45237946510315
time_elpased: 2.47
batch start
#iterations: 290
currently lose_sum: 97.08281171321869
time_elpased: 2.599
batch start
#iterations: 291
currently lose_sum: 96.79665696620941
time_elpased: 2.693
batch start
#iterations: 292
currently lose_sum: 96.91230827569962
time_elpased: 2.625
batch start
#iterations: 293
currently lose_sum: 96.96087574958801
time_elpased: 2.506
batch start
#iterations: 294
currently lose_sum: 96.97097635269165
time_elpased: 2.733
batch start
#iterations: 295
currently lose_sum: 97.08721661567688
time_elpased: 2.725
batch start
#iterations: 296
currently lose_sum: 96.98248082399368
time_elpased: 2.645
batch start
#iterations: 297
currently lose_sum: 97.02375465631485
time_elpased: 2.558
batch start
#iterations: 298
currently lose_sum: 96.88392078876495
time_elpased: 2.396
batch start
#iterations: 299
currently lose_sum: 96.8591098189354
time_elpased: 2.232
start validation test
0.624020618557
0.638504630159
0.574765874241
0.604961005199
0.624107092857
63.707
batch start
#iterations: 300
currently lose_sum: 96.64346009492874
time_elpased: 2.718
batch start
#iterations: 301
currently lose_sum: 96.90293157100677
time_elpased: 2.62
batch start
#iterations: 302
currently lose_sum: 96.8929340839386
time_elpased: 2.529
batch start
#iterations: 303
currently lose_sum: 97.02444714307785
time_elpased: 2.544
batch start
#iterations: 304
currently lose_sum: 96.8963331580162
time_elpased: 2.72
batch start
#iterations: 305
currently lose_sum: 97.11767768859863
time_elpased: 2.382
batch start
#iterations: 306
currently lose_sum: 96.94505733251572
time_elpased: 2.293
batch start
#iterations: 307
currently lose_sum: 97.03520053625107
time_elpased: 2.449
batch start
#iterations: 308
currently lose_sum: 96.83386808633804
time_elpased: 2.426
batch start
#iterations: 309
currently lose_sum: 97.01658970117569
time_elpased: 2.633
batch start
#iterations: 310
currently lose_sum: 96.92642056941986
time_elpased: 2.427
batch start
#iterations: 311
currently lose_sum: 96.99573266506195
time_elpased: 2.346
batch start
#iterations: 312
currently lose_sum: 96.95024263858795
time_elpased: 2.478
batch start
#iterations: 313
currently lose_sum: 97.01491838693619
time_elpased: 2.567
batch start
#iterations: 314
currently lose_sum: 96.77549535036087
time_elpased: 2.381
batch start
#iterations: 315
currently lose_sum: 96.81976610422134
time_elpased: 2.519
batch start
#iterations: 316
currently lose_sum: 97.13744127750397
time_elpased: 2.514
batch start
#iterations: 317
currently lose_sum: 97.02172803878784
time_elpased: 2.492
batch start
#iterations: 318
currently lose_sum: 96.87456375360489
time_elpased: 2.351
batch start
#iterations: 319
currently lose_sum: 97.04376029968262
time_elpased: 2.562
start validation test
0.624175257732
0.637000225887
0.580426057425
0.607398632276
0.624252066201
63.631
batch start
#iterations: 320
currently lose_sum: 97.0591424703598
time_elpased: 2.593
batch start
#iterations: 321
currently lose_sum: 96.81754630804062
time_elpased: 2.487
batch start
#iterations: 322
currently lose_sum: 96.78631407022476
time_elpased: 2.437
batch start
#iterations: 323
currently lose_sum: 97.27483052015305
time_elpased: 2.422
batch start
#iterations: 324
currently lose_sum: 97.03322559595108
time_elpased: 2.464
batch start
#iterations: 325
currently lose_sum: 96.90653097629547
time_elpased: 2.363
batch start
#iterations: 326
currently lose_sum: 97.0433679819107
time_elpased: 2.369
batch start
#iterations: 327
currently lose_sum: 96.95732021331787
time_elpased: 2.324
batch start
#iterations: 328
currently lose_sum: 96.83553129434586
time_elpased: 2.714
batch start
#iterations: 329
currently lose_sum: 96.57868814468384
time_elpased: 2.493
batch start
#iterations: 330
currently lose_sum: 96.92930287122726
time_elpased: 2.485
batch start
#iterations: 331
currently lose_sum: 96.7619731426239
time_elpased: 2.372
batch start
#iterations: 332
currently lose_sum: 96.85585737228394
time_elpased: 2.525
batch start
#iterations: 333
currently lose_sum: 96.79561305046082
time_elpased: 2.251
batch start
#iterations: 334
currently lose_sum: 96.85210174322128
time_elpased: 2.485
batch start
#iterations: 335
currently lose_sum: 96.73120188713074
time_elpased: 2.437
batch start
#iterations: 336
currently lose_sum: 96.85689979791641
time_elpased: 2.316
batch start
#iterations: 337
currently lose_sum: 96.81624680757523
time_elpased: 2.621
batch start
#iterations: 338
currently lose_sum: 96.89350467920303
time_elpased: 2.472
batch start
#iterations: 339
currently lose_sum: 96.74732166528702
time_elpased: 2.587
start validation test
0.629948453608
0.636275773196
0.609756097561
0.622733722213
0.629983904404
63.406
batch start
#iterations: 340
currently lose_sum: 96.93412959575653
time_elpased: 2.465
batch start
#iterations: 341
currently lose_sum: 97.14028453826904
time_elpased: 2.354
batch start
#iterations: 342
currently lose_sum: 96.92096763849258
time_elpased: 2.856
batch start
#iterations: 343
currently lose_sum: 96.91967815160751
time_elpased: 2.599
batch start
#iterations: 344
currently lose_sum: 96.93687605857849
time_elpased: 2.631
batch start
#iterations: 345
currently lose_sum: 96.58503758907318
time_elpased: 2.567
batch start
#iterations: 346
currently lose_sum: 97.11262911558151
time_elpased: 2.468
batch start
#iterations: 347
currently lose_sum: 96.70818585157394
time_elpased: 2.457
batch start
#iterations: 348
currently lose_sum: 96.98030537366867
time_elpased: 2.409
batch start
#iterations: 349
currently lose_sum: 97.06042325496674
time_elpased: 2.446
batch start
#iterations: 350
currently lose_sum: 96.9019107222557
time_elpased: 2.398
batch start
#iterations: 351
currently lose_sum: 96.94575595855713
time_elpased: 2.488
batch start
#iterations: 352
currently lose_sum: 96.9959186911583
time_elpased: 2.744
batch start
#iterations: 353
currently lose_sum: 97.40456169843674
time_elpased: 2.453
batch start
#iterations: 354
currently lose_sum: 97.40045380592346
time_elpased: 2.443
batch start
#iterations: 355
currently lose_sum: 97.09068387746811
time_elpased: 2.512
batch start
#iterations: 356
currently lose_sum: 96.93132585287094
time_elpased: 2.568
batch start
#iterations: 357
currently lose_sum: 97.11027109622955
time_elpased: 2.629
batch start
#iterations: 358
currently lose_sum: 97.28852075338364
time_elpased: 2.664
batch start
#iterations: 359
currently lose_sum: 96.83428347110748
time_elpased: 2.421
start validation test
0.626804123711
0.634517215162
0.601214366574
0.617417036567
0.626849050477
63.533
batch start
#iterations: 360
currently lose_sum: 97.08747977018356
time_elpased: 2.432
batch start
#iterations: 361
currently lose_sum: 97.15587389469147
time_elpased: 2.577
batch start
#iterations: 362
currently lose_sum: 96.60208696126938
time_elpased: 2.734
batch start
#iterations: 363
currently lose_sum: 97.06336349248886
time_elpased: 2.665
batch start
#iterations: 364
currently lose_sum: 97.12196737527847
time_elpased: 2.677
batch start
#iterations: 365
currently lose_sum: 96.99169385433197
time_elpased: 2.53
batch start
#iterations: 366
currently lose_sum: 96.92594957351685
time_elpased: 2.542
batch start
#iterations: 367
currently lose_sum: 96.58513015508652
time_elpased: 2.407
batch start
#iterations: 368
currently lose_sum: 97.02799355983734
time_elpased: 2.477
batch start
#iterations: 369
currently lose_sum: 96.81893783807755
time_elpased: 2.407
batch start
#iterations: 370
currently lose_sum: 96.70068198442459
time_elpased: 2.484
batch start
#iterations: 371
currently lose_sum: 97.05061966180801
time_elpased: 2.443
batch start
#iterations: 372
currently lose_sum: 97.06617194414139
time_elpased: 2.535
batch start
#iterations: 373
currently lose_sum: 96.91701477766037
time_elpased: 2.536
batch start
#iterations: 374
currently lose_sum: 96.89589363336563
time_elpased: 2.158
batch start
#iterations: 375
currently lose_sum: 96.76857405900955
time_elpased: 2.521
batch start
#iterations: 376
currently lose_sum: 96.91909164190292
time_elpased: 2.417
batch start
#iterations: 377
currently lose_sum: 97.06768155097961
time_elpased: 2.494
batch start
#iterations: 378
currently lose_sum: 96.59823733568192
time_elpased: 2.447
batch start
#iterations: 379
currently lose_sum: 96.6938858628273
time_elpased: 2.13
start validation test
0.623453608247
0.639325323475
0.569517340743
0.602405704022
0.623548301684
63.549
batch start
#iterations: 380
currently lose_sum: 96.91081643104553
time_elpased: 2.168
batch start
#iterations: 381
currently lose_sum: 96.8714349269867
time_elpased: 2.04
batch start
#iterations: 382
currently lose_sum: 96.78480434417725
time_elpased: 2.23
batch start
#iterations: 383
currently lose_sum: 96.9335989356041
time_elpased: 2.047
batch start
#iterations: 384
currently lose_sum: 96.81158381700516
time_elpased: 2.159
batch start
#iterations: 385
currently lose_sum: 96.8448994755745
time_elpased: 2.632
batch start
#iterations: 386
currently lose_sum: 96.65489655733109
time_elpased: 2.489
batch start
#iterations: 387
currently lose_sum: 96.6500369310379
time_elpased: 2.625
batch start
#iterations: 388
currently lose_sum: 97.06630182266235
time_elpased: 2.559
batch start
#iterations: 389
currently lose_sum: 96.8346403837204
time_elpased: 2.571
batch start
#iterations: 390
currently lose_sum: 96.72980439662933
time_elpased: 2.463
batch start
#iterations: 391
currently lose_sum: 96.97922539710999
time_elpased: 2.448
batch start
#iterations: 392
currently lose_sum: 96.63039988279343
time_elpased: 2.469
batch start
#iterations: 393
currently lose_sum: 96.86851727962494
time_elpased: 2.711
batch start
#iterations: 394
currently lose_sum: 96.77252346277237
time_elpased: 2.495
batch start
#iterations: 395
currently lose_sum: 96.85451805591583
time_elpased: 2.562
batch start
#iterations: 396
currently lose_sum: 96.70246857404709
time_elpased: 2.512
batch start
#iterations: 397
currently lose_sum: 96.82985723018646
time_elpased: 2.533
batch start
#iterations: 398
currently lose_sum: 96.57335537672043
time_elpased: 2.565
batch start
#iterations: 399
currently lose_sum: 96.71357774734497
time_elpased: 2.594
start validation test
0.627319587629
0.635946211873
0.598641556036
0.616730279898
0.627369936337
63.521
acc: 0.630
pre: 0.637
rec: 0.610
F1: 0.623
auc: 0.671
