start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.67039328813553
time_elpased: 2.197
batch start
#iterations: 1
currently lose_sum: 100.26628315448761
time_elpased: 2.314
batch start
#iterations: 2
currently lose_sum: 100.14114147424698
time_elpased: 2.351
batch start
#iterations: 3
currently lose_sum: 99.89940029382706
time_elpased: 2.454
batch start
#iterations: 4
currently lose_sum: 99.61699402332306
time_elpased: 2.339
batch start
#iterations: 5
currently lose_sum: 99.51906019449234
time_elpased: 2.345
batch start
#iterations: 6
currently lose_sum: 99.30330449342728
time_elpased: 2.286
batch start
#iterations: 7
currently lose_sum: 99.32106983661652
time_elpased: 2.349
batch start
#iterations: 8
currently lose_sum: 99.1908854842186
time_elpased: 2.216
batch start
#iterations: 9
currently lose_sum: 99.26215499639511
time_elpased: 2.167
batch start
#iterations: 10
currently lose_sum: 98.9559161067009
time_elpased: 2.129
batch start
#iterations: 11
currently lose_sum: 99.1393591761589
time_elpased: 2.32
batch start
#iterations: 12
currently lose_sum: 98.96165752410889
time_elpased: 2.32
batch start
#iterations: 13
currently lose_sum: 99.13910919427872
time_elpased: 2.326
batch start
#iterations: 14
currently lose_sum: 98.73699736595154
time_elpased: 1.924
batch start
#iterations: 15
currently lose_sum: 98.91987329721451
time_elpased: 2.263
batch start
#iterations: 16
currently lose_sum: 98.68627321720123
time_elpased: 2.313
batch start
#iterations: 17
currently lose_sum: 98.80525493621826
time_elpased: 1.957
batch start
#iterations: 18
currently lose_sum: 98.73268008232117
time_elpased: 2.076
batch start
#iterations: 19
currently lose_sum: 98.75859642028809
time_elpased: 2.13
start validation test
0.61881443299
0.622597676874
0.606771637337
0.614582790431
0.618835575975
64.241
batch start
#iterations: 20
currently lose_sum: 98.62417256832123
time_elpased: 2.089
batch start
#iterations: 21
currently lose_sum: 98.4984700679779
time_elpased: 2.224
batch start
#iterations: 22
currently lose_sum: 98.49951821565628
time_elpased: 2.401
batch start
#iterations: 23
currently lose_sum: 98.43472683429718
time_elpased: 2.356
batch start
#iterations: 24
currently lose_sum: 98.5455014705658
time_elpased: 1.863
batch start
#iterations: 25
currently lose_sum: 98.4927265048027
time_elpased: 2.106
batch start
#iterations: 26
currently lose_sum: 98.6708123087883
time_elpased: 1.849
batch start
#iterations: 27
currently lose_sum: 98.53686141967773
time_elpased: 1.987
batch start
#iterations: 28
currently lose_sum: 98.37550961971283
time_elpased: 2.02
batch start
#iterations: 29
currently lose_sum: 98.44403523206711
time_elpased: 1.959
batch start
#iterations: 30
currently lose_sum: 98.59472513198853
time_elpased: 2.147
batch start
#iterations: 31
currently lose_sum: 98.14651119709015
time_elpased: 2.228
batch start
#iterations: 32
currently lose_sum: 98.39611828327179
time_elpased: 1.921
batch start
#iterations: 33
currently lose_sum: 98.3545463681221
time_elpased: 2.449
batch start
#iterations: 34
currently lose_sum: 98.20972609519958
time_elpased: 2.483
batch start
#iterations: 35
currently lose_sum: 98.52787816524506
time_elpased: 2.401
batch start
#iterations: 36
currently lose_sum: 98.5497385263443
time_elpased: 2.373
batch start
#iterations: 37
currently lose_sum: 98.07809299230576
time_elpased: 2.03
batch start
#iterations: 38
currently lose_sum: 98.11543250083923
time_elpased: 2.384
batch start
#iterations: 39
currently lose_sum: 98.21648919582367
time_elpased: 2.198
start validation test
0.621546391753
0.646081928897
0.540496037872
0.588591280959
0.62168868815
64.070
batch start
#iterations: 40
currently lose_sum: 98.30375784635544
time_elpased: 2.268
batch start
#iterations: 41
currently lose_sum: 98.25229948759079
time_elpased: 2.4
batch start
#iterations: 42
currently lose_sum: 98.1146941781044
time_elpased: 2.132
batch start
#iterations: 43
currently lose_sum: 98.1549202799797
time_elpased: 2.289
batch start
#iterations: 44
currently lose_sum: 98.21244049072266
time_elpased: 2.398
batch start
#iterations: 45
currently lose_sum: 98.41667878627777
time_elpased: 2.327
batch start
#iterations: 46
currently lose_sum: 97.93456315994263
time_elpased: 2.335
batch start
#iterations: 47
currently lose_sum: 98.17972892522812
time_elpased: 2.226
batch start
#iterations: 48
currently lose_sum: 98.27183133363724
time_elpased: 2.064
batch start
#iterations: 49
currently lose_sum: 98.33521103858948
time_elpased: 2.17
batch start
#iterations: 50
currently lose_sum: 98.06435054540634
time_elpased: 2.336
batch start
#iterations: 51
currently lose_sum: 97.92931264638901
time_elpased: 2.321
batch start
#iterations: 52
currently lose_sum: 97.88521230220795
time_elpased: 2.465
batch start
#iterations: 53
currently lose_sum: 98.01163256168365
time_elpased: 2.267
batch start
#iterations: 54
currently lose_sum: 98.07920324802399
time_elpased: 2.153
batch start
#iterations: 55
currently lose_sum: 97.9532875418663
time_elpased: 2.165
batch start
#iterations: 56
currently lose_sum: 98.1279610991478
time_elpased: 2.279
batch start
#iterations: 57
currently lose_sum: 98.18586558103561
time_elpased: 2.294
batch start
#iterations: 58
currently lose_sum: 98.06300848722458
time_elpased: 2.213
batch start
#iterations: 59
currently lose_sum: 98.08608800172806
time_elpased: 2.073
start validation test
0.621907216495
0.644223782998
0.547494082536
0.591933240612
0.622037860229
63.967
batch start
#iterations: 60
currently lose_sum: 97.89595049619675
time_elpased: 2.492
batch start
#iterations: 61
currently lose_sum: 98.28533351421356
time_elpased: 2.383
batch start
#iterations: 62
currently lose_sum: 97.96839958429337
time_elpased: 2.427
batch start
#iterations: 63
currently lose_sum: 98.12784779071808
time_elpased: 2.286
batch start
#iterations: 64
currently lose_sum: 98.06949657201767
time_elpased: 2.416
batch start
#iterations: 65
currently lose_sum: 98.06702220439911
time_elpased: 2.223
batch start
#iterations: 66
currently lose_sum: 97.82892459630966
time_elpased: 2.272
batch start
#iterations: 67
currently lose_sum: 97.78650891780853
time_elpased: 2.105
batch start
#iterations: 68
currently lose_sum: 98.12208420038223
time_elpased: 2.401
batch start
#iterations: 69
currently lose_sum: 97.98978066444397
time_elpased: 2.251
batch start
#iterations: 70
currently lose_sum: 97.78690433502197
time_elpased: 2.016
batch start
#iterations: 71
currently lose_sum: 98.03738647699356
time_elpased: 2.136
batch start
#iterations: 72
currently lose_sum: 98.00563591718674
time_elpased: 2.377
batch start
#iterations: 73
currently lose_sum: 97.98427420854568
time_elpased: 2.334
batch start
#iterations: 74
currently lose_sum: 98.14430195093155
time_elpased: 2.3
batch start
#iterations: 75
currently lose_sum: 97.93466174602509
time_elpased: 2.13
batch start
#iterations: 76
currently lose_sum: 97.98104530572891
time_elpased: 2.165
batch start
#iterations: 77
currently lose_sum: 98.1708567738533
time_elpased: 2.084
batch start
#iterations: 78
currently lose_sum: 97.9095162153244
time_elpased: 2.367
batch start
#iterations: 79
currently lose_sum: 98.04874300956726
time_elpased: 2.288
start validation test
0.626597938144
0.633719044014
0.603066790162
0.61801307741
0.626639250704
63.519
batch start
#iterations: 80
currently lose_sum: 98.03739327192307
time_elpased: 2.28
batch start
#iterations: 81
currently lose_sum: 97.77508902549744
time_elpased: 2.251
batch start
#iterations: 82
currently lose_sum: 98.04578483104706
time_elpased: 2.398
batch start
#iterations: 83
currently lose_sum: 97.94080936908722
time_elpased: 2.478
batch start
#iterations: 84
currently lose_sum: 97.80891644954681
time_elpased: 2.253
batch start
#iterations: 85
currently lose_sum: 97.87735652923584
time_elpased: 2.237
batch start
#iterations: 86
currently lose_sum: 97.92967700958252
time_elpased: 2.009
batch start
#iterations: 87
currently lose_sum: 97.66548496484756
time_elpased: 2.269
batch start
#iterations: 88
currently lose_sum: 97.87418258190155
time_elpased: 2.287
batch start
#iterations: 89
currently lose_sum: 97.97773665189743
time_elpased: 2.308
batch start
#iterations: 90
currently lose_sum: 97.8841924071312
time_elpased: 2.374
batch start
#iterations: 91
currently lose_sum: 98.03693842887878
time_elpased: 2.325
batch start
#iterations: 92
currently lose_sum: 97.81723588705063
time_elpased: 2.403
batch start
#iterations: 93
currently lose_sum: 98.0396072268486
time_elpased: 2.128
batch start
#iterations: 94
currently lose_sum: 97.94202363491058
time_elpased: 2.168
batch start
#iterations: 95
currently lose_sum: 97.89607983827591
time_elpased: 2.184
batch start
#iterations: 96
currently lose_sum: 97.72289925813675
time_elpased: 2.069
batch start
#iterations: 97
currently lose_sum: 97.66081720590591
time_elpased: 2.27
batch start
#iterations: 98
currently lose_sum: 97.9651694893837
time_elpased: 2.418
batch start
#iterations: 99
currently lose_sum: 97.72865241765976
time_elpased: 2.276
start validation test
0.627113402062
0.635107193383
0.600596892045
0.617370147043
0.627159955885
63.509
batch start
#iterations: 100
currently lose_sum: 97.77044075727463
time_elpased: 2.468
batch start
#iterations: 101
currently lose_sum: 98.04350751638412
time_elpased: 2.453
batch start
#iterations: 102
currently lose_sum: 98.13708984851837
time_elpased: 2.128
batch start
#iterations: 103
currently lose_sum: 97.63088971376419
time_elpased: 2.18
batch start
#iterations: 104
currently lose_sum: 97.86506050825119
time_elpased: 2.158
batch start
#iterations: 105
currently lose_sum: 97.63547396659851
time_elpased: 2.169
batch start
#iterations: 106
currently lose_sum: 97.71677559614182
time_elpased: 2.172
batch start
#iterations: 107
currently lose_sum: 97.87474370002747
time_elpased: 2.487
batch start
#iterations: 108
currently lose_sum: 97.91936010122299
time_elpased: 2.091
batch start
#iterations: 109
currently lose_sum: 97.73988664150238
time_elpased: 2.075
batch start
#iterations: 110
currently lose_sum: 97.94616943597794
time_elpased: 2.046
batch start
#iterations: 111
currently lose_sum: 97.45932221412659
time_elpased: 1.748
batch start
#iterations: 112
currently lose_sum: 97.70853286981583
time_elpased: 1.54
batch start
#iterations: 113
currently lose_sum: 97.51028215885162
time_elpased: 2.085
batch start
#iterations: 114
currently lose_sum: 97.86534178256989
time_elpased: 2.0
batch start
#iterations: 115
currently lose_sum: 97.53168100118637
time_elpased: 2.327
batch start
#iterations: 116
currently lose_sum: 97.87866425514221
time_elpased: 2.303
batch start
#iterations: 117
currently lose_sum: 97.37028473615646
time_elpased: 2.405
batch start
#iterations: 118
currently lose_sum: 97.80461001396179
time_elpased: 2.426
batch start
#iterations: 119
currently lose_sum: 97.596952855587
time_elpased: 2.371
start validation test
0.624536082474
0.633637262441
0.593598847381
0.612964930925
0.624590397562
63.461
batch start
#iterations: 120
currently lose_sum: 97.71731853485107
time_elpased: 2.004
batch start
#iterations: 121
currently lose_sum: 97.74111777544022
time_elpased: 2.016
batch start
#iterations: 122
currently lose_sum: 97.9457055926323
time_elpased: 1.663
batch start
#iterations: 123
currently lose_sum: 97.69194436073303
time_elpased: 1.883
batch start
#iterations: 124
currently lose_sum: 97.70266354084015
time_elpased: 2.251
batch start
#iterations: 125
currently lose_sum: 97.97077268362045
time_elpased: 2.408
batch start
#iterations: 126
currently lose_sum: 97.56733548641205
time_elpased: 1.985
batch start
#iterations: 127
currently lose_sum: 98.00326079130173
time_elpased: 1.962
batch start
#iterations: 128
currently lose_sum: 97.54675936698914
time_elpased: 2.315
batch start
#iterations: 129
currently lose_sum: 97.76024186611176
time_elpased: 2.376
batch start
#iterations: 130
currently lose_sum: 97.59935355186462
time_elpased: 2.338
batch start
#iterations: 131
currently lose_sum: 97.94268041849136
time_elpased: 2.313
batch start
#iterations: 132
currently lose_sum: 97.92294430732727
time_elpased: 2.281
batch start
#iterations: 133
currently lose_sum: 97.73219031095505
time_elpased: 2.128
batch start
#iterations: 134
currently lose_sum: 97.63079279661179
time_elpased: 2.156
batch start
#iterations: 135
currently lose_sum: 97.63836419582367
time_elpased: 2.245
batch start
#iterations: 136
currently lose_sum: 97.5218425989151
time_elpased: 2.371
batch start
#iterations: 137
currently lose_sum: 97.73688441514969
time_elpased: 2.065
batch start
#iterations: 138
currently lose_sum: 97.69942498207092
time_elpased: 2.266
batch start
#iterations: 139
currently lose_sum: 97.53031557798386
time_elpased: 2.305
start validation test
0.629793814433
0.639731010914
0.597200782134
0.617734724292
0.629851036528
63.367
batch start
#iterations: 140
currently lose_sum: 97.50464504957199
time_elpased: 2.209
batch start
#iterations: 141
currently lose_sum: 97.90416640043259
time_elpased: 2.234
batch start
#iterations: 142
currently lose_sum: 97.84159749746323
time_elpased: 2.352
batch start
#iterations: 143
currently lose_sum: 97.58468753099442
time_elpased: 2.413
batch start
#iterations: 144
currently lose_sum: 97.75163716077805
time_elpased: 2.402
batch start
#iterations: 145
currently lose_sum: 97.81983983516693
time_elpased: 2.187
batch start
#iterations: 146
currently lose_sum: 97.54554915428162
time_elpased: 2.306
batch start
#iterations: 147
currently lose_sum: 97.540114402771
time_elpased: 2.432
batch start
#iterations: 148
currently lose_sum: 97.58289843797684
time_elpased: 2.203
batch start
#iterations: 149
currently lose_sum: 97.56631726026535
time_elpased: 2.297
batch start
#iterations: 150
currently lose_sum: 97.57844686508179
time_elpased: 2.165
batch start
#iterations: 151
currently lose_sum: 97.64971387386322
time_elpased: 2.18
batch start
#iterations: 152
currently lose_sum: 97.36183422803879
time_elpased: 2.043
batch start
#iterations: 153
currently lose_sum: 97.70946580171585
time_elpased: 1.868
batch start
#iterations: 154
currently lose_sum: 97.88916951417923
time_elpased: 1.642
batch start
#iterations: 155
currently lose_sum: 97.51969265937805
time_elpased: 1.617
batch start
#iterations: 156
currently lose_sum: 97.59178400039673
time_elpased: 1.848
batch start
#iterations: 157
currently lose_sum: 97.37203413248062
time_elpased: 2.245
batch start
#iterations: 158
currently lose_sum: 97.74670392274857
time_elpased: 2.202
batch start
#iterations: 159
currently lose_sum: 97.63171350955963
time_elpased: 2.25
start validation test
0.622525773196
0.644880174292
0.548317381908
0.592691473386
0.622656057473
63.603
batch start
#iterations: 160
currently lose_sum: 97.34233993291855
time_elpased: 2.238
batch start
#iterations: 161
currently lose_sum: 97.70699656009674
time_elpased: 2.328
batch start
#iterations: 162
currently lose_sum: 97.83787679672241
time_elpased: 2.094
batch start
#iterations: 163
currently lose_sum: 97.89728260040283
time_elpased: 2.223
batch start
#iterations: 164
currently lose_sum: 97.41148573160172
time_elpased: 1.97
batch start
#iterations: 165
currently lose_sum: 97.62947529554367
time_elpased: 2.212
batch start
#iterations: 166
currently lose_sum: 97.54290163516998
time_elpased: 2.363
batch start
#iterations: 167
currently lose_sum: 97.4857092499733
time_elpased: 2.128
batch start
#iterations: 168
currently lose_sum: 97.50335371494293
time_elpased: 1.71
batch start
#iterations: 169
currently lose_sum: 97.56913489103317
time_elpased: 1.714
batch start
#iterations: 170
currently lose_sum: 97.82579672336578
time_elpased: 1.857
batch start
#iterations: 171
currently lose_sum: 97.74121314287186
time_elpased: 2.31
batch start
#iterations: 172
currently lose_sum: 97.713543176651
time_elpased: 2.392
batch start
#iterations: 173
currently lose_sum: 97.91826486587524
time_elpased: 2.521
batch start
#iterations: 174
currently lose_sum: 97.38089525699615
time_elpased: 2.208
batch start
#iterations: 175
currently lose_sum: 97.35880994796753
time_elpased: 2.234
batch start
#iterations: 176
currently lose_sum: 97.46525806188583
time_elpased: 2.45
batch start
#iterations: 177
currently lose_sum: 97.49667966365814
time_elpased: 2.278
batch start
#iterations: 178
currently lose_sum: 97.7399572134018
time_elpased: 2.352
batch start
#iterations: 179
currently lose_sum: 97.68402081727982
time_elpased: 1.747
start validation test
0.627371134021
0.627068437181
0.631779355768
0.629415081766
0.627363394707
63.421
batch start
#iterations: 180
currently lose_sum: 97.58116430044174
time_elpased: 1.824
batch start
#iterations: 181
currently lose_sum: 97.78666162490845
time_elpased: 1.986
batch start
#iterations: 182
currently lose_sum: 97.44387078285217
time_elpased: 2.041
batch start
#iterations: 183
currently lose_sum: 97.68425583839417
time_elpased: 2.235
batch start
#iterations: 184
currently lose_sum: 97.59091532230377
time_elpased: 2.313
batch start
#iterations: 185
currently lose_sum: 97.62139630317688
time_elpased: 1.955
batch start
#iterations: 186
currently lose_sum: 97.66463613510132
time_elpased: 2.518
batch start
#iterations: 187
currently lose_sum: 97.36286187171936
time_elpased: 2.515
batch start
#iterations: 188
currently lose_sum: 97.92507541179657
time_elpased: 2.44
batch start
#iterations: 189
currently lose_sum: 97.56583487987518
time_elpased: 2.294
batch start
#iterations: 190
currently lose_sum: 97.61228960752487
time_elpased: 2.076
batch start
#iterations: 191
currently lose_sum: 97.39957767724991
time_elpased: 2.524
batch start
#iterations: 192
currently lose_sum: 97.38247156143188
time_elpased: 2.463
batch start
#iterations: 193
currently lose_sum: 97.59589719772339
time_elpased: 2.142
batch start
#iterations: 194
currently lose_sum: 97.51106148958206
time_elpased: 2.471
batch start
#iterations: 195
currently lose_sum: 97.44485431909561
time_elpased: 2.482
batch start
#iterations: 196
currently lose_sum: 97.43414288759232
time_elpased: 2.101
batch start
#iterations: 197
currently lose_sum: 97.7815672159195
time_elpased: 2.008
batch start
#iterations: 198
currently lose_sum: 97.57448959350586
time_elpased: 2.303
batch start
#iterations: 199
currently lose_sum: 97.3436558842659
time_elpased: 2.126
start validation test
0.628402061856
0.633205863607
0.613460944736
0.623177042496
0.628428293291
63.349
batch start
#iterations: 200
currently lose_sum: 97.42308747768402
time_elpased: 2.426
batch start
#iterations: 201
currently lose_sum: 97.63987046480179
time_elpased: 2.317
batch start
#iterations: 202
currently lose_sum: 97.29811590909958
time_elpased: 2.276
batch start
#iterations: 203
currently lose_sum: 97.13023394346237
time_elpased: 2.29
batch start
#iterations: 204
currently lose_sum: 97.4552276134491
time_elpased: 2.274
batch start
#iterations: 205
currently lose_sum: 97.26011556386948
time_elpased: 2.489
batch start
#iterations: 206
currently lose_sum: 97.40676683187485
time_elpased: 1.913
batch start
#iterations: 207
currently lose_sum: 97.44738644361496
time_elpased: 2.438
batch start
#iterations: 208
currently lose_sum: 97.66546767950058
time_elpased: 2.295
batch start
#iterations: 209
currently lose_sum: 97.42498111724854
time_elpased: 2.266
batch start
#iterations: 210
currently lose_sum: 97.30021595954895
time_elpased: 2.33
batch start
#iterations: 211
currently lose_sum: 97.46603667736053
time_elpased: 2.391
batch start
#iterations: 212
currently lose_sum: 97.60858380794525
time_elpased: 2.162
batch start
#iterations: 213
currently lose_sum: 97.31584084033966
time_elpased: 2.391
batch start
#iterations: 214
currently lose_sum: 97.6195176243782
time_elpased: 2.218
batch start
#iterations: 215
currently lose_sum: 97.54925513267517
time_elpased: 2.271
batch start
#iterations: 216
currently lose_sum: 97.39206892251968
time_elpased: 2.327
batch start
#iterations: 217
currently lose_sum: 97.39386212825775
time_elpased: 2.293
batch start
#iterations: 218
currently lose_sum: 97.30431634187698
time_elpased: 2.314
batch start
#iterations: 219
currently lose_sum: 97.39313143491745
time_elpased: 2.235
start validation test
0.620206185567
0.651607073706
0.51950190388
0.578103527256
0.620382987466
63.821
batch start
#iterations: 220
currently lose_sum: 97.53379744291306
time_elpased: 2.363
batch start
#iterations: 221
currently lose_sum: 97.35484737157822
time_elpased: 2.213
batch start
#iterations: 222
currently lose_sum: 97.49197101593018
time_elpased: 2.374
batch start
#iterations: 223
currently lose_sum: 97.47852820158005
time_elpased: 2.236
batch start
#iterations: 224
currently lose_sum: 97.4436047077179
time_elpased: 2.166
batch start
#iterations: 225
currently lose_sum: 97.45472502708435
time_elpased: 2.325
batch start
#iterations: 226
currently lose_sum: 97.4772961139679
time_elpased: 2.379
batch start
#iterations: 227
currently lose_sum: 97.50299662351608
time_elpased: 2.291
batch start
#iterations: 228
currently lose_sum: 97.5236120223999
time_elpased: 2.314
batch start
#iterations: 229
currently lose_sum: 97.51921260356903
time_elpased: 2.142
batch start
#iterations: 230
currently lose_sum: 97.39801967144012
time_elpased: 2.324
batch start
#iterations: 231
currently lose_sum: 97.34265822172165
time_elpased: 2.398
batch start
#iterations: 232
currently lose_sum: 97.41231852769852
time_elpased: 2.211
batch start
#iterations: 233
currently lose_sum: 97.55601096153259
time_elpased: 2.237
batch start
#iterations: 234
currently lose_sum: 97.49793606996536
time_elpased: 2.378
batch start
#iterations: 235
currently lose_sum: 97.38846606016159
time_elpased: 2.326
batch start
#iterations: 236
currently lose_sum: 97.41281300783157
time_elpased: 2.397
batch start
#iterations: 237
currently lose_sum: 97.79030525684357
time_elpased: 2.317
batch start
#iterations: 238
currently lose_sum: 97.37014931440353
time_elpased: 2.213
batch start
#iterations: 239
currently lose_sum: 97.50537574291229
time_elpased: 2.339
start validation test
0.629020618557
0.635425623388
0.608418236081
0.621628726145
0.629056789217
63.440
batch start
#iterations: 240
currently lose_sum: 97.12852030992508
time_elpased: 1.973
batch start
#iterations: 241
currently lose_sum: 97.21937078237534
time_elpased: 2.459
batch start
#iterations: 242
currently lose_sum: 97.42324244976044
time_elpased: 2.353
batch start
#iterations: 243
currently lose_sum: 97.41486489772797
time_elpased: 2.354
batch start
#iterations: 244
currently lose_sum: 97.5113742351532
time_elpased: 2.247
batch start
#iterations: 245
currently lose_sum: 97.60961675643921
time_elpased: 2.61
batch start
#iterations: 246
currently lose_sum: 97.35841196775436
time_elpased: 2.359
batch start
#iterations: 247
currently lose_sum: 97.32420670986176
time_elpased: 2.429
batch start
#iterations: 248
currently lose_sum: 97.39247953891754
time_elpased: 2.336
batch start
#iterations: 249
currently lose_sum: 97.2497593164444
time_elpased: 2.375
batch start
#iterations: 250
currently lose_sum: 97.56067806482315
time_elpased: 2.347
batch start
#iterations: 251
currently lose_sum: 97.22905349731445
time_elpased: 2.383
batch start
#iterations: 252
currently lose_sum: 97.06153631210327
time_elpased: 2.169
batch start
#iterations: 253
currently lose_sum: 97.48785382509232
time_elpased: 2.381
batch start
#iterations: 254
currently lose_sum: 97.13294118642807
time_elpased: 2.145
batch start
#iterations: 255
currently lose_sum: 97.71249282360077
time_elpased: 2.191
batch start
#iterations: 256
currently lose_sum: 97.64336133003235
time_elpased: 2.381
batch start
#iterations: 257
currently lose_sum: 97.25216031074524
time_elpased: 2.296
batch start
#iterations: 258
currently lose_sum: 97.33294403553009
time_elpased: 2.322
batch start
#iterations: 259
currently lose_sum: 97.28610450029373
time_elpased: 2.294
start validation test
0.627319587629
0.639734801663
0.585880415766
0.61162440911
0.627392340487
63.374
batch start
#iterations: 260
currently lose_sum: 97.33089989423752
time_elpased: 2.252
batch start
#iterations: 261
currently lose_sum: 97.25616818666458
time_elpased: 2.374
batch start
#iterations: 262
currently lose_sum: 97.08308869600296
time_elpased: 2.275
batch start
#iterations: 263
currently lose_sum: 97.40943336486816
time_elpased: 2.268
batch start
#iterations: 264
currently lose_sum: 97.24643325805664
time_elpased: 2.391
batch start
#iterations: 265
currently lose_sum: 97.30187076330185
time_elpased: 2.339
batch start
#iterations: 266
currently lose_sum: 97.32995092868805
time_elpased: 2.338
batch start
#iterations: 267
currently lose_sum: 97.08094018697739
time_elpased: 2.324
batch start
#iterations: 268
currently lose_sum: 97.37126088142395
time_elpased: 2.279
batch start
#iterations: 269
currently lose_sum: 97.52978849411011
time_elpased: 2.338
batch start
#iterations: 270
currently lose_sum: 97.31675094366074
time_elpased: 2.266
batch start
#iterations: 271
currently lose_sum: 97.36843818426132
time_elpased: 2.117
batch start
#iterations: 272
currently lose_sum: 97.29813665151596
time_elpased: 2.076
batch start
#iterations: 273
currently lose_sum: 97.27831798791885
time_elpased: 2.338
batch start
#iterations: 274
currently lose_sum: 97.16117042303085
time_elpased: 2.373
batch start
#iterations: 275
currently lose_sum: 97.71783763170242
time_elpased: 2.139
batch start
#iterations: 276
currently lose_sum: 97.27770209312439
time_elpased: 2.336
batch start
#iterations: 277
currently lose_sum: 97.45364916324615
time_elpased: 2.318
batch start
#iterations: 278
currently lose_sum: 97.12356871366501
time_elpased: 2.335
batch start
#iterations: 279
currently lose_sum: 97.51269716024399
time_elpased: 2.247
start validation test
0.625
0.640830449827
0.571781414017
0.604340022842
0.625093433436
63.486
batch start
#iterations: 280
currently lose_sum: 97.39567613601685
time_elpased: 2.273
batch start
#iterations: 281
currently lose_sum: 97.4841822385788
time_elpased: 2.046
batch start
#iterations: 282
currently lose_sum: 97.32749032974243
time_elpased: 2.337
batch start
#iterations: 283
currently lose_sum: 97.36644089221954
time_elpased: 2.408
batch start
#iterations: 284
currently lose_sum: 97.16733574867249
time_elpased: 2.548
batch start
#iterations: 285
currently lose_sum: 97.21403169631958
time_elpased: 2.293
batch start
#iterations: 286
currently lose_sum: 97.60725963115692
time_elpased: 2.199
batch start
#iterations: 287
currently lose_sum: 97.19085830450058
time_elpased: 2.372
batch start
#iterations: 288
currently lose_sum: 97.3973200917244
time_elpased: 2.008
batch start
#iterations: 289
currently lose_sum: 97.41506904363632
time_elpased: 2.096
batch start
#iterations: 290
currently lose_sum: 97.00327342748642
time_elpased: 2.142
batch start
#iterations: 291
currently lose_sum: 97.58414036035538
time_elpased: 2.003
batch start
#iterations: 292
currently lose_sum: 97.52255779504776
time_elpased: 2.345
batch start
#iterations: 293
currently lose_sum: 97.00740498304367
time_elpased: 2.203
batch start
#iterations: 294
currently lose_sum: 97.40142357349396
time_elpased: 2.365
batch start
#iterations: 295
currently lose_sum: 97.4406366944313
time_elpased: 2.373
batch start
#iterations: 296
currently lose_sum: 97.39339393377304
time_elpased: 2.314
batch start
#iterations: 297
currently lose_sum: 97.220243871212
time_elpased: 1.634
batch start
#iterations: 298
currently lose_sum: 97.32475173473358
time_elpased: 2.066
batch start
#iterations: 299
currently lose_sum: 97.29715305566788
time_elpased: 2.232
start validation test
0.628453608247
0.63522690525
0.606462900072
0.62051174055
0.628492216327
63.383
batch start
#iterations: 300
currently lose_sum: 97.45308828353882
time_elpased: 2.374
batch start
#iterations: 301
currently lose_sum: 97.39387035369873
time_elpased: 2.252
batch start
#iterations: 302
currently lose_sum: 97.22411668300629
time_elpased: 1.988
batch start
#iterations: 303
currently lose_sum: 97.21112048625946
time_elpased: 2.211
batch start
#iterations: 304
currently lose_sum: 97.17684429883957
time_elpased: 2.121
batch start
#iterations: 305
currently lose_sum: 97.40759181976318
time_elpased: 2.218
batch start
#iterations: 306
currently lose_sum: 97.35312712192535
time_elpased: 2.303
batch start
#iterations: 307
currently lose_sum: 97.25914400815964
time_elpased: 2.318
batch start
#iterations: 308
currently lose_sum: 97.34310442209244
time_elpased: 2.36
batch start
#iterations: 309
currently lose_sum: 96.8670956492424
time_elpased: 2.126
batch start
#iterations: 310
currently lose_sum: 97.45778208971024
time_elpased: 2.122
batch start
#iterations: 311
currently lose_sum: 97.38310474157333
time_elpased: 2.115
batch start
#iterations: 312
currently lose_sum: 97.21561747789383
time_elpased: 2.212
batch start
#iterations: 313
currently lose_sum: 97.19480377435684
time_elpased: 2.448
batch start
#iterations: 314
currently lose_sum: 97.33982127904892
time_elpased: 2.311
batch start
#iterations: 315
currently lose_sum: 97.17641472816467
time_elpased: 2.368
batch start
#iterations: 316
currently lose_sum: 97.05288881063461
time_elpased: 2.412
batch start
#iterations: 317
currently lose_sum: 97.36287438869476
time_elpased: 2.376
batch start
#iterations: 318
currently lose_sum: 97.41849493980408
time_elpased: 2.111
batch start
#iterations: 319
currently lose_sum: 97.30477488040924
time_elpased: 2.191
start validation test
0.627474226804
0.640234287002
0.584954203972
0.611347136327
0.627548877262
63.559
batch start
#iterations: 320
currently lose_sum: 97.1436437368393
time_elpased: 2.406
batch start
#iterations: 321
currently lose_sum: 97.29582983255386
time_elpased: 2.313
batch start
#iterations: 322
currently lose_sum: 97.45479106903076
time_elpased: 2.354
batch start
#iterations: 323
currently lose_sum: 97.16785246133804
time_elpased: 2.3
batch start
#iterations: 324
currently lose_sum: 97.40515023469925
time_elpased: 2.373
batch start
#iterations: 325
currently lose_sum: 97.42698884010315
time_elpased: 2.329
batch start
#iterations: 326
currently lose_sum: 97.28712910413742
time_elpased: 2.06
batch start
#iterations: 327
currently lose_sum: 97.3366534113884
time_elpased: 2.027
batch start
#iterations: 328
currently lose_sum: 97.25333815813065
time_elpased: 2.054
batch start
#iterations: 329
currently lose_sum: 97.358751475811
time_elpased: 1.897
batch start
#iterations: 330
currently lose_sum: 96.8489585518837
time_elpased: 1.519
batch start
#iterations: 331
currently lose_sum: 97.46771639585495
time_elpased: 1.669
batch start
#iterations: 332
currently lose_sum: 97.2861402630806
time_elpased: 1.884
batch start
#iterations: 333
currently lose_sum: 97.20554667711258
time_elpased: 1.746
batch start
#iterations: 334
currently lose_sum: 97.19325882196426
time_elpased: 1.994
batch start
#iterations: 335
currently lose_sum: 96.98043090105057
time_elpased: 2.076
batch start
#iterations: 336
currently lose_sum: 97.18258315324783
time_elpased: 1.926
batch start
#iterations: 337
currently lose_sum: 97.4593255519867
time_elpased: 2.076
batch start
#iterations: 338
currently lose_sum: 97.32849168777466
time_elpased: 1.843
batch start
#iterations: 339
currently lose_sum: 97.45867270231247
time_elpased: 1.792
start validation test
0.630051546392
0.63539445629
0.613358032315
0.624181808661
0.630080854431
63.367
batch start
#iterations: 340
currently lose_sum: 97.28419315814972
time_elpased: 1.69
batch start
#iterations: 341
currently lose_sum: 97.5019902586937
time_elpased: 1.66
batch start
#iterations: 342
currently lose_sum: 97.27077031135559
time_elpased: 1.708
batch start
#iterations: 343
currently lose_sum: 97.54624086618423
time_elpased: 1.612
batch start
#iterations: 344
currently lose_sum: 96.95771211385727
time_elpased: 1.7
batch start
#iterations: 345
currently lose_sum: 97.30473244190216
time_elpased: 1.645
batch start
#iterations: 346
currently lose_sum: 97.19831186532974
time_elpased: 1.589
batch start
#iterations: 347
currently lose_sum: 97.23050266504288
time_elpased: 1.599
batch start
#iterations: 348
currently lose_sum: 97.5604110956192
time_elpased: 1.574
batch start
#iterations: 349
currently lose_sum: 97.4850063920021
time_elpased: 1.488
batch start
#iterations: 350
currently lose_sum: 97.54351824522018
time_elpased: 1.497
batch start
#iterations: 351
currently lose_sum: 97.11634868383408
time_elpased: 1.466
batch start
#iterations: 352
currently lose_sum: 97.49482703208923
time_elpased: 1.492
batch start
#iterations: 353
currently lose_sum: 97.32100415229797
time_elpased: 1.456
batch start
#iterations: 354
currently lose_sum: 97.49515408277512
time_elpased: 1.472
batch start
#iterations: 355
currently lose_sum: 97.28854632377625
time_elpased: 1.409
batch start
#iterations: 356
currently lose_sum: 97.25324153900146
time_elpased: 1.634
batch start
#iterations: 357
currently lose_sum: 97.43748354911804
time_elpased: 1.853
batch start
#iterations: 358
currently lose_sum: 97.31710314750671
time_elpased: 1.966
batch start
#iterations: 359
currently lose_sum: 97.19469672441483
time_elpased: 2.521
start validation test
0.625618556701
0.640646492435
0.575177523927
0.606149341142
0.625707113714
63.438
batch start
#iterations: 360
currently lose_sum: 97.35719168186188
time_elpased: 2.217
batch start
#iterations: 361
currently lose_sum: 97.43298929929733
time_elpased: 2.156
batch start
#iterations: 362
currently lose_sum: 97.38384288549423
time_elpased: 2.182
batch start
#iterations: 363
currently lose_sum: 96.88790571689606
time_elpased: 2.134
batch start
#iterations: 364
currently lose_sum: 97.16096287965775
time_elpased: 2.149
batch start
#iterations: 365
currently lose_sum: 97.38762950897217
time_elpased: 1.949
batch start
#iterations: 366
currently lose_sum: 97.39553385972977
time_elpased: 1.983
batch start
#iterations: 367
currently lose_sum: 97.23203760385513
time_elpased: 2.341
batch start
#iterations: 368
currently lose_sum: 97.31062889099121
time_elpased: 2.483
batch start
#iterations: 369
currently lose_sum: 97.30087614059448
time_elpased: 2.452
batch start
#iterations: 370
currently lose_sum: 97.33943212032318
time_elpased: 2.211
batch start
#iterations: 371
currently lose_sum: 97.2477075457573
time_elpased: 2.748
batch start
#iterations: 372
currently lose_sum: 97.16640424728394
time_elpased: 2.227
batch start
#iterations: 373
currently lose_sum: 97.04309779405594
time_elpased: 2.225
batch start
#iterations: 374
currently lose_sum: 97.4425710439682
time_elpased: 2.433
batch start
#iterations: 375
currently lose_sum: 97.0998764038086
time_elpased: 2.356
batch start
#iterations: 376
currently lose_sum: 97.05287444591522
time_elpased: 2.188
batch start
#iterations: 377
currently lose_sum: 97.44725877046585
time_elpased: 2.027
batch start
#iterations: 378
currently lose_sum: 97.22694033384323
time_elpased: 2.265
batch start
#iterations: 379
currently lose_sum: 97.39675796031952
time_elpased: 2.164
start validation test
0.630412371134
0.631900569653
0.62786868375
0.629878174685
0.63041683697
63.329
batch start
#iterations: 380
currently lose_sum: 97.34475237131119
time_elpased: 2.106
batch start
#iterations: 381
currently lose_sum: 97.29712361097336
time_elpased: 2.426
batch start
#iterations: 382
currently lose_sum: 97.45453709363937
time_elpased: 2.623
batch start
#iterations: 383
currently lose_sum: 97.3148182630539
time_elpased: 2.066
batch start
#iterations: 384
currently lose_sum: 97.18461853265762
time_elpased: 2.222
batch start
#iterations: 385
currently lose_sum: 97.35720348358154
time_elpased: 2.218
batch start
#iterations: 386
currently lose_sum: 97.3618381023407
time_elpased: 2.104
batch start
#iterations: 387
currently lose_sum: 97.55870372056961
time_elpased: 2.15
batch start
#iterations: 388
currently lose_sum: 97.49410617351532
time_elpased: 2.333
batch start
#iterations: 389
currently lose_sum: 97.40388226509094
time_elpased: 2.101
batch start
#iterations: 390
currently lose_sum: 97.55246752500534
time_elpased: 2.34
batch start
#iterations: 391
currently lose_sum: 97.23787349462509
time_elpased: 2.387
batch start
#iterations: 392
currently lose_sum: 97.22068744897842
time_elpased: 2.316
batch start
#iterations: 393
currently lose_sum: 97.26881808042526
time_elpased: 2.513
batch start
#iterations: 394
currently lose_sum: 97.16380232572556
time_elpased: 2.488
batch start
#iterations: 395
currently lose_sum: 97.29944133758545
time_elpased: 2.409
batch start
#iterations: 396
currently lose_sum: 96.99208474159241
time_elpased: 2.419
batch start
#iterations: 397
currently lose_sum: 97.30107700824738
time_elpased: 2.303
batch start
#iterations: 398
currently lose_sum: 97.16014069318771
time_elpased: 2.444
batch start
#iterations: 399
currently lose_sum: 97.35085141658783
time_elpased: 2.515
start validation test
0.628041237113
0.64071115112
0.585983328188
0.612126424425
0.62811507626
63.437
acc: 0.625
pre: 0.628
rec: 0.616
F1: 0.622
auc: 0.661
