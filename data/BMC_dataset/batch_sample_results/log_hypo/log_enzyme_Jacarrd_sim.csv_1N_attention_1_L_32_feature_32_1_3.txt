start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.85994738340378
time_elpased: 2.376
batch start
#iterations: 1
currently lose_sum: 100.33820474147797
time_elpased: 2.243
batch start
#iterations: 2
currently lose_sum: 100.11272209882736
time_elpased: 2.404
batch start
#iterations: 3
currently lose_sum: 99.97984355688095
time_elpased: 2.239
batch start
#iterations: 4
currently lose_sum: 99.79815584421158
time_elpased: 2.454
batch start
#iterations: 5
currently lose_sum: 99.60343718528748
time_elpased: 2.28
batch start
#iterations: 6
currently lose_sum: 99.56057643890381
time_elpased: 2.378
batch start
#iterations: 7
currently lose_sum: 99.40034830570221
time_elpased: 2.352
batch start
#iterations: 8
currently lose_sum: 99.3214185833931
time_elpased: 2.261
batch start
#iterations: 9
currently lose_sum: 99.28280305862427
time_elpased: 2.211
batch start
#iterations: 10
currently lose_sum: 99.14181470870972
time_elpased: 2.432
batch start
#iterations: 11
currently lose_sum: 99.08022254705429
time_elpased: 2.295
batch start
#iterations: 12
currently lose_sum: 98.6811261177063
time_elpased: 2.231
batch start
#iterations: 13
currently lose_sum: 99.10604202747345
time_elpased: 2.226
batch start
#iterations: 14
currently lose_sum: 98.83709442615509
time_elpased: 2.26
batch start
#iterations: 15
currently lose_sum: 98.88591772317886
time_elpased: 2.334
batch start
#iterations: 16
currently lose_sum: 98.96662402153015
time_elpased: 2.456
batch start
#iterations: 17
currently lose_sum: 99.08703911304474
time_elpased: 2.433
batch start
#iterations: 18
currently lose_sum: 98.92779767513275
time_elpased: 2.481
batch start
#iterations: 19
currently lose_sum: 98.68299382925034
time_elpased: 2.334
start validation test
0.613608247423
0.632268827455
0.546109510086
0.586039319638
0.6137197695
64.509
batch start
#iterations: 20
currently lose_sum: 98.6409243941307
time_elpased: 2.3
batch start
#iterations: 21
currently lose_sum: 98.52841681241989
time_elpased: 2.346
batch start
#iterations: 22
currently lose_sum: 98.40660721063614
time_elpased: 1.824
batch start
#iterations: 23
currently lose_sum: 98.65321922302246
time_elpased: 2.316
batch start
#iterations: 24
currently lose_sum: 98.39190465211868
time_elpased: 2.292
batch start
#iterations: 25
currently lose_sum: 98.57837104797363
time_elpased: 2.244
batch start
#iterations: 26
currently lose_sum: 98.51700329780579
time_elpased: 2.276
batch start
#iterations: 27
currently lose_sum: 98.53917294740677
time_elpased: 2.155
batch start
#iterations: 28
currently lose_sum: 98.790511906147
time_elpased: 2.339
batch start
#iterations: 29
currently lose_sum: 98.56824457645416
time_elpased: 2.274
batch start
#iterations: 30
currently lose_sum: 98.42303413152695
time_elpased: 2.082
batch start
#iterations: 31
currently lose_sum: 98.40320152044296
time_elpased: 2.205
batch start
#iterations: 32
currently lose_sum: 98.24490934610367
time_elpased: 1.761
batch start
#iterations: 33
currently lose_sum: 98.27969169616699
time_elpased: 2.235
batch start
#iterations: 34
currently lose_sum: 98.33925694227219
time_elpased: 2.066
batch start
#iterations: 35
currently lose_sum: 98.59461051225662
time_elpased: 2.233
batch start
#iterations: 36
currently lose_sum: 98.41865342855453
time_elpased: 2.008
batch start
#iterations: 37
currently lose_sum: 98.30574494600296
time_elpased: 2.09
batch start
#iterations: 38
currently lose_sum: 98.42581140995026
time_elpased: 2.334
batch start
#iterations: 39
currently lose_sum: 98.33878594636917
time_elpased: 2.384
start validation test
0.617319587629
0.639858433
0.539625360231
0.585482970408
0.617447954795
64.224
batch start
#iterations: 40
currently lose_sum: 98.3491280078888
time_elpased: 2.369
batch start
#iterations: 41
currently lose_sum: 98.24584001302719
time_elpased: 2.202
batch start
#iterations: 42
currently lose_sum: 98.23238307237625
time_elpased: 2.26
batch start
#iterations: 43
currently lose_sum: 98.22327411174774
time_elpased: 2.366
batch start
#iterations: 44
currently lose_sum: 98.28420919179916
time_elpased: 2.127
batch start
#iterations: 45
currently lose_sum: 98.30032294988632
time_elpased: 2.303
batch start
#iterations: 46
currently lose_sum: 98.17471075057983
time_elpased: 2.374
batch start
#iterations: 47
currently lose_sum: 98.04054659605026
time_elpased: 2.044
batch start
#iterations: 48
currently lose_sum: 98.32079535722733
time_elpased: 2.157
batch start
#iterations: 49
currently lose_sum: 97.97405636310577
time_elpased: 2.407
batch start
#iterations: 50
currently lose_sum: 98.24579387903214
time_elpased: 2.415
batch start
#iterations: 51
currently lose_sum: 98.28434592485428
time_elpased: 2.093
batch start
#iterations: 52
currently lose_sum: 98.03080838918686
time_elpased: 2.196
batch start
#iterations: 53
currently lose_sum: 98.21729719638824
time_elpased: 2.346
batch start
#iterations: 54
currently lose_sum: 98.13749170303345
time_elpased: 2.242
batch start
#iterations: 55
currently lose_sum: 98.02540576457977
time_elpased: 1.729
batch start
#iterations: 56
currently lose_sum: 98.25400012731552
time_elpased: 1.812
batch start
#iterations: 57
currently lose_sum: 97.92037272453308
time_elpased: 1.777
batch start
#iterations: 58
currently lose_sum: 98.00001275539398
time_elpased: 1.991
batch start
#iterations: 59
currently lose_sum: 98.30247443914413
time_elpased: 1.873
start validation test
0.623298969072
0.646757679181
0.546109510086
0.5921875
0.623426502255
63.980
batch start
#iterations: 60
currently lose_sum: 98.05715149641037
time_elpased: 1.879
batch start
#iterations: 61
currently lose_sum: 98.23812246322632
time_elpased: 1.97
batch start
#iterations: 62
currently lose_sum: 98.29457253217697
time_elpased: 2.003
batch start
#iterations: 63
currently lose_sum: 98.1883031129837
time_elpased: 1.9
batch start
#iterations: 64
currently lose_sum: 98.14396893978119
time_elpased: 2.076
batch start
#iterations: 65
currently lose_sum: 98.15266448259354
time_elpased: 2.258
batch start
#iterations: 66
currently lose_sum: 98.06194627285004
time_elpased: 1.946
batch start
#iterations: 67
currently lose_sum: 98.08222168684006
time_elpased: 2.38
batch start
#iterations: 68
currently lose_sum: 97.86073297262192
time_elpased: 2.447
batch start
#iterations: 69
currently lose_sum: 97.99459517002106
time_elpased: 2.366
batch start
#iterations: 70
currently lose_sum: 98.08515429496765
time_elpased: 1.989
batch start
#iterations: 71
currently lose_sum: 97.96736872196198
time_elpased: 2.262
batch start
#iterations: 72
currently lose_sum: 98.08968818187714
time_elpased: 2.238
batch start
#iterations: 73
currently lose_sum: 97.74016803503036
time_elpased: 2.347
batch start
#iterations: 74
currently lose_sum: 97.91382664442062
time_elpased: 2.271
batch start
#iterations: 75
currently lose_sum: 97.99920463562012
time_elpased: 2.303
batch start
#iterations: 76
currently lose_sum: 98.01751178503036
time_elpased: 2.305
batch start
#iterations: 77
currently lose_sum: 97.87099897861481
time_elpased: 2.512
batch start
#iterations: 78
currently lose_sum: 98.13659435510635
time_elpased: 2.157
batch start
#iterations: 79
currently lose_sum: 97.7982068657875
time_elpased: 2.313
start validation test
0.625670103093
0.635191714412
0.593351173322
0.61355896126
0.625723500746
63.544
batch start
#iterations: 80
currently lose_sum: 97.96557170152664
time_elpased: 2.202
batch start
#iterations: 81
currently lose_sum: 97.97714638710022
time_elpased: 2.298
batch start
#iterations: 82
currently lose_sum: 98.04058611392975
time_elpased: 1.892
batch start
#iterations: 83
currently lose_sum: 97.87233185768127
time_elpased: 2.347
batch start
#iterations: 84
currently lose_sum: 98.03614562749863
time_elpased: 2.294
batch start
#iterations: 85
currently lose_sum: 97.76080018281937
time_elpased: 2.174
batch start
#iterations: 86
currently lose_sum: 97.9849466085434
time_elpased: 2.23
batch start
#iterations: 87
currently lose_sum: 97.48452574014664
time_elpased: 2.064
batch start
#iterations: 88
currently lose_sum: 97.71916502714157
time_elpased: 2.191
batch start
#iterations: 89
currently lose_sum: 97.96781092882156
time_elpased: 1.84
batch start
#iterations: 90
currently lose_sum: 97.80263096094131
time_elpased: 2.096
batch start
#iterations: 91
currently lose_sum: 97.88955801725388
time_elpased: 2.122
batch start
#iterations: 92
currently lose_sum: 97.71382886171341
time_elpased: 2.004
batch start
#iterations: 93
currently lose_sum: 97.76680010557175
time_elpased: 2.013
batch start
#iterations: 94
currently lose_sum: 97.90463352203369
time_elpased: 1.876
batch start
#iterations: 95
currently lose_sum: 97.8226226568222
time_elpased: 1.85
batch start
#iterations: 96
currently lose_sum: 97.93107122182846
time_elpased: 1.689
batch start
#iterations: 97
currently lose_sum: 97.86408537626266
time_elpased: 1.838
batch start
#iterations: 98
currently lose_sum: 97.81349796056747
time_elpased: 2.228
batch start
#iterations: 99
currently lose_sum: 97.69835084676743
time_elpased: 2.165
start validation test
0.625515463918
0.648025123807
0.552181967888
0.59627674354
0.625636626241
63.805
batch start
#iterations: 100
currently lose_sum: 97.85625571012497
time_elpased: 2.433
batch start
#iterations: 101
currently lose_sum: 98.00561559200287
time_elpased: 2.33
batch start
#iterations: 102
currently lose_sum: 97.94728779792786
time_elpased: 2.292
batch start
#iterations: 103
currently lose_sum: 97.94696015119553
time_elpased: 1.931
batch start
#iterations: 104
currently lose_sum: 97.80138725042343
time_elpased: 2.091
batch start
#iterations: 105
currently lose_sum: 97.85356599092484
time_elpased: 2.007
batch start
#iterations: 106
currently lose_sum: 98.00247120857239
time_elpased: 2.254
batch start
#iterations: 107
currently lose_sum: 97.93302726745605
time_elpased: 2.274
batch start
#iterations: 108
currently lose_sum: 97.83240306377411
time_elpased: 2.248
batch start
#iterations: 109
currently lose_sum: 97.68316304683685
time_elpased: 2.183
batch start
#iterations: 110
currently lose_sum: 97.9426229596138
time_elpased: 2.169
batch start
#iterations: 111
currently lose_sum: 97.51282292604446
time_elpased: 2.335
batch start
#iterations: 112
currently lose_sum: 97.76482570171356
time_elpased: 2.124
batch start
#iterations: 113
currently lose_sum: 97.76853531599045
time_elpased: 2.194
batch start
#iterations: 114
currently lose_sum: 97.947634100914
time_elpased: 2.307
batch start
#iterations: 115
currently lose_sum: 97.63406109809875
time_elpased: 2.073
batch start
#iterations: 116
currently lose_sum: 98.20019209384918
time_elpased: 2.318
batch start
#iterations: 117
currently lose_sum: 97.42043179273605
time_elpased: 2.441
batch start
#iterations: 118
currently lose_sum: 98.08333826065063
time_elpased: 2.348
batch start
#iterations: 119
currently lose_sum: 97.60937625169754
time_elpased: 2.392
start validation test
0.627577319588
0.648079895375
0.561033347056
0.601423291223
0.627687264193
63.900
batch start
#iterations: 120
currently lose_sum: 97.5960499048233
time_elpased: 2.325
batch start
#iterations: 121
currently lose_sum: 98.02745079994202
time_elpased: 2.284
batch start
#iterations: 122
currently lose_sum: 97.82437378168106
time_elpased: 2.431
batch start
#iterations: 123
currently lose_sum: 97.85920298099518
time_elpased: 2.403
batch start
#iterations: 124
currently lose_sum: 98.00493931770325
time_elpased: 2.369
batch start
#iterations: 125
currently lose_sum: 97.66787230968475
time_elpased: 2.367
batch start
#iterations: 126
currently lose_sum: 97.46775829792023
time_elpased: 2.324
batch start
#iterations: 127
currently lose_sum: 97.74139600992203
time_elpased: 2.282
batch start
#iterations: 128
currently lose_sum: 97.58860504627228
time_elpased: 2.128
batch start
#iterations: 129
currently lose_sum: 97.73592382669449
time_elpased: 1.956
batch start
#iterations: 130
currently lose_sum: 97.98129045963287
time_elpased: 2.266
batch start
#iterations: 131
currently lose_sum: 97.47108900547028
time_elpased: 2.28
batch start
#iterations: 132
currently lose_sum: 97.75502800941467
time_elpased: 1.728
batch start
#iterations: 133
currently lose_sum: 97.74535363912582
time_elpased: 1.483
batch start
#iterations: 134
currently lose_sum: 97.50171399116516
time_elpased: 1.464
batch start
#iterations: 135
currently lose_sum: 97.98992449045181
time_elpased: 1.575
batch start
#iterations: 136
currently lose_sum: 97.71525114774704
time_elpased: 1.662
batch start
#iterations: 137
currently lose_sum: 97.93118625879288
time_elpased: 1.947
batch start
#iterations: 138
currently lose_sum: 97.51401507854462
time_elpased: 1.682
batch start
#iterations: 139
currently lose_sum: 97.57521861791611
time_elpased: 1.577
start validation test
0.630412371134
0.638973799127
0.602408398518
0.620152574698
0.630458639573
63.496
batch start
#iterations: 140
currently lose_sum: 97.38805538415909
time_elpased: 2.314
batch start
#iterations: 141
currently lose_sum: 97.788046002388
time_elpased: 2.024
batch start
#iterations: 142
currently lose_sum: 97.78456711769104
time_elpased: 1.954
batch start
#iterations: 143
currently lose_sum: 97.62597745656967
time_elpased: 2.003
batch start
#iterations: 144
currently lose_sum: 97.66158521175385
time_elpased: 2.337
batch start
#iterations: 145
currently lose_sum: 97.49422079324722
time_elpased: 2.183
batch start
#iterations: 146
currently lose_sum: 97.54181748628616
time_elpased: 2.291
batch start
#iterations: 147
currently lose_sum: 97.95665860176086
time_elpased: 1.653
batch start
#iterations: 148
currently lose_sum: 97.84211272001266
time_elpased: 1.743
batch start
#iterations: 149
currently lose_sum: 97.4639903306961
time_elpased: 1.712
batch start
#iterations: 150
currently lose_sum: 97.8044393658638
time_elpased: 2.411
batch start
#iterations: 151
currently lose_sum: 97.81680470705032
time_elpased: 2.141
batch start
#iterations: 152
currently lose_sum: 97.64661967754364
time_elpased: 2.152
batch start
#iterations: 153
currently lose_sum: 97.59742653369904
time_elpased: 2.39
batch start
#iterations: 154
currently lose_sum: 97.92276775836945
time_elpased: 2.047
batch start
#iterations: 155
currently lose_sum: 97.56506806612015
time_elpased: 2.265
batch start
#iterations: 156
currently lose_sum: 97.61957257986069
time_elpased: 2.183
batch start
#iterations: 157
currently lose_sum: 97.56440514326096
time_elpased: 2.244
batch start
#iterations: 158
currently lose_sum: 97.69939613342285
time_elpased: 1.989
batch start
#iterations: 159
currently lose_sum: 97.82951682806015
time_elpased: 1.965
start validation test
0.631030927835
0.635948129252
0.615788390284
0.625705919264
0.631056111705
63.522
batch start
#iterations: 160
currently lose_sum: 97.69510525465012
time_elpased: 2.316
batch start
#iterations: 161
currently lose_sum: 97.6859599351883
time_elpased: 2.288
batch start
#iterations: 162
currently lose_sum: 97.56762379407883
time_elpased: 2.223
batch start
#iterations: 163
currently lose_sum: 97.7451291680336
time_elpased: 1.786
batch start
#iterations: 164
currently lose_sum: 97.76266711950302
time_elpased: 2.242
batch start
#iterations: 165
currently lose_sum: 97.57077580690384
time_elpased: 2.41
batch start
#iterations: 166
currently lose_sum: 97.6189820766449
time_elpased: 2.004
batch start
#iterations: 167
currently lose_sum: 97.35901808738708
time_elpased: 2.303
batch start
#iterations: 168
currently lose_sum: 97.93147158622742
time_elpased: 2.022
batch start
#iterations: 169
currently lose_sum: 97.47172373533249
time_elpased: 1.959
batch start
#iterations: 170
currently lose_sum: 97.59170562028885
time_elpased: 2.31
batch start
#iterations: 171
currently lose_sum: 97.76615023612976
time_elpased: 2.115
batch start
#iterations: 172
currently lose_sum: 97.45399153232574
time_elpased: 2.339
batch start
#iterations: 173
currently lose_sum: 97.54391700029373
time_elpased: 2.12
batch start
#iterations: 174
currently lose_sum: 97.40716427564621
time_elpased: 2.186
batch start
#iterations: 175
currently lose_sum: 97.45240300893784
time_elpased: 2.283
batch start
#iterations: 176
currently lose_sum: 97.60611635446548
time_elpased: 2.163
batch start
#iterations: 177
currently lose_sum: 97.874471783638
time_elpased: 2.373
batch start
#iterations: 178
currently lose_sum: 97.68760067224503
time_elpased: 2.197
batch start
#iterations: 179
currently lose_sum: 97.42220342159271
time_elpased: 2.345
start validation test
0.63293814433
0.642067228731
0.603540551667
0.622208074699
0.632986715321
63.434
batch start
#iterations: 180
currently lose_sum: 97.50676727294922
time_elpased: 2.065
batch start
#iterations: 181
currently lose_sum: 97.74620193243027
time_elpased: 2.117
batch start
#iterations: 182
currently lose_sum: 97.6688842177391
time_elpased: 2.444
batch start
#iterations: 183
currently lose_sum: 97.70140272378922
time_elpased: 2.171
batch start
#iterations: 184
currently lose_sum: 97.41954374313354
time_elpased: 2.227
batch start
#iterations: 185
currently lose_sum: 97.55730557441711
time_elpased: 2.224
batch start
#iterations: 186
currently lose_sum: 97.47125035524368
time_elpased: 2.419
batch start
#iterations: 187
currently lose_sum: 97.57054620981216
time_elpased: 2.387
batch start
#iterations: 188
currently lose_sum: 97.44302415847778
time_elpased: 2.227
batch start
#iterations: 189
currently lose_sum: 97.75447303056717
time_elpased: 2.345
batch start
#iterations: 190
currently lose_sum: 97.81372064352036
time_elpased: 2.098
batch start
#iterations: 191
currently lose_sum: 97.39562338590622
time_elpased: 1.858
batch start
#iterations: 192
currently lose_sum: 97.54377406835556
time_elpased: 2.217
batch start
#iterations: 193
currently lose_sum: 97.40662086009979
time_elpased: 2.091
batch start
#iterations: 194
currently lose_sum: 97.55161321163177
time_elpased: 1.99
batch start
#iterations: 195
currently lose_sum: 97.58934593200684
time_elpased: 1.938
batch start
#iterations: 196
currently lose_sum: 97.25811368227005
time_elpased: 2.225
batch start
#iterations: 197
currently lose_sum: 97.2631162405014
time_elpased: 2.202
batch start
#iterations: 198
currently lose_sum: 97.49228626489639
time_elpased: 2.516
batch start
#iterations: 199
currently lose_sum: 97.37407183647156
time_elpased: 2.077
start validation test
0.629639175258
0.64534282761
0.578324413339
0.609998371601
0.629723958012
63.612
batch start
#iterations: 200
currently lose_sum: 97.71953225135803
time_elpased: 2.127
batch start
#iterations: 201
currently lose_sum: 97.23862773180008
time_elpased: 2.284
batch start
#iterations: 202
currently lose_sum: 97.53127253055573
time_elpased: 2.255
batch start
#iterations: 203
currently lose_sum: 97.4103896021843
time_elpased: 1.704
batch start
#iterations: 204
currently lose_sum: 97.52642542123795
time_elpased: 1.887
batch start
#iterations: 205
currently lose_sum: 97.71541744470596
time_elpased: 1.971
batch start
#iterations: 206
currently lose_sum: 97.50478690862656
time_elpased: 2.023
batch start
#iterations: 207
currently lose_sum: 97.37690961360931
time_elpased: 2.131
batch start
#iterations: 208
currently lose_sum: 97.58661252260208
time_elpased: 1.913
batch start
#iterations: 209
currently lose_sum: 97.51596176624298
time_elpased: 2.038
batch start
#iterations: 210
currently lose_sum: 97.55171412229538
time_elpased: 1.86
batch start
#iterations: 211
currently lose_sum: 97.11893635988235
time_elpased: 1.723
batch start
#iterations: 212
currently lose_sum: 97.51974815130234
time_elpased: 1.948
batch start
#iterations: 213
currently lose_sum: 97.67550492286682
time_elpased: 2.01
batch start
#iterations: 214
currently lose_sum: 97.52150893211365
time_elpased: 1.896
batch start
#iterations: 215
currently lose_sum: 97.52186691761017
time_elpased: 2.335
batch start
#iterations: 216
currently lose_sum: 97.17515677213669
time_elpased: 2.334
batch start
#iterations: 217
currently lose_sum: 97.52363187074661
time_elpased: 1.904
batch start
#iterations: 218
currently lose_sum: 97.5960550904274
time_elpased: 2.092
batch start
#iterations: 219
currently lose_sum: 97.48512989282608
time_elpased: 2.149
start validation test
0.635051546392
0.635068661611
0.637813915191
0.636438328027
0.635046982379
63.348
batch start
#iterations: 220
currently lose_sum: 97.47070175409317
time_elpased: 2.208
batch start
#iterations: 221
currently lose_sum: 97.65381681919098
time_elpased: 2.375
batch start
#iterations: 222
currently lose_sum: 97.22306030988693
time_elpased: 2.227
batch start
#iterations: 223
currently lose_sum: 97.15194344520569
time_elpased: 2.312
batch start
#iterations: 224
currently lose_sum: 97.47531819343567
time_elpased: 2.196
batch start
#iterations: 225
currently lose_sum: 97.4391320347786
time_elpased: 1.847
batch start
#iterations: 226
currently lose_sum: 97.49950474500656
time_elpased: 1.976
batch start
#iterations: 227
currently lose_sum: 97.44167393445969
time_elpased: 2.119
batch start
#iterations: 228
currently lose_sum: 97.34160351753235
time_elpased: 2.055
batch start
#iterations: 229
currently lose_sum: 97.41768550872803
time_elpased: 2.299
batch start
#iterations: 230
currently lose_sum: 97.38693153858185
time_elpased: 2.292
batch start
#iterations: 231
currently lose_sum: 97.28011310100555
time_elpased: 2.32
batch start
#iterations: 232
currently lose_sum: 97.73558866977692
time_elpased: 2.29
batch start
#iterations: 233
currently lose_sum: 97.40968322753906
time_elpased: 2.271
batch start
#iterations: 234
currently lose_sum: 97.46438390016556
time_elpased: 2.312
batch start
#iterations: 235
currently lose_sum: 97.41115945577621
time_elpased: 2.182
batch start
#iterations: 236
currently lose_sum: 97.44059664011002
time_elpased: 1.939
batch start
#iterations: 237
currently lose_sum: 97.14616966247559
time_elpased: 2.034
batch start
#iterations: 238
currently lose_sum: 97.53776526451111
time_elpased: 2.336
batch start
#iterations: 239
currently lose_sum: 97.54818749427795
time_elpased: 2.336
start validation test
0.63324742268
0.641558724284
0.606628242075
0.623604718828
0.633291403152
63.415
batch start
#iterations: 240
currently lose_sum: 97.39411532878876
time_elpased: 2.464
batch start
#iterations: 241
currently lose_sum: 97.23176372051239
time_elpased: 2.277
batch start
#iterations: 242
currently lose_sum: 97.33562052249908
time_elpased: 2.381
batch start
#iterations: 243
currently lose_sum: 97.55687236785889
time_elpased: 1.941
batch start
#iterations: 244
currently lose_sum: 97.46738213300705
time_elpased: 2.348
batch start
#iterations: 245
currently lose_sum: 97.77348387241364
time_elpased: 2.262
batch start
#iterations: 246
currently lose_sum: 97.7523222565651
time_elpased: 2.361
batch start
#iterations: 247
currently lose_sum: 97.05711138248444
time_elpased: 2.052
batch start
#iterations: 248
currently lose_sum: 97.67388504743576
time_elpased: 2.298
batch start
#iterations: 249
currently lose_sum: 97.67916053533554
time_elpased: 1.789
batch start
#iterations: 250
currently lose_sum: 97.39612519741058
time_elpased: 2.289
batch start
#iterations: 251
currently lose_sum: 97.45516222715378
time_elpased: 2.397
batch start
#iterations: 252
currently lose_sum: 97.72679686546326
time_elpased: 2.176
batch start
#iterations: 253
currently lose_sum: 97.52143490314484
time_elpased: 2.432
batch start
#iterations: 254
currently lose_sum: 97.5981742143631
time_elpased: 2.234
batch start
#iterations: 255
currently lose_sum: 97.17281180620193
time_elpased: 1.817
batch start
#iterations: 256
currently lose_sum: 97.26988053321838
time_elpased: 2.209
batch start
#iterations: 257
currently lose_sum: 97.41713780164719
time_elpased: 2.342
batch start
#iterations: 258
currently lose_sum: 97.32889086008072
time_elpased: 2.058
batch start
#iterations: 259
currently lose_sum: 97.48579490184784
time_elpased: 2.13
start validation test
0.62881443299
0.641117719672
0.587999176616
0.613410640468
0.628881868358
63.505
batch start
#iterations: 260
currently lose_sum: 97.60980224609375
time_elpased: 2.12
batch start
#iterations: 261
currently lose_sum: 97.28721213340759
time_elpased: 2.321
batch start
#iterations: 262
currently lose_sum: 97.28208267688751
time_elpased: 2.164
batch start
#iterations: 263
currently lose_sum: 97.40727466344833
time_elpased: 2.031
batch start
#iterations: 264
currently lose_sum: 97.51992774009705
time_elpased: 2.247
batch start
#iterations: 265
currently lose_sum: 97.34024959802628
time_elpased: 2.294
batch start
#iterations: 266
currently lose_sum: 97.37561863660812
time_elpased: 2.201
batch start
#iterations: 267
currently lose_sum: 97.59307593107224
time_elpased: 2.119
batch start
#iterations: 268
currently lose_sum: 97.36561453342438
time_elpased: 2.114
batch start
#iterations: 269
currently lose_sum: 97.52368861436844
time_elpased: 2.217
batch start
#iterations: 270
currently lose_sum: 97.0306339263916
time_elpased: 1.84
batch start
#iterations: 271
currently lose_sum: 97.57946014404297
time_elpased: 1.832
batch start
#iterations: 272
currently lose_sum: 97.31441289186478
time_elpased: 1.751
batch start
#iterations: 273
currently lose_sum: 97.48347014188766
time_elpased: 1.743
batch start
#iterations: 274
currently lose_sum: 97.11491990089417
time_elpased: 1.819
batch start
#iterations: 275
currently lose_sum: 97.72953110933304
time_elpased: 1.88
batch start
#iterations: 276
currently lose_sum: 97.47527551651001
time_elpased: 2.254
batch start
#iterations: 277
currently lose_sum: 97.35901743173599
time_elpased: 2.205
batch start
#iterations: 278
currently lose_sum: 97.48865211009979
time_elpased: 2.105
batch start
#iterations: 279
currently lose_sum: 97.36678355932236
time_elpased: 2.292
start validation test
0.63118556701
0.641601238527
0.597159324825
0.618583080122
0.631241785502
63.428
batch start
#iterations: 280
currently lose_sum: 97.54801338911057
time_elpased: 2.129
batch start
#iterations: 281
currently lose_sum: 97.1220822930336
time_elpased: 2.365
batch start
#iterations: 282
currently lose_sum: 97.27283948659897
time_elpased: 2.305
batch start
#iterations: 283
currently lose_sum: 97.59679400920868
time_elpased: 1.989
batch start
#iterations: 284
currently lose_sum: 97.2879182100296
time_elpased: 2.156
batch start
#iterations: 285
currently lose_sum: 97.26716876029968
time_elpased: 2.308
batch start
#iterations: 286
currently lose_sum: 97.19402503967285
time_elpased: 2.263
batch start
#iterations: 287
currently lose_sum: 97.4825690984726
time_elpased: 2.254
batch start
#iterations: 288
currently lose_sum: 97.39394354820251
time_elpased: 1.999
batch start
#iterations: 289
currently lose_sum: 97.3143355846405
time_elpased: 2.23
batch start
#iterations: 290
currently lose_sum: 97.34643292427063
time_elpased: 2.204
batch start
#iterations: 291
currently lose_sum: 97.44909453392029
time_elpased: 2.017
batch start
#iterations: 292
currently lose_sum: 97.38551127910614
time_elpased: 2.281
batch start
#iterations: 293
currently lose_sum: 97.18971925973892
time_elpased: 2.405
batch start
#iterations: 294
currently lose_sum: 97.37847310304642
time_elpased: 2.356
batch start
#iterations: 295
currently lose_sum: 97.20932859182358
time_elpased: 2.227
batch start
#iterations: 296
currently lose_sum: 97.45386928319931
time_elpased: 2.416
batch start
#iterations: 297
currently lose_sum: 97.4975191950798
time_elpased: 2.36
batch start
#iterations: 298
currently lose_sum: 97.04270350933075
time_elpased: 2.257
batch start
#iterations: 299
currently lose_sum: 97.31399649381638
time_elpased: 2.129
start validation test
0.631030927835
0.632869312279
0.627006998765
0.629924516596
0.63103757621
63.422
batch start
#iterations: 300
currently lose_sum: 97.4963510632515
time_elpased: 2.085
batch start
#iterations: 301
currently lose_sum: 97.46789562702179
time_elpased: 2.02
batch start
#iterations: 302
currently lose_sum: 97.30182307958603
time_elpased: 2.462
batch start
#iterations: 303
currently lose_sum: 97.01539427042007
time_elpased: 2.342
batch start
#iterations: 304
currently lose_sum: 96.90075242519379
time_elpased: 1.683
batch start
#iterations: 305
currently lose_sum: 97.1981999874115
time_elpased: 2.459
batch start
#iterations: 306
currently lose_sum: 97.32775890827179
time_elpased: 2.483
batch start
#iterations: 307
currently lose_sum: 97.63494920730591
time_elpased: 2.378
batch start
#iterations: 308
currently lose_sum: 97.1309284567833
time_elpased: 2.329
batch start
#iterations: 309
currently lose_sum: 97.07916992902756
time_elpased: 2.361
batch start
#iterations: 310
currently lose_sum: 97.20332300662994
time_elpased: 2.083
batch start
#iterations: 311
currently lose_sum: 97.05172967910767
time_elpased: 2.17
batch start
#iterations: 312
currently lose_sum: 97.44413733482361
time_elpased: 2.371
batch start
#iterations: 313
currently lose_sum: 97.32146608829498
time_elpased: 2.455
batch start
#iterations: 314
currently lose_sum: 97.65433865785599
time_elpased: 2.405
batch start
#iterations: 315
currently lose_sum: 97.05026882886887
time_elpased: 2.306
batch start
#iterations: 316
currently lose_sum: 97.29952269792557
time_elpased: 2.335
batch start
#iterations: 317
currently lose_sum: 97.07724809646606
time_elpased: 2.205
batch start
#iterations: 318
currently lose_sum: 97.0563178062439
time_elpased: 2.122
batch start
#iterations: 319
currently lose_sum: 97.67753142118454
time_elpased: 1.732
start validation test
0.631597938144
0.634940645026
0.622066694113
0.628437743696
0.631613685759
63.407
batch start
#iterations: 320
currently lose_sum: 97.48518794775009
time_elpased: 2.092
batch start
#iterations: 321
currently lose_sum: 97.32669413089752
time_elpased: 2.198
batch start
#iterations: 322
currently lose_sum: 97.14008742570877
time_elpased: 2.205
batch start
#iterations: 323
currently lose_sum: 97.3942323923111
time_elpased: 2.247
batch start
#iterations: 324
currently lose_sum: 97.51959073543549
time_elpased: 2.34
batch start
#iterations: 325
currently lose_sum: 97.17134153842926
time_elpased: 2.449
batch start
#iterations: 326
currently lose_sum: 97.17933464050293
time_elpased: 2.341
batch start
#iterations: 327
currently lose_sum: 97.35721808671951
time_elpased: 2.133
batch start
#iterations: 328
currently lose_sum: 97.25136142969131
time_elpased: 1.79
batch start
#iterations: 329
currently lose_sum: 97.52369886636734
time_elpased: 1.796
batch start
#iterations: 330
currently lose_sum: 97.18524086475372
time_elpased: 2.066
batch start
#iterations: 331
currently lose_sum: 97.35760313272476
time_elpased: 1.922
batch start
#iterations: 332
currently lose_sum: 97.26103234291077
time_elpased: 1.993
batch start
#iterations: 333
currently lose_sum: 97.45428478717804
time_elpased: 2.034
batch start
#iterations: 334
currently lose_sum: 97.3148866891861
time_elpased: 2.186
batch start
#iterations: 335
currently lose_sum: 97.287642121315
time_elpased: 2.064
batch start
#iterations: 336
currently lose_sum: 97.24605405330658
time_elpased: 2.37
batch start
#iterations: 337
currently lose_sum: 97.42177248001099
time_elpased: 2.137
batch start
#iterations: 338
currently lose_sum: 97.29641342163086
time_elpased: 2.073
batch start
#iterations: 339
currently lose_sum: 97.30759650468826
time_elpased: 2.369
start validation test
0.631237113402
0.635383639822
0.618773157678
0.626968401293
0.631257706472
63.414
batch start
#iterations: 340
currently lose_sum: 97.46384680271149
time_elpased: 2.288
batch start
#iterations: 341
currently lose_sum: 97.04602146148682
time_elpased: 2.324
batch start
#iterations: 342
currently lose_sum: 97.49305939674377
time_elpased: 2.245
batch start
#iterations: 343
currently lose_sum: 97.49851655960083
time_elpased: 2.0
batch start
#iterations: 344
currently lose_sum: 97.25002092123032
time_elpased: 2.126
batch start
#iterations: 345
currently lose_sum: 97.33115214109421
time_elpased: 1.945
batch start
#iterations: 346
currently lose_sum: 97.09875792264938
time_elpased: 1.975
batch start
#iterations: 347
currently lose_sum: 97.40927952528
time_elpased: 2.299
batch start
#iterations: 348
currently lose_sum: 97.2413477897644
time_elpased: 2.041
batch start
#iterations: 349
currently lose_sum: 97.46510797739029
time_elpased: 2.246
batch start
#iterations: 350
currently lose_sum: 97.23754107952118
time_elpased: 2.314
batch start
#iterations: 351
currently lose_sum: 97.60327237844467
time_elpased: 2.274
batch start
#iterations: 352
currently lose_sum: 97.22473609447479
time_elpased: 2.063
batch start
#iterations: 353
currently lose_sum: 97.53308182954788
time_elpased: 2.234
batch start
#iterations: 354
currently lose_sum: 97.33490431308746
time_elpased: 2.216
batch start
#iterations: 355
currently lose_sum: 97.10939586162567
time_elpased: 1.885
batch start
#iterations: 356
currently lose_sum: 97.26230156421661
time_elpased: 2.079
batch start
#iterations: 357
currently lose_sum: 97.28787887096405
time_elpased: 1.998
batch start
#iterations: 358
currently lose_sum: 97.49700075387955
time_elpased: 1.864
batch start
#iterations: 359
currently lose_sum: 97.0573017001152
time_elpased: 1.925
start validation test
0.631907216495
0.63774473093
0.613524083985
0.625399989508
0.631937589287
63.442
batch start
#iterations: 360
currently lose_sum: 97.48394918441772
time_elpased: 1.941
batch start
#iterations: 361
currently lose_sum: 97.36498093605042
time_elpased: 2.132
batch start
#iterations: 362
currently lose_sum: 97.29587352275848
time_elpased: 1.894
batch start
#iterations: 363
currently lose_sum: 97.25525522232056
time_elpased: 1.876
batch start
#iterations: 364
currently lose_sum: 97.27733814716339
time_elpased: 1.794
batch start
#iterations: 365
currently lose_sum: 97.35374414920807
time_elpased: 1.888
batch start
#iterations: 366
currently lose_sum: 97.23665100336075
time_elpased: 1.942
batch start
#iterations: 367
currently lose_sum: 97.20512288808823
time_elpased: 1.91
batch start
#iterations: 368
currently lose_sum: 97.27999192476273
time_elpased: 2.236
batch start
#iterations: 369
currently lose_sum: 97.3868219256401
time_elpased: 2.397
batch start
#iterations: 370
currently lose_sum: 97.50719183683395
time_elpased: 2.073
batch start
#iterations: 371
currently lose_sum: 96.98038613796234
time_elpased: 2.033
batch start
#iterations: 372
currently lose_sum: 97.08549708127975
time_elpased: 2.251
batch start
#iterations: 373
currently lose_sum: 97.26504307985306
time_elpased: 2.356
batch start
#iterations: 374
currently lose_sum: 97.33269536495209
time_elpased: 2.175
batch start
#iterations: 375
currently lose_sum: 97.10310053825378
time_elpased: 2.394
batch start
#iterations: 376
currently lose_sum: 96.83921468257904
time_elpased: 1.993
batch start
#iterations: 377
currently lose_sum: 97.08516001701355
time_elpased: 2.338
batch start
#iterations: 378
currently lose_sum: 97.65617990493774
time_elpased: 2.118
batch start
#iterations: 379
currently lose_sum: 97.2827615737915
time_elpased: 1.914
start validation test
0.625721649485
0.647305892236
0.555166735282
0.597706244113
0.625838221007
63.722
batch start
#iterations: 380
currently lose_sum: 97.06734877824783
time_elpased: 2.152
batch start
#iterations: 381
currently lose_sum: 97.04241800308228
time_elpased: 2.178
batch start
#iterations: 382
currently lose_sum: 97.52966243028641
time_elpased: 2.029
batch start
#iterations: 383
currently lose_sum: 97.2417179942131
time_elpased: 2.016
batch start
#iterations: 384
currently lose_sum: 97.1214947104454
time_elpased: 2.003
batch start
#iterations: 385
currently lose_sum: 97.3034400343895
time_elpased: 2.153
batch start
#iterations: 386
currently lose_sum: 97.15582478046417
time_elpased: 2.36
batch start
#iterations: 387
currently lose_sum: 97.18096840381622
time_elpased: 1.885
batch start
#iterations: 388
currently lose_sum: 97.34886610507965
time_elpased: 2.228
batch start
#iterations: 389
currently lose_sum: 97.17415952682495
time_elpased: 2.083
batch start
#iterations: 390
currently lose_sum: 97.49743318557739
time_elpased: 2.168
batch start
#iterations: 391
currently lose_sum: 97.09352487325668
time_elpased: 2.479
batch start
#iterations: 392
currently lose_sum: 97.40199434757233
time_elpased: 2.446
batch start
#iterations: 393
currently lose_sum: 97.02823704481125
time_elpased: 2.298
batch start
#iterations: 394
currently lose_sum: 96.981374502182
time_elpased: 2.384
batch start
#iterations: 395
currently lose_sum: 97.31135421991348
time_elpased: 2.243
batch start
#iterations: 396
currently lose_sum: 97.20094019174576
time_elpased: 1.834
batch start
#iterations: 397
currently lose_sum: 97.12066948413849
time_elpased: 2.329
batch start
#iterations: 398
currently lose_sum: 97.0209031701088
time_elpased: 2.186
batch start
#iterations: 399
currently lose_sum: 97.15262526273727
time_elpased: 2.077
start validation test
0.62912371134
0.646416540829
0.572766570605
0.607366984993
0.629216825162
63.663
acc: 0.628
pre: 0.629
rec: 0.628
F1: 0.629
auc: 0.665
