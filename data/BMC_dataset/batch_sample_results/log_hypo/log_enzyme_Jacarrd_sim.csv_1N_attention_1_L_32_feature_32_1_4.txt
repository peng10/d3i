start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.73933547735214
time_elpased: 2.737
batch start
#iterations: 1
currently lose_sum: 100.42046231031418
time_elpased: 2.784
batch start
#iterations: 2
currently lose_sum: 100.09673178195953
time_elpased: 2.873
batch start
#iterations: 3
currently lose_sum: 99.94761717319489
time_elpased: 2.846
batch start
#iterations: 4
currently lose_sum: 99.83820408582687
time_elpased: 2.924
batch start
#iterations: 5
currently lose_sum: 99.80698585510254
time_elpased: 2.709
batch start
#iterations: 6
currently lose_sum: 99.50707077980042
time_elpased: 2.282
batch start
#iterations: 7
currently lose_sum: 99.44988137483597
time_elpased: 2.177
batch start
#iterations: 8
currently lose_sum: 99.49617528915405
time_elpased: 2.39
batch start
#iterations: 9
currently lose_sum: 99.11094111204147
time_elpased: 2.519
batch start
#iterations: 10
currently lose_sum: 99.18644344806671
time_elpased: 2.763
batch start
#iterations: 11
currently lose_sum: 99.29602402448654
time_elpased: 2.584
batch start
#iterations: 12
currently lose_sum: 99.09049171209335
time_elpased: 2.43
batch start
#iterations: 13
currently lose_sum: 99.17391103506088
time_elpased: 2.506
batch start
#iterations: 14
currently lose_sum: 98.85578793287277
time_elpased: 2.491
batch start
#iterations: 15
currently lose_sum: 98.89568084478378
time_elpased: 2.641
batch start
#iterations: 16
currently lose_sum: 98.94675254821777
time_elpased: 2.424
batch start
#iterations: 17
currently lose_sum: 99.00125902891159
time_elpased: 2.398
batch start
#iterations: 18
currently lose_sum: 98.68055242300034
time_elpased: 2.182
batch start
#iterations: 19
currently lose_sum: 98.8810703754425
time_elpased: 2.587
start validation test
0.616030927835
0.626280623608
0.578779458681
0.601593838584
0.616096328535
64.437
batch start
#iterations: 20
currently lose_sum: 98.86451357603073
time_elpased: 2.075
batch start
#iterations: 21
currently lose_sum: 98.67006301879883
time_elpased: 2.255
batch start
#iterations: 22
currently lose_sum: 98.73701119422913
time_elpased: 2.635
batch start
#iterations: 23
currently lose_sum: 98.94316458702087
time_elpased: 2.72
batch start
#iterations: 24
currently lose_sum: 98.5450536608696
time_elpased: 2.283
batch start
#iterations: 25
currently lose_sum: 98.6298103928566
time_elpased: 2.648
batch start
#iterations: 26
currently lose_sum: 98.69955027103424
time_elpased: 2.533
batch start
#iterations: 27
currently lose_sum: 98.48097151517868
time_elpased: 2.93
batch start
#iterations: 28
currently lose_sum: 98.43357223272324
time_elpased: 3.207
batch start
#iterations: 29
currently lose_sum: 98.3523901104927
time_elpased: 3.42
batch start
#iterations: 30
currently lose_sum: 98.60696023702621
time_elpased: 3.649
batch start
#iterations: 31
currently lose_sum: 98.32462871074677
time_elpased: 3.808
batch start
#iterations: 32
currently lose_sum: 98.58864587545395
time_elpased: 3.654
batch start
#iterations: 33
currently lose_sum: 98.254185795784
time_elpased: 3.844
batch start
#iterations: 34
currently lose_sum: 98.38678538799286
time_elpased: 3.37
batch start
#iterations: 35
currently lose_sum: 98.18349027633667
time_elpased: 3.288
batch start
#iterations: 36
currently lose_sum: 98.3525162935257
time_elpased: 3.522
batch start
#iterations: 37
currently lose_sum: 98.36074429750443
time_elpased: 3.573
batch start
#iterations: 38
currently lose_sum: 98.15190041065216
time_elpased: 3.796
batch start
#iterations: 39
currently lose_sum: 98.18058443069458
time_elpased: 3.555
start validation test
0.613969072165
0.637871287129
0.530410620562
0.579198741361
0.614115771915
64.143
batch start
#iterations: 40
currently lose_sum: 98.54526901245117
time_elpased: 3.944
batch start
#iterations: 41
currently lose_sum: 98.22707092761993
time_elpased: 3.971
batch start
#iterations: 42
currently lose_sum: 98.36727839708328
time_elpased: 3.85
batch start
#iterations: 43
currently lose_sum: 98.30140900611877
time_elpased: 3.358
batch start
#iterations: 44
currently lose_sum: 98.15727418661118
time_elpased: 3.179
batch start
#iterations: 45
currently lose_sum: 97.99079531431198
time_elpased: 3.685
batch start
#iterations: 46
currently lose_sum: 98.34969782829285
time_elpased: 4.122
batch start
#iterations: 47
currently lose_sum: 98.47644472122192
time_elpased: 3.36
batch start
#iterations: 48
currently lose_sum: 98.36913746595383
time_elpased: 3.707
batch start
#iterations: 49
currently lose_sum: 98.02310222387314
time_elpased: 3.853
batch start
#iterations: 50
currently lose_sum: 98.14350062608719
time_elpased: 4.194
batch start
#iterations: 51
currently lose_sum: 97.97851026058197
time_elpased: 3.684
batch start
#iterations: 52
currently lose_sum: 98.35976821184158
time_elpased: 3.436
batch start
#iterations: 53
currently lose_sum: 97.91949814558029
time_elpased: 3.736
batch start
#iterations: 54
currently lose_sum: 98.09430474042892
time_elpased: 3.621
batch start
#iterations: 55
currently lose_sum: 98.07474792003632
time_elpased: 3.423
batch start
#iterations: 56
currently lose_sum: 98.19529610872269
time_elpased: 3.171
batch start
#iterations: 57
currently lose_sum: 98.09684008359909
time_elpased: 3.578
batch start
#iterations: 58
currently lose_sum: 98.11108684539795
time_elpased: 3.94
batch start
#iterations: 59
currently lose_sum: 98.27819591760635
time_elpased: 3.954
start validation test
0.609587628866
0.644660456325
0.491406812802
0.557696799813
0.609795113517
64.319
batch start
#iterations: 60
currently lose_sum: 98.05019801855087
time_elpased: 3.603
batch start
#iterations: 61
currently lose_sum: 98.31820380687714
time_elpased: 3.948
batch start
#iterations: 62
currently lose_sum: 98.07716768980026
time_elpased: 3.648
batch start
#iterations: 63
currently lose_sum: 98.05610305070877
time_elpased: 3.678
batch start
#iterations: 64
currently lose_sum: 98.3916484117508
time_elpased: 3.665
batch start
#iterations: 65
currently lose_sum: 98.09247261285782
time_elpased: 3.486
batch start
#iterations: 66
currently lose_sum: 98.02881395816803
time_elpased: 3.462
batch start
#iterations: 67
currently lose_sum: 97.83943265676498
time_elpased: 3.027
batch start
#iterations: 68
currently lose_sum: 97.72508323192596
time_elpased: 3.685
batch start
#iterations: 69
currently lose_sum: 98.36980015039444
time_elpased: 3.961
batch start
#iterations: 70
currently lose_sum: 98.06250059604645
time_elpased: 3.807
batch start
#iterations: 71
currently lose_sum: 97.99614000320435
time_elpased: 3.735
batch start
#iterations: 72
currently lose_sum: 97.80828738212585
time_elpased: 3.711
batch start
#iterations: 73
currently lose_sum: 98.06243997812271
time_elpased: 4.026
batch start
#iterations: 74
currently lose_sum: 97.91016906499863
time_elpased: 4.264
batch start
#iterations: 75
currently lose_sum: 97.97981578111649
time_elpased: 3.443
batch start
#iterations: 76
currently lose_sum: 97.81911331415176
time_elpased: 3.712
batch start
#iterations: 77
currently lose_sum: 98.09191578626633
time_elpased: 3.847
batch start
#iterations: 78
currently lose_sum: 98.0268023610115
time_elpased: 3.429
batch start
#iterations: 79
currently lose_sum: 98.12464725971222
time_elpased: 3.913
start validation test
0.623762886598
0.632304661852
0.594627971596
0.612887828162
0.623814037435
63.686
batch start
#iterations: 80
currently lose_sum: 98.04642921686172
time_elpased: 2.376
batch start
#iterations: 81
currently lose_sum: 98.11838263273239
time_elpased: 6.24
batch start
#iterations: 82
currently lose_sum: 97.99677044153214
time_elpased: 4.023
batch start
#iterations: 83
currently lose_sum: 97.77740329504013
time_elpased: 3.028
batch start
#iterations: 84
currently lose_sum: 97.97793304920197
time_elpased: 2.404
batch start
#iterations: 85
currently lose_sum: 98.1121740937233
time_elpased: 4.362
batch start
#iterations: 86
currently lose_sum: 97.95787280797958
time_elpased: 3.638
batch start
#iterations: 87
currently lose_sum: 98.08763843774796
time_elpased: 3.92
batch start
#iterations: 88
currently lose_sum: 97.87149459123611
time_elpased: 4.318
batch start
#iterations: 89
currently lose_sum: 98.08072525262833
time_elpased: 3.661
batch start
#iterations: 90
currently lose_sum: 97.86086428165436
time_elpased: 3.449
batch start
#iterations: 91
currently lose_sum: 98.07017827033997
time_elpased: 4.572
batch start
#iterations: 92
currently lose_sum: 97.80812817811966
time_elpased: 4.322
batch start
#iterations: 93
currently lose_sum: 97.5840876698494
time_elpased: 3.014
batch start
#iterations: 94
currently lose_sum: 98.07341867685318
time_elpased: 2.689
batch start
#iterations: 95
currently lose_sum: 97.9862590432167
time_elpased: 2.394
batch start
#iterations: 96
currently lose_sum: 97.90211778879166
time_elpased: 2.756
batch start
#iterations: 97
currently lose_sum: 97.9688749909401
time_elpased: 3.009
batch start
#iterations: 98
currently lose_sum: 98.10806047916412
time_elpased: 2.542
batch start
#iterations: 99
currently lose_sum: 98.04292404651642
time_elpased: 2.513
start validation test
0.618453608247
0.64284832778
0.536070803746
0.584624017957
0.618598243967
63.834
batch start
#iterations: 100
currently lose_sum: 97.86664485931396
time_elpased: 2.609
batch start
#iterations: 101
currently lose_sum: 97.81532788276672
time_elpased: 3.204
batch start
#iterations: 102
currently lose_sum: 97.79188817739487
time_elpased: 3.237
batch start
#iterations: 103
currently lose_sum: 97.83346456289291
time_elpased: 2.984
batch start
#iterations: 104
currently lose_sum: 97.71351683139801
time_elpased: 3.048
batch start
#iterations: 105
currently lose_sum: 97.84397995471954
time_elpased: 3.349
batch start
#iterations: 106
currently lose_sum: 97.90283936262131
time_elpased: 3.577
batch start
#iterations: 107
currently lose_sum: 97.97188806533813
time_elpased: 3.301
batch start
#iterations: 108
currently lose_sum: 97.88136804103851
time_elpased: 3.931
batch start
#iterations: 109
currently lose_sum: 97.84079718589783
time_elpased: 3.627
batch start
#iterations: 110
currently lose_sum: 97.87283712625504
time_elpased: 4.007
batch start
#iterations: 111
currently lose_sum: 97.95192259550095
time_elpased: 3.931
batch start
#iterations: 112
currently lose_sum: 97.85725545883179
time_elpased: 2.968
batch start
#iterations: 113
currently lose_sum: 97.54039478302002
time_elpased: 6.883
batch start
#iterations: 114
currently lose_sum: 97.83511739969254
time_elpased: 2.999
batch start
#iterations: 115
currently lose_sum: 97.84859299659729
time_elpased: 2.796
batch start
#iterations: 116
currently lose_sum: 97.84885895252228
time_elpased: 3.013
batch start
#iterations: 117
currently lose_sum: 97.77336412668228
time_elpased: 2.612
batch start
#iterations: 118
currently lose_sum: 97.7088730931282
time_elpased: 3.359
batch start
#iterations: 119
currently lose_sum: 97.71732646226883
time_elpased: 3.775
start validation test
0.625051546392
0.634749034749
0.592158073479
0.612714300926
0.625109295957
63.585
batch start
#iterations: 120
currently lose_sum: 97.58597230911255
time_elpased: 3.114
batch start
#iterations: 121
currently lose_sum: 97.75268334150314
time_elpased: 3.098
batch start
#iterations: 122
currently lose_sum: 97.70879513025284
time_elpased: 3.284
batch start
#iterations: 123
currently lose_sum: 97.5055638551712
time_elpased: 2.981
batch start
#iterations: 124
currently lose_sum: 97.6130222082138
time_elpased: 2.991
batch start
#iterations: 125
currently lose_sum: 97.7452604174614
time_elpased: 3.204
batch start
#iterations: 126
currently lose_sum: 97.78240597248077
time_elpased: 3.123
batch start
#iterations: 127
currently lose_sum: 97.9859448671341
time_elpased: 3.141
batch start
#iterations: 128
currently lose_sum: 97.59974956512451
time_elpased: 3.334
batch start
#iterations: 129
currently lose_sum: 97.56033682823181
time_elpased: 3.266
batch start
#iterations: 130
currently lose_sum: 97.72461491823196
time_elpased: 3.027
batch start
#iterations: 131
currently lose_sum: 97.60146051645279
time_elpased: 3.059
batch start
#iterations: 132
currently lose_sum: 97.78614324331284
time_elpased: 3.004
batch start
#iterations: 133
currently lose_sum: 97.71687227487564
time_elpased: 2.86
batch start
#iterations: 134
currently lose_sum: 97.67338293790817
time_elpased: 2.809
batch start
#iterations: 135
currently lose_sum: 97.96872913837433
time_elpased: 3.451
batch start
#iterations: 136
currently lose_sum: 97.59971511363983
time_elpased: 3.401
batch start
#iterations: 137
currently lose_sum: 97.82824689149857
time_elpased: 2.979
batch start
#iterations: 138
currently lose_sum: 97.9179162979126
time_elpased: 2.958
batch start
#iterations: 139
currently lose_sum: 97.59171426296234
time_elpased: 3.067
start validation test
0.626391752577
0.634170198891
0.600493979623
0.616872819537
0.626437220112
63.520
batch start
#iterations: 140
currently lose_sum: 97.4931366443634
time_elpased: 3.23
batch start
#iterations: 141
currently lose_sum: 97.62565690279007
time_elpased: 2.994
batch start
#iterations: 142
currently lose_sum: 97.65481865406036
time_elpased: 3.153
batch start
#iterations: 143
currently lose_sum: 97.68613743782043
time_elpased: 2.864
batch start
#iterations: 144
currently lose_sum: 97.62987101078033
time_elpased: 2.074
batch start
#iterations: 145
currently lose_sum: 97.97762477397919
time_elpased: 3.838
batch start
#iterations: 146
currently lose_sum: 97.6185393333435
time_elpased: 2.535
batch start
#iterations: 147
currently lose_sum: 97.77458941936493
time_elpased: 2.941
batch start
#iterations: 148
currently lose_sum: 97.61993265151978
time_elpased: 2.905
batch start
#iterations: 149
currently lose_sum: 97.68640577793121
time_elpased: 2.681
batch start
#iterations: 150
currently lose_sum: 97.49646520614624
time_elpased: 2.521
batch start
#iterations: 151
currently lose_sum: 97.73060941696167
time_elpased: 2.598
batch start
#iterations: 152
currently lose_sum: 97.48103737831116
time_elpased: 2.372
batch start
#iterations: 153
currently lose_sum: 97.68012875318527
time_elpased: 3.043
batch start
#iterations: 154
currently lose_sum: 97.66389042139053
time_elpased: 3.304
batch start
#iterations: 155
currently lose_sum: 97.73796933889389
time_elpased: 3.149
batch start
#iterations: 156
currently lose_sum: 97.79280418157578
time_elpased: 4.035
batch start
#iterations: 157
currently lose_sum: 97.54911011457443
time_elpased: 4.179
batch start
#iterations: 158
currently lose_sum: 97.71175676584244
time_elpased: 2.989
batch start
#iterations: 159
currently lose_sum: 97.7724404335022
time_elpased: 3.312
start validation test
0.627577319588
0.639124609201
0.589070700834
0.613077705778
0.627644923896
63.568
batch start
#iterations: 160
currently lose_sum: 97.84938502311707
time_elpased: 3.418
batch start
#iterations: 161
currently lose_sum: 97.82453215122223
time_elpased: 2.857
batch start
#iterations: 162
currently lose_sum: 97.55089819431305
time_elpased: 3.3
batch start
#iterations: 163
currently lose_sum: 97.35156887769699
time_elpased: 3.144
batch start
#iterations: 164
currently lose_sum: 97.46385484933853
time_elpased: 2.812
batch start
#iterations: 165
currently lose_sum: 97.73808586597443
time_elpased: 3.009
batch start
#iterations: 166
currently lose_sum: 97.57240068912506
time_elpased: 3.214
batch start
#iterations: 167
currently lose_sum: 97.86863458156586
time_elpased: 3.154
batch start
#iterations: 168
currently lose_sum: 97.74002814292908
time_elpased: 3.606
batch start
#iterations: 169
currently lose_sum: 97.52257585525513
time_elpased: 3.708
batch start
#iterations: 170
currently lose_sum: 97.31528276205063
time_elpased: 3.524
batch start
#iterations: 171
currently lose_sum: 97.6323652267456
time_elpased: 4.034
batch start
#iterations: 172
currently lose_sum: 97.49532800912857
time_elpased: 13.14
batch start
#iterations: 173
currently lose_sum: 97.49637138843536
time_elpased: 3.041
batch start
#iterations: 174
currently lose_sum: 97.5666755437851
time_elpased: 3.29
batch start
#iterations: 175
currently lose_sum: 97.82327342033386
time_elpased: 3.774
batch start
#iterations: 176
currently lose_sum: 97.4056943655014
time_elpased: 4.024
batch start
#iterations: 177
currently lose_sum: 97.61498069763184
time_elpased: 3.441
batch start
#iterations: 178
currently lose_sum: 97.47233712673187
time_elpased: 4.019
batch start
#iterations: 179
currently lose_sum: 97.55284857749939
time_elpased: 3.738
start validation test
0.627783505155
0.63937904847
0.589173613255
0.613250495421
0.627851290775
63.558
batch start
#iterations: 180
currently lose_sum: 97.67400747537613
time_elpased: 3.045
batch start
#iterations: 181
currently lose_sum: 97.58130931854248
time_elpased: 2.542
batch start
#iterations: 182
currently lose_sum: 97.63784164190292
time_elpased: 2.159
batch start
#iterations: 183
currently lose_sum: 97.47255152463913
time_elpased: 2.265
batch start
#iterations: 184
currently lose_sum: 97.48986232280731
time_elpased: 2.673
batch start
#iterations: 185
currently lose_sum: 97.45199996232986
time_elpased: 2.829
batch start
#iterations: 186
currently lose_sum: 97.50118046998978
time_elpased: 3.663
batch start
#iterations: 187
currently lose_sum: 97.55795645713806
time_elpased: 3.71
batch start
#iterations: 188
currently lose_sum: 97.50654238462448
time_elpased: 3.85
batch start
#iterations: 189
currently lose_sum: 97.65137940645218
time_elpased: 3.948
batch start
#iterations: 190
currently lose_sum: 97.46316826343536
time_elpased: 4.568
batch start
#iterations: 191
currently lose_sum: 97.54658365249634
time_elpased: 3.472
batch start
#iterations: 192
currently lose_sum: 97.63198590278625
time_elpased: 3.627
batch start
#iterations: 193
currently lose_sum: 97.42459881305695
time_elpased: 3.021
batch start
#iterations: 194
currently lose_sum: 97.52064222097397
time_elpased: 3.39
batch start
#iterations: 195
currently lose_sum: 97.3159761428833
time_elpased: 3.363
batch start
#iterations: 196
currently lose_sum: 97.30765491724014
time_elpased: 3.147
batch start
#iterations: 197
currently lose_sum: 97.85786598920822
time_elpased: 2.635
batch start
#iterations: 198
currently lose_sum: 97.15302342176437
time_elpased: 2.826
batch start
#iterations: 199
currently lose_sum: 97.26024633646011
time_elpased: 2.717
start validation test
0.623659793814
0.641418871459
0.563857157559
0.600142395531
0.623764786566
63.610
batch start
#iterations: 200
currently lose_sum: 97.26270085573196
time_elpased: 2.655
batch start
#iterations: 201
currently lose_sum: 97.45618206262589
time_elpased: 2.185
batch start
#iterations: 202
currently lose_sum: 97.23526018857956
time_elpased: 2.774
batch start
#iterations: 203
currently lose_sum: 97.55115360021591
time_elpased: 2.353
batch start
#iterations: 204
currently lose_sum: 97.45268160104752
time_elpased: 2.126
batch start
#iterations: 205
currently lose_sum: 97.3561235666275
time_elpased: 2.201
batch start
#iterations: 206
currently lose_sum: 97.48581796884537
time_elpased: 2.306
batch start
#iterations: 207
currently lose_sum: 97.76500767469406
time_elpased: 2.297
batch start
#iterations: 208
currently lose_sum: 97.33471810817719
time_elpased: 2.261
batch start
#iterations: 209
currently lose_sum: 97.20513260364532
time_elpased: 2.374
batch start
#iterations: 210
currently lose_sum: 97.46373373270035
time_elpased: 2.411
batch start
#iterations: 211
currently lose_sum: 97.48439049720764
time_elpased: 2.343
batch start
#iterations: 212
currently lose_sum: 97.44627720117569
time_elpased: 2.292
batch start
#iterations: 213
currently lose_sum: 97.34758561849594
time_elpased: 2.394
batch start
#iterations: 214
currently lose_sum: 97.45325762033463
time_elpased: 2.402
batch start
#iterations: 215
currently lose_sum: 97.14639204740524
time_elpased: 1.97
batch start
#iterations: 216
currently lose_sum: 97.43702167272568
time_elpased: 2.048
batch start
#iterations: 217
currently lose_sum: 97.41595339775085
time_elpased: 2.116
batch start
#iterations: 218
currently lose_sum: 97.42966955900192
time_elpased: 2.298
batch start
#iterations: 219
currently lose_sum: 97.41313642263412
time_elpased: 2.419
start validation test
0.625360824742
0.64513452649
0.560152310384
0.599647460615
0.625475308347
63.620
batch start
#iterations: 220
currently lose_sum: 97.28176921606064
time_elpased: 2.378
batch start
#iterations: 221
currently lose_sum: 97.2895080447197
time_elpased: 2.162
batch start
#iterations: 222
currently lose_sum: 97.43194884061813
time_elpased: 2.313
batch start
#iterations: 223
currently lose_sum: 97.6553402543068
time_elpased: 2.23
batch start
#iterations: 224
currently lose_sum: 97.58514672517776
time_elpased: 2.309
batch start
#iterations: 225
currently lose_sum: 97.4128777384758
time_elpased: 2.311
batch start
#iterations: 226
currently lose_sum: 97.2327099442482
time_elpased: 2.18
batch start
#iterations: 227
currently lose_sum: 97.19280940294266
time_elpased: 2.033
batch start
#iterations: 228
currently lose_sum: 97.4631478190422
time_elpased: 2.123
batch start
#iterations: 229
currently lose_sum: 97.4376431107521
time_elpased: 2.301
batch start
#iterations: 230
currently lose_sum: 97.45744305849075
time_elpased: 2.069
batch start
#iterations: 231
currently lose_sum: 97.35365015268326
time_elpased: 2.006
batch start
#iterations: 232
currently lose_sum: 97.69716227054596
time_elpased: 1.649
batch start
#iterations: 233
currently lose_sum: 97.38468915224075
time_elpased: 2.055
batch start
#iterations: 234
currently lose_sum: 97.12604159116745
time_elpased: 2.128
batch start
#iterations: 235
currently lose_sum: 97.73750251531601
time_elpased: 1.798
batch start
#iterations: 236
currently lose_sum: 97.22878301143646
time_elpased: 1.808
batch start
#iterations: 237
currently lose_sum: 97.37263202667236
time_elpased: 2.008
batch start
#iterations: 238
currently lose_sum: 97.35698682069778
time_elpased: 2.081
batch start
#iterations: 239
currently lose_sum: 97.27404272556305
time_elpased: 2.034
start validation test
0.62881443299
0.640968175706
0.588659051147
0.613700981707
0.628884931956
63.532
batch start
#iterations: 240
currently lose_sum: 97.59611105918884
time_elpased: 1.598
batch start
#iterations: 241
currently lose_sum: 97.73831230401993
time_elpased: 1.517
batch start
#iterations: 242
currently lose_sum: 97.20404309034348
time_elpased: 1.596
batch start
#iterations: 243
currently lose_sum: 97.77079623937607
time_elpased: 1.632
batch start
#iterations: 244
currently lose_sum: 97.6404139995575
time_elpased: 1.743
batch start
#iterations: 245
currently lose_sum: 97.31440657377243
time_elpased: 2.105
batch start
#iterations: 246
currently lose_sum: 97.83877235651016
time_elpased: 1.832
batch start
#iterations: 247
currently lose_sum: 97.19255656003952
time_elpased: 1.542
batch start
#iterations: 248
currently lose_sum: 97.5586467385292
time_elpased: 1.801
batch start
#iterations: 249
currently lose_sum: 97.23737680912018
time_elpased: 1.795
batch start
#iterations: 250
currently lose_sum: 97.09109193086624
time_elpased: 1.59
batch start
#iterations: 251
currently lose_sum: 97.27840220928192
time_elpased: 1.949
batch start
#iterations: 252
currently lose_sum: 97.29677766561508
time_elpased: 1.916
batch start
#iterations: 253
currently lose_sum: 97.45360165834427
time_elpased: 1.841
batch start
#iterations: 254
currently lose_sum: 97.34546399116516
time_elpased: 1.853
batch start
#iterations: 255
currently lose_sum: 97.39357566833496
time_elpased: 1.735
batch start
#iterations: 256
currently lose_sum: 97.07682275772095
time_elpased: 1.796
batch start
#iterations: 257
currently lose_sum: 97.31385987997055
time_elpased: 2.147
batch start
#iterations: 258
currently lose_sum: 97.1069318652153
time_elpased: 2.135
batch start
#iterations: 259
currently lose_sum: 97.30923587083817
time_elpased: 2.077
start validation test
0.625773195876
0.639459643546
0.579705670474
0.608118320199
0.625854074523
63.654
batch start
#iterations: 260
currently lose_sum: 97.18275779485703
time_elpased: 1.565
batch start
#iterations: 261
currently lose_sum: 97.34468227624893
time_elpased: 1.861
batch start
#iterations: 262
currently lose_sum: 97.21251678466797
time_elpased: 1.672
batch start
#iterations: 263
currently lose_sum: 97.28175735473633
time_elpased: 1.722
batch start
#iterations: 264
currently lose_sum: 97.60261297225952
time_elpased: 1.562
batch start
#iterations: 265
currently lose_sum: 97.55853867530823
time_elpased: 1.565
batch start
#iterations: 266
currently lose_sum: 97.28646457195282
time_elpased: 1.579
batch start
#iterations: 267
currently lose_sum: 97.31872177124023
time_elpased: 1.829
batch start
#iterations: 268
currently lose_sum: 97.27059888839722
time_elpased: 1.479
batch start
#iterations: 269
currently lose_sum: 97.69343668222427
time_elpased: 1.475
batch start
#iterations: 270
currently lose_sum: 97.3467767238617
time_elpased: 1.456
batch start
#iterations: 271
currently lose_sum: 97.48911476135254
time_elpased: 1.499
batch start
#iterations: 272
currently lose_sum: 97.3987205028534
time_elpased: 1.624
batch start
#iterations: 273
currently lose_sum: 97.35293185710907
time_elpased: 1.589
batch start
#iterations: 274
currently lose_sum: 97.47628492116928
time_elpased: 1.495
batch start
#iterations: 275
currently lose_sum: 97.48192363977432
time_elpased: 1.456
batch start
#iterations: 276
currently lose_sum: 97.27564686536789
time_elpased: 1.477
batch start
#iterations: 277
currently lose_sum: 97.7073991894722
time_elpased: 1.511
batch start
#iterations: 278
currently lose_sum: 97.18262529373169
time_elpased: 1.469
batch start
#iterations: 279
currently lose_sum: 97.319773375988
time_elpased: 1.587
start validation test
0.620773195876
0.642202940468
0.54842029433
0.591618096031
0.620900222555
63.775
batch start
#iterations: 280
currently lose_sum: 97.55031329393387
time_elpased: 1.503
batch start
#iterations: 281
currently lose_sum: 97.04494541883469
time_elpased: 1.776
batch start
#iterations: 282
currently lose_sum: 97.35215592384338
time_elpased: 1.548
batch start
#iterations: 283
currently lose_sum: 97.2960067987442
time_elpased: 1.495
batch start
#iterations: 284
currently lose_sum: 97.31248080730438
time_elpased: 1.492
batch start
#iterations: 285
currently lose_sum: 97.18929845094681
time_elpased: 1.48
batch start
#iterations: 286
currently lose_sum: 97.39892268180847
time_elpased: 1.484
batch start
#iterations: 287
currently lose_sum: 97.10738390684128
time_elpased: 1.558
batch start
#iterations: 288
currently lose_sum: 97.27778643369675
time_elpased: 1.509
batch start
#iterations: 289
currently lose_sum: 97.24125927686691
time_elpased: 1.482
batch start
#iterations: 290
currently lose_sum: 97.23431551456451
time_elpased: 1.532
batch start
#iterations: 291
currently lose_sum: 97.11602067947388
time_elpased: 1.476
batch start
#iterations: 292
currently lose_sum: 97.35779798030853
time_elpased: 1.477
batch start
#iterations: 293
currently lose_sum: 97.30820482969284
time_elpased: 1.482
batch start
#iterations: 294
currently lose_sum: 97.2001713514328
time_elpased: 1.499
batch start
#iterations: 295
currently lose_sum: 97.1192438006401
time_elpased: 1.49
batch start
#iterations: 296
currently lose_sum: 97.12470239400864
time_elpased: 1.433
batch start
#iterations: 297
currently lose_sum: 97.68321496248245
time_elpased: 1.438
batch start
#iterations: 298
currently lose_sum: 97.21945238113403
time_elpased: 1.427
batch start
#iterations: 299
currently lose_sum: 97.20582604408264
time_elpased: 1.426
start validation test
0.631082474227
0.630372784681
0.636924976845
0.633631942667
0.631072216812
63.239
batch start
#iterations: 300
currently lose_sum: 97.00900161266327
time_elpased: 1.421
batch start
#iterations: 301
currently lose_sum: 97.25540614128113
time_elpased: 1.453
batch start
#iterations: 302
currently lose_sum: 97.48180550336838
time_elpased: 1.417
batch start
#iterations: 303
currently lose_sum: 97.1073694229126
time_elpased: 1.412
batch start
#iterations: 304
currently lose_sum: 97.22555983066559
time_elpased: 1.43
batch start
#iterations: 305
currently lose_sum: 97.26702708005905
time_elpased: 1.432
batch start
#iterations: 306
currently lose_sum: 97.09079420566559
time_elpased: 1.458
batch start
#iterations: 307
currently lose_sum: 97.00676000118256
time_elpased: 1.421
batch start
#iterations: 308
currently lose_sum: 97.5648455619812
time_elpased: 1.431
batch start
#iterations: 309
currently lose_sum: 97.31731450557709
time_elpased: 1.436
batch start
#iterations: 310
currently lose_sum: 97.29224854707718
time_elpased: 1.424
batch start
#iterations: 311
currently lose_sum: 97.19702613353729
time_elpased: 1.438
batch start
#iterations: 312
currently lose_sum: 97.33519572019577
time_elpased: 1.468
batch start
#iterations: 313
currently lose_sum: 97.17511665821075
time_elpased: 1.471
batch start
#iterations: 314
currently lose_sum: 97.10350847244263
time_elpased: 1.418
batch start
#iterations: 315
currently lose_sum: 97.13095462322235
time_elpased: 1.404
batch start
#iterations: 316
currently lose_sum: 96.83438175916672
time_elpased: 1.405
batch start
#iterations: 317
currently lose_sum: 97.23227459192276
time_elpased: 1.445
batch start
#iterations: 318
currently lose_sum: 97.32758450508118
time_elpased: 1.41
batch start
#iterations: 319
currently lose_sum: 97.23254173994064
time_elpased: 1.455
start validation test
0.62706185567
0.633097383097
0.607492024287
0.620030460585
0.627096213528
63.470
batch start
#iterations: 320
currently lose_sum: 97.30007708072662
time_elpased: 1.415
batch start
#iterations: 321
currently lose_sum: 96.99365770816803
time_elpased: 1.485
batch start
#iterations: 322
currently lose_sum: 97.32709527015686
time_elpased: 1.404
batch start
#iterations: 323
currently lose_sum: 96.76844549179077
time_elpased: 1.465
batch start
#iterations: 324
currently lose_sum: 97.37222826480865
time_elpased: 1.437
batch start
#iterations: 325
currently lose_sum: 97.31943529844284
time_elpased: 1.452
batch start
#iterations: 326
currently lose_sum: 97.46361488103867
time_elpased: 1.413
batch start
#iterations: 327
currently lose_sum: 97.05124461650848
time_elpased: 1.487
batch start
#iterations: 328
currently lose_sum: 97.25250059366226
time_elpased: 1.427
batch start
#iterations: 329
currently lose_sum: 97.16371983289719
time_elpased: 1.412
batch start
#iterations: 330
currently lose_sum: 97.06292468309402
time_elpased: 1.427
batch start
#iterations: 331
currently lose_sum: 97.13667142391205
time_elpased: 1.419
batch start
#iterations: 332
currently lose_sum: 97.29297512769699
time_elpased: 1.399
batch start
#iterations: 333
currently lose_sum: 97.30044972896576
time_elpased: 1.43
batch start
#iterations: 334
currently lose_sum: 97.47654008865356
time_elpased: 1.425
batch start
#iterations: 335
currently lose_sum: 97.11649721860886
time_elpased: 1.411
batch start
#iterations: 336
currently lose_sum: 97.37798923254013
time_elpased: 1.465
batch start
#iterations: 337
currently lose_sum: 97.21544402837753
time_elpased: 1.42
batch start
#iterations: 338
currently lose_sum: 97.27411538362503
time_elpased: 1.428
batch start
#iterations: 339
currently lose_sum: 97.015634059906
time_elpased: 1.467
start validation test
0.630773195876
0.637252794497
0.610167747247
0.623416224173
0.63080937192
63.406
batch start
#iterations: 340
currently lose_sum: 97.14636844396591
time_elpased: 1.43
batch start
#iterations: 341
currently lose_sum: 97.65446388721466
time_elpased: 1.414
batch start
#iterations: 342
currently lose_sum: 97.18604189157486
time_elpased: 1.441
batch start
#iterations: 343
currently lose_sum: 97.24697715044022
time_elpased: 1.446
batch start
#iterations: 344
currently lose_sum: 97.24624383449554
time_elpased: 1.422
batch start
#iterations: 345
currently lose_sum: 97.36147719621658
time_elpased: 1.432
batch start
#iterations: 346
currently lose_sum: 97.18158853054047
time_elpased: 1.473
batch start
#iterations: 347
currently lose_sum: 96.8273708820343
time_elpased: 1.417
batch start
#iterations: 348
currently lose_sum: 97.57030916213989
time_elpased: 1.437
batch start
#iterations: 349
currently lose_sum: 97.24573999643326
time_elpased: 1.422
batch start
#iterations: 350
currently lose_sum: 97.22899752855301
time_elpased: 1.426
batch start
#iterations: 351
currently lose_sum: 97.31854009628296
time_elpased: 1.42
batch start
#iterations: 352
currently lose_sum: 97.04638236761093
time_elpased: 1.44
batch start
#iterations: 353
currently lose_sum: 97.1752188205719
time_elpased: 1.432
batch start
#iterations: 354
currently lose_sum: 97.19208645820618
time_elpased: 1.453
batch start
#iterations: 355
currently lose_sum: 97.00341212749481
time_elpased: 1.42
batch start
#iterations: 356
currently lose_sum: 97.21930813789368
time_elpased: 1.459
batch start
#iterations: 357
currently lose_sum: 97.29543727636337
time_elpased: 1.427
batch start
#iterations: 358
currently lose_sum: 97.1847163438797
time_elpased: 1.477
batch start
#iterations: 359
currently lose_sum: 97.23420476913452
time_elpased: 1.416
start validation test
0.622319587629
0.638118354138
0.568179479263
0.601121454625
0.62241463894
63.593
batch start
#iterations: 360
currently lose_sum: 97.1870629787445
time_elpased: 1.446
batch start
#iterations: 361
currently lose_sum: 97.39701426029205
time_elpased: 1.435
batch start
#iterations: 362
currently lose_sum: 97.43542224168777
time_elpased: 1.454
batch start
#iterations: 363
currently lose_sum: 97.21527642011642
time_elpased: 1.427
batch start
#iterations: 364
currently lose_sum: 96.95359736680984
time_elpased: 1.436
batch start
#iterations: 365
currently lose_sum: 97.18335235118866
time_elpased: 1.431
batch start
#iterations: 366
currently lose_sum: 97.25686752796173
time_elpased: 1.433
batch start
#iterations: 367
currently lose_sum: 97.19261753559113
time_elpased: 1.416
batch start
#iterations: 368
currently lose_sum: 97.306192278862
time_elpased: 1.421
batch start
#iterations: 369
currently lose_sum: 97.35508531332016
time_elpased: 1.424
batch start
#iterations: 370
currently lose_sum: 97.16231709718704
time_elpased: 1.43
batch start
#iterations: 371
currently lose_sum: 97.11965638399124
time_elpased: 1.43
batch start
#iterations: 372
currently lose_sum: 97.13963168859482
time_elpased: 1.427
batch start
#iterations: 373
currently lose_sum: 97.23073905706406
time_elpased: 1.459
batch start
#iterations: 374
currently lose_sum: 97.13029712438583
time_elpased: 1.459
batch start
#iterations: 375
currently lose_sum: 96.94467616081238
time_elpased: 1.416
batch start
#iterations: 376
currently lose_sum: 97.02202993631363
time_elpased: 1.463
batch start
#iterations: 377
currently lose_sum: 97.3088590502739
time_elpased: 1.431
batch start
#iterations: 378
currently lose_sum: 97.19184827804565
time_elpased: 1.49
batch start
#iterations: 379
currently lose_sum: 97.4902794957161
time_elpased: 1.423
start validation test
0.63
0.635673827081
0.612123083256
0.623676208451
0.630031385685
63.491
batch start
#iterations: 380
currently lose_sum: 97.32339692115784
time_elpased: 1.418
batch start
#iterations: 381
currently lose_sum: 97.07868903875351
time_elpased: 1.413
batch start
#iterations: 382
currently lose_sum: 96.9300234913826
time_elpased: 1.453
batch start
#iterations: 383
currently lose_sum: 97.0086042881012
time_elpased: 1.414
batch start
#iterations: 384
currently lose_sum: 97.07904243469238
time_elpased: 1.459
batch start
#iterations: 385
currently lose_sum: 97.691919028759
time_elpased: 1.419
batch start
#iterations: 386
currently lose_sum: 97.32747483253479
time_elpased: 1.413
batch start
#iterations: 387
currently lose_sum: 97.09226393699646
time_elpased: 1.416
batch start
#iterations: 388
currently lose_sum: 97.31269747018814
time_elpased: 1.445
batch start
#iterations: 389
currently lose_sum: 97.0070630311966
time_elpased: 1.429
batch start
#iterations: 390
currently lose_sum: 97.32522529363632
time_elpased: 1.438
batch start
#iterations: 391
currently lose_sum: 97.39241307973862
time_elpased: 1.44
batch start
#iterations: 392
currently lose_sum: 97.21066671609879
time_elpased: 1.448
batch start
#iterations: 393
currently lose_sum: 97.20214211940765
time_elpased: 1.433
batch start
#iterations: 394
currently lose_sum: 96.99070173501968
time_elpased: 1.427
batch start
#iterations: 395
currently lose_sum: 96.83251041173935
time_elpased: 1.414
batch start
#iterations: 396
currently lose_sum: 97.18707686662674
time_elpased: 1.453
batch start
#iterations: 397
currently lose_sum: 97.42690420150757
time_elpased: 1.429
batch start
#iterations: 398
currently lose_sum: 97.11410766839981
time_elpased: 1.42
batch start
#iterations: 399
currently lose_sum: 97.03425443172455
time_elpased: 1.433
start validation test
0.619793814433
0.643250520132
0.540907687558
0.587656529517
0.619932311196
63.710
acc: 0.634
pre: 0.633
rec: 0.641
F1: 0.637
auc: 0.668
