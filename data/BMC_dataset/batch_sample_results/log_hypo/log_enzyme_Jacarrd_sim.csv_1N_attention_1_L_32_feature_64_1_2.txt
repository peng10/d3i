start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.55286407470703
time_elpased: 2.416
batch start
#iterations: 1
currently lose_sum: 100.33699029684067
time_elpased: 2.533
batch start
#iterations: 2
currently lose_sum: 100.17277407646179
time_elpased: 2.4
batch start
#iterations: 3
currently lose_sum: 99.8982458114624
time_elpased: 2.594
batch start
#iterations: 4
currently lose_sum: 99.64231407642365
time_elpased: 2.613
batch start
#iterations: 5
currently lose_sum: 99.64839166402817
time_elpased: 2.616
batch start
#iterations: 6
currently lose_sum: 99.44614803791046
time_elpased: 2.269
batch start
#iterations: 7
currently lose_sum: 99.24267810583115
time_elpased: 2.518
batch start
#iterations: 8
currently lose_sum: 99.3418202996254
time_elpased: 2.58
batch start
#iterations: 9
currently lose_sum: 99.20674872398376
time_elpased: 2.4
batch start
#iterations: 10
currently lose_sum: 99.10611635446548
time_elpased: 2.421
batch start
#iterations: 11
currently lose_sum: 98.98371517658234
time_elpased: 2.627
batch start
#iterations: 12
currently lose_sum: 99.26231145858765
time_elpased: 2.7
batch start
#iterations: 13
currently lose_sum: 98.88254463672638
time_elpased: 2.277
batch start
#iterations: 14
currently lose_sum: 99.02757620811462
time_elpased: 2.567
batch start
#iterations: 15
currently lose_sum: 98.94191735982895
time_elpased: 2.459
batch start
#iterations: 16
currently lose_sum: 98.69560468196869
time_elpased: 2.567
batch start
#iterations: 17
currently lose_sum: 98.85631453990936
time_elpased: 2.524
batch start
#iterations: 18
currently lose_sum: 98.65187788009644
time_elpased: 2.17
batch start
#iterations: 19
currently lose_sum: 98.74544268846512
time_elpased: 2.563
start validation test
0.612319587629
0.636397515528
0.527220335494
0.576687116564
0.612468992492
64.570
batch start
#iterations: 20
currently lose_sum: 98.55422335863113
time_elpased: 2.482
batch start
#iterations: 21
currently lose_sum: 98.63415789604187
time_elpased: 2.495
batch start
#iterations: 22
currently lose_sum: 98.37817019224167
time_elpased: 2.619
batch start
#iterations: 23
currently lose_sum: 98.49036264419556
time_elpased: 2.483
batch start
#iterations: 24
currently lose_sum: 98.44468259811401
time_elpased: 2.562
batch start
#iterations: 25
currently lose_sum: 98.63430106639862
time_elpased: 2.636
batch start
#iterations: 26
currently lose_sum: 98.50721424818039
time_elpased: 2.524
batch start
#iterations: 27
currently lose_sum: 98.59745532274246
time_elpased: 2.568
batch start
#iterations: 28
currently lose_sum: 98.37881368398666
time_elpased: 2.648
batch start
#iterations: 29
currently lose_sum: 98.52793973684311
time_elpased: 2.611
batch start
#iterations: 30
currently lose_sum: 98.34290379285812
time_elpased: 2.489
batch start
#iterations: 31
currently lose_sum: 98.24372333288193
time_elpased: 2.469
batch start
#iterations: 32
currently lose_sum: 98.33784818649292
time_elpased: 2.494
batch start
#iterations: 33
currently lose_sum: 98.57968682050705
time_elpased: 2.556
batch start
#iterations: 34
currently lose_sum: 98.18974334001541
time_elpased: 2.58
batch start
#iterations: 35
currently lose_sum: 98.64711308479309
time_elpased: 2.555
batch start
#iterations: 36
currently lose_sum: 98.1698786020279
time_elpased: 2.472
batch start
#iterations: 37
currently lose_sum: 98.21024090051651
time_elpased: 2.576
batch start
#iterations: 38
currently lose_sum: 98.15495711565018
time_elpased: 2.557
batch start
#iterations: 39
currently lose_sum: 98.26052004098892
time_elpased: 2.626
start validation test
0.621340206186
0.632177500279
0.583513430071
0.606871454565
0.621406616925
64.039
batch start
#iterations: 40
currently lose_sum: 98.12179356813431
time_elpased: 2.466
batch start
#iterations: 41
currently lose_sum: 98.14403301477432
time_elpased: 2.3
batch start
#iterations: 42
currently lose_sum: 98.24016183614731
time_elpased: 2.656
batch start
#iterations: 43
currently lose_sum: 98.17624682188034
time_elpased: 2.594
batch start
#iterations: 44
currently lose_sum: 98.24446392059326
time_elpased: 2.644
batch start
#iterations: 45
currently lose_sum: 98.26490730047226
time_elpased: 2.641
batch start
#iterations: 46
currently lose_sum: 98.0468293428421
time_elpased: 2.614
batch start
#iterations: 47
currently lose_sum: 98.10141146183014
time_elpased: 2.6
batch start
#iterations: 48
currently lose_sum: 98.19720584154129
time_elpased: 2.548
batch start
#iterations: 49
currently lose_sum: 98.29137188196182
time_elpased: 2.511
batch start
#iterations: 50
currently lose_sum: 97.96902078390121
time_elpased: 2.406
batch start
#iterations: 51
currently lose_sum: 97.99332743883133
time_elpased: 2.311
batch start
#iterations: 52
currently lose_sum: 98.06481891870499
time_elpased: 2.646
batch start
#iterations: 53
currently lose_sum: 97.92106646299362
time_elpased: 2.801
batch start
#iterations: 54
currently lose_sum: 97.94286972284317
time_elpased: 2.73
batch start
#iterations: 55
currently lose_sum: 98.12420696020126
time_elpased: 2.69
batch start
#iterations: 56
currently lose_sum: 98.11782157421112
time_elpased: 2.57
batch start
#iterations: 57
currently lose_sum: 98.21556907892227
time_elpased: 2.433
batch start
#iterations: 58
currently lose_sum: 97.90623992681503
time_elpased: 2.519
batch start
#iterations: 59
currently lose_sum: 98.116868019104
time_elpased: 2.579
start validation test
0.622319587629
0.633878557024
0.582278481013
0.60698385453
0.622389885967
63.761
batch start
#iterations: 60
currently lose_sum: 97.88313579559326
time_elpased: 2.388
batch start
#iterations: 61
currently lose_sum: 98.12485009431839
time_elpased: 2.657
batch start
#iterations: 62
currently lose_sum: 98.14731746912003
time_elpased: 2.585
batch start
#iterations: 63
currently lose_sum: 98.11045700311661
time_elpased: 2.51
batch start
#iterations: 64
currently lose_sum: 97.90815126895905
time_elpased: 2.378
batch start
#iterations: 65
currently lose_sum: 97.80050593614578
time_elpased: 2.608
batch start
#iterations: 66
currently lose_sum: 97.95934647321701
time_elpased: 2.52
batch start
#iterations: 67
currently lose_sum: 97.7812927365303
time_elpased: 2.702
batch start
#iterations: 68
currently lose_sum: 98.18172919750214
time_elpased: 2.587
batch start
#iterations: 69
currently lose_sum: 97.76982539892197
time_elpased: 2.58
batch start
#iterations: 70
currently lose_sum: 97.9029016494751
time_elpased: 1.994
batch start
#iterations: 71
currently lose_sum: 97.9901237487793
time_elpased: 2.587
batch start
#iterations: 72
currently lose_sum: 97.88946652412415
time_elpased: 2.557
batch start
#iterations: 73
currently lose_sum: 97.89265674352646
time_elpased: 2.549
batch start
#iterations: 74
currently lose_sum: 98.15090727806091
time_elpased: 2.476
batch start
#iterations: 75
currently lose_sum: 98.02898240089417
time_elpased: 2.598
batch start
#iterations: 76
currently lose_sum: 97.91474378108978
time_elpased: 2.611
batch start
#iterations: 77
currently lose_sum: 98.10009723901749
time_elpased: 2.528
batch start
#iterations: 78
currently lose_sum: 98.02670174837112
time_elpased: 2.391
batch start
#iterations: 79
currently lose_sum: 97.88435918092728
time_elpased: 2.472
start validation test
0.625773195876
0.63730859506
0.58680662756
0.611015859408
0.625841607697
63.568
batch start
#iterations: 80
currently lose_sum: 98.08734279870987
time_elpased: 2.532
batch start
#iterations: 81
currently lose_sum: 97.74629878997803
time_elpased: 2.243
batch start
#iterations: 82
currently lose_sum: 97.90266925096512
time_elpased: 2.276
batch start
#iterations: 83
currently lose_sum: 97.90962612628937
time_elpased: 2.684
batch start
#iterations: 84
currently lose_sum: 97.91781461238861
time_elpased: 2.511
batch start
#iterations: 85
currently lose_sum: 97.90282648801804
time_elpased: 2.665
batch start
#iterations: 86
currently lose_sum: 97.65385341644287
time_elpased: 2.358
batch start
#iterations: 87
currently lose_sum: 97.81294560432434
time_elpased: 2.609
batch start
#iterations: 88
currently lose_sum: 97.71738559007645
time_elpased: 2.447
batch start
#iterations: 89
currently lose_sum: 97.90463000535965
time_elpased: 2.622
batch start
#iterations: 90
currently lose_sum: 97.95366472005844
time_elpased: 2.631
batch start
#iterations: 91
currently lose_sum: 97.93579411506653
time_elpased: 2.575
batch start
#iterations: 92
currently lose_sum: 97.80962538719177
time_elpased: 2.045
batch start
#iterations: 93
currently lose_sum: 97.9476033449173
time_elpased: 2.647
batch start
#iterations: 94
currently lose_sum: 97.92498296499252
time_elpased: 2.511
batch start
#iterations: 95
currently lose_sum: 97.54695928096771
time_elpased: 2.121
batch start
#iterations: 96
currently lose_sum: 97.86957705020905
time_elpased: 2.518
batch start
#iterations: 97
currently lose_sum: 97.74331319332123
time_elpased: 2.374
batch start
#iterations: 98
currently lose_sum: 97.90707260370255
time_elpased: 2.651
batch start
#iterations: 99
currently lose_sum: 97.52638030052185
time_elpased: 2.339
start validation test
0.626804123711
0.624936951478
0.637542451374
0.63117677025
0.626785270921
63.391
batch start
#iterations: 100
currently lose_sum: 98.09071439504623
time_elpased: 2.561
batch start
#iterations: 101
currently lose_sum: 97.77649676799774
time_elpased: 2.624
batch start
#iterations: 102
currently lose_sum: 97.82885891199112
time_elpased: 2.58
batch start
#iterations: 103
currently lose_sum: 97.81555271148682
time_elpased: 2.303
batch start
#iterations: 104
currently lose_sum: 97.769710958004
time_elpased: 2.466
batch start
#iterations: 105
currently lose_sum: 97.70531636476517
time_elpased: 2.247
batch start
#iterations: 106
currently lose_sum: 97.67088866233826
time_elpased: 2.499
batch start
#iterations: 107
currently lose_sum: 97.88536596298218
time_elpased: 2.403
batch start
#iterations: 108
currently lose_sum: 97.93647021055222
time_elpased: 2.481
batch start
#iterations: 109
currently lose_sum: 97.73710882663727
time_elpased: 2.558
batch start
#iterations: 110
currently lose_sum: 97.75085002183914
time_elpased: 2.643
batch start
#iterations: 111
currently lose_sum: 97.44453275203705
time_elpased: 2.634
batch start
#iterations: 112
currently lose_sum: 97.59464180469513
time_elpased: 2.559
batch start
#iterations: 113
currently lose_sum: 97.63285833597183
time_elpased: 2.509
batch start
#iterations: 114
currently lose_sum: 97.60081696510315
time_elpased: 2.346
batch start
#iterations: 115
currently lose_sum: 97.74334168434143
time_elpased: 2.532
batch start
#iterations: 116
currently lose_sum: 97.4352091550827
time_elpased: 2.606
batch start
#iterations: 117
currently lose_sum: 97.58513098955154
time_elpased: 2.451
batch start
#iterations: 118
currently lose_sum: 97.51685827970505
time_elpased: 2.378
batch start
#iterations: 119
currently lose_sum: 97.60058528184891
time_elpased: 2.641
start validation test
0.623556701031
0.640446823365
0.566429968097
0.601168696412
0.623656995822
63.515
batch start
#iterations: 120
currently lose_sum: 97.84931296110153
time_elpased: 2.391
batch start
#iterations: 121
currently lose_sum: 97.59253531694412
time_elpased: 2.513
batch start
#iterations: 122
currently lose_sum: 97.60150676965714
time_elpased: 2.571
batch start
#iterations: 123
currently lose_sum: 97.82707816362381
time_elpased: 2.201
batch start
#iterations: 124
currently lose_sum: 97.73592406511307
time_elpased: 2.505
batch start
#iterations: 125
currently lose_sum: 97.64022582769394
time_elpased: 2.638
batch start
#iterations: 126
currently lose_sum: 98.07302266359329
time_elpased: 2.582
batch start
#iterations: 127
currently lose_sum: 97.69514012336731
time_elpased: 2.494
batch start
#iterations: 128
currently lose_sum: 97.31144452095032
time_elpased: 2.304
batch start
#iterations: 129
currently lose_sum: 97.87036103010178
time_elpased: 2.444
batch start
#iterations: 130
currently lose_sum: 97.54205930233002
time_elpased: 2.603
batch start
#iterations: 131
currently lose_sum: 97.97473847866058
time_elpased: 2.644
batch start
#iterations: 132
currently lose_sum: 97.72877848148346
time_elpased: 2.397
batch start
#iterations: 133
currently lose_sum: 97.6451513171196
time_elpased: 2.557
batch start
#iterations: 134
currently lose_sum: 97.63565200567245
time_elpased: 2.057
batch start
#iterations: 135
currently lose_sum: 97.58082282543182
time_elpased: 2.569
batch start
#iterations: 136
currently lose_sum: 97.59117257595062
time_elpased: 2.398
batch start
#iterations: 137
currently lose_sum: 97.39472562074661
time_elpased: 2.546
batch start
#iterations: 138
currently lose_sum: 97.50206047296524
time_elpased: 2.73
batch start
#iterations: 139
currently lose_sum: 97.52395737171173
time_elpased: 2.538
start validation test
0.630103092784
0.638564728978
0.602552228054
0.620036005507
0.630151462576
63.390
batch start
#iterations: 140
currently lose_sum: 97.60628199577332
time_elpased: 2.702
batch start
#iterations: 141
currently lose_sum: 97.72691315412521
time_elpased: 2.929
batch start
#iterations: 142
currently lose_sum: 97.68887674808502
time_elpased: 2.742
batch start
#iterations: 143
currently lose_sum: 97.65223330259323
time_elpased: 2.608
batch start
#iterations: 144
currently lose_sum: 97.75536113977432
time_elpased: 2.353
batch start
#iterations: 145
currently lose_sum: 97.65489482879639
time_elpased: 2.515
batch start
#iterations: 146
currently lose_sum: 97.36852067708969
time_elpased: 2.446
batch start
#iterations: 147
currently lose_sum: 97.5956392288208
time_elpased: 2.471
batch start
#iterations: 148
currently lose_sum: 97.48829561471939
time_elpased: 2.632
batch start
#iterations: 149
currently lose_sum: 97.57873117923737
time_elpased: 2.608
batch start
#iterations: 150
currently lose_sum: 97.56728756427765
time_elpased: 2.52
batch start
#iterations: 151
currently lose_sum: 97.4842426776886
time_elpased: 2.661
batch start
#iterations: 152
currently lose_sum: 97.43655514717102
time_elpased: 2.667
batch start
#iterations: 153
currently lose_sum: 97.84088790416718
time_elpased: 2.54
batch start
#iterations: 154
currently lose_sum: 97.59415078163147
time_elpased: 2.256
batch start
#iterations: 155
currently lose_sum: 97.59337067604065
time_elpased: 2.443
batch start
#iterations: 156
currently lose_sum: 97.58451819419861
time_elpased: 2.475
batch start
#iterations: 157
currently lose_sum: 97.26659965515137
time_elpased: 2.687
batch start
#iterations: 158
currently lose_sum: 97.63355761766434
time_elpased: 2.336
batch start
#iterations: 159
currently lose_sum: 97.46197831630707
time_elpased: 2.549
start validation test
0.625206185567
0.645283915419
0.559020273747
0.599062586159
0.625322385144
63.606
batch start
#iterations: 160
currently lose_sum: 97.37615621089935
time_elpased: 2.649
batch start
#iterations: 161
currently lose_sum: 97.80566132068634
time_elpased: 2.595
batch start
#iterations: 162
currently lose_sum: 97.69308757781982
time_elpased: 2.554
batch start
#iterations: 163
currently lose_sum: 97.86248284578323
time_elpased: 2.543
batch start
#iterations: 164
currently lose_sum: 97.25930297374725
time_elpased: 2.526
batch start
#iterations: 165
currently lose_sum: 97.56590592861176
time_elpased: 2.726
batch start
#iterations: 166
currently lose_sum: 97.4902235865593
time_elpased: 2.591
batch start
#iterations: 167
currently lose_sum: 97.40448087453842
time_elpased: 2.427
batch start
#iterations: 168
currently lose_sum: 97.41285014152527
time_elpased: 2.311
batch start
#iterations: 169
currently lose_sum: 97.55347949266434
time_elpased: 2.428
batch start
#iterations: 170
currently lose_sum: 97.6762216091156
time_elpased: 2.476
batch start
#iterations: 171
currently lose_sum: 97.61413168907166
time_elpased: 2.717
batch start
#iterations: 172
currently lose_sum: 97.75112360715866
time_elpased: 2.17
batch start
#iterations: 173
currently lose_sum: 97.87947082519531
time_elpased: 2.523
batch start
#iterations: 174
currently lose_sum: 97.18570452928543
time_elpased: 2.525
batch start
#iterations: 175
currently lose_sum: 97.38237768411636
time_elpased: 2.653
batch start
#iterations: 176
currently lose_sum: 97.28057211637497
time_elpased: 2.722
batch start
#iterations: 177
currently lose_sum: 97.54759126901627
time_elpased: 2.373
batch start
#iterations: 178
currently lose_sum: 97.6703879237175
time_elpased: 2.546
batch start
#iterations: 179
currently lose_sum: 97.57919210195541
time_elpased: 2.452
start validation test
0.627731958763
0.64021580308
0.586189153031
0.612012463737
0.627804893566
63.434
batch start
#iterations: 180
currently lose_sum: 97.5957281589508
time_elpased: 2.734
batch start
#iterations: 181
currently lose_sum: 97.52343487739563
time_elpased: 2.409
batch start
#iterations: 182
currently lose_sum: 97.58742195367813
time_elpased: 2.716
batch start
#iterations: 183
currently lose_sum: 97.57685840129852
time_elpased: 2.636
batch start
#iterations: 184
currently lose_sum: 97.39884430170059
time_elpased: 2.723
batch start
#iterations: 185
currently lose_sum: 97.4758951663971
time_elpased: 2.559
batch start
#iterations: 186
currently lose_sum: 97.71977317333221
time_elpased: 2.576
batch start
#iterations: 187
currently lose_sum: 97.45345950126648
time_elpased: 2.47
batch start
#iterations: 188
currently lose_sum: 97.63411259651184
time_elpased: 2.473
batch start
#iterations: 189
currently lose_sum: 97.64002460241318
time_elpased: 2.628
batch start
#iterations: 190
currently lose_sum: 97.3410621881485
time_elpased: 2.614
batch start
#iterations: 191
currently lose_sum: 97.33974224328995
time_elpased: 2.432
batch start
#iterations: 192
currently lose_sum: 97.46864318847656
time_elpased: 2.658
batch start
#iterations: 193
currently lose_sum: 97.44876313209534
time_elpased: 2.587
batch start
#iterations: 194
currently lose_sum: 97.18659341335297
time_elpased: 2.784
batch start
#iterations: 195
currently lose_sum: 97.49198788404465
time_elpased: 2.463
batch start
#iterations: 196
currently lose_sum: 97.45288932323456
time_elpased: 2.385
batch start
#iterations: 197
currently lose_sum: 97.62235748767853
time_elpased: 2.636
batch start
#iterations: 198
currently lose_sum: 97.62497133016586
time_elpased: 2.654
batch start
#iterations: 199
currently lose_sum: 97.21806687116623
time_elpased: 2.729
start validation test
0.628092783505
0.632268978642
0.615416280745
0.623728813559
0.628115039061
63.224
batch start
#iterations: 200
currently lose_sum: 97.46542525291443
time_elpased: 2.389
batch start
#iterations: 201
currently lose_sum: 97.47069835662842
time_elpased: 2.577
batch start
#iterations: 202
currently lose_sum: 97.09085619449615
time_elpased: 2.35
batch start
#iterations: 203
currently lose_sum: 97.29128831624985
time_elpased: 2.547
batch start
#iterations: 204
currently lose_sum: 97.25650161504745
time_elpased: 2.529
batch start
#iterations: 205
currently lose_sum: 97.10690099000931
time_elpased: 2.663
batch start
#iterations: 206
currently lose_sum: 97.34670436382294
time_elpased: 2.391
batch start
#iterations: 207
currently lose_sum: 97.40639811754227
time_elpased: 2.562
batch start
#iterations: 208
currently lose_sum: 97.22352474927902
time_elpased: 2.521
batch start
#iterations: 209
currently lose_sum: 97.42516350746155
time_elpased: 2.499
batch start
#iterations: 210
currently lose_sum: 97.09335905313492
time_elpased: 2.586
batch start
#iterations: 211
currently lose_sum: 97.64210218191147
time_elpased: 2.55
batch start
#iterations: 212
currently lose_sum: 97.41710466146469
time_elpased: 2.58
batch start
#iterations: 213
currently lose_sum: 97.34851372241974
time_elpased: 2.591
batch start
#iterations: 214
currently lose_sum: 97.4439355134964
time_elpased: 2.559
batch start
#iterations: 215
currently lose_sum: 97.39870417118073
time_elpased: 2.566
batch start
#iterations: 216
currently lose_sum: 97.212886095047
time_elpased: 2.548
batch start
#iterations: 217
currently lose_sum: 97.38923990726471
time_elpased: 2.585
batch start
#iterations: 218
currently lose_sum: 97.19438230991364
time_elpased: 2.588
batch start
#iterations: 219
currently lose_sum: 97.42750746011734
time_elpased: 2.473
start validation test
0.623711340206
0.642663203872
0.560255222805
0.598636463602
0.623822747208
63.585
batch start
#iterations: 220
currently lose_sum: 97.52451825141907
time_elpased: 2.228
batch start
#iterations: 221
currently lose_sum: 97.43223142623901
time_elpased: 2.457
batch start
#iterations: 222
currently lose_sum: 97.36503791809082
time_elpased: 2.542
batch start
#iterations: 223
currently lose_sum: 97.26471614837646
time_elpased: 2.522
batch start
#iterations: 224
currently lose_sum: 97.28448367118835
time_elpased: 2.5
batch start
#iterations: 225
currently lose_sum: 97.69309848546982
time_elpased: 2.463
batch start
#iterations: 226
currently lose_sum: 97.1477884054184
time_elpased: 2.472
batch start
#iterations: 227
currently lose_sum: 97.59147959947586
time_elpased: 2.299
batch start
#iterations: 228
currently lose_sum: 97.38192003965378
time_elpased: 2.402
batch start
#iterations: 229
currently lose_sum: 97.20290076732635
time_elpased: 2.756
batch start
#iterations: 230
currently lose_sum: 97.39842343330383
time_elpased: 2.88
batch start
#iterations: 231
currently lose_sum: 97.12936490774155
time_elpased: 2.163
batch start
#iterations: 232
currently lose_sum: 97.38151133060455
time_elpased: 2.63
batch start
#iterations: 233
currently lose_sum: 97.59784811735153
time_elpased: 2.711
batch start
#iterations: 234
currently lose_sum: 97.19777941703796
time_elpased: 2.704
batch start
#iterations: 235
currently lose_sum: 97.19961363077164
time_elpased: 2.545
batch start
#iterations: 236
currently lose_sum: 97.4355291724205
time_elpased: 2.669
batch start
#iterations: 237
currently lose_sum: 97.56701570749283
time_elpased: 2.625
batch start
#iterations: 238
currently lose_sum: 97.42403066158295
time_elpased: 2.635
batch start
#iterations: 239
currently lose_sum: 97.08060073852539
time_elpased: 2.454
start validation test
0.628608247423
0.638968798407
0.594319234332
0.615835777126
0.628668447074
63.320
batch start
#iterations: 240
currently lose_sum: 96.94285184144974
time_elpased: 2.501
batch start
#iterations: 241
currently lose_sum: 97.36000919342041
time_elpased: 2.634
batch start
#iterations: 242
currently lose_sum: 97.4166265130043
time_elpased: 2.569
batch start
#iterations: 243
currently lose_sum: 97.34831583499908
time_elpased: 2.526
batch start
#iterations: 244
currently lose_sum: 97.49353075027466
time_elpased: 2.461
batch start
#iterations: 245
currently lose_sum: 97.27976053953171
time_elpased: 2.607
batch start
#iterations: 246
currently lose_sum: 97.36541783809662
time_elpased: 2.606
batch start
#iterations: 247
currently lose_sum: 97.18335723876953
time_elpased: 2.711
batch start
#iterations: 248
currently lose_sum: 97.26094156503677
time_elpased: 2.549
batch start
#iterations: 249
currently lose_sum: 97.32925546169281
time_elpased: 2.546
batch start
#iterations: 250
currently lose_sum: 97.28174197673798
time_elpased: 2.569
batch start
#iterations: 251
currently lose_sum: 97.18210643529892
time_elpased: 2.587
batch start
#iterations: 252
currently lose_sum: 97.08541756868362
time_elpased: 2.554
batch start
#iterations: 253
currently lose_sum: 97.15996199846268
time_elpased: 2.527
batch start
#iterations: 254
currently lose_sum: 97.40911018848419
time_elpased: 2.58
batch start
#iterations: 255
currently lose_sum: 97.46435880661011
time_elpased: 2.621
batch start
#iterations: 256
currently lose_sum: 97.18425393104553
time_elpased: 2.667
batch start
#iterations: 257
currently lose_sum: 97.39840656518936
time_elpased: 2.691
batch start
#iterations: 258
currently lose_sum: 97.34495919942856
time_elpased: 2.579
batch start
#iterations: 259
currently lose_sum: 97.0942372083664
time_elpased: 2.353
start validation test
0.630257731959
0.637216828479
0.607903673973
0.622215199874
0.630296977955
63.106
batch start
#iterations: 260
currently lose_sum: 97.33224874734879
time_elpased: 2.512
batch start
#iterations: 261
currently lose_sum: 96.89118713140488
time_elpased: 2.536
batch start
#iterations: 262
currently lose_sum: 97.12226295471191
time_elpased: 2.454
batch start
#iterations: 263
currently lose_sum: 97.51532101631165
time_elpased: 2.677
batch start
#iterations: 264
currently lose_sum: 97.05443722009659
time_elpased: 2.15
batch start
#iterations: 265
currently lose_sum: 97.41127246618271
time_elpased: 2.408
batch start
#iterations: 266
currently lose_sum: 97.07054960727692
time_elpased: 2.604
batch start
#iterations: 267
currently lose_sum: 97.20809507369995
time_elpased: 2.356
batch start
#iterations: 268
currently lose_sum: 97.13603860139847
time_elpased: 2.598
batch start
#iterations: 269
currently lose_sum: 97.31058597564697
time_elpased: 2.508
batch start
#iterations: 270
currently lose_sum: 97.20890784263611
time_elpased: 2.147
batch start
#iterations: 271
currently lose_sum: 97.34700065851212
time_elpased: 2.522
batch start
#iterations: 272
currently lose_sum: 97.1487889289856
time_elpased: 2.499
batch start
#iterations: 273
currently lose_sum: 97.10934144258499
time_elpased: 2.407
batch start
#iterations: 274
currently lose_sum: 97.285948574543
time_elpased: 2.707
batch start
#iterations: 275
currently lose_sum: 97.50866544246674
time_elpased: 2.674
batch start
#iterations: 276
currently lose_sum: 97.23486810922623
time_elpased: 2.649
batch start
#iterations: 277
currently lose_sum: 97.12144559621811
time_elpased: 2.255
batch start
#iterations: 278
currently lose_sum: 97.365119099617
time_elpased: 2.607
batch start
#iterations: 279
currently lose_sum: 97.18400710821152
time_elpased: 2.299
start validation test
0.630360824742
0.640912109807
0.595862920655
0.61756706309
0.630421391134
63.200
batch start
#iterations: 280
currently lose_sum: 97.45178651809692
time_elpased: 2.321
batch start
#iterations: 281
currently lose_sum: 97.28592079877853
time_elpased: 2.461
batch start
#iterations: 282
currently lose_sum: 97.15576022863388
time_elpased: 2.55
batch start
#iterations: 283
currently lose_sum: 97.38271194696426
time_elpased: 2.418
batch start
#iterations: 284
currently lose_sum: 97.12785410881042
time_elpased: 2.274
batch start
#iterations: 285
currently lose_sum: 97.232974588871
time_elpased: 2.302
batch start
#iterations: 286
currently lose_sum: 97.28517776727676
time_elpased: 2.41
batch start
#iterations: 287
currently lose_sum: 97.16970467567444
time_elpased: 2.377
batch start
#iterations: 288
currently lose_sum: 97.47448426485062
time_elpased: 2.759
batch start
#iterations: 289
currently lose_sum: 96.93580687046051
time_elpased: 3.01
batch start
#iterations: 290
currently lose_sum: 97.26916086673737
time_elpased: 2.664
batch start
#iterations: 291
currently lose_sum: 97.38685762882233
time_elpased: 2.834
batch start
#iterations: 292
currently lose_sum: 97.28137654066086
time_elpased: 2.496
batch start
#iterations: 293
currently lose_sum: 97.1605561375618
time_elpased: 2.854
batch start
#iterations: 294
currently lose_sum: 97.18266862630844
time_elpased: 2.496
batch start
#iterations: 295
currently lose_sum: 97.30824190378189
time_elpased: 2.535
batch start
#iterations: 296
currently lose_sum: 97.24618071317673
time_elpased: 2.57
batch start
#iterations: 297
currently lose_sum: 97.11270034313202
time_elpased: 2.653
batch start
#iterations: 298
currently lose_sum: 97.22743058204651
time_elpased: 2.583
batch start
#iterations: 299
currently lose_sum: 97.30128175020218
time_elpased: 2.636
start validation test
0.629536082474
0.645068807339
0.578882371102
0.610186038943
0.629625012877
63.366
batch start
#iterations: 300
currently lose_sum: 97.1334725022316
time_elpased: 2.267
batch start
#iterations: 301
currently lose_sum: 97.3217945098877
time_elpased: 2.603
batch start
#iterations: 302
currently lose_sum: 97.00863575935364
time_elpased: 2.6
batch start
#iterations: 303
currently lose_sum: 97.25486558675766
time_elpased: 2.52
batch start
#iterations: 304
currently lose_sum: 97.26473104953766
time_elpased: 2.398
batch start
#iterations: 305
currently lose_sum: 97.12847030162811
time_elpased: 2.594
batch start
#iterations: 306
currently lose_sum: 97.16308778524399
time_elpased: 2.367
batch start
#iterations: 307
currently lose_sum: 97.24401330947876
time_elpased: 2.5
batch start
#iterations: 308
currently lose_sum: 96.93341678380966
time_elpased: 2.534
batch start
#iterations: 309
currently lose_sum: 97.0300607085228
time_elpased: 2.672
batch start
#iterations: 310
currently lose_sum: 97.5086544752121
time_elpased: 2.659
batch start
#iterations: 311
currently lose_sum: 97.22287386655807
time_elpased: 2.482
batch start
#iterations: 312
currently lose_sum: 96.99469769001007
time_elpased: 2.384
batch start
#iterations: 313
currently lose_sum: 97.23759418725967
time_elpased: 2.561
batch start
#iterations: 314
currently lose_sum: 97.20816105604172
time_elpased: 2.558
batch start
#iterations: 315
currently lose_sum: 96.9212212562561
time_elpased: 2.62
batch start
#iterations: 316
currently lose_sum: 97.07574951648712
time_elpased: 2.535
batch start
#iterations: 317
currently lose_sum: 97.16426301002502
time_elpased: 2.524
batch start
#iterations: 318
currently lose_sum: 97.15695106983185
time_elpased: 2.505
batch start
#iterations: 319
currently lose_sum: 97.22120362520218
time_elpased: 2.72
start validation test
0.626958762887
0.649217809868
0.55521251415
0.598546624508
0.627084724492
63.539
batch start
#iterations: 320
currently lose_sum: 97.05828964710236
time_elpased: 2.511
batch start
#iterations: 321
currently lose_sum: 97.3445640206337
time_elpased: 2.493
batch start
#iterations: 322
currently lose_sum: 97.13848394155502
time_elpased: 2.479
batch start
#iterations: 323
currently lose_sum: 97.3167953491211
time_elpased: 2.401
batch start
#iterations: 324
currently lose_sum: 97.25248116254807
time_elpased: 2.251
batch start
#iterations: 325
currently lose_sum: 97.39739626646042
time_elpased: 2.549
batch start
#iterations: 326
currently lose_sum: 96.9826927781105
time_elpased: 2.7
batch start
#iterations: 327
currently lose_sum: 97.11599570512772
time_elpased: 2.694
batch start
#iterations: 328
currently lose_sum: 97.22820955514908
time_elpased: 2.659
batch start
#iterations: 329
currently lose_sum: 96.89139819145203
time_elpased: 2.525
batch start
#iterations: 330
currently lose_sum: 97.2646769285202
time_elpased: 2.585
batch start
#iterations: 331
currently lose_sum: 97.32909274101257
time_elpased: 2.796
batch start
#iterations: 332
currently lose_sum: 97.13345718383789
time_elpased: 2.474
batch start
#iterations: 333
currently lose_sum: 97.02327978610992
time_elpased: 2.626
batch start
#iterations: 334
currently lose_sum: 96.93812566995621
time_elpased: 2.418
batch start
#iterations: 335
currently lose_sum: 97.05899000167847
time_elpased: 2.339
batch start
#iterations: 336
currently lose_sum: 97.12150806188583
time_elpased: 2.609
batch start
#iterations: 337
currently lose_sum: 97.45760810375214
time_elpased: 2.542
batch start
#iterations: 338
currently lose_sum: 97.16808766126633
time_elpased: 2.614
batch start
#iterations: 339
currently lose_sum: 97.45080763101578
time_elpased: 2.594
start validation test
0.632164948454
0.635514018692
0.622825975095
0.629106029106
0.632181344462
63.325
batch start
#iterations: 340
currently lose_sum: 97.20082145929337
time_elpased: 2.589
batch start
#iterations: 341
currently lose_sum: 97.19520956277847
time_elpased: 2.661
batch start
#iterations: 342
currently lose_sum: 97.06919002532959
time_elpased: 2.586
batch start
#iterations: 343
currently lose_sum: 97.32951200008392
time_elpased: 2.681
batch start
#iterations: 344
currently lose_sum: 97.17617052793503
time_elpased: 2.673
batch start
#iterations: 345
currently lose_sum: 97.1319472193718
time_elpased: 2.624
batch start
#iterations: 346
currently lose_sum: 97.08633852005005
time_elpased: 2.83
batch start
#iterations: 347
currently lose_sum: 97.37317913770676
time_elpased: 2.833
batch start
#iterations: 348
currently lose_sum: 97.01923072338104
time_elpased: 2.407
batch start
#iterations: 349
currently lose_sum: 97.8656530380249
time_elpased: 2.784
batch start
#iterations: 350
currently lose_sum: 97.08931541442871
time_elpased: 2.552
batch start
#iterations: 351
currently lose_sum: 97.00285595655441
time_elpased: 2.357
batch start
#iterations: 352
currently lose_sum: 97.33393496274948
time_elpased: 2.618
batch start
#iterations: 353
currently lose_sum: 97.20039981603622
time_elpased: 2.535
batch start
#iterations: 354
currently lose_sum: 97.34984791278839
time_elpased: 2.531
batch start
#iterations: 355
currently lose_sum: 96.99782478809357
time_elpased: 2.513
batch start
#iterations: 356
currently lose_sum: 97.33064073324203
time_elpased: 2.608
batch start
#iterations: 357
currently lose_sum: 97.23202139139175
time_elpased: 2.57
batch start
#iterations: 358
currently lose_sum: 97.3631477355957
time_elpased: 2.298
batch start
#iterations: 359
currently lose_sum: 97.07185167074203
time_elpased: 2.486
start validation test
0.629432989691
0.644292237443
0.580837707111
0.610921686421
0.629518306205
63.367
batch start
#iterations: 360
currently lose_sum: 97.32743036746979
time_elpased: 2.628
batch start
#iterations: 361
currently lose_sum: 97.1163718700409
time_elpased: 2.509
batch start
#iterations: 362
currently lose_sum: 97.3064553141594
time_elpased: 2.478
batch start
#iterations: 363
currently lose_sum: 96.86354905366898
time_elpased: 2.532
batch start
#iterations: 364
currently lose_sum: 97.00096076726913
time_elpased: 2.891
batch start
#iterations: 365
currently lose_sum: 97.47921627759933
time_elpased: 2.264
batch start
#iterations: 366
currently lose_sum: 97.08290654420853
time_elpased: 2.402
batch start
#iterations: 367
currently lose_sum: 97.20089918375015
time_elpased: 2.425
batch start
#iterations: 368
currently lose_sum: 97.27838093042374
time_elpased: 2.46
batch start
#iterations: 369
currently lose_sum: 97.19162321090698
time_elpased: 2.462
batch start
#iterations: 370
currently lose_sum: 97.02230936288834
time_elpased: 2.465
batch start
#iterations: 371
currently lose_sum: 97.12998390197754
time_elpased: 2.548
batch start
#iterations: 372
currently lose_sum: 97.03034073114395
time_elpased: 2.542
batch start
#iterations: 373
currently lose_sum: 97.32646524906158
time_elpased: 2.513
batch start
#iterations: 374
currently lose_sum: 97.37273806333542
time_elpased: 2.587
batch start
#iterations: 375
currently lose_sum: 96.7509776353836
time_elpased: 2.464
batch start
#iterations: 376
currently lose_sum: 97.3673974275589
time_elpased: 2.503
batch start
#iterations: 377
currently lose_sum: 97.34448051452637
time_elpased: 2.421
batch start
#iterations: 378
currently lose_sum: 97.14445859193802
time_elpased: 2.447
batch start
#iterations: 379
currently lose_sum: 97.05012542009354
time_elpased: 2.521
start validation test
0.632680412371
0.635640247095
0.624781311104
0.630164002491
0.632694280462
63.149
batch start
#iterations: 380
currently lose_sum: 97.45317161083221
time_elpased: 2.501
batch start
#iterations: 381
currently lose_sum: 97.0692355632782
time_elpased: 2.536
batch start
#iterations: 382
currently lose_sum: 97.25058197975159
time_elpased: 2.414
batch start
#iterations: 383
currently lose_sum: 97.25049901008606
time_elpased: 2.575
batch start
#iterations: 384
currently lose_sum: 97.19349211454391
time_elpased: 2.377
batch start
#iterations: 385
currently lose_sum: 97.43540579080582
time_elpased: 2.196
batch start
#iterations: 386
currently lose_sum: 97.10644716024399
time_elpased: 2.563
batch start
#iterations: 387
currently lose_sum: 97.59849339723587
time_elpased: 2.566
batch start
#iterations: 388
currently lose_sum: 97.31383514404297
time_elpased: 2.431
batch start
#iterations: 389
currently lose_sum: 97.57526808977127
time_elpased: 2.618
batch start
#iterations: 390
currently lose_sum: 97.390505194664
time_elpased: 2.414
batch start
#iterations: 391
currently lose_sum: 97.21716213226318
time_elpased: 2.382
batch start
#iterations: 392
currently lose_sum: 97.03193986415863
time_elpased: 2.415
batch start
#iterations: 393
currently lose_sum: 97.03835779428482
time_elpased: 2.086
batch start
#iterations: 394
currently lose_sum: 97.2376098036766
time_elpased: 2.559
batch start
#iterations: 395
currently lose_sum: 97.12547659873962
time_elpased: 2.777
batch start
#iterations: 396
currently lose_sum: 96.87708276510239
time_elpased: 2.67
batch start
#iterations: 397
currently lose_sum: 97.23187458515167
time_elpased: 2.581
batch start
#iterations: 398
currently lose_sum: 97.08094054460526
time_elpased: 2.536
batch start
#iterations: 399
currently lose_sum: 97.35407096147537
time_elpased: 2.15
start validation test
0.631030927835
0.64058894627
0.599979417516
0.619619513232
0.631085443551
63.406
acc: 0.622
pre: 0.630
rec: 0.594
F1: 0.611
auc: 0.659
