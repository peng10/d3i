start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.78854620456696
time_elpased: 1.756
batch start
#iterations: 1
currently lose_sum: 100.34090107679367
time_elpased: 1.653
batch start
#iterations: 2
currently lose_sum: 99.99980157613754
time_elpased: 1.822
batch start
#iterations: 3
currently lose_sum: 99.8367178440094
time_elpased: 2.097
batch start
#iterations: 4
currently lose_sum: 99.74995982646942
time_elpased: 2.243
batch start
#iterations: 5
currently lose_sum: 99.55721980333328
time_elpased: 2.55
batch start
#iterations: 6
currently lose_sum: 99.5346508026123
time_elpased: 2.599
batch start
#iterations: 7
currently lose_sum: 99.17596125602722
time_elpased: 2.555
batch start
#iterations: 8
currently lose_sum: 99.29461151361465
time_elpased: 2.523
batch start
#iterations: 9
currently lose_sum: 99.26975417137146
time_elpased: 2.356
batch start
#iterations: 10
currently lose_sum: 98.9615907073021
time_elpased: 2.265
batch start
#iterations: 11
currently lose_sum: 99.12081116437912
time_elpased: 2.222
batch start
#iterations: 12
currently lose_sum: 99.00439608097076
time_elpased: 2.246
batch start
#iterations: 13
currently lose_sum: 98.8737478852272
time_elpased: 2.34
batch start
#iterations: 14
currently lose_sum: 99.08246821165085
time_elpased: 2.418
batch start
#iterations: 15
currently lose_sum: 98.54121845960617
time_elpased: 2.468
batch start
#iterations: 16
currently lose_sum: 98.569793343544
time_elpased: 2.427
batch start
#iterations: 17
currently lose_sum: 98.58490300178528
time_elpased: 2.441
batch start
#iterations: 18
currently lose_sum: 98.51287466287613
time_elpased: 2.065
batch start
#iterations: 19
currently lose_sum: 98.69200897216797
time_elpased: 2.494
start validation test
0.616443298969
0.627321548445
0.577029947515
0.601125703565
0.616512495187
64.475
batch start
#iterations: 20
currently lose_sum: 98.47070562839508
time_elpased: 2.214
batch start
#iterations: 21
currently lose_sum: 98.32253152132034
time_elpased: 2.276
batch start
#iterations: 22
currently lose_sum: 98.63383561372757
time_elpased: 2.389
batch start
#iterations: 23
currently lose_sum: 98.43284279108047
time_elpased: 2.072
batch start
#iterations: 24
currently lose_sum: 98.56400507688522
time_elpased: 2.446
batch start
#iterations: 25
currently lose_sum: 98.54369652271271
time_elpased: 2.276
batch start
#iterations: 26
currently lose_sum: 98.37796741724014
time_elpased: 2.399
batch start
#iterations: 27
currently lose_sum: 98.39061826467514
time_elpased: 2.408
batch start
#iterations: 28
currently lose_sum: 98.35120743513107
time_elpased: 2.058
batch start
#iterations: 29
currently lose_sum: 98.45048350095749
time_elpased: 2.189
batch start
#iterations: 30
currently lose_sum: 98.25783216953278
time_elpased: 2.373
batch start
#iterations: 31
currently lose_sum: 98.28443360328674
time_elpased: 2.352
batch start
#iterations: 32
currently lose_sum: 98.17401367425919
time_elpased: 2.421
batch start
#iterations: 33
currently lose_sum: 98.47375422716141
time_elpased: 2.308
batch start
#iterations: 34
currently lose_sum: 98.2121764421463
time_elpased: 2.247
batch start
#iterations: 35
currently lose_sum: 98.16763478517532
time_elpased: 2.225
batch start
#iterations: 36
currently lose_sum: 98.28493547439575
time_elpased: 2.434
batch start
#iterations: 37
currently lose_sum: 98.39999550580978
time_elpased: 2.444
batch start
#iterations: 38
currently lose_sum: 98.20752900838852
time_elpased: 2.345
batch start
#iterations: 39
currently lose_sum: 98.0632883310318
time_elpased: 2.328
start validation test
0.614948453608
0.635149765428
0.543377585675
0.585690515807
0.615074107306
64.176
batch start
#iterations: 40
currently lose_sum: 98.07936197519302
time_elpased: 2.135
batch start
#iterations: 41
currently lose_sum: 98.19478917121887
time_elpased: 2.379
batch start
#iterations: 42
currently lose_sum: 97.94714123010635
time_elpased: 2.419
batch start
#iterations: 43
currently lose_sum: 98.16864365339279
time_elpased: 2.122
batch start
#iterations: 44
currently lose_sum: 98.0270026922226
time_elpased: 2.064
batch start
#iterations: 45
currently lose_sum: 98.02275878190994
time_elpased: 2.312
batch start
#iterations: 46
currently lose_sum: 97.79815834760666
time_elpased: 2.378
batch start
#iterations: 47
currently lose_sum: 97.96740084886551
time_elpased: 2.387
batch start
#iterations: 48
currently lose_sum: 98.1856010556221
time_elpased: 2.449
batch start
#iterations: 49
currently lose_sum: 98.16272217035294
time_elpased: 2.262
batch start
#iterations: 50
currently lose_sum: 98.12643468379974
time_elpased: 2.245
batch start
#iterations: 51
currently lose_sum: 98.01627719402313
time_elpased: 2.273
batch start
#iterations: 52
currently lose_sum: 98.09318286180496
time_elpased: 2.297
batch start
#iterations: 53
currently lose_sum: 98.11012977361679
time_elpased: 2.161
batch start
#iterations: 54
currently lose_sum: 98.25597923994064
time_elpased: 2.216
batch start
#iterations: 55
currently lose_sum: 98.23762398958206
time_elpased: 2.297
batch start
#iterations: 56
currently lose_sum: 97.78068822622299
time_elpased: 2.38
batch start
#iterations: 57
currently lose_sum: 97.99995547533035
time_elpased: 2.356
batch start
#iterations: 58
currently lose_sum: 98.15308421850204
time_elpased: 2.419
batch start
#iterations: 59
currently lose_sum: 98.20752334594727
time_elpased: 2.345
start validation test
0.62087628866
0.624289623237
0.610476484512
0.617305791144
0.62089454712
63.896
batch start
#iterations: 60
currently lose_sum: 98.20370185375214
time_elpased: 2.463
batch start
#iterations: 61
currently lose_sum: 97.86913174390793
time_elpased: 2.475
batch start
#iterations: 62
currently lose_sum: 98.01880580186844
time_elpased: 2.417
batch start
#iterations: 63
currently lose_sum: 98.17591243982315
time_elpased: 2.32
batch start
#iterations: 64
currently lose_sum: 97.93289488554001
time_elpased: 2.418
batch start
#iterations: 65
currently lose_sum: 97.8213461637497
time_elpased: 2.181
batch start
#iterations: 66
currently lose_sum: 97.7597953081131
time_elpased: 2.326
batch start
#iterations: 67
currently lose_sum: 97.96557462215424
time_elpased: 2.316
batch start
#iterations: 68
currently lose_sum: 97.89295935630798
time_elpased: 2.379
batch start
#iterations: 69
currently lose_sum: 98.15484005212784
time_elpased: 2.443
batch start
#iterations: 70
currently lose_sum: 97.90724354982376
time_elpased: 2.361
batch start
#iterations: 71
currently lose_sum: 97.77004688978195
time_elpased: 2.267
batch start
#iterations: 72
currently lose_sum: 97.79690533876419
time_elpased: 2.197
batch start
#iterations: 73
currently lose_sum: 97.77358216047287
time_elpased: 2.118
batch start
#iterations: 74
currently lose_sum: 97.8261890411377
time_elpased: 2.263
batch start
#iterations: 75
currently lose_sum: 97.83366864919662
time_elpased: 2.249
batch start
#iterations: 76
currently lose_sum: 97.61974608898163
time_elpased: 2.363
batch start
#iterations: 77
currently lose_sum: 97.95470523834229
time_elpased: 2.327
batch start
#iterations: 78
currently lose_sum: 97.79015070199966
time_elpased: 2.326
batch start
#iterations: 79
currently lose_sum: 98.03732717037201
time_elpased: 2.173
start validation test
0.613711340206
0.635235430101
0.537305752804
0.582181088314
0.613845482
63.906
batch start
#iterations: 80
currently lose_sum: 97.93951094150543
time_elpased: 2.383
batch start
#iterations: 81
currently lose_sum: 97.74694097042084
time_elpased: 2.436
batch start
#iterations: 82
currently lose_sum: 97.89860939979553
time_elpased: 2.387
batch start
#iterations: 83
currently lose_sum: 97.55907517671585
time_elpased: 2.37
batch start
#iterations: 84
currently lose_sum: 97.64648377895355
time_elpased: 2.432
batch start
#iterations: 85
currently lose_sum: 97.68485617637634
time_elpased: 2.232
batch start
#iterations: 86
currently lose_sum: 97.85301727056503
time_elpased: 2.251
batch start
#iterations: 87
currently lose_sum: 97.53667622804642
time_elpased: 2.361
batch start
#iterations: 88
currently lose_sum: 97.57916623353958
time_elpased: 2.333
batch start
#iterations: 89
currently lose_sum: 97.44367170333862
time_elpased: 2.359
batch start
#iterations: 90
currently lose_sum: 97.88173669576645
time_elpased: 2.249
batch start
#iterations: 91
currently lose_sum: 97.782395362854
time_elpased: 2.29
batch start
#iterations: 92
currently lose_sum: 97.56083709001541
time_elpased: 2.148
batch start
#iterations: 93
currently lose_sum: 97.77944242954254
time_elpased: 2.243
batch start
#iterations: 94
currently lose_sum: 97.58858942985535
time_elpased: 2.237
batch start
#iterations: 95
currently lose_sum: 97.74654841423035
time_elpased: 2.344
batch start
#iterations: 96
currently lose_sum: 97.73070150613785
time_elpased: 2.3
batch start
#iterations: 97
currently lose_sum: 97.4607115983963
time_elpased: 1.818
batch start
#iterations: 98
currently lose_sum: 97.75598394870758
time_elpased: 2.231
batch start
#iterations: 99
currently lose_sum: 97.37687677145004
time_elpased: 2.294
start validation test
0.620515463918
0.636300497743
0.565709581146
0.598932229244
0.620611684098
63.823
batch start
#iterations: 100
currently lose_sum: 97.89762729406357
time_elpased: 2.368
batch start
#iterations: 101
currently lose_sum: 97.74534404277802
time_elpased: 2.284
batch start
#iterations: 102
currently lose_sum: 97.39477390050888
time_elpased: 1.983
batch start
#iterations: 103
currently lose_sum: 97.56365847587585
time_elpased: 2.089
batch start
#iterations: 104
currently lose_sum: 97.76404631137848
time_elpased: 1.95
batch start
#iterations: 105
currently lose_sum: 97.693066239357
time_elpased: 2.016
batch start
#iterations: 106
currently lose_sum: 97.7268562912941
time_elpased: 2.02
batch start
#iterations: 107
currently lose_sum: 97.61079317331314
time_elpased: 1.935
batch start
#iterations: 108
currently lose_sum: 97.6421086192131
time_elpased: 2.297
batch start
#iterations: 109
currently lose_sum: 97.89909702539444
time_elpased: 2.335
batch start
#iterations: 110
currently lose_sum: 97.34015816450119
time_elpased: 2.413
batch start
#iterations: 111
currently lose_sum: 97.55459946393967
time_elpased: 2.427
batch start
#iterations: 112
currently lose_sum: 97.49996387958527
time_elpased: 2.315
batch start
#iterations: 113
currently lose_sum: 97.63407081365585
time_elpased: 2.646
batch start
#iterations: 114
currently lose_sum: 97.34465879201889
time_elpased: 2.601
batch start
#iterations: 115
currently lose_sum: 97.42654234170914
time_elpased: 2.406
batch start
#iterations: 116
currently lose_sum: 97.83839464187622
time_elpased: 2.428
batch start
#iterations: 117
currently lose_sum: 97.63733035326004
time_elpased: 2.431
batch start
#iterations: 118
currently lose_sum: 97.66642636060715
time_elpased: 2.303
batch start
#iterations: 119
currently lose_sum: 97.55011337995529
time_elpased: 2.422
start validation test
0.625360824742
0.637878617273
0.582998867963
0.609205290891
0.625435197691
63.573
batch start
#iterations: 120
currently lose_sum: 97.66352039575577
time_elpased: 2.294
batch start
#iterations: 121
currently lose_sum: 97.58648067712784
time_elpased: 2.229
batch start
#iterations: 122
currently lose_sum: 97.8140881061554
time_elpased: 2.129
batch start
#iterations: 123
currently lose_sum: 97.4143528342247
time_elpased: 2.163
batch start
#iterations: 124
currently lose_sum: 97.13042289018631
time_elpased: 2.415
batch start
#iterations: 125
currently lose_sum: 97.54473620653152
time_elpased: 2.325
batch start
#iterations: 126
currently lose_sum: 97.7831643819809
time_elpased: 2.344
batch start
#iterations: 127
currently lose_sum: 97.72847187519073
time_elpased: 2.452
batch start
#iterations: 128
currently lose_sum: 97.75307840108871
time_elpased: 2.199
batch start
#iterations: 129
currently lose_sum: 97.26944237947464
time_elpased: 2.353
batch start
#iterations: 130
currently lose_sum: 97.65180546045303
time_elpased: 2.221
batch start
#iterations: 131
currently lose_sum: 97.36422663927078
time_elpased: 2.428
batch start
#iterations: 132
currently lose_sum: 97.33013623952866
time_elpased: 2.454
batch start
#iterations: 133
currently lose_sum: 97.38089287281036
time_elpased: 2.339
batch start
#iterations: 134
currently lose_sum: 97.46017622947693
time_elpased: 2.282
batch start
#iterations: 135
currently lose_sum: 97.67084723711014
time_elpased: 2.265
batch start
#iterations: 136
currently lose_sum: 97.2456288933754
time_elpased: 2.191
batch start
#iterations: 137
currently lose_sum: 97.57478195428848
time_elpased: 2.342
batch start
#iterations: 138
currently lose_sum: 97.2418721318245
time_elpased: 2.348
batch start
#iterations: 139
currently lose_sum: 97.38508176803589
time_elpased: 2.244
start validation test
0.627164948454
0.633922794911
0.605022126171
0.619135379917
0.627203823593
63.507
batch start
#iterations: 140
currently lose_sum: 97.58468699455261
time_elpased: 2.373
batch start
#iterations: 141
currently lose_sum: 97.60774254798889
time_elpased: 2.471
batch start
#iterations: 142
currently lose_sum: 97.58154088258743
time_elpased: 2.384
batch start
#iterations: 143
currently lose_sum: 97.50953876972198
time_elpased: 2.145
batch start
#iterations: 144
currently lose_sum: 97.54891729354858
time_elpased: 2.274
batch start
#iterations: 145
currently lose_sum: 97.72148084640503
time_elpased: 2.285
batch start
#iterations: 146
currently lose_sum: 97.14316034317017
time_elpased: 2.262
batch start
#iterations: 147
currently lose_sum: 97.36747354269028
time_elpased: 2.308
batch start
#iterations: 148
currently lose_sum: 97.76490032672882
time_elpased: 2.261
batch start
#iterations: 149
currently lose_sum: 97.46687358617783
time_elpased: 2.054
batch start
#iterations: 150
currently lose_sum: 97.38385248184204
time_elpased: 2.357
batch start
#iterations: 151
currently lose_sum: 97.31495928764343
time_elpased: 2.35
batch start
#iterations: 152
currently lose_sum: 97.43987107276917
time_elpased: 2.254
batch start
#iterations: 153
currently lose_sum: 97.69758486747742
time_elpased: 2.365
batch start
#iterations: 154
currently lose_sum: 97.59117311239243
time_elpased: 2.512
batch start
#iterations: 155
currently lose_sum: 97.40917718410492
time_elpased: 2.427
batch start
#iterations: 156
currently lose_sum: 97.40999525785446
time_elpased: 2.063
batch start
#iterations: 157
currently lose_sum: 97.83121770620346
time_elpased: 2.441
batch start
#iterations: 158
currently lose_sum: 97.69482213258743
time_elpased: 2.39
batch start
#iterations: 159
currently lose_sum: 97.90173691511154
time_elpased: 2.23
start validation test
0.625463917526
0.644023974615
0.56396006998
0.601338746845
0.625571897016
63.848
batch start
#iterations: 160
currently lose_sum: 97.4135029911995
time_elpased: 2.478
batch start
#iterations: 161
currently lose_sum: 97.50004917383194
time_elpased: 2.217
batch start
#iterations: 162
currently lose_sum: 97.29562240839005
time_elpased: 2.333
batch start
#iterations: 163
currently lose_sum: 97.41362476348877
time_elpased: 2.417
batch start
#iterations: 164
currently lose_sum: 97.42845636606216
time_elpased: 2.298
batch start
#iterations: 165
currently lose_sum: 97.63648545742035
time_elpased: 2.156
batch start
#iterations: 166
currently lose_sum: 97.43562376499176
time_elpased: 2.391
batch start
#iterations: 167
currently lose_sum: 97.48089063167572
time_elpased: 2.407
batch start
#iterations: 168
currently lose_sum: 97.07507693767548
time_elpased: 2.211
batch start
#iterations: 169
currently lose_sum: 97.42823022603989
time_elpased: 2.438
batch start
#iterations: 170
currently lose_sum: 97.32182031869888
time_elpased: 2.496
batch start
#iterations: 171
currently lose_sum: 97.43798762559891
time_elpased: 2.082
batch start
#iterations: 172
currently lose_sum: 97.70515555143356
time_elpased: 2.465
batch start
#iterations: 173
currently lose_sum: 97.393694460392
time_elpased: 2.374
batch start
#iterations: 174
currently lose_sum: 97.41168254613876
time_elpased: 2.283
batch start
#iterations: 175
currently lose_sum: 97.2791765332222
time_elpased: 2.421
batch start
#iterations: 176
currently lose_sum: 97.47580999135971
time_elpased: 2.464
batch start
#iterations: 177
currently lose_sum: 97.43691438436508
time_elpased: 2.487
batch start
#iterations: 178
currently lose_sum: 97.09384661912918
time_elpased: 2.271
batch start
#iterations: 179
currently lose_sum: 97.63544750213623
time_elpased: 2.172
start validation test
0.624484536082
0.632983377078
0.595657095811
0.613753247442
0.6245351471
63.690
batch start
#iterations: 180
currently lose_sum: 97.30115115642548
time_elpased: 2.049
batch start
#iterations: 181
currently lose_sum: 97.54985964298248
time_elpased: 2.263
batch start
#iterations: 182
currently lose_sum: 97.48699235916138
time_elpased: 2.216
batch start
#iterations: 183
currently lose_sum: 97.11538636684418
time_elpased: 2.185
batch start
#iterations: 184
currently lose_sum: 97.20375174283981
time_elpased: 2.481
batch start
#iterations: 185
currently lose_sum: 97.45030552148819
time_elpased: 2.486
batch start
#iterations: 186
currently lose_sum: 97.40736526250839
time_elpased: 2.459
batch start
#iterations: 187
currently lose_sum: 96.7926732301712
time_elpased: 2.413
batch start
#iterations: 188
currently lose_sum: 97.63870006799698
time_elpased: 2.333
batch start
#iterations: 189
currently lose_sum: 97.35708779096603
time_elpased: 2.342
batch start
#iterations: 190
currently lose_sum: 97.64528226852417
time_elpased: 2.228
batch start
#iterations: 191
currently lose_sum: 97.33347737789154
time_elpased: 2.299
batch start
#iterations: 192
currently lose_sum: 97.10715156793594
time_elpased: 2.379
batch start
#iterations: 193
currently lose_sum: 97.3848705291748
time_elpased: 2.37
batch start
#iterations: 194
currently lose_sum: 97.40019416809082
time_elpased: 2.41
batch start
#iterations: 195
currently lose_sum: 97.28862416744232
time_elpased: 2.328
batch start
#iterations: 196
currently lose_sum: 97.42223751544952
time_elpased: 2.19
batch start
#iterations: 197
currently lose_sum: 97.3353779911995
time_elpased: 2.367
batch start
#iterations: 198
currently lose_sum: 97.35287177562714
time_elpased: 2.184
batch start
#iterations: 199
currently lose_sum: 97.26515501737595
time_elpased: 2.337
start validation test
0.621134020619
0.639382875986
0.558711536482
0.596331282953
0.621243612917
63.685
batch start
#iterations: 200
currently lose_sum: 97.24007987976074
time_elpased: 2.373
batch start
#iterations: 201
currently lose_sum: 97.35786664485931
time_elpased: 2.385
batch start
#iterations: 202
currently lose_sum: 97.00653475522995
time_elpased: 2.352
batch start
#iterations: 203
currently lose_sum: 97.4157138466835
time_elpased: 2.385
batch start
#iterations: 204
currently lose_sum: 96.92824983596802
time_elpased: 2.371
batch start
#iterations: 205
currently lose_sum: 97.18502068519592
time_elpased: 2.074
batch start
#iterations: 206
currently lose_sum: 97.37364768981934
time_elpased: 2.275
batch start
#iterations: 207
currently lose_sum: 97.32321971654892
time_elpased: 2.426
batch start
#iterations: 208
currently lose_sum: 96.93697434663773
time_elpased: 2.267
batch start
#iterations: 209
currently lose_sum: 97.18698519468307
time_elpased: 2.246
batch start
#iterations: 210
currently lose_sum: 97.1227753162384
time_elpased: 2.231
batch start
#iterations: 211
currently lose_sum: 97.23749715089798
time_elpased: 2.064
batch start
#iterations: 212
currently lose_sum: 97.03435164690018
time_elpased: 2.2
batch start
#iterations: 213
currently lose_sum: 97.26811826229095
time_elpased: 2.347
batch start
#iterations: 214
currently lose_sum: 97.4125463962555
time_elpased: 2.352
batch start
#iterations: 215
currently lose_sum: 97.20365160703659
time_elpased: 2.202
batch start
#iterations: 216
currently lose_sum: 96.85641098022461
time_elpased: 2.25
batch start
#iterations: 217
currently lose_sum: 97.44910126924515
time_elpased: 2.3
batch start
#iterations: 218
currently lose_sum: 97.35307669639587
time_elpased: 2.536
batch start
#iterations: 219
currently lose_sum: 97.08368909358978
time_elpased: 2.148
start validation test
0.625412371134
0.640901771337
0.573428012761
0.605290315572
0.625503637693
63.752
batch start
#iterations: 220
currently lose_sum: 97.43489247560501
time_elpased: 2.524
batch start
#iterations: 221
currently lose_sum: 97.19583439826965
time_elpased: 2.413
batch start
#iterations: 222
currently lose_sum: 97.20827388763428
time_elpased: 2.431
batch start
#iterations: 223
currently lose_sum: 97.07341021299362
time_elpased: 2.352
batch start
#iterations: 224
currently lose_sum: 97.13132584095001
time_elpased: 2.418
batch start
#iterations: 225
currently lose_sum: 97.13046556711197
time_elpased: 2.518
batch start
#iterations: 226
currently lose_sum: 97.32837837934494
time_elpased: 2.387
batch start
#iterations: 227
currently lose_sum: 97.22458863258362
time_elpased: 2.222
batch start
#iterations: 228
currently lose_sum: 97.00608021020889
time_elpased: 2.469
batch start
#iterations: 229
currently lose_sum: 97.19602942466736
time_elpased: 2.392
batch start
#iterations: 230
currently lose_sum: 97.29330778121948
time_elpased: 2.371
batch start
#iterations: 231
currently lose_sum: 97.27681964635849
time_elpased: 2.131
batch start
#iterations: 232
currently lose_sum: 97.2073786854744
time_elpased: 2.328
batch start
#iterations: 233
currently lose_sum: 97.20875197649002
time_elpased: 2.4
batch start
#iterations: 234
currently lose_sum: 97.17710810899734
time_elpased: 2.234
batch start
#iterations: 235
currently lose_sum: 97.06881844997406
time_elpased: 2.402
batch start
#iterations: 236
currently lose_sum: 97.30291825532913
time_elpased: 2.392
batch start
#iterations: 237
currently lose_sum: 97.24688756465912
time_elpased: 2.343
batch start
#iterations: 238
currently lose_sum: 97.28543770313263
time_elpased: 2.491
batch start
#iterations: 239
currently lose_sum: 97.18799984455109
time_elpased: 2.268
start validation test
0.615463917526
0.643265202488
0.521457239889
0.575991815392
0.615628960748
63.912
batch start
#iterations: 240
currently lose_sum: 96.93025481700897
time_elpased: 2.282
batch start
#iterations: 241
currently lose_sum: 97.09430766105652
time_elpased: 2.455
batch start
#iterations: 242
currently lose_sum: 97.15378487110138
time_elpased: 2.309
batch start
#iterations: 243
currently lose_sum: 97.17382967472076
time_elpased: 2.161
batch start
#iterations: 244
currently lose_sum: 97.40349292755127
time_elpased: 2.351
batch start
#iterations: 245
currently lose_sum: 97.53452545404434
time_elpased: 2.279
batch start
#iterations: 246
currently lose_sum: 97.02121657133102
time_elpased: 2.352
batch start
#iterations: 247
currently lose_sum: 96.96404069662094
time_elpased: 2.34
batch start
#iterations: 248
currently lose_sum: 97.15873980522156
time_elpased: 2.301
batch start
#iterations: 249
currently lose_sum: 97.33644688129425
time_elpased: 2.297
batch start
#iterations: 250
currently lose_sum: 97.16031455993652
time_elpased: 2.212
batch start
#iterations: 251
currently lose_sum: 97.21089226007462
time_elpased: 2.148
batch start
#iterations: 252
currently lose_sum: 97.31980746984482
time_elpased: 2.233
batch start
#iterations: 253
currently lose_sum: 97.28654164075851
time_elpased: 1.922
batch start
#iterations: 254
currently lose_sum: 97.09834623336792
time_elpased: 2.262
batch start
#iterations: 255
currently lose_sum: 97.2045863866806
time_elpased: 2.463
batch start
#iterations: 256
currently lose_sum: 97.13729470968246
time_elpased: 2.488
batch start
#iterations: 257
currently lose_sum: 96.79645639657974
time_elpased: 2.517
batch start
#iterations: 258
currently lose_sum: 97.17719054222107
time_elpased: 2.454
batch start
#iterations: 259
currently lose_sum: 97.03631442785263
time_elpased: 2.21
start validation test
0.625
0.637098585223
0.583925079757
0.609354024593
0.625072113358
63.528
batch start
#iterations: 260
currently lose_sum: 97.02136898040771
time_elpased: 2.37
batch start
#iterations: 261
currently lose_sum: 96.96568715572357
time_elpased: 2.36
batch start
#iterations: 262
currently lose_sum: 96.85018336772919
time_elpased: 2.508
batch start
#iterations: 263
currently lose_sum: 97.16171020269394
time_elpased: 2.41
batch start
#iterations: 264
currently lose_sum: 96.82187795639038
time_elpased: 2.452
batch start
#iterations: 265
currently lose_sum: 97.4685429930687
time_elpased: 2.443
batch start
#iterations: 266
currently lose_sum: 97.19147419929504
time_elpased: 2.233
batch start
#iterations: 267
currently lose_sum: 97.07669734954834
time_elpased: 2.271
batch start
#iterations: 268
currently lose_sum: 96.8620252609253
time_elpased: 2.286
batch start
#iterations: 269
currently lose_sum: 97.2684673666954
time_elpased: 2.395
batch start
#iterations: 270
currently lose_sum: 96.96590673923492
time_elpased: 2.422
batch start
#iterations: 271
currently lose_sum: 97.03808379173279
time_elpased: 2.359
batch start
#iterations: 272
currently lose_sum: 97.26235383749008
time_elpased: 2.43
batch start
#iterations: 273
currently lose_sum: 97.0660212635994
time_elpased: 2.445
batch start
#iterations: 274
currently lose_sum: 97.03680270910263
time_elpased: 2.376
batch start
#iterations: 275
currently lose_sum: 97.28452056646347
time_elpased: 2.303
batch start
#iterations: 276
currently lose_sum: 97.09186112880707
time_elpased: 2.215
batch start
#iterations: 277
currently lose_sum: 97.13920962810516
time_elpased: 2.28
batch start
#iterations: 278
currently lose_sum: 96.95063585042953
time_elpased: 2.355
batch start
#iterations: 279
currently lose_sum: 97.17547011375427
time_elpased: 2.447
start validation test
0.625979381443
0.637194782027
0.58814448904
0.611687894681
0.626045806433
63.524
batch start
#iterations: 280
currently lose_sum: 96.87456601858139
time_elpased: 2.197
batch start
#iterations: 281
currently lose_sum: 97.33954620361328
time_elpased: 2.398
batch start
#iterations: 282
currently lose_sum: 97.08575266599655
time_elpased: 2.182
batch start
#iterations: 283
currently lose_sum: 97.054962515831
time_elpased: 2.343
batch start
#iterations: 284
currently lose_sum: 97.03684550523758
time_elpased: 2.434
batch start
#iterations: 285
currently lose_sum: 96.6955354809761
time_elpased: 2.185
batch start
#iterations: 286
currently lose_sum: 97.11037093400955
time_elpased: 2.467
batch start
#iterations: 287
currently lose_sum: 97.01620811223984
time_elpased: 2.349
batch start
#iterations: 288
currently lose_sum: 96.90011531114578
time_elpased: 2.129
batch start
#iterations: 289
currently lose_sum: 97.3238296508789
time_elpased: 2.18
batch start
#iterations: 290
currently lose_sum: 97.36917895078659
time_elpased: 2.267
batch start
#iterations: 291
currently lose_sum: 97.00117897987366
time_elpased: 2.397
batch start
#iterations: 292
currently lose_sum: 97.18172061443329
time_elpased: 2.341
batch start
#iterations: 293
currently lose_sum: 97.23443162441254
time_elpased: 2.207
batch start
#iterations: 294
currently lose_sum: 96.92561846971512
time_elpased: 2.291
batch start
#iterations: 295
currently lose_sum: 97.05887722969055
time_elpased: 2.305
batch start
#iterations: 296
currently lose_sum: 97.24087738990784
time_elpased: 2.346
batch start
#iterations: 297
currently lose_sum: 97.14072024822235
time_elpased: 2.277
batch start
#iterations: 298
currently lose_sum: 97.25533163547516
time_elpased: 2.575
batch start
#iterations: 299
currently lose_sum: 96.99451321363449
time_elpased: 2.388
start validation test
0.625051546392
0.638791046472
0.578573633838
0.607193001404
0.625133145536
63.678
batch start
#iterations: 300
currently lose_sum: 97.0265035033226
time_elpased: 2.42
batch start
#iterations: 301
currently lose_sum: 96.72849929332733
time_elpased: 2.2
batch start
#iterations: 302
currently lose_sum: 97.08451616764069
time_elpased: 2.54
batch start
#iterations: 303
currently lose_sum: 96.82953268289566
time_elpased: 2.384
batch start
#iterations: 304
currently lose_sum: 97.16981506347656
time_elpased: 2.385
batch start
#iterations: 305
currently lose_sum: 97.2814964056015
time_elpased: 2.531
batch start
#iterations: 306
currently lose_sum: 97.11976689100266
time_elpased: 2.375
batch start
#iterations: 307
currently lose_sum: 97.07270574569702
time_elpased: 2.436
batch start
#iterations: 308
currently lose_sum: 96.73937451839447
time_elpased: 2.424
batch start
#iterations: 309
currently lose_sum: 97.1576036810875
time_elpased: 2.149
batch start
#iterations: 310
currently lose_sum: 97.03142261505127
time_elpased: 2.016
batch start
#iterations: 311
currently lose_sum: 97.05946224927902
time_elpased: 2.376
batch start
#iterations: 312
currently lose_sum: 97.08131837844849
time_elpased: 2.379
batch start
#iterations: 313
currently lose_sum: 97.18445473909378
time_elpased: 2.24
batch start
#iterations: 314
currently lose_sum: 97.0431460738182
time_elpased: 2.178
batch start
#iterations: 315
currently lose_sum: 96.95607441663742
time_elpased: 2.106
batch start
#iterations: 316
currently lose_sum: 96.94495522975922
time_elpased: 2.28
batch start
#iterations: 317
currently lose_sum: 97.20958495140076
time_elpased: 2.321
batch start
#iterations: 318
currently lose_sum: 97.15600180625916
time_elpased: 2.56
batch start
#iterations: 319
currently lose_sum: 96.82538360357285
time_elpased: 2.698
start validation test
0.625670103093
0.636373736252
0.58948235052
0.612031199915
0.625733636274
63.597
batch start
#iterations: 320
currently lose_sum: 97.30342531204224
time_elpased: 2.084
batch start
#iterations: 321
currently lose_sum: 96.9581767320633
time_elpased: 2.295
batch start
#iterations: 322
currently lose_sum: 96.98954600095749
time_elpased: 2.299
batch start
#iterations: 323
currently lose_sum: 97.11368036270142
time_elpased: 2.217
batch start
#iterations: 324
currently lose_sum: 97.21561062335968
time_elpased: 2.357
batch start
#iterations: 325
currently lose_sum: 97.20742636919022
time_elpased: 2.397
batch start
#iterations: 326
currently lose_sum: 97.07910227775574
time_elpased: 2.31
batch start
#iterations: 327
currently lose_sum: 96.98716288805008
time_elpased: 2.355
batch start
#iterations: 328
currently lose_sum: 97.21786969900131
time_elpased: 2.315
batch start
#iterations: 329
currently lose_sum: 96.92072904109955
time_elpased: 2.307
batch start
#iterations: 330
currently lose_sum: 96.95977056026459
time_elpased: 2.48
batch start
#iterations: 331
currently lose_sum: 97.04438948631287
time_elpased: 2.425
batch start
#iterations: 332
currently lose_sum: 96.8590931892395
time_elpased: 2.178
batch start
#iterations: 333
currently lose_sum: 96.72061049938202
time_elpased: 2.461
batch start
#iterations: 334
currently lose_sum: 96.87620544433594
time_elpased: 2.217
batch start
#iterations: 335
currently lose_sum: 96.83028358221054
time_elpased: 2.418
batch start
#iterations: 336
currently lose_sum: 96.74331510066986
time_elpased: 2.379
batch start
#iterations: 337
currently lose_sum: 97.16736102104187
time_elpased: 2.332
batch start
#iterations: 338
currently lose_sum: 96.84886366128922
time_elpased: 2.357
batch start
#iterations: 339
currently lose_sum: 96.99583494663239
time_elpased: 2.454
start validation test
0.628092783505
0.634951456311
0.605742513121
0.620003160057
0.628132022852
63.619
batch start
#iterations: 340
currently lose_sum: 97.0521211028099
time_elpased: 2.361
batch start
#iterations: 341
currently lose_sum: 97.33344203233719
time_elpased: 2.311
batch start
#iterations: 342
currently lose_sum: 96.9067171216011
time_elpased: 2.302
batch start
#iterations: 343
currently lose_sum: 97.11226838827133
time_elpased: 2.36
batch start
#iterations: 344
currently lose_sum: 96.95907723903656
time_elpased: 2.358
batch start
#iterations: 345
currently lose_sum: 97.06056237220764
time_elpased: 2.417
batch start
#iterations: 346
currently lose_sum: 96.84860414266586
time_elpased: 2.353
batch start
#iterations: 347
currently lose_sum: 96.87472385168076
time_elpased: 2.342
batch start
#iterations: 348
currently lose_sum: 97.16363739967346
time_elpased: 2.403
batch start
#iterations: 349
currently lose_sum: 97.07039207220078
time_elpased: 2.384
batch start
#iterations: 350
currently lose_sum: 97.20094138383865
time_elpased: 2.133
batch start
#iterations: 351
currently lose_sum: 97.00651663541794
time_elpased: 2.195
batch start
#iterations: 352
currently lose_sum: 97.11509412527084
time_elpased: 2.239
batch start
#iterations: 353
currently lose_sum: 97.32265049219131
time_elpased: 2.344
batch start
#iterations: 354
currently lose_sum: 97.42273664474487
time_elpased: 2.319
batch start
#iterations: 355
currently lose_sum: 97.43887293338776
time_elpased: 2.079
batch start
#iterations: 356
currently lose_sum: 96.99338603019714
time_elpased: 2.342
batch start
#iterations: 357
currently lose_sum: 96.97228121757507
time_elpased: 2.367
batch start
#iterations: 358
currently lose_sum: 97.45606499910355
time_elpased: 2.359
batch start
#iterations: 359
currently lose_sum: 97.16382706165314
time_elpased: 2.296
start validation test
0.623041237113
0.636435868331
0.577029947515
0.605278782318
0.623122017029
63.592
batch start
#iterations: 360
currently lose_sum: 96.9596551656723
time_elpased: 2.465
batch start
#iterations: 361
currently lose_sum: 97.3001007437706
time_elpased: 2.316
batch start
#iterations: 362
currently lose_sum: 96.9963931441307
time_elpased: 2.537
batch start
#iterations: 363
currently lose_sum: 96.80152016878128
time_elpased: 2.521
batch start
#iterations: 364
currently lose_sum: 97.44297617673874
time_elpased: 2.346
batch start
#iterations: 365
currently lose_sum: 97.0317519903183
time_elpased: 2.4
batch start
#iterations: 366
currently lose_sum: 97.08754181861877
time_elpased: 2.476
batch start
#iterations: 367
currently lose_sum: 96.9601863026619
time_elpased: 2.204
batch start
#iterations: 368
currently lose_sum: 97.24060654640198
time_elpased: 2.301
batch start
#iterations: 369
currently lose_sum: 96.83266866207123
time_elpased: 2.394
batch start
#iterations: 370
currently lose_sum: 96.94065898656845
time_elpased: 2.418
batch start
#iterations: 371
currently lose_sum: 96.83184814453125
time_elpased: 2.468
batch start
#iterations: 372
currently lose_sum: 97.10750448703766
time_elpased: 2.443
batch start
#iterations: 373
currently lose_sum: 97.19680780172348
time_elpased: 2.509
batch start
#iterations: 374
currently lose_sum: 96.98813915252686
time_elpased: 2.32
batch start
#iterations: 375
currently lose_sum: 97.1636009812355
time_elpased: 2.311
batch start
#iterations: 376
currently lose_sum: 96.72458380460739
time_elpased: 2.143
batch start
#iterations: 377
currently lose_sum: 97.24438953399658
time_elpased: 2.112
batch start
#iterations: 378
currently lose_sum: 97.05994153022766
time_elpased: 2.295
batch start
#iterations: 379
currently lose_sum: 96.71984040737152
time_elpased: 2.361
start validation test
0.626958762887
0.634577816366
0.601728928682
0.617716972162
0.627003057752
63.600
batch start
#iterations: 380
currently lose_sum: 96.96410191059113
time_elpased: 2.299
batch start
#iterations: 381
currently lose_sum: 96.86069297790527
time_elpased: 2.306
batch start
#iterations: 382
currently lose_sum: 96.88931107521057
time_elpased: 2.264
batch start
#iterations: 383
currently lose_sum: 96.94463211297989
time_elpased: 2.068
batch start
#iterations: 384
currently lose_sum: 96.81712806224823
time_elpased: 2.409
batch start
#iterations: 385
currently lose_sum: 97.05866533517838
time_elpased: 2.409
batch start
#iterations: 386
currently lose_sum: 96.99070072174072
time_elpased: 2.374
batch start
#iterations: 387
currently lose_sum: 96.65199095010757
time_elpased: 2.42
batch start
#iterations: 388
currently lose_sum: 96.98160183429718
time_elpased: 2.331
batch start
#iterations: 389
currently lose_sum: 96.99628829956055
time_elpased: 2.167
batch start
#iterations: 390
currently lose_sum: 97.08609330654144
time_elpased: 2.483
batch start
#iterations: 391
currently lose_sum: 96.77851665019989
time_elpased: 2.332
batch start
#iterations: 392
currently lose_sum: 96.7326033115387
time_elpased: 2.357
batch start
#iterations: 393
currently lose_sum: 96.91553109884262
time_elpased: 2.395
batch start
#iterations: 394
currently lose_sum: 96.8797858953476
time_elpased: 2.436
batch start
#iterations: 395
currently lose_sum: 97.12300837039948
time_elpased: 2.439
batch start
#iterations: 396
currently lose_sum: 96.71498334407806
time_elpased: 2.043
batch start
#iterations: 397
currently lose_sum: 96.91837394237518
time_elpased: 1.972
batch start
#iterations: 398
currently lose_sum: 96.76862210035324
time_elpased: 2.395
batch start
#iterations: 399
currently lose_sum: 96.84761852025986
time_elpased: 2.352
start validation test
0.627113402062
0.638236276584
0.589894000206
0.613113702
0.627178746463
63.669
acc: 0.630
pre: 0.636
rec: 0.610
F1: 0.623
auc: 0.669
