start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.23348850011826
time_elpased: 1.579
batch start
#iterations: 1
currently lose_sum: 99.57209026813507
time_elpased: 1.462
batch start
#iterations: 2
currently lose_sum: 99.1664827466011
time_elpased: 1.451
batch start
#iterations: 3
currently lose_sum: 98.42884439229965
time_elpased: 1.474
batch start
#iterations: 4
currently lose_sum: 98.4942979812622
time_elpased: 1.462
batch start
#iterations: 5
currently lose_sum: 97.87126362323761
time_elpased: 1.525
batch start
#iterations: 6
currently lose_sum: 97.77768164873123
time_elpased: 1.5
batch start
#iterations: 7
currently lose_sum: 97.3821417093277
time_elpased: 1.472
batch start
#iterations: 8
currently lose_sum: 97.47058027982712
time_elpased: 1.481
batch start
#iterations: 9
currently lose_sum: 97.18861871957779
time_elpased: 1.508
batch start
#iterations: 10
currently lose_sum: 97.06211423873901
time_elpased: 1.554
batch start
#iterations: 11
currently lose_sum: 96.99121224880219
time_elpased: 1.505
batch start
#iterations: 12
currently lose_sum: 96.81071090698242
time_elpased: 1.472
batch start
#iterations: 13
currently lose_sum: 97.03384286165237
time_elpased: 1.548
batch start
#iterations: 14
currently lose_sum: 96.7074499130249
time_elpased: 1.483
batch start
#iterations: 15
currently lose_sum: 96.52872306108475
time_elpased: 1.464
batch start
#iterations: 16
currently lose_sum: 96.4930773973465
time_elpased: 1.5
batch start
#iterations: 17
currently lose_sum: 96.62898373603821
time_elpased: 1.439
batch start
#iterations: 18
currently lose_sum: 96.60483068227768
time_elpased: 1.531
batch start
#iterations: 19
currently lose_sum: 96.1494088768959
time_elpased: 1.497
start validation test
0.648453608247
0.643679301449
0.667661589131
0.655451146812
0.648421872633
62.168
batch start
#iterations: 20
currently lose_sum: 96.19966024160385
time_elpased: 1.47
batch start
#iterations: 21
currently lose_sum: 95.9916181564331
time_elpased: 1.489
batch start
#iterations: 22
currently lose_sum: 96.22513365745544
time_elpased: 1.458
batch start
#iterations: 23
currently lose_sum: 96.1372327208519
time_elpased: 1.475
batch start
#iterations: 24
currently lose_sum: 96.0881758928299
time_elpased: 1.515
batch start
#iterations: 25
currently lose_sum: 95.77092695236206
time_elpased: 1.486
batch start
#iterations: 26
currently lose_sum: 95.9574823975563
time_elpased: 1.465
batch start
#iterations: 27
currently lose_sum: 95.80079793930054
time_elpased: 1.564
batch start
#iterations: 28
currently lose_sum: 95.79895329475403
time_elpased: 1.498
batch start
#iterations: 29
currently lose_sum: 96.30477643013
time_elpased: 1.545
batch start
#iterations: 30
currently lose_sum: 95.90382832288742
time_elpased: 1.513
batch start
#iterations: 31
currently lose_sum: 95.69936394691467
time_elpased: 1.455
batch start
#iterations: 32
currently lose_sum: 95.6736769080162
time_elpased: 1.483
batch start
#iterations: 33
currently lose_sum: 95.8121383190155
time_elpased: 1.438
batch start
#iterations: 34
currently lose_sum: 95.69405019283295
time_elpased: 1.471
batch start
#iterations: 35
currently lose_sum: 95.735506772995
time_elpased: 1.452
batch start
#iterations: 36
currently lose_sum: 95.83881729841232
time_elpased: 1.522
batch start
#iterations: 37
currently lose_sum: 95.67715805768967
time_elpased: 1.468
batch start
#iterations: 38
currently lose_sum: 95.61585640907288
time_elpased: 1.426
batch start
#iterations: 39
currently lose_sum: 95.547935962677
time_elpased: 1.488
start validation test
0.658144329897
0.645033860045
0.705846027172
0.674071161785
0.658065516684
61.544
batch start
#iterations: 40
currently lose_sum: 95.49409681558609
time_elpased: 1.438
batch start
#iterations: 41
currently lose_sum: 95.49561512470245
time_elpased: 1.477
batch start
#iterations: 42
currently lose_sum: 95.66462361812592
time_elpased: 1.425
batch start
#iterations: 43
currently lose_sum: 95.5099248290062
time_elpased: 1.57
batch start
#iterations: 44
currently lose_sum: 95.77676862478256
time_elpased: 1.442
batch start
#iterations: 45
currently lose_sum: 95.64176189899445
time_elpased: 1.529
batch start
#iterations: 46
currently lose_sum: 94.99583595991135
time_elpased: 1.553
batch start
#iterations: 47
currently lose_sum: 95.21507626771927
time_elpased: 1.473
batch start
#iterations: 48
currently lose_sum: 95.38350230455399
time_elpased: 1.482
batch start
#iterations: 49
currently lose_sum: 95.2906169295311
time_elpased: 1.481
batch start
#iterations: 50
currently lose_sum: 95.37243682146072
time_elpased: 1.461
batch start
#iterations: 51
currently lose_sum: 95.03587651252747
time_elpased: 1.509
batch start
#iterations: 52
currently lose_sum: 95.44920521974564
time_elpased: 1.418
batch start
#iterations: 53
currently lose_sum: 94.93139493465424
time_elpased: 1.531
batch start
#iterations: 54
currently lose_sum: 95.26533854007721
time_elpased: 1.539
batch start
#iterations: 55
currently lose_sum: 95.25410115718842
time_elpased: 1.532
batch start
#iterations: 56
currently lose_sum: 95.53973245620728
time_elpased: 1.49
batch start
#iterations: 57
currently lose_sum: 95.25330340862274
time_elpased: 1.444
batch start
#iterations: 58
currently lose_sum: 95.23726785182953
time_elpased: 1.426
batch start
#iterations: 59
currently lose_sum: 95.35901898145676
time_elpased: 1.533
start validation test
0.665412371134
0.645519357459
0.736208316179
0.687887676107
0.665295401378
61.055
batch start
#iterations: 60
currently lose_sum: 94.8737445473671
time_elpased: 1.486
batch start
#iterations: 61
currently lose_sum: 95.54392659664154
time_elpased: 1.524
batch start
#iterations: 62
currently lose_sum: 95.32240253686905
time_elpased: 1.537
batch start
#iterations: 63
currently lose_sum: 95.29877573251724
time_elpased: 1.518
batch start
#iterations: 64
currently lose_sum: 95.29258120059967
time_elpased: 1.462
batch start
#iterations: 65
currently lose_sum: 95.17028164863586
time_elpased: 1.445
batch start
#iterations: 66
currently lose_sum: 95.18055945634842
time_elpased: 1.497
batch start
#iterations: 67
currently lose_sum: 94.93012881278992
time_elpased: 1.496
batch start
#iterations: 68
currently lose_sum: 95.04814088344574
time_elpased: 1.493
batch start
#iterations: 69
currently lose_sum: 94.87073332071304
time_elpased: 1.473
batch start
#iterations: 70
currently lose_sum: 95.29726362228394
time_elpased: 1.487
batch start
#iterations: 71
currently lose_sum: 95.02946239709854
time_elpased: 1.534
batch start
#iterations: 72
currently lose_sum: 94.91286355257034
time_elpased: 1.467
batch start
#iterations: 73
currently lose_sum: 94.98971056938171
time_elpased: 1.465
batch start
#iterations: 74
currently lose_sum: 95.22800892591476
time_elpased: 1.527
batch start
#iterations: 75
currently lose_sum: 95.01572895050049
time_elpased: 1.465
batch start
#iterations: 76
currently lose_sum: 94.9692912697792
time_elpased: 1.554
batch start
#iterations: 77
currently lose_sum: 94.81354558467865
time_elpased: 1.497
batch start
#iterations: 78
currently lose_sum: 95.25356167554855
time_elpased: 1.465
batch start
#iterations: 79
currently lose_sum: 94.94695949554443
time_elpased: 1.534
start validation test
0.655773195876
0.64684841454
0.688657883903
0.667098703888
0.655718863471
61.050
batch start
#iterations: 80
currently lose_sum: 94.98007428646088
time_elpased: 1.497
batch start
#iterations: 81
currently lose_sum: 95.20643001794815
time_elpased: 1.504
batch start
#iterations: 82
currently lose_sum: 94.9053248167038
time_elpased: 1.466
batch start
#iterations: 83
currently lose_sum: 95.00494599342346
time_elpased: 1.47
batch start
#iterations: 84
currently lose_sum: 94.78945738077164
time_elpased: 1.543
batch start
#iterations: 85
currently lose_sum: 94.9282990694046
time_elpased: 1.457
batch start
#iterations: 86
currently lose_sum: 95.05434912443161
time_elpased: 1.51
batch start
#iterations: 87
currently lose_sum: 94.54506701231003
time_elpased: 1.574
batch start
#iterations: 88
currently lose_sum: 94.61421644687653
time_elpased: 1.497
batch start
#iterations: 89
currently lose_sum: 94.78189158439636
time_elpased: 1.453
batch start
#iterations: 90
currently lose_sum: 95.09800857305527
time_elpased: 1.586
batch start
#iterations: 91
currently lose_sum: 94.78578114509583
time_elpased: 1.521
batch start
#iterations: 92
currently lose_sum: 94.8357697725296
time_elpased: 1.472
batch start
#iterations: 93
currently lose_sum: 94.31652408838272
time_elpased: 1.513
batch start
#iterations: 94
currently lose_sum: 94.9401199221611
time_elpased: 1.503
batch start
#iterations: 95
currently lose_sum: 95.0221557021141
time_elpased: 1.467
batch start
#iterations: 96
currently lose_sum: 94.44029873609543
time_elpased: 1.54
batch start
#iterations: 97
currently lose_sum: 94.67179363965988
time_elpased: 1.468
batch start
#iterations: 98
currently lose_sum: 94.68101423978806
time_elpased: 1.529
batch start
#iterations: 99
currently lose_sum: 94.52088814973831
time_elpased: 1.463
start validation test
0.665360824742
0.635053619303
0.780156442981
0.700166266396
0.665171158293
60.827
batch start
#iterations: 100
currently lose_sum: 94.90922474861145
time_elpased: 1.499
batch start
#iterations: 101
currently lose_sum: 95.17218577861786
time_elpased: 1.488
batch start
#iterations: 102
currently lose_sum: 94.8309268951416
time_elpased: 1.514
batch start
#iterations: 103
currently lose_sum: 94.95138418674469
time_elpased: 1.514
batch start
#iterations: 104
currently lose_sum: 94.60708320140839
time_elpased: 1.488
batch start
#iterations: 105
currently lose_sum: 94.57001423835754
time_elpased: 1.496
batch start
#iterations: 106
currently lose_sum: 94.56514775753021
time_elpased: 1.473
batch start
#iterations: 107
currently lose_sum: 94.68989586830139
time_elpased: 1.459
batch start
#iterations: 108
currently lose_sum: 94.57236564159393
time_elpased: 1.513
batch start
#iterations: 109
currently lose_sum: 94.66310971975327
time_elpased: 1.461
batch start
#iterations: 110
currently lose_sum: 95.22318440675735
time_elpased: 1.46
batch start
#iterations: 111
currently lose_sum: 94.50234347581863
time_elpased: 1.509
batch start
#iterations: 112
currently lose_sum: 94.69627499580383
time_elpased: 1.547
batch start
#iterations: 113
currently lose_sum: 94.40021938085556
time_elpased: 1.429
batch start
#iterations: 114
currently lose_sum: 94.56600105762482
time_elpased: 1.496
batch start
#iterations: 115
currently lose_sum: 94.47581958770752
time_elpased: 1.461
batch start
#iterations: 116
currently lose_sum: 95.09510689973831
time_elpased: 1.522
batch start
#iterations: 117
currently lose_sum: 94.16914927959442
time_elpased: 1.575
batch start
#iterations: 118
currently lose_sum: 94.76743233203888
time_elpased: 1.562
batch start
#iterations: 119
currently lose_sum: 94.72363847494125
time_elpased: 1.489
start validation test
0.665257731959
0.638807513355
0.763071222725
0.695431948222
0.665096123548
61.016
batch start
#iterations: 120
currently lose_sum: 94.49261510372162
time_elpased: 1.472
batch start
#iterations: 121
currently lose_sum: 94.31184321641922
time_elpased: 1.429
batch start
#iterations: 122
currently lose_sum: 94.79939883947372
time_elpased: 1.443
batch start
#iterations: 123
currently lose_sum: 94.52867239713669
time_elpased: 1.492
batch start
#iterations: 124
currently lose_sum: 95.02461695671082
time_elpased: 1.426
batch start
#iterations: 125
currently lose_sum: 94.74600583314896
time_elpased: 1.521
batch start
#iterations: 126
currently lose_sum: 94.11331576108932
time_elpased: 1.48
batch start
#iterations: 127
currently lose_sum: 94.48855012655258
time_elpased: 1.503
batch start
#iterations: 128
currently lose_sum: 94.5910833477974
time_elpased: 1.545
batch start
#iterations: 129
currently lose_sum: 94.553238093853
time_elpased: 1.434
batch start
#iterations: 130
currently lose_sum: 94.7078275680542
time_elpased: 1.463
batch start
#iterations: 131
currently lose_sum: 94.30787205696106
time_elpased: 1.439
batch start
#iterations: 132
currently lose_sum: 94.63081455230713
time_elpased: 1.461
batch start
#iterations: 133
currently lose_sum: 94.48206436634064
time_elpased: 1.442
batch start
#iterations: 134
currently lose_sum: 94.30559974908829
time_elpased: 1.495
batch start
#iterations: 135
currently lose_sum: 94.65877640247345
time_elpased: 1.501
batch start
#iterations: 136
currently lose_sum: 94.6777074933052
time_elpased: 1.475
batch start
#iterations: 137
currently lose_sum: 94.5275816321373
time_elpased: 1.498
batch start
#iterations: 138
currently lose_sum: 94.48738485574722
time_elpased: 1.507
batch start
#iterations: 139
currently lose_sum: 94.2526593208313
time_elpased: 1.469
start validation test
0.665412371134
0.642158159217
0.749691230959
0.691770739351
0.665273124773
60.645
batch start
#iterations: 140
currently lose_sum: 94.04430145025253
time_elpased: 1.494
batch start
#iterations: 141
currently lose_sum: 94.39550220966339
time_elpased: 1.463
batch start
#iterations: 142
currently lose_sum: 94.51862496137619
time_elpased: 1.554
batch start
#iterations: 143
currently lose_sum: 94.2609526515007
time_elpased: 1.536
batch start
#iterations: 144
currently lose_sum: 94.4353963136673
time_elpased: 1.537
batch start
#iterations: 145
currently lose_sum: 94.24188947677612
time_elpased: 1.49
batch start
#iterations: 146
currently lose_sum: 93.97887855768204
time_elpased: 1.482
batch start
#iterations: 147
currently lose_sum: 94.62109237909317
time_elpased: 1.559
batch start
#iterations: 148
currently lose_sum: 94.81701809167862
time_elpased: 1.548
batch start
#iterations: 149
currently lose_sum: 94.2468855381012
time_elpased: 1.463
batch start
#iterations: 150
currently lose_sum: 94.61985921859741
time_elpased: 1.481
batch start
#iterations: 151
currently lose_sum: 94.40061414241791
time_elpased: 1.409
batch start
#iterations: 152
currently lose_sum: 94.49364453554153
time_elpased: 1.542
batch start
#iterations: 153
currently lose_sum: 94.45373249053955
time_elpased: 1.448
batch start
#iterations: 154
currently lose_sum: 94.93675470352173
time_elpased: 1.496
batch start
#iterations: 155
currently lose_sum: 94.21492683887482
time_elpased: 1.51
batch start
#iterations: 156
currently lose_sum: 94.53370922803879
time_elpased: 1.502
batch start
#iterations: 157
currently lose_sum: 93.94678407907486
time_elpased: 1.477
batch start
#iterations: 158
currently lose_sum: 94.56695437431335
time_elpased: 1.483
batch start
#iterations: 159
currently lose_sum: 94.27801871299744
time_elpased: 1.474
start validation test
0.66412371134
0.636286201022
0.768834911486
0.696308724832
0.663950706466
60.806
batch start
#iterations: 160
currently lose_sum: 94.1618064045906
time_elpased: 1.479
batch start
#iterations: 161
currently lose_sum: 94.70631057024002
time_elpased: 1.448
batch start
#iterations: 162
currently lose_sum: 94.4892743229866
time_elpased: 1.472
batch start
#iterations: 163
currently lose_sum: 94.19335889816284
time_elpased: 1.454
batch start
#iterations: 164
currently lose_sum: 94.64865040779114
time_elpased: 1.51
batch start
#iterations: 165
currently lose_sum: 94.36583542823792
time_elpased: 1.444
batch start
#iterations: 166
currently lose_sum: 94.33195716142654
time_elpased: 1.478
batch start
#iterations: 167
currently lose_sum: 94.1750059723854
time_elpased: 1.591
batch start
#iterations: 168
currently lose_sum: 94.66559320688248
time_elpased: 1.535
batch start
#iterations: 169
currently lose_sum: 94.29591596126556
time_elpased: 1.494
batch start
#iterations: 170
currently lose_sum: 94.38148415088654
time_elpased: 1.477
batch start
#iterations: 171
currently lose_sum: 94.40530788898468
time_elpased: 1.44
batch start
#iterations: 172
currently lose_sum: 94.13065147399902
time_elpased: 1.493
batch start
#iterations: 173
currently lose_sum: 94.35910594463348
time_elpased: 1.519
batch start
#iterations: 174
currently lose_sum: 93.81003820896149
time_elpased: 1.51
batch start
#iterations: 175
currently lose_sum: 93.85567486286163
time_elpased: 1.479
batch start
#iterations: 176
currently lose_sum: 94.6525547504425
time_elpased: 1.488
batch start
#iterations: 177
currently lose_sum: 94.55980396270752
time_elpased: 1.559
batch start
#iterations: 178
currently lose_sum: 94.22084885835648
time_elpased: 1.532
batch start
#iterations: 179
currently lose_sum: 94.28843283653259
time_elpased: 1.532
start validation test
0.662783505155
0.638723776224
0.752058460272
0.690773303082
0.662636004196
60.824
batch start
#iterations: 180
currently lose_sum: 94.06769454479218
time_elpased: 1.482
batch start
#iterations: 181
currently lose_sum: 94.43141865730286
time_elpased: 1.485
batch start
#iterations: 182
currently lose_sum: 94.83779609203339
time_elpased: 1.552
batch start
#iterations: 183
currently lose_sum: 94.03158164024353
time_elpased: 1.492
batch start
#iterations: 184
currently lose_sum: 94.24281579256058
time_elpased: 1.503
batch start
#iterations: 185
currently lose_sum: 94.61016696691513
time_elpased: 1.543
batch start
#iterations: 186
currently lose_sum: 93.82241410017014
time_elpased: 1.452
batch start
#iterations: 187
currently lose_sum: 94.40596199035645
time_elpased: 1.432
batch start
#iterations: 188
currently lose_sum: 94.45531189441681
time_elpased: 1.526
batch start
#iterations: 189
currently lose_sum: 94.38382977247238
time_elpased: 1.539
batch start
#iterations: 190
currently lose_sum: 94.45772486925125
time_elpased: 1.512
batch start
#iterations: 191
currently lose_sum: 94.21453958749771
time_elpased: 1.484
batch start
#iterations: 192
currently lose_sum: 94.4351698756218
time_elpased: 1.511
batch start
#iterations: 193
currently lose_sum: 94.05489385128021
time_elpased: 1.507
batch start
#iterations: 194
currently lose_sum: 94.08226704597473
time_elpased: 1.509
batch start
#iterations: 195
currently lose_sum: 94.23491179943085
time_elpased: 1.458
batch start
#iterations: 196
currently lose_sum: 93.8536388874054
time_elpased: 1.528
batch start
#iterations: 197
currently lose_sum: 94.12489545345306
time_elpased: 1.44
batch start
#iterations: 198
currently lose_sum: 94.18522495031357
time_elpased: 1.566
batch start
#iterations: 199
currently lose_sum: 93.8460950255394
time_elpased: 1.492
start validation test
0.669742268041
0.64296206688
0.765850144092
0.699046455916
0.669583477664
60.782
batch start
#iterations: 200
currently lose_sum: 94.39762818813324
time_elpased: 1.574
batch start
#iterations: 201
currently lose_sum: 93.86049121618271
time_elpased: 1.503
batch start
#iterations: 202
currently lose_sum: 94.36472916603088
time_elpased: 1.497
batch start
#iterations: 203
currently lose_sum: 94.60703599452972
time_elpased: 1.454
batch start
#iterations: 204
currently lose_sum: 94.2690280675888
time_elpased: 1.547
batch start
#iterations: 205
currently lose_sum: 94.70055890083313
time_elpased: 1.444
batch start
#iterations: 206
currently lose_sum: 93.83698761463165
time_elpased: 1.524
batch start
#iterations: 207
currently lose_sum: 94.02293503284454
time_elpased: 1.514
batch start
#iterations: 208
currently lose_sum: 94.14537078142166
time_elpased: 1.495
batch start
#iterations: 209
currently lose_sum: 94.29765605926514
time_elpased: 1.506
batch start
#iterations: 210
currently lose_sum: 94.07247859239578
time_elpased: 1.519
batch start
#iterations: 211
currently lose_sum: 93.41720539331436
time_elpased: 1.441
batch start
#iterations: 212
currently lose_sum: 94.3424881696701
time_elpased: 1.46
batch start
#iterations: 213
currently lose_sum: 94.36656498908997
time_elpased: 1.457
batch start
#iterations: 214
currently lose_sum: 94.42973566055298
time_elpased: 1.572
batch start
#iterations: 215
currently lose_sum: 94.04924803972244
time_elpased: 1.485
batch start
#iterations: 216
currently lose_sum: 93.66297030448914
time_elpased: 1.533
batch start
#iterations: 217
currently lose_sum: 93.95346355438232
time_elpased: 1.483
batch start
#iterations: 218
currently lose_sum: 94.42793476581573
time_elpased: 1.453
batch start
#iterations: 219
currently lose_sum: 94.13620489835739
time_elpased: 1.473
start validation test
0.668969072165
0.644397685429
0.756484149856
0.695956822271
0.668824478893
60.700
batch start
#iterations: 220
currently lose_sum: 94.47596174478531
time_elpased: 1.485
batch start
#iterations: 221
currently lose_sum: 94.04252910614014
time_elpased: 1.461
batch start
#iterations: 222
currently lose_sum: 94.1131711602211
time_elpased: 1.48
batch start
#iterations: 223
currently lose_sum: 93.36729788780212
time_elpased: 1.578
batch start
#iterations: 224
currently lose_sum: 94.07816588878632
time_elpased: 1.447
batch start
#iterations: 225
currently lose_sum: 93.91746860742569
time_elpased: 1.519
batch start
#iterations: 226
currently lose_sum: 93.86307656764984
time_elpased: 1.511
batch start
#iterations: 227
currently lose_sum: 94.0459657907486
time_elpased: 1.472
batch start
#iterations: 228
currently lose_sum: 93.96552401781082
time_elpased: 1.514
batch start
#iterations: 229
currently lose_sum: 93.62901657819748
time_elpased: 1.395
batch start
#iterations: 230
currently lose_sum: 94.0612763762474
time_elpased: 1.457
batch start
#iterations: 231
currently lose_sum: 94.32701975107193
time_elpased: 1.547
batch start
#iterations: 232
currently lose_sum: 94.44666665792465
time_elpased: 1.431
batch start
#iterations: 233
currently lose_sum: 94.28704434633255
time_elpased: 1.448
batch start
#iterations: 234
currently lose_sum: 94.12094503641129
time_elpased: 1.473
batch start
#iterations: 235
currently lose_sum: 94.3255587220192
time_elpased: 1.492
batch start
#iterations: 236
currently lose_sum: 94.17181259393692
time_elpased: 1.483
batch start
#iterations: 237
currently lose_sum: 93.8008708357811
time_elpased: 1.424
batch start
#iterations: 238
currently lose_sum: 94.115309715271
time_elpased: 1.464
batch start
#iterations: 239
currently lose_sum: 94.36576616764069
time_elpased: 1.511
start validation test
0.671494845361
0.642049800289
0.777583367641
0.703346832379
0.671319564861
60.723
batch start
#iterations: 240
currently lose_sum: 93.96790438890457
time_elpased: 1.513
batch start
#iterations: 241
currently lose_sum: 93.83022713661194
time_elpased: 1.496
batch start
#iterations: 242
currently lose_sum: 93.92046159505844
time_elpased: 1.486
batch start
#iterations: 243
currently lose_sum: 93.98040664196014
time_elpased: 1.45
batch start
#iterations: 244
currently lose_sum: 93.91889584064484
time_elpased: 1.5
batch start
#iterations: 245
currently lose_sum: 94.19537824392319
time_elpased: 1.482
batch start
#iterations: 246
currently lose_sum: 93.85586553812027
time_elpased: 1.516
batch start
#iterations: 247
currently lose_sum: 94.08088493347168
time_elpased: 1.477
batch start
#iterations: 248
currently lose_sum: 94.21923643350601
time_elpased: 1.483
batch start
#iterations: 249
currently lose_sum: 93.99730414152145
time_elpased: 1.495
batch start
#iterations: 250
currently lose_sum: 94.34489494562149
time_elpased: 1.485
batch start
#iterations: 251
currently lose_sum: 93.86818689107895
time_elpased: 1.498
batch start
#iterations: 252
currently lose_sum: 94.17818248271942
time_elpased: 1.465
batch start
#iterations: 253
currently lose_sum: 93.9951143860817
time_elpased: 1.423
batch start
#iterations: 254
currently lose_sum: 93.93974924087524
time_elpased: 1.48
batch start
#iterations: 255
currently lose_sum: 94.05237048864365
time_elpased: 1.468
batch start
#iterations: 256
currently lose_sum: 93.77117908000946
time_elpased: 1.531
batch start
#iterations: 257
currently lose_sum: 93.60266798734665
time_elpased: 1.511
batch start
#iterations: 258
currently lose_sum: 94.17894899845123
time_elpased: 1.449
batch start
#iterations: 259
currently lose_sum: 94.08990305662155
time_elpased: 1.457
start validation test
0.665154639175
0.637021276596
0.77037875669
0.69738190627
0.664980786854
60.971
batch start
#iterations: 260
currently lose_sum: 93.99592477083206
time_elpased: 1.44
batch start
#iterations: 261
currently lose_sum: 93.9230409860611
time_elpased: 1.536
batch start
#iterations: 262
currently lose_sum: 93.583003282547
time_elpased: 1.565
batch start
#iterations: 263
currently lose_sum: 93.77980571985245
time_elpased: 1.546
batch start
#iterations: 264
currently lose_sum: 94.09396260976791
time_elpased: 1.499
batch start
#iterations: 265
currently lose_sum: 94.42742848396301
time_elpased: 1.465
batch start
#iterations: 266
currently lose_sum: 93.62104564905167
time_elpased: 1.508
batch start
#iterations: 267
currently lose_sum: 94.24439418315887
time_elpased: 1.479
batch start
#iterations: 268
currently lose_sum: 94.05617076158524
time_elpased: 1.484
batch start
#iterations: 269
currently lose_sum: 94.22576040029526
time_elpased: 1.457
batch start
#iterations: 270
currently lose_sum: 93.68930208683014
time_elpased: 1.458
batch start
#iterations: 271
currently lose_sum: 94.07142901420593
time_elpased: 1.454
batch start
#iterations: 272
currently lose_sum: 93.98905009031296
time_elpased: 1.489
batch start
#iterations: 273
currently lose_sum: 93.84590715169907
time_elpased: 1.501
batch start
#iterations: 274
currently lose_sum: 93.62158519029617
time_elpased: 1.422
batch start
#iterations: 275
currently lose_sum: 94.08501452207565
time_elpased: 1.479
batch start
#iterations: 276
currently lose_sum: 93.91560530662537
time_elpased: 1.527
batch start
#iterations: 277
currently lose_sum: 93.55484300851822
time_elpased: 1.478
batch start
#iterations: 278
currently lose_sum: 94.2094556093216
time_elpased: 1.492
batch start
#iterations: 279
currently lose_sum: 94.08281928300858
time_elpased: 1.447
start validation test
0.667577319588
0.636935199933
0.782009057225
0.702055902056
0.667388254346
60.750
batch start
#iterations: 280
currently lose_sum: 93.68118292093277
time_elpased: 1.517
batch start
#iterations: 281
currently lose_sum: 93.53251546621323
time_elpased: 1.46
batch start
#iterations: 282
currently lose_sum: 93.83662116527557
time_elpased: 1.45
batch start
#iterations: 283
currently lose_sum: 94.26390564441681
time_elpased: 1.51
batch start
#iterations: 284
currently lose_sum: 93.75984013080597
time_elpased: 1.522
batch start
#iterations: 285
currently lose_sum: 93.56849443912506
time_elpased: 1.43
batch start
#iterations: 286
currently lose_sum: 93.458764731884
time_elpased: 1.498
batch start
#iterations: 287
currently lose_sum: 93.66822803020477
time_elpased: 1.498
batch start
#iterations: 288
currently lose_sum: 93.97698187828064
time_elpased: 1.465
batch start
#iterations: 289
currently lose_sum: 93.91194272041321
time_elpased: 1.474
batch start
#iterations: 290
currently lose_sum: 94.08235013484955
time_elpased: 1.484
batch start
#iterations: 291
currently lose_sum: 93.76818639039993
time_elpased: 1.456
batch start
#iterations: 292
currently lose_sum: 93.7947307229042
time_elpased: 1.557
batch start
#iterations: 293
currently lose_sum: 93.76931971311569
time_elpased: 1.463
batch start
#iterations: 294
currently lose_sum: 94.11039745807648
time_elpased: 1.452
batch start
#iterations: 295
currently lose_sum: 93.97842389345169
time_elpased: 1.564
batch start
#iterations: 296
currently lose_sum: 93.91136246919632
time_elpased: 1.507
batch start
#iterations: 297
currently lose_sum: 94.12892538309097
time_elpased: 1.487
batch start
#iterations: 298
currently lose_sum: 93.68505364656448
time_elpased: 1.571
batch start
#iterations: 299
currently lose_sum: 93.85335999727249
time_elpased: 1.509
start validation test
0.666082474227
0.641101621056
0.757101687937
0.69428975932
0.665932091387
60.976
batch start
#iterations: 300
currently lose_sum: 94.18740206956863
time_elpased: 1.512
batch start
#iterations: 301
currently lose_sum: 93.71986043453217
time_elpased: 1.445
batch start
#iterations: 302
currently lose_sum: 93.6393313407898
time_elpased: 1.547
batch start
#iterations: 303
currently lose_sum: 93.40146261453629
time_elpased: 1.482
batch start
#iterations: 304
currently lose_sum: 93.15165591239929
time_elpased: 1.429
batch start
#iterations: 305
currently lose_sum: 93.55338782072067
time_elpased: 1.468
batch start
#iterations: 306
currently lose_sum: 93.8271073102951
time_elpased: 1.546
batch start
#iterations: 307
currently lose_sum: 94.17344236373901
time_elpased: 1.54
batch start
#iterations: 308
currently lose_sum: 93.68556350469589
time_elpased: 1.583
batch start
#iterations: 309
currently lose_sum: 93.54761910438538
time_elpased: 1.457
batch start
#iterations: 310
currently lose_sum: 93.85541123151779
time_elpased: 1.508
batch start
#iterations: 311
currently lose_sum: 93.58491265773773
time_elpased: 1.493
batch start
#iterations: 312
currently lose_sum: 93.73573929071426
time_elpased: 1.447
batch start
#iterations: 313
currently lose_sum: 93.93176352977753
time_elpased: 1.483
batch start
#iterations: 314
currently lose_sum: 93.93059796094894
time_elpased: 1.469
batch start
#iterations: 315
currently lose_sum: 93.3290069103241
time_elpased: 1.424
batch start
#iterations: 316
currently lose_sum: 93.73611271381378
time_elpased: 1.471
batch start
#iterations: 317
currently lose_sum: 93.61873227357864
time_elpased: 1.49
batch start
#iterations: 318
currently lose_sum: 93.71146041154861
time_elpased: 1.557
batch start
#iterations: 319
currently lose_sum: 93.95036566257477
time_elpased: 1.425
start validation test
0.666443298969
0.640342530923
0.761939069576
0.695868778493
0.666285519918
60.863
batch start
#iterations: 320
currently lose_sum: 94.07276952266693
time_elpased: 1.478
batch start
#iterations: 321
currently lose_sum: 93.8990797996521
time_elpased: 1.47
batch start
#iterations: 322
currently lose_sum: 93.65034908056259
time_elpased: 1.466
batch start
#iterations: 323
currently lose_sum: 93.61380904912949
time_elpased: 1.428
batch start
#iterations: 324
currently lose_sum: 93.6616023182869
time_elpased: 1.513
batch start
#iterations: 325
currently lose_sum: 93.7543016076088
time_elpased: 1.527
batch start
#iterations: 326
currently lose_sum: 93.25370877981186
time_elpased: 1.46
batch start
#iterations: 327
currently lose_sum: 93.96243697404861
time_elpased: 1.518
batch start
#iterations: 328
currently lose_sum: 93.72949582338333
time_elpased: 1.543
batch start
#iterations: 329
currently lose_sum: 93.9103364944458
time_elpased: 1.462
batch start
#iterations: 330
currently lose_sum: 93.67459481954575
time_elpased: 1.529
batch start
#iterations: 331
currently lose_sum: 93.96070581674576
time_elpased: 1.531
batch start
#iterations: 332
currently lose_sum: 93.87437522411346
time_elpased: 1.514
batch start
#iterations: 333
currently lose_sum: 93.83693158626556
time_elpased: 1.492
batch start
#iterations: 334
currently lose_sum: 93.73199379444122
time_elpased: 1.531
batch start
#iterations: 335
currently lose_sum: 93.3899410367012
time_elpased: 1.429
batch start
#iterations: 336
currently lose_sum: 93.7350013256073
time_elpased: 1.459
batch start
#iterations: 337
currently lose_sum: 93.55072808265686
time_elpased: 1.573
batch start
#iterations: 338
currently lose_sum: 93.25408017635345
time_elpased: 1.487
batch start
#iterations: 339
currently lose_sum: 93.79206997156143
time_elpased: 1.497
start validation test
0.667164948454
0.640777537797
0.763379991766
0.696726316284
0.667005981013
60.812
batch start
#iterations: 340
currently lose_sum: 94.08605659008026
time_elpased: 1.488
batch start
#iterations: 341
currently lose_sum: 93.69757854938507
time_elpased: 1.519
batch start
#iterations: 342
currently lose_sum: 93.96118724346161
time_elpased: 1.498
batch start
#iterations: 343
currently lose_sum: 94.26548081636429
time_elpased: 1.528
batch start
#iterations: 344
currently lose_sum: 93.90277808904648
time_elpased: 1.592
batch start
#iterations: 345
currently lose_sum: 93.87614995241165
time_elpased: 1.408
batch start
#iterations: 346
currently lose_sum: 93.32410264015198
time_elpased: 1.527
batch start
#iterations: 347
currently lose_sum: 93.69349879026413
time_elpased: 1.544
batch start
#iterations: 348
currently lose_sum: 93.69352155923843
time_elpased: 1.453
batch start
#iterations: 349
currently lose_sum: 94.02375811338425
time_elpased: 1.505
batch start
#iterations: 350
currently lose_sum: 93.88869267702103
time_elpased: 1.454
batch start
#iterations: 351
currently lose_sum: 93.93379628658295
time_elpased: 1.463
batch start
#iterations: 352
currently lose_sum: 93.8089029788971
time_elpased: 1.526
batch start
#iterations: 353
currently lose_sum: 94.33922302722931
time_elpased: 1.468
batch start
#iterations: 354
currently lose_sum: 93.73588049411774
time_elpased: 1.48
batch start
#iterations: 355
currently lose_sum: 93.59168034791946
time_elpased: 1.529
batch start
#iterations: 356
currently lose_sum: 93.61472600698471
time_elpased: 1.475
batch start
#iterations: 357
currently lose_sum: 93.91646844148636
time_elpased: 1.55
batch start
#iterations: 358
currently lose_sum: 93.79168528318405
time_elpased: 1.546
batch start
#iterations: 359
currently lose_sum: 93.13044703006744
time_elpased: 1.503
start validation test
0.670309278351
0.642930945411
0.768526142445
0.700140646976
0.670147003482
60.879
batch start
#iterations: 360
currently lose_sum: 93.90177065134048
time_elpased: 1.498
batch start
#iterations: 361
currently lose_sum: 93.89268440008163
time_elpased: 1.48
batch start
#iterations: 362
currently lose_sum: 93.91248190402985
time_elpased: 1.528
batch start
#iterations: 363
currently lose_sum: 93.91850280761719
time_elpased: 1.487
batch start
#iterations: 364
currently lose_sum: 93.51337790489197
time_elpased: 1.438
batch start
#iterations: 365
currently lose_sum: 93.90655839443207
time_elpased: 1.516
batch start
#iterations: 366
currently lose_sum: 94.0054212808609
time_elpased: 1.491
batch start
#iterations: 367
currently lose_sum: 93.56395316123962
time_elpased: 1.468
batch start
#iterations: 368
currently lose_sum: 93.88667333126068
time_elpased: 1.489
batch start
#iterations: 369
currently lose_sum: 93.87922018766403
time_elpased: 1.514
batch start
#iterations: 370
currently lose_sum: 93.85638070106506
time_elpased: 1.493
batch start
#iterations: 371
currently lose_sum: 93.54414039850235
time_elpased: 1.469
batch start
#iterations: 372
currently lose_sum: 93.50042498111725
time_elpased: 1.431
batch start
#iterations: 373
currently lose_sum: 93.92419004440308
time_elpased: 1.478
batch start
#iterations: 374
currently lose_sum: 93.80751568078995
time_elpased: 1.46
batch start
#iterations: 375
currently lose_sum: 93.70130509138107
time_elpased: 1.484
batch start
#iterations: 376
currently lose_sum: 93.10955482721329
time_elpased: 1.457
batch start
#iterations: 377
currently lose_sum: 93.37322264909744
time_elpased: 1.48
batch start
#iterations: 378
currently lose_sum: 93.97580885887146
time_elpased: 1.561
batch start
#iterations: 379
currently lose_sum: 93.65891563892365
time_elpased: 1.512
start validation test
0.664536082474
0.641172328815
0.749794153973
0.691242053326
0.66439521825
60.853
batch start
#iterations: 380
currently lose_sum: 93.69204443693161
time_elpased: 1.507
batch start
#iterations: 381
currently lose_sum: 93.50025659799576
time_elpased: 1.449
batch start
#iterations: 382
currently lose_sum: 93.89745289087296
time_elpased: 1.43
batch start
#iterations: 383
currently lose_sum: 93.60575008392334
time_elpased: 1.473
batch start
#iterations: 384
currently lose_sum: 93.43860179185867
time_elpased: 1.437
batch start
#iterations: 385
currently lose_sum: 93.75133442878723
time_elpased: 1.491
batch start
#iterations: 386
currently lose_sum: 93.72197967767715
time_elpased: 1.442
batch start
#iterations: 387
currently lose_sum: 93.47984743118286
time_elpased: 1.447
batch start
#iterations: 388
currently lose_sum: 93.89100652933121
time_elpased: 1.44
batch start
#iterations: 389
currently lose_sum: 93.57035261392593
time_elpased: 1.48
batch start
#iterations: 390
currently lose_sum: 93.80836987495422
time_elpased: 1.469
batch start
#iterations: 391
currently lose_sum: 93.2593297958374
time_elpased: 1.485
batch start
#iterations: 392
currently lose_sum: 94.02065348625183
time_elpased: 1.454
batch start
#iterations: 393
currently lose_sum: 93.40964621305466
time_elpased: 1.438
batch start
#iterations: 394
currently lose_sum: 93.32868981361389
time_elpased: 1.528
batch start
#iterations: 395
currently lose_sum: 93.49532228708267
time_elpased: 1.452
batch start
#iterations: 396
currently lose_sum: 93.58610451221466
time_elpased: 1.444
batch start
#iterations: 397
currently lose_sum: 93.77818936109543
time_elpased: 1.489
batch start
#iterations: 398
currently lose_sum: 93.89663195610046
time_elpased: 1.476
batch start
#iterations: 399
currently lose_sum: 93.48992049694061
time_elpased: 1.457
start validation test
0.663350515464
0.642914834425
0.737340469329
0.686897741982
0.663228268535
61.020
acc: 0.668
pre: 0.643
rec: 0.759
F1: 0.696
auc: 0.706
