start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.07146269083023
time_elpased: 1.783
batch start
#iterations: 1
currently lose_sum: 99.31378537416458
time_elpased: 1.779
batch start
#iterations: 2
currently lose_sum: 99.06356304883957
time_elpased: 1.758
batch start
#iterations: 3
currently lose_sum: 98.79384315013885
time_elpased: 1.769
batch start
#iterations: 4
currently lose_sum: 98.39344727993011
time_elpased: 1.757
batch start
#iterations: 5
currently lose_sum: 97.9116108417511
time_elpased: 1.782
batch start
#iterations: 6
currently lose_sum: 97.8459125161171
time_elpased: 1.757
batch start
#iterations: 7
currently lose_sum: 97.61625230312347
time_elpased: 1.79
batch start
#iterations: 8
currently lose_sum: 97.51142245531082
time_elpased: 1.725
batch start
#iterations: 9
currently lose_sum: 97.26121413707733
time_elpased: 1.818
batch start
#iterations: 10
currently lose_sum: 97.30328869819641
time_elpased: 1.791
batch start
#iterations: 11
currently lose_sum: 97.20242565870285
time_elpased: 1.794
batch start
#iterations: 12
currently lose_sum: 96.94648742675781
time_elpased: 1.787
batch start
#iterations: 13
currently lose_sum: 96.8878378868103
time_elpased: 1.747
batch start
#iterations: 14
currently lose_sum: 96.65931099653244
time_elpased: 1.803
batch start
#iterations: 15
currently lose_sum: 96.79623800516129
time_elpased: 1.771
batch start
#iterations: 16
currently lose_sum: 96.71916669607162
time_elpased: 1.733
batch start
#iterations: 17
currently lose_sum: 96.6573371887207
time_elpased: 1.72
batch start
#iterations: 18
currently lose_sum: 96.26370871067047
time_elpased: 1.77
batch start
#iterations: 19
currently lose_sum: 96.58257639408112
time_elpased: 1.754
start validation test
0.642731958763
0.63178807947
0.687249150973
0.658352639621
0.642653801966
62.194
batch start
#iterations: 20
currently lose_sum: 96.39555424451828
time_elpased: 1.797
batch start
#iterations: 21
currently lose_sum: 96.1771674156189
time_elpased: 1.746
batch start
#iterations: 22
currently lose_sum: 96.54781717061996
time_elpased: 1.77
batch start
#iterations: 23
currently lose_sum: 95.88248962163925
time_elpased: 1.736
batch start
#iterations: 24
currently lose_sum: 95.89999204874039
time_elpased: 1.736
batch start
#iterations: 25
currently lose_sum: 96.10024064779282
time_elpased: 1.73
batch start
#iterations: 26
currently lose_sum: 96.11713546514511
time_elpased: 1.766
batch start
#iterations: 27
currently lose_sum: 96.0244882106781
time_elpased: 1.788
batch start
#iterations: 28
currently lose_sum: 95.90683460235596
time_elpased: 1.746
batch start
#iterations: 29
currently lose_sum: 95.87303078174591
time_elpased: 1.753
batch start
#iterations: 30
currently lose_sum: 95.82099986076355
time_elpased: 1.755
batch start
#iterations: 31
currently lose_sum: 96.00510275363922
time_elpased: 1.774
batch start
#iterations: 32
currently lose_sum: 95.54232597351074
time_elpased: 1.766
batch start
#iterations: 33
currently lose_sum: 96.11259996891022
time_elpased: 1.743
batch start
#iterations: 34
currently lose_sum: 95.82494658231735
time_elpased: 1.763
batch start
#iterations: 35
currently lose_sum: 95.54010534286499
time_elpased: 1.793
batch start
#iterations: 36
currently lose_sum: 95.26837176084518
time_elpased: 1.749
batch start
#iterations: 37
currently lose_sum: 95.85966956615448
time_elpased: 1.813
batch start
#iterations: 38
currently lose_sum: 95.50382208824158
time_elpased: 1.778
batch start
#iterations: 39
currently lose_sum: 95.35628885030746
time_elpased: 1.752
start validation test
0.658092783505
0.634133611691
0.750231552948
0.68731438269
0.657931019684
61.209
batch start
#iterations: 40
currently lose_sum: 95.57435262203217
time_elpased: 1.763
batch start
#iterations: 41
currently lose_sum: 95.29253429174423
time_elpased: 1.773
batch start
#iterations: 42
currently lose_sum: 95.29194378852844
time_elpased: 1.78
batch start
#iterations: 43
currently lose_sum: 95.16810637712479
time_elpased: 1.792
batch start
#iterations: 44
currently lose_sum: 95.6283415555954
time_elpased: 1.742
batch start
#iterations: 45
currently lose_sum: 95.2692603468895
time_elpased: 1.783
batch start
#iterations: 46
currently lose_sum: 95.37263756990433
time_elpased: 1.757
batch start
#iterations: 47
currently lose_sum: 95.50462603569031
time_elpased: 1.755
batch start
#iterations: 48
currently lose_sum: 95.63627249002457
time_elpased: 1.802
batch start
#iterations: 49
currently lose_sum: 95.27173006534576
time_elpased: 1.771
batch start
#iterations: 50
currently lose_sum: 95.35057419538498
time_elpased: 1.777
batch start
#iterations: 51
currently lose_sum: 95.34071558713913
time_elpased: 1.739
batch start
#iterations: 52
currently lose_sum: 95.6362841129303
time_elpased: 1.767
batch start
#iterations: 53
currently lose_sum: 94.96298557519913
time_elpased: 1.776
batch start
#iterations: 54
currently lose_sum: 95.2246145606041
time_elpased: 1.767
batch start
#iterations: 55
currently lose_sum: 95.66885143518448
time_elpased: 1.773
batch start
#iterations: 56
currently lose_sum: 94.9996846318245
time_elpased: 1.753
batch start
#iterations: 57
currently lose_sum: 95.09637463092804
time_elpased: 1.787
batch start
#iterations: 58
currently lose_sum: 95.11103403568268
time_elpased: 1.785
batch start
#iterations: 59
currently lose_sum: 95.33245331048965
time_elpased: 1.761
start validation test
0.659845360825
0.634095991743
0.758670371514
0.690811975823
0.659671858276
61.035
batch start
#iterations: 60
currently lose_sum: 95.03976368904114
time_elpased: 1.768
batch start
#iterations: 61
currently lose_sum: 95.34933030605316
time_elpased: 1.776
batch start
#iterations: 62
currently lose_sum: 94.9642214179039
time_elpased: 1.761
batch start
#iterations: 63
currently lose_sum: 94.9292961359024
time_elpased: 1.75
batch start
#iterations: 64
currently lose_sum: 95.15067809820175
time_elpased: 1.808
batch start
#iterations: 65
currently lose_sum: 95.09607428312302
time_elpased: 1.749
batch start
#iterations: 66
currently lose_sum: 95.43324393033981
time_elpased: 1.769
batch start
#iterations: 67
currently lose_sum: 95.20733207464218
time_elpased: 1.776
batch start
#iterations: 68
currently lose_sum: 94.99378913640976
time_elpased: 1.808
batch start
#iterations: 69
currently lose_sum: 94.77587658166885
time_elpased: 1.782
batch start
#iterations: 70
currently lose_sum: 94.76644110679626
time_elpased: 1.778
batch start
#iterations: 71
currently lose_sum: 94.97955471277237
time_elpased: 1.75
batch start
#iterations: 72
currently lose_sum: 95.1699812412262
time_elpased: 1.788
batch start
#iterations: 73
currently lose_sum: 94.5110719203949
time_elpased: 1.761
batch start
#iterations: 74
currently lose_sum: 95.12811332941055
time_elpased: 1.712
batch start
#iterations: 75
currently lose_sum: 95.06542330980301
time_elpased: 1.754
batch start
#iterations: 76
currently lose_sum: 95.07883900403976
time_elpased: 1.767
batch start
#iterations: 77
currently lose_sum: 94.96661305427551
time_elpased: 1.756
batch start
#iterations: 78
currently lose_sum: 95.08465480804443
time_elpased: 1.776
batch start
#iterations: 79
currently lose_sum: 94.66638392210007
time_elpased: 1.79
start validation test
0.660979381443
0.632310803978
0.772151898734
0.69526942501
0.660784200942
60.901
batch start
#iterations: 80
currently lose_sum: 94.65385210514069
time_elpased: 1.751
batch start
#iterations: 81
currently lose_sum: 95.02372288703918
time_elpased: 1.788
batch start
#iterations: 82
currently lose_sum: 94.74028420448303
time_elpased: 1.738
batch start
#iterations: 83
currently lose_sum: 95.09115725755692
time_elpased: 1.752
batch start
#iterations: 84
currently lose_sum: 94.65607964992523
time_elpased: 1.761
batch start
#iterations: 85
currently lose_sum: 94.87959986925125
time_elpased: 1.805
batch start
#iterations: 86
currently lose_sum: 95.24075722694397
time_elpased: 1.787
batch start
#iterations: 87
currently lose_sum: 94.76731687784195
time_elpased: 1.751
batch start
#iterations: 88
currently lose_sum: 95.08446288108826
time_elpased: 1.776
batch start
#iterations: 89
currently lose_sum: 94.56922352313995
time_elpased: 1.791
batch start
#iterations: 90
currently lose_sum: 94.21412587165833
time_elpased: 1.785
batch start
#iterations: 91
currently lose_sum: 94.86251020431519
time_elpased: 1.789
batch start
#iterations: 92
currently lose_sum: 94.67431688308716
time_elpased: 1.806
batch start
#iterations: 93
currently lose_sum: 95.25916075706482
time_elpased: 1.736
batch start
#iterations: 94
currently lose_sum: 94.61001372337341
time_elpased: 1.76
batch start
#iterations: 95
currently lose_sum: 94.78333985805511
time_elpased: 1.76
batch start
#iterations: 96
currently lose_sum: 95.01360529661179
time_elpased: 1.757
batch start
#iterations: 97
currently lose_sum: 94.24837666749954
time_elpased: 1.763
batch start
#iterations: 98
currently lose_sum: 94.03969901800156
time_elpased: 1.75
batch start
#iterations: 99
currently lose_sum: 94.41325157880783
time_elpased: 1.771
start validation test
0.650412371134
0.64028295574
0.689307399403
0.663891366835
0.650344084913
61.089
batch start
#iterations: 100
currently lose_sum: 94.7609875202179
time_elpased: 1.79
batch start
#iterations: 101
currently lose_sum: 94.60223895311356
time_elpased: 1.746
batch start
#iterations: 102
currently lose_sum: 94.15624797344208
time_elpased: 1.771
batch start
#iterations: 103
currently lose_sum: 94.61127692461014
time_elpased: 1.75
batch start
#iterations: 104
currently lose_sum: 94.55168437957764
time_elpased: 1.744
batch start
#iterations: 105
currently lose_sum: 94.48130804300308
time_elpased: 1.773
batch start
#iterations: 106
currently lose_sum: 95.03879982233047
time_elpased: 1.738
batch start
#iterations: 107
currently lose_sum: 94.92011368274689
time_elpased: 1.79
batch start
#iterations: 108
currently lose_sum: 94.37090867757797
time_elpased: 1.806
batch start
#iterations: 109
currently lose_sum: 94.13836973905563
time_elpased: 1.762
batch start
#iterations: 110
currently lose_sum: 94.34994387626648
time_elpased: 1.77
batch start
#iterations: 111
currently lose_sum: 94.67436617612839
time_elpased: 1.782
batch start
#iterations: 112
currently lose_sum: 94.88948076963425
time_elpased: 1.798
batch start
#iterations: 113
currently lose_sum: 94.5162365436554
time_elpased: 1.787
batch start
#iterations: 114
currently lose_sum: 94.39000362157822
time_elpased: 1.766
batch start
#iterations: 115
currently lose_sum: 94.07951378822327
time_elpased: 1.822
batch start
#iterations: 116
currently lose_sum: 94.84763032197952
time_elpased: 1.796
batch start
#iterations: 117
currently lose_sum: 94.78118246793747
time_elpased: 1.77
batch start
#iterations: 118
currently lose_sum: 94.2494768500328
time_elpased: 1.801
batch start
#iterations: 119
currently lose_sum: 94.52971023321152
time_elpased: 1.738
start validation test
0.649432989691
0.640706427331
0.683235566533
0.661287912745
0.649373644053
61.228
batch start
#iterations: 120
currently lose_sum: 94.23664617538452
time_elpased: 1.755
batch start
#iterations: 121
currently lose_sum: 94.3891287446022
time_elpased: 1.764
batch start
#iterations: 122
currently lose_sum: 94.53229928016663
time_elpased: 1.761
batch start
#iterations: 123
currently lose_sum: 94.53037542104721
time_elpased: 1.761
batch start
#iterations: 124
currently lose_sum: 94.37105244398117
time_elpased: 1.77
batch start
#iterations: 125
currently lose_sum: 94.72104924917221
time_elpased: 1.789
batch start
#iterations: 126
currently lose_sum: 94.26671636104584
time_elpased: 1.743
batch start
#iterations: 127
currently lose_sum: 94.12507599592209
time_elpased: 1.788
batch start
#iterations: 128
currently lose_sum: 94.30516707897186
time_elpased: 1.774
batch start
#iterations: 129
currently lose_sum: 94.13249999284744
time_elpased: 1.78
batch start
#iterations: 130
currently lose_sum: 94.60564839839935
time_elpased: 1.764
batch start
#iterations: 131
currently lose_sum: 93.96705895662308
time_elpased: 1.774
batch start
#iterations: 132
currently lose_sum: 94.30395179986954
time_elpased: 1.795
batch start
#iterations: 133
currently lose_sum: 93.81552320718765
time_elpased: 1.766
batch start
#iterations: 134
currently lose_sum: 94.21463668346405
time_elpased: 1.762
batch start
#iterations: 135
currently lose_sum: 94.32481950521469
time_elpased: 1.803
batch start
#iterations: 136
currently lose_sum: 94.35840964317322
time_elpased: 1.746
batch start
#iterations: 137
currently lose_sum: 94.35455006361008
time_elpased: 1.762
batch start
#iterations: 138
currently lose_sum: 94.66050016880035
time_elpased: 1.729
batch start
#iterations: 139
currently lose_sum: 94.02387523651123
time_elpased: 1.738
start validation test
0.653969072165
0.633226893738
0.734691777298
0.680196274594
0.653827351005
60.910
batch start
#iterations: 140
currently lose_sum: 94.33233124017715
time_elpased: 1.806
batch start
#iterations: 141
currently lose_sum: 94.11779683828354
time_elpased: 1.798
batch start
#iterations: 142
currently lose_sum: 94.68319654464722
time_elpased: 1.762
batch start
#iterations: 143
currently lose_sum: 93.97445744276047
time_elpased: 1.775
batch start
#iterations: 144
currently lose_sum: 94.18096566200256
time_elpased: 1.752
batch start
#iterations: 145
currently lose_sum: 94.30017399787903
time_elpased: 1.746
batch start
#iterations: 146
currently lose_sum: 94.09893906116486
time_elpased: 1.767
batch start
#iterations: 147
currently lose_sum: 93.78073340654373
time_elpased: 1.757
batch start
#iterations: 148
currently lose_sum: 94.17157554626465
time_elpased: 1.77
batch start
#iterations: 149
currently lose_sum: 94.24104940891266
time_elpased: 1.816
batch start
#iterations: 150
currently lose_sum: 94.11166793107986
time_elpased: 1.771
batch start
#iterations: 151
currently lose_sum: 94.45218497514725
time_elpased: 1.769
batch start
#iterations: 152
currently lose_sum: 94.3770455121994
time_elpased: 1.797
batch start
#iterations: 153
currently lose_sum: 93.78354042768478
time_elpased: 1.773
batch start
#iterations: 154
currently lose_sum: 94.51773166656494
time_elpased: 1.764
batch start
#iterations: 155
currently lose_sum: 94.63025397062302
time_elpased: 1.793
batch start
#iterations: 156
currently lose_sum: 94.53868824243546
time_elpased: 1.834
batch start
#iterations: 157
currently lose_sum: 94.59266525506973
time_elpased: 1.774
batch start
#iterations: 158
currently lose_sum: 94.43565821647644
time_elpased: 1.798
batch start
#iterations: 159
currently lose_sum: 94.42207181453705
time_elpased: 1.771
start validation test
0.653608247423
0.64297299876
0.693526808686
0.667293791465
0.653538164231
60.780
batch start
#iterations: 160
currently lose_sum: 94.14104342460632
time_elpased: 1.78
batch start
#iterations: 161
currently lose_sum: 94.16891086101532
time_elpased: 1.779
batch start
#iterations: 162
currently lose_sum: 94.19249379634857
time_elpased: 1.781
batch start
#iterations: 163
currently lose_sum: 94.18125087022781
time_elpased: 1.773
batch start
#iterations: 164
currently lose_sum: 94.35352575778961
time_elpased: 1.758
batch start
#iterations: 165
currently lose_sum: 94.30876749753952
time_elpased: 1.737
batch start
#iterations: 166
currently lose_sum: 94.30714052915573
time_elpased: 1.742
batch start
#iterations: 167
currently lose_sum: 94.26005345582962
time_elpased: 1.735
batch start
#iterations: 168
currently lose_sum: 94.0429636836052
time_elpased: 1.766
batch start
#iterations: 169
currently lose_sum: 94.1426607966423
time_elpased: 1.789
batch start
#iterations: 170
currently lose_sum: 94.14354491233826
time_elpased: 1.758
batch start
#iterations: 171
currently lose_sum: 94.36725044250488
time_elpased: 1.787
batch start
#iterations: 172
currently lose_sum: 94.28822481632233
time_elpased: 1.767
batch start
#iterations: 173
currently lose_sum: 94.24823206663132
time_elpased: 1.895
batch start
#iterations: 174
currently lose_sum: 93.76446163654327
time_elpased: 1.767
batch start
#iterations: 175
currently lose_sum: 94.1305364370346
time_elpased: 1.751
batch start
#iterations: 176
currently lose_sum: 94.1004713177681
time_elpased: 1.752
batch start
#iterations: 177
currently lose_sum: 94.54383832216263
time_elpased: 1.821
batch start
#iterations: 178
currently lose_sum: 94.223972260952
time_elpased: 1.766
batch start
#iterations: 179
currently lose_sum: 94.33281683921814
time_elpased: 1.745
start validation test
0.653762886598
0.631671348315
0.740557785325
0.681794495239
0.653610504766
60.762
batch start
#iterations: 180
currently lose_sum: 93.805843770504
time_elpased: 1.771
batch start
#iterations: 181
currently lose_sum: 93.82476353645325
time_elpased: 1.797
batch start
#iterations: 182
currently lose_sum: 94.16095507144928
time_elpased: 1.788
batch start
#iterations: 183
currently lose_sum: 93.85201144218445
time_elpased: 1.785
batch start
#iterations: 184
currently lose_sum: 94.10937494039536
time_elpased: 1.752
batch start
#iterations: 185
currently lose_sum: 94.0580283999443
time_elpased: 1.763
batch start
#iterations: 186
currently lose_sum: 94.02286541461945
time_elpased: 1.834
batch start
#iterations: 187
currently lose_sum: 93.91138166189194
time_elpased: 1.763
batch start
#iterations: 188
currently lose_sum: 94.12764501571655
time_elpased: 1.782
batch start
#iterations: 189
currently lose_sum: 93.9824184179306
time_elpased: 1.781
batch start
#iterations: 190
currently lose_sum: 94.27550411224365
time_elpased: 1.785
batch start
#iterations: 191
currently lose_sum: 94.20284056663513
time_elpased: 1.765
batch start
#iterations: 192
currently lose_sum: 94.43923377990723
time_elpased: 1.789
batch start
#iterations: 193
currently lose_sum: 94.22572475671768
time_elpased: 1.774
batch start
#iterations: 194
currently lose_sum: 93.74925255775452
time_elpased: 1.76
batch start
#iterations: 195
currently lose_sum: 94.05377519130707
time_elpased: 1.768
batch start
#iterations: 196
currently lose_sum: 94.4680386185646
time_elpased: 1.778
batch start
#iterations: 197
currently lose_sum: 93.87956553697586
time_elpased: 1.795
batch start
#iterations: 198
currently lose_sum: 94.08619505167007
time_elpased: 1.78
batch start
#iterations: 199
currently lose_sum: 94.0992186665535
time_elpased: 1.744
start validation test
0.65793814433
0.631162196679
0.762889780797
0.690802348337
0.657753885544
60.578
batch start
#iterations: 200
currently lose_sum: 94.39667928218842
time_elpased: 1.765
batch start
#iterations: 201
currently lose_sum: 93.76992291212082
time_elpased: 1.725
batch start
#iterations: 202
currently lose_sum: 93.6355094909668
time_elpased: 1.768
batch start
#iterations: 203
currently lose_sum: 94.01440924406052
time_elpased: 1.748
batch start
#iterations: 204
currently lose_sum: 94.16817283630371
time_elpased: 1.761
batch start
#iterations: 205
currently lose_sum: 94.06964594125748
time_elpased: 1.756
batch start
#iterations: 206
currently lose_sum: 93.93844187259674
time_elpased: 1.779
batch start
#iterations: 207
currently lose_sum: 94.06588453054428
time_elpased: 1.762
batch start
#iterations: 208
currently lose_sum: 93.88961559534073
time_elpased: 1.745
batch start
#iterations: 209
currently lose_sum: 93.73067116737366
time_elpased: 1.815
batch start
#iterations: 210
currently lose_sum: 93.94786649942398
time_elpased: 1.774
batch start
#iterations: 211
currently lose_sum: 93.88329464197159
time_elpased: 1.734
batch start
#iterations: 212
currently lose_sum: 94.14914470911026
time_elpased: 1.771
batch start
#iterations: 213
currently lose_sum: 93.82387381792068
time_elpased: 1.735
batch start
#iterations: 214
currently lose_sum: 94.11251038312912
time_elpased: 1.802
batch start
#iterations: 215
currently lose_sum: 93.97097021341324
time_elpased: 1.764
batch start
#iterations: 216
currently lose_sum: 93.89845764636993
time_elpased: 1.762
batch start
#iterations: 217
currently lose_sum: 94.31107598543167
time_elpased: 1.779
batch start
#iterations: 218
currently lose_sum: 93.79137527942657
time_elpased: 1.799
batch start
#iterations: 219
currently lose_sum: 94.23327487707138
time_elpased: 1.75
start validation test
0.656391752577
0.633710228767
0.744056807657
0.684464640727
0.656237843052
60.805
batch start
#iterations: 220
currently lose_sum: 94.0225002169609
time_elpased: 1.752
batch start
#iterations: 221
currently lose_sum: 94.0977333188057
time_elpased: 1.752
batch start
#iterations: 222
currently lose_sum: 94.0808846950531
time_elpased: 1.782
batch start
#iterations: 223
currently lose_sum: 93.78510802984238
time_elpased: 1.769
batch start
#iterations: 224
currently lose_sum: 93.64629763364792
time_elpased: 1.776
batch start
#iterations: 225
currently lose_sum: 93.94886875152588
time_elpased: 1.731
batch start
#iterations: 226
currently lose_sum: 93.95965141057968
time_elpased: 1.769
batch start
#iterations: 227
currently lose_sum: 93.98245137929916
time_elpased: 1.783
batch start
#iterations: 228
currently lose_sum: 93.60711401700974
time_elpased: 1.777
batch start
#iterations: 229
currently lose_sum: 93.80498945713043
time_elpased: 1.775
batch start
#iterations: 230
currently lose_sum: 93.56923460960388
time_elpased: 1.806
batch start
#iterations: 231
currently lose_sum: 94.11935013532639
time_elpased: 1.78
batch start
#iterations: 232
currently lose_sum: 94.029541015625
time_elpased: 1.753
batch start
#iterations: 233
currently lose_sum: 93.57730323076248
time_elpased: 1.785
batch start
#iterations: 234
currently lose_sum: 93.79818350076675
time_elpased: 1.812
batch start
#iterations: 235
currently lose_sum: 94.06182789802551
time_elpased: 1.779
batch start
#iterations: 236
currently lose_sum: 94.2321463227272
time_elpased: 1.759
batch start
#iterations: 237
currently lose_sum: 93.86081397533417
time_elpased: 1.739
batch start
#iterations: 238
currently lose_sum: 93.81314158439636
time_elpased: 1.734
batch start
#iterations: 239
currently lose_sum: 94.33665090799332
time_elpased: 1.789
start validation test
0.656907216495
0.628494668794
0.770402387568
0.692250786018
0.656707958217
60.762
batch start
#iterations: 240
currently lose_sum: 93.59535896778107
time_elpased: 1.768
batch start
#iterations: 241
currently lose_sum: 93.93279320001602
time_elpased: 1.751
batch start
#iterations: 242
currently lose_sum: 94.1641395688057
time_elpased: 1.752
batch start
#iterations: 243
currently lose_sum: 94.37146174907684
time_elpased: 1.784
batch start
#iterations: 244
currently lose_sum: 93.93606871366501
time_elpased: 1.787
batch start
#iterations: 245
currently lose_sum: 94.1912881731987
time_elpased: 1.792
batch start
#iterations: 246
currently lose_sum: 93.5701413154602
time_elpased: 1.824
batch start
#iterations: 247
currently lose_sum: 94.29879981279373
time_elpased: 1.783
batch start
#iterations: 248
currently lose_sum: 93.56613630056381
time_elpased: 1.769
batch start
#iterations: 249
currently lose_sum: 93.90913045406342
time_elpased: 1.753
batch start
#iterations: 250
currently lose_sum: 94.06556624174118
time_elpased: 1.79
batch start
#iterations: 251
currently lose_sum: 93.94035345315933
time_elpased: 1.77
batch start
#iterations: 252
currently lose_sum: 93.68775099515915
time_elpased: 1.779
batch start
#iterations: 253
currently lose_sum: 94.01475435495377
time_elpased: 1.801
batch start
#iterations: 254
currently lose_sum: 93.7660420536995
time_elpased: 1.807
batch start
#iterations: 255
currently lose_sum: 93.76300925016403
time_elpased: 1.821
batch start
#iterations: 256
currently lose_sum: 93.68059635162354
time_elpased: 1.768
batch start
#iterations: 257
currently lose_sum: 93.67477959394455
time_elpased: 1.77
batch start
#iterations: 258
currently lose_sum: 93.8751922249794
time_elpased: 1.786
batch start
#iterations: 259
currently lose_sum: 93.7245866060257
time_elpased: 1.784
start validation test
0.658041237113
0.634476140626
0.748482041782
0.686779981114
0.657882454331
60.557
batch start
#iterations: 260
currently lose_sum: 93.51725643873215
time_elpased: 1.763
batch start
#iterations: 261
currently lose_sum: 93.55306196212769
time_elpased: 1.789
batch start
#iterations: 262
currently lose_sum: 93.92381918430328
time_elpased: 1.758
batch start
#iterations: 263
currently lose_sum: 93.78922003507614
time_elpased: 1.737
batch start
#iterations: 264
currently lose_sum: 94.03034377098083
time_elpased: 1.789
batch start
#iterations: 265
currently lose_sum: 93.52329409122467
time_elpased: 1.753
batch start
#iterations: 266
currently lose_sum: 93.99612587690353
time_elpased: 1.777
batch start
#iterations: 267
currently lose_sum: 93.68090093135834
time_elpased: 1.768
batch start
#iterations: 268
currently lose_sum: 93.77326953411102
time_elpased: 1.766
batch start
#iterations: 269
currently lose_sum: 94.04834026098251
time_elpased: 1.745
batch start
#iterations: 270
currently lose_sum: 93.91133481264114
time_elpased: 1.784
batch start
#iterations: 271
currently lose_sum: 93.38256293535233
time_elpased: 1.782
batch start
#iterations: 272
currently lose_sum: 93.91558802127838
time_elpased: 1.777
batch start
#iterations: 273
currently lose_sum: 93.95539957284927
time_elpased: 1.756
batch start
#iterations: 274
currently lose_sum: 93.78683108091354
time_elpased: 1.759
batch start
#iterations: 275
currently lose_sum: 94.25031316280365
time_elpased: 1.773
batch start
#iterations: 276
currently lose_sum: 93.47292649745941
time_elpased: 1.781
batch start
#iterations: 277
currently lose_sum: 93.59819996356964
time_elpased: 1.756
batch start
#iterations: 278
currently lose_sum: 93.33797085285187
time_elpased: 1.798
batch start
#iterations: 279
currently lose_sum: 93.7655543088913
time_elpased: 1.75
start validation test
0.658298969072
0.632988802756
0.756303385819
0.689173348338
0.658126907202
60.624
batch start
#iterations: 280
currently lose_sum: 93.3156590461731
time_elpased: 1.764
batch start
#iterations: 281
currently lose_sum: 93.69516384601593
time_elpased: 1.766
batch start
#iterations: 282
currently lose_sum: 93.93047481775284
time_elpased: 1.775
batch start
#iterations: 283
currently lose_sum: 94.0582103729248
time_elpased: 1.768
batch start
#iterations: 284
currently lose_sum: 93.26642096042633
time_elpased: 1.787
batch start
#iterations: 285
currently lose_sum: 93.47096526622772
time_elpased: 1.769
batch start
#iterations: 286
currently lose_sum: 93.58896714448929
time_elpased: 1.784
batch start
#iterations: 287
currently lose_sum: 93.82855552434921
time_elpased: 1.784
batch start
#iterations: 288
currently lose_sum: 93.78613567352295
time_elpased: 1.809
batch start
#iterations: 289
currently lose_sum: 94.04968160390854
time_elpased: 1.74
batch start
#iterations: 290
currently lose_sum: 94.07407534122467
time_elpased: 1.789
batch start
#iterations: 291
currently lose_sum: 93.56286484003067
time_elpased: 1.773
batch start
#iterations: 292
currently lose_sum: 93.67785227298737
time_elpased: 1.758
batch start
#iterations: 293
currently lose_sum: 93.83303290605545
time_elpased: 1.729
batch start
#iterations: 294
currently lose_sum: 93.6748599410057
time_elpased: 1.75
batch start
#iterations: 295
currently lose_sum: 94.0240860581398
time_elpased: 1.784
batch start
#iterations: 296
currently lose_sum: 93.8227716088295
time_elpased: 1.759
batch start
#iterations: 297
currently lose_sum: 94.01944762468338
time_elpased: 1.793
batch start
#iterations: 298
currently lose_sum: 93.40219175815582
time_elpased: 1.765
batch start
#iterations: 299
currently lose_sum: 93.47572541236877
time_elpased: 1.842
start validation test
0.652628865979
0.631120112716
0.7375733251
0.680206899824
0.65247973288
60.829
batch start
#iterations: 300
currently lose_sum: 93.49445080757141
time_elpased: 1.804
batch start
#iterations: 301
currently lose_sum: 93.92096608877182
time_elpased: 1.819
batch start
#iterations: 302
currently lose_sum: 93.40774631500244
time_elpased: 1.756
batch start
#iterations: 303
currently lose_sum: 93.88353896141052
time_elpased: 1.78
batch start
#iterations: 304
currently lose_sum: 93.41632175445557
time_elpased: 1.777
batch start
#iterations: 305
currently lose_sum: 93.7186890244484
time_elpased: 1.775
batch start
#iterations: 306
currently lose_sum: 94.00353562831879
time_elpased: 1.787
batch start
#iterations: 307
currently lose_sum: 93.89613407850266
time_elpased: 1.771
batch start
#iterations: 308
currently lose_sum: 93.45445400476456
time_elpased: 1.768
batch start
#iterations: 309
currently lose_sum: 93.7611768245697
time_elpased: 1.744
batch start
#iterations: 310
currently lose_sum: 93.63497298955917
time_elpased: 1.813
batch start
#iterations: 311
currently lose_sum: 93.8190285563469
time_elpased: 1.76
batch start
#iterations: 312
currently lose_sum: 93.34406191110611
time_elpased: 1.743
batch start
#iterations: 313
currently lose_sum: 93.56218749284744
time_elpased: 1.769
batch start
#iterations: 314
currently lose_sum: 93.691965341568
time_elpased: 1.779
batch start
#iterations: 315
currently lose_sum: 93.6647761464119
time_elpased: 1.746
batch start
#iterations: 316
currently lose_sum: 93.62182128429413
time_elpased: 1.761
batch start
#iterations: 317
currently lose_sum: 93.9853709936142
time_elpased: 1.749
batch start
#iterations: 318
currently lose_sum: 93.89963871240616
time_elpased: 1.804
batch start
#iterations: 319
currently lose_sum: 93.74048840999603
time_elpased: 1.792
start validation test
0.65175257732
0.631541537095
0.73150149223
0.677856189205
0.651612565799
61.017
batch start
#iterations: 320
currently lose_sum: 93.20896804332733
time_elpased: 1.788
batch start
#iterations: 321
currently lose_sum: 94.14935439825058
time_elpased: 1.717
batch start
#iterations: 322
currently lose_sum: 93.9401878118515
time_elpased: 1.778
batch start
#iterations: 323
currently lose_sum: 93.82010918855667
time_elpased: 1.783
batch start
#iterations: 324
currently lose_sum: 93.60232138633728
time_elpased: 1.813
batch start
#iterations: 325
currently lose_sum: 93.7398453950882
time_elpased: 1.749
batch start
#iterations: 326
currently lose_sum: 93.76694601774216
time_elpased: 1.762
batch start
#iterations: 327
currently lose_sum: 93.24312102794647
time_elpased: 1.762
batch start
#iterations: 328
currently lose_sum: 93.50018692016602
time_elpased: 1.764
batch start
#iterations: 329
currently lose_sum: 93.8683734536171
time_elpased: 1.735
batch start
#iterations: 330
currently lose_sum: 93.29538905620575
time_elpased: 1.81
batch start
#iterations: 331
currently lose_sum: 93.2256800532341
time_elpased: 1.761
batch start
#iterations: 332
currently lose_sum: 93.61654806137085
time_elpased: 1.814
batch start
#iterations: 333
currently lose_sum: 93.27466797828674
time_elpased: 1.749
batch start
#iterations: 334
currently lose_sum: 93.56778639554977
time_elpased: 1.775
batch start
#iterations: 335
currently lose_sum: 93.43944031000137
time_elpased: 1.813
batch start
#iterations: 336
currently lose_sum: 93.55541032552719
time_elpased: 1.814
batch start
#iterations: 337
currently lose_sum: 93.7056964635849
time_elpased: 1.73
batch start
#iterations: 338
currently lose_sum: 93.764373421669
time_elpased: 1.793
batch start
#iterations: 339
currently lose_sum: 93.56686806678772
time_elpased: 1.763
start validation test
0.657577319588
0.632203681404
0.75640629824
0.68875040997
0.657403810072
60.802
batch start
#iterations: 340
currently lose_sum: 93.76115620136261
time_elpased: 1.755
batch start
#iterations: 341
currently lose_sum: 93.67070662975311
time_elpased: 1.763
batch start
#iterations: 342
currently lose_sum: 93.39899057149887
time_elpased: 1.742
batch start
#iterations: 343
currently lose_sum: 93.45210868120193
time_elpased: 1.757
batch start
#iterations: 344
currently lose_sum: 93.85803234577179
time_elpased: 1.796
batch start
#iterations: 345
currently lose_sum: 93.45046353340149
time_elpased: 1.784
batch start
#iterations: 346
currently lose_sum: 93.74242264032364
time_elpased: 1.74
batch start
#iterations: 347
currently lose_sum: 93.8958660364151
time_elpased: 1.765
batch start
#iterations: 348
currently lose_sum: 93.88299733400345
time_elpased: 1.794
batch start
#iterations: 349
currently lose_sum: 93.54295927286148
time_elpased: 1.772
batch start
#iterations: 350
currently lose_sum: 93.87914162874222
time_elpased: 1.762
batch start
#iterations: 351
currently lose_sum: 93.83813405036926
time_elpased: 1.79
batch start
#iterations: 352
currently lose_sum: 93.8010323047638
time_elpased: 1.805
batch start
#iterations: 353
currently lose_sum: 94.43074995279312
time_elpased: 1.761
batch start
#iterations: 354
currently lose_sum: 93.2578803896904
time_elpased: 1.78
batch start
#iterations: 355
currently lose_sum: 93.89161866903305
time_elpased: 1.781
batch start
#iterations: 356
currently lose_sum: 93.90706223249435
time_elpased: 1.817
batch start
#iterations: 357
currently lose_sum: 93.69666004180908
time_elpased: 1.762
batch start
#iterations: 358
currently lose_sum: 93.95902281999588
time_elpased: 1.733
batch start
#iterations: 359
currently lose_sum: 93.8761842250824
time_elpased: 1.766
start validation test
0.657268041237
0.638848660391
0.726355871154
0.679797736576
0.657146746896
60.915
batch start
#iterations: 360
currently lose_sum: 93.50126546621323
time_elpased: 1.761
batch start
#iterations: 361
currently lose_sum: 93.9305447936058
time_elpased: 1.746
batch start
#iterations: 362
currently lose_sum: 93.59584653377533
time_elpased: 1.774
batch start
#iterations: 363
currently lose_sum: 93.93705403804779
time_elpased: 1.807
batch start
#iterations: 364
currently lose_sum: 94.34263098239899
time_elpased: 1.751
batch start
#iterations: 365
currently lose_sum: 93.32819747924805
time_elpased: 1.773
batch start
#iterations: 366
currently lose_sum: 93.2433893084526
time_elpased: 1.809
batch start
#iterations: 367
currently lose_sum: 93.25126838684082
time_elpased: 1.741
batch start
#iterations: 368
currently lose_sum: 93.61857831478119
time_elpased: 1.775
batch start
#iterations: 369
currently lose_sum: 93.83892625570297
time_elpased: 1.773
batch start
#iterations: 370
currently lose_sum: 93.70539957284927
time_elpased: 1.78
batch start
#iterations: 371
currently lose_sum: 93.45127087831497
time_elpased: 1.736
batch start
#iterations: 372
currently lose_sum: 93.69297927618027
time_elpased: 1.779
batch start
#iterations: 373
currently lose_sum: 93.71524584293365
time_elpased: 1.77
batch start
#iterations: 374
currently lose_sum: 93.26946419477463
time_elpased: 1.764
batch start
#iterations: 375
currently lose_sum: 94.13019996881485
time_elpased: 1.766
batch start
#iterations: 376
currently lose_sum: 93.17741602659225
time_elpased: 1.83
batch start
#iterations: 377
currently lose_sum: 93.76020330190659
time_elpased: 1.768
batch start
#iterations: 378
currently lose_sum: 93.39258688688278
time_elpased: 1.769
batch start
#iterations: 379
currently lose_sum: 93.29550647735596
time_elpased: 1.753
start validation test
0.654690721649
0.629706034038
0.753936400123
0.686244204019
0.654516480553
60.831
batch start
#iterations: 380
currently lose_sum: 93.90706896781921
time_elpased: 1.775
batch start
#iterations: 381
currently lose_sum: 93.3032945394516
time_elpased: 1.784
batch start
#iterations: 382
currently lose_sum: 93.88427233695984
time_elpased: 1.752
batch start
#iterations: 383
currently lose_sum: 93.67897528409958
time_elpased: 1.801
batch start
#iterations: 384
currently lose_sum: 93.59400010108948
time_elpased: 1.796
batch start
#iterations: 385
currently lose_sum: 93.52975058555603
time_elpased: 1.772
batch start
#iterations: 386
currently lose_sum: 93.51591545343399
time_elpased: 1.742
batch start
#iterations: 387
currently lose_sum: 93.6035948395729
time_elpased: 1.806
batch start
#iterations: 388
currently lose_sum: 93.45770561695099
time_elpased: 1.752
batch start
#iterations: 389
currently lose_sum: 93.38692820072174
time_elpased: 1.738
batch start
#iterations: 390
currently lose_sum: 93.65489059686661
time_elpased: 1.735
batch start
#iterations: 391
currently lose_sum: 93.38344609737396
time_elpased: 1.792
batch start
#iterations: 392
currently lose_sum: 93.47489774227142
time_elpased: 1.793
batch start
#iterations: 393
currently lose_sum: 93.70415151119232
time_elpased: 1.755
batch start
#iterations: 394
currently lose_sum: 93.44165343046188
time_elpased: 1.76
batch start
#iterations: 395
currently lose_sum: 93.57560610771179
time_elpased: 1.742
batch start
#iterations: 396
currently lose_sum: 93.77526462078094
time_elpased: 1.739
batch start
#iterations: 397
currently lose_sum: 93.44489532709122
time_elpased: 1.775
batch start
#iterations: 398
currently lose_sum: 93.70654308795929
time_elpased: 1.763
batch start
#iterations: 399
currently lose_sum: 93.41673040390015
time_elpased: 1.767
start validation test
0.654484536082
0.625062240664
0.775136358959
0.692056783204
0.654272713198
60.755
acc: 0.655
pre: 0.631
rec: 0.748
F1: 0.685
auc: 0.703
