start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.06777095794678
time_elpased: 1.567
batch start
#iterations: 1
currently lose_sum: 99.41296577453613
time_elpased: 1.538
batch start
#iterations: 2
currently lose_sum: 99.31521034240723
time_elpased: 1.573
batch start
#iterations: 3
currently lose_sum: 98.72057545185089
time_elpased: 1.569
batch start
#iterations: 4
currently lose_sum: 98.67768144607544
time_elpased: 1.638
batch start
#iterations: 5
currently lose_sum: 98.10045862197876
time_elpased: 1.619
batch start
#iterations: 6
currently lose_sum: 97.673210978508
time_elpased: 1.561
batch start
#iterations: 7
currently lose_sum: 97.73833072185516
time_elpased: 1.553
batch start
#iterations: 8
currently lose_sum: 97.62649220228195
time_elpased: 1.582
batch start
#iterations: 9
currently lose_sum: 97.58836060762405
time_elpased: 1.599
batch start
#iterations: 10
currently lose_sum: 97.3096911907196
time_elpased: 1.597
batch start
#iterations: 11
currently lose_sum: 97.16693830490112
time_elpased: 1.544
batch start
#iterations: 12
currently lose_sum: 96.94304603338242
time_elpased: 1.55
batch start
#iterations: 13
currently lose_sum: 96.92645692825317
time_elpased: 1.553
batch start
#iterations: 14
currently lose_sum: 96.74541807174683
time_elpased: 1.551
batch start
#iterations: 15
currently lose_sum: 96.72593623399734
time_elpased: 1.565
batch start
#iterations: 16
currently lose_sum: 96.7211201786995
time_elpased: 1.597
batch start
#iterations: 17
currently lose_sum: 96.63212525844574
time_elpased: 1.553
batch start
#iterations: 18
currently lose_sum: 96.03823721408844
time_elpased: 1.531
batch start
#iterations: 19
currently lose_sum: 96.57439303398132
time_elpased: 1.59
start validation test
0.655824742268
0.639988948241
0.715138417207
0.675479951397
0.655720607963
61.877
batch start
#iterations: 20
currently lose_sum: 96.3229575753212
time_elpased: 1.596
batch start
#iterations: 21
currently lose_sum: 95.97567570209503
time_elpased: 1.584
batch start
#iterations: 22
currently lose_sum: 96.28967875242233
time_elpased: 1.546
batch start
#iterations: 23
currently lose_sum: 96.32478135824203
time_elpased: 1.528
batch start
#iterations: 24
currently lose_sum: 96.29789650440216
time_elpased: 1.578
batch start
#iterations: 25
currently lose_sum: 96.20613223314285
time_elpased: 1.529
batch start
#iterations: 26
currently lose_sum: 95.86748218536377
time_elpased: 1.615
batch start
#iterations: 27
currently lose_sum: 95.83090084791183
time_elpased: 1.542
batch start
#iterations: 28
currently lose_sum: 95.58030998706818
time_elpased: 1.563
batch start
#iterations: 29
currently lose_sum: 96.4084900021553
time_elpased: 1.561
batch start
#iterations: 30
currently lose_sum: 95.34528291225433
time_elpased: 1.544
batch start
#iterations: 31
currently lose_sum: 95.79126340150833
time_elpased: 1.547
batch start
#iterations: 32
currently lose_sum: 95.70534181594849
time_elpased: 1.55
batch start
#iterations: 33
currently lose_sum: 96.00652223825455
time_elpased: 1.612
batch start
#iterations: 34
currently lose_sum: 95.60859125852585
time_elpased: 1.521
batch start
#iterations: 35
currently lose_sum: 95.56846785545349
time_elpased: 1.598
batch start
#iterations: 36
currently lose_sum: 95.57915383577347
time_elpased: 1.567
batch start
#iterations: 37
currently lose_sum: 95.62514978647232
time_elpased: 1.567
batch start
#iterations: 38
currently lose_sum: 95.16951304674149
time_elpased: 1.544
batch start
#iterations: 39
currently lose_sum: 95.68803203105927
time_elpased: 1.541
start validation test
0.657113402062
0.632764445985
0.75167232685
0.687111947319
0.656947389285
61.395
batch start
#iterations: 40
currently lose_sum: 95.5558899641037
time_elpased: 1.541
batch start
#iterations: 41
currently lose_sum: 95.29119604825974
time_elpased: 1.563
batch start
#iterations: 42
currently lose_sum: 95.28102028369904
time_elpased: 1.533
batch start
#iterations: 43
currently lose_sum: 95.50979977846146
time_elpased: 1.554
batch start
#iterations: 44
currently lose_sum: 95.40345340967178
time_elpased: 1.543
batch start
#iterations: 45
currently lose_sum: 95.05476319789886
time_elpased: 1.575
batch start
#iterations: 46
currently lose_sum: 95.18271785974503
time_elpased: 1.523
batch start
#iterations: 47
currently lose_sum: 95.42853629589081
time_elpased: 1.577
batch start
#iterations: 48
currently lose_sum: 95.33976686000824
time_elpased: 1.611
batch start
#iterations: 49
currently lose_sum: 95.41904515028
time_elpased: 1.555
batch start
#iterations: 50
currently lose_sum: 95.43537718057632
time_elpased: 1.558
batch start
#iterations: 51
currently lose_sum: 95.31672233343124
time_elpased: 1.538
batch start
#iterations: 52
currently lose_sum: 95.35615652799606
time_elpased: 1.535
batch start
#iterations: 53
currently lose_sum: 95.42047393321991
time_elpased: 1.555
batch start
#iterations: 54
currently lose_sum: 95.52529835700989
time_elpased: 1.613
batch start
#iterations: 55
currently lose_sum: 94.81517744064331
time_elpased: 1.551
batch start
#iterations: 56
currently lose_sum: 95.02242994308472
time_elpased: 1.549
batch start
#iterations: 57
currently lose_sum: 95.20152425765991
time_elpased: 1.571
batch start
#iterations: 58
currently lose_sum: 95.27234983444214
time_elpased: 1.554
batch start
#iterations: 59
currently lose_sum: 94.84884530305862
time_elpased: 1.595
start validation test
0.663917525773
0.642227956224
0.742821858598
0.688871922123
0.663778997047
61.048
batch start
#iterations: 60
currently lose_sum: 94.80207759141922
time_elpased: 1.554
batch start
#iterations: 61
currently lose_sum: 95.3327117562294
time_elpased: 1.583
batch start
#iterations: 62
currently lose_sum: 94.79793691635132
time_elpased: 1.605
batch start
#iterations: 63
currently lose_sum: 95.28054183721542
time_elpased: 1.606
batch start
#iterations: 64
currently lose_sum: 94.83237159252167
time_elpased: 1.53
batch start
#iterations: 65
currently lose_sum: 94.77328503131866
time_elpased: 1.602
batch start
#iterations: 66
currently lose_sum: 94.92283481359482
time_elpased: 1.531
batch start
#iterations: 67
currently lose_sum: 95.0331420302391
time_elpased: 1.544
batch start
#iterations: 68
currently lose_sum: 95.13315838575363
time_elpased: 1.599
batch start
#iterations: 69
currently lose_sum: 94.9781539440155
time_elpased: 1.544
batch start
#iterations: 70
currently lose_sum: 94.6803206205368
time_elpased: 1.575
batch start
#iterations: 71
currently lose_sum: 94.67075252532959
time_elpased: 1.533
batch start
#iterations: 72
currently lose_sum: 95.04484641551971
time_elpased: 1.538
batch start
#iterations: 73
currently lose_sum: 94.8686540722847
time_elpased: 1.516
batch start
#iterations: 74
currently lose_sum: 94.76830404996872
time_elpased: 1.541
batch start
#iterations: 75
currently lose_sum: 94.62480527162552
time_elpased: 1.552
batch start
#iterations: 76
currently lose_sum: 94.71140372753143
time_elpased: 1.542
batch start
#iterations: 77
currently lose_sum: 94.78530639410019
time_elpased: 1.539
batch start
#iterations: 78
currently lose_sum: 95.03340339660645
time_elpased: 1.54
batch start
#iterations: 79
currently lose_sum: 94.79405146837234
time_elpased: 1.569
start validation test
0.667113402062
0.645556051809
0.743748070392
0.691182096404
0.666978858082
60.958
batch start
#iterations: 80
currently lose_sum: 94.54870462417603
time_elpased: 1.56
batch start
#iterations: 81
currently lose_sum: 94.96158742904663
time_elpased: 1.58
batch start
#iterations: 82
currently lose_sum: 94.91911607980728
time_elpased: 1.621
batch start
#iterations: 83
currently lose_sum: 94.40062606334686
time_elpased: 1.589
batch start
#iterations: 84
currently lose_sum: 94.63464844226837
time_elpased: 1.543
batch start
#iterations: 85
currently lose_sum: 94.91667246818542
time_elpased: 1.556
batch start
#iterations: 86
currently lose_sum: 94.70550829172134
time_elpased: 1.535
batch start
#iterations: 87
currently lose_sum: 94.41145521402359
time_elpased: 1.556
batch start
#iterations: 88
currently lose_sum: 94.61351388692856
time_elpased: 1.533
batch start
#iterations: 89
currently lose_sum: 94.31409645080566
time_elpased: 1.561
batch start
#iterations: 90
currently lose_sum: 94.80809539556503
time_elpased: 1.55
batch start
#iterations: 91
currently lose_sum: 94.50109434127808
time_elpased: 1.548
batch start
#iterations: 92
currently lose_sum: 94.29766917228699
time_elpased: 1.576
batch start
#iterations: 93
currently lose_sum: 94.50607484579086
time_elpased: 1.541
batch start
#iterations: 94
currently lose_sum: 94.79195028543472
time_elpased: 1.543
batch start
#iterations: 95
currently lose_sum: 94.78958302736282
time_elpased: 1.542
batch start
#iterations: 96
currently lose_sum: 95.09175163507462
time_elpased: 1.527
batch start
#iterations: 97
currently lose_sum: 94.56799411773682
time_elpased: 1.563
batch start
#iterations: 98
currently lose_sum: 94.27246308326721
time_elpased: 1.539
batch start
#iterations: 99
currently lose_sum: 94.36346143484116
time_elpased: 1.547
start validation test
0.667422680412
0.638476545933
0.774621796851
0.69999070027
0.667234475829
60.724
batch start
#iterations: 100
currently lose_sum: 94.38495659828186
time_elpased: 1.547
batch start
#iterations: 101
currently lose_sum: 94.60621327161789
time_elpased: 1.579
batch start
#iterations: 102
currently lose_sum: 94.26665109395981
time_elpased: 1.557
batch start
#iterations: 103
currently lose_sum: 94.58315414190292
time_elpased: 1.554
batch start
#iterations: 104
currently lose_sum: 94.61586898565292
time_elpased: 1.536
batch start
#iterations: 105
currently lose_sum: 94.74730038642883
time_elpased: 1.574
batch start
#iterations: 106
currently lose_sum: 94.64674216508865
time_elpased: 1.518
batch start
#iterations: 107
currently lose_sum: 94.86618101596832
time_elpased: 1.517
batch start
#iterations: 108
currently lose_sum: 94.34069728851318
time_elpased: 1.557
batch start
#iterations: 109
currently lose_sum: 94.4195448756218
time_elpased: 1.565
batch start
#iterations: 110
currently lose_sum: 94.1591477394104
time_elpased: 1.572
batch start
#iterations: 111
currently lose_sum: 94.20626717805862
time_elpased: 1.551
batch start
#iterations: 112
currently lose_sum: 94.42149198055267
time_elpased: 1.604
batch start
#iterations: 113
currently lose_sum: 94.32956802845001
time_elpased: 1.548
batch start
#iterations: 114
currently lose_sum: 93.86767220497131
time_elpased: 1.52
batch start
#iterations: 115
currently lose_sum: 94.34370362758636
time_elpased: 1.569
batch start
#iterations: 116
currently lose_sum: 94.64198768138885
time_elpased: 1.549
batch start
#iterations: 117
currently lose_sum: 94.43974500894547
time_elpased: 1.571
batch start
#iterations: 118
currently lose_sum: 94.40620529651642
time_elpased: 1.572
batch start
#iterations: 119
currently lose_sum: 94.49290311336517
time_elpased: 1.553
start validation test
0.663917525773
0.640305450715
0.750746115056
0.691141639034
0.663765084792
60.666
batch start
#iterations: 120
currently lose_sum: 94.51560884714127
time_elpased: 1.52
batch start
#iterations: 121
currently lose_sum: 94.542012155056
time_elpased: 1.599
batch start
#iterations: 122
currently lose_sum: 94.62390595674515
time_elpased: 1.539
batch start
#iterations: 123
currently lose_sum: 94.16359621286392
time_elpased: 1.608
batch start
#iterations: 124
currently lose_sum: 94.24359184503555
time_elpased: 1.56
batch start
#iterations: 125
currently lose_sum: 94.56397706270218
time_elpased: 1.599
batch start
#iterations: 126
currently lose_sum: 94.43603128194809
time_elpased: 1.532
batch start
#iterations: 127
currently lose_sum: 94.53192806243896
time_elpased: 1.56
batch start
#iterations: 128
currently lose_sum: 94.12120324373245
time_elpased: 1.569
batch start
#iterations: 129
currently lose_sum: 94.09231513738632
time_elpased: 1.562
batch start
#iterations: 130
currently lose_sum: 94.16402304172516
time_elpased: 1.514
batch start
#iterations: 131
currently lose_sum: 94.10201221704483
time_elpased: 1.566
batch start
#iterations: 132
currently lose_sum: 94.2766278386116
time_elpased: 1.545
batch start
#iterations: 133
currently lose_sum: 94.07478255033493
time_elpased: 1.565
batch start
#iterations: 134
currently lose_sum: 94.33857011795044
time_elpased: 1.537
batch start
#iterations: 135
currently lose_sum: 94.23760831356049
time_elpased: 1.537
batch start
#iterations: 136
currently lose_sum: 93.99121725559235
time_elpased: 1.536
batch start
#iterations: 137
currently lose_sum: 94.05668532848358
time_elpased: 1.569
batch start
#iterations: 138
currently lose_sum: 94.05125039815903
time_elpased: 1.58
batch start
#iterations: 139
currently lose_sum: 94.35258972644806
time_elpased: 1.561
start validation test
0.668969072165
0.639583156825
0.776885870125
0.701579925651
0.668779607581
60.516
batch start
#iterations: 140
currently lose_sum: 94.413605093956
time_elpased: 1.558
batch start
#iterations: 141
currently lose_sum: 94.04223871231079
time_elpased: 1.567
batch start
#iterations: 142
currently lose_sum: 94.23665255308151
time_elpased: 1.555
batch start
#iterations: 143
currently lose_sum: 94.27253890037537
time_elpased: 1.549
batch start
#iterations: 144
currently lose_sum: 94.75393342971802
time_elpased: 1.565
batch start
#iterations: 145
currently lose_sum: 94.01383435726166
time_elpased: 1.545
batch start
#iterations: 146
currently lose_sum: 93.99911773204803
time_elpased: 1.542
batch start
#iterations: 147
currently lose_sum: 94.29018825292587
time_elpased: 1.546
batch start
#iterations: 148
currently lose_sum: 94.37864696979523
time_elpased: 1.538
batch start
#iterations: 149
currently lose_sum: 93.92390167713165
time_elpased: 1.594
batch start
#iterations: 150
currently lose_sum: 93.65146720409393
time_elpased: 1.564
batch start
#iterations: 151
currently lose_sum: 93.99912512302399
time_elpased: 1.563
batch start
#iterations: 152
currently lose_sum: 94.30604201555252
time_elpased: 1.568
batch start
#iterations: 153
currently lose_sum: 94.33314847946167
time_elpased: 1.597
batch start
#iterations: 154
currently lose_sum: 94.16297906637192
time_elpased: 1.558
batch start
#iterations: 155
currently lose_sum: 94.20417273044586
time_elpased: 1.578
batch start
#iterations: 156
currently lose_sum: 94.22635346651077
time_elpased: 1.58
batch start
#iterations: 157
currently lose_sum: 94.53451639413834
time_elpased: 1.557
batch start
#iterations: 158
currently lose_sum: 94.81758660078049
time_elpased: 1.549
batch start
#iterations: 159
currently lose_sum: 94.52514898777008
time_elpased: 1.562
start validation test
0.670927835052
0.64193850609
0.775650921066
0.702488582347
0.670743977522
60.709
batch start
#iterations: 160
currently lose_sum: 93.8450545668602
time_elpased: 1.536
batch start
#iterations: 161
currently lose_sum: 94.11432528495789
time_elpased: 1.534
batch start
#iterations: 162
currently lose_sum: 94.05914056301117
time_elpased: 1.586
batch start
#iterations: 163
currently lose_sum: 93.73490649461746
time_elpased: 1.575
batch start
#iterations: 164
currently lose_sum: 94.08753138780594
time_elpased: 1.561
batch start
#iterations: 165
currently lose_sum: 94.46681135892868
time_elpased: 1.526
batch start
#iterations: 166
currently lose_sum: 94.13473397493362
time_elpased: 1.567
batch start
#iterations: 167
currently lose_sum: 94.09289860725403
time_elpased: 1.542
batch start
#iterations: 168
currently lose_sum: 93.9783187508583
time_elpased: 1.594
batch start
#iterations: 169
currently lose_sum: 93.74824750423431
time_elpased: 1.584
batch start
#iterations: 170
currently lose_sum: 94.10343223810196
time_elpased: 1.553
batch start
#iterations: 171
currently lose_sum: 94.21566188335419
time_elpased: 1.58
batch start
#iterations: 172
currently lose_sum: 94.33376348018646
time_elpased: 1.571
batch start
#iterations: 173
currently lose_sum: 94.09810310602188
time_elpased: 1.63
batch start
#iterations: 174
currently lose_sum: 93.97902089357376
time_elpased: 1.551
batch start
#iterations: 175
currently lose_sum: 93.81671369075775
time_elpased: 1.534
batch start
#iterations: 176
currently lose_sum: 93.87914735078812
time_elpased: 1.569
batch start
#iterations: 177
currently lose_sum: 93.93295484781265
time_elpased: 1.589
batch start
#iterations: 178
currently lose_sum: 93.73805743455887
time_elpased: 1.609
batch start
#iterations: 179
currently lose_sum: 94.62442922592163
time_elpased: 1.55
start validation test
0.669690721649
0.644662061729
0.758773283935
0.697078566701
0.669534323471
60.425
batch start
#iterations: 180
currently lose_sum: 94.24456536769867
time_elpased: 1.545
batch start
#iterations: 181
currently lose_sum: 94.26648420095444
time_elpased: 1.552
batch start
#iterations: 182
currently lose_sum: 94.02913016080856
time_elpased: 1.546
batch start
#iterations: 183
currently lose_sum: 93.55960714817047
time_elpased: 1.572
batch start
#iterations: 184
currently lose_sum: 94.15208840370178
time_elpased: 1.553
batch start
#iterations: 185
currently lose_sum: 94.1894166469574
time_elpased: 1.558
batch start
#iterations: 186
currently lose_sum: 93.8152317404747
time_elpased: 1.545
batch start
#iterations: 187
currently lose_sum: 93.74789416790009
time_elpased: 1.573
batch start
#iterations: 188
currently lose_sum: 94.07578414678574
time_elpased: 1.551
batch start
#iterations: 189
currently lose_sum: 94.0180076956749
time_elpased: 1.529
batch start
#iterations: 190
currently lose_sum: 94.31485438346863
time_elpased: 1.545
batch start
#iterations: 191
currently lose_sum: 93.86292493343353
time_elpased: 1.508
batch start
#iterations: 192
currently lose_sum: 93.8573727607727
time_elpased: 1.553
batch start
#iterations: 193
currently lose_sum: 94.09647798538208
time_elpased: 1.568
batch start
#iterations: 194
currently lose_sum: 94.20838856697083
time_elpased: 1.541
batch start
#iterations: 195
currently lose_sum: 94.06328499317169
time_elpased: 1.543
batch start
#iterations: 196
currently lose_sum: 94.43791925907135
time_elpased: 1.543
batch start
#iterations: 197
currently lose_sum: 93.98103505373001
time_elpased: 1.54
batch start
#iterations: 198
currently lose_sum: 93.91292208433151
time_elpased: 1.605
batch start
#iterations: 199
currently lose_sum: 93.8273738026619
time_elpased: 1.547
start validation test
0.671649484536
0.644528888505
0.768035401873
0.700882794891
0.671480264192
60.393
batch start
#iterations: 200
currently lose_sum: 93.7866336107254
time_elpased: 1.599
batch start
#iterations: 201
currently lose_sum: 93.50274765491486
time_elpased: 1.547
batch start
#iterations: 202
currently lose_sum: 94.14618182182312
time_elpased: 1.539
batch start
#iterations: 203
currently lose_sum: 93.64350420236588
time_elpased: 1.57
batch start
#iterations: 204
currently lose_sum: 93.7498310804367
time_elpased: 1.556
batch start
#iterations: 205
currently lose_sum: 94.01577234268188
time_elpased: 1.533
batch start
#iterations: 206
currently lose_sum: 93.68128550052643
time_elpased: 1.54
batch start
#iterations: 207
currently lose_sum: 94.14360642433167
time_elpased: 1.558
batch start
#iterations: 208
currently lose_sum: 93.31894642114639
time_elpased: 1.585
batch start
#iterations: 209
currently lose_sum: 93.86829298734665
time_elpased: 1.557
batch start
#iterations: 210
currently lose_sum: 93.79163563251495
time_elpased: 1.55
batch start
#iterations: 211
currently lose_sum: 93.46528536081314
time_elpased: 1.608
batch start
#iterations: 212
currently lose_sum: 93.94860583543777
time_elpased: 1.507
batch start
#iterations: 213
currently lose_sum: 93.92694395780563
time_elpased: 1.612
batch start
#iterations: 214
currently lose_sum: 94.20104247331619
time_elpased: 1.57
batch start
#iterations: 215
currently lose_sum: 93.65117520093918
time_elpased: 1.591
batch start
#iterations: 216
currently lose_sum: 93.70731800794601
time_elpased: 1.57
batch start
#iterations: 217
currently lose_sum: 93.88147932291031
time_elpased: 1.545
batch start
#iterations: 218
currently lose_sum: 93.94394344091415
time_elpased: 1.54
batch start
#iterations: 219
currently lose_sum: 94.155313372612
time_elpased: 1.584
start validation test
0.671701030928
0.643715659341
0.771637336627
0.701895623684
0.671525577329
60.263
batch start
#iterations: 220
currently lose_sum: 93.42780870199203
time_elpased: 1.594
batch start
#iterations: 221
currently lose_sum: 94.04501056671143
time_elpased: 1.566
batch start
#iterations: 222
currently lose_sum: 93.71048736572266
time_elpased: 1.567
batch start
#iterations: 223
currently lose_sum: 93.90749228000641
time_elpased: 1.531
batch start
#iterations: 224
currently lose_sum: 93.94638949632645
time_elpased: 1.557
batch start
#iterations: 225
currently lose_sum: 93.97268062829971
time_elpased: 1.587
batch start
#iterations: 226
currently lose_sum: 93.72063624858856
time_elpased: 1.563
batch start
#iterations: 227
currently lose_sum: 93.74867576360703
time_elpased: 1.59
batch start
#iterations: 228
currently lose_sum: 93.58043611049652
time_elpased: 1.622
batch start
#iterations: 229
currently lose_sum: 93.79741007089615
time_elpased: 1.545
batch start
#iterations: 230
currently lose_sum: 93.7800218462944
time_elpased: 1.554
batch start
#iterations: 231
currently lose_sum: 93.8589455485344
time_elpased: 1.57
batch start
#iterations: 232
currently lose_sum: 93.72899985313416
time_elpased: 1.549
batch start
#iterations: 233
currently lose_sum: 94.01687675714493
time_elpased: 1.563
batch start
#iterations: 234
currently lose_sum: 93.79075330495834
time_elpased: 1.585
batch start
#iterations: 235
currently lose_sum: 94.05954474210739
time_elpased: 1.573
batch start
#iterations: 236
currently lose_sum: 93.97616600990295
time_elpased: 1.548
batch start
#iterations: 237
currently lose_sum: 93.9850624203682
time_elpased: 1.548
batch start
#iterations: 238
currently lose_sum: 94.23987197875977
time_elpased: 1.582
batch start
#iterations: 239
currently lose_sum: 93.83379232883453
time_elpased: 1.589
start validation test
0.666494845361
0.644426652433
0.745497581558
0.691287336578
0.666356143872
60.455
batch start
#iterations: 240
currently lose_sum: 93.803660094738
time_elpased: 1.528
batch start
#iterations: 241
currently lose_sum: 93.9614006280899
time_elpased: 1.566
batch start
#iterations: 242
currently lose_sum: 93.7394146323204
time_elpased: 1.588
batch start
#iterations: 243
currently lose_sum: 94.33217424154282
time_elpased: 1.575
batch start
#iterations: 244
currently lose_sum: 93.8158888220787
time_elpased: 1.593
batch start
#iterations: 245
currently lose_sum: 94.2763746380806
time_elpased: 1.537
batch start
#iterations: 246
currently lose_sum: 93.73360657691956
time_elpased: 1.572
batch start
#iterations: 247
currently lose_sum: 93.89746284484863
time_elpased: 1.527
batch start
#iterations: 248
currently lose_sum: 93.5218613743782
time_elpased: 1.559
batch start
#iterations: 249
currently lose_sum: 94.04513871669769
time_elpased: 1.539
batch start
#iterations: 250
currently lose_sum: 93.7562381029129
time_elpased: 1.549
batch start
#iterations: 251
currently lose_sum: 94.23464149236679
time_elpased: 1.54
batch start
#iterations: 252
currently lose_sum: 93.73522245883942
time_elpased: 1.576
batch start
#iterations: 253
currently lose_sum: 94.17340886592865
time_elpased: 1.535
batch start
#iterations: 254
currently lose_sum: 93.77613312005997
time_elpased: 1.536
batch start
#iterations: 255
currently lose_sum: 93.93230944871902
time_elpased: 1.554
batch start
#iterations: 256
currently lose_sum: 93.61205196380615
time_elpased: 1.555
batch start
#iterations: 257
currently lose_sum: 93.43572551012039
time_elpased: 1.546
batch start
#iterations: 258
currently lose_sum: 93.47295391559601
time_elpased: 1.603
batch start
#iterations: 259
currently lose_sum: 93.80493181943893
time_elpased: 1.616
start validation test
0.667010309278
0.643215196553
0.752701451065
0.693664643399
0.666859865262
60.461
batch start
#iterations: 260
currently lose_sum: 93.57599222660065
time_elpased: 1.55
batch start
#iterations: 261
currently lose_sum: 93.73066288232803
time_elpased: 1.605
batch start
#iterations: 262
currently lose_sum: 93.46590912342072
time_elpased: 1.526
batch start
#iterations: 263
currently lose_sum: 93.66454696655273
time_elpased: 1.557
batch start
#iterations: 264
currently lose_sum: 93.95274096727371
time_elpased: 1.55
batch start
#iterations: 265
currently lose_sum: 93.94019287824631
time_elpased: 1.566
batch start
#iterations: 266
currently lose_sum: 94.13684666156769
time_elpased: 1.537
batch start
#iterations: 267
currently lose_sum: 93.77258414030075
time_elpased: 1.577
batch start
#iterations: 268
currently lose_sum: 93.66510397195816
time_elpased: 1.51
batch start
#iterations: 269
currently lose_sum: 93.66715431213379
time_elpased: 1.528
batch start
#iterations: 270
currently lose_sum: 94.02521866559982
time_elpased: 1.553
batch start
#iterations: 271
currently lose_sum: 93.79404252767563
time_elpased: 1.57
batch start
#iterations: 272
currently lose_sum: 93.97320312261581
time_elpased: 1.528
batch start
#iterations: 273
currently lose_sum: 93.60096973180771
time_elpased: 1.534
batch start
#iterations: 274
currently lose_sum: 94.31586080789566
time_elpased: 1.567
batch start
#iterations: 275
currently lose_sum: 94.05270034074783
time_elpased: 1.54
batch start
#iterations: 276
currently lose_sum: 93.56902968883514
time_elpased: 1.533
batch start
#iterations: 277
currently lose_sum: 94.03778266906738
time_elpased: 1.586
batch start
#iterations: 278
currently lose_sum: 93.66566073894501
time_elpased: 1.55
batch start
#iterations: 279
currently lose_sum: 93.12539649009705
time_elpased: 1.55
start validation test
0.671907216495
0.641841570752
0.780487804878
0.704407188966
0.671716586525
60.398
batch start
#iterations: 280
currently lose_sum: 93.85467541217804
time_elpased: 1.615
batch start
#iterations: 281
currently lose_sum: 94.00680392980576
time_elpased: 1.55
batch start
#iterations: 282
currently lose_sum: 93.58532047271729
time_elpased: 1.569
batch start
#iterations: 283
currently lose_sum: 93.9852796792984
time_elpased: 1.61
batch start
#iterations: 284
currently lose_sum: 93.69383937120438
time_elpased: 1.531
batch start
#iterations: 285
currently lose_sum: 93.61455231904984
time_elpased: 1.586
batch start
#iterations: 286
currently lose_sum: 93.37023550271988
time_elpased: 1.557
batch start
#iterations: 287
currently lose_sum: 93.43706941604614
time_elpased: 1.568
batch start
#iterations: 288
currently lose_sum: 93.25707352161407
time_elpased: 1.59
batch start
#iterations: 289
currently lose_sum: 94.34255981445312
time_elpased: 1.54
batch start
#iterations: 290
currently lose_sum: 93.74099910259247
time_elpased: 1.565
batch start
#iterations: 291
currently lose_sum: 93.47975778579712
time_elpased: 1.585
batch start
#iterations: 292
currently lose_sum: 93.73027330636978
time_elpased: 1.585
batch start
#iterations: 293
currently lose_sum: 93.65892338752747
time_elpased: 1.591
batch start
#iterations: 294
currently lose_sum: 93.68239349126816
time_elpased: 1.583
batch start
#iterations: 295
currently lose_sum: 94.02742999792099
time_elpased: 1.572
batch start
#iterations: 296
currently lose_sum: 93.69150364398956
time_elpased: 1.527
batch start
#iterations: 297
currently lose_sum: 93.56335473060608
time_elpased: 1.584
batch start
#iterations: 298
currently lose_sum: 93.9543708562851
time_elpased: 1.565
batch start
#iterations: 299
currently lose_sum: 93.55627715587616
time_elpased: 1.547
start validation test
0.668762886598
0.641402423305
0.768138314295
0.699072773251
0.668588417707
60.479
batch start
#iterations: 300
currently lose_sum: 93.15282213687897
time_elpased: 1.544
batch start
#iterations: 301
currently lose_sum: 94.04028856754303
time_elpased: 1.566
batch start
#iterations: 302
currently lose_sum: 93.54334610700607
time_elpased: 1.604
batch start
#iterations: 303
currently lose_sum: 94.0182541012764
time_elpased: 1.548
batch start
#iterations: 304
currently lose_sum: 93.6631469130516
time_elpased: 1.576
batch start
#iterations: 305
currently lose_sum: 94.08968949317932
time_elpased: 1.602
batch start
#iterations: 306
currently lose_sum: 93.63466960191727
time_elpased: 1.553
batch start
#iterations: 307
currently lose_sum: 93.90165716409683
time_elpased: 1.567
batch start
#iterations: 308
currently lose_sum: 93.81147825717926
time_elpased: 1.515
batch start
#iterations: 309
currently lose_sum: 93.6323830485344
time_elpased: 1.56
batch start
#iterations: 310
currently lose_sum: 93.81127840280533
time_elpased: 1.527
batch start
#iterations: 311
currently lose_sum: 93.43710994720459
time_elpased: 1.554
batch start
#iterations: 312
currently lose_sum: 93.32170873880386
time_elpased: 1.555
batch start
#iterations: 313
currently lose_sum: 94.15898025035858
time_elpased: 1.586
batch start
#iterations: 314
currently lose_sum: 93.47693544626236
time_elpased: 1.56
batch start
#iterations: 315
currently lose_sum: 93.77802687883377
time_elpased: 1.592
batch start
#iterations: 316
currently lose_sum: 93.54039740562439
time_elpased: 1.604
batch start
#iterations: 317
currently lose_sum: 93.75807493925095
time_elpased: 1.57
batch start
#iterations: 318
currently lose_sum: 93.59372740983963
time_elpased: 1.538
batch start
#iterations: 319
currently lose_sum: 94.155886054039
time_elpased: 1.608
start validation test
0.661443298969
0.647080803363
0.712874343933
0.678386054255
0.661353003837
60.698
batch start
#iterations: 320
currently lose_sum: 93.92283993959427
time_elpased: 1.604
batch start
#iterations: 321
currently lose_sum: 93.8370885848999
time_elpased: 1.529
batch start
#iterations: 322
currently lose_sum: 93.50957471132278
time_elpased: 1.6
batch start
#iterations: 323
currently lose_sum: 94.19873267412186
time_elpased: 1.549
batch start
#iterations: 324
currently lose_sum: 93.81700658798218
time_elpased: 1.549
batch start
#iterations: 325
currently lose_sum: 93.64466291666031
time_elpased: 1.577
batch start
#iterations: 326
currently lose_sum: 93.83396190404892
time_elpased: 1.539
batch start
#iterations: 327
currently lose_sum: 93.27667319774628
time_elpased: 1.537
batch start
#iterations: 328
currently lose_sum: 93.6648159623146
time_elpased: 1.542
batch start
#iterations: 329
currently lose_sum: 93.54983949661255
time_elpased: 1.532
batch start
#iterations: 330
currently lose_sum: 93.58597362041473
time_elpased: 1.639
batch start
#iterations: 331
currently lose_sum: 93.19296306371689
time_elpased: 1.559
batch start
#iterations: 332
currently lose_sum: 93.24175441265106
time_elpased: 1.586
batch start
#iterations: 333
currently lose_sum: 93.37106198072433
time_elpased: 1.533
batch start
#iterations: 334
currently lose_sum: 93.66385525465012
time_elpased: 1.566
batch start
#iterations: 335
currently lose_sum: 93.48591130971909
time_elpased: 1.523
batch start
#iterations: 336
currently lose_sum: 93.64757239818573
time_elpased: 1.573
batch start
#iterations: 337
currently lose_sum: 93.44912302494049
time_elpased: 1.58
batch start
#iterations: 338
currently lose_sum: 93.7331303358078
time_elpased: 1.607
batch start
#iterations: 339
currently lose_sum: 93.4793598651886
time_elpased: 1.548
start validation test
0.672371134021
0.647115468791
0.760728619944
0.699337748344
0.672216008826
60.271
batch start
#iterations: 340
currently lose_sum: 93.94332951307297
time_elpased: 1.532
batch start
#iterations: 341
currently lose_sum: 93.94816786050797
time_elpased: 1.613
batch start
#iterations: 342
currently lose_sum: 93.55119514465332
time_elpased: 1.571
batch start
#iterations: 343
currently lose_sum: 93.22328078746796
time_elpased: 1.607
batch start
#iterations: 344
currently lose_sum: 93.68119978904724
time_elpased: 1.617
batch start
#iterations: 345
currently lose_sum: 93.58133208751678
time_elpased: 1.572
batch start
#iterations: 346
currently lose_sum: 94.14513832330704
time_elpased: 1.579
batch start
#iterations: 347
currently lose_sum: 93.74628788232803
time_elpased: 1.657
batch start
#iterations: 348
currently lose_sum: 93.72503530979156
time_elpased: 1.524
batch start
#iterations: 349
currently lose_sum: 93.68887811899185
time_elpased: 1.577
batch start
#iterations: 350
currently lose_sum: 93.54075014591217
time_elpased: 1.57
batch start
#iterations: 351
currently lose_sum: 93.69153678417206
time_elpased: 1.575
batch start
#iterations: 352
currently lose_sum: 93.64400607347488
time_elpased: 1.543
batch start
#iterations: 353
currently lose_sum: 94.20760011672974
time_elpased: 1.554
batch start
#iterations: 354
currently lose_sum: 94.16111665964127
time_elpased: 1.578
batch start
#iterations: 355
currently lose_sum: 93.72671008110046
time_elpased: 1.589
batch start
#iterations: 356
currently lose_sum: 93.30263233184814
time_elpased: 1.57
batch start
#iterations: 357
currently lose_sum: 94.17671656608582
time_elpased: 1.591
batch start
#iterations: 358
currently lose_sum: 94.03324061632156
time_elpased: 1.6
batch start
#iterations: 359
currently lose_sum: 93.68166464567184
time_elpased: 1.539
start validation test
0.669278350515
0.645047895246
0.755377174025
0.695866514979
0.669127190751
60.564
batch start
#iterations: 360
currently lose_sum: 93.91606652736664
time_elpased: 1.555
batch start
#iterations: 361
currently lose_sum: 94.05601364374161
time_elpased: 1.579
batch start
#iterations: 362
currently lose_sum: 92.94051450490952
time_elpased: 1.589
batch start
#iterations: 363
currently lose_sum: 93.90042817592621
time_elpased: 1.592
batch start
#iterations: 364
currently lose_sum: 94.07959127426147
time_elpased: 1.556
batch start
#iterations: 365
currently lose_sum: 93.97691315412521
time_elpased: 1.589
batch start
#iterations: 366
currently lose_sum: 93.84514093399048
time_elpased: 1.553
batch start
#iterations: 367
currently lose_sum: 93.31279909610748
time_elpased: 1.547
batch start
#iterations: 368
currently lose_sum: 93.97683638334274
time_elpased: 1.554
batch start
#iterations: 369
currently lose_sum: 93.40270644426346
time_elpased: 1.561
batch start
#iterations: 370
currently lose_sum: 93.70054513216019
time_elpased: 1.614
batch start
#iterations: 371
currently lose_sum: 93.92157822847366
time_elpased: 1.538
batch start
#iterations: 372
currently lose_sum: 93.71940338611603
time_elpased: 1.584
batch start
#iterations: 373
currently lose_sum: 93.57697373628616
time_elpased: 1.532
batch start
#iterations: 374
currently lose_sum: 93.73348307609558
time_elpased: 1.569
batch start
#iterations: 375
currently lose_sum: 93.58521503210068
time_elpased: 1.597
batch start
#iterations: 376
currently lose_sum: 93.52802121639252
time_elpased: 1.547
batch start
#iterations: 377
currently lose_sum: 93.87938845157623
time_elpased: 1.567
batch start
#iterations: 378
currently lose_sum: 93.46015882492065
time_elpased: 1.586
batch start
#iterations: 379
currently lose_sum: 93.58462172746658
time_elpased: 1.536
start validation test
0.66824742268
0.644601145879
0.752598538644
0.694425980439
0.668099331286
60.474
batch start
#iterations: 380
currently lose_sum: 93.62142288684845
time_elpased: 1.559
batch start
#iterations: 381
currently lose_sum: 93.49963945150375
time_elpased: 1.549
batch start
#iterations: 382
currently lose_sum: 93.59327691793442
time_elpased: 1.544
batch start
#iterations: 383
currently lose_sum: 93.36192858219147
time_elpased: 1.526
batch start
#iterations: 384
currently lose_sum: 93.80091804265976
time_elpased: 1.621
batch start
#iterations: 385
currently lose_sum: 93.67634582519531
time_elpased: 1.559
batch start
#iterations: 386
currently lose_sum: 93.38099890947342
time_elpased: 1.554
batch start
#iterations: 387
currently lose_sum: 93.516486287117
time_elpased: 1.581
batch start
#iterations: 388
currently lose_sum: 93.98941826820374
time_elpased: 1.548
batch start
#iterations: 389
currently lose_sum: 93.74440801143646
time_elpased: 1.599
batch start
#iterations: 390
currently lose_sum: 93.56124478578568
time_elpased: 1.523
batch start
#iterations: 391
currently lose_sum: 93.37782227993011
time_elpased: 1.62
batch start
#iterations: 392
currently lose_sum: 93.3017486333847
time_elpased: 1.582
batch start
#iterations: 393
currently lose_sum: 93.57381635904312
time_elpased: 1.568
batch start
#iterations: 394
currently lose_sum: 93.58283150196075
time_elpased: 1.535
batch start
#iterations: 395
currently lose_sum: 93.6451763510704
time_elpased: 1.579
batch start
#iterations: 396
currently lose_sum: 93.27620214223862
time_elpased: 1.581
batch start
#iterations: 397
currently lose_sum: 93.35012370347977
time_elpased: 1.585
batch start
#iterations: 398
currently lose_sum: 93.62821799516678
time_elpased: 1.545
batch start
#iterations: 399
currently lose_sum: 93.56039220094681
time_elpased: 1.521
start validation test
0.666649484536
0.641698639693
0.757332510034
0.694736842105
0.666490276498
60.455
acc: 0.669
pre: 0.642
rec: 0.765
F1: 0.698
auc: 0.711
