start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.08597147464752
time_elpased: 1.594
batch start
#iterations: 1
currently lose_sum: 99.50024700164795
time_elpased: 1.547
batch start
#iterations: 2
currently lose_sum: 99.02348929643631
time_elpased: 1.604
batch start
#iterations: 3
currently lose_sum: 98.22098970413208
time_elpased: 1.57
batch start
#iterations: 4
currently lose_sum: 98.16165488958359
time_elpased: 1.512
batch start
#iterations: 5
currently lose_sum: 98.01219707727432
time_elpased: 1.53
batch start
#iterations: 6
currently lose_sum: 97.4960932135582
time_elpased: 1.564
batch start
#iterations: 7
currently lose_sum: 97.60758203268051
time_elpased: 1.574
batch start
#iterations: 8
currently lose_sum: 97.25829845666885
time_elpased: 1.541
batch start
#iterations: 9
currently lose_sum: 97.55373859405518
time_elpased: 1.574
batch start
#iterations: 10
currently lose_sum: 97.03372865915298
time_elpased: 1.587
batch start
#iterations: 11
currently lose_sum: 97.1101484298706
time_elpased: 1.554
batch start
#iterations: 12
currently lose_sum: 97.15806037187576
time_elpased: 1.566
batch start
#iterations: 13
currently lose_sum: 96.70238572359085
time_elpased: 1.579
batch start
#iterations: 14
currently lose_sum: 96.39066088199615
time_elpased: 1.552
batch start
#iterations: 15
currently lose_sum: 96.71255618333817
time_elpased: 1.544
batch start
#iterations: 16
currently lose_sum: 96.44320052862167
time_elpased: 1.555
batch start
#iterations: 17
currently lose_sum: 96.32286268472672
time_elpased: 1.551
batch start
#iterations: 18
currently lose_sum: 96.27623790502548
time_elpased: 1.526
batch start
#iterations: 19
currently lose_sum: 96.16845083236694
time_elpased: 1.526
start validation test
0.645979381443
0.631302424187
0.704847175054
0.666050763396
0.645876029952
62.040
batch start
#iterations: 20
currently lose_sum: 96.62165755033493
time_elpased: 1.583
batch start
#iterations: 21
currently lose_sum: 96.13602691888809
time_elpased: 1.532
batch start
#iterations: 22
currently lose_sum: 96.13004344701767
time_elpased: 1.548
batch start
#iterations: 23
currently lose_sum: 95.72083473205566
time_elpased: 1.535
batch start
#iterations: 24
currently lose_sum: 95.79224514961243
time_elpased: 1.594
batch start
#iterations: 25
currently lose_sum: 96.11721515655518
time_elpased: 1.562
batch start
#iterations: 26
currently lose_sum: 95.72361052036285
time_elpased: 1.584
batch start
#iterations: 27
currently lose_sum: 95.721266746521
time_elpased: 1.549
batch start
#iterations: 28
currently lose_sum: 95.57766532897949
time_elpased: 1.557
batch start
#iterations: 29
currently lose_sum: 96.35425144433975
time_elpased: 1.579
batch start
#iterations: 30
currently lose_sum: 95.54640728235245
time_elpased: 1.548
batch start
#iterations: 31
currently lose_sum: 95.54371863603592
time_elpased: 1.521
batch start
#iterations: 32
currently lose_sum: 95.53381407260895
time_elpased: 1.575
batch start
#iterations: 33
currently lose_sum: 95.68916320800781
time_elpased: 1.539
batch start
#iterations: 34
currently lose_sum: 95.71233582496643
time_elpased: 1.528
batch start
#iterations: 35
currently lose_sum: 95.82265716791153
time_elpased: 1.534
batch start
#iterations: 36
currently lose_sum: 95.19012308120728
time_elpased: 1.554
batch start
#iterations: 37
currently lose_sum: 95.44986361265182
time_elpased: 1.521
batch start
#iterations: 38
currently lose_sum: 95.4612432718277
time_elpased: 1.592
batch start
#iterations: 39
currently lose_sum: 95.46438443660736
time_elpased: 1.559
start validation test
0.654432989691
0.636150022594
0.724400535145
0.677413145992
0.654310150873
61.364
batch start
#iterations: 40
currently lose_sum: 95.30362939834595
time_elpased: 1.582
batch start
#iterations: 41
currently lose_sum: 95.3849726319313
time_elpased: 1.553
batch start
#iterations: 42
currently lose_sum: 95.16668260097504
time_elpased: 1.569
batch start
#iterations: 43
currently lose_sum: 95.4819267988205
time_elpased: 1.552
batch start
#iterations: 44
currently lose_sum: 95.4936956167221
time_elpased: 1.554
batch start
#iterations: 45
currently lose_sum: 95.3183718919754
time_elpased: 1.553
batch start
#iterations: 46
currently lose_sum: 95.16162842512131
time_elpased: 1.625
batch start
#iterations: 47
currently lose_sum: 95.361432492733
time_elpased: 1.587
batch start
#iterations: 48
currently lose_sum: 95.42907494306564
time_elpased: 1.581
batch start
#iterations: 49
currently lose_sum: 95.43334090709686
time_elpased: 1.579
batch start
#iterations: 50
currently lose_sum: 95.07285690307617
time_elpased: 1.539
batch start
#iterations: 51
currently lose_sum: 95.30100333690643
time_elpased: 1.552
batch start
#iterations: 52
currently lose_sum: 95.0453513264656
time_elpased: 1.542
batch start
#iterations: 53
currently lose_sum: 95.04466807842255
time_elpased: 1.539
batch start
#iterations: 54
currently lose_sum: 95.0169968008995
time_elpased: 1.571
batch start
#iterations: 55
currently lose_sum: 95.23340129852295
time_elpased: 1.548
batch start
#iterations: 56
currently lose_sum: 95.44517803192139
time_elpased: 1.539
batch start
#iterations: 57
currently lose_sum: 95.31149566173553
time_elpased: 1.56
batch start
#iterations: 58
currently lose_sum: 95.15340733528137
time_elpased: 1.523
batch start
#iterations: 59
currently lose_sum: 95.11642044782639
time_elpased: 1.534
start validation test
0.667010309278
0.645622820352
0.743027683441
0.690909090909
0.666876849053
60.913
batch start
#iterations: 60
currently lose_sum: 94.971404671669
time_elpased: 1.574
batch start
#iterations: 61
currently lose_sum: 95.05701041221619
time_elpased: 1.561
batch start
#iterations: 62
currently lose_sum: 95.53828382492065
time_elpased: 1.555
batch start
#iterations: 63
currently lose_sum: 95.18866449594498
time_elpased: 1.57
batch start
#iterations: 64
currently lose_sum: 94.98555397987366
time_elpased: 1.605
batch start
#iterations: 65
currently lose_sum: 94.8837720155716
time_elpased: 1.564
batch start
#iterations: 66
currently lose_sum: 94.97426217794418
time_elpased: 1.549
batch start
#iterations: 67
currently lose_sum: 94.91858333349228
time_elpased: 1.554
batch start
#iterations: 68
currently lose_sum: 95.06570988893509
time_elpased: 1.534
batch start
#iterations: 69
currently lose_sum: 94.7010109424591
time_elpased: 1.564
batch start
#iterations: 70
currently lose_sum: 94.87962168455124
time_elpased: 1.554
batch start
#iterations: 71
currently lose_sum: 95.01237148046494
time_elpased: 1.556
batch start
#iterations: 72
currently lose_sum: 94.88193821907043
time_elpased: 1.536
batch start
#iterations: 73
currently lose_sum: 95.1665009856224
time_elpased: 1.557
batch start
#iterations: 74
currently lose_sum: 95.12736755609512
time_elpased: 1.563
batch start
#iterations: 75
currently lose_sum: 95.19148421287537
time_elpased: 1.521
batch start
#iterations: 76
currently lose_sum: 94.65899401903152
time_elpased: 1.554
batch start
#iterations: 77
currently lose_sum: 94.97110068798065
time_elpased: 1.6
batch start
#iterations: 78
currently lose_sum: 95.02769887447357
time_elpased: 1.56
batch start
#iterations: 79
currently lose_sum: 95.08333146572113
time_elpased: 1.548
start validation test
0.659484536082
0.646207350315
0.707522898014
0.67547651798
0.659400197329
61.128
batch start
#iterations: 80
currently lose_sum: 95.20826262235641
time_elpased: 1.58
batch start
#iterations: 81
currently lose_sum: 95.06648200750351
time_elpased: 1.545
batch start
#iterations: 82
currently lose_sum: 95.02119714021683
time_elpased: 1.597
batch start
#iterations: 83
currently lose_sum: 94.87916034460068
time_elpased: 1.558
batch start
#iterations: 84
currently lose_sum: 94.98759031295776
time_elpased: 1.539
batch start
#iterations: 85
currently lose_sum: 94.88650804758072
time_elpased: 1.61
batch start
#iterations: 86
currently lose_sum: 94.60161292552948
time_elpased: 1.518
batch start
#iterations: 87
currently lose_sum: 94.76073360443115
time_elpased: 1.559
batch start
#iterations: 88
currently lose_sum: 94.58839935064316
time_elpased: 1.567
batch start
#iterations: 89
currently lose_sum: 95.05382424592972
time_elpased: 1.565
batch start
#iterations: 90
currently lose_sum: 95.07790106534958
time_elpased: 1.555
batch start
#iterations: 91
currently lose_sum: 94.80099004507065
time_elpased: 1.595
batch start
#iterations: 92
currently lose_sum: 94.72470617294312
time_elpased: 1.545
batch start
#iterations: 93
currently lose_sum: 94.88883364200592
time_elpased: 1.631
batch start
#iterations: 94
currently lose_sum: 94.72543001174927
time_elpased: 1.58
batch start
#iterations: 95
currently lose_sum: 94.33272588253021
time_elpased: 1.622
batch start
#iterations: 96
currently lose_sum: 94.53522175550461
time_elpased: 1.525
batch start
#iterations: 97
currently lose_sum: 94.70436561107635
time_elpased: 1.52
batch start
#iterations: 98
currently lose_sum: 95.00258022546768
time_elpased: 1.55
batch start
#iterations: 99
currently lose_sum: 94.70497369766235
time_elpased: 1.547
start validation test
0.662422680412
0.639805825243
0.746012143666
0.688839264503
0.662275926217
60.723
batch start
#iterations: 100
currently lose_sum: 95.02332907915115
time_elpased: 1.545
batch start
#iterations: 101
currently lose_sum: 94.8205937743187
time_elpased: 1.587
batch start
#iterations: 102
currently lose_sum: 94.61847472190857
time_elpased: 1.59
batch start
#iterations: 103
currently lose_sum: 94.74373143911362
time_elpased: 1.541
batch start
#iterations: 104
currently lose_sum: 94.6529643535614
time_elpased: 1.546
batch start
#iterations: 105
currently lose_sum: 94.38836252689362
time_elpased: 1.545
batch start
#iterations: 106
currently lose_sum: 94.81300073862076
time_elpased: 1.549
batch start
#iterations: 107
currently lose_sum: 94.65088278055191
time_elpased: 1.566
batch start
#iterations: 108
currently lose_sum: 94.51109343767166
time_elpased: 1.58
batch start
#iterations: 109
currently lose_sum: 94.6306459903717
time_elpased: 1.576
batch start
#iterations: 110
currently lose_sum: 94.56959861516953
time_elpased: 1.537
batch start
#iterations: 111
currently lose_sum: 94.33186131715775
time_elpased: 1.573
batch start
#iterations: 112
currently lose_sum: 94.25004869699478
time_elpased: 1.55
batch start
#iterations: 113
currently lose_sum: 94.23776537179947
time_elpased: 1.557
batch start
#iterations: 114
currently lose_sum: 94.32059651613235
time_elpased: 1.584
batch start
#iterations: 115
currently lose_sum: 94.69073867797852
time_elpased: 1.578
batch start
#iterations: 116
currently lose_sum: 94.19285362958908
time_elpased: 1.589
batch start
#iterations: 117
currently lose_sum: 94.28896242380142
time_elpased: 1.526
batch start
#iterations: 118
currently lose_sum: 94.14965975284576
time_elpased: 1.624
batch start
#iterations: 119
currently lose_sum: 94.55093967914581
time_elpased: 1.518
start validation test
0.661391752577
0.642109064644
0.731913141916
0.684076371856
0.661267941401
60.758
batch start
#iterations: 120
currently lose_sum: 94.32130175828934
time_elpased: 1.512
batch start
#iterations: 121
currently lose_sum: 94.37394404411316
time_elpased: 1.569
batch start
#iterations: 122
currently lose_sum: 94.3015649318695
time_elpased: 1.555
batch start
#iterations: 123
currently lose_sum: 94.68386036157608
time_elpased: 1.538
batch start
#iterations: 124
currently lose_sum: 94.45752018690109
time_elpased: 1.551
batch start
#iterations: 125
currently lose_sum: 94.32861226797104
time_elpased: 1.588
batch start
#iterations: 126
currently lose_sum: 94.77180129289627
time_elpased: 1.515
batch start
#iterations: 127
currently lose_sum: 94.92422193288803
time_elpased: 1.542
batch start
#iterations: 128
currently lose_sum: 94.42312687635422
time_elpased: 1.587
batch start
#iterations: 129
currently lose_sum: 94.73848021030426
time_elpased: 1.595
batch start
#iterations: 130
currently lose_sum: 94.05925983190536
time_elpased: 1.54
batch start
#iterations: 131
currently lose_sum: 95.02306520938873
time_elpased: 1.531
batch start
#iterations: 132
currently lose_sum: 94.23816519975662
time_elpased: 1.567
batch start
#iterations: 133
currently lose_sum: 94.29086720943451
time_elpased: 1.595
batch start
#iterations: 134
currently lose_sum: 94.31269365549088
time_elpased: 1.561
batch start
#iterations: 135
currently lose_sum: 94.33907985687256
time_elpased: 1.58
batch start
#iterations: 136
currently lose_sum: 94.33168053627014
time_elpased: 1.548
batch start
#iterations: 137
currently lose_sum: 94.27417862415314
time_elpased: 1.561
batch start
#iterations: 138
currently lose_sum: 94.31240725517273
time_elpased: 1.574
batch start
#iterations: 139
currently lose_sum: 94.13344258069992
time_elpased: 1.583
start validation test
0.664742268041
0.64
0.755788823711
0.693091732729
0.66458242177
60.535
batch start
#iterations: 140
currently lose_sum: 94.32344377040863
time_elpased: 1.535
batch start
#iterations: 141
currently lose_sum: 94.42428386211395
time_elpased: 1.529
batch start
#iterations: 142
currently lose_sum: 94.10499453544617
time_elpased: 1.601
batch start
#iterations: 143
currently lose_sum: 94.43097621202469
time_elpased: 1.549
batch start
#iterations: 144
currently lose_sum: 94.60284864902496
time_elpased: 1.571
batch start
#iterations: 145
currently lose_sum: 94.18261975049973
time_elpased: 1.533
batch start
#iterations: 146
currently lose_sum: 94.02309381961823
time_elpased: 1.525
batch start
#iterations: 147
currently lose_sum: 94.2995235323906
time_elpased: 1.575
batch start
#iterations: 148
currently lose_sum: 94.16307878494263
time_elpased: 1.546
batch start
#iterations: 149
currently lose_sum: 94.11446297168732
time_elpased: 1.575
batch start
#iterations: 150
currently lose_sum: 94.33643585443497
time_elpased: 1.533
batch start
#iterations: 151
currently lose_sum: 94.06069213151932
time_elpased: 1.589
batch start
#iterations: 152
currently lose_sum: 94.35729253292084
time_elpased: 1.556
batch start
#iterations: 153
currently lose_sum: 94.5102733373642
time_elpased: 1.547
batch start
#iterations: 154
currently lose_sum: 94.1122989654541
time_elpased: 1.551
batch start
#iterations: 155
currently lose_sum: 94.28272300958633
time_elpased: 1.545
batch start
#iterations: 156
currently lose_sum: 94.3720126748085
time_elpased: 1.544
batch start
#iterations: 157
currently lose_sum: 93.58730840682983
time_elpased: 1.55
batch start
#iterations: 158
currently lose_sum: 94.20357370376587
time_elpased: 1.567
batch start
#iterations: 159
currently lose_sum: 94.12396603822708
time_elpased: 1.635
start validation test
0.663659793814
0.637966804979
0.759493670886
0.693446088795
0.663491542662
60.593
batch start
#iterations: 160
currently lose_sum: 94.11745089292526
time_elpased: 1.634
batch start
#iterations: 161
currently lose_sum: 94.69436299800873
time_elpased: 1.631
batch start
#iterations: 162
currently lose_sum: 94.4691019654274
time_elpased: 1.529
batch start
#iterations: 163
currently lose_sum: 94.406094789505
time_elpased: 1.562
batch start
#iterations: 164
currently lose_sum: 94.14477747678757
time_elpased: 1.526
batch start
#iterations: 165
currently lose_sum: 94.33171820640564
time_elpased: 1.583
batch start
#iterations: 166
currently lose_sum: 94.36272448301315
time_elpased: 1.567
batch start
#iterations: 167
currently lose_sum: 94.2898381948471
time_elpased: 1.592
batch start
#iterations: 168
currently lose_sum: 94.44930344820023
time_elpased: 1.547
batch start
#iterations: 169
currently lose_sum: 94.13080817461014
time_elpased: 1.567
batch start
#iterations: 170
currently lose_sum: 94.53029584884644
time_elpased: 1.541
batch start
#iterations: 171
currently lose_sum: 94.41421991586685
time_elpased: 1.583
batch start
#iterations: 172
currently lose_sum: 94.36620378494263
time_elpased: 1.527
batch start
#iterations: 173
currently lose_sum: 94.32560050487518
time_elpased: 1.648
batch start
#iterations: 174
currently lose_sum: 93.77746385335922
time_elpased: 1.545
batch start
#iterations: 175
currently lose_sum: 94.078508913517
time_elpased: 1.537
batch start
#iterations: 176
currently lose_sum: 93.72850787639618
time_elpased: 1.56
batch start
#iterations: 177
currently lose_sum: 94.08140301704407
time_elpased: 1.57
batch start
#iterations: 178
currently lose_sum: 93.852647960186
time_elpased: 1.579
batch start
#iterations: 179
currently lose_sum: 93.93592667579651
time_elpased: 1.529
start validation test
0.669690721649
0.640628984275
0.775650921066
0.701703751978
0.669504692176
60.418
batch start
#iterations: 180
currently lose_sum: 94.03956407308578
time_elpased: 1.574
batch start
#iterations: 181
currently lose_sum: 94.05235475301743
time_elpased: 1.601
batch start
#iterations: 182
currently lose_sum: 94.17303401231766
time_elpased: 1.556
batch start
#iterations: 183
currently lose_sum: 94.0235047340393
time_elpased: 1.544
batch start
#iterations: 184
currently lose_sum: 94.49841666221619
time_elpased: 1.627
batch start
#iterations: 185
currently lose_sum: 93.87754321098328
time_elpased: 1.541
batch start
#iterations: 186
currently lose_sum: 94.48066544532776
time_elpased: 1.585
batch start
#iterations: 187
currently lose_sum: 94.16989588737488
time_elpased: 1.56
batch start
#iterations: 188
currently lose_sum: 94.35503739118576
time_elpased: 1.602
batch start
#iterations: 189
currently lose_sum: 94.47074562311172
time_elpased: 1.535
batch start
#iterations: 190
currently lose_sum: 94.00287508964539
time_elpased: 1.525
batch start
#iterations: 191
currently lose_sum: 94.27797156572342
time_elpased: 1.651
batch start
#iterations: 192
currently lose_sum: 94.0694191455841
time_elpased: 1.532
batch start
#iterations: 193
currently lose_sum: 94.08982515335083
time_elpased: 1.578
batch start
#iterations: 194
currently lose_sum: 93.86893808841705
time_elpased: 1.554
batch start
#iterations: 195
currently lose_sum: 94.34882533550262
time_elpased: 1.572
batch start
#iterations: 196
currently lose_sum: 94.02217549085617
time_elpased: 1.562
batch start
#iterations: 197
currently lose_sum: 94.29236871004105
time_elpased: 1.591
batch start
#iterations: 198
currently lose_sum: 94.08917129039764
time_elpased: 1.547
batch start
#iterations: 199
currently lose_sum: 93.79918897151947
time_elpased: 1.551
start validation test
0.668453608247
0.644700907409
0.753113100751
0.694702866907
0.668304975451
60.264
batch start
#iterations: 200
currently lose_sum: 94.05068266391754
time_elpased: 1.538
batch start
#iterations: 201
currently lose_sum: 93.95909720659256
time_elpased: 1.522
batch start
#iterations: 202
currently lose_sum: 93.770656645298
time_elpased: 1.575
batch start
#iterations: 203
currently lose_sum: 94.08639401197433
time_elpased: 1.612
batch start
#iterations: 204
currently lose_sum: 94.14251202344894
time_elpased: 1.536
batch start
#iterations: 205
currently lose_sum: 93.67028367519379
time_elpased: 1.54
batch start
#iterations: 206
currently lose_sum: 93.88530170917511
time_elpased: 1.56
batch start
#iterations: 207
currently lose_sum: 94.14190620183945
time_elpased: 1.586
batch start
#iterations: 208
currently lose_sum: 94.01377415657043
time_elpased: 1.555
batch start
#iterations: 209
currently lose_sum: 93.87984377145767
time_elpased: 1.567
batch start
#iterations: 210
currently lose_sum: 93.78756874799728
time_elpased: 1.522
batch start
#iterations: 211
currently lose_sum: 94.31404554843903
time_elpased: 1.617
batch start
#iterations: 212
currently lose_sum: 94.2196671962738
time_elpased: 1.538
batch start
#iterations: 213
currently lose_sum: 94.11397689580917
time_elpased: 1.55
batch start
#iterations: 214
currently lose_sum: 93.79380178451538
time_elpased: 1.538
batch start
#iterations: 215
currently lose_sum: 93.99805468320847
time_elpased: 1.579
batch start
#iterations: 216
currently lose_sum: 93.95426070690155
time_elpased: 1.56
batch start
#iterations: 217
currently lose_sum: 94.03454566001892
time_elpased: 1.575
batch start
#iterations: 218
currently lose_sum: 93.63180112838745
time_elpased: 1.549
batch start
#iterations: 219
currently lose_sum: 94.33755415678024
time_elpased: 1.557
start validation test
0.667113402062
0.642401468146
0.756509210662
0.694801512287
0.666956453932
60.579
batch start
#iterations: 220
currently lose_sum: 93.9981216788292
time_elpased: 1.6
batch start
#iterations: 221
currently lose_sum: 94.26206457614899
time_elpased: 1.545
batch start
#iterations: 222
currently lose_sum: 93.79855227470398
time_elpased: 1.543
batch start
#iterations: 223
currently lose_sum: 93.5593768954277
time_elpased: 1.562
batch start
#iterations: 224
currently lose_sum: 93.74006396532059
time_elpased: 1.519
batch start
#iterations: 225
currently lose_sum: 94.3281626701355
time_elpased: 1.577
batch start
#iterations: 226
currently lose_sum: 93.80631881952286
time_elpased: 1.571
batch start
#iterations: 227
currently lose_sum: 94.07982414960861
time_elpased: 1.544
batch start
#iterations: 228
currently lose_sum: 93.87278193235397
time_elpased: 1.525
batch start
#iterations: 229
currently lose_sum: 94.00663828849792
time_elpased: 1.563
batch start
#iterations: 230
currently lose_sum: 94.19361478090286
time_elpased: 1.536
batch start
#iterations: 231
currently lose_sum: 93.62424379587173
time_elpased: 1.567
batch start
#iterations: 232
currently lose_sum: 93.95219016075134
time_elpased: 1.557
batch start
#iterations: 233
currently lose_sum: 94.17364197969437
time_elpased: 1.621
batch start
#iterations: 234
currently lose_sum: 93.86688876152039
time_elpased: 1.551
batch start
#iterations: 235
currently lose_sum: 93.64285385608673
time_elpased: 1.578
batch start
#iterations: 236
currently lose_sum: 94.16280114650726
time_elpased: 1.554
batch start
#iterations: 237
currently lose_sum: 94.10163742303848
time_elpased: 1.557
batch start
#iterations: 238
currently lose_sum: 93.90825510025024
time_elpased: 1.59
batch start
#iterations: 239
currently lose_sum: 93.89763104915619
time_elpased: 1.574
start validation test
0.66381443299
0.637989116351
0.760111145415
0.69371653987
0.663645369258
60.423
batch start
#iterations: 240
currently lose_sum: 93.64012485742569
time_elpased: 1.55
batch start
#iterations: 241
currently lose_sum: 94.09209823608398
time_elpased: 1.584
batch start
#iterations: 242
currently lose_sum: 93.98302787542343
time_elpased: 1.525
batch start
#iterations: 243
currently lose_sum: 93.9740999341011
time_elpased: 1.56
batch start
#iterations: 244
currently lose_sum: 94.04771673679352
time_elpased: 1.549
batch start
#iterations: 245
currently lose_sum: 93.71172308921814
time_elpased: 1.547
batch start
#iterations: 246
currently lose_sum: 94.00095909833908
time_elpased: 1.557
batch start
#iterations: 247
currently lose_sum: 93.57201737165451
time_elpased: 1.572
batch start
#iterations: 248
currently lose_sum: 93.82043260335922
time_elpased: 1.551
batch start
#iterations: 249
currently lose_sum: 93.90741896629333
time_elpased: 1.58
batch start
#iterations: 250
currently lose_sum: 93.75253212451935
time_elpased: 1.619
batch start
#iterations: 251
currently lose_sum: 93.80290096998215
time_elpased: 1.634
batch start
#iterations: 252
currently lose_sum: 93.94884836673737
time_elpased: 1.537
batch start
#iterations: 253
currently lose_sum: 93.80974668264389
time_elpased: 1.618
batch start
#iterations: 254
currently lose_sum: 94.1367461681366
time_elpased: 1.54
batch start
#iterations: 255
currently lose_sum: 94.08639413118362
time_elpased: 1.597
batch start
#iterations: 256
currently lose_sum: 93.53842484951019
time_elpased: 1.584
batch start
#iterations: 257
currently lose_sum: 94.01082611083984
time_elpased: 1.554
batch start
#iterations: 258
currently lose_sum: 93.97590559720993
time_elpased: 1.546
batch start
#iterations: 259
currently lose_sum: 93.75453668832779
time_elpased: 1.571
start validation test
0.668144329897
0.643175268535
0.757949984563
0.695861678005
0.667986662219
60.105
batch start
#iterations: 260
currently lose_sum: 93.55360060930252
time_elpased: 1.56
batch start
#iterations: 261
currently lose_sum: 93.22338658571243
time_elpased: 1.533
batch start
#iterations: 262
currently lose_sum: 94.10265523195267
time_elpased: 1.542
batch start
#iterations: 263
currently lose_sum: 93.93322676420212
time_elpased: 1.614
batch start
#iterations: 264
currently lose_sum: 93.7539935708046
time_elpased: 1.545
batch start
#iterations: 265
currently lose_sum: 93.87011355161667
time_elpased: 1.594
batch start
#iterations: 266
currently lose_sum: 93.48646640777588
time_elpased: 1.538
batch start
#iterations: 267
currently lose_sum: 93.90747159719467
time_elpased: 1.587
batch start
#iterations: 268
currently lose_sum: 93.78711050748825
time_elpased: 1.575
batch start
#iterations: 269
currently lose_sum: 93.69699990749359
time_elpased: 1.534
batch start
#iterations: 270
currently lose_sum: 93.75657564401627
time_elpased: 1.57
batch start
#iterations: 271
currently lose_sum: 93.84819215536118
time_elpased: 1.544
batch start
#iterations: 272
currently lose_sum: 93.5642459988594
time_elpased: 1.63
batch start
#iterations: 273
currently lose_sum: 93.40059345960617
time_elpased: 1.601
batch start
#iterations: 274
currently lose_sum: 94.11216461658478
time_elpased: 1.569
batch start
#iterations: 275
currently lose_sum: 93.87842637300491
time_elpased: 1.578
batch start
#iterations: 276
currently lose_sum: 93.93162095546722
time_elpased: 1.532
batch start
#iterations: 277
currently lose_sum: 93.53904515504837
time_elpased: 1.569
batch start
#iterations: 278
currently lose_sum: 93.57863730192184
time_elpased: 1.546
batch start
#iterations: 279
currently lose_sum: 93.92284423112869
time_elpased: 1.56
start validation test
0.668453608247
0.641144624903
0.76782957703
0.698791795448
0.668279138407
60.460
batch start
#iterations: 280
currently lose_sum: 93.8151690363884
time_elpased: 1.62
batch start
#iterations: 281
currently lose_sum: 93.6992963552475
time_elpased: 1.582
batch start
#iterations: 282
currently lose_sum: 93.74416840076447
time_elpased: 1.575
batch start
#iterations: 283
currently lose_sum: 93.88278985023499
time_elpased: 1.623
batch start
#iterations: 284
currently lose_sum: 93.50618189573288
time_elpased: 1.573
batch start
#iterations: 285
currently lose_sum: 93.70435523986816
time_elpased: 1.544
batch start
#iterations: 286
currently lose_sum: 93.592609167099
time_elpased: 1.564
batch start
#iterations: 287
currently lose_sum: 93.65053355693817
time_elpased: 1.561
batch start
#iterations: 288
currently lose_sum: 93.8961232304573
time_elpased: 1.631
batch start
#iterations: 289
currently lose_sum: 93.36442720890045
time_elpased: 1.568
batch start
#iterations: 290
currently lose_sum: 93.85028749704361
time_elpased: 1.546
batch start
#iterations: 291
currently lose_sum: 94.00619089603424
time_elpased: 1.56
batch start
#iterations: 292
currently lose_sum: 93.65517050027847
time_elpased: 1.572
batch start
#iterations: 293
currently lose_sum: 93.56376665830612
time_elpased: 1.558
batch start
#iterations: 294
currently lose_sum: 93.65886574983597
time_elpased: 1.544
batch start
#iterations: 295
currently lose_sum: 94.09657317399979
time_elpased: 1.556
batch start
#iterations: 296
currently lose_sum: 93.72424179315567
time_elpased: 1.581
batch start
#iterations: 297
currently lose_sum: 93.86541271209717
time_elpased: 1.569
batch start
#iterations: 298
currently lose_sum: 93.92630738019943
time_elpased: 1.527
batch start
#iterations: 299
currently lose_sum: 93.57245111465454
time_elpased: 1.558
start validation test
0.667113402062
0.644446414325
0.748173304518
0.692446899705
0.6669710889
60.338
batch start
#iterations: 300
currently lose_sum: 93.65924167633057
time_elpased: 1.564
batch start
#iterations: 301
currently lose_sum: 94.07294803857803
time_elpased: 1.567
batch start
#iterations: 302
currently lose_sum: 93.63382351398468
time_elpased: 1.544
batch start
#iterations: 303
currently lose_sum: 93.78364688158035
time_elpased: 1.599
batch start
#iterations: 304
currently lose_sum: 93.7599127292633
time_elpased: 1.55
batch start
#iterations: 305
currently lose_sum: 93.50199657678604
time_elpased: 1.546
batch start
#iterations: 306
currently lose_sum: 93.56316816806793
time_elpased: 1.578
batch start
#iterations: 307
currently lose_sum: 94.16477751731873
time_elpased: 1.55
batch start
#iterations: 308
currently lose_sum: 93.33484268188477
time_elpased: 1.542
batch start
#iterations: 309
currently lose_sum: 93.5595777630806
time_elpased: 1.554
batch start
#iterations: 310
currently lose_sum: 93.83499383926392
time_elpased: 1.508
batch start
#iterations: 311
currently lose_sum: 93.6286107301712
time_elpased: 1.55
batch start
#iterations: 312
currently lose_sum: 93.59775948524475
time_elpased: 1.56
batch start
#iterations: 313
currently lose_sum: 93.92436784505844
time_elpased: 1.546
batch start
#iterations: 314
currently lose_sum: 93.74762696027756
time_elpased: 1.524
batch start
#iterations: 315
currently lose_sum: 93.5427148938179
time_elpased: 1.598
batch start
#iterations: 316
currently lose_sum: 93.63494545221329
time_elpased: 1.539
batch start
#iterations: 317
currently lose_sum: 93.42055368423462
time_elpased: 1.561
batch start
#iterations: 318
currently lose_sum: 93.80816769599915
time_elpased: 1.559
batch start
#iterations: 319
currently lose_sum: 93.87544655799866
time_elpased: 1.599
start validation test
0.66412371134
0.641724962366
0.745806318823
0.68986197049
0.663980304924
60.522
batch start
#iterations: 320
currently lose_sum: 93.38703298568726
time_elpased: 1.564
batch start
#iterations: 321
currently lose_sum: 93.73497265577316
time_elpased: 1.52
batch start
#iterations: 322
currently lose_sum: 93.8867159485817
time_elpased: 1.553
batch start
#iterations: 323
currently lose_sum: 93.89426171779633
time_elpased: 1.64
batch start
#iterations: 324
currently lose_sum: 93.54494088888168
time_elpased: 1.542
batch start
#iterations: 325
currently lose_sum: 93.76821261644363
time_elpased: 1.604
batch start
#iterations: 326
currently lose_sum: 93.46121698617935
time_elpased: 1.603
batch start
#iterations: 327
currently lose_sum: 93.60526132583618
time_elpased: 1.539
batch start
#iterations: 328
currently lose_sum: 93.70654731988907
time_elpased: 1.529
batch start
#iterations: 329
currently lose_sum: 93.38428503274918
time_elpased: 1.551
batch start
#iterations: 330
currently lose_sum: 94.12642109394073
time_elpased: 1.58
batch start
#iterations: 331
currently lose_sum: 93.5740168094635
time_elpased: 1.582
batch start
#iterations: 332
currently lose_sum: 93.1693240404129
time_elpased: 1.562
batch start
#iterations: 333
currently lose_sum: 93.67363774776459
time_elpased: 1.569
batch start
#iterations: 334
currently lose_sum: 93.21377408504486
time_elpased: 1.556
batch start
#iterations: 335
currently lose_sum: 93.44943588972092
time_elpased: 1.546
batch start
#iterations: 336
currently lose_sum: 93.61060100793839
time_elpased: 1.587
batch start
#iterations: 337
currently lose_sum: 94.00979554653168
time_elpased: 1.604
batch start
#iterations: 338
currently lose_sum: 93.54738062620163
time_elpased: 1.586
batch start
#iterations: 339
currently lose_sum: 93.7858704328537
time_elpased: 1.589
start validation test
0.669845360825
0.637450199203
0.790367397345
0.70572019297
0.6696337658
60.329
batch start
#iterations: 340
currently lose_sum: 93.92513740062714
time_elpased: 1.587
batch start
#iterations: 341
currently lose_sum: 93.76348108053207
time_elpased: 1.593
batch start
#iterations: 342
currently lose_sum: 93.97780972719193
time_elpased: 1.561
batch start
#iterations: 343
currently lose_sum: 93.47741949558258
time_elpased: 1.61
batch start
#iterations: 344
currently lose_sum: 93.59531682729721
time_elpased: 1.537
batch start
#iterations: 345
currently lose_sum: 93.6760670542717
time_elpased: 1.584
batch start
#iterations: 346
currently lose_sum: 93.46196621656418
time_elpased: 1.569
batch start
#iterations: 347
currently lose_sum: 93.920863032341
time_elpased: 1.568
batch start
#iterations: 348
currently lose_sum: 93.61822736263275
time_elpased: 1.528
batch start
#iterations: 349
currently lose_sum: 94.2339985370636
time_elpased: 1.545
batch start
#iterations: 350
currently lose_sum: 93.4728536605835
time_elpased: 1.554
batch start
#iterations: 351
currently lose_sum: 93.70920032262802
time_elpased: 1.599
batch start
#iterations: 352
currently lose_sum: 93.56934201717377
time_elpased: 1.575
batch start
#iterations: 353
currently lose_sum: 93.35424995422363
time_elpased: 1.582
batch start
#iterations: 354
currently lose_sum: 93.76012629270554
time_elpased: 1.55
batch start
#iterations: 355
currently lose_sum: 93.49402970075607
time_elpased: 1.588
batch start
#iterations: 356
currently lose_sum: 94.01282978057861
time_elpased: 1.534
batch start
#iterations: 357
currently lose_sum: 93.71619188785553
time_elpased: 1.597
batch start
#iterations: 358
currently lose_sum: 93.44143122434616
time_elpased: 1.601
batch start
#iterations: 359
currently lose_sum: 93.48394882678986
time_elpased: 1.552
start validation test
0.666701030928
0.641114680094
0.760008232994
0.695517046525
0.666537215743
60.373
batch start
#iterations: 360
currently lose_sum: 93.78302174806595
time_elpased: 1.555
batch start
#iterations: 361
currently lose_sum: 93.92545127868652
time_elpased: 1.597
batch start
#iterations: 362
currently lose_sum: 93.62601226568222
time_elpased: 1.547
batch start
#iterations: 363
currently lose_sum: 93.30236053466797
time_elpased: 1.619
batch start
#iterations: 364
currently lose_sum: 93.4599484205246
time_elpased: 1.534
batch start
#iterations: 365
currently lose_sum: 94.10246628522873
time_elpased: 1.588
batch start
#iterations: 366
currently lose_sum: 93.56868094205856
time_elpased: 1.522
batch start
#iterations: 367
currently lose_sum: 93.63587856292725
time_elpased: 1.546
batch start
#iterations: 368
currently lose_sum: 93.66592508554459
time_elpased: 1.56
batch start
#iterations: 369
currently lose_sum: 93.98899668455124
time_elpased: 1.564
batch start
#iterations: 370
currently lose_sum: 93.47930580377579
time_elpased: 1.54
batch start
#iterations: 371
currently lose_sum: 93.4145519733429
time_elpased: 1.592
batch start
#iterations: 372
currently lose_sum: 93.03127747774124
time_elpased: 1.544
batch start
#iterations: 373
currently lose_sum: 93.9016894698143
time_elpased: 1.58
batch start
#iterations: 374
currently lose_sum: 93.90832036733627
time_elpased: 1.63
batch start
#iterations: 375
currently lose_sum: 93.05488866567612
time_elpased: 1.611
batch start
#iterations: 376
currently lose_sum: 94.0287514925003
time_elpased: 1.55
batch start
#iterations: 377
currently lose_sum: 93.7288293838501
time_elpased: 1.557
batch start
#iterations: 378
currently lose_sum: 93.62724125385284
time_elpased: 1.576
batch start
#iterations: 379
currently lose_sum: 93.59940123558044
time_elpased: 1.579
start validation test
0.670412371134
0.641247980957
0.776268395595
0.702327746741
0.670226524556
60.262
batch start
#iterations: 380
currently lose_sum: 93.91172844171524
time_elpased: 1.562
batch start
#iterations: 381
currently lose_sum: 93.41495072841644
time_elpased: 1.551
batch start
#iterations: 382
currently lose_sum: 93.90242183208466
time_elpased: 1.516
batch start
#iterations: 383
currently lose_sum: 93.80842262506485
time_elpased: 1.538
batch start
#iterations: 384
currently lose_sum: 93.98593991994858
time_elpased: 1.58
batch start
#iterations: 385
currently lose_sum: 93.69635713100433
time_elpased: 1.555
batch start
#iterations: 386
currently lose_sum: 93.54418194293976
time_elpased: 1.53
batch start
#iterations: 387
currently lose_sum: 94.15138691663742
time_elpased: 1.601
batch start
#iterations: 388
currently lose_sum: 94.01679927110672
time_elpased: 1.561
batch start
#iterations: 389
currently lose_sum: 93.78989320993423
time_elpased: 1.559
batch start
#iterations: 390
currently lose_sum: 93.86100554466248
time_elpased: 1.577
batch start
#iterations: 391
currently lose_sum: 93.75903850793839
time_elpased: 1.582
batch start
#iterations: 392
currently lose_sum: 93.52378517389297
time_elpased: 1.614
batch start
#iterations: 393
currently lose_sum: 93.53933668136597
time_elpased: 1.579
batch start
#iterations: 394
currently lose_sum: 93.6427435874939
time_elpased: 1.543
batch start
#iterations: 395
currently lose_sum: 93.46828806400299
time_elpased: 1.593
batch start
#iterations: 396
currently lose_sum: 93.31031900644302
time_elpased: 1.544
batch start
#iterations: 397
currently lose_sum: 93.7895987033844
time_elpased: 1.572
batch start
#iterations: 398
currently lose_sum: 93.31468838453293
time_elpased: 1.579
batch start
#iterations: 399
currently lose_sum: 93.8587709069252
time_elpased: 1.549
start validation test
0.665257731959
0.644180012526
0.740969435011
0.689193069781
0.665124808386
60.487
acc: 0.665
pre: 0.642
rec: 0.747
F1: 0.691
auc: 0.702
