start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.23236972093582
time_elpased: 1.442
batch start
#iterations: 1
currently lose_sum: 99.55503076314926
time_elpased: 1.419
batch start
#iterations: 2
currently lose_sum: 99.16960436105728
time_elpased: 1.498
batch start
#iterations: 3
currently lose_sum: 98.45006316900253
time_elpased: 1.417
batch start
#iterations: 4
currently lose_sum: 98.50227653980255
time_elpased: 1.447
batch start
#iterations: 5
currently lose_sum: 97.91994035243988
time_elpased: 1.476
batch start
#iterations: 6
currently lose_sum: 97.79939448833466
time_elpased: 1.476
batch start
#iterations: 7
currently lose_sum: 97.4027710556984
time_elpased: 1.446
batch start
#iterations: 8
currently lose_sum: 97.49164813756943
time_elpased: 1.427
batch start
#iterations: 9
currently lose_sum: 97.21693390607834
time_elpased: 1.5
batch start
#iterations: 10
currently lose_sum: 97.0842050909996
time_elpased: 1.468
batch start
#iterations: 11
currently lose_sum: 97.01943570375443
time_elpased: 1.413
batch start
#iterations: 12
currently lose_sum: 96.84174650907516
time_elpased: 1.491
batch start
#iterations: 13
currently lose_sum: 97.0610203742981
time_elpased: 1.461
batch start
#iterations: 14
currently lose_sum: 96.73015707731247
time_elpased: 1.42
batch start
#iterations: 15
currently lose_sum: 96.56062895059586
time_elpased: 1.438
batch start
#iterations: 16
currently lose_sum: 96.49375033378601
time_elpased: 1.421
batch start
#iterations: 17
currently lose_sum: 96.65619152784348
time_elpased: 1.422
batch start
#iterations: 18
currently lose_sum: 96.60765773057938
time_elpased: 1.416
batch start
#iterations: 19
currently lose_sum: 96.16078090667725
time_elpased: 1.427
start validation test
0.648041237113
0.644169329073
0.664059283656
0.653963105615
0.648014771939
62.178
batch start
#iterations: 20
currently lose_sum: 96.22216057777405
time_elpased: 1.433
batch start
#iterations: 21
currently lose_sum: 96.03778278827667
time_elpased: 1.408
batch start
#iterations: 22
currently lose_sum: 96.24635469913483
time_elpased: 1.427
batch start
#iterations: 23
currently lose_sum: 96.16128998994827
time_elpased: 1.395
batch start
#iterations: 24
currently lose_sum: 96.09634017944336
time_elpased: 1.486
batch start
#iterations: 25
currently lose_sum: 95.80303514003754
time_elpased: 1.414
batch start
#iterations: 26
currently lose_sum: 95.96326184272766
time_elpased: 1.455
batch start
#iterations: 27
currently lose_sum: 95.82213425636292
time_elpased: 1.462
batch start
#iterations: 28
currently lose_sum: 95.80463290214539
time_elpased: 1.41
batch start
#iterations: 29
currently lose_sum: 96.30190283060074
time_elpased: 1.432
batch start
#iterations: 30
currently lose_sum: 95.90551680326462
time_elpased: 1.435
batch start
#iterations: 31
currently lose_sum: 95.69534170627594
time_elpased: 1.514
batch start
#iterations: 32
currently lose_sum: 95.69030237197876
time_elpased: 1.431
batch start
#iterations: 33
currently lose_sum: 95.79426407814026
time_elpased: 1.508
batch start
#iterations: 34
currently lose_sum: 95.64243167638779
time_elpased: 1.424
batch start
#iterations: 35
currently lose_sum: 95.73431485891342
time_elpased: 1.432
batch start
#iterations: 36
currently lose_sum: 95.82243448495865
time_elpased: 1.452
batch start
#iterations: 37
currently lose_sum: 95.68752133846283
time_elpased: 1.43
batch start
#iterations: 38
currently lose_sum: 95.61197745800018
time_elpased: 1.454
batch start
#iterations: 39
currently lose_sum: 95.55265438556671
time_elpased: 1.519
start validation test
0.658144329897
0.64577424844
0.703067105805
0.673203902631
0.658070108045
61.543
batch start
#iterations: 40
currently lose_sum: 95.48211652040482
time_elpased: 1.437
batch start
#iterations: 41
currently lose_sum: 95.47476989030838
time_elpased: 1.461
batch start
#iterations: 42
currently lose_sum: 95.64059579372406
time_elpased: 1.418
batch start
#iterations: 43
currently lose_sum: 95.49152970314026
time_elpased: 1.408
batch start
#iterations: 44
currently lose_sum: 95.73256874084473
time_elpased: 1.504
batch start
#iterations: 45
currently lose_sum: 95.68290466070175
time_elpased: 1.533
batch start
#iterations: 46
currently lose_sum: 94.98546952009201
time_elpased: 1.407
batch start
#iterations: 47
currently lose_sum: 95.22526580095291
time_elpased: 1.498
batch start
#iterations: 48
currently lose_sum: 95.38223224878311
time_elpased: 1.404
batch start
#iterations: 49
currently lose_sum: 95.25919687747955
time_elpased: 1.418
batch start
#iterations: 50
currently lose_sum: 95.37733739614487
time_elpased: 1.446
batch start
#iterations: 51
currently lose_sum: 95.01521420478821
time_elpased: 1.412
batch start
#iterations: 52
currently lose_sum: 95.43143707513809
time_elpased: 1.427
batch start
#iterations: 53
currently lose_sum: 94.9149381518364
time_elpased: 1.414
batch start
#iterations: 54
currently lose_sum: 95.25219768285751
time_elpased: 1.428
batch start
#iterations: 55
currently lose_sum: 95.24661362171173
time_elpased: 1.426
batch start
#iterations: 56
currently lose_sum: 95.53715407848358
time_elpased: 1.469
batch start
#iterations: 57
currently lose_sum: 95.23771250247955
time_elpased: 1.424
batch start
#iterations: 58
currently lose_sum: 95.22990643978119
time_elpased: 1.531
batch start
#iterations: 59
currently lose_sum: 95.35189688205719
time_elpased: 1.427
start validation test
0.665618556701
0.646732709261
0.732400164677
0.686905738694
0.665508219472
61.020
batch start
#iterations: 60
currently lose_sum: 94.87233901023865
time_elpased: 1.499
batch start
#iterations: 61
currently lose_sum: 95.57359653711319
time_elpased: 1.442
batch start
#iterations: 62
currently lose_sum: 95.32364994287491
time_elpased: 1.433
batch start
#iterations: 63
currently lose_sum: 95.30068457126617
time_elpased: 1.454
batch start
#iterations: 64
currently lose_sum: 95.27895367145538
time_elpased: 1.456
batch start
#iterations: 65
currently lose_sum: 95.16612589359283
time_elpased: 1.44
batch start
#iterations: 66
currently lose_sum: 95.15145164728165
time_elpased: 1.439
batch start
#iterations: 67
currently lose_sum: 94.91689711809158
time_elpased: 1.406
batch start
#iterations: 68
currently lose_sum: 95.06539505720139
time_elpased: 1.42
batch start
#iterations: 69
currently lose_sum: 94.8353710770607
time_elpased: 1.427
batch start
#iterations: 70
currently lose_sum: 95.25181406736374
time_elpased: 1.488
batch start
#iterations: 71
currently lose_sum: 95.01663559675217
time_elpased: 1.474
batch start
#iterations: 72
currently lose_sum: 94.89292949438095
time_elpased: 1.422
batch start
#iterations: 73
currently lose_sum: 94.97573059797287
time_elpased: 1.407
batch start
#iterations: 74
currently lose_sum: 95.2246378660202
time_elpased: 1.413
batch start
#iterations: 75
currently lose_sum: 95.00952845811844
time_elpased: 1.501
batch start
#iterations: 76
currently lose_sum: 94.92623746395111
time_elpased: 1.448
batch start
#iterations: 77
currently lose_sum: 94.80134052038193
time_elpased: 1.444
batch start
#iterations: 78
currently lose_sum: 95.23534661531448
time_elpased: 1.431
batch start
#iterations: 79
currently lose_sum: 94.91789549589157
time_elpased: 1.457
start validation test
0.657783505155
0.646175771971
0.699979415397
0.672002371424
0.657713788657
61.001
batch start
#iterations: 80
currently lose_sum: 94.98229312896729
time_elpased: 1.429
batch start
#iterations: 81
currently lose_sum: 95.18080371618271
time_elpased: 1.474
batch start
#iterations: 82
currently lose_sum: 94.8847850561142
time_elpased: 1.43
batch start
#iterations: 83
currently lose_sum: 94.93518626689911
time_elpased: 1.472
batch start
#iterations: 84
currently lose_sum: 94.79755544662476
time_elpased: 1.428
batch start
#iterations: 85
currently lose_sum: 94.91623783111572
time_elpased: 1.445
batch start
#iterations: 86
currently lose_sum: 95.0409973859787
time_elpased: 1.46
batch start
#iterations: 87
currently lose_sum: 94.48535478115082
time_elpased: 1.492
batch start
#iterations: 88
currently lose_sum: 94.62960833311081
time_elpased: 1.457
batch start
#iterations: 89
currently lose_sum: 94.75227010250092
time_elpased: 1.479
batch start
#iterations: 90
currently lose_sum: 95.11851668357849
time_elpased: 1.517
batch start
#iterations: 91
currently lose_sum: 94.72598254680634
time_elpased: 1.434
batch start
#iterations: 92
currently lose_sum: 94.8019443154335
time_elpased: 1.46
batch start
#iterations: 93
currently lose_sum: 94.34208911657333
time_elpased: 1.467
batch start
#iterations: 94
currently lose_sum: 94.94698417186737
time_elpased: 1.436
batch start
#iterations: 95
currently lose_sum: 95.00363105535507
time_elpased: 1.446
batch start
#iterations: 96
currently lose_sum: 94.4213941693306
time_elpased: 1.413
batch start
#iterations: 97
currently lose_sum: 94.63948798179626
time_elpased: 1.428
batch start
#iterations: 98
currently lose_sum: 94.69550722837448
time_elpased: 1.419
batch start
#iterations: 99
currently lose_sum: 94.50029349327087
time_elpased: 1.486
start validation test
0.665618556701
0.635501468737
0.779333058872
0.70010632888
0.665430676483
60.781
batch start
#iterations: 100
currently lose_sum: 94.89763301610947
time_elpased: 1.431
batch start
#iterations: 101
currently lose_sum: 95.1493472456932
time_elpased: 1.453
batch start
#iterations: 102
currently lose_sum: 94.8243842124939
time_elpased: 1.514
batch start
#iterations: 103
currently lose_sum: 94.94425213336945
time_elpased: 1.45
batch start
#iterations: 104
currently lose_sum: 94.6142104268074
time_elpased: 1.412
batch start
#iterations: 105
currently lose_sum: 94.60297226905823
time_elpased: 1.475
batch start
#iterations: 106
currently lose_sum: 94.53010231256485
time_elpased: 1.521
batch start
#iterations: 107
currently lose_sum: 94.69988578557968
time_elpased: 1.434
batch start
#iterations: 108
currently lose_sum: 94.54764181375504
time_elpased: 1.539
batch start
#iterations: 109
currently lose_sum: 94.66183853149414
time_elpased: 1.471
batch start
#iterations: 110
currently lose_sum: 95.19983386993408
time_elpased: 1.441
batch start
#iterations: 111
currently lose_sum: 94.48189920186996
time_elpased: 1.513
batch start
#iterations: 112
currently lose_sum: 94.69537550210953
time_elpased: 1.408
batch start
#iterations: 113
currently lose_sum: 94.38944774866104
time_elpased: 1.414
batch start
#iterations: 114
currently lose_sum: 94.57906132936478
time_elpased: 1.459
batch start
#iterations: 115
currently lose_sum: 94.47123199701309
time_elpased: 1.413
batch start
#iterations: 116
currently lose_sum: 95.07609313726425
time_elpased: 1.459
batch start
#iterations: 117
currently lose_sum: 94.13983583450317
time_elpased: 1.457
batch start
#iterations: 118
currently lose_sum: 94.78934019804001
time_elpased: 1.433
batch start
#iterations: 119
currently lose_sum: 94.70221388339996
time_elpased: 1.477
start validation test
0.665257731959
0.63921534739
0.761321531494
0.694945509207
0.665099014405
60.990
batch start
#iterations: 120
currently lose_sum: 94.54756116867065
time_elpased: 1.456
batch start
#iterations: 121
currently lose_sum: 94.28142142295837
time_elpased: 1.489
batch start
#iterations: 122
currently lose_sum: 94.76913493871689
time_elpased: 1.482
batch start
#iterations: 123
currently lose_sum: 94.55453306436539
time_elpased: 1.431
batch start
#iterations: 124
currently lose_sum: 95.01467502117157
time_elpased: 1.464
batch start
#iterations: 125
currently lose_sum: 94.74322098493576
time_elpased: 1.438
batch start
#iterations: 126
currently lose_sum: 94.07427281141281
time_elpased: 1.439
batch start
#iterations: 127
currently lose_sum: 94.49542951583862
time_elpased: 1.533
batch start
#iterations: 128
currently lose_sum: 94.59051597118378
time_elpased: 1.476
batch start
#iterations: 129
currently lose_sum: 94.57308685779572
time_elpased: 1.515
batch start
#iterations: 130
currently lose_sum: 94.67936217784882
time_elpased: 1.397
batch start
#iterations: 131
currently lose_sum: 94.30730921030045
time_elpased: 1.428
batch start
#iterations: 132
currently lose_sum: 94.63241755962372
time_elpased: 1.46
batch start
#iterations: 133
currently lose_sum: 94.4806095957756
time_elpased: 1.489
batch start
#iterations: 134
currently lose_sum: 94.302903175354
time_elpased: 1.434
batch start
#iterations: 135
currently lose_sum: 94.65397554636002
time_elpased: 1.441
batch start
#iterations: 136
currently lose_sum: 94.714300096035
time_elpased: 1.529
batch start
#iterations: 137
currently lose_sum: 94.50312912464142
time_elpased: 1.534
batch start
#iterations: 138
currently lose_sum: 94.48433983325958
time_elpased: 1.413
batch start
#iterations: 139
currently lose_sum: 94.25292122364044
time_elpased: 1.466
start validation test
0.665360824742
0.641577375725
0.751852614245
0.692351435883
0.665217922157
60.566
batch start
#iterations: 140
currently lose_sum: 94.07472556829453
time_elpased: 1.432
batch start
#iterations: 141
currently lose_sum: 94.40432971715927
time_elpased: 1.507
batch start
#iterations: 142
currently lose_sum: 94.46959984302521
time_elpased: 1.419
batch start
#iterations: 143
currently lose_sum: 94.23091113567352
time_elpased: 1.452
batch start
#iterations: 144
currently lose_sum: 94.45888823270798
time_elpased: 1.452
batch start
#iterations: 145
currently lose_sum: 94.2003664970398
time_elpased: 1.448
batch start
#iterations: 146
currently lose_sum: 93.95581084489822
time_elpased: 1.435
batch start
#iterations: 147
currently lose_sum: 94.56308913230896
time_elpased: 1.432
batch start
#iterations: 148
currently lose_sum: 94.77811205387115
time_elpased: 1.452
batch start
#iterations: 149
currently lose_sum: 94.28693789243698
time_elpased: 1.443
batch start
#iterations: 150
currently lose_sum: 94.5674654841423
time_elpased: 1.451
batch start
#iterations: 151
currently lose_sum: 94.38356190919876
time_elpased: 1.398
batch start
#iterations: 152
currently lose_sum: 94.51117116212845
time_elpased: 1.418
batch start
#iterations: 153
currently lose_sum: 94.40263545513153
time_elpased: 1.443
batch start
#iterations: 154
currently lose_sum: 94.89259439706802
time_elpased: 1.409
batch start
#iterations: 155
currently lose_sum: 94.2389589548111
time_elpased: 1.442
batch start
#iterations: 156
currently lose_sum: 94.55688005685806
time_elpased: 1.433
batch start
#iterations: 157
currently lose_sum: 93.97888004779816
time_elpased: 1.446
batch start
#iterations: 158
currently lose_sum: 94.60109180212021
time_elpased: 1.412
batch start
#iterations: 159
currently lose_sum: 94.23189038038254
time_elpased: 1.472
start validation test
0.663298969072
0.637312402967
0.760498147386
0.693477240732
0.663138375634
60.709
batch start
#iterations: 160
currently lose_sum: 94.17151844501495
time_elpased: 1.459
batch start
#iterations: 161
currently lose_sum: 94.71414828300476
time_elpased: 1.439
batch start
#iterations: 162
currently lose_sum: 94.4411090016365
time_elpased: 1.436
batch start
#iterations: 163
currently lose_sum: 94.21668940782547
time_elpased: 1.405
batch start
#iterations: 164
currently lose_sum: 94.67174291610718
time_elpased: 1.446
batch start
#iterations: 165
currently lose_sum: 94.46025133132935
time_elpased: 1.448
batch start
#iterations: 166
currently lose_sum: 94.35909706354141
time_elpased: 1.439
batch start
#iterations: 167
currently lose_sum: 94.12844491004944
time_elpased: 1.467
batch start
#iterations: 168
currently lose_sum: 94.64022082090378
time_elpased: 1.413
batch start
#iterations: 169
currently lose_sum: 94.2799922823906
time_elpased: 1.437
batch start
#iterations: 170
currently lose_sum: 94.37727111577988
time_elpased: 1.42
batch start
#iterations: 171
currently lose_sum: 94.41249656677246
time_elpased: 1.442
batch start
#iterations: 172
currently lose_sum: 94.12991178035736
time_elpased: 1.5
batch start
#iterations: 173
currently lose_sum: 94.2877054810524
time_elpased: 1.49
batch start
#iterations: 174
currently lose_sum: 93.8697400689125
time_elpased: 1.447
batch start
#iterations: 175
currently lose_sum: 93.85556644201279
time_elpased: 1.508
batch start
#iterations: 176
currently lose_sum: 94.61451733112335
time_elpased: 1.441
batch start
#iterations: 177
currently lose_sum: 94.56798070669174
time_elpased: 1.451
batch start
#iterations: 178
currently lose_sum: 94.19918417930603
time_elpased: 1.436
batch start
#iterations: 179
currently lose_sum: 94.26156622171402
time_elpased: 1.417
start validation test
0.662835051546
0.641703115237
0.739913544669
0.687317749414
0.662707701702
60.741
batch start
#iterations: 180
currently lose_sum: 94.03352290391922
time_elpased: 1.474
batch start
#iterations: 181
currently lose_sum: 94.43578267097473
time_elpased: 1.404
batch start
#iterations: 182
currently lose_sum: 94.81431233882904
time_elpased: 1.426
batch start
#iterations: 183
currently lose_sum: 94.05861699581146
time_elpased: 1.445
batch start
#iterations: 184
currently lose_sum: 94.27042019367218
time_elpased: 1.424
batch start
#iterations: 185
currently lose_sum: 94.59830558300018
time_elpased: 1.509
batch start
#iterations: 186
currently lose_sum: 93.78766989707947
time_elpased: 1.407
batch start
#iterations: 187
currently lose_sum: 94.36242944002151
time_elpased: 1.462
batch start
#iterations: 188
currently lose_sum: 94.35924178361893
time_elpased: 1.419
batch start
#iterations: 189
currently lose_sum: 94.294085085392
time_elpased: 1.433
batch start
#iterations: 190
currently lose_sum: 94.45087540149689
time_elpased: 1.46
batch start
#iterations: 191
currently lose_sum: 94.1853340268135
time_elpased: 1.45
batch start
#iterations: 192
currently lose_sum: 94.4038695693016
time_elpased: 1.414
batch start
#iterations: 193
currently lose_sum: 94.06275671720505
time_elpased: 1.387
batch start
#iterations: 194
currently lose_sum: 94.03027200698853
time_elpased: 1.451
batch start
#iterations: 195
currently lose_sum: 94.21196031570435
time_elpased: 1.449
batch start
#iterations: 196
currently lose_sum: 93.83084398508072
time_elpased: 1.455
batch start
#iterations: 197
currently lose_sum: 94.12864083051682
time_elpased: 1.491
batch start
#iterations: 198
currently lose_sum: 94.17393136024475
time_elpased: 1.448
batch start
#iterations: 199
currently lose_sum: 93.83505940437317
time_elpased: 1.432
start validation test
0.668453608247
0.645955555556
0.747941539728
0.693217590384
0.668322277506
60.635
batch start
#iterations: 200
currently lose_sum: 94.38879203796387
time_elpased: 1.428
batch start
#iterations: 201
currently lose_sum: 93.82883566617966
time_elpased: 1.497
batch start
#iterations: 202
currently lose_sum: 94.3355079293251
time_elpased: 1.402
batch start
#iterations: 203
currently lose_sum: 94.59248894453049
time_elpased: 1.452
batch start
#iterations: 204
currently lose_sum: 94.3087608218193
time_elpased: 1.509
batch start
#iterations: 205
currently lose_sum: 94.72110629081726
time_elpased: 1.447
batch start
#iterations: 206
currently lose_sum: 93.87535119056702
time_elpased: 1.441
batch start
#iterations: 207
currently lose_sum: 94.00733441114426
time_elpased: 1.465
batch start
#iterations: 208
currently lose_sum: 94.13746690750122
time_elpased: 1.405
batch start
#iterations: 209
currently lose_sum: 94.30820524692535
time_elpased: 1.435
batch start
#iterations: 210
currently lose_sum: 94.02842372655869
time_elpased: 1.416
batch start
#iterations: 211
currently lose_sum: 93.39522612094879
time_elpased: 1.417
batch start
#iterations: 212
currently lose_sum: 94.2848511338234
time_elpased: 1.445
batch start
#iterations: 213
currently lose_sum: 94.31584125757217
time_elpased: 1.418
batch start
#iterations: 214
currently lose_sum: 94.36046487092972
time_elpased: 1.396
batch start
#iterations: 215
currently lose_sum: 93.98535591363907
time_elpased: 1.445
batch start
#iterations: 216
currently lose_sum: 93.61635744571686
time_elpased: 1.42
batch start
#iterations: 217
currently lose_sum: 93.9321191906929
time_elpased: 1.474
batch start
#iterations: 218
currently lose_sum: 94.39980918169022
time_elpased: 1.471
batch start
#iterations: 219
currently lose_sum: 94.1528360247612
time_elpased: 1.53
start validation test
0.669484536082
0.643777197563
0.761321531494
0.697632745449
0.669332802096
60.619
batch start
#iterations: 220
currently lose_sum: 94.35561376810074
time_elpased: 1.438
batch start
#iterations: 221
currently lose_sum: 94.03579533100128
time_elpased: 1.465
batch start
#iterations: 222
currently lose_sum: 94.08783632516861
time_elpased: 1.456
batch start
#iterations: 223
currently lose_sum: 93.41444420814514
time_elpased: 1.468
batch start
#iterations: 224
currently lose_sum: 93.99736493825912
time_elpased: 1.42
batch start
#iterations: 225
currently lose_sum: 93.8513034582138
time_elpased: 1.479
batch start
#iterations: 226
currently lose_sum: 93.84020054340363
time_elpased: 1.5
batch start
#iterations: 227
currently lose_sum: 93.98780959844589
time_elpased: 1.474
batch start
#iterations: 228
currently lose_sum: 93.91707664728165
time_elpased: 1.46
batch start
#iterations: 229
currently lose_sum: 93.73692220449448
time_elpased: 1.518
batch start
#iterations: 230
currently lose_sum: 93.99304735660553
time_elpased: 1.434
batch start
#iterations: 231
currently lose_sum: 94.2085509300232
time_elpased: 1.436
batch start
#iterations: 232
currently lose_sum: 94.3536326289177
time_elpased: 1.441
batch start
#iterations: 233
currently lose_sum: 94.20625191926956
time_elpased: 1.413
batch start
#iterations: 234
currently lose_sum: 94.18970447778702
time_elpased: 1.432
batch start
#iterations: 235
currently lose_sum: 94.33335947990417
time_elpased: 1.494
batch start
#iterations: 236
currently lose_sum: 94.06402152776718
time_elpased: 1.472
batch start
#iterations: 237
currently lose_sum: 93.7473286986351
time_elpased: 1.479
batch start
#iterations: 238
currently lose_sum: 94.07713550329208
time_elpased: 1.454
batch start
#iterations: 239
currently lose_sum: 94.39735335111618
time_elpased: 1.435
start validation test
0.66793814433
0.640708268867
0.767188143269
0.698266978923
0.667774162506
60.654
batch start
#iterations: 240
currently lose_sum: 94.04452139139175
time_elpased: 1.43
batch start
#iterations: 241
currently lose_sum: 93.88960754871368
time_elpased: 1.467
batch start
#iterations: 242
currently lose_sum: 93.85580706596375
time_elpased: 1.404
batch start
#iterations: 243
currently lose_sum: 93.97638291120529
time_elpased: 1.444
batch start
#iterations: 244
currently lose_sum: 93.86788529157639
time_elpased: 1.429
batch start
#iterations: 245
currently lose_sum: 94.2160176038742
time_elpased: 1.477
batch start
#iterations: 246
currently lose_sum: 93.7447999715805
time_elpased: 1.438
batch start
#iterations: 247
currently lose_sum: 93.98683547973633
time_elpased: 1.416
batch start
#iterations: 248
currently lose_sum: 94.20220655202866
time_elpased: 1.387
batch start
#iterations: 249
currently lose_sum: 93.97927439212799
time_elpased: 1.419
batch start
#iterations: 250
currently lose_sum: 94.31335526704788
time_elpased: 1.426
batch start
#iterations: 251
currently lose_sum: 93.81379491090775
time_elpased: 1.421
batch start
#iterations: 252
currently lose_sum: 94.22610360383987
time_elpased: 1.419
batch start
#iterations: 253
currently lose_sum: 94.00379246473312
time_elpased: 1.443
batch start
#iterations: 254
currently lose_sum: 93.92027878761292
time_elpased: 1.46
batch start
#iterations: 255
currently lose_sum: 93.99314194917679
time_elpased: 1.489
batch start
#iterations: 256
currently lose_sum: 93.7258449792862
time_elpased: 1.439
batch start
#iterations: 257
currently lose_sum: 93.65185499191284
time_elpased: 1.442
batch start
#iterations: 258
currently lose_sum: 94.09092974662781
time_elpased: 1.489
batch start
#iterations: 259
currently lose_sum: 93.96618902683258
time_elpased: 1.472
start validation test
0.665103092784
0.636873883834
0.770790448744
0.697462165308
0.664928475095
60.913
batch start
#iterations: 260
currently lose_sum: 93.95950824022293
time_elpased: 1.398
batch start
#iterations: 261
currently lose_sum: 93.87144720554352
time_elpased: 1.456
batch start
#iterations: 262
currently lose_sum: 93.59471070766449
time_elpased: 1.419
batch start
#iterations: 263
currently lose_sum: 93.76717519760132
time_elpased: 1.439
batch start
#iterations: 264
currently lose_sum: 94.08588600158691
time_elpased: 1.45
batch start
#iterations: 265
currently lose_sum: 94.3105937242508
time_elpased: 1.425
batch start
#iterations: 266
currently lose_sum: 93.59961420297623
time_elpased: 1.437
batch start
#iterations: 267
currently lose_sum: 94.21385782957077
time_elpased: 1.421
batch start
#iterations: 268
currently lose_sum: 93.96913945674896
time_elpased: 1.452
batch start
#iterations: 269
currently lose_sum: 94.30315089225769
time_elpased: 1.438
batch start
#iterations: 270
currently lose_sum: 93.71142733097076
time_elpased: 1.431
batch start
#iterations: 271
currently lose_sum: 93.94585198163986
time_elpased: 1.459
batch start
#iterations: 272
currently lose_sum: 93.92509204149246
time_elpased: 1.441
batch start
#iterations: 273
currently lose_sum: 93.91455811262131
time_elpased: 1.441
batch start
#iterations: 274
currently lose_sum: 93.64806765317917
time_elpased: 1.401
batch start
#iterations: 275
currently lose_sum: 94.05410140752792
time_elpased: 1.477
batch start
#iterations: 276
currently lose_sum: 93.88906401395798
time_elpased: 1.416
batch start
#iterations: 277
currently lose_sum: 93.58419042825699
time_elpased: 1.419
batch start
#iterations: 278
currently lose_sum: 94.13366043567657
time_elpased: 1.404
batch start
#iterations: 279
currently lose_sum: 94.0986088514328
time_elpased: 1.504
start validation test
0.667525773196
0.636561297876
0.783449979415
0.702408415613
0.667334242083
60.698
batch start
#iterations: 280
currently lose_sum: 93.68510937690735
time_elpased: 1.443
batch start
#iterations: 281
currently lose_sum: 93.55161201953888
time_elpased: 1.457
batch start
#iterations: 282
currently lose_sum: 93.89442992210388
time_elpased: 1.423
batch start
#iterations: 283
currently lose_sum: 94.21246790885925
time_elpased: 1.438
batch start
#iterations: 284
currently lose_sum: 93.81192654371262
time_elpased: 1.441
batch start
#iterations: 285
currently lose_sum: 93.5095100402832
time_elpased: 1.43
batch start
#iterations: 286
currently lose_sum: 93.37336039543152
time_elpased: 1.455
batch start
#iterations: 287
currently lose_sum: 93.65386831760406
time_elpased: 1.456
batch start
#iterations: 288
currently lose_sum: 93.95861405134201
time_elpased: 1.407
batch start
#iterations: 289
currently lose_sum: 93.99480295181274
time_elpased: 1.427
batch start
#iterations: 290
currently lose_sum: 93.94385194778442
time_elpased: 1.415
batch start
#iterations: 291
currently lose_sum: 93.78654485940933
time_elpased: 1.437
batch start
#iterations: 292
currently lose_sum: 93.74528539180756
time_elpased: 1.448
batch start
#iterations: 293
currently lose_sum: 93.77445328235626
time_elpased: 1.569
batch start
#iterations: 294
currently lose_sum: 93.998166680336
time_elpased: 1.41
batch start
#iterations: 295
currently lose_sum: 94.00329023599625
time_elpased: 1.447
batch start
#iterations: 296
currently lose_sum: 93.83964627981186
time_elpased: 1.479
batch start
#iterations: 297
currently lose_sum: 94.11655575037003
time_elpased: 1.466
batch start
#iterations: 298
currently lose_sum: 93.73733842372894
time_elpased: 1.447
batch start
#iterations: 299
currently lose_sum: 93.8204156756401
time_elpased: 1.433
start validation test
0.667989690722
0.641860868059
0.762556607657
0.697022437556
0.667833446332
60.872
batch start
#iterations: 300
currently lose_sum: 94.16097575426102
time_elpased: 1.423
batch start
#iterations: 301
currently lose_sum: 93.66888529062271
time_elpased: 1.422
batch start
#iterations: 302
currently lose_sum: 93.59144848585129
time_elpased: 1.415
batch start
#iterations: 303
currently lose_sum: 93.45147228240967
time_elpased: 1.467
batch start
#iterations: 304
currently lose_sum: 93.17183697223663
time_elpased: 1.483
batch start
#iterations: 305
currently lose_sum: 93.58656650781631
time_elpased: 1.476
batch start
#iterations: 306
currently lose_sum: 93.91164880990982
time_elpased: 1.438
batch start
#iterations: 307
currently lose_sum: 94.11753898859024
time_elpased: 1.424
batch start
#iterations: 308
currently lose_sum: 93.64558935165405
time_elpased: 1.437
batch start
#iterations: 309
currently lose_sum: 93.48156094551086
time_elpased: 1.422
batch start
#iterations: 310
currently lose_sum: 93.82662463188171
time_elpased: 1.457
batch start
#iterations: 311
currently lose_sum: 93.51974946260452
time_elpased: 1.521
batch start
#iterations: 312
currently lose_sum: 93.67968320846558
time_elpased: 1.423
batch start
#iterations: 313
currently lose_sum: 93.9356558918953
time_elpased: 1.48
batch start
#iterations: 314
currently lose_sum: 93.8689836859703
time_elpased: 1.419
batch start
#iterations: 315
currently lose_sum: 93.29350912570953
time_elpased: 1.477
batch start
#iterations: 316
currently lose_sum: 93.75543904304504
time_elpased: 1.431
batch start
#iterations: 317
currently lose_sum: 93.70481485128403
time_elpased: 1.408
batch start
#iterations: 318
currently lose_sum: 93.7434389591217
time_elpased: 1.441
batch start
#iterations: 319
currently lose_sum: 93.9653291106224
time_elpased: 1.459
start validation test
0.667010309278
0.640175650077
0.765232606011
0.697140178153
0.666848025434
60.794
batch start
#iterations: 320
currently lose_sum: 94.09010165929794
time_elpased: 1.397
batch start
#iterations: 321
currently lose_sum: 93.89148372411728
time_elpased: 1.479
batch start
#iterations: 322
currently lose_sum: 93.60504406690598
time_elpased: 1.411
batch start
#iterations: 323
currently lose_sum: 93.66135424375534
time_elpased: 1.428
batch start
#iterations: 324
currently lose_sum: 93.60282200574875
time_elpased: 1.442
batch start
#iterations: 325
currently lose_sum: 93.66790688037872
time_elpased: 1.515
batch start
#iterations: 326
currently lose_sum: 93.19993591308594
time_elpased: 1.493
batch start
#iterations: 327
currently lose_sum: 93.90203666687012
time_elpased: 1.435
batch start
#iterations: 328
currently lose_sum: 93.76683926582336
time_elpased: 1.434
batch start
#iterations: 329
currently lose_sum: 93.88305616378784
time_elpased: 1.435
batch start
#iterations: 330
currently lose_sum: 93.61144459247589
time_elpased: 1.425
batch start
#iterations: 331
currently lose_sum: 93.90423500537872
time_elpased: 1.469
batch start
#iterations: 332
currently lose_sum: 93.92825925350189
time_elpased: 1.475
batch start
#iterations: 333
currently lose_sum: 93.83719652891159
time_elpased: 1.42
batch start
#iterations: 334
currently lose_sum: 93.6960016489029
time_elpased: 1.493
batch start
#iterations: 335
currently lose_sum: 93.25267976522446
time_elpased: 1.47
batch start
#iterations: 336
currently lose_sum: 93.69579130411148
time_elpased: 1.42
batch start
#iterations: 337
currently lose_sum: 93.46080350875854
time_elpased: 1.45
batch start
#iterations: 338
currently lose_sum: 93.19497787952423
time_elpased: 1.399
batch start
#iterations: 339
currently lose_sum: 93.8549507856369
time_elpased: 1.427
start validation test
0.666597938144
0.641119221411
0.759365994236
0.695250659631
0.66644466585
60.795
batch start
#iterations: 340
currently lose_sum: 94.04614001512527
time_elpased: 1.446
batch start
#iterations: 341
currently lose_sum: 93.56303012371063
time_elpased: 1.463
batch start
#iterations: 342
currently lose_sum: 93.80290067195892
time_elpased: 1.419
batch start
#iterations: 343
currently lose_sum: 94.12098222970963
time_elpased: 1.459
batch start
#iterations: 344
currently lose_sum: 93.90832489728928
time_elpased: 1.454
batch start
#iterations: 345
currently lose_sum: 93.86991959810257
time_elpased: 1.466
batch start
#iterations: 346
currently lose_sum: 93.52346134185791
time_elpased: 1.423
batch start
#iterations: 347
currently lose_sum: 93.68493038415909
time_elpased: 1.42
batch start
#iterations: 348
currently lose_sum: 93.54738837480545
time_elpased: 1.42
batch start
#iterations: 349
currently lose_sum: 94.0556578040123
time_elpased: 1.519
batch start
#iterations: 350
currently lose_sum: 93.81129115819931
time_elpased: 1.425
batch start
#iterations: 351
currently lose_sum: 93.80184584856033
time_elpased: 1.466
batch start
#iterations: 352
currently lose_sum: 93.61674410104752
time_elpased: 1.439
batch start
#iterations: 353
currently lose_sum: 94.13913768529892
time_elpased: 1.501
batch start
#iterations: 354
currently lose_sum: 93.79963421821594
time_elpased: 1.422
batch start
#iterations: 355
currently lose_sum: 93.55158215761185
time_elpased: 1.468
batch start
#iterations: 356
currently lose_sum: 93.5842159986496
time_elpased: 1.495
batch start
#iterations: 357
currently lose_sum: 93.92566865682602
time_elpased: 1.49
batch start
#iterations: 358
currently lose_sum: 93.8002341389656
time_elpased: 1.405
batch start
#iterations: 359
currently lose_sum: 93.19341999292374
time_elpased: 1.42
start validation test
0.669845360825
0.640284721634
0.777686290655
0.702328391504
0.66966718498
60.917
batch start
#iterations: 360
currently lose_sum: 93.82628053426743
time_elpased: 1.402
batch start
#iterations: 361
currently lose_sum: 93.82594114542007
time_elpased: 1.462
batch start
#iterations: 362
currently lose_sum: 93.92715674638748
time_elpased: 1.434
batch start
#iterations: 363
currently lose_sum: 93.84196072816849
time_elpased: 1.418
batch start
#iterations: 364
currently lose_sum: 93.54517251253128
time_elpased: 1.437
batch start
#iterations: 365
currently lose_sum: 93.88069933652878
time_elpased: 1.454
batch start
#iterations: 366
currently lose_sum: 94.02711308002472
time_elpased: 1.458
batch start
#iterations: 367
currently lose_sum: 93.72018766403198
time_elpased: 1.498
batch start
#iterations: 368
currently lose_sum: 93.8705330491066
time_elpased: 1.459
batch start
#iterations: 369
currently lose_sum: 93.91171264648438
time_elpased: 1.456
batch start
#iterations: 370
currently lose_sum: 93.79422920942307
time_elpased: 1.487
batch start
#iterations: 371
currently lose_sum: 93.62202966213226
time_elpased: 1.434
batch start
#iterations: 372
currently lose_sum: 93.43216443061829
time_elpased: 1.411
batch start
#iterations: 373
currently lose_sum: 93.87039840221405
time_elpased: 1.495
batch start
#iterations: 374
currently lose_sum: 93.73716700077057
time_elpased: 1.481
batch start
#iterations: 375
currently lose_sum: 93.70014017820358
time_elpased: 1.433
batch start
#iterations: 376
currently lose_sum: 92.99399161338806
time_elpased: 1.405
batch start
#iterations: 377
currently lose_sum: 93.29760730266571
time_elpased: 1.492
batch start
#iterations: 378
currently lose_sum: 93.9353899359703
time_elpased: 1.427
batch start
#iterations: 379
currently lose_sum: 93.57488721609116
time_elpased: 1.423
start validation test
0.66412371134
0.638504155125
0.759160148209
0.693624224187
0.663966691205
60.897
batch start
#iterations: 380
currently lose_sum: 93.63983011245728
time_elpased: 1.468
batch start
#iterations: 381
currently lose_sum: 93.5364499092102
time_elpased: 1.459
batch start
#iterations: 382
currently lose_sum: 93.73746192455292
time_elpased: 1.435
batch start
#iterations: 383
currently lose_sum: 93.66312485933304
time_elpased: 1.442
batch start
#iterations: 384
currently lose_sum: 93.44108021259308
time_elpased: 1.422
batch start
#iterations: 385
currently lose_sum: 93.8016921877861
time_elpased: 1.478
batch start
#iterations: 386
currently lose_sum: 93.60650968551636
time_elpased: 1.428
batch start
#iterations: 387
currently lose_sum: 93.41233444213867
time_elpased: 1.473
batch start
#iterations: 388
currently lose_sum: 93.77438735961914
time_elpased: 1.452
batch start
#iterations: 389
currently lose_sum: 93.56847935914993
time_elpased: 1.503
batch start
#iterations: 390
currently lose_sum: 93.76196956634521
time_elpased: 1.427
batch start
#iterations: 391
currently lose_sum: 93.20602667331696
time_elpased: 1.473
batch start
#iterations: 392
currently lose_sum: 93.85518902540207
time_elpased: 1.418
batch start
#iterations: 393
currently lose_sum: 93.42189198732376
time_elpased: 1.479
batch start
#iterations: 394
currently lose_sum: 93.2572112083435
time_elpased: 1.467
batch start
#iterations: 395
currently lose_sum: 93.51827329397202
time_elpased: 1.503
batch start
#iterations: 396
currently lose_sum: 93.65577507019043
time_elpased: 1.441
batch start
#iterations: 397
currently lose_sum: 93.72443771362305
time_elpased: 1.497
batch start
#iterations: 398
currently lose_sum: 93.8723658323288
time_elpased: 1.412
batch start
#iterations: 399
currently lose_sum: 93.60921031236649
time_elpased: 1.437
start validation test
0.663350515464
0.640817048369
0.745883079457
0.68936979786
0.66321415435
61.056
acc: 0.669
pre: 0.643
rec: 0.763
F1: 0.698
auc: 0.707
