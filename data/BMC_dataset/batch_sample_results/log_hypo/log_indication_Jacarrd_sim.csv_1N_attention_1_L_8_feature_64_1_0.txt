start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.08700102567673
time_elpased: 1.58
batch start
#iterations: 1
currently lose_sum: 99.32872289419174
time_elpased: 1.613
batch start
#iterations: 2
currently lose_sum: 99.21081179380417
time_elpased: 1.576
batch start
#iterations: 3
currently lose_sum: 98.70538187026978
time_elpased: 1.561
batch start
#iterations: 4
currently lose_sum: 98.5418210029602
time_elpased: 1.558
batch start
#iterations: 5
currently lose_sum: 98.14337372779846
time_elpased: 1.59
batch start
#iterations: 6
currently lose_sum: 97.95507764816284
time_elpased: 1.571
batch start
#iterations: 7
currently lose_sum: 97.74250727891922
time_elpased: 1.558
batch start
#iterations: 8
currently lose_sum: 97.68289339542389
time_elpased: 1.596
batch start
#iterations: 9
currently lose_sum: 97.38253170251846
time_elpased: 1.581
batch start
#iterations: 10
currently lose_sum: 97.14625334739685
time_elpased: 1.566
batch start
#iterations: 11
currently lose_sum: 97.37672156095505
time_elpased: 1.581
batch start
#iterations: 12
currently lose_sum: 97.34917509555817
time_elpased: 1.572
batch start
#iterations: 13
currently lose_sum: 96.72373467683792
time_elpased: 1.554
batch start
#iterations: 14
currently lose_sum: 96.84110325574875
time_elpased: 1.576
batch start
#iterations: 15
currently lose_sum: 96.89907962083817
time_elpased: 1.538
batch start
#iterations: 16
currently lose_sum: 96.61801862716675
time_elpased: 1.598
batch start
#iterations: 17
currently lose_sum: 96.5713951587677
time_elpased: 1.602
batch start
#iterations: 18
currently lose_sum: 96.49881553649902
time_elpased: 1.555
batch start
#iterations: 19
currently lose_sum: 96.30874317884445
time_elpased: 1.549
start validation test
0.654329896907
0.641614147305
0.701965627251
0.670434440731
0.654246265035
61.767
batch start
#iterations: 20
currently lose_sum: 96.39502185583115
time_elpased: 1.578
batch start
#iterations: 21
currently lose_sum: 96.09006470441818
time_elpased: 1.574
batch start
#iterations: 22
currently lose_sum: 96.05399018526077
time_elpased: 1.598
batch start
#iterations: 23
currently lose_sum: 96.15856063365936
time_elpased: 1.587
batch start
#iterations: 24
currently lose_sum: 95.91474026441574
time_elpased: 1.586
batch start
#iterations: 25
currently lose_sum: 95.98346889019012
time_elpased: 1.597
batch start
#iterations: 26
currently lose_sum: 95.69063723087311
time_elpased: 1.563
batch start
#iterations: 27
currently lose_sum: 95.64472359418869
time_elpased: 1.577
batch start
#iterations: 28
currently lose_sum: 95.88873690366745
time_elpased: 1.585
batch start
#iterations: 29
currently lose_sum: 95.61305075883865
time_elpased: 1.598
batch start
#iterations: 30
currently lose_sum: 95.77713799476624
time_elpased: 1.551
batch start
#iterations: 31
currently lose_sum: 95.34513866901398
time_elpased: 1.574
batch start
#iterations: 32
currently lose_sum: 95.40562796592712
time_elpased: 1.574
batch start
#iterations: 33
currently lose_sum: 95.9015479683876
time_elpased: 1.597
batch start
#iterations: 34
currently lose_sum: 95.71968710422516
time_elpased: 1.587
batch start
#iterations: 35
currently lose_sum: 95.6482008099556
time_elpased: 1.616
batch start
#iterations: 36
currently lose_sum: 95.83912241458893
time_elpased: 1.641
batch start
#iterations: 37
currently lose_sum: 95.48628944158554
time_elpased: 1.595
batch start
#iterations: 38
currently lose_sum: 95.45923513174057
time_elpased: 1.57
batch start
#iterations: 39
currently lose_sum: 95.64968287944794
time_elpased: 1.567
start validation test
0.652886597938
0.642577191473
0.69177729752
0.666270195262
0.652818319317
61.348
batch start
#iterations: 40
currently lose_sum: 95.46569925546646
time_elpased: 1.577
batch start
#iterations: 41
currently lose_sum: 95.23274886608124
time_elpased: 1.644
batch start
#iterations: 42
currently lose_sum: 95.24228912591934
time_elpased: 1.569
batch start
#iterations: 43
currently lose_sum: 95.31262117624283
time_elpased: 1.575
batch start
#iterations: 44
currently lose_sum: 95.22051048278809
time_elpased: 1.569
batch start
#iterations: 45
currently lose_sum: 95.01044476032257
time_elpased: 1.576
batch start
#iterations: 46
currently lose_sum: 94.95778489112854
time_elpased: 1.584
batch start
#iterations: 47
currently lose_sum: 94.94316864013672
time_elpased: 1.586
batch start
#iterations: 48
currently lose_sum: 95.3038871884346
time_elpased: 1.606
batch start
#iterations: 49
currently lose_sum: 95.4646270275116
time_elpased: 1.625
batch start
#iterations: 50
currently lose_sum: 94.9730007648468
time_elpased: 1.606
batch start
#iterations: 51
currently lose_sum: 94.9916883111
time_elpased: 1.576
batch start
#iterations: 52
currently lose_sum: 94.86693197488785
time_elpased: 1.596
batch start
#iterations: 53
currently lose_sum: 95.05042064189911
time_elpased: 1.597
batch start
#iterations: 54
currently lose_sum: 95.19161415100098
time_elpased: 1.614
batch start
#iterations: 55
currently lose_sum: 95.24679327011108
time_elpased: 1.565
batch start
#iterations: 56
currently lose_sum: 95.05516141653061
time_elpased: 1.609
batch start
#iterations: 57
currently lose_sum: 95.04603666067123
time_elpased: 1.614
batch start
#iterations: 58
currently lose_sum: 95.34002131223679
time_elpased: 1.599
batch start
#iterations: 59
currently lose_sum: 95.19991534948349
time_elpased: 1.594
start validation test
0.661494845361
0.639528703047
0.74292477102
0.687360152345
0.661351882567
61.007
batch start
#iterations: 60
currently lose_sum: 95.02171421051025
time_elpased: 1.614
batch start
#iterations: 61
currently lose_sum: 95.27624922990799
time_elpased: 1.578
batch start
#iterations: 62
currently lose_sum: 95.07086235284805
time_elpased: 1.633
batch start
#iterations: 63
currently lose_sum: 95.18003672361374
time_elpased: 1.584
batch start
#iterations: 64
currently lose_sum: 95.31772792339325
time_elpased: 1.589
batch start
#iterations: 65
currently lose_sum: 94.57304209470749
time_elpased: 1.65
batch start
#iterations: 66
currently lose_sum: 94.31910157203674
time_elpased: 1.597
batch start
#iterations: 67
currently lose_sum: 94.87614053487778
time_elpased: 1.567
batch start
#iterations: 68
currently lose_sum: 95.34845799207687
time_elpased: 1.606
batch start
#iterations: 69
currently lose_sum: 94.74236422777176
time_elpased: 1.588
batch start
#iterations: 70
currently lose_sum: 94.79531639814377
time_elpased: 1.558
batch start
#iterations: 71
currently lose_sum: 94.80480527877808
time_elpased: 1.562
batch start
#iterations: 72
currently lose_sum: 94.11495590209961
time_elpased: 1.611
batch start
#iterations: 73
currently lose_sum: 94.1884298324585
time_elpased: 1.614
batch start
#iterations: 74
currently lose_sum: 94.5930460691452
time_elpased: 1.562
batch start
#iterations: 75
currently lose_sum: 94.63206332921982
time_elpased: 1.593
batch start
#iterations: 76
currently lose_sum: 94.46418982744217
time_elpased: 1.578
batch start
#iterations: 77
currently lose_sum: 94.76372301578522
time_elpased: 1.592
batch start
#iterations: 78
currently lose_sum: 94.31926047801971
time_elpased: 1.576
batch start
#iterations: 79
currently lose_sum: 94.49949967861176
time_elpased: 1.587
start validation test
0.662783505155
0.632038592697
0.782031491201
0.699080036799
0.662574146922
60.811
batch start
#iterations: 80
currently lose_sum: 94.90728241205215
time_elpased: 1.631
batch start
#iterations: 81
currently lose_sum: 94.20967930555344
time_elpased: 1.589
batch start
#iterations: 82
currently lose_sum: 94.78625041246414
time_elpased: 1.588
batch start
#iterations: 83
currently lose_sum: 94.92765909433365
time_elpased: 1.574
batch start
#iterations: 84
currently lose_sum: 94.89552223682404
time_elpased: 1.593
batch start
#iterations: 85
currently lose_sum: 94.34769690036774
time_elpased: 1.582
batch start
#iterations: 86
currently lose_sum: 94.88643568754196
time_elpased: 1.6
batch start
#iterations: 87
currently lose_sum: 94.20467531681061
time_elpased: 1.59
batch start
#iterations: 88
currently lose_sum: 94.77062368392944
time_elpased: 1.562
batch start
#iterations: 89
currently lose_sum: 94.95199984312057
time_elpased: 1.596
batch start
#iterations: 90
currently lose_sum: 94.73150998353958
time_elpased: 1.589
batch start
#iterations: 91
currently lose_sum: 94.1240182518959
time_elpased: 1.566
batch start
#iterations: 92
currently lose_sum: 94.32510244846344
time_elpased: 1.577
batch start
#iterations: 93
currently lose_sum: 94.38542991876602
time_elpased: 1.6
batch start
#iterations: 94
currently lose_sum: 94.70549362897873
time_elpased: 1.587
batch start
#iterations: 95
currently lose_sum: 94.44351381063461
time_elpased: 1.572
batch start
#iterations: 96
currently lose_sum: 94.82610589265823
time_elpased: 1.594
batch start
#iterations: 97
currently lose_sum: 94.305694937706
time_elpased: 1.601
batch start
#iterations: 98
currently lose_sum: 94.49897235631943
time_elpased: 1.645
batch start
#iterations: 99
currently lose_sum: 94.49516755342484
time_elpased: 1.552
start validation test
0.66412371134
0.632021776788
0.788514973757
0.701648351648
0.663905323293
60.674
batch start
#iterations: 100
currently lose_sum: 94.66584873199463
time_elpased: 1.593
batch start
#iterations: 101
currently lose_sum: 94.55494409799576
time_elpased: 1.592
batch start
#iterations: 102
currently lose_sum: 94.47197300195694
time_elpased: 1.57
batch start
#iterations: 103
currently lose_sum: 94.12496411800385
time_elpased: 1.579
batch start
#iterations: 104
currently lose_sum: 93.90401071310043
time_elpased: 1.545
batch start
#iterations: 105
currently lose_sum: 94.33445137739182
time_elpased: 1.556
batch start
#iterations: 106
currently lose_sum: 94.78266662359238
time_elpased: 1.596
batch start
#iterations: 107
currently lose_sum: 94.16401302814484
time_elpased: 1.584
batch start
#iterations: 108
currently lose_sum: 94.0030791759491
time_elpased: 1.65
batch start
#iterations: 109
currently lose_sum: 94.48302751779556
time_elpased: 1.604
batch start
#iterations: 110
currently lose_sum: 94.34338480234146
time_elpased: 1.586
batch start
#iterations: 111
currently lose_sum: 94.32531958818436
time_elpased: 1.594
batch start
#iterations: 112
currently lose_sum: 94.29484385251999
time_elpased: 1.61
batch start
#iterations: 113
currently lose_sum: 94.19546693563461
time_elpased: 1.64
batch start
#iterations: 114
currently lose_sum: 94.60331183671951
time_elpased: 1.568
batch start
#iterations: 115
currently lose_sum: 94.33953535556793
time_elpased: 1.57
batch start
#iterations: 116
currently lose_sum: 94.26996856927872
time_elpased: 1.607
batch start
#iterations: 117
currently lose_sum: 94.3993444442749
time_elpased: 1.587
batch start
#iterations: 118
currently lose_sum: 94.54203325510025
time_elpased: 1.601
batch start
#iterations: 119
currently lose_sum: 94.4918572306633
time_elpased: 1.611
start validation test
0.661288659794
0.648396226415
0.707317073171
0.676576266181
0.661207849815
60.725
batch start
#iterations: 120
currently lose_sum: 94.51164066791534
time_elpased: 1.572
batch start
#iterations: 121
currently lose_sum: 94.550925552845
time_elpased: 1.589
batch start
#iterations: 122
currently lose_sum: 94.38814973831177
time_elpased: 1.55
batch start
#iterations: 123
currently lose_sum: 94.63185960054398
time_elpased: 1.547
batch start
#iterations: 124
currently lose_sum: 94.73913717269897
time_elpased: 1.585
batch start
#iterations: 125
currently lose_sum: 94.34223365783691
time_elpased: 1.615
batch start
#iterations: 126
currently lose_sum: 94.34406751394272
time_elpased: 1.573
batch start
#iterations: 127
currently lose_sum: 94.48294651508331
time_elpased: 1.551
batch start
#iterations: 128
currently lose_sum: 94.64651554822922
time_elpased: 1.554
batch start
#iterations: 129
currently lose_sum: 94.30520343780518
time_elpased: 1.601
batch start
#iterations: 130
currently lose_sum: 93.81886631250381
time_elpased: 1.565
batch start
#iterations: 131
currently lose_sum: 94.64936155080795
time_elpased: 1.578
batch start
#iterations: 132
currently lose_sum: 94.39470762014389
time_elpased: 1.603
batch start
#iterations: 133
currently lose_sum: 94.26810783147812
time_elpased: 1.588
batch start
#iterations: 134
currently lose_sum: 94.24231320619583
time_elpased: 1.582
batch start
#iterations: 135
currently lose_sum: 94.55428546667099
time_elpased: 1.545
batch start
#iterations: 136
currently lose_sum: 94.18319481611252
time_elpased: 1.609
batch start
#iterations: 137
currently lose_sum: 93.88685315847397
time_elpased: 1.585
batch start
#iterations: 138
currently lose_sum: 94.40402007102966
time_elpased: 1.573
batch start
#iterations: 139
currently lose_sum: 94.48315471410751
time_elpased: 1.579
start validation test
0.670257731959
0.644172311968
0.763301430483
0.698695304037
0.670094379395
60.598
batch start
#iterations: 140
currently lose_sum: 94.32580530643463
time_elpased: 1.576
batch start
#iterations: 141
currently lose_sum: 94.33574604988098
time_elpased: 1.578
batch start
#iterations: 142
currently lose_sum: 94.18506622314453
time_elpased: 1.599
batch start
#iterations: 143
currently lose_sum: 94.10266917943954
time_elpased: 1.604
batch start
#iterations: 144
currently lose_sum: 94.41659551858902
time_elpased: 1.588
batch start
#iterations: 145
currently lose_sum: 94.16179740428925
time_elpased: 1.575
batch start
#iterations: 146
currently lose_sum: 94.14263463020325
time_elpased: 1.555
batch start
#iterations: 147
currently lose_sum: 94.23074686527252
time_elpased: 1.57
batch start
#iterations: 148
currently lose_sum: 94.0789178609848
time_elpased: 1.61
batch start
#iterations: 149
currently lose_sum: 94.30889362096786
time_elpased: 1.56
batch start
#iterations: 150
currently lose_sum: 94.51051133871078
time_elpased: 1.609
batch start
#iterations: 151
currently lose_sum: 94.1302410364151
time_elpased: 1.588
batch start
#iterations: 152
currently lose_sum: 94.39581698179245
time_elpased: 1.573
batch start
#iterations: 153
currently lose_sum: 94.32745933532715
time_elpased: 1.594
batch start
#iterations: 154
currently lose_sum: 94.51443189382553
time_elpased: 1.632
batch start
#iterations: 155
currently lose_sum: 94.29216569662094
time_elpased: 1.55
batch start
#iterations: 156
currently lose_sum: 94.16999042034149
time_elpased: 1.586
batch start
#iterations: 157
currently lose_sum: 94.23393321037292
time_elpased: 1.571
batch start
#iterations: 158
currently lose_sum: 93.64174741506577
time_elpased: 1.599
batch start
#iterations: 159
currently lose_sum: 94.37967610359192
time_elpased: 1.584
start validation test
0.665463917526
0.638272345531
0.76649171555
0.696530440475
0.665286547644
60.480
batch start
#iterations: 160
currently lose_sum: 94.30669397115707
time_elpased: 1.558
batch start
#iterations: 161
currently lose_sum: 94.12228620052338
time_elpased: 1.576
batch start
#iterations: 162
currently lose_sum: 94.03383308649063
time_elpased: 1.589
batch start
#iterations: 163
currently lose_sum: 94.2214366197586
time_elpased: 1.599
batch start
#iterations: 164
currently lose_sum: 94.20056819915771
time_elpased: 1.596
batch start
#iterations: 165
currently lose_sum: 94.21648120880127
time_elpased: 1.611
batch start
#iterations: 166
currently lose_sum: 94.37928539514542
time_elpased: 1.616
batch start
#iterations: 167
currently lose_sum: 94.00340700149536
time_elpased: 1.576
batch start
#iterations: 168
currently lose_sum: 93.99692487716675
time_elpased: 1.615
batch start
#iterations: 169
currently lose_sum: 94.11002832651138
time_elpased: 1.602
batch start
#iterations: 170
currently lose_sum: 94.2771560549736
time_elpased: 1.596
batch start
#iterations: 171
currently lose_sum: 93.87935549020767
time_elpased: 1.659
batch start
#iterations: 172
currently lose_sum: 93.87732595205307
time_elpased: 1.604
batch start
#iterations: 173
currently lose_sum: 93.90634721517563
time_elpased: 1.664
batch start
#iterations: 174
currently lose_sum: 94.3272625207901
time_elpased: 1.601
batch start
#iterations: 175
currently lose_sum: 93.86910283565521
time_elpased: 1.6
batch start
#iterations: 176
currently lose_sum: 94.5679184794426
time_elpased: 1.629
batch start
#iterations: 177
currently lose_sum: 94.18776178359985
time_elpased: 1.591
batch start
#iterations: 178
currently lose_sum: 93.89357715845108
time_elpased: 1.589
batch start
#iterations: 179
currently lose_sum: 93.82480823993683
time_elpased: 1.582
start validation test
0.667319587629
0.641660154554
0.760522795101
0.696053499105
0.667155955023
60.767
batch start
#iterations: 180
currently lose_sum: 93.96682381629944
time_elpased: 1.604
batch start
#iterations: 181
currently lose_sum: 93.80922329425812
time_elpased: 1.581
batch start
#iterations: 182
currently lose_sum: 93.92560178041458
time_elpased: 1.552
batch start
#iterations: 183
currently lose_sum: 93.5076966881752
time_elpased: 1.594
batch start
#iterations: 184
currently lose_sum: 93.8844193816185
time_elpased: 1.61
batch start
#iterations: 185
currently lose_sum: 93.91564816236496
time_elpased: 1.61
batch start
#iterations: 186
currently lose_sum: 93.83118963241577
time_elpased: 1.598
batch start
#iterations: 187
currently lose_sum: 93.38724237680435
time_elpased: 1.586
batch start
#iterations: 188
currently lose_sum: 93.76111388206482
time_elpased: 1.58
batch start
#iterations: 189
currently lose_sum: 93.78280752897263
time_elpased: 1.553
batch start
#iterations: 190
currently lose_sum: 93.8148381114006
time_elpased: 1.58
batch start
#iterations: 191
currently lose_sum: 94.04414284229279
time_elpased: 1.62
batch start
#iterations: 192
currently lose_sum: 93.43409544229507
time_elpased: 1.567
batch start
#iterations: 193
currently lose_sum: 93.70036751031876
time_elpased: 1.567
batch start
#iterations: 194
currently lose_sum: 93.81343913078308
time_elpased: 1.595
batch start
#iterations: 195
currently lose_sum: 94.11130285263062
time_elpased: 1.543
batch start
#iterations: 196
currently lose_sum: 93.88835453987122
time_elpased: 1.608
batch start
#iterations: 197
currently lose_sum: 94.11103218793869
time_elpased: 1.602
batch start
#iterations: 198
currently lose_sum: 94.28574883937836
time_elpased: 1.596
batch start
#iterations: 199
currently lose_sum: 94.55139923095703
time_elpased: 1.58
start validation test
0.668402061856
0.636129995026
0.789647010394
0.704623720097
0.668189197648
60.310
batch start
#iterations: 200
currently lose_sum: 93.97847211360931
time_elpased: 1.615
batch start
#iterations: 201
currently lose_sum: 93.95086741447449
time_elpased: 1.564
batch start
#iterations: 202
currently lose_sum: 94.01714783906937
time_elpased: 1.583
batch start
#iterations: 203
currently lose_sum: 94.04183131456375
time_elpased: 1.602
batch start
#iterations: 204
currently lose_sum: 93.73888248205185
time_elpased: 1.622
batch start
#iterations: 205
currently lose_sum: 94.01988703012466
time_elpased: 1.572
batch start
#iterations: 206
currently lose_sum: 93.77890932559967
time_elpased: 1.616
batch start
#iterations: 207
currently lose_sum: 94.1622064113617
time_elpased: 1.605
batch start
#iterations: 208
currently lose_sum: 93.83143544197083
time_elpased: 1.617
batch start
#iterations: 209
currently lose_sum: 94.21944904327393
time_elpased: 1.587
batch start
#iterations: 210
currently lose_sum: 93.58390659093857
time_elpased: 1.62
batch start
#iterations: 211
currently lose_sum: 93.63117575645447
time_elpased: 1.554
batch start
#iterations: 212
currently lose_sum: 93.42305839061737
time_elpased: 1.662
batch start
#iterations: 213
currently lose_sum: 93.72999888658524
time_elpased: 1.614
batch start
#iterations: 214
currently lose_sum: 93.69158101081848
time_elpased: 1.599
batch start
#iterations: 215
currently lose_sum: 94.07973563671112
time_elpased: 1.58
batch start
#iterations: 216
currently lose_sum: 93.84252607822418
time_elpased: 1.611
batch start
#iterations: 217
currently lose_sum: 93.63784873485565
time_elpased: 1.58
batch start
#iterations: 218
currently lose_sum: 94.02895814180374
time_elpased: 1.577
batch start
#iterations: 219
currently lose_sum: 94.14756351709366
time_elpased: 1.594
start validation test
0.66618556701
0.640024194245
0.762272306267
0.695819633631
0.666016871919
60.414
batch start
#iterations: 220
currently lose_sum: 94.14843797683716
time_elpased: 1.599
batch start
#iterations: 221
currently lose_sum: 94.19494295120239
time_elpased: 1.643
batch start
#iterations: 222
currently lose_sum: 94.03107953071594
time_elpased: 1.636
batch start
#iterations: 223
currently lose_sum: 93.9083354473114
time_elpased: 1.582
batch start
#iterations: 224
currently lose_sum: 94.03774124383926
time_elpased: 1.582
batch start
#iterations: 225
currently lose_sum: 93.70603603124619
time_elpased: 1.589
batch start
#iterations: 226
currently lose_sum: 94.1808420419693
time_elpased: 1.572
batch start
#iterations: 227
currently lose_sum: 93.70154029130936
time_elpased: 1.605
batch start
#iterations: 228
currently lose_sum: 93.93020617961884
time_elpased: 1.544
batch start
#iterations: 229
currently lose_sum: 94.42469787597656
time_elpased: 1.548
batch start
#iterations: 230
currently lose_sum: 93.34549081325531
time_elpased: 1.559
batch start
#iterations: 231
currently lose_sum: 93.16855680942535
time_elpased: 1.553
batch start
#iterations: 232
currently lose_sum: 93.8367447257042
time_elpased: 1.589
batch start
#iterations: 233
currently lose_sum: 94.2334526181221
time_elpased: 1.589
batch start
#iterations: 234
currently lose_sum: 93.81797075271606
time_elpased: 1.581
batch start
#iterations: 235
currently lose_sum: 93.85443758964539
time_elpased: 1.6
batch start
#iterations: 236
currently lose_sum: 94.13719356060028
time_elpased: 1.646
batch start
#iterations: 237
currently lose_sum: 93.71201503276825
time_elpased: 1.586
batch start
#iterations: 238
currently lose_sum: 93.92004126310349
time_elpased: 1.619
batch start
#iterations: 239
currently lose_sum: 93.3374308347702
time_elpased: 1.575
start validation test
0.668350515464
0.63623537223
0.788926623443
0.704401359919
0.668138825509
60.325
batch start
#iterations: 240
currently lose_sum: 93.76535731554031
time_elpased: 1.581
batch start
#iterations: 241
currently lose_sum: 93.80435651540756
time_elpased: 1.561
batch start
#iterations: 242
currently lose_sum: 93.51015585660934
time_elpased: 1.584
batch start
#iterations: 243
currently lose_sum: 93.95571845769882
time_elpased: 1.595
batch start
#iterations: 244
currently lose_sum: 94.00910699367523
time_elpased: 1.6
batch start
#iterations: 245
currently lose_sum: 94.44184708595276
time_elpased: 1.623
batch start
#iterations: 246
currently lose_sum: 93.96650862693787
time_elpased: 1.561
batch start
#iterations: 247
currently lose_sum: 93.62732887268066
time_elpased: 1.567
batch start
#iterations: 248
currently lose_sum: 94.3012620806694
time_elpased: 1.567
batch start
#iterations: 249
currently lose_sum: 94.09452241659164
time_elpased: 1.568
batch start
#iterations: 250
currently lose_sum: 93.75080096721649
time_elpased: 1.564
batch start
#iterations: 251
currently lose_sum: 93.49723041057587
time_elpased: 1.605
batch start
#iterations: 252
currently lose_sum: 93.91920441389084
time_elpased: 1.565
batch start
#iterations: 253
currently lose_sum: 93.96979051828384
time_elpased: 1.573
batch start
#iterations: 254
currently lose_sum: 93.70438981056213
time_elpased: 1.624
batch start
#iterations: 255
currently lose_sum: 93.82222360372543
time_elpased: 1.661
batch start
#iterations: 256
currently lose_sum: 93.37128847837448
time_elpased: 1.596
batch start
#iterations: 257
currently lose_sum: 93.76849853992462
time_elpased: 1.554
batch start
#iterations: 258
currently lose_sum: 93.67442744970322
time_elpased: 1.648
batch start
#iterations: 259
currently lose_sum: 93.71829849481583
time_elpased: 1.568
start validation test
0.666546391753
0.639542876783
0.765977153442
0.697073284945
0.666371825714
60.518
batch start
#iterations: 260
currently lose_sum: 93.72094285488129
time_elpased: 1.555
batch start
#iterations: 261
currently lose_sum: 94.04753535985947
time_elpased: 1.609
batch start
#iterations: 262
currently lose_sum: 93.59278166294098
time_elpased: 1.589
batch start
#iterations: 263
currently lose_sum: 93.88134652376175
time_elpased: 1.583
batch start
#iterations: 264
currently lose_sum: 93.62261402606964
time_elpased: 1.556
batch start
#iterations: 265
currently lose_sum: 93.76990628242493
time_elpased: 1.616
batch start
#iterations: 266
currently lose_sum: 93.61556208133698
time_elpased: 1.607
batch start
#iterations: 267
currently lose_sum: 93.51313501596451
time_elpased: 1.59
batch start
#iterations: 268
currently lose_sum: 93.64429038763046
time_elpased: 1.591
batch start
#iterations: 269
currently lose_sum: 94.05299133062363
time_elpased: 1.559
batch start
#iterations: 270
currently lose_sum: 94.17741858959198
time_elpased: 1.605
batch start
#iterations: 271
currently lose_sum: 93.64778351783752
time_elpased: 1.569
batch start
#iterations: 272
currently lose_sum: 93.91836774349213
time_elpased: 1.585
batch start
#iterations: 273
currently lose_sum: 93.38777023553848
time_elpased: 1.544
batch start
#iterations: 274
currently lose_sum: 93.59673571586609
time_elpased: 1.561
batch start
#iterations: 275
currently lose_sum: 94.16494446992874
time_elpased: 1.557
batch start
#iterations: 276
currently lose_sum: 94.2067461013794
time_elpased: 1.604
batch start
#iterations: 277
currently lose_sum: 94.20509386062622
time_elpased: 1.614
batch start
#iterations: 278
currently lose_sum: 93.63613027334213
time_elpased: 1.603
batch start
#iterations: 279
currently lose_sum: 94.16514652967453
time_elpased: 1.597
start validation test
0.668041237113
0.637585019733
0.781414016672
0.702210302414
0.667842193712
60.457
batch start
#iterations: 280
currently lose_sum: 94.02637338638306
time_elpased: 1.563
batch start
#iterations: 281
currently lose_sum: 93.94350802898407
time_elpased: 1.577
batch start
#iterations: 282
currently lose_sum: 94.07262015342712
time_elpased: 1.648
batch start
#iterations: 283
currently lose_sum: 93.99856299161911
time_elpased: 1.573
batch start
#iterations: 284
currently lose_sum: 93.5278468132019
time_elpased: 1.608
batch start
#iterations: 285
currently lose_sum: 93.98477202653885
time_elpased: 1.601
batch start
#iterations: 286
currently lose_sum: 93.99165141582489
time_elpased: 1.612
batch start
#iterations: 287
currently lose_sum: 93.47296476364136
time_elpased: 1.564
batch start
#iterations: 288
currently lose_sum: 93.94563150405884
time_elpased: 1.591
batch start
#iterations: 289
currently lose_sum: 93.61580514907837
time_elpased: 1.603
batch start
#iterations: 290
currently lose_sum: 93.74111205339432
time_elpased: 1.555
batch start
#iterations: 291
currently lose_sum: 93.5123843550682
time_elpased: 1.636
batch start
#iterations: 292
currently lose_sum: 93.43472999334335
time_elpased: 1.618
batch start
#iterations: 293
currently lose_sum: 93.62687510251999
time_elpased: 1.563
batch start
#iterations: 294
currently lose_sum: 93.55062162876129
time_elpased: 1.607
batch start
#iterations: 295
currently lose_sum: 93.23443442583084
time_elpased: 1.55
batch start
#iterations: 296
currently lose_sum: 93.95836329460144
time_elpased: 1.588
batch start
#iterations: 297
currently lose_sum: 93.72564160823822
time_elpased: 1.563
batch start
#iterations: 298
currently lose_sum: 93.70472759008408
time_elpased: 1.608
batch start
#iterations: 299
currently lose_sum: 93.32497274875641
time_elpased: 1.559
start validation test
0.669639175258
0.635551548926
0.79808582896
0.707605273963
0.669413667346
60.272
batch start
#iterations: 300
currently lose_sum: 93.76948142051697
time_elpased: 1.625
batch start
#iterations: 301
currently lose_sum: 93.8180803656578
time_elpased: 1.594
batch start
#iterations: 302
currently lose_sum: 93.47692608833313
time_elpased: 1.616
batch start
#iterations: 303
currently lose_sum: 93.41181778907776
time_elpased: 1.565
batch start
#iterations: 304
currently lose_sum: 93.56477481126785
time_elpased: 1.63
batch start
#iterations: 305
currently lose_sum: 93.91206783056259
time_elpased: 1.583
batch start
#iterations: 306
currently lose_sum: 93.6834511756897
time_elpased: 1.609
batch start
#iterations: 307
currently lose_sum: 93.61928832530975
time_elpased: 1.568
batch start
#iterations: 308
currently lose_sum: 93.84718930721283
time_elpased: 1.596
batch start
#iterations: 309
currently lose_sum: 93.62426048517227
time_elpased: 1.609
batch start
#iterations: 310
currently lose_sum: 94.06300818920135
time_elpased: 1.583
batch start
#iterations: 311
currently lose_sum: 93.62143737077713
time_elpased: 1.609
batch start
#iterations: 312
currently lose_sum: 93.71698451042175
time_elpased: 1.584
batch start
#iterations: 313
currently lose_sum: 93.487697660923
time_elpased: 1.547
batch start
#iterations: 314
currently lose_sum: 93.43009060621262
time_elpased: 1.609
batch start
#iterations: 315
currently lose_sum: 93.88539493083954
time_elpased: 1.616
batch start
#iterations: 316
currently lose_sum: 93.8954890370369
time_elpased: 1.578
batch start
#iterations: 317
currently lose_sum: 93.72430723905563
time_elpased: 1.543
batch start
#iterations: 318
currently lose_sum: 93.53044718503952
time_elpased: 1.61
batch start
#iterations: 319
currently lose_sum: 93.63032907247543
time_elpased: 1.571
start validation test
0.666701030928
0.639924248946
0.765050941649
0.696915721384
0.66652836249
60.376
batch start
#iterations: 320
currently lose_sum: 93.8159590959549
time_elpased: 1.623
batch start
#iterations: 321
currently lose_sum: 93.7395031452179
time_elpased: 1.588
batch start
#iterations: 322
currently lose_sum: 93.83688807487488
time_elpased: 1.577
batch start
#iterations: 323
currently lose_sum: 93.78966253995895
time_elpased: 1.662
batch start
#iterations: 324
currently lose_sum: 94.02114886045456
time_elpased: 1.598
batch start
#iterations: 325
currently lose_sum: 93.8552393913269
time_elpased: 1.577
batch start
#iterations: 326
currently lose_sum: 93.29877889156342
time_elpased: 1.637
batch start
#iterations: 327
currently lose_sum: 93.82201528549194
time_elpased: 1.579
batch start
#iterations: 328
currently lose_sum: 93.54192298650742
time_elpased: 1.558
batch start
#iterations: 329
currently lose_sum: 94.15138107538223
time_elpased: 1.574
batch start
#iterations: 330
currently lose_sum: 93.42699331045151
time_elpased: 1.555
batch start
#iterations: 331
currently lose_sum: 93.48918914794922
time_elpased: 1.596
batch start
#iterations: 332
currently lose_sum: 93.749338388443
time_elpased: 1.575
batch start
#iterations: 333
currently lose_sum: 93.30303174257278
time_elpased: 1.62
batch start
#iterations: 334
currently lose_sum: 93.91393649578094
time_elpased: 1.557
batch start
#iterations: 335
currently lose_sum: 93.73978507518768
time_elpased: 1.569
batch start
#iterations: 336
currently lose_sum: 93.60045915842056
time_elpased: 1.598
batch start
#iterations: 337
currently lose_sum: 93.71390920877457
time_elpased: 1.607
batch start
#iterations: 338
currently lose_sum: 93.71388065814972
time_elpased: 1.596
batch start
#iterations: 339
currently lose_sum: 93.27670013904572
time_elpased: 1.579
start validation test
0.668556701031
0.640650406504
0.770402387568
0.699560788711
0.66837789522
60.244
batch start
#iterations: 340
currently lose_sum: 93.51456958055496
time_elpased: 1.594
batch start
#iterations: 341
currently lose_sum: 93.59288215637207
time_elpased: 1.61
batch start
#iterations: 342
currently lose_sum: 93.68497174978256
time_elpased: 1.667
batch start
#iterations: 343
currently lose_sum: 93.75470072031021
time_elpased: 1.595
batch start
#iterations: 344
currently lose_sum: 93.58805257081985
time_elpased: 1.6
batch start
#iterations: 345
currently lose_sum: 93.86034965515137
time_elpased: 1.585
batch start
#iterations: 346
currently lose_sum: 93.73815989494324
time_elpased: 1.596
batch start
#iterations: 347
currently lose_sum: 93.63992947340012
time_elpased: 1.606
batch start
#iterations: 348
currently lose_sum: 93.71744507551193
time_elpased: 1.591
batch start
#iterations: 349
currently lose_sum: 93.6537874341011
time_elpased: 1.551
batch start
#iterations: 350
currently lose_sum: 93.58055251836777
time_elpased: 1.594
batch start
#iterations: 351
currently lose_sum: 93.63862025737762
time_elpased: 1.603
batch start
#iterations: 352
currently lose_sum: 93.55869406461716
time_elpased: 1.584
batch start
#iterations: 353
currently lose_sum: 93.85720086097717
time_elpased: 1.637
batch start
#iterations: 354
currently lose_sum: 93.78689366579056
time_elpased: 1.603
batch start
#iterations: 355
currently lose_sum: 93.82005912065506
time_elpased: 1.596
batch start
#iterations: 356
currently lose_sum: 93.64644187688828
time_elpased: 1.586
batch start
#iterations: 357
currently lose_sum: 93.85917043685913
time_elpased: 1.545
batch start
#iterations: 358
currently lose_sum: 93.65828311443329
time_elpased: 1.577
batch start
#iterations: 359
currently lose_sum: 93.31931126117706
time_elpased: 1.586
start validation test
0.666597938144
0.643799238736
0.748482041782
0.692205196536
0.666454177971
60.503
batch start
#iterations: 360
currently lose_sum: 93.84344297647476
time_elpased: 1.595
batch start
#iterations: 361
currently lose_sum: 93.75622183084488
time_elpased: 1.558
batch start
#iterations: 362
currently lose_sum: 93.522028028965
time_elpased: 1.601
batch start
#iterations: 363
currently lose_sum: 93.4789924621582
time_elpased: 1.631
batch start
#iterations: 364
currently lose_sum: 94.20735496282578
time_elpased: 1.619
batch start
#iterations: 365
currently lose_sum: 93.32432818412781
time_elpased: 1.545
batch start
#iterations: 366
currently lose_sum: 93.8197745680809
time_elpased: 1.589
batch start
#iterations: 367
currently lose_sum: 93.87925326824188
time_elpased: 1.609
batch start
#iterations: 368
currently lose_sum: 94.1644759774208
time_elpased: 1.626
batch start
#iterations: 369
currently lose_sum: 93.16709476709366
time_elpased: 1.584
batch start
#iterations: 370
currently lose_sum: 93.45323884487152
time_elpased: 1.598
batch start
#iterations: 371
currently lose_sum: 93.91187876462936
time_elpased: 1.586
batch start
#iterations: 372
currently lose_sum: 93.6625326871872
time_elpased: 1.577
batch start
#iterations: 373
currently lose_sum: 93.33696138858795
time_elpased: 1.581
batch start
#iterations: 374
currently lose_sum: 93.58560979366302
time_elpased: 1.597
batch start
#iterations: 375
currently lose_sum: 93.96475261449814
time_elpased: 1.585
batch start
#iterations: 376
currently lose_sum: 93.01850461959839
time_elpased: 1.583
batch start
#iterations: 377
currently lose_sum: 93.35535913705826
time_elpased: 1.602
batch start
#iterations: 378
currently lose_sum: 93.536756336689
time_elpased: 1.614
batch start
#iterations: 379
currently lose_sum: 93.52223736047745
time_elpased: 1.558
start validation test
0.662628865979
0.637554206418
0.756509210662
0.691956511508
0.662464044554
60.530
batch start
#iterations: 380
currently lose_sum: 93.38029819726944
time_elpased: 1.661
batch start
#iterations: 381
currently lose_sum: 93.89343506097794
time_elpased: 1.56
batch start
#iterations: 382
currently lose_sum: 93.40164190530777
time_elpased: 1.553
batch start
#iterations: 383
currently lose_sum: 93.26593053340912
time_elpased: 1.58
batch start
#iterations: 384
currently lose_sum: 93.84658002853394
time_elpased: 1.6
batch start
#iterations: 385
currently lose_sum: 93.84352141618729
time_elpased: 1.554
batch start
#iterations: 386
currently lose_sum: 93.30978727340698
time_elpased: 1.641
batch start
#iterations: 387
currently lose_sum: 93.62946629524231
time_elpased: 1.581
batch start
#iterations: 388
currently lose_sum: 93.84135735034943
time_elpased: 1.545
batch start
#iterations: 389
currently lose_sum: 93.969202876091
time_elpased: 1.579
batch start
#iterations: 390
currently lose_sum: 93.92797994613647
time_elpased: 1.582
batch start
#iterations: 391
currently lose_sum: 93.59751880168915
time_elpased: 1.534
batch start
#iterations: 392
currently lose_sum: 93.43489646911621
time_elpased: 1.588
batch start
#iterations: 393
currently lose_sum: 93.70584934949875
time_elpased: 1.545
batch start
#iterations: 394
currently lose_sum: 93.17915040254593
time_elpased: 1.599
batch start
#iterations: 395
currently lose_sum: 93.16491323709488
time_elpased: 1.61
batch start
#iterations: 396
currently lose_sum: 93.21125763654709
time_elpased: 1.598
batch start
#iterations: 397
currently lose_sum: 93.46850514411926
time_elpased: 1.567
batch start
#iterations: 398
currently lose_sum: 93.4197124838829
time_elpased: 1.564
batch start
#iterations: 399
currently lose_sum: 93.77087926864624
time_elpased: 1.546
start validation test
0.665257731959
0.636417506137
0.773695585057
0.698374361356
0.665067352582
60.582
acc: 0.671
pre: 0.643
rec: 0.772
F1: 0.701
auc: 0.709
