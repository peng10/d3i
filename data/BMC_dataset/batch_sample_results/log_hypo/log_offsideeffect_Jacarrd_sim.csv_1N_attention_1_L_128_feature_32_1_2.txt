start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.80316984653473
time_elpased: 1.532
batch start
#iterations: 1
currently lose_sum: 100.01801025867462
time_elpased: 1.436
batch start
#iterations: 2
currently lose_sum: 99.8666204214096
time_elpased: 1.486
batch start
#iterations: 3
currently lose_sum: 99.5964862704277
time_elpased: 1.507
batch start
#iterations: 4
currently lose_sum: 99.17197293043137
time_elpased: 1.473
batch start
#iterations: 5
currently lose_sum: 98.99539840221405
time_elpased: 1.531
batch start
#iterations: 6
currently lose_sum: 98.62631213665009
time_elpased: 1.515
batch start
#iterations: 7
currently lose_sum: 98.62659078836441
time_elpased: 1.537
batch start
#iterations: 8
currently lose_sum: 98.70941913127899
time_elpased: 1.436
batch start
#iterations: 9
currently lose_sum: 98.39714688062668
time_elpased: 1.494
batch start
#iterations: 10
currently lose_sum: 97.93280118703842
time_elpased: 1.47
batch start
#iterations: 11
currently lose_sum: 98.13424849510193
time_elpased: 1.487
batch start
#iterations: 12
currently lose_sum: 97.73828130960464
time_elpased: 1.51
batch start
#iterations: 13
currently lose_sum: 97.98115450143814
time_elpased: 1.483
batch start
#iterations: 14
currently lose_sum: 97.24953985214233
time_elpased: 1.488
batch start
#iterations: 15
currently lose_sum: 97.56728553771973
time_elpased: 1.505
batch start
#iterations: 16
currently lose_sum: 97.01731216907501
time_elpased: 1.457
batch start
#iterations: 17
currently lose_sum: 97.3748733997345
time_elpased: 1.483
batch start
#iterations: 18
currently lose_sum: 97.19351327419281
time_elpased: 1.568
batch start
#iterations: 19
currently lose_sum: 96.86876446008682
time_elpased: 1.529
start validation test
0.650979381443
0.625297720313
0.756509210662
0.68467377637
0.650794107551
62.359
batch start
#iterations: 20
currently lose_sum: 97.05954176187515
time_elpased: 1.493
batch start
#iterations: 21
currently lose_sum: 96.76267975568771
time_elpased: 1.463
batch start
#iterations: 22
currently lose_sum: 97.0790147781372
time_elpased: 1.469
batch start
#iterations: 23
currently lose_sum: 96.68616580963135
time_elpased: 1.461
batch start
#iterations: 24
currently lose_sum: 96.62391912937164
time_elpased: 1.501
batch start
#iterations: 25
currently lose_sum: 96.42019301652908
time_elpased: 1.464
batch start
#iterations: 26
currently lose_sum: 96.67729985713959
time_elpased: 1.484
batch start
#iterations: 27
currently lose_sum: 96.60599851608276
time_elpased: 1.482
batch start
#iterations: 28
currently lose_sum: 96.31454157829285
time_elpased: 1.492
batch start
#iterations: 29
currently lose_sum: 96.67622774839401
time_elpased: 1.509
batch start
#iterations: 30
currently lose_sum: 96.63939142227173
time_elpased: 1.505
batch start
#iterations: 31
currently lose_sum: 96.22919911146164
time_elpased: 1.504
batch start
#iterations: 32
currently lose_sum: 96.13806158304214
time_elpased: 1.49
batch start
#iterations: 33
currently lose_sum: 96.44607722759247
time_elpased: 1.474
batch start
#iterations: 34
currently lose_sum: 96.40535867214203
time_elpased: 1.443
batch start
#iterations: 35
currently lose_sum: 96.14497369527817
time_elpased: 1.486
batch start
#iterations: 36
currently lose_sum: 96.37012606859207
time_elpased: 1.46
batch start
#iterations: 37
currently lose_sum: 96.23370999097824
time_elpased: 1.506
batch start
#iterations: 38
currently lose_sum: 96.14180618524551
time_elpased: 1.5
batch start
#iterations: 39
currently lose_sum: 96.10261964797974
time_elpased: 1.569
start validation test
0.660618556701
0.63634781095
0.752392713801
0.689521833443
0.660457433013
61.319
batch start
#iterations: 40
currently lose_sum: 96.12379878759384
time_elpased: 1.454
batch start
#iterations: 41
currently lose_sum: 96.05177861452103
time_elpased: 1.493
batch start
#iterations: 42
currently lose_sum: 96.023746073246
time_elpased: 1.467
batch start
#iterations: 43
currently lose_sum: 95.99846506118774
time_elpased: 1.485
batch start
#iterations: 44
currently lose_sum: 96.28650909662247
time_elpased: 1.57
batch start
#iterations: 45
currently lose_sum: 96.14752811193466
time_elpased: 1.479
batch start
#iterations: 46
currently lose_sum: 95.85485184192657
time_elpased: 1.53
batch start
#iterations: 47
currently lose_sum: 96.1292023062706
time_elpased: 1.47
batch start
#iterations: 48
currently lose_sum: 96.35283929109573
time_elpased: 1.476
batch start
#iterations: 49
currently lose_sum: 96.17422831058502
time_elpased: 1.47
batch start
#iterations: 50
currently lose_sum: 95.8376715183258
time_elpased: 1.515
batch start
#iterations: 51
currently lose_sum: 95.88938736915588
time_elpased: 1.595
batch start
#iterations: 52
currently lose_sum: 95.7816042304039
time_elpased: 1.495
batch start
#iterations: 53
currently lose_sum: 95.56162697076797
time_elpased: 1.444
batch start
#iterations: 54
currently lose_sum: 96.07444548606873
time_elpased: 1.495
batch start
#iterations: 55
currently lose_sum: 95.97947841882706
time_elpased: 1.532
batch start
#iterations: 56
currently lose_sum: 96.04023033380508
time_elpased: 1.452
batch start
#iterations: 57
currently lose_sum: 95.69364315271378
time_elpased: 1.474
batch start
#iterations: 58
currently lose_sum: 96.06805491447449
time_elpased: 1.461
batch start
#iterations: 59
currently lose_sum: 95.94352263212204
time_elpased: 1.496
start validation test
0.663917525773
0.63264459381
0.784604301739
0.700477765527
0.663705641523
61.031
batch start
#iterations: 60
currently lose_sum: 95.85858517885208
time_elpased: 1.525
batch start
#iterations: 61
currently lose_sum: 95.96956086158752
time_elpased: 1.474
batch start
#iterations: 62
currently lose_sum: 95.99927109479904
time_elpased: 1.521
batch start
#iterations: 63
currently lose_sum: 95.89771920442581
time_elpased: 1.553
batch start
#iterations: 64
currently lose_sum: 95.72141915559769
time_elpased: 1.507
batch start
#iterations: 65
currently lose_sum: 96.0502415895462
time_elpased: 1.481
batch start
#iterations: 66
currently lose_sum: 95.55542480945587
time_elpased: 1.503
batch start
#iterations: 67
currently lose_sum: 95.6311936378479
time_elpased: 1.466
batch start
#iterations: 68
currently lose_sum: 95.79789251089096
time_elpased: 1.493
batch start
#iterations: 69
currently lose_sum: 95.67913520336151
time_elpased: 1.538
batch start
#iterations: 70
currently lose_sum: 95.55374002456665
time_elpased: 1.475
batch start
#iterations: 71
currently lose_sum: 95.75913745164871
time_elpased: 1.458
batch start
#iterations: 72
currently lose_sum: 95.61018359661102
time_elpased: 1.467
batch start
#iterations: 73
currently lose_sum: 95.93221825361252
time_elpased: 1.459
batch start
#iterations: 74
currently lose_sum: 95.6950489282608
time_elpased: 1.584
batch start
#iterations: 75
currently lose_sum: 95.64130836725235
time_elpased: 1.497
batch start
#iterations: 76
currently lose_sum: 95.85839504003525
time_elpased: 1.45
batch start
#iterations: 77
currently lose_sum: 95.75799882411957
time_elpased: 1.51
batch start
#iterations: 78
currently lose_sum: 95.53628194332123
time_elpased: 1.463
batch start
#iterations: 79
currently lose_sum: 96.40085333585739
time_elpased: 1.453
start validation test
0.665515463918
0.636918900577
0.772666460842
0.698256219484
0.665327343816
60.978
batch start
#iterations: 80
currently lose_sum: 95.72538936138153
time_elpased: 1.478
batch start
#iterations: 81
currently lose_sum: 95.82171416282654
time_elpased: 1.492
batch start
#iterations: 82
currently lose_sum: 95.95904487371445
time_elpased: 1.477
batch start
#iterations: 83
currently lose_sum: 95.39213126897812
time_elpased: 1.535
batch start
#iterations: 84
currently lose_sum: 95.68055886030197
time_elpased: 1.565
batch start
#iterations: 85
currently lose_sum: 95.68200331926346
time_elpased: 1.442
batch start
#iterations: 86
currently lose_sum: 95.78562313318253
time_elpased: 1.455
batch start
#iterations: 87
currently lose_sum: 95.62896430492401
time_elpased: 1.467
batch start
#iterations: 88
currently lose_sum: 95.7008421421051
time_elpased: 1.46
batch start
#iterations: 89
currently lose_sum: 95.51260203123093
time_elpased: 1.437
batch start
#iterations: 90
currently lose_sum: 95.8674567937851
time_elpased: 1.491
batch start
#iterations: 91
currently lose_sum: 95.65400320291519
time_elpased: 1.517
batch start
#iterations: 92
currently lose_sum: 95.63815706968307
time_elpased: 1.489
batch start
#iterations: 93
currently lose_sum: 95.5427680015564
time_elpased: 1.479
batch start
#iterations: 94
currently lose_sum: 95.52806490659714
time_elpased: 1.469
batch start
#iterations: 95
currently lose_sum: 95.74492102861404
time_elpased: 1.458
batch start
#iterations: 96
currently lose_sum: 95.24599009752274
time_elpased: 1.508
batch start
#iterations: 97
currently lose_sum: 95.41791546344757
time_elpased: 1.442
batch start
#iterations: 98
currently lose_sum: 95.61539250612259
time_elpased: 1.47
batch start
#iterations: 99
currently lose_sum: 95.55309057235718
time_elpased: 1.482
start validation test
0.646340206186
0.671717171717
0.574868786663
0.61953086009
0.646465685286
61.788
batch start
#iterations: 100
currently lose_sum: 95.45132571458817
time_elpased: 1.503
batch start
#iterations: 101
currently lose_sum: 95.50275135040283
time_elpased: 1.501
batch start
#iterations: 102
currently lose_sum: 95.84249210357666
time_elpased: 1.53
batch start
#iterations: 103
currently lose_sum: 95.30681371688843
time_elpased: 1.564
batch start
#iterations: 104
currently lose_sum: 95.70289903879166
time_elpased: 1.494
batch start
#iterations: 105
currently lose_sum: 95.23452353477478
time_elpased: 1.472
batch start
#iterations: 106
currently lose_sum: 95.20992869138718
time_elpased: 1.538
batch start
#iterations: 107
currently lose_sum: 95.56156474351883
time_elpased: 1.538
batch start
#iterations: 108
currently lose_sum: 95.5321774482727
time_elpased: 1.469
batch start
#iterations: 109
currently lose_sum: 95.38018321990967
time_elpased: 1.462
batch start
#iterations: 110
currently lose_sum: 95.48475527763367
time_elpased: 1.484
batch start
#iterations: 111
currently lose_sum: 94.86960870027542
time_elpased: 1.486
batch start
#iterations: 112
currently lose_sum: 95.64328497648239
time_elpased: 1.484
batch start
#iterations: 113
currently lose_sum: 94.98474609851837
time_elpased: 1.487
batch start
#iterations: 114
currently lose_sum: 95.39266782999039
time_elpased: 1.497
batch start
#iterations: 115
currently lose_sum: 95.24994850158691
time_elpased: 1.538
batch start
#iterations: 116
currently lose_sum: 95.08599865436554
time_elpased: 1.479
batch start
#iterations: 117
currently lose_sum: 94.98458880186081
time_elpased: 1.477
batch start
#iterations: 118
currently lose_sum: 95.42175108194351
time_elpased: 1.507
batch start
#iterations: 119
currently lose_sum: 95.15198630094528
time_elpased: 1.464
start validation test
0.663092783505
0.624990176817
0.818462488422
0.708760360039
0.662820008024
60.806
batch start
#iterations: 120
currently lose_sum: 95.22304105758667
time_elpased: 1.433
batch start
#iterations: 121
currently lose_sum: 94.97638767957687
time_elpased: 1.446
batch start
#iterations: 122
currently lose_sum: 95.36235415935516
time_elpased: 1.457
batch start
#iterations: 123
currently lose_sum: 95.43684428930283
time_elpased: 1.512
batch start
#iterations: 124
currently lose_sum: 95.59509062767029
time_elpased: 1.477
batch start
#iterations: 125
currently lose_sum: 95.28937721252441
time_elpased: 1.446
batch start
#iterations: 126
currently lose_sum: 95.10291427373886
time_elpased: 1.473
batch start
#iterations: 127
currently lose_sum: 95.67368108034134
time_elpased: 1.456
batch start
#iterations: 128
currently lose_sum: 95.33201718330383
time_elpased: 1.433
batch start
#iterations: 129
currently lose_sum: 95.4165912270546
time_elpased: 1.465
batch start
#iterations: 130
currently lose_sum: 95.2168875336647
time_elpased: 1.451
batch start
#iterations: 131
currently lose_sum: 95.61151617765427
time_elpased: 1.516
batch start
#iterations: 132
currently lose_sum: 95.42004877328873
time_elpased: 1.444
batch start
#iterations: 133
currently lose_sum: 94.96847873926163
time_elpased: 1.483
batch start
#iterations: 134
currently lose_sum: 94.99349296092987
time_elpased: 1.492
batch start
#iterations: 135
currently lose_sum: 95.1990385055542
time_elpased: 1.513
batch start
#iterations: 136
currently lose_sum: 95.2262362241745
time_elpased: 1.522
batch start
#iterations: 137
currently lose_sum: 95.27790582180023
time_elpased: 1.467
batch start
#iterations: 138
currently lose_sum: 95.29692393541336
time_elpased: 1.463
batch start
#iterations: 139
currently lose_sum: 95.06826573610306
time_elpased: 1.513
start validation test
0.666546391753
0.627974783294
0.820109087167
0.711295577275
0.666276788755
60.842
batch start
#iterations: 140
currently lose_sum: 94.96600902080536
time_elpased: 1.508
batch start
#iterations: 141
currently lose_sum: 95.56804114580154
time_elpased: 1.458
batch start
#iterations: 142
currently lose_sum: 95.0964732170105
time_elpased: 1.491
batch start
#iterations: 143
currently lose_sum: 95.16932213306427
time_elpased: 1.464
batch start
#iterations: 144
currently lose_sum: 95.37558615207672
time_elpased: 1.443
batch start
#iterations: 145
currently lose_sum: 95.20888292789459
time_elpased: 1.496
batch start
#iterations: 146
currently lose_sum: 95.28684890270233
time_elpased: 1.464
batch start
#iterations: 147
currently lose_sum: 95.14188754558563
time_elpased: 1.501
batch start
#iterations: 148
currently lose_sum: 95.11691874265671
time_elpased: 1.451
batch start
#iterations: 149
currently lose_sum: 95.43641299009323
time_elpased: 1.468
batch start
#iterations: 150
currently lose_sum: 95.26001280546188
time_elpased: 1.493
batch start
#iterations: 151
currently lose_sum: 95.11722028255463
time_elpased: 1.454
batch start
#iterations: 152
currently lose_sum: 95.28613716363907
time_elpased: 1.482
batch start
#iterations: 153
currently lose_sum: 95.43295377492905
time_elpased: 1.461
batch start
#iterations: 154
currently lose_sum: 95.32560968399048
time_elpased: 1.518
batch start
#iterations: 155
currently lose_sum: 95.12752574682236
time_elpased: 1.519
batch start
#iterations: 156
currently lose_sum: 95.26022332906723
time_elpased: 1.446
batch start
#iterations: 157
currently lose_sum: 94.66312599182129
time_elpased: 1.509
batch start
#iterations: 158
currently lose_sum: 94.78434216976166
time_elpased: 1.442
batch start
#iterations: 159
currently lose_sum: 95.10465770959854
time_elpased: 1.447
start validation test
0.649536082474
0.659872890642
0.619738602449
0.639176351961
0.649588396546
61.492
batch start
#iterations: 160
currently lose_sum: 94.93790584802628
time_elpased: 1.519
batch start
#iterations: 161
currently lose_sum: 95.2514955997467
time_elpased: 1.457
batch start
#iterations: 162
currently lose_sum: 95.49477487802505
time_elpased: 1.496
batch start
#iterations: 163
currently lose_sum: 95.31704837083817
time_elpased: 1.501
batch start
#iterations: 164
currently lose_sum: 95.2855978012085
time_elpased: 1.505
batch start
#iterations: 165
currently lose_sum: 94.88475048542023
time_elpased: 1.523
batch start
#iterations: 166
currently lose_sum: 95.13836139440536
time_elpased: 1.479
batch start
#iterations: 167
currently lose_sum: 95.09442788362503
time_elpased: 1.455
batch start
#iterations: 168
currently lose_sum: 95.12840658426285
time_elpased: 1.451
batch start
#iterations: 169
currently lose_sum: 95.3603110909462
time_elpased: 1.542
batch start
#iterations: 170
currently lose_sum: 95.0347860455513
time_elpased: 1.591
batch start
#iterations: 171
currently lose_sum: 95.39134871959686
time_elpased: 1.464
batch start
#iterations: 172
currently lose_sum: 95.47979724407196
time_elpased: 1.43
batch start
#iterations: 173
currently lose_sum: 95.11372643709183
time_elpased: 1.521
batch start
#iterations: 174
currently lose_sum: 94.68101817369461
time_elpased: 1.494
batch start
#iterations: 175
currently lose_sum: 94.87873357534409
time_elpased: 1.489
batch start
#iterations: 176
currently lose_sum: 94.88449585437775
time_elpased: 1.479
batch start
#iterations: 177
currently lose_sum: 95.30268734693527
time_elpased: 1.478
batch start
#iterations: 178
currently lose_sum: 95.21338522434235
time_elpased: 1.465
batch start
#iterations: 179
currently lose_sum: 94.79470711946487
time_elpased: 1.469
start validation test
0.665721649485
0.641654978962
0.753318925594
0.693017751479
0.665567858955
60.577
batch start
#iterations: 180
currently lose_sum: 94.65289014577866
time_elpased: 1.513
batch start
#iterations: 181
currently lose_sum: 94.90376901626587
time_elpased: 1.449
batch start
#iterations: 182
currently lose_sum: 94.78286278247833
time_elpased: 1.525
batch start
#iterations: 183
currently lose_sum: 94.95563834905624
time_elpased: 1.457
batch start
#iterations: 184
currently lose_sum: 95.37129527330399
time_elpased: 1.428
batch start
#iterations: 185
currently lose_sum: 95.08071780204773
time_elpased: 1.498
batch start
#iterations: 186
currently lose_sum: 94.66126489639282
time_elpased: 1.512
batch start
#iterations: 187
currently lose_sum: 95.22581744194031
time_elpased: 1.492
batch start
#iterations: 188
currently lose_sum: 95.32421451807022
time_elpased: 1.483
batch start
#iterations: 189
currently lose_sum: 94.98475861549377
time_elpased: 1.444
batch start
#iterations: 190
currently lose_sum: 95.26104235649109
time_elpased: 1.445
batch start
#iterations: 191
currently lose_sum: 95.0459833741188
time_elpased: 1.518
batch start
#iterations: 192
currently lose_sum: 94.79747581481934
time_elpased: 1.541
batch start
#iterations: 193
currently lose_sum: 95.3971461057663
time_elpased: 1.427
batch start
#iterations: 194
currently lose_sum: 94.90534096956253
time_elpased: 1.482
batch start
#iterations: 195
currently lose_sum: 94.75651198625565
time_elpased: 1.503
batch start
#iterations: 196
currently lose_sum: 95.09939128160477
time_elpased: 1.507
batch start
#iterations: 197
currently lose_sum: 95.0353353023529
time_elpased: 1.515
batch start
#iterations: 198
currently lose_sum: 95.13512486219406
time_elpased: 1.559
batch start
#iterations: 199
currently lose_sum: 94.84545886516571
time_elpased: 1.516
start validation test
0.662010309278
0.654116269996
0.690130698775
0.671641043618
0.661960939597
60.943
batch start
#iterations: 200
currently lose_sum: 95.03838407993317
time_elpased: 1.49
batch start
#iterations: 201
currently lose_sum: 94.81800997257233
time_elpased: 1.44
batch start
#iterations: 202
currently lose_sum: 94.9019844532013
time_elpased: 1.494
batch start
#iterations: 203
currently lose_sum: 94.80920046567917
time_elpased: 1.497
batch start
#iterations: 204
currently lose_sum: 95.00328814983368
time_elpased: 1.492
batch start
#iterations: 205
currently lose_sum: 94.91889417171478
time_elpased: 1.461
batch start
#iterations: 206
currently lose_sum: 94.64105367660522
time_elpased: 1.45
batch start
#iterations: 207
currently lose_sum: 94.86735939979553
time_elpased: 1.527
batch start
#iterations: 208
currently lose_sum: 95.19496858119965
time_elpased: 1.473
batch start
#iterations: 209
currently lose_sum: 95.11179089546204
time_elpased: 1.486
batch start
#iterations: 210
currently lose_sum: 94.73694586753845
time_elpased: 1.474
batch start
#iterations: 211
currently lose_sum: 95.00993698835373
time_elpased: 1.49
batch start
#iterations: 212
currently lose_sum: 95.05334144830704
time_elpased: 1.492
batch start
#iterations: 213
currently lose_sum: 94.70185267925262
time_elpased: 1.528
batch start
#iterations: 214
currently lose_sum: 94.84850758314133
time_elpased: 1.498
batch start
#iterations: 215
currently lose_sum: 94.86500179767609
time_elpased: 1.531
batch start
#iterations: 216
currently lose_sum: 94.70290869474411
time_elpased: 1.486
batch start
#iterations: 217
currently lose_sum: 95.0219509601593
time_elpased: 1.475
batch start
#iterations: 218
currently lose_sum: 94.71207624673843
time_elpased: 1.48
batch start
#iterations: 219
currently lose_sum: 94.9630885720253
time_elpased: 1.492
start validation test
0.666340206186
0.646389891697
0.737058762993
0.688753185556
0.666216048852
60.763
batch start
#iterations: 220
currently lose_sum: 95.05695337057114
time_elpased: 1.528
batch start
#iterations: 221
currently lose_sum: 95.00294089317322
time_elpased: 1.459
batch start
#iterations: 222
currently lose_sum: 95.02444583177567
time_elpased: 1.493
batch start
#iterations: 223
currently lose_sum: 94.67283409833908
time_elpased: 1.522
batch start
#iterations: 224
currently lose_sum: 94.7560310959816
time_elpased: 1.477
batch start
#iterations: 225
currently lose_sum: 94.50854426622391
time_elpased: 1.457
batch start
#iterations: 226
currently lose_sum: 94.99406737089157
time_elpased: 1.499
batch start
#iterations: 227
currently lose_sum: 94.81651210784912
time_elpased: 1.474
batch start
#iterations: 228
currently lose_sum: 94.84768795967102
time_elpased: 1.46
batch start
#iterations: 229
currently lose_sum: 94.87138831615448
time_elpased: 1.539
batch start
#iterations: 230
currently lose_sum: 94.83137863874435
time_elpased: 1.487
batch start
#iterations: 231
currently lose_sum: 94.702831864357
time_elpased: 1.492
batch start
#iterations: 232
currently lose_sum: 94.50764119625092
time_elpased: 1.485
batch start
#iterations: 233
currently lose_sum: 95.07600748538971
time_elpased: 1.519
batch start
#iterations: 234
currently lose_sum: 94.58893173933029
time_elpased: 1.475
batch start
#iterations: 235
currently lose_sum: 94.90588003396988
time_elpased: 1.452
batch start
#iterations: 236
currently lose_sum: 94.8199120759964
time_elpased: 1.483
batch start
#iterations: 237
currently lose_sum: 95.04612106084824
time_elpased: 1.458
batch start
#iterations: 238
currently lose_sum: 95.07401847839355
time_elpased: 1.481
batch start
#iterations: 239
currently lose_sum: 94.99426198005676
time_elpased: 1.441
start validation test
0.665051546392
0.624180233007
0.832561490172
0.713466796014
0.664757456849
60.749
batch start
#iterations: 240
currently lose_sum: 94.606804728508
time_elpased: 1.472
batch start
#iterations: 241
currently lose_sum: 94.94844996929169
time_elpased: 1.437
batch start
#iterations: 242
currently lose_sum: 94.86566907167435
time_elpased: 1.538
batch start
#iterations: 243
currently lose_sum: 94.52041256427765
time_elpased: 1.517
batch start
#iterations: 244
currently lose_sum: 95.0620573759079
time_elpased: 1.452
batch start
#iterations: 245
currently lose_sum: 94.96298009157181
time_elpased: 1.475
batch start
#iterations: 246
currently lose_sum: 95.0555585026741
time_elpased: 1.488
batch start
#iterations: 247
currently lose_sum: 94.40623396635056
time_elpased: 1.459
batch start
#iterations: 248
currently lose_sum: 94.72811561822891
time_elpased: 1.443
batch start
#iterations: 249
currently lose_sum: 94.5895369052887
time_elpased: 1.554
batch start
#iterations: 250
currently lose_sum: 94.68251579999924
time_elpased: 1.438
batch start
#iterations: 251
currently lose_sum: 94.67806476354599
time_elpased: 1.482
batch start
#iterations: 252
currently lose_sum: 94.77655512094498
time_elpased: 1.562
batch start
#iterations: 253
currently lose_sum: 94.94319379329681
time_elpased: 1.49
batch start
#iterations: 254
currently lose_sum: 94.64182305335999
time_elpased: 1.509
batch start
#iterations: 255
currently lose_sum: 94.67629367113113
time_elpased: 1.475
batch start
#iterations: 256
currently lose_sum: 94.88187313079834
time_elpased: 1.446
batch start
#iterations: 257
currently lose_sum: 94.54856097698212
time_elpased: 1.572
batch start
#iterations: 258
currently lose_sum: 94.68785101175308
time_elpased: 1.468
batch start
#iterations: 259
currently lose_sum: 94.89161342382431
time_elpased: 1.48
start validation test
0.664226804124
0.632344434344
0.787485849542
0.70143917866
0.664010403858
60.657
batch start
#iterations: 260
currently lose_sum: 94.55454367399216
time_elpased: 1.44
batch start
#iterations: 261
currently lose_sum: 94.51538807153702
time_elpased: 1.498
batch start
#iterations: 262
currently lose_sum: 94.33649784326553
time_elpased: 1.412
batch start
#iterations: 263
currently lose_sum: 94.93581545352936
time_elpased: 1.498
batch start
#iterations: 264
currently lose_sum: 95.00621205568314
time_elpased: 1.505
batch start
#iterations: 265
currently lose_sum: 94.58289694786072
time_elpased: 1.494
batch start
#iterations: 266
currently lose_sum: 94.55101484060287
time_elpased: 1.489
batch start
#iterations: 267
currently lose_sum: 94.9116490483284
time_elpased: 1.437
batch start
#iterations: 268
currently lose_sum: 94.59873229265213
time_elpased: 1.467
batch start
#iterations: 269
currently lose_sum: 94.9226223230362
time_elpased: 1.559
batch start
#iterations: 270
currently lose_sum: 94.6954333782196
time_elpased: 1.454
batch start
#iterations: 271
currently lose_sum: 94.5585173368454
time_elpased: 1.504
batch start
#iterations: 272
currently lose_sum: 94.5241727232933
time_elpased: 1.531
batch start
#iterations: 273
currently lose_sum: 94.3864923119545
time_elpased: 1.481
batch start
#iterations: 274
currently lose_sum: 94.66272169351578
time_elpased: 1.444
batch start
#iterations: 275
currently lose_sum: 94.77455413341522
time_elpased: 1.505
batch start
#iterations: 276
currently lose_sum: 94.54727458953857
time_elpased: 1.473
batch start
#iterations: 277
currently lose_sum: 94.77197992801666
time_elpased: 1.483
batch start
#iterations: 278
currently lose_sum: 94.17308551073074
time_elpased: 1.512
batch start
#iterations: 279
currently lose_sum: 94.77329844236374
time_elpased: 1.433
start validation test
0.666340206186
0.63469523335
0.786559637748
0.702513902293
0.666129142431
60.567
batch start
#iterations: 280
currently lose_sum: 94.75055313110352
time_elpased: 1.538
batch start
#iterations: 281
currently lose_sum: 94.49033975601196
time_elpased: 1.476
batch start
#iterations: 282
currently lose_sum: 94.60953164100647
time_elpased: 1.466
batch start
#iterations: 283
currently lose_sum: 94.97495740652084
time_elpased: 1.482
batch start
#iterations: 284
currently lose_sum: 94.44333696365356
time_elpased: 1.504
batch start
#iterations: 285
currently lose_sum: 94.69385278224945
time_elpased: 1.464
batch start
#iterations: 286
currently lose_sum: 94.4813112616539
time_elpased: 1.471
batch start
#iterations: 287
currently lose_sum: 94.49363547563553
time_elpased: 1.441
batch start
#iterations: 288
currently lose_sum: 94.51671785116196
time_elpased: 1.501
batch start
#iterations: 289
currently lose_sum: 94.70395189523697
time_elpased: 1.453
batch start
#iterations: 290
currently lose_sum: 94.19958180189133
time_elpased: 1.534
batch start
#iterations: 291
currently lose_sum: 94.77173507213593
time_elpased: 1.462
batch start
#iterations: 292
currently lose_sum: 95.11140179634094
time_elpased: 1.489
batch start
#iterations: 293
currently lose_sum: 94.2619177699089
time_elpased: 1.436
batch start
#iterations: 294
currently lose_sum: 94.7374849319458
time_elpased: 1.481
batch start
#iterations: 295
currently lose_sum: 94.92989248037338
time_elpased: 1.427
batch start
#iterations: 296
currently lose_sum: 94.84171712398529
time_elpased: 1.467
batch start
#iterations: 297
currently lose_sum: 94.53485995531082
time_elpased: 1.516
batch start
#iterations: 298
currently lose_sum: 94.86346942186356
time_elpased: 1.437
batch start
#iterations: 299
currently lose_sum: 94.61367893218994
time_elpased: 1.454
start validation test
0.656237113402
0.648711943794
0.684161778327
0.665965439519
0.656188087346
60.872
batch start
#iterations: 300
currently lose_sum: 94.44605076313019
time_elpased: 1.493
batch start
#iterations: 301
currently lose_sum: 95.05077028274536
time_elpased: 1.496
batch start
#iterations: 302
currently lose_sum: 94.76982283592224
time_elpased: 1.461
batch start
#iterations: 303
currently lose_sum: 94.53798466920853
time_elpased: 1.442
batch start
#iterations: 304
currently lose_sum: 94.30394357442856
time_elpased: 1.464
batch start
#iterations: 305
currently lose_sum: 94.36630606651306
time_elpased: 1.497
batch start
#iterations: 306
currently lose_sum: 94.58051681518555
time_elpased: 1.476
batch start
#iterations: 307
currently lose_sum: 94.75193333625793
time_elpased: 1.468
batch start
#iterations: 308
currently lose_sum: 94.42600905895233
time_elpased: 1.449
batch start
#iterations: 309
currently lose_sum: 94.1717357635498
time_elpased: 1.49
batch start
#iterations: 310
currently lose_sum: 94.71023452281952
time_elpased: 1.438
batch start
#iterations: 311
currently lose_sum: 94.74959546327591
time_elpased: 1.512
batch start
#iterations: 312
currently lose_sum: 94.47925877571106
time_elpased: 1.506
batch start
#iterations: 313
currently lose_sum: 94.31558495759964
time_elpased: 1.455
batch start
#iterations: 314
currently lose_sum: 94.88973790407181
time_elpased: 1.507
batch start
#iterations: 315
currently lose_sum: 94.72851186990738
time_elpased: 1.484
batch start
#iterations: 316
currently lose_sum: 94.2915860414505
time_elpased: 1.503
batch start
#iterations: 317
currently lose_sum: 94.55415147542953
time_elpased: 1.483
batch start
#iterations: 318
currently lose_sum: 94.57425773143768
time_elpased: 1.468
batch start
#iterations: 319
currently lose_sum: 94.66966170072556
time_elpased: 1.477
start validation test
0.667783505155
0.641131815045
0.764845116806
0.697545637993
0.667613098525
60.511
batch start
#iterations: 320
currently lose_sum: 94.49531030654907
time_elpased: 1.523
batch start
#iterations: 321
currently lose_sum: 94.51265323162079
time_elpased: 1.471
batch start
#iterations: 322
currently lose_sum: 94.80202615261078
time_elpased: 1.479
batch start
#iterations: 323
currently lose_sum: 94.46915179491043
time_elpased: 1.479
batch start
#iterations: 324
currently lose_sum: 94.93490904569626
time_elpased: 1.482
batch start
#iterations: 325
currently lose_sum: 94.42854672670364
time_elpased: 1.552
batch start
#iterations: 326
currently lose_sum: 94.57234251499176
time_elpased: 1.484
batch start
#iterations: 327
currently lose_sum: 94.49768441915512
time_elpased: 1.449
batch start
#iterations: 328
currently lose_sum: 94.46006488800049
time_elpased: 1.533
batch start
#iterations: 329
currently lose_sum: 94.48841857910156
time_elpased: 1.479
batch start
#iterations: 330
currently lose_sum: 94.70323705673218
time_elpased: 1.466
batch start
#iterations: 331
currently lose_sum: 94.68742895126343
time_elpased: 1.451
batch start
#iterations: 332
currently lose_sum: 94.1960175037384
time_elpased: 1.453
batch start
#iterations: 333
currently lose_sum: 94.37117546796799
time_elpased: 1.457
batch start
#iterations: 334
currently lose_sum: 94.21676015853882
time_elpased: 1.445
batch start
#iterations: 335
currently lose_sum: 94.12705761194229
time_elpased: 1.49
batch start
#iterations: 336
currently lose_sum: 94.28508281707764
time_elpased: 1.457
batch start
#iterations: 337
currently lose_sum: 94.86763256788254
time_elpased: 1.459
batch start
#iterations: 338
currently lose_sum: 94.29116922616959
time_elpased: 1.446
batch start
#iterations: 339
currently lose_sum: 94.60241866111755
time_elpased: 1.463
start validation test
0.665824742268
0.631978452498
0.796850879901
0.70490236242
0.665594705674
60.595
batch start
#iterations: 340
currently lose_sum: 94.87093472480774
time_elpased: 1.543
batch start
#iterations: 341
currently lose_sum: 94.57260870933533
time_elpased: 1.458
batch start
#iterations: 342
currently lose_sum: 94.85181123018265
time_elpased: 1.453
batch start
#iterations: 343
currently lose_sum: 94.69504052400589
time_elpased: 1.509
batch start
#iterations: 344
currently lose_sum: 94.13958674669266
time_elpased: 1.537
batch start
#iterations: 345
currently lose_sum: 94.7760426402092
time_elpased: 1.496
batch start
#iterations: 346
currently lose_sum: 94.32687747478485
time_elpased: 1.48
batch start
#iterations: 347
currently lose_sum: 94.42545318603516
time_elpased: 1.505
batch start
#iterations: 348
currently lose_sum: 94.9574226140976
time_elpased: 1.47
batch start
#iterations: 349
currently lose_sum: 94.86540019512177
time_elpased: 1.502
batch start
#iterations: 350
currently lose_sum: 94.78148609399796
time_elpased: 1.455
batch start
#iterations: 351
currently lose_sum: 94.31838071346283
time_elpased: 1.483
batch start
#iterations: 352
currently lose_sum: 94.72298616170883
time_elpased: 1.475
batch start
#iterations: 353
currently lose_sum: 94.32836717367172
time_elpased: 1.452
batch start
#iterations: 354
currently lose_sum: 94.43992680311203
time_elpased: 1.434
batch start
#iterations: 355
currently lose_sum: 94.28722959756851
time_elpased: 1.485
batch start
#iterations: 356
currently lose_sum: 94.77877748012543
time_elpased: 1.455
batch start
#iterations: 357
currently lose_sum: 94.79575824737549
time_elpased: 1.497
batch start
#iterations: 358
currently lose_sum: 94.52600014209747
time_elpased: 1.484
batch start
#iterations: 359
currently lose_sum: 94.44062131643295
time_elpased: 1.516
start validation test
0.670257731959
0.639754167368
0.782031491201
0.703774021764
0.670061495884
60.390
batch start
#iterations: 360
currently lose_sum: 94.70982092618942
time_elpased: 1.496
batch start
#iterations: 361
currently lose_sum: 94.93782961368561
time_elpased: 1.453
batch start
#iterations: 362
currently lose_sum: 94.59128308296204
time_elpased: 1.512
batch start
#iterations: 363
currently lose_sum: 94.32330799102783
time_elpased: 1.466
batch start
#iterations: 364
currently lose_sum: 94.47054183483124
time_elpased: 1.518
batch start
#iterations: 365
currently lose_sum: 94.5918065905571
time_elpased: 1.462
batch start
#iterations: 366
currently lose_sum: 94.9549018740654
time_elpased: 1.485
batch start
#iterations: 367
currently lose_sum: 94.28360456228256
time_elpased: 1.454
batch start
#iterations: 368
currently lose_sum: 94.86638510227203
time_elpased: 1.527
batch start
#iterations: 369
currently lose_sum: 94.70056855678558
time_elpased: 1.497
batch start
#iterations: 370
currently lose_sum: 94.38924252986908
time_elpased: 1.429
batch start
#iterations: 371
currently lose_sum: 94.63528645038605
time_elpased: 1.458
batch start
#iterations: 372
currently lose_sum: 94.40575581789017
time_elpased: 1.502
batch start
#iterations: 373
currently lose_sum: 94.46964603662491
time_elpased: 1.522
batch start
#iterations: 374
currently lose_sum: 94.65759295225143
time_elpased: 1.477
batch start
#iterations: 375
currently lose_sum: 94.54314404726028
time_elpased: 1.573
batch start
#iterations: 376
currently lose_sum: 94.25519740581512
time_elpased: 1.486
batch start
#iterations: 377
currently lose_sum: 94.88555634021759
time_elpased: 1.477
batch start
#iterations: 378
currently lose_sum: 94.6437674164772
time_elpased: 1.478
batch start
#iterations: 379
currently lose_sum: 94.70142728090286
time_elpased: 1.492
start validation test
0.668865979381
0.644139017771
0.757229597612
0.696121097446
0.66871084342
60.491
batch start
#iterations: 380
currently lose_sum: 94.6557964682579
time_elpased: 1.457
batch start
#iterations: 381
currently lose_sum: 94.56492280960083
time_elpased: 1.514
batch start
#iterations: 382
currently lose_sum: 94.45522266626358
time_elpased: 1.458
batch start
#iterations: 383
currently lose_sum: 94.88311290740967
time_elpased: 1.488
batch start
#iterations: 384
currently lose_sum: 94.52375864982605
time_elpased: 1.519
batch start
#iterations: 385
currently lose_sum: 94.73021352291107
time_elpased: 1.495
batch start
#iterations: 386
currently lose_sum: 94.90493166446686
time_elpased: 1.504
batch start
#iterations: 387
currently lose_sum: 94.76460933685303
time_elpased: 1.467
batch start
#iterations: 388
currently lose_sum: 94.76890790462494
time_elpased: 1.496
batch start
#iterations: 389
currently lose_sum: 94.40374386310577
time_elpased: 1.51
batch start
#iterations: 390
currently lose_sum: 94.7048653960228
time_elpased: 1.451
batch start
#iterations: 391
currently lose_sum: 94.46702498197556
time_elpased: 1.51
batch start
#iterations: 392
currently lose_sum: 94.69673824310303
time_elpased: 1.524
batch start
#iterations: 393
currently lose_sum: 94.45776760578156
time_elpased: 1.413
batch start
#iterations: 394
currently lose_sum: 94.68350660800934
time_elpased: 1.455
batch start
#iterations: 395
currently lose_sum: 94.14145612716675
time_elpased: 1.49
batch start
#iterations: 396
currently lose_sum: 94.1286980509758
time_elpased: 1.458
batch start
#iterations: 397
currently lose_sum: 94.24169927835464
time_elpased: 1.495
batch start
#iterations: 398
currently lose_sum: 94.5039033293724
time_elpased: 1.438
batch start
#iterations: 399
currently lose_sum: 94.39251518249512
time_elpased: 1.498
start validation test
0.665051546392
0.64824537165
0.724297622723
0.68416447944
0.664947530767
60.636
acc: 0.665
pre: 0.635
rec: 0.776
F1: 0.699
auc: 0.702
