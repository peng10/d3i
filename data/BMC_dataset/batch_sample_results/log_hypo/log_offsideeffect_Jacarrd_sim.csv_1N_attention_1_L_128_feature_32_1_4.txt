start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.8083239197731
time_elpased: 1.54
batch start
#iterations: 1
currently lose_sum: 100.10512083768845
time_elpased: 1.486
batch start
#iterations: 2
currently lose_sum: 99.69249105453491
time_elpased: 1.463
batch start
#iterations: 3
currently lose_sum: 99.48646891117096
time_elpased: 1.461
batch start
#iterations: 4
currently lose_sum: 99.48778659105301
time_elpased: 1.529
batch start
#iterations: 5
currently lose_sum: 99.10912066698074
time_elpased: 1.517
batch start
#iterations: 6
currently lose_sum: 98.71188354492188
time_elpased: 1.474
batch start
#iterations: 7
currently lose_sum: 98.56758624315262
time_elpased: 1.446
batch start
#iterations: 8
currently lose_sum: 98.54986608028412
time_elpased: 1.458
batch start
#iterations: 9
currently lose_sum: 98.11473536491394
time_elpased: 1.479
batch start
#iterations: 10
currently lose_sum: 97.96281093358994
time_elpased: 1.525
batch start
#iterations: 11
currently lose_sum: 97.82677555084229
time_elpased: 1.486
batch start
#iterations: 12
currently lose_sum: 97.62923461198807
time_elpased: 1.472
batch start
#iterations: 13
currently lose_sum: 97.82369154691696
time_elpased: 1.428
batch start
#iterations: 14
currently lose_sum: 97.37914115190506
time_elpased: 1.459
batch start
#iterations: 15
currently lose_sum: 97.41313707828522
time_elpased: 1.458
batch start
#iterations: 16
currently lose_sum: 97.38738417625427
time_elpased: 1.485
batch start
#iterations: 17
currently lose_sum: 97.36162716150284
time_elpased: 1.462
batch start
#iterations: 18
currently lose_sum: 96.50820851325989
time_elpased: 1.468
batch start
#iterations: 19
currently lose_sum: 97.22845786809921
time_elpased: 1.47
start validation test
0.65175257732
0.644933920705
0.677987033035
0.66104756171
0.651706518686
62.174
batch start
#iterations: 20
currently lose_sum: 97.13156127929688
time_elpased: 1.523
batch start
#iterations: 21
currently lose_sum: 96.62929493188858
time_elpased: 1.476
batch start
#iterations: 22
currently lose_sum: 97.0269986987114
time_elpased: 1.48
batch start
#iterations: 23
currently lose_sum: 97.27914637327194
time_elpased: 1.47
batch start
#iterations: 24
currently lose_sum: 96.8765988945961
time_elpased: 1.434
batch start
#iterations: 25
currently lose_sum: 96.80537641048431
time_elpased: 1.492
batch start
#iterations: 26
currently lose_sum: 97.0932646393776
time_elpased: 1.437
batch start
#iterations: 27
currently lose_sum: 96.46362674236298
time_elpased: 1.473
batch start
#iterations: 28
currently lose_sum: 96.66256821155548
time_elpased: 1.507
batch start
#iterations: 29
currently lose_sum: 96.5249873995781
time_elpased: 1.513
batch start
#iterations: 30
currently lose_sum: 96.62791872024536
time_elpased: 1.505
batch start
#iterations: 31
currently lose_sum: 96.6470872759819
time_elpased: 1.579
batch start
#iterations: 32
currently lose_sum: 96.44780683517456
time_elpased: 1.516
batch start
#iterations: 33
currently lose_sum: 96.23292517662048
time_elpased: 1.43
batch start
#iterations: 34
currently lose_sum: 96.47530770301819
time_elpased: 1.473
batch start
#iterations: 35
currently lose_sum: 96.19830685853958
time_elpased: 1.471
batch start
#iterations: 36
currently lose_sum: 96.14023470878601
time_elpased: 1.485
batch start
#iterations: 37
currently lose_sum: 96.37071323394775
time_elpased: 1.477
batch start
#iterations: 38
currently lose_sum: 95.7978453040123
time_elpased: 1.474
batch start
#iterations: 39
currently lose_sum: 95.95717400312424
time_elpased: 1.517
start validation test
0.660360824742
0.637385804638
0.746732530616
0.687739917539
0.660209185891
61.507
batch start
#iterations: 40
currently lose_sum: 96.5180104970932
time_elpased: 1.459
batch start
#iterations: 41
currently lose_sum: 95.91030812263489
time_elpased: 1.472
batch start
#iterations: 42
currently lose_sum: 95.69780886173248
time_elpased: 1.483
batch start
#iterations: 43
currently lose_sum: 96.44539213180542
time_elpased: 1.507
batch start
#iterations: 44
currently lose_sum: 96.1632165312767
time_elpased: 1.453
batch start
#iterations: 45
currently lose_sum: 95.82685536146164
time_elpased: 1.5
batch start
#iterations: 46
currently lose_sum: 95.78056782484055
time_elpased: 1.461
batch start
#iterations: 47
currently lose_sum: 96.2305839061737
time_elpased: 1.475
batch start
#iterations: 48
currently lose_sum: 95.80472469329834
time_elpased: 1.429
batch start
#iterations: 49
currently lose_sum: 96.09403032064438
time_elpased: 1.486
batch start
#iterations: 50
currently lose_sum: 95.88002294301987
time_elpased: 1.477
batch start
#iterations: 51
currently lose_sum: 96.01370912790298
time_elpased: 1.488
batch start
#iterations: 52
currently lose_sum: 95.83151882886887
time_elpased: 1.469
batch start
#iterations: 53
currently lose_sum: 95.68894374370575
time_elpased: 1.471
batch start
#iterations: 54
currently lose_sum: 95.78950732946396
time_elpased: 1.523
batch start
#iterations: 55
currently lose_sum: 96.10540288686752
time_elpased: 1.481
batch start
#iterations: 56
currently lose_sum: 95.86393350362778
time_elpased: 1.483
batch start
#iterations: 57
currently lose_sum: 95.65614277124405
time_elpased: 1.46
batch start
#iterations: 58
currently lose_sum: 95.75314128398895
time_elpased: 1.428
batch start
#iterations: 59
currently lose_sum: 95.76783066987991
time_elpased: 1.476
start validation test
0.667371134021
0.639534883721
0.769784913039
0.698641012469
0.667191330835
61.000
batch start
#iterations: 60
currently lose_sum: 96.22099947929382
time_elpased: 1.445
batch start
#iterations: 61
currently lose_sum: 96.02020394802094
time_elpased: 1.471
batch start
#iterations: 62
currently lose_sum: 95.77528256177902
time_elpased: 1.478
batch start
#iterations: 63
currently lose_sum: 96.02188563346863
time_elpased: 1.497
batch start
#iterations: 64
currently lose_sum: 96.09938788414001
time_elpased: 1.453
batch start
#iterations: 65
currently lose_sum: 96.02713644504547
time_elpased: 1.468
batch start
#iterations: 66
currently lose_sum: 95.66979092359543
time_elpased: 1.458
batch start
#iterations: 67
currently lose_sum: 95.51593291759491
time_elpased: 1.538
batch start
#iterations: 68
currently lose_sum: 95.42293620109558
time_elpased: 1.452
batch start
#iterations: 69
currently lose_sum: 95.8014457821846
time_elpased: 1.506
batch start
#iterations: 70
currently lose_sum: 95.6836838722229
time_elpased: 1.544
batch start
#iterations: 71
currently lose_sum: 95.74012917280197
time_elpased: 1.5
batch start
#iterations: 72
currently lose_sum: 95.68513107299805
time_elpased: 1.459
batch start
#iterations: 73
currently lose_sum: 95.4794949889183
time_elpased: 1.482
batch start
#iterations: 74
currently lose_sum: 95.65255451202393
time_elpased: 1.468
batch start
#iterations: 75
currently lose_sum: 95.55419111251831
time_elpased: 1.494
batch start
#iterations: 76
currently lose_sum: 95.35939103364944
time_elpased: 1.456
batch start
#iterations: 77
currently lose_sum: 95.45159637928009
time_elpased: 1.46
batch start
#iterations: 78
currently lose_sum: 95.6570296883583
time_elpased: 1.443
batch start
#iterations: 79
currently lose_sum: 95.54040145874023
time_elpased: 1.469
start validation test
0.663092783505
0.637765266349
0.75774415972
0.692597121625
0.662926608415
61.007
batch start
#iterations: 80
currently lose_sum: 95.42234122753143
time_elpased: 1.467
batch start
#iterations: 81
currently lose_sum: 95.24507558345795
time_elpased: 1.448
batch start
#iterations: 82
currently lose_sum: 95.43729400634766
time_elpased: 1.512
batch start
#iterations: 83
currently lose_sum: 95.31605863571167
time_elpased: 1.446
batch start
#iterations: 84
currently lose_sum: 95.76071226596832
time_elpased: 1.475
batch start
#iterations: 85
currently lose_sum: 95.46506422758102
time_elpased: 1.454
batch start
#iterations: 86
currently lose_sum: 95.48540395498276
time_elpased: 1.512
batch start
#iterations: 87
currently lose_sum: 95.36629861593246
time_elpased: 1.481
batch start
#iterations: 88
currently lose_sum: 95.57221233844757
time_elpased: 1.433
batch start
#iterations: 89
currently lose_sum: 95.77952599525452
time_elpased: 1.43
batch start
#iterations: 90
currently lose_sum: 95.04749637842178
time_elpased: 1.454
batch start
#iterations: 91
currently lose_sum: 95.68451601266861
time_elpased: 1.437
batch start
#iterations: 92
currently lose_sum: 95.36697274446487
time_elpased: 1.486
batch start
#iterations: 93
currently lose_sum: 94.90375745296478
time_elpased: 1.464
batch start
#iterations: 94
currently lose_sum: 95.33163863420486
time_elpased: 1.486
batch start
#iterations: 95
currently lose_sum: 95.80327826738358
time_elpased: 1.463
batch start
#iterations: 96
currently lose_sum: 95.50562471151352
time_elpased: 1.511
batch start
#iterations: 97
currently lose_sum: 95.00982773303986
time_elpased: 1.483
batch start
#iterations: 98
currently lose_sum: 95.68356204032898
time_elpased: 1.447
batch start
#iterations: 99
currently lose_sum: 95.59035229682922
time_elpased: 1.477
start validation test
0.664329896907
0.652924897414
0.704126788103
0.677559912854
0.664260027326
61.078
batch start
#iterations: 100
currently lose_sum: 95.38248604536057
time_elpased: 1.493
batch start
#iterations: 101
currently lose_sum: 95.36560022830963
time_elpased: 1.452
batch start
#iterations: 102
currently lose_sum: 95.12383741140366
time_elpased: 1.455
batch start
#iterations: 103
currently lose_sum: 95.51232433319092
time_elpased: 1.479
batch start
#iterations: 104
currently lose_sum: 94.7971260547638
time_elpased: 1.496
batch start
#iterations: 105
currently lose_sum: 95.75870275497437
time_elpased: 1.5
batch start
#iterations: 106
currently lose_sum: 95.44129091501236
time_elpased: 1.484
batch start
#iterations: 107
currently lose_sum: 95.63453751802444
time_elpased: 1.497
batch start
#iterations: 108
currently lose_sum: 95.40655201673508
time_elpased: 1.455
batch start
#iterations: 109
currently lose_sum: 95.32865124940872
time_elpased: 1.458
batch start
#iterations: 110
currently lose_sum: 95.38396370410919
time_elpased: 1.482
batch start
#iterations: 111
currently lose_sum: 95.42289465665817
time_elpased: 1.469
batch start
#iterations: 112
currently lose_sum: 95.24870318174362
time_elpased: 1.464
batch start
#iterations: 113
currently lose_sum: 94.5376723408699
time_elpased: 1.483
batch start
#iterations: 114
currently lose_sum: 95.09790259599686
time_elpased: 1.471
batch start
#iterations: 115
currently lose_sum: 95.35153496265411
time_elpased: 1.506
batch start
#iterations: 116
currently lose_sum: 95.19091713428497
time_elpased: 1.514
batch start
#iterations: 117
currently lose_sum: 95.15045535564423
time_elpased: 1.501
batch start
#iterations: 118
currently lose_sum: 95.06713837385178
time_elpased: 1.465
batch start
#iterations: 119
currently lose_sum: 95.13763701915741
time_elpased: 1.457
start validation test
0.671907216495
0.643052236258
0.775342183802
0.703028040872
0.671725620456
60.570
batch start
#iterations: 120
currently lose_sum: 94.7027678489685
time_elpased: 1.472
batch start
#iterations: 121
currently lose_sum: 95.52130579948425
time_elpased: 1.445
batch start
#iterations: 122
currently lose_sum: 95.32368856668472
time_elpased: 1.479
batch start
#iterations: 123
currently lose_sum: 95.09418296813965
time_elpased: 1.485
batch start
#iterations: 124
currently lose_sum: 94.95441114902496
time_elpased: 1.465
batch start
#iterations: 125
currently lose_sum: 94.84322947263718
time_elpased: 1.554
batch start
#iterations: 126
currently lose_sum: 95.1425067782402
time_elpased: 1.44
batch start
#iterations: 127
currently lose_sum: 95.56578022241592
time_elpased: 1.482
batch start
#iterations: 128
currently lose_sum: 95.20020484924316
time_elpased: 1.451
batch start
#iterations: 129
currently lose_sum: 94.92546778917313
time_elpased: 1.454
batch start
#iterations: 130
currently lose_sum: 95.19603008031845
time_elpased: 1.498
batch start
#iterations: 131
currently lose_sum: 95.45787632465363
time_elpased: 1.473
batch start
#iterations: 132
currently lose_sum: 95.32682406902313
time_elpased: 1.47
batch start
#iterations: 133
currently lose_sum: 94.90411275625229
time_elpased: 1.455
batch start
#iterations: 134
currently lose_sum: 94.8514153957367
time_elpased: 1.446
batch start
#iterations: 135
currently lose_sum: 95.21120113134384
time_elpased: 1.485
batch start
#iterations: 136
currently lose_sum: 95.0657628774643
time_elpased: 1.478
batch start
#iterations: 137
currently lose_sum: 95.2118747830391
time_elpased: 1.514
batch start
#iterations: 138
currently lose_sum: 95.0554563999176
time_elpased: 1.484
batch start
#iterations: 139
currently lose_sum: 95.16718965768814
time_elpased: 1.535
start validation test
0.664896907216
0.644267001615
0.739014099002
0.688395724488
0.664766783055
60.731
batch start
#iterations: 140
currently lose_sum: 95.10417211055756
time_elpased: 1.48
batch start
#iterations: 141
currently lose_sum: 95.04504668712616
time_elpased: 1.446
batch start
#iterations: 142
currently lose_sum: 95.1171914935112
time_elpased: 1.439
batch start
#iterations: 143
currently lose_sum: 95.07056927680969
time_elpased: 1.492
batch start
#iterations: 144
currently lose_sum: 94.94653052091599
time_elpased: 1.456
batch start
#iterations: 145
currently lose_sum: 95.38765978813171
time_elpased: 1.444
batch start
#iterations: 146
currently lose_sum: 95.07957118749619
time_elpased: 1.547
batch start
#iterations: 147
currently lose_sum: 95.02389723062515
time_elpased: 1.5
batch start
#iterations: 148
currently lose_sum: 95.04969745874405
time_elpased: 1.463
batch start
#iterations: 149
currently lose_sum: 95.09848880767822
time_elpased: 1.457
batch start
#iterations: 150
currently lose_sum: 95.01988577842712
time_elpased: 1.486
batch start
#iterations: 151
currently lose_sum: 95.15664386749268
time_elpased: 1.447
batch start
#iterations: 152
currently lose_sum: 94.83079320192337
time_elpased: 1.478
batch start
#iterations: 153
currently lose_sum: 95.39958131313324
time_elpased: 1.47
batch start
#iterations: 154
currently lose_sum: 94.69438207149506
time_elpased: 1.49
batch start
#iterations: 155
currently lose_sum: 95.02536082267761
time_elpased: 1.48
batch start
#iterations: 156
currently lose_sum: 95.22507083415985
time_elpased: 1.455
batch start
#iterations: 157
currently lose_sum: 94.95233261585236
time_elpased: 1.465
batch start
#iterations: 158
currently lose_sum: 95.22878223657608
time_elpased: 1.459
batch start
#iterations: 159
currently lose_sum: 95.29348409175873
time_elpased: 1.456
start validation test
0.670360824742
0.635260586319
0.80281980035
0.709278537982
0.67012827258
60.426
batch start
#iterations: 160
currently lose_sum: 95.13280212879181
time_elpased: 1.451
batch start
#iterations: 161
currently lose_sum: 95.08721005916595
time_elpased: 1.459
batch start
#iterations: 162
currently lose_sum: 94.99001228809357
time_elpased: 1.47
batch start
#iterations: 163
currently lose_sum: 94.70072537660599
time_elpased: 1.465
batch start
#iterations: 164
currently lose_sum: 95.23449689149857
time_elpased: 1.482
batch start
#iterations: 165
currently lose_sum: 94.80366456508636
time_elpased: 1.494
batch start
#iterations: 166
currently lose_sum: 94.8205543756485
time_elpased: 1.486
batch start
#iterations: 167
currently lose_sum: 95.33943128585815
time_elpased: 1.464
batch start
#iterations: 168
currently lose_sum: 94.82061183452606
time_elpased: 1.469
batch start
#iterations: 169
currently lose_sum: 95.02153652906418
time_elpased: 1.451
batch start
#iterations: 170
currently lose_sum: 94.42475086450577
time_elpased: 1.471
batch start
#iterations: 171
currently lose_sum: 94.89189183712006
time_elpased: 1.512
batch start
#iterations: 172
currently lose_sum: 94.96108758449554
time_elpased: 1.476
batch start
#iterations: 173
currently lose_sum: 94.79090642929077
time_elpased: 1.571
batch start
#iterations: 174
currently lose_sum: 94.74944347143173
time_elpased: 1.552
batch start
#iterations: 175
currently lose_sum: 94.92954784631729
time_elpased: 1.495
batch start
#iterations: 176
currently lose_sum: 95.14475017786026
time_elpased: 1.442
batch start
#iterations: 177
currently lose_sum: 95.0572372674942
time_elpased: 1.488
batch start
#iterations: 178
currently lose_sum: 94.55922693014145
time_elpased: 1.498
batch start
#iterations: 179
currently lose_sum: 94.92274361848831
time_elpased: 1.452
start validation test
0.667628865979
0.638976277527
0.773386847793
0.699785827358
0.66744319153
60.713
batch start
#iterations: 180
currently lose_sum: 94.98296427726746
time_elpased: 1.474
batch start
#iterations: 181
currently lose_sum: 95.02800333499908
time_elpased: 1.537
batch start
#iterations: 182
currently lose_sum: 94.8227447271347
time_elpased: 1.456
batch start
#iterations: 183
currently lose_sum: 94.87612324953079
time_elpased: 1.467
batch start
#iterations: 184
currently lose_sum: 95.0511137843132
time_elpased: 1.463
batch start
#iterations: 185
currently lose_sum: 94.9877056479454
time_elpased: 1.518
batch start
#iterations: 186
currently lose_sum: 94.55388194322586
time_elpased: 1.45
batch start
#iterations: 187
currently lose_sum: 94.59698402881622
time_elpased: 1.471
batch start
#iterations: 188
currently lose_sum: 94.61803823709488
time_elpased: 1.486
batch start
#iterations: 189
currently lose_sum: 95.03498238325119
time_elpased: 1.441
batch start
#iterations: 190
currently lose_sum: 94.44077861309052
time_elpased: 1.461
batch start
#iterations: 191
currently lose_sum: 95.0551648736
time_elpased: 1.495
batch start
#iterations: 192
currently lose_sum: 94.94714832305908
time_elpased: 1.467
batch start
#iterations: 193
currently lose_sum: 94.80897849798203
time_elpased: 1.476
batch start
#iterations: 194
currently lose_sum: 94.68397688865662
time_elpased: 1.513
batch start
#iterations: 195
currently lose_sum: 94.84202706813812
time_elpased: 1.465
batch start
#iterations: 196
currently lose_sum: 94.50440984964371
time_elpased: 1.537
batch start
#iterations: 197
currently lose_sum: 95.17438524961472
time_elpased: 1.463
batch start
#iterations: 198
currently lose_sum: 94.61868238449097
time_elpased: 1.455
batch start
#iterations: 199
currently lose_sum: 94.83592319488525
time_elpased: 1.492
start validation test
0.67206185567
0.642438651609
0.778635381291
0.704010421513
0.671874749408
60.455
batch start
#iterations: 200
currently lose_sum: 94.61380207538605
time_elpased: 1.504
batch start
#iterations: 201
currently lose_sum: 94.84960371255875
time_elpased: 1.463
batch start
#iterations: 202
currently lose_sum: 94.70887917280197
time_elpased: 1.48
batch start
#iterations: 203
currently lose_sum: 95.11200904846191
time_elpased: 1.467
batch start
#iterations: 204
currently lose_sum: 94.78748458623886
time_elpased: 1.455
batch start
#iterations: 205
currently lose_sum: 94.65033596754074
time_elpased: 1.464
batch start
#iterations: 206
currently lose_sum: 94.84067237377167
time_elpased: 1.538
batch start
#iterations: 207
currently lose_sum: 94.59483087062836
time_elpased: 1.475
batch start
#iterations: 208
currently lose_sum: 94.67001241445541
time_elpased: 1.446
batch start
#iterations: 209
currently lose_sum: 94.71243864297867
time_elpased: 1.466
batch start
#iterations: 210
currently lose_sum: 94.5889795422554
time_elpased: 1.448
batch start
#iterations: 211
currently lose_sum: 94.61926829814911
time_elpased: 1.505
batch start
#iterations: 212
currently lose_sum: 94.79716664552689
time_elpased: 1.478
batch start
#iterations: 213
currently lose_sum: 94.71400022506714
time_elpased: 1.481
batch start
#iterations: 214
currently lose_sum: 94.74152684211731
time_elpased: 1.495
batch start
#iterations: 215
currently lose_sum: 94.2489949464798
time_elpased: 1.486
batch start
#iterations: 216
currently lose_sum: 94.45196944475174
time_elpased: 1.514
batch start
#iterations: 217
currently lose_sum: 95.08033865690231
time_elpased: 1.533
batch start
#iterations: 218
currently lose_sum: 94.50225180387497
time_elpased: 1.462
batch start
#iterations: 219
currently lose_sum: 94.66347032785416
time_elpased: 1.474
start validation test
0.668711340206
0.638281775387
0.781414016672
0.702632674779
0.668513473274
60.382
batch start
#iterations: 220
currently lose_sum: 94.641477227211
time_elpased: 1.49
batch start
#iterations: 221
currently lose_sum: 94.64418417215347
time_elpased: 1.548
batch start
#iterations: 222
currently lose_sum: 94.75199162960052
time_elpased: 1.475
batch start
#iterations: 223
currently lose_sum: 94.80886965990067
time_elpased: 1.511
batch start
#iterations: 224
currently lose_sum: 94.89646518230438
time_elpased: 1.469
batch start
#iterations: 225
currently lose_sum: 94.85469251871109
time_elpased: 1.501
batch start
#iterations: 226
currently lose_sum: 94.6091428399086
time_elpased: 1.476
batch start
#iterations: 227
currently lose_sum: 94.48696208000183
time_elpased: 1.471
batch start
#iterations: 228
currently lose_sum: 94.85384774208069
time_elpased: 1.465
batch start
#iterations: 229
currently lose_sum: 94.70640075206757
time_elpased: 1.491
batch start
#iterations: 230
currently lose_sum: 94.87239569425583
time_elpased: 1.5
batch start
#iterations: 231
currently lose_sum: 94.74979811906815
time_elpased: 1.464
batch start
#iterations: 232
currently lose_sum: 94.69090563058853
time_elpased: 1.509
batch start
#iterations: 233
currently lose_sum: 94.49495601654053
time_elpased: 1.438
batch start
#iterations: 234
currently lose_sum: 94.52338659763336
time_elpased: 1.468
batch start
#iterations: 235
currently lose_sum: 94.88995862007141
time_elpased: 1.507
batch start
#iterations: 236
currently lose_sum: 94.56570190191269
time_elpased: 1.448
batch start
#iterations: 237
currently lose_sum: 94.92046999931335
time_elpased: 1.472
batch start
#iterations: 238
currently lose_sum: 94.68054157495499
time_elpased: 1.487
batch start
#iterations: 239
currently lose_sum: 94.41360062360764
time_elpased: 1.462
start validation test
0.667525773196
0.638891250744
0.773283935371
0.699692708818
0.66734009843
60.301
batch start
#iterations: 240
currently lose_sum: 94.83742797374725
time_elpased: 1.479
batch start
#iterations: 241
currently lose_sum: 95.22545874118805
time_elpased: 1.526
batch start
#iterations: 242
currently lose_sum: 94.13046878576279
time_elpased: 1.46
batch start
#iterations: 243
currently lose_sum: 95.09170639514923
time_elpased: 1.507
batch start
#iterations: 244
currently lose_sum: 94.74161666631699
time_elpased: 1.526
batch start
#iterations: 245
currently lose_sum: 94.35988467931747
time_elpased: 1.491
batch start
#iterations: 246
currently lose_sum: 94.81340032815933
time_elpased: 1.471
batch start
#iterations: 247
currently lose_sum: 94.70109415054321
time_elpased: 1.501
batch start
#iterations: 248
currently lose_sum: 94.88022035360336
time_elpased: 1.521
batch start
#iterations: 249
currently lose_sum: 94.64119130373001
time_elpased: 1.521
batch start
#iterations: 250
currently lose_sum: 94.67296409606934
time_elpased: 1.442
batch start
#iterations: 251
currently lose_sum: 94.84520131349564
time_elpased: 1.505
batch start
#iterations: 252
currently lose_sum: 94.42572671175003
time_elpased: 1.484
batch start
#iterations: 253
currently lose_sum: 94.56726241111755
time_elpased: 1.467
batch start
#iterations: 254
currently lose_sum: 94.51049393415451
time_elpased: 1.467
batch start
#iterations: 255
currently lose_sum: 94.93791973590851
time_elpased: 1.484
batch start
#iterations: 256
currently lose_sum: 93.96888417005539
time_elpased: 1.494
batch start
#iterations: 257
currently lose_sum: 94.9119194149971
time_elpased: 1.499
batch start
#iterations: 258
currently lose_sum: 94.36260837316513
time_elpased: 1.469
batch start
#iterations: 259
currently lose_sum: 94.45261114835739
time_elpased: 1.523
start validation test
0.660721649485
0.653872582703
0.685499639807
0.669312700965
0.660678147901
60.965
batch start
#iterations: 260
currently lose_sum: 94.67375373840332
time_elpased: 1.488
batch start
#iterations: 261
currently lose_sum: 94.57102119922638
time_elpased: 1.502
batch start
#iterations: 262
currently lose_sum: 94.48797965049744
time_elpased: 1.464
batch start
#iterations: 263
currently lose_sum: 94.82145488262177
time_elpased: 1.491
batch start
#iterations: 264
currently lose_sum: 94.67857348918915
time_elpased: 1.506
batch start
#iterations: 265
currently lose_sum: 94.87318432331085
time_elpased: 1.435
batch start
#iterations: 266
currently lose_sum: 94.64237761497498
time_elpased: 1.469
batch start
#iterations: 267
currently lose_sum: 94.90593707561493
time_elpased: 1.495
batch start
#iterations: 268
currently lose_sum: 94.4866191148758
time_elpased: 1.44
batch start
#iterations: 269
currently lose_sum: 94.86054992675781
time_elpased: 1.548
batch start
#iterations: 270
currently lose_sum: 94.81594878435135
time_elpased: 1.422
batch start
#iterations: 271
currently lose_sum: 94.38683193922043
time_elpased: 1.483
batch start
#iterations: 272
currently lose_sum: 94.66418886184692
time_elpased: 1.456
batch start
#iterations: 273
currently lose_sum: 94.51730382442474
time_elpased: 1.506
batch start
#iterations: 274
currently lose_sum: 94.94563835859299
time_elpased: 1.458
batch start
#iterations: 275
currently lose_sum: 94.5252456665039
time_elpased: 1.46
batch start
#iterations: 276
currently lose_sum: 94.45764315128326
time_elpased: 1.488
batch start
#iterations: 277
currently lose_sum: 94.68027871847153
time_elpased: 1.535
batch start
#iterations: 278
currently lose_sum: 94.68890011310577
time_elpased: 1.541
batch start
#iterations: 279
currently lose_sum: 94.49309569597244
time_elpased: 1.565
start validation test
0.667319587629
0.647154324885
0.738396624473
0.689771197847
0.667194800928
60.461
batch start
#iterations: 280
currently lose_sum: 94.9075208902359
time_elpased: 1.453
batch start
#iterations: 281
currently lose_sum: 94.35759001970291
time_elpased: 1.513
batch start
#iterations: 282
currently lose_sum: 94.48226934671402
time_elpased: 1.441
batch start
#iterations: 283
currently lose_sum: 94.32997369766235
time_elpased: 1.463
batch start
#iterations: 284
currently lose_sum: 94.6110897064209
time_elpased: 1.451
batch start
#iterations: 285
currently lose_sum: 94.21179485321045
time_elpased: 1.505
batch start
#iterations: 286
currently lose_sum: 94.56957072019577
time_elpased: 1.45
batch start
#iterations: 287
currently lose_sum: 94.25661385059357
time_elpased: 1.458
batch start
#iterations: 288
currently lose_sum: 94.7976211309433
time_elpased: 1.481
batch start
#iterations: 289
currently lose_sum: 94.37012892961502
time_elpased: 1.445
batch start
#iterations: 290
currently lose_sum: 94.308822453022
time_elpased: 1.459
batch start
#iterations: 291
currently lose_sum: 94.12700760364532
time_elpased: 1.498
batch start
#iterations: 292
currently lose_sum: 94.54616713523865
time_elpased: 1.481
batch start
#iterations: 293
currently lose_sum: 94.24611121416092
time_elpased: 1.488
batch start
#iterations: 294
currently lose_sum: 94.10540509223938
time_elpased: 1.449
batch start
#iterations: 295
currently lose_sum: 94.1799972653389
time_elpased: 1.487
batch start
#iterations: 296
currently lose_sum: 94.0197988152504
time_elpased: 1.471
batch start
#iterations: 297
currently lose_sum: 94.5575960278511
time_elpased: 1.478
batch start
#iterations: 298
currently lose_sum: 94.3960725069046
time_elpased: 1.439
batch start
#iterations: 299
currently lose_sum: 94.34911459684372
time_elpased: 1.444
start validation test
0.667577319588
0.632500810898
0.802716887928
0.707515080049
0.667340061232
60.313
batch start
#iterations: 300
currently lose_sum: 94.29398119449615
time_elpased: 1.448
batch start
#iterations: 301
currently lose_sum: 94.15633887052536
time_elpased: 1.46
batch start
#iterations: 302
currently lose_sum: 94.62678861618042
time_elpased: 1.447
batch start
#iterations: 303
currently lose_sum: 94.38157898187637
time_elpased: 1.466
batch start
#iterations: 304
currently lose_sum: 94.50839501619339
time_elpased: 1.505
batch start
#iterations: 305
currently lose_sum: 94.38065952062607
time_elpased: 1.498
batch start
#iterations: 306
currently lose_sum: 94.39356797933578
time_elpased: 1.455
batch start
#iterations: 307
currently lose_sum: 94.56383389234543
time_elpased: 1.516
batch start
#iterations: 308
currently lose_sum: 94.39658999443054
time_elpased: 1.45
batch start
#iterations: 309
currently lose_sum: 94.59961402416229
time_elpased: 1.499
batch start
#iterations: 310
currently lose_sum: 94.80188119411469
time_elpased: 1.505
batch start
#iterations: 311
currently lose_sum: 94.09187752008438
time_elpased: 1.461
batch start
#iterations: 312
currently lose_sum: 94.4046373963356
time_elpased: 1.499
batch start
#iterations: 313
currently lose_sum: 93.98871803283691
time_elpased: 1.48
batch start
#iterations: 314
currently lose_sum: 94.1238352060318
time_elpased: 1.504
batch start
#iterations: 315
currently lose_sum: 94.12714862823486
time_elpased: 1.434
batch start
#iterations: 316
currently lose_sum: 94.21674418449402
time_elpased: 1.444
batch start
#iterations: 317
currently lose_sum: 94.4492439031601
time_elpased: 1.471
batch start
#iterations: 318
currently lose_sum: 94.38548094034195
time_elpased: 1.466
batch start
#iterations: 319
currently lose_sum: 94.12181502580643
time_elpased: 1.466
start validation test
0.654432989691
0.650514536917
0.670062776577
0.660143972422
0.654405549189
60.851
batch start
#iterations: 320
currently lose_sum: 94.6085352897644
time_elpased: 1.442
batch start
#iterations: 321
currently lose_sum: 94.07692468166351
time_elpased: 1.482
batch start
#iterations: 322
currently lose_sum: 94.7649832367897
time_elpased: 1.48
batch start
#iterations: 323
currently lose_sum: 94.52860522270203
time_elpased: 1.459
batch start
#iterations: 324
currently lose_sum: 94.50764924287796
time_elpased: 1.428
batch start
#iterations: 325
currently lose_sum: 94.61945223808289
time_elpased: 1.461
batch start
#iterations: 326
currently lose_sum: 94.67466562986374
time_elpased: 1.479
batch start
#iterations: 327
currently lose_sum: 94.13668614625931
time_elpased: 1.488
batch start
#iterations: 328
currently lose_sum: 94.45924776792526
time_elpased: 1.479
batch start
#iterations: 329
currently lose_sum: 94.13721483945847
time_elpased: 1.459
batch start
#iterations: 330
currently lose_sum: 94.50308042764664
time_elpased: 1.494
batch start
#iterations: 331
currently lose_sum: 94.7466629743576
time_elpased: 1.445
batch start
#iterations: 332
currently lose_sum: 94.44355714321136
time_elpased: 1.53
batch start
#iterations: 333
currently lose_sum: 94.24752378463745
time_elpased: 1.467
batch start
#iterations: 334
currently lose_sum: 94.80336838960648
time_elpased: 1.431
batch start
#iterations: 335
currently lose_sum: 94.1722846031189
time_elpased: 1.448
batch start
#iterations: 336
currently lose_sum: 94.5197120308876
time_elpased: 1.484
batch start
#iterations: 337
currently lose_sum: 94.48476219177246
time_elpased: 1.458
batch start
#iterations: 338
currently lose_sum: 94.22895139455795
time_elpased: 1.476
batch start
#iterations: 339
currently lose_sum: 94.4192276597023
time_elpased: 1.496
start validation test
0.66824742268
0.650739685748
0.72882576927
0.687572815534
0.668141068049
60.675
batch start
#iterations: 340
currently lose_sum: 94.06344431638718
time_elpased: 1.475
batch start
#iterations: 341
currently lose_sum: 94.92680555582047
time_elpased: 1.456
batch start
#iterations: 342
currently lose_sum: 94.27461665868759
time_elpased: 1.432
batch start
#iterations: 343
currently lose_sum: 94.18352818489075
time_elpased: 1.44
batch start
#iterations: 344
currently lose_sum: 94.27579474449158
time_elpased: 1.467
batch start
#iterations: 345
currently lose_sum: 94.49509489536285
time_elpased: 1.459
batch start
#iterations: 346
currently lose_sum: 93.99475502967834
time_elpased: 1.469
batch start
#iterations: 347
currently lose_sum: 94.0342932343483
time_elpased: 1.456
batch start
#iterations: 348
currently lose_sum: 94.56525260210037
time_elpased: 1.462
batch start
#iterations: 349
currently lose_sum: 94.45883232355118
time_elpased: 1.441
batch start
#iterations: 350
currently lose_sum: 94.77995932102203
time_elpased: 1.475
batch start
#iterations: 351
currently lose_sum: 94.68443065881729
time_elpased: 1.461
batch start
#iterations: 352
currently lose_sum: 93.97996807098389
time_elpased: 1.459
batch start
#iterations: 353
currently lose_sum: 94.42113280296326
time_elpased: 1.509
batch start
#iterations: 354
currently lose_sum: 94.12563467025757
time_elpased: 1.449
batch start
#iterations: 355
currently lose_sum: 94.0616347193718
time_elpased: 1.537
batch start
#iterations: 356
currently lose_sum: 94.49416691064835
time_elpased: 1.476
batch start
#iterations: 357
currently lose_sum: 94.06951570510864
time_elpased: 1.48
batch start
#iterations: 358
currently lose_sum: 94.27280032634735
time_elpased: 1.553
batch start
#iterations: 359
currently lose_sum: 94.20041078329086
time_elpased: 1.479
start validation test
0.670051546392
0.64422407794
0.762169393846
0.698251072456
0.669889819302
60.493
batch start
#iterations: 360
currently lose_sum: 94.6679762005806
time_elpased: 1.455
batch start
#iterations: 361
currently lose_sum: 94.51005697250366
time_elpased: 1.454
batch start
#iterations: 362
currently lose_sum: 94.64680325984955
time_elpased: 1.445
batch start
#iterations: 363
currently lose_sum: 94.20607650279999
time_elpased: 1.45
batch start
#iterations: 364
currently lose_sum: 94.50600552558899
time_elpased: 1.464
batch start
#iterations: 365
currently lose_sum: 94.42563480138779
time_elpased: 1.495
batch start
#iterations: 366
currently lose_sum: 94.58983546495438
time_elpased: 1.456
batch start
#iterations: 367
currently lose_sum: 94.25592583417892
time_elpased: 1.432
batch start
#iterations: 368
currently lose_sum: 94.40961891412735
time_elpased: 1.532
batch start
#iterations: 369
currently lose_sum: 94.50001603364944
time_elpased: 1.424
batch start
#iterations: 370
currently lose_sum: 94.40490400791168
time_elpased: 1.474
batch start
#iterations: 371
currently lose_sum: 94.1279268860817
time_elpased: 1.476
batch start
#iterations: 372
currently lose_sum: 94.2743216753006
time_elpased: 1.509
batch start
#iterations: 373
currently lose_sum: 94.46880757808685
time_elpased: 1.469
batch start
#iterations: 374
currently lose_sum: 94.35898119211197
time_elpased: 1.459
batch start
#iterations: 375
currently lose_sum: 94.36810278892517
time_elpased: 1.452
batch start
#iterations: 376
currently lose_sum: 94.63819938898087
time_elpased: 1.464
batch start
#iterations: 377
currently lose_sum: 94.6982262134552
time_elpased: 1.444
batch start
#iterations: 378
currently lose_sum: 94.14445269107819
time_elpased: 1.459
batch start
#iterations: 379
currently lose_sum: 94.46532821655273
time_elpased: 1.452
start validation test
0.668865979381
0.646577049764
0.747452917567
0.693365155131
0.66872800789
60.274
batch start
#iterations: 380
currently lose_sum: 94.72685807943344
time_elpased: 1.462
batch start
#iterations: 381
currently lose_sum: 94.48792690038681
time_elpased: 1.492
batch start
#iterations: 382
currently lose_sum: 94.06176674365997
time_elpased: 1.471
batch start
#iterations: 383
currently lose_sum: 93.94435405731201
time_elpased: 1.492
batch start
#iterations: 384
currently lose_sum: 94.25616538524628
time_elpased: 1.456
batch start
#iterations: 385
currently lose_sum: 94.54584956169128
time_elpased: 1.44
batch start
#iterations: 386
currently lose_sum: 94.13078624010086
time_elpased: 1.487
batch start
#iterations: 387
currently lose_sum: 94.25347715616226
time_elpased: 1.505
batch start
#iterations: 388
currently lose_sum: 94.67620360851288
time_elpased: 1.558
batch start
#iterations: 389
currently lose_sum: 94.58671814203262
time_elpased: 1.499
batch start
#iterations: 390
currently lose_sum: 94.36334270238876
time_elpased: 1.503
batch start
#iterations: 391
currently lose_sum: 94.13420879840851
time_elpased: 1.497
batch start
#iterations: 392
currently lose_sum: 94.22427135705948
time_elpased: 1.46
batch start
#iterations: 393
currently lose_sum: 94.40717822313309
time_elpased: 1.496
batch start
#iterations: 394
currently lose_sum: 94.28750610351562
time_elpased: 1.44
batch start
#iterations: 395
currently lose_sum: 94.21246862411499
time_elpased: 1.454
batch start
#iterations: 396
currently lose_sum: 94.07855945825577
time_elpased: 1.492
batch start
#iterations: 397
currently lose_sum: 94.53593248128891
time_elpased: 1.517
batch start
#iterations: 398
currently lose_sum: 94.10960507392883
time_elpased: 1.454
batch start
#iterations: 399
currently lose_sum: 93.69054365158081
time_elpased: 1.455
start validation test
0.658453608247
0.659741602067
0.656889986621
0.658312706271
0.658456353426
60.815
acc: 0.663
pre: 0.642
rec: 0.741
F1: 0.688
auc: 0.700
