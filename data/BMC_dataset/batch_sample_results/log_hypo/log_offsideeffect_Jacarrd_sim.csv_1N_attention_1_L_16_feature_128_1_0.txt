start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.32784700393677
time_elpased: 1.873
batch start
#iterations: 1
currently lose_sum: 99.86042481660843
time_elpased: 1.831
batch start
#iterations: 2
currently lose_sum: 99.49750715494156
time_elpased: 1.824
batch start
#iterations: 3
currently lose_sum: 99.11557823419571
time_elpased: 1.797
batch start
#iterations: 4
currently lose_sum: 99.05822640657425
time_elpased: 1.865
batch start
#iterations: 5
currently lose_sum: 98.6524932384491
time_elpased: 1.841
batch start
#iterations: 6
currently lose_sum: 98.69582778215408
time_elpased: 1.817
batch start
#iterations: 7
currently lose_sum: 98.41158348321915
time_elpased: 1.826
batch start
#iterations: 8
currently lose_sum: 98.00689178705215
time_elpased: 1.849
batch start
#iterations: 9
currently lose_sum: 98.19305491447449
time_elpased: 1.795
batch start
#iterations: 10
currently lose_sum: 98.07719886302948
time_elpased: 1.839
batch start
#iterations: 11
currently lose_sum: 97.95790195465088
time_elpased: 1.838
batch start
#iterations: 12
currently lose_sum: 97.94027763605118
time_elpased: 1.818
batch start
#iterations: 13
currently lose_sum: 97.51773595809937
time_elpased: 1.834
batch start
#iterations: 14
currently lose_sum: 97.90275591611862
time_elpased: 1.836
batch start
#iterations: 15
currently lose_sum: 97.29037123918533
time_elpased: 1.888
batch start
#iterations: 16
currently lose_sum: 97.46899765729904
time_elpased: 1.809
batch start
#iterations: 17
currently lose_sum: 97.39899569749832
time_elpased: 1.825
batch start
#iterations: 18
currently lose_sum: 97.09264004230499
time_elpased: 1.845
batch start
#iterations: 19
currently lose_sum: 97.48647475242615
time_elpased: 1.876
start validation test
0.635618556701
0.656946420104
0.570340640115
0.610587781634
0.635733162152
62.865
batch start
#iterations: 20
currently lose_sum: 97.38785874843597
time_elpased: 1.821
batch start
#iterations: 21
currently lose_sum: 96.58314168453217
time_elpased: 1.847
batch start
#iterations: 22
currently lose_sum: 96.97702759504318
time_elpased: 1.858
batch start
#iterations: 23
currently lose_sum: 96.86021453142166
time_elpased: 1.815
batch start
#iterations: 24
currently lose_sum: 96.63661724328995
time_elpased: 1.835
batch start
#iterations: 25
currently lose_sum: 96.5050705075264
time_elpased: 1.804
batch start
#iterations: 26
currently lose_sum: 96.89691913127899
time_elpased: 1.838
batch start
#iterations: 27
currently lose_sum: 96.64381605386734
time_elpased: 1.805
batch start
#iterations: 28
currently lose_sum: 96.57128417491913
time_elpased: 1.836
batch start
#iterations: 29
currently lose_sum: 96.50215572118759
time_elpased: 1.842
batch start
#iterations: 30
currently lose_sum: 96.63729053735733
time_elpased: 1.797
batch start
#iterations: 31
currently lose_sum: 96.5081816315651
time_elpased: 1.838
batch start
#iterations: 32
currently lose_sum: 96.26997244358063
time_elpased: 1.83
batch start
#iterations: 33
currently lose_sum: 96.25455111265182
time_elpased: 1.822
batch start
#iterations: 34
currently lose_sum: 96.43758153915405
time_elpased: 1.819
batch start
#iterations: 35
currently lose_sum: 96.4596876502037
time_elpased: 1.799
batch start
#iterations: 36
currently lose_sum: 96.31078201532364
time_elpased: 1.789
batch start
#iterations: 37
currently lose_sum: 96.08815807104111
time_elpased: 1.855
batch start
#iterations: 38
currently lose_sum: 96.15092039108276
time_elpased: 1.821
batch start
#iterations: 39
currently lose_sum: 96.28964293003082
time_elpased: 1.807
start validation test
0.663195876289
0.638959224657
0.753113100751
0.691355692017
0.663038012732
61.290
batch start
#iterations: 40
currently lose_sum: 96.61344867944717
time_elpased: 1.811
batch start
#iterations: 41
currently lose_sum: 96.52441734075546
time_elpased: 1.854
batch start
#iterations: 42
currently lose_sum: 96.0995859503746
time_elpased: 1.809
batch start
#iterations: 43
currently lose_sum: 95.6539775133133
time_elpased: 1.8
batch start
#iterations: 44
currently lose_sum: 96.01751410961151
time_elpased: 1.822
batch start
#iterations: 45
currently lose_sum: 95.89565545320511
time_elpased: 1.821
batch start
#iterations: 46
currently lose_sum: 96.04978346824646
time_elpased: 1.826
batch start
#iterations: 47
currently lose_sum: 96.0094946026802
time_elpased: 1.817
batch start
#iterations: 48
currently lose_sum: 95.92489612102509
time_elpased: 1.824
batch start
#iterations: 49
currently lose_sum: 96.28862297534943
time_elpased: 1.818
batch start
#iterations: 50
currently lose_sum: 95.88868910074234
time_elpased: 1.843
batch start
#iterations: 51
currently lose_sum: 95.89286923408508
time_elpased: 1.82
batch start
#iterations: 52
currently lose_sum: 96.00609171390533
time_elpased: 1.857
batch start
#iterations: 53
currently lose_sum: 95.86846214532852
time_elpased: 1.847
batch start
#iterations: 54
currently lose_sum: 96.10339897871017
time_elpased: 1.811
batch start
#iterations: 55
currently lose_sum: 95.82100290060043
time_elpased: 1.853
batch start
#iterations: 56
currently lose_sum: 95.6747294664383
time_elpased: 1.782
batch start
#iterations: 57
currently lose_sum: 95.78340888023376
time_elpased: 1.836
batch start
#iterations: 58
currently lose_sum: 96.17994546890259
time_elpased: 1.834
batch start
#iterations: 59
currently lose_sum: 96.04935240745544
time_elpased: 1.856
start validation test
0.655206185567
0.657086532476
0.651744365545
0.654404546629
0.655212263326
61.566
batch start
#iterations: 60
currently lose_sum: 96.086001932621
time_elpased: 1.832
batch start
#iterations: 61
currently lose_sum: 96.09522688388824
time_elpased: 1.809
batch start
#iterations: 62
currently lose_sum: 95.80333417654037
time_elpased: 1.818
batch start
#iterations: 63
currently lose_sum: 95.78691178560257
time_elpased: 1.836
batch start
#iterations: 64
currently lose_sum: 96.04711908102036
time_elpased: 1.853
batch start
#iterations: 65
currently lose_sum: 95.29146230220795
time_elpased: 1.807
batch start
#iterations: 66
currently lose_sum: 96.0488588809967
time_elpased: 1.835
batch start
#iterations: 67
currently lose_sum: 96.02620482444763
time_elpased: 1.803
batch start
#iterations: 68
currently lose_sum: 95.36368703842163
time_elpased: 1.805
batch start
#iterations: 69
currently lose_sum: 95.40032416582108
time_elpased: 1.836
batch start
#iterations: 70
currently lose_sum: 95.30805391073227
time_elpased: 1.83
batch start
#iterations: 71
currently lose_sum: 95.43116575479507
time_elpased: 1.863
batch start
#iterations: 72
currently lose_sum: 95.29090386629105
time_elpased: 1.814
batch start
#iterations: 73
currently lose_sum: 95.2197060585022
time_elpased: 1.816
batch start
#iterations: 74
currently lose_sum: 95.29540580511093
time_elpased: 1.781
batch start
#iterations: 75
currently lose_sum: 95.3189058303833
time_elpased: 1.865
batch start
#iterations: 76
currently lose_sum: 95.68921649456024
time_elpased: 1.85
batch start
#iterations: 77
currently lose_sum: 95.30698734521866
time_elpased: 1.782
batch start
#iterations: 78
currently lose_sum: 95.63314366340637
time_elpased: 1.833
batch start
#iterations: 79
currently lose_sum: 95.18548691272736
time_elpased: 1.87
start validation test
0.616701030928
0.66327845383
0.476793248945
0.554783858221
0.616946660618
62.295
batch start
#iterations: 80
currently lose_sum: 95.59036070108414
time_elpased: 1.81
batch start
#iterations: 81
currently lose_sum: 95.62427079677582
time_elpased: 1.79
batch start
#iterations: 82
currently lose_sum: 95.53661900758743
time_elpased: 1.814
batch start
#iterations: 83
currently lose_sum: 95.52976274490356
time_elpased: 1.824
batch start
#iterations: 84
currently lose_sum: 95.58097195625305
time_elpased: 1.798
batch start
#iterations: 85
currently lose_sum: 95.65605038404465
time_elpased: 1.781
batch start
#iterations: 86
currently lose_sum: 95.69745165109634
time_elpased: 1.813
batch start
#iterations: 87
currently lose_sum: 95.35689562559128
time_elpased: 1.803
batch start
#iterations: 88
currently lose_sum: 95.38309848308563
time_elpased: 1.815
batch start
#iterations: 89
currently lose_sum: 95.43147391080856
time_elpased: 1.793
batch start
#iterations: 90
currently lose_sum: 94.95948696136475
time_elpased: 1.859
batch start
#iterations: 91
currently lose_sum: 95.48811930418015
time_elpased: 1.812
batch start
#iterations: 92
currently lose_sum: 95.40309792757034
time_elpased: 1.848
batch start
#iterations: 93
currently lose_sum: 95.25192838907242
time_elpased: 1.876
batch start
#iterations: 94
currently lose_sum: 95.14070004224777
time_elpased: 1.814
batch start
#iterations: 95
currently lose_sum: 95.43384742736816
time_elpased: 1.807
batch start
#iterations: 96
currently lose_sum: 95.51381486654282
time_elpased: 1.811
batch start
#iterations: 97
currently lose_sum: 95.3014126420021
time_elpased: 1.814
batch start
#iterations: 98
currently lose_sum: 95.85579174757004
time_elpased: 1.816
batch start
#iterations: 99
currently lose_sum: 95.59344482421875
time_elpased: 1.821
start validation test
0.667989690722
0.648450244698
0.736338376042
0.689605320226
0.667869694063
60.919
batch start
#iterations: 100
currently lose_sum: 95.4644181728363
time_elpased: 1.82
batch start
#iterations: 101
currently lose_sum: 95.0224381685257
time_elpased: 1.895
batch start
#iterations: 102
currently lose_sum: 95.21514958143234
time_elpased: 1.85
batch start
#iterations: 103
currently lose_sum: 95.04069763422012
time_elpased: 1.824
batch start
#iterations: 104
currently lose_sum: 94.73084783554077
time_elpased: 1.84
batch start
#iterations: 105
currently lose_sum: 95.34867423772812
time_elpased: 1.813
batch start
#iterations: 106
currently lose_sum: 95.3488689661026
time_elpased: 1.828
batch start
#iterations: 107
currently lose_sum: 95.12052822113037
time_elpased: 1.804
batch start
#iterations: 108
currently lose_sum: 95.2918530702591
time_elpased: 1.846
batch start
#iterations: 109
currently lose_sum: 95.10862743854523
time_elpased: 1.814
batch start
#iterations: 110
currently lose_sum: 94.99402898550034
time_elpased: 1.822
batch start
#iterations: 111
currently lose_sum: 95.21283966302872
time_elpased: 1.854
batch start
#iterations: 112
currently lose_sum: 95.57329297065735
time_elpased: 1.838
batch start
#iterations: 113
currently lose_sum: 95.07369935512543
time_elpased: 1.826
batch start
#iterations: 114
currently lose_sum: 95.42560374736786
time_elpased: 1.836
batch start
#iterations: 115
currently lose_sum: 95.36009657382965
time_elpased: 1.831
batch start
#iterations: 116
currently lose_sum: 94.90619868040085
time_elpased: 1.819
batch start
#iterations: 117
currently lose_sum: 95.20018655061722
time_elpased: 1.821
batch start
#iterations: 118
currently lose_sum: 95.08223676681519
time_elpased: 1.86
batch start
#iterations: 119
currently lose_sum: 95.26930099725723
time_elpased: 1.812
start validation test
0.667216494845
0.6294355799
0.815992590306
0.710674912611
0.666955295463
60.582
batch start
#iterations: 120
currently lose_sum: 95.41629213094711
time_elpased: 1.801
batch start
#iterations: 121
currently lose_sum: 95.12698674201965
time_elpased: 1.872
batch start
#iterations: 122
currently lose_sum: 95.45592999458313
time_elpased: 1.877
batch start
#iterations: 123
currently lose_sum: 94.78862136602402
time_elpased: 1.834
batch start
#iterations: 124
currently lose_sum: 95.65874791145325
time_elpased: 1.859
batch start
#iterations: 125
currently lose_sum: 95.05855423212051
time_elpased: 1.833
batch start
#iterations: 126
currently lose_sum: 95.25650697946548
time_elpased: 1.817
batch start
#iterations: 127
currently lose_sum: 95.16277450323105
time_elpased: 1.829
batch start
#iterations: 128
currently lose_sum: 95.21435153484344
time_elpased: 1.846
batch start
#iterations: 129
currently lose_sum: 95.08507388830185
time_elpased: 1.83
batch start
#iterations: 130
currently lose_sum: 94.94407206773758
time_elpased: 1.782
batch start
#iterations: 131
currently lose_sum: 95.03506290912628
time_elpased: 1.886
batch start
#iterations: 132
currently lose_sum: 95.07420539855957
time_elpased: 1.832
batch start
#iterations: 133
currently lose_sum: 95.6241945028305
time_elpased: 1.837
batch start
#iterations: 134
currently lose_sum: 94.94938451051712
time_elpased: 1.831
batch start
#iterations: 135
currently lose_sum: 94.94676566123962
time_elpased: 1.86
batch start
#iterations: 136
currently lose_sum: 95.43780952692032
time_elpased: 1.802
batch start
#iterations: 137
currently lose_sum: 94.8252182006836
time_elpased: 1.784
batch start
#iterations: 138
currently lose_sum: 95.18930453062057
time_elpased: 1.801
batch start
#iterations: 139
currently lose_sum: 95.34302186965942
time_elpased: 1.789
start validation test
0.667010309278
0.650078333794
0.725944221468
0.685919875535
0.666906841706
60.829
batch start
#iterations: 140
currently lose_sum: 95.25049078464508
time_elpased: 1.844
batch start
#iterations: 141
currently lose_sum: 95.01029348373413
time_elpased: 1.802
batch start
#iterations: 142
currently lose_sum: 95.13409352302551
time_elpased: 1.818
batch start
#iterations: 143
currently lose_sum: 95.07372504472733
time_elpased: 1.835
batch start
#iterations: 144
currently lose_sum: 94.74148881435394
time_elpased: 1.811
batch start
#iterations: 145
currently lose_sum: 95.07441383600235
time_elpased: 1.823
batch start
#iterations: 146
currently lose_sum: 94.86109215021133
time_elpased: 1.836
batch start
#iterations: 147
currently lose_sum: 95.56476020812988
time_elpased: 1.864
batch start
#iterations: 148
currently lose_sum: 94.87748962640762
time_elpased: 1.874
batch start
#iterations: 149
currently lose_sum: 95.21363562345505
time_elpased: 1.808
batch start
#iterations: 150
currently lose_sum: 95.1735942363739
time_elpased: 1.811
batch start
#iterations: 151
currently lose_sum: 95.09594565629959
time_elpased: 1.827
batch start
#iterations: 152
currently lose_sum: 95.14878016710281
time_elpased: 1.837
batch start
#iterations: 153
currently lose_sum: 95.00776559114456
time_elpased: 1.812
batch start
#iterations: 154
currently lose_sum: 95.26605069637299
time_elpased: 1.884
batch start
#iterations: 155
currently lose_sum: 95.15867441892624
time_elpased: 1.817
batch start
#iterations: 156
currently lose_sum: 95.01466155052185
time_elpased: 1.802
batch start
#iterations: 157
currently lose_sum: 94.92445665597916
time_elpased: 1.803
batch start
#iterations: 158
currently lose_sum: 94.8225439786911
time_elpased: 1.836
batch start
#iterations: 159
currently lose_sum: 94.9240751862526
time_elpased: 1.831
start validation test
0.663711340206
0.642506471481
0.740763610168
0.688145315488
0.663576063062
60.878
batch start
#iterations: 160
currently lose_sum: 94.81968158483505
time_elpased: 1.846
batch start
#iterations: 161
currently lose_sum: 95.10890132188797
time_elpased: 1.848
batch start
#iterations: 162
currently lose_sum: 94.93082863092422
time_elpased: 1.851
batch start
#iterations: 163
currently lose_sum: 94.65205800533295
time_elpased: 1.833
batch start
#iterations: 164
currently lose_sum: 94.98039895296097
time_elpased: 1.836
batch start
#iterations: 165
currently lose_sum: 94.8537746667862
time_elpased: 1.865
batch start
#iterations: 166
currently lose_sum: 94.957266330719
time_elpased: 1.824
batch start
#iterations: 167
currently lose_sum: 94.75927144289017
time_elpased: 1.868
batch start
#iterations: 168
currently lose_sum: 94.79495799541473
time_elpased: 1.821
batch start
#iterations: 169
currently lose_sum: 94.78758174180984
time_elpased: 1.874
batch start
#iterations: 170
currently lose_sum: 95.04642415046692
time_elpased: 1.824
batch start
#iterations: 171
currently lose_sum: 94.85551273822784
time_elpased: 1.8
batch start
#iterations: 172
currently lose_sum: 94.73882031440735
time_elpased: 1.911
batch start
#iterations: 173
currently lose_sum: 94.67049932479858
time_elpased: 1.84
batch start
#iterations: 174
currently lose_sum: 94.88531774282455
time_elpased: 1.866
batch start
#iterations: 175
currently lose_sum: 95.01495951414108
time_elpased: 1.841
batch start
#iterations: 176
currently lose_sum: 94.95075035095215
time_elpased: 1.827
batch start
#iterations: 177
currently lose_sum: 94.79665237665176
time_elpased: 1.833
batch start
#iterations: 178
currently lose_sum: 94.8021873831749
time_elpased: 1.84
batch start
#iterations: 179
currently lose_sum: 94.66138434410095
time_elpased: 1.795
start validation test
0.661649484536
0.657131466162
0.678501595143
0.66764556962
0.661619898057
60.681
batch start
#iterations: 180
currently lose_sum: 95.02826941013336
time_elpased: 1.801
batch start
#iterations: 181
currently lose_sum: 94.34069818258286
time_elpased: 1.824
batch start
#iterations: 182
currently lose_sum: 95.11760586500168
time_elpased: 1.813
batch start
#iterations: 183
currently lose_sum: 94.6715521812439
time_elpased: 1.838
batch start
#iterations: 184
currently lose_sum: 94.67204016447067
time_elpased: 1.831
batch start
#iterations: 185
currently lose_sum: 94.39727425575256
time_elpased: 1.842
batch start
#iterations: 186
currently lose_sum: 94.44954866170883
time_elpased: 1.79
batch start
#iterations: 187
currently lose_sum: 94.53214931488037
time_elpased: 1.801
batch start
#iterations: 188
currently lose_sum: 94.72278815507889
time_elpased: 1.848
batch start
#iterations: 189
currently lose_sum: 94.85522246360779
time_elpased: 1.829
batch start
#iterations: 190
currently lose_sum: 94.6960551738739
time_elpased: 1.826
batch start
#iterations: 191
currently lose_sum: 94.74259352684021
time_elpased: 1.812
batch start
#iterations: 192
currently lose_sum: 94.2294089794159
time_elpased: 1.812
batch start
#iterations: 193
currently lose_sum: 94.59453356266022
time_elpased: 1.804
batch start
#iterations: 194
currently lose_sum: 94.49589878320694
time_elpased: 1.832
batch start
#iterations: 195
currently lose_sum: 94.72483319044113
time_elpased: 1.811
batch start
#iterations: 196
currently lose_sum: 94.86530613899231
time_elpased: 1.808
batch start
#iterations: 197
currently lose_sum: 95.39309418201447
time_elpased: 1.791
batch start
#iterations: 198
currently lose_sum: 94.61064755916595
time_elpased: 1.818
batch start
#iterations: 199
currently lose_sum: 94.8696454167366
time_elpased: 1.793
start validation test
0.649226804124
0.651603498542
0.64402593393
0.647792557321
0.649235935054
61.025
batch start
#iterations: 200
currently lose_sum: 94.75271105766296
time_elpased: 1.817
batch start
#iterations: 201
currently lose_sum: 94.93157333135605
time_elpased: 1.801
batch start
#iterations: 202
currently lose_sum: 94.48009806871414
time_elpased: 1.838
batch start
#iterations: 203
currently lose_sum: 94.9556759595871
time_elpased: 1.83
batch start
#iterations: 204
currently lose_sum: 94.55719697475433
time_elpased: 1.841
batch start
#iterations: 205
currently lose_sum: 94.80057919025421
time_elpased: 1.836
batch start
#iterations: 206
currently lose_sum: 94.81551313400269
time_elpased: 1.838
batch start
#iterations: 207
currently lose_sum: 94.69146925210953
time_elpased: 1.837
batch start
#iterations: 208
currently lose_sum: 94.26714223623276
time_elpased: 1.834
batch start
#iterations: 209
currently lose_sum: 94.59796786308289
time_elpased: 1.826
batch start
#iterations: 210
currently lose_sum: 94.28655105829239
time_elpased: 1.826
batch start
#iterations: 211
currently lose_sum: 94.43321561813354
time_elpased: 1.839
batch start
#iterations: 212
currently lose_sum: 94.1616986989975
time_elpased: 1.83
batch start
#iterations: 213
currently lose_sum: 94.61948484182358
time_elpased: 1.864
batch start
#iterations: 214
currently lose_sum: 94.19062715768814
time_elpased: 1.846
batch start
#iterations: 215
currently lose_sum: 94.53038638830185
time_elpased: 1.837
batch start
#iterations: 216
currently lose_sum: 95.02711623907089
time_elpased: 1.822
batch start
#iterations: 217
currently lose_sum: 94.60799938440323
time_elpased: 1.857
batch start
#iterations: 218
currently lose_sum: 94.99774312973022
time_elpased: 1.831
batch start
#iterations: 219
currently lose_sum: 94.75988817214966
time_elpased: 1.824
start validation test
0.659742268041
0.618876850298
0.834722651024
0.71077421899
0.65943506299
60.565
batch start
#iterations: 220
currently lose_sum: 95.23303431272507
time_elpased: 1.861
batch start
#iterations: 221
currently lose_sum: 94.75385439395905
time_elpased: 1.845
batch start
#iterations: 222
currently lose_sum: 94.90567624568939
time_elpased: 1.84
batch start
#iterations: 223
currently lose_sum: 94.5249792933464
time_elpased: 1.842
batch start
#iterations: 224
currently lose_sum: 94.63588899374008
time_elpased: 1.827
batch start
#iterations: 225
currently lose_sum: 94.50641775131226
time_elpased: 1.837
batch start
#iterations: 226
currently lose_sum: 94.44552308320999
time_elpased: 1.831
batch start
#iterations: 227
currently lose_sum: 94.93484431505203
time_elpased: 1.805
batch start
#iterations: 228
currently lose_sum: 94.72591894865036
time_elpased: 1.805
batch start
#iterations: 229
currently lose_sum: 94.45491981506348
time_elpased: 1.829
batch start
#iterations: 230
currently lose_sum: 94.25605654716492
time_elpased: 1.806
batch start
#iterations: 231
currently lose_sum: 94.72256594896317
time_elpased: 1.838
batch start
#iterations: 232
currently lose_sum: 94.7213888168335
time_elpased: 1.813
batch start
#iterations: 233
currently lose_sum: 94.50863933563232
time_elpased: 1.827
batch start
#iterations: 234
currently lose_sum: 94.90637928247452
time_elpased: 1.8
batch start
#iterations: 235
currently lose_sum: 94.58385878801346
time_elpased: 1.823
batch start
#iterations: 236
currently lose_sum: 94.72410118579865
time_elpased: 1.837
batch start
#iterations: 237
currently lose_sum: 94.24389803409576
time_elpased: 1.843
batch start
#iterations: 238
currently lose_sum: 94.7529901266098
time_elpased: 1.835
batch start
#iterations: 239
currently lose_sum: 94.3812608718872
time_elpased: 1.817
start validation test
0.630618556701
0.665198808445
0.528558196974
0.589058378254
0.630797739404
61.420
batch start
#iterations: 240
currently lose_sum: 94.37923151254654
time_elpased: 1.837
batch start
#iterations: 241
currently lose_sum: 94.62053179740906
time_elpased: 1.82
batch start
#iterations: 242
currently lose_sum: 94.4908401966095
time_elpased: 1.815
batch start
#iterations: 243
currently lose_sum: 94.63318759202957
time_elpased: 1.826
batch start
#iterations: 244
currently lose_sum: 94.95807093381882
time_elpased: 1.842
batch start
#iterations: 245
currently lose_sum: 94.7527831196785
time_elpased: 1.821
batch start
#iterations: 246
currently lose_sum: 94.75982189178467
time_elpased: 1.833
batch start
#iterations: 247
currently lose_sum: 94.79386204481125
time_elpased: 1.82
batch start
#iterations: 248
currently lose_sum: 95.10159516334534
time_elpased: 1.864
batch start
#iterations: 249
currently lose_sum: 94.34087961912155
time_elpased: 1.88
batch start
#iterations: 250
currently lose_sum: 94.70432579517365
time_elpased: 1.866
batch start
#iterations: 251
currently lose_sum: 94.29672437906265
time_elpased: 1.822
batch start
#iterations: 252
currently lose_sum: 94.093625664711
time_elpased: 1.811
batch start
#iterations: 253
currently lose_sum: 94.4197872877121
time_elpased: 1.838
batch start
#iterations: 254
currently lose_sum: 94.61847984790802
time_elpased: 1.793
batch start
#iterations: 255
currently lose_sum: 93.89008110761642
time_elpased: 1.844
batch start
#iterations: 256
currently lose_sum: 94.48364347219467
time_elpased: 1.841
batch start
#iterations: 257
currently lose_sum: 94.31037783622742
time_elpased: 1.838
batch start
#iterations: 258
currently lose_sum: 94.57511258125305
time_elpased: 1.838
batch start
#iterations: 259
currently lose_sum: 94.67413848638535
time_elpased: 1.841
start validation test
0.649896907216
0.639591486112
0.689616136668
0.663662474002
0.649827173983
60.785
batch start
#iterations: 260
currently lose_sum: 94.33289915323257
time_elpased: 1.845
batch start
#iterations: 261
currently lose_sum: 94.54836010932922
time_elpased: 1.85
batch start
#iterations: 262
currently lose_sum: 94.30798500776291
time_elpased: 1.802
batch start
#iterations: 263
currently lose_sum: 94.55990445613861
time_elpased: 1.855
batch start
#iterations: 264
currently lose_sum: 94.57656806707382
time_elpased: 1.849
batch start
#iterations: 265
currently lose_sum: 94.55129444599152
time_elpased: 1.834
batch start
#iterations: 266
currently lose_sum: 94.65704363584518
time_elpased: 1.869
batch start
#iterations: 267
currently lose_sum: 94.44285321235657
time_elpased: 1.849
batch start
#iterations: 268
currently lose_sum: 94.45631664991379
time_elpased: 1.81
batch start
#iterations: 269
currently lose_sum: 94.42815315723419
time_elpased: 1.854
batch start
#iterations: 270
currently lose_sum: 94.66931837797165
time_elpased: 1.879
batch start
#iterations: 271
currently lose_sum: 94.13559991121292
time_elpased: 1.822
batch start
#iterations: 272
currently lose_sum: 94.33283519744873
time_elpased: 1.846
batch start
#iterations: 273
currently lose_sum: 94.57538950443268
time_elpased: 1.84
batch start
#iterations: 274
currently lose_sum: 94.55180931091309
time_elpased: 1.793
batch start
#iterations: 275
currently lose_sum: 94.96383261680603
time_elpased: 1.824
batch start
#iterations: 276
currently lose_sum: 94.52981150150299
time_elpased: 1.79
batch start
#iterations: 277
currently lose_sum: 94.75460147857666
time_elpased: 1.807
batch start
#iterations: 278
currently lose_sum: 94.48001474142075
time_elpased: 1.827
batch start
#iterations: 279
currently lose_sum: 94.68528079986572
time_elpased: 1.8
start validation test
0.670257731959
0.6377593361
0.790881959453
0.706114760877
0.670045957522
60.324
batch start
#iterations: 280
currently lose_sum: 94.3740359544754
time_elpased: 1.818
batch start
#iterations: 281
currently lose_sum: 94.35228979587555
time_elpased: 1.829
batch start
#iterations: 282
currently lose_sum: 94.44340997934341
time_elpased: 1.833
batch start
#iterations: 283
currently lose_sum: 94.91478782892227
time_elpased: 1.762
batch start
#iterations: 284
currently lose_sum: 94.18175786733627
time_elpased: 1.8
batch start
#iterations: 285
currently lose_sum: 94.61915910243988
time_elpased: 1.801
batch start
#iterations: 286
currently lose_sum: 94.42077499628067
time_elpased: 1.772
batch start
#iterations: 287
currently lose_sum: 94.58113372325897
time_elpased: 1.795
batch start
#iterations: 288
currently lose_sum: 94.33177036046982
time_elpased: 1.766
batch start
#iterations: 289
currently lose_sum: 94.50848680734634
time_elpased: 1.812
batch start
#iterations: 290
currently lose_sum: 94.35072845220566
time_elpased: 1.815
batch start
#iterations: 291
currently lose_sum: 94.1996778845787
time_elpased: 1.783
batch start
#iterations: 292
currently lose_sum: 94.12706595659256
time_elpased: 1.814
batch start
#iterations: 293
currently lose_sum: 94.01825028657913
time_elpased: 1.82
batch start
#iterations: 294
currently lose_sum: 94.57794523239136
time_elpased: 1.847
batch start
#iterations: 295
currently lose_sum: 94.45677721500397
time_elpased: 1.794
batch start
#iterations: 296
currently lose_sum: 94.34757387638092
time_elpased: 1.813
batch start
#iterations: 297
currently lose_sum: 94.3188893198967
time_elpased: 1.769
batch start
#iterations: 298
currently lose_sum: 94.68745881319046
time_elpased: 1.793
batch start
#iterations: 299
currently lose_sum: 94.12181502580643
time_elpased: 1.822
start validation test
0.666701030928
0.636356010402
0.780693629721
0.7011738608
0.666500899339
60.244
batch start
#iterations: 300
currently lose_sum: 94.58951199054718
time_elpased: 1.791
batch start
#iterations: 301
currently lose_sum: 94.18007135391235
time_elpased: 1.826
batch start
#iterations: 302
currently lose_sum: 94.30361884832382
time_elpased: 1.807
batch start
#iterations: 303
currently lose_sum: 94.48192685842514
time_elpased: 1.816
batch start
#iterations: 304
currently lose_sum: 94.18013942241669
time_elpased: 1.826
batch start
#iterations: 305
currently lose_sum: 94.30118238925934
time_elpased: 1.787
batch start
#iterations: 306
currently lose_sum: 94.39601409435272
time_elpased: 1.866
batch start
#iterations: 307
currently lose_sum: 94.29806280136108
time_elpased: 1.833
batch start
#iterations: 308
currently lose_sum: 94.36507612466812
time_elpased: 1.799
batch start
#iterations: 309
currently lose_sum: 94.56057327985764
time_elpased: 1.804
batch start
#iterations: 310
currently lose_sum: 94.20528596639633
time_elpased: 1.839
batch start
#iterations: 311
currently lose_sum: 94.20645868778229
time_elpased: 1.861
batch start
#iterations: 312
currently lose_sum: 94.4771763086319
time_elpased: 1.797
batch start
#iterations: 313
currently lose_sum: 94.49856412410736
time_elpased: 1.868
batch start
#iterations: 314
currently lose_sum: 94.62459480762482
time_elpased: 1.823
batch start
#iterations: 315
currently lose_sum: 94.42408740520477
time_elpased: 1.789
batch start
#iterations: 316
currently lose_sum: 94.2442957162857
time_elpased: 1.828
batch start
#iterations: 317
currently lose_sum: 94.18245804309845
time_elpased: 1.786
batch start
#iterations: 318
currently lose_sum: 94.45373916625977
time_elpased: 1.778
batch start
#iterations: 319
currently lose_sum: 94.45274525880814
time_elpased: 1.785
start validation test
0.662886597938
0.63847964432
0.75373057528
0.691334717765
0.662727107324
60.215
batch start
#iterations: 320
currently lose_sum: 94.2904868721962
time_elpased: 1.774
batch start
#iterations: 321
currently lose_sum: 94.29598087072372
time_elpased: 1.81
batch start
#iterations: 322
currently lose_sum: 94.88155657052994
time_elpased: 1.834
batch start
#iterations: 323
currently lose_sum: 94.37816059589386
time_elpased: 1.81
batch start
#iterations: 324
currently lose_sum: 94.37464398145676
time_elpased: 1.797
batch start
#iterations: 325
currently lose_sum: 94.26170140504837
time_elpased: 1.786
batch start
#iterations: 326
currently lose_sum: 93.90584242343903
time_elpased: 1.829
batch start
#iterations: 327
currently lose_sum: 94.81576657295227
time_elpased: 1.782
batch start
#iterations: 328
currently lose_sum: 94.23202872276306
time_elpased: 1.807
batch start
#iterations: 329
currently lose_sum: 94.49352610111237
time_elpased: 1.811
batch start
#iterations: 330
currently lose_sum: 94.25499922037125
time_elpased: 1.818
batch start
#iterations: 331
currently lose_sum: 94.45290666818619
time_elpased: 1.83
batch start
#iterations: 332
currently lose_sum: 94.12420618534088
time_elpased: 1.807
batch start
#iterations: 333
currently lose_sum: 94.42040085792542
time_elpased: 1.83
batch start
#iterations: 334
currently lose_sum: 94.49253612756729
time_elpased: 1.772
batch start
#iterations: 335
currently lose_sum: 94.47799867391586
time_elpased: 1.835
batch start
#iterations: 336
currently lose_sum: 94.20410186052322
time_elpased: 1.846
batch start
#iterations: 337
currently lose_sum: 94.29717421531677
time_elpased: 1.8
batch start
#iterations: 338
currently lose_sum: 94.09029358625412
time_elpased: 1.804
batch start
#iterations: 339
currently lose_sum: 94.13872641324997
time_elpased: 1.821
start validation test
0.666546391753
0.639088729017
0.767932489451
0.697611368205
0.66636839282
60.309
batch start
#iterations: 340
currently lose_sum: 94.31856989860535
time_elpased: 1.828
batch start
#iterations: 341
currently lose_sum: 94.24422192573547
time_elpased: 1.79
batch start
#iterations: 342
currently lose_sum: 94.3719716668129
time_elpased: 1.835
batch start
#iterations: 343
currently lose_sum: 94.64400172233582
time_elpased: 1.81
batch start
#iterations: 344
currently lose_sum: 94.26783740520477
time_elpased: 1.782
batch start
#iterations: 345
currently lose_sum: 94.73154801130295
time_elpased: 1.827
batch start
#iterations: 346
currently lose_sum: 94.16815483570099
time_elpased: 1.795
batch start
#iterations: 347
currently lose_sum: 94.44486427307129
time_elpased: 1.821
batch start
#iterations: 348
currently lose_sum: 94.06874918937683
time_elpased: 1.804
batch start
#iterations: 349
currently lose_sum: 94.46122598648071
time_elpased: 1.81
batch start
#iterations: 350
currently lose_sum: 94.38522636890411
time_elpased: 1.812
batch start
#iterations: 351
currently lose_sum: 94.35528117418289
time_elpased: 1.815
batch start
#iterations: 352
currently lose_sum: 94.42870485782623
time_elpased: 1.81
batch start
#iterations: 353
currently lose_sum: 94.13344579935074
time_elpased: 1.799
batch start
#iterations: 354
currently lose_sum: 94.46550971269608
time_elpased: 1.826
batch start
#iterations: 355
currently lose_sum: 94.3381250500679
time_elpased: 1.822
batch start
#iterations: 356
currently lose_sum: 94.59318280220032
time_elpased: 1.829
batch start
#iterations: 357
currently lose_sum: 94.38571709394455
time_elpased: 1.838
batch start
#iterations: 358
currently lose_sum: 94.52759099006653
time_elpased: 1.822
batch start
#iterations: 359
currently lose_sum: 94.38063138723373
time_elpased: 1.782
start validation test
0.654690721649
0.65467404674
0.657301636308
0.655985210291
0.654686137786
61.141
batch start
#iterations: 360
currently lose_sum: 94.42275494337082
time_elpased: 1.823
batch start
#iterations: 361
currently lose_sum: 94.15070933103561
time_elpased: 1.826
batch start
#iterations: 362
currently lose_sum: 94.53205853700638
time_elpased: 1.854
batch start
#iterations: 363
currently lose_sum: 94.30370408296585
time_elpased: 1.838
batch start
#iterations: 364
currently lose_sum: 94.13414889574051
time_elpased: 1.801
batch start
#iterations: 365
currently lose_sum: 94.17902338504791
time_elpased: 1.806
batch start
#iterations: 366
currently lose_sum: 94.27257436513901
time_elpased: 1.8
batch start
#iterations: 367
currently lose_sum: 94.29368788003922
time_elpased: 1.855
batch start
#iterations: 368
currently lose_sum: 94.2839989066124
time_elpased: 1.807
batch start
#iterations: 369
currently lose_sum: 94.53427565097809
time_elpased: 1.808
batch start
#iterations: 370
currently lose_sum: 94.05114883184433
time_elpased: 1.848
batch start
#iterations: 371
currently lose_sum: 94.31649231910706
time_elpased: 1.797
batch start
#iterations: 372
currently lose_sum: 94.00793820619583
time_elpased: 1.781
batch start
#iterations: 373
currently lose_sum: 94.17733800411224
time_elpased: 1.788
batch start
#iterations: 374
currently lose_sum: 94.26774823665619
time_elpased: 1.825
batch start
#iterations: 375
currently lose_sum: 94.3395716547966
time_elpased: 1.817
batch start
#iterations: 376
currently lose_sum: 94.03192865848541
time_elpased: 1.799
batch start
#iterations: 377
currently lose_sum: 94.32793670892715
time_elpased: 1.766
batch start
#iterations: 378
currently lose_sum: 94.25915437936783
time_elpased: 1.791
batch start
#iterations: 379
currently lose_sum: 93.94564837217331
time_elpased: 1.789
start validation test
0.668041237113
0.635000411963
0.793146032726
0.70531710442
0.667821596349
60.082
batch start
#iterations: 380
currently lose_sum: 94.3841661810875
time_elpased: 1.814
batch start
#iterations: 381
currently lose_sum: 94.14078629016876
time_elpased: 1.856
batch start
#iterations: 382
currently lose_sum: 94.3484817147255
time_elpased: 1.796
batch start
#iterations: 383
currently lose_sum: 94.34025466442108
time_elpased: 1.792
batch start
#iterations: 384
currently lose_sum: 95.04317545890808
time_elpased: 1.816
batch start
#iterations: 385
currently lose_sum: 93.91021001338959
time_elpased: 1.821
batch start
#iterations: 386
currently lose_sum: 94.03322225809097
time_elpased: 1.86
batch start
#iterations: 387
currently lose_sum: 94.50535207986832
time_elpased: 1.779
batch start
#iterations: 388
currently lose_sum: 94.24013984203339
time_elpased: 1.825
batch start
#iterations: 389
currently lose_sum: 94.55559277534485
time_elpased: 1.801
batch start
#iterations: 390
currently lose_sum: 94.42612606287003
time_elpased: 1.841
batch start
#iterations: 391
currently lose_sum: 94.25683748722076
time_elpased: 1.8
batch start
#iterations: 392
currently lose_sum: 93.905453145504
time_elpased: 1.842
batch start
#iterations: 393
currently lose_sum: 94.05732595920563
time_elpased: 1.837
batch start
#iterations: 394
currently lose_sum: 93.77874290943146
time_elpased: 1.822
batch start
#iterations: 395
currently lose_sum: 93.85615706443787
time_elpased: 1.813
batch start
#iterations: 396
currently lose_sum: 94.7524066567421
time_elpased: 1.8
batch start
#iterations: 397
currently lose_sum: 94.39586329460144
time_elpased: 1.827
batch start
#iterations: 398
currently lose_sum: 94.13689267635345
time_elpased: 1.812
batch start
#iterations: 399
currently lose_sum: 94.34408020973206
time_elpased: 1.809
start validation test
0.666649484536
0.641995805662
0.756097560976
0.694390624262
0.666492444641
60.485
acc: 0.667
pre: 0.635
rec: 0.792
F1: 0.705
auc: 0.705
