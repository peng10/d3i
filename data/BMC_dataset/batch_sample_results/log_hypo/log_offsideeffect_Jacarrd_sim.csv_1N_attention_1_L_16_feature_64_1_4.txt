start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.40056335926056
time_elpased: 1.575
batch start
#iterations: 1
currently lose_sum: 99.8650992512703
time_elpased: 1.562
batch start
#iterations: 2
currently lose_sum: 99.6067545413971
time_elpased: 1.538
batch start
#iterations: 3
currently lose_sum: 99.37414360046387
time_elpased: 1.595
batch start
#iterations: 4
currently lose_sum: 99.25273567438126
time_elpased: 1.528
batch start
#iterations: 5
currently lose_sum: 98.86713200807571
time_elpased: 1.566
batch start
#iterations: 6
currently lose_sum: 98.52184057235718
time_elpased: 1.553
batch start
#iterations: 7
currently lose_sum: 98.52052342891693
time_elpased: 1.54
batch start
#iterations: 8
currently lose_sum: 98.48773902654648
time_elpased: 1.581
batch start
#iterations: 9
currently lose_sum: 97.65287107229233
time_elpased: 1.575
batch start
#iterations: 10
currently lose_sum: 97.99517893791199
time_elpased: 1.55
batch start
#iterations: 11
currently lose_sum: 97.71683633327484
time_elpased: 1.516
batch start
#iterations: 12
currently lose_sum: 97.61346423625946
time_elpased: 1.577
batch start
#iterations: 13
currently lose_sum: 97.64965945482254
time_elpased: 1.617
batch start
#iterations: 14
currently lose_sum: 97.15223675966263
time_elpased: 1.557
batch start
#iterations: 15
currently lose_sum: 97.44195759296417
time_elpased: 1.553
batch start
#iterations: 16
currently lose_sum: 97.60835373401642
time_elpased: 1.567
batch start
#iterations: 17
currently lose_sum: 96.90059900283813
time_elpased: 1.548
batch start
#iterations: 18
currently lose_sum: 97.05916565656662
time_elpased: 1.553
batch start
#iterations: 19
currently lose_sum: 97.01544976234436
time_elpased: 1.55
start validation test
0.648969072165
0.637602953706
0.693115159
0.664201183432
0.6488915669
62.503
batch start
#iterations: 20
currently lose_sum: 96.97559022903442
time_elpased: 1.545
batch start
#iterations: 21
currently lose_sum: 96.8560756444931
time_elpased: 1.554
batch start
#iterations: 22
currently lose_sum: 97.15715962648392
time_elpased: 1.527
batch start
#iterations: 23
currently lose_sum: 96.84042054414749
time_elpased: 1.516
batch start
#iterations: 24
currently lose_sum: 96.87957799434662
time_elpased: 1.577
batch start
#iterations: 25
currently lose_sum: 97.01807850599289
time_elpased: 1.549
batch start
#iterations: 26
currently lose_sum: 96.68273395299911
time_elpased: 1.545
batch start
#iterations: 27
currently lose_sum: 96.31971764564514
time_elpased: 1.543
batch start
#iterations: 28
currently lose_sum: 96.86529594659805
time_elpased: 1.564
batch start
#iterations: 29
currently lose_sum: 96.2533431649208
time_elpased: 1.558
batch start
#iterations: 30
currently lose_sum: 96.56484073400497
time_elpased: 1.577
batch start
#iterations: 31
currently lose_sum: 96.40175354480743
time_elpased: 1.558
batch start
#iterations: 32
currently lose_sum: 96.36314767599106
time_elpased: 1.586
batch start
#iterations: 33
currently lose_sum: 96.30171072483063
time_elpased: 1.588
batch start
#iterations: 34
currently lose_sum: 96.37480157613754
time_elpased: 1.565
batch start
#iterations: 35
currently lose_sum: 95.97434437274933
time_elpased: 1.532
batch start
#iterations: 36
currently lose_sum: 96.27920204401016
time_elpased: 1.575
batch start
#iterations: 37
currently lose_sum: 95.95899194478989
time_elpased: 1.565
batch start
#iterations: 38
currently lose_sum: 96.13035589456558
time_elpased: 1.566
batch start
#iterations: 39
currently lose_sum: 96.14044803380966
time_elpased: 1.506
start validation test
0.65587628866
0.636111359771
0.731295667387
0.680390654921
0.655743878308
61.813
batch start
#iterations: 40
currently lose_sum: 96.10027199983597
time_elpased: 1.548
batch start
#iterations: 41
currently lose_sum: 96.01777285337448
time_elpased: 1.601
batch start
#iterations: 42
currently lose_sum: 95.9039831161499
time_elpased: 1.53
batch start
#iterations: 43
currently lose_sum: 96.15059095621109
time_elpased: 1.581
batch start
#iterations: 44
currently lose_sum: 95.93877965211868
time_elpased: 1.544
batch start
#iterations: 45
currently lose_sum: 95.71716183423996
time_elpased: 1.549
batch start
#iterations: 46
currently lose_sum: 95.83443123102188
time_elpased: 1.537
batch start
#iterations: 47
currently lose_sum: 95.95479303598404
time_elpased: 1.532
batch start
#iterations: 48
currently lose_sum: 95.93478453159332
time_elpased: 1.536
batch start
#iterations: 49
currently lose_sum: 96.08660072088242
time_elpased: 1.533
batch start
#iterations: 50
currently lose_sum: 95.86654859781265
time_elpased: 1.532
batch start
#iterations: 51
currently lose_sum: 95.83082813024521
time_elpased: 1.568
batch start
#iterations: 52
currently lose_sum: 95.88878536224365
time_elpased: 1.545
batch start
#iterations: 53
currently lose_sum: 95.25460433959961
time_elpased: 1.556
batch start
#iterations: 54
currently lose_sum: 96.1673703789711
time_elpased: 1.534
batch start
#iterations: 55
currently lose_sum: 95.67074698209763
time_elpased: 1.578
batch start
#iterations: 56
currently lose_sum: 95.78375518321991
time_elpased: 1.53
batch start
#iterations: 57
currently lose_sum: 95.61144614219666
time_elpased: 1.629
batch start
#iterations: 58
currently lose_sum: 95.6625428199768
time_elpased: 1.591
batch start
#iterations: 59
currently lose_sum: 96.0608057975769
time_elpased: 1.526
start validation test
0.668041237113
0.658355078767
0.701039415457
0.679027113238
0.667983303722
61.131
batch start
#iterations: 60
currently lose_sum: 96.14768695831299
time_elpased: 1.55
batch start
#iterations: 61
currently lose_sum: 95.96633034944534
time_elpased: 1.548
batch start
#iterations: 62
currently lose_sum: 95.5694580078125
time_elpased: 1.586
batch start
#iterations: 63
currently lose_sum: 95.88921749591827
time_elpased: 1.545
batch start
#iterations: 64
currently lose_sum: 96.09446120262146
time_elpased: 1.542
batch start
#iterations: 65
currently lose_sum: 95.47318947315216
time_elpased: 1.537
batch start
#iterations: 66
currently lose_sum: 95.79857933521271
time_elpased: 1.544
batch start
#iterations: 67
currently lose_sum: 95.59813916683197
time_elpased: 1.534
batch start
#iterations: 68
currently lose_sum: 95.59320586919785
time_elpased: 1.551
batch start
#iterations: 69
currently lose_sum: 95.43916112184525
time_elpased: 1.565
batch start
#iterations: 70
currently lose_sum: 95.46774679422379
time_elpased: 1.524
batch start
#iterations: 71
currently lose_sum: 95.73899281024933
time_elpased: 1.539
batch start
#iterations: 72
currently lose_sum: 95.46706587076187
time_elpased: 1.527
batch start
#iterations: 73
currently lose_sum: 95.34574431180954
time_elpased: 1.568
batch start
#iterations: 74
currently lose_sum: 95.51467907428741
time_elpased: 1.548
batch start
#iterations: 75
currently lose_sum: 95.41099268198013
time_elpased: 1.568
batch start
#iterations: 76
currently lose_sum: 95.47926008701324
time_elpased: 1.515
batch start
#iterations: 77
currently lose_sum: 95.70137518644333
time_elpased: 1.544
batch start
#iterations: 78
currently lose_sum: 95.44258856773376
time_elpased: 1.534
batch start
#iterations: 79
currently lose_sum: 95.29646050930023
time_elpased: 1.597
start validation test
0.661855670103
0.647152046238
0.714418030256
0.679123459206
0.661763388773
60.855
batch start
#iterations: 80
currently lose_sum: 95.64585500955582
time_elpased: 1.573
batch start
#iterations: 81
currently lose_sum: 94.93790209293365
time_elpased: 1.518
batch start
#iterations: 82
currently lose_sum: 95.49165380001068
time_elpased: 1.534
batch start
#iterations: 83
currently lose_sum: 95.59990006685257
time_elpased: 1.57
batch start
#iterations: 84
currently lose_sum: 95.55594235658646
time_elpased: 1.517
batch start
#iterations: 85
currently lose_sum: 95.52752250432968
time_elpased: 1.548
batch start
#iterations: 86
currently lose_sum: 95.45981824398041
time_elpased: 1.533
batch start
#iterations: 87
currently lose_sum: 95.27016019821167
time_elpased: 1.546
batch start
#iterations: 88
currently lose_sum: 95.84694743156433
time_elpased: 1.581
batch start
#iterations: 89
currently lose_sum: 95.41059064865112
time_elpased: 1.563
batch start
#iterations: 90
currently lose_sum: 95.26056796312332
time_elpased: 1.53
batch start
#iterations: 91
currently lose_sum: 95.6971201300621
time_elpased: 1.564
batch start
#iterations: 92
currently lose_sum: 95.1232939362526
time_elpased: 1.565
batch start
#iterations: 93
currently lose_sum: 95.06530582904816
time_elpased: 1.531
batch start
#iterations: 94
currently lose_sum: 95.47684478759766
time_elpased: 1.514
batch start
#iterations: 95
currently lose_sum: 95.7065337896347
time_elpased: 1.551
batch start
#iterations: 96
currently lose_sum: 95.3067923784256
time_elpased: 1.559
batch start
#iterations: 97
currently lose_sum: 95.2874123454094
time_elpased: 1.535
batch start
#iterations: 98
currently lose_sum: 95.74114269018173
time_elpased: 1.571
batch start
#iterations: 99
currently lose_sum: 95.70167374610901
time_elpased: 1.571
start validation test
0.648505154639
0.667282382822
0.594833796439
0.62897872572
0.648599382987
61.367
batch start
#iterations: 100
currently lose_sum: 95.12114357948303
time_elpased: 1.56
batch start
#iterations: 101
currently lose_sum: 95.36889630556107
time_elpased: 1.562
batch start
#iterations: 102
currently lose_sum: 95.28733742237091
time_elpased: 1.522
batch start
#iterations: 103
currently lose_sum: 95.34855675697327
time_elpased: 1.608
batch start
#iterations: 104
currently lose_sum: 95.20933926105499
time_elpased: 1.568
batch start
#iterations: 105
currently lose_sum: 95.41003811359406
time_elpased: 1.544
batch start
#iterations: 106
currently lose_sum: 95.37169831991196
time_elpased: 1.519
batch start
#iterations: 107
currently lose_sum: 95.42850333452225
time_elpased: 1.576
batch start
#iterations: 108
currently lose_sum: 95.45719790458679
time_elpased: 1.547
batch start
#iterations: 109
currently lose_sum: 95.27762007713318
time_elpased: 1.555
batch start
#iterations: 110
currently lose_sum: 95.34930145740509
time_elpased: 1.583
batch start
#iterations: 111
currently lose_sum: 95.40650808811188
time_elpased: 1.541
batch start
#iterations: 112
currently lose_sum: 94.8973560333252
time_elpased: 1.546
batch start
#iterations: 113
currently lose_sum: 94.47385776042938
time_elpased: 1.552
batch start
#iterations: 114
currently lose_sum: 95.55168199539185
time_elpased: 1.553
batch start
#iterations: 115
currently lose_sum: 95.24652755260468
time_elpased: 1.524
batch start
#iterations: 116
currently lose_sum: 94.86583018302917
time_elpased: 1.581
batch start
#iterations: 117
currently lose_sum: 95.27218693494797
time_elpased: 1.537
batch start
#iterations: 118
currently lose_sum: 95.05996859073639
time_elpased: 1.541
batch start
#iterations: 119
currently lose_sum: 94.87523674964905
time_elpased: 1.525
start validation test
0.64
0.668516463189
0.55788823711
0.608212722989
0.640144159865
61.432
batch start
#iterations: 120
currently lose_sum: 95.01643735170364
time_elpased: 1.577
batch start
#iterations: 121
currently lose_sum: 95.13949298858643
time_elpased: 1.563
batch start
#iterations: 122
currently lose_sum: 95.32749134302139
time_elpased: 1.521
batch start
#iterations: 123
currently lose_sum: 95.29833650588989
time_elpased: 1.58
batch start
#iterations: 124
currently lose_sum: 94.84953212738037
time_elpased: 1.544
batch start
#iterations: 125
currently lose_sum: 95.06594771146774
time_elpased: 1.569
batch start
#iterations: 126
currently lose_sum: 95.3778088092804
time_elpased: 1.547
batch start
#iterations: 127
currently lose_sum: 95.04560899734497
time_elpased: 1.571
batch start
#iterations: 128
currently lose_sum: 95.08093041181564
time_elpased: 1.529
batch start
#iterations: 129
currently lose_sum: 95.01396918296814
time_elpased: 1.541
batch start
#iterations: 130
currently lose_sum: 95.02691388130188
time_elpased: 1.565
batch start
#iterations: 131
currently lose_sum: 95.35362130403519
time_elpased: 1.576
batch start
#iterations: 132
currently lose_sum: 95.45696061849594
time_elpased: 1.57
batch start
#iterations: 133
currently lose_sum: 94.6615639925003
time_elpased: 1.542
batch start
#iterations: 134
currently lose_sum: 95.08585888147354
time_elpased: 1.533
batch start
#iterations: 135
currently lose_sum: 95.23426812887192
time_elpased: 1.564
batch start
#iterations: 136
currently lose_sum: 94.91062653064728
time_elpased: 1.528
batch start
#iterations: 137
currently lose_sum: 95.19362723827362
time_elpased: 1.545
batch start
#iterations: 138
currently lose_sum: 95.08656591176987
time_elpased: 1.554
batch start
#iterations: 139
currently lose_sum: 95.00587207078934
time_elpased: 1.587
start validation test
0.670154639175
0.639881956155
0.781002366986
0.703434212356
0.669960028892
60.454
batch start
#iterations: 140
currently lose_sum: 95.13161486387253
time_elpased: 1.537
batch start
#iterations: 141
currently lose_sum: 95.17479574680328
time_elpased: 1.544
batch start
#iterations: 142
currently lose_sum: 95.08193814754486
time_elpased: 1.577
batch start
#iterations: 143
currently lose_sum: 95.07698029279709
time_elpased: 1.546
batch start
#iterations: 144
currently lose_sum: 95.15397775173187
time_elpased: 1.566
batch start
#iterations: 145
currently lose_sum: 95.13656282424927
time_elpased: 1.607
batch start
#iterations: 146
currently lose_sum: 95.12694138288498
time_elpased: 1.558
batch start
#iterations: 147
currently lose_sum: 95.24830639362335
time_elpased: 1.507
batch start
#iterations: 148
currently lose_sum: 94.77152466773987
time_elpased: 1.565
batch start
#iterations: 149
currently lose_sum: 95.15487027168274
time_elpased: 1.538
batch start
#iterations: 150
currently lose_sum: 94.99434077739716
time_elpased: 1.565
batch start
#iterations: 151
currently lose_sum: 94.95322275161743
time_elpased: 1.538
batch start
#iterations: 152
currently lose_sum: 95.01544862985611
time_elpased: 1.539
batch start
#iterations: 153
currently lose_sum: 95.18333721160889
time_elpased: 1.551
batch start
#iterations: 154
currently lose_sum: 95.18787151575089
time_elpased: 1.59
batch start
#iterations: 155
currently lose_sum: 94.63488608598709
time_elpased: 1.539
batch start
#iterations: 156
currently lose_sum: 95.04985761642456
time_elpased: 1.647
batch start
#iterations: 157
currently lose_sum: 94.9708663225174
time_elpased: 1.565
batch start
#iterations: 158
currently lose_sum: 95.24853348731995
time_elpased: 1.566
batch start
#iterations: 159
currently lose_sum: 95.09369361400604
time_elpased: 1.539
start validation test
0.670309278351
0.65102319236
0.736647113307
0.691193511008
0.670192812049
60.425
batch start
#iterations: 160
currently lose_sum: 95.05320513248444
time_elpased: 1.533
batch start
#iterations: 161
currently lose_sum: 94.90729033946991
time_elpased: 1.561
batch start
#iterations: 162
currently lose_sum: 94.63060253858566
time_elpased: 1.585
batch start
#iterations: 163
currently lose_sum: 94.89305019378662
time_elpased: 1.529
batch start
#iterations: 164
currently lose_sum: 95.28986477851868
time_elpased: 1.552
batch start
#iterations: 165
currently lose_sum: 94.86681562662125
time_elpased: 1.557
batch start
#iterations: 166
currently lose_sum: 94.76906025409698
time_elpased: 1.552
batch start
#iterations: 167
currently lose_sum: 95.121697306633
time_elpased: 1.549
batch start
#iterations: 168
currently lose_sum: 95.01319545507431
time_elpased: 1.566
batch start
#iterations: 169
currently lose_sum: 94.5928385257721
time_elpased: 1.554
batch start
#iterations: 170
currently lose_sum: 94.87399345636368
time_elpased: 1.539
batch start
#iterations: 171
currently lose_sum: 94.51806336641312
time_elpased: 1.573
batch start
#iterations: 172
currently lose_sum: 94.87826228141785
time_elpased: 1.576
batch start
#iterations: 173
currently lose_sum: 94.6957134604454
time_elpased: 1.607
batch start
#iterations: 174
currently lose_sum: 94.7494570016861
time_elpased: 1.535
batch start
#iterations: 175
currently lose_sum: 94.96556633710861
time_elpased: 1.56
batch start
#iterations: 176
currently lose_sum: 95.36546939611435
time_elpased: 1.536
batch start
#iterations: 177
currently lose_sum: 94.61860156059265
time_elpased: 1.576
batch start
#iterations: 178
currently lose_sum: 94.53791743516922
time_elpased: 1.594
batch start
#iterations: 179
currently lose_sum: 95.12661397457123
time_elpased: 1.564
start validation test
0.671494845361
0.642419080068
0.776165483174
0.702987370089
0.671311079912
60.659
batch start
#iterations: 180
currently lose_sum: 94.89863741397858
time_elpased: 1.594
batch start
#iterations: 181
currently lose_sum: 95.00119251012802
time_elpased: 1.576
batch start
#iterations: 182
currently lose_sum: 94.89647841453552
time_elpased: 1.535
batch start
#iterations: 183
currently lose_sum: 94.62172996997833
time_elpased: 1.552
batch start
#iterations: 184
currently lose_sum: 94.74082046747208
time_elpased: 1.537
batch start
#iterations: 185
currently lose_sum: 95.15117049217224
time_elpased: 1.54
batch start
#iterations: 186
currently lose_sum: 94.4030270576477
time_elpased: 1.56
batch start
#iterations: 187
currently lose_sum: 94.8194408416748
time_elpased: 1.546
batch start
#iterations: 188
currently lose_sum: 95.02579176425934
time_elpased: 1.56
batch start
#iterations: 189
currently lose_sum: 94.29283833503723
time_elpased: 1.553
batch start
#iterations: 190
currently lose_sum: 94.69664704799652
time_elpased: 1.571
batch start
#iterations: 191
currently lose_sum: 94.63886141777039
time_elpased: 1.574
batch start
#iterations: 192
currently lose_sum: 95.30323630571365
time_elpased: 1.592
batch start
#iterations: 193
currently lose_sum: 94.84644317626953
time_elpased: 1.521
batch start
#iterations: 194
currently lose_sum: 94.58254253864288
time_elpased: 1.537
batch start
#iterations: 195
currently lose_sum: 94.83009737730026
time_elpased: 1.598
batch start
#iterations: 196
currently lose_sum: 94.68404841423035
time_elpased: 1.554
batch start
#iterations: 197
currently lose_sum: 94.64573699235916
time_elpased: 1.551
batch start
#iterations: 198
currently lose_sum: 94.74660897254944
time_elpased: 1.545
batch start
#iterations: 199
currently lose_sum: 95.01263284683228
time_elpased: 1.57
start validation test
0.665309278351
0.61994047619
0.857466296182
0.719609621281
0.664971917068
60.547
batch start
#iterations: 200
currently lose_sum: 94.93527632951736
time_elpased: 1.556
batch start
#iterations: 201
currently lose_sum: 94.76776283979416
time_elpased: 1.546
batch start
#iterations: 202
currently lose_sum: 94.62968051433563
time_elpased: 1.57
batch start
#iterations: 203
currently lose_sum: 94.88451093435287
time_elpased: 1.553
batch start
#iterations: 204
currently lose_sum: 94.5432556271553
time_elpased: 1.555
batch start
#iterations: 205
currently lose_sum: 94.81391859054565
time_elpased: 1.558
batch start
#iterations: 206
currently lose_sum: 94.9625933766365
time_elpased: 1.562
batch start
#iterations: 207
currently lose_sum: 94.62754207849503
time_elpased: 1.552
batch start
#iterations: 208
currently lose_sum: 94.90147364139557
time_elpased: 1.581
batch start
#iterations: 209
currently lose_sum: 94.29297745227814
time_elpased: 1.525
batch start
#iterations: 210
currently lose_sum: 94.83578157424927
time_elpased: 1.556
batch start
#iterations: 211
currently lose_sum: 94.41605907678604
time_elpased: 1.538
batch start
#iterations: 212
currently lose_sum: 94.64789772033691
time_elpased: 1.591
batch start
#iterations: 213
currently lose_sum: 94.58603721857071
time_elpased: 1.555
batch start
#iterations: 214
currently lose_sum: 94.6392651796341
time_elpased: 1.585
batch start
#iterations: 215
currently lose_sum: 94.54304003715515
time_elpased: 1.551
batch start
#iterations: 216
currently lose_sum: 94.5212544798851
time_elpased: 1.541
batch start
#iterations: 217
currently lose_sum: 94.9988145828247
time_elpased: 1.529
batch start
#iterations: 218
currently lose_sum: 94.33446913957596
time_elpased: 1.557
batch start
#iterations: 219
currently lose_sum: 94.59673023223877
time_elpased: 1.598
start validation test
0.668865979381
0.629411302366
0.824122671606
0.713725490196
0.668593402312
60.260
batch start
#iterations: 220
currently lose_sum: 94.75670719146729
time_elpased: 1.549
batch start
#iterations: 221
currently lose_sum: 94.74195498228073
time_elpased: 1.563
batch start
#iterations: 222
currently lose_sum: 94.54163122177124
time_elpased: 1.524
batch start
#iterations: 223
currently lose_sum: 94.59713679552078
time_elpased: 1.517
batch start
#iterations: 224
currently lose_sum: 95.02688890695572
time_elpased: 1.578
batch start
#iterations: 225
currently lose_sum: 94.51875513792038
time_elpased: 1.613
batch start
#iterations: 226
currently lose_sum: 94.45795154571533
time_elpased: 1.533
batch start
#iterations: 227
currently lose_sum: 94.7955276966095
time_elpased: 1.57
batch start
#iterations: 228
currently lose_sum: 94.42380201816559
time_elpased: 1.576
batch start
#iterations: 229
currently lose_sum: 95.08842444419861
time_elpased: 1.565
batch start
#iterations: 230
currently lose_sum: 94.6576799750328
time_elpased: 1.535
batch start
#iterations: 231
currently lose_sum: 94.57446622848511
time_elpased: 1.562
batch start
#iterations: 232
currently lose_sum: 94.50360232591629
time_elpased: 1.566
batch start
#iterations: 233
currently lose_sum: 94.29895412921906
time_elpased: 1.531
batch start
#iterations: 234
currently lose_sum: 94.69850689172745
time_elpased: 1.545
batch start
#iterations: 235
currently lose_sum: 94.6375914812088
time_elpased: 1.526
batch start
#iterations: 236
currently lose_sum: 94.78715866804123
time_elpased: 1.543
batch start
#iterations: 237
currently lose_sum: 94.76269036531448
time_elpased: 1.566
batch start
#iterations: 238
currently lose_sum: 94.45912826061249
time_elpased: 1.576
batch start
#iterations: 239
currently lose_sum: 94.65486973524094
time_elpased: 1.529
start validation test
0.666649484536
0.642643960674
0.753421838016
0.693637784831
0.666497142286
60.515
batch start
#iterations: 240
currently lose_sum: 94.86980897188187
time_elpased: 1.556
batch start
#iterations: 241
currently lose_sum: 95.08567500114441
time_elpased: 1.565
batch start
#iterations: 242
currently lose_sum: 94.38290667533875
time_elpased: 1.58
batch start
#iterations: 243
currently lose_sum: 94.8919233083725
time_elpased: 1.574
batch start
#iterations: 244
currently lose_sum: 94.69313484430313
time_elpased: 1.539
batch start
#iterations: 245
currently lose_sum: 94.39060741662979
time_elpased: 1.542
batch start
#iterations: 246
currently lose_sum: 95.05862987041473
time_elpased: 1.551
batch start
#iterations: 247
currently lose_sum: 94.31769669055939
time_elpased: 1.547
batch start
#iterations: 248
currently lose_sum: 94.70941984653473
time_elpased: 1.575
batch start
#iterations: 249
currently lose_sum: 94.59314090013504
time_elpased: 1.528
batch start
#iterations: 250
currently lose_sum: 94.69834530353546
time_elpased: 1.568
batch start
#iterations: 251
currently lose_sum: 94.65969449281693
time_elpased: 1.54
batch start
#iterations: 252
currently lose_sum: 94.55026990175247
time_elpased: 1.533
batch start
#iterations: 253
currently lose_sum: 94.36766862869263
time_elpased: 1.592
batch start
#iterations: 254
currently lose_sum: 94.50309318304062
time_elpased: 1.58
batch start
#iterations: 255
currently lose_sum: 94.45683717727661
time_elpased: 1.534
batch start
#iterations: 256
currently lose_sum: 94.24646764993668
time_elpased: 1.579
batch start
#iterations: 257
currently lose_sum: 94.91046237945557
time_elpased: 1.531
batch start
#iterations: 258
currently lose_sum: 94.16656106710434
time_elpased: 1.521
batch start
#iterations: 259
currently lose_sum: 94.46463805437088
time_elpased: 1.561
start validation test
0.660927835052
0.650725055219
0.697334568282
0.673224043716
0.660863917416
60.638
batch start
#iterations: 260
currently lose_sum: 94.58462995290756
time_elpased: 1.591
batch start
#iterations: 261
currently lose_sum: 94.50062704086304
time_elpased: 1.553
batch start
#iterations: 262
currently lose_sum: 94.56570971012115
time_elpased: 1.571
batch start
#iterations: 263
currently lose_sum: 94.33111840486526
time_elpased: 1.563
batch start
#iterations: 264
currently lose_sum: 94.69860249757767
time_elpased: 1.522
batch start
#iterations: 265
currently lose_sum: 94.74754583835602
time_elpased: 1.541
batch start
#iterations: 266
currently lose_sum: 94.87306088209152
time_elpased: 1.583
batch start
#iterations: 267
currently lose_sum: 94.52646070718765
time_elpased: 1.529
batch start
#iterations: 268
currently lose_sum: 94.72365772724152
time_elpased: 1.534
batch start
#iterations: 269
currently lose_sum: 94.71393650770187
time_elpased: 1.519
batch start
#iterations: 270
currently lose_sum: 94.66884398460388
time_elpased: 1.553
batch start
#iterations: 271
currently lose_sum: 94.35706984996796
time_elpased: 1.551
batch start
#iterations: 272
currently lose_sum: 94.43200892210007
time_elpased: 1.581
batch start
#iterations: 273
currently lose_sum: 94.67558002471924
time_elpased: 1.548
batch start
#iterations: 274
currently lose_sum: 94.68136686086655
time_elpased: 1.547
batch start
#iterations: 275
currently lose_sum: 94.45567029714584
time_elpased: 1.531
batch start
#iterations: 276
currently lose_sum: 94.59458124637604
time_elpased: 1.569
batch start
#iterations: 277
currently lose_sum: 94.4436029791832
time_elpased: 1.525
batch start
#iterations: 278
currently lose_sum: 94.77737158536911
time_elpased: 1.59
batch start
#iterations: 279
currently lose_sum: 94.38539814949036
time_elpased: 1.536
start validation test
0.671134020619
0.644296462856
0.766697540393
0.700187969925
0.67096624412
60.305
batch start
#iterations: 280
currently lose_sum: 94.82670772075653
time_elpased: 1.516
batch start
#iterations: 281
currently lose_sum: 94.18218332529068
time_elpased: 1.547
batch start
#iterations: 282
currently lose_sum: 94.25474846363068
time_elpased: 1.557
batch start
#iterations: 283
currently lose_sum: 94.28659105300903
time_elpased: 1.585
batch start
#iterations: 284
currently lose_sum: 94.16732287406921
time_elpased: 1.579
batch start
#iterations: 285
currently lose_sum: 94.19352793693542
time_elpased: 1.519
batch start
#iterations: 286
currently lose_sum: 94.66882014274597
time_elpased: 1.564
batch start
#iterations: 287
currently lose_sum: 94.34895724058151
time_elpased: 1.524
batch start
#iterations: 288
currently lose_sum: 94.53244775533676
time_elpased: 1.54
batch start
#iterations: 289
currently lose_sum: 94.24201834201813
time_elpased: 1.573
batch start
#iterations: 290
currently lose_sum: 94.385537981987
time_elpased: 1.54
batch start
#iterations: 291
currently lose_sum: 94.15211141109467
time_elpased: 1.552
batch start
#iterations: 292
currently lose_sum: 94.21845984458923
time_elpased: 1.589
batch start
#iterations: 293
currently lose_sum: 94.06122136116028
time_elpased: 1.524
batch start
#iterations: 294
currently lose_sum: 94.10909831523895
time_elpased: 1.567
batch start
#iterations: 295
currently lose_sum: 94.3049305677414
time_elpased: 1.537
batch start
#iterations: 296
currently lose_sum: 93.91314774751663
time_elpased: 1.554
batch start
#iterations: 297
currently lose_sum: 94.92234128713608
time_elpased: 1.549
batch start
#iterations: 298
currently lose_sum: 93.99814558029175
time_elpased: 1.592
batch start
#iterations: 299
currently lose_sum: 94.3188829421997
time_elpased: 1.516
start validation test
0.669845360825
0.638739946381
0.784604301739
0.704198032605
0.669643883804
60.102
batch start
#iterations: 300
currently lose_sum: 94.39198976755142
time_elpased: 1.541
batch start
#iterations: 301
currently lose_sum: 94.22515970468521
time_elpased: 1.546
batch start
#iterations: 302
currently lose_sum: 94.39597302675247
time_elpased: 1.559
batch start
#iterations: 303
currently lose_sum: 94.14454519748688
time_elpased: 1.588
batch start
#iterations: 304
currently lose_sum: 94.26791685819626
time_elpased: 1.544
batch start
#iterations: 305
currently lose_sum: 94.37568324804306
time_elpased: 1.593
batch start
#iterations: 306
currently lose_sum: 94.30466479063034
time_elpased: 1.555
batch start
#iterations: 307
currently lose_sum: 94.10405433177948
time_elpased: 1.608
batch start
#iterations: 308
currently lose_sum: 94.58858340978622
time_elpased: 1.588
batch start
#iterations: 309
currently lose_sum: 94.43946236371994
time_elpased: 1.518
batch start
#iterations: 310
currently lose_sum: 94.38976228237152
time_elpased: 1.526
batch start
#iterations: 311
currently lose_sum: 94.33148920536041
time_elpased: 1.555
batch start
#iterations: 312
currently lose_sum: 94.3078778386116
time_elpased: 1.56
batch start
#iterations: 313
currently lose_sum: 93.93182361125946
time_elpased: 1.561
batch start
#iterations: 314
currently lose_sum: 93.92751085758209
time_elpased: 1.574
batch start
#iterations: 315
currently lose_sum: 94.0724995136261
time_elpased: 1.587
batch start
#iterations: 316
currently lose_sum: 94.54460972547531
time_elpased: 1.555
batch start
#iterations: 317
currently lose_sum: 94.2689339518547
time_elpased: 1.563
batch start
#iterations: 318
currently lose_sum: 94.37616539001465
time_elpased: 1.529
batch start
#iterations: 319
currently lose_sum: 94.44956880807877
time_elpased: 1.524
start validation test
0.655979381443
0.658077922078
0.651847277966
0.654947782029
0.655986635988
60.971
batch start
#iterations: 320
currently lose_sum: 94.09297901391983
time_elpased: 1.542
batch start
#iterations: 321
currently lose_sum: 94.22427171468735
time_elpased: 1.61
batch start
#iterations: 322
currently lose_sum: 94.52292966842651
time_elpased: 1.571
batch start
#iterations: 323
currently lose_sum: 94.42714023590088
time_elpased: 1.535
batch start
#iterations: 324
currently lose_sum: 94.5617687702179
time_elpased: 1.513
batch start
#iterations: 325
currently lose_sum: 94.68595135211945
time_elpased: 1.539
batch start
#iterations: 326
currently lose_sum: 94.08296310901642
time_elpased: 1.528
batch start
#iterations: 327
currently lose_sum: 94.22701668739319
time_elpased: 1.543
batch start
#iterations: 328
currently lose_sum: 94.2710200548172
time_elpased: 1.568
batch start
#iterations: 329
currently lose_sum: 94.15859818458557
time_elpased: 1.557
batch start
#iterations: 330
currently lose_sum: 94.61583077907562
time_elpased: 1.555
batch start
#iterations: 331
currently lose_sum: 94.37302833795547
time_elpased: 1.634
batch start
#iterations: 332
currently lose_sum: 94.27119559049606
time_elpased: 1.571
batch start
#iterations: 333
currently lose_sum: 94.20771461725235
time_elpased: 1.56
batch start
#iterations: 334
currently lose_sum: 94.5095345377922
time_elpased: 1.533
batch start
#iterations: 335
currently lose_sum: 94.00517141819
time_elpased: 1.531
batch start
#iterations: 336
currently lose_sum: 94.68961125612259
time_elpased: 1.545
batch start
#iterations: 337
currently lose_sum: 94.07897186279297
time_elpased: 1.558
batch start
#iterations: 338
currently lose_sum: 94.18322628736496
time_elpased: 1.547
batch start
#iterations: 339
currently lose_sum: 94.13103187084198
time_elpased: 1.53
start validation test
0.666340206186
0.645627581253
0.740043223217
0.689618796452
0.666210809171
60.663
batch start
#iterations: 340
currently lose_sum: 94.3762246966362
time_elpased: 1.538
batch start
#iterations: 341
currently lose_sum: 94.63435691595078
time_elpased: 1.558
batch start
#iterations: 342
currently lose_sum: 94.10661655664444
time_elpased: 1.51
batch start
#iterations: 343
currently lose_sum: 94.2404802441597
time_elpased: 1.517
batch start
#iterations: 344
currently lose_sum: 94.08906984329224
time_elpased: 1.538
batch start
#iterations: 345
currently lose_sum: 94.23435908555984
time_elpased: 1.531
batch start
#iterations: 346
currently lose_sum: 94.1014958024025
time_elpased: 1.51
batch start
#iterations: 347
currently lose_sum: 94.21929490566254
time_elpased: 1.54
batch start
#iterations: 348
currently lose_sum: 94.56372857093811
time_elpased: 1.551
batch start
#iterations: 349
currently lose_sum: 94.2938945889473
time_elpased: 1.541
batch start
#iterations: 350
currently lose_sum: 94.63319820165634
time_elpased: 1.517
batch start
#iterations: 351
currently lose_sum: 94.09436303377151
time_elpased: 1.595
batch start
#iterations: 352
currently lose_sum: 94.3350128531456
time_elpased: 1.562
batch start
#iterations: 353
currently lose_sum: 94.24356603622437
time_elpased: 1.584
batch start
#iterations: 354
currently lose_sum: 94.19430762529373
time_elpased: 1.539
batch start
#iterations: 355
currently lose_sum: 93.93035328388214
time_elpased: 1.582
batch start
#iterations: 356
currently lose_sum: 93.91935741901398
time_elpased: 1.543
batch start
#iterations: 357
currently lose_sum: 94.38685250282288
time_elpased: 1.61
batch start
#iterations: 358
currently lose_sum: 94.15973621606827
time_elpased: 1.551
batch start
#iterations: 359
currently lose_sum: 94.19545805454254
time_elpased: 1.524
start validation test
0.669278350515
0.63188174191
0.813831429454
0.711406980928
0.66902456529
60.109
batch start
#iterations: 360
currently lose_sum: 94.71040737628937
time_elpased: 1.549
batch start
#iterations: 361
currently lose_sum: 94.61087608337402
time_elpased: 1.563
batch start
#iterations: 362
currently lose_sum: 94.065986931324
time_elpased: 1.533
batch start
#iterations: 363
currently lose_sum: 94.345443546772
time_elpased: 1.56
batch start
#iterations: 364
currently lose_sum: 94.10703903436661
time_elpased: 1.548
batch start
#iterations: 365
currently lose_sum: 94.74286186695099
time_elpased: 1.546
batch start
#iterations: 366
currently lose_sum: 93.97734189033508
time_elpased: 1.524
batch start
#iterations: 367
currently lose_sum: 94.25212472677231
time_elpased: 1.559
batch start
#iterations: 368
currently lose_sum: 94.14851915836334
time_elpased: 1.506
batch start
#iterations: 369
currently lose_sum: 94.63039165735245
time_elpased: 1.554
batch start
#iterations: 370
currently lose_sum: 94.33675229549408
time_elpased: 1.565
batch start
#iterations: 371
currently lose_sum: 93.8514484167099
time_elpased: 1.543
batch start
#iterations: 372
currently lose_sum: 94.26523751020432
time_elpased: 1.554
batch start
#iterations: 373
currently lose_sum: 94.39603531360626
time_elpased: 1.532
batch start
#iterations: 374
currently lose_sum: 94.38757491111755
time_elpased: 1.509
batch start
#iterations: 375
currently lose_sum: 94.34129267930984
time_elpased: 1.507
batch start
#iterations: 376
currently lose_sum: 94.49407631158829
time_elpased: 1.515
batch start
#iterations: 377
currently lose_sum: 94.52661889791489
time_elpased: 1.567
batch start
#iterations: 378
currently lose_sum: 94.10774028301239
time_elpased: 1.585
batch start
#iterations: 379
currently lose_sum: 94.52542251348495
time_elpased: 1.602
start validation test
0.668505154639
0.649445151901
0.734794689719
0.689488677514
0.668388773136
60.400
batch start
#iterations: 380
currently lose_sum: 94.20612120628357
time_elpased: 1.493
batch start
#iterations: 381
currently lose_sum: 94.55971574783325
time_elpased: 1.553
batch start
#iterations: 382
currently lose_sum: 93.80245530605316
time_elpased: 1.555
batch start
#iterations: 383
currently lose_sum: 94.23765176534653
time_elpased: 1.528
batch start
#iterations: 384
currently lose_sum: 94.05121475458145
time_elpased: 1.608
batch start
#iterations: 385
currently lose_sum: 94.49003040790558
time_elpased: 1.554
batch start
#iterations: 386
currently lose_sum: 94.24805730581284
time_elpased: 1.571
batch start
#iterations: 387
currently lose_sum: 94.45466041564941
time_elpased: 1.532
batch start
#iterations: 388
currently lose_sum: 94.19261401891708
time_elpased: 1.538
batch start
#iterations: 389
currently lose_sum: 94.35283434391022
time_elpased: 1.581
batch start
#iterations: 390
currently lose_sum: 94.32981199026108
time_elpased: 1.522
batch start
#iterations: 391
currently lose_sum: 94.10729193687439
time_elpased: 1.587
batch start
#iterations: 392
currently lose_sum: 94.37554574012756
time_elpased: 1.55
batch start
#iterations: 393
currently lose_sum: 94.0113895535469
time_elpased: 1.584
batch start
#iterations: 394
currently lose_sum: 94.27075880765915
time_elpased: 1.574
batch start
#iterations: 395
currently lose_sum: 94.17052817344666
time_elpased: 1.551
batch start
#iterations: 396
currently lose_sum: 94.28612965345383
time_elpased: 1.56
batch start
#iterations: 397
currently lose_sum: 94.24813956022263
time_elpased: 1.552
batch start
#iterations: 398
currently lose_sum: 93.90669852495193
time_elpased: 1.581
batch start
#iterations: 399
currently lose_sum: 94.07753401994705
time_elpased: 1.55
start validation test
0.671082474227
0.644565782631
0.765359678913
0.699788285109
0.670916956053
60.215
acc: 0.668
pre: 0.637
rec: 0.784
F1: 0.703
auc: 0.704
