start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.13069772720337
time_elpased: 1.839
batch start
#iterations: 1
currently lose_sum: 99.96752798557281
time_elpased: 1.768
batch start
#iterations: 2
currently lose_sum: 99.27003663778305
time_elpased: 1.751
batch start
#iterations: 3
currently lose_sum: 98.98747086524963
time_elpased: 1.783
batch start
#iterations: 4
currently lose_sum: 98.79892909526825
time_elpased: 1.782
batch start
#iterations: 5
currently lose_sum: 98.77978801727295
time_elpased: 1.831
batch start
#iterations: 6
currently lose_sum: 98.61383306980133
time_elpased: 1.749
batch start
#iterations: 7
currently lose_sum: 98.30765724182129
time_elpased: 1.762
batch start
#iterations: 8
currently lose_sum: 98.1707792878151
time_elpased: 1.772
batch start
#iterations: 9
currently lose_sum: 98.01850944757462
time_elpased: 1.828
batch start
#iterations: 10
currently lose_sum: 97.87835657596588
time_elpased: 1.757
batch start
#iterations: 11
currently lose_sum: 97.60119235515594
time_elpased: 1.752
batch start
#iterations: 12
currently lose_sum: 97.48521327972412
time_elpased: 1.785
batch start
#iterations: 13
currently lose_sum: 97.73726779222488
time_elpased: 1.763
batch start
#iterations: 14
currently lose_sum: 97.35908728837967
time_elpased: 1.779
batch start
#iterations: 15
currently lose_sum: 97.30578124523163
time_elpased: 1.743
batch start
#iterations: 16
currently lose_sum: 97.50128084421158
time_elpased: 1.755
batch start
#iterations: 17
currently lose_sum: 97.13364106416702
time_elpased: 1.853
batch start
#iterations: 18
currently lose_sum: 97.13583225011826
time_elpased: 1.797
batch start
#iterations: 19
currently lose_sum: 97.03935128450394
time_elpased: 1.779
start validation test
0.637680412371
0.650870297586
0.596541786744
0.622522957951
0.637748382013
62.908
batch start
#iterations: 20
currently lose_sum: 96.75749790668488
time_elpased: 1.766
batch start
#iterations: 21
currently lose_sum: 96.57141816616058
time_elpased: 1.75
batch start
#iterations: 22
currently lose_sum: 96.68233698606491
time_elpased: 1.778
batch start
#iterations: 23
currently lose_sum: 96.79049438238144
time_elpased: 1.791
batch start
#iterations: 24
currently lose_sum: 96.56776833534241
time_elpased: 1.823
batch start
#iterations: 25
currently lose_sum: 96.6736649274826
time_elpased: 1.869
batch start
#iterations: 26
currently lose_sum: 96.79491370916367
time_elpased: 1.788
batch start
#iterations: 27
currently lose_sum: 96.90115505456924
time_elpased: 1.811
batch start
#iterations: 28
currently lose_sum: 96.4975516796112
time_elpased: 1.761
batch start
#iterations: 29
currently lose_sum: 96.61737430095673
time_elpased: 1.761
batch start
#iterations: 30
currently lose_sum: 96.25651985406876
time_elpased: 1.808
batch start
#iterations: 31
currently lose_sum: 96.627878844738
time_elpased: 1.789
batch start
#iterations: 32
currently lose_sum: 96.27898406982422
time_elpased: 1.834
batch start
#iterations: 33
currently lose_sum: 96.41200983524323
time_elpased: 1.786
batch start
#iterations: 34
currently lose_sum: 96.28519207239151
time_elpased: 1.828
batch start
#iterations: 35
currently lose_sum: 96.48697674274445
time_elpased: 1.786
batch start
#iterations: 36
currently lose_sum: 96.10042244195938
time_elpased: 1.79
batch start
#iterations: 37
currently lose_sum: 96.31913030147552
time_elpased: 1.826
batch start
#iterations: 38
currently lose_sum: 96.48214161396027
time_elpased: 1.753
batch start
#iterations: 39
currently lose_sum: 96.31669932603836
time_elpased: 1.794
start validation test
0.656340206186
0.63946573964
0.719431864965
0.677095946142
0.656235965527
62.002
batch start
#iterations: 40
currently lose_sum: 96.04422390460968
time_elpased: 1.772
batch start
#iterations: 41
currently lose_sum: 96.61158114671707
time_elpased: 1.856
batch start
#iterations: 42
currently lose_sum: 96.11504089832306
time_elpased: 1.784
batch start
#iterations: 43
currently lose_sum: 96.18278867006302
time_elpased: 1.743
batch start
#iterations: 44
currently lose_sum: 96.37404376268387
time_elpased: 1.77
batch start
#iterations: 45
currently lose_sum: 96.1381886601448
time_elpased: 1.761
batch start
#iterations: 46
currently lose_sum: 96.31809800863266
time_elpased: 1.739
batch start
#iterations: 47
currently lose_sum: 96.02865713834763
time_elpased: 1.778
batch start
#iterations: 48
currently lose_sum: 96.15258210897446
time_elpased: 1.839
batch start
#iterations: 49
currently lose_sum: 95.97927612066269
time_elpased: 1.776
batch start
#iterations: 50
currently lose_sum: 95.92846083641052
time_elpased: 1.787
batch start
#iterations: 51
currently lose_sum: 95.41228538751602
time_elpased: 1.796
batch start
#iterations: 52
currently lose_sum: 96.17005902528763
time_elpased: 1.789
batch start
#iterations: 53
currently lose_sum: 95.9941514134407
time_elpased: 1.785
batch start
#iterations: 54
currently lose_sum: 95.89335417747498
time_elpased: 1.788
batch start
#iterations: 55
currently lose_sum: 95.99938005208969
time_elpased: 1.779
batch start
#iterations: 56
currently lose_sum: 95.86896342039108
time_elpased: 1.78
batch start
#iterations: 57
currently lose_sum: 96.15773832798004
time_elpased: 1.78
batch start
#iterations: 58
currently lose_sum: 95.87640780210495
time_elpased: 1.767
batch start
#iterations: 59
currently lose_sum: 95.66299313306808
time_elpased: 1.812
start validation test
0.654536082474
0.652530364372
0.663544668588
0.657991426822
0.6545211984
61.730
batch start
#iterations: 60
currently lose_sum: 95.95501214265823
time_elpased: 1.819
batch start
#iterations: 61
currently lose_sum: 96.25178903341293
time_elpased: 1.779
batch start
#iterations: 62
currently lose_sum: 95.90366393327713
time_elpased: 1.833
batch start
#iterations: 63
currently lose_sum: 95.65470921993256
time_elpased: 1.782
batch start
#iterations: 64
currently lose_sum: 95.97514128684998
time_elpased: 1.769
batch start
#iterations: 65
currently lose_sum: 95.69107019901276
time_elpased: 1.822
batch start
#iterations: 66
currently lose_sum: 95.83861654996872
time_elpased: 1.786
batch start
#iterations: 67
currently lose_sum: 95.62784159183502
time_elpased: 1.788
batch start
#iterations: 68
currently lose_sum: 95.78246682882309
time_elpased: 1.776
batch start
#iterations: 69
currently lose_sum: 96.05723094940186
time_elpased: 1.811
batch start
#iterations: 70
currently lose_sum: 95.99371176958084
time_elpased: 1.77
batch start
#iterations: 71
currently lose_sum: 95.72852838039398
time_elpased: 1.81
batch start
#iterations: 72
currently lose_sum: 95.43509268760681
time_elpased: 1.783
batch start
#iterations: 73
currently lose_sum: 95.63475376367569
time_elpased: 1.767
batch start
#iterations: 74
currently lose_sum: 95.71686577796936
time_elpased: 1.775
batch start
#iterations: 75
currently lose_sum: 95.85017746686935
time_elpased: 1.778
batch start
#iterations: 76
currently lose_sum: 95.77780485153198
time_elpased: 1.769
batch start
#iterations: 77
currently lose_sum: 95.61759215593338
time_elpased: 1.839
batch start
#iterations: 78
currently lose_sum: 96.06977671384811
time_elpased: 1.78
batch start
#iterations: 79
currently lose_sum: 95.32584089040756
time_elpased: 1.765
start validation test
0.662422680412
0.631051891087
0.784787978592
0.699573374925
0.662220507264
61.011
batch start
#iterations: 80
currently lose_sum: 95.85079699754715
time_elpased: 1.779
batch start
#iterations: 81
currently lose_sum: 95.84695935249329
time_elpased: 1.809
batch start
#iterations: 82
currently lose_sum: 95.31495463848114
time_elpased: 1.795
batch start
#iterations: 83
currently lose_sum: 95.53290963172913
time_elpased: 1.752
batch start
#iterations: 84
currently lose_sum: 95.52134174108505
time_elpased: 1.808
batch start
#iterations: 85
currently lose_sum: 95.5510396361351
time_elpased: 1.773
batch start
#iterations: 86
currently lose_sum: 95.33058446645737
time_elpased: 1.771
batch start
#iterations: 87
currently lose_sum: 95.60630595684052
time_elpased: 1.807
batch start
#iterations: 88
currently lose_sum: 95.78947359323502
time_elpased: 1.776
batch start
#iterations: 89
currently lose_sum: 95.8084671497345
time_elpased: 1.77
batch start
#iterations: 90
currently lose_sum: 95.70796370506287
time_elpased: 1.759
batch start
#iterations: 91
currently lose_sum: 95.49615347385406
time_elpased: 1.776
batch start
#iterations: 92
currently lose_sum: 95.46197497844696
time_elpased: 1.754
batch start
#iterations: 93
currently lose_sum: 95.48377126455307
time_elpased: 1.736
batch start
#iterations: 94
currently lose_sum: 95.65697646141052
time_elpased: 1.765
batch start
#iterations: 95
currently lose_sum: 95.85786801576614
time_elpased: 1.778
batch start
#iterations: 96
currently lose_sum: 95.64561998844147
time_elpased: 1.82
batch start
#iterations: 97
currently lose_sum: 95.29216474294662
time_elpased: 1.795
batch start
#iterations: 98
currently lose_sum: 95.75986057519913
time_elpased: 1.738
batch start
#iterations: 99
currently lose_sum: 95.80279129743576
time_elpased: 1.732
start validation test
0.665
0.655125855917
0.699156031289
0.67642519293
0.664943567069
61.411
batch start
#iterations: 100
currently lose_sum: 95.59982579946518
time_elpased: 1.802
batch start
#iterations: 101
currently lose_sum: 95.47425323724747
time_elpased: 1.843
batch start
#iterations: 102
currently lose_sum: 95.50469321012497
time_elpased: 1.768
batch start
#iterations: 103
currently lose_sum: 95.27138847112656
time_elpased: 1.795
batch start
#iterations: 104
currently lose_sum: 95.22684299945831
time_elpased: 1.774
batch start
#iterations: 105
currently lose_sum: 95.47206687927246
time_elpased: 1.773
batch start
#iterations: 106
currently lose_sum: 95.28757864236832
time_elpased: 1.852
batch start
#iterations: 107
currently lose_sum: 95.43295747041702
time_elpased: 1.813
batch start
#iterations: 108
currently lose_sum: 95.23754268884659
time_elpased: 1.808
batch start
#iterations: 109
currently lose_sum: 95.4551819562912
time_elpased: 1.745
batch start
#iterations: 110
currently lose_sum: 95.21725034713745
time_elpased: 1.785
batch start
#iterations: 111
currently lose_sum: 95.33331906795502
time_elpased: 1.769
batch start
#iterations: 112
currently lose_sum: 95.17902463674545
time_elpased: 1.768
batch start
#iterations: 113
currently lose_sum: 95.26552730798721
time_elpased: 1.745
batch start
#iterations: 114
currently lose_sum: 95.59372818470001
time_elpased: 1.781
batch start
#iterations: 115
currently lose_sum: 95.20080816745758
time_elpased: 1.787
batch start
#iterations: 116
currently lose_sum: 95.47042739391327
time_elpased: 1.784
batch start
#iterations: 117
currently lose_sum: 95.16328376531601
time_elpased: 1.788
batch start
#iterations: 118
currently lose_sum: 95.44917768239975
time_elpased: 1.819
batch start
#iterations: 119
currently lose_sum: 95.48219293355942
time_elpased: 1.745
start validation test
0.652164948454
0.656079091292
0.642033758748
0.648980441115
0.652181687305
61.653
batch start
#iterations: 120
currently lose_sum: 95.04391986131668
time_elpased: 1.723
batch start
#iterations: 121
currently lose_sum: 94.85905021429062
time_elpased: 1.772
batch start
#iterations: 122
currently lose_sum: 95.4194228053093
time_elpased: 1.721
batch start
#iterations: 123
currently lose_sum: 95.57739913463593
time_elpased: 1.792
batch start
#iterations: 124
currently lose_sum: 95.42404454946518
time_elpased: 1.789
batch start
#iterations: 125
currently lose_sum: 95.43986237049103
time_elpased: 1.789
batch start
#iterations: 126
currently lose_sum: 94.96370881795883
time_elpased: 1.776
batch start
#iterations: 127
currently lose_sum: 95.3839790225029
time_elpased: 1.747
batch start
#iterations: 128
currently lose_sum: 94.96170729398727
time_elpased: 1.76
batch start
#iterations: 129
currently lose_sum: 95.56010687351227
time_elpased: 1.725
batch start
#iterations: 130
currently lose_sum: 95.30703449249268
time_elpased: 1.804
batch start
#iterations: 131
currently lose_sum: 94.851005256176
time_elpased: 1.825
batch start
#iterations: 132
currently lose_sum: 95.25865995883942
time_elpased: 1.739
batch start
#iterations: 133
currently lose_sum: 95.49468237161636
time_elpased: 1.772
batch start
#iterations: 134
currently lose_sum: 95.20252645015717
time_elpased: 1.763
batch start
#iterations: 135
currently lose_sum: 95.57565528154373
time_elpased: 1.78
batch start
#iterations: 136
currently lose_sum: 95.48256266117096
time_elpased: 1.797
batch start
#iterations: 137
currently lose_sum: 95.39521235227585
time_elpased: 1.772
batch start
#iterations: 138
currently lose_sum: 95.2239590883255
time_elpased: 1.789
batch start
#iterations: 139
currently lose_sum: 95.20146995782852
time_elpased: 1.733
start validation test
0.655515463918
0.63740146779
0.724063400576
0.67797426878
0.655402208343
61.300
batch start
#iterations: 140
currently lose_sum: 94.92834877967834
time_elpased: 1.812
batch start
#iterations: 141
currently lose_sum: 95.07627111673355
time_elpased: 1.77
batch start
#iterations: 142
currently lose_sum: 95.21847188472748
time_elpased: 1.78
batch start
#iterations: 143
currently lose_sum: 95.17914098501205
time_elpased: 1.81
batch start
#iterations: 144
currently lose_sum: 95.28265732526779
time_elpased: 1.796
batch start
#iterations: 145
currently lose_sum: 95.2461810708046
time_elpased: 1.805
batch start
#iterations: 146
currently lose_sum: 94.97000896930695
time_elpased: 1.759
batch start
#iterations: 147
currently lose_sum: 95.37215578556061
time_elpased: 1.795
batch start
#iterations: 148
currently lose_sum: 95.40424448251724
time_elpased: 1.755
batch start
#iterations: 149
currently lose_sum: 95.21977078914642
time_elpased: 1.767
batch start
#iterations: 150
currently lose_sum: 95.07774883508682
time_elpased: 1.764
batch start
#iterations: 151
currently lose_sum: 95.495758831501
time_elpased: 1.735
batch start
#iterations: 152
currently lose_sum: 95.00536113977432
time_elpased: 1.823
batch start
#iterations: 153
currently lose_sum: 95.36805486679077
time_elpased: 1.787
batch start
#iterations: 154
currently lose_sum: 94.90511006116867
time_elpased: 1.779
batch start
#iterations: 155
currently lose_sum: 94.78137594461441
time_elpased: 1.75
batch start
#iterations: 156
currently lose_sum: 95.3821627497673
time_elpased: 1.748
batch start
#iterations: 157
currently lose_sum: 94.85225331783295
time_elpased: 1.76
batch start
#iterations: 158
currently lose_sum: 95.38986510038376
time_elpased: 1.749
batch start
#iterations: 159
currently lose_sum: 95.02011275291443
time_elpased: 1.746
start validation test
0.660463917526
0.639899847984
0.73651708522
0.684817455381
0.660338261734
61.244
batch start
#iterations: 160
currently lose_sum: 94.96146237850189
time_elpased: 1.738
batch start
#iterations: 161
currently lose_sum: 95.37535512447357
time_elpased: 1.777
batch start
#iterations: 162
currently lose_sum: 95.43502032756805
time_elpased: 1.755
batch start
#iterations: 163
currently lose_sum: 95.12284827232361
time_elpased: 1.742
batch start
#iterations: 164
currently lose_sum: 95.31284809112549
time_elpased: 1.832
batch start
#iterations: 165
currently lose_sum: 95.09720528125763
time_elpased: 1.734
batch start
#iterations: 166
currently lose_sum: 95.04922330379486
time_elpased: 1.741
batch start
#iterations: 167
currently lose_sum: 95.21589404344559
time_elpased: 1.769
batch start
#iterations: 168
currently lose_sum: 95.08617973327637
time_elpased: 1.766
batch start
#iterations: 169
currently lose_sum: 95.08251535892487
time_elpased: 1.803
batch start
#iterations: 170
currently lose_sum: 94.82979309558868
time_elpased: 1.742
batch start
#iterations: 171
currently lose_sum: 94.84522181749344
time_elpased: 1.737
batch start
#iterations: 172
currently lose_sum: 94.74651849269867
time_elpased: 1.745
batch start
#iterations: 173
currently lose_sum: 94.92109870910645
time_elpased: 1.829
batch start
#iterations: 174
currently lose_sum: 95.4017105102539
time_elpased: 1.79
batch start
#iterations: 175
currently lose_sum: 95.24925225973129
time_elpased: 1.789
batch start
#iterations: 176
currently lose_sum: 95.12307316064835
time_elpased: 1.795
batch start
#iterations: 177
currently lose_sum: 94.88486808538437
time_elpased: 1.765
batch start
#iterations: 178
currently lose_sum: 94.81828910112381
time_elpased: 1.763
batch start
#iterations: 179
currently lose_sum: 95.2497193813324
time_elpased: 1.776
start validation test
0.65881443299
0.64486855646
0.709448332647
0.675618720902
0.658730775163
61.065
batch start
#iterations: 180
currently lose_sum: 94.77795892953873
time_elpased: 1.757
batch start
#iterations: 181
currently lose_sum: 95.07071667909622
time_elpased: 1.738
batch start
#iterations: 182
currently lose_sum: 95.56453746557236
time_elpased: 1.772
batch start
#iterations: 183
currently lose_sum: 94.8415339589119
time_elpased: 1.735
batch start
#iterations: 184
currently lose_sum: 94.90015524625778
time_elpased: 1.774
batch start
#iterations: 185
currently lose_sum: 95.14435237646103
time_elpased: 1.735
batch start
#iterations: 186
currently lose_sum: 94.96106815338135
time_elpased: 1.807
batch start
#iterations: 187
currently lose_sum: 94.91323584318161
time_elpased: 1.757
batch start
#iterations: 188
currently lose_sum: 95.22519654035568
time_elpased: 1.799
batch start
#iterations: 189
currently lose_sum: 94.94706493616104
time_elpased: 1.734
batch start
#iterations: 190
currently lose_sum: 95.27643555402756
time_elpased: 1.764
batch start
#iterations: 191
currently lose_sum: 95.05143070220947
time_elpased: 1.754
batch start
#iterations: 192
currently lose_sum: 94.97309678792953
time_elpased: 1.738
batch start
#iterations: 193
currently lose_sum: 94.70594781637192
time_elpased: 1.785
batch start
#iterations: 194
currently lose_sum: 94.79014015197754
time_elpased: 1.75
batch start
#iterations: 195
currently lose_sum: 94.63211381435394
time_elpased: 1.754
batch start
#iterations: 196
currently lose_sum: 94.7102724313736
time_elpased: 1.762
batch start
#iterations: 197
currently lose_sum: 94.72345739603043
time_elpased: 1.763
batch start
#iterations: 198
currently lose_sum: 94.84316444396973
time_elpased: 1.747
batch start
#iterations: 199
currently lose_sum: 94.87598752975464
time_elpased: 1.796
start validation test
0.669020618557
0.649297689171
0.737443392343
0.690569129199
0.668907569777
60.888
batch start
#iterations: 200
currently lose_sum: 95.15325689315796
time_elpased: 1.808
batch start
#iterations: 201
currently lose_sum: 94.67106294631958
time_elpased: 1.728
batch start
#iterations: 202
currently lose_sum: 95.4088721871376
time_elpased: 1.759
batch start
#iterations: 203
currently lose_sum: 95.06345516443253
time_elpased: 1.781
batch start
#iterations: 204
currently lose_sum: 95.18548673391342
time_elpased: 1.802
batch start
#iterations: 205
currently lose_sum: 95.2255630493164
time_elpased: 1.762
batch start
#iterations: 206
currently lose_sum: 94.89126771688461
time_elpased: 1.789
batch start
#iterations: 207
currently lose_sum: 94.86880660057068
time_elpased: 1.763
batch start
#iterations: 208
currently lose_sum: 94.44040507078171
time_elpased: 1.724
batch start
#iterations: 209
currently lose_sum: 94.55703645944595
time_elpased: 1.793
batch start
#iterations: 210
currently lose_sum: 95.04868996143341
time_elpased: 1.794
batch start
#iterations: 211
currently lose_sum: 94.71171927452087
time_elpased: 1.794
batch start
#iterations: 212
currently lose_sum: 94.83896046876907
time_elpased: 1.755
batch start
#iterations: 213
currently lose_sum: 94.43571382761002
time_elpased: 1.784
batch start
#iterations: 214
currently lose_sum: 94.81226736307144
time_elpased: 1.812
batch start
#iterations: 215
currently lose_sum: 95.12604081630707
time_elpased: 1.776
batch start
#iterations: 216
currently lose_sum: 94.65043425559998
time_elpased: 1.759
batch start
#iterations: 217
currently lose_sum: 94.87113857269287
time_elpased: 1.788
batch start
#iterations: 218
currently lose_sum: 94.83082729578018
time_elpased: 1.799
batch start
#iterations: 219
currently lose_sum: 94.873240172863
time_elpased: 1.789
start validation test
0.665412371134
0.637973817062
0.767393989296
0.696724758211
0.665243876102
60.753
batch start
#iterations: 220
currently lose_sum: 95.02544051408768
time_elpased: 1.777
batch start
#iterations: 221
currently lose_sum: 94.48615980148315
time_elpased: 1.726
batch start
#iterations: 222
currently lose_sum: 94.63070285320282
time_elpased: 1.77
batch start
#iterations: 223
currently lose_sum: 94.13569587469101
time_elpased: 1.736
batch start
#iterations: 224
currently lose_sum: 94.85738831758499
time_elpased: 1.772
batch start
#iterations: 225
currently lose_sum: 94.65666711330414
time_elpased: 1.748
batch start
#iterations: 226
currently lose_sum: 94.69626224040985
time_elpased: 1.754
batch start
#iterations: 227
currently lose_sum: 94.60002398490906
time_elpased: 1.787
batch start
#iterations: 228
currently lose_sum: 94.9438938498497
time_elpased: 1.789
batch start
#iterations: 229
currently lose_sum: 94.98968148231506
time_elpased: 1.768
batch start
#iterations: 230
currently lose_sum: 94.69238805770874
time_elpased: 1.783
batch start
#iterations: 231
currently lose_sum: 94.83835458755493
time_elpased: 1.769
batch start
#iterations: 232
currently lose_sum: 95.005246758461
time_elpased: 1.745
batch start
#iterations: 233
currently lose_sum: 95.16178238391876
time_elpased: 1.733
batch start
#iterations: 234
currently lose_sum: 94.79154926538467
time_elpased: 1.779
batch start
#iterations: 235
currently lose_sum: 95.15423285961151
time_elpased: 1.771
batch start
#iterations: 236
currently lose_sum: 94.90888285636902
time_elpased: 1.771
batch start
#iterations: 237
currently lose_sum: 94.76620399951935
time_elpased: 1.801
batch start
#iterations: 238
currently lose_sum: 94.49531626701355
time_elpased: 1.821
batch start
#iterations: 239
currently lose_sum: 95.08270531892776
time_elpased: 1.787
start validation test
0.663865979381
0.644165689017
0.734664470976
0.68644516036
0.663749005418
61.069
batch start
#iterations: 240
currently lose_sum: 95.03029644489288
time_elpased: 1.775
batch start
#iterations: 241
currently lose_sum: 94.47269767522812
time_elpased: 1.821
batch start
#iterations: 242
currently lose_sum: 94.81785899400711
time_elpased: 1.807
batch start
#iterations: 243
currently lose_sum: 94.7888395190239
time_elpased: 1.763
batch start
#iterations: 244
currently lose_sum: 94.62049704790115
time_elpased: 1.76
batch start
#iterations: 245
currently lose_sum: 94.98618668317795
time_elpased: 1.779
batch start
#iterations: 246
currently lose_sum: 94.2167683839798
time_elpased: 1.807
batch start
#iterations: 247
currently lose_sum: 94.82821422815323
time_elpased: 1.76
batch start
#iterations: 248
currently lose_sum: 94.82952374219894
time_elpased: 1.757
batch start
#iterations: 249
currently lose_sum: 95.39065432548523
time_elpased: 1.758
batch start
#iterations: 250
currently lose_sum: 94.6748788356781
time_elpased: 1.753
batch start
#iterations: 251
currently lose_sum: 94.80343687534332
time_elpased: 1.794
batch start
#iterations: 252
currently lose_sum: 94.4202686548233
time_elpased: 1.789
batch start
#iterations: 253
currently lose_sum: 94.99562156200409
time_elpased: 1.792
batch start
#iterations: 254
currently lose_sum: 94.86089080572128
time_elpased: 1.784
batch start
#iterations: 255
currently lose_sum: 94.66632652282715
time_elpased: 1.784
batch start
#iterations: 256
currently lose_sum: 94.32311499118805
time_elpased: 1.791
batch start
#iterations: 257
currently lose_sum: 94.8877375125885
time_elpased: 1.8
batch start
#iterations: 258
currently lose_sum: 94.64470195770264
time_elpased: 1.785
batch start
#iterations: 259
currently lose_sum: 94.56128042936325
time_elpased: 1.732
start validation test
0.629948453608
0.664462595618
0.527480444627
0.588100292616
0.63011775226
62.060
batch start
#iterations: 260
currently lose_sum: 94.58865475654602
time_elpased: 1.774
batch start
#iterations: 261
currently lose_sum: 94.71644616127014
time_elpased: 1.746
batch start
#iterations: 262
currently lose_sum: 94.82459795475006
time_elpased: 1.728
batch start
#iterations: 263
currently lose_sum: 94.47834414243698
time_elpased: 1.748
batch start
#iterations: 264
currently lose_sum: 94.85714024305344
time_elpased: 1.78
batch start
#iterations: 265
currently lose_sum: 94.9396978020668
time_elpased: 1.765
batch start
#iterations: 266
currently lose_sum: 94.19514155387878
time_elpased: 1.79
batch start
#iterations: 267
currently lose_sum: 94.71272897720337
time_elpased: 1.779
batch start
#iterations: 268
currently lose_sum: 94.37399011850357
time_elpased: 1.718
batch start
#iterations: 269
currently lose_sum: 95.19135683774948
time_elpased: 1.769
batch start
#iterations: 270
currently lose_sum: 94.35892975330353
time_elpased: 1.768
batch start
#iterations: 271
currently lose_sum: 94.39264822006226
time_elpased: 1.749
batch start
#iterations: 272
currently lose_sum: 94.74966585636139
time_elpased: 1.833
batch start
#iterations: 273
currently lose_sum: 94.7867940068245
time_elpased: 1.777
batch start
#iterations: 274
currently lose_sum: 94.70279830694199
time_elpased: 1.774
batch start
#iterations: 275
currently lose_sum: 94.41755908727646
time_elpased: 1.752
batch start
#iterations: 276
currently lose_sum: 94.57666802406311
time_elpased: 1.772
batch start
#iterations: 277
currently lose_sum: 94.26423954963684
time_elpased: 1.819
batch start
#iterations: 278
currently lose_sum: 94.72916781902313
time_elpased: 1.792
batch start
#iterations: 279
currently lose_sum: 94.8101459145546
time_elpased: 1.788
start validation test
0.666546391753
0.63811144194
0.772025524907
0.698709887756
0.666372118092
60.778
batch start
#iterations: 280
currently lose_sum: 94.64845907688141
time_elpased: 1.798
batch start
#iterations: 281
currently lose_sum: 94.50617057085037
time_elpased: 1.746
batch start
#iterations: 282
currently lose_sum: 94.66576063632965
time_elpased: 1.78
batch start
#iterations: 283
currently lose_sum: 94.34802705049515
time_elpased: 1.86
batch start
#iterations: 284
currently lose_sum: 94.28113847970963
time_elpased: 1.797
batch start
#iterations: 285
currently lose_sum: 94.45463472604752
time_elpased: 1.761
batch start
#iterations: 286
currently lose_sum: 94.02606171369553
time_elpased: 1.753
batch start
#iterations: 287
currently lose_sum: 94.61703366041183
time_elpased: 1.768
batch start
#iterations: 288
currently lose_sum: 94.54667240381241
time_elpased: 1.769
batch start
#iterations: 289
currently lose_sum: 94.71623015403748
time_elpased: 1.752
batch start
#iterations: 290
currently lose_sum: 94.57198983430862
time_elpased: 1.74
batch start
#iterations: 291
currently lose_sum: 94.63435381650925
time_elpased: 1.811
batch start
#iterations: 292
currently lose_sum: 94.29883635044098
time_elpased: 1.76
batch start
#iterations: 293
currently lose_sum: 94.73305869102478
time_elpased: 1.76
batch start
#iterations: 294
currently lose_sum: 94.7060877084732
time_elpased: 1.788
batch start
#iterations: 295
currently lose_sum: 94.49050837755203
time_elpased: 1.765
batch start
#iterations: 296
currently lose_sum: 94.52752566337585
time_elpased: 1.789
batch start
#iterations: 297
currently lose_sum: 94.97940629720688
time_elpased: 1.766
batch start
#iterations: 298
currently lose_sum: 94.69691622257233
time_elpased: 1.802
batch start
#iterations: 299
currently lose_sum: 94.71014869213104
time_elpased: 1.771
start validation test
0.665154639175
0.634953897737
0.779641827913
0.699898364594
0.664965482317
60.514
batch start
#iterations: 300
currently lose_sum: 94.67245191335678
time_elpased: 1.754
batch start
#iterations: 301
currently lose_sum: 94.13017600774765
time_elpased: 1.833
batch start
#iterations: 302
currently lose_sum: 94.63863694667816
time_elpased: 1.77
batch start
#iterations: 303
currently lose_sum: 94.0380249619484
time_elpased: 1.779
batch start
#iterations: 304
currently lose_sum: 94.19230818748474
time_elpased: 1.727
batch start
#iterations: 305
currently lose_sum: 94.66087436676025
time_elpased: 1.765
batch start
#iterations: 306
currently lose_sum: 94.6008015871048
time_elpased: 1.777
batch start
#iterations: 307
currently lose_sum: 94.48011833429337
time_elpased: 1.782
batch start
#iterations: 308
currently lose_sum: 94.38608819246292
time_elpased: 1.796
batch start
#iterations: 309
currently lose_sum: 94.4433131814003
time_elpased: 1.741
batch start
#iterations: 310
currently lose_sum: 94.65487277507782
time_elpased: 1.769
batch start
#iterations: 311
currently lose_sum: 94.23535251617432
time_elpased: 1.774
batch start
#iterations: 312
currently lose_sum: 94.63336527347565
time_elpased: 1.775
batch start
#iterations: 313
currently lose_sum: 94.191410779953
time_elpased: 1.774
batch start
#iterations: 314
currently lose_sum: 94.20032382011414
time_elpased: 1.814
batch start
#iterations: 315
currently lose_sum: 94.07220786809921
time_elpased: 1.745
batch start
#iterations: 316
currently lose_sum: 94.27192199230194
time_elpased: 1.758
batch start
#iterations: 317
currently lose_sum: 94.72399926185608
time_elpased: 1.771
batch start
#iterations: 318
currently lose_sum: 93.99110263586044
time_elpased: 1.788
batch start
#iterations: 319
currently lose_sum: 94.39653581380844
time_elpased: 1.771
start validation test
0.668711340206
0.633060927259
0.805269658296
0.70885617214
0.668485717211
60.394
batch start
#iterations: 320
currently lose_sum: 94.5925355553627
time_elpased: 1.792
batch start
#iterations: 321
currently lose_sum: 94.66153866052628
time_elpased: 1.761
batch start
#iterations: 322
currently lose_sum: 94.53401672840118
time_elpased: 1.759
batch start
#iterations: 323
currently lose_sum: 94.2681491971016
time_elpased: 1.757
batch start
#iterations: 324
currently lose_sum: 94.39282923936844
time_elpased: 1.778
batch start
#iterations: 325
currently lose_sum: 94.19014900922775
time_elpased: 1.747
batch start
#iterations: 326
currently lose_sum: 94.35006594657898
time_elpased: 1.821
batch start
#iterations: 327
currently lose_sum: 94.73377132415771
time_elpased: 1.807
batch start
#iterations: 328
currently lose_sum: 94.54612284898758
time_elpased: 1.807
batch start
#iterations: 329
currently lose_sum: 94.47260820865631
time_elpased: 1.744
batch start
#iterations: 330
currently lose_sum: 94.6787416934967
time_elpased: 1.751
batch start
#iterations: 331
currently lose_sum: 94.35769236087799
time_elpased: 1.802
batch start
#iterations: 332
currently lose_sum: 94.15523731708527
time_elpased: 1.774
batch start
#iterations: 333
currently lose_sum: 94.62847691774368
time_elpased: 1.773
batch start
#iterations: 334
currently lose_sum: 94.35043752193451
time_elpased: 1.769
batch start
#iterations: 335
currently lose_sum: 94.43233674764633
time_elpased: 1.818
batch start
#iterations: 336
currently lose_sum: 94.24611300230026
time_elpased: 1.741
batch start
#iterations: 337
currently lose_sum: 94.6335843205452
time_elpased: 1.788
batch start
#iterations: 338
currently lose_sum: 94.18627244234085
time_elpased: 1.786
batch start
#iterations: 339
currently lose_sum: 94.311254799366
time_elpased: 1.788
start validation test
0.664175257732
0.638703527169
0.758542610128
0.693483886144
0.664019343065
60.615
batch start
#iterations: 340
currently lose_sum: 95.05572044849396
time_elpased: 1.765
batch start
#iterations: 341
currently lose_sum: 94.49395418167114
time_elpased: 1.788
batch start
#iterations: 342
currently lose_sum: 94.50795274972916
time_elpased: 1.782
batch start
#iterations: 343
currently lose_sum: 94.73876965045929
time_elpased: 1.737
batch start
#iterations: 344
currently lose_sum: 94.4275352358818
time_elpased: 1.826
batch start
#iterations: 345
currently lose_sum: 94.75619775056839
time_elpased: 1.745
batch start
#iterations: 346
currently lose_sum: 94.76716607809067
time_elpased: 1.738
batch start
#iterations: 347
currently lose_sum: 94.4398341178894
time_elpased: 1.772
batch start
#iterations: 348
currently lose_sum: 94.53916519880295
time_elpased: 1.771
batch start
#iterations: 349
currently lose_sum: 94.33693534135818
time_elpased: 1.751
batch start
#iterations: 350
currently lose_sum: 94.76901441812515
time_elpased: 1.768
batch start
#iterations: 351
currently lose_sum: 94.43132489919662
time_elpased: 1.805
batch start
#iterations: 352
currently lose_sum: 94.18339371681213
time_elpased: 1.767
batch start
#iterations: 353
currently lose_sum: 94.60055124759674
time_elpased: 1.787
batch start
#iterations: 354
currently lose_sum: 94.66778922080994
time_elpased: 1.758
batch start
#iterations: 355
currently lose_sum: 94.27067774534225
time_elpased: 1.781
batch start
#iterations: 356
currently lose_sum: 94.09774309396744
time_elpased: 1.81
batch start
#iterations: 357
currently lose_sum: 94.32405561208725
time_elpased: 1.759
batch start
#iterations: 358
currently lose_sum: 94.62616115808487
time_elpased: 1.718
batch start
#iterations: 359
currently lose_sum: 93.98010671138763
time_elpased: 1.778
start validation test
0.66618556701
0.658450704225
0.69287772746
0.675225677031
0.66614146596
60.978
batch start
#iterations: 360
currently lose_sum: 94.45966464281082
time_elpased: 1.827
batch start
#iterations: 361
currently lose_sum: 94.42048597335815
time_elpased: 1.792
batch start
#iterations: 362
currently lose_sum: 94.67942005395889
time_elpased: 1.751
batch start
#iterations: 363
currently lose_sum: 94.38206934928894
time_elpased: 1.73
batch start
#iterations: 364
currently lose_sum: 94.73502838611603
time_elpased: 1.795
batch start
#iterations: 365
currently lose_sum: 94.43216043710709
time_elpased: 1.781
batch start
#iterations: 366
currently lose_sum: 94.51780092716217
time_elpased: 1.76
batch start
#iterations: 367
currently lose_sum: 94.38107961416245
time_elpased: 1.775
batch start
#iterations: 368
currently lose_sum: 94.18430250883102
time_elpased: 1.776
batch start
#iterations: 369
currently lose_sum: 94.39491856098175
time_elpased: 1.808
batch start
#iterations: 370
currently lose_sum: 94.64713776111603
time_elpased: 1.721
batch start
#iterations: 371
currently lose_sum: 94.30690097808838
time_elpased: 1.808
batch start
#iterations: 372
currently lose_sum: 94.3898703455925
time_elpased: 1.771
batch start
#iterations: 373
currently lose_sum: 94.0535939335823
time_elpased: 1.765
batch start
#iterations: 374
currently lose_sum: 94.28204613924026
time_elpased: 1.747
batch start
#iterations: 375
currently lose_sum: 94.67413222789764
time_elpased: 1.816
batch start
#iterations: 376
currently lose_sum: 94.25848829746246
time_elpased: 1.747
batch start
#iterations: 377
currently lose_sum: 94.27873581647873
time_elpased: 1.735
batch start
#iterations: 378
currently lose_sum: 94.49112451076508
time_elpased: 1.79
batch start
#iterations: 379
currently lose_sum: 94.62908214330673
time_elpased: 1.762
start validation test
0.662835051546
0.651320179201
0.703272951832
0.676300291978
0.66276823965
60.831
batch start
#iterations: 380
currently lose_sum: 94.2798016667366
time_elpased: 1.716
batch start
#iterations: 381
currently lose_sum: 93.83265590667725
time_elpased: 1.789
batch start
#iterations: 382
currently lose_sum: 94.37671130895615
time_elpased: 1.777
batch start
#iterations: 383
currently lose_sum: 94.58469951152802
time_elpased: 1.782
batch start
#iterations: 384
currently lose_sum: 93.91977006196976
time_elpased: 1.739
batch start
#iterations: 385
currently lose_sum: 94.30745685100555
time_elpased: 1.736
batch start
#iterations: 386
currently lose_sum: 94.49967527389526
time_elpased: 1.781
batch start
#iterations: 387
currently lose_sum: 94.24340552091599
time_elpased: 1.795
batch start
#iterations: 388
currently lose_sum: 94.35816949605942
time_elpased: 1.729
batch start
#iterations: 389
currently lose_sum: 93.8576187491417
time_elpased: 1.785
batch start
#iterations: 390
currently lose_sum: 94.19764292240143
time_elpased: 1.794
batch start
#iterations: 391
currently lose_sum: 93.85813075304031
time_elpased: 1.768
batch start
#iterations: 392
currently lose_sum: 94.21658498048782
time_elpased: 1.764
batch start
#iterations: 393
currently lose_sum: 94.12968069314957
time_elpased: 1.798
batch start
#iterations: 394
currently lose_sum: 94.32503491640091
time_elpased: 1.811
batch start
#iterations: 395
currently lose_sum: 94.35686838626862
time_elpased: 1.777
batch start
#iterations: 396
currently lose_sum: 94.44297868013382
time_elpased: 1.79
batch start
#iterations: 397
currently lose_sum: 94.52514433860779
time_elpased: 1.766
batch start
#iterations: 398
currently lose_sum: 94.18068760633469
time_elpased: 1.756
batch start
#iterations: 399
currently lose_sum: 94.64879602193832
time_elpased: 1.765
start validation test
0.655824742268
0.653655576904
0.665294359819
0.659423616424
0.655809096473
61.308
acc: 0.675
pre: 0.639
rec: 0.810
F1: 0.714
auc: 0.718
