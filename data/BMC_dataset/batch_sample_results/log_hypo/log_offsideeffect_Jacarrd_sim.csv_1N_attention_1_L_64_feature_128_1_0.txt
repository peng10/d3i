start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.3204556107521
time_elpased: 1.872
batch start
#iterations: 1
currently lose_sum: 99.84454262256622
time_elpased: 1.877
batch start
#iterations: 2
currently lose_sum: 99.46927553415298
time_elpased: 1.842
batch start
#iterations: 3
currently lose_sum: 99.04606968164444
time_elpased: 1.894
batch start
#iterations: 4
currently lose_sum: 98.99435412883759
time_elpased: 1.844
batch start
#iterations: 5
currently lose_sum: 98.5944516658783
time_elpased: 1.814
batch start
#iterations: 6
currently lose_sum: 98.65303522348404
time_elpased: 1.874
batch start
#iterations: 7
currently lose_sum: 98.38647097349167
time_elpased: 1.856
batch start
#iterations: 8
currently lose_sum: 97.95765775442123
time_elpased: 1.905
batch start
#iterations: 9
currently lose_sum: 98.14708709716797
time_elpased: 1.857
batch start
#iterations: 10
currently lose_sum: 98.01608353853226
time_elpased: 1.819
batch start
#iterations: 11
currently lose_sum: 97.91323006153107
time_elpased: 1.912
batch start
#iterations: 12
currently lose_sum: 97.86855268478394
time_elpased: 1.912
batch start
#iterations: 13
currently lose_sum: 97.4473152756691
time_elpased: 1.903
batch start
#iterations: 14
currently lose_sum: 97.86745965480804
time_elpased: 1.879
batch start
#iterations: 15
currently lose_sum: 97.28851705789566
time_elpased: 1.89
batch start
#iterations: 16
currently lose_sum: 97.402558863163
time_elpased: 1.911
batch start
#iterations: 17
currently lose_sum: 97.39295655488968
time_elpased: 1.878
batch start
#iterations: 18
currently lose_sum: 97.03344529867172
time_elpased: 1.883
batch start
#iterations: 19
currently lose_sum: 97.43634271621704
time_elpased: 1.899
start validation test
0.618092783505
0.662352279122
0.48451168056
0.559643387816
0.618327305735
63.228
batch start
#iterations: 20
currently lose_sum: 97.35925936698914
time_elpased: 1.883
batch start
#iterations: 21
currently lose_sum: 96.5110713839531
time_elpased: 1.87
batch start
#iterations: 22
currently lose_sum: 96.9292181134224
time_elpased: 1.853
batch start
#iterations: 23
currently lose_sum: 96.81623357534409
time_elpased: 1.917
batch start
#iterations: 24
currently lose_sum: 96.66471934318542
time_elpased: 1.872
batch start
#iterations: 25
currently lose_sum: 96.49006676673889
time_elpased: 1.902
batch start
#iterations: 26
currently lose_sum: 96.85089933872223
time_elpased: 1.938
batch start
#iterations: 27
currently lose_sum: 96.49878352880478
time_elpased: 1.926
batch start
#iterations: 28
currently lose_sum: 96.52906584739685
time_elpased: 1.865
batch start
#iterations: 29
currently lose_sum: 96.55975496768951
time_elpased: 1.904
batch start
#iterations: 30
currently lose_sum: 96.59100878238678
time_elpased: 1.879
batch start
#iterations: 31
currently lose_sum: 96.44564545154572
time_elpased: 1.86
batch start
#iterations: 32
currently lose_sum: 96.17149996757507
time_elpased: 1.877
batch start
#iterations: 33
currently lose_sum: 96.22597342729568
time_elpased: 1.838
batch start
#iterations: 34
currently lose_sum: 96.47326177358627
time_elpased: 1.856
batch start
#iterations: 35
currently lose_sum: 96.51203340291977
time_elpased: 1.877
batch start
#iterations: 36
currently lose_sum: 96.2972599864006
time_elpased: 1.926
batch start
#iterations: 37
currently lose_sum: 96.19256108999252
time_elpased: 1.871
batch start
#iterations: 38
currently lose_sum: 96.18757635354996
time_elpased: 1.883
batch start
#iterations: 39
currently lose_sum: 96.322967171669
time_elpased: 1.872
start validation test
0.660309278351
0.641480409013
0.729546156221
0.682684899846
0.660187722332
61.395
batch start
#iterations: 40
currently lose_sum: 96.68552601337433
time_elpased: 1.865
batch start
#iterations: 41
currently lose_sum: 96.42282527685165
time_elpased: 1.908
batch start
#iterations: 42
currently lose_sum: 96.11806970834732
time_elpased: 1.863
batch start
#iterations: 43
currently lose_sum: 95.63512349128723
time_elpased: 1.901
batch start
#iterations: 44
currently lose_sum: 96.02406048774719
time_elpased: 1.849
batch start
#iterations: 45
currently lose_sum: 95.9099212884903
time_elpased: 1.859
batch start
#iterations: 46
currently lose_sum: 96.11073017120361
time_elpased: 1.841
batch start
#iterations: 47
currently lose_sum: 96.02672010660172
time_elpased: 1.877
batch start
#iterations: 48
currently lose_sum: 96.00681364536285
time_elpased: 1.877
batch start
#iterations: 49
currently lose_sum: 96.26267045736313
time_elpased: 1.905
batch start
#iterations: 50
currently lose_sum: 95.90988320112228
time_elpased: 1.912
batch start
#iterations: 51
currently lose_sum: 96.01420038938522
time_elpased: 1.875
batch start
#iterations: 52
currently lose_sum: 95.97639685869217
time_elpased: 1.885
batch start
#iterations: 53
currently lose_sum: 95.91470754146576
time_elpased: 1.845
batch start
#iterations: 54
currently lose_sum: 96.13314723968506
time_elpased: 1.849
batch start
#iterations: 55
currently lose_sum: 95.84031850099564
time_elpased: 1.871
batch start
#iterations: 56
currently lose_sum: 95.78621029853821
time_elpased: 1.885
batch start
#iterations: 57
currently lose_sum: 95.7894914150238
time_elpased: 1.854
batch start
#iterations: 58
currently lose_sum: 96.2544395327568
time_elpased: 1.867
batch start
#iterations: 59
currently lose_sum: 95.97979706525803
time_elpased: 1.888
start validation test
0.651958762887
0.649340183338
0.663373469178
0.656281816331
0.651938722609
61.728
batch start
#iterations: 60
currently lose_sum: 96.02745771408081
time_elpased: 1.86
batch start
#iterations: 61
currently lose_sum: 96.20291417837143
time_elpased: 1.863
batch start
#iterations: 62
currently lose_sum: 95.89412540197372
time_elpased: 1.856
batch start
#iterations: 63
currently lose_sum: 95.93904459476471
time_elpased: 1.85
batch start
#iterations: 64
currently lose_sum: 96.00374960899353
time_elpased: 1.895
batch start
#iterations: 65
currently lose_sum: 95.31478375196457
time_elpased: 1.838
batch start
#iterations: 66
currently lose_sum: 96.08365219831467
time_elpased: 1.857
batch start
#iterations: 67
currently lose_sum: 96.0184018611908
time_elpased: 1.849
batch start
#iterations: 68
currently lose_sum: 95.34969437122345
time_elpased: 1.856
batch start
#iterations: 69
currently lose_sum: 95.45692420005798
time_elpased: 1.867
batch start
#iterations: 70
currently lose_sum: 95.40532743930817
time_elpased: 1.849
batch start
#iterations: 71
currently lose_sum: 95.51227736473083
time_elpased: 1.912
batch start
#iterations: 72
currently lose_sum: 95.31724989414215
time_elpased: 1.912
batch start
#iterations: 73
currently lose_sum: 95.25184887647629
time_elpased: 1.867
batch start
#iterations: 74
currently lose_sum: 95.26048737764359
time_elpased: 1.911
batch start
#iterations: 75
currently lose_sum: 95.36047399044037
time_elpased: 1.895
batch start
#iterations: 76
currently lose_sum: 95.7466681599617
time_elpased: 1.88
batch start
#iterations: 77
currently lose_sum: 95.44888758659363
time_elpased: 1.899
batch start
#iterations: 78
currently lose_sum: 95.58551239967346
time_elpased: 1.959
batch start
#iterations: 79
currently lose_sum: 95.26809906959534
time_elpased: 1.862
start validation test
0.633608247423
0.654983961031
0.567356179891
0.608029116577
0.633724563146
62.136
batch start
#iterations: 80
currently lose_sum: 95.62292367219925
time_elpased: 1.855
batch start
#iterations: 81
currently lose_sum: 95.7410877943039
time_elpased: 1.87
batch start
#iterations: 82
currently lose_sum: 95.58955025672913
time_elpased: 1.868
batch start
#iterations: 83
currently lose_sum: 95.61358612775803
time_elpased: 1.867
batch start
#iterations: 84
currently lose_sum: 95.55623894929886
time_elpased: 1.879
batch start
#iterations: 85
currently lose_sum: 95.6914929151535
time_elpased: 1.853
batch start
#iterations: 86
currently lose_sum: 95.74740755558014
time_elpased: 1.878
batch start
#iterations: 87
currently lose_sum: 95.36835843324661
time_elpased: 1.86
batch start
#iterations: 88
currently lose_sum: 95.48413640260696
time_elpased: 1.874
batch start
#iterations: 89
currently lose_sum: 95.52751606702805
time_elpased: 1.849
batch start
#iterations: 90
currently lose_sum: 95.06305658817291
time_elpased: 1.855
batch start
#iterations: 91
currently lose_sum: 95.5846775174141
time_elpased: 1.869
batch start
#iterations: 92
currently lose_sum: 95.38006836175919
time_elpased: 1.884
batch start
#iterations: 93
currently lose_sum: 95.34101587533951
time_elpased: 1.892
batch start
#iterations: 94
currently lose_sum: 95.14956003427505
time_elpased: 1.847
batch start
#iterations: 95
currently lose_sum: 95.44532054662704
time_elpased: 1.798
batch start
#iterations: 96
currently lose_sum: 95.55825370550156
time_elpased: 1.852
batch start
#iterations: 97
currently lose_sum: 95.36547428369522
time_elpased: 1.853
batch start
#iterations: 98
currently lose_sum: 95.85768228769302
time_elpased: 1.899
batch start
#iterations: 99
currently lose_sum: 95.63954001665115
time_elpased: 1.883
start validation test
0.665154639175
0.643295666874
0.744056807657
0.690017178851
0.665016114249
61.009
batch start
#iterations: 100
currently lose_sum: 95.35025590658188
time_elpased: 1.873
batch start
#iterations: 101
currently lose_sum: 95.11149060726166
time_elpased: 1.857
batch start
#iterations: 102
currently lose_sum: 95.25065475702286
time_elpased: 1.828
batch start
#iterations: 103
currently lose_sum: 95.1792864203453
time_elpased: 1.823
batch start
#iterations: 104
currently lose_sum: 94.75730192661285
time_elpased: 1.912
batch start
#iterations: 105
currently lose_sum: 95.4053190946579
time_elpased: 1.87
batch start
#iterations: 106
currently lose_sum: 95.32201647758484
time_elpased: 1.877
batch start
#iterations: 107
currently lose_sum: 95.13868713378906
time_elpased: 1.845
batch start
#iterations: 108
currently lose_sum: 95.28926879167557
time_elpased: 1.86
batch start
#iterations: 109
currently lose_sum: 95.12013399600983
time_elpased: 1.876
batch start
#iterations: 110
currently lose_sum: 95.06591993570328
time_elpased: 1.89
batch start
#iterations: 111
currently lose_sum: 95.24950700998306
time_elpased: 1.839
batch start
#iterations: 112
currently lose_sum: 95.54427021741867
time_elpased: 1.833
batch start
#iterations: 113
currently lose_sum: 95.07360589504242
time_elpased: 1.829
batch start
#iterations: 114
currently lose_sum: 95.4357602596283
time_elpased: 1.831
batch start
#iterations: 115
currently lose_sum: 95.40223830938339
time_elpased: 1.867
batch start
#iterations: 116
currently lose_sum: 94.92754757404327
time_elpased: 1.824
batch start
#iterations: 117
currently lose_sum: 95.22757560014725
time_elpased: 1.821
batch start
#iterations: 118
currently lose_sum: 95.1570338010788
time_elpased: 1.818
batch start
#iterations: 119
currently lose_sum: 95.4064701795578
time_elpased: 1.826
start validation test
0.666649484536
0.630606011895
0.807450859319
0.708154700122
0.666402286006
60.655
batch start
#iterations: 120
currently lose_sum: 95.47648841142654
time_elpased: 1.892
batch start
#iterations: 121
currently lose_sum: 95.1957221031189
time_elpased: 1.802
batch start
#iterations: 122
currently lose_sum: 95.45159459114075
time_elpased: 1.876
batch start
#iterations: 123
currently lose_sum: 94.81816506385803
time_elpased: 1.829
batch start
#iterations: 124
currently lose_sum: 95.74738156795502
time_elpased: 1.842
batch start
#iterations: 125
currently lose_sum: 95.01262438297272
time_elpased: 1.838
batch start
#iterations: 126
currently lose_sum: 95.30113184452057
time_elpased: 1.868
batch start
#iterations: 127
currently lose_sum: 95.16783899068832
time_elpased: 1.86
batch start
#iterations: 128
currently lose_sum: 95.24507158994675
time_elpased: 1.816
batch start
#iterations: 129
currently lose_sum: 95.08919256925583
time_elpased: 1.808
batch start
#iterations: 130
currently lose_sum: 95.02527332305908
time_elpased: 1.851
batch start
#iterations: 131
currently lose_sum: 95.144950568676
time_elpased: 1.822
batch start
#iterations: 132
currently lose_sum: 95.11708158254623
time_elpased: 1.875
batch start
#iterations: 133
currently lose_sum: 95.5395113825798
time_elpased: 1.824
batch start
#iterations: 134
currently lose_sum: 95.01406693458557
time_elpased: 1.842
batch start
#iterations: 135
currently lose_sum: 95.03371578454971
time_elpased: 1.818
batch start
#iterations: 136
currently lose_sum: 95.42797422409058
time_elpased: 1.824
batch start
#iterations: 137
currently lose_sum: 94.91688197851181
time_elpased: 1.835
batch start
#iterations: 138
currently lose_sum: 95.33463180065155
time_elpased: 1.848
batch start
#iterations: 139
currently lose_sum: 95.38098925352097
time_elpased: 1.857
start validation test
0.66
0.64187653423
0.726561695997
0.68159876424
0.659883140676
61.229
batch start
#iterations: 140
currently lose_sum: 95.32883822917938
time_elpased: 1.823
batch start
#iterations: 141
currently lose_sum: 95.03191071748734
time_elpased: 1.831
batch start
#iterations: 142
currently lose_sum: 95.14534378051758
time_elpased: 1.821
batch start
#iterations: 143
currently lose_sum: 95.12909889221191
time_elpased: 1.809
batch start
#iterations: 144
currently lose_sum: 94.81179928779602
time_elpased: 1.807
batch start
#iterations: 145
currently lose_sum: 95.1083772778511
time_elpased: 1.827
batch start
#iterations: 146
currently lose_sum: 94.887619972229
time_elpased: 1.822
batch start
#iterations: 147
currently lose_sum: 95.44503253698349
time_elpased: 1.829
batch start
#iterations: 148
currently lose_sum: 94.86821764707565
time_elpased: 1.871
batch start
#iterations: 149
currently lose_sum: 95.30395042896271
time_elpased: 1.854
batch start
#iterations: 150
currently lose_sum: 95.11718195676804
time_elpased: 1.794
batch start
#iterations: 151
currently lose_sum: 95.1488224864006
time_elpased: 1.8
batch start
#iterations: 152
currently lose_sum: 95.23814231157303
time_elpased: 1.83
batch start
#iterations: 153
currently lose_sum: 95.08054941892624
time_elpased: 1.804
batch start
#iterations: 154
currently lose_sum: 95.36171239614487
time_elpased: 1.893
batch start
#iterations: 155
currently lose_sum: 95.11598229408264
time_elpased: 1.798
batch start
#iterations: 156
currently lose_sum: 95.03752064704895
time_elpased: 1.833
batch start
#iterations: 157
currently lose_sum: 95.00955039262772
time_elpased: 1.822
batch start
#iterations: 158
currently lose_sum: 95.02702295780182
time_elpased: 1.846
batch start
#iterations: 159
currently lose_sum: 94.93255537748337
time_elpased: 1.874
start validation test
0.668711340206
0.644146512443
0.756509210662
0.69582090965
0.668557197503
60.675
batch start
#iterations: 160
currently lose_sum: 94.901322722435
time_elpased: 1.851
batch start
#iterations: 161
currently lose_sum: 95.14828342199326
time_elpased: 1.818
batch start
#iterations: 162
currently lose_sum: 94.95862454175949
time_elpased: 1.847
batch start
#iterations: 163
currently lose_sum: 94.75029557943344
time_elpased: 1.831
batch start
#iterations: 164
currently lose_sum: 95.03332775831223
time_elpased: 1.855
batch start
#iterations: 165
currently lose_sum: 95.00363713502884
time_elpased: 1.843
batch start
#iterations: 166
currently lose_sum: 94.93974882364273
time_elpased: 1.87
batch start
#iterations: 167
currently lose_sum: 94.75977343320847
time_elpased: 1.847
batch start
#iterations: 168
currently lose_sum: 94.74738627672195
time_elpased: 1.822
batch start
#iterations: 169
currently lose_sum: 94.78757745027542
time_elpased: 1.803
batch start
#iterations: 170
currently lose_sum: 95.09468340873718
time_elpased: 1.842
batch start
#iterations: 171
currently lose_sum: 94.8780187368393
time_elpased: 1.846
batch start
#iterations: 172
currently lose_sum: 94.7370463013649
time_elpased: 1.873
batch start
#iterations: 173
currently lose_sum: 94.72511303424835
time_elpased: 1.836
batch start
#iterations: 174
currently lose_sum: 95.00471127033234
time_elpased: 1.842
batch start
#iterations: 175
currently lose_sum: 95.09467798471451
time_elpased: 1.849
batch start
#iterations: 176
currently lose_sum: 94.93337911367416
time_elpased: 1.798
batch start
#iterations: 177
currently lose_sum: 94.79047006368637
time_elpased: 1.859
batch start
#iterations: 178
currently lose_sum: 94.86022138595581
time_elpased: 1.863
batch start
#iterations: 179
currently lose_sum: 94.68729728460312
time_elpased: 1.848
start validation test
0.661804123711
0.646165246388
0.717917052588
0.680154048652
0.66170560881
60.579
batch start
#iterations: 180
currently lose_sum: 95.07244825363159
time_elpased: 1.872
batch start
#iterations: 181
currently lose_sum: 94.31648987531662
time_elpased: 1.812
batch start
#iterations: 182
currently lose_sum: 95.08133429288864
time_elpased: 1.811
batch start
#iterations: 183
currently lose_sum: 94.69909918308258
time_elpased: 1.825
batch start
#iterations: 184
currently lose_sum: 94.63393211364746
time_elpased: 1.803
batch start
#iterations: 185
currently lose_sum: 94.4432253241539
time_elpased: 1.873
batch start
#iterations: 186
currently lose_sum: 94.40265601873398
time_elpased: 1.845
batch start
#iterations: 187
currently lose_sum: 94.49879264831543
time_elpased: 1.924
batch start
#iterations: 188
currently lose_sum: 94.64828830957413
time_elpased: 1.839
batch start
#iterations: 189
currently lose_sum: 94.86186188459396
time_elpased: 1.815
batch start
#iterations: 190
currently lose_sum: 94.75822329521179
time_elpased: 1.853
batch start
#iterations: 191
currently lose_sum: 94.82293170690536
time_elpased: 1.833
batch start
#iterations: 192
currently lose_sum: 94.25735276937485
time_elpased: 1.833
batch start
#iterations: 193
currently lose_sum: 94.6805117726326
time_elpased: 1.822
batch start
#iterations: 194
currently lose_sum: 94.36926746368408
time_elpased: 1.916
batch start
#iterations: 195
currently lose_sum: 94.69985496997833
time_elpased: 1.858
batch start
#iterations: 196
currently lose_sum: 94.92420989274979
time_elpased: 1.857
batch start
#iterations: 197
currently lose_sum: 95.42200821638107
time_elpased: 1.832
batch start
#iterations: 198
currently lose_sum: 94.63277089595795
time_elpased: 1.876
batch start
#iterations: 199
currently lose_sum: 94.7901519536972
time_elpased: 1.849
start validation test
0.647371134021
0.650733752621
0.638880312854
0.644752557512
0.647386040967
61.084
batch start
#iterations: 200
currently lose_sum: 94.72863584756851
time_elpased: 1.861
batch start
#iterations: 201
currently lose_sum: 94.91591030359268
time_elpased: 1.836
batch start
#iterations: 202
currently lose_sum: 94.47050589323044
time_elpased: 1.85
batch start
#iterations: 203
currently lose_sum: 94.95102047920227
time_elpased: 1.839
batch start
#iterations: 204
currently lose_sum: 94.63286846876144
time_elpased: 1.824
batch start
#iterations: 205
currently lose_sum: 94.74744147062302
time_elpased: 1.826
batch start
#iterations: 206
currently lose_sum: 94.76684218645096
time_elpased: 1.879
batch start
#iterations: 207
currently lose_sum: 94.69004988670349
time_elpased: 1.852
batch start
#iterations: 208
currently lose_sum: 94.27298492193222
time_elpased: 1.871
batch start
#iterations: 209
currently lose_sum: 94.63417279720306
time_elpased: 1.859
batch start
#iterations: 210
currently lose_sum: 94.2345085144043
time_elpased: 1.838
batch start
#iterations: 211
currently lose_sum: 94.43095880746841
time_elpased: 1.85
batch start
#iterations: 212
currently lose_sum: 94.20653820037842
time_elpased: 1.866
batch start
#iterations: 213
currently lose_sum: 94.59140467643738
time_elpased: 1.858
batch start
#iterations: 214
currently lose_sum: 94.18401086330414
time_elpased: 1.834
batch start
#iterations: 215
currently lose_sum: 94.51782566308975
time_elpased: 1.833
batch start
#iterations: 216
currently lose_sum: 94.96351927518845
time_elpased: 1.818
batch start
#iterations: 217
currently lose_sum: 94.54042786359787
time_elpased: 1.887
batch start
#iterations: 218
currently lose_sum: 94.98636108636856
time_elpased: 1.839
batch start
#iterations: 219
currently lose_sum: 94.85025626420975
time_elpased: 1.87
start validation test
0.659690721649
0.618884054652
0.834413913759
0.710667017267
0.659383968137
60.597
batch start
#iterations: 220
currently lose_sum: 95.28386932611465
time_elpased: 1.846
batch start
#iterations: 221
currently lose_sum: 94.79180210828781
time_elpased: 1.859
batch start
#iterations: 222
currently lose_sum: 94.84887272119522
time_elpased: 1.846
batch start
#iterations: 223
currently lose_sum: 94.5266124010086
time_elpased: 1.817
batch start
#iterations: 224
currently lose_sum: 94.63612353801727
time_elpased: 1.862
batch start
#iterations: 225
currently lose_sum: 94.5416516661644
time_elpased: 1.844
batch start
#iterations: 226
currently lose_sum: 94.50725054740906
time_elpased: 1.818
batch start
#iterations: 227
currently lose_sum: 94.93650698661804
time_elpased: 1.802
batch start
#iterations: 228
currently lose_sum: 94.68176609277725
time_elpased: 1.843
batch start
#iterations: 229
currently lose_sum: 94.44462591409683
time_elpased: 1.828
batch start
#iterations: 230
currently lose_sum: 94.20274204015732
time_elpased: 1.855
batch start
#iterations: 231
currently lose_sum: 94.70960420370102
time_elpased: 1.868
batch start
#iterations: 232
currently lose_sum: 94.73025888204575
time_elpased: 1.853
batch start
#iterations: 233
currently lose_sum: 94.58510488271713
time_elpased: 1.798
batch start
#iterations: 234
currently lose_sum: 94.82104825973511
time_elpased: 1.856
batch start
#iterations: 235
currently lose_sum: 94.50532668828964
time_elpased: 1.84
batch start
#iterations: 236
currently lose_sum: 94.69491112232208
time_elpased: 1.874
batch start
#iterations: 237
currently lose_sum: 94.1816828250885
time_elpased: 1.884
batch start
#iterations: 238
currently lose_sum: 94.75812309980392
time_elpased: 1.852
batch start
#iterations: 239
currently lose_sum: 94.46000444889069
time_elpased: 1.822
start validation test
0.625309278351
0.668085690744
0.50066893074
0.572386610977
0.625528103705
61.586
batch start
#iterations: 240
currently lose_sum: 94.46397966146469
time_elpased: 1.875
batch start
#iterations: 241
currently lose_sum: 94.65525078773499
time_elpased: 1.817
batch start
#iterations: 242
currently lose_sum: 94.5498451590538
time_elpased: 1.917
batch start
#iterations: 243
currently lose_sum: 94.61369889974594
time_elpased: 1.834
batch start
#iterations: 244
currently lose_sum: 94.90855926275253
time_elpased: 1.835
batch start
#iterations: 245
currently lose_sum: 94.74137073755264
time_elpased: 1.862
batch start
#iterations: 246
currently lose_sum: 94.77219545841217
time_elpased: 1.852
batch start
#iterations: 247
currently lose_sum: 94.78881269693375
time_elpased: 1.865
batch start
#iterations: 248
currently lose_sum: 95.08804875612259
time_elpased: 1.872
batch start
#iterations: 249
currently lose_sum: 94.31902951002121
time_elpased: 1.828
batch start
#iterations: 250
currently lose_sum: 94.6288458108902
time_elpased: 1.842
batch start
#iterations: 251
currently lose_sum: 94.26914697885513
time_elpased: 1.869
batch start
#iterations: 252
currently lose_sum: 94.06848984956741
time_elpased: 1.844
batch start
#iterations: 253
currently lose_sum: 94.45147979259491
time_elpased: 1.844
batch start
#iterations: 254
currently lose_sum: 94.59184116125107
time_elpased: 1.845
batch start
#iterations: 255
currently lose_sum: 93.8931056857109
time_elpased: 1.834
batch start
#iterations: 256
currently lose_sum: 94.45642602443695
time_elpased: 1.858
batch start
#iterations: 257
currently lose_sum: 94.25610494613647
time_elpased: 1.853
batch start
#iterations: 258
currently lose_sum: 94.47350341081619
time_elpased: 1.884
batch start
#iterations: 259
currently lose_sum: 94.63016158342361
time_elpased: 1.87
start validation test
0.649484536082
0.634263094909
0.709066584337
0.669582118562
0.649379930607
60.663
batch start
#iterations: 260
currently lose_sum: 94.25961494445801
time_elpased: 1.853
batch start
#iterations: 261
currently lose_sum: 94.50688230991364
time_elpased: 1.821
batch start
#iterations: 262
currently lose_sum: 94.31424832344055
time_elpased: 1.881
batch start
#iterations: 263
currently lose_sum: 94.56620645523071
time_elpased: 1.854
batch start
#iterations: 264
currently lose_sum: 94.53749346733093
time_elpased: 1.857
batch start
#iterations: 265
currently lose_sum: 94.56935459375381
time_elpased: 1.849
batch start
#iterations: 266
currently lose_sum: 94.68990767002106
time_elpased: 1.842
batch start
#iterations: 267
currently lose_sum: 94.42907387018204
time_elpased: 1.823
batch start
#iterations: 268
currently lose_sum: 94.50683122873306
time_elpased: 1.848
batch start
#iterations: 269
currently lose_sum: 94.44588512182236
time_elpased: 1.854
batch start
#iterations: 270
currently lose_sum: 94.68928915262222
time_elpased: 1.86
batch start
#iterations: 271
currently lose_sum: 94.1241723895073
time_elpased: 1.836
batch start
#iterations: 272
currently lose_sum: 94.28120774030685
time_elpased: 1.881
batch start
#iterations: 273
currently lose_sum: 94.50239050388336
time_elpased: 1.825
batch start
#iterations: 274
currently lose_sum: 94.65260195732117
time_elpased: 1.829
batch start
#iterations: 275
currently lose_sum: 94.91393965482712
time_elpased: 1.894
batch start
#iterations: 276
currently lose_sum: 94.4735655784607
time_elpased: 1.864
batch start
#iterations: 277
currently lose_sum: 94.76595211029053
time_elpased: 1.913
batch start
#iterations: 278
currently lose_sum: 94.58405834436417
time_elpased: 1.855
batch start
#iterations: 279
currently lose_sum: 94.7052047252655
time_elpased: 1.849
start validation test
0.667886597938
0.632916531341
0.802202325821
0.707575001135
0.667650785961
60.495
batch start
#iterations: 280
currently lose_sum: 94.29314863681793
time_elpased: 1.815
batch start
#iterations: 281
currently lose_sum: 94.35534137487411
time_elpased: 1.827
batch start
#iterations: 282
currently lose_sum: 94.48519432544708
time_elpased: 1.867
batch start
#iterations: 283
currently lose_sum: 94.86481660604477
time_elpased: 1.887
batch start
#iterations: 284
currently lose_sum: 94.11727666854858
time_elpased: 1.879
batch start
#iterations: 285
currently lose_sum: 94.56669390201569
time_elpased: 1.861
batch start
#iterations: 286
currently lose_sum: 94.37427574396133
time_elpased: 1.885
batch start
#iterations: 287
currently lose_sum: 94.5641342997551
time_elpased: 1.883
batch start
#iterations: 288
currently lose_sum: 94.38218760490417
time_elpased: 1.852
batch start
#iterations: 289
currently lose_sum: 94.52941769361496
time_elpased: 1.858
batch start
#iterations: 290
currently lose_sum: 94.39006584882736
time_elpased: 1.836
batch start
#iterations: 291
currently lose_sum: 94.09456807374954
time_elpased: 1.84
batch start
#iterations: 292
currently lose_sum: 94.13616758584976
time_elpased: 1.834
batch start
#iterations: 293
currently lose_sum: 94.03377705812454
time_elpased: 1.825
batch start
#iterations: 294
currently lose_sum: 94.59247368574142
time_elpased: 1.872
batch start
#iterations: 295
currently lose_sum: 94.39912837743759
time_elpased: 1.845
batch start
#iterations: 296
currently lose_sum: 94.31280565261841
time_elpased: 1.814
batch start
#iterations: 297
currently lose_sum: 94.26419365406036
time_elpased: 1.87
batch start
#iterations: 298
currently lose_sum: 94.68022924661636
time_elpased: 1.85
batch start
#iterations: 299
currently lose_sum: 94.21714013814926
time_elpased: 1.858
start validation test
0.66618556701
0.634581845362
0.786353812905
0.702362349481
0.66597459312
60.333
batch start
#iterations: 300
currently lose_sum: 94.57106161117554
time_elpased: 1.829
batch start
#iterations: 301
currently lose_sum: 94.18313503265381
time_elpased: 1.834
batch start
#iterations: 302
currently lose_sum: 94.33534324169159
time_elpased: 1.84
batch start
#iterations: 303
currently lose_sum: 94.48406499624252
time_elpased: 1.832
batch start
#iterations: 304
currently lose_sum: 94.14430046081543
time_elpased: 1.83
batch start
#iterations: 305
currently lose_sum: 94.25747680664062
time_elpased: 1.878
batch start
#iterations: 306
currently lose_sum: 94.3715870976448
time_elpased: 1.888
batch start
#iterations: 307
currently lose_sum: 94.22932010889053
time_elpased: 1.848
batch start
#iterations: 308
currently lose_sum: 94.31004822254181
time_elpased: 1.857
batch start
#iterations: 309
currently lose_sum: 94.77916157245636
time_elpased: 1.839
batch start
#iterations: 310
currently lose_sum: 94.1382178068161
time_elpased: 1.888
batch start
#iterations: 311
currently lose_sum: 94.24454337358475
time_elpased: 1.839
batch start
#iterations: 312
currently lose_sum: 94.44184428453445
time_elpased: 1.843
batch start
#iterations: 313
currently lose_sum: 94.43633633852005
time_elpased: 1.823
batch start
#iterations: 314
currently lose_sum: 94.63691645860672
time_elpased: 1.869
batch start
#iterations: 315
currently lose_sum: 94.46330666542053
time_elpased: 1.912
batch start
#iterations: 316
currently lose_sum: 94.19897198677063
time_elpased: 1.866
batch start
#iterations: 317
currently lose_sum: 94.13232159614563
time_elpased: 1.887
batch start
#iterations: 318
currently lose_sum: 94.40853500366211
time_elpased: 1.84
batch start
#iterations: 319
currently lose_sum: 94.4892715215683
time_elpased: 1.91
start validation test
0.662989690722
0.639908458762
0.748173304518
0.689818768384
0.662840137749
60.339
batch start
#iterations: 320
currently lose_sum: 94.31528639793396
time_elpased: 1.827
batch start
#iterations: 321
currently lose_sum: 94.27187460660934
time_elpased: 1.872
batch start
#iterations: 322
currently lose_sum: 94.82716256380081
time_elpased: 1.881
batch start
#iterations: 323
currently lose_sum: 94.36388206481934
time_elpased: 1.904
batch start
#iterations: 324
currently lose_sum: 94.34614300727844
time_elpased: 1.816
batch start
#iterations: 325
currently lose_sum: 94.25411891937256
time_elpased: 1.845
batch start
#iterations: 326
currently lose_sum: 93.93041628599167
time_elpased: 1.878
batch start
#iterations: 327
currently lose_sum: 94.81074047088623
time_elpased: 1.849
batch start
#iterations: 328
currently lose_sum: 94.19520258903503
time_elpased: 1.859
batch start
#iterations: 329
currently lose_sum: 94.59726023674011
time_elpased: 1.852
batch start
#iterations: 330
currently lose_sum: 94.21803951263428
time_elpased: 1.868
batch start
#iterations: 331
currently lose_sum: 94.44572025537491
time_elpased: 1.859
batch start
#iterations: 332
currently lose_sum: 94.09278780221939
time_elpased: 1.853
batch start
#iterations: 333
currently lose_sum: 94.38757467269897
time_elpased: 1.845
batch start
#iterations: 334
currently lose_sum: 94.53243392705917
time_elpased: 1.88
batch start
#iterations: 335
currently lose_sum: 94.44467329978943
time_elpased: 1.885
batch start
#iterations: 336
currently lose_sum: 94.09561264514923
time_elpased: 1.84
batch start
#iterations: 337
currently lose_sum: 94.25535947084427
time_elpased: 1.889
batch start
#iterations: 338
currently lose_sum: 94.07147896289825
time_elpased: 1.931
batch start
#iterations: 339
currently lose_sum: 94.12277781963348
time_elpased: 1.81
start validation test
0.666701030928
0.643633471768
0.749614078419
0.692592944756
0.666555464285
60.299
batch start
#iterations: 340
currently lose_sum: 94.3522509932518
time_elpased: 1.837
batch start
#iterations: 341
currently lose_sum: 94.20316749811172
time_elpased: 1.86
batch start
#iterations: 342
currently lose_sum: 94.35228127241135
time_elpased: 1.827
batch start
#iterations: 343
currently lose_sum: 94.60664027929306
time_elpased: 1.838
batch start
#iterations: 344
currently lose_sum: 94.28800451755524
time_elpased: 1.842
batch start
#iterations: 345
currently lose_sum: 94.71598708629608
time_elpased: 1.841
batch start
#iterations: 346
currently lose_sum: 94.16755020618439
time_elpased: 1.872
batch start
#iterations: 347
currently lose_sum: 94.3928114771843
time_elpased: 1.857
batch start
#iterations: 348
currently lose_sum: 94.13471275568008
time_elpased: 1.814
batch start
#iterations: 349
currently lose_sum: 94.42018473148346
time_elpased: 1.852
batch start
#iterations: 350
currently lose_sum: 94.43177723884583
time_elpased: 1.853
batch start
#iterations: 351
currently lose_sum: 94.35322350263596
time_elpased: 1.851
batch start
#iterations: 352
currently lose_sum: 94.29703539609909
time_elpased: 1.889
batch start
#iterations: 353
currently lose_sum: 94.09443229436874
time_elpased: 1.844
batch start
#iterations: 354
currently lose_sum: 94.49111920595169
time_elpased: 1.887
batch start
#iterations: 355
currently lose_sum: 94.3536736369133
time_elpased: 1.848
batch start
#iterations: 356
currently lose_sum: 94.59688687324524
time_elpased: 1.85
batch start
#iterations: 357
currently lose_sum: 94.34020465612411
time_elpased: 1.817
batch start
#iterations: 358
currently lose_sum: 94.52626037597656
time_elpased: 1.847
batch start
#iterations: 359
currently lose_sum: 94.38250261545181
time_elpased: 1.817
start validation test
0.661030927835
0.650734235531
0.697746217969
0.673420738975
0.66096646848
61.072
batch start
#iterations: 360
currently lose_sum: 94.43205046653748
time_elpased: 1.874
batch start
#iterations: 361
currently lose_sum: 94.09059190750122
time_elpased: 1.878
batch start
#iterations: 362
currently lose_sum: 94.3559200167656
time_elpased: 1.879
batch start
#iterations: 363
currently lose_sum: 94.23820668458939
time_elpased: 1.874
batch start
#iterations: 364
currently lose_sum: 94.1197983622551
time_elpased: 1.878
batch start
#iterations: 365
currently lose_sum: 94.19616103172302
time_elpased: 1.836
batch start
#iterations: 366
currently lose_sum: 94.29748040437698
time_elpased: 1.847
batch start
#iterations: 367
currently lose_sum: 94.29086232185364
time_elpased: 1.83
batch start
#iterations: 368
currently lose_sum: 94.22373956441879
time_elpased: 1.847
batch start
#iterations: 369
currently lose_sum: 94.60836976766586
time_elpased: 1.822
batch start
#iterations: 370
currently lose_sum: 94.01351290941238
time_elpased: 1.867
batch start
#iterations: 371
currently lose_sum: 94.28289878368378
time_elpased: 1.885
batch start
#iterations: 372
currently lose_sum: 93.97677916288376
time_elpased: 1.801
batch start
#iterations: 373
currently lose_sum: 94.11292678117752
time_elpased: 1.828
batch start
#iterations: 374
currently lose_sum: 94.27681112289429
time_elpased: 1.933
batch start
#iterations: 375
currently lose_sum: 94.41280674934387
time_elpased: 1.817
batch start
#iterations: 376
currently lose_sum: 94.04840421676636
time_elpased: 1.903
batch start
#iterations: 377
currently lose_sum: 94.37192159891129
time_elpased: 1.837
batch start
#iterations: 378
currently lose_sum: 94.23696559667587
time_elpased: 1.897
batch start
#iterations: 379
currently lose_sum: 93.96609246730804
time_elpased: 1.849
start validation test
0.667422680412
0.637334903676
0.779664505506
0.701351601555
0.667225622576
60.296
batch start
#iterations: 380
currently lose_sum: 94.29642271995544
time_elpased: 1.819
batch start
#iterations: 381
currently lose_sum: 94.12754911184311
time_elpased: 1.829
batch start
#iterations: 382
currently lose_sum: 94.35819119215012
time_elpased: 1.866
batch start
#iterations: 383
currently lose_sum: 94.34149944782257
time_elpased: 1.864
batch start
#iterations: 384
currently lose_sum: 95.04408925771713
time_elpased: 1.837
batch start
#iterations: 385
currently lose_sum: 93.76992303133011
time_elpased: 1.85
batch start
#iterations: 386
currently lose_sum: 94.1309312582016
time_elpased: 1.881
batch start
#iterations: 387
currently lose_sum: 94.40187555551529
time_elpased: 1.882
batch start
#iterations: 388
currently lose_sum: 94.15817958116531
time_elpased: 1.823
batch start
#iterations: 389
currently lose_sum: 94.5625547170639
time_elpased: 1.869
batch start
#iterations: 390
currently lose_sum: 94.33965390920639
time_elpased: 1.837
batch start
#iterations: 391
currently lose_sum: 94.23510587215424
time_elpased: 1.855
batch start
#iterations: 392
currently lose_sum: 93.94030398130417
time_elpased: 1.854
batch start
#iterations: 393
currently lose_sum: 94.11099767684937
time_elpased: 1.854
batch start
#iterations: 394
currently lose_sum: 93.64981192350388
time_elpased: 1.886
batch start
#iterations: 395
currently lose_sum: 93.84736847877502
time_elpased: 1.846
batch start
#iterations: 396
currently lose_sum: 94.72635543346405
time_elpased: 1.829
batch start
#iterations: 397
currently lose_sum: 94.47183710336685
time_elpased: 1.87
batch start
#iterations: 398
currently lose_sum: 94.08143049478531
time_elpased: 1.832
batch start
#iterations: 399
currently lose_sum: 94.45087045431137
time_elpased: 1.845
start validation test
0.666494845361
0.640210726315
0.762889780797
0.696187077385
0.666325609184
60.380
acc: 0.669
pre: 0.639
rec: 0.779
F1: 0.702
auc: 0.704
