start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.32270568609238
time_elpased: 1.626
batch start
#iterations: 1
currently lose_sum: 99.77184969186783
time_elpased: 1.602
batch start
#iterations: 2
currently lose_sum: 99.79812550544739
time_elpased: 1.618
batch start
#iterations: 3
currently lose_sum: 99.4761683344841
time_elpased: 1.631
batch start
#iterations: 4
currently lose_sum: 99.36151880025864
time_elpased: 1.642
batch start
#iterations: 5
currently lose_sum: 98.92354565858841
time_elpased: 1.604
batch start
#iterations: 6
currently lose_sum: 98.61333572864532
time_elpased: 1.588
batch start
#iterations: 7
currently lose_sum: 98.51094657182693
time_elpased: 1.614
batch start
#iterations: 8
currently lose_sum: 98.45506882667542
time_elpased: 1.631
batch start
#iterations: 9
currently lose_sum: 98.0514349937439
time_elpased: 1.581
batch start
#iterations: 10
currently lose_sum: 97.81061136722565
time_elpased: 1.633
batch start
#iterations: 11
currently lose_sum: 97.80290484428406
time_elpased: 1.615
batch start
#iterations: 12
currently lose_sum: 97.66504508256912
time_elpased: 1.684
batch start
#iterations: 13
currently lose_sum: 97.58970034122467
time_elpased: 1.684
batch start
#iterations: 14
currently lose_sum: 97.27827036380768
time_elpased: 1.642
batch start
#iterations: 15
currently lose_sum: 97.39948463439941
time_elpased: 1.657
batch start
#iterations: 16
currently lose_sum: 97.36038148403168
time_elpased: 1.607
batch start
#iterations: 17
currently lose_sum: 97.04923516511917
time_elpased: 1.625
batch start
#iterations: 18
currently lose_sum: 97.08272278308868
time_elpased: 1.631
batch start
#iterations: 19
currently lose_sum: 97.18633395433426
time_elpased: 1.672
start validation test
0.652525773196
0.638831871618
0.704641350211
0.670124785907
0.652434276262
62.450
batch start
#iterations: 20
currently lose_sum: 96.99616581201553
time_elpased: 1.659
batch start
#iterations: 21
currently lose_sum: 96.83604860305786
time_elpased: 1.67
batch start
#iterations: 22
currently lose_sum: 96.80888503789902
time_elpased: 1.611
batch start
#iterations: 23
currently lose_sum: 97.16744697093964
time_elpased: 1.596
batch start
#iterations: 24
currently lose_sum: 96.86656385660172
time_elpased: 1.633
batch start
#iterations: 25
currently lose_sum: 96.65280896425247
time_elpased: 1.594
batch start
#iterations: 26
currently lose_sum: 96.62047529220581
time_elpased: 1.644
batch start
#iterations: 27
currently lose_sum: 96.59531491994858
time_elpased: 1.604
batch start
#iterations: 28
currently lose_sum: 96.39377188682556
time_elpased: 1.629
batch start
#iterations: 29
currently lose_sum: 96.98518520593643
time_elpased: 1.68
batch start
#iterations: 30
currently lose_sum: 96.32402449846268
time_elpased: 1.68
batch start
#iterations: 31
currently lose_sum: 96.10515260696411
time_elpased: 1.644
batch start
#iterations: 32
currently lose_sum: 96.32303029298782
time_elpased: 1.587
batch start
#iterations: 33
currently lose_sum: 96.46493089199066
time_elpased: 1.66
batch start
#iterations: 34
currently lose_sum: 96.2238455414772
time_elpased: 1.645
batch start
#iterations: 35
currently lose_sum: 96.47204214334488
time_elpased: 1.675
batch start
#iterations: 36
currently lose_sum: 96.46406853199005
time_elpased: 1.629
batch start
#iterations: 37
currently lose_sum: 96.35285770893097
time_elpased: 1.684
batch start
#iterations: 38
currently lose_sum: 95.9518711566925
time_elpased: 1.594
batch start
#iterations: 39
currently lose_sum: 96.22744023799896
time_elpased: 1.612
start validation test
0.654381443299
0.648814229249
0.675722959761
0.66199526138
0.654343974975
61.574
batch start
#iterations: 40
currently lose_sum: 96.5088112950325
time_elpased: 1.602
batch start
#iterations: 41
currently lose_sum: 96.10358726978302
time_elpased: 1.625
batch start
#iterations: 42
currently lose_sum: 96.02088797092438
time_elpased: 1.693
batch start
#iterations: 43
currently lose_sum: 96.20508301258087
time_elpased: 1.646
batch start
#iterations: 44
currently lose_sum: 95.97663342952728
time_elpased: 1.652
batch start
#iterations: 45
currently lose_sum: 95.9029832482338
time_elpased: 1.64
batch start
#iterations: 46
currently lose_sum: 95.53195947408676
time_elpased: 1.686
batch start
#iterations: 47
currently lose_sum: 96.26715075969696
time_elpased: 1.642
batch start
#iterations: 48
currently lose_sum: 96.03018617630005
time_elpased: 1.644
batch start
#iterations: 49
currently lose_sum: 95.96948611736298
time_elpased: 1.656
batch start
#iterations: 50
currently lose_sum: 96.51632332801819
time_elpased: 1.594
batch start
#iterations: 51
currently lose_sum: 96.07132989168167
time_elpased: 1.682
batch start
#iterations: 52
currently lose_sum: 96.13074415922165
time_elpased: 1.651
batch start
#iterations: 53
currently lose_sum: 96.07517296075821
time_elpased: 1.631
batch start
#iterations: 54
currently lose_sum: 96.00846230983734
time_elpased: 1.668
batch start
#iterations: 55
currently lose_sum: 95.82542705535889
time_elpased: 1.609
batch start
#iterations: 56
currently lose_sum: 95.75218230485916
time_elpased: 1.678
batch start
#iterations: 57
currently lose_sum: 95.8063975572586
time_elpased: 1.633
batch start
#iterations: 58
currently lose_sum: 95.96844828128815
time_elpased: 1.671
batch start
#iterations: 59
currently lose_sum: 95.77800357341766
time_elpased: 1.635
start validation test
0.658505154639
0.646152391756
0.703406401153
0.673564917467
0.658426323575
61.267
batch start
#iterations: 60
currently lose_sum: 95.45251262187958
time_elpased: 1.651
batch start
#iterations: 61
currently lose_sum: 96.18607592582703
time_elpased: 1.61
batch start
#iterations: 62
currently lose_sum: 95.75260335206985
time_elpased: 1.64
batch start
#iterations: 63
currently lose_sum: 96.22750180959702
time_elpased: 1.598
batch start
#iterations: 64
currently lose_sum: 95.5186573266983
time_elpased: 1.577
batch start
#iterations: 65
currently lose_sum: 95.47093993425369
time_elpased: 1.626
batch start
#iterations: 66
currently lose_sum: 95.67202007770538
time_elpased: 1.596
batch start
#iterations: 67
currently lose_sum: 95.87406539916992
time_elpased: 1.606
batch start
#iterations: 68
currently lose_sum: 96.01843494176865
time_elpased: 1.666
batch start
#iterations: 69
currently lose_sum: 95.86612665653229
time_elpased: 1.677
batch start
#iterations: 70
currently lose_sum: 95.55873620510101
time_elpased: 1.625
batch start
#iterations: 71
currently lose_sum: 95.55338203907013
time_elpased: 1.643
batch start
#iterations: 72
currently lose_sum: 95.84393155574799
time_elpased: 1.609
batch start
#iterations: 73
currently lose_sum: 95.46059781312943
time_elpased: 1.679
batch start
#iterations: 74
currently lose_sum: 95.58197969198227
time_elpased: 1.587
batch start
#iterations: 75
currently lose_sum: 95.50164651870728
time_elpased: 1.652
batch start
#iterations: 76
currently lose_sum: 95.52504903078079
time_elpased: 1.587
batch start
#iterations: 77
currently lose_sum: 95.57788985967636
time_elpased: 1.614
batch start
#iterations: 78
currently lose_sum: 95.89330017566681
time_elpased: 1.623
batch start
#iterations: 79
currently lose_sum: 95.53492993116379
time_elpased: 1.602
start validation test
0.612371134021
0.668974003999
0.447566121231
0.536317671723
0.61266047464
62.669
batch start
#iterations: 80
currently lose_sum: 95.73600417375565
time_elpased: 1.665
batch start
#iterations: 81
currently lose_sum: 95.71041947603226
time_elpased: 1.611
batch start
#iterations: 82
currently lose_sum: 95.73676067590714
time_elpased: 1.636
batch start
#iterations: 83
currently lose_sum: 95.47125339508057
time_elpased: 1.597
batch start
#iterations: 84
currently lose_sum: 95.60828918218613
time_elpased: 1.621
batch start
#iterations: 85
currently lose_sum: 95.73742341995239
time_elpased: 1.649
batch start
#iterations: 86
currently lose_sum: 95.68520748615265
time_elpased: 1.6
batch start
#iterations: 87
currently lose_sum: 95.13342851400375
time_elpased: 1.654
batch start
#iterations: 88
currently lose_sum: 95.54671663045883
time_elpased: 1.585
batch start
#iterations: 89
currently lose_sum: 95.46777510643005
time_elpased: 1.655
batch start
#iterations: 90
currently lose_sum: 95.64680176973343
time_elpased: 1.621
batch start
#iterations: 91
currently lose_sum: 95.2205759882927
time_elpased: 1.632
batch start
#iterations: 92
currently lose_sum: 95.42200994491577
time_elpased: 1.638
batch start
#iterations: 93
currently lose_sum: 95.25349420309067
time_elpased: 1.608
batch start
#iterations: 94
currently lose_sum: 95.54528439044952
time_elpased: 1.7
batch start
#iterations: 95
currently lose_sum: 95.5907654762268
time_elpased: 1.627
batch start
#iterations: 96
currently lose_sum: 95.73720836639404
time_elpased: 1.62
batch start
#iterations: 97
currently lose_sum: 95.4550017118454
time_elpased: 1.631
batch start
#iterations: 98
currently lose_sum: 95.56220018863678
time_elpased: 1.659
batch start
#iterations: 99
currently lose_sum: 94.99827975034714
time_elpased: 1.574
start validation test
0.667268041237
0.630647228452
0.8102294947
0.709247331201
0.667017050355
60.918
batch start
#iterations: 100
currently lose_sum: 95.32600557804108
time_elpased: 1.634
batch start
#iterations: 101
currently lose_sum: 95.41379582881927
time_elpased: 1.607
batch start
#iterations: 102
currently lose_sum: 94.95430374145508
time_elpased: 1.612
batch start
#iterations: 103
currently lose_sum: 95.25467216968536
time_elpased: 1.592
batch start
#iterations: 104
currently lose_sum: 95.56288015842438
time_elpased: 1.624
batch start
#iterations: 105
currently lose_sum: 95.35861283540726
time_elpased: 1.647
batch start
#iterations: 106
currently lose_sum: 95.5436543226242
time_elpased: 1.614
batch start
#iterations: 107
currently lose_sum: 95.97826725244522
time_elpased: 1.654
batch start
#iterations: 108
currently lose_sum: 95.27004659175873
time_elpased: 1.599
batch start
#iterations: 109
currently lose_sum: 95.33171772956848
time_elpased: 1.71
batch start
#iterations: 110
currently lose_sum: 95.1927461028099
time_elpased: 1.639
batch start
#iterations: 111
currently lose_sum: 95.0950574874878
time_elpased: 1.629
batch start
#iterations: 112
currently lose_sum: 95.3286983370781
time_elpased: 1.695
batch start
#iterations: 113
currently lose_sum: 95.2470234632492
time_elpased: 1.623
batch start
#iterations: 114
currently lose_sum: 94.89877390861511
time_elpased: 1.675
batch start
#iterations: 115
currently lose_sum: 95.10626077651978
time_elpased: 1.61
batch start
#iterations: 116
currently lose_sum: 95.44789695739746
time_elpased: 1.638
batch start
#iterations: 117
currently lose_sum: 95.4203896522522
time_elpased: 1.684
batch start
#iterations: 118
currently lose_sum: 95.2806276679039
time_elpased: 1.611
batch start
#iterations: 119
currently lose_sum: 95.42349398136139
time_elpased: 1.608
start validation test
0.664329896907
0.661039091549
0.67695790882
0.668903803132
0.664307726485
61.158
batch start
#iterations: 120
currently lose_sum: 95.08503782749176
time_elpased: 1.614
batch start
#iterations: 121
currently lose_sum: 95.19218343496323
time_elpased: 1.606
batch start
#iterations: 122
currently lose_sum: 95.35739850997925
time_elpased: 1.646
batch start
#iterations: 123
currently lose_sum: 95.01158708333969
time_elpased: 1.651
batch start
#iterations: 124
currently lose_sum: 95.22105866670609
time_elpased: 1.69
batch start
#iterations: 125
currently lose_sum: 95.34056687355042
time_elpased: 1.645
batch start
#iterations: 126
currently lose_sum: 95.45697700977325
time_elpased: 1.635
batch start
#iterations: 127
currently lose_sum: 95.30523037910461
time_elpased: 1.692
batch start
#iterations: 128
currently lose_sum: 94.92211627960205
time_elpased: 1.585
batch start
#iterations: 129
currently lose_sum: 94.82239073514938
time_elpased: 1.649
batch start
#iterations: 130
currently lose_sum: 95.22687619924545
time_elpased: 1.691
batch start
#iterations: 131
currently lose_sum: 95.2682095170021
time_elpased: 1.624
batch start
#iterations: 132
currently lose_sum: 95.14261478185654
time_elpased: 1.64
batch start
#iterations: 133
currently lose_sum: 94.97852444648743
time_elpased: 1.646
batch start
#iterations: 134
currently lose_sum: 95.22477865219116
time_elpased: 1.646
batch start
#iterations: 135
currently lose_sum: 94.9256283044815
time_elpased: 1.637
batch start
#iterations: 136
currently lose_sum: 95.13002735376358
time_elpased: 1.647
batch start
#iterations: 137
currently lose_sum: 94.97798329591751
time_elpased: 1.636
batch start
#iterations: 138
currently lose_sum: 94.84372174739838
time_elpased: 1.693
batch start
#iterations: 139
currently lose_sum: 95.52544492483139
time_elpased: 1.595
start validation test
0.663195876289
0.632327263657
0.78264896573
0.699503311258
0.662986157966
61.130
batch start
#iterations: 140
currently lose_sum: 95.5447565317154
time_elpased: 1.683
batch start
#iterations: 141
currently lose_sum: 94.90807247161865
time_elpased: 1.626
batch start
#iterations: 142
currently lose_sum: 95.10656368732452
time_elpased: 1.622
batch start
#iterations: 143
currently lose_sum: 95.16657876968384
time_elpased: 1.626
batch start
#iterations: 144
currently lose_sum: 95.37528294324875
time_elpased: 1.629
batch start
#iterations: 145
currently lose_sum: 95.13084614276886
time_elpased: 1.634
batch start
#iterations: 146
currently lose_sum: 95.00121116638184
time_elpased: 1.632
batch start
#iterations: 147
currently lose_sum: 95.19387978315353
time_elpased: 1.628
batch start
#iterations: 148
currently lose_sum: 95.35221701860428
time_elpased: 1.642
batch start
#iterations: 149
currently lose_sum: 94.95831674337387
time_elpased: 1.664
batch start
#iterations: 150
currently lose_sum: 94.79847675561905
time_elpased: 1.603
batch start
#iterations: 151
currently lose_sum: 95.26915144920349
time_elpased: 1.589
batch start
#iterations: 152
currently lose_sum: 95.49059945344925
time_elpased: 1.684
batch start
#iterations: 153
currently lose_sum: 95.17189449071884
time_elpased: 1.609
batch start
#iterations: 154
currently lose_sum: 95.19096446037292
time_elpased: 1.668
batch start
#iterations: 155
currently lose_sum: 94.99707865715027
time_elpased: 1.636
batch start
#iterations: 156
currently lose_sum: 95.20351082086563
time_elpased: 1.614
batch start
#iterations: 157
currently lose_sum: 95.41234177350998
time_elpased: 1.612
batch start
#iterations: 158
currently lose_sum: 95.74444383382797
time_elpased: 1.588
batch start
#iterations: 159
currently lose_sum: 95.51406705379486
time_elpased: 1.617
start validation test
0.664845360825
0.660991487231
0.679221982093
0.669982742869
0.664820120449
61.236
batch start
#iterations: 160
currently lose_sum: 94.9480174779892
time_elpased: 1.663
batch start
#iterations: 161
currently lose_sum: 94.96245712041855
time_elpased: 1.637
batch start
#iterations: 162
currently lose_sum: 94.79542237520218
time_elpased: 1.658
batch start
#iterations: 163
currently lose_sum: 94.85290098190308
time_elpased: 1.643
batch start
#iterations: 164
currently lose_sum: 95.06252253055573
time_elpased: 1.647
batch start
#iterations: 165
currently lose_sum: 95.340012550354
time_elpased: 1.632
batch start
#iterations: 166
currently lose_sum: 95.03144013881683
time_elpased: 1.658
batch start
#iterations: 167
currently lose_sum: 95.06699693202972
time_elpased: 1.618
batch start
#iterations: 168
currently lose_sum: 94.79809975624084
time_elpased: 1.671
batch start
#iterations: 169
currently lose_sum: 94.93388986587524
time_elpased: 1.635
batch start
#iterations: 170
currently lose_sum: 94.9703534245491
time_elpased: 1.683
batch start
#iterations: 171
currently lose_sum: 95.13859272003174
time_elpased: 1.681
batch start
#iterations: 172
currently lose_sum: 95.2066655755043
time_elpased: 1.606
batch start
#iterations: 173
currently lose_sum: 94.96710187196732
time_elpased: 1.708
batch start
#iterations: 174
currently lose_sum: 94.94843304157257
time_elpased: 1.689
batch start
#iterations: 175
currently lose_sum: 94.65593379735947
time_elpased: 1.687
batch start
#iterations: 176
currently lose_sum: 94.89250379800797
time_elpased: 1.641
batch start
#iterations: 177
currently lose_sum: 94.88386398553848
time_elpased: 1.633
batch start
#iterations: 178
currently lose_sum: 94.89324867725372
time_elpased: 1.658
batch start
#iterations: 179
currently lose_sum: 95.29412627220154
time_elpased: 1.606
start validation test
0.662886597938
0.631902349913
0.783163527838
0.699448529412
0.662675433236
60.815
batch start
#iterations: 180
currently lose_sum: 95.040318608284
time_elpased: 1.636
batch start
#iterations: 181
currently lose_sum: 94.84012120962143
time_elpased: 1.574
batch start
#iterations: 182
currently lose_sum: 94.64975219964981
time_elpased: 1.694
batch start
#iterations: 183
currently lose_sum: 94.5602480173111
time_elpased: 1.612
batch start
#iterations: 184
currently lose_sum: 94.9588440656662
time_elpased: 1.579
batch start
#iterations: 185
currently lose_sum: 94.92620033025742
time_elpased: 1.675
batch start
#iterations: 186
currently lose_sum: 94.52207839488983
time_elpased: 1.628
batch start
#iterations: 187
currently lose_sum: 94.91890680789948
time_elpased: 1.64
batch start
#iterations: 188
currently lose_sum: 94.68264335393906
time_elpased: 1.613
batch start
#iterations: 189
currently lose_sum: 95.0350152850151
time_elpased: 1.662
batch start
#iterations: 190
currently lose_sum: 95.05959671735764
time_elpased: 1.652
batch start
#iterations: 191
currently lose_sum: 94.95728302001953
time_elpased: 1.673
batch start
#iterations: 192
currently lose_sum: 94.8908252120018
time_elpased: 1.662
batch start
#iterations: 193
currently lose_sum: 94.84202951192856
time_elpased: 1.685
batch start
#iterations: 194
currently lose_sum: 95.15072298049927
time_elpased: 1.706
batch start
#iterations: 195
currently lose_sum: 95.03881525993347
time_elpased: 1.667
batch start
#iterations: 196
currently lose_sum: 95.43644946813583
time_elpased: 1.663
batch start
#iterations: 197
currently lose_sum: 94.73940008878708
time_elpased: 1.645
batch start
#iterations: 198
currently lose_sum: 94.84826564788818
time_elpased: 1.614
batch start
#iterations: 199
currently lose_sum: 94.89438056945801
time_elpased: 1.63
start validation test
0.662010309278
0.63392100356
0.769682000617
0.695235881943
0.661821275017
60.718
batch start
#iterations: 200
currently lose_sum: 94.71558010578156
time_elpased: 1.63
batch start
#iterations: 201
currently lose_sum: 94.54690670967102
time_elpased: 1.667
batch start
#iterations: 202
currently lose_sum: 95.13886606693268
time_elpased: 1.664
batch start
#iterations: 203
currently lose_sum: 94.4748398065567
time_elpased: 1.677
batch start
#iterations: 204
currently lose_sum: 94.59815979003906
time_elpased: 1.653
batch start
#iterations: 205
currently lose_sum: 95.03466314077377
time_elpased: 1.646
batch start
#iterations: 206
currently lose_sum: 94.80149948596954
time_elpased: 1.641
batch start
#iterations: 207
currently lose_sum: 95.08942401409149
time_elpased: 1.642
batch start
#iterations: 208
currently lose_sum: 94.42901331186295
time_elpased: 1.611
batch start
#iterations: 209
currently lose_sum: 94.79823791980743
time_elpased: 1.59
batch start
#iterations: 210
currently lose_sum: 94.60183066129684
time_elpased: 1.601
batch start
#iterations: 211
currently lose_sum: 94.2513558268547
time_elpased: 1.585
batch start
#iterations: 212
currently lose_sum: 94.75536584854126
time_elpased: 1.639
batch start
#iterations: 213
currently lose_sum: 94.80341249704361
time_elpased: 1.604
batch start
#iterations: 214
currently lose_sum: 94.7770100235939
time_elpased: 1.635
batch start
#iterations: 215
currently lose_sum: 94.53007328510284
time_elpased: 1.634
batch start
#iterations: 216
currently lose_sum: 94.47536396980286
time_elpased: 1.634
batch start
#iterations: 217
currently lose_sum: 95.18932163715363
time_elpased: 1.557
batch start
#iterations: 218
currently lose_sum: 94.97887188196182
time_elpased: 1.626
batch start
#iterations: 219
currently lose_sum: 95.02661782503128
time_elpased: 1.63
start validation test
0.668865979381
0.645411993288
0.752083976536
0.694676806084
0.668719877352
60.839
batch start
#iterations: 220
currently lose_sum: 94.48111271858215
time_elpased: 1.631
batch start
#iterations: 221
currently lose_sum: 94.8585444688797
time_elpased: 1.654
batch start
#iterations: 222
currently lose_sum: 94.67980659008026
time_elpased: 1.6
batch start
#iterations: 223
currently lose_sum: 94.71102893352509
time_elpased: 1.643
batch start
#iterations: 224
currently lose_sum: 94.8178705573082
time_elpased: 1.628
batch start
#iterations: 225
currently lose_sum: 94.67730504274368
time_elpased: 1.666
batch start
#iterations: 226
currently lose_sum: 94.41422373056412
time_elpased: 1.642
batch start
#iterations: 227
currently lose_sum: 94.82585352659225
time_elpased: 1.653
batch start
#iterations: 228
currently lose_sum: 94.53848177194595
time_elpased: 1.594
batch start
#iterations: 229
currently lose_sum: 94.46192920207977
time_elpased: 1.693
batch start
#iterations: 230
currently lose_sum: 94.83423715829849
time_elpased: 1.64
batch start
#iterations: 231
currently lose_sum: 94.5741907954216
time_elpased: 1.658
batch start
#iterations: 232
currently lose_sum: 94.65023028850555
time_elpased: 1.61
batch start
#iterations: 233
currently lose_sum: 94.84660565853119
time_elpased: 1.649
batch start
#iterations: 234
currently lose_sum: 94.68845075368881
time_elpased: 1.625
batch start
#iterations: 235
currently lose_sum: 94.88505899906158
time_elpased: 1.695
batch start
#iterations: 236
currently lose_sum: 94.82769650220871
time_elpased: 1.65
batch start
#iterations: 237
currently lose_sum: 95.05178397893906
time_elpased: 1.644
batch start
#iterations: 238
currently lose_sum: 94.92275017499924
time_elpased: 1.61
batch start
#iterations: 239
currently lose_sum: 94.75307965278625
time_elpased: 1.564
start validation test
0.663659793814
0.651913192461
0.704847175054
0.677347574544
0.663587483014
60.876
batch start
#iterations: 240
currently lose_sum: 94.71008878946304
time_elpased: 1.692
batch start
#iterations: 241
currently lose_sum: 94.67403173446655
time_elpased: 1.609
batch start
#iterations: 242
currently lose_sum: 94.67683881521225
time_elpased: 1.652
batch start
#iterations: 243
currently lose_sum: 95.24138802289963
time_elpased: 1.593
batch start
#iterations: 244
currently lose_sum: 94.70201462507248
time_elpased: 1.654
batch start
#iterations: 245
currently lose_sum: 94.75235772132874
time_elpased: 1.627
batch start
#iterations: 246
currently lose_sum: 94.6870169043541
time_elpased: 1.618
batch start
#iterations: 247
currently lose_sum: 94.91742324829102
time_elpased: 1.588
batch start
#iterations: 248
currently lose_sum: 94.67155140638351
time_elpased: 1.617
batch start
#iterations: 249
currently lose_sum: 94.60050398111343
time_elpased: 1.619
batch start
#iterations: 250
currently lose_sum: 94.56288701295853
time_elpased: 1.665
batch start
#iterations: 251
currently lose_sum: 94.9005566239357
time_elpased: 1.64
batch start
#iterations: 252
currently lose_sum: 94.67094647884369
time_elpased: 1.645
batch start
#iterations: 253
currently lose_sum: 95.05998754501343
time_elpased: 1.609
batch start
#iterations: 254
currently lose_sum: 94.44409137964249
time_elpased: 1.653
batch start
#iterations: 255
currently lose_sum: 94.82658243179321
time_elpased: 1.644
batch start
#iterations: 256
currently lose_sum: 94.34641474485397
time_elpased: 1.632
batch start
#iterations: 257
currently lose_sum: 94.30411511659622
time_elpased: 1.645
batch start
#iterations: 258
currently lose_sum: 94.40118235349655
time_elpased: 1.648
batch start
#iterations: 259
currently lose_sum: 94.42150408029556
time_elpased: 1.662
start validation test
0.664793814433
0.633405279761
0.785221776268
0.701190093278
0.664582384571
60.501
batch start
#iterations: 260
currently lose_sum: 94.41285347938538
time_elpased: 1.616
batch start
#iterations: 261
currently lose_sum: 94.61529016494751
time_elpased: 1.637
batch start
#iterations: 262
currently lose_sum: 94.44899982213974
time_elpased: 1.655
batch start
#iterations: 263
currently lose_sum: 94.35351830720901
time_elpased: 1.666
batch start
#iterations: 264
currently lose_sum: 94.77524465322495
time_elpased: 1.655
batch start
#iterations: 265
currently lose_sum: 94.95165622234344
time_elpased: 1.655
batch start
#iterations: 266
currently lose_sum: 94.77266776561737
time_elpased: 1.573
batch start
#iterations: 267
currently lose_sum: 94.51654708385468
time_elpased: 1.586
batch start
#iterations: 268
currently lose_sum: 94.56928902864456
time_elpased: 1.628
batch start
#iterations: 269
currently lose_sum: 94.52180421352386
time_elpased: 1.589
batch start
#iterations: 270
currently lose_sum: 94.36267232894897
time_elpased: 1.657
batch start
#iterations: 271
currently lose_sum: 94.38339745998383
time_elpased: 1.61
batch start
#iterations: 272
currently lose_sum: 94.77999234199524
time_elpased: 1.619
batch start
#iterations: 273
currently lose_sum: 94.48942494392395
time_elpased: 1.617
batch start
#iterations: 274
currently lose_sum: 95.24687933921814
time_elpased: 1.621
batch start
#iterations: 275
currently lose_sum: 94.60088574886322
time_elpased: 1.67
batch start
#iterations: 276
currently lose_sum: 94.24648714065552
time_elpased: 1.616
batch start
#iterations: 277
currently lose_sum: 94.80374389886856
time_elpased: 1.618
batch start
#iterations: 278
currently lose_sum: 94.47220385074615
time_elpased: 1.59
batch start
#iterations: 279
currently lose_sum: 94.09173291921616
time_elpased: 1.564
start validation test
0.661958762887
0.652593952275
0.69517340743
0.67321108232
0.661900449455
60.887
batch start
#iterations: 280
currently lose_sum: 94.66825878620148
time_elpased: 1.634
batch start
#iterations: 281
currently lose_sum: 94.71515917778015
time_elpased: 1.637
batch start
#iterations: 282
currently lose_sum: 94.22530353069305
time_elpased: 1.669
batch start
#iterations: 283
currently lose_sum: 94.84497213363647
time_elpased: 1.589
batch start
#iterations: 284
currently lose_sum: 94.58451449871063
time_elpased: 1.653
batch start
#iterations: 285
currently lose_sum: 94.59814661741257
time_elpased: 1.68
batch start
#iterations: 286
currently lose_sum: 93.93782371282578
time_elpased: 1.616
batch start
#iterations: 287
currently lose_sum: 94.34722465276718
time_elpased: 1.631
batch start
#iterations: 288
currently lose_sum: 94.06509381532669
time_elpased: 1.659
batch start
#iterations: 289
currently lose_sum: 95.20506608486176
time_elpased: 1.603
batch start
#iterations: 290
currently lose_sum: 94.71448612213135
time_elpased: 1.629
batch start
#iterations: 291
currently lose_sum: 94.34143334627151
time_elpased: 1.595
batch start
#iterations: 292
currently lose_sum: 94.73517227172852
time_elpased: 1.586
batch start
#iterations: 293
currently lose_sum: 94.73940443992615
time_elpased: 1.645
batch start
#iterations: 294
currently lose_sum: 94.73622435331345
time_elpased: 1.616
batch start
#iterations: 295
currently lose_sum: 94.76102513074875
time_elpased: 1.606
batch start
#iterations: 296
currently lose_sum: 94.27853935956955
time_elpased: 1.648
batch start
#iterations: 297
currently lose_sum: 94.71886360645294
time_elpased: 1.696
batch start
#iterations: 298
currently lose_sum: 94.46862918138504
time_elpased: 1.605
batch start
#iterations: 299
currently lose_sum: 94.3942021727562
time_elpased: 1.616
start validation test
0.669484536082
0.638111157543
0.785736338376
0.704270823725
0.669280438113
60.436
batch start
#iterations: 300
currently lose_sum: 94.01428246498108
time_elpased: 1.613
batch start
#iterations: 301
currently lose_sum: 94.70272266864777
time_elpased: 1.666
batch start
#iterations: 302
currently lose_sum: 94.5290322303772
time_elpased: 1.651
batch start
#iterations: 303
currently lose_sum: 94.54893136024475
time_elpased: 1.68
batch start
#iterations: 304
currently lose_sum: 94.53448766469955
time_elpased: 1.637
batch start
#iterations: 305
currently lose_sum: 94.7773574590683
time_elpased: 1.669
batch start
#iterations: 306
currently lose_sum: 94.68306005001068
time_elpased: 1.671
batch start
#iterations: 307
currently lose_sum: 94.56877273321152
time_elpased: 1.606
batch start
#iterations: 308
currently lose_sum: 94.39967757463455
time_elpased: 1.594
batch start
#iterations: 309
currently lose_sum: 94.4735934138298
time_elpased: 1.669
batch start
#iterations: 310
currently lose_sum: 94.69714856147766
time_elpased: 1.634
batch start
#iterations: 311
currently lose_sum: 94.09032881259918
time_elpased: 1.652
batch start
#iterations: 312
currently lose_sum: 94.37908387184143
time_elpased: 1.608
batch start
#iterations: 313
currently lose_sum: 94.95684152841568
time_elpased: 1.688
batch start
#iterations: 314
currently lose_sum: 94.40994876623154
time_elpased: 1.64
batch start
#iterations: 315
currently lose_sum: 94.67964082956314
time_elpased: 1.63
batch start
#iterations: 316
currently lose_sum: 94.40147703886032
time_elpased: 1.648
batch start
#iterations: 317
currently lose_sum: 94.46483033895493
time_elpased: 1.622
batch start
#iterations: 318
currently lose_sum: 94.46536350250244
time_elpased: 1.635
batch start
#iterations: 319
currently lose_sum: 94.93546044826508
time_elpased: 1.646
start validation test
0.659536082474
0.659622486664
0.661726870433
0.660673002826
0.659532236208
61.089
batch start
#iterations: 320
currently lose_sum: 94.6142029762268
time_elpased: 1.637
batch start
#iterations: 321
currently lose_sum: 94.42835366725922
time_elpased: 1.614
batch start
#iterations: 322
currently lose_sum: 94.69431114196777
time_elpased: 1.647
batch start
#iterations: 323
currently lose_sum: 95.00618225336075
time_elpased: 1.586
batch start
#iterations: 324
currently lose_sum: 94.68280893564224
time_elpased: 1.595
batch start
#iterations: 325
currently lose_sum: 94.53587734699249
time_elpased: 1.593
batch start
#iterations: 326
currently lose_sum: 94.74394696950912
time_elpased: 1.592
batch start
#iterations: 327
currently lose_sum: 94.16336780786514
time_elpased: 1.616
batch start
#iterations: 328
currently lose_sum: 94.66944175958633
time_elpased: 1.624
batch start
#iterations: 329
currently lose_sum: 94.29789346456528
time_elpased: 1.671
batch start
#iterations: 330
currently lose_sum: 94.3773707151413
time_elpased: 1.663
batch start
#iterations: 331
currently lose_sum: 94.12616711854935
time_elpased: 1.609
batch start
#iterations: 332
currently lose_sum: 94.287178337574
time_elpased: 1.638
batch start
#iterations: 333
currently lose_sum: 94.23994451761246
time_elpased: 1.631
batch start
#iterations: 334
currently lose_sum: 94.44832742214203
time_elpased: 1.671
batch start
#iterations: 335
currently lose_sum: 94.26052105426788
time_elpased: 1.621
batch start
#iterations: 336
currently lose_sum: 94.44262653589249
time_elpased: 1.66
batch start
#iterations: 337
currently lose_sum: 94.26922076940536
time_elpased: 1.614
batch start
#iterations: 338
currently lose_sum: 94.23288595676422
time_elpased: 1.649
batch start
#iterations: 339
currently lose_sum: 94.46429657936096
time_elpased: 1.612
start validation test
0.66881443299
0.643529822114
0.759493670886
0.696719376918
0.668655231601
60.667
batch start
#iterations: 340
currently lose_sum: 94.71987986564636
time_elpased: 1.651
batch start
#iterations: 341
currently lose_sum: 94.56454652547836
time_elpased: 1.669
batch start
#iterations: 342
currently lose_sum: 94.36013185977936
time_elpased: 1.618
batch start
#iterations: 343
currently lose_sum: 94.32097178697586
time_elpased: 1.679
batch start
#iterations: 344
currently lose_sum: 94.3709973692894
time_elpased: 1.609
batch start
#iterations: 345
currently lose_sum: 94.352254986763
time_elpased: 1.693
batch start
#iterations: 346
currently lose_sum: 94.97093045711517
time_elpased: 1.639
batch start
#iterations: 347
currently lose_sum: 94.5458374619484
time_elpased: 1.596
batch start
#iterations: 348
currently lose_sum: 94.74831414222717
time_elpased: 1.593
batch start
#iterations: 349
currently lose_sum: 94.47480595111847
time_elpased: 1.689
batch start
#iterations: 350
currently lose_sum: 94.55386900901794
time_elpased: 1.582
batch start
#iterations: 351
currently lose_sum: 94.45640796422958
time_elpased: 1.623
batch start
#iterations: 352
currently lose_sum: 94.58228588104248
time_elpased: 1.631
batch start
#iterations: 353
currently lose_sum: 94.920419216156
time_elpased: 1.6
batch start
#iterations: 354
currently lose_sum: 94.90386533737183
time_elpased: 1.599
batch start
#iterations: 355
currently lose_sum: 94.43700277805328
time_elpased: 1.645
batch start
#iterations: 356
currently lose_sum: 94.48797672986984
time_elpased: 1.645
batch start
#iterations: 357
currently lose_sum: 94.84188681840897
time_elpased: 1.64
batch start
#iterations: 358
currently lose_sum: 94.72375756502151
time_elpased: 1.588
batch start
#iterations: 359
currently lose_sum: 94.40000188350677
time_elpased: 1.636
start validation test
0.661597938144
0.635932378817
0.758773283935
0.691943127962
0.661427331837
60.836
batch start
#iterations: 360
currently lose_sum: 94.8543593287468
time_elpased: 1.617
batch start
#iterations: 361
currently lose_sum: 94.631118953228
time_elpased: 1.622
batch start
#iterations: 362
currently lose_sum: 93.82027131319046
time_elpased: 1.586
batch start
#iterations: 363
currently lose_sum: 94.71551549434662
time_elpased: 1.651
batch start
#iterations: 364
currently lose_sum: 94.77152252197266
time_elpased: 1.623
batch start
#iterations: 365
currently lose_sum: 94.68260359764099
time_elpased: 1.598
batch start
#iterations: 366
currently lose_sum: 94.52575123310089
time_elpased: 1.676
batch start
#iterations: 367
currently lose_sum: 93.99738717079163
time_elpased: 1.657
batch start
#iterations: 368
currently lose_sum: 94.70707362890244
time_elpased: 1.64
batch start
#iterations: 369
currently lose_sum: 94.3521558046341
time_elpased: 1.632
batch start
#iterations: 370
currently lose_sum: 94.31391459703445
time_elpased: 1.612
batch start
#iterations: 371
currently lose_sum: 94.61172699928284
time_elpased: 1.633
batch start
#iterations: 372
currently lose_sum: 94.65086150169373
time_elpased: 1.616
batch start
#iterations: 373
currently lose_sum: 94.36669999361038
time_elpased: 1.65
batch start
#iterations: 374
currently lose_sum: 94.35083174705505
time_elpased: 1.708
batch start
#iterations: 375
currently lose_sum: 94.35808020830154
time_elpased: 1.593
batch start
#iterations: 376
currently lose_sum: 94.12327915430069
time_elpased: 1.658
batch start
#iterations: 377
currently lose_sum: 94.42946356534958
time_elpased: 1.626
batch start
#iterations: 378
currently lose_sum: 94.50644189119339
time_elpased: 1.613
batch start
#iterations: 379
currently lose_sum: 94.46527969837189
time_elpased: 1.626
start validation test
0.668711340206
0.632575757576
0.807759596583
0.709514124294
0.668467219545
60.501
batch start
#iterations: 380
currently lose_sum: 94.489641726017
time_elpased: 1.676
batch start
#iterations: 381
currently lose_sum: 94.22209858894348
time_elpased: 1.635
batch start
#iterations: 382
currently lose_sum: 94.26987832784653
time_elpased: 1.581
batch start
#iterations: 383
currently lose_sum: 94.10746067762375
time_elpased: 1.63
batch start
#iterations: 384
currently lose_sum: 94.6581352353096
time_elpased: 1.622
batch start
#iterations: 385
currently lose_sum: 94.41791433095932
time_elpased: 1.668
batch start
#iterations: 386
currently lose_sum: 94.1894639134407
time_elpased: 1.634
batch start
#iterations: 387
currently lose_sum: 94.23906624317169
time_elpased: 1.64
batch start
#iterations: 388
currently lose_sum: 94.74290305376053
time_elpased: 1.595
batch start
#iterations: 389
currently lose_sum: 94.3941530585289
time_elpased: 1.653
batch start
#iterations: 390
currently lose_sum: 94.47576195001602
time_elpased: 1.63
batch start
#iterations: 391
currently lose_sum: 94.29251706600189
time_elpased: 1.695
batch start
#iterations: 392
currently lose_sum: 94.13142085075378
time_elpased: 1.648
batch start
#iterations: 393
currently lose_sum: 94.08466672897339
time_elpased: 1.643
batch start
#iterations: 394
currently lose_sum: 94.53387159109116
time_elpased: 1.648
batch start
#iterations: 395
currently lose_sum: 94.44625842571259
time_elpased: 1.673
batch start
#iterations: 396
currently lose_sum: 94.11174410581589
time_elpased: 1.582
batch start
#iterations: 397
currently lose_sum: 94.07611870765686
time_elpased: 1.626
batch start
#iterations: 398
currently lose_sum: 94.57173228263855
time_elpased: 1.668
batch start
#iterations: 399
currently lose_sum: 94.35059821605682
time_elpased: 1.674
start validation test
0.665360824742
0.642535136569
0.748070392096
0.691298145506
0.66521561534
60.732
acc: 0.670
pre: 0.639
rec: 0.784
F1: 0.704
auc: 0.713
