start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.29527693986893
time_elpased: 1.66
batch start
#iterations: 1
currently lose_sum: 99.98043930530548
time_elpased: 1.599
batch start
#iterations: 2
currently lose_sum: 99.42730236053467
time_elpased: 1.59
batch start
#iterations: 3
currently lose_sum: 99.32517021894455
time_elpased: 1.63
batch start
#iterations: 4
currently lose_sum: 98.88635593652725
time_elpased: 1.675
batch start
#iterations: 5
currently lose_sum: 98.81283915042877
time_elpased: 1.583
batch start
#iterations: 6
currently lose_sum: 98.56997162103653
time_elpased: 1.675
batch start
#iterations: 7
currently lose_sum: 98.35634583234787
time_elpased: 1.606
batch start
#iterations: 8
currently lose_sum: 98.4252422451973
time_elpased: 1.621
batch start
#iterations: 9
currently lose_sum: 97.92124271392822
time_elpased: 1.651
batch start
#iterations: 10
currently lose_sum: 98.06154519319534
time_elpased: 1.623
batch start
#iterations: 11
currently lose_sum: 97.62642019987106
time_elpased: 1.592
batch start
#iterations: 12
currently lose_sum: 97.81084871292114
time_elpased: 1.615
batch start
#iterations: 13
currently lose_sum: 97.55064779520035
time_elpased: 1.611
batch start
#iterations: 14
currently lose_sum: 97.469302713871
time_elpased: 1.614
batch start
#iterations: 15
currently lose_sum: 97.44192105531693
time_elpased: 1.658
batch start
#iterations: 16
currently lose_sum: 97.3840417265892
time_elpased: 1.632
batch start
#iterations: 17
currently lose_sum: 97.09793359041214
time_elpased: 1.604
batch start
#iterations: 18
currently lose_sum: 97.26766705513
time_elpased: 1.621
batch start
#iterations: 19
currently lose_sum: 97.09116750955582
time_elpased: 1.585
start validation test
0.652731958763
0.62341536167
0.77439275422
0.69075051641
0.652530949601
62.515
batch start
#iterations: 20
currently lose_sum: 96.79503279924393
time_elpased: 1.67
batch start
#iterations: 21
currently lose_sum: 96.84725558757782
time_elpased: 1.609
batch start
#iterations: 22
currently lose_sum: 96.750741481781
time_elpased: 1.607
batch start
#iterations: 23
currently lose_sum: 96.60506516695023
time_elpased: 1.579
batch start
#iterations: 24
currently lose_sum: 96.77021539211273
time_elpased: 1.578
batch start
#iterations: 25
currently lose_sum: 96.58111864328384
time_elpased: 1.666
batch start
#iterations: 26
currently lose_sum: 96.49795532226562
time_elpased: 1.622
batch start
#iterations: 27
currently lose_sum: 96.7961483001709
time_elpased: 1.621
batch start
#iterations: 28
currently lose_sum: 96.37190693616867
time_elpased: 1.67
batch start
#iterations: 29
currently lose_sum: 96.87589305639267
time_elpased: 1.647
batch start
#iterations: 30
currently lose_sum: 96.61937254667282
time_elpased: 1.666
batch start
#iterations: 31
currently lose_sum: 96.15097314119339
time_elpased: 1.689
batch start
#iterations: 32
currently lose_sum: 96.39023560285568
time_elpased: 1.585
batch start
#iterations: 33
currently lose_sum: 96.55032396316528
time_elpased: 1.565
batch start
#iterations: 34
currently lose_sum: 96.22793805599213
time_elpased: 1.662
batch start
#iterations: 35
currently lose_sum: 96.47043359279633
time_elpased: 1.622
batch start
#iterations: 36
currently lose_sum: 96.3411705493927
time_elpased: 1.641
batch start
#iterations: 37
currently lose_sum: 96.29581433534622
time_elpased: 1.606
batch start
#iterations: 38
currently lose_sum: 96.36621487140656
time_elpased: 1.634
batch start
#iterations: 39
currently lose_sum: 96.4265131354332
time_elpased: 1.583
start validation test
0.665257731959
0.64526600541
0.73651708522
0.687878496588
0.665139996555
61.680
batch start
#iterations: 40
currently lose_sum: 96.11163300275803
time_elpased: 1.641
batch start
#iterations: 41
currently lose_sum: 96.43993675708771
time_elpased: 1.531
batch start
#iterations: 42
currently lose_sum: 96.11890000104904
time_elpased: 1.663
batch start
#iterations: 43
currently lose_sum: 96.23033422231674
time_elpased: 1.608
batch start
#iterations: 44
currently lose_sum: 96.20565736293793
time_elpased: 1.616
batch start
#iterations: 45
currently lose_sum: 96.19513666629791
time_elpased: 1.636
batch start
#iterations: 46
currently lose_sum: 96.14538073539734
time_elpased: 1.59
batch start
#iterations: 47
currently lose_sum: 95.89512342214584
time_elpased: 1.629
batch start
#iterations: 48
currently lose_sum: 96.08471268415451
time_elpased: 1.568
batch start
#iterations: 49
currently lose_sum: 96.53357464075089
time_elpased: 1.584
batch start
#iterations: 50
currently lose_sum: 95.92549228668213
time_elpased: 1.579
batch start
#iterations: 51
currently lose_sum: 95.63981020450592
time_elpased: 1.641
batch start
#iterations: 52
currently lose_sum: 95.91264772415161
time_elpased: 1.571
batch start
#iterations: 53
currently lose_sum: 95.64402306079865
time_elpased: 1.61
batch start
#iterations: 54
currently lose_sum: 96.140625
time_elpased: 1.573
batch start
#iterations: 55
currently lose_sum: 96.05315434932709
time_elpased: 1.615
batch start
#iterations: 56
currently lose_sum: 95.6960318684578
time_elpased: 1.588
batch start
#iterations: 57
currently lose_sum: 96.31523597240448
time_elpased: 1.558
batch start
#iterations: 58
currently lose_sum: 95.92335134744644
time_elpased: 1.577
batch start
#iterations: 59
currently lose_sum: 95.61377775669098
time_elpased: 1.636
start validation test
0.662886597938
0.637919055063
0.755969534788
0.691945360339
0.662732805395
61.509
batch start
#iterations: 60
currently lose_sum: 95.65258437395096
time_elpased: 1.603
batch start
#iterations: 61
currently lose_sum: 96.17882418632507
time_elpased: 1.583
batch start
#iterations: 62
currently lose_sum: 96.23296058177948
time_elpased: 1.643
batch start
#iterations: 63
currently lose_sum: 95.72999566793442
time_elpased: 1.624
batch start
#iterations: 64
currently lose_sum: 95.79848718643188
time_elpased: 1.602
batch start
#iterations: 65
currently lose_sum: 95.8766148686409
time_elpased: 1.609
batch start
#iterations: 66
currently lose_sum: 95.66615515947342
time_elpased: 1.589
batch start
#iterations: 67
currently lose_sum: 95.67987233400345
time_elpased: 1.625
batch start
#iterations: 68
currently lose_sum: 95.78868198394775
time_elpased: 1.691
batch start
#iterations: 69
currently lose_sum: 95.95178258419037
time_elpased: 1.599
batch start
#iterations: 70
currently lose_sum: 95.62929874658585
time_elpased: 1.608
batch start
#iterations: 71
currently lose_sum: 95.83618986606598
time_elpased: 1.604
batch start
#iterations: 72
currently lose_sum: 95.56085413694382
time_elpased: 1.565
batch start
#iterations: 73
currently lose_sum: 95.81623810529709
time_elpased: 1.59
batch start
#iterations: 74
currently lose_sum: 95.51153790950775
time_elpased: 1.665
batch start
#iterations: 75
currently lose_sum: 95.68036007881165
time_elpased: 1.641
batch start
#iterations: 76
currently lose_sum: 95.45656514167786
time_elpased: 1.633
batch start
#iterations: 77
currently lose_sum: 95.92470186948776
time_elpased: 1.61
batch start
#iterations: 78
currently lose_sum: 95.88236153125763
time_elpased: 1.616
batch start
#iterations: 79
currently lose_sum: 95.55227208137512
time_elpased: 1.646
start validation test
0.664020618557
0.635050675676
0.773878139152
0.697624791241
0.663839110881
61.138
batch start
#iterations: 80
currently lose_sum: 95.65760844945908
time_elpased: 1.66
batch start
#iterations: 81
currently lose_sum: 95.27250587940216
time_elpased: 1.578
batch start
#iterations: 82
currently lose_sum: 95.91110616922379
time_elpased: 1.601
batch start
#iterations: 83
currently lose_sum: 95.63199377059937
time_elpased: 1.598
batch start
#iterations: 84
currently lose_sum: 95.5962278842926
time_elpased: 1.601
batch start
#iterations: 85
currently lose_sum: 95.67986214160919
time_elpased: 1.586
batch start
#iterations: 86
currently lose_sum: 95.39210140705109
time_elpased: 1.586
batch start
#iterations: 87
currently lose_sum: 95.55743682384491
time_elpased: 1.657
batch start
#iterations: 88
currently lose_sum: 95.43926954269409
time_elpased: 1.633
batch start
#iterations: 89
currently lose_sum: 95.98313558101654
time_elpased: 1.641
batch start
#iterations: 90
currently lose_sum: 95.49759238958359
time_elpased: 1.708
batch start
#iterations: 91
currently lose_sum: 95.58293932676315
time_elpased: 1.538
batch start
#iterations: 92
currently lose_sum: 95.68455272912979
time_elpased: 1.632
batch start
#iterations: 93
currently lose_sum: 95.37214708328247
time_elpased: 1.638
batch start
#iterations: 94
currently lose_sum: 95.62520515918732
time_elpased: 1.602
batch start
#iterations: 95
currently lose_sum: 95.60051041841507
time_elpased: 1.601
batch start
#iterations: 96
currently lose_sum: 95.74100250005722
time_elpased: 1.683
batch start
#iterations: 97
currently lose_sum: 95.19659167528152
time_elpased: 1.636
batch start
#iterations: 98
currently lose_sum: 95.4972083568573
time_elpased: 1.661
batch start
#iterations: 99
currently lose_sum: 95.56122809648514
time_elpased: 1.575
start validation test
0.659690721649
0.652916912198
0.684232194319
0.668207860086
0.659650173987
61.464
batch start
#iterations: 100
currently lose_sum: 95.65542411804199
time_elpased: 1.683
batch start
#iterations: 101
currently lose_sum: 95.59600645303726
time_elpased: 1.599
batch start
#iterations: 102
currently lose_sum: 95.77505433559418
time_elpased: 1.606
batch start
#iterations: 103
currently lose_sum: 95.44181650876999
time_elpased: 1.643
batch start
#iterations: 104
currently lose_sum: 95.14973878860474
time_elpased: 1.689
batch start
#iterations: 105
currently lose_sum: 95.50058311223984
time_elpased: 1.695
batch start
#iterations: 106
currently lose_sum: 95.3360503911972
time_elpased: 1.605
batch start
#iterations: 107
currently lose_sum: 95.4296065568924
time_elpased: 1.671
batch start
#iterations: 108
currently lose_sum: 95.40216326713562
time_elpased: 1.656
batch start
#iterations: 109
currently lose_sum: 95.5246593952179
time_elpased: 1.599
batch start
#iterations: 110
currently lose_sum: 95.33416163921356
time_elpased: 1.629
batch start
#iterations: 111
currently lose_sum: 95.42215341329575
time_elpased: 1.595
batch start
#iterations: 112
currently lose_sum: 95.19593942165375
time_elpased: 1.582
batch start
#iterations: 113
currently lose_sum: 95.24853086471558
time_elpased: 1.647
batch start
#iterations: 114
currently lose_sum: 95.61663210391998
time_elpased: 1.661
batch start
#iterations: 115
currently lose_sum: 95.36703771352768
time_elpased: 1.647
batch start
#iterations: 116
currently lose_sum: 95.48338168859482
time_elpased: 1.612
batch start
#iterations: 117
currently lose_sum: 95.20355319976807
time_elpased: 1.596
batch start
#iterations: 118
currently lose_sum: 95.51276409626007
time_elpased: 1.569
batch start
#iterations: 119
currently lose_sum: 95.71282112598419
time_elpased: 1.681
start validation test
0.660567010309
0.644725894425
0.717785096748
0.67929674183
0.660472474024
61.311
batch start
#iterations: 120
currently lose_sum: 95.03757619857788
time_elpased: 1.672
batch start
#iterations: 121
currently lose_sum: 94.94819188117981
time_elpased: 1.627
batch start
#iterations: 122
currently lose_sum: 95.09215205907822
time_elpased: 1.594
batch start
#iterations: 123
currently lose_sum: 95.83357042074203
time_elpased: 1.603
batch start
#iterations: 124
currently lose_sum: 95.46960043907166
time_elpased: 1.594
batch start
#iterations: 125
currently lose_sum: 95.34499353170395
time_elpased: 1.58
batch start
#iterations: 126
currently lose_sum: 95.06623750925064
time_elpased: 1.616
batch start
#iterations: 127
currently lose_sum: 95.64384800195694
time_elpased: 1.646
batch start
#iterations: 128
currently lose_sum: 94.93146765232086
time_elpased: 1.56
batch start
#iterations: 129
currently lose_sum: 95.05148792266846
time_elpased: 1.61
batch start
#iterations: 130
currently lose_sum: 95.25788861513138
time_elpased: 1.581
batch start
#iterations: 131
currently lose_sum: 95.3606389760971
time_elpased: 1.58
batch start
#iterations: 132
currently lose_sum: 95.00995171070099
time_elpased: 1.672
batch start
#iterations: 133
currently lose_sum: 95.19458585977554
time_elpased: 1.558
batch start
#iterations: 134
currently lose_sum: 95.3601810336113
time_elpased: 1.661
batch start
#iterations: 135
currently lose_sum: 95.6920115351677
time_elpased: 1.632
batch start
#iterations: 136
currently lose_sum: 95.26649063825607
time_elpased: 1.655
batch start
#iterations: 137
currently lose_sum: 95.34460866451263
time_elpased: 1.612
batch start
#iterations: 138
currently lose_sum: 95.21453994512558
time_elpased: 1.654
batch start
#iterations: 139
currently lose_sum: 94.97324806451797
time_elpased: 1.609
start validation test
0.662268041237
0.643037974684
0.731988472622
0.684636118598
0.662152848455
60.998
batch start
#iterations: 140
currently lose_sum: 95.172043800354
time_elpased: 1.672
batch start
#iterations: 141
currently lose_sum: 95.184421479702
time_elpased: 1.657
batch start
#iterations: 142
currently lose_sum: 95.33756363391876
time_elpased: 1.575
batch start
#iterations: 143
currently lose_sum: 95.37396568059921
time_elpased: 1.616
batch start
#iterations: 144
currently lose_sum: 95.10470199584961
time_elpased: 1.577
batch start
#iterations: 145
currently lose_sum: 94.92004084587097
time_elpased: 1.633
batch start
#iterations: 146
currently lose_sum: 95.09154093265533
time_elpased: 1.581
batch start
#iterations: 147
currently lose_sum: 95.45915722846985
time_elpased: 1.656
batch start
#iterations: 148
currently lose_sum: 95.54548752307892
time_elpased: 1.655
batch start
#iterations: 149
currently lose_sum: 95.03563326597214
time_elpased: 1.608
batch start
#iterations: 150
currently lose_sum: 95.4675885438919
time_elpased: 1.689
batch start
#iterations: 151
currently lose_sum: 95.07516479492188
time_elpased: 1.66
batch start
#iterations: 152
currently lose_sum: 95.16282373666763
time_elpased: 1.661
batch start
#iterations: 153
currently lose_sum: 95.48003852367401
time_elpased: 1.654
batch start
#iterations: 154
currently lose_sum: 95.18338882923126
time_elpased: 1.629
batch start
#iterations: 155
currently lose_sum: 94.88596653938293
time_elpased: 1.712
batch start
#iterations: 156
currently lose_sum: 95.2620102763176
time_elpased: 1.597
batch start
#iterations: 157
currently lose_sum: 94.79207545518875
time_elpased: 1.618
batch start
#iterations: 158
currently lose_sum: 95.50783658027649
time_elpased: 1.627
batch start
#iterations: 159
currently lose_sum: 94.84909933805466
time_elpased: 1.685
start validation test
0.659639175258
0.625170888621
0.800123507616
0.701909620333
0.659407065663
61.044
batch start
#iterations: 160
currently lose_sum: 95.34046757221222
time_elpased: 1.654
batch start
#iterations: 161
currently lose_sum: 95.24440103769302
time_elpased: 1.644
batch start
#iterations: 162
currently lose_sum: 95.28548955917358
time_elpased: 1.612
batch start
#iterations: 163
currently lose_sum: 95.04579484462738
time_elpased: 1.616
batch start
#iterations: 164
currently lose_sum: 95.58351689577103
time_elpased: 1.563
batch start
#iterations: 165
currently lose_sum: 95.14240711927414
time_elpased: 1.686
batch start
#iterations: 166
currently lose_sum: 95.12853509187698
time_elpased: 1.609
batch start
#iterations: 167
currently lose_sum: 94.93991130590439
time_elpased: 1.626
batch start
#iterations: 168
currently lose_sum: 95.45809727907181
time_elpased: 1.678
batch start
#iterations: 169
currently lose_sum: 94.99409663677216
time_elpased: 1.601
batch start
#iterations: 170
currently lose_sum: 94.7686659693718
time_elpased: 1.662
batch start
#iterations: 171
currently lose_sum: 95.0048206448555
time_elpased: 1.613
batch start
#iterations: 172
currently lose_sum: 94.9543908238411
time_elpased: 1.627
batch start
#iterations: 173
currently lose_sum: 94.87872970104218
time_elpased: 1.764
batch start
#iterations: 174
currently lose_sum: 94.9974462389946
time_elpased: 1.603
batch start
#iterations: 175
currently lose_sum: 94.92214304208755
time_elpased: 1.601
batch start
#iterations: 176
currently lose_sum: 95.51814448833466
time_elpased: 1.588
batch start
#iterations: 177
currently lose_sum: 95.23920047283173
time_elpased: 1.587
batch start
#iterations: 178
currently lose_sum: 94.86041176319122
time_elpased: 1.597
batch start
#iterations: 179
currently lose_sum: 94.95917850732803
time_elpased: 1.598
start validation test
0.665257731959
0.639770952629
0.758954302182
0.694284907259
0.665102925564
61.039
batch start
#iterations: 180
currently lose_sum: 94.89130908250809
time_elpased: 1.672
batch start
#iterations: 181
currently lose_sum: 95.1352766752243
time_elpased: 1.633
batch start
#iterations: 182
currently lose_sum: 95.20575350522995
time_elpased: 1.694
batch start
#iterations: 183
currently lose_sum: 94.95481246709824
time_elpased: 1.633
batch start
#iterations: 184
currently lose_sum: 95.18915140628815
time_elpased: 1.635
batch start
#iterations: 185
currently lose_sum: 94.95789617300034
time_elpased: 1.651
batch start
#iterations: 186
currently lose_sum: 94.98388373851776
time_elpased: 1.604
batch start
#iterations: 187
currently lose_sum: 94.93623292446136
time_elpased: 1.599
batch start
#iterations: 188
currently lose_sum: 95.262619972229
time_elpased: 1.593
batch start
#iterations: 189
currently lose_sum: 95.00789600610733
time_elpased: 1.615
batch start
#iterations: 190
currently lose_sum: 95.02994120121002
time_elpased: 1.627
batch start
#iterations: 191
currently lose_sum: 94.90741789340973
time_elpased: 1.639
batch start
#iterations: 192
currently lose_sum: 94.84716582298279
time_elpased: 1.623
batch start
#iterations: 193
currently lose_sum: 94.9930202960968
time_elpased: 1.676
batch start
#iterations: 194
currently lose_sum: 95.21450871229172
time_elpased: 1.643
batch start
#iterations: 195
currently lose_sum: 94.71708691120148
time_elpased: 1.614
batch start
#iterations: 196
currently lose_sum: 94.6649643778801
time_elpased: 1.58
batch start
#iterations: 197
currently lose_sum: 94.82718878984451
time_elpased: 1.656
batch start
#iterations: 198
currently lose_sum: 94.62122541666031
time_elpased: 1.581
batch start
#iterations: 199
currently lose_sum: 94.93082970380783
time_elpased: 1.611
start validation test
0.66381443299
0.650831129581
0.70924248662
0.67878250591
0.663739376313
60.992
batch start
#iterations: 200
currently lose_sum: 94.96860480308533
time_elpased: 1.668
batch start
#iterations: 201
currently lose_sum: 94.78327232599258
time_elpased: 1.611
batch start
#iterations: 202
currently lose_sum: 94.95062112808228
time_elpased: 1.613
batch start
#iterations: 203
currently lose_sum: 95.27766168117523
time_elpased: 1.617
batch start
#iterations: 204
currently lose_sum: 95.51402908563614
time_elpased: 1.611
batch start
#iterations: 205
currently lose_sum: 95.09856861829758
time_elpased: 1.635
batch start
#iterations: 206
currently lose_sum: 94.77339100837708
time_elpased: 1.626
batch start
#iterations: 207
currently lose_sum: 95.28759777545929
time_elpased: 1.698
batch start
#iterations: 208
currently lose_sum: 94.77852606773376
time_elpased: 1.64
batch start
#iterations: 209
currently lose_sum: 94.73728239536285
time_elpased: 1.635
batch start
#iterations: 210
currently lose_sum: 94.63215774297714
time_elpased: 1.65
batch start
#iterations: 211
currently lose_sum: 94.41576200723648
time_elpased: 1.613
batch start
#iterations: 212
currently lose_sum: 95.25941479206085
time_elpased: 1.667
batch start
#iterations: 213
currently lose_sum: 94.72849231958389
time_elpased: 1.574
batch start
#iterations: 214
currently lose_sum: 94.72752636671066
time_elpased: 1.622
batch start
#iterations: 215
currently lose_sum: 94.78394293785095
time_elpased: 1.617
batch start
#iterations: 216
currently lose_sum: 94.52923291921616
time_elpased: 1.644
batch start
#iterations: 217
currently lose_sum: 94.83589613437653
time_elpased: 1.637
batch start
#iterations: 218
currently lose_sum: 94.99056547880173
time_elpased: 1.621
batch start
#iterations: 219
currently lose_sum: 94.77127200365067
time_elpased: 1.596
start validation test
0.667010309278
0.633991769547
0.792815973652
0.704564163542
0.666802451923
60.731
batch start
#iterations: 220
currently lose_sum: 95.04521471261978
time_elpased: 1.678
batch start
#iterations: 221
currently lose_sum: 94.89257746934891
time_elpased: 1.647
batch start
#iterations: 222
currently lose_sum: 94.53127783536911
time_elpased: 1.624
batch start
#iterations: 223
currently lose_sum: 94.4716123342514
time_elpased: 1.621
batch start
#iterations: 224
currently lose_sum: 94.59729081392288
time_elpased: 1.607
batch start
#iterations: 225
currently lose_sum: 94.46476423740387
time_elpased: 1.639
batch start
#iterations: 226
currently lose_sum: 95.02044796943665
time_elpased: 1.627
batch start
#iterations: 227
currently lose_sum: 94.81322968006134
time_elpased: 1.594
batch start
#iterations: 228
currently lose_sum: 94.70905709266663
time_elpased: 1.553
batch start
#iterations: 229
currently lose_sum: 94.61881983280182
time_elpased: 1.613
batch start
#iterations: 230
currently lose_sum: 94.9583368897438
time_elpased: 1.668
batch start
#iterations: 231
currently lose_sum: 94.90980213880539
time_elpased: 1.618
batch start
#iterations: 232
currently lose_sum: 95.09648060798645
time_elpased: 1.544
batch start
#iterations: 233
currently lose_sum: 95.06166404485703
time_elpased: 1.587
batch start
#iterations: 234
currently lose_sum: 94.75256371498108
time_elpased: 1.628
batch start
#iterations: 235
currently lose_sum: 95.0742399096489
time_elpased: 1.569
batch start
#iterations: 236
currently lose_sum: 94.8310478925705
time_elpased: 1.625
batch start
#iterations: 237
currently lose_sum: 94.7321891784668
time_elpased: 1.641
batch start
#iterations: 238
currently lose_sum: 94.94893980026245
time_elpased: 1.627
batch start
#iterations: 239
currently lose_sum: 94.71931278705597
time_elpased: 1.625
start validation test
0.670773195876
0.645307725884
0.760806916427
0.698313731047
0.670624441278
60.603
batch start
#iterations: 240
currently lose_sum: 94.94776701927185
time_elpased: 1.612
batch start
#iterations: 241
currently lose_sum: 94.75668978691101
time_elpased: 1.661
batch start
#iterations: 242
currently lose_sum: 94.78001058101654
time_elpased: 1.652
batch start
#iterations: 243
currently lose_sum: 94.55837225914001
time_elpased: 1.576
batch start
#iterations: 244
currently lose_sum: 95.0132674574852
time_elpased: 1.648
batch start
#iterations: 245
currently lose_sum: 95.14223802089691
time_elpased: 1.608
batch start
#iterations: 246
currently lose_sum: 94.41776567697525
time_elpased: 1.616
batch start
#iterations: 247
currently lose_sum: 94.92399853467941
time_elpased: 1.64
batch start
#iterations: 248
currently lose_sum: 94.77590429782867
time_elpased: 1.687
batch start
#iterations: 249
currently lose_sum: 95.02983218431473
time_elpased: 1.672
batch start
#iterations: 250
currently lose_sum: 94.8181980252266
time_elpased: 1.64
batch start
#iterations: 251
currently lose_sum: 95.09002858400345
time_elpased: 1.611
batch start
#iterations: 252
currently lose_sum: 94.72724539041519
time_elpased: 1.615
batch start
#iterations: 253
currently lose_sum: 94.62243187427521
time_elpased: 1.591
batch start
#iterations: 254
currently lose_sum: 94.82173907756805
time_elpased: 1.638
batch start
#iterations: 255
currently lose_sum: 95.2566642165184
time_elpased: 1.609
batch start
#iterations: 256
currently lose_sum: 94.07947117090225
time_elpased: 1.642
batch start
#iterations: 257
currently lose_sum: 94.62022709846497
time_elpased: 1.609
batch start
#iterations: 258
currently lose_sum: 94.7189404964447
time_elpased: 1.613
batch start
#iterations: 259
currently lose_sum: 94.71921998262405
time_elpased: 1.598
start validation test
0.662628865979
0.643549117248
0.731576780568
0.684745436154
0.662514949557
60.826
batch start
#iterations: 260
currently lose_sum: 94.48311948776245
time_elpased: 1.596
batch start
#iterations: 261
currently lose_sum: 94.64579617977142
time_elpased: 1.654
batch start
#iterations: 262
currently lose_sum: 94.50902998447418
time_elpased: 1.74
batch start
#iterations: 263
currently lose_sum: 94.65862327814102
time_elpased: 1.677
batch start
#iterations: 264
currently lose_sum: 94.79960888624191
time_elpased: 1.624
batch start
#iterations: 265
currently lose_sum: 94.57754880189896
time_elpased: 1.667
batch start
#iterations: 266
currently lose_sum: 94.57254993915558
time_elpased: 1.638
batch start
#iterations: 267
currently lose_sum: 94.84010654687881
time_elpased: 1.624
batch start
#iterations: 268
currently lose_sum: 94.36186963319778
time_elpased: 1.682
batch start
#iterations: 269
currently lose_sum: 94.96273481845856
time_elpased: 1.611
batch start
#iterations: 270
currently lose_sum: 94.54604756832123
time_elpased: 1.605
batch start
#iterations: 271
currently lose_sum: 94.56474739313126
time_elpased: 1.645
batch start
#iterations: 272
currently lose_sum: 94.70524597167969
time_elpased: 1.648
batch start
#iterations: 273
currently lose_sum: 94.6120982170105
time_elpased: 1.618
batch start
#iterations: 274
currently lose_sum: 94.68633425235748
time_elpased: 1.645
batch start
#iterations: 275
currently lose_sum: 94.68772798776627
time_elpased: 1.653
batch start
#iterations: 276
currently lose_sum: 94.61310744285583
time_elpased: 1.644
batch start
#iterations: 277
currently lose_sum: 94.19604581594467
time_elpased: 1.648
batch start
#iterations: 278
currently lose_sum: 94.86030036211014
time_elpased: 1.659
batch start
#iterations: 279
currently lose_sum: 94.97084838151932
time_elpased: 1.63
start validation test
0.665670103093
0.631194151097
0.799711815562
0.705529828385
0.665448638058
60.702
batch start
#iterations: 280
currently lose_sum: 94.53090262413025
time_elpased: 1.616
batch start
#iterations: 281
currently lose_sum: 94.37039506435394
time_elpased: 1.674
batch start
#iterations: 282
currently lose_sum: 94.92462265491486
time_elpased: 1.624
batch start
#iterations: 283
currently lose_sum: 94.57781946659088
time_elpased: 1.651
batch start
#iterations: 284
currently lose_sum: 94.11522084474564
time_elpased: 1.645
batch start
#iterations: 285
currently lose_sum: 94.27724015712738
time_elpased: 1.652
batch start
#iterations: 286
currently lose_sum: 94.55871856212616
time_elpased: 1.656
batch start
#iterations: 287
currently lose_sum: 94.66783148050308
time_elpased: 1.602
batch start
#iterations: 288
currently lose_sum: 94.51823669672012
time_elpased: 1.657
batch start
#iterations: 289
currently lose_sum: 94.54436123371124
time_elpased: 1.598
batch start
#iterations: 290
currently lose_sum: 94.6357330083847
time_elpased: 1.691
batch start
#iterations: 291
currently lose_sum: 94.87754881381989
time_elpased: 1.694
batch start
#iterations: 292
currently lose_sum: 94.3223340511322
time_elpased: 1.619
batch start
#iterations: 293
currently lose_sum: 94.51320070028305
time_elpased: 1.626
batch start
#iterations: 294
currently lose_sum: 94.78083896636963
time_elpased: 1.646
batch start
#iterations: 295
currently lose_sum: 94.83102887868881
time_elpased: 1.622
batch start
#iterations: 296
currently lose_sum: 94.73992174863815
time_elpased: 1.687
batch start
#iterations: 297
currently lose_sum: 94.46818435192108
time_elpased: 1.648
batch start
#iterations: 298
currently lose_sum: 94.67470294237137
time_elpased: 1.6
batch start
#iterations: 299
currently lose_sum: 94.7768006324768
time_elpased: 1.691
start validation test
0.660567010309
0.64140547376
0.730856319473
0.683215471208
0.660450877622
60.740
batch start
#iterations: 300
currently lose_sum: 94.72198164463043
time_elpased: 1.623
batch start
#iterations: 301
currently lose_sum: 94.38741546869278
time_elpased: 1.621
batch start
#iterations: 302
currently lose_sum: 94.30039149522781
time_elpased: 1.637
batch start
#iterations: 303
currently lose_sum: 94.09910678863525
time_elpased: 1.64
batch start
#iterations: 304
currently lose_sum: 93.93934273719788
time_elpased: 1.596
batch start
#iterations: 305
currently lose_sum: 94.48597526550293
time_elpased: 1.623
batch start
#iterations: 306
currently lose_sum: 94.88011837005615
time_elpased: 1.613
batch start
#iterations: 307
currently lose_sum: 94.56599724292755
time_elpased: 1.583
batch start
#iterations: 308
currently lose_sum: 94.4757468700409
time_elpased: 1.583
batch start
#iterations: 309
currently lose_sum: 94.254938185215
time_elpased: 1.643
batch start
#iterations: 310
currently lose_sum: 94.30391412973404
time_elpased: 1.632
batch start
#iterations: 311
currently lose_sum: 94.60800766944885
time_elpased: 1.679
batch start
#iterations: 312
currently lose_sum: 94.78008735179901
time_elpased: 1.613
batch start
#iterations: 313
currently lose_sum: 94.51493114233017
time_elpased: 1.585
batch start
#iterations: 314
currently lose_sum: 94.30207711458206
time_elpased: 1.595
batch start
#iterations: 315
currently lose_sum: 94.05587285757065
time_elpased: 1.694
batch start
#iterations: 316
currently lose_sum: 94.14275449514389
time_elpased: 1.651
batch start
#iterations: 317
currently lose_sum: 94.67087870836258
time_elpased: 1.582
batch start
#iterations: 318
currently lose_sum: 94.20392173528671
time_elpased: 1.585
batch start
#iterations: 319
currently lose_sum: 94.55412524938583
time_elpased: 1.577
start validation test
0.669072164948
0.639519133085
0.777480444627
0.701783723523
0.668893051723
60.556
batch start
#iterations: 320
currently lose_sum: 94.76442712545395
time_elpased: 1.655
batch start
#iterations: 321
currently lose_sum: 94.38579124212265
time_elpased: 1.611
batch start
#iterations: 322
currently lose_sum: 94.53604805469513
time_elpased: 1.65
batch start
#iterations: 323
currently lose_sum: 94.71245157718658
time_elpased: 1.635
batch start
#iterations: 324
currently lose_sum: 94.16025239229202
time_elpased: 1.61
batch start
#iterations: 325
currently lose_sum: 94.44999349117279
time_elpased: 1.646
batch start
#iterations: 326
currently lose_sum: 94.27688831090927
time_elpased: 1.633
batch start
#iterations: 327
currently lose_sum: 94.37488877773285
time_elpased: 1.618
batch start
#iterations: 328
currently lose_sum: 94.77164274454117
time_elpased: 1.604
batch start
#iterations: 329
currently lose_sum: 94.6310704946518
time_elpased: 1.622
batch start
#iterations: 330
currently lose_sum: 94.7100795507431
time_elpased: 1.619
batch start
#iterations: 331
currently lose_sum: 94.4250910282135
time_elpased: 1.739
batch start
#iterations: 332
currently lose_sum: 94.26408940553665
time_elpased: 1.654
batch start
#iterations: 333
currently lose_sum: 94.85020184516907
time_elpased: 1.65
batch start
#iterations: 334
currently lose_sum: 94.36375838518143
time_elpased: 1.555
batch start
#iterations: 335
currently lose_sum: 94.20397579669952
time_elpased: 1.633
batch start
#iterations: 336
currently lose_sum: 94.43552780151367
time_elpased: 1.683
batch start
#iterations: 337
currently lose_sum: 94.37913864850998
time_elpased: 1.624
batch start
#iterations: 338
currently lose_sum: 94.26826548576355
time_elpased: 1.656
batch start
#iterations: 339
currently lose_sum: 94.68661940097809
time_elpased: 1.668
start validation test
0.666082474227
0.636004704301
0.779230135858
0.700370027752
0.665895530548
60.633
batch start
#iterations: 340
currently lose_sum: 94.78486657142639
time_elpased: 1.629
batch start
#iterations: 341
currently lose_sum: 94.39638447761536
time_elpased: 1.758
batch start
#iterations: 342
currently lose_sum: 94.72164523601532
time_elpased: 1.633
batch start
#iterations: 343
currently lose_sum: 94.97037154436111
time_elpased: 1.597
batch start
#iterations: 344
currently lose_sum: 94.54663383960724
time_elpased: 1.536
batch start
#iterations: 345
currently lose_sum: 94.12374484539032
time_elpased: 1.652
batch start
#iterations: 346
currently lose_sum: 94.56807404756546
time_elpased: 1.558
batch start
#iterations: 347
currently lose_sum: 94.74201971292496
time_elpased: 1.625
batch start
#iterations: 348
currently lose_sum: 94.51795798540115
time_elpased: 1.674
batch start
#iterations: 349
currently lose_sum: 94.19307845830917
time_elpased: 1.667
batch start
#iterations: 350
currently lose_sum: 94.63781309127808
time_elpased: 1.634
batch start
#iterations: 351
currently lose_sum: 94.73299258947372
time_elpased: 1.603
batch start
#iterations: 352
currently lose_sum: 94.78050684928894
time_elpased: 1.625
batch start
#iterations: 353
currently lose_sum: 94.55300348997116
time_elpased: 1.665
batch start
#iterations: 354
currently lose_sum: 94.34666013717651
time_elpased: 1.616
batch start
#iterations: 355
currently lose_sum: 94.1530659198761
time_elpased: 1.629
batch start
#iterations: 356
currently lose_sum: 94.60014176368713
time_elpased: 1.729
batch start
#iterations: 357
currently lose_sum: 94.23601961135864
time_elpased: 1.589
batch start
#iterations: 358
currently lose_sum: 94.41095793247223
time_elpased: 1.577
batch start
#iterations: 359
currently lose_sum: 94.24209451675415
time_elpased: 1.678
start validation test
0.66912371134
0.650177644165
0.734561547962
0.689798482579
0.669015594303
60.650
batch start
#iterations: 360
currently lose_sum: 94.54885619878769
time_elpased: 1.642
batch start
#iterations: 361
currently lose_sum: 94.42119812965393
time_elpased: 1.613
batch start
#iterations: 362
currently lose_sum: 94.716612637043
time_elpased: 1.623
batch start
#iterations: 363
currently lose_sum: 94.50000923871994
time_elpased: 1.59
batch start
#iterations: 364
currently lose_sum: 94.32483386993408
time_elpased: 1.641
batch start
#iterations: 365
currently lose_sum: 94.73792499303818
time_elpased: 1.621
batch start
#iterations: 366
currently lose_sum: 94.92950117588043
time_elpased: 1.659
batch start
#iterations: 367
currently lose_sum: 94.34073215723038
time_elpased: 1.64
batch start
#iterations: 368
currently lose_sum: 94.66642016172409
time_elpased: 1.59
batch start
#iterations: 369
currently lose_sum: 94.40141260623932
time_elpased: 1.646
batch start
#iterations: 370
currently lose_sum: 94.49576812982559
time_elpased: 1.638
batch start
#iterations: 371
currently lose_sum: 94.51848381757736
time_elpased: 1.661
batch start
#iterations: 372
currently lose_sum: 94.61078751087189
time_elpased: 1.659
batch start
#iterations: 373
currently lose_sum: 94.11590820550919
time_elpased: 1.628
batch start
#iterations: 374
currently lose_sum: 94.53388565778732
time_elpased: 1.65
batch start
#iterations: 375
currently lose_sum: 94.40560847520828
time_elpased: 1.59
batch start
#iterations: 376
currently lose_sum: 94.2502366900444
time_elpased: 1.64
batch start
#iterations: 377
currently lose_sum: 94.26207715272903
time_elpased: 1.634
batch start
#iterations: 378
currently lose_sum: 94.32065039873123
time_elpased: 1.633
batch start
#iterations: 379
currently lose_sum: 94.80383813381195
time_elpased: 1.632
start validation test
0.667835051546
0.640453296703
0.76780568135
0.698371091556
0.667669879089
60.669
batch start
#iterations: 380
currently lose_sum: 94.20263934135437
time_elpased: 1.625
batch start
#iterations: 381
currently lose_sum: 94.07907009124756
time_elpased: 1.674
batch start
#iterations: 382
currently lose_sum: 94.51897275447845
time_elpased: 1.56
batch start
#iterations: 383
currently lose_sum: 94.21125370264053
time_elpased: 1.64
batch start
#iterations: 384
currently lose_sum: 94.39763730764389
time_elpased: 1.654
batch start
#iterations: 385
currently lose_sum: 94.56991600990295
time_elpased: 1.613
batch start
#iterations: 386
currently lose_sum: 94.19260340929031
time_elpased: 1.629
batch start
#iterations: 387
currently lose_sum: 94.15520519018173
time_elpased: 1.59
batch start
#iterations: 388
currently lose_sum: 94.66364175081253
time_elpased: 1.639
batch start
#iterations: 389
currently lose_sum: 94.0241864323616
time_elpased: 1.608
batch start
#iterations: 390
currently lose_sum: 94.27797716856003
time_elpased: 1.692
batch start
#iterations: 391
currently lose_sum: 94.07989704608917
time_elpased: 1.588
batch start
#iterations: 392
currently lose_sum: 94.07262647151947
time_elpased: 1.644
batch start
#iterations: 393
currently lose_sum: 94.17555052042007
time_elpased: 1.679
batch start
#iterations: 394
currently lose_sum: 94.43804341554642
time_elpased: 1.651
batch start
#iterations: 395
currently lose_sum: 94.31268566846848
time_elpased: 1.58
batch start
#iterations: 396
currently lose_sum: 94.1748588681221
time_elpased: 1.647
batch start
#iterations: 397
currently lose_sum: 94.51470190286636
time_elpased: 1.643
batch start
#iterations: 398
currently lose_sum: 94.45818203687668
time_elpased: 1.664
batch start
#iterations: 399
currently lose_sum: 94.21466553211212
time_elpased: 1.608
start validation test
0.668608247423
0.642875771538
0.761115685467
0.697016824544
0.668455405724
60.667
acc: 0.676
pre: 0.646
rec: 0.782
F1: 0.707
auc: 0.718
