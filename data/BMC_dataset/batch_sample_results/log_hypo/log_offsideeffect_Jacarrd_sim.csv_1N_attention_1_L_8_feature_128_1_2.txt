start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.27170538902283
time_elpased: 1.78
batch start
#iterations: 1
currently lose_sum: 99.85147082805634
time_elpased: 1.771
batch start
#iterations: 2
currently lose_sum: 99.4106216430664
time_elpased: 1.72
batch start
#iterations: 3
currently lose_sum: 99.17841976881027
time_elpased: 1.767
batch start
#iterations: 4
currently lose_sum: 98.83488827943802
time_elpased: 1.767
batch start
#iterations: 5
currently lose_sum: 98.74377459287643
time_elpased: 1.752
batch start
#iterations: 6
currently lose_sum: 98.46124738454819
time_elpased: 1.784
batch start
#iterations: 7
currently lose_sum: 98.48139023780823
time_elpased: 1.77
batch start
#iterations: 8
currently lose_sum: 98.36284774541855
time_elpased: 1.764
batch start
#iterations: 9
currently lose_sum: 98.29679918289185
time_elpased: 1.792
batch start
#iterations: 10
currently lose_sum: 98.10514909029007
time_elpased: 1.798
batch start
#iterations: 11
currently lose_sum: 98.00266695022583
time_elpased: 1.78
batch start
#iterations: 12
currently lose_sum: 97.65500313043594
time_elpased: 1.753
batch start
#iterations: 13
currently lose_sum: 97.62044167518616
time_elpased: 1.754
batch start
#iterations: 14
currently lose_sum: 97.56559371948242
time_elpased: 1.743
batch start
#iterations: 15
currently lose_sum: 97.2857186794281
time_elpased: 1.713
batch start
#iterations: 16
currently lose_sum: 97.17488294839859
time_elpased: 1.777
batch start
#iterations: 17
currently lose_sum: 97.1361557841301
time_elpased: 1.754
batch start
#iterations: 18
currently lose_sum: 97.27527886629105
time_elpased: 1.728
batch start
#iterations: 19
currently lose_sum: 96.92770552635193
time_elpased: 1.782
start validation test
0.631804123711
0.654538904899
0.560975609756
0.604156275977
0.631928474092
62.988
batch start
#iterations: 20
currently lose_sum: 96.81414359807968
time_elpased: 1.794
batch start
#iterations: 21
currently lose_sum: 96.57968491315842
time_elpased: 1.756
batch start
#iterations: 22
currently lose_sum: 96.95244914293289
time_elpased: 1.736
batch start
#iterations: 23
currently lose_sum: 96.75391745567322
time_elpased: 1.776
batch start
#iterations: 24
currently lose_sum: 96.7022653222084
time_elpased: 1.762
batch start
#iterations: 25
currently lose_sum: 96.76273912191391
time_elpased: 1.809
batch start
#iterations: 26
currently lose_sum: 96.6024786233902
time_elpased: 1.76
batch start
#iterations: 27
currently lose_sum: 96.82697278261185
time_elpased: 1.791
batch start
#iterations: 28
currently lose_sum: 96.57980728149414
time_elpased: 1.748
batch start
#iterations: 29
currently lose_sum: 96.59526407718658
time_elpased: 1.781
batch start
#iterations: 30
currently lose_sum: 96.32753533124924
time_elpased: 1.721
batch start
#iterations: 31
currently lose_sum: 96.41269421577454
time_elpased: 1.779
batch start
#iterations: 32
currently lose_sum: 96.16613334417343
time_elpased: 1.782
batch start
#iterations: 33
currently lose_sum: 96.80249655246735
time_elpased: 1.737
batch start
#iterations: 34
currently lose_sum: 96.54643958806992
time_elpased: 1.745
batch start
#iterations: 35
currently lose_sum: 96.16181606054306
time_elpased: 1.764
batch start
#iterations: 36
currently lose_sum: 95.97471606731415
time_elpased: 1.711
batch start
#iterations: 37
currently lose_sum: 96.21310895681381
time_elpased: 1.789
batch start
#iterations: 38
currently lose_sum: 95.94534134864807
time_elpased: 1.744
batch start
#iterations: 39
currently lose_sum: 96.05988466739655
time_elpased: 1.752
start validation test
0.654845360825
0.625613305613
0.774210147165
0.692024652746
0.654635797532
61.655
batch start
#iterations: 40
currently lose_sum: 96.02305966615677
time_elpased: 1.736
batch start
#iterations: 41
currently lose_sum: 96.1989398598671
time_elpased: 1.751
batch start
#iterations: 42
currently lose_sum: 95.86378425359726
time_elpased: 1.767
batch start
#iterations: 43
currently lose_sum: 96.08259922266006
time_elpased: 1.729
batch start
#iterations: 44
currently lose_sum: 96.31235033273697
time_elpased: 1.806
batch start
#iterations: 45
currently lose_sum: 96.13068652153015
time_elpased: 1.755
batch start
#iterations: 46
currently lose_sum: 96.22709953784943
time_elpased: 1.812
batch start
#iterations: 47
currently lose_sum: 96.1091223359108
time_elpased: 1.771
batch start
#iterations: 48
currently lose_sum: 96.02799963951111
time_elpased: 1.764
batch start
#iterations: 49
currently lose_sum: 96.06282418966293
time_elpased: 1.803
batch start
#iterations: 50
currently lose_sum: 95.79886275529861
time_elpased: 1.816
batch start
#iterations: 51
currently lose_sum: 96.1090716123581
time_elpased: 1.766
batch start
#iterations: 52
currently lose_sum: 95.98263120651245
time_elpased: 1.758
batch start
#iterations: 53
currently lose_sum: 95.76699250936508
time_elpased: 1.77
batch start
#iterations: 54
currently lose_sum: 95.93747168779373
time_elpased: 1.756
batch start
#iterations: 55
currently lose_sum: 96.06049579381943
time_elpased: 1.742
batch start
#iterations: 56
currently lose_sum: 96.01312041282654
time_elpased: 1.808
batch start
#iterations: 57
currently lose_sum: 95.91476345062256
time_elpased: 1.777
batch start
#iterations: 58
currently lose_sum: 96.2873352766037
time_elpased: 1.738
batch start
#iterations: 59
currently lose_sum: 95.9435430765152
time_elpased: 1.755
start validation test
0.646855670103
0.669706300332
0.581969743748
0.622763063708
0.646969587355
61.813
batch start
#iterations: 60
currently lose_sum: 95.92023348808289
time_elpased: 1.746
batch start
#iterations: 61
currently lose_sum: 96.00485825538635
time_elpased: 1.72
batch start
#iterations: 62
currently lose_sum: 96.17787289619446
time_elpased: 1.762
batch start
#iterations: 63
currently lose_sum: 95.42596763372421
time_elpased: 1.785
batch start
#iterations: 64
currently lose_sum: 95.61633378267288
time_elpased: 1.792
batch start
#iterations: 65
currently lose_sum: 96.06255751848221
time_elpased: 1.81
batch start
#iterations: 66
currently lose_sum: 95.98145031929016
time_elpased: 1.742
batch start
#iterations: 67
currently lose_sum: 95.53584390878677
time_elpased: 1.731
batch start
#iterations: 68
currently lose_sum: 95.54269421100616
time_elpased: 1.748
batch start
#iterations: 69
currently lose_sum: 95.74437767267227
time_elpased: 1.733
batch start
#iterations: 70
currently lose_sum: 95.6294857263565
time_elpased: 1.769
batch start
#iterations: 71
currently lose_sum: 95.64490640163422
time_elpased: 1.799
batch start
#iterations: 72
currently lose_sum: 95.69113433361053
time_elpased: 1.762
batch start
#iterations: 73
currently lose_sum: 95.82960683107376
time_elpased: 1.812
batch start
#iterations: 74
currently lose_sum: 96.12074381113052
time_elpased: 1.756
batch start
#iterations: 75
currently lose_sum: 95.69462352991104
time_elpased: 1.766
batch start
#iterations: 76
currently lose_sum: 95.65905845165253
time_elpased: 1.796
batch start
#iterations: 77
currently lose_sum: 95.6628287434578
time_elpased: 1.752
batch start
#iterations: 78
currently lose_sum: 95.84012639522552
time_elpased: 1.774
batch start
#iterations: 79
currently lose_sum: 96.3089429140091
time_elpased: 1.778
start validation test
0.661288659794
0.622718052738
0.821446948647
0.708409141336
0.661007477215
61.269
batch start
#iterations: 80
currently lose_sum: 95.8973519206047
time_elpased: 1.745
batch start
#iterations: 81
currently lose_sum: 95.47644186019897
time_elpased: 1.744
batch start
#iterations: 82
currently lose_sum: 95.7737438082695
time_elpased: 1.738
batch start
#iterations: 83
currently lose_sum: 95.52690654993057
time_elpased: 1.744
batch start
#iterations: 84
currently lose_sum: 95.55275774002075
time_elpased: 1.753
batch start
#iterations: 85
currently lose_sum: 95.69995367527008
time_elpased: 1.751
batch start
#iterations: 86
currently lose_sum: 95.56907445192337
time_elpased: 1.773
batch start
#iterations: 87
currently lose_sum: 96.20869785547256
time_elpased: 1.761
batch start
#iterations: 88
currently lose_sum: 95.51777678728104
time_elpased: 1.779
batch start
#iterations: 89
currently lose_sum: 95.74822950363159
time_elpased: 1.751
batch start
#iterations: 90
currently lose_sum: 95.73262351751328
time_elpased: 1.749
batch start
#iterations: 91
currently lose_sum: 95.70362389087677
time_elpased: 1.774
batch start
#iterations: 92
currently lose_sum: 95.6573725938797
time_elpased: 1.776
batch start
#iterations: 93
currently lose_sum: 95.45950144529343
time_elpased: 1.764
batch start
#iterations: 94
currently lose_sum: 95.30118852853775
time_elpased: 1.755
batch start
#iterations: 95
currently lose_sum: 95.38405376672745
time_elpased: 1.764
batch start
#iterations: 96
currently lose_sum: 95.32996708154678
time_elpased: 1.753
batch start
#iterations: 97
currently lose_sum: 95.44918608665466
time_elpased: 1.776
batch start
#iterations: 98
currently lose_sum: 95.89848780632019
time_elpased: 1.761
batch start
#iterations: 99
currently lose_sum: 95.57645285129547
time_elpased: 1.781
start validation test
0.665206185567
0.638640275387
0.763713080169
0.695599193889
0.66503324152
60.536
batch start
#iterations: 100
currently lose_sum: 95.50202786922455
time_elpased: 1.755
batch start
#iterations: 101
currently lose_sum: 95.32273072004318
time_elpased: 1.77
batch start
#iterations: 102
currently lose_sum: 95.58729803562164
time_elpased: 1.768
batch start
#iterations: 103
currently lose_sum: 95.33182066679001
time_elpased: 1.738
batch start
#iterations: 104
currently lose_sum: 95.59802955389023
time_elpased: 1.807
batch start
#iterations: 105
currently lose_sum: 95.38176310062408
time_elpased: 1.754
batch start
#iterations: 106
currently lose_sum: 95.5880132317543
time_elpased: 1.744
batch start
#iterations: 107
currently lose_sum: 95.77313321828842
time_elpased: 1.768
batch start
#iterations: 108
currently lose_sum: 95.21275240182877
time_elpased: 1.783
batch start
#iterations: 109
currently lose_sum: 95.40763926506042
time_elpased: 1.755
batch start
#iterations: 110
currently lose_sum: 95.49252015352249
time_elpased: 1.814
batch start
#iterations: 111
currently lose_sum: 95.00858986377716
time_elpased: 1.757
batch start
#iterations: 112
currently lose_sum: 95.2913578748703
time_elpased: 1.733
batch start
#iterations: 113
currently lose_sum: 95.15040159225464
time_elpased: 1.777
batch start
#iterations: 114
currently lose_sum: 95.03907412290573
time_elpased: 1.778
batch start
#iterations: 115
currently lose_sum: 95.49806427955627
time_elpased: 1.75
batch start
#iterations: 116
currently lose_sum: 95.10465234518051
time_elpased: 1.759
batch start
#iterations: 117
currently lose_sum: 95.14111298322678
time_elpased: 1.741
batch start
#iterations: 118
currently lose_sum: 94.94401955604553
time_elpased: 1.78
batch start
#iterations: 119
currently lose_sum: 95.24004739522934
time_elpased: 1.767
start validation test
0.665206185567
0.627010406812
0.818462488422
0.710057586715
0.664937120489
60.675
batch start
#iterations: 120
currently lose_sum: 95.08344799280167
time_elpased: 1.729
batch start
#iterations: 121
currently lose_sum: 95.52273803949356
time_elpased: 1.784
batch start
#iterations: 122
currently lose_sum: 95.37011581659317
time_elpased: 1.729
batch start
#iterations: 123
currently lose_sum: 95.49385988712311
time_elpased: 1.755
batch start
#iterations: 124
currently lose_sum: 95.44620156288147
time_elpased: 1.764
batch start
#iterations: 125
currently lose_sum: 95.56395024061203
time_elpased: 1.731
batch start
#iterations: 126
currently lose_sum: 95.40032345056534
time_elpased: 1.768
batch start
#iterations: 127
currently lose_sum: 95.51242339611053
time_elpased: 1.775
batch start
#iterations: 128
currently lose_sum: 95.08446669578552
time_elpased: 1.811
batch start
#iterations: 129
currently lose_sum: 95.84287190437317
time_elpased: 1.732
batch start
#iterations: 130
currently lose_sum: 95.24812746047974
time_elpased: 1.784
batch start
#iterations: 131
currently lose_sum: 95.4551454782486
time_elpased: 1.774
batch start
#iterations: 132
currently lose_sum: 95.02524596452713
time_elpased: 1.761
batch start
#iterations: 133
currently lose_sum: 95.18202126026154
time_elpased: 1.779
batch start
#iterations: 134
currently lose_sum: 95.23186248540878
time_elpased: 1.763
batch start
#iterations: 135
currently lose_sum: 95.08132749795914
time_elpased: 1.759
batch start
#iterations: 136
currently lose_sum: 95.19306343793869
time_elpased: 1.784
batch start
#iterations: 137
currently lose_sum: 95.15469706058502
time_elpased: 1.729
batch start
#iterations: 138
currently lose_sum: 95.64951479434967
time_elpased: 1.729
batch start
#iterations: 139
currently lose_sum: 95.17056185007095
time_elpased: 1.8
start validation test
0.668505154639
0.639901226158
0.773386847793
0.700340151903
0.66832101865
60.672
batch start
#iterations: 140
currently lose_sum: 94.71369051933289
time_elpased: 1.756
batch start
#iterations: 141
currently lose_sum: 95.43468183279037
time_elpased: 1.752
batch start
#iterations: 142
currently lose_sum: 95.22998428344727
time_elpased: 1.743
batch start
#iterations: 143
currently lose_sum: 95.34088295698166
time_elpased: 1.786
batch start
#iterations: 144
currently lose_sum: 95.18617796897888
time_elpased: 1.796
batch start
#iterations: 145
currently lose_sum: 95.29866874217987
time_elpased: 1.752
batch start
#iterations: 146
currently lose_sum: 95.27619707584381
time_elpased: 1.755
batch start
#iterations: 147
currently lose_sum: 95.34248369932175
time_elpased: 1.761
batch start
#iterations: 148
currently lose_sum: 95.36094641685486
time_elpased: 1.779
batch start
#iterations: 149
currently lose_sum: 95.16645669937134
time_elpased: 1.756
batch start
#iterations: 150
currently lose_sum: 95.27482283115387
time_elpased: 1.785
batch start
#iterations: 151
currently lose_sum: 95.24103182554245
time_elpased: 1.778
batch start
#iterations: 152
currently lose_sum: 95.11114937067032
time_elpased: 1.749
batch start
#iterations: 153
currently lose_sum: 95.35995906591415
time_elpased: 1.728
batch start
#iterations: 154
currently lose_sum: 95.13705998659134
time_elpased: 1.742
batch start
#iterations: 155
currently lose_sum: 95.06109929084778
time_elpased: 1.768
batch start
#iterations: 156
currently lose_sum: 94.99689495563507
time_elpased: 1.747
batch start
#iterations: 157
currently lose_sum: 94.92733401060104
time_elpased: 1.783
batch start
#iterations: 158
currently lose_sum: 95.00555139780045
time_elpased: 1.752
batch start
#iterations: 159
currently lose_sum: 95.06904625892639
time_elpased: 1.77
start validation test
0.662422680412
0.664862614488
0.657404548729
0.661112548512
0.662431490517
61.249
batch start
#iterations: 160
currently lose_sum: 95.14546948671341
time_elpased: 1.758
batch start
#iterations: 161
currently lose_sum: 95.5258966088295
time_elpased: 1.783
batch start
#iterations: 162
currently lose_sum: 94.99739897251129
time_elpased: 1.784
batch start
#iterations: 163
currently lose_sum: 95.47240328788757
time_elpased: 1.785
batch start
#iterations: 164
currently lose_sum: 95.22521489858627
time_elpased: 1.745
batch start
#iterations: 165
currently lose_sum: 95.21816807985306
time_elpased: 1.776
batch start
#iterations: 166
currently lose_sum: 95.11144280433655
time_elpased: 1.784
batch start
#iterations: 167
currently lose_sum: 95.14670699834824
time_elpased: 1.773
batch start
#iterations: 168
currently lose_sum: 95.11654329299927
time_elpased: 1.759
batch start
#iterations: 169
currently lose_sum: 95.03046005964279
time_elpased: 1.739
batch start
#iterations: 170
currently lose_sum: 95.33233284950256
time_elpased: 1.789
batch start
#iterations: 171
currently lose_sum: 95.23251730203629
time_elpased: 1.748
batch start
#iterations: 172
currently lose_sum: 95.39611154794693
time_elpased: 1.798
batch start
#iterations: 173
currently lose_sum: 95.20741999149323
time_elpased: 1.828
batch start
#iterations: 174
currently lose_sum: 94.71171522140503
time_elpased: 1.788
batch start
#iterations: 175
currently lose_sum: 94.97841912508011
time_elpased: 1.782
batch start
#iterations: 176
currently lose_sum: 94.54917234182358
time_elpased: 1.759
batch start
#iterations: 177
currently lose_sum: 94.90726965665817
time_elpased: 1.771
batch start
#iterations: 178
currently lose_sum: 95.23470586538315
time_elpased: 1.765
batch start
#iterations: 179
currently lose_sum: 95.07482349872589
time_elpased: 1.786
start validation test
0.661701030928
0.656448412698
0.680971493259
0.668485124009
0.661667198659
60.575
batch start
#iterations: 180
currently lose_sum: 94.7853615283966
time_elpased: 1.751
batch start
#iterations: 181
currently lose_sum: 94.75787991285324
time_elpased: 1.755
batch start
#iterations: 182
currently lose_sum: 95.09599786996841
time_elpased: 1.73
batch start
#iterations: 183
currently lose_sum: 94.24140352010727
time_elpased: 1.797
batch start
#iterations: 184
currently lose_sum: 95.35184854269028
time_elpased: 1.811
batch start
#iterations: 185
currently lose_sum: 95.20403254032135
time_elpased: 1.751
batch start
#iterations: 186
currently lose_sum: 95.52798765897751
time_elpased: 1.765
batch start
#iterations: 187
currently lose_sum: 95.06973791122437
time_elpased: 1.761
batch start
#iterations: 188
currently lose_sum: 95.3077746629715
time_elpased: 1.742
batch start
#iterations: 189
currently lose_sum: 94.83130204677582
time_elpased: 1.83
batch start
#iterations: 190
currently lose_sum: 95.2219786643982
time_elpased: 1.74
batch start
#iterations: 191
currently lose_sum: 94.90554428100586
time_elpased: 1.771
batch start
#iterations: 192
currently lose_sum: 94.71199148893356
time_elpased: 1.779
batch start
#iterations: 193
currently lose_sum: 95.07594084739685
time_elpased: 1.794
batch start
#iterations: 194
currently lose_sum: 94.78611361980438
time_elpased: 1.731
batch start
#iterations: 195
currently lose_sum: 95.06111824512482
time_elpased: 1.804
batch start
#iterations: 196
currently lose_sum: 94.90992891788483
time_elpased: 1.756
batch start
#iterations: 197
currently lose_sum: 95.1075907945633
time_elpased: 1.753
batch start
#iterations: 198
currently lose_sum: 94.84670090675354
time_elpased: 1.741
batch start
#iterations: 199
currently lose_sum: 94.79631334543228
time_elpased: 1.736
start validation test
0.666391752577
0.635649193211
0.782443140887
0.701448473106
0.666188006465
60.424
batch start
#iterations: 200
currently lose_sum: 95.06785625219345
time_elpased: 1.737
batch start
#iterations: 201
currently lose_sum: 95.1374762058258
time_elpased: 1.769
batch start
#iterations: 202
currently lose_sum: 95.03929990530014
time_elpased: 1.776
batch start
#iterations: 203
currently lose_sum: 94.8596538901329
time_elpased: 1.837
batch start
#iterations: 204
currently lose_sum: 94.75836211442947
time_elpased: 1.8
batch start
#iterations: 205
currently lose_sum: 95.0766413807869
time_elpased: 1.777
batch start
#iterations: 206
currently lose_sum: 94.84848582744598
time_elpased: 1.764
batch start
#iterations: 207
currently lose_sum: 94.98425853252411
time_elpased: 1.79
batch start
#iterations: 208
currently lose_sum: 94.68064397573471
time_elpased: 1.8
batch start
#iterations: 209
currently lose_sum: 94.9151856303215
time_elpased: 1.748
batch start
#iterations: 210
currently lose_sum: 94.9159744977951
time_elpased: 1.733
batch start
#iterations: 211
currently lose_sum: 95.15160310268402
time_elpased: 1.768
batch start
#iterations: 212
currently lose_sum: 94.92427492141724
time_elpased: 1.769
batch start
#iterations: 213
currently lose_sum: 94.83397495746613
time_elpased: 1.818
batch start
#iterations: 214
currently lose_sum: 94.80878037214279
time_elpased: 1.747
batch start
#iterations: 215
currently lose_sum: 94.58057886362076
time_elpased: 1.764
batch start
#iterations: 216
currently lose_sum: 94.57049012184143
time_elpased: 1.778
batch start
#iterations: 217
currently lose_sum: 95.06762564182281
time_elpased: 1.762
batch start
#iterations: 218
currently lose_sum: 94.66463017463684
time_elpased: 1.74
batch start
#iterations: 219
currently lose_sum: 95.10044431686401
time_elpased: 1.738
start validation test
0.672319587629
0.633248730964
0.82165277349
0.715251959686
0.672057410188
60.135
batch start
#iterations: 220
currently lose_sum: 94.81501066684723
time_elpased: 1.745
batch start
#iterations: 221
currently lose_sum: 94.78269475698471
time_elpased: 1.771
batch start
#iterations: 222
currently lose_sum: 94.41608011722565
time_elpased: 1.735
batch start
#iterations: 223
currently lose_sum: 94.56149858236313
time_elpased: 1.752
batch start
#iterations: 224
currently lose_sum: 94.66282725334167
time_elpased: 1.736
batch start
#iterations: 225
currently lose_sum: 95.23555356264114
time_elpased: 1.773
batch start
#iterations: 226
currently lose_sum: 94.92017978429794
time_elpased: 1.759
batch start
#iterations: 227
currently lose_sum: 94.8458321094513
time_elpased: 1.744
batch start
#iterations: 228
currently lose_sum: 94.8848951458931
time_elpased: 1.749
batch start
#iterations: 229
currently lose_sum: 94.7765132188797
time_elpased: 1.809
batch start
#iterations: 230
currently lose_sum: 94.60735857486725
time_elpased: 1.73
batch start
#iterations: 231
currently lose_sum: 94.640256524086
time_elpased: 1.802
batch start
#iterations: 232
currently lose_sum: 94.78699523210526
time_elpased: 1.77
batch start
#iterations: 233
currently lose_sum: 94.92179304361343
time_elpased: 1.762
batch start
#iterations: 234
currently lose_sum: 94.71253871917725
time_elpased: 1.762
batch start
#iterations: 235
currently lose_sum: 94.84272015094757
time_elpased: 1.795
batch start
#iterations: 236
currently lose_sum: 94.89682000875473
time_elpased: 1.748
batch start
#iterations: 237
currently lose_sum: 94.90211135149002
time_elpased: 1.742
batch start
#iterations: 238
currently lose_sum: 94.6397043466568
time_elpased: 1.79
batch start
#iterations: 239
currently lose_sum: 95.24057173728943
time_elpased: 1.773
start validation test
0.669175257732
0.647079803834
0.746835443038
0.693388113893
0.669038913298
60.641
batch start
#iterations: 240
currently lose_sum: 94.57500576972961
time_elpased: 1.758
batch start
#iterations: 241
currently lose_sum: 95.13680684566498
time_elpased: 1.746
batch start
#iterations: 242
currently lose_sum: 95.02298188209534
time_elpased: 1.748
batch start
#iterations: 243
currently lose_sum: 94.45980477333069
time_elpased: 1.742
batch start
#iterations: 244
currently lose_sum: 95.15895479917526
time_elpased: 1.731
batch start
#iterations: 245
currently lose_sum: 94.739253282547
time_elpased: 1.776
batch start
#iterations: 246
currently lose_sum: 94.82917499542236
time_elpased: 1.787
batch start
#iterations: 247
currently lose_sum: 94.39882081747055
time_elpased: 1.801
batch start
#iterations: 248
currently lose_sum: 94.4825159907341
time_elpased: 1.775
batch start
#iterations: 249
currently lose_sum: 94.84016001224518
time_elpased: 1.823
batch start
#iterations: 250
currently lose_sum: 94.77165907621384
time_elpased: 1.826
batch start
#iterations: 251
currently lose_sum: 94.57370173931122
time_elpased: 1.817
batch start
#iterations: 252
currently lose_sum: 94.71558892726898
time_elpased: 1.738
batch start
#iterations: 253
currently lose_sum: 94.46651214361191
time_elpased: 1.776
batch start
#iterations: 254
currently lose_sum: 94.60100334882736
time_elpased: 1.754
batch start
#iterations: 255
currently lose_sum: 94.8110962510109
time_elpased: 1.773
batch start
#iterations: 256
currently lose_sum: 94.93345028162003
time_elpased: 1.745
batch start
#iterations: 257
currently lose_sum: 94.54400169849396
time_elpased: 1.802
batch start
#iterations: 258
currently lose_sum: 94.28356891870499
time_elpased: 1.744
batch start
#iterations: 259
currently lose_sum: 94.16343069076538
time_elpased: 1.763
start validation test
0.670824742268
0.644712833435
0.763610167747
0.69914256101
0.670661843142
60.206
batch start
#iterations: 260
currently lose_sum: 94.61600476503372
time_elpased: 1.747
batch start
#iterations: 261
currently lose_sum: 94.62509083747864
time_elpased: 1.747
batch start
#iterations: 262
currently lose_sum: 94.81689685583115
time_elpased: 1.771
batch start
#iterations: 263
currently lose_sum: 94.7892165184021
time_elpased: 1.762
batch start
#iterations: 264
currently lose_sum: 94.71166557073593
time_elpased: 1.78
batch start
#iterations: 265
currently lose_sum: 94.68494212627411
time_elpased: 1.788
batch start
#iterations: 266
currently lose_sum: 94.57608306407928
time_elpased: 1.755
batch start
#iterations: 267
currently lose_sum: 94.50078332424164
time_elpased: 1.752
batch start
#iterations: 268
currently lose_sum: 94.5198318362236
time_elpased: 1.846
batch start
#iterations: 269
currently lose_sum: 94.74888551235199
time_elpased: 1.754
batch start
#iterations: 270
currently lose_sum: 94.64045959711075
time_elpased: 1.768
batch start
#iterations: 271
currently lose_sum: 94.64762222766876
time_elpased: 1.76
batch start
#iterations: 272
currently lose_sum: 94.45402610301971
time_elpased: 1.771
batch start
#iterations: 273
currently lose_sum: 94.21441900730133
time_elpased: 1.732
batch start
#iterations: 274
currently lose_sum: 94.8432611823082
time_elpased: 1.749
batch start
#iterations: 275
currently lose_sum: 94.5492371916771
time_elpased: 1.791
batch start
#iterations: 276
currently lose_sum: 94.5696092247963
time_elpased: 1.743
batch start
#iterations: 277
currently lose_sum: 94.44479632377625
time_elpased: 1.769
batch start
#iterations: 278
currently lose_sum: 94.41994321346283
time_elpased: 1.764
batch start
#iterations: 279
currently lose_sum: 94.55488216876984
time_elpased: 1.806
start validation test
0.670154639175
0.63062992126
0.824225584028
0.714546995584
0.669884143868
60.146
batch start
#iterations: 280
currently lose_sum: 95.02876269817352
time_elpased: 1.759
batch start
#iterations: 281
currently lose_sum: 94.49330335855484
time_elpased: 1.785
batch start
#iterations: 282
currently lose_sum: 94.40388822555542
time_elpased: 1.781
batch start
#iterations: 283
currently lose_sum: 94.64045614004135
time_elpased: 1.759
batch start
#iterations: 284
currently lose_sum: 94.37481504678726
time_elpased: 1.736
batch start
#iterations: 285
currently lose_sum: 94.19614607095718
time_elpased: 1.801
batch start
#iterations: 286
currently lose_sum: 94.8921566605568
time_elpased: 1.757
batch start
#iterations: 287
currently lose_sum: 94.23293483257294
time_elpased: 1.75
batch start
#iterations: 288
currently lose_sum: 94.62546294927597
time_elpased: 1.85
batch start
#iterations: 289
currently lose_sum: 94.50288707017899
time_elpased: 1.775
batch start
#iterations: 290
currently lose_sum: 94.25512272119522
time_elpased: 1.786
batch start
#iterations: 291
currently lose_sum: 94.76550418138504
time_elpased: 1.773
batch start
#iterations: 292
currently lose_sum: 94.98297071456909
time_elpased: 1.816
batch start
#iterations: 293
currently lose_sum: 94.39803171157837
time_elpased: 1.762
batch start
#iterations: 294
currently lose_sum: 94.64494794607162
time_elpased: 1.735
batch start
#iterations: 295
currently lose_sum: 94.84095275402069
time_elpased: 1.752
batch start
#iterations: 296
currently lose_sum: 94.86720663309097
time_elpased: 1.736
batch start
#iterations: 297
currently lose_sum: 94.64546066522598
time_elpased: 1.783
batch start
#iterations: 298
currently lose_sum: 94.6147631406784
time_elpased: 1.78
batch start
#iterations: 299
currently lose_sum: 94.57433861494064
time_elpased: 1.821
start validation test
0.665515463918
0.649582947173
0.721313162499
0.683571463403
0.665417502451
60.547
batch start
#iterations: 300
currently lose_sum: 94.1696400642395
time_elpased: 1.715
batch start
#iterations: 301
currently lose_sum: 94.75077801942825
time_elpased: 1.72
batch start
#iterations: 302
currently lose_sum: 94.61832875013351
time_elpased: 1.737
batch start
#iterations: 303
currently lose_sum: 94.7410437464714
time_elpased: 1.754
batch start
#iterations: 304
currently lose_sum: 94.47562742233276
time_elpased: 1.731
batch start
#iterations: 305
currently lose_sum: 94.42550945281982
time_elpased: 1.758
batch start
#iterations: 306
currently lose_sum: 93.93077754974365
time_elpased: 1.761
batch start
#iterations: 307
currently lose_sum: 94.76558840274811
time_elpased: 1.791
batch start
#iterations: 308
currently lose_sum: 94.41445136070251
time_elpased: 1.735
batch start
#iterations: 309
currently lose_sum: 94.50402915477753
time_elpased: 1.749
batch start
#iterations: 310
currently lose_sum: 94.69709330797195
time_elpased: 1.725
batch start
#iterations: 311
currently lose_sum: 94.53589820861816
time_elpased: 1.778
batch start
#iterations: 312
currently lose_sum: 94.2533569931984
time_elpased: 1.739
batch start
#iterations: 313
currently lose_sum: 94.03841471672058
time_elpased: 1.76
batch start
#iterations: 314
currently lose_sum: 94.66612207889557
time_elpased: 1.781
batch start
#iterations: 315
currently lose_sum: 94.42436230182648
time_elpased: 1.8
batch start
#iterations: 316
currently lose_sum: 94.42585015296936
time_elpased: 1.739
batch start
#iterations: 317
currently lose_sum: 94.45003074407578
time_elpased: 1.813
batch start
#iterations: 318
currently lose_sum: 94.90834987163544
time_elpased: 1.724
batch start
#iterations: 319
currently lose_sum: 94.68840831518173
time_elpased: 1.759
start validation test
0.669175257732
0.656960700352
0.710507358238
0.682685652131
0.669102692854
60.622
batch start
#iterations: 320
currently lose_sum: 94.12958174943924
time_elpased: 1.807
batch start
#iterations: 321
currently lose_sum: 94.68103760480881
time_elpased: 1.779
batch start
#iterations: 322
currently lose_sum: 94.50231379270554
time_elpased: 1.753
batch start
#iterations: 323
currently lose_sum: 94.64287519454956
time_elpased: 1.748
batch start
#iterations: 324
currently lose_sum: 94.59527289867401
time_elpased: 1.806
batch start
#iterations: 325
currently lose_sum: 94.38234120607376
time_elpased: 1.774
batch start
#iterations: 326
currently lose_sum: 94.42024427652359
time_elpased: 1.769
batch start
#iterations: 327
currently lose_sum: 94.30521583557129
time_elpased: 1.756
batch start
#iterations: 328
currently lose_sum: 94.83614993095398
time_elpased: 1.731
batch start
#iterations: 329
currently lose_sum: 94.26564717292786
time_elpased: 1.741
batch start
#iterations: 330
currently lose_sum: 94.5512763261795
time_elpased: 1.822
batch start
#iterations: 331
currently lose_sum: 94.27126610279083
time_elpased: 1.795
batch start
#iterations: 332
currently lose_sum: 93.90740513801575
time_elpased: 1.742
batch start
#iterations: 333
currently lose_sum: 94.5276306271553
time_elpased: 1.768
batch start
#iterations: 334
currently lose_sum: 94.42028445005417
time_elpased: 1.749
batch start
#iterations: 335
currently lose_sum: 94.42129188776016
time_elpased: 1.78
batch start
#iterations: 336
currently lose_sum: 94.19606405496597
time_elpased: 1.757
batch start
#iterations: 337
currently lose_sum: 94.79694676399231
time_elpased: 1.764
batch start
#iterations: 338
currently lose_sum: 94.6254261136055
time_elpased: 1.771
batch start
#iterations: 339
currently lose_sum: 94.42061752080917
time_elpased: 1.759
start validation test
0.669948453608
0.640829508754
0.775959658331
0.70195037937
0.669762334587
60.134
batch start
#iterations: 340
currently lose_sum: 94.78388142585754
time_elpased: 1.765
batch start
#iterations: 341
currently lose_sum: 94.35428804159164
time_elpased: 1.795
batch start
#iterations: 342
currently lose_sum: 94.56671637296677
time_elpased: 1.782
batch start
#iterations: 343
currently lose_sum: 94.48243319988251
time_elpased: 1.752
batch start
#iterations: 344
currently lose_sum: 94.20797818899155
time_elpased: 1.735
batch start
#iterations: 345
currently lose_sum: 94.95676499605179
time_elpased: 1.814
batch start
#iterations: 346
currently lose_sum: 94.02102887630463
time_elpased: 1.79
batch start
#iterations: 347
currently lose_sum: 94.712959587574
time_elpased: 1.77
batch start
#iterations: 348
currently lose_sum: 94.4493248462677
time_elpased: 1.788
batch start
#iterations: 349
currently lose_sum: 94.60888522863388
time_elpased: 1.795
batch start
#iterations: 350
currently lose_sum: 94.64013427495956
time_elpased: 1.734
batch start
#iterations: 351
currently lose_sum: 94.3476493358612
time_elpased: 1.746
batch start
#iterations: 352
currently lose_sum: 94.35190773010254
time_elpased: 1.769
batch start
#iterations: 353
currently lose_sum: 94.37944543361664
time_elpased: 1.754
batch start
#iterations: 354
currently lose_sum: 95.13876849412918
time_elpased: 1.748
batch start
#iterations: 355
currently lose_sum: 94.12699127197266
time_elpased: 1.732
batch start
#iterations: 356
currently lose_sum: 94.77032911777496
time_elpased: 1.752
batch start
#iterations: 357
currently lose_sum: 94.5366280078888
time_elpased: 1.808
batch start
#iterations: 358
currently lose_sum: 94.69880253076553
time_elpased: 1.74
batch start
#iterations: 359
currently lose_sum: 94.70963650941849
time_elpased: 1.807
start validation test
0.665412371134
0.65137012012
0.714315117835
0.681392038482
0.665326514819
60.632
batch start
#iterations: 360
currently lose_sum: 94.50705575942993
time_elpased: 1.769
batch start
#iterations: 361
currently lose_sum: 94.72153156995773
time_elpased: 1.802
batch start
#iterations: 362
currently lose_sum: 94.5068045258522
time_elpased: 1.739
batch start
#iterations: 363
currently lose_sum: 94.48671662807465
time_elpased: 1.801
batch start
#iterations: 364
currently lose_sum: 94.56612902879715
time_elpased: 1.749
batch start
#iterations: 365
currently lose_sum: 94.62074559926987
time_elpased: 1.753
batch start
#iterations: 366
currently lose_sum: 94.55599933862686
time_elpased: 1.778
batch start
#iterations: 367
currently lose_sum: 94.32106119394302
time_elpased: 1.765
batch start
#iterations: 368
currently lose_sum: 94.7363902926445
time_elpased: 1.755
batch start
#iterations: 369
currently lose_sum: 94.53126215934753
time_elpased: 1.765
batch start
#iterations: 370
currently lose_sum: 94.24538445472717
time_elpased: 1.772
batch start
#iterations: 371
currently lose_sum: 94.23353677988052
time_elpased: 1.795
batch start
#iterations: 372
currently lose_sum: 94.38357681035995
time_elpased: 1.757
batch start
#iterations: 373
currently lose_sum: 94.30373692512512
time_elpased: 1.774
batch start
#iterations: 374
currently lose_sum: 94.98381698131561
time_elpased: 1.759
batch start
#iterations: 375
currently lose_sum: 94.48692518472672
time_elpased: 1.749
batch start
#iterations: 376
currently lose_sum: 94.24532812833786
time_elpased: 1.746
batch start
#iterations: 377
currently lose_sum: 94.49824655056
time_elpased: 1.779
batch start
#iterations: 378
currently lose_sum: 94.69846987724304
time_elpased: 1.73
batch start
#iterations: 379
currently lose_sum: 94.64776110649109
time_elpased: 1.79
start validation test
0.673092783505
0.643117632092
0.780384892457
0.705132973777
0.672904415659
60.062
batch start
#iterations: 380
currently lose_sum: 94.5476343035698
time_elpased: 1.753
batch start
#iterations: 381
currently lose_sum: 94.30222660303116
time_elpased: 1.774
batch start
#iterations: 382
currently lose_sum: 94.61820703744888
time_elpased: 1.737
batch start
#iterations: 383
currently lose_sum: 94.61145156621933
time_elpased: 1.775
batch start
#iterations: 384
currently lose_sum: 94.54194670915604
time_elpased: 1.716
batch start
#iterations: 385
currently lose_sum: 94.6515182852745
time_elpased: 1.823
batch start
#iterations: 386
currently lose_sum: 94.67542660236359
time_elpased: 1.751
batch start
#iterations: 387
currently lose_sum: 94.68507844209671
time_elpased: 1.77
batch start
#iterations: 388
currently lose_sum: 94.59420067071915
time_elpased: 1.781
batch start
#iterations: 389
currently lose_sum: 94.48997050523758
time_elpased: 1.757
batch start
#iterations: 390
currently lose_sum: 94.51464909315109
time_elpased: 1.759
batch start
#iterations: 391
currently lose_sum: 94.50797241926193
time_elpased: 1.779
batch start
#iterations: 392
currently lose_sum: 94.22507703304291
time_elpased: 1.742
batch start
#iterations: 393
currently lose_sum: 94.19850546121597
time_elpased: 1.799
batch start
#iterations: 394
currently lose_sum: 94.39059764146805
time_elpased: 1.713
batch start
#iterations: 395
currently lose_sum: 94.47413241863251
time_elpased: 1.745
batch start
#iterations: 396
currently lose_sum: 94.09330695867538
time_elpased: 1.783
batch start
#iterations: 397
currently lose_sum: 94.3640713095665
time_elpased: 1.758
batch start
#iterations: 398
currently lose_sum: 94.32410222291946
time_elpased: 1.787
batch start
#iterations: 399
currently lose_sum: 94.62853163480759
time_elpased: 1.821
start validation test
0.668298969072
0.643043933054
0.759184933621
0.696304686394
0.668139404743
60.496
acc: 0.672
pre: 0.643
rec: 0.778
F1: 0.704
auc: 0.710
