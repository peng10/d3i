start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.7526490688324
time_elpased: 1.429
batch start
#iterations: 1
currently lose_sum: 99.9825878739357
time_elpased: 1.451
batch start
#iterations: 2
currently lose_sum: 99.8362722992897
time_elpased: 1.426
batch start
#iterations: 3
currently lose_sum: 99.64304798841476
time_elpased: 1.454
batch start
#iterations: 4
currently lose_sum: 99.6157910823822
time_elpased: 1.392
batch start
#iterations: 5
currently lose_sum: 99.31887340545654
time_elpased: 1.427
batch start
#iterations: 6
currently lose_sum: 99.11591923236847
time_elpased: 1.43
batch start
#iterations: 7
currently lose_sum: 98.61220812797546
time_elpased: 1.424
batch start
#iterations: 8
currently lose_sum: 98.85074031352997
time_elpased: 1.414
batch start
#iterations: 9
currently lose_sum: 98.5915709733963
time_elpased: 1.408
batch start
#iterations: 10
currently lose_sum: 98.32056373357773
time_elpased: 1.384
batch start
#iterations: 11
currently lose_sum: 98.27744281291962
time_elpased: 1.431
batch start
#iterations: 12
currently lose_sum: 97.88934886455536
time_elpased: 1.384
batch start
#iterations: 13
currently lose_sum: 97.91057598590851
time_elpased: 1.433
batch start
#iterations: 14
currently lose_sum: 97.57616478204727
time_elpased: 1.45
batch start
#iterations: 15
currently lose_sum: 97.64059525728226
time_elpased: 1.401
batch start
#iterations: 16
currently lose_sum: 97.59703522920609
time_elpased: 1.404
batch start
#iterations: 17
currently lose_sum: 97.2544355392456
time_elpased: 1.413
batch start
#iterations: 18
currently lose_sum: 97.27142935991287
time_elpased: 1.43
batch start
#iterations: 19
currently lose_sum: 97.1331998705864
time_elpased: 1.42
start validation test
0.647835051546
0.651602732528
0.638057013482
0.644758735441
0.6478522184
62.775
batch start
#iterations: 20
currently lose_sum: 97.3740770816803
time_elpased: 1.411
batch start
#iterations: 21
currently lose_sum: 96.94431984424591
time_elpased: 1.408
batch start
#iterations: 22
currently lose_sum: 97.10049176216125
time_elpased: 1.4
batch start
#iterations: 23
currently lose_sum: 96.74634397029877
time_elpased: 1.416
batch start
#iterations: 24
currently lose_sum: 97.09782367944717
time_elpased: 1.473
batch start
#iterations: 25
currently lose_sum: 96.6875131726265
time_elpased: 1.421
batch start
#iterations: 26
currently lose_sum: 96.66618800163269
time_elpased: 1.422
batch start
#iterations: 27
currently lose_sum: 96.74059331417084
time_elpased: 1.377
batch start
#iterations: 28
currently lose_sum: 96.46838629245758
time_elpased: 1.405
batch start
#iterations: 29
currently lose_sum: 96.74637788534164
time_elpased: 1.428
batch start
#iterations: 30
currently lose_sum: 96.54530960321426
time_elpased: 1.42
batch start
#iterations: 31
currently lose_sum: 96.27496629953384
time_elpased: 1.409
batch start
#iterations: 32
currently lose_sum: 96.20166629552841
time_elpased: 1.439
batch start
#iterations: 33
currently lose_sum: 96.4645659327507
time_elpased: 1.395
batch start
#iterations: 34
currently lose_sum: 96.28400564193726
time_elpased: 1.396
batch start
#iterations: 35
currently lose_sum: 96.65070456266403
time_elpased: 1.429
batch start
#iterations: 36
currently lose_sum: 96.50757950544357
time_elpased: 1.417
batch start
#iterations: 37
currently lose_sum: 96.52404129505157
time_elpased: 1.415
batch start
#iterations: 38
currently lose_sum: 96.0709120631218
time_elpased: 1.447
batch start
#iterations: 39
currently lose_sum: 95.92041045427322
time_elpased: 1.421
start validation test
0.656649484536
0.640132061629
0.718328702274
0.676979777896
0.656541197156
61.405
batch start
#iterations: 40
currently lose_sum: 96.43428528308868
time_elpased: 1.423
batch start
#iterations: 41
currently lose_sum: 96.17891752719879
time_elpased: 1.471
batch start
#iterations: 42
currently lose_sum: 96.11869943141937
time_elpased: 1.423
batch start
#iterations: 43
currently lose_sum: 96.09845769405365
time_elpased: 1.399
batch start
#iterations: 44
currently lose_sum: 95.99540293216705
time_elpased: 1.437
batch start
#iterations: 45
currently lose_sum: 96.18312919139862
time_elpased: 1.408
batch start
#iterations: 46
currently lose_sum: 95.54512304067612
time_elpased: 1.47
batch start
#iterations: 47
currently lose_sum: 96.12375658750534
time_elpased: 1.391
batch start
#iterations: 48
currently lose_sum: 96.17408519983292
time_elpased: 1.459
batch start
#iterations: 49
currently lose_sum: 96.08888339996338
time_elpased: 1.395
batch start
#iterations: 50
currently lose_sum: 96.12289947271347
time_elpased: 1.393
batch start
#iterations: 51
currently lose_sum: 96.09599334001541
time_elpased: 1.454
batch start
#iterations: 52
currently lose_sum: 96.28307092189789
time_elpased: 1.428
batch start
#iterations: 53
currently lose_sum: 95.82705515623093
time_elpased: 1.385
batch start
#iterations: 54
currently lose_sum: 96.2548246383667
time_elpased: 1.383
batch start
#iterations: 55
currently lose_sum: 95.94530683755875
time_elpased: 1.445
batch start
#iterations: 56
currently lose_sum: 95.5595725774765
time_elpased: 1.398
batch start
#iterations: 57
currently lose_sum: 95.63685834407806
time_elpased: 1.389
batch start
#iterations: 58
currently lose_sum: 95.78203469514847
time_elpased: 1.424
batch start
#iterations: 59
currently lose_sum: 95.7599288225174
time_elpased: 1.386
start validation test
0.655309278351
0.664174252276
0.630750231553
0.64703087886
0.655352395545
61.533
batch start
#iterations: 60
currently lose_sum: 95.79414188861847
time_elpased: 1.475
batch start
#iterations: 61
currently lose_sum: 95.73017090559006
time_elpased: 1.41
batch start
#iterations: 62
currently lose_sum: 95.6317897439003
time_elpased: 1.39
batch start
#iterations: 63
currently lose_sum: 96.19421952962875
time_elpased: 1.452
batch start
#iterations: 64
currently lose_sum: 95.71228617429733
time_elpased: 1.408
batch start
#iterations: 65
currently lose_sum: 95.33296221494675
time_elpased: 1.411
batch start
#iterations: 66
currently lose_sum: 95.49538308382034
time_elpased: 1.393
batch start
#iterations: 67
currently lose_sum: 95.86915296316147
time_elpased: 1.388
batch start
#iterations: 68
currently lose_sum: 95.79535847902298
time_elpased: 1.409
batch start
#iterations: 69
currently lose_sum: 95.87080591917038
time_elpased: 1.402
batch start
#iterations: 70
currently lose_sum: 95.93403851985931
time_elpased: 1.436
batch start
#iterations: 71
currently lose_sum: 95.57349932193756
time_elpased: 1.379
batch start
#iterations: 72
currently lose_sum: 95.61479371786118
time_elpased: 1.424
batch start
#iterations: 73
currently lose_sum: 95.39984655380249
time_elpased: 1.369
batch start
#iterations: 74
currently lose_sum: 95.59899890422821
time_elpased: 1.413
batch start
#iterations: 75
currently lose_sum: 95.37855297327042
time_elpased: 1.479
batch start
#iterations: 76
currently lose_sum: 95.21786433458328
time_elpased: 1.437
batch start
#iterations: 77
currently lose_sum: 95.54081308841705
time_elpased: 1.405
batch start
#iterations: 78
currently lose_sum: 95.44972783327103
time_elpased: 1.47
batch start
#iterations: 79
currently lose_sum: 95.95985436439514
time_elpased: 1.418
start validation test
0.664948453608
0.636905268533
0.770093650304
0.69719556508
0.664763854998
61.134
batch start
#iterations: 80
currently lose_sum: 95.69908118247986
time_elpased: 1.411
batch start
#iterations: 81
currently lose_sum: 95.5447126030922
time_elpased: 1.414
batch start
#iterations: 82
currently lose_sum: 95.76379156112671
time_elpased: 1.421
batch start
#iterations: 83
currently lose_sum: 95.58124154806137
time_elpased: 1.393
batch start
#iterations: 84
currently lose_sum: 95.52795749902725
time_elpased: 1.464
batch start
#iterations: 85
currently lose_sum: 95.85686928033829
time_elpased: 1.409
batch start
#iterations: 86
currently lose_sum: 95.68222773075104
time_elpased: 1.392
batch start
#iterations: 87
currently lose_sum: 95.14180248975754
time_elpased: 1.519
batch start
#iterations: 88
currently lose_sum: 95.47833156585693
time_elpased: 1.418
batch start
#iterations: 89
currently lose_sum: 95.03908383846283
time_elpased: 1.397
batch start
#iterations: 90
currently lose_sum: 95.46962612867355
time_elpased: 1.412
batch start
#iterations: 91
currently lose_sum: 95.52643728256226
time_elpased: 1.421
batch start
#iterations: 92
currently lose_sum: 95.23536598682404
time_elpased: 1.39
batch start
#iterations: 93
currently lose_sum: 95.5148137807846
time_elpased: 1.424
batch start
#iterations: 94
currently lose_sum: 95.10253423452377
time_elpased: 1.433
batch start
#iterations: 95
currently lose_sum: 95.50271475315094
time_elpased: 1.385
batch start
#iterations: 96
currently lose_sum: 95.56924057006836
time_elpased: 1.397
batch start
#iterations: 97
currently lose_sum: 95.37747830152512
time_elpased: 1.409
batch start
#iterations: 98
currently lose_sum: 95.44223755598068
time_elpased: 1.42
batch start
#iterations: 99
currently lose_sum: 94.98629814386368
time_elpased: 1.397
start validation test
0.664690721649
0.628438899552
0.808685808377
0.707258899239
0.664437916065
60.951
batch start
#iterations: 100
currently lose_sum: 95.17473441362381
time_elpased: 1.407
batch start
#iterations: 101
currently lose_sum: 95.40879333019257
time_elpased: 1.39
batch start
#iterations: 102
currently lose_sum: 94.818987429142
time_elpased: 1.404
batch start
#iterations: 103
currently lose_sum: 95.1139121055603
time_elpased: 1.397
batch start
#iterations: 104
currently lose_sum: 95.53138464689255
time_elpased: 1.38
batch start
#iterations: 105
currently lose_sum: 95.40838289260864
time_elpased: 1.394
batch start
#iterations: 106
currently lose_sum: 95.34179329872131
time_elpased: 1.389
batch start
#iterations: 107
currently lose_sum: 95.44460183382034
time_elpased: 1.396
batch start
#iterations: 108
currently lose_sum: 95.64083021879196
time_elpased: 1.418
batch start
#iterations: 109
currently lose_sum: 95.28263849020004
time_elpased: 1.418
batch start
#iterations: 110
currently lose_sum: 95.19034308195114
time_elpased: 1.416
batch start
#iterations: 111
currently lose_sum: 95.02916210889816
time_elpased: 1.395
batch start
#iterations: 112
currently lose_sum: 95.14932155609131
time_elpased: 1.446
batch start
#iterations: 113
currently lose_sum: 95.16702687740326
time_elpased: 1.435
batch start
#iterations: 114
currently lose_sum: 94.87963062524796
time_elpased: 1.454
batch start
#iterations: 115
currently lose_sum: 95.09300374984741
time_elpased: 1.393
batch start
#iterations: 116
currently lose_sum: 95.2823201417923
time_elpased: 1.404
batch start
#iterations: 117
currently lose_sum: 95.171173453331
time_elpased: 1.399
batch start
#iterations: 118
currently lose_sum: 95.5093709230423
time_elpased: 1.439
batch start
#iterations: 119
currently lose_sum: 95.26531457901001
time_elpased: 1.399
start validation test
0.654432989691
0.664662804678
0.62581043532
0.644651754479
0.654483241
61.555
batch start
#iterations: 120
currently lose_sum: 95.03838330507278
time_elpased: 1.431
batch start
#iterations: 121
currently lose_sum: 95.33621728420258
time_elpased: 1.443
batch start
#iterations: 122
currently lose_sum: 95.22818434238434
time_elpased: 1.417
batch start
#iterations: 123
currently lose_sum: 94.91536802053452
time_elpased: 1.39
batch start
#iterations: 124
currently lose_sum: 95.06359994411469
time_elpased: 1.4
batch start
#iterations: 125
currently lose_sum: 95.31462794542313
time_elpased: 1.393
batch start
#iterations: 126
currently lose_sum: 95.31083953380585
time_elpased: 1.386
batch start
#iterations: 127
currently lose_sum: 95.4250538945198
time_elpased: 1.414
batch start
#iterations: 128
currently lose_sum: 95.23209714889526
time_elpased: 1.411
batch start
#iterations: 129
currently lose_sum: 94.7870265841484
time_elpased: 1.388
batch start
#iterations: 130
currently lose_sum: 95.1229259967804
time_elpased: 1.403
batch start
#iterations: 131
currently lose_sum: 94.83730655908585
time_elpased: 1.4
batch start
#iterations: 132
currently lose_sum: 95.07358032464981
time_elpased: 1.419
batch start
#iterations: 133
currently lose_sum: 95.07949584722519
time_elpased: 1.433
batch start
#iterations: 134
currently lose_sum: 95.17050409317017
time_elpased: 1.415
batch start
#iterations: 135
currently lose_sum: 95.4048061966896
time_elpased: 1.421
batch start
#iterations: 136
currently lose_sum: 94.52433985471725
time_elpased: 1.452
batch start
#iterations: 137
currently lose_sum: 95.28090900182724
time_elpased: 1.407
batch start
#iterations: 138
currently lose_sum: 94.78869259357452
time_elpased: 1.501
batch start
#iterations: 139
currently lose_sum: 95.29058265686035
time_elpased: 1.401
start validation test
0.672731958763
0.639542591979
0.794278069363
0.708560936424
0.672518565819
60.747
batch start
#iterations: 140
currently lose_sum: 95.33871155977249
time_elpased: 1.416
batch start
#iterations: 141
currently lose_sum: 95.42603671550751
time_elpased: 1.386
batch start
#iterations: 142
currently lose_sum: 94.7871156334877
time_elpased: 1.404
batch start
#iterations: 143
currently lose_sum: 95.08755815029144
time_elpased: 1.404
batch start
#iterations: 144
currently lose_sum: 95.06996327638626
time_elpased: 1.432
batch start
#iterations: 145
currently lose_sum: 95.40421098470688
time_elpased: 1.366
batch start
#iterations: 146
currently lose_sum: 94.96401381492615
time_elpased: 1.399
batch start
#iterations: 147
currently lose_sum: 94.93146842718124
time_elpased: 1.402
batch start
#iterations: 148
currently lose_sum: 95.52376300096512
time_elpased: 1.37
batch start
#iterations: 149
currently lose_sum: 94.93498110771179
time_elpased: 1.418
batch start
#iterations: 150
currently lose_sum: 94.93681424856186
time_elpased: 1.422
batch start
#iterations: 151
currently lose_sum: 94.76908713579178
time_elpased: 1.464
batch start
#iterations: 152
currently lose_sum: 95.3790437579155
time_elpased: 1.473
batch start
#iterations: 153
currently lose_sum: 95.20516675710678
time_elpased: 1.444
batch start
#iterations: 154
currently lose_sum: 95.21828103065491
time_elpased: 1.419
batch start
#iterations: 155
currently lose_sum: 95.13994657993317
time_elpased: 1.405
batch start
#iterations: 156
currently lose_sum: 94.87826961278915
time_elpased: 1.434
batch start
#iterations: 157
currently lose_sum: 95.41595643758774
time_elpased: 1.395
batch start
#iterations: 158
currently lose_sum: 95.60652607679367
time_elpased: 1.388
batch start
#iterations: 159
currently lose_sum: 95.70289349555969
time_elpased: 1.41
start validation test
0.670309278351
0.651105651106
0.736338376042
0.691104027818
0.670193354085
61.174
batch start
#iterations: 160
currently lose_sum: 95.0150535106659
time_elpased: 1.416
batch start
#iterations: 161
currently lose_sum: 95.129145860672
time_elpased: 1.434
batch start
#iterations: 162
currently lose_sum: 94.67719036340714
time_elpased: 1.416
batch start
#iterations: 163
currently lose_sum: 94.74590349197388
time_elpased: 1.458
batch start
#iterations: 164
currently lose_sum: 94.85413819551468
time_elpased: 1.43
batch start
#iterations: 165
currently lose_sum: 95.09084117412567
time_elpased: 1.468
batch start
#iterations: 166
currently lose_sum: 94.9242280125618
time_elpased: 1.403
batch start
#iterations: 167
currently lose_sum: 95.2703508734703
time_elpased: 1.413
batch start
#iterations: 168
currently lose_sum: 94.75887805223465
time_elpased: 1.411
batch start
#iterations: 169
currently lose_sum: 94.79617595672607
time_elpased: 1.439
batch start
#iterations: 170
currently lose_sum: 94.8560254573822
time_elpased: 1.414
batch start
#iterations: 171
currently lose_sum: 95.21310126781464
time_elpased: 1.386
batch start
#iterations: 172
currently lose_sum: 95.17127895355225
time_elpased: 1.476
batch start
#iterations: 173
currently lose_sum: 94.91203266382217
time_elpased: 1.413
batch start
#iterations: 174
currently lose_sum: 94.81422704458237
time_elpased: 1.433
batch start
#iterations: 175
currently lose_sum: 94.71966469287872
time_elpased: 1.4
batch start
#iterations: 176
currently lose_sum: 94.74977964162827
time_elpased: 1.402
batch start
#iterations: 177
currently lose_sum: 94.69123977422714
time_elpased: 1.394
batch start
#iterations: 178
currently lose_sum: 95.09825205802917
time_elpased: 1.437
batch start
#iterations: 179
currently lose_sum: 95.02520108222961
time_elpased: 1.441
start validation test
0.665515463918
0.634119993352
0.78532468869
0.701668888787
0.665305120344
60.699
batch start
#iterations: 180
currently lose_sum: 94.944060921669
time_elpased: 1.463
batch start
#iterations: 181
currently lose_sum: 95.23237776756287
time_elpased: 1.392
batch start
#iterations: 182
currently lose_sum: 94.883021235466
time_elpased: 1.403
batch start
#iterations: 183
currently lose_sum: 94.41468477249146
time_elpased: 1.416
batch start
#iterations: 184
currently lose_sum: 94.66222178936005
time_elpased: 1.423
batch start
#iterations: 185
currently lose_sum: 94.95783656835556
time_elpased: 1.389
batch start
#iterations: 186
currently lose_sum: 94.63808381557465
time_elpased: 1.39
batch start
#iterations: 187
currently lose_sum: 94.56046736240387
time_elpased: 1.406
batch start
#iterations: 188
currently lose_sum: 94.91580408811569
time_elpased: 1.417
batch start
#iterations: 189
currently lose_sum: 94.94397658109665
time_elpased: 1.417
batch start
#iterations: 190
currently lose_sum: 94.94766509532928
time_elpased: 1.404
batch start
#iterations: 191
currently lose_sum: 94.79665243625641
time_elpased: 1.421
batch start
#iterations: 192
currently lose_sum: 94.73944413661957
time_elpased: 1.427
batch start
#iterations: 193
currently lose_sum: 95.06443411111832
time_elpased: 1.401
batch start
#iterations: 194
currently lose_sum: 94.90001064538956
time_elpased: 1.397
batch start
#iterations: 195
currently lose_sum: 94.93971186876297
time_elpased: 1.441
batch start
#iterations: 196
currently lose_sum: 95.50033915042877
time_elpased: 1.467
batch start
#iterations: 197
currently lose_sum: 94.8564168214798
time_elpased: 1.399
batch start
#iterations: 198
currently lose_sum: 94.95100367069244
time_elpased: 1.439
batch start
#iterations: 199
currently lose_sum: 94.96731466054916
time_elpased: 1.408
start validation test
0.667319587629
0.639242126824
0.770814037254
0.698889614631
0.66713788716
60.877
batch start
#iterations: 200
currently lose_sum: 94.52336674928665
time_elpased: 1.423
batch start
#iterations: 201
currently lose_sum: 94.49197816848755
time_elpased: 1.39
batch start
#iterations: 202
currently lose_sum: 94.9220803976059
time_elpased: 1.424
batch start
#iterations: 203
currently lose_sum: 94.65559959411621
time_elpased: 1.402
batch start
#iterations: 204
currently lose_sum: 94.9192026257515
time_elpased: 1.438
batch start
#iterations: 205
currently lose_sum: 94.55461430549622
time_elpased: 1.416
batch start
#iterations: 206
currently lose_sum: 94.90189898014069
time_elpased: 1.392
batch start
#iterations: 207
currently lose_sum: 94.96301418542862
time_elpased: 1.405
batch start
#iterations: 208
currently lose_sum: 94.60767662525177
time_elpased: 1.401
batch start
#iterations: 209
currently lose_sum: 94.68636763095856
time_elpased: 1.441
batch start
#iterations: 210
currently lose_sum: 94.81504571437836
time_elpased: 1.405
batch start
#iterations: 211
currently lose_sum: 94.65784752368927
time_elpased: 1.415
batch start
#iterations: 212
currently lose_sum: 94.47995883226395
time_elpased: 1.403
batch start
#iterations: 213
currently lose_sum: 94.80027490854263
time_elpased: 1.389
batch start
#iterations: 214
currently lose_sum: 94.56390857696533
time_elpased: 1.416
batch start
#iterations: 215
currently lose_sum: 94.83963531255722
time_elpased: 1.44
batch start
#iterations: 216
currently lose_sum: 94.4078825712204
time_elpased: 1.43
batch start
#iterations: 217
currently lose_sum: 94.71421313285828
time_elpased: 1.453
batch start
#iterations: 218
currently lose_sum: 94.96556973457336
time_elpased: 1.408
batch start
#iterations: 219
currently lose_sum: 94.84023576974869
time_elpased: 1.435
start validation test
0.665670103093
0.63701127979
0.772975198106
0.69843779059
0.665481712448
60.747
batch start
#iterations: 220
currently lose_sum: 94.97216123342514
time_elpased: 1.425
batch start
#iterations: 221
currently lose_sum: 94.44623583555222
time_elpased: 1.403
batch start
#iterations: 222
currently lose_sum: 94.84400177001953
time_elpased: 1.428
batch start
#iterations: 223
currently lose_sum: 94.45150685310364
time_elpased: 1.449
batch start
#iterations: 224
currently lose_sum: 94.6115648150444
time_elpased: 1.452
batch start
#iterations: 225
currently lose_sum: 94.86322683095932
time_elpased: 1.392
batch start
#iterations: 226
currently lose_sum: 94.62807780504227
time_elpased: 1.452
batch start
#iterations: 227
currently lose_sum: 94.94886916875839
time_elpased: 1.422
batch start
#iterations: 228
currently lose_sum: 94.50171381235123
time_elpased: 1.48
batch start
#iterations: 229
currently lose_sum: 94.19448173046112
time_elpased: 1.382
batch start
#iterations: 230
currently lose_sum: 94.88315063714981
time_elpased: 1.412
batch start
#iterations: 231
currently lose_sum: 94.72665196657181
time_elpased: 1.435
batch start
#iterations: 232
currently lose_sum: 94.63671612739563
time_elpased: 1.392
batch start
#iterations: 233
currently lose_sum: 94.91389638185501
time_elpased: 1.385
batch start
#iterations: 234
currently lose_sum: 94.50573909282684
time_elpased: 1.427
batch start
#iterations: 235
currently lose_sum: 95.02024102210999
time_elpased: 1.445
batch start
#iterations: 236
currently lose_sum: 94.8204745054245
time_elpased: 1.431
batch start
#iterations: 237
currently lose_sum: 94.86070567369461
time_elpased: 1.414
batch start
#iterations: 238
currently lose_sum: 94.9380292892456
time_elpased: 1.399
batch start
#iterations: 239
currently lose_sum: 94.93843024969101
time_elpased: 1.396
start validation test
0.660154639175
0.630318705156
0.777503344654
0.696217112842
0.659948615423
60.710
batch start
#iterations: 240
currently lose_sum: 94.74546718597412
time_elpased: 1.471
batch start
#iterations: 241
currently lose_sum: 94.64002895355225
time_elpased: 1.395
batch start
#iterations: 242
currently lose_sum: 95.01869577169418
time_elpased: 1.448
batch start
#iterations: 243
currently lose_sum: 94.62547779083252
time_elpased: 1.394
batch start
#iterations: 244
currently lose_sum: 94.95047664642334
time_elpased: 1.443
batch start
#iterations: 245
currently lose_sum: 94.99449270963669
time_elpased: 1.432
batch start
#iterations: 246
currently lose_sum: 94.65654861927032
time_elpased: 1.444
batch start
#iterations: 247
currently lose_sum: 94.82611322402954
time_elpased: 1.415
batch start
#iterations: 248
currently lose_sum: 94.56112611293793
time_elpased: 1.517
batch start
#iterations: 249
currently lose_sum: 94.67524588108063
time_elpased: 1.5
batch start
#iterations: 250
currently lose_sum: 94.67715841531754
time_elpased: 1.49
batch start
#iterations: 251
currently lose_sum: 94.66540735960007
time_elpased: 1.43
batch start
#iterations: 252
currently lose_sum: 94.82838773727417
time_elpased: 1.393
batch start
#iterations: 253
currently lose_sum: 94.71844524145126
time_elpased: 1.409
batch start
#iterations: 254
currently lose_sum: 94.8939641714096
time_elpased: 1.387
batch start
#iterations: 255
currently lose_sum: 94.69729262590408
time_elpased: 1.439
batch start
#iterations: 256
currently lose_sum: 94.46999704837799
time_elpased: 1.419
batch start
#iterations: 257
currently lose_sum: 94.322112262249
time_elpased: 1.395
batch start
#iterations: 258
currently lose_sum: 94.16241776943207
time_elpased: 1.511
batch start
#iterations: 259
currently lose_sum: 94.40775436162949
time_elpased: 1.42
start validation test
0.666082474227
0.647994151512
0.729751981064
0.686447241045
0.665970692587
60.753
batch start
#iterations: 260
currently lose_sum: 94.53919064998627
time_elpased: 1.418
batch start
#iterations: 261
currently lose_sum: 94.28163474798203
time_elpased: 1.384
batch start
#iterations: 262
currently lose_sum: 94.74850326776505
time_elpased: 1.387
batch start
#iterations: 263
currently lose_sum: 94.4115361571312
time_elpased: 1.456
batch start
#iterations: 264
currently lose_sum: 94.67766743898392
time_elpased: 1.377
batch start
#iterations: 265
currently lose_sum: 94.83099341392517
time_elpased: 1.402
batch start
#iterations: 266
currently lose_sum: 94.79830241203308
time_elpased: 1.387
batch start
#iterations: 267
currently lose_sum: 94.95317965745926
time_elpased: 1.399
batch start
#iterations: 268
currently lose_sum: 94.33088481426239
time_elpased: 1.445
batch start
#iterations: 269
currently lose_sum: 94.56810742616653
time_elpased: 1.415
batch start
#iterations: 270
currently lose_sum: 94.4018879532814
time_elpased: 1.397
batch start
#iterations: 271
currently lose_sum: 94.38793677091599
time_elpased: 1.432
batch start
#iterations: 272
currently lose_sum: 94.79495936632156
time_elpased: 1.415
batch start
#iterations: 273
currently lose_sum: 94.63828092813492
time_elpased: 1.445
batch start
#iterations: 274
currently lose_sum: 94.95107585191727
time_elpased: 1.411
batch start
#iterations: 275
currently lose_sum: 94.82165360450745
time_elpased: 1.403
batch start
#iterations: 276
currently lose_sum: 94.27178901433945
time_elpased: 1.402
batch start
#iterations: 277
currently lose_sum: 94.6293894648552
time_elpased: 1.383
batch start
#iterations: 278
currently lose_sum: 94.48386842012405
time_elpased: 1.384
batch start
#iterations: 279
currently lose_sum: 94.58128321170807
time_elpased: 1.397
start validation test
0.665618556701
0.644506084467
0.741278172275
0.689513234098
0.665485724576
60.570
batch start
#iterations: 280
currently lose_sum: 94.45583522319794
time_elpased: 1.409
batch start
#iterations: 281
currently lose_sum: 94.44821214675903
time_elpased: 1.403
batch start
#iterations: 282
currently lose_sum: 94.56589889526367
time_elpased: 1.394
batch start
#iterations: 283
currently lose_sum: 94.47452467679977
time_elpased: 1.415
batch start
#iterations: 284
currently lose_sum: 94.59057575464249
time_elpased: 1.408
batch start
#iterations: 285
currently lose_sum: 94.57672649621964
time_elpased: 1.427
batch start
#iterations: 286
currently lose_sum: 94.26948845386505
time_elpased: 1.388
batch start
#iterations: 287
currently lose_sum: 94.24676960706711
time_elpased: 1.418
batch start
#iterations: 288
currently lose_sum: 93.93323862552643
time_elpased: 1.415
batch start
#iterations: 289
currently lose_sum: 94.80262476205826
time_elpased: 1.432
batch start
#iterations: 290
currently lose_sum: 95.10538750886917
time_elpased: 1.388
batch start
#iterations: 291
currently lose_sum: 94.6035385131836
time_elpased: 1.41
batch start
#iterations: 292
currently lose_sum: 94.3669051527977
time_elpased: 1.413
batch start
#iterations: 293
currently lose_sum: 94.84315323829651
time_elpased: 1.427
batch start
#iterations: 294
currently lose_sum: 94.28635722398758
time_elpased: 1.398
batch start
#iterations: 295
currently lose_sum: 94.84687352180481
time_elpased: 1.406
batch start
#iterations: 296
currently lose_sum: 94.21821135282516
time_elpased: 1.412
batch start
#iterations: 297
currently lose_sum: 94.58965563774109
time_elpased: 1.396
batch start
#iterations: 298
currently lose_sum: 94.84211122989655
time_elpased: 1.443
batch start
#iterations: 299
currently lose_sum: 94.25946766138077
time_elpased: 1.414
start validation test
0.666391752577
0.624836500731
0.835751775239
0.715065598309
0.666094414936
60.618
batch start
#iterations: 300
currently lose_sum: 94.36793094873428
time_elpased: 1.405
batch start
#iterations: 301
currently lose_sum: 94.26526713371277
time_elpased: 1.389
batch start
#iterations: 302
currently lose_sum: 94.51978623867035
time_elpased: 1.418
batch start
#iterations: 303
currently lose_sum: 94.51481103897095
time_elpased: 1.429
batch start
#iterations: 304
currently lose_sum: 94.71035325527191
time_elpased: 1.428
batch start
#iterations: 305
currently lose_sum: 94.86758238077164
time_elpased: 1.379
batch start
#iterations: 306
currently lose_sum: 94.5874160528183
time_elpased: 1.439
batch start
#iterations: 307
currently lose_sum: 94.89926427602768
time_elpased: 1.402
batch start
#iterations: 308
currently lose_sum: 94.32862603664398
time_elpased: 1.412
batch start
#iterations: 309
currently lose_sum: 94.45185375213623
time_elpased: 1.417
batch start
#iterations: 310
currently lose_sum: 94.6955663561821
time_elpased: 1.442
batch start
#iterations: 311
currently lose_sum: 94.0538300871849
time_elpased: 1.413
batch start
#iterations: 312
currently lose_sum: 94.16721814870834
time_elpased: 1.445
batch start
#iterations: 313
currently lose_sum: 94.47156578302383
time_elpased: 1.426
batch start
#iterations: 314
currently lose_sum: 94.87692046165466
time_elpased: 1.42
batch start
#iterations: 315
currently lose_sum: 94.16124391555786
time_elpased: 1.425
batch start
#iterations: 316
currently lose_sum: 94.48075115680695
time_elpased: 1.404
batch start
#iterations: 317
currently lose_sum: 94.68138533830643
time_elpased: 1.42
batch start
#iterations: 318
currently lose_sum: 94.55726444721222
time_elpased: 1.504
batch start
#iterations: 319
currently lose_sum: 94.60645061731339
time_elpased: 1.386
start validation test
0.656855670103
0.657927332783
0.655963774828
0.656944086576
0.656857235963
61.192
batch start
#iterations: 320
currently lose_sum: 94.6370677947998
time_elpased: 1.393
batch start
#iterations: 321
currently lose_sum: 94.68494737148285
time_elpased: 1.435
batch start
#iterations: 322
currently lose_sum: 94.85982996225357
time_elpased: 1.382
batch start
#iterations: 323
currently lose_sum: 94.41358888149261
time_elpased: 1.405
batch start
#iterations: 324
currently lose_sum: 95.05761486291885
time_elpased: 1.402
batch start
#iterations: 325
currently lose_sum: 94.9373517036438
time_elpased: 1.445
batch start
#iterations: 326
currently lose_sum: 94.63449937105179
time_elpased: 1.41
batch start
#iterations: 327
currently lose_sum: 94.1805539727211
time_elpased: 1.405
batch start
#iterations: 328
currently lose_sum: 94.41240608692169
time_elpased: 1.401
batch start
#iterations: 329
currently lose_sum: 94.35272663831711
time_elpased: 1.375
batch start
#iterations: 330
currently lose_sum: 94.40972465276718
time_elpased: 1.398
batch start
#iterations: 331
currently lose_sum: 94.25946825742722
time_elpased: 1.379
batch start
#iterations: 332
currently lose_sum: 94.20215380191803
time_elpased: 1.422
batch start
#iterations: 333
currently lose_sum: 94.05507093667984
time_elpased: 1.42
batch start
#iterations: 334
currently lose_sum: 94.4960333108902
time_elpased: 1.386
batch start
#iterations: 335
currently lose_sum: 94.20422458648682
time_elpased: 1.409
batch start
#iterations: 336
currently lose_sum: 94.0703250169754
time_elpased: 1.373
batch start
#iterations: 337
currently lose_sum: 94.43718838691711
time_elpased: 1.442
batch start
#iterations: 338
currently lose_sum: 94.11759680509567
time_elpased: 1.409
batch start
#iterations: 339
currently lose_sum: 94.65889942646027
time_elpased: 1.423
start validation test
0.670154639175
0.650790765315
0.73685293815
0.691153047927
0.670037540024
60.708
batch start
#iterations: 340
currently lose_sum: 94.43016600608826
time_elpased: 1.416
batch start
#iterations: 341
currently lose_sum: 94.73070359230042
time_elpased: 1.416
batch start
#iterations: 342
currently lose_sum: 94.42251724004745
time_elpased: 1.393
batch start
#iterations: 343
currently lose_sum: 94.13182747364044
time_elpased: 1.4
batch start
#iterations: 344
currently lose_sum: 94.44254434108734
time_elpased: 1.41
batch start
#iterations: 345
currently lose_sum: 94.52247726917267
time_elpased: 1.423
batch start
#iterations: 346
currently lose_sum: 94.4167131781578
time_elpased: 1.418
batch start
#iterations: 347
currently lose_sum: 94.7340921163559
time_elpased: 1.436
batch start
#iterations: 348
currently lose_sum: 94.43499284982681
time_elpased: 1.396
batch start
#iterations: 349
currently lose_sum: 94.52123147249222
time_elpased: 1.371
batch start
#iterations: 350
currently lose_sum: 94.55868738889694
time_elpased: 1.402
batch start
#iterations: 351
currently lose_sum: 94.47179794311523
time_elpased: 1.382
batch start
#iterations: 352
currently lose_sum: 94.35650593042374
time_elpased: 1.394
batch start
#iterations: 353
currently lose_sum: 94.75773668289185
time_elpased: 1.392
batch start
#iterations: 354
currently lose_sum: 94.85903745889664
time_elpased: 1.363
batch start
#iterations: 355
currently lose_sum: 94.91998505592346
time_elpased: 1.435
batch start
#iterations: 356
currently lose_sum: 94.3926522731781
time_elpased: 1.391
batch start
#iterations: 357
currently lose_sum: 94.5421444773674
time_elpased: 1.412
batch start
#iterations: 358
currently lose_sum: 94.88726592063904
time_elpased: 1.397
batch start
#iterations: 359
currently lose_sum: 94.59570533037186
time_elpased: 1.397
start validation test
0.665824742268
0.656352736415
0.698569517341
0.676803429882
0.665767253765
60.902
batch start
#iterations: 360
currently lose_sum: 94.75761884450912
time_elpased: 1.406
batch start
#iterations: 361
currently lose_sum: 94.72876042127609
time_elpased: 1.395
batch start
#iterations: 362
currently lose_sum: 94.17822861671448
time_elpased: 1.389
batch start
#iterations: 363
currently lose_sum: 94.32008111476898
time_elpased: 1.418
batch start
#iterations: 364
currently lose_sum: 94.98841500282288
time_elpased: 1.399
batch start
#iterations: 365
currently lose_sum: 94.42716372013092
time_elpased: 1.42
batch start
#iterations: 366
currently lose_sum: 94.68011027574539
time_elpased: 1.413
batch start
#iterations: 367
currently lose_sum: 94.27202928066254
time_elpased: 1.401
batch start
#iterations: 368
currently lose_sum: 94.52730363607407
time_elpased: 1.445
batch start
#iterations: 369
currently lose_sum: 94.26532590389252
time_elpased: 1.398
batch start
#iterations: 370
currently lose_sum: 94.49430429935455
time_elpased: 1.413
batch start
#iterations: 371
currently lose_sum: 94.11488997936249
time_elpased: 1.41
batch start
#iterations: 372
currently lose_sum: 94.67130941152573
time_elpased: 1.407
batch start
#iterations: 373
currently lose_sum: 94.52599519491196
time_elpased: 1.418
batch start
#iterations: 374
currently lose_sum: 94.2220710515976
time_elpased: 1.435
batch start
#iterations: 375
currently lose_sum: 94.77071189880371
time_elpased: 1.394
batch start
#iterations: 376
currently lose_sum: 94.18231064081192
time_elpased: 1.439
batch start
#iterations: 377
currently lose_sum: 94.14281004667282
time_elpased: 1.401
batch start
#iterations: 378
currently lose_sum: 94.92787313461304
time_elpased: 1.44
batch start
#iterations: 379
currently lose_sum: 94.23062074184418
time_elpased: 1.436
start validation test
0.669381443299
0.633865607522
0.804775136359
0.709168404825
0.669143738788
60.386
batch start
#iterations: 380
currently lose_sum: 94.59239572286606
time_elpased: 1.399
batch start
#iterations: 381
currently lose_sum: 94.19747948646545
time_elpased: 1.443
batch start
#iterations: 382
currently lose_sum: 94.31069445610046
time_elpased: 1.402
batch start
#iterations: 383
currently lose_sum: 94.19568228721619
time_elpased: 1.41
batch start
#iterations: 384
currently lose_sum: 94.16866058111191
time_elpased: 1.403
batch start
#iterations: 385
currently lose_sum: 94.42629528045654
time_elpased: 1.422
batch start
#iterations: 386
currently lose_sum: 94.44445425271988
time_elpased: 1.408
batch start
#iterations: 387
currently lose_sum: 94.0507744550705
time_elpased: 1.437
batch start
#iterations: 388
currently lose_sum: 94.429647564888
time_elpased: 1.409
batch start
#iterations: 389
currently lose_sum: 94.45972722768784
time_elpased: 1.429
batch start
#iterations: 390
currently lose_sum: 94.37660539150238
time_elpased: 1.424
batch start
#iterations: 391
currently lose_sum: 94.52054613828659
time_elpased: 1.521
batch start
#iterations: 392
currently lose_sum: 94.02239543199539
time_elpased: 1.459
batch start
#iterations: 393
currently lose_sum: 94.4133238196373
time_elpased: 1.414
batch start
#iterations: 394
currently lose_sum: 94.06071674823761
time_elpased: 1.401
batch start
#iterations: 395
currently lose_sum: 94.77922278642654
time_elpased: 1.401
batch start
#iterations: 396
currently lose_sum: 94.14654016494751
time_elpased: 1.403
batch start
#iterations: 397
currently lose_sum: 94.14181333780289
time_elpased: 1.454
batch start
#iterations: 398
currently lose_sum: 94.15216720104218
time_elpased: 1.402
batch start
#iterations: 399
currently lose_sum: 94.41761022806168
time_elpased: 1.449
start validation test
0.669226804124
0.644813059505
0.756097560976
0.696035242291
0.669074289111
60.646
acc: 0.671
pre: 0.635
rec: 0.805
F1: 0.710
auc: 0.711
