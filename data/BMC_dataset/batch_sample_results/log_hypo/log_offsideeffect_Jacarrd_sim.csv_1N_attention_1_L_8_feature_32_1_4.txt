start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.797487616539
time_elpased: 1.393
batch start
#iterations: 1
currently lose_sum: 100.08700555562973
time_elpased: 1.393
batch start
#iterations: 2
currently lose_sum: 99.68590354919434
time_elpased: 1.425
batch start
#iterations: 3
currently lose_sum: 99.5744252204895
time_elpased: 1.455
batch start
#iterations: 4
currently lose_sum: 99.57382369041443
time_elpased: 1.403
batch start
#iterations: 5
currently lose_sum: 99.22581195831299
time_elpased: 1.437
batch start
#iterations: 6
currently lose_sum: 98.87463265657425
time_elpased: 1.436
batch start
#iterations: 7
currently lose_sum: 98.71834981441498
time_elpased: 1.405
batch start
#iterations: 8
currently lose_sum: 98.70660561323166
time_elpased: 1.412
batch start
#iterations: 9
currently lose_sum: 98.33318960666656
time_elpased: 1.395
batch start
#iterations: 10
currently lose_sum: 98.1067146062851
time_elpased: 1.457
batch start
#iterations: 11
currently lose_sum: 98.04664844274521
time_elpased: 1.412
batch start
#iterations: 12
currently lose_sum: 97.79114705324173
time_elpased: 1.427
batch start
#iterations: 13
currently lose_sum: 97.98052996397018
time_elpased: 1.432
batch start
#iterations: 14
currently lose_sum: 97.43397337198257
time_elpased: 1.475
batch start
#iterations: 15
currently lose_sum: 97.56625521183014
time_elpased: 1.43
batch start
#iterations: 16
currently lose_sum: 97.49734002351761
time_elpased: 1.428
batch start
#iterations: 17
currently lose_sum: 97.47688692808151
time_elpased: 1.421
batch start
#iterations: 18
currently lose_sum: 96.66898536682129
time_elpased: 1.442
batch start
#iterations: 19
currently lose_sum: 97.3570739030838
time_elpased: 1.423
start validation test
0.653969072165
0.645458067015
0.685911289493
0.665070099287
0.653912992676
62.244
batch start
#iterations: 20
currently lose_sum: 97.30983179807663
time_elpased: 1.397
batch start
#iterations: 21
currently lose_sum: 96.69729161262512
time_elpased: 1.44
batch start
#iterations: 22
currently lose_sum: 97.08962923288345
time_elpased: 1.424
batch start
#iterations: 23
currently lose_sum: 97.35218751430511
time_elpased: 1.416
batch start
#iterations: 24
currently lose_sum: 96.87548303604126
time_elpased: 1.453
batch start
#iterations: 25
currently lose_sum: 96.81284135580063
time_elpased: 1.466
batch start
#iterations: 26
currently lose_sum: 97.15309101343155
time_elpased: 1.395
batch start
#iterations: 27
currently lose_sum: 96.48571711778641
time_elpased: 1.393
batch start
#iterations: 28
currently lose_sum: 96.72365760803223
time_elpased: 1.405
batch start
#iterations: 29
currently lose_sum: 96.50091284513474
time_elpased: 1.425
batch start
#iterations: 30
currently lose_sum: 96.63412427902222
time_elpased: 1.401
batch start
#iterations: 31
currently lose_sum: 96.73596757650375
time_elpased: 1.415
batch start
#iterations: 32
currently lose_sum: 96.47032928466797
time_elpased: 1.399
batch start
#iterations: 33
currently lose_sum: 96.22390872240067
time_elpased: 1.454
batch start
#iterations: 34
currently lose_sum: 96.48483896255493
time_elpased: 1.464
batch start
#iterations: 35
currently lose_sum: 96.17772114276886
time_elpased: 1.46
batch start
#iterations: 36
currently lose_sum: 96.19783627986908
time_elpased: 1.428
batch start
#iterations: 37
currently lose_sum: 96.40199744701385
time_elpased: 1.425
batch start
#iterations: 38
currently lose_sum: 95.85385173559189
time_elpased: 1.405
batch start
#iterations: 39
currently lose_sum: 95.97754180431366
time_elpased: 1.387
start validation test
0.650360824742
0.643682664055
0.67634043429
0.659607567622
0.650315213531
61.741
batch start
#iterations: 40
currently lose_sum: 96.51397317647934
time_elpased: 1.419
batch start
#iterations: 41
currently lose_sum: 95.92918735742569
time_elpased: 1.391
batch start
#iterations: 42
currently lose_sum: 95.71742260456085
time_elpased: 1.427
batch start
#iterations: 43
currently lose_sum: 96.4456102848053
time_elpased: 1.418
batch start
#iterations: 44
currently lose_sum: 96.09930509328842
time_elpased: 1.398
batch start
#iterations: 45
currently lose_sum: 95.79772734642029
time_elpased: 1.399
batch start
#iterations: 46
currently lose_sum: 95.84290039539337
time_elpased: 1.434
batch start
#iterations: 47
currently lose_sum: 96.0930746793747
time_elpased: 1.431
batch start
#iterations: 48
currently lose_sum: 95.78664493560791
time_elpased: 1.39
batch start
#iterations: 49
currently lose_sum: 96.01507622003555
time_elpased: 1.435
batch start
#iterations: 50
currently lose_sum: 95.86814510822296
time_elpased: 1.42
batch start
#iterations: 51
currently lose_sum: 96.01632916927338
time_elpased: 1.408
batch start
#iterations: 52
currently lose_sum: 95.83235323429108
time_elpased: 1.433
batch start
#iterations: 53
currently lose_sum: 95.60787761211395
time_elpased: 1.407
batch start
#iterations: 54
currently lose_sum: 95.79841232299805
time_elpased: 1.405
batch start
#iterations: 55
currently lose_sum: 95.9691390991211
time_elpased: 1.399
batch start
#iterations: 56
currently lose_sum: 95.834468126297
time_elpased: 1.417
batch start
#iterations: 57
currently lose_sum: 95.61262232065201
time_elpased: 1.41
batch start
#iterations: 58
currently lose_sum: 95.71498328447342
time_elpased: 1.41
batch start
#iterations: 59
currently lose_sum: 95.66882365942001
time_elpased: 1.447
start validation test
0.66793814433
0.63901859241
0.774621796851
0.700316337923
0.667750844723
60.955
batch start
#iterations: 60
currently lose_sum: 96.20888328552246
time_elpased: 1.42
batch start
#iterations: 61
currently lose_sum: 96.04221665859222
time_elpased: 1.434
batch start
#iterations: 62
currently lose_sum: 95.76335853338242
time_elpased: 1.423
batch start
#iterations: 63
currently lose_sum: 95.89443641901016
time_elpased: 1.453
batch start
#iterations: 64
currently lose_sum: 96.03426551818848
time_elpased: 1.445
batch start
#iterations: 65
currently lose_sum: 96.02309197187424
time_elpased: 1.389
batch start
#iterations: 66
currently lose_sum: 95.68718010187149
time_elpased: 1.392
batch start
#iterations: 67
currently lose_sum: 95.38649040460587
time_elpased: 1.405
batch start
#iterations: 68
currently lose_sum: 95.51979178190231
time_elpased: 1.401
batch start
#iterations: 69
currently lose_sum: 95.75147098302841
time_elpased: 1.415
batch start
#iterations: 70
currently lose_sum: 95.65997588634491
time_elpased: 1.391
batch start
#iterations: 71
currently lose_sum: 95.64664053916931
time_elpased: 1.41
batch start
#iterations: 72
currently lose_sum: 95.6959617137909
time_elpased: 1.4
batch start
#iterations: 73
currently lose_sum: 95.37579852342606
time_elpased: 1.435
batch start
#iterations: 74
currently lose_sum: 95.54326498508453
time_elpased: 1.437
batch start
#iterations: 75
currently lose_sum: 95.50858861207962
time_elpased: 1.446
batch start
#iterations: 76
currently lose_sum: 95.23825293779373
time_elpased: 1.467
batch start
#iterations: 77
currently lose_sum: 95.43832278251648
time_elpased: 1.418
batch start
#iterations: 78
currently lose_sum: 95.59380573034286
time_elpased: 1.434
batch start
#iterations: 79
currently lose_sum: 95.40015894174576
time_elpased: 1.446
start validation test
0.659948453608
0.649568552253
0.697231655861
0.672556708195
0.659882997196
61.022
batch start
#iterations: 80
currently lose_sum: 95.30845564603806
time_elpased: 1.427
batch start
#iterations: 81
currently lose_sum: 95.2798136472702
time_elpased: 1.426
batch start
#iterations: 82
currently lose_sum: 95.43480616807938
time_elpased: 1.409
batch start
#iterations: 83
currently lose_sum: 95.37437129020691
time_elpased: 1.403
batch start
#iterations: 84
currently lose_sum: 95.83807468414307
time_elpased: 1.396
batch start
#iterations: 85
currently lose_sum: 95.4105761051178
time_elpased: 1.413
batch start
#iterations: 86
currently lose_sum: 95.50164598226547
time_elpased: 1.45
batch start
#iterations: 87
currently lose_sum: 95.17775279283524
time_elpased: 1.422
batch start
#iterations: 88
currently lose_sum: 95.50812804698944
time_elpased: 1.435
batch start
#iterations: 89
currently lose_sum: 95.70741617679596
time_elpased: 1.446
batch start
#iterations: 90
currently lose_sum: 94.96946030855179
time_elpased: 1.424
batch start
#iterations: 91
currently lose_sum: 95.59219896793365
time_elpased: 1.387
batch start
#iterations: 92
currently lose_sum: 95.29639214277267
time_elpased: 1.391
batch start
#iterations: 93
currently lose_sum: 94.82804721593857
time_elpased: 1.434
batch start
#iterations: 94
currently lose_sum: 95.3980523943901
time_elpased: 1.399
batch start
#iterations: 95
currently lose_sum: 95.78554081916809
time_elpased: 1.409
batch start
#iterations: 96
currently lose_sum: 95.53195607662201
time_elpased: 1.476
batch start
#iterations: 97
currently lose_sum: 95.00389873981476
time_elpased: 1.421
batch start
#iterations: 98
currently lose_sum: 95.67733931541443
time_elpased: 1.423
batch start
#iterations: 99
currently lose_sum: 95.52773976325989
time_elpased: 1.412
start validation test
0.662010309278
0.657873701039
0.677575383349
0.667579214195
0.66198298239
60.937
batch start
#iterations: 100
currently lose_sum: 95.25858956575394
time_elpased: 1.419
batch start
#iterations: 101
currently lose_sum: 95.24049496650696
time_elpased: 1.42
batch start
#iterations: 102
currently lose_sum: 95.0173259973526
time_elpased: 1.406
batch start
#iterations: 103
currently lose_sum: 95.64149379730225
time_elpased: 1.401
batch start
#iterations: 104
currently lose_sum: 94.83603000640869
time_elpased: 1.428
batch start
#iterations: 105
currently lose_sum: 95.59560453891754
time_elpased: 1.441
batch start
#iterations: 106
currently lose_sum: 95.33810466527939
time_elpased: 1.435
batch start
#iterations: 107
currently lose_sum: 95.58428508043289
time_elpased: 1.437
batch start
#iterations: 108
currently lose_sum: 95.33262479305267
time_elpased: 1.437
batch start
#iterations: 109
currently lose_sum: 95.36838799715042
time_elpased: 1.43
batch start
#iterations: 110
currently lose_sum: 95.30496454238892
time_elpased: 1.433
batch start
#iterations: 111
currently lose_sum: 95.47019249200821
time_elpased: 1.457
batch start
#iterations: 112
currently lose_sum: 95.0722000002861
time_elpased: 1.443
batch start
#iterations: 113
currently lose_sum: 94.56316143274307
time_elpased: 1.416
batch start
#iterations: 114
currently lose_sum: 95.04811650514603
time_elpased: 1.482
batch start
#iterations: 115
currently lose_sum: 95.28352689743042
time_elpased: 1.401
batch start
#iterations: 116
currently lose_sum: 95.12134665250778
time_elpased: 1.419
batch start
#iterations: 117
currently lose_sum: 95.09979248046875
time_elpased: 1.401
batch start
#iterations: 118
currently lose_sum: 95.03815484046936
time_elpased: 1.461
batch start
#iterations: 119
currently lose_sum: 95.04900884628296
time_elpased: 1.441
start validation test
0.671237113402
0.636096845194
0.803025625193
0.709879912664
0.671005738343
60.447
batch start
#iterations: 120
currently lose_sum: 94.6346765756607
time_elpased: 1.459
batch start
#iterations: 121
currently lose_sum: 95.49313694238663
time_elpased: 1.411
batch start
#iterations: 122
currently lose_sum: 95.24679374694824
time_elpased: 1.397
batch start
#iterations: 123
currently lose_sum: 95.01756989955902
time_elpased: 1.438
batch start
#iterations: 124
currently lose_sum: 94.89823651313782
time_elpased: 1.426
batch start
#iterations: 125
currently lose_sum: 94.75943577289581
time_elpased: 1.45
batch start
#iterations: 126
currently lose_sum: 95.09015476703644
time_elpased: 1.433
batch start
#iterations: 127
currently lose_sum: 95.5005972981453
time_elpased: 1.418
batch start
#iterations: 128
currently lose_sum: 94.94256752729416
time_elpased: 1.433
batch start
#iterations: 129
currently lose_sum: 94.98618334531784
time_elpased: 1.408
batch start
#iterations: 130
currently lose_sum: 95.08532291650772
time_elpased: 1.45
batch start
#iterations: 131
currently lose_sum: 95.39680045843124
time_elpased: 1.412
batch start
#iterations: 132
currently lose_sum: 95.26031774282455
time_elpased: 1.446
batch start
#iterations: 133
currently lose_sum: 94.81074887514114
time_elpased: 1.444
batch start
#iterations: 134
currently lose_sum: 94.86882889270782
time_elpased: 1.412
batch start
#iterations: 135
currently lose_sum: 95.11270320415497
time_elpased: 1.404
batch start
#iterations: 136
currently lose_sum: 94.97978627681732
time_elpased: 1.441
batch start
#iterations: 137
currently lose_sum: 95.1458495259285
time_elpased: 1.394
batch start
#iterations: 138
currently lose_sum: 95.0388571023941
time_elpased: 1.423
batch start
#iterations: 139
currently lose_sum: 95.01368594169617
time_elpased: 1.462
start validation test
0.661597938144
0.642161284503
0.732633528867
0.684420516272
0.661473224208
60.601
batch start
#iterations: 140
currently lose_sum: 95.02004784345627
time_elpased: 1.428
batch start
#iterations: 141
currently lose_sum: 94.93910539150238
time_elpased: 1.401
batch start
#iterations: 142
currently lose_sum: 95.11050820350647
time_elpased: 1.428
batch start
#iterations: 143
currently lose_sum: 94.96702861785889
time_elpased: 1.411
batch start
#iterations: 144
currently lose_sum: 94.95889514684677
time_elpased: 1.416
batch start
#iterations: 145
currently lose_sum: 95.220179438591
time_elpased: 1.41
batch start
#iterations: 146
currently lose_sum: 95.14791488647461
time_elpased: 1.421
batch start
#iterations: 147
currently lose_sum: 94.94884043931961
time_elpased: 1.433
batch start
#iterations: 148
currently lose_sum: 95.0464369058609
time_elpased: 1.452
batch start
#iterations: 149
currently lose_sum: 95.00525242090225
time_elpased: 1.447
batch start
#iterations: 150
currently lose_sum: 94.8475701212883
time_elpased: 1.406
batch start
#iterations: 151
currently lose_sum: 95.07143902778625
time_elpased: 1.426
batch start
#iterations: 152
currently lose_sum: 94.81125491857529
time_elpased: 1.398
batch start
#iterations: 153
currently lose_sum: 95.26581501960754
time_elpased: 1.417
batch start
#iterations: 154
currently lose_sum: 94.68154966831207
time_elpased: 1.432
batch start
#iterations: 155
currently lose_sum: 94.95749348402023
time_elpased: 1.42
batch start
#iterations: 156
currently lose_sum: 95.09888225793839
time_elpased: 1.395
batch start
#iterations: 157
currently lose_sum: 94.75837063789368
time_elpased: 1.431
batch start
#iterations: 158
currently lose_sum: 95.19067972898483
time_elpased: 1.402
batch start
#iterations: 159
currently lose_sum: 95.2426318526268
time_elpased: 1.418
start validation test
0.672371134021
0.637105327568
0.803643099722
0.710749067079
0.672140665838
60.161
batch start
#iterations: 160
currently lose_sum: 95.04318845272064
time_elpased: 1.403
batch start
#iterations: 161
currently lose_sum: 95.00772321224213
time_elpased: 1.42
batch start
#iterations: 162
currently lose_sum: 94.9108218550682
time_elpased: 1.435
batch start
#iterations: 163
currently lose_sum: 94.53547048568726
time_elpased: 1.474
batch start
#iterations: 164
currently lose_sum: 95.07363259792328
time_elpased: 1.424
batch start
#iterations: 165
currently lose_sum: 94.79987132549286
time_elpased: 1.434
batch start
#iterations: 166
currently lose_sum: 94.72366517782211
time_elpased: 1.42
batch start
#iterations: 167
currently lose_sum: 95.27719616889954
time_elpased: 1.422
batch start
#iterations: 168
currently lose_sum: 94.73491388559341
time_elpased: 1.407
batch start
#iterations: 169
currently lose_sum: 94.89238113164902
time_elpased: 1.422
batch start
#iterations: 170
currently lose_sum: 94.41769170761108
time_elpased: 1.435
batch start
#iterations: 171
currently lose_sum: 94.84946209192276
time_elpased: 1.414
batch start
#iterations: 172
currently lose_sum: 94.84266489744186
time_elpased: 1.438
batch start
#iterations: 173
currently lose_sum: 94.68290936946869
time_elpased: 1.497
batch start
#iterations: 174
currently lose_sum: 94.63263136148453
time_elpased: 1.404
batch start
#iterations: 175
currently lose_sum: 94.76877868175507
time_elpased: 1.446
batch start
#iterations: 176
currently lose_sum: 95.10926604270935
time_elpased: 1.41
batch start
#iterations: 177
currently lose_sum: 94.93529719114304
time_elpased: 1.417
batch start
#iterations: 178
currently lose_sum: 94.47829842567444
time_elpased: 1.446
batch start
#iterations: 179
currently lose_sum: 94.80635386705399
time_elpased: 1.411
start validation test
0.667783505155
0.627097576134
0.830709066584
0.714684138297
0.667497464202
60.349
batch start
#iterations: 180
currently lose_sum: 94.93059808015823
time_elpased: 1.435
batch start
#iterations: 181
currently lose_sum: 94.89310348033905
time_elpased: 1.423
batch start
#iterations: 182
currently lose_sum: 94.7247062921524
time_elpased: 1.412
batch start
#iterations: 183
currently lose_sum: 94.82500725984573
time_elpased: 1.424
batch start
#iterations: 184
currently lose_sum: 94.94577151536942
time_elpased: 1.434
batch start
#iterations: 185
currently lose_sum: 94.9572007060051
time_elpased: 1.435
batch start
#iterations: 186
currently lose_sum: 94.39594048261642
time_elpased: 1.475
batch start
#iterations: 187
currently lose_sum: 94.55598610639572
time_elpased: 1.41
batch start
#iterations: 188
currently lose_sum: 94.5525141954422
time_elpased: 1.453
batch start
#iterations: 189
currently lose_sum: 94.9229519367218
time_elpased: 1.401
batch start
#iterations: 190
currently lose_sum: 94.3807864189148
time_elpased: 1.418
batch start
#iterations: 191
currently lose_sum: 94.80002629756927
time_elpased: 1.462
batch start
#iterations: 192
currently lose_sum: 94.84885936975479
time_elpased: 1.4
batch start
#iterations: 193
currently lose_sum: 94.67567527294159
time_elpased: 1.441
batch start
#iterations: 194
currently lose_sum: 94.69972813129425
time_elpased: 1.4
batch start
#iterations: 195
currently lose_sum: 94.80940234661102
time_elpased: 1.44
batch start
#iterations: 196
currently lose_sum: 94.35832804441452
time_elpased: 1.417
batch start
#iterations: 197
currently lose_sum: 95.03851240873337
time_elpased: 1.425
batch start
#iterations: 198
currently lose_sum: 94.58079260587692
time_elpased: 1.409
batch start
#iterations: 199
currently lose_sum: 94.69584465026855
time_elpased: 1.428
start validation test
0.675927835052
0.642252820173
0.796850879901
0.711247875809
0.675715535995
60.143
batch start
#iterations: 200
currently lose_sum: 94.61193281412125
time_elpased: 1.405
batch start
#iterations: 201
currently lose_sum: 94.69702559709549
time_elpased: 1.413
batch start
#iterations: 202
currently lose_sum: 94.68516051769257
time_elpased: 1.392
batch start
#iterations: 203
currently lose_sum: 95.04693698883057
time_elpased: 1.439
batch start
#iterations: 204
currently lose_sum: 94.60066676139832
time_elpased: 1.41
batch start
#iterations: 205
currently lose_sum: 94.39100784063339
time_elpased: 1.448
batch start
#iterations: 206
currently lose_sum: 94.82219177484512
time_elpased: 1.414
batch start
#iterations: 207
currently lose_sum: 94.58135050535202
time_elpased: 1.417
batch start
#iterations: 208
currently lose_sum: 94.5342760682106
time_elpased: 1.399
batch start
#iterations: 209
currently lose_sum: 94.57275879383087
time_elpased: 1.442
batch start
#iterations: 210
currently lose_sum: 94.50045669078827
time_elpased: 1.407
batch start
#iterations: 211
currently lose_sum: 94.49019134044647
time_elpased: 1.443
batch start
#iterations: 212
currently lose_sum: 94.68829971551895
time_elpased: 1.424
batch start
#iterations: 213
currently lose_sum: 94.63282012939453
time_elpased: 1.457
batch start
#iterations: 214
currently lose_sum: 94.66216713190079
time_elpased: 1.393
batch start
#iterations: 215
currently lose_sum: 94.16666162014008
time_elpased: 1.425
batch start
#iterations: 216
currently lose_sum: 94.40775746107101
time_elpased: 1.412
batch start
#iterations: 217
currently lose_sum: 94.95685291290283
time_elpased: 1.441
batch start
#iterations: 218
currently lose_sum: 94.38552838563919
time_elpased: 1.414
batch start
#iterations: 219
currently lose_sum: 94.49842417240143
time_elpased: 1.465
start validation test
0.672474226804
0.638794882377
0.796439230215
0.70895932576
0.67225658712
60.099
batch start
#iterations: 220
currently lose_sum: 94.58603113889694
time_elpased: 1.407
batch start
#iterations: 221
currently lose_sum: 94.53644996881485
time_elpased: 1.465
batch start
#iterations: 222
currently lose_sum: 94.6776493191719
time_elpased: 1.42
batch start
#iterations: 223
currently lose_sum: 94.67661303281784
time_elpased: 1.438
batch start
#iterations: 224
currently lose_sum: 94.84773069620132
time_elpased: 1.393
batch start
#iterations: 225
currently lose_sum: 94.55308872461319
time_elpased: 1.42
batch start
#iterations: 226
currently lose_sum: 94.50861692428589
time_elpased: 1.435
batch start
#iterations: 227
currently lose_sum: 94.36089438199997
time_elpased: 1.433
batch start
#iterations: 228
currently lose_sum: 94.78358900547028
time_elpased: 1.414
batch start
#iterations: 229
currently lose_sum: 94.4752082824707
time_elpased: 1.431
batch start
#iterations: 230
currently lose_sum: 94.80328845977783
time_elpased: 1.43
batch start
#iterations: 231
currently lose_sum: 94.68365454673767
time_elpased: 1.424
batch start
#iterations: 232
currently lose_sum: 94.60888451337814
time_elpased: 1.401
batch start
#iterations: 233
currently lose_sum: 94.3388135433197
time_elpased: 1.406
batch start
#iterations: 234
currently lose_sum: 94.38014775514603
time_elpased: 1.415
batch start
#iterations: 235
currently lose_sum: 94.7455153465271
time_elpased: 1.415
batch start
#iterations: 236
currently lose_sum: 94.46885496377945
time_elpased: 1.408
batch start
#iterations: 237
currently lose_sum: 94.75367945432663
time_elpased: 1.428
batch start
#iterations: 238
currently lose_sum: 94.55612748861313
time_elpased: 1.457
batch start
#iterations: 239
currently lose_sum: 94.26330155134201
time_elpased: 1.431
start validation test
0.666855670103
0.627989301447
0.821549861068
0.711846270453
0.666584080591
60.073
batch start
#iterations: 240
currently lose_sum: 94.80036759376526
time_elpased: 1.399
batch start
#iterations: 241
currently lose_sum: 95.22104281187057
time_elpased: 1.432
batch start
#iterations: 242
currently lose_sum: 94.11521375179291
time_elpased: 1.383
batch start
#iterations: 243
currently lose_sum: 94.7935289144516
time_elpased: 1.461
batch start
#iterations: 244
currently lose_sum: 94.62793666124344
time_elpased: 1.41
batch start
#iterations: 245
currently lose_sum: 94.22513616085052
time_elpased: 1.431
batch start
#iterations: 246
currently lose_sum: 94.80944073200226
time_elpased: 1.407
batch start
#iterations: 247
currently lose_sum: 94.67673516273499
time_elpased: 1.432
batch start
#iterations: 248
currently lose_sum: 94.71485489606857
time_elpased: 1.413
batch start
#iterations: 249
currently lose_sum: 94.62185841798782
time_elpased: 1.432
batch start
#iterations: 250
currently lose_sum: 94.48115599155426
time_elpased: 1.461
batch start
#iterations: 251
currently lose_sum: 94.69905287027359
time_elpased: 1.456
batch start
#iterations: 252
currently lose_sum: 94.3346819281578
time_elpased: 1.423
batch start
#iterations: 253
currently lose_sum: 94.42429691553116
time_elpased: 1.439
batch start
#iterations: 254
currently lose_sum: 94.36832046508789
time_elpased: 1.429
batch start
#iterations: 255
currently lose_sum: 94.81464928388596
time_elpased: 1.434
batch start
#iterations: 256
currently lose_sum: 93.91582894325256
time_elpased: 1.417
batch start
#iterations: 257
currently lose_sum: 94.80749344825745
time_elpased: 1.414
batch start
#iterations: 258
currently lose_sum: 94.37713176012039
time_elpased: 1.399
batch start
#iterations: 259
currently lose_sum: 94.31450229883194
time_elpased: 1.422
start validation test
0.662319587629
0.658808186196
0.675825872183
0.667208534417
0.662295875263
60.592
batch start
#iterations: 260
currently lose_sum: 94.49636036157608
time_elpased: 1.417
batch start
#iterations: 261
currently lose_sum: 94.3802262544632
time_elpased: 1.419
batch start
#iterations: 262
currently lose_sum: 94.44689399003983
time_elpased: 1.41
batch start
#iterations: 263
currently lose_sum: 94.54079884290695
time_elpased: 1.407
batch start
#iterations: 264
currently lose_sum: 94.60207343101501
time_elpased: 1.428
batch start
#iterations: 265
currently lose_sum: 94.72393852472305
time_elpased: 1.455
batch start
#iterations: 266
currently lose_sum: 94.62189793586731
time_elpased: 1.377
batch start
#iterations: 267
currently lose_sum: 94.7989889383316
time_elpased: 1.435
batch start
#iterations: 268
currently lose_sum: 94.46338123083115
time_elpased: 1.4
batch start
#iterations: 269
currently lose_sum: 94.694042801857
time_elpased: 1.424
batch start
#iterations: 270
currently lose_sum: 94.53322154283524
time_elpased: 1.419
batch start
#iterations: 271
currently lose_sum: 94.29725658893585
time_elpased: 1.425
batch start
#iterations: 272
currently lose_sum: 94.50849878787994
time_elpased: 1.417
batch start
#iterations: 273
currently lose_sum: 94.33948796987534
time_elpased: 1.409
batch start
#iterations: 274
currently lose_sum: 95.0532101392746
time_elpased: 1.474
batch start
#iterations: 275
currently lose_sum: 94.368727684021
time_elpased: 1.417
batch start
#iterations: 276
currently lose_sum: 94.33900338411331
time_elpased: 1.414
batch start
#iterations: 277
currently lose_sum: 94.5512022972107
time_elpased: 1.431
batch start
#iterations: 278
currently lose_sum: 94.60393673181534
time_elpased: 1.434
batch start
#iterations: 279
currently lose_sum: 94.36425471305847
time_elpased: 1.445
start validation test
0.670463917526
0.648844707147
0.74560049398
0.693865823876
0.670332003677
60.197
batch start
#iterations: 280
currently lose_sum: 94.69761157035828
time_elpased: 1.419
batch start
#iterations: 281
currently lose_sum: 94.21865975856781
time_elpased: 1.406
batch start
#iterations: 282
currently lose_sum: 94.33855944871902
time_elpased: 1.421
batch start
#iterations: 283
currently lose_sum: 94.26924049854279
time_elpased: 1.434
batch start
#iterations: 284
currently lose_sum: 94.31654524803162
time_elpased: 1.421
batch start
#iterations: 285
currently lose_sum: 94.18024760484695
time_elpased: 1.428
batch start
#iterations: 286
currently lose_sum: 94.31989473104477
time_elpased: 1.393
batch start
#iterations: 287
currently lose_sum: 94.28252649307251
time_elpased: 1.449
batch start
#iterations: 288
currently lose_sum: 94.77164369821548
time_elpased: 1.402
batch start
#iterations: 289
currently lose_sum: 94.21255123615265
time_elpased: 1.457
batch start
#iterations: 290
currently lose_sum: 94.18892639875412
time_elpased: 1.407
batch start
#iterations: 291
currently lose_sum: 93.95586550235748
time_elpased: 1.422
batch start
#iterations: 292
currently lose_sum: 94.44380730390549
time_elpased: 1.402
batch start
#iterations: 293
currently lose_sum: 94.19729363918304
time_elpased: 1.435
batch start
#iterations: 294
currently lose_sum: 94.05652487277985
time_elpased: 1.427
batch start
#iterations: 295
currently lose_sum: 94.00227552652359
time_elpased: 1.419
batch start
#iterations: 296
currently lose_sum: 94.00767403841019
time_elpased: 1.413
batch start
#iterations: 297
currently lose_sum: 94.40618830919266
time_elpased: 1.441
batch start
#iterations: 298
currently lose_sum: 94.22759646177292
time_elpased: 1.415
batch start
#iterations: 299
currently lose_sum: 94.26341116428375
time_elpased: 1.405
start validation test
0.668144329897
0.645694481472
0.747761654832
0.692989985694
0.668004549403
60.062
batch start
#iterations: 300
currently lose_sum: 94.10172927379608
time_elpased: 1.418
batch start
#iterations: 301
currently lose_sum: 94.12989842891693
time_elpased: 1.455
batch start
#iterations: 302
currently lose_sum: 94.47206968069077
time_elpased: 1.436
batch start
#iterations: 303
currently lose_sum: 94.12293881177902
time_elpased: 1.442
batch start
#iterations: 304
currently lose_sum: 94.46753787994385
time_elpased: 1.427
batch start
#iterations: 305
currently lose_sum: 94.26622718572617
time_elpased: 1.44
batch start
#iterations: 306
currently lose_sum: 94.30768805742264
time_elpased: 1.428
batch start
#iterations: 307
currently lose_sum: 94.46301585435867
time_elpased: 1.418
batch start
#iterations: 308
currently lose_sum: 94.31796330213547
time_elpased: 1.425
batch start
#iterations: 309
currently lose_sum: 94.62080979347229
time_elpased: 1.464
batch start
#iterations: 310
currently lose_sum: 94.6713570356369
time_elpased: 1.399
batch start
#iterations: 311
currently lose_sum: 94.00539088249207
time_elpased: 1.438
batch start
#iterations: 312
currently lose_sum: 94.32336896657944
time_elpased: 1.408
batch start
#iterations: 313
currently lose_sum: 94.06638258695602
time_elpased: 1.504
batch start
#iterations: 314
currently lose_sum: 94.06070464849472
time_elpased: 1.435
batch start
#iterations: 315
currently lose_sum: 93.9900279045105
time_elpased: 1.417
batch start
#iterations: 316
currently lose_sum: 94.14301413297653
time_elpased: 1.426
batch start
#iterations: 317
currently lose_sum: 94.38457030057907
time_elpased: 1.413
batch start
#iterations: 318
currently lose_sum: 94.31753486394882
time_elpased: 1.412
batch start
#iterations: 319
currently lose_sum: 94.06230789422989
time_elpased: 1.429
start validation test
0.66912371134
0.644750702247
0.755891736133
0.695911696433
0.66897137669
60.384
batch start
#iterations: 320
currently lose_sum: 94.39996373653412
time_elpased: 1.44
batch start
#iterations: 321
currently lose_sum: 94.00879180431366
time_elpased: 1.424
batch start
#iterations: 322
currently lose_sum: 94.60475701093674
time_elpased: 1.414
batch start
#iterations: 323
currently lose_sum: 94.37587374448776
time_elpased: 1.466
batch start
#iterations: 324
currently lose_sum: 94.4245970249176
time_elpased: 1.417
batch start
#iterations: 325
currently lose_sum: 94.4132616519928
time_elpased: 1.447
batch start
#iterations: 326
currently lose_sum: 94.54622226953506
time_elpased: 1.404
batch start
#iterations: 327
currently lose_sum: 94.09739923477173
time_elpased: 1.434
batch start
#iterations: 328
currently lose_sum: 94.3178146481514
time_elpased: 1.398
batch start
#iterations: 329
currently lose_sum: 94.08531892299652
time_elpased: 1.402
batch start
#iterations: 330
currently lose_sum: 94.39550560712814
time_elpased: 1.456
batch start
#iterations: 331
currently lose_sum: 94.7386412024498
time_elpased: 1.442
batch start
#iterations: 332
currently lose_sum: 94.32286942005157
time_elpased: 1.405
batch start
#iterations: 333
currently lose_sum: 94.11068850755692
time_elpased: 1.421
batch start
#iterations: 334
currently lose_sum: 94.71845990419388
time_elpased: 1.421
batch start
#iterations: 335
currently lose_sum: 94.10409164428711
time_elpased: 1.457
batch start
#iterations: 336
currently lose_sum: 94.45647084712982
time_elpased: 1.421
batch start
#iterations: 337
currently lose_sum: 94.33487063646317
time_elpased: 1.411
batch start
#iterations: 338
currently lose_sum: 94.06358963251114
time_elpased: 1.419
batch start
#iterations: 339
currently lose_sum: 94.2937536239624
time_elpased: 1.432
start validation test
0.669896907216
0.641375778783
0.773386847793
0.701222357003
0.669715214664
60.314
batch start
#iterations: 340
currently lose_sum: 94.00532621145248
time_elpased: 1.401
batch start
#iterations: 341
currently lose_sum: 94.85662317276001
time_elpased: 1.422
batch start
#iterations: 342
currently lose_sum: 94.19058799743652
time_elpased: 1.416
batch start
#iterations: 343
currently lose_sum: 94.09252852201462
time_elpased: 1.433
batch start
#iterations: 344
currently lose_sum: 94.20010161399841
time_elpased: 1.405
batch start
#iterations: 345
currently lose_sum: 94.44259375333786
time_elpased: 1.417
batch start
#iterations: 346
currently lose_sum: 93.92550194263458
time_elpased: 1.405
batch start
#iterations: 347
currently lose_sum: 93.92313104867935
time_elpased: 1.435
batch start
#iterations: 348
currently lose_sum: 94.51263105869293
time_elpased: 1.383
batch start
#iterations: 349
currently lose_sum: 94.2701427936554
time_elpased: 1.424
batch start
#iterations: 350
currently lose_sum: 94.56067776679993
time_elpased: 1.42
batch start
#iterations: 351
currently lose_sum: 94.50298172235489
time_elpased: 1.45
batch start
#iterations: 352
currently lose_sum: 93.89221113920212
time_elpased: 1.397
batch start
#iterations: 353
currently lose_sum: 94.23534798622131
time_elpased: 1.439
batch start
#iterations: 354
currently lose_sum: 93.94221645593643
time_elpased: 1.4
batch start
#iterations: 355
currently lose_sum: 94.03046476840973
time_elpased: 1.434
batch start
#iterations: 356
currently lose_sum: 94.4248029589653
time_elpased: 1.401
batch start
#iterations: 357
currently lose_sum: 93.98324930667877
time_elpased: 1.434
batch start
#iterations: 358
currently lose_sum: 94.31473809480667
time_elpased: 1.426
batch start
#iterations: 359
currently lose_sum: 94.17289537191391
time_elpased: 1.417
start validation test
0.672731958763
0.641894169194
0.78398682721
0.705860551309
0.672536633681
60.048
batch start
#iterations: 360
currently lose_sum: 94.50660228729248
time_elpased: 1.402
batch start
#iterations: 361
currently lose_sum: 94.40561437606812
time_elpased: 1.414
batch start
#iterations: 362
currently lose_sum: 94.38022071123123
time_elpased: 1.414
batch start
#iterations: 363
currently lose_sum: 94.12533956766129
time_elpased: 1.411
batch start
#iterations: 364
currently lose_sum: 94.40395498275757
time_elpased: 1.386
batch start
#iterations: 365
currently lose_sum: 94.27709913253784
time_elpased: 1.449
batch start
#iterations: 366
currently lose_sum: 94.45888167619705
time_elpased: 1.393
batch start
#iterations: 367
currently lose_sum: 94.1700006723404
time_elpased: 1.454
batch start
#iterations: 368
currently lose_sum: 94.13982027769089
time_elpased: 1.403
batch start
#iterations: 369
currently lose_sum: 94.37211591005325
time_elpased: 1.401
batch start
#iterations: 370
currently lose_sum: 94.31249326467514
time_elpased: 1.422
batch start
#iterations: 371
currently lose_sum: 93.92869979143143
time_elpased: 1.414
batch start
#iterations: 372
currently lose_sum: 94.15795123577118
time_elpased: 1.419
batch start
#iterations: 373
currently lose_sum: 94.40521937608719
time_elpased: 1.424
batch start
#iterations: 374
currently lose_sum: 94.2901023030281
time_elpased: 1.413
batch start
#iterations: 375
currently lose_sum: 94.34827572107315
time_elpased: 1.42
batch start
#iterations: 376
currently lose_sum: 94.5450536608696
time_elpased: 1.417
batch start
#iterations: 377
currently lose_sum: 94.47993087768555
time_elpased: 1.43
batch start
#iterations: 378
currently lose_sum: 94.07723879814148
time_elpased: 1.4
batch start
#iterations: 379
currently lose_sum: 94.38476347923279
time_elpased: 1.429
start validation test
0.669020618557
0.650255288111
0.733971390347
0.689581822577
0.668906587459
60.206
batch start
#iterations: 380
currently lose_sum: 94.55984371900558
time_elpased: 1.397
batch start
#iterations: 381
currently lose_sum: 94.49585783481598
time_elpased: 1.41
batch start
#iterations: 382
currently lose_sum: 93.9120991230011
time_elpased: 1.415
batch start
#iterations: 383
currently lose_sum: 93.81720197200775
time_elpased: 1.416
batch start
#iterations: 384
currently lose_sum: 94.21945852041245
time_elpased: 1.432
batch start
#iterations: 385
currently lose_sum: 94.60366499423981
time_elpased: 1.441
batch start
#iterations: 386
currently lose_sum: 94.07311218976974
time_elpased: 1.428
batch start
#iterations: 387
currently lose_sum: 94.26842164993286
time_elpased: 1.418
batch start
#iterations: 388
currently lose_sum: 94.54896652698517
time_elpased: 1.428
batch start
#iterations: 389
currently lose_sum: 94.43648743629456
time_elpased: 1.432
batch start
#iterations: 390
currently lose_sum: 94.28429192304611
time_elpased: 1.422
batch start
#iterations: 391
currently lose_sum: 94.0622045993805
time_elpased: 1.394
batch start
#iterations: 392
currently lose_sum: 94.17835205793381
time_elpased: 1.42
batch start
#iterations: 393
currently lose_sum: 94.27387183904648
time_elpased: 1.422
batch start
#iterations: 394
currently lose_sum: 94.12089449167252
time_elpased: 1.429
batch start
#iterations: 395
currently lose_sum: 94.09727245569229
time_elpased: 1.424
batch start
#iterations: 396
currently lose_sum: 94.03560519218445
time_elpased: 1.425
batch start
#iterations: 397
currently lose_sum: 94.53748339414597
time_elpased: 1.428
batch start
#iterations: 398
currently lose_sum: 93.92168891429901
time_elpased: 1.404
batch start
#iterations: 399
currently lose_sum: 93.72795224189758
time_elpased: 1.433
start validation test
0.666958762887
0.654254311162
0.71061027066
0.681268807656
0.666882125932
60.158
acc: 0.669
pre: 0.638
rec: 0.781
F1: 0.702
auc: 0.703
