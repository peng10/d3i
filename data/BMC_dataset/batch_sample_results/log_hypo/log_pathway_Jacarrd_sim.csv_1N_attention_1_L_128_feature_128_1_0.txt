start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.82758736610413
time_elpased: 1.931
batch start
#iterations: 1
currently lose_sum: 100.342360496521
time_elpased: 1.942
batch start
#iterations: 2
currently lose_sum: 100.48400604724884
time_elpased: 1.897
batch start
#iterations: 3
currently lose_sum: 99.82370716333389
time_elpased: 1.959
batch start
#iterations: 4
currently lose_sum: 99.90066647529602
time_elpased: 1.946
batch start
#iterations: 5
currently lose_sum: 99.77029240131378
time_elpased: 1.908
batch start
#iterations: 6
currently lose_sum: 99.48589825630188
time_elpased: 1.908
batch start
#iterations: 7
currently lose_sum: 99.48192119598389
time_elpased: 1.901
batch start
#iterations: 8
currently lose_sum: 99.13168823719025
time_elpased: 1.909
batch start
#iterations: 9
currently lose_sum: 99.2398219704628
time_elpased: 1.967
batch start
#iterations: 10
currently lose_sum: 99.24282497167587
time_elpased: 1.943
batch start
#iterations: 11
currently lose_sum: 99.09331768751144
time_elpased: 1.922
batch start
#iterations: 12
currently lose_sum: 98.90977245569229
time_elpased: 1.958
batch start
#iterations: 13
currently lose_sum: 98.4690717458725
time_elpased: 1.889
batch start
#iterations: 14
currently lose_sum: 98.82779717445374
time_elpased: 1.911
batch start
#iterations: 15
currently lose_sum: 98.70983618497849
time_elpased: 1.953
batch start
#iterations: 16
currently lose_sum: 98.63249385356903
time_elpased: 1.896
batch start
#iterations: 17
currently lose_sum: 98.42497479915619
time_elpased: 1.895
batch start
#iterations: 18
currently lose_sum: 98.30243319272995
time_elpased: 1.864
batch start
#iterations: 19
currently lose_sum: 98.23563677072525
time_elpased: 1.965
start validation test
0.617835051546
0.629658822205
0.575486261192
0.601354984407
0.61790940138
63.934
batch start
#iterations: 20
currently lose_sum: 98.18917167186737
time_elpased: 1.884
batch start
#iterations: 21
currently lose_sum: 97.67441004514694
time_elpased: 1.958
batch start
#iterations: 22
currently lose_sum: 97.94778716564178
time_elpased: 1.918
batch start
#iterations: 23
currently lose_sum: 97.96756768226624
time_elpased: 1.892
batch start
#iterations: 24
currently lose_sum: 97.66912192106247
time_elpased: 1.891
batch start
#iterations: 25
currently lose_sum: 97.56257259845734
time_elpased: 1.922
batch start
#iterations: 26
currently lose_sum: 97.73930072784424
time_elpased: 1.902
batch start
#iterations: 27
currently lose_sum: 97.72915434837341
time_elpased: 1.916
batch start
#iterations: 28
currently lose_sum: 97.35626250505447
time_elpased: 1.998
batch start
#iterations: 29
currently lose_sum: 97.5358504652977
time_elpased: 1.889
batch start
#iterations: 30
currently lose_sum: 97.36047500371933
time_elpased: 1.898
batch start
#iterations: 31
currently lose_sum: 97.34703987836838
time_elpased: 1.937
batch start
#iterations: 32
currently lose_sum: 97.26344871520996
time_elpased: 1.915
batch start
#iterations: 33
currently lose_sum: 97.30194914340973
time_elpased: 1.916
batch start
#iterations: 34
currently lose_sum: 97.35436594486237
time_elpased: 1.924
batch start
#iterations: 35
currently lose_sum: 97.19269615411758
time_elpased: 1.951
batch start
#iterations: 36
currently lose_sum: 97.3686528801918
time_elpased: 1.911
batch start
#iterations: 37
currently lose_sum: 97.36294531822205
time_elpased: 1.93
batch start
#iterations: 38
currently lose_sum: 97.30373573303223
time_elpased: 1.908
batch start
#iterations: 39
currently lose_sum: 97.23890948295593
time_elpased: 1.931
start validation test
0.626082474227
0.603149342491
0.741072347432
0.6650350942
0.625880591768
63.000
batch start
#iterations: 40
currently lose_sum: 97.45564812421799
time_elpased: 1.95
batch start
#iterations: 41
currently lose_sum: 97.49522972106934
time_elpased: 1.934
batch start
#iterations: 42
currently lose_sum: 97.3260064125061
time_elpased: 1.922
batch start
#iterations: 43
currently lose_sum: 96.73259824514389
time_elpased: 1.898
batch start
#iterations: 44
currently lose_sum: 97.29924064874649
time_elpased: 1.949
batch start
#iterations: 45
currently lose_sum: 97.14855760335922
time_elpased: 1.908
batch start
#iterations: 46
currently lose_sum: 96.80217617750168
time_elpased: 1.926
batch start
#iterations: 47
currently lose_sum: 97.3281557559967
time_elpased: 1.922
batch start
#iterations: 48
currently lose_sum: 97.2799443602562
time_elpased: 1.924
batch start
#iterations: 49
currently lose_sum: 97.16872835159302
time_elpased: 1.93
batch start
#iterations: 50
currently lose_sum: 97.10681891441345
time_elpased: 1.894
batch start
#iterations: 51
currently lose_sum: 96.90475589036942
time_elpased: 1.899
batch start
#iterations: 52
currently lose_sum: 96.86922883987427
time_elpased: 1.923
batch start
#iterations: 53
currently lose_sum: 96.73557704687119
time_elpased: 1.928
batch start
#iterations: 54
currently lose_sum: 97.05179631710052
time_elpased: 1.964
batch start
#iterations: 55
currently lose_sum: 97.1073471903801
time_elpased: 1.897
batch start
#iterations: 56
currently lose_sum: 97.03071933984756
time_elpased: 1.911
batch start
#iterations: 57
currently lose_sum: 96.8852368593216
time_elpased: 1.961
batch start
#iterations: 58
currently lose_sum: 97.07035076618195
time_elpased: 1.952
batch start
#iterations: 59
currently lose_sum: 97.03960412740707
time_elpased: 1.917
start validation test
0.632731958763
0.608816120907
0.746217968509
0.670550700513
0.632532716569
62.802
batch start
#iterations: 60
currently lose_sum: 97.13449519872665
time_elpased: 1.928
batch start
#iterations: 61
currently lose_sum: 97.12135499715805
time_elpased: 1.942
batch start
#iterations: 62
currently lose_sum: 96.8791491985321
time_elpased: 1.914
batch start
#iterations: 63
currently lose_sum: 96.82179337739944
time_elpased: 1.94
batch start
#iterations: 64
currently lose_sum: 97.12328815460205
time_elpased: 1.886
batch start
#iterations: 65
currently lose_sum: 96.54243797063828
time_elpased: 1.893
batch start
#iterations: 66
currently lose_sum: 96.67277652025223
time_elpased: 1.964
batch start
#iterations: 67
currently lose_sum: 96.87889552116394
time_elpased: 1.911
batch start
#iterations: 68
currently lose_sum: 96.71330028772354
time_elpased: 1.893
batch start
#iterations: 69
currently lose_sum: 96.43641501665115
time_elpased: 1.922
batch start
#iterations: 70
currently lose_sum: 96.17294710874557
time_elpased: 1.981
batch start
#iterations: 71
currently lose_sum: 96.53927445411682
time_elpased: 1.949
batch start
#iterations: 72
currently lose_sum: 96.5978769659996
time_elpased: 1.885
batch start
#iterations: 73
currently lose_sum: 96.63164097070694
time_elpased: 1.906
batch start
#iterations: 74
currently lose_sum: 96.3768779039383
time_elpased: 1.913
batch start
#iterations: 75
currently lose_sum: 96.46226316690445
time_elpased: 1.979
batch start
#iterations: 76
currently lose_sum: 96.62892180681229
time_elpased: 1.929
batch start
#iterations: 77
currently lose_sum: 96.7592756152153
time_elpased: 1.922
batch start
#iterations: 78
currently lose_sum: 96.88185197114944
time_elpased: 1.915
batch start
#iterations: 79
currently lose_sum: 96.49092608690262
time_elpased: 1.945
start validation test
0.631597938144
0.616037565469
0.702068539673
0.656245490837
0.631474216134
62.485
batch start
#iterations: 80
currently lose_sum: 96.81411731243134
time_elpased: 1.901
batch start
#iterations: 81
currently lose_sum: 97.0716900229454
time_elpased: 1.945
batch start
#iterations: 82
currently lose_sum: 96.7631094455719
time_elpased: 1.926
batch start
#iterations: 83
currently lose_sum: 96.65238547325134
time_elpased: 1.924
batch start
#iterations: 84
currently lose_sum: 96.50129586458206
time_elpased: 1.992
batch start
#iterations: 85
currently lose_sum: 96.75612318515778
time_elpased: 1.907
batch start
#iterations: 86
currently lose_sum: 96.62072801589966
time_elpased: 1.924
batch start
#iterations: 87
currently lose_sum: 96.48834329843521
time_elpased: 1.901
batch start
#iterations: 88
currently lose_sum: 96.43824273347855
time_elpased: 1.938
batch start
#iterations: 89
currently lose_sum: 96.45713782310486
time_elpased: 1.893
batch start
#iterations: 90
currently lose_sum: 96.65481704473495
time_elpased: 1.932
batch start
#iterations: 91
currently lose_sum: 96.24945098161697
time_elpased: 1.948
batch start
#iterations: 92
currently lose_sum: 96.64813679456711
time_elpased: 1.913
batch start
#iterations: 93
currently lose_sum: 96.32710832357407
time_elpased: 1.906
batch start
#iterations: 94
currently lose_sum: 96.5433976650238
time_elpased: 1.885
batch start
#iterations: 95
currently lose_sum: 96.69592380523682
time_elpased: 1.88
batch start
#iterations: 96
currently lose_sum: 96.64872765541077
time_elpased: 1.869
batch start
#iterations: 97
currently lose_sum: 96.4926010966301
time_elpased: 1.896
batch start
#iterations: 98
currently lose_sum: 96.43391042947769
time_elpased: 1.9
batch start
#iterations: 99
currently lose_sum: 96.38379400968552
time_elpased: 1.908
start validation test
0.623969072165
0.616085122699
0.661418133169
0.63794729267
0.623903324562
62.798
batch start
#iterations: 100
currently lose_sum: 96.4780843257904
time_elpased: 1.928
batch start
#iterations: 101
currently lose_sum: 96.18654036521912
time_elpased: 1.882
batch start
#iterations: 102
currently lose_sum: 96.3028274178505
time_elpased: 1.945
batch start
#iterations: 103
currently lose_sum: 96.62437945604324
time_elpased: 1.905
batch start
#iterations: 104
currently lose_sum: 96.22065889835358
time_elpased: 1.914
batch start
#iterations: 105
currently lose_sum: 96.40887260437012
time_elpased: 1.925
batch start
#iterations: 106
currently lose_sum: 96.43268072605133
time_elpased: 1.889
batch start
#iterations: 107
currently lose_sum: 96.23810696601868
time_elpased: 1.896
batch start
#iterations: 108
currently lose_sum: 96.40520644187927
time_elpased: 1.892
batch start
#iterations: 109
currently lose_sum: 96.30432945489883
time_elpased: 1.902
batch start
#iterations: 110
currently lose_sum: 96.20757973194122
time_elpased: 1.914
batch start
#iterations: 111
currently lose_sum: 96.28401130437851
time_elpased: 1.906
batch start
#iterations: 112
currently lose_sum: 96.69336968660355
time_elpased: 1.924
batch start
#iterations: 113
currently lose_sum: 96.35183328390121
time_elpased: 1.92
batch start
#iterations: 114
currently lose_sum: 96.3273583650589
time_elpased: 1.905
batch start
#iterations: 115
currently lose_sum: 96.2520614862442
time_elpased: 1.914
batch start
#iterations: 116
currently lose_sum: 96.15997505187988
time_elpased: 1.941
batch start
#iterations: 117
currently lose_sum: 96.40217512845993
time_elpased: 1.886
batch start
#iterations: 118
currently lose_sum: 96.55955511331558
time_elpased: 1.922
batch start
#iterations: 119
currently lose_sum: 96.21911561489105
time_elpased: 1.876
start validation test
0.636340206186
0.612968935665
0.743233508284
0.671845202102
0.636152538507
62.386
batch start
#iterations: 120
currently lose_sum: 96.34912252426147
time_elpased: 1.907
batch start
#iterations: 121
currently lose_sum: 96.5483587384224
time_elpased: 1.879
batch start
#iterations: 122
currently lose_sum: 96.5560405254364
time_elpased: 1.928
batch start
#iterations: 123
currently lose_sum: 96.20420575141907
time_elpased: 1.904
batch start
#iterations: 124
currently lose_sum: 96.8215702176094
time_elpased: 1.903
batch start
#iterations: 125
currently lose_sum: 96.26562756299973
time_elpased: 1.94
batch start
#iterations: 126
currently lose_sum: 96.1594330072403
time_elpased: 1.884
batch start
#iterations: 127
currently lose_sum: 96.24893170595169
time_elpased: 1.889
batch start
#iterations: 128
currently lose_sum: 96.29635471105576
time_elpased: 1.923
batch start
#iterations: 129
currently lose_sum: 96.18284821510315
time_elpased: 1.893
batch start
#iterations: 130
currently lose_sum: 96.285080909729
time_elpased: 1.905
batch start
#iterations: 131
currently lose_sum: 96.12157887220383
time_elpased: 1.916
batch start
#iterations: 132
currently lose_sum: 96.4675686955452
time_elpased: 1.932
batch start
#iterations: 133
currently lose_sum: 96.2310546040535
time_elpased: 1.887
batch start
#iterations: 134
currently lose_sum: 96.4958466887474
time_elpased: 1.913
batch start
#iterations: 135
currently lose_sum: 96.49507838487625
time_elpased: 1.966
batch start
#iterations: 136
currently lose_sum: 96.14805066585541
time_elpased: 1.886
batch start
#iterations: 137
currently lose_sum: 96.05703896284103
time_elpased: 1.914
batch start
#iterations: 138
currently lose_sum: 95.97014850378036
time_elpased: 1.944
batch start
#iterations: 139
currently lose_sum: 96.41672819852829
time_elpased: 1.9
start validation test
0.634278350515
0.620164986251
0.696305444067
0.656033354341
0.634169452386
62.432
batch start
#iterations: 140
currently lose_sum: 96.27688896656036
time_elpased: 1.903
batch start
#iterations: 141
currently lose_sum: 96.54669684171677
time_elpased: 1.947
batch start
#iterations: 142
currently lose_sum: 96.16202574968338
time_elpased: 1.94
batch start
#iterations: 143
currently lose_sum: 96.40312802791595
time_elpased: 1.888
batch start
#iterations: 144
currently lose_sum: 96.0393698811531
time_elpased: 1.957
batch start
#iterations: 145
currently lose_sum: 96.28035813570023
time_elpased: 1.903
batch start
#iterations: 146
currently lose_sum: 96.17852300405502
time_elpased: 1.916
batch start
#iterations: 147
currently lose_sum: 96.37326449155807
time_elpased: 1.923
batch start
#iterations: 148
currently lose_sum: 96.60343760251999
time_elpased: 1.927
batch start
#iterations: 149
currently lose_sum: 96.29693812131882
time_elpased: 1.919
batch start
#iterations: 150
currently lose_sum: 96.0392319560051
time_elpased: 1.931
batch start
#iterations: 151
currently lose_sum: 96.25408017635345
time_elpased: 1.896
batch start
#iterations: 152
currently lose_sum: 96.37217175960541
time_elpased: 1.928
batch start
#iterations: 153
currently lose_sum: 96.25952470302582
time_elpased: 1.924
batch start
#iterations: 154
currently lose_sum: 96.3094192147255
time_elpased: 1.927
batch start
#iterations: 155
currently lose_sum: 96.41419488191605
time_elpased: 1.892
batch start
#iterations: 156
currently lose_sum: 95.80205702781677
time_elpased: 1.902
batch start
#iterations: 157
currently lose_sum: 96.1229413151741
time_elpased: 1.924
batch start
#iterations: 158
currently lose_sum: 96.35850006341934
time_elpased: 1.915
batch start
#iterations: 159
currently lose_sum: 96.20766544342041
time_elpased: 1.929
start validation test
0.635721649485
0.616268866269
0.7227539364
0.665277317293
0.635568850881
62.352
batch start
#iterations: 160
currently lose_sum: 96.3284957408905
time_elpased: 1.917
batch start
#iterations: 161
currently lose_sum: 96.39664781093597
time_elpased: 1.924
batch start
#iterations: 162
currently lose_sum: 96.40473747253418
time_elpased: 1.973
batch start
#iterations: 163
currently lose_sum: 95.86142259836197
time_elpased: 1.92
batch start
#iterations: 164
currently lose_sum: 96.13304531574249
time_elpased: 1.969
batch start
#iterations: 165
currently lose_sum: 96.39121633768082
time_elpased: 1.922
batch start
#iterations: 166
currently lose_sum: 96.3263835310936
time_elpased: 1.956
batch start
#iterations: 167
currently lose_sum: 96.23841845989227
time_elpased: 1.922
batch start
#iterations: 168
currently lose_sum: 96.0477774143219
time_elpased: 1.885
batch start
#iterations: 169
currently lose_sum: 96.11290919780731
time_elpased: 1.963
batch start
#iterations: 170
currently lose_sum: 96.04642003774643
time_elpased: 1.928
batch start
#iterations: 171
currently lose_sum: 96.15363872051239
time_elpased: 1.963
batch start
#iterations: 172
currently lose_sum: 95.99898207187653
time_elpased: 1.906
batch start
#iterations: 173
currently lose_sum: 96.16076046228409
time_elpased: 1.967
batch start
#iterations: 174
currently lose_sum: 96.46776127815247
time_elpased: 1.891
batch start
#iterations: 175
currently lose_sum: 96.20328146219254
time_elpased: 1.942
batch start
#iterations: 176
currently lose_sum: 96.19130730628967
time_elpased: 1.934
batch start
#iterations: 177
currently lose_sum: 96.09172278642654
time_elpased: 1.899
batch start
#iterations: 178
currently lose_sum: 96.15868580341339
time_elpased: 1.903
batch start
#iterations: 179
currently lose_sum: 96.23594129085541
time_elpased: 1.876
start validation test
0.637628865979
0.606958044742
0.784604301739
0.684442050453
0.637370827933
62.274
batch start
#iterations: 180
currently lose_sum: 96.37632882595062
time_elpased: 1.912
batch start
#iterations: 181
currently lose_sum: 95.61663818359375
time_elpased: 1.97
batch start
#iterations: 182
currently lose_sum: 96.4559543132782
time_elpased: 1.891
batch start
#iterations: 183
currently lose_sum: 95.8410028219223
time_elpased: 1.893
batch start
#iterations: 184
currently lose_sum: 96.24375075101852
time_elpased: 1.882
batch start
#iterations: 185
currently lose_sum: 95.99257153272629
time_elpased: 1.895
batch start
#iterations: 186
currently lose_sum: 95.97468394041061
time_elpased: 1.905
batch start
#iterations: 187
currently lose_sum: 95.948710501194
time_elpased: 1.913
batch start
#iterations: 188
currently lose_sum: 96.02269619703293
time_elpased: 1.896
batch start
#iterations: 189
currently lose_sum: 95.95191472768784
time_elpased: 1.927
batch start
#iterations: 190
currently lose_sum: 96.24311059713364
time_elpased: 1.94
batch start
#iterations: 191
currently lose_sum: 95.77698862552643
time_elpased: 1.876
batch start
#iterations: 192
currently lose_sum: 95.48963046073914
time_elpased: 1.921
batch start
#iterations: 193
currently lose_sum: 96.25283378362656
time_elpased: 1.924
batch start
#iterations: 194
currently lose_sum: 96.09696292877197
time_elpased: 1.897
batch start
#iterations: 195
currently lose_sum: 96.19175958633423
time_elpased: 1.936
batch start
#iterations: 196
currently lose_sum: 96.03819674253464
time_elpased: 1.904
batch start
#iterations: 197
currently lose_sum: 96.60249280929565
time_elpased: 1.912
batch start
#iterations: 198
currently lose_sum: 96.29471576213837
time_elpased: 1.912
batch start
#iterations: 199
currently lose_sum: 96.41631960868835
time_elpased: 1.901
start validation test
0.623917525773
0.623836317136
0.627559946486
0.625692591833
0.623911130942
62.769
batch start
#iterations: 200
currently lose_sum: 95.94883590936661
time_elpased: 1.952
batch start
#iterations: 201
currently lose_sum: 95.91787946224213
time_elpased: 1.899
batch start
#iterations: 202
currently lose_sum: 95.95520436763763
time_elpased: 1.963
batch start
#iterations: 203
currently lose_sum: 96.28781700134277
time_elpased: 1.911
batch start
#iterations: 204
currently lose_sum: 96.26168084144592
time_elpased: 1.933
batch start
#iterations: 205
currently lose_sum: 95.915840446949
time_elpased: 1.944
batch start
#iterations: 206
currently lose_sum: 95.93538826704025
time_elpased: 1.929
batch start
#iterations: 207
currently lose_sum: 95.8312417268753
time_elpased: 1.913
batch start
#iterations: 208
currently lose_sum: 95.73194515705109
time_elpased: 1.895
batch start
#iterations: 209
currently lose_sum: 95.70718669891357
time_elpased: 1.922
batch start
#iterations: 210
currently lose_sum: 95.78147149085999
time_elpased: 1.929
batch start
#iterations: 211
currently lose_sum: 95.45976835489273
time_elpased: 1.944
batch start
#iterations: 212
currently lose_sum: 96.08055627346039
time_elpased: 1.906
batch start
#iterations: 213
currently lose_sum: 96.07616424560547
time_elpased: 1.952
batch start
#iterations: 214
currently lose_sum: 95.61906737089157
time_elpased: 1.937
batch start
#iterations: 215
currently lose_sum: 95.89933604001999
time_elpased: 1.905
batch start
#iterations: 216
currently lose_sum: 96.07883542776108
time_elpased: 1.921
batch start
#iterations: 217
currently lose_sum: 95.76746195554733
time_elpased: 1.92
batch start
#iterations: 218
currently lose_sum: 96.2922613620758
time_elpased: 1.899
batch start
#iterations: 219
currently lose_sum: 95.70619821548462
time_elpased: 1.904
start validation test
0.634278350515
0.61374284227
0.728002469898
0.666007626042
0.634113803368
62.236
batch start
#iterations: 220
currently lose_sum: 96.32066577672958
time_elpased: 1.955
batch start
#iterations: 221
currently lose_sum: 95.69140559434891
time_elpased: 1.958
batch start
#iterations: 222
currently lose_sum: 96.04337555170059
time_elpased: 1.958
batch start
#iterations: 223
currently lose_sum: 95.90848398208618
time_elpased: 1.945
batch start
#iterations: 224
currently lose_sum: 96.1738532781601
time_elpased: 1.947
batch start
#iterations: 225
currently lose_sum: 96.0960955619812
time_elpased: 1.916
batch start
#iterations: 226
currently lose_sum: 95.96132671833038
time_elpased: 1.95
batch start
#iterations: 227
currently lose_sum: 96.27182990312576
time_elpased: 1.942
batch start
#iterations: 228
currently lose_sum: 96.00170016288757
time_elpased: 1.923
batch start
#iterations: 229
currently lose_sum: 95.59656953811646
time_elpased: 1.939
batch start
#iterations: 230
currently lose_sum: 95.82344871759415
time_elpased: 1.9
batch start
#iterations: 231
currently lose_sum: 96.13249415159225
time_elpased: 1.959
batch start
#iterations: 232
currently lose_sum: 96.20711678266525
time_elpased: 1.928
batch start
#iterations: 233
currently lose_sum: 95.87459969520569
time_elpased: 1.947
batch start
#iterations: 234
currently lose_sum: 96.32604682445526
time_elpased: 1.945
batch start
#iterations: 235
currently lose_sum: 95.72906512022018
time_elpased: 1.908
batch start
#iterations: 236
currently lose_sum: 95.95726907253265
time_elpased: 1.953
batch start
#iterations: 237
currently lose_sum: 95.80305403470993
time_elpased: 1.957
batch start
#iterations: 238
currently lose_sum: 95.97933715581894
time_elpased: 1.933
batch start
#iterations: 239
currently lose_sum: 95.93069624900818
time_elpased: 1.913
start validation test
0.63881443299
0.61259764002
0.758670371514
0.677853891775
0.638604007403
62.073
batch start
#iterations: 240
currently lose_sum: 95.96883100271225
time_elpased: 1.926
batch start
#iterations: 241
currently lose_sum: 95.95623230934143
time_elpased: 1.873
batch start
#iterations: 242
currently lose_sum: 95.80366909503937
time_elpased: 1.914
batch start
#iterations: 243
currently lose_sum: 95.96580559015274
time_elpased: 1.917
batch start
#iterations: 244
currently lose_sum: 95.91094875335693
time_elpased: 1.938
batch start
#iterations: 245
currently lose_sum: 95.70011377334595
time_elpased: 1.896
batch start
#iterations: 246
currently lose_sum: 96.37440711259842
time_elpased: 2.009
batch start
#iterations: 247
currently lose_sum: 96.10389691591263
time_elpased: 1.926
batch start
#iterations: 248
currently lose_sum: 96.2632566690445
time_elpased: 1.943
batch start
#iterations: 249
currently lose_sum: 95.6660298705101
time_elpased: 1.934
batch start
#iterations: 250
currently lose_sum: 96.29133492708206
time_elpased: 1.916
batch start
#iterations: 251
currently lose_sum: 95.7338707447052
time_elpased: 1.955
batch start
#iterations: 252
currently lose_sum: 95.49854117631912
time_elpased: 1.937
batch start
#iterations: 253
currently lose_sum: 96.26427632570267
time_elpased: 1.914
batch start
#iterations: 254
currently lose_sum: 95.60165107250214
time_elpased: 1.889
batch start
#iterations: 255
currently lose_sum: 95.84036535024643
time_elpased: 1.967
batch start
#iterations: 256
currently lose_sum: 95.94812428951263
time_elpased: 1.922
batch start
#iterations: 257
currently lose_sum: 95.92943751811981
time_elpased: 1.97
batch start
#iterations: 258
currently lose_sum: 96.15153795480728
time_elpased: 1.947
batch start
#iterations: 259
currently lose_sum: 96.14915817975998
time_elpased: 1.965
start validation test
0.639072164948
0.6131910281
0.756817947926
0.677475817596
0.638865444065
62.188
batch start
#iterations: 260
currently lose_sum: 95.60837119817734
time_elpased: 1.927
batch start
#iterations: 261
currently lose_sum: 96.09917694330215
time_elpased: 1.903
batch start
#iterations: 262
currently lose_sum: 95.5963082909584
time_elpased: 1.957
batch start
#iterations: 263
currently lose_sum: 95.89457380771637
time_elpased: 1.966
batch start
#iterations: 264
currently lose_sum: 96.14331412315369
time_elpased: 1.914
batch start
#iterations: 265
currently lose_sum: 95.74581903219223
time_elpased: 1.922
batch start
#iterations: 266
currently lose_sum: 95.77723360061646
time_elpased: 1.938
batch start
#iterations: 267
currently lose_sum: 96.06087458133698
time_elpased: 1.976
batch start
#iterations: 268
currently lose_sum: 96.1175497174263
time_elpased: 1.912
batch start
#iterations: 269
currently lose_sum: 95.58606743812561
time_elpased: 1.923
batch start
#iterations: 270
currently lose_sum: 96.06099760532379
time_elpased: 1.969
batch start
#iterations: 271
currently lose_sum: 96.1525326371193
time_elpased: 1.946
batch start
#iterations: 272
currently lose_sum: 95.42428356409073
time_elpased: 1.954
batch start
#iterations: 273
currently lose_sum: 96.10348945856094
time_elpased: 1.908
batch start
#iterations: 274
currently lose_sum: 95.91612553596497
time_elpased: 1.906
batch start
#iterations: 275
currently lose_sum: 96.04185211658478
time_elpased: 1.94
batch start
#iterations: 276
currently lose_sum: 95.78283113241196
time_elpased: 1.971
batch start
#iterations: 277
currently lose_sum: 95.99059116840363
time_elpased: 1.962
batch start
#iterations: 278
currently lose_sum: 95.99540203809738
time_elpased: 1.941
batch start
#iterations: 279
currently lose_sum: 96.14126187562943
time_elpased: 1.937
start validation test
0.630927835052
0.607427947231
0.743953895235
0.668794523083
0.63072940037
62.453
batch start
#iterations: 280
currently lose_sum: 95.45148074626923
time_elpased: 1.947
batch start
#iterations: 281
currently lose_sum: 95.74162793159485
time_elpased: 1.914
batch start
#iterations: 282
currently lose_sum: 95.97798889875412
time_elpased: 1.932
batch start
#iterations: 283
currently lose_sum: 96.26308554410934
time_elpased: 1.944
batch start
#iterations: 284
currently lose_sum: 95.926822245121
time_elpased: 1.932
batch start
#iterations: 285
currently lose_sum: 96.13181126117706
time_elpased: 1.887
batch start
#iterations: 286
currently lose_sum: 95.87213605642319
time_elpased: 1.911
batch start
#iterations: 287
currently lose_sum: 95.80952972173691
time_elpased: 1.946
batch start
#iterations: 288
currently lose_sum: 95.81615042686462
time_elpased: 1.972
batch start
#iterations: 289
currently lose_sum: 95.68303728103638
time_elpased: 1.916
batch start
#iterations: 290
currently lose_sum: 95.88690906763077
time_elpased: 1.946
batch start
#iterations: 291
currently lose_sum: 95.48493164777756
time_elpased: 1.938
batch start
#iterations: 292
currently lose_sum: 95.66861659288406
time_elpased: 1.953
batch start
#iterations: 293
currently lose_sum: 95.57371926307678
time_elpased: 1.945
batch start
#iterations: 294
currently lose_sum: 95.71460044384003
time_elpased: 1.939
batch start
#iterations: 295
currently lose_sum: 95.74027651548386
time_elpased: 1.915
batch start
#iterations: 296
currently lose_sum: 95.77816146612167
time_elpased: 1.887
batch start
#iterations: 297
currently lose_sum: 95.7698523402214
time_elpased: 1.96
batch start
#iterations: 298
currently lose_sum: 96.03052347898483
time_elpased: 1.923
batch start
#iterations: 299
currently lose_sum: 95.91266840696335
time_elpased: 1.928
start validation test
0.63912371134
0.62122835208
0.716167541422
0.665328170563
0.638988449013
62.306
batch start
#iterations: 300
currently lose_sum: 96.23257392644882
time_elpased: 1.937
batch start
#iterations: 301
currently lose_sum: 95.66598039865494
time_elpased: 1.912
batch start
#iterations: 302
currently lose_sum: 95.71966254711151
time_elpased: 1.902
batch start
#iterations: 303
currently lose_sum: 95.96305131912231
time_elpased: 1.943
batch start
#iterations: 304
currently lose_sum: 95.57946622371674
time_elpased: 1.95
batch start
#iterations: 305
currently lose_sum: 95.72027170658112
time_elpased: 1.91
batch start
#iterations: 306
currently lose_sum: 95.7771281003952
time_elpased: 1.932
batch start
#iterations: 307
currently lose_sum: 95.6926702260971
time_elpased: 1.932
batch start
#iterations: 308
currently lose_sum: 95.75991117954254
time_elpased: 1.957
batch start
#iterations: 309
currently lose_sum: 96.07692861557007
time_elpased: 1.929
batch start
#iterations: 310
currently lose_sum: 95.787846326828
time_elpased: 1.936
batch start
#iterations: 311
currently lose_sum: 95.62213903665543
time_elpased: 1.93
batch start
#iterations: 312
currently lose_sum: 95.75586813688278
time_elpased: 1.947
batch start
#iterations: 313
currently lose_sum: 95.65138566493988
time_elpased: 1.93
batch start
#iterations: 314
currently lose_sum: 96.19927948713303
time_elpased: 1.917
batch start
#iterations: 315
currently lose_sum: 95.89373183250427
time_elpased: 1.952
batch start
#iterations: 316
currently lose_sum: 95.6900669336319
time_elpased: 1.934
batch start
#iterations: 317
currently lose_sum: 95.70806419849396
time_elpased: 1.925
batch start
#iterations: 318
currently lose_sum: 95.80995982885361
time_elpased: 2.019
batch start
#iterations: 319
currently lose_sum: 95.63689059019089
time_elpased: 1.938
start validation test
0.640051546392
0.606846959512
0.799012040753
0.689796099685
0.639772466726
62.144
batch start
#iterations: 320
currently lose_sum: 96.06686437129974
time_elpased: 1.944
batch start
#iterations: 321
currently lose_sum: 95.87779998779297
time_elpased: 1.905
batch start
#iterations: 322
currently lose_sum: 96.0953488945961
time_elpased: 1.914
batch start
#iterations: 323
currently lose_sum: 95.66202473640442
time_elpased: 1.929
batch start
#iterations: 324
currently lose_sum: 96.06053984165192
time_elpased: 1.935
batch start
#iterations: 325
currently lose_sum: 95.82965463399887
time_elpased: 1.981
batch start
#iterations: 326
currently lose_sum: 95.35900545120239
time_elpased: 1.963
batch start
#iterations: 327
currently lose_sum: 96.07289904356003
time_elpased: 1.913
batch start
#iterations: 328
currently lose_sum: 95.92884582281113
time_elpased: 1.929
batch start
#iterations: 329
currently lose_sum: 95.53023529052734
time_elpased: 1.911
batch start
#iterations: 330
currently lose_sum: 95.62588596343994
time_elpased: 1.941
batch start
#iterations: 331
currently lose_sum: 95.90255385637283
time_elpased: 1.939
batch start
#iterations: 332
currently lose_sum: 95.66563272476196
time_elpased: 1.944
batch start
#iterations: 333
currently lose_sum: 95.91794556379318
time_elpased: 1.895
batch start
#iterations: 334
currently lose_sum: 95.9010608792305
time_elpased: 1.924
batch start
#iterations: 335
currently lose_sum: 95.82594138383865
time_elpased: 1.949
batch start
#iterations: 336
currently lose_sum: 95.75794005393982
time_elpased: 1.927
batch start
#iterations: 337
currently lose_sum: 95.77193015813828
time_elpased: 1.912
batch start
#iterations: 338
currently lose_sum: 95.60187435150146
time_elpased: 1.964
batch start
#iterations: 339
currently lose_sum: 95.85980546474457
time_elpased: 1.968
start validation test
0.639742268041
0.610552763819
0.77523927138
0.683110405804
0.639504382153
62.152
batch start
#iterations: 340
currently lose_sum: 96.09193968772888
time_elpased: 1.919
batch start
#iterations: 341
currently lose_sum: 95.70574933290482
time_elpased: 1.971
batch start
#iterations: 342
currently lose_sum: 95.75698721408844
time_elpased: 1.927
batch start
#iterations: 343
currently lose_sum: 95.69685685634613
time_elpased: 1.97
batch start
#iterations: 344
currently lose_sum: 95.95890784263611
time_elpased: 1.98
batch start
#iterations: 345
currently lose_sum: 95.97554385662079
time_elpased: 1.905
batch start
#iterations: 346
currently lose_sum: 95.88044798374176
time_elpased: 1.983
batch start
#iterations: 347
currently lose_sum: 95.41265434026718
time_elpased: 1.91
batch start
#iterations: 348
currently lose_sum: 95.58319467306137
time_elpased: 2.005
batch start
#iterations: 349
currently lose_sum: 95.67706543207169
time_elpased: 1.981
batch start
#iterations: 350
currently lose_sum: 95.57264018058777
time_elpased: 1.929
batch start
#iterations: 351
currently lose_sum: 95.69283962249756
time_elpased: 1.957
batch start
#iterations: 352
currently lose_sum: 96.10543251037598
time_elpased: 1.923
batch start
#iterations: 353
currently lose_sum: 96.06796824932098
time_elpased: 1.964
batch start
#iterations: 354
currently lose_sum: 95.67856860160828
time_elpased: 1.93
batch start
#iterations: 355
currently lose_sum: 95.55529320240021
time_elpased: 1.939
batch start
#iterations: 356
currently lose_sum: 95.57198125123978
time_elpased: 1.939
batch start
#iterations: 357
currently lose_sum: 95.61938017606735
time_elpased: 1.911
batch start
#iterations: 358
currently lose_sum: 96.10709220170975
time_elpased: 1.942
batch start
#iterations: 359
currently lose_sum: 95.7211663722992
time_elpased: 1.896
start validation test
0.635412371134
0.613030095759
0.737882062365
0.669686638958
0.635232469786
62.399
batch start
#iterations: 360
currently lose_sum: 95.89108109474182
time_elpased: 1.946
batch start
#iterations: 361
currently lose_sum: 95.79441344738007
time_elpased: 1.904
batch start
#iterations: 362
currently lose_sum: 95.94183146953583
time_elpased: 1.959
batch start
#iterations: 363
currently lose_sum: 95.92405247688293
time_elpased: 1.928
batch start
#iterations: 364
currently lose_sum: 95.97579604387283
time_elpased: 1.91
batch start
#iterations: 365
currently lose_sum: 95.92579692602158
time_elpased: 1.935
batch start
#iterations: 366
currently lose_sum: 95.81351846456528
time_elpased: 1.905
batch start
#iterations: 367
currently lose_sum: 95.74728947877884
time_elpased: 1.912
batch start
#iterations: 368
currently lose_sum: 95.5658073425293
time_elpased: 1.963
batch start
#iterations: 369
currently lose_sum: 95.85758513212204
time_elpased: 1.934
batch start
#iterations: 370
currently lose_sum: 95.80396789312363
time_elpased: 1.914
batch start
#iterations: 371
currently lose_sum: 95.57922649383545
time_elpased: 1.99
batch start
#iterations: 372
currently lose_sum: 95.57256412506104
time_elpased: 1.94
batch start
#iterations: 373
currently lose_sum: 95.51197522878647
time_elpased: 1.887
batch start
#iterations: 374
currently lose_sum: 95.44618725776672
time_elpased: 1.941
batch start
#iterations: 375
currently lose_sum: 95.98183560371399
time_elpased: 1.926
batch start
#iterations: 376
currently lose_sum: 95.66995507478714
time_elpased: 1.921
batch start
#iterations: 377
currently lose_sum: 95.73859399557114
time_elpased: 1.936
batch start
#iterations: 378
currently lose_sum: 95.46172416210175
time_elpased: 1.915
batch start
#iterations: 379
currently lose_sum: 95.82183963060379
time_elpased: 1.914
start validation test
0.636443298969
0.615224913495
0.731913141916
0.668515298209
0.636275686935
62.248
batch start
#iterations: 380
currently lose_sum: 95.68522346019745
time_elpased: 1.954
batch start
#iterations: 381
currently lose_sum: 95.80193740129471
time_elpased: 1.948
batch start
#iterations: 382
currently lose_sum: 95.79152619838715
time_elpased: 1.957
batch start
#iterations: 383
currently lose_sum: 95.61524587869644
time_elpased: 1.918
batch start
#iterations: 384
currently lose_sum: 96.25720584392548
time_elpased: 1.902
batch start
#iterations: 385
currently lose_sum: 95.66092377901077
time_elpased: 1.941
batch start
#iterations: 386
currently lose_sum: 95.67496532201767
time_elpased: 1.907
batch start
#iterations: 387
currently lose_sum: 95.92224138975143
time_elpased: 1.924
batch start
#iterations: 388
currently lose_sum: 95.97521245479584
time_elpased: 1.931
batch start
#iterations: 389
currently lose_sum: 95.85532492399216
time_elpased: 1.956
batch start
#iterations: 390
currently lose_sum: 95.89876914024353
time_elpased: 1.913
batch start
#iterations: 391
currently lose_sum: 95.50955957174301
time_elpased: 1.897
batch start
#iterations: 392
currently lose_sum: 95.87560039758682
time_elpased: 1.932
batch start
#iterations: 393
currently lose_sum: 95.59983813762665
time_elpased: 1.921
batch start
#iterations: 394
currently lose_sum: 95.45642894506454
time_elpased: 1.932
batch start
#iterations: 395
currently lose_sum: 95.44274681806564
time_elpased: 1.971
batch start
#iterations: 396
currently lose_sum: 95.82228708267212
time_elpased: 1.939
batch start
#iterations: 397
currently lose_sum: 95.82578307390213
time_elpased: 1.923
batch start
#iterations: 398
currently lose_sum: 95.49562728404999
time_elpased: 1.927
batch start
#iterations: 399
currently lose_sum: 95.83499330282211
time_elpased: 1.942
start validation test
0.638865979381
0.614494467438
0.748687866626
0.674986082761
0.63867317012
62.117
acc: 0.640
pre: 0.614
rec: 0.759
F1: 0.679
auc: 0.677
