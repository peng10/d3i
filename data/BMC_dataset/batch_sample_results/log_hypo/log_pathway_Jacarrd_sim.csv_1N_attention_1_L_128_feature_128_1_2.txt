start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.62241715192795
time_elpased: 1.873
batch start
#iterations: 1
currently lose_sum: 100.38861435651779
time_elpased: 1.871
batch start
#iterations: 2
currently lose_sum: 100.34977781772614
time_elpased: 1.891
batch start
#iterations: 3
currently lose_sum: 99.82213413715363
time_elpased: 1.854
batch start
#iterations: 4
currently lose_sum: 99.66266006231308
time_elpased: 1.846
batch start
#iterations: 5
currently lose_sum: 99.51602053642273
time_elpased: 1.858
batch start
#iterations: 6
currently lose_sum: 99.36067712306976
time_elpased: 1.927
batch start
#iterations: 7
currently lose_sum: 99.42454212903976
time_elpased: 1.856
batch start
#iterations: 8
currently lose_sum: 99.09134685993195
time_elpased: 1.904
batch start
#iterations: 9
currently lose_sum: 99.12765592336655
time_elpased: 1.881
batch start
#iterations: 10
currently lose_sum: 99.01548343896866
time_elpased: 1.88
batch start
#iterations: 11
currently lose_sum: 98.97693699598312
time_elpased: 1.9
batch start
#iterations: 12
currently lose_sum: 98.85349094867706
time_elpased: 1.898
batch start
#iterations: 13
currently lose_sum: 98.68378984928131
time_elpased: 1.892
batch start
#iterations: 14
currently lose_sum: 98.45135766267776
time_elpased: 1.843
batch start
#iterations: 15
currently lose_sum: 98.48503041267395
time_elpased: 1.851
batch start
#iterations: 16
currently lose_sum: 98.46413153409958
time_elpased: 1.833
batch start
#iterations: 17
currently lose_sum: 98.46521079540253
time_elpased: 1.857
batch start
#iterations: 18
currently lose_sum: 98.22035443782806
time_elpased: 1.878
batch start
#iterations: 19
currently lose_sum: 97.9873942732811
time_elpased: 1.844
start validation test
0.61324742268
0.616354845491
0.603478439848
0.609848681816
0.613264573637
63.778
batch start
#iterations: 20
currently lose_sum: 97.93122124671936
time_elpased: 1.91
batch start
#iterations: 21
currently lose_sum: 98.02265357971191
time_elpased: 1.827
batch start
#iterations: 22
currently lose_sum: 98.05103534460068
time_elpased: 1.853
batch start
#iterations: 23
currently lose_sum: 98.06201219558716
time_elpased: 1.89
batch start
#iterations: 24
currently lose_sum: 97.47754579782486
time_elpased: 1.858
batch start
#iterations: 25
currently lose_sum: 97.73823827505112
time_elpased: 1.879
batch start
#iterations: 26
currently lose_sum: 97.75441479682922
time_elpased: 1.85
batch start
#iterations: 27
currently lose_sum: 97.80717861652374
time_elpased: 1.85
batch start
#iterations: 28
currently lose_sum: 97.87561666965485
time_elpased: 1.876
batch start
#iterations: 29
currently lose_sum: 97.81759995222092
time_elpased: 1.844
batch start
#iterations: 30
currently lose_sum: 97.72877806425095
time_elpased: 1.855
batch start
#iterations: 31
currently lose_sum: 97.51102989912033
time_elpased: 1.867
batch start
#iterations: 32
currently lose_sum: 97.35018688440323
time_elpased: 1.864
batch start
#iterations: 33
currently lose_sum: 97.70024806261063
time_elpased: 1.889
batch start
#iterations: 34
currently lose_sum: 97.24131226539612
time_elpased: 1.848
batch start
#iterations: 35
currently lose_sum: 97.56982034444809
time_elpased: 1.86
batch start
#iterations: 36
currently lose_sum: 97.36871415376663
time_elpased: 1.926
batch start
#iterations: 37
currently lose_sum: 97.60915172100067
time_elpased: 1.876
batch start
#iterations: 38
currently lose_sum: 97.31237423419952
time_elpased: 1.859
batch start
#iterations: 39
currently lose_sum: 97.3304967880249
time_elpased: 1.845
start validation test
0.632577319588
0.619297760575
0.691571472677
0.653442240373
0.632473746253
62.705
batch start
#iterations: 40
currently lose_sum: 97.31163114309311
time_elpased: 1.846
batch start
#iterations: 41
currently lose_sum: 97.46448189020157
time_elpased: 1.849
batch start
#iterations: 42
currently lose_sum: 97.17731273174286
time_elpased: 1.858
batch start
#iterations: 43
currently lose_sum: 97.55613040924072
time_elpased: 1.88
batch start
#iterations: 44
currently lose_sum: 97.22430920600891
time_elpased: 1.846
batch start
#iterations: 45
currently lose_sum: 97.32811909914017
time_elpased: 1.869
batch start
#iterations: 46
currently lose_sum: 97.18308091163635
time_elpased: 1.849
batch start
#iterations: 47
currently lose_sum: 97.37682056427002
time_elpased: 1.859
batch start
#iterations: 48
currently lose_sum: 97.29590797424316
time_elpased: 1.85
batch start
#iterations: 49
currently lose_sum: 97.25259655714035
time_elpased: 1.925
batch start
#iterations: 50
currently lose_sum: 97.31791996955872
time_elpased: 1.878
batch start
#iterations: 51
currently lose_sum: 97.10143929719925
time_elpased: 1.832
batch start
#iterations: 52
currently lose_sum: 97.38637727499008
time_elpased: 1.894
batch start
#iterations: 53
currently lose_sum: 97.07032680511475
time_elpased: 1.923
batch start
#iterations: 54
currently lose_sum: 97.17788457870483
time_elpased: 1.874
batch start
#iterations: 55
currently lose_sum: 96.79230016469955
time_elpased: 1.813
batch start
#iterations: 56
currently lose_sum: 97.44502228498459
time_elpased: 1.906
batch start
#iterations: 57
currently lose_sum: 97.18556928634644
time_elpased: 1.887
batch start
#iterations: 58
currently lose_sum: 97.38476926088333
time_elpased: 1.839
batch start
#iterations: 59
currently lose_sum: 97.38033825159073
time_elpased: 1.851
start validation test
0.629072164948
0.62003618703
0.670062776577
0.644079533089
0.629000199607
62.671
batch start
#iterations: 60
currently lose_sum: 97.16081357002258
time_elpased: 1.873
batch start
#iterations: 61
currently lose_sum: 97.04950314760208
time_elpased: 1.879
batch start
#iterations: 62
currently lose_sum: 97.06501811742783
time_elpased: 1.92
batch start
#iterations: 63
currently lose_sum: 96.76993763446808
time_elpased: 1.878
batch start
#iterations: 64
currently lose_sum: 97.19496291875839
time_elpased: 1.887
batch start
#iterations: 65
currently lose_sum: 97.0926404595375
time_elpased: 1.86
batch start
#iterations: 66
currently lose_sum: 96.78254383802414
time_elpased: 1.895
batch start
#iterations: 67
currently lose_sum: 96.97580289840698
time_elpased: 1.92
batch start
#iterations: 68
currently lose_sum: 96.84194672107697
time_elpased: 1.832
batch start
#iterations: 69
currently lose_sum: 96.73089373111725
time_elpased: 1.889
batch start
#iterations: 70
currently lose_sum: 96.8983119726181
time_elpased: 1.864
batch start
#iterations: 71
currently lose_sum: 96.99140506982803
time_elpased: 1.892
batch start
#iterations: 72
currently lose_sum: 96.97252285480499
time_elpased: 1.882
batch start
#iterations: 73
currently lose_sum: 96.94189685583115
time_elpased: 1.899
batch start
#iterations: 74
currently lose_sum: 97.08616179227829
time_elpased: 1.876
batch start
#iterations: 75
currently lose_sum: 96.85043448209763
time_elpased: 1.893
batch start
#iterations: 76
currently lose_sum: 96.85220175981522
time_elpased: 1.864
batch start
#iterations: 77
currently lose_sum: 96.98433148860931
time_elpased: 1.908
batch start
#iterations: 78
currently lose_sum: 97.11376190185547
time_elpased: 1.893
batch start
#iterations: 79
currently lose_sum: 96.99431681632996
time_elpased: 1.861
start validation test
0.636804123711
0.615778066753
0.730986930122
0.668454733672
0.636638771268
62.339
batch start
#iterations: 80
currently lose_sum: 97.08072274923325
time_elpased: 1.853
batch start
#iterations: 81
currently lose_sum: 96.76332062482834
time_elpased: 1.876
batch start
#iterations: 82
currently lose_sum: 96.85372513532639
time_elpased: 1.932
batch start
#iterations: 83
currently lose_sum: 96.63113230466843
time_elpased: 1.892
batch start
#iterations: 84
currently lose_sum: 96.9489209651947
time_elpased: 1.841
batch start
#iterations: 85
currently lose_sum: 96.97006261348724
time_elpased: 1.864
batch start
#iterations: 86
currently lose_sum: 96.90609097480774
time_elpased: 1.825
batch start
#iterations: 87
currently lose_sum: 96.84304285049438
time_elpased: 1.83
batch start
#iterations: 88
currently lose_sum: 96.9948217868805
time_elpased: 1.894
batch start
#iterations: 89
currently lose_sum: 96.77340060472488
time_elpased: 1.856
batch start
#iterations: 90
currently lose_sum: 96.74813383817673
time_elpased: 1.911
batch start
#iterations: 91
currently lose_sum: 96.9751363992691
time_elpased: 1.884
batch start
#iterations: 92
currently lose_sum: 97.04416340589523
time_elpased: 1.869
batch start
#iterations: 93
currently lose_sum: 96.82263803482056
time_elpased: 1.83
batch start
#iterations: 94
currently lose_sum: 96.47520869970322
time_elpased: 1.869
batch start
#iterations: 95
currently lose_sum: 96.79387557506561
time_elpased: 1.867
batch start
#iterations: 96
currently lose_sum: 96.85480624437332
time_elpased: 1.906
batch start
#iterations: 97
currently lose_sum: 96.59476602077484
time_elpased: 1.878
batch start
#iterations: 98
currently lose_sum: 96.94371539354324
time_elpased: 1.885
batch start
#iterations: 99
currently lose_sum: 96.87219059467316
time_elpased: 1.883
start validation test
0.627525773196
0.618359783332
0.669651126891
0.642984189723
0.62745181564
62.624
batch start
#iterations: 100
currently lose_sum: 96.70777136087418
time_elpased: 1.92
batch start
#iterations: 101
currently lose_sum: 96.70034384727478
time_elpased: 1.846
batch start
#iterations: 102
currently lose_sum: 96.65853941440582
time_elpased: 1.846
batch start
#iterations: 103
currently lose_sum: 96.58404302597046
time_elpased: 1.872
batch start
#iterations: 104
currently lose_sum: 97.08440470695496
time_elpased: 1.865
batch start
#iterations: 105
currently lose_sum: 96.47582823038101
time_elpased: 1.918
batch start
#iterations: 106
currently lose_sum: 96.8357241153717
time_elpased: 1.841
batch start
#iterations: 107
currently lose_sum: 96.97254282236099
time_elpased: 1.878
batch start
#iterations: 108
currently lose_sum: 96.6344935297966
time_elpased: 1.861
batch start
#iterations: 109
currently lose_sum: 96.59092110395432
time_elpased: 1.89
batch start
#iterations: 110
currently lose_sum: 96.86525857448578
time_elpased: 1.865
batch start
#iterations: 111
currently lose_sum: 96.3817218542099
time_elpased: 1.843
batch start
#iterations: 112
currently lose_sum: 97.02365124225616
time_elpased: 1.869
batch start
#iterations: 113
currently lose_sum: 96.79432255029678
time_elpased: 1.896
batch start
#iterations: 114
currently lose_sum: 96.38531875610352
time_elpased: 1.842
batch start
#iterations: 115
currently lose_sum: 96.5459536910057
time_elpased: 1.914
batch start
#iterations: 116
currently lose_sum: 96.79823178052902
time_elpased: 1.852
batch start
#iterations: 117
currently lose_sum: 96.60160827636719
time_elpased: 1.881
batch start
#iterations: 118
currently lose_sum: 96.47490847110748
time_elpased: 1.891
batch start
#iterations: 119
currently lose_sum: 96.51711714267731
time_elpased: 1.89
start validation test
0.639072164948
0.621389609228
0.715138417207
0.664976076555
0.63893861891
62.371
batch start
#iterations: 120
currently lose_sum: 96.4492540359497
time_elpased: 1.838
batch start
#iterations: 121
currently lose_sum: 96.46913707256317
time_elpased: 1.856
batch start
#iterations: 122
currently lose_sum: 96.9021942615509
time_elpased: 1.85
batch start
#iterations: 123
currently lose_sum: 96.55261754989624
time_elpased: 1.85
batch start
#iterations: 124
currently lose_sum: 96.9419915676117
time_elpased: 1.873
batch start
#iterations: 125
currently lose_sum: 96.92479515075684
time_elpased: 1.921
batch start
#iterations: 126
currently lose_sum: 96.88397789001465
time_elpased: 1.861
batch start
#iterations: 127
currently lose_sum: 96.82767063379288
time_elpased: 1.896
batch start
#iterations: 128
currently lose_sum: 96.5317884683609
time_elpased: 1.91
batch start
#iterations: 129
currently lose_sum: 96.74660247564316
time_elpased: 1.862
batch start
#iterations: 130
currently lose_sum: 96.51532143354416
time_elpased: 1.843
batch start
#iterations: 131
currently lose_sum: 96.6225636601448
time_elpased: 1.881
batch start
#iterations: 132
currently lose_sum: 96.49669086933136
time_elpased: 1.853
batch start
#iterations: 133
currently lose_sum: 96.8752452135086
time_elpased: 1.894
batch start
#iterations: 134
currently lose_sum: 96.69872164726257
time_elpased: 1.895
batch start
#iterations: 135
currently lose_sum: 96.4281274676323
time_elpased: 1.872
batch start
#iterations: 136
currently lose_sum: 96.29982036352158
time_elpased: 1.89
batch start
#iterations: 137
currently lose_sum: 96.49386620521545
time_elpased: 1.856
batch start
#iterations: 138
currently lose_sum: 96.36567044258118
time_elpased: 1.861
batch start
#iterations: 139
currently lose_sum: 96.60768228769302
time_elpased: 1.871
start validation test
0.631907216495
0.622108456579
0.675311310075
0.647619047619
0.631831013914
62.357
batch start
#iterations: 140
currently lose_sum: 96.32742893695831
time_elpased: 1.893
batch start
#iterations: 141
currently lose_sum: 96.71081292629242
time_elpased: 1.83
batch start
#iterations: 142
currently lose_sum: 96.7068299651146
time_elpased: 1.857
batch start
#iterations: 143
currently lose_sum: 96.58224189281464
time_elpased: 1.855
batch start
#iterations: 144
currently lose_sum: 96.65210211277008
time_elpased: 1.895
batch start
#iterations: 145
currently lose_sum: 96.41600334644318
time_elpased: 1.867
batch start
#iterations: 146
currently lose_sum: 96.67340391874313
time_elpased: 1.876
batch start
#iterations: 147
currently lose_sum: 96.41720151901245
time_elpased: 1.878
batch start
#iterations: 148
currently lose_sum: 96.37701684236526
time_elpased: 1.914
batch start
#iterations: 149
currently lose_sum: 96.27303755283356
time_elpased: 1.886
batch start
#iterations: 150
currently lose_sum: 96.69709050655365
time_elpased: 1.882
batch start
#iterations: 151
currently lose_sum: 96.47457146644592
time_elpased: 1.842
batch start
#iterations: 152
currently lose_sum: 96.41515666246414
time_elpased: 1.881
batch start
#iterations: 153
currently lose_sum: 96.4811726808548
time_elpased: 1.889
batch start
#iterations: 154
currently lose_sum: 96.541588306427
time_elpased: 1.831
batch start
#iterations: 155
currently lose_sum: 96.386374771595
time_elpased: 1.889
batch start
#iterations: 156
currently lose_sum: 96.4349096417427
time_elpased: 1.874
batch start
#iterations: 157
currently lose_sum: 96.18695342540741
time_elpased: 1.888
batch start
#iterations: 158
currently lose_sum: 96.42796391248703
time_elpased: 1.873
batch start
#iterations: 159
currently lose_sum: 96.54675370454788
time_elpased: 1.871
start validation test
0.635979381443
0.613626636994
0.737779149943
0.67
0.635800656248
62.319
batch start
#iterations: 160
currently lose_sum: 96.16805160045624
time_elpased: 1.904
batch start
#iterations: 161
currently lose_sum: 96.50860023498535
time_elpased: 1.933
batch start
#iterations: 162
currently lose_sum: 96.35504001379013
time_elpased: 1.869
batch start
#iterations: 163
currently lose_sum: 96.6577177643776
time_elpased: 1.874
batch start
#iterations: 164
currently lose_sum: 96.28364765644073
time_elpased: 1.879
batch start
#iterations: 165
currently lose_sum: 96.31049662828445
time_elpased: 1.924
batch start
#iterations: 166
currently lose_sum: 96.27130949497223
time_elpased: 1.893
batch start
#iterations: 167
currently lose_sum: 96.48699295520782
time_elpased: 1.927
batch start
#iterations: 168
currently lose_sum: 96.33343720436096
time_elpased: 1.886
batch start
#iterations: 169
currently lose_sum: 96.66369563341141
time_elpased: 1.878
batch start
#iterations: 170
currently lose_sum: 96.68549889326096
time_elpased: 1.904
batch start
#iterations: 171
currently lose_sum: 96.4467111825943
time_elpased: 1.922
batch start
#iterations: 172
currently lose_sum: 96.35859823226929
time_elpased: 1.872
batch start
#iterations: 173
currently lose_sum: 96.59046483039856
time_elpased: 1.948
batch start
#iterations: 174
currently lose_sum: 96.29377883672714
time_elpased: 1.863
batch start
#iterations: 175
currently lose_sum: 96.09196108579636
time_elpased: 1.883
batch start
#iterations: 176
currently lose_sum: 96.19041621685028
time_elpased: 1.864
batch start
#iterations: 177
currently lose_sum: 96.19505286216736
time_elpased: 1.895
batch start
#iterations: 178
currently lose_sum: 96.71787863969803
time_elpased: 1.903
batch start
#iterations: 179
currently lose_sum: 96.30670928955078
time_elpased: 1.883
start validation test
0.636340206186
0.619608195543
0.709581146444
0.661549532262
0.636211620418
62.105
batch start
#iterations: 180
currently lose_sum: 96.26639968156815
time_elpased: 1.874
batch start
#iterations: 181
currently lose_sum: 96.17610055208206
time_elpased: 1.858
batch start
#iterations: 182
currently lose_sum: 96.60003751516342
time_elpased: 1.856
batch start
#iterations: 183
currently lose_sum: 96.2071368098259
time_elpased: 1.897
batch start
#iterations: 184
currently lose_sum: 96.4252837896347
time_elpased: 1.857
batch start
#iterations: 185
currently lose_sum: 96.48625320196152
time_elpased: 1.904
batch start
#iterations: 186
currently lose_sum: 96.47269958257675
time_elpased: 1.936
batch start
#iterations: 187
currently lose_sum: 96.44971239566803
time_elpased: 1.878
batch start
#iterations: 188
currently lose_sum: 96.57319486141205
time_elpased: 1.859
batch start
#iterations: 189
currently lose_sum: 96.17961746454239
time_elpased: 1.86
batch start
#iterations: 190
currently lose_sum: 96.58834999799728
time_elpased: 1.84
batch start
#iterations: 191
currently lose_sum: 96.47825348377228
time_elpased: 1.883
batch start
#iterations: 192
currently lose_sum: 96.21116727590561
time_elpased: 1.879
batch start
#iterations: 193
currently lose_sum: 96.38799494504929
time_elpased: 1.845
batch start
#iterations: 194
currently lose_sum: 96.33515983819962
time_elpased: 1.845
batch start
#iterations: 195
currently lose_sum: 96.4065334200859
time_elpased: 1.881
batch start
#iterations: 196
currently lose_sum: 96.06649547815323
time_elpased: 1.895
batch start
#iterations: 197
currently lose_sum: 96.33288592100143
time_elpased: 1.893
batch start
#iterations: 198
currently lose_sum: 96.34663945436478
time_elpased: 1.881
batch start
#iterations: 199
currently lose_sum: 96.19152730703354
time_elpased: 1.875
start validation test
0.637216494845
0.616144975288
0.731295667387
0.6688
0.637051324347
62.134
batch start
#iterations: 200
currently lose_sum: 96.25866913795471
time_elpased: 1.835
batch start
#iterations: 201
currently lose_sum: 96.38304054737091
time_elpased: 1.879
batch start
#iterations: 202
currently lose_sum: 96.27186214923859
time_elpased: 1.882
batch start
#iterations: 203
currently lose_sum: 96.09114348888397
time_elpased: 1.916
batch start
#iterations: 204
currently lose_sum: 96.28887742757797
time_elpased: 1.854
batch start
#iterations: 205
currently lose_sum: 96.38730919361115
time_elpased: 1.87
batch start
#iterations: 206
currently lose_sum: 96.10436570644379
time_elpased: 1.861
batch start
#iterations: 207
currently lose_sum: 96.33194518089294
time_elpased: 1.875
batch start
#iterations: 208
currently lose_sum: 96.10868149995804
time_elpased: 1.853
batch start
#iterations: 209
currently lose_sum: 96.11402308940887
time_elpased: 1.939
batch start
#iterations: 210
currently lose_sum: 96.66912370920181
time_elpased: 1.84
batch start
#iterations: 211
currently lose_sum: 96.66795653104782
time_elpased: 1.863
batch start
#iterations: 212
currently lose_sum: 96.34664165973663
time_elpased: 1.888
batch start
#iterations: 213
currently lose_sum: 96.21456044912338
time_elpased: 1.886
batch start
#iterations: 214
currently lose_sum: 96.5525414943695
time_elpased: 1.857
batch start
#iterations: 215
currently lose_sum: 96.00373405218124
time_elpased: 1.853
batch start
#iterations: 216
currently lose_sum: 96.1102904677391
time_elpased: 1.834
batch start
#iterations: 217
currently lose_sum: 96.19556850194931
time_elpased: 1.833
batch start
#iterations: 218
currently lose_sum: 95.87734949588776
time_elpased: 1.906
batch start
#iterations: 219
currently lose_sum: 96.39838510751724
time_elpased: 1.863
start validation test
0.639329896907
0.609236947791
0.7805907173
0.684350611269
0.639081891749
62.063
batch start
#iterations: 220
currently lose_sum: 96.24537873268127
time_elpased: 1.913
batch start
#iterations: 221
currently lose_sum: 96.20036119222641
time_elpased: 1.877
batch start
#iterations: 222
currently lose_sum: 96.01619577407837
time_elpased: 1.85
batch start
#iterations: 223
currently lose_sum: 96.29951810836792
time_elpased: 1.898
batch start
#iterations: 224
currently lose_sum: 96.20186430215836
time_elpased: 1.855
batch start
#iterations: 225
currently lose_sum: 96.45553690195084
time_elpased: 1.852
batch start
#iterations: 226
currently lose_sum: 96.07787984609604
time_elpased: 1.851
batch start
#iterations: 227
currently lose_sum: 95.99543291330338
time_elpased: 1.85
batch start
#iterations: 228
currently lose_sum: 96.1945076584816
time_elpased: 1.875
batch start
#iterations: 229
currently lose_sum: 96.3013408780098
time_elpased: 1.926
batch start
#iterations: 230
currently lose_sum: 96.29055655002594
time_elpased: 1.846
batch start
#iterations: 231
currently lose_sum: 96.37746810913086
time_elpased: 1.867
batch start
#iterations: 232
currently lose_sum: 96.18332016468048
time_elpased: 1.899
batch start
#iterations: 233
currently lose_sum: 96.16260099411011
time_elpased: 1.854
batch start
#iterations: 234
currently lose_sum: 96.1013730764389
time_elpased: 1.907
batch start
#iterations: 235
currently lose_sum: 96.44378459453583
time_elpased: 1.897
batch start
#iterations: 236
currently lose_sum: 96.12945312261581
time_elpased: 1.883
batch start
#iterations: 237
currently lose_sum: 96.05083578824997
time_elpased: 1.867
batch start
#iterations: 238
currently lose_sum: 96.06666362285614
time_elpased: 1.896
batch start
#iterations: 239
currently lose_sum: 96.72332054376602
time_elpased: 1.875
start validation test
0.638608247423
0.608727097396
0.779561593084
0.683633410045
0.638360782084
62.239
batch start
#iterations: 240
currently lose_sum: 95.90768772363663
time_elpased: 1.866
batch start
#iterations: 241
currently lose_sum: 96.28226178884506
time_elpased: 1.878
batch start
#iterations: 242
currently lose_sum: 96.3622869849205
time_elpased: 1.89
batch start
#iterations: 243
currently lose_sum: 95.95758211612701
time_elpased: 1.874
batch start
#iterations: 244
currently lose_sum: 96.50373083353043
time_elpased: 1.872
batch start
#iterations: 245
currently lose_sum: 96.02275091409683
time_elpased: 1.889
batch start
#iterations: 246
currently lose_sum: 96.20233458280563
time_elpased: 1.872
batch start
#iterations: 247
currently lose_sum: 96.09963190555573
time_elpased: 1.879
batch start
#iterations: 248
currently lose_sum: 95.86293691396713
time_elpased: 1.858
batch start
#iterations: 249
currently lose_sum: 96.338447868824
time_elpased: 1.901
batch start
#iterations: 250
currently lose_sum: 96.33907288312912
time_elpased: 1.839
batch start
#iterations: 251
currently lose_sum: 95.95865571498871
time_elpased: 1.916
batch start
#iterations: 252
currently lose_sum: 96.30106419324875
time_elpased: 1.835
batch start
#iterations: 253
currently lose_sum: 96.15517520904541
time_elpased: 1.843
batch start
#iterations: 254
currently lose_sum: 96.03643369674683
time_elpased: 1.878
batch start
#iterations: 255
currently lose_sum: 96.1527065038681
time_elpased: 1.841
batch start
#iterations: 256
currently lose_sum: 96.33553141355515
time_elpased: 1.907
batch start
#iterations: 257
currently lose_sum: 95.9053122997284
time_elpased: 1.857
batch start
#iterations: 258
currently lose_sum: 96.05122417211533
time_elpased: 1.862
batch start
#iterations: 259
currently lose_sum: 96.0740812420845
time_elpased: 1.877
start validation test
0.637835051546
0.618661257606
0.721930637028
0.666318389058
0.637687408775
62.169
batch start
#iterations: 260
currently lose_sum: 96.15728831291199
time_elpased: 1.873
batch start
#iterations: 261
currently lose_sum: 96.30065655708313
time_elpased: 1.874
batch start
#iterations: 262
currently lose_sum: 96.0528039932251
time_elpased: 1.922
batch start
#iterations: 263
currently lose_sum: 96.36519205570221
time_elpased: 1.907
batch start
#iterations: 264
currently lose_sum: 96.1920376420021
time_elpased: 1.861
batch start
#iterations: 265
currently lose_sum: 96.31928288936615
time_elpased: 1.852
batch start
#iterations: 266
currently lose_sum: 96.01094168424606
time_elpased: 1.872
batch start
#iterations: 267
currently lose_sum: 95.70618635416031
time_elpased: 1.873
batch start
#iterations: 268
currently lose_sum: 96.19789731502533
time_elpased: 1.836
batch start
#iterations: 269
currently lose_sum: 96.21009165048599
time_elpased: 1.879
batch start
#iterations: 270
currently lose_sum: 95.83951276540756
time_elpased: 1.833
batch start
#iterations: 271
currently lose_sum: 95.99354660511017
time_elpased: 1.879
batch start
#iterations: 272
currently lose_sum: 96.01043742895126
time_elpased: 1.901
batch start
#iterations: 273
currently lose_sum: 95.8306776881218
time_elpased: 1.857
batch start
#iterations: 274
currently lose_sum: 95.80008083581924
time_elpased: 1.863
batch start
#iterations: 275
currently lose_sum: 96.21826475858688
time_elpased: 1.888
batch start
#iterations: 276
currently lose_sum: 95.97642529010773
time_elpased: 1.867
batch start
#iterations: 277
currently lose_sum: 95.92047798633575
time_elpased: 1.863
batch start
#iterations: 278
currently lose_sum: 96.16797435283661
time_elpased: 1.869
batch start
#iterations: 279
currently lose_sum: 96.11607110500336
time_elpased: 1.886
start validation test
0.634175257732
0.60792552315
0.759390758465
0.675268817204
0.633955422607
62.220
batch start
#iterations: 280
currently lose_sum: 96.34749984741211
time_elpased: 1.847
batch start
#iterations: 281
currently lose_sum: 96.18116343021393
time_elpased: 1.842
batch start
#iterations: 282
currently lose_sum: 95.96964383125305
time_elpased: 1.873
batch start
#iterations: 283
currently lose_sum: 96.47659814357758
time_elpased: 1.944
batch start
#iterations: 284
currently lose_sum: 95.8816230893135
time_elpased: 1.9
batch start
#iterations: 285
currently lose_sum: 95.80103361606598
time_elpased: 1.908
batch start
#iterations: 286
currently lose_sum: 96.21628165245056
time_elpased: 1.825
batch start
#iterations: 287
currently lose_sum: 95.9384052157402
time_elpased: 1.895
batch start
#iterations: 288
currently lose_sum: 95.7631099820137
time_elpased: 1.846
batch start
#iterations: 289
currently lose_sum: 96.1623123884201
time_elpased: 1.87
batch start
#iterations: 290
currently lose_sum: 95.94315969944
time_elpased: 1.863
batch start
#iterations: 291
currently lose_sum: 96.54574924707413
time_elpased: 1.87
batch start
#iterations: 292
currently lose_sum: 96.25979197025299
time_elpased: 1.876
batch start
#iterations: 293
currently lose_sum: 95.94041836261749
time_elpased: 1.864
batch start
#iterations: 294
currently lose_sum: 96.2239745259285
time_elpased: 1.84
batch start
#iterations: 295
currently lose_sum: 96.15749806165695
time_elpased: 1.906
batch start
#iterations: 296
currently lose_sum: 96.165642619133
time_elpased: 1.844
batch start
#iterations: 297
currently lose_sum: 96.18397778272629
time_elpased: 1.88
batch start
#iterations: 298
currently lose_sum: 96.22359496355057
time_elpased: 1.857
batch start
#iterations: 299
currently lose_sum: 96.14024156332016
time_elpased: 1.879
start validation test
0.640154639175
0.612131147541
0.768549963981
0.68148012958
0.639929221379
61.948
batch start
#iterations: 300
currently lose_sum: 95.9081814289093
time_elpased: 1.871
batch start
#iterations: 301
currently lose_sum: 96.4728194475174
time_elpased: 1.895
batch start
#iterations: 302
currently lose_sum: 96.31549602746964
time_elpased: 1.848
batch start
#iterations: 303
currently lose_sum: 96.1636843085289
time_elpased: 1.87
batch start
#iterations: 304
currently lose_sum: 96.36300766468048
time_elpased: 1.9
batch start
#iterations: 305
currently lose_sum: 96.1584222316742
time_elpased: 1.871
batch start
#iterations: 306
currently lose_sum: 95.70271521806717
time_elpased: 1.873
batch start
#iterations: 307
currently lose_sum: 96.0212008357048
time_elpased: 1.894
batch start
#iterations: 308
currently lose_sum: 96.467335999012
time_elpased: 1.838
batch start
#iterations: 309
currently lose_sum: 95.75144308805466
time_elpased: 1.844
batch start
#iterations: 310
currently lose_sum: 96.12912875413895
time_elpased: 1.865
batch start
#iterations: 311
currently lose_sum: 96.13338088989258
time_elpased: 1.897
batch start
#iterations: 312
currently lose_sum: 96.01659673452377
time_elpased: 1.903
batch start
#iterations: 313
currently lose_sum: 96.01369321346283
time_elpased: 1.898
batch start
#iterations: 314
currently lose_sum: 96.18928509950638
time_elpased: 1.801
batch start
#iterations: 315
currently lose_sum: 96.05053001642227
time_elpased: 1.955
batch start
#iterations: 316
currently lose_sum: 95.81980854272842
time_elpased: 1.895
batch start
#iterations: 317
currently lose_sum: 96.07674664258957
time_elpased: 1.899
batch start
#iterations: 318
currently lose_sum: 96.2381574511528
time_elpased: 1.862
batch start
#iterations: 319
currently lose_sum: 96.45243608951569
time_elpased: 1.9
start validation test
0.63587628866
0.615518592702
0.727384995369
0.66679245283
0.635715631011
62.335
batch start
#iterations: 320
currently lose_sum: 95.92245727777481
time_elpased: 1.885
batch start
#iterations: 321
currently lose_sum: 96.23947739601135
time_elpased: 1.872
batch start
#iterations: 322
currently lose_sum: 96.1810182929039
time_elpased: 1.859
batch start
#iterations: 323
currently lose_sum: 96.25157046318054
time_elpased: 1.87
batch start
#iterations: 324
currently lose_sum: 96.15458196401596
time_elpased: 1.882
batch start
#iterations: 325
currently lose_sum: 96.23061835765839
time_elpased: 1.881
batch start
#iterations: 326
currently lose_sum: 95.74190962314606
time_elpased: 1.867
batch start
#iterations: 327
currently lose_sum: 95.86537081003189
time_elpased: 1.879
batch start
#iterations: 328
currently lose_sum: 96.30446028709412
time_elpased: 1.889
batch start
#iterations: 329
currently lose_sum: 95.84476935863495
time_elpased: 1.86
batch start
#iterations: 330
currently lose_sum: 96.29663163423538
time_elpased: 1.86
batch start
#iterations: 331
currently lose_sum: 95.97773945331573
time_elpased: 1.891
batch start
#iterations: 332
currently lose_sum: 95.53663903474808
time_elpased: 1.888
batch start
#iterations: 333
currently lose_sum: 96.02289652824402
time_elpased: 1.885
batch start
#iterations: 334
currently lose_sum: 96.30915057659149
time_elpased: 1.843
batch start
#iterations: 335
currently lose_sum: 96.20658057928085
time_elpased: 1.912
batch start
#iterations: 336
currently lose_sum: 95.75910574197769
time_elpased: 1.901
batch start
#iterations: 337
currently lose_sum: 96.44019919633865
time_elpased: 1.96
batch start
#iterations: 338
currently lose_sum: 96.16866511106491
time_elpased: 1.903
batch start
#iterations: 339
currently lose_sum: 96.01855677366257
time_elpased: 1.899
start validation test
0.635103092784
0.613628532047
0.733045178553
0.668042203986
0.634931140345
62.207
batch start
#iterations: 340
currently lose_sum: 96.1064465045929
time_elpased: 1.867
batch start
#iterations: 341
currently lose_sum: 95.84319245815277
time_elpased: 1.877
batch start
#iterations: 342
currently lose_sum: 96.12379986047745
time_elpased: 1.865
batch start
#iterations: 343
currently lose_sum: 96.00684994459152
time_elpased: 1.922
batch start
#iterations: 344
currently lose_sum: 95.79964989423752
time_elpased: 1.859
batch start
#iterations: 345
currently lose_sum: 96.16357070207596
time_elpased: 1.887
batch start
#iterations: 346
currently lose_sum: 96.0849637389183
time_elpased: 1.881
batch start
#iterations: 347
currently lose_sum: 96.0446628332138
time_elpased: 1.838
batch start
#iterations: 348
currently lose_sum: 95.89424151182175
time_elpased: 1.855
batch start
#iterations: 349
currently lose_sum: 96.06649321317673
time_elpased: 1.907
batch start
#iterations: 350
currently lose_sum: 96.23494309186935
time_elpased: 1.88
batch start
#iterations: 351
currently lose_sum: 95.87437301874161
time_elpased: 1.869
batch start
#iterations: 352
currently lose_sum: 96.19163274765015
time_elpased: 1.864
batch start
#iterations: 353
currently lose_sum: 96.04379850625992
time_elpased: 1.864
batch start
#iterations: 354
currently lose_sum: 96.56933653354645
time_elpased: 1.895
batch start
#iterations: 355
currently lose_sum: 96.03611266613007
time_elpased: 1.882
batch start
#iterations: 356
currently lose_sum: 96.26309251785278
time_elpased: 1.88
batch start
#iterations: 357
currently lose_sum: 96.16030305624008
time_elpased: 1.916
batch start
#iterations: 358
currently lose_sum: 96.25746297836304
time_elpased: 1.87
batch start
#iterations: 359
currently lose_sum: 96.1516774892807
time_elpased: 1.87
start validation test
0.636391752577
0.608437169151
0.768858701245
0.679305328241
0.636159186417
62.114
batch start
#iterations: 360
currently lose_sum: 95.92695301771164
time_elpased: 1.865
batch start
#iterations: 361
currently lose_sum: 96.07782226800919
time_elpased: 1.882
batch start
#iterations: 362
currently lose_sum: 95.97342926263809
time_elpased: 1.857
batch start
#iterations: 363
currently lose_sum: 95.90513944625854
time_elpased: 1.898
batch start
#iterations: 364
currently lose_sum: 96.21864455938339
time_elpased: 1.826
batch start
#iterations: 365
currently lose_sum: 96.02355253696442
time_elpased: 1.854
batch start
#iterations: 366
currently lose_sum: 96.05008614063263
time_elpased: 1.874
batch start
#iterations: 367
currently lose_sum: 96.02020794153214
time_elpased: 1.89
batch start
#iterations: 368
currently lose_sum: 95.93727427721024
time_elpased: 1.886
batch start
#iterations: 369
currently lose_sum: 95.89770609140396
time_elpased: 1.835
batch start
#iterations: 370
currently lose_sum: 95.81370228528976
time_elpased: 1.884
batch start
#iterations: 371
currently lose_sum: 95.91552919149399
time_elpased: 1.839
batch start
#iterations: 372
currently lose_sum: 96.0016959309578
time_elpased: 1.92
batch start
#iterations: 373
currently lose_sum: 95.76084089279175
time_elpased: 1.907
batch start
#iterations: 374
currently lose_sum: 96.12333965301514
time_elpased: 1.859
batch start
#iterations: 375
currently lose_sum: 96.27623480558395
time_elpased: 1.879
batch start
#iterations: 376
currently lose_sum: 95.93184983730316
time_elpased: 1.849
batch start
#iterations: 377
currently lose_sum: 95.8637113571167
time_elpased: 1.854
batch start
#iterations: 378
currently lose_sum: 96.43890845775604
time_elpased: 1.903
batch start
#iterations: 379
currently lose_sum: 95.85533171892166
time_elpased: 1.895
start validation test
0.63381443299
0.608720978614
0.752804363487
0.673138860771
0.633605527814
62.209
batch start
#iterations: 380
currently lose_sum: 96.22312873601913
time_elpased: 1.876
batch start
#iterations: 381
currently lose_sum: 95.80431818962097
time_elpased: 1.908
batch start
#iterations: 382
currently lose_sum: 96.22017830610275
time_elpased: 1.863
batch start
#iterations: 383
currently lose_sum: 96.30598968267441
time_elpased: 1.854
batch start
#iterations: 384
currently lose_sum: 96.24458068609238
time_elpased: 1.884
batch start
#iterations: 385
currently lose_sum: 96.01601791381836
time_elpased: 1.889
batch start
#iterations: 386
currently lose_sum: 96.08202093839645
time_elpased: 1.891
batch start
#iterations: 387
currently lose_sum: 96.31453895568848
time_elpased: 1.926
batch start
#iterations: 388
currently lose_sum: 96.12808984518051
time_elpased: 1.863
batch start
#iterations: 389
currently lose_sum: 96.36631286144257
time_elpased: 1.923
batch start
#iterations: 390
currently lose_sum: 96.01789736747742
time_elpased: 1.862
batch start
#iterations: 391
currently lose_sum: 95.87847179174423
time_elpased: 1.895
batch start
#iterations: 392
currently lose_sum: 95.94360899925232
time_elpased: 1.869
batch start
#iterations: 393
currently lose_sum: 95.82186776399612
time_elpased: 1.898
batch start
#iterations: 394
currently lose_sum: 96.06916517019272
time_elpased: 1.872
batch start
#iterations: 395
currently lose_sum: 96.23358821868896
time_elpased: 1.859
batch start
#iterations: 396
currently lose_sum: 95.78613984584808
time_elpased: 1.926
batch start
#iterations: 397
currently lose_sum: 95.84450149536133
time_elpased: 1.865
batch start
#iterations: 398
currently lose_sum: 95.98176521062851
time_elpased: 1.904
batch start
#iterations: 399
currently lose_sum: 95.99521702528
time_elpased: 1.885
start validation test
0.637319587629
0.609688241551
0.766800452815
0.679277965175
0.637092263999
62.074
acc: 0.640
pre: 0.612
rec: 0.768
F1: 0.681
auc: 0.681
