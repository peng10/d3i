start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.8819562792778
time_elpased: 1.837
batch start
#iterations: 1
currently lose_sum: 100.30358386039734
time_elpased: 1.891
batch start
#iterations: 2
currently lose_sum: 99.9533502459526
time_elpased: 1.869
batch start
#iterations: 3
currently lose_sum: 99.81687873601913
time_elpased: 1.847
batch start
#iterations: 4
currently lose_sum: 99.82194274663925
time_elpased: 1.878
batch start
#iterations: 5
currently lose_sum: 99.64227771759033
time_elpased: 1.922
batch start
#iterations: 6
currently lose_sum: 99.42950171232224
time_elpased: 1.866
batch start
#iterations: 7
currently lose_sum: 99.12563395500183
time_elpased: 1.853
batch start
#iterations: 8
currently lose_sum: 99.10071700811386
time_elpased: 1.874
batch start
#iterations: 9
currently lose_sum: 98.85049229860306
time_elpased: 1.88
batch start
#iterations: 10
currently lose_sum: 98.80909502506256
time_elpased: 1.873
batch start
#iterations: 11
currently lose_sum: 98.89922392368317
time_elpased: 1.848
batch start
#iterations: 12
currently lose_sum: 98.38461291790009
time_elpased: 1.859
batch start
#iterations: 13
currently lose_sum: 98.48509514331818
time_elpased: 1.847
batch start
#iterations: 14
currently lose_sum: 98.36501151323318
time_elpased: 1.862
batch start
#iterations: 15
currently lose_sum: 98.32048600912094
time_elpased: 1.889
batch start
#iterations: 16
currently lose_sum: 98.37399232387543
time_elpased: 1.852
batch start
#iterations: 17
currently lose_sum: 98.18621182441711
time_elpased: 1.868
batch start
#iterations: 18
currently lose_sum: 98.25492423772812
time_elpased: 1.916
batch start
#iterations: 19
currently lose_sum: 97.97735869884491
time_elpased: 1.876
start validation test
0.62706185567
0.614047991174
0.68742280774
0.648667022775
0.626962126712
63.642
batch start
#iterations: 20
currently lose_sum: 97.91607797145844
time_elpased: 1.882
batch start
#iterations: 21
currently lose_sum: 97.92369055747986
time_elpased: 1.887
batch start
#iterations: 22
currently lose_sum: 97.79881942272186
time_elpased: 1.843
batch start
#iterations: 23
currently lose_sum: 97.83227151632309
time_elpased: 1.902
batch start
#iterations: 24
currently lose_sum: 97.68015438318253
time_elpased: 1.847
batch start
#iterations: 25
currently lose_sum: 97.49184876680374
time_elpased: 1.913
batch start
#iterations: 26
currently lose_sum: 97.69065171480179
time_elpased: 1.888
batch start
#iterations: 27
currently lose_sum: 97.6082689166069
time_elpased: 1.878
batch start
#iterations: 28
currently lose_sum: 97.54598182439804
time_elpased: 1.815
batch start
#iterations: 29
currently lose_sum: 97.69447404146194
time_elpased: 1.88
batch start
#iterations: 30
currently lose_sum: 97.4093673825264
time_elpased: 1.86
batch start
#iterations: 31
currently lose_sum: 97.60773706436157
time_elpased: 1.915
batch start
#iterations: 32
currently lose_sum: 97.35060173273087
time_elpased: 1.829
batch start
#iterations: 33
currently lose_sum: 97.4488610625267
time_elpased: 1.906
batch start
#iterations: 34
currently lose_sum: 97.08856046199799
time_elpased: 1.885
batch start
#iterations: 35
currently lose_sum: 97.37769395112991
time_elpased: 1.87
batch start
#iterations: 36
currently lose_sum: 97.07008773088455
time_elpased: 1.913
batch start
#iterations: 37
currently lose_sum: 97.35054188966751
time_elpased: 1.873
batch start
#iterations: 38
currently lose_sum: 97.22120493650436
time_elpased: 1.852
batch start
#iterations: 39
currently lose_sum: 97.26831543445587
time_elpased: 1.821
start validation test
0.618711340206
0.615730112786
0.634932070811
0.62518368381
0.618684540156
63.317
batch start
#iterations: 40
currently lose_sum: 97.0736032128334
time_elpased: 1.906
batch start
#iterations: 41
currently lose_sum: 97.09274905920029
time_elpased: 1.815
batch start
#iterations: 42
currently lose_sum: 97.0381931066513
time_elpased: 1.862
batch start
#iterations: 43
currently lose_sum: 97.00576657056808
time_elpased: 1.869
batch start
#iterations: 44
currently lose_sum: 97.17919826507568
time_elpased: 1.814
batch start
#iterations: 45
currently lose_sum: 97.25840699672699
time_elpased: 1.847
batch start
#iterations: 46
currently lose_sum: 97.19295883178711
time_elpased: 1.882
batch start
#iterations: 47
currently lose_sum: 96.85841065645218
time_elpased: 1.855
batch start
#iterations: 48
currently lose_sum: 96.9960685968399
time_elpased: 1.856
batch start
#iterations: 49
currently lose_sum: 97.00569993257523
time_elpased: 1.889
batch start
#iterations: 50
currently lose_sum: 97.21830481290817
time_elpased: 1.849
batch start
#iterations: 51
currently lose_sum: 96.92553627490997
time_elpased: 1.885
batch start
#iterations: 52
currently lose_sum: 97.22437781095505
time_elpased: 1.889
batch start
#iterations: 53
currently lose_sum: 96.86414116621017
time_elpased: 1.846
batch start
#iterations: 54
currently lose_sum: 96.91747814416885
time_elpased: 1.871
batch start
#iterations: 55
currently lose_sum: 96.74124419689178
time_elpased: 1.85
batch start
#iterations: 56
currently lose_sum: 97.12607878446579
time_elpased: 1.886
batch start
#iterations: 57
currently lose_sum: 96.8605929017067
time_elpased: 1.874
batch start
#iterations: 58
currently lose_sum: 96.68192327022552
time_elpased: 1.912
batch start
#iterations: 59
currently lose_sum: 96.90500354766846
time_elpased: 1.846
start validation test
0.634793814433
0.611568145195
0.742177850967
0.670572371786
0.634616393472
62.595
batch start
#iterations: 60
currently lose_sum: 96.8617108464241
time_elpased: 1.894
batch start
#iterations: 61
currently lose_sum: 97.2072361111641
time_elpased: 1.887
batch start
#iterations: 62
currently lose_sum: 97.333471596241
time_elpased: 1.902
batch start
#iterations: 63
currently lose_sum: 96.7132271528244
time_elpased: 1.934
batch start
#iterations: 64
currently lose_sum: 96.78638738393784
time_elpased: 1.835
batch start
#iterations: 65
currently lose_sum: 96.90025526285172
time_elpased: 1.872
batch start
#iterations: 66
currently lose_sum: 96.73017275333405
time_elpased: 1.902
batch start
#iterations: 67
currently lose_sum: 96.77281904220581
time_elpased: 1.871
batch start
#iterations: 68
currently lose_sum: 96.88013941049576
time_elpased: 1.896
batch start
#iterations: 69
currently lose_sum: 96.76684242486954
time_elpased: 1.909
batch start
#iterations: 70
currently lose_sum: 96.8898577094078
time_elpased: 1.827
batch start
#iterations: 71
currently lose_sum: 96.54509890079498
time_elpased: 1.855
batch start
#iterations: 72
currently lose_sum: 96.5458357334137
time_elpased: 1.886
batch start
#iterations: 73
currently lose_sum: 96.59513103961945
time_elpased: 1.911
batch start
#iterations: 74
currently lose_sum: 96.97867268323898
time_elpased: 1.876
batch start
#iterations: 75
currently lose_sum: 96.95427668094635
time_elpased: 1.829
batch start
#iterations: 76
currently lose_sum: 96.96378529071808
time_elpased: 1.839
batch start
#iterations: 77
currently lose_sum: 96.77337658405304
time_elpased: 1.877
batch start
#iterations: 78
currently lose_sum: 96.94917297363281
time_elpased: 1.897
batch start
#iterations: 79
currently lose_sum: 96.5081290602684
time_elpased: 1.89
start validation test
0.627216494845
0.619148119724
0.664265129683
0.640913604767
0.627155282727
62.700
batch start
#iterations: 80
currently lose_sum: 96.82865738868713
time_elpased: 1.855
batch start
#iterations: 81
currently lose_sum: 96.62235182523727
time_elpased: 1.84
batch start
#iterations: 82
currently lose_sum: 96.4621542096138
time_elpased: 1.83
batch start
#iterations: 83
currently lose_sum: 96.71292126178741
time_elpased: 1.833
batch start
#iterations: 84
currently lose_sum: 96.53079843521118
time_elpased: 1.887
batch start
#iterations: 85
currently lose_sum: 96.43246269226074
time_elpased: 1.849
batch start
#iterations: 86
currently lose_sum: 96.63672590255737
time_elpased: 1.866
batch start
#iterations: 87
currently lose_sum: 96.34634566307068
time_elpased: 1.896
batch start
#iterations: 88
currently lose_sum: 96.88301813602448
time_elpased: 1.826
batch start
#iterations: 89
currently lose_sum: 96.6969586610794
time_elpased: 1.915
batch start
#iterations: 90
currently lose_sum: 96.56194680929184
time_elpased: 1.905
batch start
#iterations: 91
currently lose_sum: 96.72705382108688
time_elpased: 1.851
batch start
#iterations: 92
currently lose_sum: 96.29516649246216
time_elpased: 1.878
batch start
#iterations: 93
currently lose_sum: 96.61481237411499
time_elpased: 1.859
batch start
#iterations: 94
currently lose_sum: 96.5653635263443
time_elpased: 1.894
batch start
#iterations: 95
currently lose_sum: 96.8077826499939
time_elpased: 1.892
batch start
#iterations: 96
currently lose_sum: 96.35212850570679
time_elpased: 1.864
batch start
#iterations: 97
currently lose_sum: 96.20866560935974
time_elpased: 1.848
batch start
#iterations: 98
currently lose_sum: 96.48139625787735
time_elpased: 1.909
batch start
#iterations: 99
currently lose_sum: 96.56814777851105
time_elpased: 1.844
start validation test
0.620309278351
0.611522399393
0.663132976534
0.636282836263
0.620238524615
62.695
batch start
#iterations: 100
currently lose_sum: 96.61037331819534
time_elpased: 1.868
batch start
#iterations: 101
currently lose_sum: 96.64283657073975
time_elpased: 1.874
batch start
#iterations: 102
currently lose_sum: 96.63134342432022
time_elpased: 1.89
batch start
#iterations: 103
currently lose_sum: 96.59812670946121
time_elpased: 1.855
batch start
#iterations: 104
currently lose_sum: 96.53465855121613
time_elpased: 1.886
batch start
#iterations: 105
currently lose_sum: 96.44942957162857
time_elpased: 1.826
batch start
#iterations: 106
currently lose_sum: 96.42133408784866
time_elpased: 1.881
batch start
#iterations: 107
currently lose_sum: 96.97655892372131
time_elpased: 1.847
batch start
#iterations: 108
currently lose_sum: 96.43294209241867
time_elpased: 1.915
batch start
#iterations: 109
currently lose_sum: 96.31423372030258
time_elpased: 1.86
batch start
#iterations: 110
currently lose_sum: 96.36807107925415
time_elpased: 1.849
batch start
#iterations: 111
currently lose_sum: 96.54565805196762
time_elpased: 1.932
batch start
#iterations: 112
currently lose_sum: 96.34684896469116
time_elpased: 1.894
batch start
#iterations: 113
currently lose_sum: 96.51783972978592
time_elpased: 1.877
batch start
#iterations: 114
currently lose_sum: 96.33378463983536
time_elpased: 1.86
batch start
#iterations: 115
currently lose_sum: 96.40240120887756
time_elpased: 1.837
batch start
#iterations: 116
currently lose_sum: 96.7625378370285
time_elpased: 1.85
batch start
#iterations: 117
currently lose_sum: 96.4466478228569
time_elpased: 1.855
batch start
#iterations: 118
currently lose_sum: 97.02020663022995
time_elpased: 1.855
batch start
#iterations: 119
currently lose_sum: 96.43917512893677
time_elpased: 1.852
start validation test
0.634948453608
0.61794734014
0.710168793742
0.660856239824
0.634824173823
62.401
batch start
#iterations: 120
currently lose_sum: 96.22928941249847
time_elpased: 1.844
batch start
#iterations: 121
currently lose_sum: 96.45800268650055
time_elpased: 1.863
batch start
#iterations: 122
currently lose_sum: 96.57154262065887
time_elpased: 1.881
batch start
#iterations: 123
currently lose_sum: 96.43489408493042
time_elpased: 1.818
batch start
#iterations: 124
currently lose_sum: 96.57414454221725
time_elpased: 1.872
batch start
#iterations: 125
currently lose_sum: 96.84886783361435
time_elpased: 1.855
batch start
#iterations: 126
currently lose_sum: 96.27201062440872
time_elpased: 1.85
batch start
#iterations: 127
currently lose_sum: 96.74081122875214
time_elpased: 1.834
batch start
#iterations: 128
currently lose_sum: 96.4575566649437
time_elpased: 1.854
batch start
#iterations: 129
currently lose_sum: 96.26448911428452
time_elpased: 1.831
batch start
#iterations: 130
currently lose_sum: 96.23310196399689
time_elpased: 1.9
batch start
#iterations: 131
currently lose_sum: 96.2854915857315
time_elpased: 1.862
batch start
#iterations: 132
currently lose_sum: 96.3692137002945
time_elpased: 1.85
batch start
#iterations: 133
currently lose_sum: 96.42941546440125
time_elpased: 1.84
batch start
#iterations: 134
currently lose_sum: 96.41798055171967
time_elpased: 1.882
batch start
#iterations: 135
currently lose_sum: 96.32511168718338
time_elpased: 1.855
batch start
#iterations: 136
currently lose_sum: 96.4378371834755
time_elpased: 1.875
batch start
#iterations: 137
currently lose_sum: 96.03574931621552
time_elpased: 1.867
batch start
#iterations: 138
currently lose_sum: 96.50983625650406
time_elpased: 1.864
batch start
#iterations: 139
currently lose_sum: 96.30538487434387
time_elpased: 1.792
start validation test
0.631546391753
0.608721422523
0.739913544669
0.667936448945
0.631367346477
62.399
batch start
#iterations: 140
currently lose_sum: 95.94446730613708
time_elpased: 1.854
batch start
#iterations: 141
currently lose_sum: 96.6144899725914
time_elpased: 1.871
batch start
#iterations: 142
currently lose_sum: 96.305235683918
time_elpased: 1.814
batch start
#iterations: 143
currently lose_sum: 96.20135921239853
time_elpased: 1.834
batch start
#iterations: 144
currently lose_sum: 96.6351969242096
time_elpased: 1.886
batch start
#iterations: 145
currently lose_sum: 96.22141206264496
time_elpased: 1.863
batch start
#iterations: 146
currently lose_sum: 96.04306054115295
time_elpased: 1.865
batch start
#iterations: 147
currently lose_sum: 96.43357843160629
time_elpased: 1.861
batch start
#iterations: 148
currently lose_sum: 96.76930022239685
time_elpased: 1.846
batch start
#iterations: 149
currently lose_sum: 96.76938199996948
time_elpased: 1.82
batch start
#iterations: 150
currently lose_sum: 96.1571397781372
time_elpased: 1.879
batch start
#iterations: 151
currently lose_sum: 96.54270273447037
time_elpased: 1.899
batch start
#iterations: 152
currently lose_sum: 96.22168600559235
time_elpased: 1.848
batch start
#iterations: 153
currently lose_sum: 96.36531537771225
time_elpased: 1.807
batch start
#iterations: 154
currently lose_sum: 96.26036900281906
time_elpased: 1.889
batch start
#iterations: 155
currently lose_sum: 96.44139415025711
time_elpased: 1.851
batch start
#iterations: 156
currently lose_sum: 96.34361100196838
time_elpased: 1.91
batch start
#iterations: 157
currently lose_sum: 96.29328405857086
time_elpased: 1.826
batch start
#iterations: 158
currently lose_sum: 96.362883746624
time_elpased: 1.93
batch start
#iterations: 159
currently lose_sum: 96.32423841953278
time_elpased: 1.847
start validation test
0.630360824742
0.611339574766
0.719123095924
0.660865452826
0.630214170845
62.446
batch start
#iterations: 160
currently lose_sum: 96.19918656349182
time_elpased: 1.825
batch start
#iterations: 161
currently lose_sum: 96.5246753692627
time_elpased: 1.834
batch start
#iterations: 162
currently lose_sum: 96.18172544240952
time_elpased: 1.883
batch start
#iterations: 163
currently lose_sum: 96.34597796201706
time_elpased: 1.822
batch start
#iterations: 164
currently lose_sum: 96.38291388750076
time_elpased: 1.866
batch start
#iterations: 165
currently lose_sum: 96.31021851301193
time_elpased: 1.855
batch start
#iterations: 166
currently lose_sum: 96.22460705041885
time_elpased: 1.829
batch start
#iterations: 167
currently lose_sum: 96.61800628900528
time_elpased: 1.839
batch start
#iterations: 168
currently lose_sum: 96.13317614793777
time_elpased: 1.827
batch start
#iterations: 169
currently lose_sum: 96.0336023569107
time_elpased: 1.837
batch start
#iterations: 170
currently lose_sum: 95.83478146791458
time_elpased: 1.863
batch start
#iterations: 171
currently lose_sum: 96.23370641469955
time_elpased: 1.866
batch start
#iterations: 172
currently lose_sum: 96.16011410951614
time_elpased: 1.887
batch start
#iterations: 173
currently lose_sum: 96.26568329334259
time_elpased: 1.894
batch start
#iterations: 174
currently lose_sum: 96.57085770368576
time_elpased: 1.821
batch start
#iterations: 175
currently lose_sum: 96.15948724746704
time_elpased: 1.862
batch start
#iterations: 176
currently lose_sum: 95.94308376312256
time_elpased: 1.867
batch start
#iterations: 177
currently lose_sum: 96.28851187229156
time_elpased: 1.83
batch start
#iterations: 178
currently lose_sum: 96.38542538881302
time_elpased: 1.818
batch start
#iterations: 179
currently lose_sum: 96.18613708019257
time_elpased: 1.819
start validation test
0.628298969072
0.618821743668
0.67136681762
0.6440242879
0.628227811949
62.610
batch start
#iterations: 180
currently lose_sum: 95.98905277252197
time_elpased: 1.858
batch start
#iterations: 181
currently lose_sum: 96.05049419403076
time_elpased: 1.824
batch start
#iterations: 182
currently lose_sum: 96.42780017852783
time_elpased: 1.83
batch start
#iterations: 183
currently lose_sum: 96.01726591587067
time_elpased: 1.832
batch start
#iterations: 184
currently lose_sum: 96.35108608007431
time_elpased: 1.826
batch start
#iterations: 185
currently lose_sum: 96.11105060577393
time_elpased: 1.832
batch start
#iterations: 186
currently lose_sum: 95.9449251294136
time_elpased: 1.854
batch start
#iterations: 187
currently lose_sum: 96.36661195755005
time_elpased: 1.821
batch start
#iterations: 188
currently lose_sum: 96.64341998100281
time_elpased: 1.869
batch start
#iterations: 189
currently lose_sum: 95.92342352867126
time_elpased: 1.803
batch start
#iterations: 190
currently lose_sum: 96.54403167963028
time_elpased: 1.855
batch start
#iterations: 191
currently lose_sum: 96.27431172132492
time_elpased: 1.861
batch start
#iterations: 192
currently lose_sum: 96.22607278823853
time_elpased: 1.853
batch start
#iterations: 193
currently lose_sum: 95.91737997531891
time_elpased: 1.841
batch start
#iterations: 194
currently lose_sum: 96.1809521317482
time_elpased: 1.91
batch start
#iterations: 195
currently lose_sum: 95.99106603860855
time_elpased: 1.87
batch start
#iterations: 196
currently lose_sum: 96.10582619905472
time_elpased: 1.869
batch start
#iterations: 197
currently lose_sum: 96.15939730405807
time_elpased: 1.808
batch start
#iterations: 198
currently lose_sum: 96.20328092575073
time_elpased: 1.845
batch start
#iterations: 199
currently lose_sum: 95.99299675226212
time_elpased: 1.892
start validation test
0.637474226804
0.611244713492
0.758645533141
0.677014925373
0.637274026381
62.327
batch start
#iterations: 200
currently lose_sum: 96.59921652078629
time_elpased: 1.852
batch start
#iterations: 201
currently lose_sum: 95.89699417352676
time_elpased: 1.803
batch start
#iterations: 202
currently lose_sum: 96.63520020246506
time_elpased: 1.817
batch start
#iterations: 203
currently lose_sum: 96.35764753818512
time_elpased: 1.818
batch start
#iterations: 204
currently lose_sum: 96.36907649040222
time_elpased: 1.869
batch start
#iterations: 205
currently lose_sum: 96.22059631347656
time_elpased: 1.836
batch start
#iterations: 206
currently lose_sum: 96.33196127414703
time_elpased: 1.852
batch start
#iterations: 207
currently lose_sum: 95.8324208855629
time_elpased: 1.839
batch start
#iterations: 208
currently lose_sum: 95.67476034164429
time_elpased: 1.881
batch start
#iterations: 209
currently lose_sum: 96.08829939365387
time_elpased: 1.894
batch start
#iterations: 210
currently lose_sum: 96.27680069208145
time_elpased: 1.876
batch start
#iterations: 211
currently lose_sum: 96.23563265800476
time_elpased: 1.837
batch start
#iterations: 212
currently lose_sum: 96.15944653749466
time_elpased: 1.873
batch start
#iterations: 213
currently lose_sum: 95.87055343389511
time_elpased: 1.868
batch start
#iterations: 214
currently lose_sum: 95.99012953042984
time_elpased: 1.854
batch start
#iterations: 215
currently lose_sum: 96.26050823926926
time_elpased: 1.871
batch start
#iterations: 216
currently lose_sum: 95.71768766641617
time_elpased: 1.821
batch start
#iterations: 217
currently lose_sum: 96.31916189193726
time_elpased: 1.846
batch start
#iterations: 218
currently lose_sum: 96.3381940126419
time_elpased: 1.84
batch start
#iterations: 219
currently lose_sum: 95.94485193490982
time_elpased: 1.816
start validation test
0.63793814433
0.615141146279
0.740119390696
0.671867700645
0.63776931947
62.191
batch start
#iterations: 220
currently lose_sum: 96.11071622371674
time_elpased: 1.928
batch start
#iterations: 221
currently lose_sum: 96.00403267145157
time_elpased: 1.84
batch start
#iterations: 222
currently lose_sum: 95.79169970750809
time_elpased: 1.83
batch start
#iterations: 223
currently lose_sum: 95.92436349391937
time_elpased: 1.863
batch start
#iterations: 224
currently lose_sum: 96.09204739332199
time_elpased: 1.863
batch start
#iterations: 225
currently lose_sum: 96.15854799747467
time_elpased: 1.863
batch start
#iterations: 226
currently lose_sum: 95.80696791410446
time_elpased: 1.886
batch start
#iterations: 227
currently lose_sum: 95.99448353052139
time_elpased: 1.874
batch start
#iterations: 228
currently lose_sum: 96.12581968307495
time_elpased: 1.803
batch start
#iterations: 229
currently lose_sum: 95.91114485263824
time_elpased: 1.817
batch start
#iterations: 230
currently lose_sum: 96.09812927246094
time_elpased: 1.85
batch start
#iterations: 231
currently lose_sum: 96.30778384208679
time_elpased: 1.853
batch start
#iterations: 232
currently lose_sum: 95.96311289072037
time_elpased: 1.901
batch start
#iterations: 233
currently lose_sum: 95.92421388626099
time_elpased: 1.837
batch start
#iterations: 234
currently lose_sum: 95.9900689125061
time_elpased: 1.859
batch start
#iterations: 235
currently lose_sum: 96.1919082403183
time_elpased: 1.882
batch start
#iterations: 236
currently lose_sum: 96.3314379453659
time_elpased: 1.882
batch start
#iterations: 237
currently lose_sum: 96.29642951488495
time_elpased: 1.864
batch start
#iterations: 238
currently lose_sum: 95.79507511854172
time_elpased: 1.899
batch start
#iterations: 239
currently lose_sum: 96.06563693284988
time_elpased: 1.836
start validation test
0.637628865979
0.613544132567
0.746912309592
0.673691050873
0.6374483068
62.215
batch start
#iterations: 240
currently lose_sum: 96.04690998792648
time_elpased: 1.892
batch start
#iterations: 241
currently lose_sum: 95.76091223955154
time_elpased: 1.873
batch start
#iterations: 242
currently lose_sum: 96.035973072052
time_elpased: 1.88
batch start
#iterations: 243
currently lose_sum: 96.3929113149643
time_elpased: 1.853
batch start
#iterations: 244
currently lose_sum: 95.61535555124283
time_elpased: 1.865
batch start
#iterations: 245
currently lose_sum: 96.20877540111542
time_elpased: 1.86
batch start
#iterations: 246
currently lose_sum: 95.69637888669968
time_elpased: 1.856
batch start
#iterations: 247
currently lose_sum: 95.93825089931488
time_elpased: 1.905
batch start
#iterations: 248
currently lose_sum: 96.16801828145981
time_elpased: 1.83
batch start
#iterations: 249
currently lose_sum: 96.57136255502701
time_elpased: 1.859
batch start
#iterations: 250
currently lose_sum: 96.12041592597961
time_elpased: 1.863
batch start
#iterations: 251
currently lose_sum: 95.79202300310135
time_elpased: 1.886
batch start
#iterations: 252
currently lose_sum: 95.86117023229599
time_elpased: 1.862
batch start
#iterations: 253
currently lose_sum: 96.25030642747879
time_elpased: 1.842
batch start
#iterations: 254
currently lose_sum: 95.95182770490646
time_elpased: 1.867
batch start
#iterations: 255
currently lose_sum: 95.88649398088455
time_elpased: 1.845
batch start
#iterations: 256
currently lose_sum: 95.88684582710266
time_elpased: 1.83
batch start
#iterations: 257
currently lose_sum: 96.32262116670609
time_elpased: 1.815
batch start
#iterations: 258
currently lose_sum: 96.03683227300644
time_elpased: 1.849
batch start
#iterations: 259
currently lose_sum: 96.18524265289307
time_elpased: 1.852
start validation test
0.635927835052
0.617316706465
0.718402634829
0.66403462874
0.635791569377
62.474
batch start
#iterations: 260
currently lose_sum: 96.10851794481277
time_elpased: 1.897
batch start
#iterations: 261
currently lose_sum: 96.10816115140915
time_elpased: 1.814
batch start
#iterations: 262
currently lose_sum: 95.77373868227005
time_elpased: 1.829
batch start
#iterations: 263
currently lose_sum: 95.91857671737671
time_elpased: 1.909
batch start
#iterations: 264
currently lose_sum: 95.94360661506653
time_elpased: 1.832
batch start
#iterations: 265
currently lose_sum: 96.06019699573517
time_elpased: 1.899
batch start
#iterations: 266
currently lose_sum: 95.78085589408875
time_elpased: 1.844
batch start
#iterations: 267
currently lose_sum: 96.04157549142838
time_elpased: 1.861
batch start
#iterations: 268
currently lose_sum: 95.94856441020966
time_elpased: 1.858
batch start
#iterations: 269
currently lose_sum: 96.28883051872253
time_elpased: 1.862
batch start
#iterations: 270
currently lose_sum: 95.43680512905121
time_elpased: 1.904
batch start
#iterations: 271
currently lose_sum: 95.69254755973816
time_elpased: 1.846
batch start
#iterations: 272
currently lose_sum: 95.78381073474884
time_elpased: 1.857
batch start
#iterations: 273
currently lose_sum: 96.03366643190384
time_elpased: 1.838
batch start
#iterations: 274
currently lose_sum: 95.97930258512497
time_elpased: 1.931
batch start
#iterations: 275
currently lose_sum: 96.08947294950485
time_elpased: 1.85
batch start
#iterations: 276
currently lose_sum: 95.90329784154892
time_elpased: 1.814
batch start
#iterations: 277
currently lose_sum: 95.83678358793259
time_elpased: 1.866
batch start
#iterations: 278
currently lose_sum: 96.14102560281754
time_elpased: 1.886
batch start
#iterations: 279
currently lose_sum: 95.90458869934082
time_elpased: 1.844
start validation test
0.638298969072
0.614064745161
0.747735693701
0.674339815288
0.63811815664
62.313
batch start
#iterations: 280
currently lose_sum: 96.15540248155594
time_elpased: 1.866
batch start
#iterations: 281
currently lose_sum: 96.02304667234421
time_elpased: 1.836
batch start
#iterations: 282
currently lose_sum: 95.78349882364273
time_elpased: 1.85
batch start
#iterations: 283
currently lose_sum: 96.19424384832382
time_elpased: 1.859
batch start
#iterations: 284
currently lose_sum: 96.14862489700317
time_elpased: 1.874
batch start
#iterations: 285
currently lose_sum: 95.95808899402618
time_elpased: 1.863
batch start
#iterations: 286
currently lose_sum: 95.4857103228569
time_elpased: 1.861
batch start
#iterations: 287
currently lose_sum: 96.24828469753265
time_elpased: 1.847
batch start
#iterations: 288
currently lose_sum: 95.9909679889679
time_elpased: 1.856
batch start
#iterations: 289
currently lose_sum: 96.05061858892441
time_elpased: 1.807
batch start
#iterations: 290
currently lose_sum: 95.7882068157196
time_elpased: 1.869
batch start
#iterations: 291
currently lose_sum: 95.91039037704468
time_elpased: 1.835
batch start
#iterations: 292
currently lose_sum: 95.58972102403641
time_elpased: 1.842
batch start
#iterations: 293
currently lose_sum: 96.0610665678978
time_elpased: 1.905
batch start
#iterations: 294
currently lose_sum: 95.84074181318283
time_elpased: 1.827
batch start
#iterations: 295
currently lose_sum: 95.99821305274963
time_elpased: 1.856
batch start
#iterations: 296
currently lose_sum: 95.68715113401413
time_elpased: 1.869
batch start
#iterations: 297
currently lose_sum: 95.98605811595917
time_elpased: 1.879
batch start
#iterations: 298
currently lose_sum: 95.93212443590164
time_elpased: 1.88
batch start
#iterations: 299
currently lose_sum: 95.80146843194962
time_elpased: 1.879
start validation test
0.629381443299
0.618391451069
0.678983120626
0.647272370487
0.62929949092
62.465
batch start
#iterations: 300
currently lose_sum: 95.93789291381836
time_elpased: 1.838
batch start
#iterations: 301
currently lose_sum: 95.66568267345428
time_elpased: 1.868
batch start
#iterations: 302
currently lose_sum: 95.96077460050583
time_elpased: 1.853
batch start
#iterations: 303
currently lose_sum: 95.82175815105438
time_elpased: 1.824
batch start
#iterations: 304
currently lose_sum: 95.78276258707047
time_elpased: 1.857
batch start
#iterations: 305
currently lose_sum: 95.93841391801834
time_elpased: 1.888
batch start
#iterations: 306
currently lose_sum: 95.51768273115158
time_elpased: 1.833
batch start
#iterations: 307
currently lose_sum: 96.0264909863472
time_elpased: 1.823
batch start
#iterations: 308
currently lose_sum: 95.78356260061264
time_elpased: 1.835
batch start
#iterations: 309
currently lose_sum: 95.73763328790665
time_elpased: 1.847
batch start
#iterations: 310
currently lose_sum: 95.89590972661972
time_elpased: 1.849
batch start
#iterations: 311
currently lose_sum: 95.62166076898575
time_elpased: 1.814
batch start
#iterations: 312
currently lose_sum: 95.99762892723083
time_elpased: 1.85
batch start
#iterations: 313
currently lose_sum: 95.96137672662735
time_elpased: 1.869
batch start
#iterations: 314
currently lose_sum: 95.76141345500946
time_elpased: 1.834
batch start
#iterations: 315
currently lose_sum: 95.6462173461914
time_elpased: 1.913
batch start
#iterations: 316
currently lose_sum: 95.90550315380096
time_elpased: 1.819
batch start
#iterations: 317
currently lose_sum: 95.7002911567688
time_elpased: 1.864
batch start
#iterations: 318
currently lose_sum: 95.70975971221924
time_elpased: 1.858
batch start
#iterations: 319
currently lose_sum: 96.28600436449051
time_elpased: 1.87
start validation test
0.636340206186
0.622277364213
0.69689172499
0.657474389474
0.636240162371
62.292
batch start
#iterations: 320
currently lose_sum: 95.88473564386368
time_elpased: 1.848
batch start
#iterations: 321
currently lose_sum: 96.27179497480392
time_elpased: 1.858
batch start
#iterations: 322
currently lose_sum: 95.81942546367645
time_elpased: 1.8
batch start
#iterations: 323
currently lose_sum: 95.7900829911232
time_elpased: 1.866
batch start
#iterations: 324
currently lose_sum: 96.153375685215
time_elpased: 1.822
batch start
#iterations: 325
currently lose_sum: 95.33696377277374
time_elpased: 1.823
batch start
#iterations: 326
currently lose_sum: 96.11301350593567
time_elpased: 1.841
batch start
#iterations: 327
currently lose_sum: 95.9585223197937
time_elpased: 1.852
batch start
#iterations: 328
currently lose_sum: 95.61529225111008
time_elpased: 1.89
batch start
#iterations: 329
currently lose_sum: 95.54526525735855
time_elpased: 1.85
batch start
#iterations: 330
currently lose_sum: 95.86443966627121
time_elpased: 1.84
batch start
#iterations: 331
currently lose_sum: 95.88490110635757
time_elpased: 1.877
batch start
#iterations: 332
currently lose_sum: 95.74080908298492
time_elpased: 1.814
batch start
#iterations: 333
currently lose_sum: 95.67441010475159
time_elpased: 1.918
batch start
#iterations: 334
currently lose_sum: 95.73273307085037
time_elpased: 1.86
batch start
#iterations: 335
currently lose_sum: 95.77583771944046
time_elpased: 1.856
batch start
#iterations: 336
currently lose_sum: 96.07119297981262
time_elpased: 1.908
batch start
#iterations: 337
currently lose_sum: 95.88749033212662
time_elpased: 1.842
batch start
#iterations: 338
currently lose_sum: 95.56435889005661
time_elpased: 1.877
batch start
#iterations: 339
currently lose_sum: 95.74127691984177
time_elpased: 1.873
start validation test
0.63675257732
0.614677322334
0.736208316179
0.669976115768
0.63658825557
62.220
batch start
#iterations: 340
currently lose_sum: 95.93216323852539
time_elpased: 1.846
batch start
#iterations: 341
currently lose_sum: 95.8104173541069
time_elpased: 1.846
batch start
#iterations: 342
currently lose_sum: 95.8206045627594
time_elpased: 1.843
batch start
#iterations: 343
currently lose_sum: 96.07548499107361
time_elpased: 1.9
batch start
#iterations: 344
currently lose_sum: 96.06548470258713
time_elpased: 1.887
batch start
#iterations: 345
currently lose_sum: 95.80039656162262
time_elpased: 1.855
batch start
#iterations: 346
currently lose_sum: 96.00323587656021
time_elpased: 1.881
batch start
#iterations: 347
currently lose_sum: 95.70155727863312
time_elpased: 1.935
batch start
#iterations: 348
currently lose_sum: 96.01144301891327
time_elpased: 1.867
batch start
#iterations: 349
currently lose_sum: 95.691231071949
time_elpased: 1.899
batch start
#iterations: 350
currently lose_sum: 96.14682763814926
time_elpased: 1.85
batch start
#iterations: 351
currently lose_sum: 95.97919696569443
time_elpased: 1.87
batch start
#iterations: 352
currently lose_sum: 95.78199011087418
time_elpased: 1.86
batch start
#iterations: 353
currently lose_sum: 96.08950740098953
time_elpased: 1.864
batch start
#iterations: 354
currently lose_sum: 95.93591779470444
time_elpased: 1.792
batch start
#iterations: 355
currently lose_sum: 95.71907824277878
time_elpased: 1.847
batch start
#iterations: 356
currently lose_sum: 95.64710021018982
time_elpased: 1.854
batch start
#iterations: 357
currently lose_sum: 95.90872746706009
time_elpased: 1.833
batch start
#iterations: 358
currently lose_sum: 95.9580585360527
time_elpased: 1.853
batch start
#iterations: 359
currently lose_sum: 95.44922888278961
time_elpased: 1.86
start validation test
0.636030927835
0.608642278419
0.765438452038
0.678094369729
0.635817119451
62.277
batch start
#iterations: 360
currently lose_sum: 96.00724732875824
time_elpased: 1.844
batch start
#iterations: 361
currently lose_sum: 95.8815131187439
time_elpased: 1.873
batch start
#iterations: 362
currently lose_sum: 95.87178760766983
time_elpased: 1.865
batch start
#iterations: 363
currently lose_sum: 95.86908775568008
time_elpased: 1.829
batch start
#iterations: 364
currently lose_sum: 95.9926609992981
time_elpased: 1.846
batch start
#iterations: 365
currently lose_sum: 96.20028364658356
time_elpased: 1.861
batch start
#iterations: 366
currently lose_sum: 96.13177388906479
time_elpased: 1.837
batch start
#iterations: 367
currently lose_sum: 95.48049384355545
time_elpased: 1.866
batch start
#iterations: 368
currently lose_sum: 95.6644178032875
time_elpased: 1.839
batch start
#iterations: 369
currently lose_sum: 95.86829876899719
time_elpased: 1.837
batch start
#iterations: 370
currently lose_sum: 95.83673447370529
time_elpased: 1.856
batch start
#iterations: 371
currently lose_sum: 95.83106869459152
time_elpased: 1.905
batch start
#iterations: 372
currently lose_sum: 96.00198900699615
time_elpased: 1.886
batch start
#iterations: 373
currently lose_sum: 95.75892466306686
time_elpased: 1.83
batch start
#iterations: 374
currently lose_sum: 95.62491476535797
time_elpased: 1.821
batch start
#iterations: 375
currently lose_sum: 96.07114523649216
time_elpased: 1.899
batch start
#iterations: 376
currently lose_sum: 95.6308114528656
time_elpased: 1.853
batch start
#iterations: 377
currently lose_sum: 95.82263916730881
time_elpased: 1.874
batch start
#iterations: 378
currently lose_sum: 95.87617123126984
time_elpased: 1.826
batch start
#iterations: 379
currently lose_sum: 96.10360836982727
time_elpased: 1.894
start validation test
0.639432989691
0.612895195419
0.760189378345
0.67864198098
0.639233474798
62.218
batch start
#iterations: 380
currently lose_sum: 95.73061221837997
time_elpased: 1.862
batch start
#iterations: 381
currently lose_sum: 95.74924713373184
time_elpased: 1.849
batch start
#iterations: 382
currently lose_sum: 96.02854007482529
time_elpased: 1.877
batch start
#iterations: 383
currently lose_sum: 95.99513149261475
time_elpased: 1.887
batch start
#iterations: 384
currently lose_sum: 95.81056398153305
time_elpased: 1.824
batch start
#iterations: 385
currently lose_sum: 95.43292212486267
time_elpased: 1.87
batch start
#iterations: 386
currently lose_sum: 95.62986606359482
time_elpased: 1.858
batch start
#iterations: 387
currently lose_sum: 95.64460653066635
time_elpased: 1.874
batch start
#iterations: 388
currently lose_sum: 95.78030663728714
time_elpased: 1.87
batch start
#iterations: 389
currently lose_sum: 95.69745641946793
time_elpased: 1.839
batch start
#iterations: 390
currently lose_sum: 95.62540155649185
time_elpased: 1.817
batch start
#iterations: 391
currently lose_sum: 95.41103380918503
time_elpased: 1.893
batch start
#iterations: 392
currently lose_sum: 95.64271986484528
time_elpased: 1.844
batch start
#iterations: 393
currently lose_sum: 95.71080446243286
time_elpased: 1.852
batch start
#iterations: 394
currently lose_sum: 95.78338319063187
time_elpased: 1.854
batch start
#iterations: 395
currently lose_sum: 95.64986324310303
time_elpased: 1.885
batch start
#iterations: 396
currently lose_sum: 95.84111112356186
time_elpased: 1.829
batch start
#iterations: 397
currently lose_sum: 95.81110769510269
time_elpased: 1.858
batch start
#iterations: 398
currently lose_sum: 95.23024195432663
time_elpased: 1.889
batch start
#iterations: 399
currently lose_sum: 95.74092864990234
time_elpased: 1.865
start validation test
0.636701030928
0.610284391534
0.759777686291
0.676875114616
0.636497682468
62.318
acc: 0.638
pre: 0.615
rec: 0.739
F1: 0.672
auc: 0.685
