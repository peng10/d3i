start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.87801760435104
time_elpased: 1.446
batch start
#iterations: 1
currently lose_sum: 100.28000771999359
time_elpased: 1.468
batch start
#iterations: 2
currently lose_sum: 100.19930642843246
time_elpased: 1.464
batch start
#iterations: 3
currently lose_sum: 99.90123558044434
time_elpased: 1.463
batch start
#iterations: 4
currently lose_sum: 99.70896887779236
time_elpased: 1.458
batch start
#iterations: 5
currently lose_sum: 99.63095915317535
time_elpased: 1.455
batch start
#iterations: 6
currently lose_sum: 99.60081112384796
time_elpased: 1.487
batch start
#iterations: 7
currently lose_sum: 99.47469425201416
time_elpased: 1.462
batch start
#iterations: 8
currently lose_sum: 99.27014255523682
time_elpased: 1.433
batch start
#iterations: 9
currently lose_sum: 99.03417205810547
time_elpased: 1.474
batch start
#iterations: 10
currently lose_sum: 99.00202143192291
time_elpased: 1.474
batch start
#iterations: 11
currently lose_sum: 98.97037303447723
time_elpased: 1.446
batch start
#iterations: 12
currently lose_sum: 98.9992396235466
time_elpased: 1.434
batch start
#iterations: 13
currently lose_sum: 98.90484946966171
time_elpased: 1.426
batch start
#iterations: 14
currently lose_sum: 98.68854576349258
time_elpased: 1.451
batch start
#iterations: 15
currently lose_sum: 98.71577394008636
time_elpased: 1.461
batch start
#iterations: 16
currently lose_sum: 98.26335614919662
time_elpased: 1.45
batch start
#iterations: 17
currently lose_sum: 98.57631570100784
time_elpased: 1.483
batch start
#iterations: 18
currently lose_sum: 98.37223130464554
time_elpased: 1.52
batch start
#iterations: 19
currently lose_sum: 98.12100505828857
time_elpased: 1.457
start validation test
0.625927835052
0.627832051549
0.621693938458
0.624747918713
0.62593526831
63.739
batch start
#iterations: 20
currently lose_sum: 98.21847975254059
time_elpased: 1.43
batch start
#iterations: 21
currently lose_sum: 97.98261243104935
time_elpased: 1.535
batch start
#iterations: 22
currently lose_sum: 98.25871866941452
time_elpased: 1.441
batch start
#iterations: 23
currently lose_sum: 97.85461378097534
time_elpased: 1.464
batch start
#iterations: 24
currently lose_sum: 97.87523740530014
time_elpased: 1.424
batch start
#iterations: 25
currently lose_sum: 97.84774804115295
time_elpased: 1.467
batch start
#iterations: 26
currently lose_sum: 97.96322864294052
time_elpased: 1.445
batch start
#iterations: 27
currently lose_sum: 97.72535920143127
time_elpased: 1.489
batch start
#iterations: 28
currently lose_sum: 97.79992324113846
time_elpased: 1.516
batch start
#iterations: 29
currently lose_sum: 98.0255897641182
time_elpased: 1.465
batch start
#iterations: 30
currently lose_sum: 97.88161045312881
time_elpased: 1.495
batch start
#iterations: 31
currently lose_sum: 97.56349694728851
time_elpased: 1.454
batch start
#iterations: 32
currently lose_sum: 97.53665274381638
time_elpased: 1.493
batch start
#iterations: 33
currently lose_sum: 97.46853053569794
time_elpased: 1.528
batch start
#iterations: 34
currently lose_sum: 97.50153940916061
time_elpased: 1.476
batch start
#iterations: 35
currently lose_sum: 97.5228066444397
time_elpased: 1.432
batch start
#iterations: 36
currently lose_sum: 97.68678396940231
time_elpased: 1.443
batch start
#iterations: 37
currently lose_sum: 97.25538086891174
time_elpased: 1.469
batch start
#iterations: 38
currently lose_sum: 97.35522645711899
time_elpased: 1.453
batch start
#iterations: 39
currently lose_sum: 97.63805556297302
time_elpased: 1.492
start validation test
0.624896907216
0.620816003169
0.645157970567
0.632752964926
0.624861335795
62.986
batch start
#iterations: 40
currently lose_sum: 97.45922005176544
time_elpased: 1.515
batch start
#iterations: 41
currently lose_sum: 97.5138755440712
time_elpased: 1.444
batch start
#iterations: 42
currently lose_sum: 97.3422224521637
time_elpased: 1.479
batch start
#iterations: 43
currently lose_sum: 97.42401337623596
time_elpased: 1.452
batch start
#iterations: 44
currently lose_sum: 97.43802279233932
time_elpased: 1.503
batch start
#iterations: 45
currently lose_sum: 97.50566440820694
time_elpased: 1.508
batch start
#iterations: 46
currently lose_sum: 97.08595246076584
time_elpased: 1.416
batch start
#iterations: 47
currently lose_sum: 97.20397144556046
time_elpased: 1.468
batch start
#iterations: 48
currently lose_sum: 97.37553495168686
time_elpased: 1.443
batch start
#iterations: 49
currently lose_sum: 97.44360262155533
time_elpased: 1.508
batch start
#iterations: 50
currently lose_sum: 97.2473486661911
time_elpased: 1.472
batch start
#iterations: 51
currently lose_sum: 97.06699347496033
time_elpased: 1.466
batch start
#iterations: 52
currently lose_sum: 97.3280497789383
time_elpased: 1.423
batch start
#iterations: 53
currently lose_sum: 97.05571115016937
time_elpased: 1.479
batch start
#iterations: 54
currently lose_sum: 97.28522408008575
time_elpased: 1.521
batch start
#iterations: 55
currently lose_sum: 96.79161924123764
time_elpased: 1.454
batch start
#iterations: 56
currently lose_sum: 97.38589751720428
time_elpased: 1.513
batch start
#iterations: 57
currently lose_sum: 97.20432329177856
time_elpased: 1.49
batch start
#iterations: 58
currently lose_sum: 97.0658408999443
time_elpased: 1.445
batch start
#iterations: 59
currently lose_sum: 97.37105375528336
time_elpased: 1.457
start validation test
0.632989690722
0.629500349058
0.649583204693
0.639384116694
0.632960558248
62.676
batch start
#iterations: 60
currently lose_sum: 97.36502259969711
time_elpased: 1.481
batch start
#iterations: 61
currently lose_sum: 97.23146671056747
time_elpased: 1.451
batch start
#iterations: 62
currently lose_sum: 96.98142570257187
time_elpased: 1.523
batch start
#iterations: 63
currently lose_sum: 97.25743007659912
time_elpased: 1.432
batch start
#iterations: 64
currently lose_sum: 97.13361740112305
time_elpased: 1.446
batch start
#iterations: 65
currently lose_sum: 97.01181095838547
time_elpased: 1.459
batch start
#iterations: 66
currently lose_sum: 96.70387697219849
time_elpased: 1.486
batch start
#iterations: 67
currently lose_sum: 96.82600408792496
time_elpased: 1.458
batch start
#iterations: 68
currently lose_sum: 97.0145959854126
time_elpased: 1.448
batch start
#iterations: 69
currently lose_sum: 96.70829743146896
time_elpased: 1.479
batch start
#iterations: 70
currently lose_sum: 96.78812634944916
time_elpased: 1.439
batch start
#iterations: 71
currently lose_sum: 97.0177521109581
time_elpased: 1.475
batch start
#iterations: 72
currently lose_sum: 97.15946590900421
time_elpased: 1.489
batch start
#iterations: 73
currently lose_sum: 96.91189455986023
time_elpased: 1.537
batch start
#iterations: 74
currently lose_sum: 96.89396286010742
time_elpased: 1.455
batch start
#iterations: 75
currently lose_sum: 96.75890582799911
time_elpased: 1.472
batch start
#iterations: 76
currently lose_sum: 96.88067829608917
time_elpased: 1.478
batch start
#iterations: 77
currently lose_sum: 96.95099920034409
time_elpased: 1.452
batch start
#iterations: 78
currently lose_sum: 96.71472334861755
time_elpased: 1.477
batch start
#iterations: 79
currently lose_sum: 97.06557482481003
time_elpased: 1.496
start validation test
0.638092783505
0.618515913487
0.723988885458
0.667109193495
0.63794197965
62.480
batch start
#iterations: 80
currently lose_sum: 97.07016760110855
time_elpased: 1.485
batch start
#iterations: 81
currently lose_sum: 96.94054937362671
time_elpased: 1.449
batch start
#iterations: 82
currently lose_sum: 96.94514405727386
time_elpased: 1.439
batch start
#iterations: 83
currently lose_sum: 96.82934933900833
time_elpased: 1.452
batch start
#iterations: 84
currently lose_sum: 96.79759734869003
time_elpased: 1.496
batch start
#iterations: 85
currently lose_sum: 96.96134907007217
time_elpased: 1.458
batch start
#iterations: 86
currently lose_sum: 96.6553567647934
time_elpased: 1.474
batch start
#iterations: 87
currently lose_sum: 96.84806090593338
time_elpased: 1.474
batch start
#iterations: 88
currently lose_sum: 97.12840116024017
time_elpased: 1.44
batch start
#iterations: 89
currently lose_sum: 96.73414045572281
time_elpased: 1.466
batch start
#iterations: 90
currently lose_sum: 96.90807795524597
time_elpased: 1.472
batch start
#iterations: 91
currently lose_sum: 96.79359579086304
time_elpased: 1.457
batch start
#iterations: 92
currently lose_sum: 96.96494990587234
time_elpased: 1.489
batch start
#iterations: 93
currently lose_sum: 96.73115110397339
time_elpased: 1.488
batch start
#iterations: 94
currently lose_sum: 96.86633586883545
time_elpased: 1.436
batch start
#iterations: 95
currently lose_sum: 97.00020319223404
time_elpased: 1.433
batch start
#iterations: 96
currently lose_sum: 96.71294629573822
time_elpased: 1.437
batch start
#iterations: 97
currently lose_sum: 96.72103238105774
time_elpased: 1.472
batch start
#iterations: 98
currently lose_sum: 96.93331813812256
time_elpased: 1.428
batch start
#iterations: 99
currently lose_sum: 96.93522733449936
time_elpased: 1.475
start validation test
0.628402061856
0.617130580983
0.679942369044
0.647015619645
0.628311574897
62.703
batch start
#iterations: 100
currently lose_sum: 96.63708966970444
time_elpased: 1.491
batch start
#iterations: 101
currently lose_sum: 96.65383207798004
time_elpased: 1.448
batch start
#iterations: 102
currently lose_sum: 96.97430592775345
time_elpased: 1.476
batch start
#iterations: 103
currently lose_sum: 96.46122652292252
time_elpased: 1.467
batch start
#iterations: 104
currently lose_sum: 96.98140013217926
time_elpased: 1.441
batch start
#iterations: 105
currently lose_sum: 96.45020657777786
time_elpased: 1.45
batch start
#iterations: 106
currently lose_sum: 96.62155103683472
time_elpased: 1.465
batch start
#iterations: 107
currently lose_sum: 96.63881212472916
time_elpased: 1.442
batch start
#iterations: 108
currently lose_sum: 96.72911393642426
time_elpased: 1.458
batch start
#iterations: 109
currently lose_sum: 96.85657507181168
time_elpased: 1.449
batch start
#iterations: 110
currently lose_sum: 97.21287512779236
time_elpased: 1.487
batch start
#iterations: 111
currently lose_sum: 96.47846043109894
time_elpased: 1.46
batch start
#iterations: 112
currently lose_sum: 97.07846254110336
time_elpased: 1.426
batch start
#iterations: 113
currently lose_sum: 96.53796577453613
time_elpased: 1.464
batch start
#iterations: 114
currently lose_sum: 96.72195208072662
time_elpased: 1.453
batch start
#iterations: 115
currently lose_sum: 96.83444088697433
time_elpased: 1.462
batch start
#iterations: 116
currently lose_sum: 96.9633070230484
time_elpased: 1.441
batch start
#iterations: 117
currently lose_sum: 96.15562868118286
time_elpased: 1.488
batch start
#iterations: 118
currently lose_sum: 96.36586314439774
time_elpased: 1.469
batch start
#iterations: 119
currently lose_sum: 96.27120357751846
time_elpased: 1.449
start validation test
0.634690721649
0.615757042254
0.719872388597
0.663756701618
0.634541172095
62.625
batch start
#iterations: 120
currently lose_sum: 96.78310096263885
time_elpased: 1.468
batch start
#iterations: 121
currently lose_sum: 96.48790091276169
time_elpased: 1.477
batch start
#iterations: 122
currently lose_sum: 96.93589645624161
time_elpased: 1.465
batch start
#iterations: 123
currently lose_sum: 96.66520845890045
time_elpased: 1.471
batch start
#iterations: 124
currently lose_sum: 96.78066563606262
time_elpased: 1.436
batch start
#iterations: 125
currently lose_sum: 96.9903034567833
time_elpased: 1.468
batch start
#iterations: 126
currently lose_sum: 96.49861377477646
time_elpased: 1.466
batch start
#iterations: 127
currently lose_sum: 97.07244688272476
time_elpased: 1.455
batch start
#iterations: 128
currently lose_sum: 96.60164308547974
time_elpased: 1.436
batch start
#iterations: 129
currently lose_sum: 96.77224439382553
time_elpased: 1.479
batch start
#iterations: 130
currently lose_sum: 96.75336754322052
time_elpased: 1.462
batch start
#iterations: 131
currently lose_sum: 96.81127679347992
time_elpased: 1.474
batch start
#iterations: 132
currently lose_sum: 96.56072479486465
time_elpased: 1.45
batch start
#iterations: 133
currently lose_sum: 96.55403631925583
time_elpased: 1.449
batch start
#iterations: 134
currently lose_sum: 96.4340096116066
time_elpased: 1.462
batch start
#iterations: 135
currently lose_sum: 96.73721385002136
time_elpased: 1.512
batch start
#iterations: 136
currently lose_sum: 96.54412317276001
time_elpased: 1.505
batch start
#iterations: 137
currently lose_sum: 96.67647331953049
time_elpased: 1.493
batch start
#iterations: 138
currently lose_sum: 96.42226296663284
time_elpased: 1.45
batch start
#iterations: 139
currently lose_sum: 96.30318170785904
time_elpased: 1.461
start validation test
0.640360824742
0.608851104402
0.788617886179
0.68717212931
0.640100536604
62.150
batch start
#iterations: 140
currently lose_sum: 96.28061479330063
time_elpased: 1.51
batch start
#iterations: 141
currently lose_sum: 96.75149917602539
time_elpased: 1.491
batch start
#iterations: 142
currently lose_sum: 96.69101405143738
time_elpased: 1.523
batch start
#iterations: 143
currently lose_sum: 96.51984453201294
time_elpased: 1.448
batch start
#iterations: 144
currently lose_sum: 96.55967545509338
time_elpased: 1.48
batch start
#iterations: 145
currently lose_sum: 96.88775950670242
time_elpased: 1.472
batch start
#iterations: 146
currently lose_sum: 96.50074023008347
time_elpased: 1.499
batch start
#iterations: 147
currently lose_sum: 96.5518142580986
time_elpased: 1.445
batch start
#iterations: 148
currently lose_sum: 96.44556367397308
time_elpased: 1.502
batch start
#iterations: 149
currently lose_sum: 96.30735170841217
time_elpased: 1.468
batch start
#iterations: 150
currently lose_sum: 96.49589323997498
time_elpased: 1.475
batch start
#iterations: 151
currently lose_sum: 96.52492892742157
time_elpased: 1.495
batch start
#iterations: 152
currently lose_sum: 96.46482646465302
time_elpased: 1.451
batch start
#iterations: 153
currently lose_sum: 96.5283710360527
time_elpased: 1.486
batch start
#iterations: 154
currently lose_sum: 96.52974164485931
time_elpased: 1.478
batch start
#iterations: 155
currently lose_sum: 96.48296922445297
time_elpased: 1.501
batch start
#iterations: 156
currently lose_sum: 96.5649443268776
time_elpased: 1.472
batch start
#iterations: 157
currently lose_sum: 96.33808249235153
time_elpased: 1.467
batch start
#iterations: 158
currently lose_sum: 96.6496233344078
time_elpased: 1.443
batch start
#iterations: 159
currently lose_sum: 96.37775111198425
time_elpased: 1.457
start validation test
0.637628865979
0.618694230939
0.72069568797
0.665810990683
0.637483029361
62.399
batch start
#iterations: 160
currently lose_sum: 96.3382276892662
time_elpased: 1.46
batch start
#iterations: 161
currently lose_sum: 96.27787578105927
time_elpased: 1.475
batch start
#iterations: 162
currently lose_sum: 96.7364165186882
time_elpased: 1.529
batch start
#iterations: 163
currently lose_sum: 96.73344421386719
time_elpased: 1.521
batch start
#iterations: 164
currently lose_sum: 96.47707092761993
time_elpased: 1.468
batch start
#iterations: 165
currently lose_sum: 96.24624663591385
time_elpased: 1.455
batch start
#iterations: 166
currently lose_sum: 96.48593419790268
time_elpased: 1.451
batch start
#iterations: 167
currently lose_sum: 96.29903930425644
time_elpased: 1.47
batch start
#iterations: 168
currently lose_sum: 96.293162047863
time_elpased: 1.468
batch start
#iterations: 169
currently lose_sum: 96.7101514339447
time_elpased: 1.487
batch start
#iterations: 170
currently lose_sum: 96.53226965665817
time_elpased: 1.459
batch start
#iterations: 171
currently lose_sum: 96.46286273002625
time_elpased: 1.46
batch start
#iterations: 172
currently lose_sum: 96.6518606543541
time_elpased: 1.468
batch start
#iterations: 173
currently lose_sum: 96.73114502429962
time_elpased: 1.565
batch start
#iterations: 174
currently lose_sum: 96.32368612289429
time_elpased: 1.476
batch start
#iterations: 175
currently lose_sum: 96.0269187092781
time_elpased: 1.446
batch start
#iterations: 176
currently lose_sum: 96.17167419195175
time_elpased: 1.463
batch start
#iterations: 177
currently lose_sum: 96.4362325668335
time_elpased: 1.476
batch start
#iterations: 178
currently lose_sum: 96.67151945829391
time_elpased: 1.428
batch start
#iterations: 179
currently lose_sum: 95.98326563835144
time_elpased: 1.45
start validation test
0.637319587629
0.616880285988
0.72810538232
0.667893892193
0.637160199164
62.238
batch start
#iterations: 180
currently lose_sum: 96.41755777597427
time_elpased: 1.458
batch start
#iterations: 181
currently lose_sum: 96.39289236068726
time_elpased: 1.449
batch start
#iterations: 182
currently lose_sum: 96.518490254879
time_elpased: 1.46
batch start
#iterations: 183
currently lose_sum: 96.14443850517273
time_elpased: 1.475
batch start
#iterations: 184
currently lose_sum: 96.22654581069946
time_elpased: 1.429
batch start
#iterations: 185
currently lose_sum: 96.64640164375305
time_elpased: 1.462
batch start
#iterations: 186
currently lose_sum: 96.65323346853256
time_elpased: 1.46
batch start
#iterations: 187
currently lose_sum: 96.30323100090027
time_elpased: 1.448
batch start
#iterations: 188
currently lose_sum: 96.41245287656784
time_elpased: 1.443
batch start
#iterations: 189
currently lose_sum: 96.34606373310089
time_elpased: 1.42
batch start
#iterations: 190
currently lose_sum: 96.68864732980728
time_elpased: 1.453
batch start
#iterations: 191
currently lose_sum: 96.5293590426445
time_elpased: 1.511
batch start
#iterations: 192
currently lose_sum: 96.26266068220139
time_elpased: 1.439
batch start
#iterations: 193
currently lose_sum: 96.63223832845688
time_elpased: 1.461
batch start
#iterations: 194
currently lose_sum: 96.35928255319595
time_elpased: 1.478
batch start
#iterations: 195
currently lose_sum: 96.29497706890106
time_elpased: 1.448
batch start
#iterations: 196
currently lose_sum: 96.39510667324066
time_elpased: 1.461
batch start
#iterations: 197
currently lose_sum: 96.45231300592422
time_elpased: 1.437
batch start
#iterations: 198
currently lose_sum: 96.41334903240204
time_elpased: 1.486
batch start
#iterations: 199
currently lose_sum: 96.25006854534149
time_elpased: 1.5
start validation test
0.634020618557
0.618706341286
0.70186271483
0.657666345227
0.633901511293
62.296
batch start
#iterations: 200
currently lose_sum: 96.37647616863251
time_elpased: 1.467
batch start
#iterations: 201
currently lose_sum: 96.5616888999939
time_elpased: 1.438
batch start
#iterations: 202
currently lose_sum: 96.33928519487381
time_elpased: 1.513
batch start
#iterations: 203
currently lose_sum: 96.00937002897263
time_elpased: 1.458
batch start
#iterations: 204
currently lose_sum: 96.18365043401718
time_elpased: 1.51
batch start
#iterations: 205
currently lose_sum: 95.82832753658295
time_elpased: 1.484
batch start
#iterations: 206
currently lose_sum: 96.19505780935287
time_elpased: 1.426
batch start
#iterations: 207
currently lose_sum: 96.67133468389511
time_elpased: 1.453
batch start
#iterations: 208
currently lose_sum: 96.3669096827507
time_elpased: 1.487
batch start
#iterations: 209
currently lose_sum: 96.17369204759598
time_elpased: 1.473
batch start
#iterations: 210
currently lose_sum: 96.19622892141342
time_elpased: 1.461
batch start
#iterations: 211
currently lose_sum: 96.42282009124756
time_elpased: 1.459
batch start
#iterations: 212
currently lose_sum: 96.54335463047028
time_elpased: 1.439
batch start
#iterations: 213
currently lose_sum: 96.44376820325851
time_elpased: 1.451
batch start
#iterations: 214
currently lose_sum: 96.60675436258316
time_elpased: 1.46
batch start
#iterations: 215
currently lose_sum: 96.22024917602539
time_elpased: 1.45
batch start
#iterations: 216
currently lose_sum: 96.23937654495239
time_elpased: 1.481
batch start
#iterations: 217
currently lose_sum: 96.28757810592651
time_elpased: 1.419
batch start
#iterations: 218
currently lose_sum: 96.1968058347702
time_elpased: 1.493
batch start
#iterations: 219
currently lose_sum: 96.13836371898651
time_elpased: 1.517
start validation test
0.643402061856
0.613329014495
0.779458680663
0.686485996556
0.643163193476
62.109
batch start
#iterations: 220
currently lose_sum: 96.2911166548729
time_elpased: 1.479
batch start
#iterations: 221
currently lose_sum: 96.18604058027267
time_elpased: 1.45
batch start
#iterations: 222
currently lose_sum: 96.55215233564377
time_elpased: 1.504
batch start
#iterations: 223
currently lose_sum: 96.21107918024063
time_elpased: 1.471
batch start
#iterations: 224
currently lose_sum: 96.01137816905975
time_elpased: 1.493
batch start
#iterations: 225
currently lose_sum: 96.22201490402222
time_elpased: 1.465
batch start
#iterations: 226
currently lose_sum: 96.21995556354523
time_elpased: 1.434
batch start
#iterations: 227
currently lose_sum: 96.13679546117783
time_elpased: 1.513
batch start
#iterations: 228
currently lose_sum: 96.38947814702988
time_elpased: 1.438
batch start
#iterations: 229
currently lose_sum: 96.46185618638992
time_elpased: 1.468
batch start
#iterations: 230
currently lose_sum: 96.13264352083206
time_elpased: 1.447
batch start
#iterations: 231
currently lose_sum: 96.26126390695572
time_elpased: 1.462
batch start
#iterations: 232
currently lose_sum: 95.98796993494034
time_elpased: 1.456
batch start
#iterations: 233
currently lose_sum: 96.42026716470718
time_elpased: 1.45
batch start
#iterations: 234
currently lose_sum: 96.23734712600708
time_elpased: 1.506
batch start
#iterations: 235
currently lose_sum: 96.3013026714325
time_elpased: 1.51
batch start
#iterations: 236
currently lose_sum: 96.04768925905228
time_elpased: 1.437
batch start
#iterations: 237
currently lose_sum: 96.36624771356583
time_elpased: 1.451
batch start
#iterations: 238
currently lose_sum: 96.16158854961395
time_elpased: 1.476
batch start
#iterations: 239
currently lose_sum: 96.6436362862587
time_elpased: 1.44
start validation test
0.637371134021
0.612330373597
0.752289801379
0.675132763796
0.637169376575
62.191
batch start
#iterations: 240
currently lose_sum: 95.80485987663269
time_elpased: 1.461
batch start
#iterations: 241
currently lose_sum: 96.18513238430023
time_elpased: 1.453
batch start
#iterations: 242
currently lose_sum: 96.39228248596191
time_elpased: 1.435
batch start
#iterations: 243
currently lose_sum: 95.93969076871872
time_elpased: 1.433
batch start
#iterations: 244
currently lose_sum: 96.57693266868591
time_elpased: 1.457
batch start
#iterations: 245
currently lose_sum: 96.28530180454254
time_elpased: 1.459
batch start
#iterations: 246
currently lose_sum: 96.33491259813309
time_elpased: 1.521
batch start
#iterations: 247
currently lose_sum: 96.23766088485718
time_elpased: 1.504
batch start
#iterations: 248
currently lose_sum: 95.88648962974548
time_elpased: 1.446
batch start
#iterations: 249
currently lose_sum: 96.09407985210419
time_elpased: 1.484
batch start
#iterations: 250
currently lose_sum: 95.89799386262894
time_elpased: 1.45
batch start
#iterations: 251
currently lose_sum: 96.44589579105377
time_elpased: 1.461
batch start
#iterations: 252
currently lose_sum: 96.36153143644333
time_elpased: 1.48
batch start
#iterations: 253
currently lose_sum: 96.10222047567368
time_elpased: 1.469
batch start
#iterations: 254
currently lose_sum: 95.92161720991135
time_elpased: 1.441
batch start
#iterations: 255
currently lose_sum: 96.32613319158554
time_elpased: 1.496
batch start
#iterations: 256
currently lose_sum: 96.29448223114014
time_elpased: 1.475
batch start
#iterations: 257
currently lose_sum: 95.92281341552734
time_elpased: 1.467
batch start
#iterations: 258
currently lose_sum: 96.14927852153778
time_elpased: 1.459
batch start
#iterations: 259
currently lose_sum: 96.29806512594223
time_elpased: 1.515
start validation test
0.633350515464
0.620756816917
0.688792837295
0.653007463779
0.633253177917
62.315
batch start
#iterations: 260
currently lose_sum: 96.07236379384995
time_elpased: 1.453
batch start
#iterations: 261
currently lose_sum: 96.31784552335739
time_elpased: 1.478
batch start
#iterations: 262
currently lose_sum: 95.86222541332245
time_elpased: 1.459
batch start
#iterations: 263
currently lose_sum: 96.23390603065491
time_elpased: 1.44
batch start
#iterations: 264
currently lose_sum: 96.27681940793991
time_elpased: 1.469
batch start
#iterations: 265
currently lose_sum: 96.1758319735527
time_elpased: 1.476
batch start
#iterations: 266
currently lose_sum: 96.19455283880234
time_elpased: 1.44
batch start
#iterations: 267
currently lose_sum: 95.99269247055054
time_elpased: 1.472
batch start
#iterations: 268
currently lose_sum: 96.1004730463028
time_elpased: 1.496
batch start
#iterations: 269
currently lose_sum: 96.3500143289566
time_elpased: 1.475
batch start
#iterations: 270
currently lose_sum: 95.90860378742218
time_elpased: 1.483
batch start
#iterations: 271
currently lose_sum: 96.37052619457245
time_elpased: 1.444
batch start
#iterations: 272
currently lose_sum: 96.16029578447342
time_elpased: 1.478
batch start
#iterations: 273
currently lose_sum: 95.88111817836761
time_elpased: 1.478
batch start
#iterations: 274
currently lose_sum: 95.8238570690155
time_elpased: 1.444
batch start
#iterations: 275
currently lose_sum: 96.3528578877449
time_elpased: 1.444
batch start
#iterations: 276
currently lose_sum: 95.71128308773041
time_elpased: 1.491
batch start
#iterations: 277
currently lose_sum: 95.84821611642838
time_elpased: 1.467
batch start
#iterations: 278
currently lose_sum: 96.15434283018112
time_elpased: 1.476
batch start
#iterations: 279
currently lose_sum: 96.37602800130844
time_elpased: 1.454
start validation test
0.637474226804
0.611647254576
0.756612123083
0.676450292129
0.637265061851
62.197
batch start
#iterations: 280
currently lose_sum: 96.22973382472992
time_elpased: 1.465
batch start
#iterations: 281
currently lose_sum: 96.12061023712158
time_elpased: 1.437
batch start
#iterations: 282
currently lose_sum: 95.97042375802994
time_elpased: 1.478
batch start
#iterations: 283
currently lose_sum: 96.03801226615906
time_elpased: 1.472
batch start
#iterations: 284
currently lose_sum: 96.05518651008606
time_elpased: 1.447
batch start
#iterations: 285
currently lose_sum: 96.18860077857971
time_elpased: 1.453
batch start
#iterations: 286
currently lose_sum: 96.46420347690582
time_elpased: 1.506
batch start
#iterations: 287
currently lose_sum: 95.86666738986969
time_elpased: 1.449
batch start
#iterations: 288
currently lose_sum: 95.98308199644089
time_elpased: 1.428
batch start
#iterations: 289
currently lose_sum: 96.39948207139969
time_elpased: 1.482
batch start
#iterations: 290
currently lose_sum: 95.65169441699982
time_elpased: 1.466
batch start
#iterations: 291
currently lose_sum: 96.25287246704102
time_elpased: 1.491
batch start
#iterations: 292
currently lose_sum: 96.15505313873291
time_elpased: 1.477
batch start
#iterations: 293
currently lose_sum: 95.78847759962082
time_elpased: 1.442
batch start
#iterations: 294
currently lose_sum: 96.57954174280167
time_elpased: 1.474
batch start
#iterations: 295
currently lose_sum: 96.2842178940773
time_elpased: 1.498
batch start
#iterations: 296
currently lose_sum: 96.29633802175522
time_elpased: 1.477
batch start
#iterations: 297
currently lose_sum: 96.01987493038177
time_elpased: 1.524
batch start
#iterations: 298
currently lose_sum: 96.33379656076431
time_elpased: 1.475
batch start
#iterations: 299
currently lose_sum: 96.11913353204727
time_elpased: 1.423
start validation test
0.639742268041
0.613818424566
0.757023772769
0.677941108705
0.63953636227
62.051
batch start
#iterations: 300
currently lose_sum: 96.06135994195938
time_elpased: 1.511
batch start
#iterations: 301
currently lose_sum: 96.48029482364655
time_elpased: 1.452
batch start
#iterations: 302
currently lose_sum: 96.17941278219223
time_elpased: 1.502
batch start
#iterations: 303
currently lose_sum: 96.11860281229019
time_elpased: 1.49
batch start
#iterations: 304
currently lose_sum: 96.19738560914993
time_elpased: 1.446
batch start
#iterations: 305
currently lose_sum: 96.12989377975464
time_elpased: 1.469
batch start
#iterations: 306
currently lose_sum: 96.1915009021759
time_elpased: 1.471
batch start
#iterations: 307
currently lose_sum: 96.23729115724564
time_elpased: 1.44
batch start
#iterations: 308
currently lose_sum: 96.39246767759323
time_elpased: 1.456
batch start
#iterations: 309
currently lose_sum: 95.53626143932343
time_elpased: 1.452
batch start
#iterations: 310
currently lose_sum: 96.10214465856552
time_elpased: 1.494
batch start
#iterations: 311
currently lose_sum: 96.31128871440887
time_elpased: 1.434
batch start
#iterations: 312
currently lose_sum: 96.08868664503098
time_elpased: 1.456
batch start
#iterations: 313
currently lose_sum: 95.83334040641785
time_elpased: 1.466
batch start
#iterations: 314
currently lose_sum: 96.34906733036041
time_elpased: 1.497
batch start
#iterations: 315
currently lose_sum: 96.15817838907242
time_elpased: 1.472
batch start
#iterations: 316
currently lose_sum: 95.80524295568466
time_elpased: 1.457
batch start
#iterations: 317
currently lose_sum: 96.59428268671036
time_elpased: 1.508
batch start
#iterations: 318
currently lose_sum: 95.99516177177429
time_elpased: 1.483
batch start
#iterations: 319
currently lose_sum: 96.12786269187927
time_elpased: 1.432
start validation test
0.640618556701
0.614137214137
0.760008232994
0.679330328397
0.64040894971
62.088
batch start
#iterations: 320
currently lose_sum: 95.8448588848114
time_elpased: 1.465
batch start
#iterations: 321
currently lose_sum: 96.32660973072052
time_elpased: 1.44
batch start
#iterations: 322
currently lose_sum: 96.32013630867004
time_elpased: 1.459
batch start
#iterations: 323
currently lose_sum: 96.33736008405685
time_elpased: 1.447
batch start
#iterations: 324
currently lose_sum: 96.21769970655441
time_elpased: 1.478
batch start
#iterations: 325
currently lose_sum: 96.2301259636879
time_elpased: 1.433
batch start
#iterations: 326
currently lose_sum: 95.8045043349266
time_elpased: 1.457
batch start
#iterations: 327
currently lose_sum: 96.14464497566223
time_elpased: 1.478
batch start
#iterations: 328
currently lose_sum: 96.33945822715759
time_elpased: 1.455
batch start
#iterations: 329
currently lose_sum: 95.83041793107986
time_elpased: 1.46
batch start
#iterations: 330
currently lose_sum: 95.85606241226196
time_elpased: 1.44
batch start
#iterations: 331
currently lose_sum: 96.29161977767944
time_elpased: 1.436
batch start
#iterations: 332
currently lose_sum: 96.05211716890335
time_elpased: 1.515
batch start
#iterations: 333
currently lose_sum: 96.10697597265244
time_elpased: 1.451
batch start
#iterations: 334
currently lose_sum: 95.95791083574295
time_elpased: 1.466
batch start
#iterations: 335
currently lose_sum: 95.6246754527092
time_elpased: 1.493
batch start
#iterations: 336
currently lose_sum: 95.86460244655609
time_elpased: 1.48
batch start
#iterations: 337
currently lose_sum: 96.47636640071869
time_elpased: 1.427
batch start
#iterations: 338
currently lose_sum: 96.04883897304535
time_elpased: 1.471
batch start
#iterations: 339
currently lose_sum: 96.25165712833405
time_elpased: 1.444
start validation test
0.640360824742
0.615378137106
0.751981064114
0.676856097448
0.640164858196
62.145
batch start
#iterations: 340
currently lose_sum: 96.13753294944763
time_elpased: 1.509
batch start
#iterations: 341
currently lose_sum: 96.01078355312347
time_elpased: 1.442
batch start
#iterations: 342
currently lose_sum: 96.2689516544342
time_elpased: 1.456
batch start
#iterations: 343
currently lose_sum: 96.00049954652786
time_elpased: 1.469
batch start
#iterations: 344
currently lose_sum: 95.60660898685455
time_elpased: 1.466
batch start
#iterations: 345
currently lose_sum: 96.1764709353447
time_elpased: 1.453
batch start
#iterations: 346
currently lose_sum: 96.04811590909958
time_elpased: 1.437
batch start
#iterations: 347
currently lose_sum: 95.88312339782715
time_elpased: 1.462
batch start
#iterations: 348
currently lose_sum: 96.11341887712479
time_elpased: 1.451
batch start
#iterations: 349
currently lose_sum: 96.08381134271622
time_elpased: 1.449
batch start
#iterations: 350
currently lose_sum: 96.26573020219803
time_elpased: 1.522
batch start
#iterations: 351
currently lose_sum: 95.7150382399559
time_elpased: 1.485
batch start
#iterations: 352
currently lose_sum: 96.3099867105484
time_elpased: 1.463
batch start
#iterations: 353
currently lose_sum: 95.77002704143524
time_elpased: 1.489
batch start
#iterations: 354
currently lose_sum: 96.35600167512894
time_elpased: 1.455
batch start
#iterations: 355
currently lose_sum: 96.225601375103
time_elpased: 1.457
batch start
#iterations: 356
currently lose_sum: 96.10831171274185
time_elpased: 1.482
batch start
#iterations: 357
currently lose_sum: 96.28587657213211
time_elpased: 1.454
batch start
#iterations: 358
currently lose_sum: 96.20781230926514
time_elpased: 1.491
batch start
#iterations: 359
currently lose_sum: 96.02649658918381
time_elpased: 1.428
start validation test
0.638350515464
0.609984526427
0.770814037254
0.681032915075
0.63811795532
62.053
batch start
#iterations: 360
currently lose_sum: 96.20779299736023
time_elpased: 1.433
batch start
#iterations: 361
currently lose_sum: 96.33805853128433
time_elpased: 1.456
batch start
#iterations: 362
currently lose_sum: 96.15179461240768
time_elpased: 1.442
batch start
#iterations: 363
currently lose_sum: 95.7724044919014
time_elpased: 1.48
batch start
#iterations: 364
currently lose_sum: 96.09915369749069
time_elpased: 1.497
batch start
#iterations: 365
currently lose_sum: 96.26211708784103
time_elpased: 1.506
batch start
#iterations: 366
currently lose_sum: 96.15786236524582
time_elpased: 1.497
batch start
#iterations: 367
currently lose_sum: 95.72327256202698
time_elpased: 1.47
batch start
#iterations: 368
currently lose_sum: 96.15377235412598
time_elpased: 1.497
batch start
#iterations: 369
currently lose_sum: 96.04585552215576
time_elpased: 1.457
batch start
#iterations: 370
currently lose_sum: 96.00663185119629
time_elpased: 1.45
batch start
#iterations: 371
currently lose_sum: 95.97266715765
time_elpased: 1.514
batch start
#iterations: 372
currently lose_sum: 95.76884877681732
time_elpased: 1.499
batch start
#iterations: 373
currently lose_sum: 95.92323464155197
time_elpased: 1.434
batch start
#iterations: 374
currently lose_sum: 96.21824538707733
time_elpased: 1.436
batch start
#iterations: 375
currently lose_sum: 96.11406403779984
time_elpased: 1.446
batch start
#iterations: 376
currently lose_sum: 95.86814320087433
time_elpased: 1.433
batch start
#iterations: 377
currently lose_sum: 96.02609485387802
time_elpased: 1.451
batch start
#iterations: 378
currently lose_sum: 96.21871417760849
time_elpased: 1.437
batch start
#iterations: 379
currently lose_sum: 95.98566097021103
time_elpased: 1.548
start validation test
0.637422680412
0.617685761909
0.724606359988
0.666887668119
0.637269616016
62.275
batch start
#iterations: 380
currently lose_sum: 96.03098261356354
time_elpased: 1.483
batch start
#iterations: 381
currently lose_sum: 95.94995337724686
time_elpased: 1.456
batch start
#iterations: 382
currently lose_sum: 96.30395728349686
time_elpased: 1.417
batch start
#iterations: 383
currently lose_sum: 96.07538735866547
time_elpased: 1.455
batch start
#iterations: 384
currently lose_sum: 96.27822941541672
time_elpased: 1.488
batch start
#iterations: 385
currently lose_sum: 96.08054876327515
time_elpased: 1.456
batch start
#iterations: 386
currently lose_sum: 96.39949184656143
time_elpased: 1.451
batch start
#iterations: 387
currently lose_sum: 96.2418395280838
time_elpased: 1.516
batch start
#iterations: 388
currently lose_sum: 96.25557059049606
time_elpased: 1.498
batch start
#iterations: 389
currently lose_sum: 96.0158760547638
time_elpased: 1.417
batch start
#iterations: 390
currently lose_sum: 96.26702922582626
time_elpased: 1.44
batch start
#iterations: 391
currently lose_sum: 96.01905977725983
time_elpased: 1.486
batch start
#iterations: 392
currently lose_sum: 95.97797757387161
time_elpased: 1.509
batch start
#iterations: 393
currently lose_sum: 96.01217049360275
time_elpased: 1.484
batch start
#iterations: 394
currently lose_sum: 96.2707514166832
time_elpased: 1.462
batch start
#iterations: 395
currently lose_sum: 96.00361895561218
time_elpased: 1.484
batch start
#iterations: 396
currently lose_sum: 95.98014545440674
time_elpased: 1.462
batch start
#iterations: 397
currently lose_sum: 95.89404678344727
time_elpased: 1.46
batch start
#iterations: 398
currently lose_sum: 96.10544723272324
time_elpased: 1.455
batch start
#iterations: 399
currently lose_sum: 95.95481902360916
time_elpased: 1.463
start validation test
0.635670103093
0.615424836601
0.72676752084
0.666477916195
0.635510167525
62.230
acc: 0.638
pre: 0.612
rec: 0.759
F1: 0.678
auc: 0.677
