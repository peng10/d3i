start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.78427332639694
time_elpased: 1.462
batch start
#iterations: 1
currently lose_sum: 100.24175095558167
time_elpased: 1.45
batch start
#iterations: 2
currently lose_sum: 100.25651913881302
time_elpased: 1.445
batch start
#iterations: 3
currently lose_sum: 100.00815284252167
time_elpased: 1.444
batch start
#iterations: 4
currently lose_sum: 99.9161587357521
time_elpased: 1.424
batch start
#iterations: 5
currently lose_sum: 99.90456640720367
time_elpased: 1.456
batch start
#iterations: 6
currently lose_sum: 99.60258835554123
time_elpased: 1.455
batch start
#iterations: 7
currently lose_sum: 99.56430947780609
time_elpased: 1.416
batch start
#iterations: 8
currently lose_sum: 99.26069438457489
time_elpased: 1.481
batch start
#iterations: 9
currently lose_sum: 99.39470559358597
time_elpased: 1.439
batch start
#iterations: 10
currently lose_sum: 99.1816435456276
time_elpased: 1.433
batch start
#iterations: 11
currently lose_sum: 99.00284725427628
time_elpased: 1.415
batch start
#iterations: 12
currently lose_sum: 99.12851941585541
time_elpased: 1.426
batch start
#iterations: 13
currently lose_sum: 98.8885349035263
time_elpased: 1.45
batch start
#iterations: 14
currently lose_sum: 98.75976431369781
time_elpased: 1.47
batch start
#iterations: 15
currently lose_sum: 98.69905996322632
time_elpased: 1.441
batch start
#iterations: 16
currently lose_sum: 98.52760463953018
time_elpased: 1.406
batch start
#iterations: 17
currently lose_sum: 98.67469692230225
time_elpased: 1.445
batch start
#iterations: 18
currently lose_sum: 98.30424559116364
time_elpased: 1.433
batch start
#iterations: 19
currently lose_sum: 98.48339927196503
time_elpased: 1.424
start validation test
0.610979381443
0.64
0.510445610785
0.567928092975
0.611155883984
64.026
batch start
#iterations: 20
currently lose_sum: 98.19759160280228
time_elpased: 1.418
batch start
#iterations: 21
currently lose_sum: 98.03196835517883
time_elpased: 1.439
batch start
#iterations: 22
currently lose_sum: 98.16797059774399
time_elpased: 1.438
batch start
#iterations: 23
currently lose_sum: 98.20858144760132
time_elpased: 1.428
batch start
#iterations: 24
currently lose_sum: 98.1911449432373
time_elpased: 1.412
batch start
#iterations: 25
currently lose_sum: 97.69548499584198
time_elpased: 1.453
batch start
#iterations: 26
currently lose_sum: 97.58798372745514
time_elpased: 1.384
batch start
#iterations: 27
currently lose_sum: 97.78055292367935
time_elpased: 1.493
batch start
#iterations: 28
currently lose_sum: 97.376669049263
time_elpased: 1.478
batch start
#iterations: 29
currently lose_sum: 97.64945322275162
time_elpased: 1.426
batch start
#iterations: 30
currently lose_sum: 97.51033967733383
time_elpased: 1.441
batch start
#iterations: 31
currently lose_sum: 97.20338445901871
time_elpased: 1.426
batch start
#iterations: 32
currently lose_sum: 97.3330420255661
time_elpased: 1.421
batch start
#iterations: 33
currently lose_sum: 97.4421694278717
time_elpased: 1.43
batch start
#iterations: 34
currently lose_sum: 97.39460533857346
time_elpased: 1.463
batch start
#iterations: 35
currently lose_sum: 97.3026322722435
time_elpased: 1.465
batch start
#iterations: 36
currently lose_sum: 97.465824842453
time_elpased: 1.442
batch start
#iterations: 37
currently lose_sum: 97.28091663122177
time_elpased: 1.464
batch start
#iterations: 38
currently lose_sum: 97.14864134788513
time_elpased: 1.475
batch start
#iterations: 39
currently lose_sum: 97.09341490268707
time_elpased: 1.42
start validation test
0.632371134021
0.617961120745
0.696820006175
0.655025636065
0.632257984085
63.044
batch start
#iterations: 40
currently lose_sum: 97.28512626886368
time_elpased: 1.447
batch start
#iterations: 41
currently lose_sum: 97.30465996265411
time_elpased: 1.539
batch start
#iterations: 42
currently lose_sum: 97.21824163198471
time_elpased: 1.458
batch start
#iterations: 43
currently lose_sum: 97.2373588681221
time_elpased: 1.431
batch start
#iterations: 44
currently lose_sum: 97.24415504932404
time_elpased: 1.445
batch start
#iterations: 45
currently lose_sum: 97.0598629117012
time_elpased: 1.468
batch start
#iterations: 46
currently lose_sum: 96.8780506849289
time_elpased: 1.446
batch start
#iterations: 47
currently lose_sum: 96.9447695016861
time_elpased: 1.413
batch start
#iterations: 48
currently lose_sum: 97.32824271917343
time_elpased: 1.469
batch start
#iterations: 49
currently lose_sum: 97.2305000424385
time_elpased: 1.415
batch start
#iterations: 50
currently lose_sum: 97.42305153608322
time_elpased: 1.457
batch start
#iterations: 51
currently lose_sum: 96.75137460231781
time_elpased: 1.468
batch start
#iterations: 52
currently lose_sum: 96.74287486076355
time_elpased: 1.443
batch start
#iterations: 53
currently lose_sum: 96.93293368816376
time_elpased: 1.436
batch start
#iterations: 54
currently lose_sum: 97.0780987739563
time_elpased: 1.404
batch start
#iterations: 55
currently lose_sum: 96.79471325874329
time_elpased: 1.449
batch start
#iterations: 56
currently lose_sum: 96.99804246425629
time_elpased: 1.446
batch start
#iterations: 57
currently lose_sum: 96.58043134212494
time_elpased: 1.403
batch start
#iterations: 58
currently lose_sum: 97.15262532234192
time_elpased: 1.459
batch start
#iterations: 59
currently lose_sum: 96.83863121271133
time_elpased: 1.427
start validation test
0.633917525773
0.607462809238
0.760625707523
0.675470663498
0.633695070017
62.627
batch start
#iterations: 60
currently lose_sum: 97.16607332229614
time_elpased: 1.403
batch start
#iterations: 61
currently lose_sum: 96.74840742349625
time_elpased: 1.428
batch start
#iterations: 62
currently lose_sum: 97.03124606609344
time_elpased: 1.439
batch start
#iterations: 63
currently lose_sum: 97.29313486814499
time_elpased: 1.403
batch start
#iterations: 64
currently lose_sum: 97.2104019522667
time_elpased: 1.408
batch start
#iterations: 65
currently lose_sum: 96.49556583166122
time_elpased: 1.45
batch start
#iterations: 66
currently lose_sum: 96.41266572475433
time_elpased: 1.45
batch start
#iterations: 67
currently lose_sum: 96.55644369125366
time_elpased: 1.423
batch start
#iterations: 68
currently lose_sum: 96.83496671915054
time_elpased: 1.424
batch start
#iterations: 69
currently lose_sum: 96.94796758890152
time_elpased: 1.429
batch start
#iterations: 70
currently lose_sum: 96.5893828868866
time_elpased: 1.453
batch start
#iterations: 71
currently lose_sum: 96.4460060596466
time_elpased: 1.446
batch start
#iterations: 72
currently lose_sum: 96.58187526464462
time_elpased: 1.454
batch start
#iterations: 73
currently lose_sum: 96.32454001903534
time_elpased: 1.464
batch start
#iterations: 74
currently lose_sum: 96.47007983922958
time_elpased: 1.453
batch start
#iterations: 75
currently lose_sum: 96.439523935318
time_elpased: 1.444
batch start
#iterations: 76
currently lose_sum: 96.28799027204514
time_elpased: 1.442
batch start
#iterations: 77
currently lose_sum: 96.7295892238617
time_elpased: 1.449
batch start
#iterations: 78
currently lose_sum: 96.62721884250641
time_elpased: 1.45
batch start
#iterations: 79
currently lose_sum: 96.77963846921921
time_elpased: 1.43
start validation test
0.638711340206
0.615785873097
0.741072347432
0.672644902153
0.63853162967
62.489
batch start
#iterations: 80
currently lose_sum: 96.40505301952362
time_elpased: 1.505
batch start
#iterations: 81
currently lose_sum: 97.03991544246674
time_elpased: 1.433
batch start
#iterations: 82
currently lose_sum: 96.40985333919525
time_elpased: 1.435
batch start
#iterations: 83
currently lose_sum: 96.99146246910095
time_elpased: 1.44
batch start
#iterations: 84
currently lose_sum: 96.5940511226654
time_elpased: 1.434
batch start
#iterations: 85
currently lose_sum: 96.80142396688461
time_elpased: 1.441
batch start
#iterations: 86
currently lose_sum: 96.20082098245621
time_elpased: 1.46
batch start
#iterations: 87
currently lose_sum: 96.58803486824036
time_elpased: 1.534
batch start
#iterations: 88
currently lose_sum: 96.58380049467087
time_elpased: 1.443
batch start
#iterations: 89
currently lose_sum: 96.5651963353157
time_elpased: 1.49
batch start
#iterations: 90
currently lose_sum: 96.65039306879044
time_elpased: 1.426
batch start
#iterations: 91
currently lose_sum: 96.14227306842804
time_elpased: 1.404
batch start
#iterations: 92
currently lose_sum: 96.50024497509003
time_elpased: 1.447
batch start
#iterations: 93
currently lose_sum: 96.37265890836716
time_elpased: 1.488
batch start
#iterations: 94
currently lose_sum: 96.47589880228043
time_elpased: 1.423
batch start
#iterations: 95
currently lose_sum: 96.64841318130493
time_elpased: 1.462
batch start
#iterations: 96
currently lose_sum: 96.62881416082382
time_elpased: 1.432
batch start
#iterations: 97
currently lose_sum: 96.6413836479187
time_elpased: 1.436
batch start
#iterations: 98
currently lose_sum: 96.3372533917427
time_elpased: 1.446
batch start
#iterations: 99
currently lose_sum: 96.46365535259247
time_elpased: 1.439
start validation test
0.630721649485
0.605766840666
0.752392713801
0.671164968328
0.630508037165
62.598
batch start
#iterations: 100
currently lose_sum: 96.11788707971573
time_elpased: 1.436
batch start
#iterations: 101
currently lose_sum: 96.50170457363129
time_elpased: 1.442
batch start
#iterations: 102
currently lose_sum: 96.31756776571274
time_elpased: 1.427
batch start
#iterations: 103
currently lose_sum: 96.66786366701126
time_elpased: 1.408
batch start
#iterations: 104
currently lose_sum: 96.34818512201309
time_elpased: 1.454
batch start
#iterations: 105
currently lose_sum: 96.2315593957901
time_elpased: 1.409
batch start
#iterations: 106
currently lose_sum: 96.55633354187012
time_elpased: 1.478
batch start
#iterations: 107
currently lose_sum: 96.14797168970108
time_elpased: 1.458
batch start
#iterations: 108
currently lose_sum: 96.12777054309845
time_elpased: 1.486
batch start
#iterations: 109
currently lose_sum: 96.24197471141815
time_elpased: 1.511
batch start
#iterations: 110
currently lose_sum: 96.6981874704361
time_elpased: 1.438
batch start
#iterations: 111
currently lose_sum: 96.1865171790123
time_elpased: 1.469
batch start
#iterations: 112
currently lose_sum: 96.38023275136948
time_elpased: 1.434
batch start
#iterations: 113
currently lose_sum: 96.05501705408096
time_elpased: 1.446
batch start
#iterations: 114
currently lose_sum: 96.60737353563309
time_elpased: 1.435
batch start
#iterations: 115
currently lose_sum: 96.37959343194962
time_elpased: 1.412
batch start
#iterations: 116
currently lose_sum: 96.16936755180359
time_elpased: 1.459
batch start
#iterations: 117
currently lose_sum: 96.57387924194336
time_elpased: 1.454
batch start
#iterations: 118
currently lose_sum: 96.34113246202469
time_elpased: 1.43
batch start
#iterations: 119
currently lose_sum: 96.1815784573555
time_elpased: 1.458
start validation test
0.64087628866
0.614335606187
0.760316970258
0.679575035644
0.640666592121
62.177
batch start
#iterations: 120
currently lose_sum: 96.36833691596985
time_elpased: 1.436
batch start
#iterations: 121
currently lose_sum: 96.41532242298126
time_elpased: 1.45
batch start
#iterations: 122
currently lose_sum: 96.3358873128891
time_elpased: 1.439
batch start
#iterations: 123
currently lose_sum: 95.98305892944336
time_elpased: 1.439
batch start
#iterations: 124
currently lose_sum: 96.37285935878754
time_elpased: 1.459
batch start
#iterations: 125
currently lose_sum: 96.444207072258
time_elpased: 1.445
batch start
#iterations: 126
currently lose_sum: 96.46853697299957
time_elpased: 1.498
batch start
#iterations: 127
currently lose_sum: 96.487708568573
time_elpased: 1.476
batch start
#iterations: 128
currently lose_sum: 96.27842247486115
time_elpased: 1.458
batch start
#iterations: 129
currently lose_sum: 96.24949377775192
time_elpased: 1.486
batch start
#iterations: 130
currently lose_sum: 96.12070786952972
time_elpased: 1.52
batch start
#iterations: 131
currently lose_sum: 96.28217458724976
time_elpased: 1.427
batch start
#iterations: 132
currently lose_sum: 96.30720311403275
time_elpased: 1.48
batch start
#iterations: 133
currently lose_sum: 96.27938640117645
time_elpased: 1.426
batch start
#iterations: 134
currently lose_sum: 96.07341516017914
time_elpased: 1.447
batch start
#iterations: 135
currently lose_sum: 96.58934688568115
time_elpased: 1.426
batch start
#iterations: 136
currently lose_sum: 96.27648729085922
time_elpased: 1.442
batch start
#iterations: 137
currently lose_sum: 95.93688237667084
time_elpased: 1.492
batch start
#iterations: 138
currently lose_sum: 96.1131232380867
time_elpased: 1.45
batch start
#iterations: 139
currently lose_sum: 96.5082813501358
time_elpased: 1.447
start validation test
0.639639175258
0.621805183199
0.716064629001
0.665614387526
0.639504998586
62.333
batch start
#iterations: 140
currently lose_sum: 96.1405736207962
time_elpased: 1.428
batch start
#iterations: 141
currently lose_sum: 96.40619957447052
time_elpased: 1.445
batch start
#iterations: 142
currently lose_sum: 96.00693970918655
time_elpased: 1.468
batch start
#iterations: 143
currently lose_sum: 96.43463551998138
time_elpased: 1.435
batch start
#iterations: 144
currently lose_sum: 95.96460920572281
time_elpased: 1.493
batch start
#iterations: 145
currently lose_sum: 96.25915914773941
time_elpased: 1.43
batch start
#iterations: 146
currently lose_sum: 96.21520185470581
time_elpased: 1.402
batch start
#iterations: 147
currently lose_sum: 96.3301602602005
time_elpased: 1.431
batch start
#iterations: 148
currently lose_sum: 96.18800330162048
time_elpased: 1.457
batch start
#iterations: 149
currently lose_sum: 96.53058689832687
time_elpased: 1.476
batch start
#iterations: 150
currently lose_sum: 96.28568422794342
time_elpased: 1.431
batch start
#iterations: 151
currently lose_sum: 96.46111822128296
time_elpased: 1.45
batch start
#iterations: 152
currently lose_sum: 96.0452231168747
time_elpased: 1.427
batch start
#iterations: 153
currently lose_sum: 96.02462875843048
time_elpased: 1.408
batch start
#iterations: 154
currently lose_sum: 96.38785564899445
time_elpased: 1.425
batch start
#iterations: 155
currently lose_sum: 96.54162472486496
time_elpased: 1.481
batch start
#iterations: 156
currently lose_sum: 95.842698097229
time_elpased: 1.462
batch start
#iterations: 157
currently lose_sum: 96.09819561243057
time_elpased: 1.465
batch start
#iterations: 158
currently lose_sum: 96.2942715883255
time_elpased: 1.466
batch start
#iterations: 159
currently lose_sum: 96.32497030496597
time_elpased: 1.441
start validation test
0.640206185567
0.614605141948
0.755274261603
0.677717240742
0.640004165811
62.228
batch start
#iterations: 160
currently lose_sum: 96.45801627635956
time_elpased: 1.444
batch start
#iterations: 161
currently lose_sum: 96.15875792503357
time_elpased: 1.417
batch start
#iterations: 162
currently lose_sum: 95.99769991636276
time_elpased: 1.414
batch start
#iterations: 163
currently lose_sum: 96.11400830745697
time_elpased: 1.428
batch start
#iterations: 164
currently lose_sum: 96.44684958457947
time_elpased: 1.467
batch start
#iterations: 165
currently lose_sum: 96.35825496912003
time_elpased: 1.442
batch start
#iterations: 166
currently lose_sum: 96.02167481184006
time_elpased: 1.433
batch start
#iterations: 167
currently lose_sum: 96.1946611404419
time_elpased: 1.449
batch start
#iterations: 168
currently lose_sum: 95.98739016056061
time_elpased: 1.445
batch start
#iterations: 169
currently lose_sum: 96.41168063879013
time_elpased: 1.471
batch start
#iterations: 170
currently lose_sum: 96.10514688491821
time_elpased: 1.461
batch start
#iterations: 171
currently lose_sum: 96.3248280286789
time_elpased: 1.442
batch start
#iterations: 172
currently lose_sum: 96.14307302236557
time_elpased: 1.5
batch start
#iterations: 173
currently lose_sum: 96.11987096071243
time_elpased: 1.495
batch start
#iterations: 174
currently lose_sum: 95.93936860561371
time_elpased: 1.446
batch start
#iterations: 175
currently lose_sum: 96.16274416446686
time_elpased: 1.408
batch start
#iterations: 176
currently lose_sum: 96.41907203197479
time_elpased: 1.459
batch start
#iterations: 177
currently lose_sum: 96.23656404018402
time_elpased: 1.502
batch start
#iterations: 178
currently lose_sum: 96.10226619243622
time_elpased: 1.427
batch start
#iterations: 179
currently lose_sum: 95.79910719394684
time_elpased: 1.446
start validation test
0.636288659794
0.603856061197
0.79613049295
0.686789772727
0.636008032802
62.258
batch start
#iterations: 180
currently lose_sum: 96.37352722883224
time_elpased: 1.411
batch start
#iterations: 181
currently lose_sum: 95.81615251302719
time_elpased: 1.41
batch start
#iterations: 182
currently lose_sum: 96.38646191358566
time_elpased: 1.447
batch start
#iterations: 183
currently lose_sum: 95.97507303953171
time_elpased: 1.433
batch start
#iterations: 184
currently lose_sum: 96.20046502351761
time_elpased: 1.469
batch start
#iterations: 185
currently lose_sum: 95.85859858989716
time_elpased: 1.44
batch start
#iterations: 186
currently lose_sum: 95.89611512422562
time_elpased: 1.441
batch start
#iterations: 187
currently lose_sum: 96.18814784288406
time_elpased: 1.449
batch start
#iterations: 188
currently lose_sum: 96.0626180768013
time_elpased: 1.45
batch start
#iterations: 189
currently lose_sum: 95.84749436378479
time_elpased: 1.396
batch start
#iterations: 190
currently lose_sum: 96.22690975666046
time_elpased: 1.433
batch start
#iterations: 191
currently lose_sum: 95.87776345014572
time_elpased: 1.481
batch start
#iterations: 192
currently lose_sum: 95.93940824270248
time_elpased: 1.452
batch start
#iterations: 193
currently lose_sum: 96.09674835205078
time_elpased: 1.417
batch start
#iterations: 194
currently lose_sum: 96.13349241018295
time_elpased: 1.449
batch start
#iterations: 195
currently lose_sum: 95.99520605802536
time_elpased: 1.444
batch start
#iterations: 196
currently lose_sum: 96.02275425195694
time_elpased: 1.497
batch start
#iterations: 197
currently lose_sum: 96.18296039104462
time_elpased: 1.445
batch start
#iterations: 198
currently lose_sum: 96.12121176719666
time_elpased: 1.458
batch start
#iterations: 199
currently lose_sum: 96.4083132147789
time_elpased: 1.491
start validation test
0.636597938144
0.623323776935
0.693629721107
0.656600097418
0.636497810053
62.302
batch start
#iterations: 200
currently lose_sum: 96.28298503160477
time_elpased: 1.453
batch start
#iterations: 201
currently lose_sum: 95.83829319477081
time_elpased: 1.432
batch start
#iterations: 202
currently lose_sum: 96.12844443321228
time_elpased: 1.486
batch start
#iterations: 203
currently lose_sum: 96.2305399775505
time_elpased: 1.428
batch start
#iterations: 204
currently lose_sum: 96.21688383817673
time_elpased: 1.495
batch start
#iterations: 205
currently lose_sum: 95.90945994853973
time_elpased: 1.407
batch start
#iterations: 206
currently lose_sum: 95.88821113109589
time_elpased: 1.481
batch start
#iterations: 207
currently lose_sum: 96.01878327131271
time_elpased: 1.428
batch start
#iterations: 208
currently lose_sum: 95.68221265077591
time_elpased: 1.454
batch start
#iterations: 209
currently lose_sum: 95.96480578184128
time_elpased: 1.475
batch start
#iterations: 210
currently lose_sum: 95.9871637225151
time_elpased: 1.453
batch start
#iterations: 211
currently lose_sum: 95.91531890630722
time_elpased: 1.486
batch start
#iterations: 212
currently lose_sum: 95.77837723493576
time_elpased: 1.437
batch start
#iterations: 213
currently lose_sum: 95.49983638525009
time_elpased: 1.452
batch start
#iterations: 214
currently lose_sum: 95.7291561961174
time_elpased: 1.486
batch start
#iterations: 215
currently lose_sum: 95.89136242866516
time_elpased: 1.456
batch start
#iterations: 216
currently lose_sum: 96.06630945205688
time_elpased: 1.446
batch start
#iterations: 217
currently lose_sum: 95.76981431245804
time_elpased: 1.445
batch start
#iterations: 218
currently lose_sum: 96.02788931131363
time_elpased: 1.426
batch start
#iterations: 219
currently lose_sum: 95.92147326469421
time_elpased: 1.428
start validation test
0.637474226804
0.617636746143
0.725120922095
0.667076923077
0.637320349512
62.320
batch start
#iterations: 220
currently lose_sum: 96.181236743927
time_elpased: 1.44
batch start
#iterations: 221
currently lose_sum: 96.1504562497139
time_elpased: 1.424
batch start
#iterations: 222
currently lose_sum: 95.90765792131424
time_elpased: 1.47
batch start
#iterations: 223
currently lose_sum: 95.89846849441528
time_elpased: 1.454
batch start
#iterations: 224
currently lose_sum: 96.16775357723236
time_elpased: 1.447
batch start
#iterations: 225
currently lose_sum: 96.08023464679718
time_elpased: 1.451
batch start
#iterations: 226
currently lose_sum: 96.25078189373016
time_elpased: 1.44
batch start
#iterations: 227
currently lose_sum: 96.07618999481201
time_elpased: 1.426
batch start
#iterations: 228
currently lose_sum: 95.9312157034874
time_elpased: 1.436
batch start
#iterations: 229
currently lose_sum: 95.91081774234772
time_elpased: 1.437
batch start
#iterations: 230
currently lose_sum: 96.04243677854538
time_elpased: 1.461
batch start
#iterations: 231
currently lose_sum: 95.64108902215958
time_elpased: 1.461
batch start
#iterations: 232
currently lose_sum: 95.94999599456787
time_elpased: 1.487
batch start
#iterations: 233
currently lose_sum: 96.07396513223648
time_elpased: 1.467
batch start
#iterations: 234
currently lose_sum: 96.31125515699387
time_elpased: 1.475
batch start
#iterations: 235
currently lose_sum: 95.738476395607
time_elpased: 1.454
batch start
#iterations: 236
currently lose_sum: 96.0336030125618
time_elpased: 1.473
batch start
#iterations: 237
currently lose_sum: 96.06203901767731
time_elpased: 1.499
batch start
#iterations: 238
currently lose_sum: 95.84528684616089
time_elpased: 1.456
batch start
#iterations: 239
currently lose_sum: 95.82081532478333
time_elpased: 1.457
start validation test
0.641804123711
0.611111111111
0.783369352681
0.686600820818
0.641555584117
62.079
batch start
#iterations: 240
currently lose_sum: 95.94486492872238
time_elpased: 1.438
batch start
#iterations: 241
currently lose_sum: 95.76803731918335
time_elpased: 1.462
batch start
#iterations: 242
currently lose_sum: 96.03657913208008
time_elpased: 1.408
batch start
#iterations: 243
currently lose_sum: 96.10219472646713
time_elpased: 1.487
batch start
#iterations: 244
currently lose_sum: 96.08363157510757
time_elpased: 1.419
batch start
#iterations: 245
currently lose_sum: 95.92500030994415
time_elpased: 1.425
batch start
#iterations: 246
currently lose_sum: 96.15232694149017
time_elpased: 1.48
batch start
#iterations: 247
currently lose_sum: 95.83512037992477
time_elpased: 1.426
batch start
#iterations: 248
currently lose_sum: 95.98820024728775
time_elpased: 1.461
batch start
#iterations: 249
currently lose_sum: 96.07152539491653
time_elpased: 1.436
batch start
#iterations: 250
currently lose_sum: 96.16877806186676
time_elpased: 1.454
batch start
#iterations: 251
currently lose_sum: 95.86454075574875
time_elpased: 1.439
batch start
#iterations: 252
currently lose_sum: 95.82768470048904
time_elpased: 1.459
batch start
#iterations: 253
currently lose_sum: 95.96612399816513
time_elpased: 1.461
batch start
#iterations: 254
currently lose_sum: 95.81237196922302
time_elpased: 1.443
batch start
#iterations: 255
currently lose_sum: 95.8752156496048
time_elpased: 1.454
batch start
#iterations: 256
currently lose_sum: 96.00080287456512
time_elpased: 1.434
batch start
#iterations: 257
currently lose_sum: 95.80343306064606
time_elpased: 1.422
batch start
#iterations: 258
currently lose_sum: 95.79977059364319
time_elpased: 1.47
batch start
#iterations: 259
currently lose_sum: 95.86552608013153
time_elpased: 1.436
start validation test
0.634896907216
0.612814802124
0.73623546362
0.668879435277
0.63471899175
62.251
batch start
#iterations: 260
currently lose_sum: 95.62277460098267
time_elpased: 1.506
batch start
#iterations: 261
currently lose_sum: 96.13155668973923
time_elpased: 1.437
batch start
#iterations: 262
currently lose_sum: 95.93253362178802
time_elpased: 1.466
batch start
#iterations: 263
currently lose_sum: 95.93659138679504
time_elpased: 1.437
batch start
#iterations: 264
currently lose_sum: 96.21117067337036
time_elpased: 1.462
batch start
#iterations: 265
currently lose_sum: 95.66555631160736
time_elpased: 1.45
batch start
#iterations: 266
currently lose_sum: 96.05997037887573
time_elpased: 1.449
batch start
#iterations: 267
currently lose_sum: 95.82832992076874
time_elpased: 1.429
batch start
#iterations: 268
currently lose_sum: 95.80525326728821
time_elpased: 1.443
batch start
#iterations: 269
currently lose_sum: 95.61236476898193
time_elpased: 1.43
batch start
#iterations: 270
currently lose_sum: 96.05418360233307
time_elpased: 1.44
batch start
#iterations: 271
currently lose_sum: 96.02987933158875
time_elpased: 1.452
batch start
#iterations: 272
currently lose_sum: 95.6843763589859
time_elpased: 1.478
batch start
#iterations: 273
currently lose_sum: 96.05865281820297
time_elpased: 1.443
batch start
#iterations: 274
currently lose_sum: 95.66350334882736
time_elpased: 1.479
batch start
#iterations: 275
currently lose_sum: 95.76430153846741
time_elpased: 1.46
batch start
#iterations: 276
currently lose_sum: 96.12772458791733
time_elpased: 1.491
batch start
#iterations: 277
currently lose_sum: 95.98534053564072
time_elpased: 1.436
batch start
#iterations: 278
currently lose_sum: 95.96110767126083
time_elpased: 1.467
batch start
#iterations: 279
currently lose_sum: 95.9606945514679
time_elpased: 1.422
start validation test
0.639587628866
0.614988606633
0.749922815684
0.675785959381
0.639393918427
62.242
batch start
#iterations: 280
currently lose_sum: 95.77662533521652
time_elpased: 1.519
batch start
#iterations: 281
currently lose_sum: 95.98642802238464
time_elpased: 1.459
batch start
#iterations: 282
currently lose_sum: 95.92451667785645
time_elpased: 1.458
batch start
#iterations: 283
currently lose_sum: 96.1816958785057
time_elpased: 1.467
batch start
#iterations: 284
currently lose_sum: 95.88583743572235
time_elpased: 1.478
batch start
#iterations: 285
currently lose_sum: 96.04500007629395
time_elpased: 1.447
batch start
#iterations: 286
currently lose_sum: 95.6982552409172
time_elpased: 1.459
batch start
#iterations: 287
currently lose_sum: 96.07526379823685
time_elpased: 1.47
batch start
#iterations: 288
currently lose_sum: 95.83780312538147
time_elpased: 1.429
batch start
#iterations: 289
currently lose_sum: 95.96356046199799
time_elpased: 1.428
batch start
#iterations: 290
currently lose_sum: 95.95994311571121
time_elpased: 1.453
batch start
#iterations: 291
currently lose_sum: 95.6938379406929
time_elpased: 1.456
batch start
#iterations: 292
currently lose_sum: 95.66092360019684
time_elpased: 1.456
batch start
#iterations: 293
currently lose_sum: 95.89777737855911
time_elpased: 1.456
batch start
#iterations: 294
currently lose_sum: 95.53062355518341
time_elpased: 1.442
batch start
#iterations: 295
currently lose_sum: 95.59133142232895
time_elpased: 1.462
batch start
#iterations: 296
currently lose_sum: 95.54469001293182
time_elpased: 1.433
batch start
#iterations: 297
currently lose_sum: 95.66686648130417
time_elpased: 1.499
batch start
#iterations: 298
currently lose_sum: 95.78747999668121
time_elpased: 1.435
batch start
#iterations: 299
currently lose_sum: 95.83276522159576
time_elpased: 1.44
start validation test
0.637783505155
0.627078609221
0.68302974169
0.653859415792
0.637704068408
62.410
batch start
#iterations: 300
currently lose_sum: 96.06405657529831
time_elpased: 1.466
batch start
#iterations: 301
currently lose_sum: 96.08887016773224
time_elpased: 1.471
batch start
#iterations: 302
currently lose_sum: 96.10407394170761
time_elpased: 1.452
batch start
#iterations: 303
currently lose_sum: 95.83319145441055
time_elpased: 1.443
batch start
#iterations: 304
currently lose_sum: 95.42760694026947
time_elpased: 1.429
batch start
#iterations: 305
currently lose_sum: 95.86478871107101
time_elpased: 1.433
batch start
#iterations: 306
currently lose_sum: 96.1678535938263
time_elpased: 1.479
batch start
#iterations: 307
currently lose_sum: 95.77590072154999
time_elpased: 1.515
batch start
#iterations: 308
currently lose_sum: 95.92726302146912
time_elpased: 1.457
batch start
#iterations: 309
currently lose_sum: 95.85801130533218
time_elpased: 1.454
batch start
#iterations: 310
currently lose_sum: 95.72028142213821
time_elpased: 1.45
batch start
#iterations: 311
currently lose_sum: 95.87610822916031
time_elpased: 1.447
batch start
#iterations: 312
currently lose_sum: 96.00020897388458
time_elpased: 1.444
batch start
#iterations: 313
currently lose_sum: 95.24124783277512
time_elpased: 1.442
batch start
#iterations: 314
currently lose_sum: 95.85952085256577
time_elpased: 1.426
batch start
#iterations: 315
currently lose_sum: 95.79826092720032
time_elpased: 1.507
batch start
#iterations: 316
currently lose_sum: 95.98878121376038
time_elpased: 1.489
batch start
#iterations: 317
currently lose_sum: 95.87961745262146
time_elpased: 1.476
batch start
#iterations: 318
currently lose_sum: 95.89349377155304
time_elpased: 1.451
batch start
#iterations: 319
currently lose_sum: 95.63564908504486
time_elpased: 1.47
start validation test
0.639278350515
0.610322161811
0.774004322322
0.682486388385
0.639041818292
62.089
batch start
#iterations: 320
currently lose_sum: 96.07893806695938
time_elpased: 1.454
batch start
#iterations: 321
currently lose_sum: 95.57858544588089
time_elpased: 1.452
batch start
#iterations: 322
currently lose_sum: 95.61474579572678
time_elpased: 1.461
batch start
#iterations: 323
currently lose_sum: 95.99200743436813
time_elpased: 1.459
batch start
#iterations: 324
currently lose_sum: 96.30428266525269
time_elpased: 1.439
batch start
#iterations: 325
currently lose_sum: 96.16908186674118
time_elpased: 1.466
batch start
#iterations: 326
currently lose_sum: 95.76681369543076
time_elpased: 1.487
batch start
#iterations: 327
currently lose_sum: 95.94870179891586
time_elpased: 1.453
batch start
#iterations: 328
currently lose_sum: 95.63291698694229
time_elpased: 1.471
batch start
#iterations: 329
currently lose_sum: 95.66487139463425
time_elpased: 1.461
batch start
#iterations: 330
currently lose_sum: 95.83572471141815
time_elpased: 1.474
batch start
#iterations: 331
currently lose_sum: 96.03427362442017
time_elpased: 1.457
batch start
#iterations: 332
currently lose_sum: 95.56045216321945
time_elpased: 1.473
batch start
#iterations: 333
currently lose_sum: 95.74115705490112
time_elpased: 1.519
batch start
#iterations: 334
currently lose_sum: 96.02305901050568
time_elpased: 1.429
batch start
#iterations: 335
currently lose_sum: 95.83285403251648
time_elpased: 1.463
batch start
#iterations: 336
currently lose_sum: 95.73244762420654
time_elpased: 1.475
batch start
#iterations: 337
currently lose_sum: 95.9082663655281
time_elpased: 1.469
batch start
#iterations: 338
currently lose_sum: 95.77417695522308
time_elpased: 1.492
batch start
#iterations: 339
currently lose_sum: 95.77117049694061
time_elpased: 1.459
start validation test
0.64087628866
0.612208258528
0.772048986313
0.68290018661
0.640645994757
62.061
batch start
#iterations: 340
currently lose_sum: 95.98561918735504
time_elpased: 1.455
batch start
#iterations: 341
currently lose_sum: 95.86363959312439
time_elpased: 1.495
batch start
#iterations: 342
currently lose_sum: 95.89906758069992
time_elpased: 1.46
batch start
#iterations: 343
currently lose_sum: 96.06149464845657
time_elpased: 1.46
batch start
#iterations: 344
currently lose_sum: 95.72367018461227
time_elpased: 1.453
batch start
#iterations: 345
currently lose_sum: 95.71165025234222
time_elpased: 1.512
batch start
#iterations: 346
currently lose_sum: 95.66103261709213
time_elpased: 1.476
batch start
#iterations: 347
currently lose_sum: 95.71229469776154
time_elpased: 1.478
batch start
#iterations: 348
currently lose_sum: 95.75129717588425
time_elpased: 1.464
batch start
#iterations: 349
currently lose_sum: 96.07839787006378
time_elpased: 1.505
batch start
#iterations: 350
currently lose_sum: 95.44020420312881
time_elpased: 1.419
batch start
#iterations: 351
currently lose_sum: 95.54055851697922
time_elpased: 1.443
batch start
#iterations: 352
currently lose_sum: 95.63529688119888
time_elpased: 1.452
batch start
#iterations: 353
currently lose_sum: 95.84714835882187
time_elpased: 1.524
batch start
#iterations: 354
currently lose_sum: 95.92835086584091
time_elpased: 1.435
batch start
#iterations: 355
currently lose_sum: 95.92348152399063
time_elpased: 1.466
batch start
#iterations: 356
currently lose_sum: 95.79114735126495
time_elpased: 1.458
batch start
#iterations: 357
currently lose_sum: 95.473279774189
time_elpased: 1.5
batch start
#iterations: 358
currently lose_sum: 95.92916589975357
time_elpased: 1.49
batch start
#iterations: 359
currently lose_sum: 95.93412971496582
time_elpased: 1.48
start validation test
0.638092783505
0.614840688363
0.742718946177
0.672756933116
0.637909096139
62.322
batch start
#iterations: 360
currently lose_sum: 95.8992070555687
time_elpased: 1.491
batch start
#iterations: 361
currently lose_sum: 96.00487476587296
time_elpased: 1.453
batch start
#iterations: 362
currently lose_sum: 95.69559448957443
time_elpased: 1.456
batch start
#iterations: 363
currently lose_sum: 95.66498625278473
time_elpased: 1.477
batch start
#iterations: 364
currently lose_sum: 95.91589510440826
time_elpased: 1.488
batch start
#iterations: 365
currently lose_sum: 96.17077386379242
time_elpased: 1.441
batch start
#iterations: 366
currently lose_sum: 96.01091992855072
time_elpased: 1.44
batch start
#iterations: 367
currently lose_sum: 96.117136657238
time_elpased: 1.477
batch start
#iterations: 368
currently lose_sum: 95.74412500858307
time_elpased: 1.433
batch start
#iterations: 369
currently lose_sum: 95.7352706193924
time_elpased: 1.483
batch start
#iterations: 370
currently lose_sum: 96.00466692447662
time_elpased: 1.475
batch start
#iterations: 371
currently lose_sum: 95.4128480553627
time_elpased: 1.455
batch start
#iterations: 372
currently lose_sum: 95.76296669244766
time_elpased: 1.515
batch start
#iterations: 373
currently lose_sum: 95.60156762599945
time_elpased: 1.494
batch start
#iterations: 374
currently lose_sum: 95.43849855661392
time_elpased: 1.45
batch start
#iterations: 375
currently lose_sum: 95.80480420589447
time_elpased: 1.49
batch start
#iterations: 376
currently lose_sum: 95.77415829896927
time_elpased: 1.486
batch start
#iterations: 377
currently lose_sum: 95.81596630811691
time_elpased: 1.493
batch start
#iterations: 378
currently lose_sum: 95.8675285577774
time_elpased: 1.492
batch start
#iterations: 379
currently lose_sum: 95.95660102367401
time_elpased: 1.477
start validation test
0.638505154639
0.613005683718
0.754759699496
0.676537060099
0.638301051855
62.108
batch start
#iterations: 380
currently lose_sum: 95.6930639743805
time_elpased: 1.51
batch start
#iterations: 381
currently lose_sum: 95.66278368234634
time_elpased: 1.454
batch start
#iterations: 382
currently lose_sum: 95.78499048948288
time_elpased: 1.447
batch start
#iterations: 383
currently lose_sum: 95.5272775888443
time_elpased: 1.445
batch start
#iterations: 384
currently lose_sum: 96.07366865873337
time_elpased: 1.449
batch start
#iterations: 385
currently lose_sum: 95.68182694911957
time_elpased: 1.488
batch start
#iterations: 386
currently lose_sum: 95.87784379720688
time_elpased: 1.458
batch start
#iterations: 387
currently lose_sum: 95.97210901975632
time_elpased: 1.49
batch start
#iterations: 388
currently lose_sum: 95.81220692396164
time_elpased: 1.463
batch start
#iterations: 389
currently lose_sum: 96.06052380800247
time_elpased: 1.466
batch start
#iterations: 390
currently lose_sum: 95.9102737903595
time_elpased: 1.469
batch start
#iterations: 391
currently lose_sum: 95.90779131650925
time_elpased: 1.456
batch start
#iterations: 392
currently lose_sum: 95.95210635662079
time_elpased: 1.453
batch start
#iterations: 393
currently lose_sum: 95.86138015985489
time_elpased: 1.458
batch start
#iterations: 394
currently lose_sum: 95.53158628940582
time_elpased: 1.427
batch start
#iterations: 395
currently lose_sum: 95.54896777868271
time_elpased: 1.458
batch start
#iterations: 396
currently lose_sum: 95.74678987264633
time_elpased: 1.436
batch start
#iterations: 397
currently lose_sum: 95.47637838125229
time_elpased: 1.488
batch start
#iterations: 398
currently lose_sum: 95.5423059463501
time_elpased: 1.472
batch start
#iterations: 399
currently lose_sum: 95.78036832809448
time_elpased: 1.416
start validation test
0.637422680412
0.615338320007
0.736544200885
0.670507775904
0.637248657295
62.177
acc: 0.644
pre: 0.616
rec: 0.768
F1: 0.683
auc: 0.683
