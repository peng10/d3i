start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.96837395429611
time_elpased: 1.45
batch start
#iterations: 1
currently lose_sum: 100.35412472486496
time_elpased: 1.393
batch start
#iterations: 2
currently lose_sum: 100.1830302476883
time_elpased: 1.418
batch start
#iterations: 3
currently lose_sum: 100.07810145616531
time_elpased: 1.406
batch start
#iterations: 4
currently lose_sum: 99.92616099119186
time_elpased: 1.417
batch start
#iterations: 5
currently lose_sum: 99.77639734745026
time_elpased: 1.437
batch start
#iterations: 6
currently lose_sum: 99.61568456888199
time_elpased: 1.409
batch start
#iterations: 7
currently lose_sum: 99.43260759115219
time_elpased: 1.422
batch start
#iterations: 8
currently lose_sum: 99.40000450611115
time_elpased: 1.381
batch start
#iterations: 9
currently lose_sum: 99.35487300157547
time_elpased: 1.475
batch start
#iterations: 10
currently lose_sum: 99.2999712228775
time_elpased: 1.401
batch start
#iterations: 11
currently lose_sum: 99.0711116194725
time_elpased: 1.43
batch start
#iterations: 12
currently lose_sum: 98.87756425142288
time_elpased: 1.423
batch start
#iterations: 13
currently lose_sum: 98.95869445800781
time_elpased: 1.418
batch start
#iterations: 14
currently lose_sum: 98.89510542154312
time_elpased: 1.395
batch start
#iterations: 15
currently lose_sum: 98.55815941095352
time_elpased: 1.428
batch start
#iterations: 16
currently lose_sum: 98.42041748762131
time_elpased: 1.416
batch start
#iterations: 17
currently lose_sum: 98.59465074539185
time_elpased: 1.4
batch start
#iterations: 18
currently lose_sum: 98.39391630887985
time_elpased: 1.414
batch start
#iterations: 19
currently lose_sum: 98.37593597173691
time_elpased: 1.487
start validation test
0.632680412371
0.640494523371
0.607800761552
0.623719505756
0.632724092436
63.701
batch start
#iterations: 20
currently lose_sum: 98.3172858953476
time_elpased: 1.436
batch start
#iterations: 21
currently lose_sum: 98.10005497932434
time_elpased: 1.426
batch start
#iterations: 22
currently lose_sum: 98.15421891212463
time_elpased: 1.422
batch start
#iterations: 23
currently lose_sum: 98.10810869932175
time_elpased: 1.439
batch start
#iterations: 24
currently lose_sum: 98.18768554925919
time_elpased: 1.416
batch start
#iterations: 25
currently lose_sum: 98.04952734708786
time_elpased: 1.437
batch start
#iterations: 26
currently lose_sum: 98.01430028676987
time_elpased: 1.4
batch start
#iterations: 27
currently lose_sum: 97.8663022518158
time_elpased: 1.451
batch start
#iterations: 28
currently lose_sum: 97.6527469754219
time_elpased: 1.386
batch start
#iterations: 29
currently lose_sum: 97.95866346359253
time_elpased: 1.451
batch start
#iterations: 30
currently lose_sum: 97.9163983464241
time_elpased: 1.424
batch start
#iterations: 31
currently lose_sum: 97.45252460241318
time_elpased: 1.42
batch start
#iterations: 32
currently lose_sum: 97.65715086460114
time_elpased: 1.415
batch start
#iterations: 33
currently lose_sum: 97.68233448266983
time_elpased: 1.42
batch start
#iterations: 34
currently lose_sum: 97.67888510227203
time_elpased: 1.458
batch start
#iterations: 35
currently lose_sum: 97.53572225570679
time_elpased: 1.424
batch start
#iterations: 36
currently lose_sum: 97.58565509319305
time_elpased: 1.461
batch start
#iterations: 37
currently lose_sum: 97.4311443567276
time_elpased: 1.446
batch start
#iterations: 38
currently lose_sum: 97.5059329867363
time_elpased: 1.394
batch start
#iterations: 39
currently lose_sum: 97.1488031744957
time_elpased: 1.404
start validation test
0.63
0.635760881189
0.611814345992
0.623557793161
0.630031927721
62.847
batch start
#iterations: 40
currently lose_sum: 97.3664111495018
time_elpased: 1.419
batch start
#iterations: 41
currently lose_sum: 97.39277195930481
time_elpased: 1.418
batch start
#iterations: 42
currently lose_sum: 97.30897068977356
time_elpased: 1.413
batch start
#iterations: 43
currently lose_sum: 97.26544523239136
time_elpased: 1.504
batch start
#iterations: 44
currently lose_sum: 97.0713357925415
time_elpased: 1.43
batch start
#iterations: 45
currently lose_sum: 97.31140691041946
time_elpased: 1.392
batch start
#iterations: 46
currently lose_sum: 96.8745601773262
time_elpased: 1.407
batch start
#iterations: 47
currently lose_sum: 97.22728967666626
time_elpased: 1.394
batch start
#iterations: 48
currently lose_sum: 97.37539684772491
time_elpased: 1.403
batch start
#iterations: 49
currently lose_sum: 97.28426951169968
time_elpased: 1.427
batch start
#iterations: 50
currently lose_sum: 97.30994588136673
time_elpased: 1.402
batch start
#iterations: 51
currently lose_sum: 97.2171094417572
time_elpased: 1.404
batch start
#iterations: 52
currently lose_sum: 97.28270846605301
time_elpased: 1.413
batch start
#iterations: 53
currently lose_sum: 97.18318909406662
time_elpased: 1.432
batch start
#iterations: 54
currently lose_sum: 97.25252312421799
time_elpased: 1.419
batch start
#iterations: 55
currently lose_sum: 97.27285635471344
time_elpased: 1.41
batch start
#iterations: 56
currently lose_sum: 96.76700979471207
time_elpased: 1.421
batch start
#iterations: 57
currently lose_sum: 96.93471473455429
time_elpased: 1.388
batch start
#iterations: 58
currently lose_sum: 97.15186989307404
time_elpased: 1.427
batch start
#iterations: 59
currently lose_sum: 97.11402207612991
time_elpased: 1.408
start validation test
0.637783505155
0.633326724822
0.657507461151
0.645190608432
0.637748876708
62.439
batch start
#iterations: 60
currently lose_sum: 97.031076669693
time_elpased: 1.442
batch start
#iterations: 61
currently lose_sum: 96.80770146846771
time_elpased: 1.414
batch start
#iterations: 62
currently lose_sum: 96.95287674665451
time_elpased: 1.429
batch start
#iterations: 63
currently lose_sum: 97.19671058654785
time_elpased: 1.441
batch start
#iterations: 64
currently lose_sum: 96.83593022823334
time_elpased: 1.396
batch start
#iterations: 65
currently lose_sum: 96.81360393762589
time_elpased: 1.399
batch start
#iterations: 66
currently lose_sum: 96.73268729448318
time_elpased: 1.392
batch start
#iterations: 67
currently lose_sum: 97.13870215415955
time_elpased: 1.403
batch start
#iterations: 68
currently lose_sum: 97.08275610208511
time_elpased: 1.414
batch start
#iterations: 69
currently lose_sum: 96.98480784893036
time_elpased: 1.408
batch start
#iterations: 70
currently lose_sum: 96.78388583660126
time_elpased: 1.409
batch start
#iterations: 71
currently lose_sum: 96.7939921617508
time_elpased: 1.389
batch start
#iterations: 72
currently lose_sum: 96.84721493721008
time_elpased: 1.401
batch start
#iterations: 73
currently lose_sum: 96.732990026474
time_elpased: 1.379
batch start
#iterations: 74
currently lose_sum: 96.8274610042572
time_elpased: 1.449
batch start
#iterations: 75
currently lose_sum: 96.66792738437653
time_elpased: 1.41
batch start
#iterations: 76
currently lose_sum: 96.31636273860931
time_elpased: 1.403
batch start
#iterations: 77
currently lose_sum: 96.81341856718063
time_elpased: 1.419
batch start
#iterations: 78
currently lose_sum: 96.67879021167755
time_elpased: 1.431
batch start
#iterations: 79
currently lose_sum: 96.77634114027023
time_elpased: 1.406
start validation test
0.636597938144
0.619307506487
0.712359781826
0.662582559586
0.636464926542
62.357
batch start
#iterations: 80
currently lose_sum: 96.86655080318451
time_elpased: 1.408
batch start
#iterations: 81
currently lose_sum: 96.95096623897552
time_elpased: 1.412
batch start
#iterations: 82
currently lose_sum: 96.79783463478088
time_elpased: 1.451
batch start
#iterations: 83
currently lose_sum: 96.85919934511185
time_elpased: 1.406
batch start
#iterations: 84
currently lose_sum: 96.64346444606781
time_elpased: 1.476
batch start
#iterations: 85
currently lose_sum: 96.8255313038826
time_elpased: 1.414
batch start
#iterations: 86
currently lose_sum: 96.78694188594818
time_elpased: 1.432
batch start
#iterations: 87
currently lose_sum: 96.56407994031906
time_elpased: 1.397
batch start
#iterations: 88
currently lose_sum: 96.71596932411194
time_elpased: 1.427
batch start
#iterations: 89
currently lose_sum: 96.26659488677979
time_elpased: 1.399
batch start
#iterations: 90
currently lose_sum: 96.74615889787674
time_elpased: 1.443
batch start
#iterations: 91
currently lose_sum: 96.76275283098221
time_elpased: 1.393
batch start
#iterations: 92
currently lose_sum: 96.61580687761307
time_elpased: 1.421
batch start
#iterations: 93
currently lose_sum: 96.86406189203262
time_elpased: 1.434
batch start
#iterations: 94
currently lose_sum: 96.5991336107254
time_elpased: 1.486
batch start
#iterations: 95
currently lose_sum: 96.5095636844635
time_elpased: 1.388
batch start
#iterations: 96
currently lose_sum: 96.73221516609192
time_elpased: 1.416
batch start
#iterations: 97
currently lose_sum: 96.56611639261246
time_elpased: 1.402
batch start
#iterations: 98
currently lose_sum: 96.53080344200134
time_elpased: 1.455
batch start
#iterations: 99
currently lose_sum: 96.25062131881714
time_elpased: 1.406
start validation test
0.641134020619
0.633414043584
0.673047236801
0.652629478096
0.641077992045
62.237
batch start
#iterations: 100
currently lose_sum: 96.61726528406143
time_elpased: 1.414
batch start
#iterations: 101
currently lose_sum: 96.50464868545532
time_elpased: 1.402
batch start
#iterations: 102
currently lose_sum: 96.18878096342087
time_elpased: 1.399
batch start
#iterations: 103
currently lose_sum: 96.46352189779282
time_elpased: 1.407
batch start
#iterations: 104
currently lose_sum: 96.76706802845001
time_elpased: 1.433
batch start
#iterations: 105
currently lose_sum: 96.61978948116302
time_elpased: 1.424
batch start
#iterations: 106
currently lose_sum: 96.44085884094238
time_elpased: 1.411
batch start
#iterations: 107
currently lose_sum: 96.43980079889297
time_elpased: 1.401
batch start
#iterations: 108
currently lose_sum: 96.50823938846588
time_elpased: 1.4
batch start
#iterations: 109
currently lose_sum: 96.52026909589767
time_elpased: 1.405
batch start
#iterations: 110
currently lose_sum: 96.57104671001434
time_elpased: 1.397
batch start
#iterations: 111
currently lose_sum: 96.54055094718933
time_elpased: 1.396
batch start
#iterations: 112
currently lose_sum: 96.27253431081772
time_elpased: 1.401
batch start
#iterations: 113
currently lose_sum: 96.49654775857925
time_elpased: 1.439
batch start
#iterations: 114
currently lose_sum: 96.2403074502945
time_elpased: 1.465
batch start
#iterations: 115
currently lose_sum: 96.40358781814575
time_elpased: 1.424
batch start
#iterations: 116
currently lose_sum: 96.74992233514786
time_elpased: 1.442
batch start
#iterations: 117
currently lose_sum: 96.49245822429657
time_elpased: 1.399
batch start
#iterations: 118
currently lose_sum: 96.51130110025406
time_elpased: 1.41
batch start
#iterations: 119
currently lose_sum: 96.37263822555542
time_elpased: 1.407
start validation test
0.638969072165
0.632406051733
0.666769579088
0.649133353371
0.638920264087
62.290
batch start
#iterations: 120
currently lose_sum: 96.60305255651474
time_elpased: 1.384
batch start
#iterations: 121
currently lose_sum: 96.65129148960114
time_elpased: 1.42
batch start
#iterations: 122
currently lose_sum: 96.5536817908287
time_elpased: 1.449
batch start
#iterations: 123
currently lose_sum: 96.05769044160843
time_elpased: 1.434
batch start
#iterations: 124
currently lose_sum: 96.29938000440598
time_elpased: 1.402
batch start
#iterations: 125
currently lose_sum: 96.42767691612244
time_elpased: 1.434
batch start
#iterations: 126
currently lose_sum: 96.62699431180954
time_elpased: 1.438
batch start
#iterations: 127
currently lose_sum: 96.43033683300018
time_elpased: 1.427
batch start
#iterations: 128
currently lose_sum: 96.71044600009918
time_elpased: 1.438
batch start
#iterations: 129
currently lose_sum: 96.2547836303711
time_elpased: 1.394
batch start
#iterations: 130
currently lose_sum: 96.05542153120041
time_elpased: 1.413
batch start
#iterations: 131
currently lose_sum: 96.40675377845764
time_elpased: 1.431
batch start
#iterations: 132
currently lose_sum: 96.34452301263809
time_elpased: 1.415
batch start
#iterations: 133
currently lose_sum: 96.0189957022667
time_elpased: 1.4
batch start
#iterations: 134
currently lose_sum: 96.59254437685013
time_elpased: 1.428
batch start
#iterations: 135
currently lose_sum: 96.68103730678558
time_elpased: 1.431
batch start
#iterations: 136
currently lose_sum: 96.00201201438904
time_elpased: 1.41
batch start
#iterations: 137
currently lose_sum: 96.26563364267349
time_elpased: 1.473
batch start
#iterations: 138
currently lose_sum: 96.02027899026871
time_elpased: 1.431
batch start
#iterations: 139
currently lose_sum: 96.47643899917603
time_elpased: 1.41
start validation test
0.641082474227
0.627950195131
0.695482144695
0.659993163729
0.640986967215
62.224
batch start
#iterations: 140
currently lose_sum: 96.59112393856049
time_elpased: 1.431
batch start
#iterations: 141
currently lose_sum: 96.77024751901627
time_elpased: 1.436
batch start
#iterations: 142
currently lose_sum: 96.33771193027496
time_elpased: 1.426
batch start
#iterations: 143
currently lose_sum: 96.4377007484436
time_elpased: 1.441
batch start
#iterations: 144
currently lose_sum: 96.25285267829895
time_elpased: 1.414
batch start
#iterations: 145
currently lose_sum: 96.50060367584229
time_elpased: 1.387
batch start
#iterations: 146
currently lose_sum: 96.05702024698257
time_elpased: 1.42
batch start
#iterations: 147
currently lose_sum: 95.94335877895355
time_elpased: 1.405
batch start
#iterations: 148
currently lose_sum: 96.34422677755356
time_elpased: 1.441
batch start
#iterations: 149
currently lose_sum: 96.48509079217911
time_elpased: 1.427
batch start
#iterations: 150
currently lose_sum: 96.32733005285263
time_elpased: 1.401
batch start
#iterations: 151
currently lose_sum: 96.44216072559357
time_elpased: 1.441
batch start
#iterations: 152
currently lose_sum: 96.48730707168579
time_elpased: 1.449
batch start
#iterations: 153
currently lose_sum: 96.16234171390533
time_elpased: 1.441
batch start
#iterations: 154
currently lose_sum: 96.52899652719498
time_elpased: 1.405
batch start
#iterations: 155
currently lose_sum: 96.378309071064
time_elpased: 1.417
batch start
#iterations: 156
currently lose_sum: 96.32167708873749
time_elpased: 1.399
batch start
#iterations: 157
currently lose_sum: 96.68270242214203
time_elpased: 1.417
batch start
#iterations: 158
currently lose_sum: 96.63973593711853
time_elpased: 1.426
batch start
#iterations: 159
currently lose_sum: 96.74438315629959
time_elpased: 1.415
start validation test
0.638453608247
0.632201897682
0.665122980344
0.648244734203
0.638406786051
62.380
batch start
#iterations: 160
currently lose_sum: 96.14385938644409
time_elpased: 1.398
batch start
#iterations: 161
currently lose_sum: 96.16146403551102
time_elpased: 1.446
batch start
#iterations: 162
currently lose_sum: 96.08853840827942
time_elpased: 1.412
batch start
#iterations: 163
currently lose_sum: 96.2805927991867
time_elpased: 1.439
batch start
#iterations: 164
currently lose_sum: 96.28651040792465
time_elpased: 1.423
batch start
#iterations: 165
currently lose_sum: 96.22083848714828
time_elpased: 1.476
batch start
#iterations: 166
currently lose_sum: 96.18656647205353
time_elpased: 1.409
batch start
#iterations: 167
currently lose_sum: 96.41039496660233
time_elpased: 1.433
batch start
#iterations: 168
currently lose_sum: 95.96146023273468
time_elpased: 1.428
batch start
#iterations: 169
currently lose_sum: 96.39175951480865
time_elpased: 1.413
batch start
#iterations: 170
currently lose_sum: 96.0890953540802
time_elpased: 1.436
batch start
#iterations: 171
currently lose_sum: 96.23058915138245
time_elpased: 1.438
batch start
#iterations: 172
currently lose_sum: 96.60294765233994
time_elpased: 1.442
batch start
#iterations: 173
currently lose_sum: 96.54534012079239
time_elpased: 1.487
batch start
#iterations: 174
currently lose_sum: 95.88400655984879
time_elpased: 1.441
batch start
#iterations: 175
currently lose_sum: 96.11488032341003
time_elpased: 1.444
batch start
#iterations: 176
currently lose_sum: 96.33015143871307
time_elpased: 1.403
batch start
#iterations: 177
currently lose_sum: 95.97195035219193
time_elpased: 1.448
batch start
#iterations: 178
currently lose_sum: 95.63217967748642
time_elpased: 1.415
batch start
#iterations: 179
currently lose_sum: 96.63774716854095
time_elpased: 1.428
start validation test
0.638711340206
0.62969348659
0.676546259133
0.6522796051
0.63864491517
62.289
batch start
#iterations: 180
currently lose_sum: 96.30102515220642
time_elpased: 1.426
batch start
#iterations: 181
currently lose_sum: 96.47542124986649
time_elpased: 1.449
batch start
#iterations: 182
currently lose_sum: 96.31185334920883
time_elpased: 1.44
batch start
#iterations: 183
currently lose_sum: 95.9757234454155
time_elpased: 1.406
batch start
#iterations: 184
currently lose_sum: 96.21136504411697
time_elpased: 1.515
batch start
#iterations: 185
currently lose_sum: 96.10151660442352
time_elpased: 1.427
batch start
#iterations: 186
currently lose_sum: 95.93099266290665
time_elpased: 1.465
batch start
#iterations: 187
currently lose_sum: 95.84302181005478
time_elpased: 1.432
batch start
#iterations: 188
currently lose_sum: 96.28840655088425
time_elpased: 1.394
batch start
#iterations: 189
currently lose_sum: 96.29705482721329
time_elpased: 1.416
batch start
#iterations: 190
currently lose_sum: 96.32580071687698
time_elpased: 1.413
batch start
#iterations: 191
currently lose_sum: 96.41843277215958
time_elpased: 1.433
batch start
#iterations: 192
currently lose_sum: 95.99617892503738
time_elpased: 1.4
batch start
#iterations: 193
currently lose_sum: 96.29394191503525
time_elpased: 1.423
batch start
#iterations: 194
currently lose_sum: 96.13691592216492
time_elpased: 1.445
batch start
#iterations: 195
currently lose_sum: 96.27172136306763
time_elpased: 1.423
batch start
#iterations: 196
currently lose_sum: 96.48680597543716
time_elpased: 1.444
batch start
#iterations: 197
currently lose_sum: 96.15749049186707
time_elpased: 1.407
batch start
#iterations: 198
currently lose_sum: 96.13252341747284
time_elpased: 1.451
batch start
#iterations: 199
currently lose_sum: 96.05964893102646
time_elpased: 1.435
start validation test
0.636391752577
0.630373053951
0.662550169805
0.64606121425
0.636345827441
62.317
batch start
#iterations: 200
currently lose_sum: 96.0808197259903
time_elpased: 1.415
batch start
#iterations: 201
currently lose_sum: 95.95095747709274
time_elpased: 1.417
batch start
#iterations: 202
currently lose_sum: 96.01366513967514
time_elpased: 1.403
batch start
#iterations: 203
currently lose_sum: 96.484001994133
time_elpased: 1.427
batch start
#iterations: 204
currently lose_sum: 95.95161932706833
time_elpased: 1.407
batch start
#iterations: 205
currently lose_sum: 96.17984348535538
time_elpased: 1.422
batch start
#iterations: 206
currently lose_sum: 96.3200877904892
time_elpased: 1.433
batch start
#iterations: 207
currently lose_sum: 96.22270530462265
time_elpased: 1.44
batch start
#iterations: 208
currently lose_sum: 95.84828197956085
time_elpased: 1.419
batch start
#iterations: 209
currently lose_sum: 96.17517280578613
time_elpased: 1.405
batch start
#iterations: 210
currently lose_sum: 96.07907968759537
time_elpased: 1.419
batch start
#iterations: 211
currently lose_sum: 95.8393674492836
time_elpased: 1.407
batch start
#iterations: 212
currently lose_sum: 95.94333177804947
time_elpased: 1.433
batch start
#iterations: 213
currently lose_sum: 96.10194230079651
time_elpased: 1.462
batch start
#iterations: 214
currently lose_sum: 96.02137583494186
time_elpased: 1.431
batch start
#iterations: 215
currently lose_sum: 96.50984954833984
time_elpased: 1.456
batch start
#iterations: 216
currently lose_sum: 96.04184019565582
time_elpased: 1.43
batch start
#iterations: 217
currently lose_sum: 96.15452706813812
time_elpased: 1.428
batch start
#iterations: 218
currently lose_sum: 96.33310955762863
time_elpased: 1.425
batch start
#iterations: 219
currently lose_sum: 96.3628460764885
time_elpased: 1.416
start validation test
0.644587628866
0.628459577567
0.710404445817
0.666924303174
0.644472077292
62.116
batch start
#iterations: 220
currently lose_sum: 96.38724261522293
time_elpased: 1.427
batch start
#iterations: 221
currently lose_sum: 95.78060191869736
time_elpased: 1.435
batch start
#iterations: 222
currently lose_sum: 96.33756905794144
time_elpased: 1.417
batch start
#iterations: 223
currently lose_sum: 96.08400362730026
time_elpased: 1.432
batch start
#iterations: 224
currently lose_sum: 95.95777529478073
time_elpased: 1.407
batch start
#iterations: 225
currently lose_sum: 96.44523280858994
time_elpased: 1.405
batch start
#iterations: 226
currently lose_sum: 95.92356133460999
time_elpased: 1.422
batch start
#iterations: 227
currently lose_sum: 96.11493504047394
time_elpased: 1.448
batch start
#iterations: 228
currently lose_sum: 95.83428829908371
time_elpased: 1.427
batch start
#iterations: 229
currently lose_sum: 96.01554924249649
time_elpased: 1.414
batch start
#iterations: 230
currently lose_sum: 96.10308164358139
time_elpased: 1.506
batch start
#iterations: 231
currently lose_sum: 96.2209740281105
time_elpased: 1.416
batch start
#iterations: 232
currently lose_sum: 95.80514341592789
time_elpased: 1.435
batch start
#iterations: 233
currently lose_sum: 96.08666509389877
time_elpased: 1.416
batch start
#iterations: 234
currently lose_sum: 96.03359425067902
time_elpased: 1.464
batch start
#iterations: 235
currently lose_sum: 96.3740354180336
time_elpased: 1.484
batch start
#iterations: 236
currently lose_sum: 96.06357246637344
time_elpased: 1.434
batch start
#iterations: 237
currently lose_sum: 96.08944344520569
time_elpased: 1.44
batch start
#iterations: 238
currently lose_sum: 96.38783997297287
time_elpased: 1.451
batch start
#iterations: 239
currently lose_sum: 96.29213613271713
time_elpased: 1.418
start validation test
0.635206185567
0.628755364807
0.663373469178
0.645600681056
0.635156733556
62.270
batch start
#iterations: 240
currently lose_sum: 95.82905507087708
time_elpased: 1.413
batch start
#iterations: 241
currently lose_sum: 95.92034059762955
time_elpased: 1.434
batch start
#iterations: 242
currently lose_sum: 96.24518710374832
time_elpased: 1.486
batch start
#iterations: 243
currently lose_sum: 96.24536895751953
time_elpased: 1.409
batch start
#iterations: 244
currently lose_sum: 96.22143930196762
time_elpased: 1.423
batch start
#iterations: 245
currently lose_sum: 96.4689467549324
time_elpased: 1.397
batch start
#iterations: 246
currently lose_sum: 95.8141405582428
time_elpased: 1.436
batch start
#iterations: 247
currently lose_sum: 96.15066504478455
time_elpased: 1.41
batch start
#iterations: 248
currently lose_sum: 95.93238943815231
time_elpased: 1.42
batch start
#iterations: 249
currently lose_sum: 95.93416649103165
time_elpased: 1.433
batch start
#iterations: 250
currently lose_sum: 96.16184663772583
time_elpased: 1.422
batch start
#iterations: 251
currently lose_sum: 96.08019375801086
time_elpased: 1.455
batch start
#iterations: 252
currently lose_sum: 96.14322650432587
time_elpased: 1.443
batch start
#iterations: 253
currently lose_sum: 95.950554728508
time_elpased: 1.463
batch start
#iterations: 254
currently lose_sum: 96.0478732585907
time_elpased: 1.444
batch start
#iterations: 255
currently lose_sum: 96.04522168636322
time_elpased: 1.411
batch start
#iterations: 256
currently lose_sum: 95.99726814031601
time_elpased: 1.427
batch start
#iterations: 257
currently lose_sum: 95.71339499950409
time_elpased: 1.458
batch start
#iterations: 258
currently lose_sum: 95.96081387996674
time_elpased: 1.457
batch start
#iterations: 259
currently lose_sum: 95.61923503875732
time_elpased: 1.446
start validation test
0.640051546392
0.627661561449
0.691674385098
0.65811505508
0.639960914536
61.913
batch start
#iterations: 260
currently lose_sum: 95.98488003015518
time_elpased: 1.438
batch start
#iterations: 261
currently lose_sum: 95.79204988479614
time_elpased: 1.396
batch start
#iterations: 262
currently lose_sum: 95.98784190416336
time_elpased: 1.448
batch start
#iterations: 263
currently lose_sum: 95.84436064958572
time_elpased: 1.461
batch start
#iterations: 264
currently lose_sum: 95.86178529262543
time_elpased: 1.526
batch start
#iterations: 265
currently lose_sum: 96.08867746591568
time_elpased: 1.475
batch start
#iterations: 266
currently lose_sum: 96.35112023353577
time_elpased: 1.423
batch start
#iterations: 267
currently lose_sum: 96.11567687988281
time_elpased: 1.458
batch start
#iterations: 268
currently lose_sum: 95.79039692878723
time_elpased: 1.42
batch start
#iterations: 269
currently lose_sum: 96.11123824119568
time_elpased: 1.43
batch start
#iterations: 270
currently lose_sum: 95.97784960269928
time_elpased: 1.429
batch start
#iterations: 271
currently lose_sum: 95.85959374904633
time_elpased: 1.41
batch start
#iterations: 272
currently lose_sum: 96.1965280175209
time_elpased: 1.419
batch start
#iterations: 273
currently lose_sum: 95.67121207714081
time_elpased: 1.412
batch start
#iterations: 274
currently lose_sum: 96.013671875
time_elpased: 1.43
batch start
#iterations: 275
currently lose_sum: 96.07405596971512
time_elpased: 1.415
batch start
#iterations: 276
currently lose_sum: 95.75970536470413
time_elpased: 1.419
batch start
#iterations: 277
currently lose_sum: 96.02661257982254
time_elpased: 1.423
batch start
#iterations: 278
currently lose_sum: 96.0534417629242
time_elpased: 1.392
batch start
#iterations: 279
currently lose_sum: 96.19567692279816
time_elpased: 1.417
start validation test
0.64324742268
0.632161089053
0.688175362766
0.658980044346
0.643168544752
62.159
batch start
#iterations: 280
currently lose_sum: 95.69300824403763
time_elpased: 1.413
batch start
#iterations: 281
currently lose_sum: 95.96990913152695
time_elpased: 1.4
batch start
#iterations: 282
currently lose_sum: 95.78185945749283
time_elpased: 1.462
batch start
#iterations: 283
currently lose_sum: 96.11145228147507
time_elpased: 1.418
batch start
#iterations: 284
currently lose_sum: 95.85691076517105
time_elpased: 1.427
batch start
#iterations: 285
currently lose_sum: 95.89041352272034
time_elpased: 1.416
batch start
#iterations: 286
currently lose_sum: 95.9663393497467
time_elpased: 1.437
batch start
#iterations: 287
currently lose_sum: 95.82413691282272
time_elpased: 1.454
batch start
#iterations: 288
currently lose_sum: 95.68548601865768
time_elpased: 1.413
batch start
#iterations: 289
currently lose_sum: 96.09850907325745
time_elpased: 1.416
batch start
#iterations: 290
currently lose_sum: 96.23581618070602
time_elpased: 1.404
batch start
#iterations: 291
currently lose_sum: 95.61175954341888
time_elpased: 1.473
batch start
#iterations: 292
currently lose_sum: 95.97977185249329
time_elpased: 1.439
batch start
#iterations: 293
currently lose_sum: 96.17670291662216
time_elpased: 1.45
batch start
#iterations: 294
currently lose_sum: 95.72376608848572
time_elpased: 1.408
batch start
#iterations: 295
currently lose_sum: 95.95318233966827
time_elpased: 1.452
batch start
#iterations: 296
currently lose_sum: 95.90805047750473
time_elpased: 1.498
batch start
#iterations: 297
currently lose_sum: 95.84739398956299
time_elpased: 1.409
batch start
#iterations: 298
currently lose_sum: 96.10777288675308
time_elpased: 1.428
batch start
#iterations: 299
currently lose_sum: 95.82485139369965
time_elpased: 1.427
start validation test
0.637989690722
0.62867787543
0.677266646084
0.6520683676
0.637920733968
62.047
batch start
#iterations: 300
currently lose_sum: 95.7187734246254
time_elpased: 1.43
batch start
#iterations: 301
currently lose_sum: 95.69906836748123
time_elpased: 1.411
batch start
#iterations: 302
currently lose_sum: 95.79152101278305
time_elpased: 1.449
batch start
#iterations: 303
currently lose_sum: 95.82744175195694
time_elpased: 1.44
batch start
#iterations: 304
currently lose_sum: 96.05190002918243
time_elpased: 1.442
batch start
#iterations: 305
currently lose_sum: 96.299400806427
time_elpased: 1.421
batch start
#iterations: 306
currently lose_sum: 95.99436366558075
time_elpased: 1.418
batch start
#iterations: 307
currently lose_sum: 95.83676558732986
time_elpased: 1.436
batch start
#iterations: 308
currently lose_sum: 95.78523397445679
time_elpased: 1.416
batch start
#iterations: 309
currently lose_sum: 96.05423986911774
time_elpased: 1.405
batch start
#iterations: 310
currently lose_sum: 96.01930862665176
time_elpased: 1.433
batch start
#iterations: 311
currently lose_sum: 95.80404609441757
time_elpased: 1.43
batch start
#iterations: 312
currently lose_sum: 95.61697655916214
time_elpased: 1.43
batch start
#iterations: 313
currently lose_sum: 95.88211840391159
time_elpased: 1.452
batch start
#iterations: 314
currently lose_sum: 96.02602326869965
time_elpased: 1.409
batch start
#iterations: 315
currently lose_sum: 95.77671474218369
time_elpased: 1.478
batch start
#iterations: 316
currently lose_sum: 95.90788686275482
time_elpased: 1.422
batch start
#iterations: 317
currently lose_sum: 95.90796065330505
time_elpased: 1.408
batch start
#iterations: 318
currently lose_sum: 95.7484472990036
time_elpased: 1.443
batch start
#iterations: 319
currently lose_sum: 96.11590880155563
time_elpased: 1.462
start validation test
0.639072164948
0.630265809423
0.675928784604
0.652299135962
0.639007457468
62.030
batch start
#iterations: 320
currently lose_sum: 96.38736885786057
time_elpased: 1.417
batch start
#iterations: 321
currently lose_sum: 96.28963869810104
time_elpased: 1.406
batch start
#iterations: 322
currently lose_sum: 95.89512383937836
time_elpased: 1.482
batch start
#iterations: 323
currently lose_sum: 95.81312078237534
time_elpased: 1.402
batch start
#iterations: 324
currently lose_sum: 96.24159687757492
time_elpased: 1.429
batch start
#iterations: 325
currently lose_sum: 96.26286554336548
time_elpased: 1.438
batch start
#iterations: 326
currently lose_sum: 96.20917880535126
time_elpased: 1.432
batch start
#iterations: 327
currently lose_sum: 95.99922066926956
time_elpased: 1.398
batch start
#iterations: 328
currently lose_sum: 95.81949180364609
time_elpased: 1.445
batch start
#iterations: 329
currently lose_sum: 95.66462415456772
time_elpased: 1.417
batch start
#iterations: 330
currently lose_sum: 95.81297731399536
time_elpased: 1.436
batch start
#iterations: 331
currently lose_sum: 95.75266486406326
time_elpased: 1.415
batch start
#iterations: 332
currently lose_sum: 95.76708805561066
time_elpased: 1.453
batch start
#iterations: 333
currently lose_sum: 95.50241583585739
time_elpased: 1.403
batch start
#iterations: 334
currently lose_sum: 95.87413591146469
time_elpased: 1.429
batch start
#iterations: 335
currently lose_sum: 95.80072730779648
time_elpased: 1.421
batch start
#iterations: 336
currently lose_sum: 95.67869931459427
time_elpased: 1.398
batch start
#iterations: 337
currently lose_sum: 95.80377340316772
time_elpased: 1.405
batch start
#iterations: 338
currently lose_sum: 95.8391861319542
time_elpased: 1.428
batch start
#iterations: 339
currently lose_sum: 96.0292546749115
time_elpased: 1.405
start validation test
0.642371134021
0.629605447253
0.694658845323
0.660534298855
0.642279334879
62.023
batch start
#iterations: 340
currently lose_sum: 95.91341936588287
time_elpased: 1.424
batch start
#iterations: 341
currently lose_sum: 96.07800132036209
time_elpased: 1.485
batch start
#iterations: 342
currently lose_sum: 95.99105948209763
time_elpased: 1.462
batch start
#iterations: 343
currently lose_sum: 95.95858585834503
time_elpased: 1.428
batch start
#iterations: 344
currently lose_sum: 95.79575175046921
time_elpased: 1.449
batch start
#iterations: 345
currently lose_sum: 95.8424341082573
time_elpased: 1.451
batch start
#iterations: 346
currently lose_sum: 95.94857519865036
time_elpased: 1.438
batch start
#iterations: 347
currently lose_sum: 96.1590479016304
time_elpased: 1.412
batch start
#iterations: 348
currently lose_sum: 95.82950097322464
time_elpased: 1.434
batch start
#iterations: 349
currently lose_sum: 96.08564841747284
time_elpased: 1.403
batch start
#iterations: 350
currently lose_sum: 95.98676002025604
time_elpased: 1.407
batch start
#iterations: 351
currently lose_sum: 95.8739196062088
time_elpased: 1.396
batch start
#iterations: 352
currently lose_sum: 95.74723541736603
time_elpased: 1.411
batch start
#iterations: 353
currently lose_sum: 96.04235225915909
time_elpased: 1.432
batch start
#iterations: 354
currently lose_sum: 96.19880759716034
time_elpased: 1.424
batch start
#iterations: 355
currently lose_sum: 96.11095225811005
time_elpased: 1.434
batch start
#iterations: 356
currently lose_sum: 96.01310873031616
time_elpased: 1.413
batch start
#iterations: 357
currently lose_sum: 95.97967058420181
time_elpased: 1.429
batch start
#iterations: 358
currently lose_sum: 96.09808510541916
time_elpased: 1.444
batch start
#iterations: 359
currently lose_sum: 96.0778945684433
time_elpased: 1.417
start validation test
0.640463917526
0.618701298701
0.735412164248
0.672027084215
0.640297221234
61.968
batch start
#iterations: 360
currently lose_sum: 96.02165919542313
time_elpased: 1.418
batch start
#iterations: 361
currently lose_sum: 96.23817855119705
time_elpased: 1.398
batch start
#iterations: 362
currently lose_sum: 95.8781145811081
time_elpased: 1.436
batch start
#iterations: 363
currently lose_sum: 95.67366695404053
time_elpased: 1.466
batch start
#iterations: 364
currently lose_sum: 96.30425411462784
time_elpased: 1.476
batch start
#iterations: 365
currently lose_sum: 95.92551982402802
time_elpased: 1.42
batch start
#iterations: 366
currently lose_sum: 96.08900225162506
time_elpased: 1.514
batch start
#iterations: 367
currently lose_sum: 95.71312457323074
time_elpased: 1.391
batch start
#iterations: 368
currently lose_sum: 95.74957436323166
time_elpased: 1.463
batch start
#iterations: 369
currently lose_sum: 95.94697481393814
time_elpased: 1.426
batch start
#iterations: 370
currently lose_sum: 95.74874621629715
time_elpased: 1.424
batch start
#iterations: 371
currently lose_sum: 95.4571059346199
time_elpased: 1.439
batch start
#iterations: 372
currently lose_sum: 96.24098306894302
time_elpased: 1.402
batch start
#iterations: 373
currently lose_sum: 95.88831394910812
time_elpased: 1.422
batch start
#iterations: 374
currently lose_sum: 95.77378165721893
time_elpased: 1.453
batch start
#iterations: 375
currently lose_sum: 95.66329222917557
time_elpased: 1.44
batch start
#iterations: 376
currently lose_sum: 95.4823168516159
time_elpased: 1.427
batch start
#iterations: 377
currently lose_sum: 95.87971848249435
time_elpased: 1.396
batch start
#iterations: 378
currently lose_sum: 96.13400334119797
time_elpased: 1.449
batch start
#iterations: 379
currently lose_sum: 95.50392884016037
time_elpased: 1.424
start validation test
0.64118556701
0.630467714448
0.685293814963
0.656738497954
0.641108128178
61.970
batch start
#iterations: 380
currently lose_sum: 95.98946446180344
time_elpased: 1.424
batch start
#iterations: 381
currently lose_sum: 95.77844142913818
time_elpased: 1.406
batch start
#iterations: 382
currently lose_sum: 95.99200105667114
time_elpased: 1.429
batch start
#iterations: 383
currently lose_sum: 95.97213995456696
time_elpased: 1.4
batch start
#iterations: 384
currently lose_sum: 95.75384265184402
time_elpased: 1.449
batch start
#iterations: 385
currently lose_sum: 95.72769373655319
time_elpased: 1.409
batch start
#iterations: 386
currently lose_sum: 95.863884806633
time_elpased: 1.392
batch start
#iterations: 387
currently lose_sum: 95.65791046619415
time_elpased: 1.478
batch start
#iterations: 388
currently lose_sum: 96.18913394212723
time_elpased: 1.396
batch start
#iterations: 389
currently lose_sum: 95.98902767896652
time_elpased: 1.411
batch start
#iterations: 390
currently lose_sum: 95.96343755722046
time_elpased: 1.4
batch start
#iterations: 391
currently lose_sum: 95.68151015043259
time_elpased: 1.377
batch start
#iterations: 392
currently lose_sum: 95.68172591924667
time_elpased: 1.393
batch start
#iterations: 393
currently lose_sum: 95.79939591884613
time_elpased: 1.448
batch start
#iterations: 394
currently lose_sum: 95.89281064271927
time_elpased: 1.451
batch start
#iterations: 395
currently lose_sum: 96.15433478355408
time_elpased: 1.399
batch start
#iterations: 396
currently lose_sum: 95.71070677042007
time_elpased: 1.412
batch start
#iterations: 397
currently lose_sum: 95.83522528409958
time_elpased: 1.4
batch start
#iterations: 398
currently lose_sum: 95.81234103441238
time_elpased: 1.407
batch start
#iterations: 399
currently lose_sum: 95.97907936573029
time_elpased: 1.398
start validation test
0.641082474227
0.633197910621
0.673664711331
0.652804786836
0.641025271084
62.245
acc: 0.640
pre: 0.627
rec: 0.692
F1: 0.658
auc: 0.683
