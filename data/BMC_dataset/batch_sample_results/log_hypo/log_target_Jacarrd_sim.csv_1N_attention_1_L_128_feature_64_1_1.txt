start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.37438821792603
time_elpased: 1.603
batch start
#iterations: 1
currently lose_sum: 99.9672902226448
time_elpased: 1.636
batch start
#iterations: 2
currently lose_sum: 99.9512767791748
time_elpased: 1.71
batch start
#iterations: 3
currently lose_sum: 99.61491298675537
time_elpased: 1.682
batch start
#iterations: 4
currently lose_sum: 99.26437646150589
time_elpased: 1.639
batch start
#iterations: 5
currently lose_sum: 98.84340107440948
time_elpased: 1.667
batch start
#iterations: 6
currently lose_sum: 98.74094170331955
time_elpased: 1.617
batch start
#iterations: 7
currently lose_sum: 98.5193185210228
time_elpased: 1.625
batch start
#iterations: 8
currently lose_sum: 98.46914094686508
time_elpased: 1.649
batch start
#iterations: 9
currently lose_sum: 98.18322658538818
time_elpased: 1.624
batch start
#iterations: 10
currently lose_sum: 98.21145749092102
time_elpased: 1.653
batch start
#iterations: 11
currently lose_sum: 97.89926010370255
time_elpased: 1.61
batch start
#iterations: 12
currently lose_sum: 97.93458235263824
time_elpased: 1.61
batch start
#iterations: 13
currently lose_sum: 97.71157598495483
time_elpased: 1.692
batch start
#iterations: 14
currently lose_sum: 97.64611077308655
time_elpased: 1.668
batch start
#iterations: 15
currently lose_sum: 97.70609283447266
time_elpased: 1.682
batch start
#iterations: 16
currently lose_sum: 97.61134856939316
time_elpased: 1.673
batch start
#iterations: 17
currently lose_sum: 97.46209889650345
time_elpased: 1.707
batch start
#iterations: 18
currently lose_sum: 97.13961923122406
time_elpased: 1.743
batch start
#iterations: 19
currently lose_sum: 97.46288698911667
time_elpased: 1.625
start validation test
0.642474226804
0.627650784908
0.703612225996
0.6634643377
0.642366889617
62.871
batch start
#iterations: 20
currently lose_sum: 97.12004518508911
time_elpased: 1.643
batch start
#iterations: 21
currently lose_sum: 97.19906604290009
time_elpased: 1.624
batch start
#iterations: 22
currently lose_sum: 97.14837473630905
time_elpased: 1.608
batch start
#iterations: 23
currently lose_sum: 97.23177844285965
time_elpased: 1.631
batch start
#iterations: 24
currently lose_sum: 97.1832731962204
time_elpased: 1.643
batch start
#iterations: 25
currently lose_sum: 97.14542299509048
time_elpased: 1.653
batch start
#iterations: 26
currently lose_sum: 97.13487339019775
time_elpased: 1.616
batch start
#iterations: 27
currently lose_sum: 97.03416013717651
time_elpased: 1.663
batch start
#iterations: 28
currently lose_sum: 96.82534801959991
time_elpased: 1.623
batch start
#iterations: 29
currently lose_sum: 97.30461746454239
time_elpased: 1.639
batch start
#iterations: 30
currently lose_sum: 96.62383717298508
time_elpased: 1.609
batch start
#iterations: 31
currently lose_sum: 96.74543869495392
time_elpased: 1.668
batch start
#iterations: 32
currently lose_sum: 96.79678291082382
time_elpased: 1.652
batch start
#iterations: 33
currently lose_sum: 96.9869339466095
time_elpased: 1.64
batch start
#iterations: 34
currently lose_sum: 96.82357823848724
time_elpased: 1.612
batch start
#iterations: 35
currently lose_sum: 96.63305217027664
time_elpased: 1.615
batch start
#iterations: 36
currently lose_sum: 96.67264658212662
time_elpased: 1.694
batch start
#iterations: 37
currently lose_sum: 96.5480129122734
time_elpased: 1.6
batch start
#iterations: 38
currently lose_sum: 96.34347242116928
time_elpased: 1.713
batch start
#iterations: 39
currently lose_sum: 96.49073070287704
time_elpased: 1.659
start validation test
0.648505154639
0.639541602465
0.683441391376
0.660763146112
0.648443818687
62.489
batch start
#iterations: 40
currently lose_sum: 96.53432351350784
time_elpased: 1.597
batch start
#iterations: 41
currently lose_sum: 96.65526866912842
time_elpased: 1.711
batch start
#iterations: 42
currently lose_sum: 96.44031202793121
time_elpased: 1.633
batch start
#iterations: 43
currently lose_sum: 96.27762508392334
time_elpased: 1.624
batch start
#iterations: 44
currently lose_sum: 96.45371460914612
time_elpased: 1.639
batch start
#iterations: 45
currently lose_sum: 96.09157192707062
time_elpased: 1.67
batch start
#iterations: 46
currently lose_sum: 96.20045095682144
time_elpased: 1.633
batch start
#iterations: 47
currently lose_sum: 96.40714663267136
time_elpased: 1.647
batch start
#iterations: 48
currently lose_sum: 96.55049556493759
time_elpased: 1.697
batch start
#iterations: 49
currently lose_sum: 96.30532938241959
time_elpased: 1.653
batch start
#iterations: 50
currently lose_sum: 96.55105376243591
time_elpased: 1.623
batch start
#iterations: 51
currently lose_sum: 96.11920529603958
time_elpased: 1.606
batch start
#iterations: 52
currently lose_sum: 96.28358721733093
time_elpased: 1.659
batch start
#iterations: 53
currently lose_sum: 96.57689887285233
time_elpased: 1.676
batch start
#iterations: 54
currently lose_sum: 96.50453823804855
time_elpased: 1.694
batch start
#iterations: 55
currently lose_sum: 95.9440153837204
time_elpased: 1.746
batch start
#iterations: 56
currently lose_sum: 95.9178318977356
time_elpased: 1.631
batch start
#iterations: 57
currently lose_sum: 96.13246780633926
time_elpased: 1.674
batch start
#iterations: 58
currently lose_sum: 96.39568108320236
time_elpased: 1.657
batch start
#iterations: 59
currently lose_sum: 96.0226320028305
time_elpased: 1.637
start validation test
0.649484536082
0.644563385866
0.669239477205
0.656669696052
0.649449853236
62.211
batch start
#iterations: 60
currently lose_sum: 95.87601912021637
time_elpased: 1.65
batch start
#iterations: 61
currently lose_sum: 96.49668079614639
time_elpased: 1.711
batch start
#iterations: 62
currently lose_sum: 95.69309133291245
time_elpased: 1.616
batch start
#iterations: 63
currently lose_sum: 96.38271117210388
time_elpased: 1.622
batch start
#iterations: 64
currently lose_sum: 96.06930756568909
time_elpased: 1.685
batch start
#iterations: 65
currently lose_sum: 95.91919672489166
time_elpased: 1.647
batch start
#iterations: 66
currently lose_sum: 96.00565218925476
time_elpased: 1.607
batch start
#iterations: 67
currently lose_sum: 96.04512107372284
time_elpased: 1.647
batch start
#iterations: 68
currently lose_sum: 96.09635210037231
time_elpased: 1.635
batch start
#iterations: 69
currently lose_sum: 96.25498896837234
time_elpased: 1.658
batch start
#iterations: 70
currently lose_sum: 95.7671035528183
time_elpased: 1.602
batch start
#iterations: 71
currently lose_sum: 95.68577647209167
time_elpased: 1.726
batch start
#iterations: 72
currently lose_sum: 96.12630212306976
time_elpased: 1.638
batch start
#iterations: 73
currently lose_sum: 95.7359990477562
time_elpased: 1.647
batch start
#iterations: 74
currently lose_sum: 95.90091675519943
time_elpased: 1.632
batch start
#iterations: 75
currently lose_sum: 95.55097949504852
time_elpased: 1.641
batch start
#iterations: 76
currently lose_sum: 95.68645483255386
time_elpased: 1.675
batch start
#iterations: 77
currently lose_sum: 96.01888632774353
time_elpased: 1.713
batch start
#iterations: 78
currently lose_sum: 96.11498594284058
time_elpased: 1.624
batch start
#iterations: 79
currently lose_sum: 96.02129942178726
time_elpased: 1.668
start validation test
0.650927835052
0.637398525707
0.702994751467
0.668591563081
0.650836423549
62.011
batch start
#iterations: 80
currently lose_sum: 95.8812444806099
time_elpased: 1.603
batch start
#iterations: 81
currently lose_sum: 96.07823956012726
time_elpased: 1.613
batch start
#iterations: 82
currently lose_sum: 96.2477245926857
time_elpased: 1.624
batch start
#iterations: 83
currently lose_sum: 95.70898842811584
time_elpased: 1.652
batch start
#iterations: 84
currently lose_sum: 95.8979389667511
time_elpased: 1.624
batch start
#iterations: 85
currently lose_sum: 96.00552707910538
time_elpased: 1.596
batch start
#iterations: 86
currently lose_sum: 96.23297542333603
time_elpased: 1.692
batch start
#iterations: 87
currently lose_sum: 95.30137830972672
time_elpased: 1.663
batch start
#iterations: 88
currently lose_sum: 95.90396308898926
time_elpased: 1.7
batch start
#iterations: 89
currently lose_sum: 95.66777992248535
time_elpased: 1.611
batch start
#iterations: 90
currently lose_sum: 96.07457685470581
time_elpased: 1.602
batch start
#iterations: 91
currently lose_sum: 95.78590685129166
time_elpased: 1.646
batch start
#iterations: 92
currently lose_sum: 95.63085985183716
time_elpased: 1.749
batch start
#iterations: 93
currently lose_sum: 96.1373535990715
time_elpased: 1.676
batch start
#iterations: 94
currently lose_sum: 95.73787951469421
time_elpased: 1.617
batch start
#iterations: 95
currently lose_sum: 95.71357786655426
time_elpased: 1.671
batch start
#iterations: 96
currently lose_sum: 95.85591065883636
time_elpased: 1.597
batch start
#iterations: 97
currently lose_sum: 95.46833956241608
time_elpased: 1.622
batch start
#iterations: 98
currently lose_sum: 95.61633658409119
time_elpased: 1.616
batch start
#iterations: 99
currently lose_sum: 95.45995384454727
time_elpased: 1.617
start validation test
0.654329896907
0.636133465955
0.723988885458
0.677223719677
0.654207599809
61.709
batch start
#iterations: 100
currently lose_sum: 95.48431289196014
time_elpased: 1.643
batch start
#iterations: 101
currently lose_sum: 95.76917725801468
time_elpased: 1.654
batch start
#iterations: 102
currently lose_sum: 95.71609807014465
time_elpased: 1.687
batch start
#iterations: 103
currently lose_sum: 95.52472454309464
time_elpased: 1.632
batch start
#iterations: 104
currently lose_sum: 95.81164014339447
time_elpased: 1.633
batch start
#iterations: 105
currently lose_sum: 95.66289550065994
time_elpased: 1.624
batch start
#iterations: 106
currently lose_sum: 95.80051749944687
time_elpased: 1.602
batch start
#iterations: 107
currently lose_sum: 95.68446838855743
time_elpased: 1.624
batch start
#iterations: 108
currently lose_sum: 95.56764322519302
time_elpased: 1.659
batch start
#iterations: 109
currently lose_sum: 95.70117390155792
time_elpased: 1.624
batch start
#iterations: 110
currently lose_sum: 95.52587354183197
time_elpased: 1.644
batch start
#iterations: 111
currently lose_sum: 95.61254984140396
time_elpased: 1.679
batch start
#iterations: 112
currently lose_sum: 95.61521220207214
time_elpased: 1.66
batch start
#iterations: 113
currently lose_sum: 95.40114158391953
time_elpased: 1.673
batch start
#iterations: 114
currently lose_sum: 95.40165084600449
time_elpased: 1.6
batch start
#iterations: 115
currently lose_sum: 95.60771507024765
time_elpased: 1.686
batch start
#iterations: 116
currently lose_sum: 95.71393233537674
time_elpased: 1.669
batch start
#iterations: 117
currently lose_sum: 95.7126117348671
time_elpased: 1.566
batch start
#iterations: 118
currently lose_sum: 95.58972507715225
time_elpased: 1.627
batch start
#iterations: 119
currently lose_sum: 95.71292394399643
time_elpased: 1.636
start validation test
0.652577319588
0.640120493269
0.699804466399
0.668633235005
0.652494405047
61.818
batch start
#iterations: 120
currently lose_sum: 95.58356368541718
time_elpased: 1.671
batch start
#iterations: 121
currently lose_sum: 95.3571662902832
time_elpased: 1.644
batch start
#iterations: 122
currently lose_sum: 95.62294709682465
time_elpased: 1.631
batch start
#iterations: 123
currently lose_sum: 95.57519727945328
time_elpased: 1.676
batch start
#iterations: 124
currently lose_sum: 95.59259116649628
time_elpased: 1.7
batch start
#iterations: 125
currently lose_sum: 95.70885610580444
time_elpased: 1.69
batch start
#iterations: 126
currently lose_sum: 95.63294291496277
time_elpased: 1.704
batch start
#iterations: 127
currently lose_sum: 95.7042943239212
time_elpased: 1.687
batch start
#iterations: 128
currently lose_sum: 95.5011043548584
time_elpased: 1.644
batch start
#iterations: 129
currently lose_sum: 95.49909740686417
time_elpased: 1.682
batch start
#iterations: 130
currently lose_sum: 95.29305493831635
time_elpased: 1.68
batch start
#iterations: 131
currently lose_sum: 95.69179570674896
time_elpased: 1.731
batch start
#iterations: 132
currently lose_sum: 95.56692898273468
time_elpased: 1.671
batch start
#iterations: 133
currently lose_sum: 95.356181204319
time_elpased: 1.649
batch start
#iterations: 134
currently lose_sum: 95.67224532365799
time_elpased: 1.658
batch start
#iterations: 135
currently lose_sum: 95.37390178442001
time_elpased: 1.646
batch start
#iterations: 136
currently lose_sum: 95.40538012981415
time_elpased: 1.668
batch start
#iterations: 137
currently lose_sum: 95.1588431596756
time_elpased: 1.679
batch start
#iterations: 138
currently lose_sum: 95.32251995801926
time_elpased: 1.65
batch start
#iterations: 139
currently lose_sum: 95.79985070228577
time_elpased: 1.768
start validation test
0.653762886598
0.635135135135
0.725532571781
0.677331027526
0.653636883846
61.703
batch start
#iterations: 140
currently lose_sum: 95.7901428937912
time_elpased: 1.642
batch start
#iterations: 141
currently lose_sum: 95.47158133983612
time_elpased: 1.637
batch start
#iterations: 142
currently lose_sum: 95.62503981590271
time_elpased: 1.642
batch start
#iterations: 143
currently lose_sum: 95.435899913311
time_elpased: 1.709
batch start
#iterations: 144
currently lose_sum: 95.89106869697571
time_elpased: 1.62
batch start
#iterations: 145
currently lose_sum: 95.07753068208694
time_elpased: 1.655
batch start
#iterations: 146
currently lose_sum: 95.43309181928635
time_elpased: 1.587
batch start
#iterations: 147
currently lose_sum: 95.5674192905426
time_elpased: 1.632
batch start
#iterations: 148
currently lose_sum: 95.42788457870483
time_elpased: 1.622
batch start
#iterations: 149
currently lose_sum: 95.18326038122177
time_elpased: 1.637
batch start
#iterations: 150
currently lose_sum: 95.03715354204178
time_elpased: 1.651
batch start
#iterations: 151
currently lose_sum: 95.3026157617569
time_elpased: 1.663
batch start
#iterations: 152
currently lose_sum: 95.4967737197876
time_elpased: 1.6
batch start
#iterations: 153
currently lose_sum: 95.554796397686
time_elpased: 1.611
batch start
#iterations: 154
currently lose_sum: 95.68554747104645
time_elpased: 1.657
batch start
#iterations: 155
currently lose_sum: 95.41369444131851
time_elpased: 1.646
batch start
#iterations: 156
currently lose_sum: 95.3934617638588
time_elpased: 1.571
batch start
#iterations: 157
currently lose_sum: 95.68126046657562
time_elpased: 1.628
batch start
#iterations: 158
currently lose_sum: 95.99248397350311
time_elpased: 1.663
batch start
#iterations: 159
currently lose_sum: 95.8807064294815
time_elpased: 1.631
start validation test
0.654793814433
0.63896558071
0.714520942678
0.674634407035
0.654688954247
61.788
batch start
#iterations: 160
currently lose_sum: 95.288001537323
time_elpased: 1.672
batch start
#iterations: 161
currently lose_sum: 94.9544289112091
time_elpased: 1.67
batch start
#iterations: 162
currently lose_sum: 95.45311057567596
time_elpased: 1.637
batch start
#iterations: 163
currently lose_sum: 95.07199293375015
time_elpased: 1.701
batch start
#iterations: 164
currently lose_sum: 95.29509103298187
time_elpased: 1.696
batch start
#iterations: 165
currently lose_sum: 95.50972318649292
time_elpased: 1.659
batch start
#iterations: 166
currently lose_sum: 95.24441808462143
time_elpased: 1.658
batch start
#iterations: 167
currently lose_sum: 95.51874196529388
time_elpased: 1.65
batch start
#iterations: 168
currently lose_sum: 95.21663892269135
time_elpased: 1.65
batch start
#iterations: 169
currently lose_sum: 95.26786506175995
time_elpased: 1.653
batch start
#iterations: 170
currently lose_sum: 95.34701752662659
time_elpased: 1.649
batch start
#iterations: 171
currently lose_sum: 95.42365598678589
time_elpased: 1.642
batch start
#iterations: 172
currently lose_sum: 95.36382532119751
time_elpased: 1.687
batch start
#iterations: 173
currently lose_sum: 95.5081884264946
time_elpased: 1.697
batch start
#iterations: 174
currently lose_sum: 95.23621308803558
time_elpased: 1.709
batch start
#iterations: 175
currently lose_sum: 95.24637562036514
time_elpased: 1.63
batch start
#iterations: 176
currently lose_sum: 95.28734135627747
time_elpased: 1.651
batch start
#iterations: 177
currently lose_sum: 95.28401619195938
time_elpased: 1.68
batch start
#iterations: 178
currently lose_sum: 94.91263937950134
time_elpased: 1.653
batch start
#iterations: 179
currently lose_sum: 95.56125926971436
time_elpased: 1.674
start validation test
0.65087628866
0.639341158652
0.695070495009
0.666042108377
0.650798698914
61.808
batch start
#iterations: 180
currently lose_sum: 95.39449107646942
time_elpased: 1.659
batch start
#iterations: 181
currently lose_sum: 95.38976413011551
time_elpased: 1.595
batch start
#iterations: 182
currently lose_sum: 95.27132004499435
time_elpased: 1.621
batch start
#iterations: 183
currently lose_sum: 95.09854364395142
time_elpased: 1.647
batch start
#iterations: 184
currently lose_sum: 95.37889862060547
time_elpased: 1.68
batch start
#iterations: 185
currently lose_sum: 95.26135617494583
time_elpased: 1.693
batch start
#iterations: 186
currently lose_sum: 95.00653982162476
time_elpased: 1.634
batch start
#iterations: 187
currently lose_sum: 95.32953119277954
time_elpased: 1.659
batch start
#iterations: 188
currently lose_sum: 95.28130096197128
time_elpased: 1.674
batch start
#iterations: 189
currently lose_sum: 95.57044267654419
time_elpased: 1.665
batch start
#iterations: 190
currently lose_sum: 95.58523494005203
time_elpased: 1.699
batch start
#iterations: 191
currently lose_sum: 95.13711047172546
time_elpased: 1.695
batch start
#iterations: 192
currently lose_sum: 95.06976693868637
time_elpased: 1.61
batch start
#iterations: 193
currently lose_sum: 95.21525001525879
time_elpased: 1.733
batch start
#iterations: 194
currently lose_sum: 95.38368439674377
time_elpased: 1.673
batch start
#iterations: 195
currently lose_sum: 95.29273998737335
time_elpased: 1.673
batch start
#iterations: 196
currently lose_sum: 95.61311340332031
time_elpased: 1.696
batch start
#iterations: 197
currently lose_sum: 95.4073873758316
time_elpased: 1.679
batch start
#iterations: 198
currently lose_sum: 95.38563370704651
time_elpased: 1.648
batch start
#iterations: 199
currently lose_sum: 95.18833935260773
time_elpased: 1.682
start validation test
0.653350515464
0.642340627973
0.694761757744
0.66752360706
0.653277811641
61.686
batch start
#iterations: 200
currently lose_sum: 95.26642262935638
time_elpased: 1.689
batch start
#iterations: 201
currently lose_sum: 95.04816722869873
time_elpased: 1.647
batch start
#iterations: 202
currently lose_sum: 95.30001616477966
time_elpased: 1.607
batch start
#iterations: 203
currently lose_sum: 94.83856719732285
time_elpased: 1.665
batch start
#iterations: 204
currently lose_sum: 94.91449069976807
time_elpased: 1.596
batch start
#iterations: 205
currently lose_sum: 95.28791046142578
time_elpased: 1.614
batch start
#iterations: 206
currently lose_sum: 95.19601738452911
time_elpased: 1.669
batch start
#iterations: 207
currently lose_sum: 95.36775016784668
time_elpased: 1.665
batch start
#iterations: 208
currently lose_sum: 94.83613514900208
time_elpased: 1.628
batch start
#iterations: 209
currently lose_sum: 94.92992347478867
time_elpased: 1.649
batch start
#iterations: 210
currently lose_sum: 94.97907507419586
time_elpased: 1.638
batch start
#iterations: 211
currently lose_sum: 94.67779809236526
time_elpased: 1.62
batch start
#iterations: 212
currently lose_sum: 95.26935577392578
time_elpased: 1.638
batch start
#iterations: 213
currently lose_sum: 95.08845859766006
time_elpased: 1.675
batch start
#iterations: 214
currently lose_sum: 95.20399469137192
time_elpased: 1.682
batch start
#iterations: 215
currently lose_sum: 94.96378415822983
time_elpased: 1.622
batch start
#iterations: 216
currently lose_sum: 94.95884704589844
time_elpased: 1.694
batch start
#iterations: 217
currently lose_sum: 95.2933634519577
time_elpased: 1.671
batch start
#iterations: 218
currently lose_sum: 95.10464197397232
time_elpased: 1.655
batch start
#iterations: 219
currently lose_sum: 95.40665864944458
time_elpased: 1.657
start validation test
0.651288659794
0.63880007523
0.699084079448
0.667583902511
0.651204747563
61.658
batch start
#iterations: 220
currently lose_sum: 94.97944271564484
time_elpased: 1.611
batch start
#iterations: 221
currently lose_sum: 95.36029851436615
time_elpased: 1.667
batch start
#iterations: 222
currently lose_sum: 95.06495207548141
time_elpased: 1.73
batch start
#iterations: 223
currently lose_sum: 95.2171785235405
time_elpased: 1.64
batch start
#iterations: 224
currently lose_sum: 95.30865478515625
time_elpased: 1.659
batch start
#iterations: 225
currently lose_sum: 95.14947181940079
time_elpased: 1.671
batch start
#iterations: 226
currently lose_sum: 94.92462611198425
time_elpased: 1.581
batch start
#iterations: 227
currently lose_sum: 95.22559642791748
time_elpased: 1.615
batch start
#iterations: 228
currently lose_sum: 94.78148603439331
time_elpased: 1.694
batch start
#iterations: 229
currently lose_sum: 95.22164940834045
time_elpased: 1.679
batch start
#iterations: 230
currently lose_sum: 94.98421680927277
time_elpased: 1.605
batch start
#iterations: 231
currently lose_sum: 95.03455287218094
time_elpased: 1.676
batch start
#iterations: 232
currently lose_sum: 95.07029783725739
time_elpased: 1.647
batch start
#iterations: 233
currently lose_sum: 95.21868497133255
time_elpased: 1.656
batch start
#iterations: 234
currently lose_sum: 95.1466611623764
time_elpased: 1.681
batch start
#iterations: 235
currently lose_sum: 95.3486720919609
time_elpased: 1.702
batch start
#iterations: 236
currently lose_sum: 95.22587239742279
time_elpased: 1.598
batch start
#iterations: 237
currently lose_sum: 95.43705332279205
time_elpased: 1.654
batch start
#iterations: 238
currently lose_sum: 95.45457863807678
time_elpased: 1.7
batch start
#iterations: 239
currently lose_sum: 95.05849766731262
time_elpased: 1.683
start validation test
0.648092783505
0.64188923802
0.672738499537
0.656951911964
0.648049514149
61.796
batch start
#iterations: 240
currently lose_sum: 94.84592717885971
time_elpased: 1.639
batch start
#iterations: 241
currently lose_sum: 95.07240146398544
time_elpased: 1.67
batch start
#iterations: 242
currently lose_sum: 95.03193438053131
time_elpased: 1.612
batch start
#iterations: 243
currently lose_sum: 95.48413264751434
time_elpased: 1.713
batch start
#iterations: 244
currently lose_sum: 95.19793736934662
time_elpased: 1.717
batch start
#iterations: 245
currently lose_sum: 95.45726102590561
time_elpased: 1.645
batch start
#iterations: 246
currently lose_sum: 94.89378821849823
time_elpased: 1.653
batch start
#iterations: 247
currently lose_sum: 95.23836570978165
time_elpased: 1.631
batch start
#iterations: 248
currently lose_sum: 95.16698759794235
time_elpased: 1.627
batch start
#iterations: 249
currently lose_sum: 95.13510179519653
time_elpased: 1.665
batch start
#iterations: 250
currently lose_sum: 94.89780735969543
time_elpased: 1.655
batch start
#iterations: 251
currently lose_sum: 95.32235270738602
time_elpased: 1.635
batch start
#iterations: 252
currently lose_sum: 95.00064331293106
time_elpased: 1.661
batch start
#iterations: 253
currently lose_sum: 95.442147731781
time_elpased: 1.634
batch start
#iterations: 254
currently lose_sum: 94.90036761760712
time_elpased: 1.656
batch start
#iterations: 255
currently lose_sum: 95.05748635530472
time_elpased: 1.652
batch start
#iterations: 256
currently lose_sum: 94.7870557308197
time_elpased: 1.645
batch start
#iterations: 257
currently lose_sum: 94.97409981489182
time_elpased: 1.649
batch start
#iterations: 258
currently lose_sum: 94.69776463508606
time_elpased: 1.649
batch start
#iterations: 259
currently lose_sum: 95.02720642089844
time_elpased: 1.632
start validation test
0.655824742268
0.638660828316
0.720489863126
0.677112046037
0.655711212674
61.644
batch start
#iterations: 260
currently lose_sum: 94.92738837003708
time_elpased: 1.637
batch start
#iterations: 261
currently lose_sum: 95.02058804035187
time_elpased: 1.662
batch start
#iterations: 262
currently lose_sum: 94.68087989091873
time_elpased: 1.676
batch start
#iterations: 263
currently lose_sum: 94.48401618003845
time_elpased: 1.635
batch start
#iterations: 264
currently lose_sum: 94.92588478326797
time_elpased: 1.674
batch start
#iterations: 265
currently lose_sum: 95.49954760074615
time_elpased: 1.645
batch start
#iterations: 266
currently lose_sum: 95.15266489982605
time_elpased: 1.613
batch start
#iterations: 267
currently lose_sum: 95.17099809646606
time_elpased: 1.689
batch start
#iterations: 268
currently lose_sum: 95.00468689203262
time_elpased: 1.674
batch start
#iterations: 269
currently lose_sum: 95.12592738866806
time_elpased: 1.63
batch start
#iterations: 270
currently lose_sum: 95.13594830036163
time_elpased: 1.678
batch start
#iterations: 271
currently lose_sum: 95.10795277357101
time_elpased: 1.653
batch start
#iterations: 272
currently lose_sum: 95.23408085107803
time_elpased: 1.706
batch start
#iterations: 273
currently lose_sum: 94.71535211801529
time_elpased: 1.633
batch start
#iterations: 274
currently lose_sum: 95.50120413303375
time_elpased: 1.663
batch start
#iterations: 275
currently lose_sum: 95.14086103439331
time_elpased: 1.612
batch start
#iterations: 276
currently lose_sum: 94.79133212566376
time_elpased: 1.669
batch start
#iterations: 277
currently lose_sum: 95.16298466920853
time_elpased: 1.631
batch start
#iterations: 278
currently lose_sum: 94.99192947149277
time_elpased: 1.573
batch start
#iterations: 279
currently lose_sum: 94.3270435333252
time_elpased: 1.618
start validation test
0.657577319588
0.641242418673
0.718122877431
0.677508616923
0.657471022522
61.523
batch start
#iterations: 280
currently lose_sum: 95.08144348859787
time_elpased: 1.641
batch start
#iterations: 281
currently lose_sum: 95.35654664039612
time_elpased: 1.653
batch start
#iterations: 282
currently lose_sum: 94.75187939405441
time_elpased: 1.63
batch start
#iterations: 283
currently lose_sum: 95.1345928311348
time_elpased: 1.61
batch start
#iterations: 284
currently lose_sum: 94.9264897108078
time_elpased: 1.591
batch start
#iterations: 285
currently lose_sum: 94.94960868358612
time_elpased: 1.615
batch start
#iterations: 286
currently lose_sum: 94.92298871278763
time_elpased: 1.675
batch start
#iterations: 287
currently lose_sum: 94.62725389003754
time_elpased: 1.646
batch start
#iterations: 288
currently lose_sum: 94.624791264534
time_elpased: 1.614
batch start
#iterations: 289
currently lose_sum: 95.44759541749954
time_elpased: 1.632
batch start
#iterations: 290
currently lose_sum: 94.96914726495743
time_elpased: 1.647
batch start
#iterations: 291
currently lose_sum: 94.7361952662468
time_elpased: 1.631
batch start
#iterations: 292
currently lose_sum: 95.04464852809906
time_elpased: 1.641
batch start
#iterations: 293
currently lose_sum: 94.9464619755745
time_elpased: 1.681
batch start
#iterations: 294
currently lose_sum: 95.1143147945404
time_elpased: 1.608
batch start
#iterations: 295
currently lose_sum: 95.06355041265488
time_elpased: 1.584
batch start
#iterations: 296
currently lose_sum: 94.70899993181229
time_elpased: 1.624
batch start
#iterations: 297
currently lose_sum: 94.85464322566986
time_elpased: 1.663
batch start
#iterations: 298
currently lose_sum: 94.74438029527664
time_elpased: 1.638
batch start
#iterations: 299
currently lose_sum: 94.87375032901764
time_elpased: 1.714
start validation test
0.652680412371
0.636839687644
0.713388906041
0.672944374333
0.652573829247
61.621
batch start
#iterations: 300
currently lose_sum: 94.37927055358887
time_elpased: 1.628
batch start
#iterations: 301
currently lose_sum: 95.24478578567505
time_elpased: 1.613
batch start
#iterations: 302
currently lose_sum: 94.85860115289688
time_elpased: 1.609
batch start
#iterations: 303
currently lose_sum: 95.0282216668129
time_elpased: 1.665
batch start
#iterations: 304
currently lose_sum: 95.00323098897934
time_elpased: 1.656
batch start
#iterations: 305
currently lose_sum: 94.81824260950089
time_elpased: 1.702
batch start
#iterations: 306
currently lose_sum: 94.91448628902435
time_elpased: 1.685
batch start
#iterations: 307
currently lose_sum: 95.1796236038208
time_elpased: 1.647
batch start
#iterations: 308
currently lose_sum: 94.73625260591507
time_elpased: 1.683
batch start
#iterations: 309
currently lose_sum: 94.84663552045822
time_elpased: 1.675
batch start
#iterations: 310
currently lose_sum: 95.00582355260849
time_elpased: 1.612
batch start
#iterations: 311
currently lose_sum: 94.57755208015442
time_elpased: 1.641
batch start
#iterations: 312
currently lose_sum: 94.78417199850082
time_elpased: 1.614
batch start
#iterations: 313
currently lose_sum: 95.35426032543182
time_elpased: 1.682
batch start
#iterations: 314
currently lose_sum: 94.8729692697525
time_elpased: 1.649
batch start
#iterations: 315
currently lose_sum: 94.78040939569473
time_elpased: 1.633
batch start
#iterations: 316
currently lose_sum: 94.74221014976501
time_elpased: 1.629
batch start
#iterations: 317
currently lose_sum: 95.11155593395233
time_elpased: 1.657
batch start
#iterations: 318
currently lose_sum: 94.57175344228745
time_elpased: 1.644
batch start
#iterations: 319
currently lose_sum: 95.36511564254761
time_elpased: 1.686
start validation test
0.653917525773
0.642660332542
0.696099619224
0.668313407766
0.653843468602
61.777
batch start
#iterations: 320
currently lose_sum: 94.98448026180267
time_elpased: 1.645
batch start
#iterations: 321
currently lose_sum: 94.96841633319855
time_elpased: 1.64
batch start
#iterations: 322
currently lose_sum: 94.89288371801376
time_elpased: 1.634
batch start
#iterations: 323
currently lose_sum: 94.98246145248413
time_elpased: 1.685
batch start
#iterations: 324
currently lose_sum: 95.19877225160599
time_elpased: 1.637
batch start
#iterations: 325
currently lose_sum: 94.72963178157806
time_elpased: 1.609
batch start
#iterations: 326
currently lose_sum: 95.08537620306015
time_elpased: 1.673
batch start
#iterations: 327
currently lose_sum: 94.88926750421524
time_elpased: 1.614
batch start
#iterations: 328
currently lose_sum: 94.7568079829216
time_elpased: 1.641
batch start
#iterations: 329
currently lose_sum: 94.97997546195984
time_elpased: 1.66
batch start
#iterations: 330
currently lose_sum: 94.65625393390656
time_elpased: 1.71
batch start
#iterations: 331
currently lose_sum: 94.42665839195251
time_elpased: 1.652
batch start
#iterations: 332
currently lose_sum: 94.58994269371033
time_elpased: 1.664
batch start
#iterations: 333
currently lose_sum: 94.5025976896286
time_elpased: 1.663
batch start
#iterations: 334
currently lose_sum: 94.7536084651947
time_elpased: 1.65
batch start
#iterations: 335
currently lose_sum: 94.6717255115509
time_elpased: 1.621
batch start
#iterations: 336
currently lose_sum: 95.06113934516907
time_elpased: 1.655
batch start
#iterations: 337
currently lose_sum: 94.74167513847351
time_elpased: 1.641
batch start
#iterations: 338
currently lose_sum: 94.9045439362526
time_elpased: 1.668
batch start
#iterations: 339
currently lose_sum: 94.56654798984528
time_elpased: 1.658
start validation test
0.650309278351
0.63758326297
0.699392816713
0.667059285434
0.650223104628
61.545
batch start
#iterations: 340
currently lose_sum: 95.05146127939224
time_elpased: 1.642
batch start
#iterations: 341
currently lose_sum: 95.0473929643631
time_elpased: 1.615
batch start
#iterations: 342
currently lose_sum: 94.82460701465607
time_elpased: 1.589
batch start
#iterations: 343
currently lose_sum: 94.74756890535355
time_elpased: 1.63
batch start
#iterations: 344
currently lose_sum: 94.78887295722961
time_elpased: 1.666
batch start
#iterations: 345
currently lose_sum: 94.72882831096649
time_elpased: 1.651
batch start
#iterations: 346
currently lose_sum: 94.9860652089119
time_elpased: 1.642
batch start
#iterations: 347
currently lose_sum: 94.97546029090881
time_elpased: 1.669
batch start
#iterations: 348
currently lose_sum: 94.9453821182251
time_elpased: 1.599
batch start
#iterations: 349
currently lose_sum: 94.88932430744171
time_elpased: 1.638
batch start
#iterations: 350
currently lose_sum: 94.86177265644073
time_elpased: 1.635
batch start
#iterations: 351
currently lose_sum: 95.02611589431763
time_elpased: 1.677
batch start
#iterations: 352
currently lose_sum: 95.28031557798386
time_elpased: 1.631
batch start
#iterations: 353
currently lose_sum: 95.22949874401093
time_elpased: 1.672
batch start
#iterations: 354
currently lose_sum: 95.40810549259186
time_elpased: 1.676
batch start
#iterations: 355
currently lose_sum: 95.04879450798035
time_elpased: 1.641
batch start
#iterations: 356
currently lose_sum: 94.56684547662735
time_elpased: 1.699
batch start
#iterations: 357
currently lose_sum: 95.07439756393433
time_elpased: 1.656
batch start
#iterations: 358
currently lose_sum: 95.1800684928894
time_elpased: 1.649
batch start
#iterations: 359
currently lose_sum: 94.77406793832779
time_elpased: 1.689
start validation test
0.653402061856
0.63827035018
0.710919007924
0.672638753651
0.653301081985
61.727
batch start
#iterations: 360
currently lose_sum: 95.05094319581985
time_elpased: 1.606
batch start
#iterations: 361
currently lose_sum: 95.27769601345062
time_elpased: 1.639
batch start
#iterations: 362
currently lose_sum: 94.28625124692917
time_elpased: 1.684
batch start
#iterations: 363
currently lose_sum: 95.04417222738266
time_elpased: 1.622
batch start
#iterations: 364
currently lose_sum: 95.29737108945847
time_elpased: 1.665
batch start
#iterations: 365
currently lose_sum: 94.91209816932678
time_elpased: 1.654
batch start
#iterations: 366
currently lose_sum: 95.03864848613739
time_elpased: 1.596
batch start
#iterations: 367
currently lose_sum: 94.54764068126678
time_elpased: 1.667
batch start
#iterations: 368
currently lose_sum: 94.94764000177383
time_elpased: 1.637
batch start
#iterations: 369
currently lose_sum: 94.69195234775543
time_elpased: 1.629
batch start
#iterations: 370
currently lose_sum: 94.61484861373901
time_elpased: 1.641
batch start
#iterations: 371
currently lose_sum: 94.94036889076233
time_elpased: 1.649
batch start
#iterations: 372
currently lose_sum: 94.98518669605255
time_elpased: 1.709
batch start
#iterations: 373
currently lose_sum: 94.74392431974411
time_elpased: 1.638
batch start
#iterations: 374
currently lose_sum: 94.85302472114563
time_elpased: 1.664
batch start
#iterations: 375
currently lose_sum: 94.65199595689774
time_elpased: 1.678
batch start
#iterations: 376
currently lose_sum: 94.5476570725441
time_elpased: 1.646
batch start
#iterations: 377
currently lose_sum: 95.34252923727036
time_elpased: 1.674
batch start
#iterations: 378
currently lose_sum: 94.6066330075264
time_elpased: 1.622
batch start
#iterations: 379
currently lose_sum: 94.87262737751007
time_elpased: 1.692
start validation test
0.65293814433
0.638327461524
0.708552022229
0.671609032824
0.652840505589
61.609
batch start
#iterations: 380
currently lose_sum: 94.90872198343277
time_elpased: 1.606
batch start
#iterations: 381
currently lose_sum: 94.86066180467606
time_elpased: 1.604
batch start
#iterations: 382
currently lose_sum: 94.66358095407486
time_elpased: 1.665
batch start
#iterations: 383
currently lose_sum: 94.5645939707756
time_elpased: 1.675
batch start
#iterations: 384
currently lose_sum: 94.69564765691757
time_elpased: 1.671
batch start
#iterations: 385
currently lose_sum: 94.95228010416031
time_elpased: 1.655
batch start
#iterations: 386
currently lose_sum: 94.49393773078918
time_elpased: 1.653
batch start
#iterations: 387
currently lose_sum: 94.60768932104111
time_elpased: 1.672
batch start
#iterations: 388
currently lose_sum: 95.15896445512772
time_elpased: 1.644
batch start
#iterations: 389
currently lose_sum: 94.62475335597992
time_elpased: 1.596
batch start
#iterations: 390
currently lose_sum: 94.88074165582657
time_elpased: 1.633
batch start
#iterations: 391
currently lose_sum: 94.51379626989365
time_elpased: 1.677
batch start
#iterations: 392
currently lose_sum: 94.58850854635239
time_elpased: 1.717
batch start
#iterations: 393
currently lose_sum: 94.59650009870529
time_elpased: 1.662
batch start
#iterations: 394
currently lose_sum: 94.82229685783386
time_elpased: 1.683
batch start
#iterations: 395
currently lose_sum: 94.67683279514313
time_elpased: 1.659
batch start
#iterations: 396
currently lose_sum: 94.2547242641449
time_elpased: 1.67
batch start
#iterations: 397
currently lose_sum: 94.53667831420898
time_elpased: 1.643
batch start
#iterations: 398
currently lose_sum: 94.7565940618515
time_elpased: 1.669
batch start
#iterations: 399
currently lose_sum: 94.73783540725708
time_elpased: 1.683
start validation test
0.654742268041
0.636940941667
0.722548111557
0.677049180328
0.654623224425
61.660
acc: 0.658
pre: 0.644
rec: 0.712
F1: 0.676
auc: 0.696
