start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.27970540523529
time_elpased: 1.678
batch start
#iterations: 1
currently lose_sum: 100.07692509889603
time_elpased: 1.631
batch start
#iterations: 2
currently lose_sum: 99.7945848107338
time_elpased: 1.716
batch start
#iterations: 3
currently lose_sum: 99.35439735651016
time_elpased: 1.686
batch start
#iterations: 4
currently lose_sum: 99.13783860206604
time_elpased: 1.65
batch start
#iterations: 5
currently lose_sum: 98.99379235506058
time_elpased: 1.713
batch start
#iterations: 6
currently lose_sum: 98.55059432983398
time_elpased: 1.696
batch start
#iterations: 7
currently lose_sum: 98.39111357927322
time_elpased: 1.665
batch start
#iterations: 8
currently lose_sum: 98.273835003376
time_elpased: 1.722
batch start
#iterations: 9
currently lose_sum: 98.26820540428162
time_elpased: 1.68
batch start
#iterations: 10
currently lose_sum: 98.1991525888443
time_elpased: 1.691
batch start
#iterations: 11
currently lose_sum: 97.8796655535698
time_elpased: 1.635
batch start
#iterations: 12
currently lose_sum: 98.12096589803696
time_elpased: 1.718
batch start
#iterations: 13
currently lose_sum: 97.86412245035172
time_elpased: 1.672
batch start
#iterations: 14
currently lose_sum: 97.5536920428276
time_elpased: 1.65
batch start
#iterations: 15
currently lose_sum: 97.81498390436172
time_elpased: 1.661
batch start
#iterations: 16
currently lose_sum: 97.48690927028656
time_elpased: 1.59
batch start
#iterations: 17
currently lose_sum: 97.31747686862946
time_elpased: 1.686
batch start
#iterations: 18
currently lose_sum: 97.25620996952057
time_elpased: 1.618
batch start
#iterations: 19
currently lose_sum: 97.32940578460693
time_elpased: 1.69
start validation test
0.63912371134
0.629977029096
0.677369558506
0.652814282172
0.639056564856
62.947
batch start
#iterations: 20
currently lose_sum: 97.36907905340195
time_elpased: 1.697
batch start
#iterations: 21
currently lose_sum: 97.32745462656021
time_elpased: 1.62
batch start
#iterations: 22
currently lose_sum: 97.03602534532547
time_elpased: 1.688
batch start
#iterations: 23
currently lose_sum: 96.92263013124466
time_elpased: 1.683
batch start
#iterations: 24
currently lose_sum: 96.902379155159
time_elpased: 1.647
batch start
#iterations: 25
currently lose_sum: 97.31562143564224
time_elpased: 1.627
batch start
#iterations: 26
currently lose_sum: 97.13073468208313
time_elpased: 1.67
batch start
#iterations: 27
currently lose_sum: 96.8567287325859
time_elpased: 1.616
batch start
#iterations: 28
currently lose_sum: 96.87452632188797
time_elpased: 1.689
batch start
#iterations: 29
currently lose_sum: 97.09251409769058
time_elpased: 1.674
batch start
#iterations: 30
currently lose_sum: 96.73428446054459
time_elpased: 1.642
batch start
#iterations: 31
currently lose_sum: 96.50596761703491
time_elpased: 1.625
batch start
#iterations: 32
currently lose_sum: 96.63116431236267
time_elpased: 1.67
batch start
#iterations: 33
currently lose_sum: 96.86388862133026
time_elpased: 1.703
batch start
#iterations: 34
currently lose_sum: 96.61150765419006
time_elpased: 1.647
batch start
#iterations: 35
currently lose_sum: 96.84166878461838
time_elpased: 1.779
batch start
#iterations: 36
currently lose_sum: 96.26100873947144
time_elpased: 1.598
batch start
#iterations: 37
currently lose_sum: 96.69653338193893
time_elpased: 1.635
batch start
#iterations: 38
currently lose_sum: 96.67157089710236
time_elpased: 1.708
batch start
#iterations: 39
currently lose_sum: 96.56372505426407
time_elpased: 1.673
start validation test
0.644690721649
0.638485680659
0.669959864156
0.653844222367
0.644646357772
62.475
batch start
#iterations: 40
currently lose_sum: 96.69961607456207
time_elpased: 1.653
batch start
#iterations: 41
currently lose_sum: 96.41708201169968
time_elpased: 1.686
batch start
#iterations: 42
currently lose_sum: 96.5643612742424
time_elpased: 1.667
batch start
#iterations: 43
currently lose_sum: 96.29704719781876
time_elpased: 1.669
batch start
#iterations: 44
currently lose_sum: 96.46397376060486
time_elpased: 1.655
batch start
#iterations: 45
currently lose_sum: 96.35422646999359
time_elpased: 1.696
batch start
#iterations: 46
currently lose_sum: 96.26053500175476
time_elpased: 1.658
batch start
#iterations: 47
currently lose_sum: 96.45725029706955
time_elpased: 1.657
batch start
#iterations: 48
currently lose_sum: 96.570003926754
time_elpased: 1.63
batch start
#iterations: 49
currently lose_sum: 96.2801770567894
time_elpased: 1.684
batch start
#iterations: 50
currently lose_sum: 96.30020415782928
time_elpased: 1.703
batch start
#iterations: 51
currently lose_sum: 96.12346810102463
time_elpased: 1.676
batch start
#iterations: 52
currently lose_sum: 96.12883698940277
time_elpased: 1.658
batch start
#iterations: 53
currently lose_sum: 96.39658463001251
time_elpased: 1.684
batch start
#iterations: 54
currently lose_sum: 96.18475830554962
time_elpased: 1.627
batch start
#iterations: 55
currently lose_sum: 96.1000303030014
time_elpased: 1.663
batch start
#iterations: 56
currently lose_sum: 96.24328798055649
time_elpased: 1.661
batch start
#iterations: 57
currently lose_sum: 96.10025018453598
time_elpased: 1.68
batch start
#iterations: 58
currently lose_sum: 95.96528369188309
time_elpased: 1.635
batch start
#iterations: 59
currently lose_sum: 96.195805311203
time_elpased: 1.663
start validation test
0.652680412371
0.636764300799
0.713697643306
0.673039596273
0.652573287211
62.097
batch start
#iterations: 60
currently lose_sum: 96.05713766813278
time_elpased: 1.629
batch start
#iterations: 61
currently lose_sum: 95.91245502233505
time_elpased: 1.676
batch start
#iterations: 62
currently lose_sum: 96.27008873224258
time_elpased: 1.635
batch start
#iterations: 63
currently lose_sum: 96.06204211711884
time_elpased: 1.7
batch start
#iterations: 64
currently lose_sum: 96.3832973241806
time_elpased: 1.671
batch start
#iterations: 65
currently lose_sum: 95.92134839296341
time_elpased: 1.649
batch start
#iterations: 66
currently lose_sum: 96.3430147767067
time_elpased: 1.636
batch start
#iterations: 67
currently lose_sum: 95.87233978509903
time_elpased: 1.671
batch start
#iterations: 68
currently lose_sum: 95.98182451725006
time_elpased: 1.686
batch start
#iterations: 69
currently lose_sum: 95.90185058116913
time_elpased: 1.661
batch start
#iterations: 70
currently lose_sum: 95.86615842580795
time_elpased: 1.649
batch start
#iterations: 71
currently lose_sum: 96.02064746618271
time_elpased: 1.623
batch start
#iterations: 72
currently lose_sum: 96.07004809379578
time_elpased: 1.678
batch start
#iterations: 73
currently lose_sum: 96.12572425603867
time_elpased: 1.685
batch start
#iterations: 74
currently lose_sum: 96.15748012065887
time_elpased: 1.637
batch start
#iterations: 75
currently lose_sum: 96.20557010173798
time_elpased: 1.693
batch start
#iterations: 76
currently lose_sum: 95.63881170749664
time_elpased: 1.632
batch start
#iterations: 77
currently lose_sum: 95.79986733198166
time_elpased: 1.639
batch start
#iterations: 78
currently lose_sum: 96.11995136737823
time_elpased: 1.693
batch start
#iterations: 79
currently lose_sum: 95.94068777561188
time_elpased: 1.659
start validation test
0.650515463918
0.638029890027
0.698569517341
0.66692866968
0.650431097615
61.960
batch start
#iterations: 80
currently lose_sum: 96.09148836135864
time_elpased: 1.642
batch start
#iterations: 81
currently lose_sum: 95.99807214736938
time_elpased: 1.655
batch start
#iterations: 82
currently lose_sum: 96.16811233758926
time_elpased: 1.658
batch start
#iterations: 83
currently lose_sum: 95.73269826173782
time_elpased: 1.683
batch start
#iterations: 84
currently lose_sum: 96.06589549779892
time_elpased: 1.635
batch start
#iterations: 85
currently lose_sum: 96.1147409081459
time_elpased: 1.655
batch start
#iterations: 86
currently lose_sum: 95.67166978120804
time_elpased: 1.651
batch start
#iterations: 87
currently lose_sum: 96.06404823064804
time_elpased: 1.614
batch start
#iterations: 88
currently lose_sum: 95.59686172008514
time_elpased: 1.669
batch start
#iterations: 89
currently lose_sum: 96.00757098197937
time_elpased: 1.632
batch start
#iterations: 90
currently lose_sum: 96.03395283222198
time_elpased: 1.635
batch start
#iterations: 91
currently lose_sum: 95.91449123620987
time_elpased: 1.668
batch start
#iterations: 92
currently lose_sum: 95.76710748672485
time_elpased: 1.673
batch start
#iterations: 93
currently lose_sum: 96.00598508119583
time_elpased: 1.597
batch start
#iterations: 94
currently lose_sum: 95.73792827129364
time_elpased: 1.648
batch start
#iterations: 95
currently lose_sum: 95.57697284221649
time_elpased: 1.617
batch start
#iterations: 96
currently lose_sum: 95.68331825733185
time_elpased: 1.722
batch start
#iterations: 97
currently lose_sum: 95.66527515649796
time_elpased: 1.648
batch start
#iterations: 98
currently lose_sum: 95.79922324419022
time_elpased: 1.627
batch start
#iterations: 99
currently lose_sum: 96.01419293880463
time_elpased: 1.724
start validation test
0.64912371134
0.638968481375
0.688484100031
0.662802793877
0.649054608107
61.962
batch start
#iterations: 100
currently lose_sum: 96.08114975690842
time_elpased: 1.63
batch start
#iterations: 101
currently lose_sum: 95.5442316532135
time_elpased: 1.688
batch start
#iterations: 102
currently lose_sum: 95.8656398653984
time_elpased: 1.676
batch start
#iterations: 103
currently lose_sum: 95.98435235023499
time_elpased: 1.675
batch start
#iterations: 104
currently lose_sum: 95.72742253541946
time_elpased: 1.683
batch start
#iterations: 105
currently lose_sum: 95.55998438596725
time_elpased: 1.73
batch start
#iterations: 106
currently lose_sum: 95.9499996304512
time_elpased: 1.642
batch start
#iterations: 107
currently lose_sum: 96.05752295255661
time_elpased: 1.653
batch start
#iterations: 108
currently lose_sum: 95.53084087371826
time_elpased: 1.663
batch start
#iterations: 109
currently lose_sum: 96.05612796545029
time_elpased: 1.71
batch start
#iterations: 110
currently lose_sum: 95.83671075105667
time_elpased: 1.649
batch start
#iterations: 111
currently lose_sum: 95.61876577138901
time_elpased: 1.736
batch start
#iterations: 112
currently lose_sum: 95.56388705968857
time_elpased: 1.675
batch start
#iterations: 113
currently lose_sum: 95.48459088802338
time_elpased: 1.701
batch start
#iterations: 114
currently lose_sum: 95.52475994825363
time_elpased: 1.661
batch start
#iterations: 115
currently lose_sum: 95.87675255537033
time_elpased: 1.704
batch start
#iterations: 116
currently lose_sum: 95.31384521722794
time_elpased: 1.652
batch start
#iterations: 117
currently lose_sum: 95.62257409095764
time_elpased: 1.712
batch start
#iterations: 118
currently lose_sum: 95.20690453052521
time_elpased: 1.683
batch start
#iterations: 119
currently lose_sum: 95.6006218791008
time_elpased: 1.687
start validation test
0.647989690722
0.637158054711
0.690336523618
0.662682143739
0.647915344325
61.808
batch start
#iterations: 120
currently lose_sum: 95.72353768348694
time_elpased: 1.709
batch start
#iterations: 121
currently lose_sum: 95.43215274810791
time_elpased: 1.768
batch start
#iterations: 122
currently lose_sum: 95.39365154504776
time_elpased: 1.696
batch start
#iterations: 123
currently lose_sum: 95.68902164697647
time_elpased: 1.665
batch start
#iterations: 124
currently lose_sum: 95.78943759202957
time_elpased: 1.686
batch start
#iterations: 125
currently lose_sum: 95.59624326229095
time_elpased: 1.633
batch start
#iterations: 126
currently lose_sum: 95.96590119600296
time_elpased: 1.693
batch start
#iterations: 127
currently lose_sum: 95.67695957422256
time_elpased: 1.657
batch start
#iterations: 128
currently lose_sum: 95.52900797128677
time_elpased: 1.709
batch start
#iterations: 129
currently lose_sum: 96.2243070602417
time_elpased: 1.645
batch start
#iterations: 130
currently lose_sum: 95.28809589147568
time_elpased: 1.644
batch start
#iterations: 131
currently lose_sum: 95.93372517824173
time_elpased: 1.677
batch start
#iterations: 132
currently lose_sum: 95.55564558506012
time_elpased: 1.66
batch start
#iterations: 133
currently lose_sum: 95.51144641637802
time_elpased: 1.638
batch start
#iterations: 134
currently lose_sum: 95.25848698616028
time_elpased: 1.652
batch start
#iterations: 135
currently lose_sum: 95.47808575630188
time_elpased: 1.703
batch start
#iterations: 136
currently lose_sum: 95.4910096526146
time_elpased: 1.749
batch start
#iterations: 137
currently lose_sum: 95.42309445142746
time_elpased: 1.639
batch start
#iterations: 138
currently lose_sum: 95.67926353216171
time_elpased: 1.688
batch start
#iterations: 139
currently lose_sum: 95.29641962051392
time_elpased: 1.678
start validation test
0.647680412371
0.637447539107
0.68776371308
0.661650413346
0.647610039954
61.779
batch start
#iterations: 140
currently lose_sum: 95.63791006803513
time_elpased: 1.66
batch start
#iterations: 141
currently lose_sum: 95.67078459262848
time_elpased: 1.655
batch start
#iterations: 142
currently lose_sum: 95.36395293474197
time_elpased: 1.659
batch start
#iterations: 143
currently lose_sum: 95.50839030742645
time_elpased: 1.62
batch start
#iterations: 144
currently lose_sum: 95.59181100130081
time_elpased: 1.69
batch start
#iterations: 145
currently lose_sum: 95.45942836999893
time_elpased: 1.691
batch start
#iterations: 146
currently lose_sum: 95.49310374259949
time_elpased: 1.651
batch start
#iterations: 147
currently lose_sum: 95.53831446170807
time_elpased: 1.642
batch start
#iterations: 148
currently lose_sum: 95.41634351015091
time_elpased: 1.696
batch start
#iterations: 149
currently lose_sum: 95.63018476963043
time_elpased: 1.619
batch start
#iterations: 150
currently lose_sum: 95.5243576169014
time_elpased: 1.638
batch start
#iterations: 151
currently lose_sum: 95.37617528438568
time_elpased: 1.648
batch start
#iterations: 152
currently lose_sum: 95.49805504083633
time_elpased: 1.696
batch start
#iterations: 153
currently lose_sum: 95.80433624982834
time_elpased: 1.663
batch start
#iterations: 154
currently lose_sum: 95.36328220367432
time_elpased: 1.642
batch start
#iterations: 155
currently lose_sum: 95.43533629179001
time_elpased: 1.619
batch start
#iterations: 156
currently lose_sum: 95.70148140192032
time_elpased: 1.644
batch start
#iterations: 157
currently lose_sum: 95.05020934343338
time_elpased: 1.695
batch start
#iterations: 158
currently lose_sum: 95.4141696691513
time_elpased: 1.618
batch start
#iterations: 159
currently lose_sum: 95.3598603606224
time_elpased: 1.683
start validation test
0.651391752577
0.634738186462
0.716064629001
0.672953237584
0.651278209368
61.586
batch start
#iterations: 160
currently lose_sum: 95.31744945049286
time_elpased: 1.699
batch start
#iterations: 161
currently lose_sum: 95.97318994998932
time_elpased: 1.661
batch start
#iterations: 162
currently lose_sum: 95.7321463227272
time_elpased: 1.7
batch start
#iterations: 163
currently lose_sum: 95.50639021396637
time_elpased: 1.656
batch start
#iterations: 164
currently lose_sum: 95.3679147362709
time_elpased: 1.662
batch start
#iterations: 165
currently lose_sum: 95.47187185287476
time_elpased: 1.692
batch start
#iterations: 166
currently lose_sum: 95.68507027626038
time_elpased: 1.648
batch start
#iterations: 167
currently lose_sum: 95.27429330348969
time_elpased: 1.688
batch start
#iterations: 168
currently lose_sum: 95.32320952415466
time_elpased: 1.677
batch start
#iterations: 169
currently lose_sum: 95.33906334638596
time_elpased: 1.654
batch start
#iterations: 170
currently lose_sum: 95.78103905916214
time_elpased: 1.677
batch start
#iterations: 171
currently lose_sum: 95.64367789030075
time_elpased: 1.733
batch start
#iterations: 172
currently lose_sum: 95.74217647314072
time_elpased: 1.675
batch start
#iterations: 173
currently lose_sum: 95.61025935411453
time_elpased: 1.765
batch start
#iterations: 174
currently lose_sum: 95.04046893119812
time_elpased: 1.696
batch start
#iterations: 175
currently lose_sum: 95.10053873062134
time_elpased: 1.703
batch start
#iterations: 176
currently lose_sum: 95.32938832044601
time_elpased: 1.684
batch start
#iterations: 177
currently lose_sum: 95.3256322145462
time_elpased: 1.611
batch start
#iterations: 178
currently lose_sum: 95.47764992713928
time_elpased: 1.645
batch start
#iterations: 179
currently lose_sum: 95.47928661108017
time_elpased: 1.662
start validation test
0.657216494845
0.635840198423
0.738705361737
0.683423783681
0.657073428571
61.411
batch start
#iterations: 180
currently lose_sum: 95.22207564115524
time_elpased: 1.683
batch start
#iterations: 181
currently lose_sum: 95.26380491256714
time_elpased: 1.646
batch start
#iterations: 182
currently lose_sum: 95.27712959051132
time_elpased: 1.685
batch start
#iterations: 183
currently lose_sum: 95.3416296839714
time_elpased: 1.626
batch start
#iterations: 184
currently lose_sum: 95.76412749290466
time_elpased: 1.658
batch start
#iterations: 185
currently lose_sum: 94.99231600761414
time_elpased: 1.668
batch start
#iterations: 186
currently lose_sum: 95.70952922105789
time_elpased: 1.672
batch start
#iterations: 187
currently lose_sum: 95.261867582798
time_elpased: 1.684
batch start
#iterations: 188
currently lose_sum: 95.39683252573013
time_elpased: 1.695
batch start
#iterations: 189
currently lose_sum: 95.28080266714096
time_elpased: 1.729
batch start
#iterations: 190
currently lose_sum: 94.79103088378906
time_elpased: 1.638
batch start
#iterations: 191
currently lose_sum: 95.25771248340607
time_elpased: 1.686
batch start
#iterations: 192
currently lose_sum: 95.32171130180359
time_elpased: 1.637
batch start
#iterations: 193
currently lose_sum: 95.29799842834473
time_elpased: 1.631
batch start
#iterations: 194
currently lose_sum: 95.0145965218544
time_elpased: 1.678
batch start
#iterations: 195
currently lose_sum: 95.38612949848175
time_elpased: 1.642
batch start
#iterations: 196
currently lose_sum: 95.30783927440643
time_elpased: 1.685
batch start
#iterations: 197
currently lose_sum: 95.46889978647232
time_elpased: 1.677
batch start
#iterations: 198
currently lose_sum: 95.21204876899719
time_elpased: 1.654
batch start
#iterations: 199
currently lose_sum: 95.03593748807907
time_elpased: 1.613
start validation test
0.65118556701
0.638186246955
0.701039415457
0.668137903977
0.65109804089
61.444
batch start
#iterations: 200
currently lose_sum: 95.44237035512924
time_elpased: 1.679
batch start
#iterations: 201
currently lose_sum: 95.29908639192581
time_elpased: 1.62
batch start
#iterations: 202
currently lose_sum: 94.79552102088928
time_elpased: 1.65
batch start
#iterations: 203
currently lose_sum: 95.4580882191658
time_elpased: 1.631
batch start
#iterations: 204
currently lose_sum: 94.90795052051544
time_elpased: 1.699
batch start
#iterations: 205
currently lose_sum: 94.91842442750931
time_elpased: 1.663
batch start
#iterations: 206
currently lose_sum: 95.02067476511002
time_elpased: 1.661
batch start
#iterations: 207
currently lose_sum: 95.24751913547516
time_elpased: 1.604
batch start
#iterations: 208
currently lose_sum: 94.9242929816246
time_elpased: 1.796
batch start
#iterations: 209
currently lose_sum: 95.09585249423981
time_elpased: 1.685
batch start
#iterations: 210
currently lose_sum: 95.13774681091309
time_elpased: 1.661
batch start
#iterations: 211
currently lose_sum: 95.32037818431854
time_elpased: 1.706
batch start
#iterations: 212
currently lose_sum: 95.78036206960678
time_elpased: 1.741
batch start
#iterations: 213
currently lose_sum: 95.32612371444702
time_elpased: 1.705
batch start
#iterations: 214
currently lose_sum: 95.05800241231918
time_elpased: 1.667
batch start
#iterations: 215
currently lose_sum: 95.06578052043915
time_elpased: 1.663
batch start
#iterations: 216
currently lose_sum: 95.04346573352814
time_elpased: 1.65
batch start
#iterations: 217
currently lose_sum: 95.25395607948303
time_elpased: 1.636
batch start
#iterations: 218
currently lose_sum: 94.72795128822327
time_elpased: 1.706
batch start
#iterations: 219
currently lose_sum: 95.41861152648926
time_elpased: 1.663
start validation test
0.658556701031
0.641504254735
0.721518987342
0.679163034002
0.658446161026
61.488
batch start
#iterations: 220
currently lose_sum: 95.12513959407806
time_elpased: 1.632
batch start
#iterations: 221
currently lose_sum: 95.34996849298477
time_elpased: 1.646
batch start
#iterations: 222
currently lose_sum: 95.06652545928955
time_elpased: 1.68
batch start
#iterations: 223
currently lose_sum: 94.79593753814697
time_elpased: 1.618
batch start
#iterations: 224
currently lose_sum: 95.11842340230942
time_elpased: 1.612
batch start
#iterations: 225
currently lose_sum: 95.72119623422623
time_elpased: 1.682
batch start
#iterations: 226
currently lose_sum: 94.72082668542862
time_elpased: 1.691
batch start
#iterations: 227
currently lose_sum: 94.85336756706238
time_elpased: 1.654
batch start
#iterations: 228
currently lose_sum: 94.99788051843643
time_elpased: 1.697
batch start
#iterations: 229
currently lose_sum: 95.15528702735901
time_elpased: 1.648
batch start
#iterations: 230
currently lose_sum: 95.35966056585312
time_elpased: 1.7
batch start
#iterations: 231
currently lose_sum: 94.84514236450195
time_elpased: 1.731
batch start
#iterations: 232
currently lose_sum: 95.05984735488892
time_elpased: 1.628
batch start
#iterations: 233
currently lose_sum: 95.28122693300247
time_elpased: 1.631
batch start
#iterations: 234
currently lose_sum: 95.27246958017349
time_elpased: 1.658
batch start
#iterations: 235
currently lose_sum: 94.66087520122528
time_elpased: 1.615
batch start
#iterations: 236
currently lose_sum: 95.37459480762482
time_elpased: 1.765
batch start
#iterations: 237
currently lose_sum: 95.25215917825699
time_elpased: 1.648
batch start
#iterations: 238
currently lose_sum: 95.2145865559578
time_elpased: 1.652
batch start
#iterations: 239
currently lose_sum: 95.18483990430832
time_elpased: 1.649
start validation test
0.651030927835
0.639681486397
0.69445302048
0.66594295865
0.650954693654
61.372
batch start
#iterations: 240
currently lose_sum: 94.84821939468384
time_elpased: 1.666
batch start
#iterations: 241
currently lose_sum: 95.19294595718384
time_elpased: 1.69
batch start
#iterations: 242
currently lose_sum: 95.40195590257645
time_elpased: 1.646
batch start
#iterations: 243
currently lose_sum: 95.18929743766785
time_elpased: 1.717
batch start
#iterations: 244
currently lose_sum: 95.0008881688118
time_elpased: 1.681
batch start
#iterations: 245
currently lose_sum: 95.30775940418243
time_elpased: 1.684
batch start
#iterations: 246
currently lose_sum: 95.45668417215347
time_elpased: 1.647
batch start
#iterations: 247
currently lose_sum: 94.83224868774414
time_elpased: 1.632
batch start
#iterations: 248
currently lose_sum: 94.84527987241745
time_elpased: 1.731
batch start
#iterations: 249
currently lose_sum: 95.3815245628357
time_elpased: 1.687
batch start
#iterations: 250
currently lose_sum: 94.8817309141159
time_elpased: 1.658
batch start
#iterations: 251
currently lose_sum: 95.1156234741211
time_elpased: 1.628
batch start
#iterations: 252
currently lose_sum: 95.39702069759369
time_elpased: 1.625
batch start
#iterations: 253
currently lose_sum: 94.90283101797104
time_elpased: 1.631
batch start
#iterations: 254
currently lose_sum: 95.15338224172592
time_elpased: 1.702
batch start
#iterations: 255
currently lose_sum: 95.18382531404495
time_elpased: 1.76
batch start
#iterations: 256
currently lose_sum: 94.64846783876419
time_elpased: 1.665
batch start
#iterations: 257
currently lose_sum: 95.04732811450958
time_elpased: 1.661
batch start
#iterations: 258
currently lose_sum: 95.01811254024506
time_elpased: 1.641
batch start
#iterations: 259
currently lose_sum: 94.71523177623749
time_elpased: 1.717
start validation test
0.659948453608
0.640692640693
0.731089842544
0.682912761355
0.659823553927
61.147
batch start
#iterations: 260
currently lose_sum: 94.75310283899307
time_elpased: 1.604
batch start
#iterations: 261
currently lose_sum: 94.69744092226028
time_elpased: 1.616
batch start
#iterations: 262
currently lose_sum: 95.18688893318176
time_elpased: 1.697
batch start
#iterations: 263
currently lose_sum: 95.40494793653488
time_elpased: 1.699
batch start
#iterations: 264
currently lose_sum: 94.99825149774551
time_elpased: 1.709
batch start
#iterations: 265
currently lose_sum: 95.0202853679657
time_elpased: 1.624
batch start
#iterations: 266
currently lose_sum: 94.94745534658432
time_elpased: 1.637
batch start
#iterations: 267
currently lose_sum: 95.01625001430511
time_elpased: 1.702
batch start
#iterations: 268
currently lose_sum: 94.98957341909409
time_elpased: 1.6
batch start
#iterations: 269
currently lose_sum: 95.12965786457062
time_elpased: 1.64
batch start
#iterations: 270
currently lose_sum: 95.0993817448616
time_elpased: 1.681
batch start
#iterations: 271
currently lose_sum: 94.80299067497253
time_elpased: 1.69
batch start
#iterations: 272
currently lose_sum: 94.7028900384903
time_elpased: 1.668
batch start
#iterations: 273
currently lose_sum: 94.79768168926239
time_elpased: 1.7
batch start
#iterations: 274
currently lose_sum: 95.18353599309921
time_elpased: 1.694
batch start
#iterations: 275
currently lose_sum: 95.13419020175934
time_elpased: 1.647
batch start
#iterations: 276
currently lose_sum: 94.93457221984863
time_elpased: 1.618
batch start
#iterations: 277
currently lose_sum: 94.80808424949646
time_elpased: 1.64
batch start
#iterations: 278
currently lose_sum: 94.73814392089844
time_elpased: 1.627
batch start
#iterations: 279
currently lose_sum: 94.99877214431763
time_elpased: 1.664
start validation test
0.658092783505
0.638943953866
0.729751981064
0.681335575306
0.657966974731
61.206
batch start
#iterations: 280
currently lose_sum: 95.17317467927933
time_elpased: 1.695
batch start
#iterations: 281
currently lose_sum: 94.89174515008926
time_elpased: 1.662
batch start
#iterations: 282
currently lose_sum: 94.71382164955139
time_elpased: 1.639
batch start
#iterations: 283
currently lose_sum: 95.08502596616745
time_elpased: 1.663
batch start
#iterations: 284
currently lose_sum: 94.86463099718094
time_elpased: 1.667
batch start
#iterations: 285
currently lose_sum: 94.94655865430832
time_elpased: 1.734
batch start
#iterations: 286
currently lose_sum: 94.7478409409523
time_elpased: 1.701
batch start
#iterations: 287
currently lose_sum: 94.72258996963501
time_elpased: 1.649
batch start
#iterations: 288
currently lose_sum: 95.08685207366943
time_elpased: 1.665
batch start
#iterations: 289
currently lose_sum: 94.89913350343704
time_elpased: 1.694
batch start
#iterations: 290
currently lose_sum: 94.72853130102158
time_elpased: 1.658
batch start
#iterations: 291
currently lose_sum: 95.0632176399231
time_elpased: 1.64
batch start
#iterations: 292
currently lose_sum: 95.15466541051865
time_elpased: 1.652
batch start
#iterations: 293
currently lose_sum: 95.00785678625107
time_elpased: 1.621
batch start
#iterations: 294
currently lose_sum: 95.2060496211052
time_elpased: 1.607
batch start
#iterations: 295
currently lose_sum: 95.32214671373367
time_elpased: 1.657
batch start
#iterations: 296
currently lose_sum: 94.87814998626709
time_elpased: 1.721
batch start
#iterations: 297
currently lose_sum: 94.95879685878754
time_elpased: 1.644
batch start
#iterations: 298
currently lose_sum: 95.06340384483337
time_elpased: 1.661
batch start
#iterations: 299
currently lose_sum: 94.70830178260803
time_elpased: 1.665
start validation test
0.66293814433
0.636794077135
0.761243182052
0.693479585619
0.662765554674
61.092
batch start
#iterations: 300
currently lose_sum: 95.03488701581955
time_elpased: 1.699
batch start
#iterations: 301
currently lose_sum: 95.09609228372574
time_elpased: 1.651
batch start
#iterations: 302
currently lose_sum: 94.77625268697739
time_elpased: 1.646
batch start
#iterations: 303
currently lose_sum: 95.04711943864822
time_elpased: 1.728
batch start
#iterations: 304
currently lose_sum: 95.42165648937225
time_elpased: 1.623
batch start
#iterations: 305
currently lose_sum: 94.93759328126907
time_elpased: 1.628
batch start
#iterations: 306
currently lose_sum: 94.79205644130707
time_elpased: 1.661
batch start
#iterations: 307
currently lose_sum: 94.83221119642258
time_elpased: 1.661
batch start
#iterations: 308
currently lose_sum: 94.37460833787918
time_elpased: 1.679
batch start
#iterations: 309
currently lose_sum: 94.84627670049667
time_elpased: 1.72
batch start
#iterations: 310
currently lose_sum: 95.3201869726181
time_elpased: 1.695
batch start
#iterations: 311
currently lose_sum: 94.89034110307693
time_elpased: 1.668
batch start
#iterations: 312
currently lose_sum: 94.91370499134064
time_elpased: 1.667
batch start
#iterations: 313
currently lose_sum: 94.8986109495163
time_elpased: 1.648
batch start
#iterations: 314
currently lose_sum: 94.96988385915756
time_elpased: 1.64
batch start
#iterations: 315
currently lose_sum: 94.74241608381271
time_elpased: 1.666
batch start
#iterations: 316
currently lose_sum: 94.82037061452866
time_elpased: 1.641
batch start
#iterations: 317
currently lose_sum: 94.74301159381866
time_elpased: 1.719
batch start
#iterations: 318
currently lose_sum: 94.91326528787613
time_elpased: 1.691
batch start
#iterations: 319
currently lose_sum: 94.80730140209198
time_elpased: 1.659
start validation test
0.65793814433
0.640313325439
0.723474323351
0.679358330112
0.657823085459
61.269
batch start
#iterations: 320
currently lose_sum: 94.64289635419846
time_elpased: 1.681
batch start
#iterations: 321
currently lose_sum: 95.00676518678665
time_elpased: 1.634
batch start
#iterations: 322
currently lose_sum: 95.1332756280899
time_elpased: 1.711
batch start
#iterations: 323
currently lose_sum: 94.88423097133636
time_elpased: 1.628
batch start
#iterations: 324
currently lose_sum: 94.24216437339783
time_elpased: 1.68
batch start
#iterations: 325
currently lose_sum: 95.0882898569107
time_elpased: 1.676
batch start
#iterations: 326
currently lose_sum: 94.35864335298538
time_elpased: 1.653
batch start
#iterations: 327
currently lose_sum: 95.0177361369133
time_elpased: 1.648
batch start
#iterations: 328
currently lose_sum: 94.91484385728836
time_elpased: 1.666
batch start
#iterations: 329
currently lose_sum: 94.57043677568436
time_elpased: 1.66
batch start
#iterations: 330
currently lose_sum: 95.19787746667862
time_elpased: 1.682
batch start
#iterations: 331
currently lose_sum: 94.73288953304291
time_elpased: 1.62
batch start
#iterations: 332
currently lose_sum: 94.70026022195816
time_elpased: 1.723
batch start
#iterations: 333
currently lose_sum: 94.8135941028595
time_elpased: 1.686
batch start
#iterations: 334
currently lose_sum: 94.63466787338257
time_elpased: 1.72
batch start
#iterations: 335
currently lose_sum: 94.8019728064537
time_elpased: 1.649
batch start
#iterations: 336
currently lose_sum: 94.76461219787598
time_elpased: 1.65
batch start
#iterations: 337
currently lose_sum: 95.32144463062286
time_elpased: 1.689
batch start
#iterations: 338
currently lose_sum: 94.81515419483185
time_elpased: 1.694
batch start
#iterations: 339
currently lose_sum: 94.87958776950836
time_elpased: 1.693
start validation test
0.658350515464
0.638607197344
0.732324791602
0.682262703739
0.658220642212
61.221
batch start
#iterations: 340
currently lose_sum: 94.64648360013962
time_elpased: 1.715
batch start
#iterations: 341
currently lose_sum: 94.8810984492302
time_elpased: 1.595
batch start
#iterations: 342
currently lose_sum: 95.1398486495018
time_elpased: 1.659
batch start
#iterations: 343
currently lose_sum: 94.80052816867828
time_elpased: 1.633
batch start
#iterations: 344
currently lose_sum: 94.53214061260223
time_elpased: 1.648
batch start
#iterations: 345
currently lose_sum: 94.96476113796234
time_elpased: 1.65
batch start
#iterations: 346
currently lose_sum: 94.78772336244583
time_elpased: 1.656
batch start
#iterations: 347
currently lose_sum: 94.65645909309387
time_elpased: 1.687
batch start
#iterations: 348
currently lose_sum: 94.81137990951538
time_elpased: 1.661
batch start
#iterations: 349
currently lose_sum: 95.33473908901215
time_elpased: 1.703
batch start
#iterations: 350
currently lose_sum: 94.71392548084259
time_elpased: 1.707
batch start
#iterations: 351
currently lose_sum: 94.92708492279053
time_elpased: 1.686
batch start
#iterations: 352
currently lose_sum: 94.72771233320236
time_elpased: 1.657
batch start
#iterations: 353
currently lose_sum: 94.59721368551254
time_elpased: 1.694
batch start
#iterations: 354
currently lose_sum: 95.28936892747879
time_elpased: 1.706
batch start
#iterations: 355
currently lose_sum: 94.71286815404892
time_elpased: 1.732
batch start
#iterations: 356
currently lose_sum: 94.80910444259644
time_elpased: 1.638
batch start
#iterations: 357
currently lose_sum: 94.75535869598389
time_elpased: 1.697
batch start
#iterations: 358
currently lose_sum: 94.78135681152344
time_elpased: 1.671
batch start
#iterations: 359
currently lose_sum: 94.70094734430313
time_elpased: 1.637
start validation test
0.65706185567
0.641878125579
0.713285993619
0.6757007068
0.656963145524
61.326
batch start
#iterations: 360
currently lose_sum: 95.16188961267471
time_elpased: 1.66
batch start
#iterations: 361
currently lose_sum: 95.11242914199829
time_elpased: 1.752
batch start
#iterations: 362
currently lose_sum: 94.64566969871521
time_elpased: 1.639
batch start
#iterations: 363
currently lose_sum: 94.74667453765869
time_elpased: 1.679
batch start
#iterations: 364
currently lose_sum: 94.91607254743576
time_elpased: 1.624
batch start
#iterations: 365
currently lose_sum: 95.12887585163116
time_elpased: 1.635
batch start
#iterations: 366
currently lose_sum: 94.74426108598709
time_elpased: 1.625
batch start
#iterations: 367
currently lose_sum: 94.89998012781143
time_elpased: 1.658
batch start
#iterations: 368
currently lose_sum: 94.99721640348434
time_elpased: 1.707
batch start
#iterations: 369
currently lose_sum: 94.68840432167053
time_elpased: 1.721
batch start
#iterations: 370
currently lose_sum: 94.5790941119194
time_elpased: 1.628
batch start
#iterations: 371
currently lose_sum: 94.99519300460815
time_elpased: 1.663
batch start
#iterations: 372
currently lose_sum: 94.50021928548813
time_elpased: 1.667
batch start
#iterations: 373
currently lose_sum: 94.93392992019653
time_elpased: 1.618
batch start
#iterations: 374
currently lose_sum: 94.98365432024002
time_elpased: 1.682
batch start
#iterations: 375
currently lose_sum: 94.49489861726761
time_elpased: 1.656
batch start
#iterations: 376
currently lose_sum: 95.01544576883316
time_elpased: 1.665
batch start
#iterations: 377
currently lose_sum: 95.06110602617264
time_elpased: 1.636
batch start
#iterations: 378
currently lose_sum: 94.86224436759949
time_elpased: 1.647
batch start
#iterations: 379
currently lose_sum: 94.7219187617302
time_elpased: 1.669
start validation test
0.661597938144
0.636995827538
0.754142224967
0.690636633523
0.661435462375
61.107
batch start
#iterations: 380
currently lose_sum: 95.14039295911789
time_elpased: 1.679
batch start
#iterations: 381
currently lose_sum: 94.88666242361069
time_elpased: 1.62
batch start
#iterations: 382
currently lose_sum: 94.80664247274399
time_elpased: 1.629
batch start
#iterations: 383
currently lose_sum: 94.96241223812103
time_elpased: 1.601
batch start
#iterations: 384
currently lose_sum: 95.32703292369843
time_elpased: 1.597
batch start
#iterations: 385
currently lose_sum: 95.06342834234238
time_elpased: 1.666
batch start
#iterations: 386
currently lose_sum: 94.52637058496475
time_elpased: 1.667
batch start
#iterations: 387
currently lose_sum: 95.18721115589142
time_elpased: 1.681
batch start
#iterations: 388
currently lose_sum: 94.86786276102066
time_elpased: 1.622
batch start
#iterations: 389
currently lose_sum: 94.8842021226883
time_elpased: 1.653
batch start
#iterations: 390
currently lose_sum: 94.8621677160263
time_elpased: 1.727
batch start
#iterations: 391
currently lose_sum: 94.91776847839355
time_elpased: 1.699
batch start
#iterations: 392
currently lose_sum: 94.70685636997223
time_elpased: 1.609
batch start
#iterations: 393
currently lose_sum: 94.79452365636826
time_elpased: 1.677
batch start
#iterations: 394
currently lose_sum: 94.61274755001068
time_elpased: 1.624
batch start
#iterations: 395
currently lose_sum: 94.66988241672516
time_elpased: 1.688
batch start
#iterations: 396
currently lose_sum: 94.76399177312851
time_elpased: 1.653
batch start
#iterations: 397
currently lose_sum: 94.93857628107071
time_elpased: 1.693
batch start
#iterations: 398
currently lose_sum: 94.70713812112808
time_elpased: 1.7
batch start
#iterations: 399
currently lose_sum: 94.94531381130219
time_elpased: 1.694
start validation test
0.65793814433
0.640441243504
0.722959761243
0.679203325921
0.657823988853
61.287
acc: 0.658
pre: 0.634
rec: 0.753
F1: 0.688
auc: 0.694
