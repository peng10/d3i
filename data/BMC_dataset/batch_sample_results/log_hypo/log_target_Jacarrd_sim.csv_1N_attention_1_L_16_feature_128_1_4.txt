start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.46950787305832
time_elpased: 1.753
batch start
#iterations: 1
currently lose_sum: 99.91619914770126
time_elpased: 1.752
batch start
#iterations: 2
currently lose_sum: 99.91878271102905
time_elpased: 1.762
batch start
#iterations: 3
currently lose_sum: 99.46207708120346
time_elpased: 1.762
batch start
#iterations: 4
currently lose_sum: 99.51044529676437
time_elpased: 1.758
batch start
#iterations: 5
currently lose_sum: 99.0421707034111
time_elpased: 1.754
batch start
#iterations: 6
currently lose_sum: 98.95080119371414
time_elpased: 1.77
batch start
#iterations: 7
currently lose_sum: 98.50564277172089
time_elpased: 1.785
batch start
#iterations: 8
currently lose_sum: 98.47755968570709
time_elpased: 1.754
batch start
#iterations: 9
currently lose_sum: 98.18345099687576
time_elpased: 1.731
batch start
#iterations: 10
currently lose_sum: 98.0132200717926
time_elpased: 1.738
batch start
#iterations: 11
currently lose_sum: 98.01120489835739
time_elpased: 1.779
batch start
#iterations: 12
currently lose_sum: 97.85106593370438
time_elpased: 1.774
batch start
#iterations: 13
currently lose_sum: 97.80017149448395
time_elpased: 1.767
batch start
#iterations: 14
currently lose_sum: 97.68053829669952
time_elpased: 1.725
batch start
#iterations: 15
currently lose_sum: 97.65930736064911
time_elpased: 1.774
batch start
#iterations: 16
currently lose_sum: 97.63679778575897
time_elpased: 1.784
batch start
#iterations: 17
currently lose_sum: 97.3632623553276
time_elpased: 1.787
batch start
#iterations: 18
currently lose_sum: 97.35591977834702
time_elpased: 1.762
batch start
#iterations: 19
currently lose_sum: 97.40863370895386
time_elpased: 1.725
start validation test
0.64118556701
0.641246412464
0.643820109087
0.642530683511
0.641180941665
62.886
batch start
#iterations: 20
currently lose_sum: 97.62480372190475
time_elpased: 1.762
batch start
#iterations: 21
currently lose_sum: 96.97149187326431
time_elpased: 1.77
batch start
#iterations: 22
currently lose_sum: 97.15952986478806
time_elpased: 1.743
batch start
#iterations: 23
currently lose_sum: 97.36667865514755
time_elpased: 1.766
batch start
#iterations: 24
currently lose_sum: 97.10756331682205
time_elpased: 1.758
batch start
#iterations: 25
currently lose_sum: 97.02537876367569
time_elpased: 1.766
batch start
#iterations: 26
currently lose_sum: 97.26839458942413
time_elpased: 1.744
batch start
#iterations: 27
currently lose_sum: 96.62573528289795
time_elpased: 1.733
batch start
#iterations: 28
currently lose_sum: 96.69573348760605
time_elpased: 1.757
batch start
#iterations: 29
currently lose_sum: 96.64595484733582
time_elpased: 1.757
batch start
#iterations: 30
currently lose_sum: 96.97046160697937
time_elpased: 1.76
batch start
#iterations: 31
currently lose_sum: 96.69118368625641
time_elpased: 1.745
batch start
#iterations: 32
currently lose_sum: 96.78161209821701
time_elpased: 1.783
batch start
#iterations: 33
currently lose_sum: 96.65967899560928
time_elpased: 1.742
batch start
#iterations: 34
currently lose_sum: 96.86627405881882
time_elpased: 1.816
batch start
#iterations: 35
currently lose_sum: 96.24813038110733
time_elpased: 1.787
batch start
#iterations: 36
currently lose_sum: 96.60802489519119
time_elpased: 1.722
batch start
#iterations: 37
currently lose_sum: 96.46033895015717
time_elpased: 1.737
batch start
#iterations: 38
currently lose_sum: 96.61078703403473
time_elpased: 1.782
batch start
#iterations: 39
currently lose_sum: 96.57073253393173
time_elpased: 1.756
start validation test
0.646134020619
0.645302628897
0.651744365545
0.648507500896
0.646124170793
62.186
batch start
#iterations: 40
currently lose_sum: 96.81421595811844
time_elpased: 1.743
batch start
#iterations: 41
currently lose_sum: 96.40322744846344
time_elpased: 1.767
batch start
#iterations: 42
currently lose_sum: 96.25727927684784
time_elpased: 1.752
batch start
#iterations: 43
currently lose_sum: 96.45530015230179
time_elpased: 1.758
batch start
#iterations: 44
currently lose_sum: 96.32234489917755
time_elpased: 1.788
batch start
#iterations: 45
currently lose_sum: 96.30026352405548
time_elpased: 1.777
batch start
#iterations: 46
currently lose_sum: 96.28835773468018
time_elpased: 1.743
batch start
#iterations: 47
currently lose_sum: 96.48344737291336
time_elpased: 1.769
batch start
#iterations: 48
currently lose_sum: 96.35337340831757
time_elpased: 1.756
batch start
#iterations: 49
currently lose_sum: 96.27081829309464
time_elpased: 1.807
batch start
#iterations: 50
currently lose_sum: 96.0154379606247
time_elpased: 1.769
batch start
#iterations: 51
currently lose_sum: 95.89324188232422
time_elpased: 1.807
batch start
#iterations: 52
currently lose_sum: 96.15493422746658
time_elpased: 1.742
batch start
#iterations: 53
currently lose_sum: 95.84608107805252
time_elpased: 1.776
batch start
#iterations: 54
currently lose_sum: 96.17189079523087
time_elpased: 1.785
batch start
#iterations: 55
currently lose_sum: 95.93797117471695
time_elpased: 1.78
batch start
#iterations: 56
currently lose_sum: 96.10008639097214
time_elpased: 1.762
batch start
#iterations: 57
currently lose_sum: 96.06695932149887
time_elpased: 1.795
batch start
#iterations: 58
currently lose_sum: 96.07856911420822
time_elpased: 1.745
batch start
#iterations: 59
currently lose_sum: 96.42312157154083
time_elpased: 1.745
start validation test
0.636597938144
0.638603055815
0.632293917876
0.635432826559
0.636605494516
62.033
batch start
#iterations: 60
currently lose_sum: 96.35966473817825
time_elpased: 1.786
batch start
#iterations: 61
currently lose_sum: 96.16858839988708
time_elpased: 1.747
batch start
#iterations: 62
currently lose_sum: 95.84780949354172
time_elpased: 1.742
batch start
#iterations: 63
currently lose_sum: 95.94046938419342
time_elpased: 1.802
batch start
#iterations: 64
currently lose_sum: 96.01578050851822
time_elpased: 1.776
batch start
#iterations: 65
currently lose_sum: 95.90219742059708
time_elpased: 1.78
batch start
#iterations: 66
currently lose_sum: 96.09929591417313
time_elpased: 1.736
batch start
#iterations: 67
currently lose_sum: 95.917167365551
time_elpased: 1.776
batch start
#iterations: 68
currently lose_sum: 95.5747429728508
time_elpased: 1.785
batch start
#iterations: 69
currently lose_sum: 95.73325824737549
time_elpased: 1.785
batch start
#iterations: 70
currently lose_sum: 95.98489129543304
time_elpased: 1.745
batch start
#iterations: 71
currently lose_sum: 95.91577714681625
time_elpased: 1.746
batch start
#iterations: 72
currently lose_sum: 95.56215351819992
time_elpased: 1.72
batch start
#iterations: 73
currently lose_sum: 95.89472603797913
time_elpased: 1.744
batch start
#iterations: 74
currently lose_sum: 95.53454995155334
time_elpased: 1.746
batch start
#iterations: 75
currently lose_sum: 95.94417941570282
time_elpased: 1.741
batch start
#iterations: 76
currently lose_sum: 95.59891414642334
time_elpased: 1.772
batch start
#iterations: 77
currently lose_sum: 95.98476302623749
time_elpased: 1.757
batch start
#iterations: 78
currently lose_sum: 96.11724573373795
time_elpased: 1.782
batch start
#iterations: 79
currently lose_sum: 95.59482020139694
time_elpased: 1.786
start validation test
0.657216494845
0.637793153024
0.730472368015
0.680993955675
0.657087882861
61.471
batch start
#iterations: 80
currently lose_sum: 95.37410873174667
time_elpased: 1.769
batch start
#iterations: 81
currently lose_sum: 95.61505478620529
time_elpased: 1.742
batch start
#iterations: 82
currently lose_sum: 95.9960086941719
time_elpased: 1.759
batch start
#iterations: 83
currently lose_sum: 95.39833784103394
time_elpased: 1.801
batch start
#iterations: 84
currently lose_sum: 95.65974843502045
time_elpased: 1.75
batch start
#iterations: 85
currently lose_sum: 95.78080320358276
time_elpased: 1.808
batch start
#iterations: 86
currently lose_sum: 96.0582429766655
time_elpased: 1.766
batch start
#iterations: 87
currently lose_sum: 95.67273944616318
time_elpased: 1.756
batch start
#iterations: 88
currently lose_sum: 95.70117431879044
time_elpased: 1.791
batch start
#iterations: 89
currently lose_sum: 95.61946922540665
time_elpased: 1.73
batch start
#iterations: 90
currently lose_sum: 95.49888980388641
time_elpased: 1.743
batch start
#iterations: 91
currently lose_sum: 96.13836175203323
time_elpased: 1.751
batch start
#iterations: 92
currently lose_sum: 95.64621365070343
time_elpased: 1.747
batch start
#iterations: 93
currently lose_sum: 95.43198889493942
time_elpased: 1.793
batch start
#iterations: 94
currently lose_sum: 95.2338462471962
time_elpased: 1.76
batch start
#iterations: 95
currently lose_sum: 95.83279848098755
time_elpased: 1.766
batch start
#iterations: 96
currently lose_sum: 95.68769955635071
time_elpased: 1.756
batch start
#iterations: 97
currently lose_sum: 95.84379297494888
time_elpased: 1.784
batch start
#iterations: 98
currently lose_sum: 95.84288418292999
time_elpased: 1.784
batch start
#iterations: 99
currently lose_sum: 95.90110903978348
time_elpased: 1.777
start validation test
0.647628865979
0.641600314558
0.671709375322
0.656309703369
0.647586588931
61.520
batch start
#iterations: 100
currently lose_sum: 95.44842779636383
time_elpased: 1.742
batch start
#iterations: 101
currently lose_sum: 95.68288916349411
time_elpased: 1.731
batch start
#iterations: 102
currently lose_sum: 95.3790380358696
time_elpased: 1.764
batch start
#iterations: 103
currently lose_sum: 95.57396560907364
time_elpased: 1.751
batch start
#iterations: 104
currently lose_sum: 95.46307599544525
time_elpased: 1.749
batch start
#iterations: 105
currently lose_sum: 95.58914291858673
time_elpased: 1.781
batch start
#iterations: 106
currently lose_sum: 95.71959275007248
time_elpased: 1.754
batch start
#iterations: 107
currently lose_sum: 95.48008304834366
time_elpased: 1.733
batch start
#iterations: 108
currently lose_sum: 95.64462995529175
time_elpased: 1.812
batch start
#iterations: 109
currently lose_sum: 95.97925835847855
time_elpased: 1.775
batch start
#iterations: 110
currently lose_sum: 95.48442888259888
time_elpased: 1.755
batch start
#iterations: 111
currently lose_sum: 95.45791590213776
time_elpased: 1.796
batch start
#iterations: 112
currently lose_sum: 95.51185309886932
time_elpased: 1.754
batch start
#iterations: 113
currently lose_sum: 95.20054441690445
time_elpased: 1.78
batch start
#iterations: 114
currently lose_sum: 95.74457782506943
time_elpased: 1.771
batch start
#iterations: 115
currently lose_sum: 95.1184196472168
time_elpased: 1.729
batch start
#iterations: 116
currently lose_sum: 95.37817972898483
time_elpased: 1.752
batch start
#iterations: 117
currently lose_sum: 95.25014585256577
time_elpased: 1.798
batch start
#iterations: 118
currently lose_sum: 95.52279549837112
time_elpased: 1.773
batch start
#iterations: 119
currently lose_sum: 95.22407668828964
time_elpased: 1.745
start validation test
0.654175257732
0.643347312238
0.694658845323
0.668019199367
0.65410418255
61.321
batch start
#iterations: 120
currently lose_sum: 95.04652804136276
time_elpased: 1.796
batch start
#iterations: 121
currently lose_sum: 95.55156594514847
time_elpased: 1.821
batch start
#iterations: 122
currently lose_sum: 95.75803524255753
time_elpased: 1.778
batch start
#iterations: 123
currently lose_sum: 95.23057812452316
time_elpased: 1.733
batch start
#iterations: 124
currently lose_sum: 95.45576560497284
time_elpased: 1.805
batch start
#iterations: 125
currently lose_sum: 95.09996551275253
time_elpased: 1.773
batch start
#iterations: 126
currently lose_sum: 95.11805295944214
time_elpased: 1.761
batch start
#iterations: 127
currently lose_sum: 95.45155370235443
time_elpased: 1.781
batch start
#iterations: 128
currently lose_sum: 95.20380508899689
time_elpased: 1.792
batch start
#iterations: 129
currently lose_sum: 95.10842251777649
time_elpased: 1.778
batch start
#iterations: 130
currently lose_sum: 95.29583662748337
time_elpased: 1.781
batch start
#iterations: 131
currently lose_sum: 95.33026146888733
time_elpased: 1.802
batch start
#iterations: 132
currently lose_sum: 95.59922814369202
time_elpased: 1.757
batch start
#iterations: 133
currently lose_sum: 95.65622395277023
time_elpased: 1.777
batch start
#iterations: 134
currently lose_sum: 95.5534576177597
time_elpased: 1.752
batch start
#iterations: 135
currently lose_sum: 95.45864194631577
time_elpased: 1.74
batch start
#iterations: 136
currently lose_sum: 95.16312551498413
time_elpased: 1.778
batch start
#iterations: 137
currently lose_sum: 95.47737950086594
time_elpased: 1.803
batch start
#iterations: 138
currently lose_sum: 95.23185133934021
time_elpased: 1.755
batch start
#iterations: 139
currently lose_sum: 95.64657038450241
time_elpased: 1.756
start validation test
0.657422680412
0.642083834552
0.714109292992
0.676183979731
0.657323158321
61.428
batch start
#iterations: 140
currently lose_sum: 95.52091813087463
time_elpased: 1.746
batch start
#iterations: 141
currently lose_sum: 95.2994334101677
time_elpased: 1.762
batch start
#iterations: 142
currently lose_sum: 95.55713379383087
time_elpased: 1.76
batch start
#iterations: 143
currently lose_sum: 95.34827262163162
time_elpased: 1.742
batch start
#iterations: 144
currently lose_sum: 95.21880716085434
time_elpased: 1.76
batch start
#iterations: 145
currently lose_sum: 95.4371200799942
time_elpased: 1.746
batch start
#iterations: 146
currently lose_sum: 95.01251113414764
time_elpased: 1.777
batch start
#iterations: 147
currently lose_sum: 95.48085719347
time_elpased: 1.768
batch start
#iterations: 148
currently lose_sum: 95.51682662963867
time_elpased: 1.804
batch start
#iterations: 149
currently lose_sum: 95.47204393148422
time_elpased: 1.741
batch start
#iterations: 150
currently lose_sum: 95.18095713853836
time_elpased: 1.744
batch start
#iterations: 151
currently lose_sum: 95.66660928726196
time_elpased: 1.812
batch start
#iterations: 152
currently lose_sum: 95.10555416345596
time_elpased: 1.808
batch start
#iterations: 153
currently lose_sum: 95.5417508482933
time_elpased: 1.758
batch start
#iterations: 154
currently lose_sum: 95.02500122785568
time_elpased: 1.778
batch start
#iterations: 155
currently lose_sum: 95.1366376876831
time_elpased: 1.819
batch start
#iterations: 156
currently lose_sum: 95.56464183330536
time_elpased: 1.748
batch start
#iterations: 157
currently lose_sum: 95.46188819408417
time_elpased: 1.783
batch start
#iterations: 158
currently lose_sum: 95.48704332113266
time_elpased: 1.77
batch start
#iterations: 159
currently lose_sum: 95.34225976467133
time_elpased: 1.756
start validation test
0.652268041237
0.641705618621
0.692291859627
0.66603960396
0.652197773251
61.458
batch start
#iterations: 160
currently lose_sum: 95.23343181610107
time_elpased: 1.741
batch start
#iterations: 161
currently lose_sum: 95.14751553535461
time_elpased: 1.777
batch start
#iterations: 162
currently lose_sum: 95.22528839111328
time_elpased: 1.757
batch start
#iterations: 163
currently lose_sum: 95.27693992853165
time_elpased: 1.772
batch start
#iterations: 164
currently lose_sum: 95.64911085367203
time_elpased: 1.776
batch start
#iterations: 165
currently lose_sum: 95.20995980501175
time_elpased: 1.794
batch start
#iterations: 166
currently lose_sum: 95.13570368289948
time_elpased: 1.767
batch start
#iterations: 167
currently lose_sum: 95.2664402127266
time_elpased: 1.753
batch start
#iterations: 168
currently lose_sum: 95.60905975103378
time_elpased: 1.745
batch start
#iterations: 169
currently lose_sum: 95.0583079457283
time_elpased: 1.752
batch start
#iterations: 170
currently lose_sum: 94.81623792648315
time_elpased: 1.738
batch start
#iterations: 171
currently lose_sum: 94.91690331697464
time_elpased: 1.797
batch start
#iterations: 172
currently lose_sum: 95.05367946624756
time_elpased: 1.766
batch start
#iterations: 173
currently lose_sum: 95.14198392629623
time_elpased: 1.814
batch start
#iterations: 174
currently lose_sum: 95.55009788274765
time_elpased: 1.776
batch start
#iterations: 175
currently lose_sum: 95.3930476307869
time_elpased: 1.772
batch start
#iterations: 176
currently lose_sum: 95.42910045385361
time_elpased: 1.753
batch start
#iterations: 177
currently lose_sum: 95.15449631214142
time_elpased: 1.806
batch start
#iterations: 178
currently lose_sum: 95.16571033000946
time_elpased: 1.757
batch start
#iterations: 179
currently lose_sum: 95.15399926900864
time_elpased: 1.753
start validation test
0.660103092784
0.644116289802
0.718225789853
0.679155313351
0.660001049424
61.186
batch start
#iterations: 180
currently lose_sum: 95.23668986558914
time_elpased: 1.755
batch start
#iterations: 181
currently lose_sum: 95.36100196838379
time_elpased: 1.784
batch start
#iterations: 182
currently lose_sum: 94.68237578868866
time_elpased: 1.774
batch start
#iterations: 183
currently lose_sum: 94.98779183626175
time_elpased: 1.817
batch start
#iterations: 184
currently lose_sum: 95.21824967861176
time_elpased: 1.745
batch start
#iterations: 185
currently lose_sum: 95.10231441259384
time_elpased: 1.794
batch start
#iterations: 186
currently lose_sum: 95.00527656078339
time_elpased: 1.744
batch start
#iterations: 187
currently lose_sum: 95.28434270620346
time_elpased: 1.753
batch start
#iterations: 188
currently lose_sum: 95.10086220502853
time_elpased: 1.763
batch start
#iterations: 189
currently lose_sum: 94.97718089818954
time_elpased: 1.8
batch start
#iterations: 190
currently lose_sum: 95.12252289056778
time_elpased: 1.742
batch start
#iterations: 191
currently lose_sum: 95.30016165971756
time_elpased: 1.745
batch start
#iterations: 192
currently lose_sum: 95.42337626218796
time_elpased: 1.731
batch start
#iterations: 193
currently lose_sum: 95.29962658882141
time_elpased: 1.732
batch start
#iterations: 194
currently lose_sum: 94.82902586460114
time_elpased: 1.745
batch start
#iterations: 195
currently lose_sum: 94.99185222387314
time_elpased: 1.753
batch start
#iterations: 196
currently lose_sum: 94.88114374876022
time_elpased: 1.758
batch start
#iterations: 197
currently lose_sum: 95.00219225883484
time_elpased: 1.749
batch start
#iterations: 198
currently lose_sum: 95.22148674726486
time_elpased: 1.757
batch start
#iterations: 199
currently lose_sum: 95.15599477291107
time_elpased: 1.791
start validation test
0.657371134021
0.643619011976
0.7079345477
0.674246508209
0.657282362149
61.506
batch start
#iterations: 200
currently lose_sum: 95.00087034702301
time_elpased: 1.775
batch start
#iterations: 201
currently lose_sum: 94.98704594373703
time_elpased: 1.766
batch start
#iterations: 202
currently lose_sum: 94.84633642435074
time_elpased: 1.771
batch start
#iterations: 203
currently lose_sum: 95.1379988193512
time_elpased: 1.764
batch start
#iterations: 204
currently lose_sum: 94.82989639043808
time_elpased: 1.805
batch start
#iterations: 205
currently lose_sum: 94.86000144481659
time_elpased: 1.761
batch start
#iterations: 206
currently lose_sum: 95.27302557229996
time_elpased: 1.774
batch start
#iterations: 207
currently lose_sum: 95.04447388648987
time_elpased: 1.772
batch start
#iterations: 208
currently lose_sum: 94.92543530464172
time_elpased: 1.729
batch start
#iterations: 209
currently lose_sum: 94.74133342504501
time_elpased: 1.781
batch start
#iterations: 210
currently lose_sum: 94.79678428173065
time_elpased: 1.75
batch start
#iterations: 211
currently lose_sum: 94.62112933397293
time_elpased: 1.748
batch start
#iterations: 212
currently lose_sum: 94.661552131176
time_elpased: 1.734
batch start
#iterations: 213
currently lose_sum: 94.93501818180084
time_elpased: 1.79
batch start
#iterations: 214
currently lose_sum: 95.2826754450798
time_elpased: 1.758
batch start
#iterations: 215
currently lose_sum: 95.14544779062271
time_elpased: 1.78
batch start
#iterations: 216
currently lose_sum: 94.83627808094025
time_elpased: 1.77
batch start
#iterations: 217
currently lose_sum: 95.00127077102661
time_elpased: 1.763
batch start
#iterations: 218
currently lose_sum: 95.06609237194061
time_elpased: 1.747
batch start
#iterations: 219
currently lose_sum: 94.93687784671783
time_elpased: 1.775
start validation test
0.652422680412
0.641081593928
0.695379232273
0.667127412746
0.65234726356
61.329
batch start
#iterations: 220
currently lose_sum: 94.80111867189407
time_elpased: 1.763
batch start
#iterations: 221
currently lose_sum: 95.34817224740982
time_elpased: 1.745
batch start
#iterations: 222
currently lose_sum: 95.05267006158829
time_elpased: 1.769
batch start
#iterations: 223
currently lose_sum: 95.19034683704376
time_elpased: 1.799
batch start
#iterations: 224
currently lose_sum: 95.02877599000931
time_elpased: 1.748
batch start
#iterations: 225
currently lose_sum: 94.77224749326706
time_elpased: 1.806
batch start
#iterations: 226
currently lose_sum: 94.74096006155014
time_elpased: 1.744
batch start
#iterations: 227
currently lose_sum: 94.87083649635315
time_elpased: 1.759
batch start
#iterations: 228
currently lose_sum: 95.05991220474243
time_elpased: 1.731
batch start
#iterations: 229
currently lose_sum: 95.17002910375595
time_elpased: 1.783
batch start
#iterations: 230
currently lose_sum: 94.8419418334961
time_elpased: 1.762
batch start
#iterations: 231
currently lose_sum: 94.79860883951187
time_elpased: 1.743
batch start
#iterations: 232
currently lose_sum: 95.1233075261116
time_elpased: 1.768
batch start
#iterations: 233
currently lose_sum: 94.77882939577103
time_elpased: 1.759
batch start
#iterations: 234
currently lose_sum: 95.14095336198807
time_elpased: 1.752
batch start
#iterations: 235
currently lose_sum: 95.08769106864929
time_elpased: 1.748
batch start
#iterations: 236
currently lose_sum: 95.05347937345505
time_elpased: 1.736
batch start
#iterations: 237
currently lose_sum: 95.0920559167862
time_elpased: 1.774
batch start
#iterations: 238
currently lose_sum: 95.03681701421738
time_elpased: 1.746
batch start
#iterations: 239
currently lose_sum: 95.09550148248672
time_elpased: 1.769
start validation test
0.65324742268
0.642353837364
0.694247195637
0.667293140116
0.653175441255
61.300
batch start
#iterations: 240
currently lose_sum: 95.06762158870697
time_elpased: 1.737
batch start
#iterations: 241
currently lose_sum: 95.01307249069214
time_elpased: 1.777
batch start
#iterations: 242
currently lose_sum: 95.00582075119019
time_elpased: 1.736
batch start
#iterations: 243
currently lose_sum: 94.93623220920563
time_elpased: 1.775
batch start
#iterations: 244
currently lose_sum: 95.1686230301857
time_elpased: 1.739
batch start
#iterations: 245
currently lose_sum: 94.9794293642044
time_elpased: 1.74
batch start
#iterations: 246
currently lose_sum: 95.08161050081253
time_elpased: 1.8
batch start
#iterations: 247
currently lose_sum: 94.819793343544
time_elpased: 1.799
batch start
#iterations: 248
currently lose_sum: 95.1750659942627
time_elpased: 1.73
batch start
#iterations: 249
currently lose_sum: 94.9458264708519
time_elpased: 1.766
batch start
#iterations: 250
currently lose_sum: 94.82826113700867
time_elpased: 1.746
batch start
#iterations: 251
currently lose_sum: 94.82819324731827
time_elpased: 1.812
batch start
#iterations: 252
currently lose_sum: 95.03869545459747
time_elpased: 1.777
batch start
#iterations: 253
currently lose_sum: 94.73740482330322
time_elpased: 1.782
batch start
#iterations: 254
currently lose_sum: 94.60815060138702
time_elpased: 1.742
batch start
#iterations: 255
currently lose_sum: 95.41512662172318
time_elpased: 1.737
batch start
#iterations: 256
currently lose_sum: 94.87423175573349
time_elpased: 1.742
batch start
#iterations: 257
currently lose_sum: 95.19440460205078
time_elpased: 1.782
batch start
#iterations: 258
currently lose_sum: 94.71367985010147
time_elpased: 1.781
batch start
#iterations: 259
currently lose_sum: 94.75368893146515
time_elpased: 1.74
start validation test
0.658505154639
0.638035714286
0.735412164248
0.683271979729
0.658370132522
61.208
batch start
#iterations: 260
currently lose_sum: 95.3000453710556
time_elpased: 1.74
batch start
#iterations: 261
currently lose_sum: 94.27334648370743
time_elpased: 1.744
batch start
#iterations: 262
currently lose_sum: 94.86781620979309
time_elpased: 1.759
batch start
#iterations: 263
currently lose_sum: 94.9422556757927
time_elpased: 1.754
batch start
#iterations: 264
currently lose_sum: 95.28278559446335
time_elpased: 1.779
batch start
#iterations: 265
currently lose_sum: 94.86875224113464
time_elpased: 1.728
batch start
#iterations: 266
currently lose_sum: 95.10943299531937
time_elpased: 1.772
batch start
#iterations: 267
currently lose_sum: 94.98920720815659
time_elpased: 1.712
batch start
#iterations: 268
currently lose_sum: 95.12204211950302
time_elpased: 1.745
batch start
#iterations: 269
currently lose_sum: 94.86924773454666
time_elpased: 1.826
batch start
#iterations: 270
currently lose_sum: 94.91563421487808
time_elpased: 1.757
batch start
#iterations: 271
currently lose_sum: 94.52686899900436
time_elpased: 1.748
batch start
#iterations: 272
currently lose_sum: 95.3128753900528
time_elpased: 1.758
batch start
#iterations: 273
currently lose_sum: 94.95895266532898
time_elpased: 1.849
batch start
#iterations: 274
currently lose_sum: 95.30504429340363
time_elpased: 1.733
batch start
#iterations: 275
currently lose_sum: 94.92574340105057
time_elpased: 1.786
batch start
#iterations: 276
currently lose_sum: 94.92090821266174
time_elpased: 1.744
batch start
#iterations: 277
currently lose_sum: 95.08627319335938
time_elpased: 1.772
batch start
#iterations: 278
currently lose_sum: 95.3160582780838
time_elpased: 1.775
batch start
#iterations: 279
currently lose_sum: 94.56226962804794
time_elpased: 1.725
start validation test
0.651391752577
0.643649095507
0.681074405681
0.661833091655
0.651339640102
61.426
batch start
#iterations: 280
currently lose_sum: 94.99882954359055
time_elpased: 1.762
batch start
#iterations: 281
currently lose_sum: 94.73820072412491
time_elpased: 1.746
batch start
#iterations: 282
currently lose_sum: 94.59851861000061
time_elpased: 1.799
batch start
#iterations: 283
currently lose_sum: 94.81093293428421
time_elpased: 1.766
batch start
#iterations: 284
currently lose_sum: 94.76652443408966
time_elpased: 1.732
batch start
#iterations: 285
currently lose_sum: 94.76948672533035
time_elpased: 1.767
batch start
#iterations: 286
currently lose_sum: 94.84683567285538
time_elpased: 1.717
batch start
#iterations: 287
currently lose_sum: 94.55779039859772
time_elpased: 1.775
batch start
#iterations: 288
currently lose_sum: 94.50439101457596
time_elpased: 1.731
batch start
#iterations: 289
currently lose_sum: 94.86343252658844
time_elpased: 1.782
batch start
#iterations: 290
currently lose_sum: 94.96195447444916
time_elpased: 1.753
batch start
#iterations: 291
currently lose_sum: 94.67553925514221
time_elpased: 1.782
batch start
#iterations: 292
currently lose_sum: 94.57535910606384
time_elpased: 1.77
batch start
#iterations: 293
currently lose_sum: 94.79114997386932
time_elpased: 1.746
batch start
#iterations: 294
currently lose_sum: 94.23549097776413
time_elpased: 1.724
batch start
#iterations: 295
currently lose_sum: 94.67253881692886
time_elpased: 1.761
batch start
#iterations: 296
currently lose_sum: 94.59931802749634
time_elpased: 1.759
batch start
#iterations: 297
currently lose_sum: 95.07399672269821
time_elpased: 1.735
batch start
#iterations: 298
currently lose_sum: 94.45169669389725
time_elpased: 1.74
batch start
#iterations: 299
currently lose_sum: 94.77466094493866
time_elpased: 1.771
start validation test
0.657010309278
0.638184607056
0.727899557477
0.680096153846
0.656885852269
61.251
batch start
#iterations: 300
currently lose_sum: 94.70341390371323
time_elpased: 1.753
batch start
#iterations: 301
currently lose_sum: 94.63008934259415
time_elpased: 1.791
batch start
#iterations: 302
currently lose_sum: 94.80662423372269
time_elpased: 1.769
batch start
#iterations: 303
currently lose_sum: 94.48096418380737
time_elpased: 1.769
batch start
#iterations: 304
currently lose_sum: 94.84237092733383
time_elpased: 1.763
batch start
#iterations: 305
currently lose_sum: 94.66706997156143
time_elpased: 1.744
batch start
#iterations: 306
currently lose_sum: 94.73382812738419
time_elpased: 1.764
batch start
#iterations: 307
currently lose_sum: 94.38566613197327
time_elpased: 1.754
batch start
#iterations: 308
currently lose_sum: 94.73124545812607
time_elpased: 1.741
batch start
#iterations: 309
currently lose_sum: 94.5665374994278
time_elpased: 1.753
batch start
#iterations: 310
currently lose_sum: 95.18896752595901
time_elpased: 1.769
batch start
#iterations: 311
currently lose_sum: 94.54152238368988
time_elpased: 1.743
batch start
#iterations: 312
currently lose_sum: 94.66302233934402
time_elpased: 1.775
batch start
#iterations: 313
currently lose_sum: 94.248370885849
time_elpased: 1.743
batch start
#iterations: 314
currently lose_sum: 95.07398688793182
time_elpased: 1.755
batch start
#iterations: 315
currently lose_sum: 94.67569071054459
time_elpased: 1.759
batch start
#iterations: 316
currently lose_sum: 95.10373812913895
time_elpased: 1.79
batch start
#iterations: 317
currently lose_sum: 94.85559391975403
time_elpased: 1.786
batch start
#iterations: 318
currently lose_sum: 94.54661029577255
time_elpased: 1.778
batch start
#iterations: 319
currently lose_sum: 94.70778036117554
time_elpased: 1.732
start validation test
0.653195876289
0.639425319526
0.705361737162
0.67077706009
0.653104291074
61.237
batch start
#iterations: 320
currently lose_sum: 94.55124926567078
time_elpased: 1.776
batch start
#iterations: 321
currently lose_sum: 95.01613676548004
time_elpased: 1.783
batch start
#iterations: 322
currently lose_sum: 95.17345350980759
time_elpased: 1.768
batch start
#iterations: 323
currently lose_sum: 95.11382621526718
time_elpased: 1.785
batch start
#iterations: 324
currently lose_sum: 95.03447663784027
time_elpased: 1.773
batch start
#iterations: 325
currently lose_sum: 94.50881206989288
time_elpased: 1.75
batch start
#iterations: 326
currently lose_sum: 94.484090924263
time_elpased: 1.769
batch start
#iterations: 327
currently lose_sum: 94.46897882223129
time_elpased: 1.785
batch start
#iterations: 328
currently lose_sum: 94.65954208374023
time_elpased: 1.748
batch start
#iterations: 329
currently lose_sum: 94.77941954135895
time_elpased: 1.783
batch start
#iterations: 330
currently lose_sum: 94.24192428588867
time_elpased: 1.771
batch start
#iterations: 331
currently lose_sum: 94.59585475921631
time_elpased: 1.731
batch start
#iterations: 332
currently lose_sum: 94.66665887832642
time_elpased: 1.744
batch start
#iterations: 333
currently lose_sum: 94.6710399389267
time_elpased: 1.739
batch start
#iterations: 334
currently lose_sum: 95.52034467458725
time_elpased: 1.75
batch start
#iterations: 335
currently lose_sum: 94.42744773626328
time_elpased: 1.77
batch start
#iterations: 336
currently lose_sum: 94.72169148921967
time_elpased: 1.73
batch start
#iterations: 337
currently lose_sum: 94.38136845827103
time_elpased: 1.81
batch start
#iterations: 338
currently lose_sum: 94.82437252998352
time_elpased: 1.743
batch start
#iterations: 339
currently lose_sum: 94.84852647781372
time_elpased: 1.761
start validation test
0.659948453608
0.637833539495
0.74292477102
0.686379843119
0.659802775885
61.207
batch start
#iterations: 340
currently lose_sum: 94.64437264204025
time_elpased: 1.754
batch start
#iterations: 341
currently lose_sum: 95.09662246704102
time_elpased: 1.74
batch start
#iterations: 342
currently lose_sum: 94.33920896053314
time_elpased: 1.777
batch start
#iterations: 343
currently lose_sum: 94.47861474752426
time_elpased: 1.767
batch start
#iterations: 344
currently lose_sum: 94.66031169891357
time_elpased: 1.745
batch start
#iterations: 345
currently lose_sum: 94.62648898363113
time_elpased: 1.741
batch start
#iterations: 346
currently lose_sum: 94.91645485162735
time_elpased: 1.749
batch start
#iterations: 347
currently lose_sum: 94.45846092700958
time_elpased: 1.815
batch start
#iterations: 348
currently lose_sum: 94.81122487783432
time_elpased: 1.744
batch start
#iterations: 349
currently lose_sum: 94.41994333267212
time_elpased: 1.751
batch start
#iterations: 350
currently lose_sum: 94.87919688224792
time_elpased: 1.796
batch start
#iterations: 351
currently lose_sum: 94.57342433929443
time_elpased: 1.765
batch start
#iterations: 352
currently lose_sum: 94.7569255232811
time_elpased: 1.761
batch start
#iterations: 353
currently lose_sum: 94.86047768592834
time_elpased: 1.755
batch start
#iterations: 354
currently lose_sum: 94.75509124994278
time_elpased: 1.76
batch start
#iterations: 355
currently lose_sum: 94.40408307313919
time_elpased: 1.803
batch start
#iterations: 356
currently lose_sum: 94.5128144621849
time_elpased: 1.791
batch start
#iterations: 357
currently lose_sum: 94.70648545026779
time_elpased: 1.745
batch start
#iterations: 358
currently lose_sum: 94.39355325698853
time_elpased: 1.777
batch start
#iterations: 359
currently lose_sum: 94.911592066288
time_elpased: 1.732
start validation test
0.657474226804
0.638353449829
0.729340331378
0.680820404438
0.657348054773
61.179
batch start
#iterations: 360
currently lose_sum: 94.78028559684753
time_elpased: 1.775
batch start
#iterations: 361
currently lose_sum: 95.08370465040207
time_elpased: 1.769
batch start
#iterations: 362
currently lose_sum: 94.53892159461975
time_elpased: 1.774
batch start
#iterations: 363
currently lose_sum: 94.65092641115189
time_elpased: 1.782
batch start
#iterations: 364
currently lose_sum: 94.42587566375732
time_elpased: 1.749
batch start
#iterations: 365
currently lose_sum: 94.94343364238739
time_elpased: 1.765
batch start
#iterations: 366
currently lose_sum: 94.88736510276794
time_elpased: 1.777
batch start
#iterations: 367
currently lose_sum: 94.75822162628174
time_elpased: 1.737
batch start
#iterations: 368
currently lose_sum: 94.86839658021927
time_elpased: 1.777
batch start
#iterations: 369
currently lose_sum: 94.87019610404968
time_elpased: 1.746
batch start
#iterations: 370
currently lose_sum: 94.51509773731232
time_elpased: 1.773
batch start
#iterations: 371
currently lose_sum: 94.82149863243103
time_elpased: 1.778
batch start
#iterations: 372
currently lose_sum: 95.02436780929565
time_elpased: 1.776
batch start
#iterations: 373
currently lose_sum: 94.69846767187119
time_elpased: 1.773
batch start
#iterations: 374
currently lose_sum: 94.64152139425278
time_elpased: 1.797
batch start
#iterations: 375
currently lose_sum: 94.67729079723358
time_elpased: 1.754
batch start
#iterations: 376
currently lose_sum: 95.1030462384224
time_elpased: 1.737
batch start
#iterations: 377
currently lose_sum: 94.77953612804413
time_elpased: 1.777
batch start
#iterations: 378
currently lose_sum: 94.56244480609894
time_elpased: 1.765
batch start
#iterations: 379
currently lose_sum: 94.79613274335861
time_elpased: 1.705
start validation test
0.656494845361
0.63790767007
0.726664608418
0.679399595882
0.656371651519
61.216
batch start
#iterations: 380
currently lose_sum: 94.76046228408813
time_elpased: 1.763
batch start
#iterations: 381
currently lose_sum: 94.8501667380333
time_elpased: 1.779
batch start
#iterations: 382
currently lose_sum: 94.7848539352417
time_elpased: 1.724
batch start
#iterations: 383
currently lose_sum: 94.69997441768646
time_elpased: 1.75
batch start
#iterations: 384
currently lose_sum: 94.6450914144516
time_elpased: 1.748
batch start
#iterations: 385
currently lose_sum: 94.9492152929306
time_elpased: 1.789
batch start
#iterations: 386
currently lose_sum: 94.56909584999084
time_elpased: 1.802
batch start
#iterations: 387
currently lose_sum: 94.65005856752396
time_elpased: 1.763
batch start
#iterations: 388
currently lose_sum: 94.972565472126
time_elpased: 1.771
batch start
#iterations: 389
currently lose_sum: 94.88867902755737
time_elpased: 1.772
batch start
#iterations: 390
currently lose_sum: 95.11013841629028
time_elpased: 1.749
batch start
#iterations: 391
currently lose_sum: 94.55406200885773
time_elpased: 1.737
batch start
#iterations: 392
currently lose_sum: 94.61182367801666
time_elpased: 1.776
batch start
#iterations: 393
currently lose_sum: 94.31280827522278
time_elpased: 1.79
batch start
#iterations: 394
currently lose_sum: 94.66585385799408
time_elpased: 1.765
batch start
#iterations: 395
currently lose_sum: 94.98346495628357
time_elpased: 1.76
batch start
#iterations: 396
currently lose_sum: 94.46655595302582
time_elpased: 1.827
batch start
#iterations: 397
currently lose_sum: 94.9686871767044
time_elpased: 1.734
batch start
#iterations: 398
currently lose_sum: 94.73437112569809
time_elpased: 1.793
batch start
#iterations: 399
currently lose_sum: 94.3543826341629
time_elpased: 1.779
start validation test
0.66206185567
0.638167671999
0.751260677164
0.690111552278
0.661905253381
61.092
acc: 0.659
pre: 0.635
rec: 0.748
F1: 0.687
auc: 0.693
