start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.2992212176323
time_elpased: 1.629
batch start
#iterations: 1
currently lose_sum: 100.07253593206406
time_elpased: 1.652
batch start
#iterations: 2
currently lose_sum: 99.79874122142792
time_elpased: 1.594
batch start
#iterations: 3
currently lose_sum: 99.35625594854355
time_elpased: 1.613
batch start
#iterations: 4
currently lose_sum: 99.150665640831
time_elpased: 1.553
batch start
#iterations: 5
currently lose_sum: 99.02019673585892
time_elpased: 1.574
batch start
#iterations: 6
currently lose_sum: 98.58984708786011
time_elpased: 1.597
batch start
#iterations: 7
currently lose_sum: 98.43534177541733
time_elpased: 1.578
batch start
#iterations: 8
currently lose_sum: 98.29345178604126
time_elpased: 1.655
batch start
#iterations: 9
currently lose_sum: 98.26981323957443
time_elpased: 1.613
batch start
#iterations: 10
currently lose_sum: 98.16787272691727
time_elpased: 1.591
batch start
#iterations: 11
currently lose_sum: 97.88611453771591
time_elpased: 1.587
batch start
#iterations: 12
currently lose_sum: 98.121717274189
time_elpased: 1.589
batch start
#iterations: 13
currently lose_sum: 97.88669866323471
time_elpased: 1.609
batch start
#iterations: 14
currently lose_sum: 97.54908812046051
time_elpased: 1.568
batch start
#iterations: 15
currently lose_sum: 97.78965353965759
time_elpased: 1.614
batch start
#iterations: 16
currently lose_sum: 97.46093767881393
time_elpased: 1.575
batch start
#iterations: 17
currently lose_sum: 97.2770214676857
time_elpased: 1.586
batch start
#iterations: 18
currently lose_sum: 97.23506039381027
time_elpased: 1.561
batch start
#iterations: 19
currently lose_sum: 97.29110330343246
time_elpased: 1.609
start validation test
0.639278350515
0.631314594803
0.672635587115
0.651320378675
0.639219786742
62.900
batch start
#iterations: 20
currently lose_sum: 97.34352570772171
time_elpased: 1.542
batch start
#iterations: 21
currently lose_sum: 97.27216291427612
time_elpased: 1.592
batch start
#iterations: 22
currently lose_sum: 97.07489544153214
time_elpased: 1.598
batch start
#iterations: 23
currently lose_sum: 96.90643268823624
time_elpased: 1.604
batch start
#iterations: 24
currently lose_sum: 96.84908819198608
time_elpased: 1.627
batch start
#iterations: 25
currently lose_sum: 97.30005270242691
time_elpased: 1.579
batch start
#iterations: 26
currently lose_sum: 97.04997843503952
time_elpased: 1.603
batch start
#iterations: 27
currently lose_sum: 96.81587129831314
time_elpased: 1.57
batch start
#iterations: 28
currently lose_sum: 96.83525866270065
time_elpased: 1.576
batch start
#iterations: 29
currently lose_sum: 97.11103236675262
time_elpased: 1.581
batch start
#iterations: 30
currently lose_sum: 96.68762511014938
time_elpased: 1.593
batch start
#iterations: 31
currently lose_sum: 96.42386358976364
time_elpased: 1.585
batch start
#iterations: 32
currently lose_sum: 96.60894823074341
time_elpased: 1.591
batch start
#iterations: 33
currently lose_sum: 96.83832985162735
time_elpased: 1.564
batch start
#iterations: 34
currently lose_sum: 96.59793144464493
time_elpased: 1.618
batch start
#iterations: 35
currently lose_sum: 96.79100698232651
time_elpased: 1.583
batch start
#iterations: 36
currently lose_sum: 96.18252509832382
time_elpased: 1.584
batch start
#iterations: 37
currently lose_sum: 96.61120504140854
time_elpased: 1.604
batch start
#iterations: 38
currently lose_sum: 96.61075133085251
time_elpased: 1.617
batch start
#iterations: 39
currently lose_sum: 96.46642792224884
time_elpased: 1.561
start validation test
0.647268041237
0.638546085615
0.681588967788
0.659365822092
0.647207785557
62.404
batch start
#iterations: 40
currently lose_sum: 96.65414780378342
time_elpased: 1.607
batch start
#iterations: 41
currently lose_sum: 96.41500788927078
time_elpased: 1.604
batch start
#iterations: 42
currently lose_sum: 96.46650516986847
time_elpased: 1.566
batch start
#iterations: 43
currently lose_sum: 96.25460767745972
time_elpased: 1.596
batch start
#iterations: 44
currently lose_sum: 96.39481580257416
time_elpased: 1.609
batch start
#iterations: 45
currently lose_sum: 96.35131794214249
time_elpased: 1.55
batch start
#iterations: 46
currently lose_sum: 96.20839458703995
time_elpased: 1.613
batch start
#iterations: 47
currently lose_sum: 96.37128102779388
time_elpased: 1.573
batch start
#iterations: 48
currently lose_sum: 96.50503754615784
time_elpased: 1.573
batch start
#iterations: 49
currently lose_sum: 96.1878337264061
time_elpased: 1.603
batch start
#iterations: 50
currently lose_sum: 96.22628098726273
time_elpased: 1.579
batch start
#iterations: 51
currently lose_sum: 96.03631883859634
time_elpased: 1.59
batch start
#iterations: 52
currently lose_sum: 96.05191344022751
time_elpased: 1.617
batch start
#iterations: 53
currently lose_sum: 96.34526771306992
time_elpased: 1.563
batch start
#iterations: 54
currently lose_sum: 96.13233786821365
time_elpased: 1.564
batch start
#iterations: 55
currently lose_sum: 96.04287374019623
time_elpased: 1.614
batch start
#iterations: 56
currently lose_sum: 96.19605505466461
time_elpased: 1.616
batch start
#iterations: 57
currently lose_sum: 95.98934519290924
time_elpased: 1.579
batch start
#iterations: 58
currently lose_sum: 95.83644890785217
time_elpased: 1.586
batch start
#iterations: 59
currently lose_sum: 96.17253494262695
time_elpased: 1.564
start validation test
0.651649484536
0.63559710384
0.713697643306
0.672387046733
0.651540549423
61.971
batch start
#iterations: 60
currently lose_sum: 96.02434319257736
time_elpased: 1.583
batch start
#iterations: 61
currently lose_sum: 95.87703561782837
time_elpased: 1.567
batch start
#iterations: 62
currently lose_sum: 96.25529563426971
time_elpased: 1.599
batch start
#iterations: 63
currently lose_sum: 95.92243218421936
time_elpased: 1.549
batch start
#iterations: 64
currently lose_sum: 96.31835198402405
time_elpased: 1.576
batch start
#iterations: 65
currently lose_sum: 95.80337429046631
time_elpased: 1.592
batch start
#iterations: 66
currently lose_sum: 96.29481106996536
time_elpased: 1.578
batch start
#iterations: 67
currently lose_sum: 95.78487366437912
time_elpased: 1.594
batch start
#iterations: 68
currently lose_sum: 95.94338327646255
time_elpased: 1.584
batch start
#iterations: 69
currently lose_sum: 95.80592638254166
time_elpased: 1.616
batch start
#iterations: 70
currently lose_sum: 95.73121756315231
time_elpased: 1.581
batch start
#iterations: 71
currently lose_sum: 95.94321203231812
time_elpased: 1.561
batch start
#iterations: 72
currently lose_sum: 95.99217706918716
time_elpased: 1.586
batch start
#iterations: 73
currently lose_sum: 96.08652675151825
time_elpased: 1.544
batch start
#iterations: 74
currently lose_sum: 96.08912754058838
time_elpased: 1.651
batch start
#iterations: 75
currently lose_sum: 96.17251670360565
time_elpased: 1.539
batch start
#iterations: 76
currently lose_sum: 95.53334707021713
time_elpased: 1.548
batch start
#iterations: 77
currently lose_sum: 95.7368940114975
time_elpased: 1.573
batch start
#iterations: 78
currently lose_sum: 96.0834596157074
time_elpased: 1.581
batch start
#iterations: 79
currently lose_sum: 95.90026068687439
time_elpased: 1.567
start validation test
0.65175257732
0.64039829303
0.694967582587
0.666567959728
0.651676706712
61.789
batch start
#iterations: 80
currently lose_sum: 95.99644094705582
time_elpased: 1.594
batch start
#iterations: 81
currently lose_sum: 95.88762217760086
time_elpased: 1.605
batch start
#iterations: 82
currently lose_sum: 96.05064523220062
time_elpased: 1.565
batch start
#iterations: 83
currently lose_sum: 95.63712251186371
time_elpased: 1.602
batch start
#iterations: 84
currently lose_sum: 95.95912772417068
time_elpased: 1.576
batch start
#iterations: 85
currently lose_sum: 95.97581887245178
time_elpased: 1.65
batch start
#iterations: 86
currently lose_sum: 95.63671940565109
time_elpased: 1.553
batch start
#iterations: 87
currently lose_sum: 96.01316279172897
time_elpased: 1.546
batch start
#iterations: 88
currently lose_sum: 95.53191924095154
time_elpased: 1.575
batch start
#iterations: 89
currently lose_sum: 95.89316463470459
time_elpased: 1.574
batch start
#iterations: 90
currently lose_sum: 95.91160446405411
time_elpased: 1.588
batch start
#iterations: 91
currently lose_sum: 95.83218967914581
time_elpased: 1.583
batch start
#iterations: 92
currently lose_sum: 95.6374061703682
time_elpased: 1.626
batch start
#iterations: 93
currently lose_sum: 95.95005238056183
time_elpased: 1.615
batch start
#iterations: 94
currently lose_sum: 95.62620294094086
time_elpased: 1.598
batch start
#iterations: 95
currently lose_sum: 95.5198255777359
time_elpased: 1.601
batch start
#iterations: 96
currently lose_sum: 95.55755120515823
time_elpased: 1.532
batch start
#iterations: 97
currently lose_sum: 95.62314337491989
time_elpased: 1.577
batch start
#iterations: 98
currently lose_sum: 95.72971624135971
time_elpased: 1.545
batch start
#iterations: 99
currently lose_sum: 95.84723597764969
time_elpased: 1.55
start validation test
0.652783505155
0.636705493901
0.714418030256
0.673326867119
0.652675296239
61.727
batch start
#iterations: 100
currently lose_sum: 96.05324304103851
time_elpased: 1.578
batch start
#iterations: 101
currently lose_sum: 95.44235324859619
time_elpased: 1.58
batch start
#iterations: 102
currently lose_sum: 95.72442072629929
time_elpased: 1.536
batch start
#iterations: 103
currently lose_sum: 95.9419037103653
time_elpased: 1.582
batch start
#iterations: 104
currently lose_sum: 95.66627895832062
time_elpased: 1.602
batch start
#iterations: 105
currently lose_sum: 95.46003043651581
time_elpased: 1.571
batch start
#iterations: 106
currently lose_sum: 95.87729561328888
time_elpased: 1.571
batch start
#iterations: 107
currently lose_sum: 95.98536598682404
time_elpased: 1.647
batch start
#iterations: 108
currently lose_sum: 95.44021266698837
time_elpased: 1.564
batch start
#iterations: 109
currently lose_sum: 96.05965608358383
time_elpased: 1.579
batch start
#iterations: 110
currently lose_sum: 95.7458388209343
time_elpased: 1.64
batch start
#iterations: 111
currently lose_sum: 95.54492175579071
time_elpased: 1.629
batch start
#iterations: 112
currently lose_sum: 95.48464041948318
time_elpased: 1.614
batch start
#iterations: 113
currently lose_sum: 95.37033087015152
time_elpased: 1.604
batch start
#iterations: 114
currently lose_sum: 95.4149860739708
time_elpased: 1.573
batch start
#iterations: 115
currently lose_sum: 95.79211956262589
time_elpased: 1.626
batch start
#iterations: 116
currently lose_sum: 95.17657524347305
time_elpased: 1.566
batch start
#iterations: 117
currently lose_sum: 95.54571348428726
time_elpased: 1.585
batch start
#iterations: 118
currently lose_sum: 95.09017914533615
time_elpased: 1.587
batch start
#iterations: 119
currently lose_sum: 95.47511351108551
time_elpased: 1.595
start validation test
0.64881443299
0.637110481586
0.694350108058
0.664499926134
0.648734488089
61.651
batch start
#iterations: 120
currently lose_sum: 95.57874548435211
time_elpased: 1.594
batch start
#iterations: 121
currently lose_sum: 95.3279778957367
time_elpased: 1.565
batch start
#iterations: 122
currently lose_sum: 95.26477640867233
time_elpased: 1.537
batch start
#iterations: 123
currently lose_sum: 95.60422563552856
time_elpased: 1.574
batch start
#iterations: 124
currently lose_sum: 95.66250717639923
time_elpased: 1.583
batch start
#iterations: 125
currently lose_sum: 95.47984337806702
time_elpased: 1.633
batch start
#iterations: 126
currently lose_sum: 95.80191272497177
time_elpased: 1.535
batch start
#iterations: 127
currently lose_sum: 95.60868299007416
time_elpased: 1.587
batch start
#iterations: 128
currently lose_sum: 95.39392012357712
time_elpased: 1.585
batch start
#iterations: 129
currently lose_sum: 96.09999227523804
time_elpased: 1.577
batch start
#iterations: 130
currently lose_sum: 95.17617410421371
time_elpased: 1.573
batch start
#iterations: 131
currently lose_sum: 95.8563466668129
time_elpased: 1.611
batch start
#iterations: 132
currently lose_sum: 95.43299895524979
time_elpased: 1.553
batch start
#iterations: 133
currently lose_sum: 95.47824043035507
time_elpased: 1.591
batch start
#iterations: 134
currently lose_sum: 95.10246735811234
time_elpased: 1.609
batch start
#iterations: 135
currently lose_sum: 95.42023539543152
time_elpased: 1.605
batch start
#iterations: 136
currently lose_sum: 95.33123701810837
time_elpased: 1.567
batch start
#iterations: 137
currently lose_sum: 95.30471926927567
time_elpased: 1.58
batch start
#iterations: 138
currently lose_sum: 95.56922328472137
time_elpased: 1.553
batch start
#iterations: 139
currently lose_sum: 95.19108974933624
time_elpased: 1.612
start validation test
0.648195876289
0.640252182347
0.679324894515
0.65921006641
0.648141224496
61.631
batch start
#iterations: 140
currently lose_sum: 95.570472240448
time_elpased: 1.626
batch start
#iterations: 141
currently lose_sum: 95.54010158777237
time_elpased: 1.574
batch start
#iterations: 142
currently lose_sum: 95.2604940533638
time_elpased: 1.618
batch start
#iterations: 143
currently lose_sum: 95.4252005815506
time_elpased: 1.613
batch start
#iterations: 144
currently lose_sum: 95.56467634439468
time_elpased: 1.578
batch start
#iterations: 145
currently lose_sum: 95.35728693008423
time_elpased: 1.554
batch start
#iterations: 146
currently lose_sum: 95.35052275657654
time_elpased: 1.566
batch start
#iterations: 147
currently lose_sum: 95.44172692298889
time_elpased: 1.542
batch start
#iterations: 148
currently lose_sum: 95.26816391944885
time_elpased: 1.63
batch start
#iterations: 149
currently lose_sum: 95.49808835983276
time_elpased: 1.638
batch start
#iterations: 150
currently lose_sum: 95.4535825252533
time_elpased: 1.678
batch start
#iterations: 151
currently lose_sum: 95.32856559753418
time_elpased: 1.585
batch start
#iterations: 152
currently lose_sum: 95.40266579389572
time_elpased: 1.596
batch start
#iterations: 153
currently lose_sum: 95.63692909479141
time_elpased: 1.607
batch start
#iterations: 154
currently lose_sum: 95.28112590312958
time_elpased: 1.582
batch start
#iterations: 155
currently lose_sum: 95.32868373394012
time_elpased: 1.584
batch start
#iterations: 156
currently lose_sum: 95.60093230009079
time_elpased: 1.586
batch start
#iterations: 157
currently lose_sum: 94.93935894966125
time_elpased: 1.568
batch start
#iterations: 158
currently lose_sum: 95.32320064306259
time_elpased: 1.565
batch start
#iterations: 159
currently lose_sum: 95.20069199800491
time_elpased: 1.6
start validation test
0.655979381443
0.6394719956
0.717917052588
0.676427809561
0.655870640308
61.377
batch start
#iterations: 160
currently lose_sum: 95.252616584301
time_elpased: 1.607
batch start
#iterations: 161
currently lose_sum: 95.92316740751266
time_elpased: 1.57
batch start
#iterations: 162
currently lose_sum: 95.5441118478775
time_elpased: 1.657
batch start
#iterations: 163
currently lose_sum: 95.35472178459167
time_elpased: 1.59
batch start
#iterations: 164
currently lose_sum: 95.26223415136337
time_elpased: 1.572
batch start
#iterations: 165
currently lose_sum: 95.33524942398071
time_elpased: 1.614
batch start
#iterations: 166
currently lose_sum: 95.38462662696838
time_elpased: 1.557
batch start
#iterations: 167
currently lose_sum: 95.13263058662415
time_elpased: 1.58
batch start
#iterations: 168
currently lose_sum: 95.1901826262474
time_elpased: 1.638
batch start
#iterations: 169
currently lose_sum: 95.23149305582047
time_elpased: 1.606
batch start
#iterations: 170
currently lose_sum: 95.65208172798157
time_elpased: 1.583
batch start
#iterations: 171
currently lose_sum: 95.45177400112152
time_elpased: 1.581
batch start
#iterations: 172
currently lose_sum: 95.60199707746506
time_elpased: 1.649
batch start
#iterations: 173
currently lose_sum: 95.4259791970253
time_elpased: 1.62
batch start
#iterations: 174
currently lose_sum: 94.90112137794495
time_elpased: 1.59
batch start
#iterations: 175
currently lose_sum: 95.00880759954453
time_elpased: 1.613
batch start
#iterations: 176
currently lose_sum: 95.18150222301483
time_elpased: 1.634
batch start
#iterations: 177
currently lose_sum: 95.23248022794724
time_elpased: 1.599
batch start
#iterations: 178
currently lose_sum: 95.37304294109344
time_elpased: 1.598
batch start
#iterations: 179
currently lose_sum: 95.34842842817307
time_elpased: 1.588
start validation test
0.658402061856
0.638490498387
0.733045178553
0.682508503809
0.658271014351
61.231
batch start
#iterations: 180
currently lose_sum: 95.05045008659363
time_elpased: 1.53
batch start
#iterations: 181
currently lose_sum: 95.18574512004852
time_elpased: 1.572
batch start
#iterations: 182
currently lose_sum: 95.20657873153687
time_elpased: 1.564
batch start
#iterations: 183
currently lose_sum: 95.25553053617477
time_elpased: 1.572
batch start
#iterations: 184
currently lose_sum: 95.68523889780045
time_elpased: 1.617
batch start
#iterations: 185
currently lose_sum: 94.90033662319183
time_elpased: 1.595
batch start
#iterations: 186
currently lose_sum: 95.5895437002182
time_elpased: 1.569
batch start
#iterations: 187
currently lose_sum: 95.18865424394608
time_elpased: 1.591
batch start
#iterations: 188
currently lose_sum: 95.23264753818512
time_elpased: 1.576
batch start
#iterations: 189
currently lose_sum: 95.18973195552826
time_elpased: 1.566
batch start
#iterations: 190
currently lose_sum: 94.73272794485092
time_elpased: 1.59
batch start
#iterations: 191
currently lose_sum: 95.12558472156525
time_elpased: 1.603
batch start
#iterations: 192
currently lose_sum: 95.23148518800735
time_elpased: 1.547
batch start
#iterations: 193
currently lose_sum: 95.23488855361938
time_elpased: 1.602
batch start
#iterations: 194
currently lose_sum: 94.96205401420593
time_elpased: 1.55
batch start
#iterations: 195
currently lose_sum: 95.32462322711945
time_elpased: 1.533
batch start
#iterations: 196
currently lose_sum: 95.23321104049683
time_elpased: 1.55
batch start
#iterations: 197
currently lose_sum: 95.3666405081749
time_elpased: 1.597
batch start
#iterations: 198
currently lose_sum: 95.16738837957382
time_elpased: 1.595
batch start
#iterations: 199
currently lose_sum: 94.93545746803284
time_elpased: 1.602
start validation test
0.652783505155
0.639234002802
0.704229700525
0.67015963177
0.652693183424
61.335
batch start
#iterations: 200
currently lose_sum: 95.26611369848251
time_elpased: 1.562
batch start
#iterations: 201
currently lose_sum: 95.15700703859329
time_elpased: 1.582
batch start
#iterations: 202
currently lose_sum: 94.74389427900314
time_elpased: 1.598
batch start
#iterations: 203
currently lose_sum: 95.36319011449814
time_elpased: 1.601
batch start
#iterations: 204
currently lose_sum: 94.87226796150208
time_elpased: 1.597
batch start
#iterations: 205
currently lose_sum: 94.84956407546997
time_elpased: 1.644
batch start
#iterations: 206
currently lose_sum: 94.90487682819366
time_elpased: 1.565
batch start
#iterations: 207
currently lose_sum: 95.24410510063171
time_elpased: 1.562
batch start
#iterations: 208
currently lose_sum: 94.75653272867203
time_elpased: 1.592
batch start
#iterations: 209
currently lose_sum: 95.00350618362427
time_elpased: 1.577
batch start
#iterations: 210
currently lose_sum: 94.96584570407867
time_elpased: 1.613
batch start
#iterations: 211
currently lose_sum: 95.28131622076035
time_elpased: 1.578
batch start
#iterations: 212
currently lose_sum: 95.61621928215027
time_elpased: 1.555
batch start
#iterations: 213
currently lose_sum: 95.31041395664215
time_elpased: 1.64
batch start
#iterations: 214
currently lose_sum: 94.95884293317795
time_elpased: 1.6
batch start
#iterations: 215
currently lose_sum: 94.98862183094025
time_elpased: 1.623
batch start
#iterations: 216
currently lose_sum: 95.02524304389954
time_elpased: 1.596
batch start
#iterations: 217
currently lose_sum: 95.19016933441162
time_elpased: 1.607
batch start
#iterations: 218
currently lose_sum: 94.66617196798325
time_elpased: 1.618
batch start
#iterations: 219
currently lose_sum: 95.38167279958725
time_elpased: 1.559
start validation test
0.65706185567
0.641642011834
0.714212205413
0.675984999756
0.656961519416
61.433
batch start
#iterations: 220
currently lose_sum: 95.1168042421341
time_elpased: 1.583
batch start
#iterations: 221
currently lose_sum: 95.26004523038864
time_elpased: 1.59
batch start
#iterations: 222
currently lose_sum: 94.95869439840317
time_elpased: 1.587
batch start
#iterations: 223
currently lose_sum: 94.73779654502869
time_elpased: 1.582
batch start
#iterations: 224
currently lose_sum: 95.03411370515823
time_elpased: 1.576
batch start
#iterations: 225
currently lose_sum: 95.57442605495453
time_elpased: 1.575
batch start
#iterations: 226
currently lose_sum: 94.62718498706818
time_elpased: 1.572
batch start
#iterations: 227
currently lose_sum: 94.89663672447205
time_elpased: 1.549
batch start
#iterations: 228
currently lose_sum: 94.90303725004196
time_elpased: 1.588
batch start
#iterations: 229
currently lose_sum: 95.02076947689056
time_elpased: 1.567
batch start
#iterations: 230
currently lose_sum: 95.2390643954277
time_elpased: 1.591
batch start
#iterations: 231
currently lose_sum: 94.8042643070221
time_elpased: 1.555
batch start
#iterations: 232
currently lose_sum: 94.91719490289688
time_elpased: 1.625
batch start
#iterations: 233
currently lose_sum: 95.2371015548706
time_elpased: 1.568
batch start
#iterations: 234
currently lose_sum: 95.15188241004944
time_elpased: 1.6
batch start
#iterations: 235
currently lose_sum: 94.65763252973557
time_elpased: 1.624
batch start
#iterations: 236
currently lose_sum: 95.16123819351196
time_elpased: 1.55
batch start
#iterations: 237
currently lose_sum: 95.1256971359253
time_elpased: 1.612
batch start
#iterations: 238
currently lose_sum: 95.15976411104202
time_elpased: 1.578
batch start
#iterations: 239
currently lose_sum: 95.11818981170654
time_elpased: 1.546
start validation test
0.655670103093
0.640536788524
0.712256869404
0.674495663191
0.655570756297
61.286
batch start
#iterations: 240
currently lose_sum: 94.82064992189407
time_elpased: 1.59
batch start
#iterations: 241
currently lose_sum: 95.11098730564117
time_elpased: 1.533
batch start
#iterations: 242
currently lose_sum: 95.22158539295197
time_elpased: 1.596
batch start
#iterations: 243
currently lose_sum: 95.01607477664948
time_elpased: 1.591
batch start
#iterations: 244
currently lose_sum: 94.92248338460922
time_elpased: 1.595
batch start
#iterations: 245
currently lose_sum: 95.2857101559639
time_elpased: 1.548
batch start
#iterations: 246
currently lose_sum: 95.34936302900314
time_elpased: 1.577
batch start
#iterations: 247
currently lose_sum: 94.73629486560822
time_elpased: 1.566
batch start
#iterations: 248
currently lose_sum: 94.81129842996597
time_elpased: 1.581
batch start
#iterations: 249
currently lose_sum: 95.24265021085739
time_elpased: 1.544
batch start
#iterations: 250
currently lose_sum: 94.75453847646713
time_elpased: 1.572
batch start
#iterations: 251
currently lose_sum: 94.98130965232849
time_elpased: 1.626
batch start
#iterations: 252
currently lose_sum: 95.32950353622437
time_elpased: 1.628
batch start
#iterations: 253
currently lose_sum: 94.77580964565277
time_elpased: 1.599
batch start
#iterations: 254
currently lose_sum: 95.07431083917618
time_elpased: 1.589
batch start
#iterations: 255
currently lose_sum: 95.00532186031342
time_elpased: 1.563
batch start
#iterations: 256
currently lose_sum: 94.58782488107681
time_elpased: 1.608
batch start
#iterations: 257
currently lose_sum: 94.92320364713669
time_elpased: 1.576
batch start
#iterations: 258
currently lose_sum: 94.958731174469
time_elpased: 1.614
batch start
#iterations: 259
currently lose_sum: 94.58444881439209
time_elpased: 1.621
start validation test
0.663402061856
0.643519769432
0.735309251827
0.686359269933
0.663275817693
60.993
batch start
#iterations: 260
currently lose_sum: 94.68660432100296
time_elpased: 1.558
batch start
#iterations: 261
currently lose_sum: 94.57557946443558
time_elpased: 1.583
batch start
#iterations: 262
currently lose_sum: 95.16457855701447
time_elpased: 1.597
batch start
#iterations: 263
currently lose_sum: 95.3921428322792
time_elpased: 1.646
batch start
#iterations: 264
currently lose_sum: 94.89191490411758
time_elpased: 1.583
batch start
#iterations: 265
currently lose_sum: 94.90112382173538
time_elpased: 1.555
batch start
#iterations: 266
currently lose_sum: 94.82509255409241
time_elpased: 1.613
batch start
#iterations: 267
currently lose_sum: 94.95861947536469
time_elpased: 1.6
batch start
#iterations: 268
currently lose_sum: 94.84037959575653
time_elpased: 1.615
batch start
#iterations: 269
currently lose_sum: 95.15578436851501
time_elpased: 1.531
batch start
#iterations: 270
currently lose_sum: 94.96054947376251
time_elpased: 1.605
batch start
#iterations: 271
currently lose_sum: 94.75231105089188
time_elpased: 1.633
batch start
#iterations: 272
currently lose_sum: 94.6083515882492
time_elpased: 1.597
batch start
#iterations: 273
currently lose_sum: 94.71174997091293
time_elpased: 1.553
batch start
#iterations: 274
currently lose_sum: 95.2015762925148
time_elpased: 1.554
batch start
#iterations: 275
currently lose_sum: 95.10882288217545
time_elpased: 1.562
batch start
#iterations: 276
currently lose_sum: 94.76026719808578
time_elpased: 1.614
batch start
#iterations: 277
currently lose_sum: 94.73268246650696
time_elpased: 1.618
batch start
#iterations: 278
currently lose_sum: 94.66562521457672
time_elpased: 1.603
batch start
#iterations: 279
currently lose_sum: 94.95034992694855
time_elpased: 1.598
start validation test
0.659639175258
0.64075212439
0.7294432438
0.682227248665
0.659516623449
61.165
batch start
#iterations: 280
currently lose_sum: 95.05527263879776
time_elpased: 1.62
batch start
#iterations: 281
currently lose_sum: 94.92192894220352
time_elpased: 1.631
batch start
#iterations: 282
currently lose_sum: 94.68751966953278
time_elpased: 1.582
batch start
#iterations: 283
currently lose_sum: 94.95879524946213
time_elpased: 1.592
batch start
#iterations: 284
currently lose_sum: 94.80185288190842
time_elpased: 1.551
batch start
#iterations: 285
currently lose_sum: 94.96017676591873
time_elpased: 1.569
batch start
#iterations: 286
currently lose_sum: 94.72220551967621
time_elpased: 1.651
batch start
#iterations: 287
currently lose_sum: 94.67350202798843
time_elpased: 1.603
batch start
#iterations: 288
currently lose_sum: 95.03281027078629
time_elpased: 1.582
batch start
#iterations: 289
currently lose_sum: 94.79866498708725
time_elpased: 1.581
batch start
#iterations: 290
currently lose_sum: 94.67721742391586
time_elpased: 1.574
batch start
#iterations: 291
currently lose_sum: 94.96225875616074
time_elpased: 1.594
batch start
#iterations: 292
currently lose_sum: 95.06419855356216
time_elpased: 1.546
batch start
#iterations: 293
currently lose_sum: 95.04147559404373
time_elpased: 1.634
batch start
#iterations: 294
currently lose_sum: 95.18863755464554
time_elpased: 1.604
batch start
#iterations: 295
currently lose_sum: 95.19541501998901
time_elpased: 1.599
batch start
#iterations: 296
currently lose_sum: 94.82587516307831
time_elpased: 1.559
batch start
#iterations: 297
currently lose_sum: 94.80894094705582
time_elpased: 1.575
batch start
#iterations: 298
currently lose_sum: 95.05361044406891
time_elpased: 1.62
batch start
#iterations: 299
currently lose_sum: 94.66142964363098
time_elpased: 1.576
start validation test
0.662010309278
0.638766906728
0.748482041782
0.689285883524
0.661858494815
61.106
batch start
#iterations: 300
currently lose_sum: 94.99275851249695
time_elpased: 1.628
batch start
#iterations: 301
currently lose_sum: 94.99991911649704
time_elpased: 1.615
batch start
#iterations: 302
currently lose_sum: 94.7084242105484
time_elpased: 1.592
batch start
#iterations: 303
currently lose_sum: 95.09624379873276
time_elpased: 1.607
batch start
#iterations: 304
currently lose_sum: 95.35914731025696
time_elpased: 1.58
batch start
#iterations: 305
currently lose_sum: 94.945043861866
time_elpased: 1.591
batch start
#iterations: 306
currently lose_sum: 94.69069820642471
time_elpased: 1.601
batch start
#iterations: 307
currently lose_sum: 94.78789907693863
time_elpased: 1.572
batch start
#iterations: 308
currently lose_sum: 94.26702290773392
time_elpased: 1.606
batch start
#iterations: 309
currently lose_sum: 94.73797869682312
time_elpased: 1.608
batch start
#iterations: 310
currently lose_sum: 95.29595857858658
time_elpased: 1.644
batch start
#iterations: 311
currently lose_sum: 94.8283760547638
time_elpased: 1.584
batch start
#iterations: 312
currently lose_sum: 94.80038100481033
time_elpased: 1.58
batch start
#iterations: 313
currently lose_sum: 94.82554948329926
time_elpased: 1.616
batch start
#iterations: 314
currently lose_sum: 94.85088467597961
time_elpased: 1.592
batch start
#iterations: 315
currently lose_sum: 94.65872430801392
time_elpased: 1.563
batch start
#iterations: 316
currently lose_sum: 94.71808975934982
time_elpased: 1.561
batch start
#iterations: 317
currently lose_sum: 94.66606384515762
time_elpased: 1.546
batch start
#iterations: 318
currently lose_sum: 94.8442410826683
time_elpased: 1.558
batch start
#iterations: 319
currently lose_sum: 94.7758766412735
time_elpased: 1.582
start validation test
0.65587628866
0.640565776093
0.713080168776
0.674880685692
0.655775858425
61.186
batch start
#iterations: 320
currently lose_sum: 94.59563374519348
time_elpased: 1.586
batch start
#iterations: 321
currently lose_sum: 94.94971680641174
time_elpased: 1.59
batch start
#iterations: 322
currently lose_sum: 95.08493161201477
time_elpased: 1.58
batch start
#iterations: 323
currently lose_sum: 94.85284441709518
time_elpased: 1.612
batch start
#iterations: 324
currently lose_sum: 94.29380238056183
time_elpased: 1.572
batch start
#iterations: 325
currently lose_sum: 95.15472519397736
time_elpased: 1.592
batch start
#iterations: 326
currently lose_sum: 94.39535796642303
time_elpased: 1.569
batch start
#iterations: 327
currently lose_sum: 94.97956705093384
time_elpased: 1.551
batch start
#iterations: 328
currently lose_sum: 94.88281136751175
time_elpased: 1.582
batch start
#iterations: 329
currently lose_sum: 94.5725302696228
time_elpased: 1.571
batch start
#iterations: 330
currently lose_sum: 95.13987296819687
time_elpased: 1.586
batch start
#iterations: 331
currently lose_sum: 94.72750294208527
time_elpased: 1.563
batch start
#iterations: 332
currently lose_sum: 94.6725686788559
time_elpased: 1.626
batch start
#iterations: 333
currently lose_sum: 94.79294365644455
time_elpased: 1.597
batch start
#iterations: 334
currently lose_sum: 94.55218660831451
time_elpased: 1.581
batch start
#iterations: 335
currently lose_sum: 94.74550312757492
time_elpased: 1.619
batch start
#iterations: 336
currently lose_sum: 94.71055436134338
time_elpased: 1.635
batch start
#iterations: 337
currently lose_sum: 95.33429354429245
time_elpased: 1.589
batch start
#iterations: 338
currently lose_sum: 94.75332808494568
time_elpased: 1.628
batch start
#iterations: 339
currently lose_sum: 94.77409183979034
time_elpased: 1.603
start validation test
0.659072164948
0.639335428828
0.732633528867
0.682812200269
0.658943016628
61.150
batch start
#iterations: 340
currently lose_sum: 94.50691252946854
time_elpased: 1.646
batch start
#iterations: 341
currently lose_sum: 94.82172429561615
time_elpased: 1.593
batch start
#iterations: 342
currently lose_sum: 95.07444614171982
time_elpased: 1.588
batch start
#iterations: 343
currently lose_sum: 94.75264763832092
time_elpased: 1.621
batch start
#iterations: 344
currently lose_sum: 94.54902195930481
time_elpased: 1.621
batch start
#iterations: 345
currently lose_sum: 94.91604137420654
time_elpased: 1.583
batch start
#iterations: 346
currently lose_sum: 94.73406970500946
time_elpased: 1.58
batch start
#iterations: 347
currently lose_sum: 94.6916675567627
time_elpased: 1.57
batch start
#iterations: 348
currently lose_sum: 94.7383838891983
time_elpased: 1.634
batch start
#iterations: 349
currently lose_sum: 95.34068393707275
time_elpased: 1.582
batch start
#iterations: 350
currently lose_sum: 94.65283727645874
time_elpased: 1.581
batch start
#iterations: 351
currently lose_sum: 94.92844545841217
time_elpased: 1.602
batch start
#iterations: 352
currently lose_sum: 94.70268249511719
time_elpased: 1.581
batch start
#iterations: 353
currently lose_sum: 94.55526369810104
time_elpased: 1.547
batch start
#iterations: 354
currently lose_sum: 95.17814183235168
time_elpased: 1.607
batch start
#iterations: 355
currently lose_sum: 94.62482607364655
time_elpased: 1.605
batch start
#iterations: 356
currently lose_sum: 94.83031487464905
time_elpased: 1.569
batch start
#iterations: 357
currently lose_sum: 94.69337290525436
time_elpased: 1.59
batch start
#iterations: 358
currently lose_sum: 94.7083552479744
time_elpased: 1.627
batch start
#iterations: 359
currently lose_sum: 94.65944516658783
time_elpased: 1.603
start validation test
0.654381443299
0.641834620456
0.701348152722
0.670272928448
0.654298985996
61.238
batch start
#iterations: 360
currently lose_sum: 95.12926912307739
time_elpased: 1.547
batch start
#iterations: 361
currently lose_sum: 95.03364336490631
time_elpased: 1.637
batch start
#iterations: 362
currently lose_sum: 94.602126121521
time_elpased: 1.6
batch start
#iterations: 363
currently lose_sum: 94.75224137306213
time_elpased: 1.601
batch start
#iterations: 364
currently lose_sum: 94.7305606007576
time_elpased: 1.57
batch start
#iterations: 365
currently lose_sum: 95.12391889095306
time_elpased: 1.545
batch start
#iterations: 366
currently lose_sum: 94.67278438806534
time_elpased: 1.54
batch start
#iterations: 367
currently lose_sum: 94.88816171884537
time_elpased: 1.55
batch start
#iterations: 368
currently lose_sum: 94.93080025911331
time_elpased: 1.604
batch start
#iterations: 369
currently lose_sum: 94.60444414615631
time_elpased: 1.596
batch start
#iterations: 370
currently lose_sum: 94.55805951356888
time_elpased: 1.564
batch start
#iterations: 371
currently lose_sum: 94.90641397237778
time_elpased: 1.574
batch start
#iterations: 372
currently lose_sum: 94.45618051290512
time_elpased: 1.575
batch start
#iterations: 373
currently lose_sum: 94.96946692466736
time_elpased: 1.611
batch start
#iterations: 374
currently lose_sum: 94.96554946899414
time_elpased: 1.567
batch start
#iterations: 375
currently lose_sum: 94.42582601308823
time_elpased: 1.576
batch start
#iterations: 376
currently lose_sum: 95.0205591917038
time_elpased: 1.541
batch start
#iterations: 377
currently lose_sum: 94.99468982219696
time_elpased: 1.632
batch start
#iterations: 378
currently lose_sum: 94.83809471130371
time_elpased: 1.57
batch start
#iterations: 379
currently lose_sum: 94.60836619138718
time_elpased: 1.613
start validation test
0.661958762887
0.639052733515
0.747041267881
0.68884038717
0.661809387426
61.054
batch start
#iterations: 380
currently lose_sum: 95.16241538524628
time_elpased: 1.631
batch start
#iterations: 381
currently lose_sum: 94.79278612136841
time_elpased: 1.548
batch start
#iterations: 382
currently lose_sum: 94.77243411540985
time_elpased: 1.588
batch start
#iterations: 383
currently lose_sum: 94.89775490760803
time_elpased: 1.61
batch start
#iterations: 384
currently lose_sum: 95.25013536214828
time_elpased: 1.613
batch start
#iterations: 385
currently lose_sum: 94.98627150058746
time_elpased: 1.57
batch start
#iterations: 386
currently lose_sum: 94.4903199672699
time_elpased: 1.574
batch start
#iterations: 387
currently lose_sum: 95.17630338668823
time_elpased: 1.572
batch start
#iterations: 388
currently lose_sum: 94.77899658679962
time_elpased: 1.551
batch start
#iterations: 389
currently lose_sum: 94.826804459095
time_elpased: 1.58
batch start
#iterations: 390
currently lose_sum: 94.81844872236252
time_elpased: 1.623
batch start
#iterations: 391
currently lose_sum: 94.84834718704224
time_elpased: 1.63
batch start
#iterations: 392
currently lose_sum: 94.61387836933136
time_elpased: 1.6
batch start
#iterations: 393
currently lose_sum: 94.68574446439743
time_elpased: 1.6
batch start
#iterations: 394
currently lose_sum: 94.5429350733757
time_elpased: 1.575
batch start
#iterations: 395
currently lose_sum: 94.58892339468002
time_elpased: 1.603
batch start
#iterations: 396
currently lose_sum: 94.6795688867569
time_elpased: 1.554
batch start
#iterations: 397
currently lose_sum: 94.84903645515442
time_elpased: 1.63
batch start
#iterations: 398
currently lose_sum: 94.67151588201523
time_elpased: 1.568
batch start
#iterations: 399
currently lose_sum: 94.89545518159866
time_elpased: 1.591
start validation test
0.658453608247
0.64282413825
0.715858804158
0.677378517869
0.658352824572
61.260
acc: 0.658
pre: 0.640
rec: 0.725
F1: 0.680
auc: 0.693
