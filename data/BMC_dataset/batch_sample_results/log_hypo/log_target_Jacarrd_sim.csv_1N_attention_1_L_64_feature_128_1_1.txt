start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.4786126613617
time_elpased: 1.846
batch start
#iterations: 1
currently lose_sum: 99.96881449222565
time_elpased: 1.821
batch start
#iterations: 2
currently lose_sum: 99.79766499996185
time_elpased: 1.86
batch start
#iterations: 3
currently lose_sum: 99.44001215696335
time_elpased: 1.799
batch start
#iterations: 4
currently lose_sum: 99.13788920640945
time_elpased: 1.797
batch start
#iterations: 5
currently lose_sum: 98.89019763469696
time_elpased: 1.802
batch start
#iterations: 6
currently lose_sum: 98.8470589518547
time_elpased: 1.904
batch start
#iterations: 7
currently lose_sum: 98.4748193025589
time_elpased: 1.853
batch start
#iterations: 8
currently lose_sum: 98.44529342651367
time_elpased: 1.789
batch start
#iterations: 9
currently lose_sum: 98.20335626602173
time_elpased: 1.856
batch start
#iterations: 10
currently lose_sum: 98.38757610321045
time_elpased: 1.837
batch start
#iterations: 11
currently lose_sum: 97.92765808105469
time_elpased: 1.824
batch start
#iterations: 12
currently lose_sum: 97.81274783611298
time_elpased: 1.823
batch start
#iterations: 13
currently lose_sum: 97.84988337755203
time_elpased: 1.869
batch start
#iterations: 14
currently lose_sum: 97.83864837884903
time_elpased: 1.812
batch start
#iterations: 15
currently lose_sum: 97.7079074382782
time_elpased: 1.836
batch start
#iterations: 16
currently lose_sum: 97.69147729873657
time_elpased: 1.8
batch start
#iterations: 17
currently lose_sum: 97.61543661355972
time_elpased: 1.826
batch start
#iterations: 18
currently lose_sum: 97.20448517799377
time_elpased: 1.786
batch start
#iterations: 19
currently lose_sum: 97.58350414037704
time_elpased: 1.867
start validation test
0.636907216495
0.63591986169
0.643511371823
0.639693094629
0.636895621882
63.168
batch start
#iterations: 20
currently lose_sum: 97.1829679608345
time_elpased: 1.823
batch start
#iterations: 21
currently lose_sum: 97.08612644672394
time_elpased: 1.8
batch start
#iterations: 22
currently lose_sum: 97.21171563863754
time_elpased: 1.793
batch start
#iterations: 23
currently lose_sum: 97.05901724100113
time_elpased: 1.819
batch start
#iterations: 24
currently lose_sum: 97.16626113653183
time_elpased: 1.854
batch start
#iterations: 25
currently lose_sum: 97.17738085985184
time_elpased: 1.839
batch start
#iterations: 26
currently lose_sum: 97.21329355239868
time_elpased: 1.812
batch start
#iterations: 27
currently lose_sum: 97.11490488052368
time_elpased: 1.874
batch start
#iterations: 28
currently lose_sum: 96.91772377490997
time_elpased: 1.802
batch start
#iterations: 29
currently lose_sum: 96.82288843393326
time_elpased: 1.834
batch start
#iterations: 30
currently lose_sum: 96.84431993961334
time_elpased: 1.846
batch start
#iterations: 31
currently lose_sum: 96.97175651788712
time_elpased: 1.809
batch start
#iterations: 32
currently lose_sum: 97.11038988828659
time_elpased: 1.835
batch start
#iterations: 33
currently lose_sum: 96.83428353071213
time_elpased: 1.817
batch start
#iterations: 34
currently lose_sum: 96.8455381989479
time_elpased: 1.821
batch start
#iterations: 35
currently lose_sum: 96.56365591287613
time_elpased: 1.885
batch start
#iterations: 36
currently lose_sum: 96.2126715183258
time_elpased: 1.806
batch start
#iterations: 37
currently lose_sum: 96.77663415670395
time_elpased: 1.877
batch start
#iterations: 38
currently lose_sum: 96.46654343605042
time_elpased: 1.821
batch start
#iterations: 39
currently lose_sum: 96.53207069635391
time_elpased: 1.839
start validation test
0.647577319588
0.631964809384
0.709684058866
0.668573367589
0.647468281628
62.478
batch start
#iterations: 40
currently lose_sum: 96.51732498407364
time_elpased: 1.766
batch start
#iterations: 41
currently lose_sum: 96.50989776849747
time_elpased: 1.806
batch start
#iterations: 42
currently lose_sum: 96.25906091928482
time_elpased: 1.851
batch start
#iterations: 43
currently lose_sum: 96.13104057312012
time_elpased: 1.841
batch start
#iterations: 44
currently lose_sum: 96.42057466506958
time_elpased: 1.822
batch start
#iterations: 45
currently lose_sum: 96.33549398183823
time_elpased: 1.845
batch start
#iterations: 46
currently lose_sum: 96.24280256032944
time_elpased: 1.814
batch start
#iterations: 47
currently lose_sum: 96.27776074409485
time_elpased: 1.874
batch start
#iterations: 48
currently lose_sum: 96.53182768821716
time_elpased: 1.854
batch start
#iterations: 49
currently lose_sum: 96.35848158597946
time_elpased: 1.863
batch start
#iterations: 50
currently lose_sum: 96.08185231685638
time_elpased: 1.841
batch start
#iterations: 51
currently lose_sum: 96.33825957775116
time_elpased: 1.851
batch start
#iterations: 52
currently lose_sum: 96.57474029064178
time_elpased: 1.838
batch start
#iterations: 53
currently lose_sum: 95.95476454496384
time_elpased: 1.843
batch start
#iterations: 54
currently lose_sum: 96.32191771268845
time_elpased: 1.846
batch start
#iterations: 55
currently lose_sum: 96.21414977312088
time_elpased: 1.789
batch start
#iterations: 56
currently lose_sum: 96.08645379543304
time_elpased: 1.814
batch start
#iterations: 57
currently lose_sum: 96.04893243312836
time_elpased: 1.868
batch start
#iterations: 58
currently lose_sum: 96.05595856904984
time_elpased: 1.841
batch start
#iterations: 59
currently lose_sum: 96.12590306997299
time_elpased: 1.831
start validation test
0.646082474227
0.635982066202
0.686117114336
0.660099009901
0.646012187241
62.094
batch start
#iterations: 60
currently lose_sum: 95.86661231517792
time_elpased: 1.843
batch start
#iterations: 61
currently lose_sum: 96.30042362213135
time_elpased: 1.81
batch start
#iterations: 62
currently lose_sum: 95.84468305110931
time_elpased: 1.849
batch start
#iterations: 63
currently lose_sum: 96.20581829547882
time_elpased: 1.81
batch start
#iterations: 64
currently lose_sum: 96.20250535011292
time_elpased: 1.843
batch start
#iterations: 65
currently lose_sum: 95.88197576999664
time_elpased: 1.841
batch start
#iterations: 66
currently lose_sum: 96.11655223369598
time_elpased: 1.834
batch start
#iterations: 67
currently lose_sum: 95.87864422798157
time_elpased: 1.827
batch start
#iterations: 68
currently lose_sum: 95.87635463476181
time_elpased: 1.792
batch start
#iterations: 69
currently lose_sum: 95.97641944885254
time_elpased: 1.854
batch start
#iterations: 70
currently lose_sum: 95.74450409412384
time_elpased: 1.826
batch start
#iterations: 71
currently lose_sum: 95.54928237199783
time_elpased: 1.844
batch start
#iterations: 72
currently lose_sum: 95.74597179889679
time_elpased: 1.819
batch start
#iterations: 73
currently lose_sum: 95.6205250620842
time_elpased: 1.872
batch start
#iterations: 74
currently lose_sum: 95.94019371271133
time_elpased: 1.813
batch start
#iterations: 75
currently lose_sum: 95.72612309455872
time_elpased: 1.831
batch start
#iterations: 76
currently lose_sum: 95.93973207473755
time_elpased: 1.803
batch start
#iterations: 77
currently lose_sum: 96.05443578958511
time_elpased: 1.882
batch start
#iterations: 78
currently lose_sum: 95.88155370950699
time_elpased: 1.857
batch start
#iterations: 79
currently lose_sum: 95.71109223365784
time_elpased: 1.833
start validation test
0.652216494845
0.640332640333
0.697334568282
0.667619094537
0.652137283108
61.789
batch start
#iterations: 80
currently lose_sum: 96.02467554807663
time_elpased: 1.787
batch start
#iterations: 81
currently lose_sum: 96.0683183670044
time_elpased: 1.791
batch start
#iterations: 82
currently lose_sum: 95.99252724647522
time_elpased: 1.854
batch start
#iterations: 83
currently lose_sum: 95.91427981853485
time_elpased: 1.815
batch start
#iterations: 84
currently lose_sum: 95.66632217168808
time_elpased: 1.886
batch start
#iterations: 85
currently lose_sum: 95.56884098052979
time_elpased: 1.851
batch start
#iterations: 86
currently lose_sum: 95.87020045518875
time_elpased: 1.826
batch start
#iterations: 87
currently lose_sum: 95.65520733594894
time_elpased: 1.806
batch start
#iterations: 88
currently lose_sum: 96.10441666841507
time_elpased: 1.818
batch start
#iterations: 89
currently lose_sum: 95.65724921226501
time_elpased: 1.816
batch start
#iterations: 90
currently lose_sum: 95.49923038482666
time_elpased: 1.815
batch start
#iterations: 91
currently lose_sum: 95.79064631462097
time_elpased: 1.821
batch start
#iterations: 92
currently lose_sum: 95.63322132825851
time_elpased: 1.821
batch start
#iterations: 93
currently lose_sum: 96.18987691402435
time_elpased: 1.805
batch start
#iterations: 94
currently lose_sum: 95.57179993391037
time_elpased: 1.796
batch start
#iterations: 95
currently lose_sum: 95.47763931751251
time_elpased: 1.847
batch start
#iterations: 96
currently lose_sum: 95.66451174020767
time_elpased: 1.81
batch start
#iterations: 97
currently lose_sum: 95.52827388048172
time_elpased: 1.84
batch start
#iterations: 98
currently lose_sum: 95.08259356021881
time_elpased: 1.815
batch start
#iterations: 99
currently lose_sum: 95.31600457429886
time_elpased: 1.801
start validation test
0.652577319588
0.641586607058
0.694144283215
0.666831438458
0.652504342372
61.612
batch start
#iterations: 100
currently lose_sum: 95.69807052612305
time_elpased: 1.835
batch start
#iterations: 101
currently lose_sum: 95.84618866443634
time_elpased: 1.846
batch start
#iterations: 102
currently lose_sum: 95.30730932950974
time_elpased: 1.83
batch start
#iterations: 103
currently lose_sum: 95.62464129924774
time_elpased: 1.834
batch start
#iterations: 104
currently lose_sum: 95.68158060312271
time_elpased: 1.821
batch start
#iterations: 105
currently lose_sum: 95.52674853801727
time_elpased: 1.814
batch start
#iterations: 106
currently lose_sum: 95.62334948778152
time_elpased: 1.829
batch start
#iterations: 107
currently lose_sum: 95.64388889074326
time_elpased: 1.823
batch start
#iterations: 108
currently lose_sum: 95.47497648000717
time_elpased: 1.851
batch start
#iterations: 109
currently lose_sum: 95.59903770685196
time_elpased: 1.836
batch start
#iterations: 110
currently lose_sum: 95.35953825712204
time_elpased: 1.849
batch start
#iterations: 111
currently lose_sum: 95.6819896697998
time_elpased: 1.83
batch start
#iterations: 112
currently lose_sum: 95.40371757745743
time_elpased: 1.832
batch start
#iterations: 113
currently lose_sum: 95.43112707138062
time_elpased: 1.839
batch start
#iterations: 114
currently lose_sum: 95.36846834421158
time_elpased: 1.819
batch start
#iterations: 115
currently lose_sum: 95.44537496566772
time_elpased: 1.792
batch start
#iterations: 116
currently lose_sum: 95.56561523675919
time_elpased: 1.827
batch start
#iterations: 117
currently lose_sum: 95.61415249109268
time_elpased: 1.847
batch start
#iterations: 118
currently lose_sum: 95.67019826173782
time_elpased: 1.832
batch start
#iterations: 119
currently lose_sum: 95.44070041179657
time_elpased: 1.804
start validation test
0.65206185567
0.639544727683
0.699701553978
0.668272066051
0.651978216832
61.840
batch start
#iterations: 120
currently lose_sum: 95.1207270026207
time_elpased: 1.854
batch start
#iterations: 121
currently lose_sum: 95.2996654510498
time_elpased: 1.784
batch start
#iterations: 122
currently lose_sum: 95.80423295497894
time_elpased: 1.848
batch start
#iterations: 123
currently lose_sum: 95.70830005407333
time_elpased: 1.86
batch start
#iterations: 124
currently lose_sum: 95.56885606050491
time_elpased: 1.793
batch start
#iterations: 125
currently lose_sum: 95.58045357465744
time_elpased: 1.829
batch start
#iterations: 126
currently lose_sum: 95.51325565576553
time_elpased: 1.847
batch start
#iterations: 127
currently lose_sum: 95.08333784341812
time_elpased: 1.881
batch start
#iterations: 128
currently lose_sum: 95.40145444869995
time_elpased: 1.893
batch start
#iterations: 129
currently lose_sum: 95.39753168821335
time_elpased: 1.831
batch start
#iterations: 130
currently lose_sum: 95.64578968286514
time_elpased: 1.835
batch start
#iterations: 131
currently lose_sum: 95.27701580524445
time_elpased: 1.821
batch start
#iterations: 132
currently lose_sum: 95.64364755153656
time_elpased: 1.833
batch start
#iterations: 133
currently lose_sum: 95.22922587394714
time_elpased: 1.823
batch start
#iterations: 134
currently lose_sum: 95.24879795312881
time_elpased: 1.83
batch start
#iterations: 135
currently lose_sum: 95.33743393421173
time_elpased: 1.831
batch start
#iterations: 136
currently lose_sum: 95.41067206859589
time_elpased: 1.82
batch start
#iterations: 137
currently lose_sum: 95.13747304677963
time_elpased: 1.798
batch start
#iterations: 138
currently lose_sum: 95.53760212659836
time_elpased: 1.823
batch start
#iterations: 139
currently lose_sum: 95.59755623340607
time_elpased: 1.812
start validation test
0.65324742268
0.63432165319
0.726561695997
0.677315680913
0.653118708166
61.774
batch start
#iterations: 140
currently lose_sum: 95.5083674788475
time_elpased: 1.861
batch start
#iterations: 141
currently lose_sum: 95.27492260932922
time_elpased: 1.858
batch start
#iterations: 142
currently lose_sum: 95.62950897216797
time_elpased: 2.042
batch start
#iterations: 143
currently lose_sum: 94.9529600739479
time_elpased: 1.841
batch start
#iterations: 144
currently lose_sum: 95.3572084903717
time_elpased: 1.855
batch start
#iterations: 145
currently lose_sum: 95.62394398450851
time_elpased: 1.826
batch start
#iterations: 146
currently lose_sum: 95.2890152335167
time_elpased: 1.838
batch start
#iterations: 147
currently lose_sum: 95.11158114671707
time_elpased: 1.914
batch start
#iterations: 148
currently lose_sum: 95.09233671426773
time_elpased: 1.861
batch start
#iterations: 149
currently lose_sum: 95.19865602254868
time_elpased: 1.841
batch start
#iterations: 150
currently lose_sum: 95.11582839488983
time_elpased: 1.83
batch start
#iterations: 151
currently lose_sum: 95.50178772211075
time_elpased: 1.821
batch start
#iterations: 152
currently lose_sum: 95.46879887580872
time_elpased: 1.883
batch start
#iterations: 153
currently lose_sum: 94.92009729146957
time_elpased: 1.816
batch start
#iterations: 154
currently lose_sum: 95.55578595399857
time_elpased: 1.846
batch start
#iterations: 155
currently lose_sum: 95.59367436170578
time_elpased: 1.764
batch start
#iterations: 156
currently lose_sum: 95.48226577043533
time_elpased: 1.846
batch start
#iterations: 157
currently lose_sum: 95.59400224685669
time_elpased: 1.863
batch start
#iterations: 158
currently lose_sum: 95.56430846452713
time_elpased: 1.816
batch start
#iterations: 159
currently lose_sum: 95.58883136510849
time_elpased: 1.786
start validation test
0.656340206186
0.640294388224
0.716270453844
0.676154855006
0.656234989392
61.608
batch start
#iterations: 160
currently lose_sum: 95.27521812915802
time_elpased: 1.804
batch start
#iterations: 161
currently lose_sum: 95.04981964826584
time_elpased: 1.846
batch start
#iterations: 162
currently lose_sum: 95.47523337602615
time_elpased: 1.83
batch start
#iterations: 163
currently lose_sum: 94.87795090675354
time_elpased: 1.849
batch start
#iterations: 164
currently lose_sum: 95.06263893842697
time_elpased: 1.847
batch start
#iterations: 165
currently lose_sum: 95.38192284107208
time_elpased: 1.848
batch start
#iterations: 166
currently lose_sum: 95.18379551172256
time_elpased: 1.848
batch start
#iterations: 167
currently lose_sum: 95.11358743906021
time_elpased: 1.822
batch start
#iterations: 168
currently lose_sum: 95.34224885702133
time_elpased: 1.811
batch start
#iterations: 169
currently lose_sum: 95.29490715265274
time_elpased: 1.849
batch start
#iterations: 170
currently lose_sum: 94.7946771979332
time_elpased: 1.856
batch start
#iterations: 171
currently lose_sum: 95.43239241838455
time_elpased: 1.84
batch start
#iterations: 172
currently lose_sum: 95.24641859531403
time_elpased: 1.817
batch start
#iterations: 173
currently lose_sum: 95.47491002082825
time_elpased: 1.883
batch start
#iterations: 174
currently lose_sum: 95.07584631443024
time_elpased: 1.779
batch start
#iterations: 175
currently lose_sum: 94.82654654979706
time_elpased: 1.854
batch start
#iterations: 176
currently lose_sum: 95.04527872800827
time_elpased: 1.837
batch start
#iterations: 177
currently lose_sum: 95.30462259054184
time_elpased: 1.831
batch start
#iterations: 178
currently lose_sum: 95.28192090988159
time_elpased: 1.808
batch start
#iterations: 179
currently lose_sum: 95.1941745877266
time_elpased: 1.819
start validation test
0.652010309278
0.640355858414
0.696305444067
0.667159690381
0.651932542337
61.706
batch start
#iterations: 180
currently lose_sum: 95.13611668348312
time_elpased: 1.836
batch start
#iterations: 181
currently lose_sum: 95.19513869285583
time_elpased: 1.825
batch start
#iterations: 182
currently lose_sum: 95.13166809082031
time_elpased: 1.822
batch start
#iterations: 183
currently lose_sum: 94.82584083080292
time_elpased: 1.85
batch start
#iterations: 184
currently lose_sum: 95.11579215526581
time_elpased: 1.861
batch start
#iterations: 185
currently lose_sum: 95.15475165843964
time_elpased: 1.819
batch start
#iterations: 186
currently lose_sum: 95.08096706867218
time_elpased: 1.819
batch start
#iterations: 187
currently lose_sum: 95.21898394823074
time_elpased: 1.8
batch start
#iterations: 188
currently lose_sum: 95.22807723283768
time_elpased: 1.828
batch start
#iterations: 189
currently lose_sum: 95.3112331032753
time_elpased: 1.828
batch start
#iterations: 190
currently lose_sum: 95.1714499592781
time_elpased: 1.82
batch start
#iterations: 191
currently lose_sum: 94.97942066192627
time_elpased: 1.842
batch start
#iterations: 192
currently lose_sum: 95.26155757904053
time_elpased: 1.854
batch start
#iterations: 193
currently lose_sum: 95.13870471715927
time_elpased: 1.818
batch start
#iterations: 194
currently lose_sum: 94.99156606197357
time_elpased: 1.84
batch start
#iterations: 195
currently lose_sum: 95.3449359536171
time_elpased: 1.841
batch start
#iterations: 196
currently lose_sum: 95.4491748213768
time_elpased: 1.925
batch start
#iterations: 197
currently lose_sum: 94.95533502101898
time_elpased: 1.809
batch start
#iterations: 198
currently lose_sum: 95.08732050657272
time_elpased: 1.859
batch start
#iterations: 199
currently lose_sum: 95.16498947143555
time_elpased: 1.766
start validation test
0.654639175258
0.642271055362
0.700833590614
0.670275590551
0.654558073837
61.542
batch start
#iterations: 200
currently lose_sum: 95.25887727737427
time_elpased: 1.815
batch start
#iterations: 201
currently lose_sum: 94.8203746676445
time_elpased: 1.814
batch start
#iterations: 202
currently lose_sum: 94.70339781045914
time_elpased: 1.789
batch start
#iterations: 203
currently lose_sum: 94.85872966051102
time_elpased: 1.808
batch start
#iterations: 204
currently lose_sum: 95.02457308769226
time_elpased: 1.816
batch start
#iterations: 205
currently lose_sum: 95.01774847507477
time_elpased: 1.819
batch start
#iterations: 206
currently lose_sum: 94.81335920095444
time_elpased: 1.792
batch start
#iterations: 207
currently lose_sum: 94.92877531051636
time_elpased: 1.829
batch start
#iterations: 208
currently lose_sum: 95.07133108377457
time_elpased: 1.825
batch start
#iterations: 209
currently lose_sum: 94.78257030248642
time_elpased: 1.804
batch start
#iterations: 210
currently lose_sum: 94.80509531497955
time_elpased: 1.852
batch start
#iterations: 211
currently lose_sum: 94.75336045026779
time_elpased: 1.824
batch start
#iterations: 212
currently lose_sum: 94.96222275495529
time_elpased: 1.794
batch start
#iterations: 213
currently lose_sum: 94.94200319051743
time_elpased: 1.822
batch start
#iterations: 214
currently lose_sum: 94.82536286115646
time_elpased: 1.848
batch start
#iterations: 215
currently lose_sum: 94.88857561349869
time_elpased: 1.802
batch start
#iterations: 216
currently lose_sum: 95.05536144971848
time_elpased: 1.804
batch start
#iterations: 217
currently lose_sum: 95.32707458734512
time_elpased: 1.841
batch start
#iterations: 218
currently lose_sum: 94.60112297534943
time_elpased: 1.847
batch start
#iterations: 219
currently lose_sum: 95.26492816209793
time_elpased: 1.829
start validation test
0.655670103093
0.640536788524
0.712256869404
0.674495663191
0.655570756297
61.477
batch start
#iterations: 220
currently lose_sum: 95.19963026046753
time_elpased: 1.779
batch start
#iterations: 221
currently lose_sum: 95.09764641523361
time_elpased: 1.817
batch start
#iterations: 222
currently lose_sum: 95.15616625547409
time_elpased: 1.867
batch start
#iterations: 223
currently lose_sum: 95.04850190877914
time_elpased: 1.827
batch start
#iterations: 224
currently lose_sum: 94.748790204525
time_elpased: 1.824
batch start
#iterations: 225
currently lose_sum: 94.88656783103943
time_elpased: 1.84
batch start
#iterations: 226
currently lose_sum: 94.44446194171906
time_elpased: 1.824
batch start
#iterations: 227
currently lose_sum: 95.29679876565933
time_elpased: 1.834
batch start
#iterations: 228
currently lose_sum: 94.91444724798203
time_elpased: 1.836
batch start
#iterations: 229
currently lose_sum: 94.7974563241005
time_elpased: 1.828
batch start
#iterations: 230
currently lose_sum: 94.81652522087097
time_elpased: 1.841
batch start
#iterations: 231
currently lose_sum: 94.98732763528824
time_elpased: 1.845
batch start
#iterations: 232
currently lose_sum: 95.20083689689636
time_elpased: 1.857
batch start
#iterations: 233
currently lose_sum: 95.15357255935669
time_elpased: 1.831
batch start
#iterations: 234
currently lose_sum: 94.64888441562653
time_elpased: 1.856
batch start
#iterations: 235
currently lose_sum: 95.10416030883789
time_elpased: 1.816
batch start
#iterations: 236
currently lose_sum: 95.41058307886124
time_elpased: 1.823
batch start
#iterations: 237
currently lose_sum: 95.14262473583221
time_elpased: 1.814
batch start
#iterations: 238
currently lose_sum: 94.92517518997192
time_elpased: 1.876
batch start
#iterations: 239
currently lose_sum: 95.20773071050644
time_elpased: 1.813
start validation test
0.655309278351
0.644505913773
0.695379232273
0.668976783328
0.655238929366
61.633
batch start
#iterations: 240
currently lose_sum: 94.65579891204834
time_elpased: 1.851
batch start
#iterations: 241
currently lose_sum: 94.93577772378922
time_elpased: 1.87
batch start
#iterations: 242
currently lose_sum: 95.06733298301697
time_elpased: 1.809
batch start
#iterations: 243
currently lose_sum: 95.40213656425476
time_elpased: 1.851
batch start
#iterations: 244
currently lose_sum: 94.6223623752594
time_elpased: 1.855
batch start
#iterations: 245
currently lose_sum: 95.27341520786285
time_elpased: 1.803
batch start
#iterations: 246
currently lose_sum: 94.77710038423538
time_elpased: 1.828
batch start
#iterations: 247
currently lose_sum: 95.13840281963348
time_elpased: 1.818
batch start
#iterations: 248
currently lose_sum: 94.7848539352417
time_elpased: 1.861
batch start
#iterations: 249
currently lose_sum: 95.00650584697723
time_elpased: 1.813
batch start
#iterations: 250
currently lose_sum: 95.13408720493317
time_elpased: 1.827
batch start
#iterations: 251
currently lose_sum: 94.90815544128418
time_elpased: 1.842
batch start
#iterations: 252
currently lose_sum: 94.72964286804199
time_elpased: 1.844
batch start
#iterations: 253
currently lose_sum: 95.14970952272415
time_elpased: 1.805
batch start
#iterations: 254
currently lose_sum: 94.47638219594955
time_elpased: 1.82
batch start
#iterations: 255
currently lose_sum: 94.76099336147308
time_elpased: 1.801
batch start
#iterations: 256
currently lose_sum: 94.66517525911331
time_elpased: 1.883
batch start
#iterations: 257
currently lose_sum: 94.98307192325592
time_elpased: 1.815
batch start
#iterations: 258
currently lose_sum: 94.63569700717926
time_elpased: 1.817
batch start
#iterations: 259
currently lose_sum: 94.64291441440582
time_elpased: 1.825
start validation test
0.656237113402
0.641688359985
0.710301533395
0.674253895374
0.656142194974
61.383
batch start
#iterations: 260
currently lose_sum: 94.95621025562286
time_elpased: 1.857
batch start
#iterations: 261
currently lose_sum: 94.57540506124496
time_elpased: 1.799
batch start
#iterations: 262
currently lose_sum: 94.60049092769623
time_elpased: 1.825
batch start
#iterations: 263
currently lose_sum: 94.86818903684616
time_elpased: 1.829
batch start
#iterations: 264
currently lose_sum: 95.20508676767349
time_elpased: 1.837
batch start
#iterations: 265
currently lose_sum: 95.06303882598877
time_elpased: 1.884
batch start
#iterations: 266
currently lose_sum: 94.86856800317764
time_elpased: 1.839
batch start
#iterations: 267
currently lose_sum: 94.98135185241699
time_elpased: 1.802
batch start
#iterations: 268
currently lose_sum: 95.04108810424805
time_elpased: 1.847
batch start
#iterations: 269
currently lose_sum: 94.94574826955795
time_elpased: 1.796
batch start
#iterations: 270
currently lose_sum: 95.44929367303848
time_elpased: 1.852
batch start
#iterations: 271
currently lose_sum: 94.45915424823761
time_elpased: 1.856
batch start
#iterations: 272
currently lose_sum: 95.02837979793549
time_elpased: 1.863
batch start
#iterations: 273
currently lose_sum: 95.02496653795242
time_elpased: 1.771
batch start
#iterations: 274
currently lose_sum: 94.95460391044617
time_elpased: 1.836
batch start
#iterations: 275
currently lose_sum: 95.1151482462883
time_elpased: 1.879
batch start
#iterations: 276
currently lose_sum: 94.73237895965576
time_elpased: 1.841
batch start
#iterations: 277
currently lose_sum: 94.54926347732544
time_elpased: 1.919
batch start
#iterations: 278
currently lose_sum: 94.49094378948212
time_elpased: 1.835
batch start
#iterations: 279
currently lose_sum: 95.05529570579529
time_elpased: 1.836
start validation test
0.659381443299
0.641100117999
0.726870433261
0.681296421337
0.659262955968
61.296
batch start
#iterations: 280
currently lose_sum: 94.83940237760544
time_elpased: 1.807
batch start
#iterations: 281
currently lose_sum: 94.9252917766571
time_elpased: 1.834
batch start
#iterations: 282
currently lose_sum: 94.90199905633926
time_elpased: 1.844
batch start
#iterations: 283
currently lose_sum: 95.12474584579468
time_elpased: 1.82
batch start
#iterations: 284
currently lose_sum: 94.85995000600815
time_elpased: 1.845
batch start
#iterations: 285
currently lose_sum: 94.4636287689209
time_elpased: 1.828
batch start
#iterations: 286
currently lose_sum: 94.70157563686371
time_elpased: 1.818
batch start
#iterations: 287
currently lose_sum: 94.75039410591125
time_elpased: 1.8
batch start
#iterations: 288
currently lose_sum: 94.70113378763199
time_elpased: 1.82
batch start
#iterations: 289
currently lose_sum: 95.14497941732407
time_elpased: 1.821
batch start
#iterations: 290
currently lose_sum: 94.80226069688797
time_elpased: 1.861
batch start
#iterations: 291
currently lose_sum: 94.75903022289276
time_elpased: 1.858
batch start
#iterations: 292
currently lose_sum: 94.77945125102997
time_elpased: 1.782
batch start
#iterations: 293
currently lose_sum: 94.90307706594467
time_elpased: 1.784
batch start
#iterations: 294
currently lose_sum: 94.8055225610733
time_elpased: 1.835
batch start
#iterations: 295
currently lose_sum: 95.07149910926819
time_elpased: 1.79
batch start
#iterations: 296
currently lose_sum: 94.47950565814972
time_elpased: 1.864
batch start
#iterations: 297
currently lose_sum: 94.90877681970596
time_elpased: 1.812
batch start
#iterations: 298
currently lose_sum: 94.37915080785751
time_elpased: 1.806
batch start
#iterations: 299
currently lose_sum: 94.73700892925262
time_elpased: 1.825
start validation test
0.652886597938
0.638319577112
0.708346197386
0.671512195122
0.652789230057
61.388
batch start
#iterations: 300
currently lose_sum: 94.67008626461029
time_elpased: 1.853
batch start
#iterations: 301
currently lose_sum: 95.06630790233612
time_elpased: 1.818
batch start
#iterations: 302
currently lose_sum: 94.54027408361435
time_elpased: 1.81
batch start
#iterations: 303
currently lose_sum: 94.61070340871811
time_elpased: 1.837
batch start
#iterations: 304
currently lose_sum: 94.8422223329544
time_elpased: 1.846
batch start
#iterations: 305
currently lose_sum: 94.82526141405106
time_elpased: 1.816
batch start
#iterations: 306
currently lose_sum: 94.69655078649521
time_elpased: 1.819
batch start
#iterations: 307
currently lose_sum: 95.1390832066536
time_elpased: 1.832
batch start
#iterations: 308
currently lose_sum: 94.56609326601028
time_elpased: 1.811
batch start
#iterations: 309
currently lose_sum: 94.62663596868515
time_elpased: 1.859
batch start
#iterations: 310
currently lose_sum: 95.01314800977707
time_elpased: 1.832
batch start
#iterations: 311
currently lose_sum: 94.63503968715668
time_elpased: 1.885
batch start
#iterations: 312
currently lose_sum: 95.01497530937195
time_elpased: 1.855
batch start
#iterations: 313
currently lose_sum: 94.69061827659607
time_elpased: 1.798
batch start
#iterations: 314
currently lose_sum: 94.58997076749802
time_elpased: 1.858
batch start
#iterations: 315
currently lose_sum: 94.68285328149796
time_elpased: 1.821
batch start
#iterations: 316
currently lose_sum: 94.18653607368469
time_elpased: 1.826
batch start
#iterations: 317
currently lose_sum: 95.16954934597015
time_elpased: 1.816
batch start
#iterations: 318
currently lose_sum: 94.91504949331284
time_elpased: 1.837
batch start
#iterations: 319
currently lose_sum: 95.22863912582397
time_elpased: 1.877
start validation test
0.654948453608
0.639527370073
0.712977256355
0.674257907543
0.654846575095
61.530
batch start
#iterations: 320
currently lose_sum: 94.77943360805511
time_elpased: 1.786
batch start
#iterations: 321
currently lose_sum: 94.93325626850128
time_elpased: 1.781
batch start
#iterations: 322
currently lose_sum: 95.12851524353027
time_elpased: 1.873
batch start
#iterations: 323
currently lose_sum: 94.73169261217117
time_elpased: 1.813
batch start
#iterations: 324
currently lose_sum: 94.67294907569885
time_elpased: 1.808
batch start
#iterations: 325
currently lose_sum: 94.76231509447098
time_elpased: 1.806
batch start
#iterations: 326
currently lose_sum: 95.02915793657303
time_elpased: 1.893
batch start
#iterations: 327
currently lose_sum: 94.66151595115662
time_elpased: 1.832
batch start
#iterations: 328
currently lose_sum: 94.56406831741333
time_elpased: 1.797
batch start
#iterations: 329
currently lose_sum: 94.43040823936462
time_elpased: 1.81
batch start
#iterations: 330
currently lose_sum: 94.23563224077225
time_elpased: 1.835
batch start
#iterations: 331
currently lose_sum: 94.573646068573
time_elpased: 1.803
batch start
#iterations: 332
currently lose_sum: 94.64140111207962
time_elpased: 1.889
batch start
#iterations: 333
currently lose_sum: 94.15620279312134
time_elpased: 1.867
batch start
#iterations: 334
currently lose_sum: 95.17196935415268
time_elpased: 1.785
batch start
#iterations: 335
currently lose_sum: 94.68121939897537
time_elpased: 1.853
batch start
#iterations: 336
currently lose_sum: 94.6479526758194
time_elpased: 1.86
batch start
#iterations: 337
currently lose_sum: 94.73527336120605
time_elpased: 1.808
batch start
#iterations: 338
currently lose_sum: 94.84232431650162
time_elpased: 1.821
batch start
#iterations: 339
currently lose_sum: 94.40681338310242
time_elpased: 1.877
start validation test
0.651288659794
0.643719571568
0.68035401873
0.661529994496
0.651237631073
61.560
batch start
#iterations: 340
currently lose_sum: 94.83042001724243
time_elpased: 1.877
batch start
#iterations: 341
currently lose_sum: 95.10438615083694
time_elpased: 1.852
batch start
#iterations: 342
currently lose_sum: 94.57580709457397
time_elpased: 1.786
batch start
#iterations: 343
currently lose_sum: 94.6978108882904
time_elpased: 1.841
batch start
#iterations: 344
currently lose_sum: 94.72965788841248
time_elpased: 1.849
batch start
#iterations: 345
currently lose_sum: 94.83652526140213
time_elpased: 1.83
batch start
#iterations: 346
currently lose_sum: 94.50308835506439
time_elpased: 1.822
batch start
#iterations: 347
currently lose_sum: 95.01957929134369
time_elpased: 1.822
batch start
#iterations: 348
currently lose_sum: 95.06855165958405
time_elpased: 1.822
batch start
#iterations: 349
currently lose_sum: 94.43937474489212
time_elpased: 1.878
batch start
#iterations: 350
currently lose_sum: 94.8570801615715
time_elpased: 1.826
batch start
#iterations: 351
currently lose_sum: 95.05333858728409
time_elpased: 1.84
batch start
#iterations: 352
currently lose_sum: 95.2308047413826
time_elpased: 1.85
batch start
#iterations: 353
currently lose_sum: 95.28969430923462
time_elpased: 1.806
batch start
#iterations: 354
currently lose_sum: 94.60906875133514
time_elpased: 1.813
batch start
#iterations: 355
currently lose_sum: 95.34311258792877
time_elpased: 1.801
batch start
#iterations: 356
currently lose_sum: 94.81424725055695
time_elpased: 1.826
batch start
#iterations: 357
currently lose_sum: 94.65745949745178
time_elpased: 1.827
batch start
#iterations: 358
currently lose_sum: 94.83309870958328
time_elpased: 1.833
batch start
#iterations: 359
currently lose_sum: 94.79356849193573
time_elpased: 1.829
start validation test
0.65324742268
0.643832980566
0.688689924874
0.665506439262
0.653185197901
61.732
batch start
#iterations: 360
currently lose_sum: 94.59724420309067
time_elpased: 1.86
batch start
#iterations: 361
currently lose_sum: 95.34277510643005
time_elpased: 1.834
batch start
#iterations: 362
currently lose_sum: 94.44933533668518
time_elpased: 1.861
batch start
#iterations: 363
currently lose_sum: 94.98779356479645
time_elpased: 1.826
batch start
#iterations: 364
currently lose_sum: 95.57289016246796
time_elpased: 1.863
batch start
#iterations: 365
currently lose_sum: 94.18672376871109
time_elpased: 1.858
batch start
#iterations: 366
currently lose_sum: 94.6828790307045
time_elpased: 1.879
batch start
#iterations: 367
currently lose_sum: 94.50832223892212
time_elpased: 1.806
batch start
#iterations: 368
currently lose_sum: 94.71695071458817
time_elpased: 1.819
batch start
#iterations: 369
currently lose_sum: 94.89434123039246
time_elpased: 1.846
batch start
#iterations: 370
currently lose_sum: 94.91174912452698
time_elpased: 1.889
batch start
#iterations: 371
currently lose_sum: 94.68994837999344
time_elpased: 1.819
batch start
#iterations: 372
currently lose_sum: 94.72308975458145
time_elpased: 1.818
batch start
#iterations: 373
currently lose_sum: 94.60487502813339
time_elpased: 1.898
batch start
#iterations: 374
currently lose_sum: 94.40915882587433
time_elpased: 1.795
batch start
#iterations: 375
currently lose_sum: 95.25060850381851
time_elpased: 1.835
batch start
#iterations: 376
currently lose_sum: 94.33717185258865
time_elpased: 1.802
batch start
#iterations: 377
currently lose_sum: 94.89369690418243
time_elpased: 1.813
batch start
#iterations: 378
currently lose_sum: 94.60886853933334
time_elpased: 1.865
batch start
#iterations: 379
currently lose_sum: 94.74603825807571
time_elpased: 1.818
start validation test
0.654536082474
0.635945531608
0.725738396624
0.677881380371
0.65441107583
61.491
batch start
#iterations: 380
currently lose_sum: 95.04371726512909
time_elpased: 1.832
batch start
#iterations: 381
currently lose_sum: 94.17436397075653
time_elpased: 1.819
batch start
#iterations: 382
currently lose_sum: 94.57156801223755
time_elpased: 1.856
batch start
#iterations: 383
currently lose_sum: 94.95914721488953
time_elpased: 1.826
batch start
#iterations: 384
currently lose_sum: 94.50720858573914
time_elpased: 1.856
batch start
#iterations: 385
currently lose_sum: 94.57276690006256
time_elpased: 1.81
batch start
#iterations: 386
currently lose_sum: 94.7935454249382
time_elpased: 1.888
batch start
#iterations: 387
currently lose_sum: 94.54370230436325
time_elpased: 1.806
batch start
#iterations: 388
currently lose_sum: 94.7305719256401
time_elpased: 1.833
batch start
#iterations: 389
currently lose_sum: 94.48160737752914
time_elpased: 1.827
batch start
#iterations: 390
currently lose_sum: 95.03428268432617
time_elpased: 1.829
batch start
#iterations: 391
currently lose_sum: 94.44207608699799
time_elpased: 1.816
batch start
#iterations: 392
currently lose_sum: 94.54690474271774
time_elpased: 1.88
batch start
#iterations: 393
currently lose_sum: 94.74648559093475
time_elpased: 1.808
batch start
#iterations: 394
currently lose_sum: 94.41846883296967
time_elpased: 1.767
batch start
#iterations: 395
currently lose_sum: 94.64747935533524
time_elpased: 1.802
batch start
#iterations: 396
currently lose_sum: 94.67219388484955
time_elpased: 1.829
batch start
#iterations: 397
currently lose_sum: 94.32614934444427
time_elpased: 1.803
batch start
#iterations: 398
currently lose_sum: 94.76243287324905
time_elpased: 1.907
batch start
#iterations: 399
currently lose_sum: 94.72776907682419
time_elpased: 1.846
start validation test
0.65675257732
0.64223255814
0.710507358238
0.67464699272
0.656658202511
61.490
acc: 0.660
pre: 0.643
rec: 0.723
F1: 0.680
auc: 0.699
