start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.49737334251404
time_elpased: 1.494
batch start
#iterations: 1
currently lose_sum: 99.98304748535156
time_elpased: 1.49
batch start
#iterations: 2
currently lose_sum: 99.83293461799622
time_elpased: 1.498
batch start
#iterations: 3
currently lose_sum: 99.46648383140564
time_elpased: 1.454
batch start
#iterations: 4
currently lose_sum: 99.31472271680832
time_elpased: 1.451
batch start
#iterations: 5
currently lose_sum: 98.98462778329849
time_elpased: 1.449
batch start
#iterations: 6
currently lose_sum: 98.85246175527573
time_elpased: 1.457
batch start
#iterations: 7
currently lose_sum: 98.47488164901733
time_elpased: 1.458
batch start
#iterations: 8
currently lose_sum: 98.49767136573792
time_elpased: 1.468
batch start
#iterations: 9
currently lose_sum: 98.26085704565048
time_elpased: 1.434
batch start
#iterations: 10
currently lose_sum: 98.23248273134232
time_elpased: 1.494
batch start
#iterations: 11
currently lose_sum: 97.8614729642868
time_elpased: 1.457
batch start
#iterations: 12
currently lose_sum: 98.0760948061943
time_elpased: 1.448
batch start
#iterations: 13
currently lose_sum: 97.85923904180527
time_elpased: 1.448
batch start
#iterations: 14
currently lose_sum: 97.86279636621475
time_elpased: 1.463
batch start
#iterations: 15
currently lose_sum: 97.70288300514221
time_elpased: 1.465
batch start
#iterations: 16
currently lose_sum: 97.83168691396713
time_elpased: 1.513
batch start
#iterations: 17
currently lose_sum: 97.6761069893837
time_elpased: 1.473
batch start
#iterations: 18
currently lose_sum: 97.42429685592651
time_elpased: 1.466
batch start
#iterations: 19
currently lose_sum: 97.49608212709427
time_elpased: 1.453
start validation test
0.642268041237
0.640664572992
0.650818153751
0.645701449867
0.642253030196
63.087
batch start
#iterations: 20
currently lose_sum: 97.4906342625618
time_elpased: 1.475
batch start
#iterations: 21
currently lose_sum: 97.17415773868561
time_elpased: 1.448
batch start
#iterations: 22
currently lose_sum: 97.2493765950203
time_elpased: 1.444
batch start
#iterations: 23
currently lose_sum: 97.22884583473206
time_elpased: 1.515
batch start
#iterations: 24
currently lose_sum: 97.00576603412628
time_elpased: 1.444
batch start
#iterations: 25
currently lose_sum: 96.86633479595184
time_elpased: 1.517
batch start
#iterations: 26
currently lose_sum: 96.9451367855072
time_elpased: 1.479
batch start
#iterations: 27
currently lose_sum: 96.8964341878891
time_elpased: 1.452
batch start
#iterations: 28
currently lose_sum: 96.76274627447128
time_elpased: 1.475
batch start
#iterations: 29
currently lose_sum: 97.01091194152832
time_elpased: 1.511
batch start
#iterations: 30
currently lose_sum: 96.82873183488846
time_elpased: 1.469
batch start
#iterations: 31
currently lose_sum: 96.64468693733215
time_elpased: 1.435
batch start
#iterations: 32
currently lose_sum: 96.24522662162781
time_elpased: 1.491
batch start
#iterations: 33
currently lose_sum: 96.7042486667633
time_elpased: 1.445
batch start
#iterations: 34
currently lose_sum: 96.8759970664978
time_elpased: 1.497
batch start
#iterations: 35
currently lose_sum: 96.78808170557022
time_elpased: 1.46
batch start
#iterations: 36
currently lose_sum: 96.77701473236084
time_elpased: 1.474
batch start
#iterations: 37
currently lose_sum: 96.9775921702385
time_elpased: 1.484
batch start
#iterations: 38
currently lose_sum: 96.42357009649277
time_elpased: 1.446
batch start
#iterations: 39
currently lose_sum: 96.62849062681198
time_elpased: 1.447
start validation test
0.648711340206
0.636192979163
0.697540393125
0.665455795003
0.648625613272
62.365
batch start
#iterations: 40
currently lose_sum: 96.6617003083229
time_elpased: 1.452
batch start
#iterations: 41
currently lose_sum: 96.5651478767395
time_elpased: 1.479
batch start
#iterations: 42
currently lose_sum: 96.48905974626541
time_elpased: 1.454
batch start
#iterations: 43
currently lose_sum: 96.4321916103363
time_elpased: 1.451
batch start
#iterations: 44
currently lose_sum: 96.64393275976181
time_elpased: 1.459
batch start
#iterations: 45
currently lose_sum: 96.52421540021896
time_elpased: 1.457
batch start
#iterations: 46
currently lose_sum: 96.33647727966309
time_elpased: 1.483
batch start
#iterations: 47
currently lose_sum: 96.13703399896622
time_elpased: 1.463
batch start
#iterations: 48
currently lose_sum: 96.32109171152115
time_elpased: 1.486
batch start
#iterations: 49
currently lose_sum: 96.68772542476654
time_elpased: 1.463
batch start
#iterations: 50
currently lose_sum: 96.66635584831238
time_elpased: 1.483
batch start
#iterations: 51
currently lose_sum: 96.1574826836586
time_elpased: 1.461
batch start
#iterations: 52
currently lose_sum: 95.93233674764633
time_elpased: 1.441
batch start
#iterations: 53
currently lose_sum: 96.29974287748337
time_elpased: 1.517
batch start
#iterations: 54
currently lose_sum: 96.73778367042542
time_elpased: 1.486
batch start
#iterations: 55
currently lose_sum: 96.16845619678497
time_elpased: 1.486
batch start
#iterations: 56
currently lose_sum: 96.65826910734177
time_elpased: 1.46
batch start
#iterations: 57
currently lose_sum: 95.9856048822403
time_elpased: 1.463
batch start
#iterations: 58
currently lose_sum: 96.40861678123474
time_elpased: 1.439
batch start
#iterations: 59
currently lose_sum: 96.10497945547104
time_elpased: 1.456
start validation test
0.649381443299
0.629751624677
0.728002469898
0.675322195704
0.64924341196
62.086
batch start
#iterations: 60
currently lose_sum: 96.46494424343109
time_elpased: 1.474
batch start
#iterations: 61
currently lose_sum: 96.30284082889557
time_elpased: 1.436
batch start
#iterations: 62
currently lose_sum: 96.20772463083267
time_elpased: 1.49
batch start
#iterations: 63
currently lose_sum: 96.38051480054855
time_elpased: 1.431
batch start
#iterations: 64
currently lose_sum: 96.52600169181824
time_elpased: 1.45
batch start
#iterations: 65
currently lose_sum: 95.91765016317368
time_elpased: 1.43
batch start
#iterations: 66
currently lose_sum: 95.65868073701859
time_elpased: 1.46
batch start
#iterations: 67
currently lose_sum: 95.93937963247299
time_elpased: 1.448
batch start
#iterations: 68
currently lose_sum: 96.14748251438141
time_elpased: 1.456
batch start
#iterations: 69
currently lose_sum: 96.31333792209625
time_elpased: 1.521
batch start
#iterations: 70
currently lose_sum: 95.80800664424896
time_elpased: 1.44
batch start
#iterations: 71
currently lose_sum: 95.82607048749924
time_elpased: 1.466
batch start
#iterations: 72
currently lose_sum: 95.65328347682953
time_elpased: 1.484
batch start
#iterations: 73
currently lose_sum: 95.48253256082535
time_elpased: 1.505
batch start
#iterations: 74
currently lose_sum: 95.67670601606369
time_elpased: 1.432
batch start
#iterations: 75
currently lose_sum: 95.4679781794548
time_elpased: 1.45
batch start
#iterations: 76
currently lose_sum: 95.88833266496658
time_elpased: 1.472
batch start
#iterations: 77
currently lose_sum: 95.82113361358643
time_elpased: 1.463
batch start
#iterations: 78
currently lose_sum: 95.56638246774673
time_elpased: 1.445
batch start
#iterations: 79
currently lose_sum: 95.89700317382812
time_elpased: 1.414
start validation test
0.649432989691
0.633516483516
0.71194813214
0.670446285797
0.649323234716
61.802
batch start
#iterations: 80
currently lose_sum: 95.53690135478973
time_elpased: 1.452
batch start
#iterations: 81
currently lose_sum: 96.15708285570145
time_elpased: 1.443
batch start
#iterations: 82
currently lose_sum: 95.53910559415817
time_elpased: 1.473
batch start
#iterations: 83
currently lose_sum: 95.96315670013428
time_elpased: 1.486
batch start
#iterations: 84
currently lose_sum: 95.84283632040024
time_elpased: 1.461
batch start
#iterations: 85
currently lose_sum: 96.08031034469604
time_elpased: 1.464
batch start
#iterations: 86
currently lose_sum: 95.7578477859497
time_elpased: 1.462
batch start
#iterations: 87
currently lose_sum: 95.43539303541183
time_elpased: 1.458
batch start
#iterations: 88
currently lose_sum: 95.67389279603958
time_elpased: 1.431
batch start
#iterations: 89
currently lose_sum: 95.76308023929596
time_elpased: 1.47
batch start
#iterations: 90
currently lose_sum: 95.95397448539734
time_elpased: 1.432
batch start
#iterations: 91
currently lose_sum: 95.60776191949844
time_elpased: 1.418
batch start
#iterations: 92
currently lose_sum: 95.51855301856995
time_elpased: 1.45
batch start
#iterations: 93
currently lose_sum: 95.54971128702164
time_elpased: 1.441
batch start
#iterations: 94
currently lose_sum: 95.85791480541229
time_elpased: 1.481
batch start
#iterations: 95
currently lose_sum: 95.56357151269913
time_elpased: 1.44
batch start
#iterations: 96
currently lose_sum: 95.68100416660309
time_elpased: 1.447
batch start
#iterations: 97
currently lose_sum: 95.64578032493591
time_elpased: 1.464
batch start
#iterations: 98
currently lose_sum: 95.38693469762802
time_elpased: 1.451
batch start
#iterations: 99
currently lose_sum: 95.72630167007446
time_elpased: 1.45
start validation test
0.649845360825
0.637071066942
0.699289904291
0.666732080655
0.649758553302
61.921
batch start
#iterations: 100
currently lose_sum: 95.25342130661011
time_elpased: 1.47
batch start
#iterations: 101
currently lose_sum: 95.89418464899063
time_elpased: 1.567
batch start
#iterations: 102
currently lose_sum: 95.4164320230484
time_elpased: 1.484
batch start
#iterations: 103
currently lose_sum: 95.61717051267624
time_elpased: 1.434
batch start
#iterations: 104
currently lose_sum: 95.28063929080963
time_elpased: 1.446
batch start
#iterations: 105
currently lose_sum: 95.1880978345871
time_elpased: 1.442
batch start
#iterations: 106
currently lose_sum: 95.81465893983841
time_elpased: 1.534
batch start
#iterations: 107
currently lose_sum: 95.38989573717117
time_elpased: 1.489
batch start
#iterations: 108
currently lose_sum: 95.37347060441971
time_elpased: 1.452
batch start
#iterations: 109
currently lose_sum: 95.43047577142715
time_elpased: 1.488
batch start
#iterations: 110
currently lose_sum: 95.63368314504623
time_elpased: 1.511
batch start
#iterations: 111
currently lose_sum: 95.20842534303665
time_elpased: 1.456
batch start
#iterations: 112
currently lose_sum: 95.61634188890457
time_elpased: 1.462
batch start
#iterations: 113
currently lose_sum: 94.92006236314774
time_elpased: 1.462
batch start
#iterations: 114
currently lose_sum: 95.62781119346619
time_elpased: 1.446
batch start
#iterations: 115
currently lose_sum: 95.65709286928177
time_elpased: 1.427
batch start
#iterations: 116
currently lose_sum: 95.3997346162796
time_elpased: 1.491
batch start
#iterations: 117
currently lose_sum: 95.59817123413086
time_elpased: 1.49
batch start
#iterations: 118
currently lose_sum: 95.35170847177505
time_elpased: 1.472
batch start
#iterations: 119
currently lose_sum: 95.5851126909256
time_elpased: 1.447
start validation test
0.643917525773
0.64628684512
0.638571575589
0.642406046175
0.643926911413
61.943
batch start
#iterations: 120
currently lose_sum: 95.45822030305862
time_elpased: 1.492
batch start
#iterations: 121
currently lose_sum: 95.43784439563751
time_elpased: 1.468
batch start
#iterations: 122
currently lose_sum: 95.36611491441727
time_elpased: 1.462
batch start
#iterations: 123
currently lose_sum: 95.24369269609451
time_elpased: 1.474
batch start
#iterations: 124
currently lose_sum: 95.77127623558044
time_elpased: 1.469
batch start
#iterations: 125
currently lose_sum: 95.89830046892166
time_elpased: 1.504
batch start
#iterations: 126
currently lose_sum: 95.41782927513123
time_elpased: 1.447
batch start
#iterations: 127
currently lose_sum: 95.65663385391235
time_elpased: 1.48
batch start
#iterations: 128
currently lose_sum: 95.81204372644424
time_elpased: 1.474
batch start
#iterations: 129
currently lose_sum: 95.34278482198715
time_elpased: 1.446
batch start
#iterations: 130
currently lose_sum: 95.15419125556946
time_elpased: 1.447
batch start
#iterations: 131
currently lose_sum: 95.36880320310593
time_elpased: 1.519
batch start
#iterations: 132
currently lose_sum: 95.47634273767471
time_elpased: 1.492
batch start
#iterations: 133
currently lose_sum: 95.55303716659546
time_elpased: 1.478
batch start
#iterations: 134
currently lose_sum: 95.30683118104935
time_elpased: 1.525
batch start
#iterations: 135
currently lose_sum: 95.30900239944458
time_elpased: 1.498
batch start
#iterations: 136
currently lose_sum: 95.6571791768074
time_elpased: 1.504
batch start
#iterations: 137
currently lose_sum: 94.9597128033638
time_elpased: 1.492
batch start
#iterations: 138
currently lose_sum: 95.35059201717377
time_elpased: 1.462
batch start
#iterations: 139
currently lose_sum: 95.6873089671135
time_elpased: 1.505
start validation test
0.653608247423
0.640585420771
0.702686014202
0.670200235571
0.653522083833
61.718
batch start
#iterations: 140
currently lose_sum: 95.6661069393158
time_elpased: 1.448
batch start
#iterations: 141
currently lose_sum: 95.2452866435051
time_elpased: 1.466
batch start
#iterations: 142
currently lose_sum: 95.06757766008377
time_elpased: 1.464
batch start
#iterations: 143
currently lose_sum: 95.39155340194702
time_elpased: 1.442
batch start
#iterations: 144
currently lose_sum: 95.07699257135391
time_elpased: 1.447
batch start
#iterations: 145
currently lose_sum: 95.49401026964188
time_elpased: 1.463
batch start
#iterations: 146
currently lose_sum: 95.46785527467728
time_elpased: 1.421
batch start
#iterations: 147
currently lose_sum: 95.52686089277267
time_elpased: 1.444
batch start
#iterations: 148
currently lose_sum: 95.19939637184143
time_elpased: 1.445
batch start
#iterations: 149
currently lose_sum: 95.71883708238602
time_elpased: 1.465
batch start
#iterations: 150
currently lose_sum: 95.42054802179337
time_elpased: 1.5
batch start
#iterations: 151
currently lose_sum: 95.54507994651794
time_elpased: 1.44
batch start
#iterations: 152
currently lose_sum: 95.00533527135849
time_elpased: 1.439
batch start
#iterations: 153
currently lose_sum: 95.67024290561676
time_elpased: 1.442
batch start
#iterations: 154
currently lose_sum: 95.33181136846542
time_elpased: 1.435
batch start
#iterations: 155
currently lose_sum: 95.5518793463707
time_elpased: 1.458
batch start
#iterations: 156
currently lose_sum: 95.13978570699692
time_elpased: 1.507
batch start
#iterations: 157
currently lose_sum: 95.18524724245071
time_elpased: 1.443
batch start
#iterations: 158
currently lose_sum: 95.46243572235107
time_elpased: 1.42
batch start
#iterations: 159
currently lose_sum: 95.28420615196228
time_elpased: 1.489
start validation test
0.651340206186
0.636221053603
0.709684058866
0.670947655186
0.651237774553
61.826
batch start
#iterations: 160
currently lose_sum: 95.33076882362366
time_elpased: 1.483
batch start
#iterations: 161
currently lose_sum: 95.31501746177673
time_elpased: 1.414
batch start
#iterations: 162
currently lose_sum: 95.09311896562576
time_elpased: 1.435
batch start
#iterations: 163
currently lose_sum: 95.297492146492
time_elpased: 1.468
batch start
#iterations: 164
currently lose_sum: 95.58429539203644
time_elpased: 1.453
batch start
#iterations: 165
currently lose_sum: 95.2016561627388
time_elpased: 1.458
batch start
#iterations: 166
currently lose_sum: 95.4515683054924
time_elpased: 1.46
batch start
#iterations: 167
currently lose_sum: 95.26867097616196
time_elpased: 1.484
batch start
#iterations: 168
currently lose_sum: 95.22080308198929
time_elpased: 1.429
batch start
#iterations: 169
currently lose_sum: 95.09699887037277
time_elpased: 1.495
batch start
#iterations: 170
currently lose_sum: 95.37100648880005
time_elpased: 1.456
batch start
#iterations: 171
currently lose_sum: 94.96752274036407
time_elpased: 1.533
batch start
#iterations: 172
currently lose_sum: 94.8389202952385
time_elpased: 1.432
batch start
#iterations: 173
currently lose_sum: 95.3000203371048
time_elpased: 1.574
batch start
#iterations: 174
currently lose_sum: 94.95660817623138
time_elpased: 1.454
batch start
#iterations: 175
currently lose_sum: 95.39227020740509
time_elpased: 1.516
batch start
#iterations: 176
currently lose_sum: 95.4461156129837
time_elpased: 1.455
batch start
#iterations: 177
currently lose_sum: 95.49324440956116
time_elpased: 1.461
batch start
#iterations: 178
currently lose_sum: 95.37303185462952
time_elpased: 1.474
batch start
#iterations: 179
currently lose_sum: 94.58902847766876
time_elpased: 1.452
start validation test
0.650051546392
0.641805501744
0.681897705053
0.66124444888
0.649995635548
61.714
batch start
#iterations: 180
currently lose_sum: 95.30724281072617
time_elpased: 1.452
batch start
#iterations: 181
currently lose_sum: 95.08775770664215
time_elpased: 1.455
batch start
#iterations: 182
currently lose_sum: 95.43530023097992
time_elpased: 1.444
batch start
#iterations: 183
currently lose_sum: 94.61812508106232
time_elpased: 1.448
batch start
#iterations: 184
currently lose_sum: 94.9880833029747
time_elpased: 1.499
batch start
#iterations: 185
currently lose_sum: 95.0026074051857
time_elpased: 1.465
batch start
#iterations: 186
currently lose_sum: 94.91353613138199
time_elpased: 1.464
batch start
#iterations: 187
currently lose_sum: 94.77941703796387
time_elpased: 1.521
batch start
#iterations: 188
currently lose_sum: 94.99681442975998
time_elpased: 1.485
batch start
#iterations: 189
currently lose_sum: 94.95989668369293
time_elpased: 1.488
batch start
#iterations: 190
currently lose_sum: 94.92688912153244
time_elpased: 1.469
batch start
#iterations: 191
currently lose_sum: 95.07230019569397
time_elpased: 1.428
batch start
#iterations: 192
currently lose_sum: 94.86757010221481
time_elpased: 1.445
batch start
#iterations: 193
currently lose_sum: 94.93803930282593
time_elpased: 1.454
batch start
#iterations: 194
currently lose_sum: 94.79647082090378
time_elpased: 1.428
batch start
#iterations: 195
currently lose_sum: 95.15562903881073
time_elpased: 1.47
batch start
#iterations: 196
currently lose_sum: 94.96724301576614
time_elpased: 1.475
batch start
#iterations: 197
currently lose_sum: 95.36647146940231
time_elpased: 1.493
batch start
#iterations: 198
currently lose_sum: 95.00189417600632
time_elpased: 1.457
batch start
#iterations: 199
currently lose_sum: 95.47029286623001
time_elpased: 1.462
start validation test
0.652628865979
0.639628657164
0.701965627251
0.66934890339
0.652542247685
61.753
batch start
#iterations: 200
currently lose_sum: 95.380903840065
time_elpased: 1.45
batch start
#iterations: 201
currently lose_sum: 94.94758576154709
time_elpased: 1.465
batch start
#iterations: 202
currently lose_sum: 95.24940431118011
time_elpased: 1.483
batch start
#iterations: 203
currently lose_sum: 95.27759325504303
time_elpased: 1.468
batch start
#iterations: 204
currently lose_sum: 95.05059385299683
time_elpased: 1.463
batch start
#iterations: 205
currently lose_sum: 94.84914749860764
time_elpased: 1.459
batch start
#iterations: 206
currently lose_sum: 95.1130958199501
time_elpased: 1.459
batch start
#iterations: 207
currently lose_sum: 94.73918640613556
time_elpased: 1.441
batch start
#iterations: 208
currently lose_sum: 95.19925302267075
time_elpased: 1.496
batch start
#iterations: 209
currently lose_sum: 95.06313872337341
time_elpased: 1.491
batch start
#iterations: 210
currently lose_sum: 94.8770922422409
time_elpased: 1.491
batch start
#iterations: 211
currently lose_sum: 95.02253139019012
time_elpased: 1.485
batch start
#iterations: 212
currently lose_sum: 95.02771013975143
time_elpased: 1.489
batch start
#iterations: 213
currently lose_sum: 94.75921106338501
time_elpased: 1.459
batch start
#iterations: 214
currently lose_sum: 94.80717819929123
time_elpased: 1.46
batch start
#iterations: 215
currently lose_sum: 94.75084382295609
time_elpased: 1.452
batch start
#iterations: 216
currently lose_sum: 95.39169359207153
time_elpased: 1.494
batch start
#iterations: 217
currently lose_sum: 94.96162807941437
time_elpased: 1.507
batch start
#iterations: 218
currently lose_sum: 95.17396873235703
time_elpased: 1.443
batch start
#iterations: 219
currently lose_sum: 95.18401569128036
time_elpased: 1.525
start validation test
0.655309278351
0.644478352088
0.695482144695
0.669009553037
0.655238748687
61.766
batch start
#iterations: 220
currently lose_sum: 95.2495306134224
time_elpased: 1.479
batch start
#iterations: 221
currently lose_sum: 95.1503079533577
time_elpased: 1.467
batch start
#iterations: 222
currently lose_sum: 94.9769578576088
time_elpased: 1.461
batch start
#iterations: 223
currently lose_sum: 95.1173802614212
time_elpased: 1.448
batch start
#iterations: 224
currently lose_sum: 95.19180417060852
time_elpased: 1.482
batch start
#iterations: 225
currently lose_sum: 95.18262833356857
time_elpased: 1.483
batch start
#iterations: 226
currently lose_sum: 95.1277249455452
time_elpased: 1.504
batch start
#iterations: 227
currently lose_sum: 95.03270417451859
time_elpased: 1.549
batch start
#iterations: 228
currently lose_sum: 94.83705550432205
time_elpased: 1.46
batch start
#iterations: 229
currently lose_sum: 95.00832056999207
time_elpased: 1.469
batch start
#iterations: 230
currently lose_sum: 94.90307134389877
time_elpased: 1.46
batch start
#iterations: 231
currently lose_sum: 94.49609935283661
time_elpased: 1.454
batch start
#iterations: 232
currently lose_sum: 94.72570240497589
time_elpased: 1.46
batch start
#iterations: 233
currently lose_sum: 95.26967930793762
time_elpased: 1.447
batch start
#iterations: 234
currently lose_sum: 95.33310359716415
time_elpased: 1.473
batch start
#iterations: 235
currently lose_sum: 94.86960309743881
time_elpased: 1.49
batch start
#iterations: 236
currently lose_sum: 95.15117353200912
time_elpased: 1.488
batch start
#iterations: 237
currently lose_sum: 94.90302330255508
time_elpased: 1.504
batch start
#iterations: 238
currently lose_sum: 95.0075210928917
time_elpased: 1.493
batch start
#iterations: 239
currently lose_sum: 94.82408142089844
time_elpased: 1.553
start validation test
0.655412371134
0.641391531431
0.707728722857
0.672929203973
0.655320521709
61.667
batch start
#iterations: 240
currently lose_sum: 94.82909291982651
time_elpased: 1.488
batch start
#iterations: 241
currently lose_sum: 95.15945839881897
time_elpased: 1.468
batch start
#iterations: 242
currently lose_sum: 94.84515482187271
time_elpased: 1.494
batch start
#iterations: 243
currently lose_sum: 94.67518877983093
time_elpased: 1.479
batch start
#iterations: 244
currently lose_sum: 95.11629402637482
time_elpased: 1.508
batch start
#iterations: 245
currently lose_sum: 95.46605557203293
time_elpased: 1.504
batch start
#iterations: 246
currently lose_sum: 95.16650366783142
time_elpased: 1.52
batch start
#iterations: 247
currently lose_sum: 95.23827373981476
time_elpased: 1.461
batch start
#iterations: 248
currently lose_sum: 95.17251181602478
time_elpased: 1.5
batch start
#iterations: 249
currently lose_sum: 95.10810405015945
time_elpased: 1.488
batch start
#iterations: 250
currently lose_sum: 95.16573733091354
time_elpased: 1.477
batch start
#iterations: 251
currently lose_sum: 94.77841222286224
time_elpased: 1.475
batch start
#iterations: 252
currently lose_sum: 94.87809544801712
time_elpased: 1.461
batch start
#iterations: 253
currently lose_sum: 95.1445946097374
time_elpased: 1.546
batch start
#iterations: 254
currently lose_sum: 94.93901717662811
time_elpased: 1.477
batch start
#iterations: 255
currently lose_sum: 94.47134399414062
time_elpased: 1.466
batch start
#iterations: 256
currently lose_sum: 94.6203733086586
time_elpased: 1.473
batch start
#iterations: 257
currently lose_sum: 94.8516982793808
time_elpased: 1.474
batch start
#iterations: 258
currently lose_sum: 94.84283459186554
time_elpased: 1.492
batch start
#iterations: 259
currently lose_sum: 94.7602481842041
time_elpased: 1.445
start validation test
0.656907216495
0.639886664839
0.720489863126
0.677800367896
0.656795587352
61.556
batch start
#iterations: 260
currently lose_sum: 94.662493288517
time_elpased: 1.49
batch start
#iterations: 261
currently lose_sum: 95.00166285037994
time_elpased: 1.505
batch start
#iterations: 262
currently lose_sum: 94.80369901657104
time_elpased: 1.571
batch start
#iterations: 263
currently lose_sum: 94.68125998973846
time_elpased: 1.484
batch start
#iterations: 264
currently lose_sum: 94.97019171714783
time_elpased: 1.473
batch start
#iterations: 265
currently lose_sum: 94.4920825958252
time_elpased: 1.523
batch start
#iterations: 266
currently lose_sum: 95.31728273630142
time_elpased: 1.476
batch start
#iterations: 267
currently lose_sum: 94.63301634788513
time_elpased: 1.489
batch start
#iterations: 268
currently lose_sum: 94.69369614124298
time_elpased: 1.505
batch start
#iterations: 269
currently lose_sum: 94.78919327259064
time_elpased: 1.507
batch start
#iterations: 270
currently lose_sum: 95.1497814655304
time_elpased: 1.458
batch start
#iterations: 271
currently lose_sum: 94.95514917373657
time_elpased: 1.511
batch start
#iterations: 272
currently lose_sum: 95.16480177640915
time_elpased: 1.494
batch start
#iterations: 273
currently lose_sum: 94.7828586101532
time_elpased: 1.463
batch start
#iterations: 274
currently lose_sum: 94.6930884718895
time_elpased: 1.49
batch start
#iterations: 275
currently lose_sum: 94.98228776454926
time_elpased: 1.474
batch start
#iterations: 276
currently lose_sum: 95.32431143522263
time_elpased: 1.462
batch start
#iterations: 277
currently lose_sum: 95.05574661493301
time_elpased: 1.448
batch start
#iterations: 278
currently lose_sum: 95.13442200422287
time_elpased: 1.509
batch start
#iterations: 279
currently lose_sum: 95.01111787557602
time_elpased: 1.469
start validation test
0.657113402062
0.637259292432
0.732221879181
0.681448137152
0.656981537546
61.492
batch start
#iterations: 280
currently lose_sum: 94.93165773153305
time_elpased: 1.463
batch start
#iterations: 281
currently lose_sum: 95.03274685144424
time_elpased: 1.482
batch start
#iterations: 282
currently lose_sum: 95.16778838634491
time_elpased: 1.523
batch start
#iterations: 283
currently lose_sum: 94.98798477649689
time_elpased: 1.469
batch start
#iterations: 284
currently lose_sum: 94.65136271715164
time_elpased: 1.513
batch start
#iterations: 285
currently lose_sum: 94.96011185646057
time_elpased: 1.498
batch start
#iterations: 286
currently lose_sum: 95.00318515300751
time_elpased: 1.444
batch start
#iterations: 287
currently lose_sum: 94.97229737043381
time_elpased: 1.482
batch start
#iterations: 288
currently lose_sum: 94.56808656454086
time_elpased: 1.511
batch start
#iterations: 289
currently lose_sum: 94.9526498913765
time_elpased: 1.534
batch start
#iterations: 290
currently lose_sum: 94.9231686592102
time_elpased: 1.502
batch start
#iterations: 291
currently lose_sum: 94.95760864019394
time_elpased: 1.476
batch start
#iterations: 292
currently lose_sum: 94.7121269106865
time_elpased: 1.458
batch start
#iterations: 293
currently lose_sum: 94.6639478802681
time_elpased: 1.455
batch start
#iterations: 294
currently lose_sum: 94.68377900123596
time_elpased: 1.441
batch start
#iterations: 295
currently lose_sum: 94.58191293478012
time_elpased: 1.45
batch start
#iterations: 296
currently lose_sum: 94.55290973186493
time_elpased: 1.463
batch start
#iterations: 297
currently lose_sum: 94.8409133553505
time_elpased: 1.496
batch start
#iterations: 298
currently lose_sum: 94.98448711633682
time_elpased: 1.455
batch start
#iterations: 299
currently lose_sum: 94.30408304929733
time_elpased: 1.418
start validation test
0.658350515464
0.643901984534
0.711227745189
0.675892420538
0.658257681331
61.513
batch start
#iterations: 300
currently lose_sum: 94.66585040092468
time_elpased: 1.44
batch start
#iterations: 301
currently lose_sum: 94.96930247545242
time_elpased: 1.503
batch start
#iterations: 302
currently lose_sum: 95.0508422255516
time_elpased: 1.512
batch start
#iterations: 303
currently lose_sum: 94.74464017152786
time_elpased: 1.461
batch start
#iterations: 304
currently lose_sum: 94.56536948680878
time_elpased: 1.478
batch start
#iterations: 305
currently lose_sum: 94.90250957012177
time_elpased: 1.456
batch start
#iterations: 306
currently lose_sum: 95.25597578287125
time_elpased: 1.473
batch start
#iterations: 307
currently lose_sum: 94.71686255931854
time_elpased: 1.449
batch start
#iterations: 308
currently lose_sum: 94.47236061096191
time_elpased: 1.462
batch start
#iterations: 309
currently lose_sum: 94.54671013355255
time_elpased: 1.509
batch start
#iterations: 310
currently lose_sum: 94.7751105427742
time_elpased: 1.451
batch start
#iterations: 311
currently lose_sum: 94.800521671772
time_elpased: 1.457
batch start
#iterations: 312
currently lose_sum: 94.78036516904831
time_elpased: 1.484
batch start
#iterations: 313
currently lose_sum: 94.4536920785904
time_elpased: 1.478
batch start
#iterations: 314
currently lose_sum: 94.81016939878464
time_elpased: 1.439
batch start
#iterations: 315
currently lose_sum: 94.72293078899384
time_elpased: 1.454
batch start
#iterations: 316
currently lose_sum: 94.98633337020874
time_elpased: 1.473
batch start
#iterations: 317
currently lose_sum: 94.94559353590012
time_elpased: 1.456
batch start
#iterations: 318
currently lose_sum: 94.85679173469543
time_elpased: 1.468
batch start
#iterations: 319
currently lose_sum: 94.8161661028862
time_elpased: 1.461
start validation test
0.652319587629
0.644215838509
0.683132654111
0.663103741072
0.652265490538
61.651
batch start
#iterations: 320
currently lose_sum: 94.81769424676895
time_elpased: 1.485
batch start
#iterations: 321
currently lose_sum: 94.62378644943237
time_elpased: 1.488
batch start
#iterations: 322
currently lose_sum: 95.05973708629608
time_elpased: 1.454
batch start
#iterations: 323
currently lose_sum: 94.59988975524902
time_elpased: 1.462
batch start
#iterations: 324
currently lose_sum: 95.23802638053894
time_elpased: 1.466
batch start
#iterations: 325
currently lose_sum: 95.13866150379181
time_elpased: 1.462
batch start
#iterations: 326
currently lose_sum: 94.645980656147
time_elpased: 1.495
batch start
#iterations: 327
currently lose_sum: 94.64622515439987
time_elpased: 1.479
batch start
#iterations: 328
currently lose_sum: 94.45160335302353
time_elpased: 1.455
batch start
#iterations: 329
currently lose_sum: 94.83376884460449
time_elpased: 1.44
batch start
#iterations: 330
currently lose_sum: 94.71408522129059
time_elpased: 1.467
batch start
#iterations: 331
currently lose_sum: 94.75360757112503
time_elpased: 1.515
batch start
#iterations: 332
currently lose_sum: 94.40585440397263
time_elpased: 1.467
batch start
#iterations: 333
currently lose_sum: 94.60790157318115
time_elpased: 1.48
batch start
#iterations: 334
currently lose_sum: 94.79652971029282
time_elpased: 1.481
batch start
#iterations: 335
currently lose_sum: 94.9241510629654
time_elpased: 1.508
batch start
#iterations: 336
currently lose_sum: 94.50624877214432
time_elpased: 1.479
batch start
#iterations: 337
currently lose_sum: 95.2425479888916
time_elpased: 1.474
batch start
#iterations: 338
currently lose_sum: 94.72309869527817
time_elpased: 1.439
batch start
#iterations: 339
currently lose_sum: 94.4934361577034
time_elpased: 1.58
start validation test
0.656804123711
0.645680541004
0.697643305547
0.670656905421
0.656732424229
61.480
batch start
#iterations: 340
currently lose_sum: 94.64265555143356
time_elpased: 1.467
batch start
#iterations: 341
currently lose_sum: 94.6097531914711
time_elpased: 1.436
batch start
#iterations: 342
currently lose_sum: 94.74311470985413
time_elpased: 1.451
batch start
#iterations: 343
currently lose_sum: 94.86379826068878
time_elpased: 1.454
batch start
#iterations: 344
currently lose_sum: 94.55636721849442
time_elpased: 1.486
batch start
#iterations: 345
currently lose_sum: 94.7624523639679
time_elpased: 1.489
batch start
#iterations: 346
currently lose_sum: 94.78329968452454
time_elpased: 1.491
batch start
#iterations: 347
currently lose_sum: 95.05714851617813
time_elpased: 1.436
batch start
#iterations: 348
currently lose_sum: 94.7335347533226
time_elpased: 1.434
batch start
#iterations: 349
currently lose_sum: 94.76897436380386
time_elpased: 1.47
batch start
#iterations: 350
currently lose_sum: 94.76353448629379
time_elpased: 1.455
batch start
#iterations: 351
currently lose_sum: 94.80475109815598
time_elpased: 1.483
batch start
#iterations: 352
currently lose_sum: 94.7854875922203
time_elpased: 1.486
batch start
#iterations: 353
currently lose_sum: 94.80660963058472
time_elpased: 1.465
batch start
#iterations: 354
currently lose_sum: 94.91408145427704
time_elpased: 1.501
batch start
#iterations: 355
currently lose_sum: 94.79890024662018
time_elpased: 1.458
batch start
#iterations: 356
currently lose_sum: 94.94390827417374
time_elpased: 1.465
batch start
#iterations: 357
currently lose_sum: 94.71232658624649
time_elpased: 1.461
batch start
#iterations: 358
currently lose_sum: 94.94958502054214
time_elpased: 1.491
batch start
#iterations: 359
currently lose_sum: 94.68302565813065
time_elpased: 1.492
start validation test
0.658041237113
0.643971233772
0.709581146444
0.675186055621
0.657950750853
61.607
batch start
#iterations: 360
currently lose_sum: 94.79340118169785
time_elpased: 1.496
batch start
#iterations: 361
currently lose_sum: 94.8763456940651
time_elpased: 1.515
batch start
#iterations: 362
currently lose_sum: 94.5530207157135
time_elpased: 1.478
batch start
#iterations: 363
currently lose_sum: 94.92579501867294
time_elpased: 1.497
batch start
#iterations: 364
currently lose_sum: 95.12323653697968
time_elpased: 1.478
batch start
#iterations: 365
currently lose_sum: 94.85714912414551
time_elpased: 1.471
batch start
#iterations: 366
currently lose_sum: 94.6847158074379
time_elpased: 1.443
batch start
#iterations: 367
currently lose_sum: 95.15322369337082
time_elpased: 1.488
batch start
#iterations: 368
currently lose_sum: 94.93363410234451
time_elpased: 1.443
batch start
#iterations: 369
currently lose_sum: 94.82371371984482
time_elpased: 1.463
batch start
#iterations: 370
currently lose_sum: 94.53541171550751
time_elpased: 1.433
batch start
#iterations: 371
currently lose_sum: 94.52666646242142
time_elpased: 1.481
batch start
#iterations: 372
currently lose_sum: 94.92692083120346
time_elpased: 1.466
batch start
#iterations: 373
currently lose_sum: 94.626325070858
time_elpased: 1.446
batch start
#iterations: 374
currently lose_sum: 94.431565284729
time_elpased: 1.487
batch start
#iterations: 375
currently lose_sum: 94.84060436487198
time_elpased: 1.495
batch start
#iterations: 376
currently lose_sum: 94.3966892361641
time_elpased: 1.454
batch start
#iterations: 377
currently lose_sum: 94.43621456623077
time_elpased: 1.456
batch start
#iterations: 378
currently lose_sum: 94.94143974781036
time_elpased: 1.463
batch start
#iterations: 379
currently lose_sum: 94.8675417304039
time_elpased: 1.454
start validation test
0.65824742268
0.641436818473
0.720386950705
0.678623364033
0.658138327155
61.566
batch start
#iterations: 380
currently lose_sum: 94.41096860170364
time_elpased: 1.484
batch start
#iterations: 381
currently lose_sum: 94.66660565137863
time_elpased: 1.482
batch start
#iterations: 382
currently lose_sum: 94.80526399612427
time_elpased: 1.508
batch start
#iterations: 383
currently lose_sum: 94.39962822198868
time_elpased: 1.526
batch start
#iterations: 384
currently lose_sum: 94.88561445474625
time_elpased: 1.46
batch start
#iterations: 385
currently lose_sum: 94.55725711584091
time_elpased: 1.471
batch start
#iterations: 386
currently lose_sum: 94.57289636135101
time_elpased: 1.463
batch start
#iterations: 387
currently lose_sum: 94.60959905385971
time_elpased: 1.468
batch start
#iterations: 388
currently lose_sum: 94.73142522573471
time_elpased: 1.509
batch start
#iterations: 389
currently lose_sum: 95.30634427070618
time_elpased: 1.512
batch start
#iterations: 390
currently lose_sum: 94.86582577228546
time_elpased: 1.459
batch start
#iterations: 391
currently lose_sum: 94.9878118634224
time_elpased: 1.491
batch start
#iterations: 392
currently lose_sum: 94.68430453538895
time_elpased: 1.444
batch start
#iterations: 393
currently lose_sum: 94.70469790697098
time_elpased: 1.456
batch start
#iterations: 394
currently lose_sum: 94.2386828660965
time_elpased: 1.472
batch start
#iterations: 395
currently lose_sum: 94.17070478200912
time_elpased: 1.488
batch start
#iterations: 396
currently lose_sum: 94.60622549057007
time_elpased: 1.477
batch start
#iterations: 397
currently lose_sum: 94.44112384319305
time_elpased: 1.488
batch start
#iterations: 398
currently lose_sum: 94.2786049246788
time_elpased: 1.444
batch start
#iterations: 399
currently lose_sum: 94.56111043691635
time_elpased: 1.481
start validation test
0.653144329897
0.634473447345
0.72542965936
0.676909780573
0.653017421852
61.627
acc: 0.658
pre: 0.646
rec: 0.701
F1: 0.672
auc: 0.697
