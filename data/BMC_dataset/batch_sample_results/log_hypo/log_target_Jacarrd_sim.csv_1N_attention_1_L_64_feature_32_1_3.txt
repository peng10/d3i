start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.54605484008789
time_elpased: 1.472
batch start
#iterations: 1
currently lose_sum: 100.07852685451508
time_elpased: 1.442
batch start
#iterations: 2
currently lose_sum: 99.85354548692703
time_elpased: 1.454
batch start
#iterations: 3
currently lose_sum: 99.44686603546143
time_elpased: 1.474
batch start
#iterations: 4
currently lose_sum: 99.31816852092743
time_elpased: 1.465
batch start
#iterations: 5
currently lose_sum: 98.91891396045685
time_elpased: 1.474
batch start
#iterations: 6
currently lose_sum: 98.80824553966522
time_elpased: 1.478
batch start
#iterations: 7
currently lose_sum: 98.46821957826614
time_elpased: 1.501
batch start
#iterations: 8
currently lose_sum: 98.40098679065704
time_elpased: 1.503
batch start
#iterations: 9
currently lose_sum: 98.18377095460892
time_elpased: 1.494
batch start
#iterations: 10
currently lose_sum: 98.02474993467331
time_elpased: 1.439
batch start
#iterations: 11
currently lose_sum: 97.84219539165497
time_elpased: 1.462
batch start
#iterations: 12
currently lose_sum: 97.66037046909332
time_elpased: 1.476
batch start
#iterations: 13
currently lose_sum: 97.84370046854019
time_elpased: 1.468
batch start
#iterations: 14
currently lose_sum: 97.63203155994415
time_elpased: 1.426
batch start
#iterations: 15
currently lose_sum: 97.49149286746979
time_elpased: 1.486
batch start
#iterations: 16
currently lose_sum: 97.51439654827118
time_elpased: 1.465
batch start
#iterations: 17
currently lose_sum: 97.46255803108215
time_elpased: 1.435
batch start
#iterations: 18
currently lose_sum: 97.24546629190445
time_elpased: 1.47
batch start
#iterations: 19
currently lose_sum: 97.11393815279007
time_elpased: 1.519
start validation test
0.633711340206
0.62862211709
0.65644298065
0.642231396637
0.633673782766
63.024
batch start
#iterations: 20
currently lose_sum: 96.95223379135132
time_elpased: 1.431
batch start
#iterations: 21
currently lose_sum: 96.93005782365799
time_elpased: 1.46
batch start
#iterations: 22
currently lose_sum: 96.95287096500397
time_elpased: 1.45
batch start
#iterations: 23
currently lose_sum: 96.94063901901245
time_elpased: 1.48
batch start
#iterations: 24
currently lose_sum: 96.97709083557129
time_elpased: 1.445
batch start
#iterations: 25
currently lose_sum: 96.7650243639946
time_elpased: 1.468
batch start
#iterations: 26
currently lose_sum: 96.92725610733032
time_elpased: 1.474
batch start
#iterations: 27
currently lose_sum: 96.88224750757217
time_elpased: 1.472
batch start
#iterations: 28
currently lose_sum: 96.73936235904694
time_elpased: 1.438
batch start
#iterations: 29
currently lose_sum: 97.05387848615646
time_elpased: 1.532
batch start
#iterations: 30
currently lose_sum: 96.79756337404251
time_elpased: 1.435
batch start
#iterations: 31
currently lose_sum: 96.64045172929764
time_elpased: 1.505
batch start
#iterations: 32
currently lose_sum: 96.50212144851685
time_elpased: 1.466
batch start
#iterations: 33
currently lose_sum: 96.63326263427734
time_elpased: 1.492
batch start
#iterations: 34
currently lose_sum: 96.5909343957901
time_elpased: 1.489
batch start
#iterations: 35
currently lose_sum: 96.6925602555275
time_elpased: 1.562
batch start
#iterations: 36
currently lose_sum: 96.63266229629517
time_elpased: 1.423
batch start
#iterations: 37
currently lose_sum: 96.42623317241669
time_elpased: 1.474
batch start
#iterations: 38
currently lose_sum: 96.39191544055939
time_elpased: 1.551
batch start
#iterations: 39
currently lose_sum: 96.34564924240112
time_elpased: 1.513
start validation test
0.646134020619
0.637027780448
0.682070811033
0.658780257468
0.6460746455
62.455
batch start
#iterations: 40
currently lose_sum: 96.37794631719589
time_elpased: 1.571
batch start
#iterations: 41
currently lose_sum: 96.71650636196136
time_elpased: 1.458
batch start
#iterations: 42
currently lose_sum: 96.51577597856522
time_elpased: 1.455
batch start
#iterations: 43
currently lose_sum: 96.32820093631744
time_elpased: 1.464
batch start
#iterations: 44
currently lose_sum: 96.34830313920975
time_elpased: 1.421
batch start
#iterations: 45
currently lose_sum: 96.68448638916016
time_elpased: 1.45
batch start
#iterations: 46
currently lose_sum: 95.9766840338707
time_elpased: 1.425
batch start
#iterations: 47
currently lose_sum: 96.07756477594376
time_elpased: 1.469
batch start
#iterations: 48
currently lose_sum: 96.2641282081604
time_elpased: 1.456
batch start
#iterations: 49
currently lose_sum: 95.99592107534409
time_elpased: 1.432
batch start
#iterations: 50
currently lose_sum: 96.21470355987549
time_elpased: 1.464
batch start
#iterations: 51
currently lose_sum: 96.21579211950302
time_elpased: 1.425
batch start
#iterations: 52
currently lose_sum: 96.37655872106552
time_elpased: 1.443
batch start
#iterations: 53
currently lose_sum: 95.95110249519348
time_elpased: 1.464
batch start
#iterations: 54
currently lose_sum: 96.29625338315964
time_elpased: 1.459
batch start
#iterations: 55
currently lose_sum: 95.95716524124146
time_elpased: 1.474
batch start
#iterations: 56
currently lose_sum: 96.38201129436493
time_elpased: 1.478
batch start
#iterations: 57
currently lose_sum: 95.89946007728577
time_elpased: 1.45
batch start
#iterations: 58
currently lose_sum: 96.29978394508362
time_elpased: 1.498
batch start
#iterations: 59
currently lose_sum: 96.00925368070602
time_elpased: 1.444
start validation test
0.64912371134
0.640926266835
0.68083573487
0.660278484803
0.649071316423
62.279
batch start
#iterations: 60
currently lose_sum: 95.99537718296051
time_elpased: 1.459
batch start
#iterations: 61
currently lose_sum: 96.16235721111298
time_elpased: 1.522
batch start
#iterations: 62
currently lose_sum: 96.31786155700684
time_elpased: 1.44
batch start
#iterations: 63
currently lose_sum: 96.02512180805206
time_elpased: 1.468
batch start
#iterations: 64
currently lose_sum: 96.13417476415634
time_elpased: 1.507
batch start
#iterations: 65
currently lose_sum: 96.1644879579544
time_elpased: 1.485
batch start
#iterations: 66
currently lose_sum: 96.18006139993668
time_elpased: 1.444
batch start
#iterations: 67
currently lose_sum: 96.01416492462158
time_elpased: 1.448
batch start
#iterations: 68
currently lose_sum: 95.94138944149017
time_elpased: 1.521
batch start
#iterations: 69
currently lose_sum: 95.52125144004822
time_elpased: 1.487
batch start
#iterations: 70
currently lose_sum: 96.332610309124
time_elpased: 1.472
batch start
#iterations: 71
currently lose_sum: 95.87227576971054
time_elpased: 1.44
batch start
#iterations: 72
currently lose_sum: 96.01537281274796
time_elpased: 1.475
batch start
#iterations: 73
currently lose_sum: 95.67433595657349
time_elpased: 1.461
batch start
#iterations: 74
currently lose_sum: 96.02687710523605
time_elpased: 1.472
batch start
#iterations: 75
currently lose_sum: 95.70993912220001
time_elpased: 1.441
batch start
#iterations: 76
currently lose_sum: 95.95311230421066
time_elpased: 1.458
batch start
#iterations: 77
currently lose_sum: 95.60452741384506
time_elpased: 1.453
batch start
#iterations: 78
currently lose_sum: 96.12844288349152
time_elpased: 1.481
batch start
#iterations: 79
currently lose_sum: 95.8809524178505
time_elpased: 1.428
start validation test
0.650206185567
0.642150203765
0.681144503911
0.661072819898
0.650155068973
61.943
batch start
#iterations: 80
currently lose_sum: 95.73196268081665
time_elpased: 1.489
batch start
#iterations: 81
currently lose_sum: 96.0105749964714
time_elpased: 1.448
batch start
#iterations: 82
currently lose_sum: 95.82644176483154
time_elpased: 1.455
batch start
#iterations: 83
currently lose_sum: 95.9351561665535
time_elpased: 1.497
batch start
#iterations: 84
currently lose_sum: 95.67815244197845
time_elpased: 1.477
batch start
#iterations: 85
currently lose_sum: 95.40559059381485
time_elpased: 1.458
batch start
#iterations: 86
currently lose_sum: 95.96685546636581
time_elpased: 1.511
batch start
#iterations: 87
currently lose_sum: 95.20488303899765
time_elpased: 1.476
batch start
#iterations: 88
currently lose_sum: 95.8084711432457
time_elpased: 1.442
batch start
#iterations: 89
currently lose_sum: 95.74800825119019
time_elpased: 1.501
batch start
#iterations: 90
currently lose_sum: 95.94507253170013
time_elpased: 1.434
batch start
#iterations: 91
currently lose_sum: 95.66210240125656
time_elpased: 1.465
batch start
#iterations: 92
currently lose_sum: 95.85548388957977
time_elpased: 1.479
batch start
#iterations: 93
currently lose_sum: 95.2443500161171
time_elpased: 1.443
batch start
#iterations: 94
currently lose_sum: 95.62238371372223
time_elpased: 1.513
batch start
#iterations: 95
currently lose_sum: 95.80844980478287
time_elpased: 1.439
batch start
#iterations: 96
currently lose_sum: 95.49079596996307
time_elpased: 1.449
batch start
#iterations: 97
currently lose_sum: 95.5016862154007
time_elpased: 1.453
batch start
#iterations: 98
currently lose_sum: 95.6170557141304
time_elpased: 1.464
batch start
#iterations: 99
currently lose_sum: 95.53165638446808
time_elpased: 1.448
start validation test
0.647886597938
0.639036144578
0.682379580074
0.659997013588
0.647829608294
62.101
batch start
#iterations: 100
currently lose_sum: 95.96130907535553
time_elpased: 1.477
batch start
#iterations: 101
currently lose_sum: 95.87487268447876
time_elpased: 1.471
batch start
#iterations: 102
currently lose_sum: 96.14399552345276
time_elpased: 1.46
batch start
#iterations: 103
currently lose_sum: 95.8403844833374
time_elpased: 1.435
batch start
#iterations: 104
currently lose_sum: 95.60674035549164
time_elpased: 1.472
batch start
#iterations: 105
currently lose_sum: 95.78919553756714
time_elpased: 1.477
batch start
#iterations: 106
currently lose_sum: 95.44220674037933
time_elpased: 1.575
batch start
#iterations: 107
currently lose_sum: 95.81428653001785
time_elpased: 1.447
batch start
#iterations: 108
currently lose_sum: 95.43980211019516
time_elpased: 1.466
batch start
#iterations: 109
currently lose_sum: 95.43261831998825
time_elpased: 1.454
batch start
#iterations: 110
currently lose_sum: 95.92696583271027
time_elpased: 1.441
batch start
#iterations: 111
currently lose_sum: 95.43738985061646
time_elpased: 1.453
batch start
#iterations: 112
currently lose_sum: 95.662533223629
time_elpased: 1.474
batch start
#iterations: 113
currently lose_sum: 95.49148160219193
time_elpased: 1.427
batch start
#iterations: 114
currently lose_sum: 95.64117342233658
time_elpased: 1.47
batch start
#iterations: 115
currently lose_sum: 95.5724213719368
time_elpased: 1.445
batch start
#iterations: 116
currently lose_sum: 95.86914891004562
time_elpased: 1.467
batch start
#iterations: 117
currently lose_sum: 95.31042909622192
time_elpased: 1.456
batch start
#iterations: 118
currently lose_sum: 95.79061686992645
time_elpased: 1.478
batch start
#iterations: 119
currently lose_sum: 95.82620507478714
time_elpased: 1.459
start validation test
0.657422680412
0.636153982615
0.738163853438
0.683373034778
0.657289279053
61.880
batch start
#iterations: 120
currently lose_sum: 95.18815690279007
time_elpased: 1.453
batch start
#iterations: 121
currently lose_sum: 95.47153890132904
time_elpased: 1.465
batch start
#iterations: 122
currently lose_sum: 95.67661851644516
time_elpased: 1.505
batch start
#iterations: 123
currently lose_sum: 95.41577106714249
time_elpased: 1.491
batch start
#iterations: 124
currently lose_sum: 95.70809924602509
time_elpased: 1.465
batch start
#iterations: 125
currently lose_sum: 95.54939842224121
time_elpased: 1.461
batch start
#iterations: 126
currently lose_sum: 95.10795974731445
time_elpased: 1.464
batch start
#iterations: 127
currently lose_sum: 95.46926015615463
time_elpased: 1.446
batch start
#iterations: 128
currently lose_sum: 95.57744854688644
time_elpased: 1.456
batch start
#iterations: 129
currently lose_sum: 95.20324218273163
time_elpased: 1.517
batch start
#iterations: 130
currently lose_sum: 95.74705630540848
time_elpased: 1.467
batch start
#iterations: 131
currently lose_sum: 95.2855293750763
time_elpased: 1.521
batch start
#iterations: 132
currently lose_sum: 95.46396511793137
time_elpased: 1.447
batch start
#iterations: 133
currently lose_sum: 95.60730069875717
time_elpased: 1.493
batch start
#iterations: 134
currently lose_sum: 95.3308892250061
time_elpased: 1.502
batch start
#iterations: 135
currently lose_sum: 95.61663734912872
time_elpased: 1.464
batch start
#iterations: 136
currently lose_sum: 95.59599339962006
time_elpased: 1.492
batch start
#iterations: 137
currently lose_sum: 95.73235082626343
time_elpased: 1.469
batch start
#iterations: 138
currently lose_sum: 95.59400397539139
time_elpased: 1.485
batch start
#iterations: 139
currently lose_sum: 95.17399299144745
time_elpased: 1.506
start validation test
0.652268041237
0.636564281773
0.712433100041
0.672365225838
0.652168635935
61.642
batch start
#iterations: 140
currently lose_sum: 95.19161993265152
time_elpased: 1.461
batch start
#iterations: 141
currently lose_sum: 95.36239975690842
time_elpased: 1.462
batch start
#iterations: 142
currently lose_sum: 95.54736918210983
time_elpased: 1.446
batch start
#iterations: 143
currently lose_sum: 95.53128963708878
time_elpased: 1.464
batch start
#iterations: 144
currently lose_sum: 95.45258861780167
time_elpased: 1.447
batch start
#iterations: 145
currently lose_sum: 95.2825887799263
time_elpased: 1.456
batch start
#iterations: 146
currently lose_sum: 95.10963445901871
time_elpased: 1.453
batch start
#iterations: 147
currently lose_sum: 95.36086225509644
time_elpased: 1.519
batch start
#iterations: 148
currently lose_sum: 95.61929285526276
time_elpased: 1.463
batch start
#iterations: 149
currently lose_sum: 95.25012093782425
time_elpased: 1.485
batch start
#iterations: 150
currently lose_sum: 95.47260338068008
time_elpased: 1.438
batch start
#iterations: 151
currently lose_sum: 95.59950143098831
time_elpased: 1.492
batch start
#iterations: 152
currently lose_sum: 95.47210425138474
time_elpased: 1.459
batch start
#iterations: 153
currently lose_sum: 95.51986515522003
time_elpased: 1.451
batch start
#iterations: 154
currently lose_sum: 95.78086721897125
time_elpased: 1.479
batch start
#iterations: 155
currently lose_sum: 95.17575305700302
time_elpased: 1.476
batch start
#iterations: 156
currently lose_sum: 95.48631620407104
time_elpased: 1.494
batch start
#iterations: 157
currently lose_sum: 95.25049269199371
time_elpased: 1.521
batch start
#iterations: 158
currently lose_sum: 95.40807908773422
time_elpased: 1.505
batch start
#iterations: 159
currently lose_sum: 95.41218209266663
time_elpased: 1.498
start validation test
0.657577319588
0.635338677002
0.742383696995
0.684702643695
0.657437201657
61.860
batch start
#iterations: 160
currently lose_sum: 95.29142820835114
time_elpased: 1.452
batch start
#iterations: 161
currently lose_sum: 95.5720551609993
time_elpased: 1.447
batch start
#iterations: 162
currently lose_sum: 95.52341705560684
time_elpased: 1.511
batch start
#iterations: 163
currently lose_sum: 95.41002798080444
time_elpased: 1.458
batch start
#iterations: 164
currently lose_sum: 95.43974488973618
time_elpased: 1.524
batch start
#iterations: 165
currently lose_sum: 95.3525812625885
time_elpased: 1.427
batch start
#iterations: 166
currently lose_sum: 95.0835337638855
time_elpased: 1.485
batch start
#iterations: 167
currently lose_sum: 94.95130574703217
time_elpased: 1.424
batch start
#iterations: 168
currently lose_sum: 95.45136201381683
time_elpased: 1.454
batch start
#iterations: 169
currently lose_sum: 95.51290971040726
time_elpased: 1.521
batch start
#iterations: 170
currently lose_sum: 95.08401364088058
time_elpased: 1.445
batch start
#iterations: 171
currently lose_sum: 95.5692852139473
time_elpased: 1.451
batch start
#iterations: 172
currently lose_sum: 95.18762999773026
time_elpased: 1.476
batch start
#iterations: 173
currently lose_sum: 95.33374571800232
time_elpased: 1.514
batch start
#iterations: 174
currently lose_sum: 94.8327567577362
time_elpased: 1.486
batch start
#iterations: 175
currently lose_sum: 95.01275557279587
time_elpased: 1.47
batch start
#iterations: 176
currently lose_sum: 95.45419436693192
time_elpased: 1.472
batch start
#iterations: 177
currently lose_sum: 95.53952813148499
time_elpased: 1.443
batch start
#iterations: 178
currently lose_sum: 95.07715147733688
time_elpased: 1.491
batch start
#iterations: 179
currently lose_sum: 95.16953778266907
time_elpased: 1.467
start validation test
0.657113402062
0.634645807699
0.743207081103
0.684649663411
0.656971157239
61.706
batch start
#iterations: 180
currently lose_sum: 94.99597662687302
time_elpased: 1.481
batch start
#iterations: 181
currently lose_sum: 95.56856203079224
time_elpased: 1.458
batch start
#iterations: 182
currently lose_sum: 95.485087454319
time_elpased: 1.489
batch start
#iterations: 183
currently lose_sum: 95.14164263010025
time_elpased: 1.469
batch start
#iterations: 184
currently lose_sum: 95.26589524745941
time_elpased: 1.473
batch start
#iterations: 185
currently lose_sum: 95.24478828907013
time_elpased: 1.465
batch start
#iterations: 186
currently lose_sum: 94.91400426626205
time_elpased: 1.473
batch start
#iterations: 187
currently lose_sum: 95.52630090713501
time_elpased: 1.467
batch start
#iterations: 188
currently lose_sum: 95.33579689264297
time_elpased: 1.482
batch start
#iterations: 189
currently lose_sum: 95.50244051218033
time_elpased: 1.46
batch start
#iterations: 190
currently lose_sum: 95.4810379743576
time_elpased: 1.426
batch start
#iterations: 191
currently lose_sum: 95.30613940954208
time_elpased: 1.476
batch start
#iterations: 192
currently lose_sum: 95.18731677532196
time_elpased: 1.451
batch start
#iterations: 193
currently lose_sum: 95.00534880161285
time_elpased: 1.449
batch start
#iterations: 194
currently lose_sum: 95.20265239477158
time_elpased: 1.461
batch start
#iterations: 195
currently lose_sum: 95.4746168255806
time_elpased: 1.444
batch start
#iterations: 196
currently lose_sum: 94.97768813371658
time_elpased: 1.477
batch start
#iterations: 197
currently lose_sum: 94.99976271390915
time_elpased: 1.516
batch start
#iterations: 198
currently lose_sum: 94.94759726524353
time_elpased: 1.468
batch start
#iterations: 199
currently lose_sum: 94.76369655132294
time_elpased: 1.454
start validation test
0.657886597938
0.641797918394
0.717167558666
0.677392699169
0.657788653352
61.705
batch start
#iterations: 200
currently lose_sum: 95.48133605718613
time_elpased: 1.483
batch start
#iterations: 201
currently lose_sum: 94.99781024456024
time_elpased: 1.45
batch start
#iterations: 202
currently lose_sum: 95.28523224592209
time_elpased: 1.552
batch start
#iterations: 203
currently lose_sum: 95.26627314090729
time_elpased: 1.477
batch start
#iterations: 204
currently lose_sum: 95.43368190526962
time_elpased: 1.444
batch start
#iterations: 205
currently lose_sum: 95.48519426584244
time_elpased: 1.434
batch start
#iterations: 206
currently lose_sum: 95.09412509202957
time_elpased: 1.499
batch start
#iterations: 207
currently lose_sum: 94.9657107591629
time_elpased: 1.449
batch start
#iterations: 208
currently lose_sum: 95.21140372753143
time_elpased: 1.488
batch start
#iterations: 209
currently lose_sum: 94.9473158121109
time_elpased: 1.471
batch start
#iterations: 210
currently lose_sum: 95.1713182926178
time_elpased: 1.508
batch start
#iterations: 211
currently lose_sum: 94.61967122554779
time_elpased: 1.487
batch start
#iterations: 212
currently lose_sum: 95.2040913105011
time_elpased: 1.469
batch start
#iterations: 213
currently lose_sum: 95.2696670293808
time_elpased: 1.443
batch start
#iterations: 214
currently lose_sum: 95.27475500106812
time_elpased: 1.451
batch start
#iterations: 215
currently lose_sum: 94.73571020364761
time_elpased: 1.456
batch start
#iterations: 216
currently lose_sum: 94.67203092575073
time_elpased: 1.427
batch start
#iterations: 217
currently lose_sum: 95.3121749162674
time_elpased: 1.478
batch start
#iterations: 218
currently lose_sum: 95.10986763238907
time_elpased: 1.437
batch start
#iterations: 219
currently lose_sum: 95.2704553604126
time_elpased: 1.462
start validation test
0.656443298969
0.638241957408
0.724886784685
0.678810659727
0.656330215969
61.646
batch start
#iterations: 220
currently lose_sum: 95.1748496890068
time_elpased: 1.446
batch start
#iterations: 221
currently lose_sum: 94.9936313033104
time_elpased: 1.513
batch start
#iterations: 222
currently lose_sum: 95.03457528352737
time_elpased: 1.459
batch start
#iterations: 223
currently lose_sum: 94.8391825556755
time_elpased: 1.459
batch start
#iterations: 224
currently lose_sum: 94.94752049446106
time_elpased: 1.452
batch start
#iterations: 225
currently lose_sum: 95.09775763750076
time_elpased: 1.497
batch start
#iterations: 226
currently lose_sum: 95.07386308908463
time_elpased: 1.442
batch start
#iterations: 227
currently lose_sum: 95.07319903373718
time_elpased: 1.487
batch start
#iterations: 228
currently lose_sum: 94.96304684877396
time_elpased: 1.445
batch start
#iterations: 229
currently lose_sum: 94.79062086343765
time_elpased: 1.46
batch start
#iterations: 230
currently lose_sum: 94.99423450231552
time_elpased: 1.437
batch start
#iterations: 231
currently lose_sum: 95.22544658184052
time_elpased: 1.453
batch start
#iterations: 232
currently lose_sum: 95.64105248451233
time_elpased: 1.492
batch start
#iterations: 233
currently lose_sum: 95.32187926769257
time_elpased: 1.443
batch start
#iterations: 234
currently lose_sum: 95.26801443099976
time_elpased: 1.436
batch start
#iterations: 235
currently lose_sum: 95.33000910282135
time_elpased: 1.529
batch start
#iterations: 236
currently lose_sum: 95.07226711511612
time_elpased: 1.488
batch start
#iterations: 237
currently lose_sum: 94.81752896308899
time_elpased: 1.609
batch start
#iterations: 238
currently lose_sum: 95.23325276374817
time_elpased: 1.473
batch start
#iterations: 239
currently lose_sum: 95.30373907089233
time_elpased: 1.45
start validation test
0.657628865979
0.641894387001
0.715623713462
0.676756861982
0.657533046322
61.653
batch start
#iterations: 240
currently lose_sum: 95.03947234153748
time_elpased: 1.463
batch start
#iterations: 241
currently lose_sum: 94.73946821689606
time_elpased: 1.508
batch start
#iterations: 242
currently lose_sum: 95.10974705219269
time_elpased: 1.456
batch start
#iterations: 243
currently lose_sum: 95.21582329273224
time_elpased: 1.453
batch start
#iterations: 244
currently lose_sum: 95.03111660480499
time_elpased: 1.487
batch start
#iterations: 245
currently lose_sum: 95.41774219274521
time_elpased: 1.44
batch start
#iterations: 246
currently lose_sum: 95.09642821550369
time_elpased: 1.498
batch start
#iterations: 247
currently lose_sum: 95.05377507209778
time_elpased: 1.445
batch start
#iterations: 248
currently lose_sum: 95.13777136802673
time_elpased: 1.473
batch start
#iterations: 249
currently lose_sum: 94.86434173583984
time_elpased: 1.456
batch start
#iterations: 250
currently lose_sum: 95.04274451732635
time_elpased: 1.511
batch start
#iterations: 251
currently lose_sum: 94.81730079650879
time_elpased: 1.457
batch start
#iterations: 252
currently lose_sum: 95.43520563840866
time_elpased: 1.445
batch start
#iterations: 253
currently lose_sum: 95.00188755989075
time_elpased: 1.474
batch start
#iterations: 254
currently lose_sum: 95.09740835428238
time_elpased: 1.45
batch start
#iterations: 255
currently lose_sum: 94.77376717329025
time_elpased: 1.503
batch start
#iterations: 256
currently lose_sum: 95.00336021184921
time_elpased: 1.463
batch start
#iterations: 257
currently lose_sum: 94.97749960422516
time_elpased: 1.46
batch start
#iterations: 258
currently lose_sum: 94.9518832564354
time_elpased: 1.435
batch start
#iterations: 259
currently lose_sum: 95.12950152158737
time_elpased: 1.475
start validation test
0.651804123711
0.642561386615
0.686805269658
0.66394706731
0.651746294474
61.749
batch start
#iterations: 260
currently lose_sum: 95.01401060819626
time_elpased: 1.454
batch start
#iterations: 261
currently lose_sum: 95.00091987848282
time_elpased: 1.442
batch start
#iterations: 262
currently lose_sum: 95.00106978416443
time_elpased: 1.421
batch start
#iterations: 263
currently lose_sum: 94.66235846281052
time_elpased: 1.48
batch start
#iterations: 264
currently lose_sum: 95.21231830120087
time_elpased: 1.448
batch start
#iterations: 265
currently lose_sum: 95.20093297958374
time_elpased: 1.463
batch start
#iterations: 266
currently lose_sum: 95.13472104072571
time_elpased: 1.465
batch start
#iterations: 267
currently lose_sum: 94.97493916749954
time_elpased: 1.473
batch start
#iterations: 268
currently lose_sum: 94.93019777536392
time_elpased: 1.43
batch start
#iterations: 269
currently lose_sum: 95.10456788539886
time_elpased: 1.503
batch start
#iterations: 270
currently lose_sum: 94.68878620862961
time_elpased: 1.414
batch start
#iterations: 271
currently lose_sum: 94.96131014823914
time_elpased: 1.493
batch start
#iterations: 272
currently lose_sum: 95.29362940788269
time_elpased: 1.495
batch start
#iterations: 273
currently lose_sum: 94.91873699426651
time_elpased: 1.453
batch start
#iterations: 274
currently lose_sum: 95.17156559228897
time_elpased: 1.416
batch start
#iterations: 275
currently lose_sum: 95.16136127710342
time_elpased: 1.458
batch start
#iterations: 276
currently lose_sum: 94.96146959066391
time_elpased: 1.435
batch start
#iterations: 277
currently lose_sum: 94.74505132436752
time_elpased: 1.465
batch start
#iterations: 278
currently lose_sum: 95.11903864145279
time_elpased: 1.472
batch start
#iterations: 279
currently lose_sum: 95.06605488061905
time_elpased: 1.454
start validation test
0.658402061856
0.636572641259
0.740942774804
0.684803804994
0.658265687278
61.611
batch start
#iterations: 280
currently lose_sum: 94.89283949136734
time_elpased: 1.449
batch start
#iterations: 281
currently lose_sum: 94.85466837882996
time_elpased: 1.465
batch start
#iterations: 282
currently lose_sum: 95.05274730920792
time_elpased: 1.46
batch start
#iterations: 283
currently lose_sum: 95.26061445474625
time_elpased: 1.457
batch start
#iterations: 284
currently lose_sum: 94.72127395868301
time_elpased: 1.425
batch start
#iterations: 285
currently lose_sum: 94.96898370981216
time_elpased: 1.468
batch start
#iterations: 286
currently lose_sum: 94.7533637881279
time_elpased: 1.457
batch start
#iterations: 287
currently lose_sum: 95.0326868891716
time_elpased: 1.531
batch start
#iterations: 288
currently lose_sum: 94.89237189292908
time_elpased: 1.425
batch start
#iterations: 289
currently lose_sum: 94.80103069543839
time_elpased: 1.467
batch start
#iterations: 290
currently lose_sum: 94.73601526021957
time_elpased: 1.436
batch start
#iterations: 291
currently lose_sum: 95.09352082014084
time_elpased: 1.436
batch start
#iterations: 292
currently lose_sum: 94.76409620046616
time_elpased: 1.484
batch start
#iterations: 293
currently lose_sum: 94.92464542388916
time_elpased: 1.461
batch start
#iterations: 294
currently lose_sum: 95.14931964874268
time_elpased: 1.484
batch start
#iterations: 295
currently lose_sum: 94.85669642686844
time_elpased: 1.483
batch start
#iterations: 296
currently lose_sum: 95.17836046218872
time_elpased: 1.519
batch start
#iterations: 297
currently lose_sum: 95.21009540557861
time_elpased: 1.498
batch start
#iterations: 298
currently lose_sum: 94.74257701635361
time_elpased: 1.44
batch start
#iterations: 299
currently lose_sum: 94.8799359202385
time_elpased: 1.452
start validation test
0.655618556701
0.635261609769
0.733532317826
0.680869357535
0.655489826819
61.695
batch start
#iterations: 300
currently lose_sum: 95.16840016841888
time_elpased: 1.434
batch start
#iterations: 301
currently lose_sum: 94.95085370540619
time_elpased: 1.456
batch start
#iterations: 302
currently lose_sum: 94.65786725282669
time_elpased: 1.493
batch start
#iterations: 303
currently lose_sum: 94.58997279405594
time_elpased: 1.514
batch start
#iterations: 304
currently lose_sum: 94.51089787483215
time_elpased: 1.432
batch start
#iterations: 305
currently lose_sum: 95.05202043056488
time_elpased: 1.475
batch start
#iterations: 306
currently lose_sum: 95.10018545389175
time_elpased: 1.457
batch start
#iterations: 307
currently lose_sum: 95.20110750198364
time_elpased: 1.456
batch start
#iterations: 308
currently lose_sum: 94.83666741847992
time_elpased: 1.449
batch start
#iterations: 309
currently lose_sum: 94.60608959197998
time_elpased: 1.427
batch start
#iterations: 310
currently lose_sum: 94.74325740337372
time_elpased: 1.505
batch start
#iterations: 311
currently lose_sum: 94.55121332406998
time_elpased: 1.468
batch start
#iterations: 312
currently lose_sum: 95.08815175294876
time_elpased: 1.456
batch start
#iterations: 313
currently lose_sum: 94.72552579641342
time_elpased: 1.486
batch start
#iterations: 314
currently lose_sum: 94.93057495355606
time_elpased: 1.429
batch start
#iterations: 315
currently lose_sum: 94.44199830293655
time_elpased: 1.497
batch start
#iterations: 316
currently lose_sum: 94.74768841266632
time_elpased: 1.503
batch start
#iterations: 317
currently lose_sum: 94.60617691278458
time_elpased: 1.506
batch start
#iterations: 318
currently lose_sum: 94.5352548956871
time_elpased: 1.47
batch start
#iterations: 319
currently lose_sum: 94.74109244346619
time_elpased: 1.475
start validation test
0.656082474227
0.641423527225
0.710477562783
0.674186932318
0.655992602127
61.570
batch start
#iterations: 320
currently lose_sum: 94.99700152873993
time_elpased: 1.45
batch start
#iterations: 321
currently lose_sum: 94.79133194684982
time_elpased: 1.452
batch start
#iterations: 322
currently lose_sum: 94.82741230726242
time_elpased: 1.544
batch start
#iterations: 323
currently lose_sum: 95.07187032699585
time_elpased: 1.435
batch start
#iterations: 324
currently lose_sum: 95.00260454416275
time_elpased: 1.497
batch start
#iterations: 325
currently lose_sum: 94.73691517114639
time_elpased: 1.513
batch start
#iterations: 326
currently lose_sum: 94.34390610456467
time_elpased: 1.446
batch start
#iterations: 327
currently lose_sum: 95.1029292345047
time_elpased: 1.463
batch start
#iterations: 328
currently lose_sum: 94.73340564966202
time_elpased: 1.487
batch start
#iterations: 329
currently lose_sum: 95.02528893947601
time_elpased: 1.472
batch start
#iterations: 330
currently lose_sum: 94.7299776673317
time_elpased: 1.47
batch start
#iterations: 331
currently lose_sum: 94.98017460107803
time_elpased: 1.464
batch start
#iterations: 332
currently lose_sum: 94.92820823192596
time_elpased: 1.476
batch start
#iterations: 333
currently lose_sum: 94.77853220701218
time_elpased: 1.481
batch start
#iterations: 334
currently lose_sum: 94.88605123758316
time_elpased: 1.518
batch start
#iterations: 335
currently lose_sum: 94.60903710126877
time_elpased: 1.538
batch start
#iterations: 336
currently lose_sum: 94.59811753034592
time_elpased: 1.486
batch start
#iterations: 337
currently lose_sum: 94.84486836194992
time_elpased: 1.443
batch start
#iterations: 338
currently lose_sum: 94.54791170358658
time_elpased: 1.472
batch start
#iterations: 339
currently lose_sum: 94.72056114673615
time_elpased: 1.527
start validation test
0.65675257732
0.637789597043
0.72818032112
0.679994233264
0.656634563699
61.642
batch start
#iterations: 340
currently lose_sum: 95.15596264600754
time_elpased: 1.445
batch start
#iterations: 341
currently lose_sum: 94.63862383365631
time_elpased: 1.46
batch start
#iterations: 342
currently lose_sum: 94.92590987682343
time_elpased: 1.418
batch start
#iterations: 343
currently lose_sum: 95.3879035115242
time_elpased: 1.5
batch start
#iterations: 344
currently lose_sum: 95.10379350185394
time_elpased: 1.436
batch start
#iterations: 345
currently lose_sum: 95.04863029718399
time_elpased: 1.488
batch start
#iterations: 346
currently lose_sum: 94.6159057021141
time_elpased: 1.443
batch start
#iterations: 347
currently lose_sum: 94.87213760614395
time_elpased: 1.481
batch start
#iterations: 348
currently lose_sum: 94.97478342056274
time_elpased: 1.49
batch start
#iterations: 349
currently lose_sum: 94.9660701751709
time_elpased: 1.467
batch start
#iterations: 350
currently lose_sum: 94.68912130594254
time_elpased: 1.48
batch start
#iterations: 351
currently lose_sum: 95.08372831344604
time_elpased: 1.478
batch start
#iterations: 352
currently lose_sum: 94.84531265497208
time_elpased: 1.422
batch start
#iterations: 353
currently lose_sum: 95.34889853000641
time_elpased: 1.455
batch start
#iterations: 354
currently lose_sum: 94.69730228185654
time_elpased: 1.472
batch start
#iterations: 355
currently lose_sum: 94.54346561431885
time_elpased: 1.467
batch start
#iterations: 356
currently lose_sum: 94.7537043094635
time_elpased: 1.461
batch start
#iterations: 357
currently lose_sum: 94.7096107006073
time_elpased: 1.502
batch start
#iterations: 358
currently lose_sum: 94.74563348293304
time_elpased: 1.462
batch start
#iterations: 359
currently lose_sum: 94.62572866678238
time_elpased: 1.489
start validation test
0.658762886598
0.639308855292
0.731165088514
0.68215863261
0.658643262968
61.614
batch start
#iterations: 360
currently lose_sum: 95.16893631219864
time_elpased: 1.454
batch start
#iterations: 361
currently lose_sum: 94.89366322755814
time_elpased: 1.463
batch start
#iterations: 362
currently lose_sum: 94.96176236867905
time_elpased: 1.531
batch start
#iterations: 363
currently lose_sum: 94.76124829053879
time_elpased: 1.437
batch start
#iterations: 364
currently lose_sum: 94.4947320818901
time_elpased: 1.46
batch start
#iterations: 365
currently lose_sum: 95.08540296554565
time_elpased: 1.442
batch start
#iterations: 366
currently lose_sum: 94.88813692331314
time_elpased: 1.472
batch start
#iterations: 367
currently lose_sum: 94.6401737332344
time_elpased: 1.549
batch start
#iterations: 368
currently lose_sum: 94.89697676897049
time_elpased: 1.476
batch start
#iterations: 369
currently lose_sum: 94.82312089204788
time_elpased: 1.469
batch start
#iterations: 370
currently lose_sum: 94.8384221792221
time_elpased: 1.434
batch start
#iterations: 371
currently lose_sum: 94.37537318468094
time_elpased: 1.455
batch start
#iterations: 372
currently lose_sum: 94.75190490484238
time_elpased: 1.476
batch start
#iterations: 373
currently lose_sum: 94.8606367111206
time_elpased: 1.458
batch start
#iterations: 374
currently lose_sum: 94.85431855916977
time_elpased: 1.49
batch start
#iterations: 375
currently lose_sum: 94.62398034334183
time_elpased: 1.468
batch start
#iterations: 376
currently lose_sum: 94.43870890140533
time_elpased: 1.489
batch start
#iterations: 377
currently lose_sum: 94.6081913113594
time_elpased: 1.474
batch start
#iterations: 378
currently lose_sum: 94.90217924118042
time_elpased: 1.448
batch start
#iterations: 379
currently lose_sum: 94.60639554262161
time_elpased: 1.452
start validation test
0.654432989691
0.638904261206
0.712947715109
0.673898239128
0.654336311086
61.734
batch start
#iterations: 380
currently lose_sum: 94.70150125026703
time_elpased: 1.47
batch start
#iterations: 381
currently lose_sum: 94.70553785562515
time_elpased: 1.536
batch start
#iterations: 382
currently lose_sum: 95.01278817653656
time_elpased: 1.463
batch start
#iterations: 383
currently lose_sum: 94.68697339296341
time_elpased: 1.448
batch start
#iterations: 384
currently lose_sum: 94.51778411865234
time_elpased: 1.483
batch start
#iterations: 385
currently lose_sum: 94.82608699798584
time_elpased: 1.493
batch start
#iterations: 386
currently lose_sum: 95.07789647579193
time_elpased: 1.459
batch start
#iterations: 387
currently lose_sum: 94.46935492753983
time_elpased: 1.471
batch start
#iterations: 388
currently lose_sum: 94.67457443475723
time_elpased: 1.468
batch start
#iterations: 389
currently lose_sum: 94.5419732928276
time_elpased: 1.47
batch start
#iterations: 390
currently lose_sum: 94.67498552799225
time_elpased: 1.461
batch start
#iterations: 391
currently lose_sum: 94.23660916090012
time_elpased: 1.517
batch start
#iterations: 392
currently lose_sum: 94.91591429710388
time_elpased: 1.435
batch start
#iterations: 393
currently lose_sum: 94.70763427019119
time_elpased: 1.462
batch start
#iterations: 394
currently lose_sum: 94.54000622034073
time_elpased: 1.423
batch start
#iterations: 395
currently lose_sum: 94.45551866292953
time_elpased: 1.456
batch start
#iterations: 396
currently lose_sum: 94.48291885852814
time_elpased: 1.444
batch start
#iterations: 397
currently lose_sum: 94.66883909702301
time_elpased: 1.478
batch start
#iterations: 398
currently lose_sum: 94.79845893383026
time_elpased: 1.487
batch start
#iterations: 399
currently lose_sum: 94.92692703008652
time_elpased: 1.561
start validation test
0.657474226804
0.635848889675
0.739707698641
0.683857462296
0.657338359854
61.663
acc: 0.665
pre: 0.648
rec: 0.725
F1: 0.684
auc: 0.704
