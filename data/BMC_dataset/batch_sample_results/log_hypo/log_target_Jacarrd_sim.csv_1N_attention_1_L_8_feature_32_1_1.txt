start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.55980151891708
time_elpased: 1.463
batch start
#iterations: 1
currently lose_sum: 100.06186401844025
time_elpased: 1.388
batch start
#iterations: 2
currently lose_sum: 99.99300801753998
time_elpased: 1.423
batch start
#iterations: 3
currently lose_sum: 99.7362864613533
time_elpased: 1.42
batch start
#iterations: 4
currently lose_sum: 99.58234530687332
time_elpased: 1.405
batch start
#iterations: 5
currently lose_sum: 99.11218988895416
time_elpased: 1.443
batch start
#iterations: 6
currently lose_sum: 98.99517446756363
time_elpased: 1.4
batch start
#iterations: 7
currently lose_sum: 98.58282399177551
time_elpased: 1.386
batch start
#iterations: 8
currently lose_sum: 98.79476547241211
time_elpased: 1.393
batch start
#iterations: 9
currently lose_sum: 98.28719210624695
time_elpased: 1.428
batch start
#iterations: 10
currently lose_sum: 98.21432518959045
time_elpased: 1.404
batch start
#iterations: 11
currently lose_sum: 98.11756730079651
time_elpased: 1.411
batch start
#iterations: 12
currently lose_sum: 97.90573179721832
time_elpased: 1.408
batch start
#iterations: 13
currently lose_sum: 97.9437882900238
time_elpased: 1.451
batch start
#iterations: 14
currently lose_sum: 97.72397035360336
time_elpased: 1.398
batch start
#iterations: 15
currently lose_sum: 97.51833820343018
time_elpased: 1.403
batch start
#iterations: 16
currently lose_sum: 97.64892303943634
time_elpased: 1.391
batch start
#iterations: 17
currently lose_sum: 97.54438662528992
time_elpased: 1.47
batch start
#iterations: 18
currently lose_sum: 97.2255385518074
time_elpased: 1.403
batch start
#iterations: 19
currently lose_sum: 97.21344339847565
time_elpased: 1.424
start validation test
0.639639175258
0.637012464817
0.652156015231
0.644495296212
0.639617200015
62.804
batch start
#iterations: 20
currently lose_sum: 97.26511031389236
time_elpased: 1.416
batch start
#iterations: 21
currently lose_sum: 96.91793513298035
time_elpased: 1.402
batch start
#iterations: 22
currently lose_sum: 97.20873457193375
time_elpased: 1.407
batch start
#iterations: 23
currently lose_sum: 96.89373689889908
time_elpased: 1.455
batch start
#iterations: 24
currently lose_sum: 97.2383309006691
time_elpased: 1.384
batch start
#iterations: 25
currently lose_sum: 96.8817857503891
time_elpased: 1.415
batch start
#iterations: 26
currently lose_sum: 96.99423396587372
time_elpased: 1.395
batch start
#iterations: 27
currently lose_sum: 96.99568051099777
time_elpased: 1.432
batch start
#iterations: 28
currently lose_sum: 96.59160661697388
time_elpased: 1.449
batch start
#iterations: 29
currently lose_sum: 96.94463098049164
time_elpased: 1.391
batch start
#iterations: 30
currently lose_sum: 96.88834261894226
time_elpased: 1.395
batch start
#iterations: 31
currently lose_sum: 96.4308357834816
time_elpased: 1.409
batch start
#iterations: 32
currently lose_sum: 96.62470453977585
time_elpased: 1.426
batch start
#iterations: 33
currently lose_sum: 96.51750993728638
time_elpased: 1.388
batch start
#iterations: 34
currently lose_sum: 96.79195553064346
time_elpased: 1.39
batch start
#iterations: 35
currently lose_sum: 96.47088170051575
time_elpased: 1.396
batch start
#iterations: 36
currently lose_sum: 96.50112944841385
time_elpased: 1.413
batch start
#iterations: 37
currently lose_sum: 96.70140624046326
time_elpased: 1.398
batch start
#iterations: 38
currently lose_sum: 96.26585727930069
time_elpased: 1.412
batch start
#iterations: 39
currently lose_sum: 96.0217170715332
time_elpased: 1.403
start validation test
0.647010309278
0.63585566815
0.690953998148
0.662260800947
0.646933159355
62.278
batch start
#iterations: 40
currently lose_sum: 96.32239496707916
time_elpased: 1.408
batch start
#iterations: 41
currently lose_sum: 96.46047621965408
time_elpased: 1.415
batch start
#iterations: 42
currently lose_sum: 96.28371506929398
time_elpased: 1.39
batch start
#iterations: 43
currently lose_sum: 96.11982464790344
time_elpased: 1.441
batch start
#iterations: 44
currently lose_sum: 96.07754224538803
time_elpased: 1.393
batch start
#iterations: 45
currently lose_sum: 96.2507416009903
time_elpased: 1.386
batch start
#iterations: 46
currently lose_sum: 95.67282110452652
time_elpased: 1.429
batch start
#iterations: 47
currently lose_sum: 96.18681120872498
time_elpased: 1.384
batch start
#iterations: 48
currently lose_sum: 96.2772057056427
time_elpased: 1.381
batch start
#iterations: 49
currently lose_sum: 96.26039707660675
time_elpased: 1.43
batch start
#iterations: 50
currently lose_sum: 96.27067935466766
time_elpased: 1.379
batch start
#iterations: 51
currently lose_sum: 96.1852023601532
time_elpased: 1.433
batch start
#iterations: 52
currently lose_sum: 96.07135438919067
time_elpased: 1.435
batch start
#iterations: 53
currently lose_sum: 96.10308313369751
time_elpased: 1.469
batch start
#iterations: 54
currently lose_sum: 96.56063836812973
time_elpased: 1.448
batch start
#iterations: 55
currently lose_sum: 96.07065999507904
time_elpased: 1.385
batch start
#iterations: 56
currently lose_sum: 95.70778012275696
time_elpased: 1.385
batch start
#iterations: 57
currently lose_sum: 95.82498788833618
time_elpased: 1.429
batch start
#iterations: 58
currently lose_sum: 96.22471237182617
time_elpased: 1.388
batch start
#iterations: 59
currently lose_sum: 96.06042283773422
time_elpased: 1.421
start validation test
0.657680412371
0.640945747801
0.719769476176
0.678074555238
0.657571405443
61.758
batch start
#iterations: 60
currently lose_sum: 95.90634793043137
time_elpased: 1.476
batch start
#iterations: 61
currently lose_sum: 95.93971300125122
time_elpased: 1.419
batch start
#iterations: 62
currently lose_sum: 95.7713131904602
time_elpased: 1.395
batch start
#iterations: 63
currently lose_sum: 96.11090099811554
time_elpased: 1.441
batch start
#iterations: 64
currently lose_sum: 96.1325695514679
time_elpased: 1.426
batch start
#iterations: 65
currently lose_sum: 95.42057126760483
time_elpased: 1.437
batch start
#iterations: 66
currently lose_sum: 95.90766942501068
time_elpased: 1.418
batch start
#iterations: 67
currently lose_sum: 95.877008497715
time_elpased: 1.402
batch start
#iterations: 68
currently lose_sum: 95.91627943515778
time_elpased: 1.419
batch start
#iterations: 69
currently lose_sum: 95.89287084341049
time_elpased: 1.397
batch start
#iterations: 70
currently lose_sum: 95.7898098230362
time_elpased: 1.414
batch start
#iterations: 71
currently lose_sum: 95.56963682174683
time_elpased: 1.399
batch start
#iterations: 72
currently lose_sum: 95.95618492364883
time_elpased: 1.398
batch start
#iterations: 73
currently lose_sum: 95.73881769180298
time_elpased: 1.391
batch start
#iterations: 74
currently lose_sum: 95.68963158130646
time_elpased: 1.403
batch start
#iterations: 75
currently lose_sum: 95.46687412261963
time_elpased: 1.401
batch start
#iterations: 76
currently lose_sum: 95.26127928495407
time_elpased: 1.404
batch start
#iterations: 77
currently lose_sum: 95.85598039627075
time_elpased: 1.397
batch start
#iterations: 78
currently lose_sum: 95.64110267162323
time_elpased: 1.432
batch start
#iterations: 79
currently lose_sum: 96.07207280397415
time_elpased: 1.404
start validation test
0.645824742268
0.636564299424
0.682618092004
0.658787306947
0.645760145868
62.011
batch start
#iterations: 80
currently lose_sum: 95.90020829439163
time_elpased: 1.461
batch start
#iterations: 81
currently lose_sum: 95.80933207273483
time_elpased: 1.44
batch start
#iterations: 82
currently lose_sum: 95.97699946165085
time_elpased: 1.404
batch start
#iterations: 83
currently lose_sum: 95.64208799600601
time_elpased: 1.437
batch start
#iterations: 84
currently lose_sum: 95.67136389017105
time_elpased: 1.413
batch start
#iterations: 85
currently lose_sum: 95.91631650924683
time_elpased: 1.423
batch start
#iterations: 86
currently lose_sum: 95.90481007099152
time_elpased: 1.447
batch start
#iterations: 87
currently lose_sum: 95.43814063072205
time_elpased: 1.423
batch start
#iterations: 88
currently lose_sum: 95.34343582391739
time_elpased: 1.393
batch start
#iterations: 89
currently lose_sum: 95.22397178411484
time_elpased: 1.412
batch start
#iterations: 90
currently lose_sum: 95.86273139715195
time_elpased: 1.411
batch start
#iterations: 91
currently lose_sum: 95.80755496025085
time_elpased: 1.387
batch start
#iterations: 92
currently lose_sum: 95.39066797494888
time_elpased: 1.422
batch start
#iterations: 93
currently lose_sum: 96.01509594917297
time_elpased: 1.422
batch start
#iterations: 94
currently lose_sum: 95.34264224767685
time_elpased: 1.394
batch start
#iterations: 95
currently lose_sum: 95.58849936723709
time_elpased: 1.45
batch start
#iterations: 96
currently lose_sum: 95.80165684223175
time_elpased: 1.433
batch start
#iterations: 97
currently lose_sum: 95.35050535202026
time_elpased: 1.428
batch start
#iterations: 98
currently lose_sum: 95.36929380893707
time_elpased: 1.433
batch start
#iterations: 99
currently lose_sum: 95.23315298557281
time_elpased: 1.407
start validation test
0.655515463918
0.642709313264
0.703097663888
0.671548631248
0.655431926026
61.516
batch start
#iterations: 100
currently lose_sum: 95.48394966125488
time_elpased: 1.405
batch start
#iterations: 101
currently lose_sum: 95.36226284503937
time_elpased: 1.423
batch start
#iterations: 102
currently lose_sum: 95.20631575584412
time_elpased: 1.417
batch start
#iterations: 103
currently lose_sum: 95.32080036401749
time_elpased: 1.433
batch start
#iterations: 104
currently lose_sum: 95.74904882907867
time_elpased: 1.45
batch start
#iterations: 105
currently lose_sum: 95.49406510591507
time_elpased: 1.477
batch start
#iterations: 106
currently lose_sum: 95.62270534038544
time_elpased: 1.504
batch start
#iterations: 107
currently lose_sum: 95.41273200511932
time_elpased: 1.41
batch start
#iterations: 108
currently lose_sum: 95.50956773757935
time_elpased: 1.404
batch start
#iterations: 109
currently lose_sum: 95.43552696704865
time_elpased: 1.41
batch start
#iterations: 110
currently lose_sum: 95.24780815839767
time_elpased: 1.429
batch start
#iterations: 111
currently lose_sum: 95.5907791852951
time_elpased: 1.446
batch start
#iterations: 112
currently lose_sum: 95.30848568677902
time_elpased: 1.427
batch start
#iterations: 113
currently lose_sum: 95.35737729072571
time_elpased: 1.416
batch start
#iterations: 114
currently lose_sum: 95.20236974954605
time_elpased: 1.455
batch start
#iterations: 115
currently lose_sum: 95.29635900259018
time_elpased: 1.416
batch start
#iterations: 116
currently lose_sum: 95.63203012943268
time_elpased: 1.42
batch start
#iterations: 117
currently lose_sum: 95.29408329725266
time_elpased: 1.431
batch start
#iterations: 118
currently lose_sum: 95.60115295648575
time_elpased: 1.407
batch start
#iterations: 119
currently lose_sum: 95.5433720946312
time_elpased: 1.417
start validation test
0.655618556701
0.639624724062
0.715652979315
0.675506338336
0.655513157012
61.568
batch start
#iterations: 120
currently lose_sum: 95.32064545154572
time_elpased: 1.474
batch start
#iterations: 121
currently lose_sum: 95.3175163269043
time_elpased: 1.403
batch start
#iterations: 122
currently lose_sum: 95.54813474416733
time_elpased: 1.458
batch start
#iterations: 123
currently lose_sum: 95.27704405784607
time_elpased: 1.421
batch start
#iterations: 124
currently lose_sum: 95.09652704000473
time_elpased: 1.442
batch start
#iterations: 125
currently lose_sum: 95.62666839361191
time_elpased: 1.403
batch start
#iterations: 126
currently lose_sum: 95.61359369754791
time_elpased: 1.449
batch start
#iterations: 127
currently lose_sum: 95.52516031265259
time_elpased: 1.457
batch start
#iterations: 128
currently lose_sum: 95.69032394886017
time_elpased: 1.444
batch start
#iterations: 129
currently lose_sum: 95.2248552441597
time_elpased: 1.399
batch start
#iterations: 130
currently lose_sum: 95.21458613872528
time_elpased: 1.447
batch start
#iterations: 131
currently lose_sum: 95.21573078632355
time_elpased: 1.441
batch start
#iterations: 132
currently lose_sum: 95.52210801839828
time_elpased: 1.398
batch start
#iterations: 133
currently lose_sum: 95.42537194490433
time_elpased: 1.457
batch start
#iterations: 134
currently lose_sum: 94.9864730834961
time_elpased: 1.47
batch start
#iterations: 135
currently lose_sum: 95.78156048059464
time_elpased: 1.41
batch start
#iterations: 136
currently lose_sum: 94.79133528470993
time_elpased: 1.398
batch start
#iterations: 137
currently lose_sum: 95.12224054336548
time_elpased: 1.474
batch start
#iterations: 138
currently lose_sum: 95.04266321659088
time_elpased: 1.405
batch start
#iterations: 139
currently lose_sum: 95.45985859632492
time_elpased: 1.49
start validation test
0.654381443299
0.639599555061
0.710095708552
0.67300658376
0.654283628313
61.528
batch start
#iterations: 140
currently lose_sum: 95.57450556755066
time_elpased: 1.396
batch start
#iterations: 141
currently lose_sum: 95.70368754863739
time_elpased: 1.418
batch start
#iterations: 142
currently lose_sum: 95.48920530080795
time_elpased: 1.396
batch start
#iterations: 143
currently lose_sum: 95.27420389652252
time_elpased: 1.405
batch start
#iterations: 144
currently lose_sum: 95.38843017816544
time_elpased: 1.414
batch start
#iterations: 145
currently lose_sum: 95.40976256132126
time_elpased: 1.428
batch start
#iterations: 146
currently lose_sum: 95.05019944906235
time_elpased: 1.415
batch start
#iterations: 147
currently lose_sum: 95.20726674795151
time_elpased: 1.403
batch start
#iterations: 148
currently lose_sum: 95.52034640312195
time_elpased: 1.447
batch start
#iterations: 149
currently lose_sum: 95.22520953416824
time_elpased: 1.409
batch start
#iterations: 150
currently lose_sum: 95.0422014594078
time_elpased: 1.413
batch start
#iterations: 151
currently lose_sum: 94.71799939870834
time_elpased: 1.389
batch start
#iterations: 152
currently lose_sum: 95.43200314044952
time_elpased: 1.429
batch start
#iterations: 153
currently lose_sum: 95.1635964512825
time_elpased: 1.415
batch start
#iterations: 154
currently lose_sum: 95.5533138513565
time_elpased: 1.412
batch start
#iterations: 155
currently lose_sum: 95.43515509366989
time_elpased: 1.437
batch start
#iterations: 156
currently lose_sum: 94.93923443555832
time_elpased: 1.418
batch start
#iterations: 157
currently lose_sum: 95.59074777364731
time_elpased: 1.411
batch start
#iterations: 158
currently lose_sum: 95.71921133995056
time_elpased: 1.405
batch start
#iterations: 159
currently lose_sum: 95.9239667057991
time_elpased: 1.407
start validation test
0.654329896907
0.638896577175
0.71266851909
0.673769215801
0.654227474458
61.579
batch start
#iterations: 160
currently lose_sum: 95.19759130477905
time_elpased: 1.4
batch start
#iterations: 161
currently lose_sum: 95.08068895339966
time_elpased: 1.429
batch start
#iterations: 162
currently lose_sum: 95.2187066078186
time_elpased: 1.376
batch start
#iterations: 163
currently lose_sum: 95.04209613800049
time_elpased: 1.409
batch start
#iterations: 164
currently lose_sum: 94.96853965520859
time_elpased: 1.412
batch start
#iterations: 165
currently lose_sum: 95.1596839427948
time_elpased: 1.398
batch start
#iterations: 166
currently lose_sum: 95.12212854623795
time_elpased: 1.458
batch start
#iterations: 167
currently lose_sum: 95.26501941680908
time_elpased: 1.437
batch start
#iterations: 168
currently lose_sum: 95.10341733694077
time_elpased: 1.448
batch start
#iterations: 169
currently lose_sum: 95.01097214221954
time_elpased: 1.409
batch start
#iterations: 170
currently lose_sum: 95.01499629020691
time_elpased: 1.415
batch start
#iterations: 171
currently lose_sum: 95.30383402109146
time_elpased: 1.421
batch start
#iterations: 172
currently lose_sum: 95.39442592859268
time_elpased: 1.395
batch start
#iterations: 173
currently lose_sum: 95.17948985099792
time_elpased: 1.498
batch start
#iterations: 174
currently lose_sum: 94.84389859437943
time_elpased: 1.428
batch start
#iterations: 175
currently lose_sum: 95.02791053056717
time_elpased: 1.406
batch start
#iterations: 176
currently lose_sum: 95.3546952009201
time_elpased: 1.405
batch start
#iterations: 177
currently lose_sum: 94.9592502117157
time_elpased: 1.387
batch start
#iterations: 178
currently lose_sum: 94.96211206912994
time_elpased: 1.437
batch start
#iterations: 179
currently lose_sum: 95.20159161090851
time_elpased: 1.468
start validation test
0.656855670103
0.639853747715
0.720386950705
0.677736360556
0.656744131141
61.471
batch start
#iterations: 180
currently lose_sum: 95.17531102895737
time_elpased: 1.447
batch start
#iterations: 181
currently lose_sum: 95.50348454713821
time_elpased: 1.461
batch start
#iterations: 182
currently lose_sum: 95.27949267625809
time_elpased: 1.401
batch start
#iterations: 183
currently lose_sum: 94.96675956249237
time_elpased: 1.416
batch start
#iterations: 184
currently lose_sum: 95.19503730535507
time_elpased: 1.389
batch start
#iterations: 185
currently lose_sum: 95.0218146443367
time_elpased: 1.43
batch start
#iterations: 186
currently lose_sum: 94.95075929164886
time_elpased: 1.421
batch start
#iterations: 187
currently lose_sum: 94.8535526394844
time_elpased: 1.41
batch start
#iterations: 188
currently lose_sum: 95.17545121908188
time_elpased: 1.42
batch start
#iterations: 189
currently lose_sum: 95.32873612642288
time_elpased: 1.407
batch start
#iterations: 190
currently lose_sum: 95.38020151853561
time_elpased: 1.449
batch start
#iterations: 191
currently lose_sum: 95.08658134937286
time_elpased: 1.456
batch start
#iterations: 192
currently lose_sum: 95.01604890823364
time_elpased: 1.421
batch start
#iterations: 193
currently lose_sum: 94.9152153134346
time_elpased: 1.42
batch start
#iterations: 194
currently lose_sum: 94.97987508773804
time_elpased: 1.419
batch start
#iterations: 195
currently lose_sum: 95.20876425504684
time_elpased: 1.463
batch start
#iterations: 196
currently lose_sum: 95.59802740812302
time_elpased: 1.416
batch start
#iterations: 197
currently lose_sum: 95.1942611336708
time_elpased: 1.429
batch start
#iterations: 198
currently lose_sum: 95.38853693008423
time_elpased: 1.385
batch start
#iterations: 199
currently lose_sum: 95.05543690919876
time_elpased: 1.409
start validation test
0.654587628866
0.642857142857
0.698363692498
0.669461845805
0.654510773234
61.436
batch start
#iterations: 200
currently lose_sum: 94.99798089265823
time_elpased: 1.417
batch start
#iterations: 201
currently lose_sum: 94.88066011667252
time_elpased: 1.411
batch start
#iterations: 202
currently lose_sum: 94.97653257846832
time_elpased: 1.423
batch start
#iterations: 203
currently lose_sum: 95.03703331947327
time_elpased: 1.412
batch start
#iterations: 204
currently lose_sum: 94.77956134080887
time_elpased: 1.383
batch start
#iterations: 205
currently lose_sum: 94.79291278123856
time_elpased: 1.45
batch start
#iterations: 206
currently lose_sum: 95.07140910625458
time_elpased: 1.432
batch start
#iterations: 207
currently lose_sum: 95.1600192785263
time_elpased: 1.428
batch start
#iterations: 208
currently lose_sum: 94.83897650241852
time_elpased: 1.428
batch start
#iterations: 209
currently lose_sum: 94.6251854300499
time_elpased: 1.416
batch start
#iterations: 210
currently lose_sum: 94.73100745677948
time_elpased: 1.416
batch start
#iterations: 211
currently lose_sum: 94.87800592184067
time_elpased: 1.397
batch start
#iterations: 212
currently lose_sum: 94.85668569803238
time_elpased: 1.397
batch start
#iterations: 213
currently lose_sum: 95.02636247873306
time_elpased: 1.424
batch start
#iterations: 214
currently lose_sum: 94.99727016687393
time_elpased: 1.411
batch start
#iterations: 215
currently lose_sum: 95.07983165979385
time_elpased: 1.425
batch start
#iterations: 216
currently lose_sum: 94.53029865026474
time_elpased: 1.451
batch start
#iterations: 217
currently lose_sum: 94.98761212825775
time_elpased: 1.414
batch start
#iterations: 218
currently lose_sum: 95.15325719118118
time_elpased: 1.386
batch start
#iterations: 219
currently lose_sum: 95.24816966056824
time_elpased: 1.519
start validation test
0.657474226804
0.643874110154
0.707419985592
0.674152895601
0.657386539321
61.265
batch start
#iterations: 220
currently lose_sum: 95.09592461585999
time_elpased: 1.454
batch start
#iterations: 221
currently lose_sum: 94.45640432834625
time_elpased: 1.424
batch start
#iterations: 222
currently lose_sum: 95.31586825847626
time_elpased: 1.406
batch start
#iterations: 223
currently lose_sum: 95.08059906959534
time_elpased: 1.447
batch start
#iterations: 224
currently lose_sum: 94.9910574555397
time_elpased: 1.448
batch start
#iterations: 225
currently lose_sum: 95.06524378061295
time_elpased: 1.409
batch start
#iterations: 226
currently lose_sum: 94.90948742628098
time_elpased: 1.413
batch start
#iterations: 227
currently lose_sum: 95.14757716655731
time_elpased: 1.474
batch start
#iterations: 228
currently lose_sum: 95.0308228135109
time_elpased: 1.44
batch start
#iterations: 229
currently lose_sum: 94.47974562644958
time_elpased: 1.474
batch start
#iterations: 230
currently lose_sum: 95.106738448143
time_elpased: 1.398
batch start
#iterations: 231
currently lose_sum: 94.9501491189003
time_elpased: 1.424
batch start
#iterations: 232
currently lose_sum: 94.66830259561539
time_elpased: 1.387
batch start
#iterations: 233
currently lose_sum: 95.15161156654358
time_elpased: 1.442
batch start
#iterations: 234
currently lose_sum: 94.72621476650238
time_elpased: 1.471
batch start
#iterations: 235
currently lose_sum: 95.24954414367676
time_elpased: 1.4
batch start
#iterations: 236
currently lose_sum: 95.19190818071365
time_elpased: 1.443
batch start
#iterations: 237
currently lose_sum: 94.97882717847824
time_elpased: 1.441
batch start
#iterations: 238
currently lose_sum: 95.29583615064621
time_elpased: 1.502
batch start
#iterations: 239
currently lose_sum: 95.0795493721962
time_elpased: 1.419
start validation test
0.652371134021
0.643166714822
0.687249150973
0.66447761194
0.652309900282
61.459
batch start
#iterations: 240
currently lose_sum: 94.89109599590302
time_elpased: 1.412
batch start
#iterations: 241
currently lose_sum: 94.67442721128464
time_elpased: 1.425
batch start
#iterations: 242
currently lose_sum: 95.0140431523323
time_elpased: 1.423
batch start
#iterations: 243
currently lose_sum: 95.06712025403976
time_elpased: 1.485
batch start
#iterations: 244
currently lose_sum: 94.97408896684647
time_elpased: 1.429
batch start
#iterations: 245
currently lose_sum: 95.3559142947197
time_elpased: 1.412
batch start
#iterations: 246
currently lose_sum: 94.77503287792206
time_elpased: 1.412
batch start
#iterations: 247
currently lose_sum: 95.03006863594055
time_elpased: 1.417
batch start
#iterations: 248
currently lose_sum: 95.00275313854218
time_elpased: 1.434
batch start
#iterations: 249
currently lose_sum: 95.06706273555756
time_elpased: 1.422
batch start
#iterations: 250
currently lose_sum: 94.97918474674225
time_elpased: 1.442
batch start
#iterations: 251
currently lose_sum: 94.93269330263138
time_elpased: 1.452
batch start
#iterations: 252
currently lose_sum: 95.1528109908104
time_elpased: 1.434
batch start
#iterations: 253
currently lose_sum: 94.84275478124619
time_elpased: 1.432
batch start
#iterations: 254
currently lose_sum: 95.09705746173859
time_elpased: 1.402
batch start
#iterations: 255
currently lose_sum: 94.92406237125397
time_elpased: 1.411
batch start
#iterations: 256
currently lose_sum: 94.69018942117691
time_elpased: 1.451
batch start
#iterations: 257
currently lose_sum: 94.85082018375397
time_elpased: 1.392
batch start
#iterations: 258
currently lose_sum: 94.71619629859924
time_elpased: 1.415
batch start
#iterations: 259
currently lose_sum: 94.46412724256516
time_elpased: 1.406
start validation test
0.657731958763
0.641289374598
0.718637439539
0.677763758129
0.657625029797
61.303
batch start
#iterations: 260
currently lose_sum: 94.88400238752365
time_elpased: 1.443
batch start
#iterations: 261
currently lose_sum: 94.7326830625534
time_elpased: 1.441
batch start
#iterations: 262
currently lose_sum: 94.70865362882614
time_elpased: 1.431
batch start
#iterations: 263
currently lose_sum: 94.79528135061264
time_elpased: 1.444
batch start
#iterations: 264
currently lose_sum: 94.66776555776596
time_elpased: 1.442
batch start
#iterations: 265
currently lose_sum: 95.03957915306091
time_elpased: 1.432
batch start
#iterations: 266
currently lose_sum: 95.22214990854263
time_elpased: 1.424
batch start
#iterations: 267
currently lose_sum: 95.25538319349289
time_elpased: 1.41
batch start
#iterations: 268
currently lose_sum: 94.87201297283173
time_elpased: 1.47
batch start
#iterations: 269
currently lose_sum: 94.8191727399826
time_elpased: 1.407
batch start
#iterations: 270
currently lose_sum: 95.15938186645508
time_elpased: 1.429
batch start
#iterations: 271
currently lose_sum: 94.69502282142639
time_elpased: 1.413
batch start
#iterations: 272
currently lose_sum: 95.27274584770203
time_elpased: 1.435
batch start
#iterations: 273
currently lose_sum: 94.89613449573517
time_elpased: 1.427
batch start
#iterations: 274
currently lose_sum: 95.17766445875168
time_elpased: 1.432
batch start
#iterations: 275
currently lose_sum: 95.11750572919846
time_elpased: 1.406
batch start
#iterations: 276
currently lose_sum: 94.85451918840408
time_elpased: 1.417
batch start
#iterations: 277
currently lose_sum: 94.74434900283813
time_elpased: 1.435
batch start
#iterations: 278
currently lose_sum: 94.89496874809265
time_elpased: 1.394
batch start
#iterations: 279
currently lose_sum: 94.75615006685257
time_elpased: 1.416
start validation test
0.66
0.639144003567
0.737676237522
0.684884387541
0.659863627384
61.207
batch start
#iterations: 280
currently lose_sum: 94.7286787033081
time_elpased: 1.433
batch start
#iterations: 281
currently lose_sum: 94.90569496154785
time_elpased: 1.443
batch start
#iterations: 282
currently lose_sum: 95.11303091049194
time_elpased: 1.409
batch start
#iterations: 283
currently lose_sum: 94.92166912555695
time_elpased: 1.395
batch start
#iterations: 284
currently lose_sum: 94.93115878105164
time_elpased: 1.439
batch start
#iterations: 285
currently lose_sum: 94.82311862707138
time_elpased: 1.438
batch start
#iterations: 286
currently lose_sum: 94.96321934461594
time_elpased: 1.466
batch start
#iterations: 287
currently lose_sum: 94.62595725059509
time_elpased: 1.453
batch start
#iterations: 288
currently lose_sum: 94.41386371850967
time_elpased: 1.429
batch start
#iterations: 289
currently lose_sum: 94.87793159484863
time_elpased: 1.405
batch start
#iterations: 290
currently lose_sum: 95.10825258493423
time_elpased: 1.404
batch start
#iterations: 291
currently lose_sum: 94.78579115867615
time_elpased: 1.425
batch start
#iterations: 292
currently lose_sum: 94.89375120401382
time_elpased: 1.449
batch start
#iterations: 293
currently lose_sum: 94.93945664167404
time_elpased: 1.401
batch start
#iterations: 294
currently lose_sum: 94.85044687986374
time_elpased: 1.426
batch start
#iterations: 295
currently lose_sum: 95.05673325061798
time_elpased: 1.454
batch start
#iterations: 296
currently lose_sum: 94.46511644124985
time_elpased: 1.461
batch start
#iterations: 297
currently lose_sum: 94.92621046304703
time_elpased: 1.46
batch start
#iterations: 298
currently lose_sum: 94.7349779009819
time_elpased: 1.452
batch start
#iterations: 299
currently lose_sum: 94.63806980848312
time_elpased: 1.387
start validation test
0.657422680412
0.641195402299
0.717608315324
0.677253302253
0.657317015247
61.363
batch start
#iterations: 300
currently lose_sum: 94.54402351379395
time_elpased: 1.403
batch start
#iterations: 301
currently lose_sum: 94.77475380897522
time_elpased: 1.392
batch start
#iterations: 302
currently lose_sum: 94.81450778245926
time_elpased: 1.441
batch start
#iterations: 303
currently lose_sum: 94.9243193268776
time_elpased: 1.396
batch start
#iterations: 304
currently lose_sum: 95.15240144729614
time_elpased: 1.475
batch start
#iterations: 305
currently lose_sum: 94.93064045906067
time_elpased: 1.399
batch start
#iterations: 306
currently lose_sum: 94.78684598207474
time_elpased: 1.44
batch start
#iterations: 307
currently lose_sum: 95.09276646375656
time_elpased: 1.395
batch start
#iterations: 308
currently lose_sum: 94.58016484975815
time_elpased: 1.436
batch start
#iterations: 309
currently lose_sum: 94.92930698394775
time_elpased: 1.405
batch start
#iterations: 310
currently lose_sum: 94.83237516880035
time_elpased: 1.42
batch start
#iterations: 311
currently lose_sum: 94.41325289011002
time_elpased: 1.429
batch start
#iterations: 312
currently lose_sum: 94.59479928016663
time_elpased: 1.464
batch start
#iterations: 313
currently lose_sum: 94.73681831359863
time_elpased: 1.397
batch start
#iterations: 314
currently lose_sum: 95.06419003009796
time_elpased: 1.422
batch start
#iterations: 315
currently lose_sum: 94.81723946332932
time_elpased: 1.391
batch start
#iterations: 316
currently lose_sum: 94.45072638988495
time_elpased: 1.416
batch start
#iterations: 317
currently lose_sum: 95.19250696897507
time_elpased: 1.444
batch start
#iterations: 318
currently lose_sum: 94.73821407556534
time_elpased: 1.494
batch start
#iterations: 319
currently lose_sum: 94.8524414896965
time_elpased: 1.415
start validation test
0.658762886598
0.641222070223
0.723577235772
0.679914901847
0.658649095011
61.361
batch start
#iterations: 320
currently lose_sum: 94.82104295492172
time_elpased: 1.392
batch start
#iterations: 321
currently lose_sum: 95.02159827947617
time_elpased: 1.406
batch start
#iterations: 322
currently lose_sum: 94.89854383468628
time_elpased: 1.429
batch start
#iterations: 323
currently lose_sum: 94.74066919088364
time_elpased: 1.41
batch start
#iterations: 324
currently lose_sum: 95.14074009656906
time_elpased: 1.419
batch start
#iterations: 325
currently lose_sum: 95.14002734422684
time_elpased: 1.429
batch start
#iterations: 326
currently lose_sum: 94.83434110879898
time_elpased: 1.418
batch start
#iterations: 327
currently lose_sum: 94.77399450540543
time_elpased: 1.412
batch start
#iterations: 328
currently lose_sum: 94.67998421192169
time_elpased: 1.43
batch start
#iterations: 329
currently lose_sum: 94.97543108463287
time_elpased: 1.386
batch start
#iterations: 330
currently lose_sum: 94.69092267751694
time_elpased: 1.414
batch start
#iterations: 331
currently lose_sum: 94.58587920665741
time_elpased: 1.389
batch start
#iterations: 332
currently lose_sum: 94.36078774929047
time_elpased: 1.428
batch start
#iterations: 333
currently lose_sum: 94.14511585235596
time_elpased: 1.456
batch start
#iterations: 334
currently lose_sum: 94.78064811229706
time_elpased: 1.442
batch start
#iterations: 335
currently lose_sum: 94.64454728364944
time_elpased: 1.434
batch start
#iterations: 336
currently lose_sum: 94.43330031633377
time_elpased: 1.431
batch start
#iterations: 337
currently lose_sum: 94.9646577835083
time_elpased: 1.41
batch start
#iterations: 338
currently lose_sum: 94.67763149738312
time_elpased: 1.394
batch start
#iterations: 339
currently lose_sum: 94.56020134687424
time_elpased: 1.398
start validation test
0.65675257732
0.63882331578
0.72409179788
0.678790217549
0.656634352932
61.333
batch start
#iterations: 340
currently lose_sum: 94.9916622042656
time_elpased: 1.412
batch start
#iterations: 341
currently lose_sum: 95.23644268512726
time_elpased: 1.424
batch start
#iterations: 342
currently lose_sum: 94.71804934740067
time_elpased: 1.46
batch start
#iterations: 343
currently lose_sum: 94.74766063690186
time_elpased: 1.424
batch start
#iterations: 344
currently lose_sum: 94.32428294420242
time_elpased: 1.404
batch start
#iterations: 345
currently lose_sum: 94.97278594970703
time_elpased: 1.4
batch start
#iterations: 346
currently lose_sum: 94.48680770397186
time_elpased: 1.46
batch start
#iterations: 347
currently lose_sum: 95.17911553382874
time_elpased: 1.404
batch start
#iterations: 348
currently lose_sum: 94.71201860904694
time_elpased: 1.41
batch start
#iterations: 349
currently lose_sum: 94.57796853780746
time_elpased: 1.428
batch start
#iterations: 350
currently lose_sum: 94.9276881814003
time_elpased: 1.42
batch start
#iterations: 351
currently lose_sum: 94.715975522995
time_elpased: 1.388
batch start
#iterations: 352
currently lose_sum: 94.93681651353836
time_elpased: 1.416
batch start
#iterations: 353
currently lose_sum: 95.28879183530807
time_elpased: 1.408
batch start
#iterations: 354
currently lose_sum: 95.11915814876556
time_elpased: 1.433
batch start
#iterations: 355
currently lose_sum: 95.31013768911362
time_elpased: 1.48
batch start
#iterations: 356
currently lose_sum: 94.65730714797974
time_elpased: 1.463
batch start
#iterations: 357
currently lose_sum: 94.60807263851166
time_elpased: 1.417
batch start
#iterations: 358
currently lose_sum: 95.301340341568
time_elpased: 1.423
batch start
#iterations: 359
currently lose_sum: 95.01304829120636
time_elpased: 1.41
start validation test
0.656597938144
0.638926784902
0.722959761243
0.678350714562
0.656481429728
61.312
batch start
#iterations: 360
currently lose_sum: 94.89266049861908
time_elpased: 1.426
batch start
#iterations: 361
currently lose_sum: 95.23652929067612
time_elpased: 1.45
batch start
#iterations: 362
currently lose_sum: 94.58413273096085
time_elpased: 1.403
batch start
#iterations: 363
currently lose_sum: 94.53539717197418
time_elpased: 1.483
batch start
#iterations: 364
currently lose_sum: 95.14895927906036
time_elpased: 1.415
batch start
#iterations: 365
currently lose_sum: 95.07944762706757
time_elpased: 1.395
batch start
#iterations: 366
currently lose_sum: 95.1068195104599
time_elpased: 1.451
batch start
#iterations: 367
currently lose_sum: 94.78796660900116
time_elpased: 1.399
batch start
#iterations: 368
currently lose_sum: 94.81379443407059
time_elpased: 1.436
batch start
#iterations: 369
currently lose_sum: 94.58347123861313
time_elpased: 1.44
batch start
#iterations: 370
currently lose_sum: 94.826256275177
time_elpased: 1.444
batch start
#iterations: 371
currently lose_sum: 94.53353369235992
time_elpased: 1.399
batch start
#iterations: 372
currently lose_sum: 94.95433950424194
time_elpased: 1.41
batch start
#iterations: 373
currently lose_sum: 94.922238945961
time_elpased: 1.453
batch start
#iterations: 374
currently lose_sum: 94.66305643320084
time_elpased: 1.448
batch start
#iterations: 375
currently lose_sum: 95.06753414869308
time_elpased: 1.432
batch start
#iterations: 376
currently lose_sum: 94.28516137599945
time_elpased: 1.426
batch start
#iterations: 377
currently lose_sum: 94.87926477193832
time_elpased: 1.43
batch start
#iterations: 378
currently lose_sum: 94.96502405405045
time_elpased: 1.45
batch start
#iterations: 379
currently lose_sum: 94.49725550413132
time_elpased: 1.394
start validation test
0.656237113402
0.636510211394
0.731295667387
0.680618744313
0.656105336534
61.274
batch start
#iterations: 380
currently lose_sum: 95.1142560839653
time_elpased: 1.45
batch start
#iterations: 381
currently lose_sum: 94.46407109498978
time_elpased: 1.413
batch start
#iterations: 382
currently lose_sum: 94.87019127607346
time_elpased: 1.423
batch start
#iterations: 383
currently lose_sum: 94.63340646028519
time_elpased: 1.386
batch start
#iterations: 384
currently lose_sum: 94.43930500745773
time_elpased: 1.441
batch start
#iterations: 385
currently lose_sum: 94.82949042320251
time_elpased: 1.436
batch start
#iterations: 386
currently lose_sum: 94.95153653621674
time_elpased: 1.383
batch start
#iterations: 387
currently lose_sum: 94.29168975353241
time_elpased: 1.424
batch start
#iterations: 388
currently lose_sum: 94.83158421516418
time_elpased: 1.433
batch start
#iterations: 389
currently lose_sum: 94.90302431583405
time_elpased: 1.41
batch start
#iterations: 390
currently lose_sum: 94.83488464355469
time_elpased: 1.412
batch start
#iterations: 391
currently lose_sum: 94.53046590089798
time_elpased: 1.39
batch start
#iterations: 392
currently lose_sum: 94.47728878259659
time_elpased: 1.406
batch start
#iterations: 393
currently lose_sum: 94.47556644678116
time_elpased: 1.397
batch start
#iterations: 394
currently lose_sum: 94.70777004957199
time_elpased: 1.396
batch start
#iterations: 395
currently lose_sum: 94.98797315359116
time_elpased: 1.383
batch start
#iterations: 396
currently lose_sum: 94.54449915885925
time_elpased: 1.462
batch start
#iterations: 397
currently lose_sum: 94.47288435697556
time_elpased: 1.43
batch start
#iterations: 398
currently lose_sum: 94.78931605815887
time_elpased: 1.415
batch start
#iterations: 399
currently lose_sum: 94.92867583036423
time_elpased: 1.398
start validation test
0.656443298969
0.640463917526
0.716064629001
0.676157621107
0.656338624528
61.320
acc: 0.659
pre: 0.640
rec: 0.733
F1: 0.683
auc: 0.698
