start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.63498336076736
time_elpased: 1.434
batch start
#iterations: 1
currently lose_sum: 100.06429344415665
time_elpased: 1.447
batch start
#iterations: 2
currently lose_sum: 99.82033252716064
time_elpased: 1.408
batch start
#iterations: 3
currently lose_sum: 99.63929641246796
time_elpased: 1.383
batch start
#iterations: 4
currently lose_sum: 99.55652898550034
time_elpased: 1.403
batch start
#iterations: 5
currently lose_sum: 99.22525960206985
time_elpased: 1.419
batch start
#iterations: 6
currently lose_sum: 98.89941585063934
time_elpased: 1.444
batch start
#iterations: 7
currently lose_sum: 98.7337156534195
time_elpased: 1.394
batch start
#iterations: 8
currently lose_sum: 98.7217190861702
time_elpased: 1.426
batch start
#iterations: 9
currently lose_sum: 98.41357052326202
time_elpased: 1.388
batch start
#iterations: 10
currently lose_sum: 98.26368403434753
time_elpased: 1.398
batch start
#iterations: 11
currently lose_sum: 98.02834326028824
time_elpased: 1.41
batch start
#iterations: 12
currently lose_sum: 97.7493776679039
time_elpased: 1.395
batch start
#iterations: 13
currently lose_sum: 98.04515224695206
time_elpased: 1.416
batch start
#iterations: 14
currently lose_sum: 97.47228068113327
time_elpased: 1.454
batch start
#iterations: 15
currently lose_sum: 97.53521305322647
time_elpased: 1.396
batch start
#iterations: 16
currently lose_sum: 97.70795530080795
time_elpased: 1.454
batch start
#iterations: 17
currently lose_sum: 97.38907891511917
time_elpased: 1.39
batch start
#iterations: 18
currently lose_sum: 97.1135493516922
time_elpased: 1.421
batch start
#iterations: 19
currently lose_sum: 97.27914434671402
time_elpased: 1.421
start validation test
0.643453608247
0.633562297272
0.683441391376
0.657557304817
0.643383403526
62.692
batch start
#iterations: 20
currently lose_sum: 97.34583121538162
time_elpased: 1.395
batch start
#iterations: 21
currently lose_sum: 97.04321575164795
time_elpased: 1.402
batch start
#iterations: 22
currently lose_sum: 97.15211188793182
time_elpased: 1.404
batch start
#iterations: 23
currently lose_sum: 97.3032928109169
time_elpased: 1.446
batch start
#iterations: 24
currently lose_sum: 96.99343103170395
time_elpased: 1.438
batch start
#iterations: 25
currently lose_sum: 96.79381597042084
time_elpased: 1.43
batch start
#iterations: 26
currently lose_sum: 97.34125435352325
time_elpased: 1.404
batch start
#iterations: 27
currently lose_sum: 96.81434154510498
time_elpased: 1.488
batch start
#iterations: 28
currently lose_sum: 96.53123205900192
time_elpased: 1.416
batch start
#iterations: 29
currently lose_sum: 96.72737574577332
time_elpased: 1.447
batch start
#iterations: 30
currently lose_sum: 96.7283234000206
time_elpased: 1.462
batch start
#iterations: 31
currently lose_sum: 96.55941408872604
time_elpased: 1.384
batch start
#iterations: 32
currently lose_sum: 96.68609035015106
time_elpased: 1.4
batch start
#iterations: 33
currently lose_sum: 96.39798730611801
time_elpased: 1.441
batch start
#iterations: 34
currently lose_sum: 96.4921977519989
time_elpased: 1.412
batch start
#iterations: 35
currently lose_sum: 96.09475922584534
time_elpased: 1.408
batch start
#iterations: 36
currently lose_sum: 96.4949482679367
time_elpased: 1.42
batch start
#iterations: 37
currently lose_sum: 96.44626373052597
time_elpased: 1.42
batch start
#iterations: 38
currently lose_sum: 96.1831631064415
time_elpased: 1.4
batch start
#iterations: 39
currently lose_sum: 96.3276030421257
time_elpased: 1.42
start validation test
0.642164948454
0.638791637491
0.657198723886
0.647864461804
0.642138554342
62.086
batch start
#iterations: 40
currently lose_sum: 96.58602172136307
time_elpased: 1.435
batch start
#iterations: 41
currently lose_sum: 96.28487956523895
time_elpased: 1.398
batch start
#iterations: 42
currently lose_sum: 96.2154791355133
time_elpased: 1.386
batch start
#iterations: 43
currently lose_sum: 96.65030169487
time_elpased: 1.451
batch start
#iterations: 44
currently lose_sum: 96.26586073637009
time_elpased: 1.408
batch start
#iterations: 45
currently lose_sum: 96.30468797683716
time_elpased: 1.402
batch start
#iterations: 46
currently lose_sum: 96.2036771774292
time_elpased: 1.433
batch start
#iterations: 47
currently lose_sum: 96.13397359848022
time_elpased: 1.396
batch start
#iterations: 48
currently lose_sum: 96.2448034286499
time_elpased: 1.37
batch start
#iterations: 49
currently lose_sum: 96.24568980932236
time_elpased: 1.41
batch start
#iterations: 50
currently lose_sum: 95.96158021688461
time_elpased: 1.427
batch start
#iterations: 51
currently lose_sum: 96.010622382164
time_elpased: 1.405
batch start
#iterations: 52
currently lose_sum: 95.92456406354904
time_elpased: 1.392
batch start
#iterations: 53
currently lose_sum: 95.8809425830841
time_elpased: 1.395
batch start
#iterations: 54
currently lose_sum: 95.95621681213379
time_elpased: 1.408
batch start
#iterations: 55
currently lose_sum: 95.96639150381088
time_elpased: 1.423
batch start
#iterations: 56
currently lose_sum: 96.0525136590004
time_elpased: 1.401
batch start
#iterations: 57
currently lose_sum: 95.99766385555267
time_elpased: 1.442
batch start
#iterations: 58
currently lose_sum: 95.89475989341736
time_elpased: 1.407
batch start
#iterations: 59
currently lose_sum: 95.9683912396431
time_elpased: 1.486
start validation test
0.644845360825
0.640828932948
0.661932695276
0.651209881543
0.644815361374
61.778
batch start
#iterations: 60
currently lose_sum: 96.20240837335587
time_elpased: 1.404
batch start
#iterations: 61
currently lose_sum: 96.08037573099136
time_elpased: 1.474
batch start
#iterations: 62
currently lose_sum: 95.99926418066025
time_elpased: 1.409
batch start
#iterations: 63
currently lose_sum: 96.2730861902237
time_elpased: 1.413
batch start
#iterations: 64
currently lose_sum: 96.1711995601654
time_elpased: 1.394
batch start
#iterations: 65
currently lose_sum: 96.00611197948456
time_elpased: 1.384
batch start
#iterations: 66
currently lose_sum: 95.66177767515182
time_elpased: 1.411
batch start
#iterations: 67
currently lose_sum: 95.65246641635895
time_elpased: 1.401
batch start
#iterations: 68
currently lose_sum: 95.44365096092224
time_elpased: 1.396
batch start
#iterations: 69
currently lose_sum: 96.21461814641953
time_elpased: 1.428
batch start
#iterations: 70
currently lose_sum: 95.85246926546097
time_elpased: 1.413
batch start
#iterations: 71
currently lose_sum: 95.56983822584152
time_elpased: 1.412
batch start
#iterations: 72
currently lose_sum: 95.46424162387848
time_elpased: 1.411
batch start
#iterations: 73
currently lose_sum: 95.73149782419205
time_elpased: 1.39
batch start
#iterations: 74
currently lose_sum: 95.5973853468895
time_elpased: 1.424
batch start
#iterations: 75
currently lose_sum: 95.90324026346207
time_elpased: 1.426
batch start
#iterations: 76
currently lose_sum: 95.75515502691269
time_elpased: 1.402
batch start
#iterations: 77
currently lose_sum: 95.5996081829071
time_elpased: 1.411
batch start
#iterations: 78
currently lose_sum: 95.82418459653854
time_elpased: 1.422
batch start
#iterations: 79
currently lose_sum: 95.39404535293579
time_elpased: 1.433
start validation test
0.656597938144
0.636298741858
0.733868477925
0.681609634869
0.656462277794
61.386
batch start
#iterations: 80
currently lose_sum: 95.42771917581558
time_elpased: 1.427
batch start
#iterations: 81
currently lose_sum: 95.66016358137131
time_elpased: 1.419
batch start
#iterations: 82
currently lose_sum: 95.63631528615952
time_elpased: 1.427
batch start
#iterations: 83
currently lose_sum: 95.26650255918503
time_elpased: 1.423
batch start
#iterations: 84
currently lose_sum: 95.91921770572662
time_elpased: 1.408
batch start
#iterations: 85
currently lose_sum: 95.71212303638458
time_elpased: 1.433
batch start
#iterations: 86
currently lose_sum: 95.64904397726059
time_elpased: 1.406
batch start
#iterations: 87
currently lose_sum: 95.48535567522049
time_elpased: 1.411
batch start
#iterations: 88
currently lose_sum: 95.74963855743408
time_elpased: 1.435
batch start
#iterations: 89
currently lose_sum: 95.80650395154953
time_elpased: 1.414
batch start
#iterations: 90
currently lose_sum: 95.44210296869278
time_elpased: 1.404
batch start
#iterations: 91
currently lose_sum: 95.98808854818344
time_elpased: 1.391
batch start
#iterations: 92
currently lose_sum: 95.4070925116539
time_elpased: 1.43
batch start
#iterations: 93
currently lose_sum: 95.13225728273392
time_elpased: 1.429
batch start
#iterations: 94
currently lose_sum: 95.74452584981918
time_elpased: 1.431
batch start
#iterations: 95
currently lose_sum: 95.84183168411255
time_elpased: 1.416
batch start
#iterations: 96
currently lose_sum: 95.66445863246918
time_elpased: 1.418
batch start
#iterations: 97
currently lose_sum: 95.4960452914238
time_elpased: 1.448
batch start
#iterations: 98
currently lose_sum: 95.78860104084015
time_elpased: 1.449
batch start
#iterations: 99
currently lose_sum: 95.73929870128632
time_elpased: 1.41
start validation test
0.651804123711
0.642239723396
0.688175362766
0.664414526305
0.651740268391
61.596
batch start
#iterations: 100
currently lose_sum: 95.872303545475
time_elpased: 1.412
batch start
#iterations: 101
currently lose_sum: 95.75168961286545
time_elpased: 1.409
batch start
#iterations: 102
currently lose_sum: 95.42166346311569
time_elpased: 1.446
batch start
#iterations: 103
currently lose_sum: 95.50261515378952
time_elpased: 1.412
batch start
#iterations: 104
currently lose_sum: 95.17936009168625
time_elpased: 1.398
batch start
#iterations: 105
currently lose_sum: 95.5029479265213
time_elpased: 1.404
batch start
#iterations: 106
currently lose_sum: 95.55789929628372
time_elpased: 1.423
batch start
#iterations: 107
currently lose_sum: 95.64143228530884
time_elpased: 1.42
batch start
#iterations: 108
currently lose_sum: 95.51163667440414
time_elpased: 1.422
batch start
#iterations: 109
currently lose_sum: 95.61920815706253
time_elpased: 1.454
batch start
#iterations: 110
currently lose_sum: 95.69733422994614
time_elpased: 1.405
batch start
#iterations: 111
currently lose_sum: 95.70622944831848
time_elpased: 1.4
batch start
#iterations: 112
currently lose_sum: 95.48575919866562
time_elpased: 1.426
batch start
#iterations: 113
currently lose_sum: 94.86341375112534
time_elpased: 1.393
batch start
#iterations: 114
currently lose_sum: 95.60701704025269
time_elpased: 1.414
batch start
#iterations: 115
currently lose_sum: 95.61624699831009
time_elpased: 1.421
batch start
#iterations: 116
currently lose_sum: 95.56458073854446
time_elpased: 1.405
batch start
#iterations: 117
currently lose_sum: 95.09909051656723
time_elpased: 1.422
batch start
#iterations: 118
currently lose_sum: 95.42332148551941
time_elpased: 1.471
batch start
#iterations: 119
currently lose_sum: 95.1880407333374
time_elpased: 1.405
start validation test
0.659278350515
0.643563441456
0.71668210353
0.678157561593
0.659177569373
61.176
batch start
#iterations: 120
currently lose_sum: 94.9953698515892
time_elpased: 1.406
batch start
#iterations: 121
currently lose_sum: 95.97200131416321
time_elpased: 1.421
batch start
#iterations: 122
currently lose_sum: 95.64089798927307
time_elpased: 1.393
batch start
#iterations: 123
currently lose_sum: 95.09102195501328
time_elpased: 1.435
batch start
#iterations: 124
currently lose_sum: 95.29342406988144
time_elpased: 1.399
batch start
#iterations: 125
currently lose_sum: 95.32230055332184
time_elpased: 1.444
batch start
#iterations: 126
currently lose_sum: 95.02680373191833
time_elpased: 1.415
batch start
#iterations: 127
currently lose_sum: 95.34998220205307
time_elpased: 1.454
batch start
#iterations: 128
currently lose_sum: 95.04560059309006
time_elpased: 1.491
batch start
#iterations: 129
currently lose_sum: 95.1814175248146
time_elpased: 1.386
batch start
#iterations: 130
currently lose_sum: 95.32212215662003
time_elpased: 1.423
batch start
#iterations: 131
currently lose_sum: 95.3809739947319
time_elpased: 1.445
batch start
#iterations: 132
currently lose_sum: 95.56012254953384
time_elpased: 1.424
batch start
#iterations: 133
currently lose_sum: 95.37419986724854
time_elpased: 1.463
batch start
#iterations: 134
currently lose_sum: 95.1760424375534
time_elpased: 1.425
batch start
#iterations: 135
currently lose_sum: 95.42832124233246
time_elpased: 1.42
batch start
#iterations: 136
currently lose_sum: 95.28693342208862
time_elpased: 1.392
batch start
#iterations: 137
currently lose_sum: 95.37834668159485
time_elpased: 1.4
batch start
#iterations: 138
currently lose_sum: 95.46573114395142
time_elpased: 1.397
batch start
#iterations: 139
currently lose_sum: 95.5135263800621
time_elpased: 1.435
start validation test
0.655103092784
0.642789731974
0.700936503036
0.670605031261
0.655022625162
61.315
batch start
#iterations: 140
currently lose_sum: 95.3611746430397
time_elpased: 1.393
batch start
#iterations: 141
currently lose_sum: 95.28129523992538
time_elpased: 1.439
batch start
#iterations: 142
currently lose_sum: 95.57659059762955
time_elpased: 1.408
batch start
#iterations: 143
currently lose_sum: 95.1547469496727
time_elpased: 1.404
batch start
#iterations: 144
currently lose_sum: 95.33259564638138
time_elpased: 1.464
batch start
#iterations: 145
currently lose_sum: 95.38673156499863
time_elpased: 1.446
batch start
#iterations: 146
currently lose_sum: 95.24848264455795
time_elpased: 1.391
batch start
#iterations: 147
currently lose_sum: 95.27050238847733
time_elpased: 1.43
batch start
#iterations: 148
currently lose_sum: 95.35055232048035
time_elpased: 1.482
batch start
#iterations: 149
currently lose_sum: 95.35473251342773
time_elpased: 1.415
batch start
#iterations: 150
currently lose_sum: 95.09534651041031
time_elpased: 1.421
batch start
#iterations: 151
currently lose_sum: 95.41210550069809
time_elpased: 1.408
batch start
#iterations: 152
currently lose_sum: 95.09396153688431
time_elpased: 1.418
batch start
#iterations: 153
currently lose_sum: 95.29908567667007
time_elpased: 1.397
batch start
#iterations: 154
currently lose_sum: 95.17047947645187
time_elpased: 1.452
batch start
#iterations: 155
currently lose_sum: 95.25119030475616
time_elpased: 1.46
batch start
#iterations: 156
currently lose_sum: 95.39651834964752
time_elpased: 1.42
batch start
#iterations: 157
currently lose_sum: 95.26441049575806
time_elpased: 1.468
batch start
#iterations: 158
currently lose_sum: 95.39231216907501
time_elpased: 1.397
batch start
#iterations: 159
currently lose_sum: 95.43140459060669
time_elpased: 1.406
start validation test
0.655618556701
0.641130531796
0.709684058866
0.673667757534
0.655523636373
61.319
batch start
#iterations: 160
currently lose_sum: 95.37871193885803
time_elpased: 1.425
batch start
#iterations: 161
currently lose_sum: 95.44999957084656
time_elpased: 1.409
batch start
#iterations: 162
currently lose_sum: 95.15133994817734
time_elpased: 1.401
batch start
#iterations: 163
currently lose_sum: 95.2124012708664
time_elpased: 1.409
batch start
#iterations: 164
currently lose_sum: 95.45550113916397
time_elpased: 1.412
batch start
#iterations: 165
currently lose_sum: 95.17545759677887
time_elpased: 1.429
batch start
#iterations: 166
currently lose_sum: 95.26175040006638
time_elpased: 1.43
batch start
#iterations: 167
currently lose_sum: 95.64773887395859
time_elpased: 1.404
batch start
#iterations: 168
currently lose_sum: 95.29745137691498
time_elpased: 1.415
batch start
#iterations: 169
currently lose_sum: 95.28921121358871
time_elpased: 1.402
batch start
#iterations: 170
currently lose_sum: 95.0252075791359
time_elpased: 1.405
batch start
#iterations: 171
currently lose_sum: 95.27626758813858
time_elpased: 1.415
batch start
#iterations: 172
currently lose_sum: 94.9231293797493
time_elpased: 1.418
batch start
#iterations: 173
currently lose_sum: 94.86251240968704
time_elpased: 1.494
batch start
#iterations: 174
currently lose_sum: 95.08902943134308
time_elpased: 1.428
batch start
#iterations: 175
currently lose_sum: 95.26356643438339
time_elpased: 1.421
batch start
#iterations: 176
currently lose_sum: 95.54619830846786
time_elpased: 1.423
batch start
#iterations: 177
currently lose_sum: 95.62172222137451
time_elpased: 1.415
batch start
#iterations: 178
currently lose_sum: 94.9601993560791
time_elpased: 1.415
batch start
#iterations: 179
currently lose_sum: 94.98554962873459
time_elpased: 1.429
start validation test
0.655824742268
0.641685309471
0.708449109808
0.673416483248
0.655732352074
61.212
batch start
#iterations: 180
currently lose_sum: 95.27012825012207
time_elpased: 1.44
batch start
#iterations: 181
currently lose_sum: 95.21136397123337
time_elpased: 1.437
batch start
#iterations: 182
currently lose_sum: 95.20880979299545
time_elpased: 1.42
batch start
#iterations: 183
currently lose_sum: 94.96601873636246
time_elpased: 1.41
batch start
#iterations: 184
currently lose_sum: 95.19034731388092
time_elpased: 1.428
batch start
#iterations: 185
currently lose_sum: 95.03819614648819
time_elpased: 1.389
batch start
#iterations: 186
currently lose_sum: 94.93824177980423
time_elpased: 1.436
batch start
#iterations: 187
currently lose_sum: 95.28823912143707
time_elpased: 1.43
batch start
#iterations: 188
currently lose_sum: 95.15688782930374
time_elpased: 1.444
batch start
#iterations: 189
currently lose_sum: 95.34306997060776
time_elpased: 1.501
batch start
#iterations: 190
currently lose_sum: 95.01655811071396
time_elpased: 1.408
batch start
#iterations: 191
currently lose_sum: 95.19203174114227
time_elpased: 1.418
batch start
#iterations: 192
currently lose_sum: 95.16961061954498
time_elpased: 1.445
batch start
#iterations: 193
currently lose_sum: 94.91506171226501
time_elpased: 1.428
batch start
#iterations: 194
currently lose_sum: 95.07199221849442
time_elpased: 1.432
batch start
#iterations: 195
currently lose_sum: 94.9691818356514
time_elpased: 1.414
batch start
#iterations: 196
currently lose_sum: 94.93828058242798
time_elpased: 1.442
batch start
#iterations: 197
currently lose_sum: 95.33848577737808
time_elpased: 1.423
batch start
#iterations: 198
currently lose_sum: 94.873191177845
time_elpased: 1.417
batch start
#iterations: 199
currently lose_sum: 94.77665048837662
time_elpased: 1.444
start validation test
0.656391752577
0.644501278772
0.700216116085
0.671204498372
0.656314812148
61.229
batch start
#iterations: 200
currently lose_sum: 94.770643055439
time_elpased: 1.405
batch start
#iterations: 201
currently lose_sum: 95.28165131807327
time_elpased: 1.416
batch start
#iterations: 202
currently lose_sum: 94.99000519514084
time_elpased: 1.417
batch start
#iterations: 203
currently lose_sum: 95.23317617177963
time_elpased: 1.415
batch start
#iterations: 204
currently lose_sum: 94.97755551338196
time_elpased: 1.419
batch start
#iterations: 205
currently lose_sum: 94.96056425571442
time_elpased: 1.405
batch start
#iterations: 206
currently lose_sum: 94.96668821573257
time_elpased: 1.4
batch start
#iterations: 207
currently lose_sum: 95.05247449874878
time_elpased: 1.436
batch start
#iterations: 208
currently lose_sum: 94.84662014245987
time_elpased: 1.411
batch start
#iterations: 209
currently lose_sum: 94.94892424345016
time_elpased: 1.431
batch start
#iterations: 210
currently lose_sum: 94.79763972759247
time_elpased: 1.396
batch start
#iterations: 211
currently lose_sum: 94.98011034727097
time_elpased: 1.404
batch start
#iterations: 212
currently lose_sum: 94.9369186758995
time_elpased: 1.405
batch start
#iterations: 213
currently lose_sum: 94.83881944417953
time_elpased: 1.413
batch start
#iterations: 214
currently lose_sum: 94.9842426776886
time_elpased: 1.396
batch start
#iterations: 215
currently lose_sum: 94.67134791612625
time_elpased: 1.407
batch start
#iterations: 216
currently lose_sum: 94.8851820230484
time_elpased: 1.443
batch start
#iterations: 217
currently lose_sum: 95.0809475183487
time_elpased: 1.403
batch start
#iterations: 218
currently lose_sum: 94.90117865800858
time_elpased: 1.406
batch start
#iterations: 219
currently lose_sum: 94.78420203924179
time_elpased: 1.439
start validation test
0.658505154639
0.637568962449
0.737367500257
0.68384633739
0.658366699628
61.134
batch start
#iterations: 220
currently lose_sum: 94.67609375715256
time_elpased: 1.403
batch start
#iterations: 221
currently lose_sum: 95.07569336891174
time_elpased: 1.45
batch start
#iterations: 222
currently lose_sum: 94.91717034578323
time_elpased: 1.384
batch start
#iterations: 223
currently lose_sum: 95.25534248352051
time_elpased: 1.405
batch start
#iterations: 224
currently lose_sum: 95.35146272182465
time_elpased: 1.394
batch start
#iterations: 225
currently lose_sum: 94.91206830739975
time_elpased: 1.474
batch start
#iterations: 226
currently lose_sum: 94.83615338802338
time_elpased: 1.409
batch start
#iterations: 227
currently lose_sum: 94.53581529855728
time_elpased: 1.415
batch start
#iterations: 228
currently lose_sum: 95.14844167232513
time_elpased: 1.427
batch start
#iterations: 229
currently lose_sum: 94.94331675767899
time_elpased: 1.429
batch start
#iterations: 230
currently lose_sum: 95.1228655576706
time_elpased: 1.385
batch start
#iterations: 231
currently lose_sum: 95.11828708648682
time_elpased: 1.46
batch start
#iterations: 232
currently lose_sum: 94.88105183839798
time_elpased: 1.396
batch start
#iterations: 233
currently lose_sum: 94.91749662160873
time_elpased: 1.389
batch start
#iterations: 234
currently lose_sum: 95.00282227993011
time_elpased: 1.432
batch start
#iterations: 235
currently lose_sum: 95.0094844698906
time_elpased: 1.411
batch start
#iterations: 236
currently lose_sum: 95.20169007778168
time_elpased: 1.424
batch start
#iterations: 237
currently lose_sum: 95.27326679229736
time_elpased: 1.392
batch start
#iterations: 238
currently lose_sum: 94.82181966304779
time_elpased: 1.393
batch start
#iterations: 239
currently lose_sum: 94.64358025789261
time_elpased: 1.399
start validation test
0.650309278351
0.640536655486
0.687866625502
0.663358475586
0.650243340635
61.187
batch start
#iterations: 240
currently lose_sum: 95.06555980443954
time_elpased: 1.432
batch start
#iterations: 241
currently lose_sum: 95.21135318279266
time_elpased: 1.42
batch start
#iterations: 242
currently lose_sum: 94.95192342996597
time_elpased: 1.396
batch start
#iterations: 243
currently lose_sum: 95.16361725330353
time_elpased: 1.424
batch start
#iterations: 244
currently lose_sum: 95.14442825317383
time_elpased: 1.424
batch start
#iterations: 245
currently lose_sum: 94.77102905511856
time_elpased: 1.424
batch start
#iterations: 246
currently lose_sum: 95.21389931440353
time_elpased: 1.411
batch start
#iterations: 247
currently lose_sum: 94.90973734855652
time_elpased: 1.413
batch start
#iterations: 248
currently lose_sum: 95.17560285329819
time_elpased: 1.408
batch start
#iterations: 249
currently lose_sum: 94.90628629922867
time_elpased: 1.408
batch start
#iterations: 250
currently lose_sum: 95.01458752155304
time_elpased: 1.431
batch start
#iterations: 251
currently lose_sum: 94.91058433055878
time_elpased: 1.456
batch start
#iterations: 252
currently lose_sum: 95.0777622461319
time_elpased: 1.396
batch start
#iterations: 253
currently lose_sum: 95.03522217273712
time_elpased: 1.405
batch start
#iterations: 254
currently lose_sum: 94.75594568252563
time_elpased: 1.401
batch start
#iterations: 255
currently lose_sum: 95.54068964719772
time_elpased: 1.431
batch start
#iterations: 256
currently lose_sum: 94.54645496606827
time_elpased: 1.414
batch start
#iterations: 257
currently lose_sum: 95.05988347530365
time_elpased: 1.405
batch start
#iterations: 258
currently lose_sum: 94.87111538648605
time_elpased: 1.394
batch start
#iterations: 259
currently lose_sum: 94.75899928808212
time_elpased: 1.409
start validation test
0.650927835052
0.641982451065
0.685190902542
0.662883313421
0.650867680952
61.371
batch start
#iterations: 260
currently lose_sum: 94.98521518707275
time_elpased: 1.416
batch start
#iterations: 261
currently lose_sum: 94.43673902750015
time_elpased: 1.442
batch start
#iterations: 262
currently lose_sum: 94.87061470746994
time_elpased: 1.404
batch start
#iterations: 263
currently lose_sum: 95.02194648981094
time_elpased: 1.414
batch start
#iterations: 264
currently lose_sum: 95.05458843708038
time_elpased: 1.421
batch start
#iterations: 265
currently lose_sum: 95.10067594051361
time_elpased: 1.418
batch start
#iterations: 266
currently lose_sum: 94.7026839852333
time_elpased: 1.404
batch start
#iterations: 267
currently lose_sum: 95.10697227716446
time_elpased: 1.406
batch start
#iterations: 268
currently lose_sum: 94.8755754828453
time_elpased: 1.425
batch start
#iterations: 269
currently lose_sum: 95.23869305849075
time_elpased: 1.394
batch start
#iterations: 270
currently lose_sum: 95.02113550901413
time_elpased: 1.442
batch start
#iterations: 271
currently lose_sum: 94.65781408548355
time_elpased: 1.438
batch start
#iterations: 272
currently lose_sum: 95.04796701669693
time_elpased: 1.433
batch start
#iterations: 273
currently lose_sum: 94.99878984689713
time_elpased: 1.463
batch start
#iterations: 274
currently lose_sum: 95.30448800325394
time_elpased: 1.411
batch start
#iterations: 275
currently lose_sum: 94.78999215364456
time_elpased: 1.444
batch start
#iterations: 276
currently lose_sum: 94.88314354419708
time_elpased: 1.403
batch start
#iterations: 277
currently lose_sum: 95.14892214536667
time_elpased: 1.417
batch start
#iterations: 278
currently lose_sum: 95.0913353562355
time_elpased: 1.402
batch start
#iterations: 279
currently lose_sum: 94.66957807540894
time_elpased: 1.41
start validation test
0.659793814433
0.641437517016
0.727384995369
0.681712962963
0.65967514769
61.260
batch start
#iterations: 280
currently lose_sum: 95.13073682785034
time_elpased: 1.413
batch start
#iterations: 281
currently lose_sum: 94.74525421857834
time_elpased: 1.503
batch start
#iterations: 282
currently lose_sum: 94.91986536979675
time_elpased: 1.402
batch start
#iterations: 283
currently lose_sum: 94.91586363315582
time_elpased: 1.421
batch start
#iterations: 284
currently lose_sum: 94.51051193475723
time_elpased: 1.416
batch start
#iterations: 285
currently lose_sum: 94.64222663640976
time_elpased: 1.443
batch start
#iterations: 286
currently lose_sum: 94.87001705169678
time_elpased: 1.413
batch start
#iterations: 287
currently lose_sum: 94.60642719268799
time_elpased: 1.396
batch start
#iterations: 288
currently lose_sum: 94.85967701673508
time_elpased: 1.421
batch start
#iterations: 289
currently lose_sum: 94.708551466465
time_elpased: 1.394
batch start
#iterations: 290
currently lose_sum: 94.83410203456879
time_elpased: 1.468
batch start
#iterations: 291
currently lose_sum: 94.51564925909042
time_elpased: 1.428
batch start
#iterations: 292
currently lose_sum: 94.97498947381973
time_elpased: 1.42
batch start
#iterations: 293
currently lose_sum: 94.8810510635376
time_elpased: 1.407
batch start
#iterations: 294
currently lose_sum: 94.53569054603577
time_elpased: 1.446
batch start
#iterations: 295
currently lose_sum: 94.52774304151535
time_elpased: 1.438
batch start
#iterations: 296
currently lose_sum: 94.4567620754242
time_elpased: 1.424
batch start
#iterations: 297
currently lose_sum: 95.08734726905823
time_elpased: 1.405
batch start
#iterations: 298
currently lose_sum: 94.63477122783661
time_elpased: 1.414
batch start
#iterations: 299
currently lose_sum: 94.72601437568665
time_elpased: 1.409
start validation test
0.658969072165
0.643445277084
0.715755891736
0.677677092468
0.658869374144
61.239
batch start
#iterations: 300
currently lose_sum: 94.42100024223328
time_elpased: 1.373
batch start
#iterations: 301
currently lose_sum: 94.76305800676346
time_elpased: 1.394
batch start
#iterations: 302
currently lose_sum: 94.92107772827148
time_elpased: 1.378
batch start
#iterations: 303
currently lose_sum: 94.87274301052094
time_elpased: 1.424
batch start
#iterations: 304
currently lose_sum: 94.75192481279373
time_elpased: 1.412
batch start
#iterations: 305
currently lose_sum: 94.69154012203217
time_elpased: 1.423
batch start
#iterations: 306
currently lose_sum: 94.5460661649704
time_elpased: 1.405
batch start
#iterations: 307
currently lose_sum: 94.61557525396347
time_elpased: 1.46
batch start
#iterations: 308
currently lose_sum: 94.88576954603195
time_elpased: 1.395
batch start
#iterations: 309
currently lose_sum: 94.75034230947495
time_elpased: 1.417
batch start
#iterations: 310
currently lose_sum: 94.95944559574127
time_elpased: 1.432
batch start
#iterations: 311
currently lose_sum: 94.60529041290283
time_elpased: 1.417
batch start
#iterations: 312
currently lose_sum: 95.10998547077179
time_elpased: 1.412
batch start
#iterations: 313
currently lose_sum: 94.27126622200012
time_elpased: 1.396
batch start
#iterations: 314
currently lose_sum: 94.39740055799484
time_elpased: 1.402
batch start
#iterations: 315
currently lose_sum: 94.54123145341873
time_elpased: 1.432
batch start
#iterations: 316
currently lose_sum: 94.70091193914413
time_elpased: 1.413
batch start
#iterations: 317
currently lose_sum: 94.91220992803574
time_elpased: 1.413
batch start
#iterations: 318
currently lose_sum: 94.92584723234177
time_elpased: 1.405
batch start
#iterations: 319
currently lose_sum: 94.7085651755333
time_elpased: 1.482
start validation test
0.652010309278
0.636255053289
0.71266851909
0.672297461288
0.651903814435
61.273
batch start
#iterations: 320
currently lose_sum: 94.927694439888
time_elpased: 1.413
batch start
#iterations: 321
currently lose_sum: 94.50437545776367
time_elpased: 1.459
batch start
#iterations: 322
currently lose_sum: 95.14168930053711
time_elpased: 1.404
batch start
#iterations: 323
currently lose_sum: 94.58480894565582
time_elpased: 1.442
batch start
#iterations: 324
currently lose_sum: 94.98427832126617
time_elpased: 1.416
batch start
#iterations: 325
currently lose_sum: 94.61297333240509
time_elpased: 1.425
batch start
#iterations: 326
currently lose_sum: 95.07005715370178
time_elpased: 1.409
batch start
#iterations: 327
currently lose_sum: 94.58855205774307
time_elpased: 1.392
batch start
#iterations: 328
currently lose_sum: 94.69616901874542
time_elpased: 1.413
batch start
#iterations: 329
currently lose_sum: 94.46716392040253
time_elpased: 1.415
batch start
#iterations: 330
currently lose_sum: 94.63724684715271
time_elpased: 1.405
batch start
#iterations: 331
currently lose_sum: 94.84225195646286
time_elpased: 1.464
batch start
#iterations: 332
currently lose_sum: 94.54363083839417
time_elpased: 1.402
batch start
#iterations: 333
currently lose_sum: 94.47663521766663
time_elpased: 1.403
batch start
#iterations: 334
currently lose_sum: 95.38877218961716
time_elpased: 1.412
batch start
#iterations: 335
currently lose_sum: 94.46823072433472
time_elpased: 1.454
batch start
#iterations: 336
currently lose_sum: 94.7957010269165
time_elpased: 1.417
batch start
#iterations: 337
currently lose_sum: 94.73098182678223
time_elpased: 1.406
batch start
#iterations: 338
currently lose_sum: 94.58495247364044
time_elpased: 1.439
batch start
#iterations: 339
currently lose_sum: 94.61421078443527
time_elpased: 1.421
start validation test
0.657731958763
0.637378337352
0.734588864876
0.68253968254
0.65759702461
61.178
batch start
#iterations: 340
currently lose_sum: 94.45872020721436
time_elpased: 1.427
batch start
#iterations: 341
currently lose_sum: 95.26502788066864
time_elpased: 1.399
batch start
#iterations: 342
currently lose_sum: 94.44581115245819
time_elpased: 1.416
batch start
#iterations: 343
currently lose_sum: 94.68221712112427
time_elpased: 1.425
batch start
#iterations: 344
currently lose_sum: 94.64842689037323
time_elpased: 1.407
batch start
#iterations: 345
currently lose_sum: 94.79799264669418
time_elpased: 1.457
batch start
#iterations: 346
currently lose_sum: 94.5942617058754
time_elpased: 1.44
batch start
#iterations: 347
currently lose_sum: 94.44656431674957
time_elpased: 1.438
batch start
#iterations: 348
currently lose_sum: 94.84048944711685
time_elpased: 1.39
batch start
#iterations: 349
currently lose_sum: 94.79867452383041
time_elpased: 1.403
batch start
#iterations: 350
currently lose_sum: 94.72187572717667
time_elpased: 1.41
batch start
#iterations: 351
currently lose_sum: 94.9338926076889
time_elpased: 1.393
batch start
#iterations: 352
currently lose_sum: 94.38388985395432
time_elpased: 1.415
batch start
#iterations: 353
currently lose_sum: 94.76373100280762
time_elpased: 1.415
batch start
#iterations: 354
currently lose_sum: 94.73173296451569
time_elpased: 1.399
batch start
#iterations: 355
currently lose_sum: 94.4442275762558
time_elpased: 1.422
batch start
#iterations: 356
currently lose_sum: 94.80027383565903
time_elpased: 1.382
batch start
#iterations: 357
currently lose_sum: 94.66024333238602
time_elpased: 1.38
batch start
#iterations: 358
currently lose_sum: 94.26263427734375
time_elpased: 1.412
batch start
#iterations: 359
currently lose_sum: 94.85134410858154
time_elpased: 1.442
start validation test
0.653402061856
0.63748277446
0.714109292992
0.673623920008
0.653295480948
61.357
batch start
#iterations: 360
currently lose_sum: 94.75947767496109
time_elpased: 1.392
batch start
#iterations: 361
currently lose_sum: 94.75567764043808
time_elpased: 1.408
batch start
#iterations: 362
currently lose_sum: 94.81779956817627
time_elpased: 1.391
batch start
#iterations: 363
currently lose_sum: 94.60719037055969
time_elpased: 1.409
batch start
#iterations: 364
currently lose_sum: 94.65597712993622
time_elpased: 1.421
batch start
#iterations: 365
currently lose_sum: 94.94008988142014
time_elpased: 1.384
batch start
#iterations: 366
currently lose_sum: 95.01352739334106
time_elpased: 1.41
batch start
#iterations: 367
currently lose_sum: 94.56059461832047
time_elpased: 1.41
batch start
#iterations: 368
currently lose_sum: 94.63916236162186
time_elpased: 1.396
batch start
#iterations: 369
currently lose_sum: 95.12716269493103
time_elpased: 1.403
batch start
#iterations: 370
currently lose_sum: 94.76673811674118
time_elpased: 1.405
batch start
#iterations: 371
currently lose_sum: 94.67216283082962
time_elpased: 1.408
batch start
#iterations: 372
currently lose_sum: 94.93712568283081
time_elpased: 1.465
batch start
#iterations: 373
currently lose_sum: 94.84545105695724
time_elpased: 1.428
batch start
#iterations: 374
currently lose_sum: 94.80113226175308
time_elpased: 1.426
batch start
#iterations: 375
currently lose_sum: 94.72293800115585
time_elpased: 1.415
batch start
#iterations: 376
currently lose_sum: 94.84945636987686
time_elpased: 1.394
batch start
#iterations: 377
currently lose_sum: 94.73063200712204
time_elpased: 1.41
batch start
#iterations: 378
currently lose_sum: 94.68173885345459
time_elpased: 1.425
batch start
#iterations: 379
currently lose_sum: 94.95345973968506
time_elpased: 1.438
start validation test
0.658298969072
0.642068457858
0.718122877431
0.677969395191
0.658193938974
61.229
batch start
#iterations: 380
currently lose_sum: 95.01631045341492
time_elpased: 1.43
batch start
#iterations: 381
currently lose_sum: 94.91883778572083
time_elpased: 1.399
batch start
#iterations: 382
currently lose_sum: 94.6490797996521
time_elpased: 1.409
batch start
#iterations: 383
currently lose_sum: 94.26914721727371
time_elpased: 1.389
batch start
#iterations: 384
currently lose_sum: 94.49107003211975
time_elpased: 1.398
batch start
#iterations: 385
currently lose_sum: 94.91386383771896
time_elpased: 1.423
batch start
#iterations: 386
currently lose_sum: 94.7126494050026
time_elpased: 1.411
batch start
#iterations: 387
currently lose_sum: 94.86701649427414
time_elpased: 1.402
batch start
#iterations: 388
currently lose_sum: 95.055251121521
time_elpased: 1.409
batch start
#iterations: 389
currently lose_sum: 94.74639159440994
time_elpased: 1.455
batch start
#iterations: 390
currently lose_sum: 94.71088773012161
time_elpased: 1.42
batch start
#iterations: 391
currently lose_sum: 94.762659907341
time_elpased: 1.392
batch start
#iterations: 392
currently lose_sum: 95.06490647792816
time_elpased: 1.402
batch start
#iterations: 393
currently lose_sum: 94.69800621271133
time_elpased: 1.385
batch start
#iterations: 394
currently lose_sum: 94.59783864021301
time_elpased: 1.401
batch start
#iterations: 395
currently lose_sum: 94.66399866342545
time_elpased: 1.475
batch start
#iterations: 396
currently lose_sum: 94.44421070814133
time_elpased: 1.448
batch start
#iterations: 397
currently lose_sum: 94.94918340444565
time_elpased: 1.391
batch start
#iterations: 398
currently lose_sum: 94.87149804830551
time_elpased: 1.388
batch start
#iterations: 399
currently lose_sum: 94.34522223472595
time_elpased: 1.418
start validation test
0.66087628866
0.635515633097
0.757229597612
0.691054238084
0.660707125564
61.236
acc: 0.655
pre: 0.634
rec: 0.735
F1: 0.681
auc: 0.694
