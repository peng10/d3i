start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.67012226581573
time_elpased: 1.425
batch start
#iterations: 1
currently lose_sum: 100.26115584373474
time_elpased: 1.399
batch start
#iterations: 2
currently lose_sum: 100.26574611663818
time_elpased: 1.461
batch start
#iterations: 3
currently lose_sum: 100.28756040334702
time_elpased: 1.427
batch start
#iterations: 4
currently lose_sum: 100.1179671883583
time_elpased: 1.418
batch start
#iterations: 5
currently lose_sum: 100.1392353773117
time_elpased: 1.425
batch start
#iterations: 6
currently lose_sum: 100.15374511480331
time_elpased: 1.423
batch start
#iterations: 7
currently lose_sum: 99.88775688409805
time_elpased: 1.436
batch start
#iterations: 8
currently lose_sum: 100.02955853939056
time_elpased: 1.404
batch start
#iterations: 9
currently lose_sum: 99.89085406064987
time_elpased: 1.428
batch start
#iterations: 10
currently lose_sum: 99.78896576166153
time_elpased: 1.411
batch start
#iterations: 11
currently lose_sum: 99.81237936019897
time_elpased: 1.439
batch start
#iterations: 12
currently lose_sum: 99.81808811426163
time_elpased: 1.373
batch start
#iterations: 13
currently lose_sum: 99.87704557180405
time_elpased: 1.425
batch start
#iterations: 14
currently lose_sum: 99.81287324428558
time_elpased: 1.401
batch start
#iterations: 15
currently lose_sum: 99.72533613443375
time_elpased: 1.399
batch start
#iterations: 16
currently lose_sum: 99.55002999305725
time_elpased: 1.392
batch start
#iterations: 17
currently lose_sum: 99.62283325195312
time_elpased: 1.386
batch start
#iterations: 18
currently lose_sum: 99.73403805494308
time_elpased: 1.4
batch start
#iterations: 19
currently lose_sum: 99.56679999828339
time_elpased: 1.402
start validation test
0.591391752577
0.631424375918
0.442523412576
0.520360622012
0.59165311391
65.674
batch start
#iterations: 20
currently lose_sum: 99.59027445316315
time_elpased: 1.475
batch start
#iterations: 21
currently lose_sum: 99.46035695075989
time_elpased: 1.485
batch start
#iterations: 22
currently lose_sum: 99.48586201667786
time_elpased: 1.387
batch start
#iterations: 23
currently lose_sum: 99.38024944067001
time_elpased: 1.442
batch start
#iterations: 24
currently lose_sum: 99.58563822507858
time_elpased: 1.413
batch start
#iterations: 25
currently lose_sum: 99.55651181936264
time_elpased: 1.395
batch start
#iterations: 26
currently lose_sum: 99.49821239709854
time_elpased: 1.411
batch start
#iterations: 27
currently lose_sum: 99.29742515087128
time_elpased: 1.406
batch start
#iterations: 28
currently lose_sum: 99.32485848665237
time_elpased: 1.4
batch start
#iterations: 29
currently lose_sum: 99.3016277551651
time_elpased: 1.403
batch start
#iterations: 30
currently lose_sum: 99.4181182384491
time_elpased: 1.425
batch start
#iterations: 31
currently lose_sum: 99.27546882629395
time_elpased: 1.387
batch start
#iterations: 32
currently lose_sum: 99.3584069609642
time_elpased: 1.41
batch start
#iterations: 33
currently lose_sum: 99.28212696313858
time_elpased: 1.385
batch start
#iterations: 34
currently lose_sum: 99.32927465438843
time_elpased: 1.409
batch start
#iterations: 35
currently lose_sum: 99.3236436843872
time_elpased: 1.405
batch start
#iterations: 36
currently lose_sum: 99.10265743732452
time_elpased: 1.416
batch start
#iterations: 37
currently lose_sum: 99.50048512220383
time_elpased: 1.379
batch start
#iterations: 38
currently lose_sum: 99.08608615398407
time_elpased: 1.416
batch start
#iterations: 39
currently lose_sum: 99.22048312425613
time_elpased: 1.414
start validation test
0.60293814433
0.619596199525
0.536894103118
0.57528808513
0.603054094831
65.142
batch start
#iterations: 40
currently lose_sum: 99.30959117412567
time_elpased: 1.465
batch start
#iterations: 41
currently lose_sum: 99.28321355581284
time_elpased: 1.407
batch start
#iterations: 42
currently lose_sum: 99.16171073913574
time_elpased: 1.465
batch start
#iterations: 43
currently lose_sum: 99.06162488460541
time_elpased: 1.42
batch start
#iterations: 44
currently lose_sum: 99.38493549823761
time_elpased: 1.421
batch start
#iterations: 45
currently lose_sum: 99.15895336866379
time_elpased: 1.385
batch start
#iterations: 46
currently lose_sum: 98.92591840028763
time_elpased: 1.398
batch start
#iterations: 47
currently lose_sum: 99.12945234775543
time_elpased: 1.383
batch start
#iterations: 48
currently lose_sum: 99.24748367071152
time_elpased: 1.399
batch start
#iterations: 49
currently lose_sum: 99.36821639537811
time_elpased: 1.405
batch start
#iterations: 50
currently lose_sum: 99.13247275352478
time_elpased: 1.443
batch start
#iterations: 51
currently lose_sum: 99.20592200756073
time_elpased: 1.393
batch start
#iterations: 52
currently lose_sum: 99.1503215432167
time_elpased: 1.398
batch start
#iterations: 53
currently lose_sum: 99.08902823925018
time_elpased: 1.38
batch start
#iterations: 54
currently lose_sum: 99.16116273403168
time_elpased: 1.434
batch start
#iterations: 55
currently lose_sum: 99.39333200454712
time_elpased: 1.412
batch start
#iterations: 56
currently lose_sum: 99.10108935832977
time_elpased: 1.402
batch start
#iterations: 57
currently lose_sum: 99.09616667032242
time_elpased: 1.45
batch start
#iterations: 58
currently lose_sum: 99.05168336629868
time_elpased: 1.468
batch start
#iterations: 59
currently lose_sum: 99.08357518911362
time_elpased: 1.401
start validation test
0.602783505155
0.623692951163
0.521765977153
0.568194553401
0.602925743921
65.046
batch start
#iterations: 60
currently lose_sum: 99.02630352973938
time_elpased: 1.417
batch start
#iterations: 61
currently lose_sum: 99.21791952848434
time_elpased: 1.457
batch start
#iterations: 62
currently lose_sum: 98.96676206588745
time_elpased: 1.396
batch start
#iterations: 63
currently lose_sum: 99.30017966032028
time_elpased: 1.45
batch start
#iterations: 64
currently lose_sum: 99.18098795413971
time_elpased: 1.399
batch start
#iterations: 65
currently lose_sum: 98.81727415323257
time_elpased: 1.409
batch start
#iterations: 66
currently lose_sum: 99.0050756931305
time_elpased: 1.433
batch start
#iterations: 67
currently lose_sum: 99.01199197769165
time_elpased: 1.428
batch start
#iterations: 68
currently lose_sum: 98.98591440916061
time_elpased: 1.386
batch start
#iterations: 69
currently lose_sum: 99.13919413089752
time_elpased: 1.408
batch start
#iterations: 70
currently lose_sum: 99.04829275608063
time_elpased: 1.441
batch start
#iterations: 71
currently lose_sum: 99.26711201667786
time_elpased: 1.409
batch start
#iterations: 72
currently lose_sum: 99.09997844696045
time_elpased: 1.423
batch start
#iterations: 73
currently lose_sum: 99.0007717013359
time_elpased: 1.379
batch start
#iterations: 74
currently lose_sum: 98.85591500997543
time_elpased: 1.441
batch start
#iterations: 75
currently lose_sum: 98.90056788921356
time_elpased: 1.421
batch start
#iterations: 76
currently lose_sum: 98.72957020998001
time_elpased: 1.417
batch start
#iterations: 77
currently lose_sum: 99.00335520505905
time_elpased: 1.441
batch start
#iterations: 78
currently lose_sum: 98.86634510755539
time_elpased: 1.394
batch start
#iterations: 79
currently lose_sum: 99.14369583129883
time_elpased: 1.392
start validation test
0.598144329897
0.624530014262
0.495729134507
0.55272518646
0.598324135569
65.107
batch start
#iterations: 80
currently lose_sum: 98.9497640132904
time_elpased: 1.382
batch start
#iterations: 81
currently lose_sum: 98.76983153820038
time_elpased: 1.405
batch start
#iterations: 82
currently lose_sum: 98.85280913114548
time_elpased: 1.384
batch start
#iterations: 83
currently lose_sum: 98.81315451860428
time_elpased: 1.427
batch start
#iterations: 84
currently lose_sum: 98.95635569095612
time_elpased: 1.385
batch start
#iterations: 85
currently lose_sum: 99.05745828151703
time_elpased: 1.381
batch start
#iterations: 86
currently lose_sum: 99.16680121421814
time_elpased: 1.407
batch start
#iterations: 87
currently lose_sum: 98.99987065792084
time_elpased: 1.442
batch start
#iterations: 88
currently lose_sum: 98.75804835557938
time_elpased: 1.388
batch start
#iterations: 89
currently lose_sum: 98.60944068431854
time_elpased: 1.413
batch start
#iterations: 90
currently lose_sum: 99.11069172620773
time_elpased: 1.381
batch start
#iterations: 91
currently lose_sum: 99.05666047334671
time_elpased: 1.486
batch start
#iterations: 92
currently lose_sum: 98.77130788564682
time_elpased: 1.398
batch start
#iterations: 93
currently lose_sum: 98.84719789028168
time_elpased: 1.404
batch start
#iterations: 94
currently lose_sum: 98.81256783008575
time_elpased: 1.445
batch start
#iterations: 95
currently lose_sum: 98.89103996753693
time_elpased: 1.393
batch start
#iterations: 96
currently lose_sum: 98.74072426557541
time_elpased: 1.372
batch start
#iterations: 97
currently lose_sum: 98.97770363092422
time_elpased: 1.395
batch start
#iterations: 98
currently lose_sum: 98.90152597427368
time_elpased: 1.416
batch start
#iterations: 99
currently lose_sum: 98.79584538936615
time_elpased: 1.399
start validation test
0.602371134021
0.626659921588
0.509931048678
0.562301407172
0.602533426848
64.904
batch start
#iterations: 100
currently lose_sum: 98.79138159751892
time_elpased: 1.413
batch start
#iterations: 101
currently lose_sum: 98.77129966020584
time_elpased: 1.41
batch start
#iterations: 102
currently lose_sum: 98.6889568567276
time_elpased: 1.417
batch start
#iterations: 103
currently lose_sum: 99.12174928188324
time_elpased: 1.433
batch start
#iterations: 104
currently lose_sum: 99.0425278544426
time_elpased: 1.395
batch start
#iterations: 105
currently lose_sum: 98.80428570508957
time_elpased: 1.465
batch start
#iterations: 106
currently lose_sum: 98.7786500453949
time_elpased: 1.408
batch start
#iterations: 107
currently lose_sum: 99.0614486336708
time_elpased: 1.418
batch start
#iterations: 108
currently lose_sum: 99.02668750286102
time_elpased: 1.43
batch start
#iterations: 109
currently lose_sum: 98.8310015797615
time_elpased: 1.431
batch start
#iterations: 110
currently lose_sum: 98.88325065374374
time_elpased: 1.411
batch start
#iterations: 111
currently lose_sum: 98.82435941696167
time_elpased: 1.41
batch start
#iterations: 112
currently lose_sum: 98.89154851436615
time_elpased: 1.47
batch start
#iterations: 113
currently lose_sum: 98.89326173067093
time_elpased: 1.402
batch start
#iterations: 114
currently lose_sum: 98.82774329185486
time_elpased: 1.422
batch start
#iterations: 115
currently lose_sum: 98.77149575948715
time_elpased: 1.41
batch start
#iterations: 116
currently lose_sum: 98.97611850500107
time_elpased: 1.424
batch start
#iterations: 117
currently lose_sum: 98.8018102645874
time_elpased: 1.397
batch start
#iterations: 118
currently lose_sum: 99.05560541152954
time_elpased: 1.434
batch start
#iterations: 119
currently lose_sum: 98.97925382852554
time_elpased: 1.421
start validation test
0.603298969072
0.622678159524
0.527837810024
0.571349003008
0.603431452776
64.921
batch start
#iterations: 120
currently lose_sum: 98.95206022262573
time_elpased: 1.429
batch start
#iterations: 121
currently lose_sum: 98.81026417016983
time_elpased: 1.398
batch start
#iterations: 122
currently lose_sum: 98.88436859846115
time_elpased: 1.452
batch start
#iterations: 123
currently lose_sum: 98.80063742399216
time_elpased: 1.431
batch start
#iterations: 124
currently lose_sum: 98.6099500656128
time_elpased: 1.398
batch start
#iterations: 125
currently lose_sum: 99.01760131120682
time_elpased: 1.401
batch start
#iterations: 126
currently lose_sum: 98.85287350416183
time_elpased: 1.39
batch start
#iterations: 127
currently lose_sum: 98.94261467456818
time_elpased: 1.427
batch start
#iterations: 128
currently lose_sum: 98.97622919082642
time_elpased: 1.426
batch start
#iterations: 129
currently lose_sum: 98.72744864225388
time_elpased: 1.433
batch start
#iterations: 130
currently lose_sum: 98.86085247993469
time_elpased: 1.413
batch start
#iterations: 131
currently lose_sum: 98.7293809056282
time_elpased: 1.46
batch start
#iterations: 132
currently lose_sum: 98.76076757907867
time_elpased: 1.417
batch start
#iterations: 133
currently lose_sum: 98.81572538614273
time_elpased: 1.411
batch start
#iterations: 134
currently lose_sum: 98.74732112884521
time_elpased: 1.423
batch start
#iterations: 135
currently lose_sum: 98.95002192258835
time_elpased: 1.412
batch start
#iterations: 136
currently lose_sum: 98.6301047205925
time_elpased: 1.444
batch start
#iterations: 137
currently lose_sum: 98.82317584753036
time_elpased: 1.472
batch start
#iterations: 138
currently lose_sum: 98.68906289339066
time_elpased: 1.425
batch start
#iterations: 139
currently lose_sum: 98.94798320531845
time_elpased: 1.411
start validation test
0.604381443299
0.621287716797
0.538231964598
0.576785221947
0.604497578912
64.775
batch start
#iterations: 140
currently lose_sum: 98.8887386918068
time_elpased: 1.431
batch start
#iterations: 141
currently lose_sum: 98.92746526002884
time_elpased: 1.414
batch start
#iterations: 142
currently lose_sum: 98.94213140010834
time_elpased: 1.415
batch start
#iterations: 143
currently lose_sum: 98.91604512929916
time_elpased: 1.416
batch start
#iterations: 144
currently lose_sum: 98.69616866111755
time_elpased: 1.444
batch start
#iterations: 145
currently lose_sum: 98.87245351076126
time_elpased: 1.437
batch start
#iterations: 146
currently lose_sum: 98.64188194274902
time_elpased: 1.433
batch start
#iterations: 147
currently lose_sum: 98.8650353550911
time_elpased: 1.395
batch start
#iterations: 148
currently lose_sum: 99.02339327335358
time_elpased: 1.4
batch start
#iterations: 149
currently lose_sum: 98.74874597787857
time_elpased: 1.468
batch start
#iterations: 150
currently lose_sum: 98.7173382639885
time_elpased: 1.421
batch start
#iterations: 151
currently lose_sum: 98.80478471517563
time_elpased: 1.401
batch start
#iterations: 152
currently lose_sum: 98.98531377315521
time_elpased: 1.416
batch start
#iterations: 153
currently lose_sum: 98.85668134689331
time_elpased: 1.43
batch start
#iterations: 154
currently lose_sum: 98.9742928147316
time_elpased: 1.414
batch start
#iterations: 155
currently lose_sum: 98.71852272748947
time_elpased: 1.506
batch start
#iterations: 156
currently lose_sum: 98.73688971996307
time_elpased: 1.438
batch start
#iterations: 157
currently lose_sum: 98.94426316022873
time_elpased: 1.436
batch start
#iterations: 158
currently lose_sum: 98.97590053081512
time_elpased: 1.416
batch start
#iterations: 159
currently lose_sum: 99.0977571606636
time_elpased: 1.395
start validation test
0.599793814433
0.631621512333
0.482247607286
0.546918767507
0.60000018493
65.065
batch start
#iterations: 160
currently lose_sum: 98.87055760622025
time_elpased: 1.404
batch start
#iterations: 161
currently lose_sum: 98.75875133275986
time_elpased: 1.394
batch start
#iterations: 162
currently lose_sum: 98.64908224344254
time_elpased: 1.461
batch start
#iterations: 163
currently lose_sum: 98.87027323246002
time_elpased: 1.417
batch start
#iterations: 164
currently lose_sum: 98.73400813341141
time_elpased: 1.473
batch start
#iterations: 165
currently lose_sum: 98.79669362306595
time_elpased: 1.41
batch start
#iterations: 166
currently lose_sum: 98.69709008932114
time_elpased: 1.419
batch start
#iterations: 167
currently lose_sum: 99.07828617095947
time_elpased: 1.482
batch start
#iterations: 168
currently lose_sum: 98.58151364326477
time_elpased: 1.415
batch start
#iterations: 169
currently lose_sum: 98.75020498037338
time_elpased: 1.429
batch start
#iterations: 170
currently lose_sum: 98.7205793261528
time_elpased: 1.396
batch start
#iterations: 171
currently lose_sum: 98.71110945940018
time_elpased: 1.406
batch start
#iterations: 172
currently lose_sum: 98.88188421726227
time_elpased: 1.399
batch start
#iterations: 173
currently lose_sum: 98.72109615802765
time_elpased: 1.497
batch start
#iterations: 174
currently lose_sum: 98.8364006280899
time_elpased: 1.426
batch start
#iterations: 175
currently lose_sum: 98.51799261569977
time_elpased: 1.409
batch start
#iterations: 176
currently lose_sum: 98.57919597625732
time_elpased: 1.382
batch start
#iterations: 177
currently lose_sum: 98.58890926837921
time_elpased: 1.455
batch start
#iterations: 178
currently lose_sum: 98.643075466156
time_elpased: 1.404
batch start
#iterations: 179
currently lose_sum: 98.78940403461456
time_elpased: 1.408
start validation test
0.601907216495
0.616173386157
0.544200885047
0.577955079513
0.60200852886
64.852
batch start
#iterations: 180
currently lose_sum: 98.60611772537231
time_elpased: 1.401
batch start
#iterations: 181
currently lose_sum: 99.00655710697174
time_elpased: 1.43
batch start
#iterations: 182
currently lose_sum: 98.86114299297333
time_elpased: 1.427
batch start
#iterations: 183
currently lose_sum: 98.78726202249527
time_elpased: 1.398
batch start
#iterations: 184
currently lose_sum: 98.73065447807312
time_elpased: 1.404
batch start
#iterations: 185
currently lose_sum: 98.7930548787117
time_elpased: 1.447
batch start
#iterations: 186
currently lose_sum: 98.77972483634949
time_elpased: 1.403
batch start
#iterations: 187
currently lose_sum: 98.52274698019028
time_elpased: 1.431
batch start
#iterations: 188
currently lose_sum: 98.8693978190422
time_elpased: 1.409
batch start
#iterations: 189
currently lose_sum: 98.64425617456436
time_elpased: 1.398
batch start
#iterations: 190
currently lose_sum: 98.8832311630249
time_elpased: 1.403
batch start
#iterations: 191
currently lose_sum: 98.801722407341
time_elpased: 1.4
batch start
#iterations: 192
currently lose_sum: 98.58277851343155
time_elpased: 1.405
batch start
#iterations: 193
currently lose_sum: 98.80359011888504
time_elpased: 1.439
batch start
#iterations: 194
currently lose_sum: 98.78682488203049
time_elpased: 1.437
batch start
#iterations: 195
currently lose_sum: 98.80872225761414
time_elpased: 1.453
batch start
#iterations: 196
currently lose_sum: 99.00817906856537
time_elpased: 1.418
batch start
#iterations: 197
currently lose_sum: 98.58872330188751
time_elpased: 1.471
batch start
#iterations: 198
currently lose_sum: 98.81230998039246
time_elpased: 1.387
batch start
#iterations: 199
currently lose_sum: 98.60069441795349
time_elpased: 1.431
start validation test
0.603659793814
0.635961383749
0.488113615313
0.55231441048
0.603862652953
64.877
batch start
#iterations: 200
currently lose_sum: 98.66786605119705
time_elpased: 1.412
batch start
#iterations: 201
currently lose_sum: 98.5929302573204
time_elpased: 1.478
batch start
#iterations: 202
currently lose_sum: 98.62739688158035
time_elpased: 1.41
batch start
#iterations: 203
currently lose_sum: 98.71548664569855
time_elpased: 1.419
batch start
#iterations: 204
currently lose_sum: 98.66130709648132
time_elpased: 1.401
batch start
#iterations: 205
currently lose_sum: 98.60872089862823
time_elpased: 1.444
batch start
#iterations: 206
currently lose_sum: 98.62288171052933
time_elpased: 1.392
batch start
#iterations: 207
currently lose_sum: 98.65742874145508
time_elpased: 1.4
batch start
#iterations: 208
currently lose_sum: 98.5776082277298
time_elpased: 1.427
batch start
#iterations: 209
currently lose_sum: 98.50774651765823
time_elpased: 1.434
batch start
#iterations: 210
currently lose_sum: 98.52114045619965
time_elpased: 1.442
batch start
#iterations: 211
currently lose_sum: 98.70077902078629
time_elpased: 1.42
batch start
#iterations: 212
currently lose_sum: 98.57911789417267
time_elpased: 1.415
batch start
#iterations: 213
currently lose_sum: 98.88432270288467
time_elpased: 1.437
batch start
#iterations: 214
currently lose_sum: 98.83515650033951
time_elpased: 1.429
batch start
#iterations: 215
currently lose_sum: 98.7877134680748
time_elpased: 1.44
batch start
#iterations: 216
currently lose_sum: 98.42258858680725
time_elpased: 1.397
batch start
#iterations: 217
currently lose_sum: 98.75760573148727
time_elpased: 1.436
batch start
#iterations: 218
currently lose_sum: 98.62184756994247
time_elpased: 1.394
batch start
#iterations: 219
currently lose_sum: 98.8952254652977
time_elpased: 1.475
start validation test
0.59675257732
0.643571861734
0.436863229392
0.520443817814
0.597033287731
64.935
batch start
#iterations: 220
currently lose_sum: 98.68951004743576
time_elpased: 1.411
batch start
#iterations: 221
currently lose_sum: 98.54115581512451
time_elpased: 1.461
batch start
#iterations: 222
currently lose_sum: 98.61950969696045
time_elpased: 1.406
batch start
#iterations: 223
currently lose_sum: 98.40295153856277
time_elpased: 1.452
batch start
#iterations: 224
currently lose_sum: 98.53531014919281
time_elpased: 1.437
batch start
#iterations: 225
currently lose_sum: 98.75313276052475
time_elpased: 1.434
batch start
#iterations: 226
currently lose_sum: 98.65523493289948
time_elpased: 1.448
batch start
#iterations: 227
currently lose_sum: 98.8937760591507
time_elpased: 1.401
batch start
#iterations: 228
currently lose_sum: 98.5766698718071
time_elpased: 1.389
batch start
#iterations: 229
currently lose_sum: 98.72553664445877
time_elpased: 1.419
batch start
#iterations: 230
currently lose_sum: 98.8712392449379
time_elpased: 1.477
batch start
#iterations: 231
currently lose_sum: 98.69005292654037
time_elpased: 1.509
batch start
#iterations: 232
currently lose_sum: 98.61923170089722
time_elpased: 1.437
batch start
#iterations: 233
currently lose_sum: 98.81712889671326
time_elpased: 1.405
batch start
#iterations: 234
currently lose_sum: 98.5638570189476
time_elpased: 1.377
batch start
#iterations: 235
currently lose_sum: 98.79595786333084
time_elpased: 1.434
batch start
#iterations: 236
currently lose_sum: 98.79339611530304
time_elpased: 1.402
batch start
#iterations: 237
currently lose_sum: 98.68278062343597
time_elpased: 1.407
batch start
#iterations: 238
currently lose_sum: 98.6396187543869
time_elpased: 1.432
batch start
#iterations: 239
currently lose_sum: 98.84430003166199
time_elpased: 1.428
start validation test
0.602010309278
0.634610196925
0.484202943295
0.549296596813
0.60221713828
64.969
batch start
#iterations: 240
currently lose_sum: 98.48424655199051
time_elpased: 1.423
batch start
#iterations: 241
currently lose_sum: 98.70671153068542
time_elpased: 1.472
batch start
#iterations: 242
currently lose_sum: 98.70629632472992
time_elpased: 1.432
batch start
#iterations: 243
currently lose_sum: 98.6793692111969
time_elpased: 1.418
batch start
#iterations: 244
currently lose_sum: 98.55789291858673
time_elpased: 1.413
batch start
#iterations: 245
currently lose_sum: 98.86081576347351
time_elpased: 1.453
batch start
#iterations: 246
currently lose_sum: 98.76172721385956
time_elpased: 1.432
batch start
#iterations: 247
currently lose_sum: 98.66126239299774
time_elpased: 1.424
batch start
#iterations: 248
currently lose_sum: 98.50843411684036
time_elpased: 1.411
batch start
#iterations: 249
currently lose_sum: 98.60771608352661
time_elpased: 1.41
batch start
#iterations: 250
currently lose_sum: 98.67127442359924
time_elpased: 1.398
batch start
#iterations: 251
currently lose_sum: 98.5507185459137
time_elpased: 1.473
batch start
#iterations: 252
currently lose_sum: 98.63566780090332
time_elpased: 1.403
batch start
#iterations: 253
currently lose_sum: 98.70260673761368
time_elpased: 1.459
batch start
#iterations: 254
currently lose_sum: 98.82656419277191
time_elpased: 1.437
batch start
#iterations: 255
currently lose_sum: 98.5839838385582
time_elpased: 1.393
batch start
#iterations: 256
currently lose_sum: 98.47430908679962
time_elpased: 1.413
batch start
#iterations: 257
currently lose_sum: 98.32463812828064
time_elpased: 1.399
batch start
#iterations: 258
currently lose_sum: 98.45223605632782
time_elpased: 1.393
batch start
#iterations: 259
currently lose_sum: 98.63324970006943
time_elpased: 1.505
start validation test
0.606855670103
0.632311977716
0.513944633117
0.567016747091
0.607018789759
64.747
batch start
#iterations: 260
currently lose_sum: 98.6718618273735
time_elpased: 1.441
batch start
#iterations: 261
currently lose_sum: 98.53525257110596
time_elpased: 1.428
batch start
#iterations: 262
currently lose_sum: 98.35851114988327
time_elpased: 1.397
batch start
#iterations: 263
currently lose_sum: 98.51283079385757
time_elpased: 1.423
batch start
#iterations: 264
currently lose_sum: 98.57687026262283
time_elpased: 1.425
batch start
#iterations: 265
currently lose_sum: 98.60347253084183
time_elpased: 1.425
batch start
#iterations: 266
currently lose_sum: 98.8175106048584
time_elpased: 1.386
batch start
#iterations: 267
currently lose_sum: 98.60596114397049
time_elpased: 1.421
batch start
#iterations: 268
currently lose_sum: 98.64144587516785
time_elpased: 1.408
batch start
#iterations: 269
currently lose_sum: 98.70372474193573
time_elpased: 1.453
batch start
#iterations: 270
currently lose_sum: 98.37158989906311
time_elpased: 1.391
batch start
#iterations: 271
currently lose_sum: 98.55772644281387
time_elpased: 1.4
batch start
#iterations: 272
currently lose_sum: 98.87837570905685
time_elpased: 1.408
batch start
#iterations: 273
currently lose_sum: 98.66505789756775
time_elpased: 1.42
batch start
#iterations: 274
currently lose_sum: 98.53922611474991
time_elpased: 1.418
batch start
#iterations: 275
currently lose_sum: 98.61268550157547
time_elpased: 1.457
batch start
#iterations: 276
currently lose_sum: 98.74203389883041
time_elpased: 1.398
batch start
#iterations: 277
currently lose_sum: 98.6029988527298
time_elpased: 1.409
batch start
#iterations: 278
currently lose_sum: 98.72241121530533
time_elpased: 1.396
batch start
#iterations: 279
currently lose_sum: 98.62384909391403
time_elpased: 1.45
start validation test
0.609020618557
0.628899637243
0.535247504374
0.578306554734
0.609150138637
64.697
batch start
#iterations: 280
currently lose_sum: 98.53296631574631
time_elpased: 1.413
batch start
#iterations: 281
currently lose_sum: 98.45407623052597
time_elpased: 1.427
batch start
#iterations: 282
currently lose_sum: 98.62947630882263
time_elpased: 1.439
batch start
#iterations: 283
currently lose_sum: 98.52537590265274
time_elpased: 1.442
batch start
#iterations: 284
currently lose_sum: 98.56006187200546
time_elpased: 1.399
batch start
#iterations: 285
currently lose_sum: 98.71300983428955
time_elpased: 1.397
batch start
#iterations: 286
currently lose_sum: 98.50137221813202
time_elpased: 1.413
batch start
#iterations: 287
currently lose_sum: 98.5115555524826
time_elpased: 1.439
batch start
#iterations: 288
currently lose_sum: 98.36109364032745
time_elpased: 1.391
batch start
#iterations: 289
currently lose_sum: 98.62513518333435
time_elpased: 1.428
batch start
#iterations: 290
currently lose_sum: 98.77972865104675
time_elpased: 1.463
batch start
#iterations: 291
currently lose_sum: 98.39966094493866
time_elpased: 1.433
batch start
#iterations: 292
currently lose_sum: 98.39908927679062
time_elpased: 1.399
batch start
#iterations: 293
currently lose_sum: 98.76046848297119
time_elpased: 1.48
batch start
#iterations: 294
currently lose_sum: 98.44415944814682
time_elpased: 1.396
batch start
#iterations: 295
currently lose_sum: 98.6789163351059
time_elpased: 1.42
batch start
#iterations: 296
currently lose_sum: 98.61695796251297
time_elpased: 1.409
batch start
#iterations: 297
currently lose_sum: 98.48486870527267
time_elpased: 1.444
batch start
#iterations: 298
currently lose_sum: 98.64331102371216
time_elpased: 1.395
batch start
#iterations: 299
currently lose_sum: 98.71501177549362
time_elpased: 1.427
start validation test
0.607216494845
0.634994206257
0.507564062982
0.564172958133
0.60739145006
64.863
batch start
#iterations: 300
currently lose_sum: 98.44639492034912
time_elpased: 1.389
batch start
#iterations: 301
currently lose_sum: 98.4141001701355
time_elpased: 1.411
batch start
#iterations: 302
currently lose_sum: 98.5905082821846
time_elpased: 1.396
batch start
#iterations: 303
currently lose_sum: 98.7330237030983
time_elpased: 1.456
batch start
#iterations: 304
currently lose_sum: 98.53348153829575
time_elpased: 1.412
batch start
#iterations: 305
currently lose_sum: 98.65626043081284
time_elpased: 1.45
batch start
#iterations: 306
currently lose_sum: 98.49691593647003
time_elpased: 1.427
batch start
#iterations: 307
currently lose_sum: 98.81358736753464
time_elpased: 1.428
batch start
#iterations: 308
currently lose_sum: 98.35873848199844
time_elpased: 1.405
batch start
#iterations: 309
currently lose_sum: 98.58222967386246
time_elpased: 1.444
batch start
#iterations: 310
currently lose_sum: 98.7814182639122
time_elpased: 1.42
batch start
#iterations: 311
currently lose_sum: 98.36809766292572
time_elpased: 1.486
batch start
#iterations: 312
currently lose_sum: 98.60627019405365
time_elpased: 1.409
batch start
#iterations: 313
currently lose_sum: 98.44706755876541
time_elpased: 1.42
batch start
#iterations: 314
currently lose_sum: 98.7817976474762
time_elpased: 1.398
batch start
#iterations: 315
currently lose_sum: 98.5766504406929
time_elpased: 1.429
batch start
#iterations: 316
currently lose_sum: 98.63150811195374
time_elpased: 1.418
batch start
#iterations: 317
currently lose_sum: 98.64654344320297
time_elpased: 1.42
batch start
#iterations: 318
currently lose_sum: 98.67134994268417
time_elpased: 1.457
batch start
#iterations: 319
currently lose_sum: 98.78267079591751
time_elpased: 1.417
start validation test
0.606443298969
0.632207264415
0.512298034373
0.565971235291
0.606608585502
64.838
batch start
#iterations: 320
currently lose_sum: 98.56091451644897
time_elpased: 1.413
batch start
#iterations: 321
currently lose_sum: 98.57327204942703
time_elpased: 1.419
batch start
#iterations: 322
currently lose_sum: 98.77036112546921
time_elpased: 1.397
batch start
#iterations: 323
currently lose_sum: 98.51318800449371
time_elpased: 1.549
batch start
#iterations: 324
currently lose_sum: 98.94942200183868
time_elpased: 1.391
batch start
#iterations: 325
currently lose_sum: 98.68785989284515
time_elpased: 1.419
batch start
#iterations: 326
currently lose_sum: 98.65295398235321
time_elpased: 1.406
batch start
#iterations: 327
currently lose_sum: 98.64660322666168
time_elpased: 1.431
batch start
#iterations: 328
currently lose_sum: 98.70878541469574
time_elpased: 1.403
batch start
#iterations: 329
currently lose_sum: 98.5442008972168
time_elpased: 1.434
batch start
#iterations: 330
currently lose_sum: 98.77650260925293
time_elpased: 1.417
batch start
#iterations: 331
currently lose_sum: 98.53943520784378
time_elpased: 1.397
batch start
#iterations: 332
currently lose_sum: 98.34046310186386
time_elpased: 1.394
batch start
#iterations: 333
currently lose_sum: 98.41126155853271
time_elpased: 1.441
batch start
#iterations: 334
currently lose_sum: 98.52385729551315
time_elpased: 1.451
batch start
#iterations: 335
currently lose_sum: 98.5537468791008
time_elpased: 1.425
batch start
#iterations: 336
currently lose_sum: 98.51524478197098
time_elpased: 1.429
batch start
#iterations: 337
currently lose_sum: 98.48750644922256
time_elpased: 1.404
batch start
#iterations: 338
currently lose_sum: 98.46756076812744
time_elpased: 1.413
batch start
#iterations: 339
currently lose_sum: 98.73628675937653
time_elpased: 1.433
start validation test
0.606804123711
0.62522479319
0.536688278275
0.577583342563
0.606927222893
64.631
batch start
#iterations: 340
currently lose_sum: 98.69216829538345
time_elpased: 1.427
batch start
#iterations: 341
currently lose_sum: 99.02495229244232
time_elpased: 1.412
batch start
#iterations: 342
currently lose_sum: 98.49248576164246
time_elpased: 1.416
batch start
#iterations: 343
currently lose_sum: 98.66272151470184
time_elpased: 1.436
batch start
#iterations: 344
currently lose_sum: 98.67291671037674
time_elpased: 1.407
batch start
#iterations: 345
currently lose_sum: 98.6902768611908
time_elpased: 1.403
batch start
#iterations: 346
currently lose_sum: 98.47386288642883
time_elpased: 1.406
batch start
#iterations: 347
currently lose_sum: 98.70182001590729
time_elpased: 1.474
batch start
#iterations: 348
currently lose_sum: 98.66492176055908
time_elpased: 1.417
batch start
#iterations: 349
currently lose_sum: 98.53802919387817
time_elpased: 1.415
batch start
#iterations: 350
currently lose_sum: 98.68824523687363
time_elpased: 1.412
batch start
#iterations: 351
currently lose_sum: 98.84917879104614
time_elpased: 1.47
batch start
#iterations: 352
currently lose_sum: 98.62697380781174
time_elpased: 1.403
batch start
#iterations: 353
currently lose_sum: 98.72057944536209
time_elpased: 1.415
batch start
#iterations: 354
currently lose_sum: 98.64936101436615
time_elpased: 1.405
batch start
#iterations: 355
currently lose_sum: 98.72427868843079
time_elpased: 1.43
batch start
#iterations: 356
currently lose_sum: 98.5064138174057
time_elpased: 1.432
batch start
#iterations: 357
currently lose_sum: 98.59231621026993
time_elpased: 1.435
batch start
#iterations: 358
currently lose_sum: 98.63334208726883
time_elpased: 1.395
batch start
#iterations: 359
currently lose_sum: 98.80994415283203
time_elpased: 1.491
start validation test
0.606237113402
0.630298470028
0.517237830606
0.568198519021
0.60639336537
64.796
batch start
#iterations: 360
currently lose_sum: 98.82383227348328
time_elpased: 1.407
batch start
#iterations: 361
currently lose_sum: 98.81252408027649
time_elpased: 1.434
batch start
#iterations: 362
currently lose_sum: 98.62225365638733
time_elpased: 1.419
batch start
#iterations: 363
currently lose_sum: 98.42179429531097
time_elpased: 1.435
batch start
#iterations: 364
currently lose_sum: 98.74234068393707
time_elpased: 1.417
batch start
#iterations: 365
currently lose_sum: 98.66317933797836
time_elpased: 1.416
batch start
#iterations: 366
currently lose_sum: 98.80679422616959
time_elpased: 1.414
batch start
#iterations: 367
currently lose_sum: 98.32435429096222
time_elpased: 1.426
batch start
#iterations: 368
currently lose_sum: 98.57064509391785
time_elpased: 1.437
batch start
#iterations: 369
currently lose_sum: 98.46086305379868
time_elpased: 1.44
batch start
#iterations: 370
currently lose_sum: 98.72509580850601
time_elpased: 1.428
batch start
#iterations: 371
currently lose_sum: 98.47868251800537
time_elpased: 1.454
batch start
#iterations: 372
currently lose_sum: 98.62168252468109
time_elpased: 1.41
batch start
#iterations: 373
currently lose_sum: 98.63135528564453
time_elpased: 1.423
batch start
#iterations: 374
currently lose_sum: 98.52608722448349
time_elpased: 1.398
batch start
#iterations: 375
currently lose_sum: 98.7024621963501
time_elpased: 1.484
batch start
#iterations: 376
currently lose_sum: 98.52956259250641
time_elpased: 1.41
batch start
#iterations: 377
currently lose_sum: 98.43271869421005
time_elpased: 1.406
batch start
#iterations: 378
currently lose_sum: 98.76905035972595
time_elpased: 1.428
batch start
#iterations: 379
currently lose_sum: 98.35848504304886
time_elpased: 1.425
start validation test
0.607577319588
0.6338081913
0.51281259648
0.566926446328
0.607743693675
64.646
batch start
#iterations: 380
currently lose_sum: 98.70659500360489
time_elpased: 1.445
batch start
#iterations: 381
currently lose_sum: 98.4834856390953
time_elpased: 1.469
batch start
#iterations: 382
currently lose_sum: 98.61096215248108
time_elpased: 1.5
batch start
#iterations: 383
currently lose_sum: 98.47663426399231
time_elpased: 1.411
batch start
#iterations: 384
currently lose_sum: 98.47451108694077
time_elpased: 1.385
batch start
#iterations: 385
currently lose_sum: 98.68766885995865
time_elpased: 1.43
batch start
#iterations: 386
currently lose_sum: 98.59114521741867
time_elpased: 1.389
batch start
#iterations: 387
currently lose_sum: 98.30938178300858
time_elpased: 1.479
batch start
#iterations: 388
currently lose_sum: 98.5486918091774
time_elpased: 1.403
batch start
#iterations: 389
currently lose_sum: 98.59774339199066
time_elpased: 1.468
batch start
#iterations: 390
currently lose_sum: 98.72197246551514
time_elpased: 1.394
batch start
#iterations: 391
currently lose_sum: 98.44820594787598
time_elpased: 1.481
batch start
#iterations: 392
currently lose_sum: 98.33059000968933
time_elpased: 1.382
batch start
#iterations: 393
currently lose_sum: 98.49678009748459
time_elpased: 1.433
batch start
#iterations: 394
currently lose_sum: 98.4765402674675
time_elpased: 1.41
batch start
#iterations: 395
currently lose_sum: 98.73738819360733
time_elpased: 1.438
batch start
#iterations: 396
currently lose_sum: 98.4031628370285
time_elpased: 1.393
batch start
#iterations: 397
currently lose_sum: 98.4694293141365
time_elpased: 1.447
batch start
#iterations: 398
currently lose_sum: 98.47214084863663
time_elpased: 1.412
batch start
#iterations: 399
currently lose_sum: 98.71207290887833
time_elpased: 1.448
start validation test
0.607783505155
0.641248994908
0.492435937018
0.557075499156
0.607986015602
64.672
acc: 0.603
pre: 0.620
rec: 0.534
F1: 0.574
auc: 0.636
