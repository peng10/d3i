start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.59586137533188
time_elpased: 1.457
batch start
#iterations: 1
currently lose_sum: 100.42865109443665
time_elpased: 1.426
batch start
#iterations: 2
currently lose_sum: 100.31747990846634
time_elpased: 1.455
batch start
#iterations: 3
currently lose_sum: 100.28671860694885
time_elpased: 1.43
batch start
#iterations: 4
currently lose_sum: 100.09780788421631
time_elpased: 1.476
batch start
#iterations: 5
currently lose_sum: 100.09735637903214
time_elpased: 1.417
batch start
#iterations: 6
currently lose_sum: 99.88656562566757
time_elpased: 1.407
batch start
#iterations: 7
currently lose_sum: 99.96729147434235
time_elpased: 1.418
batch start
#iterations: 8
currently lose_sum: 99.90460640192032
time_elpased: 1.423
batch start
#iterations: 9
currently lose_sum: 99.80664956569672
time_elpased: 1.425
batch start
#iterations: 10
currently lose_sum: 99.75352716445923
time_elpased: 1.453
batch start
#iterations: 11
currently lose_sum: 100.0003582239151
time_elpased: 1.446
batch start
#iterations: 12
currently lose_sum: 99.82621705532074
time_elpased: 1.421
batch start
#iterations: 13
currently lose_sum: 99.89985489845276
time_elpased: 1.455
batch start
#iterations: 14
currently lose_sum: 99.76220351457596
time_elpased: 1.441
batch start
#iterations: 15
currently lose_sum: 99.7303591966629
time_elpased: 1.431
batch start
#iterations: 16
currently lose_sum: 99.63367986679077
time_elpased: 1.424
batch start
#iterations: 17
currently lose_sum: 99.71685290336609
time_elpased: 1.541
batch start
#iterations: 18
currently lose_sum: 99.68191713094711
time_elpased: 1.446
batch start
#iterations: 19
currently lose_sum: 99.45475554466248
time_elpased: 1.411
start validation test
0.581237113402
0.612198901254
0.447257383966
0.51688867745
0.581472335482
65.691
batch start
#iterations: 20
currently lose_sum: 99.5917540192604
time_elpased: 1.433
batch start
#iterations: 21
currently lose_sum: 99.5757902264595
time_elpased: 1.47
batch start
#iterations: 22
currently lose_sum: 99.69855779409409
time_elpased: 1.399
batch start
#iterations: 23
currently lose_sum: 99.60102641582489
time_elpased: 1.42
batch start
#iterations: 24
currently lose_sum: 99.40918761491776
time_elpased: 1.448
batch start
#iterations: 25
currently lose_sum: 99.49455082416534
time_elpased: 1.454
batch start
#iterations: 26
currently lose_sum: 99.49310404062271
time_elpased: 1.428
batch start
#iterations: 27
currently lose_sum: 99.40432906150818
time_elpased: 1.423
batch start
#iterations: 28
currently lose_sum: 99.23203879594803
time_elpased: 1.412
batch start
#iterations: 29
currently lose_sum: 99.36623191833496
time_elpased: 1.475
batch start
#iterations: 30
currently lose_sum: 99.3506686091423
time_elpased: 1.405
batch start
#iterations: 31
currently lose_sum: 99.29151219129562
time_elpased: 1.46
batch start
#iterations: 32
currently lose_sum: 99.32874315977097
time_elpased: 1.421
batch start
#iterations: 33
currently lose_sum: 99.22954624891281
time_elpased: 1.425
batch start
#iterations: 34
currently lose_sum: 99.2660922408104
time_elpased: 1.425
batch start
#iterations: 35
currently lose_sum: 99.2356538772583
time_elpased: 1.42
batch start
#iterations: 36
currently lose_sum: 99.31946516036987
time_elpased: 1.419
batch start
#iterations: 37
currently lose_sum: 99.25793433189392
time_elpased: 1.402
batch start
#iterations: 38
currently lose_sum: 99.26501733064651
time_elpased: 1.442
batch start
#iterations: 39
currently lose_sum: 99.25368285179138
time_elpased: 1.407
start validation test
0.586082474227
0.624281715043
0.43603993002
0.513451284537
0.586345897056
65.440
batch start
#iterations: 40
currently lose_sum: 99.20078259706497
time_elpased: 1.469
batch start
#iterations: 41
currently lose_sum: 99.2410517334938
time_elpased: 1.396
batch start
#iterations: 42
currently lose_sum: 99.13187450170517
time_elpased: 1.402
batch start
#iterations: 43
currently lose_sum: 99.13853299617767
time_elpased: 1.406
batch start
#iterations: 44
currently lose_sum: 99.1568568944931
time_elpased: 1.442
batch start
#iterations: 45
currently lose_sum: 99.29938942193985
time_elpased: 1.426
batch start
#iterations: 46
currently lose_sum: 98.90102934837341
time_elpased: 1.436
batch start
#iterations: 47
currently lose_sum: 99.14347064495087
time_elpased: 1.401
batch start
#iterations: 48
currently lose_sum: 99.22883903980255
time_elpased: 1.402
batch start
#iterations: 49
currently lose_sum: 98.9507509469986
time_elpased: 1.407
batch start
#iterations: 50
currently lose_sum: 99.02944922447205
time_elpased: 1.44
batch start
#iterations: 51
currently lose_sum: 99.0023757815361
time_elpased: 1.452
batch start
#iterations: 52
currently lose_sum: 99.1399297118187
time_elpased: 1.433
batch start
#iterations: 53
currently lose_sum: 99.08348870277405
time_elpased: 1.434
batch start
#iterations: 54
currently lose_sum: 99.06880646944046
time_elpased: 1.432
batch start
#iterations: 55
currently lose_sum: 99.04673463106155
time_elpased: 1.444
batch start
#iterations: 56
currently lose_sum: 99.04729443788528
time_elpased: 1.408
batch start
#iterations: 57
currently lose_sum: 99.16713684797287
time_elpased: 1.405
batch start
#iterations: 58
currently lose_sum: 99.0957590341568
time_elpased: 1.457
batch start
#iterations: 59
currently lose_sum: 99.05580687522888
time_elpased: 1.473
start validation test
0.586237113402
0.632155145449
0.415972007821
0.501768977717
0.586536040056
65.242
batch start
#iterations: 60
currently lose_sum: 98.88427287340164
time_elpased: 1.455
batch start
#iterations: 61
currently lose_sum: 99.11922764778137
time_elpased: 1.436
batch start
#iterations: 62
currently lose_sum: 98.94731312990189
time_elpased: 1.409
batch start
#iterations: 63
currently lose_sum: 99.22550565004349
time_elpased: 1.415
batch start
#iterations: 64
currently lose_sum: 99.02574217319489
time_elpased: 1.422
batch start
#iterations: 65
currently lose_sum: 98.97544503211975
time_elpased: 1.444
batch start
#iterations: 66
currently lose_sum: 98.81866604089737
time_elpased: 1.448
batch start
#iterations: 67
currently lose_sum: 98.8616868853569
time_elpased: 1.406
batch start
#iterations: 68
currently lose_sum: 99.16060042381287
time_elpased: 1.45
batch start
#iterations: 69
currently lose_sum: 98.89696300029755
time_elpased: 1.444
batch start
#iterations: 70
currently lose_sum: 98.94738066196442
time_elpased: 1.509
batch start
#iterations: 71
currently lose_sum: 99.03809297084808
time_elpased: 1.414
batch start
#iterations: 72
currently lose_sum: 98.949154317379
time_elpased: 1.431
batch start
#iterations: 73
currently lose_sum: 98.94194006919861
time_elpased: 1.455
batch start
#iterations: 74
currently lose_sum: 99.10712641477585
time_elpased: 1.445
batch start
#iterations: 75
currently lose_sum: 98.99784529209137
time_elpased: 1.457
batch start
#iterations: 76
currently lose_sum: 99.00662457942963
time_elpased: 1.449
batch start
#iterations: 77
currently lose_sum: 99.24244838953018
time_elpased: 1.437
batch start
#iterations: 78
currently lose_sum: 98.83850705623627
time_elpased: 1.478
batch start
#iterations: 79
currently lose_sum: 99.15597546100616
time_elpased: 1.412
start validation test
0.593298969072
0.628680095788
0.459298137285
0.530803996194
0.593534228201
64.976
batch start
#iterations: 80
currently lose_sum: 98.96753638982773
time_elpased: 1.48
batch start
#iterations: 81
currently lose_sum: 98.84304946660995
time_elpased: 1.497
batch start
#iterations: 82
currently lose_sum: 99.08463591337204
time_elpased: 1.415
batch start
#iterations: 83
currently lose_sum: 98.9723242521286
time_elpased: 1.422
batch start
#iterations: 84
currently lose_sum: 98.91047990322113
time_elpased: 1.418
batch start
#iterations: 85
currently lose_sum: 98.98781555891037
time_elpased: 1.45
batch start
#iterations: 86
currently lose_sum: 98.96821784973145
time_elpased: 1.423
batch start
#iterations: 87
currently lose_sum: 99.00993758440018
time_elpased: 1.428
batch start
#iterations: 88
currently lose_sum: 98.88627797365189
time_elpased: 1.414
batch start
#iterations: 89
currently lose_sum: 98.83788001537323
time_elpased: 1.417
batch start
#iterations: 90
currently lose_sum: 98.97853553295135
time_elpased: 1.403
batch start
#iterations: 91
currently lose_sum: 99.21420830488205
time_elpased: 1.471
batch start
#iterations: 92
currently lose_sum: 98.8426605463028
time_elpased: 1.458
batch start
#iterations: 93
currently lose_sum: 98.83315473794937
time_elpased: 1.421
batch start
#iterations: 94
currently lose_sum: 99.0310886502266
time_elpased: 1.425
batch start
#iterations: 95
currently lose_sum: 98.96969610452652
time_elpased: 1.426
batch start
#iterations: 96
currently lose_sum: 98.68826431035995
time_elpased: 1.436
batch start
#iterations: 97
currently lose_sum: 98.69748347997665
time_elpased: 1.446
batch start
#iterations: 98
currently lose_sum: 98.88703119754791
time_elpased: 1.414
batch start
#iterations: 99
currently lose_sum: 98.66627848148346
time_elpased: 1.419
start validation test
0.598041237113
0.637997986481
0.456519501904
0.532213557289
0.598289700348
64.883
batch start
#iterations: 100
currently lose_sum: 98.8346580862999
time_elpased: 1.464
batch start
#iterations: 101
currently lose_sum: 98.91060209274292
time_elpased: 1.428
batch start
#iterations: 102
currently lose_sum: 99.15191984176636
time_elpased: 1.418
batch start
#iterations: 103
currently lose_sum: 98.70429241657257
time_elpased: 1.411
batch start
#iterations: 104
currently lose_sum: 98.98221683502197
time_elpased: 1.41
batch start
#iterations: 105
currently lose_sum: 98.87606263160706
time_elpased: 1.407
batch start
#iterations: 106
currently lose_sum: 98.7220025062561
time_elpased: 1.42
batch start
#iterations: 107
currently lose_sum: 98.9059020280838
time_elpased: 1.459
batch start
#iterations: 108
currently lose_sum: 98.99630081653595
time_elpased: 1.406
batch start
#iterations: 109
currently lose_sum: 98.86715281009674
time_elpased: 1.422
batch start
#iterations: 110
currently lose_sum: 98.79001933336258
time_elpased: 1.447
batch start
#iterations: 111
currently lose_sum: 98.63844203948975
time_elpased: 1.441
batch start
#iterations: 112
currently lose_sum: 98.884212911129
time_elpased: 1.418
batch start
#iterations: 113
currently lose_sum: 98.7795090675354
time_elpased: 1.487
batch start
#iterations: 114
currently lose_sum: 98.90314948558807
time_elpased: 1.431
batch start
#iterations: 115
currently lose_sum: 98.92544049024582
time_elpased: 1.458
batch start
#iterations: 116
currently lose_sum: 98.90018039941788
time_elpased: 1.419
batch start
#iterations: 117
currently lose_sum: 98.394238114357
time_elpased: 1.415
batch start
#iterations: 118
currently lose_sum: 98.97705572843552
time_elpased: 1.404
batch start
#iterations: 119
currently lose_sum: 98.98833179473877
time_elpased: 1.423
start validation test
0.599175257732
0.635639412998
0.468045693115
0.539118065434
0.599405475908
64.874
batch start
#iterations: 120
currently lose_sum: 98.91157621145248
time_elpased: 1.424
batch start
#iterations: 121
currently lose_sum: 98.58315175771713
time_elpased: 1.448
batch start
#iterations: 122
currently lose_sum: 99.07438892126083
time_elpased: 1.492
batch start
#iterations: 123
currently lose_sum: 98.91685104370117
time_elpased: 1.422
batch start
#iterations: 124
currently lose_sum: 98.99392849206924
time_elpased: 1.428
batch start
#iterations: 125
currently lose_sum: 99.04813081026077
time_elpased: 1.436
batch start
#iterations: 126
currently lose_sum: 98.72917771339417
time_elpased: 1.423
batch start
#iterations: 127
currently lose_sum: 99.02793943881989
time_elpased: 1.42
batch start
#iterations: 128
currently lose_sum: 98.89215362071991
time_elpased: 1.427
batch start
#iterations: 129
currently lose_sum: 99.0389347076416
time_elpased: 1.452
batch start
#iterations: 130
currently lose_sum: 98.71989387273788
time_elpased: 1.421
batch start
#iterations: 131
currently lose_sum: 98.91528987884521
time_elpased: 1.437
batch start
#iterations: 132
currently lose_sum: 99.02969431877136
time_elpased: 1.435
batch start
#iterations: 133
currently lose_sum: 98.84379035234451
time_elpased: 1.431
batch start
#iterations: 134
currently lose_sum: 98.89841717481613
time_elpased: 1.425
batch start
#iterations: 135
currently lose_sum: 98.62311601638794
time_elpased: 1.423
batch start
#iterations: 136
currently lose_sum: 98.69510090351105
time_elpased: 1.43
batch start
#iterations: 137
currently lose_sum: 98.8859401345253
time_elpased: 1.432
batch start
#iterations: 138
currently lose_sum: 98.82007133960724
time_elpased: 1.434
batch start
#iterations: 139
currently lose_sum: 98.93224906921387
time_elpased: 1.42
start validation test
0.603969072165
0.625277161863
0.522383451683
0.569217830109
0.604112308305
64.652
batch start
#iterations: 140
currently lose_sum: 98.81084161996841
time_elpased: 1.438
batch start
#iterations: 141
currently lose_sum: 98.9518895149231
time_elpased: 1.429
batch start
#iterations: 142
currently lose_sum: 98.72730243206024
time_elpased: 1.409
batch start
#iterations: 143
currently lose_sum: 98.97374552488327
time_elpased: 1.437
batch start
#iterations: 144
currently lose_sum: 98.70676118135452
time_elpased: 1.436
batch start
#iterations: 145
currently lose_sum: 98.76016426086426
time_elpased: 1.419
batch start
#iterations: 146
currently lose_sum: 98.64376610517502
time_elpased: 1.402
batch start
#iterations: 147
currently lose_sum: 98.60135352611542
time_elpased: 1.444
batch start
#iterations: 148
currently lose_sum: 98.64515143632889
time_elpased: 1.437
batch start
#iterations: 149
currently lose_sum: 98.86839628219604
time_elpased: 1.416
batch start
#iterations: 150
currently lose_sum: 98.72573733329773
time_elpased: 1.493
batch start
#iterations: 151
currently lose_sum: 98.73310613632202
time_elpased: 1.44
batch start
#iterations: 152
currently lose_sum: 98.86198848485947
time_elpased: 1.432
batch start
#iterations: 153
currently lose_sum: 98.95639061927795
time_elpased: 1.42
batch start
#iterations: 154
currently lose_sum: 98.95427316427231
time_elpased: 1.463
batch start
#iterations: 155
currently lose_sum: 98.65835052728653
time_elpased: 1.45
batch start
#iterations: 156
currently lose_sum: 98.62267327308655
time_elpased: 1.416
batch start
#iterations: 157
currently lose_sum: 98.60772091150284
time_elpased: 1.44
batch start
#iterations: 158
currently lose_sum: 98.67447364330292
time_elpased: 1.469
batch start
#iterations: 159
currently lose_sum: 98.63382685184479
time_elpased: 1.442
start validation test
0.60587628866
0.623171166885
0.539158176392
0.578128448466
0.605993422596
64.622
batch start
#iterations: 160
currently lose_sum: 98.90721452236176
time_elpased: 1.481
batch start
#iterations: 161
currently lose_sum: 98.77239668369293
time_elpased: 1.465
batch start
#iterations: 162
currently lose_sum: 98.81802171468735
time_elpased: 1.423
batch start
#iterations: 163
currently lose_sum: 98.88785374164581
time_elpased: 1.421
batch start
#iterations: 164
currently lose_sum: 98.57735669612885
time_elpased: 1.441
batch start
#iterations: 165
currently lose_sum: 98.80787801742554
time_elpased: 1.478
batch start
#iterations: 166
currently lose_sum: 98.65634250640869
time_elpased: 1.433
batch start
#iterations: 167
currently lose_sum: 98.95438289642334
time_elpased: 1.412
batch start
#iterations: 168
currently lose_sum: 98.61646902561188
time_elpased: 1.443
batch start
#iterations: 169
currently lose_sum: 98.86950898170471
time_elpased: 1.451
batch start
#iterations: 170
currently lose_sum: 98.60554385185242
time_elpased: 1.44
batch start
#iterations: 171
currently lose_sum: 99.00517839193344
time_elpased: 1.448
batch start
#iterations: 172
currently lose_sum: 98.7869884967804
time_elpased: 1.425
batch start
#iterations: 173
currently lose_sum: 98.84312862157822
time_elpased: 1.599
batch start
#iterations: 174
currently lose_sum: 98.5935195684433
time_elpased: 1.405
batch start
#iterations: 175
currently lose_sum: 98.49941778182983
time_elpased: 1.406
batch start
#iterations: 176
currently lose_sum: 98.64154344797134
time_elpased: 1.437
batch start
#iterations: 177
currently lose_sum: 98.68630123138428
time_elpased: 1.431
batch start
#iterations: 178
currently lose_sum: 98.98430645465851
time_elpased: 1.399
batch start
#iterations: 179
currently lose_sum: 98.62148934602737
time_elpased: 1.443
start validation test
0.602164948454
0.615509071998
0.548111557065
0.579858464888
0.602259847519
64.579
batch start
#iterations: 180
currently lose_sum: 98.60279643535614
time_elpased: 1.466
batch start
#iterations: 181
currently lose_sum: 98.76485043764114
time_elpased: 1.431
batch start
#iterations: 182
currently lose_sum: 98.82713669538498
time_elpased: 1.437
batch start
#iterations: 183
currently lose_sum: 98.5295330286026
time_elpased: 1.484
batch start
#iterations: 184
currently lose_sum: 98.90251189470291
time_elpased: 1.414
batch start
#iterations: 185
currently lose_sum: 98.71454697847366
time_elpased: 1.471
batch start
#iterations: 186
currently lose_sum: 98.6504390835762
time_elpased: 1.416
batch start
#iterations: 187
currently lose_sum: 98.65912848711014
time_elpased: 1.439
batch start
#iterations: 188
currently lose_sum: 98.86371791362762
time_elpased: 1.457
batch start
#iterations: 189
currently lose_sum: 98.78803968429565
time_elpased: 1.396
batch start
#iterations: 190
currently lose_sum: 98.76690024137497
time_elpased: 1.436
batch start
#iterations: 191
currently lose_sum: 98.65090268850327
time_elpased: 1.442
batch start
#iterations: 192
currently lose_sum: 98.62698775529861
time_elpased: 1.47
batch start
#iterations: 193
currently lose_sum: 98.84072685241699
time_elpased: 1.412
batch start
#iterations: 194
currently lose_sum: 98.53619068861008
time_elpased: 1.438
batch start
#iterations: 195
currently lose_sum: 98.63411056995392
time_elpased: 1.409
batch start
#iterations: 196
currently lose_sum: 98.75772613286972
time_elpased: 1.426
batch start
#iterations: 197
currently lose_sum: 98.63460975885391
time_elpased: 1.425
batch start
#iterations: 198
currently lose_sum: 98.67082178592682
time_elpased: 1.408
batch start
#iterations: 199
currently lose_sum: 98.74801278114319
time_elpased: 1.457
start validation test
0.604072164948
0.621219337938
0.536894103118
0.575986751311
0.604190106397
64.612
batch start
#iterations: 200
currently lose_sum: 98.7142545580864
time_elpased: 1.402
batch start
#iterations: 201
currently lose_sum: 98.76859402656555
time_elpased: 1.392
batch start
#iterations: 202
currently lose_sum: 98.69879138469696
time_elpased: 1.418
batch start
#iterations: 203
currently lose_sum: 98.58234083652496
time_elpased: 1.417
batch start
#iterations: 204
currently lose_sum: 98.48509383201599
time_elpased: 1.443
batch start
#iterations: 205
currently lose_sum: 98.70150071382523
time_elpased: 1.447
batch start
#iterations: 206
currently lose_sum: 98.47791826725006
time_elpased: 1.531
batch start
#iterations: 207
currently lose_sum: 98.67242485284805
time_elpased: 1.42
batch start
#iterations: 208
currently lose_sum: 98.69307684898376
time_elpased: 1.451
batch start
#iterations: 209
currently lose_sum: 98.68812966346741
time_elpased: 1.433
batch start
#iterations: 210
currently lose_sum: 98.4204740524292
time_elpased: 1.46
batch start
#iterations: 211
currently lose_sum: 98.7489640712738
time_elpased: 1.429
batch start
#iterations: 212
currently lose_sum: 98.45996755361557
time_elpased: 1.443
batch start
#iterations: 213
currently lose_sum: 98.75716888904572
time_elpased: 1.415
batch start
#iterations: 214
currently lose_sum: 98.76629757881165
time_elpased: 1.44
batch start
#iterations: 215
currently lose_sum: 98.68471401929855
time_elpased: 1.44
batch start
#iterations: 216
currently lose_sum: 98.58446425199509
time_elpased: 1.498
batch start
#iterations: 217
currently lose_sum: 98.51305359601974
time_elpased: 1.403
batch start
#iterations: 218
currently lose_sum: 98.35742163658142
time_elpased: 1.439
batch start
#iterations: 219
currently lose_sum: 98.4247761964798
time_elpased: 1.504
start validation test
0.60087628866
0.634946677605
0.477925285582
0.545358463977
0.60109214811
64.747
batch start
#iterations: 220
currently lose_sum: 98.93613040447235
time_elpased: 1.469
batch start
#iterations: 221
currently lose_sum: 98.56913018226624
time_elpased: 1.445
batch start
#iterations: 222
currently lose_sum: 98.5156717300415
time_elpased: 1.445
batch start
#iterations: 223
currently lose_sum: 98.67723029851913
time_elpased: 1.415
batch start
#iterations: 224
currently lose_sum: 98.56380927562714
time_elpased: 1.418
batch start
#iterations: 225
currently lose_sum: 98.60151815414429
time_elpased: 1.413
batch start
#iterations: 226
currently lose_sum: 98.86504429578781
time_elpased: 1.429
batch start
#iterations: 227
currently lose_sum: 98.420281291008
time_elpased: 1.468
batch start
#iterations: 228
currently lose_sum: 98.8274918794632
time_elpased: 1.456
batch start
#iterations: 229
currently lose_sum: 98.7100357413292
time_elpased: 1.42
batch start
#iterations: 230
currently lose_sum: 98.53233128786087
time_elpased: 1.432
batch start
#iterations: 231
currently lose_sum: 98.5479343533516
time_elpased: 1.437
batch start
#iterations: 232
currently lose_sum: 98.36939579248428
time_elpased: 1.487
batch start
#iterations: 233
currently lose_sum: 98.54698675870895
time_elpased: 1.425
batch start
#iterations: 234
currently lose_sum: 98.52385860681534
time_elpased: 1.443
batch start
#iterations: 235
currently lose_sum: 98.82055616378784
time_elpased: 1.465
batch start
#iterations: 236
currently lose_sum: 98.69933784008026
time_elpased: 1.429
batch start
#iterations: 237
currently lose_sum: 98.71576142311096
time_elpased: 1.413
batch start
#iterations: 238
currently lose_sum: 98.74742877483368
time_elpased: 1.441
batch start
#iterations: 239
currently lose_sum: 98.7886375784874
time_elpased: 1.446
start validation test
0.604484536082
0.634438305709
0.496346609036
0.556960563543
0.604674388893
64.775
batch start
#iterations: 240
currently lose_sum: 98.59963887929916
time_elpased: 1.408
batch start
#iterations: 241
currently lose_sum: 98.64535397291183
time_elpased: 1.45
batch start
#iterations: 242
currently lose_sum: 98.54902893304825
time_elpased: 1.441
batch start
#iterations: 243
currently lose_sum: 98.69516462087631
time_elpased: 1.415
batch start
#iterations: 244
currently lose_sum: 98.71424788236618
time_elpased: 1.427
batch start
#iterations: 245
currently lose_sum: 98.69338691234589
time_elpased: 1.432
batch start
#iterations: 246
currently lose_sum: 98.57648116350174
time_elpased: 1.435
batch start
#iterations: 247
currently lose_sum: 98.47192496061325
time_elpased: 1.416
batch start
#iterations: 248
currently lose_sum: 98.62259274721146
time_elpased: 1.447
batch start
#iterations: 249
currently lose_sum: 98.45612281560898
time_elpased: 1.429
batch start
#iterations: 250
currently lose_sum: 98.42933243513107
time_elpased: 1.444
batch start
#iterations: 251
currently lose_sum: 98.67837017774582
time_elpased: 1.436
batch start
#iterations: 252
currently lose_sum: 98.55982410907745
time_elpased: 1.455
batch start
#iterations: 253
currently lose_sum: 98.69939804077148
time_elpased: 1.444
batch start
#iterations: 254
currently lose_sum: 98.54470455646515
time_elpased: 1.412
batch start
#iterations: 255
currently lose_sum: 98.57347100973129
time_elpased: 1.456
batch start
#iterations: 256
currently lose_sum: 98.6977795958519
time_elpased: 1.466
batch start
#iterations: 257
currently lose_sum: 98.34822630882263
time_elpased: 1.421
batch start
#iterations: 258
currently lose_sum: 98.5840300321579
time_elpased: 1.451
batch start
#iterations: 259
currently lose_sum: 98.50657564401627
time_elpased: 1.411
start validation test
0.60587628866
0.629388979133
0.518369867243
0.568510158014
0.60602991968
64.492
batch start
#iterations: 260
currently lose_sum: 98.62352961301804
time_elpased: 1.433
batch start
#iterations: 261
currently lose_sum: 98.22906517982483
time_elpased: 1.468
batch start
#iterations: 262
currently lose_sum: 98.45571792125702
time_elpased: 1.459
batch start
#iterations: 263
currently lose_sum: 98.6189295053482
time_elpased: 1.447
batch start
#iterations: 264
currently lose_sum: 98.59756296873093
time_elpased: 1.433
batch start
#iterations: 265
currently lose_sum: 98.5724687576294
time_elpased: 1.45
batch start
#iterations: 266
currently lose_sum: 98.4982441663742
time_elpased: 1.411
batch start
#iterations: 267
currently lose_sum: 98.61325961351395
time_elpased: 1.431
batch start
#iterations: 268
currently lose_sum: 98.59010398387909
time_elpased: 1.452
batch start
#iterations: 269
currently lose_sum: 98.61538714170456
time_elpased: 1.446
batch start
#iterations: 270
currently lose_sum: 98.74395662546158
time_elpased: 1.427
batch start
#iterations: 271
currently lose_sum: 98.53787451982498
time_elpased: 1.431
batch start
#iterations: 272
currently lose_sum: 98.39959985017776
time_elpased: 1.443
batch start
#iterations: 273
currently lose_sum: 98.29628074169159
time_elpased: 1.464
batch start
#iterations: 274
currently lose_sum: 98.65914076566696
time_elpased: 1.43
batch start
#iterations: 275
currently lose_sum: 98.71661329269409
time_elpased: 1.419
batch start
#iterations: 276
currently lose_sum: 98.69795501232147
time_elpased: 1.42
batch start
#iterations: 277
currently lose_sum: 98.47536611557007
time_elpased: 1.419
batch start
#iterations: 278
currently lose_sum: 98.51365995407104
time_elpased: 1.46
batch start
#iterations: 279
currently lose_sum: 98.66000854969025
time_elpased: 1.435
start validation test
0.605051546392
0.631849095342
0.50674076361
0.562421473444
0.605224146134
64.655
batch start
#iterations: 280
currently lose_sum: 98.6131272315979
time_elpased: 1.472
batch start
#iterations: 281
currently lose_sum: 98.3463471531868
time_elpased: 1.421
batch start
#iterations: 282
currently lose_sum: 98.70274323225021
time_elpased: 1.453
batch start
#iterations: 283
currently lose_sum: 98.53478592634201
time_elpased: 1.416
batch start
#iterations: 284
currently lose_sum: 98.53571653366089
time_elpased: 1.492
batch start
#iterations: 285
currently lose_sum: 98.58634239435196
time_elpased: 1.46
batch start
#iterations: 286
currently lose_sum: 98.5327576994896
time_elpased: 1.425
batch start
#iterations: 287
currently lose_sum: 98.56538218259811
time_elpased: 1.47
batch start
#iterations: 288
currently lose_sum: 98.51772677898407
time_elpased: 1.417
batch start
#iterations: 289
currently lose_sum: 98.62123733758926
time_elpased: 1.415
batch start
#iterations: 290
currently lose_sum: 98.4147515296936
time_elpased: 1.42
batch start
#iterations: 291
currently lose_sum: 98.49498671293259
time_elpased: 1.469
batch start
#iterations: 292
currently lose_sum: 98.39477384090424
time_elpased: 1.435
batch start
#iterations: 293
currently lose_sum: 98.54626476764679
time_elpased: 1.451
batch start
#iterations: 294
currently lose_sum: 98.79593032598495
time_elpased: 1.426
batch start
#iterations: 295
currently lose_sum: 98.66110128164291
time_elpased: 1.419
batch start
#iterations: 296
currently lose_sum: 98.5170127749443
time_elpased: 1.438
batch start
#iterations: 297
currently lose_sum: 98.3904139995575
time_elpased: 1.434
batch start
#iterations: 298
currently lose_sum: 98.50663113594055
time_elpased: 1.431
batch start
#iterations: 299
currently lose_sum: 98.59522712230682
time_elpased: 1.453
start validation test
0.600206185567
0.629268292683
0.491200987959
0.551728123916
0.600397561004
64.604
batch start
#iterations: 300
currently lose_sum: 98.51569187641144
time_elpased: 1.41
batch start
#iterations: 301
currently lose_sum: 98.62679040431976
time_elpased: 1.43
batch start
#iterations: 302
currently lose_sum: 98.56167477369308
time_elpased: 1.412
batch start
#iterations: 303
currently lose_sum: 98.49721968173981
time_elpased: 1.417
batch start
#iterations: 304
currently lose_sum: 98.48008888959885
time_elpased: 1.454
batch start
#iterations: 305
currently lose_sum: 98.41634565591812
time_elpased: 1.393
batch start
#iterations: 306
currently lose_sum: 98.46121436357498
time_elpased: 1.426
batch start
#iterations: 307
currently lose_sum: 98.79293078184128
time_elpased: 1.471
batch start
#iterations: 308
currently lose_sum: 98.6682630777359
time_elpased: 1.423
batch start
#iterations: 309
currently lose_sum: 98.38572061061859
time_elpased: 1.417
batch start
#iterations: 310
currently lose_sum: 98.65133225917816
time_elpased: 1.436
batch start
#iterations: 311
currently lose_sum: 98.47020143270493
time_elpased: 1.425
batch start
#iterations: 312
currently lose_sum: 98.49097895622253
time_elpased: 1.43
batch start
#iterations: 313
currently lose_sum: 98.49852579832077
time_elpased: 1.42
batch start
#iterations: 314
currently lose_sum: 98.5012104511261
time_elpased: 1.42
batch start
#iterations: 315
currently lose_sum: 98.43938952684402
time_elpased: 1.432
batch start
#iterations: 316
currently lose_sum: 98.34935027360916
time_elpased: 1.443
batch start
#iterations: 317
currently lose_sum: 98.50408661365509
time_elpased: 1.411
batch start
#iterations: 318
currently lose_sum: 98.41979575157166
time_elpased: 1.41
batch start
#iterations: 319
currently lose_sum: 98.76423102617264
time_elpased: 1.441
start validation test
0.605206185567
0.629433962264
0.514973757333
0.566479877738
0.605364602512
64.682
batch start
#iterations: 320
currently lose_sum: 98.52308076620102
time_elpased: 1.431
batch start
#iterations: 321
currently lose_sum: 98.74611884355545
time_elpased: 1.43
batch start
#iterations: 322
currently lose_sum: 98.54012608528137
time_elpased: 1.46
batch start
#iterations: 323
currently lose_sum: 98.68147641420364
time_elpased: 1.44
batch start
#iterations: 324
currently lose_sum: 98.69491070508957
time_elpased: 1.41
batch start
#iterations: 325
currently lose_sum: 98.76323181390762
time_elpased: 1.431
batch start
#iterations: 326
currently lose_sum: 98.39253151416779
time_elpased: 1.422
batch start
#iterations: 327
currently lose_sum: 98.49388545751572
time_elpased: 1.429
batch start
#iterations: 328
currently lose_sum: 98.59296220541
time_elpased: 1.456
batch start
#iterations: 329
currently lose_sum: 98.4830167889595
time_elpased: 1.431
batch start
#iterations: 330
currently lose_sum: 98.51039916276932
time_elpased: 1.432
batch start
#iterations: 331
currently lose_sum: 98.59288662672043
time_elpased: 1.444
batch start
#iterations: 332
currently lose_sum: 98.31163275241852
time_elpased: 1.464
batch start
#iterations: 333
currently lose_sum: 98.57733809947968
time_elpased: 1.43
batch start
#iterations: 334
currently lose_sum: 98.47118991613388
time_elpased: 1.435
batch start
#iterations: 335
currently lose_sum: 98.4983680844307
time_elpased: 1.43
batch start
#iterations: 336
currently lose_sum: 98.41361439228058
time_elpased: 1.442
batch start
#iterations: 337
currently lose_sum: 98.96373349428177
time_elpased: 1.418
batch start
#iterations: 338
currently lose_sum: 98.45401203632355
time_elpased: 1.474
batch start
#iterations: 339
currently lose_sum: 98.57284832000732
time_elpased: 1.405
start validation test
0.606443298969
0.624224343675
0.53833487702
0.578106868542
0.606562873809
64.569
batch start
#iterations: 340
currently lose_sum: 98.55019384622574
time_elpased: 1.49
batch start
#iterations: 341
currently lose_sum: 98.62763047218323
time_elpased: 1.408
batch start
#iterations: 342
currently lose_sum: 98.67124998569489
time_elpased: 1.428
batch start
#iterations: 343
currently lose_sum: 98.51601296663284
time_elpased: 1.417
batch start
#iterations: 344
currently lose_sum: 98.32977861166
time_elpased: 1.466
batch start
#iterations: 345
currently lose_sum: 98.69932305812836
time_elpased: 1.417
batch start
#iterations: 346
currently lose_sum: 98.51881808042526
time_elpased: 1.425
batch start
#iterations: 347
currently lose_sum: 98.38862037658691
time_elpased: 1.423
batch start
#iterations: 348
currently lose_sum: 98.52541816234589
time_elpased: 1.455
batch start
#iterations: 349
currently lose_sum: 98.45052462816238
time_elpased: 1.406
batch start
#iterations: 350
currently lose_sum: 98.6873539686203
time_elpased: 1.451
batch start
#iterations: 351
currently lose_sum: 98.62489569187164
time_elpased: 1.434
batch start
#iterations: 352
currently lose_sum: 98.46973294019699
time_elpased: 1.44
batch start
#iterations: 353
currently lose_sum: 98.4835951924324
time_elpased: 1.425
batch start
#iterations: 354
currently lose_sum: 98.7549791932106
time_elpased: 1.473
batch start
#iterations: 355
currently lose_sum: 98.40361994504929
time_elpased: 1.424
batch start
#iterations: 356
currently lose_sum: 98.55133259296417
time_elpased: 1.461
batch start
#iterations: 357
currently lose_sum: 98.58043038845062
time_elpased: 1.391
batch start
#iterations: 358
currently lose_sum: 98.3844022154808
time_elpased: 1.414
batch start
#iterations: 359
currently lose_sum: 98.67924737930298
time_elpased: 1.428
start validation test
0.603092783505
0.634341281471
0.490068951322
0.552949372968
0.603291214275
64.649
batch start
#iterations: 360
currently lose_sum: 98.58134204149246
time_elpased: 1.424
batch start
#iterations: 361
currently lose_sum: 98.71989566087723
time_elpased: 1.423
batch start
#iterations: 362
currently lose_sum: 98.51313042640686
time_elpased: 1.432
batch start
#iterations: 363
currently lose_sum: 98.42237186431885
time_elpased: 1.413
batch start
#iterations: 364
currently lose_sum: 98.47177165746689
time_elpased: 1.487
batch start
#iterations: 365
currently lose_sum: 98.60334897041321
time_elpased: 1.394
batch start
#iterations: 366
currently lose_sum: 98.73158550262451
time_elpased: 1.43
batch start
#iterations: 367
currently lose_sum: 98.30948972702026
time_elpased: 1.439
batch start
#iterations: 368
currently lose_sum: 98.59470695257187
time_elpased: 1.449
batch start
#iterations: 369
currently lose_sum: 98.51259917020798
time_elpased: 1.44
batch start
#iterations: 370
currently lose_sum: 98.44737178087234
time_elpased: 1.454
batch start
#iterations: 371
currently lose_sum: 98.5789367556572
time_elpased: 1.436
batch start
#iterations: 372
currently lose_sum: 98.34672683477402
time_elpased: 1.473
batch start
#iterations: 373
currently lose_sum: 98.41547775268555
time_elpased: 1.421
batch start
#iterations: 374
currently lose_sum: 98.45526951551437
time_elpased: 1.452
batch start
#iterations: 375
currently lose_sum: 98.47313219308853
time_elpased: 1.414
batch start
#iterations: 376
currently lose_sum: 98.27123194932938
time_elpased: 1.411
batch start
#iterations: 377
currently lose_sum: 98.41656047105789
time_elpased: 1.414
batch start
#iterations: 378
currently lose_sum: 98.4730777144432
time_elpased: 1.447
batch start
#iterations: 379
currently lose_sum: 98.64969968795776
time_elpased: 1.439
start validation test
0.606443298969
0.626334951456
0.531131007513
0.57481761987
0.606575521313
64.499
batch start
#iterations: 380
currently lose_sum: 98.40962326526642
time_elpased: 1.412
batch start
#iterations: 381
currently lose_sum: 98.40460288524628
time_elpased: 1.43
batch start
#iterations: 382
currently lose_sum: 98.54112076759338
time_elpased: 1.446
batch start
#iterations: 383
currently lose_sum: 98.4735563993454
time_elpased: 1.42
batch start
#iterations: 384
currently lose_sum: 98.80448919534683
time_elpased: 1.48
batch start
#iterations: 385
currently lose_sum: 98.75388836860657
time_elpased: 1.414
batch start
#iterations: 386
currently lose_sum: 98.67813318967819
time_elpased: 1.424
batch start
#iterations: 387
currently lose_sum: 98.5691813826561
time_elpased: 1.427
batch start
#iterations: 388
currently lose_sum: 98.60263508558273
time_elpased: 1.431
batch start
#iterations: 389
currently lose_sum: 98.44238018989563
time_elpased: 1.442
batch start
#iterations: 390
currently lose_sum: 98.45479661226273
time_elpased: 1.426
batch start
#iterations: 391
currently lose_sum: 98.42449271678925
time_elpased: 1.419
batch start
#iterations: 392
currently lose_sum: 98.60027813911438
time_elpased: 1.439
batch start
#iterations: 393
currently lose_sum: 98.47453820705414
time_elpased: 1.42
batch start
#iterations: 394
currently lose_sum: 98.51822477579117
time_elpased: 1.443
batch start
#iterations: 395
currently lose_sum: 98.36759114265442
time_elpased: 1.459
batch start
#iterations: 396
currently lose_sum: 98.53910160064697
time_elpased: 1.423
batch start
#iterations: 397
currently lose_sum: 98.6066757440567
time_elpased: 1.443
batch start
#iterations: 398
currently lose_sum: 98.69070744514465
time_elpased: 1.457
batch start
#iterations: 399
currently lose_sum: 98.36595386266708
time_elpased: 1.432
start validation test
0.605051546392
0.631984585742
0.506329113924
0.562221460405
0.605224868849
64.518
acc: 0.608
pre: 0.634
rec: 0.514
F1: 0.568
auc: 0.640
