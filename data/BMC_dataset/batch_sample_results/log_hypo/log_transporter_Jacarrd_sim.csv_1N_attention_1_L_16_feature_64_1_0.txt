start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.54700195789337
time_elpased: 1.671
batch start
#iterations: 1
currently lose_sum: 100.3539302945137
time_elpased: 1.696
batch start
#iterations: 2
currently lose_sum: 100.52128779888153
time_elpased: 1.687
batch start
#iterations: 3
currently lose_sum: 100.26682698726654
time_elpased: 1.616
batch start
#iterations: 4
currently lose_sum: 100.2739879488945
time_elpased: 1.615
batch start
#iterations: 5
currently lose_sum: 100.30917751789093
time_elpased: 1.709
batch start
#iterations: 6
currently lose_sum: 100.11781287193298
time_elpased: 1.707
batch start
#iterations: 7
currently lose_sum: 100.12329238653183
time_elpased: 1.663
batch start
#iterations: 8
currently lose_sum: 100.0697494149208
time_elpased: 1.661
batch start
#iterations: 9
currently lose_sum: 100.07026869058609
time_elpased: 1.72
batch start
#iterations: 10
currently lose_sum: 99.93988937139511
time_elpased: 1.678
batch start
#iterations: 11
currently lose_sum: 99.94623965024948
time_elpased: 1.642
batch start
#iterations: 12
currently lose_sum: 100.04652827978134
time_elpased: 1.634
batch start
#iterations: 13
currently lose_sum: 99.71467536687851
time_elpased: 1.661
batch start
#iterations: 14
currently lose_sum: 99.82983428239822
time_elpased: 1.719
batch start
#iterations: 15
currently lose_sum: 99.91150045394897
time_elpased: 1.642
batch start
#iterations: 16
currently lose_sum: 99.59121543169022
time_elpased: 1.662
batch start
#iterations: 17
currently lose_sum: 99.81503719091415
time_elpased: 1.639
batch start
#iterations: 18
currently lose_sum: 99.74003392457962
time_elpased: 1.648
batch start
#iterations: 19
currently lose_sum: 99.77705043554306
time_elpased: 1.654
start validation test
0.590206185567
0.611258027956
0.499536894103
0.549779136935
0.590365369493
65.712
batch start
#iterations: 20
currently lose_sum: 99.78881430625916
time_elpased: 1.649
batch start
#iterations: 21
currently lose_sum: 99.57545185089111
time_elpased: 1.691
batch start
#iterations: 22
currently lose_sum: 99.49797040224075
time_elpased: 1.695
batch start
#iterations: 23
currently lose_sum: 99.62616389989853
time_elpased: 1.655
batch start
#iterations: 24
currently lose_sum: 99.44566190242767
time_elpased: 1.676
batch start
#iterations: 25
currently lose_sum: 99.53343200683594
time_elpased: 1.645
batch start
#iterations: 26
currently lose_sum: 99.41353076696396
time_elpased: 1.653
batch start
#iterations: 27
currently lose_sum: 99.45132154226303
time_elpased: 1.616
batch start
#iterations: 28
currently lose_sum: 99.44780820608139
time_elpased: 1.658
batch start
#iterations: 29
currently lose_sum: 99.37852507829666
time_elpased: 1.694
batch start
#iterations: 30
currently lose_sum: 99.28068500757217
time_elpased: 1.63
batch start
#iterations: 31
currently lose_sum: 99.43095004558563
time_elpased: 1.663
batch start
#iterations: 32
currently lose_sum: 99.14836150407791
time_elpased: 1.643
batch start
#iterations: 33
currently lose_sum: 99.4038017988205
time_elpased: 1.632
batch start
#iterations: 34
currently lose_sum: 99.43974047899246
time_elpased: 1.67
batch start
#iterations: 35
currently lose_sum: 99.44256907701492
time_elpased: 1.633
batch start
#iterations: 36
currently lose_sum: 99.50085264444351
time_elpased: 1.654
batch start
#iterations: 37
currently lose_sum: 99.47790092229843
time_elpased: 1.651
batch start
#iterations: 38
currently lose_sum: 99.37929552793503
time_elpased: 1.632
batch start
#iterations: 39
currently lose_sum: 99.39146780967712
time_elpased: 1.615
start validation test
0.591030927835
0.605816023739
0.525264999485
0.562672252232
0.591146390066
65.253
batch start
#iterations: 40
currently lose_sum: 99.30071544647217
time_elpased: 1.7
batch start
#iterations: 41
currently lose_sum: 99.32241553068161
time_elpased: 1.667
batch start
#iterations: 42
currently lose_sum: 99.39558851718903
time_elpased: 1.608
batch start
#iterations: 43
currently lose_sum: 99.15305787324905
time_elpased: 1.63
batch start
#iterations: 44
currently lose_sum: 99.41140395402908
time_elpased: 1.623
batch start
#iterations: 45
currently lose_sum: 99.12808418273926
time_elpased: 1.629
batch start
#iterations: 46
currently lose_sum: 99.04160422086716
time_elpased: 1.592
batch start
#iterations: 47
currently lose_sum: 99.17172682285309
time_elpased: 1.645
batch start
#iterations: 48
currently lose_sum: 99.22349309921265
time_elpased: 1.652
batch start
#iterations: 49
currently lose_sum: 99.45632189512253
time_elpased: 1.656
batch start
#iterations: 50
currently lose_sum: 99.23283290863037
time_elpased: 1.61
batch start
#iterations: 51
currently lose_sum: 99.21629106998444
time_elpased: 1.685
batch start
#iterations: 52
currently lose_sum: 99.25055801868439
time_elpased: 1.675
batch start
#iterations: 53
currently lose_sum: 99.24915426969528
time_elpased: 1.682
batch start
#iterations: 54
currently lose_sum: 99.28911685943604
time_elpased: 1.649
batch start
#iterations: 55
currently lose_sum: 99.37920206785202
time_elpased: 1.628
batch start
#iterations: 56
currently lose_sum: 99.34604263305664
time_elpased: 1.676
batch start
#iterations: 57
currently lose_sum: 99.22433888912201
time_elpased: 1.672
batch start
#iterations: 58
currently lose_sum: 99.0375428199768
time_elpased: 1.659
batch start
#iterations: 59
currently lose_sum: 99.35883432626724
time_elpased: 1.641
start validation test
0.592268041237
0.626099092812
0.46166512298
0.531453619239
0.592497334804
65.317
batch start
#iterations: 60
currently lose_sum: 99.13467711210251
time_elpased: 1.622
batch start
#iterations: 61
currently lose_sum: 99.15592169761658
time_elpased: 1.691
batch start
#iterations: 62
currently lose_sum: 99.20179718732834
time_elpased: 1.701
batch start
#iterations: 63
currently lose_sum: 99.24203646183014
time_elpased: 1.672
batch start
#iterations: 64
currently lose_sum: 99.23720169067383
time_elpased: 1.657
batch start
#iterations: 65
currently lose_sum: 99.01777970790863
time_elpased: 1.65
batch start
#iterations: 66
currently lose_sum: 99.01323038339615
time_elpased: 1.636
batch start
#iterations: 67
currently lose_sum: 98.99598109722137
time_elpased: 1.657
batch start
#iterations: 68
currently lose_sum: 99.3385482430458
time_elpased: 1.667
batch start
#iterations: 69
currently lose_sum: 99.0084154009819
time_elpased: 1.622
batch start
#iterations: 70
currently lose_sum: 99.06834840774536
time_elpased: 1.693
batch start
#iterations: 71
currently lose_sum: 99.0234444141388
time_elpased: 1.651
batch start
#iterations: 72
currently lose_sum: 98.95842361450195
time_elpased: 1.679
batch start
#iterations: 73
currently lose_sum: 98.81116956472397
time_elpased: 1.65
batch start
#iterations: 74
currently lose_sum: 98.9181576371193
time_elpased: 1.634
batch start
#iterations: 75
currently lose_sum: 98.77445316314697
time_elpased: 1.654
batch start
#iterations: 76
currently lose_sum: 99.02806258201599
time_elpased: 1.669
batch start
#iterations: 77
currently lose_sum: 99.12823128700256
time_elpased: 1.66
batch start
#iterations: 78
currently lose_sum: 99.01387059688568
time_elpased: 1.62
batch start
#iterations: 79
currently lose_sum: 99.14961296319962
time_elpased: 1.639
start validation test
0.594896907216
0.621788148925
0.488113615313
0.546901124243
0.595084381756
65.151
batch start
#iterations: 80
currently lose_sum: 99.15564858913422
time_elpased: 1.672
batch start
#iterations: 81
currently lose_sum: 98.87639158964157
time_elpased: 1.666
batch start
#iterations: 82
currently lose_sum: 99.0191787481308
time_elpased: 1.628
batch start
#iterations: 83
currently lose_sum: 99.05867719650269
time_elpased: 1.629
batch start
#iterations: 84
currently lose_sum: 99.11123245954514
time_elpased: 1.708
batch start
#iterations: 85
currently lose_sum: 99.14730298519135
time_elpased: 1.686
batch start
#iterations: 86
currently lose_sum: 98.95763355493546
time_elpased: 1.643
batch start
#iterations: 87
currently lose_sum: 98.8950846195221
time_elpased: 1.631
batch start
#iterations: 88
currently lose_sum: 99.07526087760925
time_elpased: 1.656
batch start
#iterations: 89
currently lose_sum: 98.99805003404617
time_elpased: 1.666
batch start
#iterations: 90
currently lose_sum: 99.07540029287338
time_elpased: 1.672
batch start
#iterations: 91
currently lose_sum: 98.6562812924385
time_elpased: 1.684
batch start
#iterations: 92
currently lose_sum: 98.89153397083282
time_elpased: 1.733
batch start
#iterations: 93
currently lose_sum: 98.8912627696991
time_elpased: 1.662
batch start
#iterations: 94
currently lose_sum: 99.07100921869278
time_elpased: 1.682
batch start
#iterations: 95
currently lose_sum: 99.11671245098114
time_elpased: 1.705
batch start
#iterations: 96
currently lose_sum: 98.91163176298141
time_elpased: 1.669
batch start
#iterations: 97
currently lose_sum: 98.81677502393723
time_elpased: 1.646
batch start
#iterations: 98
currently lose_sum: 99.19434744119644
time_elpased: 1.681
batch start
#iterations: 99
currently lose_sum: 99.01065480709076
time_elpased: 1.711
start validation test
0.601546391753
0.623308923917
0.51682618092
0.565095082705
0.60169513115
64.884
batch start
#iterations: 100
currently lose_sum: 98.9559690952301
time_elpased: 1.659
batch start
#iterations: 101
currently lose_sum: 99.06055998802185
time_elpased: 1.675
batch start
#iterations: 102
currently lose_sum: 98.80627244710922
time_elpased: 1.662
batch start
#iterations: 103
currently lose_sum: 98.8320706486702
time_elpased: 1.674
batch start
#iterations: 104
currently lose_sum: 98.87293010950089
time_elpased: 1.687
batch start
#iterations: 105
currently lose_sum: 99.14183455705643
time_elpased: 1.65
batch start
#iterations: 106
currently lose_sum: 99.15835720300674
time_elpased: 1.704
batch start
#iterations: 107
currently lose_sum: 98.87609481811523
time_elpased: 1.693
batch start
#iterations: 108
currently lose_sum: 98.85094511508942
time_elpased: 1.681
batch start
#iterations: 109
currently lose_sum: 98.9563262462616
time_elpased: 1.72
batch start
#iterations: 110
currently lose_sum: 99.07012957334518
time_elpased: 1.67
batch start
#iterations: 111
currently lose_sum: 98.71049165725708
time_elpased: 1.71
batch start
#iterations: 112
currently lose_sum: 98.95466011762619
time_elpased: 1.658
batch start
#iterations: 113
currently lose_sum: 99.05997967720032
time_elpased: 1.717
batch start
#iterations: 114
currently lose_sum: 98.95799964666367
time_elpased: 1.635
batch start
#iterations: 115
currently lose_sum: 98.90466666221619
time_elpased: 1.641
batch start
#iterations: 116
currently lose_sum: 98.83276176452637
time_elpased: 1.678
batch start
#iterations: 117
currently lose_sum: 98.98575776815414
time_elpased: 1.667
batch start
#iterations: 118
currently lose_sum: 98.92126905918121
time_elpased: 1.679
batch start
#iterations: 119
currently lose_sum: 98.94379907846451
time_elpased: 1.692
start validation test
0.603505154639
0.621504860194
0.5329834311
0.573850415512
0.603628966402
64.909
batch start
#iterations: 120
currently lose_sum: 99.07678323984146
time_elpased: 1.66
batch start
#iterations: 121
currently lose_sum: 98.77835834026337
time_elpased: 1.646
batch start
#iterations: 122
currently lose_sum: 99.00653833150864
time_elpased: 1.698
batch start
#iterations: 123
currently lose_sum: 98.8743228316307
time_elpased: 1.663
batch start
#iterations: 124
currently lose_sum: 99.2453989982605
time_elpased: 1.703
batch start
#iterations: 125
currently lose_sum: 98.83244800567627
time_elpased: 1.679
batch start
#iterations: 126
currently lose_sum: 98.80053579807281
time_elpased: 1.646
batch start
#iterations: 127
currently lose_sum: 98.76304721832275
time_elpased: 1.673
batch start
#iterations: 128
currently lose_sum: 99.12159758806229
time_elpased: 1.679
batch start
#iterations: 129
currently lose_sum: 99.03152590990067
time_elpased: 1.673
batch start
#iterations: 130
currently lose_sum: 98.91920816898346
time_elpased: 1.723
batch start
#iterations: 131
currently lose_sum: 98.76028376817703
time_elpased: 1.695
batch start
#iterations: 132
currently lose_sum: 98.95416182279587
time_elpased: 1.679
batch start
#iterations: 133
currently lose_sum: 98.84776318073273
time_elpased: 1.754
batch start
#iterations: 134
currently lose_sum: 98.70726555585861
time_elpased: 1.665
batch start
#iterations: 135
currently lose_sum: 98.94877201318741
time_elpased: 1.739
batch start
#iterations: 136
currently lose_sum: 98.90910875797272
time_elpased: 1.673
batch start
#iterations: 137
currently lose_sum: 98.8215863108635
time_elpased: 1.667
batch start
#iterations: 138
currently lose_sum: 98.83052396774292
time_elpased: 1.648
batch start
#iterations: 139
currently lose_sum: 98.899678170681
time_elpased: 1.654
start validation test
0.603092783505
0.63150345547
0.498404857466
0.557114920051
0.603276579306
64.868
batch start
#iterations: 140
currently lose_sum: 98.72716581821442
time_elpased: 1.679
batch start
#iterations: 141
currently lose_sum: 98.80392247438431
time_elpased: 1.687
batch start
#iterations: 142
currently lose_sum: 99.01231896877289
time_elpased: 1.683
batch start
#iterations: 143
currently lose_sum: 98.79005914926529
time_elpased: 1.719
batch start
#iterations: 144
currently lose_sum: 98.9279842376709
time_elpased: 1.729
batch start
#iterations: 145
currently lose_sum: 98.88886731863022
time_elpased: 1.627
batch start
#iterations: 146
currently lose_sum: 98.92553901672363
time_elpased: 1.686
batch start
#iterations: 147
currently lose_sum: 98.88079696893692
time_elpased: 1.658
batch start
#iterations: 148
currently lose_sum: 99.27623319625854
time_elpased: 1.677
batch start
#iterations: 149
currently lose_sum: 98.78053611516953
time_elpased: 1.68
batch start
#iterations: 150
currently lose_sum: 98.93084597587585
time_elpased: 1.644
batch start
#iterations: 151
currently lose_sum: 98.86233848333359
time_elpased: 1.689
batch start
#iterations: 152
currently lose_sum: 98.89336770772934
time_elpased: 1.687
batch start
#iterations: 153
currently lose_sum: 98.8555069565773
time_elpased: 1.641
batch start
#iterations: 154
currently lose_sum: 98.95930242538452
time_elpased: 1.694
batch start
#iterations: 155
currently lose_sum: 98.93640899658203
time_elpased: 1.645
batch start
#iterations: 156
currently lose_sum: 98.71574997901917
time_elpased: 1.657
batch start
#iterations: 157
currently lose_sum: 98.87259584665298
time_elpased: 1.667
batch start
#iterations: 158
currently lose_sum: 98.81848537921906
time_elpased: 1.667
batch start
#iterations: 159
currently lose_sum: 98.87005114555359
time_elpased: 1.713
start validation test
0.605412371134
0.62503031773
0.530410620562
0.573846239492
0.605544048275
64.873
batch start
#iterations: 160
currently lose_sum: 98.75423562526703
time_elpased: 1.676
batch start
#iterations: 161
currently lose_sum: 98.80998003482819
time_elpased: 1.659
batch start
#iterations: 162
currently lose_sum: 98.86031889915466
time_elpased: 1.707
batch start
#iterations: 163
currently lose_sum: 98.9119787812233
time_elpased: 1.702
batch start
#iterations: 164
currently lose_sum: 98.80975198745728
time_elpased: 1.663
batch start
#iterations: 165
currently lose_sum: 98.87038880586624
time_elpased: 1.717
batch start
#iterations: 166
currently lose_sum: 98.93024718761444
time_elpased: 1.723
batch start
#iterations: 167
currently lose_sum: 98.91502422094345
time_elpased: 1.669
batch start
#iterations: 168
currently lose_sum: 98.82662534713745
time_elpased: 1.651
batch start
#iterations: 169
currently lose_sum: 98.71380656957626
time_elpased: 1.643
batch start
#iterations: 170
currently lose_sum: 98.9759635925293
time_elpased: 1.739
batch start
#iterations: 171
currently lose_sum: 98.84551590681076
time_elpased: 1.692
batch start
#iterations: 172
currently lose_sum: 98.78123182058334
time_elpased: 1.727
batch start
#iterations: 173
currently lose_sum: 98.67758083343506
time_elpased: 1.661
batch start
#iterations: 174
currently lose_sum: 98.92939227819443
time_elpased: 1.652
batch start
#iterations: 175
currently lose_sum: 98.92164605855942
time_elpased: 1.682
batch start
#iterations: 176
currently lose_sum: 99.06266069412231
time_elpased: 1.685
batch start
#iterations: 177
currently lose_sum: 98.8332850933075
time_elpased: 1.635
batch start
#iterations: 178
currently lose_sum: 98.68301749229431
time_elpased: 1.687
batch start
#iterations: 179
currently lose_sum: 98.74643224477768
time_elpased: 1.648
start validation test
0.598298969072
0.633945976051
0.468560255223
0.538848452571
0.598526745395
64.973
batch start
#iterations: 180
currently lose_sum: 98.89814120531082
time_elpased: 1.685
batch start
#iterations: 181
currently lose_sum: 98.85052394866943
time_elpased: 1.674
batch start
#iterations: 182
currently lose_sum: 98.71427035331726
time_elpased: 1.715
batch start
#iterations: 183
currently lose_sum: 98.63216257095337
time_elpased: 1.644
batch start
#iterations: 184
currently lose_sum: 98.74158596992493
time_elpased: 1.638
batch start
#iterations: 185
currently lose_sum: 98.86381703615189
time_elpased: 1.701
batch start
#iterations: 186
currently lose_sum: 98.82603973150253
time_elpased: 1.667
batch start
#iterations: 187
currently lose_sum: 98.53725039958954
time_elpased: 1.643
batch start
#iterations: 188
currently lose_sum: 98.70140987634659
time_elpased: 1.684
batch start
#iterations: 189
currently lose_sum: 98.66922557353973
time_elpased: 1.642
batch start
#iterations: 190
currently lose_sum: 98.77325677871704
time_elpased: 1.639
batch start
#iterations: 191
currently lose_sum: 98.75904422998428
time_elpased: 1.673
batch start
#iterations: 192
currently lose_sum: 98.69376015663147
time_elpased: 1.685
batch start
#iterations: 193
currently lose_sum: 98.6090475320816
time_elpased: 1.657
batch start
#iterations: 194
currently lose_sum: 98.89132422208786
time_elpased: 1.667
batch start
#iterations: 195
currently lose_sum: 98.97196739912033
time_elpased: 1.652
batch start
#iterations: 196
currently lose_sum: 98.97436481714249
time_elpased: 1.723
batch start
#iterations: 197
currently lose_sum: 98.8470767736435
time_elpased: 1.639
batch start
#iterations: 198
currently lose_sum: 98.63168382644653
time_elpased: 1.704
batch start
#iterations: 199
currently lose_sum: 98.9251617193222
time_elpased: 1.685
start validation test
0.604793814433
0.621503082029
0.539569826078
0.577645568226
0.604908325205
64.664
batch start
#iterations: 200
currently lose_sum: 98.78245586156845
time_elpased: 1.707
batch start
#iterations: 201
currently lose_sum: 98.88305747509003
time_elpased: 1.684
batch start
#iterations: 202
currently lose_sum: 98.68241947889328
time_elpased: 1.682
batch start
#iterations: 203
currently lose_sum: 98.83490544557571
time_elpased: 1.658
batch start
#iterations: 204
currently lose_sum: 98.7849931716919
time_elpased: 1.71
batch start
#iterations: 205
currently lose_sum: 98.70532304048538
time_elpased: 1.67
batch start
#iterations: 206
currently lose_sum: 98.62069582939148
time_elpased: 1.667
batch start
#iterations: 207
currently lose_sum: 98.89554154872894
time_elpased: 1.635
batch start
#iterations: 208
currently lose_sum: 98.7560133934021
time_elpased: 1.657
batch start
#iterations: 209
currently lose_sum: 98.75724220275879
time_elpased: 1.632
batch start
#iterations: 210
currently lose_sum: 98.599480509758
time_elpased: 1.711
batch start
#iterations: 211
currently lose_sum: 98.60271179676056
time_elpased: 1.644
batch start
#iterations: 212
currently lose_sum: 98.5704984664917
time_elpased: 1.666
batch start
#iterations: 213
currently lose_sum: 98.60554647445679
time_elpased: 1.635
batch start
#iterations: 214
currently lose_sum: 98.72071105241776
time_elpased: 1.712
batch start
#iterations: 215
currently lose_sum: 98.58180689811707
time_elpased: 1.649
batch start
#iterations: 216
currently lose_sum: 98.7851231098175
time_elpased: 1.668
batch start
#iterations: 217
currently lose_sum: 98.36676037311554
time_elpased: 1.684
batch start
#iterations: 218
currently lose_sum: 98.74380081892014
time_elpased: 1.685
batch start
#iterations: 219
currently lose_sum: 98.76485294103622
time_elpased: 1.643
start validation test
0.607010309278
0.63377221015
0.510239785942
0.565336374002
0.607180204858
64.832
batch start
#iterations: 220
currently lose_sum: 98.75830936431885
time_elpased: 1.646
batch start
#iterations: 221
currently lose_sum: 99.00009459257126
time_elpased: 1.741
batch start
#iterations: 222
currently lose_sum: 98.69665169715881
time_elpased: 1.662
batch start
#iterations: 223
currently lose_sum: 98.5860965847969
time_elpased: 1.678
batch start
#iterations: 224
currently lose_sum: 98.8201168179512
time_elpased: 1.76
batch start
#iterations: 225
currently lose_sum: 98.43187415599823
time_elpased: 1.642
batch start
#iterations: 226
currently lose_sum: 98.66909152269363
time_elpased: 1.646
batch start
#iterations: 227
currently lose_sum: 98.66654163599014
time_elpased: 1.73
batch start
#iterations: 228
currently lose_sum: 98.67218267917633
time_elpased: 1.726
batch start
#iterations: 229
currently lose_sum: 98.81327599287033
time_elpased: 1.653
batch start
#iterations: 230
currently lose_sum: 98.30978858470917
time_elpased: 1.688
batch start
#iterations: 231
currently lose_sum: 98.73213541507721
time_elpased: 1.639
batch start
#iterations: 232
currently lose_sum: 98.71626937389374
time_elpased: 1.682
batch start
#iterations: 233
currently lose_sum: 98.71860682964325
time_elpased: 1.665
batch start
#iterations: 234
currently lose_sum: 98.61316752433777
time_elpased: 1.65
batch start
#iterations: 235
currently lose_sum: 98.61008948087692
time_elpased: 1.694
batch start
#iterations: 236
currently lose_sum: 98.82976531982422
time_elpased: 1.676
batch start
#iterations: 237
currently lose_sum: 98.78511714935303
time_elpased: 1.662
batch start
#iterations: 238
currently lose_sum: 98.84171921014786
time_elpased: 1.674
batch start
#iterations: 239
currently lose_sum: 98.67615336179733
time_elpased: 1.654
start validation test
0.605824742268
0.627904102818
0.52289801379
0.570610365546
0.60597033293
64.719
batch start
#iterations: 240
currently lose_sum: 98.75955903530121
time_elpased: 1.655
batch start
#iterations: 241
currently lose_sum: 98.7607074379921
time_elpased: 1.669
batch start
#iterations: 242
currently lose_sum: 98.49584066867828
time_elpased: 1.667
batch start
#iterations: 243
currently lose_sum: 98.76186013221741
time_elpased: 1.656
batch start
#iterations: 244
currently lose_sum: 98.73910182714462
time_elpased: 1.659
batch start
#iterations: 245
currently lose_sum: 98.65616464614868
time_elpased: 1.674
batch start
#iterations: 246
currently lose_sum: 98.6705647110939
time_elpased: 1.714
batch start
#iterations: 247
currently lose_sum: 98.57313668727875
time_elpased: 1.653
batch start
#iterations: 248
currently lose_sum: 99.0788562297821
time_elpased: 1.693
batch start
#iterations: 249
currently lose_sum: 98.66301077604294
time_elpased: 1.695
batch start
#iterations: 250
currently lose_sum: 98.73707550764084
time_elpased: 1.723
batch start
#iterations: 251
currently lose_sum: 98.56602740287781
time_elpased: 1.671
batch start
#iterations: 252
currently lose_sum: 98.88141626119614
time_elpased: 1.698
batch start
#iterations: 253
currently lose_sum: 98.93125087022781
time_elpased: 1.688
batch start
#iterations: 254
currently lose_sum: 98.35601603984833
time_elpased: 1.659
batch start
#iterations: 255
currently lose_sum: 98.51826518774033
time_elpased: 1.674
batch start
#iterations: 256
currently lose_sum: 98.62067717313766
time_elpased: 1.674
batch start
#iterations: 257
currently lose_sum: 98.58006548881531
time_elpased: 1.661
batch start
#iterations: 258
currently lose_sum: 98.77077859640121
time_elpased: 1.68
batch start
#iterations: 259
currently lose_sum: 98.61787968873978
time_elpased: 1.686
start validation test
0.605979381443
0.629030250218
0.520016465987
0.569352112676
0.6061303026
64.812
batch start
#iterations: 260
currently lose_sum: 98.50453853607178
time_elpased: 1.666
batch start
#iterations: 261
currently lose_sum: 98.84812116622925
time_elpased: 1.652
batch start
#iterations: 262
currently lose_sum: 98.6821967959404
time_elpased: 1.66
batch start
#iterations: 263
currently lose_sum: 98.60883510112762
time_elpased: 1.752
batch start
#iterations: 264
currently lose_sum: 98.63236600160599
time_elpased: 1.682
batch start
#iterations: 265
currently lose_sum: 98.76898646354675
time_elpased: 1.651
batch start
#iterations: 266
currently lose_sum: 98.43273502588272
time_elpased: 1.653
batch start
#iterations: 267
currently lose_sum: 98.53310698270798
time_elpased: 1.715
batch start
#iterations: 268
currently lose_sum: 98.69883477687836
time_elpased: 1.673
batch start
#iterations: 269
currently lose_sum: 98.59025710821152
time_elpased: 1.627
batch start
#iterations: 270
currently lose_sum: 98.9309293627739
time_elpased: 1.642
batch start
#iterations: 271
currently lose_sum: 98.5880024433136
time_elpased: 1.679
batch start
#iterations: 272
currently lose_sum: 98.69010204076767
time_elpased: 1.655
batch start
#iterations: 273
currently lose_sum: 98.46754676103592
time_elpased: 1.617
batch start
#iterations: 274
currently lose_sum: 98.55059438943863
time_elpased: 1.691
batch start
#iterations: 275
currently lose_sum: 98.88179725408554
time_elpased: 1.65
batch start
#iterations: 276
currently lose_sum: 98.99216961860657
time_elpased: 1.662
batch start
#iterations: 277
currently lose_sum: 98.7602391242981
time_elpased: 1.67
batch start
#iterations: 278
currently lose_sum: 98.66033548116684
time_elpased: 1.662
batch start
#iterations: 279
currently lose_sum: 98.91662234067917
time_elpased: 1.684
start validation test
0.606907216495
0.627952514992
0.528043634867
0.573680679785
0.607045673676
64.634
batch start
#iterations: 280
currently lose_sum: 98.77848762273788
time_elpased: 1.698
batch start
#iterations: 281
currently lose_sum: 98.79708820581436
time_elpased: 1.637
batch start
#iterations: 282
currently lose_sum: 98.79568475484848
time_elpased: 1.692
batch start
#iterations: 283
currently lose_sum: 98.81656795740128
time_elpased: 1.735
batch start
#iterations: 284
currently lose_sum: 98.46052247285843
time_elpased: 1.653
batch start
#iterations: 285
currently lose_sum: 98.8974199295044
time_elpased: 1.662
batch start
#iterations: 286
currently lose_sum: 98.56893819570541
time_elpased: 1.665
batch start
#iterations: 287
currently lose_sum: 98.57101041078568
time_elpased: 1.632
batch start
#iterations: 288
currently lose_sum: 98.65172904729843
time_elpased: 1.679
batch start
#iterations: 289
currently lose_sum: 98.71517944335938
time_elpased: 1.678
batch start
#iterations: 290
currently lose_sum: 98.58793365955353
time_elpased: 1.7
batch start
#iterations: 291
currently lose_sum: 98.4447351694107
time_elpased: 1.655
batch start
#iterations: 292
currently lose_sum: 98.67155057191849
time_elpased: 1.635
batch start
#iterations: 293
currently lose_sum: 98.51985096931458
time_elpased: 1.661
batch start
#iterations: 294
currently lose_sum: 98.66118586063385
time_elpased: 1.68
batch start
#iterations: 295
currently lose_sum: 98.49187171459198
time_elpased: 1.613
batch start
#iterations: 296
currently lose_sum: 98.80695223808289
time_elpased: 1.663
batch start
#iterations: 297
currently lose_sum: 98.58727794885635
time_elpased: 1.661
batch start
#iterations: 298
currently lose_sum: 98.63151371479034
time_elpased: 1.694
batch start
#iterations: 299
currently lose_sum: 98.5565260052681
time_elpased: 1.666
start validation test
0.604587628866
0.626734390486
0.520633940517
0.568778458598
0.604735022515
64.724
batch start
#iterations: 300
currently lose_sum: 98.58729928731918
time_elpased: 1.636
batch start
#iterations: 301
currently lose_sum: 98.72512650489807
time_elpased: 1.641
batch start
#iterations: 302
currently lose_sum: 98.76190555095673
time_elpased: 1.634
batch start
#iterations: 303
currently lose_sum: 98.77537178993225
time_elpased: 1.715
batch start
#iterations: 304
currently lose_sum: 98.65057265758514
time_elpased: 1.661
batch start
#iterations: 305
currently lose_sum: 98.7975749373436
time_elpased: 1.637
batch start
#iterations: 306
currently lose_sum: 98.89201760292053
time_elpased: 1.649
batch start
#iterations: 307
currently lose_sum: 98.58124017715454
time_elpased: 1.681
batch start
#iterations: 308
currently lose_sum: 98.64931190013885
time_elpased: 1.69
batch start
#iterations: 309
currently lose_sum: 98.45708227157593
time_elpased: 1.661
batch start
#iterations: 310
currently lose_sum: 98.76035326719284
time_elpased: 1.685
batch start
#iterations: 311
currently lose_sum: 98.59118473529816
time_elpased: 1.631
batch start
#iterations: 312
currently lose_sum: 98.81804567575455
time_elpased: 1.702
batch start
#iterations: 313
currently lose_sum: 98.3010927438736
time_elpased: 1.652
batch start
#iterations: 314
currently lose_sum: 98.55204832553864
time_elpased: 1.646
batch start
#iterations: 315
currently lose_sum: 98.67208242416382
time_elpased: 1.686
batch start
#iterations: 316
currently lose_sum: 98.80495947599411
time_elpased: 1.652
batch start
#iterations: 317
currently lose_sum: 98.62078499794006
time_elpased: 1.644
batch start
#iterations: 318
currently lose_sum: 98.80668413639069
time_elpased: 1.645
batch start
#iterations: 319
currently lose_sum: 98.60900205373764
time_elpased: 1.686
start validation test
0.605721649485
0.634565330557
0.501800967377
0.560427561634
0.605904098271
64.852
batch start
#iterations: 320
currently lose_sum: 98.6720187664032
time_elpased: 1.624
batch start
#iterations: 321
currently lose_sum: 98.59090250730515
time_elpased: 1.656
batch start
#iterations: 322
currently lose_sum: 98.53109502792358
time_elpased: 1.644
batch start
#iterations: 323
currently lose_sum: 98.67737954854965
time_elpased: 1.636
batch start
#iterations: 324
currently lose_sum: 98.70146989822388
time_elpased: 1.664
batch start
#iterations: 325
currently lose_sum: 98.77531492710114
time_elpased: 1.685
batch start
#iterations: 326
currently lose_sum: 98.44362562894821
time_elpased: 1.693
batch start
#iterations: 327
currently lose_sum: 98.62653523683548
time_elpased: 1.648
batch start
#iterations: 328
currently lose_sum: 98.41099691390991
time_elpased: 1.656
batch start
#iterations: 329
currently lose_sum: 98.83968275785446
time_elpased: 1.63
batch start
#iterations: 330
currently lose_sum: 98.45769184827805
time_elpased: 1.699
batch start
#iterations: 331
currently lose_sum: 98.58087986707687
time_elpased: 1.7
batch start
#iterations: 332
currently lose_sum: 98.67808365821838
time_elpased: 1.628
batch start
#iterations: 333
currently lose_sum: 98.58486258983612
time_elpased: 1.679
batch start
#iterations: 334
currently lose_sum: 98.74946695566177
time_elpased: 1.68
batch start
#iterations: 335
currently lose_sum: 98.63782179355621
time_elpased: 1.668
batch start
#iterations: 336
currently lose_sum: 98.50391030311584
time_elpased: 1.662
batch start
#iterations: 337
currently lose_sum: 98.54893386363983
time_elpased: 1.644
batch start
#iterations: 338
currently lose_sum: 98.65311872959137
time_elpased: 1.631
batch start
#iterations: 339
currently lose_sum: 98.58322215080261
time_elpased: 1.64
start validation test
0.602319587629
0.629730430275
0.500051456211
0.55744851718
0.602499135107
64.763
batch start
#iterations: 340
currently lose_sum: 98.52282351255417
time_elpased: 1.637
batch start
#iterations: 341
currently lose_sum: 98.76832431554794
time_elpased: 1.724
batch start
#iterations: 342
currently lose_sum: 98.72138953208923
time_elpased: 1.701
batch start
#iterations: 343
currently lose_sum: 98.74266022443771
time_elpased: 1.658
batch start
#iterations: 344
currently lose_sum: 98.61387622356415
time_elpased: 1.666
batch start
#iterations: 345
currently lose_sum: 98.83896678686142
time_elpased: 1.662
batch start
#iterations: 346
currently lose_sum: 98.62813246250153
time_elpased: 1.674
batch start
#iterations: 347
currently lose_sum: 98.65693897008896
time_elpased: 1.704
batch start
#iterations: 348
currently lose_sum: 98.75380957126617
time_elpased: 1.658
batch start
#iterations: 349
currently lose_sum: 98.71826642751694
time_elpased: 1.641
batch start
#iterations: 350
currently lose_sum: 98.46052747964859
time_elpased: 1.627
batch start
#iterations: 351
currently lose_sum: 98.57524991035461
time_elpased: 1.739
batch start
#iterations: 352
currently lose_sum: 98.60389333963394
time_elpased: 1.641
batch start
#iterations: 353
currently lose_sum: 98.71961987018585
time_elpased: 1.664
batch start
#iterations: 354
currently lose_sum: 98.75535315275192
time_elpased: 1.719
batch start
#iterations: 355
currently lose_sum: 98.49053704738617
time_elpased: 1.668
batch start
#iterations: 356
currently lose_sum: 98.46782559156418
time_elpased: 1.68
batch start
#iterations: 357
currently lose_sum: 98.63082349300385
time_elpased: 1.649
batch start
#iterations: 358
currently lose_sum: 98.51585501432419
time_elpased: 1.659
batch start
#iterations: 359
currently lose_sum: 98.38020515441895
time_elpased: 1.65
start validation test
0.605618556701
0.63095841785
0.512195121951
0.565407554672
0.60578257595
64.718
batch start
#iterations: 360
currently lose_sum: 98.92576688528061
time_elpased: 1.683
batch start
#iterations: 361
currently lose_sum: 98.59799361228943
time_elpased: 1.666
batch start
#iterations: 362
currently lose_sum: 98.36585694551468
time_elpased: 1.674
batch start
#iterations: 363
currently lose_sum: 98.39116710424423
time_elpased: 1.665
batch start
#iterations: 364
currently lose_sum: 98.81771665811539
time_elpased: 1.64
batch start
#iterations: 365
currently lose_sum: 98.54297649860382
time_elpased: 1.645
batch start
#iterations: 366
currently lose_sum: 98.81058144569397
time_elpased: 1.667
batch start
#iterations: 367
currently lose_sum: 98.63309562206268
time_elpased: 1.68
batch start
#iterations: 368
currently lose_sum: 98.74367779493332
time_elpased: 1.662
batch start
#iterations: 369
currently lose_sum: 98.4658272266388
time_elpased: 1.649
batch start
#iterations: 370
currently lose_sum: 98.53005421161652
time_elpased: 1.69
batch start
#iterations: 371
currently lose_sum: 98.72697567939758
time_elpased: 1.747
batch start
#iterations: 372
currently lose_sum: 98.60346281528473
time_elpased: 1.651
batch start
#iterations: 373
currently lose_sum: 98.51874452829361
time_elpased: 1.723
batch start
#iterations: 374
currently lose_sum: 98.78722703456879
time_elpased: 1.641
batch start
#iterations: 375
currently lose_sum: 98.73440915346146
time_elpased: 1.658
batch start
#iterations: 376
currently lose_sum: 98.61866927146912
time_elpased: 1.7
batch start
#iterations: 377
currently lose_sum: 98.65409350395203
time_elpased: 1.666
batch start
#iterations: 378
currently lose_sum: 98.70940589904785
time_elpased: 1.7
batch start
#iterations: 379
currently lose_sum: 98.69971340894699
time_elpased: 1.674
start validation test
0.602525773196
0.629385964912
0.502109704641
0.558589501402
0.602702069092
64.779
batch start
#iterations: 380
currently lose_sum: 98.57848209142685
time_elpased: 1.695
batch start
#iterations: 381
currently lose_sum: 98.51095402240753
time_elpased: 1.756
batch start
#iterations: 382
currently lose_sum: 98.57184433937073
time_elpased: 1.648
batch start
#iterations: 383
currently lose_sum: 98.37019354104996
time_elpased: 1.657
batch start
#iterations: 384
currently lose_sum: 98.7101446390152
time_elpased: 1.646
batch start
#iterations: 385
currently lose_sum: 98.76391071081161
time_elpased: 1.664
batch start
#iterations: 386
currently lose_sum: 98.44668966531754
time_elpased: 1.657
batch start
#iterations: 387
currently lose_sum: 98.74697440862656
time_elpased: 1.656
batch start
#iterations: 388
currently lose_sum: 98.5433737039566
time_elpased: 1.634
batch start
#iterations: 389
currently lose_sum: 98.73393559455872
time_elpased: 1.65
batch start
#iterations: 390
currently lose_sum: 98.75775337219238
time_elpased: 1.666
batch start
#iterations: 391
currently lose_sum: 98.61315488815308
time_elpased: 1.646
batch start
#iterations: 392
currently lose_sum: 98.57928276062012
time_elpased: 1.634
batch start
#iterations: 393
currently lose_sum: 98.40956997871399
time_elpased: 1.674
batch start
#iterations: 394
currently lose_sum: 98.43212431669235
time_elpased: 1.664
batch start
#iterations: 395
currently lose_sum: 98.50478023290634
time_elpased: 1.654
batch start
#iterations: 396
currently lose_sum: 98.46249514818192
time_elpased: 1.642
batch start
#iterations: 397
currently lose_sum: 98.62875252962112
time_elpased: 1.659
batch start
#iterations: 398
currently lose_sum: 98.481380879879
time_elpased: 1.639
batch start
#iterations: 399
currently lose_sum: 98.47638368606567
time_elpased: 1.635
start validation test
0.598659793814
0.635737382258
0.465369970155
0.537373737374
0.598893804658
65.051
acc: 0.610
pre: 0.630
rec: 0.536
F1: 0.579
auc: 0.640
