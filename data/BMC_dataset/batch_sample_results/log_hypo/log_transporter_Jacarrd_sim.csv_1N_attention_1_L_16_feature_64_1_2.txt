start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.61995261907578
time_elpased: 1.593
batch start
#iterations: 1
currently lose_sum: 100.37803930044174
time_elpased: 1.582
batch start
#iterations: 2
currently lose_sum: 100.45459014177322
time_elpased: 1.529
batch start
#iterations: 3
currently lose_sum: 100.36169457435608
time_elpased: 1.541
batch start
#iterations: 4
currently lose_sum: 100.22176116704941
time_elpased: 1.545
batch start
#iterations: 5
currently lose_sum: 100.21986091136932
time_elpased: 1.592
batch start
#iterations: 6
currently lose_sum: 100.11815416812897
time_elpased: 1.562
batch start
#iterations: 7
currently lose_sum: 100.0169860124588
time_elpased: 1.577
batch start
#iterations: 8
currently lose_sum: 99.97067189216614
time_elpased: 1.6
batch start
#iterations: 9
currently lose_sum: 99.79704189300537
time_elpased: 1.555
batch start
#iterations: 10
currently lose_sum: 99.9577711224556
time_elpased: 1.55
batch start
#iterations: 11
currently lose_sum: 99.9701492190361
time_elpased: 1.537
batch start
#iterations: 12
currently lose_sum: 99.94562041759491
time_elpased: 1.591
batch start
#iterations: 13
currently lose_sum: 99.83802664279938
time_elpased: 1.544
batch start
#iterations: 14
currently lose_sum: 99.88095939159393
time_elpased: 1.595
batch start
#iterations: 15
currently lose_sum: 99.64515316486359
time_elpased: 1.528
batch start
#iterations: 16
currently lose_sum: 99.68999588489532
time_elpased: 1.526
batch start
#iterations: 17
currently lose_sum: 99.92687278985977
time_elpased: 1.527
batch start
#iterations: 18
currently lose_sum: 99.484588265419
time_elpased: 1.551
batch start
#iterations: 19
currently lose_sum: 99.69243174791336
time_elpased: 1.552
start validation test
0.583453608247
0.594326568266
0.530410620562
0.560552504214
0.583546733394
65.688
batch start
#iterations: 20
currently lose_sum: 99.5947277545929
time_elpased: 1.582
batch start
#iterations: 21
currently lose_sum: 99.67513436079025
time_elpased: 1.596
batch start
#iterations: 22
currently lose_sum: 99.64008516073227
time_elpased: 1.563
batch start
#iterations: 23
currently lose_sum: 99.4730578660965
time_elpased: 1.552
batch start
#iterations: 24
currently lose_sum: 99.48222434520721
time_elpased: 1.561
batch start
#iterations: 25
currently lose_sum: 99.64199262857437
time_elpased: 1.576
batch start
#iterations: 26
currently lose_sum: 99.40527713298798
time_elpased: 1.524
batch start
#iterations: 27
currently lose_sum: 99.39274847507477
time_elpased: 1.568
batch start
#iterations: 28
currently lose_sum: 99.24638456106186
time_elpased: 1.556
batch start
#iterations: 29
currently lose_sum: 99.42583042383194
time_elpased: 1.567
batch start
#iterations: 30
currently lose_sum: 99.28654116392136
time_elpased: 1.566
batch start
#iterations: 31
currently lose_sum: 99.32885175943375
time_elpased: 1.585
batch start
#iterations: 32
currently lose_sum: 99.30646681785583
time_elpased: 1.614
batch start
#iterations: 33
currently lose_sum: 99.35244309902191
time_elpased: 1.592
batch start
#iterations: 34
currently lose_sum: 99.25428068637848
time_elpased: 1.539
batch start
#iterations: 35
currently lose_sum: 99.29998344182968
time_elpased: 1.599
batch start
#iterations: 36
currently lose_sum: 99.1917142868042
time_elpased: 1.541
batch start
#iterations: 37
currently lose_sum: 99.46259987354279
time_elpased: 1.585
batch start
#iterations: 38
currently lose_sum: 99.31250482797623
time_elpased: 1.562
batch start
#iterations: 39
currently lose_sum: 99.3097135424614
time_elpased: 1.564
start validation test
0.596494845361
0.612695382413
0.528455284553
0.567466018345
0.596614299304
65.365
batch start
#iterations: 40
currently lose_sum: 99.16047477722168
time_elpased: 1.587
batch start
#iterations: 41
currently lose_sum: 99.2612172961235
time_elpased: 1.554
batch start
#iterations: 42
currently lose_sum: 99.21218818426132
time_elpased: 1.544
batch start
#iterations: 43
currently lose_sum: 99.140360891819
time_elpased: 1.552
batch start
#iterations: 44
currently lose_sum: 99.24000972509384
time_elpased: 1.574
batch start
#iterations: 45
currently lose_sum: 99.26278394460678
time_elpased: 1.561
batch start
#iterations: 46
currently lose_sum: 98.85878330469131
time_elpased: 1.53
batch start
#iterations: 47
currently lose_sum: 99.18309462070465
time_elpased: 1.582
batch start
#iterations: 48
currently lose_sum: 99.25225222110748
time_elpased: 1.519
batch start
#iterations: 49
currently lose_sum: 98.96711671352386
time_elpased: 1.588
batch start
#iterations: 50
currently lose_sum: 98.99377483129501
time_elpased: 1.595
batch start
#iterations: 51
currently lose_sum: 99.08060538768768
time_elpased: 1.607
batch start
#iterations: 52
currently lose_sum: 99.21622228622437
time_elpased: 1.536
batch start
#iterations: 53
currently lose_sum: 99.16829419136047
time_elpased: 1.568
batch start
#iterations: 54
currently lose_sum: 99.01329153776169
time_elpased: 1.57
batch start
#iterations: 55
currently lose_sum: 99.04301589727402
time_elpased: 1.618
batch start
#iterations: 56
currently lose_sum: 99.19216239452362
time_elpased: 1.535
batch start
#iterations: 57
currently lose_sum: 99.0483267903328
time_elpased: 1.527
batch start
#iterations: 58
currently lose_sum: 99.0665118098259
time_elpased: 1.583
batch start
#iterations: 59
currently lose_sum: 99.11610102653503
time_elpased: 1.599
start validation test
0.590412371134
0.645330707369
0.404651641453
0.497406704617
0.590738502747
65.223
batch start
#iterations: 60
currently lose_sum: 98.84965270757675
time_elpased: 1.487
batch start
#iterations: 61
currently lose_sum: 99.09510546922684
time_elpased: 1.574
batch start
#iterations: 62
currently lose_sum: 99.220090508461
time_elpased: 1.611
batch start
#iterations: 63
currently lose_sum: 99.24008762836456
time_elpased: 1.557
batch start
#iterations: 64
currently lose_sum: 98.90970778465271
time_elpased: 1.528
batch start
#iterations: 65
currently lose_sum: 98.86790579557419
time_elpased: 1.578
batch start
#iterations: 66
currently lose_sum: 99.02525913715363
time_elpased: 1.562
batch start
#iterations: 67
currently lose_sum: 99.00339537858963
time_elpased: 1.509
batch start
#iterations: 68
currently lose_sum: 98.9789656996727
time_elpased: 1.576
batch start
#iterations: 69
currently lose_sum: 98.9817880988121
time_elpased: 1.554
batch start
#iterations: 70
currently lose_sum: 98.9330552816391
time_elpased: 1.532
batch start
#iterations: 71
currently lose_sum: 99.05858105421066
time_elpased: 1.58
batch start
#iterations: 72
currently lose_sum: 98.8991037607193
time_elpased: 1.535
batch start
#iterations: 73
currently lose_sum: 99.04261994361877
time_elpased: 1.553
batch start
#iterations: 74
currently lose_sum: 99.15248310565948
time_elpased: 1.61
batch start
#iterations: 75
currently lose_sum: 98.92910677194595
time_elpased: 1.533
batch start
#iterations: 76
currently lose_sum: 99.11199361085892
time_elpased: 1.576
batch start
#iterations: 77
currently lose_sum: 99.0735302567482
time_elpased: 1.564
batch start
#iterations: 78
currently lose_sum: 98.92432403564453
time_elpased: 1.562
batch start
#iterations: 79
currently lose_sum: 99.05350452661514
time_elpased: 1.593
start validation test
0.600463917526
0.651184251
0.435731192755
0.522103705531
0.600753131232
65.153
batch start
#iterations: 80
currently lose_sum: 98.92049705982208
time_elpased: 1.547
batch start
#iterations: 81
currently lose_sum: 98.92131870985031
time_elpased: 1.569
batch start
#iterations: 82
currently lose_sum: 99.03288567066193
time_elpased: 1.572
batch start
#iterations: 83
currently lose_sum: 98.81116634607315
time_elpased: 1.547
batch start
#iterations: 84
currently lose_sum: 98.97278100252151
time_elpased: 1.501
batch start
#iterations: 85
currently lose_sum: 98.91668671369553
time_elpased: 1.535
batch start
#iterations: 86
currently lose_sum: 98.86789584159851
time_elpased: 1.54
batch start
#iterations: 87
currently lose_sum: 99.16728436946869
time_elpased: 1.546
batch start
#iterations: 88
currently lose_sum: 98.73079043626785
time_elpased: 1.551
batch start
#iterations: 89
currently lose_sum: 98.90121775865555
time_elpased: 1.555
batch start
#iterations: 90
currently lose_sum: 99.06124359369278
time_elpased: 1.572
batch start
#iterations: 91
currently lose_sum: 99.06246894598007
time_elpased: 1.572
batch start
#iterations: 92
currently lose_sum: 98.72845697402954
time_elpased: 1.517
batch start
#iterations: 93
currently lose_sum: 98.9751763343811
time_elpased: 1.57
batch start
#iterations: 94
currently lose_sum: 98.81968927383423
time_elpased: 1.592
batch start
#iterations: 95
currently lose_sum: 98.80159914493561
time_elpased: 1.534
batch start
#iterations: 96
currently lose_sum: 98.74159616231918
time_elpased: 1.535
batch start
#iterations: 97
currently lose_sum: 98.83303344249725
time_elpased: 1.529
batch start
#iterations: 98
currently lose_sum: 98.78834700584412
time_elpased: 1.524
batch start
#iterations: 99
currently lose_sum: 98.62065571546555
time_elpased: 1.604
start validation test
0.605
0.621022861183
0.542348461459
0.579025435368
0.605109994439
64.659
batch start
#iterations: 100
currently lose_sum: 98.97002482414246
time_elpased: 1.543
batch start
#iterations: 101
currently lose_sum: 98.88294368982315
time_elpased: 1.568
batch start
#iterations: 102
currently lose_sum: 98.86230546236038
time_elpased: 1.553
batch start
#iterations: 103
currently lose_sum: 98.9585713148117
time_elpased: 1.534
batch start
#iterations: 104
currently lose_sum: 98.9182517528534
time_elpased: 1.58
batch start
#iterations: 105
currently lose_sum: 98.74835991859436
time_elpased: 1.56
batch start
#iterations: 106
currently lose_sum: 98.7731751203537
time_elpased: 1.574
batch start
#iterations: 107
currently lose_sum: 99.02189660072327
time_elpased: 1.536
batch start
#iterations: 108
currently lose_sum: 98.77219700813293
time_elpased: 1.611
batch start
#iterations: 109
currently lose_sum: 98.93112927675247
time_elpased: 1.55
batch start
#iterations: 110
currently lose_sum: 98.7708740234375
time_elpased: 1.627
batch start
#iterations: 111
currently lose_sum: 98.67204564809799
time_elpased: 1.59
batch start
#iterations: 112
currently lose_sum: 98.84537196159363
time_elpased: 1.556
batch start
#iterations: 113
currently lose_sum: 98.8708027601242
time_elpased: 1.562
batch start
#iterations: 114
currently lose_sum: 98.78440076112747
time_elpased: 1.561
batch start
#iterations: 115
currently lose_sum: 98.95702177286148
time_elpased: 1.562
batch start
#iterations: 116
currently lose_sum: 98.63192594051361
time_elpased: 1.529
batch start
#iterations: 117
currently lose_sum: 98.65294802188873
time_elpased: 1.576
batch start
#iterations: 118
currently lose_sum: 98.81093347072601
time_elpased: 1.56
batch start
#iterations: 119
currently lose_sum: 98.96746677160263
time_elpased: 1.541
start validation test
0.599793814433
0.632766825289
0.478954409797
0.545220243674
0.600005966646
64.781
batch start
#iterations: 120
currently lose_sum: 98.57666820287704
time_elpased: 1.586
batch start
#iterations: 121
currently lose_sum: 98.84359365701675
time_elpased: 1.621
batch start
#iterations: 122
currently lose_sum: 98.9138171672821
time_elpased: 1.596
batch start
#iterations: 123
currently lose_sum: 98.99223691225052
time_elpased: 1.542
batch start
#iterations: 124
currently lose_sum: 99.03237968683243
time_elpased: 1.539
batch start
#iterations: 125
currently lose_sum: 98.81157833337784
time_elpased: 1.547
batch start
#iterations: 126
currently lose_sum: 98.8815386891365
time_elpased: 1.565
batch start
#iterations: 127
currently lose_sum: 98.92625534534454
time_elpased: 1.557
batch start
#iterations: 128
currently lose_sum: 98.89103668928146
time_elpased: 1.572
batch start
#iterations: 129
currently lose_sum: 98.97852778434753
time_elpased: 1.53
batch start
#iterations: 130
currently lose_sum: 98.7615818977356
time_elpased: 1.552
batch start
#iterations: 131
currently lose_sum: 98.98768049478531
time_elpased: 1.59
batch start
#iterations: 132
currently lose_sum: 98.89669531583786
time_elpased: 1.532
batch start
#iterations: 133
currently lose_sum: 98.74900096654892
time_elpased: 1.602
batch start
#iterations: 134
currently lose_sum: 98.8044084906578
time_elpased: 1.561
batch start
#iterations: 135
currently lose_sum: 98.64244824647903
time_elpased: 1.603
batch start
#iterations: 136
currently lose_sum: 98.81793159246445
time_elpased: 1.518
batch start
#iterations: 137
currently lose_sum: 98.70491117238998
time_elpased: 1.563
batch start
#iterations: 138
currently lose_sum: 98.86385327577591
time_elpased: 1.556
batch start
#iterations: 139
currently lose_sum: 98.98604506254196
time_elpased: 1.546
start validation test
0.604329896907
0.622936995543
0.532160131728
0.573981573982
0.604456602061
64.650
batch start
#iterations: 140
currently lose_sum: 98.82469433546066
time_elpased: 1.623
batch start
#iterations: 141
currently lose_sum: 98.66912353038788
time_elpased: 1.555
batch start
#iterations: 142
currently lose_sum: 98.84938979148865
time_elpased: 1.556
batch start
#iterations: 143
currently lose_sum: 98.73057025671005
time_elpased: 1.548
batch start
#iterations: 144
currently lose_sum: 98.87907284498215
time_elpased: 1.639
batch start
#iterations: 145
currently lose_sum: 98.64344108104706
time_elpased: 1.516
batch start
#iterations: 146
currently lose_sum: 98.49747687578201
time_elpased: 1.559
batch start
#iterations: 147
currently lose_sum: 98.58047860860825
time_elpased: 1.537
batch start
#iterations: 148
currently lose_sum: 98.89125776290894
time_elpased: 1.576
batch start
#iterations: 149
currently lose_sum: 98.61713749170303
time_elpased: 1.544
batch start
#iterations: 150
currently lose_sum: 98.74846857786179
time_elpased: 1.534
batch start
#iterations: 151
currently lose_sum: 98.78295809030533
time_elpased: 1.558
batch start
#iterations: 152
currently lose_sum: 99.01774209737778
time_elpased: 1.558
batch start
#iterations: 153
currently lose_sum: 98.82230198383331
time_elpased: 1.567
batch start
#iterations: 154
currently lose_sum: 98.84400236606598
time_elpased: 1.555
batch start
#iterations: 155
currently lose_sum: 98.55068039894104
time_elpased: 1.537
batch start
#iterations: 156
currently lose_sum: 98.69603496789932
time_elpased: 1.54
batch start
#iterations: 157
currently lose_sum: 98.61857348680496
time_elpased: 1.54
batch start
#iterations: 158
currently lose_sum: 98.61035650968552
time_elpased: 1.52
batch start
#iterations: 159
currently lose_sum: 98.6985667347908
time_elpased: 1.55
start validation test
0.601237113402
0.632792599544
0.485746629618
0.549604098742
0.60143987476
64.745
batch start
#iterations: 160
currently lose_sum: 98.75771963596344
time_elpased: 1.559
batch start
#iterations: 161
currently lose_sum: 98.83114582300186
time_elpased: 1.575
batch start
#iterations: 162
currently lose_sum: 98.7903528213501
time_elpased: 1.567
batch start
#iterations: 163
currently lose_sum: 98.75417685508728
time_elpased: 1.578
batch start
#iterations: 164
currently lose_sum: 98.47569245100021
time_elpased: 1.588
batch start
#iterations: 165
currently lose_sum: 98.85668343305588
time_elpased: 1.63
batch start
#iterations: 166
currently lose_sum: 98.64346218109131
time_elpased: 1.607
batch start
#iterations: 167
currently lose_sum: 98.79888552427292
time_elpased: 1.576
batch start
#iterations: 168
currently lose_sum: 98.64593154191971
time_elpased: 1.543
batch start
#iterations: 169
currently lose_sum: 98.67358410358429
time_elpased: 1.602
batch start
#iterations: 170
currently lose_sum: 98.8475016951561
time_elpased: 1.583
batch start
#iterations: 171
currently lose_sum: 98.80585461854935
time_elpased: 1.554
batch start
#iterations: 172
currently lose_sum: 98.74326741695404
time_elpased: 1.55
batch start
#iterations: 173
currently lose_sum: 98.80940473079681
time_elpased: 1.633
batch start
#iterations: 174
currently lose_sum: 98.40963959693909
time_elpased: 1.565
batch start
#iterations: 175
currently lose_sum: 98.62458378076553
time_elpased: 1.558
batch start
#iterations: 176
currently lose_sum: 98.63769882917404
time_elpased: 1.551
batch start
#iterations: 177
currently lose_sum: 98.58956784009933
time_elpased: 1.608
batch start
#iterations: 178
currently lose_sum: 98.76791661977768
time_elpased: 1.593
batch start
#iterations: 179
currently lose_sum: 98.64264333248138
time_elpased: 1.555
start validation test
0.607113402062
0.625946855837
0.535762066481
0.577353887102
0.607238670337
64.611
batch start
#iterations: 180
currently lose_sum: 98.80094069242477
time_elpased: 1.54
batch start
#iterations: 181
currently lose_sum: 98.53777259588242
time_elpased: 1.578
batch start
#iterations: 182
currently lose_sum: 98.74746698141098
time_elpased: 1.538
batch start
#iterations: 183
currently lose_sum: 98.55850785970688
time_elpased: 1.608
batch start
#iterations: 184
currently lose_sum: 98.80729240179062
time_elpased: 1.558
batch start
#iterations: 185
currently lose_sum: 98.54460722208023
time_elpased: 1.539
batch start
#iterations: 186
currently lose_sum: 98.84715974330902
time_elpased: 1.572
batch start
#iterations: 187
currently lose_sum: 98.57432532310486
time_elpased: 1.582
batch start
#iterations: 188
currently lose_sum: 98.72290056943893
time_elpased: 1.62
batch start
#iterations: 189
currently lose_sum: 98.83371049165726
time_elpased: 1.564
batch start
#iterations: 190
currently lose_sum: 98.63632327318192
time_elpased: 1.543
batch start
#iterations: 191
currently lose_sum: 98.6368533372879
time_elpased: 1.605
batch start
#iterations: 192
currently lose_sum: 98.71853405237198
time_elpased: 1.54
batch start
#iterations: 193
currently lose_sum: 98.60628265142441
time_elpased: 1.562
batch start
#iterations: 194
currently lose_sum: 98.38000911474228
time_elpased: 1.524
batch start
#iterations: 195
currently lose_sum: 98.82821720838547
time_elpased: 1.557
batch start
#iterations: 196
currently lose_sum: 98.55155175924301
time_elpased: 1.571
batch start
#iterations: 197
currently lose_sum: 98.65311986207962
time_elpased: 1.563
batch start
#iterations: 198
currently lose_sum: 98.67824220657349
time_elpased: 1.548
batch start
#iterations: 199
currently lose_sum: 98.54724365472794
time_elpased: 1.571
start validation test
0.602010309278
0.629644063393
0.498816507152
0.55664656905
0.602191481915
64.585
batch start
#iterations: 200
currently lose_sum: 98.74476474523544
time_elpased: 1.559
batch start
#iterations: 201
currently lose_sum: 98.76565724611282
time_elpased: 1.534
batch start
#iterations: 202
currently lose_sum: 98.49582099914551
time_elpased: 1.567
batch start
#iterations: 203
currently lose_sum: 98.56196278333664
time_elpased: 1.539
batch start
#iterations: 204
currently lose_sum: 98.49443143606186
time_elpased: 1.541
batch start
#iterations: 205
currently lose_sum: 98.56060141324997
time_elpased: 1.607
batch start
#iterations: 206
currently lose_sum: 98.60784673690796
time_elpased: 1.536
batch start
#iterations: 207
currently lose_sum: 98.70045042037964
time_elpased: 1.611
batch start
#iterations: 208
currently lose_sum: 98.50677412748337
time_elpased: 1.58
batch start
#iterations: 209
currently lose_sum: 98.67249345779419
time_elpased: 1.559
batch start
#iterations: 210
currently lose_sum: 98.53726899623871
time_elpased: 1.563
batch start
#iterations: 211
currently lose_sum: 98.61861455440521
time_elpased: 1.62
batch start
#iterations: 212
currently lose_sum: 98.5434741973877
time_elpased: 1.574
batch start
#iterations: 213
currently lose_sum: 98.76181858778
time_elpased: 1.567
batch start
#iterations: 214
currently lose_sum: 98.59942555427551
time_elpased: 1.593
batch start
#iterations: 215
currently lose_sum: 98.57720184326172
time_elpased: 1.573
batch start
#iterations: 216
currently lose_sum: 98.5593296289444
time_elpased: 1.537
batch start
#iterations: 217
currently lose_sum: 98.36525297164917
time_elpased: 1.538
batch start
#iterations: 218
currently lose_sum: 98.31737339496613
time_elpased: 1.561
batch start
#iterations: 219
currently lose_sum: 98.78219044208527
time_elpased: 1.553
start validation test
0.598865979381
0.639994212126
0.455181640424
0.531994226606
0.5991182394
64.811
batch start
#iterations: 220
currently lose_sum: 98.70455020666122
time_elpased: 1.532
batch start
#iterations: 221
currently lose_sum: 98.49897569417953
time_elpased: 1.579
batch start
#iterations: 222
currently lose_sum: 98.53135597705841
time_elpased: 1.628
batch start
#iterations: 223
currently lose_sum: 98.41389447450638
time_elpased: 1.537
batch start
#iterations: 224
currently lose_sum: 98.4903227686882
time_elpased: 1.589
batch start
#iterations: 225
currently lose_sum: 98.83234816789627
time_elpased: 1.546
batch start
#iterations: 226
currently lose_sum: 98.72573745250702
time_elpased: 1.542
batch start
#iterations: 227
currently lose_sum: 98.49849462509155
time_elpased: 1.57
batch start
#iterations: 228
currently lose_sum: 98.71536481380463
time_elpased: 1.58
batch start
#iterations: 229
currently lose_sum: 98.63010382652283
time_elpased: 1.582
batch start
#iterations: 230
currently lose_sum: 98.49607652425766
time_elpased: 1.534
batch start
#iterations: 231
currently lose_sum: 98.43599438667297
time_elpased: 1.6
batch start
#iterations: 232
currently lose_sum: 98.3543791770935
time_elpased: 1.579
batch start
#iterations: 233
currently lose_sum: 98.68762576580048
time_elpased: 1.558
batch start
#iterations: 234
currently lose_sum: 98.62187057733536
time_elpased: 1.552
batch start
#iterations: 235
currently lose_sum: 98.54418623447418
time_elpased: 1.548
batch start
#iterations: 236
currently lose_sum: 98.74564093351364
time_elpased: 1.626
batch start
#iterations: 237
currently lose_sum: 98.72861564159393
time_elpased: 1.556
batch start
#iterations: 238
currently lose_sum: 98.82243180274963
time_elpased: 1.624
batch start
#iterations: 239
currently lose_sum: 98.48231238126755
time_elpased: 1.533
start validation test
0.603402061856
0.637413394919
0.482865081815
0.549478861693
0.603613683116
64.672
batch start
#iterations: 240
currently lose_sum: 98.59623217582703
time_elpased: 1.558
batch start
#iterations: 241
currently lose_sum: 98.49937689304352
time_elpased: 1.56
batch start
#iterations: 242
currently lose_sum: 98.69251364469528
time_elpased: 1.553
batch start
#iterations: 243
currently lose_sum: 98.81151884794235
time_elpased: 1.603
batch start
#iterations: 244
currently lose_sum: 98.53482687473297
time_elpased: 1.552
batch start
#iterations: 245
currently lose_sum: 98.51251202821732
time_elpased: 1.574
batch start
#iterations: 246
currently lose_sum: 98.79452407360077
time_elpased: 1.578
batch start
#iterations: 247
currently lose_sum: 98.20485037565231
time_elpased: 1.537
batch start
#iterations: 248
currently lose_sum: 98.51283740997314
time_elpased: 1.558
batch start
#iterations: 249
currently lose_sum: 98.44489634037018
time_elpased: 1.575
batch start
#iterations: 250
currently lose_sum: 98.48798125982285
time_elpased: 1.584
batch start
#iterations: 251
currently lose_sum: 98.62615597248077
time_elpased: 1.557
batch start
#iterations: 252
currently lose_sum: 98.7499263882637
time_elpased: 1.55
batch start
#iterations: 253
currently lose_sum: 98.35311019420624
time_elpased: 1.554
batch start
#iterations: 254
currently lose_sum: 98.62333744764328
time_elpased: 1.611
batch start
#iterations: 255
currently lose_sum: 98.6275053024292
time_elpased: 1.556
batch start
#iterations: 256
currently lose_sum: 98.39262461662292
time_elpased: 1.548
batch start
#iterations: 257
currently lose_sum: 98.46707963943481
time_elpased: 1.564
batch start
#iterations: 258
currently lose_sum: 98.69252854585648
time_elpased: 1.586
batch start
#iterations: 259
currently lose_sum: 98.32435923814774
time_elpased: 1.591
start validation test
0.603041237113
0.636659436009
0.483276731501
0.549464693149
0.603251502175
64.428
batch start
#iterations: 260
currently lose_sum: 98.4733510017395
time_elpased: 1.523
batch start
#iterations: 261
currently lose_sum: 98.26290518045425
time_elpased: 1.577
batch start
#iterations: 262
currently lose_sum: 98.48053395748138
time_elpased: 1.54
batch start
#iterations: 263
currently lose_sum: 98.65878456830978
time_elpased: 1.582
batch start
#iterations: 264
currently lose_sum: 98.48830652236938
time_elpased: 1.544
batch start
#iterations: 265
currently lose_sum: 98.59840321540833
time_elpased: 1.562
batch start
#iterations: 266
currently lose_sum: 98.23167264461517
time_elpased: 1.539
batch start
#iterations: 267
currently lose_sum: 98.68659049272537
time_elpased: 1.587
batch start
#iterations: 268
currently lose_sum: 98.56965327262878
time_elpased: 1.54
batch start
#iterations: 269
currently lose_sum: 98.5646248459816
time_elpased: 1.596
batch start
#iterations: 270
currently lose_sum: 98.64385890960693
time_elpased: 1.585
batch start
#iterations: 271
currently lose_sum: 98.62379521131516
time_elpased: 1.544
batch start
#iterations: 272
currently lose_sum: 98.20793133974075
time_elpased: 1.566
batch start
#iterations: 273
currently lose_sum: 98.42886996269226
time_elpased: 1.575
batch start
#iterations: 274
currently lose_sum: 98.64925962686539
time_elpased: 1.594
batch start
#iterations: 275
currently lose_sum: 98.69546735286713
time_elpased: 1.578
batch start
#iterations: 276
currently lose_sum: 98.60649633407593
time_elpased: 1.561
batch start
#iterations: 277
currently lose_sum: 98.4062448143959
time_elpased: 1.598
batch start
#iterations: 278
currently lose_sum: 98.70638799667358
time_elpased: 1.588
batch start
#iterations: 279
currently lose_sum: 98.69181686639786
time_elpased: 1.597
start validation test
0.605670103093
0.640938224465
0.483688381188
0.551319648094
0.60588426082
64.644
batch start
#iterations: 280
currently lose_sum: 98.50667804479599
time_elpased: 1.586
batch start
#iterations: 281
currently lose_sum: 98.47983938455582
time_elpased: 1.546
batch start
#iterations: 282
currently lose_sum: 98.5908254981041
time_elpased: 1.576
batch start
#iterations: 283
currently lose_sum: 98.57763659954071
time_elpased: 1.607
batch start
#iterations: 284
currently lose_sum: 98.5043756365776
time_elpased: 1.582
batch start
#iterations: 285
currently lose_sum: 98.52485203742981
time_elpased: 1.57
batch start
#iterations: 286
currently lose_sum: 98.44725942611694
time_elpased: 1.611
batch start
#iterations: 287
currently lose_sum: 98.71945720911026
time_elpased: 1.61
batch start
#iterations: 288
currently lose_sum: 98.51357018947601
time_elpased: 1.529
batch start
#iterations: 289
currently lose_sum: 98.32175368070602
time_elpased: 1.536
batch start
#iterations: 290
currently lose_sum: 98.67351323366165
time_elpased: 1.565
batch start
#iterations: 291
currently lose_sum: 98.45259362459183
time_elpased: 1.552
batch start
#iterations: 292
currently lose_sum: 98.34314727783203
time_elpased: 1.574
batch start
#iterations: 293
currently lose_sum: 98.62471026182175
time_elpased: 1.547
batch start
#iterations: 294
currently lose_sum: 98.67594414949417
time_elpased: 1.515
batch start
#iterations: 295
currently lose_sum: 98.68481189012527
time_elpased: 1.559
batch start
#iterations: 296
currently lose_sum: 98.2658560872078
time_elpased: 1.529
batch start
#iterations: 297
currently lose_sum: 98.59515565633774
time_elpased: 1.528
batch start
#iterations: 298
currently lose_sum: 98.39213907718658
time_elpased: 1.538
batch start
#iterations: 299
currently lose_sum: 98.50085175037384
time_elpased: 1.622
start validation test
0.603092783505
0.631298008072
0.499022331995
0.557420393149
0.603275495235
64.556
batch start
#iterations: 300
currently lose_sum: 98.41701352596283
time_elpased: 1.544
batch start
#iterations: 301
currently lose_sum: 98.75197339057922
time_elpased: 1.571
batch start
#iterations: 302
currently lose_sum: 98.39160096645355
time_elpased: 1.529
batch start
#iterations: 303
currently lose_sum: 98.6792323589325
time_elpased: 1.548
batch start
#iterations: 304
currently lose_sum: 98.3380417227745
time_elpased: 1.534
batch start
#iterations: 305
currently lose_sum: 98.25103777647018
time_elpased: 1.569
batch start
#iterations: 306
currently lose_sum: 98.5248361825943
time_elpased: 1.58
batch start
#iterations: 307
currently lose_sum: 98.85807073116302
time_elpased: 1.602
batch start
#iterations: 308
currently lose_sum: 98.34851837158203
time_elpased: 1.673
batch start
#iterations: 309
currently lose_sum: 98.47909545898438
time_elpased: 1.568
batch start
#iterations: 310
currently lose_sum: 98.69689077138901
time_elpased: 1.595
batch start
#iterations: 311
currently lose_sum: 98.36152756214142
time_elpased: 1.57
batch start
#iterations: 312
currently lose_sum: 98.50937259197235
time_elpased: 1.589
batch start
#iterations: 313
currently lose_sum: 98.51286906003952
time_elpased: 1.585
batch start
#iterations: 314
currently lose_sum: 98.50479972362518
time_elpased: 1.528
batch start
#iterations: 315
currently lose_sum: 98.42345815896988
time_elpased: 1.572
batch start
#iterations: 316
currently lose_sum: 98.43598055839539
time_elpased: 1.539
batch start
#iterations: 317
currently lose_sum: 98.35291427373886
time_elpased: 1.615
batch start
#iterations: 318
currently lose_sum: 98.58940094709396
time_elpased: 1.589
batch start
#iterations: 319
currently lose_sum: 98.58589482307434
time_elpased: 1.574
start validation test
0.600206185567
0.638664969594
0.464752495626
0.538003335716
0.600443995412
64.791
batch start
#iterations: 320
currently lose_sum: 98.56654113531113
time_elpased: 1.583
batch start
#iterations: 321
currently lose_sum: 98.71571135520935
time_elpased: 1.554
batch start
#iterations: 322
currently lose_sum: 98.45929002761841
time_elpased: 1.551
batch start
#iterations: 323
currently lose_sum: 98.75283604860306
time_elpased: 1.577
batch start
#iterations: 324
currently lose_sum: 98.54817193746567
time_elpased: 1.593
batch start
#iterations: 325
currently lose_sum: 98.71527016162872
time_elpased: 1.56
batch start
#iterations: 326
currently lose_sum: 98.27391386032104
time_elpased: 1.563
batch start
#iterations: 327
currently lose_sum: 98.45744240283966
time_elpased: 1.536
batch start
#iterations: 328
currently lose_sum: 98.63125842809677
time_elpased: 1.53
batch start
#iterations: 329
currently lose_sum: 98.2277324795723
time_elpased: 1.655
batch start
#iterations: 330
currently lose_sum: 98.66968607902527
time_elpased: 1.571
batch start
#iterations: 331
currently lose_sum: 98.29682546854019
time_elpased: 1.561
batch start
#iterations: 332
currently lose_sum: 98.37573403120041
time_elpased: 1.583
batch start
#iterations: 333
currently lose_sum: 98.45503896474838
time_elpased: 1.581
batch start
#iterations: 334
currently lose_sum: 98.46538370847702
time_elpased: 1.572
batch start
#iterations: 335
currently lose_sum: 98.60604518651962
time_elpased: 1.583
batch start
#iterations: 336
currently lose_sum: 98.43532526493073
time_elpased: 1.578
batch start
#iterations: 337
currently lose_sum: 98.72836607694626
time_elpased: 1.563
batch start
#iterations: 338
currently lose_sum: 98.50786113739014
time_elpased: 1.518
batch start
#iterations: 339
currently lose_sum: 98.52498489618301
time_elpased: 1.541
start validation test
0.606391752577
0.62391330237
0.539158176392
0.578447609584
0.60650979149
64.586
batch start
#iterations: 340
currently lose_sum: 98.52190905809402
time_elpased: 1.567
batch start
#iterations: 341
currently lose_sum: 98.5591459274292
time_elpased: 1.537
batch start
#iterations: 342
currently lose_sum: 98.59390962123871
time_elpased: 1.539
batch start
#iterations: 343
currently lose_sum: 98.42102515697479
time_elpased: 1.576
batch start
#iterations: 344
currently lose_sum: 98.53183811903
time_elpased: 1.55
batch start
#iterations: 345
currently lose_sum: 98.56214964389801
time_elpased: 1.612
batch start
#iterations: 346
currently lose_sum: 98.34668350219727
time_elpased: 1.553
batch start
#iterations: 347
currently lose_sum: 98.67981404066086
time_elpased: 1.573
batch start
#iterations: 348
currently lose_sum: 98.40960067510605
time_elpased: 1.541
batch start
#iterations: 349
currently lose_sum: 98.63729238510132
time_elpased: 1.537
batch start
#iterations: 350
currently lose_sum: 98.56868875026703
time_elpased: 1.605
batch start
#iterations: 351
currently lose_sum: 98.46829968690872
time_elpased: 1.552
batch start
#iterations: 352
currently lose_sum: 98.43408840894699
time_elpased: 1.533
batch start
#iterations: 353
currently lose_sum: 98.51857805252075
time_elpased: 1.546
batch start
#iterations: 354
currently lose_sum: 98.60033082962036
time_elpased: 1.582
batch start
#iterations: 355
currently lose_sum: 98.5067395567894
time_elpased: 1.616
batch start
#iterations: 356
currently lose_sum: 98.53239411115646
time_elpased: 1.551
batch start
#iterations: 357
currently lose_sum: 98.58272188901901
time_elpased: 1.542
batch start
#iterations: 358
currently lose_sum: 98.44442784786224
time_elpased: 1.539
batch start
#iterations: 359
currently lose_sum: 98.56316858530045
time_elpased: 1.564
start validation test
0.604690721649
0.63697164259
0.490068951322
0.553946373524
0.604891957846
64.572
batch start
#iterations: 360
currently lose_sum: 98.64947140216827
time_elpased: 1.613
batch start
#iterations: 361
currently lose_sum: 98.46996265649796
time_elpased: 1.556
batch start
#iterations: 362
currently lose_sum: 98.67328119277954
time_elpased: 1.58
batch start
#iterations: 363
currently lose_sum: 98.41976314783096
time_elpased: 1.564
batch start
#iterations: 364
currently lose_sum: 98.38784146308899
time_elpased: 1.526
batch start
#iterations: 365
currently lose_sum: 98.73149991035461
time_elpased: 1.554
batch start
#iterations: 366
currently lose_sum: 98.44951844215393
time_elpased: 1.557
batch start
#iterations: 367
currently lose_sum: 98.46744054555893
time_elpased: 1.553
batch start
#iterations: 368
currently lose_sum: 98.47416973114014
time_elpased: 1.572
batch start
#iterations: 369
currently lose_sum: 98.36230581998825
time_elpased: 1.559
batch start
#iterations: 370
currently lose_sum: 98.5192511677742
time_elpased: 1.559
batch start
#iterations: 371
currently lose_sum: 98.60904318094254
time_elpased: 1.557
batch start
#iterations: 372
currently lose_sum: 98.14046102762222
time_elpased: 1.529
batch start
#iterations: 373
currently lose_sum: 98.51812785863876
time_elpased: 1.544
batch start
#iterations: 374
currently lose_sum: 98.4144880771637
time_elpased: 1.557
batch start
#iterations: 375
currently lose_sum: 98.11237746477127
time_elpased: 1.627
batch start
#iterations: 376
currently lose_sum: 98.58960103988647
time_elpased: 1.615
batch start
#iterations: 377
currently lose_sum: 98.39527750015259
time_elpased: 1.56
batch start
#iterations: 378
currently lose_sum: 98.5791385769844
time_elpased: 1.553
batch start
#iterations: 379
currently lose_sum: 98.25295341014862
time_elpased: 1.574
start validation test
0.605051546392
0.624500181752
0.530410620562
0.573622704508
0.60518259005
64.449
batch start
#iterations: 380
currently lose_sum: 98.7230007648468
time_elpased: 1.528
batch start
#iterations: 381
currently lose_sum: 98.33999049663544
time_elpased: 1.568
batch start
#iterations: 382
currently lose_sum: 98.4111270904541
time_elpased: 1.562
batch start
#iterations: 383
currently lose_sum: 98.56008571386337
time_elpased: 1.548
batch start
#iterations: 384
currently lose_sum: 98.80260491371155
time_elpased: 1.545
batch start
#iterations: 385
currently lose_sum: 98.7813829779625
time_elpased: 1.606
batch start
#iterations: 386
currently lose_sum: 98.53447639942169
time_elpased: 1.52
batch start
#iterations: 387
currently lose_sum: 98.80254852771759
time_elpased: 1.595
batch start
#iterations: 388
currently lose_sum: 98.33933126926422
time_elpased: 1.578
batch start
#iterations: 389
currently lose_sum: 98.53384172916412
time_elpased: 1.601
batch start
#iterations: 390
currently lose_sum: 98.36142438650131
time_elpased: 1.549
batch start
#iterations: 391
currently lose_sum: 98.68615484237671
time_elpased: 1.541
batch start
#iterations: 392
currently lose_sum: 98.41878455877304
time_elpased: 1.526
batch start
#iterations: 393
currently lose_sum: 98.4916962981224
time_elpased: 1.596
batch start
#iterations: 394
currently lose_sum: 98.45112353563309
time_elpased: 1.552
batch start
#iterations: 395
currently lose_sum: 98.41357159614563
time_elpased: 1.569
batch start
#iterations: 396
currently lose_sum: 98.59284293651581
time_elpased: 1.53
batch start
#iterations: 397
currently lose_sum: 98.48344624042511
time_elpased: 1.595
batch start
#iterations: 398
currently lose_sum: 98.56625235080719
time_elpased: 1.594
batch start
#iterations: 399
currently lose_sum: 98.42136806249619
time_elpased: 1.569
start validation test
0.600154639175
0.627604166667
0.496037871771
0.554118526183
0.600337432219
64.528
acc: 0.602
pre: 0.639
rec: 0.475
F1: 0.545
auc: 0.640
