start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.74188596010208
time_elpased: 1.779
batch start
#iterations: 1
currently lose_sum: 100.42067700624466
time_elpased: 1.767
batch start
#iterations: 2
currently lose_sum: 100.41956865787506
time_elpased: 1.762
batch start
#iterations: 3
currently lose_sum: 100.40634828805923
time_elpased: 1.834
batch start
#iterations: 4
currently lose_sum: 100.23221570253372
time_elpased: 1.813
batch start
#iterations: 5
currently lose_sum: 100.37244039773941
time_elpased: 1.78
batch start
#iterations: 6
currently lose_sum: 100.14891064167023
time_elpased: 1.799
batch start
#iterations: 7
currently lose_sum: 100.11628443002701
time_elpased: 1.789
batch start
#iterations: 8
currently lose_sum: 100.0306783914566
time_elpased: 1.829
batch start
#iterations: 9
currently lose_sum: 99.95764166116714
time_elpased: 1.78
batch start
#iterations: 10
currently lose_sum: 99.99991816282272
time_elpased: 1.738
batch start
#iterations: 11
currently lose_sum: 99.85261648893356
time_elpased: 1.796
batch start
#iterations: 12
currently lose_sum: 99.91231769323349
time_elpased: 1.807
batch start
#iterations: 13
currently lose_sum: 99.77953100204468
time_elpased: 1.743
batch start
#iterations: 14
currently lose_sum: 99.63760137557983
time_elpased: 1.77
batch start
#iterations: 15
currently lose_sum: 99.98750185966492
time_elpased: 1.792
batch start
#iterations: 16
currently lose_sum: 99.72243928909302
time_elpased: 1.785
batch start
#iterations: 17
currently lose_sum: 99.8041467666626
time_elpased: 1.788
batch start
#iterations: 18
currently lose_sum: 99.527538895607
time_elpased: 1.783
batch start
#iterations: 19
currently lose_sum: 99.574667096138
time_elpased: 1.822
start validation test
0.588350515464
0.599654576857
0.535967891324
0.566025432018
0.58844248124
65.693
batch start
#iterations: 20
currently lose_sum: 99.53092366456985
time_elpased: 1.767
batch start
#iterations: 21
currently lose_sum: 99.59988337755203
time_elpased: 1.788
batch start
#iterations: 22
currently lose_sum: 99.54021972417831
time_elpased: 1.741
batch start
#iterations: 23
currently lose_sum: 99.52512872219086
time_elpased: 1.848
batch start
#iterations: 24
currently lose_sum: 99.40069192647934
time_elpased: 1.842
batch start
#iterations: 25
currently lose_sum: 99.58577126264572
time_elpased: 1.776
batch start
#iterations: 26
currently lose_sum: 99.400042116642
time_elpased: 1.833
batch start
#iterations: 27
currently lose_sum: 99.44264775514603
time_elpased: 1.766
batch start
#iterations: 28
currently lose_sum: 99.40737074613571
time_elpased: 1.785
batch start
#iterations: 29
currently lose_sum: 99.39495587348938
time_elpased: 1.754
batch start
#iterations: 30
currently lose_sum: 99.34931713342667
time_elpased: 1.775
batch start
#iterations: 31
currently lose_sum: 99.43667566776276
time_elpased: 1.806
batch start
#iterations: 32
currently lose_sum: 99.38184261322021
time_elpased: 1.785
batch start
#iterations: 33
currently lose_sum: 99.39835721254349
time_elpased: 1.783
batch start
#iterations: 34
currently lose_sum: 99.3258541226387
time_elpased: 1.757
batch start
#iterations: 35
currently lose_sum: 99.41517573595047
time_elpased: 1.767
batch start
#iterations: 36
currently lose_sum: 99.01240772008896
time_elpased: 1.838
batch start
#iterations: 37
currently lose_sum: 99.56553041934967
time_elpased: 1.801
batch start
#iterations: 38
currently lose_sum: 99.11710220575333
time_elpased: 1.758
batch start
#iterations: 39
currently lose_sum: 99.26842921972275
time_elpased: 1.797
start validation test
0.602989690722
0.630420711974
0.501183492848
0.558422199289
0.603168427205
65.292
batch start
#iterations: 40
currently lose_sum: 99.38285404443741
time_elpased: 1.787
batch start
#iterations: 41
currently lose_sum: 99.34773766994476
time_elpased: 1.783
batch start
#iterations: 42
currently lose_sum: 99.2799568772316
time_elpased: 1.784
batch start
#iterations: 43
currently lose_sum: 99.0219578742981
time_elpased: 1.758
batch start
#iterations: 44
currently lose_sum: 99.28592020273209
time_elpased: 1.788
batch start
#iterations: 45
currently lose_sum: 99.18834286928177
time_elpased: 1.818
batch start
#iterations: 46
currently lose_sum: 99.10352784395218
time_elpased: 1.809
batch start
#iterations: 47
currently lose_sum: 99.33490073680878
time_elpased: 1.753
batch start
#iterations: 48
currently lose_sum: 99.2398624420166
time_elpased: 1.747
batch start
#iterations: 49
currently lose_sum: 99.28319048881531
time_elpased: 1.734
batch start
#iterations: 50
currently lose_sum: 99.21265143156052
time_elpased: 1.779
batch start
#iterations: 51
currently lose_sum: 99.2844648361206
time_elpased: 1.789
batch start
#iterations: 52
currently lose_sum: 99.41644883155823
time_elpased: 1.784
batch start
#iterations: 53
currently lose_sum: 99.00700384378433
time_elpased: 1.789
batch start
#iterations: 54
currently lose_sum: 99.13976961374283
time_elpased: 1.774
batch start
#iterations: 55
currently lose_sum: 99.33286011219025
time_elpased: 1.798
batch start
#iterations: 56
currently lose_sum: 99.20955830812454
time_elpased: 1.764
batch start
#iterations: 57
currently lose_sum: 98.98628187179565
time_elpased: 1.777
batch start
#iterations: 58
currently lose_sum: 99.09568685293198
time_elpased: 1.759
batch start
#iterations: 59
currently lose_sum: 99.02425003051758
time_elpased: 1.85
start validation test
0.598762886598
0.640296124256
0.453946691366
0.5312537637
0.599017133765
65.218
batch start
#iterations: 60
currently lose_sum: 98.9985893368721
time_elpased: 1.774
batch start
#iterations: 61
currently lose_sum: 99.40517610311508
time_elpased: 1.806
batch start
#iterations: 62
currently lose_sum: 98.97900426387787
time_elpased: 1.757
batch start
#iterations: 63
currently lose_sum: 99.27762180566788
time_elpased: 1.768
batch start
#iterations: 64
currently lose_sum: 99.10685163736343
time_elpased: 1.777
batch start
#iterations: 65
currently lose_sum: 98.94520056247711
time_elpased: 1.757
batch start
#iterations: 66
currently lose_sum: 99.06298059225082
time_elpased: 1.808
batch start
#iterations: 67
currently lose_sum: 99.16488945484161
time_elpased: 1.797
batch start
#iterations: 68
currently lose_sum: 99.01953965425491
time_elpased: 1.775
batch start
#iterations: 69
currently lose_sum: 99.00666773319244
time_elpased: 1.797
batch start
#iterations: 70
currently lose_sum: 99.04148399829865
time_elpased: 1.746
batch start
#iterations: 71
currently lose_sum: 99.06328082084656
time_elpased: 1.766
batch start
#iterations: 72
currently lose_sum: 99.16915833950043
time_elpased: 1.784
batch start
#iterations: 73
currently lose_sum: 98.8323130607605
time_elpased: 1.773
batch start
#iterations: 74
currently lose_sum: 99.0315375328064
time_elpased: 1.734
batch start
#iterations: 75
currently lose_sum: 99.0515034198761
time_elpased: 1.825
batch start
#iterations: 76
currently lose_sum: 99.01211339235306
time_elpased: 1.772
batch start
#iterations: 77
currently lose_sum: 99.03949916362762
time_elpased: 1.746
batch start
#iterations: 78
currently lose_sum: 98.87949293851852
time_elpased: 1.804
batch start
#iterations: 79
currently lose_sum: 98.95958250761032
time_elpased: 1.774
start validation test
0.60293814433
0.632221638655
0.495523309663
0.555587607454
0.60312672764
64.946
batch start
#iterations: 80
currently lose_sum: 98.7905969619751
time_elpased: 1.763
batch start
#iterations: 81
currently lose_sum: 98.84895902872086
time_elpased: 1.806
batch start
#iterations: 82
currently lose_sum: 98.96780174970627
time_elpased: 1.744
batch start
#iterations: 83
currently lose_sum: 99.06325721740723
time_elpased: 1.792
batch start
#iterations: 84
currently lose_sum: 98.88615590333939
time_elpased: 1.747
batch start
#iterations: 85
currently lose_sum: 98.85826879739761
time_elpased: 1.74
batch start
#iterations: 86
currently lose_sum: 99.08641749620438
time_elpased: 1.761
batch start
#iterations: 87
currently lose_sum: 99.07504361867905
time_elpased: 1.754
batch start
#iterations: 88
currently lose_sum: 99.0032212138176
time_elpased: 1.788
batch start
#iterations: 89
currently lose_sum: 98.74582433700562
time_elpased: 1.823
batch start
#iterations: 90
currently lose_sum: 98.85078972578049
time_elpased: 1.755
batch start
#iterations: 91
currently lose_sum: 98.91489154100418
time_elpased: 1.792
batch start
#iterations: 92
currently lose_sum: 98.78954350948334
time_elpased: 1.743
batch start
#iterations: 93
currently lose_sum: 98.92319047451019
time_elpased: 1.792
batch start
#iterations: 94
currently lose_sum: 98.91978645324707
time_elpased: 1.78
batch start
#iterations: 95
currently lose_sum: 98.93077611923218
time_elpased: 1.755
batch start
#iterations: 96
currently lose_sum: 98.84345096349716
time_elpased: 1.806
batch start
#iterations: 97
currently lose_sum: 98.86439847946167
time_elpased: 1.783
batch start
#iterations: 98
currently lose_sum: 98.56630045175552
time_elpased: 1.805
batch start
#iterations: 99
currently lose_sum: 98.78218227624893
time_elpased: 1.809
start validation test
0.598505154639
0.634187082405
0.468868992487
0.539139695876
0.598732750917
65.084
batch start
#iterations: 100
currently lose_sum: 98.91345858573914
time_elpased: 1.78
batch start
#iterations: 101
currently lose_sum: 99.03009670972824
time_elpased: 1.781
batch start
#iterations: 102
currently lose_sum: 98.72396546602249
time_elpased: 1.789
batch start
#iterations: 103
currently lose_sum: 98.96177744865417
time_elpased: 1.768
batch start
#iterations: 104
currently lose_sum: 98.99640989303589
time_elpased: 1.783
batch start
#iterations: 105
currently lose_sum: 98.93459045886993
time_elpased: 1.774
batch start
#iterations: 106
currently lose_sum: 98.88213223218918
time_elpased: 1.772
batch start
#iterations: 107
currently lose_sum: 99.04816108942032
time_elpased: 1.776
batch start
#iterations: 108
currently lose_sum: 98.92851042747498
time_elpased: 1.781
batch start
#iterations: 109
currently lose_sum: 98.74744701385498
time_elpased: 1.791
batch start
#iterations: 110
currently lose_sum: 98.87605959177017
time_elpased: 1.774
batch start
#iterations: 111
currently lose_sum: 98.92371475696564
time_elpased: 1.783
batch start
#iterations: 112
currently lose_sum: 99.01622980833054
time_elpased: 1.812
batch start
#iterations: 113
currently lose_sum: 98.8886610865593
time_elpased: 1.799
batch start
#iterations: 114
currently lose_sum: 98.73012185096741
time_elpased: 1.803
batch start
#iterations: 115
currently lose_sum: 98.81099784374237
time_elpased: 1.808
batch start
#iterations: 116
currently lose_sum: 98.99998956918716
time_elpased: 1.815
batch start
#iterations: 117
currently lose_sum: 99.10196006298065
time_elpased: 1.782
batch start
#iterations: 118
currently lose_sum: 99.02040231227875
time_elpased: 1.79
batch start
#iterations: 119
currently lose_sum: 98.96713131666183
time_elpased: 1.783
start validation test
0.602731958763
0.636809147836
0.481424307914
0.548320928324
0.602944933054
65.034
batch start
#iterations: 120
currently lose_sum: 98.7005627155304
time_elpased: 1.757
batch start
#iterations: 121
currently lose_sum: 98.8270012140274
time_elpased: 1.752
batch start
#iterations: 122
currently lose_sum: 98.93506002426147
time_elpased: 1.813
batch start
#iterations: 123
currently lose_sum: 98.86950445175171
time_elpased: 1.783
batch start
#iterations: 124
currently lose_sum: 98.97261273860931
time_elpased: 1.824
batch start
#iterations: 125
currently lose_sum: 99.03901833295822
time_elpased: 1.784
batch start
#iterations: 126
currently lose_sum: 98.7514335513115
time_elpased: 1.782
batch start
#iterations: 127
currently lose_sum: 98.8656256198883
time_elpased: 1.777
batch start
#iterations: 128
currently lose_sum: 98.66907632350922
time_elpased: 1.792
batch start
#iterations: 129
currently lose_sum: 98.65004241466522
time_elpased: 1.8
batch start
#iterations: 130
currently lose_sum: 99.04231351613998
time_elpased: 1.742
batch start
#iterations: 131
currently lose_sum: 98.65654164552689
time_elpased: 1.766
batch start
#iterations: 132
currently lose_sum: 98.96199834346771
time_elpased: 1.806
batch start
#iterations: 133
currently lose_sum: 98.61766183376312
time_elpased: 1.75
batch start
#iterations: 134
currently lose_sum: 98.73304235935211
time_elpased: 1.795
batch start
#iterations: 135
currently lose_sum: 98.74173039197922
time_elpased: 1.775
batch start
#iterations: 136
currently lose_sum: 98.9545709490776
time_elpased: 1.749
batch start
#iterations: 137
currently lose_sum: 98.818823158741
time_elpased: 1.762
batch start
#iterations: 138
currently lose_sum: 99.00487822294235
time_elpased: 1.732
batch start
#iterations: 139
currently lose_sum: 98.87973040342331
time_elpased: 1.775
start validation test
0.604587628866
0.629822335025
0.51075434805
0.564073421606
0.604752367663
64.943
batch start
#iterations: 140
currently lose_sum: 98.95378637313843
time_elpased: 1.765
batch start
#iterations: 141
currently lose_sum: 98.54609477519989
time_elpased: 1.773
batch start
#iterations: 142
currently lose_sum: 98.97518688440323
time_elpased: 1.812
batch start
#iterations: 143
currently lose_sum: 98.6264471411705
time_elpased: 1.776
batch start
#iterations: 144
currently lose_sum: 98.93797320127487
time_elpased: 1.818
batch start
#iterations: 145
currently lose_sum: 98.79127991199493
time_elpased: 1.801
batch start
#iterations: 146
currently lose_sum: 98.90193903446198
time_elpased: 1.788
batch start
#iterations: 147
currently lose_sum: 98.77627521753311
time_elpased: 1.794
batch start
#iterations: 148
currently lose_sum: 98.79675608873367
time_elpased: 1.836
batch start
#iterations: 149
currently lose_sum: 98.81253319978714
time_elpased: 1.766
batch start
#iterations: 150
currently lose_sum: 98.78082346916199
time_elpased: 1.772
batch start
#iterations: 151
currently lose_sum: 99.04120761156082
time_elpased: 1.834
batch start
#iterations: 152
currently lose_sum: 99.00899720191956
time_elpased: 1.81
batch start
#iterations: 153
currently lose_sum: 98.64298576116562
time_elpased: 1.782
batch start
#iterations: 154
currently lose_sum: 98.8909432888031
time_elpased: 1.764
batch start
#iterations: 155
currently lose_sum: 98.71537965536118
time_elpased: 1.753
batch start
#iterations: 156
currently lose_sum: 98.92803364992142
time_elpased: 1.778
batch start
#iterations: 157
currently lose_sum: 98.96288043260574
time_elpased: 1.745
batch start
#iterations: 158
currently lose_sum: 98.919881939888
time_elpased: 1.823
batch start
#iterations: 159
currently lose_sum: 99.0454888343811
time_elpased: 1.766
start validation test
0.605257731959
0.632105735917
0.506946588453
0.562649914335
0.605430332335
64.911
batch start
#iterations: 160
currently lose_sum: 98.79352301359177
time_elpased: 1.827
batch start
#iterations: 161
currently lose_sum: 98.83956521749496
time_elpased: 1.779
batch start
#iterations: 162
currently lose_sum: 98.62754815816879
time_elpased: 1.77
batch start
#iterations: 163
currently lose_sum: 98.84062772989273
time_elpased: 1.823
batch start
#iterations: 164
currently lose_sum: 98.7573875784874
time_elpased: 1.761
batch start
#iterations: 165
currently lose_sum: 98.98005473613739
time_elpased: 1.858
batch start
#iterations: 166
currently lose_sum: 98.50057369470596
time_elpased: 1.798
batch start
#iterations: 167
currently lose_sum: 98.87808656692505
time_elpased: 1.789
batch start
#iterations: 168
currently lose_sum: 98.7772107720375
time_elpased: 1.731
batch start
#iterations: 169
currently lose_sum: 98.8634569644928
time_elpased: 1.789
batch start
#iterations: 170
currently lose_sum: 98.71372354030609
time_elpased: 1.79
batch start
#iterations: 171
currently lose_sum: 98.81413459777832
time_elpased: 1.813
batch start
#iterations: 172
currently lose_sum: 98.83275908231735
time_elpased: 1.79
batch start
#iterations: 173
currently lose_sum: 98.59953558444977
time_elpased: 1.862
batch start
#iterations: 174
currently lose_sum: 98.91060876846313
time_elpased: 1.766
batch start
#iterations: 175
currently lose_sum: 98.46419423818588
time_elpased: 1.817
batch start
#iterations: 176
currently lose_sum: 98.63158482313156
time_elpased: 1.769
batch start
#iterations: 177
currently lose_sum: 98.85297185182571
time_elpased: 1.762
batch start
#iterations: 178
currently lose_sum: 98.78781723976135
time_elpased: 1.787
batch start
#iterations: 179
currently lose_sum: 98.71790927648544
time_elpased: 1.772
start validation test
0.602680412371
0.629730078781
0.501800967377
0.558533791523
0.602857521796
65.053
batch start
#iterations: 180
currently lose_sum: 98.77215814590454
time_elpased: 1.738
batch start
#iterations: 181
currently lose_sum: 98.90430575609207
time_elpased: 1.777
batch start
#iterations: 182
currently lose_sum: 98.82100075483322
time_elpased: 1.755
batch start
#iterations: 183
currently lose_sum: 98.63092190027237
time_elpased: 1.761
batch start
#iterations: 184
currently lose_sum: 98.62885117530823
time_elpased: 1.799
batch start
#iterations: 185
currently lose_sum: 98.82471668720245
time_elpased: 1.758
batch start
#iterations: 186
currently lose_sum: 98.74475073814392
time_elpased: 1.868
batch start
#iterations: 187
currently lose_sum: 98.63763231039047
time_elpased: 1.817
batch start
#iterations: 188
currently lose_sum: 98.94215333461761
time_elpased: 1.762
batch start
#iterations: 189
currently lose_sum: 98.63710767030716
time_elpased: 1.802
batch start
#iterations: 190
currently lose_sum: 98.80169743299484
time_elpased: 1.812
batch start
#iterations: 191
currently lose_sum: 98.84985214471817
time_elpased: 1.837
batch start
#iterations: 192
currently lose_sum: 98.88963490724564
time_elpased: 1.798
batch start
#iterations: 193
currently lose_sum: 98.856110394001
time_elpased: 1.805
batch start
#iterations: 194
currently lose_sum: 98.70442032814026
time_elpased: 1.789
batch start
#iterations: 195
currently lose_sum: 98.69864350557327
time_elpased: 1.799
batch start
#iterations: 196
currently lose_sum: 98.74382036924362
time_elpased: 1.752
batch start
#iterations: 197
currently lose_sum: 98.60001569986343
time_elpased: 1.77
batch start
#iterations: 198
currently lose_sum: 98.72215884923935
time_elpased: 1.752
batch start
#iterations: 199
currently lose_sum: 98.50166028738022
time_elpased: 1.808
start validation test
0.605824742268
0.636435539151
0.496861171143
0.558053516731
0.606016044624
64.794
batch start
#iterations: 200
currently lose_sum: 98.85007464885712
time_elpased: 1.809
batch start
#iterations: 201
currently lose_sum: 98.59738039970398
time_elpased: 1.753
batch start
#iterations: 202
currently lose_sum: 98.67324990034103
time_elpased: 1.802
batch start
#iterations: 203
currently lose_sum: 98.48384135961533
time_elpased: 1.794
batch start
#iterations: 204
currently lose_sum: 98.84056395292282
time_elpased: 1.795
batch start
#iterations: 205
currently lose_sum: 98.78841197490692
time_elpased: 1.763
batch start
#iterations: 206
currently lose_sum: 98.64687478542328
time_elpased: 1.771
batch start
#iterations: 207
currently lose_sum: 98.56889539957047
time_elpased: 1.831
batch start
#iterations: 208
currently lose_sum: 98.59897792339325
time_elpased: 1.764
batch start
#iterations: 209
currently lose_sum: 98.5216012597084
time_elpased: 1.791
batch start
#iterations: 210
currently lose_sum: 98.55013364553452
time_elpased: 1.787
batch start
#iterations: 211
currently lose_sum: 98.66594475507736
time_elpased: 1.786
batch start
#iterations: 212
currently lose_sum: 98.70137912034988
time_elpased: 1.772
batch start
#iterations: 213
currently lose_sum: 98.74573534727097
time_elpased: 1.79
batch start
#iterations: 214
currently lose_sum: 98.5699200630188
time_elpased: 1.789
batch start
#iterations: 215
currently lose_sum: 98.52049958705902
time_elpased: 1.772
batch start
#iterations: 216
currently lose_sum: 98.56178015470505
time_elpased: 1.86
batch start
#iterations: 217
currently lose_sum: 98.88228034973145
time_elpased: 1.83
batch start
#iterations: 218
currently lose_sum: 98.55437076091766
time_elpased: 1.803
batch start
#iterations: 219
currently lose_sum: 98.89175635576248
time_elpased: 1.778
start validation test
0.600515463918
0.634744485546
0.476793248945
0.544546309356
0.600732677349
65.001
batch start
#iterations: 220
currently lose_sum: 98.57372933626175
time_elpased: 1.783
batch start
#iterations: 221
currently lose_sum: 98.56312501430511
time_elpased: 1.784
batch start
#iterations: 222
currently lose_sum: 98.58740454912186
time_elpased: 1.808
batch start
#iterations: 223
currently lose_sum: 98.44818305969238
time_elpased: 1.822
batch start
#iterations: 224
currently lose_sum: 98.68539929389954
time_elpased: 1.788
batch start
#iterations: 225
currently lose_sum: 98.790372133255
time_elpased: 1.784
batch start
#iterations: 226
currently lose_sum: 98.57228529453278
time_elpased: 1.802
batch start
#iterations: 227
currently lose_sum: 98.75701266527176
time_elpased: 1.762
batch start
#iterations: 228
currently lose_sum: 98.77016150951385
time_elpased: 1.811
batch start
#iterations: 229
currently lose_sum: 98.64454966783524
time_elpased: 1.817
batch start
#iterations: 230
currently lose_sum: 98.64931327104568
time_elpased: 1.761
batch start
#iterations: 231
currently lose_sum: 98.60975879430771
time_elpased: 1.767
batch start
#iterations: 232
currently lose_sum: 98.73983520269394
time_elpased: 1.774
batch start
#iterations: 233
currently lose_sum: 98.68591636419296
time_elpased: 1.767
batch start
#iterations: 234
currently lose_sum: 98.48779118061066
time_elpased: 1.773
batch start
#iterations: 235
currently lose_sum: 98.73392021656036
time_elpased: 1.847
batch start
#iterations: 236
currently lose_sum: 98.89848911762238
time_elpased: 1.78
batch start
#iterations: 237
currently lose_sum: 98.83796268701553
time_elpased: 1.816
batch start
#iterations: 238
currently lose_sum: 98.772565305233
time_elpased: 1.769
batch start
#iterations: 239
currently lose_sum: 98.79254621267319
time_elpased: 1.834
start validation test
0.602577319588
0.633390934468
0.490377688587
0.552784222738
0.602774303345
64.948
batch start
#iterations: 240
currently lose_sum: 98.46179336309433
time_elpased: 1.786
batch start
#iterations: 241
currently lose_sum: 98.75591313838959
time_elpased: 1.84
batch start
#iterations: 242
currently lose_sum: 98.74358016252518
time_elpased: 1.759
batch start
#iterations: 243
currently lose_sum: 98.90433180332184
time_elpased: 1.777
batch start
#iterations: 244
currently lose_sum: 98.55140888690948
time_elpased: 1.762
batch start
#iterations: 245
currently lose_sum: 98.6993059515953
time_elpased: 1.798
batch start
#iterations: 246
currently lose_sum: 98.72162955999374
time_elpased: 1.754
batch start
#iterations: 247
currently lose_sum: 98.61803889274597
time_elpased: 1.813
batch start
#iterations: 248
currently lose_sum: 98.55620259046555
time_elpased: 1.811
batch start
#iterations: 249
currently lose_sum: 98.74469238519669
time_elpased: 1.779
batch start
#iterations: 250
currently lose_sum: 98.67500978708267
time_elpased: 1.767
batch start
#iterations: 251
currently lose_sum: 98.68342834711075
time_elpased: 1.811
batch start
#iterations: 252
currently lose_sum: 98.2490553855896
time_elpased: 1.758
batch start
#iterations: 253
currently lose_sum: 98.85287582874298
time_elpased: 1.79
batch start
#iterations: 254
currently lose_sum: 98.4315938949585
time_elpased: 1.794
batch start
#iterations: 255
currently lose_sum: 98.5363420844078
time_elpased: 1.789
batch start
#iterations: 256
currently lose_sum: 98.55045413970947
time_elpased: 1.802
batch start
#iterations: 257
currently lose_sum: 98.5981593132019
time_elpased: 1.831
batch start
#iterations: 258
currently lose_sum: 98.58091402053833
time_elpased: 1.789
batch start
#iterations: 259
currently lose_sum: 98.42175763845444
time_elpased: 1.808
start validation test
0.605257731959
0.623871976898
0.533600905629
0.575216330153
0.60538353657
64.725
batch start
#iterations: 260
currently lose_sum: 98.69750136137009
time_elpased: 1.787
batch start
#iterations: 261
currently lose_sum: 98.4954446554184
time_elpased: 1.796
batch start
#iterations: 262
currently lose_sum: 98.3366973400116
time_elpased: 1.76
batch start
#iterations: 263
currently lose_sum: 98.61783146858215
time_elpased: 1.792
batch start
#iterations: 264
currently lose_sum: 98.85768216848373
time_elpased: 1.78
batch start
#iterations: 265
currently lose_sum: 98.79257363080978
time_elpased: 1.795
batch start
#iterations: 266
currently lose_sum: 98.6903390288353
time_elpased: 1.744
batch start
#iterations: 267
currently lose_sum: 98.43489772081375
time_elpased: 1.797
batch start
#iterations: 268
currently lose_sum: 98.62916910648346
time_elpased: 1.77
batch start
#iterations: 269
currently lose_sum: 98.77911585569382
time_elpased: 1.793
batch start
#iterations: 270
currently lose_sum: 98.64616930484772
time_elpased: 1.787
batch start
#iterations: 271
currently lose_sum: 98.5815799832344
time_elpased: 1.797
batch start
#iterations: 272
currently lose_sum: 98.65599912405014
time_elpased: 1.803
batch start
#iterations: 273
currently lose_sum: 98.63349395990372
time_elpased: 1.776
batch start
#iterations: 274
currently lose_sum: 98.52309024333954
time_elpased: 1.761
batch start
#iterations: 275
currently lose_sum: 98.84380012750626
time_elpased: 1.763
batch start
#iterations: 276
currently lose_sum: 98.63322097063065
time_elpased: 1.801
batch start
#iterations: 277
currently lose_sum: 98.50735872983932
time_elpased: 1.755
batch start
#iterations: 278
currently lose_sum: 98.45058727264404
time_elpased: 1.758
batch start
#iterations: 279
currently lose_sum: 98.64323061704636
time_elpased: 1.811
start validation test
0.605721649485
0.631485249237
0.511063085314
0.564928047324
0.605887837194
64.737
batch start
#iterations: 280
currently lose_sum: 98.52948415279388
time_elpased: 1.753
batch start
#iterations: 281
currently lose_sum: 98.54155504703522
time_elpased: 1.784
batch start
#iterations: 282
currently lose_sum: 98.69915664196014
time_elpased: 1.777
batch start
#iterations: 283
currently lose_sum: 98.67883110046387
time_elpased: 1.783
batch start
#iterations: 284
currently lose_sum: 98.41875517368317
time_elpased: 1.771
batch start
#iterations: 285
currently lose_sum: 98.52845931053162
time_elpased: 1.811
batch start
#iterations: 286
currently lose_sum: 98.51010763645172
time_elpased: 1.832
batch start
#iterations: 287
currently lose_sum: 98.49928855895996
time_elpased: 1.761
batch start
#iterations: 288
currently lose_sum: 98.37836146354675
time_elpased: 1.713
batch start
#iterations: 289
currently lose_sum: 98.58060491085052
time_elpased: 1.781
batch start
#iterations: 290
currently lose_sum: 98.82652485370636
time_elpased: 1.795
batch start
#iterations: 291
currently lose_sum: 98.55965203046799
time_elpased: 1.777
batch start
#iterations: 292
currently lose_sum: 98.25728529691696
time_elpased: 1.755
batch start
#iterations: 293
currently lose_sum: 98.76310294866562
time_elpased: 1.776
batch start
#iterations: 294
currently lose_sum: 98.63173288106918
time_elpased: 1.835
batch start
#iterations: 295
currently lose_sum: 98.5767513513565
time_elpased: 1.802
batch start
#iterations: 296
currently lose_sum: 98.64450186491013
time_elpased: 1.756
batch start
#iterations: 297
currently lose_sum: 98.53244692087173
time_elpased: 1.759
batch start
#iterations: 298
currently lose_sum: 98.41360348463058
time_elpased: 1.811
batch start
#iterations: 299
currently lose_sum: 98.6533915400505
time_elpased: 1.768
start validation test
0.605670103093
0.631572246976
0.510548523207
0.56464830412
0.605837103698
64.845
batch start
#iterations: 300
currently lose_sum: 98.4245954155922
time_elpased: 1.757
batch start
#iterations: 301
currently lose_sum: 98.57870024442673
time_elpased: 1.762
batch start
#iterations: 302
currently lose_sum: 98.46166288852692
time_elpased: 1.806
batch start
#iterations: 303
currently lose_sum: 98.53518283367157
time_elpased: 1.793
batch start
#iterations: 304
currently lose_sum: 98.52879452705383
time_elpased: 1.785
batch start
#iterations: 305
currently lose_sum: 98.75599217414856
time_elpased: 1.792
batch start
#iterations: 306
currently lose_sum: 98.32805675268173
time_elpased: 1.816
batch start
#iterations: 307
currently lose_sum: 98.6707614660263
time_elpased: 1.785
batch start
#iterations: 308
currently lose_sum: 98.39770364761353
time_elpased: 1.753
batch start
#iterations: 309
currently lose_sum: 98.77882027626038
time_elpased: 1.776
batch start
#iterations: 310
currently lose_sum: 98.6266793012619
time_elpased: 1.76
batch start
#iterations: 311
currently lose_sum: 98.53464102745056
time_elpased: 1.799
batch start
#iterations: 312
currently lose_sum: 98.60947805643082
time_elpased: 1.767
batch start
#iterations: 313
currently lose_sum: 98.48737072944641
time_elpased: 1.778
batch start
#iterations: 314
currently lose_sum: 98.64809691905975
time_elpased: 1.784
batch start
#iterations: 315
currently lose_sum: 98.60894817113876
time_elpased: 1.773
batch start
#iterations: 316
currently lose_sum: 98.48898601531982
time_elpased: 1.766
batch start
#iterations: 317
currently lose_sum: 98.75746291875839
time_elpased: 1.763
batch start
#iterations: 318
currently lose_sum: 98.71000981330872
time_elpased: 1.762
batch start
#iterations: 319
currently lose_sum: 98.85480839014053
time_elpased: 1.753
start validation test
0.606701030928
0.631274374135
0.516414531234
0.568096909317
0.606859542804
64.793
batch start
#iterations: 320
currently lose_sum: 98.42990988492966
time_elpased: 1.784
batch start
#iterations: 321
currently lose_sum: 98.6100680232048
time_elpased: 1.74
batch start
#iterations: 322
currently lose_sum: 98.69407516717911
time_elpased: 1.765
batch start
#iterations: 323
currently lose_sum: 98.68573486804962
time_elpased: 1.801
batch start
#iterations: 324
currently lose_sum: 98.67544108629227
time_elpased: 1.775
batch start
#iterations: 325
currently lose_sum: 98.7282487154007
time_elpased: 1.754
batch start
#iterations: 326
currently lose_sum: 98.62692093849182
time_elpased: 1.786
batch start
#iterations: 327
currently lose_sum: 98.59040802717209
time_elpased: 1.757
batch start
#iterations: 328
currently lose_sum: 98.42624789476395
time_elpased: 1.763
batch start
#iterations: 329
currently lose_sum: 98.3764198422432
time_elpased: 1.782
batch start
#iterations: 330
currently lose_sum: 98.53144264221191
time_elpased: 1.795
batch start
#iterations: 331
currently lose_sum: 98.5947767496109
time_elpased: 1.769
batch start
#iterations: 332
currently lose_sum: 98.46540862321854
time_elpased: 1.776
batch start
#iterations: 333
currently lose_sum: 98.50830119848251
time_elpased: 1.787
batch start
#iterations: 334
currently lose_sum: 98.62140655517578
time_elpased: 1.758
batch start
#iterations: 335
currently lose_sum: 98.55128490924835
time_elpased: 1.764
batch start
#iterations: 336
currently lose_sum: 98.5142030119896
time_elpased: 1.762
batch start
#iterations: 337
currently lose_sum: 98.32735812664032
time_elpased: 1.773
batch start
#iterations: 338
currently lose_sum: 98.59050112962723
time_elpased: 1.801
batch start
#iterations: 339
currently lose_sum: 98.67109322547913
time_elpased: 1.784
start validation test
0.603659793814
0.63882803943
0.480189358856
0.548263909289
0.603876565207
64.944
batch start
#iterations: 340
currently lose_sum: 98.79817807674408
time_elpased: 1.76
batch start
#iterations: 341
currently lose_sum: 98.90397691726685
time_elpased: 1.774
batch start
#iterations: 342
currently lose_sum: 98.51420432329178
time_elpased: 1.785
batch start
#iterations: 343
currently lose_sum: 98.586669921875
time_elpased: 1.817
batch start
#iterations: 344
currently lose_sum: 98.68012768030167
time_elpased: 1.778
batch start
#iterations: 345
currently lose_sum: 98.61797654628754
time_elpased: 1.78
batch start
#iterations: 346
currently lose_sum: 98.49695777893066
time_elpased: 1.797
batch start
#iterations: 347
currently lose_sum: 98.72561556100845
time_elpased: 1.798
batch start
#iterations: 348
currently lose_sum: 98.70037722587585
time_elpased: 1.763
batch start
#iterations: 349
currently lose_sum: 98.60070157051086
time_elpased: 1.782
batch start
#iterations: 350
currently lose_sum: 98.68199825286865
time_elpased: 1.743
batch start
#iterations: 351
currently lose_sum: 98.74735140800476
time_elpased: 1.762
batch start
#iterations: 352
currently lose_sum: 98.5411850810051
time_elpased: 1.808
batch start
#iterations: 353
currently lose_sum: 98.70913797616959
time_elpased: 1.791
batch start
#iterations: 354
currently lose_sum: 98.78460985422134
time_elpased: 1.779
batch start
#iterations: 355
currently lose_sum: 98.72384029626846
time_elpased: 1.785
batch start
#iterations: 356
currently lose_sum: 98.61487179994583
time_elpased: 1.812
batch start
#iterations: 357
currently lose_sum: 98.67269337177277
time_elpased: 1.747
batch start
#iterations: 358
currently lose_sum: 98.68099194765091
time_elpased: 1.777
batch start
#iterations: 359
currently lose_sum: 98.82512193918228
time_elpased: 1.798
start validation test
0.606597938144
0.630133566346
0.51950190388
0.569494584838
0.606750848666
64.818
batch start
#iterations: 360
currently lose_sum: 98.57731610536575
time_elpased: 1.798
batch start
#iterations: 361
currently lose_sum: 98.85398018360138
time_elpased: 1.775
batch start
#iterations: 362
currently lose_sum: 98.5708994269371
time_elpased: 1.796
batch start
#iterations: 363
currently lose_sum: 98.43451637029648
time_elpased: 1.805
batch start
#iterations: 364
currently lose_sum: 98.93131279945374
time_elpased: 1.782
batch start
#iterations: 365
currently lose_sum: 98.49789202213287
time_elpased: 1.781
batch start
#iterations: 366
currently lose_sum: 98.47231501340866
time_elpased: 1.764
batch start
#iterations: 367
currently lose_sum: 98.32803118228912
time_elpased: 1.79
batch start
#iterations: 368
currently lose_sum: 98.57207185029984
time_elpased: 1.786
batch start
#iterations: 369
currently lose_sum: 98.58078998327255
time_elpased: 1.781
batch start
#iterations: 370
currently lose_sum: 98.67480021715164
time_elpased: 1.801
batch start
#iterations: 371
currently lose_sum: 98.61875194311142
time_elpased: 1.8
batch start
#iterations: 372
currently lose_sum: 98.51706421375275
time_elpased: 1.826
batch start
#iterations: 373
currently lose_sum: 98.53967034816742
time_elpased: 1.84
batch start
#iterations: 374
currently lose_sum: 98.60122394561768
time_elpased: 1.778
batch start
#iterations: 375
currently lose_sum: 98.81973803043365
time_elpased: 1.758
batch start
#iterations: 376
currently lose_sum: 98.60137915611267
time_elpased: 1.767
batch start
#iterations: 377
currently lose_sum: 98.44443106651306
time_elpased: 1.803
batch start
#iterations: 378
currently lose_sum: 98.63558983802795
time_elpased: 1.786
batch start
#iterations: 379
currently lose_sum: 98.59794706106186
time_elpased: 1.807
start validation test
0.60324742268
0.633491937616
0.49325923639
0.554649077128
0.603440523906
64.778
batch start
#iterations: 380
currently lose_sum: 98.63292217254639
time_elpased: 1.787
batch start
#iterations: 381
currently lose_sum: 98.3892012834549
time_elpased: 1.767
batch start
#iterations: 382
currently lose_sum: 98.54924213886261
time_elpased: 1.764
batch start
#iterations: 383
currently lose_sum: 98.62533396482468
time_elpased: 1.781
batch start
#iterations: 384
currently lose_sum: 98.38581085205078
time_elpased: 1.813
batch start
#iterations: 385
currently lose_sum: 98.56886500120163
time_elpased: 1.791
batch start
#iterations: 386
currently lose_sum: 98.47856694459915
time_elpased: 1.763
batch start
#iterations: 387
currently lose_sum: 98.55738002061844
time_elpased: 1.804
batch start
#iterations: 388
currently lose_sum: 98.64311534166336
time_elpased: 1.772
batch start
#iterations: 389
currently lose_sum: 98.48929768800735
time_elpased: 1.75
batch start
#iterations: 390
currently lose_sum: 98.62702453136444
time_elpased: 1.794
batch start
#iterations: 391
currently lose_sum: 98.54036700725555
time_elpased: 1.775
batch start
#iterations: 392
currently lose_sum: 98.38041985034943
time_elpased: 1.772
batch start
#iterations: 393
currently lose_sum: 98.551981985569
time_elpased: 1.746
batch start
#iterations: 394
currently lose_sum: 98.30373334884644
time_elpased: 1.809
batch start
#iterations: 395
currently lose_sum: 98.60048973560333
time_elpased: 1.805
batch start
#iterations: 396
currently lose_sum: 98.57429021596909
time_elpased: 1.813
batch start
#iterations: 397
currently lose_sum: 98.52972865104675
time_elpased: 1.83
batch start
#iterations: 398
currently lose_sum: 98.70040863752365
time_elpased: 1.78
batch start
#iterations: 399
currently lose_sum: 98.70967692136765
time_elpased: 1.787
start validation test
0.608144329897
0.627609508869
0.535247504374
0.577760497667
0.608272311518
64.705
acc: 0.602
pre: 0.620
rec: 0.528
F1: 0.571
auc: 0.633
