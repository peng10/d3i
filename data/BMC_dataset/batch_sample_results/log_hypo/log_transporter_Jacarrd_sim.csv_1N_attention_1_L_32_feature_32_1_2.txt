start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.58382165431976
time_elpased: 1.451
batch start
#iterations: 1
currently lose_sum: 100.4361909031868
time_elpased: 1.412
batch start
#iterations: 2
currently lose_sum: 100.32097655534744
time_elpased: 1.459
batch start
#iterations: 3
currently lose_sum: 100.28892642259598
time_elpased: 1.442
batch start
#iterations: 4
currently lose_sum: 100.10555964708328
time_elpased: 1.438
batch start
#iterations: 5
currently lose_sum: 100.0861868262291
time_elpased: 1.422
batch start
#iterations: 6
currently lose_sum: 99.85987627506256
time_elpased: 1.398
batch start
#iterations: 7
currently lose_sum: 99.95853811502457
time_elpased: 1.431
batch start
#iterations: 8
currently lose_sum: 99.89908826351166
time_elpased: 1.478
batch start
#iterations: 9
currently lose_sum: 99.78924232721329
time_elpased: 1.465
batch start
#iterations: 10
currently lose_sum: 99.73762112855911
time_elpased: 1.394
batch start
#iterations: 11
currently lose_sum: 99.97753548622131
time_elpased: 1.392
batch start
#iterations: 12
currently lose_sum: 99.80794185400009
time_elpased: 1.423
batch start
#iterations: 13
currently lose_sum: 99.87511402368546
time_elpased: 1.437
batch start
#iterations: 14
currently lose_sum: 99.7555827498436
time_elpased: 1.409
batch start
#iterations: 15
currently lose_sum: 99.7179691195488
time_elpased: 1.409
batch start
#iterations: 16
currently lose_sum: 99.61677104234695
time_elpased: 1.396
batch start
#iterations: 17
currently lose_sum: 99.69446605443954
time_elpased: 1.479
batch start
#iterations: 18
currently lose_sum: 99.65607815980911
time_elpased: 1.424
batch start
#iterations: 19
currently lose_sum: 99.4470146894455
time_elpased: 1.422
start validation test
0.580360824742
0.611519954713
0.444684573428
0.514925817792
0.580599025328
65.674
batch start
#iterations: 20
currently lose_sum: 99.57084965705872
time_elpased: 1.403
batch start
#iterations: 21
currently lose_sum: 99.54702585935593
time_elpased: 1.436
batch start
#iterations: 22
currently lose_sum: 99.6865205168724
time_elpased: 1.434
batch start
#iterations: 23
currently lose_sum: 99.58448898792267
time_elpased: 1.456
batch start
#iterations: 24
currently lose_sum: 99.39354276657104
time_elpased: 1.421
batch start
#iterations: 25
currently lose_sum: 99.4689090847969
time_elpased: 1.414
batch start
#iterations: 26
currently lose_sum: 99.46698784828186
time_elpased: 1.43
batch start
#iterations: 27
currently lose_sum: 99.3891179561615
time_elpased: 1.39
batch start
#iterations: 28
currently lose_sum: 99.21388214826584
time_elpased: 1.408
batch start
#iterations: 29
currently lose_sum: 99.33627200126648
time_elpased: 1.449
batch start
#iterations: 30
currently lose_sum: 99.34945273399353
time_elpased: 1.44
batch start
#iterations: 31
currently lose_sum: 99.28018128871918
time_elpased: 1.43
batch start
#iterations: 32
currently lose_sum: 99.32532495260239
time_elpased: 1.463
batch start
#iterations: 33
currently lose_sum: 99.21683329343796
time_elpased: 1.467
batch start
#iterations: 34
currently lose_sum: 99.24740672111511
time_elpased: 1.411
batch start
#iterations: 35
currently lose_sum: 99.22731006145477
time_elpased: 1.417
batch start
#iterations: 36
currently lose_sum: 99.29021537303925
time_elpased: 1.422
batch start
#iterations: 37
currently lose_sum: 99.2296512722969
time_elpased: 1.403
batch start
#iterations: 38
currently lose_sum: 99.24060440063477
time_elpased: 1.402
batch start
#iterations: 39
currently lose_sum: 99.2218611240387
time_elpased: 1.431
start validation test
0.584587628866
0.622814814815
0.432643820109
0.510596951479
0.584854389658
65.456
batch start
#iterations: 40
currently lose_sum: 99.1839736700058
time_elpased: 1.43
batch start
#iterations: 41
currently lose_sum: 99.24056643247604
time_elpased: 1.413
batch start
#iterations: 42
currently lose_sum: 99.11450439691544
time_elpased: 1.423
batch start
#iterations: 43
currently lose_sum: 99.14599025249481
time_elpased: 1.395
batch start
#iterations: 44
currently lose_sum: 99.15360623598099
time_elpased: 1.413
batch start
#iterations: 45
currently lose_sum: 99.28196793794632
time_elpased: 1.456
batch start
#iterations: 46
currently lose_sum: 98.88437902927399
time_elpased: 1.437
batch start
#iterations: 47
currently lose_sum: 99.12923806905746
time_elpased: 1.413
batch start
#iterations: 48
currently lose_sum: 99.20610290765762
time_elpased: 1.457
batch start
#iterations: 49
currently lose_sum: 98.92891615629196
time_elpased: 1.428
batch start
#iterations: 50
currently lose_sum: 99.040562748909
time_elpased: 1.466
batch start
#iterations: 51
currently lose_sum: 99.00919878482819
time_elpased: 1.41
batch start
#iterations: 52
currently lose_sum: 99.12918728590012
time_elpased: 1.441
batch start
#iterations: 53
currently lose_sum: 99.09597438573837
time_elpased: 1.482
batch start
#iterations: 54
currently lose_sum: 99.05966657400131
time_elpased: 1.414
batch start
#iterations: 55
currently lose_sum: 99.04547429084778
time_elpased: 1.402
batch start
#iterations: 56
currently lose_sum: 99.04135346412659
time_elpased: 1.41
batch start
#iterations: 57
currently lose_sum: 99.16446661949158
time_elpased: 1.427
batch start
#iterations: 58
currently lose_sum: 99.09373193979263
time_elpased: 1.455
batch start
#iterations: 59
currently lose_sum: 99.06947791576385
time_elpased: 1.41
start validation test
0.587680412371
0.634555137845
0.416898219615
0.503198559096
0.587980246852
65.172
batch start
#iterations: 60
currently lose_sum: 98.8880968093872
time_elpased: 1.444
batch start
#iterations: 61
currently lose_sum: 99.11796146631241
time_elpased: 1.417
batch start
#iterations: 62
currently lose_sum: 98.94163531064987
time_elpased: 1.432
batch start
#iterations: 63
currently lose_sum: 99.22472405433655
time_elpased: 1.506
batch start
#iterations: 64
currently lose_sum: 99.02594649791718
time_elpased: 1.42
batch start
#iterations: 65
currently lose_sum: 98.97450852394104
time_elpased: 1.426
batch start
#iterations: 66
currently lose_sum: 98.82127249240875
time_elpased: 1.423
batch start
#iterations: 67
currently lose_sum: 98.85627937316895
time_elpased: 1.431
batch start
#iterations: 68
currently lose_sum: 99.17855370044708
time_elpased: 1.428
batch start
#iterations: 69
currently lose_sum: 98.88887894153595
time_elpased: 1.401
batch start
#iterations: 70
currently lose_sum: 98.95989990234375
time_elpased: 1.447
batch start
#iterations: 71
currently lose_sum: 99.00502693653107
time_elpased: 1.437
batch start
#iterations: 72
currently lose_sum: 98.96517688035965
time_elpased: 1.48
batch start
#iterations: 73
currently lose_sum: 98.95190554857254
time_elpased: 1.438
batch start
#iterations: 74
currently lose_sum: 99.09670907258987
time_elpased: 1.428
batch start
#iterations: 75
currently lose_sum: 98.99918222427368
time_elpased: 1.464
batch start
#iterations: 76
currently lose_sum: 99.03440940380096
time_elpased: 1.419
batch start
#iterations: 77
currently lose_sum: 99.25466805696487
time_elpased: 1.434
batch start
#iterations: 78
currently lose_sum: 98.85852980613708
time_elpased: 1.415
batch start
#iterations: 79
currently lose_sum: 99.16919749975204
time_elpased: 1.406
start validation test
0.59206185567
0.628108568992
0.454872903159
0.527635191596
0.592302712036
64.982
batch start
#iterations: 80
currently lose_sum: 98.96464914083481
time_elpased: 1.486
batch start
#iterations: 81
currently lose_sum: 98.8447778224945
time_elpased: 1.414
batch start
#iterations: 82
currently lose_sum: 99.10987061262131
time_elpased: 1.434
batch start
#iterations: 83
currently lose_sum: 98.93910264968872
time_elpased: 1.432
batch start
#iterations: 84
currently lose_sum: 98.93073403835297
time_elpased: 1.433
batch start
#iterations: 85
currently lose_sum: 99.01709944009781
time_elpased: 1.407
batch start
#iterations: 86
currently lose_sum: 98.96233397722244
time_elpased: 1.426
batch start
#iterations: 87
currently lose_sum: 99.03492599725723
time_elpased: 1.412
batch start
#iterations: 88
currently lose_sum: 98.87234753370285
time_elpased: 1.419
batch start
#iterations: 89
currently lose_sum: 98.82569295167923
time_elpased: 1.42
batch start
#iterations: 90
currently lose_sum: 98.98557424545288
time_elpased: 1.441
batch start
#iterations: 91
currently lose_sum: 99.22121542692184
time_elpased: 1.412
batch start
#iterations: 92
currently lose_sum: 98.83661687374115
time_elpased: 1.401
batch start
#iterations: 93
currently lose_sum: 98.84744316339493
time_elpased: 1.483
batch start
#iterations: 94
currently lose_sum: 99.02142828702927
time_elpased: 1.426
batch start
#iterations: 95
currently lose_sum: 98.94313114881516
time_elpased: 1.469
batch start
#iterations: 96
currently lose_sum: 98.68470722436905
time_elpased: 1.461
batch start
#iterations: 97
currently lose_sum: 98.71520626544952
time_elpased: 1.411
batch start
#iterations: 98
currently lose_sum: 98.9044697880745
time_elpased: 1.402
batch start
#iterations: 99
currently lose_sum: 98.6551502943039
time_elpased: 1.435
start validation test
0.596494845361
0.637742452968
0.450036019348
0.527693978521
0.59675197642
64.925
batch start
#iterations: 100
currently lose_sum: 98.82201552391052
time_elpased: 1.44
batch start
#iterations: 101
currently lose_sum: 98.89941072463989
time_elpased: 1.413
batch start
#iterations: 102
currently lose_sum: 99.13777434825897
time_elpased: 1.475
batch start
#iterations: 103
currently lose_sum: 98.71681696176529
time_elpased: 1.434
batch start
#iterations: 104
currently lose_sum: 98.97687351703644
time_elpased: 1.445
batch start
#iterations: 105
currently lose_sum: 98.83800864219666
time_elpased: 1.406
batch start
#iterations: 106
currently lose_sum: 98.73723089694977
time_elpased: 1.397
batch start
#iterations: 107
currently lose_sum: 98.89999842643738
time_elpased: 1.425
batch start
#iterations: 108
currently lose_sum: 99.0023962855339
time_elpased: 1.409
batch start
#iterations: 109
currently lose_sum: 98.8810915350914
time_elpased: 1.391
batch start
#iterations: 110
currently lose_sum: 98.77660894393921
time_elpased: 1.414
batch start
#iterations: 111
currently lose_sum: 98.63778901100159
time_elpased: 1.426
batch start
#iterations: 112
currently lose_sum: 98.87167680263519
time_elpased: 1.458
batch start
#iterations: 113
currently lose_sum: 98.76223641633987
time_elpased: 1.409
batch start
#iterations: 114
currently lose_sum: 98.89696913957596
time_elpased: 1.414
batch start
#iterations: 115
currently lose_sum: 98.90596973896027
time_elpased: 1.469
batch start
#iterations: 116
currently lose_sum: 98.88014513254166
time_elpased: 1.404
batch start
#iterations: 117
currently lose_sum: 98.37428706884384
time_elpased: 1.401
batch start
#iterations: 118
currently lose_sum: 98.95663070678711
time_elpased: 1.411
batch start
#iterations: 119
currently lose_sum: 98.99905037879944
time_elpased: 1.402
start validation test
0.60087628866
0.637388641425
0.471235978183
0.541861428318
0.60110389222
64.906
batch start
#iterations: 120
currently lose_sum: 98.8872680068016
time_elpased: 1.418
batch start
#iterations: 121
currently lose_sum: 98.56165558099747
time_elpased: 1.413
batch start
#iterations: 122
currently lose_sum: 99.06378376483917
time_elpased: 1.423
batch start
#iterations: 123
currently lose_sum: 98.90531575679779
time_elpased: 1.415
batch start
#iterations: 124
currently lose_sum: 98.98466283082962
time_elpased: 1.417
batch start
#iterations: 125
currently lose_sum: 99.04573845863342
time_elpased: 1.425
batch start
#iterations: 126
currently lose_sum: 98.74416571855545
time_elpased: 1.427
batch start
#iterations: 127
currently lose_sum: 99.03093028068542
time_elpased: 1.412
batch start
#iterations: 128
currently lose_sum: 98.88750749826431
time_elpased: 1.424
batch start
#iterations: 129
currently lose_sum: 98.98896962404251
time_elpased: 1.428
batch start
#iterations: 130
currently lose_sum: 98.69620740413666
time_elpased: 1.405
batch start
#iterations: 131
currently lose_sum: 98.89234977960587
time_elpased: 1.459
batch start
#iterations: 132
currently lose_sum: 99.00168204307556
time_elpased: 1.401
batch start
#iterations: 133
currently lose_sum: 98.82828849554062
time_elpased: 1.413
batch start
#iterations: 134
currently lose_sum: 98.85946315526962
time_elpased: 1.444
batch start
#iterations: 135
currently lose_sum: 98.62629795074463
time_elpased: 1.437
batch start
#iterations: 136
currently lose_sum: 98.68384450674057
time_elpased: 1.413
batch start
#iterations: 137
currently lose_sum: 98.89387518167496
time_elpased: 1.413
batch start
#iterations: 138
currently lose_sum: 98.79423534870148
time_elpased: 1.461
batch start
#iterations: 139
currently lose_sum: 98.92896264791489
time_elpased: 1.427
start validation test
0.603350515464
0.628266937326
0.509622311413
0.562759247685
0.603515069783
64.712
batch start
#iterations: 140
currently lose_sum: 98.8062509894371
time_elpased: 1.474
batch start
#iterations: 141
currently lose_sum: 98.93899989128113
time_elpased: 1.405
batch start
#iterations: 142
currently lose_sum: 98.71116071939468
time_elpased: 1.408
batch start
#iterations: 143
currently lose_sum: 98.94193029403687
time_elpased: 1.448
batch start
#iterations: 144
currently lose_sum: 98.73080068826675
time_elpased: 1.406
batch start
#iterations: 145
currently lose_sum: 98.73579478263855
time_elpased: 1.431
batch start
#iterations: 146
currently lose_sum: 98.64956188201904
time_elpased: 1.381
batch start
#iterations: 147
currently lose_sum: 98.6142807006836
time_elpased: 1.405
batch start
#iterations: 148
currently lose_sum: 98.61704748868942
time_elpased: 1.535
batch start
#iterations: 149
currently lose_sum: 98.8736383318901
time_elpased: 1.43
batch start
#iterations: 150
currently lose_sum: 98.71607428789139
time_elpased: 1.462
batch start
#iterations: 151
currently lose_sum: 98.75560665130615
time_elpased: 1.431
batch start
#iterations: 152
currently lose_sum: 98.87751525640488
time_elpased: 1.432
batch start
#iterations: 153
currently lose_sum: 98.94746792316437
time_elpased: 1.423
batch start
#iterations: 154
currently lose_sum: 98.97975891828537
time_elpased: 1.416
batch start
#iterations: 155
currently lose_sum: 98.6782591342926
time_elpased: 1.427
batch start
#iterations: 156
currently lose_sum: 98.58668261766434
time_elpased: 1.453
batch start
#iterations: 157
currently lose_sum: 98.60882437229156
time_elpased: 1.438
batch start
#iterations: 158
currently lose_sum: 98.69049537181854
time_elpased: 1.425
batch start
#iterations: 159
currently lose_sum: 98.58204340934753
time_elpased: 1.402
start validation test
0.604432989691
0.623295111648
0.531439744777
0.573714031774
0.604561140591
64.632
batch start
#iterations: 160
currently lose_sum: 98.85853725671768
time_elpased: 1.495
batch start
#iterations: 161
currently lose_sum: 98.74222725629807
time_elpased: 1.43
batch start
#iterations: 162
currently lose_sum: 98.81989669799805
time_elpased: 1.468
batch start
#iterations: 163
currently lose_sum: 98.8824754357338
time_elpased: 1.432
batch start
#iterations: 164
currently lose_sum: 98.58865612745285
time_elpased: 1.411
batch start
#iterations: 165
currently lose_sum: 98.77058839797974
time_elpased: 1.458
batch start
#iterations: 166
currently lose_sum: 98.64148092269897
time_elpased: 1.429
batch start
#iterations: 167
currently lose_sum: 98.96950149536133
time_elpased: 1.39
batch start
#iterations: 168
currently lose_sum: 98.59536397457123
time_elpased: 1.408
batch start
#iterations: 169
currently lose_sum: 98.854745388031
time_elpased: 1.462
batch start
#iterations: 170
currently lose_sum: 98.5878176689148
time_elpased: 1.408
batch start
#iterations: 171
currently lose_sum: 98.95748621225357
time_elpased: 1.419
batch start
#iterations: 172
currently lose_sum: 98.76941519975662
time_elpased: 1.401
batch start
#iterations: 173
currently lose_sum: 98.86214047670364
time_elpased: 1.503
batch start
#iterations: 174
currently lose_sum: 98.63020348548889
time_elpased: 1.419
batch start
#iterations: 175
currently lose_sum: 98.48013997077942
time_elpased: 1.447
batch start
#iterations: 176
currently lose_sum: 98.6831351518631
time_elpased: 1.457
batch start
#iterations: 177
currently lose_sum: 98.66647660732269
time_elpased: 1.456
batch start
#iterations: 178
currently lose_sum: 98.98033607006073
time_elpased: 1.416
batch start
#iterations: 179
currently lose_sum: 98.62513703107834
time_elpased: 1.382
start validation test
0.603711340206
0.611988078154
0.570546464958
0.590541116319
0.60376956626
64.593
batch start
#iterations: 180
currently lose_sum: 98.57933169603348
time_elpased: 1.454
batch start
#iterations: 181
currently lose_sum: 98.76216351985931
time_elpased: 1.415
batch start
#iterations: 182
currently lose_sum: 98.81348931789398
time_elpased: 1.434
batch start
#iterations: 183
currently lose_sum: 98.55814415216446
time_elpased: 1.431
batch start
#iterations: 184
currently lose_sum: 98.8812785744667
time_elpased: 1.401
batch start
#iterations: 185
currently lose_sum: 98.7178720831871
time_elpased: 1.426
batch start
#iterations: 186
currently lose_sum: 98.62757050991058
time_elpased: 1.51
batch start
#iterations: 187
currently lose_sum: 98.62243896722794
time_elpased: 1.396
batch start
#iterations: 188
currently lose_sum: 98.89589458703995
time_elpased: 1.429
batch start
#iterations: 189
currently lose_sum: 98.7599767446518
time_elpased: 1.467
batch start
#iterations: 190
currently lose_sum: 98.77140098810196
time_elpased: 1.405
batch start
#iterations: 191
currently lose_sum: 98.6151847243309
time_elpased: 1.433
batch start
#iterations: 192
currently lose_sum: 98.6071829199791
time_elpased: 1.42
batch start
#iterations: 193
currently lose_sum: 98.85659122467041
time_elpased: 1.409
batch start
#iterations: 194
currently lose_sum: 98.53846317529678
time_elpased: 1.424
batch start
#iterations: 195
currently lose_sum: 98.62559646368027
time_elpased: 1.432
batch start
#iterations: 196
currently lose_sum: 98.73743802309036
time_elpased: 1.401
batch start
#iterations: 197
currently lose_sum: 98.63008522987366
time_elpased: 1.411
batch start
#iterations: 198
currently lose_sum: 98.63886678218842
time_elpased: 1.466
batch start
#iterations: 199
currently lose_sum: 98.73794054985046
time_elpased: 1.443
start validation test
0.604175257732
0.619657116017
0.54306884841
0.578840563813
0.604282539459
64.639
batch start
#iterations: 200
currently lose_sum: 98.72555267810822
time_elpased: 1.421
batch start
#iterations: 201
currently lose_sum: 98.7529228925705
time_elpased: 1.42
batch start
#iterations: 202
currently lose_sum: 98.67430329322815
time_elpased: 1.445
batch start
#iterations: 203
currently lose_sum: 98.57465386390686
time_elpased: 1.498
batch start
#iterations: 204
currently lose_sum: 98.50123697519302
time_elpased: 1.423
batch start
#iterations: 205
currently lose_sum: 98.67035281658173
time_elpased: 1.443
batch start
#iterations: 206
currently lose_sum: 98.47842371463776
time_elpased: 1.445
batch start
#iterations: 207
currently lose_sum: 98.67972457408905
time_elpased: 1.417
batch start
#iterations: 208
currently lose_sum: 98.66945230960846
time_elpased: 1.484
batch start
#iterations: 209
currently lose_sum: 98.7058077454567
time_elpased: 1.414
batch start
#iterations: 210
currently lose_sum: 98.42400652170181
time_elpased: 1.453
batch start
#iterations: 211
currently lose_sum: 98.7714251279831
time_elpased: 1.475
batch start
#iterations: 212
currently lose_sum: 98.42801982164383
time_elpased: 1.416
batch start
#iterations: 213
currently lose_sum: 98.74413377046585
time_elpased: 1.466
batch start
#iterations: 214
currently lose_sum: 98.75003081560135
time_elpased: 1.419
batch start
#iterations: 215
currently lose_sum: 98.65793114900589
time_elpased: 1.528
batch start
#iterations: 216
currently lose_sum: 98.59898573160172
time_elpased: 1.417
batch start
#iterations: 217
currently lose_sum: 98.47679197788239
time_elpased: 1.435
batch start
#iterations: 218
currently lose_sum: 98.33490669727325
time_elpased: 1.406
batch start
#iterations: 219
currently lose_sum: 98.43790262937546
time_elpased: 1.429
start validation test
0.603659793814
0.631859557867
0.500051456211
0.558281151261
0.603841694232
64.716
batch start
#iterations: 220
currently lose_sum: 98.90772843360901
time_elpased: 1.43
batch start
#iterations: 221
currently lose_sum: 98.57568222284317
time_elpased: 1.446
batch start
#iterations: 222
currently lose_sum: 98.52886408567429
time_elpased: 1.494
batch start
#iterations: 223
currently lose_sum: 98.68130719661713
time_elpased: 1.423
batch start
#iterations: 224
currently lose_sum: 98.58354771137238
time_elpased: 1.393
batch start
#iterations: 225
currently lose_sum: 98.5512158870697
time_elpased: 1.413
batch start
#iterations: 226
currently lose_sum: 98.87652742862701
time_elpased: 1.442
batch start
#iterations: 227
currently lose_sum: 98.41312456130981
time_elpased: 1.411
batch start
#iterations: 228
currently lose_sum: 98.81871783733368
time_elpased: 1.446
batch start
#iterations: 229
currently lose_sum: 98.76033854484558
time_elpased: 1.409
batch start
#iterations: 230
currently lose_sum: 98.52006644010544
time_elpased: 1.43
batch start
#iterations: 231
currently lose_sum: 98.57886701822281
time_elpased: 1.428
batch start
#iterations: 232
currently lose_sum: 98.32821649312973
time_elpased: 1.418
batch start
#iterations: 233
currently lose_sum: 98.52459186315536
time_elpased: 1.463
batch start
#iterations: 234
currently lose_sum: 98.50287246704102
time_elpased: 1.453
batch start
#iterations: 235
currently lose_sum: 98.78656500577927
time_elpased: 1.413
batch start
#iterations: 236
currently lose_sum: 98.68435597419739
time_elpased: 1.458
batch start
#iterations: 237
currently lose_sum: 98.70971316099167
time_elpased: 1.411
batch start
#iterations: 238
currently lose_sum: 98.73277294635773
time_elpased: 1.426
batch start
#iterations: 239
currently lose_sum: 98.78273320198059
time_elpased: 1.425
start validation test
0.605309278351
0.630710659898
0.511474735001
0.564869011763
0.605474019364
64.806
batch start
#iterations: 240
currently lose_sum: 98.60273569822311
time_elpased: 1.425
batch start
#iterations: 241
currently lose_sum: 98.6332682967186
time_elpased: 1.41
batch start
#iterations: 242
currently lose_sum: 98.55914551019669
time_elpased: 1.423
batch start
#iterations: 243
currently lose_sum: 98.70640569925308
time_elpased: 1.418
batch start
#iterations: 244
currently lose_sum: 98.66299599409103
time_elpased: 1.436
batch start
#iterations: 245
currently lose_sum: 98.69291669130325
time_elpased: 1.449
batch start
#iterations: 246
currently lose_sum: 98.62569892406464
time_elpased: 1.407
batch start
#iterations: 247
currently lose_sum: 98.49138277769089
time_elpased: 1.423
batch start
#iterations: 248
currently lose_sum: 98.59328585863113
time_elpased: 1.443
batch start
#iterations: 249
currently lose_sum: 98.4642106294632
time_elpased: 1.454
batch start
#iterations: 250
currently lose_sum: 98.407410800457
time_elpased: 1.45
batch start
#iterations: 251
currently lose_sum: 98.65708684921265
time_elpased: 1.427
batch start
#iterations: 252
currently lose_sum: 98.57654464244843
time_elpased: 1.447
batch start
#iterations: 253
currently lose_sum: 98.7380878329277
time_elpased: 1.428
batch start
#iterations: 254
currently lose_sum: 98.51238745450974
time_elpased: 1.419
batch start
#iterations: 255
currently lose_sum: 98.54969781637192
time_elpased: 1.417
batch start
#iterations: 256
currently lose_sum: 98.66067212820053
time_elpased: 1.415
batch start
#iterations: 257
currently lose_sum: 98.34290951490402
time_elpased: 1.417
batch start
#iterations: 258
currently lose_sum: 98.6095524430275
time_elpased: 1.425
batch start
#iterations: 259
currently lose_sum: 98.49516129493713
time_elpased: 1.414
start validation test
0.605412371134
0.629914314516
0.514459195225
0.566362657905
0.605572053463
64.568
batch start
#iterations: 260
currently lose_sum: 98.63383620977402
time_elpased: 1.436
batch start
#iterations: 261
currently lose_sum: 98.2117623090744
time_elpased: 1.401
batch start
#iterations: 262
currently lose_sum: 98.46290224790573
time_elpased: 1.413
batch start
#iterations: 263
currently lose_sum: 98.56159073114395
time_elpased: 1.409
batch start
#iterations: 264
currently lose_sum: 98.58190548419952
time_elpased: 1.434
batch start
#iterations: 265
currently lose_sum: 98.52501082420349
time_elpased: 1.47
batch start
#iterations: 266
currently lose_sum: 98.54130059480667
time_elpased: 1.399
batch start
#iterations: 267
currently lose_sum: 98.58771342039108
time_elpased: 1.429
batch start
#iterations: 268
currently lose_sum: 98.55979603528976
time_elpased: 1.418
batch start
#iterations: 269
currently lose_sum: 98.61148697137833
time_elpased: 1.461
batch start
#iterations: 270
currently lose_sum: 98.71779066324234
time_elpased: 1.434
batch start
#iterations: 271
currently lose_sum: 98.51402753591537
time_elpased: 1.446
batch start
#iterations: 272
currently lose_sum: 98.38886749744415
time_elpased: 1.457
batch start
#iterations: 273
currently lose_sum: 98.28890705108643
time_elpased: 1.448
batch start
#iterations: 274
currently lose_sum: 98.66464191675186
time_elpased: 1.5
batch start
#iterations: 275
currently lose_sum: 98.69354909658432
time_elpased: 1.442
batch start
#iterations: 276
currently lose_sum: 98.74015384912491
time_elpased: 1.412
batch start
#iterations: 277
currently lose_sum: 98.44486200809479
time_elpased: 1.41
batch start
#iterations: 278
currently lose_sum: 98.53066277503967
time_elpased: 1.419
batch start
#iterations: 279
currently lose_sum: 98.68984019756317
time_elpased: 1.43
start validation test
0.604639175258
0.632218059682
0.503653390964
0.560659869401
0.604816471378
64.722
batch start
#iterations: 280
currently lose_sum: 98.61216020584106
time_elpased: 1.45
batch start
#iterations: 281
currently lose_sum: 98.35035467147827
time_elpased: 1.422
batch start
#iterations: 282
currently lose_sum: 98.71416407823563
time_elpased: 1.424
batch start
#iterations: 283
currently lose_sum: 98.50343948602676
time_elpased: 1.422
batch start
#iterations: 284
currently lose_sum: 98.57585746049881
time_elpased: 1.42
batch start
#iterations: 285
currently lose_sum: 98.557872235775
time_elpased: 1.397
batch start
#iterations: 286
currently lose_sum: 98.56587022542953
time_elpased: 1.455
batch start
#iterations: 287
currently lose_sum: 98.53480899333954
time_elpased: 1.426
batch start
#iterations: 288
currently lose_sum: 98.52339297533035
time_elpased: 1.41
batch start
#iterations: 289
currently lose_sum: 98.63895404338837
time_elpased: 1.433
batch start
#iterations: 290
currently lose_sum: 98.43831133842468
time_elpased: 1.445
batch start
#iterations: 291
currently lose_sum: 98.50610488653183
time_elpased: 1.433
batch start
#iterations: 292
currently lose_sum: 98.40740180015564
time_elpased: 1.396
batch start
#iterations: 293
currently lose_sum: 98.5324718952179
time_elpased: 1.43
batch start
#iterations: 294
currently lose_sum: 98.7609674334526
time_elpased: 1.448
batch start
#iterations: 295
currently lose_sum: 98.616395175457
time_elpased: 1.441
batch start
#iterations: 296
currently lose_sum: 98.56018126010895
time_elpased: 1.464
batch start
#iterations: 297
currently lose_sum: 98.39495307207108
time_elpased: 1.412
batch start
#iterations: 298
currently lose_sum: 98.5212197303772
time_elpased: 1.432
batch start
#iterations: 299
currently lose_sum: 98.58616238832474
time_elpased: 1.458
start validation test
0.602113402062
0.62974025974
0.499022331995
0.556812309812
0.602294394336
64.594
batch start
#iterations: 300
currently lose_sum: 98.48442506790161
time_elpased: 1.483
batch start
#iterations: 301
currently lose_sum: 98.64087897539139
time_elpased: 1.42
batch start
#iterations: 302
currently lose_sum: 98.57922202348709
time_elpased: 1.454
batch start
#iterations: 303
currently lose_sum: 98.47369873523712
time_elpased: 1.44
batch start
#iterations: 304
currently lose_sum: 98.44388431310654
time_elpased: 1.431
batch start
#iterations: 305
currently lose_sum: 98.39960491657257
time_elpased: 1.424
batch start
#iterations: 306
currently lose_sum: 98.44223654270172
time_elpased: 1.47
batch start
#iterations: 307
currently lose_sum: 98.76667946577072
time_elpased: 1.43
batch start
#iterations: 308
currently lose_sum: 98.66510546207428
time_elpased: 1.475
batch start
#iterations: 309
currently lose_sum: 98.40259802341461
time_elpased: 1.426
batch start
#iterations: 310
currently lose_sum: 98.62581878900528
time_elpased: 1.426
batch start
#iterations: 311
currently lose_sum: 98.47961360216141
time_elpased: 1.432
batch start
#iterations: 312
currently lose_sum: 98.53947013616562
time_elpased: 1.444
batch start
#iterations: 313
currently lose_sum: 98.4706055521965
time_elpased: 1.456
batch start
#iterations: 314
currently lose_sum: 98.50301086902618
time_elpased: 1.426
batch start
#iterations: 315
currently lose_sum: 98.45592492818832
time_elpased: 1.46
batch start
#iterations: 316
currently lose_sum: 98.3490446805954
time_elpased: 1.436
batch start
#iterations: 317
currently lose_sum: 98.51954901218414
time_elpased: 1.416
batch start
#iterations: 318
currently lose_sum: 98.40935295820236
time_elpased: 1.432
batch start
#iterations: 319
currently lose_sum: 98.73895251750946
time_elpased: 1.414
start validation test
0.604896907216
0.63010398174
0.511371822579
0.564562858604
0.605061104928
64.712
batch start
#iterations: 320
currently lose_sum: 98.50343108177185
time_elpased: 1.437
batch start
#iterations: 321
currently lose_sum: 98.76852929592133
time_elpased: 1.439
batch start
#iterations: 322
currently lose_sum: 98.5473285317421
time_elpased: 1.459
batch start
#iterations: 323
currently lose_sum: 98.6767126917839
time_elpased: 1.413
batch start
#iterations: 324
currently lose_sum: 98.69406086206436
time_elpased: 1.461
batch start
#iterations: 325
currently lose_sum: 98.72550290822983
time_elpased: 1.476
batch start
#iterations: 326
currently lose_sum: 98.43126875162125
time_elpased: 1.454
batch start
#iterations: 327
currently lose_sum: 98.47877389192581
time_elpased: 1.42
batch start
#iterations: 328
currently lose_sum: 98.55649429559708
time_elpased: 1.463
batch start
#iterations: 329
currently lose_sum: 98.51765877008438
time_elpased: 1.437
batch start
#iterations: 330
currently lose_sum: 98.48988038301468
time_elpased: 1.463
batch start
#iterations: 331
currently lose_sum: 98.56231451034546
time_elpased: 1.472
batch start
#iterations: 332
currently lose_sum: 98.25925016403198
time_elpased: 1.486
batch start
#iterations: 333
currently lose_sum: 98.57502692937851
time_elpased: 1.395
batch start
#iterations: 334
currently lose_sum: 98.51888144016266
time_elpased: 1.4
batch start
#iterations: 335
currently lose_sum: 98.5040380358696
time_elpased: 1.428
batch start
#iterations: 336
currently lose_sum: 98.39083456993103
time_elpased: 1.44
batch start
#iterations: 337
currently lose_sum: 98.95760172605515
time_elpased: 1.439
batch start
#iterations: 338
currently lose_sum: 98.44100177288055
time_elpased: 1.445
batch start
#iterations: 339
currently lose_sum: 98.54394906759262
time_elpased: 1.429
start validation test
0.60706185567
0.623496107573
0.543995060204
0.581038746908
0.607172579157
64.619
batch start
#iterations: 340
currently lose_sum: 98.53021347522736
time_elpased: 1.45
batch start
#iterations: 341
currently lose_sum: 98.62476855516434
time_elpased: 1.448
batch start
#iterations: 342
currently lose_sum: 98.64887714385986
time_elpased: 1.429
batch start
#iterations: 343
currently lose_sum: 98.51903361082077
time_elpased: 1.392
batch start
#iterations: 344
currently lose_sum: 98.3423336148262
time_elpased: 1.448
batch start
#iterations: 345
currently lose_sum: 98.7117428779602
time_elpased: 1.399
batch start
#iterations: 346
currently lose_sum: 98.50103783607483
time_elpased: 1.418
batch start
#iterations: 347
currently lose_sum: 98.39696252346039
time_elpased: 1.418
batch start
#iterations: 348
currently lose_sum: 98.57477974891663
time_elpased: 1.418
batch start
#iterations: 349
currently lose_sum: 98.47602355480194
time_elpased: 1.43
batch start
#iterations: 350
currently lose_sum: 98.7458655834198
time_elpased: 1.418
batch start
#iterations: 351
currently lose_sum: 98.616326212883
time_elpased: 1.457
batch start
#iterations: 352
currently lose_sum: 98.46469128131866
time_elpased: 1.419
batch start
#iterations: 353
currently lose_sum: 98.48496282100677
time_elpased: 1.427
batch start
#iterations: 354
currently lose_sum: 98.75743299722672
time_elpased: 1.439
batch start
#iterations: 355
currently lose_sum: 98.43871939182281
time_elpased: 1.413
batch start
#iterations: 356
currently lose_sum: 98.53516358137131
time_elpased: 1.42
batch start
#iterations: 357
currently lose_sum: 98.58443093299866
time_elpased: 1.395
batch start
#iterations: 358
currently lose_sum: 98.42025727033615
time_elpased: 1.442
batch start
#iterations: 359
currently lose_sum: 98.6559294462204
time_elpased: 1.416
start validation test
0.601237113402
0.632508361204
0.48656992899
0.550023266636
0.60143842933
64.681
batch start
#iterations: 360
currently lose_sum: 98.60213142633438
time_elpased: 1.438
batch start
#iterations: 361
currently lose_sum: 98.74517035484314
time_elpased: 1.464
batch start
#iterations: 362
currently lose_sum: 98.49847054481506
time_elpased: 1.413
batch start
#iterations: 363
currently lose_sum: 98.4253198504448
time_elpased: 1.407
batch start
#iterations: 364
currently lose_sum: 98.49660927057266
time_elpased: 1.437
batch start
#iterations: 365
currently lose_sum: 98.56217360496521
time_elpased: 1.425
batch start
#iterations: 366
currently lose_sum: 98.80676168203354
time_elpased: 1.428
batch start
#iterations: 367
currently lose_sum: 98.31501084566116
time_elpased: 1.403
batch start
#iterations: 368
currently lose_sum: 98.56380587816238
time_elpased: 1.461
batch start
#iterations: 369
currently lose_sum: 98.4863908290863
time_elpased: 1.43
batch start
#iterations: 370
currently lose_sum: 98.44032144546509
time_elpased: 1.39
batch start
#iterations: 371
currently lose_sum: 98.60706943273544
time_elpased: 1.476
batch start
#iterations: 372
currently lose_sum: 98.42534577846527
time_elpased: 1.391
batch start
#iterations: 373
currently lose_sum: 98.42799377441406
time_elpased: 1.407
batch start
#iterations: 374
currently lose_sum: 98.46079552173615
time_elpased: 1.425
batch start
#iterations: 375
currently lose_sum: 98.44108814001083
time_elpased: 1.43
batch start
#iterations: 376
currently lose_sum: 98.3070861697197
time_elpased: 1.478
batch start
#iterations: 377
currently lose_sum: 98.43562549352646
time_elpased: 1.406
batch start
#iterations: 378
currently lose_sum: 98.50574773550034
time_elpased: 1.412
batch start
#iterations: 379
currently lose_sum: 98.67785578966141
time_elpased: 1.467
start validation test
0.605412371134
0.622359363874
0.5396727385
0.578074188392
0.605527787199
64.589
batch start
#iterations: 380
currently lose_sum: 98.4004425406456
time_elpased: 1.45
batch start
#iterations: 381
currently lose_sum: 98.42015689611435
time_elpased: 1.41
batch start
#iterations: 382
currently lose_sum: 98.54738914966583
time_elpased: 1.421
batch start
#iterations: 383
currently lose_sum: 98.47487252950668
time_elpased: 1.434
batch start
#iterations: 384
currently lose_sum: 98.81823205947876
time_elpased: 1.392
batch start
#iterations: 385
currently lose_sum: 98.75556242465973
time_elpased: 1.386
batch start
#iterations: 386
currently lose_sum: 98.69426500797272
time_elpased: 1.395
batch start
#iterations: 387
currently lose_sum: 98.59732061624527
time_elpased: 1.425
batch start
#iterations: 388
currently lose_sum: 98.58811259269714
time_elpased: 1.402
batch start
#iterations: 389
currently lose_sum: 98.46402078866959
time_elpased: 1.396
batch start
#iterations: 390
currently lose_sum: 98.44795334339142
time_elpased: 1.414
batch start
#iterations: 391
currently lose_sum: 98.40054792165756
time_elpased: 1.399
batch start
#iterations: 392
currently lose_sum: 98.66908490657806
time_elpased: 1.406
batch start
#iterations: 393
currently lose_sum: 98.50462639331818
time_elpased: 1.457
batch start
#iterations: 394
currently lose_sum: 98.54581052064896
time_elpased: 1.434
batch start
#iterations: 395
currently lose_sum: 98.36022573709488
time_elpased: 1.444
batch start
#iterations: 396
currently lose_sum: 98.5365617275238
time_elpased: 1.438
batch start
#iterations: 397
currently lose_sum: 98.59392249584198
time_elpased: 1.406
batch start
#iterations: 398
currently lose_sum: 98.70133519172668
time_elpased: 1.404
batch start
#iterations: 399
currently lose_sum: 98.34605860710144
time_elpased: 1.439
start validation test
0.603041237113
0.628505864355
0.507358238139
0.561471442401
0.603209223376
64.539
acc: 0.607
pre: 0.635
rec: 0.505
F1: 0.563
auc: 0.640
