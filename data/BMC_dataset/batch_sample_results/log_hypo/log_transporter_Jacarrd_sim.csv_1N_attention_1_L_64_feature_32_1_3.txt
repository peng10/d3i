start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.68655216693878
time_elpased: 1.421
batch start
#iterations: 1
currently lose_sum: 100.43360376358032
time_elpased: 1.397
batch start
#iterations: 2
currently lose_sum: 100.2984219789505
time_elpased: 1.399
batch start
#iterations: 3
currently lose_sum: 100.13470685482025
time_elpased: 1.444
batch start
#iterations: 4
currently lose_sum: 100.06266087293625
time_elpased: 1.465
batch start
#iterations: 5
currently lose_sum: 99.91261368989944
time_elpased: 1.457
batch start
#iterations: 6
currently lose_sum: 99.9317991733551
time_elpased: 1.411
batch start
#iterations: 7
currently lose_sum: 99.71774870157242
time_elpased: 1.465
batch start
#iterations: 8
currently lose_sum: 99.95199620723724
time_elpased: 1.475
batch start
#iterations: 9
currently lose_sum: 99.96298450231552
time_elpased: 1.416
batch start
#iterations: 10
currently lose_sum: 99.69361543655396
time_elpased: 1.473
batch start
#iterations: 11
currently lose_sum: 99.68935042619705
time_elpased: 1.446
batch start
#iterations: 12
currently lose_sum: 99.6861082315445
time_elpased: 1.446
batch start
#iterations: 13
currently lose_sum: 99.90971779823303
time_elpased: 1.411
batch start
#iterations: 14
currently lose_sum: 99.62314665317535
time_elpased: 1.425
batch start
#iterations: 15
currently lose_sum: 99.69011348485947
time_elpased: 1.487
batch start
#iterations: 16
currently lose_sum: 99.7426186800003
time_elpased: 1.399
batch start
#iterations: 17
currently lose_sum: 99.704325735569
time_elpased: 1.395
batch start
#iterations: 18
currently lose_sum: 99.54052597284317
time_elpased: 1.414
batch start
#iterations: 19
currently lose_sum: 99.48981231451035
time_elpased: 1.476
start validation test
0.580773195876
0.64151618094
0.369287772746
0.468743876151
0.581122614171
65.796
batch start
#iterations: 20
currently lose_sum: 99.58569979667664
time_elpased: 1.466
batch start
#iterations: 21
currently lose_sum: 99.59608870744705
time_elpased: 1.464
batch start
#iterations: 22
currently lose_sum: 99.59732818603516
time_elpased: 1.391
batch start
#iterations: 23
currently lose_sum: 99.4338002204895
time_elpased: 1.398
batch start
#iterations: 24
currently lose_sum: 99.25772094726562
time_elpased: 1.404
batch start
#iterations: 25
currently lose_sum: 99.32845902442932
time_elpased: 1.426
batch start
#iterations: 26
currently lose_sum: 99.47049087285995
time_elpased: 1.446
batch start
#iterations: 27
currently lose_sum: 99.36361384391785
time_elpased: 1.466
batch start
#iterations: 28
currently lose_sum: 99.42684322595596
time_elpased: 1.479
batch start
#iterations: 29
currently lose_sum: 99.45880669355392
time_elpased: 1.415
batch start
#iterations: 30
currently lose_sum: 99.14117366075516
time_elpased: 1.434
batch start
#iterations: 31
currently lose_sum: 99.32121628522873
time_elpased: 1.437
batch start
#iterations: 32
currently lose_sum: 99.40193116664886
time_elpased: 1.383
batch start
#iterations: 33
currently lose_sum: 99.24955010414124
time_elpased: 1.425
batch start
#iterations: 34
currently lose_sum: 99.38772124052048
time_elpased: 1.421
batch start
#iterations: 35
currently lose_sum: 99.33455395698547
time_elpased: 1.432
batch start
#iterations: 36
currently lose_sum: 99.21640276908875
time_elpased: 1.408
batch start
#iterations: 37
currently lose_sum: 99.22521859407425
time_elpased: 1.44
batch start
#iterations: 38
currently lose_sum: 99.21350461244583
time_elpased: 1.437
batch start
#iterations: 39
currently lose_sum: 99.0334238409996
time_elpased: 1.441
start validation test
0.594690721649
0.633636232511
0.452140798683
0.527719382545
0.594926244034
65.274
batch start
#iterations: 40
currently lose_sum: 99.15739268064499
time_elpased: 1.442
batch start
#iterations: 41
currently lose_sum: 99.23539781570435
time_elpased: 1.372
batch start
#iterations: 42
currently lose_sum: 99.25123709440231
time_elpased: 1.444
batch start
#iterations: 43
currently lose_sum: 99.138662815094
time_elpased: 1.388
batch start
#iterations: 44
currently lose_sum: 99.05915635824203
time_elpased: 1.449
batch start
#iterations: 45
currently lose_sum: 99.32135182619095
time_elpased: 1.415
batch start
#iterations: 46
currently lose_sum: 98.98082810640335
time_elpased: 1.406
batch start
#iterations: 47
currently lose_sum: 99.13222813606262
time_elpased: 1.468
batch start
#iterations: 48
currently lose_sum: 99.09244269132614
time_elpased: 1.408
batch start
#iterations: 49
currently lose_sum: 99.13317477703094
time_elpased: 1.455
batch start
#iterations: 50
currently lose_sum: 99.21046435832977
time_elpased: 1.412
batch start
#iterations: 51
currently lose_sum: 99.05038523674011
time_elpased: 1.434
batch start
#iterations: 52
currently lose_sum: 98.98563146591187
time_elpased: 1.472
batch start
#iterations: 53
currently lose_sum: 98.9762709736824
time_elpased: 1.406
batch start
#iterations: 54
currently lose_sum: 99.15746539831161
time_elpased: 1.427
batch start
#iterations: 55
currently lose_sum: 99.12416625022888
time_elpased: 1.436
batch start
#iterations: 56
currently lose_sum: 99.00801581144333
time_elpased: 1.411
batch start
#iterations: 57
currently lose_sum: 99.03663146495819
time_elpased: 1.429
batch start
#iterations: 58
currently lose_sum: 99.14348709583282
time_elpased: 1.492
batch start
#iterations: 59
currently lose_sum: 99.02249145507812
time_elpased: 1.432
start validation test
0.580051546392
0.630728211965
0.389563606422
0.481644079659
0.580366272439
65.463
batch start
#iterations: 60
currently lose_sum: 98.95654851198196
time_elpased: 1.414
batch start
#iterations: 61
currently lose_sum: 99.15334749221802
time_elpased: 1.443
batch start
#iterations: 62
currently lose_sum: 99.15298748016357
time_elpased: 1.425
batch start
#iterations: 63
currently lose_sum: 99.00982737541199
time_elpased: 1.513
batch start
#iterations: 64
currently lose_sum: 99.0531075000763
time_elpased: 1.455
batch start
#iterations: 65
currently lose_sum: 98.82252728939056
time_elpased: 1.494
batch start
#iterations: 66
currently lose_sum: 99.14780563116074
time_elpased: 1.517
batch start
#iterations: 67
currently lose_sum: 98.91380888223648
time_elpased: 1.428
batch start
#iterations: 68
currently lose_sum: 99.0559909939766
time_elpased: 1.436
batch start
#iterations: 69
currently lose_sum: 98.92910087108612
time_elpased: 1.497
batch start
#iterations: 70
currently lose_sum: 98.98321086168289
time_elpased: 1.379
batch start
#iterations: 71
currently lose_sum: 98.91993772983551
time_elpased: 1.409
batch start
#iterations: 72
currently lose_sum: 98.87554949522018
time_elpased: 1.443
batch start
#iterations: 73
currently lose_sum: 98.9576695561409
time_elpased: 1.458
batch start
#iterations: 74
currently lose_sum: 99.026760160923
time_elpased: 1.43
batch start
#iterations: 75
currently lose_sum: 99.13096421957016
time_elpased: 1.437
batch start
#iterations: 76
currently lose_sum: 98.98442316055298
time_elpased: 1.477
batch start
#iterations: 77
currently lose_sum: 98.84466803073883
time_elpased: 1.439
batch start
#iterations: 78
currently lose_sum: 99.0720784664154
time_elpased: 1.415
batch start
#iterations: 79
currently lose_sum: 98.93858057260513
time_elpased: 1.455
start validation test
0.593195876289
0.623110151188
0.475092630712
0.539126372343
0.593391007632
65.103
batch start
#iterations: 80
currently lose_sum: 98.94039368629456
time_elpased: 1.43
batch start
#iterations: 81
currently lose_sum: 98.97689127922058
time_elpased: 1.478
batch start
#iterations: 82
currently lose_sum: 98.99845832586288
time_elpased: 1.447
batch start
#iterations: 83
currently lose_sum: 98.78939920663834
time_elpased: 1.427
batch start
#iterations: 84
currently lose_sum: 98.94367289543152
time_elpased: 1.409
batch start
#iterations: 85
currently lose_sum: 98.97738081216812
time_elpased: 1.485
batch start
#iterations: 86
currently lose_sum: 98.9254041314125
time_elpased: 1.453
batch start
#iterations: 87
currently lose_sum: 98.74812072515488
time_elpased: 1.508
batch start
#iterations: 88
currently lose_sum: 98.83252811431885
time_elpased: 1.444
batch start
#iterations: 89
currently lose_sum: 98.74100351333618
time_elpased: 1.43
batch start
#iterations: 90
currently lose_sum: 98.88602066040039
time_elpased: 1.447
batch start
#iterations: 91
currently lose_sum: 99.08109027147293
time_elpased: 1.423
batch start
#iterations: 92
currently lose_sum: 98.84165573120117
time_elpased: 1.466
batch start
#iterations: 93
currently lose_sum: 98.52071785926819
time_elpased: 1.488
batch start
#iterations: 94
currently lose_sum: 99.07558089494705
time_elpased: 1.395
batch start
#iterations: 95
currently lose_sum: 98.92504113912582
time_elpased: 1.414
batch start
#iterations: 96
currently lose_sum: 98.96245354413986
time_elpased: 1.414
batch start
#iterations: 97
currently lose_sum: 98.87014359235764
time_elpased: 1.434
batch start
#iterations: 98
currently lose_sum: 98.77781736850739
time_elpased: 1.459
batch start
#iterations: 99
currently lose_sum: 98.95284396409988
time_elpased: 1.453
start validation test
0.597731958763
0.628771551724
0.480444627419
0.544690781797
0.597925742045
65.066
batch start
#iterations: 100
currently lose_sum: 99.01607096195221
time_elpased: 1.446
batch start
#iterations: 101
currently lose_sum: 99.02659207582474
time_elpased: 1.444
batch start
#iterations: 102
currently lose_sum: 99.00934875011444
time_elpased: 1.413
batch start
#iterations: 103
currently lose_sum: 98.89963668584824
time_elpased: 1.405
batch start
#iterations: 104
currently lose_sum: 99.02225482463837
time_elpased: 1.437
batch start
#iterations: 105
currently lose_sum: 98.87092018127441
time_elpased: 1.46
batch start
#iterations: 106
currently lose_sum: 98.8531106710434
time_elpased: 1.407
batch start
#iterations: 107
currently lose_sum: 98.77256125211716
time_elpased: 1.457
batch start
#iterations: 108
currently lose_sum: 98.80405110120773
time_elpased: 1.427
batch start
#iterations: 109
currently lose_sum: 98.85206884145737
time_elpased: 1.394
batch start
#iterations: 110
currently lose_sum: 98.95594227313995
time_elpased: 1.392
batch start
#iterations: 111
currently lose_sum: 98.88032305240631
time_elpased: 1.416
batch start
#iterations: 112
currently lose_sum: 98.8075458407402
time_elpased: 1.446
batch start
#iterations: 113
currently lose_sum: 98.841801404953
time_elpased: 1.433
batch start
#iterations: 114
currently lose_sum: 99.08212506771088
time_elpased: 1.435
batch start
#iterations: 115
currently lose_sum: 98.8576911687851
time_elpased: 1.435
batch start
#iterations: 116
currently lose_sum: 98.96077251434326
time_elpased: 1.481
batch start
#iterations: 117
currently lose_sum: 98.78489696979523
time_elpased: 1.45
batch start
#iterations: 118
currently lose_sum: 99.21989786624908
time_elpased: 1.477
batch start
#iterations: 119
currently lose_sum: 98.97097831964493
time_elpased: 1.412
start validation test
0.60118556701
0.618262220629
0.53242074928
0.57213957861
0.601299180918
65.015
batch start
#iterations: 120
currently lose_sum: 98.71588134765625
time_elpased: 1.433
batch start
#iterations: 121
currently lose_sum: 98.94752812385559
time_elpased: 1.43
batch start
#iterations: 122
currently lose_sum: 99.00132060050964
time_elpased: 1.415
batch start
#iterations: 123
currently lose_sum: 99.01388722658157
time_elpased: 1.469
batch start
#iterations: 124
currently lose_sum: 98.89136910438538
time_elpased: 1.425
batch start
#iterations: 125
currently lose_sum: 98.8393082022667
time_elpased: 1.461
batch start
#iterations: 126
currently lose_sum: 98.67627829313278
time_elpased: 1.444
batch start
#iterations: 127
currently lose_sum: 98.87743151187897
time_elpased: 1.424
batch start
#iterations: 128
currently lose_sum: 98.87737888097763
time_elpased: 1.518
batch start
#iterations: 129
currently lose_sum: 98.68671131134033
time_elpased: 1.455
batch start
#iterations: 130
currently lose_sum: 98.7838716506958
time_elpased: 1.429
batch start
#iterations: 131
currently lose_sum: 98.85681223869324
time_elpased: 1.462
batch start
#iterations: 132
currently lose_sum: 98.77471554279327
time_elpased: 1.452
batch start
#iterations: 133
currently lose_sum: 98.90884840488434
time_elpased: 1.412
batch start
#iterations: 134
currently lose_sum: 98.75570952892303
time_elpased: 1.555
batch start
#iterations: 135
currently lose_sum: 98.91764032840729
time_elpased: 1.415
batch start
#iterations: 136
currently lose_sum: 98.8413205742836
time_elpased: 1.449
batch start
#iterations: 137
currently lose_sum: 98.86313509941101
time_elpased: 1.417
batch start
#iterations: 138
currently lose_sum: 98.75094765424728
time_elpased: 1.494
batch start
#iterations: 139
currently lose_sum: 98.67601305246353
time_elpased: 1.407
start validation test
0.597886597938
0.62930452397
0.47962124331
0.544360726593
0.59808199712
65.094
batch start
#iterations: 140
currently lose_sum: 98.81823319196701
time_elpased: 1.436
batch start
#iterations: 141
currently lose_sum: 98.92487066984177
time_elpased: 1.452
batch start
#iterations: 142
currently lose_sum: 99.00894260406494
time_elpased: 1.431
batch start
#iterations: 143
currently lose_sum: 98.74533069133759
time_elpased: 1.429
batch start
#iterations: 144
currently lose_sum: 98.65867948532104
time_elpased: 1.433
batch start
#iterations: 145
currently lose_sum: 98.75796747207642
time_elpased: 1.427
batch start
#iterations: 146
currently lose_sum: 98.84324038028717
time_elpased: 1.481
batch start
#iterations: 147
currently lose_sum: 98.82667398452759
time_elpased: 1.469
batch start
#iterations: 148
currently lose_sum: 98.83332300186157
time_elpased: 1.411
batch start
#iterations: 149
currently lose_sum: 98.76798677444458
time_elpased: 1.5
batch start
#iterations: 150
currently lose_sum: 98.8346699476242
time_elpased: 1.456
batch start
#iterations: 151
currently lose_sum: 98.76490372419357
time_elpased: 1.435
batch start
#iterations: 152
currently lose_sum: 98.648164331913
time_elpased: 1.428
batch start
#iterations: 153
currently lose_sum: 98.70985573530197
time_elpased: 1.429
batch start
#iterations: 154
currently lose_sum: 98.95179402828217
time_elpased: 1.437
batch start
#iterations: 155
currently lose_sum: 98.6953295469284
time_elpased: 1.42
batch start
#iterations: 156
currently lose_sum: 98.59864002466202
time_elpased: 1.446
batch start
#iterations: 157
currently lose_sum: 98.63246136903763
time_elpased: 1.424
batch start
#iterations: 158
currently lose_sum: 99.01237839460373
time_elpased: 1.488
batch start
#iterations: 159
currently lose_sum: 98.60088872909546
time_elpased: 1.5
start validation test
0.603041237113
0.632165813984
0.49598600247
0.555856739143
0.603218114825
65.008
batch start
#iterations: 160
currently lose_sum: 98.53595048189163
time_elpased: 1.451
batch start
#iterations: 161
currently lose_sum: 98.94400095939636
time_elpased: 1.407
batch start
#iterations: 162
currently lose_sum: 98.72182589769363
time_elpased: 1.47
batch start
#iterations: 163
currently lose_sum: 98.76436913013458
time_elpased: 1.423
batch start
#iterations: 164
currently lose_sum: 98.72703158855438
time_elpased: 1.489
batch start
#iterations: 165
currently lose_sum: 98.70799344778061
time_elpased: 1.413
batch start
#iterations: 166
currently lose_sum: 98.7966223359108
time_elpased: 1.424
batch start
#iterations: 167
currently lose_sum: 98.80024003982544
time_elpased: 1.405
batch start
#iterations: 168
currently lose_sum: 99.08922803401947
time_elpased: 1.41
batch start
#iterations: 169
currently lose_sum: 98.67553555965424
time_elpased: 1.404
batch start
#iterations: 170
currently lose_sum: 98.69637876749039
time_elpased: 1.427
batch start
#iterations: 171
currently lose_sum: 98.8476613163948
time_elpased: 1.513
batch start
#iterations: 172
currently lose_sum: 98.84865033626556
time_elpased: 1.481
batch start
#iterations: 173
currently lose_sum: 98.58233517408371
time_elpased: 1.517
batch start
#iterations: 174
currently lose_sum: 98.66625291109085
time_elpased: 1.456
batch start
#iterations: 175
currently lose_sum: 98.69070965051651
time_elpased: 1.427
batch start
#iterations: 176
currently lose_sum: 98.61369699239731
time_elpased: 1.458
batch start
#iterations: 177
currently lose_sum: 98.84394991397858
time_elpased: 1.419
batch start
#iterations: 178
currently lose_sum: 98.74113482236862
time_elpased: 1.411
batch start
#iterations: 179
currently lose_sum: 98.5332139134407
time_elpased: 1.513
start validation test
0.601546391753
0.62816210635
0.500926307122
0.557375171782
0.601712637246
64.941
batch start
#iterations: 180
currently lose_sum: 98.56680381298065
time_elpased: 1.428
batch start
#iterations: 181
currently lose_sum: 98.85027146339417
time_elpased: 1.42
batch start
#iterations: 182
currently lose_sum: 98.7656444311142
time_elpased: 1.433
batch start
#iterations: 183
currently lose_sum: 98.66881394386292
time_elpased: 1.428
batch start
#iterations: 184
currently lose_sum: 98.55437344312668
time_elpased: 1.42
batch start
#iterations: 185
currently lose_sum: 98.81408876180649
time_elpased: 1.41
batch start
#iterations: 186
currently lose_sum: 98.70163363218307
time_elpased: 1.447
batch start
#iterations: 187
currently lose_sum: 98.6549221277237
time_elpased: 1.446
batch start
#iterations: 188
currently lose_sum: 98.70134907960892
time_elpased: 1.429
batch start
#iterations: 189
currently lose_sum: 98.81361174583435
time_elpased: 1.435
batch start
#iterations: 190
currently lose_sum: 98.8870512843132
time_elpased: 1.43
batch start
#iterations: 191
currently lose_sum: 98.52527225017548
time_elpased: 1.46
batch start
#iterations: 192
currently lose_sum: 98.74597769975662
time_elpased: 1.426
batch start
#iterations: 193
currently lose_sum: 98.58822774887085
time_elpased: 1.468
batch start
#iterations: 194
currently lose_sum: 98.74778890609741
time_elpased: 1.427
batch start
#iterations: 195
currently lose_sum: 98.73786753416061
time_elpased: 1.415
batch start
#iterations: 196
currently lose_sum: 98.67100042104721
time_elpased: 1.434
batch start
#iterations: 197
currently lose_sum: 98.47960376739502
time_elpased: 1.408
batch start
#iterations: 198
currently lose_sum: 98.65544807910919
time_elpased: 1.425
batch start
#iterations: 199
currently lose_sum: 98.54078996181488
time_elpased: 1.449
start validation test
0.601391752577
0.631936127745
0.488781391519
0.551215831931
0.601577808523
65.032
batch start
#iterations: 200
currently lose_sum: 98.86141848564148
time_elpased: 1.44
batch start
#iterations: 201
currently lose_sum: 98.61652672290802
time_elpased: 1.444
batch start
#iterations: 202
currently lose_sum: 98.60349488258362
time_elpased: 1.482
batch start
#iterations: 203
currently lose_sum: 98.63151979446411
time_elpased: 1.443
batch start
#iterations: 204
currently lose_sum: 98.7425889968872
time_elpased: 1.468
batch start
#iterations: 205
currently lose_sum: 98.65176570415497
time_elpased: 1.405
batch start
#iterations: 206
currently lose_sum: 98.65279769897461
time_elpased: 1.472
batch start
#iterations: 207
currently lose_sum: 98.54253852367401
time_elpased: 1.491
batch start
#iterations: 208
currently lose_sum: 98.86914777755737
time_elpased: 1.403
batch start
#iterations: 209
currently lose_sum: 98.74811547994614
time_elpased: 1.403
batch start
#iterations: 210
currently lose_sum: 98.80756330490112
time_elpased: 1.438
batch start
#iterations: 211
currently lose_sum: 98.1072126030922
time_elpased: 1.489
batch start
#iterations: 212
currently lose_sum: 98.6449186205864
time_elpased: 1.488
batch start
#iterations: 213
currently lose_sum: 98.66854137182236
time_elpased: 1.42
batch start
#iterations: 214
currently lose_sum: 98.67273354530334
time_elpased: 1.448
batch start
#iterations: 215
currently lose_sum: 98.46495795249939
time_elpased: 1.521
batch start
#iterations: 216
currently lose_sum: 98.717893242836
time_elpased: 1.459
batch start
#iterations: 217
currently lose_sum: 98.50680726766586
time_elpased: 1.429
batch start
#iterations: 218
currently lose_sum: 98.58349514007568
time_elpased: 1.461
batch start
#iterations: 219
currently lose_sum: 98.45472025871277
time_elpased: 1.428
start validation test
0.595103092784
0.625862302178
0.476224783862
0.540884914373
0.595299504694
64.924
batch start
#iterations: 220
currently lose_sum: 98.51716846227646
time_elpased: 1.475
batch start
#iterations: 221
currently lose_sum: 98.40428864955902
time_elpased: 1.433
batch start
#iterations: 222
currently lose_sum: 98.70508706569672
time_elpased: 1.409
batch start
#iterations: 223
currently lose_sum: 98.37859761714935
time_elpased: 1.456
batch start
#iterations: 224
currently lose_sum: 98.41051262617111
time_elpased: 1.418
batch start
#iterations: 225
currently lose_sum: 98.42932111024857
time_elpased: 1.477
batch start
#iterations: 226
currently lose_sum: 98.60521322488785
time_elpased: 1.439
batch start
#iterations: 227
currently lose_sum: 98.62032860517502
time_elpased: 1.454
batch start
#iterations: 228
currently lose_sum: 98.63412111997604
time_elpased: 1.411
batch start
#iterations: 229
currently lose_sum: 98.3859812617302
time_elpased: 1.458
batch start
#iterations: 230
currently lose_sum: 98.52858555316925
time_elpased: 1.453
batch start
#iterations: 231
currently lose_sum: 98.66361576318741
time_elpased: 1.448
batch start
#iterations: 232
currently lose_sum: 98.72706687450409
time_elpased: 1.44
batch start
#iterations: 233
currently lose_sum: 98.4923175573349
time_elpased: 1.419
batch start
#iterations: 234
currently lose_sum: 98.64195907115936
time_elpased: 1.398
batch start
#iterations: 235
currently lose_sum: 98.96793776750565
time_elpased: 1.422
batch start
#iterations: 236
currently lose_sum: 98.75031816959381
time_elpased: 1.442
batch start
#iterations: 237
currently lose_sum: 98.48292791843414
time_elpased: 1.477
batch start
#iterations: 238
currently lose_sum: 98.505242228508
time_elpased: 1.426
batch start
#iterations: 239
currently lose_sum: 98.8261798620224
time_elpased: 1.489
start validation test
0.597783505155
0.638804237411
0.453067105805
0.530137893659
0.598022607012
65.144
batch start
#iterations: 240
currently lose_sum: 98.50399965047836
time_elpased: 1.464
batch start
#iterations: 241
currently lose_sum: 98.64127743244171
time_elpased: 1.406
batch start
#iterations: 242
currently lose_sum: 98.58341562747955
time_elpased: 1.382
batch start
#iterations: 243
currently lose_sum: 98.82933223247528
time_elpased: 1.419
batch start
#iterations: 244
currently lose_sum: 98.49242216348648
time_elpased: 1.4
batch start
#iterations: 245
currently lose_sum: 98.60040497779846
time_elpased: 1.47
batch start
#iterations: 246
currently lose_sum: 98.8245142698288
time_elpased: 1.422
batch start
#iterations: 247
currently lose_sum: 98.54114723205566
time_elpased: 1.421
batch start
#iterations: 248
currently lose_sum: 98.63664466142654
time_elpased: 1.413
batch start
#iterations: 249
currently lose_sum: 98.37339246273041
time_elpased: 1.419
batch start
#iterations: 250
currently lose_sum: 98.73674094676971
time_elpased: 1.465
batch start
#iterations: 251
currently lose_sum: 98.46512687206268
time_elpased: 1.4
batch start
#iterations: 252
currently lose_sum: 98.68759745359421
time_elpased: 1.443
batch start
#iterations: 253
currently lose_sum: 98.58379870653152
time_elpased: 1.441
batch start
#iterations: 254
currently lose_sum: 98.48274654150009
time_elpased: 1.431
batch start
#iterations: 255
currently lose_sum: 98.48420906066895
time_elpased: 1.434
batch start
#iterations: 256
currently lose_sum: 98.35529392957687
time_elpased: 1.453
batch start
#iterations: 257
currently lose_sum: 98.77821725606918
time_elpased: 1.408
batch start
#iterations: 258
currently lose_sum: 98.60547333955765
time_elpased: 1.396
batch start
#iterations: 259
currently lose_sum: 98.41321665048599
time_elpased: 1.435
start validation test
0.598453608247
0.639565217391
0.454199258954
0.531174771305
0.598691946701
64.935
batch start
#iterations: 260
currently lose_sum: 98.60576742887497
time_elpased: 1.449
batch start
#iterations: 261
currently lose_sum: 98.57098722457886
time_elpased: 1.423
batch start
#iterations: 262
currently lose_sum: 98.55072319507599
time_elpased: 1.412
batch start
#iterations: 263
currently lose_sum: 98.68381196260452
time_elpased: 1.415
batch start
#iterations: 264
currently lose_sum: 98.5748119354248
time_elpased: 1.434
batch start
#iterations: 265
currently lose_sum: 98.48123979568481
time_elpased: 1.453
batch start
#iterations: 266
currently lose_sum: 98.6193596124649
time_elpased: 1.469
batch start
#iterations: 267
currently lose_sum: 98.52061980962753
time_elpased: 1.408
batch start
#iterations: 268
currently lose_sum: 98.56496089696884
time_elpased: 1.394
batch start
#iterations: 269
currently lose_sum: 98.30763894319534
time_elpased: 1.464
batch start
#iterations: 270
currently lose_sum: 98.45467674732208
time_elpased: 1.412
batch start
#iterations: 271
currently lose_sum: 98.50587540864944
time_elpased: 1.424
batch start
#iterations: 272
currently lose_sum: 98.62324506044388
time_elpased: 1.425
batch start
#iterations: 273
currently lose_sum: 98.58345746994019
time_elpased: 1.417
batch start
#iterations: 274
currently lose_sum: 98.65269297361374
time_elpased: 1.404
batch start
#iterations: 275
currently lose_sum: 98.47315925359726
time_elpased: 1.427
batch start
#iterations: 276
currently lose_sum: 98.7753536105156
time_elpased: 1.401
batch start
#iterations: 277
currently lose_sum: 98.49499809741974
time_elpased: 1.433
batch start
#iterations: 278
currently lose_sum: 98.45642441511154
time_elpased: 1.428
batch start
#iterations: 279
currently lose_sum: 98.61061561107635
time_elpased: 1.399
start validation test
0.598195876289
0.640198511166
0.451420337587
0.529486328243
0.598438380277
65.005
batch start
#iterations: 280
currently lose_sum: 98.69542682170868
time_elpased: 1.44
batch start
#iterations: 281
currently lose_sum: 98.42490327358246
time_elpased: 1.491
batch start
#iterations: 282
currently lose_sum: 98.79417914152145
time_elpased: 1.433
batch start
#iterations: 283
currently lose_sum: 98.61597335338593
time_elpased: 1.437
batch start
#iterations: 284
currently lose_sum: 98.24685662984848
time_elpased: 1.456
batch start
#iterations: 285
currently lose_sum: 98.38098806142807
time_elpased: 1.452
batch start
#iterations: 286
currently lose_sum: 98.56160074472427
time_elpased: 1.398
batch start
#iterations: 287
currently lose_sum: 98.59602302312851
time_elpased: 1.449
batch start
#iterations: 288
currently lose_sum: 98.53881227970123
time_elpased: 1.419
batch start
#iterations: 289
currently lose_sum: 98.24041604995728
time_elpased: 1.475
batch start
#iterations: 290
currently lose_sum: 98.30838084220886
time_elpased: 1.414
batch start
#iterations: 291
currently lose_sum: 98.27044451236725
time_elpased: 1.424
batch start
#iterations: 292
currently lose_sum: 98.31669235229492
time_elpased: 1.426
batch start
#iterations: 293
currently lose_sum: 98.60080420970917
time_elpased: 1.525
batch start
#iterations: 294
currently lose_sum: 98.74436843395233
time_elpased: 1.408
batch start
#iterations: 295
currently lose_sum: 98.51456195116043
time_elpased: 1.449
batch start
#iterations: 296
currently lose_sum: 98.66652119159698
time_elpased: 1.496
batch start
#iterations: 297
currently lose_sum: 98.67649918794632
time_elpased: 1.515
batch start
#iterations: 298
currently lose_sum: 98.405193567276
time_elpased: 1.456
batch start
#iterations: 299
currently lose_sum: 98.61752712726593
time_elpased: 1.42
start validation test
0.596907216495
0.622957198444
0.494339234253
0.551245265695
0.597076680323
64.937
batch start
#iterations: 300
currently lose_sum: 98.54484486579895
time_elpased: 1.432
batch start
#iterations: 301
currently lose_sum: 98.53726553916931
time_elpased: 1.455
batch start
#iterations: 302
currently lose_sum: 98.51186114549637
time_elpased: 1.459
batch start
#iterations: 303
currently lose_sum: 98.4123101234436
time_elpased: 1.433
batch start
#iterations: 304
currently lose_sum: 98.26133567094803
time_elpased: 1.414
batch start
#iterations: 305
currently lose_sum: 98.53557187318802
time_elpased: 1.44
batch start
#iterations: 306
currently lose_sum: 98.56217730045319
time_elpased: 1.427
batch start
#iterations: 307
currently lose_sum: 98.6969426870346
time_elpased: 1.459
batch start
#iterations: 308
currently lose_sum: 98.43287134170532
time_elpased: 1.437
batch start
#iterations: 309
currently lose_sum: 98.47803151607513
time_elpased: 1.403
batch start
#iterations: 310
currently lose_sum: 98.64427697658539
time_elpased: 1.421
batch start
#iterations: 311
currently lose_sum: 98.25171399116516
time_elpased: 1.435
batch start
#iterations: 312
currently lose_sum: 98.5506020784378
time_elpased: 1.421
batch start
#iterations: 313
currently lose_sum: 98.55391985177994
time_elpased: 1.426
batch start
#iterations: 314
currently lose_sum: 98.71860629320145
time_elpased: 1.458
batch start
#iterations: 315
currently lose_sum: 98.4444186091423
time_elpased: 1.441
batch start
#iterations: 316
currently lose_sum: 98.36045616865158
time_elpased: 1.444
batch start
#iterations: 317
currently lose_sum: 98.55191850662231
time_elpased: 1.448
batch start
#iterations: 318
currently lose_sum: 98.31472969055176
time_elpased: 1.518
batch start
#iterations: 319
currently lose_sum: 98.70059847831726
time_elpased: 1.467
start validation test
0.603505154639
0.62678526685
0.51492383697
0.565374618601
0.603651509563
64.882
batch start
#iterations: 320
currently lose_sum: 98.62818795442581
time_elpased: 1.416
batch start
#iterations: 321
currently lose_sum: 98.4714737534523
time_elpased: 1.405
batch start
#iterations: 322
currently lose_sum: 98.48821514844894
time_elpased: 1.425
batch start
#iterations: 323
currently lose_sum: 98.39320868253708
time_elpased: 1.43
batch start
#iterations: 324
currently lose_sum: 98.60155004262924
time_elpased: 1.443
batch start
#iterations: 325
currently lose_sum: 98.4333478808403
time_elpased: 1.411
batch start
#iterations: 326
currently lose_sum: 98.46205443143845
time_elpased: 1.479
batch start
#iterations: 327
currently lose_sum: 98.56565850973129
time_elpased: 1.44
batch start
#iterations: 328
currently lose_sum: 98.42323952913284
time_elpased: 1.422
batch start
#iterations: 329
currently lose_sum: 98.40080153942108
time_elpased: 1.46
batch start
#iterations: 330
currently lose_sum: 98.48912566900253
time_elpased: 1.429
batch start
#iterations: 331
currently lose_sum: 98.629938185215
time_elpased: 1.465
batch start
#iterations: 332
currently lose_sum: 98.20250940322876
time_elpased: 1.414
batch start
#iterations: 333
currently lose_sum: 98.6137335896492
time_elpased: 1.406
batch start
#iterations: 334
currently lose_sum: 98.58848631381989
time_elpased: 1.422
batch start
#iterations: 335
currently lose_sum: 98.32795083522797
time_elpased: 1.404
batch start
#iterations: 336
currently lose_sum: 98.16500437259674
time_elpased: 1.405
batch start
#iterations: 337
currently lose_sum: 98.41490375995636
time_elpased: 1.417
batch start
#iterations: 338
currently lose_sum: 98.61223876476288
time_elpased: 1.491
batch start
#iterations: 339
currently lose_sum: 98.34956580400467
time_elpased: 1.498
start validation test
0.600567010309
0.634744485546
0.476842321943
0.544578313253
0.600771429456
64.845
batch start
#iterations: 340
currently lose_sum: 98.53356224298477
time_elpased: 1.403
batch start
#iterations: 341
currently lose_sum: 98.48012179136276
time_elpased: 1.425
batch start
#iterations: 342
currently lose_sum: 98.6545724272728
time_elpased: 1.472
batch start
#iterations: 343
currently lose_sum: 98.85056900978088
time_elpased: 1.456
batch start
#iterations: 344
currently lose_sum: 98.51205587387085
time_elpased: 1.43
batch start
#iterations: 345
currently lose_sum: 98.35424882173538
time_elpased: 1.448
batch start
#iterations: 346
currently lose_sum: 98.32175534963608
time_elpased: 1.439
batch start
#iterations: 347
currently lose_sum: 98.41864997148514
time_elpased: 1.396
batch start
#iterations: 348
currently lose_sum: 98.55726701021194
time_elpased: 1.49
batch start
#iterations: 349
currently lose_sum: 98.60707330703735
time_elpased: 1.408
batch start
#iterations: 350
currently lose_sum: 98.5447211265564
time_elpased: 1.41
batch start
#iterations: 351
currently lose_sum: 98.65914034843445
time_elpased: 1.416
batch start
#iterations: 352
currently lose_sum: 98.52008885145187
time_elpased: 1.491
batch start
#iterations: 353
currently lose_sum: 98.6722920536995
time_elpased: 1.438
batch start
#iterations: 354
currently lose_sum: 98.46522694826126
time_elpased: 1.425
batch start
#iterations: 355
currently lose_sum: 98.44611668586731
time_elpased: 1.419
batch start
#iterations: 356
currently lose_sum: 98.5776731967926
time_elpased: 1.434
batch start
#iterations: 357
currently lose_sum: 98.396131336689
time_elpased: 1.418
batch start
#iterations: 358
currently lose_sum: 98.28908628225327
time_elpased: 1.46
batch start
#iterations: 359
currently lose_sum: 98.34059190750122
time_elpased: 1.511
start validation test
0.59912371134
0.631958622567
0.477871552079
0.54421848444
0.59932404535
64.896
batch start
#iterations: 360
currently lose_sum: 98.44718235731125
time_elpased: 1.394
batch start
#iterations: 361
currently lose_sum: 98.43307954072952
time_elpased: 1.477
batch start
#iterations: 362
currently lose_sum: 98.60338544845581
time_elpased: 1.447
batch start
#iterations: 363
currently lose_sum: 98.47146517038345
time_elpased: 1.448
batch start
#iterations: 364
currently lose_sum: 98.18247020244598
time_elpased: 1.412
batch start
#iterations: 365
currently lose_sum: 98.43353933095932
time_elpased: 1.422
batch start
#iterations: 366
currently lose_sum: 98.57350248098373
time_elpased: 1.401
batch start
#iterations: 367
currently lose_sum: 98.41113007068634
time_elpased: 1.444
batch start
#iterations: 368
currently lose_sum: 98.44124835729599
time_elpased: 1.424
batch start
#iterations: 369
currently lose_sum: 98.25109130144119
time_elpased: 1.44
batch start
#iterations: 370
currently lose_sum: 98.43520200252533
time_elpased: 1.429
batch start
#iterations: 371
currently lose_sum: 98.30261373519897
time_elpased: 1.481
batch start
#iterations: 372
currently lose_sum: 98.41659051179886
time_elpased: 1.397
batch start
#iterations: 373
currently lose_sum: 98.31094628572464
time_elpased: 1.423
batch start
#iterations: 374
currently lose_sum: 98.28983074426651
time_elpased: 1.454
batch start
#iterations: 375
currently lose_sum: 98.25095707178116
time_elpased: 1.441
batch start
#iterations: 376
currently lose_sum: 98.38603633642197
time_elpased: 1.392
batch start
#iterations: 377
currently lose_sum: 98.3416159749031
time_elpased: 1.431
batch start
#iterations: 378
currently lose_sum: 98.64219212532043
time_elpased: 1.444
batch start
#iterations: 379
currently lose_sum: 98.34165334701538
time_elpased: 1.465
start validation test
0.594690721649
0.64057047489
0.434540963359
0.517814435518
0.594955322655
65.109
batch start
#iterations: 380
currently lose_sum: 98.50883454084396
time_elpased: 1.44
batch start
#iterations: 381
currently lose_sum: 98.22637742757797
time_elpased: 1.413
batch start
#iterations: 382
currently lose_sum: 98.45673364400864
time_elpased: 1.49
batch start
#iterations: 383
currently lose_sum: 98.57954221963882
time_elpased: 1.41
batch start
#iterations: 384
currently lose_sum: 98.3778093457222
time_elpased: 1.499
batch start
#iterations: 385
currently lose_sum: 98.80564457178116
time_elpased: 1.424
batch start
#iterations: 386
currently lose_sum: 98.66748893260956
time_elpased: 1.435
batch start
#iterations: 387
currently lose_sum: 98.41705322265625
time_elpased: 1.416
batch start
#iterations: 388
currently lose_sum: 98.65846717357635
time_elpased: 1.432
batch start
#iterations: 389
currently lose_sum: 98.42148655653
time_elpased: 1.468
batch start
#iterations: 390
currently lose_sum: 98.50356858968735
time_elpased: 1.439
batch start
#iterations: 391
currently lose_sum: 98.50174105167389
time_elpased: 1.431
batch start
#iterations: 392
currently lose_sum: 98.46352511644363
time_elpased: 1.511
batch start
#iterations: 393
currently lose_sum: 98.43448603153229
time_elpased: 1.408
batch start
#iterations: 394
currently lose_sum: 98.36787587404251
time_elpased: 1.403
batch start
#iterations: 395
currently lose_sum: 98.28438258171082
time_elpased: 1.469
batch start
#iterations: 396
currently lose_sum: 98.32320880889893
time_elpased: 1.454
batch start
#iterations: 397
currently lose_sum: 98.66616332530975
time_elpased: 1.422
batch start
#iterations: 398
currently lose_sum: 98.48664236068726
time_elpased: 1.412
batch start
#iterations: 399
currently lose_sum: 98.47222071886063
time_elpased: 1.421
start validation test
0.597474226804
0.646895701741
0.432173734047
0.51817116061
0.597747337903
65.105
acc: 0.604
pre: 0.640
rec: 0.481
F1: 0.549
auc: 0.638
