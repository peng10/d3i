start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.63770985603333
time_elpased: 1.425
batch start
#iterations: 1
currently lose_sum: 100.38195258378983
time_elpased: 1.429
batch start
#iterations: 2
currently lose_sum: 100.28684854507446
time_elpased: 1.474
batch start
#iterations: 3
currently lose_sum: 100.1712926030159
time_elpased: 1.527
batch start
#iterations: 4
currently lose_sum: 100.18532717227936
time_elpased: 1.436
batch start
#iterations: 5
currently lose_sum: 100.09280651807785
time_elpased: 1.44
batch start
#iterations: 6
currently lose_sum: 99.97433692216873
time_elpased: 1.449
batch start
#iterations: 7
currently lose_sum: 99.95255291461945
time_elpased: 1.435
batch start
#iterations: 8
currently lose_sum: 99.95816028118134
time_elpased: 1.477
batch start
#iterations: 9
currently lose_sum: 100.07453852891922
time_elpased: 1.424
batch start
#iterations: 10
currently lose_sum: 99.80992460250854
time_elpased: 1.493
batch start
#iterations: 11
currently lose_sum: 99.82100641727448
time_elpased: 1.437
batch start
#iterations: 12
currently lose_sum: 99.74584555625916
time_elpased: 1.445
batch start
#iterations: 13
currently lose_sum: 99.99751836061478
time_elpased: 1.429
batch start
#iterations: 14
currently lose_sum: 99.59214437007904
time_elpased: 1.458
batch start
#iterations: 15
currently lose_sum: 99.8241251707077
time_elpased: 1.46
batch start
#iterations: 16
currently lose_sum: 99.68383431434631
time_elpased: 1.452
batch start
#iterations: 17
currently lose_sum: 99.79141497612
time_elpased: 1.43
batch start
#iterations: 18
currently lose_sum: 99.56292080879211
time_elpased: 1.483
batch start
#iterations: 19
currently lose_sum: 99.65343129634857
time_elpased: 1.447
start validation test
0.589175257732
0.607033451783
0.509828136256
0.55420069359
0.589314563842
65.573
batch start
#iterations: 20
currently lose_sum: 99.60259056091309
time_elpased: 1.446
batch start
#iterations: 21
currently lose_sum: 99.60329496860504
time_elpased: 1.43
batch start
#iterations: 22
currently lose_sum: 99.51883435249329
time_elpased: 1.424
batch start
#iterations: 23
currently lose_sum: 99.62155365943909
time_elpased: 1.447
batch start
#iterations: 24
currently lose_sum: 99.48175799846649
time_elpased: 1.476
batch start
#iterations: 25
currently lose_sum: 99.34152925014496
time_elpased: 1.439
batch start
#iterations: 26
currently lose_sum: 99.53808724880219
time_elpased: 1.429
batch start
#iterations: 27
currently lose_sum: 99.38688963651657
time_elpased: 1.455
batch start
#iterations: 28
currently lose_sum: 99.38666361570358
time_elpased: 1.467
batch start
#iterations: 29
currently lose_sum: 99.52631080150604
time_elpased: 1.432
batch start
#iterations: 30
currently lose_sum: 99.36372888088226
time_elpased: 1.464
batch start
#iterations: 31
currently lose_sum: 99.27396816015244
time_elpased: 1.45
batch start
#iterations: 32
currently lose_sum: 99.4938103556633
time_elpased: 1.428
batch start
#iterations: 33
currently lose_sum: 99.32693022489548
time_elpased: 1.447
batch start
#iterations: 34
currently lose_sum: 99.19583320617676
time_elpased: 1.455
batch start
#iterations: 35
currently lose_sum: 99.16032457351685
time_elpased: 1.438
batch start
#iterations: 36
currently lose_sum: 99.36306965351105
time_elpased: 1.439
batch start
#iterations: 37
currently lose_sum: 99.37576979398727
time_elpased: 1.437
batch start
#iterations: 38
currently lose_sum: 99.13932001590729
time_elpased: 1.497
batch start
#iterations: 39
currently lose_sum: 99.09885650873184
time_elpased: 1.407
start validation test
0.6
0.629791749569
0.488628177421
0.55030134446
0.600195530412
65.123
batch start
#iterations: 40
currently lose_sum: 99.29557156562805
time_elpased: 1.474
batch start
#iterations: 41
currently lose_sum: 99.09338790178299
time_elpased: 1.516
batch start
#iterations: 42
currently lose_sum: 99.1469179391861
time_elpased: 1.436
batch start
#iterations: 43
currently lose_sum: 99.32433998584747
time_elpased: 1.434
batch start
#iterations: 44
currently lose_sum: 99.18726617097855
time_elpased: 1.462
batch start
#iterations: 45
currently lose_sum: 99.14669477939606
time_elpased: 1.477
batch start
#iterations: 46
currently lose_sum: 99.23431295156479
time_elpased: 1.453
batch start
#iterations: 47
currently lose_sum: 99.18036937713623
time_elpased: 1.443
batch start
#iterations: 48
currently lose_sum: 99.22442936897278
time_elpased: 1.438
batch start
#iterations: 49
currently lose_sum: 99.07067227363586
time_elpased: 1.506
batch start
#iterations: 50
currently lose_sum: 99.14227193593979
time_elpased: 1.448
batch start
#iterations: 51
currently lose_sum: 99.11916345357895
time_elpased: 1.471
batch start
#iterations: 52
currently lose_sum: 99.11848396062851
time_elpased: 1.456
batch start
#iterations: 53
currently lose_sum: 99.15185558795929
time_elpased: 1.471
batch start
#iterations: 54
currently lose_sum: 99.26698595285416
time_elpased: 1.472
batch start
#iterations: 55
currently lose_sum: 99.12227368354797
time_elpased: 1.431
batch start
#iterations: 56
currently lose_sum: 99.02535265684128
time_elpased: 1.423
batch start
#iterations: 57
currently lose_sum: 99.0667233467102
time_elpased: 1.46
batch start
#iterations: 58
currently lose_sum: 99.16101157665253
time_elpased: 1.453
batch start
#iterations: 59
currently lose_sum: 99.0246279835701
time_elpased: 1.435
start validation test
0.590154639175
0.647856664434
0.398168158897
0.493211804449
0.590491701053
65.421
batch start
#iterations: 60
currently lose_sum: 99.0814363360405
time_elpased: 1.466
batch start
#iterations: 61
currently lose_sum: 99.16086286306381
time_elpased: 1.459
batch start
#iterations: 62
currently lose_sum: 99.11860817670822
time_elpased: 1.474
batch start
#iterations: 63
currently lose_sum: 99.18109041452408
time_elpased: 1.492
batch start
#iterations: 64
currently lose_sum: 99.26904022693634
time_elpased: 1.434
batch start
#iterations: 65
currently lose_sum: 98.95563942193985
time_elpased: 1.434
batch start
#iterations: 66
currently lose_sum: 99.18974471092224
time_elpased: 1.451
batch start
#iterations: 67
currently lose_sum: 99.03929537534714
time_elpased: 1.458
batch start
#iterations: 68
currently lose_sum: 99.0064297914505
time_elpased: 1.454
batch start
#iterations: 69
currently lose_sum: 99.19774770736694
time_elpased: 1.511
batch start
#iterations: 70
currently lose_sum: 99.03996187448502
time_elpased: 1.481
batch start
#iterations: 71
currently lose_sum: 98.96777373552322
time_elpased: 1.439
batch start
#iterations: 72
currently lose_sum: 98.97783815860748
time_elpased: 1.442
batch start
#iterations: 73
currently lose_sum: 98.98565119504929
time_elpased: 1.467
batch start
#iterations: 74
currently lose_sum: 98.91251701116562
time_elpased: 1.452
batch start
#iterations: 75
currently lose_sum: 99.1389667391777
time_elpased: 1.491
batch start
#iterations: 76
currently lose_sum: 98.88552004098892
time_elpased: 1.433
batch start
#iterations: 77
currently lose_sum: 98.89150500297546
time_elpased: 1.432
batch start
#iterations: 78
currently lose_sum: 99.10838580131531
time_elpased: 1.526
batch start
#iterations: 79
currently lose_sum: 99.00652241706848
time_elpased: 1.452
start validation test
0.608969072165
0.632607342875
0.523103838633
0.572667868409
0.609119821826
64.856
batch start
#iterations: 80
currently lose_sum: 98.9462942481041
time_elpased: 1.483
batch start
#iterations: 81
currently lose_sum: 99.0046176314354
time_elpased: 1.491
batch start
#iterations: 82
currently lose_sum: 99.04298275709152
time_elpased: 1.446
batch start
#iterations: 83
currently lose_sum: 98.77667593955994
time_elpased: 1.439
batch start
#iterations: 84
currently lose_sum: 99.00569379329681
time_elpased: 1.453
batch start
#iterations: 85
currently lose_sum: 99.07312828302383
time_elpased: 1.423
batch start
#iterations: 86
currently lose_sum: 99.10545432567596
time_elpased: 1.473
batch start
#iterations: 87
currently lose_sum: 98.92343366146088
time_elpased: 1.426
batch start
#iterations: 88
currently lose_sum: 98.81241327524185
time_elpased: 1.456
batch start
#iterations: 89
currently lose_sum: 99.17830276489258
time_elpased: 1.44
batch start
#iterations: 90
currently lose_sum: 98.73722875118256
time_elpased: 1.436
batch start
#iterations: 91
currently lose_sum: 99.11616224050522
time_elpased: 1.474
batch start
#iterations: 92
currently lose_sum: 98.91655898094177
time_elpased: 1.46
batch start
#iterations: 93
currently lose_sum: 98.85596001148224
time_elpased: 1.49
batch start
#iterations: 94
currently lose_sum: 98.93760472536087
time_elpased: 1.423
batch start
#iterations: 95
currently lose_sum: 99.04514855146408
time_elpased: 1.476
batch start
#iterations: 96
currently lose_sum: 99.12471681833267
time_elpased: 1.431
batch start
#iterations: 97
currently lose_sum: 99.01395434141159
time_elpased: 1.469
batch start
#iterations: 98
currently lose_sum: 99.11069667339325
time_elpased: 1.478
batch start
#iterations: 99
currently lose_sum: 99.0112372636795
time_elpased: 1.443
start validation test
0.607525773196
0.624688722874
0.542142636616
0.580495867769
0.607640563377
64.779
batch start
#iterations: 100
currently lose_sum: 98.95475572347641
time_elpased: 1.47
batch start
#iterations: 101
currently lose_sum: 98.91523241996765
time_elpased: 1.424
batch start
#iterations: 102
currently lose_sum: 98.80938619375229
time_elpased: 1.456
batch start
#iterations: 103
currently lose_sum: 98.99696975946426
time_elpased: 1.472
batch start
#iterations: 104
currently lose_sum: 98.75009739398956
time_elpased: 1.448
batch start
#iterations: 105
currently lose_sum: 98.8019168972969
time_elpased: 1.454
batch start
#iterations: 106
currently lose_sum: 98.98323911428452
time_elpased: 1.445
batch start
#iterations: 107
currently lose_sum: 99.0061007142067
time_elpased: 1.447
batch start
#iterations: 108
currently lose_sum: 99.17292308807373
time_elpased: 1.466
batch start
#iterations: 109
currently lose_sum: 98.81688857078552
time_elpased: 1.479
batch start
#iterations: 110
currently lose_sum: 98.91040778160095
time_elpased: 1.5
batch start
#iterations: 111
currently lose_sum: 99.31974637508392
time_elpased: 1.429
batch start
#iterations: 112
currently lose_sum: 98.92210221290588
time_elpased: 1.468
batch start
#iterations: 113
currently lose_sum: 98.79968577623367
time_elpased: 1.515
batch start
#iterations: 114
currently lose_sum: 99.09577733278275
time_elpased: 1.441
batch start
#iterations: 115
currently lose_sum: 98.80248701572418
time_elpased: 1.426
batch start
#iterations: 116
currently lose_sum: 98.93486547470093
time_elpased: 1.471
batch start
#iterations: 117
currently lose_sum: 98.7853912115097
time_elpased: 1.482
batch start
#iterations: 118
currently lose_sum: 98.89943993091583
time_elpased: 1.449
batch start
#iterations: 119
currently lose_sum: 98.91535645723343
time_elpased: 1.434
start validation test
0.600412371134
0.6322875993
0.483276731501
0.547830144657
0.600618020816
64.845
batch start
#iterations: 120
currently lose_sum: 98.92317694425583
time_elpased: 1.432
batch start
#iterations: 121
currently lose_sum: 99.0540771484375
time_elpased: 1.48
batch start
#iterations: 122
currently lose_sum: 99.03134828805923
time_elpased: 1.436
batch start
#iterations: 123
currently lose_sum: 98.7988868355751
time_elpased: 1.466
batch start
#iterations: 124
currently lose_sum: 98.94181621074677
time_elpased: 1.489
batch start
#iterations: 125
currently lose_sum: 98.736392557621
time_elpased: 1.471
batch start
#iterations: 126
currently lose_sum: 98.95166862010956
time_elpased: 1.49
batch start
#iterations: 127
currently lose_sum: 98.86732363700867
time_elpased: 1.463
batch start
#iterations: 128
currently lose_sum: 98.83180940151215
time_elpased: 1.44
batch start
#iterations: 129
currently lose_sum: 98.85661548376083
time_elpased: 1.429
batch start
#iterations: 130
currently lose_sum: 98.76704394817352
time_elpased: 1.438
batch start
#iterations: 131
currently lose_sum: 98.96904683113098
time_elpased: 1.43
batch start
#iterations: 132
currently lose_sum: 99.11888068914413
time_elpased: 1.466
batch start
#iterations: 133
currently lose_sum: 98.78113782405853
time_elpased: 1.462
batch start
#iterations: 134
currently lose_sum: 98.84813666343689
time_elpased: 1.456
batch start
#iterations: 135
currently lose_sum: 98.70466434955597
time_elpased: 1.446
batch start
#iterations: 136
currently lose_sum: 98.816559612751
time_elpased: 1.46
batch start
#iterations: 137
currently lose_sum: 98.97710418701172
time_elpased: 1.437
batch start
#iterations: 138
currently lose_sum: 98.94968891143799
time_elpased: 1.45
batch start
#iterations: 139
currently lose_sum: 98.66917353868484
time_elpased: 1.453
start validation test
0.604639175258
0.637696757702
0.487804878049
0.5527696793
0.604844295887
64.883
batch start
#iterations: 140
currently lose_sum: 98.9537826180458
time_elpased: 1.42
batch start
#iterations: 141
currently lose_sum: 98.91181170940399
time_elpased: 1.424
batch start
#iterations: 142
currently lose_sum: 99.12744039297104
time_elpased: 1.432
batch start
#iterations: 143
currently lose_sum: 98.89519929885864
time_elpased: 1.463
batch start
#iterations: 144
currently lose_sum: 98.75333005189896
time_elpased: 1.473
batch start
#iterations: 145
currently lose_sum: 98.94322150945663
time_elpased: 1.435
batch start
#iterations: 146
currently lose_sum: 98.75881761312485
time_elpased: 1.458
batch start
#iterations: 147
currently lose_sum: 98.69376611709595
time_elpased: 1.433
batch start
#iterations: 148
currently lose_sum: 98.74948865175247
time_elpased: 1.49
batch start
#iterations: 149
currently lose_sum: 98.95223385095596
time_elpased: 1.413
batch start
#iterations: 150
currently lose_sum: 98.8352033495903
time_elpased: 1.509
batch start
#iterations: 151
currently lose_sum: 98.65181398391724
time_elpased: 1.515
batch start
#iterations: 152
currently lose_sum: 98.53866642713547
time_elpased: 1.447
batch start
#iterations: 153
currently lose_sum: 98.91231602430344
time_elpased: 1.442
batch start
#iterations: 154
currently lose_sum: 98.78741472959518
time_elpased: 1.49
batch start
#iterations: 155
currently lose_sum: 98.56179565191269
time_elpased: 1.421
batch start
#iterations: 156
currently lose_sum: 99.17308294773102
time_elpased: 1.45
batch start
#iterations: 157
currently lose_sum: 98.85727494955063
time_elpased: 1.488
batch start
#iterations: 158
currently lose_sum: 98.75568491220474
time_elpased: 1.419
batch start
#iterations: 159
currently lose_sum: 98.88948357105255
time_elpased: 1.441
start validation test
0.608298969072
0.6383951908
0.502727179171
0.562496401635
0.608484316633
64.804
batch start
#iterations: 160
currently lose_sum: 98.8088988661766
time_elpased: 1.43
batch start
#iterations: 161
currently lose_sum: 98.78332656621933
time_elpased: 1.457
batch start
#iterations: 162
currently lose_sum: 98.74087792634964
time_elpased: 1.447
batch start
#iterations: 163
currently lose_sum: 98.90648818016052
time_elpased: 1.449
batch start
#iterations: 164
currently lose_sum: 98.97727400064468
time_elpased: 1.485
batch start
#iterations: 165
currently lose_sum: 98.73004376888275
time_elpased: 1.449
batch start
#iterations: 166
currently lose_sum: 98.92248541116714
time_elpased: 1.511
batch start
#iterations: 167
currently lose_sum: 99.09988176822662
time_elpased: 1.447
batch start
#iterations: 168
currently lose_sum: 98.93579924106598
time_elpased: 1.466
batch start
#iterations: 169
currently lose_sum: 98.92307186126709
time_elpased: 1.436
batch start
#iterations: 170
currently lose_sum: 98.61787527799606
time_elpased: 1.433
batch start
#iterations: 171
currently lose_sum: 98.89927566051483
time_elpased: 1.477
batch start
#iterations: 172
currently lose_sum: 98.68864142894745
time_elpased: 1.56
batch start
#iterations: 173
currently lose_sum: 98.64025777578354
time_elpased: 1.464
batch start
#iterations: 174
currently lose_sum: 98.77922010421753
time_elpased: 1.524
batch start
#iterations: 175
currently lose_sum: 98.73818349838257
time_elpased: 1.452
batch start
#iterations: 176
currently lose_sum: 98.88037669658661
time_elpased: 1.474
batch start
#iterations: 177
currently lose_sum: 98.88136303424835
time_elpased: 1.428
batch start
#iterations: 178
currently lose_sum: 98.71384483575821
time_elpased: 1.49
batch start
#iterations: 179
currently lose_sum: 98.52281546592712
time_elpased: 1.413
start validation test
0.605567010309
0.6400379764
0.485643717197
0.552252779403
0.605777554147
64.879
batch start
#iterations: 180
currently lose_sum: 98.72745096683502
time_elpased: 1.441
batch start
#iterations: 181
currently lose_sum: 98.91807359457016
time_elpased: 1.47
batch start
#iterations: 182
currently lose_sum: 98.8082737326622
time_elpased: 1.46
batch start
#iterations: 183
currently lose_sum: 98.6752261519432
time_elpased: 1.453
batch start
#iterations: 184
currently lose_sum: 98.80154061317444
time_elpased: 1.439
batch start
#iterations: 185
currently lose_sum: 98.72504377365112
time_elpased: 1.516
batch start
#iterations: 186
currently lose_sum: 98.77927589416504
time_elpased: 1.459
batch start
#iterations: 187
currently lose_sum: 98.81669396162033
time_elpased: 1.457
batch start
#iterations: 188
currently lose_sum: 98.78417301177979
time_elpased: 1.52
batch start
#iterations: 189
currently lose_sum: 99.03141433000565
time_elpased: 1.495
batch start
#iterations: 190
currently lose_sum: 98.7578513622284
time_elpased: 1.422
batch start
#iterations: 191
currently lose_sum: 98.65749961137772
time_elpased: 1.453
batch start
#iterations: 192
currently lose_sum: 98.74047070741653
time_elpased: 1.439
batch start
#iterations: 193
currently lose_sum: 98.74238902330399
time_elpased: 1.422
batch start
#iterations: 194
currently lose_sum: 98.81603342294693
time_elpased: 1.48
batch start
#iterations: 195
currently lose_sum: 98.87356686592102
time_elpased: 1.44
batch start
#iterations: 196
currently lose_sum: 98.92499625682831
time_elpased: 1.459
batch start
#iterations: 197
currently lose_sum: 98.93299132585526
time_elpased: 1.431
batch start
#iterations: 198
currently lose_sum: 98.81859236955643
time_elpased: 1.444
batch start
#iterations: 199
currently lose_sum: 98.76658809185028
time_elpased: 1.431
start validation test
0.605463917526
0.643045347386
0.477204898631
0.547849716446
0.605689096016
64.916
batch start
#iterations: 200
currently lose_sum: 98.58512085676193
time_elpased: 1.453
batch start
#iterations: 201
currently lose_sum: 98.84266126155853
time_elpased: 1.484
batch start
#iterations: 202
currently lose_sum: 98.74631887674332
time_elpased: 1.484
batch start
#iterations: 203
currently lose_sum: 99.10047662258148
time_elpased: 1.417
batch start
#iterations: 204
currently lose_sum: 98.81958156824112
time_elpased: 1.433
batch start
#iterations: 205
currently lose_sum: 98.60827416181564
time_elpased: 1.472
batch start
#iterations: 206
currently lose_sum: 98.83702158927917
time_elpased: 1.452
batch start
#iterations: 207
currently lose_sum: 98.72562873363495
time_elpased: 1.44
batch start
#iterations: 208
currently lose_sum: 98.64183354377747
time_elpased: 1.459
batch start
#iterations: 209
currently lose_sum: 98.8812672495842
time_elpased: 1.497
batch start
#iterations: 210
currently lose_sum: 98.629629611969
time_elpased: 1.44
batch start
#iterations: 211
currently lose_sum: 98.66470491886139
time_elpased: 1.48
batch start
#iterations: 212
currently lose_sum: 98.76174110174179
time_elpased: 1.459
batch start
#iterations: 213
currently lose_sum: 98.69488400220871
time_elpased: 1.444
batch start
#iterations: 214
currently lose_sum: 98.7918192744255
time_elpased: 1.485
batch start
#iterations: 215
currently lose_sum: 98.57608199119568
time_elpased: 1.441
batch start
#iterations: 216
currently lose_sum: 98.73475873470306
time_elpased: 1.457
batch start
#iterations: 217
currently lose_sum: 98.79258662462234
time_elpased: 1.464
batch start
#iterations: 218
currently lose_sum: 98.62120825052261
time_elpased: 1.493
batch start
#iterations: 219
currently lose_sum: 98.52754825353622
time_elpased: 1.459
start validation test
0.60881443299
0.642933906502
0.492538849439
0.557776353359
0.609018572711
64.705
batch start
#iterations: 220
currently lose_sum: 98.63089966773987
time_elpased: 1.446
batch start
#iterations: 221
currently lose_sum: 98.64473760128021
time_elpased: 1.451
batch start
#iterations: 222
currently lose_sum: 98.81429213285446
time_elpased: 1.469
batch start
#iterations: 223
currently lose_sum: 98.81908053159714
time_elpased: 1.436
batch start
#iterations: 224
currently lose_sum: 98.97158044576645
time_elpased: 1.442
batch start
#iterations: 225
currently lose_sum: 98.66388183832169
time_elpased: 1.495
batch start
#iterations: 226
currently lose_sum: 98.61352348327637
time_elpased: 1.465
batch start
#iterations: 227
currently lose_sum: 98.53863841295242
time_elpased: 1.481
batch start
#iterations: 228
currently lose_sum: 98.79454094171524
time_elpased: 1.452
batch start
#iterations: 229
currently lose_sum: 98.88635748624802
time_elpased: 1.461
batch start
#iterations: 230
currently lose_sum: 98.78968018293381
time_elpased: 1.457
batch start
#iterations: 231
currently lose_sum: 98.89225482940674
time_elpased: 1.453
batch start
#iterations: 232
currently lose_sum: 98.70477241277695
time_elpased: 1.428
batch start
#iterations: 233
currently lose_sum: 98.78718602657318
time_elpased: 1.433
batch start
#iterations: 234
currently lose_sum: 98.61883854866028
time_elpased: 1.472
batch start
#iterations: 235
currently lose_sum: 98.66224670410156
time_elpased: 1.434
batch start
#iterations: 236
currently lose_sum: 98.52681165933609
time_elpased: 1.47
batch start
#iterations: 237
currently lose_sum: 98.82691824436188
time_elpased: 1.462
batch start
#iterations: 238
currently lose_sum: 98.69483816623688
time_elpased: 1.471
batch start
#iterations: 239
currently lose_sum: 98.48487657308578
time_elpased: 1.423
start validation test
0.60675257732
0.63198482933
0.514459195225
0.567198048448
0.606914612587
64.610
batch start
#iterations: 240
currently lose_sum: 98.82707059383392
time_elpased: 1.443
batch start
#iterations: 241
currently lose_sum: 98.78145736455917
time_elpased: 1.466
batch start
#iterations: 242
currently lose_sum: 98.61944788694382
time_elpased: 1.455
batch start
#iterations: 243
currently lose_sum: 98.69766169786453
time_elpased: 1.454
batch start
#iterations: 244
currently lose_sum: 98.69733291864395
time_elpased: 1.446
batch start
#iterations: 245
currently lose_sum: 98.68016350269318
time_elpased: 1.431
batch start
#iterations: 246
currently lose_sum: 98.7480856180191
time_elpased: 1.445
batch start
#iterations: 247
currently lose_sum: 98.75244706869125
time_elpased: 1.424
batch start
#iterations: 248
currently lose_sum: 98.81728559732437
time_elpased: 1.433
batch start
#iterations: 249
currently lose_sum: 98.65458673238754
time_elpased: 1.438
batch start
#iterations: 250
currently lose_sum: 98.67729300260544
time_elpased: 1.44
batch start
#iterations: 251
currently lose_sum: 98.85700052976608
time_elpased: 1.432
batch start
#iterations: 252
currently lose_sum: 98.71020090579987
time_elpased: 1.48
batch start
#iterations: 253
currently lose_sum: 98.66578179597855
time_elpased: 1.442
batch start
#iterations: 254
currently lose_sum: 98.62172108888626
time_elpased: 1.438
batch start
#iterations: 255
currently lose_sum: 98.61102002859116
time_elpased: 1.477
batch start
#iterations: 256
currently lose_sum: 98.36358958482742
time_elpased: 1.455
batch start
#iterations: 257
currently lose_sum: 98.8612431883812
time_elpased: 1.472
batch start
#iterations: 258
currently lose_sum: 98.75212663412094
time_elpased: 1.459
batch start
#iterations: 259
currently lose_sum: 98.50942379236221
time_elpased: 1.453
start validation test
0.611134020619
0.630573248408
0.539981475764
0.581771815057
0.611258939886
64.779
batch start
#iterations: 260
currently lose_sum: 98.5993315577507
time_elpased: 1.504
batch start
#iterations: 261
currently lose_sum: 98.77162671089172
time_elpased: 1.451
batch start
#iterations: 262
currently lose_sum: 98.6063973903656
time_elpased: 1.437
batch start
#iterations: 263
currently lose_sum: 98.63409608602524
time_elpased: 1.449
batch start
#iterations: 264
currently lose_sum: 98.70367342233658
time_elpased: 1.431
batch start
#iterations: 265
currently lose_sum: 98.77711474895477
time_elpased: 1.442
batch start
#iterations: 266
currently lose_sum: 98.57123327255249
time_elpased: 1.474
batch start
#iterations: 267
currently lose_sum: 98.76362818479538
time_elpased: 1.46
batch start
#iterations: 268
currently lose_sum: 98.4662014245987
time_elpased: 1.475
batch start
#iterations: 269
currently lose_sum: 98.87460714578629
time_elpased: 1.429
batch start
#iterations: 270
currently lose_sum: 98.74691164493561
time_elpased: 1.424
batch start
#iterations: 271
currently lose_sum: 98.48537665605545
time_elpased: 1.464
batch start
#iterations: 272
currently lose_sum: 98.87620842456818
time_elpased: 1.438
batch start
#iterations: 273
currently lose_sum: 98.79063910245895
time_elpased: 1.459
batch start
#iterations: 274
currently lose_sum: 98.69612073898315
time_elpased: 1.464
batch start
#iterations: 275
currently lose_sum: 98.77482372522354
time_elpased: 1.509
batch start
#iterations: 276
currently lose_sum: 98.79393994808197
time_elpased: 1.447
batch start
#iterations: 277
currently lose_sum: 98.81914538145065
time_elpased: 1.451
batch start
#iterations: 278
currently lose_sum: 98.74804675579071
time_elpased: 1.472
batch start
#iterations: 279
currently lose_sum: 98.56241881847382
time_elpased: 1.506
start validation test
0.610567010309
0.63365479723
0.527426160338
0.575680988486
0.610712976895
64.649
batch start
#iterations: 280
currently lose_sum: 98.81670379638672
time_elpased: 1.473
batch start
#iterations: 281
currently lose_sum: 98.72766613960266
time_elpased: 1.431
batch start
#iterations: 282
currently lose_sum: 98.6480033993721
time_elpased: 1.428
batch start
#iterations: 283
currently lose_sum: 98.603810608387
time_elpased: 1.44
batch start
#iterations: 284
currently lose_sum: 98.73550659418106
time_elpased: 1.483
batch start
#iterations: 285
currently lose_sum: 98.6212272644043
time_elpased: 1.478
batch start
#iterations: 286
currently lose_sum: 98.55012822151184
time_elpased: 1.432
batch start
#iterations: 287
currently lose_sum: 98.57574552297592
time_elpased: 1.465
batch start
#iterations: 288
currently lose_sum: 98.61568504571915
time_elpased: 1.459
batch start
#iterations: 289
currently lose_sum: 98.65812969207764
time_elpased: 1.427
batch start
#iterations: 290
currently lose_sum: 98.55961257219315
time_elpased: 1.441
batch start
#iterations: 291
currently lose_sum: 98.37480050325394
time_elpased: 1.439
batch start
#iterations: 292
currently lose_sum: 98.67142933607101
time_elpased: 1.532
batch start
#iterations: 293
currently lose_sum: 98.66582727432251
time_elpased: 1.457
batch start
#iterations: 294
currently lose_sum: 98.56234669685364
time_elpased: 1.444
batch start
#iterations: 295
currently lose_sum: 98.69871598482132
time_elpased: 1.459
batch start
#iterations: 296
currently lose_sum: 98.3713071346283
time_elpased: 1.445
batch start
#iterations: 297
currently lose_sum: 98.80636584758759
time_elpased: 1.443
batch start
#iterations: 298
currently lose_sum: 98.67456758022308
time_elpased: 1.474
batch start
#iterations: 299
currently lose_sum: 98.48234796524048
time_elpased: 1.448
start validation test
0.610206185567
0.63872795159
0.510548523207
0.567490276825
0.610381149964
64.636
batch start
#iterations: 300
currently lose_sum: 98.38150733709335
time_elpased: 1.494
batch start
#iterations: 301
currently lose_sum: 98.43451362848282
time_elpased: 1.451
batch start
#iterations: 302
currently lose_sum: 98.60347992181778
time_elpased: 1.429
batch start
#iterations: 303
currently lose_sum: 98.55122077465057
time_elpased: 1.442
batch start
#iterations: 304
currently lose_sum: 98.68409371376038
time_elpased: 1.456
batch start
#iterations: 305
currently lose_sum: 98.67199915647507
time_elpased: 1.449
batch start
#iterations: 306
currently lose_sum: 98.5355943441391
time_elpased: 1.497
batch start
#iterations: 307
currently lose_sum: 98.5844275355339
time_elpased: 1.477
batch start
#iterations: 308
currently lose_sum: 98.702352643013
time_elpased: 1.454
batch start
#iterations: 309
currently lose_sum: 98.75637024641037
time_elpased: 1.49
batch start
#iterations: 310
currently lose_sum: 98.76697808504105
time_elpased: 1.488
batch start
#iterations: 311
currently lose_sum: 98.44874680042267
time_elpased: 1.444
batch start
#iterations: 312
currently lose_sum: 98.70841419696808
time_elpased: 1.478
batch start
#iterations: 313
currently lose_sum: 98.61215341091156
time_elpased: 1.523
batch start
#iterations: 314
currently lose_sum: 98.64146131277084
time_elpased: 1.447
batch start
#iterations: 315
currently lose_sum: 98.39191329479218
time_elpased: 1.456
batch start
#iterations: 316
currently lose_sum: 98.55428922176361
time_elpased: 1.455
batch start
#iterations: 317
currently lose_sum: 98.42159909009933
time_elpased: 1.492
batch start
#iterations: 318
currently lose_sum: 98.61390918493271
time_elpased: 1.46
batch start
#iterations: 319
currently lose_sum: 98.59432703256607
time_elpased: 1.483
start validation test
0.609175257732
0.643115699155
0.493670886076
0.558570097811
0.609378043472
64.750
batch start
#iterations: 320
currently lose_sum: 98.75726765394211
time_elpased: 1.442
batch start
#iterations: 321
currently lose_sum: 98.70267689228058
time_elpased: 1.423
batch start
#iterations: 322
currently lose_sum: 98.83882510662079
time_elpased: 1.48
batch start
#iterations: 323
currently lose_sum: 98.3951365351677
time_elpased: 1.433
batch start
#iterations: 324
currently lose_sum: 98.55960094928741
time_elpased: 1.458
batch start
#iterations: 325
currently lose_sum: 98.60063397884369
time_elpased: 1.441
batch start
#iterations: 326
currently lose_sum: 98.68989688158035
time_elpased: 1.463
batch start
#iterations: 327
currently lose_sum: 98.6581963300705
time_elpased: 1.51
batch start
#iterations: 328
currently lose_sum: 98.59344655275345
time_elpased: 1.5
batch start
#iterations: 329
currently lose_sum: 98.67951595783234
time_elpased: 1.431
batch start
#iterations: 330
currently lose_sum: 98.4349462389946
time_elpased: 1.443
batch start
#iterations: 331
currently lose_sum: 98.73072326183319
time_elpased: 1.45
batch start
#iterations: 332
currently lose_sum: 98.38949424028397
time_elpased: 1.485
batch start
#iterations: 333
currently lose_sum: 98.57601284980774
time_elpased: 1.481
batch start
#iterations: 334
currently lose_sum: 98.84985548257828
time_elpased: 1.447
batch start
#iterations: 335
currently lose_sum: 98.4475228190422
time_elpased: 1.459
batch start
#iterations: 336
currently lose_sum: 98.57319450378418
time_elpased: 1.466
batch start
#iterations: 337
currently lose_sum: 98.72505766153336
time_elpased: 1.434
batch start
#iterations: 338
currently lose_sum: 98.59809893369675
time_elpased: 1.542
batch start
#iterations: 339
currently lose_sum: 98.51255285739899
time_elpased: 1.442
start validation test
0.608762886598
0.636223901627
0.511165997736
0.566879707829
0.60893423299
64.722
batch start
#iterations: 340
currently lose_sum: 98.50963109731674
time_elpased: 1.479
batch start
#iterations: 341
currently lose_sum: 98.92939519882202
time_elpased: 1.453
batch start
#iterations: 342
currently lose_sum: 98.62112247943878
time_elpased: 1.452
batch start
#iterations: 343
currently lose_sum: 98.5106371641159
time_elpased: 1.433
batch start
#iterations: 344
currently lose_sum: 98.61844211816788
time_elpased: 1.491
batch start
#iterations: 345
currently lose_sum: 98.55872857570648
time_elpased: 1.428
batch start
#iterations: 346
currently lose_sum: 98.62564074993134
time_elpased: 1.473
batch start
#iterations: 347
currently lose_sum: 98.48523128032684
time_elpased: 1.447
batch start
#iterations: 348
currently lose_sum: 98.68914330005646
time_elpased: 1.46
batch start
#iterations: 349
currently lose_sum: 98.64269214868546
time_elpased: 1.468
batch start
#iterations: 350
currently lose_sum: 98.78661471605301
time_elpased: 1.462
batch start
#iterations: 351
currently lose_sum: 98.56945008039474
time_elpased: 1.408
batch start
#iterations: 352
currently lose_sum: 98.39185374975204
time_elpased: 1.429
batch start
#iterations: 353
currently lose_sum: 98.64184337854385
time_elpased: 1.458
batch start
#iterations: 354
currently lose_sum: 98.62725633382797
time_elpased: 1.433
batch start
#iterations: 355
currently lose_sum: 98.51963913440704
time_elpased: 1.483
batch start
#iterations: 356
currently lose_sum: 98.53876119852066
time_elpased: 1.493
batch start
#iterations: 357
currently lose_sum: 98.47035920619965
time_elpased: 1.487
batch start
#iterations: 358
currently lose_sum: 98.44388973712921
time_elpased: 1.444
batch start
#iterations: 359
currently lose_sum: 98.39012277126312
time_elpased: 1.418
start validation test
0.609072164948
0.634100339495
0.518987341772
0.570797962649
0.60923032275
64.671
batch start
#iterations: 360
currently lose_sum: 98.63231056928635
time_elpased: 1.514
batch start
#iterations: 361
currently lose_sum: 98.64309853315353
time_elpased: 1.493
batch start
#iterations: 362
currently lose_sum: 98.73537510633469
time_elpased: 1.444
batch start
#iterations: 363
currently lose_sum: 98.48485374450684
time_elpased: 1.445
batch start
#iterations: 364
currently lose_sum: 98.53385937213898
time_elpased: 1.428
batch start
#iterations: 365
currently lose_sum: 98.46443992853165
time_elpased: 1.478
batch start
#iterations: 366
currently lose_sum: 98.4492557644844
time_elpased: 1.488
batch start
#iterations: 367
currently lose_sum: 98.46203184127808
time_elpased: 1.429
batch start
#iterations: 368
currently lose_sum: 98.52198123931885
time_elpased: 1.447
batch start
#iterations: 369
currently lose_sum: 98.64435786008835
time_elpased: 1.429
batch start
#iterations: 370
currently lose_sum: 98.49127250909805
time_elpased: 1.429
batch start
#iterations: 371
currently lose_sum: 98.34896665811539
time_elpased: 1.449
batch start
#iterations: 372
currently lose_sum: 98.54898136854172
time_elpased: 1.47
batch start
#iterations: 373
currently lose_sum: 98.57584315538406
time_elpased: 1.43
batch start
#iterations: 374
currently lose_sum: 98.58534300327301
time_elpased: 1.46
batch start
#iterations: 375
currently lose_sum: 98.43400150537491
time_elpased: 1.489
batch start
#iterations: 376
currently lose_sum: 98.63662254810333
time_elpased: 1.47
batch start
#iterations: 377
currently lose_sum: 98.53075873851776
time_elpased: 1.45
batch start
#iterations: 378
currently lose_sum: 98.68991631269455
time_elpased: 1.46
batch start
#iterations: 379
currently lose_sum: 98.48310559988022
time_elpased: 1.465
start validation test
0.607680412371
0.637899423782
0.501286405269
0.561401486775
0.607867203461
64.814
batch start
#iterations: 380
currently lose_sum: 98.94209867715836
time_elpased: 1.419
batch start
#iterations: 381
currently lose_sum: 98.82725232839584
time_elpased: 1.472
batch start
#iterations: 382
currently lose_sum: 98.48384481668472
time_elpased: 1.474
batch start
#iterations: 383
currently lose_sum: 98.55546200275421
time_elpased: 1.45
batch start
#iterations: 384
currently lose_sum: 98.49575275182724
time_elpased: 1.45
batch start
#iterations: 385
currently lose_sum: 98.80009388923645
time_elpased: 1.451
batch start
#iterations: 386
currently lose_sum: 98.59848231077194
time_elpased: 1.439
batch start
#iterations: 387
currently lose_sum: 98.5227399468422
time_elpased: 1.465
batch start
#iterations: 388
currently lose_sum: 98.68508207798004
time_elpased: 1.447
batch start
#iterations: 389
currently lose_sum: 98.46338307857513
time_elpased: 1.452
batch start
#iterations: 390
currently lose_sum: 98.58844584226608
time_elpased: 1.458
batch start
#iterations: 391
currently lose_sum: 98.5789624452591
time_elpased: 1.441
batch start
#iterations: 392
currently lose_sum: 98.53368163108826
time_elpased: 1.449
batch start
#iterations: 393
currently lose_sum: 98.70187246799469
time_elpased: 1.447
batch start
#iterations: 394
currently lose_sum: 98.51637667417526
time_elpased: 1.444
batch start
#iterations: 395
currently lose_sum: 98.35277879238129
time_elpased: 1.491
batch start
#iterations: 396
currently lose_sum: 98.39652436971664
time_elpased: 1.477
batch start
#iterations: 397
currently lose_sum: 98.59490269422531
time_elpased: 1.455
batch start
#iterations: 398
currently lose_sum: 98.3949602842331
time_elpased: 1.433
batch start
#iterations: 399
currently lose_sum: 98.39070123434067
time_elpased: 1.441
start validation test
0.608762886598
0.634910567043
0.515076669754
0.56875
0.608927367202
64.651
acc: 0.605
pre: 0.631
rec: 0.509
F1: 0.564
auc: 0.640
