start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.6452385187149
time_elpased: 1.841
batch start
#iterations: 1
currently lose_sum: 100.58190566301346
time_elpased: 1.838
batch start
#iterations: 2
currently lose_sum: 100.40932846069336
time_elpased: 1.827
batch start
#iterations: 3
currently lose_sum: 100.40779852867126
time_elpased: 1.815
batch start
#iterations: 4
currently lose_sum: 100.30952578783035
time_elpased: 1.824
batch start
#iterations: 5
currently lose_sum: 100.32404166460037
time_elpased: 1.839
batch start
#iterations: 6
currently lose_sum: 100.25643348693848
time_elpased: 1.808
batch start
#iterations: 7
currently lose_sum: 100.09063702821732
time_elpased: 1.809
batch start
#iterations: 8
currently lose_sum: 100.09752094745636
time_elpased: 1.826
batch start
#iterations: 9
currently lose_sum: 100.06519281864166
time_elpased: 1.786
batch start
#iterations: 10
currently lose_sum: 100.05199062824249
time_elpased: 1.861
batch start
#iterations: 11
currently lose_sum: 100.06446671485901
time_elpased: 1.8
batch start
#iterations: 12
currently lose_sum: 99.99209010601044
time_elpased: 1.813
batch start
#iterations: 13
currently lose_sum: 99.86557394266129
time_elpased: 1.797
batch start
#iterations: 14
currently lose_sum: 99.86916869878769
time_elpased: 1.797
batch start
#iterations: 15
currently lose_sum: 99.80837619304657
time_elpased: 1.806
batch start
#iterations: 16
currently lose_sum: 99.72561860084534
time_elpased: 1.803
batch start
#iterations: 17
currently lose_sum: 99.81539499759674
time_elpased: 1.801
batch start
#iterations: 18
currently lose_sum: 99.84785413742065
time_elpased: 1.863
batch start
#iterations: 19
currently lose_sum: 99.68433171510696
time_elpased: 1.795
start validation test
0.569020618557
0.641426783479
0.316455696203
0.423816415133
0.569464035233
66.149
batch start
#iterations: 20
currently lose_sum: 99.82245248556137
time_elpased: 1.788
batch start
#iterations: 21
currently lose_sum: 99.63354951143265
time_elpased: 1.83
batch start
#iterations: 22
currently lose_sum: 99.4636265039444
time_elpased: 1.822
batch start
#iterations: 23
currently lose_sum: 99.67477983236313
time_elpased: 1.801
batch start
#iterations: 24
currently lose_sum: 99.4814954996109
time_elpased: 1.809
batch start
#iterations: 25
currently lose_sum: 99.55218732357025
time_elpased: 1.791
batch start
#iterations: 26
currently lose_sum: 99.59081095457077
time_elpased: 1.798
batch start
#iterations: 27
currently lose_sum: 99.43535029888153
time_elpased: 1.8
batch start
#iterations: 28
currently lose_sum: 99.38050019741058
time_elpased: 1.838
batch start
#iterations: 29
currently lose_sum: 99.5293498635292
time_elpased: 1.797
batch start
#iterations: 30
currently lose_sum: 99.37683182954788
time_elpased: 1.842
batch start
#iterations: 31
currently lose_sum: 99.31235963106155
time_elpased: 1.824
batch start
#iterations: 32
currently lose_sum: 99.08542954921722
time_elpased: 1.795
batch start
#iterations: 33
currently lose_sum: 99.42786794900894
time_elpased: 1.803
batch start
#iterations: 34
currently lose_sum: 99.63211238384247
time_elpased: 1.826
batch start
#iterations: 35
currently lose_sum: 99.49032193422318
time_elpased: 1.848
batch start
#iterations: 36
currently lose_sum: 99.30187314748764
time_elpased: 1.785
batch start
#iterations: 37
currently lose_sum: 99.4650160074234
time_elpased: 1.819
batch start
#iterations: 38
currently lose_sum: 99.39610242843628
time_elpased: 1.83
batch start
#iterations: 39
currently lose_sum: 99.39210057258606
time_elpased: 1.787
start validation test
0.602783505155
0.626973102664
0.510960172893
0.56305284645
0.602944715177
65.185
batch start
#iterations: 40
currently lose_sum: 99.51625722646713
time_elpased: 1.798
batch start
#iterations: 41
currently lose_sum: 99.51337611675262
time_elpased: 1.83
batch start
#iterations: 42
currently lose_sum: 99.42221367359161
time_elpased: 1.84
batch start
#iterations: 43
currently lose_sum: 99.0684905052185
time_elpased: 1.816
batch start
#iterations: 44
currently lose_sum: 99.24962842464447
time_elpased: 1.81
batch start
#iterations: 45
currently lose_sum: 98.97762703895569
time_elpased: 1.782
batch start
#iterations: 46
currently lose_sum: 99.14465349912643
time_elpased: 1.813
batch start
#iterations: 47
currently lose_sum: 99.43111646175385
time_elpased: 1.836
batch start
#iterations: 48
currently lose_sum: 99.35969865322113
time_elpased: 1.803
batch start
#iterations: 49
currently lose_sum: 99.37770986557007
time_elpased: 1.804
batch start
#iterations: 50
currently lose_sum: 99.38288950920105
time_elpased: 1.853
batch start
#iterations: 51
currently lose_sum: 99.23127549886703
time_elpased: 1.834
batch start
#iterations: 52
currently lose_sum: 99.39202785491943
time_elpased: 1.889
batch start
#iterations: 53
currently lose_sum: 99.21922433376312
time_elpased: 1.868
batch start
#iterations: 54
currently lose_sum: 99.26391434669495
time_elpased: 1.775
batch start
#iterations: 55
currently lose_sum: 99.25860667228699
time_elpased: 1.848
batch start
#iterations: 56
currently lose_sum: 99.22071206569672
time_elpased: 1.81
batch start
#iterations: 57
currently lose_sum: 99.22677040100098
time_elpased: 1.826
batch start
#iterations: 58
currently lose_sum: 99.29756689071655
time_elpased: 1.809
batch start
#iterations: 59
currently lose_sum: 99.27928429841995
time_elpased: 1.809
start validation test
0.598762886598
0.616179829306
0.527529072759
0.568418718119
0.598887948545
64.964
batch start
#iterations: 60
currently lose_sum: 99.23710489273071
time_elpased: 1.833
batch start
#iterations: 61
currently lose_sum: 99.40398460626602
time_elpased: 1.804
batch start
#iterations: 62
currently lose_sum: 98.90573489665985
time_elpased: 1.809
batch start
#iterations: 63
currently lose_sum: 99.20965266227722
time_elpased: 1.793
batch start
#iterations: 64
currently lose_sum: 99.37255042791367
time_elpased: 1.831
batch start
#iterations: 65
currently lose_sum: 99.0915099978447
time_elpased: 1.836
batch start
#iterations: 66
currently lose_sum: 99.24906146526337
time_elpased: 1.835
batch start
#iterations: 67
currently lose_sum: 99.16177147626877
time_elpased: 1.801
batch start
#iterations: 68
currently lose_sum: 99.04800975322723
time_elpased: 1.846
batch start
#iterations: 69
currently lose_sum: 98.9034241437912
time_elpased: 1.872
batch start
#iterations: 70
currently lose_sum: 98.92719268798828
time_elpased: 1.824
batch start
#iterations: 71
currently lose_sum: 99.17228704690933
time_elpased: 1.838
batch start
#iterations: 72
currently lose_sum: 98.85159122943878
time_elpased: 1.827
batch start
#iterations: 73
currently lose_sum: 98.73926323652267
time_elpased: 1.808
batch start
#iterations: 74
currently lose_sum: 98.93626916408539
time_elpased: 1.873
batch start
#iterations: 75
currently lose_sum: 99.14827120304108
time_elpased: 1.85
batch start
#iterations: 76
currently lose_sum: 99.14275950193405
time_elpased: 1.799
batch start
#iterations: 77
currently lose_sum: 99.02141064405441
time_elpased: 1.818
batch start
#iterations: 78
currently lose_sum: 99.2441897392273
time_elpased: 1.86
batch start
#iterations: 79
currently lose_sum: 99.1393558382988
time_elpased: 1.804
start validation test
0.600618556701
0.626428663157
0.50200679222
0.557358318099
0.600791684863
65.062
batch start
#iterations: 80
currently lose_sum: 99.11259293556213
time_elpased: 1.813
batch start
#iterations: 81
currently lose_sum: 99.02588587999344
time_elpased: 1.784
batch start
#iterations: 82
currently lose_sum: 99.07065677642822
time_elpased: 1.825
batch start
#iterations: 83
currently lose_sum: 99.14458048343658
time_elpased: 1.819
batch start
#iterations: 84
currently lose_sum: 98.95587491989136
time_elpased: 1.821
batch start
#iterations: 85
currently lose_sum: 99.0950613617897
time_elpased: 1.8
batch start
#iterations: 86
currently lose_sum: 99.06444674730301
time_elpased: 1.799
batch start
#iterations: 87
currently lose_sum: 99.04498851299286
time_elpased: 1.829
batch start
#iterations: 88
currently lose_sum: 99.20576649904251
time_elpased: 1.77
batch start
#iterations: 89
currently lose_sum: 98.84191280603409
time_elpased: 1.849
batch start
#iterations: 90
currently lose_sum: 98.90399724245071
time_elpased: 1.873
batch start
#iterations: 91
currently lose_sum: 99.06243813037872
time_elpased: 1.811
batch start
#iterations: 92
currently lose_sum: 99.0421182513237
time_elpased: 1.851
batch start
#iterations: 93
currently lose_sum: 99.02909338474274
time_elpased: 1.84
batch start
#iterations: 94
currently lose_sum: 98.92359882593155
time_elpased: 1.822
batch start
#iterations: 95
currently lose_sum: 99.1541491150856
time_elpased: 1.799
batch start
#iterations: 96
currently lose_sum: 98.91903162002563
time_elpased: 1.821
batch start
#iterations: 97
currently lose_sum: 98.96991741657257
time_elpased: 1.849
batch start
#iterations: 98
currently lose_sum: 99.25481569766998
time_elpased: 1.851
batch start
#iterations: 99
currently lose_sum: 99.05734598636627
time_elpased: 1.795
start validation test
0.582731958763
0.610400217806
0.461459298137
0.525581667936
0.582944871624
65.195
batch start
#iterations: 100
currently lose_sum: 99.03836119174957
time_elpased: 1.794
batch start
#iterations: 101
currently lose_sum: 98.87394458055496
time_elpased: 1.798
batch start
#iterations: 102
currently lose_sum: 98.78119003772736
time_elpased: 1.806
batch start
#iterations: 103
currently lose_sum: 99.16418635845184
time_elpased: 1.782
batch start
#iterations: 104
currently lose_sum: 98.80961447954178
time_elpased: 1.833
batch start
#iterations: 105
currently lose_sum: 99.15660983324051
time_elpased: 1.869
batch start
#iterations: 106
currently lose_sum: 99.17899811267853
time_elpased: 1.804
batch start
#iterations: 107
currently lose_sum: 98.71449619531631
time_elpased: 1.8
batch start
#iterations: 108
currently lose_sum: 99.0462457537651
time_elpased: 1.83
batch start
#iterations: 109
currently lose_sum: 98.87141215801239
time_elpased: 1.84
batch start
#iterations: 110
currently lose_sum: 99.0052900314331
time_elpased: 1.884
batch start
#iterations: 111
currently lose_sum: 98.91372621059418
time_elpased: 1.868
batch start
#iterations: 112
currently lose_sum: 99.16090679168701
time_elpased: 1.807
batch start
#iterations: 113
currently lose_sum: 98.90673500299454
time_elpased: 1.812
batch start
#iterations: 114
currently lose_sum: 99.0888746380806
time_elpased: 1.794
batch start
#iterations: 115
currently lose_sum: 98.99652916193008
time_elpased: 1.835
batch start
#iterations: 116
currently lose_sum: 98.95418375730515
time_elpased: 1.819
batch start
#iterations: 117
currently lose_sum: 98.95424163341522
time_elpased: 1.853
batch start
#iterations: 118
currently lose_sum: 98.90854740142822
time_elpased: 1.823
batch start
#iterations: 119
currently lose_sum: 98.90219682455063
time_elpased: 1.894
start validation test
0.60381443299
0.619176153034
0.542965935988
0.578572211865
0.603921261911
64.769
batch start
#iterations: 120
currently lose_sum: 99.12986522912979
time_elpased: 1.843
batch start
#iterations: 121
currently lose_sum: 98.81550723314285
time_elpased: 1.829
batch start
#iterations: 122
currently lose_sum: 99.10099411010742
time_elpased: 1.784
batch start
#iterations: 123
currently lose_sum: 98.94888913631439
time_elpased: 1.813
batch start
#iterations: 124
currently lose_sum: 99.1313316822052
time_elpased: 1.812
batch start
#iterations: 125
currently lose_sum: 98.90588003396988
time_elpased: 1.811
batch start
#iterations: 126
currently lose_sum: 98.83154368400574
time_elpased: 1.814
batch start
#iterations: 127
currently lose_sum: 98.8054587841034
time_elpased: 1.783
batch start
#iterations: 128
currently lose_sum: 99.07274007797241
time_elpased: 1.801
batch start
#iterations: 129
currently lose_sum: 99.10056799650192
time_elpased: 1.845
batch start
#iterations: 130
currently lose_sum: 99.04875421524048
time_elpased: 1.803
batch start
#iterations: 131
currently lose_sum: 98.77443730831146
time_elpased: 1.807
batch start
#iterations: 132
currently lose_sum: 99.03826588392258
time_elpased: 1.833
batch start
#iterations: 133
currently lose_sum: 98.85833865404129
time_elpased: 1.789
batch start
#iterations: 134
currently lose_sum: 98.79447507858276
time_elpased: 1.791
batch start
#iterations: 135
currently lose_sum: 98.8693710565567
time_elpased: 1.813
batch start
#iterations: 136
currently lose_sum: 98.83213829994202
time_elpased: 1.807
batch start
#iterations: 137
currently lose_sum: 99.06413263082504
time_elpased: 1.853
batch start
#iterations: 138
currently lose_sum: 98.69257098436356
time_elpased: 1.814
batch start
#iterations: 139
currently lose_sum: 98.84486258029938
time_elpased: 1.812
start validation test
0.603505154639
0.638679632927
0.479880621591
0.548007991538
0.603722196575
64.950
batch start
#iterations: 140
currently lose_sum: 98.84665083885193
time_elpased: 1.825
batch start
#iterations: 141
currently lose_sum: 98.933928668499
time_elpased: 1.799
batch start
#iterations: 142
currently lose_sum: 99.17253279685974
time_elpased: 1.793
batch start
#iterations: 143
currently lose_sum: 98.75461411476135
time_elpased: 1.83
batch start
#iterations: 144
currently lose_sum: 98.89087301492691
time_elpased: 1.787
batch start
#iterations: 145
currently lose_sum: 99.0584626197815
time_elpased: 1.818
batch start
#iterations: 146
currently lose_sum: 99.09411561489105
time_elpased: 1.808
batch start
#iterations: 147
currently lose_sum: 98.89800190925598
time_elpased: 1.817
batch start
#iterations: 148
currently lose_sum: 99.15175348520279
time_elpased: 1.835
batch start
#iterations: 149
currently lose_sum: 98.80305057764053
time_elpased: 1.787
batch start
#iterations: 150
currently lose_sum: 98.92824739217758
time_elpased: 1.826
batch start
#iterations: 151
currently lose_sum: 98.86394947767258
time_elpased: 1.812
batch start
#iterations: 152
currently lose_sum: 98.9344213604927
time_elpased: 1.781
batch start
#iterations: 153
currently lose_sum: 98.74189329147339
time_elpased: 1.835
batch start
#iterations: 154
currently lose_sum: 99.23277527093887
time_elpased: 1.829
batch start
#iterations: 155
currently lose_sum: 98.91712439060211
time_elpased: 1.823
batch start
#iterations: 156
currently lose_sum: 98.84319800138474
time_elpased: 1.776
batch start
#iterations: 157
currently lose_sum: 98.79522293806076
time_elpased: 1.862
batch start
#iterations: 158
currently lose_sum: 98.86879223585129
time_elpased: 1.802
batch start
#iterations: 159
currently lose_sum: 98.79669845104218
time_elpased: 1.828
start validation test
0.602010309278
0.61508302583
0.548934856437
0.5801294252
0.602103491422
64.744
batch start
#iterations: 160
currently lose_sum: 98.85946029424667
time_elpased: 1.837
batch start
#iterations: 161
currently lose_sum: 99.1724534034729
time_elpased: 1.818
batch start
#iterations: 162
currently lose_sum: 98.85137861967087
time_elpased: 1.804
batch start
#iterations: 163
currently lose_sum: 98.71606075763702
time_elpased: 1.826
batch start
#iterations: 164
currently lose_sum: 98.81299459934235
time_elpased: 1.825
batch start
#iterations: 165
currently lose_sum: 99.08394080400467
time_elpased: 1.896
batch start
#iterations: 166
currently lose_sum: 98.91726487874985
time_elpased: 1.786
batch start
#iterations: 167
currently lose_sum: 98.97599422931671
time_elpased: 1.833
batch start
#iterations: 168
currently lose_sum: 98.97154766321182
time_elpased: 1.776
batch start
#iterations: 169
currently lose_sum: 98.74488127231598
time_elpased: 1.782
batch start
#iterations: 170
currently lose_sum: 98.94974261522293
time_elpased: 1.837
batch start
#iterations: 171
currently lose_sum: 98.88518905639648
time_elpased: 1.795
batch start
#iterations: 172
currently lose_sum: 98.83813166618347
time_elpased: 1.813
batch start
#iterations: 173
currently lose_sum: 98.89294415712357
time_elpased: 1.851
batch start
#iterations: 174
currently lose_sum: 99.13649678230286
time_elpased: 1.79
batch start
#iterations: 175
currently lose_sum: 98.93751668930054
time_elpased: 1.835
batch start
#iterations: 176
currently lose_sum: 98.88688462972641
time_elpased: 1.769
batch start
#iterations: 177
currently lose_sum: 98.80364525318146
time_elpased: 1.835
batch start
#iterations: 178
currently lose_sum: 99.00494253635406
time_elpased: 1.801
batch start
#iterations: 179
currently lose_sum: 98.94386214017868
time_elpased: 1.826
start validation test
0.605515463918
0.632036847492
0.508387362355
0.563508811955
0.605685987281
64.741
batch start
#iterations: 180
currently lose_sum: 98.8526377081871
time_elpased: 1.812
batch start
#iterations: 181
currently lose_sum: 98.75000047683716
time_elpased: 1.831
batch start
#iterations: 182
currently lose_sum: 98.91726863384247
time_elpased: 1.826
batch start
#iterations: 183
currently lose_sum: 98.9342435002327
time_elpased: 1.805
batch start
#iterations: 184
currently lose_sum: 98.89812958240509
time_elpased: 1.809
batch start
#iterations: 185
currently lose_sum: 98.64977753162384
time_elpased: 1.822
batch start
#iterations: 186
currently lose_sum: 98.78367882966995
time_elpased: 1.827
batch start
#iterations: 187
currently lose_sum: 98.55146634578705
time_elpased: 1.859
batch start
#iterations: 188
currently lose_sum: 98.72989231348038
time_elpased: 1.788
batch start
#iterations: 189
currently lose_sum: 98.80808699131012
time_elpased: 1.842
batch start
#iterations: 190
currently lose_sum: 98.86408019065857
time_elpased: 1.816
batch start
#iterations: 191
currently lose_sum: 98.76538228988647
time_elpased: 1.827
batch start
#iterations: 192
currently lose_sum: 98.72157597541809
time_elpased: 1.797
batch start
#iterations: 193
currently lose_sum: 98.79251611232758
time_elpased: 1.859
batch start
#iterations: 194
currently lose_sum: 98.9049431681633
time_elpased: 1.832
batch start
#iterations: 195
currently lose_sum: 98.92387229204178
time_elpased: 1.818
batch start
#iterations: 196
currently lose_sum: 98.80268996953964
time_elpased: 1.791
batch start
#iterations: 197
currently lose_sum: 99.08251112699509
time_elpased: 1.818
batch start
#iterations: 198
currently lose_sum: 98.72128462791443
time_elpased: 1.819
batch start
#iterations: 199
currently lose_sum: 98.93611085414886
time_elpased: 1.816
start validation test
0.598144329897
0.643852029355
0.442420500154
0.524460168354
0.598417727099
65.151
batch start
#iterations: 200
currently lose_sum: 98.88367074728012
time_elpased: 1.798
batch start
#iterations: 201
currently lose_sum: 99.04430055618286
time_elpased: 1.826
batch start
#iterations: 202
currently lose_sum: 98.64398640394211
time_elpased: 1.853
batch start
#iterations: 203
currently lose_sum: 98.92575281858444
time_elpased: 1.815
batch start
#iterations: 204
currently lose_sum: 98.82670438289642
time_elpased: 1.843
batch start
#iterations: 205
currently lose_sum: 98.93613886833191
time_elpased: 1.789
batch start
#iterations: 206
currently lose_sum: 98.67023950815201
time_elpased: 1.85
batch start
#iterations: 207
currently lose_sum: 98.79715418815613
time_elpased: 1.816
batch start
#iterations: 208
currently lose_sum: 98.6993436217308
time_elpased: 1.841
batch start
#iterations: 209
currently lose_sum: 98.62240248918533
time_elpased: 1.847
batch start
#iterations: 210
currently lose_sum: 98.6264756321907
time_elpased: 1.828
batch start
#iterations: 211
currently lose_sum: 98.54419195652008
time_elpased: 1.828
batch start
#iterations: 212
currently lose_sum: 98.84566760063171
time_elpased: 1.847
batch start
#iterations: 213
currently lose_sum: 98.87353819608688
time_elpased: 1.843
batch start
#iterations: 214
currently lose_sum: 98.74981451034546
time_elpased: 1.858
batch start
#iterations: 215
currently lose_sum: 98.63730329275131
time_elpased: 1.832
batch start
#iterations: 216
currently lose_sum: 98.80705332756042
time_elpased: 1.852
batch start
#iterations: 217
currently lose_sum: 98.44128715991974
time_elpased: 1.83
batch start
#iterations: 218
currently lose_sum: 98.76585590839386
time_elpased: 1.815
batch start
#iterations: 219
currently lose_sum: 98.83625543117523
time_elpased: 1.805
start validation test
0.604690721649
0.625582536179
0.524956262221
0.570869005652
0.604830707791
64.646
batch start
#iterations: 220
currently lose_sum: 99.01971107721329
time_elpased: 1.824
batch start
#iterations: 221
currently lose_sum: 98.89341419935226
time_elpased: 1.812
batch start
#iterations: 222
currently lose_sum: 98.68671798706055
time_elpased: 1.867
batch start
#iterations: 223
currently lose_sum: 98.54464292526245
time_elpased: 1.823
batch start
#iterations: 224
currently lose_sum: 98.90538793802261
time_elpased: 1.819
batch start
#iterations: 225
currently lose_sum: 98.40666562318802
time_elpased: 1.802
batch start
#iterations: 226
currently lose_sum: 98.73541206121445
time_elpased: 1.819
batch start
#iterations: 227
currently lose_sum: 98.92074698209763
time_elpased: 1.833
batch start
#iterations: 228
currently lose_sum: 98.71122980117798
time_elpased: 1.839
batch start
#iterations: 229
currently lose_sum: 98.65271323919296
time_elpased: 1.813
batch start
#iterations: 230
currently lose_sum: 98.51167410612106
time_elpased: 1.794
batch start
#iterations: 231
currently lose_sum: 98.76512837409973
time_elpased: 1.825
batch start
#iterations: 232
currently lose_sum: 98.69765013456345
time_elpased: 1.787
batch start
#iterations: 233
currently lose_sum: 98.69031757116318
time_elpased: 1.854
batch start
#iterations: 234
currently lose_sum: 98.89969086647034
time_elpased: 1.841
batch start
#iterations: 235
currently lose_sum: 98.78514832258224
time_elpased: 1.809
batch start
#iterations: 236
currently lose_sum: 98.63037931919098
time_elpased: 1.83
batch start
#iterations: 237
currently lose_sum: 98.80931752920151
time_elpased: 1.819
batch start
#iterations: 238
currently lose_sum: 98.96291667222977
time_elpased: 1.802
batch start
#iterations: 239
currently lose_sum: 98.76514029502869
time_elpased: 1.855
start validation test
0.604329896907
0.62359210367
0.529896058454
0.572938689218
0.604460576991
64.713
batch start
#iterations: 240
currently lose_sum: 98.71301990747452
time_elpased: 1.838
batch start
#iterations: 241
currently lose_sum: 98.86920607089996
time_elpased: 1.797
batch start
#iterations: 242
currently lose_sum: 98.63635891675949
time_elpased: 1.817
batch start
#iterations: 243
currently lose_sum: 98.83939403295517
time_elpased: 1.84
batch start
#iterations: 244
currently lose_sum: 98.72514086961746
time_elpased: 1.835
batch start
#iterations: 245
currently lose_sum: 98.57962304353714
time_elpased: 1.836
batch start
#iterations: 246
currently lose_sum: 98.77962934970856
time_elpased: 1.826
batch start
#iterations: 247
currently lose_sum: 98.88736253976822
time_elpased: 1.811
batch start
#iterations: 248
currently lose_sum: 99.03286987543106
time_elpased: 1.82
batch start
#iterations: 249
currently lose_sum: 98.74314105510712
time_elpased: 1.86
batch start
#iterations: 250
currently lose_sum: 98.76442515850067
time_elpased: 1.862
batch start
#iterations: 251
currently lose_sum: 98.81846505403519
time_elpased: 1.841
batch start
#iterations: 252
currently lose_sum: 98.65885096788406
time_elpased: 1.835
batch start
#iterations: 253
currently lose_sum: 98.84426295757294
time_elpased: 1.84
batch start
#iterations: 254
currently lose_sum: 98.68687850236893
time_elpased: 1.833
batch start
#iterations: 255
currently lose_sum: 98.5017209649086
time_elpased: 1.84
batch start
#iterations: 256
currently lose_sum: 98.67140072584152
time_elpased: 1.861
batch start
#iterations: 257
currently lose_sum: 98.70070415735245
time_elpased: 1.823
batch start
#iterations: 258
currently lose_sum: 98.85165977478027
time_elpased: 1.826
batch start
#iterations: 259
currently lose_sum: 98.82945716381073
time_elpased: 1.82
start validation test
0.604175257732
0.626615308151
0.518987341772
0.567745567126
0.604324818258
64.838
batch start
#iterations: 260
currently lose_sum: 98.59768015146255
time_elpased: 1.78
batch start
#iterations: 261
currently lose_sum: 98.96434503793716
time_elpased: 1.799
batch start
#iterations: 262
currently lose_sum: 98.51693373918533
time_elpased: 1.84
batch start
#iterations: 263
currently lose_sum: 98.66036105155945
time_elpased: 1.82
batch start
#iterations: 264
currently lose_sum: 98.78301948308945
time_elpased: 1.812
batch start
#iterations: 265
currently lose_sum: 98.60988533496857
time_elpased: 1.87
batch start
#iterations: 266
currently lose_sum: 98.52134335041046
time_elpased: 1.825
batch start
#iterations: 267
currently lose_sum: 98.7417122721672
time_elpased: 1.832
batch start
#iterations: 268
currently lose_sum: 98.87797904014587
time_elpased: 1.861
batch start
#iterations: 269
currently lose_sum: 98.76686632633209
time_elpased: 1.8
batch start
#iterations: 270
currently lose_sum: 98.69954007863998
time_elpased: 1.819
batch start
#iterations: 271
currently lose_sum: 98.68784874677658
time_elpased: 1.844
batch start
#iterations: 272
currently lose_sum: 98.5928909778595
time_elpased: 1.836
batch start
#iterations: 273
currently lose_sum: 98.82130432128906
time_elpased: 1.846
batch start
#iterations: 274
currently lose_sum: 98.72016316652298
time_elpased: 1.844
batch start
#iterations: 275
currently lose_sum: 98.87249994277954
time_elpased: 1.86
batch start
#iterations: 276
currently lose_sum: 98.85144144296646
time_elpased: 1.831
batch start
#iterations: 277
currently lose_sum: 98.94232100248337
time_elpased: 1.838
batch start
#iterations: 278
currently lose_sum: 98.89558225870132
time_elpased: 1.808
batch start
#iterations: 279
currently lose_sum: 98.88456177711487
time_elpased: 1.822
start validation test
0.604948453608
0.63367625993
0.500771843161
0.559438951483
0.605131351716
64.965
batch start
#iterations: 280
currently lose_sum: 98.71885496377945
time_elpased: 1.835
batch start
#iterations: 281
currently lose_sum: 98.76896286010742
time_elpased: 1.865
batch start
#iterations: 282
currently lose_sum: 98.82789891958237
time_elpased: 1.827
batch start
#iterations: 283
currently lose_sum: 98.80725830793381
time_elpased: 1.799
batch start
#iterations: 284
currently lose_sum: 98.49021434783936
time_elpased: 1.832
batch start
#iterations: 285
currently lose_sum: 98.84090834856033
time_elpased: 1.834
batch start
#iterations: 286
currently lose_sum: 98.64878565073013
time_elpased: 1.808
batch start
#iterations: 287
currently lose_sum: 98.6972599029541
time_elpased: 1.852
batch start
#iterations: 288
currently lose_sum: 98.60366094112396
time_elpased: 1.836
batch start
#iterations: 289
currently lose_sum: 98.61629498004913
time_elpased: 1.84
batch start
#iterations: 290
currently lose_sum: 98.68468004465103
time_elpased: 1.846
batch start
#iterations: 291
currently lose_sum: 98.69554156064987
time_elpased: 1.797
batch start
#iterations: 292
currently lose_sum: 98.55163478851318
time_elpased: 1.84
batch start
#iterations: 293
currently lose_sum: 98.44326436519623
time_elpased: 1.834
batch start
#iterations: 294
currently lose_sum: 98.83102196455002
time_elpased: 1.825
batch start
#iterations: 295
currently lose_sum: 98.68125820159912
time_elpased: 1.867
batch start
#iterations: 296
currently lose_sum: 98.67431223392487
time_elpased: 1.821
batch start
#iterations: 297
currently lose_sum: 98.631178855896
time_elpased: 1.795
batch start
#iterations: 298
currently lose_sum: 98.85469233989716
time_elpased: 1.823
batch start
#iterations: 299
currently lose_sum: 98.64442765712738
time_elpased: 1.808
start validation test
0.605979381443
0.635119280407
0.501389317691
0.56038647343
0.606163005432
64.674
batch start
#iterations: 300
currently lose_sum: 98.73787730932236
time_elpased: 1.808
batch start
#iterations: 301
currently lose_sum: 98.82793915271759
time_elpased: 1.829
batch start
#iterations: 302
currently lose_sum: 98.87304908037186
time_elpased: 1.8
batch start
#iterations: 303
currently lose_sum: 98.67880749702454
time_elpased: 1.797
batch start
#iterations: 304
currently lose_sum: 98.9137669801712
time_elpased: 1.79
batch start
#iterations: 305
currently lose_sum: 98.84270375967026
time_elpased: 1.844
batch start
#iterations: 306
currently lose_sum: 98.6182177066803
time_elpased: 1.808
batch start
#iterations: 307
currently lose_sum: 98.61522072553635
time_elpased: 1.806
batch start
#iterations: 308
currently lose_sum: 98.53499984741211
time_elpased: 1.81
batch start
#iterations: 309
currently lose_sum: 98.88408702611923
time_elpased: 1.858
batch start
#iterations: 310
currently lose_sum: 98.79584187269211
time_elpased: 1.816
batch start
#iterations: 311
currently lose_sum: 98.5100490450859
time_elpased: 1.858
batch start
#iterations: 312
currently lose_sum: 98.56336456537247
time_elpased: 1.809
batch start
#iterations: 313
currently lose_sum: 98.56573551893234
time_elpased: 1.833
batch start
#iterations: 314
currently lose_sum: 98.96788918972015
time_elpased: 1.798
batch start
#iterations: 315
currently lose_sum: 98.58007055521011
time_elpased: 1.789
batch start
#iterations: 316
currently lose_sum: 98.85277837514877
time_elpased: 1.822
batch start
#iterations: 317
currently lose_sum: 98.69620859622955
time_elpased: 1.825
batch start
#iterations: 318
currently lose_sum: 98.66049337387085
time_elpased: 1.83
batch start
#iterations: 319
currently lose_sum: 98.66420149803162
time_elpased: 1.798
start validation test
0.606701030928
0.640576586286
0.489348564372
0.554842473746
0.606907061283
64.798
batch start
#iterations: 320
currently lose_sum: 98.65556365251541
time_elpased: 1.804
batch start
#iterations: 321
currently lose_sum: 98.72935217618942
time_elpased: 1.822
batch start
#iterations: 322
currently lose_sum: 98.75832724571228
time_elpased: 1.818
batch start
#iterations: 323
currently lose_sum: 98.7897516489029
time_elpased: 1.799
batch start
#iterations: 324
currently lose_sum: 98.70446115732193
time_elpased: 1.858
batch start
#iterations: 325
currently lose_sum: 98.62546277046204
time_elpased: 1.791
batch start
#iterations: 326
currently lose_sum: 98.42770689725876
time_elpased: 1.866
batch start
#iterations: 327
currently lose_sum: 98.81606966257095
time_elpased: 1.793
batch start
#iterations: 328
currently lose_sum: 98.5572292804718
time_elpased: 1.811
batch start
#iterations: 329
currently lose_sum: 98.73970401287079
time_elpased: 1.868
batch start
#iterations: 330
currently lose_sum: 98.66239708662033
time_elpased: 1.816
batch start
#iterations: 331
currently lose_sum: 98.64457255601883
time_elpased: 1.844
batch start
#iterations: 332
currently lose_sum: 98.69028520584106
time_elpased: 1.814
batch start
#iterations: 333
currently lose_sum: 98.79469221830368
time_elpased: 1.82
batch start
#iterations: 334
currently lose_sum: 98.78706884384155
time_elpased: 1.814
batch start
#iterations: 335
currently lose_sum: 98.66062223911285
time_elpased: 1.862
batch start
#iterations: 336
currently lose_sum: 98.65052622556686
time_elpased: 1.819
batch start
#iterations: 337
currently lose_sum: 98.66782701015472
time_elpased: 1.826
batch start
#iterations: 338
currently lose_sum: 98.52464962005615
time_elpased: 1.809
batch start
#iterations: 339
currently lose_sum: 98.66704231500626
time_elpased: 1.844
start validation test
0.605567010309
0.627390499692
0.523309663476
0.570643025474
0.605711425769
64.701
batch start
#iterations: 340
currently lose_sum: 98.5443297624588
time_elpased: 1.826
batch start
#iterations: 341
currently lose_sum: 98.7197932600975
time_elpased: 1.781
batch start
#iterations: 342
currently lose_sum: 98.85045993328094
time_elpased: 1.845
batch start
#iterations: 343
currently lose_sum: 98.93088811635971
time_elpased: 1.823
batch start
#iterations: 344
currently lose_sum: 98.66121542453766
time_elpased: 1.856
batch start
#iterations: 345
currently lose_sum: 98.75925409793854
time_elpased: 1.846
batch start
#iterations: 346
currently lose_sum: 98.72870653867722
time_elpased: 1.88
batch start
#iterations: 347
currently lose_sum: 98.63509684801102
time_elpased: 1.873
batch start
#iterations: 348
currently lose_sum: 98.56642091274261
time_elpased: 1.825
batch start
#iterations: 349
currently lose_sum: 98.63300883769989
time_elpased: 1.827
batch start
#iterations: 350
currently lose_sum: 98.72613179683685
time_elpased: 1.839
batch start
#iterations: 351
currently lose_sum: 98.65987318754196
time_elpased: 1.822
batch start
#iterations: 352
currently lose_sum: 98.72316235303879
time_elpased: 1.83
batch start
#iterations: 353
currently lose_sum: 98.69726490974426
time_elpased: 1.832
batch start
#iterations: 354
currently lose_sum: 98.73807173967361
time_elpased: 1.802
batch start
#iterations: 355
currently lose_sum: 98.55070966482162
time_elpased: 1.811
batch start
#iterations: 356
currently lose_sum: 98.5212813615799
time_elpased: 1.857
batch start
#iterations: 357
currently lose_sum: 98.57788962125778
time_elpased: 1.835
batch start
#iterations: 358
currently lose_sum: 98.66694158315659
time_elpased: 1.793
batch start
#iterations: 359
currently lose_sum: 98.48717319965363
time_elpased: 1.794
start validation test
0.604948453608
0.630614582008
0.510033961099
0.563950842057
0.605115090639
64.709
batch start
#iterations: 360
currently lose_sum: 98.61374807357788
time_elpased: 1.831
batch start
#iterations: 361
currently lose_sum: 98.61627298593521
time_elpased: 1.843
batch start
#iterations: 362
currently lose_sum: 98.66848140954971
time_elpased: 1.87
batch start
#iterations: 363
currently lose_sum: 98.6586766242981
time_elpased: 1.856
batch start
#iterations: 364
currently lose_sum: 98.83500504493713
time_elpased: 1.828
batch start
#iterations: 365
currently lose_sum: 98.5924900174141
time_elpased: 1.81
batch start
#iterations: 366
currently lose_sum: 98.55638027191162
time_elpased: 1.853
batch start
#iterations: 367
currently lose_sum: 98.83874464035034
time_elpased: 1.852
batch start
#iterations: 368
currently lose_sum: 98.61443358659744
time_elpased: 1.856
batch start
#iterations: 369
currently lose_sum: 98.67446154356003
time_elpased: 1.83
batch start
#iterations: 370
currently lose_sum: 98.48907923698425
time_elpased: 1.821
batch start
#iterations: 371
currently lose_sum: 98.50063979625702
time_elpased: 1.818
batch start
#iterations: 372
currently lose_sum: 98.73080402612686
time_elpased: 1.86
batch start
#iterations: 373
currently lose_sum: 98.82790315151215
time_elpased: 1.8
batch start
#iterations: 374
currently lose_sum: 98.67486107349396
time_elpased: 1.82
batch start
#iterations: 375
currently lose_sum: 98.74822574853897
time_elpased: 1.824
batch start
#iterations: 376
currently lose_sum: 99.06493902206421
time_elpased: 1.846
batch start
#iterations: 377
currently lose_sum: 98.65588974952698
time_elpased: 1.815
batch start
#iterations: 378
currently lose_sum: 98.66714495420456
time_elpased: 1.838
batch start
#iterations: 379
currently lose_sum: 98.61488652229309
time_elpased: 1.856
start validation test
0.601597938144
0.636388583974
0.477307811053
0.545486621582
0.601816148633
64.819
batch start
#iterations: 380
currently lose_sum: 98.74328714609146
time_elpased: 1.856
batch start
#iterations: 381
currently lose_sum: 98.50381445884705
time_elpased: 1.827
batch start
#iterations: 382
currently lose_sum: 98.59238302707672
time_elpased: 1.83
batch start
#iterations: 383
currently lose_sum: 98.60967475175858
time_elpased: 1.845
batch start
#iterations: 384
currently lose_sum: 98.78076130151749
time_elpased: 1.824
batch start
#iterations: 385
currently lose_sum: 98.60952407121658
time_elpased: 1.823
batch start
#iterations: 386
currently lose_sum: 98.61324751377106
time_elpased: 1.845
batch start
#iterations: 387
currently lose_sum: 98.85036259889603
time_elpased: 1.845
batch start
#iterations: 388
currently lose_sum: 98.70380371809006
time_elpased: 1.832
batch start
#iterations: 389
currently lose_sum: 98.62200659513474
time_elpased: 1.806
batch start
#iterations: 390
currently lose_sum: 98.57444202899933
time_elpased: 1.822
batch start
#iterations: 391
currently lose_sum: 98.59117895364761
time_elpased: 1.799
batch start
#iterations: 392
currently lose_sum: 98.51503962278366
time_elpased: 1.813
batch start
#iterations: 393
currently lose_sum: 98.3281369805336
time_elpased: 1.864
batch start
#iterations: 394
currently lose_sum: 98.70656007528305
time_elpased: 1.834
batch start
#iterations: 395
currently lose_sum: 98.37798750400543
time_elpased: 1.842
batch start
#iterations: 396
currently lose_sum: 98.7676255106926
time_elpased: 1.881
batch start
#iterations: 397
currently lose_sum: 98.74581283330917
time_elpased: 1.829
batch start
#iterations: 398
currently lose_sum: 98.4220883846283
time_elpased: 1.831
batch start
#iterations: 399
currently lose_sum: 98.665567278862
time_elpased: 1.815
start validation test
0.603041237113
0.632806324111
0.494288360605
0.555035534755
0.603232169562
64.971
acc: 0.611
pre: 0.631
rec: 0.535
F1: 0.579
auc: 0.638
