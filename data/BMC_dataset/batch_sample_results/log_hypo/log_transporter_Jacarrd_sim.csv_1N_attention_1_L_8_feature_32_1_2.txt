start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.59307193756104
time_elpased: 1.451
batch start
#iterations: 1
currently lose_sum: 100.44055503606796
time_elpased: 1.403
batch start
#iterations: 2
currently lose_sum: 100.31365704536438
time_elpased: 1.414
batch start
#iterations: 3
currently lose_sum: 100.2836212515831
time_elpased: 1.467
batch start
#iterations: 4
currently lose_sum: 100.09481394290924
time_elpased: 1.394
batch start
#iterations: 5
currently lose_sum: 100.07845103740692
time_elpased: 1.397
batch start
#iterations: 6
currently lose_sum: 99.87261164188385
time_elpased: 1.417
batch start
#iterations: 7
currently lose_sum: 99.95468401908875
time_elpased: 1.461
batch start
#iterations: 8
currently lose_sum: 99.89881831407547
time_elpased: 1.401
batch start
#iterations: 9
currently lose_sum: 99.79003465175629
time_elpased: 1.431
batch start
#iterations: 10
currently lose_sum: 99.74783450365067
time_elpased: 1.393
batch start
#iterations: 11
currently lose_sum: 100.00542497634888
time_elpased: 1.413
batch start
#iterations: 12
currently lose_sum: 99.82299274206161
time_elpased: 1.433
batch start
#iterations: 13
currently lose_sum: 99.90333074331284
time_elpased: 1.408
batch start
#iterations: 14
currently lose_sum: 99.77955424785614
time_elpased: 1.455
batch start
#iterations: 15
currently lose_sum: 99.74399614334106
time_elpased: 1.421
batch start
#iterations: 16
currently lose_sum: 99.6482914686203
time_elpased: 1.389
batch start
#iterations: 17
currently lose_sum: 99.71492248773575
time_elpased: 1.441
batch start
#iterations: 18
currently lose_sum: 99.70319378376007
time_elpased: 1.423
batch start
#iterations: 19
currently lose_sum: 99.45919746160507
time_elpased: 1.397
start validation test
0.578195876289
0.608241603161
0.443552536791
0.513003630304
0.578432263438
65.737
batch start
#iterations: 20
currently lose_sum: 99.60432475805283
time_elpased: 1.422
batch start
#iterations: 21
currently lose_sum: 99.55864125490189
time_elpased: 1.407
batch start
#iterations: 22
currently lose_sum: 99.71480691432953
time_elpased: 1.396
batch start
#iterations: 23
currently lose_sum: 99.61559242010117
time_elpased: 1.39
batch start
#iterations: 24
currently lose_sum: 99.45315504074097
time_elpased: 1.394
batch start
#iterations: 25
currently lose_sum: 99.48554521799088
time_elpased: 1.45
batch start
#iterations: 26
currently lose_sum: 99.50361627340317
time_elpased: 1.409
batch start
#iterations: 27
currently lose_sum: 99.41023701429367
time_elpased: 1.415
batch start
#iterations: 28
currently lose_sum: 99.2540453672409
time_elpased: 1.454
batch start
#iterations: 29
currently lose_sum: 99.3786329627037
time_elpased: 1.407
batch start
#iterations: 30
currently lose_sum: 99.38702028989792
time_elpased: 1.439
batch start
#iterations: 31
currently lose_sum: 99.30238199234009
time_elpased: 1.427
batch start
#iterations: 32
currently lose_sum: 99.33696985244751
time_elpased: 1.414
batch start
#iterations: 33
currently lose_sum: 99.24083930253983
time_elpased: 1.429
batch start
#iterations: 34
currently lose_sum: 99.28877657651901
time_elpased: 1.416
batch start
#iterations: 35
currently lose_sum: 99.25446337461472
time_elpased: 1.461
batch start
#iterations: 36
currently lose_sum: 99.32161712646484
time_elpased: 1.421
batch start
#iterations: 37
currently lose_sum: 99.24889355897903
time_elpased: 1.397
batch start
#iterations: 38
currently lose_sum: 99.24638307094574
time_elpased: 1.422
batch start
#iterations: 39
currently lose_sum: 99.2455575466156
time_elpased: 1.44
start validation test
0.587783505155
0.621160890392
0.453740866523
0.524412726732
0.588018837681
65.411
batch start
#iterations: 40
currently lose_sum: 99.21422910690308
time_elpased: 1.445
batch start
#iterations: 41
currently lose_sum: 99.24654537439346
time_elpased: 1.401
batch start
#iterations: 42
currently lose_sum: 99.12651216983795
time_elpased: 1.418
batch start
#iterations: 43
currently lose_sum: 99.14224499464035
time_elpased: 1.393
batch start
#iterations: 44
currently lose_sum: 99.16140657663345
time_elpased: 1.425
batch start
#iterations: 45
currently lose_sum: 99.2972002029419
time_elpased: 1.456
batch start
#iterations: 46
currently lose_sum: 98.90335690975189
time_elpased: 1.4
batch start
#iterations: 47
currently lose_sum: 99.14446467161179
time_elpased: 1.482
batch start
#iterations: 48
currently lose_sum: 99.24008572101593
time_elpased: 1.42
batch start
#iterations: 49
currently lose_sum: 98.95502805709839
time_elpased: 1.405
batch start
#iterations: 50
currently lose_sum: 99.04475837945938
time_elpased: 1.405
batch start
#iterations: 51
currently lose_sum: 99.01258689165115
time_elpased: 1.391
batch start
#iterations: 52
currently lose_sum: 99.16283106803894
time_elpased: 1.404
batch start
#iterations: 53
currently lose_sum: 99.09189969301224
time_elpased: 1.447
batch start
#iterations: 54
currently lose_sum: 99.06934612989426
time_elpased: 1.401
batch start
#iterations: 55
currently lose_sum: 99.05160617828369
time_elpased: 1.389
batch start
#iterations: 56
currently lose_sum: 99.07597649097443
time_elpased: 1.399
batch start
#iterations: 57
currently lose_sum: 99.15379589796066
time_elpased: 1.44
batch start
#iterations: 58
currently lose_sum: 99.1085637807846
time_elpased: 1.426
batch start
#iterations: 59
currently lose_sum: 99.04521852731705
time_elpased: 1.392
start validation test
0.587268041237
0.635671215487
0.412267160646
0.500156064673
0.587575282275
65.175
batch start
#iterations: 60
currently lose_sum: 98.88859742879868
time_elpased: 1.453
batch start
#iterations: 61
currently lose_sum: 99.13834804296494
time_elpased: 1.435
batch start
#iterations: 62
currently lose_sum: 98.96043837070465
time_elpased: 1.416
batch start
#iterations: 63
currently lose_sum: 99.23438221216202
time_elpased: 1.463
batch start
#iterations: 64
currently lose_sum: 99.0281001329422
time_elpased: 1.401
batch start
#iterations: 65
currently lose_sum: 98.98306965827942
time_elpased: 1.403
batch start
#iterations: 66
currently lose_sum: 98.81111127138138
time_elpased: 1.401
batch start
#iterations: 67
currently lose_sum: 98.87623029947281
time_elpased: 1.408
batch start
#iterations: 68
currently lose_sum: 99.16410237550735
time_elpased: 1.414
batch start
#iterations: 69
currently lose_sum: 98.89829188585281
time_elpased: 1.461
batch start
#iterations: 70
currently lose_sum: 98.93040585517883
time_elpased: 1.462
batch start
#iterations: 71
currently lose_sum: 99.02834552526474
time_elpased: 1.394
batch start
#iterations: 72
currently lose_sum: 98.95136559009552
time_elpased: 1.403
batch start
#iterations: 73
currently lose_sum: 98.96823155879974
time_elpased: 1.48
batch start
#iterations: 74
currently lose_sum: 99.1019401550293
time_elpased: 1.406
batch start
#iterations: 75
currently lose_sum: 98.99451071023941
time_elpased: 1.455
batch start
#iterations: 76
currently lose_sum: 99.0050557255745
time_elpased: 1.383
batch start
#iterations: 77
currently lose_sum: 99.25788754224777
time_elpased: 1.49
batch start
#iterations: 78
currently lose_sum: 98.86374539136887
time_elpased: 1.418
batch start
#iterations: 79
currently lose_sum: 99.15741515159607
time_elpased: 1.408
start validation test
0.590618556701
0.627313154497
0.450036019348
0.524089165868
0.590865371029
65.050
batch start
#iterations: 80
currently lose_sum: 98.98749250173569
time_elpased: 1.434
batch start
#iterations: 81
currently lose_sum: 98.83806240558624
time_elpased: 1.431
batch start
#iterations: 82
currently lose_sum: 99.09853947162628
time_elpased: 1.412
batch start
#iterations: 83
currently lose_sum: 98.96946084499359
time_elpased: 1.416
batch start
#iterations: 84
currently lose_sum: 98.89542818069458
time_elpased: 1.428
batch start
#iterations: 85
currently lose_sum: 99.00250333547592
time_elpased: 1.387
batch start
#iterations: 86
currently lose_sum: 98.98347157239914
time_elpased: 1.388
batch start
#iterations: 87
currently lose_sum: 99.0231226682663
time_elpased: 1.401
batch start
#iterations: 88
currently lose_sum: 98.8656964302063
time_elpased: 1.427
batch start
#iterations: 89
currently lose_sum: 98.8229228258133
time_elpased: 1.417
batch start
#iterations: 90
currently lose_sum: 99.0104079246521
time_elpased: 1.426
batch start
#iterations: 91
currently lose_sum: 99.19822853803635
time_elpased: 1.389
batch start
#iterations: 92
currently lose_sum: 98.84516459703445
time_elpased: 1.408
batch start
#iterations: 93
currently lose_sum: 98.8081905245781
time_elpased: 1.4
batch start
#iterations: 94
currently lose_sum: 99.0456554889679
time_elpased: 1.397
batch start
#iterations: 95
currently lose_sum: 98.96570426225662
time_elpased: 1.389
batch start
#iterations: 96
currently lose_sum: 98.68250519037247
time_elpased: 1.427
batch start
#iterations: 97
currently lose_sum: 98.70691740512848
time_elpased: 1.44
batch start
#iterations: 98
currently lose_sum: 98.9006484746933
time_elpased: 1.441
batch start
#iterations: 99
currently lose_sum: 98.6716040968895
time_elpased: 1.417
start validation test
0.597164948454
0.634778911565
0.46094473603
0.534072616705
0.597404104047
64.904
batch start
#iterations: 100
currently lose_sum: 98.85913532972336
time_elpased: 1.411
batch start
#iterations: 101
currently lose_sum: 98.90077716112137
time_elpased: 1.427
batch start
#iterations: 102
currently lose_sum: 99.14676856994629
time_elpased: 1.411
batch start
#iterations: 103
currently lose_sum: 98.72870171070099
time_elpased: 1.412
batch start
#iterations: 104
currently lose_sum: 98.95182174444199
time_elpased: 1.402
batch start
#iterations: 105
currently lose_sum: 98.86566942930222
time_elpased: 1.421
batch start
#iterations: 106
currently lose_sum: 98.7292086482048
time_elpased: 1.425
batch start
#iterations: 107
currently lose_sum: 98.91378438472748
time_elpased: 1.403
batch start
#iterations: 108
currently lose_sum: 98.99643504619598
time_elpased: 1.409
batch start
#iterations: 109
currently lose_sum: 98.86498957872391
time_elpased: 1.449
batch start
#iterations: 110
currently lose_sum: 98.81506186723709
time_elpased: 1.416
batch start
#iterations: 111
currently lose_sum: 98.61570686101913
time_elpased: 1.406
batch start
#iterations: 112
currently lose_sum: 98.88663506507874
time_elpased: 1.394
batch start
#iterations: 113
currently lose_sum: 98.77715772390366
time_elpased: 1.466
batch start
#iterations: 114
currently lose_sum: 98.92120361328125
time_elpased: 1.408
batch start
#iterations: 115
currently lose_sum: 98.8739664554596
time_elpased: 1.41
batch start
#iterations: 116
currently lose_sum: 98.89787864685059
time_elpased: 1.438
batch start
#iterations: 117
currently lose_sum: 98.40683752298355
time_elpased: 1.41
batch start
#iterations: 118
currently lose_sum: 98.98976904153824
time_elpased: 1.415
batch start
#iterations: 119
currently lose_sum: 99.01907646656036
time_elpased: 1.408
start validation test
0.597628865979
0.636949978501
0.457342801276
0.532406852761
0.597875159804
64.913
batch start
#iterations: 120
currently lose_sum: 98.88797205686569
time_elpased: 1.398
batch start
#iterations: 121
currently lose_sum: 98.57770854234695
time_elpased: 1.406
batch start
#iterations: 122
currently lose_sum: 99.0765420794487
time_elpased: 1.456
batch start
#iterations: 123
currently lose_sum: 98.9441390633583
time_elpased: 1.429
batch start
#iterations: 124
currently lose_sum: 98.99141544103622
time_elpased: 1.396
batch start
#iterations: 125
currently lose_sum: 99.04433345794678
time_elpased: 1.437
batch start
#iterations: 126
currently lose_sum: 98.70644575357437
time_elpased: 1.402
batch start
#iterations: 127
currently lose_sum: 99.03574758768082
time_elpased: 1.391
batch start
#iterations: 128
currently lose_sum: 98.84665340185165
time_elpased: 1.41
batch start
#iterations: 129
currently lose_sum: 99.01364403963089
time_elpased: 1.411
batch start
#iterations: 130
currently lose_sum: 98.74044382572174
time_elpased: 1.398
batch start
#iterations: 131
currently lose_sum: 98.93609112501144
time_elpased: 1.405
batch start
#iterations: 132
currently lose_sum: 99.087646484375
time_elpased: 1.438
batch start
#iterations: 133
currently lose_sum: 98.84712690114975
time_elpased: 1.432
batch start
#iterations: 134
currently lose_sum: 98.87590050697327
time_elpased: 1.435
batch start
#iterations: 135
currently lose_sum: 98.63739913702011
time_elpased: 1.407
batch start
#iterations: 136
currently lose_sum: 98.68903005123138
time_elpased: 1.403
batch start
#iterations: 137
currently lose_sum: 98.93767422437668
time_elpased: 1.403
batch start
#iterations: 138
currently lose_sum: 98.78991687297821
time_elpased: 1.413
batch start
#iterations: 139
currently lose_sum: 98.94368135929108
time_elpased: 1.457
start validation test
0.605463917526
0.62657994846
0.525470824328
0.571588492108
0.605604357739
64.608
batch start
#iterations: 140
currently lose_sum: 98.82453864812851
time_elpased: 1.422
batch start
#iterations: 141
currently lose_sum: 98.9805321097374
time_elpased: 1.434
batch start
#iterations: 142
currently lose_sum: 98.71487712860107
time_elpased: 1.403
batch start
#iterations: 143
currently lose_sum: 98.93562573194504
time_elpased: 1.404
batch start
#iterations: 144
currently lose_sum: 98.75503253936768
time_elpased: 1.459
batch start
#iterations: 145
currently lose_sum: 98.75157356262207
time_elpased: 1.403
batch start
#iterations: 146
currently lose_sum: 98.67655658721924
time_elpased: 1.403
batch start
#iterations: 147
currently lose_sum: 98.61301040649414
time_elpased: 1.409
batch start
#iterations: 148
currently lose_sum: 98.61097007989883
time_elpased: 1.397
batch start
#iterations: 149
currently lose_sum: 98.87095415592194
time_elpased: 1.443
batch start
#iterations: 150
currently lose_sum: 98.72545450925827
time_elpased: 1.42
batch start
#iterations: 151
currently lose_sum: 98.7686876654625
time_elpased: 1.428
batch start
#iterations: 152
currently lose_sum: 98.84705150127411
time_elpased: 1.432
batch start
#iterations: 153
currently lose_sum: 98.93306708335876
time_elpased: 1.421
batch start
#iterations: 154
currently lose_sum: 98.9692656993866
time_elpased: 1.455
batch start
#iterations: 155
currently lose_sum: 98.67262452840805
time_elpased: 1.44
batch start
#iterations: 156
currently lose_sum: 98.64520704746246
time_elpased: 1.454
batch start
#iterations: 157
currently lose_sum: 98.5957499742508
time_elpased: 1.419
batch start
#iterations: 158
currently lose_sum: 98.68688422441483
time_elpased: 1.401
batch start
#iterations: 159
currently lose_sum: 98.58612358570099
time_elpased: 1.408
start validation test
0.606082474227
0.622795597112
0.541525162087
0.57932401189
0.606195814546
64.578
batch start
#iterations: 160
currently lose_sum: 98.89404881000519
time_elpased: 1.429
batch start
#iterations: 161
currently lose_sum: 98.75394994020462
time_elpased: 1.432
batch start
#iterations: 162
currently lose_sum: 98.78040421009064
time_elpased: 1.419
batch start
#iterations: 163
currently lose_sum: 98.892642557621
time_elpased: 1.426
batch start
#iterations: 164
currently lose_sum: 98.60291230678558
time_elpased: 1.435
batch start
#iterations: 165
currently lose_sum: 98.82656490802765
time_elpased: 1.444
batch start
#iterations: 166
currently lose_sum: 98.64226877689362
time_elpased: 1.432
batch start
#iterations: 167
currently lose_sum: 98.93887627124786
time_elpased: 1.418
batch start
#iterations: 168
currently lose_sum: 98.59782010316849
time_elpased: 1.429
batch start
#iterations: 169
currently lose_sum: 98.85958951711655
time_elpased: 1.444
batch start
#iterations: 170
currently lose_sum: 98.56405824422836
time_elpased: 1.431
batch start
#iterations: 171
currently lose_sum: 98.97906595468521
time_elpased: 1.426
batch start
#iterations: 172
currently lose_sum: 98.7650865316391
time_elpased: 1.438
batch start
#iterations: 173
currently lose_sum: 98.93074268102646
time_elpased: 1.483
batch start
#iterations: 174
currently lose_sum: 98.60317140817642
time_elpased: 1.401
batch start
#iterations: 175
currently lose_sum: 98.45710039138794
time_elpased: 1.404
batch start
#iterations: 176
currently lose_sum: 98.63948458433151
time_elpased: 1.411
batch start
#iterations: 177
currently lose_sum: 98.69051456451416
time_elpased: 1.402
batch start
#iterations: 178
currently lose_sum: 98.95759284496307
time_elpased: 1.422
batch start
#iterations: 179
currently lose_sum: 98.56424736976624
time_elpased: 1.404
start validation test
0.606391752577
0.616975829117
0.564783369353
0.589727057812
0.606464802512
64.466
batch start
#iterations: 180
currently lose_sum: 98.56460458040237
time_elpased: 1.433
batch start
#iterations: 181
currently lose_sum: 98.73155164718628
time_elpased: 1.386
batch start
#iterations: 182
currently lose_sum: 98.78990805149078
time_elpased: 1.413
batch start
#iterations: 183
currently lose_sum: 98.5845199227333
time_elpased: 1.469
batch start
#iterations: 184
currently lose_sum: 98.89302706718445
time_elpased: 1.402
batch start
#iterations: 185
currently lose_sum: 98.68409997224808
time_elpased: 1.45
batch start
#iterations: 186
currently lose_sum: 98.6573776602745
time_elpased: 1.453
batch start
#iterations: 187
currently lose_sum: 98.64057278633118
time_elpased: 1.436
batch start
#iterations: 188
currently lose_sum: 98.90486758947372
time_elpased: 1.421
batch start
#iterations: 189
currently lose_sum: 98.79081308841705
time_elpased: 1.426
batch start
#iterations: 190
currently lose_sum: 98.80231297016144
time_elpased: 1.408
batch start
#iterations: 191
currently lose_sum: 98.61193460226059
time_elpased: 1.441
batch start
#iterations: 192
currently lose_sum: 98.62887734174728
time_elpased: 1.45
batch start
#iterations: 193
currently lose_sum: 98.85736167430878
time_elpased: 1.424
batch start
#iterations: 194
currently lose_sum: 98.5375047326088
time_elpased: 1.441
batch start
#iterations: 195
currently lose_sum: 98.6667150259018
time_elpased: 1.441
batch start
#iterations: 196
currently lose_sum: 98.72368574142456
time_elpased: 1.43
batch start
#iterations: 197
currently lose_sum: 98.64631134271622
time_elpased: 1.406
batch start
#iterations: 198
currently lose_sum: 98.68747794628143
time_elpased: 1.427
batch start
#iterations: 199
currently lose_sum: 98.81106877326965
time_elpased: 1.4
start validation test
0.604381443299
0.625399164824
0.524030050427
0.570244694552
0.604522512563
64.584
batch start
#iterations: 200
currently lose_sum: 98.67473047971725
time_elpased: 1.417
batch start
#iterations: 201
currently lose_sum: 98.71720939874649
time_elpased: 1.464
batch start
#iterations: 202
currently lose_sum: 98.68483102321625
time_elpased: 1.415
batch start
#iterations: 203
currently lose_sum: 98.57969725131989
time_elpased: 1.454
batch start
#iterations: 204
currently lose_sum: 98.51269644498825
time_elpased: 1.422
batch start
#iterations: 205
currently lose_sum: 98.6548912525177
time_elpased: 1.421
batch start
#iterations: 206
currently lose_sum: 98.51733529567719
time_elpased: 1.45
batch start
#iterations: 207
currently lose_sum: 98.62498903274536
time_elpased: 1.388
batch start
#iterations: 208
currently lose_sum: 98.68648874759674
time_elpased: 1.414
batch start
#iterations: 209
currently lose_sum: 98.6512839794159
time_elpased: 1.452
batch start
#iterations: 210
currently lose_sum: 98.4309818148613
time_elpased: 1.441
batch start
#iterations: 211
currently lose_sum: 98.75701969861984
time_elpased: 1.417
batch start
#iterations: 212
currently lose_sum: 98.42616564035416
time_elpased: 1.402
batch start
#iterations: 213
currently lose_sum: 98.71266996860504
time_elpased: 1.435
batch start
#iterations: 214
currently lose_sum: 98.70416069030762
time_elpased: 1.419
batch start
#iterations: 215
currently lose_sum: 98.66160047054291
time_elpased: 1.478
batch start
#iterations: 216
currently lose_sum: 98.60659497976303
time_elpased: 1.438
batch start
#iterations: 217
currently lose_sum: 98.48282408714294
time_elpased: 1.408
batch start
#iterations: 218
currently lose_sum: 98.32738786935806
time_elpased: 1.403
batch start
#iterations: 219
currently lose_sum: 98.4433131814003
time_elpased: 1.417
start validation test
0.602525773196
0.635871037659
0.483070906658
0.549037955436
0.602735494639
64.643
batch start
#iterations: 220
currently lose_sum: 98.92312216758728
time_elpased: 1.463
batch start
#iterations: 221
currently lose_sum: 98.5986596941948
time_elpased: 1.416
batch start
#iterations: 222
currently lose_sum: 98.49619805812836
time_elpased: 1.467
batch start
#iterations: 223
currently lose_sum: 98.70090103149414
time_elpased: 1.428
batch start
#iterations: 224
currently lose_sum: 98.58565551042557
time_elpased: 1.431
batch start
#iterations: 225
currently lose_sum: 98.6317812204361
time_elpased: 1.404
batch start
#iterations: 226
currently lose_sum: 98.84947806596756
time_elpased: 1.417
batch start
#iterations: 227
currently lose_sum: 98.47764277458191
time_elpased: 1.426
batch start
#iterations: 228
currently lose_sum: 98.81802868843079
time_elpased: 1.393
batch start
#iterations: 229
currently lose_sum: 98.78009963035583
time_elpased: 1.449
batch start
#iterations: 230
currently lose_sum: 98.44303780794144
time_elpased: 1.408
batch start
#iterations: 231
currently lose_sum: 98.5898859500885
time_elpased: 1.434
batch start
#iterations: 232
currently lose_sum: 98.34653145074844
time_elpased: 1.413
batch start
#iterations: 233
currently lose_sum: 98.49286365509033
time_elpased: 1.416
batch start
#iterations: 234
currently lose_sum: 98.49146908521652
time_elpased: 1.416
batch start
#iterations: 235
currently lose_sum: 98.84395217895508
time_elpased: 1.42
batch start
#iterations: 236
currently lose_sum: 98.70072489976883
time_elpased: 1.421
batch start
#iterations: 237
currently lose_sum: 98.70778280496597
time_elpased: 1.413
batch start
#iterations: 238
currently lose_sum: 98.74117469787598
time_elpased: 1.41
batch start
#iterations: 239
currently lose_sum: 98.8014246225357
time_elpased: 1.425
start validation test
0.606237113402
0.629648115797
0.519296079037
0.569172635497
0.606389751798
64.738
batch start
#iterations: 240
currently lose_sum: 98.55839097499847
time_elpased: 1.444
batch start
#iterations: 241
currently lose_sum: 98.65867549180984
time_elpased: 1.417
batch start
#iterations: 242
currently lose_sum: 98.54181027412415
time_elpased: 1.407
batch start
#iterations: 243
currently lose_sum: 98.68328946828842
time_elpased: 1.412
batch start
#iterations: 244
currently lose_sum: 98.75173848867416
time_elpased: 1.417
batch start
#iterations: 245
currently lose_sum: 98.66248857975006
time_elpased: 1.442
batch start
#iterations: 246
currently lose_sum: 98.55228793621063
time_elpased: 1.398
batch start
#iterations: 247
currently lose_sum: 98.51385545730591
time_elpased: 1.431
batch start
#iterations: 248
currently lose_sum: 98.57457846403122
time_elpased: 1.429
batch start
#iterations: 249
currently lose_sum: 98.44374024868011
time_elpased: 1.481
batch start
#iterations: 250
currently lose_sum: 98.43524235486984
time_elpased: 1.416
batch start
#iterations: 251
currently lose_sum: 98.65769785642624
time_elpased: 1.409
batch start
#iterations: 252
currently lose_sum: 98.61188489198685
time_elpased: 1.416
batch start
#iterations: 253
currently lose_sum: 98.68760108947754
time_elpased: 1.441
batch start
#iterations: 254
currently lose_sum: 98.48776626586914
time_elpased: 1.414
batch start
#iterations: 255
currently lose_sum: 98.61161309480667
time_elpased: 1.446
batch start
#iterations: 256
currently lose_sum: 98.69154155254364
time_elpased: 1.394
batch start
#iterations: 257
currently lose_sum: 98.384172976017
time_elpased: 1.396
batch start
#iterations: 258
currently lose_sum: 98.54917067289352
time_elpased: 1.389
batch start
#iterations: 259
currently lose_sum: 98.4917426109314
time_elpased: 1.433
start validation test
0.607268041237
0.628302348337
0.528661109396
0.574191024423
0.60740604783
64.460
batch start
#iterations: 260
currently lose_sum: 98.65163385868073
time_elpased: 1.457
batch start
#iterations: 261
currently lose_sum: 98.26187056303024
time_elpased: 1.397
batch start
#iterations: 262
currently lose_sum: 98.4402647614479
time_elpased: 1.432
batch start
#iterations: 263
currently lose_sum: 98.54700690507889
time_elpased: 1.417
batch start
#iterations: 264
currently lose_sum: 98.57619369029999
time_elpased: 1.423
batch start
#iterations: 265
currently lose_sum: 98.50986099243164
time_elpased: 1.449
batch start
#iterations: 266
currently lose_sum: 98.51511663198471
time_elpased: 1.436
batch start
#iterations: 267
currently lose_sum: 98.63267731666565
time_elpased: 1.424
batch start
#iterations: 268
currently lose_sum: 98.55339223146439
time_elpased: 1.412
batch start
#iterations: 269
currently lose_sum: 98.68909376859665
time_elpased: 1.417
batch start
#iterations: 270
currently lose_sum: 98.7674087882042
time_elpased: 1.421
batch start
#iterations: 271
currently lose_sum: 98.50019735097885
time_elpased: 1.443
batch start
#iterations: 272
currently lose_sum: 98.44623863697052
time_elpased: 1.49
batch start
#iterations: 273
currently lose_sum: 98.28206479549408
time_elpased: 1.475
batch start
#iterations: 274
currently lose_sum: 98.66478890180588
time_elpased: 1.406
batch start
#iterations: 275
currently lose_sum: 98.72696495056152
time_elpased: 1.446
batch start
#iterations: 276
currently lose_sum: 98.6883253455162
time_elpased: 1.408
batch start
#iterations: 277
currently lose_sum: 98.46842777729034
time_elpased: 1.474
batch start
#iterations: 278
currently lose_sum: 98.50493341684341
time_elpased: 1.38
batch start
#iterations: 279
currently lose_sum: 98.65469467639923
time_elpased: 1.442
start validation test
0.605
0.632447768893
0.50468251518
0.561387442047
0.605176122818
64.634
batch start
#iterations: 280
currently lose_sum: 98.54091173410416
time_elpased: 1.425
batch start
#iterations: 281
currently lose_sum: 98.32612007856369
time_elpased: 1.409
batch start
#iterations: 282
currently lose_sum: 98.71739840507507
time_elpased: 1.42
batch start
#iterations: 283
currently lose_sum: 98.47490447759628
time_elpased: 1.444
batch start
#iterations: 284
currently lose_sum: 98.56198757886887
time_elpased: 1.451
batch start
#iterations: 285
currently lose_sum: 98.5514018535614
time_elpased: 1.451
batch start
#iterations: 286
currently lose_sum: 98.56231355667114
time_elpased: 1.409
batch start
#iterations: 287
currently lose_sum: 98.51245534420013
time_elpased: 1.425
batch start
#iterations: 288
currently lose_sum: 98.51733148097992
time_elpased: 1.42
batch start
#iterations: 289
currently lose_sum: 98.61071908473969
time_elpased: 1.432
batch start
#iterations: 290
currently lose_sum: 98.48565804958344
time_elpased: 1.384
batch start
#iterations: 291
currently lose_sum: 98.43122452497482
time_elpased: 1.422
batch start
#iterations: 292
currently lose_sum: 98.41069453954697
time_elpased: 1.387
batch start
#iterations: 293
currently lose_sum: 98.54095876216888
time_elpased: 1.429
batch start
#iterations: 294
currently lose_sum: 98.73811024427414
time_elpased: 1.434
batch start
#iterations: 295
currently lose_sum: 98.5983315706253
time_elpased: 1.415
batch start
#iterations: 296
currently lose_sum: 98.57224601507187
time_elpased: 1.4
batch start
#iterations: 297
currently lose_sum: 98.36496406793594
time_elpased: 1.401
batch start
#iterations: 298
currently lose_sum: 98.5390276312828
time_elpased: 1.421
batch start
#iterations: 299
currently lose_sum: 98.58541840314865
time_elpased: 1.39
start validation test
0.599226804124
0.625940337224
0.4966553463
0.553853216274
0.599406884138
64.574
batch start
#iterations: 300
currently lose_sum: 98.50981682538986
time_elpased: 1.429
batch start
#iterations: 301
currently lose_sum: 98.69755393266678
time_elpased: 1.433
batch start
#iterations: 302
currently lose_sum: 98.57182627916336
time_elpased: 1.406
batch start
#iterations: 303
currently lose_sum: 98.51758944988251
time_elpased: 1.432
batch start
#iterations: 304
currently lose_sum: 98.48380446434021
time_elpased: 1.436
batch start
#iterations: 305
currently lose_sum: 98.29796147346497
time_elpased: 1.403
batch start
#iterations: 306
currently lose_sum: 98.51193445920944
time_elpased: 1.406
batch start
#iterations: 307
currently lose_sum: 98.7998149394989
time_elpased: 1.42
batch start
#iterations: 308
currently lose_sum: 98.6245481967926
time_elpased: 1.429
batch start
#iterations: 309
currently lose_sum: 98.45683020353317
time_elpased: 1.444
batch start
#iterations: 310
currently lose_sum: 98.61617475748062
time_elpased: 1.416
batch start
#iterations: 311
currently lose_sum: 98.43514287471771
time_elpased: 1.459
batch start
#iterations: 312
currently lose_sum: 98.55509519577026
time_elpased: 1.429
batch start
#iterations: 313
currently lose_sum: 98.50231063365936
time_elpased: 1.422
batch start
#iterations: 314
currently lose_sum: 98.53389126062393
time_elpased: 1.408
batch start
#iterations: 315
currently lose_sum: 98.46535611152649
time_elpased: 1.423
batch start
#iterations: 316
currently lose_sum: 98.3172954916954
time_elpased: 1.428
batch start
#iterations: 317
currently lose_sum: 98.49298846721649
time_elpased: 1.47
batch start
#iterations: 318
currently lose_sum: 98.39405703544617
time_elpased: 1.417
batch start
#iterations: 319
currently lose_sum: 98.7647739648819
time_elpased: 1.464
start validation test
0.606134020619
0.632600919775
0.509622311413
0.564491308065
0.60630346181
64.616
batch start
#iterations: 320
currently lose_sum: 98.50282675027847
time_elpased: 1.42
batch start
#iterations: 321
currently lose_sum: 98.74389946460724
time_elpased: 1.433
batch start
#iterations: 322
currently lose_sum: 98.49910885095596
time_elpased: 1.402
batch start
#iterations: 323
currently lose_sum: 98.66133064031601
time_elpased: 1.425
batch start
#iterations: 324
currently lose_sum: 98.64858853816986
time_elpased: 1.418
batch start
#iterations: 325
currently lose_sum: 98.76297825574875
time_elpased: 1.444
batch start
#iterations: 326
currently lose_sum: 98.41450387239456
time_elpased: 1.42
batch start
#iterations: 327
currently lose_sum: 98.5157739520073
time_elpased: 1.422
batch start
#iterations: 328
currently lose_sum: 98.58981341123581
time_elpased: 1.486
batch start
#iterations: 329
currently lose_sum: 98.49352824687958
time_elpased: 1.425
batch start
#iterations: 330
currently lose_sum: 98.42759132385254
time_elpased: 1.406
batch start
#iterations: 331
currently lose_sum: 98.55785757303238
time_elpased: 1.445
batch start
#iterations: 332
currently lose_sum: 98.31848460435867
time_elpased: 1.445
batch start
#iterations: 333
currently lose_sum: 98.59359008073807
time_elpased: 1.443
batch start
#iterations: 334
currently lose_sum: 98.50440818071365
time_elpased: 1.403
batch start
#iterations: 335
currently lose_sum: 98.48470371961594
time_elpased: 1.458
batch start
#iterations: 336
currently lose_sum: 98.40685880184174
time_elpased: 1.466
batch start
#iterations: 337
currently lose_sum: 98.91712617874146
time_elpased: 1.47
batch start
#iterations: 338
currently lose_sum: 98.4227728843689
time_elpased: 1.419
batch start
#iterations: 339
currently lose_sum: 98.55176097154617
time_elpased: 1.412
start validation test
0.609226804124
0.625617501764
0.547391170114
0.583895932817
0.609335366117
64.475
batch start
#iterations: 340
currently lose_sum: 98.62492114305496
time_elpased: 1.474
batch start
#iterations: 341
currently lose_sum: 98.65553021430969
time_elpased: 1.412
batch start
#iterations: 342
currently lose_sum: 98.63141399621964
time_elpased: 1.405
batch start
#iterations: 343
currently lose_sum: 98.50013822317123
time_elpased: 1.404
batch start
#iterations: 344
currently lose_sum: 98.34223449230194
time_elpased: 1.439
batch start
#iterations: 345
currently lose_sum: 98.64920872449875
time_elpased: 1.467
batch start
#iterations: 346
currently lose_sum: 98.54550784826279
time_elpased: 1.403
batch start
#iterations: 347
currently lose_sum: 98.36653292179108
time_elpased: 1.412
batch start
#iterations: 348
currently lose_sum: 98.60368311405182
time_elpased: 1.397
batch start
#iterations: 349
currently lose_sum: 98.48950493335724
time_elpased: 1.455
batch start
#iterations: 350
currently lose_sum: 98.77570909261703
time_elpased: 1.4
batch start
#iterations: 351
currently lose_sum: 98.54717189073563
time_elpased: 1.412
batch start
#iterations: 352
currently lose_sum: 98.4733458161354
time_elpased: 1.407
batch start
#iterations: 353
currently lose_sum: 98.54018425941467
time_elpased: 1.4
batch start
#iterations: 354
currently lose_sum: 98.74604654312134
time_elpased: 1.403
batch start
#iterations: 355
currently lose_sum: 98.4520154595375
time_elpased: 1.424
batch start
#iterations: 356
currently lose_sum: 98.52578949928284
time_elpased: 1.394
batch start
#iterations: 357
currently lose_sum: 98.53670358657837
time_elpased: 1.407
batch start
#iterations: 358
currently lose_sum: 98.36467164754868
time_elpased: 1.395
batch start
#iterations: 359
currently lose_sum: 98.63852137327194
time_elpased: 1.403
start validation test
0.605721649485
0.636519672564
0.496140784193
0.557631137586
0.605914035595
64.580
batch start
#iterations: 360
currently lose_sum: 98.63045012950897
time_elpased: 1.425
batch start
#iterations: 361
currently lose_sum: 98.67758017778397
time_elpased: 1.422
batch start
#iterations: 362
currently lose_sum: 98.51354646682739
time_elpased: 1.441
batch start
#iterations: 363
currently lose_sum: 98.39107668399811
time_elpased: 1.477
batch start
#iterations: 364
currently lose_sum: 98.55573600530624
time_elpased: 1.449
batch start
#iterations: 365
currently lose_sum: 98.647784948349
time_elpased: 1.425
batch start
#iterations: 366
currently lose_sum: 98.79574173688889
time_elpased: 1.402
batch start
#iterations: 367
currently lose_sum: 98.26354867219925
time_elpased: 1.431
batch start
#iterations: 368
currently lose_sum: 98.6072501540184
time_elpased: 1.466
batch start
#iterations: 369
currently lose_sum: 98.47312718629837
time_elpased: 1.467
batch start
#iterations: 370
currently lose_sum: 98.44827353954315
time_elpased: 1.425
batch start
#iterations: 371
currently lose_sum: 98.55739414691925
time_elpased: 1.412
batch start
#iterations: 372
currently lose_sum: 98.42119371891022
time_elpased: 1.39
batch start
#iterations: 373
currently lose_sum: 98.35836213827133
time_elpased: 1.41
batch start
#iterations: 374
currently lose_sum: 98.42791336774826
time_elpased: 1.416
batch start
#iterations: 375
currently lose_sum: 98.43623119592667
time_elpased: 1.423
batch start
#iterations: 376
currently lose_sum: 98.26600301265717
time_elpased: 1.445
batch start
#iterations: 377
currently lose_sum: 98.46697479486465
time_elpased: 1.442
batch start
#iterations: 378
currently lose_sum: 98.48932498693466
time_elpased: 1.439
batch start
#iterations: 379
currently lose_sum: 98.64331871271133
time_elpased: 1.445
start validation test
0.606030927835
0.622028712638
0.543995060204
0.580400768597
0.606139841369
64.487
batch start
#iterations: 380
currently lose_sum: 98.43009144067764
time_elpased: 1.421
batch start
#iterations: 381
currently lose_sum: 98.4144868850708
time_elpased: 1.415
batch start
#iterations: 382
currently lose_sum: 98.50441282987595
time_elpased: 1.416
batch start
#iterations: 383
currently lose_sum: 98.4801675081253
time_elpased: 1.409
batch start
#iterations: 384
currently lose_sum: 98.79970222711563
time_elpased: 1.436
batch start
#iterations: 385
currently lose_sum: 98.75895923376083
time_elpased: 1.51
batch start
#iterations: 386
currently lose_sum: 98.68621784448624
time_elpased: 1.406
batch start
#iterations: 387
currently lose_sum: 98.59719783067703
time_elpased: 1.447
batch start
#iterations: 388
currently lose_sum: 98.62022596597672
time_elpased: 1.403
batch start
#iterations: 389
currently lose_sum: 98.43995082378387
time_elpased: 1.455
batch start
#iterations: 390
currently lose_sum: 98.50360995531082
time_elpased: 1.41
batch start
#iterations: 391
currently lose_sum: 98.40981948375702
time_elpased: 1.402
batch start
#iterations: 392
currently lose_sum: 98.61389297246933
time_elpased: 1.404
batch start
#iterations: 393
currently lose_sum: 98.50447702407837
time_elpased: 1.425
batch start
#iterations: 394
currently lose_sum: 98.5378350019455
time_elpased: 1.421
batch start
#iterations: 395
currently lose_sum: 98.298235476017
time_elpased: 1.436
batch start
#iterations: 396
currently lose_sum: 98.56482690572739
time_elpased: 1.423
batch start
#iterations: 397
currently lose_sum: 98.6245978474617
time_elpased: 1.44
batch start
#iterations: 398
currently lose_sum: 98.66312712430954
time_elpased: 1.425
batch start
#iterations: 399
currently lose_sum: 98.3230437040329
time_elpased: 1.466
start validation test
0.604896907216
0.634469200524
0.498199032623
0.558136853635
0.605084231792
64.493
acc: 0.607
pre: 0.629
rec: 0.524
F1: 0.572
auc: 0.639
