start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.43179881572723
time_elpased: 2.147
batch start
#iterations: 1
currently lose_sum: 100.40946978330612
time_elpased: 2.016
batch start
#iterations: 2
currently lose_sum: 100.24934822320938
time_elpased: 1.98
batch start
#iterations: 3
currently lose_sum: 100.14876818656921
time_elpased: 1.947
batch start
#iterations: 4
currently lose_sum: 100.01710504293442
time_elpased: 2.032
batch start
#iterations: 5
currently lose_sum: 99.95496129989624
time_elpased: 2.021
batch start
#iterations: 6
currently lose_sum: 99.93888556957245
time_elpased: 2.024
batch start
#iterations: 7
currently lose_sum: 99.78108483552933
time_elpased: 2.014
batch start
#iterations: 8
currently lose_sum: 99.65199822187424
time_elpased: 1.99
batch start
#iterations: 9
currently lose_sum: 99.70213347673416
time_elpased: 1.932
batch start
#iterations: 10
currently lose_sum: 99.71787816286087
time_elpased: 2.004
batch start
#iterations: 11
currently lose_sum: 99.62342447042465
time_elpased: 2.003
batch start
#iterations: 12
currently lose_sum: 99.38565164804459
time_elpased: 2.008
batch start
#iterations: 13
currently lose_sum: 99.40243923664093
time_elpased: 1.999
batch start
#iterations: 14
currently lose_sum: 99.27531832456589
time_elpased: 1.988
batch start
#iterations: 15
currently lose_sum: 99.15113097429276
time_elpased: 1.944
batch start
#iterations: 16
currently lose_sum: 99.16526907682419
time_elpased: 1.985
batch start
#iterations: 17
currently lose_sum: 98.97311967611313
time_elpased: 2.006
batch start
#iterations: 18
currently lose_sum: 98.87318068742752
time_elpased: 2.006
batch start
#iterations: 19
currently lose_sum: 98.88144081830978
time_elpased: 2.024
start validation test
0.582113402062
0.554739562084
0.839559534836
0.668058797036
0.581661415667
65.347
batch start
#iterations: 20
currently lose_sum: 98.86916863918304
time_elpased: 2.006
batch start
#iterations: 21
currently lose_sum: 98.61478674411774
time_elpased: 1.946
batch start
#iterations: 22
currently lose_sum: 98.78815007209778
time_elpased: 2.017
batch start
#iterations: 23
currently lose_sum: 98.34252345561981
time_elpased: 2.058
batch start
#iterations: 24
currently lose_sum: 98.14856910705566
time_elpased: 2.074
batch start
#iterations: 25
currently lose_sum: 98.47333270311356
time_elpased: 2.055
batch start
#iterations: 26
currently lose_sum: 98.47477108240128
time_elpased: 2.077
batch start
#iterations: 27
currently lose_sum: 98.3810071349144
time_elpased: 2.071
batch start
#iterations: 28
currently lose_sum: 98.01694017648697
time_elpased: 1.914
batch start
#iterations: 29
currently lose_sum: 98.22002357244492
time_elpased: 1.857
batch start
#iterations: 30
currently lose_sum: 98.11319106817245
time_elpased: 1.889
batch start
#iterations: 31
currently lose_sum: 98.11442267894745
time_elpased: 1.942
batch start
#iterations: 32
currently lose_sum: 98.25790679454803
time_elpased: 1.9
batch start
#iterations: 33
currently lose_sum: 97.99632179737091
time_elpased: 1.905
batch start
#iterations: 34
currently lose_sum: 97.64628344774246
time_elpased: 1.955
batch start
#iterations: 35
currently lose_sum: 98.01261931657791
time_elpased: 1.924
batch start
#iterations: 36
currently lose_sum: 97.60458153486252
time_elpased: 1.982
batch start
#iterations: 37
currently lose_sum: 97.46543723344803
time_elpased: 1.948
batch start
#iterations: 38
currently lose_sum: 97.36797714233398
time_elpased: 1.886
batch start
#iterations: 39
currently lose_sum: 97.49837070703506
time_elpased: 2.013
start validation test
0.618298969072
0.642189421894
0.537305752804
0.585084327898
0.618441165156
63.692
batch start
#iterations: 40
currently lose_sum: 97.61914610862732
time_elpased: 1.965
batch start
#iterations: 41
currently lose_sum: 97.41625690460205
time_elpased: 1.994
batch start
#iterations: 42
currently lose_sum: 97.72210657596588
time_elpased: 2.008
batch start
#iterations: 43
currently lose_sum: 97.15611189603806
time_elpased: 2.029
batch start
#iterations: 44
currently lose_sum: 97.52181315422058
time_elpased: 1.848
batch start
#iterations: 45
currently lose_sum: 97.24544507265091
time_elpased: 1.865
batch start
#iterations: 46
currently lose_sum: 97.2084566950798
time_elpased: 2.042
batch start
#iterations: 47
currently lose_sum: 97.47350436449051
time_elpased: 1.95
batch start
#iterations: 48
currently lose_sum: 97.05714404582977
time_elpased: 1.953
batch start
#iterations: 49
currently lose_sum: 97.19948476552963
time_elpased: 1.919
batch start
#iterations: 50
currently lose_sum: 97.07502943277359
time_elpased: 1.938
batch start
#iterations: 51
currently lose_sum: 97.14963346719742
time_elpased: 1.985
batch start
#iterations: 52
currently lose_sum: 97.42507481575012
time_elpased: 1.986
batch start
#iterations: 53
currently lose_sum: 96.68480533361435
time_elpased: 1.98
batch start
#iterations: 54
currently lose_sum: 97.25958466529846
time_elpased: 1.889
batch start
#iterations: 55
currently lose_sum: 96.87640172243118
time_elpased: 1.868
batch start
#iterations: 56
currently lose_sum: 96.50411921739578
time_elpased: 1.942
batch start
#iterations: 57
currently lose_sum: 96.83586698770523
time_elpased: 1.866
batch start
#iterations: 58
currently lose_sum: 96.85072082281113
time_elpased: 1.996
batch start
#iterations: 59
currently lose_sum: 96.69744861125946
time_elpased: 2.005
start validation test
0.594329896907
0.675137492888
0.366368220644
0.474983322215
0.594730118791
63.531
batch start
#iterations: 60
currently lose_sum: 97.02661073207855
time_elpased: 2.026
batch start
#iterations: 61
currently lose_sum: 97.013926923275
time_elpased: 2.024
batch start
#iterations: 62
currently lose_sum: 96.60230445861816
time_elpased: 1.956
batch start
#iterations: 63
currently lose_sum: 96.68698823451996
time_elpased: 1.958
batch start
#iterations: 64
currently lose_sum: 96.85888367891312
time_elpased: 1.977
batch start
#iterations: 65
currently lose_sum: 96.55367374420166
time_elpased: 1.972
batch start
#iterations: 66
currently lose_sum: 96.67732399702072
time_elpased: 1.948
batch start
#iterations: 67
currently lose_sum: 96.52944338321686
time_elpased: 2.062
batch start
#iterations: 68
currently lose_sum: 96.57551729679108
time_elpased: 1.924
batch start
#iterations: 69
currently lose_sum: 96.39732623100281
time_elpased: 1.919
batch start
#iterations: 70
currently lose_sum: 96.77795058488846
time_elpased: 1.951
batch start
#iterations: 71
currently lose_sum: 96.3677122592926
time_elpased: 1.988
batch start
#iterations: 72
currently lose_sum: 96.09489285945892
time_elpased: 2.055
batch start
#iterations: 73
currently lose_sum: 96.42308592796326
time_elpased: 2.062
batch start
#iterations: 74
currently lose_sum: 96.53634840250015
time_elpased: 1.968
batch start
#iterations: 75
currently lose_sum: 96.48898869752884
time_elpased: 2.01
batch start
#iterations: 76
currently lose_sum: 96.10048884153366
time_elpased: 1.888
batch start
#iterations: 77
currently lose_sum: 96.1788290143013
time_elpased: 1.865
batch start
#iterations: 78
currently lose_sum: 96.54142725467682
time_elpased: 1.963
batch start
#iterations: 79
currently lose_sum: 96.161576628685
time_elpased: 1.906
start validation test
0.591082474227
0.66849263317
0.364207059792
0.471520884685
0.59148078901
63.516
batch start
#iterations: 80
currently lose_sum: 96.14453911781311
time_elpased: 1.994
batch start
#iterations: 81
currently lose_sum: 96.34735810756683
time_elpased: 2.087
batch start
#iterations: 82
currently lose_sum: 96.0160002708435
time_elpased: 1.957
batch start
#iterations: 83
currently lose_sum: 96.17551273107529
time_elpased: 1.896
batch start
#iterations: 84
currently lose_sum: 95.58195298910141
time_elpased: 1.906
batch start
#iterations: 85
currently lose_sum: 95.95315963029861
time_elpased: 1.82
batch start
#iterations: 86
currently lose_sum: 96.44821083545685
time_elpased: 1.818
batch start
#iterations: 87
currently lose_sum: 95.76581937074661
time_elpased: 1.853
batch start
#iterations: 88
currently lose_sum: 95.94994986057281
time_elpased: 1.892
batch start
#iterations: 89
currently lose_sum: 96.410684466362
time_elpased: 2.023
batch start
#iterations: 90
currently lose_sum: 95.57597541809082
time_elpased: 1.98
batch start
#iterations: 91
currently lose_sum: 95.62467670440674
time_elpased: 1.852
batch start
#iterations: 92
currently lose_sum: 95.84343588352203
time_elpased: 1.856
batch start
#iterations: 93
currently lose_sum: 95.53647834062576
time_elpased: 1.884
batch start
#iterations: 94
currently lose_sum: 95.53074979782104
time_elpased: 1.99
batch start
#iterations: 95
currently lose_sum: 95.88510125875473
time_elpased: 1.872
batch start
#iterations: 96
currently lose_sum: 95.93289303779602
time_elpased: 1.809
batch start
#iterations: 97
currently lose_sum: 95.02137476205826
time_elpased: 1.814
batch start
#iterations: 98
currently lose_sum: 95.08126586675644
time_elpased: 1.803
batch start
#iterations: 99
currently lose_sum: 95.66019469499588
time_elpased: 1.876
start validation test
0.626030927835
0.669886834115
0.499536894103
0.572304427283
0.626253007622
62.390
batch start
#iterations: 100
currently lose_sum: 95.43119353055954
time_elpased: 2.018
batch start
#iterations: 101
currently lose_sum: 95.73778355121613
time_elpased: 2.001
batch start
#iterations: 102
currently lose_sum: 95.33814060688019
time_elpased: 1.968
batch start
#iterations: 103
currently lose_sum: 95.7974351644516
time_elpased: 1.952
batch start
#iterations: 104
currently lose_sum: 95.72207641601562
time_elpased: 1.93
batch start
#iterations: 105
currently lose_sum: 95.27492880821228
time_elpased: 1.842
batch start
#iterations: 106
currently lose_sum: 95.33120411634445
time_elpased: 1.844
batch start
#iterations: 107
currently lose_sum: 95.5069710612297
time_elpased: 1.904
batch start
#iterations: 108
currently lose_sum: 94.96944433450699
time_elpased: 1.841
batch start
#iterations: 109
currently lose_sum: 95.21702438592911
time_elpased: 1.802
batch start
#iterations: 110
currently lose_sum: 94.96497321128845
time_elpased: 1.837
batch start
#iterations: 111
currently lose_sum: 95.29863822460175
time_elpased: 1.828
batch start
#iterations: 112
currently lose_sum: 95.62007981538773
time_elpased: 1.824
batch start
#iterations: 113
currently lose_sum: 94.7292668223381
time_elpased: 1.943
batch start
#iterations: 114
currently lose_sum: 95.09403067827225
time_elpased: 1.949
batch start
#iterations: 115
currently lose_sum: 95.24077308177948
time_elpased: 2.021
batch start
#iterations: 116
currently lose_sum: 95.4471925497055
time_elpased: 2.016
batch start
#iterations: 117
currently lose_sum: 95.22735691070557
time_elpased: 2.022
batch start
#iterations: 118
currently lose_sum: 95.46570432186127
time_elpased: 1.951
batch start
#iterations: 119
currently lose_sum: 94.89554572105408
time_elpased: 1.87
start validation test
0.643505154639
0.651782811315
0.618915303077
0.634923986486
0.643548325916
61.923
batch start
#iterations: 120
currently lose_sum: 95.0443617105484
time_elpased: 2.026
batch start
#iterations: 121
currently lose_sum: 95.21066009998322
time_elpased: 1.943
batch start
#iterations: 122
currently lose_sum: 94.90601468086243
time_elpased: 2.006
batch start
#iterations: 123
currently lose_sum: 94.60093104839325
time_elpased: 1.973
batch start
#iterations: 124
currently lose_sum: 94.8642207980156
time_elpased: 1.938
batch start
#iterations: 125
currently lose_sum: 94.80036300420761
time_elpased: 1.955
batch start
#iterations: 126
currently lose_sum: 94.73563361167908
time_elpased: 1.794
batch start
#iterations: 127
currently lose_sum: 94.94547814130783
time_elpased: 1.84
batch start
#iterations: 128
currently lose_sum: 94.71482586860657
time_elpased: 1.817
batch start
#iterations: 129
currently lose_sum: 94.32798510789871
time_elpased: 1.837
batch start
#iterations: 130
currently lose_sum: 94.77262097597122
time_elpased: 1.825
batch start
#iterations: 131
currently lose_sum: 94.82952332496643
time_elpased: 1.961
batch start
#iterations: 132
currently lose_sum: 94.67170941829681
time_elpased: 1.993
batch start
#iterations: 133
currently lose_sum: 94.56739276647568
time_elpased: 2.065
batch start
#iterations: 134
currently lose_sum: 94.43403422832489
time_elpased: 2.053
batch start
#iterations: 135
currently lose_sum: 94.12278652191162
time_elpased: 2.024
batch start
#iterations: 136
currently lose_sum: 94.60472631454468
time_elpased: 1.956
batch start
#iterations: 137
currently lose_sum: 94.51851630210876
time_elpased: 1.806
batch start
#iterations: 138
currently lose_sum: 94.47143650054932
time_elpased: 1.789
batch start
#iterations: 139
currently lose_sum: 94.15561556816101
time_elpased: 1.862
start validation test
0.622886597938
0.663444520082
0.501492230112
0.571210877974
0.623099724475
62.282
batch start
#iterations: 140
currently lose_sum: 94.22283208370209
time_elpased: 1.879
batch start
#iterations: 141
currently lose_sum: 94.57024365663528
time_elpased: 1.967
batch start
#iterations: 142
currently lose_sum: 94.10571670532227
time_elpased: 1.984
batch start
#iterations: 143
currently lose_sum: 93.85636043548584
time_elpased: 2.021
batch start
#iterations: 144
currently lose_sum: 94.01080346107483
time_elpased: 2.007
batch start
#iterations: 145
currently lose_sum: 94.65419542789459
time_elpased: 1.989
batch start
#iterations: 146
currently lose_sum: 93.82546836137772
time_elpased: 1.953
batch start
#iterations: 147
currently lose_sum: 94.20683056116104
time_elpased: 1.968
batch start
#iterations: 148
currently lose_sum: 94.18588906526566
time_elpased: 1.843
batch start
#iterations: 149
currently lose_sum: 93.66197282075882
time_elpased: 1.913
batch start
#iterations: 150
currently lose_sum: 94.47362214326859
time_elpased: 1.904
batch start
#iterations: 151
currently lose_sum: 94.35895127058029
time_elpased: 1.938
batch start
#iterations: 152
currently lose_sum: 94.11729139089584
time_elpased: 1.956
batch start
#iterations: 153
currently lose_sum: 93.48491191864014
time_elpased: 1.903
batch start
#iterations: 154
currently lose_sum: 94.35452044010162
time_elpased: 1.825
batch start
#iterations: 155
currently lose_sum: 94.19281846284866
time_elpased: 1.88
batch start
#iterations: 156
currently lose_sum: 93.9658173918724
time_elpased: 1.78
batch start
#iterations: 157
currently lose_sum: 93.9629180431366
time_elpased: 1.846
batch start
#iterations: 158
currently lose_sum: 93.91274523735046
time_elpased: 1.824
batch start
#iterations: 159
currently lose_sum: 94.53650748729706
time_elpased: 1.904
start validation test
0.646340206186
0.644945188794
0.653905526397
0.649394450406
0.646326924099
61.772
batch start
#iterations: 160
currently lose_sum: 93.84827703237534
time_elpased: 2.019
batch start
#iterations: 161
currently lose_sum: 94.7621328830719
time_elpased: 2.026
batch start
#iterations: 162
currently lose_sum: 93.3264953494072
time_elpased: 1.951
batch start
#iterations: 163
currently lose_sum: 93.3062983751297
time_elpased: 1.782
batch start
#iterations: 164
currently lose_sum: 94.04297524690628
time_elpased: 1.788
batch start
#iterations: 165
currently lose_sum: 94.30580914020538
time_elpased: 1.798
batch start
#iterations: 166
currently lose_sum: 93.04998183250427
time_elpased: 1.79
batch start
#iterations: 167
currently lose_sum: 93.86143904924393
time_elpased: 1.777
batch start
#iterations: 168
currently lose_sum: 93.56373381614685
time_elpased: 1.785
batch start
#iterations: 169
currently lose_sum: 93.7270273566246
time_elpased: 1.821
batch start
#iterations: 170
currently lose_sum: 93.28849577903748
time_elpased: 1.804
batch start
#iterations: 171
currently lose_sum: 93.98438823223114
time_elpased: 1.926
batch start
#iterations: 172
currently lose_sum: 93.89679700136185
time_elpased: 1.955
batch start
#iterations: 173
currently lose_sum: 93.61695623397827
time_elpased: 1.906
batch start
#iterations: 174
currently lose_sum: 93.05464470386505
time_elpased: 1.922
batch start
#iterations: 175
currently lose_sum: 93.05474954843521
time_elpased: 2.009
batch start
#iterations: 176
currently lose_sum: 93.4243294596672
time_elpased: 1.872
batch start
#iterations: 177
currently lose_sum: 93.19299411773682
time_elpased: 1.842
batch start
#iterations: 178
currently lose_sum: 93.62497824430466
time_elpased: 1.885
batch start
#iterations: 179
currently lose_sum: 93.11499178409576
time_elpased: 1.86
start validation test
0.63412371134
0.645645645646
0.597406606977
0.620590121873
0.63418817388
61.864
batch start
#iterations: 180
currently lose_sum: 92.6311583518982
time_elpased: 1.793
batch start
#iterations: 181
currently lose_sum: 93.47551476955414
time_elpased: 1.791
batch start
#iterations: 182
currently lose_sum: 93.67945462465286
time_elpased: 1.818
batch start
#iterations: 183
currently lose_sum: 92.91962736845016
time_elpased: 1.813
batch start
#iterations: 184
currently lose_sum: 93.0258378982544
time_elpased: 1.978
batch start
#iterations: 185
currently lose_sum: 93.82340294122696
time_elpased: 1.971
batch start
#iterations: 186
currently lose_sum: 93.0353872179985
time_elpased: 1.973
batch start
#iterations: 187
currently lose_sum: 93.47647041082382
time_elpased: 1.995
batch start
#iterations: 188
currently lose_sum: 92.94468116760254
time_elpased: 1.801
batch start
#iterations: 189
currently lose_sum: 93.3574658036232
time_elpased: 1.826
batch start
#iterations: 190
currently lose_sum: 93.48824948072433
time_elpased: 1.965
batch start
#iterations: 191
currently lose_sum: 93.10576379299164
time_elpased: 1.998
batch start
#iterations: 192
currently lose_sum: 93.06439542770386
time_elpased: 1.973
batch start
#iterations: 193
currently lose_sum: 92.88596379756927
time_elpased: 1.993
batch start
#iterations: 194
currently lose_sum: 93.34549254179001
time_elpased: 1.855
batch start
#iterations: 195
currently lose_sum: 92.74197632074356
time_elpased: 1.876
batch start
#iterations: 196
currently lose_sum: 92.79123485088348
time_elpased: 1.993
batch start
#iterations: 197
currently lose_sum: 92.56152433156967
time_elpased: 1.94
batch start
#iterations: 198
currently lose_sum: 93.50648057460785
time_elpased: 1.943
batch start
#iterations: 199
currently lose_sum: 92.83338356018066
time_elpased: 1.844
start validation test
0.639690721649
0.643149606299
0.630441494288
0.636732148425
0.639706960095
61.679
batch start
#iterations: 200
currently lose_sum: 92.80227470397949
time_elpased: 1.853
batch start
#iterations: 201
currently lose_sum: 92.4560335278511
time_elpased: 1.891
batch start
#iterations: 202
currently lose_sum: 92.81372821331024
time_elpased: 1.892
batch start
#iterations: 203
currently lose_sum: 92.37547934055328
time_elpased: 1.913
batch start
#iterations: 204
currently lose_sum: 92.83828675746918
time_elpased: 1.893
batch start
#iterations: 205
currently lose_sum: 92.57100421190262
time_elpased: 1.914
batch start
#iterations: 206
currently lose_sum: 92.14840525388718
time_elpased: 1.85
batch start
#iterations: 207
currently lose_sum: 92.4904654622078
time_elpased: 1.87
batch start
#iterations: 208
currently lose_sum: 92.58404940366745
time_elpased: 1.913
batch start
#iterations: 209
currently lose_sum: 92.31495290994644
time_elpased: 1.948
batch start
#iterations: 210
currently lose_sum: 92.69000059366226
time_elpased: 1.966
batch start
#iterations: 211
currently lose_sum: 92.71760749816895
time_elpased: 1.944
batch start
#iterations: 212
currently lose_sum: 92.9006752371788
time_elpased: 1.943
batch start
#iterations: 213
currently lose_sum: 92.37027889490128
time_elpased: 1.939
batch start
#iterations: 214
currently lose_sum: 92.43259257078171
time_elpased: 1.897
batch start
#iterations: 215
currently lose_sum: 92.32620847225189
time_elpased: 1.989
batch start
#iterations: 216
currently lose_sum: 92.13752615451813
time_elpased: 1.897
batch start
#iterations: 217
currently lose_sum: 92.77976381778717
time_elpased: 1.938
batch start
#iterations: 218
currently lose_sum: 91.59071081876755
time_elpased: 1.872
batch start
#iterations: 219
currently lose_sum: 92.84109389781952
time_elpased: 1.97
start validation test
0.640618556701
0.640725930483
0.643099722136
0.641910631741
0.640614200632
61.822
batch start
#iterations: 220
currently lose_sum: 92.27079403400421
time_elpased: 1.955
batch start
#iterations: 221
currently lose_sum: 92.31839776039124
time_elpased: 1.959
batch start
#iterations: 222
currently lose_sum: 92.2403718829155
time_elpased: 1.908
batch start
#iterations: 223
currently lose_sum: 92.21795094013214
time_elpased: 1.96
batch start
#iterations: 224
currently lose_sum: 92.18993037939072
time_elpased: 1.91
batch start
#iterations: 225
currently lose_sum: 91.80181610584259
time_elpased: 1.848
batch start
#iterations: 226
currently lose_sum: 91.65269327163696
time_elpased: 2.005
batch start
#iterations: 227
currently lose_sum: 92.4218116402626
time_elpased: 1.946
batch start
#iterations: 228
currently lose_sum: 91.54523402452469
time_elpased: 1.938
batch start
#iterations: 229
currently lose_sum: 92.06721436977386
time_elpased: 1.935
batch start
#iterations: 230
currently lose_sum: 91.85450619459152
time_elpased: 1.848
batch start
#iterations: 231
currently lose_sum: 92.82056623697281
time_elpased: 1.972
batch start
#iterations: 232
currently lose_sum: 91.53610473871231
time_elpased: 1.859
batch start
#iterations: 233
currently lose_sum: 91.733278632164
time_elpased: 1.93
batch start
#iterations: 234
currently lose_sum: 92.35651081800461
time_elpased: 1.86
batch start
#iterations: 235
currently lose_sum: 92.07919484376907
time_elpased: 1.908
batch start
#iterations: 236
currently lose_sum: 92.33108133077621
time_elpased: 1.92
batch start
#iterations: 237
currently lose_sum: 91.98725026845932
time_elpased: 1.889
batch start
#iterations: 238
currently lose_sum: 91.28935754299164
time_elpased: 1.936
batch start
#iterations: 239
currently lose_sum: 91.86740964651108
time_elpased: 2.002
start validation test
0.625103092784
0.653171220857
0.536276628589
0.588979937836
0.625259041342
61.985
batch start
#iterations: 240
currently lose_sum: 90.85128653049469
time_elpased: 2.002
batch start
#iterations: 241
currently lose_sum: 92.20211774110794
time_elpased: 1.984
batch start
#iterations: 242
currently lose_sum: 91.69353151321411
time_elpased: 1.962
batch start
#iterations: 243
currently lose_sum: 92.69241744279861
time_elpased: 1.919
batch start
#iterations: 244
currently lose_sum: 91.3961626291275
time_elpased: 1.936
batch start
#iterations: 245
currently lose_sum: 91.88749915361404
time_elpased: 1.92
batch start
#iterations: 246
currently lose_sum: 91.09744673967361
time_elpased: 2.018
batch start
#iterations: 247
currently lose_sum: 91.56185227632523
time_elpased: 1.974
batch start
#iterations: 248
currently lose_sum: 92.01302593946457
time_elpased: 2.03
batch start
#iterations: 249
currently lose_sum: 91.8208646774292
time_elpased: 1.979
batch start
#iterations: 250
currently lose_sum: 91.12937712669373
time_elpased: 1.941
batch start
#iterations: 251
currently lose_sum: 91.06703817844391
time_elpased: 1.849
batch start
#iterations: 252
currently lose_sum: 91.26867741346359
time_elpased: 1.929
batch start
#iterations: 253
currently lose_sum: 91.35721665620804
time_elpased: 1.956
batch start
#iterations: 254
currently lose_sum: 91.30770707130432
time_elpased: 1.945
batch start
#iterations: 255
currently lose_sum: 91.9067941904068
time_elpased: 1.998
batch start
#iterations: 256
currently lose_sum: 91.4594549536705
time_elpased: 2.148
batch start
#iterations: 257
currently lose_sum: 90.85020816326141
time_elpased: 2.089
batch start
#iterations: 258
currently lose_sum: 90.81765615940094
time_elpased: 2.156
batch start
#iterations: 259
currently lose_sum: 90.9984678030014
time_elpased: 2.119
start validation test
0.59618556701
0.669000179501
0.38355459504
0.487571951858
0.596558873478
63.364
batch start
#iterations: 260
currently lose_sum: 90.62820422649384
time_elpased: 2.097
batch start
#iterations: 261
currently lose_sum: 91.4627680182457
time_elpased: 2.085
batch start
#iterations: 262
currently lose_sum: 90.94581460952759
time_elpased: 2.127
batch start
#iterations: 263
currently lose_sum: 91.22154486179352
time_elpased: 2.076
batch start
#iterations: 264
currently lose_sum: 91.4608765244484
time_elpased: 2.124
batch start
#iterations: 265
currently lose_sum: 91.49185526371002
time_elpased: 2.105
batch start
#iterations: 266
currently lose_sum: 90.41514402627945
time_elpased: 2.129
batch start
#iterations: 267
currently lose_sum: 90.62438827753067
time_elpased: 2.115
batch start
#iterations: 268
currently lose_sum: 90.67905324697495
time_elpased: 2.117
batch start
#iterations: 269
currently lose_sum: 91.39825195074081
time_elpased: 2.153
batch start
#iterations: 270
currently lose_sum: 90.98675245046616
time_elpased: 2.091
batch start
#iterations: 271
currently lose_sum: 91.1120325922966
time_elpased: 1.971
batch start
#iterations: 272
currently lose_sum: 90.99177318811417
time_elpased: 2.028
batch start
#iterations: 273
currently lose_sum: 91.3061996102333
time_elpased: 1.945
batch start
#iterations: 274
currently lose_sum: 90.52924758195877
time_elpased: 2.017
batch start
#iterations: 275
currently lose_sum: 91.39056092500687
time_elpased: 1.97
batch start
#iterations: 276
currently lose_sum: 90.43950748443604
time_elpased: 2.082
batch start
#iterations: 277
currently lose_sum: 90.15927106142044
time_elpased: 2.118
batch start
#iterations: 278
currently lose_sum: 90.34021496772766
time_elpased: 2.082
batch start
#iterations: 279
currently lose_sum: 90.70818489789963
time_elpased: 1.955
start validation test
0.561237113402
0.677258017064
0.23690439436
0.351021652943
0.561806529515
66.184
batch start
#iterations: 280
currently lose_sum: 91.30078327655792
time_elpased: 2.19
batch start
#iterations: 281
currently lose_sum: 90.66990560293198
time_elpased: 2.083
batch start
#iterations: 282
currently lose_sum: 91.34624111652374
time_elpased: 2.201
batch start
#iterations: 283
currently lose_sum: 90.68575149774551
time_elpased: 1.964
batch start
#iterations: 284
currently lose_sum: 89.79385316371918
time_elpased: 1.989
batch start
#iterations: 285
currently lose_sum: 90.73004710674286
time_elpased: 1.954
batch start
#iterations: 286
currently lose_sum: 90.38926565647125
time_elpased: 1.981
batch start
#iterations: 287
currently lose_sum: 90.5311541557312
time_elpased: 1.942
batch start
#iterations: 288
currently lose_sum: 89.95209556818008
time_elpased: 1.956
batch start
#iterations: 289
currently lose_sum: 90.82063603401184
time_elpased: 2.186
batch start
#iterations: 290
currently lose_sum: 90.62917506694794
time_elpased: 2.008
batch start
#iterations: 291
currently lose_sum: 90.21902734041214
time_elpased: 2.104
batch start
#iterations: 292
currently lose_sum: 90.23669123649597
time_elpased: 2.047
batch start
#iterations: 293
currently lose_sum: 90.56581431627274
time_elpased: 2.0
batch start
#iterations: 294
currently lose_sum: 90.0235208272934
time_elpased: 1.976
batch start
#iterations: 295
currently lose_sum: 91.91038680076599
time_elpased: 1.961
batch start
#iterations: 296
currently lose_sum: 90.06686758995056
time_elpased: 1.962
batch start
#iterations: 297
currently lose_sum: 90.64441341161728
time_elpased: 1.961
batch start
#iterations: 298
currently lose_sum: 89.33237272500992
time_elpased: 1.965
batch start
#iterations: 299
currently lose_sum: 90.30926394462585
time_elpased: 2.029
start validation test
0.633041237113
0.63772264631
0.619018215499
0.628231239229
0.633065856691
61.853
batch start
#iterations: 300
currently lose_sum: 90.08430898189545
time_elpased: 2.009
batch start
#iterations: 301
currently lose_sum: 91.17819803953171
time_elpased: 1.991
batch start
#iterations: 302
currently lose_sum: 89.90293568372726
time_elpased: 2.013
batch start
#iterations: 303
currently lose_sum: 89.96686214208603
time_elpased: 2.028
batch start
#iterations: 304
currently lose_sum: 90.04989111423492
time_elpased: 1.966
batch start
#iterations: 305
currently lose_sum: 90.3008229136467
time_elpased: 2.063
batch start
#iterations: 306
currently lose_sum: 90.79768788814545
time_elpased: 1.97
batch start
#iterations: 307
currently lose_sum: 91.2734249830246
time_elpased: 2.051
batch start
#iterations: 308
currently lose_sum: 90.41639471054077
time_elpased: 2.018
batch start
#iterations: 309
currently lose_sum: 90.18258637189865
time_elpased: 1.923
batch start
#iterations: 310
currently lose_sum: 90.23267138004303
time_elpased: 2.085
batch start
#iterations: 311
currently lose_sum: 89.22397315502167
time_elpased: 2.021
batch start
#iterations: 312
currently lose_sum: 89.6377831697464
time_elpased: 2.092
batch start
#iterations: 313
currently lose_sum: 90.00467067956924
time_elpased: 2.133
batch start
#iterations: 314
currently lose_sum: 90.05884575843811
time_elpased: 1.979
batch start
#iterations: 315
currently lose_sum: 89.85483187437057
time_elpased: 2.065
batch start
#iterations: 316
currently lose_sum: 89.86685818433762
time_elpased: 2.041
batch start
#iterations: 317
currently lose_sum: 90.46053355932236
time_elpased: 1.996
batch start
#iterations: 318
currently lose_sum: 89.58674997091293
time_elpased: 2.092
batch start
#iterations: 319
currently lose_sum: 90.39557605981827
time_elpased: 1.889
start validation test
0.635927835052
0.605434609884
0.784192652053
0.68331614581
0.635667533297
62.382
batch start
#iterations: 320
currently lose_sum: 89.68688470125198
time_elpased: 1.943
batch start
#iterations: 321
currently lose_sum: 89.74155533313751
time_elpased: 2.063
batch start
#iterations: 322
currently lose_sum: 89.6917816400528
time_elpased: 2.071
batch start
#iterations: 323
currently lose_sum: 89.98410946130753
time_elpased: 2.111
batch start
#iterations: 324
currently lose_sum: 89.54296344518661
time_elpased: 2.1
batch start
#iterations: 325
currently lose_sum: 90.11172223091125
time_elpased: 1.936
batch start
#iterations: 326
currently lose_sum: 89.88417446613312
time_elpased: 2.13
batch start
#iterations: 327
currently lose_sum: 89.53368425369263
time_elpased: 2.039
batch start
#iterations: 328
currently lose_sum: 90.33797198534012
time_elpased: 2.055
batch start
#iterations: 329
currently lose_sum: 89.21540868282318
time_elpased: 2.084
batch start
#iterations: 330
currently lose_sum: 89.35508352518082
time_elpased: 2.04
batch start
#iterations: 331
currently lose_sum: 89.55786836147308
time_elpased: 2.047
batch start
#iterations: 332
currently lose_sum: 89.67230528593063
time_elpased: 2.127
batch start
#iterations: 333
currently lose_sum: 89.37028592824936
time_elpased: 2.116
batch start
#iterations: 334
currently lose_sum: 90.16907060146332
time_elpased: 2.144
batch start
#iterations: 335
currently lose_sum: 89.56822538375854
time_elpased: 2.261
batch start
#iterations: 336
currently lose_sum: 89.82493031024933
time_elpased: 2.076
batch start
#iterations: 337
currently lose_sum: 89.6063050031662
time_elpased: 2.011
batch start
#iterations: 338
currently lose_sum: 89.56761920452118
time_elpased: 2.178
batch start
#iterations: 339
currently lose_sum: 89.33190631866455
time_elpased: 2.106
start validation test
0.626030927835
0.639981805777
0.579191108367
0.608070876776
0.626113162363
62.020
batch start
#iterations: 340
currently lose_sum: 89.46385782957077
time_elpased: 2.122
batch start
#iterations: 341
currently lose_sum: 89.82863730192184
time_elpased: 2.102
batch start
#iterations: 342
currently lose_sum: 89.83748078346252
time_elpased: 2.113
batch start
#iterations: 343
currently lose_sum: 89.9246689081192
time_elpased: 1.987
batch start
#iterations: 344
currently lose_sum: 89.59492492675781
time_elpased: 2.056
batch start
#iterations: 345
currently lose_sum: 88.95094388723373
time_elpased: 2.147
batch start
#iterations: 346
currently lose_sum: 89.42025059461594
time_elpased: 2.096
batch start
#iterations: 347
currently lose_sum: 88.9912777543068
time_elpased: 2.093
batch start
#iterations: 348
currently lose_sum: 89.41372060775757
time_elpased: 2.151
batch start
#iterations: 349
currently lose_sum: 89.06447923183441
time_elpased: 2.068
batch start
#iterations: 350
currently lose_sum: 88.79516243934631
time_elpased: 2.112
batch start
#iterations: 351
currently lose_sum: 89.65418004989624
time_elpased: 2.13
batch start
#iterations: 352
currently lose_sum: 89.33121955394745
time_elpased: 2.1
batch start
#iterations: 353
currently lose_sum: 89.95426201820374
time_elpased: 2.088
batch start
#iterations: 354
currently lose_sum: 89.43620753288269
time_elpased: 2.129
batch start
#iterations: 355
currently lose_sum: 89.18095624446869
time_elpased: 1.953
batch start
#iterations: 356
currently lose_sum: 89.37124651670456
time_elpased: 2.055
batch start
#iterations: 357
currently lose_sum: 88.87582498788834
time_elpased: 2.104
batch start
#iterations: 358
currently lose_sum: 88.86649680137634
time_elpased: 2.15
batch start
#iterations: 359
currently lose_sum: 90.21060317754745
time_elpased: 2.139
start validation test
0.543195876289
0.681374628765
0.165277348976
0.26602617194
0.543859370553
68.806
batch start
#iterations: 360
currently lose_sum: 88.61186343431473
time_elpased: 2.145
batch start
#iterations: 361
currently lose_sum: 89.53009980916977
time_elpased: 2.219
batch start
#iterations: 362
currently lose_sum: 88.9658573269844
time_elpased: 2.159
batch start
#iterations: 363
currently lose_sum: 88.30151242017746
time_elpased: 1.989
batch start
#iterations: 364
currently lose_sum: 89.98064094781876
time_elpased: 2.0
batch start
#iterations: 365
currently lose_sum: 88.96703451871872
time_elpased: 1.93
batch start
#iterations: 366
currently lose_sum: 88.66039127111435
time_elpased: 1.98
batch start
#iterations: 367
currently lose_sum: 89.7285704612732
time_elpased: 1.959
batch start
#iterations: 368
currently lose_sum: 88.91910791397095
time_elpased: 1.98
batch start
#iterations: 369
currently lose_sum: 88.83237355947495
time_elpased: 1.958
batch start
#iterations: 370
currently lose_sum: 88.1044312119484
time_elpased: 2.113
batch start
#iterations: 371
currently lose_sum: 88.48006957769394
time_elpased: 2.13
batch start
#iterations: 372
currently lose_sum: 88.55346930027008
time_elpased: 2.131
batch start
#iterations: 373
currently lose_sum: 88.97827416658401
time_elpased: 2.038
batch start
#iterations: 374
currently lose_sum: 89.22943544387817
time_elpased: 2.147
batch start
#iterations: 375
currently lose_sum: 88.69078290462494
time_elpased: 1.961
batch start
#iterations: 376
currently lose_sum: 88.79617184400558
time_elpased: 1.912
batch start
#iterations: 377
currently lose_sum: 88.87685865163803
time_elpased: 1.991
batch start
#iterations: 378
currently lose_sum: 88.62511777877808
time_elpased: 2.077
batch start
#iterations: 379
currently lose_sum: 88.81175035238266
time_elpased: 2.018
start validation test
0.530927835052
0.681791396582
0.119069671709
0.202733485194
0.531650915582
71.441
batch start
#iterations: 380
currently lose_sum: 88.75492542982101
time_elpased: 1.958
batch start
#iterations: 381
currently lose_sum: 88.17561548948288
time_elpased: 1.921
batch start
#iterations: 382
currently lose_sum: 89.65809148550034
time_elpased: 1.975
batch start
#iterations: 383
currently lose_sum: 88.98597115278244
time_elpased: 1.93
batch start
#iterations: 384
currently lose_sum: 88.12713438272476
time_elpased: 2.183
batch start
#iterations: 385
currently lose_sum: 88.24870264530182
time_elpased: 1.928
batch start
#iterations: 386
currently lose_sum: 88.44576317071915
time_elpased: 1.941
batch start
#iterations: 387
currently lose_sum: 88.8609294295311
time_elpased: 1.928
batch start
#iterations: 388
currently lose_sum: 87.90155565738678
time_elpased: 1.996
batch start
#iterations: 389
currently lose_sum: 87.82290947437286
time_elpased: 2.138
batch start
#iterations: 390
currently lose_sum: 88.8028262257576
time_elpased: 1.992
batch start
#iterations: 391
currently lose_sum: 87.19346225261688
time_elpased: 1.977
batch start
#iterations: 392
currently lose_sum: 88.21056288480759
time_elpased: 1.963
batch start
#iterations: 393
currently lose_sum: 88.43753099441528
time_elpased: 1.957
batch start
#iterations: 394
currently lose_sum: 88.18675142526627
time_elpased: 1.96
batch start
#iterations: 395
currently lose_sum: 88.52905774116516
time_elpased: 1.943
batch start
#iterations: 396
currently lose_sum: 87.82375437021255
time_elpased: 1.932
batch start
#iterations: 397
currently lose_sum: 88.24960649013519
time_elpased: 1.85
batch start
#iterations: 398
currently lose_sum: 88.24839037656784
time_elpased: 1.794
batch start
#iterations: 399
currently lose_sum: 88.493468105793
time_elpased: 1.999
start validation test
0.613969072165
0.650744248985
0.494905835134
0.562225989361
0.614178106042
62.971
acc: 0.640
pre: 0.644
rec: 0.629
F1: 0.637
auc: 0.640
