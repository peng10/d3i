start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 101.45183718204498
time_elpased: 2.091
batch start
#iterations: 1
currently lose_sum: 100.3817435503006
time_elpased: 2.14
batch start
#iterations: 2
currently lose_sum: 100.18906652927399
time_elpased: 2.125
batch start
#iterations: 3
currently lose_sum: 100.03763371706009
time_elpased: 2.101
batch start
#iterations: 4
currently lose_sum: 99.97135889530182
time_elpased: 2.105
batch start
#iterations: 5
currently lose_sum: 99.8429986834526
time_elpased: 2.099
batch start
#iterations: 6
currently lose_sum: 99.73404729366302
time_elpased: 2.121
batch start
#iterations: 7
currently lose_sum: 99.69577705860138
time_elpased: 2.114
batch start
#iterations: 8
currently lose_sum: 99.5274720788002
time_elpased: 2.128
batch start
#iterations: 9
currently lose_sum: 99.67632305622101
time_elpased: 2.077
batch start
#iterations: 10
currently lose_sum: 99.65128946304321
time_elpased: 2.126
batch start
#iterations: 11
currently lose_sum: 99.33827233314514
time_elpased: 2.149
batch start
#iterations: 12
currently lose_sum: 99.34433251619339
time_elpased: 2.115
batch start
#iterations: 13
currently lose_sum: 99.20641475915909
time_elpased: 2.124
batch start
#iterations: 14
currently lose_sum: 99.2565325498581
time_elpased: 2.11
batch start
#iterations: 15
currently lose_sum: 99.00372952222824
time_elpased: 2.106
batch start
#iterations: 16
currently lose_sum: 98.63804882764816
time_elpased: 2.096
batch start
#iterations: 17
currently lose_sum: 98.80857527256012
time_elpased: 2.241
batch start
#iterations: 18
currently lose_sum: 98.86200475692749
time_elpased: 2.151
batch start
#iterations: 19
currently lose_sum: 98.58721125125885
time_elpased: 2.236
start validation test
0.598092783505
0.626548905879
0.489142739529
0.549384499798
0.598284062112
64.910
batch start
#iterations: 20
currently lose_sum: 98.61165803670883
time_elpased: 2.355
batch start
#iterations: 21
currently lose_sum: 98.62318468093872
time_elpased: 2.284
batch start
#iterations: 22
currently lose_sum: 98.27866327762604
time_elpased: 2.328
batch start
#iterations: 23
currently lose_sum: 98.62431955337524
time_elpased: 2.414
batch start
#iterations: 24
currently lose_sum: 98.1849257349968
time_elpased: 2.384
batch start
#iterations: 25
currently lose_sum: 98.3457562327385
time_elpased: 2.385
batch start
#iterations: 26
currently lose_sum: 98.20283967256546
time_elpased: 2.428
batch start
#iterations: 27
currently lose_sum: 98.21528655290604
time_elpased: 2.425
batch start
#iterations: 28
currently lose_sum: 98.03909009695053
time_elpased: 2.265
batch start
#iterations: 29
currently lose_sum: 97.96697306632996
time_elpased: 2.414
batch start
#iterations: 30
currently lose_sum: 97.96274542808533
time_elpased: 2.39
batch start
#iterations: 31
currently lose_sum: 97.78220611810684
time_elpased: 2.423
batch start
#iterations: 32
currently lose_sum: 97.8061226606369
time_elpased: 2.384
batch start
#iterations: 33
currently lose_sum: 98.10299104452133
time_elpased: 2.347
batch start
#iterations: 34
currently lose_sum: 97.73167872428894
time_elpased: 2.426
batch start
#iterations: 35
currently lose_sum: 97.62272864580154
time_elpased: 2.317
batch start
#iterations: 36
currently lose_sum: 97.46985065937042
time_elpased: 2.387
batch start
#iterations: 37
currently lose_sum: 97.64517211914062
time_elpased: 2.172
batch start
#iterations: 38
currently lose_sum: 97.42757320404053
time_elpased: 2.144
batch start
#iterations: 39
currently lose_sum: 97.68473273515701
time_elpased: 2.257
start validation test
0.643659793814
0.649562620013
0.626633734692
0.637892200513
0.643689685687
63.332
batch start
#iterations: 40
currently lose_sum: 97.3790751695633
time_elpased: 2.189
batch start
#iterations: 41
currently lose_sum: 97.18512535095215
time_elpased: 2.239
batch start
#iterations: 42
currently lose_sum: 97.30893671512604
time_elpased: 2.268
batch start
#iterations: 43
currently lose_sum: 97.28257709741592
time_elpased: 2.308
batch start
#iterations: 44
currently lose_sum: 97.49153995513916
time_elpased: 2.294
batch start
#iterations: 45
currently lose_sum: 97.48594218492508
time_elpased: 2.287
batch start
#iterations: 46
currently lose_sum: 97.28327190876007
time_elpased: 2.277
batch start
#iterations: 47
currently lose_sum: 97.22607278823853
time_elpased: 2.312
batch start
#iterations: 48
currently lose_sum: 97.1040523648262
time_elpased: 2.242
batch start
#iterations: 49
currently lose_sum: 96.99193930625916
time_elpased: 2.272
batch start
#iterations: 50
currently lose_sum: 97.029920399189
time_elpased: 2.258
batch start
#iterations: 51
currently lose_sum: 97.21384280920029
time_elpased: 2.276
batch start
#iterations: 52
currently lose_sum: 97.00850188732147
time_elpased: 2.259
batch start
#iterations: 53
currently lose_sum: 96.87808668613434
time_elpased: 2.316
batch start
#iterations: 54
currently lose_sum: 97.12799847126007
time_elpased: 2.268
batch start
#iterations: 55
currently lose_sum: 97.06651532649994
time_elpased: 2.294
batch start
#iterations: 56
currently lose_sum: 96.75881522893906
time_elpased: 2.29
batch start
#iterations: 57
currently lose_sum: 96.70189517736435
time_elpased: 2.265
batch start
#iterations: 58
currently lose_sum: 96.94279259443283
time_elpased: 2.279
batch start
#iterations: 59
currently lose_sum: 96.83685147762299
time_elpased: 2.292
start validation test
0.607680412371
0.667889030612
0.431100133786
0.523985239852
0.607990426286
63.319
batch start
#iterations: 60
currently lose_sum: 96.79143697023392
time_elpased: 2.261
batch start
#iterations: 61
currently lose_sum: 96.72539359331131
time_elpased: 2.254
batch start
#iterations: 62
currently lose_sum: 96.76664316654205
time_elpased: 2.266
batch start
#iterations: 63
currently lose_sum: 96.46468716859818
time_elpased: 2.271
batch start
#iterations: 64
currently lose_sum: 96.35355812311172
time_elpased: 2.305
batch start
#iterations: 65
currently lose_sum: 96.86513698101044
time_elpased: 2.285
batch start
#iterations: 66
currently lose_sum: 96.42437064647675
time_elpased: 2.273
batch start
#iterations: 67
currently lose_sum: 96.27158254384995
time_elpased: 2.304
batch start
#iterations: 68
currently lose_sum: 96.37162524461746
time_elpased: 2.229
batch start
#iterations: 69
currently lose_sum: 96.4999480843544
time_elpased: 2.228
batch start
#iterations: 70
currently lose_sum: 96.36173087358475
time_elpased: 2.244
batch start
#iterations: 71
currently lose_sum: 96.11343324184418
time_elpased: 2.262
batch start
#iterations: 72
currently lose_sum: 96.44286984205246
time_elpased: 2.299
batch start
#iterations: 73
currently lose_sum: 96.35535591840744
time_elpased: 2.252
batch start
#iterations: 74
currently lose_sum: 96.49773579835892
time_elpased: 2.254
batch start
#iterations: 75
currently lose_sum: 96.22746235132217
time_elpased: 2.25
batch start
#iterations: 76
currently lose_sum: 95.96987468004227
time_elpased: 2.267
batch start
#iterations: 77
currently lose_sum: 96.59884655475616
time_elpased: 2.295
batch start
#iterations: 78
currently lose_sum: 96.165223300457
time_elpased: 2.304
batch start
#iterations: 79
currently lose_sum: 96.30825746059418
time_elpased: 2.294
start validation test
0.651288659794
0.648970528866
0.661726870433
0.655286624204
0.651270333905
62.260
batch start
#iterations: 80
currently lose_sum: 96.43179363012314
time_elpased: 2.305
batch start
#iterations: 81
currently lose_sum: 96.54066413640976
time_elpased: 2.264
batch start
#iterations: 82
currently lose_sum: 96.14683091640472
time_elpased: 2.278
batch start
#iterations: 83
currently lose_sum: 96.02124774456024
time_elpased: 2.308
batch start
#iterations: 84
currently lose_sum: 95.78509563207626
time_elpased: 2.249
batch start
#iterations: 85
currently lose_sum: 95.90406209230423
time_elpased: 2.308
batch start
#iterations: 86
currently lose_sum: 96.26213151216507
time_elpased: 2.288
batch start
#iterations: 87
currently lose_sum: 95.85390031337738
time_elpased: 2.334
batch start
#iterations: 88
currently lose_sum: 95.82100278139114
time_elpased: 2.303
batch start
#iterations: 89
currently lose_sum: 96.04895824193954
time_elpased: 2.266
batch start
#iterations: 90
currently lose_sum: 95.95868587493896
time_elpased: 2.256
batch start
#iterations: 91
currently lose_sum: 95.84064793586731
time_elpased: 2.289
batch start
#iterations: 92
currently lose_sum: 96.00782406330109
time_elpased: 2.338
batch start
#iterations: 93
currently lose_sum: 95.58810120820999
time_elpased: 2.258
batch start
#iterations: 94
currently lose_sum: 95.60920667648315
time_elpased: 2.197
batch start
#iterations: 95
currently lose_sum: 95.81017696857452
time_elpased: 2.405
batch start
#iterations: 96
currently lose_sum: 95.88579571247101
time_elpased: 2.424
batch start
#iterations: 97
currently lose_sum: 95.26120740175247
time_elpased: 2.347
batch start
#iterations: 98
currently lose_sum: 95.99946701526642
time_elpased: 2.381
batch start
#iterations: 99
currently lose_sum: 95.59444439411163
time_elpased: 2.377
start validation test
0.557577319588
0.678077889447
0.222187918082
0.334702736222
0.558166147412
64.724
batch start
#iterations: 100
currently lose_sum: 96.066890001297
time_elpased: 2.365
batch start
#iterations: 101
currently lose_sum: 95.47405171394348
time_elpased: 2.369
batch start
#iterations: 102
currently lose_sum: 95.40840059518814
time_elpased: 2.419
batch start
#iterations: 103
currently lose_sum: 95.64579802751541
time_elpased: 2.397
batch start
#iterations: 104
currently lose_sum: 95.84439235925674
time_elpased: 2.371
batch start
#iterations: 105
currently lose_sum: 95.45478218793869
time_elpased: 2.464
batch start
#iterations: 106
currently lose_sum: 95.49572688341141
time_elpased: 2.337
batch start
#iterations: 107
currently lose_sum: 95.5437331199646
time_elpased: 2.384
batch start
#iterations: 108
currently lose_sum: 94.99155282974243
time_elpased: 2.391
batch start
#iterations: 109
currently lose_sum: 95.50541943311691
time_elpased: 2.332
batch start
#iterations: 110
currently lose_sum: 95.39438170194626
time_elpased: 2.349
batch start
#iterations: 111
currently lose_sum: 94.83202570676804
time_elpased: 2.352
batch start
#iterations: 112
currently lose_sum: 95.13259881734848
time_elpased: 2.334
batch start
#iterations: 113
currently lose_sum: 95.25243294239044
time_elpased: 2.343
batch start
#iterations: 114
currently lose_sum: 94.91568201780319
time_elpased: 2.407
batch start
#iterations: 115
currently lose_sum: 95.61780887842178
time_elpased: 2.405
batch start
#iterations: 116
currently lose_sum: 94.8652703166008
time_elpased: 2.39
batch start
#iterations: 117
currently lose_sum: 95.39276015758514
time_elpased: 2.376
batch start
#iterations: 118
currently lose_sum: 94.89079517126083
time_elpased: 2.337
batch start
#iterations: 119
currently lose_sum: 95.12275350093842
time_elpased: 2.334
start validation test
0.627577319588
0.58373655914
0.893897293403
0.706264991666
0.627109753797
63.382
batch start
#iterations: 120
currently lose_sum: 94.82719826698303
time_elpased: 2.295
batch start
#iterations: 121
currently lose_sum: 94.96787369251251
time_elpased: 2.358
batch start
#iterations: 122
currently lose_sum: 95.28314137458801
time_elpased: 2.344
batch start
#iterations: 123
currently lose_sum: 95.14683049917221
time_elpased: 2.392
batch start
#iterations: 124
currently lose_sum: 95.26530855894089
time_elpased: 2.347
batch start
#iterations: 125
currently lose_sum: 95.1918318271637
time_elpased: 2.356
batch start
#iterations: 126
currently lose_sum: 94.53454786539078
time_elpased: 2.346
batch start
#iterations: 127
currently lose_sum: 94.99910873174667
time_elpased: 2.365
batch start
#iterations: 128
currently lose_sum: 94.99150782823563
time_elpased: 2.4
batch start
#iterations: 129
currently lose_sum: 95.32850790023804
time_elpased: 2.37
batch start
#iterations: 130
currently lose_sum: 94.79372715950012
time_elpased: 2.32
batch start
#iterations: 131
currently lose_sum: 94.72155839204788
time_elpased: 2.382
batch start
#iterations: 132
currently lose_sum: 94.97563344240189
time_elpased: 2.377
batch start
#iterations: 133
currently lose_sum: 94.86171108484268
time_elpased: 2.431
batch start
#iterations: 134
currently lose_sum: 95.05608296394348
time_elpased: 2.367
batch start
#iterations: 135
currently lose_sum: 94.29816991090775
time_elpased: 2.392
batch start
#iterations: 136
currently lose_sum: 94.39397209882736
time_elpased: 2.334
batch start
#iterations: 137
currently lose_sum: 94.1938858628273
time_elpased: 2.346
batch start
#iterations: 138
currently lose_sum: 94.5350832939148
time_elpased: 2.372
batch start
#iterations: 139
currently lose_sum: 94.18795156478882
time_elpased: 2.373
start validation test
0.647680412371
0.615798778528
0.788617886179
0.691575289924
0.647432974898
62.108
batch start
#iterations: 140
currently lose_sum: 94.43692946434021
time_elpased: 2.37
batch start
#iterations: 141
currently lose_sum: 95.13390690088272
time_elpased: 2.354
batch start
#iterations: 142
currently lose_sum: 94.99729031324387
time_elpased: 2.34
batch start
#iterations: 143
currently lose_sum: 94.60178762674332
time_elpased: 2.347
batch start
#iterations: 144
currently lose_sum: 94.99952590465546
time_elpased: 2.345
batch start
#iterations: 145
currently lose_sum: 94.38548171520233
time_elpased: 2.367
batch start
#iterations: 146
currently lose_sum: 94.26214683055878
time_elpased: 2.396
batch start
#iterations: 147
currently lose_sum: 94.22184580564499
time_elpased: 2.393
batch start
#iterations: 148
currently lose_sum: 94.22281891107559
time_elpased: 2.329
batch start
#iterations: 149
currently lose_sum: 93.84288746118546
time_elpased: 2.346
batch start
#iterations: 150
currently lose_sum: 94.04048484563828
time_elpased: 2.339
batch start
#iterations: 151
currently lose_sum: 93.94039469957352
time_elpased: 2.343
batch start
#iterations: 152
currently lose_sum: 94.43270993232727
time_elpased: 2.359
batch start
#iterations: 153
currently lose_sum: 95.08553808927536
time_elpased: 2.415
batch start
#iterations: 154
currently lose_sum: 94.60118293762207
time_elpased: 2.363
batch start
#iterations: 155
currently lose_sum: 93.97266268730164
time_elpased: 2.353
batch start
#iterations: 156
currently lose_sum: 93.99306374788284
time_elpased: 2.49
batch start
#iterations: 157
currently lose_sum: 93.82960140705109
time_elpased: 2.416
batch start
#iterations: 158
currently lose_sum: 94.1876944899559
time_elpased: 2.416
batch start
#iterations: 159
currently lose_sum: 94.79970067739487
time_elpased: 2.318
start validation test
0.622731958763
0.579731347254
0.897190490892
0.704342557059
0.622250104477
63.634
batch start
#iterations: 160
currently lose_sum: 93.70387673377991
time_elpased: 2.321
batch start
#iterations: 161
currently lose_sum: 94.09291976690292
time_elpased: 2.239
batch start
#iterations: 162
currently lose_sum: 93.57902479171753
time_elpased: 2.283
batch start
#iterations: 163
currently lose_sum: 94.32081842422485
time_elpased: 2.199
batch start
#iterations: 164
currently lose_sum: 93.39840006828308
time_elpased: 2.163
batch start
#iterations: 165
currently lose_sum: 93.87805312871933
time_elpased: 2.183
batch start
#iterations: 166
currently lose_sum: 93.86923748254776
time_elpased: 2.152
batch start
#iterations: 167
currently lose_sum: 94.02421075105667
time_elpased: 2.192
batch start
#iterations: 168
currently lose_sum: 93.80998814105988
time_elpased: 2.161
batch start
#iterations: 169
currently lose_sum: 94.04801350831985
time_elpased: 2.176
batch start
#iterations: 170
currently lose_sum: 94.03413701057434
time_elpased: 2.197
batch start
#iterations: 171
currently lose_sum: 93.54521697759628
time_elpased: 2.194
batch start
#iterations: 172
currently lose_sum: 93.94262218475342
time_elpased: 2.312
batch start
#iterations: 173
currently lose_sum: 93.44261878728867
time_elpased: 2.526
batch start
#iterations: 174
currently lose_sum: 93.48314386606216
time_elpased: 2.521
batch start
#iterations: 175
currently lose_sum: 93.5245109796524
time_elpased: 2.468
batch start
#iterations: 176
currently lose_sum: 93.30057865381241
time_elpased: 2.352
batch start
#iterations: 177
currently lose_sum: 93.35075449943542
time_elpased: 2.198
batch start
#iterations: 178
currently lose_sum: 94.36557847261429
time_elpased: 2.19
batch start
#iterations: 179
currently lose_sum: 93.56322115659714
time_elpased: 2.175
start validation test
0.630670103093
0.656487613441
0.550890192446
0.59907112081
0.630810169031
62.096
batch start
#iterations: 180
currently lose_sum: 93.88504654169083
time_elpased: 2.19
batch start
#iterations: 181
currently lose_sum: 92.98941105604172
time_elpased: 2.148
batch start
#iterations: 182
currently lose_sum: 93.37944006919861
time_elpased: 2.139
batch start
#iterations: 183
currently lose_sum: 93.32421630620956
time_elpased: 2.196
batch start
#iterations: 184
currently lose_sum: 93.5330421924591
time_elpased: 2.166
batch start
#iterations: 185
currently lose_sum: 93.06356245279312
time_elpased: 2.197
batch start
#iterations: 186
currently lose_sum: 93.55183792114258
time_elpased: 2.144
batch start
#iterations: 187
currently lose_sum: 93.26932924985886
time_elpased: 2.166
batch start
#iterations: 188
currently lose_sum: 93.51643979549408
time_elpased: 2.137
batch start
#iterations: 189
currently lose_sum: 93.66168749332428
time_elpased: 2.175
batch start
#iterations: 190
currently lose_sum: 92.92392253875732
time_elpased: 2.118
batch start
#iterations: 191
currently lose_sum: 92.99535012245178
time_elpased: 2.185
batch start
#iterations: 192
currently lose_sum: 92.88515549898148
time_elpased: 2.158
batch start
#iterations: 193
currently lose_sum: 93.11472445726395
time_elpased: 2.17
batch start
#iterations: 194
currently lose_sum: 93.17348116636276
time_elpased: 2.173
batch start
#iterations: 195
currently lose_sum: 92.97394323348999
time_elpased: 2.216
batch start
#iterations: 196
currently lose_sum: 93.79299575090408
time_elpased: 2.167
batch start
#iterations: 197
currently lose_sum: 93.28150993585587
time_elpased: 2.195
batch start
#iterations: 198
currently lose_sum: 92.62593853473663
time_elpased: 2.144
batch start
#iterations: 199
currently lose_sum: 92.92903661727905
time_elpased: 2.154
start validation test
0.608711340206
0.66713836478
0.436657404549
0.527834795049
0.609013407428
62.665
batch start
#iterations: 200
currently lose_sum: 93.05963975191116
time_elpased: 2.17
batch start
#iterations: 201
currently lose_sum: 93.2358507514
time_elpased: 2.207
batch start
#iterations: 202
currently lose_sum: 92.75144946575165
time_elpased: 2.155
batch start
#iterations: 203
currently lose_sum: 94.16806465387344
time_elpased: 2.166
batch start
#iterations: 204
currently lose_sum: 92.39724123477936
time_elpased: 2.148
batch start
#iterations: 205
currently lose_sum: 93.5051184296608
time_elpased: 2.214
batch start
#iterations: 206
currently lose_sum: 92.31165045499802
time_elpased: 2.185
batch start
#iterations: 207
currently lose_sum: 92.74223870038986
time_elpased: 2.179
batch start
#iterations: 208
currently lose_sum: 92.90936231613159
time_elpased: 2.165
batch start
#iterations: 209
currently lose_sum: 92.47389978170395
time_elpased: 2.201
batch start
#iterations: 210
currently lose_sum: 92.38872534036636
time_elpased: 2.169
batch start
#iterations: 211
currently lose_sum: 93.94510215520859
time_elpased: 2.165
batch start
#iterations: 212
currently lose_sum: 92.44046688079834
time_elpased: 2.185
batch start
#iterations: 213
currently lose_sum: 93.14555102586746
time_elpased: 2.192
batch start
#iterations: 214
currently lose_sum: 92.57586055994034
time_elpased: 2.189
batch start
#iterations: 215
currently lose_sum: 92.49864411354065
time_elpased: 2.234
batch start
#iterations: 216
currently lose_sum: 92.38115590810776
time_elpased: 2.236
batch start
#iterations: 217
currently lose_sum: 92.18812024593353
time_elpased: 2.202
batch start
#iterations: 218
currently lose_sum: 92.55313801765442
time_elpased: 2.185
batch start
#iterations: 219
currently lose_sum: 92.97161954641342
time_elpased: 2.212
start validation test
0.63793814433
0.630487450334
0.669548214469
0.649431024157
0.637882647976
62.015
batch start
#iterations: 220
currently lose_sum: 92.38400900363922
time_elpased: 2.264
batch start
#iterations: 221
currently lose_sum: 92.61525058746338
time_elpased: 2.198
batch start
#iterations: 222
currently lose_sum: 91.57337868213654
time_elpased: 2.176
batch start
#iterations: 223
currently lose_sum: 92.90860825777054
time_elpased: 2.322
batch start
#iterations: 224
currently lose_sum: 92.56972289085388
time_elpased: 2.245
batch start
#iterations: 225
currently lose_sum: 92.64617508649826
time_elpased: 2.172
batch start
#iterations: 226
currently lose_sum: 92.45688676834106
time_elpased: 2.156
batch start
#iterations: 227
currently lose_sum: 92.66753017902374
time_elpased: 2.165
batch start
#iterations: 228
currently lose_sum: 92.40813571214676
time_elpased: 2.204
batch start
#iterations: 229
currently lose_sum: 92.3780432343483
time_elpased: 2.224
batch start
#iterations: 230
currently lose_sum: 92.3896621465683
time_elpased: 2.309
batch start
#iterations: 231
currently lose_sum: 92.40982437133789
time_elpased: 2.254
batch start
#iterations: 232
currently lose_sum: 91.73625284433365
time_elpased: 2.224
batch start
#iterations: 233
currently lose_sum: 92.07289105653763
time_elpased: 2.393
batch start
#iterations: 234
currently lose_sum: 92.32167536020279
time_elpased: 2.589
batch start
#iterations: 235
currently lose_sum: 92.29780149459839
time_elpased: 2.543
batch start
#iterations: 236
currently lose_sum: 92.39972710609436
time_elpased: 2.51
batch start
#iterations: 237
currently lose_sum: 92.49747371673584
time_elpased: 2.568
batch start
#iterations: 238
currently lose_sum: 91.63624078035355
time_elpased: 2.491
batch start
#iterations: 239
currently lose_sum: 93.18876802921295
time_elpased: 2.535
start validation test
0.563556701031
0.680219146482
0.242770402388
0.357830868411
0.564119890856
65.209
batch start
#iterations: 240
currently lose_sum: 91.76657688617706
time_elpased: 2.4
batch start
#iterations: 241
currently lose_sum: 92.22045814990997
time_elpased: 2.43
batch start
#iterations: 242
currently lose_sum: 92.01306515932083
time_elpased: 2.396
batch start
#iterations: 243
currently lose_sum: 91.42524820566177
time_elpased: 2.421
batch start
#iterations: 244
currently lose_sum: 92.2605938911438
time_elpased: 2.404
batch start
#iterations: 245
currently lose_sum: 92.15737164020538
time_elpased: 2.406
batch start
#iterations: 246
currently lose_sum: 91.44162482023239
time_elpased: 2.408
batch start
#iterations: 247
currently lose_sum: 91.42323684692383
time_elpased: 2.411
batch start
#iterations: 248
currently lose_sum: 91.5443286895752
time_elpased: 2.38
batch start
#iterations: 249
currently lose_sum: 92.48739641904831
time_elpased: 2.426
batch start
#iterations: 250
currently lose_sum: 91.35618376731873
time_elpased: 2.377
batch start
#iterations: 251
currently lose_sum: 91.59422087669373
time_elpased: 2.399
batch start
#iterations: 252
currently lose_sum: 91.98860716819763
time_elpased: 2.393
batch start
#iterations: 253
currently lose_sum: 91.56853449344635
time_elpased: 2.396
batch start
#iterations: 254
currently lose_sum: 91.19298332929611
time_elpased: 2.367
batch start
#iterations: 255
currently lose_sum: 91.73556953668594
time_elpased: 2.423
batch start
#iterations: 256
currently lose_sum: 91.72193908691406
time_elpased: 2.383
batch start
#iterations: 257
currently lose_sum: 91.58973151445389
time_elpased: 2.397
batch start
#iterations: 258
currently lose_sum: 91.32305645942688
time_elpased: 2.313
batch start
#iterations: 259
currently lose_sum: 91.48219132423401
time_elpased: 2.354
start validation test
0.585412371134
0.671025745811
0.337964392302
0.449524330984
0.585846804227
63.972
batch start
#iterations: 260
currently lose_sum: 91.1046679019928
time_elpased: 2.332
batch start
#iterations: 261
currently lose_sum: 92.24774384498596
time_elpased: 2.357
batch start
#iterations: 262
currently lose_sum: 91.45993632078171
time_elpased: 2.377
batch start
#iterations: 263
currently lose_sum: 91.60302966833115
time_elpased: 2.41
batch start
#iterations: 264
currently lose_sum: 91.60536170005798
time_elpased: 2.391
batch start
#iterations: 265
currently lose_sum: 91.24149632453918
time_elpased: 2.43
batch start
#iterations: 266
currently lose_sum: 91.08509057760239
time_elpased: 2.382
batch start
#iterations: 267
currently lose_sum: 91.24156618118286
time_elpased: 2.387
batch start
#iterations: 268
currently lose_sum: 90.60499882698059
time_elpased: 2.39
batch start
#iterations: 269
currently lose_sum: 91.59223002195358
time_elpased: 2.413
batch start
#iterations: 270
currently lose_sum: 91.63120752573013
time_elpased: 2.367
batch start
#iterations: 271
currently lose_sum: 91.17878168821335
time_elpased: 2.391
batch start
#iterations: 272
currently lose_sum: 91.1048014163971
time_elpased: 2.39
batch start
#iterations: 273
currently lose_sum: 91.55741506814957
time_elpased: 2.392
batch start
#iterations: 274
currently lose_sum: 91.80439502000809
time_elpased: 2.36
batch start
#iterations: 275
currently lose_sum: 91.61101543903351
time_elpased: 2.428
batch start
#iterations: 276
currently lose_sum: 90.69180691242218
time_elpased: 2.39
batch start
#iterations: 277
currently lose_sum: 91.44627863168716
time_elpased: 2.421
batch start
#iterations: 278
currently lose_sum: 90.9881683588028
time_elpased: 2.377
batch start
#iterations: 279
currently lose_sum: 91.40662789344788
time_elpased: 2.399
start validation test
0.559175257732
0.686758576467
0.220438406916
0.333748831412
0.55976996252
66.190
batch start
#iterations: 280
currently lose_sum: 91.25116020441055
time_elpased: 2.399
batch start
#iterations: 281
currently lose_sum: 91.12992990016937
time_elpased: 2.401
batch start
#iterations: 282
currently lose_sum: 90.91145795583725
time_elpased: 2.415
batch start
#iterations: 283
currently lose_sum: 90.84539031982422
time_elpased: 2.428
batch start
#iterations: 284
currently lose_sum: 90.9028382897377
time_elpased: 2.384
batch start
#iterations: 285
currently lose_sum: 90.85643672943115
time_elpased: 2.428
batch start
#iterations: 286
currently lose_sum: 91.18570053577423
time_elpased: 2.326
batch start
#iterations: 287
currently lose_sum: 91.03592324256897
time_elpased: 2.427
batch start
#iterations: 288
currently lose_sum: 90.52723103761673
time_elpased: 2.389
batch start
#iterations: 289
currently lose_sum: 90.89435976743698
time_elpased: 2.381
batch start
#iterations: 290
currently lose_sum: 90.59433490037918
time_elpased: 2.378
batch start
#iterations: 291
currently lose_sum: 91.15516972541809
time_elpased: 2.372
batch start
#iterations: 292
currently lose_sum: 91.20667958259583
time_elpased: 2.363
batch start
#iterations: 293
currently lose_sum: 91.6309602856636
time_elpased: 2.36
batch start
#iterations: 294
currently lose_sum: 90.76233470439911
time_elpased: 2.333
batch start
#iterations: 295
currently lose_sum: 90.83904278278351
time_elpased: 2.442
batch start
#iterations: 296
currently lose_sum: 90.54600930213928
time_elpased: 2.282
batch start
#iterations: 297
currently lose_sum: 90.77394163608551
time_elpased: 2.259
batch start
#iterations: 298
currently lose_sum: 91.03919976949692
time_elpased: 2.096
batch start
#iterations: 299
currently lose_sum: 90.63597005605698
time_elpased: 2.154
start validation test
0.61675257732
0.655407245982
0.495214572399
0.564159681107
0.616965956033
62.383
batch start
#iterations: 300
currently lose_sum: 90.47670066356659
time_elpased: 2.064
batch start
#iterations: 301
currently lose_sum: 92.24120384454727
time_elpased: 2.076
batch start
#iterations: 302
currently lose_sum: 90.4201568365097
time_elpased: 2.063
batch start
#iterations: 303
currently lose_sum: 90.27750039100647
time_elpased: 2.092
batch start
#iterations: 304
currently lose_sum: 90.52109485864639
time_elpased: 2.074
batch start
#iterations: 305
currently lose_sum: 90.61470538377762
time_elpased: 2.073
batch start
#iterations: 306
currently lose_sum: 90.51134753227234
time_elpased: 2.056
batch start
#iterations: 307
currently lose_sum: 90.28433084487915
time_elpased: 2.071
batch start
#iterations: 308
currently lose_sum: 90.33831197023392
time_elpased: 2.069
batch start
#iterations: 309
currently lose_sum: 90.63415348529816
time_elpased: 2.088
batch start
#iterations: 310
currently lose_sum: 90.58517277240753
time_elpased: 2.063
batch start
#iterations: 311
currently lose_sum: 90.22084850072861
time_elpased: 2.077
batch start
#iterations: 312
currently lose_sum: 89.88629651069641
time_elpased: 2.056
batch start
#iterations: 313
currently lose_sum: 89.71768915653229
time_elpased: 2.067
batch start
#iterations: 314
currently lose_sum: 90.76979154348373
time_elpased: 2.075
batch start
#iterations: 315
currently lose_sum: 90.16535049676895
time_elpased: 2.077
batch start
#iterations: 316
currently lose_sum: 89.95391291379929
time_elpased: 2.059
batch start
#iterations: 317
currently lose_sum: 90.65802723169327
time_elpased: 2.096
batch start
#iterations: 318
currently lose_sum: 90.16391265392303
time_elpased: 2.085
batch start
#iterations: 319
currently lose_sum: 90.64752870798111
time_elpased: 2.078
start validation test
0.545618556701
0.68870292887
0.169393845837
0.271908813083
0.54627907721
67.991
batch start
#iterations: 320
currently lose_sum: 90.35385304689407
time_elpased: 2.053
batch start
#iterations: 321
currently lose_sum: 90.41255927085876
time_elpased: 2.094
batch start
#iterations: 322
currently lose_sum: 90.44314694404602
time_elpased: 2.062
batch start
#iterations: 323
currently lose_sum: 90.78154861927032
time_elpased: 2.075
batch start
#iterations: 324
currently lose_sum: 90.08554798364639
time_elpased: 2.055
batch start
#iterations: 325
currently lose_sum: 90.26591730117798
time_elpased: 2.08
batch start
#iterations: 326
currently lose_sum: 89.56354117393494
time_elpased: 2.063
batch start
#iterations: 327
currently lose_sum: 90.24448543787003
time_elpased: 2.079
batch start
#iterations: 328
currently lose_sum: 90.26074934005737
time_elpased: 2.055
batch start
#iterations: 329
currently lose_sum: 89.93568956851959
time_elpased: 2.075
batch start
#iterations: 330
currently lose_sum: 89.71810442209244
time_elpased: 2.071
batch start
#iterations: 331
currently lose_sum: 90.7509235739708
time_elpased: 2.076
batch start
#iterations: 332
currently lose_sum: 89.2065144777298
time_elpased: 2.068
batch start
#iterations: 333
currently lose_sum: 90.88930195569992
time_elpased: 2.087
batch start
#iterations: 334
currently lose_sum: 90.12443208694458
time_elpased: 2.062
batch start
#iterations: 335
currently lose_sum: 89.80976873636246
time_elpased: 2.093
batch start
#iterations: 336
currently lose_sum: 90.23935401439667
time_elpased: 2.079
batch start
#iterations: 337
currently lose_sum: 89.95734536647797
time_elpased: 2.094
batch start
#iterations: 338
currently lose_sum: 89.76443523168564
time_elpased: 2.076
batch start
#iterations: 339
currently lose_sum: 89.55090147256851
time_elpased: 2.097
start validation test
0.635463917526
0.611839323467
0.744571369764
0.671711076038
0.635272362565
62.264
batch start
#iterations: 340
currently lose_sum: 90.7974870800972
time_elpased: 2.074
batch start
#iterations: 341
currently lose_sum: 90.26076602935791
time_elpased: 2.091
batch start
#iterations: 342
currently lose_sum: 90.26411467790604
time_elpased: 2.075
batch start
#iterations: 343
currently lose_sum: 90.49963593482971
time_elpased: 2.086
batch start
#iterations: 344
currently lose_sum: 89.61193543672562
time_elpased: 2.062
batch start
#iterations: 345
currently lose_sum: 90.67698806524277
time_elpased: 2.083
batch start
#iterations: 346
currently lose_sum: 89.915651679039
time_elpased: 2.07
batch start
#iterations: 347
currently lose_sum: 89.59069186449051
time_elpased: 2.077
batch start
#iterations: 348
currently lose_sum: 89.7340732216835
time_elpased: 2.075
batch start
#iterations: 349
currently lose_sum: 89.54187273979187
time_elpased: 2.095
batch start
#iterations: 350
currently lose_sum: 89.12142431735992
time_elpased: 2.057
batch start
#iterations: 351
currently lose_sum: 89.62384742498398
time_elpased: 2.089
batch start
#iterations: 352
currently lose_sum: 89.12613892555237
time_elpased: 2.076
batch start
#iterations: 353
currently lose_sum: 89.1780931353569
time_elpased: 2.089
batch start
#iterations: 354
currently lose_sum: 89.64788669347763
time_elpased: 2.056
batch start
#iterations: 355
currently lose_sum: 89.36862444877625
time_elpased: 2.063
batch start
#iterations: 356
currently lose_sum: 89.85697948932648
time_elpased: 2.058
batch start
#iterations: 357
currently lose_sum: 90.17539536952972
time_elpased: 2.08
batch start
#iterations: 358
currently lose_sum: 90.05360269546509
time_elpased: 2.062
batch start
#iterations: 359
currently lose_sum: 89.66188383102417
time_elpased: 2.075
start validation test
0.54087628866
0.685779816514
0.153854070186
0.251323863159
0.541555765858
69.270
batch start
#iterations: 360
currently lose_sum: 88.96945369243622
time_elpased: 2.068
batch start
#iterations: 361
currently lose_sum: 90.25232261419296
time_elpased: 2.107
batch start
#iterations: 362
currently lose_sum: 89.6796156167984
time_elpased: 2.073
batch start
#iterations: 363
currently lose_sum: 90.45509541034698
time_elpased: 2.085
batch start
#iterations: 364
currently lose_sum: 89.73355436325073
time_elpased: 2.076
batch start
#iterations: 365
currently lose_sum: 88.42130607366562
time_elpased: 2.087
batch start
#iterations: 366
currently lose_sum: 89.2859218120575
time_elpased: 2.077
batch start
#iterations: 367
currently lose_sum: 88.56820923089981
time_elpased: 2.1
batch start
#iterations: 368
currently lose_sum: 89.54531300067902
time_elpased: 2.058
batch start
#iterations: 369
currently lose_sum: 89.77811759710312
time_elpased: 2.083
batch start
#iterations: 370
currently lose_sum: 89.41329485177994
time_elpased: 2.071
batch start
#iterations: 371
currently lose_sum: 88.77828747034073
time_elpased: 2.082
batch start
#iterations: 372
currently lose_sum: 89.41147297620773
time_elpased: 2.052
batch start
#iterations: 373
currently lose_sum: 88.71302479505539
time_elpased: 2.079
batch start
#iterations: 374
currently lose_sum: 89.87727129459381
time_elpased: 2.055
batch start
#iterations: 375
currently lose_sum: 89.47360998392105
time_elpased: 2.077
batch start
#iterations: 376
currently lose_sum: 89.40644472837448
time_elpased: 2.075
batch start
#iterations: 377
currently lose_sum: 89.14722901582718
time_elpased: 2.072
batch start
#iterations: 378
currently lose_sum: 89.01184833049774
time_elpased: 2.074
batch start
#iterations: 379
currently lose_sum: 89.23914939165115
time_elpased: 2.086
start validation test
0.598350515464
0.654420022461
0.419779767418
0.511473354232
0.598664023955
63.546
batch start
#iterations: 380
currently lose_sum: 89.8005565404892
time_elpased: 2.064
batch start
#iterations: 381
currently lose_sum: 89.26994127035141
time_elpased: 2.098
batch start
#iterations: 382
currently lose_sum: 89.69137209653854
time_elpased: 2.071
batch start
#iterations: 383
currently lose_sum: 89.32715845108032
time_elpased: 2.085
batch start
#iterations: 384
currently lose_sum: 89.00627326965332
time_elpased: 2.041
batch start
#iterations: 385
currently lose_sum: 89.00110018253326
time_elpased: 2.093
batch start
#iterations: 386
currently lose_sum: 89.60948330163956
time_elpased: 2.058
batch start
#iterations: 387
currently lose_sum: 89.4894288778305
time_elpased: 2.082
batch start
#iterations: 388
currently lose_sum: 88.65750843286514
time_elpased: 2.075
batch start
#iterations: 389
currently lose_sum: 88.70484352111816
time_elpased: 2.069
batch start
#iterations: 390
currently lose_sum: 88.79810851812363
time_elpased: 2.054
batch start
#iterations: 391
currently lose_sum: 88.70018047094345
time_elpased: 2.083
batch start
#iterations: 392
currently lose_sum: 89.63690286874771
time_elpased: 2.079
batch start
#iterations: 393
currently lose_sum: 88.70795321464539
time_elpased: 2.092
batch start
#iterations: 394
currently lose_sum: 88.51522970199585
time_elpased: 2.066
batch start
#iterations: 395
currently lose_sum: 88.71739876270294
time_elpased: 2.09
batch start
#iterations: 396
currently lose_sum: 89.24324136972427
time_elpased: 2.097
batch start
#iterations: 397
currently lose_sum: 88.56137305498123
time_elpased: 2.088
batch start
#iterations: 398
currently lose_sum: 88.7618373632431
time_elpased: 2.079
batch start
#iterations: 399
currently lose_sum: 89.53936225175858
time_elpased: 2.171
start validation test
0.614020618557
0.637203003816
0.532674693836
0.580269058296
0.614163433874
62.656
acc: 0.634
pre: 0.625
rec: 0.671
F1: 0.647
auc: 0.634
