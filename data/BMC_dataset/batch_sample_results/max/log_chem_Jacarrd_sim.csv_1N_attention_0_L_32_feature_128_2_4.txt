start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 101.05616420507431
time_elpased: 1.816
batch start
#iterations: 1
currently lose_sum: 100.33968704938889
time_elpased: 1.774
batch start
#iterations: 2
currently lose_sum: 100.38480913639069
time_elpased: 1.756
batch start
#iterations: 3
currently lose_sum: 100.15466767549515
time_elpased: 1.794
batch start
#iterations: 4
currently lose_sum: 100.19328653812408
time_elpased: 1.791
batch start
#iterations: 5
currently lose_sum: 99.99766105413437
time_elpased: 1.793
batch start
#iterations: 6
currently lose_sum: 100.04175794124603
time_elpased: 1.881
batch start
#iterations: 7
currently lose_sum: 99.86613875627518
time_elpased: 1.8
batch start
#iterations: 8
currently lose_sum: 99.77607160806656
time_elpased: 1.771
batch start
#iterations: 9
currently lose_sum: 99.65201097726822
time_elpased: 1.789
batch start
#iterations: 10
currently lose_sum: 99.6152890920639
time_elpased: 1.791
batch start
#iterations: 11
currently lose_sum: 99.53736227750778
time_elpased: 1.817
batch start
#iterations: 12
currently lose_sum: 99.30128371715546
time_elpased: 1.766
batch start
#iterations: 13
currently lose_sum: 99.40272200107574
time_elpased: 1.763
batch start
#iterations: 14
currently lose_sum: 98.99155670404434
time_elpased: 1.754
batch start
#iterations: 15
currently lose_sum: 99.18651819229126
time_elpased: 1.773
batch start
#iterations: 16
currently lose_sum: 99.00013154745102
time_elpased: 1.79
batch start
#iterations: 17
currently lose_sum: 98.73688179254532
time_elpased: 1.822
batch start
#iterations: 18
currently lose_sum: 98.7843673825264
time_elpased: 1.747
batch start
#iterations: 19
currently lose_sum: 98.6134603023529
time_elpased: 1.808
start validation test
0.596701030928
0.630822391154
0.46969229186
0.538461538462
0.596924014359
64.764
batch start
#iterations: 20
currently lose_sum: 98.8970450758934
time_elpased: 1.761
batch start
#iterations: 21
currently lose_sum: 98.34793883562088
time_elpased: 1.77
batch start
#iterations: 22
currently lose_sum: 98.29834425449371
time_elpased: 1.768
batch start
#iterations: 23
currently lose_sum: 98.44912564754486
time_elpased: 1.749
batch start
#iterations: 24
currently lose_sum: 98.20228111743927
time_elpased: 1.77
batch start
#iterations: 25
currently lose_sum: 97.85946762561798
time_elpased: 1.783
batch start
#iterations: 26
currently lose_sum: 98.27561604976654
time_elpased: 1.802
batch start
#iterations: 27
currently lose_sum: 97.95832312107086
time_elpased: 1.759
batch start
#iterations: 28
currently lose_sum: 98.10820025205612
time_elpased: 1.798
batch start
#iterations: 29
currently lose_sum: 97.83689749240875
time_elpased: 1.748
batch start
#iterations: 30
currently lose_sum: 97.59691572189331
time_elpased: 1.769
batch start
#iterations: 31
currently lose_sum: 97.83658969402313
time_elpased: 1.79
batch start
#iterations: 32
currently lose_sum: 97.85113197565079
time_elpased: 1.781
batch start
#iterations: 33
currently lose_sum: 97.61585587263107
time_elpased: 1.763
batch start
#iterations: 34
currently lose_sum: 97.55079758167267
time_elpased: 1.803
batch start
#iterations: 35
currently lose_sum: 96.95703506469727
time_elpased: 1.748
batch start
#iterations: 36
currently lose_sum: 97.32293158769608
time_elpased: 1.809
batch start
#iterations: 37
currently lose_sum: 97.01740443706512
time_elpased: 1.806
batch start
#iterations: 38
currently lose_sum: 97.17301422357559
time_elpased: 1.843
batch start
#iterations: 39
currently lose_sum: 96.88586795330048
time_elpased: 1.794
start validation test
0.621907216495
0.658884738527
0.508284449933
0.573868587695
0.622106698787
63.111
batch start
#iterations: 40
currently lose_sum: 97.21301597356796
time_elpased: 1.854
batch start
#iterations: 41
currently lose_sum: 97.0480284690857
time_elpased: 1.758
batch start
#iterations: 42
currently lose_sum: 96.82802736759186
time_elpased: 1.811
batch start
#iterations: 43
currently lose_sum: 97.13571208715439
time_elpased: 1.763
batch start
#iterations: 44
currently lose_sum: 96.84470021724701
time_elpased: 1.777
batch start
#iterations: 45
currently lose_sum: 96.78175729513168
time_elpased: 1.776
batch start
#iterations: 46
currently lose_sum: 96.635218501091
time_elpased: 1.829
batch start
#iterations: 47
currently lose_sum: 96.92314046621323
time_elpased: 1.815
batch start
#iterations: 48
currently lose_sum: 96.8731449842453
time_elpased: 1.787
batch start
#iterations: 49
currently lose_sum: 96.776496052742
time_elpased: 1.803
batch start
#iterations: 50
currently lose_sum: 96.33452916145325
time_elpased: 1.773
batch start
#iterations: 51
currently lose_sum: 96.42738509178162
time_elpased: 1.813
batch start
#iterations: 52
currently lose_sum: 96.48839330673218
time_elpased: 1.845
batch start
#iterations: 53
currently lose_sum: 96.4202566742897
time_elpased: 1.773
batch start
#iterations: 54
currently lose_sum: 96.3191813826561
time_elpased: 1.78
batch start
#iterations: 55
currently lose_sum: 96.8026208281517
time_elpased: 1.758
batch start
#iterations: 56
currently lose_sum: 96.46869486570358
time_elpased: 1.834
batch start
#iterations: 57
currently lose_sum: 96.5490174293518
time_elpased: 1.811
batch start
#iterations: 58
currently lose_sum: 97.1073511838913
time_elpased: 1.781
batch start
#iterations: 59
currently lose_sum: 96.9345241189003
time_elpased: 1.723
start validation test
0.574226804124
0.674324000957
0.29000720387
0.405584340818
0.57472579547
64.047
batch start
#iterations: 60
currently lose_sum: 96.68564105033875
time_elpased: 1.814
batch start
#iterations: 61
currently lose_sum: 96.91835415363312
time_elpased: 1.762
batch start
#iterations: 62
currently lose_sum: 95.82988405227661
time_elpased: 1.782
batch start
#iterations: 63
currently lose_sum: 96.14164876937866
time_elpased: 1.727
batch start
#iterations: 64
currently lose_sum: 96.44871377944946
time_elpased: 1.763
batch start
#iterations: 65
currently lose_sum: 96.38281840085983
time_elpased: 1.844
batch start
#iterations: 66
currently lose_sum: 96.29995501041412
time_elpased: 1.848
batch start
#iterations: 67
currently lose_sum: 96.60920739173889
time_elpased: 1.788
batch start
#iterations: 68
currently lose_sum: 95.73995804786682
time_elpased: 1.793
batch start
#iterations: 69
currently lose_sum: 95.93872773647308
time_elpased: 1.858
batch start
#iterations: 70
currently lose_sum: 96.28753650188446
time_elpased: 1.785
batch start
#iterations: 71
currently lose_sum: 96.24816560745239
time_elpased: 1.768
batch start
#iterations: 72
currently lose_sum: 95.9617241024971
time_elpased: 1.808
batch start
#iterations: 73
currently lose_sum: 97.10320037603378
time_elpased: 1.744
batch start
#iterations: 74
currently lose_sum: 95.49490755796432
time_elpased: 1.759
batch start
#iterations: 75
currently lose_sum: 96.06285554170609
time_elpased: 1.783
batch start
#iterations: 76
currently lose_sum: 96.58811074495316
time_elpased: 1.797
batch start
#iterations: 77
currently lose_sum: 95.76079159975052
time_elpased: 1.783
batch start
#iterations: 78
currently lose_sum: 95.6158276796341
time_elpased: 1.811
batch start
#iterations: 79
currently lose_sum: 96.17113137245178
time_elpased: 1.807
start validation test
0.660103092784
0.627251242768
0.792116908511
0.700109150446
0.659871322168
61.184
batch start
#iterations: 80
currently lose_sum: 95.49981331825256
time_elpased: 1.784
batch start
#iterations: 81
currently lose_sum: 96.14620786905289
time_elpased: 1.764
batch start
#iterations: 82
currently lose_sum: 96.52716702222824
time_elpased: 1.781
batch start
#iterations: 83
currently lose_sum: 95.72281223535538
time_elpased: 1.79
batch start
#iterations: 84
currently lose_sum: 96.04628372192383
time_elpased: 1.845
batch start
#iterations: 85
currently lose_sum: 96.4486432671547
time_elpased: 1.785
batch start
#iterations: 86
currently lose_sum: 96.24028891324997
time_elpased: 1.825
batch start
#iterations: 87
currently lose_sum: 95.37391763925552
time_elpased: 1.795
batch start
#iterations: 88
currently lose_sum: 95.30831968784332
time_elpased: 1.8
batch start
#iterations: 89
currently lose_sum: 95.72374731302261
time_elpased: 1.763
batch start
#iterations: 90
currently lose_sum: 96.51289892196655
time_elpased: 1.769
batch start
#iterations: 91
currently lose_sum: 95.98632472753525
time_elpased: 1.774
batch start
#iterations: 92
currently lose_sum: 95.39480835199356
time_elpased: 1.777
batch start
#iterations: 93
currently lose_sum: 96.22307032346725
time_elpased: 1.788
batch start
#iterations: 94
currently lose_sum: 96.15235406160355
time_elpased: 1.819
batch start
#iterations: 95
currently lose_sum: 96.13036340475082
time_elpased: 1.764
batch start
#iterations: 96
currently lose_sum: 95.81969821453094
time_elpased: 1.758
batch start
#iterations: 97
currently lose_sum: 95.38550978899002
time_elpased: 1.79
batch start
#iterations: 98
currently lose_sum: 94.80318504571915
time_elpased: 1.795
batch start
#iterations: 99
currently lose_sum: 95.93610328435898
time_elpased: 1.777
start validation test
0.524175257732
0.66922005571
0.0988988370896
0.172330314714
0.524921896083
68.573
batch start
#iterations: 100
currently lose_sum: 95.996841609478
time_elpased: 1.777
batch start
#iterations: 101
currently lose_sum: 95.69205343723297
time_elpased: 1.827
batch start
#iterations: 102
currently lose_sum: 95.54212588071823
time_elpased: 1.792
batch start
#iterations: 103
currently lose_sum: 96.21376329660416
time_elpased: 1.805
batch start
#iterations: 104
currently lose_sum: 95.92512118816376
time_elpased: 1.763
batch start
#iterations: 105
currently lose_sum: 95.36374616622925
time_elpased: 1.772
batch start
#iterations: 106
currently lose_sum: 95.81136083602905
time_elpased: 1.771
batch start
#iterations: 107
currently lose_sum: 95.58391267061234
time_elpased: 1.774
batch start
#iterations: 108
currently lose_sum: 95.56412905454636
time_elpased: 1.793
batch start
#iterations: 109
currently lose_sum: 95.24081861972809
time_elpased: 1.802
batch start
#iterations: 110
currently lose_sum: 95.21378284692764
time_elpased: 1.815
batch start
#iterations: 111
currently lose_sum: 95.56904649734497
time_elpased: 1.846
batch start
#iterations: 112
currently lose_sum: 95.23876202106476
time_elpased: 1.806
batch start
#iterations: 113
currently lose_sum: 95.90260434150696
time_elpased: 1.804
batch start
#iterations: 114
currently lose_sum: 95.41007852554321
time_elpased: 1.76
batch start
#iterations: 115
currently lose_sum: 94.93177968263626
time_elpased: 1.794
batch start
#iterations: 116
currently lose_sum: 95.83645576238632
time_elpased: 1.807
batch start
#iterations: 117
currently lose_sum: 94.95026963949203
time_elpased: 1.818
batch start
#iterations: 118
currently lose_sum: 95.23070973157883
time_elpased: 1.882
batch start
#iterations: 119
currently lose_sum: 95.64197605848312
time_elpased: 1.847
start validation test
0.65118556701
0.655459527825
0.640012349491
0.647643842749
0.651205183317
61.213
batch start
#iterations: 120
currently lose_sum: 95.56514751911163
time_elpased: 1.821
batch start
#iterations: 121
currently lose_sum: 95.03218686580658
time_elpased: 1.821
batch start
#iterations: 122
currently lose_sum: 95.78816586732864
time_elpased: 1.773
batch start
#iterations: 123
currently lose_sum: 95.15413808822632
time_elpased: 1.797
batch start
#iterations: 124
currently lose_sum: 96.03760010004044
time_elpased: 1.811
batch start
#iterations: 125
currently lose_sum: 94.89364564418793
time_elpased: 1.798
batch start
#iterations: 126
currently lose_sum: 95.41316616535187
time_elpased: 1.87
batch start
#iterations: 127
currently lose_sum: 95.83567440509796
time_elpased: 1.783
batch start
#iterations: 128
currently lose_sum: 95.2191908955574
time_elpased: 1.971
batch start
#iterations: 129
currently lose_sum: 94.61049002408981
time_elpased: 1.759
batch start
#iterations: 130
currently lose_sum: 94.93397641181946
time_elpased: 1.793
batch start
#iterations: 131
currently lose_sum: 94.99073499441147
time_elpased: 1.791
batch start
#iterations: 132
currently lose_sum: 98.9971661567688
time_elpased: 1.783
batch start
#iterations: 133
currently lose_sum: 95.80931723117828
time_elpased: 1.777
batch start
#iterations: 134
currently lose_sum: 95.45353811979294
time_elpased: 1.781
batch start
#iterations: 135
currently lose_sum: 96.28565794229507
time_elpased: 1.779
batch start
#iterations: 136
currently lose_sum: 96.27785909175873
time_elpased: 1.813
batch start
#iterations: 137
currently lose_sum: 94.97294116020203
time_elpased: 1.785
batch start
#iterations: 138
currently lose_sum: 95.3842214345932
time_elpased: 1.768
batch start
#iterations: 139
currently lose_sum: 94.71314018964767
time_elpased: 1.757
start validation test
0.651391752577
0.608332110899
0.853555624164
0.710376429275
0.651036822719
61.291
batch start
#iterations: 140
currently lose_sum: 94.81564432382584
time_elpased: 1.902
batch start
#iterations: 141
currently lose_sum: 94.20346939563751
time_elpased: 1.773
batch start
#iterations: 142
currently lose_sum: 95.36191827058792
time_elpased: 1.832
batch start
#iterations: 143
currently lose_sum: 94.98636364936829
time_elpased: 1.779
batch start
#iterations: 144
currently lose_sum: 95.60658544301987
time_elpased: 1.823
batch start
#iterations: 145
currently lose_sum: 94.2134672999382
time_elpased: 1.822
batch start
#iterations: 146
currently lose_sum: 94.35856807231903
time_elpased: 1.777
batch start
#iterations: 147
currently lose_sum: 94.35311669111252
time_elpased: 1.793
batch start
#iterations: 148
currently lose_sum: 94.61325311660767
time_elpased: 1.778
batch start
#iterations: 149
currently lose_sum: 94.44640618562698
time_elpased: 1.848
batch start
#iterations: 150
currently lose_sum: 94.77674502134323
time_elpased: 1.783
batch start
#iterations: 151
currently lose_sum: 94.4274492263794
time_elpased: 1.805
batch start
#iterations: 152
currently lose_sum: 94.35769575834274
time_elpased: 1.787
batch start
#iterations: 153
currently lose_sum: 95.03272724151611
time_elpased: 1.776
batch start
#iterations: 154
currently lose_sum: 95.62334084510803
time_elpased: 1.791
batch start
#iterations: 155
currently lose_sum: 94.43895810842514
time_elpased: 1.767
batch start
#iterations: 156
currently lose_sum: 94.67964178323746
time_elpased: 1.776
batch start
#iterations: 157
currently lose_sum: 94.61422157287598
time_elpased: 1.874
batch start
#iterations: 158
currently lose_sum: 94.3738676905632
time_elpased: 1.761
batch start
#iterations: 159
currently lose_sum: 94.04772537946701
time_elpased: 1.781
start validation test
0.665
0.639331485972
0.759802408151
0.694380437338
0.66483355975
60.669
batch start
#iterations: 160
currently lose_sum: 94.55554974079132
time_elpased: 1.82
batch start
#iterations: 161
currently lose_sum: 95.08450227975845
time_elpased: 1.78
batch start
#iterations: 162
currently lose_sum: 94.11456418037415
time_elpased: 1.801
batch start
#iterations: 163
currently lose_sum: 93.70600825548172
time_elpased: 1.776
batch start
#iterations: 164
currently lose_sum: 94.98719257116318
time_elpased: 1.771
batch start
#iterations: 165
currently lose_sum: 98.08338284492493
time_elpased: 1.772
batch start
#iterations: 166
currently lose_sum: 93.85212230682373
time_elpased: 1.764
batch start
#iterations: 167
currently lose_sum: 94.66979414224625
time_elpased: 1.776
batch start
#iterations: 168
currently lose_sum: 96.06747442483902
time_elpased: 1.795
batch start
#iterations: 169
currently lose_sum: 94.74227440357208
time_elpased: 1.754
batch start
#iterations: 170
currently lose_sum: 94.54659080505371
time_elpased: 1.818
batch start
#iterations: 171
currently lose_sum: 94.99514997005463
time_elpased: 1.809
batch start
#iterations: 172
currently lose_sum: 100.29415792226791
time_elpased: 1.811
batch start
#iterations: 173
currently lose_sum: 98.96951919794083
time_elpased: 1.926
batch start
#iterations: 174
currently lose_sum: 96.00794500112534
time_elpased: 1.773
batch start
#iterations: 175
currently lose_sum: 94.9304810166359
time_elpased: 1.833
batch start
#iterations: 176
currently lose_sum: 95.9002503156662
time_elpased: 1.758
batch start
#iterations: 177
currently lose_sum: 94.60943961143494
time_elpased: 1.802
batch start
#iterations: 178
currently lose_sum: 94.1107891201973
time_elpased: 1.885
batch start
#iterations: 179
currently lose_sum: 94.56743943691254
time_elpased: 1.784
start validation test
0.662886597938
0.634356762243
0.77184316147
0.696378830084
0.662695307885
60.191
batch start
#iterations: 180
currently lose_sum: 95.24330675601959
time_elpased: 1.809
batch start
#iterations: 181
currently lose_sum: 94.79886782169342
time_elpased: 1.818
batch start
#iterations: 182
currently lose_sum: 94.28470927476883
time_elpased: 1.8
batch start
#iterations: 183
currently lose_sum: 93.95515865087509
time_elpased: 1.777
batch start
#iterations: 184
currently lose_sum: 93.63259184360504
time_elpased: 1.81
batch start
#iterations: 185
currently lose_sum: 94.09916764497757
time_elpased: 1.811
batch start
#iterations: 186
currently lose_sum: 95.23686403036118
time_elpased: 1.866
batch start
#iterations: 187
currently lose_sum: 94.21860164403915
time_elpased: 1.774
batch start
#iterations: 188
currently lose_sum: 93.05128055810928
time_elpased: 1.786
batch start
#iterations: 189
currently lose_sum: 93.37760722637177
time_elpased: 1.857
batch start
#iterations: 190
currently lose_sum: 94.26125091314316
time_elpased: 1.793
batch start
#iterations: 191
currently lose_sum: 94.68524825572968
time_elpased: 1.826
batch start
#iterations: 192
currently lose_sum: 94.32277190685272
time_elpased: 1.813
batch start
#iterations: 193
currently lose_sum: 94.99346423149109
time_elpased: 1.825
batch start
#iterations: 194
currently lose_sum: 94.62607073783875
time_elpased: 1.796
batch start
#iterations: 195
currently lose_sum: 93.72160458564758
time_elpased: 1.912
batch start
#iterations: 196
currently lose_sum: 93.17857086658478
time_elpased: 1.84
batch start
#iterations: 197
currently lose_sum: 93.54800260066986
time_elpased: 1.909
batch start
#iterations: 198
currently lose_sum: 94.36706268787384
time_elpased: 1.807
batch start
#iterations: 199
currently lose_sum: 93.48606592416763
time_elpased: 1.797
start validation test
0.623917525773
0.66471628793
0.502727179171
0.572483300129
0.624130294119
61.776
batch start
#iterations: 200
currently lose_sum: 93.88347554206848
time_elpased: 1.816
batch start
#iterations: 201
currently lose_sum: 93.71378600597382
time_elpased: 1.831
batch start
#iterations: 202
currently lose_sum: 93.63977527618408
time_elpased: 1.774
batch start
#iterations: 203
currently lose_sum: 94.00044578313828
time_elpased: 1.785
batch start
#iterations: 204
currently lose_sum: 93.60378813743591
time_elpased: 1.765
batch start
#iterations: 205
currently lose_sum: 92.97500693798065
time_elpased: 1.795
batch start
#iterations: 206
currently lose_sum: 93.9717081785202
time_elpased: 1.777
batch start
#iterations: 207
currently lose_sum: 93.76194649934769
time_elpased: 1.744
batch start
#iterations: 208
currently lose_sum: 92.78581464290619
time_elpased: 1.785
batch start
#iterations: 209
currently lose_sum: 92.88777333498001
time_elpased: 1.761
batch start
#iterations: 210
currently lose_sum: 93.35811227560043
time_elpased: 1.749
batch start
#iterations: 211
currently lose_sum: 92.7623723745346
time_elpased: 1.758
batch start
#iterations: 212
currently lose_sum: 93.59044563770294
time_elpased: 1.786
batch start
#iterations: 213
currently lose_sum: 93.29151231050491
time_elpased: 1.796
batch start
#iterations: 214
currently lose_sum: 93.36706346273422
time_elpased: 1.771
batch start
#iterations: 215
currently lose_sum: 92.94959729909897
time_elpased: 1.789
batch start
#iterations: 216
currently lose_sum: 92.91939729452133
time_elpased: 1.767
batch start
#iterations: 217
currently lose_sum: 93.7311863899231
time_elpased: 1.795
batch start
#iterations: 218
currently lose_sum: 92.51872944831848
time_elpased: 1.791
batch start
#iterations: 219
currently lose_sum: 93.02291172742844
time_elpased: 1.811
start validation test
0.655051546392
0.633365664403
0.739219923845
0.682211036186
0.654903775823
60.999
batch start
#iterations: 220
currently lose_sum: 93.31347507238388
time_elpased: 1.76
batch start
#iterations: 221
currently lose_sum: 93.2101401090622
time_elpased: 1.777
batch start
#iterations: 222
currently lose_sum: 92.9141491651535
time_elpased: 1.765
batch start
#iterations: 223
currently lose_sum: 93.07786029577255
time_elpased: 1.812
batch start
#iterations: 224
currently lose_sum: 93.2293444275856
time_elpased: 1.817
batch start
#iterations: 225
currently lose_sum: 92.47338885068893
time_elpased: 1.833
batch start
#iterations: 226
currently lose_sum: 92.61742848157883
time_elpased: 1.808
batch start
#iterations: 227
currently lose_sum: 93.48389846086502
time_elpased: 1.841
batch start
#iterations: 228
currently lose_sum: 93.56103479862213
time_elpased: 1.798
batch start
#iterations: 229
currently lose_sum: 93.3093187212944
time_elpased: 1.786
batch start
#iterations: 230
currently lose_sum: 92.60560524463654
time_elpased: 1.853
batch start
#iterations: 231
currently lose_sum: 92.17108184099197
time_elpased: 1.776
batch start
#iterations: 232
currently lose_sum: 92.52799606323242
time_elpased: 1.758
batch start
#iterations: 233
currently lose_sum: 92.80539804697037
time_elpased: 1.839
batch start
#iterations: 234
currently lose_sum: 93.61214965581894
time_elpased: 1.762
batch start
#iterations: 235
currently lose_sum: 93.43799531459808
time_elpased: 1.804
batch start
#iterations: 236
currently lose_sum: 92.47678828239441
time_elpased: 1.847
batch start
#iterations: 237
currently lose_sum: 92.38356113433838
time_elpased: 1.824
batch start
#iterations: 238
currently lose_sum: 92.54905647039413
time_elpased: 1.8
batch start
#iterations: 239
currently lose_sum: 92.13908261060715
time_elpased: 1.798
start validation test
0.653917525773
0.648149975333
0.676031697026
0.66179730002
0.653878700935
60.493
batch start
#iterations: 240
currently lose_sum: 92.79447108507156
time_elpased: 1.771
batch start
#iterations: 241
currently lose_sum: 92.02313023805618
time_elpased: 1.761
batch start
#iterations: 242
currently lose_sum: 92.32319086790085
time_elpased: 1.762
batch start
#iterations: 243
currently lose_sum: 92.34697538614273
time_elpased: 1.805
batch start
#iterations: 244
currently lose_sum: 93.36806237697601
time_elpased: 1.81
batch start
#iterations: 245
currently lose_sum: 92.96325021982193
time_elpased: 1.848
batch start
#iterations: 246
currently lose_sum: 92.1406689286232
time_elpased: 1.801
batch start
#iterations: 247
currently lose_sum: 91.46066057682037
time_elpased: 1.831
batch start
#iterations: 248
currently lose_sum: 92.75868707895279
time_elpased: 1.856
batch start
#iterations: 249
currently lose_sum: 91.869802236557
time_elpased: 1.802
batch start
#iterations: 250
currently lose_sum: 92.84466105699539
time_elpased: 1.8
batch start
#iterations: 251
currently lose_sum: 92.44690549373627
time_elpased: 1.734
batch start
#iterations: 252
currently lose_sum: 92.43048691749573
time_elpased: 1.823
batch start
#iterations: 253
currently lose_sum: 91.9930272102356
time_elpased: 1.771
batch start
#iterations: 254
currently lose_sum: 91.62678068876266
time_elpased: 1.797
batch start
#iterations: 255
currently lose_sum: 92.05316972732544
time_elpased: 1.822
batch start
#iterations: 256
currently lose_sum: 92.06816238164902
time_elpased: 1.753
batch start
#iterations: 257
currently lose_sum: 91.85334467887878
time_elpased: 1.752
batch start
#iterations: 258
currently lose_sum: 91.04365539550781
time_elpased: 1.79
batch start
#iterations: 259
currently lose_sum: 92.43020612001419
time_elpased: 1.774
start validation test
0.655670103093
0.651895568671
0.670680251106
0.661154509486
0.655643750463
60.647
batch start
#iterations: 260
currently lose_sum: 92.19862008094788
time_elpased: 1.753
batch start
#iterations: 261
currently lose_sum: 92.01323795318604
time_elpased: 1.808
batch start
#iterations: 262
currently lose_sum: 91.457488656044
time_elpased: 1.767
batch start
#iterations: 263
currently lose_sum: 91.49752759933472
time_elpased: 1.794
batch start
#iterations: 264
currently lose_sum: 92.63923925161362
time_elpased: 1.819
batch start
#iterations: 265
currently lose_sum: 92.14937144517899
time_elpased: 1.779
batch start
#iterations: 266
currently lose_sum: 91.96967178583145
time_elpased: 1.748
batch start
#iterations: 267
currently lose_sum: 91.44237035512924
time_elpased: 1.749
batch start
#iterations: 268
currently lose_sum: 92.08842831850052
time_elpased: 1.809
batch start
#iterations: 269
currently lose_sum: 92.39794838428497
time_elpased: 1.748
batch start
#iterations: 270
currently lose_sum: 92.04157239198685
time_elpased: 1.741
batch start
#iterations: 271
currently lose_sum: 90.87150406837463
time_elpased: 1.79
batch start
#iterations: 272
currently lose_sum: 93.60069364309311
time_elpased: 1.766
batch start
#iterations: 273
currently lose_sum: 93.81836175918579
time_elpased: 1.804
batch start
#iterations: 274
currently lose_sum: 92.36808437108994
time_elpased: 1.761
batch start
#iterations: 275
currently lose_sum: 91.0597591996193
time_elpased: 1.806
batch start
#iterations: 276
currently lose_sum: 91.90726733207703
time_elpased: 1.767
batch start
#iterations: 277
currently lose_sum: 91.16221332550049
time_elpased: 1.804
batch start
#iterations: 278
currently lose_sum: 91.4456434249878
time_elpased: 1.787
batch start
#iterations: 279
currently lose_sum: 91.87619352340698
time_elpased: 1.81
start validation test
0.653865979381
0.638904312419
0.710507358238
0.672806119963
0.653766536705
60.699
batch start
#iterations: 280
currently lose_sum: 91.15874803066254
time_elpased: 1.776
batch start
#iterations: 281
currently lose_sum: 92.36427468061447
time_elpased: 1.781
batch start
#iterations: 282
currently lose_sum: 91.06759941577911
time_elpased: 1.838
batch start
#iterations: 283
currently lose_sum: 91.576735496521
time_elpased: 1.813
batch start
#iterations: 284
currently lose_sum: 91.28040957450867
time_elpased: 1.763
batch start
#iterations: 285
currently lose_sum: 91.85745108127594
time_elpased: 1.78
batch start
#iterations: 286
currently lose_sum: 91.36269420385361
time_elpased: 1.792
batch start
#iterations: 287
currently lose_sum: 90.94979816675186
time_elpased: 1.815
batch start
#iterations: 288
currently lose_sum: 90.69416731595993
time_elpased: 1.854
batch start
#iterations: 289
currently lose_sum: 91.31496143341064
time_elpased: 1.844
batch start
#iterations: 290
currently lose_sum: 91.21669644117355
time_elpased: 1.808
batch start
#iterations: 291
currently lose_sum: 92.04763633012772
time_elpased: 1.901
batch start
#iterations: 292
currently lose_sum: 90.34223234653473
time_elpased: 1.833
batch start
#iterations: 293
currently lose_sum: 90.14038306474686
time_elpased: 1.805
batch start
#iterations: 294
currently lose_sum: 90.89340710639954
time_elpased: 1.795
batch start
#iterations: 295
currently lose_sum: 90.36944031715393
time_elpased: 1.761
batch start
#iterations: 296
currently lose_sum: 90.88134044408798
time_elpased: 1.814
batch start
#iterations: 297
currently lose_sum: 90.69036823511124
time_elpased: 1.794
batch start
#iterations: 298
currently lose_sum: 90.08879208564758
time_elpased: 1.799
batch start
#iterations: 299
currently lose_sum: 91.00049787759781
time_elpased: 1.772
start validation test
0.641958762887
0.63675846412
0.663888031285
0.650040306328
0.641920262674
61.141
batch start
#iterations: 300
currently lose_sum: 90.0964024066925
time_elpased: 1.812
batch start
#iterations: 301
currently lose_sum: 90.80431270599365
time_elpased: 1.792
batch start
#iterations: 302
currently lose_sum: 90.32265120744705
time_elpased: 1.839
batch start
#iterations: 303
currently lose_sum: 90.59697961807251
time_elpased: 1.753
batch start
#iterations: 304
currently lose_sum: 91.6955035328865
time_elpased: 1.839
batch start
#iterations: 305
currently lose_sum: 90.43404251337051
time_elpased: 1.803
batch start
#iterations: 306
currently lose_sum: 91.3987957239151
time_elpased: 1.75
batch start
#iterations: 307
currently lose_sum: 90.0739815235138
time_elpased: 1.845
batch start
#iterations: 308
currently lose_sum: 90.74999326467514
time_elpased: 1.782
batch start
#iterations: 309
currently lose_sum: 89.9386796951294
time_elpased: 1.78
batch start
#iterations: 310
currently lose_sum: 91.62009102106094
time_elpased: 1.776
batch start
#iterations: 311
currently lose_sum: 89.89377266168594
time_elpased: 1.769
batch start
#iterations: 312
currently lose_sum: 89.94840002059937
time_elpased: 1.786
batch start
#iterations: 313
currently lose_sum: 89.58353102207184
time_elpased: 1.787
batch start
#iterations: 314
currently lose_sum: 90.3430290222168
time_elpased: 1.796
batch start
#iterations: 315
currently lose_sum: 89.8127469420433
time_elpased: 1.74
batch start
#iterations: 316
currently lose_sum: 90.56226164102554
time_elpased: 1.786
batch start
#iterations: 317
currently lose_sum: 89.59027791023254
time_elpased: 1.809
batch start
#iterations: 318
currently lose_sum: 90.323517203331
time_elpased: 1.812
batch start
#iterations: 319
currently lose_sum: 89.97400975227356
time_elpased: 1.915
start validation test
0.640515463918
0.622335206494
0.71801996501
0.666762232416
0.640379392812
61.057
batch start
#iterations: 320
currently lose_sum: 89.86195504665375
time_elpased: 1.862
batch start
#iterations: 321
currently lose_sum: 90.62764865159988
time_elpased: 1.845
batch start
#iterations: 322
currently lose_sum: 89.81217032670975
time_elpased: 1.984
batch start
#iterations: 323
currently lose_sum: 91.0438984632492
time_elpased: 2.016
batch start
#iterations: 324
currently lose_sum: 89.77613228559494
time_elpased: 1.879
batch start
#iterations: 325
currently lose_sum: 89.51855558156967
time_elpased: 1.774
batch start
#iterations: 326
currently lose_sum: 89.43055200576782
time_elpased: 1.829
batch start
#iterations: 327
currently lose_sum: 91.8183821439743
time_elpased: 1.807
batch start
#iterations: 328
currently lose_sum: 90.93541073799133
time_elpased: 1.754
batch start
#iterations: 329
currently lose_sum: 90.1813639998436
time_elpased: 1.758
batch start
#iterations: 330
currently lose_sum: 89.59918105602264
time_elpased: 1.893
batch start
#iterations: 331
currently lose_sum: 88.96041816473007
time_elpased: 1.742
batch start
#iterations: 332
currently lose_sum: 89.41907387971878
time_elpased: 1.81
batch start
#iterations: 333
currently lose_sum: 89.38925969600677
time_elpased: 1.786
batch start
#iterations: 334
currently lose_sum: 89.91568744182587
time_elpased: 1.897
batch start
#iterations: 335
currently lose_sum: 89.7036474943161
time_elpased: 1.77
batch start
#iterations: 336
currently lose_sum: 90.00322782993317
time_elpased: 1.764
batch start
#iterations: 337
currently lose_sum: 89.1297852396965
time_elpased: 1.755
batch start
#iterations: 338
currently lose_sum: 89.36070764064789
time_elpased: 1.753
batch start
#iterations: 339
currently lose_sum: 89.48545849323273
time_elpased: 1.817
start validation test
0.629484536082
0.676877885019
0.49799320778
0.573817146923
0.62971538939
63.632
batch start
#iterations: 340
currently lose_sum: 90.18621569871902
time_elpased: 1.759
batch start
#iterations: 341
currently lose_sum: 90.06148439645767
time_elpased: 1.79
batch start
#iterations: 342
currently lose_sum: 88.90453714132309
time_elpased: 1.805
batch start
#iterations: 343
currently lose_sum: 88.34772300720215
time_elpased: 1.76
batch start
#iterations: 344
currently lose_sum: 88.5786943435669
time_elpased: 1.771
batch start
#iterations: 345
currently lose_sum: 88.70778805017471
time_elpased: 1.746
batch start
#iterations: 346
currently lose_sum: 89.49733537435532
time_elpased: 1.779
batch start
#iterations: 347
currently lose_sum: 88.66235327720642
time_elpased: 1.761
batch start
#iterations: 348
currently lose_sum: 88.97984284162521
time_elpased: 1.753
batch start
#iterations: 349
currently lose_sum: 89.2492647767067
time_elpased: 1.772
batch start
#iterations: 350
currently lose_sum: 88.73502725362778
time_elpased: 1.819
batch start
#iterations: 351
currently lose_sum: 88.48211467266083
time_elpased: 1.757
batch start
#iterations: 352
currently lose_sum: 89.19783598184586
time_elpased: 1.755
batch start
#iterations: 353
currently lose_sum: 88.9460021853447
time_elpased: 1.742
batch start
#iterations: 354
currently lose_sum: 88.64874988794327
time_elpased: 1.714
batch start
#iterations: 355
currently lose_sum: 89.1398743391037
time_elpased: 1.769
batch start
#iterations: 356
currently lose_sum: 89.41945171356201
time_elpased: 1.819
batch start
#iterations: 357
currently lose_sum: 88.10847169160843
time_elpased: 1.785
batch start
#iterations: 358
currently lose_sum: 89.3035461306572
time_elpased: 1.803
batch start
#iterations: 359
currently lose_sum: 88.65915244817734
time_elpased: 1.781
start validation test
0.641082474227
0.613952333664
0.763507255326
0.680610981148
0.640867538641
61.180
batch start
#iterations: 360
currently lose_sum: 88.9201568365097
time_elpased: 1.802
batch start
#iterations: 361
currently lose_sum: 88.46323210000992
time_elpased: 1.834
batch start
#iterations: 362
currently lose_sum: 88.50414043664932
time_elpased: 1.781
batch start
#iterations: 363
currently lose_sum: 88.71950906515121
time_elpased: 1.778
batch start
#iterations: 364
currently lose_sum: 87.78181046247482
time_elpased: 1.781
batch start
#iterations: 365
currently lose_sum: 88.61866760253906
time_elpased: 1.749
batch start
#iterations: 366
currently lose_sum: 88.06628179550171
time_elpased: 1.813
batch start
#iterations: 367
currently lose_sum: 88.92821311950684
time_elpased: 1.779
batch start
#iterations: 368
currently lose_sum: 88.6806611418724
time_elpased: 1.756
batch start
#iterations: 369
currently lose_sum: 88.4631844162941
time_elpased: 1.767
batch start
#iterations: 370
currently lose_sum: 87.95714497566223
time_elpased: 1.832
batch start
#iterations: 371
currently lose_sum: 87.93538522720337
time_elpased: 1.92
batch start
#iterations: 372
currently lose_sum: 88.87577146291733
time_elpased: 1.818
batch start
#iterations: 373
currently lose_sum: 88.46254575252533
time_elpased: 1.794
batch start
#iterations: 374
currently lose_sum: 88.93606460094452
time_elpased: 1.896
batch start
#iterations: 375
currently lose_sum: 88.2914651632309
time_elpased: 1.861
batch start
#iterations: 376
currently lose_sum: 89.66070312261581
time_elpased: 1.762
batch start
#iterations: 377
currently lose_sum: 88.22203660011292
time_elpased: 1.74
batch start
#iterations: 378
currently lose_sum: 87.39716678857803
time_elpased: 1.777
batch start
#iterations: 379
currently lose_sum: 88.07589131593704
time_elpased: 1.837
start validation test
0.642474226804
0.653935569578
0.607903673973
0.63008
0.642534920742
60.975
batch start
#iterations: 380
currently lose_sum: 88.25578373670578
time_elpased: 1.79
batch start
#iterations: 381
currently lose_sum: 88.14832490682602
time_elpased: 1.773
batch start
#iterations: 382
currently lose_sum: 87.23806309700012
time_elpased: 1.762
batch start
#iterations: 383
currently lose_sum: 87.69234144687653
time_elpased: 1.768
batch start
#iterations: 384
currently lose_sum: 88.76226037740707
time_elpased: 1.81
batch start
#iterations: 385
currently lose_sum: 88.6826503276825
time_elpased: 1.816
batch start
#iterations: 386
currently lose_sum: 88.00739705562592
time_elpased: 1.799
batch start
#iterations: 387
currently lose_sum: 88.26494216918945
time_elpased: 1.756
batch start
#iterations: 388
currently lose_sum: 87.80618298053741
time_elpased: 1.807
batch start
#iterations: 389
currently lose_sum: 88.1223372220993
time_elpased: 1.757
batch start
#iterations: 390
currently lose_sum: 87.97278267145157
time_elpased: 1.796
batch start
#iterations: 391
currently lose_sum: 87.25238007307053
time_elpased: 1.756
batch start
#iterations: 392
currently lose_sum: 87.38631451129913
time_elpased: 1.787
batch start
#iterations: 393
currently lose_sum: 87.53618490695953
time_elpased: 1.753
batch start
#iterations: 394
currently lose_sum: 87.6198919415474
time_elpased: 1.776
batch start
#iterations: 395
currently lose_sum: 87.6502177119255
time_elpased: 1.794
batch start
#iterations: 396
currently lose_sum: 87.17201340198517
time_elpased: 1.799
batch start
#iterations: 397
currently lose_sum: 87.80797511339188
time_elpased: 2.0
batch start
#iterations: 398
currently lose_sum: 87.85701191425323
time_elpased: 2.473
batch start
#iterations: 399
currently lose_sum: 87.80466866493225
time_elpased: 2.216
start validation test
0.644278350515
0.622222222222
0.737676237522
0.675048264821
0.644114376119
60.953
acc: 0.659
pre: 0.630
rec: 0.777
F1: 0.696
auc: 0.659
