start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.54152458906174
time_elpased: 2.651
batch start
#iterations: 1
currently lose_sum: 100.36293423175812
time_elpased: 2.324
batch start
#iterations: 2
currently lose_sum: 100.32492458820343
time_elpased: 2.417
batch start
#iterations: 3
currently lose_sum: 100.25816631317139
time_elpased: 2.4
batch start
#iterations: 4
currently lose_sum: 100.17573982477188
time_elpased: 2.388
batch start
#iterations: 5
currently lose_sum: 100.02908545732498
time_elpased: 2.323
batch start
#iterations: 6
currently lose_sum: 100.01514232158661
time_elpased: 2.427
batch start
#iterations: 7
currently lose_sum: 99.85647815465927
time_elpased: 2.604
batch start
#iterations: 8
currently lose_sum: 99.73276442289352
time_elpased: 2.463
batch start
#iterations: 9
currently lose_sum: 99.61475771665573
time_elpased: 2.48
batch start
#iterations: 10
currently lose_sum: 99.51838225126266
time_elpased: 2.475
batch start
#iterations: 11
currently lose_sum: 99.52536845207214
time_elpased: 2.437
batch start
#iterations: 12
currently lose_sum: 99.26013100147247
time_elpased: 2.345
batch start
#iterations: 13
currently lose_sum: 99.43544387817383
time_elpased: 2.332
batch start
#iterations: 14
currently lose_sum: 99.28700929880142
time_elpased: 2.337
batch start
#iterations: 15
currently lose_sum: 98.9541146159172
time_elpased: 2.321
batch start
#iterations: 16
currently lose_sum: 99.20438402891159
time_elpased: 2.3
batch start
#iterations: 17
currently lose_sum: 98.83981627225876
time_elpased: 2.459
batch start
#iterations: 18
currently lose_sum: 98.84358638525009
time_elpased: 2.422
batch start
#iterations: 19
currently lose_sum: 98.87402045726776
time_elpased: 2.246
start validation test
0.61793814433
0.601946041575
0.700319028507
0.647416991723
0.617793511981
65.468
batch start
#iterations: 20
currently lose_sum: 98.91830259561539
time_elpased: 2.426
batch start
#iterations: 21
currently lose_sum: 98.93941003084183
time_elpased: 2.345
batch start
#iterations: 22
currently lose_sum: 98.40188467502594
time_elpased: 2.515
batch start
#iterations: 23
currently lose_sum: 99.40182477235794
time_elpased: 2.486
batch start
#iterations: 24
currently lose_sum: 98.55190217494965
time_elpased: 2.374
batch start
#iterations: 25
currently lose_sum: 98.52380740642548
time_elpased: 2.478
batch start
#iterations: 26
currently lose_sum: 98.98738718032837
time_elpased: 2.414
batch start
#iterations: 27
currently lose_sum: 98.43670952320099
time_elpased: 2.482
batch start
#iterations: 28
currently lose_sum: 98.49812418222427
time_elpased: 2.445
batch start
#iterations: 29
currently lose_sum: 98.04429930448532
time_elpased: 2.42
batch start
#iterations: 30
currently lose_sum: 98.18701249361038
time_elpased: 2.453
batch start
#iterations: 31
currently lose_sum: 99.00192481279373
time_elpased: 2.371
batch start
#iterations: 32
currently lose_sum: 98.80515569448471
time_elpased: 2.386
batch start
#iterations: 33
currently lose_sum: 98.06853818893433
time_elpased: 2.398
batch start
#iterations: 34
currently lose_sum: 98.8342227935791
time_elpased: 2.473
batch start
#iterations: 35
currently lose_sum: 98.4807316660881
time_elpased: 2.472
batch start
#iterations: 36
currently lose_sum: 98.31935530900955
time_elpased: 2.506
batch start
#iterations: 37
currently lose_sum: 97.91146445274353
time_elpased: 2.553
batch start
#iterations: 38
currently lose_sum: 97.80271011590958
time_elpased: 2.536
batch start
#iterations: 39
currently lose_sum: 98.1435974240303
time_elpased: 2.546
start validation test
0.626391752577
0.591586912976
0.820623649274
0.687532333161
0.626050748524
64.622
batch start
#iterations: 40
currently lose_sum: 98.50577306747437
time_elpased: 2.517
batch start
#iterations: 41
currently lose_sum: 99.55935800075531
time_elpased: 2.504
batch start
#iterations: 42
currently lose_sum: 99.63405847549438
time_elpased: 2.497
batch start
#iterations: 43
currently lose_sum: 100.16930782794952
time_elpased: 2.489
batch start
#iterations: 44
currently lose_sum: 98.65046244859695
time_elpased: 2.468
batch start
#iterations: 45
currently lose_sum: 99.20307129621506
time_elpased: 2.56
batch start
#iterations: 46
currently lose_sum: 100.50599485635757
time_elpased: 2.449
batch start
#iterations: 47
currently lose_sum: 100.50405192375183
time_elpased: 2.504
batch start
#iterations: 48
currently lose_sum: 100.49840223789215
time_elpased: 2.453
batch start
#iterations: 49
currently lose_sum: 100.43788552284241
time_elpased: 2.541
batch start
#iterations: 50
currently lose_sum: 98.78329235315323
time_elpased: 2.45
batch start
#iterations: 51
currently lose_sum: 99.28736531734467
time_elpased: 2.538
batch start
#iterations: 52
currently lose_sum: 99.60972785949707
time_elpased: 2.546
batch start
#iterations: 53
currently lose_sum: 98.7516039609909
time_elpased: 2.576
batch start
#iterations: 54
currently lose_sum: 98.28090310096741
time_elpased: 2.566
batch start
#iterations: 55
currently lose_sum: 98.34434062242508
time_elpased: 2.554
batch start
#iterations: 56
currently lose_sum: 97.41127061843872
time_elpased: 2.433
batch start
#iterations: 57
currently lose_sum: 98.83405858278275
time_elpased: 2.552
batch start
#iterations: 58
currently lose_sum: 99.8476614356041
time_elpased: 2.497
batch start
#iterations: 59
currently lose_sum: 97.95923745632172
time_elpased: 2.567
start validation test
0.540515463918
0.522083493757
0.976844705156
0.680478887375
0.539749420635
66.307
batch start
#iterations: 60
currently lose_sum: 98.18230479955673
time_elpased: 2.459
batch start
#iterations: 61
currently lose_sum: 99.3196833729744
time_elpased: 2.358
batch start
#iterations: 62
currently lose_sum: 97.69503855705261
time_elpased: 2.318
batch start
#iterations: 63
currently lose_sum: 97.66135883331299
time_elpased: 2.412
batch start
#iterations: 64
currently lose_sum: 99.42419052124023
time_elpased: 2.327
batch start
#iterations: 65
currently lose_sum: 98.64543217420578
time_elpased: 2.384
batch start
#iterations: 66
currently lose_sum: 98.11048990488052
time_elpased: 2.584
batch start
#iterations: 67
currently lose_sum: 98.59628814458847
time_elpased: 2.361
batch start
#iterations: 68
currently lose_sum: 100.50741165876389
time_elpased: 2.518
batch start
#iterations: 69
currently lose_sum: 100.506174325943
time_elpased: 2.583
batch start
#iterations: 70
currently lose_sum: 100.50567483901978
time_elpased: 2.581
batch start
#iterations: 71
currently lose_sum: 100.50459688901901
time_elpased: 2.58
batch start
#iterations: 72
currently lose_sum: 100.50148314237595
time_elpased: 2.397
batch start
#iterations: 73
currently lose_sum: 100.48368817567825
time_elpased: 2.469
batch start
#iterations: 74
currently lose_sum: 99.38792753219604
time_elpased: 2.5
batch start
#iterations: 75
currently lose_sum: 99.40142947435379
time_elpased: 2.396
batch start
#iterations: 76
currently lose_sum: 100.50616294145584
time_elpased: 2.537
batch start
#iterations: 77
currently lose_sum: 100.50604975223541
time_elpased: 2.557
batch start
#iterations: 78
currently lose_sum: 100.50605112314224
time_elpased: 2.531
batch start
#iterations: 79
currently lose_sum: 100.50588446855545
time_elpased: 2.57
start validation test
0.54412371134
0.535023670063
0.686220026757
0.601262398557
0.543874239342
67.235
batch start
#iterations: 80
currently lose_sum: 100.50580608844757
time_elpased: 2.407
batch start
#iterations: 81
currently lose_sum: 100.50567263364792
time_elpased: 2.359
batch start
#iterations: 82
currently lose_sum: 100.50551956892014
time_elpased: 2.464
batch start
#iterations: 83
currently lose_sum: 100.50518661737442
time_elpased: 2.579
batch start
#iterations: 84
currently lose_sum: 100.50490260124207
time_elpased: 2.591
batch start
#iterations: 85
currently lose_sum: 100.50377506017685
time_elpased: 2.723
batch start
#iterations: 86
currently lose_sum: 100.4991665482521
time_elpased: 2.553
batch start
#iterations: 87
currently lose_sum: 100.42475682497025
time_elpased: 2.582
batch start
#iterations: 88
currently lose_sum: 99.66861176490784
time_elpased: 2.361
batch start
#iterations: 89
currently lose_sum: 99.97713381052017
time_elpased: 2.436
batch start
#iterations: 90
currently lose_sum: 100.50855052471161
time_elpased: 2.373
batch start
#iterations: 91
currently lose_sum: 100.5016416311264
time_elpased: 2.332
batch start
#iterations: 92
currently lose_sum: 100.4966231584549
time_elpased: 2.35
batch start
#iterations: 93
currently lose_sum: 100.21869677305222
time_elpased: 2.379
batch start
#iterations: 94
currently lose_sum: 100.64785939455032
time_elpased: 2.488
batch start
#iterations: 95
currently lose_sum: 100.46294265985489
time_elpased: 2.524
batch start
#iterations: 96
currently lose_sum: 100.20022684335709
time_elpased: 2.533
batch start
#iterations: 97
currently lose_sum: 99.26139843463898
time_elpased: 2.568
batch start
#iterations: 98
currently lose_sum: 100.84784573316574
time_elpased: 2.556
batch start
#iterations: 99
currently lose_sum: 100.50600200891495
time_elpased: 2.417
start validation test
0.537886597938
0.530147530468
0.680456931152
0.59597097661
0.537636293728
67.235
batch start
#iterations: 100
currently lose_sum: 100.50585770606995
time_elpased: 2.483
batch start
#iterations: 101
currently lose_sum: 100.50576448440552
time_elpased: 2.515
batch start
#iterations: 102
currently lose_sum: 100.50576537847519
time_elpased: 2.485
batch start
#iterations: 103
currently lose_sum: 100.50576561689377
time_elpased: 2.541
batch start
#iterations: 104
currently lose_sum: 100.50571703910828
time_elpased: 2.52
batch start
#iterations: 105
currently lose_sum: 100.50584304332733
time_elpased: 2.5
batch start
#iterations: 106
currently lose_sum: 100.50563979148865
time_elpased: 2.586
batch start
#iterations: 107
currently lose_sum: 100.50552535057068
time_elpased: 2.532
batch start
#iterations: 108
currently lose_sum: 100.50519043207169
time_elpased: 2.524
batch start
#iterations: 109
currently lose_sum: 100.50476628541946
time_elpased: 2.552
batch start
#iterations: 110
currently lose_sum: 100.50312113761902
time_elpased: 2.459
batch start
#iterations: 111
currently lose_sum: 100.49896794557571
time_elpased: 2.539
batch start
#iterations: 112
currently lose_sum: 100.4554163813591
time_elpased: 2.54
batch start
#iterations: 113
currently lose_sum: 99.92144584655762
time_elpased: 2.456
batch start
#iterations: 114
currently lose_sum: 100.50430047512054
time_elpased: 2.536
batch start
#iterations: 115
currently lose_sum: 100.50363993644714
time_elpased: 2.526
batch start
#iterations: 116
currently lose_sum: 100.5025646686554
time_elpased: 2.509
batch start
#iterations: 117
currently lose_sum: 100.5015629529953
time_elpased: 2.536
batch start
#iterations: 118
currently lose_sum: 100.48953646421432
time_elpased: 2.573
batch start
#iterations: 119
currently lose_sum: 101.01963299512863
time_elpased: 2.461
start validation test
0.50087628866
0.50087628866
1.0
0.667445135144
0.5
67.235
batch start
#iterations: 120
currently lose_sum: 100.50555205345154
time_elpased: 2.553
batch start
#iterations: 121
currently lose_sum: 100.50538444519043
time_elpased: 2.501
batch start
#iterations: 122
currently lose_sum: 100.50420540571213
time_elpased: 2.533
batch start
#iterations: 123
currently lose_sum: 100.50088411569595
time_elpased: 2.516
batch start
#iterations: 124
currently lose_sum: 100.48479622602463
time_elpased: 2.46
batch start
#iterations: 125
currently lose_sum: 99.99941664934158
time_elpased: 2.547
batch start
#iterations: 126
currently lose_sum: 99.14043861627579
time_elpased: 2.494
batch start
#iterations: 127
currently lose_sum: 99.64572525024414
time_elpased: 2.521
batch start
#iterations: 128
currently lose_sum: 100.50607770681381
time_elpased: 2.596
batch start
#iterations: 129
currently lose_sum: 100.50612539052963
time_elpased: 2.563
batch start
#iterations: 130
currently lose_sum: 100.50609177350998
time_elpased: 2.545
batch start
#iterations: 131
currently lose_sum: 100.50605815649033
time_elpased: 2.686
batch start
#iterations: 132
currently lose_sum: 100.50599265098572
time_elpased: 2.602
batch start
#iterations: 133
currently lose_sum: 100.50607186555862
time_elpased: 2.569
batch start
#iterations: 134
currently lose_sum: 100.50603896379471
time_elpased: 2.572
batch start
#iterations: 135
currently lose_sum: 100.50593590736389
time_elpased: 2.59
batch start
#iterations: 136
currently lose_sum: 100.50601816177368
time_elpased: 2.573
batch start
#iterations: 137
currently lose_sum: 100.50595963001251
time_elpased: 2.462
batch start
#iterations: 138
currently lose_sum: 100.50597584247589
time_elpased: 2.588
batch start
#iterations: 139
currently lose_sum: 100.50579881668091
time_elpased: 2.589
start validation test
0.54175257732
0.531285465688
0.722651023979
0.612365919595
0.541434982195
67.235
batch start
#iterations: 140
currently lose_sum: 100.50569635629654
time_elpased: 2.6
batch start
#iterations: 141
currently lose_sum: 100.50571620464325
time_elpased: 2.573
batch start
#iterations: 142
currently lose_sum: 100.50547677278519
time_elpased: 2.479
batch start
#iterations: 143
currently lose_sum: 100.50476747751236
time_elpased: 2.529
batch start
#iterations: 144
currently lose_sum: 100.50117671489716
time_elpased: 2.56
batch start
#iterations: 145
currently lose_sum: 100.47532105445862
time_elpased: 2.548
batch start
#iterations: 146
currently lose_sum: 99.95922464132309
time_elpased: 2.579
batch start
#iterations: 147
currently lose_sum: 100.36894249916077
time_elpased: 2.561
batch start
#iterations: 148
currently lose_sum: 100.5028989315033
time_elpased: 2.512
batch start
#iterations: 149
currently lose_sum: 100.4990023970604
time_elpased: 2.58
batch start
#iterations: 150
currently lose_sum: 100.43675118684769
time_elpased: 2.563
batch start
#iterations: 151
currently lose_sum: 99.38973569869995
time_elpased: 2.619
batch start
#iterations: 152
currently lose_sum: 98.35280615091324
time_elpased: 2.613
batch start
#iterations: 153
currently lose_sum: 98.1427429318428
time_elpased: 2.576
batch start
#iterations: 154
currently lose_sum: 98.28117364645004
time_elpased: 2.479
batch start
#iterations: 155
currently lose_sum: 97.81200927495956
time_elpased: 2.401
batch start
#iterations: 156
currently lose_sum: 97.52228689193726
time_elpased: 2.414
batch start
#iterations: 157
currently lose_sum: 97.32529437541962
time_elpased: 2.453
batch start
#iterations: 158
currently lose_sum: 97.36238080263138
time_elpased: 2.497
batch start
#iterations: 159
currently lose_sum: 97.16687726974487
time_elpased: 2.555
start validation test
0.654845360825
0.65103489651
0.670062776577
0.660411806471
0.654818644304
61.523
batch start
#iterations: 160
currently lose_sum: 97.79091674089432
time_elpased: 2.615
batch start
#iterations: 161
currently lose_sum: 99.42322236299515
time_elpased: 2.579
batch start
#iterations: 162
currently lose_sum: 97.10445791482925
time_elpased: 2.565
batch start
#iterations: 163
currently lose_sum: 96.54066067934036
time_elpased: 2.498
batch start
#iterations: 164
currently lose_sum: 97.117500603199
time_elpased: 2.555
batch start
#iterations: 165
currently lose_sum: 99.10041642189026
time_elpased: 2.589
batch start
#iterations: 166
currently lose_sum: 96.89629220962524
time_elpased: 2.473
batch start
#iterations: 167
currently lose_sum: 97.61248171329498
time_elpased: 2.46
batch start
#iterations: 168
currently lose_sum: 96.28103744983673
time_elpased: 2.457
batch start
#iterations: 169
currently lose_sum: 98.29234963655472
time_elpased: 2.414
batch start
#iterations: 170
currently lose_sum: 96.57727581262589
time_elpased: 2.487
batch start
#iterations: 171
currently lose_sum: 96.57593983411789
time_elpased: 2.537
batch start
#iterations: 172
currently lose_sum: 96.09453189373016
time_elpased: 2.56
batch start
#iterations: 173
currently lose_sum: 96.58265143632889
time_elpased: 2.598
batch start
#iterations: 174
currently lose_sum: 96.23951870203018
time_elpased: 2.54
batch start
#iterations: 175
currently lose_sum: 96.63997048139572
time_elpased: 2.656
batch start
#iterations: 176
currently lose_sum: 96.19062846899033
time_elpased: 2.546
batch start
#iterations: 177
currently lose_sum: 95.96349602937698
time_elpased: 2.528
batch start
#iterations: 178
currently lose_sum: 96.85218381881714
time_elpased: 2.524
batch start
#iterations: 179
currently lose_sum: 95.80732411146164
time_elpased: 2.54
start validation test
0.592577319588
0.554756871036
0.945147679325
0.699147381242
0.591958327941
64.682
batch start
#iterations: 180
currently lose_sum: 97.3940315246582
time_elpased: 2.5
batch start
#iterations: 181
currently lose_sum: 100.50434416532516
time_elpased: 2.555
batch start
#iterations: 182
currently lose_sum: 100.4906485080719
time_elpased: 2.498
batch start
#iterations: 183
currently lose_sum: 99.07602262496948
time_elpased: 2.548
batch start
#iterations: 184
currently lose_sum: 96.90327733755112
time_elpased: 2.502
batch start
#iterations: 185
currently lose_sum: 96.68947368860245
time_elpased: 2.494
batch start
#iterations: 186
currently lose_sum: 96.26234811544418
time_elpased: 2.503
batch start
#iterations: 187
currently lose_sum: 95.3747878074646
time_elpased: 2.527
batch start
#iterations: 188
currently lose_sum: 96.59259259700775
time_elpased: 2.51
batch start
#iterations: 189
currently lose_sum: 95.85465836524963
time_elpased: 2.547
batch start
#iterations: 190
currently lose_sum: 95.92810595035553
time_elpased: 2.55
batch start
#iterations: 191
currently lose_sum: 99.84195667505264
time_elpased: 2.547
batch start
#iterations: 192
currently lose_sum: 100.45545393228531
time_elpased: 2.618
batch start
#iterations: 193
currently lose_sum: 96.68452495336533
time_elpased: 2.563
batch start
#iterations: 194
currently lose_sum: 96.48967707157135
time_elpased: 2.523
batch start
#iterations: 195
currently lose_sum: 99.9408826828003
time_elpased: 2.429
batch start
#iterations: 196
currently lose_sum: 95.54431349039078
time_elpased: 2.541
batch start
#iterations: 197
currently lose_sum: 97.70281392335892
time_elpased: 2.584
batch start
#iterations: 198
currently lose_sum: 100.50649911165237
time_elpased: 2.606
batch start
#iterations: 199
currently lose_sum: 100.50524252653122
time_elpased: 2.572
start validation test
0.575824742268
0.54887020494
0.859936194299
0.670061344774
0.575325940793
67.233
batch start
#iterations: 200
currently lose_sum: 100.50130105018616
time_elpased: 2.551
batch start
#iterations: 201
currently lose_sum: 99.92076390981674
time_elpased: 2.564
batch start
#iterations: 202
currently lose_sum: 99.1401834487915
time_elpased: 2.569
batch start
#iterations: 203
currently lose_sum: 96.88094210624695
time_elpased: 2.513
batch start
#iterations: 204
currently lose_sum: 96.01854991912842
time_elpased: 2.603
batch start
#iterations: 205
currently lose_sum: 96.06235975027084
time_elpased: 2.544
batch start
#iterations: 206
currently lose_sum: 96.64229321479797
time_elpased: 2.472
batch start
#iterations: 207
currently lose_sum: 96.5961440205574
time_elpased: 2.53
batch start
#iterations: 208
currently lose_sum: 95.68270510435104
time_elpased: 2.601
batch start
#iterations: 209
currently lose_sum: 95.70613211393356
time_elpased: 2.63
batch start
#iterations: 210
currently lose_sum: 95.60260874032974
time_elpased: 2.616
batch start
#iterations: 211
currently lose_sum: 95.20581430196762
time_elpased: 2.488
batch start
#iterations: 212
currently lose_sum: 95.70233035087585
time_elpased: 2.442
batch start
#iterations: 213
currently lose_sum: 96.54034948348999
time_elpased: 2.553
batch start
#iterations: 214
currently lose_sum: 95.7898371219635
time_elpased: 2.578
batch start
#iterations: 215
currently lose_sum: 96.11344426870346
time_elpased: 2.562
batch start
#iterations: 216
currently lose_sum: 99.30986648797989
time_elpased: 2.596
batch start
#iterations: 217
currently lose_sum: 100.50585395097733
time_elpased: 2.567
batch start
#iterations: 218
currently lose_sum: 100.5053990483284
time_elpased: 2.512
batch start
#iterations: 219
currently lose_sum: 100.50454860925674
time_elpased: 2.385
start validation test
0.594329896907
0.563309796394
0.845631367706
0.676184990125
0.593888698415
67.233
batch start
#iterations: 220
currently lose_sum: 100.50286048650742
time_elpased: 2.435
batch start
#iterations: 221
currently lose_sum: 100.48518478870392
time_elpased: 2.376
batch start
#iterations: 222
currently lose_sum: 97.4247237443924
time_elpased: 2.404
batch start
#iterations: 223
currently lose_sum: 95.93416428565979
time_elpased: 2.406
batch start
#iterations: 224
currently lose_sum: 95.89148259162903
time_elpased: 2.413
batch start
#iterations: 225
currently lose_sum: 95.42213761806488
time_elpased: 2.545
batch start
#iterations: 226
currently lose_sum: 97.12833160161972
time_elpased: 2.525
batch start
#iterations: 227
currently lose_sum: 100.50554925203323
time_elpased: 2.427
batch start
#iterations: 228
currently lose_sum: 100.47855377197266
time_elpased: 2.394
batch start
#iterations: 229
currently lose_sum: 96.21851134300232
time_elpased: 2.41
batch start
#iterations: 230
currently lose_sum: 95.92198425531387
time_elpased: 2.547
batch start
#iterations: 231
currently lose_sum: 96.41883587837219
time_elpased: 2.401
batch start
#iterations: 232
currently lose_sum: 96.09226959943771
time_elpased: 2.506
batch start
#iterations: 233
currently lose_sum: 96.19391000270844
time_elpased: 2.483
batch start
#iterations: 234
currently lose_sum: 95.73925864696503
time_elpased: 2.761
batch start
#iterations: 235
currently lose_sum: 95.70627069473267
time_elpased: 2.398
batch start
#iterations: 236
currently lose_sum: 95.28539633750916
time_elpased: 2.487
batch start
#iterations: 237
currently lose_sum: 95.76927244663239
time_elpased: 2.454
batch start
#iterations: 238
currently lose_sum: 97.8044826388359
time_elpased: 2.348
batch start
#iterations: 239
currently lose_sum: 100.50539869070053
time_elpased: 2.389
start validation test
0.52293814433
0.512360873288
0.985386436143
0.6741770815
0.522126245026
67.233
batch start
#iterations: 240
currently lose_sum: 100.50126135349274
time_elpased: 2.426
batch start
#iterations: 241
currently lose_sum: 99.25754773616791
time_elpased: 2.509
batch start
#iterations: 242
currently lose_sum: 95.35872834920883
time_elpased: 2.617
batch start
#iterations: 243
currently lose_sum: 99.83134388923645
time_elpased: 2.529
batch start
#iterations: 244
currently lose_sum: 100.50672495365143
time_elpased: 2.492
batch start
#iterations: 245
currently lose_sum: 100.50626319646835
time_elpased: 2.476
batch start
#iterations: 246
currently lose_sum: 100.50572156906128
time_elpased: 2.547
batch start
#iterations: 247
currently lose_sum: 100.50438183546066
time_elpased: 2.507
batch start
#iterations: 248
currently lose_sum: 100.49540489912033
time_elpased: 2.492
batch start
#iterations: 249
currently lose_sum: 99.01162791252136
time_elpased: 2.493
batch start
#iterations: 250
currently lose_sum: 100.38771468400955
time_elpased: 2.558
batch start
#iterations: 251
currently lose_sum: 100.50392609834671
time_elpased: 2.481
batch start
#iterations: 252
currently lose_sum: 100.48331934213638
time_elpased: 2.495
batch start
#iterations: 253
currently lose_sum: 97.8229838013649
time_elpased: 2.463
batch start
#iterations: 254
currently lose_sum: 99.42764008045197
time_elpased: 2.39
batch start
#iterations: 255
currently lose_sum: 100.5063219666481
time_elpased: 2.46
batch start
#iterations: 256
currently lose_sum: 100.50632989406586
time_elpased: 2.446
batch start
#iterations: 257
currently lose_sum: 100.50632208585739
time_elpased: 2.354
batch start
#iterations: 258
currently lose_sum: 100.50632530450821
time_elpased: 2.467
batch start
#iterations: 259
currently lose_sum: 100.50631731748581
time_elpased: 2.607
start validation test
0.565257731959
0.548128141646
0.751878151693
0.634036275276
0.564930091028
67.235
batch start
#iterations: 260
currently lose_sum: 100.50631231069565
time_elpased: 2.459
batch start
#iterations: 261
currently lose_sum: 100.50630563497543
time_elpased: 2.556
batch start
#iterations: 262
currently lose_sum: 100.50629836320877
time_elpased: 2.583
batch start
#iterations: 263
currently lose_sum: 100.50629705190659
time_elpased: 2.539
batch start
#iterations: 264
currently lose_sum: 100.50609630346298
time_elpased: 2.577
batch start
#iterations: 265
currently lose_sum: 100.5062803030014
time_elpased: 2.564
batch start
#iterations: 266
currently lose_sum: 100.50628381967545
time_elpased: 2.584
batch start
#iterations: 267
currently lose_sum: 100.50626057386398
time_elpased: 2.513
batch start
#iterations: 268
currently lose_sum: 100.5062546133995
time_elpased: 2.489
batch start
#iterations: 269
currently lose_sum: 100.50624668598175
time_elpased: 2.291
batch start
#iterations: 270
currently lose_sum: 100.506243288517
time_elpased: 2.428
batch start
#iterations: 271
currently lose_sum: 100.50620830059052
time_elpased: 2.388
batch start
#iterations: 272
currently lose_sum: 100.50618404150009
time_elpased: 2.453
batch start
#iterations: 273
currently lose_sum: 100.50617825984955
time_elpased: 2.579
batch start
#iterations: 274
currently lose_sum: 100.50612026453018
time_elpased: 2.64
batch start
#iterations: 275
currently lose_sum: 100.50605475902557
time_elpased: 2.592
batch start
#iterations: 276
currently lose_sum: 100.50594943761826
time_elpased: 2.597
batch start
#iterations: 277
currently lose_sum: 100.50571203231812
time_elpased: 2.545
batch start
#iterations: 278
currently lose_sum: 100.50499314069748
time_elpased: 2.563
batch start
#iterations: 279
currently lose_sum: 100.48850786685944
time_elpased: 2.556
start validation test
0.622164948454
0.615885037382
0.65277348976
0.633792965627
0.622111210438
67.068
batch start
#iterations: 280
currently lose_sum: 100.61511480808258
time_elpased: 2.563
batch start
#iterations: 281
currently lose_sum: 100.50621616840363
time_elpased: 2.576
batch start
#iterations: 282
currently lose_sum: 100.50621610879898
time_elpased: 2.583
batch start
#iterations: 283
currently lose_sum: 100.50620770454407
time_elpased: 2.553
batch start
#iterations: 284
currently lose_sum: 100.5061764717102
time_elpased: 2.618
batch start
#iterations: 285
currently lose_sum: 100.50615376234055
time_elpased: 2.589
batch start
#iterations: 286
currently lose_sum: 100.50613290071487
time_elpased: 2.634
batch start
#iterations: 287
currently lose_sum: 100.50611561536789
time_elpased: 2.594
batch start
#iterations: 288
currently lose_sum: 100.50604248046875
time_elpased: 2.822
batch start
#iterations: 289
currently lose_sum: 100.50599783658981
time_elpased: 2.888
batch start
#iterations: 290
currently lose_sum: 100.50591325759888
time_elpased: 2.874
batch start
#iterations: 291
currently lose_sum: 100.50575125217438
time_elpased: 2.774
batch start
#iterations: 292
currently lose_sum: 100.5055484175682
time_elpased: 2.983
batch start
#iterations: 293
currently lose_sum: 100.50444656610489
time_elpased: 3.356
batch start
#iterations: 294
currently lose_sum: 100.32453048229218
time_elpased: 2.537
batch start
#iterations: 295
currently lose_sum: 100.53071612119675
time_elpased: 2.479
batch start
#iterations: 296
currently lose_sum: 100.08697992563248
time_elpased: 2.441
batch start
#iterations: 297
currently lose_sum: 99.9319953918457
time_elpased: 2.383
batch start
#iterations: 298
currently lose_sum: 99.88829946517944
time_elpased: 2.338
batch start
#iterations: 299
currently lose_sum: 100.5041828751564
time_elpased: 2.426
start validation test
0.610927835052
0.610651974288
0.615930842853
0.613280049185
0.6109190515
67.229
batch start
#iterations: 300
currently lose_sum: 100.47280216217041
time_elpased: 2.533
batch start
#iterations: 301
currently lose_sum: 98.94243121147156
time_elpased: 2.443
batch start
#iterations: 302
currently lose_sum: 96.83901995420456
time_elpased: 2.573
batch start
#iterations: 303
currently lose_sum: 96.87747651338577
time_elpased: 2.617
batch start
#iterations: 304
currently lose_sum: 95.34462660551071
time_elpased: 2.562
batch start
#iterations: 305
currently lose_sum: 97.4874576330185
time_elpased: 2.483
batch start
#iterations: 306
currently lose_sum: 96.34925639629364
time_elpased: 2.536
batch start
#iterations: 307
currently lose_sum: 100.6592869758606
time_elpased: 2.4
batch start
#iterations: 308
currently lose_sum: 100.50614821910858
time_elpased: 2.465
batch start
#iterations: 309
currently lose_sum: 100.5061526298523
time_elpased: 2.444
batch start
#iterations: 310
currently lose_sum: 100.50610786676407
time_elpased: 2.446
batch start
#iterations: 311
currently lose_sum: 100.50610935688019
time_elpased: 2.368
batch start
#iterations: 312
currently lose_sum: 100.50609713792801
time_elpased: 2.431
batch start
#iterations: 313
currently lose_sum: 100.50609689950943
time_elpased: 2.451
batch start
#iterations: 314
currently lose_sum: 100.50605648756027
time_elpased: 2.385
batch start
#iterations: 315
currently lose_sum: 100.50605398416519
time_elpased: 2.41
batch start
#iterations: 316
currently lose_sum: 100.50602144002914
time_elpased: 2.441
batch start
#iterations: 317
currently lose_sum: 100.50596243143082
time_elpased: 2.468
batch start
#iterations: 318
currently lose_sum: 100.5059604048729
time_elpased: 2.449
batch start
#iterations: 319
currently lose_sum: 100.50586748123169
time_elpased: 2.398
start validation test
0.593659793814
0.577936426993
0.699804466399
0.633058697575
0.59347344047
67.235
batch start
#iterations: 320
currently lose_sum: 100.50579726696014
time_elpased: 2.431
batch start
#iterations: 321
currently lose_sum: 100.50556188821793
time_elpased: 2.427
batch start
#iterations: 322
currently lose_sum: 100.50531089305878
time_elpased: 2.579
batch start
#iterations: 323
currently lose_sum: 100.50439375638962
time_elpased: 2.546
batch start
#iterations: 324
currently lose_sum: 100.50088036060333
time_elpased: 2.494
batch start
#iterations: 325
currently lose_sum: 99.21746325492859
time_elpased: 2.461
batch start
#iterations: 326
currently lose_sum: 100.53389883041382
time_elpased: 2.594
batch start
#iterations: 327
currently lose_sum: 100.50612586736679
time_elpased: 2.341
batch start
#iterations: 328
currently lose_sum: 100.5060618519783
time_elpased: 2.279
batch start
#iterations: 329
currently lose_sum: 100.50601375102997
time_elpased: 2.311
batch start
#iterations: 330
currently lose_sum: 100.50607311725616
time_elpased: 2.22
batch start
#iterations: 331
currently lose_sum: 100.50603258609772
time_elpased: 2.132
batch start
#iterations: 332
currently lose_sum: 100.50599551200867
time_elpased: 2.196
batch start
#iterations: 333
currently lose_sum: 100.50594562292099
time_elpased: 2.316
batch start
#iterations: 334
currently lose_sum: 100.50593137741089
time_elpased: 2.287
batch start
#iterations: 335
currently lose_sum: 100.50592231750488
time_elpased: 2.283
batch start
#iterations: 336
currently lose_sum: 100.50583863258362
time_elpased: 2.214
batch start
#iterations: 337
currently lose_sum: 100.50582069158554
time_elpased: 2.267
batch start
#iterations: 338
currently lose_sum: 100.50547093153
time_elpased: 2.329
batch start
#iterations: 339
currently lose_sum: 100.5049198269844
time_elpased: 2.279
start validation test
0.564793814433
0.54678319624
0.766182978285
0.6381519736
0.564440244694
67.233
batch start
#iterations: 340
currently lose_sum: 100.50178980827332
time_elpased: 2.308
batch start
#iterations: 341
currently lose_sum: 100.94196075201035
time_elpased: 2.144
batch start
#iterations: 342
currently lose_sum: 100.50683677196503
time_elpased: 2.282
batch start
#iterations: 343
currently lose_sum: 100.50634491443634
time_elpased: 2.365
batch start
#iterations: 344
currently lose_sum: 100.50634092092514
time_elpased: 2.361
batch start
#iterations: 345
currently lose_sum: 100.50634557008743
time_elpased: 2.373
batch start
#iterations: 346
currently lose_sum: 100.50634610652924
time_elpased: 2.371
batch start
#iterations: 347
currently lose_sum: 100.50634652376175
time_elpased: 2.319
batch start
#iterations: 348
currently lose_sum: 100.50634568929672
time_elpased: 2.297
batch start
#iterations: 349
currently lose_sum: 100.50634545087814
time_elpased: 2.366
batch start
#iterations: 350
currently lose_sum: 100.5063436627388
time_elpased: 2.331
batch start
#iterations: 351
currently lose_sum: 100.50634044408798
time_elpased: 2.315
batch start
#iterations: 352
currently lose_sum: 100.50633418560028
time_elpased: 2.23
batch start
#iterations: 353
currently lose_sum: 100.50633895397186
time_elpased: 2.192
batch start
#iterations: 354
currently lose_sum: 100.50633889436722
time_elpased: 2.297
batch start
#iterations: 355
currently lose_sum: 100.50633758306503
time_elpased: 2.315
batch start
#iterations: 356
currently lose_sum: 100.50633716583252
time_elpased: 2.326
batch start
#iterations: 357
currently lose_sum: 100.50633317232132
time_elpased: 2.283
batch start
#iterations: 358
currently lose_sum: 100.5063362121582
time_elpased: 2.271
batch start
#iterations: 359
currently lose_sum: 100.50633347034454
time_elpased: 2.272
start validation test
0.51
0.505581715253
0.983431100134
0.667831434761
0.509168818682
67.235
batch start
#iterations: 360
currently lose_sum: 100.50633758306503
time_elpased: 2.39
batch start
#iterations: 361
currently lose_sum: 100.50633406639099
time_elpased: 2.266
batch start
#iterations: 362
currently lose_sum: 100.5063351392746
time_elpased: 2.315
batch start
#iterations: 363
currently lose_sum: 100.50633633136749
time_elpased: 2.29
batch start
#iterations: 364
currently lose_sum: 100.50633335113525
time_elpased: 2.282
batch start
#iterations: 365
currently lose_sum: 100.50633376836777
time_elpased: 2.197
batch start
#iterations: 366
currently lose_sum: 100.50633376836777
time_elpased: 2.188
batch start
#iterations: 367
currently lose_sum: 100.50633269548416
time_elpased: 2.233
batch start
#iterations: 368
currently lose_sum: 100.50633001327515
time_elpased: 2.239
batch start
#iterations: 369
currently lose_sum: 100.50633251667023
time_elpased: 2.263
batch start
#iterations: 370
currently lose_sum: 100.50633096694946
time_elpased: 2.173
batch start
#iterations: 371
currently lose_sum: 100.50633037090302
time_elpased: 2.252
batch start
#iterations: 372
currently lose_sum: 100.50632959604263
time_elpased: 2.301
batch start
#iterations: 373
currently lose_sum: 100.50632858276367
time_elpased: 2.322
batch start
#iterations: 374
currently lose_sum: 100.50633227825165
time_elpased: 2.272
batch start
#iterations: 375
currently lose_sum: 100.50632745027542
time_elpased: 2.311
batch start
#iterations: 376
currently lose_sum: 100.50632965564728
time_elpased: 2.267
batch start
#iterations: 377
currently lose_sum: 100.50632774829865
time_elpased: 2.243
batch start
#iterations: 378
currently lose_sum: 100.50632709264755
time_elpased: 2.257
batch start
#iterations: 379
currently lose_sum: 100.50631952285767
time_elpased: 2.192
start validation test
0.513195876289
0.507571135393
0.941751569414
0.659626612845
0.512443480669
67.235
batch start
#iterations: 380
currently lose_sum: 100.50632607936859
time_elpased: 2.222
batch start
#iterations: 381
currently lose_sum: 100.5063236951828
time_elpased: 2.2
batch start
#iterations: 382
currently lose_sum: 100.50632882118225
time_elpased: 2.219
batch start
#iterations: 383
currently lose_sum: 100.50632870197296
time_elpased: 2.269
batch start
#iterations: 384
currently lose_sum: 100.50632727146149
time_elpased: 2.269
batch start
#iterations: 385
currently lose_sum: 100.50631922483444
time_elpased: 2.281
batch start
#iterations: 386
currently lose_sum: 100.50631469488144
time_elpased: 2.314
batch start
#iterations: 387
currently lose_sum: 100.50631880760193
time_elpased: 2.164
batch start
#iterations: 388
currently lose_sum: 100.50631844997406
time_elpased: 2.121
batch start
#iterations: 389
currently lose_sum: 100.50631338357925
time_elpased: 2.134
batch start
#iterations: 390
currently lose_sum: 100.5063191652298
time_elpased: 2.273
batch start
#iterations: 391
currently lose_sum: 100.5063089132309
time_elpased: 2.338
batch start
#iterations: 392
currently lose_sum: 100.50630009174347
time_elpased: 2.231
batch start
#iterations: 393
currently lose_sum: 100.50634235143661
time_elpased: 2.363
batch start
#iterations: 394
currently lose_sum: 100.50631588697433
time_elpased: 2.256
batch start
#iterations: 395
currently lose_sum: 100.50633358955383
time_elpased: 2.279
batch start
#iterations: 396
currently lose_sum: 100.50634860992432
time_elpased: 2.252
batch start
#iterations: 397
currently lose_sum: 100.50635224580765
time_elpased: 2.2
batch start
#iterations: 398
currently lose_sum: 100.50635004043579
time_elpased: 2.183
batch start
#iterations: 399
currently lose_sum: 100.50630950927734
time_elpased: 2.28
start validation test
0.502525773196
0.501705602646
0.998970875785
0.667951143988
0.501654187247
67.235
acc: 0.644
pre: 0.640
rec: 0.659
F1: 0.649
auc: 0.644
