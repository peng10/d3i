start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 101.47571861743927
time_elpased: 1.33
batch start
#iterations: 1
currently lose_sum: 100.39375454187393
time_elpased: 1.327
batch start
#iterations: 2
currently lose_sum: 100.32064235210419
time_elpased: 1.338
batch start
#iterations: 3
currently lose_sum: 100.22648757696152
time_elpased: 1.317
batch start
#iterations: 4
currently lose_sum: 100.28002673387527
time_elpased: 1.347
batch start
#iterations: 5
currently lose_sum: 100.18482309579849
time_elpased: 1.327
batch start
#iterations: 6
currently lose_sum: 100.02659225463867
time_elpased: 1.374
batch start
#iterations: 7
currently lose_sum: 99.91738474369049
time_elpased: 1.322
batch start
#iterations: 8
currently lose_sum: 99.94933182001114
time_elpased: 1.353
batch start
#iterations: 9
currently lose_sum: 99.93739360570908
time_elpased: 1.345
batch start
#iterations: 10
currently lose_sum: 99.80358880758286
time_elpased: 1.336
batch start
#iterations: 11
currently lose_sum: 99.72141492366791
time_elpased: 1.383
batch start
#iterations: 12
currently lose_sum: 99.62421530485153
time_elpased: 1.345
batch start
#iterations: 13
currently lose_sum: 99.63793164491653
time_elpased: 1.323
batch start
#iterations: 14
currently lose_sum: 99.46458965539932
time_elpased: 1.325
batch start
#iterations: 15
currently lose_sum: 99.39815086126328
time_elpased: 1.321
batch start
#iterations: 16
currently lose_sum: 99.45940124988556
time_elpased: 1.32
batch start
#iterations: 17
currently lose_sum: 99.18850368261337
time_elpased: 1.329
batch start
#iterations: 18
currently lose_sum: 98.9604942202568
time_elpased: 1.332
batch start
#iterations: 19
currently lose_sum: 99.1198633313179
time_elpased: 1.33
start validation test
0.566237113402
0.655667144907
0.282185859833
0.394560759767
0.56673580919
65.614
batch start
#iterations: 20
currently lose_sum: 99.12332540750504
time_elpased: 1.348
batch start
#iterations: 21
currently lose_sum: 98.73127216100693
time_elpased: 1.373
batch start
#iterations: 22
currently lose_sum: 98.624440908432
time_elpased: 1.323
batch start
#iterations: 23
currently lose_sum: 99.03443491458893
time_elpased: 1.323
batch start
#iterations: 24
currently lose_sum: 98.77953344583511
time_elpased: 1.344
batch start
#iterations: 25
currently lose_sum: 98.37867629528046
time_elpased: 1.365
batch start
#iterations: 26
currently lose_sum: 98.83058851957321
time_elpased: 1.35
batch start
#iterations: 27
currently lose_sum: 98.12191343307495
time_elpased: 1.338
batch start
#iterations: 28
currently lose_sum: 98.53740620613098
time_elpased: 1.334
batch start
#iterations: 29
currently lose_sum: 98.23457515239716
time_elpased: 1.328
batch start
#iterations: 30
currently lose_sum: 98.29966455698013
time_elpased: 1.321
batch start
#iterations: 31
currently lose_sum: 98.16095739603043
time_elpased: 1.318
batch start
#iterations: 32
currently lose_sum: 98.07948386669159
time_elpased: 1.324
batch start
#iterations: 33
currently lose_sum: 98.17876917123795
time_elpased: 1.36
batch start
#iterations: 34
currently lose_sum: 98.14806056022644
time_elpased: 1.327
batch start
#iterations: 35
currently lose_sum: 97.8968141078949
time_elpased: 1.39
batch start
#iterations: 36
currently lose_sum: 97.77129691839218
time_elpased: 1.368
batch start
#iterations: 37
currently lose_sum: 97.83200681209564
time_elpased: 1.342
batch start
#iterations: 38
currently lose_sum: 97.58062726259232
time_elpased: 1.334
batch start
#iterations: 39
currently lose_sum: 97.32991194725037
time_elpased: 1.335
start validation test
0.634896907216
0.606089898502
0.774313059586
0.679951199675
0.634652140658
63.456
batch start
#iterations: 40
currently lose_sum: 97.81693243980408
time_elpased: 1.309
batch start
#iterations: 41
currently lose_sum: 97.42323851585388
time_elpased: 1.338
batch start
#iterations: 42
currently lose_sum: 97.39553225040436
time_elpased: 1.323
batch start
#iterations: 43
currently lose_sum: 97.74777448177338
time_elpased: 1.371
batch start
#iterations: 44
currently lose_sum: 97.3175607919693
time_elpased: 1.335
batch start
#iterations: 45
currently lose_sum: 97.5588926076889
time_elpased: 1.329
batch start
#iterations: 46
currently lose_sum: 97.43665355443954
time_elpased: 1.343
batch start
#iterations: 47
currently lose_sum: 97.272385597229
time_elpased: 1.365
batch start
#iterations: 48
currently lose_sum: 97.1283488869667
time_elpased: 1.31
batch start
#iterations: 49
currently lose_sum: 97.21209079027176
time_elpased: 1.326
batch start
#iterations: 50
currently lose_sum: 96.99015843868256
time_elpased: 1.371
batch start
#iterations: 51
currently lose_sum: 97.09654211997986
time_elpased: 1.332
batch start
#iterations: 52
currently lose_sum: 97.24487912654877
time_elpased: 1.337
batch start
#iterations: 53
currently lose_sum: 97.0378195643425
time_elpased: 1.332
batch start
#iterations: 54
currently lose_sum: 97.01478707790375
time_elpased: 1.33
batch start
#iterations: 55
currently lose_sum: 96.97875672578812
time_elpased: 1.312
batch start
#iterations: 56
currently lose_sum: 96.89496272802353
time_elpased: 1.334
batch start
#iterations: 57
currently lose_sum: 96.9755374789238
time_elpased: 1.303
batch start
#iterations: 58
currently lose_sum: 96.57486081123352
time_elpased: 1.34
batch start
#iterations: 59
currently lose_sum: 96.88817894458771
time_elpased: 1.359
start validation test
0.623092783505
0.659166115156
0.512503859216
0.576655859194
0.623286939419
62.894
batch start
#iterations: 60
currently lose_sum: 97.41616946458817
time_elpased: 1.332
batch start
#iterations: 61
currently lose_sum: 97.26545667648315
time_elpased: 1.316
batch start
#iterations: 62
currently lose_sum: 96.76149034500122
time_elpased: 1.359
batch start
#iterations: 63
currently lose_sum: 97.04777193069458
time_elpased: 1.332
batch start
#iterations: 64
currently lose_sum: 96.97562485933304
time_elpased: 1.331
batch start
#iterations: 65
currently lose_sum: 96.71893674135208
time_elpased: 1.348
batch start
#iterations: 66
currently lose_sum: 96.5682561993599
time_elpased: 1.319
batch start
#iterations: 67
currently lose_sum: 96.43524670600891
time_elpased: 1.319
batch start
#iterations: 68
currently lose_sum: 96.5677752494812
time_elpased: 1.337
batch start
#iterations: 69
currently lose_sum: 96.98740297555923
time_elpased: 1.351
batch start
#iterations: 70
currently lose_sum: 96.62141162157059
time_elpased: 1.372
batch start
#iterations: 71
currently lose_sum: 96.71336770057678
time_elpased: 1.336
batch start
#iterations: 72
currently lose_sum: 96.80845123529434
time_elpased: 1.337
batch start
#iterations: 73
currently lose_sum: 96.65499866008759
time_elpased: 1.335
batch start
#iterations: 74
currently lose_sum: 96.21255522966385
time_elpased: 1.354
batch start
#iterations: 75
currently lose_sum: 96.73277801275253
time_elpased: 1.34
batch start
#iterations: 76
currently lose_sum: 96.41288542747498
time_elpased: 1.328
batch start
#iterations: 77
currently lose_sum: 96.69052523374557
time_elpased: 1.339
batch start
#iterations: 78
currently lose_sum: 96.58809614181519
time_elpased: 1.334
batch start
#iterations: 79
currently lose_sum: 96.7435958981514
time_elpased: 1.353
start validation test
0.609484536082
0.676912906958
0.421529278584
0.519533231862
0.609814520527
62.775
batch start
#iterations: 80
currently lose_sum: 96.27667862176895
time_elpased: 1.386
batch start
#iterations: 81
currently lose_sum: 96.45532500743866
time_elpased: 1.341
batch start
#iterations: 82
currently lose_sum: 96.25181823968887
time_elpased: 1.343
batch start
#iterations: 83
currently lose_sum: 96.17481750249863
time_elpased: 1.314
batch start
#iterations: 84
currently lose_sum: 96.48613607883453
time_elpased: 1.345
batch start
#iterations: 85
currently lose_sum: 96.26738828420639
time_elpased: 1.4
batch start
#iterations: 86
currently lose_sum: 96.50363790988922
time_elpased: 1.396
batch start
#iterations: 87
currently lose_sum: 96.09626239538193
time_elpased: 1.395
batch start
#iterations: 88
currently lose_sum: 96.0896908044815
time_elpased: 1.329
batch start
#iterations: 89
currently lose_sum: 96.20484775304794
time_elpased: 1.34
batch start
#iterations: 90
currently lose_sum: 95.80064505338669
time_elpased: 1.325
batch start
#iterations: 91
currently lose_sum: 96.3741802573204
time_elpased: 1.357
batch start
#iterations: 92
currently lose_sum: 95.98090034723282
time_elpased: 1.336
batch start
#iterations: 93
currently lose_sum: 95.87062919139862
time_elpased: 1.377
batch start
#iterations: 94
currently lose_sum: 96.24274104833603
time_elpased: 1.347
batch start
#iterations: 95
currently lose_sum: 96.47164940834045
time_elpased: 1.328
batch start
#iterations: 96
currently lose_sum: 96.1841332912445
time_elpased: 1.343
batch start
#iterations: 97
currently lose_sum: 96.18425714969635
time_elpased: 1.349
batch start
#iterations: 98
currently lose_sum: 96.02331870794296
time_elpased: 1.349
batch start
#iterations: 99
currently lose_sum: 96.05224013328552
time_elpased: 1.383
start validation test
0.605721649485
0.67663136317
0.407636101677
0.50876629632
0.606069419216
62.696
batch start
#iterations: 100
currently lose_sum: 95.8920875787735
time_elpased: 1.367
batch start
#iterations: 101
currently lose_sum: 96.08841246366501
time_elpased: 1.434
batch start
#iterations: 102
currently lose_sum: 95.81676614284515
time_elpased: 1.452
batch start
#iterations: 103
currently lose_sum: 95.94677644968033
time_elpased: 1.471
batch start
#iterations: 104
currently lose_sum: 95.42548161745071
time_elpased: 1.748
batch start
#iterations: 105
currently lose_sum: 95.88220679759979
time_elpased: 1.768
batch start
#iterations: 106
currently lose_sum: 96.17249947786331
time_elpased: 1.768
batch start
#iterations: 107
currently lose_sum: 95.6968412399292
time_elpased: 1.397
batch start
#iterations: 108
currently lose_sum: 95.70264267921448
time_elpased: 1.383
batch start
#iterations: 109
currently lose_sum: 95.53437316417694
time_elpased: 1.392
batch start
#iterations: 110
currently lose_sum: 95.9768174290657
time_elpased: 1.386
batch start
#iterations: 111
currently lose_sum: 96.12698638439178
time_elpased: 1.378
batch start
#iterations: 112
currently lose_sum: 96.01006227731705
time_elpased: 1.345
batch start
#iterations: 113
currently lose_sum: 95.26219248771667
time_elpased: 1.35
batch start
#iterations: 114
currently lose_sum: 95.48542249202728
time_elpased: 1.362
batch start
#iterations: 115
currently lose_sum: 95.65315246582031
time_elpased: 1.589
batch start
#iterations: 116
currently lose_sum: 95.64469069242477
time_elpased: 1.604
batch start
#iterations: 117
currently lose_sum: 95.5782328248024
time_elpased: 2.055
batch start
#iterations: 118
currently lose_sum: 95.3927326798439
time_elpased: 1.777
batch start
#iterations: 119
currently lose_sum: 95.473688185215
time_elpased: 1.416
start validation test
0.641907216495
0.655583015053
0.600596892045
0.626886513776
0.641979743141
61.533
batch start
#iterations: 120
currently lose_sum: 95.27230995893478
time_elpased: 1.344
batch start
#iterations: 121
currently lose_sum: 96.03017365932465
time_elpased: 1.378
batch start
#iterations: 122
currently lose_sum: 95.28413897752762
time_elpased: 1.36
batch start
#iterations: 123
currently lose_sum: 95.530308842659
time_elpased: 1.361
batch start
#iterations: 124
currently lose_sum: 95.35114753246307
time_elpased: 1.339
batch start
#iterations: 125
currently lose_sum: 95.27237850427628
time_elpased: 1.412
batch start
#iterations: 126
currently lose_sum: 95.46714234352112
time_elpased: 1.356
batch start
#iterations: 127
currently lose_sum: 95.73703932762146
time_elpased: 1.334
batch start
#iterations: 128
currently lose_sum: 95.39563375711441
time_elpased: 1.623
batch start
#iterations: 129
currently lose_sum: 95.45623403787613
time_elpased: 1.379
batch start
#iterations: 130
currently lose_sum: 95.5359097123146
time_elpased: 1.336
batch start
#iterations: 131
currently lose_sum: 95.39757388830185
time_elpased: 1.336
batch start
#iterations: 132
currently lose_sum: 95.51317775249481
time_elpased: 1.362
batch start
#iterations: 133
currently lose_sum: 95.33108711242676
time_elpased: 1.499
batch start
#iterations: 134
currently lose_sum: 95.09705525636673
time_elpased: 1.63
batch start
#iterations: 135
currently lose_sum: 95.26445591449738
time_elpased: 1.466
batch start
#iterations: 136
currently lose_sum: 95.52987551689148
time_elpased: 1.537
batch start
#iterations: 137
currently lose_sum: 94.92071241140366
time_elpased: 1.416
batch start
#iterations: 138
currently lose_sum: 95.71975934505463
time_elpased: 1.343
batch start
#iterations: 139
currently lose_sum: 95.1305148601532
time_elpased: 1.371
start validation test
0.658195876289
0.646812559467
0.699598641556
0.672170860731
0.658123187348
61.327
batch start
#iterations: 140
currently lose_sum: 95.03759062290192
time_elpased: 1.353
batch start
#iterations: 141
currently lose_sum: 95.07857573032379
time_elpased: 1.334
batch start
#iterations: 142
currently lose_sum: 95.73685616254807
time_elpased: 1.367
batch start
#iterations: 143
currently lose_sum: 94.70382046699524
time_elpased: 1.334
batch start
#iterations: 144
currently lose_sum: 95.36053204536438
time_elpased: 1.393
batch start
#iterations: 145
currently lose_sum: 95.23183000087738
time_elpased: 1.364
batch start
#iterations: 146
currently lose_sum: 95.11731553077698
time_elpased: 1.336
batch start
#iterations: 147
currently lose_sum: 94.94330310821533
time_elpased: 1.364
batch start
#iterations: 148
currently lose_sum: 95.32771909236908
time_elpased: 1.353
batch start
#iterations: 149
currently lose_sum: 95.07935482263565
time_elpased: 1.34
batch start
#iterations: 150
currently lose_sum: 94.9577094912529
time_elpased: 1.435
batch start
#iterations: 151
currently lose_sum: 95.48599624633789
time_elpased: 1.388
batch start
#iterations: 152
currently lose_sum: 95.28255051374435
time_elpased: 1.401
batch start
#iterations: 153
currently lose_sum: 95.11325973272324
time_elpased: 1.355
batch start
#iterations: 154
currently lose_sum: 94.80058020353317
time_elpased: 1.352
batch start
#iterations: 155
currently lose_sum: 94.81937456130981
time_elpased: 1.335
batch start
#iterations: 156
currently lose_sum: 95.07637876272202
time_elpased: 1.338
batch start
#iterations: 157
currently lose_sum: 94.79639863967896
time_elpased: 1.365
batch start
#iterations: 158
currently lose_sum: 95.33793634176254
time_elpased: 1.397
batch start
#iterations: 159
currently lose_sum: 95.01855999231339
time_elpased: 1.363
start validation test
0.622886597938
0.683310429073
0.460533086344
0.550227468339
0.623171634569
61.956
batch start
#iterations: 160
currently lose_sum: 94.69922351837158
time_elpased: 1.338
batch start
#iterations: 161
currently lose_sum: 94.90190923213959
time_elpased: 1.344
batch start
#iterations: 162
currently lose_sum: 94.65065556764603
time_elpased: 1.335
batch start
#iterations: 163
currently lose_sum: 94.39566338062286
time_elpased: 1.362
batch start
#iterations: 164
currently lose_sum: 94.76495319604874
time_elpased: 1.383
batch start
#iterations: 165
currently lose_sum: 94.46246856451035
time_elpased: 1.337
batch start
#iterations: 166
currently lose_sum: 94.75637829303741
time_elpased: 1.329
batch start
#iterations: 167
currently lose_sum: 95.16470408439636
time_elpased: 1.356
batch start
#iterations: 168
currently lose_sum: 94.95060682296753
time_elpased: 1.349
batch start
#iterations: 169
currently lose_sum: 94.82077538967133
time_elpased: 1.336
batch start
#iterations: 170
currently lose_sum: 94.04403132200241
time_elpased: 1.338
batch start
#iterations: 171
currently lose_sum: 94.92037951946259
time_elpased: 1.377
batch start
#iterations: 172
currently lose_sum: 94.52128756046295
time_elpased: 1.335
batch start
#iterations: 173
currently lose_sum: 94.4777802824974
time_elpased: 1.367
batch start
#iterations: 174
currently lose_sum: 94.38929200172424
time_elpased: 1.385
batch start
#iterations: 175
currently lose_sum: 94.56335383653641
time_elpased: 1.345
batch start
#iterations: 176
currently lose_sum: 94.62706285715103
time_elpased: 1.386
batch start
#iterations: 177
currently lose_sum: 95.06358754634857
time_elpased: 1.328
batch start
#iterations: 178
currently lose_sum: 94.55521273612976
time_elpased: 1.34
batch start
#iterations: 179
currently lose_sum: 94.44489639997482
time_elpased: 1.349
start validation test
0.647422680412
0.6645316253
0.597921169085
0.629469122427
0.647509587951
61.215
batch start
#iterations: 180
currently lose_sum: 94.53273391723633
time_elpased: 1.379
batch start
#iterations: 181
currently lose_sum: 94.8040634393692
time_elpased: 1.368
batch start
#iterations: 182
currently lose_sum: 94.45115518569946
time_elpased: 1.36
batch start
#iterations: 183
currently lose_sum: 94.66002810001373
time_elpased: 1.346
batch start
#iterations: 184
currently lose_sum: 94.55762827396393
time_elpased: 1.354
batch start
#iterations: 185
currently lose_sum: 94.173120200634
time_elpased: 1.553
batch start
#iterations: 186
currently lose_sum: 94.21914780139923
time_elpased: 1.446
batch start
#iterations: 187
currently lose_sum: 94.59783345460892
time_elpased: 1.732
batch start
#iterations: 188
currently lose_sum: 94.2962549328804
time_elpased: 1.508
batch start
#iterations: 189
currently lose_sum: 94.48566627502441
time_elpased: 1.338
batch start
#iterations: 190
currently lose_sum: 94.42374509572983
time_elpased: 1.377
batch start
#iterations: 191
currently lose_sum: 94.26244831085205
time_elpased: 1.449
batch start
#iterations: 192
currently lose_sum: 94.5025686621666
time_elpased: 1.388
batch start
#iterations: 193
currently lose_sum: 93.9386214017868
time_elpased: 1.525
batch start
#iterations: 194
currently lose_sum: 94.4943237900734
time_elpased: 1.473
batch start
#iterations: 195
currently lose_sum: 94.45097750425339
time_elpased: 1.403
batch start
#iterations: 196
currently lose_sum: 93.85556071996689
time_elpased: 1.364
batch start
#iterations: 197
currently lose_sum: 94.76285457611084
time_elpased: 1.357
batch start
#iterations: 198
currently lose_sum: 94.2042007446289
time_elpased: 1.367
batch start
#iterations: 199
currently lose_sum: 93.98746430873871
time_elpased: 1.374
start validation test
0.634072164948
0.670620437956
0.529484408768
0.591753407326
0.634255784886
61.543
batch start
#iterations: 200
currently lose_sum: 94.34724509716034
time_elpased: 1.36
batch start
#iterations: 201
currently lose_sum: 94.69711011648178
time_elpased: 1.391
batch start
#iterations: 202
currently lose_sum: 94.3214111328125
time_elpased: 1.401
batch start
#iterations: 203
currently lose_sum: 94.20364660024643
time_elpased: 1.395
batch start
#iterations: 204
currently lose_sum: 94.41526365280151
time_elpased: 1.344
batch start
#iterations: 205
currently lose_sum: 94.15052437782288
time_elpased: 1.363
batch start
#iterations: 206
currently lose_sum: 93.70655167102814
time_elpased: 1.367
batch start
#iterations: 207
currently lose_sum: 94.20658111572266
time_elpased: 1.341
batch start
#iterations: 208
currently lose_sum: 93.85044461488724
time_elpased: 1.367
batch start
#iterations: 209
currently lose_sum: 93.79814368486404
time_elpased: 1.354
batch start
#iterations: 210
currently lose_sum: 94.10242426395416
time_elpased: 1.398
batch start
#iterations: 211
currently lose_sum: 93.89918774366379
time_elpased: 1.341
batch start
#iterations: 212
currently lose_sum: 94.04432970285416
time_elpased: 1.323
batch start
#iterations: 213
currently lose_sum: 93.91036945581436
time_elpased: 1.345
batch start
#iterations: 214
currently lose_sum: 94.12572318315506
time_elpased: 1.383
batch start
#iterations: 215
currently lose_sum: 93.69540125131607
time_elpased: 1.376
batch start
#iterations: 216
currently lose_sum: 93.78924435377121
time_elpased: 1.383
batch start
#iterations: 217
currently lose_sum: 93.98973661661148
time_elpased: 1.357
batch start
#iterations: 218
currently lose_sum: 93.49474012851715
time_elpased: 1.405
batch start
#iterations: 219
currently lose_sum: 94.4460819363594
time_elpased: 1.376
start validation test
0.627628865979
0.685298052624
0.47442626325
0.560690829482
0.627897836778
61.766
batch start
#iterations: 220
currently lose_sum: 94.26654303073883
time_elpased: 1.372
batch start
#iterations: 221
currently lose_sum: 93.62413954734802
time_elpased: 1.386
batch start
#iterations: 222
currently lose_sum: 93.9526720046997
time_elpased: 1.541
batch start
#iterations: 223
currently lose_sum: 94.02166038751602
time_elpased: 1.752
batch start
#iterations: 224
currently lose_sum: 94.50176799297333
time_elpased: 1.426
batch start
#iterations: 225
currently lose_sum: 93.80501109361649
time_elpased: 1.463
batch start
#iterations: 226
currently lose_sum: 93.47969609498978
time_elpased: 1.44
batch start
#iterations: 227
currently lose_sum: 93.45537793636322
time_elpased: 1.501
batch start
#iterations: 228
currently lose_sum: 94.07916551828384
time_elpased: 1.539
batch start
#iterations: 229
currently lose_sum: 93.66600745916367
time_elpased: 1.778
batch start
#iterations: 230
currently lose_sum: 93.88394683599472
time_elpased: 1.769
batch start
#iterations: 231
currently lose_sum: 94.00571978092194
time_elpased: 1.758
batch start
#iterations: 232
currently lose_sum: 93.93417519330978
time_elpased: 1.447
batch start
#iterations: 233
currently lose_sum: 93.72013264894485
time_elpased: 1.349
batch start
#iterations: 234
currently lose_sum: 93.5254698395729
time_elpased: 1.357
batch start
#iterations: 235
currently lose_sum: 93.72089916467667
time_elpased: 1.431
batch start
#iterations: 236
currently lose_sum: 93.60473650693893
time_elpased: 1.746
batch start
#iterations: 237
currently lose_sum: 93.76500791311264
time_elpased: 1.369
batch start
#iterations: 238
currently lose_sum: 93.51974952220917
time_elpased: 1.382
batch start
#iterations: 239
currently lose_sum: 93.54781430959702
time_elpased: 1.367
start validation test
0.656649484536
0.618065214032
0.823196459813
0.7060329229
0.656357085633
61.316
batch start
#iterations: 240
currently lose_sum: 94.00281691551208
time_elpased: 1.366
batch start
#iterations: 241
currently lose_sum: 94.06879127025604
time_elpased: 1.334
batch start
#iterations: 242
currently lose_sum: 93.19520288705826
time_elpased: 1.346
batch start
#iterations: 243
currently lose_sum: 93.60261803865433
time_elpased: 1.321
batch start
#iterations: 244
currently lose_sum: 94.22115164995193
time_elpased: 1.323
batch start
#iterations: 245
currently lose_sum: 94.08005768060684
time_elpased: 1.346
batch start
#iterations: 246
currently lose_sum: 94.0768935084343
time_elpased: 1.354
batch start
#iterations: 247
currently lose_sum: 93.45191740989685
time_elpased: 1.363
batch start
#iterations: 248
currently lose_sum: 93.83752018213272
time_elpased: 1.461
batch start
#iterations: 249
currently lose_sum: 93.12306433916092
time_elpased: 1.441
batch start
#iterations: 250
currently lose_sum: 93.25887632369995
time_elpased: 1.361
batch start
#iterations: 251
currently lose_sum: 93.41286206245422
time_elpased: 1.377
batch start
#iterations: 252
currently lose_sum: 93.47062647342682
time_elpased: 1.33
batch start
#iterations: 253
currently lose_sum: 93.52707731723785
time_elpased: 1.392
batch start
#iterations: 254
currently lose_sum: 94.19847345352173
time_elpased: 1.363
batch start
#iterations: 255
currently lose_sum: 93.65699774026871
time_elpased: 1.342
batch start
#iterations: 256
currently lose_sum: 92.94518387317657
time_elpased: 1.356
batch start
#iterations: 257
currently lose_sum: 93.76475143432617
time_elpased: 1.45
batch start
#iterations: 258
currently lose_sum: 92.80214029550552
time_elpased: 1.438
batch start
#iterations: 259
currently lose_sum: 93.30272799730301
time_elpased: 1.558
start validation test
0.65324742268
0.657368421053
0.64268807245
0.649945360878
0.653265961248
60.920
batch start
#iterations: 260
currently lose_sum: 93.44403541088104
time_elpased: 1.374
batch start
#iterations: 261
currently lose_sum: 93.14994663000107
time_elpased: 1.375
batch start
#iterations: 262
currently lose_sum: 93.45025956630707
time_elpased: 1.428
batch start
#iterations: 263
currently lose_sum: 93.58118081092834
time_elpased: 1.381
batch start
#iterations: 264
currently lose_sum: 93.54365748167038
time_elpased: 1.432
batch start
#iterations: 265
currently lose_sum: 93.77091002464294
time_elpased: 1.356
batch start
#iterations: 266
currently lose_sum: 93.10602134466171
time_elpased: 1.329
batch start
#iterations: 267
currently lose_sum: 93.63833028078079
time_elpased: 1.357
batch start
#iterations: 268
currently lose_sum: 93.1680708527565
time_elpased: 1.317
batch start
#iterations: 269
currently lose_sum: 94.09622752666473
time_elpased: 1.388
batch start
#iterations: 270
currently lose_sum: 93.50772541761398
time_elpased: 1.427
batch start
#iterations: 271
currently lose_sum: 92.80054920911789
time_elpased: 1.401
batch start
#iterations: 272
currently lose_sum: 93.50264948606491
time_elpased: 1.336
batch start
#iterations: 273
currently lose_sum: 93.35724627971649
time_elpased: 1.352
batch start
#iterations: 274
currently lose_sum: 93.6369246840477
time_elpased: 1.323
batch start
#iterations: 275
currently lose_sum: 93.06760954856873
time_elpased: 1.346
batch start
#iterations: 276
currently lose_sum: 93.12389940023422
time_elpased: 1.359
batch start
#iterations: 277
currently lose_sum: 93.30076736211777
time_elpased: 1.386
batch start
#iterations: 278
currently lose_sum: 93.41163277626038
time_elpased: 1.387
batch start
#iterations: 279
currently lose_sum: 93.17484247684479
time_elpased: 1.369
start validation test
0.652113402062
0.65737009544
0.63795410106
0.647516582232
0.652138260899
60.952
batch start
#iterations: 280
currently lose_sum: 93.44526767730713
time_elpased: 1.346
batch start
#iterations: 281
currently lose_sum: 93.08587568998337
time_elpased: 1.34
batch start
#iterations: 282
currently lose_sum: 92.99109548330307
time_elpased: 1.358
batch start
#iterations: 283
currently lose_sum: 93.03635853528976
time_elpased: 1.334
batch start
#iterations: 284
currently lose_sum: 92.97712379693985
time_elpased: 1.36
batch start
#iterations: 285
currently lose_sum: 92.92813104391098
time_elpased: 1.358
batch start
#iterations: 286
currently lose_sum: 92.98044538497925
time_elpased: 1.353
batch start
#iterations: 287
currently lose_sum: 92.7031324505806
time_elpased: 1.331
batch start
#iterations: 288
currently lose_sum: 92.99800503253937
time_elpased: 1.334
batch start
#iterations: 289
currently lose_sum: 93.25844717025757
time_elpased: 1.331
batch start
#iterations: 290
currently lose_sum: 92.87185549736023
time_elpased: 1.339
batch start
#iterations: 291
currently lose_sum: 93.01241791248322
time_elpased: 1.336
batch start
#iterations: 292
currently lose_sum: 92.9574436545372
time_elpased: 1.38
batch start
#iterations: 293
currently lose_sum: 92.58948904275894
time_elpased: 1.321
batch start
#iterations: 294
currently lose_sum: 92.81354928016663
time_elpased: 1.358
batch start
#iterations: 295
currently lose_sum: 92.70032823085785
time_elpased: 1.354
batch start
#iterations: 296
currently lose_sum: 92.58049368858337
time_elpased: 1.323
batch start
#iterations: 297
currently lose_sum: 93.23707515001297
time_elpased: 1.329
batch start
#iterations: 298
currently lose_sum: 92.76577246189117
time_elpased: 1.342
batch start
#iterations: 299
currently lose_sum: 92.52950650453568
time_elpased: 1.351
start validation test
0.655979381443
0.644341144104
0.698981167027
0.67054990621
0.655903885176
60.779
batch start
#iterations: 300
currently lose_sum: 92.55863898992538
time_elpased: 1.311
batch start
#iterations: 301
currently lose_sum: 92.9311763048172
time_elpased: 1.334
batch start
#iterations: 302
currently lose_sum: 92.97938168048859
time_elpased: 1.318
batch start
#iterations: 303
currently lose_sum: 92.24624359607697
time_elpased: 1.335
batch start
#iterations: 304
currently lose_sum: 92.76812702417374
time_elpased: 1.348
batch start
#iterations: 305
currently lose_sum: 92.82738703489304
time_elpased: 1.363
batch start
#iterations: 306
currently lose_sum: 93.05204963684082
time_elpased: 1.345
batch start
#iterations: 307
currently lose_sum: 92.79783421754837
time_elpased: 1.348
batch start
#iterations: 308
currently lose_sum: 92.84071403741837
time_elpased: 1.383
batch start
#iterations: 309
currently lose_sum: 92.70256006717682
time_elpased: 1.351
batch start
#iterations: 310
currently lose_sum: 92.73285275697708
time_elpased: 1.331
batch start
#iterations: 311
currently lose_sum: 92.64888966083527
time_elpased: 1.339
batch start
#iterations: 312
currently lose_sum: 92.42439877986908
time_elpased: 1.347
batch start
#iterations: 313
currently lose_sum: 92.54275184869766
time_elpased: 1.351
batch start
#iterations: 314
currently lose_sum: 92.4981420636177
time_elpased: 1.354
batch start
#iterations: 315
currently lose_sum: 92.6172805428505
time_elpased: 1.403
batch start
#iterations: 316
currently lose_sum: 92.40552747249603
time_elpased: 1.377
batch start
#iterations: 317
currently lose_sum: 93.25019782781601
time_elpased: 1.391
batch start
#iterations: 318
currently lose_sum: 92.90498667955399
time_elpased: 1.438
batch start
#iterations: 319
currently lose_sum: 92.56474983692169
time_elpased: 1.409
start validation test
0.61206185567
0.683531579829
0.419882679839
0.520209103659
0.612399255855
62.215
batch start
#iterations: 320
currently lose_sum: 92.66683512926102
time_elpased: 1.346
batch start
#iterations: 321
currently lose_sum: 92.19472295045853
time_elpased: 1.38
batch start
#iterations: 322
currently lose_sum: 93.05684357881546
time_elpased: 1.344
batch start
#iterations: 323
currently lose_sum: 92.61997020244598
time_elpased: 1.378
batch start
#iterations: 324
currently lose_sum: 92.30188810825348
time_elpased: 1.369
batch start
#iterations: 325
currently lose_sum: 92.78849214315414
time_elpased: 1.39
batch start
#iterations: 326
currently lose_sum: 92.96250534057617
time_elpased: 1.356
batch start
#iterations: 327
currently lose_sum: 92.23886102437973
time_elpased: 1.341
batch start
#iterations: 328
currently lose_sum: 92.39214247465134
time_elpased: 1.338
batch start
#iterations: 329
currently lose_sum: 92.4276642203331
time_elpased: 1.314
batch start
#iterations: 330
currently lose_sum: 92.98517107963562
time_elpased: 1.38
batch start
#iterations: 331
currently lose_sum: 93.15206092596054
time_elpased: 1.396
batch start
#iterations: 332
currently lose_sum: 92.56671625375748
time_elpased: 1.356
batch start
#iterations: 333
currently lose_sum: 92.01490586996078
time_elpased: 1.332
batch start
#iterations: 334
currently lose_sum: 92.80846446752548
time_elpased: 1.346
batch start
#iterations: 335
currently lose_sum: 92.12597620487213
time_elpased: 1.365
batch start
#iterations: 336
currently lose_sum: 92.33976757526398
time_elpased: 1.336
batch start
#iterations: 337
currently lose_sum: 92.20220309495926
time_elpased: 1.328
batch start
#iterations: 338
currently lose_sum: 92.61656260490417
time_elpased: 1.387
batch start
#iterations: 339
currently lose_sum: 92.19437140226364
time_elpased: 1.362
start validation test
0.635309278351
0.666163522013
0.545024184419
0.599535857814
0.635467787758
61.509
batch start
#iterations: 340
currently lose_sum: 92.39210963249207
time_elpased: 1.338
batch start
#iterations: 341
currently lose_sum: 92.82453638315201
time_elpased: 1.317
batch start
#iterations: 342
currently lose_sum: 92.76395606994629
time_elpased: 1.415
batch start
#iterations: 343
currently lose_sum: 92.48033100366592
time_elpased: 1.339
batch start
#iterations: 344
currently lose_sum: 92.075379550457
time_elpased: 1.318
batch start
#iterations: 345
currently lose_sum: 92.39392685890198
time_elpased: 1.367
batch start
#iterations: 346
currently lose_sum: 91.67955154180527
time_elpased: 1.365
batch start
#iterations: 347
currently lose_sum: 92.19407767057419
time_elpased: 1.382
batch start
#iterations: 348
currently lose_sum: 92.81236708164215
time_elpased: 1.378
batch start
#iterations: 349
currently lose_sum: 92.16143733263016
time_elpased: 1.343
batch start
#iterations: 350
currently lose_sum: 92.52381145954132
time_elpased: 1.344
batch start
#iterations: 351
currently lose_sum: 92.63718456029892
time_elpased: 1.345
batch start
#iterations: 352
currently lose_sum: 91.78772008419037
time_elpased: 1.383
batch start
#iterations: 353
currently lose_sum: 92.12489968538284
time_elpased: 1.356
batch start
#iterations: 354
currently lose_sum: 91.76207911968231
time_elpased: 1.335
batch start
#iterations: 355
currently lose_sum: 92.38199335336685
time_elpased: 1.371
batch start
#iterations: 356
currently lose_sum: 92.51248353719711
time_elpased: 1.334
batch start
#iterations: 357
currently lose_sum: 92.48295885324478
time_elpased: 1.342
batch start
#iterations: 358
currently lose_sum: 91.6449339389801
time_elpased: 1.382
batch start
#iterations: 359
currently lose_sum: 91.4969551563263
time_elpased: 1.408
start validation test
0.625515463918
0.673950056754
0.488834002264
0.566656725321
0.625755429305
61.736
batch start
#iterations: 360
currently lose_sum: 92.23364108800888
time_elpased: 1.505
batch start
#iterations: 361
currently lose_sum: 92.91206681728363
time_elpased: 1.367
batch start
#iterations: 362
currently lose_sum: 92.34718364477158
time_elpased: 1.35
batch start
#iterations: 363
currently lose_sum: 91.9978654384613
time_elpased: 1.348
batch start
#iterations: 364
currently lose_sum: 92.37038606405258
time_elpased: 1.345
batch start
#iterations: 365
currently lose_sum: 92.03139752149582
time_elpased: 1.351
batch start
#iterations: 366
currently lose_sum: 92.62747687101364
time_elpased: 1.353
batch start
#iterations: 367
currently lose_sum: 91.90946137905121
time_elpased: 1.363
batch start
#iterations: 368
currently lose_sum: 91.66728472709656
time_elpased: 1.352
batch start
#iterations: 369
currently lose_sum: 92.2626404762268
time_elpased: 1.35
batch start
#iterations: 370
currently lose_sum: 92.01519757509232
time_elpased: 1.356
batch start
#iterations: 371
currently lose_sum: 91.5113086104393
time_elpased: 1.354
batch start
#iterations: 372
currently lose_sum: 91.63189417123795
time_elpased: 1.361
batch start
#iterations: 373
currently lose_sum: 92.28386628627777
time_elpased: 1.351
batch start
#iterations: 374
currently lose_sum: 92.18890762329102
time_elpased: 1.35
batch start
#iterations: 375
currently lose_sum: 92.71878468990326
time_elpased: 1.341
batch start
#iterations: 376
currently lose_sum: 91.97258621454239
time_elpased: 1.325
batch start
#iterations: 377
currently lose_sum: 92.86548912525177
time_elpased: 1.332
batch start
#iterations: 378
currently lose_sum: 91.70449966192245
time_elpased: 1.366
batch start
#iterations: 379
currently lose_sum: 91.8741385936737
time_elpased: 1.341
start validation test
0.607474226804
0.671900556101
0.422764227642
0.518981744678
0.607798513697
62.321
batch start
#iterations: 380
currently lose_sum: 92.40647941827774
time_elpased: 1.399
batch start
#iterations: 381
currently lose_sum: 92.063894033432
time_elpased: 1.346
batch start
#iterations: 382
currently lose_sum: 91.59586101770401
time_elpased: 1.313
batch start
#iterations: 383
currently lose_sum: 91.55919861793518
time_elpased: 1.327
batch start
#iterations: 384
currently lose_sum: 92.06736481189728
time_elpased: 1.34
batch start
#iterations: 385
currently lose_sum: 92.30668824911118
time_elpased: 1.323
batch start
#iterations: 386
currently lose_sum: 92.04545438289642
time_elpased: 1.339
batch start
#iterations: 387
currently lose_sum: 92.21515476703644
time_elpased: 1.377
batch start
#iterations: 388
currently lose_sum: 92.49646544456482
time_elpased: 1.387
batch start
#iterations: 389
currently lose_sum: 91.76131463050842
time_elpased: 1.359
batch start
#iterations: 390
currently lose_sum: 92.0341557264328
time_elpased: 1.357
batch start
#iterations: 391
currently lose_sum: 91.64831721782684
time_elpased: 1.336
batch start
#iterations: 392
currently lose_sum: 91.96334850788116
time_elpased: 1.369
batch start
#iterations: 393
currently lose_sum: 91.92811471223831
time_elpased: 1.342
batch start
#iterations: 394
currently lose_sum: 91.7899951338768
time_elpased: 1.369
batch start
#iterations: 395
currently lose_sum: 92.19599598646164
time_elpased: 1.338
batch start
#iterations: 396
currently lose_sum: 91.87379825115204
time_elpased: 1.442
batch start
#iterations: 397
currently lose_sum: 91.67889261245728
time_elpased: 1.376
batch start
#iterations: 398
currently lose_sum: 92.07173544168472
time_elpased: 1.355
batch start
#iterations: 399
currently lose_sum: 91.40345370769501
time_elpased: 1.363
start validation test
0.606907216495
0.674279046508
0.416280745086
0.514762025961
0.607241890668
62.432
acc: 0.653
pre: 0.641
rec: 0.696
F1: 0.667
auc: 0.652
