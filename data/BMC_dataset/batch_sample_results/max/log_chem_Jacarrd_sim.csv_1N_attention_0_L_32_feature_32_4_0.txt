start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.5111665725708
time_elpased: 2.122
batch start
#iterations: 1
currently lose_sum: 100.40539109706879
time_elpased: 2.022
batch start
#iterations: 2
currently lose_sum: 100.31171369552612
time_elpased: 2.018
batch start
#iterations: 3
currently lose_sum: 100.33171778917313
time_elpased: 2.059
batch start
#iterations: 4
currently lose_sum: 100.2024085521698
time_elpased: 2.034
batch start
#iterations: 5
currently lose_sum: 100.16644161939621
time_elpased: 2.127
batch start
#iterations: 6
currently lose_sum: 100.10465317964554
time_elpased: 2.069
batch start
#iterations: 7
currently lose_sum: 99.94644963741302
time_elpased: 2.082
batch start
#iterations: 8
currently lose_sum: 99.73572915792465
time_elpased: 2.066
batch start
#iterations: 9
currently lose_sum: 99.93429726362228
time_elpased: 2.067
batch start
#iterations: 10
currently lose_sum: 99.6975571513176
time_elpased: 2.059
batch start
#iterations: 11
currently lose_sum: 99.50454062223434
time_elpased: 2.091
batch start
#iterations: 12
currently lose_sum: 99.51067626476288
time_elpased: 2.065
batch start
#iterations: 13
currently lose_sum: 99.33459734916687
time_elpased: 2.035
batch start
#iterations: 14
currently lose_sum: 99.1502086520195
time_elpased: 2.074
batch start
#iterations: 15
currently lose_sum: 98.92970603704453
time_elpased: 2.073
batch start
#iterations: 16
currently lose_sum: 99.18256443738937
time_elpased: 2.059
batch start
#iterations: 17
currently lose_sum: 99.01450192928314
time_elpased: 2.065
batch start
#iterations: 18
currently lose_sum: 98.6926742196083
time_elpased: 2.084
batch start
#iterations: 19
currently lose_sum: 98.92257553339005
time_elpased: 2.111
start validation test
0.608350515464
0.647255038221
0.479263147062
0.55073320719
0.60857714825
64.821
batch start
#iterations: 20
currently lose_sum: 98.93759995698929
time_elpased: 2.103
batch start
#iterations: 21
currently lose_sum: 98.54490047693253
time_elpased: 2.111
batch start
#iterations: 22
currently lose_sum: 98.58590430021286
time_elpased: 2.104
batch start
#iterations: 23
currently lose_sum: 98.97868883609772
time_elpased: 2.074
batch start
#iterations: 24
currently lose_sum: 98.38080680370331
time_elpased: 2.063
batch start
#iterations: 25
currently lose_sum: 98.27510261535645
time_elpased: 2.065
batch start
#iterations: 26
currently lose_sum: 98.33610981702805
time_elpased: 2.084
batch start
#iterations: 27
currently lose_sum: 98.4096229672432
time_elpased: 2.07
batch start
#iterations: 28
currently lose_sum: 97.99160087108612
time_elpased: 2.08
batch start
#iterations: 29
currently lose_sum: 98.3724656701088
time_elpased: 2.061
batch start
#iterations: 30
currently lose_sum: 97.93979501724243
time_elpased: 2.106
batch start
#iterations: 31
currently lose_sum: 97.89769721031189
time_elpased: 2.072
batch start
#iterations: 32
currently lose_sum: 97.42158639431
time_elpased: 2.04
batch start
#iterations: 33
currently lose_sum: 97.8075145483017
time_elpased: 2.064
batch start
#iterations: 34
currently lose_sum: 97.75691145658493
time_elpased: 2.085
batch start
#iterations: 35
currently lose_sum: 97.86873632669449
time_elpased: 2.092
batch start
#iterations: 36
currently lose_sum: 97.96982938051224
time_elpased: 2.08
batch start
#iterations: 37
currently lose_sum: 97.69980758428574
time_elpased: 2.125
batch start
#iterations: 38
currently lose_sum: 97.69575124979019
time_elpased: 2.092
batch start
#iterations: 39
currently lose_sum: 97.39819157123566
time_elpased: 2.1
start validation test
0.613195876289
0.671046529603
0.446742821859
0.536389472384
0.613488110299
63.681
batch start
#iterations: 40
currently lose_sum: 97.11816567182541
time_elpased: 2.085
batch start
#iterations: 41
currently lose_sum: 99.75010704994202
time_elpased: 2.104
batch start
#iterations: 42
currently lose_sum: 98.8145399093628
time_elpased: 2.047
batch start
#iterations: 43
currently lose_sum: 97.51057761907578
time_elpased: 2.096
batch start
#iterations: 44
currently lose_sum: 97.49069315195084
time_elpased: 2.061
batch start
#iterations: 45
currently lose_sum: 97.53342199325562
time_elpased: 2.058
batch start
#iterations: 46
currently lose_sum: 97.8497257232666
time_elpased: 2.063
batch start
#iterations: 47
currently lose_sum: 97.3917230963707
time_elpased: 2.052
batch start
#iterations: 48
currently lose_sum: 97.08868616819382
time_elpased: 2.072
batch start
#iterations: 49
currently lose_sum: 97.06456768512726
time_elpased: 2.052
batch start
#iterations: 50
currently lose_sum: 99.37947696447372
time_elpased: 2.048
batch start
#iterations: 51
currently lose_sum: 100.42584824562073
time_elpased: 2.067
batch start
#iterations: 52
currently lose_sum: 98.18378818035126
time_elpased: 2.085
batch start
#iterations: 53
currently lose_sum: 97.7023708820343
time_elpased: 2.063
batch start
#iterations: 54
currently lose_sum: 97.71370244026184
time_elpased: 2.043
batch start
#iterations: 55
currently lose_sum: 97.21568536758423
time_elpased: 2.069
batch start
#iterations: 56
currently lose_sum: 97.95321464538574
time_elpased: 2.066
batch start
#iterations: 57
currently lose_sum: 97.96581411361694
time_elpased: 2.053
batch start
#iterations: 58
currently lose_sum: 98.46070969104767
time_elpased: 2.046
batch start
#iterations: 59
currently lose_sum: 97.39893651008606
time_elpased: 2.092
start validation test
0.638659793814
0.59787403283
0.850879901204
0.702284889153
0.638287208683
62.449
batch start
#iterations: 60
currently lose_sum: 97.36629515886307
time_elpased: 2.054
batch start
#iterations: 61
currently lose_sum: 97.58312964439392
time_elpased: 2.072
batch start
#iterations: 62
currently lose_sum: 99.18553423881531
time_elpased: 2.086
batch start
#iterations: 63
currently lose_sum: 97.48033410310745
time_elpased: 2.074
batch start
#iterations: 64
currently lose_sum: 97.15722453594208
time_elpased: 2.049
batch start
#iterations: 65
currently lose_sum: 97.70126539468765
time_elpased: 2.034
batch start
#iterations: 66
currently lose_sum: 96.65492230653763
time_elpased: 2.106
batch start
#iterations: 67
currently lose_sum: 96.91364586353302
time_elpased: 2.088
batch start
#iterations: 68
currently lose_sum: 96.89287519454956
time_elpased: 2.04
batch start
#iterations: 69
currently lose_sum: 97.4370419383049
time_elpased: 2.079
batch start
#iterations: 70
currently lose_sum: 96.77040958404541
time_elpased: 2.069
batch start
#iterations: 71
currently lose_sum: 96.54405093193054
time_elpased: 2.068
batch start
#iterations: 72
currently lose_sum: 97.72903567552567
time_elpased: 2.026
batch start
#iterations: 73
currently lose_sum: 99.93299651145935
time_elpased: 2.07
batch start
#iterations: 74
currently lose_sum: 97.75773096084595
time_elpased: 2.064
batch start
#iterations: 75
currently lose_sum: 96.39269715547562
time_elpased: 2.072
batch start
#iterations: 76
currently lose_sum: 96.39526551961899
time_elpased: 2.059
batch start
#iterations: 77
currently lose_sum: 96.76100271940231
time_elpased: 2.075
batch start
#iterations: 78
currently lose_sum: 96.572938144207
time_elpased: 2.076
batch start
#iterations: 79
currently lose_sum: 97.33889353275299
time_elpased: 2.124
start validation test
0.544278350515
0.524134890897
0.978902953586
0.682720258389
0.543515299989
66.296
batch start
#iterations: 80
currently lose_sum: 96.65458059310913
time_elpased: 2.054
batch start
#iterations: 81
currently lose_sum: 99.16437470912933
time_elpased: 2.09
batch start
#iterations: 82
currently lose_sum: 100.50634115934372
time_elpased: 2.046
batch start
#iterations: 83
currently lose_sum: 100.50232237577438
time_elpased: 2.14
batch start
#iterations: 84
currently lose_sum: 100.49324929714203
time_elpased: 2.106
batch start
#iterations: 85
currently lose_sum: 100.42178666591644
time_elpased: 2.163
batch start
#iterations: 86
currently lose_sum: 97.15057802200317
time_elpased: 2.104
batch start
#iterations: 87
currently lose_sum: 98.60649436712265
time_elpased: 2.139
batch start
#iterations: 88
currently lose_sum: 97.51619839668274
time_elpased: 2.077
batch start
#iterations: 89
currently lose_sum: 96.54333353042603
time_elpased: 2.133
batch start
#iterations: 90
currently lose_sum: 96.37427788972855
time_elpased: 2.16
batch start
#iterations: 91
currently lose_sum: 96.65494626760483
time_elpased: 2.065
batch start
#iterations: 92
currently lose_sum: 96.54786258935928
time_elpased: 2.081
batch start
#iterations: 93
currently lose_sum: 96.70462375879288
time_elpased: 2.084
batch start
#iterations: 94
currently lose_sum: 96.08228689432144
time_elpased: 2.083
batch start
#iterations: 95
currently lose_sum: 96.42616307735443
time_elpased: 2.06
batch start
#iterations: 96
currently lose_sum: 96.35813015699387
time_elpased: 2.058
batch start
#iterations: 97
currently lose_sum: 96.87121856212616
time_elpased: 2.061
batch start
#iterations: 98
currently lose_sum: 96.52395862340927
time_elpased: 2.094
batch start
#iterations: 99
currently lose_sum: 96.95262277126312
time_elpased: 2.074
start validation test
0.662371134021
0.632765993125
0.776680045281
0.697375716134
0.662170447096
61.173
batch start
#iterations: 100
currently lose_sum: 100.0907586812973
time_elpased: 2.081
batch start
#iterations: 101
currently lose_sum: 100.5101187825203
time_elpased: 2.097
batch start
#iterations: 102
currently lose_sum: 100.50831139087677
time_elpased: 2.058
batch start
#iterations: 103
currently lose_sum: 100.50668668746948
time_elpased: 2.03
batch start
#iterations: 104
currently lose_sum: 100.50467246770859
time_elpased: 2.097
batch start
#iterations: 105
currently lose_sum: 100.50138086080551
time_elpased: 2.074
batch start
#iterations: 106
currently lose_sum: 100.49438267946243
time_elpased: 2.09
batch start
#iterations: 107
currently lose_sum: 100.1588334441185
time_elpased: 2.095
batch start
#iterations: 108
currently lose_sum: 97.97145962715149
time_elpased: 2.072
batch start
#iterations: 109
currently lose_sum: 100.04298973083496
time_elpased: 2.111
batch start
#iterations: 110
currently lose_sum: 100.45823925733566
time_elpased: 2.084
batch start
#iterations: 111
currently lose_sum: 98.26405668258667
time_elpased: 2.151
batch start
#iterations: 112
currently lose_sum: 97.60567593574524
time_elpased: 2.062
batch start
#iterations: 113
currently lose_sum: 96.80330127477646
time_elpased: 2.116
batch start
#iterations: 114
currently lose_sum: 98.11665046215057
time_elpased: 2.059
batch start
#iterations: 115
currently lose_sum: 96.81551331281662
time_elpased: 2.157
batch start
#iterations: 116
currently lose_sum: 96.76265114545822
time_elpased: 2.087
batch start
#iterations: 117
currently lose_sum: 96.94756191968918
time_elpased: 2.109
batch start
#iterations: 118
currently lose_sum: 98.22440963983536
time_elpased: 2.108
batch start
#iterations: 119
currently lose_sum: 96.91624009609222
time_elpased: 1.981
start validation test
0.633505154639
0.590151462757
0.878151692909
0.705906684315
0.633075639907
63.733
batch start
#iterations: 120
currently lose_sum: 96.56718015670776
time_elpased: 2.137
batch start
#iterations: 121
currently lose_sum: 96.31234765052795
time_elpased: 2.126
batch start
#iterations: 122
currently lose_sum: 96.11463779211044
time_elpased: 2.106
batch start
#iterations: 123
currently lose_sum: 95.64168471097946
time_elpased: 2.059
batch start
#iterations: 124
currently lose_sum: 97.1374414563179
time_elpased: 1.916
batch start
#iterations: 125
currently lose_sum: 96.18054920434952
time_elpased: 1.97
batch start
#iterations: 126
currently lose_sum: 96.7257661819458
time_elpased: 1.937
batch start
#iterations: 127
currently lose_sum: 97.0325374007225
time_elpased: 2.013
batch start
#iterations: 128
currently lose_sum: 100.51653093099594
time_elpased: 2.107
batch start
#iterations: 129
currently lose_sum: 100.49978917837143
time_elpased: 2.099
batch start
#iterations: 130
currently lose_sum: 100.34907507896423
time_elpased: 2.038
batch start
#iterations: 131
currently lose_sum: 96.3222223520279
time_elpased: 2.059
batch start
#iterations: 132
currently lose_sum: 99.07089453935623
time_elpased: 2.084
batch start
#iterations: 133
currently lose_sum: 100.50330209732056
time_elpased: 2.086
batch start
#iterations: 134
currently lose_sum: 100.49953806400299
time_elpased: 2.089
batch start
#iterations: 135
currently lose_sum: 100.4846009016037
time_elpased: 2.094
batch start
#iterations: 136
currently lose_sum: 98.76879018545151
time_elpased: 2.071
batch start
#iterations: 137
currently lose_sum: 96.327123939991
time_elpased: 2.377
batch start
#iterations: 138
currently lose_sum: 96.08970183134079
time_elpased: 2.124
batch start
#iterations: 139
currently lose_sum: 96.70300394296646
time_elpased: 2.112
start validation test
0.501391752577
0.501134605467
1.0
0.667674442574
0.500516368894
67.904
batch start
#iterations: 140
currently lose_sum: 100.53771013021469
time_elpased: 2.146
batch start
#iterations: 141
currently lose_sum: 99.67312270402908
time_elpased: 2.081
batch start
#iterations: 142
currently lose_sum: 96.74843537807465
time_elpased: 2.119
batch start
#iterations: 143
currently lose_sum: 96.58546501398087
time_elpased: 2.082
batch start
#iterations: 144
currently lose_sum: 96.91353225708008
time_elpased: 2.065
batch start
#iterations: 145
currently lose_sum: 98.5500648021698
time_elpased: 2.167
batch start
#iterations: 146
currently lose_sum: 96.0153278708458
time_elpased: 2.114
batch start
#iterations: 147
currently lose_sum: 99.75715899467468
time_elpased: 2.08
batch start
#iterations: 148
currently lose_sum: 95.59696847200394
time_elpased: 1.975
batch start
#iterations: 149
currently lose_sum: 97.11211514472961
time_elpased: 1.949
batch start
#iterations: 150
currently lose_sum: 95.90388643741608
time_elpased: 2.014
batch start
#iterations: 151
currently lose_sum: 97.43382203578949
time_elpased: 2.057
batch start
#iterations: 152
currently lose_sum: 96.12250477075577
time_elpased: 2.084
batch start
#iterations: 153
currently lose_sum: 96.28329902887344
time_elpased: 2.069
batch start
#iterations: 154
currently lose_sum: 95.91402900218964
time_elpased: 2.081
batch start
#iterations: 155
currently lose_sum: 96.28350692987442
time_elpased: 2.061
batch start
#iterations: 156
currently lose_sum: 96.53256595134735
time_elpased: 2.168
batch start
#iterations: 157
currently lose_sum: 99.51158213615417
time_elpased: 2.088
batch start
#iterations: 158
currently lose_sum: 100.48804479837418
time_elpased: 2.059
batch start
#iterations: 159
currently lose_sum: 99.0200983285904
time_elpased: 2.119
start validation test
0.663762886598
0.637553832903
0.76175774416
0.694143573873
0.663590841511
60.960
batch start
#iterations: 160
currently lose_sum: 96.17040467262268
time_elpased: 2.057
batch start
#iterations: 161
currently lose_sum: 96.29154342412949
time_elpased: 2.117
batch start
#iterations: 162
currently lose_sum: 95.6430002450943
time_elpased: 2.198
batch start
#iterations: 163
currently lose_sum: 95.6589760184288
time_elpased: 2.093
batch start
#iterations: 164
currently lose_sum: 97.91532009840012
time_elpased: 2.067
batch start
#iterations: 165
currently lose_sum: 100.46977788209915
time_elpased: 2.118
batch start
#iterations: 166
currently lose_sum: 96.785196185112
time_elpased: 2.094
batch start
#iterations: 167
currently lose_sum: 95.66218686103821
time_elpased: 2.074
batch start
#iterations: 168
currently lose_sum: 96.2364012002945
time_elpased: 2.088
batch start
#iterations: 169
currently lose_sum: 95.97874844074249
time_elpased: 2.111
batch start
#iterations: 170
currently lose_sum: 95.42433387041092
time_elpased: 2.028
batch start
#iterations: 171
currently lose_sum: 96.57396972179413
time_elpased: 2.013
batch start
#iterations: 172
currently lose_sum: 95.01946604251862
time_elpased: 1.942
batch start
#iterations: 173
currently lose_sum: 96.2258226275444
time_elpased: 1.945
batch start
#iterations: 174
currently lose_sum: 94.96313840150833
time_elpased: 1.989
batch start
#iterations: 175
currently lose_sum: 96.27860563993454
time_elpased: 1.944
batch start
#iterations: 176
currently lose_sum: 95.84228843450546
time_elpased: 1.929
batch start
#iterations: 177
currently lose_sum: 95.89813840389252
time_elpased: 1.982
batch start
#iterations: 178
currently lose_sum: 95.2999963760376
time_elpased: 1.937
batch start
#iterations: 179
currently lose_sum: 95.15483433008194
time_elpased: 1.951
start validation test
0.662680412371
0.623588065747
0.823813934342
0.709851910969
0.66239751762
60.631
batch start
#iterations: 180
currently lose_sum: 95.98873376846313
time_elpased: 2.001
batch start
#iterations: 181
currently lose_sum: 94.98620212078094
time_elpased: 1.979
batch start
#iterations: 182
currently lose_sum: 95.44992715120316
time_elpased: 2.07
batch start
#iterations: 183
currently lose_sum: 95.56315875053406
time_elpased: 2.102
batch start
#iterations: 184
currently lose_sum: 94.98117798566818
time_elpased: 2.128
batch start
#iterations: 185
currently lose_sum: 95.36672276258469
time_elpased: 2.133
batch start
#iterations: 186
currently lose_sum: 95.18734687566757
time_elpased: 2.108
batch start
#iterations: 187
currently lose_sum: 97.99677294492722
time_elpased: 2.134
batch start
#iterations: 188
currently lose_sum: 100.4938924908638
time_elpased: 2.138
batch start
#iterations: 189
currently lose_sum: 98.82233852148056
time_elpased: 2.178
batch start
#iterations: 190
currently lose_sum: 95.24047881364822
time_elpased: 2.083
batch start
#iterations: 191
currently lose_sum: 95.47833108901978
time_elpased: 2.117
batch start
#iterations: 192
currently lose_sum: 94.91626715660095
time_elpased: 2.082
batch start
#iterations: 193
currently lose_sum: 95.24595785140991
time_elpased: 2.1
batch start
#iterations: 194
currently lose_sum: 95.08782714605331
time_elpased: 2.036
batch start
#iterations: 195
currently lose_sum: 95.37043130397797
time_elpased: 2.089
batch start
#iterations: 196
currently lose_sum: 96.05637663602829
time_elpased: 2.054
batch start
#iterations: 197
currently lose_sum: 95.43265986442566
time_elpased: 2.078
batch start
#iterations: 198
currently lose_sum: 95.09467780590057
time_elpased: 2.051
batch start
#iterations: 199
currently lose_sum: 95.89478331804276
time_elpased: 2.077
start validation test
0.662268041237
0.661430174436
0.667284141196
0.664344262295
0.6622592347
61.929
batch start
#iterations: 200
currently lose_sum: 95.2444806098938
time_elpased: 2.134
batch start
#iterations: 201
currently lose_sum: 95.29020112752914
time_elpased: 2.129
batch start
#iterations: 202
currently lose_sum: 95.37991636991501
time_elpased: 2.113
batch start
#iterations: 203
currently lose_sum: 95.29868298768997
time_elpased: 2.09
batch start
#iterations: 204
currently lose_sum: 96.23098230361938
time_elpased: 2.072
batch start
#iterations: 205
currently lose_sum: 95.16029423475266
time_elpased: 2.106
batch start
#iterations: 206
currently lose_sum: 95.19914031028748
time_elpased: 2.129
batch start
#iterations: 207
currently lose_sum: 94.83060657978058
time_elpased: 2.07
batch start
#iterations: 208
currently lose_sum: 95.21286851167679
time_elpased: 2.07
batch start
#iterations: 209
currently lose_sum: 95.30824017524719
time_elpased: 2.095
batch start
#iterations: 210
currently lose_sum: 95.3896986246109
time_elpased: 2.066
batch start
#iterations: 211
currently lose_sum: 94.57282763719559
time_elpased: 2.18
batch start
#iterations: 212
currently lose_sum: 95.32198596000671
time_elpased: 2.056
batch start
#iterations: 213
currently lose_sum: 94.51359713077545
time_elpased: 2.111
batch start
#iterations: 214
currently lose_sum: 94.91599106788635
time_elpased: 2.053
batch start
#iterations: 215
currently lose_sum: 95.01687031984329
time_elpased: 2.166
batch start
#iterations: 216
currently lose_sum: 95.02071154117584
time_elpased: 2.103
batch start
#iterations: 217
currently lose_sum: 94.76611614227295
time_elpased: 2.125
batch start
#iterations: 218
currently lose_sum: 94.60647422075272
time_elpased: 2.071
batch start
#iterations: 219
currently lose_sum: 95.14060628414154
time_elpased: 2.035
start validation test
0.604226804124
0.562719163334
0.941339919728
0.704373941167
0.603634950053
65.232
batch start
#iterations: 220
currently lose_sum: 95.5443314909935
time_elpased: 2.131
batch start
#iterations: 221
currently lose_sum: 95.39069491624832
time_elpased: 2.073
batch start
#iterations: 222
currently lose_sum: 95.16773664951324
time_elpased: 2.122
batch start
#iterations: 223
currently lose_sum: 95.21076375246048
time_elpased: 2.168
batch start
#iterations: 224
currently lose_sum: 95.43753355741501
time_elpased: 2.079
batch start
#iterations: 225
currently lose_sum: 94.5195437669754
time_elpased: 2.109
batch start
#iterations: 226
currently lose_sum: 94.70169323682785
time_elpased: 2.032
batch start
#iterations: 227
currently lose_sum: 95.4659538269043
time_elpased: 2.099
batch start
#iterations: 228
currently lose_sum: 94.82238227128983
time_elpased: 2.045
batch start
#iterations: 229
currently lose_sum: 95.63952255249023
time_elpased: 2.093
batch start
#iterations: 230
currently lose_sum: 95.07164216041565
time_elpased: 2.067
batch start
#iterations: 231
currently lose_sum: 94.75632894039154
time_elpased: 2.088
batch start
#iterations: 232
currently lose_sum: 94.59532928466797
time_elpased: 2.148
batch start
#iterations: 233
currently lose_sum: 94.36507660150528
time_elpased: 2.111
batch start
#iterations: 234
currently lose_sum: 94.68878853321075
time_elpased: 2.055
batch start
#iterations: 235
currently lose_sum: 94.36100840568542
time_elpased: 2.106
batch start
#iterations: 236
currently lose_sum: 94.8024628162384
time_elpased: 2.086
batch start
#iterations: 237
currently lose_sum: 95.16284716129303
time_elpased: 2.148
batch start
#iterations: 238
currently lose_sum: 94.76035040616989
time_elpased: 2.101
batch start
#iterations: 239
currently lose_sum: 95.30645024776459
time_elpased: 2.095
start validation test
0.662577319588
0.622973706663
0.826592569723
0.710482087572
0.662289365518
60.844
batch start
#iterations: 240
currently lose_sum: 94.20164352655411
time_elpased: 2.076
batch start
#iterations: 241
currently lose_sum: 95.19161856174469
time_elpased: 2.067
batch start
#iterations: 242
currently lose_sum: 94.56669634580612
time_elpased: 2.049
batch start
#iterations: 243
currently lose_sum: 94.6489183306694
time_elpased: 2.073
batch start
#iterations: 244
currently lose_sum: 95.3132016658783
time_elpased: 2.044
batch start
#iterations: 245
currently lose_sum: 94.99361807107925
time_elpased: 2.095
batch start
#iterations: 246
currently lose_sum: 94.97673118114471
time_elpased: 2.07
batch start
#iterations: 247
currently lose_sum: 94.54629027843475
time_elpased: 2.089
batch start
#iterations: 248
currently lose_sum: 94.51676279306412
time_elpased: 2.075
batch start
#iterations: 249
currently lose_sum: 95.19925886392593
time_elpased: 2.096
batch start
#iterations: 250
currently lose_sum: 94.53061354160309
time_elpased: 2.062
batch start
#iterations: 251
currently lose_sum: 94.39755362272263
time_elpased: 2.113
batch start
#iterations: 252
currently lose_sum: 94.1877663731575
time_elpased: 2.082
batch start
#iterations: 253
currently lose_sum: 94.50032818317413
time_elpased: 2.151
batch start
#iterations: 254
currently lose_sum: 94.45240944623947
time_elpased: 2.085
batch start
#iterations: 255
currently lose_sum: 94.25106590986252
time_elpased: 2.08
batch start
#iterations: 256
currently lose_sum: 94.95170396566391
time_elpased: 2.067
batch start
#iterations: 257
currently lose_sum: 94.59566801786423
time_elpased: 2.065
batch start
#iterations: 258
currently lose_sum: 94.14129561185837
time_elpased: 2.076
batch start
#iterations: 259
currently lose_sum: 93.88987737894058
time_elpased: 2.075
start validation test
0.624639175258
0.672377176837
0.488731089843
0.566030989273
0.624877782864
63.568
batch start
#iterations: 260
currently lose_sum: 94.03136736154556
time_elpased: 2.196
batch start
#iterations: 261
currently lose_sum: 94.97471070289612
time_elpased: 2.109
batch start
#iterations: 262
currently lose_sum: 94.18144696950912
time_elpased: 2.048
batch start
#iterations: 263
currently lose_sum: 94.90982693433762
time_elpased: 2.09
batch start
#iterations: 264
currently lose_sum: 94.30973052978516
time_elpased: 2.123
batch start
#iterations: 265
currently lose_sum: 94.46353912353516
time_elpased: 2.123
batch start
#iterations: 266
currently lose_sum: 94.43576294183731
time_elpased: 2.087
batch start
#iterations: 267
currently lose_sum: 94.18532955646515
time_elpased: 2.114
batch start
#iterations: 268
currently lose_sum: 94.35394394397736
time_elpased: 2.112
batch start
#iterations: 269
currently lose_sum: 93.97095388174057
time_elpased: 2.169
batch start
#iterations: 270
currently lose_sum: 94.07655453681946
time_elpased: 2.186
batch start
#iterations: 271
currently lose_sum: 95.1799835562706
time_elpased: 2.171
batch start
#iterations: 272
currently lose_sum: 94.30498719215393
time_elpased: 2.082
batch start
#iterations: 273
currently lose_sum: 94.26931303739548
time_elpased: 2.113
batch start
#iterations: 274
currently lose_sum: 94.04459661245346
time_elpased: 2.063
batch start
#iterations: 275
currently lose_sum: 93.83065712451935
time_elpased: 2.07
batch start
#iterations: 276
currently lose_sum: 94.32645851373672
time_elpased: 2.069
batch start
#iterations: 277
currently lose_sum: 94.40516769886017
time_elpased: 2.084
batch start
#iterations: 278
currently lose_sum: 94.36689323186874
time_elpased: 2.051
batch start
#iterations: 279
currently lose_sum: 93.97372788190842
time_elpased: 2.043
start validation test
0.651082474227
0.670050761421
0.597715344242
0.631819418004
0.651176168455
60.918
batch start
#iterations: 280
currently lose_sum: 94.0490403175354
time_elpased: 2.117
batch start
#iterations: 281
currently lose_sum: 93.95163142681122
time_elpased: 2.11
batch start
#iterations: 282
currently lose_sum: 94.61917579174042
time_elpased: 2.098
batch start
#iterations: 283
currently lose_sum: 94.45453172922134
time_elpased: 2.121
batch start
#iterations: 284
currently lose_sum: 99.20413959026337
time_elpased: 2.048
batch start
#iterations: 285
currently lose_sum: 100.48638701438904
time_elpased: 2.126
batch start
#iterations: 286
currently lose_sum: 98.66160422563553
time_elpased: 2.147
batch start
#iterations: 287
currently lose_sum: 95.66994172334671
time_elpased: 2.086
batch start
#iterations: 288
currently lose_sum: 95.15133047103882
time_elpased: 2.025
batch start
#iterations: 289
currently lose_sum: 95.17081248760223
time_elpased: 2.088
batch start
#iterations: 290
currently lose_sum: 95.14614063501358
time_elpased: 2.13
batch start
#iterations: 291
currently lose_sum: 94.54959523677826
time_elpased: 2.131
batch start
#iterations: 292
currently lose_sum: 94.35333549976349
time_elpased: 2.061
batch start
#iterations: 293
currently lose_sum: 94.44068962335587
time_elpased: 2.1
batch start
#iterations: 294
currently lose_sum: 94.7908998131752
time_elpased: 2.033
batch start
#iterations: 295
currently lose_sum: 94.18911057710648
time_elpased: 2.103
batch start
#iterations: 296
currently lose_sum: 93.85803782939911
time_elpased: 2.135
batch start
#iterations: 297
currently lose_sum: 94.25018793344498
time_elpased: 2.082
batch start
#iterations: 298
currently lose_sum: 94.12123274803162
time_elpased: 2.048
batch start
#iterations: 299
currently lose_sum: 94.6445620059967
time_elpased: 2.122
start validation test
0.662835051546
0.658072864822
0.68035401873
0.669027981582
0.662804294297
60.613
batch start
#iterations: 300
currently lose_sum: 94.77722358703613
time_elpased: 2.001
batch start
#iterations: 301
currently lose_sum: 94.2975058555603
time_elpased: 1.996
batch start
#iterations: 302
currently lose_sum: 93.89098310470581
time_elpased: 1.961
batch start
#iterations: 303
currently lose_sum: 94.45321428775787
time_elpased: 1.967
batch start
#iterations: 304
currently lose_sum: 93.4879098534584
time_elpased: 1.936
batch start
#iterations: 305
currently lose_sum: 95.01961421966553
time_elpased: 1.954
batch start
#iterations: 306
currently lose_sum: 94.64268517494202
time_elpased: 1.935
batch start
#iterations: 307
currently lose_sum: 93.84269672632217
time_elpased: 1.971
batch start
#iterations: 308
currently lose_sum: 93.66102355718613
time_elpased: 1.953
batch start
#iterations: 309
currently lose_sum: 94.13877862691879
time_elpased: 2.081
batch start
#iterations: 310
currently lose_sum: 93.56199485063553
time_elpased: 2.062
batch start
#iterations: 311
currently lose_sum: 94.4188602566719
time_elpased: 2.073
batch start
#iterations: 312
currently lose_sum: 94.9881506562233
time_elpased: 2.069
batch start
#iterations: 313
currently lose_sum: 93.08753085136414
time_elpased: 2.068
batch start
#iterations: 314
currently lose_sum: 93.70570015907288
time_elpased: 2.074
batch start
#iterations: 315
currently lose_sum: 93.90715086460114
time_elpased: 2.106
batch start
#iterations: 316
currently lose_sum: 93.85275053977966
time_elpased: 2.056
batch start
#iterations: 317
currently lose_sum: 94.47227823734283
time_elpased: 2.072
batch start
#iterations: 318
currently lose_sum: 93.327368080616
time_elpased: 2.104
batch start
#iterations: 319
currently lose_sum: 93.71496492624283
time_elpased: 2.127
start validation test
0.661288659794
0.650814956855
0.698569517341
0.673847222912
0.661223207498
60.184
batch start
#iterations: 320
currently lose_sum: 93.16919875144958
time_elpased: 2.085
batch start
#iterations: 321
currently lose_sum: 93.6474939584732
time_elpased: 2.146
batch start
#iterations: 322
currently lose_sum: 93.45862579345703
time_elpased: 2.17
batch start
#iterations: 323
currently lose_sum: 94.20881032943726
time_elpased: 2.146
batch start
#iterations: 324
currently lose_sum: 93.57397991418839
time_elpased: 2.076
batch start
#iterations: 325
currently lose_sum: 93.91456991434097
time_elpased: 2.102
batch start
#iterations: 326
currently lose_sum: 93.75198495388031
time_elpased: 2.072
batch start
#iterations: 327
currently lose_sum: 93.8413907289505
time_elpased: 2.143
batch start
#iterations: 328
currently lose_sum: 93.27882212400436
time_elpased: 2.057
batch start
#iterations: 329
currently lose_sum: 94.02497923374176
time_elpased: 2.034
batch start
#iterations: 330
currently lose_sum: 93.46263682842255
time_elpased: 1.934
batch start
#iterations: 331
currently lose_sum: 94.55531287193298
time_elpased: 2.068
batch start
#iterations: 332
currently lose_sum: 93.179731965065
time_elpased: 2.148
batch start
#iterations: 333
currently lose_sum: 93.98163306713104
time_elpased: 2.105
batch start
#iterations: 334
currently lose_sum: 93.70536488294601
time_elpased: 2.064
batch start
#iterations: 335
currently lose_sum: 93.29677724838257
time_elpased: 2.082
batch start
#iterations: 336
currently lose_sum: 93.80327582359314
time_elpased: 2.101
batch start
#iterations: 337
currently lose_sum: 94.96173202991486
time_elpased: 2.121
batch start
#iterations: 338
currently lose_sum: 93.6613205075264
time_elpased: 2.135
batch start
#iterations: 339
currently lose_sum: 93.09763914346695
time_elpased: 2.089
start validation test
0.658195876289
0.665168058232
0.639497787383
0.652080381972
0.658228703668
60.893
batch start
#iterations: 340
currently lose_sum: 93.86258924007416
time_elpased: 2.109
batch start
#iterations: 341
currently lose_sum: 93.00651717185974
time_elpased: 2.155
batch start
#iterations: 342
currently lose_sum: 93.75833112001419
time_elpased: 2.102
batch start
#iterations: 343
currently lose_sum: 93.36757981777191
time_elpased: 2.119
batch start
#iterations: 344
currently lose_sum: 93.28278303146362
time_elpased: 2.069
batch start
#iterations: 345
currently lose_sum: 93.64812850952148
time_elpased: 2.09
batch start
#iterations: 346
currently lose_sum: 92.96897834539413
time_elpased: 2.069
batch start
#iterations: 347
currently lose_sum: 94.41021555662155
time_elpased: 2.114
batch start
#iterations: 348
currently lose_sum: 94.05558055639267
time_elpased: 2.08
batch start
#iterations: 349
currently lose_sum: 93.8574807047844
time_elpased: 2.107
batch start
#iterations: 350
currently lose_sum: 93.15958571434021
time_elpased: 2.032
batch start
#iterations: 351
currently lose_sum: 93.29979109764099
time_elpased: 2.077
batch start
#iterations: 352
currently lose_sum: 93.7791833281517
time_elpased: 2.049
batch start
#iterations: 353
currently lose_sum: 93.05511271953583
time_elpased: 2.075
batch start
#iterations: 354
currently lose_sum: 93.59352165460587
time_elpased: 2.06
batch start
#iterations: 355
currently lose_sum: 93.81700795888901
time_elpased: 2.065
batch start
#iterations: 356
currently lose_sum: 94.14832270145416
time_elpased: 2.044
batch start
#iterations: 357
currently lose_sum: 92.9343928694725
time_elpased: 2.113
batch start
#iterations: 358
currently lose_sum: 94.24167174100876
time_elpased: 2.087
batch start
#iterations: 359
currently lose_sum: 93.40737909078598
time_elpased: 2.1
start validation test
0.654278350515
0.662141779789
0.632499742719
0.646981420075
0.654316586221
60.808
batch start
#iterations: 360
currently lose_sum: 93.14005291461945
time_elpased: 2.05
batch start
#iterations: 361
currently lose_sum: 93.86584061384201
time_elpased: 2.078
batch start
#iterations: 362
currently lose_sum: 93.66932410001755
time_elpased: 2.061
batch start
#iterations: 363
currently lose_sum: 93.58067339658737
time_elpased: 2.105
batch start
#iterations: 364
currently lose_sum: 93.77280485630035
time_elpased: 1.948
batch start
#iterations: 365
currently lose_sum: 94.36773163080215
time_elpased: 1.951
batch start
#iterations: 366
currently lose_sum: 93.22635990381241
time_elpased: 1.938
batch start
#iterations: 367
currently lose_sum: 93.45603370666504
time_elpased: 1.958
batch start
#iterations: 368
currently lose_sum: 93.04092741012573
time_elpased: 1.931
batch start
#iterations: 369
currently lose_sum: 93.56197029352188
time_elpased: 1.947
batch start
#iterations: 370
currently lose_sum: 92.8634546995163
time_elpased: 1.93
batch start
#iterations: 371
currently lose_sum: 92.8997887969017
time_elpased: 1.965
batch start
#iterations: 372
currently lose_sum: 93.72465962171555
time_elpased: 1.959
batch start
#iterations: 373
currently lose_sum: 93.66495454311371
time_elpased: 1.97
batch start
#iterations: 374
currently lose_sum: 93.47689998149872
time_elpased: 1.93
batch start
#iterations: 375
currently lose_sum: 93.52681630849838
time_elpased: 1.937
batch start
#iterations: 376
currently lose_sum: 93.72698944807053
time_elpased: 1.922
batch start
#iterations: 377
currently lose_sum: 93.16391944885254
time_elpased: 1.924
batch start
#iterations: 378
currently lose_sum: 93.52757835388184
time_elpased: 2.116
batch start
#iterations: 379
currently lose_sum: 92.87072795629501
time_elpased: 2.081
start validation test
0.635824742268
0.602330606575
0.803231450036
0.68842337376
0.635530833972
61.614
batch start
#iterations: 380
currently lose_sum: 92.93572896718979
time_elpased: 2.098
batch start
#iterations: 381
currently lose_sum: 92.89485031366348
time_elpased: 2.102
batch start
#iterations: 382
currently lose_sum: 93.12807476520538
time_elpased: 2.081
batch start
#iterations: 383
currently lose_sum: 92.86227136850357
time_elpased: 2.09
batch start
#iterations: 384
currently lose_sum: 93.3961973786354
time_elpased: 2.067
batch start
#iterations: 385
currently lose_sum: 93.78475868701935
time_elpased: 2.088
batch start
#iterations: 386
currently lose_sum: 93.49761426448822
time_elpased: 2.112
batch start
#iterations: 387
currently lose_sum: 93.56468516588211
time_elpased: 2.08
batch start
#iterations: 388
currently lose_sum: 92.72580540180206
time_elpased: 2.118
batch start
#iterations: 389
currently lose_sum: 92.95944726467133
time_elpased: 2.099
batch start
#iterations: 390
currently lose_sum: 93.23389887809753
time_elpased: 2.088
batch start
#iterations: 391
currently lose_sum: 93.28767383098602
time_elpased: 2.072
batch start
#iterations: 392
currently lose_sum: 92.89846336841583
time_elpased: 2.048
batch start
#iterations: 393
currently lose_sum: 93.43344044685364
time_elpased: 2.087
batch start
#iterations: 394
currently lose_sum: 92.5335853099823
time_elpased: 2.085
batch start
#iterations: 395
currently lose_sum: 92.72311556339264
time_elpased: 2.051
batch start
#iterations: 396
currently lose_sum: 93.2420637011528
time_elpased: 1.971
batch start
#iterations: 397
currently lose_sum: 92.77832132577896
time_elpased: 1.94
batch start
#iterations: 398
currently lose_sum: 92.4310992360115
time_elpased: 1.949
batch start
#iterations: 399
currently lose_sum: 93.07560455799103
time_elpased: 1.944
start validation test
0.642886597938
0.620163722533
0.740660697746
0.675077384861
0.642714940425
60.638
acc: 0.663
pre: 0.653
rec: 0.698
F1: 0.675
auc: 0.663
