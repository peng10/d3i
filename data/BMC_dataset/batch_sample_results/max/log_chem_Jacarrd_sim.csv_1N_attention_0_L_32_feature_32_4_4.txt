start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.54263770580292
time_elpased: 1.884
batch start
#iterations: 1
currently lose_sum: 100.40150672197342
time_elpased: 2.038
batch start
#iterations: 2
currently lose_sum: 100.37541568279266
time_elpased: 2.01
batch start
#iterations: 3
currently lose_sum: 100.24524927139282
time_elpased: 1.94
batch start
#iterations: 4
currently lose_sum: 100.341776907444
time_elpased: 1.879
batch start
#iterations: 5
currently lose_sum: 100.2629200220108
time_elpased: 1.944
batch start
#iterations: 6
currently lose_sum: 100.04628592729568
time_elpased: 2.112
batch start
#iterations: 7
currently lose_sum: 99.96263110637665
time_elpased: 2.195
batch start
#iterations: 8
currently lose_sum: 99.95087891817093
time_elpased: 2.429
batch start
#iterations: 9
currently lose_sum: 99.90750217437744
time_elpased: 2.222
batch start
#iterations: 10
currently lose_sum: 99.84020358324051
time_elpased: 2.127
batch start
#iterations: 11
currently lose_sum: 99.76517587900162
time_elpased: 2.171
batch start
#iterations: 12
currently lose_sum: 99.62565368413925
time_elpased: 2.349
batch start
#iterations: 13
currently lose_sum: 99.51943707466125
time_elpased: 2.162
batch start
#iterations: 14
currently lose_sum: 99.27275288105011
time_elpased: 1.994
batch start
#iterations: 15
currently lose_sum: 99.21146208047867
time_elpased: 2.134
batch start
#iterations: 16
currently lose_sum: 99.20438033342361
time_elpased: 2.009
batch start
#iterations: 17
currently lose_sum: 98.95876604318619
time_elpased: 2.25
batch start
#iterations: 18
currently lose_sum: 98.68214231729507
time_elpased: 2.085
batch start
#iterations: 19
currently lose_sum: 98.7528024315834
time_elpased: 1.84
start validation test
0.611288659794
0.596935138988
0.689513224246
0.639893032806
0.611151324506
64.277
batch start
#iterations: 20
currently lose_sum: 98.6882187128067
time_elpased: 1.683
batch start
#iterations: 21
currently lose_sum: 98.55484575033188
time_elpased: 1.523
batch start
#iterations: 22
currently lose_sum: 98.22574877738953
time_elpased: 1.528
batch start
#iterations: 23
currently lose_sum: 98.7911291718483
time_elpased: 1.546
batch start
#iterations: 24
currently lose_sum: 98.794952750206
time_elpased: 1.653
batch start
#iterations: 25
currently lose_sum: 98.1922277212143
time_elpased: 1.537
batch start
#iterations: 26
currently lose_sum: 98.23411452770233
time_elpased: 1.54
batch start
#iterations: 27
currently lose_sum: 97.9211397767067
time_elpased: 1.541
batch start
#iterations: 28
currently lose_sum: 98.09601628780365
time_elpased: 1.463
batch start
#iterations: 29
currently lose_sum: 98.39327490329742
time_elpased: 1.453
batch start
#iterations: 30
currently lose_sum: 97.72517758607864
time_elpased: 1.464
batch start
#iterations: 31
currently lose_sum: 98.0786999464035
time_elpased: 1.463
batch start
#iterations: 32
currently lose_sum: 98.17062056064606
time_elpased: 1.423
batch start
#iterations: 33
currently lose_sum: 98.92504334449768
time_elpased: 1.426
batch start
#iterations: 34
currently lose_sum: 98.14893579483032
time_elpased: 1.463
batch start
#iterations: 35
currently lose_sum: 97.85550487041473
time_elpased: 1.47
batch start
#iterations: 36
currently lose_sum: 97.6808443069458
time_elpased: 1.45
batch start
#iterations: 37
currently lose_sum: 97.79200118780136
time_elpased: 1.488
batch start
#iterations: 38
currently lose_sum: 96.97287082672119
time_elpased: 1.438
batch start
#iterations: 39
currently lose_sum: 96.92849332094193
time_elpased: 1.43
start validation test
0.577371134021
0.679432624113
0.295770299475
0.412131641213
0.57786552772
65.872
batch start
#iterations: 40
currently lose_sum: 98.22690033912659
time_elpased: 1.459
batch start
#iterations: 41
currently lose_sum: 97.18183863162994
time_elpased: 1.495
batch start
#iterations: 42
currently lose_sum: 97.58913505077362
time_elpased: 1.449
batch start
#iterations: 43
currently lose_sum: 97.61917841434479
time_elpased: 1.433
batch start
#iterations: 44
currently lose_sum: 98.73754340410233
time_elpased: 1.455
batch start
#iterations: 45
currently lose_sum: 97.42693519592285
time_elpased: 1.435
batch start
#iterations: 46
currently lose_sum: 98.28349512815475
time_elpased: 1.465
batch start
#iterations: 47
currently lose_sum: 97.52506685256958
time_elpased: 1.464
batch start
#iterations: 48
currently lose_sum: 97.57486754655838
time_elpased: 1.413
batch start
#iterations: 49
currently lose_sum: 98.1230479478836
time_elpased: 1.449
batch start
#iterations: 50
currently lose_sum: 97.0875716805458
time_elpased: 1.486
batch start
#iterations: 51
currently lose_sum: 98.38328617811203
time_elpased: 1.461
batch start
#iterations: 52
currently lose_sum: 97.86356735229492
time_elpased: 1.46
batch start
#iterations: 53
currently lose_sum: 97.50987577438354
time_elpased: 1.456
batch start
#iterations: 54
currently lose_sum: 97.16525214910507
time_elpased: 1.464
batch start
#iterations: 55
currently lose_sum: 96.87226039171219
time_elpased: 1.468
batch start
#iterations: 56
currently lose_sum: 96.53020656108856
time_elpased: 1.443
batch start
#iterations: 57
currently lose_sum: 96.54504716396332
time_elpased: 1.458
batch start
#iterations: 58
currently lose_sum: 97.59910494089127
time_elpased: 1.491
batch start
#iterations: 59
currently lose_sum: 97.25756484270096
time_elpased: 1.46
start validation test
0.648092783505
0.647298674822
0.653493876711
0.650381523019
0.648083301053
61.763
batch start
#iterations: 60
currently lose_sum: 97.6603952050209
time_elpased: 1.462
batch start
#iterations: 61
currently lose_sum: 97.58597868680954
time_elpased: 1.467
batch start
#iterations: 62
currently lose_sum: 97.38275146484375
time_elpased: 1.464
batch start
#iterations: 63
currently lose_sum: 97.24213498830795
time_elpased: 1.471
batch start
#iterations: 64
currently lose_sum: 97.96059513092041
time_elpased: 1.434
batch start
#iterations: 65
currently lose_sum: 96.93965721130371
time_elpased: 1.417
batch start
#iterations: 66
currently lose_sum: 96.78357326984406
time_elpased: 1.439
batch start
#iterations: 67
currently lose_sum: 96.29522001743317
time_elpased: 1.432
batch start
#iterations: 68
currently lose_sum: 97.06871634721756
time_elpased: 1.441
batch start
#iterations: 69
currently lose_sum: 96.7918456196785
time_elpased: 1.492
batch start
#iterations: 70
currently lose_sum: 96.76212102174759
time_elpased: 1.453
batch start
#iterations: 71
currently lose_sum: 96.40399610996246
time_elpased: 1.47
batch start
#iterations: 72
currently lose_sum: 96.4707864522934
time_elpased: 1.449
batch start
#iterations: 73
currently lose_sum: 96.62741547822952
time_elpased: 1.467
batch start
#iterations: 74
currently lose_sum: 96.0821875333786
time_elpased: 1.495
batch start
#iterations: 75
currently lose_sum: 96.99539566040039
time_elpased: 1.47
batch start
#iterations: 76
currently lose_sum: 96.44721645116806
time_elpased: 1.427
batch start
#iterations: 77
currently lose_sum: 97.02877849340439
time_elpased: 1.481
batch start
#iterations: 78
currently lose_sum: 99.22047013044357
time_elpased: 1.495
batch start
#iterations: 79
currently lose_sum: 97.00514662265778
time_elpased: 1.45
start validation test
0.49912371134
0.0
0.0
nan
0.5
67.193
acc: 0.648
pre: 0.645
rec: 0.660
F1: 0.653
auc: 0.648
