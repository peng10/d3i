start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.68375605344772
time_elpased: 1.853
batch start
#iterations: 1
currently lose_sum: 100.3476470708847
time_elpased: 1.773
batch start
#iterations: 2
currently lose_sum: 100.34336560964584
time_elpased: 1.788
batch start
#iterations: 3
currently lose_sum: 100.28076416254044
time_elpased: 1.773
batch start
#iterations: 4
currently lose_sum: 100.22301977872849
time_elpased: 1.786
batch start
#iterations: 5
currently lose_sum: 100.13641738891602
time_elpased: 1.799
batch start
#iterations: 6
currently lose_sum: 99.90558969974518
time_elpased: 1.776
batch start
#iterations: 7
currently lose_sum: 99.99173355102539
time_elpased: 1.793
batch start
#iterations: 8
currently lose_sum: 99.9052284359932
time_elpased: 1.795
batch start
#iterations: 9
currently lose_sum: 99.76774001121521
time_elpased: 1.779
batch start
#iterations: 10
currently lose_sum: 99.69133704900742
time_elpased: 1.787
batch start
#iterations: 11
currently lose_sum: 99.58876222372055
time_elpased: 1.763
batch start
#iterations: 12
currently lose_sum: 99.37825757265091
time_elpased: 1.763
batch start
#iterations: 13
currently lose_sum: 99.43109029531479
time_elpased: 1.767
batch start
#iterations: 14
currently lose_sum: 99.22674280405045
time_elpased: 1.763
batch start
#iterations: 15
currently lose_sum: 99.13233637809753
time_elpased: 1.789
batch start
#iterations: 16
currently lose_sum: 99.0160408616066
time_elpased: 1.798
batch start
#iterations: 17
currently lose_sum: 99.17223769426346
time_elpased: 1.79
batch start
#iterations: 18
currently lose_sum: 98.77365893125534
time_elpased: 1.779
batch start
#iterations: 19
currently lose_sum: 98.8181979060173
time_elpased: 1.797
start validation test
0.607680412371
0.617365135979
0.570031902851
0.592755096581
0.607746510136
64.601
batch start
#iterations: 20
currently lose_sum: 98.56252121925354
time_elpased: 1.917
batch start
#iterations: 21
currently lose_sum: 98.48993742465973
time_elpased: 1.801
batch start
#iterations: 22
currently lose_sum: 98.63561809062958
time_elpased: 1.762
batch start
#iterations: 23
currently lose_sum: 98.52909231185913
time_elpased: 1.775
batch start
#iterations: 24
currently lose_sum: 98.17068892717361
time_elpased: 1.77
batch start
#iterations: 25
currently lose_sum: 98.21655768156052
time_elpased: 1.811
batch start
#iterations: 26
currently lose_sum: 98.16317999362946
time_elpased: 1.819
batch start
#iterations: 27
currently lose_sum: 98.09759521484375
time_elpased: 1.805
batch start
#iterations: 28
currently lose_sum: 97.9404114484787
time_elpased: 1.774
batch start
#iterations: 29
currently lose_sum: 98.38041460514069
time_elpased: 1.874
batch start
#iterations: 30
currently lose_sum: 97.78987240791321
time_elpased: 1.768
batch start
#iterations: 31
currently lose_sum: 97.8498564362526
time_elpased: 1.791
batch start
#iterations: 32
currently lose_sum: 97.82629609107971
time_elpased: 1.769
batch start
#iterations: 33
currently lose_sum: 97.92902779579163
time_elpased: 1.78
batch start
#iterations: 34
currently lose_sum: 97.7019117474556
time_elpased: 1.79
batch start
#iterations: 35
currently lose_sum: 97.7778405547142
time_elpased: 1.795
batch start
#iterations: 36
currently lose_sum: 97.8950457572937
time_elpased: 1.748
batch start
#iterations: 37
currently lose_sum: 97.29409736394882
time_elpased: 1.767
batch start
#iterations: 38
currently lose_sum: 97.14169710874557
time_elpased: 1.775
batch start
#iterations: 39
currently lose_sum: 97.49723970890045
time_elpased: 1.786
start validation test
0.642525773196
0.620224719101
0.738499536894
0.674214309203
0.642357276451
63.006
batch start
#iterations: 40
currently lose_sum: 97.48638397455215
time_elpased: 1.804
batch start
#iterations: 41
currently lose_sum: 97.17507654428482
time_elpased: 1.778
batch start
#iterations: 42
currently lose_sum: 97.38391625881195
time_elpased: 1.807
batch start
#iterations: 43
currently lose_sum: 97.1527532339096
time_elpased: 1.791
batch start
#iterations: 44
currently lose_sum: 97.31920647621155
time_elpased: 1.794
batch start
#iterations: 45
currently lose_sum: 97.04528099298477
time_elpased: 1.778
batch start
#iterations: 46
currently lose_sum: 96.85978013277054
time_elpased: 1.746
batch start
#iterations: 47
currently lose_sum: 97.05563402175903
time_elpased: 1.779
batch start
#iterations: 48
currently lose_sum: 97.2486480474472
time_elpased: 1.749
batch start
#iterations: 49
currently lose_sum: 97.1198661327362
time_elpased: 1.783
batch start
#iterations: 50
currently lose_sum: 97.04970812797546
time_elpased: 1.855
batch start
#iterations: 51
currently lose_sum: 96.8128269314766
time_elpased: 1.899
batch start
#iterations: 52
currently lose_sum: 97.13310188055038
time_elpased: 1.89
batch start
#iterations: 53
currently lose_sum: 97.2182007431984
time_elpased: 1.91
batch start
#iterations: 54
currently lose_sum: 97.0617333650589
time_elpased: 1.931
batch start
#iterations: 55
currently lose_sum: 96.56410098075867
time_elpased: 1.955
batch start
#iterations: 56
currently lose_sum: 96.52756530046463
time_elpased: 1.889
batch start
#iterations: 57
currently lose_sum: 96.74942016601562
time_elpased: 1.884
batch start
#iterations: 58
currently lose_sum: 96.6438580751419
time_elpased: 1.922
batch start
#iterations: 59
currently lose_sum: 96.58053696155548
time_elpased: 1.932
start validation test
0.647886597938
0.61380126183
0.800967376762
0.695003795151
0.64761784102
62.697
batch start
#iterations: 60
currently lose_sum: 96.63471686840057
time_elpased: 1.916
batch start
#iterations: 61
currently lose_sum: 96.88353860378265
time_elpased: 1.892
batch start
#iterations: 62
currently lose_sum: 96.79591274261475
time_elpased: 1.88
batch start
#iterations: 63
currently lose_sum: 96.8295127749443
time_elpased: 1.916
batch start
#iterations: 64
currently lose_sum: 96.84769809246063
time_elpased: 1.865
batch start
#iterations: 65
currently lose_sum: 96.38758617639542
time_elpased: 1.917
batch start
#iterations: 66
currently lose_sum: 96.29510742425919
time_elpased: 1.91
batch start
#iterations: 67
currently lose_sum: 96.61748230457306
time_elpased: 1.906
batch start
#iterations: 68
currently lose_sum: 96.63420814275742
time_elpased: 1.886
batch start
#iterations: 69
currently lose_sum: 96.91929841041565
time_elpased: 1.927
batch start
#iterations: 70
currently lose_sum: 96.44296610355377
time_elpased: 1.91
batch start
#iterations: 71
currently lose_sum: 96.53865450620651
time_elpased: 1.893
batch start
#iterations: 72
currently lose_sum: 96.26492929458618
time_elpased: 1.931
batch start
#iterations: 73
currently lose_sum: 96.10238879919052
time_elpased: 1.951
batch start
#iterations: 74
currently lose_sum: 96.06801837682724
time_elpased: 1.913
batch start
#iterations: 75
currently lose_sum: 96.06256675720215
time_elpased: 1.89
batch start
#iterations: 76
currently lose_sum: 96.0647531747818
time_elpased: 1.891
batch start
#iterations: 77
currently lose_sum: 96.06111133098602
time_elpased: 1.883
batch start
#iterations: 78
currently lose_sum: 96.37145352363586
time_elpased: 1.912
batch start
#iterations: 79
currently lose_sum: 95.77186274528503
time_elpased: 1.933
start validation test
0.653556701031
0.627078384798
0.760728619944
0.687468030691
0.653368544197
62.002
batch start
#iterations: 80
currently lose_sum: 96.0663765668869
time_elpased: 1.901
batch start
#iterations: 81
currently lose_sum: 96.2375100851059
time_elpased: 1.911
batch start
#iterations: 82
currently lose_sum: 96.476911008358
time_elpased: 1.925
batch start
#iterations: 83
currently lose_sum: 95.72451639175415
time_elpased: 1.884
batch start
#iterations: 84
currently lose_sum: 96.49309903383255
time_elpased: 1.892
batch start
#iterations: 85
currently lose_sum: 95.89296078681946
time_elpased: 1.9
batch start
#iterations: 86
currently lose_sum: 96.3664379119873
time_elpased: 1.909
batch start
#iterations: 87
currently lose_sum: 96.19604724645615
time_elpased: 1.894
batch start
#iterations: 88
currently lose_sum: 95.4880268573761
time_elpased: 1.887
batch start
#iterations: 89
currently lose_sum: 96.25940579175949
time_elpased: 1.948
batch start
#iterations: 90
currently lose_sum: 96.74353629350662
time_elpased: 1.92
batch start
#iterations: 91
currently lose_sum: 95.96489715576172
time_elpased: 1.941
batch start
#iterations: 92
currently lose_sum: 96.14553594589233
time_elpased: 1.928
batch start
#iterations: 93
currently lose_sum: 95.15902614593506
time_elpased: 1.894
batch start
#iterations: 94
currently lose_sum: 95.62606436014175
time_elpased: 1.92
batch start
#iterations: 95
currently lose_sum: 96.1799087524414
time_elpased: 1.946
batch start
#iterations: 96
currently lose_sum: 95.96666538715363
time_elpased: 1.929
batch start
#iterations: 97
currently lose_sum: 97.00211107730865
time_elpased: 1.921
batch start
#iterations: 98
currently lose_sum: 95.51947230100632
time_elpased: 1.913
batch start
#iterations: 99
currently lose_sum: 96.08183670043945
time_elpased: 1.924
start validation test
0.649432989691
0.665606542481
0.603066790162
0.632795205442
0.649514392705
61.562
batch start
#iterations: 100
currently lose_sum: 95.36480683088303
time_elpased: 1.949
batch start
#iterations: 101
currently lose_sum: 96.29278761148453
time_elpased: 1.932
batch start
#iterations: 102
currently lose_sum: 95.65890562534332
time_elpased: 1.892
batch start
#iterations: 103
currently lose_sum: 95.45922613143921
time_elpased: 1.946
batch start
#iterations: 104
currently lose_sum: 96.36876845359802
time_elpased: 1.965
batch start
#iterations: 105
currently lose_sum: 95.32662743330002
time_elpased: 1.948
batch start
#iterations: 106
currently lose_sum: 96.04402816295624
time_elpased: 1.88
batch start
#iterations: 107
currently lose_sum: 95.91768485307693
time_elpased: 1.927
batch start
#iterations: 108
currently lose_sum: 95.32068753242493
time_elpased: 1.887
batch start
#iterations: 109
currently lose_sum: 95.63153457641602
time_elpased: 1.952
batch start
#iterations: 110
currently lose_sum: 95.83104628324509
time_elpased: 1.881
batch start
#iterations: 111
currently lose_sum: 95.52121692895889
time_elpased: 1.916
batch start
#iterations: 112
currently lose_sum: 95.1989775300026
time_elpased: 1.91
batch start
#iterations: 113
currently lose_sum: 95.82449680566788
time_elpased: 1.94
batch start
#iterations: 114
currently lose_sum: 95.51796400547028
time_elpased: 1.936
batch start
#iterations: 115
currently lose_sum: 95.33455842733383
time_elpased: 1.951
batch start
#iterations: 116
currently lose_sum: 96.3961935043335
time_elpased: 1.924
batch start
#iterations: 117
currently lose_sum: 95.55581468343735
time_elpased: 1.9
batch start
#iterations: 118
currently lose_sum: 95.71232008934021
time_elpased: 1.909
batch start
#iterations: 119
currently lose_sum: 95.45471829175949
time_elpased: 1.899
start validation test
0.662989690722
0.641805691855
0.740351960482
0.687565707732
0.662853869325
60.653
batch start
#iterations: 120
currently lose_sum: 95.93006122112274
time_elpased: 1.907
batch start
#iterations: 121
currently lose_sum: 95.33805042505264
time_elpased: 1.932
batch start
#iterations: 122
currently lose_sum: 95.53182262182236
time_elpased: 1.93
batch start
#iterations: 123
currently lose_sum: 95.79109877347946
time_elpased: 1.934
batch start
#iterations: 124
currently lose_sum: 95.97465002536774
time_elpased: 1.892
batch start
#iterations: 125
currently lose_sum: 95.34834289550781
time_elpased: 1.937
batch start
#iterations: 126
currently lose_sum: 95.81138730049133
time_elpased: 1.946
batch start
#iterations: 127
currently lose_sum: 95.37266248464584
time_elpased: 1.947
batch start
#iterations: 128
currently lose_sum: 95.70268034934998
time_elpased: 1.964
batch start
#iterations: 129
currently lose_sum: 95.28662765026093
time_elpased: 1.901
batch start
#iterations: 130
currently lose_sum: 95.47968727350235
time_elpased: 1.916
batch start
#iterations: 131
currently lose_sum: 95.0284383893013
time_elpased: 1.97
batch start
#iterations: 132
currently lose_sum: 96.16466850042343
time_elpased: 1.948
batch start
#iterations: 133
currently lose_sum: 96.3020601272583
time_elpased: 1.907
batch start
#iterations: 134
currently lose_sum: 95.39178043603897
time_elpased: 1.882
batch start
#iterations: 135
currently lose_sum: 94.71874445676804
time_elpased: 1.976
batch start
#iterations: 136
currently lose_sum: 95.0747002363205
time_elpased: 1.946
batch start
#iterations: 137
currently lose_sum: 96.46092092990875
time_elpased: 1.905
batch start
#iterations: 138
currently lose_sum: 94.6163609623909
time_elpased: 1.919
batch start
#iterations: 139
currently lose_sum: 94.80828356742859
time_elpased: 1.927
start validation test
0.652319587629
0.664927857936
0.616548317382
0.639824851818
0.652382389611
61.484
batch start
#iterations: 140
currently lose_sum: 95.90432685613632
time_elpased: 2.04
batch start
#iterations: 141
currently lose_sum: 94.8025518655777
time_elpased: 1.869
batch start
#iterations: 142
currently lose_sum: 95.45577448606491
time_elpased: 1.91
batch start
#iterations: 143
currently lose_sum: 94.57059401273727
time_elpased: 1.933
batch start
#iterations: 144
currently lose_sum: 95.0216993689537
time_elpased: 1.931
batch start
#iterations: 145
currently lose_sum: 94.78595036268234
time_elpased: 1.961
batch start
#iterations: 146
currently lose_sum: 95.14213693141937
time_elpased: 1.942
batch start
#iterations: 147
currently lose_sum: 95.27758371829987
time_elpased: 1.935
batch start
#iterations: 148
currently lose_sum: 95.62758469581604
time_elpased: 2.022
batch start
#iterations: 149
currently lose_sum: 94.82051837444305
time_elpased: 1.921
batch start
#iterations: 150
currently lose_sum: 94.3715028166771
time_elpased: 1.918
batch start
#iterations: 151
currently lose_sum: 95.4245445728302
time_elpased: 1.894
batch start
#iterations: 152
currently lose_sum: 95.39791804552078
time_elpased: 1.937
batch start
#iterations: 153
currently lose_sum: 94.97241562604904
time_elpased: 1.928
batch start
#iterations: 154
currently lose_sum: 95.80558758974075
time_elpased: 1.944
batch start
#iterations: 155
currently lose_sum: 95.44983154535294
time_elpased: 1.917
batch start
#iterations: 156
currently lose_sum: 95.25422084331512
time_elpased: 1.917
batch start
#iterations: 157
currently lose_sum: 95.57555675506592
time_elpased: 1.926
batch start
#iterations: 158
currently lose_sum: 95.17986446619034
time_elpased: 1.911
batch start
#iterations: 159
currently lose_sum: 95.08261835575104
time_elpased: 1.932
start validation test
0.651701030928
0.607997664915
0.857466296182
0.711498228086
0.651339778268
61.775
batch start
#iterations: 160
currently lose_sum: 95.20937603712082
time_elpased: 2.091
batch start
#iterations: 161
currently lose_sum: 94.95514714717865
time_elpased: 1.895
batch start
#iterations: 162
currently lose_sum: 95.6027837395668
time_elpased: 1.91
batch start
#iterations: 163
currently lose_sum: 94.25375127792358
time_elpased: 1.926
batch start
#iterations: 164
currently lose_sum: 94.79757577180862
time_elpased: 1.946
batch start
#iterations: 165
currently lose_sum: 94.98915481567383
time_elpased: 1.937
batch start
#iterations: 166
currently lose_sum: 94.70673257112503
time_elpased: 1.965
batch start
#iterations: 167
currently lose_sum: 94.8283799290657
time_elpased: 1.963
batch start
#iterations: 168
currently lose_sum: 94.59541845321655
time_elpased: 1.986
batch start
#iterations: 169
currently lose_sum: 94.84339565038681
time_elpased: 1.934
batch start
#iterations: 170
currently lose_sum: 97.44050878286362
time_elpased: 1.93
batch start
#iterations: 171
currently lose_sum: 97.16386264562607
time_elpased: 1.974
batch start
#iterations: 172
currently lose_sum: 94.62998354434967
time_elpased: 1.954
batch start
#iterations: 173
currently lose_sum: 94.6964339017868
time_elpased: 1.913
batch start
#iterations: 174
currently lose_sum: 96.02276265621185
time_elpased: 1.928
batch start
#iterations: 175
currently lose_sum: 94.05502891540527
time_elpased: 1.933
batch start
#iterations: 176
currently lose_sum: 94.26308703422546
time_elpased: 1.912
batch start
#iterations: 177
currently lose_sum: 93.9977599978447
time_elpased: 1.956
batch start
#iterations: 178
currently lose_sum: 94.70497381687164
time_elpased: 1.95
batch start
#iterations: 179
currently lose_sum: 95.15893816947937
time_elpased: 1.939
start validation test
0.638505154639
0.593357271095
0.884326438201
0.710194636142
0.638073577461
62.275
batch start
#iterations: 180
currently lose_sum: 95.23980098962784
time_elpased: 1.946
batch start
#iterations: 181
currently lose_sum: 93.99130839109421
time_elpased: 1.956
batch start
#iterations: 182
currently lose_sum: 94.8111190199852
time_elpased: 1.941
batch start
#iterations: 183
currently lose_sum: 94.05232560634613
time_elpased: 1.978
batch start
#iterations: 184
currently lose_sum: 95.9685737490654
time_elpased: 2.037
batch start
#iterations: 185
currently lose_sum: 94.56124019622803
time_elpased: 1.983
batch start
#iterations: 186
currently lose_sum: 94.53389686346054
time_elpased: 2.038
batch start
#iterations: 187
currently lose_sum: 95.58164536952972
time_elpased: 1.945
batch start
#iterations: 188
currently lose_sum: 93.93432748317719
time_elpased: 1.931
batch start
#iterations: 189
currently lose_sum: 94.64927285909653
time_elpased: 1.957
batch start
#iterations: 190
currently lose_sum: 94.94505482912064
time_elpased: 1.947
batch start
#iterations: 191
currently lose_sum: 94.21238946914673
time_elpased: 1.949
batch start
#iterations: 192
currently lose_sum: 94.65670883655548
time_elpased: 1.955
batch start
#iterations: 193
currently lose_sum: 95.0109310746193
time_elpased: 1.966
batch start
#iterations: 194
currently lose_sum: 94.91199296712875
time_elpased: 1.945
batch start
#iterations: 195
currently lose_sum: 94.45026862621307
time_elpased: 1.965
batch start
#iterations: 196
currently lose_sum: 94.67364555597305
time_elpased: 1.959
batch start
#iterations: 197
currently lose_sum: 94.71374100446701
time_elpased: 1.946
batch start
#iterations: 198
currently lose_sum: 94.50670033693314
time_elpased: 1.944
batch start
#iterations: 199
currently lose_sum: 93.72396314144135
time_elpased: 1.939
start validation test
0.664329896907
0.641102403804
0.749305341155
0.690993641454
0.664180709408
60.715
batch start
#iterations: 200
currently lose_sum: 94.44476008415222
time_elpased: 1.961
batch start
#iterations: 201
currently lose_sum: 94.22166681289673
time_elpased: 1.932
batch start
#iterations: 202
currently lose_sum: 93.99396884441376
time_elpased: 1.944
batch start
#iterations: 203
currently lose_sum: 94.36880791187286
time_elpased: 1.948
batch start
#iterations: 204
currently lose_sum: 95.09941560029984
time_elpased: 1.974
batch start
#iterations: 205
currently lose_sum: 94.17964047193527
time_elpased: 1.924
batch start
#iterations: 206
currently lose_sum: 94.8854667544365
time_elpased: 1.988
batch start
#iterations: 207
currently lose_sum: 94.3553159236908
time_elpased: 1.952
batch start
#iterations: 208
currently lose_sum: 93.53467690944672
time_elpased: 1.983
batch start
#iterations: 209
currently lose_sum: 93.87563502788544
time_elpased: 1.945
batch start
#iterations: 210
currently lose_sum: 94.57982993125916
time_elpased: 1.939
batch start
#iterations: 211
currently lose_sum: 93.62354928255081
time_elpased: 1.954
batch start
#iterations: 212
currently lose_sum: 94.05414009094238
time_elpased: 1.95
batch start
#iterations: 213
currently lose_sum: 94.36868804693222
time_elpased: 1.955
batch start
#iterations: 214
currently lose_sum: 94.10378104448318
time_elpased: 1.93
batch start
#iterations: 215
currently lose_sum: 94.26653516292572
time_elpased: 1.96
batch start
#iterations: 216
currently lose_sum: 93.85832381248474
time_elpased: 1.939
batch start
#iterations: 217
currently lose_sum: 94.67439550161362
time_elpased: 1.918
batch start
#iterations: 218
currently lose_sum: 94.57687622308731
time_elpased: 1.926
batch start
#iterations: 219
currently lose_sum: 94.02587485313416
time_elpased: 1.919
start validation test
0.63175257732
0.587037412895
0.89297108161
0.708384357907
0.631293967945
62.204
batch start
#iterations: 220
currently lose_sum: 93.87424695491791
time_elpased: 1.941
batch start
#iterations: 221
currently lose_sum: 94.64675676822662
time_elpased: 1.927
batch start
#iterations: 222
currently lose_sum: 93.89245676994324
time_elpased: 1.92
batch start
#iterations: 223
currently lose_sum: 93.7830758690834
time_elpased: 1.943
batch start
#iterations: 224
currently lose_sum: 93.49982714653015
time_elpased: 1.922
batch start
#iterations: 225
currently lose_sum: 94.11116510629654
time_elpased: 1.955
batch start
#iterations: 226
currently lose_sum: 94.49409598112106
time_elpased: 1.913
batch start
#iterations: 227
currently lose_sum: 93.42009925842285
time_elpased: 1.946
batch start
#iterations: 228
currently lose_sum: 94.48702436685562
time_elpased: 1.949
batch start
#iterations: 229
currently lose_sum: 92.79052144289017
time_elpased: 1.968
batch start
#iterations: 230
currently lose_sum: 93.24543070793152
time_elpased: 1.942
batch start
#iterations: 231
currently lose_sum: 94.2496343255043
time_elpased: 1.94
batch start
#iterations: 232
currently lose_sum: 93.83431029319763
time_elpased: 1.937
batch start
#iterations: 233
currently lose_sum: 93.48757421970367
time_elpased: 1.957
batch start
#iterations: 234
currently lose_sum: 94.61557751893997
time_elpased: 1.94
batch start
#iterations: 235
currently lose_sum: 93.31573641300201
time_elpased: 1.974
batch start
#iterations: 236
currently lose_sum: 93.99090111255646
time_elpased: 1.936
batch start
#iterations: 237
currently lose_sum: 93.69657051563263
time_elpased: 1.917
batch start
#iterations: 238
currently lose_sum: 95.53066122531891
time_elpased: 1.992
batch start
#iterations: 239
currently lose_sum: 93.89512652158737
time_elpased: 1.909
start validation test
0.662113402062
0.623670212766
0.820520736853
0.708679614239
0.661835293553
60.835
batch start
#iterations: 240
currently lose_sum: 93.93253546953201
time_elpased: 1.927
batch start
#iterations: 241
currently lose_sum: 93.6903470158577
time_elpased: 1.943
batch start
#iterations: 242
currently lose_sum: 93.84130030870438
time_elpased: 1.948
batch start
#iterations: 243
currently lose_sum: 94.60284370183945
time_elpased: 1.905
batch start
#iterations: 244
currently lose_sum: 93.28800451755524
time_elpased: 1.96
batch start
#iterations: 245
currently lose_sum: 93.42593932151794
time_elpased: 1.945
batch start
#iterations: 246
currently lose_sum: 93.50918406248093
time_elpased: 1.935
batch start
#iterations: 247
currently lose_sum: 94.10409951210022
time_elpased: 1.943
batch start
#iterations: 248
currently lose_sum: 93.46175765991211
time_elpased: 1.926
batch start
#iterations: 249
currently lose_sum: 94.24136584997177
time_elpased: 1.961
batch start
#iterations: 250
currently lose_sum: 93.5898848772049
time_elpased: 1.93
batch start
#iterations: 251
currently lose_sum: 93.86479038000107
time_elpased: 1.935
batch start
#iterations: 252
currently lose_sum: 92.85173988342285
time_elpased: 1.979
batch start
#iterations: 253
currently lose_sum: 93.19043374061584
time_elpased: 1.93
batch start
#iterations: 254
currently lose_sum: 92.70734560489655
time_elpased: 1.932
batch start
#iterations: 255
currently lose_sum: 93.03779590129852
time_elpased: 1.934
batch start
#iterations: 256
currently lose_sum: 93.47255319356918
time_elpased: 1.927
batch start
#iterations: 257
currently lose_sum: 93.15297031402588
time_elpased: 1.932
batch start
#iterations: 258
currently lose_sum: 93.46351683139801
time_elpased: 1.949
batch start
#iterations: 259
currently lose_sum: 94.39290833473206
time_elpased: 1.966
start validation test
0.63118556701
0.584231983167
0.914376865288
0.712938816449
0.630688381007
62.735
batch start
#iterations: 260
currently lose_sum: 93.35656523704529
time_elpased: 1.985
batch start
#iterations: 261
currently lose_sum: 93.2843845486641
time_elpased: 1.932
batch start
#iterations: 262
currently lose_sum: 92.26073914766312
time_elpased: 1.962
batch start
#iterations: 263
currently lose_sum: 93.03442627191544
time_elpased: 1.915
batch start
#iterations: 264
currently lose_sum: 93.56829577684402
time_elpased: 1.999
batch start
#iterations: 265
currently lose_sum: 93.76600861549377
time_elpased: 1.943
batch start
#iterations: 266
currently lose_sum: 93.24710458517075
time_elpased: 1.954
batch start
#iterations: 267
currently lose_sum: 93.09762614965439
time_elpased: 1.951
batch start
#iterations: 268
currently lose_sum: 92.77019900083542
time_elpased: 1.956
batch start
#iterations: 269
currently lose_sum: 93.67349743843079
time_elpased: 1.907
batch start
#iterations: 270
currently lose_sum: 93.23951822519302
time_elpased: 1.953
batch start
#iterations: 271
currently lose_sum: 93.61040145158768
time_elpased: 1.938
batch start
#iterations: 272
currently lose_sum: 93.21637123823166
time_elpased: 1.955
batch start
#iterations: 273
currently lose_sum: 92.31942331790924
time_elpased: 1.937
batch start
#iterations: 274
currently lose_sum: 93.67046993970871
time_elpased: 1.945
batch start
#iterations: 275
currently lose_sum: 93.5471601486206
time_elpased: 1.947
batch start
#iterations: 276
currently lose_sum: 92.62347728013992
time_elpased: 1.956
batch start
#iterations: 277
currently lose_sum: 93.63211262226105
time_elpased: 1.93
batch start
#iterations: 278
currently lose_sum: 92.55050492286682
time_elpased: 1.928
batch start
#iterations: 279
currently lose_sum: 93.22134721279144
time_elpased: 1.909
start validation test
0.670567010309
0.638884249207
0.787280024699
0.705361670739
0.670362102611
60.130
batch start
#iterations: 280
currently lose_sum: 92.79629343748093
time_elpased: 1.955
batch start
#iterations: 281
currently lose_sum: 93.51076364517212
time_elpased: 1.927
batch start
#iterations: 282
currently lose_sum: 92.99529659748077
time_elpased: 1.952
batch start
#iterations: 283
currently lose_sum: 93.01160979270935
time_elpased: 1.94
batch start
#iterations: 284
currently lose_sum: 93.62872910499573
time_elpased: 1.933
batch start
#iterations: 285
currently lose_sum: 92.97101348638535
time_elpased: 1.905
batch start
#iterations: 286
currently lose_sum: 92.45615738630295
time_elpased: 1.981
batch start
#iterations: 287
currently lose_sum: 92.85226917266846
time_elpased: 1.976
batch start
#iterations: 288
currently lose_sum: 92.78623348474503
time_elpased: 1.948
batch start
#iterations: 289
currently lose_sum: 93.32330071926117
time_elpased: 1.896
batch start
#iterations: 290
currently lose_sum: 93.06324726343155
time_elpased: 1.927
batch start
#iterations: 291
currently lose_sum: 92.83417689800262
time_elpased: 1.904
batch start
#iterations: 292
currently lose_sum: 92.83212047815323
time_elpased: 1.935
batch start
#iterations: 293
currently lose_sum: 94.02894580364227
time_elpased: 1.9
batch start
#iterations: 294
currently lose_sum: 92.41770523786545
time_elpased: 1.956
batch start
#iterations: 295
currently lose_sum: 92.92340511083603
time_elpased: 1.903
batch start
#iterations: 296
currently lose_sum: 93.11860120296478
time_elpased: 1.952
batch start
#iterations: 297
currently lose_sum: 92.7958300113678
time_elpased: 1.922
batch start
#iterations: 298
currently lose_sum: 92.47383886575699
time_elpased: 1.925
batch start
#iterations: 299
currently lose_sum: 92.40406060218811
time_elpased: 1.898
start validation test
0.621804123711
0.693181818182
0.43943603993
0.537884990867
0.622124299011
64.374
batch start
#iterations: 300
currently lose_sum: 91.89495295286179
time_elpased: 1.918
batch start
#iterations: 301
currently lose_sum: 92.45478820800781
time_elpased: 1.929
batch start
#iterations: 302
currently lose_sum: 92.8194128870964
time_elpased: 1.95
batch start
#iterations: 303
currently lose_sum: 93.30053442716599
time_elpased: 1.946
batch start
#iterations: 304
currently lose_sum: 92.57145237922668
time_elpased: 1.966
batch start
#iterations: 305
currently lose_sum: 92.72352635860443
time_elpased: 1.911
batch start
#iterations: 306
currently lose_sum: 93.36470484733582
time_elpased: 1.934
batch start
#iterations: 307
currently lose_sum: 92.00364416837692
time_elpased: 1.951
batch start
#iterations: 308
currently lose_sum: 92.21421825885773
time_elpased: 1.967
batch start
#iterations: 309
currently lose_sum: 93.46110826730728
time_elpased: 1.924
batch start
#iterations: 310
currently lose_sum: 92.89777559041977
time_elpased: 1.898
batch start
#iterations: 311
currently lose_sum: 91.75248754024506
time_elpased: 1.922
batch start
#iterations: 312
currently lose_sum: 92.36534655094147
time_elpased: 1.955
batch start
#iterations: 313
currently lose_sum: 93.13628494739532
time_elpased: 1.932
batch start
#iterations: 314
currently lose_sum: 91.50499820709229
time_elpased: 1.907
batch start
#iterations: 315
currently lose_sum: 93.09699952602386
time_elpased: 1.918
batch start
#iterations: 316
currently lose_sum: 92.40251618623734
time_elpased: 1.91
batch start
#iterations: 317
currently lose_sum: 92.37475287914276
time_elpased: 1.898
batch start
#iterations: 318
currently lose_sum: 92.18122911453247
time_elpased: 1.896
batch start
#iterations: 319
currently lose_sum: 92.85238325595856
time_elpased: 1.9
start validation test
0.668762886598
0.655750118315
0.712977256355
0.683167340499
0.668685261452
59.872
batch start
#iterations: 320
currently lose_sum: 92.87783175706863
time_elpased: 1.914
batch start
#iterations: 321
currently lose_sum: 92.05114138126373
time_elpased: 1.95
batch start
#iterations: 322
currently lose_sum: 92.03245609998703
time_elpased: 1.955
batch start
#iterations: 323
currently lose_sum: 92.82622504234314
time_elpased: 1.921
batch start
#iterations: 324
currently lose_sum: 92.13611394166946
time_elpased: 1.935
batch start
#iterations: 325
currently lose_sum: 91.8519561290741
time_elpased: 1.936
batch start
#iterations: 326
currently lose_sum: 91.78540575504303
time_elpased: 1.958
batch start
#iterations: 327
currently lose_sum: 91.65628957748413
time_elpased: 1.949
batch start
#iterations: 328
currently lose_sum: 92.23217058181763
time_elpased: 1.915
batch start
#iterations: 329
currently lose_sum: 92.73798477649689
time_elpased: 1.956
batch start
#iterations: 330
currently lose_sum: 92.3915074467659
time_elpased: 1.937
batch start
#iterations: 331
currently lose_sum: 91.25710475444794
time_elpased: 1.942
batch start
#iterations: 332
currently lose_sum: 92.27955412864685
time_elpased: 1.947
batch start
#iterations: 333
currently lose_sum: 92.00532454252243
time_elpased: 1.927
batch start
#iterations: 334
currently lose_sum: 92.11164110898972
time_elpased: 1.937
batch start
#iterations: 335
currently lose_sum: 91.35006493330002
time_elpased: 1.934
batch start
#iterations: 336
currently lose_sum: 92.27723497152328
time_elpased: 1.911
batch start
#iterations: 337
currently lose_sum: 92.1325341463089
time_elpased: 1.941
batch start
#iterations: 338
currently lose_sum: 92.13545316457748
time_elpased: 1.99
batch start
#iterations: 339
currently lose_sum: 92.44798272848129
time_elpased: 1.938
start validation test
0.60324742268
0.566056245912
0.890707008336
0.692206182269
0.60274274304
64.181
batch start
#iterations: 340
currently lose_sum: 91.85336756706238
time_elpased: 1.929
batch start
#iterations: 341
currently lose_sum: 92.02561831474304
time_elpased: 1.944
batch start
#iterations: 342
currently lose_sum: 91.53429055213928
time_elpased: 1.955
batch start
#iterations: 343
currently lose_sum: 91.23652464151382
time_elpased: 2.003
batch start
#iterations: 344
currently lose_sum: 91.62283569574356
time_elpased: 1.937
batch start
#iterations: 345
currently lose_sum: 91.99277240037918
time_elpased: 1.948
batch start
#iterations: 346
currently lose_sum: 92.6168954372406
time_elpased: 1.935
batch start
#iterations: 347
currently lose_sum: 91.11921101808548
time_elpased: 1.962
batch start
#iterations: 348
currently lose_sum: 92.12351274490356
time_elpased: 1.945
batch start
#iterations: 349
currently lose_sum: 91.86790019273758
time_elpased: 1.964
batch start
#iterations: 350
currently lose_sum: 91.24644297361374
time_elpased: 1.944
batch start
#iterations: 351
currently lose_sum: 91.80462163686752
time_elpased: 1.928
batch start
#iterations: 352
currently lose_sum: 91.71514213085175
time_elpased: 1.952
batch start
#iterations: 353
currently lose_sum: 92.35026228427887
time_elpased: 1.938
batch start
#iterations: 354
currently lose_sum: 92.11715483665466
time_elpased: 1.956
batch start
#iterations: 355
currently lose_sum: 91.65458768606186
time_elpased: 1.928
batch start
#iterations: 356
currently lose_sum: 91.5021932721138
time_elpased: 1.985
batch start
#iterations: 357
currently lose_sum: 92.03818702697754
time_elpased: 1.938
batch start
#iterations: 358
currently lose_sum: 91.8035894036293
time_elpased: 1.942
batch start
#iterations: 359
currently lose_sum: 91.90574771165848
time_elpased: 1.936
start validation test
0.642525773196
0.607016464071
0.811979005866
0.694695135373
0.64222827191
61.413
batch start
#iterations: 360
currently lose_sum: 91.87580865621567
time_elpased: 1.93
batch start
#iterations: 361
currently lose_sum: 92.34480077028275
time_elpased: 1.928
batch start
#iterations: 362
currently lose_sum: 90.65125578641891
time_elpased: 1.923
batch start
#iterations: 363
currently lose_sum: 92.64830476045609
time_elpased: 1.991
batch start
#iterations: 364
currently lose_sum: 92.20109927654266
time_elpased: 1.991
batch start
#iterations: 365
currently lose_sum: 91.10355067253113
time_elpased: 1.941
batch start
#iterations: 366
currently lose_sum: 92.28039234876633
time_elpased: 1.937
batch start
#iterations: 367
currently lose_sum: 91.64684635400772
time_elpased: 1.982
batch start
#iterations: 368
currently lose_sum: 91.07550185918808
time_elpased: 1.933
batch start
#iterations: 369
currently lose_sum: 91.80226916074753
time_elpased: 1.942
batch start
#iterations: 370
currently lose_sum: 91.39715874195099
time_elpased: 1.927
batch start
#iterations: 371
currently lose_sum: 93.30790573358536
time_elpased: 1.914
batch start
#iterations: 372
currently lose_sum: 91.7302617430687
time_elpased: 1.928
batch start
#iterations: 373
currently lose_sum: 91.2086865901947
time_elpased: 1.935
batch start
#iterations: 374
currently lose_sum: 91.62413173913956
time_elpased: 1.937
batch start
#iterations: 375
currently lose_sum: 91.95087212324142
time_elpased: 1.956
batch start
#iterations: 376
currently lose_sum: 90.932488322258
time_elpased: 1.917
batch start
#iterations: 377
currently lose_sum: 91.66560447216034
time_elpased: 1.935
batch start
#iterations: 378
currently lose_sum: 91.83432507514954
time_elpased: 1.912
batch start
#iterations: 379
currently lose_sum: 91.53509217500687
time_elpased: 1.931
start validation test
0.664639175258
0.655526494236
0.696408356489
0.675349301397
0.66458339956
59.811
batch start
#iterations: 380
currently lose_sum: 91.62889063358307
time_elpased: 1.938
batch start
#iterations: 381
currently lose_sum: 91.01608180999756
time_elpased: 1.919
batch start
#iterations: 382
currently lose_sum: 91.09470015764236
time_elpased: 1.958
batch start
#iterations: 383
currently lose_sum: 90.80761128664017
time_elpased: 2.001
batch start
#iterations: 384
currently lose_sum: 91.50867021083832
time_elpased: 2.007
batch start
#iterations: 385
currently lose_sum: 91.90042287111282
time_elpased: 1.929
batch start
#iterations: 386
currently lose_sum: 91.10026633739471
time_elpased: 1.99
batch start
#iterations: 387
currently lose_sum: 91.20587289333344
time_elpased: 1.918
batch start
#iterations: 388
currently lose_sum: 91.04286509752274
time_elpased: 1.964
batch start
#iterations: 389
currently lose_sum: 91.63802886009216
time_elpased: 1.938
batch start
#iterations: 390
currently lose_sum: 91.70952397584915
time_elpased: 1.975
batch start
#iterations: 391
currently lose_sum: 91.30785471200943
time_elpased: 1.931
batch start
#iterations: 392
currently lose_sum: 91.58098196983337
time_elpased: 1.932
batch start
#iterations: 393
currently lose_sum: 91.36605101823807
time_elpased: 1.937
batch start
#iterations: 394
currently lose_sum: 91.68369656801224
time_elpased: 1.959
batch start
#iterations: 395
currently lose_sum: 91.55030119419098
time_elpased: 1.926
batch start
#iterations: 396
currently lose_sum: 90.61121189594269
time_elpased: 1.933
batch start
#iterations: 397
currently lose_sum: 90.7577840089798
time_elpased: 1.935
batch start
#iterations: 398
currently lose_sum: 91.2000064253807
time_elpased: 1.957
batch start
#iterations: 399
currently lose_sum: 90.98737215995789
time_elpased: 1.921
start validation test
0.665154639175
0.653776377351
0.704641350211
0.678256562655
0.665085314164
59.631
acc: 0.665
pre: 0.655
rec: 0.699
F1: 0.676
auc: 0.665
