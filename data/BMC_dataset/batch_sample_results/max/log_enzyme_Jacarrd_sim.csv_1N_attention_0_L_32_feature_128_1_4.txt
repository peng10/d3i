start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.83896315097809
time_elpased: 2.973
batch start
#iterations: 1
currently lose_sum: 100.20686602592468
time_elpased: 2.792
batch start
#iterations: 2
currently lose_sum: 99.99384117126465
time_elpased: 2.59
batch start
#iterations: 3
currently lose_sum: 99.75211781263351
time_elpased: 2.634
batch start
#iterations: 4
currently lose_sum: 99.6888496875763
time_elpased: 2.728
batch start
#iterations: 5
currently lose_sum: 99.44479393959045
time_elpased: 2.93
batch start
#iterations: 6
currently lose_sum: 99.35945898294449
time_elpased: 2.893
batch start
#iterations: 7
currently lose_sum: 99.40039330720901
time_elpased: 3.156
batch start
#iterations: 8
currently lose_sum: 99.40380948781967
time_elpased: 2.889
batch start
#iterations: 9
currently lose_sum: 99.13813984394073
time_elpased: 2.766
batch start
#iterations: 10
currently lose_sum: 99.17331981658936
time_elpased: 2.877
batch start
#iterations: 11
currently lose_sum: 99.28946733474731
time_elpased: 2.973
batch start
#iterations: 12
currently lose_sum: 98.83542293310165
time_elpased: 2.085
batch start
#iterations: 13
currently lose_sum: 99.13497513532639
time_elpased: 2.97
batch start
#iterations: 14
currently lose_sum: 98.71802896261215
time_elpased: 2.957
batch start
#iterations: 15
currently lose_sum: 98.91944909095764
time_elpased: 2.903
batch start
#iterations: 16
currently lose_sum: 98.76411002874374
time_elpased: 2.957
batch start
#iterations: 17
currently lose_sum: 98.77966237068176
time_elpased: 2.467
batch start
#iterations: 18
currently lose_sum: 98.51593613624573
time_elpased: 2.9
batch start
#iterations: 19
currently lose_sum: 98.69727998971939
time_elpased: 2.578
start validation test
0.591597938144
0.596952010376
0.568385304106
0.58231851969
0.591638691504
64.919
batch start
#iterations: 20
currently lose_sum: 98.67837059497833
time_elpased: 2.231
batch start
#iterations: 21
currently lose_sum: 98.38808286190033
time_elpased: 2.957
batch start
#iterations: 22
currently lose_sum: 98.40460079908371
time_elpased: 3.039
batch start
#iterations: 23
currently lose_sum: 98.70713251829147
time_elpased: 2.832
batch start
#iterations: 24
currently lose_sum: 98.28979843854904
time_elpased: 3.089
batch start
#iterations: 25
currently lose_sum: 98.3310159444809
time_elpased: 2.937
batch start
#iterations: 26
currently lose_sum: 98.4941885471344
time_elpased: 2.895
batch start
#iterations: 27
currently lose_sum: 98.08149200677872
time_elpased: 2.957
batch start
#iterations: 28
currently lose_sum: 97.90187180042267
time_elpased: 2.909
batch start
#iterations: 29
currently lose_sum: 97.8438064455986
time_elpased: 2.766
batch start
#iterations: 30
currently lose_sum: 98.24018681049347
time_elpased: 2.96
batch start
#iterations: 31
currently lose_sum: 97.82212471961975
time_elpased: 2.902
batch start
#iterations: 32
currently lose_sum: 97.93743932247162
time_elpased: 2.869
batch start
#iterations: 33
currently lose_sum: 97.6847243309021
time_elpased: 2.633
batch start
#iterations: 34
currently lose_sum: 97.73162072896957
time_elpased: 3.111
batch start
#iterations: 35
currently lose_sum: 98.01988798379898
time_elpased: 2.884
batch start
#iterations: 36
currently lose_sum: 97.66566932201385
time_elpased: 2.946
batch start
#iterations: 37
currently lose_sum: 97.69919544458389
time_elpased: 2.926
batch start
#iterations: 38
currently lose_sum: 97.77473539113998
time_elpased: 3.113
batch start
#iterations: 39
currently lose_sum: 97.5436259508133
time_elpased: 2.864
start validation test
0.596907216495
0.614650066481
0.523309663476
0.565314063369
0.597036428351
64.856
batch start
#iterations: 40
currently lose_sum: 97.83459359407425
time_elpased: 2.627
batch start
#iterations: 41
currently lose_sum: 97.44325840473175
time_elpased: 2.91
batch start
#iterations: 42
currently lose_sum: 97.58904618024826
time_elpased: 2.881
batch start
#iterations: 43
currently lose_sum: 97.4335076212883
time_elpased: 2.531
batch start
#iterations: 44
currently lose_sum: 97.47661036252975
time_elpased: 2.839
batch start
#iterations: 45
currently lose_sum: 97.31255835294724
time_elpased: 2.72
batch start
#iterations: 46
currently lose_sum: 97.27494198083878
time_elpased: 2.799
batch start
#iterations: 47
currently lose_sum: 97.45113283395767
time_elpased: 2.752
batch start
#iterations: 48
currently lose_sum: 97.13171303272247
time_elpased: 2.644
batch start
#iterations: 49
currently lose_sum: 96.95563495159149
time_elpased: 2.791
batch start
#iterations: 50
currently lose_sum: 96.98695886135101
time_elpased: 2.414
batch start
#iterations: 51
currently lose_sum: 97.0475857257843
time_elpased: 2.822
batch start
#iterations: 52
currently lose_sum: 97.18754822015762
time_elpased: 2.718
batch start
#iterations: 53
currently lose_sum: 96.90689677000046
time_elpased: 2.805
batch start
#iterations: 54
currently lose_sum: 96.75128889083862
time_elpased: 2.54
batch start
#iterations: 55
currently lose_sum: 96.68996149301529
time_elpased: 2.987
batch start
#iterations: 56
currently lose_sum: 96.89451205730438
time_elpased: 2.958
batch start
#iterations: 57
currently lose_sum: 96.86606466770172
time_elpased: 2.915
batch start
#iterations: 58
currently lose_sum: 96.6101685166359
time_elpased: 2.622
batch start
#iterations: 59
currently lose_sum: 97.00291162729263
time_elpased: 2.667
start validation test
0.572474226804
0.625418649744
0.365133271586
0.461078622482
0.572838245831
65.671
batch start
#iterations: 60
currently lose_sum: 96.84866714477539
time_elpased: 2.567
batch start
#iterations: 61
currently lose_sum: 96.86762714385986
time_elpased: 2.457
batch start
#iterations: 62
currently lose_sum: 96.519455909729
time_elpased: 2.666
batch start
#iterations: 63
currently lose_sum: 96.18707692623138
time_elpased: 2.762
batch start
#iterations: 64
currently lose_sum: 96.58718585968018
time_elpased: 2.743
batch start
#iterations: 65
currently lose_sum: 96.31230080127716
time_elpased: 2.632
batch start
#iterations: 66
currently lose_sum: 96.77025234699249
time_elpased: 2.715
batch start
#iterations: 67
currently lose_sum: 96.01081508398056
time_elpased: 2.796
batch start
#iterations: 68
currently lose_sum: 96.54069155454636
time_elpased: 2.662
batch start
#iterations: 69
currently lose_sum: 96.17058289051056
time_elpased: 2.652
batch start
#iterations: 70
currently lose_sum: 96.26227760314941
time_elpased: 2.781
batch start
#iterations: 71
currently lose_sum: 96.24186313152313
time_elpased: 2.77
batch start
#iterations: 72
currently lose_sum: 95.79218941926956
time_elpased: 2.62
batch start
#iterations: 73
currently lose_sum: 96.25937712192535
time_elpased: 2.656
batch start
#iterations: 74
currently lose_sum: 95.69225990772247
time_elpased: 2.494
batch start
#iterations: 75
currently lose_sum: 96.34804213047028
time_elpased: 2.719
batch start
#iterations: 76
currently lose_sum: 95.8657791018486
time_elpased: 2.657
batch start
#iterations: 77
currently lose_sum: 96.05563455820084
time_elpased: 2.658
batch start
#iterations: 78
currently lose_sum: 95.9907956123352
time_elpased: 2.511
batch start
#iterations: 79
currently lose_sum: 95.73182260990143
time_elpased: 2.25
start validation test
0.587525773196
0.623898280595
0.444375836163
0.519052770766
0.58777709499
65.340
batch start
#iterations: 80
currently lose_sum: 95.71746867895126
time_elpased: 2.348
batch start
#iterations: 81
currently lose_sum: 95.73631012439728
time_elpased: 2.762
batch start
#iterations: 82
currently lose_sum: 95.66941982507706
time_elpased: 2.718
batch start
#iterations: 83
currently lose_sum: 95.43055152893066
time_elpased: 2.782
batch start
#iterations: 84
currently lose_sum: 95.55096989870071
time_elpased: 2.636
batch start
#iterations: 85
currently lose_sum: 95.6579778790474
time_elpased: 2.673
batch start
#iterations: 86
currently lose_sum: 95.49522107839584
time_elpased: 2.357
batch start
#iterations: 87
currently lose_sum: 95.38790619373322
time_elpased: 2.696
batch start
#iterations: 88
currently lose_sum: 95.40474772453308
time_elpased: 2.741
batch start
#iterations: 89
currently lose_sum: 95.42425668239594
time_elpased: 2.74
batch start
#iterations: 90
currently lose_sum: 94.99404233694077
time_elpased: 2.602
batch start
#iterations: 91
currently lose_sum: 95.44498753547668
time_elpased: 2.748
batch start
#iterations: 92
currently lose_sum: 95.32108551263809
time_elpased: 2.942
batch start
#iterations: 93
currently lose_sum: 94.72475814819336
time_elpased: 2.931
batch start
#iterations: 94
currently lose_sum: 95.41405361890793
time_elpased: 2.666
batch start
#iterations: 95
currently lose_sum: 95.22517597675323
time_elpased: 2.202
batch start
#iterations: 96
currently lose_sum: 94.90964835882187
time_elpased: 2.501
batch start
#iterations: 97
currently lose_sum: 94.9502694606781
time_elpased: 2.848
batch start
#iterations: 98
currently lose_sum: 95.2521812915802
time_elpased: 2.631
batch start
#iterations: 99
currently lose_sum: 95.07939392328262
time_elpased: 2.666
start validation test
0.585670103093
0.608983512917
0.482762169394
0.538576349024
0.585850773843
65.347
batch start
#iterations: 100
currently lose_sum: 94.83491480350494
time_elpased: 2.757
batch start
#iterations: 101
currently lose_sum: 94.740198969841
time_elpased: 2.758
batch start
#iterations: 102
currently lose_sum: 94.91707491874695
time_elpased: 2.686
batch start
#iterations: 103
currently lose_sum: 94.68849855661392
time_elpased: 2.494
batch start
#iterations: 104
currently lose_sum: 94.62987023591995
time_elpased: 2.814
batch start
#iterations: 105
currently lose_sum: 94.43827497959137
time_elpased: 2.677
batch start
#iterations: 106
currently lose_sum: 94.45453411340714
time_elpased: 2.673
batch start
#iterations: 107
currently lose_sum: 94.20308810472488
time_elpased: 2.689
batch start
#iterations: 108
currently lose_sum: 94.05622744560242
time_elpased: 2.81
batch start
#iterations: 109
currently lose_sum: 94.63693881034851
time_elpased: 2.655
batch start
#iterations: 110
currently lose_sum: 94.09985226392746
time_elpased: 2.654
batch start
#iterations: 111
currently lose_sum: 94.67303937673569
time_elpased: 2.725
batch start
#iterations: 112
currently lose_sum: 94.09805059432983
time_elpased: 2.73
batch start
#iterations: 113
currently lose_sum: 93.75864344835281
time_elpased: 2.73
batch start
#iterations: 114
currently lose_sum: 94.45880091190338
time_elpased: 2.691
batch start
#iterations: 115
currently lose_sum: 93.68742752075195
time_elpased: 2.375
batch start
#iterations: 116
currently lose_sum: 94.06887549161911
time_elpased: 2.574
batch start
#iterations: 117
currently lose_sum: 94.07995045185089
time_elpased: 2.825
batch start
#iterations: 118
currently lose_sum: 94.05096089839935
time_elpased: 2.753
batch start
#iterations: 119
currently lose_sum: 93.51619625091553
time_elpased: 2.746
start validation test
0.582783505155
0.615548910722
0.444890398271
0.516487455197
0.583025597772
66.139
batch start
#iterations: 120
currently lose_sum: 93.84312444925308
time_elpased: 2.757
batch start
#iterations: 121
currently lose_sum: 93.73534256219864
time_elpased: 2.622
batch start
#iterations: 122
currently lose_sum: 93.73385804891586
time_elpased: 2.669
batch start
#iterations: 123
currently lose_sum: 93.6753745675087
time_elpased: 2.878
batch start
#iterations: 124
currently lose_sum: 93.58758985996246
time_elpased: 2.807
batch start
#iterations: 125
currently lose_sum: 93.61929225921631
time_elpased: 2.727
batch start
#iterations: 126
currently lose_sum: 93.65427720546722
time_elpased: 2.874
batch start
#iterations: 127
currently lose_sum: 93.66826736927032
time_elpased: 2.961
batch start
#iterations: 128
currently lose_sum: 93.26830172538757
time_elpased: 2.905
batch start
#iterations: 129
currently lose_sum: 93.58123463392258
time_elpased: 3.001
batch start
#iterations: 130
currently lose_sum: 93.00360602140427
time_elpased: 2.789
batch start
#iterations: 131
currently lose_sum: 93.40150779485703
time_elpased: 2.722
batch start
#iterations: 132
currently lose_sum: 93.45742017030716
time_elpased: 2.874
batch start
#iterations: 133
currently lose_sum: 93.40719932317734
time_elpased: 2.663
batch start
#iterations: 134
currently lose_sum: 93.0866346359253
time_elpased: 2.69
batch start
#iterations: 135
currently lose_sum: 93.32179266214371
time_elpased: 2.71
batch start
#iterations: 136
currently lose_sum: 92.9720950126648
time_elpased: 2.774
batch start
#iterations: 137
currently lose_sum: 93.1075342297554
time_elpased: 2.641
batch start
#iterations: 138
currently lose_sum: 93.03451406955719
time_elpased: 2.559
batch start
#iterations: 139
currently lose_sum: 93.05691057443619
time_elpased: 2.757
start validation test
0.588144329897
0.605035883712
0.511886384687
0.554576875906
0.588278212482
65.882
batch start
#iterations: 140
currently lose_sum: 92.76070827245712
time_elpased: 2.713
batch start
#iterations: 141
currently lose_sum: 93.16337323188782
time_elpased: 2.672
batch start
#iterations: 142
currently lose_sum: 93.01260101795197
time_elpased: 2.765
batch start
#iterations: 143
currently lose_sum: 92.56160420179367
time_elpased: 2.763
batch start
#iterations: 144
currently lose_sum: 92.67618995904922
time_elpased: 2.665
batch start
#iterations: 145
currently lose_sum: 92.52397918701172
time_elpased: 2.655
batch start
#iterations: 146
currently lose_sum: 92.41767621040344
time_elpased: 2.785
batch start
#iterations: 147
currently lose_sum: 92.56505340337753
time_elpased: 2.793
batch start
#iterations: 148
currently lose_sum: 92.48918944597244
time_elpased: 2.678
batch start
#iterations: 149
currently lose_sum: 92.5680119395256
time_elpased: 2.554
batch start
#iterations: 150
currently lose_sum: 92.55225139856339
time_elpased: 2.566
batch start
#iterations: 151
currently lose_sum: 92.36283254623413
time_elpased: 2.277
batch start
#iterations: 152
currently lose_sum: 92.41149467229843
time_elpased: 2.794
batch start
#iterations: 153
currently lose_sum: 92.69695091247559
time_elpased: 2.777
batch start
#iterations: 154
currently lose_sum: 91.7822242975235
time_elpased: 2.749
batch start
#iterations: 155
currently lose_sum: 92.00433886051178
time_elpased: 2.757
batch start
#iterations: 156
currently lose_sum: 92.7424823641777
time_elpased: 2.66
batch start
#iterations: 157
currently lose_sum: 92.31792271137238
time_elpased: 2.385
batch start
#iterations: 158
currently lose_sum: 92.39573192596436
time_elpased: 2.841
batch start
#iterations: 159
currently lose_sum: 92.44187957048416
time_elpased: 2.843
start validation test
0.572010309278
0.604369648657
0.421323453741
0.496513249651
0.572274863295
67.581
batch start
#iterations: 160
currently lose_sum: 92.22226071357727
time_elpased: 2.275
batch start
#iterations: 161
currently lose_sum: 91.94385796785355
time_elpased: 2.6
batch start
#iterations: 162
currently lose_sum: 92.2903373837471
time_elpased: 2.876
batch start
#iterations: 163
currently lose_sum: 91.95261991024017
time_elpased: 2.916
batch start
#iterations: 164
currently lose_sum: 92.21021610498428
time_elpased: 2.978
batch start
#iterations: 165
currently lose_sum: 91.4259158372879
time_elpased: 2.883
batch start
#iterations: 166
currently lose_sum: 91.47594481706619
time_elpased: 2.41
batch start
#iterations: 167
currently lose_sum: 91.64849096536636
time_elpased: 2.238
batch start
#iterations: 168
currently lose_sum: 92.03650724887848
time_elpased: 2.003
batch start
#iterations: 169
currently lose_sum: 91.95512056350708
time_elpased: 2.532
batch start
#iterations: 170
currently lose_sum: 91.47754448652267
time_elpased: 2.822
batch start
#iterations: 171
currently lose_sum: 91.7045886516571
time_elpased: 2.656
batch start
#iterations: 172
currently lose_sum: 92.11638712882996
time_elpased: 2.735
batch start
#iterations: 173
currently lose_sum: 91.3197408914566
time_elpased: 2.768
batch start
#iterations: 174
currently lose_sum: 91.75356543064117
time_elpased: 2.721
batch start
#iterations: 175
currently lose_sum: 91.26168757677078
time_elpased: 2.639
batch start
#iterations: 176
currently lose_sum: 91.59896820783615
time_elpased: 2.531
batch start
#iterations: 177
currently lose_sum: 91.0015732049942
time_elpased: 2.747
batch start
#iterations: 178
currently lose_sum: 91.99358761310577
time_elpased: 2.356
batch start
#iterations: 179
currently lose_sum: 91.10045075416565
time_elpased: 2.597
start validation test
0.569226804124
0.603626943005
0.407636101677
0.486639228454
0.569510501526
67.707
batch start
#iterations: 180
currently lose_sum: 91.30082684755325
time_elpased: 2.648
batch start
#iterations: 181
currently lose_sum: 92.00308102369308
time_elpased: 2.716
batch start
#iterations: 182
currently lose_sum: 91.0661211013794
time_elpased: 2.734
batch start
#iterations: 183
currently lose_sum: 91.50015246868134
time_elpased: 2.699
batch start
#iterations: 184
currently lose_sum: 91.14294981956482
time_elpased: 2.438
batch start
#iterations: 185
currently lose_sum: 90.59168136119843
time_elpased: 2.757
batch start
#iterations: 186
currently lose_sum: 91.22378700971603
time_elpased: 2.717
batch start
#iterations: 187
currently lose_sum: 91.11615687608719
time_elpased: 2.773
batch start
#iterations: 188
currently lose_sum: 90.36566317081451
time_elpased: 2.636
batch start
#iterations: 189
currently lose_sum: 90.72597497701645
time_elpased: 2.654
batch start
#iterations: 190
currently lose_sum: 90.89672750234604
time_elpased: 2.654
batch start
#iterations: 191
currently lose_sum: 90.87525528669357
time_elpased: 2.672
batch start
#iterations: 192
currently lose_sum: 91.18154972791672
time_elpased: 2.801
batch start
#iterations: 193
currently lose_sum: 90.28573662042618
time_elpased: 2.708
batch start
#iterations: 194
currently lose_sum: 90.63868474960327
time_elpased: 2.473
batch start
#iterations: 195
currently lose_sum: 90.20720255374908
time_elpased: 2.495
batch start
#iterations: 196
currently lose_sum: 90.27777940034866
time_elpased: 2.613
batch start
#iterations: 197
currently lose_sum: 90.75508320331573
time_elpased: 2.743
batch start
#iterations: 198
currently lose_sum: 90.78259807825089
time_elpased: 2.682
batch start
#iterations: 199
currently lose_sum: 90.22670763731003
time_elpased: 2.817
start validation test
0.568298969072
0.593872411863
0.436863229392
0.503409427809
0.568529724786
67.939
batch start
#iterations: 200
currently lose_sum: 90.21130657196045
time_elpased: 2.846
batch start
#iterations: 201
currently lose_sum: 90.25088673830032
time_elpased: 2.748
batch start
#iterations: 202
currently lose_sum: 90.03456258773804
time_elpased: 2.999
batch start
#iterations: 203
currently lose_sum: 90.24282586574554
time_elpased: 3.001
batch start
#iterations: 204
currently lose_sum: 90.47145819664001
time_elpased: 2.866
batch start
#iterations: 205
currently lose_sum: 90.10544121265411
time_elpased: 2.653
batch start
#iterations: 206
currently lose_sum: 89.98189526796341
time_elpased: 2.912
batch start
#iterations: 207
currently lose_sum: 90.37761980295181
time_elpased: 2.681
batch start
#iterations: 208
currently lose_sum: 90.21789908409119
time_elpased: 2.721
batch start
#iterations: 209
currently lose_sum: 90.02684211730957
time_elpased: 2.74
batch start
#iterations: 210
currently lose_sum: 89.92903167009354
time_elpased: 2.807
batch start
#iterations: 211
currently lose_sum: 90.05481398105621
time_elpased: 2.689
batch start
#iterations: 212
currently lose_sum: 89.72155123949051
time_elpased: 2.611
batch start
#iterations: 213
currently lose_sum: 90.13724678754807
time_elpased: 2.678
batch start
#iterations: 214
currently lose_sum: 90.21983188390732
time_elpased: 2.768
batch start
#iterations: 215
currently lose_sum: 89.97085452079773
time_elpased: 2.592
batch start
#iterations: 216
currently lose_sum: 89.45179212093353
time_elpased: 2.78
batch start
#iterations: 217
currently lose_sum: 89.89616918563843
time_elpased: 2.88
batch start
#iterations: 218
currently lose_sum: 89.94255608320236
time_elpased: 2.721
batch start
#iterations: 219
currently lose_sum: 89.86450827121735
time_elpased: 2.681
start validation test
0.554845360825
0.594774679993
0.349078933827
0.439948119326
0.555206615525
70.308
batch start
#iterations: 220
currently lose_sum: 90.05970335006714
time_elpased: 2.486
batch start
#iterations: 221
currently lose_sum: 89.93276405334473
time_elpased: 2.76
batch start
#iterations: 222
currently lose_sum: 89.83312767744064
time_elpased: 3.524
batch start
#iterations: 223
currently lose_sum: 89.57225203514099
time_elpased: 2.687
batch start
#iterations: 224
currently lose_sum: 89.69294363260269
time_elpased: 2.5
batch start
#iterations: 225
currently lose_sum: 89.63509404659271
time_elpased: 2.364
batch start
#iterations: 226
currently lose_sum: 89.59356236457825
time_elpased: 2.7
batch start
#iterations: 227
currently lose_sum: 89.51311385631561
time_elpased: 2.736
batch start
#iterations: 228
currently lose_sum: 89.57285457849503
time_elpased: 2.731
batch start
#iterations: 229
currently lose_sum: 89.29843926429749
time_elpased: 2.732
batch start
#iterations: 230
currently lose_sum: 89.55349749326706
time_elpased: 2.69
batch start
#iterations: 231
currently lose_sum: 89.25623995065689
time_elpased: 2.612
batch start
#iterations: 232
currently lose_sum: 89.50822788476944
time_elpased: 2.844
batch start
#iterations: 233
currently lose_sum: 88.91730254888535
time_elpased: 2.738
batch start
#iterations: 234
currently lose_sum: 89.47752457857132
time_elpased: 2.739
batch start
#iterations: 235
currently lose_sum: 89.47029858827591
time_elpased: 2.801
batch start
#iterations: 236
currently lose_sum: 89.22980111837387
time_elpased: 2.907
batch start
#iterations: 237
currently lose_sum: 89.18894267082214
time_elpased: 2.987
batch start
#iterations: 238
currently lose_sum: 89.0214131474495
time_elpased: 2.892
batch start
#iterations: 239
currently lose_sum: 89.0793268084526
time_elpased: 2.826
start validation test
0.568608247423
0.583066305152
0.486878666255
0.530648869946
0.568751736308
68.378
batch start
#iterations: 240
currently lose_sum: 89.31182718276978
time_elpased: 2.755
batch start
#iterations: 241
currently lose_sum: 89.29887652397156
time_elpased: 2.568
batch start
#iterations: 242
currently lose_sum: 88.65451776981354
time_elpased: 2.788
batch start
#iterations: 243
currently lose_sum: 89.12483555078506
time_elpased: 2.709
batch start
#iterations: 244
currently lose_sum: 88.97528249025345
time_elpased: 2.732
batch start
#iterations: 245
currently lose_sum: 89.00209987163544
time_elpased: 2.911
batch start
#iterations: 246
currently lose_sum: 88.63573461771011
time_elpased: 2.758
batch start
#iterations: 247
currently lose_sum: 88.55604493618011
time_elpased: 2.794
batch start
#iterations: 248
currently lose_sum: 89.01800853013992
time_elpased: 2.749
batch start
#iterations: 249
currently lose_sum: 88.72055244445801
time_elpased: 2.78
batch start
#iterations: 250
currently lose_sum: 88.73085361719131
time_elpased: 2.713
batch start
#iterations: 251
currently lose_sum: 88.44871789216995
time_elpased: 2.708
batch start
#iterations: 252
currently lose_sum: 88.86668074131012
time_elpased: 2.743
batch start
#iterations: 253
currently lose_sum: 88.60876548290253
time_elpased: 2.702
batch start
#iterations: 254
currently lose_sum: 88.63703632354736
time_elpased: 2.618
batch start
#iterations: 255
currently lose_sum: 88.77358692884445
time_elpased: 2.709
batch start
#iterations: 256
currently lose_sum: 88.32715648412704
time_elpased: 2.789
batch start
#iterations: 257
currently lose_sum: 88.80816984176636
time_elpased: 2.532
batch start
#iterations: 258
currently lose_sum: 88.18202865123749
time_elpased: 2.447
batch start
#iterations: 259
currently lose_sum: 88.03799247741699
time_elpased: 2.705
start validation test
0.566494845361
0.583429082089
0.470309766389
0.520797720798
0.566663713102
68.912
batch start
#iterations: 260
currently lose_sum: 88.17430055141449
time_elpased: 2.545
batch start
#iterations: 261
currently lose_sum: 87.9544769525528
time_elpased: 2.65
batch start
#iterations: 262
currently lose_sum: 88.51280933618546
time_elpased: 2.734
batch start
#iterations: 263
currently lose_sum: 87.52135688066483
time_elpased: 2.597
batch start
#iterations: 264
currently lose_sum: 88.48314344882965
time_elpased: 2.752
batch start
#iterations: 265
currently lose_sum: 88.40920239686966
time_elpased: 2.661
batch start
#iterations: 266
currently lose_sum: 88.3776046037674
time_elpased: 2.729
batch start
#iterations: 267
currently lose_sum: 88.01631861925125
time_elpased: 2.314
batch start
#iterations: 268
currently lose_sum: 88.49971336126328
time_elpased: 2.814
batch start
#iterations: 269
currently lose_sum: 88.17725706100464
time_elpased: 2.8
batch start
#iterations: 270
currently lose_sum: 87.57063817977905
time_elpased: 2.723
batch start
#iterations: 271
currently lose_sum: 88.27099025249481
time_elpased: 2.783
batch start
#iterations: 272
currently lose_sum: 88.66168814897537
time_elpased: 2.781
batch start
#iterations: 273
currently lose_sum: 88.29703307151794
time_elpased: 2.269
batch start
#iterations: 274
currently lose_sum: 87.81050562858582
time_elpased: 2.477
batch start
#iterations: 275
currently lose_sum: 88.10095310211182
time_elpased: 2.897
batch start
#iterations: 276
currently lose_sum: 87.5687980055809
time_elpased: 2.243
batch start
#iterations: 277
currently lose_sum: 88.27182239294052
time_elpased: 2.52
batch start
#iterations: 278
currently lose_sum: 87.79850745201111
time_elpased: 2.786
batch start
#iterations: 279
currently lose_sum: 87.71395725011826
time_elpased: 2.742
start validation test
0.573556701031
0.601804850536
0.439230215087
0.507823189958
0.573792531895
69.416
batch start
#iterations: 280
currently lose_sum: 87.93205040693283
time_elpased: 2.641
batch start
#iterations: 281
currently lose_sum: 88.09614282846451
time_elpased: 2.786
batch start
#iterations: 282
currently lose_sum: 87.92630124092102
time_elpased: 2.851
batch start
#iterations: 283
currently lose_sum: 87.9756668806076
time_elpased: 2.608
batch start
#iterations: 284
currently lose_sum: 88.00423240661621
time_elpased: 2.385
batch start
#iterations: 285
currently lose_sum: 87.56229943037033
time_elpased: 2.807
batch start
#iterations: 286
currently lose_sum: 87.83661848306656
time_elpased: 2.809
batch start
#iterations: 287
currently lose_sum: 87.21946221590042
time_elpased: 2.745
batch start
#iterations: 288
currently lose_sum: 87.65980017185211
time_elpased: 2.686
batch start
#iterations: 289
currently lose_sum: 87.56131619215012
time_elpased: 3.024
batch start
#iterations: 290
currently lose_sum: 87.0567838549614
time_elpased: 2.908
batch start
#iterations: 291
currently lose_sum: 87.11199378967285
time_elpased: 2.984
batch start
#iterations: 292
currently lose_sum: 87.22681999206543
time_elpased: 3.161
batch start
#iterations: 293
currently lose_sum: 87.54991906881332
time_elpased: 2.894
batch start
#iterations: 294
currently lose_sum: 87.30214196443558
time_elpased: 2.898
batch start
#iterations: 295
currently lose_sum: 86.99183082580566
time_elpased: 3.174
batch start
#iterations: 296
currently lose_sum: 87.31519985198975
time_elpased: 3.131
batch start
#iterations: 297
currently lose_sum: 87.4542629122734
time_elpased: 3.365
batch start
#iterations: 298
currently lose_sum: 87.32831883430481
time_elpased: 3.347
batch start
#iterations: 299
currently lose_sum: 87.34023326635361
time_elpased: 3.191
start validation test
0.559432989691
0.592886630676
0.38427498199
0.46631283172
0.55974050659
71.274
batch start
#iterations: 300
currently lose_sum: 87.10460126399994
time_elpased: 3.23
batch start
#iterations: 301
currently lose_sum: 86.70067518949509
time_elpased: 3.143
batch start
#iterations: 302
currently lose_sum: 86.92675310373306
time_elpased: 2.562
batch start
#iterations: 303
currently lose_sum: 86.7915426492691
time_elpased: 2.358
batch start
#iterations: 304
currently lose_sum: 86.83046382665634
time_elpased: 2.973
batch start
#iterations: 305
currently lose_sum: 86.94604796171188
time_elpased: 2.932
batch start
#iterations: 306
currently lose_sum: 87.11843901872635
time_elpased: 2.855
batch start
#iterations: 307
currently lose_sum: 87.2587326169014
time_elpased: 2.25
batch start
#iterations: 308
currently lose_sum: 87.29771882295609
time_elpased: 2.476
batch start
#iterations: 309
currently lose_sum: 87.14140337705612
time_elpased: 3.007
batch start
#iterations: 310
currently lose_sum: 87.02721905708313
time_elpased: 2.986
batch start
#iterations: 311
currently lose_sum: 86.43914586305618
time_elpased: 2.779
batch start
#iterations: 312
currently lose_sum: 86.3773022890091
time_elpased: 2.902
batch start
#iterations: 313
currently lose_sum: 86.5540047287941
time_elpased: 2.783
batch start
#iterations: 314
currently lose_sum: 86.11241507530212
time_elpased: 2.604
batch start
#iterations: 315
currently lose_sum: 86.73762857913971
time_elpased: 2.444
batch start
#iterations: 316
currently lose_sum: 86.71969592571259
time_elpased: 2.612
batch start
#iterations: 317
currently lose_sum: 86.66077411174774
time_elpased: 2.564
batch start
#iterations: 318
currently lose_sum: 86.3839880824089
time_elpased: 2.714
batch start
#iterations: 319
currently lose_sum: 86.52962511777878
time_elpased: 2.656
start validation test
0.561443298969
0.594586136755
0.391067201811
0.471815247082
0.561742420486
71.176
batch start
#iterations: 320
currently lose_sum: 86.60092598199844
time_elpased: 2.74
batch start
#iterations: 321
currently lose_sum: 86.40504306554794
time_elpased: 2.77
batch start
#iterations: 322
currently lose_sum: 86.86936104297638
time_elpased: 2.632
batch start
#iterations: 323
currently lose_sum: 86.61205232143402
time_elpased: 2.656
batch start
#iterations: 324
currently lose_sum: 86.64172583818436
time_elpased: 2.777
batch start
#iterations: 325
currently lose_sum: 86.419851064682
time_elpased: 2.61
batch start
#iterations: 326
currently lose_sum: 86.63311779499054
time_elpased: 2.67
batch start
#iterations: 327
currently lose_sum: 86.27202367782593
time_elpased: 2.761
batch start
#iterations: 328
currently lose_sum: 85.80499184131622
time_elpased: 2.911
batch start
#iterations: 329
currently lose_sum: 86.65035378932953
time_elpased: 3.007
batch start
#iterations: 330
currently lose_sum: 86.56062012910843
time_elpased: 2.709
batch start
#iterations: 331
currently lose_sum: 86.44257855415344
time_elpased: 2.892
batch start
#iterations: 332
currently lose_sum: 86.14418464899063
time_elpased: 2.662
batch start
#iterations: 333
currently lose_sum: 86.24813413619995
time_elpased: 2.929
batch start
#iterations: 334
currently lose_sum: 86.40563851594925
time_elpased: 2.926
batch start
#iterations: 335
currently lose_sum: 86.25432497262955
time_elpased: 2.959
batch start
#iterations: 336
currently lose_sum: 86.2523278594017
time_elpased: 2.629
batch start
#iterations: 337
currently lose_sum: 85.81758314371109
time_elpased: 2.865
batch start
#iterations: 338
currently lose_sum: 85.80494910478592
time_elpased: 2.645
batch start
#iterations: 339
currently lose_sum: 86.29963290691376
time_elpased: 2.811
start validation test
0.553556701031
0.588829071332
0.360193475352
0.446970180704
0.553896179998
73.107
batch start
#iterations: 340
currently lose_sum: 86.39226830005646
time_elpased: 2.767
batch start
#iterations: 341
currently lose_sum: 86.98627471923828
time_elpased: 2.626
batch start
#iterations: 342
currently lose_sum: 86.35274040699005
time_elpased: 2.614
batch start
#iterations: 343
currently lose_sum: 86.31839221715927
time_elpased: 2.126
batch start
#iterations: 344
currently lose_sum: 85.63448739051819
time_elpased: 2.825
batch start
#iterations: 345
currently lose_sum: 85.90916126966476
time_elpased: 2.73
batch start
#iterations: 346
currently lose_sum: 86.27521908283234
time_elpased: 2.323
batch start
#iterations: 347
currently lose_sum: 85.24522829055786
time_elpased: 2.453
batch start
#iterations: 348
currently lose_sum: 85.79541462659836
time_elpased: 2.883
batch start
#iterations: 349
currently lose_sum: 85.86537528038025
time_elpased: 2.912
batch start
#iterations: 350
currently lose_sum: 85.41894221305847
time_elpased: 2.92
batch start
#iterations: 351
currently lose_sum: 86.14824610948563
time_elpased: 2.334
batch start
#iterations: 352
currently lose_sum: 85.37597560882568
time_elpased: 2.563
batch start
#iterations: 353
currently lose_sum: 85.74617910385132
time_elpased: 2.692
batch start
#iterations: 354
currently lose_sum: 85.49446332454681
time_elpased: 2.771
batch start
#iterations: 355
currently lose_sum: 85.37420743703842
time_elpased: 2.57
batch start
#iterations: 356
currently lose_sum: 85.55187541246414
time_elpased: 2.629
batch start
#iterations: 357
currently lose_sum: 86.13975518941879
time_elpased: 2.749
batch start
#iterations: 358
currently lose_sum: 85.54768764972687
time_elpased: 2.767
batch start
#iterations: 359
currently lose_sum: 85.479641020298
time_elpased: 2.657
start validation test
0.555463917526
0.587147185457
0.378923536071
0.460595446585
0.555773861395
73.025
batch start
#iterations: 360
currently lose_sum: 85.3325634598732
time_elpased: 2.715
batch start
#iterations: 361
currently lose_sum: 85.50509256124496
time_elpased: 2.784
batch start
#iterations: 362
currently lose_sum: 85.42868602275848
time_elpased: 2.62
batch start
#iterations: 363
currently lose_sum: 85.07479906082153
time_elpased: 2.67
batch start
#iterations: 364
currently lose_sum: 85.23635935783386
time_elpased: 2.691
batch start
#iterations: 365
currently lose_sum: 85.60311561822891
time_elpased: 2.796
batch start
#iterations: 366
currently lose_sum: 85.45377784967422
time_elpased: 2.686
batch start
#iterations: 367
currently lose_sum: 85.58710986375809
time_elpased: 2.644
batch start
#iterations: 368
currently lose_sum: 85.7759782075882
time_elpased: 2.695
batch start
#iterations: 369
currently lose_sum: 85.42783051729202
time_elpased: 2.768
batch start
#iterations: 370
currently lose_sum: 85.03977990150452
time_elpased: 2.705
batch start
#iterations: 371
currently lose_sum: 84.71861267089844
time_elpased: 2.706
batch start
#iterations: 372
currently lose_sum: 85.18158656358719
time_elpased: 2.708
batch start
#iterations: 373
currently lose_sum: 85.01567947864532
time_elpased: 2.674
batch start
#iterations: 374
currently lose_sum: 85.38542592525482
time_elpased: 2.805
batch start
#iterations: 375
currently lose_sum: 85.48012453317642
time_elpased: 2.757
batch start
#iterations: 376
currently lose_sum: 85.43285173177719
time_elpased: 2.799
batch start
#iterations: 377
currently lose_sum: 84.8381010890007
time_elpased: 2.749
batch start
#iterations: 378
currently lose_sum: 85.1397248506546
time_elpased: 2.35
batch start
#iterations: 379
currently lose_sum: 85.32672369480133
time_elpased: 2.569
start validation test
0.551649484536
0.602083750751
0.309251826695
0.408621158553
0.552075051012
74.775
batch start
#iterations: 380
currently lose_sum: 85.40818065404892
time_elpased: 2.525
batch start
#iterations: 381
currently lose_sum: 84.82002288103104
time_elpased: 2.752
batch start
#iterations: 382
currently lose_sum: 84.60057073831558
time_elpased: 2.666
batch start
#iterations: 383
currently lose_sum: 85.20826995372772
time_elpased: 2.693
batch start
#iterations: 384
currently lose_sum: 84.80960750579834
time_elpased: 2.912
batch start
#iterations: 385
currently lose_sum: 85.07760971784592
time_elpased: 3.022
batch start
#iterations: 386
currently lose_sum: 85.26142758131027
time_elpased: 2.855
batch start
#iterations: 387
currently lose_sum: 84.88395965099335
time_elpased: 2.905
batch start
#iterations: 388
currently lose_sum: 84.62164774537086
time_elpased: 2.591
batch start
#iterations: 389
currently lose_sum: 84.24665135145187
time_elpased: 2.551
batch start
#iterations: 390
currently lose_sum: 84.86003237962723
time_elpased: 2.816
batch start
#iterations: 391
currently lose_sum: 84.47024071216583
time_elpased: 2.739
batch start
#iterations: 392
currently lose_sum: 84.43873488903046
time_elpased: 2.799
batch start
#iterations: 393
currently lose_sum: 84.69241839647293
time_elpased: 2.819
batch start
#iterations: 394
currently lose_sum: 84.69764846563339
time_elpased: 2.673
batch start
#iterations: 395
currently lose_sum: 84.88785952329636
time_elpased: 2.711
batch start
#iterations: 396
currently lose_sum: 84.80891001224518
time_elpased: 2.706
batch start
#iterations: 397
currently lose_sum: 84.95119774341583
time_elpased: 2.701
batch start
#iterations: 398
currently lose_sum: 84.77552473545074
time_elpased: 2.678
batch start
#iterations: 399
currently lose_sum: 84.74701726436615
time_elpased: 2.736
start validation test
0.550051546392
0.582941571525
0.35731192755
0.443054935239
0.550389930521
73.397
acc: 0.597
pre: 0.616
rec: 0.519
F1: 0.563
auc: 0.597
