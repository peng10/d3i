start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.56237417459488
time_elpased: 2.444
batch start
#iterations: 1
currently lose_sum: 100.19364047050476
time_elpased: 2.481
batch start
#iterations: 2
currently lose_sum: 100.01509296894073
time_elpased: 2.428
batch start
#iterations: 3
currently lose_sum: 99.98640352487564
time_elpased: 2.482
batch start
#iterations: 4
currently lose_sum: 99.787229180336
time_elpased: 2.517
batch start
#iterations: 5
currently lose_sum: 99.74271565675735
time_elpased: 2.738
batch start
#iterations: 6
currently lose_sum: 99.66057968139648
time_elpased: 2.362
batch start
#iterations: 7
currently lose_sum: 99.45062375068665
time_elpased: 2.535
batch start
#iterations: 8
currently lose_sum: 99.16882055997849
time_elpased: 2.471
batch start
#iterations: 9
currently lose_sum: 99.5048298239708
time_elpased: 2.544
batch start
#iterations: 10
currently lose_sum: 99.20472377538681
time_elpased: 2.349
batch start
#iterations: 11
currently lose_sum: 99.27320951223373
time_elpased: 2.475
batch start
#iterations: 12
currently lose_sum: 98.93852972984314
time_elpased: 2.345
batch start
#iterations: 13
currently lose_sum: 98.75022161006927
time_elpased: 2.355
batch start
#iterations: 14
currently lose_sum: 99.00391000509262
time_elpased: 2.281
batch start
#iterations: 15
currently lose_sum: 98.64341896772385
time_elpased: 2.39
batch start
#iterations: 16
currently lose_sum: 98.98877358436584
time_elpased: 2.308
batch start
#iterations: 17
currently lose_sum: 98.67394107580185
time_elpased: 2.321
batch start
#iterations: 18
currently lose_sum: 98.44013273715973
time_elpased: 2.356
batch start
#iterations: 19
currently lose_sum: 98.79425173997879
time_elpased: 2.432
start validation test
0.589948453608
0.6406449553
0.412987547597
0.502221387898
0.590259135773
65.157
batch start
#iterations: 20
currently lose_sum: 98.33829987049103
time_elpased: 2.518
batch start
#iterations: 21
currently lose_sum: 98.17341363430023
time_elpased: 2.336
batch start
#iterations: 22
currently lose_sum: 98.22480022907257
time_elpased: 2.432
batch start
#iterations: 23
currently lose_sum: 98.27960228919983
time_elpased: 2.484
batch start
#iterations: 24
currently lose_sum: 98.00865375995636
time_elpased: 2.42
batch start
#iterations: 25
currently lose_sum: 98.00619924068451
time_elpased: 2.321
batch start
#iterations: 26
currently lose_sum: 98.20745712518692
time_elpased: 2.4
batch start
#iterations: 27
currently lose_sum: 98.01226705312729
time_elpased: 2.517
batch start
#iterations: 28
currently lose_sum: 97.55345165729523
time_elpased: 2.473
batch start
#iterations: 29
currently lose_sum: 97.72982436418533
time_elpased: 2.481
batch start
#iterations: 30
currently lose_sum: 97.84850758314133
time_elpased: 2.425
batch start
#iterations: 31
currently lose_sum: 97.84628319740295
time_elpased: 2.567
batch start
#iterations: 32
currently lose_sum: 97.35600852966309
time_elpased: 2.424
batch start
#iterations: 33
currently lose_sum: 97.35290712118149
time_elpased: 2.48
batch start
#iterations: 34
currently lose_sum: 97.34022575616837
time_elpased: 2.458
batch start
#iterations: 35
currently lose_sum: 97.39062714576721
time_elpased: 2.512
batch start
#iterations: 36
currently lose_sum: 97.55357855558395
time_elpased: 2.433
batch start
#iterations: 37
currently lose_sum: 97.12123852968216
time_elpased: 2.299
batch start
#iterations: 38
currently lose_sum: 97.17431902885437
time_elpased: 2.343
batch start
#iterations: 39
currently lose_sum: 97.30274069309235
time_elpased: 2.386
start validation test
0.607731958763
0.611328331396
0.595348358547
0.603232533889
0.607753700083
64.499
batch start
#iterations: 40
currently lose_sum: 97.32959759235382
time_elpased: 2.5
batch start
#iterations: 41
currently lose_sum: 96.94168603420258
time_elpased: 2.459
batch start
#iterations: 42
currently lose_sum: 96.71098655462265
time_elpased: 2.343
batch start
#iterations: 43
currently lose_sum: 96.61588108539581
time_elpased: 2.335
batch start
#iterations: 44
currently lose_sum: 96.93260502815247
time_elpased: 2.443
batch start
#iterations: 45
currently lose_sum: 96.4683187007904
time_elpased: 2.369
batch start
#iterations: 46
currently lose_sum: 96.70384341478348
time_elpased: 2.326
batch start
#iterations: 47
currently lose_sum: 96.93909496068954
time_elpased: 2.563
batch start
#iterations: 48
currently lose_sum: 96.4587185382843
time_elpased: 2.461
batch start
#iterations: 49
currently lose_sum: 96.6398366689682
time_elpased: 2.507
batch start
#iterations: 50
currently lose_sum: 96.45373106002808
time_elpased: 2.517
batch start
#iterations: 51
currently lose_sum: 96.23562163114548
time_elpased: 2.328
batch start
#iterations: 52
currently lose_sum: 96.06439745426178
time_elpased: 2.369
batch start
#iterations: 53
currently lose_sum: 96.42579084634781
time_elpased: 2.355
batch start
#iterations: 54
currently lose_sum: 95.96850717067719
time_elpased: 2.355
batch start
#iterations: 55
currently lose_sum: 96.03639441728592
time_elpased: 2.339
batch start
#iterations: 56
currently lose_sum: 95.99844139814377
time_elpased: 2.41
batch start
#iterations: 57
currently lose_sum: 95.7672478556633
time_elpased: 2.564
batch start
#iterations: 58
currently lose_sum: 95.85801273584366
time_elpased: 2.607
batch start
#iterations: 59
currently lose_sum: 95.7420084476471
time_elpased: 2.618
start validation test
0.590567010309
0.616373655209
0.483482556345
0.541899763539
0.590755013585
65.340
batch start
#iterations: 60
currently lose_sum: 95.66192239522934
time_elpased: 2.503
batch start
#iterations: 61
currently lose_sum: 96.11686450242996
time_elpased: 2.367
batch start
#iterations: 62
currently lose_sum: 95.23984390497208
time_elpased: 2.452
batch start
#iterations: 63
currently lose_sum: 95.18054485321045
time_elpased: 2.498
batch start
#iterations: 64
currently lose_sum: 95.37841802835464
time_elpased: 2.579
batch start
#iterations: 65
currently lose_sum: 95.00763928890228
time_elpased: 2.5
batch start
#iterations: 66
currently lose_sum: 95.04806208610535
time_elpased: 2.524
batch start
#iterations: 67
currently lose_sum: 95.18552601337433
time_elpased: 2.499
batch start
#iterations: 68
currently lose_sum: 94.83336627483368
time_elpased: 2.405
batch start
#iterations: 69
currently lose_sum: 94.65973043441772
time_elpased: 2.511
batch start
#iterations: 70
currently lose_sum: 94.48237651586533
time_elpased: 2.475
batch start
#iterations: 71
currently lose_sum: 94.75105059146881
time_elpased: 2.5
batch start
#iterations: 72
currently lose_sum: 94.01890009641647
time_elpased: 2.462
batch start
#iterations: 73
currently lose_sum: 94.25674837827682
time_elpased: 2.501
batch start
#iterations: 74
currently lose_sum: 93.9978911280632
time_elpased: 2.505
batch start
#iterations: 75
currently lose_sum: 94.05152040719986
time_elpased: 2.493
batch start
#iterations: 76
currently lose_sum: 94.4824126958847
time_elpased: 2.515
batch start
#iterations: 77
currently lose_sum: 93.80441921949387
time_elpased: 2.473
batch start
#iterations: 78
currently lose_sum: 94.20252907276154
time_elpased: 2.459
batch start
#iterations: 79
currently lose_sum: 93.9167617559433
time_elpased: 2.531
start validation test
0.577577319588
0.616289731051
0.415045796028
0.496033454277
0.577862668746
66.619
batch start
#iterations: 80
currently lose_sum: 94.40026897192001
time_elpased: 2.397
batch start
#iterations: 81
currently lose_sum: 93.98356604576111
time_elpased: 2.395
batch start
#iterations: 82
currently lose_sum: 93.77283751964569
time_elpased: 2.376
batch start
#iterations: 83
currently lose_sum: 93.34518307447433
time_elpased: 2.448
batch start
#iterations: 84
currently lose_sum: 93.3370344042778
time_elpased: 2.452
batch start
#iterations: 85
currently lose_sum: 93.27275794744492
time_elpased: 2.468
batch start
#iterations: 86
currently lose_sum: 93.43987429141998
time_elpased: 2.534
batch start
#iterations: 87
currently lose_sum: 93.48635393381119
time_elpased: 2.486
batch start
#iterations: 88
currently lose_sum: 93.62626308202744
time_elpased: 2.578
batch start
#iterations: 89
currently lose_sum: 93.06919729709625
time_elpased: 2.517
batch start
#iterations: 90
currently lose_sum: 92.72059726715088
time_elpased: 2.401
batch start
#iterations: 91
currently lose_sum: 92.48717594146729
time_elpased: 2.481
batch start
#iterations: 92
currently lose_sum: 92.9290983080864
time_elpased: 2.482
batch start
#iterations: 93
currently lose_sum: 92.22991061210632
time_elpased: 2.501
batch start
#iterations: 94
currently lose_sum: 92.22626799345016
time_elpased: 2.466
batch start
#iterations: 95
currently lose_sum: 92.80819016695023
time_elpased: 2.481
batch start
#iterations: 96
currently lose_sum: 92.47310066223145
time_elpased: 2.45
batch start
#iterations: 97
currently lose_sum: 91.8785195350647
time_elpased: 2.491
batch start
#iterations: 98
currently lose_sum: 92.05669832229614
time_elpased: 2.483
batch start
#iterations: 99
currently lose_sum: 92.03320801258087
time_elpased: 2.484
start validation test
0.586855670103
0.612834791832
0.475661212308
0.535604612086
0.587050889124
67.003
batch start
#iterations: 100
currently lose_sum: 91.85995042324066
time_elpased: 2.475
batch start
#iterations: 101
currently lose_sum: 91.66110664606094
time_elpased: 2.633
batch start
#iterations: 102
currently lose_sum: 91.47690027952194
time_elpased: 2.432
batch start
#iterations: 103
currently lose_sum: 91.72094631195068
time_elpased: 2.344
batch start
#iterations: 104
currently lose_sum: 91.43831706047058
time_elpased: 2.44
batch start
#iterations: 105
currently lose_sum: 91.3774601817131
time_elpased: 2.491
batch start
#iterations: 106
currently lose_sum: 91.25213873386383
time_elpased: 2.503
batch start
#iterations: 107
currently lose_sum: 91.11653298139572
time_elpased: 2.58
batch start
#iterations: 108
currently lose_sum: 90.82708305120468
time_elpased: 2.494
batch start
#iterations: 109
currently lose_sum: 91.10124093294144
time_elpased: 2.497
batch start
#iterations: 110
currently lose_sum: 90.86532193422318
time_elpased: 2.492
batch start
#iterations: 111
currently lose_sum: 91.18663102388382
time_elpased: 2.499
batch start
#iterations: 112
currently lose_sum: 90.45182943344116
time_elpased: 2.452
batch start
#iterations: 113
currently lose_sum: 90.2798884510994
time_elpased: 2.496
batch start
#iterations: 114
currently lose_sum: 90.33309042453766
time_elpased: 2.357
batch start
#iterations: 115
currently lose_sum: 90.6538924574852
time_elpased: 2.467
batch start
#iterations: 116
currently lose_sum: 90.38604426383972
time_elpased: 2.501
batch start
#iterations: 117
currently lose_sum: 90.1811752319336
time_elpased: 2.557
batch start
#iterations: 118
currently lose_sum: 90.10010713338852
time_elpased: 2.626
batch start
#iterations: 119
currently lose_sum: 89.89263331890106
time_elpased: 2.49
start validation test
0.577216494845
0.622751579971
0.395492435937
0.483761329305
0.57753553946
68.967
batch start
#iterations: 120
currently lose_sum: 90.27742493152618
time_elpased: 2.555
batch start
#iterations: 121
currently lose_sum: 89.43026745319366
time_elpased: 2.438
batch start
#iterations: 122
currently lose_sum: 89.94716215133667
time_elpased: 2.542
batch start
#iterations: 123
currently lose_sum: 89.64771854877472
time_elpased: 2.573
batch start
#iterations: 124
currently lose_sum: 90.44885921478271
time_elpased: 2.537
batch start
#iterations: 125
currently lose_sum: 89.81197595596313
time_elpased: 2.393
batch start
#iterations: 126
currently lose_sum: 89.24696260690689
time_elpased: 2.498
batch start
#iterations: 127
currently lose_sum: 89.0827060341835
time_elpased: 2.507
batch start
#iterations: 128
currently lose_sum: 89.18612313270569
time_elpased: 2.46
batch start
#iterations: 129
currently lose_sum: 89.30015569925308
time_elpased: 2.454
batch start
#iterations: 130
currently lose_sum: 89.05288189649582
time_elpased: 2.533
batch start
#iterations: 131
currently lose_sum: 89.37913864850998
time_elpased: 2.557
batch start
#iterations: 132
currently lose_sum: 88.77629578113556
time_elpased: 2.471
batch start
#iterations: 133
currently lose_sum: 89.2171196937561
time_elpased: 2.593
batch start
#iterations: 134
currently lose_sum: 89.1395052075386
time_elpased: 2.53
batch start
#iterations: 135
currently lose_sum: 89.0052278637886
time_elpased: 2.593
batch start
#iterations: 136
currently lose_sum: 88.34750020503998
time_elpased: 2.494
batch start
#iterations: 137
currently lose_sum: 88.46896135807037
time_elpased: 2.538
batch start
#iterations: 138
currently lose_sum: 88.26393157243729
time_elpased: 2.822
batch start
#iterations: 139
currently lose_sum: 88.20483893156052
time_elpased: 2.545
start validation test
0.579381443299
0.60207158778
0.472573839662
0.529520295203
0.579568960521
68.957
batch start
#iterations: 140
currently lose_sum: 88.17405325174332
time_elpased: 2.602
batch start
#iterations: 141
currently lose_sum: 88.18195194005966
time_elpased: 2.473
batch start
#iterations: 142
currently lose_sum: 88.20919561386108
time_elpased: 2.404
batch start
#iterations: 143
currently lose_sum: 87.71710640192032
time_elpased: 2.363
batch start
#iterations: 144
currently lose_sum: 88.09005171060562
time_elpased: 2.406
batch start
#iterations: 145
currently lose_sum: 87.85327738523483
time_elpased: 2.453
batch start
#iterations: 146
currently lose_sum: 87.99578607082367
time_elpased: 2.358
batch start
#iterations: 147
currently lose_sum: 87.94796431064606
time_elpased: 2.445
batch start
#iterations: 148
currently lose_sum: 87.8354332447052
time_elpased: 2.423
batch start
#iterations: 149
currently lose_sum: 87.89316761493683
time_elpased: 2.446
batch start
#iterations: 150
currently lose_sum: 87.30482232570648
time_elpased: 2.367
batch start
#iterations: 151
currently lose_sum: 86.99794626235962
time_elpased: 2.353
batch start
#iterations: 152
currently lose_sum: 87.23733282089233
time_elpased: 2.379
batch start
#iterations: 153
currently lose_sum: 87.36185681819916
time_elpased: 2.446
batch start
#iterations: 154
currently lose_sum: 87.61579263210297
time_elpased: 2.518
batch start
#iterations: 155
currently lose_sum: 86.93914777040482
time_elpased: 2.363
batch start
#iterations: 156
currently lose_sum: 86.82406789064407
time_elpased: 2.446
batch start
#iterations: 157
currently lose_sum: 87.07734912633896
time_elpased: 2.472
batch start
#iterations: 158
currently lose_sum: 86.78272634744644
time_elpased: 2.445
batch start
#iterations: 159
currently lose_sum: 86.63224136829376
time_elpased: 2.468
start validation test
0.559793814433
0.628241446938
0.296696511269
0.403047672305
0.560255722329
74.145
batch start
#iterations: 160
currently lose_sum: 86.42833453416824
time_elpased: 2.617
batch start
#iterations: 161
currently lose_sum: 86.5398376584053
time_elpased: 2.596
batch start
#iterations: 162
currently lose_sum: 86.68035191297531
time_elpased: 2.344
batch start
#iterations: 163
currently lose_sum: 86.56735360622406
time_elpased: 2.384
batch start
#iterations: 164
currently lose_sum: 86.22322905063629
time_elpased: 2.397
batch start
#iterations: 165
currently lose_sum: 85.70266515016556
time_elpased: 2.415
batch start
#iterations: 166
currently lose_sum: 85.4617383480072
time_elpased: 2.444
batch start
#iterations: 167
currently lose_sum: 86.03064978122711
time_elpased: 2.445
batch start
#iterations: 168
currently lose_sum: 85.15871560573578
time_elpased: 2.406
batch start
#iterations: 169
currently lose_sum: 85.82537800073624
time_elpased: 2.455
batch start
#iterations: 170
currently lose_sum: 85.83327376842499
time_elpased: 2.372
batch start
#iterations: 171
currently lose_sum: 85.29334902763367
time_elpased: 2.41
batch start
#iterations: 172
currently lose_sum: 85.47547847032547
time_elpased: 2.43
batch start
#iterations: 173
currently lose_sum: 84.7476025223732
time_elpased: 2.459
batch start
#iterations: 174
currently lose_sum: 85.3879052400589
time_elpased: 2.444
batch start
#iterations: 175
currently lose_sum: 85.3563414812088
time_elpased: 2.598
batch start
#iterations: 176
currently lose_sum: 85.3309998512268
time_elpased: 2.217
batch start
#iterations: 177
currently lose_sum: 85.54990249872208
time_elpased: 2.346
batch start
#iterations: 178
currently lose_sum: 84.81673902273178
time_elpased: 2.407
batch start
#iterations: 179
currently lose_sum: 84.37552946805954
time_elpased: 2.434
start validation test
0.566030927835
0.604643663334
0.385921580735
0.471135121553
0.566347137574
73.723
batch start
#iterations: 180
currently lose_sum: 85.1626256108284
time_elpased: 2.525
batch start
#iterations: 181
currently lose_sum: 83.82921600341797
time_elpased: 2.389
batch start
#iterations: 182
currently lose_sum: 84.44558864831924
time_elpased: 2.443
batch start
#iterations: 183
currently lose_sum: 84.53136855363846
time_elpased: 2.416
batch start
#iterations: 184
currently lose_sum: 84.33562159538269
time_elpased: 2.56
batch start
#iterations: 185
currently lose_sum: 83.77601248025894
time_elpased: 2.426
batch start
#iterations: 186
currently lose_sum: 84.21754574775696
time_elpased: 2.463
batch start
#iterations: 187
currently lose_sum: 84.21978533267975
time_elpased: 2.342
batch start
#iterations: 188
currently lose_sum: 84.30551046133041
time_elpased: 2.227
batch start
#iterations: 189
currently lose_sum: 83.85327732563019
time_elpased: 2.14
batch start
#iterations: 190
currently lose_sum: 84.19022464752197
time_elpased: 2.263
batch start
#iterations: 191
currently lose_sum: 83.4471645951271
time_elpased: 2.603
batch start
#iterations: 192
currently lose_sum: 83.78537505865097
time_elpased: 2.693
batch start
#iterations: 193
currently lose_sum: 83.15859639644623
time_elpased: 2.431
batch start
#iterations: 194
currently lose_sum: 83.49731159210205
time_elpased: 2.507
batch start
#iterations: 195
currently lose_sum: 83.36001235246658
time_elpased: 2.397
batch start
#iterations: 196
currently lose_sum: 83.52926743030548
time_elpased: 2.457
batch start
#iterations: 197
currently lose_sum: 83.58799314498901
time_elpased: 2.566
batch start
#iterations: 198
currently lose_sum: 83.2194941341877
time_elpased: 2.482
batch start
#iterations: 199
currently lose_sum: 83.41130721569061
time_elpased: 2.579
start validation test
0.57706185567
0.599369085174
0.469280642174
0.526406926407
0.577251082215
72.272
batch start
#iterations: 200
currently lose_sum: 82.73945325613022
time_elpased: 2.569
batch start
#iterations: 201
currently lose_sum: 82.86722874641418
time_elpased: 2.486
batch start
#iterations: 202
currently lose_sum: 82.76143485307693
time_elpased: 2.53
batch start
#iterations: 203
currently lose_sum: 82.87183439731598
time_elpased: 2.49
batch start
#iterations: 204
currently lose_sum: 82.9736333489418
time_elpased: 2.486
batch start
#iterations: 205
currently lose_sum: 82.83741331100464
time_elpased: 2.452
batch start
#iterations: 206
currently lose_sum: 81.90843218564987
time_elpased: 2.583
batch start
#iterations: 207
currently lose_sum: 82.48820698261261
time_elpased: 2.508
batch start
#iterations: 208
currently lose_sum: 82.39140731096268
time_elpased: 2.475
batch start
#iterations: 209
currently lose_sum: 82.11958211660385
time_elpased: 2.531
batch start
#iterations: 210
currently lose_sum: 81.82530826330185
time_elpased: 2.498
batch start
#iterations: 211
currently lose_sum: 81.42701116204262
time_elpased: 2.528
batch start
#iterations: 212
currently lose_sum: 82.2333869934082
time_elpased: 2.462
batch start
#iterations: 213
currently lose_sum: 82.3201230764389
time_elpased: 2.501
batch start
#iterations: 214
currently lose_sum: 81.98508948087692
time_elpased: 2.459
batch start
#iterations: 215
currently lose_sum: 81.74183690547943
time_elpased: 2.476
batch start
#iterations: 216
currently lose_sum: 82.13608258962631
time_elpased: 2.535
batch start
#iterations: 217
currently lose_sum: 81.61030501127243
time_elpased: 2.462
batch start
#iterations: 218
currently lose_sum: 82.24147915840149
time_elpased: 2.457
batch start
#iterations: 219
currently lose_sum: 81.03716212511063
time_elpased: 2.476
start validation test
0.566958762887
0.592754440372
0.432746732531
0.500267681875
0.567194392807
76.026
batch start
#iterations: 220
currently lose_sum: 81.67231449484825
time_elpased: 2.507
batch start
#iterations: 221
currently lose_sum: 81.3704445362091
time_elpased: 2.48
batch start
#iterations: 222
currently lose_sum: 80.82823276519775
time_elpased: 2.573
batch start
#iterations: 223
currently lose_sum: 80.34675881266594
time_elpased: 2.505
batch start
#iterations: 224
currently lose_sum: 80.45389890670776
time_elpased: 2.466
batch start
#iterations: 225
currently lose_sum: 81.04879143834114
time_elpased: 2.429
batch start
#iterations: 226
currently lose_sum: 81.14970481395721
time_elpased: 2.421
batch start
#iterations: 227
currently lose_sum: 80.71853891015053
time_elpased: 2.364
batch start
#iterations: 228
currently lose_sum: 81.04448735713959
time_elpased: 2.451
batch start
#iterations: 229
currently lose_sum: 79.98849883675575
time_elpased: 2.516
batch start
#iterations: 230
currently lose_sum: 80.36730691790581
time_elpased: 2.448
batch start
#iterations: 231
currently lose_sum: 81.01542538404465
time_elpased: 2.446
batch start
#iterations: 232
currently lose_sum: 80.0359807908535
time_elpased: 2.507
batch start
#iterations: 233
currently lose_sum: 80.41895925998688
time_elpased: 2.507
batch start
#iterations: 234
currently lose_sum: 80.31358280777931
time_elpased: 2.47
batch start
#iterations: 235
currently lose_sum: 80.41753512620926
time_elpased: 2.439
batch start
#iterations: 236
currently lose_sum: 79.76542419195175
time_elpased: 2.427
batch start
#iterations: 237
currently lose_sum: 80.81375807523727
time_elpased: 2.397
batch start
#iterations: 238
currently lose_sum: 80.39647725224495
time_elpased: 2.459
batch start
#iterations: 239
currently lose_sum: 79.40627577900887
time_elpased: 2.263
start validation test
0.542783505155
0.61242367932
0.237418956468
0.342183328389
0.543319619719
92.231
batch start
#iterations: 240
currently lose_sum: 80.32278117537498
time_elpased: 2.321
batch start
#iterations: 241
currently lose_sum: 79.30010044574738
time_elpased: 2.371
batch start
#iterations: 242
currently lose_sum: 79.92115280032158
time_elpased: 2.286
batch start
#iterations: 243
currently lose_sum: 79.18236565589905
time_elpased: 2.283
batch start
#iterations: 244
currently lose_sum: 79.33809724450111
time_elpased: 2.303
batch start
#iterations: 245
currently lose_sum: 78.97019976377487
time_elpased: 2.31
batch start
#iterations: 246
currently lose_sum: 79.9748665690422
time_elpased: 2.344
batch start
#iterations: 247
currently lose_sum: 79.45019242167473
time_elpased: 2.344
batch start
#iterations: 248
currently lose_sum: 79.4822099506855
time_elpased: 2.332
batch start
#iterations: 249
currently lose_sum: 79.20445328950882
time_elpased: 2.302
batch start
#iterations: 250
currently lose_sum: 79.01010182499886
time_elpased: 2.415
batch start
#iterations: 251
currently lose_sum: 78.45005413889885
time_elpased: 2.413
batch start
#iterations: 252
currently lose_sum: 78.88960897922516
time_elpased: 2.442
batch start
#iterations: 253
currently lose_sum: 78.7013758122921
time_elpased: 2.382
batch start
#iterations: 254
currently lose_sum: 78.87073886394501
time_elpased: 2.411
batch start
#iterations: 255
currently lose_sum: 78.25749969482422
time_elpased: 2.446
batch start
#iterations: 256
currently lose_sum: 78.84041181206703
time_elpased: 2.499
batch start
#iterations: 257
currently lose_sum: 78.31190821528435
time_elpased: 2.336
batch start
#iterations: 258
currently lose_sum: 78.84020754694939
time_elpased: 2.35
batch start
#iterations: 259
currently lose_sum: 78.45017671585083
time_elpased: 2.304
start validation test
0.548144329897
0.611908684396
0.267572295976
0.372332808249
0.548636917378
88.216
batch start
#iterations: 260
currently lose_sum: 78.24674206972122
time_elpased: 2.43
batch start
#iterations: 261
currently lose_sum: 78.12707096338272
time_elpased: 2.404
batch start
#iterations: 262
currently lose_sum: 77.90679204463959
time_elpased: 2.463
batch start
#iterations: 263
currently lose_sum: 77.96018934249878
time_elpased: 2.422
batch start
#iterations: 264
currently lose_sum: 77.95348334312439
time_elpased: 2.448
batch start
#iterations: 265
currently lose_sum: 77.40233027935028
time_elpased: 2.436
batch start
#iterations: 266
currently lose_sum: 77.1153461933136
time_elpased: 2.458
batch start
#iterations: 267
currently lose_sum: 77.98789542913437
time_elpased: 2.441
batch start
#iterations: 268
currently lose_sum: 78.47087326645851
time_elpased: 2.702
batch start
#iterations: 269
currently lose_sum: 77.59326860308647
time_elpased: 2.519
batch start
#iterations: 270
currently lose_sum: 77.85798174142838
time_elpased: 2.583
batch start
#iterations: 271
currently lose_sum: 77.8547936975956
time_elpased: 2.465
batch start
#iterations: 272
currently lose_sum: 77.29201576113701
time_elpased: 2.503
batch start
#iterations: 273
currently lose_sum: 77.03970238566399
time_elpased: 2.397
batch start
#iterations: 274
currently lose_sum: 77.09347134828568
time_elpased: 2.541
batch start
#iterations: 275
currently lose_sum: 77.08297368884087
time_elpased: 2.418
batch start
#iterations: 276
currently lose_sum: 77.37481734156609
time_elpased: 2.412
batch start
#iterations: 277
currently lose_sum: 76.89058113098145
time_elpased: 2.466
batch start
#iterations: 278
currently lose_sum: 77.26792883872986
time_elpased: 2.433
batch start
#iterations: 279
currently lose_sum: 77.15465107560158
time_elpased: 2.722
start validation test
0.537474226804
0.582777036048
0.269527631985
0.368587713743
0.537944648379
97.493
batch start
#iterations: 280
currently lose_sum: 77.18274539709091
time_elpased: 2.518
batch start
#iterations: 281
currently lose_sum: 77.37892323732376
time_elpased: 2.505
batch start
#iterations: 282
currently lose_sum: 76.43518695235252
time_elpased: 2.572
batch start
#iterations: 283
currently lose_sum: 77.01000180840492
time_elpased: 2.565
batch start
#iterations: 284
currently lose_sum: 76.44341737031937
time_elpased: 2.606
batch start
#iterations: 285
currently lose_sum: 76.50699427723885
time_elpased: 2.477
batch start
#iterations: 286
currently lose_sum: 76.64622130990028
time_elpased: 2.46
batch start
#iterations: 287
currently lose_sum: 76.13995212316513
time_elpased: 2.422
batch start
#iterations: 288
currently lose_sum: 75.95138031244278
time_elpased: 2.499
batch start
#iterations: 289
currently lose_sum: 76.4807916879654
time_elpased: 2.43
batch start
#iterations: 290
currently lose_sum: 75.77897703647614
time_elpased: 2.588
batch start
#iterations: 291
currently lose_sum: 76.10527074337006
time_elpased: 2.536
batch start
#iterations: 292
currently lose_sum: 75.48097547888756
time_elpased: 2.491
batch start
#iterations: 293
currently lose_sum: 75.63596934080124
time_elpased: 2.437
batch start
#iterations: 294
currently lose_sum: 75.98435500264168
time_elpased: 2.565
batch start
#iterations: 295
currently lose_sum: 75.88037648797035
time_elpased: 2.554
batch start
#iterations: 296
currently lose_sum: 76.03071066737175
time_elpased: 2.41
batch start
#iterations: 297
currently lose_sum: 75.9705710709095
time_elpased: 2.36
batch start
#iterations: 298
currently lose_sum: 75.44020304083824
time_elpased: 2.425
batch start
#iterations: 299
currently lose_sum: 75.48174032568932
time_elpased: 2.47
start validation test
0.557628865979
0.60241833604
0.343521663065
0.437540962118
0.558004764198
86.468
batch start
#iterations: 300
currently lose_sum: 75.88525760173798
time_elpased: 2.373
batch start
#iterations: 301
currently lose_sum: 76.23848819732666
time_elpased: 2.329
batch start
#iterations: 302
currently lose_sum: 75.29415532946587
time_elpased: 2.347
batch start
#iterations: 303
currently lose_sum: 74.91983017325401
time_elpased: 2.364
batch start
#iterations: 304
currently lose_sum: 75.22022539377213
time_elpased: 2.516
batch start
#iterations: 305
currently lose_sum: 74.92709162831306
time_elpased: 2.162
batch start
#iterations: 306
currently lose_sum: 75.06031966209412
time_elpased: 2.395
batch start
#iterations: 307
currently lose_sum: 75.32458537817001
time_elpased: 2.449
batch start
#iterations: 308
currently lose_sum: 74.77717396616936
time_elpased: 2.335
batch start
#iterations: 309
currently lose_sum: 74.96549987792969
time_elpased: 2.305
batch start
#iterations: 310
currently lose_sum: 75.29686716198921
time_elpased: 2.401
batch start
#iterations: 311
currently lose_sum: 74.90059214830399
time_elpased: 2.379
batch start
#iterations: 312
currently lose_sum: 74.02777570486069
time_elpased: 2.477
batch start
#iterations: 313
currently lose_sum: 73.83693391084671
time_elpased: 2.313
batch start
#iterations: 314
currently lose_sum: 74.80801579356194
time_elpased: 2.382
batch start
#iterations: 315
currently lose_sum: 74.32085943222046
time_elpased: 2.37
batch start
#iterations: 316
currently lose_sum: 74.61749082803726
time_elpased: 2.342
batch start
#iterations: 317
currently lose_sum: 74.06738632917404
time_elpased: 2.4
batch start
#iterations: 318
currently lose_sum: 74.39002147316933
time_elpased: 2.472
batch start
#iterations: 319
currently lose_sum: 73.93724501132965
time_elpased: 2.425
start validation test
0.545515463918
0.589179548157
0.305958629207
0.402763665922
0.54593604289
97.137
batch start
#iterations: 320
currently lose_sum: 74.45290976762772
time_elpased: 2.479
batch start
#iterations: 321
currently lose_sum: 74.56069213151932
time_elpased: 2.428
batch start
#iterations: 322
currently lose_sum: 73.99721798300743
time_elpased: 2.466
batch start
#iterations: 323
currently lose_sum: 73.78227278590202
time_elpased: 2.574
batch start
#iterations: 324
currently lose_sum: 74.4169916510582
time_elpased: 2.493
batch start
#iterations: 325
currently lose_sum: 73.46108227968216
time_elpased: 2.45
batch start
#iterations: 326
currently lose_sum: 74.19400352239609
time_elpased: 2.526
batch start
#iterations: 327
currently lose_sum: 74.24952879548073
time_elpased: 2.473
batch start
#iterations: 328
currently lose_sum: 73.7633267045021
time_elpased: 2.478
batch start
#iterations: 329
currently lose_sum: 73.34417831897736
time_elpased: 2.481
batch start
#iterations: 330
currently lose_sum: 74.03650826215744
time_elpased: 2.468
batch start
#iterations: 331
currently lose_sum: 73.25763353705406
time_elpased: 2.417
batch start
#iterations: 332
currently lose_sum: 73.50501754879951
time_elpased: 2.451
batch start
#iterations: 333
currently lose_sum: 73.51500797271729
time_elpased: 2.435
batch start
#iterations: 334
currently lose_sum: 73.57424709200859
time_elpased: 2.429
batch start
#iterations: 335
currently lose_sum: 73.35323691368103
time_elpased: 2.387
batch start
#iterations: 336
currently lose_sum: 72.56894811987877
time_elpased: 2.346
batch start
#iterations: 337
currently lose_sum: 72.88445630669594
time_elpased: 2.489
batch start
#iterations: 338
currently lose_sum: 72.29513800144196
time_elpased: 2.435
batch start
#iterations: 339
currently lose_sum: 73.06910154223442
time_elpased: 2.491
start validation test
0.546597938144
0.581231257717
0.339096428939
0.428311451969
0.546962239049
93.488
batch start
#iterations: 340
currently lose_sum: 72.53396466374397
time_elpased: 2.564
batch start
#iterations: 341
currently lose_sum: 72.65634605288506
time_elpased: 2.387
batch start
#iterations: 342
currently lose_sum: 73.26030746102333
time_elpased: 2.355
batch start
#iterations: 343
currently lose_sum: 72.65186107158661
time_elpased: 2.412
batch start
#iterations: 344
currently lose_sum: 72.52961999177933
time_elpased: 2.525
batch start
#iterations: 345
currently lose_sum: 72.31112533807755
time_elpased: 2.604
batch start
#iterations: 346
currently lose_sum: 72.25374296307564
time_elpased: 2.552
batch start
#iterations: 347
currently lose_sum: 72.34291943907738
time_elpased: 2.431
batch start
#iterations: 348
currently lose_sum: 72.68830823898315
time_elpased: 2.554
batch start
#iterations: 349
currently lose_sum: 72.3936750292778
time_elpased: 2.438
batch start
#iterations: 350
currently lose_sum: 72.47560036182404
time_elpased: 2.367
batch start
#iterations: 351
currently lose_sum: 72.12667125463486
time_elpased: 2.453
batch start
#iterations: 352
currently lose_sum: 71.81320172548294
time_elpased: 2.381
batch start
#iterations: 353
currently lose_sum: 72.25076940655708
time_elpased: 2.479
batch start
#iterations: 354
currently lose_sum: 72.30406874418259
time_elpased: 2.665
batch start
#iterations: 355
currently lose_sum: 71.84346604347229
time_elpased: 2.469
batch start
#iterations: 356
currently lose_sum: 72.53470209240913
time_elpased: 2.508
batch start
#iterations: 357
currently lose_sum: 72.00619673728943
time_elpased: 2.369
batch start
#iterations: 358
currently lose_sum: 71.65862154960632
time_elpased: 2.534
batch start
#iterations: 359
currently lose_sum: 71.22892928123474
time_elpased: 2.543
start validation test
0.550309278351
0.593379725409
0.324688689925
0.41971531196
0.550705390093
99.104
batch start
#iterations: 360
currently lose_sum: 71.98575881123543
time_elpased: 2.485
batch start
#iterations: 361
currently lose_sum: 71.51798704266548
time_elpased: 2.446
batch start
#iterations: 362
currently lose_sum: nan
time_elpased: 2.483
train finish final lose is: nan
acc: 0.603
pre: 0.606
rec: 0.592
F1: 0.599
auc: 0.603
