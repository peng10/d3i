start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.46995776891708
time_elpased: 2.818
batch start
#iterations: 1
currently lose_sum: 100.15385007858276
time_elpased: 2.792
batch start
#iterations: 2
currently lose_sum: 99.96352583169937
time_elpased: 2.784
batch start
#iterations: 3
currently lose_sum: 99.7817000746727
time_elpased: 2.791
batch start
#iterations: 4
currently lose_sum: 99.68248385190964
time_elpased: 2.822
batch start
#iterations: 5
currently lose_sum: 99.49198538064957
time_elpased: 2.779
batch start
#iterations: 6
currently lose_sum: 99.33976626396179
time_elpased: 2.829
batch start
#iterations: 7
currently lose_sum: 99.40869015455246
time_elpased: 2.818
batch start
#iterations: 8
currently lose_sum: 99.31458193063736
time_elpased: 2.796
batch start
#iterations: 9
currently lose_sum: 98.99168974161148
time_elpased: 2.831
batch start
#iterations: 10
currently lose_sum: 99.00808817148209
time_elpased: 2.818
batch start
#iterations: 11
currently lose_sum: 98.89917373657227
time_elpased: 2.854
batch start
#iterations: 12
currently lose_sum: 98.89894539117813
time_elpased: 2.83
batch start
#iterations: 13
currently lose_sum: 98.92395305633545
time_elpased: 2.829
batch start
#iterations: 14
currently lose_sum: 98.65655165910721
time_elpased: 2.82
batch start
#iterations: 15
currently lose_sum: 98.60181659460068
time_elpased: 2.834
batch start
#iterations: 16
currently lose_sum: 98.34260368347168
time_elpased: 2.821
batch start
#iterations: 17
currently lose_sum: 98.25755441188812
time_elpased: 2.81
batch start
#iterations: 18
currently lose_sum: 98.38432031869888
time_elpased: 2.795
batch start
#iterations: 19
currently lose_sum: 98.11798471212387
time_elpased: 2.829
start validation test
0.603865979381
0.587285223368
0.703509313574
0.640164817156
0.603691040139
64.807
batch start
#iterations: 20
currently lose_sum: 98.22591418027878
time_elpased: 2.83
batch start
#iterations: 21
currently lose_sum: 98.0331711769104
time_elpased: 2.802
batch start
#iterations: 22
currently lose_sum: 97.92079281806946
time_elpased: 2.824
batch start
#iterations: 23
currently lose_sum: 98.13421297073364
time_elpased: 2.841
batch start
#iterations: 24
currently lose_sum: 97.77176076173782
time_elpased: 2.802
batch start
#iterations: 25
currently lose_sum: 97.89567428827286
time_elpased: 2.79
batch start
#iterations: 26
currently lose_sum: 97.51243937015533
time_elpased: 2.841
batch start
#iterations: 27
currently lose_sum: 97.77831649780273
time_elpased: 2.809
batch start
#iterations: 28
currently lose_sum: 97.73267662525177
time_elpased: 2.831
batch start
#iterations: 29
currently lose_sum: 97.59588253498077
time_elpased: 2.819
batch start
#iterations: 30
currently lose_sum: 97.48464262485504
time_elpased: 2.858
batch start
#iterations: 31
currently lose_sum: 97.55100482702255
time_elpased: 2.861
batch start
#iterations: 32
currently lose_sum: 97.55614626407623
time_elpased: 2.796
batch start
#iterations: 33
currently lose_sum: 97.4637975692749
time_elpased: 2.824
batch start
#iterations: 34
currently lose_sum: 97.02470362186432
time_elpased: 2.862
batch start
#iterations: 35
currently lose_sum: 97.14075803756714
time_elpased: 2.905
batch start
#iterations: 36
currently lose_sum: 97.20165705680847
time_elpased: 2.854
batch start
#iterations: 37
currently lose_sum: 97.22803419828415
time_elpased: 2.861
batch start
#iterations: 38
currently lose_sum: 96.7725231051445
time_elpased: 2.796
batch start
#iterations: 39
currently lose_sum: 96.85895103216171
time_elpased: 2.847
start validation test
0.612216494845
0.612674609696
0.613872594422
0.613273017015
0.612213587307
64.074
batch start
#iterations: 40
currently lose_sum: 96.8236101269722
time_elpased: 2.824
batch start
#iterations: 41
currently lose_sum: 96.68374985456467
time_elpased: 2.797
batch start
#iterations: 42
currently lose_sum: 96.47999119758606
time_elpased: 2.867
batch start
#iterations: 43
currently lose_sum: 96.75941777229309
time_elpased: 2.863
batch start
#iterations: 44
currently lose_sum: 96.69800108671188
time_elpased: 2.857
batch start
#iterations: 45
currently lose_sum: 96.38311129808426
time_elpased: 2.827
batch start
#iterations: 46
currently lose_sum: 96.3940681219101
time_elpased: 2.802
batch start
#iterations: 47
currently lose_sum: 96.38330167531967
time_elpased: 2.827
batch start
#iterations: 48
currently lose_sum: 96.27092880010605
time_elpased: 2.811
batch start
#iterations: 49
currently lose_sum: 96.17915111780167
time_elpased: 2.843
batch start
#iterations: 50
currently lose_sum: 96.50052917003632
time_elpased: 2.852
batch start
#iterations: 51
currently lose_sum: 96.09589374065399
time_elpased: 2.867
batch start
#iterations: 52
currently lose_sum: 96.12968522310257
time_elpased: 2.863
batch start
#iterations: 53
currently lose_sum: 95.83928829431534
time_elpased: 2.837
batch start
#iterations: 54
currently lose_sum: 96.04460281133652
time_elpased: 2.852
batch start
#iterations: 55
currently lose_sum: 96.01242464780807
time_elpased: 2.876
batch start
#iterations: 56
currently lose_sum: 95.64325481653214
time_elpased: 2.904
batch start
#iterations: 57
currently lose_sum: 95.70742720365524
time_elpased: 2.928
batch start
#iterations: 58
currently lose_sum: 95.45833843946457
time_elpased: 2.973
batch start
#iterations: 59
currently lose_sum: 95.90007781982422
time_elpased: 2.94
start validation test
0.596907216495
0.63606369244
0.456313677061
0.531399808245
0.597154050138
64.668
batch start
#iterations: 60
currently lose_sum: 95.18128538131714
time_elpased: 2.887
batch start
#iterations: 61
currently lose_sum: 95.58556020259857
time_elpased: 2.882
batch start
#iterations: 62
currently lose_sum: 95.66846817731857
time_elpased: 2.864
batch start
#iterations: 63
currently lose_sum: 94.72305518388748
time_elpased: 2.88
batch start
#iterations: 64
currently lose_sum: 94.87110334634781
time_elpased: 2.925
batch start
#iterations: 65
currently lose_sum: 95.07702451944351
time_elpased: 2.93
batch start
#iterations: 66
currently lose_sum: 95.26681989431381
time_elpased: 2.971
batch start
#iterations: 67
currently lose_sum: 94.74853873252869
time_elpased: 2.879
batch start
#iterations: 68
currently lose_sum: 94.86033165454865
time_elpased: 2.919
batch start
#iterations: 69
currently lose_sum: 94.57028859853745
time_elpased: 2.992
batch start
#iterations: 70
currently lose_sum: 94.63764363527298
time_elpased: 2.914
batch start
#iterations: 71
currently lose_sum: 94.16516637802124
time_elpased: 2.935
batch start
#iterations: 72
currently lose_sum: 94.11862540245056
time_elpased: 2.939
batch start
#iterations: 73
currently lose_sum: 94.24702274799347
time_elpased: 2.898
batch start
#iterations: 74
currently lose_sum: 94.57709378004074
time_elpased: 2.912
batch start
#iterations: 75
currently lose_sum: 94.199238717556
time_elpased: 2.962
batch start
#iterations: 76
currently lose_sum: 94.19569092988968
time_elpased: 2.956
batch start
#iterations: 77
currently lose_sum: 94.06324601173401
time_elpased: 2.929
batch start
#iterations: 78
currently lose_sum: 94.09490412473679
time_elpased: 2.952
batch start
#iterations: 79
currently lose_sum: 93.61634004116058
time_elpased: 2.941
start validation test
0.589020618557
0.614948589507
0.480086446434
0.539212853262
0.589211869298
65.881
batch start
#iterations: 80
currently lose_sum: 93.78147453069687
time_elpased: 2.897
batch start
#iterations: 81
currently lose_sum: 93.24731254577637
time_elpased: 2.909
batch start
#iterations: 82
currently lose_sum: 93.53996533155441
time_elpased: 2.896
batch start
#iterations: 83
currently lose_sum: 93.16703450679779
time_elpased: 2.945
batch start
#iterations: 84
currently lose_sum: 93.042196393013
time_elpased: 3.004
batch start
#iterations: 85
currently lose_sum: 93.36238092184067
time_elpased: 2.996
batch start
#iterations: 86
currently lose_sum: 92.85445463657379
time_elpased: 2.867
batch start
#iterations: 87
currently lose_sum: 93.31931364536285
time_elpased: 2.875
batch start
#iterations: 88
currently lose_sum: 92.79970687627792
time_elpased: 2.876
batch start
#iterations: 89
currently lose_sum: 93.04424571990967
time_elpased: 2.846
batch start
#iterations: 90
currently lose_sum: 92.34190315008163
time_elpased: 2.828
batch start
#iterations: 91
currently lose_sum: 92.6514459848404
time_elpased: 2.919
batch start
#iterations: 92
currently lose_sum: 92.05027920007706
time_elpased: 2.888
batch start
#iterations: 93
currently lose_sum: 92.4498980641365
time_elpased: 2.886
batch start
#iterations: 94
currently lose_sum: 91.89659523963928
time_elpased: 2.874
batch start
#iterations: 95
currently lose_sum: 92.01648390293121
time_elpased: 2.902
batch start
#iterations: 96
currently lose_sum: 91.85439991950989
time_elpased: 2.86
batch start
#iterations: 97
currently lose_sum: 91.79687631130219
time_elpased: 2.889
batch start
#iterations: 98
currently lose_sum: 91.93775534629822
time_elpased: 2.883
batch start
#iterations: 99
currently lose_sum: 91.41350424289703
time_elpased: 2.938
start validation test
0.58675257732
0.628398791541
0.428115673562
0.509273428414
0.587031088872
67.874
batch start
#iterations: 100
currently lose_sum: 91.6149622797966
time_elpased: 2.927
batch start
#iterations: 101
currently lose_sum: 91.40696287155151
time_elpased: 2.902
batch start
#iterations: 102
currently lose_sum: 91.48288595676422
time_elpased: 2.886
batch start
#iterations: 103
currently lose_sum: 90.77630859613419
time_elpased: 2.955
batch start
#iterations: 104
currently lose_sum: 91.36644142866135
time_elpased: 2.949
batch start
#iterations: 105
currently lose_sum: 90.49253410100937
time_elpased: 2.953
batch start
#iterations: 106
currently lose_sum: 90.41858971118927
time_elpased: 2.864
batch start
#iterations: 107
currently lose_sum: 91.29431265592575
time_elpased: 2.956
batch start
#iterations: 108
currently lose_sum: 90.80848097801208
time_elpased: 2.971
batch start
#iterations: 109
currently lose_sum: 90.14165341854095
time_elpased: 2.987
batch start
#iterations: 110
currently lose_sum: 89.86798787117004
time_elpased: 2.906
batch start
#iterations: 111
currently lose_sum: 89.75825095176697
time_elpased: 2.91
batch start
#iterations: 112
currently lose_sum: 89.76767766475677
time_elpased: 2.972
batch start
#iterations: 113
currently lose_sum: 89.74764794111252
time_elpased: 2.995
batch start
#iterations: 114
currently lose_sum: 89.69486004114151
time_elpased: 2.938
batch start
#iterations: 115
currently lose_sum: 89.9127733707428
time_elpased: 2.926
batch start
#iterations: 116
currently lose_sum: 89.17823714017868
time_elpased: 2.848
batch start
#iterations: 117
currently lose_sum: 89.625921189785
time_elpased: 2.911
batch start
#iterations: 118
currently lose_sum: 89.46028763055801
time_elpased: 3.028
batch start
#iterations: 119
currently lose_sum: 89.30813908576965
time_elpased: 2.958
start validation test
0.583762886598
0.62137788291
0.432540907688
0.510041866392
0.584028380106
69.816
batch start
#iterations: 120
currently lose_sum: 89.11404067277908
time_elpased: 2.872
batch start
#iterations: 121
currently lose_sum: 88.19698548316956
time_elpased: 2.917
batch start
#iterations: 122
currently lose_sum: 89.17708253860474
time_elpased: 2.893
batch start
#iterations: 123
currently lose_sum: 88.62138503789902
time_elpased: 2.981
batch start
#iterations: 124
currently lose_sum: 88.51166617870331
time_elpased: 2.919
batch start
#iterations: 125
currently lose_sum: 88.78920322656631
time_elpased: 2.935
batch start
#iterations: 126
currently lose_sum: 88.7888086438179
time_elpased: 3.044
batch start
#iterations: 127
currently lose_sum: 88.30529361963272
time_elpased: 2.932
batch start
#iterations: 128
currently lose_sum: 87.18099576234818
time_elpased: 2.878
batch start
#iterations: 129
currently lose_sum: 87.67451220750809
time_elpased: 2.866
batch start
#iterations: 130
currently lose_sum: 87.47296833992004
time_elpased: 2.927
batch start
#iterations: 131
currently lose_sum: 87.7191818356514
time_elpased: 3.008
batch start
#iterations: 132
currently lose_sum: 87.89599162340164
time_elpased: 2.938
batch start
#iterations: 133
currently lose_sum: 87.48518884181976
time_elpased: 2.839
batch start
#iterations: 134
currently lose_sum: 87.22698223590851
time_elpased: 2.782
batch start
#iterations: 135
currently lose_sum: 87.26021981239319
time_elpased: 2.812
batch start
#iterations: 136
currently lose_sum: 85.89193612337112
time_elpased: 2.847
batch start
#iterations: 137
currently lose_sum: 86.97668242454529
time_elpased: 2.899
batch start
#iterations: 138
currently lose_sum: 86.54459697008133
time_elpased: 2.816
batch start
#iterations: 139
currently lose_sum: 86.23398226499557
time_elpased: 2.831
start validation test
0.577731958763
0.601870407482
0.463620458989
0.523776305081
0.577932299101
72.999
batch start
#iterations: 140
currently lose_sum: 86.35818201303482
time_elpased: 2.859
batch start
#iterations: 141
currently lose_sum: 86.55851000547409
time_elpased: 2.871
batch start
#iterations: 142
currently lose_sum: 86.33398401737213
time_elpased: 2.726
batch start
#iterations: 143
currently lose_sum: 86.076584815979
time_elpased: 2.846
batch start
#iterations: 144
currently lose_sum: 85.68718355894089
time_elpased: 2.835
batch start
#iterations: 145
currently lose_sum: 85.5473296046257
time_elpased: 2.867
batch start
#iterations: 146
currently lose_sum: 85.59606766700745
time_elpased: 2.902
batch start
#iterations: 147
currently lose_sum: 85.3803648352623
time_elpased: 2.921
batch start
#iterations: 148
currently lose_sum: 85.40850114822388
time_elpased: 2.877
batch start
#iterations: 149
currently lose_sum: 85.27000802755356
time_elpased: 2.848
batch start
#iterations: 150
currently lose_sum: 84.94582140445709
time_elpased: 2.908
batch start
#iterations: 151
currently lose_sum: 85.1615794301033
time_elpased: 2.906
batch start
#iterations: 152
currently lose_sum: 84.24089515209198
time_elpased: 2.906
batch start
#iterations: 153
currently lose_sum: 84.45956248044968
time_elpased: 2.909
batch start
#iterations: 154
currently lose_sum: 84.60811179876328
time_elpased: 2.865
batch start
#iterations: 155
currently lose_sum: 84.70596355199814
time_elpased: 2.904
batch start
#iterations: 156
currently lose_sum: 83.81245118379593
time_elpased: 2.906
batch start
#iterations: 157
currently lose_sum: 83.25631320476532
time_elpased: 2.927
batch start
#iterations: 158
currently lose_sum: 83.65508508682251
time_elpased: 2.922
batch start
#iterations: 159
currently lose_sum: 83.1281618475914
time_elpased: 2.894
start validation test
0.572268041237
0.597983703908
0.445610785222
0.51067342847
0.572490407586
76.969
batch start
#iterations: 160
currently lose_sum: 82.72625905275345
time_elpased: 2.983
batch start
#iterations: 161
currently lose_sum: 83.1280426979065
time_elpased: 2.961
batch start
#iterations: 162
currently lose_sum: 82.99924728274345
time_elpased: 2.972
batch start
#iterations: 163
currently lose_sum: 83.04834407567978
time_elpased: 2.975
batch start
#iterations: 164
currently lose_sum: 82.50669580698013
time_elpased: 2.95
batch start
#iterations: 165
currently lose_sum: 82.25677174329758
time_elpased: 2.912
batch start
#iterations: 166
currently lose_sum: 81.72829994559288
time_elpased: 2.922
batch start
#iterations: 167
currently lose_sum: 82.11977514624596
time_elpased: 2.984
batch start
#iterations: 168
currently lose_sum: 82.20244067907333
time_elpased: 2.923
batch start
#iterations: 169
currently lose_sum: 81.55399051308632
time_elpased: 2.859
batch start
#iterations: 170
currently lose_sum: 81.60325866937637
time_elpased: 2.856
batch start
#iterations: 171
currently lose_sum: 81.24942791461945
time_elpased: 2.977
batch start
#iterations: 172
currently lose_sum: 80.72460770606995
time_elpased: 2.899
batch start
#iterations: 173
currently lose_sum: 81.0625481903553
time_elpased: 2.953
batch start
#iterations: 174
currently lose_sum: 80.67086911201477
time_elpased: 2.953
batch start
#iterations: 175
currently lose_sum: 80.51081937551498
time_elpased: 2.922
batch start
#iterations: 176
currently lose_sum: 80.25179919600487
time_elpased: 2.932
batch start
#iterations: 177
currently lose_sum: 79.94576025009155
time_elpased: 2.907
batch start
#iterations: 178
currently lose_sum: 80.60662341117859
time_elpased: 2.904
batch start
#iterations: 179
currently lose_sum: 79.72783786058426
time_elpased: 2.957
start validation test
0.561443298969
0.591632560255
0.401667181229
0.478484737036
0.561723810588
85.797
batch start
#iterations: 180
currently lose_sum: 79.91506478190422
time_elpased: 2.958
batch start
#iterations: 181
currently lose_sum: 79.29718589782715
time_elpased: 2.812
batch start
#iterations: 182
currently lose_sum: 79.66521233320236
time_elpased: 2.788
batch start
#iterations: 183
currently lose_sum: 79.40952095389366
time_elpased: 2.822
batch start
#iterations: 184
currently lose_sum: 78.52806210517883
time_elpased: 2.92
batch start
#iterations: 185
currently lose_sum: 78.59937167167664
time_elpased: 2.931
batch start
#iterations: 186
currently lose_sum: 79.40124359726906
time_elpased: 2.914
batch start
#iterations: 187
currently lose_sum: 78.61984556913376
time_elpased: 2.9
batch start
#iterations: 188
currently lose_sum: 77.71748939156532
time_elpased: 2.891
batch start
#iterations: 189
currently lose_sum: 77.48936620354652
time_elpased: 2.883
batch start
#iterations: 190
currently lose_sum: 77.56573557853699
time_elpased: 2.845
batch start
#iterations: 191
currently lose_sum: 77.73155725002289
time_elpased: 2.823
batch start
#iterations: 192
currently lose_sum: 78.2901120185852
time_elpased: 2.891
batch start
#iterations: 193
currently lose_sum: 77.38664877414703
time_elpased: 2.93
batch start
#iterations: 194
currently lose_sum: 76.75188931822777
time_elpased: 3.03
batch start
#iterations: 195
currently lose_sum: 76.59286522865295
time_elpased: 2.948
batch start
#iterations: 196
currently lose_sum: 76.86377945542336
time_elpased: 2.94
batch start
#iterations: 197
currently lose_sum: 76.09531760215759
time_elpased: 2.96
batch start
#iterations: 198
currently lose_sum: 77.1982230246067
time_elpased: 2.939
batch start
#iterations: 199
currently lose_sum: 77.35363155603409
time_elpased: 2.894
start validation test
0.566391752577
0.604785611049
0.387568179479
0.472403411942
0.566705704942
88.970
batch start
#iterations: 200
currently lose_sum: 76.3594876229763
time_elpased: 2.88
batch start
#iterations: 201
currently lose_sum: 76.11530700325966
time_elpased: 2.914
batch start
#iterations: 202
currently lose_sum: 75.56332680583
time_elpased: 2.871
batch start
#iterations: 203
currently lose_sum: 75.29727885127068
time_elpased: 2.991
batch start
#iterations: 204
currently lose_sum: 74.66739693284035
time_elpased: 2.852
batch start
#iterations: 205
currently lose_sum: 75.23498022556305
time_elpased: 2.899
batch start
#iterations: 206
currently lose_sum: 75.23152497410774
time_elpased: 2.958
batch start
#iterations: 207
currently lose_sum: 74.9013243317604
time_elpased: 2.873
batch start
#iterations: 208
currently lose_sum: 75.4281587600708
time_elpased: 2.916
batch start
#iterations: 209
currently lose_sum: 73.93144768476486
time_elpased: 2.938
batch start
#iterations: 210
currently lose_sum: 73.99511620402336
time_elpased: 2.993
batch start
#iterations: 211
currently lose_sum: 74.24306789040565
time_elpased: 2.973
batch start
#iterations: 212
currently lose_sum: 73.5072018802166
time_elpased: 2.882
batch start
#iterations: 213
currently lose_sum: 72.89983829855919
time_elpased: 2.911
batch start
#iterations: 214
currently lose_sum: 72.96338653564453
time_elpased: 2.865
batch start
#iterations: 215
currently lose_sum: 72.73196542263031
time_elpased: 2.901
batch start
#iterations: 216
currently lose_sum: 72.82120448350906
time_elpased: 2.903
batch start
#iterations: 217
currently lose_sum: 73.0459600687027
time_elpased: 2.87
batch start
#iterations: 218
currently lose_sum: 73.73000782728195
time_elpased: 2.885
batch start
#iterations: 219
currently lose_sum: 71.1686059832573
time_elpased: 2.979
start validation test
0.552835051546
0.591564147627
0.346403210868
0.436944246122
0.553197474483
105.384
batch start
#iterations: 220
currently lose_sum: 72.33940708637238
time_elpased: 2.926
batch start
#iterations: 221
currently lose_sum: 71.75095897912979
time_elpased: 2.935
batch start
#iterations: 222
currently lose_sum: 71.23575493693352
time_elpased: 2.895
batch start
#iterations: 223
currently lose_sum: 71.91675159335136
time_elpased: 2.919
batch start
#iterations: 224
currently lose_sum: 71.68693217635155
time_elpased: 2.936
batch start
#iterations: 225
currently lose_sum: 71.54246944189072
time_elpased: 2.815
batch start
#iterations: 226
currently lose_sum: 70.22469085454941
time_elpased: 2.851
batch start
#iterations: 227
currently lose_sum: 71.00084072351456
time_elpased: 2.945
batch start
#iterations: 228
currently lose_sum: 71.6188058257103
time_elpased: 3.004
batch start
#iterations: 229
currently lose_sum: 70.61568087339401
time_elpased: 2.918
batch start
#iterations: 230
currently lose_sum: 70.00643542408943
time_elpased: 2.851
batch start
#iterations: 231
currently lose_sum: 70.46428856253624
time_elpased: 2.857
batch start
#iterations: 232
currently lose_sum: 69.82426771521568
time_elpased: 2.831
batch start
#iterations: 233
currently lose_sum: 69.7449543774128
time_elpased: 2.863
batch start
#iterations: 234
currently lose_sum: 70.07289707660675
time_elpased: 2.854
batch start
#iterations: 235
currently lose_sum: 71.31245902180672
time_elpased: 2.901
batch start
#iterations: 236
currently lose_sum: 69.36832627654076
time_elpased: 2.861
batch start
#iterations: 237
currently lose_sum: 69.62338954210281
time_elpased: 2.856
batch start
#iterations: 238
currently lose_sum: 68.8862618803978
time_elpased: 2.922
batch start
#iterations: 239
currently lose_sum: 69.08854648470879
time_elpased: 2.986
start validation test
0.542680412371
0.598507810678
0.264176186066
0.366557189776
0.543169369497
114.128
batch start
#iterations: 240
currently lose_sum: 68.71157604455948
time_elpased: 2.876
batch start
#iterations: 241
currently lose_sum: 68.80643624067307
time_elpased: 2.905
batch start
#iterations: 242
currently lose_sum: 68.71165406703949
time_elpased: 2.949
batch start
#iterations: 243
currently lose_sum: 67.71844592690468
time_elpased: 2.893
batch start
#iterations: 244
currently lose_sum: 68.3200971186161
time_elpased: 2.887
batch start
#iterations: 245
currently lose_sum: 67.88704016804695
time_elpased: 2.877
batch start
#iterations: 246
currently lose_sum: 68.07508119940758
time_elpased: 2.92
batch start
#iterations: 247
currently lose_sum: 67.63379755616188
time_elpased: 2.9
batch start
#iterations: 248
currently lose_sum: 66.79043850302696
time_elpased: 2.873
batch start
#iterations: 249
currently lose_sum: 67.42961326241493
time_elpased: 2.895
batch start
#iterations: 250
currently lose_sum: 67.27132812142372
time_elpased: 2.873
batch start
#iterations: 251
currently lose_sum: 67.29648467898369
time_elpased: 2.893
batch start
#iterations: 252
currently lose_sum: 66.51213362812996
time_elpased: 2.881
batch start
#iterations: 253
currently lose_sum: 67.59028226137161
time_elpased: 2.884
batch start
#iterations: 254
currently lose_sum: 67.04584187269211
time_elpased: 2.886
batch start
#iterations: 255
currently lose_sum: 67.0680763721466
time_elpased: 2.938
batch start
#iterations: 256
currently lose_sum: 66.49569511413574
time_elpased: 2.973
batch start
#iterations: 257
currently lose_sum: 66.14367917180061
time_elpased: 2.982
batch start
#iterations: 258
currently lose_sum: 66.81533643603325
time_elpased: 2.873
batch start
#iterations: 259
currently lose_sum: 66.01502397656441
time_elpased: 2.82
start validation test
0.544381443299
0.606864654333
0.256560666872
0.360650994575
0.544886757065
131.074
batch start
#iterations: 260
currently lose_sum: 66.78708636760712
time_elpased: 2.909
batch start
#iterations: 261
currently lose_sum: 66.10545679926872
time_elpased: 2.952
batch start
#iterations: 262
currently lose_sum: 65.7676493525505
time_elpased: 2.958
batch start
#iterations: 263
currently lose_sum: 65.53285163640976
time_elpased: 2.903
batch start
#iterations: 264
currently lose_sum: 65.56951344013214
time_elpased: 2.971
batch start
#iterations: 265
currently lose_sum: 64.88959267735481
time_elpased: 2.945
batch start
#iterations: 266
currently lose_sum: 65.56660395860672
time_elpased: 2.89
batch start
#iterations: 267
currently lose_sum: 64.98672333359718
time_elpased: 2.948
batch start
#iterations: 268
currently lose_sum: 64.8310657441616
time_elpased: 2.934
batch start
#iterations: 269
currently lose_sum: nan
time_elpased: 2.948
train finish final lose is: nan
acc: 0.604
pre: 0.605
rec: 0.601
F1: 0.603
auc: 0.604
