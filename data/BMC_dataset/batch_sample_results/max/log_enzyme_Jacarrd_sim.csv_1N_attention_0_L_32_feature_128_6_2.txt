start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.42314434051514
time_elpased: 3.281
batch start
#iterations: 1
currently lose_sum: 100.13091850280762
time_elpased: 3.28
batch start
#iterations: 2
currently lose_sum: 99.95832246541977
time_elpased: 3.273
batch start
#iterations: 3
currently lose_sum: 99.74981647729874
time_elpased: 3.174
batch start
#iterations: 4
currently lose_sum: 99.6688734292984
time_elpased: 3.209
batch start
#iterations: 5
currently lose_sum: 99.39894413948059
time_elpased: 3.27
batch start
#iterations: 6
currently lose_sum: 99.3192321062088
time_elpased: 3.26
batch start
#iterations: 7
currently lose_sum: 99.35363465547562
time_elpased: 3.182
batch start
#iterations: 8
currently lose_sum: 99.35232818126678
time_elpased: 3.153
batch start
#iterations: 9
currently lose_sum: 99.04994130134583
time_elpased: 3.159
batch start
#iterations: 10
currently lose_sum: 99.0763373374939
time_elpased: 3.192
batch start
#iterations: 11
currently lose_sum: 99.01921766996384
time_elpased: 3.191
batch start
#iterations: 12
currently lose_sum: 98.96629106998444
time_elpased: 3.152
batch start
#iterations: 13
currently lose_sum: 98.95246285200119
time_elpased: 3.186
batch start
#iterations: 14
currently lose_sum: 98.63352119922638
time_elpased: 3.147
batch start
#iterations: 15
currently lose_sum: 98.6778729557991
time_elpased: 3.153
batch start
#iterations: 16
currently lose_sum: 98.39777392148972
time_elpased: 3.149
batch start
#iterations: 17
currently lose_sum: 98.3456420302391
time_elpased: 3.144
batch start
#iterations: 18
currently lose_sum: 98.47776108980179
time_elpased: 3.101
batch start
#iterations: 19
currently lose_sum: 98.23412764072418
time_elpased: 3.166
start validation test
0.609896907216
0.598568938629
0.671503550479
0.632942089436
0.609788747252
64.499
batch start
#iterations: 20
currently lose_sum: 98.38366115093231
time_elpased: 3.186
batch start
#iterations: 21
currently lose_sum: 98.02345657348633
time_elpased: 3.14
batch start
#iterations: 22
currently lose_sum: 97.97694778442383
time_elpased: 3.153
batch start
#iterations: 23
currently lose_sum: 98.24120432138443
time_elpased: 3.231
batch start
#iterations: 24
currently lose_sum: 97.82412993907928
time_elpased: 3.291
batch start
#iterations: 25
currently lose_sum: 97.9790603518486
time_elpased: 3.247
batch start
#iterations: 26
currently lose_sum: 97.68705075979233
time_elpased: 3.25
batch start
#iterations: 27
currently lose_sum: 97.80737513303757
time_elpased: 3.228
batch start
#iterations: 28
currently lose_sum: 97.95143204927444
time_elpased: 3.223
batch start
#iterations: 29
currently lose_sum: 97.79943352937698
time_elpased: 3.273
batch start
#iterations: 30
currently lose_sum: 97.65753269195557
time_elpased: 3.267
batch start
#iterations: 31
currently lose_sum: 97.63747298717499
time_elpased: 3.323
batch start
#iterations: 32
currently lose_sum: 97.69295328855515
time_elpased: 3.293
batch start
#iterations: 33
currently lose_sum: 97.64769285917282
time_elpased: 3.278
batch start
#iterations: 34
currently lose_sum: 97.15790575742722
time_elpased: 3.209
batch start
#iterations: 35
currently lose_sum: 97.26827865839005
time_elpased: 3.229
batch start
#iterations: 36
currently lose_sum: 97.28679895401001
time_elpased: 3.228
batch start
#iterations: 37
currently lose_sum: 97.38655114173889
time_elpased: 3.289
batch start
#iterations: 38
currently lose_sum: 97.13356429338455
time_elpased: 3.246
batch start
#iterations: 39
currently lose_sum: 97.01273369789124
time_elpased: 3.179
start validation test
0.618917525773
0.61881390593
0.622825975095
0.620813458481
0.618910663888
64.037
batch start
#iterations: 40
currently lose_sum: 97.05945986509323
time_elpased: 3.134
batch start
#iterations: 41
currently lose_sum: 96.9431266784668
time_elpased: 3.197
batch start
#iterations: 42
currently lose_sum: 96.92254328727722
time_elpased: 3.234
batch start
#iterations: 43
currently lose_sum: 96.95897287130356
time_elpased: 3.259
batch start
#iterations: 44
currently lose_sum: 97.00525391101837
time_elpased: 3.257
batch start
#iterations: 45
currently lose_sum: 96.73207730054855
time_elpased: 3.209
batch start
#iterations: 46
currently lose_sum: 96.64191257953644
time_elpased: 3.18
batch start
#iterations: 47
currently lose_sum: 96.59366250038147
time_elpased: 3.239
batch start
#iterations: 48
currently lose_sum: 96.46234929561615
time_elpased: 3.236
batch start
#iterations: 49
currently lose_sum: 96.48608255386353
time_elpased: 3.192
batch start
#iterations: 50
currently lose_sum: 96.61764043569565
time_elpased: 3.252
batch start
#iterations: 51
currently lose_sum: 96.4857873916626
time_elpased: 3.303
batch start
#iterations: 52
currently lose_sum: 96.26108515262604
time_elpased: 3.296
batch start
#iterations: 53
currently lose_sum: 96.00236928462982
time_elpased: 3.241
batch start
#iterations: 54
currently lose_sum: 96.20982730388641
time_elpased: 3.197
batch start
#iterations: 55
currently lose_sum: 96.1850380897522
time_elpased: 3.235
batch start
#iterations: 56
currently lose_sum: 95.89275753498077
time_elpased: 3.223
batch start
#iterations: 57
currently lose_sum: 96.13348013162613
time_elpased: 3.22
batch start
#iterations: 58
currently lose_sum: 95.86117976903915
time_elpased: 3.184
batch start
#iterations: 59
currently lose_sum: 95.92447805404663
time_elpased: 3.245
start validation test
0.611649484536
0.630702909831
0.542039724195
0.583019703343
0.611771695207
64.116
batch start
#iterations: 60
currently lose_sum: 95.38454735279083
time_elpased: 3.224
batch start
#iterations: 61
currently lose_sum: 95.91291999816895
time_elpased: 3.203
batch start
#iterations: 62
currently lose_sum: 95.94123578071594
time_elpased: 3.182
batch start
#iterations: 63
currently lose_sum: 95.16146266460419
time_elpased: 3.195
batch start
#iterations: 64
currently lose_sum: 95.25234895944595
time_elpased: 3.182
batch start
#iterations: 65
currently lose_sum: 95.37090730667114
time_elpased: 3.252
batch start
#iterations: 66
currently lose_sum: 95.22756832838058
time_elpased: 3.225
batch start
#iterations: 67
currently lose_sum: 94.82746255397797
time_elpased: 3.169
batch start
#iterations: 68
currently lose_sum: 95.14384919404984
time_elpased: 3.236
batch start
#iterations: 69
currently lose_sum: 94.8712385892868
time_elpased: 3.371
batch start
#iterations: 70
currently lose_sum: 95.0882215499878
time_elpased: 3.221
batch start
#iterations: 71
currently lose_sum: 94.72716945409775
time_elpased: 3.137
batch start
#iterations: 72
currently lose_sum: 94.62168848514557
time_elpased: 3.187
batch start
#iterations: 73
currently lose_sum: 94.42954355478287
time_elpased: 3.227
batch start
#iterations: 74
currently lose_sum: 95.13945358991623
time_elpased: 3.215
batch start
#iterations: 75
currently lose_sum: 94.65182322263718
time_elpased: 3.254
batch start
#iterations: 76
currently lose_sum: 94.52883154153824
time_elpased: 3.251
batch start
#iterations: 77
currently lose_sum: 94.3603744506836
time_elpased: 3.249
batch start
#iterations: 78
currently lose_sum: 94.69482457637787
time_elpased: 3.282
batch start
#iterations: 79
currently lose_sum: 94.09505397081375
time_elpased: 3.174
start validation test
0.601855670103
0.641007499646
0.466193269528
0.539799809342
0.602093846372
65.214
batch start
#iterations: 80
currently lose_sum: 94.3829939365387
time_elpased: 3.237
batch start
#iterations: 81
currently lose_sum: 93.82835727930069
time_elpased: 3.299
batch start
#iterations: 82
currently lose_sum: 94.06474667787552
time_elpased: 3.257
batch start
#iterations: 83
currently lose_sum: 93.54213386774063
time_elpased: 3.214
batch start
#iterations: 84
currently lose_sum: 93.60368347167969
time_elpased: 3.19
batch start
#iterations: 85
currently lose_sum: 93.89496624469757
time_elpased: 3.144
batch start
#iterations: 86
currently lose_sum: 93.74502515792847
time_elpased: 3.194
batch start
#iterations: 87
currently lose_sum: 94.15627884864807
time_elpased: 3.182
batch start
#iterations: 88
currently lose_sum: 93.53766214847565
time_elpased: 3.221
batch start
#iterations: 89
currently lose_sum: 93.64332240819931
time_elpased: 3.212
batch start
#iterations: 90
currently lose_sum: 93.23893177509308
time_elpased: 3.205
batch start
#iterations: 91
currently lose_sum: 93.21307891607285
time_elpased: 3.198
batch start
#iterations: 92
currently lose_sum: 92.81747108697891
time_elpased: 3.15
batch start
#iterations: 93
currently lose_sum: 93.16240763664246
time_elpased: 3.246
batch start
#iterations: 94
currently lose_sum: 92.9696751832962
time_elpased: 3.157
batch start
#iterations: 95
currently lose_sum: 92.8959801197052
time_elpased: 3.23
batch start
#iterations: 96
currently lose_sum: 92.7214744091034
time_elpased: 3.185
batch start
#iterations: 97
currently lose_sum: 92.32177764177322
time_elpased: 3.185
batch start
#iterations: 98
currently lose_sum: 92.71911644935608
time_elpased: 3.233
batch start
#iterations: 99
currently lose_sum: 92.25468635559082
time_elpased: 3.24
start validation test
0.595103092784
0.622726074347
0.486158279304
0.546032479917
0.595294362207
67.929
batch start
#iterations: 100
currently lose_sum: 92.49423587322235
time_elpased: 3.217
batch start
#iterations: 101
currently lose_sum: 91.99085354804993
time_elpased: 3.193
batch start
#iterations: 102
currently lose_sum: 92.40421772003174
time_elpased: 3.22
batch start
#iterations: 103
currently lose_sum: 92.14427065849304
time_elpased: 3.188
batch start
#iterations: 104
currently lose_sum: 91.95402365922928
time_elpased: 3.216
batch start
#iterations: 105
currently lose_sum: 91.73673915863037
time_elpased: 3.19
batch start
#iterations: 106
currently lose_sum: 91.09896832704544
time_elpased: 3.187
batch start
#iterations: 107
currently lose_sum: 91.88808363676071
time_elpased: 3.314
batch start
#iterations: 108
currently lose_sum: 91.39901739358902
time_elpased: 3.247
batch start
#iterations: 109
currently lose_sum: 91.25781637430191
time_elpased: 3.26
batch start
#iterations: 110
currently lose_sum: 91.15308278799057
time_elpased: 3.247
batch start
#iterations: 111
currently lose_sum: 91.04757779836655
time_elpased: 3.21
batch start
#iterations: 112
currently lose_sum: 90.99901211261749
time_elpased: 3.268
batch start
#iterations: 113
currently lose_sum: 90.54856210947037
time_elpased: 3.228
batch start
#iterations: 114
currently lose_sum: 90.61299502849579
time_elpased: 3.234
batch start
#iterations: 115
currently lose_sum: 90.6889089345932
time_elpased: 3.229
batch start
#iterations: 116
currently lose_sum: 90.29188472032547
time_elpased: 3.219
batch start
#iterations: 117
currently lose_sum: 90.04247790575027
time_elpased: 3.198
batch start
#iterations: 118
currently lose_sum: 90.31598353385925
time_elpased: 3.275
batch start
#iterations: 119
currently lose_sum: 90.43711495399475
time_elpased: 3.237
start validation test
0.591701030928
0.625664707529
0.460121436657
0.530273379588
0.5919320392
68.503
batch start
#iterations: 120
currently lose_sum: 89.61100816726685
time_elpased: 3.179
batch start
#iterations: 121
currently lose_sum: 89.5770600438118
time_elpased: 3.202
batch start
#iterations: 122
currently lose_sum: 89.67294102907181
time_elpased: 3.168
batch start
#iterations: 123
currently lose_sum: 89.49582332372665
time_elpased: 3.27
batch start
#iterations: 124
currently lose_sum: 89.73306971788406
time_elpased: 3.288
batch start
#iterations: 125
currently lose_sum: 89.67394065856934
time_elpased: 3.327
batch start
#iterations: 126
currently lose_sum: 89.8641551733017
time_elpased: 3.303
batch start
#iterations: 127
currently lose_sum: 89.48218959569931
time_elpased: 3.173
batch start
#iterations: 128
currently lose_sum: 88.91325879096985
time_elpased: 3.17
batch start
#iterations: 129
currently lose_sum: 89.30840110778809
time_elpased: 3.179
batch start
#iterations: 130
currently lose_sum: 88.9200050830841
time_elpased: 3.213
batch start
#iterations: 131
currently lose_sum: 88.83639377355576
time_elpased: 3.167
batch start
#iterations: 132
currently lose_sum: 88.29005992412567
time_elpased: 3.177
batch start
#iterations: 133
currently lose_sum: 88.74770522117615
time_elpased: 3.186
batch start
#iterations: 134
currently lose_sum: 88.8382728099823
time_elpased: 3.154
batch start
#iterations: 135
currently lose_sum: 88.17756283283234
time_elpased: 3.171
batch start
#iterations: 136
currently lose_sum: 87.82028150558472
time_elpased: 3.143
batch start
#iterations: 137
currently lose_sum: 88.27071863412857
time_elpased: 3.207
batch start
#iterations: 138
currently lose_sum: 87.8733321428299
time_elpased: 3.236
batch start
#iterations: 139
currently lose_sum: 87.26420903205872
time_elpased: 3.238
start validation test
0.582731958763
0.636348352387
0.389523515488
0.483242898181
0.583071165985
73.393
batch start
#iterations: 140
currently lose_sum: 87.37531381845474
time_elpased: 3.217
batch start
#iterations: 141
currently lose_sum: 87.27004837989807
time_elpased: 3.252
batch start
#iterations: 142
currently lose_sum: 87.54568988084793
time_elpased: 3.22
batch start
#iterations: 143
currently lose_sum: 87.03812593221664
time_elpased: 3.244
batch start
#iterations: 144
currently lose_sum: 86.86897403001785
time_elpased: 3.228
batch start
#iterations: 145
currently lose_sum: 86.86556398868561
time_elpased: 3.355
batch start
#iterations: 146
currently lose_sum: 87.00237315893173
time_elpased: 3.222
batch start
#iterations: 147
currently lose_sum: 86.5446714758873
time_elpased: 3.238
batch start
#iterations: 148
currently lose_sum: 86.8142939209938
time_elpased: 3.214
batch start
#iterations: 149
currently lose_sum: 85.86035072803497
time_elpased: 3.222
batch start
#iterations: 150
currently lose_sum: 86.16912788152695
time_elpased: 3.26
batch start
#iterations: 151
currently lose_sum: 86.2396912574768
time_elpased: 3.223
batch start
#iterations: 152
currently lose_sum: 85.07169008255005
time_elpased: 3.268
batch start
#iterations: 153
currently lose_sum: 85.64355581998825
time_elpased: 3.206
batch start
#iterations: 154
currently lose_sum: 85.71821027994156
time_elpased: 3.177
batch start
#iterations: 155
currently lose_sum: 85.25919371843338
time_elpased: 3.186
batch start
#iterations: 156
currently lose_sum: 85.03618371486664
time_elpased: 3.251
batch start
#iterations: 157
currently lose_sum: 84.24264508485794
time_elpased: 3.217
batch start
#iterations: 158
currently lose_sum: 84.88222700357437
time_elpased: 3.28
batch start
#iterations: 159
currently lose_sum: 84.81221055984497
time_elpased: 3.25
start validation test
0.561082474227
0.562787296281
0.554389214778
0.558556690342
0.561094225276
81.505
batch start
#iterations: 160
currently lose_sum: 84.0936329960823
time_elpased: 3.155
batch start
#iterations: 161
currently lose_sum: 84.00315970182419
time_elpased: 3.212
batch start
#iterations: 162
currently lose_sum: 84.4007499217987
time_elpased: 3.229
batch start
#iterations: 163
currently lose_sum: 84.9864599108696
time_elpased: 3.242
batch start
#iterations: 164
currently lose_sum: 82.85017383098602
time_elpased: 3.219
batch start
#iterations: 165
currently lose_sum: 83.52810490131378
time_elpased: 3.227
batch start
#iterations: 166
currently lose_sum: 82.67750751972198
time_elpased: 3.256
batch start
#iterations: 167
currently lose_sum: 83.71408134698868
time_elpased: 3.198
batch start
#iterations: 168
currently lose_sum: 83.33789414167404
time_elpased: 3.221
batch start
#iterations: 169
currently lose_sum: 82.35825204849243
time_elpased: 3.297
batch start
#iterations: 170
currently lose_sum: 83.136763215065
time_elpased: 3.304
batch start
#iterations: 171
currently lose_sum: 82.9858866930008
time_elpased: 3.245
batch start
#iterations: 172
currently lose_sum: 82.14209580421448
time_elpased: 3.191
batch start
#iterations: 173
currently lose_sum: 81.77558860182762
time_elpased: 3.174
batch start
#iterations: 174
currently lose_sum: 82.31086257100105
time_elpased: 3.184
batch start
#iterations: 175
currently lose_sum: 81.63731718063354
time_elpased: 3.155
batch start
#iterations: 176
currently lose_sum: 81.66093733906746
time_elpased: 3.165
batch start
#iterations: 177
currently lose_sum: 81.38208821415901
time_elpased: 3.191
batch start
#iterations: 178
currently lose_sum: 81.2335284948349
time_elpased: 3.258
batch start
#iterations: 179
currently lose_sum: 80.81418806314468
time_elpased: 3.199
start validation test
0.572422680412
0.616519174041
0.387156529793
0.475630570833
0.572747943715
87.913
batch start
#iterations: 180
currently lose_sum: 80.82914987206459
time_elpased: 3.231
batch start
#iterations: 181
currently lose_sum: 80.86267799139023
time_elpased: 3.195
batch start
#iterations: 182
currently lose_sum: 80.13005715608597
time_elpased: 3.212
batch start
#iterations: 183
currently lose_sum: 80.24972629547119
time_elpased: 3.303
batch start
#iterations: 184
currently lose_sum: 80.6127960383892
time_elpased: 3.231
batch start
#iterations: 185
currently lose_sum: 79.96123746037483
time_elpased: 3.234
batch start
#iterations: 186
currently lose_sum: 80.18668028712273
time_elpased: 3.186
batch start
#iterations: 187
currently lose_sum: 79.4946171939373
time_elpased: 3.194
batch start
#iterations: 188
currently lose_sum: 79.0003467798233
time_elpased: 3.216
batch start
#iterations: 189
currently lose_sum: 78.8797777891159
time_elpased: 3.203
batch start
#iterations: 190
currently lose_sum: 78.92807114124298
time_elpased: 3.206
batch start
#iterations: 191
currently lose_sum: 78.43838804960251
time_elpased: 3.19
batch start
#iterations: 192
currently lose_sum: 78.25141477584839
time_elpased: 3.196
batch start
#iterations: 193
currently lose_sum: 77.66949108242989
time_elpased: 3.164
batch start
#iterations: 194
currently lose_sum: 78.5068148970604
time_elpased: 3.209
batch start
#iterations: 195
currently lose_sum: 78.40129888057709
time_elpased: 3.26
batch start
#iterations: 196
currently lose_sum: 77.429372549057
time_elpased: 3.203
batch start
#iterations: 197
currently lose_sum: 78.27883833646774
time_elpased: 3.235
batch start
#iterations: 198
currently lose_sum: 77.82454261183739
time_elpased: 3.246
batch start
#iterations: 199
currently lose_sum: 76.9181399345398
time_elpased: 3.212
start validation test
0.566597938144
0.601962922574
0.397653596789
0.478929102628
0.566894545993
96.109
batch start
#iterations: 200
currently lose_sum: 77.13197818398476
time_elpased: 3.262
batch start
#iterations: 201
currently lose_sum: 77.19281908869743
time_elpased: 3.211
batch start
#iterations: 202
currently lose_sum: 76.01877456903458
time_elpased: 3.196
batch start
#iterations: 203
currently lose_sum: 76.13211700320244
time_elpased: 3.164
batch start
#iterations: 204
currently lose_sum: 76.21809333562851
time_elpased: 3.212
batch start
#iterations: 205
currently lose_sum: 76.38812750577927
time_elpased: 3.21
batch start
#iterations: 206
currently lose_sum: 75.55194801092148
time_elpased: 3.242
batch start
#iterations: 207
currently lose_sum: 75.52585291862488
time_elpased: 3.253
batch start
#iterations: 208
currently lose_sum: 75.70821934938431
time_elpased: 3.219
batch start
#iterations: 209
currently lose_sum: 74.28125801682472
time_elpased: 3.226
batch start
#iterations: 210
currently lose_sum: 75.42581784725189
time_elpased: 3.278
batch start
#iterations: 211
currently lose_sum: 75.18321341276169
time_elpased: 3.223
batch start
#iterations: 212
currently lose_sum: 74.39789813756943
time_elpased: 3.218
batch start
#iterations: 213
currently lose_sum: 74.1315828859806
time_elpased: 3.311
batch start
#iterations: 214
currently lose_sum: 74.76740628480911
time_elpased: 3.255
batch start
#iterations: 215
currently lose_sum: 74.12837898731232
time_elpased: 3.23
batch start
#iterations: 216
currently lose_sum: 73.35462656617165
time_elpased: 3.236
batch start
#iterations: 217
currently lose_sum: 72.96006873250008
time_elpased: 3.224
batch start
#iterations: 218
currently lose_sum: 73.19177412986755
time_elpased: 3.214
batch start
#iterations: 219
currently lose_sum: 73.21058216691017
time_elpased: 3.205
start validation test
0.560721649485
0.601391481419
0.3647216219
0.454067905189
0.561065757764
107.973
batch start
#iterations: 220
currently lose_sum: 72.84484967589378
time_elpased: 3.22
batch start
#iterations: 221
currently lose_sum: 72.73206967115402
time_elpased: 3.174
batch start
#iterations: 222
currently lose_sum: 72.50005760788918
time_elpased: 3.162
batch start
#iterations: 223
currently lose_sum: 73.21441140770912
time_elpased: 3.145
batch start
#iterations: 224
currently lose_sum: 72.20814755558968
time_elpased: 3.244
batch start
#iterations: 225
currently lose_sum: 72.21959036588669
time_elpased: 3.216
batch start
#iterations: 226
currently lose_sum: 71.7819202542305
time_elpased: 3.358
batch start
#iterations: 227
currently lose_sum: 72.31133970618248
time_elpased: 3.256
batch start
#iterations: 228
currently lose_sum: 70.99087589979172
time_elpased: 3.249
batch start
#iterations: 229
currently lose_sum: 71.53261891007423
time_elpased: 3.219
batch start
#iterations: 230
currently lose_sum: 70.95398283004761
time_elpased: 3.192
batch start
#iterations: 231
currently lose_sum: 71.81977346539497
time_elpased: 3.189
batch start
#iterations: 232
currently lose_sum: 70.9821146428585
time_elpased: 3.202
batch start
#iterations: 233
currently lose_sum: 71.16636711359024
time_elpased: 3.227
batch start
#iterations: 234
currently lose_sum: 70.75322446227074
time_elpased: 3.174
batch start
#iterations: 235
currently lose_sum: 70.1555155813694
time_elpased: 3.237
batch start
#iterations: 236
currently lose_sum: 70.19180071353912
time_elpased: 3.266
batch start
#iterations: 237
currently lose_sum: 69.54669696092606
time_elpased: 3.21
batch start
#iterations: 238
currently lose_sum: 70.243701338768
time_elpased: 3.197
batch start
#iterations: 239
currently lose_sum: 69.94630479812622
time_elpased: 3.124
start validation test
0.552783505155
0.606594306779
0.30482659257
0.405753424658
0.553218831759
119.729
batch start
#iterations: 240
currently lose_sum: 69.48576441407204
time_elpased: 3.177
batch start
#iterations: 241
currently lose_sum: 70.66565835475922
time_elpased: 3.153
batch start
#iterations: 242
currently lose_sum: 68.87096005678177
time_elpased: 3.165
batch start
#iterations: 243
currently lose_sum: 68.62029021978378
time_elpased: 3.141
batch start
#iterations: 244
currently lose_sum: 68.64633560180664
time_elpased: 3.168
batch start
#iterations: 245
currently lose_sum: 68.78480318188667
time_elpased: 3.165
batch start
#iterations: 246
currently lose_sum: 69.06558552384377
time_elpased: 3.188
batch start
#iterations: 247
currently lose_sum: 68.86327368021011
time_elpased: 3.132
batch start
#iterations: 248
currently lose_sum: 68.24377048015594
time_elpased: 3.149
batch start
#iterations: 249
currently lose_sum: 67.97465997934341
time_elpased: 3.177
batch start
#iterations: 250
currently lose_sum: 67.83345392346382
time_elpased: 3.158
batch start
#iterations: 251
currently lose_sum: 67.67292839288712
time_elpased: 3.151
batch start
#iterations: 252
currently lose_sum: 66.68249866366386
time_elpased: 3.145
batch start
#iterations: 253
currently lose_sum: 67.7314992249012
time_elpased: 3.186
batch start
#iterations: 254
currently lose_sum: 68.08949157595634
time_elpased: 3.177
batch start
#iterations: 255
currently lose_sum: 67.7380621433258
time_elpased: 3.175
batch start
#iterations: 256
currently lose_sum: 67.21603235602379
time_elpased: 3.17
batch start
#iterations: 257
currently lose_sum: 66.2314288020134
time_elpased: 3.261
batch start
#iterations: 258
currently lose_sum: 67.49633973836899
time_elpased: 3.192
batch start
#iterations: 259
currently lose_sum: 66.1989229619503
time_elpased: 3.155
start validation test
0.550360824742
0.598964555954
0.30956056396
0.408168803854
0.550783586741
137.139
batch start
#iterations: 260
currently lose_sum: 67.42029318213463
time_elpased: 3.121
batch start
#iterations: 261
currently lose_sum: 67.33998852968216
time_elpased: 3.117
batch start
#iterations: 262
currently lose_sum: 67.23126423358917
time_elpased: 3.134
batch start
#iterations: 263
currently lose_sum: 66.06254622340202
time_elpased: 3.12
batch start
#iterations: 264
currently lose_sum: 65.93934816122055
time_elpased: 3.13
batch start
#iterations: 265
currently lose_sum: 67.0706125497818
time_elpased: 3.137
batch start
#iterations: 266
currently lose_sum: 65.32247787714005
time_elpased: 3.131
batch start
#iterations: 267
currently lose_sum: 65.32907757163048
time_elpased: 3.159
batch start
#iterations: 268
currently lose_sum: 65.6782369017601
time_elpased: 3.155
batch start
#iterations: 269
currently lose_sum: 65.48709660768509
time_elpased: 3.2
batch start
#iterations: 270
currently lose_sum: 65.48207125067711
time_elpased: 3.21
batch start
#iterations: 271
currently lose_sum: 65.58685094118118
time_elpased: 3.141
batch start
#iterations: 272
currently lose_sum: 65.1172958612442
time_elpased: 3.153
batch start
#iterations: 273
currently lose_sum: 65.37469214200974
time_elpased: 3.156
batch start
#iterations: 274
currently lose_sum: 65.05213937163353
time_elpased: 3.127
batch start
#iterations: 275
currently lose_sum: 65.3168713748455
time_elpased: 3.156
batch start
#iterations: 276
currently lose_sum: 65.16310501098633
time_elpased: 3.157
batch start
#iterations: 277
currently lose_sum: 64.33905804157257
time_elpased: 3.226
batch start
#iterations: 278
currently lose_sum: 64.67394277453423
time_elpased: 3.164
batch start
#iterations: 279
currently lose_sum: 64.9042846262455
time_elpased: 3.149
start validation test
0.538917525773
0.59159943047
0.256560666872
0.357906826502
0.53941324679
146.278
batch start
#iterations: 280
currently lose_sum: 64.86614817380905
time_elpased: 3.199
batch start
#iterations: 281
currently lose_sum: 64.46560645103455
time_elpased: 3.196
batch start
#iterations: 282
currently lose_sum: 64.61915558576584
time_elpased: 3.156
batch start
#iterations: 283
currently lose_sum: 65.14344051480293
time_elpased: 3.137
batch start
#iterations: 284
currently lose_sum: 64.26355284452438
time_elpased: 3.16
batch start
#iterations: 285
currently lose_sum: 63.95180317759514
time_elpased: 3.207
batch start
#iterations: 286
currently lose_sum: 64.21193841099739
time_elpased: 3.108
batch start
#iterations: 287
currently lose_sum: 64.07578098773956
time_elpased: 3.145
batch start
#iterations: 288
currently lose_sum: 63.687006801366806
time_elpased: 3.14
batch start
#iterations: 289
currently lose_sum: 63.41577562689781
time_elpased: 3.156
batch start
#iterations: 290
currently lose_sum: 63.152359545230865
time_elpased: 3.133
batch start
#iterations: 291
currently lose_sum: 64.70378908514977
time_elpased: 3.143
batch start
#iterations: 292
currently lose_sum: 63.37780573964119
time_elpased: 3.129
batch start
#iterations: 293
currently lose_sum: 64.11312708258629
time_elpased: 3.147
batch start
#iterations: 294
currently lose_sum: 62.879572838544846
time_elpased: 3.126
batch start
#iterations: 295
currently lose_sum: 62.796262949705124
time_elpased: 3.17
batch start
#iterations: 296
currently lose_sum: 63.91925084590912
time_elpased: 3.157
batch start
#iterations: 297
currently lose_sum: 64.14297726750374
time_elpased: 3.187
batch start
#iterations: 298
currently lose_sum: 63.802009373903275
time_elpased: 3.191
batch start
#iterations: 299
currently lose_sum: 63.932961881160736
time_elpased: 3.166
start validation test
0.544948453608
0.583255291253
0.320469280642
0.413655685441
0.54534256142
143.856
batch start
#iterations: 300
currently lose_sum: 62.54416072368622
time_elpased: 3.118
batch start
#iterations: 301
currently lose_sum: 62.99599200487137
time_elpased: 3.16
batch start
#iterations: 302
currently lose_sum: 63.13152787089348
time_elpased: 3.235
batch start
#iterations: 303
currently lose_sum: 62.06143733859062
time_elpased: 3.186
batch start
#iterations: 304
currently lose_sum: 63.333601385354996
time_elpased: 3.169
batch start
#iterations: 305
currently lose_sum: 62.89531686902046
time_elpased: 3.148
batch start
#iterations: 306
currently lose_sum: 63.2350557744503
time_elpased: 3.151
batch start
#iterations: 307
currently lose_sum: 62.61273953318596
time_elpased: 3.161
batch start
#iterations: 308
currently lose_sum: 62.54116368293762
time_elpased: 3.159
batch start
#iterations: 309
currently lose_sum: 61.804749965667725
time_elpased: 3.151
batch start
#iterations: 310
currently lose_sum: 61.99941810965538
time_elpased: 3.141
batch start
#iterations: 311
currently lose_sum: 62.789466857910156
time_elpased: 3.214
batch start
#iterations: 312
currently lose_sum: 60.88392972946167
time_elpased: 3.192
batch start
#iterations: 313
currently lose_sum: 60.61916419863701
time_elpased: 3.136
batch start
#iterations: 314
currently lose_sum: 61.0912301838398
time_elpased: 3.193
batch start
#iterations: 315
currently lose_sum: 62.2912133038044
time_elpased: 3.169
batch start
#iterations: 316
currently lose_sum: 61.5561908185482
time_elpased: 3.165
batch start
#iterations: 317
currently lose_sum: 60.560670137405396
time_elpased: 3.224
batch start
#iterations: 318
currently lose_sum: 62.07586884498596
time_elpased: 3.179
batch start
#iterations: 319
currently lose_sum: 61.586971431970596
time_elpased: 3.103
start validation test
0.544175257732
0.586397785686
0.305238242256
0.401489001692
0.544594748516
164.345
batch start
#iterations: 320
currently lose_sum: 61.73141860961914
time_elpased: 3.2
batch start
#iterations: 321
currently lose_sum: 61.54531842470169
time_elpased: 3.179
batch start
#iterations: 322
currently lose_sum: 61.48381435871124
time_elpased: 3.188
batch start
#iterations: 323
currently lose_sum: 61.425342470407486
time_elpased: 3.165
batch start
#iterations: 324
currently lose_sum: 61.11281907558441
time_elpased: 3.187
batch start
#iterations: 325
currently lose_sum: 60.15616047382355
time_elpased: 3.191
batch start
#iterations: 326
currently lose_sum: 60.15423637628555
time_elpased: 3.209
batch start
#iterations: 327
currently lose_sum: 61.56696233153343
time_elpased: 3.16
batch start
#iterations: 328
currently lose_sum: 61.46542555093765
time_elpased: 3.16
batch start
#iterations: 329
currently lose_sum: 62.03082627058029
time_elpased: 3.179
batch start
#iterations: 330
currently lose_sum: 61.00381448864937
time_elpased: 3.229
batch start
#iterations: 331
currently lose_sum: 60.18323394656181
time_elpased: 3.283
batch start
#iterations: 332
currently lose_sum: 60.8640903532505
time_elpased: 3.154
batch start
#iterations: 333
currently lose_sum: 62.400513023138046
time_elpased: 3.161
batch start
#iterations: 334
currently lose_sum: 61.38418146967888
time_elpased: 3.294
batch start
#iterations: 335
currently lose_sum: 59.92158263921738
time_elpased: 3.373
batch start
#iterations: 336
currently lose_sum: 60.21999964118004
time_elpased: 3.279
batch start
#iterations: 337
currently lose_sum: 60.67284694314003
time_elpased: 3.246
batch start
#iterations: 338
currently lose_sum: 61.062612026929855
time_elpased: 3.308
batch start
#iterations: 339
currently lose_sum: 60.239042431116104
time_elpased: 3.316
start validation test
0.539278350515
0.60296061327
0.234743233508
0.337925925926
0.539813008885
177.537
batch start
#iterations: 340
currently lose_sum: 59.639466762542725
time_elpased: 3.18
batch start
#iterations: 341
currently lose_sum: 59.89778193831444
time_elpased: 3.209
batch start
#iterations: 342
currently lose_sum: 61.40524062514305
time_elpased: 3.231
batch start
#iterations: 343
currently lose_sum: 61.95095819234848
time_elpased: 3.19
batch start
#iterations: 344
currently lose_sum: 59.378029465675354
time_elpased: 3.188
batch start
#iterations: 345
currently lose_sum: 59.72749701142311
time_elpased: 3.24
batch start
#iterations: 346
currently lose_sum: 59.79865527153015
time_elpased: 3.266
batch start
#iterations: 347
currently lose_sum: 59.816421687603
time_elpased: 3.292
batch start
#iterations: 348
currently lose_sum: 59.54507100582123
time_elpased: 3.28
batch start
#iterations: 349
currently lose_sum: 59.283284693956375
time_elpased: 3.151
batch start
#iterations: 350
currently lose_sum: 59.40026015043259
time_elpased: 3.191
batch start
#iterations: 351
currently lose_sum: 58.780770510435104
time_elpased: 3.236
batch start
#iterations: 352
currently lose_sum: 59.78860351443291
time_elpased: 3.216
batch start
#iterations: 353
currently lose_sum: 59.77447125315666
time_elpased: 3.162
batch start
#iterations: 354
currently lose_sum: 59.753714472055435
time_elpased: 3.203
batch start
#iterations: 355
currently lose_sum: 60.212926387786865
time_elpased: 3.201
batch start
#iterations: 356
currently lose_sum: 60.670300513505936
time_elpased: 3.165
batch start
#iterations: 357
currently lose_sum: 59.63645276427269
time_elpased: 3.206
batch start
#iterations: 358
currently lose_sum: 60.243054777383804
time_elpased: 3.206
batch start
#iterations: 359
currently lose_sum: 58.997640401124954
time_elpased: 3.244
start validation test
0.541701030928
0.579118773946
0.311104250283
0.404766686751
0.542105879143
180.616
batch start
#iterations: 360
currently lose_sum: 58.751406878232956
time_elpased: 3.224
batch start
#iterations: 361
currently lose_sum: 58.178916931152344
time_elpased: 3.24
batch start
#iterations: 362
currently lose_sum: 59.69108048081398
time_elpased: 3.18
batch start
#iterations: 363
currently lose_sum: 58.52229815721512
time_elpased: 3.186
batch start
#iterations: 364
currently lose_sum: 59.3146967291832
time_elpased: 3.215
batch start
#iterations: 365
currently lose_sum: 61.07034370303154
time_elpased: 3.209
batch start
#iterations: 366
currently lose_sum: 59.473292768001556
time_elpased: 3.329
batch start
#iterations: 367
currently lose_sum: 58.939415752887726
time_elpased: 3.227
batch start
#iterations: 368
currently lose_sum: 60.80117276310921
time_elpased: 3.219
batch start
#iterations: 369
currently lose_sum: 59.5257768034935
time_elpased: 3.299
batch start
#iterations: 370
currently lose_sum: 58.43568155169487
time_elpased: 3.205
batch start
#iterations: 371
currently lose_sum: 58.76003208756447
time_elpased: 3.183
batch start
#iterations: 372
currently lose_sum: 58.54931780695915
time_elpased: 3.228
batch start
#iterations: 373
currently lose_sum: 57.74930441379547
time_elpased: 3.188
batch start
#iterations: 374
currently lose_sum: 61.110545575618744
time_elpased: 3.27
batch start
#iterations: 375
currently lose_sum: 58.91578856110573
time_elpased: 3.238
batch start
#iterations: 376
currently lose_sum: 58.734039813280106
time_elpased: 3.277
batch start
#iterations: 377
currently lose_sum: 58.54920080304146
time_elpased: 3.251
batch start
#iterations: 378
currently lose_sum: 58.076807141304016
time_elpased: 3.227
batch start
#iterations: 379
currently lose_sum: 58.693324595689774
time_elpased: 3.173
start validation test
0.543505154639
0.598086124402
0.270145106514
0.372182050191
0.543985080367
185.267
batch start
#iterations: 380
currently lose_sum: 59.242548763751984
time_elpased: 3.241
batch start
#iterations: 381
currently lose_sum: 58.063526540994644
time_elpased: 3.237
batch start
#iterations: 382
currently lose_sum: 57.71984791755676
time_elpased: 3.245
batch start
#iterations: 383
currently lose_sum: 59.090261816978455
time_elpased: 3.284
batch start
#iterations: 384
currently lose_sum: 58.04843059182167
time_elpased: 3.26
batch start
#iterations: 385
currently lose_sum: 58.42257183790207
time_elpased: 3.288
batch start
#iterations: 386
currently lose_sum: 58.388250440359116
time_elpased: 3.232
batch start
#iterations: 387
currently lose_sum: 57.90105339884758
time_elpased: 3.261
batch start
#iterations: 388
currently lose_sum: 58.419303476810455
time_elpased: 3.279
batch start
#iterations: 389
currently lose_sum: 59.1672306060791
time_elpased: 3.231
batch start
#iterations: 390
currently lose_sum: 59.63146758079529
time_elpased: 3.273
batch start
#iterations: 391
currently lose_sum: 58.52838718891144
time_elpased: 3.31
batch start
#iterations: 392
currently lose_sum: 58.06069174408913
time_elpased: 3.224
batch start
#iterations: 393
currently lose_sum: 59.103395879268646
time_elpased: 3.208
batch start
#iterations: 394
currently lose_sum: 59.032949298620224
time_elpased: 3.271
batch start
#iterations: 395
currently lose_sum: 58.20135974884033
time_elpased: 3.188
batch start
#iterations: 396
currently lose_sum: 58.151118874549866
time_elpased: 3.26
batch start
#iterations: 397
currently lose_sum: 58.40239769220352
time_elpased: 3.217
batch start
#iterations: 398
currently lose_sum: 58.45800691843033
time_elpased: 3.182
batch start
#iterations: 399
currently lose_sum: 58.16771671175957
time_elpased: 3.185
start validation test
0.541082474227
0.598356694055
0.254811155706
0.357416095272
0.541585067681
189.452
acc: 0.615
pre: 0.616
rec: 0.614
F1: 0.615
auc: 0.615
