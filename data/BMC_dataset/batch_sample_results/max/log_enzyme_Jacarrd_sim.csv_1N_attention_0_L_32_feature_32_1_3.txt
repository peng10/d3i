start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.8024473786354
time_elpased: 2.04
batch start
#iterations: 1
currently lose_sum: 100.16293579339981
time_elpased: 2.015
batch start
#iterations: 2
currently lose_sum: 99.83856093883514
time_elpased: 2.128
batch start
#iterations: 3
currently lose_sum: 99.81805890798569
time_elpased: 2.058
batch start
#iterations: 4
currently lose_sum: 99.72676986455917
time_elpased: 2.011
batch start
#iterations: 5
currently lose_sum: 99.61636173725128
time_elpased: 1.969
batch start
#iterations: 6
currently lose_sum: 99.37971103191376
time_elpased: 2.098
batch start
#iterations: 7
currently lose_sum: 99.29487383365631
time_elpased: 2.058
batch start
#iterations: 8
currently lose_sum: 99.15554505586624
time_elpased: 2.021
batch start
#iterations: 9
currently lose_sum: 99.23006474971771
time_elpased: 2.223
batch start
#iterations: 10
currently lose_sum: 99.12472629547119
time_elpased: 2.102
batch start
#iterations: 11
currently lose_sum: 98.96765488386154
time_elpased: 1.866
batch start
#iterations: 12
currently lose_sum: 98.72064608335495
time_elpased: 1.996
batch start
#iterations: 13
currently lose_sum: 99.13772475719452
time_elpased: 1.979
batch start
#iterations: 14
currently lose_sum: 98.87780445814133
time_elpased: 2.167
batch start
#iterations: 15
currently lose_sum: 98.80426478385925
time_elpased: 2.004
batch start
#iterations: 16
currently lose_sum: 98.99154758453369
time_elpased: 2.257
batch start
#iterations: 17
currently lose_sum: 98.96560591459274
time_elpased: 2.113
batch start
#iterations: 18
currently lose_sum: 98.86310768127441
time_elpased: 1.999
batch start
#iterations: 19
currently lose_sum: 98.71305900812149
time_elpased: 1.892
start validation test
0.596134020619
0.618168111572
0.506381226842
0.556718528996
0.596282311067
65.069
batch start
#iterations: 20
currently lose_sum: 98.60828769207001
time_elpased: 1.879
batch start
#iterations: 21
currently lose_sum: 98.52607715129852
time_elpased: 1.863
batch start
#iterations: 22
currently lose_sum: 98.47822892665863
time_elpased: 1.894
batch start
#iterations: 23
currently lose_sum: 98.46406304836273
time_elpased: 1.882
batch start
#iterations: 24
currently lose_sum: 98.31500262022018
time_elpased: 1.953
batch start
#iterations: 25
currently lose_sum: 98.55875825881958
time_elpased: 1.981
batch start
#iterations: 26
currently lose_sum: 98.52634179592133
time_elpased: 1.979
batch start
#iterations: 27
currently lose_sum: 98.43355733156204
time_elpased: 2.037
batch start
#iterations: 28
currently lose_sum: 98.67324006557465
time_elpased: 1.956
batch start
#iterations: 29
currently lose_sum: 98.48886042833328
time_elpased: 1.993
batch start
#iterations: 30
currently lose_sum: 98.18297463655472
time_elpased: 1.996
batch start
#iterations: 31
currently lose_sum: 98.31721848249435
time_elpased: 1.995
batch start
#iterations: 32
currently lose_sum: 98.0276153087616
time_elpased: 1.962
batch start
#iterations: 33
currently lose_sum: 98.124187707901
time_elpased: 2.301
batch start
#iterations: 34
currently lose_sum: 98.09963434934616
time_elpased: 1.987
batch start
#iterations: 35
currently lose_sum: 98.3765070438385
time_elpased: 1.924
batch start
#iterations: 36
currently lose_sum: 98.29763704538345
time_elpased: 1.905
batch start
#iterations: 37
currently lose_sum: 98.07332962751389
time_elpased: 1.883
batch start
#iterations: 38
currently lose_sum: 97.99366092681885
time_elpased: 1.869
batch start
#iterations: 39
currently lose_sum: 97.88937401771545
time_elpased: 1.925
start validation test
0.608092783505
0.616572878738
0.575133799918
0.595132861175
0.608147238662
64.507
batch start
#iterations: 40
currently lose_sum: 97.96492552757263
time_elpased: 1.977
batch start
#iterations: 41
currently lose_sum: 97.9520046710968
time_elpased: 1.941
batch start
#iterations: 42
currently lose_sum: 97.93678945302963
time_elpased: 1.964
batch start
#iterations: 43
currently lose_sum: 97.97332561016083
time_elpased: 1.966
batch start
#iterations: 44
currently lose_sum: 97.71651536226273
time_elpased: 1.947
batch start
#iterations: 45
currently lose_sum: 97.90452456474304
time_elpased: 1.985
batch start
#iterations: 46
currently lose_sum: 97.73476493358612
time_elpased: 2.007
batch start
#iterations: 47
currently lose_sum: 97.72922819852829
time_elpased: 1.949
batch start
#iterations: 48
currently lose_sum: 97.83576637506485
time_elpased: 1.963
batch start
#iterations: 49
currently lose_sum: 97.76249551773071
time_elpased: 1.915
batch start
#iterations: 50
currently lose_sum: 97.74925595521927
time_elpased: 1.985
batch start
#iterations: 51
currently lose_sum: 97.81496512889862
time_elpased: 1.994
batch start
#iterations: 52
currently lose_sum: 97.42073059082031
time_elpased: 1.925
batch start
#iterations: 53
currently lose_sum: 97.44557404518127
time_elpased: 1.984
batch start
#iterations: 54
currently lose_sum: 97.65550237894058
time_elpased: 2.09
batch start
#iterations: 55
currently lose_sum: 97.49464517831802
time_elpased: 1.979
batch start
#iterations: 56
currently lose_sum: 97.85150414705276
time_elpased: 1.972
batch start
#iterations: 57
currently lose_sum: 97.48399859666824
time_elpased: 1.959
batch start
#iterations: 58
currently lose_sum: 97.26756119728088
time_elpased: 1.908
batch start
#iterations: 59
currently lose_sum: 97.64399510622025
time_elpased: 1.99
start validation test
0.583917525773
0.62859824781
0.413544668588
0.498882542836
0.584199017483
64.950
batch start
#iterations: 60
currently lose_sum: 97.46940165758133
time_elpased: 1.941
batch start
#iterations: 61
currently lose_sum: 97.71029132604599
time_elpased: 1.932
batch start
#iterations: 62
currently lose_sum: 97.56575280427933
time_elpased: 1.966
batch start
#iterations: 63
currently lose_sum: 97.32355290651321
time_elpased: 1.969
batch start
#iterations: 64
currently lose_sum: 97.49406856298447
time_elpased: 1.978
batch start
#iterations: 65
currently lose_sum: 97.53003019094467
time_elpased: 1.981
batch start
#iterations: 66
currently lose_sum: 97.49351334571838
time_elpased: 1.945
batch start
#iterations: 67
currently lose_sum: 97.33692294359207
time_elpased: 1.965
batch start
#iterations: 68
currently lose_sum: 97.15992546081543
time_elpased: 2.113
batch start
#iterations: 69
currently lose_sum: 97.21863102912903
time_elpased: 2.014
batch start
#iterations: 70
currently lose_sum: 97.27543234825134
time_elpased: 2.382
batch start
#iterations: 71
currently lose_sum: 97.03941124677658
time_elpased: 1.987
batch start
#iterations: 72
currently lose_sum: 97.29639536142349
time_elpased: 2.019
batch start
#iterations: 73
currently lose_sum: 97.08086276054382
time_elpased: 1.866
batch start
#iterations: 74
currently lose_sum: 97.02388316392899
time_elpased: 1.964
batch start
#iterations: 75
currently lose_sum: 97.25012457370758
time_elpased: 2.001
batch start
#iterations: 76
currently lose_sum: 96.95534551143646
time_elpased: 1.99
batch start
#iterations: 77
currently lose_sum: 97.28525531291962
time_elpased: 1.996
batch start
#iterations: 78
currently lose_sum: 97.12133622169495
time_elpased: 1.976
batch start
#iterations: 79
currently lose_sum: 97.178053855896
time_elpased: 1.943
start validation test
0.602164948454
0.616216845044
0.545183202964
0.578527741372
0.602259094254
64.404
batch start
#iterations: 80
currently lose_sum: 96.96525394916534
time_elpased: 1.952
batch start
#iterations: 81
currently lose_sum: 97.1124769449234
time_elpased: 1.958
batch start
#iterations: 82
currently lose_sum: 96.86718517541885
time_elpased: 1.962
batch start
#iterations: 83
currently lose_sum: 96.88483893871307
time_elpased: 1.979
batch start
#iterations: 84
currently lose_sum: 96.87567847967148
time_elpased: 1.975
batch start
#iterations: 85
currently lose_sum: 96.79536068439484
time_elpased: 2.009
batch start
#iterations: 86
currently lose_sum: 96.99045461416245
time_elpased: 1.892
batch start
#iterations: 87
currently lose_sum: 96.61970400810242
time_elpased: 1.862
batch start
#iterations: 88
currently lose_sum: 96.81412023305893
time_elpased: 1.934
batch start
#iterations: 89
currently lose_sum: 96.89304649829865
time_elpased: 1.983
batch start
#iterations: 90
currently lose_sum: 96.77699500322342
time_elpased: 2.04
batch start
#iterations: 91
currently lose_sum: 96.90451270341873
time_elpased: 2.009
batch start
#iterations: 92
currently lose_sum: 96.68962377309799
time_elpased: 2.112
batch start
#iterations: 93
currently lose_sum: 96.79740554094315
time_elpased: 2.067
batch start
#iterations: 94
currently lose_sum: 96.46410077810287
time_elpased: 2.016
batch start
#iterations: 95
currently lose_sum: 96.7660910487175
time_elpased: 1.881
batch start
#iterations: 96
currently lose_sum: 96.5217267870903
time_elpased: 1.85
batch start
#iterations: 97
currently lose_sum: 96.75854378938675
time_elpased: 1.906
batch start
#iterations: 98
currently lose_sum: 96.50476104021072
time_elpased: 1.895
batch start
#iterations: 99
currently lose_sum: 96.5676366686821
time_elpased: 1.921
start validation test
0.595206185567
0.617866632924
0.50257307534
0.554287984562
0.595359234902
64.710
batch start
#iterations: 100
currently lose_sum: 96.5630955696106
time_elpased: 1.977
batch start
#iterations: 101
currently lose_sum: 96.64261990785599
time_elpased: 1.917
batch start
#iterations: 102
currently lose_sum: 96.71015775203705
time_elpased: 1.872
batch start
#iterations: 103
currently lose_sum: 96.6218176484108
time_elpased: 1.836
batch start
#iterations: 104
currently lose_sum: 96.44285649061203
time_elpased: 1.966
batch start
#iterations: 105
currently lose_sum: 96.41636776924133
time_elpased: 1.933
batch start
#iterations: 106
currently lose_sum: 96.62383264303207
time_elpased: 2.057
batch start
#iterations: 107
currently lose_sum: 96.38267076015472
time_elpased: 1.793
batch start
#iterations: 108
currently lose_sum: 96.54422581195831
time_elpased: 1.9
batch start
#iterations: 109
currently lose_sum: 96.12293660640717
time_elpased: 1.931
batch start
#iterations: 110
currently lose_sum: 96.48262745141983
time_elpased: 2.048
batch start
#iterations: 111
currently lose_sum: 95.99376809597015
time_elpased: 1.906
batch start
#iterations: 112
currently lose_sum: 96.32215279340744
time_elpased: 2.249
batch start
#iterations: 113
currently lose_sum: 96.20492196083069
time_elpased: 1.891
batch start
#iterations: 114
currently lose_sum: 96.47221875190735
time_elpased: 1.991
batch start
#iterations: 115
currently lose_sum: 95.96138322353363
time_elpased: 1.918
batch start
#iterations: 116
currently lose_sum: 96.71003270149231
time_elpased: 1.928
batch start
#iterations: 117
currently lose_sum: 95.91141390800476
time_elpased: 2.004
batch start
#iterations: 118
currently lose_sum: 96.48292642831802
time_elpased: 1.901
batch start
#iterations: 119
currently lose_sum: 96.41275614500046
time_elpased: 1.86
start validation test
0.587010309278
0.620645709431
0.451111568547
0.522469901061
0.587234842514
65.123
batch start
#iterations: 120
currently lose_sum: 96.04994529485703
time_elpased: 1.872
batch start
#iterations: 121
currently lose_sum: 96.39280444383621
time_elpased: 1.893
batch start
#iterations: 122
currently lose_sum: 96.0897770524025
time_elpased: 1.909
batch start
#iterations: 123
currently lose_sum: 96.30231046676636
time_elpased: 1.87
batch start
#iterations: 124
currently lose_sum: 96.09014767408371
time_elpased: 1.954
batch start
#iterations: 125
currently lose_sum: 95.7475374341011
time_elpased: 2.007
batch start
#iterations: 126
currently lose_sum: 95.82702153921127
time_elpased: 2.058
batch start
#iterations: 127
currently lose_sum: 96.15901654958725
time_elpased: 1.963
batch start
#iterations: 128
currently lose_sum: 95.97602462768555
time_elpased: 1.842
batch start
#iterations: 129
currently lose_sum: 95.95312142372131
time_elpased: 1.8
batch start
#iterations: 130
currently lose_sum: 96.1879089474678
time_elpased: 1.711
batch start
#iterations: 131
currently lose_sum: 95.75285303592682
time_elpased: 1.769
batch start
#iterations: 132
currently lose_sum: 96.09963673353195
time_elpased: 1.774
batch start
#iterations: 133
currently lose_sum: 96.0905711054802
time_elpased: 1.807
batch start
#iterations: 134
currently lose_sum: 95.95226311683655
time_elpased: 1.795
batch start
#iterations: 135
currently lose_sum: 95.94441443681717
time_elpased: 1.712
batch start
#iterations: 136
currently lose_sum: 96.06095153093338
time_elpased: 1.776
batch start
#iterations: 137
currently lose_sum: 96.26150465011597
time_elpased: 1.74
batch start
#iterations: 138
currently lose_sum: 95.86969822645187
time_elpased: 1.676
batch start
#iterations: 139
currently lose_sum: 95.6407241821289
time_elpased: 1.781
start validation test
0.589432989691
0.617848970252
0.472416632359
0.535433070866
0.589626325267
65.147
batch start
#iterations: 140
currently lose_sum: 95.6389878988266
time_elpased: 1.739
batch start
#iterations: 141
currently lose_sum: 95.98210734128952
time_elpased: 1.768
batch start
#iterations: 142
currently lose_sum: 96.0794226527214
time_elpased: 1.802
batch start
#iterations: 143
currently lose_sum: 95.88150417804718
time_elpased: 1.877
batch start
#iterations: 144
currently lose_sum: 95.81922435760498
time_elpased: 1.791
batch start
#iterations: 145
currently lose_sum: 95.69089180231094
time_elpased: 1.829
batch start
#iterations: 146
currently lose_sum: 95.67240941524506
time_elpased: 1.872
batch start
#iterations: 147
currently lose_sum: 95.99343627691269
time_elpased: 1.813
batch start
#iterations: 148
currently lose_sum: 95.7720895409584
time_elpased: 1.777
batch start
#iterations: 149
currently lose_sum: 95.74237561225891
time_elpased: 1.697
batch start
#iterations: 150
currently lose_sum: 95.8055409193039
time_elpased: 1.694
batch start
#iterations: 151
currently lose_sum: 95.94905734062195
time_elpased: 1.796
batch start
#iterations: 152
currently lose_sum: 95.75692236423492
time_elpased: 1.811
batch start
#iterations: 153
currently lose_sum: 95.56648552417755
time_elpased: 1.755
batch start
#iterations: 154
currently lose_sum: 95.6906026005745
time_elpased: 1.718
batch start
#iterations: 155
currently lose_sum: 95.6392593383789
time_elpased: 1.742
batch start
#iterations: 156
currently lose_sum: 95.91129034757614
time_elpased: 1.718
batch start
#iterations: 157
currently lose_sum: 95.54676008224487
time_elpased: 1.783
batch start
#iterations: 158
currently lose_sum: 95.72820365428925
time_elpased: 1.806
batch start
#iterations: 159
currently lose_sum: 95.47311800718307
time_elpased: 1.819
start validation test
0.586907216495
0.605714285714
0.501852614245
0.548913655297
0.587047744545
65.348
batch start
#iterations: 160
currently lose_sum: 95.85545718669891
time_elpased: 1.815
batch start
#iterations: 161
currently lose_sum: 95.37826037406921
time_elpased: 1.843
batch start
#iterations: 162
currently lose_sum: 95.562779545784
time_elpased: 1.673
batch start
#iterations: 163
currently lose_sum: 95.53537756204605
time_elpased: 1.693
batch start
#iterations: 164
currently lose_sum: 95.3359922170639
time_elpased: 1.71
batch start
#iterations: 165
currently lose_sum: 95.16252279281616
time_elpased: 1.703
batch start
#iterations: 166
currently lose_sum: 95.50366079807281
time_elpased: 1.753
batch start
#iterations: 167
currently lose_sum: 95.14389735460281
time_elpased: 1.83
batch start
#iterations: 168
currently lose_sum: 95.51176679134369
time_elpased: 1.842
batch start
#iterations: 169
currently lose_sum: 95.10840356349945
time_elpased: 1.859
batch start
#iterations: 170
currently lose_sum: 95.24776780605316
time_elpased: 1.828
batch start
#iterations: 171
currently lose_sum: 95.50124335289001
time_elpased: 1.852
batch start
#iterations: 172
currently lose_sum: 95.26255637407303
time_elpased: 1.803
batch start
#iterations: 173
currently lose_sum: 95.31584739685059
time_elpased: 1.815
batch start
#iterations: 174
currently lose_sum: 94.99717479944229
time_elpased: 1.846
batch start
#iterations: 175
currently lose_sum: 95.00584280490875
time_elpased: 1.866
batch start
#iterations: 176
currently lose_sum: 95.1859684586525
time_elpased: 1.831
batch start
#iterations: 177
currently lose_sum: 95.56608062982559
time_elpased: 1.855
batch start
#iterations: 178
currently lose_sum: 95.31438344717026
time_elpased: 1.818
batch start
#iterations: 179
currently lose_sum: 95.16289693117142
time_elpased: 1.726
start validation test
0.589329896907
0.615262949783
0.480444627419
0.53955961394
0.58950979822
65.281
batch start
#iterations: 180
currently lose_sum: 95.02973395586014
time_elpased: 1.808
batch start
#iterations: 181
currently lose_sum: 95.52823734283447
time_elpased: 1.879
batch start
#iterations: 182
currently lose_sum: 95.34339529275894
time_elpased: 1.872
batch start
#iterations: 183
currently lose_sum: 95.19665217399597
time_elpased: 1.92
batch start
#iterations: 184
currently lose_sum: 94.95495820045471
time_elpased: 1.858
batch start
#iterations: 185
currently lose_sum: 95.0823170542717
time_elpased: 1.899
batch start
#iterations: 186
currently lose_sum: 95.19711142778397
time_elpased: 1.799
batch start
#iterations: 187
currently lose_sum: 95.05196315050125
time_elpased: 1.874
batch start
#iterations: 188
currently lose_sum: 94.75733971595764
time_elpased: 1.896
batch start
#iterations: 189
currently lose_sum: 95.24241632223129
time_elpased: 1.919
batch start
#iterations: 190
currently lose_sum: 95.17855924367905
time_elpased: 1.89
batch start
#iterations: 191
currently lose_sum: 95.02832001447678
time_elpased: 1.905
batch start
#iterations: 192
currently lose_sum: 95.15302896499634
time_elpased: 1.877
batch start
#iterations: 193
currently lose_sum: 94.60200732946396
time_elpased: 1.87
batch start
#iterations: 194
currently lose_sum: 95.3864152431488
time_elpased: 1.781
batch start
#iterations: 195
currently lose_sum: 95.17814433574677
time_elpased: 1.794
batch start
#iterations: 196
currently lose_sum: 94.58956342935562
time_elpased: 1.784
batch start
#iterations: 197
currently lose_sum: 94.64317184686661
time_elpased: 1.798
batch start
#iterations: 198
currently lose_sum: 94.74439823627472
time_elpased: 1.78
batch start
#iterations: 199
currently lose_sum: 94.95390713214874
time_elpased: 1.841
start validation test
0.582577319588
0.622464426279
0.423219431865
0.5038598211
0.582840612256
65.899
batch start
#iterations: 200
currently lose_sum: 95.22815442085266
time_elpased: 1.812
batch start
#iterations: 201
currently lose_sum: 94.48450344800949
time_elpased: 1.863
batch start
#iterations: 202
currently lose_sum: 94.90357649326324
time_elpased: 1.836
batch start
#iterations: 203
currently lose_sum: 94.70355701446533
time_elpased: 1.876
batch start
#iterations: 204
currently lose_sum: 95.15270161628723
time_elpased: 1.841
batch start
#iterations: 205
currently lose_sum: 94.93156033754349
time_elpased: 1.867
batch start
#iterations: 206
currently lose_sum: 95.50442481040955
time_elpased: 1.807
batch start
#iterations: 207
currently lose_sum: 94.98782193660736
time_elpased: 2.001
batch start
#iterations: 208
currently lose_sum: 94.84835141897202
time_elpased: 1.811
batch start
#iterations: 209
currently lose_sum: 94.8816624879837
time_elpased: 1.783
batch start
#iterations: 210
currently lose_sum: 94.9747703075409
time_elpased: 1.754
batch start
#iterations: 211
currently lose_sum: 94.53173285722733
time_elpased: 1.781
batch start
#iterations: 212
currently lose_sum: 95.01791244745255
time_elpased: 1.776
batch start
#iterations: 213
currently lose_sum: 94.98687541484833
time_elpased: 1.793
batch start
#iterations: 214
currently lose_sum: 95.07490235567093
time_elpased: 1.729
batch start
#iterations: 215
currently lose_sum: 94.73131883144379
time_elpased: 1.844
batch start
#iterations: 216
currently lose_sum: 94.3399640917778
time_elpased: 1.814
batch start
#iterations: 217
currently lose_sum: 95.04330378770828
time_elpased: 1.806
batch start
#iterations: 218
currently lose_sum: 94.73617124557495
time_elpased: 1.821
batch start
#iterations: 219
currently lose_sum: 94.46423006057739
time_elpased: 1.818
start validation test
0.574896907216
0.625705972959
0.37628653767
0.469953081818
0.575225053222
66.211
batch start
#iterations: 220
currently lose_sum: 94.59715896844864
time_elpased: 1.828
batch start
#iterations: 221
currently lose_sum: 94.66253310441971
time_elpased: 1.789
batch start
#iterations: 222
currently lose_sum: 94.78843885660172
time_elpased: 1.981
batch start
#iterations: 223
currently lose_sum: 94.15723741054535
time_elpased: 1.856
batch start
#iterations: 224
currently lose_sum: 94.46468275785446
time_elpased: 1.797
batch start
#iterations: 225
currently lose_sum: 94.66411644220352
time_elpased: 1.894
batch start
#iterations: 226
currently lose_sum: 94.86307263374329
time_elpased: 1.831
batch start
#iterations: 227
currently lose_sum: 94.38264393806458
time_elpased: 1.809
batch start
#iterations: 228
currently lose_sum: 94.46778333187103
time_elpased: 1.893
batch start
#iterations: 229
currently lose_sum: 94.74364119768143
time_elpased: 1.829
batch start
#iterations: 230
currently lose_sum: 94.45282083749771
time_elpased: 1.807
batch start
#iterations: 231
currently lose_sum: 94.75847899913788
time_elpased: 1.779
batch start
#iterations: 232
currently lose_sum: 94.3827612400055
time_elpased: 1.777
batch start
#iterations: 233
currently lose_sum: 94.52151733636856
time_elpased: 1.876
batch start
#iterations: 234
currently lose_sum: 94.42667818069458
time_elpased: 1.797
batch start
#iterations: 235
currently lose_sum: 94.77033323049545
time_elpased: 1.875
batch start
#iterations: 236
currently lose_sum: 94.65613448619843
time_elpased: 1.796
batch start
#iterations: 237
currently lose_sum: 94.1497693657875
time_elpased: 1.704
batch start
#iterations: 238
currently lose_sum: 94.55602717399597
time_elpased: 1.72
batch start
#iterations: 239
currently lose_sum: 94.66466504335403
time_elpased: 1.702
start validation test
0.570773195876
0.613684727451
0.385858377933
0.473807266983
0.571078713956
66.472
batch start
#iterations: 240
currently lose_sum: 94.0022714138031
time_elpased: 1.824
batch start
#iterations: 241
currently lose_sum: 94.27267134189606
time_elpased: 1.849
batch start
#iterations: 242
currently lose_sum: 94.09239202737808
time_elpased: 1.811
batch start
#iterations: 243
currently lose_sum: 94.59003949165344
time_elpased: 1.85
batch start
#iterations: 244
currently lose_sum: 94.35708504915237
time_elpased: 1.874
batch start
#iterations: 245
currently lose_sum: 94.60213136672974
time_elpased: 1.869
batch start
#iterations: 246
currently lose_sum: 94.44236260652542
time_elpased: 1.872
batch start
#iterations: 247
currently lose_sum: 94.29779124259949
time_elpased: 1.905
batch start
#iterations: 248
currently lose_sum: 94.18272590637207
time_elpased: 1.834
batch start
#iterations: 249
currently lose_sum: 94.302669942379
time_elpased: 1.806
batch start
#iterations: 250
currently lose_sum: 94.61070245504379
time_elpased: 1.735
batch start
#iterations: 251
currently lose_sum: 94.46297734975815
time_elpased: 1.784
batch start
#iterations: 252
currently lose_sum: 94.67014646530151
time_elpased: 1.754
batch start
#iterations: 253
currently lose_sum: 94.41918671131134
time_elpased: 1.895
batch start
#iterations: 254
currently lose_sum: 94.48341757059097
time_elpased: 1.885
batch start
#iterations: 255
currently lose_sum: 94.1039999127388
time_elpased: 1.937
batch start
#iterations: 256
currently lose_sum: 94.13692939281464
time_elpased: 1.827
batch start
#iterations: 257
currently lose_sum: 94.38515120744705
time_elpased: 1.861
batch start
#iterations: 258
currently lose_sum: 94.20297306776047
time_elpased: 1.795
batch start
#iterations: 259
currently lose_sum: 94.25158625841141
time_elpased: 1.815
start validation test
0.572113402062
0.616922822674
0.384211609716
0.473520644384
0.57242385525
66.735
batch start
#iterations: 260
currently lose_sum: 94.25586134195328
time_elpased: 1.816
batch start
#iterations: 261
currently lose_sum: 94.2005432844162
time_elpased: 1.801
batch start
#iterations: 262
currently lose_sum: 93.871029317379
time_elpased: 1.777
batch start
#iterations: 263
currently lose_sum: 94.29943335056305
time_elpased: 1.827
batch start
#iterations: 264
currently lose_sum: 94.3556500673294
time_elpased: 1.839
batch start
#iterations: 265
currently lose_sum: 93.85712724924088
time_elpased: 1.856
batch start
#iterations: 266
currently lose_sum: 94.0434313416481
time_elpased: 1.897
batch start
#iterations: 267
currently lose_sum: 94.27373343706131
time_elpased: 1.895
batch start
#iterations: 268
currently lose_sum: 94.29013293981552
time_elpased: 1.857
batch start
#iterations: 269
currently lose_sum: 93.87045162916183
time_elpased: 1.867
batch start
#iterations: 270
currently lose_sum: 93.91795867681503
time_elpased: 1.822
batch start
#iterations: 271
currently lose_sum: 94.02053254842758
time_elpased: 1.839
batch start
#iterations: 272
currently lose_sum: 94.1247610449791
time_elpased: 1.851
batch start
#iterations: 273
currently lose_sum: 94.05581122636795
time_elpased: 1.855
batch start
#iterations: 274
currently lose_sum: 93.91515338420868
time_elpased: 1.877
batch start
#iterations: 275
currently lose_sum: 94.15605574846268
time_elpased: 1.906
batch start
#iterations: 276
currently lose_sum: 94.03702986240387
time_elpased: 1.828
batch start
#iterations: 277
currently lose_sum: 93.41837203502655
time_elpased: 1.875
batch start
#iterations: 278
currently lose_sum: 93.98643964529037
time_elpased: 1.819
batch start
#iterations: 279
currently lose_sum: 93.8501501083374
time_elpased: 1.839
start validation test
0.582474226804
0.601075806855
0.49454508028
0.542631281762
0.582619504204
65.909
batch start
#iterations: 280
currently lose_sum: 94.2963752746582
time_elpased: 1.758
batch start
#iterations: 281
currently lose_sum: 93.8208492398262
time_elpased: 1.795
batch start
#iterations: 282
currently lose_sum: 93.84027582406998
time_elpased: 1.774
batch start
#iterations: 283
currently lose_sum: 94.3934211730957
time_elpased: 1.796
batch start
#iterations: 284
currently lose_sum: 93.67088878154755
time_elpased: 1.813
batch start
#iterations: 285
currently lose_sum: 93.75533229112625
time_elpased: 1.85
batch start
#iterations: 286
currently lose_sum: 93.91994631290436
time_elpased: 1.78
batch start
#iterations: 287
currently lose_sum: 93.96364963054657
time_elpased: 1.882
batch start
#iterations: 288
currently lose_sum: 93.83374094963074
time_elpased: 1.846
batch start
#iterations: 289
currently lose_sum: 93.70739197731018
time_elpased: 1.854
batch start
#iterations: 290
currently lose_sum: 93.8959533572197
time_elpased: 1.852
batch start
#iterations: 291
currently lose_sum: 93.89745289087296
time_elpased: 1.877
batch start
#iterations: 292
currently lose_sum: 93.9051005244255
time_elpased: 1.82
batch start
#iterations: 293
currently lose_sum: 94.0115595459938
time_elpased: 1.908
batch start
#iterations: 294
currently lose_sum: 94.10279554128647
time_elpased: 1.858
batch start
#iterations: 295
currently lose_sum: 93.53477770090103
time_elpased: 1.837
batch start
#iterations: 296
currently lose_sum: 93.74519073963165
time_elpased: 1.787
batch start
#iterations: 297
currently lose_sum: 93.93746757507324
time_elpased: 1.775
batch start
#iterations: 298
currently lose_sum: 93.42363804578781
time_elpased: 1.778
batch start
#iterations: 299
currently lose_sum: 93.97072631120682
time_elpased: 1.735
start validation test
0.580567010309
0.60416941549
0.47128447921
0.529517201503
0.580747567981
66.109
batch start
#iterations: 300
currently lose_sum: 94.25688427686691
time_elpased: 1.755
batch start
#iterations: 301
currently lose_sum: 93.97445011138916
time_elpased: 1.778
batch start
#iterations: 302
currently lose_sum: 93.55938798189163
time_elpased: 1.815
batch start
#iterations: 303
currently lose_sum: 93.94549310207367
time_elpased: 1.829
batch start
#iterations: 304
currently lose_sum: 93.58199864625931
time_elpased: 1.795
batch start
#iterations: 305
currently lose_sum: 93.52005636692047
time_elpased: 1.817
batch start
#iterations: 306
currently lose_sum: 93.88089197874069
time_elpased: 1.735
batch start
#iterations: 307
currently lose_sum: 93.96487736701965
time_elpased: 1.68
batch start
#iterations: 308
currently lose_sum: 93.5843705534935
time_elpased: 1.705
batch start
#iterations: 309
currently lose_sum: 93.28761196136475
time_elpased: 1.706
batch start
#iterations: 310
currently lose_sum: 93.8001561164856
time_elpased: 1.699
batch start
#iterations: 311
currently lose_sum: 93.307188808918
time_elpased: 1.723
batch start
#iterations: 312
currently lose_sum: 93.98469060659409
time_elpased: 1.761
batch start
#iterations: 313
currently lose_sum: 93.60213053226471
time_elpased: 1.766
batch start
#iterations: 314
currently lose_sum: 93.95934188365936
time_elpased: 1.74
batch start
#iterations: 315
currently lose_sum: 93.66738730669022
time_elpased: 1.969
batch start
#iterations: 316
currently lose_sum: 94.08916467428207
time_elpased: 1.823
batch start
#iterations: 317
currently lose_sum: 93.57753872871399
time_elpased: 1.846
batch start
#iterations: 318
currently lose_sum: 93.06863820552826
time_elpased: 1.765
batch start
#iterations: 319
currently lose_sum: 93.65840780735016
time_elpased: 1.795
start validation test
0.581649484536
0.603332472229
0.480753396459
0.53511284225
0.581816186045
66.288
batch start
#iterations: 320
currently lose_sum: 93.7061128616333
time_elpased: 1.814
batch start
#iterations: 321
currently lose_sum: 93.51746970415115
time_elpased: 1.827
batch start
#iterations: 322
currently lose_sum: 93.79570960998535
time_elpased: 1.779
batch start
#iterations: 323
currently lose_sum: 93.27018111944199
time_elpased: 1.808
batch start
#iterations: 324
currently lose_sum: 93.85968232154846
time_elpased: 1.722
batch start
#iterations: 325
currently lose_sum: 93.75385576486588
time_elpased: 1.693
batch start
#iterations: 326
currently lose_sum: 93.32001602649689
time_elpased: 1.68
batch start
#iterations: 327
currently lose_sum: 93.33913522958755
time_elpased: 1.687
batch start
#iterations: 328
currently lose_sum: 93.36889004707336
time_elpased: 1.704
batch start
#iterations: 329
currently lose_sum: 93.66083979606628
time_elpased: 1.832
batch start
#iterations: 330
currently lose_sum: 93.38838917016983
time_elpased: 1.832
batch start
#iterations: 331
currently lose_sum: 93.18000984191895
time_elpased: 1.866
batch start
#iterations: 332
currently lose_sum: 93.15274572372437
time_elpased: 1.841
batch start
#iterations: 333
currently lose_sum: 93.85994231700897
time_elpased: 1.822
batch start
#iterations: 334
currently lose_sum: 93.4498153924942
time_elpased: 1.893
batch start
#iterations: 335
currently lose_sum: 93.3059173822403
time_elpased: 1.772
batch start
#iterations: 336
currently lose_sum: 93.44163513183594
time_elpased: 1.791
batch start
#iterations: 337
currently lose_sum: 93.57164913415909
time_elpased: 1.802
batch start
#iterations: 338
currently lose_sum: 93.2318816781044
time_elpased: 1.836
batch start
#iterations: 339
currently lose_sum: 93.3832700252533
time_elpased: 1.849
start validation test
0.582216494845
0.598701139566
0.50288184438
0.546624153941
0.582347572335
66.308
batch start
#iterations: 340
currently lose_sum: 93.6793841123581
time_elpased: 1.867
batch start
#iterations: 341
currently lose_sum: 93.1490051150322
time_elpased: 1.893
batch start
#iterations: 342
currently lose_sum: 93.61378127336502
time_elpased: 1.864
batch start
#iterations: 343
currently lose_sum: 93.49765944480896
time_elpased: 1.832
batch start
#iterations: 344
currently lose_sum: 93.52149105072021
time_elpased: 1.802
batch start
#iterations: 345
currently lose_sum: 93.30275362730026
time_elpased: 1.809
batch start
#iterations: 346
currently lose_sum: 93.48112827539444
time_elpased: 1.841
batch start
#iterations: 347
currently lose_sum: 93.51949548721313
time_elpased: 1.824
batch start
#iterations: 348
currently lose_sum: 93.09963113069534
time_elpased: 1.842
batch start
#iterations: 349
currently lose_sum: 93.33558803796768
time_elpased: 1.857
batch start
#iterations: 350
currently lose_sum: 93.22969502210617
time_elpased: 1.804
batch start
#iterations: 351
currently lose_sum: 93.54765611886978
time_elpased: 1.877
batch start
#iterations: 352
currently lose_sum: 93.46001446247101
time_elpased: 1.739
batch start
#iterations: 353
currently lose_sum: 93.7379219532013
time_elpased: 1.889
batch start
#iterations: 354
currently lose_sum: 93.12183392047882
time_elpased: 1.939
batch start
#iterations: 355
currently lose_sum: 93.27036261558533
time_elpased: 1.934
batch start
#iterations: 356
currently lose_sum: 92.80267065763474
time_elpased: 1.907
batch start
#iterations: 357
currently lose_sum: 93.52201849222183
time_elpased: 1.845
batch start
#iterations: 358
currently lose_sum: 93.45916849374771
time_elpased: 1.711
batch start
#iterations: 359
currently lose_sum: 92.85824966430664
time_elpased: 1.761
start validation test
0.587216494845
0.607259482542
0.497632770688
0.547007580043
0.587364505955
66.087
batch start
#iterations: 360
currently lose_sum: 93.18887186050415
time_elpased: 1.865
batch start
#iterations: 361
currently lose_sum: 93.26798444986343
time_elpased: 1.843
batch start
#iterations: 362
currently lose_sum: 92.98485457897186
time_elpased: 1.923
batch start
#iterations: 363
currently lose_sum: 93.30254071950912
time_elpased: 1.868
batch start
#iterations: 364
currently lose_sum: 93.11636847257614
time_elpased: 1.892
batch start
#iterations: 365
currently lose_sum: 93.48423337936401
time_elpased: 1.816
batch start
#iterations: 366
currently lose_sum: 93.35397619009018
time_elpased: 1.694
batch start
#iterations: 367
currently lose_sum: 93.43681567907333
time_elpased: 1.717
batch start
#iterations: 368
currently lose_sum: 93.3036059141159
time_elpased: 1.817
batch start
#iterations: 369
currently lose_sum: 93.02508264780045
time_elpased: 1.776
batch start
#iterations: 370
currently lose_sum: 93.20346641540527
time_elpased: 1.771
batch start
#iterations: 371
currently lose_sum: 93.11882483959198
time_elpased: 1.777
batch start
#iterations: 372
currently lose_sum: 92.6824539899826
time_elpased: 1.846
batch start
#iterations: 373
currently lose_sum: 93.09881579875946
time_elpased: 1.885
batch start
#iterations: 374
currently lose_sum: 93.34758013486862
time_elpased: 1.783
batch start
#iterations: 375
currently lose_sum: 92.53870630264282
time_elpased: 1.77
batch start
#iterations: 376
currently lose_sum: 92.84824699163437
time_elpased: 1.753
batch start
#iterations: 377
currently lose_sum: 93.07838875055313
time_elpased: 1.739
batch start
#iterations: 378
currently lose_sum: 93.21864140033722
time_elpased: 1.728
batch start
#iterations: 379
currently lose_sum: 93.02349758148193
time_elpased: 1.854
start validation test
0.58206185567
0.609896118097
0.45924248662
0.523954908408
0.582264779039
66.681
batch start
#iterations: 380
currently lose_sum: 93.16121679544449
time_elpased: 1.744
batch start
#iterations: 381
currently lose_sum: 92.94062465429306
time_elpased: 1.757
batch start
#iterations: 382
currently lose_sum: 93.16688376665115
time_elpased: 1.852
batch start
#iterations: 383
currently lose_sum: 93.30049955844879
time_elpased: 1.841
batch start
#iterations: 384
currently lose_sum: 92.92522716522217
time_elpased: 1.779
batch start
#iterations: 385
currently lose_sum: 93.36307764053345
time_elpased: 1.885
batch start
#iterations: 386
currently lose_sum: 92.72286576032639
time_elpased: 1.816
batch start
#iterations: 387
currently lose_sum: 92.76366382837296
time_elpased: 1.814
batch start
#iterations: 388
currently lose_sum: 92.91997909545898
time_elpased: 1.809
batch start
#iterations: 389
currently lose_sum: 92.68018889427185
time_elpased: 1.725
batch start
#iterations: 390
currently lose_sum: 93.27707320451736
time_elpased: 1.694
batch start
#iterations: 391
currently lose_sum: 92.92764270305634
time_elpased: 1.735
batch start
#iterations: 392
currently lose_sum: 93.05917870998383
time_elpased: 1.813
batch start
#iterations: 393
currently lose_sum: 92.91788709163666
time_elpased: 1.797
batch start
#iterations: 394
currently lose_sum: 92.70297831296921
time_elpased: 1.828
batch start
#iterations: 395
currently lose_sum: 92.89290457963943
time_elpased: 1.807
batch start
#iterations: 396
currently lose_sum: 92.69663494825363
time_elpased: 1.797
batch start
#iterations: 397
currently lose_sum: 92.60659593343735
time_elpased: 1.75
batch start
#iterations: 398
currently lose_sum: 92.82833814620972
time_elpased: 1.797
batch start
#iterations: 399
currently lose_sum: 93.11812943220139
time_elpased: 1.757
start validation test
0.575103092784
0.605019249964
0.436702346645
0.50726283699
0.575331759857
66.848
acc: 0.603
pre: 0.616
rec: 0.547
F1: 0.580
auc: 0.603
