start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.60358613729477
time_elpased: 2.093
batch start
#iterations: 1
currently lose_sum: 100.13081777095795
time_elpased: 2.06
batch start
#iterations: 2
currently lose_sum: 99.84555447101593
time_elpased: 2.002
batch start
#iterations: 3
currently lose_sum: 99.80189651250839
time_elpased: 2.234
batch start
#iterations: 4
currently lose_sum: 99.68477672338486
time_elpased: 2.031
batch start
#iterations: 5
currently lose_sum: 99.5521389245987
time_elpased: 2.201
batch start
#iterations: 6
currently lose_sum: 99.58443015813828
time_elpased: 2.024
batch start
#iterations: 7
currently lose_sum: 99.16545259952545
time_elpased: 2.037
batch start
#iterations: 8
currently lose_sum: 99.30284875631332
time_elpased: 2.057
batch start
#iterations: 9
currently lose_sum: 99.2131889462471
time_elpased: 2.143
batch start
#iterations: 10
currently lose_sum: 99.1495133638382
time_elpased: 2.02
batch start
#iterations: 11
currently lose_sum: 99.02626484632492
time_elpased: 2.047
batch start
#iterations: 12
currently lose_sum: 99.17069268226624
time_elpased: 2.055
batch start
#iterations: 13
currently lose_sum: 98.87731301784515
time_elpased: 2.088
batch start
#iterations: 14
currently lose_sum: 98.89539813995361
time_elpased: 2.043
batch start
#iterations: 15
currently lose_sum: 98.48874878883362
time_elpased: 2.024
batch start
#iterations: 16
currently lose_sum: 98.53278398513794
time_elpased: 2.101
batch start
#iterations: 17
currently lose_sum: 98.61069566011429
time_elpased: 2.044
batch start
#iterations: 18
currently lose_sum: 98.40358990430832
time_elpased: 2.03
batch start
#iterations: 19
currently lose_sum: 98.52950048446655
time_elpased: 1.97
start validation test
0.596082474227
0.609296920395
0.539569826078
0.572317432595
0.596181690897
64.937
batch start
#iterations: 20
currently lose_sum: 98.50992792844772
time_elpased: 2.049
batch start
#iterations: 21
currently lose_sum: 98.0671718120575
time_elpased: 2.01
batch start
#iterations: 22
currently lose_sum: 98.63087171316147
time_elpased: 2.031
batch start
#iterations: 23
currently lose_sum: 98.40706896781921
time_elpased: 2.051
batch start
#iterations: 24
currently lose_sum: 98.45480334758759
time_elpased: 1.989
batch start
#iterations: 25
currently lose_sum: 98.41877979040146
time_elpased: 1.997
batch start
#iterations: 26
currently lose_sum: 98.23567134141922
time_elpased: 1.993
batch start
#iterations: 27
currently lose_sum: 98.30044567584991
time_elpased: 2.04
batch start
#iterations: 28
currently lose_sum: 98.13281524181366
time_elpased: 2.088
batch start
#iterations: 29
currently lose_sum: 98.22183555364609
time_elpased: 2.06
batch start
#iterations: 30
currently lose_sum: 98.26215672492981
time_elpased: 2.053
batch start
#iterations: 31
currently lose_sum: 97.86280834674835
time_elpased: 2.037
batch start
#iterations: 32
currently lose_sum: 97.77459132671356
time_elpased: 2.031
batch start
#iterations: 33
currently lose_sum: 98.06988883018494
time_elpased: 2.059
batch start
#iterations: 34
currently lose_sum: 97.75541943311691
time_elpased: 2.06
batch start
#iterations: 35
currently lose_sum: 97.86252152919769
time_elpased: 2.176
batch start
#iterations: 36
currently lose_sum: 97.79382592439651
time_elpased: 2.123
batch start
#iterations: 37
currently lose_sum: 97.7538965344429
time_elpased: 2.06
batch start
#iterations: 38
currently lose_sum: 97.75932389497757
time_elpased: 2.046
batch start
#iterations: 39
currently lose_sum: 97.46921563148499
time_elpased: 2.048
start validation test
0.592474226804
0.621137123746
0.47782237316
0.540134946487
0.592675515817
64.820
batch start
#iterations: 40
currently lose_sum: 97.56387096643448
time_elpased: 2.032
batch start
#iterations: 41
currently lose_sum: 97.66898238658905
time_elpased: 2.039
batch start
#iterations: 42
currently lose_sum: 97.44331568479538
time_elpased: 2.028
batch start
#iterations: 43
currently lose_sum: 97.60732567310333
time_elpased: 2.052
batch start
#iterations: 44
currently lose_sum: 97.34436452388763
time_elpased: 2.226
batch start
#iterations: 45
currently lose_sum: 97.3433832526207
time_elpased: 2.047
batch start
#iterations: 46
currently lose_sum: 97.09220290184021
time_elpased: 2.042
batch start
#iterations: 47
currently lose_sum: 97.38390773534775
time_elpased: 2.016
batch start
#iterations: 48
currently lose_sum: 97.30306380987167
time_elpased: 2.107
batch start
#iterations: 49
currently lose_sum: 97.18831431865692
time_elpased: 2.034
batch start
#iterations: 50
currently lose_sum: 97.27359294891357
time_elpased: 2.033
batch start
#iterations: 51
currently lose_sum: 97.09033542871475
time_elpased: 2.037
batch start
#iterations: 52
currently lose_sum: 97.22860670089722
time_elpased: 2.03
batch start
#iterations: 53
currently lose_sum: 97.15414732694626
time_elpased: 2.039
batch start
#iterations: 54
currently lose_sum: 97.22784984111786
time_elpased: 1.997
batch start
#iterations: 55
currently lose_sum: 97.31915253400803
time_elpased: 2.005
batch start
#iterations: 56
currently lose_sum: 97.07374316453934
time_elpased: 1.999
batch start
#iterations: 57
currently lose_sum: 96.98392850160599
time_elpased: 2.113
batch start
#iterations: 58
currently lose_sum: 97.02012890577316
time_elpased: 2.047
batch start
#iterations: 59
currently lose_sum: 97.1846097111702
time_elpased: 2.051
start validation test
0.593505154639
0.621274340972
0.482659256972
0.543264218696
0.593699761709
64.757
batch start
#iterations: 60
currently lose_sum: 97.04210412502289
time_elpased: 2.048
batch start
#iterations: 61
currently lose_sum: 96.8686625957489
time_elpased: 2.062
batch start
#iterations: 62
currently lose_sum: 96.94798493385315
time_elpased: 2.026
batch start
#iterations: 63
currently lose_sum: 96.93310421705246
time_elpased: 2.069
batch start
#iterations: 64
currently lose_sum: 96.69423806667328
time_elpased: 1.933
batch start
#iterations: 65
currently lose_sum: 96.4562349319458
time_elpased: 1.933
batch start
#iterations: 66
currently lose_sum: 96.44055646657944
time_elpased: 1.922
batch start
#iterations: 67
currently lose_sum: 96.78918313980103
time_elpased: 2.014
batch start
#iterations: 68
currently lose_sum: 96.58160549402237
time_elpased: 2.076
batch start
#iterations: 69
currently lose_sum: 96.82878935337067
time_elpased: 2.1
batch start
#iterations: 70
currently lose_sum: 96.66314744949341
time_elpased: 2.061
batch start
#iterations: 71
currently lose_sum: 96.4971244931221
time_elpased: 2.043
batch start
#iterations: 72
currently lose_sum: 96.52846837043762
time_elpased: 2.045
batch start
#iterations: 73
currently lose_sum: 96.30546176433563
time_elpased: 2.076
batch start
#iterations: 74
currently lose_sum: 96.52038663625717
time_elpased: 2.023
batch start
#iterations: 75
currently lose_sum: 96.52733987569809
time_elpased: 2.044
batch start
#iterations: 76
currently lose_sum: 96.03776335716248
time_elpased: 2.049
batch start
#iterations: 77
currently lose_sum: 96.63188815116882
time_elpased: 2.032
batch start
#iterations: 78
currently lose_sum: 96.47297376394272
time_elpased: 2.075
batch start
#iterations: 79
currently lose_sum: 96.48632907867432
time_elpased: 1.981
start validation test
0.592577319588
0.623788065001
0.470103941546
0.536150234742
0.592792340493
64.825
batch start
#iterations: 80
currently lose_sum: 96.52171182632446
time_elpased: 2.0
batch start
#iterations: 81
currently lose_sum: 96.19837510585785
time_elpased: 2.039
batch start
#iterations: 82
currently lose_sum: 95.97481858730316
time_elpased: 2.013
batch start
#iterations: 83
currently lose_sum: 95.80575180053711
time_elpased: 1.937
batch start
#iterations: 84
currently lose_sum: 96.3567715883255
time_elpased: 1.983
batch start
#iterations: 85
currently lose_sum: 96.24170458316803
time_elpased: 2.076
batch start
#iterations: 86
currently lose_sum: 96.01882284879684
time_elpased: 2.024
batch start
#iterations: 87
currently lose_sum: 95.87590450048447
time_elpased: 2.103
batch start
#iterations: 88
currently lose_sum: 95.86526793241501
time_elpased: 2.015
batch start
#iterations: 89
currently lose_sum: 95.89637333154678
time_elpased: 2.055
batch start
#iterations: 90
currently lose_sum: 95.99656081199646
time_elpased: 2.026
batch start
#iterations: 91
currently lose_sum: 96.0163579583168
time_elpased: 2.021
batch start
#iterations: 92
currently lose_sum: 95.81449300050735
time_elpased: 2.058
batch start
#iterations: 93
currently lose_sum: 95.9239205121994
time_elpased: 1.983
batch start
#iterations: 94
currently lose_sum: 95.68533086776733
time_elpased: 1.94
batch start
#iterations: 95
currently lose_sum: 95.60463052988052
time_elpased: 2.008
batch start
#iterations: 96
currently lose_sum: 95.57579523324966
time_elpased: 2.214
batch start
#iterations: 97
currently lose_sum: 95.43644106388092
time_elpased: 2.062
batch start
#iterations: 98
currently lose_sum: 95.64485102891922
time_elpased: 2.088
batch start
#iterations: 99
currently lose_sum: 95.47591185569763
time_elpased: 2.057
start validation test
0.592216494845
0.625173274189
0.464135021097
0.532750575867
0.592441361628
64.815
batch start
#iterations: 100
currently lose_sum: 95.88036227226257
time_elpased: 2.179
batch start
#iterations: 101
currently lose_sum: 95.62466669082642
time_elpased: 1.969
batch start
#iterations: 102
currently lose_sum: 95.36712908744812
time_elpased: 2.026
batch start
#iterations: 103
currently lose_sum: 95.43982934951782
time_elpased: 2.005
batch start
#iterations: 104
currently lose_sum: 95.77173966169357
time_elpased: 2.021
batch start
#iterations: 105
currently lose_sum: 95.54564595222473
time_elpased: 2.039
batch start
#iterations: 106
currently lose_sum: 95.408720433712
time_elpased: 2.026
batch start
#iterations: 107
currently lose_sum: 95.27717524766922
time_elpased: 2.045
batch start
#iterations: 108
currently lose_sum: 95.33058321475983
time_elpased: 2.07
batch start
#iterations: 109
currently lose_sum: 95.37841582298279
time_elpased: 2.08
batch start
#iterations: 110
currently lose_sum: 95.20426023006439
time_elpased: 2.088
batch start
#iterations: 111
currently lose_sum: 95.25220555067062
time_elpased: 2.016
batch start
#iterations: 112
currently lose_sum: 95.09117537736893
time_elpased: 1.928
batch start
#iterations: 113
currently lose_sum: 94.83938753604889
time_elpased: 1.963
batch start
#iterations: 114
currently lose_sum: 94.89211809635162
time_elpased: 1.935
batch start
#iterations: 115
currently lose_sum: 95.35630029439926
time_elpased: 1.913
batch start
#iterations: 116
currently lose_sum: 95.27316397428513
time_elpased: 2.12
batch start
#iterations: 117
currently lose_sum: 95.16892218589783
time_elpased: 2.109
batch start
#iterations: 118
currently lose_sum: 95.10157322883606
time_elpased: 2.051
batch start
#iterations: 119
currently lose_sum: 95.40425592660904
time_elpased: 2.239
start validation test
0.594896907216
0.605520218083
0.548626119173
0.575670860105
0.594978142722
64.811
batch start
#iterations: 120
currently lose_sum: 95.3844593167305
time_elpased: 2.139
batch start
#iterations: 121
currently lose_sum: 95.18102180957794
time_elpased: 2.043
batch start
#iterations: 122
currently lose_sum: 95.43147802352905
time_elpased: 1.981
batch start
#iterations: 123
currently lose_sum: 94.64196121692657
time_elpased: 1.931
batch start
#iterations: 124
currently lose_sum: 94.3785589337349
time_elpased: 1.957
batch start
#iterations: 125
currently lose_sum: 94.96329748630524
time_elpased: 1.939
batch start
#iterations: 126
currently lose_sum: 95.10817390680313
time_elpased: 1.942
batch start
#iterations: 127
currently lose_sum: 95.28782922029495
time_elpased: 1.978
batch start
#iterations: 128
currently lose_sum: 94.94051533937454
time_elpased: 2.06
batch start
#iterations: 129
currently lose_sum: 94.99835407733917
time_elpased: 2.119
batch start
#iterations: 130
currently lose_sum: 95.1891359090805
time_elpased: 2.089
batch start
#iterations: 131
currently lose_sum: 94.512948513031
time_elpased: 2.071
batch start
#iterations: 132
currently lose_sum: 94.53140538930893
time_elpased: 2.101
batch start
#iterations: 133
currently lose_sum: 94.64187890291214
time_elpased: 2.022
batch start
#iterations: 134
currently lose_sum: 94.63427257537842
time_elpased: 2.034
batch start
#iterations: 135
currently lose_sum: 94.77726405858994
time_elpased: 2.023
batch start
#iterations: 136
currently lose_sum: 94.35683900117874
time_elpased: 2.091
batch start
#iterations: 137
currently lose_sum: 94.98999059200287
time_elpased: 2.206
batch start
#iterations: 138
currently lose_sum: 94.598792552948
time_elpased: 2.115
batch start
#iterations: 139
currently lose_sum: 94.50629311800003
time_elpased: 2.037
start validation test
0.593350515464
0.619445896498
0.487804878049
0.545799988485
0.59353581711
65.123
batch start
#iterations: 140
currently lose_sum: 94.88400053977966
time_elpased: 2.038
batch start
#iterations: 141
currently lose_sum: 94.93863582611084
time_elpased: 1.956
batch start
#iterations: 142
currently lose_sum: 94.54836976528168
time_elpased: 1.944
batch start
#iterations: 143
currently lose_sum: 94.29617351293564
time_elpased: 1.922
batch start
#iterations: 144
currently lose_sum: 94.54561734199524
time_elpased: 1.949
batch start
#iterations: 145
currently lose_sum: 95.09698933362961
time_elpased: 1.98
batch start
#iterations: 146
currently lose_sum: 94.05309826135635
time_elpased: 2.093
batch start
#iterations: 147
currently lose_sum: 94.28464615345001
time_elpased: 2.048
batch start
#iterations: 148
currently lose_sum: 94.93139010667801
time_elpased: 2.118
batch start
#iterations: 149
currently lose_sum: 94.44299560785294
time_elpased: 2.116
batch start
#iterations: 150
currently lose_sum: 94.30553442239761
time_elpased: 2.117
batch start
#iterations: 151
currently lose_sum: 94.41413724422455
time_elpased: 2.138
batch start
#iterations: 152
currently lose_sum: 94.47358524799347
time_elpased: 2.004
batch start
#iterations: 153
currently lose_sum: 94.51598513126373
time_elpased: 2.04
batch start
#iterations: 154
currently lose_sum: 94.66457855701447
time_elpased: 1.984
batch start
#iterations: 155
currently lose_sum: 94.34223258495331
time_elpased: 2.009
batch start
#iterations: 156
currently lose_sum: 94.24907904863358
time_elpased: 1.901
batch start
#iterations: 157
currently lose_sum: 94.80841827392578
time_elpased: 1.974
batch start
#iterations: 158
currently lose_sum: 94.67909854650497
time_elpased: 1.939
batch start
#iterations: 159
currently lose_sum: 94.72586351633072
time_elpased: 1.935
start validation test
0.594845360825
0.614785511188
0.511783472265
0.558575760979
0.594991188782
65.015
batch start
#iterations: 160
currently lose_sum: 93.93288767337799
time_elpased: 2.172
batch start
#iterations: 161
currently lose_sum: 93.93021130561829
time_elpased: 2.114
batch start
#iterations: 162
currently lose_sum: 94.17204165458679
time_elpased: 2.153
batch start
#iterations: 163
currently lose_sum: 94.40254735946655
time_elpased: 2.031
batch start
#iterations: 164
currently lose_sum: 94.2603365778923
time_elpased: 2.011
batch start
#iterations: 165
currently lose_sum: 94.30457705259323
time_elpased: 2.057
batch start
#iterations: 166
currently lose_sum: 93.91154623031616
time_elpased: 1.99
batch start
#iterations: 167
currently lose_sum: 94.04132091999054
time_elpased: 2.179
batch start
#iterations: 168
currently lose_sum: 93.35836547613144
time_elpased: 2.007
batch start
#iterations: 169
currently lose_sum: 93.99245023727417
time_elpased: 2.047
batch start
#iterations: 170
currently lose_sum: 94.35844320058823
time_elpased: 1.95
batch start
#iterations: 171
currently lose_sum: 94.0172027349472
time_elpased: 1.98
batch start
#iterations: 172
currently lose_sum: 94.03323823213577
time_elpased: 1.975
batch start
#iterations: 173
currently lose_sum: 93.95534896850586
time_elpased: 1.969
batch start
#iterations: 174
currently lose_sum: 93.98021936416626
time_elpased: 1.929
batch start
#iterations: 175
currently lose_sum: 93.99743908643723
time_elpased: 2.083
batch start
#iterations: 176
currently lose_sum: 93.64516168832779
time_elpased: 2.253
batch start
#iterations: 177
currently lose_sum: 93.92981159687042
time_elpased: 2.034
batch start
#iterations: 178
currently lose_sum: 93.78800630569458
time_elpased: 1.973
batch start
#iterations: 179
currently lose_sum: 94.04020738601685
time_elpased: 2.029
start validation test
0.591855670103
0.618934285337
0.481733045179
0.541782407407
0.592049007356
65.643
batch start
#iterations: 180
currently lose_sum: 93.86206418275833
time_elpased: 2.111
batch start
#iterations: 181
currently lose_sum: 93.88262355327606
time_elpased: 1.974
batch start
#iterations: 182
currently lose_sum: 93.61792123317719
time_elpased: 2.154
batch start
#iterations: 183
currently lose_sum: 93.47649419307709
time_elpased: 2.026
batch start
#iterations: 184
currently lose_sum: 93.37543046474457
time_elpased: 1.994
batch start
#iterations: 185
currently lose_sum: 93.57942962646484
time_elpased: 2.081
batch start
#iterations: 186
currently lose_sum: 93.63129824399948
time_elpased: 1.942
batch start
#iterations: 187
currently lose_sum: 93.08863478899002
time_elpased: 2.06
batch start
#iterations: 188
currently lose_sum: 93.54647773504257
time_elpased: 1.973
batch start
#iterations: 189
currently lose_sum: 93.35703408718109
time_elpased: 2.043
batch start
#iterations: 190
currently lose_sum: 93.96084851026535
time_elpased: 1.943
batch start
#iterations: 191
currently lose_sum: 93.64605128765106
time_elpased: 2.074
batch start
#iterations: 192
currently lose_sum: 93.18210935592651
time_elpased: 2.005
batch start
#iterations: 193
currently lose_sum: 93.73492300510406
time_elpased: 2.142
batch start
#iterations: 194
currently lose_sum: 93.31271928548813
time_elpased: 2.0
batch start
#iterations: 195
currently lose_sum: 93.50944572687149
time_elpased: 2.119
batch start
#iterations: 196
currently lose_sum: 93.43890112638474
time_elpased: 2.024
batch start
#iterations: 197
currently lose_sum: 93.50699055194855
time_elpased: 2.005
batch start
#iterations: 198
currently lose_sum: 93.69037139415741
time_elpased: 2.009
batch start
#iterations: 199
currently lose_sum: 93.08054411411285
time_elpased: 1.929
start validation test
0.58412371134
0.616471253002
0.449109807554
0.519647535127
0.584360749073
66.511
batch start
#iterations: 200
currently lose_sum: 93.49550426006317
time_elpased: 1.967
batch start
#iterations: 201
currently lose_sum: 93.27757132053375
time_elpased: 1.995
batch start
#iterations: 202
currently lose_sum: 93.00080132484436
time_elpased: 2.103
batch start
#iterations: 203
currently lose_sum: 93.51673293113708
time_elpased: 2.094
batch start
#iterations: 204
currently lose_sum: 93.09135764837265
time_elpased: 2.029
batch start
#iterations: 205
currently lose_sum: 93.4728387594223
time_elpased: 2.047
batch start
#iterations: 206
currently lose_sum: 93.5606569647789
time_elpased: 2.006
batch start
#iterations: 207
currently lose_sum: 93.31803107261658
time_elpased: 2.022
batch start
#iterations: 208
currently lose_sum: 93.16920453310013
time_elpased: 2.045
batch start
#iterations: 209
currently lose_sum: 92.89929819107056
time_elpased: 2.03
batch start
#iterations: 210
currently lose_sum: 93.26886868476868
time_elpased: 2.032
batch start
#iterations: 211
currently lose_sum: 93.13600069284439
time_elpased: 2.04
batch start
#iterations: 212
currently lose_sum: 93.00613135099411
time_elpased: 2.011
batch start
#iterations: 213
currently lose_sum: 92.80970114469528
time_elpased: 1.969
batch start
#iterations: 214
currently lose_sum: 93.12846058607101
time_elpased: 1.991
batch start
#iterations: 215
currently lose_sum: 93.09554290771484
time_elpased: 2.049
batch start
#iterations: 216
currently lose_sum: 92.8976953625679
time_elpased: 2.034
batch start
#iterations: 217
currently lose_sum: 92.91854947805405
time_elpased: 2.038
batch start
#iterations: 218
currently lose_sum: 93.21049720048904
time_elpased: 2.017
batch start
#iterations: 219
currently lose_sum: 92.85857504606247
time_elpased: 2.049
start validation test
0.583711340206
0.6180745431
0.442008850468
0.515420616825
0.583960120783
66.476
batch start
#iterations: 220
currently lose_sum: 93.13399946689606
time_elpased: 2.01
batch start
#iterations: 221
currently lose_sum: 92.64452689886093
time_elpased: 2.115
batch start
#iterations: 222
currently lose_sum: 92.96021449565887
time_elpased: 2.052
batch start
#iterations: 223
currently lose_sum: 92.72706872224808
time_elpased: 2.035
batch start
#iterations: 224
currently lose_sum: 93.16970944404602
time_elpased: 2.02
batch start
#iterations: 225
currently lose_sum: 92.57883363962173
time_elpased: 2.078
batch start
#iterations: 226
currently lose_sum: 93.08806693553925
time_elpased: 2.042
batch start
#iterations: 227
currently lose_sum: 92.67275500297546
time_elpased: 2.093
batch start
#iterations: 228
currently lose_sum: 92.60448658466339
time_elpased: 2.039
batch start
#iterations: 229
currently lose_sum: 92.82716774940491
time_elpased: 2.018
batch start
#iterations: 230
currently lose_sum: 93.07973098754883
time_elpased: 2.139
batch start
#iterations: 231
currently lose_sum: 93.02048724889755
time_elpased: 2.039
batch start
#iterations: 232
currently lose_sum: 92.72111386060715
time_elpased: 2.027
batch start
#iterations: 233
currently lose_sum: 92.76046144962311
time_elpased: 2.09
batch start
#iterations: 234
currently lose_sum: 92.83104938268661
time_elpased: 1.991
batch start
#iterations: 235
currently lose_sum: 92.79124182462692
time_elpased: 1.978
batch start
#iterations: 236
currently lose_sum: 92.93113249540329
time_elpased: 1.972
batch start
#iterations: 237
currently lose_sum: 92.79755276441574
time_elpased: 1.986
batch start
#iterations: 238
currently lose_sum: 92.66491448879242
time_elpased: 2.231
batch start
#iterations: 239
currently lose_sum: 92.53006255626678
time_elpased: 2.067
start validation test
0.583659793814
0.606548856549
0.480395183699
0.536151151439
0.583841090765
66.341
batch start
#iterations: 240
currently lose_sum: 92.23551398515701
time_elpased: 2.02
batch start
#iterations: 241
currently lose_sum: 92.57002800703049
time_elpased: 2.014
batch start
#iterations: 242
currently lose_sum: 92.74777245521545
time_elpased: 1.962
batch start
#iterations: 243
currently lose_sum: 92.76559740304947
time_elpased: 2.088
batch start
#iterations: 244
currently lose_sum: 92.79270285367966
time_elpased: 1.991
batch start
#iterations: 245
currently lose_sum: 92.62638872861862
time_elpased: 2.119
batch start
#iterations: 246
currently lose_sum: 92.64582949876785
time_elpased: 2.001
batch start
#iterations: 247
currently lose_sum: 92.46200478076935
time_elpased: 2.04
batch start
#iterations: 248
currently lose_sum: 92.35133695602417
time_elpased: 2.014
batch start
#iterations: 249
currently lose_sum: 92.8723697066307
time_elpased: 2.118
batch start
#iterations: 250
currently lose_sum: 92.68986982107162
time_elpased: 1.982
batch start
#iterations: 251
currently lose_sum: 92.61943930387497
time_elpased: 2.017
batch start
#iterations: 252
currently lose_sum: 92.15457934141159
time_elpased: 2.013
batch start
#iterations: 253
currently lose_sum: 92.14838570356369
time_elpased: 2.09
batch start
#iterations: 254
currently lose_sum: 92.24644166231155
time_elpased: 2.167
batch start
#iterations: 255
currently lose_sum: 92.57364946603775
time_elpased: 2.016
batch start
#iterations: 256
currently lose_sum: 92.43457067012787
time_elpased: 2.051
batch start
#iterations: 257
currently lose_sum: 91.87043994665146
time_elpased: 2.039
batch start
#iterations: 258
currently lose_sum: 92.29721409082413
time_elpased: 2.059
batch start
#iterations: 259
currently lose_sum: 92.05553126335144
time_elpased: 2.03
start validation test
0.585824742268
0.607517259013
0.489039827107
0.541878100234
0.585994663115
66.658
batch start
#iterations: 260
currently lose_sum: 91.90102583169937
time_elpased: 2.024
batch start
#iterations: 261
currently lose_sum: 92.25654435157776
time_elpased: 2.061
batch start
#iterations: 262
currently lose_sum: 92.06558752059937
time_elpased: 2.04
batch start
#iterations: 263
currently lose_sum: 92.19534456729889
time_elpased: 1.977
batch start
#iterations: 264
currently lose_sum: 91.93798476457596
time_elpased: 1.994
batch start
#iterations: 265
currently lose_sum: 92.58492857217789
time_elpased: 2.188
batch start
#iterations: 266
currently lose_sum: 91.86069118976593
time_elpased: 2.094
batch start
#iterations: 267
currently lose_sum: 92.17399024963379
time_elpased: 2.226
batch start
#iterations: 268
currently lose_sum: 91.89459562301636
time_elpased: 2.028
batch start
#iterations: 269
currently lose_sum: 92.59542894363403
time_elpased: 2.005
batch start
#iterations: 270
currently lose_sum: 91.98726505041122
time_elpased: 2.027
batch start
#iterations: 271
currently lose_sum: 91.99739050865173
time_elpased: 2.072
batch start
#iterations: 272
currently lose_sum: 92.07599121332169
time_elpased: 2.092
batch start
#iterations: 273
currently lose_sum: 92.13894122838974
time_elpased: 1.962
batch start
#iterations: 274
currently lose_sum: 91.92858874797821
time_elpased: 1.996
batch start
#iterations: 275
currently lose_sum: 92.56393921375275
time_elpased: 2.072
batch start
#iterations: 276
currently lose_sum: 92.49658626317978
time_elpased: 2.043
batch start
#iterations: 277
currently lose_sum: 92.26159888505936
time_elpased: 2.051
batch start
#iterations: 278
currently lose_sum: 91.25962781906128
time_elpased: 1.981
batch start
#iterations: 279
currently lose_sum: 91.96371400356293
time_elpased: 1.972
start validation test
0.584175257732
0.61288998358
0.46094473603
0.526167400881
0.58439160792
67.110
batch start
#iterations: 280
currently lose_sum: 91.85511863231659
time_elpased: 2.045
batch start
#iterations: 281
currently lose_sum: 92.31806045770645
time_elpased: 2.074
batch start
#iterations: 282
currently lose_sum: 91.74991488456726
time_elpased: 2.159
batch start
#iterations: 283
currently lose_sum: 92.01738721132278
time_elpased: 2.039
batch start
#iterations: 284
currently lose_sum: 91.88866847753525
time_elpased: 2.015
batch start
#iterations: 285
currently lose_sum: 91.59796398878098
time_elpased: 2.026
batch start
#iterations: 286
currently lose_sum: 91.63370096683502
time_elpased: 2.06
batch start
#iterations: 287
currently lose_sum: 92.09974658489227
time_elpased: 2.116
batch start
#iterations: 288
currently lose_sum: 91.48844969272614
time_elpased: 2.171
batch start
#iterations: 289
currently lose_sum: 92.08074581623077
time_elpased: 2.065
batch start
#iterations: 290
currently lose_sum: 92.02084523439407
time_elpased: 2.047
batch start
#iterations: 291
currently lose_sum: 91.92326557636261
time_elpased: 2.059
batch start
#iterations: 292
currently lose_sum: 91.75781589746475
time_elpased: 2.08
batch start
#iterations: 293
currently lose_sum: 92.09594172239304
time_elpased: 2.05
batch start
#iterations: 294
currently lose_sum: 91.47421741485596
time_elpased: 2.006
batch start
#iterations: 295
currently lose_sum: 91.75316423177719
time_elpased: 2.054
batch start
#iterations: 296
currently lose_sum: 91.61838191747665
time_elpased: 2.072
batch start
#iterations: 297
currently lose_sum: 91.65772491693497
time_elpased: 2.052
batch start
#iterations: 298
currently lose_sum: 92.09369593858719
time_elpased: 2.163
batch start
#iterations: 299
currently lose_sum: 91.63558840751648
time_elpased: 2.046
start validation test
0.586958762887
0.608895705521
0.490274776165
0.54318453908
0.587128506538
66.650
batch start
#iterations: 300
currently lose_sum: 91.24112665653229
time_elpased: 2.043
batch start
#iterations: 301
currently lose_sum: 91.65714472532272
time_elpased: 2.051
batch start
#iterations: 302
currently lose_sum: 91.41341876983643
time_elpased: 2.028
batch start
#iterations: 303
currently lose_sum: 91.28135704994202
time_elpased: 2.001
batch start
#iterations: 304
currently lose_sum: 91.74315357208252
time_elpased: 1.944
batch start
#iterations: 305
currently lose_sum: 92.11485981941223
time_elpased: 1.945
batch start
#iterations: 306
currently lose_sum: 91.75662273168564
time_elpased: 2.012
batch start
#iterations: 307
currently lose_sum: 91.68356800079346
time_elpased: 2.041
batch start
#iterations: 308
currently lose_sum: 91.40212959051132
time_elpased: 2.071
batch start
#iterations: 309
currently lose_sum: 91.322978079319
time_elpased: 2.111
batch start
#iterations: 310
currently lose_sum: 91.22433638572693
time_elpased: 2.061
batch start
#iterations: 311
currently lose_sum: 91.31349617242813
time_elpased: 2.108
batch start
#iterations: 312
currently lose_sum: 91.444431245327
time_elpased: 2.048
batch start
#iterations: 313
currently lose_sum: 91.16648072004318
time_elpased: 2.07
batch start
#iterations: 314
currently lose_sum: 91.03243041038513
time_elpased: 2.151
batch start
#iterations: 315
currently lose_sum: 91.32272744178772
time_elpased: 1.97
batch start
#iterations: 316
currently lose_sum: 91.37574464082718
time_elpased: 2.003
batch start
#iterations: 317
currently lose_sum: 91.96599125862122
time_elpased: 1.99
batch start
#iterations: 318
currently lose_sum: 91.46181732416153
time_elpased: 2.008
batch start
#iterations: 319
currently lose_sum: 91.36596542596817
time_elpased: 2.059
start validation test
0.586958762887
0.598179303987
0.534218380158
0.564392497961
0.587051356763
66.650
batch start
#iterations: 320
currently lose_sum: 91.4911835193634
time_elpased: 1.979
batch start
#iterations: 321
currently lose_sum: 91.3966389298439
time_elpased: 1.983
batch start
#iterations: 322
currently lose_sum: 91.3845962882042
time_elpased: 1.924
batch start
#iterations: 323
currently lose_sum: 91.28069198131561
time_elpased: 1.911
batch start
#iterations: 324
currently lose_sum: 91.45955902338028
time_elpased: 1.908
batch start
#iterations: 325
currently lose_sum: 91.4163481593132
time_elpased: 2.07
batch start
#iterations: 326
currently lose_sum: 91.02360159158707
time_elpased: 2.014
batch start
#iterations: 327
currently lose_sum: 90.998559653759
time_elpased: 2.041
batch start
#iterations: 328
currently lose_sum: 91.5107274055481
time_elpased: 2.026
batch start
#iterations: 329
currently lose_sum: 91.1189643740654
time_elpased: 2.033
batch start
#iterations: 330
currently lose_sum: 91.33279424905777
time_elpased: 2.071
batch start
#iterations: 331
currently lose_sum: 90.92842203378677
time_elpased: 2.07
batch start
#iterations: 332
currently lose_sum: 91.1874390244484
time_elpased: 2.026
batch start
#iterations: 333
currently lose_sum: 90.94887948036194
time_elpased: 2.058
batch start
#iterations: 334
currently lose_sum: 90.92603349685669
time_elpased: 2.048
batch start
#iterations: 335
currently lose_sum: 91.06786769628525
time_elpased: 1.985
batch start
#iterations: 336
currently lose_sum: 90.75870966911316
time_elpased: 1.985
batch start
#iterations: 337
currently lose_sum: 91.27791571617126
time_elpased: 1.942
batch start
#iterations: 338
currently lose_sum: 90.76023125648499
time_elpased: 1.919
batch start
#iterations: 339
currently lose_sum: 90.78336310386658
time_elpased: 1.941
start validation test
0.582989690722
0.609473825865
0.466090357106
0.528224865874
0.583194925532
67.821
batch start
#iterations: 340
currently lose_sum: 91.21920955181122
time_elpased: 2.067
batch start
#iterations: 341
currently lose_sum: 91.20104479789734
time_elpased: 2.106
batch start
#iterations: 342
currently lose_sum: 91.17636173963547
time_elpased: 2.074
batch start
#iterations: 343
currently lose_sum: 91.04120808839798
time_elpased: 2.077
batch start
#iterations: 344
currently lose_sum: 90.66072976589203
time_elpased: 2.052
batch start
#iterations: 345
currently lose_sum: 90.81622105836868
time_elpased: 2.155
batch start
#iterations: 346
currently lose_sum: 90.7077744603157
time_elpased: 2.052
batch start
#iterations: 347
currently lose_sum: 90.69182872772217
time_elpased: 2.053
batch start
#iterations: 348
currently lose_sum: 90.40036368370056
time_elpased: 2.03
batch start
#iterations: 349
currently lose_sum: 90.74975281953812
time_elpased: 2.104
batch start
#iterations: 350
currently lose_sum: 90.73646974563599
time_elpased: 2.017
batch start
#iterations: 351
currently lose_sum: 90.77535700798035
time_elpased: 2.056
batch start
#iterations: 352
currently lose_sum: 90.98428231477737
time_elpased: 2.055
batch start
#iterations: 353
currently lose_sum: 90.97584789991379
time_elpased: 2.071
batch start
#iterations: 354
currently lose_sum: 91.11150068044662
time_elpased: 2.046
batch start
#iterations: 355
currently lose_sum: 91.13891381025314
time_elpased: 2.044
batch start
#iterations: 356
currently lose_sum: 90.85762882232666
time_elpased: 2.08
batch start
#iterations: 357
currently lose_sum: 90.69429934024811
time_elpased: 2.062
batch start
#iterations: 358
currently lose_sum: 91.13194906711578
time_elpased: 2.054
batch start
#iterations: 359
currently lose_sum: 90.7371615767479
time_elpased: 2.039
start validation test
0.582989690722
0.598809668408
0.507358238139
0.54930362117
0.583122473402
67.529
batch start
#iterations: 360
currently lose_sum: 90.55591315031052
time_elpased: 2.051
batch start
#iterations: 361
currently lose_sum: 91.57944762706757
time_elpased: 2.044
batch start
#iterations: 362
currently lose_sum: 90.43744212388992
time_elpased: 2.019
batch start
#iterations: 363
currently lose_sum: 90.60880362987518
time_elpased: 2.092
batch start
#iterations: 364
currently lose_sum: 91.03802627325058
time_elpased: 1.991
batch start
#iterations: 365
currently lose_sum: 90.88623148202896
time_elpased: 2.053
batch start
#iterations: 366
currently lose_sum: 90.92343813180923
time_elpased: 2.039
batch start
#iterations: 367
currently lose_sum: 90.78993636369705
time_elpased: 2.078
batch start
#iterations: 368
currently lose_sum: 90.7390666604042
time_elpased: 2.067
batch start
#iterations: 369
currently lose_sum: 90.83812981843948
time_elpased: 2.102
batch start
#iterations: 370
currently lose_sum: 90.66435623168945
time_elpased: 2.029
batch start
#iterations: 371
currently lose_sum: 90.62856531143188
time_elpased: 2.03
batch start
#iterations: 372
currently lose_sum: 90.95686012506485
time_elpased: 2.063
batch start
#iterations: 373
currently lose_sum: 90.4580146074295
time_elpased: 2.061
batch start
#iterations: 374
currently lose_sum: 90.9283190369606
time_elpased: 2.064
batch start
#iterations: 375
currently lose_sum: 90.84632509946823
time_elpased: 2.057
batch start
#iterations: 376
currently lose_sum: 90.4268923997879
time_elpased: 2.052
batch start
#iterations: 377
currently lose_sum: 90.6063762307167
time_elpased: 1.945
batch start
#iterations: 378
currently lose_sum: 90.21103423833847
time_elpased: 2.036
batch start
#iterations: 379
currently lose_sum: 89.7632583975792
time_elpased: 2.024
start validation test
0.582216494845
0.605139577355
0.477410723474
0.533739860784
0.582400497542
67.931
batch start
#iterations: 380
currently lose_sum: 90.7497211098671
time_elpased: 2.065
batch start
#iterations: 381
currently lose_sum: 90.41835045814514
time_elpased: 2.144
batch start
#iterations: 382
currently lose_sum: 90.81383258104324
time_elpased: 2.048
batch start
#iterations: 383
currently lose_sum: 90.1739057302475
time_elpased: 2.039
batch start
#iterations: 384
currently lose_sum: 90.53098410367966
time_elpased: 2.044
batch start
#iterations: 385
currently lose_sum: 90.3687133193016
time_elpased: 2.103
batch start
#iterations: 386
currently lose_sum: 90.14953982830048
time_elpased: 2.006
batch start
#iterations: 387
currently lose_sum: 90.05194389820099
time_elpased: 2.014
batch start
#iterations: 388
currently lose_sum: 90.67336368560791
time_elpased: 1.89
batch start
#iterations: 389
currently lose_sum: 90.57114660739899
time_elpased: 1.89
batch start
#iterations: 390
currently lose_sum: 90.26143389940262
time_elpased: 1.88
batch start
#iterations: 391
currently lose_sum: 90.33213132619858
time_elpased: 1.89
batch start
#iterations: 392
currently lose_sum: 90.2218765616417
time_elpased: 1.86
batch start
#iterations: 393
currently lose_sum: 90.07070422172546
time_elpased: 1.838
batch start
#iterations: 394
currently lose_sum: 90.36396670341492
time_elpased: 1.834
batch start
#iterations: 395
currently lose_sum: 90.6975684762001
time_elpased: 1.841
batch start
#iterations: 396
currently lose_sum: 90.10879415273666
time_elpased: 1.832
batch start
#iterations: 397
currently lose_sum: 90.1141710281372
time_elpased: 1.831
batch start
#iterations: 398
currently lose_sum: 89.67157346010208
time_elpased: 1.882
batch start
#iterations: 399
currently lose_sum: 90.37078273296356
time_elpased: 1.916
start validation test
0.582164948454
0.612989199046
0.449727282083
0.518817523448
0.582397463204
69.028
acc: 0.598
pre: 0.627
rec: 0.487
F1: 0.548
auc: 0.598
