start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.39634281396866
time_elpased: 2.418
batch start
#iterations: 1
currently lose_sum: 100.12064164876938
time_elpased: 2.28
batch start
#iterations: 2
currently lose_sum: 99.8787333369255
time_elpased: 2.281
batch start
#iterations: 3
currently lose_sum: 99.82268005609512
time_elpased: 2.095
batch start
#iterations: 4
currently lose_sum: 99.65043592453003
time_elpased: 2.146
batch start
#iterations: 5
currently lose_sum: 99.54118168354034
time_elpased: 2.152
batch start
#iterations: 6
currently lose_sum: 99.54941117763519
time_elpased: 2.126
batch start
#iterations: 7
currently lose_sum: 99.12204819917679
time_elpased: 2.154
batch start
#iterations: 8
currently lose_sum: 99.30347162485123
time_elpased: 2.18
batch start
#iterations: 9
currently lose_sum: 99.18238931894302
time_elpased: 2.221
batch start
#iterations: 10
currently lose_sum: 99.04970562458038
time_elpased: 2.146
batch start
#iterations: 11
currently lose_sum: 98.97907948493958
time_elpased: 2.189
batch start
#iterations: 12
currently lose_sum: 98.93019396066666
time_elpased: 2.134
batch start
#iterations: 13
currently lose_sum: 98.66607904434204
time_elpased: 2.169
batch start
#iterations: 14
currently lose_sum: 98.68335026502609
time_elpased: 2.177
batch start
#iterations: 15
currently lose_sum: 98.30396682024002
time_elpased: 2.238
batch start
#iterations: 16
currently lose_sum: 98.39687103033066
time_elpased: 2.206
batch start
#iterations: 17
currently lose_sum: 98.3040155172348
time_elpased: 2.151
batch start
#iterations: 18
currently lose_sum: 98.17399972677231
time_elpased: 2.395
batch start
#iterations: 19
currently lose_sum: 98.34894835948944
time_elpased: 2.147
start validation test
0.602113402062
0.595946984249
0.638571575589
0.616523423916
0.602049394115
64.286
batch start
#iterations: 20
currently lose_sum: 98.20911985635757
time_elpased: 2.171
batch start
#iterations: 21
currently lose_sum: 97.84547019004822
time_elpased: 2.17
batch start
#iterations: 22
currently lose_sum: 98.14727360010147
time_elpased: 2.179
batch start
#iterations: 23
currently lose_sum: 98.1652661561966
time_elpased: 2.168
batch start
#iterations: 24
currently lose_sum: 98.10307466983795
time_elpased: 2.153
batch start
#iterations: 25
currently lose_sum: 97.92105334997177
time_elpased: 2.174
batch start
#iterations: 26
currently lose_sum: 97.94443500041962
time_elpased: 2.146
batch start
#iterations: 27
currently lose_sum: 97.75773280858994
time_elpased: 2.18
batch start
#iterations: 28
currently lose_sum: 97.8364571928978
time_elpased: 2.187
batch start
#iterations: 29
currently lose_sum: 97.84469771385193
time_elpased: 2.116
batch start
#iterations: 30
currently lose_sum: 97.82964026927948
time_elpased: 2.122
batch start
#iterations: 31
currently lose_sum: 97.53496706485748
time_elpased: 2.128
batch start
#iterations: 32
currently lose_sum: 97.37598377466202
time_elpased: 2.204
batch start
#iterations: 33
currently lose_sum: 97.80589550733566
time_elpased: 2.166
batch start
#iterations: 34
currently lose_sum: 97.3984060883522
time_elpased: 2.162
batch start
#iterations: 35
currently lose_sum: 97.24877160787582
time_elpased: 2.219
batch start
#iterations: 36
currently lose_sum: 97.36743754148483
time_elpased: 2.29
batch start
#iterations: 37
currently lose_sum: 97.57037419080734
time_elpased: 2.053
batch start
#iterations: 38
currently lose_sum: 97.28831499814987
time_elpased: 2.092
batch start
#iterations: 39
currently lose_sum: 97.05251038074493
time_elpased: 2.127
start validation test
0.599484536082
0.618358662614
0.523412575898
0.566937911047
0.599618092142
64.648
batch start
#iterations: 40
currently lose_sum: 97.26318567991257
time_elpased: 2.085
batch start
#iterations: 41
currently lose_sum: 97.29330116510391
time_elpased: 2.166
batch start
#iterations: 42
currently lose_sum: 96.83718717098236
time_elpased: 2.09
batch start
#iterations: 43
currently lose_sum: 97.1773995757103
time_elpased: 2.176
batch start
#iterations: 44
currently lose_sum: 96.86807388067245
time_elpased: 2.271
batch start
#iterations: 45
currently lose_sum: 97.0008773803711
time_elpased: 2.188
batch start
#iterations: 46
currently lose_sum: 96.78930413722992
time_elpased: 2.17
batch start
#iterations: 47
currently lose_sum: 96.74141246080399
time_elpased: 2.157
batch start
#iterations: 48
currently lose_sum: 97.10062611103058
time_elpased: 2.155
batch start
#iterations: 49
currently lose_sum: 96.67780202627182
time_elpased: 2.153
batch start
#iterations: 50
currently lose_sum: 96.82279378175735
time_elpased: 2.148
batch start
#iterations: 51
currently lose_sum: 96.67289298772812
time_elpased: 2.124
batch start
#iterations: 52
currently lose_sum: 96.81964802742004
time_elpased: 2.203
batch start
#iterations: 53
currently lose_sum: 96.75391709804535
time_elpased: 2.155
batch start
#iterations: 54
currently lose_sum: 96.74897330999374
time_elpased: 2.135
batch start
#iterations: 55
currently lose_sum: 96.83618986606598
time_elpased: 2.12
batch start
#iterations: 56
currently lose_sum: 96.48425924777985
time_elpased: 2.126
batch start
#iterations: 57
currently lose_sum: 96.57589721679688
time_elpased: 2.168
batch start
#iterations: 58
currently lose_sum: 96.56039488315582
time_elpased: 2.17
batch start
#iterations: 59
currently lose_sum: 96.96914464235306
time_elpased: 2.19
start validation test
0.599536082474
0.59639746635
0.620150252135
0.608041975682
0.59949989112
64.679
batch start
#iterations: 60
currently lose_sum: 96.3566637635231
time_elpased: 2.192
batch start
#iterations: 61
currently lose_sum: 96.19910657405853
time_elpased: 2.128
batch start
#iterations: 62
currently lose_sum: 96.41415840387344
time_elpased: 2.214
batch start
#iterations: 63
currently lose_sum: 96.58062940835953
time_elpased: 2.159
batch start
#iterations: 64
currently lose_sum: 96.4177577495575
time_elpased: 2.091
batch start
#iterations: 65
currently lose_sum: 95.9264822602272
time_elpased: 2.098
batch start
#iterations: 66
currently lose_sum: 96.14948123693466
time_elpased: 2.173
batch start
#iterations: 67
currently lose_sum: 96.15722101926804
time_elpased: 2.16
batch start
#iterations: 68
currently lose_sum: 96.00895345211029
time_elpased: 2.162
batch start
#iterations: 69
currently lose_sum: 96.22327256202698
time_elpased: 2.136
batch start
#iterations: 70
currently lose_sum: 96.08296936750412
time_elpased: 2.094
batch start
#iterations: 71
currently lose_sum: 95.73586255311966
time_elpased: 2.214
batch start
#iterations: 72
currently lose_sum: 95.94754725694656
time_elpased: 2.124
batch start
#iterations: 73
currently lose_sum: 95.57079499959946
time_elpased: 2.071
batch start
#iterations: 74
currently lose_sum: 95.71783936023712
time_elpased: 2.136
batch start
#iterations: 75
currently lose_sum: 95.76748639345169
time_elpased: 2.167
batch start
#iterations: 76
currently lose_sum: 95.43496924638748
time_elpased: 2.248
batch start
#iterations: 77
currently lose_sum: 95.77171093225479
time_elpased: 2.099
batch start
#iterations: 78
currently lose_sum: 95.81832152605057
time_elpased: 2.187
batch start
#iterations: 79
currently lose_sum: 95.82950812578201
time_elpased: 2.02
start validation test
0.609896907216
0.630006049607
0.535864978903
0.579134690246
0.610026881685
64.556
batch start
#iterations: 80
currently lose_sum: 95.75454676151276
time_elpased: 2.188
batch start
#iterations: 81
currently lose_sum: 95.51935768127441
time_elpased: 2.17
batch start
#iterations: 82
currently lose_sum: 95.76507776975632
time_elpased: 2.113
batch start
#iterations: 83
currently lose_sum: 95.1312524676323
time_elpased: 2.141
batch start
#iterations: 84
currently lose_sum: 95.59376060962677
time_elpased: 2.185
batch start
#iterations: 85
currently lose_sum: 95.33042681217194
time_elpased: 2.243
batch start
#iterations: 86
currently lose_sum: 95.342189848423
time_elpased: 2.184
batch start
#iterations: 87
currently lose_sum: 95.17020428180695
time_elpased: 2.223
batch start
#iterations: 88
currently lose_sum: 94.98303830623627
time_elpased: 2.183
batch start
#iterations: 89
currently lose_sum: 94.86966013908386
time_elpased: 2.149
batch start
#iterations: 90
currently lose_sum: 95.53704625368118
time_elpased: 2.137
batch start
#iterations: 91
currently lose_sum: 95.26804912090302
time_elpased: 2.162
batch start
#iterations: 92
currently lose_sum: 94.75745964050293
time_elpased: 2.143
batch start
#iterations: 93
currently lose_sum: 95.28235656023026
time_elpased: 2.206
batch start
#iterations: 94
currently lose_sum: 94.96707791090012
time_elpased: 2.148
batch start
#iterations: 95
currently lose_sum: 94.79048949480057
time_elpased: 2.192
batch start
#iterations: 96
currently lose_sum: 94.9203782081604
time_elpased: 2.147
batch start
#iterations: 97
currently lose_sum: 94.70241957902908
time_elpased: 2.163
batch start
#iterations: 98
currently lose_sum: 95.20175176858902
time_elpased: 2.216
batch start
#iterations: 99
currently lose_sum: 94.58408623933792
time_elpased: 2.275
start validation test
0.605412371134
0.631976446493
0.50807862509
0.563295110959
0.605583255538
64.664
batch start
#iterations: 100
currently lose_sum: 95.07928395271301
time_elpased: 2.136
batch start
#iterations: 101
currently lose_sum: 94.92040264606476
time_elpased: 2.171
batch start
#iterations: 102
currently lose_sum: 94.56416803598404
time_elpased: 2.172
batch start
#iterations: 103
currently lose_sum: 94.76982653141022
time_elpased: 2.142
batch start
#iterations: 104
currently lose_sum: 94.74723529815674
time_elpased: 2.148
batch start
#iterations: 105
currently lose_sum: 94.83268195390701
time_elpased: 2.185
batch start
#iterations: 106
currently lose_sum: 94.76472383737564
time_elpased: 2.134
batch start
#iterations: 107
currently lose_sum: 94.4009616971016
time_elpased: 2.172
batch start
#iterations: 108
currently lose_sum: 94.34642034769058
time_elpased: 2.121
batch start
#iterations: 109
currently lose_sum: 95.12798154354095
time_elpased: 2.186
batch start
#iterations: 110
currently lose_sum: 94.19901859760284
time_elpased: 2.165
batch start
#iterations: 111
currently lose_sum: 94.61832851171494
time_elpased: 2.178
batch start
#iterations: 112
currently lose_sum: 94.29443794488907
time_elpased: 2.292
batch start
#iterations: 113
currently lose_sum: 94.4718554019928
time_elpased: 2.201
batch start
#iterations: 114
currently lose_sum: 94.28628784418106
time_elpased: 2.188
batch start
#iterations: 115
currently lose_sum: 94.51891022920609
time_elpased: 2.256
batch start
#iterations: 116
currently lose_sum: 94.53387331962585
time_elpased: 2.143
batch start
#iterations: 117
currently lose_sum: 94.38573563098907
time_elpased: 2.096
batch start
#iterations: 118
currently lose_sum: 94.39069199562073
time_elpased: 2.123
batch start
#iterations: 119
currently lose_sum: 94.4873914718628
time_elpased: 2.137
start validation test
0.603041237113
0.617073170732
0.546773695585
0.579800294647
0.603140023461
65.413
batch start
#iterations: 120
currently lose_sum: 94.16572535037994
time_elpased: 2.14
batch start
#iterations: 121
currently lose_sum: 93.90639114379883
time_elpased: 2.205
batch start
#iterations: 122
currently lose_sum: 94.56653302907944
time_elpased: 2.196
batch start
#iterations: 123
currently lose_sum: 94.05371898412704
time_elpased: 2.194
batch start
#iterations: 124
currently lose_sum: 93.72597938776016
time_elpased: 2.267
batch start
#iterations: 125
currently lose_sum: 93.94193226099014
time_elpased: 2.165
batch start
#iterations: 126
currently lose_sum: 93.97786056995392
time_elpased: 2.17
batch start
#iterations: 127
currently lose_sum: 94.38448119163513
time_elpased: 2.179
batch start
#iterations: 128
currently lose_sum: 94.20308405160904
time_elpased: 2.255
batch start
#iterations: 129
currently lose_sum: 94.10410004854202
time_elpased: 2.152
batch start
#iterations: 130
currently lose_sum: 94.09515458345413
time_elpased: 2.144
batch start
#iterations: 131
currently lose_sum: 93.68315500020981
time_elpased: 2.144
batch start
#iterations: 132
currently lose_sum: 93.73230969905853
time_elpased: 2.239
batch start
#iterations: 133
currently lose_sum: 93.71902829408646
time_elpased: 2.194
batch start
#iterations: 134
currently lose_sum: 93.54900109767914
time_elpased: 2.21
batch start
#iterations: 135
currently lose_sum: 94.02904045581818
time_elpased: 2.149
batch start
#iterations: 136
currently lose_sum: 93.7097801566124
time_elpased: 2.17
batch start
#iterations: 137
currently lose_sum: 93.99363768100739
time_elpased: 2.127
batch start
#iterations: 138
currently lose_sum: 93.37035262584686
time_elpased: 2.359
batch start
#iterations: 139
currently lose_sum: 93.71488308906555
time_elpased: 2.162
start validation test
0.597319587629
0.616144372637
0.520016465987
0.56401384083
0.597455305182
65.696
batch start
#iterations: 140
currently lose_sum: 93.62577438354492
time_elpased: 2.184
batch start
#iterations: 141
currently lose_sum: 93.93333035707474
time_elpased: 2.168
batch start
#iterations: 142
currently lose_sum: 93.69168198108673
time_elpased: 2.133
batch start
#iterations: 143
currently lose_sum: 93.33101224899292
time_elpased: 2.209
batch start
#iterations: 144
currently lose_sum: 93.64065462350845
time_elpased: 2.165
batch start
#iterations: 145
currently lose_sum: 93.73053252696991
time_elpased: 2.157
batch start
#iterations: 146
currently lose_sum: 92.93412154912949
time_elpased: 2.155
batch start
#iterations: 147
currently lose_sum: 92.93185526132584
time_elpased: 2.14
batch start
#iterations: 148
currently lose_sum: 93.66265845298767
time_elpased: 2.171
batch start
#iterations: 149
currently lose_sum: 93.68918037414551
time_elpased: 2.169
batch start
#iterations: 150
currently lose_sum: 93.18789446353912
time_elpased: 2.158
batch start
#iterations: 151
currently lose_sum: 93.31011927127838
time_elpased: 2.107
batch start
#iterations: 152
currently lose_sum: 93.37297683954239
time_elpased: 2.21
batch start
#iterations: 153
currently lose_sum: 93.16636949777603
time_elpased: 2.149
batch start
#iterations: 154
currently lose_sum: 93.59052789211273
time_elpased: 2.148
batch start
#iterations: 155
currently lose_sum: 93.17709755897522
time_elpased: 2.206
batch start
#iterations: 156
currently lose_sum: 93.08243608474731
time_elpased: 2.139
batch start
#iterations: 157
currently lose_sum: 93.8731929063797
time_elpased: 2.169
batch start
#iterations: 158
currently lose_sum: 93.40887826681137
time_elpased: 2.183
batch start
#iterations: 159
currently lose_sum: 93.41647696495056
time_elpased: 2.148
start validation test
0.594742268041
0.625865110598
0.474632088093
0.539857193024
0.594953139988
66.807
batch start
#iterations: 160
currently lose_sum: 93.09626084566116
time_elpased: 2.191
batch start
#iterations: 161
currently lose_sum: 92.87232387065887
time_elpased: 2.157
batch start
#iterations: 162
currently lose_sum: 93.11977618932724
time_elpased: 2.155
batch start
#iterations: 163
currently lose_sum: 92.93013042211533
time_elpased: 2.222
batch start
#iterations: 164
currently lose_sum: 92.99590653181076
time_elpased: 2.256
batch start
#iterations: 165
currently lose_sum: 92.99042403697968
time_elpased: 2.228
batch start
#iterations: 166
currently lose_sum: 92.83318239450455
time_elpased: 2.156
batch start
#iterations: 167
currently lose_sum: 92.90582102537155
time_elpased: 2.198
batch start
#iterations: 168
currently lose_sum: 92.46721225976944
time_elpased: 2.206
batch start
#iterations: 169
currently lose_sum: 92.90163218975067
time_elpased: 2.384
batch start
#iterations: 170
currently lose_sum: 92.96543896198273
time_elpased: 2.162
batch start
#iterations: 171
currently lose_sum: 92.69075781106949
time_elpased: 2.155
batch start
#iterations: 172
currently lose_sum: 92.89117366075516
time_elpased: 2.307
batch start
#iterations: 173
currently lose_sum: 92.64206027984619
time_elpased: 2.269
batch start
#iterations: 174
currently lose_sum: 92.72042655944824
time_elpased: 2.179
batch start
#iterations: 175
currently lose_sum: 92.58014976978302
time_elpased: 2.115
batch start
#iterations: 176
currently lose_sum: 92.96979945898056
time_elpased: 2.191
batch start
#iterations: 177
currently lose_sum: 92.55894297361374
time_elpased: 2.187
batch start
#iterations: 178
currently lose_sum: 91.9955483675003
time_elpased: 2.225
batch start
#iterations: 179
currently lose_sum: 92.75839442014694
time_elpased: 2.196
start validation test
0.597474226804
0.622213681783
0.499845631368
0.554357130628
0.597645628862
66.796
batch start
#iterations: 180
currently lose_sum: 92.53549015522003
time_elpased: 2.194
batch start
#iterations: 181
currently lose_sum: 92.89160621166229
time_elpased: 2.119
batch start
#iterations: 182
currently lose_sum: 92.44238722324371
time_elpased: 2.108
batch start
#iterations: 183
currently lose_sum: 92.12576395273209
time_elpased: 2.099
batch start
#iterations: 184
currently lose_sum: 91.83996844291687
time_elpased: 2.1
batch start
#iterations: 185
currently lose_sum: 92.23901039361954
time_elpased: 2.108
batch start
#iterations: 186
currently lose_sum: 92.83168184757233
time_elpased: 2.204
batch start
#iterations: 187
currently lose_sum: 91.58934557437897
time_elpased: 2.098
batch start
#iterations: 188
currently lose_sum: 92.08969742059708
time_elpased: 2.211
batch start
#iterations: 189
currently lose_sum: 92.10027974843979
time_elpased: 2.279
batch start
#iterations: 190
currently lose_sum: 92.86279636621475
time_elpased: 2.159
batch start
#iterations: 191
currently lose_sum: 92.05692166090012
time_elpased: 2.163
batch start
#iterations: 192
currently lose_sum: 91.90008515119553
time_elpased: 2.178
batch start
#iterations: 193
currently lose_sum: 92.18152064085007
time_elpased: 2.179
batch start
#iterations: 194
currently lose_sum: 91.92311733961105
time_elpased: 2.162
batch start
#iterations: 195
currently lose_sum: 92.26839077472687
time_elpased: 2.207
batch start
#iterations: 196
currently lose_sum: 91.97895216941833
time_elpased: 2.096
batch start
#iterations: 197
currently lose_sum: 92.11153322458267
time_elpased: 2.131
batch start
#iterations: 198
currently lose_sum: 92.14908367395401
time_elpased: 2.111
batch start
#iterations: 199
currently lose_sum: 92.11309653520584
time_elpased: 2.114
start validation test
0.593608247423
0.617394645831
0.496037871771
0.550102716275
0.593779547266
67.795
batch start
#iterations: 200
currently lose_sum: 91.8745122551918
time_elpased: 2.139
batch start
#iterations: 201
currently lose_sum: 92.11431181430817
time_elpased: 2.143
batch start
#iterations: 202
currently lose_sum: 91.90633523464203
time_elpased: 2.176
batch start
#iterations: 203
currently lose_sum: 91.82088232040405
time_elpased: 2.15
batch start
#iterations: 204
currently lose_sum: 91.745370388031
time_elpased: 2.139
batch start
#iterations: 205
currently lose_sum: 91.70208436250687
time_elpased: 2.199
batch start
#iterations: 206
currently lose_sum: 91.54460471868515
time_elpased: 2.152
batch start
#iterations: 207
currently lose_sum: 92.21692281961441
time_elpased: 2.27
batch start
#iterations: 208
currently lose_sum: 91.78731274604797
time_elpased: 2.218
batch start
#iterations: 209
currently lose_sum: 91.30955290794373
time_elpased: 2.146
batch start
#iterations: 210
currently lose_sum: 91.93434852361679
time_elpased: 2.07
batch start
#iterations: 211
currently lose_sum: 91.37902706861496
time_elpased: 2.096
batch start
#iterations: 212
currently lose_sum: 91.74809974431992
time_elpased: 2.146
batch start
#iterations: 213
currently lose_sum: 91.51851153373718
time_elpased: 2.162
batch start
#iterations: 214
currently lose_sum: 91.17303448915482
time_elpased: 2.231
batch start
#iterations: 215
currently lose_sum: 91.44449031352997
time_elpased: 2.18
batch start
#iterations: 216
currently lose_sum: 91.34339785575867
time_elpased: 2.148
batch start
#iterations: 217
currently lose_sum: 91.50541293621063
time_elpased: 2.183
batch start
#iterations: 218
currently lose_sum: 91.66303819417953
time_elpased: 2.204
batch start
#iterations: 219
currently lose_sum: 91.46753352880478
time_elpased: 2.222
start validation test
0.591907216495
0.60768126346
0.522692188947
0.561991701245
0.592028734151
68.772
batch start
#iterations: 220
currently lose_sum: 91.58779048919678
time_elpased: 2.115
batch start
#iterations: 221
currently lose_sum: 90.78541076183319
time_elpased: 2.117
batch start
#iterations: 222
currently lose_sum: 91.15048831701279
time_elpased: 2.174
batch start
#iterations: 223
currently lose_sum: 91.34809613227844
time_elpased: 2.21
batch start
#iterations: 224
currently lose_sum: 91.4366580247879
time_elpased: 2.157
batch start
#iterations: 225
currently lose_sum: 90.76097220182419
time_elpased: 2.154
batch start
#iterations: 226
currently lose_sum: 91.69840598106384
time_elpased: 2.156
batch start
#iterations: 227
currently lose_sum: 91.06410098075867
time_elpased: 2.067
batch start
#iterations: 228
currently lose_sum: 90.85861420631409
time_elpased: 2.063
batch start
#iterations: 229
currently lose_sum: 90.95077723264694
time_elpased: 2.059
batch start
#iterations: 230
currently lose_sum: 91.41359573602676
time_elpased: 2.118
batch start
#iterations: 231
currently lose_sum: 91.3571247458458
time_elpased: 2.163
batch start
#iterations: 232
currently lose_sum: 90.81427037715912
time_elpased: 2.148
batch start
#iterations: 233
currently lose_sum: 90.9427660703659
time_elpased: 2.185
batch start
#iterations: 234
currently lose_sum: 90.94956248998642
time_elpased: 2.203
batch start
#iterations: 235
currently lose_sum: 91.12166655063629
time_elpased: 2.163
batch start
#iterations: 236
currently lose_sum: 91.155945956707
time_elpased: 2.149
batch start
#iterations: 237
currently lose_sum: 90.97639966011047
time_elpased: 2.061
batch start
#iterations: 238
currently lose_sum: 91.06942456960678
time_elpased: 2.049
batch start
#iterations: 239
currently lose_sum: 91.00655257701874
time_elpased: 2.115
start validation test
0.593505154639
0.614052572568
0.507255325718
0.555568079351
0.593656579517
68.736
batch start
#iterations: 240
currently lose_sum: 90.43107450008392
time_elpased: 2.098
batch start
#iterations: 241
currently lose_sum: 90.75980615615845
time_elpased: 2.191
batch start
#iterations: 242
currently lose_sum: 90.78028124570847
time_elpased: 2.471
batch start
#iterations: 243
currently lose_sum: 90.8676290512085
time_elpased: 2.232
batch start
#iterations: 244
currently lose_sum: 90.84742778539658
time_elpased: 2.179
batch start
#iterations: 245
currently lose_sum: 90.91034775972366
time_elpased: 2.173
batch start
#iterations: 246
currently lose_sum: 90.85655117034912
time_elpased: 2.338
batch start
#iterations: 247
currently lose_sum: 90.38441342115402
time_elpased: 2.181
batch start
#iterations: 248
currently lose_sum: 90.63204962015152
time_elpased: 2.17
batch start
#iterations: 249
currently lose_sum: 90.87120687961578
time_elpased: 2.201
batch start
#iterations: 250
currently lose_sum: 90.56297254562378
time_elpased: 2.412
batch start
#iterations: 251
currently lose_sum: 90.93453848361969
time_elpased: 2.168
batch start
#iterations: 252
currently lose_sum: 90.38003742694855
time_elpased: 2.202
batch start
#iterations: 253
currently lose_sum: 90.25670325756073
time_elpased: 2.295
batch start
#iterations: 254
currently lose_sum: 90.68915623426437
time_elpased: 2.189
batch start
#iterations: 255
currently lose_sum: 90.55800718069077
time_elpased: 2.188
batch start
#iterations: 256
currently lose_sum: 90.72271823883057
time_elpased: 2.184
batch start
#iterations: 257
currently lose_sum: 89.85921943187714
time_elpased: 2.199
batch start
#iterations: 258
currently lose_sum: 90.65253096818924
time_elpased: 2.193
batch start
#iterations: 259
currently lose_sum: 90.5644901394844
time_elpased: 2.162
start validation test
0.590824742268
0.616625147502
0.483997118452
0.542320110701
0.591012294639
70.687
batch start
#iterations: 260
currently lose_sum: 90.31176567077637
time_elpased: 2.17
batch start
#iterations: 261
currently lose_sum: 90.43530291318893
time_elpased: 2.213
batch start
#iterations: 262
currently lose_sum: 90.11811500787735
time_elpased: 2.252
batch start
#iterations: 263
currently lose_sum: 90.55317389965057
time_elpased: 2.16
batch start
#iterations: 264
currently lose_sum: 89.69030559062958
time_elpased: 2.24
batch start
#iterations: 265
currently lose_sum: 90.54731321334839
time_elpased: 2.356
batch start
#iterations: 266
currently lose_sum: 90.14267361164093
time_elpased: 2.176
batch start
#iterations: 267
currently lose_sum: 90.53030461072922
time_elpased: 2.221
batch start
#iterations: 268
currently lose_sum: 89.90553134679794
time_elpased: 2.216
batch start
#iterations: 269
currently lose_sum: 89.9787649512291
time_elpased: 2.117
batch start
#iterations: 270
currently lose_sum: 90.05175107717514
time_elpased: 2.13
batch start
#iterations: 271
currently lose_sum: 89.95688599348068
time_elpased: 2.155
batch start
#iterations: 272
currently lose_sum: 89.92919290065765
time_elpased: 2.221
batch start
#iterations: 273
currently lose_sum: 90.40694844722748
time_elpased: 2.164
batch start
#iterations: 274
currently lose_sum: 89.92598569393158
time_elpased: 2.191
batch start
#iterations: 275
currently lose_sum: 90.58884853124619
time_elpased: 2.184
batch start
#iterations: 276
currently lose_sum: 89.67497450113297
time_elpased: 2.173
batch start
#iterations: 277
currently lose_sum: 90.10655564069748
time_elpased: 2.149
batch start
#iterations: 278
currently lose_sum: 89.87664818763733
time_elpased: 2.157
batch start
#iterations: 279
currently lose_sum: 89.93931710720062
time_elpased: 2.144
start validation test
0.594329896907
0.626351074018
0.471133065761
0.537765769999
0.594546187946
69.929
batch start
#iterations: 280
currently lose_sum: 89.29769253730774
time_elpased: 2.204
batch start
#iterations: 281
currently lose_sum: 90.31367284059525
time_elpased: 2.155
batch start
#iterations: 282
currently lose_sum: 89.83843612670898
time_elpased: 2.206
batch start
#iterations: 283
currently lose_sum: 90.20114719867706
time_elpased: 2.194
batch start
#iterations: 284
currently lose_sum: 90.04153120517731
time_elpased: 2.148
batch start
#iterations: 285
currently lose_sum: 89.31162887811661
time_elpased: 2.235
batch start
#iterations: 286
currently lose_sum: 89.71390962600708
time_elpased: 2.161
batch start
#iterations: 287
currently lose_sum: 90.20188921689987
time_elpased: 2.198
batch start
#iterations: 288
currently lose_sum: 89.82597118616104
time_elpased: 2.254
batch start
#iterations: 289
currently lose_sum: 89.83269482851028
time_elpased: 2.134
batch start
#iterations: 290
currently lose_sum: 89.53426772356033
time_elpased: 2.176
batch start
#iterations: 291
currently lose_sum: 89.41301620006561
time_elpased: 2.15
batch start
#iterations: 292
currently lose_sum: 89.39041894674301
time_elpased: 2.143
batch start
#iterations: 293
currently lose_sum: 89.46806240081787
time_elpased: 2.149
batch start
#iterations: 294
currently lose_sum: 89.54094243049622
time_elpased: 2.171
batch start
#iterations: 295
currently lose_sum: 89.38790851831436
time_elpased: 2.269
batch start
#iterations: 296
currently lose_sum: 89.22895646095276
time_elpased: 2.162
batch start
#iterations: 297
currently lose_sum: 89.5210788846016
time_elpased: 2.329
batch start
#iterations: 298
currently lose_sum: 89.53948700428009
time_elpased: 2.217
batch start
#iterations: 299
currently lose_sum: 89.19652104377747
time_elpased: 2.286
start validation test
0.589845360825
0.62188365651
0.462076772666
0.53020015351
0.590069678288
70.527
batch start
#iterations: 300
currently lose_sum: 89.30006194114685
time_elpased: 2.179
batch start
#iterations: 301
currently lose_sum: 89.67692941427231
time_elpased: 2.201
batch start
#iterations: 302
currently lose_sum: 89.49949663877487
time_elpased: 2.176
batch start
#iterations: 303
currently lose_sum: 89.33899581432343
time_elpased: 2.137
batch start
#iterations: 304
currently lose_sum: 89.42821645736694
time_elpased: 2.24
batch start
#iterations: 305
currently lose_sum: 89.59080111980438
time_elpased: 2.155
batch start
#iterations: 306
currently lose_sum: 89.20770579576492
time_elpased: 2.169
batch start
#iterations: 307
currently lose_sum: 89.2027251124382
time_elpased: 2.156
batch start
#iterations: 308
currently lose_sum: 88.91663497686386
time_elpased: 2.173
batch start
#iterations: 309
currently lose_sum: 89.39324378967285
time_elpased: 2.159
batch start
#iterations: 310
currently lose_sum: 89.14910763502121
time_elpased: 2.171
batch start
#iterations: 311
currently lose_sum: 89.10061854124069
time_elpased: 2.147
batch start
#iterations: 312
currently lose_sum: 89.05478167533875
time_elpased: 2.193
batch start
#iterations: 313
currently lose_sum: 89.06281125545502
time_elpased: 2.138
batch start
#iterations: 314
currently lose_sum: 89.18232625722885
time_elpased: 2.151
batch start
#iterations: 315
currently lose_sum: 88.33650195598602
time_elpased: 2.145
batch start
#iterations: 316
currently lose_sum: 88.81405049562454
time_elpased: 2.2
batch start
#iterations: 317
currently lose_sum: 89.38512551784515
time_elpased: 2.253
batch start
#iterations: 318
currently lose_sum: 89.51279908418655
time_elpased: 2.193
batch start
#iterations: 319
currently lose_sum: 88.3726464509964
time_elpased: 2.13
start validation test
0.59206185567
0.615888931739
0.493053411547
0.547668038409
0.592235680265
70.431
batch start
#iterations: 320
currently lose_sum: 89.55285626649857
time_elpased: 2.11
batch start
#iterations: 321
currently lose_sum: 88.8571971654892
time_elpased: 2.233
batch start
#iterations: 322
currently lose_sum: 88.71872591972351
time_elpased: 2.218
batch start
#iterations: 323
currently lose_sum: 88.31533408164978
time_elpased: 2.125
batch start
#iterations: 324
currently lose_sum: 88.98134624958038
time_elpased: 2.133
batch start
#iterations: 325
currently lose_sum: 88.7435417175293
time_elpased: 2.168
batch start
#iterations: 326
currently lose_sum: 88.74765998125076
time_elpased: 2.417
batch start
#iterations: 327
currently lose_sum: 88.73496949672699
time_elpased: 2.201
batch start
#iterations: 328
currently lose_sum: 89.14632189273834
time_elpased: 2.163
batch start
#iterations: 329
currently lose_sum: 88.6066465973854
time_elpased: 2.147
batch start
#iterations: 330
currently lose_sum: 88.61039513349533
time_elpased: 2.204
batch start
#iterations: 331
currently lose_sum: 88.51985043287277
time_elpased: 2.077
batch start
#iterations: 332
currently lose_sum: 88.13550972938538
time_elpased: 2.189
batch start
#iterations: 333
currently lose_sum: 88.2069433927536
time_elpased: 2.126
batch start
#iterations: 334
currently lose_sum: 87.67036467790604
time_elpased: 2.168
batch start
#iterations: 335
currently lose_sum: 88.82714509963989
time_elpased: 2.196
batch start
#iterations: 336
currently lose_sum: 88.57635271549225
time_elpased: 2.169
batch start
#iterations: 337
currently lose_sum: 88.78391885757446
time_elpased: 2.12
batch start
#iterations: 338
currently lose_sum: 88.57698237895966
time_elpased: 2.142
batch start
#iterations: 339
currently lose_sum: 87.99637722969055
time_elpased: 2.214
start validation test
0.590206185567
0.611342155009
0.499228156839
0.549626104691
0.590365911529
70.781
batch start
#iterations: 340
currently lose_sum: 88.28537553548813
time_elpased: 2.074
batch start
#iterations: 341
currently lose_sum: 88.15826046466827
time_elpased: 2.083
batch start
#iterations: 342
currently lose_sum: 87.88780874013901
time_elpased: 2.235
batch start
#iterations: 343
currently lose_sum: 88.47672998905182
time_elpased: 2.089
batch start
#iterations: 344
currently lose_sum: 88.19860941171646
time_elpased: 2.088
batch start
#iterations: 345
currently lose_sum: 88.00005549192429
time_elpased: 2.096
batch start
#iterations: 346
currently lose_sum: 88.43983560800552
time_elpased: 2.159
batch start
#iterations: 347
currently lose_sum: 88.18509483337402
time_elpased: 2.257
batch start
#iterations: 348
currently lose_sum: 88.26610833406448
time_elpased: 2.115
batch start
#iterations: 349
currently lose_sum: 88.60595768690109
time_elpased: 2.042
batch start
#iterations: 350
currently lose_sum: 88.3591855764389
time_elpased: 2.033
batch start
#iterations: 351
currently lose_sum: 88.03611880540848
time_elpased: 2.076
batch start
#iterations: 352
currently lose_sum: 87.84985774755478
time_elpased: 2.079
batch start
#iterations: 353
currently lose_sum: 88.31772065162659
time_elpased: 2.133
batch start
#iterations: 354
currently lose_sum: 88.13286656141281
time_elpased: 2.138
batch start
#iterations: 355
currently lose_sum: 88.35186409950256
time_elpased: 2.159
batch start
#iterations: 356
currently lose_sum: 87.48073816299438
time_elpased: 2.157
batch start
#iterations: 357
currently lose_sum: 87.73077374696732
time_elpased: 2.196
batch start
#iterations: 358
currently lose_sum: 88.24082392454147
time_elpased: 2.141
batch start
#iterations: 359
currently lose_sum: 87.95518451929092
time_elpased: 2.163
start validation test
0.585412371134
0.618588835364
0.449315632397
0.520536512668
0.58565130995
73.378
batch start
#iterations: 360
currently lose_sum: 88.12388986349106
time_elpased: 2.15
batch start
#iterations: 361
currently lose_sum: 88.41526234149933
time_elpased: 2.276
batch start
#iterations: 362
currently lose_sum: 87.86740827560425
time_elpased: 2.081
batch start
#iterations: 363
currently lose_sum: 88.06911367177963
time_elpased: 2.066
batch start
#iterations: 364
currently lose_sum: 88.1172102689743
time_elpased: 2.081
batch start
#iterations: 365
currently lose_sum: 87.74743986129761
time_elpased: 2.238
batch start
#iterations: 366
currently lose_sum: 87.98741227388382
time_elpased: 2.184
batch start
#iterations: 367
currently lose_sum: 88.08436298370361
time_elpased: 2.097
batch start
#iterations: 368
currently lose_sum: 88.08108168840408
time_elpased: 2.113
batch start
#iterations: 369
currently lose_sum: 87.99721413850784
time_elpased: 2.172
batch start
#iterations: 370
currently lose_sum: 87.8192327618599
time_elpased: 2.259
batch start
#iterations: 371
currently lose_sum: 87.4230996966362
time_elpased: 2.136
batch start
#iterations: 372
currently lose_sum: 87.81525695323944
time_elpased: 2.157
batch start
#iterations: 373
currently lose_sum: 87.7097452878952
time_elpased: 2.155
batch start
#iterations: 374
currently lose_sum: 88.1616683602333
time_elpased: 2.215
batch start
#iterations: 375
currently lose_sum: 87.8658549785614
time_elpased: 2.325
batch start
#iterations: 376
currently lose_sum: 87.40268433094025
time_elpased: 2.196
batch start
#iterations: 377
currently lose_sum: 88.3546958565712
time_elpased: 2.062
batch start
#iterations: 378
currently lose_sum: 87.43497335910797
time_elpased: 2.125
batch start
#iterations: 379
currently lose_sum: 87.56272029876709
time_elpased: 2.239
start validation test
0.586855670103
0.623727827857
0.441494288361
0.517023199759
0.587110874429
73.282
batch start
#iterations: 380
currently lose_sum: 87.71029901504517
time_elpased: 2.171
batch start
#iterations: 381
currently lose_sum: 87.39766305685043
time_elpased: 2.198
batch start
#iterations: 382
currently lose_sum: 87.38654911518097
time_elpased: 2.125
batch start
#iterations: 383
currently lose_sum: 87.61582273244858
time_elpased: 2.125
batch start
#iterations: 384
currently lose_sum: 87.2897162437439
time_elpased: 2.111
batch start
#iterations: 385
currently lose_sum: 87.09822714328766
time_elpased: 2.256
batch start
#iterations: 386
currently lose_sum: 87.21813464164734
time_elpased: 2.209
batch start
#iterations: 387
currently lose_sum: 86.91846430301666
time_elpased: 2.152
batch start
#iterations: 388
currently lose_sum: 87.50827127695084
time_elpased: 2.154
batch start
#iterations: 389
currently lose_sum: 87.6945931315422
time_elpased: 2.201
batch start
#iterations: 390
currently lose_sum: 87.3174335360527
time_elpased: 2.146
batch start
#iterations: 391
currently lose_sum: 86.95353972911835
time_elpased: 2.135
batch start
#iterations: 392
currently lose_sum: 86.9969168305397
time_elpased: 2.162
batch start
#iterations: 393
currently lose_sum: 87.14672231674194
time_elpased: 2.147
batch start
#iterations: 394
currently lose_sum: 87.26358968019485
time_elpased: 2.116
batch start
#iterations: 395
currently lose_sum: 87.31678634881973
time_elpased: 2.05
batch start
#iterations: 396
currently lose_sum: 86.8854775428772
time_elpased: 2.137
batch start
#iterations: 397
currently lose_sum: 86.8880546092987
time_elpased: 2.175
batch start
#iterations: 398
currently lose_sum: 86.77057641744614
time_elpased: 2.163
batch start
#iterations: 399
currently lose_sum: 86.78541201353073
time_elpased: 2.174
start validation test
0.586288659794
0.623268698061
0.439950602038
0.515805984556
0.586545578825
73.539
acc: 0.607
pre: 0.601
rec: 0.642
F1: 0.621
auc: 0.607
