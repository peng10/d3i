start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.42240649461746
time_elpased: 1.447
batch start
#iterations: 1
currently lose_sum: 100.24314028024673
time_elpased: 1.467
batch start
#iterations: 2
currently lose_sum: 99.98662286996841
time_elpased: 1.452
batch start
#iterations: 3
currently lose_sum: 99.8987609744072
time_elpased: 1.463
batch start
#iterations: 4
currently lose_sum: 99.74428570270538
time_elpased: 1.461
batch start
#iterations: 5
currently lose_sum: 99.67624163627625
time_elpased: 1.49
batch start
#iterations: 6
currently lose_sum: 99.54833900928497
time_elpased: 1.433
batch start
#iterations: 7
currently lose_sum: 99.37954437732697
time_elpased: 1.448
batch start
#iterations: 8
currently lose_sum: 99.35810601711273
time_elpased: 1.464
batch start
#iterations: 9
currently lose_sum: 99.10467684268951
time_elpased: 1.451
batch start
#iterations: 10
currently lose_sum: 99.19545429944992
time_elpased: 1.472
batch start
#iterations: 11
currently lose_sum: 99.20287001132965
time_elpased: 1.527
batch start
#iterations: 12
currently lose_sum: 98.96270072460175
time_elpased: 1.445
batch start
#iterations: 13
currently lose_sum: 99.11759865283966
time_elpased: 1.458
batch start
#iterations: 14
currently lose_sum: 98.70231503248215
time_elpased: 1.447
batch start
#iterations: 15
currently lose_sum: 98.7625772356987
time_elpased: 1.46
batch start
#iterations: 16
currently lose_sum: 98.81578928232193
time_elpased: 1.444
batch start
#iterations: 17
currently lose_sum: 98.72826737165451
time_elpased: 1.442
batch start
#iterations: 18
currently lose_sum: 98.49135887622833
time_elpased: 1.465
batch start
#iterations: 19
currently lose_sum: 98.57054960727692
time_elpased: 1.444
start validation test
0.602525773196
0.592153619993
0.663270556756
0.625697781661
0.602419126359
64.821
batch start
#iterations: 20
currently lose_sum: 98.48825430870056
time_elpased: 1.462
batch start
#iterations: 21
currently lose_sum: 98.4119701385498
time_elpased: 1.458
batch start
#iterations: 22
currently lose_sum: 98.34171462059021
time_elpased: 1.462
batch start
#iterations: 23
currently lose_sum: 98.69363510608673
time_elpased: 1.428
batch start
#iterations: 24
currently lose_sum: 98.05272841453552
time_elpased: 1.45
batch start
#iterations: 25
currently lose_sum: 98.04678672552109
time_elpased: 1.423
batch start
#iterations: 26
currently lose_sum: 98.22050642967224
time_elpased: 1.462
batch start
#iterations: 27
currently lose_sum: 98.05062359571457
time_elpased: 1.45
batch start
#iterations: 28
currently lose_sum: 97.9443975687027
time_elpased: 1.491
batch start
#iterations: 29
currently lose_sum: 97.76869714260101
time_elpased: 1.453
batch start
#iterations: 30
currently lose_sum: 97.99708670377731
time_elpased: 1.434
batch start
#iterations: 31
currently lose_sum: 97.65701186656952
time_elpased: 1.454
batch start
#iterations: 32
currently lose_sum: 97.88119250535965
time_elpased: 1.438
batch start
#iterations: 33
currently lose_sum: 97.51461553573608
time_elpased: 1.495
batch start
#iterations: 34
currently lose_sum: 97.50356686115265
time_elpased: 1.469
batch start
#iterations: 35
currently lose_sum: 97.58294975757599
time_elpased: 1.445
batch start
#iterations: 36
currently lose_sum: 97.60452115535736
time_elpased: 1.469
batch start
#iterations: 37
currently lose_sum: 97.44367671012878
time_elpased: 1.435
batch start
#iterations: 38
currently lose_sum: 97.49357277154922
time_elpased: 1.465
batch start
#iterations: 39
currently lose_sum: 97.15804362297058
time_elpased: 1.482
start validation test
0.607422680412
0.619497213059
0.560461047648
0.588502269289
0.607505128802
64.340
batch start
#iterations: 40
currently lose_sum: 97.6170049905777
time_elpased: 1.47
batch start
#iterations: 41
currently lose_sum: 97.33658808469772
time_elpased: 1.485
batch start
#iterations: 42
currently lose_sum: 97.23961567878723
time_elpased: 1.44
batch start
#iterations: 43
currently lose_sum: 97.30855816602707
time_elpased: 1.438
batch start
#iterations: 44
currently lose_sum: 97.24118649959564
time_elpased: 1.473
batch start
#iterations: 45
currently lose_sum: 96.85472518205643
time_elpased: 1.462
batch start
#iterations: 46
currently lose_sum: 97.0305387377739
time_elpased: 1.445
batch start
#iterations: 47
currently lose_sum: 97.39614534378052
time_elpased: 1.458
batch start
#iterations: 48
currently lose_sum: 96.93016600608826
time_elpased: 1.44
batch start
#iterations: 49
currently lose_sum: 96.88046669960022
time_elpased: 1.447
batch start
#iterations: 50
currently lose_sum: 96.9427490234375
time_elpased: 1.459
batch start
#iterations: 51
currently lose_sum: 96.7286257147789
time_elpased: 1.465
batch start
#iterations: 52
currently lose_sum: 96.85801249742508
time_elpased: 1.484
batch start
#iterations: 53
currently lose_sum: 96.7897127866745
time_elpased: 1.461
batch start
#iterations: 54
currently lose_sum: 96.79978549480438
time_elpased: 1.497
batch start
#iterations: 55
currently lose_sum: 96.80584752559662
time_elpased: 1.457
batch start
#iterations: 56
currently lose_sum: 96.8277074098587
time_elpased: 1.429
batch start
#iterations: 57
currently lose_sum: 96.58521258831024
time_elpased: 1.437
batch start
#iterations: 58
currently lose_sum: 96.84547853469849
time_elpased: 1.468
batch start
#iterations: 59
currently lose_sum: 96.74809819459915
time_elpased: 1.468
start validation test
0.593917525773
0.608752217623
0.529690233611
0.566475896984
0.594030286691
64.572
batch start
#iterations: 60
currently lose_sum: 96.6425352692604
time_elpased: 1.463
batch start
#iterations: 61
currently lose_sum: 96.9692742228508
time_elpased: 1.476
batch start
#iterations: 62
currently lose_sum: 96.58193725347519
time_elpased: 1.439
batch start
#iterations: 63
currently lose_sum: 96.54613089561462
time_elpased: 1.479
batch start
#iterations: 64
currently lose_sum: 96.47426337003708
time_elpased: 1.454
batch start
#iterations: 65
currently lose_sum: 96.45320159196854
time_elpased: 1.457
batch start
#iterations: 66
currently lose_sum: 96.35047417879105
time_elpased: 1.43
batch start
#iterations: 67
currently lose_sum: 95.90735471248627
time_elpased: 1.445
batch start
#iterations: 68
currently lose_sum: 96.21386557817459
time_elpased: 1.429
batch start
#iterations: 69
currently lose_sum: 96.62059849500656
time_elpased: 1.462
batch start
#iterations: 70
currently lose_sum: 96.15375608205795
time_elpased: 1.44
batch start
#iterations: 71
currently lose_sum: 95.79111790657043
time_elpased: 1.438
batch start
#iterations: 72
currently lose_sum: 96.21912807226181
time_elpased: 1.434
batch start
#iterations: 73
currently lose_sum: 96.12408882379532
time_elpased: 1.456
batch start
#iterations: 74
currently lose_sum: 95.71705722808838
time_elpased: 1.465
batch start
#iterations: 75
currently lose_sum: 95.9219640493393
time_elpased: 1.433
batch start
#iterations: 76
currently lose_sum: 95.9723527431488
time_elpased: 1.44
batch start
#iterations: 77
currently lose_sum: 95.91586941480637
time_elpased: 1.531
batch start
#iterations: 78
currently lose_sum: 96.22278255224228
time_elpased: 1.491
batch start
#iterations: 79
currently lose_sum: 95.81436532735825
time_elpased: 1.465
start validation test
0.606649484536
0.617560865645
0.563857157559
0.589488407122
0.606724613066
64.268
batch start
#iterations: 80
currently lose_sum: 95.85099345445633
time_elpased: 1.477
batch start
#iterations: 81
currently lose_sum: 96.05287939310074
time_elpased: 1.464
batch start
#iterations: 82
currently lose_sum: 95.78109914064407
time_elpased: 1.466
batch start
#iterations: 83
currently lose_sum: 95.61363017559052
time_elpased: 1.434
batch start
#iterations: 84
currently lose_sum: 96.14381355047226
time_elpased: 1.455
batch start
#iterations: 85
currently lose_sum: 95.69180244207382
time_elpased: 1.508
batch start
#iterations: 86
currently lose_sum: 95.5623996257782
time_elpased: 1.498
batch start
#iterations: 87
currently lose_sum: 95.7496148943901
time_elpased: 1.465
batch start
#iterations: 88
currently lose_sum: 95.5699674487114
time_elpased: 1.478
batch start
#iterations: 89
currently lose_sum: 95.63802742958069
time_elpased: 1.442
batch start
#iterations: 90
currently lose_sum: 95.5535261631012
time_elpased: 1.438
batch start
#iterations: 91
currently lose_sum: 95.90908271074295
time_elpased: 1.429
batch start
#iterations: 92
currently lose_sum: 95.32475608587265
time_elpased: 1.438
batch start
#iterations: 93
currently lose_sum: 95.18512034416199
time_elpased: 1.461
batch start
#iterations: 94
currently lose_sum: 95.87010848522186
time_elpased: 1.433
batch start
#iterations: 95
currently lose_sum: 95.64687889814377
time_elpased: 1.458
batch start
#iterations: 96
currently lose_sum: 95.3532389998436
time_elpased: 1.43
batch start
#iterations: 97
currently lose_sum: 95.65108090639114
time_elpased: 1.435
batch start
#iterations: 98
currently lose_sum: 95.63446497917175
time_elpased: 1.432
batch start
#iterations: 99
currently lose_sum: 95.5296448469162
time_elpased: 1.463
start validation test
0.600670103093
0.608432408631
0.568796953792
0.587947449604
0.600726061322
64.535
batch start
#iterations: 100
currently lose_sum: 95.18846297264099
time_elpased: 1.434
batch start
#iterations: 101
currently lose_sum: 94.98404210805893
time_elpased: 1.461
batch start
#iterations: 102
currently lose_sum: 95.48506897687912
time_elpased: 1.475
batch start
#iterations: 103
currently lose_sum: 95.11716485023499
time_elpased: 1.461
batch start
#iterations: 104
currently lose_sum: 95.24324107170105
time_elpased: 1.442
batch start
#iterations: 105
currently lose_sum: 94.98717731237411
time_elpased: 1.443
batch start
#iterations: 106
currently lose_sum: 95.10036814212799
time_elpased: 1.461
batch start
#iterations: 107
currently lose_sum: 95.24084085226059
time_elpased: 1.474
batch start
#iterations: 108
currently lose_sum: 95.04873877763748
time_elpased: 1.466
batch start
#iterations: 109
currently lose_sum: 94.86200058460236
time_elpased: 1.462
batch start
#iterations: 110
currently lose_sum: 94.83620530366898
time_elpased: 1.423
batch start
#iterations: 111
currently lose_sum: 95.03827369213104
time_elpased: 1.434
batch start
#iterations: 112
currently lose_sum: 94.80658531188965
time_elpased: 1.444
batch start
#iterations: 113
currently lose_sum: 94.4313337802887
time_elpased: 1.473
batch start
#iterations: 114
currently lose_sum: 94.68937569856644
time_elpased: 1.438
batch start
#iterations: 115
currently lose_sum: 94.6045241355896
time_elpased: 1.456
batch start
#iterations: 116
currently lose_sum: 94.93141317367554
time_elpased: 1.45
batch start
#iterations: 117
currently lose_sum: 94.47532242536545
time_elpased: 1.447
batch start
#iterations: 118
currently lose_sum: 94.56824815273285
time_elpased: 1.44
batch start
#iterations: 119
currently lose_sum: 95.1628732085228
time_elpased: 1.447
start validation test
0.597474226804
0.607432432432
0.555109601729
0.580093563478
0.597548604438
65.101
batch start
#iterations: 120
currently lose_sum: 94.68666332960129
time_elpased: 1.446
batch start
#iterations: 121
currently lose_sum: 94.80288034677505
time_elpased: 1.457
batch start
#iterations: 122
currently lose_sum: 94.58565205335617
time_elpased: 1.461
batch start
#iterations: 123
currently lose_sum: 94.52060568332672
time_elpased: 1.453
batch start
#iterations: 124
currently lose_sum: 94.30308097600937
time_elpased: 1.421
batch start
#iterations: 125
currently lose_sum: 94.4311044216156
time_elpased: 1.463
batch start
#iterations: 126
currently lose_sum: 94.34386438131332
time_elpased: 1.467
batch start
#iterations: 127
currently lose_sum: 94.7168927192688
time_elpased: 1.499
batch start
#iterations: 128
currently lose_sum: 94.57807838916779
time_elpased: 1.465
batch start
#iterations: 129
currently lose_sum: 94.45463043451309
time_elpased: 1.443
batch start
#iterations: 130
currently lose_sum: 94.12040686607361
time_elpased: 1.445
batch start
#iterations: 131
currently lose_sum: 94.50929951667786
time_elpased: 1.453
batch start
#iterations: 132
currently lose_sum: 94.18638628721237
time_elpased: 1.481
batch start
#iterations: 133
currently lose_sum: 94.30288141965866
time_elpased: 1.466
batch start
#iterations: 134
currently lose_sum: 94.05744671821594
time_elpased: 1.468
batch start
#iterations: 135
currently lose_sum: 94.27534806728363
time_elpased: 1.524
batch start
#iterations: 136
currently lose_sum: 94.29446744918823
time_elpased: 1.438
batch start
#iterations: 137
currently lose_sum: 94.32983738183975
time_elpased: 1.433
batch start
#iterations: 138
currently lose_sum: 94.08913254737854
time_elpased: 1.433
batch start
#iterations: 139
currently lose_sum: 94.03668242692947
time_elpased: 1.451
start validation test
0.596546391753
0.61170212766
0.532571781414
0.569400891236
0.596658709048
66.376
batch start
#iterations: 140
currently lose_sum: 93.96928137540817
time_elpased: 1.47
batch start
#iterations: 141
currently lose_sum: 94.00063359737396
time_elpased: 1.461
batch start
#iterations: 142
currently lose_sum: 94.20155894756317
time_elpased: 1.459
batch start
#iterations: 143
currently lose_sum: 93.48344022035599
time_elpased: 1.456
batch start
#iterations: 144
currently lose_sum: 93.93055862188339
time_elpased: 1.481
batch start
#iterations: 145
currently lose_sum: 94.00303310155869
time_elpased: 1.433
batch start
#iterations: 146
currently lose_sum: 93.62819296121597
time_elpased: 1.467
batch start
#iterations: 147
currently lose_sum: 94.21362543106079
time_elpased: 1.494
batch start
#iterations: 148
currently lose_sum: 93.95222294330597
time_elpased: 1.452
batch start
#iterations: 149
currently lose_sum: 93.68869704008102
time_elpased: 1.435
batch start
#iterations: 150
currently lose_sum: 93.74949008226395
time_elpased: 1.43
batch start
#iterations: 151
currently lose_sum: 94.13845819234848
time_elpased: 1.47
batch start
#iterations: 152
currently lose_sum: 93.77167963981628
time_elpased: 1.45
batch start
#iterations: 153
currently lose_sum: 93.7432450056076
time_elpased: 1.464
batch start
#iterations: 154
currently lose_sum: 93.36798989772797
time_elpased: 1.439
batch start
#iterations: 155
currently lose_sum: 93.71758157014847
time_elpased: 1.464
batch start
#iterations: 156
currently lose_sum: 93.80431926250458
time_elpased: 1.477
batch start
#iterations: 157
currently lose_sum: 93.3665941953659
time_elpased: 1.447
batch start
#iterations: 158
currently lose_sum: 93.54269695281982
time_elpased: 1.435
batch start
#iterations: 159
currently lose_sum: 93.76655322313309
time_elpased: 1.434
start validation test
0.593505154639
0.606515415939
0.536482453432
0.569353429445
0.593605266786
66.046
batch start
#iterations: 160
currently lose_sum: 93.74204164743423
time_elpased: 1.45
batch start
#iterations: 161
currently lose_sum: 93.68908470869064
time_elpased: 1.445
batch start
#iterations: 162
currently lose_sum: 93.55217427015305
time_elpased: 1.503
batch start
#iterations: 163
currently lose_sum: 93.25005835294724
time_elpased: 1.424
batch start
#iterations: 164
currently lose_sum: 93.46576112508774
time_elpased: 1.49
batch start
#iterations: 165
currently lose_sum: 93.6027946472168
time_elpased: 1.466
batch start
#iterations: 166
currently lose_sum: 93.24070304632187
time_elpased: 1.514
batch start
#iterations: 167
currently lose_sum: 93.54404413700104
time_elpased: 1.469
batch start
#iterations: 168
currently lose_sum: 93.49131470918655
time_elpased: 1.458
batch start
#iterations: 169
currently lose_sum: 92.86080944538116
time_elpased: 1.447
batch start
#iterations: 170
currently lose_sum: 93.06004065275192
time_elpased: 1.428
batch start
#iterations: 171
currently lose_sum: 93.60955345630646
time_elpased: 1.451
batch start
#iterations: 172
currently lose_sum: 93.2627164721489
time_elpased: 1.432
batch start
#iterations: 173
currently lose_sum: 92.6672973036766
time_elpased: 1.47
batch start
#iterations: 174
currently lose_sum: 93.49257129430771
time_elpased: 1.48
batch start
#iterations: 175
currently lose_sum: 93.0110040307045
time_elpased: 1.46
batch start
#iterations: 176
currently lose_sum: 93.14125508069992
time_elpased: 1.459
batch start
#iterations: 177
currently lose_sum: 93.06725597381592
time_elpased: 1.465
batch start
#iterations: 178
currently lose_sum: 92.90871554613113
time_elpased: 1.46
batch start
#iterations: 179
currently lose_sum: 93.02987152338028
time_elpased: 1.436
start validation test
0.596546391753
0.633286318759
0.462076772666
0.53430118403
0.596782473909
66.577
batch start
#iterations: 180
currently lose_sum: 93.1310288310051
time_elpased: 1.464
batch start
#iterations: 181
currently lose_sum: 93.24414175748825
time_elpased: 1.484
batch start
#iterations: 182
currently lose_sum: 92.75643479824066
time_elpased: 1.485
batch start
#iterations: 183
currently lose_sum: 93.0029684305191
time_elpased: 1.43
batch start
#iterations: 184
currently lose_sum: 93.59015703201294
time_elpased: 1.464
batch start
#iterations: 185
currently lose_sum: 92.8298032283783
time_elpased: 1.445
batch start
#iterations: 186
currently lose_sum: 92.90703302621841
time_elpased: 1.441
batch start
#iterations: 187
currently lose_sum: 92.76734292507172
time_elpased: 1.445
batch start
#iterations: 188
currently lose_sum: 92.6014956831932
time_elpased: 1.533
batch start
#iterations: 189
currently lose_sum: 93.03616154193878
time_elpased: 1.441
batch start
#iterations: 190
currently lose_sum: 92.4653068780899
time_elpased: 1.443
batch start
#iterations: 191
currently lose_sum: 92.59827214479446
time_elpased: 1.456
batch start
#iterations: 192
currently lose_sum: 92.73644667863846
time_elpased: 1.452
batch start
#iterations: 193
currently lose_sum: 92.77609950304031
time_elpased: 1.462
batch start
#iterations: 194
currently lose_sum: 92.75478583574295
time_elpased: 1.437
batch start
#iterations: 195
currently lose_sum: 92.89178943634033
time_elpased: 1.445
batch start
#iterations: 196
currently lose_sum: 92.52623307704926
time_elpased: 1.459
batch start
#iterations: 197
currently lose_sum: 92.90686243772507
time_elpased: 1.451
batch start
#iterations: 198
currently lose_sum: 92.35576367378235
time_elpased: 1.448
batch start
#iterations: 199
currently lose_sum: 92.35746788978577
time_elpased: 1.481
start validation test
0.593659793814
0.607856974829
0.531851394463
0.567319830946
0.593768307993
67.037
batch start
#iterations: 200
currently lose_sum: 92.62946939468384
time_elpased: 1.5
batch start
#iterations: 201
currently lose_sum: 92.49287837743759
time_elpased: 1.454
batch start
#iterations: 202
currently lose_sum: 92.15057176351547
time_elpased: 1.472
batch start
#iterations: 203
currently lose_sum: 92.20675909519196
time_elpased: 1.461
batch start
#iterations: 204
currently lose_sum: 92.36964040994644
time_elpased: 1.461
batch start
#iterations: 205
currently lose_sum: 91.95824944972992
time_elpased: 1.456
batch start
#iterations: 206
currently lose_sum: 92.27732521295547
time_elpased: 1.448
batch start
#iterations: 207
currently lose_sum: 92.86776971817017
time_elpased: 1.509
batch start
#iterations: 208
currently lose_sum: 92.47520989179611
time_elpased: 1.467
batch start
#iterations: 209
currently lose_sum: 91.66195291280746
time_elpased: 1.519
batch start
#iterations: 210
currently lose_sum: 92.29776793718338
time_elpased: 1.493
batch start
#iterations: 211
currently lose_sum: 92.15951043367386
time_elpased: 1.488
batch start
#iterations: 212
currently lose_sum: 91.94141137599945
time_elpased: 1.467
batch start
#iterations: 213
currently lose_sum: 92.15246230363846
time_elpased: 1.459
batch start
#iterations: 214
currently lose_sum: 91.51084530353546
time_elpased: 1.435
batch start
#iterations: 215
currently lose_sum: 91.85862272977829
time_elpased: 1.454
batch start
#iterations: 216
currently lose_sum: 92.215864777565
time_elpased: 1.448
batch start
#iterations: 217
currently lose_sum: 92.29665470123291
time_elpased: 1.509
batch start
#iterations: 218
currently lose_sum: 91.96378016471863
time_elpased: 1.464
batch start
#iterations: 219
currently lose_sum: 91.51320821046829
time_elpased: 1.447
start validation test
0.589948453608
0.621216290589
0.464649583205
0.53164556962
0.590168435101
69.053
batch start
#iterations: 220
currently lose_sum: 91.87305015325546
time_elpased: 1.489
batch start
#iterations: 221
currently lose_sum: 92.02726876735687
time_elpased: 1.45
batch start
#iterations: 222
currently lose_sum: 91.65497434139252
time_elpased: 1.471
batch start
#iterations: 223
currently lose_sum: 92.1205785870552
time_elpased: 1.452
batch start
#iterations: 224
currently lose_sum: 92.02808612585068
time_elpased: 1.464
batch start
#iterations: 225
currently lose_sum: 91.61563611030579
time_elpased: 1.473
batch start
#iterations: 226
currently lose_sum: 91.44286274909973
time_elpased: 1.467
batch start
#iterations: 227
currently lose_sum: 91.48729515075684
time_elpased: 1.453
batch start
#iterations: 228
currently lose_sum: 91.96493762731552
time_elpased: 1.451
batch start
#iterations: 229
currently lose_sum: 91.81854200363159
time_elpased: 1.461
batch start
#iterations: 230
currently lose_sum: 92.2037822008133
time_elpased: 1.449
batch start
#iterations: 231
currently lose_sum: 91.76124358177185
time_elpased: 1.43
batch start
#iterations: 232
currently lose_sum: 91.5676742196083
time_elpased: 1.444
batch start
#iterations: 233
currently lose_sum: 91.60584831237793
time_elpased: 1.429
batch start
#iterations: 234
currently lose_sum: 91.22412383556366
time_elpased: 1.495
batch start
#iterations: 235
currently lose_sum: 92.31998407840729
time_elpased: 1.444
batch start
#iterations: 236
currently lose_sum: 91.28415495157242
time_elpased: 1.471
batch start
#iterations: 237
currently lose_sum: 91.46713548898697
time_elpased: 1.427
batch start
#iterations: 238
currently lose_sum: 91.68167316913605
time_elpased: 1.452
batch start
#iterations: 239
currently lose_sum: 90.93027013540268
time_elpased: 1.437
start validation test
0.59881443299
0.62151294295
0.509004836884
0.559660537482
0.598972107588
67.925
batch start
#iterations: 240
currently lose_sum: 91.67279839515686
time_elpased: 1.449
batch start
#iterations: 241
currently lose_sum: 91.46824425458908
time_elpased: 1.463
batch start
#iterations: 242
currently lose_sum: 91.2489184141159
time_elpased: 1.456
batch start
#iterations: 243
currently lose_sum: 91.751085460186
time_elpased: 1.443
batch start
#iterations: 244
currently lose_sum: 90.9562856554985
time_elpased: 1.468
batch start
#iterations: 245
currently lose_sum: 91.008553981781
time_elpased: 1.455
batch start
#iterations: 246
currently lose_sum: 91.59755730628967
time_elpased: 1.452
batch start
#iterations: 247
currently lose_sum: 91.07779443264008
time_elpased: 1.443
batch start
#iterations: 248
currently lose_sum: 91.45155447721481
time_elpased: 1.444
batch start
#iterations: 249
currently lose_sum: 91.01227569580078
time_elpased: 1.471
batch start
#iterations: 250
currently lose_sum: 90.40177029371262
time_elpased: 1.456
batch start
#iterations: 251
currently lose_sum: 90.68420726060867
time_elpased: 1.468
batch start
#iterations: 252
currently lose_sum: 90.51761668920517
time_elpased: 1.47
batch start
#iterations: 253
currently lose_sum: 91.60018634796143
time_elpased: 1.455
batch start
#iterations: 254
currently lose_sum: 91.03661960363388
time_elpased: 1.452
batch start
#iterations: 255
currently lose_sum: 91.01056897640228
time_elpased: 1.462
batch start
#iterations: 256
currently lose_sum: 91.0018515586853
time_elpased: 1.441
batch start
#iterations: 257
currently lose_sum: 91.03618413209915
time_elpased: 1.507
batch start
#iterations: 258
currently lose_sum: 91.04548639059067
time_elpased: 1.455
batch start
#iterations: 259
currently lose_sum: 91.1652734875679
time_elpased: 1.46
start validation test
0.585567010309
0.604252144722
0.500154368632
0.547297297297
0.585716965376
71.270
batch start
#iterations: 260
currently lose_sum: 90.72274476289749
time_elpased: 1.463
batch start
#iterations: 261
currently lose_sum: 90.35097628831863
time_elpased: 1.505
batch start
#iterations: 262
currently lose_sum: 91.15529930591583
time_elpased: 1.531
batch start
#iterations: 263
currently lose_sum: 90.68805402517319
time_elpased: 1.539
batch start
#iterations: 264
currently lose_sum: 90.97372591495514
time_elpased: 1.482
batch start
#iterations: 265
currently lose_sum: 91.26600486040115
time_elpased: 1.441
batch start
#iterations: 266
currently lose_sum: 90.44515061378479
time_elpased: 1.436
batch start
#iterations: 267
currently lose_sum: 90.36297196149826
time_elpased: 1.463
batch start
#iterations: 268
currently lose_sum: 91.06042838096619
time_elpased: 1.454
batch start
#iterations: 269
currently lose_sum: 90.76932054758072
time_elpased: 1.471
batch start
#iterations: 270
currently lose_sum: 90.55746549367905
time_elpased: 1.453
batch start
#iterations: 271
currently lose_sum: 90.51668637990952
time_elpased: 1.452
batch start
#iterations: 272
currently lose_sum: 91.08439832925797
time_elpased: 1.441
batch start
#iterations: 273
currently lose_sum: 90.46068859100342
time_elpased: 1.44
batch start
#iterations: 274
currently lose_sum: 90.48075705766678
time_elpased: 1.435
batch start
#iterations: 275
currently lose_sum: 90.61535036563873
time_elpased: 1.463
batch start
#iterations: 276
currently lose_sum: 90.38141620159149
time_elpased: 1.435
batch start
#iterations: 277
currently lose_sum: 90.53021371364594
time_elpased: 1.46
batch start
#iterations: 278
currently lose_sum: 90.34368145465851
time_elpased: 1.463
batch start
#iterations: 279
currently lose_sum: 90.22169971466064
time_elpased: 1.463
start validation test
0.586958762887
0.602799227799
0.51415045796
0.554956956401
0.587086589096
70.049
batch start
#iterations: 280
currently lose_sum: 90.5082157254219
time_elpased: 1.461
batch start
#iterations: 281
currently lose_sum: 90.27183765172958
time_elpased: 1.439
batch start
#iterations: 282
currently lose_sum: 90.36508238315582
time_elpased: 1.444
batch start
#iterations: 283
currently lose_sum: 90.32525664567947
time_elpased: 1.431
batch start
#iterations: 284
currently lose_sum: 90.82237261533737
time_elpased: 1.522
batch start
#iterations: 285
currently lose_sum: 90.28597271442413
time_elpased: 1.482
batch start
#iterations: 286
currently lose_sum: 90.5414468050003
time_elpased: 1.447
batch start
#iterations: 287
currently lose_sum: 90.43322175741196
time_elpased: 1.467
batch start
#iterations: 288
currently lose_sum: 90.11924463510513
time_elpased: 1.478
batch start
#iterations: 289
currently lose_sum: 90.02358496189117
time_elpased: 1.467
batch start
#iterations: 290
currently lose_sum: 89.78698486089706
time_elpased: 1.466
batch start
#iterations: 291
currently lose_sum: 89.98031681776047
time_elpased: 1.465
batch start
#iterations: 292
currently lose_sum: 89.9708833694458
time_elpased: 1.448
batch start
#iterations: 293
currently lose_sum: 90.74268239736557
time_elpased: 1.463
batch start
#iterations: 294
currently lose_sum: 89.42190158367157
time_elpased: 1.441
batch start
#iterations: 295
currently lose_sum: 90.09484577178955
time_elpased: 1.433
batch start
#iterations: 296
currently lose_sum: 90.32051062583923
time_elpased: 1.444
batch start
#iterations: 297
currently lose_sum: 90.11453378200531
time_elpased: 1.444
batch start
#iterations: 298
currently lose_sum: 89.76710587739944
time_elpased: 1.457
batch start
#iterations: 299
currently lose_sum: 89.7448256611824
time_elpased: 1.436
start validation test
0.59206185567
0.609791742784
0.515282494597
0.558567603748
0.592196653681
70.115
batch start
#iterations: 300
currently lose_sum: 89.82952457666397
time_elpased: 1.449
batch start
#iterations: 301
currently lose_sum: 89.67945235967636
time_elpased: 1.47
batch start
#iterations: 302
currently lose_sum: 90.30830323696136
time_elpased: 1.424
batch start
#iterations: 303
currently lose_sum: 89.72651296854019
time_elpased: 1.457
batch start
#iterations: 304
currently lose_sum: 89.8487543463707
time_elpased: 1.493
batch start
#iterations: 305
currently lose_sum: 89.60200220346451
time_elpased: 1.449
batch start
#iterations: 306
currently lose_sum: 89.83398067951202
time_elpased: 1.458
batch start
#iterations: 307
currently lose_sum: 89.85664540529251
time_elpased: 1.462
batch start
#iterations: 308
currently lose_sum: 89.86816936731339
time_elpased: 1.446
batch start
#iterations: 309
currently lose_sum: 89.58779072761536
time_elpased: 1.473
batch start
#iterations: 310
currently lose_sum: 89.37915313243866
time_elpased: 1.439
batch start
#iterations: 311
currently lose_sum: 89.46792793273926
time_elpased: 1.479
batch start
#iterations: 312
currently lose_sum: 89.36502778530121
time_elpased: 1.505
batch start
#iterations: 313
currently lose_sum: 89.51289910078049
time_elpased: 1.488
batch start
#iterations: 314
currently lose_sum: 89.65916043519974
time_elpased: 1.506
batch start
#iterations: 315
currently lose_sum: 88.61337316036224
time_elpased: 1.484
batch start
#iterations: 316
currently lose_sum: 88.83045089244843
time_elpased: 1.452
batch start
#iterations: 317
currently lose_sum: 89.24693661928177
time_elpased: 1.449
batch start
#iterations: 318
currently lose_sum: 89.70224422216415
time_elpased: 1.497
batch start
#iterations: 319
currently lose_sum: 89.0517486333847
time_elpased: 1.467
start validation test
0.591443298969
0.605192059204
0.530204795719
0.565222161273
0.591550812607
70.382
batch start
#iterations: 320
currently lose_sum: 89.2832887172699
time_elpased: 1.449
batch start
#iterations: 321
currently lose_sum: 89.05924880504608
time_elpased: 1.444
batch start
#iterations: 322
currently lose_sum: 89.34695017337799
time_elpased: 1.479
batch start
#iterations: 323
currently lose_sum: 88.64468944072723
time_elpased: 1.482
batch start
#iterations: 324
currently lose_sum: 89.30576437711716
time_elpased: 1.485
batch start
#iterations: 325
currently lose_sum: 89.48914712667465
time_elpased: 1.468
batch start
#iterations: 326
currently lose_sum: 89.59655445814133
time_elpased: 1.448
batch start
#iterations: 327
currently lose_sum: 89.01175653934479
time_elpased: 1.453
batch start
#iterations: 328
currently lose_sum: 88.99907022714615
time_elpased: 1.447
batch start
#iterations: 329
currently lose_sum: 89.06356370449066
time_elpased: 1.437
batch start
#iterations: 330
currently lose_sum: 89.17499285936356
time_elpased: 1.442
batch start
#iterations: 331
currently lose_sum: 88.9496631026268
time_elpased: 1.478
batch start
#iterations: 332
currently lose_sum: 89.59678614139557
time_elpased: 1.467
batch start
#iterations: 333
currently lose_sum: 88.70760542154312
time_elpased: 1.445
batch start
#iterations: 334
currently lose_sum: 88.74124026298523
time_elpased: 1.458
batch start
#iterations: 335
currently lose_sum: 88.81007361412048
time_elpased: 1.459
batch start
#iterations: 336
currently lose_sum: 89.60143542289734
time_elpased: 1.445
batch start
#iterations: 337
currently lose_sum: 89.00144785642624
time_elpased: 1.438
batch start
#iterations: 338
currently lose_sum: 88.47681564092636
time_elpased: 1.429
batch start
#iterations: 339
currently lose_sum: 89.02334779500961
time_elpased: 1.464
start validation test
0.59087628866
0.621884415229
0.467325306164
0.533638874199
0.591093201466
74.143
batch start
#iterations: 340
currently lose_sum: 89.10723185539246
time_elpased: 1.458
batch start
#iterations: 341
currently lose_sum: 89.69699358940125
time_elpased: 1.41
batch start
#iterations: 342
currently lose_sum: 88.98977172374725
time_elpased: 1.451
batch start
#iterations: 343
currently lose_sum: 89.16584986448288
time_elpased: 1.453
batch start
#iterations: 344
currently lose_sum: 88.98452538251877
time_elpased: 1.434
batch start
#iterations: 345
currently lose_sum: 88.47607135772705
time_elpased: 1.445
batch start
#iterations: 346
currently lose_sum: 88.9083741903305
time_elpased: 1.44
batch start
#iterations: 347
currently lose_sum: 88.62260156869888
time_elpased: 1.503
batch start
#iterations: 348
currently lose_sum: 88.51897352933884
time_elpased: 1.459
batch start
#iterations: 349
currently lose_sum: 88.84102219343185
time_elpased: 1.54
batch start
#iterations: 350
currently lose_sum: 88.21581029891968
time_elpased: 1.465
batch start
#iterations: 351
currently lose_sum: 88.63659393787384
time_elpased: 1.453
batch start
#iterations: 352
currently lose_sum: 88.2141825556755
time_elpased: 1.468
batch start
#iterations: 353
currently lose_sum: 88.62164896726608
time_elpased: 1.453
batch start
#iterations: 354
currently lose_sum: 88.07216483354568
time_elpased: 1.442
batch start
#iterations: 355
currently lose_sum: 88.25625276565552
time_elpased: 1.437
batch start
#iterations: 356
currently lose_sum: 88.3626856803894
time_elpased: 1.465
batch start
#iterations: 357
currently lose_sum: 88.71854168176651
time_elpased: 1.451
batch start
#iterations: 358
currently lose_sum: 87.86873137950897
time_elpased: 1.456
batch start
#iterations: 359
currently lose_sum: 88.60769164562225
time_elpased: 1.445
start validation test
0.58675257732
0.607431749242
0.49459709787
0.545237960179
0.586914370478
74.958
batch start
#iterations: 360
currently lose_sum: 88.1978303194046
time_elpased: 1.465
batch start
#iterations: 361
currently lose_sum: 88.48523652553558
time_elpased: 1.433
batch start
#iterations: 362
currently lose_sum: 88.07161366939545
time_elpased: 1.483
batch start
#iterations: 363
currently lose_sum: 88.40681999921799
time_elpased: 1.451
batch start
#iterations: 364
currently lose_sum: 88.0305106639862
time_elpased: 1.477
batch start
#iterations: 365
currently lose_sum: 88.39748084545135
time_elpased: 1.47
batch start
#iterations: 366
currently lose_sum: 88.23943990468979
time_elpased: 1.478
batch start
#iterations: 367
currently lose_sum: 88.7057512998581
time_elpased: 1.429
batch start
#iterations: 368
currently lose_sum: 88.00538849830627
time_elpased: 1.474
batch start
#iterations: 369
currently lose_sum: 88.19691151380539
time_elpased: 1.426
batch start
#iterations: 370
currently lose_sum: 87.73360639810562
time_elpased: 1.457
batch start
#iterations: 371
currently lose_sum: 87.97642379999161
time_elpased: 1.456
batch start
#iterations: 372
currently lose_sum: 87.67537873983383
time_elpased: 1.441
batch start
#iterations: 373
currently lose_sum: 87.99427199363708
time_elpased: 1.452
batch start
#iterations: 374
currently lose_sum: 88.44238311052322
time_elpased: 1.451
batch start
#iterations: 375
currently lose_sum: 87.89716023206711
time_elpased: 1.443
batch start
#iterations: 376
currently lose_sum: 88.2275076508522
time_elpased: 1.429
batch start
#iterations: 377
currently lose_sum: 87.92654985189438
time_elpased: 1.447
batch start
#iterations: 378
currently lose_sum: 87.92753130197525
time_elpased: 1.476
batch start
#iterations: 379
currently lose_sum: 87.86373543739319
time_elpased: 1.453
start validation test
0.578402061856
0.599896076903
0.475249562622
0.530347401665
0.578583161978
76.857
batch start
#iterations: 380
currently lose_sum: 87.74112844467163
time_elpased: 1.425
batch start
#iterations: 381
currently lose_sum: 88.23454451560974
time_elpased: 1.451
batch start
#iterations: 382
currently lose_sum: 87.52744299173355
time_elpased: 1.456
batch start
#iterations: 383
currently lose_sum: 87.78033035993576
time_elpased: 1.437
batch start
#iterations: 384
currently lose_sum: 86.86457830667496
time_elpased: 1.47
batch start
#iterations: 385
currently lose_sum: 88.30041098594666
time_elpased: 1.441
batch start
#iterations: 386
currently lose_sum: 87.58289778232574
time_elpased: 1.447
batch start
#iterations: 387
currently lose_sum: 87.48472356796265
time_elpased: 1.454
batch start
#iterations: 388
currently lose_sum: 87.45121932029724
time_elpased: 1.458
batch start
#iterations: 389
currently lose_sum: 87.46178185939789
time_elpased: 1.519
batch start
#iterations: 390
currently lose_sum: 87.11834841966629
time_elpased: 1.444
batch start
#iterations: 391
currently lose_sum: 87.55962592363358
time_elpased: 1.455
batch start
#iterations: 392
currently lose_sum: 87.8742168545723
time_elpased: 1.46
batch start
#iterations: 393
currently lose_sum: 87.27314960956573
time_elpased: 1.462
batch start
#iterations: 394
currently lose_sum: 87.06313920021057
time_elpased: 1.444
batch start
#iterations: 395
currently lose_sum: 87.64035499095917
time_elpased: 1.447
batch start
#iterations: 396
currently lose_sum: 87.57357811927795
time_elpased: 1.473
batch start
#iterations: 397
currently lose_sum: 87.8092713356018
time_elpased: 1.464
batch start
#iterations: 398
currently lose_sum: 87.26339948177338
time_elpased: 1.453
batch start
#iterations: 399
currently lose_sum: 87.57024890184402
time_elpased: 1.454
start validation test
0.589793814433
0.613821664294
0.488113615313
0.543797294199
0.589972329706
76.866
acc: 0.612
pre: 0.623
rec: 0.571
F1: 0.596
auc: 0.612
