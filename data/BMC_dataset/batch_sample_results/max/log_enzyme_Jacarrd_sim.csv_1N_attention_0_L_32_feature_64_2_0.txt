start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.4031971693039
time_elpased: 2.275
batch start
#iterations: 1
currently lose_sum: 100.05822193622589
time_elpased: 2.253
batch start
#iterations: 2
currently lose_sum: 99.96180152893066
time_elpased: 2.159
batch start
#iterations: 3
currently lose_sum: 99.96837240457535
time_elpased: 2.082
batch start
#iterations: 4
currently lose_sum: 99.80204325914383
time_elpased: 2.166
batch start
#iterations: 5
currently lose_sum: 99.80558228492737
time_elpased: 2.128
batch start
#iterations: 6
currently lose_sum: 99.59827011823654
time_elpased: 2.08
batch start
#iterations: 7
currently lose_sum: 99.59613621234894
time_elpased: 2.093
batch start
#iterations: 8
currently lose_sum: 99.30612391233444
time_elpased: 2.098
batch start
#iterations: 9
currently lose_sum: 99.28792870044708
time_elpased: 2.234
batch start
#iterations: 10
currently lose_sum: 99.26520001888275
time_elpased: 2.146
batch start
#iterations: 11
currently lose_sum: 99.25762397050858
time_elpased: 2.185
batch start
#iterations: 12
currently lose_sum: 99.14821791648865
time_elpased: 2.148
batch start
#iterations: 13
currently lose_sum: 99.03208488225937
time_elpased: 2.124
batch start
#iterations: 14
currently lose_sum: 98.841961145401
time_elpased: 2.131
batch start
#iterations: 15
currently lose_sum: 98.89080375432968
time_elpased: 2.118
batch start
#iterations: 16
currently lose_sum: 98.95135289430618
time_elpased: 2.12
batch start
#iterations: 17
currently lose_sum: 98.88883244991302
time_elpased: 2.134
batch start
#iterations: 18
currently lose_sum: 98.79120320081711
time_elpased: 2.04
batch start
#iterations: 19
currently lose_sum: 98.71513611078262
time_elpased: 2.091
start validation test
0.601546391753
0.604656062362
0.590717299578
0.597605413847
0.601565403894
64.717
batch start
#iterations: 20
currently lose_sum: 98.65283924341202
time_elpased: 2.249
batch start
#iterations: 21
currently lose_sum: 98.52906864881516
time_elpased: 2.246
batch start
#iterations: 22
currently lose_sum: 98.31078100204468
time_elpased: 2.153
batch start
#iterations: 23
currently lose_sum: 98.3789409995079
time_elpased: 2.128
batch start
#iterations: 24
currently lose_sum: 98.24693989753723
time_elpased: 2.116
batch start
#iterations: 25
currently lose_sum: 98.18524158000946
time_elpased: 2.113
batch start
#iterations: 26
currently lose_sum: 98.03645485639572
time_elpased: 2.076
batch start
#iterations: 27
currently lose_sum: 98.1505007147789
time_elpased: 2.117
batch start
#iterations: 28
currently lose_sum: 98.0876716375351
time_elpased: 2.066
batch start
#iterations: 29
currently lose_sum: 97.87535899877548
time_elpased: 2.123
batch start
#iterations: 30
currently lose_sum: 97.98059386014938
time_elpased: 2.181
batch start
#iterations: 31
currently lose_sum: 97.92235487699509
time_elpased: 2.33
batch start
#iterations: 32
currently lose_sum: 97.59071487188339
time_elpased: 2.176
batch start
#iterations: 33
currently lose_sum: 97.61231106519699
time_elpased: 2.138
batch start
#iterations: 34
currently lose_sum: 97.49391132593155
time_elpased: 2.155
batch start
#iterations: 35
currently lose_sum: 97.63522279262543
time_elpased: 2.211
batch start
#iterations: 36
currently lose_sum: 97.59509176015854
time_elpased: 2.271
batch start
#iterations: 37
currently lose_sum: 97.39823055267334
time_elpased: 2.16
batch start
#iterations: 38
currently lose_sum: 97.54470938444138
time_elpased: 2.148
batch start
#iterations: 39
currently lose_sum: 97.57646596431732
time_elpased: 2.143
start validation test
0.608865979381
0.610003100134
0.607492024287
0.608744972672
0.608868391572
64.231
batch start
#iterations: 40
currently lose_sum: 97.38714921474457
time_elpased: 2.114
batch start
#iterations: 41
currently lose_sum: 97.31472051143646
time_elpased: 2.063
batch start
#iterations: 42
currently lose_sum: 97.34965580701828
time_elpased: 2.091
batch start
#iterations: 43
currently lose_sum: 96.8776827454567
time_elpased: 2.177
batch start
#iterations: 44
currently lose_sum: 97.15151381492615
time_elpased: 2.06
batch start
#iterations: 45
currently lose_sum: 97.20284366607666
time_elpased: 2.185
batch start
#iterations: 46
currently lose_sum: 96.75493025779724
time_elpased: 2.179
batch start
#iterations: 47
currently lose_sum: 96.96414172649384
time_elpased: 2.1
batch start
#iterations: 48
currently lose_sum: 96.99950814247131
time_elpased: 2.116
batch start
#iterations: 49
currently lose_sum: 97.22635805606842
time_elpased: 2.11
batch start
#iterations: 50
currently lose_sum: 96.81364721059799
time_elpased: 2.282
batch start
#iterations: 51
currently lose_sum: 96.66648364067078
time_elpased: 2.1
batch start
#iterations: 52
currently lose_sum: 96.65100711584091
time_elpased: 2.131
batch start
#iterations: 53
currently lose_sum: 96.98688983917236
time_elpased: 2.086
batch start
#iterations: 54
currently lose_sum: 96.56973314285278
time_elpased: 2.11
batch start
#iterations: 55
currently lose_sum: 96.60588943958282
time_elpased: 2.013
batch start
#iterations: 56
currently lose_sum: 96.36317825317383
time_elpased: 2.102
batch start
#iterations: 57
currently lose_sum: 96.2552547454834
time_elpased: 2.069
batch start
#iterations: 58
currently lose_sum: 96.36912763118744
time_elpased: 2.121
batch start
#iterations: 59
currently lose_sum: 96.28426921367645
time_elpased: 2.057
start validation test
0.591494845361
0.612196343601
0.503138828857
0.5523357623
0.591649967976
64.804
batch start
#iterations: 60
currently lose_sum: 96.13521808385849
time_elpased: 2.145
batch start
#iterations: 61
currently lose_sum: 96.59373986721039
time_elpased: 2.11
batch start
#iterations: 62
currently lose_sum: 96.35471260547638
time_elpased: 2.08
batch start
#iterations: 63
currently lose_sum: 96.15532010793686
time_elpased: 2.096
batch start
#iterations: 64
currently lose_sum: 96.04602980613708
time_elpased: 2.107
batch start
#iterations: 65
currently lose_sum: 95.72210484743118
time_elpased: 2.124
batch start
#iterations: 66
currently lose_sum: 95.42604446411133
time_elpased: 2.029
batch start
#iterations: 67
currently lose_sum: 95.65879368782043
time_elpased: 2.284
batch start
#iterations: 68
currently lose_sum: 95.98627978563309
time_elpased: 2.192
batch start
#iterations: 69
currently lose_sum: 95.46802097558975
time_elpased: 2.154
batch start
#iterations: 70
currently lose_sum: 95.52926224470139
time_elpased: 2.145
batch start
#iterations: 71
currently lose_sum: 95.56898337602615
time_elpased: 2.227
batch start
#iterations: 72
currently lose_sum: 94.99200367927551
time_elpased: 2.104
batch start
#iterations: 73
currently lose_sum: 95.49946284294128
time_elpased: 2.17
batch start
#iterations: 74
currently lose_sum: 95.1140649318695
time_elpased: 2.141
batch start
#iterations: 75
currently lose_sum: 95.08219867944717
time_elpased: 2.111
batch start
#iterations: 76
currently lose_sum: 94.98600208759308
time_elpased: 2.117
batch start
#iterations: 77
currently lose_sum: 94.97990024089813
time_elpased: 2.065
batch start
#iterations: 78
currently lose_sum: 95.0800970196724
time_elpased: 1.972
batch start
#iterations: 79
currently lose_sum: 95.0065889954567
time_elpased: 2.137
start validation test
0.592680412371
0.598716414663
0.566429968097
0.582125859334
0.592726499075
64.839
batch start
#iterations: 80
currently lose_sum: 95.4757120013237
time_elpased: 2.167
batch start
#iterations: 81
currently lose_sum: 95.2659028172493
time_elpased: 2.136
batch start
#iterations: 82
currently lose_sum: 95.17348492145538
time_elpased: 2.119
batch start
#iterations: 83
currently lose_sum: 94.90076887607574
time_elpased: 2.143
batch start
#iterations: 84
currently lose_sum: 94.96387678384781
time_elpased: 2.051
batch start
#iterations: 85
currently lose_sum: 94.40642142295837
time_elpased: 1.997
batch start
#iterations: 86
currently lose_sum: 94.81302773952484
time_elpased: 2.06
batch start
#iterations: 87
currently lose_sum: 94.87441635131836
time_elpased: 2.117
batch start
#iterations: 88
currently lose_sum: 94.71798092126846
time_elpased: 2.346
batch start
#iterations: 89
currently lose_sum: 94.52746713161469
time_elpased: 2.127
batch start
#iterations: 90
currently lose_sum: 94.40374171733856
time_elpased: 2.09
batch start
#iterations: 91
currently lose_sum: 94.16308504343033
time_elpased: 2.255
batch start
#iterations: 92
currently lose_sum: 94.46363830566406
time_elpased: 2.083
batch start
#iterations: 93
currently lose_sum: 94.1281686425209
time_elpased: 2.1
batch start
#iterations: 94
currently lose_sum: 94.2842538356781
time_elpased: 2.215
batch start
#iterations: 95
currently lose_sum: 93.99735289812088
time_elpased: 2.151
batch start
#iterations: 96
currently lose_sum: 94.33129322528839
time_elpased: 2.157
batch start
#iterations: 97
currently lose_sum: 94.32722169160843
time_elpased: 2.13
batch start
#iterations: 98
currently lose_sum: 94.17308706045151
time_elpased: 2.142
batch start
#iterations: 99
currently lose_sum: 93.52258098125458
time_elpased: 2.131
start validation test
0.594587628866
0.621618071973
0.487084491098
0.546188910046
0.594776367206
65.208
batch start
#iterations: 100
currently lose_sum: 93.97783172130585
time_elpased: 2.123
batch start
#iterations: 101
currently lose_sum: 93.91388684511185
time_elpased: 2.09
batch start
#iterations: 102
currently lose_sum: 94.17867642641068
time_elpased: 2.024
batch start
#iterations: 103
currently lose_sum: 93.49022310972214
time_elpased: 2.082
batch start
#iterations: 104
currently lose_sum: 93.4202641248703
time_elpased: 2.043
batch start
#iterations: 105
currently lose_sum: 93.70586723089218
time_elpased: 2.169
batch start
#iterations: 106
currently lose_sum: 94.10027557611465
time_elpased: 2.117
batch start
#iterations: 107
currently lose_sum: 93.25375485420227
time_elpased: 2.132
batch start
#iterations: 108
currently lose_sum: 93.01528578996658
time_elpased: 2.097
batch start
#iterations: 109
currently lose_sum: 93.56776106357574
time_elpased: 2.143
batch start
#iterations: 110
currently lose_sum: 93.42343193292618
time_elpased: 2.062
batch start
#iterations: 111
currently lose_sum: 93.31825369596481
time_elpased: 2.108
batch start
#iterations: 112
currently lose_sum: 93.2288174033165
time_elpased: 2.16
batch start
#iterations: 113
currently lose_sum: 93.19514310359955
time_elpased: 2.122
batch start
#iterations: 114
currently lose_sum: 92.86076176166534
time_elpased: 2.105
batch start
#iterations: 115
currently lose_sum: 92.97738510370255
time_elpased: 2.136
batch start
#iterations: 116
currently lose_sum: 93.07450735569
time_elpased: 2.087
batch start
#iterations: 117
currently lose_sum: 93.19706451892853
time_elpased: 2.07
batch start
#iterations: 118
currently lose_sum: 92.83321636915207
time_elpased: 2.134
batch start
#iterations: 119
currently lose_sum: 92.92879146337509
time_elpased: 2.233
start validation test
0.584226804124
0.633381806431
0.403416692395
0.492895762605
0.584544244163
66.667
batch start
#iterations: 120
currently lose_sum: 93.04688477516174
time_elpased: 2.147
batch start
#iterations: 121
currently lose_sum: 92.45840591192245
time_elpased: 2.144
batch start
#iterations: 122
currently lose_sum: 92.85757100582123
time_elpased: 2.034
batch start
#iterations: 123
currently lose_sum: 92.70186710357666
time_elpased: 2.078
batch start
#iterations: 124
currently lose_sum: 92.83552175760269
time_elpased: 2.093
batch start
#iterations: 125
currently lose_sum: 92.53871238231659
time_elpased: 2.17
batch start
#iterations: 126
currently lose_sum: 92.08970594406128
time_elpased: 2.164
batch start
#iterations: 127
currently lose_sum: 92.73210763931274
time_elpased: 2.109
batch start
#iterations: 128
currently lose_sum: 92.69886749982834
time_elpased: 2.026
batch start
#iterations: 129
currently lose_sum: 92.62495940923691
time_elpased: 2.06
batch start
#iterations: 130
currently lose_sum: 92.2953330874443
time_elpased: 2.074
batch start
#iterations: 131
currently lose_sum: 92.49146395921707
time_elpased: 2.243
batch start
#iterations: 132
currently lose_sum: 92.52074670791626
time_elpased: 2.182
batch start
#iterations: 133
currently lose_sum: 92.43907248973846
time_elpased: 2.367
batch start
#iterations: 134
currently lose_sum: 91.60613214969635
time_elpased: 2.094
batch start
#iterations: 135
currently lose_sum: 92.7219557762146
time_elpased: 2.102
batch start
#iterations: 136
currently lose_sum: 92.26066565513611
time_elpased: 2.111
batch start
#iterations: 137
currently lose_sum: 92.12416213750839
time_elpased: 2.118
batch start
#iterations: 138
currently lose_sum: 91.7032899260521
time_elpased: 2.041
batch start
#iterations: 139
currently lose_sum: 91.90898072719574
time_elpased: 2.087
start validation test
0.589587628866
0.610781466986
0.497890295359
0.548588275315
0.589748617678
66.106
batch start
#iterations: 140
currently lose_sum: 92.42571532726288
time_elpased: 2.127
batch start
#iterations: 141
currently lose_sum: 92.15734910964966
time_elpased: 2.148
batch start
#iterations: 142
currently lose_sum: 92.17798954248428
time_elpased: 2.172
batch start
#iterations: 143
currently lose_sum: 91.71141225099564
time_elpased: 2.178
batch start
#iterations: 144
currently lose_sum: 91.4947960972786
time_elpased: 2.14
batch start
#iterations: 145
currently lose_sum: 91.67674905061722
time_elpased: 2.127
batch start
#iterations: 146
currently lose_sum: 91.50334072113037
time_elpased: 2.135
batch start
#iterations: 147
currently lose_sum: 91.87297308444977
time_elpased: 2.079
batch start
#iterations: 148
currently lose_sum: 91.70984870195389
time_elpased: 2.076
batch start
#iterations: 149
currently lose_sum: 91.64895594120026
time_elpased: 2.21
batch start
#iterations: 150
currently lose_sum: 91.97576034069061
time_elpased: 2.108
batch start
#iterations: 151
currently lose_sum: 91.24047183990479
time_elpased: 2.17
batch start
#iterations: 152
currently lose_sum: 91.60224294662476
time_elpased: 2.1
batch start
#iterations: 153
currently lose_sum: 91.28831762075424
time_elpased: 2.095
batch start
#iterations: 154
currently lose_sum: 91.67726057767868
time_elpased: 2.112
batch start
#iterations: 155
currently lose_sum: 91.20615488290787
time_elpased: 2.093
batch start
#iterations: 156
currently lose_sum: 91.47335058450699
time_elpased: 2.132
batch start
#iterations: 157
currently lose_sum: 91.4420737028122
time_elpased: 2.116
batch start
#iterations: 158
currently lose_sum: 91.3059144616127
time_elpased: 2.198
batch start
#iterations: 159
currently lose_sum: 91.1269581913948
time_elpased: 2.162
start validation test
0.583762886598
0.618744576222
0.440259339302
0.514460946425
0.584014829209
68.066
batch start
#iterations: 160
currently lose_sum: 91.30542171001434
time_elpased: 2.311
batch start
#iterations: 161
currently lose_sum: 90.91092640161514
time_elpased: 2.141
batch start
#iterations: 162
currently lose_sum: 91.08410584926605
time_elpased: 2.084
batch start
#iterations: 163
currently lose_sum: 91.15023070573807
time_elpased: 2.171
batch start
#iterations: 164
currently lose_sum: 91.32460480928421
time_elpased: 2.154
batch start
#iterations: 165
currently lose_sum: 91.23603481054306
time_elpased: 2.108
batch start
#iterations: 166
currently lose_sum: 90.52518290281296
time_elpased: 2.072
batch start
#iterations: 167
currently lose_sum: 90.49997675418854
time_elpased: 2.112
batch start
#iterations: 168
currently lose_sum: 90.69530510902405
time_elpased: 2.153
batch start
#iterations: 169
currently lose_sum: 90.68825763463974
time_elpased: 2.182
batch start
#iterations: 170
currently lose_sum: 90.42341578006744
time_elpased: 2.122
batch start
#iterations: 171
currently lose_sum: 90.29276210069656
time_elpased: 2.138
batch start
#iterations: 172
currently lose_sum: 90.40030229091644
time_elpased: 2.154
batch start
#iterations: 173
currently lose_sum: 89.89758586883545
time_elpased: 2.089
batch start
#iterations: 174
currently lose_sum: 90.77291119098663
time_elpased: 2.06
batch start
#iterations: 175
currently lose_sum: 90.07328617572784
time_elpased: 2.05
batch start
#iterations: 176
currently lose_sum: 90.76038181781769
time_elpased: 2.099
batch start
#iterations: 177
currently lose_sum: 90.58429378271103
time_elpased: 2.059
batch start
#iterations: 178
currently lose_sum: 90.362100481987
time_elpased: 2.152
batch start
#iterations: 179
currently lose_sum: 90.05291849374771
time_elpased: 2.11
start validation test
0.584072164948
0.607263733403
0.480086446434
0.536237714811
0.584254727916
67.751
batch start
#iterations: 180
currently lose_sum: 90.17356389760971
time_elpased: 2.16
batch start
#iterations: 181
currently lose_sum: 90.01224344968796
time_elpased: 2.05
batch start
#iterations: 182
currently lose_sum: 90.06781613826752
time_elpased: 2.04
batch start
#iterations: 183
currently lose_sum: 89.87113392353058
time_elpased: 2.129
batch start
#iterations: 184
currently lose_sum: 89.53560453653336
time_elpased: 2.121
batch start
#iterations: 185
currently lose_sum: 89.64075231552124
time_elpased: 2.143
batch start
#iterations: 186
currently lose_sum: 89.63683325052261
time_elpased: 2.145
batch start
#iterations: 187
currently lose_sum: 89.92669880390167
time_elpased: 2.194
batch start
#iterations: 188
currently lose_sum: 89.87878006696701
time_elpased: 2.079
batch start
#iterations: 189
currently lose_sum: 89.75741994380951
time_elpased: 2.075
batch start
#iterations: 190
currently lose_sum: 90.04020178318024
time_elpased: 2.078
batch start
#iterations: 191
currently lose_sum: 89.44179308414459
time_elpased: 2.103
batch start
#iterations: 192
currently lose_sum: 89.10138738155365
time_elpased: 2.205
batch start
#iterations: 193
currently lose_sum: 89.1776494383812
time_elpased: 2.108
batch start
#iterations: 194
currently lose_sum: 89.200534760952
time_elpased: 2.093
batch start
#iterations: 195
currently lose_sum: 89.81100881099701
time_elpased: 2.122
batch start
#iterations: 196
currently lose_sum: 89.55493915081024
time_elpased: 2.166
batch start
#iterations: 197
currently lose_sum: 89.44504380226135
time_elpased: 2.076
batch start
#iterations: 198
currently lose_sum: 89.57547295093536
time_elpased: 2.153
batch start
#iterations: 199
currently lose_sum: 90.18532663583755
time_elpased: 2.115
start validation test
0.563402061856
0.63451995685
0.302665431718
0.409838350056
0.563859825226
71.854
batch start
#iterations: 200
currently lose_sum: 89.31767988204956
time_elpased: 2.13
batch start
#iterations: 201
currently lose_sum: 89.18851190805435
time_elpased: 2.161
batch start
#iterations: 202
currently lose_sum: 89.06403708457947
time_elpased: 2.122
batch start
#iterations: 203
currently lose_sum: 89.17005378007889
time_elpased: 2.211
batch start
#iterations: 204
currently lose_sum: 89.26246708631516
time_elpased: 2.151
batch start
#iterations: 205
currently lose_sum: 89.51753920316696
time_elpased: 2.182
batch start
#iterations: 206
currently lose_sum: 88.82849842309952
time_elpased: 2.237
batch start
#iterations: 207
currently lose_sum: 88.67434722185135
time_elpased: 2.091
batch start
#iterations: 208
currently lose_sum: 88.77953618764877
time_elpased: 2.335
batch start
#iterations: 209
currently lose_sum: 88.98837316036224
time_elpased: 2.065
batch start
#iterations: 210
currently lose_sum: 89.11166775226593
time_elpased: 2.123
batch start
#iterations: 211
currently lose_sum: 89.26435434818268
time_elpased: 2.147
batch start
#iterations: 212
currently lose_sum: 88.86824196577072
time_elpased: 2.079
batch start
#iterations: 213
currently lose_sum: 88.54720026254654
time_elpased: 2.092
batch start
#iterations: 214
currently lose_sum: 88.50183725357056
time_elpased: 2.105
batch start
#iterations: 215
currently lose_sum: 88.74862116575241
time_elpased: 2.162
batch start
#iterations: 216
currently lose_sum: 88.71227312088013
time_elpased: 2.172
batch start
#iterations: 217
currently lose_sum: 88.4222229719162
time_elpased: 2.076
batch start
#iterations: 218
currently lose_sum: 88.97625124454498
time_elpased: 2.095
batch start
#iterations: 219
currently lose_sum: 88.66346228122711
time_elpased: 2.117
start validation test
0.575721649485
0.596744791667
0.471647627869
0.526872449273
0.575904367482
69.613
batch start
#iterations: 220
currently lose_sum: 88.31137746572495
time_elpased: 2.131
batch start
#iterations: 221
currently lose_sum: 88.57613718509674
time_elpased: 2.176
batch start
#iterations: 222
currently lose_sum: 88.52804213762283
time_elpased: 2.24
batch start
#iterations: 223
currently lose_sum: 88.53171718120575
time_elpased: 2.135
batch start
#iterations: 224
currently lose_sum: 88.1961499452591
time_elpased: 2.178
batch start
#iterations: 225
currently lose_sum: 87.95392781496048
time_elpased: 2.103
batch start
#iterations: 226
currently lose_sum: 88.10627192258835
time_elpased: 2.11
batch start
#iterations: 227
currently lose_sum: 88.17026150226593
time_elpased: 2.142
batch start
#iterations: 228
currently lose_sum: 88.5339823961258
time_elpased: 2.229
batch start
#iterations: 229
currently lose_sum: 88.42640167474747
time_elpased: 2.109
batch start
#iterations: 230
currently lose_sum: 88.01433277130127
time_elpased: 2.096
batch start
#iterations: 231
currently lose_sum: 87.97428530454636
time_elpased: 2.066
batch start
#iterations: 232
currently lose_sum: 87.93304300308228
time_elpased: 2.173
batch start
#iterations: 233
currently lose_sum: 88.14675670862198
time_elpased: 2.154
batch start
#iterations: 234
currently lose_sum: 88.27072978019714
time_elpased: 2.147
batch start
#iterations: 235
currently lose_sum: 88.1384945511818
time_elpased: 2.114
batch start
#iterations: 236
currently lose_sum: 88.42395514249802
time_elpased: 2.139
batch start
#iterations: 237
currently lose_sum: 87.66827809810638
time_elpased: 2.106
batch start
#iterations: 238
currently lose_sum: 87.97661429643631
time_elpased: 2.265
batch start
#iterations: 239
currently lose_sum: 87.5764507651329
time_elpased: 2.127
start validation test
0.565927835052
0.608216432866
0.37480703921
0.463801337154
0.566263377087
71.326
batch start
#iterations: 240
currently lose_sum: 87.6245424747467
time_elpased: 2.157
batch start
#iterations: 241
currently lose_sum: 87.62810212373734
time_elpased: 2.121
batch start
#iterations: 242
currently lose_sum: 87.76975631713867
time_elpased: 2.164
batch start
#iterations: 243
currently lose_sum: 87.07160192728043
time_elpased: 2.127
batch start
#iterations: 244
currently lose_sum: 87.8611627817154
time_elpased: 2.06
batch start
#iterations: 245
currently lose_sum: 87.66707986593246
time_elpased: 2.057
batch start
#iterations: 246
currently lose_sum: 87.66020780801773
time_elpased: 2.094
batch start
#iterations: 247
currently lose_sum: 87.14393895864487
time_elpased: 2.092
batch start
#iterations: 248
currently lose_sum: 87.34150958061218
time_elpased: 2.124
batch start
#iterations: 249
currently lose_sum: 87.0327177643776
time_elpased: 2.176
batch start
#iterations: 250
currently lose_sum: 87.40579402446747
time_elpased: 2.145
batch start
#iterations: 251
currently lose_sum: 87.18017321825027
time_elpased: 2.149
batch start
#iterations: 252
currently lose_sum: 87.57090240716934
time_elpased: 2.145
batch start
#iterations: 253
currently lose_sum: 87.0277783870697
time_elpased: 2.143
batch start
#iterations: 254
currently lose_sum: 87.27179020643234
time_elpased: 2.114
batch start
#iterations: 255
currently lose_sum: 86.98579430580139
time_elpased: 2.164
batch start
#iterations: 256
currently lose_sum: 87.09246391057968
time_elpased: 2.17
batch start
#iterations: 257
currently lose_sum: 87.19262152910233
time_elpased: 2.073
batch start
#iterations: 258
currently lose_sum: 87.20006835460663
time_elpased: 2.094
batch start
#iterations: 259
currently lose_sum: 87.12196749448776
time_elpased: 2.1
start validation test
0.581391752577
0.610220994475
0.454667078316
0.521082738692
0.581614237289
69.870
batch start
#iterations: 260
currently lose_sum: 86.43238753080368
time_elpased: 2.07
batch start
#iterations: 261
currently lose_sum: 86.99920237064362
time_elpased: 2.107
batch start
#iterations: 262
currently lose_sum: 86.74349308013916
time_elpased: 2.076
batch start
#iterations: 263
currently lose_sum: 87.1127210855484
time_elpased: 2.158
batch start
#iterations: 264
currently lose_sum: 86.45506596565247
time_elpased: 2.108
batch start
#iterations: 265
currently lose_sum: 86.02477353811264
time_elpased: 2.114
batch start
#iterations: 266
currently lose_sum: 86.38041245937347
time_elpased: 2.156
batch start
#iterations: 267
currently lose_sum: 86.89022278785706
time_elpased: 2.139
batch start
#iterations: 268
currently lose_sum: 86.52922904491425
time_elpased: 2.09
batch start
#iterations: 269
currently lose_sum: 86.40014445781708
time_elpased: 2.094
batch start
#iterations: 270
currently lose_sum: 86.69689017534256
time_elpased: 2.092
batch start
#iterations: 271
currently lose_sum: 86.58776366710663
time_elpased: 2.139
batch start
#iterations: 272
currently lose_sum: 86.80617481470108
time_elpased: 2.099
batch start
#iterations: 273
currently lose_sum: 85.98078685998917
time_elpased: 2.104
batch start
#iterations: 274
currently lose_sum: 86.24602264165878
time_elpased: 2.304
batch start
#iterations: 275
currently lose_sum: 86.40799450874329
time_elpased: 2.159
batch start
#iterations: 276
currently lose_sum: 86.40675514936447
time_elpased: 2.074
batch start
#iterations: 277
currently lose_sum: 86.07989525794983
time_elpased: 2.137
batch start
#iterations: 278
currently lose_sum: 86.48521554470062
time_elpased: 2.124
batch start
#iterations: 279
currently lose_sum: 86.35503274202347
time_elpased: 2.099
start validation test
0.57118556701
0.634010736196
0.340331377997
0.442911672136
0.571590867146
74.430
batch start
#iterations: 280
currently lose_sum: 86.17707002162933
time_elpased: 2.24
batch start
#iterations: 281
currently lose_sum: 86.23549753427505
time_elpased: 2.154
batch start
#iterations: 282
currently lose_sum: 86.25012111663818
time_elpased: 2.23
batch start
#iterations: 283
currently lose_sum: 86.54453212022781
time_elpased: 2.149
batch start
#iterations: 284
currently lose_sum: 86.05096626281738
time_elpased: 2.198
batch start
#iterations: 285
currently lose_sum: 85.7959771156311
time_elpased: 2.071
batch start
#iterations: 286
currently lose_sum: 86.16815227270126
time_elpased: 2.126
batch start
#iterations: 287
currently lose_sum: 86.0684329867363
time_elpased: 2.174
batch start
#iterations: 288
currently lose_sum: 85.87986552715302
time_elpased: 2.067
batch start
#iterations: 289
currently lose_sum: 85.8906922340393
time_elpased: 2.185
batch start
#iterations: 290
currently lose_sum: 85.39429843425751
time_elpased: 2.174
batch start
#iterations: 291
currently lose_sum: 85.97713369131088
time_elpased: 2.139
batch start
#iterations: 292
currently lose_sum: 85.39059340953827
time_elpased: 2.115
batch start
#iterations: 293
currently lose_sum: 85.64096289873123
time_elpased: 2.12
batch start
#iterations: 294
currently lose_sum: 85.45734143257141
time_elpased: 2.126
batch start
#iterations: 295
currently lose_sum: 85.59344232082367
time_elpased: 2.181
batch start
#iterations: 296
currently lose_sum: 85.99484080076218
time_elpased: 2.071
batch start
#iterations: 297
currently lose_sum: 85.66315376758575
time_elpased: 2.097
batch start
#iterations: 298
currently lose_sum: 86.23494791984558
time_elpased: 2.118
batch start
#iterations: 299
currently lose_sum: 85.37159377336502
time_elpased: 2.095
start validation test
0.570463917526
0.60023174971
0.426469074817
0.498646290837
0.570716722682
72.746
batch start
#iterations: 300
currently lose_sum: 85.29066288471222
time_elpased: 2.127
batch start
#iterations: 301
currently lose_sum: 85.81606370210648
time_elpased: 2.122
batch start
#iterations: 302
currently lose_sum: 85.69283652305603
time_elpased: 2.373
batch start
#iterations: 303
currently lose_sum: 85.59106904268265
time_elpased: 2.119
batch start
#iterations: 304
currently lose_sum: 85.14654058218002
time_elpased: 2.104
batch start
#iterations: 305
currently lose_sum: 84.76482200622559
time_elpased: 2.136
batch start
#iterations: 306
currently lose_sum: 85.32971960306168
time_elpased: 2.118
batch start
#iterations: 307
currently lose_sum: 85.21957302093506
time_elpased: 2.144
batch start
#iterations: 308
currently lose_sum: 85.4664614200592
time_elpased: 2.274
batch start
#iterations: 309
currently lose_sum: 84.94127237796783
time_elpased: 2.214
batch start
#iterations: 310
currently lose_sum: 84.65673077106476
time_elpased: 2.135
batch start
#iterations: 311
currently lose_sum: 85.40987807512283
time_elpased: 2.108
batch start
#iterations: 312
currently lose_sum: 85.77225089073181
time_elpased: 2.179
batch start
#iterations: 313
currently lose_sum: 85.00344681739807
time_elpased: 2.143
batch start
#iterations: 314
currently lose_sum: 84.88101822137833
time_elpased: 2.113
batch start
#iterations: 315
currently lose_sum: 85.25138455629349
time_elpased: 2.072
batch start
#iterations: 316
currently lose_sum: 85.19760257005692
time_elpased: 2.053
batch start
#iterations: 317
currently lose_sum: 84.5046826004982
time_elpased: 2.083
batch start
#iterations: 318
currently lose_sum: 84.65381073951721
time_elpased: 2.126
batch start
#iterations: 319
currently lose_sum: 84.95957112312317
time_elpased: 2.094
start validation test
0.57381443299
0.593665158371
0.472573839662
0.526243410497
0.573992176467
72.430
batch start
#iterations: 320
currently lose_sum: 84.97108775377274
time_elpased: 2.141
batch start
#iterations: 321
currently lose_sum: 84.87889087200165
time_elpased: 2.037
batch start
#iterations: 322
currently lose_sum: 85.10270249843597
time_elpased: 2.059
batch start
#iterations: 323
currently lose_sum: 84.63466727733612
time_elpased: 2.129
batch start
#iterations: 324
currently lose_sum: 85.01732265949249
time_elpased: 2.126
batch start
#iterations: 325
currently lose_sum: 84.37064564228058
time_elpased: 2.134
batch start
#iterations: 326
currently lose_sum: 84.35826554894447
time_elpased: 2.114
batch start
#iterations: 327
currently lose_sum: 84.4089589715004
time_elpased: 2.006
batch start
#iterations: 328
currently lose_sum: 83.83686953783035
time_elpased: 2.157
batch start
#iterations: 329
currently lose_sum: 84.83898586034775
time_elpased: 2.091
batch start
#iterations: 330
currently lose_sum: 84.00005477666855
time_elpased: 2.153
batch start
#iterations: 331
currently lose_sum: 84.88163861632347
time_elpased: 2.074
batch start
#iterations: 332
currently lose_sum: 84.52745997905731
time_elpased: 2.038
batch start
#iterations: 333
currently lose_sum: 84.53949528932571
time_elpased: 2.028
batch start
#iterations: 334
currently lose_sum: 84.39672464132309
time_elpased: 2.058
batch start
#iterations: 335
currently lose_sum: 84.28681364655495
time_elpased: 2.073
batch start
#iterations: 336
currently lose_sum: 84.25878351926804
time_elpased: 2.08
batch start
#iterations: 337
currently lose_sum: 84.75323623418808
time_elpased: 2.054
batch start
#iterations: 338
currently lose_sum: 83.98841625452042
time_elpased: 2.126
batch start
#iterations: 339
currently lose_sum: 84.09142208099365
time_elpased: 2.104
start validation test
0.567731958763
0.60815862181
0.385098281363
0.471581600504
0.568052600353
74.529
batch start
#iterations: 340
currently lose_sum: 84.28187698125839
time_elpased: 2.064
batch start
#iterations: 341
currently lose_sum: 83.82837611436844
time_elpased: 2.212
batch start
#iterations: 342
currently lose_sum: 84.25161004066467
time_elpased: 1.954
batch start
#iterations: 343
currently lose_sum: 84.00691843032837
time_elpased: 2.101
batch start
#iterations: 344
currently lose_sum: 84.10630649328232
time_elpased: 1.94
batch start
#iterations: 345
currently lose_sum: 83.41753250360489
time_elpased: 2.036
batch start
#iterations: 346
currently lose_sum: 83.92903965711594
time_elpased: 2.064
batch start
#iterations: 347
currently lose_sum: 83.48850232362747
time_elpased: 2.062
batch start
#iterations: 348
currently lose_sum: 83.99644619226456
time_elpased: 2.137
batch start
#iterations: 349
currently lose_sum: 83.84834617376328
time_elpased: 2.11
batch start
#iterations: 350
currently lose_sum: 84.30616348981857
time_elpased: 2.113
batch start
#iterations: 351
currently lose_sum: 83.84228551387787
time_elpased: 2.109
batch start
#iterations: 352
currently lose_sum: 83.68645679950714
time_elpased: 2.146
batch start
#iterations: 353
currently lose_sum: 83.7116174697876
time_elpased: 2.1
batch start
#iterations: 354
currently lose_sum: 84.03169894218445
time_elpased: 2.079
batch start
#iterations: 355
currently lose_sum: 84.08577960729599
time_elpased: 2.028
batch start
#iterations: 356
currently lose_sum: 83.93444406986237
time_elpased: 2.087
batch start
#iterations: 357
currently lose_sum: 83.32909056544304
time_elpased: 2.012
batch start
#iterations: 358
currently lose_sum: 83.32168787717819
time_elpased: 2.061
batch start
#iterations: 359
currently lose_sum: 83.52062273025513
time_elpased: 2.06
start validation test
0.56706185567
0.611279972982
0.372542965936
0.4629452011
0.567403363584
76.646
batch start
#iterations: 360
currently lose_sum: 83.6990954875946
time_elpased: 2.118
batch start
#iterations: 361
currently lose_sum: 83.66847163438797
time_elpased: 2.08
batch start
#iterations: 362
currently lose_sum: 82.65367352962494
time_elpased: 2.089
batch start
#iterations: 363
currently lose_sum: 83.37836599349976
time_elpased: 2.159
batch start
#iterations: 364
currently lose_sum: 83.7494215965271
time_elpased: 2.015
batch start
#iterations: 365
currently lose_sum: 82.93032997846603
time_elpased: 1.91
batch start
#iterations: 366
currently lose_sum: 82.86851459741592
time_elpased: 1.835
batch start
#iterations: 367
currently lose_sum: 83.64506942033768
time_elpased: 1.793
batch start
#iterations: 368
currently lose_sum: 83.00881516933441
time_elpased: 1.999
batch start
#iterations: 369
currently lose_sum: 82.9943722486496
time_elpased: 2.137
batch start
#iterations: 370
currently lose_sum: 83.26235800981522
time_elpased: 2.112
batch start
#iterations: 371
currently lose_sum: 83.26057559251785
time_elpased: 2.094
batch start
#iterations: 372
currently lose_sum: 83.25142592191696
time_elpased: 2.225
batch start
#iterations: 373
currently lose_sum: 83.12964332103729
time_elpased: 2.126
batch start
#iterations: 374
currently lose_sum: 83.57387036085129
time_elpased: 2.115
batch start
#iterations: 375
currently lose_sum: 82.84396177530289
time_elpased: 2.115
batch start
#iterations: 376
currently lose_sum: 83.2119876742363
time_elpased: 2.116
batch start
#iterations: 377
currently lose_sum: 82.74601942300797
time_elpased: 2.113
batch start
#iterations: 378
currently lose_sum: 82.94000107049942
time_elpased: 2.156
batch start
#iterations: 379
currently lose_sum: 83.30261188745499
time_elpased: 2.105
start validation test
0.572422680412
0.598313053097
0.445302047957
0.510590595315
0.57264586029
75.646
batch start
#iterations: 380
currently lose_sum: 82.65676426887512
time_elpased: 2.068
batch start
#iterations: 381
currently lose_sum: 82.86706927418709
time_elpased: 2.094
batch start
#iterations: 382
currently lose_sum: 82.70045053958893
time_elpased: 2.244
batch start
#iterations: 383
currently lose_sum: 82.65530556440353
time_elpased: 2.221
batch start
#iterations: 384
currently lose_sum: 82.82905548810959
time_elpased: 2.103
batch start
#iterations: 385
currently lose_sum: 82.78253662586212
time_elpased: 2.205
batch start
#iterations: 386
currently lose_sum: 82.0992186665535
time_elpased: 2.1
batch start
#iterations: 387
currently lose_sum: 82.40375596284866
time_elpased: 2.132
batch start
#iterations: 388
currently lose_sum: 82.35865128040314
time_elpased: 2.128
batch start
#iterations: 389
currently lose_sum: 82.31435033679008
time_elpased: 2.115
batch start
#iterations: 390
currently lose_sum: 82.52802419662476
time_elpased: 2.102
batch start
#iterations: 391
currently lose_sum: 82.97747004032135
time_elpased: 2.099
batch start
#iterations: 392
currently lose_sum: 82.08830708265305
time_elpased: 2.103
batch start
#iterations: 393
currently lose_sum: 82.41357919573784
time_elpased: 2.142
batch start
#iterations: 394
currently lose_sum: 82.90385454893112
time_elpased: 2.065
batch start
#iterations: 395
currently lose_sum: 82.37437182664871
time_elpased: 2.107
batch start
#iterations: 396
currently lose_sum: 81.81335669755936
time_elpased: 2.168
batch start
#iterations: 397
currently lose_sum: 82.78329509496689
time_elpased: 2.206
batch start
#iterations: 398
currently lose_sum: 82.38633501529694
time_elpased: 2.111
batch start
#iterations: 399
currently lose_sum: 82.52077943086624
time_elpased: 2.168
start validation test
0.561958762887
0.611691405534
0.343521663065
0.439963094767
0.562342262907
77.982
acc: 0.611
pre: 0.612
rec: 0.611
F1: 0.612
auc: 0.611
