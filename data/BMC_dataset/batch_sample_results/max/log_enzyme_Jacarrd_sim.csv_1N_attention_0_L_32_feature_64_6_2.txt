start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.37799644470215
time_elpased: 2.835
batch start
#iterations: 1
currently lose_sum: 100.14627939462662
time_elpased: 2.827
batch start
#iterations: 2
currently lose_sum: 100.14026963710785
time_elpased: 2.787
batch start
#iterations: 3
currently lose_sum: 99.88178622722626
time_elpased: 2.79
batch start
#iterations: 4
currently lose_sum: 99.62101322412491
time_elpased: 2.772
batch start
#iterations: 5
currently lose_sum: 99.52126574516296
time_elpased: 2.76
batch start
#iterations: 6
currently lose_sum: 99.28162115812302
time_elpased: 2.794
batch start
#iterations: 7
currently lose_sum: 99.18032604455948
time_elpased: 2.78
batch start
#iterations: 8
currently lose_sum: 99.3135022521019
time_elpased: 2.832
batch start
#iterations: 9
currently lose_sum: 99.0415004491806
time_elpased: 2.753
batch start
#iterations: 10
currently lose_sum: 98.86667770147324
time_elpased: 2.775
batch start
#iterations: 11
currently lose_sum: 98.80721187591553
time_elpased: 2.771
batch start
#iterations: 12
currently lose_sum: 98.9905349612236
time_elpased: 2.759
batch start
#iterations: 13
currently lose_sum: 98.63159775733948
time_elpased: 2.76
batch start
#iterations: 14
currently lose_sum: 98.73291289806366
time_elpased: 2.775
batch start
#iterations: 15
currently lose_sum: 98.67924058437347
time_elpased: 2.812
batch start
#iterations: 16
currently lose_sum: 98.51495987176895
time_elpased: 2.813
batch start
#iterations: 17
currently lose_sum: 98.38074934482574
time_elpased: 2.817
batch start
#iterations: 18
currently lose_sum: 98.26406842470169
time_elpased: 2.796
batch start
#iterations: 19
currently lose_sum: 98.22422093153
time_elpased: 2.821
start validation test
0.611804123711
0.614474235442
0.603787177112
0.609083830781
0.611818198698
64.355
batch start
#iterations: 20
currently lose_sum: 98.29638284444809
time_elpased: 2.833
batch start
#iterations: 21
currently lose_sum: 98.20769029855728
time_elpased: 2.814
batch start
#iterations: 22
currently lose_sum: 97.85976284742355
time_elpased: 2.826
batch start
#iterations: 23
currently lose_sum: 98.1823553442955
time_elpased: 2.822
batch start
#iterations: 24
currently lose_sum: 97.67804706096649
time_elpased: 2.846
batch start
#iterations: 25
currently lose_sum: 97.89040035009384
time_elpased: 2.834
batch start
#iterations: 26
currently lose_sum: 97.97860795259476
time_elpased: 2.781
batch start
#iterations: 27
currently lose_sum: 97.82231831550598
time_elpased: 2.76
batch start
#iterations: 28
currently lose_sum: 97.58699584007263
time_elpased: 2.813
batch start
#iterations: 29
currently lose_sum: 98.1248522400856
time_elpased: 2.807
batch start
#iterations: 30
currently lose_sum: 97.69326394796371
time_elpased: 2.79
batch start
#iterations: 31
currently lose_sum: 97.58025825023651
time_elpased: 2.768
batch start
#iterations: 32
currently lose_sum: 97.60543179512024
time_elpased: 2.757
batch start
#iterations: 33
currently lose_sum: 97.56891655921936
time_elpased: 2.759
batch start
#iterations: 34
currently lose_sum: 97.20845872163773
time_elpased: 2.697
batch start
#iterations: 35
currently lose_sum: 97.5285564661026
time_elpased: 2.741
batch start
#iterations: 36
currently lose_sum: 97.33533245325089
time_elpased: 2.753
batch start
#iterations: 37
currently lose_sum: 97.07557129859924
time_elpased: 2.781
batch start
#iterations: 38
currently lose_sum: 97.46339154243469
time_elpased: 2.775
batch start
#iterations: 39
currently lose_sum: 97.09789198637009
time_elpased: 2.79
start validation test
0.61175257732
0.628756629346
0.549037768859
0.586199318756
0.611862682839
64.363
batch start
#iterations: 40
currently lose_sum: 97.03657567501068
time_elpased: 2.738
batch start
#iterations: 41
currently lose_sum: 97.24162060022354
time_elpased: 2.736
batch start
#iterations: 42
currently lose_sum: 97.11915421485901
time_elpased: 2.797
batch start
#iterations: 43
currently lose_sum: 96.75561141967773
time_elpased: 2.807
batch start
#iterations: 44
currently lose_sum: 97.02433609962463
time_elpased: 2.707
batch start
#iterations: 45
currently lose_sum: 96.82415288686752
time_elpased: 2.707
batch start
#iterations: 46
currently lose_sum: 96.73799353837967
time_elpased: 2.778
batch start
#iterations: 47
currently lose_sum: 96.60748487710953
time_elpased: 2.809
batch start
#iterations: 48
currently lose_sum: 96.59355753660202
time_elpased: 2.81
batch start
#iterations: 49
currently lose_sum: 97.10218358039856
time_elpased: 2.828
batch start
#iterations: 50
currently lose_sum: 96.50591784715652
time_elpased: 2.8
batch start
#iterations: 51
currently lose_sum: 96.50984638929367
time_elpased: 2.837
batch start
#iterations: 52
currently lose_sum: 96.53294932842255
time_elpased: 2.799
batch start
#iterations: 53
currently lose_sum: 96.50136387348175
time_elpased: 2.81
batch start
#iterations: 54
currently lose_sum: 96.34328478574753
time_elpased: 2.804
batch start
#iterations: 55
currently lose_sum: 96.43970811367035
time_elpased: 2.786
batch start
#iterations: 56
currently lose_sum: 96.38225424289703
time_elpased: 2.765
batch start
#iterations: 57
currently lose_sum: 96.33359837532043
time_elpased: 2.827
batch start
#iterations: 58
currently lose_sum: 95.74811667203903
time_elpased: 2.809
batch start
#iterations: 59
currently lose_sum: 96.59674996137619
time_elpased: 2.826
start validation test
0.608195876289
0.630973013122
0.524544612535
0.572857544254
0.608342738984
63.989
batch start
#iterations: 60
currently lose_sum: 95.9046476483345
time_elpased: 2.815
batch start
#iterations: 61
currently lose_sum: 96.26658695936203
time_elpased: 2.834
batch start
#iterations: 62
currently lose_sum: 96.39275681972504
time_elpased: 2.879
batch start
#iterations: 63
currently lose_sum: 96.05573844909668
time_elpased: 2.811
batch start
#iterations: 64
currently lose_sum: 95.92991501092911
time_elpased: 2.851
batch start
#iterations: 65
currently lose_sum: 95.43391627073288
time_elpased: 2.84
batch start
#iterations: 66
currently lose_sum: 95.89990156888962
time_elpased: 2.829
batch start
#iterations: 67
currently lose_sum: 95.44154328107834
time_elpased: 2.809
batch start
#iterations: 68
currently lose_sum: 95.7566666007042
time_elpased: 2.843
batch start
#iterations: 69
currently lose_sum: 95.36897903680801
time_elpased: 2.823
batch start
#iterations: 70
currently lose_sum: 95.7529513835907
time_elpased: 2.801
batch start
#iterations: 71
currently lose_sum: 95.60380125045776
time_elpased: 2.839
batch start
#iterations: 72
currently lose_sum: 95.41583001613617
time_elpased: 2.822
batch start
#iterations: 73
currently lose_sum: 95.31608235836029
time_elpased: 3.059
batch start
#iterations: 74
currently lose_sum: 95.32919800281525
time_elpased: 2.758
batch start
#iterations: 75
currently lose_sum: 95.43147015571594
time_elpased: 2.741
batch start
#iterations: 76
currently lose_sum: 95.26551061868668
time_elpased: 2.785
batch start
#iterations: 77
currently lose_sum: 95.20464617013931
time_elpased: 2.793
batch start
#iterations: 78
currently lose_sum: 95.31943506002426
time_elpased: 2.801
batch start
#iterations: 79
currently lose_sum: 95.17724823951721
time_elpased: 2.774
start validation test
0.604278350515
0.630102040816
0.508387362355
0.562738508857
0.604446701935
64.516
batch start
#iterations: 80
currently lose_sum: 95.07366091012955
time_elpased: 2.727
batch start
#iterations: 81
currently lose_sum: 94.93434554338455
time_elpased: 2.758
batch start
#iterations: 82
currently lose_sum: 95.14271438121796
time_elpased: 2.878
batch start
#iterations: 83
currently lose_sum: 94.4040094614029
time_elpased: 2.844
batch start
#iterations: 84
currently lose_sum: 94.88149172067642
time_elpased: 2.745
batch start
#iterations: 85
currently lose_sum: 94.76430076360703
time_elpased: 2.773
batch start
#iterations: 86
currently lose_sum: 94.3616151213646
time_elpased: 2.763
batch start
#iterations: 87
currently lose_sum: 94.99919325113297
time_elpased: 2.765
batch start
#iterations: 88
currently lose_sum: 94.62568092346191
time_elpased: 2.661
batch start
#iterations: 89
currently lose_sum: 94.4622820019722
time_elpased: 2.607
batch start
#iterations: 90
currently lose_sum: 94.73287779092789
time_elpased: 2.614
batch start
#iterations: 91
currently lose_sum: 94.39151030778885
time_elpased: 2.661
batch start
#iterations: 92
currently lose_sum: 94.40047788619995
time_elpased: 2.782
batch start
#iterations: 93
currently lose_sum: 94.2970159649849
time_elpased: 2.803
batch start
#iterations: 94
currently lose_sum: 94.18301463127136
time_elpased: 2.77
batch start
#iterations: 95
currently lose_sum: 94.05850040912628
time_elpased: 2.767
batch start
#iterations: 96
currently lose_sum: 93.9233837723732
time_elpased: 2.738
batch start
#iterations: 97
currently lose_sum: 93.72642290592194
time_elpased: 2.766
batch start
#iterations: 98
currently lose_sum: 94.21807330846786
time_elpased: 2.761
batch start
#iterations: 99
currently lose_sum: 93.38978862762451
time_elpased: 2.745
start validation test
0.605412371134
0.632655687082
0.506020376659
0.562296300532
0.60558686911
65.167
batch start
#iterations: 100
currently lose_sum: 93.86921089887619
time_elpased: 2.78
batch start
#iterations: 101
currently lose_sum: 93.9326269030571
time_elpased: 2.785
batch start
#iterations: 102
currently lose_sum: 93.88525146245956
time_elpased: 2.782
batch start
#iterations: 103
currently lose_sum: 93.51146638393402
time_elpased: 2.757
batch start
#iterations: 104
currently lose_sum: 93.8478073477745
time_elpased: 2.792
batch start
#iterations: 105
currently lose_sum: 93.2421852350235
time_elpased: 2.8
batch start
#iterations: 106
currently lose_sum: 93.54453241825104
time_elpased: 2.818
batch start
#iterations: 107
currently lose_sum: 93.34097307920456
time_elpased: 2.806
batch start
#iterations: 108
currently lose_sum: 93.31584763526917
time_elpased: 2.792
batch start
#iterations: 109
currently lose_sum: 93.37872117757797
time_elpased: 2.761
batch start
#iterations: 110
currently lose_sum: 93.1661542057991
time_elpased: 2.724
batch start
#iterations: 111
currently lose_sum: 92.88440853357315
time_elpased: 2.805
batch start
#iterations: 112
currently lose_sum: 92.9768767952919
time_elpased: 2.795
batch start
#iterations: 113
currently lose_sum: 93.02809911966324
time_elpased: 2.814
batch start
#iterations: 114
currently lose_sum: 92.94362765550613
time_elpased: 2.833
batch start
#iterations: 115
currently lose_sum: 93.22968071699142
time_elpased: 2.82
batch start
#iterations: 116
currently lose_sum: 92.23338901996613
time_elpased: 2.805
batch start
#iterations: 117
currently lose_sum: 92.72023940086365
time_elpased: 2.795
batch start
#iterations: 118
currently lose_sum: 92.55085825920105
time_elpased: 2.758
batch start
#iterations: 119
currently lose_sum: 92.69659024477005
time_elpased: 2.804
start validation test
0.593402061856
0.610433522521
0.520222290831
0.561729081009
0.593530540231
66.222
batch start
#iterations: 120
currently lose_sum: 92.68945777416229
time_elpased: 2.833
batch start
#iterations: 121
currently lose_sum: 92.75230592489243
time_elpased: 2.803
batch start
#iterations: 122
currently lose_sum: 92.47000741958618
time_elpased: 2.754
batch start
#iterations: 123
currently lose_sum: 92.31398475170135
time_elpased: 2.745
batch start
#iterations: 124
currently lose_sum: 92.55215579271317
time_elpased: 2.788
batch start
#iterations: 125
currently lose_sum: 91.92166149616241
time_elpased: 2.789
batch start
#iterations: 126
currently lose_sum: 92.43476641178131
time_elpased: 2.788
batch start
#iterations: 127
currently lose_sum: 91.75860303640366
time_elpased: 2.732
batch start
#iterations: 128
currently lose_sum: 91.62235504388809
time_elpased: 2.72
batch start
#iterations: 129
currently lose_sum: 92.64043164253235
time_elpased: 2.712
batch start
#iterations: 130
currently lose_sum: 91.54012334346771
time_elpased: 2.675
batch start
#iterations: 131
currently lose_sum: 91.94701778888702
time_elpased: 2.765
batch start
#iterations: 132
currently lose_sum: 91.5485491156578
time_elpased: 2.758
batch start
#iterations: 133
currently lose_sum: 91.63037759065628
time_elpased: 2.744
batch start
#iterations: 134
currently lose_sum: 91.51384234428406
time_elpased: 2.779
batch start
#iterations: 135
currently lose_sum: 91.47226297855377
time_elpased: 2.762
batch start
#iterations: 136
currently lose_sum: 91.53151559829712
time_elpased: 2.732
batch start
#iterations: 137
currently lose_sum: 91.16040694713593
time_elpased: 2.746
batch start
#iterations: 138
currently lose_sum: 91.00917679071426
time_elpased: 2.778
batch start
#iterations: 139
currently lose_sum: 91.25994300842285
time_elpased: 2.793
start validation test
0.590721649485
0.603086204896
0.534938767109
0.566972076789
0.590819584938
68.070
batch start
#iterations: 140
currently lose_sum: 91.44997090101242
time_elpased: 2.807
batch start
#iterations: 141
currently lose_sum: 91.44434249401093
time_elpased: 2.791
batch start
#iterations: 142
currently lose_sum: 90.7681291103363
time_elpased: 2.748
batch start
#iterations: 143
currently lose_sum: 91.18621599674225
time_elpased: 2.796
batch start
#iterations: 144
currently lose_sum: 91.14294743537903
time_elpased: 2.795
batch start
#iterations: 145
currently lose_sum: 90.76243108510971
time_elpased: 2.787
batch start
#iterations: 146
currently lose_sum: 90.84095150232315
time_elpased: 2.792
batch start
#iterations: 147
currently lose_sum: 90.91384434700012
time_elpased: 2.75
batch start
#iterations: 148
currently lose_sum: 90.56538033485413
time_elpased: 2.791
batch start
#iterations: 149
currently lose_sum: 90.57387799024582
time_elpased: 2.81
batch start
#iterations: 150
currently lose_sum: 90.5789442062378
time_elpased: 2.794
batch start
#iterations: 151
currently lose_sum: 90.15526580810547
time_elpased: 2.801
batch start
#iterations: 152
currently lose_sum: 90.5896332859993
time_elpased: 2.762
batch start
#iterations: 153
currently lose_sum: 90.21074783802032
time_elpased: 2.778
batch start
#iterations: 154
currently lose_sum: 90.2404351234436
time_elpased: 2.821
batch start
#iterations: 155
currently lose_sum: 90.25373470783234
time_elpased: 2.798
batch start
#iterations: 156
currently lose_sum: 90.03591561317444
time_elpased: 2.809
batch start
#iterations: 157
currently lose_sum: 89.9905441403389
time_elpased: 2.776
batch start
#iterations: 158
currently lose_sum: 89.73766249418259
time_elpased: 2.783
batch start
#iterations: 159
currently lose_sum: 90.07158833742142
time_elpased: 2.832
start validation test
0.592113402062
0.632452276065
0.443243799527
0.521207720699
0.592374765611
69.822
batch start
#iterations: 160
currently lose_sum: 90.00942927598953
time_elpased: 2.8
batch start
#iterations: 161
currently lose_sum: 89.90382725000381
time_elpased: 2.799
batch start
#iterations: 162
currently lose_sum: 89.58993297815323
time_elpased: 2.805
batch start
#iterations: 163
currently lose_sum: 90.02631747722626
time_elpased: 2.806
batch start
#iterations: 164
currently lose_sum: 89.40197968482971
time_elpased: 2.833
batch start
#iterations: 165
currently lose_sum: 89.43043953180313
time_elpased: 2.838
batch start
#iterations: 166
currently lose_sum: 88.80909711122513
time_elpased: 2.884
batch start
#iterations: 167
currently lose_sum: 89.30595868825912
time_elpased: 2.87
batch start
#iterations: 168
currently lose_sum: 89.47724258899689
time_elpased: 2.833
batch start
#iterations: 169
currently lose_sum: 89.32736676931381
time_elpased: 2.796
batch start
#iterations: 170
currently lose_sum: 89.10858380794525
time_elpased: 2.75
batch start
#iterations: 171
currently lose_sum: 89.18952506780624
time_elpased: 2.778
batch start
#iterations: 172
currently lose_sum: 89.02632868289948
time_elpased: 2.797
batch start
#iterations: 173
currently lose_sum: 89.34247499704361
time_elpased: 2.788
batch start
#iterations: 174
currently lose_sum: 88.60880297422409
time_elpased: 2.798
batch start
#iterations: 175
currently lose_sum: 88.7854990363121
time_elpased: 2.749
batch start
#iterations: 176
currently lose_sum: 88.78100055456161
time_elpased: 2.778
batch start
#iterations: 177
currently lose_sum: 88.29317885637283
time_elpased: 2.734
batch start
#iterations: 178
currently lose_sum: 88.43263775110245
time_elpased: 2.765
batch start
#iterations: 179
currently lose_sum: 88.29503256082535
time_elpased: 2.735
start validation test
0.58412371134
0.632534962225
0.404960378718
0.493788430167
0.584438260205
74.226
batch start
#iterations: 180
currently lose_sum: 88.49584031105042
time_elpased: 2.744
batch start
#iterations: 181
currently lose_sum: 88.11723852157593
time_elpased: 2.79
batch start
#iterations: 182
currently lose_sum: 88.34126782417297
time_elpased: 2.781
batch start
#iterations: 183
currently lose_sum: 88.16551661491394
time_elpased: 2.764
batch start
#iterations: 184
currently lose_sum: 87.63041943311691
time_elpased: 2.755
batch start
#iterations: 185
currently lose_sum: 87.93133246898651
time_elpased: 2.746
batch start
#iterations: 186
currently lose_sum: 88.23260670900345
time_elpased: 2.811
batch start
#iterations: 187
currently lose_sum: 87.90668666362762
time_elpased: 2.808
batch start
#iterations: 188
currently lose_sum: 87.65575885772705
time_elpased: 2.791
batch start
#iterations: 189
currently lose_sum: 87.82543116807938
time_elpased: 2.781
batch start
#iterations: 190
currently lose_sum: 87.432726085186
time_elpased: 2.759
batch start
#iterations: 191
currently lose_sum: 86.80757677555084
time_elpased: 2.814
batch start
#iterations: 192
currently lose_sum: 87.68613296747208
time_elpased: 2.784
batch start
#iterations: 193
currently lose_sum: 86.60139566659927
time_elpased: 2.812
batch start
#iterations: 194
currently lose_sum: 86.99623560905457
time_elpased: 2.775
batch start
#iterations: 195
currently lose_sum: 87.48286926746368
time_elpased: 2.783
batch start
#iterations: 196
currently lose_sum: 87.2525365948677
time_elpased: 2.792
batch start
#iterations: 197
currently lose_sum: 87.3035072684288
time_elpased: 2.839
batch start
#iterations: 198
currently lose_sum: 86.8335953950882
time_elpased: 2.791
batch start
#iterations: 199
currently lose_sum: 87.1749666929245
time_elpased: 2.776
start validation test
0.581082474227
0.610141313383
0.453226304415
0.520106288751
0.581306945453
74.618
batch start
#iterations: 200
currently lose_sum: 86.95420384407043
time_elpased: 2.81
batch start
#iterations: 201
currently lose_sum: 86.72583365440369
time_elpased: 2.808
batch start
#iterations: 202
currently lose_sum: 86.17973762750626
time_elpased: 2.824
batch start
#iterations: 203
currently lose_sum: 86.37827306985855
time_elpased: 2.816
batch start
#iterations: 204
currently lose_sum: 86.73280501365662
time_elpased: 2.807
batch start
#iterations: 205
currently lose_sum: 86.41601896286011
time_elpased: 2.661
batch start
#iterations: 206
currently lose_sum: 86.06488847732544
time_elpased: 2.776
batch start
#iterations: 207
currently lose_sum: 86.42695415019989
time_elpased: 2.831
batch start
#iterations: 208
currently lose_sum: 85.74403423070908
time_elpased: 2.842
batch start
#iterations: 209
currently lose_sum: 85.7566978931427
time_elpased: 2.842
batch start
#iterations: 210
currently lose_sum: 85.9127665758133
time_elpased: 2.793
batch start
#iterations: 211
currently lose_sum: 86.2543454170227
time_elpased: 2.788
batch start
#iterations: 212
currently lose_sum: 85.95247983932495
time_elpased: 2.803
batch start
#iterations: 213
currently lose_sum: 85.75673162937164
time_elpased: 2.787
batch start
#iterations: 214
currently lose_sum: 85.55016607046127
time_elpased: 2.822
batch start
#iterations: 215
currently lose_sum: 85.26623958349228
time_elpased: 2.872
batch start
#iterations: 216
currently lose_sum: 84.88130193948746
time_elpased: 2.833
batch start
#iterations: 217
currently lose_sum: 85.42322266101837
time_elpased: 2.741
batch start
#iterations: 218
currently lose_sum: 85.80793744325638
time_elpased: 2.745
batch start
#iterations: 219
currently lose_sum: 85.0080326795578
time_elpased: 2.794
start validation test
0.574329896907
0.615665133978
0.399608932798
0.484648027958
0.574636646509
77.897
batch start
#iterations: 220
currently lose_sum: 84.87512063980103
time_elpased: 2.802
batch start
#iterations: 221
currently lose_sum: 84.60104471445084
time_elpased: 2.772
batch start
#iterations: 222
currently lose_sum: 85.08132344484329
time_elpased: 2.762
batch start
#iterations: 223
currently lose_sum: 84.97762459516525
time_elpased: 2.74
batch start
#iterations: 224
currently lose_sum: 84.8448434472084
time_elpased: 2.744
batch start
#iterations: 225
currently lose_sum: 85.55679869651794
time_elpased: 2.726
batch start
#iterations: 226
currently lose_sum: 84.21495926380157
time_elpased: 2.755
batch start
#iterations: 227
currently lose_sum: 84.52937138080597
time_elpased: 2.724
batch start
#iterations: 228
currently lose_sum: 84.52628302574158
time_elpased: 2.762
batch start
#iterations: 229
currently lose_sum: 84.17291069030762
time_elpased: 2.786
batch start
#iterations: 230
currently lose_sum: 84.16840711236
time_elpased: 2.776
batch start
#iterations: 231
currently lose_sum: 84.53505891561508
time_elpased: 2.736
batch start
#iterations: 232
currently lose_sum: 84.09718936681747
time_elpased: 2.758
batch start
#iterations: 233
currently lose_sum: 83.8858430981636
time_elpased: 2.811
batch start
#iterations: 234
currently lose_sum: 83.65038323402405
time_elpased: 2.802
batch start
#iterations: 235
currently lose_sum: 84.25277972221375
time_elpased: 2.775
batch start
#iterations: 236
currently lose_sum: 83.95016431808472
time_elpased: 2.789
batch start
#iterations: 237
currently lose_sum: 84.30865776538849
time_elpased: 2.78
batch start
#iterations: 238
currently lose_sum: 83.41649132966995
time_elpased: 2.76
batch start
#iterations: 239
currently lose_sum: 83.75588607788086
time_elpased: 2.77
start validation test
0.576958762887
0.617236024845
0.409076875579
0.492046790865
0.577253505434
79.491
batch start
#iterations: 240
currently lose_sum: 83.03367573022842
time_elpased: 2.779
batch start
#iterations: 241
currently lose_sum: 82.93844050168991
time_elpased: 2.738
batch start
#iterations: 242
currently lose_sum: 83.24304872751236
time_elpased: 2.756
batch start
#iterations: 243
currently lose_sum: 82.98724594712257
time_elpased: 2.827
batch start
#iterations: 244
currently lose_sum: 84.35583883523941
time_elpased: 2.789
batch start
#iterations: 245
currently lose_sum: 82.69603627920151
time_elpased: 2.814
batch start
#iterations: 246
currently lose_sum: 83.23711091279984
time_elpased: 2.82
batch start
#iterations: 247
currently lose_sum: 82.90618360042572
time_elpased: 2.8
batch start
#iterations: 248
currently lose_sum: 82.24982222914696
time_elpased: 2.815
batch start
#iterations: 249
currently lose_sum: 82.81674844026566
time_elpased: 2.813
batch start
#iterations: 250
currently lose_sum: 82.76111787557602
time_elpased: 2.827
batch start
#iterations: 251
currently lose_sum: 82.5268372297287
time_elpased: 2.85
batch start
#iterations: 252
currently lose_sum: 82.0692195892334
time_elpased: 2.803
batch start
#iterations: 253
currently lose_sum: 81.66466450691223
time_elpased: 2.892
batch start
#iterations: 254
currently lose_sum: 81.73441016674042
time_elpased: 2.858
batch start
#iterations: 255
currently lose_sum: 82.3908423781395
time_elpased: 2.85
batch start
#iterations: 256
currently lose_sum: 82.19703888893127
time_elpased: 2.818
batch start
#iterations: 257
currently lose_sum: 81.80734494328499
time_elpased: 2.804
batch start
#iterations: 258
currently lose_sum: 82.05990242958069
time_elpased: 2.846
batch start
#iterations: 259
currently lose_sum: 81.3731144964695
time_elpased: 2.843
start validation test
0.571701030928
0.621757177447
0.369970155398
0.463900896832
0.572055200595
83.828
batch start
#iterations: 260
currently lose_sum: 81.69154381752014
time_elpased: 2.766
batch start
#iterations: 261
currently lose_sum: 81.50268739461899
time_elpased: 2.862
batch start
#iterations: 262
currently lose_sum: 81.90103268623352
time_elpased: 2.858
batch start
#iterations: 263
currently lose_sum: 81.72290894389153
time_elpased: 2.883
batch start
#iterations: 264
currently lose_sum: 81.40017893910408
time_elpased: 2.877
batch start
#iterations: 265
currently lose_sum: 81.34190392494202
time_elpased: 2.886
batch start
#iterations: 266
currently lose_sum: 80.4318932890892
time_elpased: 2.872
batch start
#iterations: 267
currently lose_sum: 81.53243711590767
time_elpased: 2.826
batch start
#iterations: 268
currently lose_sum: 81.46479228138924
time_elpased: 2.741
batch start
#iterations: 269
currently lose_sum: 81.20566859841347
time_elpased: 2.733
batch start
#iterations: 270
currently lose_sum: 81.02786418795586
time_elpased: 2.746
batch start
#iterations: 271
currently lose_sum: 80.40023601055145
time_elpased: 2.782
batch start
#iterations: 272
currently lose_sum: 81.0655184686184
time_elpased: 2.732
batch start
#iterations: 273
currently lose_sum: 80.55888146162033
time_elpased: 2.879
batch start
#iterations: 274
currently lose_sum: 80.9125691652298
time_elpased: 2.804
batch start
#iterations: 275
currently lose_sum: 80.30787694454193
time_elpased: 2.755
batch start
#iterations: 276
currently lose_sum: 80.1512021124363
time_elpased: 2.784
batch start
#iterations: 277
currently lose_sum: 80.32587784528732
time_elpased: 2.785
batch start
#iterations: 278
currently lose_sum: 80.8235596716404
time_elpased: 2.764
batch start
#iterations: 279
currently lose_sum: 79.46973291039467
time_elpased: 2.806
start validation test
0.572164948454
0.59539517975
0.455078728002
0.515865608959
0.572370511373
85.722
batch start
#iterations: 280
currently lose_sum: 80.07074838876724
time_elpased: 2.77
batch start
#iterations: 281
currently lose_sum: 80.18216559290886
time_elpased: 2.81
batch start
#iterations: 282
currently lose_sum: 79.58132499456406
time_elpased: 2.764
batch start
#iterations: 283
currently lose_sum: 79.70999044179916
time_elpased: 2.779
batch start
#iterations: 284
currently lose_sum: 79.26520419120789
time_elpased: 2.761
batch start
#iterations: 285
currently lose_sum: 80.29109328985214
time_elpased: 2.787
batch start
#iterations: 286
currently lose_sum: 79.82980653643608
time_elpased: 2.758
batch start
#iterations: 287
currently lose_sum: 79.98214650154114
time_elpased: 2.786
batch start
#iterations: 288
currently lose_sum: 79.64971706271172
time_elpased: 2.747
batch start
#iterations: 289
currently lose_sum: 79.12578704953194
time_elpased: 2.785
batch start
#iterations: 290
currently lose_sum: 78.55114758014679
time_elpased: 2.791
batch start
#iterations: 291
currently lose_sum: 79.37339979410172
time_elpased: 2.818
batch start
#iterations: 292
currently lose_sum: 78.15559446811676
time_elpased: 2.789
batch start
#iterations: 293
currently lose_sum: 78.41934624314308
time_elpased: 2.802
batch start
#iterations: 294
currently lose_sum: 79.51225644350052
time_elpased: 2.771
batch start
#iterations: 295
currently lose_sum: 79.20813450217247
time_elpased: 2.827
batch start
#iterations: 296
currently lose_sum: 78.43043285608292
time_elpased: 2.815
batch start
#iterations: 297
currently lose_sum: 78.44991472363472
time_elpased: 2.821
batch start
#iterations: 298
currently lose_sum: 77.95462256669998
time_elpased: 2.783
batch start
#iterations: 299
currently lose_sum: 78.81158915162086
time_elpased: 2.823
start validation test
0.561391752577
0.603637611531
0.36204589894
0.452621421679
0.56174173497
95.361
batch start
#iterations: 300
currently lose_sum: 78.11451426148415
time_elpased: 2.768
batch start
#iterations: 301
currently lose_sum: 78.49916848540306
time_elpased: 2.828
batch start
#iterations: 302
currently lose_sum: 77.3563052713871
time_elpased: 2.802
batch start
#iterations: 303
currently lose_sum: 78.35294985771179
time_elpased: 2.832
batch start
#iterations: 304
currently lose_sum: 77.76767632365227
time_elpased: 2.766
batch start
#iterations: 305
currently lose_sum: 78.10556301474571
time_elpased: 2.851
batch start
#iterations: 306
currently lose_sum: 78.35717010498047
time_elpased: 2.825
batch start
#iterations: 307
currently lose_sum: 77.7592696249485
time_elpased: 2.814
batch start
#iterations: 308
currently lose_sum: 77.14602568745613
time_elpased: 2.782
batch start
#iterations: 309
currently lose_sum: 77.63565444946289
time_elpased: 2.808
batch start
#iterations: 310
currently lose_sum: 78.02803960442543
time_elpased: 2.799
batch start
#iterations: 311
currently lose_sum: 76.58833149075508
time_elpased: 2.796
batch start
#iterations: 312
currently lose_sum: 77.08262249827385
time_elpased: 2.785
batch start
#iterations: 313
currently lose_sum: 76.76568216085434
time_elpased: 2.831
batch start
#iterations: 314
currently lose_sum: 77.44890958070755
time_elpased: 2.782
batch start
#iterations: 315
currently lose_sum: 76.64250463247299
time_elpased: 2.804
batch start
#iterations: 316
currently lose_sum: 76.17973947525024
time_elpased: 2.731
batch start
#iterations: 317
currently lose_sum: 76.78444439172745
time_elpased: 2.767
batch start
#iterations: 318
currently lose_sum: 76.61942911148071
time_elpased: 2.722
batch start
#iterations: 319
currently lose_sum: 76.12361359596252
time_elpased: 2.745
start validation test
0.55675257732
0.616264559068
0.304929504991
0.407986230637
0.557194691564
98.172
batch start
#iterations: 320
currently lose_sum: 76.7883571088314
time_elpased: 2.754
batch start
#iterations: 321
currently lose_sum: 76.34629988670349
time_elpased: 2.796
batch start
#iterations: 322
currently lose_sum: 75.41557005047798
time_elpased: 2.731
batch start
#iterations: 323
currently lose_sum: 76.1458072066307
time_elpased: 2.755
batch start
#iterations: 324
currently lose_sum: 76.55660942196846
time_elpased: 2.734
batch start
#iterations: 325
currently lose_sum: 75.78929111361504
time_elpased: 2.807
batch start
#iterations: 326
currently lose_sum: 75.5781517624855
time_elpased: 2.769
batch start
#iterations: 327
currently lose_sum: 75.25482702255249
time_elpased: 2.8
batch start
#iterations: 328
currently lose_sum: 76.93058782815933
time_elpased: 2.795
batch start
#iterations: 329
currently lose_sum: 75.43051162362099
time_elpased: 2.786
batch start
#iterations: 330
currently lose_sum: 75.09522953629494
time_elpased: 2.796
batch start
#iterations: 331
currently lose_sum: 76.16884458065033
time_elpased: 2.8
batch start
#iterations: 332
currently lose_sum: 74.65940946340561
time_elpased: 2.807
batch start
#iterations: 333
currently lose_sum: 75.85655492544174
time_elpased: 2.794
batch start
#iterations: 334
currently lose_sum: 75.85821655392647
time_elpased: 2.813
batch start
#iterations: 335
currently lose_sum: 74.80783024430275
time_elpased: 2.801
batch start
#iterations: 336
currently lose_sum: 74.91742807626724
time_elpased: 2.806
batch start
#iterations: 337
currently lose_sum: 74.61503785848618
time_elpased: 2.799
batch start
#iterations: 338
currently lose_sum: 74.64773747324944
time_elpased: 2.789
batch start
#iterations: 339
currently lose_sum: 75.09172454476357
time_elpased: 2.78
start validation test
0.553350515464
0.608587943848
0.303385818668
0.404917244695
0.553789367044
101.840
batch start
#iterations: 340
currently lose_sum: 74.8656724691391
time_elpased: 2.76
batch start
#iterations: 341
currently lose_sum: 74.99187943339348
time_elpased: 2.771
batch start
#iterations: 342
currently lose_sum: 74.6851897239685
time_elpased: 2.791
batch start
#iterations: 343
currently lose_sum: 74.4097007215023
time_elpased: 2.819
batch start
#iterations: 344
currently lose_sum: 74.07887893915176
time_elpased: 2.817
batch start
#iterations: 345
currently lose_sum: 74.25545051693916
time_elpased: 2.804
batch start
#iterations: 346
currently lose_sum: 74.05425012111664
time_elpased: 2.825
batch start
#iterations: 347
currently lose_sum: 74.12174490094185
time_elpased: 2.836
batch start
#iterations: 348
currently lose_sum: 72.96686601638794
time_elpased: 2.766
batch start
#iterations: 349
currently lose_sum: 73.5321255326271
time_elpased: 2.811
batch start
#iterations: 350
currently lose_sum: 74.34122133255005
time_elpased: 2.836
batch start
#iterations: 351
currently lose_sum: 73.18285578489304
time_elpased: 2.789
batch start
#iterations: 352
currently lose_sum: 73.27276542782784
time_elpased: 2.789
batch start
#iterations: 353
currently lose_sum: 73.55837100744247
time_elpased: 2.824
batch start
#iterations: 354
currently lose_sum: 73.48184579610825
time_elpased: 2.804
batch start
#iterations: 355
currently lose_sum: 72.82219171524048
time_elpased: 2.808
batch start
#iterations: 356
currently lose_sum: 74.3549816608429
time_elpased: 2.787
batch start
#iterations: 357
currently lose_sum: 74.11834228038788
time_elpased: 2.853
batch start
#iterations: 358
currently lose_sum: 73.48867073655128
time_elpased: 2.803
batch start
#iterations: 359
currently lose_sum: 73.54315093159676
time_elpased: 2.797
start validation test
0.561030927835
0.599701145609
0.371719666564
0.458958068615
0.561363292953
105.878
batch start
#iterations: 360
currently lose_sum: 72.9883905351162
time_elpased: 2.788
batch start
#iterations: 361
currently lose_sum: 72.32628071308136
time_elpased: 2.746
batch start
#iterations: 362
currently lose_sum: 72.3310419023037
time_elpased: 2.708
batch start
#iterations: 363
currently lose_sum: 73.21435016393661
time_elpased: 2.763
batch start
#iterations: 364
currently lose_sum: 73.06541022658348
time_elpased: 2.73
batch start
#iterations: 365
currently lose_sum: 73.01207661628723
time_elpased: 2.762
batch start
#iterations: 366
currently lose_sum: 73.64883661270142
time_elpased: 2.749
batch start
#iterations: 367
currently lose_sum: 72.50658711791039
time_elpased: 2.777
batch start
#iterations: 368
currently lose_sum: 73.43749877810478
time_elpased: 2.756
batch start
#iterations: 369
currently lose_sum: 71.8147577047348
time_elpased: 2.741
batch start
#iterations: 370
currently lose_sum: 71.76685643196106
time_elpased: 2.729
batch start
#iterations: 371
currently lose_sum: 71.57401186227798
time_elpased: 2.771
batch start
#iterations: 372
currently lose_sum: 71.64461749792099
time_elpased: 2.739
batch start
#iterations: 373
currently lose_sum: 71.89643123745918
time_elpased: 2.764
batch start
#iterations: 374
currently lose_sum: 73.5944222509861
time_elpased: 2.771
batch start
#iterations: 375
currently lose_sum: 71.8582104742527
time_elpased: 2.761
batch start
#iterations: 376
currently lose_sum: 72.46757850050926
time_elpased: 2.797
batch start
#iterations: 377
currently lose_sum: 72.69006612896919
time_elpased: 2.81
batch start
#iterations: 378
currently lose_sum: 71.72055044770241
time_elpased: 2.805
batch start
#iterations: 379
currently lose_sum: 71.73607659339905
time_elpased: 2.797
start validation test
0.557113402062
0.617850408548
0.30348873109
0.407039337474
0.557558679291
116.420
batch start
#iterations: 380
currently lose_sum: 71.33077397942543
time_elpased: 2.761
batch start
#iterations: 381
currently lose_sum: 70.56373140215874
time_elpased: 2.745
batch start
#iterations: 382
currently lose_sum: 71.2859435081482
time_elpased: 2.749
batch start
#iterations: 383
currently lose_sum: 71.35473984479904
time_elpased: 2.751
batch start
#iterations: 384
currently lose_sum: 71.56090903282166
time_elpased: 2.763
batch start
#iterations: 385
currently lose_sum: 70.87892547249794
time_elpased: 2.73
batch start
#iterations: 386
currently lose_sum: 71.01438018679619
time_elpased: 2.748
batch start
#iterations: 387
currently lose_sum: 71.18523353338242
time_elpased: 2.771
batch start
#iterations: 388
currently lose_sum: 71.73006334900856
time_elpased: 2.763
batch start
#iterations: 389
currently lose_sum: 71.91882693767548
time_elpased: 2.765
batch start
#iterations: 390
currently lose_sum: 71.30296710133553
time_elpased: 2.748
batch start
#iterations: 391
currently lose_sum: 71.70891177654266
time_elpased: 2.791
batch start
#iterations: 392
currently lose_sum: 71.1326809823513
time_elpased: 2.77
batch start
#iterations: 393
currently lose_sum: 70.63902416825294
time_elpased: 2.824
batch start
#iterations: 394
currently lose_sum: 71.64233395457268
time_elpased: 2.805
batch start
#iterations: 395
currently lose_sum: 70.44126626849174
time_elpased: 2.789
batch start
#iterations: 396
currently lose_sum: 69.3589201271534
time_elpased: 2.776
batch start
#iterations: 397
currently lose_sum: 70.19161513447762
time_elpased: 2.799
batch start
#iterations: 398
currently lose_sum: 70.3025464117527
time_elpased: 2.853
batch start
#iterations: 399
currently lose_sum: 69.88665825128555
time_elpased: 2.803
start validation test
0.552371134021
0.602786069652
0.311721724812
0.410934744268
0.552793631176
120.324
acc: 0.608
pre: 0.631
rec: 0.522
F1: 0.571
auc: 0.608
