start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.04744410514832
time_elpased: 2.277
batch start
#iterations: 1
currently lose_sum: 98.91231995820999
time_elpased: 2.253
batch start
#iterations: 2
currently lose_sum: 98.40400207042694
time_elpased: 2.243
batch start
#iterations: 3
currently lose_sum: 97.82031738758087
time_elpased: 2.086
batch start
#iterations: 4
currently lose_sum: 97.58573228120804
time_elpased: 2.182
batch start
#iterations: 5
currently lose_sum: 97.19833314418793
time_elpased: 2.431
batch start
#iterations: 6
currently lose_sum: 96.80096012353897
time_elpased: 2.073
batch start
#iterations: 7
currently lose_sum: 96.5045902132988
time_elpased: 2.215
batch start
#iterations: 8
currently lose_sum: 96.31902951002121
time_elpased: 1.989
batch start
#iterations: 9
currently lose_sum: 96.01909774541855
time_elpased: 2.096
batch start
#iterations: 10
currently lose_sum: 95.65586370229721
time_elpased: 2.015
batch start
#iterations: 11
currently lose_sum: 95.3261046409607
time_elpased: 2.543
batch start
#iterations: 12
currently lose_sum: 94.94654846191406
time_elpased: 2.015
batch start
#iterations: 13
currently lose_sum: 94.84688049554825
time_elpased: 2.246
batch start
#iterations: 14
currently lose_sum: 94.46190172433853
time_elpased: 2.367
batch start
#iterations: 15
currently lose_sum: 94.44696360826492
time_elpased: 2.4
batch start
#iterations: 16
currently lose_sum: 94.5270727276802
time_elpased: 2.151
batch start
#iterations: 17
currently lose_sum: 93.50812083482742
time_elpased: 1.882
batch start
#iterations: 18
currently lose_sum: 93.2855903506279
time_elpased: 1.625
batch start
#iterations: 19
currently lose_sum: 93.45227527618408
time_elpased: 2.054
start validation test
0.658917525773
0.661391087047
0.653699701554
0.657522902541
0.658926686468
60.495
batch start
#iterations: 20
currently lose_sum: 93.29742062091827
time_elpased: 2.24
batch start
#iterations: 21
currently lose_sum: 92.92904388904572
time_elpased: 2.032
batch start
#iterations: 22
currently lose_sum: 93.1220052242279
time_elpased: 2.188
batch start
#iterations: 23
currently lose_sum: 93.09758287668228
time_elpased: 2.229
batch start
#iterations: 24
currently lose_sum: 92.55288982391357
time_elpased: 2.238
batch start
#iterations: 25
currently lose_sum: 92.19331234693527
time_elpased: 1.908
batch start
#iterations: 26
currently lose_sum: 92.43974632024765
time_elpased: 1.967
batch start
#iterations: 27
currently lose_sum: 92.03970408439636
time_elpased: 1.918
batch start
#iterations: 28
currently lose_sum: 91.47243535518646
time_elpased: 2.053
batch start
#iterations: 29
currently lose_sum: 91.61039990186691
time_elpased: 2.27
batch start
#iterations: 30
currently lose_sum: 91.00544953346252
time_elpased: 2.267
batch start
#iterations: 31
currently lose_sum: 91.071648478508
time_elpased: 2.073
batch start
#iterations: 32
currently lose_sum: 91.18392390012741
time_elpased: 1.928
batch start
#iterations: 33
currently lose_sum: 90.9466872215271
time_elpased: 1.884
batch start
#iterations: 34
currently lose_sum: 90.96096509695053
time_elpased: 1.713
batch start
#iterations: 35
currently lose_sum: 90.41002231836319
time_elpased: 2.408
batch start
#iterations: 36
currently lose_sum: 90.40732222795486
time_elpased: 2.093
batch start
#iterations: 37
currently lose_sum: 90.47257101535797
time_elpased: 1.997
batch start
#iterations: 38
currently lose_sum: 89.78078329563141
time_elpased: 2.361
batch start
#iterations: 39
currently lose_sum: 89.97763931751251
time_elpased: 2.317
start validation test
0.659742268041
0.678220086937
0.610167747247
0.642396662874
0.659829303759
59.526
batch start
#iterations: 40
currently lose_sum: 90.10294663906097
time_elpased: 2.362
batch start
#iterations: 41
currently lose_sum: 89.46642822027206
time_elpased: 2.034
batch start
#iterations: 42
currently lose_sum: 89.46466171741486
time_elpased: 1.502
batch start
#iterations: 43
currently lose_sum: 89.95974057912827
time_elpased: 1.458
batch start
#iterations: 44
currently lose_sum: 89.51695734262466
time_elpased: 1.568
batch start
#iterations: 45
currently lose_sum: 89.42270117998123
time_elpased: 1.584
batch start
#iterations: 46
currently lose_sum: 89.20590877532959
time_elpased: 1.742
batch start
#iterations: 47
currently lose_sum: 89.4907591342926
time_elpased: 1.575
batch start
#iterations: 48
currently lose_sum: 89.0303880572319
time_elpased: 1.716
batch start
#iterations: 49
currently lose_sum: 88.68011397123337
time_elpased: 1.491
batch start
#iterations: 50
currently lose_sum: 88.65290105342865
time_elpased: 2.059
batch start
#iterations: 51
currently lose_sum: 88.872523188591
time_elpased: 2.245
batch start
#iterations: 52
currently lose_sum: 88.56966608762741
time_elpased: 1.924
batch start
#iterations: 53
currently lose_sum: 88.12271565198898
time_elpased: 2.031
batch start
#iterations: 54
currently lose_sum: 88.01316338777542
time_elpased: 1.817
batch start
#iterations: 55
currently lose_sum: 88.58688080310822
time_elpased: 1.857
batch start
#iterations: 56
currently lose_sum: 87.71644908189774
time_elpased: 1.939
batch start
#iterations: 57
currently lose_sum: 87.97850221395493
time_elpased: 2.168
batch start
#iterations: 58
currently lose_sum: 87.83693450689316
time_elpased: 1.842
batch start
#iterations: 59
currently lose_sum: 87.84226763248444
time_elpased: 2.091
start validation test
0.654793814433
0.675867691591
0.597200782134
0.634103698847
0.654894927884
59.387
batch start
#iterations: 60
currently lose_sum: 88.51729434728622
time_elpased: 2.028
batch start
#iterations: 61
currently lose_sum: 88.20100432634354
time_elpased: 1.959
batch start
#iterations: 62
currently lose_sum: 87.82042169570923
time_elpased: 2.247
batch start
#iterations: 63
currently lose_sum: 87.23821717500687
time_elpased: 2.004
batch start
#iterations: 64
currently lose_sum: 87.90728896856308
time_elpased: 2.199
batch start
#iterations: 65
currently lose_sum: 87.32485234737396
time_elpased: 2.111
batch start
#iterations: 66
currently lose_sum: 87.2568296790123
time_elpased: 2.06
batch start
#iterations: 67
currently lose_sum: 86.73387157917023
time_elpased: 2.182
batch start
#iterations: 68
currently lose_sum: 86.71157276630402
time_elpased: 2.135
batch start
#iterations: 69
currently lose_sum: 87.06241625547409
time_elpased: 1.855
batch start
#iterations: 70
currently lose_sum: 87.41650980710983
time_elpased: 2.171
batch start
#iterations: 71
currently lose_sum: 86.79965603351593
time_elpased: 1.967
batch start
#iterations: 72
currently lose_sum: 87.03178083896637
time_elpased: 2.036
batch start
#iterations: 73
currently lose_sum: 86.0531821846962
time_elpased: 2.272
batch start
#iterations: 74
currently lose_sum: 85.97899174690247
time_elpased: 1.991
batch start
#iterations: 75
currently lose_sum: 86.53998357057571
time_elpased: 1.823
batch start
#iterations: 76
currently lose_sum: 86.11848199367523
time_elpased: 2.161
batch start
#iterations: 77
currently lose_sum: 86.31144899129868
time_elpased: 2.1
batch start
#iterations: 78
currently lose_sum: 86.08799827098846
time_elpased: 1.99
batch start
#iterations: 79
currently lose_sum: 86.08318048715591
time_elpased: 2.131
start validation test
0.660206185567
0.674405625628
0.62179685088
0.647033626044
0.660273619078
59.200
batch start
#iterations: 80
currently lose_sum: 85.62575989961624
time_elpased: 1.859
batch start
#iterations: 81
currently lose_sum: 86.27942997217178
time_elpased: 2.201
batch start
#iterations: 82
currently lose_sum: 85.98739981651306
time_elpased: 1.973
batch start
#iterations: 83
currently lose_sum: 86.13218480348587
time_elpased: 2.1
batch start
#iterations: 84
currently lose_sum: 85.91849607229233
time_elpased: 2.119
batch start
#iterations: 85
currently lose_sum: 86.02411848306656
time_elpased: 2.221
batch start
#iterations: 86
currently lose_sum: 85.69641190767288
time_elpased: 1.903
batch start
#iterations: 87
currently lose_sum: 85.82797986268997
time_elpased: 1.924
batch start
#iterations: 88
currently lose_sum: 85.09406334161758
time_elpased: 1.866
batch start
#iterations: 89
currently lose_sum: 85.79131799936295
time_elpased: 1.789
batch start
#iterations: 90
currently lose_sum: 84.99226778745651
time_elpased: 1.866
batch start
#iterations: 91
currently lose_sum: 85.54309290647507
time_elpased: 2.056
batch start
#iterations: 92
currently lose_sum: 85.34923511743546
time_elpased: 2.23
batch start
#iterations: 93
currently lose_sum: 84.70808678865433
time_elpased: 2.139
batch start
#iterations: 94
currently lose_sum: 85.60169124603271
time_elpased: 1.676
batch start
#iterations: 95
currently lose_sum: 85.11510318517685
time_elpased: 1.722
batch start
#iterations: 96
currently lose_sum: 85.01038461923599
time_elpased: 1.765
batch start
#iterations: 97
currently lose_sum: 84.37252360582352
time_elpased: 1.779
batch start
#iterations: 98
currently lose_sum: 84.23432207107544
time_elpased: 1.845
batch start
#iterations: 99
currently lose_sum: 85.26473623514175
time_elpased: 1.878
start validation test
0.644948453608
0.677768003016
0.555006689307
0.610274980197
0.645106360248
60.732
batch start
#iterations: 100
currently lose_sum: 84.71914547681808
time_elpased: 2.225
batch start
#iterations: 101
currently lose_sum: 84.70847600698471
time_elpased: 2.008
batch start
#iterations: 102
currently lose_sum: 84.74619507789612
time_elpased: 2.233
batch start
#iterations: 103
currently lose_sum: 84.41765999794006
time_elpased: 2.102
batch start
#iterations: 104
currently lose_sum: 83.65185362100601
time_elpased: 2.151
batch start
#iterations: 105
currently lose_sum: 84.42793583869934
time_elpased: 2.164
batch start
#iterations: 106
currently lose_sum: 84.27383947372437
time_elpased: 2.008
batch start
#iterations: 107
currently lose_sum: 84.69572913646698
time_elpased: 1.817
batch start
#iterations: 108
currently lose_sum: 84.13020625710487
time_elpased: 1.797
batch start
#iterations: 109
currently lose_sum: 84.10709917545319
time_elpased: 1.646
batch start
#iterations: 110
currently lose_sum: 84.20213943719864
time_elpased: 1.483
batch start
#iterations: 111
currently lose_sum: 84.38985788822174
time_elpased: 1.471
batch start
#iterations: 112
currently lose_sum: 83.69326370954514
time_elpased: 2.042
batch start
#iterations: 113
currently lose_sum: 83.43794667720795
time_elpased: 2.221
batch start
#iterations: 114
currently lose_sum: 83.77010142803192
time_elpased: 2.021
batch start
#iterations: 115
currently lose_sum: 83.87343031167984
time_elpased: 1.942
batch start
#iterations: 116
currently lose_sum: 84.08017751574516
time_elpased: 2.151
batch start
#iterations: 117
currently lose_sum: 83.5321062207222
time_elpased: 2.051
batch start
#iterations: 118
currently lose_sum: 83.4159440100193
time_elpased: 2.234
batch start
#iterations: 119
currently lose_sum: 83.2614021897316
time_elpased: 1.902
start validation test
0.649175257732
0.680069281207
0.565709581146
0.617640449438
0.649321794601
60.154
batch start
#iterations: 120
currently lose_sum: 83.21108746528625
time_elpased: 2.171
batch start
#iterations: 121
currently lose_sum: 84.27046328783035
time_elpased: 2.035
batch start
#iterations: 122
currently lose_sum: 83.93058401346207
time_elpased: 2.218
batch start
#iterations: 123
currently lose_sum: 83.4657946228981
time_elpased: 2.173
batch start
#iterations: 124
currently lose_sum: 82.87815368175507
time_elpased: 1.908
batch start
#iterations: 125
currently lose_sum: 82.72982907295227
time_elpased: 2.006
batch start
#iterations: 126
currently lose_sum: 83.05069679021835
time_elpased: 1.876
batch start
#iterations: 127
currently lose_sum: 83.15412086248398
time_elpased: 1.9
batch start
#iterations: 128
currently lose_sum: 82.72277987003326
time_elpased: 1.994
batch start
#iterations: 129
currently lose_sum: 82.42594635486603
time_elpased: 2.193
batch start
#iterations: 130
currently lose_sum: 83.17359435558319
time_elpased: 1.86
batch start
#iterations: 131
currently lose_sum: 83.17116415500641
time_elpased: 2.014
batch start
#iterations: 132
currently lose_sum: 83.8780809044838
time_elpased: 2.076
batch start
#iterations: 133
currently lose_sum: 82.29099977016449
time_elpased: 1.853
batch start
#iterations: 134
currently lose_sum: 82.20111733675003
time_elpased: 1.909
batch start
#iterations: 135
currently lose_sum: 82.57716780900955
time_elpased: 2.292
batch start
#iterations: 136
currently lose_sum: 83.11484706401825
time_elpased: 2.233
batch start
#iterations: 137
currently lose_sum: 82.7386845946312
time_elpased: 1.989
batch start
#iterations: 138
currently lose_sum: 82.29959854483604
time_elpased: 1.984
batch start
#iterations: 139
currently lose_sum: 82.03045311570168
time_elpased: 1.892
start validation test
0.639948453608
0.675128205128
0.541936811773
0.601244505338
0.640120528163
60.920
batch start
#iterations: 140
currently lose_sum: 82.09787288308144
time_elpased: 1.946
batch start
#iterations: 141
currently lose_sum: 82.4363305568695
time_elpased: 2.066
batch start
#iterations: 142
currently lose_sum: 83.17002874612808
time_elpased: 1.664
batch start
#iterations: 143
currently lose_sum: 82.36662292480469
time_elpased: 1.582
batch start
#iterations: 144
currently lose_sum: 82.4634598493576
time_elpased: 1.65
batch start
#iterations: 145
currently lose_sum: 82.05176630616188
time_elpased: 1.68
batch start
#iterations: 146
currently lose_sum: 82.39610642194748
time_elpased: 2.042
batch start
#iterations: 147
currently lose_sum: 82.03590685129166
time_elpased: 2.012
batch start
#iterations: 148
currently lose_sum: 82.43639534711838
time_elpased: 2.154
batch start
#iterations: 149
currently lose_sum: 82.33676904439926
time_elpased: 2.222
batch start
#iterations: 150
currently lose_sum: 82.27171862125397
time_elpased: 1.887
batch start
#iterations: 151
currently lose_sum: 82.50905621051788
time_elpased: 2.136
batch start
#iterations: 152
currently lose_sum: 81.97968384623528
time_elpased: 2.058
batch start
#iterations: 153
currently lose_sum: 81.9996428489685
time_elpased: 2.184
batch start
#iterations: 154
currently lose_sum: 81.69874089956284
time_elpased: 1.72
batch start
#iterations: 155
currently lose_sum: 82.18651622533798
time_elpased: 1.98
batch start
#iterations: 156
currently lose_sum: 82.36488148570061
time_elpased: 2.188
batch start
#iterations: 157
currently lose_sum: 82.18675833940506
time_elpased: 2.121
batch start
#iterations: 158
currently lose_sum: 82.01739236712456
time_elpased: 1.474
batch start
#iterations: 159
currently lose_sum: 82.19426834583282
time_elpased: 1.533
start validation test
0.639587628866
0.685147438511
0.518884429351
0.590536425392
0.63979954195
61.288
batch start
#iterations: 160
currently lose_sum: 81.74299561977386
time_elpased: 1.638
batch start
#iterations: 161
currently lose_sum: 82.07303756475449
time_elpased: 1.758
batch start
#iterations: 162
currently lose_sum: 82.0307514667511
time_elpased: 1.625
batch start
#iterations: 163
currently lose_sum: 81.37948650121689
time_elpased: 2.262
batch start
#iterations: 164
currently lose_sum: 81.38483920693398
time_elpased: 2.222
batch start
#iterations: 165
currently lose_sum: 81.25028240680695
time_elpased: 2.22
batch start
#iterations: 166
currently lose_sum: 80.87103381752968
time_elpased: 2.189
batch start
#iterations: 167
currently lose_sum: 81.8783632516861
time_elpased: 2.188
batch start
#iterations: 168
currently lose_sum: 81.772301197052
time_elpased: 2.203
batch start
#iterations: 169
currently lose_sum: 81.00496065616608
time_elpased: 2.293
batch start
#iterations: 170
currently lose_sum: 81.44888544082642
time_elpased: 2.065
batch start
#iterations: 171
currently lose_sum: 80.55017611384392
time_elpased: 2.155
batch start
#iterations: 172
currently lose_sum: 81.2198517024517
time_elpased: 2.268
batch start
#iterations: 173
currently lose_sum: 80.82360607385635
time_elpased: 2.175
batch start
#iterations: 174
currently lose_sum: 80.78817111253738
time_elpased: 1.995
batch start
#iterations: 175
currently lose_sum: 81.48419606685638
time_elpased: 2.178
batch start
#iterations: 176
currently lose_sum: 81.6163837313652
time_elpased: 2.201
batch start
#iterations: 177
currently lose_sum: 81.16958612203598
time_elpased: 2.235
batch start
#iterations: 178
currently lose_sum: 81.0947425365448
time_elpased: 2.151
batch start
#iterations: 179
currently lose_sum: 80.4026418030262
time_elpased: 1.901
start validation test
0.641701030928
0.671972146232
0.556138725944
0.608592826173
0.641851248751
61.321
batch start
#iterations: 180
currently lose_sum: 81.41084897518158
time_elpased: 2.044
batch start
#iterations: 181
currently lose_sum: 80.3683512210846
time_elpased: 2.23
batch start
#iterations: 182
currently lose_sum: 81.19125846028328
time_elpased: 1.73
batch start
#iterations: 183
currently lose_sum: 81.23130393028259
time_elpased: 2.12
batch start
#iterations: 184
currently lose_sum: 80.95474034547806
time_elpased: 1.986
batch start
#iterations: 185
currently lose_sum: 81.10483691096306
time_elpased: 2.196
batch start
#iterations: 186
currently lose_sum: 80.40044265985489
time_elpased: 2.082
batch start
#iterations: 187
currently lose_sum: 80.75861868262291
time_elpased: 1.839
batch start
#iterations: 188
currently lose_sum: 80.51772511005402
time_elpased: 2.133
batch start
#iterations: 189
currently lose_sum: 81.12920743227005
time_elpased: 2.201
batch start
#iterations: 190
currently lose_sum: 80.07724240422249
time_elpased: 1.922
batch start
#iterations: 191
currently lose_sum: 80.14863893389702
time_elpased: 1.989
batch start
#iterations: 192
currently lose_sum: 80.90514075756073
time_elpased: 2.11
batch start
#iterations: 193
currently lose_sum: 81.06426766514778
time_elpased: 2.045
batch start
#iterations: 194
currently lose_sum: 80.63887238502502
time_elpased: 2.082
batch start
#iterations: 195
currently lose_sum: 80.5911934375763
time_elpased: 2.297
batch start
#iterations: 196
currently lose_sum: 80.61078351736069
time_elpased: 2.218
batch start
#iterations: 197
currently lose_sum: 80.72456562519073
time_elpased: 2.041
batch start
#iterations: 198
currently lose_sum: 80.2539234161377
time_elpased: 2.086
batch start
#iterations: 199
currently lose_sum: 80.23015862703323
time_elpased: 2.121
start validation test
0.630309278351
0.677351916376
0.500154368632
0.57542031731
0.63053778537
62.727
batch start
#iterations: 200
currently lose_sum: 80.52983802556992
time_elpased: 1.982
batch start
#iterations: 201
currently lose_sum: 80.68323785066605
time_elpased: 1.754
batch start
#iterations: 202
currently lose_sum: 80.4784649014473
time_elpased: 1.685
batch start
#iterations: 203
currently lose_sum: 80.71810001134872
time_elpased: 2.147
batch start
#iterations: 204
currently lose_sum: 80.02013793587685
time_elpased: 2.057
batch start
#iterations: 205
currently lose_sum: 80.61728763580322
time_elpased: 2.28
batch start
#iterations: 206
currently lose_sum: 80.03123474121094
time_elpased: 2.29
batch start
#iterations: 207
currently lose_sum: 79.40515297651291
time_elpased: 2.174
batch start
#iterations: 208
currently lose_sum: 80.09143224358559
time_elpased: 2.132
batch start
#iterations: 209
currently lose_sum: 79.88622689247131
time_elpased: 2.164
batch start
#iterations: 210
currently lose_sum: 79.88254290819168
time_elpased: 2.105
batch start
#iterations: 211
currently lose_sum: 80.06711420416832
time_elpased: 2.156
batch start
#iterations: 212
currently lose_sum: 79.94358330965042
time_elpased: 2.058
batch start
#iterations: 213
currently lose_sum: 79.49639731645584
time_elpased: 2.082
batch start
#iterations: 214
currently lose_sum: 80.11828589439392
time_elpased: 2.065
batch start
#iterations: 215
currently lose_sum: 80.00077202916145
time_elpased: 1.892
batch start
#iterations: 216
currently lose_sum: 79.93996223807335
time_elpased: 2.17
batch start
#iterations: 217
currently lose_sum: 80.25600290298462
time_elpased: 2.066
batch start
#iterations: 218
currently lose_sum: 79.80737519264221
time_elpased: 2.119
batch start
#iterations: 219
currently lose_sum: 79.56753894686699
time_elpased: 1.964
start validation test
0.63324742268
0.673791076676
0.519090254194
0.586409347207
0.633447843197
62.275
batch start
#iterations: 220
currently lose_sum: 80.17967587709427
time_elpased: 2.32
batch start
#iterations: 221
currently lose_sum: 79.86667859554291
time_elpased: 2.203
batch start
#iterations: 222
currently lose_sum: 80.07444030046463
time_elpased: 2.097
batch start
#iterations: 223
currently lose_sum: 79.69599306583405
time_elpased: 3.513
batch start
#iterations: 224
currently lose_sum: 79.6338757276535
time_elpased: 2.004
batch start
#iterations: 225
currently lose_sum: 80.32317700982094
time_elpased: 2.097
batch start
#iterations: 226
currently lose_sum: 79.02087563276291
time_elpased: 2.03
batch start
#iterations: 227
currently lose_sum: 79.50588572025299
time_elpased: 1.989
batch start
#iterations: 228
currently lose_sum: 79.3645989894867
time_elpased: 1.911
batch start
#iterations: 229
currently lose_sum: 79.04905381798744
time_elpased: 1.881
batch start
#iterations: 230
currently lose_sum: 79.87132108211517
time_elpased: 2.077
batch start
#iterations: 231
currently lose_sum: 79.46197938919067
time_elpased: 2.217
batch start
#iterations: 232
currently lose_sum: 79.38070738315582
time_elpased: 2.309
batch start
#iterations: 233
currently lose_sum: 79.14144769310951
time_elpased: 2.109
batch start
#iterations: 234
currently lose_sum: 79.09949487447739
time_elpased: 2.198
batch start
#iterations: 235
currently lose_sum: 79.53342372179031
time_elpased: 2.139
batch start
#iterations: 236
currently lose_sum: 79.57400760054588
time_elpased: 2.126
batch start
#iterations: 237
currently lose_sum: 79.3428880572319
time_elpased: 2.098
batch start
#iterations: 238
currently lose_sum: 79.66467216610909
time_elpased: 2.202
batch start
#iterations: 239
currently lose_sum: 79.14807176589966
time_elpased: 2.036
start validation test
0.624742268041
0.680011818585
0.473705876299
0.558413199078
0.625007435723
63.613
batch start
#iterations: 240
currently lose_sum: 79.32205644249916
time_elpased: 1.665
batch start
#iterations: 241
currently lose_sum: 80.06569236516953
time_elpased: 1.985
batch start
#iterations: 242
currently lose_sum: 79.80297577381134
time_elpased: 1.774
batch start
#iterations: 243
currently lose_sum: 79.92714014649391
time_elpased: 2.187
batch start
#iterations: 244
currently lose_sum: 79.74397277832031
time_elpased: 2.025
batch start
#iterations: 245
currently lose_sum: 79.36633238196373
time_elpased: 2.189
batch start
#iterations: 246
currently lose_sum: 79.0318555533886
time_elpased: 2.191
batch start
#iterations: 247
currently lose_sum: 78.9921370446682
time_elpased: 2.166
batch start
#iterations: 248
currently lose_sum: 78.98416590690613
time_elpased: 1.937
batch start
#iterations: 249
currently lose_sum: 79.33377432823181
time_elpased: 2.204
batch start
#iterations: 250
currently lose_sum: 79.09530490636826
time_elpased: 2.031
batch start
#iterations: 251
currently lose_sum: 78.89612373709679
time_elpased: 2.074
batch start
#iterations: 252
currently lose_sum: 78.96150109171867
time_elpased: 2.086
batch start
#iterations: 253
currently lose_sum: 78.90553280711174
time_elpased: 2.206
batch start
#iterations: 254
currently lose_sum: 79.23148956894875
time_elpased: 1.83
batch start
#iterations: 255
currently lose_sum: 78.77026185393333
time_elpased: 1.91
batch start
#iterations: 256
currently lose_sum: 78.21448591351509
time_elpased: 2.015
batch start
#iterations: 257
currently lose_sum: 79.31037068367004
time_elpased: 2.138
batch start
#iterations: 258
currently lose_sum: 78.72945752739906
time_elpased: 2.254
batch start
#iterations: 259
currently lose_sum: 79.17586454749107
time_elpased: 2.088
start validation test
0.628608247423
0.673289183223
0.502212617063
0.575302092543
0.628830154447
63.145
batch start
#iterations: 260
currently lose_sum: 78.81810107827187
time_elpased: 1.922
batch start
#iterations: 261
currently lose_sum: 78.91594612598419
time_elpased: 2.261
batch start
#iterations: 262
currently lose_sum: 78.8770706653595
time_elpased: 2.103
batch start
#iterations: 263
currently lose_sum: 78.9019296169281
time_elpased: 2.089
batch start
#iterations: 264
currently lose_sum: 79.05411732196808
time_elpased: 1.847
batch start
#iterations: 265
currently lose_sum: 79.0152422785759
time_elpased: 2.009
batch start
#iterations: 266
currently lose_sum: 78.67578461766243
time_elpased: 2.168
batch start
#iterations: 267
currently lose_sum: 79.41773879528046
time_elpased: 2.32
batch start
#iterations: 268
currently lose_sum: 78.22937768697739
time_elpased: 1.724
batch start
#iterations: 269
currently lose_sum: 78.74771332740784
time_elpased: 1.642
batch start
#iterations: 270
currently lose_sum: 78.1813263297081
time_elpased: 1.495
batch start
#iterations: 271
currently lose_sum: 78.40543776750565
time_elpased: 1.628
batch start
#iterations: 272
currently lose_sum: 78.66959172487259
time_elpased: 1.465
batch start
#iterations: 273
currently lose_sum: 78.29386913776398
time_elpased: 1.457
batch start
#iterations: 274
currently lose_sum: 78.7484258711338
time_elpased: 1.779
batch start
#iterations: 275
currently lose_sum: 78.81915718317032
time_elpased: 1.632
batch start
#iterations: 276
currently lose_sum: 78.51496517658234
time_elpased: 1.429
batch start
#iterations: 277
currently lose_sum: 78.97176343202591
time_elpased: 1.459
batch start
#iterations: 278
currently lose_sum: 78.05886900424957
time_elpased: 1.794
batch start
#iterations: 279
currently lose_sum: 78.77017989754677
time_elpased: 2.186
start validation test
0.626391752577
0.684006558354
0.472265102398
0.558748325825
0.626662345684
63.964
batch start
#iterations: 280
currently lose_sum: 78.20772165060043
time_elpased: 2.083
batch start
#iterations: 281
currently lose_sum: 78.62448865175247
time_elpased: 1.928
batch start
#iterations: 282
currently lose_sum: 78.61305975914001
time_elpased: 2.17
batch start
#iterations: 283
currently lose_sum: 78.03092133998871
time_elpased: 2.058
batch start
#iterations: 284
currently lose_sum: 78.21043992042542
time_elpased: 2.426
batch start
#iterations: 285
currently lose_sum: 78.00548884272575
time_elpased: 2.147
batch start
#iterations: 286
currently lose_sum: 77.86713460087776
time_elpased: 1.927
batch start
#iterations: 287
currently lose_sum: 78.44745033979416
time_elpased: 1.971
batch start
#iterations: 288
currently lose_sum: 78.06256169080734
time_elpased: 2.147
batch start
#iterations: 289
currently lose_sum: 77.87241843342781
time_elpased: 2.238
batch start
#iterations: 290
currently lose_sum: 78.31664282083511
time_elpased: 2.155
batch start
#iterations: 291
currently lose_sum: 78.15146291255951
time_elpased: 2.219
batch start
#iterations: 292
currently lose_sum: 77.72872191667557
time_elpased: 2.186
batch start
#iterations: 293
currently lose_sum: 79.01252567768097
time_elpased: 2.295
batch start
#iterations: 294
currently lose_sum: 77.56853955984116
time_elpased: 2.247
batch start
#iterations: 295
currently lose_sum: 77.14487826824188
time_elpased: 1.957
batch start
#iterations: 296
currently lose_sum: 77.43017640709877
time_elpased: 2.041
batch start
#iterations: 297
currently lose_sum: 78.11799600720406
time_elpased: 1.978
batch start
#iterations: 298
currently lose_sum: 78.1098667383194
time_elpased: 2.258
batch start
#iterations: 299
currently lose_sum: 77.35562682151794
time_elpased: 2.272
start validation test
0.62381443299
0.685023711182
0.460841823608
0.551002830073
0.624100556542
64.460
batch start
#iterations: 300
currently lose_sum: 77.79194214940071
time_elpased: 2.027
batch start
#iterations: 301
currently lose_sum: 77.76785233616829
time_elpased: 2.086
batch start
#iterations: 302
currently lose_sum: 77.79016515612602
time_elpased: 2.127
batch start
#iterations: 303
currently lose_sum: 77.92670798301697
time_elpased: 2.038
batch start
#iterations: 304
currently lose_sum: 78.07027715444565
time_elpased: 2.021
batch start
#iterations: 305
currently lose_sum: 78.02648493647575
time_elpased: 2.067
batch start
#iterations: 306
currently lose_sum: 78.0305235683918
time_elpased: 2.185
batch start
#iterations: 307
currently lose_sum: 77.20464608073235
time_elpased: 2.167
batch start
#iterations: 308
currently lose_sum: 77.960678845644
time_elpased: 2.27
batch start
#iterations: 309
currently lose_sum: 77.68026486039162
time_elpased: 2.303
batch start
#iterations: 310
currently lose_sum: 78.08775091171265
time_elpased: 1.969
batch start
#iterations: 311
currently lose_sum: 77.13138234615326
time_elpased: 2.111
batch start
#iterations: 312
currently lose_sum: 77.47624370455742
time_elpased: 2.14
batch start
#iterations: 313
currently lose_sum: 77.3940671980381
time_elpased: 2.065
batch start
#iterations: 314
currently lose_sum: 77.75986570119858
time_elpased: 2.086
batch start
#iterations: 315
currently lose_sum: 77.1521045267582
time_elpased: 1.629
batch start
#iterations: 316
currently lose_sum: 77.62222620844841
time_elpased: 2.091
batch start
#iterations: 317
currently lose_sum: 78.12429669499397
time_elpased: 2.156
batch start
#iterations: 318
currently lose_sum: 77.24687930941582
time_elpased: 2.045
batch start
#iterations: 319
currently lose_sum: 77.17625886201859
time_elpased: 2.164
start validation test
0.62706185567
0.674297752809
0.494082535762
0.570291619647
0.627295321377
63.602
batch start
#iterations: 320
currently lose_sum: 76.90555268526077
time_elpased: 2.184
batch start
#iterations: 321
currently lose_sum: 77.463284522295
time_elpased: 2.205
batch start
#iterations: 322
currently lose_sum: 78.03219509124756
time_elpased: 2.07
batch start
#iterations: 323
currently lose_sum: 77.66689819097519
time_elpased: 2.249
batch start
#iterations: 324
currently lose_sum: 77.47627228498459
time_elpased: 2.034
batch start
#iterations: 325
currently lose_sum: 78.30646002292633
time_elpased: 2.116
batch start
#iterations: 326
currently lose_sum: 77.66138336062431
time_elpased: 2.277
batch start
#iterations: 327
currently lose_sum: 76.98318690061569
time_elpased: 1.988
batch start
#iterations: 328
currently lose_sum: 78.03625902533531
time_elpased: 1.742
batch start
#iterations: 329
currently lose_sum: 77.81774926185608
time_elpased: 1.467
batch start
#iterations: 330
currently lose_sum: 77.32846981287003
time_elpased: 1.497
batch start
#iterations: 331
currently lose_sum: 77.96803525090218
time_elpased: 1.544
batch start
#iterations: 332
currently lose_sum: 77.29343259334564
time_elpased: 1.706
batch start
#iterations: 333
currently lose_sum: 77.07320919632912
time_elpased: 2.015
batch start
#iterations: 334
currently lose_sum: 77.54443258047104
time_elpased: 2.24
batch start
#iterations: 335
currently lose_sum: 76.570472240448
time_elpased: 2.327
batch start
#iterations: 336
currently lose_sum: 77.37192761898041
time_elpased: 2.157
batch start
#iterations: 337
currently lose_sum: 76.85285204648972
time_elpased: 2.115
batch start
#iterations: 338
currently lose_sum: 76.91223073005676
time_elpased: 2.07
batch start
#iterations: 339
currently lose_sum: 77.15221205353737
time_elpased: 2.482
start validation test
0.627628865979
0.673970690858
0.496964083565
0.572088615093
0.627858268159
63.903
batch start
#iterations: 340
currently lose_sum: 76.84378251433372
time_elpased: 2.056
batch start
#iterations: 341
currently lose_sum: 77.89529997110367
time_elpased: 1.674
batch start
#iterations: 342
currently lose_sum: 77.81202065944672
time_elpased: 1.551
batch start
#iterations: 343
currently lose_sum: 77.35758200287819
time_elpased: 1.629
batch start
#iterations: 344
currently lose_sum: 77.31619617342949
time_elpased: 1.726
batch start
#iterations: 345
currently lose_sum: 77.41967707872391
time_elpased: 2.112
batch start
#iterations: 346
currently lose_sum: 76.77054131031036
time_elpased: 2.203
batch start
#iterations: 347
currently lose_sum: 76.93968600034714
time_elpased: 2.158
batch start
#iterations: 348
currently lose_sum: 77.51570934057236
time_elpased: 2.117
batch start
#iterations: 349
currently lose_sum: 77.80692315101624
time_elpased: 1.723
batch start
#iterations: 350
currently lose_sum: 77.0900784432888
time_elpased: 2.102
batch start
#iterations: 351
currently lose_sum: 77.0893343091011
time_elpased: 2.146
batch start
#iterations: 352
currently lose_sum: 76.46062672138214
time_elpased: 2.17
batch start
#iterations: 353
currently lose_sum: 77.29987746477127
time_elpased: 2.212
batch start
#iterations: 354
currently lose_sum: 77.03010281920433
time_elpased: 2.105
batch start
#iterations: 355
currently lose_sum: 76.72642534971237
time_elpased: 2.196
batch start
#iterations: 356
currently lose_sum: 77.1520345211029
time_elpased: 2.193
batch start
#iterations: 357
currently lose_sum: 76.71984332799911
time_elpased: 2.268
batch start
#iterations: 358
currently lose_sum: 76.4588366150856
time_elpased: 2.177
batch start
#iterations: 359
currently lose_sum: 77.0800371170044
time_elpased: 2.06
start validation test
0.623865979381
0.673750717978
0.482865081815
0.562556201667
0.624113528205
64.580
batch start
#iterations: 360
currently lose_sum: 77.6430242061615
time_elpased: 1.898
batch start
#iterations: 361
currently lose_sum: 77.30569696426392
time_elpased: 2.163
batch start
#iterations: 362
currently lose_sum: 77.2039498090744
time_elpased: 2.109
batch start
#iterations: 363
currently lose_sum: 77.30307820439339
time_elpased: 1.903
batch start
#iterations: 364
currently lose_sum: 76.08688390254974
time_elpased: 2.169
batch start
#iterations: 365
currently lose_sum: 76.65424156188965
time_elpased: 2.028
batch start
#iterations: 366
currently lose_sum: 76.74181824922562
time_elpased: 2.017
batch start
#iterations: 367
currently lose_sum: 76.0760857462883
time_elpased: 2.189
batch start
#iterations: 368
currently lose_sum: 76.41567489504814
time_elpased: 2.196
batch start
#iterations: 369
currently lose_sum: 76.73988452553749
time_elpased: 2.139
batch start
#iterations: 370
currently lose_sum: 77.07949498295784
time_elpased: 2.201
batch start
#iterations: 371
currently lose_sum: 76.23876890540123
time_elpased: 2.119
batch start
#iterations: 372
currently lose_sum: 76.7999795973301
time_elpased: 1.908
batch start
#iterations: 373
currently lose_sum: 76.25231942534447
time_elpased: 1.878
batch start
#iterations: 374
currently lose_sum: 77.04450872540474
time_elpased: 1.598
batch start
#iterations: 375
currently lose_sum: 76.69574102759361
time_elpased: 2.197
batch start
#iterations: 376
currently lose_sum: 76.96639373898506
time_elpased: 2.135
batch start
#iterations: 377
currently lose_sum: 77.32700169086456
time_elpased: 2.235
batch start
#iterations: 378
currently lose_sum: 76.34790882468224
time_elpased: 2.216
batch start
#iterations: 379
currently lose_sum: 76.86786365509033
time_elpased: 1.945
start validation test
0.628711340206
0.675607711651
0.497684470516
0.573155555556
0.628941378085
64.278
batch start
#iterations: 380
currently lose_sum: 76.872878074646
time_elpased: 2.128
batch start
#iterations: 381
currently lose_sum: 76.82501357793808
time_elpased: 1.995
batch start
#iterations: 382
currently lose_sum: 76.71280494332314
time_elpased: 1.915
batch start
#iterations: 383
currently lose_sum: 76.00873598456383
time_elpased: 1.459
batch start
#iterations: 384
currently lose_sum: 76.61077728867531
time_elpased: 1.447
batch start
#iterations: 385
currently lose_sum: 76.6073434650898
time_elpased: 1.614
batch start
#iterations: 386
currently lose_sum: 76.41347306966782
time_elpased: 1.451
batch start
#iterations: 387
currently lose_sum: 76.733393907547
time_elpased: 1.754
batch start
#iterations: 388
currently lose_sum: 76.67613962292671
time_elpased: 2.241
batch start
#iterations: 389
currently lose_sum: 77.3857179582119
time_elpased: 2.357
batch start
#iterations: 390
currently lose_sum: 76.5635130405426
time_elpased: 2.198
batch start
#iterations: 391
currently lose_sum: 76.74221980571747
time_elpased: 2.063
batch start
#iterations: 392
currently lose_sum: 77.04382824897766
time_elpased: 1.829
batch start
#iterations: 393
currently lose_sum: 76.2958348095417
time_elpased: 1.459
batch start
#iterations: 394
currently lose_sum: 76.26942270994186
time_elpased: 2.262
batch start
#iterations: 395
currently lose_sum: 76.31603783369064
time_elpased: 2.284
batch start
#iterations: 396
currently lose_sum: 76.23274010419846
time_elpased: 2.324
batch start
#iterations: 397
currently lose_sum: 76.68642577528954
time_elpased: 2.455
batch start
#iterations: 398
currently lose_sum: 76.58575391769409
time_elpased: 2.38
batch start
#iterations: 399
currently lose_sum: 76.05024459958076
time_elpased: 2.333
start validation test
0.612989690722
0.684791701523
0.421220541319
0.521600611699
0.613326371042
66.629
acc: 0.647
pre: 0.662
rec: 0.604
F1: 0.631
auc: 0.647
