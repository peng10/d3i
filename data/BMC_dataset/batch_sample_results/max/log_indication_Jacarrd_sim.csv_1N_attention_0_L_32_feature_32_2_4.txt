start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 99.82121258974075
time_elpased: 2.205
batch start
#iterations: 1
currently lose_sum: 98.96404367685318
time_elpased: 2.263
batch start
#iterations: 2
currently lose_sum: 98.44065719842911
time_elpased: 2.196
batch start
#iterations: 3
currently lose_sum: 97.78910410404205
time_elpased: 2.15
batch start
#iterations: 4
currently lose_sum: 97.45046001672745
time_elpased: 2.129
batch start
#iterations: 5
currently lose_sum: 96.99688839912415
time_elpased: 2.064
batch start
#iterations: 6
currently lose_sum: 96.29922747612
time_elpased: 1.981
batch start
#iterations: 7
currently lose_sum: 95.93004304170609
time_elpased: 2.177
batch start
#iterations: 8
currently lose_sum: 95.60343039035797
time_elpased: 2.254
batch start
#iterations: 9
currently lose_sum: 95.28751409053802
time_elpased: 2.258
batch start
#iterations: 10
currently lose_sum: 94.66821545362473
time_elpased: 2.222
batch start
#iterations: 11
currently lose_sum: 94.56742459535599
time_elpased: 2.25
batch start
#iterations: 12
currently lose_sum: 93.93217200040817
time_elpased: 2.259
batch start
#iterations: 13
currently lose_sum: 93.9246791601181
time_elpased: 2.027
batch start
#iterations: 14
currently lose_sum: 93.4276596903801
time_elpased: 2.234
batch start
#iterations: 15
currently lose_sum: 93.37926286458969
time_elpased: 1.961
batch start
#iterations: 16
currently lose_sum: 93.22092360258102
time_elpased: 2.292
batch start
#iterations: 17
currently lose_sum: 92.34353476762772
time_elpased: 2.405
batch start
#iterations: 18
currently lose_sum: 91.86432152986526
time_elpased: 2.194
batch start
#iterations: 19
currently lose_sum: 92.28080731630325
time_elpased: 2.223
start validation test
0.650051546392
0.665199729181
0.606668724915
0.634587437429
0.650127711626
60.138
batch start
#iterations: 20
currently lose_sum: 91.99805760383606
time_elpased: 2.25
batch start
#iterations: 21
currently lose_sum: 91.70108669996262
time_elpased: 2.168
batch start
#iterations: 22
currently lose_sum: 91.59451085329056
time_elpased: 1.981
batch start
#iterations: 23
currently lose_sum: 91.70700192451477
time_elpased: 1.851
batch start
#iterations: 24
currently lose_sum: 91.13043361902237
time_elpased: 2.339
batch start
#iterations: 25
currently lose_sum: 90.50619626045227
time_elpased: 2.132
batch start
#iterations: 26
currently lose_sum: 91.13136553764343
time_elpased: 2.154
batch start
#iterations: 27
currently lose_sum: 90.46002542972565
time_elpased: 2.288
batch start
#iterations: 28
currently lose_sum: 89.95055514574051
time_elpased: 2.23
batch start
#iterations: 29
currently lose_sum: 89.80126374959946
time_elpased: 2.142
batch start
#iterations: 30
currently lose_sum: 89.35965633392334
time_elpased: 2.003
batch start
#iterations: 31
currently lose_sum: 89.46132040023804
time_elpased: 1.772
batch start
#iterations: 32
currently lose_sum: 89.53323739767075
time_elpased: 2.241
batch start
#iterations: 33
currently lose_sum: 89.22975617647171
time_elpased: 2.326
batch start
#iterations: 34
currently lose_sum: 89.21370786428452
time_elpased: 2.202
batch start
#iterations: 35
currently lose_sum: 88.64972358942032
time_elpased: 2.232
batch start
#iterations: 36
currently lose_sum: 88.72199475765228
time_elpased: 2.017
batch start
#iterations: 37
currently lose_sum: 88.85722261667252
time_elpased: 2.119
batch start
#iterations: 38
currently lose_sum: 88.05137121677399
time_elpased: 2.356
batch start
#iterations: 39
currently lose_sum: 88.16042417287827
time_elpased: 2.198
start validation test
0.647731958763
0.666320526134
0.594319234332
0.628263707572
0.647825733039
59.722
batch start
#iterations: 40
currently lose_sum: 88.36638689041138
time_elpased: 2.244
batch start
#iterations: 41
currently lose_sum: 87.99395406246185
time_elpased: 2.045
batch start
#iterations: 42
currently lose_sum: 87.71811336278915
time_elpased: 2.486
batch start
#iterations: 43
currently lose_sum: 88.27738881111145
time_elpased: 2.51
batch start
#iterations: 44
currently lose_sum: 87.93063050508499
time_elpased: 1.972
batch start
#iterations: 45
currently lose_sum: 87.71462261676788
time_elpased: 1.599
batch start
#iterations: 46
currently lose_sum: 87.60564476251602
time_elpased: 1.931
batch start
#iterations: 47
currently lose_sum: 87.57119411230087
time_elpased: 2.096
batch start
#iterations: 48
currently lose_sum: 87.27549630403519
time_elpased: 1.644
batch start
#iterations: 49
currently lose_sum: 87.22892171144485
time_elpased: 2.357
batch start
#iterations: 50
currently lose_sum: 87.55215054750443
time_elpased: 2.096
batch start
#iterations: 51
currently lose_sum: 86.81940633058548
time_elpased: 2.214
batch start
#iterations: 52
currently lose_sum: 86.56831574440002
time_elpased: 2.25
batch start
#iterations: 53
currently lose_sum: 86.6169256567955
time_elpased: 2.31
batch start
#iterations: 54
currently lose_sum: 86.27869260311127
time_elpased: 2.284
batch start
#iterations: 55
currently lose_sum: 86.5716992020607
time_elpased: 1.731
batch start
#iterations: 56
currently lose_sum: 85.81025636196136
time_elpased: 2.181
batch start
#iterations: 57
currently lose_sum: 86.00861138105392
time_elpased: 2.057
batch start
#iterations: 58
currently lose_sum: 86.13755160570145
time_elpased: 1.948
batch start
#iterations: 59
currently lose_sum: 85.98913699388504
time_elpased: 2.225
start validation test
0.632371134021
0.678694870731
0.505197077287
0.579233038348
0.632594407692
61.219
batch start
#iterations: 60
currently lose_sum: 86.51761746406555
time_elpased: 2.239
batch start
#iterations: 61
currently lose_sum: 86.00418555736542
time_elpased: 2.231
batch start
#iterations: 62
currently lose_sum: 86.2829487323761
time_elpased: 2.254
batch start
#iterations: 63
currently lose_sum: 85.15571701526642
time_elpased: 2.256
batch start
#iterations: 64
currently lose_sum: 85.79164749383926
time_elpased: 2.409
batch start
#iterations: 65
currently lose_sum: 85.4711092710495
time_elpased: 2.209
batch start
#iterations: 66
currently lose_sum: 85.56318539381027
time_elpased: 2.164
batch start
#iterations: 67
currently lose_sum: 84.68907803297043
time_elpased: 2.208
batch start
#iterations: 68
currently lose_sum: 85.14575722813606
time_elpased: 2.147
batch start
#iterations: 69
currently lose_sum: 84.79150587320328
time_elpased: 2.043
batch start
#iterations: 70
currently lose_sum: 85.56923866271973
time_elpased: 2.092
batch start
#iterations: 71
currently lose_sum: 84.7665137052536
time_elpased: 2.159
batch start
#iterations: 72
currently lose_sum: 85.23039078712463
time_elpased: 2.119
batch start
#iterations: 73
currently lose_sum: 84.31440964341164
time_elpased: 2.01
batch start
#iterations: 74
currently lose_sum: 84.12649530172348
time_elpased: 2.167
batch start
#iterations: 75
currently lose_sum: 84.65525704622269
time_elpased: 2.135
batch start
#iterations: 76
currently lose_sum: 84.69354075193405
time_elpased: 2.003
batch start
#iterations: 77
currently lose_sum: 84.726287484169
time_elpased: 2.311
batch start
#iterations: 78
currently lose_sum: 84.5581915974617
time_elpased: 2.318
batch start
#iterations: 79
currently lose_sum: 84.39984142780304
time_elpased: 2.293
start validation test
0.658350515464
0.660534247999
0.654008438819
0.65725514531
0.658358138649
59.558
batch start
#iterations: 80
currently lose_sum: 84.20607954263687
time_elpased: 2.274
batch start
#iterations: 81
currently lose_sum: 84.80268388986588
time_elpased: 2.308
batch start
#iterations: 82
currently lose_sum: 84.00680959224701
time_elpased: 1.948
batch start
#iterations: 83
currently lose_sum: 84.1361711025238
time_elpased: 2.223
batch start
#iterations: 84
currently lose_sum: 84.44671189785004
time_elpased: 1.948
batch start
#iterations: 85
currently lose_sum: 84.52122592926025
time_elpased: 2.252
batch start
#iterations: 86
currently lose_sum: 84.10148417949677
time_elpased: 2.316
batch start
#iterations: 87
currently lose_sum: 84.0002401471138
time_elpased: 2.01
batch start
#iterations: 88
currently lose_sum: 83.35324257612228
time_elpased: 1.678
batch start
#iterations: 89
currently lose_sum: 83.7379760146141
time_elpased: 2.182
batch start
#iterations: 90
currently lose_sum: 83.35754859447479
time_elpased: 2.164
batch start
#iterations: 91
currently lose_sum: 83.74086511135101
time_elpased: 2.124
batch start
#iterations: 92
currently lose_sum: 83.33372214436531
time_elpased: 1.963
batch start
#iterations: 93
currently lose_sum: 82.84018161892891
time_elpased: 2.49
batch start
#iterations: 94
currently lose_sum: 83.67294171452522
time_elpased: 2.195
batch start
#iterations: 95
currently lose_sum: 83.46284011006355
time_elpased: 2.266
batch start
#iterations: 96
currently lose_sum: 82.96559876203537
time_elpased: 2.533
batch start
#iterations: 97
currently lose_sum: 82.4304711818695
time_elpased: 2.295
batch start
#iterations: 98
currently lose_sum: 82.2971026301384
time_elpased: 1.841
batch start
#iterations: 99
currently lose_sum: 83.18920013308525
time_elpased: 1.797
start validation test
0.620360824742
0.667425968109
0.482453432129
0.560062122932
0.620602942441
63.584
batch start
#iterations: 100
currently lose_sum: 83.19172412157059
time_elpased: 2.408
batch start
#iterations: 101
currently lose_sum: 82.87250888347626
time_elpased: 2.418
batch start
#iterations: 102
currently lose_sum: 82.78907001018524
time_elpased: 1.759
batch start
#iterations: 103
currently lose_sum: 82.519391477108
time_elpased: 2.114
batch start
#iterations: 104
currently lose_sum: 82.16438567638397
time_elpased: 1.933
batch start
#iterations: 105
currently lose_sum: 82.60005128383636
time_elpased: 2.165
batch start
#iterations: 106
currently lose_sum: 82.57206928730011
time_elpased: 2.375
batch start
#iterations: 107
currently lose_sum: 83.0459914803505
time_elpased: 2.289
batch start
#iterations: 108
currently lose_sum: 82.92362743616104
time_elpased: 2.175
batch start
#iterations: 109
currently lose_sum: 82.30988463759422
time_elpased: 2.247
batch start
#iterations: 110
currently lose_sum: 82.40184432268143
time_elpased: 2.27
batch start
#iterations: 111
currently lose_sum: 82.74253761768341
time_elpased: 2.405
batch start
#iterations: 112
currently lose_sum: 81.52627047896385
time_elpased: 2.192
batch start
#iterations: 113
currently lose_sum: 81.67814949154854
time_elpased: 2.13
batch start
#iterations: 114
currently lose_sum: 82.03002682328224
time_elpased: 2.13
batch start
#iterations: 115
currently lose_sum: 82.46773388981819
time_elpased: 2.26
batch start
#iterations: 116
currently lose_sum: 82.4420216679573
time_elpased: 2.282
batch start
#iterations: 117
currently lose_sum: 81.65392380952835
time_elpased: 2.211
batch start
#iterations: 118
currently lose_sum: 81.79135805368423
time_elpased: 2.174
batch start
#iterations: 119
currently lose_sum: 81.51882892847061
time_elpased: 2.04
start validation test
0.642783505155
0.662355819643
0.585057116394
0.62131147541
0.642884852734
61.197
batch start
#iterations: 120
currently lose_sum: 81.20969054102898
time_elpased: 2.315
batch start
#iterations: 121
currently lose_sum: 82.44629514217377
time_elpased: 2.332
batch start
#iterations: 122
currently lose_sum: 81.91219308972359
time_elpased: 2.213
batch start
#iterations: 123
currently lose_sum: 81.73427858948708
time_elpased: 2.02
batch start
#iterations: 124
currently lose_sum: 81.27596479654312
time_elpased: 1.836
batch start
#iterations: 125
currently lose_sum: 80.55838716030121
time_elpased: 2.258
batch start
#iterations: 126
currently lose_sum: 81.35271668434143
time_elpased: 2.153
batch start
#iterations: 127
currently lose_sum: 81.47097232937813
time_elpased: 1.667
batch start
#iterations: 128
currently lose_sum: 80.84084171056747
time_elpased: 1.469
batch start
#iterations: 129
currently lose_sum: 81.28691935539246
time_elpased: 1.825
batch start
#iterations: 130
currently lose_sum: 80.97421863675117
time_elpased: 2.248
batch start
#iterations: 131
currently lose_sum: 80.9700525701046
time_elpased: 1.968
batch start
#iterations: 132
currently lose_sum: 81.69005903601646
time_elpased: 2.333
batch start
#iterations: 133
currently lose_sum: 80.26884263753891
time_elpased: 2.056
batch start
#iterations: 134
currently lose_sum: 80.13312327861786
time_elpased: 1.844
batch start
#iterations: 135
currently lose_sum: 81.0519270002842
time_elpased: 2.231
batch start
#iterations: 136
currently lose_sum: 81.68694472312927
time_elpased: 2.19
batch start
#iterations: 137
currently lose_sum: 81.04414755105972
time_elpased: 1.885
batch start
#iterations: 138
currently lose_sum: 80.82142037153244
time_elpased: 2.211
batch start
#iterations: 139
currently lose_sum: 80.41155880689621
time_elpased: 2.282
start validation test
0.639226804124
0.660448642267
0.575692086035
0.615164678067
0.639338349121
61.983
batch start
#iterations: 140
currently lose_sum: 79.91953885555267
time_elpased: 2.263
batch start
#iterations: 141
currently lose_sum: 80.6755673289299
time_elpased: 2.229
batch start
#iterations: 142
currently lose_sum: 81.05979090929031
time_elpased: 2.153
batch start
#iterations: 143
currently lose_sum: 80.15264168381691
time_elpased: 2.011
batch start
#iterations: 144
currently lose_sum: 79.91632878780365
time_elpased: 2.423
batch start
#iterations: 145
currently lose_sum: 80.34028807282448
time_elpased: 2.433
batch start
#iterations: 146
currently lose_sum: 80.70031803846359
time_elpased: 2.415
batch start
#iterations: 147
currently lose_sum: 80.43123278021812
time_elpased: 1.992
batch start
#iterations: 148
currently lose_sum: 80.34048649668694
time_elpased: 2.363
batch start
#iterations: 149
currently lose_sum: 80.15178591012955
time_elpased: 2.434
batch start
#iterations: 150
currently lose_sum: 80.42217475175858
time_elpased: 1.829
batch start
#iterations: 151
currently lose_sum: 80.69406619668007
time_elpased: 2.232
batch start
#iterations: 152
currently lose_sum: 80.28167778253555
time_elpased: 1.961
batch start
#iterations: 153
currently lose_sum: 80.41196498274803
time_elpased: 2.424
batch start
#iterations: 154
currently lose_sum: 79.75685542821884
time_elpased: 2.197
batch start
#iterations: 155
currently lose_sum: 80.31073307991028
time_elpased: 1.819
batch start
#iterations: 156
currently lose_sum: 80.65008047223091
time_elpased: 2.159
batch start
#iterations: 157
currently lose_sum: 80.08088940382004
time_elpased: 2.098
batch start
#iterations: 158
currently lose_sum: 80.13933274149895
time_elpased: 2.245
batch start
#iterations: 159
currently lose_sum: 80.03047156333923
time_elpased: 1.877
start validation test
0.633917525773
0.675668413274
0.517546567871
0.586130536131
0.634121832939
63.849
batch start
#iterations: 160
currently lose_sum: 79.7966479063034
time_elpased: 3.48
batch start
#iterations: 161
currently lose_sum: 80.13599103689194
time_elpased: 2.17
batch start
#iterations: 162
currently lose_sum: 80.13955190777779
time_elpased: 2.333
batch start
#iterations: 163
currently lose_sum: 79.33071726560593
time_elpased: 2.349
batch start
#iterations: 164
currently lose_sum: 79.78725418448448
time_elpased: 2.165
batch start
#iterations: 165
currently lose_sum: 79.3499707877636
time_elpased: 2.255
batch start
#iterations: 166
currently lose_sum: 79.2967221736908
time_elpased: 2.05
batch start
#iterations: 167
currently lose_sum: 79.95448869466782
time_elpased: 1.914
batch start
#iterations: 168
currently lose_sum: 79.52449807524681
time_elpased: 2.11
batch start
#iterations: 169
currently lose_sum: 79.18609830737114
time_elpased: 2.039
batch start
#iterations: 170
currently lose_sum: 79.18470245599747
time_elpased: 2.147
batch start
#iterations: 171
currently lose_sum: 78.85857608914375
time_elpased: 1.916
batch start
#iterations: 172
currently lose_sum: 79.49759382009506
time_elpased: 1.999
batch start
#iterations: 173
currently lose_sum: 79.22166484594345
time_elpased: 2.113
batch start
#iterations: 174
currently lose_sum: 78.51830631494522
time_elpased: 2.137
batch start
#iterations: 175
currently lose_sum: 79.23513880372047
time_elpased: 2.251
batch start
#iterations: 176
currently lose_sum: 79.20202100276947
time_elpased: 2.09
batch start
#iterations: 177
currently lose_sum: 79.39068907499313
time_elpased: 1.882
batch start
#iterations: 178
currently lose_sum: 78.58092659711838
time_elpased: 2.221
batch start
#iterations: 179
currently lose_sum: 78.33117243647575
time_elpased: 1.796
start validation test
0.624432989691
0.662174783189
0.51075434805
0.57669068092
0.62463257008
65.504
batch start
#iterations: 180
currently lose_sum: 79.53033545613289
time_elpased: 1.768
batch start
#iterations: 181
currently lose_sum: 78.43845409154892
time_elpased: 1.484
batch start
#iterations: 182
currently lose_sum: 78.97274932265282
time_elpased: 2.013
batch start
#iterations: 183
currently lose_sum: 79.13320475816727
time_elpased: 2.311
batch start
#iterations: 184
currently lose_sum: 78.63601240515709
time_elpased: 2.253
batch start
#iterations: 185
currently lose_sum: 78.97526350617409
time_elpased: 2.195
batch start
#iterations: 186
currently lose_sum: 78.79459461569786
time_elpased: 2.29
batch start
#iterations: 187
currently lose_sum: 79.25724449753761
time_elpased: 2.246
batch start
#iterations: 188
currently lose_sum: 78.66509333252907
time_elpased: 2.174
batch start
#iterations: 189
currently lose_sum: 79.22786110639572
time_elpased: 1.999
batch start
#iterations: 190
currently lose_sum: 77.967245221138
time_elpased: 1.751
batch start
#iterations: 191
currently lose_sum: 78.17406287789345
time_elpased: 2.378
batch start
#iterations: 192
currently lose_sum: 78.64419904351234
time_elpased: 2.313
batch start
#iterations: 193
currently lose_sum: 78.85723432898521
time_elpased: 2.281
batch start
#iterations: 194
currently lose_sum: 79.01884546875954
time_elpased: 2.012
batch start
#iterations: 195
currently lose_sum: 78.46206375956535
time_elpased: 2.048
batch start
#iterations: 196
currently lose_sum: 78.31592535972595
time_elpased: 2.27
batch start
#iterations: 197
currently lose_sum: 78.90748944878578
time_elpased: 2.512
batch start
#iterations: 198
currently lose_sum: 77.84559068083763
time_elpased: 2.318
batch start
#iterations: 199
currently lose_sum: 77.62828177213669
time_elpased: 2.482
start validation test
0.631288659794
0.660691902733
0.542451373881
0.595761514552
0.631444627351
64.852
batch start
#iterations: 200
currently lose_sum: 78.2651960849762
time_elpased: 2.298
batch start
#iterations: 201
currently lose_sum: 78.35252147912979
time_elpased: 2.459
batch start
#iterations: 202
currently lose_sum: 78.49800676107407
time_elpased: 2.254
batch start
#iterations: 203
currently lose_sum: 77.63106244802475
time_elpased: 2.109
batch start
#iterations: 204
currently lose_sum: 77.8259647488594
time_elpased: 2.258
batch start
#iterations: 205
currently lose_sum: 77.999266654253
time_elpased: 2.203
batch start
#iterations: 206
currently lose_sum: 77.80661180615425
time_elpased: 1.884
batch start
#iterations: 207
currently lose_sum: 77.5630861222744
time_elpased: 2.219
batch start
#iterations: 208
currently lose_sum: 78.30862405896187
time_elpased: 1.839
batch start
#iterations: 209
currently lose_sum: 77.9608082473278
time_elpased: 2.299
batch start
#iterations: 210
currently lose_sum: 77.76891699433327
time_elpased: 2.486
batch start
#iterations: 211
currently lose_sum: 78.16767871379852
time_elpased: 1.936
batch start
#iterations: 212
currently lose_sum: 77.84508728981018
time_elpased: 2.182
batch start
#iterations: 213
currently lose_sum: 77.78963622450829
time_elpased: 2.252
batch start
#iterations: 214
currently lose_sum: 77.81711632013321
time_elpased: 2.192
batch start
#iterations: 215
currently lose_sum: 77.64072582125664
time_elpased: 2.132
batch start
#iterations: 216
currently lose_sum: 77.9525990486145
time_elpased: 2.2
batch start
#iterations: 217
currently lose_sum: 77.9636298418045
time_elpased: 2.231
batch start
#iterations: 218
currently lose_sum: 77.82558912038803
time_elpased: 2.23
batch start
#iterations: 219
currently lose_sum: 77.68548619747162
time_elpased: 2.241
start validation test
0.633969072165
0.66329588015
0.546773695585
0.599424606532
0.634122157097
65.358
batch start
#iterations: 220
currently lose_sum: 77.79029899835587
time_elpased: 2.171
batch start
#iterations: 221
currently lose_sum: 77.24090752005577
time_elpased: 2.203
batch start
#iterations: 222
currently lose_sum: 77.78910738229752
time_elpased: 2.14
batch start
#iterations: 223
currently lose_sum: 78.13449713587761
time_elpased: 1.869
batch start
#iterations: 224
currently lose_sum: 77.43307021260262
time_elpased: 2.23
batch start
#iterations: 225
currently lose_sum: 77.89139431715012
time_elpased: 2.294
batch start
#iterations: 226
currently lose_sum: 77.03157410025597
time_elpased: 2.145
batch start
#iterations: 227
currently lose_sum: 77.30239766836166
time_elpased: 2.253
batch start
#iterations: 228
currently lose_sum: 77.79626050591469
time_elpased: 2.2
batch start
#iterations: 229
currently lose_sum: 76.65197142958641
time_elpased: 2.042
batch start
#iterations: 230
currently lose_sum: 77.73795747756958
time_elpased: 2.23
batch start
#iterations: 231
currently lose_sum: 77.19330161809921
time_elpased: 1.994
batch start
#iterations: 232
currently lose_sum: 77.49715197086334
time_elpased: 1.526
batch start
#iterations: 233
currently lose_sum: 76.6553547680378
time_elpased: 1.51
batch start
#iterations: 234
currently lose_sum: 76.8510859310627
time_elpased: 1.657
batch start
#iterations: 235
currently lose_sum: 77.77063697576523
time_elpased: 1.939
batch start
#iterations: 236
currently lose_sum: 77.05389994382858
time_elpased: 1.878
batch start
#iterations: 237
currently lose_sum: 76.75540217757225
time_elpased: 1.82
batch start
#iterations: 238
currently lose_sum: 77.2049024105072
time_elpased: 2.19
batch start
#iterations: 239
currently lose_sum: 76.9574933052063
time_elpased: 2.262
start validation test
0.633298969072
0.668261150614
0.531954306885
0.592367636947
0.633476895258
65.317
batch start
#iterations: 240
currently lose_sum: 76.34187623858452
time_elpased: 1.968
batch start
#iterations: 241
currently lose_sum: 77.38431885838509
time_elpased: 2.09
batch start
#iterations: 242
currently lose_sum: 77.27427634596825
time_elpased: 1.647
batch start
#iterations: 243
currently lose_sum: 77.46305513381958
time_elpased: 1.686
batch start
#iterations: 244
currently lose_sum: 76.70761570334435
time_elpased: 2.014
batch start
#iterations: 245
currently lose_sum: 77.09007292985916
time_elpased: 1.822
batch start
#iterations: 246
currently lose_sum: 76.93250444531441
time_elpased: 2.325
batch start
#iterations: 247
currently lose_sum: 77.01227375864983
time_elpased: 2.386
batch start
#iterations: 248
currently lose_sum: 76.52811998128891
time_elpased: 1.92
batch start
#iterations: 249
currently lose_sum: 76.71348109841347
time_elpased: 2.138
batch start
#iterations: 250
currently lose_sum: 76.19422861933708
time_elpased: 1.616
batch start
#iterations: 251
currently lose_sum: 76.18305990099907
time_elpased: 1.621
batch start
#iterations: 252
currently lose_sum: 76.4206992983818
time_elpased: 1.661
batch start
#iterations: 253
currently lose_sum: 76.98941150307655
time_elpased: 1.534
batch start
#iterations: 254
currently lose_sum: 76.60009160637856
time_elpased: 1.531
batch start
#iterations: 255
currently lose_sum: 76.83422049880028
time_elpased: 1.8
batch start
#iterations: 256
currently lose_sum: 76.33246737718582
time_elpased: 1.415
batch start
#iterations: 257
currently lose_sum: 76.70311999320984
time_elpased: 1.977
batch start
#iterations: 258
currently lose_sum: 76.33187165856361
time_elpased: 2.247
batch start
#iterations: 259
currently lose_sum: 76.11797678470612
time_elpased: 2.405
start validation test
0.627731958763
0.661991949098
0.524647524956
0.585371454817
0.627912939386
66.242
batch start
#iterations: 260
currently lose_sum: 76.41411828994751
time_elpased: 2.463
batch start
#iterations: 261
currently lose_sum: 76.30203077197075
time_elpased: 2.466
batch start
#iterations: 262
currently lose_sum: 76.42611253261566
time_elpased: 2.485
batch start
#iterations: 263
currently lose_sum: 76.69068983197212
time_elpased: 2.181
batch start
#iterations: 264
currently lose_sum: 76.40605250000954
time_elpased: 2.177
batch start
#iterations: 265
currently lose_sum: 76.717658162117
time_elpased: 1.954
batch start
#iterations: 266
currently lose_sum: 75.92102360725403
time_elpased: 1.902
batch start
#iterations: 267
currently lose_sum: 76.86028963327408
time_elpased: 2.092
batch start
#iterations: 268
currently lose_sum: 76.22870582342148
time_elpased: 2.236
batch start
#iterations: 269
currently lose_sum: 76.64191779494286
time_elpased: 2.25
batch start
#iterations: 270
currently lose_sum: 75.92308789491653
time_elpased: 2.045
batch start
#iterations: 271
currently lose_sum: 76.26725646853447
time_elpased: 2.335
batch start
#iterations: 272
currently lose_sum: 75.98950651288033
time_elpased: 2.24
batch start
#iterations: 273
currently lose_sum: 75.50677385926247
time_elpased: 2.183
batch start
#iterations: 274
currently lose_sum: 76.25787371397018
time_elpased: 2.105
batch start
#iterations: 275
currently lose_sum: 76.33084148168564
time_elpased: 2.208
batch start
#iterations: 276
currently lose_sum: 75.8942502439022
time_elpased: 2.182
batch start
#iterations: 277
currently lose_sum: 76.64246344566345
time_elpased: 2.264
batch start
#iterations: 278
currently lose_sum: 75.59510177373886
time_elpased: 2.243
batch start
#iterations: 279
currently lose_sum: 76.21081474423409
time_elpased: 2.195
start validation test
0.621597938144
0.678378378378
0.464958320469
0.551749404653
0.621872943153
69.383
batch start
#iterations: 280
currently lose_sum: 76.02848786115646
time_elpased: 2.254
batch start
#iterations: 281
currently lose_sum: 76.25278478860855
time_elpased: 2.056
batch start
#iterations: 282
currently lose_sum: 76.18861910700798
time_elpased: 2.179
batch start
#iterations: 283
currently lose_sum: 76.0354186296463
time_elpased: 2.266
batch start
#iterations: 284
currently lose_sum: 75.03276920318604
time_elpased: 2.238
batch start
#iterations: 285
currently lose_sum: 75.64383912086487
time_elpased: 2.23
batch start
#iterations: 286
currently lose_sum: 75.64813947677612
time_elpased: 2.46
batch start
#iterations: 287
currently lose_sum: 75.34705924987793
time_elpased: 2.264
batch start
#iterations: 288
currently lose_sum: 76.08299249410629
time_elpased: 2.173
batch start
#iterations: 289
currently lose_sum: 75.14786764979362
time_elpased: 2.24
batch start
#iterations: 290
currently lose_sum: 75.2690164744854
time_elpased: 2.227
batch start
#iterations: 291
currently lose_sum: 75.59729614853859
time_elpased: 2.151
batch start
#iterations: 292
currently lose_sum: 75.70540872216225
time_elpased: 1.726
batch start
#iterations: 293
currently lose_sum: 76.08404225111008
time_elpased: 1.899
batch start
#iterations: 294
currently lose_sum: 75.24268266558647
time_elpased: 2.323
batch start
#iterations: 295
currently lose_sum: 74.82927405834198
time_elpased: 2.153
batch start
#iterations: 296
currently lose_sum: 74.67310792207718
time_elpased: 2.12
batch start
#iterations: 297
currently lose_sum: 75.4504101574421
time_elpased: 1.999
batch start
#iterations: 298
currently lose_sum: 75.21801170706749
time_elpased: 2.336
batch start
#iterations: 299
currently lose_sum: 74.95527639985085
time_elpased: 2.256
start validation test
0.630515463918
0.66126787296
0.537820314912
0.593189557321
0.630678204549
66.810
batch start
#iterations: 300
currently lose_sum: 75.34345468878746
time_elpased: 2.275
batch start
#iterations: 301
currently lose_sum: 75.35861346125603
time_elpased: 1.713
batch start
#iterations: 302
currently lose_sum: 75.49744349718094
time_elpased: 2.195
batch start
#iterations: 303
currently lose_sum: 75.54568538069725
time_elpased: 2.214
batch start
#iterations: 304
currently lose_sum: 75.4466905593872
time_elpased: 2.324
batch start
#iterations: 305
currently lose_sum: 75.72239270806313
time_elpased: 1.565
batch start
#iterations: 306
currently lose_sum: 75.39535063505173
time_elpased: 1.519
batch start
#iterations: 307
currently lose_sum: 75.33510673046112
time_elpased: 1.519
batch start
#iterations: 308
currently lose_sum: 75.35637325048447
time_elpased: 1.567
batch start
#iterations: 309
currently lose_sum: 75.35315129160881
time_elpased: 1.548
batch start
#iterations: 310
currently lose_sum: 75.93658772110939
time_elpased: 1.748
batch start
#iterations: 311
currently lose_sum: 74.8724916279316
time_elpased: 1.672
batch start
#iterations: 312
currently lose_sum: 75.33321756124496
time_elpased: 1.817
batch start
#iterations: 313
currently lose_sum: 75.34920737147331
time_elpased: 2.302
batch start
#iterations: 314
currently lose_sum: 74.68228754401207
time_elpased: 2.365
batch start
#iterations: 315
currently lose_sum: 74.87918674945831
time_elpased: 2.301
batch start
#iterations: 316
currently lose_sum: 74.34778162837029
time_elpased: 1.963
batch start
#iterations: 317
currently lose_sum: 75.16206696629524
time_elpased: 2.348
batch start
#iterations: 318
currently lose_sum: 75.14374125003815
time_elpased: 2.474
batch start
#iterations: 319
currently lose_sum: 74.38314798474312
time_elpased: 2.03
start validation test
0.619536082474
0.672832198875
0.467942780694
0.551987860395
0.619802227897
69.156
batch start
#iterations: 320
currently lose_sum: 74.37165623903275
time_elpased: 1.617
batch start
#iterations: 321
currently lose_sum: 74.92607739567757
time_elpased: 1.584
batch start
#iterations: 322
currently lose_sum: 75.20594149827957
time_elpased: 1.677
batch start
#iterations: 323
currently lose_sum: 74.51983174681664
time_elpased: 1.537
batch start
#iterations: 324
currently lose_sum: 74.92498421669006
time_elpased: 1.647
batch start
#iterations: 325
currently lose_sum: 75.3178046643734
time_elpased: 2.077
batch start
#iterations: 326
currently lose_sum: 74.29710173606873
time_elpased: 2.157
batch start
#iterations: 327
currently lose_sum: 74.86584544181824
time_elpased: 2.191
batch start
#iterations: 328
currently lose_sum: 74.87575832009315
time_elpased: 2.102
batch start
#iterations: 329
currently lose_sum: 74.8749637901783
time_elpased: 2.179
batch start
#iterations: 330
currently lose_sum: 74.97666501998901
time_elpased: 2.192
batch start
#iterations: 331
currently lose_sum: 75.50398674607277
time_elpased: 2.205
batch start
#iterations: 332
currently lose_sum: 74.97755041718483
time_elpased: 2.23
batch start
#iterations: 333
currently lose_sum: 74.6893914937973
time_elpased: 2.121
batch start
#iterations: 334
currently lose_sum: 75.01463195681572
time_elpased: 2.175
batch start
#iterations: 335
currently lose_sum: 75.27806285023689
time_elpased: 2.341
batch start
#iterations: 336
currently lose_sum: 74.89608877897263
time_elpased: 2.178
batch start
#iterations: 337
currently lose_sum: 74.75193133950233
time_elpased: 2.223
batch start
#iterations: 338
currently lose_sum: 74.09877929091454
time_elpased: 2.263
batch start
#iterations: 339
currently lose_sum: 74.60239225625992
time_elpased: 1.905
start validation test
0.632731958763
0.66256899147
0.543583410518
0.597207303974
0.63288847279
67.092
batch start
#iterations: 340
currently lose_sum: 75.26880496740341
time_elpased: 2.192
batch start
#iterations: 341
currently lose_sum: 74.91922384500504
time_elpased: 2.212
batch start
#iterations: 342
currently lose_sum: 74.41948953270912
time_elpased: 2.098
batch start
#iterations: 343
currently lose_sum: 75.33635699748993
time_elpased: 2.235
batch start
#iterations: 344
currently lose_sum: 74.8493363559246
time_elpased: 2.15
batch start
#iterations: 345
currently lose_sum: 75.04971811175346
time_elpased: 2.212
batch start
#iterations: 346
currently lose_sum: 73.92145255208015
time_elpased: 2.395
batch start
#iterations: 347
currently lose_sum: 74.03549551963806
time_elpased: 2.239
batch start
#iterations: 348
currently lose_sum: 74.79110199213028
time_elpased: 2.219
batch start
#iterations: 349
currently lose_sum: 74.6836551129818
time_elpased: 2.216
batch start
#iterations: 350
currently lose_sum: 74.54676669836044
time_elpased: 2.221
batch start
#iterations: 351
currently lose_sum: 74.26973748207092
time_elpased: 2.177
batch start
#iterations: 352
currently lose_sum: 73.72172060608864
time_elpased: 1.961
batch start
#iterations: 353
currently lose_sum: 74.24342122673988
time_elpased: 2.219
batch start
#iterations: 354
currently lose_sum: 74.62324264645576
time_elpased: 2.049
batch start
#iterations: 355
currently lose_sum: 74.3149117231369
time_elpased: 2.313
batch start
#iterations: 356
currently lose_sum: 73.88378962874413
time_elpased: 2.334
batch start
#iterations: 357
currently lose_sum: 73.94721573591232
time_elpased: 2.228
batch start
#iterations: 358
currently lose_sum: 73.9189658164978
time_elpased: 2.265
batch start
#iterations: 359
currently lose_sum: 74.57496854662895
time_elpased: 2.235
start validation test
0.626855670103
0.660783804827
0.524030050427
0.584514721919
0.627036196338
68.152
batch start
#iterations: 360
currently lose_sum: 74.62680238485336
time_elpased: 2.186
batch start
#iterations: 361
currently lose_sum: 74.22845214605331
time_elpased: 1.893
batch start
#iterations: 362
currently lose_sum: 74.44027644395828
time_elpased: 2.183
batch start
#iterations: 363
currently lose_sum: 74.30213662981987
time_elpased: 2.252
batch start
#iterations: 364
currently lose_sum: 73.58032521605492
time_elpased: 1.943
batch start
#iterations: 365
currently lose_sum: 73.39665043354034
time_elpased: 2.06
batch start
#iterations: 366
currently lose_sum: 73.65726432204247
time_elpased: 2.014
batch start
#iterations: 367
currently lose_sum: 73.4842723608017
time_elpased: 2.391
batch start
#iterations: 368
currently lose_sum: 73.56847542524338
time_elpased: 2.356
batch start
#iterations: 369
currently lose_sum: 74.45601823925972
time_elpased: 2.345
batch start
#iterations: 370
currently lose_sum: 74.20494151115417
time_elpased: 1.915
batch start
#iterations: 371
currently lose_sum: 73.20037257671356
time_elpased: 2.358
batch start
#iterations: 372
currently lose_sum: 74.00344869494438
time_elpased: 2.224
batch start
#iterations: 373
currently lose_sum: 73.11697101593018
time_elpased: 1.912
batch start
#iterations: 374
currently lose_sum: 74.58032801747322
time_elpased: 2.045
batch start
#iterations: 375
currently lose_sum: 73.9625024497509
time_elpased: 2.267
batch start
#iterations: 376
currently lose_sum: 74.16019523143768
time_elpased: 2.18
batch start
#iterations: 377
currently lose_sum: 74.21595805883408
time_elpased: 2.235
batch start
#iterations: 378
currently lose_sum: 73.69228011369705
time_elpased: 2.077
batch start
#iterations: 379
currently lose_sum: 73.35968536138535
time_elpased: 2.147
start validation test
0.631958762887
0.666279519938
0.531336832356
0.591205771213
0.632135420206
68.892
batch start
#iterations: 380
currently lose_sum: 73.63695284724236
time_elpased: 2.333
batch start
#iterations: 381
currently lose_sum: 73.73020991683006
time_elpased: 2.256
batch start
#iterations: 382
currently lose_sum: 73.11028283834457
time_elpased: 2.133
batch start
#iterations: 383
currently lose_sum: 72.95597356557846
time_elpased: 2.112
batch start
#iterations: 384
currently lose_sum: 72.83011013269424
time_elpased: 2.191
batch start
#iterations: 385
currently lose_sum: 73.78988030552864
time_elpased: 1.764
batch start
#iterations: 386
currently lose_sum: 73.72623339295387
time_elpased: 1.739
batch start
#iterations: 387
currently lose_sum: 73.93143326044083
time_elpased: 2.048
batch start
#iterations: 388
currently lose_sum: 73.99377956986427
time_elpased: 1.868
batch start
#iterations: 389
currently lose_sum: 74.37219521403313
time_elpased: 2.122
batch start
#iterations: 390
currently lose_sum: 73.62019217014313
time_elpased: 2.151
batch start
#iterations: 391
currently lose_sum: 73.02021181583405
time_elpased: 1.877
batch start
#iterations: 392
currently lose_sum: 74.19726067781448
time_elpased: 2.073
batch start
#iterations: 393
currently lose_sum: 72.82518768310547
time_elpased: 1.815
batch start
#iterations: 394
currently lose_sum: 73.63889899849892
time_elpased: 2.255
batch start
#iterations: 395
currently lose_sum: 73.49948620796204
time_elpased: 1.91
batch start
#iterations: 396
currently lose_sum: 73.29447656869888
time_elpased: 2.242
batch start
#iterations: 397
currently lose_sum: 73.79504552483559
time_elpased: 2.348
batch start
#iterations: 398
currently lose_sum: 73.66951707005501
time_elpased: 2.17
batch start
#iterations: 399
currently lose_sum: 73.08642798662186
time_elpased: 1.958
start validation test
0.610309278351
0.678176111019
0.422455490378
0.520608750793
0.61063908465
74.274
acc: 0.646
pre: 0.650
rec: 0.638
F1: 0.644
auc: 0.646
