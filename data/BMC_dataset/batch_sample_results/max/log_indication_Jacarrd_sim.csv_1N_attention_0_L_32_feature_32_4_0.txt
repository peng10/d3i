start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 99.91501992940903
time_elpased: 2.138
batch start
#iterations: 1
currently lose_sum: 98.84760248661041
time_elpased: 2.1
batch start
#iterations: 2
currently lose_sum: 98.0257887840271
time_elpased: 2.079
batch start
#iterations: 3
currently lose_sum: 97.37696063518524
time_elpased: 2.104
batch start
#iterations: 4
currently lose_sum: 96.35034728050232
time_elpased: 2.077
batch start
#iterations: 5
currently lose_sum: 95.47652286291122
time_elpased: 2.11
batch start
#iterations: 6
currently lose_sum: 95.04406428337097
time_elpased: 2.042
batch start
#iterations: 7
currently lose_sum: 94.40785217285156
time_elpased: 2.052
batch start
#iterations: 8
currently lose_sum: 93.75755351781845
time_elpased: 2.086
batch start
#iterations: 9
currently lose_sum: 93.72551321983337
time_elpased: 2.136
batch start
#iterations: 10
currently lose_sum: 93.48816138505936
time_elpased: 2.134
batch start
#iterations: 11
currently lose_sum: 92.98263478279114
time_elpased: 2.166
batch start
#iterations: 12
currently lose_sum: 92.62901502847672
time_elpased: 2.105
batch start
#iterations: 13
currently lose_sum: 92.8240298628807
time_elpased: 2.14
batch start
#iterations: 14
currently lose_sum: 91.92355221509933
time_elpased: 2.126
batch start
#iterations: 15
currently lose_sum: 91.30752623081207
time_elpased: 2.117
batch start
#iterations: 16
currently lose_sum: 91.62991160154343
time_elpased: 2.111
batch start
#iterations: 17
currently lose_sum: 91.35213774442673
time_elpased: 2.186
batch start
#iterations: 18
currently lose_sum: 90.7438040971756
time_elpased: 2.119
batch start
#iterations: 19
currently lose_sum: 90.83659100532532
time_elpased: 2.145
start validation test
0.64618556701
0.654634146341
0.621488113615
0.637630662021
0.646228927199
59.959
batch start
#iterations: 20
currently lose_sum: 90.81793570518494
time_elpased: 2.11
batch start
#iterations: 21
currently lose_sum: 90.14408314228058
time_elpased: 2.102
batch start
#iterations: 22
currently lose_sum: 90.22581142187119
time_elpased: 2.104
batch start
#iterations: 23
currently lose_sum: 90.24564981460571
time_elpased: 2.114
batch start
#iterations: 24
currently lose_sum: 89.6382212638855
time_elpased: 2.113
batch start
#iterations: 25
currently lose_sum: 89.37982231378555
time_elpased: 2.204
batch start
#iterations: 26
currently lose_sum: 89.36076426506042
time_elpased: 2.057
batch start
#iterations: 27
currently lose_sum: 89.01454532146454
time_elpased: 2.055
batch start
#iterations: 28
currently lose_sum: 88.81634390354156
time_elpased: 2.136
batch start
#iterations: 29
currently lose_sum: 88.7511458992958
time_elpased: 2.289
batch start
#iterations: 30
currently lose_sum: 88.55964189767838
time_elpased: 2.171
batch start
#iterations: 31
currently lose_sum: 88.1491641998291
time_elpased: 2.172
batch start
#iterations: 32
currently lose_sum: 87.75009053945541
time_elpased: 2.076
batch start
#iterations: 33
currently lose_sum: 88.52620768547058
time_elpased: 2.274
batch start
#iterations: 34
currently lose_sum: 87.5458534359932
time_elpased: 2.043
batch start
#iterations: 35
currently lose_sum: 87.75533413887024
time_elpased: 1.998
batch start
#iterations: 36
currently lose_sum: 87.79241806268692
time_elpased: 2.016
batch start
#iterations: 37
currently lose_sum: 87.48353505134583
time_elpased: 2.013
batch start
#iterations: 38
currently lose_sum: 87.77410441637039
time_elpased: 2.056
batch start
#iterations: 39
currently lose_sum: 87.22901582717896
time_elpased: 2.151
start validation test
0.655360824742
0.649442855734
0.677781208192
0.663309497432
0.655321462301
60.102
batch start
#iterations: 40
currently lose_sum: 86.38420820236206
time_elpased: 2.112
batch start
#iterations: 41
currently lose_sum: 86.82426679134369
time_elpased: 2.105
batch start
#iterations: 42
currently lose_sum: 86.64697968959808
time_elpased: 2.076
batch start
#iterations: 43
currently lose_sum: 87.20714628696442
time_elpased: 2.114
batch start
#iterations: 44
currently lose_sum: 86.22185444831848
time_elpased: 2.116
batch start
#iterations: 45
currently lose_sum: 86.13293009996414
time_elpased: 2.12
batch start
#iterations: 46
currently lose_sum: 85.99612259864807
time_elpased: 2.123
batch start
#iterations: 47
currently lose_sum: 86.14621353149414
time_elpased: 2.122
batch start
#iterations: 48
currently lose_sum: 85.76245814561844
time_elpased: 2.047
batch start
#iterations: 49
currently lose_sum: 86.15503120422363
time_elpased: 2.051
batch start
#iterations: 50
currently lose_sum: 86.4748803973198
time_elpased: 2.036
batch start
#iterations: 51
currently lose_sum: 85.49056404829025
time_elpased: 2.141
batch start
#iterations: 52
currently lose_sum: 85.90546506643295
time_elpased: 2.084
batch start
#iterations: 53
currently lose_sum: 85.44090074300766
time_elpased: 2.223
batch start
#iterations: 54
currently lose_sum: 85.87549775838852
time_elpased: 2.044
batch start
#iterations: 55
currently lose_sum: 84.76707822084427
time_elpased: 2.092
batch start
#iterations: 56
currently lose_sum: 85.41833031177521
time_elpased: 2.008
batch start
#iterations: 57
currently lose_sum: 84.97677075862885
time_elpased: 2.054
batch start
#iterations: 58
currently lose_sum: 85.41075265407562
time_elpased: 2.126
batch start
#iterations: 59
currently lose_sum: 85.08778190612793
time_elpased: 2.191
start validation test
0.657835051546
0.652350321623
0.678398682721
0.665119564121
0.65779894892
60.577
batch start
#iterations: 60
currently lose_sum: 85.21366012096405
time_elpased: 2.108
batch start
#iterations: 61
currently lose_sum: 84.90706717967987
time_elpased: 2.098
batch start
#iterations: 62
currently lose_sum: 84.36589074134827
time_elpased: 2.148
batch start
#iterations: 63
currently lose_sum: 84.94717693328857
time_elpased: 2.199
batch start
#iterations: 64
currently lose_sum: 85.00077164173126
time_elpased: 2.096
batch start
#iterations: 65
currently lose_sum: 84.73852473497391
time_elpased: 2.083
batch start
#iterations: 66
currently lose_sum: 84.3379921913147
time_elpased: 2.069
batch start
#iterations: 67
currently lose_sum: 83.82762596011162
time_elpased: 2.035
batch start
#iterations: 68
currently lose_sum: 84.01222676038742
time_elpased: 2.094
batch start
#iterations: 69
currently lose_sum: 84.2091092467308
time_elpased: 2.114
batch start
#iterations: 70
currently lose_sum: 83.77857393026352
time_elpased: 2.071
batch start
#iterations: 71
currently lose_sum: 84.12693202495575
time_elpased: 2.043
batch start
#iterations: 72
currently lose_sum: 83.42718833684921
time_elpased: 2.125
batch start
#iterations: 73
currently lose_sum: 82.94014909863472
time_elpased: 2.061
batch start
#iterations: 74
currently lose_sum: 83.0308518409729
time_elpased: 2.108
batch start
#iterations: 75
currently lose_sum: 83.10865598917007
time_elpased: 2.104
batch start
#iterations: 76
currently lose_sum: 83.13578242063522
time_elpased: 2.123
batch start
#iterations: 77
currently lose_sum: 82.84622660279274
time_elpased: 2.196
batch start
#iterations: 78
currently lose_sum: 82.65782231092453
time_elpased: 2.098
batch start
#iterations: 79
currently lose_sum: 82.57655653357506
time_elpased: 2.095
start validation test
0.642886597938
0.662889849317
0.584027992179
0.620965094649
0.642989933299
63.148
batch start
#iterations: 80
currently lose_sum: 82.49378767609596
time_elpased: 2.156
batch start
#iterations: 81
currently lose_sum: 83.16482138633728
time_elpased: 2.219
batch start
#iterations: 82
currently lose_sum: 82.42624181509018
time_elpased: 2.113
batch start
#iterations: 83
currently lose_sum: 83.07655221223831
time_elpased: 2.141
batch start
#iterations: 84
currently lose_sum: 82.88206794857979
time_elpased: 2.121
batch start
#iterations: 85
currently lose_sum: 83.06933116912842
time_elpased: 2.167
batch start
#iterations: 86
currently lose_sum: 82.24975827336311
time_elpased: 2.087
batch start
#iterations: 87
currently lose_sum: 83.08477348089218
time_elpased: 2.151
batch start
#iterations: 88
currently lose_sum: 82.73639097809792
time_elpased: 2.109
batch start
#iterations: 89
currently lose_sum: 82.7419627904892
time_elpased: 2.125
batch start
#iterations: 90
currently lose_sum: 82.1503694653511
time_elpased: 2.224
batch start
#iterations: 91
currently lose_sum: 82.05250188708305
time_elpased: 2.208
batch start
#iterations: 92
currently lose_sum: 81.99143099784851
time_elpased: 2.122
batch start
#iterations: 93
currently lose_sum: 81.81956061720848
time_elpased: 2.134
batch start
#iterations: 94
currently lose_sum: 82.07053428888321
time_elpased: 2.121
batch start
#iterations: 95
currently lose_sum: 82.33077147603035
time_elpased: 2.107
batch start
#iterations: 96
currently lose_sum: 81.94292357563972
time_elpased: 2.094
batch start
#iterations: 97
currently lose_sum: 81.58265432715416
time_elpased: 2.099
batch start
#iterations: 98
currently lose_sum: 80.93986177444458
time_elpased: 2.105
batch start
#iterations: 99
currently lose_sum: 81.29541799426079
time_elpased: 2.139
start validation test
0.660154639175
0.652360515021
0.688278275188
0.669838249286
0.660105263794
63.176
batch start
#iterations: 100
currently lose_sum: 80.95272356271744
time_elpased: 2.215
batch start
#iterations: 101
currently lose_sum: 81.71778178215027
time_elpased: 2.135
batch start
#iterations: 102
currently lose_sum: 81.29393523931503
time_elpased: 2.129
batch start
#iterations: 103
currently lose_sum: 81.08296629786491
time_elpased: 2.088
batch start
#iterations: 104
currently lose_sum: 81.0652246773243
time_elpased: 2.166
batch start
#iterations: 105
currently lose_sum: 80.60880836844444
time_elpased: 2.061
batch start
#iterations: 106
currently lose_sum: 80.76584696769714
time_elpased: 2.026
batch start
#iterations: 107
currently lose_sum: 81.35645478963852
time_elpased: 2.02
batch start
#iterations: 108
currently lose_sum: 80.98664861917496
time_elpased: 2.049
batch start
#iterations: 109
currently lose_sum: 80.26213577389717
time_elpased: 2.139
batch start
#iterations: 110
currently lose_sum: 81.05675268173218
time_elpased: 2.046
batch start
#iterations: 111
currently lose_sum: 80.92896687984467
time_elpased: 2.199
batch start
#iterations: 112
currently lose_sum: 80.96442699432373
time_elpased: 1.999
batch start
#iterations: 113
currently lose_sum: 80.32869344949722
time_elpased: 2.103
batch start
#iterations: 114
currently lose_sum: 80.35180178284645
time_elpased: 2.063
batch start
#iterations: 115
currently lose_sum: 79.86883294582367
time_elpased: 2.226
batch start
#iterations: 116
currently lose_sum: 80.34949716925621
time_elpased: 2.114
batch start
#iterations: 117
currently lose_sum: 80.29338955879211
time_elpased: 2.106
batch start
#iterations: 118
currently lose_sum: 80.42469501495361
time_elpased: 2.121
batch start
#iterations: 119
currently lose_sum: 80.10687735676765
time_elpased: 2.063
start validation test
0.646804123711
0.637833156933
0.682206442318
0.659273993038
0.646741969481
63.165
batch start
#iterations: 120
currently lose_sum: 80.054159283638
time_elpased: 2.022
batch start
#iterations: 121
currently lose_sum: 80.27386581897736
time_elpased: 1.973
batch start
#iterations: 122
currently lose_sum: 79.67592173814774
time_elpased: 1.989
batch start
#iterations: 123
currently lose_sum: 80.14029362797737
time_elpased: 1.964
batch start
#iterations: 124
currently lose_sum: 80.71665102243423
time_elpased: 2.005
batch start
#iterations: 125
currently lose_sum: 80.41699841618538
time_elpased: 2.132
batch start
#iterations: 126
currently lose_sum: 80.31538173556328
time_elpased: 2.103
batch start
#iterations: 127
currently lose_sum: 79.77571365237236
time_elpased: 2.417
batch start
#iterations: 128
currently lose_sum: 79.55994799733162
time_elpased: 2.057
batch start
#iterations: 129
currently lose_sum: 79.67780575156212
time_elpased: 2.08
batch start
#iterations: 130
currently lose_sum: 79.5552162528038
time_elpased: 2.014
batch start
#iterations: 131
currently lose_sum: 80.07505187392235
time_elpased: 2.05
batch start
#iterations: 132
currently lose_sum: 79.94526162743568
time_elpased: 2.211
batch start
#iterations: 133
currently lose_sum: 79.10549250245094
time_elpased: 2.119
batch start
#iterations: 134
currently lose_sum: 79.27462160587311
time_elpased: 2.053
batch start
#iterations: 135
currently lose_sum: 79.19484853744507
time_elpased: 2.185
batch start
#iterations: 136
currently lose_sum: 79.85356187820435
time_elpased: 2.126
batch start
#iterations: 137
currently lose_sum: 79.3337852358818
time_elpased: 1.983
batch start
#iterations: 138
currently lose_sum: 79.03969290852547
time_elpased: 2.051
batch start
#iterations: 139
currently lose_sum: 79.04295572638512
time_elpased: 2.073
start validation test
0.648865979381
0.649081391768
0.650818153751
0.649948612539
0.648862552038
65.859
batch start
#iterations: 140
currently lose_sum: 79.20844081044197
time_elpased: 1.997
batch start
#iterations: 141
currently lose_sum: 79.71797946095467
time_elpased: 2.0
batch start
#iterations: 142
currently lose_sum: 79.05512496829033
time_elpased: 1.948
batch start
#iterations: 143
currently lose_sum: 79.01300662755966
time_elpased: 1.879
batch start
#iterations: 144
currently lose_sum: 78.60751795768738
time_elpased: 1.826
batch start
#iterations: 145
currently lose_sum: 79.29621490836143
time_elpased: 2.113
batch start
#iterations: 146
currently lose_sum: 78.7750256061554
time_elpased: 2.05
batch start
#iterations: 147
currently lose_sum: 78.94764316082001
time_elpased: 2.076
batch start
#iterations: 148
currently lose_sum: 78.83211317658424
time_elpased: 2.278
batch start
#iterations: 149
currently lose_sum: 79.11136424541473
time_elpased: 2.103
batch start
#iterations: 150
currently lose_sum: 78.6160007417202
time_elpased: 2.021
batch start
#iterations: 151
currently lose_sum: 78.97415247559547
time_elpased: 2.079
batch start
#iterations: 152
currently lose_sum: 78.29895827174187
time_elpased: 2.091
batch start
#iterations: 153
currently lose_sum: 78.39513856172562
time_elpased: 2.021
batch start
#iterations: 154
currently lose_sum: 78.98427727818489
time_elpased: 2.044
batch start
#iterations: 155
currently lose_sum: 78.26961880922318
time_elpased: 2.12
batch start
#iterations: 156
currently lose_sum: 78.62713414430618
time_elpased: 2.104
batch start
#iterations: 157
currently lose_sum: 78.46092614531517
time_elpased: 2.094
batch start
#iterations: 158
currently lose_sum: 79.055290132761
time_elpased: 2.062
batch start
#iterations: 159
currently lose_sum: 78.58547157049179
time_elpased: 2.089
start validation test
0.630412371134
0.654569729336
0.555006689307
0.600690576966
0.630544757439
69.849
batch start
#iterations: 160
currently lose_sum: 78.83287939429283
time_elpased: 2.141
batch start
#iterations: 161
currently lose_sum: 77.87424165010452
time_elpased: 2.104
batch start
#iterations: 162
currently lose_sum: 77.32263249158859
time_elpased: 2.037
batch start
#iterations: 163
currently lose_sum: 78.33144479990005
time_elpased: 2.133
batch start
#iterations: 164
currently lose_sum: 78.29229533672333
time_elpased: 2.128
batch start
#iterations: 165
currently lose_sum: 77.96352124214172
time_elpased: 2.143
batch start
#iterations: 166
currently lose_sum: 78.18168011307716
time_elpased: 2.092
batch start
#iterations: 167
currently lose_sum: 78.12771478295326
time_elpased: 2.113
batch start
#iterations: 168
currently lose_sum: 77.79182815551758
time_elpased: 2.173
batch start
#iterations: 169
currently lose_sum: 77.76698806881905
time_elpased: 2.097
batch start
#iterations: 170
currently lose_sum: 77.36570453643799
time_elpased: 2.099
batch start
#iterations: 171
currently lose_sum: 77.66404223442078
time_elpased: 2.235
batch start
#iterations: 172
currently lose_sum: 76.87559372186661
time_elpased: 2.13
batch start
#iterations: 173
currently lose_sum: 77.55166697502136
time_elpased: 2.195
batch start
#iterations: 174
currently lose_sum: 77.43105751276016
time_elpased: 2.108
batch start
#iterations: 175
currently lose_sum: 77.81260126829147
time_elpased: 2.098
batch start
#iterations: 176
currently lose_sum: 77.23541182279587
time_elpased: 2.126
batch start
#iterations: 177
currently lose_sum: 77.80804714560509
time_elpased: 2.156
batch start
#iterations: 178
currently lose_sum: 77.00176519155502
time_elpased: 2.122
batch start
#iterations: 179
currently lose_sum: 77.27873075008392
time_elpased: 2.142
start validation test
0.648969072165
0.658454158945
0.621591026037
0.6394917946
0.649017138548
70.468
batch start
#iterations: 180
currently lose_sum: 77.39490324258804
time_elpased: 2.064
batch start
#iterations: 181
currently lose_sum: 76.3782048523426
time_elpased: 2.046
batch start
#iterations: 182
currently lose_sum: 76.65999320149422
time_elpased: 2.104
batch start
#iterations: 183
currently lose_sum: 76.73943716287613
time_elpased: 2.127
batch start
#iterations: 184
currently lose_sum: 76.41962257027626
time_elpased: 2.151
batch start
#iterations: 185
currently lose_sum: 77.14119815826416
time_elpased: 2.263
batch start
#iterations: 186
currently lose_sum: 77.01760864257812
time_elpased: 2.077
batch start
#iterations: 187
currently lose_sum: 77.13051387667656
time_elpased: 2.345
batch start
#iterations: 188
currently lose_sum: 76.23709657788277
time_elpased: 1.969
batch start
#iterations: 189
currently lose_sum: 76.72059866786003
time_elpased: 2.021
batch start
#iterations: 190
currently lose_sum: 76.75704622268677
time_elpased: 2.006
batch start
#iterations: 191
currently lose_sum: 76.55310878157616
time_elpased: 1.995
batch start
#iterations: 192
currently lose_sum: 77.08268737792969
time_elpased: 2.013
batch start
#iterations: 193
currently lose_sum: 76.40857875347137
time_elpased: 2.014
batch start
#iterations: 194
currently lose_sum: 77.00577667355537
time_elpased: 1.99
batch start
#iterations: 195
currently lose_sum: 76.81075829267502
time_elpased: 2.006
batch start
#iterations: 196
currently lose_sum: 77.2012468278408
time_elpased: 1.993
batch start
#iterations: 197
currently lose_sum: 76.76097717881203
time_elpased: 2.037
batch start
#iterations: 198
currently lose_sum: 76.53834021091461
time_elpased: 1.983
batch start
#iterations: 199
currently lose_sum: 77.10486215353012
time_elpased: 2.002
start validation test
0.626030927835
0.650820877236
0.546670783164
0.594216678785
0.626170256809
76.165
batch start
#iterations: 200
currently lose_sum: 77.4926863014698
time_elpased: 2.038
batch start
#iterations: 201
currently lose_sum: 76.75640177726746
time_elpased: 2.167
batch start
#iterations: 202
currently lose_sum: 76.62646463513374
time_elpased: 2.095
batch start
#iterations: 203
currently lose_sum: 76.85936161875725
time_elpased: 1.999
batch start
#iterations: 204
currently lose_sum: 75.89634880423546
time_elpased: 2.036
batch start
#iterations: 205
currently lose_sum: 76.18633037805557
time_elpased: 2.237
batch start
#iterations: 206
currently lose_sum: 76.28425937891006
time_elpased: 2.046
batch start
#iterations: 207
currently lose_sum: 75.86634474992752
time_elpased: 2.113
batch start
#iterations: 208
currently lose_sum: 76.22195616364479
time_elpased: 2.062
batch start
#iterations: 209
currently lose_sum: 76.34120243787766
time_elpased: 2.111
batch start
#iterations: 210
currently lose_sum: 76.83575204014778
time_elpased: 2.088
batch start
#iterations: 211
currently lose_sum: 76.24902772903442
time_elpased: 2.205
batch start
#iterations: 212
currently lose_sum: 76.0530244410038
time_elpased: 2.093
batch start
#iterations: 213
currently lose_sum: 75.58686882257462
time_elpased: 2.032
batch start
#iterations: 214
currently lose_sum: 75.93009549379349
time_elpased: 2.058
batch start
#iterations: 215
currently lose_sum: 76.08099973201752
time_elpased: 2.027
batch start
#iterations: 216
currently lose_sum: 76.20606091618538
time_elpased: 2.011
batch start
#iterations: 217
currently lose_sum: 75.89355334639549
time_elpased: 2.062
batch start
#iterations: 218
currently lose_sum: 75.51115933060646
time_elpased: 2.157
batch start
#iterations: 219
currently lose_sum: 75.89493998885155
time_elpased: 2.066
start validation test
0.642164948454
0.65891650441
0.592055161058
0.623699045967
0.642252923914
75.607
batch start
#iterations: 220
currently lose_sum: 76.44043198227882
time_elpased: 2.017
batch start
#iterations: 221
currently lose_sum: 76.27624291181564
time_elpased: 2.068
batch start
#iterations: 222
currently lose_sum: 75.69956755638123
time_elpased: 2.162
batch start
#iterations: 223
currently lose_sum: 75.57054296135902
time_elpased: 2.1
batch start
#iterations: 224
currently lose_sum: 75.83241692185402
time_elpased: 1.942
batch start
#iterations: 225
currently lose_sum: 76.05651235580444
time_elpased: 1.999
batch start
#iterations: 226
currently lose_sum: 75.70601961016655
time_elpased: 2.018
batch start
#iterations: 227
currently lose_sum: 75.6008343398571
time_elpased: 2.072
batch start
#iterations: 228
currently lose_sum: 75.0816610455513
time_elpased: 2.064
batch start
#iterations: 229
currently lose_sum: 74.86422663927078
time_elpased: 2.086
batch start
#iterations: 230
currently lose_sum: 75.2528808414936
time_elpased: 2.095
batch start
#iterations: 231
currently lose_sum: 75.02807223796844
time_elpased: 2.059
batch start
#iterations: 232
currently lose_sum: 74.51235032081604
time_elpased: 2.197
batch start
#iterations: 233
currently lose_sum: 74.91279488801956
time_elpased: 2.095
batch start
#iterations: 234
currently lose_sum: 75.8935815691948
time_elpased: 2.093
batch start
#iterations: 235
currently lose_sum: 75.73197627067566
time_elpased: 2.131
batch start
#iterations: 236
currently lose_sum: 74.70083892345428
time_elpased: 2.231
batch start
#iterations: 237
currently lose_sum: 75.37911495566368
time_elpased: 2.269
batch start
#iterations: 238
currently lose_sum: 75.29343634843826
time_elpased: 2.032
batch start
#iterations: 239
currently lose_sum: 74.38438442349434
time_elpased: 2.083
start validation test
0.647628865979
0.650853492512
0.639600699804
0.645178033842
0.647642960663
74.887
batch start
#iterations: 240
currently lose_sum: 75.0037172138691
time_elpased: 2.332
batch start
#iterations: 241
currently lose_sum: 75.59664249420166
time_elpased: 2.21
batch start
#iterations: 242
currently lose_sum: 75.12423729896545
time_elpased: 2.115
batch start
#iterations: 243
currently lose_sum: 74.84040302038193
time_elpased: 2.3
batch start
#iterations: 244
currently lose_sum: 75.39275586605072
time_elpased: 2.12
batch start
#iterations: 245
currently lose_sum: 75.28593796491623
time_elpased: 2.191
batch start
#iterations: 246
currently lose_sum: 74.71202376484871
time_elpased: 2.116
batch start
#iterations: 247
currently lose_sum: 74.9204343855381
time_elpased: 2.086
batch start
#iterations: 248
currently lose_sum: 74.32398855686188
time_elpased: 2.25
batch start
#iterations: 249
currently lose_sum: 75.29425424337387
time_elpased: 2.117
batch start
#iterations: 250
currently lose_sum: 74.61097636818886
time_elpased: 2.123
batch start
#iterations: 251
currently lose_sum: 74.24330487847328
time_elpased: 2.122
batch start
#iterations: 252
currently lose_sum: 75.21798247098923
time_elpased: 2.111
batch start
#iterations: 253
currently lose_sum: 74.56676396727562
time_elpased: 2.108
batch start
#iterations: 254
currently lose_sum: 74.65045347809792
time_elpased: 2.156
batch start
#iterations: 255
currently lose_sum: 73.77580934762955
time_elpased: 2.086
batch start
#iterations: 256
currently lose_sum: 74.68681308627129
time_elpased: 2.121
batch start
#iterations: 257
currently lose_sum: 74.58552062511444
time_elpased: 2.208
batch start
#iterations: 258
currently lose_sum: 74.99607938528061
time_elpased: 2.092
batch start
#iterations: 259
currently lose_sum: 74.01805025339127
time_elpased: 2.112
start validation test
0.644432989691
0.660334432943
0.597406606977
0.627296304301
0.644515551759
79.642
batch start
#iterations: 260
currently lose_sum: 74.24514216184616
time_elpased: 2.123
batch start
#iterations: 261
currently lose_sum: 74.28923061490059
time_elpased: 2.108
batch start
#iterations: 262
currently lose_sum: 74.72074899077415
time_elpased: 2.148
batch start
#iterations: 263
currently lose_sum: 73.88519278168678
time_elpased: 2.128
batch start
#iterations: 264
currently lose_sum: 74.53220781683922
time_elpased: 2.279
batch start
#iterations: 265
currently lose_sum: 73.52618297934532
time_elpased: 2.041
batch start
#iterations: 266
currently lose_sum: 74.15060555934906
time_elpased: 2.163
batch start
#iterations: 267
currently lose_sum: 73.8423770070076
time_elpased: 2.184
batch start
#iterations: 268
currently lose_sum: 73.6448165178299
time_elpased: 2.151
batch start
#iterations: 269
currently lose_sum: 74.24242198467255
time_elpased: 2.141
batch start
#iterations: 270
currently lose_sum: 73.89853778481483
time_elpased: 2.177
batch start
#iterations: 271
currently lose_sum: 74.37489241361618
time_elpased: 2.124
batch start
#iterations: 272
currently lose_sum: 73.92196464538574
time_elpased: 2.048
batch start
#iterations: 273
currently lose_sum: 73.38671517372131
time_elpased: 2.043
batch start
#iterations: 274
currently lose_sum: 73.91322201490402
time_elpased: 1.997
batch start
#iterations: 275
currently lose_sum: 73.76497608423233
time_elpased: 2.1
batch start
#iterations: 276
currently lose_sum: 73.56776931881905
time_elpased: 2.102
batch start
#iterations: 277
currently lose_sum: 74.3296788930893
time_elpased: 2.083
batch start
#iterations: 278
currently lose_sum: 73.7019527554512
time_elpased: 2.156
batch start
#iterations: 279
currently lose_sum: 73.04012209177017
time_elpased: 2.132
start validation test
0.634432989691
0.651576394503
0.580631882268
0.614061819765
0.634527445833
81.600
batch start
#iterations: 280
currently lose_sum: 74.22582820057869
time_elpased: 2.101
batch start
#iterations: 281
currently lose_sum: 73.6740592122078
time_elpased: 2.105
batch start
#iterations: 282
currently lose_sum: 73.14136323332787
time_elpased: 2.101
batch start
#iterations: 283
currently lose_sum: 73.09956958889961
time_elpased: 2.073
batch start
#iterations: 284
currently lose_sum: 73.77888137102127
time_elpased: 2.09
batch start
#iterations: 285
currently lose_sum: 73.22585591673851
time_elpased: 2.024
batch start
#iterations: 286
currently lose_sum: 73.05537781119347
time_elpased: 2.112
batch start
#iterations: 287
currently lose_sum: 73.63759291172028
time_elpased: 2.08
batch start
#iterations: 288
currently lose_sum: 73.07712712883949
time_elpased: 2.07
batch start
#iterations: 289
currently lose_sum: 72.78188541531563
time_elpased: 2.099
batch start
#iterations: 290
currently lose_sum: 73.29032701253891
time_elpased: 2.101
batch start
#iterations: 291
currently lose_sum: 72.84078848361969
time_elpased: 2.094
batch start
#iterations: 292
currently lose_sum: 73.72000229358673
time_elpased: 2.074
batch start
#iterations: 293
currently lose_sum: 72.74285131692886
time_elpased: 2.148
batch start
#iterations: 294
currently lose_sum: 72.31559041142464
time_elpased: 2.11
batch start
#iterations: 295
currently lose_sum: 72.6776212155819
time_elpased: 2.147
batch start
#iterations: 296
currently lose_sum: 72.54142421483994
time_elpased: 2.065
batch start
#iterations: 297
currently lose_sum: 73.14696902036667
time_elpased: 2.103
batch start
#iterations: 298
currently lose_sum: 72.79142054915428
time_elpased: 2.07
batch start
#iterations: 299
currently lose_sum: 72.4274976849556
time_elpased: 2.102
start validation test
0.627731958763
0.643770888556
0.574868786663
0.60737196912
0.627824768215
80.985
batch start
#iterations: 300
currently lose_sum: 73.03413188457489
time_elpased: 2.109
batch start
#iterations: 301
currently lose_sum: 72.70537972450256
time_elpased: 2.093
batch start
#iterations: 302
currently lose_sum: 73.54026809334755
time_elpased: 2.175
batch start
#iterations: 303
currently lose_sum: 73.12650656700134
time_elpased: 2.112
batch start
#iterations: 304
currently lose_sum: 72.6186494231224
time_elpased: 2.047
batch start
#iterations: 305
currently lose_sum: 72.61878517270088
time_elpased: 2.027
batch start
#iterations: 306
currently lose_sum: 73.29852819442749
time_elpased: 2.172
batch start
#iterations: 307
currently lose_sum: 72.29580929875374
time_elpased: 2.114
batch start
#iterations: 308
currently lose_sum: 72.64476323127747
time_elpased: 2.117
batch start
#iterations: 309
currently lose_sum: 72.7400980591774
time_elpased: 2.165
batch start
#iterations: 310
currently lose_sum: 72.52586841583252
time_elpased: 2.14
batch start
#iterations: 311
currently lose_sum: 73.29881712794304
time_elpased: 2.388
batch start
#iterations: 312
currently lose_sum: 72.99807876348495
time_elpased: 2.138
batch start
#iterations: 313
currently lose_sum: 72.79265230894089
time_elpased: 2.115
batch start
#iterations: 314
currently lose_sum: 72.17451962828636
time_elpased: 2.142
batch start
#iterations: 315
currently lose_sum: 72.47071370482445
time_elpased: 2.228
batch start
#iterations: 316
currently lose_sum: 72.5790077149868
time_elpased: 2.132
batch start
#iterations: 317
currently lose_sum: 72.63827368617058
time_elpased: 2.181
batch start
#iterations: 318
currently lose_sum: 72.11380559206009
time_elpased: 2.11
batch start
#iterations: 319
currently lose_sum: 72.37573140859604
time_elpased: 2.146
start validation test
0.638041237113
0.643764002987
0.620973551508
0.632163436354
0.638071202068
81.188
batch start
#iterations: 320
currently lose_sum: 72.67205014824867
time_elpased: 2.353
batch start
#iterations: 321
currently lose_sum: 72.88226106762886
time_elpased: 2.259
batch start
#iterations: 322
currently lose_sum: 71.8074386715889
time_elpased: 2.093
batch start
#iterations: 323
currently lose_sum: 72.1518802344799
time_elpased: 2.12
batch start
#iterations: 324
currently lose_sum: 72.09533035755157
time_elpased: 2.151
batch start
#iterations: 325
currently lose_sum: 72.56012850999832
time_elpased: 2.088
batch start
#iterations: 326
currently lose_sum: 72.29129341244698
time_elpased: 2.382
batch start
#iterations: 327
currently lose_sum: 71.73121502995491
time_elpased: 2.25
batch start
#iterations: 328
currently lose_sum: 71.62464439868927
time_elpased: 2.124
batch start
#iterations: 329
currently lose_sum: 72.42475163936615
time_elpased: 2.116
batch start
#iterations: 330
currently lose_sum: 72.33908298611641
time_elpased: 2.393
batch start
#iterations: 331
currently lose_sum: 72.56788486242294
time_elpased: 2.107
batch start
#iterations: 332
currently lose_sum: 71.2783271074295
time_elpased: 2.089
batch start
#iterations: 333
currently lose_sum: 71.77708491683006
time_elpased: 2.319
batch start
#iterations: 334
currently lose_sum: 72.41641557216644
time_elpased: 2.157
batch start
#iterations: 335
currently lose_sum: 72.91979438066483
time_elpased: 2.2
batch start
#iterations: 336
currently lose_sum: 72.12164252996445
time_elpased: 2.121
batch start
#iterations: 337
currently lose_sum: 72.2712234556675
time_elpased: 2.121
batch start
#iterations: 338
currently lose_sum: 72.26731875538826
time_elpased: 2.082
batch start
#iterations: 339
currently lose_sum: 71.73011529445648
time_elpased: 2.029
start validation test
0.635360824742
0.655415735623
0.573530925183
0.611745334797
0.635469376668
87.862
batch start
#iterations: 340
currently lose_sum: 71.29973131418228
time_elpased: 2.045
batch start
#iterations: 341
currently lose_sum: 71.67677694559097
time_elpased: 2.095
batch start
#iterations: 342
currently lose_sum: 71.94515505433083
time_elpased: 2.017
batch start
#iterations: 343
currently lose_sum: 71.32474508881569
time_elpased: 2.078
batch start
#iterations: 344
currently lose_sum: 71.2888375222683
time_elpased: 2.061
batch start
#iterations: 345
currently lose_sum: 71.65906742215157
time_elpased: 2.061
batch start
#iterations: 346
currently lose_sum: 71.13121411204338
time_elpased: 2.202
batch start
#iterations: 347
currently lose_sum: 72.19245326519012
time_elpased: 2.124
batch start
#iterations: 348
currently lose_sum: 71.95003080368042
time_elpased: 2.109
batch start
#iterations: 349
currently lose_sum: 71.74112638831139
time_elpased: 2.166
batch start
#iterations: 350
currently lose_sum: 71.083247423172
time_elpased: 2.06
batch start
#iterations: 351
currently lose_sum: 71.33466038107872
time_elpased: 2.105
batch start
#iterations: 352
currently lose_sum: 71.00716257095337
time_elpased: 2.112
batch start
#iterations: 353
currently lose_sum: 70.53254732489586
time_elpased: 2.056
batch start
#iterations: 354
currently lose_sum: 72.11903962492943
time_elpased: 2.018
batch start
#iterations: 355
currently lose_sum: 71.89466878771782
time_elpased: 2.082
batch start
#iterations: 356
currently lose_sum: 71.72903579473495
time_elpased: 2.078
batch start
#iterations: 357
currently lose_sum: 70.95232892036438
time_elpased: 2.046
batch start
#iterations: 358
currently lose_sum: 71.0128048658371
time_elpased: 2.015
batch start
#iterations: 359
currently lose_sum: 70.8490743637085
time_elpased: 1.968
start validation test
0.628350515464
0.654810423614
0.545641658948
0.595262153363
0.628495723618
88.836
batch start
#iterations: 360
currently lose_sum: 71.00739416480064
time_elpased: 2.058
batch start
#iterations: 361
currently lose_sum: 71.15411987900734
time_elpased: 2.011
batch start
#iterations: 362
currently lose_sum: 70.91761326789856
time_elpased: 2.005
batch start
#iterations: 363
currently lose_sum: 71.17619043588638
time_elpased: 2.01
batch start
#iterations: 364
currently lose_sum: 70.95903086662292
time_elpased: 2.076
batch start
#iterations: 365
currently lose_sum: 71.78350400924683
time_elpased: 2.129
batch start
#iterations: 366
currently lose_sum: 69.75579008460045
time_elpased: 2.043
batch start
#iterations: 367
currently lose_sum: 71.23314905166626
time_elpased: 2.07
batch start
#iterations: 368
currently lose_sum: 71.11481219530106
time_elpased: 2.095
batch start
#iterations: 369
currently lose_sum: 70.8113081753254
time_elpased: 2.048
batch start
#iterations: 370
currently lose_sum: 70.72431516647339
time_elpased: 1.995
batch start
#iterations: 371
currently lose_sum: 69.94461843371391
time_elpased: 2.058
batch start
#iterations: 372
currently lose_sum: 70.82248491048813
time_elpased: 2.102
batch start
#iterations: 373
currently lose_sum: 70.74628984928131
time_elpased: 2.091
batch start
#iterations: 374
currently lose_sum: 70.20410975813866
time_elpased: 2.072
batch start
#iterations: 375
currently lose_sum: 71.13737440109253
time_elpased: 2.012
batch start
#iterations: 376
currently lose_sum: 71.13738217949867
time_elpased: 2.0
batch start
#iterations: 377
currently lose_sum: 70.70482236146927
time_elpased: 1.996
batch start
#iterations: 378
currently lose_sum: 71.23913285136223
time_elpased: 2.27
batch start
#iterations: 379
currently lose_sum: 69.8818983733654
time_elpased: 2.093
start validation test
0.62587628866
0.647758682851
0.554697952043
0.5976272314
0.626001253208
89.899
batch start
#iterations: 380
currently lose_sum: 70.61595448851585
time_elpased: 2.077
batch start
#iterations: 381
currently lose_sum: 70.26757007837296
time_elpased: 2.082
batch start
#iterations: 382
currently lose_sum: 70.08620029687881
time_elpased: 2.172
batch start
#iterations: 383
currently lose_sum: 71.44762524962425
time_elpased: 2.09
batch start
#iterations: 384
currently lose_sum: 70.23855149745941
time_elpased: 2.053
batch start
#iterations: 385
currently lose_sum: 70.0976352095604
time_elpased: 2.345
batch start
#iterations: 386
currently lose_sum: 70.39942261576653
time_elpased: 2.274
batch start
#iterations: 387
currently lose_sum: 71.09736502170563
time_elpased: 2.072
batch start
#iterations: 388
currently lose_sum: 69.98654735088348
time_elpased: 2.107
batch start
#iterations: 389
currently lose_sum: 70.69245934486389
time_elpased: 2.121
batch start
#iterations: 390
currently lose_sum: 69.98969152569771
time_elpased: 2.167
batch start
#iterations: 391
currently lose_sum: 70.65121507644653
time_elpased: 2.136
batch start
#iterations: 392
currently lose_sum: 69.95963922142982
time_elpased: 2.123
batch start
#iterations: 393
currently lose_sum: 70.63727939128876
time_elpased: 2.144
batch start
#iterations: 394
currently lose_sum: 70.10215476155281
time_elpased: 2.064
batch start
#iterations: 395
currently lose_sum: 70.19334697723389
time_elpased: 2.08
batch start
#iterations: 396
currently lose_sum: 70.22617962956429
time_elpased: 2.115
batch start
#iterations: 397
currently lose_sum: 68.70159074664116
time_elpased: 2.202
batch start
#iterations: 398
currently lose_sum: 69.54260948300362
time_elpased: 2.167
batch start
#iterations: 399
currently lose_sum: 69.74720847606659
time_elpased: 2.053
start validation test
0.613608247423
0.639387473327
0.52423587527
0.576114001357
0.613765154407
89.862
acc: 0.647
pre: 0.657
rec: 0.615
F1: 0.636
auc: 0.647
