start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.07569807767868
time_elpased: 2.24
batch start
#iterations: 1
currently lose_sum: 98.91262763738632
time_elpased: 2.173
batch start
#iterations: 2
currently lose_sum: 98.21673601865768
time_elpased: 2.2
batch start
#iterations: 3
currently lose_sum: 97.08097720146179
time_elpased: 2.166
batch start
#iterations: 4
currently lose_sum: 96.70132595300674
time_elpased: 2.144
batch start
#iterations: 5
currently lose_sum: 95.66892468929291
time_elpased: 2.164
batch start
#iterations: 6
currently lose_sum: 94.81432366371155
time_elpased: 2.107
batch start
#iterations: 7
currently lose_sum: 94.2351438999176
time_elpased: 2.176
batch start
#iterations: 8
currently lose_sum: 93.6068884730339
time_elpased: 2.148
batch start
#iterations: 9
currently lose_sum: 93.23050194978714
time_elpased: 2.184
batch start
#iterations: 10
currently lose_sum: 93.01164847612381
time_elpased: 2.175
batch start
#iterations: 11
currently lose_sum: 92.59087830781937
time_elpased: 2.182
batch start
#iterations: 12
currently lose_sum: 92.30105900764465
time_elpased: 2.169
batch start
#iterations: 13
currently lose_sum: 92.32231748104095
time_elpased: 2.158
batch start
#iterations: 14
currently lose_sum: 91.83820515871048
time_elpased: 2.163
batch start
#iterations: 15
currently lose_sum: 91.43057733774185
time_elpased: 2.368
batch start
#iterations: 16
currently lose_sum: 91.07981526851654
time_elpased: 2.17
batch start
#iterations: 17
currently lose_sum: 91.21752369403839
time_elpased: 2.216
batch start
#iterations: 18
currently lose_sum: 90.60004311800003
time_elpased: 2.218
batch start
#iterations: 19
currently lose_sum: 90.15260112285614
time_elpased: 2.039
start validation test
0.661546391753
0.649743297205
0.703375874846
0.675496688742
0.661477280669
59.704
batch start
#iterations: 20
currently lose_sum: 90.07134854793549
time_elpased: 2.14
batch start
#iterations: 21
currently lose_sum: 90.14927345514297
time_elpased: 2.149
batch start
#iterations: 22
currently lose_sum: 89.8314637541771
time_elpased: 2.21
batch start
#iterations: 23
currently lose_sum: 89.76045620441437
time_elpased: 2.288
batch start
#iterations: 24
currently lose_sum: 88.63176989555359
time_elpased: 2.21
batch start
#iterations: 25
currently lose_sum: 88.76373666524887
time_elpased: 2.183
batch start
#iterations: 26
currently lose_sum: 89.1229630112648
time_elpased: 2.134
batch start
#iterations: 27
currently lose_sum: 88.54201120138168
time_elpased: 2.166
batch start
#iterations: 28
currently lose_sum: 88.5149387717247
time_elpased: 2.151
batch start
#iterations: 29
currently lose_sum: 88.72149527072906
time_elpased: 2.161
batch start
#iterations: 30
currently lose_sum: 88.51414597034454
time_elpased: 2.154
batch start
#iterations: 31
currently lose_sum: 87.60051238536835
time_elpased: 2.6
batch start
#iterations: 32
currently lose_sum: 87.30869340896606
time_elpased: 2.13
batch start
#iterations: 33
currently lose_sum: 88.09272021055222
time_elpased: 2.162
batch start
#iterations: 34
currently lose_sum: 88.14392328262329
time_elpased: 2.082
batch start
#iterations: 35
currently lose_sum: 87.77969419956207
time_elpased: 2.091
batch start
#iterations: 36
currently lose_sum: 87.71398156881332
time_elpased: 2.086
batch start
#iterations: 37
currently lose_sum: 87.18691056966782
time_elpased: 2.24
batch start
#iterations: 38
currently lose_sum: 86.71684247255325
time_elpased: 2.131
batch start
#iterations: 39
currently lose_sum: 87.34953689575195
time_elpased: 2.23
start validation test
0.663659793814
0.646201777696
0.725813091807
0.683697707111
0.663557103524
59.210
batch start
#iterations: 40
currently lose_sum: 86.45631164312363
time_elpased: 2.141
batch start
#iterations: 41
currently lose_sum: 86.75938212871552
time_elpased: 2.126
batch start
#iterations: 42
currently lose_sum: 87.21874350309372
time_elpased: 2.182
batch start
#iterations: 43
currently lose_sum: 86.29189759492874
time_elpased: 2.107
batch start
#iterations: 44
currently lose_sum: 86.51088184118271
time_elpased: 2.192
batch start
#iterations: 45
currently lose_sum: 86.47975927591324
time_elpased: 2.19
batch start
#iterations: 46
currently lose_sum: 85.84695827960968
time_elpased: 2.259
batch start
#iterations: 47
currently lose_sum: 85.7587480545044
time_elpased: 2.124
batch start
#iterations: 48
currently lose_sum: 86.27373194694519
time_elpased: 2.091
batch start
#iterations: 49
currently lose_sum: 85.55759382247925
time_elpased: 2.187
batch start
#iterations: 50
currently lose_sum: 85.84829449653625
time_elpased: 2.087
batch start
#iterations: 51
currently lose_sum: 86.02704054117203
time_elpased: 2.221
batch start
#iterations: 52
currently lose_sum: 85.40038323402405
time_elpased: 2.127
batch start
#iterations: 53
currently lose_sum: 85.43529373407364
time_elpased: 2.202
batch start
#iterations: 54
currently lose_sum: 85.31486463546753
time_elpased: 2.181
batch start
#iterations: 55
currently lose_sum: 85.25190514326096
time_elpased: 2.096
batch start
#iterations: 56
currently lose_sum: 84.99884617328644
time_elpased: 2.073
batch start
#iterations: 57
currently lose_sum: 84.75981044769287
time_elpased: 2.095
batch start
#iterations: 58
currently lose_sum: 84.79551863670349
time_elpased: 2.194
batch start
#iterations: 59
currently lose_sum: 85.42279309034348
time_elpased: 2.162
start validation test
0.663917525773
0.662895005097
0.669308357349
0.666086243982
0.663908618988
61.241
batch start
#iterations: 60
currently lose_sum: 84.40821045637131
time_elpased: 2.148
batch start
#iterations: 61
currently lose_sum: 85.0760834813118
time_elpased: 2.217
batch start
#iterations: 62
currently lose_sum: 84.5961366891861
time_elpased: 2.144
batch start
#iterations: 63
currently lose_sum: 84.35976910591125
time_elpased: 2.184
batch start
#iterations: 64
currently lose_sum: 84.81963855028152
time_elpased: 2.199
batch start
#iterations: 65
currently lose_sum: 84.43260425329208
time_elpased: 2.094
batch start
#iterations: 66
currently lose_sum: 84.21186995506287
time_elpased: 2.142
batch start
#iterations: 67
currently lose_sum: 84.09486413002014
time_elpased: 2.211
batch start
#iterations: 68
currently lose_sum: 84.4599621295929
time_elpased: 2.235
batch start
#iterations: 69
currently lose_sum: 83.78475272655487
time_elpased: 2.142
batch start
#iterations: 70
currently lose_sum: 83.91480940580368
time_elpased: 2.173
batch start
#iterations: 71
currently lose_sum: 83.29422426223755
time_elpased: 2.167
batch start
#iterations: 72
currently lose_sum: 83.63766676187515
time_elpased: 2.172
batch start
#iterations: 73
currently lose_sum: 83.34002751111984
time_elpased: 2.213
batch start
#iterations: 74
currently lose_sum: 83.44666987657547
time_elpased: 2.184
batch start
#iterations: 75
currently lose_sum: 83.127312541008
time_elpased: 2.218
batch start
#iterations: 76
currently lose_sum: 83.55038520693779
time_elpased: 2.222
batch start
#iterations: 77
currently lose_sum: 83.0286255478859
time_elpased: 2.175
batch start
#iterations: 78
currently lose_sum: 83.56687659025192
time_elpased: 2.193
batch start
#iterations: 79
currently lose_sum: 83.86236810684204
time_elpased: 2.238
start validation test
0.658762886598
0.652362204724
0.682173734047
0.666934996981
0.658724206966
61.646
batch start
#iterations: 80
currently lose_sum: 82.69091862440109
time_elpased: 2.212
batch start
#iterations: 81
currently lose_sum: 83.232062458992
time_elpased: 2.215
batch start
#iterations: 82
currently lose_sum: 82.98079019784927
time_elpased: 2.203
batch start
#iterations: 83
currently lose_sum: 83.2160113453865
time_elpased: 2.206
batch start
#iterations: 84
currently lose_sum: 82.79466110467911
time_elpased: 2.169
batch start
#iterations: 85
currently lose_sum: 82.17479112744331
time_elpased: 2.338
batch start
#iterations: 86
currently lose_sum: 82.74198630452156
time_elpased: 2.26
batch start
#iterations: 87
currently lose_sum: 81.78791397809982
time_elpased: 2.217
batch start
#iterations: 88
currently lose_sum: 82.22032329440117
time_elpased: 2.398
batch start
#iterations: 89
currently lose_sum: 81.89875954389572
time_elpased: 2.253
batch start
#iterations: 90
currently lose_sum: 83.25067895650864
time_elpased: 2.233
batch start
#iterations: 91
currently lose_sum: 81.99230480194092
time_elpased: 2.201
batch start
#iterations: 92
currently lose_sum: 82.13442286849022
time_elpased: 2.164
batch start
#iterations: 93
currently lose_sum: 82.01503920555115
time_elpased: 2.155
batch start
#iterations: 94
currently lose_sum: 82.29299327731133
time_elpased: 2.141
batch start
#iterations: 95
currently lose_sum: 82.26868957281113
time_elpased: 2.214
batch start
#iterations: 96
currently lose_sum: 81.27873280644417
time_elpased: 2.243
batch start
#iterations: 97
currently lose_sum: 81.84786638617516
time_elpased: 2.228
batch start
#iterations: 98
currently lose_sum: 81.70632502436638
time_elpased: 2.206
batch start
#iterations: 99
currently lose_sum: 80.94783121347427
time_elpased: 2.427
start validation test
0.651082474227
0.648853419537
0.661074516262
0.65490695896
0.651065965277
64.096
batch start
#iterations: 100
currently lose_sum: 81.75243586301804
time_elpased: 2.244
batch start
#iterations: 101
currently lose_sum: 81.96785786747932
time_elpased: 2.082
batch start
#iterations: 102
currently lose_sum: 81.32863959670067
time_elpased: 2.063
batch start
#iterations: 103
currently lose_sum: 81.92942768335342
time_elpased: 2.09
batch start
#iterations: 104
currently lose_sum: 80.71972489356995
time_elpased: 2.069
batch start
#iterations: 105
currently lose_sum: 81.07512831687927
time_elpased: 2.17
batch start
#iterations: 106
currently lose_sum: 80.73959320783615
time_elpased: 2.177
batch start
#iterations: 107
currently lose_sum: 81.2930836379528
time_elpased: 2.161
batch start
#iterations: 108
currently lose_sum: 80.72199457883835
time_elpased: 2.269
batch start
#iterations: 109
currently lose_sum: 80.51437819004059
time_elpased: 2.099
batch start
#iterations: 110
currently lose_sum: 81.20650607347488
time_elpased: 2.16
batch start
#iterations: 111
currently lose_sum: 80.60516086220741
time_elpased: 2.17
batch start
#iterations: 112
currently lose_sum: 80.98173969984055
time_elpased: 2.27
batch start
#iterations: 113
currently lose_sum: 79.99042066931725
time_elpased: 2.179
batch start
#iterations: 114
currently lose_sum: 80.76425260305405
time_elpased: 2.219
batch start
#iterations: 115
currently lose_sum: 80.24478501081467
time_elpased: 2.153
batch start
#iterations: 116
currently lose_sum: 80.59748548269272
time_elpased: 2.105
batch start
#iterations: 117
currently lose_sum: 80.33962032198906
time_elpased: 2.094
batch start
#iterations: 118
currently lose_sum: 80.87148377299309
time_elpased: 2.053
batch start
#iterations: 119
currently lose_sum: 80.34931200742722
time_elpased: 2.061
start validation test
0.643556701031
0.647095893289
0.634108686702
0.640536466185
0.643572311133
65.161
batch start
#iterations: 120
currently lose_sum: 79.62792950868607
time_elpased: 2.103
batch start
#iterations: 121
currently lose_sum: 79.79926618933678
time_elpased: 2.081
batch start
#iterations: 122
currently lose_sum: 80.54231292009354
time_elpased: 2.142
batch start
#iterations: 123
currently lose_sum: 80.0471502840519
time_elpased: 2.152
batch start
#iterations: 124
currently lose_sum: 80.48171544075012
time_elpased: 2.163
batch start
#iterations: 125
currently lose_sum: 80.23126605153084
time_elpased: 2.258
batch start
#iterations: 126
currently lose_sum: 79.25995129346848
time_elpased: 2.233
batch start
#iterations: 127
currently lose_sum: 79.12808102369308
time_elpased: 2.091
batch start
#iterations: 128
currently lose_sum: 80.09892466664314
time_elpased: 2.173
batch start
#iterations: 129
currently lose_sum: 78.73378601670265
time_elpased: 2.106
batch start
#iterations: 130
currently lose_sum: 80.36430057883263
time_elpased: 2.108
batch start
#iterations: 131
currently lose_sum: 79.68912190198898
time_elpased: 2.34
batch start
#iterations: 132
currently lose_sum: 79.30234533548355
time_elpased: 2.236
batch start
#iterations: 133
currently lose_sum: 79.40497595071793
time_elpased: 2.071
batch start
#iterations: 134
currently lose_sum: 79.69990411400795
time_elpased: 2.132
batch start
#iterations: 135
currently lose_sum: 79.70064035058022
time_elpased: 2.138
batch start
#iterations: 136
currently lose_sum: 79.24386239051819
time_elpased: 2.054
batch start
#iterations: 137
currently lose_sum: 79.17050623893738
time_elpased: 2.121
batch start
#iterations: 138
currently lose_sum: 79.49301040172577
time_elpased: 2.015
batch start
#iterations: 139
currently lose_sum: 78.25723984837532
time_elpased: 2.029
start validation test
0.641391752577
0.643712886759
0.635961300947
0.63981361636
0.641400724823
68.436
batch start
#iterations: 140
currently lose_sum: 79.41185837984085
time_elpased: 2.173
batch start
#iterations: 141
currently lose_sum: 78.90344843268394
time_elpased: 2.147
batch start
#iterations: 142
currently lose_sum: 78.52746120095253
time_elpased: 2.115
batch start
#iterations: 143
currently lose_sum: 78.94790232181549
time_elpased: 2.171
batch start
#iterations: 144
currently lose_sum: 79.46237432956696
time_elpased: 2.144
batch start
#iterations: 145
currently lose_sum: 77.98498079180717
time_elpased: 2.215
batch start
#iterations: 146
currently lose_sum: 79.00580072402954
time_elpased: 2.131
batch start
#iterations: 147
currently lose_sum: 78.76384034752846
time_elpased: 2.087
batch start
#iterations: 148
currently lose_sum: 78.74276587367058
time_elpased: 2.107
batch start
#iterations: 149
currently lose_sum: 78.90869361162186
time_elpased: 2.191
batch start
#iterations: 150
currently lose_sum: 78.59869903326035
time_elpased: 2.111
batch start
#iterations: 151
currently lose_sum: 79.1456869840622
time_elpased: 2.223
batch start
#iterations: 152
currently lose_sum: 78.45051142573357
time_elpased: 2.19
batch start
#iterations: 153
currently lose_sum: 78.74339032173157
time_elpased: 2.168
batch start
#iterations: 154
currently lose_sum: 78.74371021986008
time_elpased: 2.344
batch start
#iterations: 155
currently lose_sum: 78.14445692300797
time_elpased: 2.198
batch start
#iterations: 156
currently lose_sum: 78.26743939518929
time_elpased: 2.09
batch start
#iterations: 157
currently lose_sum: 78.27131694555283
time_elpased: 2.148
batch start
#iterations: 158
currently lose_sum: 78.58687949180603
time_elpased: 2.157
batch start
#iterations: 159
currently lose_sum: 78.3981739282608
time_elpased: 2.189
start validation test
0.642474226804
0.646531730972
0.631226842322
0.638787626289
0.642492809843
68.584
batch start
#iterations: 160
currently lose_sum: 78.87986090779305
time_elpased: 2.158
batch start
#iterations: 161
currently lose_sum: 78.11253049969673
time_elpased: 2.168
batch start
#iterations: 162
currently lose_sum: 78.53963214159012
time_elpased: 2.201
batch start
#iterations: 163
currently lose_sum: 78.43880718946457
time_elpased: 2.177
batch start
#iterations: 164
currently lose_sum: 77.5909711420536
time_elpased: 2.257
batch start
#iterations: 165
currently lose_sum: 78.10936170816422
time_elpased: 2.178
batch start
#iterations: 166
currently lose_sum: 77.75789541006088
time_elpased: 2.216
batch start
#iterations: 167
currently lose_sum: 77.72080740332603
time_elpased: 2.172
batch start
#iterations: 168
currently lose_sum: 77.803655564785
time_elpased: 2.289
batch start
#iterations: 169
currently lose_sum: 77.56581848859787
time_elpased: 2.249
batch start
#iterations: 170
currently lose_sum: 77.85462138056755
time_elpased: 2.215
batch start
#iterations: 171
currently lose_sum: 78.3893786072731
time_elpased: 2.236
batch start
#iterations: 172
currently lose_sum: 77.0535292327404
time_elpased: 2.268
batch start
#iterations: 173
currently lose_sum: 77.52352702617645
time_elpased: 2.057
batch start
#iterations: 174
currently lose_sum: 77.32900086045265
time_elpased: 2.02
batch start
#iterations: 175
currently lose_sum: 77.25704309344292
time_elpased: 2.256
batch start
#iterations: 176
currently lose_sum: 77.0025150179863
time_elpased: 2.129
batch start
#iterations: 177
currently lose_sum: 77.32148894667625
time_elpased: 2.402
batch start
#iterations: 178
currently lose_sum: 77.23187917470932
time_elpased: 2.17
batch start
#iterations: 179
currently lose_sum: 76.3971434533596
time_elpased: 2.334
start validation test
0.630824742268
0.639807313335
0.601482091396
0.620053050398
0.630873222484
72.094
batch start
#iterations: 180
currently lose_sum: 77.39810740947723
time_elpased: 2.2
batch start
#iterations: 181
currently lose_sum: 77.47580847144127
time_elpased: 2.145
batch start
#iterations: 182
currently lose_sum: 77.59258118271828
time_elpased: 2.099
batch start
#iterations: 183
currently lose_sum: 76.64132234454155
time_elpased: 2.083
batch start
#iterations: 184
currently lose_sum: 76.40465956926346
time_elpased: 2.051
batch start
#iterations: 185
currently lose_sum: 77.50915798544884
time_elpased: 2.099
batch start
#iterations: 186
currently lose_sum: 76.91214045882225
time_elpased: 2.029
batch start
#iterations: 187
currently lose_sum: 77.05670273303986
time_elpased: 2.085
batch start
#iterations: 188
currently lose_sum: 76.81864669919014
time_elpased: 2.068
batch start
#iterations: 189
currently lose_sum: 76.81913232803345
time_elpased: 2.079
batch start
#iterations: 190
currently lose_sum: 76.39227467775345
time_elpased: 2.042
batch start
#iterations: 191
currently lose_sum: 76.66425070166588
time_elpased: 2.081
batch start
#iterations: 192
currently lose_sum: 77.1479474902153
time_elpased: 2.044
batch start
#iterations: 193
currently lose_sum: 76.47046685218811
time_elpased: 2.044
batch start
#iterations: 194
currently lose_sum: 77.02610608935356
time_elpased: 2.228
batch start
#iterations: 195
currently lose_sum: 76.48045516014099
time_elpased: 2.099
batch start
#iterations: 196
currently lose_sum: 76.48642432689667
time_elpased: 2.237
batch start
#iterations: 197
currently lose_sum: 76.02949768304825
time_elpased: 2.048
batch start
#iterations: 198
currently lose_sum: 76.74118024110794
time_elpased: 2.087
batch start
#iterations: 199
currently lose_sum: 75.90588438510895
time_elpased: 2.125
start validation test
0.623144329897
0.636198890022
0.578118567312
0.605769749259
0.623218721904
72.083
batch start
#iterations: 200
currently lose_sum: 75.72297585010529
time_elpased: 2.118
batch start
#iterations: 201
currently lose_sum: 75.8892610669136
time_elpased: 2.128
batch start
#iterations: 202
currently lose_sum: 76.0807132422924
time_elpased: 2.111
batch start
#iterations: 203
currently lose_sum: 76.42934396862984
time_elpased: 2.121
batch start
#iterations: 204
currently lose_sum: 76.24210268259048
time_elpased: 2.149
batch start
#iterations: 205
currently lose_sum: 76.00574457645416
time_elpased: 2.17
batch start
#iterations: 206
currently lose_sum: 76.90882766246796
time_elpased: 2.106
batch start
#iterations: 207
currently lose_sum: 76.14583963155746
time_elpased: 2.082
batch start
#iterations: 208
currently lose_sum: 75.86325743794441
time_elpased: 2.12
batch start
#iterations: 209
currently lose_sum: 75.68585959076881
time_elpased: 2.075
batch start
#iterations: 210
currently lose_sum: 75.73432323336601
time_elpased: 2.037
batch start
#iterations: 211
currently lose_sum: 75.24206924438477
time_elpased: 2.12
batch start
#iterations: 212
currently lose_sum: 76.00935280323029
time_elpased: 2.159
batch start
#iterations: 213
currently lose_sum: 76.41679388284683
time_elpased: 2.069
batch start
#iterations: 214
currently lose_sum: 75.4901611506939
time_elpased: 1.923
batch start
#iterations: 215
currently lose_sum: 75.42029789090157
time_elpased: 2.076
batch start
#iterations: 216
currently lose_sum: 75.52620166540146
time_elpased: 2.076
batch start
#iterations: 217
currently lose_sum: 75.4797870516777
time_elpased: 2.16
batch start
#iterations: 218
currently lose_sum: 75.1715304851532
time_elpased: 2.134
batch start
#iterations: 219
currently lose_sum: 75.56779795885086
time_elpased: 1.983
start validation test
0.629381443299
0.642905634759
0.584808563195
0.612482483561
0.62945508705
74.681
batch start
#iterations: 220
currently lose_sum: 74.87977534532547
time_elpased: 2.077
batch start
#iterations: 221
currently lose_sum: 74.95012146234512
time_elpased: 2.152
batch start
#iterations: 222
currently lose_sum: 75.18626579642296
time_elpased: 2.146
batch start
#iterations: 223
currently lose_sum: 74.27560782432556
time_elpased: 2.109
batch start
#iterations: 224
currently lose_sum: 75.52878996729851
time_elpased: 2.169
batch start
#iterations: 225
currently lose_sum: 75.43984919786453
time_elpased: 2.112
batch start
#iterations: 226
currently lose_sum: 74.8317391872406
time_elpased: 2.139
batch start
#iterations: 227
currently lose_sum: 74.71410915255547
time_elpased: 2.149
batch start
#iterations: 228
currently lose_sum: 74.66744783520699
time_elpased: 2.175
batch start
#iterations: 229
currently lose_sum: 75.06288784742355
time_elpased: 2.168
batch start
#iterations: 230
currently lose_sum: 74.63143989443779
time_elpased: 2.308
batch start
#iterations: 231
currently lose_sum: 75.47779983282089
time_elpased: 2.291
batch start
#iterations: 232
currently lose_sum: 75.12565293908119
time_elpased: 2.131
batch start
#iterations: 233
currently lose_sum: 74.75874650478363
time_elpased: 2.11
batch start
#iterations: 234
currently lose_sum: 75.18504756689072
time_elpased: 2.16
batch start
#iterations: 235
currently lose_sum: 74.9553898870945
time_elpased: 2.297
batch start
#iterations: 236
currently lose_sum: 74.94397860765457
time_elpased: 2.176
batch start
#iterations: 237
currently lose_sum: 74.2573814690113
time_elpased: 2.167
batch start
#iterations: 238
currently lose_sum: 75.33639007806778
time_elpased: 2.159
batch start
#iterations: 239
currently lose_sum: 75.2364536523819
time_elpased: 2.257
start validation test
0.626134020619
0.629973614776
0.614347468094
0.622062425095
0.626153494477
73.258
batch start
#iterations: 240
currently lose_sum: 74.33149892091751
time_elpased: 2.22
batch start
#iterations: 241
currently lose_sum: 74.43968933820724
time_elpased: 2.164
batch start
#iterations: 242
currently lose_sum: 74.31936401128769
time_elpased: 2.177
batch start
#iterations: 243
currently lose_sum: 74.52450785040855
time_elpased: 2.356
batch start
#iterations: 244
currently lose_sum: 74.1340272128582
time_elpased: 2.212
batch start
#iterations: 245
currently lose_sum: 74.82206559181213
time_elpased: 2.203
batch start
#iterations: 246
currently lose_sum: 73.72857797145844
time_elpased: 2.169
batch start
#iterations: 247
currently lose_sum: 74.50285151600838
time_elpased: 2.141
batch start
#iterations: 248
currently lose_sum: 74.44494599103928
time_elpased: 2.176
batch start
#iterations: 249
currently lose_sum: 74.82440000772476
time_elpased: 2.208
batch start
#iterations: 250
currently lose_sum: 74.38777086138725
time_elpased: 2.177
batch start
#iterations: 251
currently lose_sum: 74.73371440172195
time_elpased: 2.121
batch start
#iterations: 252
currently lose_sum: 74.73425069451332
time_elpased: 2.168
batch start
#iterations: 253
currently lose_sum: 74.3493700325489
time_elpased: 2.17
batch start
#iterations: 254
currently lose_sum: 73.75529512763023
time_elpased: 2.211
batch start
#iterations: 255
currently lose_sum: 73.70818254351616
time_elpased: 2.315
batch start
#iterations: 256
currently lose_sum: 74.72060772776604
time_elpased: 2.175
batch start
#iterations: 257
currently lose_sum: 74.00442576408386
time_elpased: 2.218
batch start
#iterations: 258
currently lose_sum: 74.46851742267609
time_elpased: 2.201
batch start
#iterations: 259
currently lose_sum: 73.8772845864296
time_elpased: 2.089
start validation test
0.621649484536
0.640359168242
0.557842733635
0.596259625963
0.621754906677
78.056
batch start
#iterations: 260
currently lose_sum: 74.58575350046158
time_elpased: 2.226
batch start
#iterations: 261
currently lose_sum: 74.76183182001114
time_elpased: 2.188
batch start
#iterations: 262
currently lose_sum: 73.60887888073921
time_elpased: 2.181
batch start
#iterations: 263
currently lose_sum: 73.61851239204407
time_elpased: 2.169
batch start
#iterations: 264
currently lose_sum: 74.15723046660423
time_elpased: 2.214
batch start
#iterations: 265
currently lose_sum: 73.92590808868408
time_elpased: 2.179
batch start
#iterations: 266
currently lose_sum: 73.27345073223114
time_elpased: 2.109
batch start
#iterations: 267
currently lose_sum: 73.80419421195984
time_elpased: 2.078
batch start
#iterations: 268
currently lose_sum: 73.67443001270294
time_elpased: 2.115
batch start
#iterations: 269
currently lose_sum: 73.72660261392593
time_elpased: 2.245
batch start
#iterations: 270
currently lose_sum: 73.56242600083351
time_elpased: 2.149
batch start
#iterations: 271
currently lose_sum: 74.00273326039314
time_elpased: 2.175
batch start
#iterations: 272
currently lose_sum: 73.20617809891701
time_elpased: 2.168
batch start
#iterations: 273
currently lose_sum: 73.48757529258728
time_elpased: 2.156
batch start
#iterations: 274
currently lose_sum: 73.59871479868889
time_elpased: 2.174
batch start
#iterations: 275
currently lose_sum: 73.22399044036865
time_elpased: 2.132
batch start
#iterations: 276
currently lose_sum: 73.25628960132599
time_elpased: 2.184
batch start
#iterations: 277
currently lose_sum: 72.97243654727936
time_elpased: 2.127
batch start
#iterations: 278
currently lose_sum: 73.30904221534729
time_elpased: 2.134
batch start
#iterations: 279
currently lose_sum: 73.0430255830288
time_elpased: 2.125
start validation test
0.628917525773
0.657135722312
0.541683820502
0.593850493653
0.629061654159
88.926
batch start
#iterations: 280
currently lose_sum: 73.54534611105919
time_elpased: 2.131
batch start
#iterations: 281
currently lose_sum: 72.79941612482071
time_elpased: 2.122
batch start
#iterations: 282
currently lose_sum: 72.57134354114532
time_elpased: 2.182
batch start
#iterations: 283
currently lose_sum: 73.49129319190979
time_elpased: 2.148
batch start
#iterations: 284
currently lose_sum: 73.76883742213249
time_elpased: 2.137
batch start
#iterations: 285
currently lose_sum: 72.53325447440147
time_elpased: 2.155
batch start
#iterations: 286
currently lose_sum: 72.37357884645462
time_elpased: 2.2
batch start
#iterations: 287
currently lose_sum: 73.10753199458122
time_elpased: 2.161
batch start
#iterations: 288
currently lose_sum: 72.60882851481438
time_elpased: 2.228
batch start
#iterations: 289
currently lose_sum: 72.85908836126328
time_elpased: 2.201
batch start
#iterations: 290
currently lose_sum: 72.92149779200554
time_elpased: 2.183
batch start
#iterations: 291
currently lose_sum: 73.09103214740753
time_elpased: 2.156
batch start
#iterations: 292
currently lose_sum: 72.63906997442245
time_elpased: 2.196
batch start
#iterations: 293
currently lose_sum: 73.00364854931831
time_elpased: 2.215
batch start
#iterations: 294
currently lose_sum: 71.93963786959648
time_elpased: 2.47
batch start
#iterations: 295
currently lose_sum: 72.84953725337982
time_elpased: 2.172
batch start
#iterations: 296
currently lose_sum: 73.00346827507019
time_elpased: 2.096
batch start
#iterations: 297
currently lose_sum: 73.12766513228416
time_elpased: 2.107
batch start
#iterations: 298
currently lose_sum: 72.21345114707947
time_elpased: 2.121
batch start
#iterations: 299
currently lose_sum: 72.65967321395874
time_elpased: 2.242
start validation test
0.625979381443
0.644434006576
0.564841498559
0.602018429136
0.626080394054
80.644
batch start
#iterations: 300
currently lose_sum: 72.12132745981216
time_elpased: 2.171
batch start
#iterations: 301
currently lose_sum: 72.44218036532402
time_elpased: 2.281
batch start
#iterations: 302
currently lose_sum: 71.64315563440323
time_elpased: 2.212
batch start
#iterations: 303
currently lose_sum: 72.07545056939125
time_elpased: 2.204
batch start
#iterations: 304
currently lose_sum: 73.0431137084961
time_elpased: 2.173
batch start
#iterations: 305
currently lose_sum: 71.65538382530212
time_elpased: 2.138
batch start
#iterations: 306
currently lose_sum: 72.1406072974205
time_elpased: 2.468
batch start
#iterations: 307
currently lose_sum: 73.18459796905518
time_elpased: 2.161
batch start
#iterations: 308
currently lose_sum: 72.54726389050484
time_elpased: 2.207
batch start
#iterations: 309
currently lose_sum: 71.43922755122185
time_elpased: 2.222
batch start
#iterations: 310
currently lose_sum: 71.95282280445099
time_elpased: 2.205
batch start
#iterations: 311
currently lose_sum: 72.49935883283615
time_elpased: 2.14
batch start
#iterations: 312
currently lose_sum: 72.02818500995636
time_elpased: 2.359
batch start
#iterations: 313
currently lose_sum: 72.23809552192688
time_elpased: 2.279
batch start
#iterations: 314
currently lose_sum: 72.11620697379112
time_elpased: 2.192
batch start
#iterations: 315
currently lose_sum: 71.90443497896194
time_elpased: 2.163
batch start
#iterations: 316
currently lose_sum: 71.75972574949265
time_elpased: 2.233
batch start
#iterations: 317
currently lose_sum: 71.92333701252937
time_elpased: 2.265
batch start
#iterations: 318
currently lose_sum: 72.0574731528759
time_elpased: 2.36
batch start
#iterations: 319
currently lose_sum: 72.20372965931892
time_elpased: 2.285
start validation test
0.617783505155
0.636008984514
0.553725813092
0.592022008253
0.617889341903
81.471
batch start
#iterations: 320
currently lose_sum: 72.22448915243149
time_elpased: 2.169
batch start
#iterations: 321
currently lose_sum: 71.6941214799881
time_elpased: 2.227
batch start
#iterations: 322
currently lose_sum: 72.22520223259926
time_elpased: 2.179
batch start
#iterations: 323
currently lose_sum: 71.38548082113266
time_elpased: 2.148
batch start
#iterations: 324
currently lose_sum: 71.66424912214279
time_elpased: 2.142
batch start
#iterations: 325
currently lose_sum: 71.73908546566963
time_elpased: 2.203
batch start
#iterations: 326
currently lose_sum: 71.70894116163254
time_elpased: 2.222
batch start
#iterations: 327
currently lose_sum: 71.0822528898716
time_elpased: 2.202
batch start
#iterations: 328
currently lose_sum: 71.42126488685608
time_elpased: 2.202
batch start
#iterations: 329
currently lose_sum: 71.8540323972702
time_elpased: 2.277
batch start
#iterations: 330
currently lose_sum: 71.19647839665413
time_elpased: 2.098
batch start
#iterations: 331
currently lose_sum: 70.54207944869995
time_elpased: 2.066
batch start
#iterations: 332
currently lose_sum: 71.55000895261765
time_elpased: 2.131
batch start
#iterations: 333
currently lose_sum: 71.1916925907135
time_elpased: 2.081
batch start
#iterations: 334
currently lose_sum: 71.18058696389198
time_elpased: 2.08
batch start
#iterations: 335
currently lose_sum: 72.22371995449066
time_elpased: 2.081
batch start
#iterations: 336
currently lose_sum: 70.84929099678993
time_elpased: 2.124
batch start
#iterations: 337
currently lose_sum: 71.27703654766083
time_elpased: 2.099
batch start
#iterations: 338
currently lose_sum: 71.61205011606216
time_elpased: 2.213
batch start
#iterations: 339
currently lose_sum: 71.09064650535583
time_elpased: 2.252
start validation test
0.62706185567
0.65504311961
0.539419514203
0.591635152678
0.627206659208
94.659
batch start
#iterations: 340
currently lose_sum: 71.35055229067802
time_elpased: 2.344
batch start
#iterations: 341
currently lose_sum: 71.18768578767776
time_elpased: 2.201
batch start
#iterations: 342
currently lose_sum: 71.74263939261436
time_elpased: 2.16
batch start
#iterations: 343
currently lose_sum: 71.2325109243393
time_elpased: 2.22
batch start
#iterations: 344
currently lose_sum: 70.73009461164474
time_elpased: 2.14
batch start
#iterations: 345
currently lose_sum: 71.109787940979
time_elpased: 2.059
batch start
#iterations: 346
currently lose_sum: 70.39417687058449
time_elpased: 2.136
batch start
#iterations: 347
currently lose_sum: 70.43841376900673
time_elpased: 2.121
batch start
#iterations: 348
currently lose_sum: 70.87475380301476
time_elpased: 2.105
batch start
#iterations: 349
currently lose_sum: 71.62268036603928
time_elpased: 2.192
batch start
#iterations: 350
currently lose_sum: 70.25673946738243
time_elpased: 2.066
batch start
#iterations: 351
currently lose_sum: 70.912010461092
time_elpased: 2.083
batch start
#iterations: 352
currently lose_sum: 71.01635813713074
time_elpased: 2.107
batch start
#iterations: 353
currently lose_sum: 70.82804054021835
time_elpased: 2.108
batch start
#iterations: 354
currently lose_sum: 71.03132790327072
time_elpased: 2.062
batch start
#iterations: 355
currently lose_sum: 71.01768377423286
time_elpased: 2.207
batch start
#iterations: 356
currently lose_sum: 71.05535790324211
time_elpased: 2.218
batch start
#iterations: 357
currently lose_sum: 71.35141441226006
time_elpased: 2.219
batch start
#iterations: 358
currently lose_sum: 70.46955180168152
time_elpased: 2.074
batch start
#iterations: 359
currently lose_sum: 69.90258505940437
time_elpased: 2.155
start validation test
0.620618556701
0.645792079208
0.537052284891
0.586423915487
0.620756625717
90.363
batch start
#iterations: 360
currently lose_sum: 70.61857357621193
time_elpased: 2.145
batch start
#iterations: 361
currently lose_sum: 70.53707382082939
time_elpased: 2.053
batch start
#iterations: 362
currently lose_sum: 70.83298218250275
time_elpased: 2.182
batch start
#iterations: 363
currently lose_sum: 70.41550353169441
time_elpased: 2.141
batch start
#iterations: 364
currently lose_sum: 70.31884863972664
time_elpased: 2.157
batch start
#iterations: 365
currently lose_sum: 70.97747999429703
time_elpased: 2.152
batch start
#iterations: 366
currently lose_sum: 70.07953613996506
time_elpased: 2.225
batch start
#iterations: 367
currently lose_sum: 69.54606518149376
time_elpased: 2.1
batch start
#iterations: 368
currently lose_sum: 69.9320819079876
time_elpased: 2.547
batch start
#iterations: 369
currently lose_sum: 70.77185213565826
time_elpased: 2.22
batch start
#iterations: 370
currently lose_sum: 70.79193177819252
time_elpased: 2.197
batch start
#iterations: 371
currently lose_sum: 69.9286128282547
time_elpased: 2.17
batch start
#iterations: 372
currently lose_sum: 70.75552922487259
time_elpased: 2.119
batch start
#iterations: 373
currently lose_sum: 70.19554135203362
time_elpased: 2.237
batch start
#iterations: 374
currently lose_sum: 70.67724040150642
time_elpased: 2.154
batch start
#iterations: 375
currently lose_sum: 69.74117657542229
time_elpased: 2.121
batch start
#iterations: 376
currently lose_sum: 69.84108000993729
time_elpased: 2.358
batch start
#iterations: 377
currently lose_sum: 69.45774367451668
time_elpased: 2.303
batch start
#iterations: 378
currently lose_sum: 69.94992738962173
time_elpased: 2.119
batch start
#iterations: 379
currently lose_sum: 70.193046271801
time_elpased: 2.155
start validation test
0.616649484536
0.646861708983
0.516570605187
0.574420600858
0.616814835844
92.188
batch start
#iterations: 380
currently lose_sum: 69.79907810688019
time_elpased: 2.187
batch start
#iterations: 381
currently lose_sum: 69.55803370475769
time_elpased: 2.268
batch start
#iterations: 382
currently lose_sum: 70.89151659607887
time_elpased: 2.236
batch start
#iterations: 383
currently lose_sum: 69.33788681030273
time_elpased: 2.181
batch start
#iterations: 384
currently lose_sum: 69.16960126161575
time_elpased: 2.231
batch start
#iterations: 385
currently lose_sum: 69.57005143165588
time_elpased: 2.155
batch start
#iterations: 386
currently lose_sum: 70.08612832427025
time_elpased: 2.162
batch start
#iterations: 387
currently lose_sum: 69.18756154179573
time_elpased: 2.16
batch start
#iterations: 388
currently lose_sum: 69.42237776517868
time_elpased: 2.192
batch start
#iterations: 389
currently lose_sum: 69.85754641890526
time_elpased: 2.18
batch start
#iterations: 390
currently lose_sum: 69.73499301075935
time_elpased: 2.265
batch start
#iterations: 391
currently lose_sum: 69.08638831973076
time_elpased: 1.911
batch start
#iterations: 392
currently lose_sum: 69.14508327841759
time_elpased: 1.932
batch start
#iterations: 393
currently lose_sum: 69.08243125677109
time_elpased: 1.925
batch start
#iterations: 394
currently lose_sum: 68.95781043171883
time_elpased: 1.92
batch start
#iterations: 395
currently lose_sum: 69.87183704972267
time_elpased: 2.03
batch start
#iterations: 396
currently lose_sum: 69.48625230789185
time_elpased: 2.117
batch start
#iterations: 397
currently lose_sum: 68.67283853888512
time_elpased: 2.072
batch start
#iterations: 398
currently lose_sum: 69.15866214036942
time_elpased: 2.078
batch start
#iterations: 399
currently lose_sum: 68.7830670773983
time_elpased: 2.035
start validation test
0.620824742268
0.647647647648
0.53272951832
0.584594533544
0.620970294063
91.959
acc: 0.669
pre: 0.651
rec: 0.731
F1: 0.689
auc: 0.669
