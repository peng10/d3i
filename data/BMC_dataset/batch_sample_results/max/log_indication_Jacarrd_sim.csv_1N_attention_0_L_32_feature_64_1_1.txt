start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 99.92721527814865
time_elpased: 2.171
batch start
#iterations: 1
currently lose_sum: 98.81847375631332
time_elpased: 2.077
batch start
#iterations: 2
currently lose_sum: 98.42301070690155
time_elpased: 2.081
batch start
#iterations: 3
currently lose_sum: 97.85182899236679
time_elpased: 2.041
batch start
#iterations: 4
currently lose_sum: 97.58163839578629
time_elpased: 2.108
batch start
#iterations: 5
currently lose_sum: 97.11953592300415
time_elpased: 2.125
batch start
#iterations: 6
currently lose_sum: 96.26967471837997
time_elpased: 2.072
batch start
#iterations: 7
currently lose_sum: 96.43760699033737
time_elpased: 2.121
batch start
#iterations: 8
currently lose_sum: 95.9358338713646
time_elpased: 2.1
batch start
#iterations: 9
currently lose_sum: 95.62576025724411
time_elpased: 2.049
batch start
#iterations: 10
currently lose_sum: 95.46633160114288
time_elpased: 2.183
batch start
#iterations: 11
currently lose_sum: 95.34075218439102
time_elpased: 2.047
batch start
#iterations: 12
currently lose_sum: 94.82720601558685
time_elpased: 2.08
batch start
#iterations: 13
currently lose_sum: 94.30490392446518
time_elpased: 2.118
batch start
#iterations: 14
currently lose_sum: 93.9924709200859
time_elpased: 2.118
batch start
#iterations: 15
currently lose_sum: 93.88778275251389
time_elpased: 2.074
batch start
#iterations: 16
currently lose_sum: 93.71219658851624
time_elpased: 2.102
batch start
#iterations: 17
currently lose_sum: 93.55982983112335
time_elpased: 2.162
batch start
#iterations: 18
currently lose_sum: 92.88195580244064
time_elpased: 2.063
batch start
#iterations: 19
currently lose_sum: 92.95149874687195
time_elpased: 2.173
start validation test
0.651546391753
0.670984156355
0.597097869713
0.631888477456
0.651641984531
60.588
batch start
#iterations: 20
currently lose_sum: 92.71321195363998
time_elpased: 2.064
batch start
#iterations: 21
currently lose_sum: 92.39203453063965
time_elpased: 2.089
batch start
#iterations: 22
currently lose_sum: 92.3891686797142
time_elpased: 2.076
batch start
#iterations: 23
currently lose_sum: 92.13837319612503
time_elpased: 2.053
batch start
#iterations: 24
currently lose_sum: 91.27765864133835
time_elpased: 2.05
batch start
#iterations: 25
currently lose_sum: 91.30882126092911
time_elpased: 2.074
batch start
#iterations: 26
currently lose_sum: 91.06482338905334
time_elpased: 2.073
batch start
#iterations: 27
currently lose_sum: 90.76022082567215
time_elpased: 2.073
batch start
#iterations: 28
currently lose_sum: 90.35359567403793
time_elpased: 2.111
batch start
#iterations: 29
currently lose_sum: 90.88597470521927
time_elpased: 2.082
batch start
#iterations: 30
currently lose_sum: 89.97764617204666
time_elpased: 2.092
batch start
#iterations: 31
currently lose_sum: 89.6270180940628
time_elpased: 2.075
batch start
#iterations: 32
currently lose_sum: 89.78059375286102
time_elpased: 2.101
batch start
#iterations: 33
currently lose_sum: 89.89153057336807
time_elpased: 2.066
batch start
#iterations: 34
currently lose_sum: 89.62716734409332
time_elpased: 2.049
batch start
#iterations: 35
currently lose_sum: 89.57557970285416
time_elpased: 2.072
batch start
#iterations: 36
currently lose_sum: 88.69030117988586
time_elpased: 2.014
batch start
#iterations: 37
currently lose_sum: 88.8188009262085
time_elpased: 2.014
batch start
#iterations: 38
currently lose_sum: 88.29520225524902
time_elpased: 1.979
batch start
#iterations: 39
currently lose_sum: 87.8890580534935
time_elpased: 2.074
start validation test
0.644793814433
0.676184538653
0.558094061953
0.611490105429
0.644946029221
60.368
batch start
#iterations: 40
currently lose_sum: 88.43272662162781
time_elpased: 2.122
batch start
#iterations: 41
currently lose_sum: 87.5352812409401
time_elpased: 2.055
batch start
#iterations: 42
currently lose_sum: 87.46515566110611
time_elpased: 2.064
batch start
#iterations: 43
currently lose_sum: 87.31497371196747
time_elpased: 2.059
batch start
#iterations: 44
currently lose_sum: 87.67035984992981
time_elpased: 2.073
batch start
#iterations: 45
currently lose_sum: 87.01393860578537
time_elpased: 2.038
batch start
#iterations: 46
currently lose_sum: 86.84844404459
time_elpased: 2.13
batch start
#iterations: 47
currently lose_sum: 86.93185698986053
time_elpased: 1.995
batch start
#iterations: 48
currently lose_sum: 87.27328097820282
time_elpased: 2.033
batch start
#iterations: 49
currently lose_sum: 86.57295495271683
time_elpased: 2.019
batch start
#iterations: 50
currently lose_sum: 86.894755423069
time_elpased: 1.975
batch start
#iterations: 51
currently lose_sum: 85.8939705491066
time_elpased: 2.057
batch start
#iterations: 52
currently lose_sum: 86.12232655286789
time_elpased: 1.957
batch start
#iterations: 53
currently lose_sum: 86.53723645210266
time_elpased: 1.979
batch start
#iterations: 54
currently lose_sum: 85.28865694999695
time_elpased: 1.971
batch start
#iterations: 55
currently lose_sum: 85.01186901330948
time_elpased: 1.957
batch start
#iterations: 56
currently lose_sum: 85.42476391792297
time_elpased: 2.104
batch start
#iterations: 57
currently lose_sum: 85.02577131986618
time_elpased: 2.058
batch start
#iterations: 58
currently lose_sum: 85.09133809804916
time_elpased: 2.019
batch start
#iterations: 59
currently lose_sum: 85.0474870800972
time_elpased: 2.206
start validation test
0.632113402062
0.677441540578
0.506843676032
0.579855183376
0.632333332387
61.087
batch start
#iterations: 60
currently lose_sum: 84.2834746837616
time_elpased: 2.131
batch start
#iterations: 61
currently lose_sum: 84.84337800741196
time_elpased: 2.021
batch start
#iterations: 62
currently lose_sum: 84.5835970044136
time_elpased: 1.941
batch start
#iterations: 63
currently lose_sum: 84.49432444572449
time_elpased: 1.958
batch start
#iterations: 64
currently lose_sum: 83.9587168097496
time_elpased: 1.968
batch start
#iterations: 65
currently lose_sum: 83.4791334271431
time_elpased: 1.974
batch start
#iterations: 66
currently lose_sum: 84.15293264389038
time_elpased: 2.041
batch start
#iterations: 67
currently lose_sum: 83.31742691993713
time_elpased: 2.076
batch start
#iterations: 68
currently lose_sum: 84.09806454181671
time_elpased: 1.997
batch start
#iterations: 69
currently lose_sum: 83.56443339586258
time_elpased: 2.066
batch start
#iterations: 70
currently lose_sum: 83.2249094247818
time_elpased: 2.042
batch start
#iterations: 71
currently lose_sum: 83.56501668691635
time_elpased: 2.123
batch start
#iterations: 72
currently lose_sum: 83.03732490539551
time_elpased: 2.05
batch start
#iterations: 73
currently lose_sum: 82.7057945728302
time_elpased: 2.062
batch start
#iterations: 74
currently lose_sum: 82.68645507097244
time_elpased: 2.04
batch start
#iterations: 75
currently lose_sum: 82.45732343196869
time_elpased: 2.014
batch start
#iterations: 76
currently lose_sum: 82.56262481212616
time_elpased: 1.95
batch start
#iterations: 77
currently lose_sum: 82.40411865711212
time_elpased: 2.155
batch start
#iterations: 78
currently lose_sum: 82.3826271891594
time_elpased: 2.041
batch start
#iterations: 79
currently lose_sum: 81.94517394900322
time_elpased: 2.195
start validation test
0.627371134021
0.658430973001
0.532057219306
0.588536627013
0.627538472299
61.840
batch start
#iterations: 80
currently lose_sum: 81.95519107580185
time_elpased: 2.254
batch start
#iterations: 81
currently lose_sum: 82.21992063522339
time_elpased: 2.057
batch start
#iterations: 82
currently lose_sum: 82.4328281879425
time_elpased: 2.065
batch start
#iterations: 83
currently lose_sum: 81.58198922872543
time_elpased: 2.044
batch start
#iterations: 84
currently lose_sum: 81.72371253371239
time_elpased: 2.057
batch start
#iterations: 85
currently lose_sum: 81.99604132771492
time_elpased: 2.011
batch start
#iterations: 86
currently lose_sum: 81.70528364181519
time_elpased: 1.987
batch start
#iterations: 87
currently lose_sum: 81.29718413949013
time_elpased: 1.945
batch start
#iterations: 88
currently lose_sum: 80.91869983077049
time_elpased: 2.205
batch start
#iterations: 89
currently lose_sum: 81.03535103797913
time_elpased: 2.037
batch start
#iterations: 90
currently lose_sum: 81.3840149641037
time_elpased: 2.108
batch start
#iterations: 91
currently lose_sum: 80.47188973426819
time_elpased: 2.104
batch start
#iterations: 92
currently lose_sum: 80.77308389544487
time_elpased: 2.054
batch start
#iterations: 93
currently lose_sum: 80.329525411129
time_elpased: 2.084
batch start
#iterations: 94
currently lose_sum: 80.31906905770302
time_elpased: 2.038
batch start
#iterations: 95
currently lose_sum: 80.4502347111702
time_elpased: 1.953
batch start
#iterations: 96
currently lose_sum: 80.47566518187523
time_elpased: 1.938
batch start
#iterations: 97
currently lose_sum: 79.92755752801895
time_elpased: 2.007
batch start
#iterations: 98
currently lose_sum: 79.80982160568237
time_elpased: 2.099
batch start
#iterations: 99
currently lose_sum: 80.28779298067093
time_elpased: 2.18
start validation test
0.613865979381
0.671865348981
0.447771946074
0.53739270055
0.614157583075
63.769
batch start
#iterations: 100
currently lose_sum: 79.06614968180656
time_elpased: 2.119
batch start
#iterations: 101
currently lose_sum: 79.80517667531967
time_elpased: 2.06
batch start
#iterations: 102
currently lose_sum: 79.23132684826851
time_elpased: 2.048
batch start
#iterations: 103
currently lose_sum: 79.19202595949173
time_elpased: 2.007
batch start
#iterations: 104
currently lose_sum: 79.67731913924217
time_elpased: 2.132
batch start
#iterations: 105
currently lose_sum: 79.40758928656578
time_elpased: 2.073
batch start
#iterations: 106
currently lose_sum: 79.00604036450386
time_elpased: 2.098
batch start
#iterations: 107
currently lose_sum: 78.7855831682682
time_elpased: 2.086
batch start
#iterations: 108
currently lose_sum: 79.52730691432953
time_elpased: 2.1
batch start
#iterations: 109
currently lose_sum: 79.05705213546753
time_elpased: 2.093
batch start
#iterations: 110
currently lose_sum: 78.17527198791504
time_elpased: 2.05
batch start
#iterations: 111
currently lose_sum: 78.36369994282722
time_elpased: 2.074
batch start
#iterations: 112
currently lose_sum: 78.8731127679348
time_elpased: 2.044
batch start
#iterations: 113
currently lose_sum: 78.55268877744675
time_elpased: 2.108
batch start
#iterations: 114
currently lose_sum: 78.145224660635
time_elpased: 2.089
batch start
#iterations: 115
currently lose_sum: 77.66428419947624
time_elpased: 2.112
batch start
#iterations: 116
currently lose_sum: 78.12437736988068
time_elpased: 1.998
batch start
#iterations: 117
currently lose_sum: 77.84061861038208
time_elpased: 2.031
batch start
#iterations: 118
currently lose_sum: 77.56846761703491
time_elpased: 2.095
batch start
#iterations: 119
currently lose_sum: 77.86770525574684
time_elpased: 2.176
start validation test
0.619536082474
0.659214830971
0.497684470516
0.567172931449
0.619750011773
63.421
batch start
#iterations: 120
currently lose_sum: 78.02166989445686
time_elpased: 2.007
batch start
#iterations: 121
currently lose_sum: 77.33555191755295
time_elpased: 2.015
batch start
#iterations: 122
currently lose_sum: 77.31841838359833
time_elpased: 2.068
batch start
#iterations: 123
currently lose_sum: 77.39225545525551
time_elpased: 2.148
batch start
#iterations: 124
currently lose_sum: 77.25193935632706
time_elpased: 2.062
batch start
#iterations: 125
currently lose_sum: 77.58036214113235
time_elpased: 2.197
batch start
#iterations: 126
currently lose_sum: 76.8381561934948
time_elpased: 2.058
batch start
#iterations: 127
currently lose_sum: 77.25894409418106
time_elpased: 2.071
batch start
#iterations: 128
currently lose_sum: 76.66226518154144
time_elpased: 2.102
batch start
#iterations: 129
currently lose_sum: 76.08301442861557
time_elpased: 2.136
batch start
#iterations: 130
currently lose_sum: 76.72135552763939
time_elpased: 1.998
batch start
#iterations: 131
currently lose_sum: 76.19764092564583
time_elpased: 2.077
batch start
#iterations: 132
currently lose_sum: 76.84746551513672
time_elpased: 2.071
batch start
#iterations: 133
currently lose_sum: 75.84297007322311
time_elpased: 2.063
batch start
#iterations: 134
currently lose_sum: 76.14990064501762
time_elpased: 2.01
batch start
#iterations: 135
currently lose_sum: 76.13187000155449
time_elpased: 2.11
batch start
#iterations: 136
currently lose_sum: 76.4221453666687
time_elpased: 2.051
batch start
#iterations: 137
currently lose_sum: 76.21991068124771
time_elpased: 2.08
batch start
#iterations: 138
currently lose_sum: 75.56600821018219
time_elpased: 2.078
batch start
#iterations: 139
currently lose_sum: 75.7654404938221
time_elpased: 2.043
start validation test
0.626958762887
0.655310621242
0.538437789441
0.591153042201
0.627114175109
63.532
batch start
#iterations: 140
currently lose_sum: 76.18140131235123
time_elpased: 2.041
batch start
#iterations: 141
currently lose_sum: 75.57172870635986
time_elpased: 2.047
batch start
#iterations: 142
currently lose_sum: 76.44282805919647
time_elpased: 2.03
batch start
#iterations: 143
currently lose_sum: 75.48130643367767
time_elpased: 2.026
batch start
#iterations: 144
currently lose_sum: 76.20126977562904
time_elpased: 2.06
batch start
#iterations: 145
currently lose_sum: 75.7541201710701
time_elpased: 2.086
batch start
#iterations: 146
currently lose_sum: 74.6466019153595
time_elpased: 1.992
batch start
#iterations: 147
currently lose_sum: 75.63610309362411
time_elpased: 1.935
batch start
#iterations: 148
currently lose_sum: 75.04231169819832
time_elpased: 1.938
batch start
#iterations: 149
currently lose_sum: 75.37714302539825
time_elpased: 2.016
batch start
#iterations: 150
currently lose_sum: 75.19246366620064
time_elpased: 2.034
batch start
#iterations: 151
currently lose_sum: 75.15724295377731
time_elpased: 2.059
batch start
#iterations: 152
currently lose_sum: 75.63158276677132
time_elpased: 2.103
batch start
#iterations: 153
currently lose_sum: 74.94577115774155
time_elpased: 2.039
batch start
#iterations: 154
currently lose_sum: 75.1264716386795
time_elpased: 2.041
batch start
#iterations: 155
currently lose_sum: 75.05062609910965
time_elpased: 2.004
batch start
#iterations: 156
currently lose_sum: 75.12934449315071
time_elpased: 1.943
batch start
#iterations: 157
currently lose_sum: 74.57964825630188
time_elpased: 2.004
batch start
#iterations: 158
currently lose_sum: 75.27971982955933
time_elpased: 2.064
batch start
#iterations: 159
currently lose_sum: 74.28533923625946
time_elpased: 2.131
start validation test
0.60912371134
0.656359906213
0.46094473603
0.541563387945
0.609383862386
66.233
batch start
#iterations: 160
currently lose_sum: 74.59517708420753
time_elpased: 2.062
batch start
#iterations: 161
currently lose_sum: 73.76751464605331
time_elpased: 2.039
batch start
#iterations: 162
currently lose_sum: 74.36857822537422
time_elpased: 2.075
batch start
#iterations: 163
currently lose_sum: 73.83802261948586
time_elpased: 2.033
batch start
#iterations: 164
currently lose_sum: 74.50956133008003
time_elpased: 1.968
batch start
#iterations: 165
currently lose_sum: 73.8155859708786
time_elpased: 1.94
batch start
#iterations: 166
currently lose_sum: 74.24435025453568
time_elpased: 1.934
batch start
#iterations: 167
currently lose_sum: 73.55263340473175
time_elpased: 1.98
batch start
#iterations: 168
currently lose_sum: 73.71692278981209
time_elpased: 2.059
batch start
#iterations: 169
currently lose_sum: 73.32746362686157
time_elpased: 2.035
batch start
#iterations: 170
currently lose_sum: 73.39937943220139
time_elpased: 2.075
batch start
#iterations: 171
currently lose_sum: 73.4374215900898
time_elpased: 2.039
batch start
#iterations: 172
currently lose_sum: 73.29534673690796
time_elpased: 2.14
batch start
#iterations: 173
currently lose_sum: 73.3660651743412
time_elpased: 2.043
batch start
#iterations: 174
currently lose_sum: 73.65363216400146
time_elpased: 2.034
batch start
#iterations: 175
currently lose_sum: 73.86715695261955
time_elpased: 2.08
batch start
#iterations: 176
currently lose_sum: 72.88623467087746
time_elpased: 2.225
batch start
#iterations: 177
currently lose_sum: 72.55482840538025
time_elpased: 2.167
batch start
#iterations: 178
currently lose_sum: 73.16826540231705
time_elpased: 2.112
batch start
#iterations: 179
currently lose_sum: 73.29293030500412
time_elpased: 2.054
start validation test
0.592680412371
0.666117517847
0.374498301945
0.479446640316
0.593063464718
69.315
batch start
#iterations: 180
currently lose_sum: 73.59387668967247
time_elpased: 2.082
batch start
#iterations: 181
currently lose_sum: 72.16883319616318
time_elpased: 2.04
batch start
#iterations: 182
currently lose_sum: 72.2641966342926
time_elpased: 2.078
batch start
#iterations: 183
currently lose_sum: 72.15026634931564
time_elpased: 2.068
batch start
#iterations: 184
currently lose_sum: 73.06622326374054
time_elpased: 2.062
batch start
#iterations: 185
currently lose_sum: 72.63325920701027
time_elpased: 2.024
batch start
#iterations: 186
currently lose_sum: 73.02062356472015
time_elpased: 2.288
batch start
#iterations: 187
currently lose_sum: 72.83786019682884
time_elpased: 2.049
batch start
#iterations: 188
currently lose_sum: 72.61488071084023
time_elpased: 2.074
batch start
#iterations: 189
currently lose_sum: 72.314637362957
time_elpased: 2.076
batch start
#iterations: 190
currently lose_sum: 73.06437155604362
time_elpased: 2.281
batch start
#iterations: 191
currently lose_sum: 72.00650763511658
time_elpased: 2.063
batch start
#iterations: 192
currently lose_sum: 72.4284899532795
time_elpased: 2.084
batch start
#iterations: 193
currently lose_sum: 72.31441527605057
time_elpased: 2.218
batch start
#iterations: 194
currently lose_sum: 71.89291015267372
time_elpased: 2.163
batch start
#iterations: 195
currently lose_sum: 72.10277983546257
time_elpased: 2.121
batch start
#iterations: 196
currently lose_sum: 72.05328878760338
time_elpased: 2.096
batch start
#iterations: 197
currently lose_sum: 71.74245175719261
time_elpased: 2.098
batch start
#iterations: 198
currently lose_sum: 71.5908368229866
time_elpased: 2.108
batch start
#iterations: 199
currently lose_sum: 71.22711017727852
time_elpased: 2.174
start validation test
0.593041237113
0.66158212132
0.383863332304
0.485835232823
0.593408481189
69.656
batch start
#iterations: 200
currently lose_sum: 71.33758771419525
time_elpased: 2.042
batch start
#iterations: 201
currently lose_sum: 71.50656533241272
time_elpased: 2.128
batch start
#iterations: 202
currently lose_sum: 72.28567823767662
time_elpased: 2.05
batch start
#iterations: 203
currently lose_sum: 71.60963401198387
time_elpased: 2.058
batch start
#iterations: 204
currently lose_sum: 71.55905479192734
time_elpased: 2.077
batch start
#iterations: 205
currently lose_sum: 70.98602989315987
time_elpased: 1.967
batch start
#iterations: 206
currently lose_sum: 71.94812646508217
time_elpased: 2.007
batch start
#iterations: 207
currently lose_sum: 70.96076101064682
time_elpased: 2.096
batch start
#iterations: 208
currently lose_sum: 71.47160413861275
time_elpased: 2.014
batch start
#iterations: 209
currently lose_sum: 70.74724739789963
time_elpased: 2.131
batch start
#iterations: 210
currently lose_sum: 71.27182468771935
time_elpased: 2.085
batch start
#iterations: 211
currently lose_sum: 70.51883590221405
time_elpased: 2.067
batch start
#iterations: 212
currently lose_sum: 71.4226363003254
time_elpased: 2.104
batch start
#iterations: 213
currently lose_sum: 70.96968334913254
time_elpased: 2.065
batch start
#iterations: 214
currently lose_sum: 70.9545716047287
time_elpased: 2.042
batch start
#iterations: 215
currently lose_sum: 70.909942984581
time_elpased: 1.991
batch start
#iterations: 216
currently lose_sum: 70.58407473564148
time_elpased: 2.071
batch start
#iterations: 217
currently lose_sum: 70.84200569987297
time_elpased: 2.034
batch start
#iterations: 218
currently lose_sum: 70.70314449071884
time_elpased: 2.051
batch start
#iterations: 219
currently lose_sum: 70.7955432832241
time_elpased: 2.249
start validation test
0.600515463918
0.664272590613
0.409282700422
0.506495160469
0.600851202529
69.708
batch start
#iterations: 220
currently lose_sum: 70.04101121425629
time_elpased: 2.051
batch start
#iterations: 221
currently lose_sum: 70.41813042759895
time_elpased: 2.074
batch start
#iterations: 222
currently lose_sum: 70.42768585681915
time_elpased: 2.131
batch start
#iterations: 223
currently lose_sum: 69.88924351334572
time_elpased: 2.104
batch start
#iterations: 224
currently lose_sum: 70.30554321408272
time_elpased: 2.066
batch start
#iterations: 225
currently lose_sum: 70.90487000346184
time_elpased: 2.087
batch start
#iterations: 226
currently lose_sum: 69.92153763771057
time_elpased: 2.037
batch start
#iterations: 227
currently lose_sum: 69.992650359869
time_elpased: 2.038
batch start
#iterations: 228
currently lose_sum: 70.0415925681591
time_elpased: 2.091
batch start
#iterations: 229
currently lose_sum: 70.41202348470688
time_elpased: 2.057
batch start
#iterations: 230
currently lose_sum: 70.1199141740799
time_elpased: 2.085
batch start
#iterations: 231
currently lose_sum: 70.66458976268768
time_elpased: 2.046
batch start
#iterations: 232
currently lose_sum: 70.09774199128151
time_elpased: 2.02
batch start
#iterations: 233
currently lose_sum: 70.48747032880783
time_elpased: 2.045
batch start
#iterations: 234
currently lose_sum: 69.58749514818192
time_elpased: 2.078
batch start
#iterations: 235
currently lose_sum: 69.87684428691864
time_elpased: 2.097
batch start
#iterations: 236
currently lose_sum: 69.83939930796623
time_elpased: 2.104
batch start
#iterations: 237
currently lose_sum: 69.26044523715973
time_elpased: 2.103
batch start
#iterations: 238
currently lose_sum: 69.49286389350891
time_elpased: 2.129
batch start
#iterations: 239
currently lose_sum: 69.44706627726555
time_elpased: 2.053
start validation test
0.601340206186
0.64451246174
0.455078728002
0.533478103511
0.60159699077
69.312
batch start
#iterations: 240
currently lose_sum: 70.21868512034416
time_elpased: 2.101
batch start
#iterations: 241
currently lose_sum: 68.9286347925663
time_elpased: 2.064
batch start
#iterations: 242
currently lose_sum: 69.13446781039238
time_elpased: 2.07
batch start
#iterations: 243
currently lose_sum: 69.71613436937332
time_elpased: 2.099
batch start
#iterations: 244
currently lose_sum: 69.50297155976295
time_elpased: 2.055
batch start
#iterations: 245
currently lose_sum: 68.7206780910492
time_elpased: 2.067
batch start
#iterations: 246
currently lose_sum: 68.47537878155708
time_elpased: 2.031
batch start
#iterations: 247
currently lose_sum: 69.31890586018562
time_elpased: 2.073
batch start
#iterations: 248
currently lose_sum: 68.96851974725723
time_elpased: 2.04
batch start
#iterations: 249
currently lose_sum: 68.41597303748131
time_elpased: 2.188
batch start
#iterations: 250
currently lose_sum: 68.59883090853691
time_elpased: 2.092
batch start
#iterations: 251
currently lose_sum: 69.27391183376312
time_elpased: 2.1
batch start
#iterations: 252
currently lose_sum: 68.99231806397438
time_elpased: 2.103
batch start
#iterations: 253
currently lose_sum: 68.43539747595787
time_elpased: 2.049
batch start
#iterations: 254
currently lose_sum: 68.88499990105629
time_elpased: 2.106
batch start
#iterations: 255
currently lose_sum: 68.54664409160614
time_elpased: 2.248
batch start
#iterations: 256
currently lose_sum: 68.7785767018795
time_elpased: 2.057
batch start
#iterations: 257
currently lose_sum: 68.43738535046577
time_elpased: 2.054
batch start
#iterations: 258
currently lose_sum: 68.41548952460289
time_elpased: 2.082
batch start
#iterations: 259
currently lose_sum: 69.05464002490044
time_elpased: 2.053
start validation test
0.580773195876
0.655050900548
0.344344962437
0.451399662732
0.581188282107
73.913
batch start
#iterations: 260
currently lose_sum: 67.808304220438
time_elpased: 2.037
batch start
#iterations: 261
currently lose_sum: 69.05412650108337
time_elpased: 2.049
batch start
#iterations: 262
currently lose_sum: 68.19896054267883
time_elpased: 1.986
batch start
#iterations: 263
currently lose_sum: 67.54326730966568
time_elpased: 2.061
batch start
#iterations: 264
currently lose_sum: 68.56830403208733
time_elpased: 2.116
batch start
#iterations: 265
currently lose_sum: 68.6935124695301
time_elpased: 2.08
batch start
#iterations: 266
currently lose_sum: 68.69611558318138
time_elpased: 2.182
batch start
#iterations: 267
currently lose_sum: 68.5172216296196
time_elpased: 2.093
batch start
#iterations: 268
currently lose_sum: 68.7371823489666
time_elpased: 2.106
batch start
#iterations: 269
currently lose_sum: 68.48590224981308
time_elpased: 2.164
batch start
#iterations: 270
currently lose_sum: 68.70076876878738
time_elpased: 2.08
batch start
#iterations: 271
currently lose_sum: 68.2350625693798
time_elpased: 2.06
batch start
#iterations: 272
currently lose_sum: 68.21456837654114
time_elpased: 2.082
batch start
#iterations: 273
currently lose_sum: 67.89274397492409
time_elpased: 2.029
batch start
#iterations: 274
currently lose_sum: 68.86137759685516
time_elpased: 2.071
batch start
#iterations: 275
currently lose_sum: 67.9990722835064
time_elpased: 2.063
batch start
#iterations: 276
currently lose_sum: 67.4895681142807
time_elpased: 2.101
batch start
#iterations: 277
currently lose_sum: 68.01418375968933
time_elpased: 2.075
batch start
#iterations: 278
currently lose_sum: 67.81960606575012
time_elpased: 2.097
batch start
#iterations: 279
currently lose_sum: 67.16041800379753
time_elpased: 2.041
start validation test
0.588195876289
0.659174649963
0.368220644232
0.472499174645
0.588582076737
73.911
batch start
#iterations: 280
currently lose_sum: 67.50551319122314
time_elpased: 2.083
batch start
#iterations: 281
currently lose_sum: 67.45798510313034
time_elpased: 1.969
batch start
#iterations: 282
currently lose_sum: 67.812863022089
time_elpased: 2.154
batch start
#iterations: 283
currently lose_sum: 68.22569942474365
time_elpased: 2.052
batch start
#iterations: 284
currently lose_sum: 66.66890385746956
time_elpased: 2.114
batch start
#iterations: 285
currently lose_sum: 67.25197318196297
time_elpased: 1.994
batch start
#iterations: 286
currently lose_sum: 67.29104655981064
time_elpased: 1.981
batch start
#iterations: 287
currently lose_sum: 67.35533264279366
time_elpased: 2.271
batch start
#iterations: 288
currently lose_sum: 67.2220758497715
time_elpased: 2.138
batch start
#iterations: 289
currently lose_sum: 66.70532882213593
time_elpased: 2.05
batch start
#iterations: 290
currently lose_sum: 67.78914725780487
time_elpased: 2.064
batch start
#iterations: 291
currently lose_sum: 67.4723448753357
time_elpased: 2.042
batch start
#iterations: 292
currently lose_sum: 67.82684952020645
time_elpased: 2.111
batch start
#iterations: 293
currently lose_sum: 66.53542226552963
time_elpased: 2.012
batch start
#iterations: 294
currently lose_sum: 67.20174404978752
time_elpased: 1.971
batch start
#iterations: 295
currently lose_sum: 67.1661219894886
time_elpased: 1.914
batch start
#iterations: 296
currently lose_sum: 67.09021064639091
time_elpased: 2.072
batch start
#iterations: 297
currently lose_sum: 67.97074908018112
time_elpased: 2.009
batch start
#iterations: 298
currently lose_sum: 67.25434264540672
time_elpased: 2.065
batch start
#iterations: 299
currently lose_sum: 66.98285630345345
time_elpased: 1.935
start validation test
0.577164948454
0.663569576491
0.316044046516
0.428163123039
0.577623386472
76.603
batch start
#iterations: 300
currently lose_sum: 66.8667619228363
time_elpased: 2.056
batch start
#iterations: 301
currently lose_sum: 66.95867934823036
time_elpased: 2.053
batch start
#iterations: 302
currently lose_sum: 67.25527834892273
time_elpased: 1.998
batch start
#iterations: 303
currently lose_sum: 66.86077445745468
time_elpased: 2.013
batch start
#iterations: 304
currently lose_sum: 66.60173961520195
time_elpased: 2.049
batch start
#iterations: 305
currently lose_sum: 66.32246455550194
time_elpased: 2.085
batch start
#iterations: 306
currently lose_sum: 66.61177608370781
time_elpased: 2.07
batch start
#iterations: 307
currently lose_sum: 66.57176551222801
time_elpased: 2.012
batch start
#iterations: 308
currently lose_sum: 67.02017918229103
time_elpased: 2.012
batch start
#iterations: 309
currently lose_sum: 66.78100979328156
time_elpased: 2.072
batch start
#iterations: 310
currently lose_sum: 66.38952034711838
time_elpased: 2.387
batch start
#iterations: 311
currently lose_sum: 65.69503557682037
time_elpased: 1.945
batch start
#iterations: 312
currently lose_sum: 65.67723193764687
time_elpased: 1.974
batch start
#iterations: 313
currently lose_sum: 66.17129424214363
time_elpased: 2.045
batch start
#iterations: 314
currently lose_sum: 65.65354552865028
time_elpased: 2.037
batch start
#iterations: 315
currently lose_sum: 66.53111973404884
time_elpased: 1.964
batch start
#iterations: 316
currently lose_sum: 65.99078410863876
time_elpased: 1.986
batch start
#iterations: 317
currently lose_sum: 65.91954737901688
time_elpased: 2.038
batch start
#iterations: 318
currently lose_sum: 66.32126021385193
time_elpased: 2.068
batch start
#iterations: 319
currently lose_sum: 66.07717517018318
time_elpased: 2.058
start validation test
0.577216494845
0.65050665607
0.336935268087
0.44393220339
0.5776383456
76.477
batch start
#iterations: 320
currently lose_sum: 65.53932890295982
time_elpased: 2.096
batch start
#iterations: 321
currently lose_sum: 66.01268589496613
time_elpased: 2.058
batch start
#iterations: 322
currently lose_sum: 65.8183376789093
time_elpased: 2.068
batch start
#iterations: 323
currently lose_sum: 66.34164240956306
time_elpased: 1.996
batch start
#iterations: 324
currently lose_sum: 66.18190249800682
time_elpased: 2.047
batch start
#iterations: 325
currently lose_sum: 65.99902078509331
time_elpased: 1.973
batch start
#iterations: 326
currently lose_sum: 65.50675013661385
time_elpased: 2.006
batch start
#iterations: 327
currently lose_sum: 65.73024117946625
time_elpased: 1.924
batch start
#iterations: 328
currently lose_sum: 65.44609722495079
time_elpased: 2.038
batch start
#iterations: 329
currently lose_sum: 66.47417384386063
time_elpased: 2.07
batch start
#iterations: 330
currently lose_sum: 65.86943697929382
time_elpased: 2.081
batch start
#iterations: 331
currently lose_sum: 65.20984047651291
time_elpased: 2.045
batch start
#iterations: 332
currently lose_sum: 65.70373821258545
time_elpased: 2.05
batch start
#iterations: 333
currently lose_sum: 65.86762264370918
time_elpased: 2.038
batch start
#iterations: 334
currently lose_sum: 64.68881672620773
time_elpased: 2.015
batch start
#iterations: 335
currently lose_sum: 65.02401119470596
time_elpased: 2.134
batch start
#iterations: 336
currently lose_sum: 65.58084461092949
time_elpased: 2.042
batch start
#iterations: 337
currently lose_sum: 65.41231521964073
time_elpased: 2.092
batch start
#iterations: 338
currently lose_sum: 64.95824989676476
time_elpased: 2.105
batch start
#iterations: 339
currently lose_sum: 65.24049735069275
time_elpased: 2.062
start validation test
0.597216494845
0.650197316496
0.423896264279
0.513207077
0.597520785243
72.558
batch start
#iterations: 340
currently lose_sum: 65.55298829078674
time_elpased: 2.049
batch start
#iterations: 341
currently lose_sum: 65.49879562854767
time_elpased: 2.087
batch start
#iterations: 342
currently lose_sum: 65.59585669636726
time_elpased: 2.078
batch start
#iterations: 343
currently lose_sum: 64.5707640349865
time_elpased: 2.045
batch start
#iterations: 344
currently lose_sum: 64.36212304234505
time_elpased: 2.097
batch start
#iterations: 345
currently lose_sum: 65.54954701662064
time_elpased: 1.99
batch start
#iterations: 346
currently lose_sum: 65.43733751773834
time_elpased: 2.073
batch start
#iterations: 347
currently lose_sum: 64.82856637239456
time_elpased: 1.985
batch start
#iterations: 348
currently lose_sum: 65.85422790050507
time_elpased: 1.962
batch start
#iterations: 349
currently lose_sum: 65.01343974471092
time_elpased: 2.129
batch start
#iterations: 350
currently lose_sum: 64.42367744445801
time_elpased: 2.117
batch start
#iterations: 351
currently lose_sum: 65.5289535522461
time_elpased: 2.096
batch start
#iterations: 352
currently lose_sum: 64.64040440320969
time_elpased: 2.099
batch start
#iterations: 353
currently lose_sum: 65.35665389895439
time_elpased: 2.078
batch start
#iterations: 354
currently lose_sum: 65.1038790345192
time_elpased: 2.203
batch start
#iterations: 355
currently lose_sum: 64.45233860611916
time_elpased: 2.029
batch start
#iterations: 356
currently lose_sum: 65.1707494854927
time_elpased: 2.028
batch start
#iterations: 357
currently lose_sum: 64.31022584438324
time_elpased: 2.067
batch start
#iterations: 358
currently lose_sum: 65.0667961537838
time_elpased: 2.063
batch start
#iterations: 359
currently lose_sum: 64.06677404046059
time_elpased: 2.102
start validation test
0.583144329897
0.649651120088
0.364104147371
0.466662269999
0.583528888722
76.207
batch start
#iterations: 360
currently lose_sum: 64.46848538517952
time_elpased: 2.137
batch start
#iterations: 361
currently lose_sum: 64.45219859480858
time_elpased: 2.104
batch start
#iterations: 362
currently lose_sum: 64.04815569519997
time_elpased: 2.123
batch start
#iterations: 363
currently lose_sum: 65.23083728551865
time_elpased: 2.119
batch start
#iterations: 364
currently lose_sum: 64.13807138800621
time_elpased: 2.106
batch start
#iterations: 365
currently lose_sum: 64.3350527882576
time_elpased: 2.137
batch start
#iterations: 366
currently lose_sum: 64.70381334424019
time_elpased: 2.164
batch start
#iterations: 367
currently lose_sum: 63.696185022592545
time_elpased: 2.101
batch start
#iterations: 368
currently lose_sum: 64.54031473398209
time_elpased: 2.138
batch start
#iterations: 369
currently lose_sum: 64.30429914593697
time_elpased: 2.088
batch start
#iterations: 370
currently lose_sum: 64.15681713819504
time_elpased: 2.107
batch start
#iterations: 371
currently lose_sum: 63.67028960585594
time_elpased: 2.052
batch start
#iterations: 372
currently lose_sum: 64.09007519483566
time_elpased: 2.129
batch start
#iterations: 373
currently lose_sum: 63.53494170308113
time_elpased: 2.026
batch start
#iterations: 374
currently lose_sum: 64.20967754721642
time_elpased: 2.084
batch start
#iterations: 375
currently lose_sum: 63.93953827023506
time_elpased: 2.103
batch start
#iterations: 376
currently lose_sum: 64.33714726567268
time_elpased: 2.168
batch start
#iterations: 377
currently lose_sum: 64.35061553120613
time_elpased: 2.09
batch start
#iterations: 378
currently lose_sum: 63.9275324344635
time_elpased: 2.118
batch start
#iterations: 379
currently lose_sum: 64.25198450684547
time_elpased: 2.088
start validation test
0.579587628866
0.642505020997
0.362148811362
0.463209161511
0.579969376248
77.598
batch start
#iterations: 380
currently lose_sum: 63.92166152596474
time_elpased: 2.071
batch start
#iterations: 381
currently lose_sum: 64.83342266082764
time_elpased: 2.091
batch start
#iterations: 382
currently lose_sum: 63.46865078806877
time_elpased: 2.19
batch start
#iterations: 383
currently lose_sum: 63.68767622113228
time_elpased: 2.139
batch start
#iterations: 384
currently lose_sum: 63.71683892607689
time_elpased: 2.093
batch start
#iterations: 385
currently lose_sum: 63.6917824447155
time_elpased: 2.087
batch start
#iterations: 386
currently lose_sum: 63.74498048424721
time_elpased: 2.057
batch start
#iterations: 387
currently lose_sum: 63.68144416809082
time_elpased: 1.954
batch start
#iterations: 388
currently lose_sum: 64.30028095841408
time_elpased: 2.062
batch start
#iterations: 389
currently lose_sum: 63.0576066672802
time_elpased: 2.049
batch start
#iterations: 390
currently lose_sum: 63.58578532934189
time_elpased: 2.059
batch start
#iterations: 391
currently lose_sum: 63.78859579563141
time_elpased: 1.985
batch start
#iterations: 392
currently lose_sum: 63.25776392221451
time_elpased: 2.018
batch start
#iterations: 393
currently lose_sum: 63.00461095571518
time_elpased: 2.095
batch start
#iterations: 394
currently lose_sum: 63.66703024506569
time_elpased: 2.096
batch start
#iterations: 395
currently lose_sum: 64.09162086248398
time_elpased: 1.961
batch start
#iterations: 396
currently lose_sum: 63.242651998996735
time_elpased: 2.14
batch start
#iterations: 397
currently lose_sum: 63.61298060417175
time_elpased: 1.881
batch start
#iterations: 398
currently lose_sum: 63.45677870512009
time_elpased: 1.878
batch start
#iterations: 399
currently lose_sum: 63.307384103536606
time_elpased: 1.809
start validation test
0.594329896907
0.648831587429
0.414325409077
0.505715362392
0.594645922549
74.984
acc: 0.645
pre: 0.676
rec: 0.558
F1: 0.611
auc: 0.645
