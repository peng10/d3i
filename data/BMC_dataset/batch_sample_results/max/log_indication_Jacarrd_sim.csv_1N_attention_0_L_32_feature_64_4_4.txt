start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.15826481580734
time_elpased: 2.874
batch start
#iterations: 1
currently lose_sum: 99.03164082765579
time_elpased: 2.744
batch start
#iterations: 2
currently lose_sum: 98.68800193071365
time_elpased: 2.922
batch start
#iterations: 3
currently lose_sum: 97.52459156513214
time_elpased: 2.919
batch start
#iterations: 4
currently lose_sum: 97.01631939411163
time_elpased: 2.805
batch start
#iterations: 5
currently lose_sum: 95.67769384384155
time_elpased: 3.005
batch start
#iterations: 6
currently lose_sum: 94.90632861852646
time_elpased: 2.898
batch start
#iterations: 7
currently lose_sum: 94.54279655218124
time_elpased: 2.878
batch start
#iterations: 8
currently lose_sum: 93.73690366744995
time_elpased: 2.843
batch start
#iterations: 9
currently lose_sum: 92.68092304468155
time_elpased: 2.816
batch start
#iterations: 10
currently lose_sum: 92.61516785621643
time_elpased: 2.965
batch start
#iterations: 11
currently lose_sum: 92.36470550298691
time_elpased: 2.629
batch start
#iterations: 12
currently lose_sum: 91.77968043088913
time_elpased: 2.251
batch start
#iterations: 13
currently lose_sum: 91.21242392063141
time_elpased: 2.779
batch start
#iterations: 14
currently lose_sum: 90.77612060308456
time_elpased: 2.953
batch start
#iterations: 15
currently lose_sum: 90.84597766399384
time_elpased: 2.466
batch start
#iterations: 16
currently lose_sum: 90.20627391338348
time_elpased: 2.667
batch start
#iterations: 17
currently lose_sum: 89.6795312166214
time_elpased: 2.782
batch start
#iterations: 18
currently lose_sum: 89.79651844501495
time_elpased: 2.728
batch start
#iterations: 19
currently lose_sum: 88.70882594585419
time_elpased: 2.805
start validation test
0.659072164948
0.649744233182
0.692806421735
0.670584719594
0.659012939258
59.482
batch start
#iterations: 20
currently lose_sum: 89.54710471630096
time_elpased: 2.627
batch start
#iterations: 21
currently lose_sum: 88.80526632070541
time_elpased: 2.871
batch start
#iterations: 22
currently lose_sum: 88.40484416484833
time_elpased: 2.96
batch start
#iterations: 23
currently lose_sum: 88.50053542852402
time_elpased: 2.913
batch start
#iterations: 24
currently lose_sum: 87.96545732021332
time_elpased: 2.642
batch start
#iterations: 25
currently lose_sum: 87.73567712306976
time_elpased: 2.826
batch start
#iterations: 26
currently lose_sum: 87.49204868078232
time_elpased: 2.898
batch start
#iterations: 27
currently lose_sum: 86.2080448269844
time_elpased: 2.94
batch start
#iterations: 28
currently lose_sum: 86.90261155366898
time_elpased: 2.994
batch start
#iterations: 29
currently lose_sum: 85.70016932487488
time_elpased: 2.97
batch start
#iterations: 30
currently lose_sum: 86.06286209821701
time_elpased: 2.942
batch start
#iterations: 31
currently lose_sum: 85.43879824876785
time_elpased: 2.998
batch start
#iterations: 32
currently lose_sum: 85.67664271593094
time_elpased: 3.105
batch start
#iterations: 33
currently lose_sum: 85.7777487039566
time_elpased: 2.751
batch start
#iterations: 34
currently lose_sum: 84.28482580184937
time_elpased: 2.94
batch start
#iterations: 35
currently lose_sum: 84.52613347768784
time_elpased: 2.957
batch start
#iterations: 36
currently lose_sum: 84.50267162919044
time_elpased: 2.67
batch start
#iterations: 37
currently lose_sum: 83.9018583893776
time_elpased: 2.91
batch start
#iterations: 38
currently lose_sum: 83.86088880896568
time_elpased: 2.807
batch start
#iterations: 39
currently lose_sum: 83.86194306612015
time_elpased: 2.943
start validation test
0.659587628866
0.676174306735
0.614798806216
0.644027598103
0.659666262552
60.684
batch start
#iterations: 40
currently lose_sum: 83.12176743149757
time_elpased: 2.923
batch start
#iterations: 41
currently lose_sum: 83.25104984641075
time_elpased: 2.888
batch start
#iterations: 42
currently lose_sum: 83.34216976165771
time_elpased: 2.826
batch start
#iterations: 43
currently lose_sum: 82.84164589643478
time_elpased: 2.815
batch start
#iterations: 44
currently lose_sum: 82.53882026672363
time_elpased: 2.918
batch start
#iterations: 45
currently lose_sum: 82.5469942688942
time_elpased: 2.841
batch start
#iterations: 46
currently lose_sum: 82.80483993887901
time_elpased: 2.253
batch start
#iterations: 47
currently lose_sum: 81.71758103370667
time_elpased: 2.277
batch start
#iterations: 48
currently lose_sum: 81.9151904284954
time_elpased: 2.423
batch start
#iterations: 49
currently lose_sum: 82.04127621650696
time_elpased: 2.387
batch start
#iterations: 50
currently lose_sum: 81.31414595246315
time_elpased: 2.464
batch start
#iterations: 51
currently lose_sum: 81.13429975509644
time_elpased: 2.8
batch start
#iterations: 52
currently lose_sum: 80.72214728593826
time_elpased: 2.841
batch start
#iterations: 53
currently lose_sum: 80.77907106280327
time_elpased: 2.552
batch start
#iterations: 54
currently lose_sum: 80.78489995002747
time_elpased: 2.958
batch start
#iterations: 55
currently lose_sum: 80.3531165421009
time_elpased: 2.794
batch start
#iterations: 56
currently lose_sum: 79.93352967500687
time_elpased: 2.631
batch start
#iterations: 57
currently lose_sum: 80.47274142503738
time_elpased: 2.306
batch start
#iterations: 58
currently lose_sum: 79.76182985305786
time_elpased: 2.415
batch start
#iterations: 59
currently lose_sum: 80.46189305186272
time_elpased: 2.798
start validation test
0.635
0.645861000443
0.600596892045
0.622407081534
0.635060399962
63.475
batch start
#iterations: 60
currently lose_sum: 80.10957753658295
time_elpased: 2.859
batch start
#iterations: 61
currently lose_sum: 80.13300257921219
time_elpased: 2.883
batch start
#iterations: 62
currently lose_sum: 78.88437911868095
time_elpased: 2.876
batch start
#iterations: 63
currently lose_sum: 78.64278215169907
time_elpased: 2.903
batch start
#iterations: 64
currently lose_sum: 79.5739771425724
time_elpased: 2.917
batch start
#iterations: 65
currently lose_sum: 78.39633083343506
time_elpased: 2.55
batch start
#iterations: 66
currently lose_sum: 78.98375630378723
time_elpased: 2.65
batch start
#iterations: 67
currently lose_sum: 78.19149246811867
time_elpased: 2.38
batch start
#iterations: 68
currently lose_sum: 78.20350816845894
time_elpased: 2.863
batch start
#iterations: 69
currently lose_sum: 78.01419824361801
time_elpased: 2.875
batch start
#iterations: 70
currently lose_sum: 78.46997171640396
time_elpased: 2.976
batch start
#iterations: 71
currently lose_sum: 77.75127413868904
time_elpased: 2.938
batch start
#iterations: 72
currently lose_sum: 77.36675629019737
time_elpased: 2.86
batch start
#iterations: 73
currently lose_sum: 76.89616721868515
time_elpased: 3.065
batch start
#iterations: 74
currently lose_sum: 76.11331507563591
time_elpased: 2.812
batch start
#iterations: 75
currently lose_sum: 76.87853926420212
time_elpased: 2.829
batch start
#iterations: 76
currently lose_sum: 76.74226930737495
time_elpased: 2.482
batch start
#iterations: 77
currently lose_sum: 76.7576712667942
time_elpased: 2.964
batch start
#iterations: 78
currently lose_sum: 76.69468918442726
time_elpased: 2.856
batch start
#iterations: 79
currently lose_sum: 75.51779621839523
time_elpased: 2.705
start validation test
0.63618556701
0.659393358111
0.566018318411
0.609148299922
0.636308756438
65.704
batch start
#iterations: 80
currently lose_sum: 76.29259052872658
time_elpased: 2.536
batch start
#iterations: 81
currently lose_sum: 76.16083836555481
time_elpased: 2.23
batch start
#iterations: 82
currently lose_sum: 75.92688763141632
time_elpased: 2.158
batch start
#iterations: 83
currently lose_sum: 76.08421817421913
time_elpased: 2.253
batch start
#iterations: 84
currently lose_sum: 76.12302225828171
time_elpased: 2.829
batch start
#iterations: 85
currently lose_sum: 75.76361975073814
time_elpased: 3.007
batch start
#iterations: 86
currently lose_sum: 75.62711250782013
time_elpased: 2.617
batch start
#iterations: 87
currently lose_sum: 75.60008955001831
time_elpased: 2.706
batch start
#iterations: 88
currently lose_sum: 75.21931856870651
time_elpased: 2.804
batch start
#iterations: 89
currently lose_sum: 76.00534850358963
time_elpased: 2.723
batch start
#iterations: 90
currently lose_sum: 74.31156831979752
time_elpased: 2.246
batch start
#iterations: 91
currently lose_sum: 75.36927193403244
time_elpased: 2.573
batch start
#iterations: 92
currently lose_sum: 74.53690558671951
time_elpased: 2.84
batch start
#iterations: 93
currently lose_sum: 73.88677904009819
time_elpased: 2.893
batch start
#iterations: 94
currently lose_sum: 74.66769689321518
time_elpased: 2.867
batch start
#iterations: 95
currently lose_sum: 74.44616085290909
time_elpased: 2.814
batch start
#iterations: 96
currently lose_sum: 73.80988401174545
time_elpased: 2.514
batch start
#iterations: 97
currently lose_sum: 73.90031960606575
time_elpased: 2.626
batch start
#iterations: 98
currently lose_sum: 73.46652662754059
time_elpased: 2.825
batch start
#iterations: 99
currently lose_sum: 73.75970205664635
time_elpased: 2.698
start validation test
0.614175257732
0.669708029197
0.453226304415
0.540600257779
0.614457828444
73.628
batch start
#iterations: 100
currently lose_sum: 73.34026196599007
time_elpased: 2.975
batch start
#iterations: 101
currently lose_sum: 73.85848888754845
time_elpased: 2.698
batch start
#iterations: 102
currently lose_sum: 73.63486090302467
time_elpased: 2.885
batch start
#iterations: 103
currently lose_sum: 72.40445205569267
time_elpased: 2.822
batch start
#iterations: 104
currently lose_sum: 72.37956961989403
time_elpased: 2.875
batch start
#iterations: 105
currently lose_sum: 72.3628386259079
time_elpased: 2.565
batch start
#iterations: 106
currently lose_sum: 72.50657391548157
time_elpased: 2.955
batch start
#iterations: 107
currently lose_sum: 72.3920147716999
time_elpased: 2.668
batch start
#iterations: 108
currently lose_sum: 72.55543872714043
time_elpased: 2.813
batch start
#iterations: 109
currently lose_sum: 71.84554973244667
time_elpased: 2.751
batch start
#iterations: 110
currently lose_sum: 72.12619695067406
time_elpased: 2.959
batch start
#iterations: 111
currently lose_sum: 71.65820306539536
time_elpased: 2.968
batch start
#iterations: 112
currently lose_sum: 71.80209857225418
time_elpased: 3.023
batch start
#iterations: 113
currently lose_sum: 70.9827928841114
time_elpased: 3.075
batch start
#iterations: 114
currently lose_sum: 71.49070203304291
time_elpased: 3.004
batch start
#iterations: 115
currently lose_sum: 71.68220311403275
time_elpased: 2.85
batch start
#iterations: 116
currently lose_sum: 70.88004559278488
time_elpased: 2.653
batch start
#iterations: 117
currently lose_sum: 70.96394518017769
time_elpased: 2.758
batch start
#iterations: 118
currently lose_sum: 71.02972060441971
time_elpased: 2.971
batch start
#iterations: 119
currently lose_sum: 70.96727961301804
time_elpased: 2.998
start validation test
0.62381443299
0.66245802552
0.507564062982
0.574758186692
0.624018528445
73.734
batch start
#iterations: 120
currently lose_sum: 70.5390635728836
time_elpased: 2.92
batch start
#iterations: 121
currently lose_sum: 70.90076306462288
time_elpased: 2.62
batch start
#iterations: 122
currently lose_sum: 70.51735690236092
time_elpased: 2.509
batch start
#iterations: 123
currently lose_sum: 70.9671815931797
time_elpased: 2.985
batch start
#iterations: 124
currently lose_sum: 70.06262227892876
time_elpased: 2.856
batch start
#iterations: 125
currently lose_sum: 70.7375039756298
time_elpased: 2.826
batch start
#iterations: 126
currently lose_sum: 69.64992198348045
time_elpased: 2.873
batch start
#iterations: 127
currently lose_sum: 70.1960417330265
time_elpased: 2.922
batch start
#iterations: 128
currently lose_sum: 69.64144930243492
time_elpased: 2.828
batch start
#iterations: 129
currently lose_sum: 69.82089510560036
time_elpased: 2.924
batch start
#iterations: 130
currently lose_sum: 69.22510701417923
time_elpased: 2.841
batch start
#iterations: 131
currently lose_sum: 70.1608259677887
time_elpased: 2.848
batch start
#iterations: 132
currently lose_sum: 69.36019968986511
time_elpased: 2.681
batch start
#iterations: 133
currently lose_sum: 68.13765224814415
time_elpased: 2.748
batch start
#iterations: 134
currently lose_sum: 68.66617184877396
time_elpased: 2.692
batch start
#iterations: 135
currently lose_sum: 69.01284345984459
time_elpased: 2.889
batch start
#iterations: 136
currently lose_sum: 69.57632222771645
time_elpased: 2.808
batch start
#iterations: 137
currently lose_sum: 68.73532751202583
time_elpased: 2.828
batch start
#iterations: 138
currently lose_sum: 68.29626262187958
time_elpased: 2.838
batch start
#iterations: 139
currently lose_sum: 67.90794661641121
time_elpased: 2.474
start validation test
0.602525773196
0.654545454545
0.437171966656
0.524217930524
0.602816077307
82.856
batch start
#iterations: 140
currently lose_sum: 67.39409318566322
time_elpased: 2.882
batch start
#iterations: 141
currently lose_sum: 68.44636595249176
time_elpased: 2.805
batch start
#iterations: 142
currently lose_sum: 67.89709731936455
time_elpased: 2.716
batch start
#iterations: 143
currently lose_sum: 67.75494858622551
time_elpased: 2.82
batch start
#iterations: 144
currently lose_sum: 68.1264229118824
time_elpased: 2.791
batch start
#iterations: 145
currently lose_sum: 67.1440162062645
time_elpased: 2.789
batch start
#iterations: 146
currently lose_sum: 67.60560223460197
time_elpased: 2.484
batch start
#iterations: 147
currently lose_sum: 67.38586038351059
time_elpased: 2.144
batch start
#iterations: 148
currently lose_sum: 67.26992416381836
time_elpased: 2.425
batch start
#iterations: 149
currently lose_sum: 67.92582854628563
time_elpased: 2.799
batch start
#iterations: 150
currently lose_sum: 66.85172486305237
time_elpased: 2.855
batch start
#iterations: 151
currently lose_sum: 66.41608506441116
time_elpased: 2.968
batch start
#iterations: 152
currently lose_sum: 66.35227885842323
time_elpased: 3.048
batch start
#iterations: 153
currently lose_sum: 66.89782771468163
time_elpased: 2.962
batch start
#iterations: 154
currently lose_sum: 66.73843017220497
time_elpased: 2.506
batch start
#iterations: 155
currently lose_sum: 66.81786713004112
time_elpased: 2.212
batch start
#iterations: 156
currently lose_sum: 66.69481065869331
time_elpased: 1.933
batch start
#iterations: 157
currently lose_sum: 66.55622845888138
time_elpased: 2.228
batch start
#iterations: 158
currently lose_sum: 66.86310744285583
time_elpased: 3.001
batch start
#iterations: 159
currently lose_sum: 66.13685211539268
time_elpased: 2.648
start validation test
0.601597938144
0.63813229572
0.472573839662
0.543014249394
0.60182445985
81.383
batch start
#iterations: 160
currently lose_sum: 66.43357115983963
time_elpased: 2.452
batch start
#iterations: 161
currently lose_sum: 65.67791366577148
time_elpased: 2.439
batch start
#iterations: 162
currently lose_sum: 65.22484391927719
time_elpased: 2.415
batch start
#iterations: 163
currently lose_sum: 66.2581849694252
time_elpased: 2.868
batch start
#iterations: 164
currently lose_sum: 65.2407519519329
time_elpased: 2.676
batch start
#iterations: 165
currently lose_sum: 64.72015857696533
time_elpased: 2.471
batch start
#iterations: 166
currently lose_sum: 65.33121073246002
time_elpased: 2.688
batch start
#iterations: 167
currently lose_sum: 64.84235262870789
time_elpased: 2.79
batch start
#iterations: 168
currently lose_sum: 64.62413135170937
time_elpased: 2.542
batch start
#iterations: 169
currently lose_sum: 64.24573677778244
time_elpased: 2.686
batch start
#iterations: 170
currently lose_sum: 65.03943154215813
time_elpased: 2.417
batch start
#iterations: 171
currently lose_sum: 64.30518141388893
time_elpased: 2.222
batch start
#iterations: 172
currently lose_sum: 64.72325053811073
time_elpased: 2.613
batch start
#iterations: 173
currently lose_sum: 64.49092185497284
time_elpased: 2.835
batch start
#iterations: 174
currently lose_sum: 64.12484750151634
time_elpased: 2.943
batch start
#iterations: 175
currently lose_sum: 64.03397646546364
time_elpased: 2.787
batch start
#iterations: 176
currently lose_sum: 63.97840765118599
time_elpased: 2.519
batch start
#iterations: 177
currently lose_sum: 63.55219918489456
time_elpased: 2.371
batch start
#iterations: 178
currently lose_sum: 64.02518656849861
time_elpased: 2.824
batch start
#iterations: 179
currently lose_sum: 63.90831097960472
time_elpased: 2.601
start validation test
0.611082474227
0.656348977829
0.469177729752
0.547200384084
0.611331609893
87.777
batch start
#iterations: 180
currently lose_sum: 63.33196842670441
time_elpased: 2.843
batch start
#iterations: 181
currently lose_sum: 63.856265157461166
time_elpased: 2.67
batch start
#iterations: 182
currently lose_sum: 63.631777852773666
time_elpased: 2.86
batch start
#iterations: 183
currently lose_sum: 63.50317308306694
time_elpased: 2.863
batch start
#iterations: 184
currently lose_sum: 63.654892057180405
time_elpased: 2.906
batch start
#iterations: 185
currently lose_sum: 63.065448462963104
time_elpased: 2.888
batch start
#iterations: 186
currently lose_sum: 62.84566503763199
time_elpased: 2.842
batch start
#iterations: 187
currently lose_sum: 63.239854007959366
time_elpased: 2.547
batch start
#iterations: 188
currently lose_sum: 61.900752902030945
time_elpased: 2.779
batch start
#iterations: 189
currently lose_sum: 62.557154685258865
time_elpased: 2.835
batch start
#iterations: 190
currently lose_sum: 62.88744956254959
time_elpased: 2.796
batch start
#iterations: 191
currently lose_sum: 62.10176795721054
time_elpased: 2.825
batch start
#iterations: 192
currently lose_sum: 62.5945460498333
time_elpased: 2.708
batch start
#iterations: 193
currently lose_sum: 62.69806897640228
time_elpased: 3.044
batch start
#iterations: 194
currently lose_sum: 62.645181715488434
time_elpased: 3.034
batch start
#iterations: 195
currently lose_sum: 62.82953932881355
time_elpased: 3.054
batch start
#iterations: 196
currently lose_sum: 62.26042151451111
time_elpased: 3.059
batch start
#iterations: 197
currently lose_sum: 61.382442742586136
time_elpased: 2.393
batch start
#iterations: 198
currently lose_sum: 61.87063664197922
time_elpased: 2.914
batch start
#iterations: 199
currently lose_sum: 61.73338305950165
time_elpased: 2.824
start validation test
0.604536082474
0.656608975341
0.441185551096
0.527760679552
0.604822869527
94.105
batch start
#iterations: 200
currently lose_sum: 61.65370059013367
time_elpased: 2.896
batch start
#iterations: 201
currently lose_sum: 62.15139999985695
time_elpased: 2.923
batch start
#iterations: 202
currently lose_sum: 62.05880227684975
time_elpased: 2.599
batch start
#iterations: 203
currently lose_sum: 61.62911459803581
time_elpased: 2.716
batch start
#iterations: 204
currently lose_sum: 61.86873313784599
time_elpased: 2.456
batch start
#iterations: 205
currently lose_sum: 60.37875339388847
time_elpased: 2.234
batch start
#iterations: 206
currently lose_sum: 60.805208921432495
time_elpased: 2.151
batch start
#iterations: 207
currently lose_sum: 59.90961617231369
time_elpased: 2.531
batch start
#iterations: 208
currently lose_sum: 61.11790978908539
time_elpased: 2.909
batch start
#iterations: 209
currently lose_sum: 60.71944406628609
time_elpased: 2.861
batch start
#iterations: 210
currently lose_sum: 59.164766013622284
time_elpased: 2.775
batch start
#iterations: 211
currently lose_sum: 59.24883961677551
time_elpased: 2.89
batch start
#iterations: 212
currently lose_sum: 59.70091211795807
time_elpased: 2.938
batch start
#iterations: 213
currently lose_sum: 58.9930724799633
time_elpased: 2.628
batch start
#iterations: 214
currently lose_sum: 59.96674755215645
time_elpased: 2.396
batch start
#iterations: 215
currently lose_sum: 59.40087813138962
time_elpased: 2.412
batch start
#iterations: 216
currently lose_sum: 60.26643514633179
time_elpased: 2.444
batch start
#iterations: 217
currently lose_sum: 60.030266255140305
time_elpased: 2.801
batch start
#iterations: 218
currently lose_sum: 59.2249710559845
time_elpased: 2.87
batch start
#iterations: 219
currently lose_sum: 59.60847517848015
time_elpased: 2.809
start validation test
0.594690721649
0.638152011923
0.440670988988
0.521336823522
0.594961127046
94.846
batch start
#iterations: 220
currently lose_sum: 58.65205404162407
time_elpased: 2.528
batch start
#iterations: 221
currently lose_sum: 59.65052551031113
time_elpased: 2.893
batch start
#iterations: 222
currently lose_sum: 58.53234592080116
time_elpased: 2.811
batch start
#iterations: 223
currently lose_sum: 59.15121531486511
time_elpased: 2.388
batch start
#iterations: 224
currently lose_sum: 59.500244945287704
time_elpased: 2.602
batch start
#iterations: 225
currently lose_sum: 59.45096191763878
time_elpased: 2.833
batch start
#iterations: 226
currently lose_sum: 59.24204584956169
time_elpased: 2.81
batch start
#iterations: 227
currently lose_sum: 58.39138749241829
time_elpased: 2.826
batch start
#iterations: 228
currently lose_sum: 57.475796073675156
time_elpased: 2.85
batch start
#iterations: 229
currently lose_sum: 58.507542461156845
time_elpased: 2.573
batch start
#iterations: 230
currently lose_sum: 58.2886968255043
time_elpased: 2.964
batch start
#iterations: 231
currently lose_sum: 57.72007876634598
time_elpased: 2.68
batch start
#iterations: 232
currently lose_sum: 58.25785577297211
time_elpased: 2.689
batch start
#iterations: 233
currently lose_sum: 57.15354433655739
time_elpased: 3.047
batch start
#iterations: 234
currently lose_sum: 58.31788641214371
time_elpased: 3.043
batch start
#iterations: 235
currently lose_sum: 58.05963435769081
time_elpased: 3.005
batch start
#iterations: 236
currently lose_sum: 58.36278939247131
time_elpased: 2.958
batch start
#iterations: 237
currently lose_sum: 57.17204022407532
time_elpased: 2.919
batch start
#iterations: 238
currently lose_sum: 57.78000846505165
time_elpased: 2.896
batch start
#iterations: 239
currently lose_sum: 57.81599047780037
time_elpased: 2.941
start validation test
0.598917525773
0.663458291118
0.404342904189
0.502461794232
0.599259131532
108.258
batch start
#iterations: 240
currently lose_sum: 57.691803216934204
time_elpased: 2.865
batch start
#iterations: 241
currently lose_sum: 58.03199276328087
time_elpased: 2.745
batch start
#iterations: 242
currently lose_sum: 56.39547899365425
time_elpased: 2.829
batch start
#iterations: 243
currently lose_sum: 56.01314613223076
time_elpased: 2.867
batch start
#iterations: 244
currently lose_sum: 56.276965975761414
time_elpased: 2.819
batch start
#iterations: 245
currently lose_sum: 56.89624181389809
time_elpased: 2.771
batch start
#iterations: 246
currently lose_sum: 56.83107507228851
time_elpased: 2.743
batch start
#iterations: 247
currently lose_sum: 55.43244984745979
time_elpased: 2.84
batch start
#iterations: 248
currently lose_sum: 57.20320960879326
time_elpased: 2.837
batch start
#iterations: 249
currently lose_sum: 56.739490032196045
time_elpased: 2.848
batch start
#iterations: 250
currently lose_sum: 55.49439936876297
time_elpased: 2.726
batch start
#iterations: 251
currently lose_sum: 56.04106041789055
time_elpased: 2.369
batch start
#iterations: 252
currently lose_sum: 56.043910056352615
time_elpased: 2.872
batch start
#iterations: 253
currently lose_sum: 55.46594098210335
time_elpased: 2.902
batch start
#iterations: 254
currently lose_sum: 55.1322263777256
time_elpased: 2.493
batch start
#iterations: 255
currently lose_sum: 56.57767280936241
time_elpased: 2.508
batch start
#iterations: 256
currently lose_sum: 54.96785390377045
time_elpased: 2.901
batch start
#iterations: 257
currently lose_sum: 56.28120297193527
time_elpased: 2.637
batch start
#iterations: 258
currently lose_sum: 55.52292349934578
time_elpased: 2.234
batch start
#iterations: 259
currently lose_sum: 55.34507483243942
time_elpased: 2.779
start validation test
0.587525773196
0.673970379387
0.34187506432
0.453639218899
0.587957050904
120.290
batch start
#iterations: 260
currently lose_sum: 55.746936321258545
time_elpased: 3.007
batch start
#iterations: 261
currently lose_sum: 54.078063637018204
time_elpased: 2.925
batch start
#iterations: 262
currently lose_sum: 55.22589844465256
time_elpased: 2.697
batch start
#iterations: 263
currently lose_sum: 54.530624747276306
time_elpased: 2.957
batch start
#iterations: 264
currently lose_sum: 54.679375141859055
time_elpased: 2.769
batch start
#iterations: 265
currently lose_sum: 54.4651685655117
time_elpased: 2.415
batch start
#iterations: 266
currently lose_sum: 54.90402638912201
time_elpased: 2.338
batch start
#iterations: 267
currently lose_sum: 53.85592383146286
time_elpased: 2.859
batch start
#iterations: 268
currently lose_sum: 54.725852221250534
time_elpased: 2.65
batch start
#iterations: 269
currently lose_sum: 53.974687814712524
time_elpased: 2.746
batch start
#iterations: 270
currently lose_sum: 54.21473237872124
time_elpased: 2.788
batch start
#iterations: 271
currently lose_sum: 53.10089656710625
time_elpased: 2.965
batch start
#iterations: 272
currently lose_sum: 54.26889741420746
time_elpased: 2.774
batch start
#iterations: 273
currently lose_sum: 53.85589089989662
time_elpased: 3.107
batch start
#iterations: 274
currently lose_sum: 53.91762772202492
time_elpased: 2.51
batch start
#iterations: 275
currently lose_sum: 53.59511238336563
time_elpased: 2.654
batch start
#iterations: 276
currently lose_sum: 53.56969174742699
time_elpased: 2.609
batch start
#iterations: 277
currently lose_sum: 52.367143630981445
time_elpased: 2.962
batch start
#iterations: 278
currently lose_sum: 53.016606122255325
time_elpased: 2.699
batch start
#iterations: 279
currently lose_sum: 52.047921776771545
time_elpased: 2.384
start validation test
0.595463917526
0.660815694373
0.395183698672
0.494590417311
0.595815540341
112.756
batch start
#iterations: 280
currently lose_sum: 53.34451273083687
time_elpased: 2.421
batch start
#iterations: 281
currently lose_sum: 53.013064950704575
time_elpased: 2.835
batch start
#iterations: 282
currently lose_sum: 53.31431424617767
time_elpased: 3.065
batch start
#iterations: 283
currently lose_sum: 52.06195288896561
time_elpased: 2.679
batch start
#iterations: 284
currently lose_sum: 52.3275563120842
time_elpased: 2.674
batch start
#iterations: 285
currently lose_sum: 52.503896564245224
time_elpased: 2.833
batch start
#iterations: 286
currently lose_sum: 51.88911288976669
time_elpased: 2.921
batch start
#iterations: 287
currently lose_sum: 51.995012164115906
time_elpased: 2.89
batch start
#iterations: 288
currently lose_sum: 52.19800691306591
time_elpased: 2.819
batch start
#iterations: 289
currently lose_sum: 51.06869274377823
time_elpased: 2.861
batch start
#iterations: 290
currently lose_sum: 50.69443228840828
time_elpased: 2.892
batch start
#iterations: 291
currently lose_sum: 51.146343141794205
time_elpased: 2.34
batch start
#iterations: 292
currently lose_sum: 50.992307305336
time_elpased: 2.305
batch start
#iterations: 293
currently lose_sum: 51.81169147789478
time_elpased: 2.854
batch start
#iterations: 294
currently lose_sum: 51.52211916446686
time_elpased: 2.885
batch start
#iterations: 295
currently lose_sum: 51.33447512984276
time_elpased: 2.929
batch start
#iterations: 296
currently lose_sum: 50.98097878694534
time_elpased: 2.451
batch start
#iterations: 297
currently lose_sum: 50.93033301830292
time_elpased: 2.302
batch start
#iterations: 298
currently lose_sum: 50.74652820825577
time_elpased: 2.265
batch start
#iterations: 299
currently lose_sum: 50.112285524606705
time_elpased: 2.359
start validation test
0.581597938144
0.657418339237
0.343830400329
0.451516994392
0.58201537573
127.478
batch start
#iterations: 300
currently lose_sum: 50.31353470683098
time_elpased: 2.829
batch start
#iterations: 301
currently lose_sum: 50.915786385536194
time_elpased: 2.629
batch start
#iterations: 302
currently lose_sum: 51.20421805977821
time_elpased: 2.787
batch start
#iterations: 303
currently lose_sum: 51.1002881526947
time_elpased: 3.033
batch start
#iterations: 304
currently lose_sum: 50.70540505647659
time_elpased: 2.291
batch start
#iterations: 305
currently lose_sum: 49.81572198867798
time_elpased: 2.761
batch start
#iterations: 306
currently lose_sum: 49.74689319729805
time_elpased: 2.648
batch start
#iterations: 307
currently lose_sum: 50.338017880916595
time_elpased: 2.692
batch start
#iterations: 308
currently lose_sum: 50.17391036450863
time_elpased: 2.558
batch start
#iterations: 309
currently lose_sum: 50.22737056016922
time_elpased: 2.647
batch start
#iterations: 310
currently lose_sum: 48.65069952607155
time_elpased: 2.471
batch start
#iterations: 311
currently lose_sum: 48.939807653427124
time_elpased: 2.11
batch start
#iterations: 312
currently lose_sum: 49.7940676510334
time_elpased: 1.957
batch start
#iterations: 313
currently lose_sum: 49.11741195619106
time_elpased: 2.77
batch start
#iterations: 314
currently lose_sum: 49.336923971772194
time_elpased: 2.927
batch start
#iterations: 315
currently lose_sum: 47.54578234255314
time_elpased: 3.104
batch start
#iterations: 316
currently lose_sum: 48.1556416451931
time_elpased: 2.521
batch start
#iterations: 317
currently lose_sum: 48.94513243436813
time_elpased: 2.457
batch start
#iterations: 318
currently lose_sum: 48.29057776927948
time_elpased: 2.893
batch start
#iterations: 319
currently lose_sum: 46.864093869924545
time_elpased: 2.521
start validation test
0.579948453608
0.66456759026
0.325820726562
0.437262619985
0.58039461403
140.549
batch start
#iterations: 320
currently lose_sum: 48.528778314590454
time_elpased: 2.783
batch start
#iterations: 321
currently lose_sum: 48.99413040280342
time_elpased: 2.807
batch start
#iterations: 322
currently lose_sum: 48.56391268968582
time_elpased: 2.466
batch start
#iterations: 323
currently lose_sum: 48.29173722863197
time_elpased: 2.281
batch start
#iterations: 324
currently lose_sum: 46.89509531855583
time_elpased: 2.311
batch start
#iterations: 325
currently lose_sum: 47.19447557628155
time_elpased: 2.794
batch start
#iterations: 326
currently lose_sum: 47.64651370048523
time_elpased: 2.941
batch start
#iterations: 327
currently lose_sum: 48.13551890850067
time_elpased: 2.546
batch start
#iterations: 328
currently lose_sum: 47.297088488936424
time_elpased: 2.535
batch start
#iterations: 329
currently lose_sum: 46.511896416544914
time_elpased: 2.89
batch start
#iterations: 330
currently lose_sum: 48.52412736415863
time_elpased: 2.962
batch start
#iterations: 331
currently lose_sum: 48.70296412706375
time_elpased: 2.858
batch start
#iterations: 332
currently lose_sum: 46.83997401595116
time_elpased: 2.749
batch start
#iterations: 333
currently lose_sum: 46.73907496035099
time_elpased: 2.89
batch start
#iterations: 334
currently lose_sum: 47.21748360991478
time_elpased: 2.959
batch start
#iterations: 335
currently lose_sum: 46.77584332227707
time_elpased: 2.553
batch start
#iterations: 336
currently lose_sum: 47.500373378396034
time_elpased: 2.814
batch start
#iterations: 337
currently lose_sum: 46.55476099252701
time_elpased: 2.702
batch start
#iterations: 338
currently lose_sum: 46.25521840155125
time_elpased: 2.486
batch start
#iterations: 339
currently lose_sum: 47.49929477274418
time_elpased: 2.956
start validation test
0.569845360825
0.658283341024
0.293609138623
0.406092093089
0.570330336119
145.145
batch start
#iterations: 340
currently lose_sum: 47.22946645319462
time_elpased: 2.356
batch start
#iterations: 341
currently lose_sum: 46.09908290207386
time_elpased: 2.274
batch start
#iterations: 342
currently lose_sum: 46.02888111770153
time_elpased: 2.317
batch start
#iterations: 343
currently lose_sum: 47.17705699801445
time_elpased: 2.685
batch start
#iterations: 344
currently lose_sum: 46.35072149336338
time_elpased: 2.025
batch start
#iterations: 345
currently lose_sum: 45.913921013474464
time_elpased: 2.336
batch start
#iterations: 346
currently lose_sum: 44.40685507655144
time_elpased: 2.904
batch start
#iterations: 347
currently lose_sum: 46.312762916088104
time_elpased: 2.893
batch start
#iterations: 348
currently lose_sum: 44.85285276174545
time_elpased: 2.792
batch start
#iterations: 349
currently lose_sum: 44.538325145840645
time_elpased: 2.714
batch start
#iterations: 350
currently lose_sum: 44.087060645222664
time_elpased: 2.772
batch start
#iterations: 351
currently lose_sum: 45.05935342609882
time_elpased: 2.866
batch start
#iterations: 352
currently lose_sum: 45.07778809964657
time_elpased: 2.614
batch start
#iterations: 353
currently lose_sum: 45.24054953455925
time_elpased: 2.854
batch start
#iterations: 354
currently lose_sum: 45.26372365653515
time_elpased: 2.855
batch start
#iterations: 355
currently lose_sum: 44.407473623752594
time_elpased: 2.949
batch start
#iterations: 356
currently lose_sum: 44.12070359289646
time_elpased: 2.993
batch start
#iterations: 357
currently lose_sum: 45.128545224666595
time_elpased: 2.954
batch start
#iterations: 358
currently lose_sum: 44.304072350263596
time_elpased: 2.93
batch start
#iterations: 359
currently lose_sum: 43.91467735171318
time_elpased: 2.6
start validation test
0.563917525773
0.660290742158
0.266440259339
0.379674439067
0.564439792997
153.300
batch start
#iterations: 360
currently lose_sum: 44.482573464512825
time_elpased: 2.601
batch start
#iterations: 361
currently lose_sum: 44.497940480709076
time_elpased: 2.781
batch start
#iterations: 362
currently lose_sum: 43.69745124876499
time_elpased: 3.059
batch start
#iterations: 363
currently lose_sum: 44.190959736704826
time_elpased: 2.959
batch start
#iterations: 364
currently lose_sum: 43.21640595793724
time_elpased: 2.888
batch start
#iterations: 365
currently lose_sum: 43.14675784111023
time_elpased: 2.943
batch start
#iterations: 366
currently lose_sum: 44.256972312927246
time_elpased: 3.07
batch start
#iterations: 367
currently lose_sum: 43.35921962559223
time_elpased: 2.625
batch start
#iterations: 368
currently lose_sum: 43.405361115932465
time_elpased: 2.481
batch start
#iterations: 369
currently lose_sum: 43.832661002874374
time_elpased: 2.898
batch start
#iterations: 370
currently lose_sum: 43.475794926285744
time_elpased: 3.016
batch start
#iterations: 371
currently lose_sum: 42.89693684875965
time_elpased: 2.908
batch start
#iterations: 372
currently lose_sum: 44.37664982676506
time_elpased: 2.849
batch start
#iterations: 373
currently lose_sum: 43.1041599214077
time_elpased: 2.907
batch start
#iterations: 374
currently lose_sum: 43.254306346178055
time_elpased: 2.508
batch start
#iterations: 375
currently lose_sum: 43.061305686831474
time_elpased: 2.566
batch start
#iterations: 376
currently lose_sum: 43.58175440132618
time_elpased: 2.818
batch start
#iterations: 377
currently lose_sum: 41.949489414691925
time_elpased: 2.309
batch start
#iterations: 378
currently lose_sum: 42.502625584602356
time_elpased: 2.466
batch start
#iterations: 379
currently lose_sum: 41.40352472662926
time_elpased: 2.384
start validation test
0.575721649485
0.667041366906
0.305341154677
0.418919872926
0.576196344146
156.258
batch start
#iterations: 380
currently lose_sum: 42.18354323506355
time_elpased: 2.831
batch start
#iterations: 381
currently lose_sum: 42.13773615658283
time_elpased: 2.81
batch start
#iterations: 382
currently lose_sum: 41.58497716486454
time_elpased: 2.812
batch start
#iterations: 383
currently lose_sum: 41.94391790032387
time_elpased: 2.789
batch start
#iterations: 384
currently lose_sum: 41.69500896334648
time_elpased: 2.762
batch start
#iterations: 385
currently lose_sum: 41.14072507619858
time_elpased: 2.84
batch start
#iterations: 386
currently lose_sum: 41.87381714582443
time_elpased: 2.887
batch start
#iterations: 387
currently lose_sum: 40.458717077970505
time_elpased: 2.908
batch start
#iterations: 388
currently lose_sum: 41.51397043466568
time_elpased: 2.921
batch start
#iterations: 389
currently lose_sum: 40.93046487867832
time_elpased: 2.865
batch start
#iterations: 390
currently lose_sum: 40.6777436286211
time_elpased: 2.893
batch start
#iterations: 391
currently lose_sum: 41.142670303583145
time_elpased: 2.551
batch start
#iterations: 392
currently lose_sum: 41.0342773348093
time_elpased: 2.707
batch start
#iterations: 393
currently lose_sum: 40.69378885626793
time_elpased: 2.844
batch start
#iterations: 394
currently lose_sum: 41.22537639737129
time_elpased: 2.716
batch start
#iterations: 395
currently lose_sum: 41.59644812345505
time_elpased: 2.967
batch start
#iterations: 396
currently lose_sum: 41.3636092543602
time_elpased: 2.992
batch start
#iterations: 397
currently lose_sum: 40.34967164695263
time_elpased: 2.989
batch start
#iterations: 398
currently lose_sum: 39.33899728953838
time_elpased: 3.045
batch start
#iterations: 399
currently lose_sum: 40.646520644426346
time_elpased: 2.911
start validation test
0.554845360825
0.647556647557
0.244108263867
0.354559043348
0.555390907726
167.188
acc: 0.652
pre: 0.643
rec: 0.688
F1: 0.665
auc: 0.652
