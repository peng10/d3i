start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.03189951181412
time_elpased: 2.749
batch start
#iterations: 1
currently lose_sum: 99.08295178413391
time_elpased: 2.683
batch start
#iterations: 2
currently lose_sum: 97.97358220815659
time_elpased: 2.706
batch start
#iterations: 3
currently lose_sum: 97.2924856543541
time_elpased: 2.686
batch start
#iterations: 4
currently lose_sum: 96.4598725438118
time_elpased: 2.707
batch start
#iterations: 5
currently lose_sum: 95.20420742034912
time_elpased: 2.59
batch start
#iterations: 6
currently lose_sum: 94.42997854948044
time_elpased: 2.6
batch start
#iterations: 7
currently lose_sum: 94.02436602115631
time_elpased: 2.599
batch start
#iterations: 8
currently lose_sum: 93.51289570331573
time_elpased: 2.538
batch start
#iterations: 9
currently lose_sum: 93.19724559783936
time_elpased: 2.58
batch start
#iterations: 10
currently lose_sum: 92.78352987766266
time_elpased: 2.653
batch start
#iterations: 11
currently lose_sum: 92.3526526093483
time_elpased: 2.677
batch start
#iterations: 12
currently lose_sum: 92.39203351736069
time_elpased: 2.659
batch start
#iterations: 13
currently lose_sum: 91.78483635187149
time_elpased: 2.679
batch start
#iterations: 14
currently lose_sum: 91.47812962532043
time_elpased: 2.655
batch start
#iterations: 15
currently lose_sum: 91.24075025320053
time_elpased: 2.656
batch start
#iterations: 16
currently lose_sum: 91.04938191175461
time_elpased: 2.503
batch start
#iterations: 17
currently lose_sum: 90.47276937961578
time_elpased: 2.661
batch start
#iterations: 18
currently lose_sum: 89.62786048650742
time_elpased: 2.614
batch start
#iterations: 19
currently lose_sum: 90.19937628507614
time_elpased: 2.752
start validation test
0.643402061856
0.632858499525
0.685878962536
0.658302874642
0.643331881103
60.361
batch start
#iterations: 20
currently lose_sum: 89.47814226150513
time_elpased: 2.588
batch start
#iterations: 21
currently lose_sum: 89.55884033441544
time_elpased: 2.551
batch start
#iterations: 22
currently lose_sum: 88.97481513023376
time_elpased: 2.557
batch start
#iterations: 23
currently lose_sum: 88.5189134478569
time_elpased: 2.626
batch start
#iterations: 24
currently lose_sum: 88.5620476603508
time_elpased: 2.638
batch start
#iterations: 25
currently lose_sum: 88.03053510189056
time_elpased: 2.552
batch start
#iterations: 26
currently lose_sum: 87.98226934671402
time_elpased: 2.659
batch start
#iterations: 27
currently lose_sum: 88.56641733646393
time_elpased: 2.669
batch start
#iterations: 28
currently lose_sum: 87.48680680990219
time_elpased: 2.534
batch start
#iterations: 29
currently lose_sum: 87.59559881687164
time_elpased: 2.555
batch start
#iterations: 30
currently lose_sum: 86.83592927455902
time_elpased: 2.61
batch start
#iterations: 31
currently lose_sum: 86.37082362174988
time_elpased: 2.607
batch start
#iterations: 32
currently lose_sum: 86.61417543888092
time_elpased: 2.601
batch start
#iterations: 33
currently lose_sum: 86.41624349355698
time_elpased: 2.651
batch start
#iterations: 34
currently lose_sum: 85.61758995056152
time_elpased: 2.55
batch start
#iterations: 35
currently lose_sum: 85.91338509321213
time_elpased: 2.676
batch start
#iterations: 36
currently lose_sum: 85.78015178442001
time_elpased: 2.609
batch start
#iterations: 37
currently lose_sum: 85.12082654237747
time_elpased: 2.577
batch start
#iterations: 38
currently lose_sum: 84.85822719335556
time_elpased: 2.693
batch start
#iterations: 39
currently lose_sum: 85.2583544254303
time_elpased: 2.5
start validation test
0.652731958763
0.658609306783
0.636578839028
0.647406709583
0.652758647106
61.089
batch start
#iterations: 40
currently lose_sum: 84.41881650686264
time_elpased: 2.588
batch start
#iterations: 41
currently lose_sum: 84.79028618335724
time_elpased: 2.619
batch start
#iterations: 42
currently lose_sum: 84.52773094177246
time_elpased: 2.512
batch start
#iterations: 43
currently lose_sum: 83.7686676979065
time_elpased: 2.555
batch start
#iterations: 44
currently lose_sum: 84.17110115289688
time_elpased: 2.645
batch start
#iterations: 45
currently lose_sum: 83.89415395259857
time_elpased: 2.568
batch start
#iterations: 46
currently lose_sum: 83.53274822235107
time_elpased: 2.466
batch start
#iterations: 47
currently lose_sum: 83.19040429592133
time_elpased: 2.524
batch start
#iterations: 48
currently lose_sum: 82.61347988247871
time_elpased: 2.538
batch start
#iterations: 49
currently lose_sum: 83.35214704275131
time_elpased: 2.674
batch start
#iterations: 50
currently lose_sum: 83.29686462879181
time_elpased: 2.63
batch start
#iterations: 51
currently lose_sum: 82.00996747612953
time_elpased: 2.657
batch start
#iterations: 52
currently lose_sum: 82.30056810379028
time_elpased: 2.707
batch start
#iterations: 53
currently lose_sum: 81.70381054282188
time_elpased: 2.618
batch start
#iterations: 54
currently lose_sum: 81.97232347726822
time_elpased: 2.543
batch start
#iterations: 55
currently lose_sum: 81.5519936978817
time_elpased: 2.736
batch start
#iterations: 56
currently lose_sum: 81.23796093463898
time_elpased: 2.5
batch start
#iterations: 57
currently lose_sum: 81.49951988458633
time_elpased: 2.42
batch start
#iterations: 58
currently lose_sum: 81.91146498918533
time_elpased: 2.309
batch start
#iterations: 59
currently lose_sum: 80.7553579211235
time_elpased: 2.28
start validation test
0.631804123711
0.650064154905
0.573589954714
0.609437366723
0.631900305734
64.296
batch start
#iterations: 60
currently lose_sum: 80.86468267440796
time_elpased: 2.581
batch start
#iterations: 61
currently lose_sum: 80.6256902217865
time_elpased: 2.669
batch start
#iterations: 62
currently lose_sum: 80.27464029192924
time_elpased: 2.654
batch start
#iterations: 63
currently lose_sum: 80.20027166604996
time_elpased: 2.698
batch start
#iterations: 64
currently lose_sum: 79.97315961122513
time_elpased: 2.618
batch start
#iterations: 65
currently lose_sum: 79.9110661149025
time_elpased: 2.533
batch start
#iterations: 66
currently lose_sum: 79.58831515908241
time_elpased: 2.503
batch start
#iterations: 67
currently lose_sum: 80.13995864987373
time_elpased: 2.513
batch start
#iterations: 68
currently lose_sum: 78.67710539698601
time_elpased: 2.507
batch start
#iterations: 69
currently lose_sum: 79.23571214079857
time_elpased: 2.541
batch start
#iterations: 70
currently lose_sum: 78.2623205780983
time_elpased: 2.509
batch start
#iterations: 71
currently lose_sum: 79.12457314133644
time_elpased: 2.67
batch start
#iterations: 72
currently lose_sum: 78.15828368067741
time_elpased: 2.681
batch start
#iterations: 73
currently lose_sum: 77.97660872340202
time_elpased: 2.628
batch start
#iterations: 74
currently lose_sum: 77.41474613547325
time_elpased: 2.68
batch start
#iterations: 75
currently lose_sum: 77.86423766613007
time_elpased: 2.691
batch start
#iterations: 76
currently lose_sum: 78.57419237494469
time_elpased: 2.576
batch start
#iterations: 77
currently lose_sum: 78.14092686772346
time_elpased: 2.657
batch start
#iterations: 78
currently lose_sum: 77.95617425441742
time_elpased: 2.688
batch start
#iterations: 79
currently lose_sum: 77.65436244010925
time_elpased: 2.729
start validation test
0.611597938144
0.649363100945
0.48795800741
0.557207498384
0.611802217253
67.214
batch start
#iterations: 80
currently lose_sum: 77.64454925060272
time_elpased: 2.602
batch start
#iterations: 81
currently lose_sum: 77.1533882021904
time_elpased: 2.518
batch start
#iterations: 82
currently lose_sum: 77.5873726606369
time_elpased: 2.522
batch start
#iterations: 83
currently lose_sum: 76.99550700187683
time_elpased: 2.558
batch start
#iterations: 84
currently lose_sum: 76.98823517560959
time_elpased: 2.619
batch start
#iterations: 85
currently lose_sum: 77.15164110064507
time_elpased: 2.714
batch start
#iterations: 86
currently lose_sum: 75.91109639406204
time_elpased: 2.645
batch start
#iterations: 87
currently lose_sum: 76.41902360320091
time_elpased: 2.658
batch start
#iterations: 88
currently lose_sum: 75.80922490358353
time_elpased: 2.653
batch start
#iterations: 89
currently lose_sum: 76.82108455896378
time_elpased: 2.624
batch start
#iterations: 90
currently lose_sum: 76.2975744009018
time_elpased: 2.489
batch start
#iterations: 91
currently lose_sum: 75.41159009933472
time_elpased: 2.547
batch start
#iterations: 92
currently lose_sum: 76.08778139948845
time_elpased: 2.546
batch start
#iterations: 93
currently lose_sum: 76.23430186510086
time_elpased: 2.565
batch start
#iterations: 94
currently lose_sum: 75.83516651391983
time_elpased: 2.512
batch start
#iterations: 95
currently lose_sum: 74.55640837550163
time_elpased: 2.51
batch start
#iterations: 96
currently lose_sum: 74.87253737449646
time_elpased: 2.472
batch start
#iterations: 97
currently lose_sum: 74.9384928047657
time_elpased: 2.528
batch start
#iterations: 98
currently lose_sum: 74.78689727187157
time_elpased: 2.42
batch start
#iterations: 99
currently lose_sum: 74.99337410926819
time_elpased: 2.454
start validation test
0.59793814433
0.642771982116
0.443906957596
0.525143065871
0.598192636171
73.167
batch start
#iterations: 100
currently lose_sum: 75.66122975945473
time_elpased: 2.501
batch start
#iterations: 101
currently lose_sum: 74.08345639705658
time_elpased: 2.511
batch start
#iterations: 102
currently lose_sum: 74.14648485183716
time_elpased: 2.564
batch start
#iterations: 103
currently lose_sum: 74.73950099945068
time_elpased: 2.605
batch start
#iterations: 104
currently lose_sum: 72.99275496602058
time_elpased: 2.672
batch start
#iterations: 105
currently lose_sum: 74.13170632719994
time_elpased: 2.543
batch start
#iterations: 106
currently lose_sum: 73.57451164722443
time_elpased: 2.555
batch start
#iterations: 107
currently lose_sum: 73.68642768263817
time_elpased: 2.569
batch start
#iterations: 108
currently lose_sum: 73.77661690115929
time_elpased: 2.659
batch start
#iterations: 109
currently lose_sum: 73.60231184959412
time_elpased: 2.573
batch start
#iterations: 110
currently lose_sum: 73.50258320569992
time_elpased: 2.479
batch start
#iterations: 111
currently lose_sum: 73.69201201200485
time_elpased: 2.54
batch start
#iterations: 112
currently lose_sum: 72.69971641898155
time_elpased: 2.521
batch start
#iterations: 113
currently lose_sum: 72.35632613301277
time_elpased: 2.578
batch start
#iterations: 114
currently lose_sum: 73.2263925075531
time_elpased: 2.669
batch start
#iterations: 115
currently lose_sum: 72.58076885342598
time_elpased: 2.696
batch start
#iterations: 116
currently lose_sum: 72.65057256817818
time_elpased: 2.585
batch start
#iterations: 117
currently lose_sum: 73.02392366528511
time_elpased: 2.659
batch start
#iterations: 118
currently lose_sum: 72.41350591182709
time_elpased: 2.438
batch start
#iterations: 119
currently lose_sum: 72.5344605743885
time_elpased: 2.326
start validation test
0.614639175258
0.642493638677
0.519761218608
0.574647246245
0.61479593355
75.023
batch start
#iterations: 120
currently lose_sum: 72.56746903061867
time_elpased: 2.541
batch start
#iterations: 121
currently lose_sum: 71.06971290707588
time_elpased: 2.616
batch start
#iterations: 122
currently lose_sum: 71.88768282532692
time_elpased: 2.588
batch start
#iterations: 123
currently lose_sum: 71.76530629396439
time_elpased: 2.563
batch start
#iterations: 124
currently lose_sum: 72.00065472722054
time_elpased: 2.522
batch start
#iterations: 125
currently lose_sum: 70.80705279111862
time_elpased: 2.558
batch start
#iterations: 126
currently lose_sum: 71.67019844055176
time_elpased: 2.667
batch start
#iterations: 127
currently lose_sum: 71.38244411349297
time_elpased: 2.614
batch start
#iterations: 128
currently lose_sum: 71.35621067881584
time_elpased: 2.608
batch start
#iterations: 129
currently lose_sum: 70.86393836140633
time_elpased: 2.639
batch start
#iterations: 130
currently lose_sum: 71.2538435459137
time_elpased: 2.639
batch start
#iterations: 131
currently lose_sum: 70.88999989628792
time_elpased: 2.564
batch start
#iterations: 132
currently lose_sum: 70.56374353170395
time_elpased: 2.509
batch start
#iterations: 133
currently lose_sum: 70.91180768609047
time_elpased: 2.622
batch start
#iterations: 134
currently lose_sum: 70.76060006022453
time_elpased: 2.627
batch start
#iterations: 135
currently lose_sum: 70.35702365636826
time_elpased: 2.61
batch start
#iterations: 136
currently lose_sum: 70.97602739930153
time_elpased: 2.614
batch start
#iterations: 137
currently lose_sum: 69.96062695980072
time_elpased: 2.606
batch start
#iterations: 138
currently lose_sum: 70.09084057807922
time_elpased: 2.571
batch start
#iterations: 139
currently lose_sum: 69.26907715201378
time_elpased: 2.509
start validation test
0.580824742268
0.623441396509
0.411692054343
0.495908752789
0.581104184958
80.863
batch start
#iterations: 140
currently lose_sum: 69.97697708010674
time_elpased: 2.652
batch start
#iterations: 141
currently lose_sum: 69.53964072465897
time_elpased: 2.621
batch start
#iterations: 142
currently lose_sum: 69.89329248666763
time_elpased: 2.593
batch start
#iterations: 143
currently lose_sum: 69.50234967470169
time_elpased: 2.633
batch start
#iterations: 144
currently lose_sum: 68.7559361755848
time_elpased: 2.553
batch start
#iterations: 145
currently lose_sum: 69.36417698860168
time_elpased: 2.509
batch start
#iterations: 146
currently lose_sum: 69.58184897899628
time_elpased: 2.52
batch start
#iterations: 147
currently lose_sum: 69.5214636027813
time_elpased: 2.627
batch start
#iterations: 148
currently lose_sum: 69.10050663352013
time_elpased: 2.638
batch start
#iterations: 149
currently lose_sum: 68.99524682760239
time_elpased: 2.618
batch start
#iterations: 150
currently lose_sum: 69.34346154332161
time_elpased: 2.595
batch start
#iterations: 151
currently lose_sum: 69.50352025032043
time_elpased: 2.644
batch start
#iterations: 152
currently lose_sum: 68.38462260365486
time_elpased: 2.67
batch start
#iterations: 153
currently lose_sum: 69.23755633831024
time_elpased: 2.661
batch start
#iterations: 154
currently lose_sum: 68.1484464108944
time_elpased: 2.647
batch start
#iterations: 155
currently lose_sum: 68.09590148925781
time_elpased: 2.622
batch start
#iterations: 156
currently lose_sum: 67.14287316799164
time_elpased: 2.677
batch start
#iterations: 157
currently lose_sum: 68.70765808224678
time_elpased: 2.652
batch start
#iterations: 158
currently lose_sum: 68.65550670027733
time_elpased: 2.727
batch start
#iterations: 159
currently lose_sum: 68.42559120059013
time_elpased: 2.642
start validation test
0.593711340206
0.654846335697
0.399135446686
0.495971351835
0.59403282041
86.408
batch start
#iterations: 160
currently lose_sum: 68.02610117197037
time_elpased: 2.627
batch start
#iterations: 161
currently lose_sum: 67.84783375263214
time_elpased: 2.549
batch start
#iterations: 162
currently lose_sum: 67.9045829474926
time_elpased: 2.627
batch start
#iterations: 163
currently lose_sum: 67.47235321998596
time_elpased: 2.75
batch start
#iterations: 164
currently lose_sum: 67.64359131455421
time_elpased: 2.644
batch start
#iterations: 165
currently lose_sum: 67.31366229057312
time_elpased: 2.616
batch start
#iterations: 166
currently lose_sum: 66.89206114411354
time_elpased: 2.614
batch start
#iterations: 167
currently lose_sum: 67.45157492160797
time_elpased: 2.664
batch start
#iterations: 168
currently lose_sum: 67.02239093184471
time_elpased: 2.678
batch start
#iterations: 169
currently lose_sum: 67.14695656299591
time_elpased: 2.69
batch start
#iterations: 170
currently lose_sum: 66.78321132063866
time_elpased: 2.62
batch start
#iterations: 171
currently lose_sum: 66.9581030011177
time_elpased: 2.599
batch start
#iterations: 172
currently lose_sum: 67.03409412503242
time_elpased: 2.646
batch start
#iterations: 173
currently lose_sum: 66.32840394973755
time_elpased: 2.725
batch start
#iterations: 174
currently lose_sum: 66.4115649163723
time_elpased: 2.685
batch start
#iterations: 175
currently lose_sum: 66.50180977582932
time_elpased: 2.817
batch start
#iterations: 176
currently lose_sum: 66.08293625712395
time_elpased: 2.671
batch start
#iterations: 177
currently lose_sum: 65.61738395690918
time_elpased: 2.505
batch start
#iterations: 178
currently lose_sum: 65.97655600309372
time_elpased: 2.553
batch start
#iterations: 179
currently lose_sum: 64.94740158319473
time_elpased: 2.543
start validation test
0.608195876289
0.655446126709
0.458933717579
0.539863187844
0.608442488695
90.886
batch start
#iterations: 180
currently lose_sum: 65.81736850738525
time_elpased: 2.517
batch start
#iterations: 181
currently lose_sum: 66.44450196623802
time_elpased: 2.508
batch start
#iterations: 182
currently lose_sum: 65.45480623841286
time_elpased: 2.56
batch start
#iterations: 183
currently lose_sum: 64.78215089440346
time_elpased: 2.491
batch start
#iterations: 184
currently lose_sum: 65.16988477110863
time_elpased: 2.655
batch start
#iterations: 185
currently lose_sum: 65.08855849504471
time_elpased: 2.622
batch start
#iterations: 186
currently lose_sum: 65.2155177295208
time_elpased: 2.636
batch start
#iterations: 187
currently lose_sum: 64.80426871776581
time_elpased: 2.659
batch start
#iterations: 188
currently lose_sum: 64.31471309065819
time_elpased: 2.516
batch start
#iterations: 189
currently lose_sum: 64.57457771897316
time_elpased: 2.515
batch start
#iterations: 190
currently lose_sum: 65.06382763385773
time_elpased: 2.604
batch start
#iterations: 191
currently lose_sum: 66.0069127380848
time_elpased: 2.653
batch start
#iterations: 192
currently lose_sum: 64.59406560659409
time_elpased: 2.556
batch start
#iterations: 193
currently lose_sum: 65.43686336278915
time_elpased: 2.699
batch start
#iterations: 194
currently lose_sum: 63.717997789382935
time_elpased: 2.621
batch start
#iterations: 195
currently lose_sum: 64.27556559443474
time_elpased: 2.626
batch start
#iterations: 196
currently lose_sum: 63.53725644946098
time_elpased: 2.586
batch start
#iterations: 197
currently lose_sum: 64.44160082936287
time_elpased: 2.298
batch start
#iterations: 198
currently lose_sum: 63.695362120866776
time_elpased: 2.438
batch start
#iterations: 199
currently lose_sum: 63.868653148412704
time_elpased: 2.511
start validation test
0.589948453608
0.641354952641
0.411177439275
0.501097522734
0.590243820836
87.277
batch start
#iterations: 200
currently lose_sum: 63.85437121987343
time_elpased: 2.511
batch start
#iterations: 201
currently lose_sum: 64.1706690788269
time_elpased: 2.515
batch start
#iterations: 202
currently lose_sum: 63.15945568680763
time_elpased: 2.504
batch start
#iterations: 203
currently lose_sum: 63.54053819179535
time_elpased: 2.488
batch start
#iterations: 204
currently lose_sum: 63.78720465302467
time_elpased: 2.465
batch start
#iterations: 205
currently lose_sum: 63.496570378541946
time_elpased: 2.503
batch start
#iterations: 206
currently lose_sum: 62.26536911725998
time_elpased: 2.515
batch start
#iterations: 207
currently lose_sum: 62.78959575295448
time_elpased: 2.508
batch start
#iterations: 208
currently lose_sum: 62.96323511004448
time_elpased: 2.654
batch start
#iterations: 209
currently lose_sum: 62.85546886920929
time_elpased: 2.51
batch start
#iterations: 210
currently lose_sum: 62.3246553838253
time_elpased: 2.507
batch start
#iterations: 211
currently lose_sum: 62.2520050406456
time_elpased: 2.493
batch start
#iterations: 212
currently lose_sum: 63.600079864263535
time_elpased: 2.519
batch start
#iterations: 213
currently lose_sum: 61.56359088420868
time_elpased: 2.599
batch start
#iterations: 214
currently lose_sum: 62.15268525481224
time_elpased: 2.514
batch start
#iterations: 215
currently lose_sum: 61.408650159835815
time_elpased: 2.515
batch start
#iterations: 216
currently lose_sum: 61.80424952507019
time_elpased: 2.511
batch start
#iterations: 217
currently lose_sum: 62.002600729465485
time_elpased: 2.377
batch start
#iterations: 218
currently lose_sum: 61.05744171142578
time_elpased: 2.466
batch start
#iterations: 219
currently lose_sum: 61.224525570869446
time_elpased: 2.621
start validation test
0.580103092784
0.627435064935
0.397797447509
0.486898463089
0.580404299963
94.405
batch start
#iterations: 220
currently lose_sum: 60.99171406030655
time_elpased: 2.652
batch start
#iterations: 221
currently lose_sum: 60.0959133207798
time_elpased: 2.548
batch start
#iterations: 222
currently lose_sum: 61.23145279288292
time_elpased: 2.588
batch start
#iterations: 223
currently lose_sum: 60.251778960227966
time_elpased: 2.505
batch start
#iterations: 224
currently lose_sum: 61.37815189361572
time_elpased: 2.537
batch start
#iterations: 225
currently lose_sum: 60.766702473163605
time_elpased: 2.624
batch start
#iterations: 226
currently lose_sum: 61.467012733221054
time_elpased: 2.623
batch start
#iterations: 227
currently lose_sum: 60.12075385451317
time_elpased: 2.597
batch start
#iterations: 228
currently lose_sum: 59.583207696676254
time_elpased: 2.654
batch start
#iterations: 229
currently lose_sum: 59.64086890220642
time_elpased: 2.551
batch start
#iterations: 230
currently lose_sum: 60.37594789266586
time_elpased: 2.621
batch start
#iterations: 231
currently lose_sum: 59.58729088306427
time_elpased: 2.606
batch start
#iterations: 232
currently lose_sum: 59.802818566560745
time_elpased: 2.655
batch start
#iterations: 233
currently lose_sum: 60.996083319187164
time_elpased: 2.758
batch start
#iterations: 234
currently lose_sum: 59.93050375580788
time_elpased: 2.645
batch start
#iterations: 235
currently lose_sum: 59.66452005505562
time_elpased: 2.651
batch start
#iterations: 236
currently lose_sum: 59.52868467569351
time_elpased: 2.615
batch start
#iterations: 237
currently lose_sum: 58.8910157084465
time_elpased: 2.522
batch start
#iterations: 238
currently lose_sum: 60.65773841738701
time_elpased: 2.665
batch start
#iterations: 239
currently lose_sum: 59.658899664878845
time_elpased: 2.642
start validation test
0.57293814433
0.628756523304
0.359613009469
0.4575394487
0.573290602215
101.086
batch start
#iterations: 240
currently lose_sum: 59.93335446715355
time_elpased: 2.641
batch start
#iterations: 241
currently lose_sum: 59.57973378896713
time_elpased: 2.655
batch start
#iterations: 242
currently lose_sum: 59.053481847047806
time_elpased: 2.725
batch start
#iterations: 243
currently lose_sum: 58.28623029589653
time_elpased: 2.707
batch start
#iterations: 244
currently lose_sum: 58.825764894485474
time_elpased: 2.636
batch start
#iterations: 245
currently lose_sum: 58.25621396303177
time_elpased: 2.645
batch start
#iterations: 246
currently lose_sum: 57.9582484960556
time_elpased: 2.64
batch start
#iterations: 247
currently lose_sum: 58.16027879714966
time_elpased: 2.646
batch start
#iterations: 248
currently lose_sum: 58.82356142997742
time_elpased: 2.686
batch start
#iterations: 249
currently lose_sum: 57.523446172475815
time_elpased: 2.558
batch start
#iterations: 250
currently lose_sum: 57.18814507126808
time_elpased: 2.581
batch start
#iterations: 251
currently lose_sum: 57.922214925289154
time_elpased: 2.738
batch start
#iterations: 252
currently lose_sum: 58.03930988907814
time_elpased: 2.668
batch start
#iterations: 253
currently lose_sum: 57.05929619073868
time_elpased: 2.682
batch start
#iterations: 254
currently lose_sum: 56.42283254861832
time_elpased: 2.652
batch start
#iterations: 255
currently lose_sum: 57.429015070199966
time_elpased: 2.516
batch start
#iterations: 256
currently lose_sum: 56.43621647357941
time_elpased: 2.37
batch start
#iterations: 257
currently lose_sum: 56.593693882226944
time_elpased: 2.438
batch start
#iterations: 258
currently lose_sum: 57.4274525642395
time_elpased: 2.358
batch start
#iterations: 259
currently lose_sum: 56.69340595602989
time_elpased: 2.538
start validation test
0.563917525773
0.6533203125
0.275421984356
0.387489139878
0.564394180943
114.149
batch start
#iterations: 260
currently lose_sum: 56.28015947341919
time_elpased: 2.648
batch start
#iterations: 261
currently lose_sum: 56.23669555783272
time_elpased: 2.673
batch start
#iterations: 262
currently lose_sum: 55.989516496658325
time_elpased: 2.667
batch start
#iterations: 263
currently lose_sum: 56.303627014160156
time_elpased: 2.708
batch start
#iterations: 264
currently lose_sum: 56.65759941935539
time_elpased: 2.573
batch start
#iterations: 265
currently lose_sum: 56.06717437505722
time_elpased: 2.752
batch start
#iterations: 266
currently lose_sum: 55.22024667263031
time_elpased: 2.662
batch start
#iterations: 267
currently lose_sum: 56.07405686378479
time_elpased: 2.684
batch start
#iterations: 268
currently lose_sum: 54.95896816253662
time_elpased: 2.68
batch start
#iterations: 269
currently lose_sum: 54.69127240777016
time_elpased: 2.633
batch start
#iterations: 270
currently lose_sum: 55.64008590579033
time_elpased: 2.638
batch start
#iterations: 271
currently lose_sum: 55.36293125152588
time_elpased: 2.539
batch start
#iterations: 272
currently lose_sum: 54.84796258807182
time_elpased: 2.561
batch start
#iterations: 273
currently lose_sum: 55.76999166607857
time_elpased: 2.546
batch start
#iterations: 274
currently lose_sum: 54.20712947845459
time_elpased: 2.654
batch start
#iterations: 275
currently lose_sum: 54.9705208837986
time_elpased: 2.642
batch start
#iterations: 276
currently lose_sum: 53.51760667562485
time_elpased: 2.68
batch start
#iterations: 277
currently lose_sum: 53.35993355512619
time_elpased: 2.639
batch start
#iterations: 278
currently lose_sum: 53.35031861066818
time_elpased: 2.663
batch start
#iterations: 279
currently lose_sum: 54.406688153743744
time_elpased: 2.626
start validation test
0.571082474227
0.64325323475
0.322354878551
0.429482344875
0.571493424406
118.579
batch start
#iterations: 280
currently lose_sum: 52.2535802423954
time_elpased: 2.689
batch start
#iterations: 281
currently lose_sum: 53.48107713460922
time_elpased: 2.55
batch start
#iterations: 282
currently lose_sum: 52.97464841604233
time_elpased: 2.558
batch start
#iterations: 283
currently lose_sum: 53.896534472703934
time_elpased: 2.506
batch start
#iterations: 284
currently lose_sum: 52.6129367351532
time_elpased: 2.608
batch start
#iterations: 285
currently lose_sum: 52.829005777835846
time_elpased: 2.499
batch start
#iterations: 286
currently lose_sum: 53.52213695645332
time_elpased: 2.592
batch start
#iterations: 287
currently lose_sum: 52.74741172790527
time_elpased: 2.547
batch start
#iterations: 288
currently lose_sum: 52.93858590722084
time_elpased: 2.539
batch start
#iterations: 289
currently lose_sum: 53.62018430233002
time_elpased: 2.544
batch start
#iterations: 290
currently lose_sum: 52.16786140203476
time_elpased: 2.666
batch start
#iterations: 291
currently lose_sum: 52.41792231798172
time_elpased: 2.52
batch start
#iterations: 292
currently lose_sum: 52.05867350101471
time_elpased: 2.597
batch start
#iterations: 293
currently lose_sum: 51.93797855079174
time_elpased: 2.551
batch start
#iterations: 294
currently lose_sum: 51.89166110754013
time_elpased: 2.61
batch start
#iterations: 295
currently lose_sum: 50.81898120045662
time_elpased: 2.51
batch start
#iterations: 296
currently lose_sum: 52.054550260305405
time_elpased: 2.56
batch start
#iterations: 297
currently lose_sum: 51.57182019948959
time_elpased: 2.53
batch start
#iterations: 298
currently lose_sum: 51.23639100790024
time_elpased: 2.664
batch start
#iterations: 299
currently lose_sum: 51.044803470373154
time_elpased: 2.661
start validation test
0.564072164948
0.637957484111
0.299608892548
0.407731633868
0.564509113767
120.839
batch start
#iterations: 300
currently lose_sum: 50.84523156285286
time_elpased: 2.715
batch start
#iterations: 301
currently lose_sum: 51.61133894324303
time_elpased: 2.523
batch start
#iterations: 302
currently lose_sum: 50.192418962717056
time_elpased: 2.537
batch start
#iterations: 303
currently lose_sum: 51.31429523229599
time_elpased: 2.514
batch start
#iterations: 304
currently lose_sum: 50.5630641579628
time_elpased: 2.617
batch start
#iterations: 305
currently lose_sum: 50.52021396160126
time_elpased: 2.528
batch start
#iterations: 306
currently lose_sum: 50.67830502986908
time_elpased: 2.538
batch start
#iterations: 307
currently lose_sum: 49.749564692378044
time_elpased: 2.561
batch start
#iterations: 308
currently lose_sum: 50.90066748857498
time_elpased: 2.598
batch start
#iterations: 309
currently lose_sum: 48.39538311958313
time_elpased: 2.645
batch start
#iterations: 310
currently lose_sum: 48.4925921857357
time_elpased: 2.69
batch start
#iterations: 311
currently lose_sum: 49.56701543927193
time_elpased: 2.695
batch start
#iterations: 312
currently lose_sum: 48.36586943268776
time_elpased: 2.604
batch start
#iterations: 313
currently lose_sum: 50.23756305873394
time_elpased: 2.585
batch start
#iterations: 314
currently lose_sum: 49.22995428740978
time_elpased: 2.654
batch start
#iterations: 315
currently lose_sum: 48.83251556754112
time_elpased: 2.638
batch start
#iterations: 316
currently lose_sum: 48.735978439450264
time_elpased: 2.677
batch start
#iterations: 317
currently lose_sum: 49.53994305431843
time_elpased: 2.663
batch start
#iterations: 318
currently lose_sum: 47.75597380101681
time_elpased: 2.703
batch start
#iterations: 319
currently lose_sum: 48.85439941287041
time_elpased: 2.752
start validation test
0.568298969072
0.642629227824
0.310930424043
0.419088575987
0.568724195912
129.308
batch start
#iterations: 320
currently lose_sum: 47.73496836423874
time_elpased: 2.693
batch start
#iterations: 321
currently lose_sum: 48.43411964178085
time_elpased: 2.579
batch start
#iterations: 322
currently lose_sum: 47.30024740099907
time_elpased: 2.651
batch start
#iterations: 323
currently lose_sum: 47.16216315329075
time_elpased: 2.642
batch start
#iterations: 324
currently lose_sum: 47.560261592268944
time_elpased: 2.693
batch start
#iterations: 325
currently lose_sum: 46.462233543395996
time_elpased: 2.665
batch start
#iterations: 326
currently lose_sum: 45.979505971074104
time_elpased: 2.59
batch start
#iterations: 327
currently lose_sum: 46.91198669373989
time_elpased: 2.477
batch start
#iterations: 328
currently lose_sum: 47.03468397259712
time_elpased: 2.545
batch start
#iterations: 329
currently lose_sum: 46.66234079003334
time_elpased: 2.542
batch start
#iterations: 330
currently lose_sum: 45.29188251495361
time_elpased: 2.503
batch start
#iterations: 331
currently lose_sum: 46.84133706986904
time_elpased: 2.532
batch start
#iterations: 332
currently lose_sum: 46.03238137066364
time_elpased: 2.545
batch start
#iterations: 333
currently lose_sum: 45.35176047682762
time_elpased: 2.51
batch start
#iterations: 334
currently lose_sum: 46.068726763129234
time_elpased: 2.358
batch start
#iterations: 335
currently lose_sum: 45.60686644911766
time_elpased: 2.424
batch start
#iterations: 336
currently lose_sum: 45.822100564837456
time_elpased: 2.549
batch start
#iterations: 337
currently lose_sum: 46.15896016359329
time_elpased: 2.657
batch start
#iterations: 338
currently lose_sum: 46.31356158852577
time_elpased: 2.505
batch start
#iterations: 339
currently lose_sum: 46.218902572989464
time_elpased: 2.523
start validation test
0.56175257732
0.656604747162
0.261939069576
0.374484991171
0.562247932144
150.343
batch start
#iterations: 340
currently lose_sum: 45.385244369506836
time_elpased: 2.513
batch start
#iterations: 341
currently lose_sum: 44.51401010155678
time_elpased: 2.508
batch start
#iterations: 342
currently lose_sum: 43.689310014247894
time_elpased: 2.583
batch start
#iterations: 343
currently lose_sum: 43.909018620848656
time_elpased: 2.706
batch start
#iterations: 344
currently lose_sum: 43.634876027703285
time_elpased: 2.706
batch start
#iterations: 345
currently lose_sum: 44.59441451728344
time_elpased: 2.707
batch start
#iterations: 346
currently lose_sum: 44.457580611109734
time_elpased: 2.533
batch start
#iterations: 347
currently lose_sum: 44.07507646083832
time_elpased: 2.571
batch start
#iterations: 348
currently lose_sum: 43.59346196055412
time_elpased: 2.643
batch start
#iterations: 349
currently lose_sum: 43.96044906973839
time_elpased: 2.664
batch start
#iterations: 350
currently lose_sum: 43.561502516269684
time_elpased: 2.694
batch start
#iterations: 351
currently lose_sum: 43.29336106777191
time_elpased: 2.676
batch start
#iterations: 352
currently lose_sum: 43.1685294508934
time_elpased: 2.593
batch start
#iterations: 353
currently lose_sum: 42.66024358570576
time_elpased: 2.633
batch start
#iterations: 354
currently lose_sum: 42.7932643443346
time_elpased: 2.722
batch start
#iterations: 355
currently lose_sum: 42.551113814115524
time_elpased: 2.718
batch start
#iterations: 356
currently lose_sum: 43.609872937202454
time_elpased: 2.662
batch start
#iterations: 357
currently lose_sum: 41.893656715750694
time_elpased: 2.677
batch start
#iterations: 358
currently lose_sum: 42.21150170266628
time_elpased: 2.583
batch start
#iterations: 359
currently lose_sum: 41.63576355576515
time_elpased: 2.487
start validation test
0.547783505155
0.614081780789
0.261218608481
0.36652465882
0.548256970494
150.564
batch start
#iterations: 360
currently lose_sum: 41.80868677794933
time_elpased: 2.526
batch start
#iterations: 361
currently lose_sum: 41.85229429602623
time_elpased: 2.516
batch start
#iterations: 362
currently lose_sum: 41.43437011539936
time_elpased: 2.502
batch start
#iterations: 363
currently lose_sum: 40.806480035185814
time_elpased: 2.517
batch start
#iterations: 364
currently lose_sum: 41.336562767624855
time_elpased: 2.526
batch start
#iterations: 365
currently lose_sum: 41.684574365615845
time_elpased: 2.579
batch start
#iterations: 366
currently lose_sum: 43.26158893108368
time_elpased: 2.522
batch start
#iterations: 367
currently lose_sum: 40.97438479959965
time_elpased: 2.521
batch start
#iterations: 368
currently lose_sum: 41.902851328253746
time_elpased: 2.69
batch start
#iterations: 369
currently lose_sum: 40.25869643688202
time_elpased: 2.562
batch start
#iterations: 370
currently lose_sum: 39.78569896519184
time_elpased: 2.508
batch start
#iterations: 371
currently lose_sum: 40.703271105885506
time_elpased: 2.522
batch start
#iterations: 372
currently lose_sum: 41.81211978197098
time_elpased: 2.63
batch start
#iterations: 373
currently lose_sum: 40.47844332456589
time_elpased: 2.636
batch start
#iterations: 374
currently lose_sum: 40.04450386762619
time_elpased: 2.623
batch start
#iterations: 375
currently lose_sum: 40.73143094778061
time_elpased: 2.503
batch start
#iterations: 376
currently lose_sum: 38.913221165537834
time_elpased: 2.654
batch start
#iterations: 377
currently lose_sum: 39.64074373245239
time_elpased: 2.649
batch start
#iterations: 378
currently lose_sum: 40.00873443484306
time_elpased: 2.597
batch start
#iterations: 379
currently lose_sum: 39.87154217064381
time_elpased: 2.566
start validation test
0.540103092784
0.616490610329
0.216241251544
0.320176775373
0.540638180501
170.581
batch start
#iterations: 380
currently lose_sum: 39.08164370059967
time_elpased: 2.645
batch start
#iterations: 381
currently lose_sum: 37.816398456692696
time_elpased: 2.524
batch start
#iterations: 382
currently lose_sum: 38.69734348356724
time_elpased: 2.484
batch start
#iterations: 383
currently lose_sum: 38.368332237005234
time_elpased: 2.629
batch start
#iterations: 384
currently lose_sum: 38.41579322516918
time_elpased: 2.687
batch start
#iterations: 385
currently lose_sum: 38.445536121726036
time_elpased: 2.559
batch start
#iterations: 386
currently lose_sum: 37.94241717457771
time_elpased: 2.54
batch start
#iterations: 387
currently lose_sum: 38.80644600093365
time_elpased: 2.475
batch start
#iterations: 388
currently lose_sum: 37.37662662565708
time_elpased: 2.621
batch start
#iterations: 389
currently lose_sum: 38.022638484835625
time_elpased: 2.728
batch start
#iterations: 390
currently lose_sum: 37.660435542464256
time_elpased: 2.562
batch start
#iterations: 391
currently lose_sum: 36.895662277936935
time_elpased: 2.599
batch start
#iterations: 392
currently lose_sum: 37.67920124530792
time_elpased: 2.457
batch start
#iterations: 393
currently lose_sum: 36.77995543181896
time_elpased: 2.387
batch start
#iterations: 394
currently lose_sum: 37.09312152862549
time_elpased: 2.487
batch start
#iterations: 395
currently lose_sum: 37.085714519023895
time_elpased: 2.303
batch start
#iterations: 396
currently lose_sum: 37.588798239827156
time_elpased: 2.274
batch start
#iterations: 397
currently lose_sum: 36.81174635887146
time_elpased: 2.289
batch start
#iterations: 398
currently lose_sum: 36.31535004079342
time_elpased: 2.292
batch start
#iterations: 399
currently lose_sum: 37.49469964206219
time_elpased: 2.277
start validation test
0.544948453608
0.646728354263
0.201420337587
0.307173128237
0.545516034139
183.914
acc: 0.651
pre: 0.639
rec: 0.696
F1: 0.666
auc: 0.651
