start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 99.98745024204254
time_elpased: 2.943
batch start
#iterations: 1
currently lose_sum: 99.28122538328171
time_elpased: 2.946
batch start
#iterations: 2
currently lose_sum: 98.88026839494705
time_elpased: 2.932
batch start
#iterations: 3
currently lose_sum: 98.55381834506989
time_elpased: 2.897
batch start
#iterations: 4
currently lose_sum: 98.26988780498505
time_elpased: 2.978
batch start
#iterations: 5
currently lose_sum: 98.18816310167313
time_elpased: 3.026
batch start
#iterations: 6
currently lose_sum: 97.57851958274841
time_elpased: 3.043
batch start
#iterations: 7
currently lose_sum: 97.85068982839584
time_elpased: 2.996
batch start
#iterations: 8
currently lose_sum: 97.08728450536728
time_elpased: 2.974
batch start
#iterations: 9
currently lose_sum: 97.68915098905563
time_elpased: 2.986
batch start
#iterations: 10
currently lose_sum: 97.1636421084404
time_elpased: 2.968
batch start
#iterations: 11
currently lose_sum: 96.81220692396164
time_elpased: 2.971
batch start
#iterations: 12
currently lose_sum: 96.81414246559143
time_elpased: 2.995
batch start
#iterations: 13
currently lose_sum: 97.02645999193192
time_elpased: 2.994
batch start
#iterations: 14
currently lose_sum: 96.62829834222794
time_elpased: 2.897
batch start
#iterations: 15
currently lose_sum: 95.68982535600662
time_elpased: 2.927
batch start
#iterations: 16
currently lose_sum: 96.295933842659
time_elpased: 2.902
batch start
#iterations: 17
currently lose_sum: 96.13881379365921
time_elpased: 2.902
batch start
#iterations: 18
currently lose_sum: 96.58367818593979
time_elpased: 2.899
batch start
#iterations: 19
currently lose_sum: 96.14517211914062
time_elpased: 2.893
start validation test
0.664587628866
0.63214226906
0.790161572502
0.702373873668
0.664367164439
59.861
batch start
#iterations: 20
currently lose_sum: 95.9557141661644
time_elpased: 2.85
batch start
#iterations: 21
currently lose_sum: 94.78664171695709
time_elpased: 2.838
batch start
#iterations: 22
currently lose_sum: 96.62964969873428
time_elpased: 2.827
batch start
#iterations: 23
currently lose_sum: 95.95184755325317
time_elpased: 2.905
batch start
#iterations: 24
currently lose_sum: 96.18775427341461
time_elpased: 2.888
batch start
#iterations: 25
currently lose_sum: 96.18052750825882
time_elpased: 2.917
batch start
#iterations: 26
currently lose_sum: 97.05774754285812
time_elpased: 2.918
batch start
#iterations: 27
currently lose_sum: 95.8582335114479
time_elpased: 2.903
batch start
#iterations: 28
currently lose_sum: 95.99287921190262
time_elpased: 2.882
batch start
#iterations: 29
currently lose_sum: 95.29338389635086
time_elpased: 2.894
batch start
#iterations: 30
currently lose_sum: 96.04955953359604
time_elpased: 2.884
batch start
#iterations: 31
currently lose_sum: 99.23006594181061
time_elpased: 2.906
batch start
#iterations: 32
currently lose_sum: 95.84480786323547
time_elpased: 2.862
batch start
#iterations: 33
currently lose_sum: 96.0664781332016
time_elpased: 2.925
batch start
#iterations: 34
currently lose_sum: 94.62430304288864
time_elpased: 2.935
batch start
#iterations: 35
currently lose_sum: 94.86109918355942
time_elpased: 2.934
batch start
#iterations: 36
currently lose_sum: 95.50783896446228
time_elpased: 2.889
batch start
#iterations: 37
currently lose_sum: 95.18035387992859
time_elpased: 2.911
batch start
#iterations: 38
currently lose_sum: 94.36164343357086
time_elpased: 2.865
batch start
#iterations: 39
currently lose_sum: 94.34207034111023
time_elpased: 2.814
start validation test
0.674793814433
0.662347560976
0.715447154472
0.687874140405
0.674722441224
59.083
batch start
#iterations: 40
currently lose_sum: 94.11068046092987
time_elpased: 2.855
batch start
#iterations: 41
currently lose_sum: 94.19016367197037
time_elpased: 2.864
batch start
#iterations: 42
currently lose_sum: 93.74818915128708
time_elpased: 3.019
batch start
#iterations: 43
currently lose_sum: 93.65338909626007
time_elpased: 2.922
batch start
#iterations: 44
currently lose_sum: 94.87123727798462
time_elpased: 2.959
batch start
#iterations: 45
currently lose_sum: 94.90010768175125
time_elpased: 2.976
batch start
#iterations: 46
currently lose_sum: 93.5001066327095
time_elpased: 2.864
batch start
#iterations: 47
currently lose_sum: 94.07428896427155
time_elpased: 2.865
batch start
#iterations: 48
currently lose_sum: 93.4159500002861
time_elpased: 2.897
batch start
#iterations: 49
currently lose_sum: 93.92372560501099
time_elpased: 2.817
batch start
#iterations: 50
currently lose_sum: 94.22513914108276
time_elpased: 2.937
batch start
#iterations: 51
currently lose_sum: 96.76654648780823
time_elpased: 2.996
batch start
#iterations: 52
currently lose_sum: 99.75349450111389
time_elpased: 3.036
batch start
#iterations: 53
currently lose_sum: 93.78430843353271
time_elpased: 2.978
batch start
#iterations: 54
currently lose_sum: 94.33153563737869
time_elpased: 2.976
batch start
#iterations: 55
currently lose_sum: 93.72332006692886
time_elpased: 2.958
batch start
#iterations: 56
currently lose_sum: 93.73852694034576
time_elpased: 2.976
batch start
#iterations: 57
currently lose_sum: 95.01632124185562
time_elpased: 2.98
batch start
#iterations: 58
currently lose_sum: 93.0828412771225
time_elpased: 2.943
batch start
#iterations: 59
currently lose_sum: 93.73803275823593
time_elpased: 2.93
start validation test
0.662319587629
0.609202538631
0.908819594525
0.729442861273
0.661886818847
59.920
batch start
#iterations: 60
currently lose_sum: 93.15155130624771
time_elpased: 2.986
batch start
#iterations: 61
currently lose_sum: 93.38152229785919
time_elpased: 2.934
batch start
#iterations: 62
currently lose_sum: 93.53245782852173
time_elpased: 2.946
batch start
#iterations: 63
currently lose_sum: 93.52729737758636
time_elpased: 2.933
batch start
#iterations: 64
currently lose_sum: 92.88270556926727
time_elpased: 2.958
batch start
#iterations: 65
currently lose_sum: 93.33206236362457
time_elpased: 2.975
batch start
#iterations: 66
currently lose_sum: 92.95356529951096
time_elpased: 2.935
batch start
#iterations: 67
currently lose_sum: 92.01429396867752
time_elpased: 2.964
batch start
#iterations: 68
currently lose_sum: 92.23364037275314
time_elpased: 2.984
batch start
#iterations: 69
currently lose_sum: 92.62188374996185
time_elpased: 2.913
batch start
#iterations: 70
currently lose_sum: 92.7556539773941
time_elpased: 2.962
batch start
#iterations: 71
currently lose_sum: 92.04176586866379
time_elpased: 2.903
batch start
#iterations: 72
currently lose_sum: 100.33017474412918
time_elpased: 2.956
batch start
#iterations: 73
currently lose_sum: 95.76432472467422
time_elpased: 2.916
batch start
#iterations: 74
currently lose_sum: 94.43779385089874
time_elpased: 2.949
batch start
#iterations: 75
currently lose_sum: 93.09447705745697
time_elpased: 2.912
batch start
#iterations: 76
currently lose_sum: 92.03597205877304
time_elpased: 2.933
batch start
#iterations: 77
currently lose_sum: 92.97279417514801
time_elpased: 2.913
batch start
#iterations: 78
currently lose_sum: 92.71502542495728
time_elpased: 2.919
batch start
#iterations: 79
currently lose_sum: 92.54424530267715
time_elpased: 2.873
start validation test
0.696494845361
0.658001155402
0.820520736853
0.730328844921
0.696277098779
57.032
batch start
#iterations: 80
currently lose_sum: 93.83219331502914
time_elpased: 2.906
batch start
#iterations: 81
currently lose_sum: 91.97030693292618
time_elpased: 2.875
batch start
#iterations: 82
currently lose_sum: 92.59957629442215
time_elpased: 2.872
batch start
#iterations: 83
currently lose_sum: 91.39279061555862
time_elpased: 2.88
batch start
#iterations: 84
currently lose_sum: 92.32068717479706
time_elpased: 2.856
batch start
#iterations: 85
currently lose_sum: 93.49444979429245
time_elpased: 3.065
batch start
#iterations: 86
currently lose_sum: 92.71370524168015
time_elpased: 2.886
batch start
#iterations: 87
currently lose_sum: 92.66733193397522
time_elpased: 2.88
batch start
#iterations: 88
currently lose_sum: 92.56664210557938
time_elpased: 2.898
batch start
#iterations: 89
currently lose_sum: 91.94753432273865
time_elpased: 2.924
batch start
#iterations: 90
currently lose_sum: 91.74655795097351
time_elpased: 2.875
batch start
#iterations: 91
currently lose_sum: 91.96079099178314
time_elpased: 2.868
batch start
#iterations: 92
currently lose_sum: 91.59306716918945
time_elpased: 2.878
batch start
#iterations: 93
currently lose_sum: 91.11406326293945
time_elpased: 2.892
batch start
#iterations: 94
currently lose_sum: 91.62376564741135
time_elpased: 2.798
batch start
#iterations: 95
currently lose_sum: 91.12510168552399
time_elpased: 2.874
batch start
#iterations: 96
currently lose_sum: 92.26854372024536
time_elpased: 2.909
batch start
#iterations: 97
currently lose_sum: 91.17740762233734
time_elpased: 2.865
batch start
#iterations: 98
currently lose_sum: 90.5908453464508
time_elpased: 2.878
batch start
#iterations: 99
currently lose_sum: 91.6247267127037
time_elpased: 2.898
start validation test
0.597474226804
0.566231602333
0.839353709993
0.676257203267
0.597049570064
63.692
batch start
#iterations: 100
currently lose_sum: 91.29451191425323
time_elpased: 2.927
batch start
#iterations: 101
currently lose_sum: 91.55580991506577
time_elpased: 2.836
batch start
#iterations: 102
currently lose_sum: 91.03507542610168
time_elpased: 2.861
batch start
#iterations: 103
currently lose_sum: 91.63776981830597
time_elpased: 2.877
batch start
#iterations: 104
currently lose_sum: 91.31919491291046
time_elpased: 2.891
batch start
#iterations: 105
currently lose_sum: 91.02001732587814
time_elpased: 2.795
batch start
#iterations: 106
currently lose_sum: 90.21385031938553
time_elpased: 2.826
batch start
#iterations: 107
currently lose_sum: 91.76335942745209
time_elpased: 2.856
batch start
#iterations: 108
currently lose_sum: 90.53696888685226
time_elpased: 2.896
batch start
#iterations: 109
currently lose_sum: 90.28318905830383
time_elpased: 2.879
batch start
#iterations: 110
currently lose_sum: 91.513463139534
time_elpased: 2.882
batch start
#iterations: 111
currently lose_sum: 90.65082210302353
time_elpased: 2.878
batch start
#iterations: 112
currently lose_sum: 91.64090967178345
time_elpased: 2.905
batch start
#iterations: 113
currently lose_sum: 90.32106077671051
time_elpased: 2.894
batch start
#iterations: 114
currently lose_sum: 90.02273827791214
time_elpased: 2.845
batch start
#iterations: 115
currently lose_sum: 90.96211439371109
time_elpased: 2.905
batch start
#iterations: 116
currently lose_sum: 90.84473371505737
time_elpased: 2.902
batch start
#iterations: 117
currently lose_sum: 90.02865880727768
time_elpased: 2.887
batch start
#iterations: 118
currently lose_sum: 89.86234694719315
time_elpased: 2.929
batch start
#iterations: 119
currently lose_sum: 90.74362206459045
time_elpased: 2.898
start validation test
0.61175257732
0.575947167188
0.85262941237
0.687494813708
0.611329680883
62.212
batch start
#iterations: 120
currently lose_sum: 91.38845497369766
time_elpased: 2.888
batch start
#iterations: 121
currently lose_sum: 89.83922255039215
time_elpased: 2.881
batch start
#iterations: 122
currently lose_sum: 90.32423424720764
time_elpased: 2.911
batch start
#iterations: 123
currently lose_sum: 90.10171419382095
time_elpased: 2.887
batch start
#iterations: 124
currently lose_sum: 90.95519441366196
time_elpased: 2.928
batch start
#iterations: 125
currently lose_sum: 90.09596997499466
time_elpased: 2.918
batch start
#iterations: 126
currently lose_sum: 90.86222332715988
time_elpased: 2.942
batch start
#iterations: 127
currently lose_sum: 90.82247537374496
time_elpased: 2.88
batch start
#iterations: 128
currently lose_sum: 89.69049972295761
time_elpased: 2.929
batch start
#iterations: 129
currently lose_sum: 90.81491005420685
time_elpased: 2.855
batch start
#iterations: 130
currently lose_sum: 90.04543453454971
time_elpased: 2.865
batch start
#iterations: 131
currently lose_sum: 89.88496035337448
time_elpased: 2.843
batch start
#iterations: 132
currently lose_sum: 90.09824877977371
time_elpased: 2.933
batch start
#iterations: 133
currently lose_sum: 89.46402448415756
time_elpased: 2.904
batch start
#iterations: 134
currently lose_sum: 89.62515133619308
time_elpased: 2.907
batch start
#iterations: 135
currently lose_sum: 90.37626904249191
time_elpased: 2.939
batch start
#iterations: 136
currently lose_sum: 89.71548837423325
time_elpased: 2.914
batch start
#iterations: 137
currently lose_sum: 90.13846844434738
time_elpased: 2.868
batch start
#iterations: 138
currently lose_sum: 89.74815791845322
time_elpased: 2.877
batch start
#iterations: 139
currently lose_sum: 88.65788042545319
time_elpased: 2.889
start validation test
0.682783505155
0.691332832134
0.662447257384
0.676581879336
0.682819208574
57.572
batch start
#iterations: 140
currently lose_sum: 89.47888660430908
time_elpased: 2.924
batch start
#iterations: 141
currently lose_sum: 89.55524778366089
time_elpased: 2.817
batch start
#iterations: 142
currently lose_sum: 89.03098165988922
time_elpased: 3.01
batch start
#iterations: 143
currently lose_sum: 90.1841070652008
time_elpased: 3.034
batch start
#iterations: 144
currently lose_sum: 88.81898581981659
time_elpased: 2.971
batch start
#iterations: 145
currently lose_sum: 88.42511820793152
time_elpased: 2.984
batch start
#iterations: 146
currently lose_sum: 89.48912763595581
time_elpased: 2.99
batch start
#iterations: 147
currently lose_sum: 89.66983360052109
time_elpased: 2.957
batch start
#iterations: 148
currently lose_sum: 89.05937618017197
time_elpased: 2.926
batch start
#iterations: 149
currently lose_sum: 89.0866089463234
time_elpased: 2.895
batch start
#iterations: 150
currently lose_sum: 90.02969861030579
time_elpased: 2.965
batch start
#iterations: 151
currently lose_sum: 89.71778619289398
time_elpased: 2.963
batch start
#iterations: 152
currently lose_sum: 90.08583182096481
time_elpased: 2.985
batch start
#iterations: 153
currently lose_sum: 88.41530120372772
time_elpased: 2.968
batch start
#iterations: 154
currently lose_sum: 88.2952441573143
time_elpased: 2.924
batch start
#iterations: 155
currently lose_sum: 89.1559716463089
time_elpased: 2.937
batch start
#iterations: 156
currently lose_sum: 89.96016073226929
time_elpased: 2.956
batch start
#iterations: 157
currently lose_sum: 89.09000968933105
time_elpased: 2.959
batch start
#iterations: 158
currently lose_sum: 89.29527258872986
time_elpased: 2.978
batch start
#iterations: 159
currently lose_sum: 88.32530754804611
time_elpased: 2.964
start validation test
0.693144329897
0.695066334992
0.690130698775
0.692589723728
0.693149620791
56.626
batch start
#iterations: 160
currently lose_sum: 88.92771685123444
time_elpased: 2.975
batch start
#iterations: 161
currently lose_sum: 88.97052103281021
time_elpased: 2.738
batch start
#iterations: 162
currently lose_sum: 87.86846846342087
time_elpased: 2.908
batch start
#iterations: 163
currently lose_sum: 88.77169144153595
time_elpased: 2.949
batch start
#iterations: 164
currently lose_sum: 87.99949663877487
time_elpased: 2.935
batch start
#iterations: 165
currently lose_sum: 88.44452434778214
time_elpased: 2.919
batch start
#iterations: 166
currently lose_sum: 88.63541370630264
time_elpased: 2.994
batch start
#iterations: 167
currently lose_sum: 88.5602108836174
time_elpased: 2.951
batch start
#iterations: 168
currently lose_sum: 88.4965181350708
time_elpased: 2.963
batch start
#iterations: 169
currently lose_sum: 89.75554233789444
time_elpased: 2.97
batch start
#iterations: 170
currently lose_sum: 89.24656289815903
time_elpased: 2.878
batch start
#iterations: 171
currently lose_sum: 88.31639850139618
time_elpased: 2.969
batch start
#iterations: 172
currently lose_sum: 90.68199729919434
time_elpased: 2.957
batch start
#iterations: 173
currently lose_sum: 89.08430236577988
time_elpased: 3.011
batch start
#iterations: 174
currently lose_sum: 87.62511891126633
time_elpased: 2.985
batch start
#iterations: 175
currently lose_sum: 88.43103528022766
time_elpased: 2.919
batch start
#iterations: 176
currently lose_sum: 88.19173508882523
time_elpased: 3.035
batch start
#iterations: 177
currently lose_sum: 87.40269196033478
time_elpased: 3.046
batch start
#iterations: 178
currently lose_sum: 89.41633653640747
time_elpased: 2.943
batch start
#iterations: 179
currently lose_sum: 87.50223398208618
time_elpased: 2.897
start validation test
0.676597938144
0.699825885084
0.6204589894
0.657756927777
0.676696498728
58.168
batch start
#iterations: 180
currently lose_sum: 87.60974031686783
time_elpased: 2.939
batch start
#iterations: 181
currently lose_sum: 88.28881400823593
time_elpased: 2.933
batch start
#iterations: 182
currently lose_sum: 87.74243396520615
time_elpased: 2.893
batch start
#iterations: 183
currently lose_sum: 86.55302107334137
time_elpased: 2.958
batch start
#iterations: 184
currently lose_sum: 88.50440800189972
time_elpased: 2.911
batch start
#iterations: 185
currently lose_sum: 87.27144956588745
time_elpased: 2.785
batch start
#iterations: 186
currently lose_sum: 88.17531383037567
time_elpased: 2.933
batch start
#iterations: 187
currently lose_sum: 87.43580800294876
time_elpased: 2.907
batch start
#iterations: 188
currently lose_sum: 87.84602075815201
time_elpased: 2.918
batch start
#iterations: 189
currently lose_sum: 87.47011667490005
time_elpased: 2.923
batch start
#iterations: 190
currently lose_sum: 88.62852036952972
time_elpased: 2.912
batch start
#iterations: 191
currently lose_sum: 87.35932207107544
time_elpased: 2.882
batch start
#iterations: 192
currently lose_sum: 87.77522736787796
time_elpased: 2.834
batch start
#iterations: 193
currently lose_sum: 87.25560462474823
time_elpased: 2.872
batch start
#iterations: 194
currently lose_sum: 86.67756414413452
time_elpased: 2.879
batch start
#iterations: 195
currently lose_sum: 88.42707312107086
time_elpased: 2.918
batch start
#iterations: 196
currently lose_sum: 88.09871888160706
time_elpased: 2.884
batch start
#iterations: 197
currently lose_sum: 87.35302501916885
time_elpased: 2.861
batch start
#iterations: 198
currently lose_sum: 87.42278653383255
time_elpased: 2.829
batch start
#iterations: 199
currently lose_sum: 86.72205477952957
time_elpased: 2.874
start validation test
0.680154639175
0.689591880803
0.657301636308
0.67305969756
0.680194761147
57.454
batch start
#iterations: 200
currently lose_sum: 87.0540988445282
time_elpased: 2.895
batch start
#iterations: 201
currently lose_sum: 87.2084596157074
time_elpased: 2.848
batch start
#iterations: 202
currently lose_sum: 86.33281421661377
time_elpased: 2.884
batch start
#iterations: 203
currently lose_sum: 87.59390598535538
time_elpased: 2.903
batch start
#iterations: 204
currently lose_sum: 86.87475848197937
time_elpased: 2.891
batch start
#iterations: 205
currently lose_sum: 87.26611340045929
time_elpased: 2.891
batch start
#iterations: 206
currently lose_sum: 86.38390296697617
time_elpased: 2.903
batch start
#iterations: 207
currently lose_sum: 87.33834969997406
time_elpased: 2.906
batch start
#iterations: 208
currently lose_sum: 88.25288820266724
time_elpased: 2.875
batch start
#iterations: 209
currently lose_sum: 88.69853180646896
time_elpased: 2.877
batch start
#iterations: 210
currently lose_sum: 87.14741277694702
time_elpased: 2.913
batch start
#iterations: 211
currently lose_sum: 86.58736675977707
time_elpased: 2.899
batch start
#iterations: 212
currently lose_sum: 86.31372088193893
time_elpased: 2.865
batch start
#iterations: 213
currently lose_sum: 85.67508262395859
time_elpased: 2.87
batch start
#iterations: 214
currently lose_sum: 86.85745799541473
time_elpased: 2.845
batch start
#iterations: 215
currently lose_sum: 86.63056719303131
time_elpased: 2.842
batch start
#iterations: 216
currently lose_sum: 85.7306290268898
time_elpased: 2.873
batch start
#iterations: 217
currently lose_sum: 86.56372302770615
time_elpased: 2.919
batch start
#iterations: 218
currently lose_sum: 87.47965347766876
time_elpased: 2.926
batch start
#iterations: 219
currently lose_sum: 87.02624726295471
time_elpased: 2.942
start validation test
0.681134020619
0.699604296213
0.636822064423
0.666738498007
0.681211817092
58.371
batch start
#iterations: 220
currently lose_sum: 87.48509740829468
time_elpased: 2.917
batch start
#iterations: 221
currently lose_sum: 86.48658376932144
time_elpased: 2.873
batch start
#iterations: 222
currently lose_sum: 86.93004405498505
time_elpased: 2.933
batch start
#iterations: 223
currently lose_sum: 86.04082226753235
time_elpased: 2.887
batch start
#iterations: 224
currently lose_sum: 86.17409345507622
time_elpased: 2.912
batch start
#iterations: 225
currently lose_sum: 86.24904006719589
time_elpased: 2.933
batch start
#iterations: 226
currently lose_sum: 85.91036540269852
time_elpased: 2.872
batch start
#iterations: 227
currently lose_sum: 85.93525493144989
time_elpased: 2.879
batch start
#iterations: 228
currently lose_sum: 86.40786522626877
time_elpased: 2.892
batch start
#iterations: 229
currently lose_sum: 84.787018597126
time_elpased: 2.893
batch start
#iterations: 230
currently lose_sum: 86.55666571855545
time_elpased: 2.869
batch start
#iterations: 231
currently lose_sum: 86.04154753684998
time_elpased: 2.8
batch start
#iterations: 232
currently lose_sum: 86.84732806682587
time_elpased: 2.848
batch start
#iterations: 233
currently lose_sum: 86.04221153259277
time_elpased: 2.901
batch start
#iterations: 234
currently lose_sum: 85.26434135437012
time_elpased: 2.903
batch start
#iterations: 235
currently lose_sum: 85.36993831396103
time_elpased: 2.939
batch start
#iterations: 236
currently lose_sum: 85.48602724075317
time_elpased: 2.914
batch start
#iterations: 237
currently lose_sum: 85.56985688209534
time_elpased: 2.912
batch start
#iterations: 238
currently lose_sum: 87.55443346500397
time_elpased: 2.909
batch start
#iterations: 239
currently lose_sum: 87.26128613948822
time_elpased: 2.838
start validation test
0.657835051546
0.711382671976
0.533189255943
0.609529411765
0.658053886466
58.016
batch start
#iterations: 240
currently lose_sum: 85.47140568494797
time_elpased: 2.841
batch start
#iterations: 241
currently lose_sum: 85.83424812555313
time_elpased: 2.919
batch start
#iterations: 242
currently lose_sum: 85.69185400009155
time_elpased: 2.868
batch start
#iterations: 243
currently lose_sum: 85.54404366016388
time_elpased: 2.855
batch start
#iterations: 244
currently lose_sum: 85.51371669769287
time_elpased: 2.9
batch start
#iterations: 245
currently lose_sum: 85.29055219888687
time_elpased: 2.851
batch start
#iterations: 246
currently lose_sum: 84.47742259502411
time_elpased: 2.901
batch start
#iterations: 247
currently lose_sum: 85.26852309703827
time_elpased: 2.867
batch start
#iterations: 248
currently lose_sum: 85.22189599275589
time_elpased: 2.826
batch start
#iterations: 249
currently lose_sum: 84.18219864368439
time_elpased: 2.839
batch start
#iterations: 250
currently lose_sum: 84.75490206480026
time_elpased: 2.826
batch start
#iterations: 251
currently lose_sum: 83.86230736970901
time_elpased: 2.835
batch start
#iterations: 252
currently lose_sum: 85.41021943092346
time_elpased: 2.819
batch start
#iterations: 253
currently lose_sum: 85.29005181789398
time_elpased: 2.878
batch start
#iterations: 254
currently lose_sum: 85.03052991628647
time_elpased: 2.877
batch start
#iterations: 255
currently lose_sum: 84.3411414027214
time_elpased: 2.863
batch start
#iterations: 256
currently lose_sum: 84.19841176271439
time_elpased: 2.91
batch start
#iterations: 257
currently lose_sum: 83.51550871133804
time_elpased: 2.865
batch start
#iterations: 258
currently lose_sum: 85.6604614853859
time_elpased: 2.841
batch start
#iterations: 259
currently lose_sum: 84.21506214141846
time_elpased: 2.901
start validation test
0.672783505155
0.689078459984
0.631779355768
0.659186083969
0.672855494263
58.297
batch start
#iterations: 260
currently lose_sum: 83.74792194366455
time_elpased: 2.865
batch start
#iterations: 261
currently lose_sum: 85.16627013683319
time_elpased: 2.898
batch start
#iterations: 262
currently lose_sum: 84.20771431922913
time_elpased: 2.939
batch start
#iterations: 263
currently lose_sum: 84.2569130063057
time_elpased: 2.91
batch start
#iterations: 264
currently lose_sum: 83.57127943634987
time_elpased: 2.924
batch start
#iterations: 265
currently lose_sum: 84.61256870627403
time_elpased: 2.913
batch start
#iterations: 266
currently lose_sum: 84.69371032714844
time_elpased: 2.888
batch start
#iterations: 267
currently lose_sum: 84.92237794399261
time_elpased: 2.842
batch start
#iterations: 268
currently lose_sum: 83.29638466238976
time_elpased: 2.761
batch start
#iterations: 269
currently lose_sum: 83.47107303142548
time_elpased: 2.855
batch start
#iterations: 270
currently lose_sum: 84.22509920597076
time_elpased: 2.879
batch start
#iterations: 271
currently lose_sum: 83.74921607971191
time_elpased: 2.871
batch start
#iterations: 272
currently lose_sum: 83.83093130588531
time_elpased: 2.873
batch start
#iterations: 273
currently lose_sum: 83.64055073261261
time_elpased: 2.861
batch start
#iterations: 274
currently lose_sum: 83.4303258061409
time_elpased: 2.904
batch start
#iterations: 275
currently lose_sum: 84.01828241348267
time_elpased: 2.907
batch start
#iterations: 276
currently lose_sum: 84.66305243968964
time_elpased: 2.844
batch start
#iterations: 277
currently lose_sum: 83.8791531920433
time_elpased: 2.883
batch start
#iterations: 278
currently lose_sum: 83.28853207826614
time_elpased: 2.889
batch start
#iterations: 279
currently lose_sum: 83.34143203496933
time_elpased: 2.913
start validation test
0.684845360825
0.706239267315
0.634866728414
0.668653804466
0.684933106023
62.671
batch start
#iterations: 280
currently lose_sum: 84.28149771690369
time_elpased: 2.936
batch start
#iterations: 281
currently lose_sum: 83.83137607574463
time_elpased: 2.883
batch start
#iterations: 282
currently lose_sum: 83.19605791568756
time_elpased: 2.883
batch start
#iterations: 283
currently lose_sum: 85.15027868747711
time_elpased: 2.896
batch start
#iterations: 284
currently lose_sum: 82.89823517203331
time_elpased: 2.959
batch start
#iterations: 285
currently lose_sum: 82.73232099413872
time_elpased: 2.907
batch start
#iterations: 286
currently lose_sum: 83.36323422193527
time_elpased: 2.879
batch start
#iterations: 287
currently lose_sum: 82.12964618206024
time_elpased: 2.916
batch start
#iterations: 288
currently lose_sum: 83.00766918063164
time_elpased: 2.932
batch start
#iterations: 289
currently lose_sum: 82.69410699605942
time_elpased: 2.844
batch start
#iterations: 290
currently lose_sum: 84.38229900598526
time_elpased: 2.84
batch start
#iterations: 291
currently lose_sum: 82.81903928518295
time_elpased: 2.87
batch start
#iterations: 292
currently lose_sum: 82.7861438691616
time_elpased: 2.888
batch start
#iterations: 293
currently lose_sum: 83.13006818294525
time_elpased: 2.9
batch start
#iterations: 294
currently lose_sum: 83.07624542713165
time_elpased: 2.899
batch start
#iterations: 295
currently lose_sum: 83.61222338676453
time_elpased: 2.85
batch start
#iterations: 296
currently lose_sum: 82.42219713330269
time_elpased: 2.858
batch start
#iterations: 297
currently lose_sum: 82.96574604511261
time_elpased: 2.874
batch start
#iterations: 298
currently lose_sum: 82.51610887050629
time_elpased: 2.779
batch start
#iterations: 299
currently lose_sum: 82.87777623534203
time_elpased: 2.874
start validation test
0.695
0.684179914696
0.726355871154
0.704637348375
0.694944949932
57.918
batch start
#iterations: 300
currently lose_sum: 81.16617220640182
time_elpased: 2.829
batch start
#iterations: 301
currently lose_sum: 81.8994433581829
time_elpased: 2.822
batch start
#iterations: 302
currently lose_sum: 82.83558225631714
time_elpased: 2.84
batch start
#iterations: 303
currently lose_sum: 82.61307454109192
time_elpased: 2.85
batch start
#iterations: 304
currently lose_sum: 81.085996478796
time_elpased: 2.841
batch start
#iterations: 305
currently lose_sum: 82.6607193350792
time_elpased: 2.882
batch start
#iterations: 306
currently lose_sum: 80.40379652380943
time_elpased: 2.945
batch start
#iterations: 307
currently lose_sum: 81.66697558760643
time_elpased: 2.917
batch start
#iterations: 308
currently lose_sum: 82.6007134616375
time_elpased: 2.889
batch start
#iterations: 309
currently lose_sum: 82.21566644310951
time_elpased: 2.912
batch start
#iterations: 310
currently lose_sum: 81.19130417704582
time_elpased: 2.888
batch start
#iterations: 311
currently lose_sum: 81.53218173980713
time_elpased: 2.854
batch start
#iterations: 312
currently lose_sum: 81.28730782866478
time_elpased: 2.865
batch start
#iterations: 313
currently lose_sum: 81.99522617459297
time_elpased: 2.85
batch start
#iterations: 314
currently lose_sum: 82.01366031169891
time_elpased: 2.854
batch start
#iterations: 315
currently lose_sum: 80.96031266450882
time_elpased: 2.867
batch start
#iterations: 316
currently lose_sum: 80.60510623455048
time_elpased: 2.866
batch start
#iterations: 317
currently lose_sum: 80.57733869552612
time_elpased: 2.946
batch start
#iterations: 318
currently lose_sum: 80.4642716050148
time_elpased: 2.966
batch start
#iterations: 319
currently lose_sum: 81.49821400642395
time_elpased: 2.909
start validation test
0.667731958763
0.678139636205
0.640732736441
0.658905704307
0.667779360062
59.314
batch start
#iterations: 320
currently lose_sum: 80.36766251921654
time_elpased: 2.872
batch start
#iterations: 321
currently lose_sum: 81.39675703644753
time_elpased: 2.907
batch start
#iterations: 322
currently lose_sum: 81.00891488790512
time_elpased: 2.897
batch start
#iterations: 323
currently lose_sum: 80.07958379387856
time_elpased: 2.869
batch start
#iterations: 324
currently lose_sum: 81.55227673053741
time_elpased: 2.897
batch start
#iterations: 325
currently lose_sum: 80.38655108213425
time_elpased: 2.93
batch start
#iterations: 326
currently lose_sum: 80.54257079958916
time_elpased: 2.909
batch start
#iterations: 327
currently lose_sum: 79.46882221102715
time_elpased: 2.89
batch start
#iterations: 328
currently lose_sum: 79.74787497520447
time_elpased: 2.87
batch start
#iterations: 329
currently lose_sum: 80.83594438433647
time_elpased: 2.886
batch start
#iterations: 330
currently lose_sum: 80.39698693156242
time_elpased: 2.885
batch start
#iterations: 331
currently lose_sum: 80.57130709290504
time_elpased: 2.872
batch start
#iterations: 332
currently lose_sum: 80.45703092217445
time_elpased: 2.793
batch start
#iterations: 333
currently lose_sum: 79.15829569101334
time_elpased: 2.903
batch start
#iterations: 334
currently lose_sum: 80.46337267756462
time_elpased: 2.866
batch start
#iterations: 335
currently lose_sum: 79.5017241537571
time_elpased: 2.905
batch start
#iterations: 336
currently lose_sum: 80.60509195923805
time_elpased: 2.876
batch start
#iterations: 337
currently lose_sum: 79.9582970738411
time_elpased: 2.881
batch start
#iterations: 338
currently lose_sum: 80.01763001084328
time_elpased: 2.907
batch start
#iterations: 339
currently lose_sum: 79.98702296614647
time_elpased: 2.955
start validation test
0.668041237113
0.677192603006
0.644437583616
0.660409196372
0.668082676968
60.181
batch start
#iterations: 340
currently lose_sum: 78.77868098020554
time_elpased: 2.917
batch start
#iterations: 341
currently lose_sum: 80.1713899075985
time_elpased: 2.897
batch start
#iterations: 342
currently lose_sum: 80.05406439304352
time_elpased: 2.913
batch start
#iterations: 343
currently lose_sum: 80.25244358181953
time_elpased: 2.896
batch start
#iterations: 344
currently lose_sum: 80.6112670302391
time_elpased: 2.846
batch start
#iterations: 345
currently lose_sum: 78.86145496368408
time_elpased: 2.846
batch start
#iterations: 346
currently lose_sum: 78.46340504288673
time_elpased: 2.905
batch start
#iterations: 347
currently lose_sum: 79.01846840977669
time_elpased: 2.848
batch start
#iterations: 348
currently lose_sum: 80.01213973760605
time_elpased: 2.865
batch start
#iterations: 349
currently lose_sum: 79.04988825321198
time_elpased: 2.839
batch start
#iterations: 350
currently lose_sum: 78.15826600790024
time_elpased: 2.848
batch start
#iterations: 351
currently lose_sum: 79.1253177523613
time_elpased: 2.85
batch start
#iterations: 352
currently lose_sum: 78.22058254480362
time_elpased: 2.908
batch start
#iterations: 353
currently lose_sum: 78.39825096726418
time_elpased: 2.899
batch start
#iterations: 354
currently lose_sum: 78.9140895307064
time_elpased: 2.907
batch start
#iterations: 355
currently lose_sum: 77.71405538916588
time_elpased: 2.891
batch start
#iterations: 356
currently lose_sum: 78.17312380671501
time_elpased: 2.826
batch start
#iterations: 357
currently lose_sum: 78.22482311725616
time_elpased: 2.923
batch start
#iterations: 358
currently lose_sum: 78.143025547266
time_elpased: 2.899
batch start
#iterations: 359
currently lose_sum: 79.88847497105598
time_elpased: 2.883
start validation test
0.636237113402
0.684261568301
0.508284449933
0.583289046354
0.636461754038
64.731
batch start
#iterations: 360
currently lose_sum: 77.98941695690155
time_elpased: 2.91
batch start
#iterations: 361
currently lose_sum: 77.41602057218552
time_elpased: 2.834
batch start
#iterations: 362
currently lose_sum: 78.76185658574104
time_elpased: 2.772
batch start
#iterations: 363
currently lose_sum: 78.24313792586327
time_elpased: 2.792
batch start
#iterations: 364
currently lose_sum: 79.15037360787392
time_elpased: 2.904
batch start
#iterations: 365
currently lose_sum: 78.64494788646698
time_elpased: 2.974
batch start
#iterations: 366
currently lose_sum: 76.78919652104378
time_elpased: 2.927
batch start
#iterations: 367
currently lose_sum: 76.35531002283096
time_elpased: 2.924
batch start
#iterations: 368
currently lose_sum: 77.54681822657585
time_elpased: 2.921
batch start
#iterations: 369
currently lose_sum: 77.76202526688576
time_elpased: 2.906
batch start
#iterations: 370
currently lose_sum: 77.79097703099251
time_elpased: 2.841
batch start
#iterations: 371
currently lose_sum: 78.02127313613892
time_elpased: 2.789
batch start
#iterations: 372
currently lose_sum: 77.2451533973217
time_elpased: 2.738
batch start
#iterations: 373
currently lose_sum: 76.75249701738358
time_elpased: 2.863
batch start
#iterations: 374
currently lose_sum: 76.80377721786499
time_elpased: 2.947
batch start
#iterations: 375
currently lose_sum: 77.38737013936043
time_elpased: 2.881
batch start
#iterations: 376
currently lose_sum: 76.7119437456131
time_elpased: 2.853
batch start
#iterations: 377
currently lose_sum: 75.68289440870285
time_elpased: 2.897
batch start
#iterations: 378
currently lose_sum: 77.11925733089447
time_elpased: 2.897
batch start
#iterations: 379
currently lose_sum: 75.99821919202805
time_elpased: 2.856
start validation test
0.685515463918
0.668940384975
0.736750025728
0.701209657672
0.685425513742
59.781
batch start
#iterations: 380
currently lose_sum: 76.38816326856613
time_elpased: 2.786
batch start
#iterations: 381
currently lose_sum: 77.44754561781883
time_elpased: 2.807
batch start
#iterations: 382
currently lose_sum: 75.64980030059814
time_elpased: 2.892
batch start
#iterations: 383
currently lose_sum: 76.50731801986694
time_elpased: 2.924
batch start
#iterations: 384
currently lose_sum: 75.88263973593712
time_elpased: 2.838
batch start
#iterations: 385
currently lose_sum: 76.6463543176651
time_elpased: 2.842
batch start
#iterations: 386
currently lose_sum: 75.4426628947258
time_elpased: 2.857
batch start
#iterations: 387
currently lose_sum: 74.46220996975899
time_elpased: 2.885
batch start
#iterations: 388
currently lose_sum: 76.47596627473831
time_elpased: 2.919
batch start
#iterations: 389
currently lose_sum: 74.34540936350822
time_elpased: 2.881
batch start
#iterations: 390
currently lose_sum: 75.34517937898636
time_elpased: 2.873
batch start
#iterations: 391
currently lose_sum: 75.71757364273071
time_elpased: 2.858
batch start
#iterations: 392
currently lose_sum: 74.19926753640175
time_elpased: 2.942
batch start
#iterations: 393
currently lose_sum: 73.92680042982101
time_elpased: 2.912
batch start
#iterations: 394
currently lose_sum: 74.4587331712246
time_elpased: 2.845
batch start
#iterations: 395
currently lose_sum: 74.9183654487133
time_elpased: 2.885
batch start
#iterations: 396
currently lose_sum: 73.89998546242714
time_elpased: 2.854
batch start
#iterations: 397
currently lose_sum: 74.90860921144485
time_elpased: 2.892
batch start
#iterations: 398
currently lose_sum: 73.51969823241234
time_elpased: 2.909
batch start
#iterations: 399
currently lose_sum: 74.25838741660118
time_elpased: 2.879
start validation test
0.658350515464
0.673714992689
0.61644540496
0.643809114359
0.658424086349
63.545
acc: 0.691
pre: 0.693
rec: 0.688
F1: 0.691
auc: 0.691
