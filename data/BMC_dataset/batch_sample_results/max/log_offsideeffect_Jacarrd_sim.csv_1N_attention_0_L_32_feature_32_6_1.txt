start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.2117805480957
time_elpased: 2.068
batch start
#iterations: 1
currently lose_sum: 99.44715881347656
time_elpased: 1.97
batch start
#iterations: 2
currently lose_sum: 98.89485317468643
time_elpased: 1.993
batch start
#iterations: 3
currently lose_sum: 98.63484907150269
time_elpased: 1.949
batch start
#iterations: 4
currently lose_sum: 98.28387296199799
time_elpased: 1.95
batch start
#iterations: 5
currently lose_sum: 97.88969933986664
time_elpased: 1.952
batch start
#iterations: 6
currently lose_sum: 97.92744201421738
time_elpased: 1.973
batch start
#iterations: 7
currently lose_sum: 97.09976994991302
time_elpased: 1.927
batch start
#iterations: 8
currently lose_sum: 97.36585599184036
time_elpased: 1.931
batch start
#iterations: 9
currently lose_sum: 97.31284987926483
time_elpased: 1.941
batch start
#iterations: 10
currently lose_sum: 97.0985369682312
time_elpased: 1.933
batch start
#iterations: 11
currently lose_sum: 96.83100545406342
time_elpased: 1.981
batch start
#iterations: 12
currently lose_sum: 96.13808375597
time_elpased: 1.947
batch start
#iterations: 13
currently lose_sum: 96.79287719726562
time_elpased: 1.931
batch start
#iterations: 14
currently lose_sum: 97.26700723171234
time_elpased: 1.949
batch start
#iterations: 15
currently lose_sum: 95.781165599823
time_elpased: 1.95
batch start
#iterations: 16
currently lose_sum: 96.53805416822433
time_elpased: 1.944
batch start
#iterations: 17
currently lose_sum: 96.10225194692612
time_elpased: 1.948
batch start
#iterations: 18
currently lose_sum: 96.23896902799606
time_elpased: 1.968
batch start
#iterations: 19
currently lose_sum: 95.84963667392731
time_elpased: 1.976
start validation test
0.640824742268
0.673701503854
0.548626119173
0.604764605786
0.640986611172
61.739
batch start
#iterations: 20
currently lose_sum: 95.60478985309601
time_elpased: 2.017
batch start
#iterations: 21
currently lose_sum: 95.49644541740417
time_elpased: 1.954
batch start
#iterations: 22
currently lose_sum: 96.03048419952393
time_elpased: 1.978
batch start
#iterations: 23
currently lose_sum: 95.77288556098938
time_elpased: 1.974
batch start
#iterations: 24
currently lose_sum: 95.52093553543091
time_elpased: 1.944
batch start
#iterations: 25
currently lose_sum: 95.21839499473572
time_elpased: 1.988
batch start
#iterations: 26
currently lose_sum: 95.27485609054565
time_elpased: 2.111
batch start
#iterations: 27
currently lose_sum: 95.80513340234756
time_elpased: 2.061
batch start
#iterations: 28
currently lose_sum: 95.21890777349472
time_elpased: 2.075
batch start
#iterations: 29
currently lose_sum: 94.61261308193207
time_elpased: 2.073
batch start
#iterations: 30
currently lose_sum: 95.89869576692581
time_elpased: 2.096
batch start
#iterations: 31
currently lose_sum: 94.79220569133759
time_elpased: 2.088
batch start
#iterations: 32
currently lose_sum: 93.98022228479385
time_elpased: 2.074
batch start
#iterations: 33
currently lose_sum: 94.84063124656677
time_elpased: 2.064
batch start
#iterations: 34
currently lose_sum: 94.90078270435333
time_elpased: 2.088
batch start
#iterations: 35
currently lose_sum: 94.71247416734695
time_elpased: 2.019
batch start
#iterations: 36
currently lose_sum: 94.37941926717758
time_elpased: 2.042
batch start
#iterations: 37
currently lose_sum: 94.34447282552719
time_elpased: 1.994
batch start
#iterations: 38
currently lose_sum: 94.14860725402832
time_elpased: 2.016
batch start
#iterations: 39
currently lose_sum: 94.5364550948143
time_elpased: 2.027
start validation test
0.677577319588
0.648685792819
0.777194607389
0.707149211105
0.677402426074
59.163
batch start
#iterations: 40
currently lose_sum: 94.04879158735275
time_elpased: 2.042
batch start
#iterations: 41
currently lose_sum: 93.67805343866348
time_elpased: 2.046
batch start
#iterations: 42
currently lose_sum: 94.09422618150711
time_elpased: 2.031
batch start
#iterations: 43
currently lose_sum: 93.94380503892899
time_elpased: 2.056
batch start
#iterations: 44
currently lose_sum: 94.00695753097534
time_elpased: 2.027
batch start
#iterations: 45
currently lose_sum: 95.92430412769318
time_elpased: 2.019
batch start
#iterations: 46
currently lose_sum: 93.72244846820831
time_elpased: 2.063
batch start
#iterations: 47
currently lose_sum: 94.29755336046219
time_elpased: 2.061
batch start
#iterations: 48
currently lose_sum: 94.21689158678055
time_elpased: 1.991
batch start
#iterations: 49
currently lose_sum: 93.91933250427246
time_elpased: 2.04
batch start
#iterations: 50
currently lose_sum: 94.93973660469055
time_elpased: 2.039
batch start
#iterations: 51
currently lose_sum: 93.41238516569138
time_elpased: 2.03
batch start
#iterations: 52
currently lose_sum: 93.90767860412598
time_elpased: 2.022
batch start
#iterations: 53
currently lose_sum: 93.6464204788208
time_elpased: 2.014
batch start
#iterations: 54
currently lose_sum: 94.28422176837921
time_elpased: 2.018
batch start
#iterations: 55
currently lose_sum: 93.13401001691818
time_elpased: 2.045
batch start
#iterations: 56
currently lose_sum: 93.47675544023514
time_elpased: 2.065
batch start
#iterations: 57
currently lose_sum: 93.66510778665543
time_elpased: 2.003
batch start
#iterations: 58
currently lose_sum: 93.20962184667587
time_elpased: 2.01
batch start
#iterations: 59
currently lose_sum: 93.09496873617172
time_elpased: 2.067
start validation test
0.686082474227
0.658508871602
0.775342183802
0.712165611116
0.685925765039
58.477
batch start
#iterations: 60
currently lose_sum: 92.65396362543106
time_elpased: 2.042
batch start
#iterations: 61
currently lose_sum: 93.28747260570526
time_elpased: 2.012
batch start
#iterations: 62
currently lose_sum: 92.93104094266891
time_elpased: 2.013
batch start
#iterations: 63
currently lose_sum: 93.40485072135925
time_elpased: 2.036
batch start
#iterations: 64
currently lose_sum: 93.37743920087814
time_elpased: 2.0
batch start
#iterations: 65
currently lose_sum: 92.92890709638596
time_elpased: 2.045
batch start
#iterations: 66
currently lose_sum: 92.81756365299225
time_elpased: 2.041
batch start
#iterations: 67
currently lose_sum: 96.3441589474678
time_elpased: 2.033
batch start
#iterations: 68
currently lose_sum: 94.34484279155731
time_elpased: 2.044
batch start
#iterations: 69
currently lose_sum: 93.19400119781494
time_elpased: 2.022
batch start
#iterations: 70
currently lose_sum: 93.37062078714371
time_elpased: 2.021
batch start
#iterations: 71
currently lose_sum: 93.19574004411697
time_elpased: 2.03
batch start
#iterations: 72
currently lose_sum: 93.55686801671982
time_elpased: 2.03
batch start
#iterations: 73
currently lose_sum: 92.5669173002243
time_elpased: 2.023
batch start
#iterations: 74
currently lose_sum: 93.09033340215683
time_elpased: 2.024
batch start
#iterations: 75
currently lose_sum: 92.8712232708931
time_elpased: 2.018
batch start
#iterations: 76
currently lose_sum: 91.94145560264587
time_elpased: 2.001
batch start
#iterations: 77
currently lose_sum: 92.70990127325058
time_elpased: 2.035
batch start
#iterations: 78
currently lose_sum: 93.01805126667023
time_elpased: 2.017
batch start
#iterations: 79
currently lose_sum: 92.33283305168152
time_elpased: 1.987
start validation test
0.678762886598
0.631499509471
0.861171143357
0.728665970045
0.678442640769
58.449
batch start
#iterations: 80
currently lose_sum: 92.59926563501358
time_elpased: 2.032
batch start
#iterations: 81
currently lose_sum: 92.00233989953995
time_elpased: 2.058
batch start
#iterations: 82
currently lose_sum: 92.8669884800911
time_elpased: 2.036
batch start
#iterations: 83
currently lose_sum: 92.62378990650177
time_elpased: 2.032
batch start
#iterations: 84
currently lose_sum: 92.51983493566513
time_elpased: 2.065
batch start
#iterations: 85
currently lose_sum: 92.04698306322098
time_elpased: 2.027
batch start
#iterations: 86
currently lose_sum: 93.73006409406662
time_elpased: 2.009
batch start
#iterations: 87
currently lose_sum: 91.49161422252655
time_elpased: 2.011
batch start
#iterations: 88
currently lose_sum: 92.2614758014679
time_elpased: 2.055
batch start
#iterations: 89
currently lose_sum: 92.65038561820984
time_elpased: 2.049
batch start
#iterations: 90
currently lose_sum: 91.79356104135513
time_elpased: 2.023
batch start
#iterations: 91
currently lose_sum: 92.47075521945953
time_elpased: 1.997
batch start
#iterations: 92
currently lose_sum: 91.52190762758255
time_elpased: 2.007
batch start
#iterations: 93
currently lose_sum: 92.90591961145401
time_elpased: 2.12
batch start
#iterations: 94
currently lose_sum: 91.84105372428894
time_elpased: 2.01
batch start
#iterations: 95
currently lose_sum: 92.09441882371902
time_elpased: 2.041
batch start
#iterations: 96
currently lose_sum: 91.7039008140564
time_elpased: 2.036
batch start
#iterations: 97
currently lose_sum: 91.22815519571304
time_elpased: 2.007
batch start
#iterations: 98
currently lose_sum: 91.80378413200378
time_elpased: 2.031
batch start
#iterations: 99
currently lose_sum: 91.83602184057236
time_elpased: 2.004
start validation test
0.680721649485
0.640436897074
0.826695482145
0.721743036837
0.680465369906
58.630
batch start
#iterations: 100
currently lose_sum: 90.97829473018646
time_elpased: 2.018
batch start
#iterations: 101
currently lose_sum: 90.9750229716301
time_elpased: 2.043
batch start
#iterations: 102
currently lose_sum: 91.03735810518265
time_elpased: 2.004
batch start
#iterations: 103
currently lose_sum: 92.073526263237
time_elpased: 2.038
batch start
#iterations: 104
currently lose_sum: 91.92703211307526
time_elpased: 1.983
batch start
#iterations: 105
currently lose_sum: 91.85414814949036
time_elpased: 2.007
batch start
#iterations: 106
currently lose_sum: 92.0310789346695
time_elpased: 1.986
batch start
#iterations: 107
currently lose_sum: 91.4588034749031
time_elpased: 2.022
batch start
#iterations: 108
currently lose_sum: 91.71579813957214
time_elpased: 1.984
batch start
#iterations: 109
currently lose_sum: 91.08183342218399
time_elpased: 2.034
batch start
#iterations: 110
currently lose_sum: 91.39932006597519
time_elpased: 2.02
batch start
#iterations: 111
currently lose_sum: 91.18907630443573
time_elpased: 2.033
batch start
#iterations: 112
currently lose_sum: 91.22062557935715
time_elpased: 2.02
batch start
#iterations: 113
currently lose_sum: 91.36137264966965
time_elpased: 2.022
batch start
#iterations: 114
currently lose_sum: 90.71208393573761
time_elpased: 2.015
batch start
#iterations: 115
currently lose_sum: 90.37974160909653
time_elpased: 2.016
batch start
#iterations: 116
currently lose_sum: 91.43531036376953
time_elpased: 1.964
batch start
#iterations: 117
currently lose_sum: 90.69054847955704
time_elpased: 1.992
batch start
#iterations: 118
currently lose_sum: 91.17253023386002
time_elpased: 1.998
batch start
#iterations: 119
currently lose_sum: 91.96599435806274
time_elpased: 2.026
start validation test
0.685154639175
0.656491197641
0.779047030977
0.712537650602
0.6849897966
57.928
batch start
#iterations: 120
currently lose_sum: 90.90779536962509
time_elpased: 2.022
batch start
#iterations: 121
currently lose_sum: 91.21861225366592
time_elpased: 2.033
batch start
#iterations: 122
currently lose_sum: 90.8939882516861
time_elpased: 2.039
batch start
#iterations: 123
currently lose_sum: 90.53489381074905
time_elpased: 2.019
batch start
#iterations: 124
currently lose_sum: 90.47272562980652
time_elpased: 2.056
batch start
#iterations: 125
currently lose_sum: 91.12501519918442
time_elpased: 2.02
batch start
#iterations: 126
currently lose_sum: 91.00857925415039
time_elpased: 2.006
batch start
#iterations: 127
currently lose_sum: 91.38571387529373
time_elpased: 2.028
batch start
#iterations: 128
currently lose_sum: 90.77599734067917
time_elpased: 2.019
batch start
#iterations: 129
currently lose_sum: 90.29937022924423
time_elpased: 2.013
batch start
#iterations: 130
currently lose_sum: 90.45915305614471
time_elpased: 2.008
batch start
#iterations: 131
currently lose_sum: 90.61203461885452
time_elpased: 2.023
batch start
#iterations: 132
currently lose_sum: 91.14427727460861
time_elpased: 2.038
batch start
#iterations: 133
currently lose_sum: 89.90801960229874
time_elpased: 2.059
batch start
#iterations: 134
currently lose_sum: 90.01192390918732
time_elpased: 2.041
batch start
#iterations: 135
currently lose_sum: 90.97635489702225
time_elpased: 2.084
batch start
#iterations: 136
currently lose_sum: 89.76856607198715
time_elpased: 2.067
batch start
#iterations: 137
currently lose_sum: 90.71510362625122
time_elpased: 2.032
batch start
#iterations: 138
currently lose_sum: 90.20251131057739
time_elpased: 2.024
batch start
#iterations: 139
currently lose_sum: 90.06177389621735
time_elpased: 2.025
start validation test
0.691082474227
0.68305151396
0.715035504785
0.698677661019
0.691040420987
57.392
batch start
#iterations: 140
currently lose_sum: 90.37046378850937
time_elpased: 2.012
batch start
#iterations: 141
currently lose_sum: 90.65752041339874
time_elpased: 2.042
batch start
#iterations: 142
currently lose_sum: 90.05886727571487
time_elpased: 2.033
batch start
#iterations: 143
currently lose_sum: 91.79298150539398
time_elpased: 2.028
batch start
#iterations: 144
currently lose_sum: 90.37845808267593
time_elpased: 2.045
batch start
#iterations: 145
currently lose_sum: 90.75053828954697
time_elpased: 2.005
batch start
#iterations: 146
currently lose_sum: 90.4812280535698
time_elpased: 2.035
batch start
#iterations: 147
currently lose_sum: 90.02161830663681
time_elpased: 2.022
batch start
#iterations: 148
currently lose_sum: 90.28298687934875
time_elpased: 1.994
batch start
#iterations: 149
currently lose_sum: 90.03118455410004
time_elpased: 2.038
batch start
#iterations: 150
currently lose_sum: 89.7845333814621
time_elpased: 2.038
batch start
#iterations: 151
currently lose_sum: 89.52385115623474
time_elpased: 2.018
batch start
#iterations: 152
currently lose_sum: 91.25214380025864
time_elpased: 2.004
batch start
#iterations: 153
currently lose_sum: 90.52636283636093
time_elpased: 2.067
batch start
#iterations: 154
currently lose_sum: 90.02438950538635
time_elpased: 2.047
batch start
#iterations: 155
currently lose_sum: 89.61877644062042
time_elpased: 2.047
batch start
#iterations: 156
currently lose_sum: 89.78923588991165
time_elpased: 2.036
batch start
#iterations: 157
currently lose_sum: 90.42473006248474
time_elpased: 2.01
batch start
#iterations: 158
currently lose_sum: 89.92659819126129
time_elpased: 2.014
batch start
#iterations: 159
currently lose_sum: 89.805908203125
time_elpased: 2.006
start validation test
0.663711340206
0.708938620599
0.557476587424
0.624150247724
0.6638978517
62.822
batch start
#iterations: 160
currently lose_sum: 90.50782942771912
time_elpased: 2.018
batch start
#iterations: 161
currently lose_sum: 89.3892560005188
time_elpased: 2.041
batch start
#iterations: 162
currently lose_sum: 89.75960385799408
time_elpased: 2.024
batch start
#iterations: 163
currently lose_sum: 89.07930064201355
time_elpased: 2.03
batch start
#iterations: 164
currently lose_sum: 89.8049864768982
time_elpased: 2.008
batch start
#iterations: 165
currently lose_sum: 90.09022569656372
time_elpased: 2.039
batch start
#iterations: 166
currently lose_sum: 89.53201121091843
time_elpased: 1.993
batch start
#iterations: 167
currently lose_sum: 90.76927047967911
time_elpased: 2.034
batch start
#iterations: 168
currently lose_sum: 89.26697438955307
time_elpased: 2.014
batch start
#iterations: 169
currently lose_sum: 89.61241894960403
time_elpased: 2.014
batch start
#iterations: 170
currently lose_sum: 89.21050441265106
time_elpased: 2.002
batch start
#iterations: 171
currently lose_sum: 90.12008422613144
time_elpased: 2.028
batch start
#iterations: 172
currently lose_sum: 89.72002106904984
time_elpased: 2.011
batch start
#iterations: 173
currently lose_sum: 89.03525924682617
time_elpased: 2.044
batch start
#iterations: 174
currently lose_sum: 90.05073410272598
time_elpased: 1.997
batch start
#iterations: 175
currently lose_sum: 89.91385000944138
time_elpased: 2.038
batch start
#iterations: 176
currently lose_sum: 89.49128526449203
time_elpased: 1.998
batch start
#iterations: 177
currently lose_sum: 89.5117027759552
time_elpased: 2.006
batch start
#iterations: 178
currently lose_sum: 89.23091953992844
time_elpased: 2.002
batch start
#iterations: 179
currently lose_sum: 90.22507351636887
time_elpased: 2.039
start validation test
0.692319587629
0.688417454253
0.704641350211
0.696434928546
0.692297954874
57.144
batch start
#iterations: 180
currently lose_sum: 90.00459986925125
time_elpased: 2.043
batch start
#iterations: 181
currently lose_sum: 89.48097217082977
time_elpased: 2.002
batch start
#iterations: 182
currently lose_sum: 89.41719287633896
time_elpased: 2.021
batch start
#iterations: 183
currently lose_sum: 88.14731115102768
time_elpased: 2.036
batch start
#iterations: 184
currently lose_sum: 89.18310016393661
time_elpased: 2.044
batch start
#iterations: 185
currently lose_sum: 89.14049530029297
time_elpased: 2.012
batch start
#iterations: 186
currently lose_sum: 88.5020877122879
time_elpased: 2.022
batch start
#iterations: 187
currently lose_sum: 89.68278306722641
time_elpased: 2.025
batch start
#iterations: 188
currently lose_sum: 89.05785304307938
time_elpased: 2.028
batch start
#iterations: 189
currently lose_sum: 89.14001256227493
time_elpased: 2.016
batch start
#iterations: 190
currently lose_sum: 89.59804594516754
time_elpased: 2.025
batch start
#iterations: 191
currently lose_sum: 88.57655000686646
time_elpased: 2.02
batch start
#iterations: 192
currently lose_sum: 88.71778923273087
time_elpased: 1.996
batch start
#iterations: 193
currently lose_sum: 89.1624207496643
time_elpased: 2.024
batch start
#iterations: 194
currently lose_sum: 89.5389135479927
time_elpased: 1.993
batch start
#iterations: 195
currently lose_sum: 88.78283113241196
time_elpased: 2.019
batch start
#iterations: 196
currently lose_sum: 89.29842483997345
time_elpased: 2.059
batch start
#iterations: 197
currently lose_sum: 89.53159379959106
time_elpased: 2.019
batch start
#iterations: 198
currently lose_sum: 88.62352442741394
time_elpased: 2.017
batch start
#iterations: 199
currently lose_sum: 89.18682664632797
time_elpased: 2.019
start validation test
0.685721649485
0.703966644129
0.642893897293
0.672045613469
0.685796840209
58.534
batch start
#iterations: 200
currently lose_sum: 88.65176558494568
time_elpased: 2.02
batch start
#iterations: 201
currently lose_sum: 89.15000200271606
time_elpased: 1.999
batch start
#iterations: 202
currently lose_sum: 88.9498615860939
time_elpased: 1.994
batch start
#iterations: 203
currently lose_sum: 88.8160052895546
time_elpased: 1.991
batch start
#iterations: 204
currently lose_sum: 88.9997929930687
time_elpased: 1.97
batch start
#iterations: 205
currently lose_sum: 88.6698539853096
time_elpased: 2.043
batch start
#iterations: 206
currently lose_sum: 88.83457493782043
time_elpased: 2.007
batch start
#iterations: 207
currently lose_sum: 88.71220266819
time_elpased: 2.046
batch start
#iterations: 208
currently lose_sum: 88.158034324646
time_elpased: 2.04
batch start
#iterations: 209
currently lose_sum: 88.32513952255249
time_elpased: 2.007
batch start
#iterations: 210
currently lose_sum: 88.99940395355225
time_elpased: 2.007
batch start
#iterations: 211
currently lose_sum: 88.58688032627106
time_elpased: 2.002
batch start
#iterations: 212
currently lose_sum: 89.16495698690414
time_elpased: 2.019
batch start
#iterations: 213
currently lose_sum: 88.1030443906784
time_elpased: 2.009
batch start
#iterations: 214
currently lose_sum: 88.82557368278503
time_elpased: 1.999
batch start
#iterations: 215
currently lose_sum: 88.5092316865921
time_elpased: 2.018
batch start
#iterations: 216
currently lose_sum: 88.511594414711
time_elpased: 2.012
batch start
#iterations: 217
currently lose_sum: 89.17756766080856
time_elpased: 2.031
batch start
#iterations: 218
currently lose_sum: 89.55557006597519
time_elpased: 2.016
batch start
#iterations: 219
currently lose_sum: 88.51554131507874
time_elpased: 2.026
start validation test
0.646030927835
0.617380560132
0.771328599362
0.685821475957
0.645810948447
59.613
batch start
#iterations: 220
currently lose_sum: 88.76300716400146
time_elpased: 1.99
batch start
#iterations: 221
currently lose_sum: 88.47783386707306
time_elpased: 2.033
batch start
#iterations: 222
currently lose_sum: 88.28454387187958
time_elpased: 1.996
batch start
#iterations: 223
currently lose_sum: 87.7396999001503
time_elpased: 2.04
batch start
#iterations: 224
currently lose_sum: 88.72123736143112
time_elpased: 2.005
batch start
#iterations: 225
currently lose_sum: 88.22206801176071
time_elpased: 2.017
batch start
#iterations: 226
currently lose_sum: 88.5564756989479
time_elpased: 2.101
batch start
#iterations: 227
currently lose_sum: 88.89479792118073
time_elpased: 2.017
batch start
#iterations: 228
currently lose_sum: 89.25264620780945
time_elpased: 2.007
batch start
#iterations: 229
currently lose_sum: 87.8900978565216
time_elpased: 2.033
batch start
#iterations: 230
currently lose_sum: 88.35105466842651
time_elpased: 2.007
batch start
#iterations: 231
currently lose_sum: 88.83888161182404
time_elpased: 2.027
batch start
#iterations: 232
currently lose_sum: 88.2256190776825
time_elpased: 1.993
batch start
#iterations: 233
currently lose_sum: 88.55829685926437
time_elpased: 2.015
batch start
#iterations: 234
currently lose_sum: 88.50998657941818
time_elpased: 2.001
batch start
#iterations: 235
currently lose_sum: 87.70896202325821
time_elpased: 2.047
batch start
#iterations: 236
currently lose_sum: 88.03898304700851
time_elpased: 2.044
batch start
#iterations: 237
currently lose_sum: 89.76527446508408
time_elpased: 2.052
batch start
#iterations: 238
currently lose_sum: 88.23545700311661
time_elpased: 2.027
batch start
#iterations: 239
currently lose_sum: 89.49219620227814
time_elpased: 2.028
start validation test
0.605979381443
0.593673746046
0.676031697026
0.632181695698
0.605856393798
61.082
batch start
#iterations: 240
currently lose_sum: 89.23051422834396
time_elpased: 1.965
batch start
#iterations: 241
currently lose_sum: 88.57948052883148
time_elpased: 2.039
batch start
#iterations: 242
currently lose_sum: 87.88513398170471
time_elpased: 2.019
batch start
#iterations: 243
currently lose_sum: 88.71289575099945
time_elpased: 2.01
batch start
#iterations: 244
currently lose_sum: 88.10116356611252
time_elpased: 2.047
batch start
#iterations: 245
currently lose_sum: 88.46900296211243
time_elpased: 1.993
batch start
#iterations: 246
currently lose_sum: 87.84222167730331
time_elpased: 2.027
batch start
#iterations: 247
currently lose_sum: 88.81707048416138
time_elpased: 1.993
batch start
#iterations: 248
currently lose_sum: 88.32994872331619
time_elpased: 1.991
batch start
#iterations: 249
currently lose_sum: 87.60764563083649
time_elpased: 2.012
batch start
#iterations: 250
currently lose_sum: 88.15712881088257
time_elpased: 1.978
batch start
#iterations: 251
currently lose_sum: 88.31079071760178
time_elpased: 1.999
batch start
#iterations: 252
currently lose_sum: 88.97392934560776
time_elpased: 2.005
batch start
#iterations: 253
currently lose_sum: 88.20687568187714
time_elpased: 1.999
batch start
#iterations: 254
currently lose_sum: 88.37561959028244
time_elpased: 2.134
batch start
#iterations: 255
currently lose_sum: 88.1636957526207
time_elpased: 2.007
batch start
#iterations: 256
currently lose_sum: 88.3346655368805
time_elpased: 1.998
batch start
#iterations: 257
currently lose_sum: 88.47450542449951
time_elpased: 2.005
batch start
#iterations: 258
currently lose_sum: 87.72116965055466
time_elpased: 2.0
batch start
#iterations: 259
currently lose_sum: 87.15966469049454
time_elpased: 2.028
start validation test
0.67793814433
0.648973632225
0.777606257075
0.707490636704
0.677763161585
57.071
batch start
#iterations: 260
currently lose_sum: 88.25573289394379
time_elpased: 2.031
batch start
#iterations: 261
currently lose_sum: 88.4784289598465
time_elpased: 2.012
batch start
#iterations: 262
currently lose_sum: 87.9772197008133
time_elpased: 2.01
batch start
#iterations: 263
currently lose_sum: 87.68971383571625
time_elpased: 2.059
batch start
#iterations: 264
currently lose_sum: 87.65662735700607
time_elpased: 2.004
batch start
#iterations: 265
currently lose_sum: 88.08093506097794
time_elpased: 2.03
batch start
#iterations: 266
currently lose_sum: 87.72337245941162
time_elpased: 2.009
batch start
#iterations: 267
currently lose_sum: 88.06472271680832
time_elpased: 2.024
batch start
#iterations: 268
currently lose_sum: 88.96774005889893
time_elpased: 2.075
batch start
#iterations: 269
currently lose_sum: 87.79275250434875
time_elpased: 2.007
batch start
#iterations: 270
currently lose_sum: 88.0480586886406
time_elpased: 2.032
batch start
#iterations: 271
currently lose_sum: 87.71364343166351
time_elpased: 1.99
batch start
#iterations: 272
currently lose_sum: 88.17694795131683
time_elpased: 2.095
batch start
#iterations: 273
currently lose_sum: 87.99177378416061
time_elpased: 1.996
batch start
#iterations: 274
currently lose_sum: 89.00242125988007
time_elpased: 2.071
batch start
#iterations: 275
currently lose_sum: 88.45676612854004
time_elpased: 1.985
batch start
#iterations: 276
currently lose_sum: 87.38239747285843
time_elpased: 2.01
batch start
#iterations: 277
currently lose_sum: 87.98243790864944
time_elpased: 1.977
batch start
#iterations: 278
currently lose_sum: 87.43286120891571
time_elpased: 2.013
batch start
#iterations: 279
currently lose_sum: 87.26701730489731
time_elpased: 1.967
start validation test
0.682680412371
0.650698264917
0.791190696717
0.714099944269
0.68248990583
57.391
batch start
#iterations: 280
currently lose_sum: 87.17293912172318
time_elpased: 2.018
batch start
#iterations: 281
currently lose_sum: 87.20675963163376
time_elpased: 2.021
batch start
#iterations: 282
currently lose_sum: 87.78663265705109
time_elpased: 2.034
batch start
#iterations: 283
currently lose_sum: 86.58056908845901
time_elpased: 2.01
batch start
#iterations: 284
currently lose_sum: 88.00443857908249
time_elpased: 2.012
batch start
#iterations: 285
currently lose_sum: 88.06130784749985
time_elpased: 2.114
batch start
#iterations: 286
currently lose_sum: 86.77119529247284
time_elpased: 2.018
batch start
#iterations: 287
currently lose_sum: 87.12021088600159
time_elpased: 2.057
batch start
#iterations: 288
currently lose_sum: 88.35637980699539
time_elpased: 2.018
batch start
#iterations: 289
currently lose_sum: 87.04261076450348
time_elpased: 2.009
batch start
#iterations: 290
currently lose_sum: 88.1892558336258
time_elpased: 1.982
batch start
#iterations: 291
currently lose_sum: 87.64654469490051
time_elpased: 2.031
batch start
#iterations: 292
currently lose_sum: 86.8223408460617
time_elpased: 1.985
batch start
#iterations: 293
currently lose_sum: 87.70871180295944
time_elpased: 2.03
batch start
#iterations: 294
currently lose_sum: 87.74151933193207
time_elpased: 2.007
batch start
#iterations: 295
currently lose_sum: 87.0068541765213
time_elpased: 1.995
batch start
#iterations: 296
currently lose_sum: 88.46655136346817
time_elpased: 2.001
batch start
#iterations: 297
currently lose_sum: 87.2729880809784
time_elpased: 1.996
batch start
#iterations: 298
currently lose_sum: 88.00928264856339
time_elpased: 2.015
batch start
#iterations: 299
currently lose_sum: 86.98654699325562
time_elpased: 2.013
start validation test
0.677886597938
0.658791208791
0.740351960482
0.697194359645
0.67777693036
57.379
batch start
#iterations: 300
currently lose_sum: 87.41184687614441
time_elpased: 2.025
batch start
#iterations: 301
currently lose_sum: 86.93275102972984
time_elpased: 2.03
batch start
#iterations: 302
currently lose_sum: 86.62469738721848
time_elpased: 2.005
batch start
#iterations: 303
currently lose_sum: 87.77233082056046
time_elpased: 2.044
batch start
#iterations: 304
currently lose_sum: 87.25286513566971
time_elpased: 2.135
batch start
#iterations: 305
currently lose_sum: 87.0341305732727
time_elpased: 2.004
batch start
#iterations: 306
currently lose_sum: 87.05629175901413
time_elpased: 2.038
batch start
#iterations: 307
currently lose_sum: 87.82077711820602
time_elpased: 2.024
batch start
#iterations: 308
currently lose_sum: 88.37186080217361
time_elpased: 2.06
batch start
#iterations: 309
currently lose_sum: 87.45420986413956
time_elpased: 2.015
batch start
#iterations: 310
currently lose_sum: 88.47134393453598
time_elpased: 2.02
batch start
#iterations: 311
currently lose_sum: 86.2263633608818
time_elpased: 2.009
batch start
#iterations: 312
currently lose_sum: 86.12496560811996
time_elpased: 2.028
batch start
#iterations: 313
currently lose_sum: 87.22749269008636
time_elpased: 2.048
batch start
#iterations: 314
currently lose_sum: 87.8062914609909
time_elpased: 2.021
batch start
#iterations: 315
currently lose_sum: 87.1299512386322
time_elpased: 2.047
batch start
#iterations: 316
currently lose_sum: 87.48357731103897
time_elpased: 2.045
batch start
#iterations: 317
currently lose_sum: 87.23720645904541
time_elpased: 2.014
batch start
#iterations: 318
currently lose_sum: 86.7456447482109
time_elpased: 2.032
batch start
#iterations: 319
currently lose_sum: 86.97785592079163
time_elpased: 2.022
start validation test
0.675309278351
0.676805296917
0.673253061645
0.67502450601
0.675312888356
58.008
batch start
#iterations: 320
currently lose_sum: 87.64666819572449
time_elpased: 2.028
batch start
#iterations: 321
currently lose_sum: 87.6224319934845
time_elpased: 2.011
batch start
#iterations: 322
currently lose_sum: 87.38727843761444
time_elpased: 2.001
batch start
#iterations: 323
currently lose_sum: 86.91081899404526
time_elpased: 2.053
batch start
#iterations: 324
currently lose_sum: 86.86250859498978
time_elpased: 1.992
batch start
#iterations: 325
currently lose_sum: 87.52438443899155
time_elpased: 2.02
batch start
#iterations: 326
currently lose_sum: 87.08039379119873
time_elpased: 2.029
batch start
#iterations: 327
currently lose_sum: 86.88562142848969
time_elpased: 2.005
batch start
#iterations: 328
currently lose_sum: 87.97332382202148
time_elpased: 2.057
batch start
#iterations: 329
currently lose_sum: 87.52110600471497
time_elpased: 1.993
batch start
#iterations: 330
currently lose_sum: 86.6911370754242
time_elpased: 2.017
batch start
#iterations: 331
currently lose_sum: 86.41397249698639
time_elpased: 1.984
batch start
#iterations: 332
currently lose_sum: 86.3485649228096
time_elpased: 1.998
batch start
#iterations: 333
currently lose_sum: 87.70973747968674
time_elpased: 2.111
batch start
#iterations: 334
currently lose_sum: 85.99150604009628
time_elpased: 2.014
batch start
#iterations: 335
currently lose_sum: 87.08425188064575
time_elpased: 2.025
batch start
#iterations: 336
currently lose_sum: 86.43637788295746
time_elpased: 2.016
batch start
#iterations: 337
currently lose_sum: 87.63515430688858
time_elpased: 1.995
batch start
#iterations: 338
currently lose_sum: 87.07976484298706
time_elpased: 2.006
batch start
#iterations: 339
currently lose_sum: 87.08063715696335
time_elpased: 2.051
start validation test
0.679329896907
0.656490599821
0.754656787074
0.702159237803
0.679197648933
57.256
batch start
#iterations: 340
currently lose_sum: 87.39280658960342
time_elpased: 2.113
batch start
#iterations: 341
currently lose_sum: 86.4727623462677
time_elpased: 2.036
batch start
#iterations: 342
currently lose_sum: 86.5471271276474
time_elpased: 2.009
batch start
#iterations: 343
currently lose_sum: 86.2130696773529
time_elpased: 2.069
batch start
#iterations: 344
currently lose_sum: 85.96142846345901
time_elpased: 2.037
batch start
#iterations: 345
currently lose_sum: 85.83373552560806
time_elpased: 2.013
batch start
#iterations: 346
currently lose_sum: 86.2503290772438
time_elpased: 2.053
batch start
#iterations: 347
currently lose_sum: 86.75200682878494
time_elpased: 2.053
batch start
#iterations: 348
currently lose_sum: 86.41441571712494
time_elpased: 2.026
batch start
#iterations: 349
currently lose_sum: 87.00923639535904
time_elpased: 2.029
batch start
#iterations: 350
currently lose_sum: 86.28245842456818
time_elpased: 2.013
batch start
#iterations: 351
currently lose_sum: 86.30718356370926
time_elpased: 2.038
batch start
#iterations: 352
currently lose_sum: 86.70404821634293
time_elpased: 2.002
batch start
#iterations: 353
currently lose_sum: 86.43038415908813
time_elpased: 2.108
batch start
#iterations: 354
currently lose_sum: 85.93274265527725
time_elpased: 2.016
batch start
#iterations: 355
currently lose_sum: 86.91697490215302
time_elpased: 2.043
batch start
#iterations: 356
currently lose_sum: 85.83004075288773
time_elpased: 2.015
batch start
#iterations: 357
currently lose_sum: 86.68334901332855
time_elpased: 2.058
batch start
#iterations: 358
currently lose_sum: 86.79257297515869
time_elpased: 2.001
batch start
#iterations: 359
currently lose_sum: 87.05124408006668
time_elpased: 2.028
start validation test
0.672628865979
0.70995508982
0.585674590923
0.641854170191
0.672781527621
58.858
batch start
#iterations: 360
currently lose_sum: 86.35109847784042
time_elpased: 2.041
batch start
#iterations: 361
currently lose_sum: 86.84546148777008
time_elpased: 2.066
batch start
#iterations: 362
currently lose_sum: 86.5697511434555
time_elpased: 2.082
batch start
#iterations: 363
currently lose_sum: 86.62693440914154
time_elpased: 2.014
batch start
#iterations: 364
currently lose_sum: 86.77499258518219
time_elpased: 1.996
batch start
#iterations: 365
currently lose_sum: 85.78090661764145
time_elpased: 2.065
batch start
#iterations: 366
currently lose_sum: 85.91725313663483
time_elpased: 1.992
batch start
#iterations: 367
currently lose_sum: 86.66113781929016
time_elpased: 2.033
batch start
#iterations: 368
currently lose_sum: 86.8860531449318
time_elpased: 2.065
batch start
#iterations: 369
currently lose_sum: 86.06555771827698
time_elpased: 2.038
batch start
#iterations: 370
currently lose_sum: 86.55836671590805
time_elpased: 2.012
batch start
#iterations: 371
currently lose_sum: 86.2380701303482
time_elpased: 2.039
batch start
#iterations: 372
currently lose_sum: 86.65969502925873
time_elpased: 2.046
batch start
#iterations: 373
currently lose_sum: 86.46917998790741
time_elpased: 2.011
batch start
#iterations: 374
currently lose_sum: 86.37609416246414
time_elpased: 2.037
batch start
#iterations: 375
currently lose_sum: 87.12777101993561
time_elpased: 2.008
batch start
#iterations: 376
currently lose_sum: 86.72225540876389
time_elpased: 2.019
batch start
#iterations: 377
currently lose_sum: 86.12432312965393
time_elpased: 2.011
batch start
#iterations: 378
currently lose_sum: 86.36086344718933
time_elpased: 2.026
batch start
#iterations: 379
currently lose_sum: 85.96578025817871
time_elpased: 2.073
start validation test
0.666340206186
0.673364685763
0.648348255634
0.660619724217
0.66637179383
58.689
batch start
#iterations: 380
currently lose_sum: 86.07310843467712
time_elpased: 1.996
batch start
#iterations: 381
currently lose_sum: 87.50018227100372
time_elpased: 1.99
batch start
#iterations: 382
currently lose_sum: 85.64998733997345
time_elpased: 2.027
batch start
#iterations: 383
currently lose_sum: 85.8998007774353
time_elpased: 2.004
batch start
#iterations: 384
currently lose_sum: 87.10882073640823
time_elpased: 2.009
batch start
#iterations: 385
currently lose_sum: 85.46063780784607
time_elpased: 2.04
batch start
#iterations: 386
currently lose_sum: 86.87011080980301
time_elpased: 2.022
batch start
#iterations: 387
currently lose_sum: 85.53602230548859
time_elpased: 1.995
batch start
#iterations: 388
currently lose_sum: 85.74087244272232
time_elpased: 2.055
batch start
#iterations: 389
currently lose_sum: 85.95159566402435
time_elpased: 2.001
batch start
#iterations: 390
currently lose_sum: 86.27202081680298
time_elpased: 2.045
batch start
#iterations: 391
currently lose_sum: 85.88574314117432
time_elpased: 2.051
batch start
#iterations: 392
currently lose_sum: 85.21508032083511
time_elpased: 2.009
batch start
#iterations: 393
currently lose_sum: 86.69242417812347
time_elpased: 2.024
batch start
#iterations: 394
currently lose_sum: 86.21455818414688
time_elpased: 2.054
batch start
#iterations: 395
currently lose_sum: 86.23999166488647
time_elpased: 1.975
batch start
#iterations: 396
currently lose_sum: 85.00251537561417
time_elpased: 2.01
batch start
#iterations: 397
currently lose_sum: 86.7984099984169
time_elpased: 2.003
batch start
#iterations: 398
currently lose_sum: 86.29096466302872
time_elpased: 1.995
batch start
#iterations: 399
currently lose_sum: 86.88239252567291
time_elpased: 2.007
start validation test
0.666649484536
0.716147911679
0.554080477514
0.624775166812
0.66684711679
62.605
acc: 0.677
pre: 0.647
rec: 0.779
F1: 0.707
auc: 0.677
