start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.17618519067764
time_elpased: 2.315
batch start
#iterations: 1
currently lose_sum: 99.49607139825821
time_elpased: 2.254
batch start
#iterations: 2
currently lose_sum: 99.03147339820862
time_elpased: 2.264
batch start
#iterations: 3
currently lose_sum: 98.57607924938202
time_elpased: 2.283
batch start
#iterations: 4
currently lose_sum: 98.23121386766434
time_elpased: 2.268
batch start
#iterations: 5
currently lose_sum: 98.31829899549484
time_elpased: 2.239
batch start
#iterations: 6
currently lose_sum: 98.06369662284851
time_elpased: 2.285
batch start
#iterations: 7
currently lose_sum: 98.18288159370422
time_elpased: 2.25
batch start
#iterations: 8
currently lose_sum: 97.82026141881943
time_elpased: 2.283
batch start
#iterations: 9
currently lose_sum: 97.70232897996902
time_elpased: 2.27
batch start
#iterations: 10
currently lose_sum: 97.29695445299149
time_elpased: 2.32
batch start
#iterations: 11
currently lose_sum: 97.33466565608978
time_elpased: 2.261
batch start
#iterations: 12
currently lose_sum: 97.28978991508484
time_elpased: 2.216
batch start
#iterations: 13
currently lose_sum: 96.99006360769272
time_elpased: 2.234
batch start
#iterations: 14
currently lose_sum: 97.14440983533859
time_elpased: 2.328
batch start
#iterations: 15
currently lose_sum: 96.74352717399597
time_elpased: 2.3
batch start
#iterations: 16
currently lose_sum: 96.66946643590927
time_elpased: 2.299
batch start
#iterations: 17
currently lose_sum: 96.54094386100769
time_elpased: 2.338
batch start
#iterations: 18
currently lose_sum: 96.45911467075348
time_elpased: 2.323
batch start
#iterations: 19
currently lose_sum: 96.21729761362076
time_elpased: 2.254
start validation test
0.612319587629
0.681487603306
0.424307913965
0.522991057272
0.612649671121
62.768
batch start
#iterations: 20
currently lose_sum: 96.14962190389633
time_elpased: 2.357
batch start
#iterations: 21
currently lose_sum: 96.08233988285065
time_elpased: 2.182
batch start
#iterations: 22
currently lose_sum: 96.188207924366
time_elpased: 2.189
batch start
#iterations: 23
currently lose_sum: 95.84819513559341
time_elpased: 2.135
batch start
#iterations: 24
currently lose_sum: 95.51601696014404
time_elpased: 2.183
batch start
#iterations: 25
currently lose_sum: 95.94585132598877
time_elpased: 2.305
batch start
#iterations: 26
currently lose_sum: 95.57663077116013
time_elpased: 2.322
batch start
#iterations: 27
currently lose_sum: 95.66527652740479
time_elpased: 2.34
batch start
#iterations: 28
currently lose_sum: 95.59962338209152
time_elpased: 2.065
batch start
#iterations: 29
currently lose_sum: 95.62382125854492
time_elpased: 2.279
batch start
#iterations: 30
currently lose_sum: 94.97910296916962
time_elpased: 2.178
batch start
#iterations: 31
currently lose_sum: 94.81145399808884
time_elpased: 2.185
batch start
#iterations: 32
currently lose_sum: 94.75795924663544
time_elpased: 2.236
batch start
#iterations: 33
currently lose_sum: 95.0777376294136
time_elpased: 2.303
batch start
#iterations: 34
currently lose_sum: 95.21636217832565
time_elpased: 2.277
batch start
#iterations: 35
currently lose_sum: 95.13771545886993
time_elpased: 2.272
batch start
#iterations: 36
currently lose_sum: 94.45229864120483
time_elpased: 2.27
batch start
#iterations: 37
currently lose_sum: 94.70974147319794
time_elpased: 2.262
batch start
#iterations: 38
currently lose_sum: 94.91365724802017
time_elpased: 2.237
batch start
#iterations: 39
currently lose_sum: 94.52229499816895
time_elpased: 2.276
start validation test
0.658659793814
0.679295562507
0.603375527426
0.639088729017
0.658756853871
60.455
batch start
#iterations: 40
currently lose_sum: 94.36757606267929
time_elpased: 2.239
batch start
#iterations: 41
currently lose_sum: 94.6573538184166
time_elpased: 2.151
batch start
#iterations: 42
currently lose_sum: 94.04869544506073
time_elpased: 2.114
batch start
#iterations: 43
currently lose_sum: 94.44692641496658
time_elpased: 2.072
batch start
#iterations: 44
currently lose_sum: 94.58986747264862
time_elpased: 2.064
batch start
#iterations: 45
currently lose_sum: 94.08575069904327
time_elpased: 2.104
batch start
#iterations: 46
currently lose_sum: 93.987111389637
time_elpased: 2.158
batch start
#iterations: 47
currently lose_sum: 94.20332688093185
time_elpased: 2.213
batch start
#iterations: 48
currently lose_sum: 94.10489529371262
time_elpased: 2.202
batch start
#iterations: 49
currently lose_sum: 94.07344603538513
time_elpased: 2.201
batch start
#iterations: 50
currently lose_sum: 93.91396075487137
time_elpased: 2.054
batch start
#iterations: 51
currently lose_sum: 94.00290602445602
time_elpased: 2.171
batch start
#iterations: 52
currently lose_sum: 93.97958379983902
time_elpased: 2.11
batch start
#iterations: 53
currently lose_sum: 94.020527780056
time_elpased: 2.073
batch start
#iterations: 54
currently lose_sum: 93.8023162484169
time_elpased: 2.163
batch start
#iterations: 55
currently lose_sum: 93.7705369591713
time_elpased: 2.146
batch start
#iterations: 56
currently lose_sum: 93.54577994346619
time_elpased: 2.162
batch start
#iterations: 57
currently lose_sum: 93.68404138088226
time_elpased: 2.179
batch start
#iterations: 58
currently lose_sum: 93.67992943525314
time_elpased: 2.235
batch start
#iterations: 59
currently lose_sum: 93.44941133260727
time_elpased: 2.127
start validation test
0.652731958763
0.687468545546
0.562313471236
0.618624398528
0.652890702364
60.095
batch start
#iterations: 60
currently lose_sum: 93.7876346707344
time_elpased: 2.134
batch start
#iterations: 61
currently lose_sum: 93.17842960357666
time_elpased: 2.173
batch start
#iterations: 62
currently lose_sum: 93.84210556745529
time_elpased: 2.2
batch start
#iterations: 63
currently lose_sum: 93.25810992717743
time_elpased: 2.174
batch start
#iterations: 64
currently lose_sum: 93.19480019807816
time_elpased: 2.204
batch start
#iterations: 65
currently lose_sum: 92.89813774824142
time_elpased: 2.18
batch start
#iterations: 66
currently lose_sum: 92.70643937587738
time_elpased: 2.203
batch start
#iterations: 67
currently lose_sum: 92.83136135339737
time_elpased: 2.173
batch start
#iterations: 68
currently lose_sum: 93.07907664775848
time_elpased: 2.179
batch start
#iterations: 69
currently lose_sum: 92.85126173496246
time_elpased: 2.223
batch start
#iterations: 70
currently lose_sum: 92.5957732796669
time_elpased: 2.173
batch start
#iterations: 71
currently lose_sum: 92.91691249608994
time_elpased: 2.14
batch start
#iterations: 72
currently lose_sum: 92.84602349996567
time_elpased: 2.169
batch start
#iterations: 73
currently lose_sum: 92.9369934797287
time_elpased: 2.199
batch start
#iterations: 74
currently lose_sum: 92.64147013425827
time_elpased: 2.229
batch start
#iterations: 75
currently lose_sum: 92.8462820649147
time_elpased: 2.142
batch start
#iterations: 76
currently lose_sum: 92.37423980236053
time_elpased: 2.187
batch start
#iterations: 77
currently lose_sum: 92.87158745527267
time_elpased: 2.041
batch start
#iterations: 78
currently lose_sum: 92.49909383058548
time_elpased: 2.067
batch start
#iterations: 79
currently lose_sum: 93.01267552375793
time_elpased: 2.178
start validation test
0.680360824742
0.652896155853
0.77256354842
0.707706811218
0.68019894864
59.384
batch start
#iterations: 80
currently lose_sum: 92.68214052915573
time_elpased: 2.205
batch start
#iterations: 81
currently lose_sum: 92.52756375074387
time_elpased: 2.22
batch start
#iterations: 82
currently lose_sum: 92.43773055076599
time_elpased: 2.229
batch start
#iterations: 83
currently lose_sum: 92.36432230472565
time_elpased: 2.182
batch start
#iterations: 84
currently lose_sum: 92.49196940660477
time_elpased: 2.204
batch start
#iterations: 85
currently lose_sum: 91.8628038764
time_elpased: 2.157
batch start
#iterations: 86
currently lose_sum: 92.11301082372665
time_elpased: 2.119
batch start
#iterations: 87
currently lose_sum: 92.26053482294083
time_elpased: 2.117
batch start
#iterations: 88
currently lose_sum: 92.35923391580582
time_elpased: 2.125
batch start
#iterations: 89
currently lose_sum: 92.47049123048782
time_elpased: 2.136
batch start
#iterations: 90
currently lose_sum: 92.28668749332428
time_elpased: 2.154
batch start
#iterations: 91
currently lose_sum: 92.62598150968552
time_elpased: 2.155
batch start
#iterations: 92
currently lose_sum: 91.89940959215164
time_elpased: 2.177
batch start
#iterations: 93
currently lose_sum: 92.05483967065811
time_elpased: 2.165
batch start
#iterations: 94
currently lose_sum: 91.96148085594177
time_elpased: 2.187
batch start
#iterations: 95
currently lose_sum: 91.35898780822754
time_elpased: 2.192
batch start
#iterations: 96
currently lose_sum: 91.93052345514297
time_elpased: 2.161
batch start
#iterations: 97
currently lose_sum: 91.64019626379013
time_elpased: 2.139
batch start
#iterations: 98
currently lose_sum: 91.5112818479538
time_elpased: 2.121
batch start
#iterations: 99
currently lose_sum: 91.71839809417725
time_elpased: 2.195
start validation test
0.659948453608
0.681226765799
0.603478439848
0.64
0.660047595427
59.250
batch start
#iterations: 100
currently lose_sum: 91.85296708345413
time_elpased: 2.174
batch start
#iterations: 101
currently lose_sum: 91.87940990924835
time_elpased: 2.195
batch start
#iterations: 102
currently lose_sum: 91.27694195508957
time_elpased: 2.193
batch start
#iterations: 103
currently lose_sum: 91.68528997898102
time_elpased: 2.225
batch start
#iterations: 104
currently lose_sum: 91.35898125171661
time_elpased: 2.193
batch start
#iterations: 105
currently lose_sum: 91.4029706120491
time_elpased: 2.227
batch start
#iterations: 106
currently lose_sum: 91.67711144685745
time_elpased: 2.212
batch start
#iterations: 107
currently lose_sum: 91.81970173120499
time_elpased: 2.176
batch start
#iterations: 108
currently lose_sum: 91.2611123919487
time_elpased: 2.174
batch start
#iterations: 109
currently lose_sum: 91.49153393507004
time_elpased: 2.188
batch start
#iterations: 110
currently lose_sum: 91.43048775196075
time_elpased: 2.177
batch start
#iterations: 111
currently lose_sum: 91.39083480834961
time_elpased: 2.172
batch start
#iterations: 112
currently lose_sum: 91.45878005027771
time_elpased: 2.233
batch start
#iterations: 113
currently lose_sum: 90.64996337890625
time_elpased: 2.17
batch start
#iterations: 114
currently lose_sum: 91.326475918293
time_elpased: 2.196
batch start
#iterations: 115
currently lose_sum: 91.28030824661255
time_elpased: 2.213
batch start
#iterations: 116
currently lose_sum: 90.62024027109146
time_elpased: 2.203
batch start
#iterations: 117
currently lose_sum: 90.96260398626328
time_elpased: 2.171
batch start
#iterations: 118
currently lose_sum: 90.61371350288391
time_elpased: 2.205
batch start
#iterations: 119
currently lose_sum: 90.97317385673523
time_elpased: 2.216
start validation test
0.681649484536
0.673970718286
0.705876299269
0.68955463959
0.681606950626
58.445
batch start
#iterations: 120
currently lose_sum: 90.82173144817352
time_elpased: 2.158
batch start
#iterations: 121
currently lose_sum: 90.57441639900208
time_elpased: 2.167
batch start
#iterations: 122
currently lose_sum: 91.60009354352951
time_elpased: 2.163
batch start
#iterations: 123
currently lose_sum: 90.90497541427612
time_elpased: 2.198
batch start
#iterations: 124
currently lose_sum: 91.1428787112236
time_elpased: 2.204
batch start
#iterations: 125
currently lose_sum: 90.5617413520813
time_elpased: 2.22
batch start
#iterations: 126
currently lose_sum: 90.89959770441055
time_elpased: 2.239
batch start
#iterations: 127
currently lose_sum: 90.92570215463638
time_elpased: 2.183
batch start
#iterations: 128
currently lose_sum: 90.58109647035599
time_elpased: 2.166
batch start
#iterations: 129
currently lose_sum: 91.27762925624847
time_elpased: 2.169
batch start
#iterations: 130
currently lose_sum: 90.6500226855278
time_elpased: 2.262
batch start
#iterations: 131
currently lose_sum: 91.26162427663803
time_elpased: 2.328
batch start
#iterations: 132
currently lose_sum: 90.56095337867737
time_elpased: 2.314
batch start
#iterations: 133
currently lose_sum: 89.80354022979736
time_elpased: 2.304
batch start
#iterations: 134
currently lose_sum: 90.16775250434875
time_elpased: 2.289
batch start
#iterations: 135
currently lose_sum: 90.99798411130905
time_elpased: 2.248
batch start
#iterations: 136
currently lose_sum: 90.32503789663315
time_elpased: 2.248
batch start
#iterations: 137
currently lose_sum: 90.43023300170898
time_elpased: 2.236
batch start
#iterations: 138
currently lose_sum: 90.26418405771255
time_elpased: 2.289
batch start
#iterations: 139
currently lose_sum: 90.24649810791016
time_elpased: 2.263
start validation test
0.670206185567
0.670642673522
0.671194813214
0.670918629771
0.670204449879
58.897
batch start
#iterations: 140
currently lose_sum: 90.19044899940491
time_elpased: 2.25
batch start
#iterations: 141
currently lose_sum: 90.2137668132782
time_elpased: 2.189
batch start
#iterations: 142
currently lose_sum: 90.19133853912354
time_elpased: 2.204
batch start
#iterations: 143
currently lose_sum: 90.30367422103882
time_elpased: 2.292
batch start
#iterations: 144
currently lose_sum: 90.50049299001694
time_elpased: 2.281
batch start
#iterations: 145
currently lose_sum: 90.59434223175049
time_elpased: 2.189
batch start
#iterations: 146
currently lose_sum: 90.35955518484116
time_elpased: 2.162
batch start
#iterations: 147
currently lose_sum: 89.80517989397049
time_elpased: 2.155
batch start
#iterations: 148
currently lose_sum: 89.99514377117157
time_elpased: 2.155
batch start
#iterations: 149
currently lose_sum: 89.75264674425125
time_elpased: 2.209
batch start
#iterations: 150
currently lose_sum: 90.37331372499466
time_elpased: 2.189
batch start
#iterations: 151
currently lose_sum: 90.51536536216736
time_elpased: 2.181
batch start
#iterations: 152
currently lose_sum: 90.63254249095917
time_elpased: 2.209
batch start
#iterations: 153
currently lose_sum: 89.62300461530685
time_elpased: 2.177
batch start
#iterations: 154
currently lose_sum: 89.52765339612961
time_elpased: 2.171
batch start
#iterations: 155
currently lose_sum: 89.62448018789291
time_elpased: 2.143
batch start
#iterations: 156
currently lose_sum: 89.68537819385529
time_elpased: 2.216
batch start
#iterations: 157
currently lose_sum: 89.78167498111725
time_elpased: 2.195
batch start
#iterations: 158
currently lose_sum: 89.53601568937302
time_elpased: 2.215
batch start
#iterations: 159
currently lose_sum: 89.58499467372894
time_elpased: 2.322
start validation test
0.671134020619
0.664660021711
0.693115159
0.678589420655
0.67109542934
58.759
batch start
#iterations: 160
currently lose_sum: 89.07838600873947
time_elpased: 2.324
batch start
#iterations: 161
currently lose_sum: 89.78004485368729
time_elpased: 2.28
batch start
#iterations: 162
currently lose_sum: 89.52568525075912
time_elpased: 2.202
batch start
#iterations: 163
currently lose_sum: 90.03344172239304
time_elpased: 2.263
batch start
#iterations: 164
currently lose_sum: 89.11083608865738
time_elpased: 2.294
batch start
#iterations: 165
currently lose_sum: 89.39471340179443
time_elpased: 2.251
batch start
#iterations: 166
currently lose_sum: 89.62999933958054
time_elpased: 2.262
batch start
#iterations: 167
currently lose_sum: 89.9012263417244
time_elpased: 2.32
batch start
#iterations: 168
currently lose_sum: 89.70932966470718
time_elpased: 2.324
batch start
#iterations: 169
currently lose_sum: 89.34381610155106
time_elpased: 2.287
batch start
#iterations: 170
currently lose_sum: 89.91979998350143
time_elpased: 2.271
batch start
#iterations: 171
currently lose_sum: 89.38677316904068
time_elpased: 2.322
batch start
#iterations: 172
currently lose_sum: 89.23139363527298
time_elpased: 2.321
batch start
#iterations: 173
currently lose_sum: 89.64126342535019
time_elpased: 2.316
batch start
#iterations: 174
currently lose_sum: 88.8372933268547
time_elpased: 2.262
batch start
#iterations: 175
currently lose_sum: 89.21226036548615
time_elpased: 2.332
batch start
#iterations: 176
currently lose_sum: 88.96165585517883
time_elpased: 2.28
batch start
#iterations: 177
currently lose_sum: 89.47994828224182
time_elpased: 2.242
batch start
#iterations: 178
currently lose_sum: 89.09072178602219
time_elpased: 2.265
batch start
#iterations: 179
currently lose_sum: 88.7535405755043
time_elpased: 2.334
start validation test
0.670515463918
0.6708106442
0.671915200165
0.671362467866
0.670513006465
58.511
batch start
#iterations: 180
currently lose_sum: 89.10709875822067
time_elpased: 2.262
batch start
#iterations: 181
currently lose_sum: 89.37565922737122
time_elpased: 2.306
batch start
#iterations: 182
currently lose_sum: 89.0086595416069
time_elpased: 2.271
batch start
#iterations: 183
currently lose_sum: 88.56334257125854
time_elpased: 2.237
batch start
#iterations: 184
currently lose_sum: 89.38107818365097
time_elpased: 2.281
batch start
#iterations: 185
currently lose_sum: 89.12896633148193
time_elpased: 2.239
batch start
#iterations: 186
currently lose_sum: 89.02452158927917
time_elpased: 2.246
batch start
#iterations: 187
currently lose_sum: 88.61162894964218
time_elpased: 2.271
batch start
#iterations: 188
currently lose_sum: 88.97545182704926
time_elpased: 2.274
batch start
#iterations: 189
currently lose_sum: 89.13775372505188
time_elpased: 2.271
batch start
#iterations: 190
currently lose_sum: 88.73026514053345
time_elpased: 2.262
batch start
#iterations: 191
currently lose_sum: 89.02284789085388
time_elpased: 2.244
batch start
#iterations: 192
currently lose_sum: 89.10719656944275
time_elpased: 2.222
batch start
#iterations: 193
currently lose_sum: 89.65245884656906
time_elpased: 2.241
batch start
#iterations: 194
currently lose_sum: 88.22444212436676
time_elpased: 2.224
batch start
#iterations: 195
currently lose_sum: 88.69342213869095
time_elpased: 2.251
batch start
#iterations: 196
currently lose_sum: 88.88343769311905
time_elpased: 2.33
batch start
#iterations: 197
currently lose_sum: 88.8289669752121
time_elpased: 2.28
batch start
#iterations: 198
currently lose_sum: 89.18579268455505
time_elpased: 2.302
batch start
#iterations: 199
currently lose_sum: 88.27854478359222
time_elpased: 2.232
start validation test
0.611030927835
0.701428836519
0.389008953381
0.50046339203
0.611420721656
61.367
batch start
#iterations: 200
currently lose_sum: 88.47244536876678
time_elpased: 2.226
batch start
#iterations: 201
currently lose_sum: 89.15940874814987
time_elpased: 2.206
batch start
#iterations: 202
currently lose_sum: 88.3768395781517
time_elpased: 2.297
batch start
#iterations: 203
currently lose_sum: 88.37834864854813
time_elpased: 2.276
batch start
#iterations: 204
currently lose_sum: 88.27386265993118
time_elpased: 2.278
batch start
#iterations: 205
currently lose_sum: 88.65787923336029
time_elpased: 2.27
batch start
#iterations: 206
currently lose_sum: 88.0867925286293
time_elpased: 2.272
batch start
#iterations: 207
currently lose_sum: 88.98036402463913
time_elpased: 2.323
batch start
#iterations: 208
currently lose_sum: 88.48497188091278
time_elpased: 2.295
batch start
#iterations: 209
currently lose_sum: 88.94098854064941
time_elpased: 2.365
batch start
#iterations: 210
currently lose_sum: 88.20239686965942
time_elpased: 2.334
batch start
#iterations: 211
currently lose_sum: 88.37368530035019
time_elpased: 2.229
batch start
#iterations: 212
currently lose_sum: 88.51336252689362
time_elpased: 2.262
batch start
#iterations: 213
currently lose_sum: 88.38190484046936
time_elpased: 2.287
batch start
#iterations: 214
currently lose_sum: 87.88652455806732
time_elpased: 2.336
batch start
#iterations: 215
currently lose_sum: 87.91799759864807
time_elpased: 2.3
batch start
#iterations: 216
currently lose_sum: 88.24695402383804
time_elpased: 2.288
batch start
#iterations: 217
currently lose_sum: 87.91942572593689
time_elpased: 2.275
batch start
#iterations: 218
currently lose_sum: 88.0574802160263
time_elpased: 2.247
batch start
#iterations: 219
currently lose_sum: 88.36891305446625
time_elpased: 2.232
start validation test
0.66324742268
0.68185972127
0.614284244108
0.64631043257
0.663333385092
58.674
batch start
#iterations: 220
currently lose_sum: 88.06648999452591
time_elpased: 2.245
batch start
#iterations: 221
currently lose_sum: 87.86793893575668
time_elpased: 2.238
batch start
#iterations: 222
currently lose_sum: 87.57789349555969
time_elpased: 2.348
batch start
#iterations: 223
currently lose_sum: 87.6712526679039
time_elpased: 2.353
batch start
#iterations: 224
currently lose_sum: 87.86086654663086
time_elpased: 2.262
batch start
#iterations: 225
currently lose_sum: 88.3130412697792
time_elpased: 2.235
batch start
#iterations: 226
currently lose_sum: 87.8003101348877
time_elpased: 2.283
batch start
#iterations: 227
currently lose_sum: 88.7281613945961
time_elpased: 2.258
batch start
#iterations: 228
currently lose_sum: 88.58075821399689
time_elpased: 2.264
batch start
#iterations: 229
currently lose_sum: 87.63633370399475
time_elpased: 2.23
batch start
#iterations: 230
currently lose_sum: 87.6115050315857
time_elpased: 2.293
batch start
#iterations: 231
currently lose_sum: 87.66224843263626
time_elpased: 2.273
batch start
#iterations: 232
currently lose_sum: 88.39762032032013
time_elpased: 2.28
batch start
#iterations: 233
currently lose_sum: 87.68155872821808
time_elpased: 2.265
batch start
#iterations: 234
currently lose_sum: 87.89256274700165
time_elpased: 2.245
batch start
#iterations: 235
currently lose_sum: 87.57154554128647
time_elpased: 2.248
batch start
#iterations: 236
currently lose_sum: 88.02014219760895
time_elpased: 2.261
batch start
#iterations: 237
currently lose_sum: 88.05878335237503
time_elpased: 2.402
batch start
#iterations: 238
currently lose_sum: 88.3241907954216
time_elpased: 2.342
batch start
#iterations: 239
currently lose_sum: 87.5268269777298
time_elpased: 2.316
start validation test
0.658092783505
0.684053473383
0.589791087784
0.633434650456
0.658212697667
58.974
batch start
#iterations: 240
currently lose_sum: 86.7418572306633
time_elpased: 2.252
batch start
#iterations: 241
currently lose_sum: 88.29527759552002
time_elpased: 2.279
batch start
#iterations: 242
currently lose_sum: 87.5549390912056
time_elpased: 2.291
batch start
#iterations: 243
currently lose_sum: 87.46381962299347
time_elpased: 2.282
batch start
#iterations: 244
currently lose_sum: 87.81759989261627
time_elpased: 2.256
batch start
#iterations: 245
currently lose_sum: 87.03363996744156
time_elpased: 2.282
batch start
#iterations: 246
currently lose_sum: 87.48190701007843
time_elpased: 2.251
batch start
#iterations: 247
currently lose_sum: 86.56844234466553
time_elpased: 2.245
batch start
#iterations: 248
currently lose_sum: 87.32456731796265
time_elpased: 2.261
batch start
#iterations: 249
currently lose_sum: 87.07709008455276
time_elpased: 2.245
batch start
#iterations: 250
currently lose_sum: 87.05631268024445
time_elpased: 2.251
batch start
#iterations: 251
currently lose_sum: 87.53182452917099
time_elpased: 2.271
batch start
#iterations: 252
currently lose_sum: 87.11038798093796
time_elpased: 2.279
batch start
#iterations: 253
currently lose_sum: 86.81638824939728
time_elpased: 2.312
batch start
#iterations: 254
currently lose_sum: 88.29687649011612
time_elpased: 2.289
batch start
#iterations: 255
currently lose_sum: 87.91780465841293
time_elpased: 2.263
batch start
#iterations: 256
currently lose_sum: 87.45644807815552
time_elpased: 2.272
batch start
#iterations: 257
currently lose_sum: 87.32868993282318
time_elpased: 2.24
batch start
#iterations: 258
currently lose_sum: 87.38106232881546
time_elpased: 2.237
batch start
#iterations: 259
currently lose_sum: 87.15895807743073
time_elpased: 2.259
start validation test
0.639278350515
0.693193122069
0.50200679222
0.582308702399
0.639519351909
59.551
batch start
#iterations: 260
currently lose_sum: 86.35272783041
time_elpased: 2.301
batch start
#iterations: 261
currently lose_sum: 86.74517476558685
time_elpased: 2.262
batch start
#iterations: 262
currently lose_sum: 86.87791126966476
time_elpased: 2.277
batch start
#iterations: 263
currently lose_sum: 87.81517767906189
time_elpased: 2.274
batch start
#iterations: 264
currently lose_sum: 87.1585528254509
time_elpased: 2.34
batch start
#iterations: 265
currently lose_sum: 86.59637367725372
time_elpased: 2.319
batch start
#iterations: 266
currently lose_sum: 87.23881167173386
time_elpased: 2.327
batch start
#iterations: 267
currently lose_sum: 86.72848218679428
time_elpased: 2.276
batch start
#iterations: 268
currently lose_sum: 87.8627712726593
time_elpased: 2.296
batch start
#iterations: 269
currently lose_sum: 87.13698118925095
time_elpased: 2.213
batch start
#iterations: 270
currently lose_sum: 86.8765794634819
time_elpased: 2.333
batch start
#iterations: 271
currently lose_sum: 86.84517711400986
time_elpased: 2.329
batch start
#iterations: 272
currently lose_sum: 86.47106093168259
time_elpased: 2.305
batch start
#iterations: 273
currently lose_sum: 87.31242114305496
time_elpased: 2.275
batch start
#iterations: 274
currently lose_sum: 86.8717491030693
time_elpased: 2.3
batch start
#iterations: 275
currently lose_sum: 87.31020450592041
time_elpased: 2.269
batch start
#iterations: 276
currently lose_sum: 87.02389460802078
time_elpased: 2.257
batch start
#iterations: 277
currently lose_sum: 86.10793155431747
time_elpased: 2.246
batch start
#iterations: 278
currently lose_sum: 86.50657343864441
time_elpased: 2.303
batch start
#iterations: 279
currently lose_sum: 86.67705512046814
time_elpased: 2.256
start validation test
0.64912371134
0.689898198904
0.543995060204
0.608320386674
0.649308280902
59.290
batch start
#iterations: 280
currently lose_sum: 86.97136306762695
time_elpased: 2.299
batch start
#iterations: 281
currently lose_sum: 86.70973694324493
time_elpased: 2.285
batch start
#iterations: 282
currently lose_sum: 87.59524548053741
time_elpased: 2.225
batch start
#iterations: 283
currently lose_sum: 86.70116955041885
time_elpased: 2.209
batch start
#iterations: 284
currently lose_sum: 86.4366130232811
time_elpased: 2.259
batch start
#iterations: 285
currently lose_sum: 86.33765298128128
time_elpased: 2.226
batch start
#iterations: 286
currently lose_sum: 86.54985773563385
time_elpased: 2.286
batch start
#iterations: 287
currently lose_sum: 87.21410912275314
time_elpased: 2.291
batch start
#iterations: 288
currently lose_sum: 86.92321556806564
time_elpased: 2.329
batch start
#iterations: 289
currently lose_sum: 86.25379991531372
time_elpased: 2.311
batch start
#iterations: 290
currently lose_sum: 86.4588069319725
time_elpased: 2.294
batch start
#iterations: 291
currently lose_sum: 86.68408113718033
time_elpased: 2.242
batch start
#iterations: 292
currently lose_sum: 86.4366974234581
time_elpased: 2.282
batch start
#iterations: 293
currently lose_sum: 86.8035340309143
time_elpased: 2.24
batch start
#iterations: 294
currently lose_sum: 85.90298342704773
time_elpased: 2.282
batch start
#iterations: 295
currently lose_sum: 86.72199982404709
time_elpased: 2.251
batch start
#iterations: 296
currently lose_sum: 86.52497321367264
time_elpased: 2.224
batch start
#iterations: 297
currently lose_sum: 86.52968049049377
time_elpased: 2.248
batch start
#iterations: 298
currently lose_sum: 86.32996827363968
time_elpased: 2.235
batch start
#iterations: 299
currently lose_sum: 86.52975255250931
time_elpased: 2.333
start validation test
0.655515463918
0.681112702961
0.587115364825
0.630630630631
0.655635550841
58.765
batch start
#iterations: 300
currently lose_sum: 86.93405866622925
time_elpased: 2.282
batch start
#iterations: 301
currently lose_sum: 87.45791065692902
time_elpased: 2.26
batch start
#iterations: 302
currently lose_sum: 86.43779122829437
time_elpased: 2.318
batch start
#iterations: 303
currently lose_sum: 86.72903126478195
time_elpased: 2.271
batch start
#iterations: 304
currently lose_sum: 86.65841990709305
time_elpased: 2.313
batch start
#iterations: 305
currently lose_sum: 85.55997437238693
time_elpased: 2.302
batch start
#iterations: 306
currently lose_sum: 86.39544308185577
time_elpased: 2.324
batch start
#iterations: 307
currently lose_sum: 86.31350207328796
time_elpased: 2.254
batch start
#iterations: 308
currently lose_sum: 85.46468317508698
time_elpased: 2.315
batch start
#iterations: 309
currently lose_sum: 86.03508067131042
time_elpased: 2.261
batch start
#iterations: 310
currently lose_sum: 85.8000460267067
time_elpased: 2.286
batch start
#iterations: 311
currently lose_sum: 85.80017882585526
time_elpased: 2.279
batch start
#iterations: 312
currently lose_sum: 86.09963154792786
time_elpased: 2.305
batch start
#iterations: 313
currently lose_sum: 86.16508013010025
time_elpased: 2.317
batch start
#iterations: 314
currently lose_sum: 86.55068415403366
time_elpased: 2.263
batch start
#iterations: 315
currently lose_sum: 85.5982095003128
time_elpased: 2.218
batch start
#iterations: 316
currently lose_sum: 86.61082404851913
time_elpased: 2.332
batch start
#iterations: 317
currently lose_sum: 85.96845906972885
time_elpased: 2.268
batch start
#iterations: 318
currently lose_sum: 86.13198781013489
time_elpased: 2.288
batch start
#iterations: 319
currently lose_sum: 86.34259158372879
time_elpased: 2.284
start validation test
0.668350515464
0.663904143784
0.684264690748
0.673930670991
0.668322575675
58.334
batch start
#iterations: 320
currently lose_sum: 86.06911313533783
time_elpased: 2.257
batch start
#iterations: 321
currently lose_sum: 85.6145127415657
time_elpased: 2.269
batch start
#iterations: 322
currently lose_sum: 86.49396580457687
time_elpased: 2.27
batch start
#iterations: 323
currently lose_sum: 86.1448033452034
time_elpased: 2.357
batch start
#iterations: 324
currently lose_sum: 85.53048712015152
time_elpased: 2.247
batch start
#iterations: 325
currently lose_sum: 86.41085261106491
time_elpased: 2.274
batch start
#iterations: 326
currently lose_sum: 85.49178856611252
time_elpased: 2.268
batch start
#iterations: 327
currently lose_sum: 86.40591084957123
time_elpased: 2.302
batch start
#iterations: 328
currently lose_sum: 85.89967876672745
time_elpased: 2.262
batch start
#iterations: 329
currently lose_sum: 85.2660863995552
time_elpased: 2.195
batch start
#iterations: 330
currently lose_sum: 85.80843245983124
time_elpased: 2.227
batch start
#iterations: 331
currently lose_sum: 85.76528936624527
time_elpased: 2.262
batch start
#iterations: 332
currently lose_sum: 85.02170604467392
time_elpased: 2.263
batch start
#iterations: 333
currently lose_sum: 85.53939813375473
time_elpased: 2.22
batch start
#iterations: 334
currently lose_sum: 84.72407275438309
time_elpased: 2.283
batch start
#iterations: 335
currently lose_sum: 86.13099992275238
time_elpased: 2.313
batch start
#iterations: 336
currently lose_sum: 86.26910144090652
time_elpased: 2.304
batch start
#iterations: 337
currently lose_sum: 85.11489713191986
time_elpased: 2.274
batch start
#iterations: 338
currently lose_sum: 86.36821800470352
time_elpased: 2.29
batch start
#iterations: 339
currently lose_sum: 86.3724958896637
time_elpased: 2.267
start validation test
0.630773195876
0.692435201929
0.472882576927
0.561976395768
0.631050397211
60.349
batch start
#iterations: 340
currently lose_sum: 85.34969609975815
time_elpased: 2.255
batch start
#iterations: 341
currently lose_sum: 85.31084251403809
time_elpased: 2.313
batch start
#iterations: 342
currently lose_sum: 86.02495074272156
time_elpased: 2.261
batch start
#iterations: 343
currently lose_sum: 84.73273867368698
time_elpased: 2.324
batch start
#iterations: 344
currently lose_sum: 85.66941493749619
time_elpased: 2.288
batch start
#iterations: 345
currently lose_sum: 85.880146920681
time_elpased: 2.305
batch start
#iterations: 346
currently lose_sum: 85.18983054161072
time_elpased: 2.344
batch start
#iterations: 347
currently lose_sum: 85.56995612382889
time_elpased: 2.309
batch start
#iterations: 348
currently lose_sum: 84.67991161346436
time_elpased: 2.315
batch start
#iterations: 349
currently lose_sum: 85.47484183311462
time_elpased: 2.316
batch start
#iterations: 350
currently lose_sum: 85.14952683448792
time_elpased: 2.231
batch start
#iterations: 351
currently lose_sum: 85.763955950737
time_elpased: 2.258
batch start
#iterations: 352
currently lose_sum: 85.33569312095642
time_elpased: 2.261
batch start
#iterations: 353
currently lose_sum: 85.35844242572784
time_elpased: 2.294
batch start
#iterations: 354
currently lose_sum: 85.19834226369858
time_elpased: 2.309
batch start
#iterations: 355
currently lose_sum: 85.35512083768845
time_elpased: 2.332
batch start
#iterations: 356
currently lose_sum: 86.2190414071083
time_elpased: 2.372
batch start
#iterations: 357
currently lose_sum: 85.36782962083817
time_elpased: 2.277
batch start
#iterations: 358
currently lose_sum: 84.80077087879181
time_elpased: 2.245
batch start
#iterations: 359
currently lose_sum: 85.60667216777802
time_elpased: 2.268
start validation test
0.606701030928
0.699865926068
0.376041988268
0.489222118088
0.607105988454
62.388
batch start
#iterations: 360
currently lose_sum: 86.05216723680496
time_elpased: 2.323
batch start
#iterations: 361
currently lose_sum: 85.42173200845718
time_elpased: 2.17
batch start
#iterations: 362
currently lose_sum: 85.3790140748024
time_elpased: 2.328
batch start
#iterations: 363
currently lose_sum: 85.88024771213531
time_elpased: 2.141
batch start
#iterations: 364
currently lose_sum: 85.2213962674141
time_elpased: 2.172
batch start
#iterations: 365
currently lose_sum: 86.40148544311523
time_elpased: 2.162
batch start
#iterations: 366
currently lose_sum: 84.98257607221603
time_elpased: 2.246
batch start
#iterations: 367
currently lose_sum: 85.3578137755394
time_elpased: 2.266
batch start
#iterations: 368
currently lose_sum: 85.74693846702576
time_elpased: 2.297
batch start
#iterations: 369
currently lose_sum: 85.2322928905487
time_elpased: 2.268
batch start
#iterations: 370
currently lose_sum: 85.25686794519424
time_elpased: 2.3
batch start
#iterations: 371
currently lose_sum: 85.52328497171402
time_elpased: 2.309
batch start
#iterations: 372
currently lose_sum: 84.51068383455276
time_elpased: 2.224
batch start
#iterations: 373
currently lose_sum: 84.52565670013428
time_elpased: 2.301
batch start
#iterations: 374
currently lose_sum: 85.42619252204895
time_elpased: 2.299
batch start
#iterations: 375
currently lose_sum: 84.5321255326271
time_elpased: 2.253
batch start
#iterations: 376
currently lose_sum: 85.66701424121857
time_elpased: 2.253
batch start
#iterations: 377
currently lose_sum: 84.8576220870018
time_elpased: 2.324
batch start
#iterations: 378
currently lose_sum: 84.6516427397728
time_elpased: 2.245
batch start
#iterations: 379
currently lose_sum: 84.57577645778656
time_elpased: 2.281
start validation test
0.656082474227
0.676358160547
0.600905629309
0.636403269755
0.656179345688
59.091
batch start
#iterations: 380
currently lose_sum: 85.14776420593262
time_elpased: 2.306
batch start
#iterations: 381
currently lose_sum: 85.14857375621796
time_elpased: 2.228
batch start
#iterations: 382
currently lose_sum: 86.15756839513779
time_elpased: 2.249
batch start
#iterations: 383
currently lose_sum: 84.74861401319504
time_elpased: 2.302
batch start
#iterations: 384
currently lose_sum: 84.63137030601501
time_elpased: 2.255
batch start
#iterations: 385
currently lose_sum: 85.03356522321701
time_elpased: 2.239
batch start
#iterations: 386
currently lose_sum: 84.03119003772736
time_elpased: 2.223
batch start
#iterations: 387
currently lose_sum: 84.81780809164047
time_elpased: 2.233
batch start
#iterations: 388
currently lose_sum: 84.82623928785324
time_elpased: 2.202
batch start
#iterations: 389
currently lose_sum: 84.91295742988586
time_elpased: 2.224
batch start
#iterations: 390
currently lose_sum: 84.9299014210701
time_elpased: 2.242
batch start
#iterations: 391
currently lose_sum: 84.92345595359802
time_elpased: 2.235
batch start
#iterations: 392
currently lose_sum: 84.82565051317215
time_elpased: 2.224
batch start
#iterations: 393
currently lose_sum: 84.35836666822433
time_elpased: 2.261
batch start
#iterations: 394
currently lose_sum: 85.75024026632309
time_elpased: 2.26
batch start
#iterations: 395
currently lose_sum: 84.3334327340126
time_elpased: 2.257
batch start
#iterations: 396
currently lose_sum: 85.05532878637314
time_elpased: 2.292
batch start
#iterations: 397
currently lose_sum: 84.7707981467247
time_elpased: 2.242
batch start
#iterations: 398
currently lose_sum: 83.93165355920792
time_elpased: 2.259
batch start
#iterations: 399
currently lose_sum: 85.51473259925842
time_elpased: 2.278
start validation test
0.654845360825
0.676728676729
0.595245446125
0.633377135348
0.654949997668
59.097
acc: 0.665
pre: 0.662
rec: 0.679
F1: 0.670
auc: 0.665
