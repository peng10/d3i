start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.03605496883392
time_elpased: 2.102
batch start
#iterations: 1
currently lose_sum: 99.47960138320923
time_elpased: 2.174
batch start
#iterations: 2
currently lose_sum: 98.7600827217102
time_elpased: 2.101
batch start
#iterations: 3
currently lose_sum: 98.55954158306122
time_elpased: 2.128
batch start
#iterations: 4
currently lose_sum: 98.19413954019547
time_elpased: 2.093
batch start
#iterations: 5
currently lose_sum: 98.27405834197998
time_elpased: 2.169
batch start
#iterations: 6
currently lose_sum: 97.62350398302078
time_elpased: 2.119
batch start
#iterations: 7
currently lose_sum: 97.88921266794205
time_elpased: 2.141
batch start
#iterations: 8
currently lose_sum: 97.37223708629608
time_elpased: 2.082
batch start
#iterations: 9
currently lose_sum: 97.34020870923996
time_elpased: 2.141
batch start
#iterations: 10
currently lose_sum: 97.48706638813019
time_elpased: 2.129
batch start
#iterations: 11
currently lose_sum: 96.78433883190155
time_elpased: 2.129
batch start
#iterations: 12
currently lose_sum: 96.87485587596893
time_elpased: 2.12
batch start
#iterations: 13
currently lose_sum: 96.62243723869324
time_elpased: 2.104
batch start
#iterations: 14
currently lose_sum: 96.46382278203964
time_elpased: 2.146
batch start
#iterations: 15
currently lose_sum: 96.39216774702072
time_elpased: 2.143
batch start
#iterations: 16
currently lose_sum: 96.46206307411194
time_elpased: 2.138
batch start
#iterations: 17
currently lose_sum: 96.10631114244461
time_elpased: 2.093
batch start
#iterations: 18
currently lose_sum: 96.33391898870468
time_elpased: 2.15
batch start
#iterations: 19
currently lose_sum: 95.72067087888718
time_elpased: 2.089
start validation test
0.649329896907
0.661277820839
0.614656237135
0.637115271777
0.649387185069
61.350
batch start
#iterations: 20
currently lose_sum: 95.65918785333633
time_elpased: 2.146
batch start
#iterations: 21
currently lose_sum: 95.63238191604614
time_elpased: 2.094
batch start
#iterations: 22
currently lose_sum: 95.51265233755112
time_elpased: 2.088
batch start
#iterations: 23
currently lose_sum: 95.42733734846115
time_elpased: 2.128
batch start
#iterations: 24
currently lose_sum: 95.12104737758636
time_elpased: 2.096
batch start
#iterations: 25
currently lose_sum: 95.22991341352463
time_elpased: 2.147
batch start
#iterations: 26
currently lose_sum: 95.0795122385025
time_elpased: 2.075
batch start
#iterations: 27
currently lose_sum: 95.02573317289352
time_elpased: 2.1
batch start
#iterations: 28
currently lose_sum: 95.05549907684326
time_elpased: 2.11
batch start
#iterations: 29
currently lose_sum: 94.89697176218033
time_elpased: 2.128
batch start
#iterations: 30
currently lose_sum: 94.67901277542114
time_elpased: 2.088
batch start
#iterations: 31
currently lose_sum: 94.51177048683167
time_elpased: 2.141
batch start
#iterations: 32
currently lose_sum: 94.45840716362
time_elpased: 2.134
batch start
#iterations: 33
currently lose_sum: 94.3117967247963
time_elpased: 2.122
batch start
#iterations: 34
currently lose_sum: 94.01814126968384
time_elpased: 2.17
batch start
#iterations: 35
currently lose_sum: 94.58659237623215
time_elpased: 2.137
batch start
#iterations: 36
currently lose_sum: 93.86596751213074
time_elpased: 2.111
batch start
#iterations: 37
currently lose_sum: 93.89788138866425
time_elpased: 2.151
batch start
#iterations: 38
currently lose_sum: 93.92646342515945
time_elpased: 2.12
batch start
#iterations: 39
currently lose_sum: 94.09694057703018
time_elpased: 2.13
start validation test
0.64324742268
0.676876344766
0.550432276657
0.60714082988
0.643400772777
60.490
batch start
#iterations: 40
currently lose_sum: 93.64050191640854
time_elpased: 2.133
batch start
#iterations: 41
currently lose_sum: 94.01621687412262
time_elpased: 2.099
batch start
#iterations: 42
currently lose_sum: 93.34802812337875
time_elpased: 2.169
batch start
#iterations: 43
currently lose_sum: 93.4419874548912
time_elpased: 2.094
batch start
#iterations: 44
currently lose_sum: 93.18354439735413
time_elpased: 2.165
batch start
#iterations: 45
currently lose_sum: 93.17500084638596
time_elpased: 2.106
batch start
#iterations: 46
currently lose_sum: 92.87700480222702
time_elpased: 2.19
batch start
#iterations: 47
currently lose_sum: 93.52799183130264
time_elpased: 2.136
batch start
#iterations: 48
currently lose_sum: 93.27617347240448
time_elpased: 2.148
batch start
#iterations: 49
currently lose_sum: 93.55448591709137
time_elpased: 2.138
batch start
#iterations: 50
currently lose_sum: 93.15472787618637
time_elpased: 2.097
batch start
#iterations: 51
currently lose_sum: 93.07179123163223
time_elpased: 2.125
batch start
#iterations: 52
currently lose_sum: 92.74055069684982
time_elpased: 2.11
batch start
#iterations: 53
currently lose_sum: 92.92118036746979
time_elpased: 2.129
batch start
#iterations: 54
currently lose_sum: 93.03836071491241
time_elpased: 2.088
batch start
#iterations: 55
currently lose_sum: 92.43158262968063
time_elpased: 2.109
batch start
#iterations: 56
currently lose_sum: 92.46697908639908
time_elpased: 2.181
batch start
#iterations: 57
currently lose_sum: 92.69422620534897
time_elpased: 2.15
batch start
#iterations: 58
currently lose_sum: 92.9657786488533
time_elpased: 2.095
batch start
#iterations: 59
currently lose_sum: 92.45355331897736
time_elpased: 2.12
start validation test
0.677216494845
0.657861060329
0.740736928777
0.696843532146
0.67711154576
59.074
batch start
#iterations: 60
currently lose_sum: 91.97321659326553
time_elpased: 2.144
batch start
#iterations: 61
currently lose_sum: 92.49408775568008
time_elpased: 2.102
batch start
#iterations: 62
currently lose_sum: 92.07994985580444
time_elpased: 2.123
batch start
#iterations: 63
currently lose_sum: 91.97798782587051
time_elpased: 2.145
batch start
#iterations: 64
currently lose_sum: 92.10598772764206
time_elpased: 2.134
batch start
#iterations: 65
currently lose_sum: 92.30571365356445
time_elpased: 2.102
batch start
#iterations: 66
currently lose_sum: 91.96904295682907
time_elpased: 2.157
batch start
#iterations: 67
currently lose_sum: 91.91258549690247
time_elpased: 2.086
batch start
#iterations: 68
currently lose_sum: 91.9782977104187
time_elpased: 2.114
batch start
#iterations: 69
currently lose_sum: 91.66458016633987
time_elpased: 2.085
batch start
#iterations: 70
currently lose_sum: 91.80474799871445
time_elpased: 2.094
batch start
#iterations: 71
currently lose_sum: 91.54902172088623
time_elpased: 2.151
batch start
#iterations: 72
currently lose_sum: 91.5877954363823
time_elpased: 2.102
batch start
#iterations: 73
currently lose_sum: 91.60264557600021
time_elpased: 2.08
batch start
#iterations: 74
currently lose_sum: 91.22977083921432
time_elpased: 2.159
batch start
#iterations: 75
currently lose_sum: 91.80553621053696
time_elpased: 2.118
batch start
#iterations: 76
currently lose_sum: 91.71703547239304
time_elpased: 2.094
batch start
#iterations: 77
currently lose_sum: 91.23849976062775
time_elpased: 2.122
batch start
#iterations: 78
currently lose_sum: 91.31781470775604
time_elpased: 2.132
batch start
#iterations: 79
currently lose_sum: 91.47058254480362
time_elpased: 2.108
start validation test
0.62412371134
0.697073170732
0.441230959242
0.540400857179
0.624425888543
60.318
batch start
#iterations: 80
currently lose_sum: 91.67200636863708
time_elpased: 2.068
batch start
#iterations: 81
currently lose_sum: 90.56449943780899
time_elpased: 2.106
batch start
#iterations: 82
currently lose_sum: 91.12277460098267
time_elpased: 2.103
batch start
#iterations: 83
currently lose_sum: 91.03760594129562
time_elpased: 2.154
batch start
#iterations: 84
currently lose_sum: 90.70834410190582
time_elpased: 2.131
batch start
#iterations: 85
currently lose_sum: 91.70571613311768
time_elpased: 2.128
batch start
#iterations: 86
currently lose_sum: 90.43387860059738
time_elpased: 2.082
batch start
#iterations: 87
currently lose_sum: 90.54615437984467
time_elpased: 2.12
batch start
#iterations: 88
currently lose_sum: 90.57318031787872
time_elpased: 2.092
batch start
#iterations: 89
currently lose_sum: 90.65994411706924
time_elpased: 2.15
batch start
#iterations: 90
currently lose_sum: 90.9924321770668
time_elpased: 2.221
batch start
#iterations: 91
currently lose_sum: 90.58808028697968
time_elpased: 2.112
batch start
#iterations: 92
currently lose_sum: 91.10608917474747
time_elpased: 2.145
batch start
#iterations: 93
currently lose_sum: 90.79867100715637
time_elpased: 2.157
batch start
#iterations: 94
currently lose_sum: 90.12861233949661
time_elpased: 2.111
batch start
#iterations: 95
currently lose_sum: 89.98096948862076
time_elpased: 2.12
batch start
#iterations: 96
currently lose_sum: 90.11072099208832
time_elpased: 2.113
batch start
#iterations: 97
currently lose_sum: 90.81309223175049
time_elpased: 2.089
batch start
#iterations: 98
currently lose_sum: 90.29527217149734
time_elpased: 2.12
batch start
#iterations: 99
currently lose_sum: 90.56238597631454
time_elpased: 2.162
start validation test
0.655206185567
0.68812927284
0.569781803211
0.62338832273
0.655347324571
58.826
batch start
#iterations: 100
currently lose_sum: 90.25685626268387
time_elpased: 2.14
batch start
#iterations: 101
currently lose_sum: 90.23787373304367
time_elpased: 2.078
batch start
#iterations: 102
currently lose_sum: 90.65575432777405
time_elpased: 2.102
batch start
#iterations: 103
currently lose_sum: 90.66043132543564
time_elpased: 2.142
batch start
#iterations: 104
currently lose_sum: 89.42480272054672
time_elpased: 2.165
batch start
#iterations: 105
currently lose_sum: 89.88541632890701
time_elpased: 2.135
batch start
#iterations: 106
currently lose_sum: 89.94066399335861
time_elpased: 2.136
batch start
#iterations: 107
currently lose_sum: 89.77337789535522
time_elpased: 2.162
batch start
#iterations: 108
currently lose_sum: 89.77250784635544
time_elpased: 2.121
batch start
#iterations: 109
currently lose_sum: 90.06976914405823
time_elpased: 2.08
batch start
#iterations: 110
currently lose_sum: 89.57481008768082
time_elpased: 1.944
batch start
#iterations: 111
currently lose_sum: 90.5555135011673
time_elpased: 2.148
batch start
#iterations: 112
currently lose_sum: 89.03042483329773
time_elpased: 2.147
batch start
#iterations: 113
currently lose_sum: 89.70206904411316
time_elpased: 2.133
batch start
#iterations: 114
currently lose_sum: 90.22458893060684
time_elpased: 2.105
batch start
#iterations: 115
currently lose_sum: 89.35897904634476
time_elpased: 2.126
batch start
#iterations: 116
currently lose_sum: 89.88222759962082
time_elpased: 2.109
batch start
#iterations: 117
currently lose_sum: 89.40846353769302
time_elpased: 2.13
batch start
#iterations: 118
currently lose_sum: 89.66722738742828
time_elpased: 2.118
batch start
#iterations: 119
currently lose_sum: 89.41732740402222
time_elpased: 2.137
start validation test
0.678659793814
0.665714829621
0.719843557019
0.691721887054
0.678591749596
57.649
batch start
#iterations: 120
currently lose_sum: 89.29794454574585
time_elpased: 2.122
batch start
#iterations: 121
currently lose_sum: 88.52684473991394
time_elpased: 2.178
batch start
#iterations: 122
currently lose_sum: 89.28392350673676
time_elpased: 2.129
batch start
#iterations: 123
currently lose_sum: 90.25519025325775
time_elpased: 2.114
batch start
#iterations: 124
currently lose_sum: 89.18558752536774
time_elpased: 2.163
batch start
#iterations: 125
currently lose_sum: 88.82148861885071
time_elpased: 2.163
batch start
#iterations: 126
currently lose_sum: 88.61296927928925
time_elpased: 2.152
batch start
#iterations: 127
currently lose_sum: 89.1407083272934
time_elpased: 2.096
batch start
#iterations: 128
currently lose_sum: 89.3115628361702
time_elpased: 2.174
batch start
#iterations: 129
currently lose_sum: 88.63965559005737
time_elpased: 2.124
batch start
#iterations: 130
currently lose_sum: 88.78584492206573
time_elpased: 2.15
batch start
#iterations: 131
currently lose_sum: 89.04179453849792
time_elpased: 2.132
batch start
#iterations: 132
currently lose_sum: 87.92772191762924
time_elpased: 2.147
batch start
#iterations: 133
currently lose_sum: 88.74545747041702
time_elpased: 2.108
batch start
#iterations: 134
currently lose_sum: 89.51972442865372
time_elpased: 2.122
batch start
#iterations: 135
currently lose_sum: 88.79972523450851
time_elpased: 2.144
batch start
#iterations: 136
currently lose_sum: 88.28270536661148
time_elpased: 2.148
batch start
#iterations: 137
currently lose_sum: 89.01978361606598
time_elpased: 2.178
batch start
#iterations: 138
currently lose_sum: 88.57349491119385
time_elpased: 2.121
batch start
#iterations: 139
currently lose_sum: 88.07213199138641
time_elpased: 2.154
start validation test
0.626855670103
0.696120348377
0.452449567723
0.548437402533
0.62714382558
60.860
batch start
#iterations: 140
currently lose_sum: 88.26576501131058
time_elpased: 2.109
batch start
#iterations: 141
currently lose_sum: 88.23142790794373
time_elpased: 2.111
batch start
#iterations: 142
currently lose_sum: 88.51235473155975
time_elpased: 2.118
batch start
#iterations: 143
currently lose_sum: 88.09813696146011
time_elpased: 2.119
batch start
#iterations: 144
currently lose_sum: 87.69017589092255
time_elpased: 2.125
batch start
#iterations: 145
currently lose_sum: 87.8494246006012
time_elpased: 2.122
batch start
#iterations: 146
currently lose_sum: 88.47843444347382
time_elpased: 2.21
batch start
#iterations: 147
currently lose_sum: 88.0095209479332
time_elpased: 2.136
batch start
#iterations: 148
currently lose_sum: 89.02941304445267
time_elpased: 2.136
batch start
#iterations: 149
currently lose_sum: 88.5659344792366
time_elpased: 2.145
batch start
#iterations: 150
currently lose_sum: 87.71002781391144
time_elpased: 2.201
batch start
#iterations: 151
currently lose_sum: 88.0416533946991
time_elpased: 2.133
batch start
#iterations: 152
currently lose_sum: 88.30476474761963
time_elpased: 2.134
batch start
#iterations: 153
currently lose_sum: 88.9691771864891
time_elpased: 2.109
batch start
#iterations: 154
currently lose_sum: 88.6851863861084
time_elpased: 2.132
batch start
#iterations: 155
currently lose_sum: 88.65844488143921
time_elpased: 2.089
batch start
#iterations: 156
currently lose_sum: 88.91519612073898
time_elpased: 2.145
batch start
#iterations: 157
currently lose_sum: 88.01192021369934
time_elpased: 2.137
batch start
#iterations: 158
currently lose_sum: 88.78792750835419
time_elpased: 2.164
batch start
#iterations: 159
currently lose_sum: 87.84760850667953
time_elpased: 2.138
start validation test
0.674381443299
0.662119622246
0.714388637299
0.687261745631
0.67431534302
57.652
batch start
#iterations: 160
currently lose_sum: 88.64534819126129
time_elpased: 2.183
batch start
#iterations: 161
currently lose_sum: 87.60929083824158
time_elpased: 2.136
batch start
#iterations: 162
currently lose_sum: 88.5757874250412
time_elpased: 2.096
batch start
#iterations: 163
currently lose_sum: 87.69432455301285
time_elpased: 2.147
batch start
#iterations: 164
currently lose_sum: 88.0018898844719
time_elpased: 2.092
batch start
#iterations: 165
currently lose_sum: 88.16016733646393
time_elpased: 2.132
batch start
#iterations: 166
currently lose_sum: 87.57846367359161
time_elpased: 2.105
batch start
#iterations: 167
currently lose_sum: 87.67025923728943
time_elpased: 2.124
batch start
#iterations: 168
currently lose_sum: 89.05932241678238
time_elpased: 2.11
batch start
#iterations: 169
currently lose_sum: 87.4775031208992
time_elpased: 2.203
batch start
#iterations: 170
currently lose_sum: 87.9609375
time_elpased: 2.112
batch start
#iterations: 171
currently lose_sum: 88.13082361221313
time_elpased: 2.154
batch start
#iterations: 172
currently lose_sum: 87.48595064878464
time_elpased: 2.136
batch start
#iterations: 173
currently lose_sum: 87.29479718208313
time_elpased: 2.122
batch start
#iterations: 174
currently lose_sum: 87.93594443798065
time_elpased: 2.144
batch start
#iterations: 175
currently lose_sum: 87.56775045394897
time_elpased: 2.111
batch start
#iterations: 176
currently lose_sum: 88.13718372583389
time_elpased: 2.123
batch start
#iterations: 177
currently lose_sum: 87.96550565958023
time_elpased: 2.13
batch start
#iterations: 178
currently lose_sum: 87.09639412164688
time_elpased: 2.124
batch start
#iterations: 179
currently lose_sum: 87.51058948040009
time_elpased: 2.089
start validation test
0.682371134021
0.664964723357
0.737237546315
0.69923857868
0.682280483195
57.249
batch start
#iterations: 180
currently lose_sum: 87.46336215734482
time_elpased: 2.1
batch start
#iterations: 181
currently lose_sum: 87.33211904764175
time_elpased: 2.097
batch start
#iterations: 182
currently lose_sum: 87.24520444869995
time_elpased: 2.13
batch start
#iterations: 183
currently lose_sum: 86.35637992620468
time_elpased: 2.122
batch start
#iterations: 184
currently lose_sum: 87.95580953359604
time_elpased: 2.151
batch start
#iterations: 185
currently lose_sum: 87.52268183231354
time_elpased: 2.132
batch start
#iterations: 186
currently lose_sum: 87.55435562133789
time_elpased: 2.096
batch start
#iterations: 187
currently lose_sum: 87.11306828260422
time_elpased: 2.121
batch start
#iterations: 188
currently lose_sum: 87.86316412687302
time_elpased: 2.183
batch start
#iterations: 189
currently lose_sum: 87.19076758623123
time_elpased: 2.176
batch start
#iterations: 190
currently lose_sum: 86.83252567052841
time_elpased: 2.123
batch start
#iterations: 191
currently lose_sum: 87.33964270353317
time_elpased: 2.12
batch start
#iterations: 192
currently lose_sum: 86.98987859487534
time_elpased: 2.119
batch start
#iterations: 193
currently lose_sum: 87.44383800029755
time_elpased: 2.132
batch start
#iterations: 194
currently lose_sum: 86.62289386987686
time_elpased: 2.103
batch start
#iterations: 195
currently lose_sum: 87.44758689403534
time_elpased: 2.143
batch start
#iterations: 196
currently lose_sum: 87.63317209482193
time_elpased: 2.105
batch start
#iterations: 197
currently lose_sum: 86.59341084957123
time_elpased: 2.143
batch start
#iterations: 198
currently lose_sum: 86.33968186378479
time_elpased: 2.147
batch start
#iterations: 199
currently lose_sum: 86.3081825375557
time_elpased: 2.129
start validation test
0.654072164948
0.668915120854
0.612391930836
0.639406802429
0.654141029441
58.522
batch start
#iterations: 200
currently lose_sum: 87.03883618116379
time_elpased: 2.133
batch start
#iterations: 201
currently lose_sum: 86.23493182659149
time_elpased: 2.106
batch start
#iterations: 202
currently lose_sum: 86.6706947684288
time_elpased: 2.1
batch start
#iterations: 203
currently lose_sum: 87.48801249265671
time_elpased: 2.08
batch start
#iterations: 204
currently lose_sum: 87.70655679702759
time_elpased: 2.117
batch start
#iterations: 205
currently lose_sum: 86.08421385288239
time_elpased: 2.154
batch start
#iterations: 206
currently lose_sum: 87.1777446269989
time_elpased: 2.102
batch start
#iterations: 207
currently lose_sum: 87.11157619953156
time_elpased: 2.154
batch start
#iterations: 208
currently lose_sum: 86.55375564098358
time_elpased: 2.156
batch start
#iterations: 209
currently lose_sum: 86.97775685787201
time_elpased: 2.087
batch start
#iterations: 210
currently lose_sum: 87.15108746290207
time_elpased: 2.134
batch start
#iterations: 211
currently lose_sum: 86.3751482963562
time_elpased: 2.155
batch start
#iterations: 212
currently lose_sum: 86.57898610830307
time_elpased: 2.153
batch start
#iterations: 213
currently lose_sum: 86.8097876906395
time_elpased: 2.115
batch start
#iterations: 214
currently lose_sum: 86.33872997760773
time_elpased: 2.135
batch start
#iterations: 215
currently lose_sum: 85.98410934209824
time_elpased: 2.136
batch start
#iterations: 216
currently lose_sum: 85.94711893796921
time_elpased: 2.107
batch start
#iterations: 217
currently lose_sum: 85.88981741666794
time_elpased: 2.159
batch start
#iterations: 218
currently lose_sum: 87.16705203056335
time_elpased: 2.124
batch start
#iterations: 219
currently lose_sum: 86.9408106803894
time_elpased: 2.12
start validation test
0.623865979381
0.688718988922
0.454302181968
0.547472868217
0.624146134354
60.353
batch start
#iterations: 220
currently lose_sum: 86.08921253681183
time_elpased: 2.105
batch start
#iterations: 221
currently lose_sum: 85.75274091959
time_elpased: 2.14
batch start
#iterations: 222
currently lose_sum: 85.6362674832344
time_elpased: 2.147
batch start
#iterations: 223
currently lose_sum: 85.88906216621399
time_elpased: 2.103
batch start
#iterations: 224
currently lose_sum: 85.50950586795807
time_elpased: 2.138
batch start
#iterations: 225
currently lose_sum: 86.28601664304733
time_elpased: 2.167
batch start
#iterations: 226
currently lose_sum: 85.96334007382393
time_elpased: 2.177
batch start
#iterations: 227
currently lose_sum: 85.85050463676453
time_elpased: 2.152
batch start
#iterations: 228
currently lose_sum: 85.9015576839447
time_elpased: 2.115
batch start
#iterations: 229
currently lose_sum: 85.77665609121323
time_elpased: 2.09
batch start
#iterations: 230
currently lose_sum: 86.40370750427246
time_elpased: 2.129
batch start
#iterations: 231
currently lose_sum: 85.94027322530746
time_elpased: 2.108
batch start
#iterations: 232
currently lose_sum: 86.06340396404266
time_elpased: 2.112
batch start
#iterations: 233
currently lose_sum: 86.75235724449158
time_elpased: 2.119
batch start
#iterations: 234
currently lose_sum: 86.70372873544693
time_elpased: 2.147
batch start
#iterations: 235
currently lose_sum: 86.87945359945297
time_elpased: 2.123
batch start
#iterations: 236
currently lose_sum: 85.49449640512466
time_elpased: 2.093
batch start
#iterations: 237
currently lose_sum: 86.1393466591835
time_elpased: 2.143
batch start
#iterations: 238
currently lose_sum: 86.77130627632141
time_elpased: 2.15
batch start
#iterations: 239
currently lose_sum: 85.91894418001175
time_elpased: 2.132
start validation test
0.661237113402
0.681398569121
0.607760395224
0.642476335546
0.661325468162
58.645
batch start
#iterations: 240
currently lose_sum: 85.5405952334404
time_elpased: 2.094
batch start
#iterations: 241
currently lose_sum: 86.03900319337845
time_elpased: 2.113
batch start
#iterations: 242
currently lose_sum: 85.57885599136353
time_elpased: 2.1
batch start
#iterations: 243
currently lose_sum: 85.45171111822128
time_elpased: 2.13
batch start
#iterations: 244
currently lose_sum: 85.36937290430069
time_elpased: 2.13
batch start
#iterations: 245
currently lose_sum: 85.52150332927704
time_elpased: 2.152
batch start
#iterations: 246
currently lose_sum: 84.95399349927902
time_elpased: 2.115
batch start
#iterations: 247
currently lose_sum: 85.90406239032745
time_elpased: 2.146
batch start
#iterations: 248
currently lose_sum: 86.2310956120491
time_elpased: 2.103
batch start
#iterations: 249
currently lose_sum: 86.19912242889404
time_elpased: 2.109
batch start
#iterations: 250
currently lose_sum: 85.19985431432724
time_elpased: 2.069
batch start
#iterations: 251
currently lose_sum: 85.12185090780258
time_elpased: 2.12
batch start
#iterations: 252
currently lose_sum: 86.41570621728897
time_elpased: 2.109
batch start
#iterations: 253
currently lose_sum: 85.15745770931244
time_elpased: 2.121
batch start
#iterations: 254
currently lose_sum: 84.09500443935394
time_elpased: 2.118
batch start
#iterations: 255
currently lose_sum: 84.88961029052734
time_elpased: 2.132
batch start
#iterations: 256
currently lose_sum: 85.53747594356537
time_elpased: 2.103
batch start
#iterations: 257
currently lose_sum: 85.03777432441711
time_elpased: 2.11
batch start
#iterations: 258
currently lose_sum: 85.77172034978867
time_elpased: 2.088
batch start
#iterations: 259
currently lose_sum: 85.40441197156906
time_elpased: 2.115
start validation test
0.641701030928
0.681931833136
0.533347056402
0.598556165175
0.64188005443
59.671
batch start
#iterations: 260
currently lose_sum: 85.72014111280441
time_elpased: 2.124
batch start
#iterations: 261
currently lose_sum: 85.41916787624359
time_elpased: 2.08
batch start
#iterations: 262
currently lose_sum: 85.13987267017365
time_elpased: 2.105
batch start
#iterations: 263
currently lose_sum: 85.18350717425346
time_elpased: 2.114
batch start
#iterations: 264
currently lose_sum: 85.6213276386261
time_elpased: 2.145
batch start
#iterations: 265
currently lose_sum: 85.5259752869606
time_elpased: 2.112
batch start
#iterations: 266
currently lose_sum: 85.11449739336967
time_elpased: 2.124
batch start
#iterations: 267
currently lose_sum: 85.37486243247986
time_elpased: 2.136
batch start
#iterations: 268
currently lose_sum: 85.07228857278824
time_elpased: 2.121
batch start
#iterations: 269
currently lose_sum: 85.35565811395645
time_elpased: 2.121
batch start
#iterations: 270
currently lose_sum: 85.60321593284607
time_elpased: 2.092
batch start
#iterations: 271
currently lose_sum: 86.23836648464203
time_elpased: 2.136
batch start
#iterations: 272
currently lose_sum: 85.57172363996506
time_elpased: 2.155
batch start
#iterations: 273
currently lose_sum: 84.53633385896683
time_elpased: 2.124
batch start
#iterations: 274
currently lose_sum: 84.3556656241417
time_elpased: 2.064
batch start
#iterations: 275
currently lose_sum: 85.28421103954315
time_elpased: 2.139
batch start
#iterations: 276
currently lose_sum: 85.16411471366882
time_elpased: 2.113
batch start
#iterations: 277
currently lose_sum: 84.51787054538727
time_elpased: 2.181
batch start
#iterations: 278
currently lose_sum: 84.35259354114532
time_elpased: 2.109
batch start
#iterations: 279
currently lose_sum: 84.56555598974228
time_elpased: 2.13
start validation test
0.68206185567
0.652065832333
0.782935364347
0.711533065195
0.681895191467
57.597
batch start
#iterations: 280
currently lose_sum: 84.29541540145874
time_elpased: 2.095
batch start
#iterations: 281
currently lose_sum: 84.8006237745285
time_elpased: 2.124
batch start
#iterations: 282
currently lose_sum: 84.48501175642014
time_elpased: 2.145
batch start
#iterations: 283
currently lose_sum: 84.93084394931793
time_elpased: 2.174
batch start
#iterations: 284
currently lose_sum: 84.08486139774323
time_elpased: 2.1
batch start
#iterations: 285
currently lose_sum: 84.51504492759705
time_elpased: 2.144
batch start
#iterations: 286
currently lose_sum: 84.34153336286545
time_elpased: 2.106
batch start
#iterations: 287
currently lose_sum: 84.08167016506195
time_elpased: 2.086
batch start
#iterations: 288
currently lose_sum: 84.6550367474556
time_elpased: 2.121
batch start
#iterations: 289
currently lose_sum: 84.2707884311676
time_elpased: 2.126
batch start
#iterations: 290
currently lose_sum: 85.3503929078579
time_elpased: 2.093
batch start
#iterations: 291
currently lose_sum: 85.23940986394882
time_elpased: 2.125
batch start
#iterations: 292
currently lose_sum: 83.7661565542221
time_elpased: 2.088
batch start
#iterations: 293
currently lose_sum: 84.22280323505402
time_elpased: 2.142
batch start
#iterations: 294
currently lose_sum: 84.34927582740784
time_elpased: 2.145
batch start
#iterations: 295
currently lose_sum: 84.54779160022736
time_elpased: 2.113
batch start
#iterations: 296
currently lose_sum: 85.16319197416306
time_elpased: 2.085
batch start
#iterations: 297
currently lose_sum: 84.23928767442703
time_elpased: 2.083
batch start
#iterations: 298
currently lose_sum: 84.17570126056671
time_elpased: 2.045
batch start
#iterations: 299
currently lose_sum: 84.81645208597183
time_elpased: 2.13
start validation test
0.665360824742
0.642857142857
0.746603540552
0.690857142857
0.665226594729
58.556
batch start
#iterations: 300
currently lose_sum: 84.36242496967316
time_elpased: 2.077
batch start
#iterations: 301
currently lose_sum: 83.97933357954025
time_elpased: 2.162
batch start
#iterations: 302
currently lose_sum: 83.72207802534103
time_elpased: 2.132
batch start
#iterations: 303
currently lose_sum: 84.68012166023254
time_elpased: 2.134
batch start
#iterations: 304
currently lose_sum: 84.03974217176437
time_elpased: 2.086
batch start
#iterations: 305
currently lose_sum: 84.33276844024658
time_elpased: 2.184
batch start
#iterations: 306
currently lose_sum: 84.93966007232666
time_elpased: 2.117
batch start
#iterations: 307
currently lose_sum: 83.57850506901741
time_elpased: 2.194
batch start
#iterations: 308
currently lose_sum: 84.74975740909576
time_elpased: 2.11
batch start
#iterations: 309
currently lose_sum: 83.45554411411285
time_elpased: 2.114
batch start
#iterations: 310
currently lose_sum: 84.06486350297928
time_elpased: 2.11
batch start
#iterations: 311
currently lose_sum: 84.14237576723099
time_elpased: 2.17
batch start
#iterations: 312
currently lose_sum: 83.9957914352417
time_elpased: 2.12
batch start
#iterations: 313
currently lose_sum: 84.51711976528168
time_elpased: 2.09
batch start
#iterations: 314
currently lose_sum: 84.2196534872055
time_elpased: 2.103
batch start
#iterations: 315
currently lose_sum: 83.80023145675659
time_elpased: 2.128
batch start
#iterations: 316
currently lose_sum: 84.70489972829819
time_elpased: 2.13
batch start
#iterations: 317
currently lose_sum: 83.4742539525032
time_elpased: 2.138
batch start
#iterations: 318
currently lose_sum: 84.4116051197052
time_elpased: 2.132
batch start
#iterations: 319
currently lose_sum: 83.67148959636688
time_elpased: 2.118
start validation test
0.643917525773
0.671764130169
0.5651502676
0.613862493013
0.644047665811
59.619
batch start
#iterations: 320
currently lose_sum: 83.99738901853561
time_elpased: 2.181
batch start
#iterations: 321
currently lose_sum: 84.65555340051651
time_elpased: 2.113
batch start
#iterations: 322
currently lose_sum: 84.81093531847
time_elpased: 2.005
batch start
#iterations: 323
currently lose_sum: 83.62307566404343
time_elpased: 2.051
batch start
#iterations: 324
currently lose_sum: 83.58520823717117
time_elpased: 2.101
batch start
#iterations: 325
currently lose_sum: 83.92849856615067
time_elpased: 2.07
batch start
#iterations: 326
currently lose_sum: 83.96248260140419
time_elpased: 2.019
batch start
#iterations: 327
currently lose_sum: 84.16855478286743
time_elpased: 2.055
batch start
#iterations: 328
currently lose_sum: 83.15038645267487
time_elpased: 2.004
batch start
#iterations: 329
currently lose_sum: 83.58255285024643
time_elpased: 2.103
batch start
#iterations: 330
currently lose_sum: 82.0790598988533
time_elpased: 2.061
batch start
#iterations: 331
currently lose_sum: 83.44186264276505
time_elpased: 2.051
batch start
#iterations: 332
currently lose_sum: 83.53807032108307
time_elpased: 2.031
batch start
#iterations: 333
currently lose_sum: 84.34960734844208
time_elpased: 2.069
batch start
#iterations: 334
currently lose_sum: 83.91424778103828
time_elpased: 2.021
batch start
#iterations: 335
currently lose_sum: 83.78686159849167
time_elpased: 2.01
batch start
#iterations: 336
currently lose_sum: 82.73068207502365
time_elpased: 2.046
batch start
#iterations: 337
currently lose_sum: 84.067442715168
time_elpased: 2.076
batch start
#iterations: 338
currently lose_sum: 84.47881609201431
time_elpased: 2.045
batch start
#iterations: 339
currently lose_sum: 83.43674767017365
time_elpased: 2.112
start validation test
0.639226804124
0.679529536144
0.529230135858
0.5950355841
0.6394085417
60.278
batch start
#iterations: 340
currently lose_sum: 82.82065576314926
time_elpased: 2.108
batch start
#iterations: 341
currently lose_sum: 83.68176454305649
time_elpased: 1.971
batch start
#iterations: 342
currently lose_sum: 84.54491230845451
time_elpased: 2.016
batch start
#iterations: 343
currently lose_sum: 83.43108806014061
time_elpased: 2.025
batch start
#iterations: 344
currently lose_sum: 83.17675042152405
time_elpased: 2.083
batch start
#iterations: 345
currently lose_sum: 83.2317242026329
time_elpased: 2.069
batch start
#iterations: 346
currently lose_sum: 82.51238244771957
time_elpased: 2.02
batch start
#iterations: 347
currently lose_sum: 83.47095662355423
time_elpased: 2.036
batch start
#iterations: 348
currently lose_sum: 83.6941685974598
time_elpased: 1.962
batch start
#iterations: 349
currently lose_sum: 83.67247384786606
time_elpased: 2.028
batch start
#iterations: 350
currently lose_sum: 84.63582462072372
time_elpased: 2.031
batch start
#iterations: 351
currently lose_sum: 83.13228237628937
time_elpased: 2.021
batch start
#iterations: 352
currently lose_sum: 84.16859406232834
time_elpased: 2.029
batch start
#iterations: 353
currently lose_sum: 83.98812460899353
time_elpased: 2.051
batch start
#iterations: 354
currently lose_sum: 83.8523171544075
time_elpased: 2.007
batch start
#iterations: 355
currently lose_sum: 83.78257519006729
time_elpased: 2.045
batch start
#iterations: 356
currently lose_sum: 83.52861207723618
time_elpased: 2.015
batch start
#iterations: 357
currently lose_sum: 82.78597193956375
time_elpased: 2.022
batch start
#iterations: 358
currently lose_sum: 82.6956270635128
time_elpased: 2.018
batch start
#iterations: 359
currently lose_sum: 83.08587485551834
time_elpased: 2.015
start validation test
0.649329896907
0.68286252354
0.559798270893
0.615236694757
0.64947782194
61.150
batch start
#iterations: 360
currently lose_sum: 82.68345493078232
time_elpased: 2.015
batch start
#iterations: 361
currently lose_sum: 82.78968885540962
time_elpased: 2.039
batch start
#iterations: 362
currently lose_sum: 83.63390845060349
time_elpased: 2.008
batch start
#iterations: 363
currently lose_sum: 84.51570278406143
time_elpased: 2.098
batch start
#iterations: 364
currently lose_sum: 83.31494575738907
time_elpased: 2.042
batch start
#iterations: 365
currently lose_sum: 82.78195250034332
time_elpased: 2.056
batch start
#iterations: 366
currently lose_sum: 83.4131269454956
time_elpased: 2.064
batch start
#iterations: 367
currently lose_sum: 82.74461069703102
time_elpased: 2.046
batch start
#iterations: 368
currently lose_sum: 84.43935632705688
time_elpased: 2.037
batch start
#iterations: 369
currently lose_sum: 83.28643071651459
time_elpased: 2.025
batch start
#iterations: 370
currently lose_sum: 82.52187216281891
time_elpased: 2.021
batch start
#iterations: 371
currently lose_sum: 83.37401521205902
time_elpased: 2.069
batch start
#iterations: 372
currently lose_sum: 83.55331844091415
time_elpased: 2.046
batch start
#iterations: 373
currently lose_sum: 83.05444252490997
time_elpased: 2.027
batch start
#iterations: 374
currently lose_sum: 82.6572031378746
time_elpased: 2.089
batch start
#iterations: 375
currently lose_sum: 83.04584482312202
time_elpased: 2.073
batch start
#iterations: 376
currently lose_sum: 82.80088764429092
time_elpased: 2.021
batch start
#iterations: 377
currently lose_sum: 83.33883449435234
time_elpased: 2.022
batch start
#iterations: 378
currently lose_sum: 82.48004975914955
time_elpased: 2.046
batch start
#iterations: 379
currently lose_sum: 83.61101907491684
time_elpased: 2.052
start validation test
0.659845360825
0.665358090186
0.645430218197
0.655242672797
0.659869177665
58.794
batch start
#iterations: 380
currently lose_sum: 82.57048052549362
time_elpased: 2.05
batch start
#iterations: 381
currently lose_sum: 82.3038255572319
time_elpased: 2.06
batch start
#iterations: 382
currently lose_sum: 82.87682911753654
time_elpased: 2.009
batch start
#iterations: 383
currently lose_sum: 82.66321676969528
time_elpased: 1.895
batch start
#iterations: 384
currently lose_sum: 82.59279444813728
time_elpased: 1.92
batch start
#iterations: 385
currently lose_sum: 82.84864395856857
time_elpased: 1.911
batch start
#iterations: 386
currently lose_sum: 82.67412799596786
time_elpased: 1.936
batch start
#iterations: 387
currently lose_sum: 82.18524986505508
time_elpased: 1.862
batch start
#iterations: 388
currently lose_sum: 82.44529840350151
time_elpased: 1.926
batch start
#iterations: 389
currently lose_sum: 82.41153180599213
time_elpased: 1.925
batch start
#iterations: 390
currently lose_sum: 82.57426109910011
time_elpased: 1.908
batch start
#iterations: 391
currently lose_sum: 82.21028405427933
time_elpased: 1.936
batch start
#iterations: 392
currently lose_sum: 82.59897437691689
time_elpased: 1.914
batch start
#iterations: 393
currently lose_sum: 81.91516989469528
time_elpased: 1.947
batch start
#iterations: 394
currently lose_sum: 82.89551055431366
time_elpased: 1.927
batch start
#iterations: 395
currently lose_sum: 82.41720420122147
time_elpased: 1.898
batch start
#iterations: 396
currently lose_sum: 82.49985867738724
time_elpased: 1.924
batch start
#iterations: 397
currently lose_sum: 83.0930969119072
time_elpased: 1.969
batch start
#iterations: 398
currently lose_sum: 81.50299084186554
time_elpased: 1.968
batch start
#iterations: 399
currently lose_sum: 82.9402112364769
time_elpased: 1.978
start validation test
0.619329896907
0.687711386697
0.439481268012
0.536263736264
0.61962704458
62.402
acc: 0.696
pre: 0.679
rec: 0.746
F1: 0.711
auc: 0.696
