start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.52653121948242
time_elpased: 2.654
batch start
#iterations: 1
currently lose_sum: 100.26403892040253
time_elpased: 2.62
batch start
#iterations: 2
currently lose_sum: 99.94566649198532
time_elpased: 2.656
batch start
#iterations: 3
currently lose_sum: 99.8889251947403
time_elpased: 2.622
batch start
#iterations: 4
currently lose_sum: 99.5548843741417
time_elpased: 2.599
batch start
#iterations: 5
currently lose_sum: 99.32775616645813
time_elpased: 2.634
batch start
#iterations: 6
currently lose_sum: 99.09592247009277
time_elpased: 2.617
batch start
#iterations: 7
currently lose_sum: 98.90733468532562
time_elpased: 2.587
batch start
#iterations: 8
currently lose_sum: 98.68610751628876
time_elpased: 2.606
batch start
#iterations: 9
currently lose_sum: 98.20700961351395
time_elpased: 2.597
batch start
#iterations: 10
currently lose_sum: 98.554170191288
time_elpased: 2.603
batch start
#iterations: 11
currently lose_sum: 98.44591027498245
time_elpased: 2.591
batch start
#iterations: 12
currently lose_sum: 97.86585420370102
time_elpased: 2.614
batch start
#iterations: 13
currently lose_sum: 97.56757801771164
time_elpased: 2.575
batch start
#iterations: 14
currently lose_sum: 97.61968213319778
time_elpased: 2.607
batch start
#iterations: 15
currently lose_sum: 97.58445644378662
time_elpased: 2.653
batch start
#iterations: 16
currently lose_sum: 97.5581191778183
time_elpased: 2.646
batch start
#iterations: 17
currently lose_sum: 97.27388346195221
time_elpased: 2.604
batch start
#iterations: 18
currently lose_sum: 96.98150825500488
time_elpased: 2.621
batch start
#iterations: 19
currently lose_sum: 97.00420939922333
time_elpased: 2.659
start validation test
0.619742268041
0.58551381377
0.824431408871
0.684730116672
0.619382904683
63.512
batch start
#iterations: 20
currently lose_sum: 96.64169663190842
time_elpased: 2.626
batch start
#iterations: 21
currently lose_sum: 96.69574230909348
time_elpased: 2.598
batch start
#iterations: 22
currently lose_sum: 96.44789862632751
time_elpased: 2.628
batch start
#iterations: 23
currently lose_sum: 96.35381424427032
time_elpased: 2.631
batch start
#iterations: 24
currently lose_sum: 95.94887638092041
time_elpased: 2.601
batch start
#iterations: 25
currently lose_sum: 96.33966988325119
time_elpased: 2.601
batch start
#iterations: 26
currently lose_sum: 96.2312582731247
time_elpased: 2.615
batch start
#iterations: 27
currently lose_sum: 96.2514985203743
time_elpased: 2.573
batch start
#iterations: 28
currently lose_sum: 95.72888970375061
time_elpased: 2.633
batch start
#iterations: 29
currently lose_sum: 95.57431125640869
time_elpased: 2.569
batch start
#iterations: 30
currently lose_sum: 95.61900746822357
time_elpased: 2.596
batch start
#iterations: 31
currently lose_sum: 95.60493475198746
time_elpased: 2.629
batch start
#iterations: 32
currently lose_sum: 95.40205162763596
time_elpased: 2.577
batch start
#iterations: 33
currently lose_sum: 94.90146398544312
time_elpased: 2.592
batch start
#iterations: 34
currently lose_sum: 95.27944898605347
time_elpased: 2.584
batch start
#iterations: 35
currently lose_sum: 94.98754560947418
time_elpased: 2.565
batch start
#iterations: 36
currently lose_sum: 94.57573312520981
time_elpased: 2.588
batch start
#iterations: 37
currently lose_sum: 94.74087262153625
time_elpased: 2.624
batch start
#iterations: 38
currently lose_sum: 94.82991081476212
time_elpased: 2.629
batch start
#iterations: 39
currently lose_sum: 94.2932841181755
time_elpased: 2.612
start validation test
0.62587628866
0.603154627066
0.739837398374
0.664540580514
0.625676212354
62.787
batch start
#iterations: 40
currently lose_sum: 94.3699107170105
time_elpased: 2.642
batch start
#iterations: 41
currently lose_sum: 94.30794459581375
time_elpased: 2.603
batch start
#iterations: 42
currently lose_sum: 93.95174878835678
time_elpased: 2.578
batch start
#iterations: 43
currently lose_sum: 93.94261479377747
time_elpased: 2.604
batch start
#iterations: 44
currently lose_sum: 93.84963822364807
time_elpased: 2.548
batch start
#iterations: 45
currently lose_sum: 93.93491423130035
time_elpased: 2.57
batch start
#iterations: 46
currently lose_sum: 93.91955381631851
time_elpased: 2.591
batch start
#iterations: 47
currently lose_sum: 94.24393010139465
time_elpased: 2.66
batch start
#iterations: 48
currently lose_sum: 94.15957683324814
time_elpased: 2.638
batch start
#iterations: 49
currently lose_sum: 93.68316751718521
time_elpased: 2.587
batch start
#iterations: 50
currently lose_sum: 92.99391150474548
time_elpased: 2.579
batch start
#iterations: 51
currently lose_sum: 93.16819489002228
time_elpased: 2.622
batch start
#iterations: 52
currently lose_sum: 93.49570387601852
time_elpased: 2.586
batch start
#iterations: 53
currently lose_sum: 92.65909367799759
time_elpased: 2.626
batch start
#iterations: 54
currently lose_sum: 93.0771222114563
time_elpased: 2.646
batch start
#iterations: 55
currently lose_sum: 92.87069529294968
time_elpased: 2.693
batch start
#iterations: 56
currently lose_sum: 92.44841814041138
time_elpased: 2.587
batch start
#iterations: 57
currently lose_sum: 92.707304418087
time_elpased: 2.633
batch start
#iterations: 58
currently lose_sum: 92.05827355384827
time_elpased: 2.617
batch start
#iterations: 59
currently lose_sum: 92.52315950393677
time_elpased: 2.647
start validation test
0.617783505155
0.630676657584
0.571678501595
0.599730094467
0.6178644496
62.789
batch start
#iterations: 60
currently lose_sum: 92.56696772575378
time_elpased: 2.604
batch start
#iterations: 61
currently lose_sum: 92.32872450351715
time_elpased: 2.6
batch start
#iterations: 62
currently lose_sum: 92.52031898498535
time_elpased: 2.559
batch start
#iterations: 63
currently lose_sum: 91.81811720132828
time_elpased: 2.666
batch start
#iterations: 64
currently lose_sum: 91.62090361118317
time_elpased: 2.646
batch start
#iterations: 65
currently lose_sum: 91.34881031513214
time_elpased: 2.554
batch start
#iterations: 66
currently lose_sum: 92.11035817861557
time_elpased: 2.608
batch start
#iterations: 67
currently lose_sum: 91.35995012521744
time_elpased: 2.592
batch start
#iterations: 68
currently lose_sum: 91.1755639910698
time_elpased: 2.643
batch start
#iterations: 69
currently lose_sum: 91.12393087148666
time_elpased: 2.587
batch start
#iterations: 70
currently lose_sum: 90.96623140573502
time_elpased: 2.59
batch start
#iterations: 71
currently lose_sum: 91.35592812299728
time_elpased: 2.554
batch start
#iterations: 72
currently lose_sum: 91.31065535545349
time_elpased: 2.593
batch start
#iterations: 73
currently lose_sum: 90.69842457771301
time_elpased: 2.542
batch start
#iterations: 74
currently lose_sum: 90.8154222369194
time_elpased: 2.637
batch start
#iterations: 75
currently lose_sum: 90.42313367128372
time_elpased: 2.57
batch start
#iterations: 76
currently lose_sum: 91.04509872198105
time_elpased: 2.614
batch start
#iterations: 77
currently lose_sum: 90.24254047870636
time_elpased: 2.59
batch start
#iterations: 78
currently lose_sum: 90.21832394599915
time_elpased: 2.639
batch start
#iterations: 79
currently lose_sum: 89.99541491270065
time_elpased: 2.619
start validation test
0.629020618557
0.646443514644
0.572398888546
0.607172097593
0.629120026737
64.066
batch start
#iterations: 80
currently lose_sum: 90.6050535440445
time_elpased: 2.652
batch start
#iterations: 81
currently lose_sum: 90.1907142996788
time_elpased: 2.655
batch start
#iterations: 82
currently lose_sum: 90.07016932964325
time_elpased: 2.58
batch start
#iterations: 83
currently lose_sum: 89.5116690993309
time_elpased: 2.643
batch start
#iterations: 84
currently lose_sum: 89.73787945508957
time_elpased: 2.663
batch start
#iterations: 85
currently lose_sum: 89.59527844190598
time_elpased: 2.678
batch start
#iterations: 86
currently lose_sum: 89.26989728212357
time_elpased: 2.602
batch start
#iterations: 87
currently lose_sum: 88.81573939323425
time_elpased: 2.577
batch start
#iterations: 88
currently lose_sum: 89.45243972539902
time_elpased: 2.604
batch start
#iterations: 89
currently lose_sum: 88.91045612096786
time_elpased: 2.577
batch start
#iterations: 90
currently lose_sum: 87.86495214700699
time_elpased: 2.583
batch start
#iterations: 91
currently lose_sum: 88.87046629190445
time_elpased: 2.65
batch start
#iterations: 92
currently lose_sum: 89.38453143835068
time_elpased: 2.626
batch start
#iterations: 93
currently lose_sum: 88.52957564592361
time_elpased: 2.625
batch start
#iterations: 94
currently lose_sum: 88.52329784631729
time_elpased: 2.625
batch start
#iterations: 95
currently lose_sum: 88.49441927671432
time_elpased: 2.666
batch start
#iterations: 96
currently lose_sum: 88.48452770709991
time_elpased: 2.619
batch start
#iterations: 97
currently lose_sum: 86.82653158903122
time_elpased: 2.577
batch start
#iterations: 98
currently lose_sum: 87.08628559112549
time_elpased: 2.634
batch start
#iterations: 99
currently lose_sum: 87.13606476783752
time_elpased: 2.639
start validation test
0.626443298969
0.632397084048
0.607080374601
0.619480178525
0.626477293569
64.354
batch start
#iterations: 100
currently lose_sum: 87.2383319735527
time_elpased: 2.606
batch start
#iterations: 101
currently lose_sum: 88.27819150686264
time_elpased: 2.556
batch start
#iterations: 102
currently lose_sum: 87.18727195262909
time_elpased: 2.595
batch start
#iterations: 103
currently lose_sum: 87.33245295286179
time_elpased: 2.575
batch start
#iterations: 104
currently lose_sum: 86.5771244764328
time_elpased: 2.598
batch start
#iterations: 105
currently lose_sum: 86.65377539396286
time_elpased: 2.622
batch start
#iterations: 106
currently lose_sum: 86.19047576189041
time_elpased: 2.551
batch start
#iterations: 107
currently lose_sum: 86.60887438058853
time_elpased: 2.643
batch start
#iterations: 108
currently lose_sum: 85.52844840288162
time_elpased: 2.602
batch start
#iterations: 109
currently lose_sum: 85.91029679775238
time_elpased: 2.556
batch start
#iterations: 110
currently lose_sum: 85.4096771478653
time_elpased: 2.622
batch start
#iterations: 111
currently lose_sum: 86.12171310186386
time_elpased: 2.603
batch start
#iterations: 112
currently lose_sum: 85.73399621248245
time_elpased: 2.59
batch start
#iterations: 113
currently lose_sum: 85.61653065681458
time_elpased: 2.628
batch start
#iterations: 114
currently lose_sum: 85.54473233222961
time_elpased: 2.591
batch start
#iterations: 115
currently lose_sum: 85.39842808246613
time_elpased: 2.586
batch start
#iterations: 116
currently lose_sum: 85.13006949424744
time_elpased: 2.638
batch start
#iterations: 117
currently lose_sum: 84.62815165519714
time_elpased: 2.654
batch start
#iterations: 118
currently lose_sum: 85.10231357812881
time_elpased: 2.574
batch start
#iterations: 119
currently lose_sum: 84.34091764688492
time_elpased: 2.598
start validation test
0.59824742268
0.622969689219
0.501286405269
0.555542883212
0.598417652702
68.143
batch start
#iterations: 120
currently lose_sum: 83.38483011722565
time_elpased: 2.634
batch start
#iterations: 121
currently lose_sum: 83.88825505971909
time_elpased: 2.637
batch start
#iterations: 122
currently lose_sum: 84.49141597747803
time_elpased: 2.58
batch start
#iterations: 123
currently lose_sum: 83.75433963537216
time_elpased: 2.627
batch start
#iterations: 124
currently lose_sum: 83.81987059116364
time_elpased: 2.65
batch start
#iterations: 125
currently lose_sum: 83.39624086022377
time_elpased: 2.674
batch start
#iterations: 126
currently lose_sum: 83.64048534631729
time_elpased: 2.609
batch start
#iterations: 127
currently lose_sum: 82.25489401817322
time_elpased: 2.608
batch start
#iterations: 128
currently lose_sum: 83.26431435346603
time_elpased: 2.622
batch start
#iterations: 129
currently lose_sum: 82.9167891740799
time_elpased: 2.628
batch start
#iterations: 130
currently lose_sum: 82.3993791937828
time_elpased: 2.694
batch start
#iterations: 131
currently lose_sum: 81.49152046442032
time_elpased: 2.741
batch start
#iterations: 132
currently lose_sum: 82.57590925693512
time_elpased: 2.633
batch start
#iterations: 133
currently lose_sum: 81.84072810411453
time_elpased: 2.613
batch start
#iterations: 134
currently lose_sum: 81.74615412950516
time_elpased: 2.634
batch start
#iterations: 135
currently lose_sum: 81.64962068200111
time_elpased: 2.622
batch start
#iterations: 136
currently lose_sum: 81.6226717531681
time_elpased: 2.694
batch start
#iterations: 137
currently lose_sum: 81.69870734214783
time_elpased: 2.639
batch start
#iterations: 138
currently lose_sum: 81.54944384098053
time_elpased: 2.621
batch start
#iterations: 139
currently lose_sum: 80.13477542996407
time_elpased: 2.6
start validation test
0.594381443299
0.621132669114
0.487599053206
0.546324589219
0.594568916255
73.644
batch start
#iterations: 140
currently lose_sum: 81.18695822358131
time_elpased: 2.671
batch start
#iterations: 141
currently lose_sum: 79.73818027973175
time_elpased: 2.562
batch start
#iterations: 142
currently lose_sum: 79.71345993876457
time_elpased: 2.625
batch start
#iterations: 143
currently lose_sum: 80.16151681542397
time_elpased: 2.649
batch start
#iterations: 144
currently lose_sum: 79.55988895893097
time_elpased: 2.659
batch start
#iterations: 145
currently lose_sum: 80.27060794830322
time_elpased: 2.645
batch start
#iterations: 146
currently lose_sum: 79.47802007198334
time_elpased: 2.652
batch start
#iterations: 147
currently lose_sum: 78.90378576517105
time_elpased: 2.653
batch start
#iterations: 148
currently lose_sum: 78.85928344726562
time_elpased: 2.659
batch start
#iterations: 149
currently lose_sum: 78.20948272943497
time_elpased: 2.645
batch start
#iterations: 150
currently lose_sum: 78.17779058218002
time_elpased: 2.677
batch start
#iterations: 151
currently lose_sum: 77.67864537239075
time_elpased: 2.584
batch start
#iterations: 152
currently lose_sum: 78.64946874976158
time_elpased: 2.657
batch start
#iterations: 153
currently lose_sum: 77.19975817203522
time_elpased: 2.626
batch start
#iterations: 154
currently lose_sum: 77.58730101585388
time_elpased: 2.585
batch start
#iterations: 155
currently lose_sum: 77.40172055363655
time_elpased: 2.637
batch start
#iterations: 156
currently lose_sum: 76.69503143429756
time_elpased: 2.573
batch start
#iterations: 157
currently lose_sum: 77.26064193248749
time_elpased: 2.593
batch start
#iterations: 158
currently lose_sum: 77.45700427889824
time_elpased: 2.648
batch start
#iterations: 159
currently lose_sum: 76.85088038444519
time_elpased: 2.575
start validation test
0.581958762887
0.633538308127
0.39230215087
0.484555739164
0.582291734322
87.253
batch start
#iterations: 160
currently lose_sum: 76.36739912629128
time_elpased: 2.623
batch start
#iterations: 161
currently lose_sum: 75.78534942865372
time_elpased: 2.635
batch start
#iterations: 162
currently lose_sum: 75.54354214668274
time_elpased: 2.608
batch start
#iterations: 163
currently lose_sum: 74.9960444867611
time_elpased: 2.599
batch start
#iterations: 164
currently lose_sum: 75.10642716288567
time_elpased: 2.675
batch start
#iterations: 165
currently lose_sum: 74.12116116285324
time_elpased: 2.595
batch start
#iterations: 166
currently lose_sum: 74.72123199701309
time_elpased: 2.584
batch start
#iterations: 167
currently lose_sum: 74.36816340684891
time_elpased: 2.56
batch start
#iterations: 168
currently lose_sum: 74.46730276942253
time_elpased: 2.572
batch start
#iterations: 169
currently lose_sum: 74.28903916478157
time_elpased: 2.645
batch start
#iterations: 170
currently lose_sum: 73.3156008720398
time_elpased: 2.62
batch start
#iterations: 171
currently lose_sum: 73.91375732421875
time_elpased: 2.643
batch start
#iterations: 172
currently lose_sum: 72.8793808221817
time_elpased: 2.626
batch start
#iterations: 173
currently lose_sum: 72.41470569372177
time_elpased: 2.573
batch start
#iterations: 174
currently lose_sum: 72.32509744167328
time_elpased: 2.555
batch start
#iterations: 175
currently lose_sum: 71.69766491651535
time_elpased: 2.643
batch start
#iterations: 176
currently lose_sum: 73.03533339500427
time_elpased: 2.631
batch start
#iterations: 177
currently lose_sum: 71.38121250271797
time_elpased: 2.6
batch start
#iterations: 178
currently lose_sum: 71.46090167760849
time_elpased: 2.591
batch start
#iterations: 179
currently lose_sum: 71.02149161696434
time_elpased: 2.62
start validation test
0.575
0.621292023731
0.387979829165
0.477668672791
0.575328342756
95.685
batch start
#iterations: 180
currently lose_sum: 71.27977699041367
time_elpased: 2.612
batch start
#iterations: 181
currently lose_sum: 70.6641760468483
time_elpased: 2.574
batch start
#iterations: 182
currently lose_sum: 70.77103954553604
time_elpased: 2.578
batch start
#iterations: 183
currently lose_sum: 69.37270849943161
time_elpased: 2.58
batch start
#iterations: 184
currently lose_sum: 70.76252147555351
time_elpased: 2.597
batch start
#iterations: 185
currently lose_sum: 69.47582903504372
time_elpased: 2.585
batch start
#iterations: 186
currently lose_sum: 68.73932179808617
time_elpased: 2.659
batch start
#iterations: 187
currently lose_sum: 69.54978835582733
time_elpased: 2.585
batch start
#iterations: 188
currently lose_sum: 68.89320075511932
time_elpased: 2.585
batch start
#iterations: 189
currently lose_sum: 69.01009917259216
time_elpased: 2.569
batch start
#iterations: 190
currently lose_sum: 68.18649545311928
time_elpased: 2.628
batch start
#iterations: 191
currently lose_sum: 68.43644881248474
time_elpased: 2.571
batch start
#iterations: 192
currently lose_sum: 68.34479257464409
time_elpased: 2.577
batch start
#iterations: 193
currently lose_sum: 68.90175411105156
time_elpased: 2.625
batch start
#iterations: 194
currently lose_sum: 67.64387521147728
time_elpased: 2.617
batch start
#iterations: 195
currently lose_sum: 67.40374848246574
time_elpased: 2.531
batch start
#iterations: 196
currently lose_sum: 67.28842625021935
time_elpased: 2.633
batch start
#iterations: 197
currently lose_sum: 66.53353202342987
time_elpased: 2.578
batch start
#iterations: 198
currently lose_sum: 66.03247272968292
time_elpased: 2.407
batch start
#iterations: 199
currently lose_sum: 65.85177010297775
time_elpased: 2.184
start validation test
0.562113402062
0.594319234332
0.396212822888
0.475455387465
0.562404666117
101.590
batch start
#iterations: 200
currently lose_sum: 67.65562981367111
time_elpased: 2.162
batch start
#iterations: 201
currently lose_sum: 66.20848941802979
time_elpased: 2.159
batch start
#iterations: 202
currently lose_sum: 64.89591127634048
time_elpased: 2.14
batch start
#iterations: 203
currently lose_sum: 64.5859015583992
time_elpased: 2.156
batch start
#iterations: 204
currently lose_sum: 64.28901299834251
time_elpased: 2.174
batch start
#iterations: 205
currently lose_sum: 65.54991519451141
time_elpased: 2.15
batch start
#iterations: 206
currently lose_sum: 65.94076383113861
time_elpased: 2.161
batch start
#iterations: 207
currently lose_sum: 64.43221309781075
time_elpased: 2.146
batch start
#iterations: 208
currently lose_sum: 63.26084780693054
time_elpased: 2.164
batch start
#iterations: 209
currently lose_sum: 63.56758961081505
time_elpased: 2.111
batch start
#iterations: 210
currently lose_sum: 63.33341780304909
time_elpased: 2.202
batch start
#iterations: 211
currently lose_sum: 62.94478839635849
time_elpased: 2.198
batch start
#iterations: 212
currently lose_sum: 62.93362385034561
time_elpased: 2.239
batch start
#iterations: 213
currently lose_sum: 62.91472202539444
time_elpased: 2.232
batch start
#iterations: 214
currently lose_sum: 63.21649357676506
time_elpased: 2.224
batch start
#iterations: 215
currently lose_sum: 62.463115602731705
time_elpased: 2.196
batch start
#iterations: 216
currently lose_sum: 62.884565979242325
time_elpased: 2.186
batch start
#iterations: 217
currently lose_sum: 61.865260541439056
time_elpased: 2.186
batch start
#iterations: 218
currently lose_sum: 61.233805418014526
time_elpased: 2.233
batch start
#iterations: 219
currently lose_sum: 61.0212644636631
time_elpased: 2.21
start validation test
0.554639175258
0.614452709883
0.297519810641
0.40091526834
0.55509058796
113.168
batch start
#iterations: 220
currently lose_sum: 61.613917738199234
time_elpased: 2.23
batch start
#iterations: 221
currently lose_sum: 60.698651880025864
time_elpased: 2.207
batch start
#iterations: 222
currently lose_sum: 60.85109934210777
time_elpased: 2.255
batch start
#iterations: 223
currently lose_sum: 60.126715540885925
time_elpased: 2.21
batch start
#iterations: 224
currently lose_sum: 60.7456229031086
time_elpased: 2.21
batch start
#iterations: 225
currently lose_sum: 60.05668485164642
time_elpased: 2.228
batch start
#iterations: 226
currently lose_sum: 58.602743268013
time_elpased: 2.184
batch start
#iterations: 227
currently lose_sum: 59.81437963247299
time_elpased: 2.208
batch start
#iterations: 228
currently lose_sum: 59.611132860183716
time_elpased: 2.211
batch start
#iterations: 229
currently lose_sum: 59.56637763977051
time_elpased: 2.215
batch start
#iterations: 230
currently lose_sum: 59.130444407463074
time_elpased: 2.202
batch start
#iterations: 231
currently lose_sum: 59.028084337711334
time_elpased: 2.215
batch start
#iterations: 232
currently lose_sum: nan
time_elpased: 2.237
train finish final lose is: nan
acc: 0.622
pre: 0.600
rec: 0.736
F1: 0.661
auc: 0.622
