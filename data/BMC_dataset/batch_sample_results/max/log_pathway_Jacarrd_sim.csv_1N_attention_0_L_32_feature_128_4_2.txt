start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.44723385572433
time_elpased: 2.863
batch start
#iterations: 1
currently lose_sum: 100.08845269680023
time_elpased: 2.814
batch start
#iterations: 2
currently lose_sum: 100.06020432710648
time_elpased: 2.77
batch start
#iterations: 3
currently lose_sum: 99.75356084108353
time_elpased: 2.799
batch start
#iterations: 4
currently lose_sum: 99.6385914683342
time_elpased: 2.848
batch start
#iterations: 5
currently lose_sum: 99.36626201868057
time_elpased: 2.931
batch start
#iterations: 6
currently lose_sum: 98.91395074129105
time_elpased: 2.834
batch start
#iterations: 7
currently lose_sum: 99.00506699085236
time_elpased: 2.821
batch start
#iterations: 8
currently lose_sum: 98.43946498632431
time_elpased: 2.834
batch start
#iterations: 9
currently lose_sum: 98.53630065917969
time_elpased: 2.89
batch start
#iterations: 10
currently lose_sum: 98.43664413690567
time_elpased: 2.867
batch start
#iterations: 11
currently lose_sum: 98.29713827371597
time_elpased: 2.96
batch start
#iterations: 12
currently lose_sum: 97.94035357236862
time_elpased: 2.826
batch start
#iterations: 13
currently lose_sum: 97.81842774152756
time_elpased: 2.815
batch start
#iterations: 14
currently lose_sum: 97.5185974240303
time_elpased: 2.79
batch start
#iterations: 15
currently lose_sum: 97.45404088497162
time_elpased: 2.733
batch start
#iterations: 16
currently lose_sum: 97.25273942947388
time_elpased: 2.738
batch start
#iterations: 17
currently lose_sum: 97.31266129016876
time_elpased: 2.812
batch start
#iterations: 18
currently lose_sum: 96.95177155733109
time_elpased: 2.802
batch start
#iterations: 19
currently lose_sum: 96.90567570924759
time_elpased: 2.823
start validation test
0.632525773196
0.605563713493
0.763918905012
0.67558589306
0.632295092287
63.102
batch start
#iterations: 20
currently lose_sum: 96.4865534901619
time_elpased: 2.995
batch start
#iterations: 21
currently lose_sum: 96.55016148090363
time_elpased: 2.927
batch start
#iterations: 22
currently lose_sum: 96.48740118741989
time_elpased: 2.815
batch start
#iterations: 23
currently lose_sum: 96.35822033882141
time_elpased: 2.74
batch start
#iterations: 24
currently lose_sum: 95.95531332492828
time_elpased: 2.857
batch start
#iterations: 25
currently lose_sum: 96.2808740735054
time_elpased: 2.849
batch start
#iterations: 26
currently lose_sum: 96.09154289960861
time_elpased: 2.815
batch start
#iterations: 27
currently lose_sum: 96.00022077560425
time_elpased: 2.815
batch start
#iterations: 28
currently lose_sum: 95.41943448781967
time_elpased: 2.808
batch start
#iterations: 29
currently lose_sum: 95.54067730903625
time_elpased: 2.842
batch start
#iterations: 30
currently lose_sum: 95.62633579969406
time_elpased: 2.863
batch start
#iterations: 31
currently lose_sum: 95.83637821674347
time_elpased: 2.872
batch start
#iterations: 32
currently lose_sum: 94.93851149082184
time_elpased: 2.85
batch start
#iterations: 33
currently lose_sum: 95.3850405216217
time_elpased: 2.852
batch start
#iterations: 34
currently lose_sum: 95.21886622905731
time_elpased: 2.85
batch start
#iterations: 35
currently lose_sum: 95.2972275018692
time_elpased: 2.809
batch start
#iterations: 36
currently lose_sum: 95.24750465154648
time_elpased: 2.694
batch start
#iterations: 37
currently lose_sum: 94.91677045822144
time_elpased: 2.841
batch start
#iterations: 38
currently lose_sum: 94.7444999217987
time_elpased: 2.863
batch start
#iterations: 39
currently lose_sum: 94.73321551084518
time_elpased: 2.828
start validation test
0.607010309278
0.59000602047
0.705979211691
0.642803598201
0.606836554105
63.039
batch start
#iterations: 40
currently lose_sum: 94.31322103738785
time_elpased: 2.851
batch start
#iterations: 41
currently lose_sum: 94.34312891960144
time_elpased: 2.833
batch start
#iterations: 42
currently lose_sum: 94.10626977682114
time_elpased: 2.862
batch start
#iterations: 43
currently lose_sum: 94.58030438423157
time_elpased: 2.834
batch start
#iterations: 44
currently lose_sum: 94.2316278219223
time_elpased: 2.807
batch start
#iterations: 45
currently lose_sum: 94.02536791563034
time_elpased: 2.757
batch start
#iterations: 46
currently lose_sum: 93.77685928344727
time_elpased: 2.877
batch start
#iterations: 47
currently lose_sum: 93.38606905937195
time_elpased: 2.849
batch start
#iterations: 48
currently lose_sum: 93.79980039596558
time_elpased: 2.824
batch start
#iterations: 49
currently lose_sum: 93.66070932149887
time_elpased: 2.834
batch start
#iterations: 50
currently lose_sum: 93.8479934334755
time_elpased: 2.856
batch start
#iterations: 51
currently lose_sum: 93.47577226161957
time_elpased: 2.846
batch start
#iterations: 52
currently lose_sum: 93.92288571596146
time_elpased: 2.8
batch start
#iterations: 53
currently lose_sum: 93.51897358894348
time_elpased: 2.82
batch start
#iterations: 54
currently lose_sum: 93.12921053171158
time_elpased: 2.887
batch start
#iterations: 55
currently lose_sum: 92.80127489566803
time_elpased: 2.88
batch start
#iterations: 56
currently lose_sum: 93.27888232469559
time_elpased: 2.83
batch start
#iterations: 57
currently lose_sum: 92.87603563070297
time_elpased: 2.809
batch start
#iterations: 58
currently lose_sum: 92.85913556814194
time_elpased: 2.81
batch start
#iterations: 59
currently lose_sum: 92.6832857131958
time_elpased: 2.831
start validation test
0.631082474227
0.619158443493
0.684470515591
0.650178405592
0.630988743286
62.305
batch start
#iterations: 60
currently lose_sum: 92.60267907381058
time_elpased: 2.802
batch start
#iterations: 61
currently lose_sum: 92.82640820741653
time_elpased: 2.814
batch start
#iterations: 62
currently lose_sum: 92.47417765855789
time_elpased: 2.833
batch start
#iterations: 63
currently lose_sum: 91.81894052028656
time_elpased: 2.818
batch start
#iterations: 64
currently lose_sum: 92.21561986207962
time_elpased: 2.83
batch start
#iterations: 65
currently lose_sum: 91.51825207471848
time_elpased: 2.822
batch start
#iterations: 66
currently lose_sum: 91.96654361486435
time_elpased: 2.826
batch start
#iterations: 67
currently lose_sum: 91.7936772108078
time_elpased: 2.818
batch start
#iterations: 68
currently lose_sum: 91.39076536893845
time_elpased: 2.816
batch start
#iterations: 69
currently lose_sum: 91.25331711769104
time_elpased: 2.824
batch start
#iterations: 70
currently lose_sum: 91.03200894594193
time_elpased: 2.855
batch start
#iterations: 71
currently lose_sum: 91.165731549263
time_elpased: 2.823
batch start
#iterations: 72
currently lose_sum: 91.30696362257004
time_elpased: 2.859
batch start
#iterations: 73
currently lose_sum: 91.4532658457756
time_elpased: 2.847
batch start
#iterations: 74
currently lose_sum: 91.3875703215599
time_elpased: 2.807
batch start
#iterations: 75
currently lose_sum: 90.59070193767548
time_elpased: 2.824
batch start
#iterations: 76
currently lose_sum: 90.64786005020142
time_elpased: 2.839
batch start
#iterations: 77
currently lose_sum: 90.27730023860931
time_elpased: 2.815
batch start
#iterations: 78
currently lose_sum: 91.04600143432617
time_elpased: 2.86
batch start
#iterations: 79
currently lose_sum: 90.40008795261383
time_elpased: 2.847
start validation test
0.617628865979
0.606543701919
0.673458886488
0.638252218863
0.617530847767
63.545
batch start
#iterations: 80
currently lose_sum: 90.19896054267883
time_elpased: 2.872
batch start
#iterations: 81
currently lose_sum: 90.25439083576202
time_elpased: 2.856
batch start
#iterations: 82
currently lose_sum: 89.96836733818054
time_elpased: 2.857
batch start
#iterations: 83
currently lose_sum: 89.962615609169
time_elpased: 2.852
batch start
#iterations: 84
currently lose_sum: 89.61890465021133
time_elpased: 2.81
batch start
#iterations: 85
currently lose_sum: 89.81533700227737
time_elpased: 2.934
batch start
#iterations: 86
currently lose_sum: 89.14333385229111
time_elpased: 2.828
batch start
#iterations: 87
currently lose_sum: 89.7826299071312
time_elpased: 2.846
batch start
#iterations: 88
currently lose_sum: 89.75076615810394
time_elpased: 2.875
batch start
#iterations: 89
currently lose_sum: 88.99650025367737
time_elpased: 2.859
batch start
#iterations: 90
currently lose_sum: 89.42849451303482
time_elpased: 2.843
batch start
#iterations: 91
currently lose_sum: 89.25656813383102
time_elpased: 2.845
batch start
#iterations: 92
currently lose_sum: 89.071198284626
time_elpased: 2.864
batch start
#iterations: 93
currently lose_sum: 88.58241152763367
time_elpased: 2.771
batch start
#iterations: 94
currently lose_sum: 88.24575763940811
time_elpased: 2.758
batch start
#iterations: 95
currently lose_sum: 88.83251291513443
time_elpased: 2.828
batch start
#iterations: 96
currently lose_sum: 88.23534673452377
time_elpased: 2.822
batch start
#iterations: 97
currently lose_sum: 87.89642030000687
time_elpased: 2.806
batch start
#iterations: 98
currently lose_sum: 88.62145829200745
time_elpased: 2.846
batch start
#iterations: 99
currently lose_sum: 88.3232638835907
time_elpased: 2.85
start validation test
0.601546391753
0.640622788393
0.465781619842
0.539387438923
0.60178474775
65.360
batch start
#iterations: 100
currently lose_sum: 88.28566884994507
time_elpased: 2.869
batch start
#iterations: 101
currently lose_sum: 88.35929644107819
time_elpased: 2.79
batch start
#iterations: 102
currently lose_sum: 87.36952042579651
time_elpased: 2.871
batch start
#iterations: 103
currently lose_sum: 87.70216596126556
time_elpased: 2.872
batch start
#iterations: 104
currently lose_sum: 87.52794563770294
time_elpased: 2.847
batch start
#iterations: 105
currently lose_sum: 86.72970432043076
time_elpased: 2.766
batch start
#iterations: 106
currently lose_sum: 86.85207206010818
time_elpased: 2.725
batch start
#iterations: 107
currently lose_sum: 87.36895143985748
time_elpased: 2.751
batch start
#iterations: 108
currently lose_sum: 86.13539391756058
time_elpased: 2.836
batch start
#iterations: 109
currently lose_sum: 86.17870518565178
time_elpased: 2.795
batch start
#iterations: 110
currently lose_sum: 86.35568284988403
time_elpased: 2.818
batch start
#iterations: 111
currently lose_sum: 85.47246873378754
time_elpased: 2.813
batch start
#iterations: 112
currently lose_sum: 87.05630999803543
time_elpased: 2.839
batch start
#iterations: 113
currently lose_sum: 86.21619617938995
time_elpased: 2.836
batch start
#iterations: 114
currently lose_sum: 85.88729506731033
time_elpased: 2.814
batch start
#iterations: 115
currently lose_sum: 85.97641313076019
time_elpased: 2.736
batch start
#iterations: 116
currently lose_sum: 85.115957736969
time_elpased: 2.879
batch start
#iterations: 117
currently lose_sum: 84.89389172196388
time_elpased: 2.872
batch start
#iterations: 118
currently lose_sum: 84.8014240860939
time_elpased: 2.938
batch start
#iterations: 119
currently lose_sum: 84.68004083633423
time_elpased: 2.915
start validation test
0.610309278351
0.603215618719
0.648656992899
0.625111573936
0.610241953023
66.455
batch start
#iterations: 120
currently lose_sum: 85.01428669691086
time_elpased: 2.845
batch start
#iterations: 121
currently lose_sum: 84.5587928891182
time_elpased: 2.826
batch start
#iterations: 122
currently lose_sum: 84.62107962369919
time_elpased: 2.834
batch start
#iterations: 123
currently lose_sum: 84.55004578828812
time_elpased: 2.839
batch start
#iterations: 124
currently lose_sum: 84.08678513765335
time_elpased: 2.857
batch start
#iterations: 125
currently lose_sum: 84.2769136428833
time_elpased: 2.833
batch start
#iterations: 126
currently lose_sum: 84.16202998161316
time_elpased: 2.812
batch start
#iterations: 127
currently lose_sum: 83.87787330150604
time_elpased: 2.843
batch start
#iterations: 128
currently lose_sum: 83.4967183470726
time_elpased: 2.889
batch start
#iterations: 129
currently lose_sum: 84.37761723995209
time_elpased: 2.837
batch start
#iterations: 130
currently lose_sum: 82.97789347171783
time_elpased: 2.893
batch start
#iterations: 131
currently lose_sum: 82.22252231836319
time_elpased: 2.846
batch start
#iterations: 132
currently lose_sum: 82.60340592265129
time_elpased: 2.841
batch start
#iterations: 133
currently lose_sum: 83.03480368852615
time_elpased: 2.85
batch start
#iterations: 134
currently lose_sum: 82.6598846912384
time_elpased: 2.847
batch start
#iterations: 135
currently lose_sum: 82.43698763847351
time_elpased: 2.872
batch start
#iterations: 136
currently lose_sum: 82.41650998592377
time_elpased: 2.878
batch start
#iterations: 137
currently lose_sum: 81.72594329714775
time_elpased: 2.846
batch start
#iterations: 138
currently lose_sum: 81.85722917318344
time_elpased: 2.854
batch start
#iterations: 139
currently lose_sum: 80.10561588406563
time_elpased: 2.855
start validation test
0.595670103093
0.627536429252
0.474220438407
0.54021101993
0.595883326711
72.918
batch start
#iterations: 140
currently lose_sum: 80.712700933218
time_elpased: 2.877
batch start
#iterations: 141
currently lose_sum: 81.44832989573479
time_elpased: 2.837
batch start
#iterations: 142
currently lose_sum: 80.8004529774189
time_elpased: 2.854
batch start
#iterations: 143
currently lose_sum: 80.74922972917557
time_elpased: 2.785
batch start
#iterations: 144
currently lose_sum: 80.77328798174858
time_elpased: 2.807
batch start
#iterations: 145
currently lose_sum: 80.16829553246498
time_elpased: 2.846
batch start
#iterations: 146
currently lose_sum: 80.39420905709267
time_elpased: 2.854
batch start
#iterations: 147
currently lose_sum: 80.15112844109535
time_elpased: 2.858
batch start
#iterations: 148
currently lose_sum: 79.22166302800179
time_elpased: 2.843
batch start
#iterations: 149
currently lose_sum: 79.47025182843208
time_elpased: 2.848
batch start
#iterations: 150
currently lose_sum: 79.64936783909798
time_elpased: 2.877
batch start
#iterations: 151
currently lose_sum: 79.98729664087296
time_elpased: 2.808
batch start
#iterations: 152
currently lose_sum: 78.98095646500587
time_elpased: 2.902
batch start
#iterations: 153
currently lose_sum: 78.85588476061821
time_elpased: 2.854
batch start
#iterations: 154
currently lose_sum: 78.38490578532219
time_elpased: 2.857
batch start
#iterations: 155
currently lose_sum: 78.38726896047592
time_elpased: 2.859
batch start
#iterations: 156
currently lose_sum: 78.86597388982773
time_elpased: 2.819
batch start
#iterations: 157
currently lose_sum: 78.30898907780647
time_elpased: 2.84
batch start
#iterations: 158
currently lose_sum: 78.18913760781288
time_elpased: 2.837
batch start
#iterations: 159
currently lose_sum: 77.67810502648354
time_elpased: 2.775
start validation test
0.581288659794
0.603131469979
0.479674796748
0.534365147607
0.581467058603
78.142
batch start
#iterations: 160
currently lose_sum: 77.35435152053833
time_elpased: 2.842
batch start
#iterations: 161
currently lose_sum: 77.67160260677338
time_elpased: 2.818
batch start
#iterations: 162
currently lose_sum: 77.23936393857002
time_elpased: 2.83
batch start
#iterations: 163
currently lose_sum: 77.00531423091888
time_elpased: 2.809
batch start
#iterations: 164
currently lose_sum: 76.21904361248016
time_elpased: 2.797
batch start
#iterations: 165
currently lose_sum: 75.54014217853546
time_elpased: 2.803
batch start
#iterations: 166
currently lose_sum: 75.86302042007446
time_elpased: 2.834
batch start
#iterations: 167
currently lose_sum: 76.42463713884354
time_elpased: 2.831
batch start
#iterations: 168
currently lose_sum: 76.15299800038338
time_elpased: 2.829
batch start
#iterations: 169
currently lose_sum: 75.70906272530556
time_elpased: 2.851
batch start
#iterations: 170
currently lose_sum: 75.34882634878159
time_elpased: 2.861
batch start
#iterations: 171
currently lose_sum: 74.91940504312515
time_elpased: 2.854
batch start
#iterations: 172
currently lose_sum: 74.96163758635521
time_elpased: 2.847
batch start
#iterations: 173
currently lose_sum: 74.77290752530098
time_elpased: 2.79
batch start
#iterations: 174
currently lose_sum: 75.18358400464058
time_elpased: 2.859
batch start
#iterations: 175
currently lose_sum: 73.42516848444939
time_elpased: 2.836
batch start
#iterations: 176
currently lose_sum: 73.30695876479149
time_elpased: 2.861
batch start
#iterations: 177
currently lose_sum: 73.07342684268951
time_elpased: 2.861
batch start
#iterations: 178
currently lose_sum: 73.86153611540794
time_elpased: 2.869
batch start
#iterations: 179
currently lose_sum: 73.05575579404831
time_elpased: 2.874
start validation test
0.570463917526
0.604437066103
0.412164248225
0.490118093373
0.570741837011
84.705
batch start
#iterations: 180
currently lose_sum: 73.29854467511177
time_elpased: 2.913
batch start
#iterations: 181
currently lose_sum: 71.7715694308281
time_elpased: 2.814
batch start
#iterations: 182
currently lose_sum: 72.10232588648796
time_elpased: 2.791
batch start
#iterations: 183
currently lose_sum: 72.15023002028465
time_elpased: 2.866
batch start
#iterations: 184
currently lose_sum: 71.14056652784348
time_elpased: 2.902
batch start
#iterations: 185
currently lose_sum: 72.02679234743118
time_elpased: 2.865
batch start
#iterations: 186
currently lose_sum: 71.41884952783585
time_elpased: 2.82
batch start
#iterations: 187
currently lose_sum: 71.63948532938957
time_elpased: 2.814
batch start
#iterations: 188
currently lose_sum: 71.72161561250687
time_elpased: 2.851
batch start
#iterations: 189
currently lose_sum: 71.03573462367058
time_elpased: 2.842
batch start
#iterations: 190
currently lose_sum: 70.73035842180252
time_elpased: 2.848
batch start
#iterations: 191
currently lose_sum: 70.15927267074585
time_elpased: 2.83
batch start
#iterations: 192
currently lose_sum: 69.68750077486038
time_elpased: 2.921
batch start
#iterations: 193
currently lose_sum: 70.49374660849571
time_elpased: 2.859
batch start
#iterations: 194
currently lose_sum: 70.55776980519295
time_elpased: 2.829
batch start
#iterations: 195
currently lose_sum: 69.22786888480186
time_elpased: 2.835
batch start
#iterations: 196
currently lose_sum: 69.89081680774689
time_elpased: 2.859
batch start
#iterations: 197
currently lose_sum: 69.15892043709755
time_elpased: 2.833
batch start
#iterations: 198
currently lose_sum: 68.82343751192093
time_elpased: 2.849
batch start
#iterations: 199
currently lose_sum: 68.32464537024498
time_elpased: 2.846
start validation test
0.56087628866
0.600335008375
0.368838118761
0.456938866577
0.561213441287
101.220
batch start
#iterations: 200
currently lose_sum: 70.47969967126846
time_elpased: 2.929
batch start
#iterations: 201
currently lose_sum: 68.39074087142944
time_elpased: 2.823
batch start
#iterations: 202
currently lose_sum: 67.12576207518578
time_elpased: 2.894
batch start
#iterations: 203
currently lose_sum: 67.81021052598953
time_elpased: 2.841
batch start
#iterations: 204
currently lose_sum: 66.56581583619118
time_elpased: 2.9
batch start
#iterations: 205
currently lose_sum: 67.21676063537598
time_elpased: 2.903
batch start
#iterations: 206
currently lose_sum: 66.60516998171806
time_elpased: 2.844
batch start
#iterations: 207
currently lose_sum: 65.72068849205971
time_elpased: 2.848
batch start
#iterations: 208
currently lose_sum: 65.96144640445709
time_elpased: 2.843
batch start
#iterations: 209
currently lose_sum: 65.50465223193169
time_elpased: 2.829
batch start
#iterations: 210
currently lose_sum: 66.17424044013023
time_elpased: 2.857
batch start
#iterations: 211
currently lose_sum: 65.5121847987175
time_elpased: 2.826
batch start
#iterations: 212
currently lose_sum: 65.34854307770729
time_elpased: 2.839
batch start
#iterations: 213
currently lose_sum: 65.5499352812767
time_elpased: 2.822
batch start
#iterations: 214
currently lose_sum: 63.98378178477287
time_elpased: 2.947
batch start
#iterations: 215
currently lose_sum: 64.62655612826347
time_elpased: 2.79
batch start
#iterations: 216
currently lose_sum: 64.732441842556
time_elpased: 2.84
batch start
#iterations: 217
currently lose_sum: 63.71123290061951
time_elpased: 2.839
batch start
#iterations: 218
currently lose_sum: 63.65065908432007
time_elpased: 2.879
batch start
#iterations: 219
currently lose_sum: 63.252980560064316
time_elpased: 2.867
start validation test
0.558865979381
0.595579746
0.371616754142
0.457667934094
0.559194724278
107.183
batch start
#iterations: 220
currently lose_sum: 64.01984131336212
time_elpased: 2.85
batch start
#iterations: 221
currently lose_sum: 63.4634593129158
time_elpased: 2.815
batch start
#iterations: 222
currently lose_sum: 62.50993385910988
time_elpased: 2.821
batch start
#iterations: 223
currently lose_sum: 62.32590061426163
time_elpased: 2.788
batch start
#iterations: 224
currently lose_sum: 61.9527293741703
time_elpased: 2.914
batch start
#iterations: 225
currently lose_sum: 62.18958079814911
time_elpased: 2.862
batch start
#iterations: 226
currently lose_sum: 61.757280081510544
time_elpased: 2.868
batch start
#iterations: 227
currently lose_sum: 61.88215211033821
time_elpased: 2.856
batch start
#iterations: 228
currently lose_sum: 61.21141356229782
time_elpased: 2.886
batch start
#iterations: 229
currently lose_sum: 60.981971859931946
time_elpased: 2.897
batch start
#iterations: 230
currently lose_sum: 60.798100769519806
time_elpased: 2.857
batch start
#iterations: 231
currently lose_sum: 60.19297111034393
time_elpased: 2.829
batch start
#iterations: 232
currently lose_sum: 60.72025063633919
time_elpased: 2.857
batch start
#iterations: 233
currently lose_sum: 60.895272731781006
time_elpased: 2.738
batch start
#iterations: 234
currently lose_sum: 60.60465371608734
time_elpased: 2.889
batch start
#iterations: 235
currently lose_sum: 59.59089905023575
time_elpased: 2.832
batch start
#iterations: 236
currently lose_sum: 59.51283121109009
time_elpased: 2.79
batch start
#iterations: 237
currently lose_sum: 59.40842446684837
time_elpased: 2.863
batch start
#iterations: 238
currently lose_sum: 59.32445013523102
time_elpased: 2.842
batch start
#iterations: 239
currently lose_sum: 59.80914452672005
time_elpased: 2.834
start validation test
0.542783505155
0.58411122145
0.302665431718
0.398725596529
0.543205069468
127.703
batch start
#iterations: 240
currently lose_sum: 58.48266938328743
time_elpased: 2.846
batch start
#iterations: 241
currently lose_sum: 58.14366629719734
time_elpased: 2.817
batch start
#iterations: 242
currently lose_sum: 58.280389964580536
time_elpased: 2.802
batch start
#iterations: 243
currently lose_sum: 58.410485327243805
time_elpased: 2.856
batch start
#iterations: 244
currently lose_sum: 57.284927159547806
time_elpased: 2.843
batch start
#iterations: 245
currently lose_sum: 57.743619203567505
time_elpased: 2.843
batch start
#iterations: 246
currently lose_sum: nan
time_elpased: 2.888
train finish final lose is: nan
acc: 0.632
pre: 0.620
rec: 0.683
F1: 0.650
auc: 0.632
