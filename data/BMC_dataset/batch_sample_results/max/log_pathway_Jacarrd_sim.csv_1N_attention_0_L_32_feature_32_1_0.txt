start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.77350604534149
time_elpased: 1.976
batch start
#iterations: 1
currently lose_sum: 99.96684449911118
time_elpased: 1.911
batch start
#iterations: 2
currently lose_sum: 99.9316394329071
time_elpased: 1.891
batch start
#iterations: 3
currently lose_sum: 99.52930587530136
time_elpased: 1.872
batch start
#iterations: 4
currently lose_sum: 99.34392803907394
time_elpased: 1.869
batch start
#iterations: 5
currently lose_sum: 99.15592211484909
time_elpased: 1.943
batch start
#iterations: 6
currently lose_sum: 99.0395438671112
time_elpased: 1.884
batch start
#iterations: 7
currently lose_sum: 98.88529139757156
time_elpased: 1.898
batch start
#iterations: 8
currently lose_sum: 98.76759141683578
time_elpased: 1.875
batch start
#iterations: 9
currently lose_sum: 98.8809341788292
time_elpased: 1.877
batch start
#iterations: 10
currently lose_sum: 98.6942589879036
time_elpased: 1.865
batch start
#iterations: 11
currently lose_sum: 98.33416283130646
time_elpased: 1.895
batch start
#iterations: 12
currently lose_sum: 98.40086269378662
time_elpased: 1.849
batch start
#iterations: 13
currently lose_sum: 98.45294988155365
time_elpased: 1.862
batch start
#iterations: 14
currently lose_sum: 98.26701706647873
time_elpased: 1.867
batch start
#iterations: 15
currently lose_sum: 98.10977149009705
time_elpased: 1.878
batch start
#iterations: 16
currently lose_sum: 97.99953556060791
time_elpased: 1.859
batch start
#iterations: 17
currently lose_sum: 98.09766221046448
time_elpased: 1.874
batch start
#iterations: 18
currently lose_sum: 97.75582671165466
time_elpased: 1.918
batch start
#iterations: 19
currently lose_sum: 97.73973643779755
time_elpased: 1.911
start validation test
0.596701030928
0.617767823815
0.510960172893
0.559310577898
0.596851562229
64.291
batch start
#iterations: 20
currently lose_sum: 97.59755557775497
time_elpased: 1.852
batch start
#iterations: 21
currently lose_sum: 97.56506836414337
time_elpased: 1.905
batch start
#iterations: 22
currently lose_sum: 97.53880017995834
time_elpased: 1.865
batch start
#iterations: 23
currently lose_sum: 97.58692473173141
time_elpased: 1.869
batch start
#iterations: 24
currently lose_sum: 97.41289818286896
time_elpased: 1.928
batch start
#iterations: 25
currently lose_sum: 97.1260034441948
time_elpased: 1.888
batch start
#iterations: 26
currently lose_sum: 97.18364304304123
time_elpased: 1.865
batch start
#iterations: 27
currently lose_sum: 97.11528384685516
time_elpased: 1.861
batch start
#iterations: 28
currently lose_sum: 97.0993464589119
time_elpased: 1.841
batch start
#iterations: 29
currently lose_sum: 97.07531660795212
time_elpased: 1.872
batch start
#iterations: 30
currently lose_sum: 96.81724506616592
time_elpased: 1.928
batch start
#iterations: 31
currently lose_sum: 96.50615924596786
time_elpased: 1.957
batch start
#iterations: 32
currently lose_sum: 96.67095375061035
time_elpased: 1.874
batch start
#iterations: 33
currently lose_sum: 96.75667595863342
time_elpased: 1.873
batch start
#iterations: 34
currently lose_sum: 96.62594956159592
time_elpased: 1.967
batch start
#iterations: 35
currently lose_sum: 96.90236622095108
time_elpased: 1.88
batch start
#iterations: 36
currently lose_sum: 97.02862060070038
time_elpased: 1.906
batch start
#iterations: 37
currently lose_sum: 96.67136108875275
time_elpased: 1.919
batch start
#iterations: 38
currently lose_sum: 96.28887957334518
time_elpased: 1.907
batch start
#iterations: 39
currently lose_sum: 96.68792814016342
time_elpased: 1.898
start validation test
0.60912371134
0.624012087401
0.552536791191
0.586103378637
0.609223058406
63.600
batch start
#iterations: 40
currently lose_sum: 96.22956275939941
time_elpased: 1.849
batch start
#iterations: 41
currently lose_sum: 96.33836424350739
time_elpased: 1.894
batch start
#iterations: 42
currently lose_sum: 96.43542319536209
time_elpased: 1.865
batch start
#iterations: 43
currently lose_sum: 96.41372787952423
time_elpased: 1.865
batch start
#iterations: 44
currently lose_sum: 96.28589016199112
time_elpased: 1.885
batch start
#iterations: 45
currently lose_sum: 95.90499240159988
time_elpased: 1.881
batch start
#iterations: 46
currently lose_sum: 96.14485919475555
time_elpased: 1.853
batch start
#iterations: 47
currently lose_sum: 96.16746783256531
time_elpased: 1.898
batch start
#iterations: 48
currently lose_sum: 96.25079691410065
time_elpased: 1.88
batch start
#iterations: 49
currently lose_sum: 96.04563111066818
time_elpased: 1.864
batch start
#iterations: 50
currently lose_sum: 96.50945228338242
time_elpased: 1.886
batch start
#iterations: 51
currently lose_sum: 95.68863260746002
time_elpased: 1.89
batch start
#iterations: 52
currently lose_sum: 95.66065692901611
time_elpased: 1.886
batch start
#iterations: 53
currently lose_sum: 96.03694987297058
time_elpased: 1.885
batch start
#iterations: 54
currently lose_sum: 95.73427700996399
time_elpased: 1.895
batch start
#iterations: 55
currently lose_sum: 95.67022097110748
time_elpased: 1.872
batch start
#iterations: 56
currently lose_sum: 95.79971766471863
time_elpased: 1.926
batch start
#iterations: 57
currently lose_sum: 95.42247241735458
time_elpased: 1.882
batch start
#iterations: 58
currently lose_sum: 95.99651604890823
time_elpased: 1.914
batch start
#iterations: 59
currently lose_sum: 95.40862953662872
time_elpased: 1.867
start validation test
0.613453608247
0.623910614525
0.574662961819
0.598275030803
0.61352171121
63.239
batch start
#iterations: 60
currently lose_sum: 95.9792650938034
time_elpased: 1.862
batch start
#iterations: 61
currently lose_sum: 95.6153963804245
time_elpased: 1.844
batch start
#iterations: 62
currently lose_sum: 95.56836926937103
time_elpased: 1.89
batch start
#iterations: 63
currently lose_sum: 95.6936547756195
time_elpased: 1.885
batch start
#iterations: 64
currently lose_sum: 95.68391555547714
time_elpased: 1.877
batch start
#iterations: 65
currently lose_sum: 95.44003146886826
time_elpased: 1.871
batch start
#iterations: 66
currently lose_sum: 94.95129054784775
time_elpased: 1.885
batch start
#iterations: 67
currently lose_sum: 95.3451178073883
time_elpased: 1.848
batch start
#iterations: 68
currently lose_sum: 95.3898201584816
time_elpased: 1.867
batch start
#iterations: 69
currently lose_sum: 95.35558742284775
time_elpased: 1.872
batch start
#iterations: 70
currently lose_sum: 95.14312535524368
time_elpased: 1.85
batch start
#iterations: 71
currently lose_sum: 94.98769348859787
time_elpased: 1.876
batch start
#iterations: 72
currently lose_sum: 95.06552815437317
time_elpased: 1.859
batch start
#iterations: 73
currently lose_sum: 94.87259435653687
time_elpased: 1.871
batch start
#iterations: 74
currently lose_sum: 94.79340517520905
time_elpased: 1.898
batch start
#iterations: 75
currently lose_sum: 94.72207260131836
time_elpased: 1.891
batch start
#iterations: 76
currently lose_sum: 94.5162153840065
time_elpased: 1.866
batch start
#iterations: 77
currently lose_sum: 94.92106837034225
time_elpased: 1.858
batch start
#iterations: 78
currently lose_sum: 94.93696814775467
time_elpased: 1.944
batch start
#iterations: 79
currently lose_sum: 94.90464609861374
time_elpased: 1.878
start validation test
0.608402061856
0.633973710819
0.516208706391
0.569062340462
0.608563921511
63.168
batch start
#iterations: 80
currently lose_sum: 94.67948806285858
time_elpased: 1.909
batch start
#iterations: 81
currently lose_sum: 95.21746182441711
time_elpased: 1.911
batch start
#iterations: 82
currently lose_sum: 94.61296272277832
time_elpased: 1.933
batch start
#iterations: 83
currently lose_sum: 95.02323943376541
time_elpased: 1.864
batch start
#iterations: 84
currently lose_sum: 94.83833259344101
time_elpased: 1.841
batch start
#iterations: 85
currently lose_sum: 94.86368989944458
time_elpased: 1.896
batch start
#iterations: 86
currently lose_sum: 94.28441995382309
time_elpased: 1.879
batch start
#iterations: 87
currently lose_sum: 94.56022328138351
time_elpased: 1.925
batch start
#iterations: 88
currently lose_sum: 94.59352231025696
time_elpased: 1.852
batch start
#iterations: 89
currently lose_sum: 94.62814509868622
time_elpased: 1.889
batch start
#iterations: 90
currently lose_sum: 94.62778693437576
time_elpased: 1.871
batch start
#iterations: 91
currently lose_sum: 94.14772689342499
time_elpased: 1.89
batch start
#iterations: 92
currently lose_sum: 94.40800958871841
time_elpased: 1.874
batch start
#iterations: 93
currently lose_sum: 94.17690074443817
time_elpased: 1.884
batch start
#iterations: 94
currently lose_sum: 94.19714367389679
time_elpased: 1.883
batch start
#iterations: 95
currently lose_sum: 94.32263123989105
time_elpased: 1.852
batch start
#iterations: 96
currently lose_sum: 94.28627967834473
time_elpased: 1.947
batch start
#iterations: 97
currently lose_sum: 94.48203438520432
time_elpased: 1.854
batch start
#iterations: 98
currently lose_sum: 94.19309240579605
time_elpased: 1.927
batch start
#iterations: 99
currently lose_sum: 94.2991550564766
time_elpased: 1.883
start validation test
0.608453608247
0.62803332126
0.535350416795
0.578
0.608581952175
63.460
batch start
#iterations: 100
currently lose_sum: 93.8584463596344
time_elpased: 1.917
batch start
#iterations: 101
currently lose_sum: 94.0987138748169
time_elpased: 1.974
batch start
#iterations: 102
currently lose_sum: 93.95853292942047
time_elpased: 1.883
batch start
#iterations: 103
currently lose_sum: 94.2298601269722
time_elpased: 1.859
batch start
#iterations: 104
currently lose_sum: 93.89945036172867
time_elpased: 1.899
batch start
#iterations: 105
currently lose_sum: 93.68280673027039
time_elpased: 1.863
batch start
#iterations: 106
currently lose_sum: 94.03501737117767
time_elpased: 1.87
batch start
#iterations: 107
currently lose_sum: 93.58132433891296
time_elpased: 1.873
batch start
#iterations: 108
currently lose_sum: 93.50598472356796
time_elpased: 1.912
batch start
#iterations: 109
currently lose_sum: 94.00342237949371
time_elpased: 1.878
batch start
#iterations: 110
currently lose_sum: 94.09189587831497
time_elpased: 1.886
batch start
#iterations: 111
currently lose_sum: 93.61352950334549
time_elpased: 1.886
batch start
#iterations: 112
currently lose_sum: 93.73429161310196
time_elpased: 1.882
batch start
#iterations: 113
currently lose_sum: 93.5472571849823
time_elpased: 1.882
batch start
#iterations: 114
currently lose_sum: 93.96709215641022
time_elpased: 1.912
batch start
#iterations: 115
currently lose_sum: 93.59030944108963
time_elpased: 1.91
batch start
#iterations: 116
currently lose_sum: 93.37148940563202
time_elpased: 1.927
batch start
#iterations: 117
currently lose_sum: 93.91777092218399
time_elpased: 1.918
batch start
#iterations: 118
currently lose_sum: 93.3653205037117
time_elpased: 1.864
batch start
#iterations: 119
currently lose_sum: 93.32584846019745
time_elpased: 1.907
start validation test
0.600360824742
0.633860414395
0.478542760111
0.545358587932
0.600574695144
63.858
batch start
#iterations: 120
currently lose_sum: 93.5383853316307
time_elpased: 1.88
batch start
#iterations: 121
currently lose_sum: 93.83585560321808
time_elpased: 1.91
batch start
#iterations: 122
currently lose_sum: 93.25425058603287
time_elpased: 1.864
batch start
#iterations: 123
currently lose_sum: 93.33328145742416
time_elpased: 1.868
batch start
#iterations: 124
currently lose_sum: 93.82282567024231
time_elpased: 1.855
batch start
#iterations: 125
currently lose_sum: 93.58674865961075
time_elpased: 1.906
batch start
#iterations: 126
currently lose_sum: 93.63715779781342
time_elpased: 1.861
batch start
#iterations: 127
currently lose_sum: 93.42064481973648
time_elpased: 1.874
batch start
#iterations: 128
currently lose_sum: 93.34109473228455
time_elpased: 1.848
batch start
#iterations: 129
currently lose_sum: 93.16828519105911
time_elpased: 1.926
batch start
#iterations: 130
currently lose_sum: 93.27712160348892
time_elpased: 1.883
batch start
#iterations: 131
currently lose_sum: 93.22167932987213
time_elpased: 1.864
batch start
#iterations: 132
currently lose_sum: 93.2233772277832
time_elpased: 1.869
batch start
#iterations: 133
currently lose_sum: 93.56572753190994
time_elpased: 1.859
batch start
#iterations: 134
currently lose_sum: 93.33099508285522
time_elpased: 1.894
batch start
#iterations: 135
currently lose_sum: 93.37485408782959
time_elpased: 1.863
batch start
#iterations: 136
currently lose_sum: 93.33303928375244
time_elpased: 1.852
batch start
#iterations: 137
currently lose_sum: 92.85233110189438
time_elpased: 1.881
batch start
#iterations: 138
currently lose_sum: 92.85317134857178
time_elpased: 1.903
batch start
#iterations: 139
currently lose_sum: 93.09650892019272
time_elpased: 1.864
start validation test
0.606288659794
0.625165562914
0.53432129258
0.576184663189
0.606415009607
63.456
batch start
#iterations: 140
currently lose_sum: 93.1628914475441
time_elpased: 1.92
batch start
#iterations: 141
currently lose_sum: 93.0161964893341
time_elpased: 1.886
batch start
#iterations: 142
currently lose_sum: 92.719278216362
time_elpased: 1.89
batch start
#iterations: 143
currently lose_sum: 92.94170898199081
time_elpased: 1.888
batch start
#iterations: 144
currently lose_sum: 92.7563346028328
time_elpased: 1.89
batch start
#iterations: 145
currently lose_sum: 92.81259042024612
time_elpased: 1.869
batch start
#iterations: 146
currently lose_sum: 92.90732502937317
time_elpased: 1.885
batch start
#iterations: 147
currently lose_sum: 92.98821341991425
time_elpased: 1.857
batch start
#iterations: 148
currently lose_sum: 92.95856720209122
time_elpased: 1.87
batch start
#iterations: 149
currently lose_sum: 93.09493327140808
time_elpased: 1.852
batch start
#iterations: 150
currently lose_sum: 92.77033346891403
time_elpased: 1.871
batch start
#iterations: 151
currently lose_sum: 92.72418928146362
time_elpased: 1.924
batch start
#iterations: 152
currently lose_sum: 92.76300328969955
time_elpased: 1.878
batch start
#iterations: 153
currently lose_sum: 92.69592475891113
time_elpased: 1.902
batch start
#iterations: 154
currently lose_sum: 92.7635812163353
time_elpased: 1.869
batch start
#iterations: 155
currently lose_sum: 92.79255998134613
time_elpased: 1.912
batch start
#iterations: 156
currently lose_sum: 92.36076670885086
time_elpased: 1.89
batch start
#iterations: 157
currently lose_sum: 92.52474427223206
time_elpased: 1.938
batch start
#iterations: 158
currently lose_sum: 93.19576239585876
time_elpased: 1.83
batch start
#iterations: 159
currently lose_sum: 92.52390515804291
time_elpased: 1.867
start validation test
0.598608247423
0.627848436672
0.487701965627
0.548971908485
0.598802960506
64.157
batch start
#iterations: 160
currently lose_sum: 92.96697306632996
time_elpased: 1.905
batch start
#iterations: 161
currently lose_sum: 92.52234476804733
time_elpased: 1.883
batch start
#iterations: 162
currently lose_sum: 92.40362429618835
time_elpased: 1.864
batch start
#iterations: 163
currently lose_sum: 92.51571655273438
time_elpased: 1.908
batch start
#iterations: 164
currently lose_sum: 92.72956293821335
time_elpased: 1.871
batch start
#iterations: 165
currently lose_sum: 92.34079444408417
time_elpased: 1.915
batch start
#iterations: 166
currently lose_sum: 92.7115033864975
time_elpased: 1.872
batch start
#iterations: 167
currently lose_sum: 92.66327285766602
time_elpased: 1.902
batch start
#iterations: 168
currently lose_sum: 92.27219969034195
time_elpased: 1.866
batch start
#iterations: 169
currently lose_sum: 92.42798817157745
time_elpased: 1.904
batch start
#iterations: 170
currently lose_sum: 92.22514528036118
time_elpased: 1.866
batch start
#iterations: 171
currently lose_sum: 92.40928095579147
time_elpased: 1.901
batch start
#iterations: 172
currently lose_sum: 91.95377135276794
time_elpased: 1.899
batch start
#iterations: 173
currently lose_sum: 92.45976024866104
time_elpased: 1.88
batch start
#iterations: 174
currently lose_sum: 91.90526843070984
time_elpased: 1.933
batch start
#iterations: 175
currently lose_sum: 92.46359646320343
time_elpased: 1.884
batch start
#iterations: 176
currently lose_sum: 92.62949568033218
time_elpased: 1.93
batch start
#iterations: 177
currently lose_sum: 92.2223584651947
time_elpased: 1.941
batch start
#iterations: 178
currently lose_sum: 91.8649936914444
time_elpased: 1.907
batch start
#iterations: 179
currently lose_sum: 91.99752831459045
time_elpased: 1.876
start validation test
0.598917525773
0.632060027285
0.476793248945
0.543556050918
0.599131933778
64.318
batch start
#iterations: 180
currently lose_sum: 91.99356633424759
time_elpased: 1.906
batch start
#iterations: 181
currently lose_sum: 91.37857693433762
time_elpased: 1.899
batch start
#iterations: 182
currently lose_sum: 92.66788470745087
time_elpased: 1.881
batch start
#iterations: 183
currently lose_sum: 91.88046944141388
time_elpased: 1.888
batch start
#iterations: 184
currently lose_sum: 92.26505506038666
time_elpased: 1.893
batch start
#iterations: 185
currently lose_sum: 91.81435430049896
time_elpased: 1.899
batch start
#iterations: 186
currently lose_sum: 91.78314518928528
time_elpased: 1.881
batch start
#iterations: 187
currently lose_sum: 92.04923403263092
time_elpased: 1.932
batch start
#iterations: 188
currently lose_sum: 91.8883261680603
time_elpased: 1.9
batch start
#iterations: 189
currently lose_sum: 91.65096461772919
time_elpased: 1.895
batch start
#iterations: 190
currently lose_sum: 91.98082280158997
time_elpased: 1.894
batch start
#iterations: 191
currently lose_sum: 91.57578271627426
time_elpased: 1.891
batch start
#iterations: 192
currently lose_sum: 91.92567950487137
time_elpased: 1.917
batch start
#iterations: 193
currently lose_sum: 92.0056140422821
time_elpased: 1.971
batch start
#iterations: 194
currently lose_sum: 91.81417840719223
time_elpased: 1.863
batch start
#iterations: 195
currently lose_sum: 91.7127451300621
time_elpased: 1.902
batch start
#iterations: 196
currently lose_sum: 91.74345314502716
time_elpased: 1.927
batch start
#iterations: 197
currently lose_sum: 91.62814718484879
time_elpased: 1.91
batch start
#iterations: 198
currently lose_sum: 91.84097439050674
time_elpased: 1.906
batch start
#iterations: 199
currently lose_sum: 91.85778039693832
time_elpased: 1.908
start validation test
0.583917525773
0.638072855464
0.391170114233
0.485007017992
0.584255923583
65.446
batch start
#iterations: 200
currently lose_sum: 92.09087550640106
time_elpased: 1.897
batch start
#iterations: 201
currently lose_sum: 91.19897198677063
time_elpased: 1.898
batch start
#iterations: 202
currently lose_sum: 91.422527551651
time_elpased: 1.907
batch start
#iterations: 203
currently lose_sum: 91.50837045907974
time_elpased: 1.953
batch start
#iterations: 204
currently lose_sum: 91.87026584148407
time_elpased: 2.024
batch start
#iterations: 205
currently lose_sum: 91.70743840932846
time_elpased: 1.851
batch start
#iterations: 206
currently lose_sum: 91.73673921823502
time_elpased: 1.899
batch start
#iterations: 207
currently lose_sum: 91.49566012620926
time_elpased: 1.847
batch start
#iterations: 208
currently lose_sum: 91.28051400184631
time_elpased: 1.89
batch start
#iterations: 209
currently lose_sum: 91.59257102012634
time_elpased: 1.882
batch start
#iterations: 210
currently lose_sum: 91.57530856132507
time_elpased: 1.861
batch start
#iterations: 211
currently lose_sum: 91.5431597828865
time_elpased: 1.892
batch start
#iterations: 212
currently lose_sum: 91.08755248785019
time_elpased: 1.9
batch start
#iterations: 213
currently lose_sum: 90.68433552980423
time_elpased: 1.911
batch start
#iterations: 214
currently lose_sum: 90.99048972129822
time_elpased: 1.897
batch start
#iterations: 215
currently lose_sum: 91.42210710048676
time_elpased: 1.84
batch start
#iterations: 216
currently lose_sum: 91.36465674638748
time_elpased: 1.959
batch start
#iterations: 217
currently lose_sum: 91.49643188714981
time_elpased: 1.869
batch start
#iterations: 218
currently lose_sum: 91.43493109941483
time_elpased: 1.922
batch start
#iterations: 219
currently lose_sum: 91.44034141302109
time_elpased: 1.892
start validation test
0.577783505155
0.628581058308
0.383863332304
0.476646859626
0.578123961928
66.181
batch start
#iterations: 220
currently lose_sum: 91.57207560539246
time_elpased: 1.87
batch start
#iterations: 221
currently lose_sum: 90.91929423809052
time_elpased: 1.867
batch start
#iterations: 222
currently lose_sum: 91.65717083215714
time_elpased: 1.91
batch start
#iterations: 223
currently lose_sum: 91.30373406410217
time_elpased: 1.993
batch start
#iterations: 224
currently lose_sum: 91.20579254627228
time_elpased: 1.916
batch start
#iterations: 225
currently lose_sum: 91.44572865962982
time_elpased: 1.91
batch start
#iterations: 226
currently lose_sum: 90.92923241853714
time_elpased: 1.871
batch start
#iterations: 227
currently lose_sum: 90.80051463842392
time_elpased: 1.826
batch start
#iterations: 228
currently lose_sum: 90.74115562438965
time_elpased: 1.886
batch start
#iterations: 229
currently lose_sum: 90.98271644115448
time_elpased: 1.871
batch start
#iterations: 230
currently lose_sum: 91.00459563732147
time_elpased: 1.883
batch start
#iterations: 231
currently lose_sum: 90.8769987821579
time_elpased: 1.864
batch start
#iterations: 232
currently lose_sum: 91.14253216981888
time_elpased: 1.887
batch start
#iterations: 233
currently lose_sum: 90.86802214384079
time_elpased: 1.845
batch start
#iterations: 234
currently lose_sum: 91.4430924654007
time_elpased: 1.9
batch start
#iterations: 235
currently lose_sum: 91.00515645742416
time_elpased: 1.898
batch start
#iterations: 236
currently lose_sum: 91.08639335632324
time_elpased: 1.903
batch start
#iterations: 237
currently lose_sum: 91.39301055669785
time_elpased: 1.883
batch start
#iterations: 238
currently lose_sum: 90.60877406597137
time_elpased: 1.896
batch start
#iterations: 239
currently lose_sum: 90.99664270877838
time_elpased: 1.894
start validation test
0.602164948454
0.616369775294
0.544818359576
0.578389599039
0.602265629235
63.958
batch start
#iterations: 240
currently lose_sum: 90.82259011268616
time_elpased: 1.899
batch start
#iterations: 241
currently lose_sum: 90.8392658829689
time_elpased: 1.865
batch start
#iterations: 242
currently lose_sum: 90.89490020275116
time_elpased: 1.88
batch start
#iterations: 243
currently lose_sum: 90.98988699913025
time_elpased: 1.874
batch start
#iterations: 244
currently lose_sum: 91.22751766443253
time_elpased: 1.866
batch start
#iterations: 245
currently lose_sum: 90.92372596263885
time_elpased: 1.857
batch start
#iterations: 246
currently lose_sum: 91.20133370161057
time_elpased: 1.9
batch start
#iterations: 247
currently lose_sum: 90.8777465224266
time_elpased: 1.872
batch start
#iterations: 248
currently lose_sum: 90.8352210521698
time_elpased: 1.882
batch start
#iterations: 249
currently lose_sum: 90.87017410993576
time_elpased: 1.911
batch start
#iterations: 250
currently lose_sum: 91.24386352300644
time_elpased: 1.904
batch start
#iterations: 251
currently lose_sum: 90.48776042461395
time_elpased: 1.902
batch start
#iterations: 252
currently lose_sum: 90.88740676641464
time_elpased: 1.879
batch start
#iterations: 253
currently lose_sum: 90.83576971292496
time_elpased: 1.874
batch start
#iterations: 254
currently lose_sum: 90.82851696014404
time_elpased: 1.897
batch start
#iterations: 255
currently lose_sum: 90.4245074391365
time_elpased: 1.891
batch start
#iterations: 256
currently lose_sum: 91.23582649230957
time_elpased: 1.917
batch start
#iterations: 257
currently lose_sum: 90.87681972980499
time_elpased: 1.846
batch start
#iterations: 258
currently lose_sum: 90.89251720905304
time_elpased: 1.91
batch start
#iterations: 259
currently lose_sum: 90.50263094902039
time_elpased: 1.872
start validation test
0.598711340206
0.618266405485
0.519707728723
0.5647190383
0.598850043232
64.312
batch start
#iterations: 260
currently lose_sum: 90.59316098690033
time_elpased: 1.89
batch start
#iterations: 261
currently lose_sum: 91.01077175140381
time_elpased: 1.886
batch start
#iterations: 262
currently lose_sum: 91.06837296485901
time_elpased: 1.881
batch start
#iterations: 263
currently lose_sum: 90.64113390445709
time_elpased: 1.915
batch start
#iterations: 264
currently lose_sum: 91.40775436162949
time_elpased: 1.919
batch start
#iterations: 265
currently lose_sum: 90.31348139047623
time_elpased: 1.855
batch start
#iterations: 266
currently lose_sum: 90.75146687030792
time_elpased: 1.892
batch start
#iterations: 267
currently lose_sum: 90.70144534111023
time_elpased: 1.842
batch start
#iterations: 268
currently lose_sum: 90.63531047105789
time_elpased: 1.918
batch start
#iterations: 269
currently lose_sum: 90.57915318012238
time_elpased: 1.873
batch start
#iterations: 270
currently lose_sum: 90.81507021188736
time_elpased: 1.885
batch start
#iterations: 271
currently lose_sum: 90.4903993010521
time_elpased: 1.867
batch start
#iterations: 272
currently lose_sum: 89.77202075719833
time_elpased: 1.872
batch start
#iterations: 273
currently lose_sum: 90.4756972193718
time_elpased: 1.868
batch start
#iterations: 274
currently lose_sum: 90.17422103881836
time_elpased: 1.89
batch start
#iterations: 275
currently lose_sum: 90.22929829359055
time_elpased: 1.88
batch start
#iterations: 276
currently lose_sum: 90.83241432905197
time_elpased: 1.932
batch start
#iterations: 277
currently lose_sum: 90.50344187021255
time_elpased: 1.869
batch start
#iterations: 278
currently lose_sum: 90.74761402606964
time_elpased: 1.881
batch start
#iterations: 279
currently lose_sum: 90.28547871112823
time_elpased: 1.861
start validation test
0.58881443299
0.622122403144
0.456107852218
0.526334540704
0.589047419861
65.221
batch start
#iterations: 280
currently lose_sum: 90.37013673782349
time_elpased: 1.888
batch start
#iterations: 281
currently lose_sum: 90.32106113433838
time_elpased: 1.858
batch start
#iterations: 282
currently lose_sum: 90.20229488611221
time_elpased: 1.887
batch start
#iterations: 283
currently lose_sum: 90.17468416690826
time_elpased: 1.877
batch start
#iterations: 284
currently lose_sum: 90.28717088699341
time_elpased: 1.862
batch start
#iterations: 285
currently lose_sum: 90.3411021232605
time_elpased: 1.861
batch start
#iterations: 286
currently lose_sum: 90.43409276008606
time_elpased: 1.884
batch start
#iterations: 287
currently lose_sum: 90.5768169760704
time_elpased: 1.876
batch start
#iterations: 288
currently lose_sum: 90.23647844791412
time_elpased: 1.862
batch start
#iterations: 289
currently lose_sum: 89.78773713111877
time_elpased: 1.873
batch start
#iterations: 290
currently lose_sum: 90.03047865629196
time_elpased: 1.892
batch start
#iterations: 291
currently lose_sum: 89.99449121952057
time_elpased: 1.862
batch start
#iterations: 292
currently lose_sum: 89.96135127544403
time_elpased: 1.86
batch start
#iterations: 293
currently lose_sum: 89.61845928430557
time_elpased: 1.872
batch start
#iterations: 294
currently lose_sum: 89.36077332496643
time_elpased: 1.853
batch start
#iterations: 295
currently lose_sum: 90.26501047611237
time_elpased: 1.886
batch start
#iterations: 296
currently lose_sum: 90.14575403928757
time_elpased: 1.86
batch start
#iterations: 297
currently lose_sum: 90.53033030033112
time_elpased: 1.895
batch start
#iterations: 298
currently lose_sum: 90.2712550163269
time_elpased: 1.852
batch start
#iterations: 299
currently lose_sum: 89.9192556142807
time_elpased: 1.845
start validation test
0.60912371134
0.613245595415
0.594627971596
0.603793301635
0.609149160847
63.776
batch start
#iterations: 300
currently lose_sum: 90.03097522258759
time_elpased: 1.869
batch start
#iterations: 301
currently lose_sum: 90.28016400337219
time_elpased: 1.861
batch start
#iterations: 302
currently lose_sum: 90.26018434762955
time_elpased: 1.921
batch start
#iterations: 303
currently lose_sum: 89.72512757778168
time_elpased: 1.855
batch start
#iterations: 304
currently lose_sum: 89.67834037542343
time_elpased: 1.884
batch start
#iterations: 305
currently lose_sum: 90.16609036922455
time_elpased: 1.846
batch start
#iterations: 306
currently lose_sum: 89.79816335439682
time_elpased: 1.92
batch start
#iterations: 307
currently lose_sum: 90.18529725074768
time_elpased: 1.838
batch start
#iterations: 308
currently lose_sum: 90.01958948373795
time_elpased: 1.94
batch start
#iterations: 309
currently lose_sum: 89.90800368785858
time_elpased: 1.905
batch start
#iterations: 310
currently lose_sum: 89.62909626960754
time_elpased: 1.896
batch start
#iterations: 311
currently lose_sum: 90.12124580144882
time_elpased: 1.908
batch start
#iterations: 312
currently lose_sum: 89.94096821546555
time_elpased: 1.952
batch start
#iterations: 313
currently lose_sum: 89.32573980093002
time_elpased: 1.843
batch start
#iterations: 314
currently lose_sum: 90.21567267179489
time_elpased: 1.878
batch start
#iterations: 315
currently lose_sum: 89.86490559577942
time_elpased: 1.894
batch start
#iterations: 316
currently lose_sum: 90.08132869005203
time_elpased: 1.895
batch start
#iterations: 317
currently lose_sum: 89.66470730304718
time_elpased: 1.871
batch start
#iterations: 318
currently lose_sum: 90.22680163383484
time_elpased: 1.879
batch start
#iterations: 319
currently lose_sum: 89.67847210168839
time_elpased: 1.881
start validation test
0.595257731959
0.623854429539
0.483379643923
0.544706018787
0.595454151198
65.249
batch start
#iterations: 320
currently lose_sum: 90.21482878923416
time_elpased: 1.916
batch start
#iterations: 321
currently lose_sum: 89.85659736394882
time_elpased: 1.876
batch start
#iterations: 322
currently lose_sum: 89.86087334156036
time_elpased: 1.868
batch start
#iterations: 323
currently lose_sum: 90.02238774299622
time_elpased: 1.872
batch start
#iterations: 324
currently lose_sum: 90.47022223472595
time_elpased: 1.854
batch start
#iterations: 325
currently lose_sum: 90.20238292217255
time_elpased: 1.848
batch start
#iterations: 326
currently lose_sum: 89.28216803073883
time_elpased: 1.894
batch start
#iterations: 327
currently lose_sum: 89.46951687335968
time_elpased: 1.86
batch start
#iterations: 328
currently lose_sum: 89.61897373199463
time_elpased: 1.901
batch start
#iterations: 329
currently lose_sum: 89.39772897958755
time_elpased: 1.891
batch start
#iterations: 330
currently lose_sum: 89.34760111570358
time_elpased: 1.847
batch start
#iterations: 331
currently lose_sum: 89.69238078594208
time_elpased: 1.876
batch start
#iterations: 332
currently lose_sum: 89.5132566690445
time_elpased: 1.855
batch start
#iterations: 333
currently lose_sum: 89.59813165664673
time_elpased: 1.849
batch start
#iterations: 334
currently lose_sum: 89.6674068570137
time_elpased: 1.882
batch start
#iterations: 335
currently lose_sum: 89.96237343549728
time_elpased: 1.899
batch start
#iterations: 336
currently lose_sum: 89.45747572183609
time_elpased: 1.907
batch start
#iterations: 337
currently lose_sum: 90.02074038982391
time_elpased: 1.848
batch start
#iterations: 338
currently lose_sum: 89.5633453130722
time_elpased: 1.891
batch start
#iterations: 339
currently lose_sum: 89.40778970718384
time_elpased: 1.874
start validation test
0.603917525773
0.621315192744
0.535762066481
0.575375773652
0.604037183194
64.315
batch start
#iterations: 340
currently lose_sum: 89.6741550564766
time_elpased: 1.86
batch start
#iterations: 341
currently lose_sum: 89.34878712892532
time_elpased: 1.879
batch start
#iterations: 342
currently lose_sum: 89.57388871908188
time_elpased: 1.872
batch start
#iterations: 343
currently lose_sum: 89.81363534927368
time_elpased: 1.9
batch start
#iterations: 344
currently lose_sum: 89.57300221920013
time_elpased: 1.873
batch start
#iterations: 345
currently lose_sum: 89.7352597117424
time_elpased: 1.915
batch start
#iterations: 346
currently lose_sum: 89.32397484779358
time_elpased: 1.891
batch start
#iterations: 347
currently lose_sum: 89.38953030109406
time_elpased: 1.9
batch start
#iterations: 348
currently lose_sum: 89.719542324543
time_elpased: 1.907
batch start
#iterations: 349
currently lose_sum: 89.61556941270828
time_elpased: 1.895
batch start
#iterations: 350
currently lose_sum: 89.11572593450546
time_elpased: 1.871
batch start
#iterations: 351
currently lose_sum: 89.14435321092606
time_elpased: 1.86
batch start
#iterations: 352
currently lose_sum: 89.3024924993515
time_elpased: 1.899
batch start
#iterations: 353
currently lose_sum: 89.53964555263519
time_elpased: 1.848
batch start
#iterations: 354
currently lose_sum: 89.12195181846619
time_elpased: 1.881
batch start
#iterations: 355
currently lose_sum: 89.64641892910004
time_elpased: 1.891
batch start
#iterations: 356
currently lose_sum: 89.63930016756058
time_elpased: 1.906
batch start
#iterations: 357
currently lose_sum: 89.05508440732956
time_elpased: 1.882
batch start
#iterations: 358
currently lose_sum: 89.42892235517502
time_elpased: 1.879
batch start
#iterations: 359
currently lose_sum: 89.18485605716705
time_elpased: 1.848
start validation test
0.589278350515
0.622599186878
0.457034064012
0.527121661721
0.589510525758
65.817
batch start
#iterations: 360
currently lose_sum: 89.23550081253052
time_elpased: 1.867
batch start
#iterations: 361
currently lose_sum: 89.79486358165741
time_elpased: 1.877
batch start
#iterations: 362
currently lose_sum: 89.34851002693176
time_elpased: 1.927
batch start
#iterations: 363
currently lose_sum: 89.2384203672409
time_elpased: 1.872
batch start
#iterations: 364
currently lose_sum: 89.88720774650574
time_elpased: 1.882
batch start
#iterations: 365
currently lose_sum: 89.75163877010345
time_elpased: 1.893
batch start
#iterations: 366
currently lose_sum: 89.78443711996078
time_elpased: 1.886
batch start
#iterations: 367
currently lose_sum: 89.41807001829147
time_elpased: 1.887
batch start
#iterations: 368
currently lose_sum: 89.27051514387131
time_elpased: 1.898
batch start
#iterations: 369
currently lose_sum: 89.2107145190239
time_elpased: 1.912
batch start
#iterations: 370
currently lose_sum: 88.45030748844147
time_elpased: 1.87
batch start
#iterations: 371
currently lose_sum: 89.07040590047836
time_elpased: 1.863
batch start
#iterations: 372
currently lose_sum: 89.63595622777939
time_elpased: 1.886
batch start
#iterations: 373
currently lose_sum: 88.7271957397461
time_elpased: 1.882
batch start
#iterations: 374
currently lose_sum: 88.56487083435059
time_elpased: 1.912
batch start
#iterations: 375
currently lose_sum: 89.41911101341248
time_elpased: 1.856
batch start
#iterations: 376
currently lose_sum: 88.91921806335449
time_elpased: 1.897
batch start
#iterations: 377
currently lose_sum: 89.17701321840286
time_elpased: 1.887
batch start
#iterations: 378
currently lose_sum: 88.99858886003494
time_elpased: 1.876
batch start
#iterations: 379
currently lose_sum: 89.16692453622818
time_elpased: 1.875
start validation test
0.603659793814
0.622375090514
0.530719357826
0.572904515914
0.603787852
64.702
batch start
#iterations: 380
currently lose_sum: 89.12349212169647
time_elpased: 1.864
batch start
#iterations: 381
currently lose_sum: 89.47114139795303
time_elpased: 1.851
batch start
#iterations: 382
currently lose_sum: 88.6024004817009
time_elpased: 1.9
batch start
#iterations: 383
currently lose_sum: 89.45015943050385
time_elpased: 1.882
batch start
#iterations: 384
currently lose_sum: 89.25064569711685
time_elpased: 1.885
batch start
#iterations: 385
currently lose_sum: 88.61220735311508
time_elpased: 1.866
batch start
#iterations: 386
currently lose_sum: 89.62761467695236
time_elpased: 1.905
batch start
#iterations: 387
currently lose_sum: 89.28687578439713
time_elpased: 1.879
batch start
#iterations: 388
currently lose_sum: 89.14655303955078
time_elpased: 1.836
batch start
#iterations: 389
currently lose_sum: 89.09532678127289
time_elpased: 1.841
batch start
#iterations: 390
currently lose_sum: 89.03941339254379
time_elpased: 1.827
batch start
#iterations: 391
currently lose_sum: 88.85355567932129
time_elpased: 1.799
batch start
#iterations: 392
currently lose_sum: 88.73182213306427
time_elpased: 1.811
batch start
#iterations: 393
currently lose_sum: 89.41719609498978
time_elpased: 1.833
batch start
#iterations: 394
currently lose_sum: 88.9189544916153
time_elpased: 1.846
batch start
#iterations: 395
currently lose_sum: 89.21070742607117
time_elpased: 1.762
batch start
#iterations: 396
currently lose_sum: 89.05318266153336
time_elpased: 1.722
batch start
#iterations: 397
currently lose_sum: 88.76911652088165
time_elpased: 1.74
batch start
#iterations: 398
currently lose_sum: 88.74292773008347
time_elpased: 1.711
batch start
#iterations: 399
currently lose_sum: 88.37397885322571
time_elpased: 1.713
start validation test
0.601237113402
0.604494144952
0.589688175363
0.59699937487
0.601257389344
64.661
acc: 0.611
pre: 0.638
rec: 0.516
F1: 0.570
auc: 0.611
