start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.54094982147217
time_elpased: 1.912
batch start
#iterations: 1
currently lose_sum: 100.00015467405319
time_elpased: 1.816
batch start
#iterations: 2
currently lose_sum: 99.7872765660286
time_elpased: 1.871
batch start
#iterations: 3
currently lose_sum: 99.70994019508362
time_elpased: 1.843
batch start
#iterations: 4
currently lose_sum: 99.47559052705765
time_elpased: 1.817
batch start
#iterations: 5
currently lose_sum: 99.14113652706146
time_elpased: 1.817
batch start
#iterations: 6
currently lose_sum: 99.26369297504425
time_elpased: 1.826
batch start
#iterations: 7
currently lose_sum: 98.86118143796921
time_elpased: 1.853
batch start
#iterations: 8
currently lose_sum: 98.95243120193481
time_elpased: 1.882
batch start
#iterations: 9
currently lose_sum: 98.8275495171547
time_elpased: 1.88
batch start
#iterations: 10
currently lose_sum: 98.64058291912079
time_elpased: 1.899
batch start
#iterations: 11
currently lose_sum: 98.59390288591385
time_elpased: 1.896
batch start
#iterations: 12
currently lose_sum: 98.27512741088867
time_elpased: 1.835
batch start
#iterations: 13
currently lose_sum: 98.45836144685745
time_elpased: 1.884
batch start
#iterations: 14
currently lose_sum: 98.2750598192215
time_elpased: 1.879
batch start
#iterations: 15
currently lose_sum: 97.8312737941742
time_elpased: 1.882
batch start
#iterations: 16
currently lose_sum: 97.73267740011215
time_elpased: 1.868
batch start
#iterations: 17
currently lose_sum: 98.03274512290955
time_elpased: 1.864
batch start
#iterations: 18
currently lose_sum: 97.69176256656647
time_elpased: 1.879
batch start
#iterations: 19
currently lose_sum: 97.6625304222107
time_elpased: 1.869
start validation test
0.623298969072
0.625586487332
0.617474529176
0.621504039776
0.623309194775
63.568
batch start
#iterations: 20
currently lose_sum: 97.59595030546188
time_elpased: 1.902
batch start
#iterations: 21
currently lose_sum: 97.64669501781464
time_elpased: 1.888
batch start
#iterations: 22
currently lose_sum: 97.46551704406738
time_elpased: 1.904
batch start
#iterations: 23
currently lose_sum: 97.19724881649017
time_elpased: 1.885
batch start
#iterations: 24
currently lose_sum: 97.38251578807831
time_elpased: 1.856
batch start
#iterations: 25
currently lose_sum: 97.26647126674652
time_elpased: 1.863
batch start
#iterations: 26
currently lose_sum: 97.20257472991943
time_elpased: 1.867
batch start
#iterations: 27
currently lose_sum: 97.08986961841583
time_elpased: 1.858
batch start
#iterations: 28
currently lose_sum: 96.86424660682678
time_elpased: 1.87
batch start
#iterations: 29
currently lose_sum: 97.18866109848022
time_elpased: 1.883
batch start
#iterations: 30
currently lose_sum: 97.04730182886124
time_elpased: 1.892
batch start
#iterations: 31
currently lose_sum: 96.67000126838684
time_elpased: 1.88
batch start
#iterations: 32
currently lose_sum: 96.60650664567947
time_elpased: 1.907
batch start
#iterations: 33
currently lose_sum: 96.30164790153503
time_elpased: 1.896
batch start
#iterations: 34
currently lose_sum: 96.66370338201523
time_elpased: 1.847
batch start
#iterations: 35
currently lose_sum: 96.44859778881073
time_elpased: 1.871
batch start
#iterations: 36
currently lose_sum: 96.58327174186707
time_elpased: 1.868
batch start
#iterations: 37
currently lose_sum: 96.34242475032806
time_elpased: 1.857
batch start
#iterations: 38
currently lose_sum: 96.22948580980301
time_elpased: 1.866
batch start
#iterations: 39
currently lose_sum: 95.72746044397354
time_elpased: 1.863
start validation test
0.629948453608
0.624095442988
0.6567870742
0.640024068595
0.62990133427
62.802
batch start
#iterations: 40
currently lose_sum: 95.90730649232864
time_elpased: 1.891
batch start
#iterations: 41
currently lose_sum: 96.04053741693497
time_elpased: 1.87
batch start
#iterations: 42
currently lose_sum: 95.75839477777481
time_elpased: 1.871
batch start
#iterations: 43
currently lose_sum: 95.69424766302109
time_elpased: 1.879
batch start
#iterations: 44
currently lose_sum: 95.37923675775528
time_elpased: 1.92
batch start
#iterations: 45
currently lose_sum: 95.71480774879456
time_elpased: 1.868
batch start
#iterations: 46
currently lose_sum: 95.1953649520874
time_elpased: 1.87
batch start
#iterations: 47
currently lose_sum: 95.70110368728638
time_elpased: 1.89
batch start
#iterations: 48
currently lose_sum: 95.59994632005692
time_elpased: 1.85
batch start
#iterations: 49
currently lose_sum: 95.75162994861603
time_elpased: 1.898
batch start
#iterations: 50
currently lose_sum: 95.53468137979507
time_elpased: 1.895
batch start
#iterations: 51
currently lose_sum: 95.22556263208389
time_elpased: 1.874
batch start
#iterations: 52
currently lose_sum: 95.42866110801697
time_elpased: 1.873
batch start
#iterations: 53
currently lose_sum: 95.28130662441254
time_elpased: 1.875
batch start
#iterations: 54
currently lose_sum: 95.59865307807922
time_elpased: 1.875
batch start
#iterations: 55
currently lose_sum: 95.17234599590302
time_elpased: 1.888
batch start
#iterations: 56
currently lose_sum: 95.01658248901367
time_elpased: 1.881
batch start
#iterations: 57
currently lose_sum: 94.79835855960846
time_elpased: 1.855
batch start
#iterations: 58
currently lose_sum: 94.72353994846344
time_elpased: 1.857
batch start
#iterations: 59
currently lose_sum: 95.01086592674255
time_elpased: 1.92
start validation test
0.636597938144
0.623278173246
0.69383554595
0.656666991331
0.636497448696
62.254
batch start
#iterations: 60
currently lose_sum: 94.9330917596817
time_elpased: 1.907
batch start
#iterations: 61
currently lose_sum: 94.7806807756424
time_elpased: 1.883
batch start
#iterations: 62
currently lose_sum: 94.66726040840149
time_elpased: 1.857
batch start
#iterations: 63
currently lose_sum: 94.89545947313309
time_elpased: 1.874
batch start
#iterations: 64
currently lose_sum: 94.61447161436081
time_elpased: 1.883
batch start
#iterations: 65
currently lose_sum: 94.17082285881042
time_elpased: 1.87
batch start
#iterations: 66
currently lose_sum: 94.34614861011505
time_elpased: 1.883
batch start
#iterations: 67
currently lose_sum: 94.274478495121
time_elpased: 1.891
batch start
#iterations: 68
currently lose_sum: 94.48485738039017
time_elpased: 1.88
batch start
#iterations: 69
currently lose_sum: 94.69061440229416
time_elpased: 1.864
batch start
#iterations: 70
currently lose_sum: 94.40520286560059
time_elpased: 1.896
batch start
#iterations: 71
currently lose_sum: 94.2192103266716
time_elpased: 1.834
batch start
#iterations: 72
currently lose_sum: 94.15773439407349
time_elpased: 1.877
batch start
#iterations: 73
currently lose_sum: 94.2539991736412
time_elpased: 1.868
batch start
#iterations: 74
currently lose_sum: 94.2415702342987
time_elpased: 1.843
batch start
#iterations: 75
currently lose_sum: 93.9195625782013
time_elpased: 1.871
batch start
#iterations: 76
currently lose_sum: 93.62820637226105
time_elpased: 1.895
batch start
#iterations: 77
currently lose_sum: 94.24475800991058
time_elpased: 1.853
batch start
#iterations: 78
currently lose_sum: 94.0964982509613
time_elpased: 1.885
batch start
#iterations: 79
currently lose_sum: 94.15198063850403
time_elpased: 1.878
start validation test
0.616391752577
0.623762376238
0.589996912627
0.606409985191
0.61643809279
62.799
batch start
#iterations: 80
currently lose_sum: 94.25582981109619
time_elpased: 1.881
batch start
#iterations: 81
currently lose_sum: 94.12129980325699
time_elpased: 1.942
batch start
#iterations: 82
currently lose_sum: 93.97419553995132
time_elpased: 1.897
batch start
#iterations: 83
currently lose_sum: 93.79761707782745
time_elpased: 1.892
batch start
#iterations: 84
currently lose_sum: 93.75379848480225
time_elpased: 1.884
batch start
#iterations: 85
currently lose_sum: 94.07288265228271
time_elpased: 1.874
batch start
#iterations: 86
currently lose_sum: 93.80120527744293
time_elpased: 1.866
batch start
#iterations: 87
currently lose_sum: 93.52875131368637
time_elpased: 1.895
batch start
#iterations: 88
currently lose_sum: 93.82522678375244
time_elpased: 1.891
batch start
#iterations: 89
currently lose_sum: 92.95554894208908
time_elpased: 1.877
batch start
#iterations: 90
currently lose_sum: 93.50058543682098
time_elpased: 1.905
batch start
#iterations: 91
currently lose_sum: 93.6513774394989
time_elpased: 1.889
batch start
#iterations: 92
currently lose_sum: 93.25090408325195
time_elpased: 1.898
batch start
#iterations: 93
currently lose_sum: 93.51394987106323
time_elpased: 1.885
batch start
#iterations: 94
currently lose_sum: 93.46576231718063
time_elpased: 1.873
batch start
#iterations: 95
currently lose_sum: 93.20818746089935
time_elpased: 1.88
batch start
#iterations: 96
currently lose_sum: 93.26936733722687
time_elpased: 1.863
batch start
#iterations: 97
currently lose_sum: 92.90199393033981
time_elpased: 1.902
batch start
#iterations: 98
currently lose_sum: 93.42034530639648
time_elpased: 1.873
batch start
#iterations: 99
currently lose_sum: 92.83420795202255
time_elpased: 1.867
start validation test
0.624742268041
0.642698208221
0.564783369353
0.601226993865
0.624847535136
62.897
batch start
#iterations: 100
currently lose_sum: 93.46960574388504
time_elpased: 1.884
batch start
#iterations: 101
currently lose_sum: 93.0221735239029
time_elpased: 1.907
batch start
#iterations: 102
currently lose_sum: 92.70218026638031
time_elpased: 1.876
batch start
#iterations: 103
currently lose_sum: 92.73588573932648
time_elpased: 1.891
batch start
#iterations: 104
currently lose_sum: 93.3369979262352
time_elpased: 1.887
batch start
#iterations: 105
currently lose_sum: 93.1571055650711
time_elpased: 1.89
batch start
#iterations: 106
currently lose_sum: 92.89678090810776
time_elpased: 1.907
batch start
#iterations: 107
currently lose_sum: 92.60592675209045
time_elpased: 1.856
batch start
#iterations: 108
currently lose_sum: 92.87124919891357
time_elpased: 1.897
batch start
#iterations: 109
currently lose_sum: 93.03503102064133
time_elpased: 1.891
batch start
#iterations: 110
currently lose_sum: 92.6757305264473
time_elpased: 1.864
batch start
#iterations: 111
currently lose_sum: 92.90839570760727
time_elpased: 1.878
batch start
#iterations: 112
currently lose_sum: 92.42353129386902
time_elpased: 1.879
batch start
#iterations: 113
currently lose_sum: 92.37276375293732
time_elpased: 1.888
batch start
#iterations: 114
currently lose_sum: 92.6053677201271
time_elpased: 1.878
batch start
#iterations: 115
currently lose_sum: 92.57482939958572
time_elpased: 1.876
batch start
#iterations: 116
currently lose_sum: 92.62698459625244
time_elpased: 1.866
batch start
#iterations: 117
currently lose_sum: 92.50015586614609
time_elpased: 1.886
batch start
#iterations: 118
currently lose_sum: 92.73840939998627
time_elpased: 1.876
batch start
#iterations: 119
currently lose_sum: 92.37557309865952
time_elpased: 1.892
start validation test
0.627628865979
0.63197458973
0.614284244108
0.62300386181
0.627652294521
62.383
batch start
#iterations: 120
currently lose_sum: 92.48416352272034
time_elpased: 1.892
batch start
#iterations: 121
currently lose_sum: 92.35520446300507
time_elpased: 1.908
batch start
#iterations: 122
currently lose_sum: 92.3157387971878
time_elpased: 1.91
batch start
#iterations: 123
currently lose_sum: 91.94827318191528
time_elpased: 1.914
batch start
#iterations: 124
currently lose_sum: 92.05774009227753
time_elpased: 1.866
batch start
#iterations: 125
currently lose_sum: 92.22698193788528
time_elpased: 1.889
batch start
#iterations: 126
currently lose_sum: 92.36866891384125
time_elpased: 1.894
batch start
#iterations: 127
currently lose_sum: 92.29401957988739
time_elpased: 1.892
batch start
#iterations: 128
currently lose_sum: 92.51680117845535
time_elpased: 1.891
batch start
#iterations: 129
currently lose_sum: 92.05007994174957
time_elpased: 1.891
batch start
#iterations: 130
currently lose_sum: 91.91829764842987
time_elpased: 1.873
batch start
#iterations: 131
currently lose_sum: 91.91639572381973
time_elpased: 1.9
batch start
#iterations: 132
currently lose_sum: 92.05466508865356
time_elpased: 1.872
batch start
#iterations: 133
currently lose_sum: 91.84104609489441
time_elpased: 1.896
batch start
#iterations: 134
currently lose_sum: 92.17102545499802
time_elpased: 1.865
batch start
#iterations: 135
currently lose_sum: 92.1818140745163
time_elpased: 1.865
batch start
#iterations: 136
currently lose_sum: 91.608178794384
time_elpased: 1.881
batch start
#iterations: 137
currently lose_sum: 91.97210168838501
time_elpased: 1.893
batch start
#iterations: 138
currently lose_sum: 91.57020509243011
time_elpased: 1.886
batch start
#iterations: 139
currently lose_sum: 91.37844109535217
time_elpased: 1.902
start validation test
0.616134020619
0.625442086649
0.582381393434
0.603144151346
0.616193278562
62.935
batch start
#iterations: 140
currently lose_sum: 91.85146284103394
time_elpased: 1.856
batch start
#iterations: 141
currently lose_sum: 91.89977276325226
time_elpased: 1.929
batch start
#iterations: 142
currently lose_sum: 91.90910404920578
time_elpased: 1.877
batch start
#iterations: 143
currently lose_sum: 92.24965023994446
time_elpased: 1.891
batch start
#iterations: 144
currently lose_sum: 91.49794691801071
time_elpased: 1.886
batch start
#iterations: 145
currently lose_sum: 92.09136563539505
time_elpased: 1.879
batch start
#iterations: 146
currently lose_sum: 91.80722576379776
time_elpased: 1.873
batch start
#iterations: 147
currently lose_sum: 90.95698112249374
time_elpased: 1.895
batch start
#iterations: 148
currently lose_sum: 91.8337299823761
time_elpased: 1.896
batch start
#iterations: 149
currently lose_sum: 91.3718456029892
time_elpased: 1.898
batch start
#iterations: 150
currently lose_sum: 91.47988760471344
time_elpased: 1.867
batch start
#iterations: 151
currently lose_sum: 91.40036910772324
time_elpased: 1.884
batch start
#iterations: 152
currently lose_sum: 91.74348592758179
time_elpased: 1.878
batch start
#iterations: 153
currently lose_sum: 91.52229690551758
time_elpased: 1.872
batch start
#iterations: 154
currently lose_sum: 91.85722994804382
time_elpased: 1.886
batch start
#iterations: 155
currently lose_sum: 91.39747905731201
time_elpased: 1.899
batch start
#iterations: 156
currently lose_sum: 91.24305772781372
time_elpased: 1.849
batch start
#iterations: 157
currently lose_sum: 91.89865756034851
time_elpased: 1.875
batch start
#iterations: 158
currently lose_sum: 91.49662899971008
time_elpased: 1.865
batch start
#iterations: 159
currently lose_sum: 92.00199055671692
time_elpased: 1.868
start validation test
0.619587628866
0.626365307667
0.596068745498
0.6108415946
0.619628919893
63.041
batch start
#iterations: 160
currently lose_sum: 90.9175608754158
time_elpased: 2.014
batch start
#iterations: 161
currently lose_sum: 91.37246704101562
time_elpased: 1.912
batch start
#iterations: 162
currently lose_sum: 90.99913597106934
time_elpased: 1.866
batch start
#iterations: 163
currently lose_sum: 91.30717998743057
time_elpased: 1.891
batch start
#iterations: 164
currently lose_sum: 91.24259740114212
time_elpased: 1.89
batch start
#iterations: 165
currently lose_sum: 91.3821730017662
time_elpased: 1.895
batch start
#iterations: 166
currently lose_sum: 91.20018821954727
time_elpased: 1.842
batch start
#iterations: 167
currently lose_sum: 91.02846074104309
time_elpased: 1.887
batch start
#iterations: 168
currently lose_sum: 90.43838500976562
time_elpased: 1.915
batch start
#iterations: 169
currently lose_sum: 91.06553679704666
time_elpased: 1.949
batch start
#iterations: 170
currently lose_sum: 90.70298290252686
time_elpased: 1.895
batch start
#iterations: 171
currently lose_sum: 90.9051045179367
time_elpased: 1.885
batch start
#iterations: 172
currently lose_sum: 91.45890843868256
time_elpased: 1.88
batch start
#iterations: 173
currently lose_sum: 90.78378653526306
time_elpased: 1.915
batch start
#iterations: 174
currently lose_sum: 90.66454768180847
time_elpased: 1.904
batch start
#iterations: 175
currently lose_sum: 90.747507750988
time_elpased: 1.876
batch start
#iterations: 176
currently lose_sum: 90.71709448099136
time_elpased: 1.842
batch start
#iterations: 177
currently lose_sum: 90.70836144685745
time_elpased: 1.862
batch start
#iterations: 178
currently lose_sum: 90.47105318307877
time_elpased: 1.866
batch start
#iterations: 179
currently lose_sum: 91.02225613594055
time_elpased: 1.895
start validation test
0.618865979381
0.631020868584
0.575692086035
0.602088042191
0.61894177781
63.658
batch start
#iterations: 180
currently lose_sum: 91.04008519649506
time_elpased: 1.872
batch start
#iterations: 181
currently lose_sum: 90.6983894109726
time_elpased: 1.892
batch start
#iterations: 182
currently lose_sum: 90.69413512945175
time_elpased: 1.865
batch start
#iterations: 183
currently lose_sum: 90.3393183350563
time_elpased: 1.875
batch start
#iterations: 184
currently lose_sum: 91.52204656600952
time_elpased: 1.887
batch start
#iterations: 185
currently lose_sum: 90.33976459503174
time_elpased: 1.885
batch start
#iterations: 186
currently lose_sum: 90.68386971950531
time_elpased: 1.856
batch start
#iterations: 187
currently lose_sum: 90.30831444263458
time_elpased: 1.884
batch start
#iterations: 188
currently lose_sum: 90.71439987421036
time_elpased: 1.854
batch start
#iterations: 189
currently lose_sum: 90.35291200876236
time_elpased: 1.887
batch start
#iterations: 190
currently lose_sum: 91.04990166425705
time_elpased: 1.871
batch start
#iterations: 191
currently lose_sum: 90.5326356291771
time_elpased: 1.897
batch start
#iterations: 192
currently lose_sum: 90.3799495100975
time_elpased: 1.89
batch start
#iterations: 193
currently lose_sum: 90.73193991184235
time_elpased: 1.882
batch start
#iterations: 194
currently lose_sum: 90.30803179740906
time_elpased: 1.86
batch start
#iterations: 195
currently lose_sum: 90.29990857839584
time_elpased: 1.891
batch start
#iterations: 196
currently lose_sum: 90.8865385055542
time_elpased: 1.871
batch start
#iterations: 197
currently lose_sum: 90.94775223731995
time_elpased: 1.888
batch start
#iterations: 198
currently lose_sum: 90.27544528245926
time_elpased: 1.867
batch start
#iterations: 199
currently lose_sum: 90.51357489824295
time_elpased: 1.868
start validation test
0.612628865979
0.621711253593
0.578779458681
0.599477695464
0.612688293835
63.863
batch start
#iterations: 200
currently lose_sum: 90.11356228590012
time_elpased: 1.873
batch start
#iterations: 201
currently lose_sum: 90.26478552818298
time_elpased: 1.877
batch start
#iterations: 202
currently lose_sum: 90.3251296877861
time_elpased: 1.902
batch start
#iterations: 203
currently lose_sum: 90.54636919498444
time_elpased: 1.899
batch start
#iterations: 204
currently lose_sum: 90.15250986814499
time_elpased: 1.86
batch start
#iterations: 205
currently lose_sum: 90.1056022644043
time_elpased: 1.883
batch start
#iterations: 206
currently lose_sum: 90.40519511699677
time_elpased: 1.876
batch start
#iterations: 207
currently lose_sum: 90.41573357582092
time_elpased: 1.906
batch start
#iterations: 208
currently lose_sum: 90.00395458936691
time_elpased: 1.907
batch start
#iterations: 209
currently lose_sum: 90.48759114742279
time_elpased: 1.878
batch start
#iterations: 210
currently lose_sum: 89.87860482931137
time_elpased: 1.878
batch start
#iterations: 211
currently lose_sum: 89.89067041873932
time_elpased: 1.872
batch start
#iterations: 212
currently lose_sum: 89.56111317873001
time_elpased: 1.862
batch start
#iterations: 213
currently lose_sum: 90.0235498547554
time_elpased: 1.899
batch start
#iterations: 214
currently lose_sum: 89.79337280988693
time_elpased: 1.869
batch start
#iterations: 215
currently lose_sum: 90.1978434920311
time_elpased: 1.899
batch start
#iterations: 216
currently lose_sum: 90.00871115922928
time_elpased: 1.87
batch start
#iterations: 217
currently lose_sum: 90.23259460926056
time_elpased: 1.887
batch start
#iterations: 218
currently lose_sum: 90.27598196268082
time_elpased: 1.839
batch start
#iterations: 219
currently lose_sum: 90.14329707622528
time_elpased: 1.908
start validation test
0.607268041237
0.615047159465
0.577132859936
0.595487125033
0.607320948196
64.003
batch start
#iterations: 220
currently lose_sum: 89.90986949205399
time_elpased: 1.847
batch start
#iterations: 221
currently lose_sum: 89.44808667898178
time_elpased: 1.89
batch start
#iterations: 222
currently lose_sum: 90.27020227909088
time_elpased: 1.872
batch start
#iterations: 223
currently lose_sum: 89.80525368452072
time_elpased: 1.889
batch start
#iterations: 224
currently lose_sum: 89.21710401773453
time_elpased: 1.85
batch start
#iterations: 225
currently lose_sum: 89.78286612033844
time_elpased: 1.879
batch start
#iterations: 226
currently lose_sum: 89.60375148057938
time_elpased: 1.856
batch start
#iterations: 227
currently lose_sum: 90.00451427698135
time_elpased: 1.875
batch start
#iterations: 228
currently lose_sum: 89.40351611375809
time_elpased: 1.873
batch start
#iterations: 229
currently lose_sum: 89.31789863109589
time_elpased: 1.894
batch start
#iterations: 230
currently lose_sum: 89.47988015413284
time_elpased: 1.866
batch start
#iterations: 231
currently lose_sum: 89.73586738109589
time_elpased: 1.9
batch start
#iterations: 232
currently lose_sum: 89.34798902273178
time_elpased: 1.889
batch start
#iterations: 233
currently lose_sum: 89.90478885173798
time_elpased: 1.844
batch start
#iterations: 234
currently lose_sum: 89.98336309194565
time_elpased: 1.92
batch start
#iterations: 235
currently lose_sum: 89.50570225715637
time_elpased: 1.882
batch start
#iterations: 236
currently lose_sum: 89.57905119657516
time_elpased: 1.865
batch start
#iterations: 237
currently lose_sum: 89.63584119081497
time_elpased: 1.878
batch start
#iterations: 238
currently lose_sum: 89.94340926408768
time_elpased: 1.861
batch start
#iterations: 239
currently lose_sum: 89.68865519762039
time_elpased: 1.873
start validation test
0.608608247423
0.622406639004
0.555727076258
0.587179905399
0.608701088475
64.256
batch start
#iterations: 240
currently lose_sum: 89.58744484186172
time_elpased: 1.88
batch start
#iterations: 241
currently lose_sum: 89.59774976968765
time_elpased: 1.875
batch start
#iterations: 242
currently lose_sum: 89.74513298273087
time_elpased: 1.944
batch start
#iterations: 243
currently lose_sum: 89.76921671628952
time_elpased: 1.902
batch start
#iterations: 244
currently lose_sum: 89.81987076997757
time_elpased: 1.863
batch start
#iterations: 245
currently lose_sum: 89.49866873025894
time_elpased: 1.909
batch start
#iterations: 246
currently lose_sum: 88.98841959238052
time_elpased: 1.845
batch start
#iterations: 247
currently lose_sum: 89.10464084148407
time_elpased: 1.879
batch start
#iterations: 248
currently lose_sum: 89.81057751178741
time_elpased: 1.893
batch start
#iterations: 249
currently lose_sum: 89.65162426233292
time_elpased: 1.874
batch start
#iterations: 250
currently lose_sum: 89.40646630525589
time_elpased: 1.879
batch start
#iterations: 251
currently lose_sum: 89.3361434340477
time_elpased: 1.872
batch start
#iterations: 252
currently lose_sum: 89.69138616323471
time_elpased: 1.861
batch start
#iterations: 253
currently lose_sum: 89.37818849086761
time_elpased: 1.915
batch start
#iterations: 254
currently lose_sum: 89.42243564128876
time_elpased: 1.894
batch start
#iterations: 255
currently lose_sum: 89.4688001871109
time_elpased: 1.839
batch start
#iterations: 256
currently lose_sum: 89.20449262857437
time_elpased: 1.862
batch start
#iterations: 257
currently lose_sum: 89.01965117454529
time_elpased: 1.865
batch start
#iterations: 258
currently lose_sum: 89.3978385925293
time_elpased: 1.845
batch start
#iterations: 259
currently lose_sum: 88.88605326414108
time_elpased: 1.89
start validation test
0.608350515464
0.632321718496
0.521045590203
0.571315730084
0.608503792726
64.863
batch start
#iterations: 260
currently lose_sum: 89.04084557294846
time_elpased: 1.873
batch start
#iterations: 261
currently lose_sum: 89.07204014062881
time_elpased: 1.894
batch start
#iterations: 262
currently lose_sum: 89.27670007944107
time_elpased: 1.896
batch start
#iterations: 263
currently lose_sum: 88.49839985370636
time_elpased: 1.91
batch start
#iterations: 264
currently lose_sum: 88.43789559602737
time_elpased: 1.86
batch start
#iterations: 265
currently lose_sum: 89.47477507591248
time_elpased: 1.919
batch start
#iterations: 266
currently lose_sum: 89.2967237830162
time_elpased: 1.94
batch start
#iterations: 267
currently lose_sum: 88.8929352760315
time_elpased: 1.881
batch start
#iterations: 268
currently lose_sum: 88.68438911437988
time_elpased: 1.872
batch start
#iterations: 269
currently lose_sum: 89.4828610420227
time_elpased: 1.896
batch start
#iterations: 270
currently lose_sum: 88.44275397062302
time_elpased: 1.863
batch start
#iterations: 271
currently lose_sum: 88.98370599746704
time_elpased: 1.912
batch start
#iterations: 272
currently lose_sum: 89.43731099367142
time_elpased: 1.873
batch start
#iterations: 273
currently lose_sum: 88.91405832767487
time_elpased: 1.912
batch start
#iterations: 274
currently lose_sum: 88.73862540721893
time_elpased: 1.891
batch start
#iterations: 275
currently lose_sum: 88.7734015583992
time_elpased: 1.893
batch start
#iterations: 276
currently lose_sum: 88.7218410372734
time_elpased: 1.897
batch start
#iterations: 277
currently lose_sum: 89.17753410339355
time_elpased: 1.887
batch start
#iterations: 278
currently lose_sum: 88.81840497255325
time_elpased: 1.888
batch start
#iterations: 279
currently lose_sum: 88.80209362506866
time_elpased: 1.862
start validation test
0.624639175258
0.621859673706
0.639394874961
0.630505378526
0.624613269351
64.497
batch start
#iterations: 280
currently lose_sum: 88.14586389064789
time_elpased: 1.872
batch start
#iterations: 281
currently lose_sum: 88.91049402952194
time_elpased: 1.866
batch start
#iterations: 282
currently lose_sum: 88.62568300962448
time_elpased: 1.906
batch start
#iterations: 283
currently lose_sum: 88.90699255466461
time_elpased: 1.903
batch start
#iterations: 284
currently lose_sum: 88.54558038711548
time_elpased: 1.924
batch start
#iterations: 285
currently lose_sum: 88.28629505634308
time_elpased: 1.872
batch start
#iterations: 286
currently lose_sum: 88.64708042144775
time_elpased: 1.884
batch start
#iterations: 287
currently lose_sum: 88.41858559846878
time_elpased: 1.97
batch start
#iterations: 288
currently lose_sum: 88.6760441660881
time_elpased: 1.904
batch start
#iterations: 289
currently lose_sum: 88.32720619440079
time_elpased: 1.875
batch start
#iterations: 290
currently lose_sum: 88.8699409365654
time_elpased: 1.862
batch start
#iterations: 291
currently lose_sum: 88.21648168563843
time_elpased: 1.855
batch start
#iterations: 292
currently lose_sum: 88.14977097511292
time_elpased: 1.859
batch start
#iterations: 293
currently lose_sum: 88.72483533620834
time_elpased: 1.868
batch start
#iterations: 294
currently lose_sum: 88.47672581672668
time_elpased: 1.932
batch start
#iterations: 295
currently lose_sum: 88.25960689783096
time_elpased: 1.882
batch start
#iterations: 296
currently lose_sum: 88.82148200273514
time_elpased: 1.877
batch start
#iterations: 297
currently lose_sum: 87.8630273938179
time_elpased: 1.874
batch start
#iterations: 298
currently lose_sum: 88.11533832550049
time_elpased: 1.892
batch start
#iterations: 299
currently lose_sum: 88.43627768754959
time_elpased: 1.823
start validation test
0.623144329897
0.621515151515
0.63322012967
0.627313044808
0.623126640276
64.262
batch start
#iterations: 300
currently lose_sum: 88.04528850317001
time_elpased: 1.912
batch start
#iterations: 301
currently lose_sum: 88.77365851402283
time_elpased: 1.887
batch start
#iterations: 302
currently lose_sum: 88.04808473587036
time_elpased: 1.863
batch start
#iterations: 303
currently lose_sum: 88.48942166566849
time_elpased: 1.933
batch start
#iterations: 304
currently lose_sum: 88.70502561330795
time_elpased: 1.871
batch start
#iterations: 305
currently lose_sum: 88.35636079311371
time_elpased: 1.874
batch start
#iterations: 306
currently lose_sum: 88.08896094560623
time_elpased: 1.879
batch start
#iterations: 307
currently lose_sum: 88.04021447896957
time_elpased: 1.89
batch start
#iterations: 308
currently lose_sum: 88.31062805652618
time_elpased: 1.905
batch start
#iterations: 309
currently lose_sum: 88.64279639720917
time_elpased: 1.879
batch start
#iterations: 310
currently lose_sum: 88.21906960010529
time_elpased: 1.85
batch start
#iterations: 311
currently lose_sum: 88.03289103507996
time_elpased: 1.857
batch start
#iterations: 312
currently lose_sum: 87.68851560354233
time_elpased: 1.862
batch start
#iterations: 313
currently lose_sum: 87.5548689365387
time_elpased: 1.87
batch start
#iterations: 314
currently lose_sum: 87.92256742715836
time_elpased: 1.924
batch start
#iterations: 315
currently lose_sum: 87.95062190294266
time_elpased: 1.86
batch start
#iterations: 316
currently lose_sum: 88.27421742677689
time_elpased: 1.864
batch start
#iterations: 317
currently lose_sum: 88.48951387405396
time_elpased: 1.942
batch start
#iterations: 318
currently lose_sum: 87.72327429056168
time_elpased: 1.871
batch start
#iterations: 319
currently lose_sum: 88.26530522108078
time_elpased: 1.933
start validation test
0.609226804124
0.621418826739
0.562519296079
0.590503970183
0.609308806358
64.912
batch start
#iterations: 320
currently lose_sum: 88.25027334690094
time_elpased: 1.85
batch start
#iterations: 321
currently lose_sum: 88.61255449056625
time_elpased: 1.871
batch start
#iterations: 322
currently lose_sum: 88.25267744064331
time_elpased: 1.854
batch start
#iterations: 323
currently lose_sum: 87.93536472320557
time_elpased: 1.876
batch start
#iterations: 324
currently lose_sum: 88.52263981103897
time_elpased: 1.858
batch start
#iterations: 325
currently lose_sum: 88.35045492649078
time_elpased: 1.957
batch start
#iterations: 326
currently lose_sum: 87.92547458410263
time_elpased: 1.876
batch start
#iterations: 327
currently lose_sum: 87.69563418626785
time_elpased: 1.884
batch start
#iterations: 328
currently lose_sum: 87.77988612651825
time_elpased: 1.915
batch start
#iterations: 329
currently lose_sum: 87.74724316596985
time_elpased: 1.874
batch start
#iterations: 330
currently lose_sum: 88.08701479434967
time_elpased: 1.859
batch start
#iterations: 331
currently lose_sum: 87.97739911079407
time_elpased: 1.857
batch start
#iterations: 332
currently lose_sum: 87.52980864048004
time_elpased: 1.891
batch start
#iterations: 333
currently lose_sum: 87.44232976436615
time_elpased: 1.876
batch start
#iterations: 334
currently lose_sum: 87.56145578622818
time_elpased: 1.858
batch start
#iterations: 335
currently lose_sum: 87.57435405254364
time_elpased: 1.864
batch start
#iterations: 336
currently lose_sum: 87.30885207653046
time_elpased: 1.858
batch start
#iterations: 337
currently lose_sum: 88.41383844614029
time_elpased: 1.88
batch start
#iterations: 338
currently lose_sum: 87.72530287504196
time_elpased: 1.869
batch start
#iterations: 339
currently lose_sum: 87.3030115365982
time_elpased: 1.9
start validation test
0.613762886598
0.634559535334
0.5396727385
0.583282353595
0.613892963281
66.140
batch start
#iterations: 340
currently lose_sum: 87.73238748311996
time_elpased: 1.863
batch start
#iterations: 341
currently lose_sum: 88.25990641117096
time_elpased: 1.899
batch start
#iterations: 342
currently lose_sum: 87.73780179023743
time_elpased: 1.922
batch start
#iterations: 343
currently lose_sum: 87.90037477016449
time_elpased: 1.88
batch start
#iterations: 344
currently lose_sum: 87.30375963449478
time_elpased: 1.884
batch start
#iterations: 345
currently lose_sum: 87.83624505996704
time_elpased: 1.887
batch start
#iterations: 346
currently lose_sum: 87.9681653380394
time_elpased: 1.88
batch start
#iterations: 347
currently lose_sum: 87.85555309057236
time_elpased: 1.892
batch start
#iterations: 348
currently lose_sum: 87.09667629003525
time_elpased: 1.882
batch start
#iterations: 349
currently lose_sum: 87.51332205533981
time_elpased: 1.89
batch start
#iterations: 350
currently lose_sum: 87.1738812327385
time_elpased: 1.852
batch start
#iterations: 351
currently lose_sum: 87.49485874176025
time_elpased: 1.873
batch start
#iterations: 352
currently lose_sum: 87.67140245437622
time_elpased: 1.857
batch start
#iterations: 353
currently lose_sum: 87.87129664421082
time_elpased: 1.897
batch start
#iterations: 354
currently lose_sum: 88.07548713684082
time_elpased: 1.848
batch start
#iterations: 355
currently lose_sum: 88.051025390625
time_elpased: 1.876
batch start
#iterations: 356
currently lose_sum: 88.01012617349625
time_elpased: 1.853
batch start
#iterations: 357
currently lose_sum: 87.44637596607208
time_elpased: 1.884
batch start
#iterations: 358
currently lose_sum: 87.59417587518692
time_elpased: 1.858
batch start
#iterations: 359
currently lose_sum: 87.37120407819748
time_elpased: 1.867
start validation test
0.62118556701
0.619935170178
0.629824019759
0.624840471693
0.621170400874
64.876
batch start
#iterations: 360
currently lose_sum: 87.33686619997025
time_elpased: 1.895
batch start
#iterations: 361
currently lose_sum: 88.05296629667282
time_elpased: 1.88
batch start
#iterations: 362
currently lose_sum: 87.34038877487183
time_elpased: 1.868
batch start
#iterations: 363
currently lose_sum: 87.26318973302841
time_elpased: 1.859
batch start
#iterations: 364
currently lose_sum: 87.58377784490585
time_elpased: 1.989
batch start
#iterations: 365
currently lose_sum: 87.49136906862259
time_elpased: 1.872
batch start
#iterations: 366
currently lose_sum: 87.00504660606384
time_elpased: 1.891
batch start
#iterations: 367
currently lose_sum: 87.31219226121902
time_elpased: 1.876
batch start
#iterations: 368
currently lose_sum: 87.1271082162857
time_elpased: 1.944
batch start
#iterations: 369
currently lose_sum: 87.57115352153778
time_elpased: 1.935
batch start
#iterations: 370
currently lose_sum: 86.66232228279114
time_elpased: 1.887
batch start
#iterations: 371
currently lose_sum: 87.03244429826736
time_elpased: 1.888
batch start
#iterations: 372
currently lose_sum: 87.06598949432373
time_elpased: 1.853
batch start
#iterations: 373
currently lose_sum: 87.27431690692902
time_elpased: 1.878
batch start
#iterations: 374
currently lose_sum: 87.01606965065002
time_elpased: 1.896
batch start
#iterations: 375
currently lose_sum: 87.00073021650314
time_elpased: 1.933
batch start
#iterations: 376
currently lose_sum: 86.77092516422272
time_elpased: 1.883
batch start
#iterations: 377
currently lose_sum: 86.79207015037537
time_elpased: 1.874
batch start
#iterations: 378
currently lose_sum: 87.24686193466187
time_elpased: 1.885
batch start
#iterations: 379
currently lose_sum: 87.4305852651596
time_elpased: 1.885
start validation test
0.619793814433
0.618820424322
0.627354121642
0.623058053966
0.619780541148
64.874
batch start
#iterations: 380
currently lose_sum: 87.41881531476974
time_elpased: 1.891
batch start
#iterations: 381
currently lose_sum: 86.8342667222023
time_elpased: 1.875
batch start
#iterations: 382
currently lose_sum: 87.53533220291138
time_elpased: 1.878
batch start
#iterations: 383
currently lose_sum: 87.55793476104736
time_elpased: 1.86
batch start
#iterations: 384
currently lose_sum: 86.489293217659
time_elpased: 1.853
batch start
#iterations: 385
currently lose_sum: 86.56577152013779
time_elpased: 1.911
batch start
#iterations: 386
currently lose_sum: 87.06212067604065
time_elpased: 1.864
batch start
#iterations: 387
currently lose_sum: 86.5862067937851
time_elpased: 1.877
batch start
#iterations: 388
currently lose_sum: 87.50560742616653
time_elpased: 1.885
batch start
#iterations: 389
currently lose_sum: 86.9503744840622
time_elpased: 1.907
batch start
#iterations: 390
currently lose_sum: 87.19170361757278
time_elpased: 1.86
batch start
#iterations: 391
currently lose_sum: 86.48507714271545
time_elpased: 1.897
batch start
#iterations: 392
currently lose_sum: 86.3571857213974
time_elpased: 1.886
batch start
#iterations: 393
currently lose_sum: 86.81229829788208
time_elpased: 1.881
batch start
#iterations: 394
currently lose_sum: 87.35904359817505
time_elpased: 1.852
batch start
#iterations: 395
currently lose_sum: 87.21573722362518
time_elpased: 1.841
batch start
#iterations: 396
currently lose_sum: 87.25413602590561
time_elpased: 1.875
batch start
#iterations: 397
currently lose_sum: 86.5004306435585
time_elpased: 1.874
batch start
#iterations: 398
currently lose_sum: 86.8100636601448
time_elpased: 1.858
batch start
#iterations: 399
currently lose_sum: 87.46519577503204
time_elpased: 1.873
start validation test
0.604432989691
0.610016155089
0.582895955542
0.596147773919
0.604470801276
65.425
acc: 0.634
pre: 0.621
rec: 0.691
F1: 0.654
auc: 0.634
