start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.41413056850433
time_elpased: 2.049
batch start
#iterations: 1
currently lose_sum: 99.88257569074631
time_elpased: 2.326
batch start
#iterations: 2
currently lose_sum: 99.86066871881485
time_elpased: 2.399
batch start
#iterations: 3
currently lose_sum: 99.62544721364975
time_elpased: 2.302
batch start
#iterations: 4
currently lose_sum: 99.43076199293137
time_elpased: 2.412
batch start
#iterations: 5
currently lose_sum: 99.26118022203445
time_elpased: 2.339
batch start
#iterations: 6
currently lose_sum: 99.1292080283165
time_elpased: 2.217
batch start
#iterations: 7
currently lose_sum: 98.86815571784973
time_elpased: 2.05
batch start
#iterations: 8
currently lose_sum: 98.86108464002609
time_elpased: 2.33
batch start
#iterations: 9
currently lose_sum: 98.8178761601448
time_elpased: 2.127
batch start
#iterations: 10
currently lose_sum: 98.6286952495575
time_elpased: 2.451
batch start
#iterations: 11
currently lose_sum: 98.30077087879181
time_elpased: 2.293
batch start
#iterations: 12
currently lose_sum: 98.08519142866135
time_elpased: 2.503
batch start
#iterations: 13
currently lose_sum: 98.36961102485657
time_elpased: 2.014
batch start
#iterations: 14
currently lose_sum: 98.14699578285217
time_elpased: 2.417
batch start
#iterations: 15
currently lose_sum: 97.95340991020203
time_elpased: 2.301
batch start
#iterations: 16
currently lose_sum: 97.96927565336227
time_elpased: 2.475
batch start
#iterations: 17
currently lose_sum: 97.5126303434372
time_elpased: 2.478
batch start
#iterations: 18
currently lose_sum: 97.23010236024857
time_elpased: 2.432
batch start
#iterations: 19
currently lose_sum: 97.58013194799423
time_elpased: 2.188
start validation test
0.614742268041
0.610743556828
0.636513327159
0.623362225358
0.614704045589
63.552
batch start
#iterations: 20
currently lose_sum: 97.29558950662613
time_elpased: 2.31
batch start
#iterations: 21
currently lose_sum: 97.33131194114685
time_elpased: 2.406
batch start
#iterations: 22
currently lose_sum: 97.11441344022751
time_elpased: 2.366
batch start
#iterations: 23
currently lose_sum: 97.30330002307892
time_elpased: 2.38
batch start
#iterations: 24
currently lose_sum: 97.20913749933243
time_elpased: 2.288
batch start
#iterations: 25
currently lose_sum: 96.67360818386078
time_elpased: 2.366
batch start
#iterations: 26
currently lose_sum: 97.20502632856369
time_elpased: 2.111
batch start
#iterations: 27
currently lose_sum: 96.57712149620056
time_elpased: 2.296
batch start
#iterations: 28
currently lose_sum: 96.65771412849426
time_elpased: 2.14
batch start
#iterations: 29
currently lose_sum: 96.59166938066483
time_elpased: 2.106
batch start
#iterations: 30
currently lose_sum: 96.12233954668045
time_elpased: 2.385
batch start
#iterations: 31
currently lose_sum: 96.25458025932312
time_elpased: 2.369
batch start
#iterations: 32
currently lose_sum: 96.3784613609314
time_elpased: 2.304
batch start
#iterations: 33
currently lose_sum: 96.11531043052673
time_elpased: 2.228
batch start
#iterations: 34
currently lose_sum: 96.13902944326401
time_elpased: 2.364
batch start
#iterations: 35
currently lose_sum: 95.75672435760498
time_elpased: 2.317
batch start
#iterations: 36
currently lose_sum: 96.02322405576706
time_elpased: 2.314
batch start
#iterations: 37
currently lose_sum: 95.86379253864288
time_elpased: 2.215
batch start
#iterations: 38
currently lose_sum: 95.2404156923294
time_elpased: 2.24
batch start
#iterations: 39
currently lose_sum: 95.45468056201935
time_elpased: 2.234
start validation test
0.629432989691
0.620725883477
0.668827827519
0.643879724575
0.629363825977
62.552
batch start
#iterations: 40
currently lose_sum: 95.71756798028946
time_elpased: 2.286
batch start
#iterations: 41
currently lose_sum: 95.57839941978455
time_elpased: 2.341
batch start
#iterations: 42
currently lose_sum: 95.45118486881256
time_elpased: 2.315
batch start
#iterations: 43
currently lose_sum: 95.57741069793701
time_elpased: 2.283
batch start
#iterations: 44
currently lose_sum: 95.35537558794022
time_elpased: 2.053
batch start
#iterations: 45
currently lose_sum: 95.79351770877838
time_elpased: 2.338
batch start
#iterations: 46
currently lose_sum: 95.04578995704651
time_elpased: 2.138
batch start
#iterations: 47
currently lose_sum: 95.79119944572449
time_elpased: 2.317
batch start
#iterations: 48
currently lose_sum: 95.19874429702759
time_elpased: 2.358
batch start
#iterations: 49
currently lose_sum: 95.19194376468658
time_elpased: 2.205
batch start
#iterations: 50
currently lose_sum: 95.25745761394501
time_elpased: 2.007
batch start
#iterations: 51
currently lose_sum: 94.93673014640808
time_elpased: 2.302
batch start
#iterations: 52
currently lose_sum: 94.71007204055786
time_elpased: 2.357
batch start
#iterations: 53
currently lose_sum: 94.64810782670975
time_elpased: 2.382
batch start
#iterations: 54
currently lose_sum: 94.41994214057922
time_elpased: 2.162
batch start
#iterations: 55
currently lose_sum: 94.45412623882294
time_elpased: 2.406
batch start
#iterations: 56
currently lose_sum: 94.89359492063522
time_elpased: 2.207
batch start
#iterations: 57
currently lose_sum: 94.45098865032196
time_elpased: 2.257
batch start
#iterations: 58
currently lose_sum: 94.8478331565857
time_elpased: 1.969
batch start
#iterations: 59
currently lose_sum: 94.62862598896027
time_elpased: 1.651
start validation test
0.627113402062
0.638699586638
0.588350313883
0.61249196486
0.627181456642
62.665
batch start
#iterations: 60
currently lose_sum: 94.54568415880203
time_elpased: 2.38
batch start
#iterations: 61
currently lose_sum: 94.81543040275574
time_elpased: 1.956
batch start
#iterations: 62
currently lose_sum: 94.79291844367981
time_elpased: 1.925
batch start
#iterations: 63
currently lose_sum: 94.60498058795929
time_elpased: 2.346
batch start
#iterations: 64
currently lose_sum: 94.71434777975082
time_elpased: 2.46
batch start
#iterations: 65
currently lose_sum: 94.43026971817017
time_elpased: 2.224
batch start
#iterations: 66
currently lose_sum: 94.216712474823
time_elpased: 2.383
batch start
#iterations: 67
currently lose_sum: 93.76635479927063
time_elpased: 2.491
batch start
#iterations: 68
currently lose_sum: 93.83053857088089
time_elpased: 1.979
batch start
#iterations: 69
currently lose_sum: 94.3286446928978
time_elpased: 2.197
batch start
#iterations: 70
currently lose_sum: 94.43694990873337
time_elpased: 2.437
batch start
#iterations: 71
currently lose_sum: 94.02047485113144
time_elpased: 2.435
batch start
#iterations: 72
currently lose_sum: 93.86374026536942
time_elpased: 2.358
batch start
#iterations: 73
currently lose_sum: 93.71995800733566
time_elpased: 2.337
batch start
#iterations: 74
currently lose_sum: 93.81993329524994
time_elpased: 2.395
batch start
#iterations: 75
currently lose_sum: 93.9899822473526
time_elpased: 2.06
batch start
#iterations: 76
currently lose_sum: 93.85327810049057
time_elpased: 1.994
batch start
#iterations: 77
currently lose_sum: 93.87695074081421
time_elpased: 2.297
batch start
#iterations: 78
currently lose_sum: 93.80918079614639
time_elpased: 2.284
batch start
#iterations: 79
currently lose_sum: 93.7534863948822
time_elpased: 2.392
start validation test
0.627422680412
0.625948790608
0.636513327159
0.631186855802
0.62740672038
62.159
batch start
#iterations: 80
currently lose_sum: 93.13869363069534
time_elpased: 2.34
batch start
#iterations: 81
currently lose_sum: 93.8178122639656
time_elpased: 2.349
batch start
#iterations: 82
currently lose_sum: 93.79409694671631
time_elpased: 2.074
batch start
#iterations: 83
currently lose_sum: 93.36186689138412
time_elpased: 2.328
batch start
#iterations: 84
currently lose_sum: 93.55190527439117
time_elpased: 2.172
batch start
#iterations: 85
currently lose_sum: 93.76307338476181
time_elpased: 2.12
batch start
#iterations: 86
currently lose_sum: 93.2357742190361
time_elpased: 2.059
batch start
#iterations: 87
currently lose_sum: 93.10822314023972
time_elpased: 2.31
batch start
#iterations: 88
currently lose_sum: 93.46491414308548
time_elpased: 1.87
batch start
#iterations: 89
currently lose_sum: 93.59813618659973
time_elpased: 2.343
batch start
#iterations: 90
currently lose_sum: 93.3606516122818
time_elpased: 2.346
batch start
#iterations: 91
currently lose_sum: 93.41125577688217
time_elpased: 2.361
batch start
#iterations: 92
currently lose_sum: 92.67615616321564
time_elpased: 2.229
batch start
#iterations: 93
currently lose_sum: 93.07262182235718
time_elpased: 2.327
batch start
#iterations: 94
currently lose_sum: 93.47014546394348
time_elpased: 2.268
batch start
#iterations: 95
currently lose_sum: 93.53795576095581
time_elpased: 2.289
batch start
#iterations: 96
currently lose_sum: 93.20893800258636
time_elpased: 2.365
batch start
#iterations: 97
currently lose_sum: 93.28003233671188
time_elpased: 2.132
batch start
#iterations: 98
currently lose_sum: 92.80481392145157
time_elpased: 2.403
batch start
#iterations: 99
currently lose_sum: 93.061090528965
time_elpased: 2.202
start validation test
0.634226804124
0.613946613338
0.726664608418
0.665566971439
0.634064515301
61.730
batch start
#iterations: 100
currently lose_sum: 93.24552869796753
time_elpased: 2.23
batch start
#iterations: 101
currently lose_sum: 93.13732582330704
time_elpased: 2.111
batch start
#iterations: 102
currently lose_sum: 93.08608531951904
time_elpased: 2.317
batch start
#iterations: 103
currently lose_sum: 92.81444031000137
time_elpased: 2.498
batch start
#iterations: 104
currently lose_sum: 92.34624797105789
time_elpased: 2.477
batch start
#iterations: 105
currently lose_sum: 92.72783440351486
time_elpased: 2.519
batch start
#iterations: 106
currently lose_sum: 92.77127760648727
time_elpased: 2.113
batch start
#iterations: 107
currently lose_sum: 92.5935634970665
time_elpased: 2.427
batch start
#iterations: 108
currently lose_sum: 92.78508830070496
time_elpased: 2.237
batch start
#iterations: 109
currently lose_sum: 92.48647618293762
time_elpased: 1.856
batch start
#iterations: 110
currently lose_sum: 92.63473635911942
time_elpased: 1.496
batch start
#iterations: 111
currently lose_sum: 93.2765844464302
time_elpased: 2.155
batch start
#iterations: 112
currently lose_sum: 92.39438098669052
time_elpased: 2.284
batch start
#iterations: 113
currently lose_sum: 91.9884741306305
time_elpased: 2.307
batch start
#iterations: 114
currently lose_sum: 92.15106528997421
time_elpased: 2.323
batch start
#iterations: 115
currently lose_sum: 92.55250531435013
time_elpased: 1.732
batch start
#iterations: 116
currently lose_sum: 92.38251268863678
time_elpased: 1.471
batch start
#iterations: 117
currently lose_sum: 92.06178748607635
time_elpased: 2.002
batch start
#iterations: 118
currently lose_sum: 92.353311419487
time_elpased: 2.231
batch start
#iterations: 119
currently lose_sum: 92.02068173885345
time_elpased: 1.562
start validation test
0.610103092784
0.616039667996
0.58814448904
0.601768979678
0.610141644499
62.709
batch start
#iterations: 120
currently lose_sum: 91.82476663589478
time_elpased: 2.295
batch start
#iterations: 121
currently lose_sum: 92.35951834917068
time_elpased: 2.374
batch start
#iterations: 122
currently lose_sum: 92.31213593482971
time_elpased: 2.252
batch start
#iterations: 123
currently lose_sum: 91.5833729505539
time_elpased: 2.325
batch start
#iterations: 124
currently lose_sum: 91.96994912624359
time_elpased: 2.354
batch start
#iterations: 125
currently lose_sum: 91.94033473730087
time_elpased: 2.233
batch start
#iterations: 126
currently lose_sum: 92.15898215770721
time_elpased: 2.402
batch start
#iterations: 127
currently lose_sum: 92.20025038719177
time_elpased: 2.346
batch start
#iterations: 128
currently lose_sum: 91.77121371030807
time_elpased: 2.157
batch start
#iterations: 129
currently lose_sum: 91.59759151935577
time_elpased: 2.051
batch start
#iterations: 130
currently lose_sum: 92.33844035863876
time_elpased: 2.342
batch start
#iterations: 131
currently lose_sum: 91.7922676205635
time_elpased: 2.254
batch start
#iterations: 132
currently lose_sum: 91.98270100355148
time_elpased: 1.977
batch start
#iterations: 133
currently lose_sum: 91.8967849612236
time_elpased: 2.325
batch start
#iterations: 134
currently lose_sum: 91.27526473999023
time_elpased: 2.066
batch start
#iterations: 135
currently lose_sum: 91.6037700176239
time_elpased: 2.135
batch start
#iterations: 136
currently lose_sum: 91.91366785764694
time_elpased: 2.172
batch start
#iterations: 137
currently lose_sum: 91.73744744062424
time_elpased: 1.841
batch start
#iterations: 138
currently lose_sum: 91.99939376115799
time_elpased: 2.238
batch start
#iterations: 139
currently lose_sum: 91.70674252510071
time_elpased: 2.122
start validation test
0.60706185567
0.63974906567
0.49325923639
0.557034110059
0.607261653721
63.989
batch start
#iterations: 140
currently lose_sum: 91.55967724323273
time_elpased: 1.99
batch start
#iterations: 141
currently lose_sum: 91.58320391178131
time_elpased: 2.322
batch start
#iterations: 142
currently lose_sum: 92.00457215309143
time_elpased: 2.313
batch start
#iterations: 143
currently lose_sum: 91.00140887498856
time_elpased: 2.388
batch start
#iterations: 144
currently lose_sum: 91.84070479869843
time_elpased: 2.374
batch start
#iterations: 145
currently lose_sum: 91.36592900753021
time_elpased: 2.315
batch start
#iterations: 146
currently lose_sum: 91.09060472249985
time_elpased: 1.967
batch start
#iterations: 147
currently lose_sum: 91.18202269077301
time_elpased: 2.216
batch start
#iterations: 148
currently lose_sum: 91.47127193212509
time_elpased: 2.157
batch start
#iterations: 149
currently lose_sum: 91.88721883296967
time_elpased: 2.319
batch start
#iterations: 150
currently lose_sum: 91.09411561489105
time_elpased: 2.36
batch start
#iterations: 151
currently lose_sum: 91.27770376205444
time_elpased: 1.852
batch start
#iterations: 152
currently lose_sum: 90.70055198669434
time_elpased: 2.065
batch start
#iterations: 153
currently lose_sum: 91.0918418765068
time_elpased: 2.316
batch start
#iterations: 154
currently lose_sum: 91.2591844201088
time_elpased: 2.31
batch start
#iterations: 155
currently lose_sum: 91.58363515138626
time_elpased: 2.192
batch start
#iterations: 156
currently lose_sum: 91.4907653927803
time_elpased: 2.262
batch start
#iterations: 157
currently lose_sum: 91.10003864765167
time_elpased: 2.203
batch start
#iterations: 158
currently lose_sum: 91.47928524017334
time_elpased: 2.208
batch start
#iterations: 159
currently lose_sum: 91.97354048490524
time_elpased: 2.259
start validation test
0.628711340206
0.63423750534
0.611196871462
0.622504061632
0.628742089557
62.419
batch start
#iterations: 160
currently lose_sum: 91.36179852485657
time_elpased: 2.824
batch start
#iterations: 161
currently lose_sum: 91.21266043186188
time_elpased: 2.225
batch start
#iterations: 162
currently lose_sum: 90.60860598087311
time_elpased: 2.488
batch start
#iterations: 163
currently lose_sum: 90.6240941286087
time_elpased: 2.477
batch start
#iterations: 164
currently lose_sum: 91.1503072977066
time_elpased: 2.521
batch start
#iterations: 165
currently lose_sum: 90.78127479553223
time_elpased: 2.447
batch start
#iterations: 166
currently lose_sum: 90.70977300405502
time_elpased: 2.464
batch start
#iterations: 167
currently lose_sum: 91.01155179738998
time_elpased: 2.271
batch start
#iterations: 168
currently lose_sum: 91.03380727767944
time_elpased: 2.329
batch start
#iterations: 169
currently lose_sum: 90.55754351615906
time_elpased: 1.962
batch start
#iterations: 170
currently lose_sum: 90.77689182758331
time_elpased: 2.017
batch start
#iterations: 171
currently lose_sum: 90.94542449712753
time_elpased: 1.76
batch start
#iterations: 172
currently lose_sum: 90.82349681854248
time_elpased: 2.173
batch start
#iterations: 173
currently lose_sum: 90.83893483877182
time_elpased: 2.206
batch start
#iterations: 174
currently lose_sum: 90.49099946022034
time_elpased: 2.235
batch start
#iterations: 175
currently lose_sum: 91.14029383659363
time_elpased: 2.21
batch start
#iterations: 176
currently lose_sum: 91.25629216432571
time_elpased: 1.986
batch start
#iterations: 177
currently lose_sum: 90.89217931032181
time_elpased: 2.21
batch start
#iterations: 178
currently lose_sum: 90.11297106742859
time_elpased: 2.313
batch start
#iterations: 179
currently lose_sum: 90.49181377887726
time_elpased: 2.4
start validation test
0.612113402062
0.624460595049
0.56591540599
0.593748312908
0.61219450977
63.121
batch start
#iterations: 180
currently lose_sum: 90.61248701810837
time_elpased: 2.473
batch start
#iterations: 181
currently lose_sum: 90.24235594272614
time_elpased: 2.149
batch start
#iterations: 182
currently lose_sum: 90.36743676662445
time_elpased: 2.313
batch start
#iterations: 183
currently lose_sum: 91.05229443311691
time_elpased: 2.306
batch start
#iterations: 184
currently lose_sum: 90.4633372426033
time_elpased: 2.396
batch start
#iterations: 185
currently lose_sum: 90.24908602237701
time_elpased: 2.178
batch start
#iterations: 186
currently lose_sum: 90.53555870056152
time_elpased: 2.072
batch start
#iterations: 187
currently lose_sum: 90.5207547545433
time_elpased: 2.291
batch start
#iterations: 188
currently lose_sum: 90.02327758073807
time_elpased: 2.091
batch start
#iterations: 189
currently lose_sum: 90.80206912755966
time_elpased: 2.36
batch start
#iterations: 190
currently lose_sum: 90.1511858701706
time_elpased: 2.242
batch start
#iterations: 191
currently lose_sum: 90.73411154747009
time_elpased: 2.006
batch start
#iterations: 192
currently lose_sum: 89.9064981341362
time_elpased: 2.285
batch start
#iterations: 193
currently lose_sum: 90.20931625366211
time_elpased: 2.283
batch start
#iterations: 194
currently lose_sum: 90.76504099369049
time_elpased: 2.186
batch start
#iterations: 195
currently lose_sum: 90.61018031835556
time_elpased: 2.006
batch start
#iterations: 196
currently lose_sum: 90.32964074611664
time_elpased: 2.301
batch start
#iterations: 197
currently lose_sum: 90.66262239217758
time_elpased: 2.261
batch start
#iterations: 198
currently lose_sum: 89.92359519004822
time_elpased: 2.211
batch start
#iterations: 199
currently lose_sum: 89.78661131858826
time_elpased: 2.368
start validation test
0.60912371134
0.607974094313
0.618297828548
0.61309250472
0.609107604762
63.127
batch start
#iterations: 200
currently lose_sum: 90.20393335819244
time_elpased: 2.312
batch start
#iterations: 201
currently lose_sum: 90.56797176599503
time_elpased: 2.286
batch start
#iterations: 202
currently lose_sum: 90.25047540664673
time_elpased: 2.268
batch start
#iterations: 203
currently lose_sum: 89.97190320491791
time_elpased: 2.352
batch start
#iterations: 204
currently lose_sum: 90.23582804203033
time_elpased: 2.196
batch start
#iterations: 205
currently lose_sum: 89.92743790149689
time_elpased: 2.373
batch start
#iterations: 206
currently lose_sum: 89.88491833209991
time_elpased: 2.214
batch start
#iterations: 207
currently lose_sum: 90.02244454622269
time_elpased: 2.355
batch start
#iterations: 208
currently lose_sum: 89.46394681930542
time_elpased: 2.43
batch start
#iterations: 209
currently lose_sum: 89.86014574766159
time_elpased: 2.398
batch start
#iterations: 210
currently lose_sum: 89.57864302396774
time_elpased: 2.157
batch start
#iterations: 211
currently lose_sum: 90.20887392759323
time_elpased: 2.32
batch start
#iterations: 212
currently lose_sum: 89.88718169927597
time_elpased: 2.076
batch start
#iterations: 213
currently lose_sum: 89.74131220579147
time_elpased: 1.934
batch start
#iterations: 214
currently lose_sum: 90.08751863241196
time_elpased: 2.524
batch start
#iterations: 215
currently lose_sum: 89.89554089307785
time_elpased: 2.249
batch start
#iterations: 216
currently lose_sum: 89.43021804094315
time_elpased: 2.331
batch start
#iterations: 217
currently lose_sum: 90.02833938598633
time_elpased: 2.02
batch start
#iterations: 218
currently lose_sum: 89.23143297433853
time_elpased: 2.407
batch start
#iterations: 219
currently lose_sum: 89.4825050830841
time_elpased: 2.356
start validation test
0.617731958763
0.626972740316
0.584645466708
0.605069762488
0.617790047203
63.128
batch start
#iterations: 220
currently lose_sum: 89.59242022037506
time_elpased: 2.105
batch start
#iterations: 221
currently lose_sum: 89.98557639122009
time_elpased: 2.29
batch start
#iterations: 222
currently lose_sum: 89.19949728250504
time_elpased: 2.203
batch start
#iterations: 223
currently lose_sum: 89.44469177722931
time_elpased: 2.215
batch start
#iterations: 224
currently lose_sum: 89.28582376241684
time_elpased: 2.104
batch start
#iterations: 225
currently lose_sum: 89.72608745098114
time_elpased: 2.34
batch start
#iterations: 226
currently lose_sum: 89.39727431535721
time_elpased: 2.018
batch start
#iterations: 227
currently lose_sum: 89.3117625117302
time_elpased: 2.286
batch start
#iterations: 228
currently lose_sum: 89.54452109336853
time_elpased: 2.312
batch start
#iterations: 229
currently lose_sum: 89.3507297039032
time_elpased: 2.265
batch start
#iterations: 230
currently lose_sum: 89.79072088003159
time_elpased: 2.101
batch start
#iterations: 231
currently lose_sum: 89.11096340417862
time_elpased: 2.152
batch start
#iterations: 232
currently lose_sum: 89.43897360563278
time_elpased: 2.45
batch start
#iterations: 233
currently lose_sum: 89.16011929512024
time_elpased: 2.176
batch start
#iterations: 234
currently lose_sum: 89.06895273923874
time_elpased: 2.282
batch start
#iterations: 235
currently lose_sum: 89.81849032640457
time_elpased: 2.272
batch start
#iterations: 236
currently lose_sum: 89.55759286880493
time_elpased: 2.375
batch start
#iterations: 237
currently lose_sum: 89.2322586774826
time_elpased: 2.357
batch start
#iterations: 238
currently lose_sum: 89.37667661905289
time_elpased: 2.299
batch start
#iterations: 239
currently lose_sum: 89.05694967508316
time_elpased: 2.236
start validation test
0.61881443299
0.615224295355
0.63795410106
0.62638306472
0.61878083035
63.124
batch start
#iterations: 240
currently lose_sum: 89.40597051382065
time_elpased: 2.303
batch start
#iterations: 241
currently lose_sum: 89.4911738038063
time_elpased: 1.914
batch start
#iterations: 242
currently lose_sum: 89.41937762498856
time_elpased: 1.897
batch start
#iterations: 243
currently lose_sum: 89.46642684936523
time_elpased: 2.08
batch start
#iterations: 244
currently lose_sum: 89.69672185182571
time_elpased: 2.393
batch start
#iterations: 245
currently lose_sum: 89.57539516687393
time_elpased: 2.036
batch start
#iterations: 246
currently lose_sum: 89.42935037612915
time_elpased: 2.233
batch start
#iterations: 247
currently lose_sum: 88.81822592020035
time_elpased: 2.35
batch start
#iterations: 248
currently lose_sum: 89.36373943090439
time_elpased: 2.157
batch start
#iterations: 249
currently lose_sum: 89.23885411024094
time_elpased: 2.425
batch start
#iterations: 250
currently lose_sum: 89.11666643619537
time_elpased: 2.228
batch start
#iterations: 251
currently lose_sum: 88.9167308807373
time_elpased: 2.166
batch start
#iterations: 252
currently lose_sum: 88.66642189025879
time_elpased: 2.214
batch start
#iterations: 253
currently lose_sum: 89.48985236883163
time_elpased: 2.423
batch start
#iterations: 254
currently lose_sum: 89.23369348049164
time_elpased: 2.334
batch start
#iterations: 255
currently lose_sum: 89.36875158548355
time_elpased: 2.087
batch start
#iterations: 256
currently lose_sum: 88.86604315042496
time_elpased: 1.829
batch start
#iterations: 257
currently lose_sum: 89.09328746795654
time_elpased: 2.318
batch start
#iterations: 258
currently lose_sum: 88.89932835102081
time_elpased: 2.304
batch start
#iterations: 259
currently lose_sum: 89.09544312953949
time_elpased: 2.087
start validation test
0.601649484536
0.626285714286
0.507564062982
0.56070941337
0.601814666005
64.456
batch start
#iterations: 260
currently lose_sum: 88.79193633794785
time_elpased: 2.356
batch start
#iterations: 261
currently lose_sum: 88.93114882707596
time_elpased: 2.486
batch start
#iterations: 262
currently lose_sum: 88.90033161640167
time_elpased: 2.181
batch start
#iterations: 263
currently lose_sum: 88.67970377206802
time_elpased: 2.471
batch start
#iterations: 264
currently lose_sum: 89.03867894411087
time_elpased: 1.935
batch start
#iterations: 265
currently lose_sum: 88.99859976768494
time_elpased: 1.844
batch start
#iterations: 266
currently lose_sum: 88.42900425195694
time_elpased: 1.814
batch start
#iterations: 267
currently lose_sum: 88.97314321994781
time_elpased: 1.747
batch start
#iterations: 268
currently lose_sum: 88.95475560426712
time_elpased: 2.389
batch start
#iterations: 269
currently lose_sum: 89.48121911287308
time_elpased: 2.076
batch start
#iterations: 270
currently lose_sum: 88.5587927699089
time_elpased: 2.451
batch start
#iterations: 271
currently lose_sum: 88.51544463634491
time_elpased: 2.243
batch start
#iterations: 272
currently lose_sum: 88.99535745382309
time_elpased: 2.201
batch start
#iterations: 273
currently lose_sum: 88.35260397195816
time_elpased: 2.336
batch start
#iterations: 274
currently lose_sum: 89.19848668575287
time_elpased: 2.134
batch start
#iterations: 275
currently lose_sum: 89.05605936050415
time_elpased: 2.189
batch start
#iterations: 276
currently lose_sum: 88.89878177642822
time_elpased: 2.173
batch start
#iterations: 277
currently lose_sum: 89.04653805494308
time_elpased: 1.852
batch start
#iterations: 278
currently lose_sum: 88.4559434056282
time_elpased: 2.42
batch start
#iterations: 279
currently lose_sum: 88.39193016290665
time_elpased: 2.485
start validation test
0.620360824742
0.610985277463
0.666255016981
0.637424309555
0.620280250409
63.593
batch start
#iterations: 280
currently lose_sum: 88.69051969051361
time_elpased: 2.268
batch start
#iterations: 281
currently lose_sum: 88.81313109397888
time_elpased: 2.255
batch start
#iterations: 282
currently lose_sum: 87.97284466028214
time_elpased: 2.365
batch start
#iterations: 283
currently lose_sum: 88.15886950492859
time_elpased: 2.304
batch start
#iterations: 284
currently lose_sum: 88.35646766424179
time_elpased: 2.339
batch start
#iterations: 285
currently lose_sum: 88.54267102479935
time_elpased: 2.253
batch start
#iterations: 286
currently lose_sum: 88.7856639623642
time_elpased: 2.267
batch start
#iterations: 287
currently lose_sum: 88.45843732357025
time_elpased: 1.941
batch start
#iterations: 288
currently lose_sum: 88.3953086733818
time_elpased: 2.346
batch start
#iterations: 289
currently lose_sum: 88.00737047195435
time_elpased: 2.311
batch start
#iterations: 290
currently lose_sum: 88.76603418588638
time_elpased: 2.368
batch start
#iterations: 291
currently lose_sum: 87.85266876220703
time_elpased: 2.223
batch start
#iterations: 292
currently lose_sum: 88.62623310089111
time_elpased: 2.163
batch start
#iterations: 293
currently lose_sum: 88.01106023788452
time_elpased: 2.246
batch start
#iterations: 294
currently lose_sum: 88.09265196323395
time_elpased: 2.366
batch start
#iterations: 295
currently lose_sum: 88.17914640903473
time_elpased: 2.24
batch start
#iterations: 296
currently lose_sum: 87.83031660318375
time_elpased: 2.349
batch start
#iterations: 297
currently lose_sum: 88.78464758396149
time_elpased: 2.284
batch start
#iterations: 298
currently lose_sum: 87.80398231744766
time_elpased: 1.778
batch start
#iterations: 299
currently lose_sum: 87.72260963916779
time_elpased: 2.098
start validation test
0.616134020619
0.611318163986
0.641453123392
0.626023200924
0.616089569028
63.996
batch start
#iterations: 300
currently lose_sum: 87.83372890949249
time_elpased: 1.821
batch start
#iterations: 301
currently lose_sum: 87.65076452493668
time_elpased: 2.228
batch start
#iterations: 302
currently lose_sum: 88.11863249540329
time_elpased: 1.922
batch start
#iterations: 303
currently lose_sum: 88.06422364711761
time_elpased: 2.049
batch start
#iterations: 304
currently lose_sum: 88.29810798168182
time_elpased: 2.157
batch start
#iterations: 305
currently lose_sum: 88.0824607014656
time_elpased: 2.195
batch start
#iterations: 306
currently lose_sum: 87.69402706623077
time_elpased: 2.179
batch start
#iterations: 307
currently lose_sum: 87.48204308748245
time_elpased: 2.109
batch start
#iterations: 308
currently lose_sum: 88.2924222946167
time_elpased: 2.305
batch start
#iterations: 309
currently lose_sum: 87.70417654514313
time_elpased: 2.235
batch start
#iterations: 310
currently lose_sum: 87.89888268709183
time_elpased: 2.05
batch start
#iterations: 311
currently lose_sum: 87.6420082449913
time_elpased: 2.506
batch start
#iterations: 312
currently lose_sum: 87.60241883993149
time_elpased: 2.511
batch start
#iterations: 313
currently lose_sum: 88.36525177955627
time_elpased: 2.383
batch start
#iterations: 314
currently lose_sum: 88.5907136797905
time_elpased: 2.237
batch start
#iterations: 315
currently lose_sum: 87.53197109699249
time_elpased: 2.317
batch start
#iterations: 316
currently lose_sum: 87.58679646253586
time_elpased: 2.319
batch start
#iterations: 317
currently lose_sum: 87.51837331056595
time_elpased: 1.994
batch start
#iterations: 318
currently lose_sum: 88.22269570827484
time_elpased: 2.148
batch start
#iterations: 319
currently lose_sum: 87.35872268676758
time_elpased: 2.034
start validation test
0.600360824742
0.624429802331
0.507152413296
0.559713782725
0.600524466485
65.464
batch start
#iterations: 320
currently lose_sum: 87.72827965021133
time_elpased: 2.154
batch start
#iterations: 321
currently lose_sum: 88.39087289571762
time_elpased: 2.093
batch start
#iterations: 322
currently lose_sum: 88.09495079517365
time_elpased: 2.137
batch start
#iterations: 323
currently lose_sum: 88.16195267438889
time_elpased: 2.105
batch start
#iterations: 324
currently lose_sum: 87.20686787366867
time_elpased: 2.278
batch start
#iterations: 325
currently lose_sum: 88.44652831554413
time_elpased: 2.03
batch start
#iterations: 326
currently lose_sum: 87.64397424459457
time_elpased: 2.349
batch start
#iterations: 327
currently lose_sum: 88.02363622188568
time_elpased: 2.425
batch start
#iterations: 328
currently lose_sum: 87.7169759273529
time_elpased: 2.195
batch start
#iterations: 329
currently lose_sum: 87.76833921670914
time_elpased: 2.214
batch start
#iterations: 330
currently lose_sum: 87.57579177618027
time_elpased: 2.206
batch start
#iterations: 331
currently lose_sum: 87.72178167104721
time_elpased: 2.069
batch start
#iterations: 332
currently lose_sum: 87.56731462478638
time_elpased: 1.926
batch start
#iterations: 333
currently lose_sum: 87.34986579418182
time_elpased: 2.108
batch start
#iterations: 334
currently lose_sum: 87.31506812572479
time_elpased: 2.27
batch start
#iterations: 335
currently lose_sum: 87.41189044713974
time_elpased: 2.358
batch start
#iterations: 336
currently lose_sum: 87.66773623228073
time_elpased: 2.293
batch start
#iterations: 337
currently lose_sum: 87.37046998739243
time_elpased: 2.393
batch start
#iterations: 338
currently lose_sum: 87.53372710943222
time_elpased: 2.224
batch start
#iterations: 339
currently lose_sum: 87.66543126106262
time_elpased: 2.3
start validation test
0.606649484536
0.617349234923
0.564680456931
0.589841440473
0.606723167637
64.521
batch start
#iterations: 340
currently lose_sum: 87.3181682229042
time_elpased: 1.993
batch start
#iterations: 341
currently lose_sum: 88.01925492286682
time_elpased: 2.198
batch start
#iterations: 342
currently lose_sum: 87.70364892482758
time_elpased: 2.199
batch start
#iterations: 343
currently lose_sum: 87.16572695970535
time_elpased: 1.776
batch start
#iterations: 344
currently lose_sum: 87.4126056432724
time_elpased: 2.052
batch start
#iterations: 345
currently lose_sum: 87.4812325835228
time_elpased: 2.358
batch start
#iterations: 346
currently lose_sum: 87.13450890779495
time_elpased: 2.336
batch start
#iterations: 347
currently lose_sum: 87.01000112295151
time_elpased: 2.177
batch start
#iterations: 348
currently lose_sum: 87.51886034011841
time_elpased: 2.159
batch start
#iterations: 349
currently lose_sum: 87.3914629817009
time_elpased: 2.019
batch start
#iterations: 350
currently lose_sum: 87.63845324516296
time_elpased: 2.003
batch start
#iterations: 351
currently lose_sum: 87.71611511707306
time_elpased: 2.373
batch start
#iterations: 352
currently lose_sum: 87.16645753383636
time_elpased: 1.95
batch start
#iterations: 353
currently lose_sum: 87.3941975235939
time_elpased: 2.266
batch start
#iterations: 354
currently lose_sum: 87.01104599237442
time_elpased: 2.025
batch start
#iterations: 355
currently lose_sum: 87.51580584049225
time_elpased: 2.385
batch start
#iterations: 356
currently lose_sum: 87.3244469165802
time_elpased: 2.311
batch start
#iterations: 357
currently lose_sum: 87.42786890268326
time_elpased: 1.984
batch start
#iterations: 358
currently lose_sum: 86.97791463136673
time_elpased: 2.443
batch start
#iterations: 359
currently lose_sum: 87.27398258447647
time_elpased: 2.492
start validation test
0.612680412371
0.627858386535
0.556653288052
0.590115644774
0.612778776629
65.429
batch start
#iterations: 360
currently lose_sum: 87.51829665899277
time_elpased: 2.239
batch start
#iterations: 361
currently lose_sum: 87.15105575323105
time_elpased: 2.103
batch start
#iterations: 362
currently lose_sum: 87.12860238552094
time_elpased: 2.277
batch start
#iterations: 363
currently lose_sum: 87.13710832595825
time_elpased: 1.89
batch start
#iterations: 364
currently lose_sum: 86.91846430301666
time_elpased: 2.262
batch start
#iterations: 365
currently lose_sum: 86.94310796260834
time_elpased: 2.267
batch start
#iterations: 366
currently lose_sum: 87.09269052743912
time_elpased: 2.35
batch start
#iterations: 367
currently lose_sum: 87.32782244682312
time_elpased: 2.316
batch start
#iterations: 368
currently lose_sum: 86.46282613277435
time_elpased: 2.236
batch start
#iterations: 369
currently lose_sum: 87.49128037691116
time_elpased: 2.214
batch start
#iterations: 370
currently lose_sum: 87.454354763031
time_elpased: 2.035
batch start
#iterations: 371
currently lose_sum: 86.4615620970726
time_elpased: 2.325
batch start
#iterations: 372
currently lose_sum: 87.316297352314
time_elpased: 1.838
batch start
#iterations: 373
currently lose_sum: 86.73329001665115
time_elpased: 2.358
batch start
#iterations: 374
currently lose_sum: 86.89632940292358
time_elpased: 2.241
batch start
#iterations: 375
currently lose_sum: 86.89533841609955
time_elpased: 2.135
batch start
#iterations: 376
currently lose_sum: 87.19945693016052
time_elpased: 2.322
batch start
#iterations: 377
currently lose_sum: 86.87135028839111
time_elpased: 2.196
batch start
#iterations: 378
currently lose_sum: 86.80868136882782
time_elpased: 2.329
batch start
#iterations: 379
currently lose_sum: 86.85096925497055
time_elpased: 2.379
start validation test
0.595309278351
0.630198157968
0.464752495626
0.534976011372
0.59553849092
68.441
batch start
#iterations: 380
currently lose_sum: 87.1580725312233
time_elpased: 1.947
batch start
#iterations: 381
currently lose_sum: 87.01536357402802
time_elpased: 1.781
batch start
#iterations: 382
currently lose_sum: 86.81017482280731
time_elpased: 2.164
batch start
#iterations: 383
currently lose_sum: 86.16430413722992
time_elpased: 2.316
batch start
#iterations: 384
currently lose_sum: 86.5083377957344
time_elpased: 2.307
batch start
#iterations: 385
currently lose_sum: 86.81687301397324
time_elpased: 2.29
batch start
#iterations: 386
currently lose_sum: 86.72792637348175
time_elpased: 2.275
batch start
#iterations: 387
currently lose_sum: 86.58271408081055
time_elpased: 2.144
batch start
#iterations: 388
currently lose_sum: 87.06501013040543
time_elpased: 2.336
batch start
#iterations: 389
currently lose_sum: 86.99053859710693
time_elpased: 2.34
batch start
#iterations: 390
currently lose_sum: 86.5216366648674
time_elpased: 2.138
batch start
#iterations: 391
currently lose_sum: 86.60921400785446
time_elpased: 2.283
batch start
#iterations: 392
currently lose_sum: 87.34807276725769
time_elpased: 2.197
batch start
#iterations: 393
currently lose_sum: 86.4256221652031
time_elpased: 2.064
batch start
#iterations: 394
currently lose_sum: 86.53016322851181
time_elpased: 2.121
batch start
#iterations: 395
currently lose_sum: 86.75964391231537
time_elpased: 2.317
batch start
#iterations: 396
currently lose_sum: 86.46387112140656
time_elpased: 1.864
batch start
#iterations: 397
currently lose_sum: 86.82258933782578
time_elpased: 1.933
batch start
#iterations: 398
currently lose_sum: 86.58276379108429
time_elpased: 2.326
batch start
#iterations: 399
currently lose_sum: 86.75056004524231
time_elpased: 2.284
start validation test
0.605824742268
0.618910845588
0.554389214778
0.584875956788
0.60591504527
66.453
acc: 0.634
pre: 0.613
rec: 0.730
F1: 0.667
auc: 0.634
