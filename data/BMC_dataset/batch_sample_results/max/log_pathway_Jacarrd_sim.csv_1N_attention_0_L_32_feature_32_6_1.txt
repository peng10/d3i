start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.39897161722183
time_elpased: 2.128
batch start
#iterations: 1
currently lose_sum: 100.15721690654755
time_elpased: 2.032
batch start
#iterations: 2
currently lose_sum: 99.95037907361984
time_elpased: 1.996
batch start
#iterations: 3
currently lose_sum: 99.8206804394722
time_elpased: 2.016
batch start
#iterations: 4
currently lose_sum: 99.58361959457397
time_elpased: 1.99
batch start
#iterations: 5
currently lose_sum: 99.27539908885956
time_elpased: 2.008
batch start
#iterations: 6
currently lose_sum: 99.16796857118607
time_elpased: 2.048
batch start
#iterations: 7
currently lose_sum: 98.78226119279861
time_elpased: 2.007
batch start
#iterations: 8
currently lose_sum: 98.80771511793137
time_elpased: 2.024
batch start
#iterations: 9
currently lose_sum: 98.5713124871254
time_elpased: 2.014
batch start
#iterations: 10
currently lose_sum: 98.49231517314911
time_elpased: 1.976
batch start
#iterations: 11
currently lose_sum: 98.26085263490677
time_elpased: 2.016
batch start
#iterations: 12
currently lose_sum: 98.0926747918129
time_elpased: 1.986
batch start
#iterations: 13
currently lose_sum: 98.22672373056412
time_elpased: 2.014
batch start
#iterations: 14
currently lose_sum: 98.04146659374237
time_elpased: 2.013
batch start
#iterations: 15
currently lose_sum: 97.63949859142303
time_elpased: 2.019
batch start
#iterations: 16
currently lose_sum: 97.28004395961761
time_elpased: 2.005
batch start
#iterations: 17
currently lose_sum: 97.62232089042664
time_elpased: 2.046
batch start
#iterations: 18
currently lose_sum: 97.32563799619675
time_elpased: 2.089
batch start
#iterations: 19
currently lose_sum: 97.19132578372955
time_elpased: 2.025
start validation test
0.625773195876
0.589430006552
0.833281877123
0.690457917626
0.62540888238
63.361
batch start
#iterations: 20
currently lose_sum: 97.1630300283432
time_elpased: 2.023
batch start
#iterations: 21
currently lose_sum: 96.98593211174011
time_elpased: 2.07
batch start
#iterations: 22
currently lose_sum: 97.07619732618332
time_elpased: 2.011
batch start
#iterations: 23
currently lose_sum: 96.69039142131805
time_elpased: 2.02
batch start
#iterations: 24
currently lose_sum: 97.02485972642899
time_elpased: 2.004
batch start
#iterations: 25
currently lose_sum: 96.66772270202637
time_elpased: 2.023
batch start
#iterations: 26
currently lose_sum: 96.55797469615936
time_elpased: 2.031
batch start
#iterations: 27
currently lose_sum: 96.2611231803894
time_elpased: 2.01
batch start
#iterations: 28
currently lose_sum: 96.39722084999084
time_elpased: 2.005
batch start
#iterations: 29
currently lose_sum: 96.43703818321228
time_elpased: 2.025
batch start
#iterations: 30
currently lose_sum: 96.1236965060234
time_elpased: 2.027
batch start
#iterations: 31
currently lose_sum: 96.25038921833038
time_elpased: 2.002
batch start
#iterations: 32
currently lose_sum: 95.79404735565186
time_elpased: 2.007
batch start
#iterations: 33
currently lose_sum: 95.60894292593002
time_elpased: 2.051
batch start
#iterations: 34
currently lose_sum: 95.79346191883087
time_elpased: 2.014
batch start
#iterations: 35
currently lose_sum: 95.70143777132034
time_elpased: 2.027
batch start
#iterations: 36
currently lose_sum: 95.74598902463913
time_elpased: 2.023
batch start
#iterations: 37
currently lose_sum: 95.65874582529068
time_elpased: 2.018
batch start
#iterations: 38
currently lose_sum: 95.34042853116989
time_elpased: 2.012
batch start
#iterations: 39
currently lose_sum: 94.67359858751297
time_elpased: 2.034
start validation test
0.637371134021
0.611138736947
0.758876196357
0.677041729789
0.637157813143
61.960
batch start
#iterations: 40
currently lose_sum: 95.33358877897263
time_elpased: 2.017
batch start
#iterations: 41
currently lose_sum: 95.33516800403595
time_elpased: 2.032
batch start
#iterations: 42
currently lose_sum: 94.7154969573021
time_elpased: 2.075
batch start
#iterations: 43
currently lose_sum: 94.89436835050583
time_elpased: 2.025
batch start
#iterations: 44
currently lose_sum: 94.44538694620132
time_elpased: 2.034
batch start
#iterations: 45
currently lose_sum: 94.88511276245117
time_elpased: 2.006
batch start
#iterations: 46
currently lose_sum: 94.53802078962326
time_elpased: 2.01
batch start
#iterations: 47
currently lose_sum: 94.91379034519196
time_elpased: 2.007
batch start
#iterations: 48
currently lose_sum: 94.78492319583893
time_elpased: 2.029
batch start
#iterations: 49
currently lose_sum: 94.77098566293716
time_elpased: 2.017
batch start
#iterations: 50
currently lose_sum: 94.72496753931046
time_elpased: 2.02
batch start
#iterations: 51
currently lose_sum: 94.37728768587112
time_elpased: 2.022
batch start
#iterations: 52
currently lose_sum: 94.46204298734665
time_elpased: 2.044
batch start
#iterations: 53
currently lose_sum: 94.24561667442322
time_elpased: 2.014
batch start
#iterations: 54
currently lose_sum: 94.65767550468445
time_elpased: 2.023
batch start
#iterations: 55
currently lose_sum: 94.43040037155151
time_elpased: 2.048
batch start
#iterations: 56
currently lose_sum: 93.82592952251434
time_elpased: 2.059
batch start
#iterations: 57
currently lose_sum: 94.12636888027191
time_elpased: 2.042
batch start
#iterations: 58
currently lose_sum: 93.86413115262985
time_elpased: 2.044
batch start
#iterations: 59
currently lose_sum: 94.16280430555344
time_elpased: 2.033
start validation test
0.635824742268
0.626962849483
0.673870536174
0.649570953822
0.635757947009
61.844
batch start
#iterations: 60
currently lose_sum: 94.05806982517242
time_elpased: 2.041
batch start
#iterations: 61
currently lose_sum: 93.73209363222122
time_elpased: 2.039
batch start
#iterations: 62
currently lose_sum: 93.84876537322998
time_elpased: 2.001
batch start
#iterations: 63
currently lose_sum: 94.16465252637863
time_elpased: 2.106
batch start
#iterations: 64
currently lose_sum: 93.92611390352249
time_elpased: 2.039
batch start
#iterations: 65
currently lose_sum: 93.74437713623047
time_elpased: 2.014
batch start
#iterations: 66
currently lose_sum: 93.72710484266281
time_elpased: 2.023
batch start
#iterations: 67
currently lose_sum: 93.75035572052002
time_elpased: 2.012
batch start
#iterations: 68
currently lose_sum: 93.62314283847809
time_elpased: 2.036
batch start
#iterations: 69
currently lose_sum: 93.75225269794464
time_elpased: 2.02
batch start
#iterations: 70
currently lose_sum: 93.42286741733551
time_elpased: 2.058
batch start
#iterations: 71
currently lose_sum: 93.44964700937271
time_elpased: 2.032
batch start
#iterations: 72
currently lose_sum: 93.52206844091415
time_elpased: 2.032
batch start
#iterations: 73
currently lose_sum: 93.61233788728714
time_elpased: 2.01
batch start
#iterations: 74
currently lose_sum: 93.55018109083176
time_elpased: 2.013
batch start
#iterations: 75
currently lose_sum: 93.4059002995491
time_elpased: 2.044
batch start
#iterations: 76
currently lose_sum: 93.17287975549698
time_elpased: 2.028
batch start
#iterations: 77
currently lose_sum: 93.39642798900604
time_elpased: 2.021
batch start
#iterations: 78
currently lose_sum: 93.24543410539627
time_elpased: 2.032
batch start
#iterations: 79
currently lose_sum: 93.54849201440811
time_elpased: 2.028
start validation test
0.632628865979
0.625533152385
0.664093856128
0.644237008935
0.632573624336
62.199
batch start
#iterations: 80
currently lose_sum: 93.37309169769287
time_elpased: 2.041
batch start
#iterations: 81
currently lose_sum: 93.4501958489418
time_elpased: 2.027
batch start
#iterations: 82
currently lose_sum: 93.54375213384628
time_elpased: 2.027
batch start
#iterations: 83
currently lose_sum: 93.16251653432846
time_elpased: 2.056
batch start
#iterations: 84
currently lose_sum: 93.02229166030884
time_elpased: 2.06
batch start
#iterations: 85
currently lose_sum: 92.94595223665237
time_elpased: 2.062
batch start
#iterations: 86
currently lose_sum: 93.2082006931305
time_elpased: 2.029
batch start
#iterations: 87
currently lose_sum: 92.94025337696075
time_elpased: 2.03
batch start
#iterations: 88
currently lose_sum: 92.96814662218094
time_elpased: 2.021
batch start
#iterations: 89
currently lose_sum: 92.30931377410889
time_elpased: 2.069
batch start
#iterations: 90
currently lose_sum: 92.50620800256729
time_elpased: 2.004
batch start
#iterations: 91
currently lose_sum: 92.73865294456482
time_elpased: 2.031
batch start
#iterations: 92
currently lose_sum: 92.76451742649078
time_elpased: 2.032
batch start
#iterations: 93
currently lose_sum: 92.7943342924118
time_elpased: 2.03
batch start
#iterations: 94
currently lose_sum: 92.80459535121918
time_elpased: 2.023
batch start
#iterations: 95
currently lose_sum: 92.40671426057816
time_elpased: 2.048
batch start
#iterations: 96
currently lose_sum: 92.50532448291779
time_elpased: 2.013
batch start
#iterations: 97
currently lose_sum: 92.27604389190674
time_elpased: 2.035
batch start
#iterations: 98
currently lose_sum: 92.03553491830826
time_elpased: 2.021
batch start
#iterations: 99
currently lose_sum: 92.04750233888626
time_elpased: 2.028
start validation test
0.635309278351
0.634138911454
0.64268807245
0.638384870943
0.635296323739
62.176
batch start
#iterations: 100
currently lose_sum: 92.60576379299164
time_elpased: 2.003
batch start
#iterations: 101
currently lose_sum: 92.32143795490265
time_elpased: 2.004
batch start
#iterations: 102
currently lose_sum: 92.11963200569153
time_elpased: 2.011
batch start
#iterations: 103
currently lose_sum: 91.8883581161499
time_elpased: 2.03
batch start
#iterations: 104
currently lose_sum: 92.72989350557327
time_elpased: 2.036
batch start
#iterations: 105
currently lose_sum: 92.44551891088486
time_elpased: 2.074
batch start
#iterations: 106
currently lose_sum: 92.17233222723007
time_elpased: 2.018
batch start
#iterations: 107
currently lose_sum: 92.10659807920456
time_elpased: 2.011
batch start
#iterations: 108
currently lose_sum: 91.92524302005768
time_elpased: 2.01
batch start
#iterations: 109
currently lose_sum: 92.21066999435425
time_elpased: 2.038
batch start
#iterations: 110
currently lose_sum: 91.57003629207611
time_elpased: 2.019
batch start
#iterations: 111
currently lose_sum: 91.74471127986908
time_elpased: 2.037
batch start
#iterations: 112
currently lose_sum: 91.54548871517181
time_elpased: 2.026
batch start
#iterations: 113
currently lose_sum: 91.81455427408218
time_elpased: 2.038
batch start
#iterations: 114
currently lose_sum: 91.77484822273254
time_elpased: 2.038
batch start
#iterations: 115
currently lose_sum: 91.5517196059227
time_elpased: 2.013
batch start
#iterations: 116
currently lose_sum: 92.07783842086792
time_elpased: 2.021
batch start
#iterations: 117
currently lose_sum: 91.71115136146545
time_elpased: 2.012
batch start
#iterations: 118
currently lose_sum: 92.2266497015953
time_elpased: 2.021
batch start
#iterations: 119
currently lose_sum: 91.41469621658325
time_elpased: 2.023
start validation test
0.62824742268
0.628106781221
0.631985180611
0.630040012311
0.62824086047
62.837
batch start
#iterations: 120
currently lose_sum: 92.07593548297882
time_elpased: 2.023
batch start
#iterations: 121
currently lose_sum: 91.38755041360855
time_elpased: 2.068
batch start
#iterations: 122
currently lose_sum: 91.33565735816956
time_elpased: 2.037
batch start
#iterations: 123
currently lose_sum: 91.09362196922302
time_elpased: 2.024
batch start
#iterations: 124
currently lose_sum: 91.70326405763626
time_elpased: 2.045
batch start
#iterations: 125
currently lose_sum: 91.56657433509827
time_elpased: 2.04
batch start
#iterations: 126
currently lose_sum: 91.55738240480423
time_elpased: 2.065
batch start
#iterations: 127
currently lose_sum: 91.6641144156456
time_elpased: 2.062
batch start
#iterations: 128
currently lose_sum: 92.04214388132095
time_elpased: 2.067
batch start
#iterations: 129
currently lose_sum: 90.90625756978989
time_elpased: 2.022
batch start
#iterations: 130
currently lose_sum: 91.26100468635559
time_elpased: 2.052
batch start
#iterations: 131
currently lose_sum: 91.01957911252975
time_elpased: 2.039
batch start
#iterations: 132
currently lose_sum: 91.11255496740341
time_elpased: 2.045
batch start
#iterations: 133
currently lose_sum: 90.94927078485489
time_elpased: 2.044
batch start
#iterations: 134
currently lose_sum: 91.0975970029831
time_elpased: 2.054
batch start
#iterations: 135
currently lose_sum: 91.58046168088913
time_elpased: 2.017
batch start
#iterations: 136
currently lose_sum: 90.84043437242508
time_elpased: 2.029
batch start
#iterations: 137
currently lose_sum: 91.64106327295303
time_elpased: 2.046
batch start
#iterations: 138
currently lose_sum: 90.65326172113419
time_elpased: 2.042
batch start
#iterations: 139
currently lose_sum: 91.15515172481537
time_elpased: 2.028
start validation test
0.628762886598
0.618064031546
0.677472470927
0.646406127258
0.62867736941
62.599
batch start
#iterations: 140
currently lose_sum: 90.99101877212524
time_elpased: 2.025
batch start
#iterations: 141
currently lose_sum: 91.19759279489517
time_elpased: 2.016
batch start
#iterations: 142
currently lose_sum: 90.68174189329147
time_elpased: 2.018
batch start
#iterations: 143
currently lose_sum: 91.07987922430038
time_elpased: 2.021
batch start
#iterations: 144
currently lose_sum: 90.45630407333374
time_elpased: 2.036
batch start
#iterations: 145
currently lose_sum: 91.05787748098373
time_elpased: 2.017
batch start
#iterations: 146
currently lose_sum: 90.70557868480682
time_elpased: 2.055
batch start
#iterations: 147
currently lose_sum: 90.17343652248383
time_elpased: 2.036
batch start
#iterations: 148
currently lose_sum: 91.2251809835434
time_elpased: 2.01
batch start
#iterations: 149
currently lose_sum: 91.06792515516281
time_elpased: 2.022
batch start
#iterations: 150
currently lose_sum: 90.59781515598297
time_elpased: 2.039
batch start
#iterations: 151
currently lose_sum: 90.64582520723343
time_elpased: 2.019
batch start
#iterations: 152
currently lose_sum: 90.91353911161423
time_elpased: 2.014
batch start
#iterations: 153
currently lose_sum: 90.34077143669128
time_elpased: 2.027
batch start
#iterations: 154
currently lose_sum: 90.75232541561127
time_elpased: 2.022
batch start
#iterations: 155
currently lose_sum: 90.26778411865234
time_elpased: 2.071
batch start
#iterations: 156
currently lose_sum: 90.55029809474945
time_elpased: 2.023
batch start
#iterations: 157
currently lose_sum: 90.74969851970673
time_elpased: 2.042
batch start
#iterations: 158
currently lose_sum: 90.48032635450363
time_elpased: 2.024
batch start
#iterations: 159
currently lose_sum: 90.95480024814606
time_elpased: 2.02
start validation test
0.598711340206
0.597085427136
0.611402696305
0.604159251538
0.598689058573
64.856
batch start
#iterations: 160
currently lose_sum: 90.67810368537903
time_elpased: 2.021
batch start
#iterations: 161
currently lose_sum: 90.02879202365875
time_elpased: 2.025
batch start
#iterations: 162
currently lose_sum: 90.15865641832352
time_elpased: 2.041
batch start
#iterations: 163
currently lose_sum: 90.10753691196442
time_elpased: 2.043
batch start
#iterations: 164
currently lose_sum: 90.31952446699142
time_elpased: 2.054
batch start
#iterations: 165
currently lose_sum: 90.24507504701614
time_elpased: 2.042
batch start
#iterations: 166
currently lose_sum: 90.03125870227814
time_elpased: 2.015
batch start
#iterations: 167
currently lose_sum: 90.65434181690216
time_elpased: 2.074
batch start
#iterations: 168
currently lose_sum: 89.28242892026901
time_elpased: 2.064
batch start
#iterations: 169
currently lose_sum: 89.82531899213791
time_elpased: 2.042
batch start
#iterations: 170
currently lose_sum: 89.9744735956192
time_elpased: 2.021
batch start
#iterations: 171
currently lose_sum: 89.76927274465561
time_elpased: 2.042
batch start
#iterations: 172
currently lose_sum: 90.31141889095306
time_elpased: 2.044
batch start
#iterations: 173
currently lose_sum: 89.74303662776947
time_elpased: 2.07
batch start
#iterations: 174
currently lose_sum: 89.53239518404007
time_elpased: 2.044
batch start
#iterations: 175
currently lose_sum: 89.69789260625839
time_elpased: 2.036
batch start
#iterations: 176
currently lose_sum: 89.5922179222107
time_elpased: 2.025
batch start
#iterations: 177
currently lose_sum: 89.19649994373322
time_elpased: 2.05
batch start
#iterations: 178
currently lose_sum: 89.16519635915756
time_elpased: 2.038
batch start
#iterations: 179
currently lose_sum: 90.17739808559418
time_elpased: 2.076
start validation test
0.627628865979
0.641150492583
0.582690130699
0.610524045719
0.62770776286
64.754
batch start
#iterations: 180
currently lose_sum: 89.56653374433517
time_elpased: 2.029
batch start
#iterations: 181
currently lose_sum: 89.57192265987396
time_elpased: 2.016
batch start
#iterations: 182
currently lose_sum: 89.84028869867325
time_elpased: 2.035
batch start
#iterations: 183
currently lose_sum: 89.35058426856995
time_elpased: 2.011
batch start
#iterations: 184
currently lose_sum: 90.10878670215607
time_elpased: 2.033
batch start
#iterations: 185
currently lose_sum: 89.24479359388351
time_elpased: 2.027
batch start
#iterations: 186
currently lose_sum: 89.53874659538269
time_elpased: 2.015
batch start
#iterations: 187
currently lose_sum: 89.14531409740448
time_elpased: 2.041
batch start
#iterations: 188
currently lose_sum: 89.38858807086945
time_elpased: 2.059
batch start
#iterations: 189
currently lose_sum: 89.18363577127457
time_elpased: 2.063
batch start
#iterations: 190
currently lose_sum: 89.59793704748154
time_elpased: 2.04
batch start
#iterations: 191
currently lose_sum: 89.27568978071213
time_elpased: 2.02
batch start
#iterations: 192
currently lose_sum: 88.91460299491882
time_elpased: 2.041
batch start
#iterations: 193
currently lose_sum: 89.75852876901627
time_elpased: 2.048
batch start
#iterations: 194
currently lose_sum: 89.49149966239929
time_elpased: 2.05
batch start
#iterations: 195
currently lose_sum: 89.12031000852585
time_elpased: 2.035
batch start
#iterations: 196
currently lose_sum: 89.50510728359222
time_elpased: 2.068
batch start
#iterations: 197
currently lose_sum: 89.47962021827698
time_elpased: 2.046
batch start
#iterations: 198
currently lose_sum: 88.99010878801346
time_elpased: 2.015
batch start
#iterations: 199
currently lose_sum: 89.16137164831161
time_elpased: 2.033
start validation test
0.626340206186
0.625228333672
0.634043429042
0.629605027847
0.62632668199
64.441
batch start
#iterations: 200
currently lose_sum: 89.10666292905807
time_elpased: 2.06
batch start
#iterations: 201
currently lose_sum: 88.9993908405304
time_elpased: 2.02
batch start
#iterations: 202
currently lose_sum: 88.51803463697433
time_elpased: 2.036
batch start
#iterations: 203
currently lose_sum: 88.83695209026337
time_elpased: 2.016
batch start
#iterations: 204
currently lose_sum: 88.80821186304092
time_elpased: 2.023
batch start
#iterations: 205
currently lose_sum: 89.05910688638687
time_elpased: 2.04
batch start
#iterations: 206
currently lose_sum: 88.6035607457161
time_elpased: 2.02
batch start
#iterations: 207
currently lose_sum: 88.80003947019577
time_elpased: 2.035
batch start
#iterations: 208
currently lose_sum: 88.85825788974762
time_elpased: 2.046
batch start
#iterations: 209
currently lose_sum: 88.85843527317047
time_elpased: 2.052
batch start
#iterations: 210
currently lose_sum: 88.73699647188187
time_elpased: 2.026
batch start
#iterations: 211
currently lose_sum: 88.51510351896286
time_elpased: 2.026
batch start
#iterations: 212
currently lose_sum: 88.49919146299362
time_elpased: 2.015
batch start
#iterations: 213
currently lose_sum: 88.64224469661713
time_elpased: 2.038
batch start
#iterations: 214
currently lose_sum: 88.48326772451401
time_elpased: 2.025
batch start
#iterations: 215
currently lose_sum: 88.72854590415955
time_elpased: 2.037
batch start
#iterations: 216
currently lose_sum: 88.42677265405655
time_elpased: 2.127
batch start
#iterations: 217
currently lose_sum: 88.84642732143402
time_elpased: 2.045
batch start
#iterations: 218
currently lose_sum: 88.97819381952286
time_elpased: 2.019
batch start
#iterations: 219
currently lose_sum: 88.55470305681229
time_elpased: 2.037
start validation test
0.609536082474
0.638407857327
0.508387362355
0.566026926382
0.609713664654
65.925
batch start
#iterations: 220
currently lose_sum: 88.52292060852051
time_elpased: 2.015
batch start
#iterations: 221
currently lose_sum: 88.39136219024658
time_elpased: 2.036
batch start
#iterations: 222
currently lose_sum: 88.17892998456955
time_elpased: 2.038
batch start
#iterations: 223
currently lose_sum: 88.94743537902832
time_elpased: 2.052
batch start
#iterations: 224
currently lose_sum: 88.60060691833496
time_elpased: 2.023
batch start
#iterations: 225
currently lose_sum: 88.53324723243713
time_elpased: 2.025
batch start
#iterations: 226
currently lose_sum: 88.49139833450317
time_elpased: 2.023
batch start
#iterations: 227
currently lose_sum: 88.65557253360748
time_elpased: 2.057
batch start
#iterations: 228
currently lose_sum: 87.98869329690933
time_elpased: 2.025
batch start
#iterations: 229
currently lose_sum: 88.08535373210907
time_elpased: 2.072
batch start
#iterations: 230
currently lose_sum: 88.33631139993668
time_elpased: 2.047
batch start
#iterations: 231
currently lose_sum: 88.5199533700943
time_elpased: 2.044
batch start
#iterations: 232
currently lose_sum: 88.16778719425201
time_elpased: 2.013
batch start
#iterations: 233
currently lose_sum: 88.10524189472198
time_elpased: 2.03
batch start
#iterations: 234
currently lose_sum: 88.51960635185242
time_elpased: 2.023
batch start
#iterations: 235
currently lose_sum: 87.85772460699081
time_elpased: 2.061
batch start
#iterations: 236
currently lose_sum: 88.10541647672653
time_elpased: 2.062
batch start
#iterations: 237
currently lose_sum: 88.32186734676361
time_elpased: 2.069
batch start
#iterations: 238
currently lose_sum: 88.18735003471375
time_elpased: 2.04
batch start
#iterations: 239
currently lose_sum: 88.32438641786575
time_elpased: 2.033
start validation test
0.623092783505
0.617489008305
0.650406504065
0.633520449078
0.623044830056
64.374
batch start
#iterations: 240
currently lose_sum: 87.90373849868774
time_elpased: 2.021
batch start
#iterations: 241
currently lose_sum: 88.02516502141953
time_elpased: 2.078
batch start
#iterations: 242
currently lose_sum: 87.7601860165596
time_elpased: 2.048
batch start
#iterations: 243
currently lose_sum: 87.82894384860992
time_elpased: 2.029
batch start
#iterations: 244
currently lose_sum: 87.84861624240875
time_elpased: 2.048
batch start
#iterations: 245
currently lose_sum: 87.47810506820679
time_elpased: 2.064
batch start
#iterations: 246
currently lose_sum: 87.11474406719208
time_elpased: 2.037
batch start
#iterations: 247
currently lose_sum: 87.5435180068016
time_elpased: 2.058
batch start
#iterations: 248
currently lose_sum: 88.19678348302841
time_elpased: 2.029
batch start
#iterations: 249
currently lose_sum: 87.7709436416626
time_elpased: 2.047
batch start
#iterations: 250
currently lose_sum: 87.30720055103302
time_elpased: 2.053
batch start
#iterations: 251
currently lose_sum: 87.13225162029266
time_elpased: 2.056
batch start
#iterations: 252
currently lose_sum: 87.6264306306839
time_elpased: 2.041
batch start
#iterations: 253
currently lose_sum: 87.63340270519257
time_elpased: 2.055
batch start
#iterations: 254
currently lose_sum: 87.67377316951752
time_elpased: 2.03
batch start
#iterations: 255
currently lose_sum: 87.05505007505417
time_elpased: 2.028
batch start
#iterations: 256
currently lose_sum: 87.30490750074387
time_elpased: 2.036
batch start
#iterations: 257
currently lose_sum: 87.36894136667252
time_elpased: 2.047
batch start
#iterations: 258
currently lose_sum: 87.13395410776138
time_elpased: 2.037
batch start
#iterations: 259
currently lose_sum: 86.87919908761978
time_elpased: 2.076
start validation test
0.624432989691
0.633468760294
0.593701759802
0.612940926477
0.624486943105
66.626
batch start
#iterations: 260
currently lose_sum: 87.25231891870499
time_elpased: 2.044
batch start
#iterations: 261
currently lose_sum: 86.92817813158035
time_elpased: 2.052
batch start
#iterations: 262
currently lose_sum: 87.0685561299324
time_elpased: 2.05
batch start
#iterations: 263
currently lose_sum: 86.91100031137466
time_elpased: 2.04
batch start
#iterations: 264
currently lose_sum: 86.55358403921127
time_elpased: 2.041
batch start
#iterations: 265
currently lose_sum: 87.12523591518402
time_elpased: 2.054
batch start
#iterations: 266
currently lose_sum: 87.30260717868805
time_elpased: 2.028
batch start
#iterations: 267
currently lose_sum: 87.043272793293
time_elpased: 2.033
batch start
#iterations: 268
currently lose_sum: 86.64781373739243
time_elpased: 2.055
batch start
#iterations: 269
currently lose_sum: 87.08540678024292
time_elpased: 2.072
batch start
#iterations: 270
currently lose_sum: 86.84623908996582
time_elpased: 2.067
batch start
#iterations: 271
currently lose_sum: 87.18391871452332
time_elpased: 2.057
batch start
#iterations: 272
currently lose_sum: 87.14302027225494
time_elpased: 2.072
batch start
#iterations: 273
currently lose_sum: 86.94484800100327
time_elpased: 2.038
batch start
#iterations: 274
currently lose_sum: 86.87968063354492
time_elpased: 2.058
batch start
#iterations: 275
currently lose_sum: 86.72652381658554
time_elpased: 2.043
batch start
#iterations: 276
currently lose_sum: 86.33144795894623
time_elpased: 2.068
batch start
#iterations: 277
currently lose_sum: 86.86247569322586
time_elpased: 2.075
batch start
#iterations: 278
currently lose_sum: 86.01136910915375
time_elpased: 2.04
batch start
#iterations: 279
currently lose_sum: 86.84599143266678
time_elpased: 2.041
start validation test
0.608402061856
0.607920993688
0.614490068951
0.611187880649
0.60839137342
67.622
batch start
#iterations: 280
currently lose_sum: 86.07059025764465
time_elpased: 2.076
batch start
#iterations: 281
currently lose_sum: 87.14259564876556
time_elpased: 2.068
batch start
#iterations: 282
currently lose_sum: 86.12672626972198
time_elpased: 2.043
batch start
#iterations: 283
currently lose_sum: 85.99127411842346
time_elpased: 2.057
batch start
#iterations: 284
currently lose_sum: 87.01162040233612
time_elpased: 2.062
batch start
#iterations: 285
currently lose_sum: 86.64462351799011
time_elpased: 2.043
batch start
#iterations: 286
currently lose_sum: 86.42914181947708
time_elpased: 2.047
batch start
#iterations: 287
currently lose_sum: 86.25806677341461
time_elpased: 2.051
batch start
#iterations: 288
currently lose_sum: 86.10998755693436
time_elpased: 2.042
batch start
#iterations: 289
currently lose_sum: 86.4067651629448
time_elpased: 2.041
batch start
#iterations: 290
currently lose_sum: 86.28426277637482
time_elpased: 2.033
batch start
#iterations: 291
currently lose_sum: 86.00149899721146
time_elpased: 2.082
batch start
#iterations: 292
currently lose_sum: 86.7409520149231
time_elpased: 2.032
batch start
#iterations: 293
currently lose_sum: 86.1439665555954
time_elpased: 2.132
batch start
#iterations: 294
currently lose_sum: 86.11283105611801
time_elpased: 2.087
batch start
#iterations: 295
currently lose_sum: 85.83847367763519
time_elpased: 2.076
batch start
#iterations: 296
currently lose_sum: 85.65615046024323
time_elpased: 2.03
batch start
#iterations: 297
currently lose_sum: 85.62543660402298
time_elpased: 2.031
batch start
#iterations: 298
currently lose_sum: 86.19673919677734
time_elpased: 2.042
batch start
#iterations: 299
currently lose_sum: 85.72700667381287
time_elpased: 2.025
start validation test
0.617371134021
0.626238168611
0.585571678502
0.605222570866
0.61742696287
69.419
batch start
#iterations: 300
currently lose_sum: 85.97705447673798
time_elpased: 2.06
batch start
#iterations: 301
currently lose_sum: 86.05725252628326
time_elpased: 2.039
batch start
#iterations: 302
currently lose_sum: 86.17553567886353
time_elpased: 2.062
batch start
#iterations: 303
currently lose_sum: 86.79558330774307
time_elpased: 2.046
batch start
#iterations: 304
currently lose_sum: 85.92338371276855
time_elpased: 2.044
batch start
#iterations: 305
currently lose_sum: 85.9906347990036
time_elpased: 2.04
batch start
#iterations: 306
currently lose_sum: 85.7628955245018
time_elpased: 2.051
batch start
#iterations: 307
currently lose_sum: 85.96889168024063
time_elpased: 2.038
batch start
#iterations: 308
currently lose_sum: 85.70152765512466
time_elpased: 2.05
batch start
#iterations: 309
currently lose_sum: 86.12042582035065
time_elpased: 2.02
batch start
#iterations: 310
currently lose_sum: 85.43428510427475
time_elpased: 2.034
batch start
#iterations: 311
currently lose_sum: 85.48419100046158
time_elpased: 2.038
batch start
#iterations: 312
currently lose_sum: 84.81744855642319
time_elpased: 2.041
batch start
#iterations: 313
currently lose_sum: 85.30688714981079
time_elpased: 2.08
batch start
#iterations: 314
currently lose_sum: 85.69530433416367
time_elpased: 2.052
batch start
#iterations: 315
currently lose_sum: 85.30680751800537
time_elpased: 2.044
batch start
#iterations: 316
currently lose_sum: 86.01966112852097
time_elpased: 2.02
batch start
#iterations: 317
currently lose_sum: 85.45110660791397
time_elpased: 2.047
batch start
#iterations: 318
currently lose_sum: 85.36027908325195
time_elpased: 2.038
batch start
#iterations: 319
currently lose_sum: 85.58577519655228
time_elpased: 2.078
start validation test
0.611494845361
0.620548551205
0.577441597201
0.598219521296
0.61155463109
67.198
batch start
#iterations: 320
currently lose_sum: 85.91586714982986
time_elpased: 2.051
batch start
#iterations: 321
currently lose_sum: 85.64099729061127
time_elpased: 2.086
batch start
#iterations: 322
currently lose_sum: 85.83008641004562
time_elpased: 2.016
batch start
#iterations: 323
currently lose_sum: 85.90785205364227
time_elpased: 2.043
batch start
#iterations: 324
currently lose_sum: 85.80009514093399
time_elpased: 2.033
batch start
#iterations: 325
currently lose_sum: 85.47971433401108
time_elpased: 2.075
batch start
#iterations: 326
currently lose_sum: 85.31957858800888
time_elpased: 2.039
batch start
#iterations: 327
currently lose_sum: 85.11820417642593
time_elpased: 2.035
batch start
#iterations: 328
currently lose_sum: 84.96965366601944
time_elpased: 2.025
batch start
#iterations: 329
currently lose_sum: 85.12631595134735
time_elpased: 2.022
batch start
#iterations: 330
currently lose_sum: 84.97069978713989
time_elpased: 2.036
batch start
#iterations: 331
currently lose_sum: 84.89742058515549
time_elpased: 2.204
batch start
#iterations: 332
currently lose_sum: 84.48810130357742
time_elpased: 2.084
batch start
#iterations: 333
currently lose_sum: 84.76075464487076
time_elpased: 2.041
batch start
#iterations: 334
currently lose_sum: 84.8114482164383
time_elpased: 2.066
batch start
#iterations: 335
currently lose_sum: 84.67470920085907
time_elpased: 2.041
batch start
#iterations: 336
currently lose_sum: 84.26735216379166
time_elpased: 2.025
batch start
#iterations: 337
currently lose_sum: 85.23446881771088
time_elpased: 2.051
batch start
#iterations: 338
currently lose_sum: 85.16555690765381
time_elpased: 2.094
batch start
#iterations: 339
currently lose_sum: 84.86299702525139
time_elpased: 2.067
start validation test
0.612783505155
0.622296173045
0.577338684779
0.598975016015
0.612845734004
69.793
batch start
#iterations: 340
currently lose_sum: 85.1924420595169
time_elpased: 2.044
batch start
#iterations: 341
currently lose_sum: 84.79423439502716
time_elpased: 2.034
batch start
#iterations: 342
currently lose_sum: 84.53186964988708
time_elpased: 2.038
batch start
#iterations: 343
currently lose_sum: 84.67347002029419
time_elpased: 2.056
batch start
#iterations: 344
currently lose_sum: 84.1716360449791
time_elpased: 2.078
batch start
#iterations: 345
currently lose_sum: 84.80866503715515
time_elpased: 2.021
batch start
#iterations: 346
currently lose_sum: 84.33983790874481
time_elpased: 2.022
batch start
#iterations: 347
currently lose_sum: 84.67425620555878
time_elpased: 2.065
batch start
#iterations: 348
currently lose_sum: 84.33705860376358
time_elpased: 2.028
batch start
#iterations: 349
currently lose_sum: 84.36720210313797
time_elpased: 2.014
batch start
#iterations: 350
currently lose_sum: 84.14153617620468
time_elpased: 2.03
batch start
#iterations: 351
currently lose_sum: 84.33943670988083
time_elpased: 2.023
batch start
#iterations: 352
currently lose_sum: 84.2581535577774
time_elpased: 2.035
batch start
#iterations: 353
currently lose_sum: 84.6183847784996
time_elpased: 2.078
batch start
#iterations: 354
currently lose_sum: 84.4355406165123
time_elpased: 2.031
batch start
#iterations: 355
currently lose_sum: 84.05761569738388
time_elpased: 2.072
batch start
#iterations: 356
currently lose_sum: 84.00261288881302
time_elpased: 2.084
batch start
#iterations: 357
currently lose_sum: 83.69757914543152
time_elpased: 2.066
batch start
#iterations: 358
currently lose_sum: 83.61102080345154
time_elpased: 2.027
batch start
#iterations: 359
currently lose_sum: 84.096466422081
time_elpased: 2.073
start validation test
0.61206185567
0.62371541502
0.568385304106
0.594766314883
0.612138536593
70.587
batch start
#iterations: 360
currently lose_sum: 84.57674366235733
time_elpased: 2.036
batch start
#iterations: 361
currently lose_sum: 84.334048807621
time_elpased: 2.035
batch start
#iterations: 362
currently lose_sum: 84.05239200592041
time_elpased: 2.043
batch start
#iterations: 363
currently lose_sum: 83.51442855596542
time_elpased: 2.03
batch start
#iterations: 364
currently lose_sum: 83.96626806259155
time_elpased: 2.06
batch start
#iterations: 365
currently lose_sum: 84.1538514494896
time_elpased: 2.033
batch start
#iterations: 366
currently lose_sum: 84.14917069673538
time_elpased: 2.054
batch start
#iterations: 367
currently lose_sum: 83.71543964743614
time_elpased: 2.051
batch start
#iterations: 368
currently lose_sum: 83.54534032940865
time_elpased: 2.08
batch start
#iterations: 369
currently lose_sum: 84.0290784239769
time_elpased: 2.066
batch start
#iterations: 370
currently lose_sum: 83.1919350028038
time_elpased: 2.049
batch start
#iterations: 371
currently lose_sum: 82.91357845067978
time_elpased: 2.06
batch start
#iterations: 372
currently lose_sum: 84.10627400875092
time_elpased: 2.065
batch start
#iterations: 373
currently lose_sum: 83.27262204885483
time_elpased: 2.059
batch start
#iterations: 374
currently lose_sum: 83.40102899074554
time_elpased: 2.06
batch start
#iterations: 375
currently lose_sum: 83.13848394155502
time_elpased: 2.039
batch start
#iterations: 376
currently lose_sum: 83.05217522382736
time_elpased: 2.088
batch start
#iterations: 377
currently lose_sum: 83.70177155733109
time_elpased: 2.004
batch start
#iterations: 378
currently lose_sum: 83.57636949419975
time_elpased: 2.073
batch start
#iterations: 379
currently lose_sum: 83.7007308602333
time_elpased: 2.052
start validation test
0.603298969072
0.605778289543
0.59555418339
0.600622729632
0.603312566238
71.100
batch start
#iterations: 380
currently lose_sum: 83.77941328287125
time_elpased: 2.04
batch start
#iterations: 381
currently lose_sum: 84.6954510807991
time_elpased: 2.026
batch start
#iterations: 382
currently lose_sum: 83.09557056427002
time_elpased: 2.021
batch start
#iterations: 383
currently lose_sum: 83.85354995727539
time_elpased: 2.01
batch start
#iterations: 384
currently lose_sum: 83.64536160230637
time_elpased: 2.03
batch start
#iterations: 385
currently lose_sum: 82.71108990907669
time_elpased: 2.031
batch start
#iterations: 386
currently lose_sum: 83.83248102664948
time_elpased: 2.027
batch start
#iterations: 387
currently lose_sum: 82.56198835372925
time_elpased: 2.013
batch start
#iterations: 388
currently lose_sum: 83.25836309790611
time_elpased: 2.054
batch start
#iterations: 389
currently lose_sum: 83.04586774110794
time_elpased: 2.052
batch start
#iterations: 390
currently lose_sum: 83.53619807958603
time_elpased: 2.046
batch start
#iterations: 391
currently lose_sum: 82.80445611476898
time_elpased: 2.055
batch start
#iterations: 392
currently lose_sum: 82.55367922782898
time_elpased: 2.037
batch start
#iterations: 393
currently lose_sum: 83.2233619093895
time_elpased: 2.045
batch start
#iterations: 394
currently lose_sum: 83.32036364078522
time_elpased: 2.054
batch start
#iterations: 395
currently lose_sum: 83.43420985341072
time_elpased: 2.089
batch start
#iterations: 396
currently lose_sum: 82.82264739274979
time_elpased: 2.047
batch start
#iterations: 397
currently lose_sum: 82.31907021999359
time_elpased: 2.053
batch start
#iterations: 398
currently lose_sum: 82.85244709253311
time_elpased: 2.041
batch start
#iterations: 399
currently lose_sum: 82.89510691165924
time_elpased: 2.034
start validation test
0.587319587629
0.633442520668
0.41792734383
0.503596230159
0.58761698184
73.926
acc: 0.627
pre: 0.620
rec: 0.662
F1: 0.640
auc: 0.627
