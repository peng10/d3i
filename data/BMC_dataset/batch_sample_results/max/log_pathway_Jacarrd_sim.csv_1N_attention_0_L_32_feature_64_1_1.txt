start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.7747637629509
time_elpased: 2.061
batch start
#iterations: 1
currently lose_sum: 99.99551123380661
time_elpased: 1.96
batch start
#iterations: 2
currently lose_sum: 99.81057250499725
time_elpased: 1.951
batch start
#iterations: 3
currently lose_sum: 99.63484591245651
time_elpased: 1.947
batch start
#iterations: 4
currently lose_sum: 99.26014715433121
time_elpased: 1.947
batch start
#iterations: 5
currently lose_sum: 98.97724670171738
time_elpased: 1.957
batch start
#iterations: 6
currently lose_sum: 98.7571496963501
time_elpased: 1.93
batch start
#iterations: 7
currently lose_sum: 98.80369246006012
time_elpased: 1.939
batch start
#iterations: 8
currently lose_sum: 98.67944097518921
time_elpased: 1.905
batch start
#iterations: 9
currently lose_sum: 98.51880079507828
time_elpased: 1.922
batch start
#iterations: 10
currently lose_sum: 98.53498578071594
time_elpased: 1.97
batch start
#iterations: 11
currently lose_sum: 98.17495846748352
time_elpased: 1.961
batch start
#iterations: 12
currently lose_sum: 98.63471323251724
time_elpased: 1.931
batch start
#iterations: 13
currently lose_sum: 98.30363595485687
time_elpased: 1.998
batch start
#iterations: 14
currently lose_sum: 97.87062102556229
time_elpased: 1.935
batch start
#iterations: 15
currently lose_sum: 97.7834095954895
time_elpased: 1.938
batch start
#iterations: 16
currently lose_sum: 97.76815700531006
time_elpased: 1.942
batch start
#iterations: 17
currently lose_sum: 98.01583802700043
time_elpased: 1.959
batch start
#iterations: 18
currently lose_sum: 97.62787413597107
time_elpased: 1.903
batch start
#iterations: 19
currently lose_sum: 97.54600149393082
time_elpased: 1.943
start validation test
0.613659793814
0.624356391314
0.57404548729
0.598144871589
0.61372934284
63.985
batch start
#iterations: 20
currently lose_sum: 97.8876223564148
time_elpased: 1.974
batch start
#iterations: 21
currently lose_sum: 97.53336727619171
time_elpased: 1.955
batch start
#iterations: 22
currently lose_sum: 97.30536955595016
time_elpased: 1.936
batch start
#iterations: 23
currently lose_sum: 97.55131822824478
time_elpased: 1.942
batch start
#iterations: 24
currently lose_sum: 97.16924858093262
time_elpased: 1.925
batch start
#iterations: 25
currently lose_sum: 97.53753393888474
time_elpased: 1.995
batch start
#iterations: 26
currently lose_sum: 97.29240590333939
time_elpased: 1.944
batch start
#iterations: 27
currently lose_sum: 97.16362822055817
time_elpased: 1.944
batch start
#iterations: 28
currently lose_sum: 96.7368438243866
time_elpased: 1.997
batch start
#iterations: 29
currently lose_sum: 97.48349559307098
time_elpased: 1.997
batch start
#iterations: 30
currently lose_sum: 96.85553985834122
time_elpased: 1.942
batch start
#iterations: 31
currently lose_sum: 96.86602795124054
time_elpased: 1.935
batch start
#iterations: 32
currently lose_sum: 96.79661673307419
time_elpased: 1.914
batch start
#iterations: 33
currently lose_sum: 96.50981879234314
time_elpased: 1.954
batch start
#iterations: 34
currently lose_sum: 96.65758806467056
time_elpased: 1.908
batch start
#iterations: 35
currently lose_sum: 96.75452250242233
time_elpased: 1.96
batch start
#iterations: 36
currently lose_sum: 96.64214324951172
time_elpased: 1.933
batch start
#iterations: 37
currently lose_sum: 96.34153807163239
time_elpased: 1.943
batch start
#iterations: 38
currently lose_sum: 96.16707682609558
time_elpased: 1.952
batch start
#iterations: 39
currently lose_sum: 96.23300725221634
time_elpased: 1.934
start validation test
0.614793814433
0.623134328358
0.584336729443
0.603112220511
0.614847286543
63.444
batch start
#iterations: 40
currently lose_sum: 96.3817378282547
time_elpased: 1.911
batch start
#iterations: 41
currently lose_sum: 96.00661617517471
time_elpased: 1.994
batch start
#iterations: 42
currently lose_sum: 96.09756511449814
time_elpased: 1.927
batch start
#iterations: 43
currently lose_sum: 95.81662458181381
time_elpased: 1.958
batch start
#iterations: 44
currently lose_sum: 95.92672348022461
time_elpased: 1.909
batch start
#iterations: 45
currently lose_sum: 95.46839147806168
time_elpased: 1.937
batch start
#iterations: 46
currently lose_sum: 95.79718059301376
time_elpased: 1.953
batch start
#iterations: 47
currently lose_sum: 95.77035391330719
time_elpased: 1.962
batch start
#iterations: 48
currently lose_sum: 96.05352199077606
time_elpased: 1.936
batch start
#iterations: 49
currently lose_sum: 95.76374042034149
time_elpased: 1.961
batch start
#iterations: 50
currently lose_sum: 96.06723308563232
time_elpased: 1.95
batch start
#iterations: 51
currently lose_sum: 95.49629819393158
time_elpased: 1.957
batch start
#iterations: 52
currently lose_sum: 95.47116965055466
time_elpased: 1.982
batch start
#iterations: 53
currently lose_sum: 95.92808103561401
time_elpased: 1.958
batch start
#iterations: 54
currently lose_sum: 95.44800621271133
time_elpased: 1.941
batch start
#iterations: 55
currently lose_sum: 95.20418572425842
time_elpased: 1.957
batch start
#iterations: 56
currently lose_sum: 95.36014944314957
time_elpased: 1.936
batch start
#iterations: 57
currently lose_sum: 95.220898270607
time_elpased: 1.963
batch start
#iterations: 58
currently lose_sum: 95.14863383769989
time_elpased: 1.961
batch start
#iterations: 59
currently lose_sum: 95.35479658842087
time_elpased: 1.969
start validation test
0.618092783505
0.610706062932
0.655140475455
0.632143389107
0.618027740568
63.352
batch start
#iterations: 60
currently lose_sum: 95.1000143289566
time_elpased: 1.944
batch start
#iterations: 61
currently lose_sum: 95.35721385478973
time_elpased: 1.985
batch start
#iterations: 62
currently lose_sum: 94.93787151575089
time_elpased: 1.927
batch start
#iterations: 63
currently lose_sum: 95.06566089391708
time_elpased: 1.984
batch start
#iterations: 64
currently lose_sum: 94.7451342344284
time_elpased: 1.934
batch start
#iterations: 65
currently lose_sum: 94.60521894693375
time_elpased: 1.99
batch start
#iterations: 66
currently lose_sum: 94.61959999799728
time_elpased: 1.919
batch start
#iterations: 67
currently lose_sum: 94.83623319864273
time_elpased: 1.933
batch start
#iterations: 68
currently lose_sum: 94.99838590621948
time_elpased: 1.935
batch start
#iterations: 69
currently lose_sum: 94.75888305902481
time_elpased: 1.96
batch start
#iterations: 70
currently lose_sum: 94.69796830415726
time_elpased: 1.949
batch start
#iterations: 71
currently lose_sum: 94.47132635116577
time_elpased: 1.938
batch start
#iterations: 72
currently lose_sum: 94.68826657533646
time_elpased: 1.946
batch start
#iterations: 73
currently lose_sum: 94.43039155006409
time_elpased: 1.964
batch start
#iterations: 74
currently lose_sum: 94.49492335319519
time_elpased: 1.948
batch start
#iterations: 75
currently lose_sum: 94.22756922245026
time_elpased: 1.946
batch start
#iterations: 76
currently lose_sum: 94.18861162662506
time_elpased: 1.936
batch start
#iterations: 77
currently lose_sum: 94.4633339047432
time_elpased: 1.976
batch start
#iterations: 78
currently lose_sum: 94.5041069984436
time_elpased: 2.028
batch start
#iterations: 79
currently lose_sum: 94.21579599380493
time_elpased: 1.933
start validation test
0.605206185567
0.613400925722
0.572810538232
0.59241126071
0.605263061123
63.649
batch start
#iterations: 80
currently lose_sum: 94.561472594738
time_elpased: 1.943
batch start
#iterations: 81
currently lose_sum: 94.13778561353683
time_elpased: 1.94
batch start
#iterations: 82
currently lose_sum: 94.02078938484192
time_elpased: 1.997
batch start
#iterations: 83
currently lose_sum: 93.97268384695053
time_elpased: 1.922
batch start
#iterations: 84
currently lose_sum: 94.0515501499176
time_elpased: 1.935
batch start
#iterations: 85
currently lose_sum: 93.89054983854294
time_elpased: 1.938
batch start
#iterations: 86
currently lose_sum: 94.12542992830276
time_elpased: 1.935
batch start
#iterations: 87
currently lose_sum: 93.85394793748856
time_elpased: 1.921
batch start
#iterations: 88
currently lose_sum: 93.66580575704575
time_elpased: 1.945
batch start
#iterations: 89
currently lose_sum: 93.52218180894852
time_elpased: 1.972
batch start
#iterations: 90
currently lose_sum: 93.80768591165543
time_elpased: 1.961
batch start
#iterations: 91
currently lose_sum: 93.65889543294907
time_elpased: 1.946
batch start
#iterations: 92
currently lose_sum: 93.55638092756271
time_elpased: 1.963
batch start
#iterations: 93
currently lose_sum: 93.73646891117096
time_elpased: 1.926
batch start
#iterations: 94
currently lose_sum: 93.72167402505875
time_elpased: 1.946
batch start
#iterations: 95
currently lose_sum: 93.2101981639862
time_elpased: 1.924
batch start
#iterations: 96
currently lose_sum: 93.45360332727432
time_elpased: 1.944
batch start
#iterations: 97
currently lose_sum: 93.41782343387604
time_elpased: 1.951
batch start
#iterations: 98
currently lose_sum: 92.95008534193039
time_elpased: 1.953
batch start
#iterations: 99
currently lose_sum: 92.93695050477982
time_elpased: 1.931
start validation test
0.598505154639
0.633444075305
0.470927240918
0.540227849596
0.598729137344
64.067
batch start
#iterations: 100
currently lose_sum: 93.13643169403076
time_elpased: 1.955
batch start
#iterations: 101
currently lose_sum: 93.33757257461548
time_elpased: 1.944
batch start
#iterations: 102
currently lose_sum: 92.78835588693619
time_elpased: 2.007
batch start
#iterations: 103
currently lose_sum: 93.27883243560791
time_elpased: 1.94
batch start
#iterations: 104
currently lose_sum: 93.12711584568024
time_elpased: 1.954
batch start
#iterations: 105
currently lose_sum: 93.27638113498688
time_elpased: 1.985
batch start
#iterations: 106
currently lose_sum: 92.69589430093765
time_elpased: 1.926
batch start
#iterations: 107
currently lose_sum: 92.97463762760162
time_elpased: 1.902
batch start
#iterations: 108
currently lose_sum: 92.93866950273514
time_elpased: 1.954
batch start
#iterations: 109
currently lose_sum: 93.10821485519409
time_elpased: 1.941
batch start
#iterations: 110
currently lose_sum: 92.65591162443161
time_elpased: 1.977
batch start
#iterations: 111
currently lose_sum: 92.82255983352661
time_elpased: 1.949
batch start
#iterations: 112
currently lose_sum: 92.5181155204773
time_elpased: 1.926
batch start
#iterations: 113
currently lose_sum: 92.93390369415283
time_elpased: 1.959
batch start
#iterations: 114
currently lose_sum: 92.55146789550781
time_elpased: 1.935
batch start
#iterations: 115
currently lose_sum: 92.42844837903976
time_elpased: 1.939
batch start
#iterations: 116
currently lose_sum: 92.56506252288818
time_elpased: 1.931
batch start
#iterations: 117
currently lose_sum: 92.79298114776611
time_elpased: 2.048
batch start
#iterations: 118
currently lose_sum: 92.33645153045654
time_elpased: 1.944
batch start
#iterations: 119
currently lose_sum: 92.59275341033936
time_elpased: 1.944
start validation test
0.57912371134
0.625120928733
0.398991458269
0.487090897669
0.579439961294
65.099
batch start
#iterations: 120
currently lose_sum: 92.46488243341446
time_elpased: 1.978
batch start
#iterations: 121
currently lose_sum: 91.94539612531662
time_elpased: 1.971
batch start
#iterations: 122
currently lose_sum: 92.48380225896835
time_elpased: 1.923
batch start
#iterations: 123
currently lose_sum: 91.87668764591217
time_elpased: 1.99
batch start
#iterations: 124
currently lose_sum: 92.14165550470352
time_elpased: 1.926
batch start
#iterations: 125
currently lose_sum: 91.74802708625793
time_elpased: 1.931
batch start
#iterations: 126
currently lose_sum: 92.33993011713028
time_elpased: 1.924
batch start
#iterations: 127
currently lose_sum: 92.20518684387207
time_elpased: 1.97
batch start
#iterations: 128
currently lose_sum: 91.74128371477127
time_elpased: 1.929
batch start
#iterations: 129
currently lose_sum: 91.86523467302322
time_elpased: 1.96
batch start
#iterations: 130
currently lose_sum: 91.86481881141663
time_elpased: 1.931
batch start
#iterations: 131
currently lose_sum: 91.56686615943909
time_elpased: 1.939
batch start
#iterations: 132
currently lose_sum: 92.23980987071991
time_elpased: 1.941
batch start
#iterations: 133
currently lose_sum: 91.21308195590973
time_elpased: 1.961
batch start
#iterations: 134
currently lose_sum: 91.97353702783585
time_elpased: 1.92
batch start
#iterations: 135
currently lose_sum: 91.5176409482956
time_elpased: 1.949
batch start
#iterations: 136
currently lose_sum: 92.02070325613022
time_elpased: 2.008
batch start
#iterations: 137
currently lose_sum: 91.70439821481705
time_elpased: 1.983
batch start
#iterations: 138
currently lose_sum: 90.88076001405716
time_elpased: 1.909
batch start
#iterations: 139
currently lose_sum: 90.92528259754181
time_elpased: 1.959
start validation test
0.595824742268
0.615773882992
0.51343007101
0.559964083282
0.595969398822
64.273
batch start
#iterations: 140
currently lose_sum: 91.5207114815712
time_elpased: 1.948
batch start
#iterations: 141
currently lose_sum: 91.09199357032776
time_elpased: 1.921
batch start
#iterations: 142
currently lose_sum: 91.91202408075333
time_elpased: 1.951
batch start
#iterations: 143
currently lose_sum: 91.32124370336533
time_elpased: 1.986
batch start
#iterations: 144
currently lose_sum: 91.8194950222969
time_elpased: 1.92
batch start
#iterations: 145
currently lose_sum: 91.53971165418625
time_elpased: 1.978
batch start
#iterations: 146
currently lose_sum: 90.86596739292145
time_elpased: 1.923
batch start
#iterations: 147
currently lose_sum: 91.2997614145279
time_elpased: 1.93
batch start
#iterations: 148
currently lose_sum: 91.20700430870056
time_elpased: 1.956
batch start
#iterations: 149
currently lose_sum: 91.05027461051941
time_elpased: 1.968
batch start
#iterations: 150
currently lose_sum: 91.1560869216919
time_elpased: 1.914
batch start
#iterations: 151
currently lose_sum: 91.48079347610474
time_elpased: 1.989
batch start
#iterations: 152
currently lose_sum: 91.41894859075546
time_elpased: 1.914
batch start
#iterations: 153
currently lose_sum: 91.06578785181046
time_elpased: 1.993
batch start
#iterations: 154
currently lose_sum: 91.04952490329742
time_elpased: 1.963
batch start
#iterations: 155
currently lose_sum: 91.01558554172516
time_elpased: 1.945
batch start
#iterations: 156
currently lose_sum: 91.11536771059036
time_elpased: 1.944
batch start
#iterations: 157
currently lose_sum: 91.02012646198273
time_elpased: 1.97
batch start
#iterations: 158
currently lose_sum: 91.42859148979187
time_elpased: 1.928
batch start
#iterations: 159
currently lose_sum: 90.83041822910309
time_elpased: 1.969
start validation test
0.593659793814
0.620594423987
0.485643717197
0.544887708562
0.593849432697
64.972
batch start
#iterations: 160
currently lose_sum: 90.7399110198021
time_elpased: 1.981
batch start
#iterations: 161
currently lose_sum: 90.38673388957977
time_elpased: 1.935
batch start
#iterations: 162
currently lose_sum: 90.86283242702484
time_elpased: 1.967
batch start
#iterations: 163
currently lose_sum: 90.7276828289032
time_elpased: 1.953
batch start
#iterations: 164
currently lose_sum: 90.78254354000092
time_elpased: 1.975
batch start
#iterations: 165
currently lose_sum: 90.83822357654572
time_elpased: 1.949
batch start
#iterations: 166
currently lose_sum: 90.18552827835083
time_elpased: 1.946
batch start
#iterations: 167
currently lose_sum: 90.2074134349823
time_elpased: 1.973
batch start
#iterations: 168
currently lose_sum: 90.72570872306824
time_elpased: 1.934
batch start
#iterations: 169
currently lose_sum: 90.33956378698349
time_elpased: 1.943
batch start
#iterations: 170
currently lose_sum: 90.64483135938644
time_elpased: 1.964
batch start
#iterations: 171
currently lose_sum: 90.2103967666626
time_elpased: 1.975
batch start
#iterations: 172
currently lose_sum: 90.59309321641922
time_elpased: 1.947
batch start
#iterations: 173
currently lose_sum: 90.39316463470459
time_elpased: 1.939
batch start
#iterations: 174
currently lose_sum: 90.46642017364502
time_elpased: 1.957
batch start
#iterations: 175
currently lose_sum: 89.68005388975143
time_elpased: 1.94
batch start
#iterations: 176
currently lose_sum: 89.93200773000717
time_elpased: 1.949
batch start
#iterations: 177
currently lose_sum: 90.2917851805687
time_elpased: 1.963
batch start
#iterations: 178
currently lose_sum: 90.04661583900452
time_elpased: 1.956
batch start
#iterations: 179
currently lose_sum: 90.12179934978485
time_elpased: 1.952
start validation test
0.58618556701
0.613310076479
0.47041267881
0.532440302854
0.586388824172
65.749
batch start
#iterations: 180
currently lose_sum: 90.32633006572723
time_elpased: 1.94
batch start
#iterations: 181
currently lose_sum: 90.01791578531265
time_elpased: 1.922
batch start
#iterations: 182
currently lose_sum: 89.95029002428055
time_elpased: 1.916
batch start
#iterations: 183
currently lose_sum: 89.91688770055771
time_elpased: 1.967
batch start
#iterations: 184
currently lose_sum: 89.91984558105469
time_elpased: 1.962
batch start
#iterations: 185
currently lose_sum: 89.54550814628601
time_elpased: 1.962
batch start
#iterations: 186
currently lose_sum: 89.40479642152786
time_elpased: 1.938
batch start
#iterations: 187
currently lose_sum: 89.99339479207993
time_elpased: 1.935
batch start
#iterations: 188
currently lose_sum: 89.76832056045532
time_elpased: 1.949
batch start
#iterations: 189
currently lose_sum: 89.73496466875076
time_elpased: 1.965
batch start
#iterations: 190
currently lose_sum: 90.29158300161362
time_elpased: 1.917
batch start
#iterations: 191
currently lose_sum: 89.73760789632797
time_elpased: 1.928
batch start
#iterations: 192
currently lose_sum: 89.48313653469086
time_elpased: 1.97
batch start
#iterations: 193
currently lose_sum: 89.99058681726456
time_elpased: 1.953
batch start
#iterations: 194
currently lose_sum: 89.9100843667984
time_elpased: 1.987
batch start
#iterations: 195
currently lose_sum: 89.4664694070816
time_elpased: 1.982
batch start
#iterations: 196
currently lose_sum: 89.93746453523636
time_elpased: 1.964
batch start
#iterations: 197
currently lose_sum: 89.80365657806396
time_elpased: 1.984
batch start
#iterations: 198
currently lose_sum: 89.64978581666946
time_elpased: 1.966
batch start
#iterations: 199
currently lose_sum: 89.3238582611084
time_elpased: 1.926
start validation test
0.598505154639
0.620771736407
0.509931048678
0.559918639471
0.598660660144
65.196
batch start
#iterations: 200
currently lose_sum: 89.23394948244095
time_elpased: 1.946
batch start
#iterations: 201
currently lose_sum: 89.7788822054863
time_elpased: 1.926
batch start
#iterations: 202
currently lose_sum: 89.482137799263
time_elpased: 1.939
batch start
#iterations: 203
currently lose_sum: 88.8055489063263
time_elpased: 1.943
batch start
#iterations: 204
currently lose_sum: 89.10724526643753
time_elpased: 1.947
batch start
#iterations: 205
currently lose_sum: 89.66909098625183
time_elpased: 2.026
batch start
#iterations: 206
currently lose_sum: 89.72185409069061
time_elpased: 1.954
batch start
#iterations: 207
currently lose_sum: 89.0250294804573
time_elpased: 1.948
batch start
#iterations: 208
currently lose_sum: 88.94280564785004
time_elpased: 1.976
batch start
#iterations: 209
currently lose_sum: 89.18251103162766
time_elpased: 1.984
batch start
#iterations: 210
currently lose_sum: 88.88925677537918
time_elpased: 1.958
batch start
#iterations: 211
currently lose_sum: 88.86284297704697
time_elpased: 1.942
batch start
#iterations: 212
currently lose_sum: 89.27777171134949
time_elpased: 1.94
batch start
#iterations: 213
currently lose_sum: 89.03704172372818
time_elpased: 1.965
batch start
#iterations: 214
currently lose_sum: 88.84008234739304
time_elpased: 1.989
batch start
#iterations: 215
currently lose_sum: 88.86802566051483
time_elpased: 1.946
batch start
#iterations: 216
currently lose_sum: 88.68212759494781
time_elpased: 1.957
batch start
#iterations: 217
currently lose_sum: 88.78973656892776
time_elpased: 1.92
batch start
#iterations: 218
currently lose_sum: 89.25704818964005
time_elpased: 1.945
batch start
#iterations: 219
currently lose_sum: 89.21194702386856
time_elpased: 1.963
start validation test
0.594896907216
0.614663046161
0.512503859216
0.558953925585
0.595041560921
65.543
batch start
#iterations: 220
currently lose_sum: 88.32087260484695
time_elpased: 1.95
batch start
#iterations: 221
currently lose_sum: 89.14656466245651
time_elpased: 1.981
batch start
#iterations: 222
currently lose_sum: 88.85621786117554
time_elpased: 1.918
batch start
#iterations: 223
currently lose_sum: 88.45892143249512
time_elpased: 2.082
batch start
#iterations: 224
currently lose_sum: 88.58725982904434
time_elpased: 1.979
batch start
#iterations: 225
currently lose_sum: 88.6293825507164
time_elpased: 2.011
batch start
#iterations: 226
currently lose_sum: 88.37368077039719
time_elpased: 1.917
batch start
#iterations: 227
currently lose_sum: 88.62107598781586
time_elpased: 1.952
batch start
#iterations: 228
currently lose_sum: 88.36498427391052
time_elpased: 1.979
batch start
#iterations: 229
currently lose_sum: 88.48703134059906
time_elpased: 1.98
batch start
#iterations: 230
currently lose_sum: 88.90695667266846
time_elpased: 1.964
batch start
#iterations: 231
currently lose_sum: 88.35134434700012
time_elpased: 1.934
batch start
#iterations: 232
currently lose_sum: 88.44893461465836
time_elpased: 1.919
batch start
#iterations: 233
currently lose_sum: 88.49859547615051
time_elpased: 1.969
batch start
#iterations: 234
currently lose_sum: 89.09666723012924
time_elpased: 1.945
batch start
#iterations: 235
currently lose_sum: 88.00280314683914
time_elpased: 1.948
batch start
#iterations: 236
currently lose_sum: 88.72404688596725
time_elpased: 1.938
batch start
#iterations: 237
currently lose_sum: 88.21065038442612
time_elpased: 1.957
batch start
#iterations: 238
currently lose_sum: 88.91475838422775
time_elpased: 1.91
batch start
#iterations: 239
currently lose_sum: 88.2477810382843
time_elpased: 1.947
start validation test
0.591082474227
0.606443914081
0.523000926212
0.561640050837
0.591202001885
65.764
batch start
#iterations: 240
currently lose_sum: 88.31864845752716
time_elpased: 1.918
batch start
#iterations: 241
currently lose_sum: 88.49875116348267
time_elpased: 1.972
batch start
#iterations: 242
currently lose_sum: 87.99828290939331
time_elpased: 1.945
batch start
#iterations: 243
currently lose_sum: 89.21923518180847
time_elpased: 1.963
batch start
#iterations: 244
currently lose_sum: 88.72406023740768
time_elpased: 1.959
batch start
#iterations: 245
currently lose_sum: 88.07047522068024
time_elpased: 1.949
batch start
#iterations: 246
currently lose_sum: 87.68148028850555
time_elpased: 1.907
batch start
#iterations: 247
currently lose_sum: 88.11269932985306
time_elpased: 1.941
batch start
#iterations: 248
currently lose_sum: 87.89912569522858
time_elpased: 1.904
batch start
#iterations: 249
currently lose_sum: 88.17134773731232
time_elpased: 1.945
batch start
#iterations: 250
currently lose_sum: 88.38246750831604
time_elpased: 1.92
batch start
#iterations: 251
currently lose_sum: 88.18973153829575
time_elpased: 1.95
batch start
#iterations: 252
currently lose_sum: 87.93920063972473
time_elpased: 1.968
batch start
#iterations: 253
currently lose_sum: 88.44591253995895
time_elpased: 1.95
batch start
#iterations: 254
currently lose_sum: 88.04033941030502
time_elpased: 1.894
batch start
#iterations: 255
currently lose_sum: 87.79386168718338
time_elpased: 1.981
batch start
#iterations: 256
currently lose_sum: 87.63739985227585
time_elpased: 1.894
batch start
#iterations: 257
currently lose_sum: 87.72507774829865
time_elpased: 1.982
batch start
#iterations: 258
currently lose_sum: 87.54111295938492
time_elpased: 1.904
batch start
#iterations: 259
currently lose_sum: 87.77205121517181
time_elpased: 1.995
start validation test
0.579278350515
0.611309949893
0.43943603993
0.511316010059
0.579523865261
67.326
batch start
#iterations: 260
currently lose_sum: 87.55144107341766
time_elpased: 1.931
batch start
#iterations: 261
currently lose_sum: 87.81211155653
time_elpased: 1.966
batch start
#iterations: 262
currently lose_sum: 87.43744587898254
time_elpased: 1.933
batch start
#iterations: 263
currently lose_sum: 87.56096816062927
time_elpased: 1.976
batch start
#iterations: 264
currently lose_sum: 88.09238022565842
time_elpased: 1.926
batch start
#iterations: 265
currently lose_sum: 87.7457018494606
time_elpased: 1.937
batch start
#iterations: 266
currently lose_sum: 87.78210836648941
time_elpased: 1.96
batch start
#iterations: 267
currently lose_sum: 87.75490963459015
time_elpased: 1.982
batch start
#iterations: 268
currently lose_sum: 87.50561827421188
time_elpased: 1.936
batch start
#iterations: 269
currently lose_sum: 87.51337027549744
time_elpased: 1.968
batch start
#iterations: 270
currently lose_sum: 87.59597033262253
time_elpased: 1.913
batch start
#iterations: 271
currently lose_sum: 87.52588284015656
time_elpased: 1.966
batch start
#iterations: 272
currently lose_sum: 87.68518048524857
time_elpased: 1.976
batch start
#iterations: 273
currently lose_sum: 87.14652997255325
time_elpased: 1.978
batch start
#iterations: 274
currently lose_sum: 87.58553677797318
time_elpased: 1.894
batch start
#iterations: 275
currently lose_sum: 87.39838248491287
time_elpased: 1.954
batch start
#iterations: 276
currently lose_sum: 87.52750939130783
time_elpased: 1.939
batch start
#iterations: 277
currently lose_sum: 87.57071214914322
time_elpased: 1.955
batch start
#iterations: 278
currently lose_sum: 87.40508514642715
time_elpased: 1.908
batch start
#iterations: 279
currently lose_sum: 86.90965175628662
time_elpased: 1.936
start validation test
0.593350515464
0.609882183217
0.522074714418
0.562572775159
0.593475651126
66.363
batch start
#iterations: 280
currently lose_sum: 87.63451039791107
time_elpased: 1.913
batch start
#iterations: 281
currently lose_sum: 87.07658964395523
time_elpased: 1.975
batch start
#iterations: 282
currently lose_sum: 87.26652789115906
time_elpased: 1.91
batch start
#iterations: 283
currently lose_sum: 87.2661183476448
time_elpased: 1.961
batch start
#iterations: 284
currently lose_sum: 86.64785867929459
time_elpased: 1.928
batch start
#iterations: 285
currently lose_sum: 87.36788457632065
time_elpased: 1.943
batch start
#iterations: 286
currently lose_sum: 87.10528403520584
time_elpased: 1.968
batch start
#iterations: 287
currently lose_sum: 86.64961630105972
time_elpased: 2.001
batch start
#iterations: 288
currently lose_sum: 87.20507603883743
time_elpased: 1.916
batch start
#iterations: 289
currently lose_sum: 87.34434342384338
time_elpased: 1.944
batch start
#iterations: 290
currently lose_sum: 87.29400503635406
time_elpased: 2.049
batch start
#iterations: 291
currently lose_sum: 87.2730221748352
time_elpased: 1.977
batch start
#iterations: 292
currently lose_sum: 86.91732025146484
time_elpased: 1.943
batch start
#iterations: 293
currently lose_sum: 86.71782672405243
time_elpased: 1.925
batch start
#iterations: 294
currently lose_sum: 86.71479451656342
time_elpased: 1.919
batch start
#iterations: 295
currently lose_sum: 86.9992733001709
time_elpased: 1.926
batch start
#iterations: 296
currently lose_sum: 86.92724704742432
time_elpased: 1.905
batch start
#iterations: 297
currently lose_sum: 86.37844026088715
time_elpased: 1.968
batch start
#iterations: 298
currently lose_sum: 86.87406837940216
time_elpased: 1.92
batch start
#iterations: 299
currently lose_sum: 86.62471216917038
time_elpased: 1.919
start validation test
0.591958762887
0.608559373116
0.51950190388
0.560515212081
0.592085972078
66.476
batch start
#iterations: 300
currently lose_sum: 86.7893123626709
time_elpased: 1.968
batch start
#iterations: 301
currently lose_sum: 86.68590480089188
time_elpased: 1.933
batch start
#iterations: 302
currently lose_sum: 86.96304494142532
time_elpased: 1.955
batch start
#iterations: 303
currently lose_sum: 86.64391946792603
time_elpased: 1.933
batch start
#iterations: 304
currently lose_sum: 86.6615024805069
time_elpased: 1.946
batch start
#iterations: 305
currently lose_sum: 86.26712256669998
time_elpased: 1.981
batch start
#iterations: 306
currently lose_sum: 87.34420222043991
time_elpased: 1.923
batch start
#iterations: 307
currently lose_sum: 86.93491446971893
time_elpased: 1.975
batch start
#iterations: 308
currently lose_sum: 86.58120214939117
time_elpased: 1.944
batch start
#iterations: 309
currently lose_sum: 86.47729510068893
time_elpased: 1.93
batch start
#iterations: 310
currently lose_sum: 86.93076747655869
time_elpased: 1.952
batch start
#iterations: 311
currently lose_sum: 86.16477739810944
time_elpased: 1.963
batch start
#iterations: 312
currently lose_sum: 85.77338743209839
time_elpased: 1.954
batch start
#iterations: 313
currently lose_sum: 86.54185891151428
time_elpased: 1.915
batch start
#iterations: 314
currently lose_sum: 85.91964656114578
time_elpased: 1.991
batch start
#iterations: 315
currently lose_sum: 86.71047568321228
time_elpased: 1.934
batch start
#iterations: 316
currently lose_sum: 86.18395751714706
time_elpased: 1.939
batch start
#iterations: 317
currently lose_sum: 86.41972607374191
time_elpased: 1.915
batch start
#iterations: 318
currently lose_sum: 86.21603798866272
time_elpased: 1.944
batch start
#iterations: 319
currently lose_sum: 86.52704066038132
time_elpased: 1.913
start validation test
0.580567010309
0.610706278027
0.448492333025
0.517177950513
0.580798887776
67.679
batch start
#iterations: 320
currently lose_sum: 86.56541872024536
time_elpased: 1.96
batch start
#iterations: 321
currently lose_sum: 86.54252445697784
time_elpased: 1.953
batch start
#iterations: 322
currently lose_sum: 87.06548827886581
time_elpased: 1.927
batch start
#iterations: 323
currently lose_sum: 86.74034512042999
time_elpased: 1.916
batch start
#iterations: 324
currently lose_sum: 86.71815890073776
time_elpased: 1.95
batch start
#iterations: 325
currently lose_sum: 86.46219390630722
time_elpased: 1.925
batch start
#iterations: 326
currently lose_sum: 86.57997626066208
time_elpased: 1.938
batch start
#iterations: 327
currently lose_sum: 85.87732154130936
time_elpased: 1.96
batch start
#iterations: 328
currently lose_sum: 85.91820067167282
time_elpased: 1.955
batch start
#iterations: 329
currently lose_sum: 86.19399726390839
time_elpased: 1.931
batch start
#iterations: 330
currently lose_sum: 85.74883508682251
time_elpased: 1.978
batch start
#iterations: 331
currently lose_sum: 86.10174942016602
time_elpased: 1.944
batch start
#iterations: 332
currently lose_sum: 86.09091758728027
time_elpased: 1.954
batch start
#iterations: 333
currently lose_sum: 85.56109717488289
time_elpased: 1.906
batch start
#iterations: 334
currently lose_sum: 85.82981562614441
time_elpased: 1.963
batch start
#iterations: 335
currently lose_sum: 85.1827751994133
time_elpased: 1.991
batch start
#iterations: 336
currently lose_sum: 86.35038536787033
time_elpased: 1.944
batch start
#iterations: 337
currently lose_sum: 85.90944248437881
time_elpased: 1.918
batch start
#iterations: 338
currently lose_sum: 86.11221754550934
time_elpased: 1.944
batch start
#iterations: 339
currently lose_sum: 86.04557067155838
time_elpased: 1.914
start validation test
0.59587628866
0.602804250192
0.566327055676
0.583996604054
0.595928166896
66.336
batch start
#iterations: 340
currently lose_sum: 85.85637265443802
time_elpased: 1.916
batch start
#iterations: 341
currently lose_sum: 85.89741140604019
time_elpased: 1.923
batch start
#iterations: 342
currently lose_sum: 86.05859822034836
time_elpased: 1.954
batch start
#iterations: 343
currently lose_sum: 86.03415286540985
time_elpased: 1.941
batch start
#iterations: 344
currently lose_sum: 86.10495620965958
time_elpased: 1.962
batch start
#iterations: 345
currently lose_sum: 85.83023715019226
time_elpased: 1.913
batch start
#iterations: 346
currently lose_sum: 86.15061855316162
time_elpased: 1.933
batch start
#iterations: 347
currently lose_sum: 85.49369245767593
time_elpased: 1.986
batch start
#iterations: 348
currently lose_sum: 85.7888258099556
time_elpased: 1.977
batch start
#iterations: 349
currently lose_sum: 86.09636390209198
time_elpased: 1.941
batch start
#iterations: 350
currently lose_sum: 85.62706929445267
time_elpased: 1.962
batch start
#iterations: 351
currently lose_sum: 85.39215850830078
time_elpased: 1.917
batch start
#iterations: 352
currently lose_sum: 85.86718213558197
time_elpased: 1.956
batch start
#iterations: 353
currently lose_sum: 86.52557903528214
time_elpased: 1.947
batch start
#iterations: 354
currently lose_sum: 85.92913430929184
time_elpased: 1.924
batch start
#iterations: 355
currently lose_sum: 86.05447936058044
time_elpased: 1.91
batch start
#iterations: 356
currently lose_sum: 85.38137710094452
time_elpased: 1.953
batch start
#iterations: 357
currently lose_sum: 85.79373377561569
time_elpased: 1.976
batch start
#iterations: 358
currently lose_sum: 86.33231562376022
time_elpased: 1.944
batch start
#iterations: 359
currently lose_sum: 84.75551694631577
time_elpased: 1.966
start validation test
0.587113402062
0.615011453982
0.46969229186
0.53261757498
0.587319552932
67.713
batch start
#iterations: 360
currently lose_sum: 85.3979189991951
time_elpased: 1.93
batch start
#iterations: 361
currently lose_sum: 85.87416249513626
time_elpased: 1.928
batch start
#iterations: 362
currently lose_sum: 84.99946308135986
time_elpased: 1.941
batch start
#iterations: 363
currently lose_sum: 85.29140996932983
time_elpased: 1.941
batch start
#iterations: 364
currently lose_sum: 85.8477366566658
time_elpased: 1.987
batch start
#iterations: 365
currently lose_sum: 85.51249504089355
time_elpased: 1.944
batch start
#iterations: 366
currently lose_sum: 85.75784701108932
time_elpased: 1.936
batch start
#iterations: 367
currently lose_sum: 84.98870658874512
time_elpased: 1.951
batch start
#iterations: 368
currently lose_sum: 85.64181661605835
time_elpased: 2.007
batch start
#iterations: 369
currently lose_sum: 85.30829352140427
time_elpased: 1.986
batch start
#iterations: 370
currently lose_sum: 84.94530928134918
time_elpased: 1.944
batch start
#iterations: 371
currently lose_sum: 84.93601888418198
time_elpased: 1.983
batch start
#iterations: 372
currently lose_sum: 85.45131522417068
time_elpased: 1.934
batch start
#iterations: 373
currently lose_sum: 85.21329694986343
time_elpased: 1.948
batch start
#iterations: 374
currently lose_sum: 84.87530353665352
time_elpased: 1.928
batch start
#iterations: 375
currently lose_sum: 84.37756550312042
time_elpased: 1.965
batch start
#iterations: 376
currently lose_sum: 85.00641870498657
time_elpased: 1.958
batch start
#iterations: 377
currently lose_sum: 85.23447644710541
time_elpased: 1.952
batch start
#iterations: 378
currently lose_sum: 85.02342915534973
time_elpased: 1.895
batch start
#iterations: 379
currently lose_sum: 85.36122828722
time_elpased: 1.981
start validation test
0.58912371134
0.601135310473
0.534012555315
0.565589405417
0.589220467475
67.344
batch start
#iterations: 380
currently lose_sum: 85.12934958934784
time_elpased: 1.939
batch start
#iterations: 381
currently lose_sum: 85.21273535490036
time_elpased: 1.944
batch start
#iterations: 382
currently lose_sum: 85.27590125799179
time_elpased: 1.921
batch start
#iterations: 383
currently lose_sum: 85.16454511880875
time_elpased: 1.948
batch start
#iterations: 384
currently lose_sum: 85.35332441329956
time_elpased: 1.971
batch start
#iterations: 385
currently lose_sum: 85.14274621009827
time_elpased: 1.95
batch start
#iterations: 386
currently lose_sum: 84.71259027719498
time_elpased: 1.919
batch start
#iterations: 387
currently lose_sum: 85.17068088054657
time_elpased: 1.944
batch start
#iterations: 388
currently lose_sum: 84.87735193967819
time_elpased: 1.962
batch start
#iterations: 389
currently lose_sum: 84.91581869125366
time_elpased: 1.982
batch start
#iterations: 390
currently lose_sum: 85.11868923902512
time_elpased: 1.839
batch start
#iterations: 391
currently lose_sum: 84.22319632768631
time_elpased: 1.882
batch start
#iterations: 392
currently lose_sum: 84.44516324996948
time_elpased: 1.845
batch start
#iterations: 393
currently lose_sum: 84.94831746816635
time_elpased: 1.841
batch start
#iterations: 394
currently lose_sum: 85.22817587852478
time_elpased: 1.85
batch start
#iterations: 395
currently lose_sum: 85.27320730686188
time_elpased: 1.865
batch start
#iterations: 396
currently lose_sum: 84.51787877082825
time_elpased: 1.838
batch start
#iterations: 397
currently lose_sum: 84.52229166030884
time_elpased: 1.846
batch start
#iterations: 398
currently lose_sum: 85.1150414943695
time_elpased: 1.834
batch start
#iterations: 399
currently lose_sum: 84.44201064109802
time_elpased: 1.846
start validation test
0.582422680412
0.607304116866
0.470618503653
0.53029512379
0.582618969889
68.351
acc: 0.621
pre: 0.613
rec: 0.658
F1: 0.635
auc: 0.621
