start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.32587724924088
time_elpased: 2.779
batch start
#iterations: 1
currently lose_sum: 100.19167309999466
time_elpased: 2.788
batch start
#iterations: 2
currently lose_sum: 99.99621707201004
time_elpased: 2.866
batch start
#iterations: 3
currently lose_sum: 99.73792958259583
time_elpased: 2.947
batch start
#iterations: 4
currently lose_sum: 99.59594982862473
time_elpased: 2.862
batch start
#iterations: 5
currently lose_sum: 99.50820368528366
time_elpased: 2.864
batch start
#iterations: 6
currently lose_sum: 99.04355257749557
time_elpased: 2.865
batch start
#iterations: 7
currently lose_sum: 99.19355189800262
time_elpased: 2.64
batch start
#iterations: 8
currently lose_sum: 98.51719558238983
time_elpased: 2.694
batch start
#iterations: 9
currently lose_sum: 98.45850396156311
time_elpased: 2.64
batch start
#iterations: 10
currently lose_sum: 98.28230953216553
time_elpased: 2.715
batch start
#iterations: 11
currently lose_sum: 98.51136511564255
time_elpased: 2.579
batch start
#iterations: 12
currently lose_sum: 98.32792329788208
time_elpased: 2.727
batch start
#iterations: 13
currently lose_sum: 98.01065188646317
time_elpased: 2.551
batch start
#iterations: 14
currently lose_sum: 97.96545433998108
time_elpased: 2.73
batch start
#iterations: 15
currently lose_sum: 97.53461235761642
time_elpased: 2.758
batch start
#iterations: 16
currently lose_sum: 97.30008608102798
time_elpased: 2.651
batch start
#iterations: 17
currently lose_sum: 97.43944573402405
time_elpased: 2.642
batch start
#iterations: 18
currently lose_sum: 97.39049661159515
time_elpased: 2.701
batch start
#iterations: 19
currently lose_sum: 97.05716866254807
time_elpased: 2.694
start validation test
0.624329896907
0.590183411302
0.817947926315
0.685645272602
0.623989970593
63.053
batch start
#iterations: 20
currently lose_sum: 96.96705323457718
time_elpased: 2.73
batch start
#iterations: 21
currently lose_sum: 96.78077334165573
time_elpased: 2.726
batch start
#iterations: 22
currently lose_sum: 97.07149612903595
time_elpased: 2.745
batch start
#iterations: 23
currently lose_sum: 96.44635254144669
time_elpased: 2.775
batch start
#iterations: 24
currently lose_sum: 96.36887121200562
time_elpased: 2.955
batch start
#iterations: 25
currently lose_sum: 96.63780134916306
time_elpased: 2.801
batch start
#iterations: 26
currently lose_sum: 96.16956567764282
time_elpased: 2.646
batch start
#iterations: 27
currently lose_sum: 96.5019713640213
time_elpased: 2.564
batch start
#iterations: 28
currently lose_sum: 96.37457865476608
time_elpased: 2.741
batch start
#iterations: 29
currently lose_sum: 96.38323259353638
time_elpased: 2.777
batch start
#iterations: 30
currently lose_sum: 95.92894101142883
time_elpased: 2.778
batch start
#iterations: 31
currently lose_sum: 95.85634207725525
time_elpased: 2.719
batch start
#iterations: 32
currently lose_sum: 95.56259405612946
time_elpased: 2.714
batch start
#iterations: 33
currently lose_sum: 95.78819757699966
time_elpased: 2.685
batch start
#iterations: 34
currently lose_sum: 95.24345260858536
time_elpased: 2.721
batch start
#iterations: 35
currently lose_sum: 95.94021940231323
time_elpased: 2.753
batch start
#iterations: 36
currently lose_sum: 95.0762591958046
time_elpased: 2.747
batch start
#iterations: 37
currently lose_sum: 95.54497849941254
time_elpased: 2.709
batch start
#iterations: 38
currently lose_sum: 95.17630648612976
time_elpased: 2.734
batch start
#iterations: 39
currently lose_sum: 95.33222842216492
time_elpased: 2.741
start validation test
0.622731958763
0.596413637826
0.763301430483
0.669615853383
0.622485167374
62.729
batch start
#iterations: 40
currently lose_sum: 94.99243241548538
time_elpased: 2.713
batch start
#iterations: 41
currently lose_sum: 95.30201530456543
time_elpased: 2.709
batch start
#iterations: 42
currently lose_sum: 95.10629463195801
time_elpased: 2.726
batch start
#iterations: 43
currently lose_sum: 94.57362657785416
time_elpased: 2.781
batch start
#iterations: 44
currently lose_sum: 94.89086604118347
time_elpased: 2.786
batch start
#iterations: 45
currently lose_sum: 95.09626883268356
time_elpased: 2.765
batch start
#iterations: 46
currently lose_sum: 94.40851813554764
time_elpased: 2.741
batch start
#iterations: 47
currently lose_sum: 94.4859870672226
time_elpased: 2.773
batch start
#iterations: 48
currently lose_sum: 94.24350506067276
time_elpased: 2.725
batch start
#iterations: 49
currently lose_sum: 94.2908627986908
time_elpased: 2.816
batch start
#iterations: 50
currently lose_sum: 94.41021186113358
time_elpased: 2.734
batch start
#iterations: 51
currently lose_sum: 94.30604708194733
time_elpased: 2.606
batch start
#iterations: 52
currently lose_sum: 94.23316079378128
time_elpased: 2.75
batch start
#iterations: 53
currently lose_sum: 94.15386760234833
time_elpased: 2.789
batch start
#iterations: 54
currently lose_sum: 94.00690007209778
time_elpased: 2.781
batch start
#iterations: 55
currently lose_sum: 93.97171503305435
time_elpased: 2.741
batch start
#iterations: 56
currently lose_sum: 93.72509491443634
time_elpased: 2.704
batch start
#iterations: 57
currently lose_sum: 93.82522666454315
time_elpased: 2.747
batch start
#iterations: 58
currently lose_sum: 93.48767447471619
time_elpased: 2.738
batch start
#iterations: 59
currently lose_sum: 93.9238430261612
time_elpased: 2.749
start validation test
0.64175257732
0.621647762244
0.727590820212
0.670459933618
0.641601875045
61.694
batch start
#iterations: 60
currently lose_sum: 93.9729922413826
time_elpased: 2.682
batch start
#iterations: 61
currently lose_sum: 93.08529514074326
time_elpased: 2.675
batch start
#iterations: 62
currently lose_sum: 93.83150225877762
time_elpased: 2.717
batch start
#iterations: 63
currently lose_sum: 93.63587194681168
time_elpased: 2.73
batch start
#iterations: 64
currently lose_sum: 93.07598954439163
time_elpased: 2.593
batch start
#iterations: 65
currently lose_sum: 92.86931282281876
time_elpased: 2.577
batch start
#iterations: 66
currently lose_sum: 92.83834451436996
time_elpased: 2.698
batch start
#iterations: 67
currently lose_sum: 93.04208624362946
time_elpased: 2.694
batch start
#iterations: 68
currently lose_sum: 92.6897941827774
time_elpased: 2.741
batch start
#iterations: 69
currently lose_sum: 92.91976696252823
time_elpased: 2.779
batch start
#iterations: 70
currently lose_sum: 92.25492119789124
time_elpased: 2.731
batch start
#iterations: 71
currently lose_sum: 92.7740290760994
time_elpased: 2.749
batch start
#iterations: 72
currently lose_sum: 92.68534398078918
time_elpased: 2.727
batch start
#iterations: 73
currently lose_sum: 92.79781436920166
time_elpased: 2.764
batch start
#iterations: 74
currently lose_sum: 92.44960474967957
time_elpased: 2.747
batch start
#iterations: 75
currently lose_sum: 92.16334986686707
time_elpased: 2.691
batch start
#iterations: 76
currently lose_sum: 92.77839052677155
time_elpased: 2.721
batch start
#iterations: 77
currently lose_sum: 92.31929898262024
time_elpased: 2.784
batch start
#iterations: 78
currently lose_sum: 92.46887904405594
time_elpased: 2.765
batch start
#iterations: 79
currently lose_sum: 92.21004551649094
time_elpased: 2.767
start validation test
0.630360824742
0.635425531915
0.614695893794
0.624888842392
0.630388326945
62.698
batch start
#iterations: 80
currently lose_sum: 92.68651384115219
time_elpased: 2.717
batch start
#iterations: 81
currently lose_sum: 92.2325878739357
time_elpased: 2.756
batch start
#iterations: 82
currently lose_sum: 92.10955637693405
time_elpased: 2.75
batch start
#iterations: 83
currently lose_sum: 91.82227295637131
time_elpased: 2.775
batch start
#iterations: 84
currently lose_sum: 92.23422509431839
time_elpased: 2.735
batch start
#iterations: 85
currently lose_sum: 91.78534823656082
time_elpased: 2.747
batch start
#iterations: 86
currently lose_sum: 91.64288127422333
time_elpased: 2.706
batch start
#iterations: 87
currently lose_sum: 91.94959729909897
time_elpased: 2.78
batch start
#iterations: 88
currently lose_sum: 91.93994587659836
time_elpased: 2.73
batch start
#iterations: 89
currently lose_sum: 91.38578766584396
time_elpased: 2.773
batch start
#iterations: 90
currently lose_sum: 91.71209961175919
time_elpased: 2.76
batch start
#iterations: 91
currently lose_sum: 91.02191251516342
time_elpased: 2.764
batch start
#iterations: 92
currently lose_sum: 91.45565557479858
time_elpased: 2.686
batch start
#iterations: 93
currently lose_sum: 91.27783274650574
time_elpased: 2.761
batch start
#iterations: 94
currently lose_sum: 91.17198067903519
time_elpased: 2.74
batch start
#iterations: 95
currently lose_sum: 90.97620153427124
time_elpased: 2.79
batch start
#iterations: 96
currently lose_sum: 91.23579347133636
time_elpased: 2.744
batch start
#iterations: 97
currently lose_sum: 90.39034348726273
time_elpased: 2.807
batch start
#iterations: 98
currently lose_sum: 91.37488406896591
time_elpased: 2.764
batch start
#iterations: 99
currently lose_sum: 91.01754885911942
time_elpased: 2.759
start validation test
0.63618556701
0.643745269759
0.612740557785
0.627860381736
0.63622672834
62.467
batch start
#iterations: 100
currently lose_sum: 91.22777611017227
time_elpased: 2.74
batch start
#iterations: 101
currently lose_sum: 91.20384454727173
time_elpased: 2.771
batch start
#iterations: 102
currently lose_sum: 90.50083243846893
time_elpased: 2.746
batch start
#iterations: 103
currently lose_sum: 90.81763607263565
time_elpased: 2.765
batch start
#iterations: 104
currently lose_sum: 90.6557702422142
time_elpased: 2.71
batch start
#iterations: 105
currently lose_sum: 89.82100147008896
time_elpased: 2.708
batch start
#iterations: 106
currently lose_sum: 90.48227119445801
time_elpased: 2.735
batch start
#iterations: 107
currently lose_sum: 90.3064586520195
time_elpased: 2.741
batch start
#iterations: 108
currently lose_sum: 90.20211350917816
time_elpased: 2.714
batch start
#iterations: 109
currently lose_sum: 90.02496325969696
time_elpased: 2.71
batch start
#iterations: 110
currently lose_sum: 90.44017606973648
time_elpased: 2.702
batch start
#iterations: 111
currently lose_sum: 89.76267170906067
time_elpased: 2.724
batch start
#iterations: 112
currently lose_sum: 90.20487886667252
time_elpased: 2.707
batch start
#iterations: 113
currently lose_sum: 89.45852679014206
time_elpased: 2.692
batch start
#iterations: 114
currently lose_sum: 89.99816083908081
time_elpased: 2.725
batch start
#iterations: 115
currently lose_sum: 90.02558171749115
time_elpased: 2.72
batch start
#iterations: 116
currently lose_sum: 89.24700576066971
time_elpased: 2.767
batch start
#iterations: 117
currently lose_sum: 89.23363536596298
time_elpased: 2.786
batch start
#iterations: 118
currently lose_sum: 88.69058829545975
time_elpased: 2.746
batch start
#iterations: 119
currently lose_sum: 88.30600786209106
time_elpased: 2.742
start validation test
0.605257731959
0.636339557674
0.494494185448
0.55652073199
0.605452194449
64.786
batch start
#iterations: 120
currently lose_sum: 88.9512586593628
time_elpased: 2.77
batch start
#iterations: 121
currently lose_sum: 89.23014670610428
time_elpased: 2.75
batch start
#iterations: 122
currently lose_sum: 88.99033391475677
time_elpased: 2.663
batch start
#iterations: 123
currently lose_sum: 89.50300735235214
time_elpased: 2.739
batch start
#iterations: 124
currently lose_sum: 89.39000952243805
time_elpased: 2.738
batch start
#iterations: 125
currently lose_sum: 88.6891776919365
time_elpased: 2.776
batch start
#iterations: 126
currently lose_sum: 89.37943971157074
time_elpased: 2.733
batch start
#iterations: 127
currently lose_sum: 88.5289699435234
time_elpased: 2.722
batch start
#iterations: 128
currently lose_sum: 89.05047684907913
time_elpased: 2.726
batch start
#iterations: 129
currently lose_sum: 89.25501042604446
time_elpased: 2.701
batch start
#iterations: 130
currently lose_sum: 88.08396857976913
time_elpased: 2.724
batch start
#iterations: 131
currently lose_sum: 88.7775866985321
time_elpased: 2.726
batch start
#iterations: 132
currently lose_sum: 88.10110765695572
time_elpased: 2.714
batch start
#iterations: 133
currently lose_sum: 88.43865329027176
time_elpased: 2.734
batch start
#iterations: 134
currently lose_sum: 88.39292979240417
time_elpased: 2.73
batch start
#iterations: 135
currently lose_sum: 87.8462592959404
time_elpased: 2.731
batch start
#iterations: 136
currently lose_sum: 88.27953106164932
time_elpased: 2.728
batch start
#iterations: 137
currently lose_sum: 87.9879178404808
time_elpased: 2.747
batch start
#iterations: 138
currently lose_sum: 87.8935381770134
time_elpased: 2.742
batch start
#iterations: 139
currently lose_sum: 87.61071997880936
time_elpased: 2.752
start validation test
0.612216494845
0.632009626955
0.540496037872
0.582681533256
0.61234241117
65.745
batch start
#iterations: 140
currently lose_sum: 87.52492254972458
time_elpased: 2.704
batch start
#iterations: 141
currently lose_sum: 87.60555028915405
time_elpased: 2.711
batch start
#iterations: 142
currently lose_sum: 87.52967178821564
time_elpased: 2.717
batch start
#iterations: 143
currently lose_sum: 87.48538064956665
time_elpased: 2.802
batch start
#iterations: 144
currently lose_sum: 87.86423242092133
time_elpased: 2.77
batch start
#iterations: 145
currently lose_sum: 87.2444241642952
time_elpased: 2.748
batch start
#iterations: 146
currently lose_sum: 87.75817543268204
time_elpased: 2.777
batch start
#iterations: 147
currently lose_sum: 87.43862146139145
time_elpased: 2.734
batch start
#iterations: 148
currently lose_sum: 87.53043180704117
time_elpased: 2.639
batch start
#iterations: 149
currently lose_sum: 87.18062353134155
time_elpased: 2.726
batch start
#iterations: 150
currently lose_sum: 87.25313425064087
time_elpased: 2.731
batch start
#iterations: 151
currently lose_sum: 86.84145891666412
time_elpased: 2.779
batch start
#iterations: 152
currently lose_sum: 87.51189810037613
time_elpased: 2.751
batch start
#iterations: 153
currently lose_sum: 86.09940421581268
time_elpased: 2.751
batch start
#iterations: 154
currently lose_sum: 86.2373577952385
time_elpased: 2.745
batch start
#iterations: 155
currently lose_sum: 86.40774303674698
time_elpased: 2.715
batch start
#iterations: 156
currently lose_sum: 86.98487639427185
time_elpased: 2.728
batch start
#iterations: 157
currently lose_sum: 85.78631436824799
time_elpased: 2.694
batch start
#iterations: 158
currently lose_sum: 86.49568086862564
time_elpased: 2.715
batch start
#iterations: 159
currently lose_sum: 86.31326508522034
time_elpased: 2.769
start validation test
0.603453608247
0.597382602002
0.638880312854
0.617434979362
0.603391411203
67.190
batch start
#iterations: 160
currently lose_sum: 86.42470502853394
time_elpased: 2.737
batch start
#iterations: 161
currently lose_sum: 86.14612150192261
time_elpased: 2.726
batch start
#iterations: 162
currently lose_sum: 86.82067227363586
time_elpased: 2.664
batch start
#iterations: 163
currently lose_sum: 86.37900775671005
time_elpased: 2.691
batch start
#iterations: 164
currently lose_sum: 85.41569256782532
time_elpased: 2.739
batch start
#iterations: 165
currently lose_sum: 85.46177357435226
time_elpased: 2.783
batch start
#iterations: 166
currently lose_sum: 85.85882753133774
time_elpased: 2.742
batch start
#iterations: 167
currently lose_sum: 85.28823179006577
time_elpased: 2.765
batch start
#iterations: 168
currently lose_sum: 86.04987561702728
time_elpased: 2.73
batch start
#iterations: 169
currently lose_sum: 85.37504583597183
time_elpased: 2.739
batch start
#iterations: 170
currently lose_sum: 85.65021377801895
time_elpased: 2.705
batch start
#iterations: 171
currently lose_sum: 85.1302580833435
time_elpased: 2.694
batch start
#iterations: 172
currently lose_sum: 85.25120007991791
time_elpased: 2.74
batch start
#iterations: 173
currently lose_sum: 85.13202422857285
time_elpased: 2.764
batch start
#iterations: 174
currently lose_sum: 84.76771938800812
time_elpased: 2.903
batch start
#iterations: 175
currently lose_sum: 84.57466006278992
time_elpased: 2.651
batch start
#iterations: 176
currently lose_sum: 85.15406107902527
time_elpased: 2.737
batch start
#iterations: 177
currently lose_sum: 83.70812970399857
time_elpased: 2.721
batch start
#iterations: 178
currently lose_sum: 84.85136473178864
time_elpased: 2.565
batch start
#iterations: 179
currently lose_sum: 84.27198559045792
time_elpased: 2.658
start validation test
0.610103092784
0.640995415848
0.503653390964
0.56408483172
0.610289981654
72.126
batch start
#iterations: 180
currently lose_sum: 85.06640952825546
time_elpased: 2.716
batch start
#iterations: 181
currently lose_sum: 84.12060821056366
time_elpased: 2.743
batch start
#iterations: 182
currently lose_sum: 83.7609274983406
time_elpased: 2.748
batch start
#iterations: 183
currently lose_sum: 83.88870078325272
time_elpased: 2.718
batch start
#iterations: 184
currently lose_sum: 84.35352170467377
time_elpased: 2.712
batch start
#iterations: 185
currently lose_sum: 83.54407048225403
time_elpased: 2.723
batch start
#iterations: 186
currently lose_sum: 84.07442092895508
time_elpased: 2.729
batch start
#iterations: 187
currently lose_sum: 83.73868837952614
time_elpased: 2.767
batch start
#iterations: 188
currently lose_sum: 83.68091291189194
time_elpased: 2.766
batch start
#iterations: 189
currently lose_sum: 83.6107029914856
time_elpased: 2.753
batch start
#iterations: 190
currently lose_sum: 83.45287302136421
time_elpased: 2.658
batch start
#iterations: 191
currently lose_sum: 82.71977251768112
time_elpased: 2.743
batch start
#iterations: 192
currently lose_sum: 84.02563974261284
time_elpased: 2.732
batch start
#iterations: 193
currently lose_sum: 83.14847910404205
time_elpased: 2.772
batch start
#iterations: 194
currently lose_sum: 82.9456482231617
time_elpased: 2.768
batch start
#iterations: 195
currently lose_sum: 83.32639849185944
time_elpased: 2.73
batch start
#iterations: 196
currently lose_sum: 83.12336933612823
time_elpased: 2.736
batch start
#iterations: 197
currently lose_sum: 82.98554134368896
time_elpased: 2.767
batch start
#iterations: 198
currently lose_sum: 82.24646404385567
time_elpased: 2.757
batch start
#iterations: 199
currently lose_sum: 82.86476045846939
time_elpased: 2.824
start validation test
0.587371134021
0.619921546652
0.455387465267
0.525066745773
0.587602851708
74.881
batch start
#iterations: 200
currently lose_sum: 82.26301968097687
time_elpased: 2.767
batch start
#iterations: 201
currently lose_sum: 82.80299416184425
time_elpased: 2.735
batch start
#iterations: 202
currently lose_sum: 82.04398176074028
time_elpased: 2.697
batch start
#iterations: 203
currently lose_sum: 81.75483226776123
time_elpased: 2.694
batch start
#iterations: 204
currently lose_sum: 81.8272533416748
time_elpased: 2.733
batch start
#iterations: 205
currently lose_sum: 81.59338274598122
time_elpased: 2.818
batch start
#iterations: 206
currently lose_sum: 81.6604582965374
time_elpased: 2.642
batch start
#iterations: 207
currently lose_sum: 81.88354396820068
time_elpased: 2.796
batch start
#iterations: 208
currently lose_sum: 81.63357502222061
time_elpased: 2.749
batch start
#iterations: 209
currently lose_sum: 81.17979374527931
time_elpased: 2.768
batch start
#iterations: 210
currently lose_sum: 80.91146355867386
time_elpased: 2.729
batch start
#iterations: 211
currently lose_sum: 81.48413473367691
time_elpased: 2.642
batch start
#iterations: 212
currently lose_sum: 81.04833933711052
time_elpased: 2.619
batch start
#iterations: 213
currently lose_sum: 81.20930978655815
time_elpased: 2.706
batch start
#iterations: 214
currently lose_sum: 80.79627448320389
time_elpased: 2.709
batch start
#iterations: 215
currently lose_sum: 80.74056920409203
time_elpased: 2.771
batch start
#iterations: 216
currently lose_sum: 81.07697063684464
time_elpased: 2.749
batch start
#iterations: 217
currently lose_sum: 80.84531298279762
time_elpased: 2.735
batch start
#iterations: 218
currently lose_sum: 80.62202525138855
time_elpased: 2.768
batch start
#iterations: 219
currently lose_sum: 80.0653566122055
time_elpased: 2.747
start validation test
0.593092783505
0.619509636817
0.486261191726
0.54485701107
0.593280342842
79.451
batch start
#iterations: 220
currently lose_sum: 80.44143936038017
time_elpased: 2.77
batch start
#iterations: 221
currently lose_sum: 81.21812990307808
time_elpased: 2.823
batch start
#iterations: 222
currently lose_sum: 80.22848719358444
time_elpased: 2.761
batch start
#iterations: 223
currently lose_sum: 79.27421745657921
time_elpased: 2.793
batch start
#iterations: 224
currently lose_sum: 79.74486809968948
time_elpased: 2.745
batch start
#iterations: 225
currently lose_sum: 80.13340550661087
time_elpased: 2.732
batch start
#iterations: 226
currently lose_sum: 79.62852284312248
time_elpased: 2.721
batch start
#iterations: 227
currently lose_sum: 79.29535818099976
time_elpased: 2.728
batch start
#iterations: 228
currently lose_sum: 79.14686822891235
time_elpased: 2.717
batch start
#iterations: 229
currently lose_sum: 80.18680256605148
time_elpased: 2.714
batch start
#iterations: 230
currently lose_sum: 78.99506825208664
time_elpased: 2.721
batch start
#iterations: 231
currently lose_sum: 78.87044143676758
time_elpased: 2.753
batch start
#iterations: 232
currently lose_sum: 79.78125613927841
time_elpased: 2.736
batch start
#iterations: 233
currently lose_sum: 78.99947193264961
time_elpased: 2.729
batch start
#iterations: 234
currently lose_sum: 78.73653766512871
time_elpased: 2.68
batch start
#iterations: 235
currently lose_sum: 78.52530801296234
time_elpased: 2.69
batch start
#iterations: 236
currently lose_sum: 79.15556475520134
time_elpased: 2.701
batch start
#iterations: 237
currently lose_sum: 78.55703827738762
time_elpased: 2.77
batch start
#iterations: 238
currently lose_sum: 78.27058240771294
time_elpased: 2.749
batch start
#iterations: 239
currently lose_sum: 78.70185652375221
time_elpased: 2.738
start validation test
0.571288659794
0.644151565074
0.321910054544
0.429287037672
0.571726482399
79.684
batch start
#iterations: 240
currently lose_sum: 78.04437494277954
time_elpased: 2.663
batch start
#iterations: 241
currently lose_sum: 77.9687128663063
time_elpased: 2.72
batch start
#iterations: 242
currently lose_sum: 77.5653286576271
time_elpased: 2.738
batch start
#iterations: 243
currently lose_sum: 77.27614340186119
time_elpased: 2.78
batch start
#iterations: 244
currently lose_sum: 78.29622456431389
time_elpased: 2.73
batch start
#iterations: 245
currently lose_sum: 76.99766141176224
time_elpased: 2.77
batch start
#iterations: 246
currently lose_sum: 77.3161957859993
time_elpased: 2.742
batch start
#iterations: 247
currently lose_sum: 77.22950330376625
time_elpased: 2.739
batch start
#iterations: 248
currently lose_sum: 76.84869566559792
time_elpased: 2.708
batch start
#iterations: 249
currently lose_sum: 76.47326427698135
time_elpased: 2.729
batch start
#iterations: 250
currently lose_sum: 76.57461306452751
time_elpased: 2.722
batch start
#iterations: 251
currently lose_sum: 77.47869703173637
time_elpased: 2.599
batch start
#iterations: 252
currently lose_sum: 76.73961099982262
time_elpased: 2.709
batch start
#iterations: 253
currently lose_sum: 76.14835733175278
time_elpased: 2.691
batch start
#iterations: 254
currently lose_sum: 76.76176485419273
time_elpased: 2.707
batch start
#iterations: 255
currently lose_sum: 77.51712438464165
time_elpased: 2.718
batch start
#iterations: 256
currently lose_sum: 76.66431033611298
time_elpased: 2.664
batch start
#iterations: 257
currently lose_sum: 75.3634641468525
time_elpased: 2.716
batch start
#iterations: 258
currently lose_sum: 76.56781521439552
time_elpased: 2.721
batch start
#iterations: 259
currently lose_sum: 75.43339577317238
time_elpased: 2.731
start validation test
0.586391752577
0.621501363571
0.445610785222
0.519060177415
0.586638915279
83.999
batch start
#iterations: 260
currently lose_sum: 75.9414332807064
time_elpased: 2.687
batch start
#iterations: 261
currently lose_sum: 74.95195949077606
time_elpased: 2.721
batch start
#iterations: 262
currently lose_sum: 76.8135162293911
time_elpased: 2.662
batch start
#iterations: 263
currently lose_sum: 76.10751536488533
time_elpased: 2.795
batch start
#iterations: 264
currently lose_sum: 76.04511481523514
time_elpased: 2.794
batch start
#iterations: 265
currently lose_sum: 75.131163418293
time_elpased: 2.74
batch start
#iterations: 266
currently lose_sum: 75.4878143966198
time_elpased: 2.75
batch start
#iterations: 267
currently lose_sum: 75.21739485859871
time_elpased: 2.775
batch start
#iterations: 268
currently lose_sum: 75.01287087798119
time_elpased: 2.727
batch start
#iterations: 269
currently lose_sum: 75.30944228172302
time_elpased: 2.746
batch start
#iterations: 270
currently lose_sum: 75.25401997566223
time_elpased: 2.725
batch start
#iterations: 271
currently lose_sum: 74.25722262263298
time_elpased: 2.72
batch start
#iterations: 272
currently lose_sum: 74.46183383464813
time_elpased: 2.753
batch start
#iterations: 273
currently lose_sum: 74.76905852556229
time_elpased: 2.759
batch start
#iterations: 274
currently lose_sum: 74.12107983231544
time_elpased: 2.756
batch start
#iterations: 275
currently lose_sum: 74.01751512289047
time_elpased: 2.715
batch start
#iterations: 276
currently lose_sum: 73.27271103858948
time_elpased: 2.72
batch start
#iterations: 277
currently lose_sum: 73.32248643040657
time_elpased: 2.746
batch start
#iterations: 278
currently lose_sum: 73.92546552419662
time_elpased: 2.675
batch start
#iterations: 279
currently lose_sum: 74.85011431574821
time_elpased: 2.731
start validation test
0.569536082474
0.596360045147
0.435010805804
0.503064564118
0.569772262347
88.392
batch start
#iterations: 280
currently lose_sum: 73.13070645928383
time_elpased: 2.72
batch start
#iterations: 281
currently lose_sum: 73.07574760913849
time_elpased: 2.73
batch start
#iterations: 282
currently lose_sum: 73.59542572498322
time_elpased: 2.724
batch start
#iterations: 283
currently lose_sum: 72.99662700295448
time_elpased: 2.692
batch start
#iterations: 284
currently lose_sum: 72.86991292238235
time_elpased: 2.725
batch start
#iterations: 285
currently lose_sum: 72.39385822415352
time_elpased: 2.716
batch start
#iterations: 286
currently lose_sum: 73.05513805150986
time_elpased: 2.73
batch start
#iterations: 287
currently lose_sum: 72.69467806816101
time_elpased: 2.755
batch start
#iterations: 288
currently lose_sum: 72.49984613060951
time_elpased: 2.726
batch start
#iterations: 289
currently lose_sum: 71.9007179737091
time_elpased: 2.728
batch start
#iterations: 290
currently lose_sum: 72.77208757400513
time_elpased: 2.694
batch start
#iterations: 291
currently lose_sum: 72.82509794831276
time_elpased: 2.737
batch start
#iterations: 292
currently lose_sum: 71.7082878947258
time_elpased: 2.761
batch start
#iterations: 293
currently lose_sum: 72.23466444015503
time_elpased: 2.767
batch start
#iterations: 294
currently lose_sum: 72.78693264722824
time_elpased: 2.718
batch start
#iterations: 295
currently lose_sum: 71.29482820630074
time_elpased: 2.749
batch start
#iterations: 296
currently lose_sum: 72.40702602267265
time_elpased: 2.703
batch start
#iterations: 297
currently lose_sum: 71.55631253123283
time_elpased: 2.731
batch start
#iterations: 298
currently lose_sum: 72.84941574931145
time_elpased: 2.704
batch start
#iterations: 299
currently lose_sum: 70.98977899551392
time_elpased: 2.779
start validation test
0.575206185567
0.603390305408
0.443243799527
0.511064965885
0.57543786589
92.606
batch start
#iterations: 300
currently lose_sum: 71.54130801558495
time_elpased: 2.728
batch start
#iterations: 301
currently lose_sum: 71.27479511499405
time_elpased: 2.703
batch start
#iterations: 302
currently lose_sum: 72.3695567548275
time_elpased: 2.703
batch start
#iterations: 303
currently lose_sum: 70.66409718990326
time_elpased: 2.715
batch start
#iterations: 304
currently lose_sum: 71.1758106648922
time_elpased: 2.593
batch start
#iterations: 305
currently lose_sum: 70.40580150485039
time_elpased: 2.713
batch start
#iterations: 306
currently lose_sum: 70.34292584657669
time_elpased: 2.748
batch start
#iterations: 307
currently lose_sum: 71.52125573158264
time_elpased: 2.747
batch start
#iterations: 308
currently lose_sum: 71.04430839419365
time_elpased: 2.739
batch start
#iterations: 309
currently lose_sum: 70.81010267138481
time_elpased: 2.716
batch start
#iterations: 310
currently lose_sum: 70.27316451072693
time_elpased: 2.804
batch start
#iterations: 311
currently lose_sum: 69.67791450023651
time_elpased: 2.69
batch start
#iterations: 312
currently lose_sum: 70.49379485845566
time_elpased: 2.672
batch start
#iterations: 313
currently lose_sum: 69.13977766036987
time_elpased: 2.767
batch start
#iterations: 314
currently lose_sum: 70.2210176885128
time_elpased: 2.777
batch start
#iterations: 315
currently lose_sum: 69.44015961885452
time_elpased: 2.751
batch start
#iterations: 316
currently lose_sum: 69.61369553208351
time_elpased: 2.754
batch start
#iterations: 317
currently lose_sum: 69.15878900885582
time_elpased: 2.777
batch start
#iterations: 318
currently lose_sum: 68.63902655243874
time_elpased: 2.728
batch start
#iterations: 319
currently lose_sum: 70.34277945756912
time_elpased: 2.711
start validation test
0.570309278351
0.604194960012
0.412061335803
0.489965736662
0.570587107022
102.470
batch start
#iterations: 320
currently lose_sum: 68.77794659137726
time_elpased: 2.737
batch start
#iterations: 321
currently lose_sum: 68.8816528916359
time_elpased: 2.766
batch start
#iterations: 322
currently lose_sum: 69.41520887613297
time_elpased: 2.729
batch start
#iterations: 323
currently lose_sum: 69.58080837130547
time_elpased: 2.746
batch start
#iterations: 324
currently lose_sum: 68.47885611653328
time_elpased: 2.727
batch start
#iterations: 325
currently lose_sum: 68.0361690223217
time_elpased: 2.744
batch start
#iterations: 326
currently lose_sum: 68.45673868060112
time_elpased: 2.728
batch start
#iterations: 327
currently lose_sum: 68.71481785178185
time_elpased: 2.728
batch start
#iterations: 328
currently lose_sum: 68.39778584241867
time_elpased: 2.743
batch start
#iterations: 329
currently lose_sum: 67.92000761628151
time_elpased: 2.715
batch start
#iterations: 330
currently lose_sum: 68.63086396455765
time_elpased: 2.729
batch start
#iterations: 331
currently lose_sum: 67.78048703074455
time_elpased: 2.716
batch start
#iterations: 332
currently lose_sum: 67.9341250360012
time_elpased: 2.737
batch start
#iterations: 333
currently lose_sum: 68.23267295956612
time_elpased: 2.7
batch start
#iterations: 334
currently lose_sum: 67.50200235843658
time_elpased: 2.744
batch start
#iterations: 335
currently lose_sum: 66.94870710372925
time_elpased: 2.736
batch start
#iterations: 336
currently lose_sum: 66.39786368608475
time_elpased: 2.772
batch start
#iterations: 337
currently lose_sum: 67.29024598002434
time_elpased: 2.752
batch start
#iterations: 338
currently lose_sum: 67.26825267076492
time_elpased: 2.773
batch start
#iterations: 339
currently lose_sum: 67.88268193602562
time_elpased: 2.668
start validation test
0.564690721649
0.60195575505
0.386436142842
0.470698840489
0.565003675056
110.141
batch start
#iterations: 340
currently lose_sum: 68.02036821842194
time_elpased: 2.767
batch start
#iterations: 341
currently lose_sum: 67.70670512318611
time_elpased: 2.748
batch start
#iterations: 342
currently lose_sum: 66.56118804216385
time_elpased: 2.794
batch start
#iterations: 343
currently lose_sum: 65.78108277916908
time_elpased: 2.781
batch start
#iterations: 344
currently lose_sum: 66.8037742972374
time_elpased: 2.778
batch start
#iterations: 345
currently lose_sum: 66.73160544037819
time_elpased: 2.773
batch start
#iterations: 346
currently lose_sum: 65.86940136551857
time_elpased: 2.706
batch start
#iterations: 347
currently lose_sum: 65.35753029584885
time_elpased: 2.72
batch start
#iterations: 348
currently lose_sum: 65.53693357110023
time_elpased: 2.757
batch start
#iterations: 349
currently lose_sum: 66.25847992300987
time_elpased: 2.764
batch start
#iterations: 350
currently lose_sum: 67.08682653307915
time_elpased: 2.775
batch start
#iterations: 351
currently lose_sum: 65.44188868999481
time_elpased: 2.732
batch start
#iterations: 352
currently lose_sum: 64.72691518068314
time_elpased: 2.73
batch start
#iterations: 353
currently lose_sum: 65.80548021197319
time_elpased: 2.735
batch start
#iterations: 354
currently lose_sum: 65.33885216712952
time_elpased: 2.748
batch start
#iterations: 355
currently lose_sum: 65.26261159777641
time_elpased: 2.755
batch start
#iterations: 356
currently lose_sum: 65.08997082710266
time_elpased: 2.737
batch start
#iterations: 357
currently lose_sum: 63.79758298397064
time_elpased: 2.686
batch start
#iterations: 358
currently lose_sum: 65.27784302830696
time_elpased: 2.707
batch start
#iterations: 359
currently lose_sum: 65.42438396811485
time_elpased: 2.718
start validation test
0.562628865979
0.59183064997
0.408562313471
0.483409436834
0.562899353575
112.701
batch start
#iterations: 360
currently lose_sum: 67.02599683403969
time_elpased: 2.614
batch start
#iterations: 361
currently lose_sum: 65.87270927429199
time_elpased: 2.729
batch start
#iterations: 362
currently lose_sum: 64.14132153987885
time_elpased: 2.753
batch start
#iterations: 363
currently lose_sum: 64.97092953324318
time_elpased: 2.797
batch start
#iterations: 364
currently lose_sum: 64.93929263949394
time_elpased: 2.754
batch start
#iterations: 365
currently lose_sum: 65.2696985900402
time_elpased: 2.754
batch start
#iterations: 366
currently lose_sum: 65.78438439965248
time_elpased: 2.756
batch start
#iterations: 367
currently lose_sum: 64.1443040072918
time_elpased: 2.721
batch start
#iterations: 368
currently lose_sum: 63.72805571556091
time_elpased: 2.765
batch start
#iterations: 369
currently lose_sum: 63.71238034963608
time_elpased: 2.72
batch start
#iterations: 370
currently lose_sum: 64.45072141289711
time_elpased: 2.772
batch start
#iterations: 371
currently lose_sum: 64.37970322370529
time_elpased: 2.799
batch start
#iterations: 372
currently lose_sum: 62.02401152253151
time_elpased: 2.714
batch start
#iterations: 373
currently lose_sum: 64.33715263009071
time_elpased: 2.721
batch start
#iterations: 374
currently lose_sum: 62.70725306868553
time_elpased: 2.729
batch start
#iterations: 375
currently lose_sum: 63.689964681863785
time_elpased: 2.716
batch start
#iterations: 376
currently lose_sum: 63.85708621144295
time_elpased: 2.732
batch start
#iterations: 377
currently lose_sum: 64.12648132443428
time_elpased: 2.706
batch start
#iterations: 378
currently lose_sum: 63.1798013150692
time_elpased: 2.732
batch start
#iterations: 379
currently lose_sum: 63.61010444164276
time_elpased: 2.742
start validation test
0.553402061856
0.607823059595
0.305444067099
0.406575342466
0.55383739036
120.054
batch start
#iterations: 380
currently lose_sum: 62.08714014291763
time_elpased: 2.739
batch start
#iterations: 381
currently lose_sum: 62.836681485176086
time_elpased: 2.668
batch start
#iterations: 382
currently lose_sum: 63.05313292145729
time_elpased: 2.73
batch start
#iterations: 383
currently lose_sum: 64.30071452260017
time_elpased: 2.756
batch start
#iterations: 384
currently lose_sum: 63.21208760142326
time_elpased: 2.779
batch start
#iterations: 385
currently lose_sum: 63.67708072066307
time_elpased: 2.779
batch start
#iterations: 386
currently lose_sum: 61.89017766714096
time_elpased: 2.772
batch start
#iterations: 387
currently lose_sum: 63.48369446396828
time_elpased: 2.763
batch start
#iterations: 388
currently lose_sum: 62.19551509618759
time_elpased: 2.681
batch start
#iterations: 389
currently lose_sum: 61.858010202646255
time_elpased: 2.74
batch start
#iterations: 390
currently lose_sum: 61.65617597103119
time_elpased: 2.782
batch start
#iterations: 391
currently lose_sum: 62.27862337231636
time_elpased: 2.759
batch start
#iterations: 392
currently lose_sum: 62.43057370185852
time_elpased: 2.745
batch start
#iterations: 393
currently lose_sum: 61.86691972613335
time_elpased: 2.78
batch start
#iterations: 394
currently lose_sum: 62.110760390758514
time_elpased: 2.847
batch start
#iterations: 395
currently lose_sum: 61.43049046397209
time_elpased: 2.776
batch start
#iterations: 396
currently lose_sum: 61.07054457068443
time_elpased: 2.714
batch start
#iterations: 397
currently lose_sum: 60.419468730688095
time_elpased: 2.752
batch start
#iterations: 398
currently lose_sum: 61.107121616601944
time_elpased: 2.814
batch start
#iterations: 399
currently lose_sum: 59.39691212773323
time_elpased: 2.739
start validation test
0.55206185567
0.58342810723
0.369558505712
0.452494959677
0.55238226845
131.506
acc: 0.642
pre: 0.622
rec: 0.727
F1: 0.671
auc: 0.642
