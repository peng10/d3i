start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.39121437072754
time_elpased: 2.493
batch start
#iterations: 1
currently lose_sum: 99.88430333137512
time_elpased: 2.466
batch start
#iterations: 2
currently lose_sum: 99.42549008131027
time_elpased: 2.497
batch start
#iterations: 3
currently lose_sum: 98.82490366697311
time_elpased: 2.476
batch start
#iterations: 4
currently lose_sum: 98.6979792714119
time_elpased: 2.465
batch start
#iterations: 5
currently lose_sum: 98.19628274440765
time_elpased: 2.447
batch start
#iterations: 6
currently lose_sum: 97.48390519618988
time_elpased: 2.451
batch start
#iterations: 7
currently lose_sum: 97.09581565856934
time_elpased: 2.423
batch start
#iterations: 8
currently lose_sum: 96.17017924785614
time_elpased: 2.53
batch start
#iterations: 9
currently lose_sum: 96.43790119886398
time_elpased: 2.497
batch start
#iterations: 10
currently lose_sum: 95.46543329954147
time_elpased: 2.448
batch start
#iterations: 11
currently lose_sum: 95.54378020763397
time_elpased: 2.48
batch start
#iterations: 12
currently lose_sum: 94.96693897247314
time_elpased: 2.453
batch start
#iterations: 13
currently lose_sum: 94.42131346464157
time_elpased: 2.442
batch start
#iterations: 14
currently lose_sum: 94.47552561759949
time_elpased: 2.426
batch start
#iterations: 15
currently lose_sum: 93.70512342453003
time_elpased: 2.435
batch start
#iterations: 16
currently lose_sum: 93.6340941786766
time_elpased: 2.45
batch start
#iterations: 17
currently lose_sum: 93.27649599313736
time_elpased: 2.43
batch start
#iterations: 18
currently lose_sum: 93.04525548219681
time_elpased: 2.472
batch start
#iterations: 19
currently lose_sum: 92.40638965368271
time_elpased: 2.491
start validation test
0.602886597938
0.613896118592
0.558299886796
0.584779562359
0.602964876787
63.027
batch start
#iterations: 20
currently lose_sum: 92.02016454935074
time_elpased: 2.441
batch start
#iterations: 21
currently lose_sum: 91.43146282434464
time_elpased: 2.463
batch start
#iterations: 22
currently lose_sum: 91.38407105207443
time_elpased: 2.589
batch start
#iterations: 23
currently lose_sum: 91.41296571493149
time_elpased: 2.564
batch start
#iterations: 24
currently lose_sum: 90.5963242650032
time_elpased: 2.577
batch start
#iterations: 25
currently lose_sum: 89.81293964385986
time_elpased: 2.59
batch start
#iterations: 26
currently lose_sum: 90.56640857458115
time_elpased: 2.615
batch start
#iterations: 27
currently lose_sum: 89.7194395661354
time_elpased: 2.653
batch start
#iterations: 28
currently lose_sum: 89.69363284111023
time_elpased: 2.612
batch start
#iterations: 29
currently lose_sum: 89.04029583930969
time_elpased: 2.592
batch start
#iterations: 30
currently lose_sum: 88.56882500648499
time_elpased: 2.571
batch start
#iterations: 31
currently lose_sum: 88.25784021615982
time_elpased: 2.683
batch start
#iterations: 32
currently lose_sum: 87.91491687297821
time_elpased: 2.591
batch start
#iterations: 33
currently lose_sum: 88.17541569471359
time_elpased: 2.611
batch start
#iterations: 34
currently lose_sum: 87.41128826141357
time_elpased: 2.596
batch start
#iterations: 35
currently lose_sum: 86.7714712023735
time_elpased: 2.606
batch start
#iterations: 36
currently lose_sum: 86.73301291465759
time_elpased: 2.582
batch start
#iterations: 37
currently lose_sum: 85.9576860666275
time_elpased: 2.628
batch start
#iterations: 38
currently lose_sum: 85.9933432340622
time_elpased: 2.633
batch start
#iterations: 39
currently lose_sum: 85.76691842079163
time_elpased: 2.559
start validation test
0.620463917526
0.64516526887
0.53833487702
0.586928471248
0.620608107724
64.769
batch start
#iterations: 40
currently lose_sum: 85.48453950881958
time_elpased: 2.443
batch start
#iterations: 41
currently lose_sum: 85.1345539689064
time_elpased: 2.481
batch start
#iterations: 42
currently lose_sum: 84.68023949861526
time_elpased: 2.426
batch start
#iterations: 43
currently lose_sum: 84.1343691945076
time_elpased: 2.45
batch start
#iterations: 44
currently lose_sum: 84.02489647269249
time_elpased: 2.449
batch start
#iterations: 45
currently lose_sum: 83.11134576797485
time_elpased: 2.444
batch start
#iterations: 46
currently lose_sum: 83.44353932142258
time_elpased: 2.411
batch start
#iterations: 47
currently lose_sum: 82.87059098482132
time_elpased: 2.454
batch start
#iterations: 48
currently lose_sum: 82.61988407373428
time_elpased: 2.42
batch start
#iterations: 49
currently lose_sum: 82.44646275043488
time_elpased: 2.417
batch start
#iterations: 50
currently lose_sum: 82.0094293653965
time_elpased: 2.443
batch start
#iterations: 51
currently lose_sum: 81.67156940698624
time_elpased: 2.438
batch start
#iterations: 52
currently lose_sum: 80.99610447883606
time_elpased: 2.409
batch start
#iterations: 53
currently lose_sum: 81.1427204310894
time_elpased: 2.421
batch start
#iterations: 54
currently lose_sum: 80.94362661242485
time_elpased: 2.449
batch start
#iterations: 55
currently lose_sum: 79.86578926444054
time_elpased: 2.438
batch start
#iterations: 56
currently lose_sum: 79.9197940826416
time_elpased: 2.443
batch start
#iterations: 57
currently lose_sum: 80.2684497833252
time_elpased: 2.425
batch start
#iterations: 58
currently lose_sum: 79.72895097732544
time_elpased: 2.391
batch start
#iterations: 59
currently lose_sum: 79.49865761399269
time_elpased: 2.461
start validation test
0.608195876289
0.622994652406
0.551507666975
0.585075604564
0.608295401184
70.917
batch start
#iterations: 60
currently lose_sum: 78.62486028671265
time_elpased: 2.44
batch start
#iterations: 61
currently lose_sum: 78.42609968781471
time_elpased: 2.498
batch start
#iterations: 62
currently lose_sum: 78.3364317715168
time_elpased: 2.425
batch start
#iterations: 63
currently lose_sum: 77.60312792658806
time_elpased: 2.509
batch start
#iterations: 64
currently lose_sum: 77.9129926264286
time_elpased: 2.426
batch start
#iterations: 65
currently lose_sum: 77.13390761613846
time_elpased: 2.461
batch start
#iterations: 66
currently lose_sum: 76.73563688993454
time_elpased: 2.435
batch start
#iterations: 67
currently lose_sum: 77.5773538351059
time_elpased: 2.456
batch start
#iterations: 68
currently lose_sum: 76.85392940044403
time_elpased: 2.444
batch start
#iterations: 69
currently lose_sum: 76.52132937312126
time_elpased: 2.416
batch start
#iterations: 70
currently lose_sum: 75.21489134430885
time_elpased: 2.446
batch start
#iterations: 71
currently lose_sum: 75.4369832277298
time_elpased: 2.446
batch start
#iterations: 72
currently lose_sum: 75.30037289857864
time_elpased: 2.409
batch start
#iterations: 73
currently lose_sum: 75.46198987960815
time_elpased: 2.45
batch start
#iterations: 74
currently lose_sum: 74.53544908761978
time_elpased: 2.415
batch start
#iterations: 75
currently lose_sum: 74.68079295754433
time_elpased: 2.455
batch start
#iterations: 76
currently lose_sum: 74.54444026947021
time_elpased: 2.459
batch start
#iterations: 77
currently lose_sum: 73.51752156019211
time_elpased: 2.457
batch start
#iterations: 78
currently lose_sum: 73.38841530680656
time_elpased: 2.442
batch start
#iterations: 79
currently lose_sum: 73.22313606739044
time_elpased: 2.453
start validation test
0.588350515464
0.618448063501
0.465061232891
0.530897556391
0.588566968816
81.034
batch start
#iterations: 80
currently lose_sum: 72.71065667271614
time_elpased: 2.445
batch start
#iterations: 81
currently lose_sum: 73.30611184239388
time_elpased: 2.447
batch start
#iterations: 82
currently lose_sum: 72.3508685529232
time_elpased: 2.427
batch start
#iterations: 83
currently lose_sum: 73.15268903970718
time_elpased: 2.468
batch start
#iterations: 84
currently lose_sum: 72.54255962371826
time_elpased: 2.437
batch start
#iterations: 85
currently lose_sum: 72.30374419689178
time_elpased: 2.549
batch start
#iterations: 86
currently lose_sum: 72.06532528996468
time_elpased: 2.422
batch start
#iterations: 87
currently lose_sum: 71.33626306056976
time_elpased: 2.45
batch start
#iterations: 88
currently lose_sum: 71.05077868700027
time_elpased: 2.497
batch start
#iterations: 89
currently lose_sum: 70.72422179579735
time_elpased: 2.442
batch start
#iterations: 90
currently lose_sum: 70.64312011003494
time_elpased: 2.356
batch start
#iterations: 91
currently lose_sum: 71.34903228282928
time_elpased: 2.358
batch start
#iterations: 92
currently lose_sum: 69.80213755369186
time_elpased: 2.423
batch start
#iterations: 93
currently lose_sum: 69.60561609268188
time_elpased: 2.438
batch start
#iterations: 94
currently lose_sum: 69.45242962241173
time_elpased: 2.437
batch start
#iterations: 95
currently lose_sum: 69.1780035495758
time_elpased: 2.46
batch start
#iterations: 96
currently lose_sum: 69.5343467593193
time_elpased: 2.473
batch start
#iterations: 97
currently lose_sum: 68.80483821034431
time_elpased: 2.453
batch start
#iterations: 98
currently lose_sum: 69.14080566167831
time_elpased: 2.461
batch start
#iterations: 99
currently lose_sum: 69.63094288110733
time_elpased: 2.48
start validation test
0.592783505155
0.646178600161
0.413296284862
0.504142606076
0.593098622654
94.131
batch start
#iterations: 100
currently lose_sum: 68.59562620520592
time_elpased: 2.494
batch start
#iterations: 101
currently lose_sum: 68.43007072806358
time_elpased: 2.482
batch start
#iterations: 102
currently lose_sum: 68.61850130558014
time_elpased: 2.431
batch start
#iterations: 103
currently lose_sum: 67.89444947242737
time_elpased: 2.445
batch start
#iterations: 104
currently lose_sum: 67.0906392633915
time_elpased: 2.447
batch start
#iterations: 105
currently lose_sum: 67.77631184458733
time_elpased: 2.465
batch start
#iterations: 106
currently lose_sum: 67.21774971485138
time_elpased: 2.464
batch start
#iterations: 107
currently lose_sum: 67.07961541414261
time_elpased: 2.453
batch start
#iterations: 108
currently lose_sum: 66.73753249645233
time_elpased: 2.473
batch start
#iterations: 109
currently lose_sum: 66.32126024365425
time_elpased: 2.452
batch start
#iterations: 110
currently lose_sum: 66.18789100646973
time_elpased: 2.451
batch start
#iterations: 111
currently lose_sum: 66.54057288169861
time_elpased: 2.428
batch start
#iterations: 112
currently lose_sum: 65.28700286149979
time_elpased: 2.464
batch start
#iterations: 113
currently lose_sum: 65.21277916431427
time_elpased: 2.43
batch start
#iterations: 114
currently lose_sum: 65.66300663352013
time_elpased: 2.445
batch start
#iterations: 115
currently lose_sum: 65.16684284806252
time_elpased: 2.472
batch start
#iterations: 116
currently lose_sum: 65.02990218997002
time_elpased: 2.444
batch start
#iterations: 117
currently lose_sum: 64.90867900848389
time_elpased: 2.447
batch start
#iterations: 118
currently lose_sum: 65.02977332472801
time_elpased: 2.472
batch start
#iterations: 119
currently lose_sum: 65.36059966683388
time_elpased: 2.509
start validation test
0.579020618557
0.638195435093
0.368323556653
0.467079934747
0.579390529747
99.122
batch start
#iterations: 120
currently lose_sum: 64.51472371816635
time_elpased: 2.477
batch start
#iterations: 121
currently lose_sum: 64.55159053206444
time_elpased: 2.444
batch start
#iterations: 122
currently lose_sum: 64.60297852754593
time_elpased: 2.466
batch start
#iterations: 123
currently lose_sum: 64.86089813709259
time_elpased: 2.422
batch start
#iterations: 124
currently lose_sum: 64.00170171260834
time_elpased: 2.454
batch start
#iterations: 125
currently lose_sum: 63.72111248970032
time_elpased: 2.464
batch start
#iterations: 126
currently lose_sum: 63.82445266842842
time_elpased: 2.418
batch start
#iterations: 127
currently lose_sum: 63.097770273685455
time_elpased: 2.468
batch start
#iterations: 128
currently lose_sum: 63.54434469342232
time_elpased: 2.427
batch start
#iterations: 129
currently lose_sum: 62.596031188964844
time_elpased: 2.463
batch start
#iterations: 130
currently lose_sum: 62.76525917649269
time_elpased: 2.421
batch start
#iterations: 131
currently lose_sum: 62.717634588479996
time_elpased: 2.466
batch start
#iterations: 132
currently lose_sum: 62.291328847408295
time_elpased: 2.463
batch start
#iterations: 133
currently lose_sum: 62.79268252849579
time_elpased: 2.462
batch start
#iterations: 134
currently lose_sum: 62.01291686296463
time_elpased: 2.462
batch start
#iterations: 135
currently lose_sum: 61.54043647646904
time_elpased: 2.46
batch start
#iterations: 136
currently lose_sum: 62.138075679540634
time_elpased: 2.466
batch start
#iterations: 137
currently lose_sum: 60.68464747071266
time_elpased: 2.424
batch start
#iterations: 138
currently lose_sum: 59.71955448389053
time_elpased: 2.409
batch start
#iterations: 139
currently lose_sum: 61.48136740922928
time_elpased: 2.528
start validation test
0.574278350515
0.624063989108
0.377379849748
0.470339254794
0.574624036203
112.421
batch start
#iterations: 140
currently lose_sum: 60.658138900995255
time_elpased: 2.464
batch start
#iterations: 141
currently lose_sum: 61.12030637264252
time_elpased: 2.487
batch start
#iterations: 142
currently lose_sum: 60.28296485543251
time_elpased: 2.448
batch start
#iterations: 143
currently lose_sum: 60.09649768471718
time_elpased: 2.424
batch start
#iterations: 144
currently lose_sum: 59.670828849077225
time_elpased: 2.445
batch start
#iterations: 145
currently lose_sum: 59.5918505191803
time_elpased: 2.46
batch start
#iterations: 146
currently lose_sum: 60.391032695770264
time_elpased: 2.452
batch start
#iterations: 147
currently lose_sum: 60.95899215340614
time_elpased: 2.461
batch start
#iterations: 148
currently lose_sum: 59.45804914832115
time_elpased: 2.432
batch start
#iterations: 149
currently lose_sum: 59.633623361587524
time_elpased: 2.485
batch start
#iterations: 150
currently lose_sum: 58.091358840465546
time_elpased: 2.445
batch start
#iterations: 151
currently lose_sum: 59.82708424329758
time_elpased: 2.448
batch start
#iterations: 152
currently lose_sum: 58.57728323340416
time_elpased: 2.456
batch start
#iterations: 153
currently lose_sum: 57.78062587976456
time_elpased: 2.447
batch start
#iterations: 154
currently lose_sum: 58.23043093085289
time_elpased: 2.496
batch start
#iterations: 155
currently lose_sum: 59.0476291179657
time_elpased: 2.502
batch start
#iterations: 156
currently lose_sum: 57.798886716365814
time_elpased: 2.431
batch start
#iterations: 157
currently lose_sum: 58.26668068766594
time_elpased: 2.464
batch start
#iterations: 158
currently lose_sum: 59.227340430021286
time_elpased: 2.457
batch start
#iterations: 159
currently lose_sum: 58.10381206870079
time_elpased: 2.493
start validation test
0.561958762887
0.625955775987
0.311721724812
0.41618576532
0.562398092603
121.374
batch start
#iterations: 160
currently lose_sum: 57.978115260601044
time_elpased: 2.444
batch start
#iterations: 161
currently lose_sum: 57.335125774145126
time_elpased: 2.473
batch start
#iterations: 162
currently lose_sum: 57.45168834924698
time_elpased: 2.468
batch start
#iterations: 163
currently lose_sum: 57.508625745773315
time_elpased: 2.47
batch start
#iterations: 164
currently lose_sum: 57.996148228645325
time_elpased: 2.392
batch start
#iterations: 165
currently lose_sum: 56.260775089263916
time_elpased: 2.434
batch start
#iterations: 166
currently lose_sum: 57.21316310763359
time_elpased: 2.401
batch start
#iterations: 167
currently lose_sum: 56.70495483279228
time_elpased: 2.405
batch start
#iterations: 168
currently lose_sum: 56.58652597665787
time_elpased: 2.43
batch start
#iterations: 169
currently lose_sum: 56.331358939409256
time_elpased: 2.396
batch start
#iterations: 170
currently lose_sum: 55.575277239084244
time_elpased: 2.439
batch start
#iterations: 171
currently lose_sum: 56.49950575828552
time_elpased: 2.451
batch start
#iterations: 172
currently lose_sum: 55.56858482956886
time_elpased: 2.419
batch start
#iterations: 173
currently lose_sum: 54.49881488084793
time_elpased: 2.441
batch start
#iterations: 174
currently lose_sum: 55.07669648528099
time_elpased: 2.375
batch start
#iterations: 175
currently lose_sum: 55.116117507219315
time_elpased: 2.397
batch start
#iterations: 176
currently lose_sum: 55.526506811380386
time_elpased: 2.397
batch start
#iterations: 177
currently lose_sum: 54.2180982530117
time_elpased: 2.38
batch start
#iterations: 178
currently lose_sum: 53.18928915262222
time_elpased: 2.405
batch start
#iterations: 179
currently lose_sum: 53.80923116207123
time_elpased: 2.431
start validation test
0.55912371134
0.609234234234
0.334053720284
0.431505483549
0.559518856424
130.874
batch start
#iterations: 180
currently lose_sum: 54.09319880604744
time_elpased: 2.394
batch start
#iterations: 181
currently lose_sum: 53.18165272474289
time_elpased: 2.437
batch start
#iterations: 182
currently lose_sum: 53.40753194689751
time_elpased: 2.369
batch start
#iterations: 183
currently lose_sum: 53.15338417887688
time_elpased: 2.377
batch start
#iterations: 184
currently lose_sum: 54.43069139122963
time_elpased: 2.387
batch start
#iterations: 185
currently lose_sum: 53.2037598490715
time_elpased: 2.382
batch start
#iterations: 186
currently lose_sum: 53.35694697499275
time_elpased: 2.436
batch start
#iterations: 187
currently lose_sum: 53.661431819200516
time_elpased: 2.41
batch start
#iterations: 188
currently lose_sum: 52.39957079291344
time_elpased: 2.418
batch start
#iterations: 189
currently lose_sum: 52.539585173130035
time_elpased: 2.472
batch start
#iterations: 190
currently lose_sum: 52.50373736023903
time_elpased: 2.406
batch start
#iterations: 191
currently lose_sum: 52.45342028141022
time_elpased: 2.383
batch start
#iterations: 192
currently lose_sum: 52.42020025849342
time_elpased: 2.392
batch start
#iterations: 193
currently lose_sum: 52.7608856856823
time_elpased: 2.447
batch start
#iterations: 194
currently lose_sum: 52.630269795656204
time_elpased: 2.467
batch start
#iterations: 195
currently lose_sum: 51.91378280520439
time_elpased: 2.515
batch start
#iterations: 196
currently lose_sum: 51.81793186068535
time_elpased: 2.403
batch start
#iterations: 197
currently lose_sum: 51.91534906625748
time_elpased: 2.407
batch start
#iterations: 198
currently lose_sum: 52.02092897891998
time_elpased: 2.469
batch start
#iterations: 199
currently lose_sum: 51.26276242733002
time_elpased: 2.482
start validation test
0.550979381443
0.614318181818
0.278172275394
0.382942551534
0.551458336396
151.102
batch start
#iterations: 200
currently lose_sum: 51.01922869682312
time_elpased: 2.471
batch start
#iterations: 201
currently lose_sum: 50.724730521440506
time_elpased: 2.414
batch start
#iterations: 202
currently lose_sum: 51.37782347202301
time_elpased: 2.404
batch start
#iterations: 203
currently lose_sum: 51.25532394647598
time_elpased: 2.467
batch start
#iterations: 204
currently lose_sum: 50.629119515419006
time_elpased: 2.418
batch start
#iterations: 205
currently lose_sum: 51.93132925033569
time_elpased: 2.438
batch start
#iterations: 206
currently lose_sum: 49.74668774008751
time_elpased: 2.403
batch start
#iterations: 207
currently lose_sum: 50.98381642997265
time_elpased: 2.417
batch start
#iterations: 208
currently lose_sum: 49.793152987957
time_elpased: 2.374
batch start
#iterations: 209
currently lose_sum: 49.062064200639725
time_elpased: 2.475
batch start
#iterations: 210
currently lose_sum: 48.73049947619438
time_elpased: 2.455
batch start
#iterations: 211
currently lose_sum: 47.95880305767059
time_elpased: 2.456
batch start
#iterations: 212
currently lose_sum: 49.741011559963226
time_elpased: 2.494
batch start
#iterations: 213
currently lose_sum: 49.4988688826561
time_elpased: 2.386
batch start
#iterations: 214
currently lose_sum: 50.016382679343224
time_elpased: 2.414
batch start
#iterations: 215
currently lose_sum: 48.96973252296448
time_elpased: 2.419
batch start
#iterations: 216
currently lose_sum: 48.258838921785355
time_elpased: 2.41
batch start
#iterations: 217
currently lose_sum: 48.185023441910744
time_elpased: 2.464
batch start
#iterations: 218
currently lose_sum: 48.55068498849869
time_elpased: 2.375
batch start
#iterations: 219
currently lose_sum: 47.77502688765526
time_elpased: 2.492
start validation test
0.544432989691
0.606752489677
0.25707522898
0.361139222206
0.544937490562
161.697
batch start
#iterations: 220
currently lose_sum: 48.30236867070198
time_elpased: 2.442
batch start
#iterations: 221
currently lose_sum: 46.984899148344994
time_elpased: 2.402
batch start
#iterations: 222
currently lose_sum: 47.464357912540436
time_elpased: 2.402
batch start
#iterations: 223
currently lose_sum: 47.57958574593067
time_elpased: 2.453
batch start
#iterations: 224
currently lose_sum: 47.20935124158859
time_elpased: 2.381
batch start
#iterations: 225
currently lose_sum: 47.03551197052002
time_elpased: 2.436
batch start
#iterations: 226
currently lose_sum: 47.295137882232666
time_elpased: 2.397
batch start
#iterations: 227
currently lose_sum: 46.54738463461399
time_elpased: 2.425
batch start
#iterations: 228
currently lose_sum: 46.92027851939201
time_elpased: 2.41
batch start
#iterations: 229
currently lose_sum: 46.87241645157337
time_elpased: 2.446
batch start
#iterations: 230
currently lose_sum: 46.03346613049507
time_elpased: 2.412
batch start
#iterations: 231
currently lose_sum: 45.93661408126354
time_elpased: 2.418
batch start
#iterations: 232
currently lose_sum: 46.297503620386124
time_elpased: 2.416
batch start
#iterations: 233
currently lose_sum: 45.78512644767761
time_elpased: 2.41
batch start
#iterations: 234
currently lose_sum: 45.81108048558235
time_elpased: 2.502
batch start
#iterations: 235
currently lose_sum: 45.491218879818916
time_elpased: 2.427
batch start
#iterations: 236
currently lose_sum: 45.74322934448719
time_elpased: 2.409
batch start
#iterations: 237
currently lose_sum: 46.21098141372204
time_elpased: 2.454
batch start
#iterations: 238
currently lose_sum: 45.727906569838524
time_elpased: 2.398
batch start
#iterations: 239
currently lose_sum: 44.54668806493282
time_elpased: 2.432
start validation test
0.541494845361
0.632324533162
0.202119995884
0.306324573033
0.542090670254
175.842
batch start
#iterations: 240
currently lose_sum: 44.56314027309418
time_elpased: 2.416
batch start
#iterations: 241
currently lose_sum: 44.2783749550581
time_elpased: 2.411
batch start
#iterations: 242
currently lose_sum: 44.31975854933262
time_elpased: 2.387
batch start
#iterations: 243
currently lose_sum: 44.19032607972622
time_elpased: 2.44
batch start
#iterations: 244
currently lose_sum: 45.01250833272934
time_elpased: 2.418
batch start
#iterations: 245
currently lose_sum: 43.04720374941826
time_elpased: 2.427
batch start
#iterations: 246
currently lose_sum: 42.8069111853838
time_elpased: 2.468
batch start
#iterations: 247
currently lose_sum: 43.60488773882389
time_elpased: 2.387
batch start
#iterations: 248
currently lose_sum: 43.680822774767876
time_elpased: 2.443
batch start
#iterations: 249
currently lose_sum: 43.03486241400242
time_elpased: 2.436
batch start
#iterations: 250
currently lose_sum: 43.38865275681019
time_elpased: 2.403
batch start
#iterations: 251
currently lose_sum: 42.639356672763824
time_elpased: 2.442
batch start
#iterations: 252
currently lose_sum: 43.20485357940197
time_elpased: 2.415
batch start
#iterations: 253
currently lose_sum: 42.725318253040314
time_elpased: 2.451
batch start
#iterations: 254
currently lose_sum: 42.515803918242455
time_elpased: 2.438
batch start
#iterations: 255
currently lose_sum: nan
time_elpased: 2.4
train finish final lose is: nan
acc: 0.602
pre: 0.614
rec: 0.552
F1: 0.581
auc: 0.602
