start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.21571838855743
time_elpased: 1.931
batch start
#iterations: 1
currently lose_sum: 99.77304643392563
time_elpased: 1.875
batch start
#iterations: 2
currently lose_sum: 99.4359792470932
time_elpased: 1.86
batch start
#iterations: 3
currently lose_sum: 98.95451658964157
time_elpased: 1.897
batch start
#iterations: 4
currently lose_sum: 98.74884849786758
time_elpased: 1.841
batch start
#iterations: 5
currently lose_sum: 98.33884555101395
time_elpased: 1.892
batch start
#iterations: 6
currently lose_sum: 98.0629033446312
time_elpased: 1.89
batch start
#iterations: 7
currently lose_sum: 97.61753642559052
time_elpased: 1.877
batch start
#iterations: 8
currently lose_sum: 97.39736258983612
time_elpased: 1.88
batch start
#iterations: 9
currently lose_sum: 97.28560119867325
time_elpased: 1.879
batch start
#iterations: 10
currently lose_sum: 96.88892251253128
time_elpased: 1.854
batch start
#iterations: 11
currently lose_sum: 96.68636322021484
time_elpased: 1.868
batch start
#iterations: 12
currently lose_sum: 96.60605722665787
time_elpased: 1.841
batch start
#iterations: 13
currently lose_sum: 96.20992350578308
time_elpased: 1.858
batch start
#iterations: 14
currently lose_sum: 96.15715336799622
time_elpased: 1.888
batch start
#iterations: 15
currently lose_sum: 95.56993675231934
time_elpased: 1.918
batch start
#iterations: 16
currently lose_sum: 96.01074427366257
time_elpased: 1.872
batch start
#iterations: 17
currently lose_sum: 95.68883645534515
time_elpased: 1.874
batch start
#iterations: 18
currently lose_sum: 95.23234045505524
time_elpased: 1.854
batch start
#iterations: 19
currently lose_sum: 95.02538883686066
time_elpased: 1.849
start validation test
0.624329896907
0.656205787781
0.525059174642
0.583352389664
0.624504181972
62.666
batch start
#iterations: 20
currently lose_sum: 95.01673513650894
time_elpased: 1.86
batch start
#iterations: 21
currently lose_sum: 94.75297921895981
time_elpased: 1.912
batch start
#iterations: 22
currently lose_sum: 94.62729090452194
time_elpased: 1.882
batch start
#iterations: 23
currently lose_sum: 94.62164574861526
time_elpased: 1.912
batch start
#iterations: 24
currently lose_sum: 94.1110987663269
time_elpased: 1.865
batch start
#iterations: 25
currently lose_sum: 93.43492412567139
time_elpased: 1.875
batch start
#iterations: 26
currently lose_sum: 93.72216606140137
time_elpased: 1.865
batch start
#iterations: 27
currently lose_sum: 93.30498540401459
time_elpased: 1.885
batch start
#iterations: 28
currently lose_sum: 93.11200428009033
time_elpased: 1.885
batch start
#iterations: 29
currently lose_sum: 93.45419770479202
time_elpased: 1.914
batch start
#iterations: 30
currently lose_sum: 92.96717756986618
time_elpased: 1.878
batch start
#iterations: 31
currently lose_sum: 92.94569820165634
time_elpased: 1.883
batch start
#iterations: 32
currently lose_sum: 92.00319409370422
time_elpased: 1.874
batch start
#iterations: 33
currently lose_sum: 92.61054533720016
time_elpased: 1.869
batch start
#iterations: 34
currently lose_sum: 92.34180200099945
time_elpased: 1.872
batch start
#iterations: 35
currently lose_sum: 92.22630304098129
time_elpased: 1.877
batch start
#iterations: 36
currently lose_sum: 92.23288887739182
time_elpased: 1.933
batch start
#iterations: 37
currently lose_sum: 92.03994596004486
time_elpased: 1.899
batch start
#iterations: 38
currently lose_sum: 91.67040920257568
time_elpased: 1.862
batch start
#iterations: 39
currently lose_sum: 91.81837731599808
time_elpased: 1.838
start validation test
0.615309278351
0.641761006289
0.525059174642
0.577574008038
0.615467726328
63.264
batch start
#iterations: 40
currently lose_sum: 91.4908943772316
time_elpased: 1.868
batch start
#iterations: 41
currently lose_sum: 91.42339104413986
time_elpased: 1.872
batch start
#iterations: 42
currently lose_sum: 91.18360024690628
time_elpased: 1.857
batch start
#iterations: 43
currently lose_sum: 90.82712656259537
time_elpased: 1.868
batch start
#iterations: 44
currently lose_sum: 91.17364913225174
time_elpased: 1.887
batch start
#iterations: 45
currently lose_sum: 90.4936198592186
time_elpased: 1.878
batch start
#iterations: 46
currently lose_sum: 90.56645154953003
time_elpased: 1.916
batch start
#iterations: 47
currently lose_sum: 89.96565181016922
time_elpased: 1.877
batch start
#iterations: 48
currently lose_sum: 90.3510531783104
time_elpased: 1.877
batch start
#iterations: 49
currently lose_sum: 90.83668285608292
time_elpased: 1.867
batch start
#iterations: 50
currently lose_sum: 91.0903097987175
time_elpased: 1.893
batch start
#iterations: 51
currently lose_sum: 90.099884390831
time_elpased: 1.874
batch start
#iterations: 52
currently lose_sum: 89.94581836462021
time_elpased: 1.876
batch start
#iterations: 53
currently lose_sum: 89.69654607772827
time_elpased: 1.878
batch start
#iterations: 54
currently lose_sum: 90.09318971633911
time_elpased: 1.851
batch start
#iterations: 55
currently lose_sum: 89.47286230325699
time_elpased: 1.874
batch start
#iterations: 56
currently lose_sum: 89.93325501680374
time_elpased: 1.869
batch start
#iterations: 57
currently lose_sum: 88.95176577568054
time_elpased: 1.862
batch start
#iterations: 58
currently lose_sum: 89.44293707609177
time_elpased: 1.874
batch start
#iterations: 59
currently lose_sum: 88.96302235126495
time_elpased: 1.892
start validation test
0.614020618557
0.629698591877
0.556859112895
0.591043145822
0.614120974396
63.888
batch start
#iterations: 60
currently lose_sum: 89.84731048345566
time_elpased: 1.86
batch start
#iterations: 61
currently lose_sum: 89.06764680147171
time_elpased: 1.878
batch start
#iterations: 62
currently lose_sum: 88.87658655643463
time_elpased: 1.909
batch start
#iterations: 63
currently lose_sum: 88.9184980392456
time_elpased: 1.855
batch start
#iterations: 64
currently lose_sum: 89.43633621931076
time_elpased: 1.877
batch start
#iterations: 65
currently lose_sum: 88.87260001897812
time_elpased: 1.863
batch start
#iterations: 66
currently lose_sum: 87.90183162689209
time_elpased: 1.891
batch start
#iterations: 67
currently lose_sum: 88.00463390350342
time_elpased: 1.892
batch start
#iterations: 68
currently lose_sum: 88.76142060756683
time_elpased: 1.867
batch start
#iterations: 69
currently lose_sum: 88.55581766366959
time_elpased: 1.893
batch start
#iterations: 70
currently lose_sum: 88.23141729831696
time_elpased: 1.878
batch start
#iterations: 71
currently lose_sum: 87.97834348678589
time_elpased: 1.855
batch start
#iterations: 72
currently lose_sum: 87.38668763637543
time_elpased: 1.866
batch start
#iterations: 73
currently lose_sum: 87.81750011444092
time_elpased: 1.899
batch start
#iterations: 74
currently lose_sum: 87.86444157361984
time_elpased: 1.89
batch start
#iterations: 75
currently lose_sum: 87.8795907497406
time_elpased: 1.923
batch start
#iterations: 76
currently lose_sum: 87.77676218748093
time_elpased: 1.902
batch start
#iterations: 77
currently lose_sum: 87.61669737100601
time_elpased: 1.905
batch start
#iterations: 78
currently lose_sum: 87.39552664756775
time_elpased: 1.884
batch start
#iterations: 79
currently lose_sum: 87.46177697181702
time_elpased: 1.882
start validation test
0.613711340206
0.640252365931
0.52217762684
0.575218229226
0.613872041758
64.347
batch start
#iterations: 80
currently lose_sum: 87.21095442771912
time_elpased: 1.91
batch start
#iterations: 81
currently lose_sum: 88.04654949903488
time_elpased: 1.887
batch start
#iterations: 82
currently lose_sum: 87.26832664012909
time_elpased: 1.88
batch start
#iterations: 83
currently lose_sum: 87.42989957332611
time_elpased: 1.881
batch start
#iterations: 84
currently lose_sum: 87.28670960664749
time_elpased: 1.885
batch start
#iterations: 85
currently lose_sum: 87.10588723421097
time_elpased: 1.892
batch start
#iterations: 86
currently lose_sum: 87.07257676124573
time_elpased: 1.903
batch start
#iterations: 87
currently lose_sum: 86.89703512191772
time_elpased: 1.897
batch start
#iterations: 88
currently lose_sum: 87.00713270902634
time_elpased: 1.874
batch start
#iterations: 89
currently lose_sum: 86.5371156334877
time_elpased: 1.881
batch start
#iterations: 90
currently lose_sum: 86.77058905363083
time_elpased: 1.894
batch start
#iterations: 91
currently lose_sum: 86.43682503700256
time_elpased: 1.864
batch start
#iterations: 92
currently lose_sum: 86.1117296218872
time_elpased: 1.894
batch start
#iterations: 93
currently lose_sum: 86.20348453521729
time_elpased: 1.881
batch start
#iterations: 94
currently lose_sum: 86.61221021413803
time_elpased: 1.894
batch start
#iterations: 95
currently lose_sum: 86.65290731191635
time_elpased: 1.88
batch start
#iterations: 96
currently lose_sum: 85.94829136133194
time_elpased: 1.895
batch start
#iterations: 97
currently lose_sum: 86.20425820350647
time_elpased: 1.87
batch start
#iterations: 98
currently lose_sum: 85.92451453208923
time_elpased: 1.922
batch start
#iterations: 99
currently lose_sum: 86.09475100040436
time_elpased: 1.868
start validation test
0.607835051546
0.633497911128
0.514973757333
0.568119891008
0.607998083871
65.533
batch start
#iterations: 100
currently lose_sum: 86.20573616027832
time_elpased: 1.884
batch start
#iterations: 101
currently lose_sum: 86.3807732462883
time_elpased: 1.86
batch start
#iterations: 102
currently lose_sum: 86.2302576303482
time_elpased: 1.901
batch start
#iterations: 103
currently lose_sum: 86.00201380252838
time_elpased: 1.901
batch start
#iterations: 104
currently lose_sum: 85.4902001619339
time_elpased: 1.884
batch start
#iterations: 105
currently lose_sum: 85.59629613161087
time_elpased: 1.847
batch start
#iterations: 106
currently lose_sum: 85.80997908115387
time_elpased: 1.921
batch start
#iterations: 107
currently lose_sum: 84.71297734975815
time_elpased: 1.862
batch start
#iterations: 108
currently lose_sum: 85.61872112751007
time_elpased: 1.886
batch start
#iterations: 109
currently lose_sum: 85.32581096887589
time_elpased: 1.88
batch start
#iterations: 110
currently lose_sum: 85.93794405460358
time_elpased: 1.886
batch start
#iterations: 111
currently lose_sum: 85.67233204841614
time_elpased: 1.889
batch start
#iterations: 112
currently lose_sum: 84.83282834291458
time_elpased: 1.883
batch start
#iterations: 113
currently lose_sum: 84.93626606464386
time_elpased: 1.867
batch start
#iterations: 114
currently lose_sum: 85.3294438123703
time_elpased: 1.86
batch start
#iterations: 115
currently lose_sum: 85.08909517526627
time_elpased: 1.877
batch start
#iterations: 116
currently lose_sum: 84.99183568358421
time_elpased: 1.892
batch start
#iterations: 117
currently lose_sum: 85.37720173597336
time_elpased: 1.861
batch start
#iterations: 118
currently lose_sum: 85.06139606237411
time_elpased: 1.88
batch start
#iterations: 119
currently lose_sum: 84.54378420114517
time_elpased: 1.905
start validation test
0.60618556701
0.634922697155
0.502933004014
0.561272539336
0.60636684281
65.938
batch start
#iterations: 120
currently lose_sum: 84.68801307678223
time_elpased: 1.908
batch start
#iterations: 121
currently lose_sum: 85.08994317054749
time_elpased: 1.884
batch start
#iterations: 122
currently lose_sum: 84.55325543880463
time_elpased: 1.891
batch start
#iterations: 123
currently lose_sum: 85.07203859090805
time_elpased: 1.856
batch start
#iterations: 124
currently lose_sum: 85.12960839271545
time_elpased: 1.887
batch start
#iterations: 125
currently lose_sum: 84.82684922218323
time_elpased: 1.881
batch start
#iterations: 126
currently lose_sum: 84.88049465417862
time_elpased: 1.885
batch start
#iterations: 127
currently lose_sum: 84.09498316049576
time_elpased: 1.941
batch start
#iterations: 128
currently lose_sum: 84.47957170009613
time_elpased: 1.868
batch start
#iterations: 129
currently lose_sum: 84.27397048473358
time_elpased: 1.877
batch start
#iterations: 130
currently lose_sum: 84.27814602851868
time_elpased: 1.879
batch start
#iterations: 131
currently lose_sum: 84.4998225569725
time_elpased: 1.854
batch start
#iterations: 132
currently lose_sum: 83.87711077928543
time_elpased: 1.886
batch start
#iterations: 133
currently lose_sum: 84.34258896112442
time_elpased: 1.889
batch start
#iterations: 134
currently lose_sum: 83.74990180134773
time_elpased: 1.889
batch start
#iterations: 135
currently lose_sum: 84.17457258701324
time_elpased: 1.856
batch start
#iterations: 136
currently lose_sum: 84.43726927042007
time_elpased: 1.867
batch start
#iterations: 137
currently lose_sum: 83.7277507185936
time_elpased: 1.86
batch start
#iterations: 138
currently lose_sum: 83.80213701725006
time_elpased: 1.954
batch start
#iterations: 139
currently lose_sum: 84.42376285791397
time_elpased: 1.863
start validation test
0.59881443299
0.644328358209
0.444272923742
0.525918255467
0.599085754446
67.589
batch start
#iterations: 140
currently lose_sum: 84.16820269823074
time_elpased: 1.925
batch start
#iterations: 141
currently lose_sum: 84.35503455996513
time_elpased: 1.891
batch start
#iterations: 142
currently lose_sum: 83.22224748134613
time_elpased: 1.895
batch start
#iterations: 143
currently lose_sum: 83.71551430225372
time_elpased: 1.868
batch start
#iterations: 144
currently lose_sum: 83.21552729606628
time_elpased: 1.911
batch start
#iterations: 145
currently lose_sum: 83.89895635843277
time_elpased: 1.871
batch start
#iterations: 146
currently lose_sum: 83.5048770904541
time_elpased: 1.875
batch start
#iterations: 147
currently lose_sum: 83.40653747320175
time_elpased: 1.876
batch start
#iterations: 148
currently lose_sum: 83.53363013267517
time_elpased: 1.889
batch start
#iterations: 149
currently lose_sum: 84.12108999490738
time_elpased: 1.871
batch start
#iterations: 150
currently lose_sum: 83.61095637083054
time_elpased: 1.866
batch start
#iterations: 151
currently lose_sum: 83.70760136842728
time_elpased: 1.855
batch start
#iterations: 152
currently lose_sum: 83.71462959051132
time_elpased: 1.872
batch start
#iterations: 153
currently lose_sum: 83.9622882604599
time_elpased: 1.851
batch start
#iterations: 154
currently lose_sum: 83.28575330972672
time_elpased: 1.86
batch start
#iterations: 155
currently lose_sum: 83.23811793327332
time_elpased: 1.87
batch start
#iterations: 156
currently lose_sum: 82.5912914276123
time_elpased: 1.885
batch start
#iterations: 157
currently lose_sum: 83.127869784832
time_elpased: 1.922
batch start
#iterations: 158
currently lose_sum: 83.9764524102211
time_elpased: 1.85
batch start
#iterations: 159
currently lose_sum: 83.54781463742256
time_elpased: 1.859
start validation test
0.595
0.638599105812
0.440979726253
0.521702075851
0.595270406347
68.867
batch start
#iterations: 160
currently lose_sum: 83.69189655780792
time_elpased: 1.965
batch start
#iterations: 161
currently lose_sum: 82.756537348032
time_elpased: 1.906
batch start
#iterations: 162
currently lose_sum: 83.06517791748047
time_elpased: 1.861
batch start
#iterations: 163
currently lose_sum: 82.84929090738297
time_elpased: 1.885
batch start
#iterations: 164
currently lose_sum: 83.26050412654877
time_elpased: 1.872
batch start
#iterations: 165
currently lose_sum: 83.33028656244278
time_elpased: 1.881
batch start
#iterations: 166
currently lose_sum: 82.7438077032566
time_elpased: 1.883
batch start
#iterations: 167
currently lose_sum: 82.47345480322838
time_elpased: 1.866
batch start
#iterations: 168
currently lose_sum: 83.3059087395668
time_elpased: 1.866
batch start
#iterations: 169
currently lose_sum: 83.09581664204597
time_elpased: 1.907
batch start
#iterations: 170
currently lose_sum: 83.12191280722618
time_elpased: 1.855
batch start
#iterations: 171
currently lose_sum: 82.88488602638245
time_elpased: 1.891
batch start
#iterations: 172
currently lose_sum: 82.49656412005424
time_elpased: 1.857
batch start
#iterations: 173
currently lose_sum: 82.80737471580505
time_elpased: 1.881
batch start
#iterations: 174
currently lose_sum: 82.3238474726677
time_elpased: 1.888
batch start
#iterations: 175
currently lose_sum: 83.06313222646713
time_elpased: 1.865
batch start
#iterations: 176
currently lose_sum: 83.09700131416321
time_elpased: 1.884
batch start
#iterations: 177
currently lose_sum: 82.93823835253716
time_elpased: 1.877
batch start
#iterations: 178
currently lose_sum: 81.8672282397747
time_elpased: 1.86
batch start
#iterations: 179
currently lose_sum: 82.45549261569977
time_elpased: 1.873
start validation test
0.603453608247
0.637612183846
0.482556344551
0.549352703415
0.603665862041
68.869
batch start
#iterations: 180
currently lose_sum: 83.10512113571167
time_elpased: 1.901
batch start
#iterations: 181
currently lose_sum: 81.51827561855316
time_elpased: 1.874
batch start
#iterations: 182
currently lose_sum: 82.88564044237137
time_elpased: 1.93
batch start
#iterations: 183
currently lose_sum: 82.38782787322998
time_elpased: 1.86
batch start
#iterations: 184
currently lose_sum: 81.94999808073044
time_elpased: 1.884
batch start
#iterations: 185
currently lose_sum: 82.42210701107979
time_elpased: 1.89
batch start
#iterations: 186
currently lose_sum: 81.6952158510685
time_elpased: 1.864
batch start
#iterations: 187
currently lose_sum: 81.88631004095078
time_elpased: 1.883
batch start
#iterations: 188
currently lose_sum: 82.0484949350357
time_elpased: 1.894
batch start
#iterations: 189
currently lose_sum: 82.20294278860092
time_elpased: 1.906
batch start
#iterations: 190
currently lose_sum: 81.75612491369247
time_elpased: 1.892
batch start
#iterations: 191
currently lose_sum: 81.25352638959885
time_elpased: 1.87
batch start
#iterations: 192
currently lose_sum: 81.92483234405518
time_elpased: 1.893
batch start
#iterations: 193
currently lose_sum: 82.09370070695877
time_elpased: 1.866
batch start
#iterations: 194
currently lose_sum: 81.50137835741043
time_elpased: 1.894
batch start
#iterations: 195
currently lose_sum: 82.40536516904831
time_elpased: 1.883
batch start
#iterations: 196
currently lose_sum: 82.09356313943863
time_elpased: 1.894
batch start
#iterations: 197
currently lose_sum: 81.91482701897621
time_elpased: 1.888
batch start
#iterations: 198
currently lose_sum: 81.80477544665337
time_elpased: 1.881
batch start
#iterations: 199
currently lose_sum: 82.32616573572159
time_elpased: 1.889
start validation test
0.590773195876
0.639385387269
0.419676854996
0.506741223983
0.591073581892
69.886
batch start
#iterations: 200
currently lose_sum: 82.41569516062737
time_elpased: 1.905
batch start
#iterations: 201
currently lose_sum: 81.30026704072952
time_elpased: 1.886
batch start
#iterations: 202
currently lose_sum: 81.66651791334152
time_elpased: 1.886
batch start
#iterations: 203
currently lose_sum: 81.8415145277977
time_elpased: 1.864
batch start
#iterations: 204
currently lose_sum: 81.7839951813221
time_elpased: 1.9
batch start
#iterations: 205
currently lose_sum: 81.66792857646942
time_elpased: 1.866
batch start
#iterations: 206
currently lose_sum: 81.60890299081802
time_elpased: 1.877
batch start
#iterations: 207
currently lose_sum: 81.330884963274
time_elpased: 1.841
batch start
#iterations: 208
currently lose_sum: 81.42883437871933
time_elpased: 1.884
batch start
#iterations: 209
currently lose_sum: 81.76591575145721
time_elpased: 1.878
batch start
#iterations: 210
currently lose_sum: 81.05582427978516
time_elpased: 1.9
batch start
#iterations: 211
currently lose_sum: 81.13313123583794
time_elpased: 1.881
batch start
#iterations: 212
currently lose_sum: 81.2381374835968
time_elpased: 1.901
batch start
#iterations: 213
currently lose_sum: 81.12528732419014
time_elpased: 1.874
batch start
#iterations: 214
currently lose_sum: 80.91138452291489
time_elpased: 1.89
batch start
#iterations: 215
currently lose_sum: 81.12584587931633
time_elpased: 1.947
batch start
#iterations: 216
currently lose_sum: 81.05750036239624
time_elpased: 1.91
batch start
#iterations: 217
currently lose_sum: 81.24607175588608
time_elpased: 1.881
batch start
#iterations: 218
currently lose_sum: 81.18386521935463
time_elpased: 1.881
batch start
#iterations: 219
currently lose_sum: 81.13938960433006
time_elpased: 1.859
start validation test
0.600360824742
0.631003201708
0.486775753833
0.549584616278
0.600560240854
69.631
batch start
#iterations: 220
currently lose_sum: 81.69453990459442
time_elpased: 1.884
batch start
#iterations: 221
currently lose_sum: 81.32597076892853
time_elpased: 1.869
batch start
#iterations: 222
currently lose_sum: 81.35604172945023
time_elpased: 1.897
batch start
#iterations: 223
currently lose_sum: 80.75951358675957
time_elpased: 1.876
batch start
#iterations: 224
currently lose_sum: 81.26343643665314
time_elpased: 1.908
batch start
#iterations: 225
currently lose_sum: 81.21558356285095
time_elpased: 1.897
batch start
#iterations: 226
currently lose_sum: 81.12525573372841
time_elpased: 1.861
batch start
#iterations: 227
currently lose_sum: 80.90236559510231
time_elpased: 1.864
batch start
#iterations: 228
currently lose_sum: 80.98406168818474
time_elpased: 1.888
batch start
#iterations: 229
currently lose_sum: 80.56396049261093
time_elpased: 1.845
batch start
#iterations: 230
currently lose_sum: 80.63466107845306
time_elpased: 1.873
batch start
#iterations: 231
currently lose_sum: 80.48332142829895
time_elpased: 1.9
batch start
#iterations: 232
currently lose_sum: 80.67246267199516
time_elpased: 1.864
batch start
#iterations: 233
currently lose_sum: 80.07271239161491
time_elpased: 1.886
batch start
#iterations: 234
currently lose_sum: 81.17669701576233
time_elpased: 1.89
batch start
#iterations: 235
currently lose_sum: 80.39678040146828
time_elpased: 1.874
batch start
#iterations: 236
currently lose_sum: 80.33165553212166
time_elpased: 1.886
batch start
#iterations: 237
currently lose_sum: 80.7633236348629
time_elpased: 1.911
batch start
#iterations: 238
currently lose_sum: 80.18008208274841
time_elpased: 1.88
batch start
#iterations: 239
currently lose_sum: 79.97020247578621
time_elpased: 1.957
start validation test
0.598556701031
0.638557678494
0.457445713698
0.533037534477
0.598804443134
71.096
batch start
#iterations: 240
currently lose_sum: 81.09082221984863
time_elpased: 1.875
batch start
#iterations: 241
currently lose_sum: 80.40556854009628
time_elpased: 1.871
batch start
#iterations: 242
currently lose_sum: 80.16103249788284
time_elpased: 1.887
batch start
#iterations: 243
currently lose_sum: 80.34604135155678
time_elpased: 1.869
batch start
#iterations: 244
currently lose_sum: 81.00120708346367
time_elpased: 1.88
batch start
#iterations: 245
currently lose_sum: 80.74145025014877
time_elpased: 1.865
batch start
#iterations: 246
currently lose_sum: 80.35069468617439
time_elpased: 1.912
batch start
#iterations: 247
currently lose_sum: 80.52110040187836
time_elpased: 1.841
batch start
#iterations: 248
currently lose_sum: 80.73000612854958
time_elpased: 1.892
batch start
#iterations: 249
currently lose_sum: 80.42160451412201
time_elpased: 1.87
batch start
#iterations: 250
currently lose_sum: 80.35120403766632
time_elpased: 1.981
batch start
#iterations: 251
currently lose_sum: 79.98771077394485
time_elpased: 1.894
batch start
#iterations: 252
currently lose_sum: 79.88543671369553
time_elpased: 1.924
batch start
#iterations: 253
currently lose_sum: 80.65947729349136
time_elpased: 1.882
batch start
#iterations: 254
currently lose_sum: 80.32485231757164
time_elpased: 1.904
batch start
#iterations: 255
currently lose_sum: 79.8988707959652
time_elpased: 1.876
batch start
#iterations: 256
currently lose_sum: 79.84115955233574
time_elpased: 1.867
batch start
#iterations: 257
currently lose_sum: 80.31487706303596
time_elpased: 1.862
batch start
#iterations: 258
currently lose_sum: 79.99816688895226
time_elpased: 1.874
batch start
#iterations: 259
currently lose_sum: 79.49098438024521
time_elpased: 1.874
start validation test
0.597371134021
0.639736070381
0.449006895132
0.527665235532
0.597631610326
71.364
batch start
#iterations: 260
currently lose_sum: 79.44053879380226
time_elpased: 1.926
batch start
#iterations: 261
currently lose_sum: 79.75562804937363
time_elpased: 1.875
batch start
#iterations: 262
currently lose_sum: 80.1374845802784
time_elpased: 1.874
batch start
#iterations: 263
currently lose_sum: 79.69714632630348
time_elpased: 1.866
batch start
#iterations: 264
currently lose_sum: 79.62362095713615
time_elpased: 1.857
batch start
#iterations: 265
currently lose_sum: 79.93914806842804
time_elpased: 1.846
batch start
#iterations: 266
currently lose_sum: 80.39670872688293
time_elpased: 1.889
batch start
#iterations: 267
currently lose_sum: 79.98654022812843
time_elpased: 1.925
batch start
#iterations: 268
currently lose_sum: 80.09964063763618
time_elpased: 1.89
batch start
#iterations: 269
currently lose_sum: 80.00486841797829
time_elpased: 1.874
batch start
#iterations: 270
currently lose_sum: 80.17415538430214
time_elpased: 1.881
batch start
#iterations: 271
currently lose_sum: 79.83179125189781
time_elpased: 1.882
batch start
#iterations: 272
currently lose_sum: 79.60902890563011
time_elpased: 1.882
batch start
#iterations: 273
currently lose_sum: 80.33179771900177
time_elpased: 1.898
batch start
#iterations: 274
currently lose_sum: 79.38329210877419
time_elpased: 1.866
batch start
#iterations: 275
currently lose_sum: 79.99060133099556
time_elpased: 1.888
batch start
#iterations: 276
currently lose_sum: 79.68334206938744
time_elpased: 1.871
batch start
#iterations: 277
currently lose_sum: 80.59050440788269
time_elpased: 1.872
batch start
#iterations: 278
currently lose_sum: 79.78534796833992
time_elpased: 1.867
batch start
#iterations: 279
currently lose_sum: 80.21864876151085
time_elpased: 1.866
start validation test
0.598298969072
0.627993613624
0.485746629618
0.547786224105
0.598496572064
70.312
batch start
#iterations: 280
currently lose_sum: 79.87235015630722
time_elpased: 1.849
batch start
#iterations: 281
currently lose_sum: 79.55009511113167
time_elpased: 1.85
batch start
#iterations: 282
currently lose_sum: 79.9637581706047
time_elpased: 1.878
batch start
#iterations: 283
currently lose_sum: 79.38660454750061
time_elpased: 1.879
batch start
#iterations: 284
currently lose_sum: 79.62876716256142
time_elpased: 1.889
batch start
#iterations: 285
currently lose_sum: 78.94956907629967
time_elpased: 1.871
batch start
#iterations: 286
currently lose_sum: 79.27511832118034
time_elpased: 1.852
batch start
#iterations: 287
currently lose_sum: 79.52920839190483
time_elpased: 1.863
batch start
#iterations: 288
currently lose_sum: 79.4143922328949
time_elpased: 1.846
batch start
#iterations: 289
currently lose_sum: 79.29761016368866
time_elpased: 1.891
batch start
#iterations: 290
currently lose_sum: 79.37602722644806
time_elpased: 1.871
batch start
#iterations: 291
currently lose_sum: 79.75715872645378
time_elpased: 1.858
batch start
#iterations: 292
currently lose_sum: 79.44373294711113
time_elpased: 1.9
batch start
#iterations: 293
currently lose_sum: 78.96343633532524
time_elpased: 1.884
batch start
#iterations: 294
currently lose_sum: 78.83963117003441
time_elpased: 1.892
batch start
#iterations: 295
currently lose_sum: 79.67687213420868
time_elpased: 1.862
batch start
#iterations: 296
currently lose_sum: 78.56325635313988
time_elpased: 1.867
batch start
#iterations: 297
currently lose_sum: 79.20207706093788
time_elpased: 1.859
batch start
#iterations: 298
currently lose_sum: 79.12179347872734
time_elpased: 1.887
batch start
#iterations: 299
currently lose_sum: 78.27044141292572
time_elpased: 1.848
start validation test
0.599793814433
0.638845442912
0.462385509931
0.53647761194
0.600035055905
72.211
batch start
#iterations: 300
currently lose_sum: 79.10461392998695
time_elpased: 1.894
batch start
#iterations: 301
currently lose_sum: 79.34979379177094
time_elpased: 1.857
batch start
#iterations: 302
currently lose_sum: 79.31735461950302
time_elpased: 1.892
batch start
#iterations: 303
currently lose_sum: 78.93963861465454
time_elpased: 1.853
batch start
#iterations: 304
currently lose_sum: 78.91121408343315
time_elpased: 1.874
batch start
#iterations: 305
currently lose_sum: 79.22284489870071
time_elpased: 1.88
batch start
#iterations: 306
currently lose_sum: 79.69402331113815
time_elpased: 1.859
batch start
#iterations: 307
currently lose_sum: 79.11368054151535
time_elpased: 1.87
batch start
#iterations: 308
currently lose_sum: 78.9362361729145
time_elpased: 1.882
batch start
#iterations: 309
currently lose_sum: 78.61902552843094
time_elpased: 1.891
batch start
#iterations: 310
currently lose_sum: 79.12116453051567
time_elpased: 1.901
batch start
#iterations: 311
currently lose_sum: 78.74613744020462
time_elpased: 1.883
batch start
#iterations: 312
currently lose_sum: 78.48374420404434
time_elpased: 1.869
batch start
#iterations: 313
currently lose_sum: 78.21199172735214
time_elpased: 1.868
batch start
#iterations: 314
currently lose_sum: 78.80896198749542
time_elpased: 1.874
batch start
#iterations: 315
currently lose_sum: 78.96481162309647
time_elpased: 1.878
batch start
#iterations: 316
currently lose_sum: 78.78166827559471
time_elpased: 1.902
batch start
#iterations: 317
currently lose_sum: 78.78519248962402
time_elpased: 1.91
batch start
#iterations: 318
currently lose_sum: 78.45241448283195
time_elpased: 1.863
batch start
#iterations: 319
currently lose_sum: 78.58686146140099
time_elpased: 1.883
start validation test
0.599072164948
0.631421987258
0.479366059483
0.544986544987
0.59928232748
72.508
batch start
#iterations: 320
currently lose_sum: 78.23210707306862
time_elpased: 1.886
batch start
#iterations: 321
currently lose_sum: 79.47314509749413
time_elpased: 1.903
batch start
#iterations: 322
currently lose_sum: 78.80501213669777
time_elpased: 1.856
batch start
#iterations: 323
currently lose_sum: 78.7596530020237
time_elpased: 1.871
batch start
#iterations: 324
currently lose_sum: 79.10561662912369
time_elpased: 1.864
batch start
#iterations: 325
currently lose_sum: 79.02755498886108
time_elpased: 1.879
batch start
#iterations: 326
currently lose_sum: 78.91759204864502
time_elpased: 1.885
batch start
#iterations: 327
currently lose_sum: 78.59473726153374
time_elpased: 1.859
batch start
#iterations: 328
currently lose_sum: 78.8647172152996
time_elpased: 1.878
batch start
#iterations: 329
currently lose_sum: 78.13063904643059
time_elpased: 1.889
batch start
#iterations: 330
currently lose_sum: 78.50902834534645
time_elpased: 1.864
batch start
#iterations: 331
currently lose_sum: 78.41017878055573
time_elpased: 1.874
batch start
#iterations: 332
currently lose_sum: 77.77943021059036
time_elpased: 1.916
batch start
#iterations: 333
currently lose_sum: 78.36599543690681
time_elpased: 1.876
batch start
#iterations: 334
currently lose_sum: 78.36781698465347
time_elpased: 1.88
batch start
#iterations: 335
currently lose_sum: 78.37340745329857
time_elpased: 1.898
batch start
#iterations: 336
currently lose_sum: 78.50598666071892
time_elpased: 1.894
batch start
#iterations: 337
currently lose_sum: 78.80235415697098
time_elpased: 1.862
batch start
#iterations: 338
currently lose_sum: 78.52666679024696
time_elpased: 1.868
batch start
#iterations: 339
currently lose_sum: 77.8021784722805
time_elpased: 1.892
start validation test
0.597010309278
0.629342051492
0.475455387465
0.541681322547
0.597223717692
72.992
batch start
#iterations: 340
currently lose_sum: 78.3234603703022
time_elpased: 1.879
batch start
#iterations: 341
currently lose_sum: 78.51783558726311
time_elpased: 1.867
batch start
#iterations: 342
currently lose_sum: 78.20197865366936
time_elpased: 1.868
batch start
#iterations: 343
currently lose_sum: 78.6765148639679
time_elpased: 1.887
batch start
#iterations: 344
currently lose_sum: 77.78820857405663
time_elpased: 1.886
batch start
#iterations: 345
currently lose_sum: 78.5334820151329
time_elpased: 1.904
batch start
#iterations: 346
currently lose_sum: 77.97900077700615
time_elpased: 1.867
batch start
#iterations: 347
currently lose_sum: 78.6909930408001
time_elpased: 1.868
batch start
#iterations: 348
currently lose_sum: 77.75776079297066
time_elpased: 1.876
batch start
#iterations: 349
currently lose_sum: 78.37896743416786
time_elpased: 1.891
batch start
#iterations: 350
currently lose_sum: 77.6631708741188
time_elpased: 1.87
batch start
#iterations: 351
currently lose_sum: 78.254963606596
time_elpased: 1.868
batch start
#iterations: 352
currently lose_sum: 78.09848472476006
time_elpased: 1.878
batch start
#iterations: 353
currently lose_sum: 79.1996408700943
time_elpased: 1.957
batch start
#iterations: 354
currently lose_sum: 78.5797169804573
time_elpased: 1.862
batch start
#iterations: 355
currently lose_sum: 78.81423884630203
time_elpased: 1.865
batch start
#iterations: 356
currently lose_sum: 77.9229443371296
time_elpased: 1.909
batch start
#iterations: 357
currently lose_sum: 77.74867326021194
time_elpased: 1.912
batch start
#iterations: 358
currently lose_sum: 78.66889464855194
time_elpased: 1.879
batch start
#iterations: 359
currently lose_sum: 77.72059866786003
time_elpased: 1.886
start validation test
0.598711340206
0.633314932376
0.472265102398
0.541059954018
0.59893333608
73.491
batch start
#iterations: 360
currently lose_sum: 77.34240731596947
time_elpased: 1.858
batch start
#iterations: 361
currently lose_sum: 78.36822617053986
time_elpased: 1.905
batch start
#iterations: 362
currently lose_sum: 77.69374084472656
time_elpased: 1.877
batch start
#iterations: 363
currently lose_sum: 77.98541337251663
time_elpased: 1.883
batch start
#iterations: 364
currently lose_sum: 78.08633187413216
time_elpased: 1.885
batch start
#iterations: 365
currently lose_sum: 78.13077488541603
time_elpased: 1.9
batch start
#iterations: 366
currently lose_sum: 77.30556592345238
time_elpased: 1.874
batch start
#iterations: 367
currently lose_sum: 77.89661654829979
time_elpased: 1.904
batch start
#iterations: 368
currently lose_sum: 77.56836771965027
time_elpased: 1.863
batch start
#iterations: 369
currently lose_sum: 78.01422625780106
time_elpased: 1.903
batch start
#iterations: 370
currently lose_sum: 77.02355164289474
time_elpased: 1.889
batch start
#iterations: 371
currently lose_sum: 77.55084505677223
time_elpased: 1.909
batch start
#iterations: 372
currently lose_sum: 77.4634730219841
time_elpased: 1.882
batch start
#iterations: 373
currently lose_sum: 78.16193926334381
time_elpased: 1.87
batch start
#iterations: 374
currently lose_sum: 77.67973184585571
time_elpased: 1.914
batch start
#iterations: 375
currently lose_sum: 77.75634783506393
time_elpased: 1.874
batch start
#iterations: 376
currently lose_sum: 77.0909626185894
time_elpased: 1.896
batch start
#iterations: 377
currently lose_sum: 77.8633460700512
time_elpased: 1.898
batch start
#iterations: 378
currently lose_sum: 77.57140246033669
time_elpased: 1.892
batch start
#iterations: 379
currently lose_sum: 78.09290081262589
time_elpased: 1.874
start validation test
0.601288659794
0.630257623554
0.493465061233
0.553535353535
0.601477960752
72.537
batch start
#iterations: 380
currently lose_sum: 76.96613547205925
time_elpased: 1.921
batch start
#iterations: 381
currently lose_sum: 76.49100083112717
time_elpased: 1.882
batch start
#iterations: 382
currently lose_sum: 77.34867709875107
time_elpased: 1.865
batch start
#iterations: 383
currently lose_sum: 77.78642678260803
time_elpased: 1.896
batch start
#iterations: 384
currently lose_sum: 77.41784545779228
time_elpased: 1.891
batch start
#iterations: 385
currently lose_sum: 77.08427548408508
time_elpased: 1.913
batch start
#iterations: 386
currently lose_sum: 77.88327798247337
time_elpased: 1.903
batch start
#iterations: 387
currently lose_sum: 78.18643942475319
time_elpased: 1.883
batch start
#iterations: 388
currently lose_sum: 77.03218141198158
time_elpased: 1.905
batch start
#iterations: 389
currently lose_sum: 78.18779543042183
time_elpased: 1.904
batch start
#iterations: 390
currently lose_sum: 77.3436678647995
time_elpased: 1.884
batch start
#iterations: 391
currently lose_sum: 78.83615228533745
time_elpased: 1.885
batch start
#iterations: 392
currently lose_sum: 77.53140687942505
time_elpased: 1.867
batch start
#iterations: 393
currently lose_sum: 77.31313017010689
time_elpased: 1.886
batch start
#iterations: 394
currently lose_sum: 77.2590759396553
time_elpased: 1.898
batch start
#iterations: 395
currently lose_sum: 77.3505000770092
time_elpased: 1.932
batch start
#iterations: 396
currently lose_sum: 77.4100689291954
time_elpased: 1.896
batch start
#iterations: 397
currently lose_sum: 77.47527369856834
time_elpased: 1.913
batch start
#iterations: 398
currently lose_sum: 76.91645476222038
time_elpased: 1.891
batch start
#iterations: 399
currently lose_sum: 77.58799889683723
time_elpased: 1.864
start validation test
0.592371134021
0.629306647605
0.453020479572
0.52680708473
0.592615785588
73.885
acc: 0.615
pre: 0.645
rec: 0.515
F1: 0.573
auc: 0.615
