start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.33397859334946
time_elpased: 1.888
batch start
#iterations: 1
currently lose_sum: 99.85766649246216
time_elpased: 1.784
batch start
#iterations: 2
currently lose_sum: 99.47815722227097
time_elpased: 1.786
batch start
#iterations: 3
currently lose_sum: 98.88771015405655
time_elpased: 1.852
batch start
#iterations: 4
currently lose_sum: 98.71252363920212
time_elpased: 1.845
batch start
#iterations: 5
currently lose_sum: 98.31960213184357
time_elpased: 1.878
batch start
#iterations: 6
currently lose_sum: 97.9163675904274
time_elpased: 1.897
batch start
#iterations: 7
currently lose_sum: 97.71750831604004
time_elpased: 1.92
batch start
#iterations: 8
currently lose_sum: 97.50448995828629
time_elpased: 1.896
batch start
#iterations: 9
currently lose_sum: 97.1552545428276
time_elpased: 1.917
batch start
#iterations: 10
currently lose_sum: 96.76398319005966
time_elpased: 1.891
batch start
#iterations: 11
currently lose_sum: 96.55227941274643
time_elpased: 1.905
batch start
#iterations: 12
currently lose_sum: 96.2471449971199
time_elpased: 1.928
batch start
#iterations: 13
currently lose_sum: 96.59601426124573
time_elpased: 1.892
batch start
#iterations: 14
currently lose_sum: 96.0174605846405
time_elpased: 1.892
batch start
#iterations: 15
currently lose_sum: 95.8131976723671
time_elpased: 1.929
batch start
#iterations: 16
currently lose_sum: 95.72090369462967
time_elpased: 1.888
batch start
#iterations: 17
currently lose_sum: 95.96470075845718
time_elpased: 1.885
batch start
#iterations: 18
currently lose_sum: 95.38519847393036
time_elpased: 1.897
batch start
#iterations: 19
currently lose_sum: 94.96864008903503
time_elpased: 1.921
start validation test
0.632164948454
0.651124648547
0.57204610951
0.609029147491
0.63226427739
62.314
batch start
#iterations: 20
currently lose_sum: 94.73754572868347
time_elpased: 1.902
batch start
#iterations: 21
currently lose_sum: 94.65677613019943
time_elpased: 1.905
batch start
#iterations: 22
currently lose_sum: 94.80066126585007
time_elpased: 1.866
batch start
#iterations: 23
currently lose_sum: 94.34804946184158
time_elpased: 1.911
batch start
#iterations: 24
currently lose_sum: 94.31519687175751
time_elpased: 1.929
batch start
#iterations: 25
currently lose_sum: 94.09889626502991
time_elpased: 1.882
batch start
#iterations: 26
currently lose_sum: 93.97736304998398
time_elpased: 1.95
batch start
#iterations: 27
currently lose_sum: 93.79389280080795
time_elpased: 1.894
batch start
#iterations: 28
currently lose_sum: 93.4492130279541
time_elpased: 1.909
batch start
#iterations: 29
currently lose_sum: 93.42207467556
time_elpased: 1.896
batch start
#iterations: 30
currently lose_sum: 93.39684402942657
time_elpased: 1.875
batch start
#iterations: 31
currently lose_sum: 92.96869885921478
time_elpased: 1.916
batch start
#iterations: 32
currently lose_sum: 92.89107811450958
time_elpased: 1.876
batch start
#iterations: 33
currently lose_sum: 92.76147127151489
time_elpased: 1.913
batch start
#iterations: 34
currently lose_sum: 92.12139427661896
time_elpased: 1.887
batch start
#iterations: 35
currently lose_sum: 92.93832725286484
time_elpased: 1.927
batch start
#iterations: 36
currently lose_sum: 92.00000607967377
time_elpased: 1.892
batch start
#iterations: 37
currently lose_sum: 91.70472824573517
time_elpased: 1.908
batch start
#iterations: 38
currently lose_sum: 91.65463477373123
time_elpased: 1.905
batch start
#iterations: 39
currently lose_sum: 91.38902455568314
time_elpased: 1.91
start validation test
0.615309278351
0.633140290746
0.551358583779
0.589426197942
0.615414938317
63.086
batch start
#iterations: 40
currently lose_sum: 91.7034124135971
time_elpased: 1.876
batch start
#iterations: 41
currently lose_sum: 91.47849261760712
time_elpased: 1.873
batch start
#iterations: 42
currently lose_sum: 91.47514021396637
time_elpased: 1.89
batch start
#iterations: 43
currently lose_sum: 91.10520374774933
time_elpased: 1.871
batch start
#iterations: 44
currently lose_sum: 90.91834139823914
time_elpased: 1.894
batch start
#iterations: 45
currently lose_sum: 90.73396515846252
time_elpased: 1.887
batch start
#iterations: 46
currently lose_sum: 90.5057612657547
time_elpased: 1.891
batch start
#iterations: 47
currently lose_sum: 90.05332952737808
time_elpased: 1.904
batch start
#iterations: 48
currently lose_sum: 90.18758809566498
time_elpased: 1.871
batch start
#iterations: 49
currently lose_sum: 90.41961771249771
time_elpased: 1.886
batch start
#iterations: 50
currently lose_sum: 90.19646334648132
time_elpased: 1.874
batch start
#iterations: 51
currently lose_sum: 89.95050251483917
time_elpased: 1.845
batch start
#iterations: 52
currently lose_sum: 90.22460067272186
time_elpased: 1.864
batch start
#iterations: 53
currently lose_sum: 89.5486312508583
time_elpased: 1.879
batch start
#iterations: 54
currently lose_sum: 89.89745479822159
time_elpased: 1.893
batch start
#iterations: 55
currently lose_sum: 89.7714866399765
time_elpased: 1.898
batch start
#iterations: 56
currently lose_sum: 89.53118830919266
time_elpased: 1.883
batch start
#iterations: 57
currently lose_sum: 88.8727667927742
time_elpased: 1.887
batch start
#iterations: 58
currently lose_sum: 89.12140238285065
time_elpased: 1.905
batch start
#iterations: 59
currently lose_sum: 89.89931344985962
time_elpased: 1.899
start validation test
0.611907216495
0.637668387259
0.521305063812
0.57364516677
0.612056910262
64.017
batch start
#iterations: 60
currently lose_sum: 88.94465219974518
time_elpased: 1.94
batch start
#iterations: 61
currently lose_sum: 89.11780488491058
time_elpased: 1.894
batch start
#iterations: 62
currently lose_sum: 89.3945335149765
time_elpased: 1.908
batch start
#iterations: 63
currently lose_sum: 89.02533203363419
time_elpased: 1.924
batch start
#iterations: 64
currently lose_sum: 88.44988334178925
time_elpased: 1.908
batch start
#iterations: 65
currently lose_sum: 88.80954170227051
time_elpased: 1.904
batch start
#iterations: 66
currently lose_sum: 88.64368742704391
time_elpased: 1.913
batch start
#iterations: 67
currently lose_sum: 88.51889276504517
time_elpased: 1.884
batch start
#iterations: 68
currently lose_sum: 88.12495934963226
time_elpased: 1.902
batch start
#iterations: 69
currently lose_sum: 87.93018716573715
time_elpased: 1.993
batch start
#iterations: 70
currently lose_sum: 88.00158697366714
time_elpased: 1.997
batch start
#iterations: 71
currently lose_sum: 87.61270135641098
time_elpased: 1.959
batch start
#iterations: 72
currently lose_sum: 87.5466940999031
time_elpased: 2.003
batch start
#iterations: 73
currently lose_sum: 87.55868977308273
time_elpased: 1.976
batch start
#iterations: 74
currently lose_sum: 87.77069449424744
time_elpased: 1.992
batch start
#iterations: 75
currently lose_sum: 87.83102822303772
time_elpased: 2.015
batch start
#iterations: 76
currently lose_sum: 87.83282572031021
time_elpased: 2.003
batch start
#iterations: 77
currently lose_sum: 87.5337085723877
time_elpased: 1.998
batch start
#iterations: 78
currently lose_sum: 87.26955235004425
time_elpased: 2.045
batch start
#iterations: 79
currently lose_sum: 87.13428956270218
time_elpased: 2.08
start validation test
0.608195876289
0.633002138096
0.518011527378
0.569762834663
0.608344879757
65.154
batch start
#iterations: 80
currently lose_sum: 87.33523958921432
time_elpased: 1.996
batch start
#iterations: 81
currently lose_sum: 87.41393041610718
time_elpased: 2.003
batch start
#iterations: 82
currently lose_sum: 87.00996512174606
time_elpased: 2.012
batch start
#iterations: 83
currently lose_sum: 87.51224434375763
time_elpased: 2.037
batch start
#iterations: 84
currently lose_sum: 86.94727516174316
time_elpased: 2.001
batch start
#iterations: 85
currently lose_sum: 86.84954464435577
time_elpased: 2.01
batch start
#iterations: 86
currently lose_sum: 87.407162129879
time_elpased: 2.064
batch start
#iterations: 87
currently lose_sum: 86.26391267776489
time_elpased: 1.97
batch start
#iterations: 88
currently lose_sum: 86.72121465206146
time_elpased: 2.018
batch start
#iterations: 89
currently lose_sum: 86.54848378896713
time_elpased: 2.003
batch start
#iterations: 90
currently lose_sum: 87.11376678943634
time_elpased: 1.988
batch start
#iterations: 91
currently lose_sum: 86.50803661346436
time_elpased: 1.978
batch start
#iterations: 92
currently lose_sum: 86.72734522819519
time_elpased: 1.988
batch start
#iterations: 93
currently lose_sum: 86.18381989002228
time_elpased: 2.001
batch start
#iterations: 94
currently lose_sum: 86.27891397476196
time_elpased: 1.98
batch start
#iterations: 95
currently lose_sum: 86.36824381351471
time_elpased: 1.977
batch start
#iterations: 96
currently lose_sum: 85.91355139017105
time_elpased: 1.977
batch start
#iterations: 97
currently lose_sum: 85.75760620832443
time_elpased: 1.974
batch start
#iterations: 98
currently lose_sum: 86.23864018917084
time_elpased: 1.99
batch start
#iterations: 99
currently lose_sum: 86.21576237678528
time_elpased: 1.972
start validation test
0.605721649485
0.626763154667
0.525936599424
0.571940231686
0.605853471129
65.895
batch start
#iterations: 100
currently lose_sum: 86.49317568540573
time_elpased: 2.038
batch start
#iterations: 101
currently lose_sum: 86.45772087574005
time_elpased: 2.013
batch start
#iterations: 102
currently lose_sum: 86.15317964553833
time_elpased: 1.996
batch start
#iterations: 103
currently lose_sum: 85.80442267656326
time_elpased: 1.982
batch start
#iterations: 104
currently lose_sum: 86.42672920227051
time_elpased: 1.992
batch start
#iterations: 105
currently lose_sum: 85.52804005146027
time_elpased: 1.993
batch start
#iterations: 106
currently lose_sum: 85.58501261472702
time_elpased: 2.024
batch start
#iterations: 107
currently lose_sum: 85.82106512784958
time_elpased: 1.98
batch start
#iterations: 108
currently lose_sum: 85.29044443368912
time_elpased: 2.074
batch start
#iterations: 109
currently lose_sum: 85.5798624753952
time_elpased: 2.007
batch start
#iterations: 110
currently lose_sum: 85.10086059570312
time_elpased: 1.991
batch start
#iterations: 111
currently lose_sum: 85.21108502149582
time_elpased: 1.991
batch start
#iterations: 112
currently lose_sum: 85.26984196901321
time_elpased: 2.005
batch start
#iterations: 113
currently lose_sum: 84.87166684865952
time_elpased: 1.989
batch start
#iterations: 114
currently lose_sum: 85.45969259738922
time_elpased: 2.003
batch start
#iterations: 115
currently lose_sum: 85.2358136177063
time_elpased: 1.978
batch start
#iterations: 116
currently lose_sum: 85.49543535709381
time_elpased: 1.995
batch start
#iterations: 117
currently lose_sum: 85.20419657230377
time_elpased: 1.971
batch start
#iterations: 118
currently lose_sum: 85.06111043691635
time_elpased: 2.008
batch start
#iterations: 119
currently lose_sum: 85.10962969064713
time_elpased: 1.967
start validation test
0.599587628866
0.625483122906
0.499691230959
0.555555555556
0.599752678677
66.970
batch start
#iterations: 120
currently lose_sum: 84.77048343420029
time_elpased: 2.043
batch start
#iterations: 121
currently lose_sum: 85.41936182975769
time_elpased: 2.026
batch start
#iterations: 122
currently lose_sum: 84.6935995221138
time_elpased: 2.037
batch start
#iterations: 123
currently lose_sum: 85.21184515953064
time_elpased: 1.967
batch start
#iterations: 124
currently lose_sum: 85.11352753639221
time_elpased: 2.027
batch start
#iterations: 125
currently lose_sum: 85.04421275854111
time_elpased: 1.969
batch start
#iterations: 126
currently lose_sum: 84.17474526166916
time_elpased: 1.963
batch start
#iterations: 127
currently lose_sum: 84.5987160205841
time_elpased: 2.08
batch start
#iterations: 128
currently lose_sum: 84.46212261915207
time_elpased: 1.996
batch start
#iterations: 129
currently lose_sum: 84.18272560834885
time_elpased: 1.979
batch start
#iterations: 130
currently lose_sum: 84.84095853567123
time_elpased: 2.002
batch start
#iterations: 131
currently lose_sum: 84.34626591205597
time_elpased: 1.977
batch start
#iterations: 132
currently lose_sum: 84.6195450425148
time_elpased: 1.979
batch start
#iterations: 133
currently lose_sum: 84.76240438222885
time_elpased: 1.992
batch start
#iterations: 134
currently lose_sum: 84.39803522825241
time_elpased: 1.976
batch start
#iterations: 135
currently lose_sum: 84.28364986181259
time_elpased: 1.957
batch start
#iterations: 136
currently lose_sum: 84.31344690918922
time_elpased: 1.975
batch start
#iterations: 137
currently lose_sum: 84.61206889152527
time_elpased: 1.991
batch start
#iterations: 138
currently lose_sum: 84.01521456241608
time_elpased: 2.04
batch start
#iterations: 139
currently lose_sum: 82.8943452835083
time_elpased: 2.039
start validation test
0.607371134021
0.630486137014
0.521922601894
0.571090714567
0.607512312925
67.789
batch start
#iterations: 140
currently lose_sum: 83.81960988044739
time_elpased: 2.002
batch start
#iterations: 141
currently lose_sum: 83.80603832006454
time_elpased: 2.034
batch start
#iterations: 142
currently lose_sum: 83.48832482099533
time_elpased: 1.972
batch start
#iterations: 143
currently lose_sum: 83.70898973941803
time_elpased: 1.964
batch start
#iterations: 144
currently lose_sum: 83.61389175057411
time_elpased: 1.986
batch start
#iterations: 145
currently lose_sum: 83.48133379220963
time_elpased: 1.968
batch start
#iterations: 146
currently lose_sum: 84.05317163467407
time_elpased: 1.976
batch start
#iterations: 147
currently lose_sum: 83.5461944937706
time_elpased: 2.027
batch start
#iterations: 148
currently lose_sum: 83.8156059384346
time_elpased: 1.982
batch start
#iterations: 149
currently lose_sum: 83.88925039768219
time_elpased: 2.033
batch start
#iterations: 150
currently lose_sum: 83.76907473802567
time_elpased: 1.982
batch start
#iterations: 151
currently lose_sum: 83.53956478834152
time_elpased: 1.968
batch start
#iterations: 152
currently lose_sum: 83.25130844116211
time_elpased: 1.984
batch start
#iterations: 153
currently lose_sum: 83.45923364162445
time_elpased: 1.956
batch start
#iterations: 154
currently lose_sum: 83.86173379421234
time_elpased: 1.999
batch start
#iterations: 155
currently lose_sum: 83.72696977853775
time_elpased: 2.02
batch start
#iterations: 156
currently lose_sum: 83.12522292137146
time_elpased: 1.97
batch start
#iterations: 157
currently lose_sum: 82.60723280906677
time_elpased: 1.992
batch start
#iterations: 158
currently lose_sum: 83.46952822804451
time_elpased: 1.985
batch start
#iterations: 159
currently lose_sum: 83.13385915756226
time_elpased: 1.999
start validation test
0.594381443299
0.624042981867
0.47818032112
0.541460287862
0.594573431935
69.183
batch start
#iterations: 160
currently lose_sum: 83.05017450451851
time_elpased: 2.112
batch start
#iterations: 161
currently lose_sum: 83.19336813688278
time_elpased: 1.994
batch start
#iterations: 162
currently lose_sum: 82.99167358875275
time_elpased: 1.992
batch start
#iterations: 163
currently lose_sum: 83.35057705640793
time_elpased: 2.031
batch start
#iterations: 164
currently lose_sum: 83.16859924793243
time_elpased: 1.997
batch start
#iterations: 165
currently lose_sum: 82.76620754599571
time_elpased: 2.035
batch start
#iterations: 166
currently lose_sum: 83.06899905204773
time_elpased: 2.014
batch start
#iterations: 167
currently lose_sum: 82.68954914808273
time_elpased: 2.0
batch start
#iterations: 168
currently lose_sum: 82.89115077257156
time_elpased: 1.983
batch start
#iterations: 169
currently lose_sum: 82.72488361597061
time_elpased: 1.996
batch start
#iterations: 170
currently lose_sum: 82.62145417928696
time_elpased: 1.983
batch start
#iterations: 171
currently lose_sum: 83.00742083787918
time_elpased: 1.998
batch start
#iterations: 172
currently lose_sum: 82.77157753705978
time_elpased: 2.006
batch start
#iterations: 173
currently lose_sum: 83.25677636265755
time_elpased: 2.006
batch start
#iterations: 174
currently lose_sum: 82.22496673464775
time_elpased: 1.996
batch start
#iterations: 175
currently lose_sum: 82.3863999247551
time_elpased: 1.991
batch start
#iterations: 176
currently lose_sum: 82.6882563829422
time_elpased: 1.982
batch start
#iterations: 177
currently lose_sum: 82.72634643316269
time_elpased: 2.025
batch start
#iterations: 178
currently lose_sum: 82.30435812473297
time_elpased: 2.006
batch start
#iterations: 179
currently lose_sum: 82.28894260525703
time_elpased: 1.978
start validation test
0.601340206186
0.634281842818
0.481782626595
0.547613476837
0.601537740394
70.068
batch start
#iterations: 180
currently lose_sum: 81.99842566251755
time_elpased: 1.994
batch start
#iterations: 181
currently lose_sum: 82.58531194925308
time_elpased: 2.024
batch start
#iterations: 182
currently lose_sum: 82.47735410928726
time_elpased: 1.99
batch start
#iterations: 183
currently lose_sum: 82.84929883480072
time_elpased: 2.001
batch start
#iterations: 184
currently lose_sum: 81.51400277018547
time_elpased: 1.993
batch start
#iterations: 185
currently lose_sum: 82.05687811970711
time_elpased: 1.989
batch start
#iterations: 186
currently lose_sum: 81.96454924345016
time_elpased: 2.008
batch start
#iterations: 187
currently lose_sum: 82.20589166879654
time_elpased: 1.999
batch start
#iterations: 188
currently lose_sum: 82.08774322271347
time_elpased: 1.992
batch start
#iterations: 189
currently lose_sum: 82.37951463460922
time_elpased: 1.999
batch start
#iterations: 190
currently lose_sum: 82.32454758882523
time_elpased: 1.982
batch start
#iterations: 191
currently lose_sum: 81.70007920265198
time_elpased: 1.982
batch start
#iterations: 192
currently lose_sum: 82.30498731136322
time_elpased: 1.976
batch start
#iterations: 193
currently lose_sum: 81.94915464520454
time_elpased: 2.007
batch start
#iterations: 194
currently lose_sum: 82.6373639702797
time_elpased: 1.978
batch start
#iterations: 195
currently lose_sum: 81.87828296422958
time_elpased: 2.0
batch start
#iterations: 196
currently lose_sum: 81.78400665521622
time_elpased: 1.994
batch start
#iterations: 197
currently lose_sum: 82.04167360067368
time_elpased: 1.983
batch start
#iterations: 198
currently lose_sum: 81.99990579485893
time_elpased: 2.0
batch start
#iterations: 199
currently lose_sum: 81.26815459132195
time_elpased: 2.023
start validation test
0.59793814433
0.625491223474
0.491457389872
0.550432276657
0.598114072879
70.209
batch start
#iterations: 200
currently lose_sum: 81.31257298588753
time_elpased: 1.979
batch start
#iterations: 201
currently lose_sum: 81.68115338683128
time_elpased: 1.969
batch start
#iterations: 202
currently lose_sum: 81.75153648853302
time_elpased: 1.989
batch start
#iterations: 203
currently lose_sum: 81.84234642982483
time_elpased: 1.968
batch start
#iterations: 204
currently lose_sum: 81.72021043300629
time_elpased: 1.993
batch start
#iterations: 205
currently lose_sum: 82.17306178808212
time_elpased: 2.006
batch start
#iterations: 206
currently lose_sum: 81.41053694486618
time_elpased: 2.025
batch start
#iterations: 207
currently lose_sum: 81.82328712940216
time_elpased: 2.011
batch start
#iterations: 208
currently lose_sum: 81.3107468187809
time_elpased: 1.982
batch start
#iterations: 209
currently lose_sum: 81.59271204471588
time_elpased: 2.017
batch start
#iterations: 210
currently lose_sum: 80.97218585014343
time_elpased: 1.979
batch start
#iterations: 211
currently lose_sum: 81.07887804508209
time_elpased: 1.97
batch start
#iterations: 212
currently lose_sum: 81.19217872619629
time_elpased: 1.996
batch start
#iterations: 213
currently lose_sum: 81.68945768475533
time_elpased: 2.008
batch start
#iterations: 214
currently lose_sum: 81.05525010824203
time_elpased: 1.993
batch start
#iterations: 215
currently lose_sum: 81.23189514875412
time_elpased: 2.016
batch start
#iterations: 216
currently lose_sum: 81.34442290663719
time_elpased: 2.004
batch start
#iterations: 217
currently lose_sum: 81.48055911064148
time_elpased: 2.027
batch start
#iterations: 218
currently lose_sum: 81.2820291519165
time_elpased: 1.987
batch start
#iterations: 219
currently lose_sum: 81.63177698850632
time_elpased: 1.975
start validation test
0.596237113402
0.630891144168
0.467064635653
0.536755573955
0.59645053344
70.807
batch start
#iterations: 220
currently lose_sum: 81.46986043453217
time_elpased: 2.018
batch start
#iterations: 221
currently lose_sum: 81.0380562543869
time_elpased: 2.048
batch start
#iterations: 222
currently lose_sum: 81.33300858736038
time_elpased: 2.047
batch start
#iterations: 223
currently lose_sum: 80.87724554538727
time_elpased: 2.005
batch start
#iterations: 224
currently lose_sum: 81.34642398357391
time_elpased: 1.978
batch start
#iterations: 225
currently lose_sum: 80.7573606967926
time_elpased: 2.033
batch start
#iterations: 226
currently lose_sum: 80.55913043022156
time_elpased: 1.962
batch start
#iterations: 227
currently lose_sum: 80.69604378938675
time_elpased: 2.008
batch start
#iterations: 228
currently lose_sum: 80.78582242131233
time_elpased: 2.017
batch start
#iterations: 229
currently lose_sum: 80.2909255027771
time_elpased: 2.039
batch start
#iterations: 230
currently lose_sum: 80.85501253604889
time_elpased: 1.963
batch start
#iterations: 231
currently lose_sum: 81.13852334022522
time_elpased: 2.044
batch start
#iterations: 232
currently lose_sum: 81.22741496562958
time_elpased: 1.99
batch start
#iterations: 233
currently lose_sum: 81.60188907384872
time_elpased: 2.031
batch start
#iterations: 234
currently lose_sum: 81.49895843863487
time_elpased: 1.972
batch start
#iterations: 235
currently lose_sum: 80.71022632718086
time_elpased: 2.014
batch start
#iterations: 236
currently lose_sum: 81.0651989877224
time_elpased: 2.029
batch start
#iterations: 237
currently lose_sum: 80.39854553341866
time_elpased: 2.025
batch start
#iterations: 238
currently lose_sum: 80.53286346793175
time_elpased: 1.982
batch start
#iterations: 239
currently lose_sum: 81.55126610398293
time_elpased: 2.012
start validation test
0.591958762887
0.634973005399
0.435776039522
0.516845703125
0.592216809518
72.189
batch start
#iterations: 240
currently lose_sum: 80.55366504192352
time_elpased: 2.012
batch start
#iterations: 241
currently lose_sum: 80.39825350046158
time_elpased: 2.0
batch start
#iterations: 242
currently lose_sum: 80.75076732039452
time_elpased: 1.998
batch start
#iterations: 243
currently lose_sum: 81.02393704652786
time_elpased: 1.963
batch start
#iterations: 244
currently lose_sum: 80.19905263185501
time_elpased: 1.994
batch start
#iterations: 245
currently lose_sum: 81.14719897508621
time_elpased: 2.011
batch start
#iterations: 246
currently lose_sum: 80.80186998844147
time_elpased: 1.998
batch start
#iterations: 247
currently lose_sum: 80.43383398652077
time_elpased: 2.01
batch start
#iterations: 248
currently lose_sum: 80.5214467048645
time_elpased: 1.984
batch start
#iterations: 249
currently lose_sum: 80.47883948683739
time_elpased: 2.023
batch start
#iterations: 250
currently lose_sum: 80.61161908507347
time_elpased: 2.153
batch start
#iterations: 251
currently lose_sum: 80.53689506649971
time_elpased: 2.046
batch start
#iterations: 252
currently lose_sum: 80.96853911876678
time_elpased: 2.07
batch start
#iterations: 253
currently lose_sum: 80.7933100759983
time_elpased: 2.02
batch start
#iterations: 254
currently lose_sum: 80.10208770632744
time_elpased: 2.01
batch start
#iterations: 255
currently lose_sum: 80.05466619133949
time_elpased: 2.014
batch start
#iterations: 256
currently lose_sum: 80.37229552865028
time_elpased: 2.004
batch start
#iterations: 257
currently lose_sum: 80.71364170312881
time_elpased: 2.004
batch start
#iterations: 258
currently lose_sum: 80.90746900439262
time_elpased: 1.996
batch start
#iterations: 259
currently lose_sum: 80.39339259266853
time_elpased: 1.986
start validation test
0.581804123711
0.622161255906
0.420131741457
0.501566627757
0.582071240411
73.510
batch start
#iterations: 260
currently lose_sum: 80.3050445318222
time_elpased: 2.003
batch start
#iterations: 261
currently lose_sum: 80.06417760252953
time_elpased: 2.024
batch start
#iterations: 262
currently lose_sum: 79.8939528465271
time_elpased: 1.976
batch start
#iterations: 263
currently lose_sum: 79.91869676113129
time_elpased: 2.035
batch start
#iterations: 264
currently lose_sum: 80.92293256521225
time_elpased: 2.035
batch start
#iterations: 265
currently lose_sum: 80.23805475234985
time_elpased: 2.024
batch start
#iterations: 266
currently lose_sum: 80.40860882401466
time_elpased: 2.06
batch start
#iterations: 267
currently lose_sum: 80.13003066182137
time_elpased: 1.993
batch start
#iterations: 268
currently lose_sum: 80.26762434840202
time_elpased: 1.986
batch start
#iterations: 269
currently lose_sum: 79.97290447354317
time_elpased: 2.011
batch start
#iterations: 270
currently lose_sum: 79.3215980231762
time_elpased: 1.986
batch start
#iterations: 271
currently lose_sum: 80.16891753673553
time_elpased: 2.05
batch start
#iterations: 272
currently lose_sum: 80.37949925661087
time_elpased: 2.023
batch start
#iterations: 273
currently lose_sum: 79.5133368074894
time_elpased: 1.992
batch start
#iterations: 274
currently lose_sum: 80.23148384690285
time_elpased: 2.039
batch start
#iterations: 275
currently lose_sum: 79.9946291744709
time_elpased: 2.007
batch start
#iterations: 276
currently lose_sum: 79.5233461856842
time_elpased: 2.001
batch start
#iterations: 277
currently lose_sum: 79.33071434497833
time_elpased: 2.018
batch start
#iterations: 278
currently lose_sum: 79.40914759039879
time_elpased: 1.987
batch start
#iterations: 279
currently lose_sum: 79.37432739138603
time_elpased: 2.034
start validation test
0.595309278351
0.627792243388
0.471490325237
0.538529359901
0.595513853242
73.264
batch start
#iterations: 280
currently lose_sum: 79.09058305621147
time_elpased: 1.969
batch start
#iterations: 281
currently lose_sum: 79.12609541416168
time_elpased: 1.958
batch start
#iterations: 282
currently lose_sum: 79.56279385089874
time_elpased: 2.001
batch start
#iterations: 283
currently lose_sum: 80.00321066379547
time_elpased: 1.988
batch start
#iterations: 284
currently lose_sum: 79.81865319609642
time_elpased: 2.038
batch start
#iterations: 285
currently lose_sum: 79.1824007332325
time_elpased: 2.023
batch start
#iterations: 286
currently lose_sum: 78.91921278834343
time_elpased: 1.991
batch start
#iterations: 287
currently lose_sum: 79.50514221191406
time_elpased: 2.001
batch start
#iterations: 288
currently lose_sum: 80.50587278604507
time_elpased: 1.99
batch start
#iterations: 289
currently lose_sum: 79.54331609606743
time_elpased: 2.008
batch start
#iterations: 290
currently lose_sum: 79.96386447548866
time_elpased: 1.982
batch start
#iterations: 291
currently lose_sum: 79.88191103935242
time_elpased: 2.004
batch start
#iterations: 292
currently lose_sum: 79.42078170180321
time_elpased: 2.01
batch start
#iterations: 293
currently lose_sum: 79.5524867773056
time_elpased: 1.992
batch start
#iterations: 294
currently lose_sum: 78.9463395178318
time_elpased: 2.01
batch start
#iterations: 295
currently lose_sum: 78.6719643175602
time_elpased: 2.022
batch start
#iterations: 296
currently lose_sum: 79.4206694662571
time_elpased: 1.997
batch start
#iterations: 297
currently lose_sum: 79.36032354831696
time_elpased: 2.008
batch start
#iterations: 298
currently lose_sum: 79.70081636309624
time_elpased: 1.998
batch start
#iterations: 299
currently lose_sum: 79.3341491818428
time_elpased: 2.028
start validation test
0.593402061856
0.630051223677
0.455743104158
0.528905876732
0.593629503339
73.692
batch start
#iterations: 300
currently lose_sum: 79.30936607718468
time_elpased: 2.01
batch start
#iterations: 301
currently lose_sum: 79.52873966097832
time_elpased: 2.001
batch start
#iterations: 302
currently lose_sum: 78.96766477823257
time_elpased: 2.044
batch start
#iterations: 303
currently lose_sum: 78.48157036304474
time_elpased: 2.008
batch start
#iterations: 304
currently lose_sum: 79.49427163600922
time_elpased: 2.036
batch start
#iterations: 305
currently lose_sum: 79.33121779561043
time_elpased: 2.009
batch start
#iterations: 306
currently lose_sum: 79.48102623224258
time_elpased: 2.01
batch start
#iterations: 307
currently lose_sum: 79.54582569003105
time_elpased: 1.954
batch start
#iterations: 308
currently lose_sum: 79.29760748147964
time_elpased: 2.001
batch start
#iterations: 309
currently lose_sum: 78.58100810647011
time_elpased: 1.989
batch start
#iterations: 310
currently lose_sum: 78.55754840373993
time_elpased: 2.005
batch start
#iterations: 311
currently lose_sum: 78.9943220615387
time_elpased: 1.981
batch start
#iterations: 312
currently lose_sum: 78.89624953269958
time_elpased: 2.0
batch start
#iterations: 313
currently lose_sum: 79.14988997578621
time_elpased: 1.996
batch start
#iterations: 314
currently lose_sum: 79.14344844222069
time_elpased: 2.027
batch start
#iterations: 315
currently lose_sum: 78.87183555960655
time_elpased: 1.98
batch start
#iterations: 316
currently lose_sum: 78.8235875070095
time_elpased: 1.976
batch start
#iterations: 317
currently lose_sum: 78.17982256412506
time_elpased: 1.971
batch start
#iterations: 318
currently lose_sum: 78.65681105852127
time_elpased: 1.974
batch start
#iterations: 319
currently lose_sum: 78.89918449521065
time_elpased: 1.981
start validation test
0.590412371134
0.625889046942
0.452861259778
0.525498626538
0.590639634432
74.494
batch start
#iterations: 320
currently lose_sum: 79.35935541987419
time_elpased: 2.015
batch start
#iterations: 321
currently lose_sum: 79.2457489669323
time_elpased: 1.976
batch start
#iterations: 322
currently lose_sum: 79.21559157967567
time_elpased: 1.968
batch start
#iterations: 323
currently lose_sum: 78.21588200330734
time_elpased: 1.996
batch start
#iterations: 324
currently lose_sum: 78.83264926075935
time_elpased: 1.976
batch start
#iterations: 325
currently lose_sum: 79.09200736880302
time_elpased: 1.983
batch start
#iterations: 326
currently lose_sum: 77.89226096868515
time_elpased: 1.998
batch start
#iterations: 327
currently lose_sum: 78.91689512133598
time_elpased: 2.03
batch start
#iterations: 328
currently lose_sum: 78.23346078395844
time_elpased: 2.033
batch start
#iterations: 329
currently lose_sum: 78.39981043338776
time_elpased: 1.994
batch start
#iterations: 330
currently lose_sum: 78.70924717187881
time_elpased: 1.962
batch start
#iterations: 331
currently lose_sum: 78.51976388692856
time_elpased: 1.991
batch start
#iterations: 332
currently lose_sum: 79.19956946372986
time_elpased: 1.981
batch start
#iterations: 333
currently lose_sum: 78.6794176697731
time_elpased: 1.968
batch start
#iterations: 334
currently lose_sum: 78.92026117444038
time_elpased: 2.011
batch start
#iterations: 335
currently lose_sum: 78.84434753656387
time_elpased: 1.989
batch start
#iterations: 336
currently lose_sum: 78.42601481080055
time_elpased: 1.975
batch start
#iterations: 337
currently lose_sum: 77.90297383069992
time_elpased: 2.037
batch start
#iterations: 338
currently lose_sum: 78.12111985683441
time_elpased: 1.978
batch start
#iterations: 339
currently lose_sum: 78.65936750173569
time_elpased: 2.04
start validation test
0.596494845361
0.630494885264
0.469431864965
0.538171091445
0.596704780066
74.500
batch start
#iterations: 340
currently lose_sum: 78.90238404273987
time_elpased: 1.991
batch start
#iterations: 341
currently lose_sum: 78.74368622899055
time_elpased: 2.053
batch start
#iterations: 342
currently lose_sum: 78.99329793453217
time_elpased: 1.965
batch start
#iterations: 343
currently lose_sum: 78.81870520114899
time_elpased: 2.073
batch start
#iterations: 344
currently lose_sum: 78.73587000370026
time_elpased: 2.012
batch start
#iterations: 345
currently lose_sum: 78.05899703502655
time_elpased: 2.016
batch start
#iterations: 346
currently lose_sum: 78.23344948887825
time_elpased: 2.045
batch start
#iterations: 347
currently lose_sum: 77.76408010721207
time_elpased: 2.003
batch start
#iterations: 348
currently lose_sum: 78.24635231494904
time_elpased: 1.982
batch start
#iterations: 349
currently lose_sum: 78.9546265900135
time_elpased: 2.014
batch start
#iterations: 350
currently lose_sum: 78.40138384699821
time_elpased: 1.974
batch start
#iterations: 351
currently lose_sum: 78.31922340393066
time_elpased: 2.006
batch start
#iterations: 352
currently lose_sum: 78.45315408706665
time_elpased: 1.949
batch start
#iterations: 353
currently lose_sum: 78.40697634220123
time_elpased: 2.01
batch start
#iterations: 354
currently lose_sum: 78.24117001891136
time_elpased: 2.083
batch start
#iterations: 355
currently lose_sum: 78.53277796506882
time_elpased: 2.013
batch start
#iterations: 356
currently lose_sum: 78.09504419565201
time_elpased: 1.975
batch start
#iterations: 357
currently lose_sum: 78.49449118971825
time_elpased: 2.022
batch start
#iterations: 358
currently lose_sum: 78.7950708270073
time_elpased: 1.965
batch start
#iterations: 359
currently lose_sum: 77.78089734911919
time_elpased: 2.044
start validation test
0.592474226804
0.62346521146
0.470358172087
0.536196175056
0.59267598815
75.216
batch start
#iterations: 360
currently lose_sum: 78.76450672745705
time_elpased: 1.983
batch start
#iterations: 361
currently lose_sum: 77.46278920769691
time_elpased: 2.025
batch start
#iterations: 362
currently lose_sum: 78.24604034423828
time_elpased: 1.987
batch start
#iterations: 363
currently lose_sum: 77.7220196723938
time_elpased: 1.992
batch start
#iterations: 364
currently lose_sum: 78.09242716431618
time_elpased: 2.007
batch start
#iterations: 365
currently lose_sum: 78.24446192383766
time_elpased: 2.015
batch start
#iterations: 366
currently lose_sum: 78.23634460568428
time_elpased: 1.967
batch start
#iterations: 367
currently lose_sum: 78.28762689232826
time_elpased: 2.061
batch start
#iterations: 368
currently lose_sum: 78.25873500108719
time_elpased: 1.991
batch start
#iterations: 369
currently lose_sum: 78.17316082119942
time_elpased: 1.988
batch start
#iterations: 370
currently lose_sum: 78.44691336154938
time_elpased: 1.979
batch start
#iterations: 371
currently lose_sum: 77.92601364850998
time_elpased: 2.017
batch start
#iterations: 372
currently lose_sum: 78.54394242167473
time_elpased: 1.945
batch start
#iterations: 373
currently lose_sum: 78.69260165095329
time_elpased: 1.987
batch start
#iterations: 374
currently lose_sum: 78.24980393052101
time_elpased: 1.945
batch start
#iterations: 375
currently lose_sum: 77.91684198379517
time_elpased: 2.024
batch start
#iterations: 376
currently lose_sum: 77.9832338988781
time_elpased: 1.959
batch start
#iterations: 377
currently lose_sum: 77.98791992664337
time_elpased: 1.988
batch start
#iterations: 378
currently lose_sum: 78.4925282895565
time_elpased: 1.982
batch start
#iterations: 379
currently lose_sum: 77.87529549002647
time_elpased: 1.973
start validation test
0.587628865979
0.619465329992
0.457904487443
0.526571191857
0.587843197873
75.781
batch start
#iterations: 380
currently lose_sum: 77.56557959318161
time_elpased: 1.94
batch start
#iterations: 381
currently lose_sum: 78.19512978196144
time_elpased: 2.019
batch start
#iterations: 382
currently lose_sum: 78.34131118655205
time_elpased: 1.964
batch start
#iterations: 383
currently lose_sum: 78.19122502207756
time_elpased: 1.987
batch start
#iterations: 384
currently lose_sum: 77.62350916862488
time_elpased: 1.986
batch start
#iterations: 385
currently lose_sum: 77.30969372391701
time_elpased: 1.933
batch start
#iterations: 386
currently lose_sum: 78.2162729203701
time_elpased: 1.884
batch start
#iterations: 387
currently lose_sum: 77.36843022704124
time_elpased: 1.895
batch start
#iterations: 388
currently lose_sum: 77.69920954108238
time_elpased: 1.916
batch start
#iterations: 389
currently lose_sum: 77.47196337580681
time_elpased: 1.967
batch start
#iterations: 390
currently lose_sum: 77.35112062096596
time_elpased: 1.901
batch start
#iterations: 391
currently lose_sum: 77.1665623486042
time_elpased: 1.979
batch start
#iterations: 392
currently lose_sum: 78.16436505317688
time_elpased: 1.899
batch start
#iterations: 393
currently lose_sum: 77.71392071247101
time_elpased: 1.897
batch start
#iterations: 394
currently lose_sum: 77.2545862197876
time_elpased: 1.883
batch start
#iterations: 395
currently lose_sum: 77.85981440544128
time_elpased: 1.908
batch start
#iterations: 396
currently lose_sum: 77.23127406835556
time_elpased: 1.902
batch start
#iterations: 397
currently lose_sum: 77.32050853967667
time_elpased: 1.906
batch start
#iterations: 398
currently lose_sum: 78.11322745680809
time_elpased: 1.897
batch start
#iterations: 399
currently lose_sum: 77.37416145205498
time_elpased: 1.929
start validation test
0.596030927835
0.61860876152
0.504322766571
0.555650053864
0.596182448961
74.769
acc: 0.634
pre: 0.653
rec: 0.573
F1: 0.610
auc: 0.634
