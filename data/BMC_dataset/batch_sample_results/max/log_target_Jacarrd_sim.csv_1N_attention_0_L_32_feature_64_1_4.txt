start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.37852561473846
time_elpased: 2.425
batch start
#iterations: 1
currently lose_sum: 99.5298016667366
time_elpased: 2.375
batch start
#iterations: 2
currently lose_sum: 99.37956118583679
time_elpased: 2.138
batch start
#iterations: 3
currently lose_sum: 98.98709470033646
time_elpased: 2.33
batch start
#iterations: 4
currently lose_sum: 98.75451576709747
time_elpased: 2.13
batch start
#iterations: 5
currently lose_sum: 98.28858226537704
time_elpased: 2.521
batch start
#iterations: 6
currently lose_sum: 98.01827973127365
time_elpased: 2.412
batch start
#iterations: 7
currently lose_sum: 98.01840800046921
time_elpased: 2.305
batch start
#iterations: 8
currently lose_sum: 97.70014035701752
time_elpased: 2.507
batch start
#iterations: 9
currently lose_sum: 97.4593215584755
time_elpased: 2.384
batch start
#iterations: 10
currently lose_sum: 97.21254748106003
time_elpased: 2.328
batch start
#iterations: 11
currently lose_sum: 96.91455328464508
time_elpased: 2.364
batch start
#iterations: 12
currently lose_sum: 96.7270450592041
time_elpased: 2.387
batch start
#iterations: 13
currently lose_sum: 96.71319663524628
time_elpased: 2.598
batch start
#iterations: 14
currently lose_sum: 96.36603623628616
time_elpased: 2.27
batch start
#iterations: 15
currently lose_sum: 96.35058748722076
time_elpased: 2.352
batch start
#iterations: 16
currently lose_sum: 96.35045558214188
time_elpased: 2.428
batch start
#iterations: 17
currently lose_sum: 95.54114878177643
time_elpased: 2.338
batch start
#iterations: 18
currently lose_sum: 95.70073300600052
time_elpased: 2.539
batch start
#iterations: 19
currently lose_sum: 95.38178896903992
time_elpased: 2.341
start validation test
0.629896907216
0.640367378555
0.59555418339
0.617148341687
0.629957201165
62.665
batch start
#iterations: 20
currently lose_sum: 95.58077257871628
time_elpased: 2.045
batch start
#iterations: 21
currently lose_sum: 94.96159082651138
time_elpased: 2.125
batch start
#iterations: 22
currently lose_sum: 95.19115006923676
time_elpased: 2.484
batch start
#iterations: 23
currently lose_sum: 94.78306168317795
time_elpased: 2.55
batch start
#iterations: 24
currently lose_sum: 94.37456631660461
time_elpased: 2.167
batch start
#iterations: 25
currently lose_sum: 94.39839798212051
time_elpased: 2.01
batch start
#iterations: 26
currently lose_sum: 94.57858365774155
time_elpased: 2.056
batch start
#iterations: 27
currently lose_sum: 93.74661791324615
time_elpased: 2.208
batch start
#iterations: 28
currently lose_sum: 93.83986556529999
time_elpased: 2.152
batch start
#iterations: 29
currently lose_sum: 93.24823409318924
time_elpased: 2.514
batch start
#iterations: 30
currently lose_sum: 93.478151679039
time_elpased: 2.395
batch start
#iterations: 31
currently lose_sum: 92.9354652762413
time_elpased: 2.384
batch start
#iterations: 32
currently lose_sum: 93.01478558778763
time_elpased: 2.209
batch start
#iterations: 33
currently lose_sum: 93.19382482767105
time_elpased: 1.988
batch start
#iterations: 34
currently lose_sum: 92.5073834657669
time_elpased: 2.126
batch start
#iterations: 35
currently lose_sum: 92.41494005918503
time_elpased: 2.028
batch start
#iterations: 36
currently lose_sum: 92.39317280054092
time_elpased: 2.135
batch start
#iterations: 37
currently lose_sum: 91.84357136487961
time_elpased: 2.112
batch start
#iterations: 38
currently lose_sum: 91.79828691482544
time_elpased: 2.228
batch start
#iterations: 39
currently lose_sum: 92.0072865486145
time_elpased: 2.027
start validation test
0.610721649485
0.6650404025
0.448903982711
0.53600393217
0.611005745357
63.097
batch start
#iterations: 40
currently lose_sum: 91.62179094552994
time_elpased: 1.953
batch start
#iterations: 41
currently lose_sum: 91.45663714408875
time_elpased: 2.514
batch start
#iterations: 42
currently lose_sum: 91.70511215925217
time_elpased: 2.553
batch start
#iterations: 43
currently lose_sum: 90.77988922595978
time_elpased: 2.491
batch start
#iterations: 44
currently lose_sum: 91.36518007516861
time_elpased: 2.5
batch start
#iterations: 45
currently lose_sum: 90.87639993429184
time_elpased: 2.5
batch start
#iterations: 46
currently lose_sum: 90.68449884653091
time_elpased: 2.431
batch start
#iterations: 47
currently lose_sum: 90.8749948143959
time_elpased: 2.344
batch start
#iterations: 48
currently lose_sum: 90.56141048669815
time_elpased: 2.111
batch start
#iterations: 49
currently lose_sum: 90.47479528188705
time_elpased: 2.332
batch start
#iterations: 50
currently lose_sum: 89.87834519147873
time_elpased: 2.161
batch start
#iterations: 51
currently lose_sum: 89.90915155410767
time_elpased: 2.165
batch start
#iterations: 52
currently lose_sum: 89.97743672132492
time_elpased: 2.022
batch start
#iterations: 53
currently lose_sum: 89.49374336004257
time_elpased: 2.573
batch start
#iterations: 54
currently lose_sum: 89.6858121752739
time_elpased: 2.455
batch start
#iterations: 55
currently lose_sum: 89.20823323726654
time_elpased: 2.469
batch start
#iterations: 56
currently lose_sum: 89.3053789138794
time_elpased: 2.469
batch start
#iterations: 57
currently lose_sum: 89.12220805883408
time_elpased: 2.177
batch start
#iterations: 58
currently lose_sum: 89.01321238279343
time_elpased: 2.429
batch start
#iterations: 59
currently lose_sum: 89.46520382165909
time_elpased: 2.178
start validation test
0.610773195876
0.645956873315
0.49325923639
0.559374452938
0.610979509757
63.342
batch start
#iterations: 60
currently lose_sum: 88.48012399673462
time_elpased: 2.512
batch start
#iterations: 61
currently lose_sum: 89.37392342090607
time_elpased: 2.229
batch start
#iterations: 62
currently lose_sum: 88.5207069516182
time_elpased: 2.619
batch start
#iterations: 63
currently lose_sum: 88.48020964860916
time_elpased: 2.202
batch start
#iterations: 64
currently lose_sum: 88.82033669948578
time_elpased: 2.594
batch start
#iterations: 65
currently lose_sum: 87.54028970003128
time_elpased: 2.7
batch start
#iterations: 66
currently lose_sum: 88.06653529405594
time_elpased: 2.312
batch start
#iterations: 67
currently lose_sum: 87.53500998020172
time_elpased: 2.638
batch start
#iterations: 68
currently lose_sum: 87.75743007659912
time_elpased: 2.503
batch start
#iterations: 69
currently lose_sum: 87.87842464447021
time_elpased: 2.153
batch start
#iterations: 70
currently lose_sum: 87.20812559127808
time_elpased: 2.307
batch start
#iterations: 71
currently lose_sum: 87.38407391309738
time_elpased: 2.58
batch start
#iterations: 72
currently lose_sum: 87.16427367925644
time_elpased: 2.302
batch start
#iterations: 73
currently lose_sum: 87.19405448436737
time_elpased: 2.38
batch start
#iterations: 74
currently lose_sum: 87.01286774873734
time_elpased: 2.268
batch start
#iterations: 75
currently lose_sum: 86.81408649682999
time_elpased: 2.184
batch start
#iterations: 76
currently lose_sum: 86.72182351350784
time_elpased: 2.199
batch start
#iterations: 77
currently lose_sum: 87.03198570013046
time_elpased: 2.47
batch start
#iterations: 78
currently lose_sum: 86.40422397851944
time_elpased: 2.494
batch start
#iterations: 79
currently lose_sum: 86.03107064962387
time_elpased: 2.586
start validation test
0.606134020619
0.657942787584
0.444993310693
0.530910430352
0.606416927989
64.483
batch start
#iterations: 80
currently lose_sum: 85.90572941303253
time_elpased: 2.185
batch start
#iterations: 81
currently lose_sum: 86.10842907428741
time_elpased: 2.346
batch start
#iterations: 82
currently lose_sum: 85.71727085113525
time_elpased: 2.472
batch start
#iterations: 83
currently lose_sum: 85.95719999074936
time_elpased: 2.249
batch start
#iterations: 84
currently lose_sum: 85.70958465337753
time_elpased: 2.242
batch start
#iterations: 85
currently lose_sum: 86.01343673467636
time_elpased: 2.467
batch start
#iterations: 86
currently lose_sum: 85.52060544490814
time_elpased: 2.531
batch start
#iterations: 87
currently lose_sum: 85.77955329418182
time_elpased: 2.465
batch start
#iterations: 88
currently lose_sum: 85.05413663387299
time_elpased: 2.469
batch start
#iterations: 89
currently lose_sum: 85.50096869468689
time_elpased: 1.99
batch start
#iterations: 90
currently lose_sum: 85.25429517030716
time_elpased: 2.03
batch start
#iterations: 91
currently lose_sum: 85.5347547531128
time_elpased: 2.31
batch start
#iterations: 92
currently lose_sum: 84.75099557638168
time_elpased: 2.496
batch start
#iterations: 93
currently lose_sum: 84.93659794330597
time_elpased: 2.508
batch start
#iterations: 94
currently lose_sum: 84.77403599023819
time_elpased: 2.325
batch start
#iterations: 95
currently lose_sum: 84.74235206842422
time_elpased: 2.49
batch start
#iterations: 96
currently lose_sum: 83.84908103942871
time_elpased: 2.442
batch start
#iterations: 97
currently lose_sum: 84.5372623205185
time_elpased: 2.453
batch start
#iterations: 98
currently lose_sum: 84.49264186620712
time_elpased: 2.286
batch start
#iterations: 99
currently lose_sum: 84.5067355632782
time_elpased: 2.54
start validation test
0.601288659794
0.644587102422
0.454667078316
0.533220686742
0.601546076595
65.329
batch start
#iterations: 100
currently lose_sum: 84.55029791593552
time_elpased: 2.356
batch start
#iterations: 101
currently lose_sum: 84.06991147994995
time_elpased: 2.23
batch start
#iterations: 102
currently lose_sum: 84.25460827350616
time_elpased: 2.4
batch start
#iterations: 103
currently lose_sum: 83.32997161149979
time_elpased: 2.453
batch start
#iterations: 104
currently lose_sum: 83.23821687698364
time_elpased: 2.458
batch start
#iterations: 105
currently lose_sum: 83.92043471336365
time_elpased: 2.596
batch start
#iterations: 106
currently lose_sum: 83.93874114751816
time_elpased: 2.569
batch start
#iterations: 107
currently lose_sum: 83.50420224666595
time_elpased: 2.638
batch start
#iterations: 108
currently lose_sum: 83.10311424732208
time_elpased: 2.636
batch start
#iterations: 109
currently lose_sum: 83.44016528129578
time_elpased: 2.664
batch start
#iterations: 110
currently lose_sum: 83.49185633659363
time_elpased: 2.636
batch start
#iterations: 111
currently lose_sum: 83.5170333981514
time_elpased: 2.612
batch start
#iterations: 112
currently lose_sum: 82.81848323345184
time_elpased: 2.251
batch start
#iterations: 113
currently lose_sum: 82.6063904762268
time_elpased: 2.65
batch start
#iterations: 114
currently lose_sum: 82.68097048997879
time_elpased: 2.593
batch start
#iterations: 115
currently lose_sum: 82.70232397317886
time_elpased: 2.666
batch start
#iterations: 116
currently lose_sum: 82.6591888666153
time_elpased: 2.186
batch start
#iterations: 117
currently lose_sum: 82.34959337115288
time_elpased: 2.234
batch start
#iterations: 118
currently lose_sum: 82.21147200465202
time_elpased: 2.192
batch start
#iterations: 119
currently lose_sum: 82.10988545417786
time_elpased: 1.99
start validation test
0.600618556701
0.649098894442
0.441082638674
0.525245098039
0.600898646612
65.979
batch start
#iterations: 120
currently lose_sum: 82.27641582489014
time_elpased: 2.124
batch start
#iterations: 121
currently lose_sum: 81.80705267190933
time_elpased: 2.366
batch start
#iterations: 122
currently lose_sum: 82.49930316209793
time_elpased: 2.511
batch start
#iterations: 123
currently lose_sum: 82.4867799282074
time_elpased: 2.489
batch start
#iterations: 124
currently lose_sum: 82.20863604545593
time_elpased: 2.489
batch start
#iterations: 125
currently lose_sum: 82.08073842525482
time_elpased: 2.054
batch start
#iterations: 126
currently lose_sum: 81.63951906561852
time_elpased: 2.005
batch start
#iterations: 127
currently lose_sum: 81.75960260629654
time_elpased: 1.951
batch start
#iterations: 128
currently lose_sum: 81.2338373363018
time_elpased: 2.04
batch start
#iterations: 129
currently lose_sum: 81.41621422767639
time_elpased: 2.145
batch start
#iterations: 130
currently lose_sum: 81.4857285618782
time_elpased: 2.338
batch start
#iterations: 131
currently lose_sum: 81.61034089326859
time_elpased: 1.947
batch start
#iterations: 132
currently lose_sum: 81.64205181598663
time_elpased: 1.98
batch start
#iterations: 133
currently lose_sum: 80.64881494641304
time_elpased: 1.982
batch start
#iterations: 134
currently lose_sum: 80.91401386260986
time_elpased: 2.506
batch start
#iterations: 135
currently lose_sum: 81.27056822180748
time_elpased: 2.494
batch start
#iterations: 136
currently lose_sum: 81.20562574267387
time_elpased: 2.108
batch start
#iterations: 137
currently lose_sum: 80.91362872719765
time_elpased: 2.502
batch start
#iterations: 138
currently lose_sum: 80.90014359354973
time_elpased: 2.615
batch start
#iterations: 139
currently lose_sum: 80.79773896932602
time_elpased: 2.534
start validation test
0.587680412371
0.641097240473
0.401667181229
0.493894337235
0.588006987289
68.538
batch start
#iterations: 140
currently lose_sum: 80.15340453386307
time_elpased: 2.477
batch start
#iterations: 141
currently lose_sum: 81.43179762363434
time_elpased: 2.289
batch start
#iterations: 142
currently lose_sum: 80.47410482168198
time_elpased: 1.961
batch start
#iterations: 143
currently lose_sum: 80.5223188996315
time_elpased: 2.132
batch start
#iterations: 144
currently lose_sum: 80.48143875598907
time_elpased: 2.078
batch start
#iterations: 145
currently lose_sum: 80.90808427333832
time_elpased: 2.496
batch start
#iterations: 146
currently lose_sum: 80.06425619125366
time_elpased: 2.403
batch start
#iterations: 147
currently lose_sum: 80.1431702375412
time_elpased: 2.158
batch start
#iterations: 148
currently lose_sum: 80.24381494522095
time_elpased: 2.251
batch start
#iterations: 149
currently lose_sum: 80.53109622001648
time_elpased: 2.43
batch start
#iterations: 150
currently lose_sum: 80.47257959842682
time_elpased: 2.384
batch start
#iterations: 151
currently lose_sum: 80.45703136920929
time_elpased: 2.553
batch start
#iterations: 152
currently lose_sum: 79.99975445866585
time_elpased: 2.214
batch start
#iterations: 153
currently lose_sum: 79.77851814031601
time_elpased: 2.494
batch start
#iterations: 154
currently lose_sum: 79.78939881920815
time_elpased: 2.392
batch start
#iterations: 155
currently lose_sum: 79.73175567388535
time_elpased: 1.634
batch start
#iterations: 156
currently lose_sum: 79.45797166228294
time_elpased: 2.253
batch start
#iterations: 157
currently lose_sum: 79.30716839432716
time_elpased: 2.596
batch start
#iterations: 158
currently lose_sum: 79.58819404244423
time_elpased: 2.289
batch start
#iterations: 159
currently lose_sum: 79.23935320973396
time_elpased: 2.483
start validation test
0.594536082474
0.639487565938
0.436657404549
0.518957925636
0.594813262844
68.411
batch start
#iterations: 160
currently lose_sum: 79.7105547785759
time_elpased: 2.708
batch start
#iterations: 161
currently lose_sum: 79.58407407999039
time_elpased: 2.602
batch start
#iterations: 162
currently lose_sum: 78.1314110159874
time_elpased: 2.23
batch start
#iterations: 163
currently lose_sum: 78.96456003189087
time_elpased: 2.448
batch start
#iterations: 164
currently lose_sum: 78.81992074847221
time_elpased: 1.932
batch start
#iterations: 165
currently lose_sum: 78.5145734846592
time_elpased: 2.01
batch start
#iterations: 166
currently lose_sum: 78.57430475950241
time_elpased: 2.082
batch start
#iterations: 167
currently lose_sum: 78.18846452236176
time_elpased: 2.244
batch start
#iterations: 168
currently lose_sum: 79.25770291686058
time_elpased: 2.456
batch start
#iterations: 169
currently lose_sum: 78.923230946064
time_elpased: 2.491
batch start
#iterations: 170
currently lose_sum: 77.86955150961876
time_elpased: 2.027
batch start
#iterations: 171
currently lose_sum: 78.16154080629349
time_elpased: 2.161
batch start
#iterations: 172
currently lose_sum: 78.08961036801338
time_elpased: 2.125
batch start
#iterations: 173
currently lose_sum: 78.43926241993904
time_elpased: 2.047
batch start
#iterations: 174
currently lose_sum: 78.9130676984787
time_elpased: 2.024
batch start
#iterations: 175
currently lose_sum: 78.39161396026611
time_elpased: 2.071
batch start
#iterations: 176
currently lose_sum: 78.33346027135849
time_elpased: 1.972
batch start
#iterations: 177
currently lose_sum: 77.75187689065933
time_elpased: 1.975
batch start
#iterations: 178
currently lose_sum: 77.41335278749466
time_elpased: 2.009
batch start
#iterations: 179
currently lose_sum: 78.34100595116615
time_elpased: 2.337
start validation test
0.595927835052
0.638618246236
0.445199135536
0.524649808987
0.596192462532
69.024
batch start
#iterations: 180
currently lose_sum: 77.32654955983162
time_elpased: 2.5
batch start
#iterations: 181
currently lose_sum: 77.99291688203812
time_elpased: 2.508
batch start
#iterations: 182
currently lose_sum: 78.03852713108063
time_elpased: 2.704
batch start
#iterations: 183
currently lose_sum: 77.44219028949738
time_elpased: 2.505
batch start
#iterations: 184
currently lose_sum: 77.14593681693077
time_elpased: 2.286
batch start
#iterations: 185
currently lose_sum: 77.86615759134293
time_elpased: 2.05
batch start
#iterations: 186
currently lose_sum: 77.3294647037983
time_elpased: 2.096
batch start
#iterations: 187
currently lose_sum: 77.88733544945717
time_elpased: 2.238
batch start
#iterations: 188
currently lose_sum: 77.5028695166111
time_elpased: 2.08
batch start
#iterations: 189
currently lose_sum: 77.05847239494324
time_elpased: 2.264
batch start
#iterations: 190
currently lose_sum: 77.5390776693821
time_elpased: 2.043
batch start
#iterations: 191
currently lose_sum: 76.90156477689743
time_elpased: 2.373
batch start
#iterations: 192
currently lose_sum: 76.81531795859337
time_elpased: 2.023
batch start
#iterations: 193
currently lose_sum: 77.1965816617012
time_elpased: 2.048
batch start
#iterations: 194
currently lose_sum: 77.36482837796211
time_elpased: 2.199
batch start
#iterations: 195
currently lose_sum: 76.60549712181091
time_elpased: 2.46
batch start
#iterations: 196
currently lose_sum: 76.99747696518898
time_elpased: 2.367
batch start
#iterations: 197
currently lose_sum: 76.6541596353054
time_elpased: 2.514
batch start
#iterations: 198
currently lose_sum: 76.55400854349136
time_elpased: 2.554
batch start
#iterations: 199
currently lose_sum: 76.74574327468872
time_elpased: 2.449
start validation test
0.589690721649
0.638630266688
0.416486569929
0.504173414725
0.589994808253
70.381
batch start
#iterations: 200
currently lose_sum: 76.78630489110947
time_elpased: 2.538
batch start
#iterations: 201
currently lose_sum: 77.22120514512062
time_elpased: 2.352
batch start
#iterations: 202
currently lose_sum: 76.24423131346703
time_elpased: 2.267
batch start
#iterations: 203
currently lose_sum: 76.65126693248749
time_elpased: 2.657
batch start
#iterations: 204
currently lose_sum: 76.85252296924591
time_elpased: 2.651
batch start
#iterations: 205
currently lose_sum: 76.00964081287384
time_elpased: 2.597
batch start
#iterations: 206
currently lose_sum: 76.00247782468796
time_elpased: 2.592
batch start
#iterations: 207
currently lose_sum: 76.0160023868084
time_elpased: 2.462
batch start
#iterations: 208
currently lose_sum: 76.50665727257729
time_elpased: 2.667
batch start
#iterations: 209
currently lose_sum: 75.65681007504463
time_elpased: 2.747
batch start
#iterations: 210
currently lose_sum: 75.80936804413795
time_elpased: 2.72
batch start
#iterations: 211
currently lose_sum: 76.16602352261543
time_elpased: 2.507
batch start
#iterations: 212
currently lose_sum: 76.00449275970459
time_elpased: 2.006
batch start
#iterations: 213
currently lose_sum: 76.136424690485
time_elpased: 2.075
batch start
#iterations: 214
currently lose_sum: 74.71064940094948
time_elpased: 2.449
batch start
#iterations: 215
currently lose_sum: 75.68556261062622
time_elpased: 2.108
batch start
#iterations: 216
currently lose_sum: 76.09751564264297
time_elpased: 2.264
batch start
#iterations: 217
currently lose_sum: 76.7496557533741
time_elpased: 2.027
batch start
#iterations: 218
currently lose_sum: 75.77026480436325
time_elpased: 2.076
batch start
#iterations: 219
currently lose_sum: 75.91380995512009
time_elpased: 2.009
start validation test
0.587268041237
0.638215324927
0.406298240198
0.496510092435
0.587585761636
71.161
batch start
#iterations: 220
currently lose_sum: 75.99249735474586
time_elpased: 2.017
batch start
#iterations: 221
currently lose_sum: 75.43240866065025
time_elpased: 2.296
batch start
#iterations: 222
currently lose_sum: 74.9138845205307
time_elpased: 2.535
batch start
#iterations: 223
currently lose_sum: 74.83815640211105
time_elpased: 2.411
batch start
#iterations: 224
currently lose_sum: 75.73228055238724
time_elpased: 2.093
batch start
#iterations: 225
currently lose_sum: 75.47556295990944
time_elpased: 2.508
batch start
#iterations: 226
currently lose_sum: 74.52463820576668
time_elpased: 2.377
batch start
#iterations: 227
currently lose_sum: 75.30315726995468
time_elpased: 2.141
batch start
#iterations: 228
currently lose_sum: 75.21665394306183
time_elpased: 1.973
batch start
#iterations: 229
currently lose_sum: 75.3279342353344
time_elpased: 1.999
batch start
#iterations: 230
currently lose_sum: 74.78352344036102
time_elpased: 1.969
batch start
#iterations: 231
currently lose_sum: 74.44590663909912
time_elpased: 2.156
batch start
#iterations: 232
currently lose_sum: 75.12679091095924
time_elpased: 2.01
batch start
#iterations: 233
currently lose_sum: 74.99221107363701
time_elpased: 1.981
batch start
#iterations: 234
currently lose_sum: 75.03593415021896
time_elpased: 2.26
batch start
#iterations: 235
currently lose_sum: 75.09294962882996
time_elpased: 2.493
batch start
#iterations: 236
currently lose_sum: 75.2683752477169
time_elpased: 2.472
batch start
#iterations: 237
currently lose_sum: 74.50477916002274
time_elpased: 2.107
batch start
#iterations: 238
currently lose_sum: 74.89854165911674
time_elpased: 2.246
batch start
#iterations: 239
currently lose_sum: 74.83264860510826
time_elpased: 2.357
start validation test
0.587680412371
0.636739891754
0.411649686117
0.500031251953
0.587989461462
72.078
batch start
#iterations: 240
currently lose_sum: 75.10851892828941
time_elpased: 1.964
batch start
#iterations: 241
currently lose_sum: 74.8820561170578
time_elpased: 2.231
batch start
#iterations: 242
currently lose_sum: 74.33066421747208
time_elpased: 2.513
batch start
#iterations: 243
currently lose_sum: 74.89520066976547
time_elpased: 2.409
batch start
#iterations: 244
currently lose_sum: 74.36795139312744
time_elpased: 2.617
batch start
#iterations: 245
currently lose_sum: 74.59953525662422
time_elpased: 2.58
batch start
#iterations: 246
currently lose_sum: 74.82627326250076
time_elpased: 2.332
batch start
#iterations: 247
currently lose_sum: 73.96267059445381
time_elpased: 2.545
batch start
#iterations: 248
currently lose_sum: 73.75242653489113
time_elpased: 2.402
batch start
#iterations: 249
currently lose_sum: 73.9235423207283
time_elpased: 2.261
batch start
#iterations: 250
currently lose_sum: 74.00826886296272
time_elpased: 2.275
batch start
#iterations: 251
currently lose_sum: 73.48354837298393
time_elpased: 2.442
batch start
#iterations: 252
currently lose_sum: 73.97316205501556
time_elpased: 2.028
batch start
#iterations: 253
currently lose_sum: 73.69707477092743
time_elpased: 2.13
batch start
#iterations: 254
currently lose_sum: 74.28594881296158
time_elpased: 2.536
batch start
#iterations: 255
currently lose_sum: 73.93793973326683
time_elpased: 2.519
batch start
#iterations: 256
currently lose_sum: 73.87433359026909
time_elpased: 2.711
batch start
#iterations: 257
currently lose_sum: 73.91963997483253
time_elpased: 2.469
batch start
#iterations: 258
currently lose_sum: 73.86945524811745
time_elpased: 2.153
batch start
#iterations: 259
currently lose_sum: 73.37885844707489
time_elpased: 2.342
start validation test
0.588092783505
0.636248815914
0.414737058763
0.502149398791
0.588397136218
72.296
batch start
#iterations: 260
currently lose_sum: 73.79944008588791
time_elpased: 2.115
batch start
#iterations: 261
currently lose_sum: 73.61886838078499
time_elpased: 2.552
batch start
#iterations: 262
currently lose_sum: 74.0546401143074
time_elpased: 2.424
batch start
#iterations: 263
currently lose_sum: 73.43183651566505
time_elpased: 2.481
batch start
#iterations: 264
currently lose_sum: 73.92278516292572
time_elpased: 2.505
batch start
#iterations: 265
currently lose_sum: 73.22689673304558
time_elpased: 1.932
batch start
#iterations: 266
currently lose_sum: 73.60301664471626
time_elpased: 2.264
batch start
#iterations: 267
currently lose_sum: 73.03577655553818
time_elpased: 2.521
batch start
#iterations: 268
currently lose_sum: 73.65585696697235
time_elpased: 2.263
batch start
#iterations: 269
currently lose_sum: 73.4841757118702
time_elpased: 2.132
batch start
#iterations: 270
currently lose_sum: 73.43558239936829
time_elpased: 2.348
batch start
#iterations: 271
currently lose_sum: 72.8673674762249
time_elpased: 2.04
batch start
#iterations: 272
currently lose_sum: 73.07093641161919
time_elpased: 2.045
batch start
#iterations: 273
currently lose_sum: 73.76530960202217
time_elpased: 1.909
batch start
#iterations: 274
currently lose_sum: 73.6917832493782
time_elpased: 2.025
batch start
#iterations: 275
currently lose_sum: 72.83393859863281
time_elpased: 2.064
batch start
#iterations: 276
currently lose_sum: 73.32361242175102
time_elpased: 2.344
batch start
#iterations: 277
currently lose_sum: 72.81278175115585
time_elpased: 2.202
batch start
#iterations: 278
currently lose_sum: 73.998221129179
time_elpased: 2.422
batch start
#iterations: 279
currently lose_sum: 73.04188299179077
time_elpased: 2.489
start validation test
0.569896907216
0.63888326927
0.324997427189
0.430832196453
0.570326866027
77.386
batch start
#iterations: 280
currently lose_sum: 73.52637457847595
time_elpased: 2.479
batch start
#iterations: 281
currently lose_sum: 72.44336184859276
time_elpased: 2.19
batch start
#iterations: 282
currently lose_sum: 72.56108319759369
time_elpased: 2.086
batch start
#iterations: 283
currently lose_sum: 72.50312268733978
time_elpased: 2.34
batch start
#iterations: 284
currently lose_sum: 72.75745606422424
time_elpased: 2.491
batch start
#iterations: 285
currently lose_sum: 72.71494579315186
time_elpased: 2.151
batch start
#iterations: 286
currently lose_sum: 72.46619406342506
time_elpased: 2.066
batch start
#iterations: 287
currently lose_sum: 72.4318015575409
time_elpased: 2.009
batch start
#iterations: 288
currently lose_sum: 72.48361900448799
time_elpased: 2.079
batch start
#iterations: 289
currently lose_sum: 72.5079953968525
time_elpased: 2.139
batch start
#iterations: 290
currently lose_sum: 72.16535165905952
time_elpased: 2.494
batch start
#iterations: 291
currently lose_sum: 72.28191179037094
time_elpased: 2.106
batch start
#iterations: 292
currently lose_sum: 72.00491291284561
time_elpased: 2.472
batch start
#iterations: 293
currently lose_sum: 72.65442723035812
time_elpased: 2.521
batch start
#iterations: 294
currently lose_sum: 72.49554374814034
time_elpased: 2.367
batch start
#iterations: 295
currently lose_sum: 72.22978109121323
time_elpased: 2.453
batch start
#iterations: 296
currently lose_sum: 71.78019943833351
time_elpased: 2.329
batch start
#iterations: 297
currently lose_sum: 72.00522392988205
time_elpased: 2.325
batch start
#iterations: 298
currently lose_sum: 71.56322115659714
time_elpased: 2.544
batch start
#iterations: 299
currently lose_sum: 71.95590060949326
time_elpased: 2.391
start validation test
0.579896907216
0.639536954586
0.369558505712
0.468432037568
0.580266188723
76.125
batch start
#iterations: 300
currently lose_sum: 72.41193664073944
time_elpased: 2.497
batch start
#iterations: 301
currently lose_sum: 72.05810961127281
time_elpased: 2.634
batch start
#iterations: 302
currently lose_sum: 72.1312046945095
time_elpased: 2.423
batch start
#iterations: 303
currently lose_sum: 71.94930005073547
time_elpased: 2.551
batch start
#iterations: 304
currently lose_sum: 71.75339502096176
time_elpased: 2.711
batch start
#iterations: 305
currently lose_sum: 72.56525695323944
time_elpased: 2.683
batch start
#iterations: 306
currently lose_sum: 72.0660067498684
time_elpased: 2.296
batch start
#iterations: 307
currently lose_sum: 72.00596478581429
time_elpased: 2.192
batch start
#iterations: 308
currently lose_sum: 71.51536360383034
time_elpased: 2.269
batch start
#iterations: 309
currently lose_sum: 71.84299379587173
time_elpased: 2.178
batch start
#iterations: 310
currently lose_sum: 71.52455154061317
time_elpased: 2.171
batch start
#iterations: 311
currently lose_sum: 71.19755956530571
time_elpased: 2.445
batch start
#iterations: 312
currently lose_sum: 71.5686841905117
time_elpased: 2.145
batch start
#iterations: 313
currently lose_sum: 71.98565703630447
time_elpased: 2.384
batch start
#iterations: 314
currently lose_sum: 70.91324830055237
time_elpased: 2.451
batch start
#iterations: 315
currently lose_sum: 71.21099147200584
time_elpased: 2.34
batch start
#iterations: 316
currently lose_sum: 71.64877671003342
time_elpased: 2.011
batch start
#iterations: 317
currently lose_sum: 71.94945254921913
time_elpased: 2.092
batch start
#iterations: 318
currently lose_sum: 71.56951603293419
time_elpased: 2.496
batch start
#iterations: 319
currently lose_sum: 71.0427408516407
time_elpased: 2.614
start validation test
0.576443298969
0.639457047229
0.353915817639
0.455647565419
0.576833980285
77.193
batch start
#iterations: 320
currently lose_sum: 72.11985576152802
time_elpased: 2.432
batch start
#iterations: 321
currently lose_sum: 71.91216704249382
time_elpased: 1.916
batch start
#iterations: 322
currently lose_sum: 71.66038730740547
time_elpased: 2.034
batch start
#iterations: 323
currently lose_sum: 72.08563414216042
time_elpased: 2.082
batch start
#iterations: 324
currently lose_sum: 71.33029696345329
time_elpased: 2.518
batch start
#iterations: 325
currently lose_sum: 71.53728902339935
time_elpased: 2.484
batch start
#iterations: 326
currently lose_sum: 70.48422011733055
time_elpased: 2.496
batch start
#iterations: 327
currently lose_sum: 71.73149079084396
time_elpased: 2.509
batch start
#iterations: 328
currently lose_sum: 70.78232243657112
time_elpased: 2.489
batch start
#iterations: 329
currently lose_sum: 71.27514496445656
time_elpased: 2.037
batch start
#iterations: 330
currently lose_sum: 70.7186521589756
time_elpased: 2.16
batch start
#iterations: 331
currently lose_sum: 71.27340596914291
time_elpased: 1.956
batch start
#iterations: 332
currently lose_sum: 71.32811185717583
time_elpased: 2.126
batch start
#iterations: 333
currently lose_sum: 70.92396154999733
time_elpased: 2.518
batch start
#iterations: 334
currently lose_sum: 71.1863369345665
time_elpased: 2.276
batch start
#iterations: 335
currently lose_sum: 69.97473034262657
time_elpased: 2.03
batch start
#iterations: 336
currently lose_sum: 71.21670779585838
time_elpased: 2.159
batch start
#iterations: 337
currently lose_sum: 70.81250464916229
time_elpased: 2.487
batch start
#iterations: 338
currently lose_sum: 70.5960801243782
time_elpased: 2.254
batch start
#iterations: 339
currently lose_sum: 70.52346009016037
time_elpased: 2.421
start validation test
0.57087628866
0.634000770119
0.338890604096
0.441687344913
0.57128357531
79.106
batch start
#iterations: 340
currently lose_sum: 70.28092670440674
time_elpased: 2.296
batch start
#iterations: 341
currently lose_sum: 70.97642076015472
time_elpased: 1.964
batch start
#iterations: 342
currently lose_sum: 70.30789956450462
time_elpased: 2.286
batch start
#iterations: 343
currently lose_sum: 70.93986842036247
time_elpased: 2.322
batch start
#iterations: 344
currently lose_sum: 70.03992891311646
time_elpased: 2.262
batch start
#iterations: 345
currently lose_sum: 71.08300364017487
time_elpased: 2.263
batch start
#iterations: 346
currently lose_sum: 70.70325121283531
time_elpased: 2.572
batch start
#iterations: 347
currently lose_sum: 69.69615826010704
time_elpased: 2.536
batch start
#iterations: 348
currently lose_sum: 70.57624399662018
time_elpased: 2.262
batch start
#iterations: 349
currently lose_sum: 70.45699280500412
time_elpased: 2.058
batch start
#iterations: 350
currently lose_sum: 71.05094113945961
time_elpased: 2.451
batch start
#iterations: 351
currently lose_sum: 70.29337841272354
time_elpased: 2.21
batch start
#iterations: 352
currently lose_sum: 70.51597100496292
time_elpased: 2.488
batch start
#iterations: 353
currently lose_sum: 70.5721675157547
time_elpased: 2.719
batch start
#iterations: 354
currently lose_sum: 71.00444430112839
time_elpased: 2.452
batch start
#iterations: 355
currently lose_sum: 70.52065280079842
time_elpased: 2.268
batch start
#iterations: 356
currently lose_sum: 70.39693048596382
time_elpased: 1.964
batch start
#iterations: 357
currently lose_sum: 70.46746435761452
time_elpased: 2.058
batch start
#iterations: 358
currently lose_sum: 69.9856296479702
time_elpased: 2.116
batch start
#iterations: 359
currently lose_sum: 69.92837637662888
time_elpased: 2.454
start validation test
0.566443298969
0.631124497992
0.323453740867
0.427706334626
0.566869904617
79.352
batch start
#iterations: 360
currently lose_sum: 70.0033707022667
time_elpased: 2.658
batch start
#iterations: 361
currently lose_sum: 70.33229112625122
time_elpased: 2.29
batch start
#iterations: 362
currently lose_sum: 69.94459801912308
time_elpased: 2.059
batch start
#iterations: 363
currently lose_sum: 70.06674593687057
time_elpased: 2.411
batch start
#iterations: 364
currently lose_sum: 69.47687152028084
time_elpased: 2.479
batch start
#iterations: 365
currently lose_sum: 69.6195031106472
time_elpased: 2.522
batch start
#iterations: 366
currently lose_sum: 69.41135796904564
time_elpased: 2.347
batch start
#iterations: 367
currently lose_sum: 69.97277748584747
time_elpased: 2.386
batch start
#iterations: 368
currently lose_sum: 70.058006554842
time_elpased: 2.047
batch start
#iterations: 369
currently lose_sum: 69.87315034866333
time_elpased: 1.921
batch start
#iterations: 370
currently lose_sum: 70.04709228873253
time_elpased: 2.517
batch start
#iterations: 371
currently lose_sum: 68.9229177236557
time_elpased: 2.341
batch start
#iterations: 372
currently lose_sum: 69.68550971150398
time_elpased: 2.502
batch start
#iterations: 373
currently lose_sum: 69.82569202780724
time_elpased: 2.123
batch start
#iterations: 374
currently lose_sum: 70.67012375593185
time_elpased: 2.345
batch start
#iterations: 375
currently lose_sum: 69.64727845788002
time_elpased: 2.212
batch start
#iterations: 376
currently lose_sum: 70.37380176782608
time_elpased: 2.264
batch start
#iterations: 377
currently lose_sum: 69.10257136821747
time_elpased: 2.585
batch start
#iterations: 378
currently lose_sum: 69.4396396279335
time_elpased: 2.545
batch start
#iterations: 379
currently lose_sum: 69.61760672926903
time_elpased: 2.505
start validation test
0.565463917526
0.633478531425
0.31429453535
0.420140321915
0.565904884116
81.148
batch start
#iterations: 380
currently lose_sum: 69.81187814474106
time_elpased: 2.501
batch start
#iterations: 381
currently lose_sum: 69.70963037014008
time_elpased: 2.479
batch start
#iterations: 382
currently lose_sum: 69.45784002542496
time_elpased: 2.341
batch start
#iterations: 383
currently lose_sum: 68.89713925123215
time_elpased: 2.389
batch start
#iterations: 384
currently lose_sum: 69.14724978804588
time_elpased: 2.327
batch start
#iterations: 385
currently lose_sum: 69.64607989788055
time_elpased: 2.563
batch start
#iterations: 386
currently lose_sum: 69.13681679964066
time_elpased: 2.59
batch start
#iterations: 387
currently lose_sum: 69.9002637565136
time_elpased: 2.568
batch start
#iterations: 388
currently lose_sum: 69.0903625190258
time_elpased: 2.281
batch start
#iterations: 389
currently lose_sum: 69.44205021858215
time_elpased: 2.313
batch start
#iterations: 390
currently lose_sum: 69.24750551581383
time_elpased: 2.554
batch start
#iterations: 391
currently lose_sum: 69.60939356684685
time_elpased: 2.381
batch start
#iterations: 392
currently lose_sum: 69.9949471950531
time_elpased: 2.44
batch start
#iterations: 393
currently lose_sum: 68.95446938276291
time_elpased: 2.378
batch start
#iterations: 394
currently lose_sum: 69.3584514260292
time_elpased: 2.532
batch start
#iterations: 395
currently lose_sum: 68.57625904679298
time_elpased: 2.307
batch start
#iterations: 396
currently lose_sum: 69.33992478251457
time_elpased: 2.082
batch start
#iterations: 397
currently lose_sum: 68.93086668848991
time_elpased: 2.576
batch start
#iterations: 398
currently lose_sum: 69.08239984512329
time_elpased: 2.115
batch start
#iterations: 399
currently lose_sum: 68.77299430966377
time_elpased: 2.525
start validation test
0.571907216495
0.628785114922
0.354739117011
0.453582472531
0.572288488589
79.402
acc: 0.627
pre: 0.636
rec: 0.597
F1: 0.616
auc: 0.627
