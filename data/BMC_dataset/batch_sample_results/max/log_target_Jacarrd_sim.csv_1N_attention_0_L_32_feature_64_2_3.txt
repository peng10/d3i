start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.28919506072998
time_elpased: 1.926
batch start
#iterations: 1
currently lose_sum: 99.7888046503067
time_elpased: 1.874
batch start
#iterations: 2
currently lose_sum: 99.30518853664398
time_elpased: 1.855
batch start
#iterations: 3
currently lose_sum: 98.92270678281784
time_elpased: 1.842
batch start
#iterations: 4
currently lose_sum: 98.60552269220352
time_elpased: 1.818
batch start
#iterations: 5
currently lose_sum: 98.33512085676193
time_elpased: 1.869
batch start
#iterations: 6
currently lose_sum: 97.84732949733734
time_elpased: 1.841
batch start
#iterations: 7
currently lose_sum: 97.75923305749893
time_elpased: 1.842
batch start
#iterations: 8
currently lose_sum: 97.28477507829666
time_elpased: 1.852
batch start
#iterations: 9
currently lose_sum: 97.3383778333664
time_elpased: 1.838
batch start
#iterations: 10
currently lose_sum: 96.64342910051346
time_elpased: 1.828
batch start
#iterations: 11
currently lose_sum: 96.37750625610352
time_elpased: 1.869
batch start
#iterations: 12
currently lose_sum: 96.57496339082718
time_elpased: 1.843
batch start
#iterations: 13
currently lose_sum: 96.45340025424957
time_elpased: 1.814
batch start
#iterations: 14
currently lose_sum: 95.69410389661789
time_elpased: 1.861
batch start
#iterations: 15
currently lose_sum: 95.82656902074814
time_elpased: 1.835
batch start
#iterations: 16
currently lose_sum: 95.68118929862976
time_elpased: 1.848
batch start
#iterations: 17
currently lose_sum: 95.56321424245834
time_elpased: 1.81
batch start
#iterations: 18
currently lose_sum: 95.02836471796036
time_elpased: 1.833
batch start
#iterations: 19
currently lose_sum: 94.47912234067917
time_elpased: 1.813
start validation test
0.625154639175
0.641337034467
0.570708110333
0.603964709727
0.625244596265
62.554
batch start
#iterations: 20
currently lose_sum: 94.1475441455841
time_elpased: 1.853
batch start
#iterations: 21
currently lose_sum: 94.27533614635468
time_elpased: 1.834
batch start
#iterations: 22
currently lose_sum: 94.03203195333481
time_elpased: 1.805
batch start
#iterations: 23
currently lose_sum: 93.45090770721436
time_elpased: 1.842
batch start
#iterations: 24
currently lose_sum: 93.29393833875656
time_elpased: 1.857
batch start
#iterations: 25
currently lose_sum: 93.19662761688232
time_elpased: 1.817
batch start
#iterations: 26
currently lose_sum: 92.91575860977173
time_elpased: 1.854
batch start
#iterations: 27
currently lose_sum: 92.86074763536453
time_elpased: 1.834
batch start
#iterations: 28
currently lose_sum: 92.05461198091507
time_elpased: 1.845
batch start
#iterations: 29
currently lose_sum: 92.24352633953094
time_elpased: 1.838
batch start
#iterations: 30
currently lose_sum: 91.93995749950409
time_elpased: 1.832
batch start
#iterations: 31
currently lose_sum: 91.30238193273544
time_elpased: 1.862
batch start
#iterations: 32
currently lose_sum: 91.33432793617249
time_elpased: 1.933
batch start
#iterations: 33
currently lose_sum: 91.28172850608826
time_elpased: 1.941
batch start
#iterations: 34
currently lose_sum: 90.81945765018463
time_elpased: 1.942
batch start
#iterations: 35
currently lose_sum: 91.24547201395035
time_elpased: 1.96
batch start
#iterations: 36
currently lose_sum: 89.96747195720673
time_elpased: 1.945
batch start
#iterations: 37
currently lose_sum: 90.07670766115189
time_elpased: 1.968
batch start
#iterations: 38
currently lose_sum: 89.2679198384285
time_elpased: 1.937
batch start
#iterations: 39
currently lose_sum: 89.39986509084702
time_elpased: 1.949
start validation test
0.602010309278
0.633373445648
0.487546315356
0.550974120384
0.602199427814
64.918
batch start
#iterations: 40
currently lose_sum: 89.52068418264389
time_elpased: 1.909
batch start
#iterations: 41
currently lose_sum: 88.78353935480118
time_elpased: 1.913
batch start
#iterations: 42
currently lose_sum: 88.55068075656891
time_elpased: 1.891
batch start
#iterations: 43
currently lose_sum: 88.47346037626266
time_elpased: 1.938
batch start
#iterations: 44
currently lose_sum: 88.39405012130737
time_elpased: 1.96
batch start
#iterations: 45
currently lose_sum: 88.63212543725967
time_elpased: 1.904
batch start
#iterations: 46
currently lose_sum: 87.86757689714432
time_elpased: 1.92
batch start
#iterations: 47
currently lose_sum: 87.6971629858017
time_elpased: 1.903
batch start
#iterations: 48
currently lose_sum: 87.29491257667542
time_elpased: 1.93
batch start
#iterations: 49
currently lose_sum: 87.986079454422
time_elpased: 1.932
batch start
#iterations: 50
currently lose_sum: 87.39641380310059
time_elpased: 1.934
batch start
#iterations: 51
currently lose_sum: 87.19730484485626
time_elpased: 1.927
batch start
#iterations: 52
currently lose_sum: 86.61239314079285
time_elpased: 1.96
batch start
#iterations: 53
currently lose_sum: 86.51517730951309
time_elpased: 1.926
batch start
#iterations: 54
currently lose_sum: 86.98154556751251
time_elpased: 1.928
batch start
#iterations: 55
currently lose_sum: 86.00142753124237
time_elpased: 1.872
batch start
#iterations: 56
currently lose_sum: 86.0729523897171
time_elpased: 1.931
batch start
#iterations: 57
currently lose_sum: 86.15432071685791
time_elpased: 1.899
batch start
#iterations: 58
currently lose_sum: 85.90850061178207
time_elpased: 1.922
batch start
#iterations: 59
currently lose_sum: 85.80715447664261
time_elpased: 1.864
start validation test
0.598350515464
0.629964874358
0.479930012351
0.544806636289
0.598546170983
66.020
batch start
#iterations: 60
currently lose_sum: 85.05377864837646
time_elpased: 1.905
batch start
#iterations: 61
currently lose_sum: 85.3107453584671
time_elpased: 1.877
batch start
#iterations: 62
currently lose_sum: 85.05856782197952
time_elpased: 1.953
batch start
#iterations: 63
currently lose_sum: 84.74664747714996
time_elpased: 1.886
batch start
#iterations: 64
currently lose_sum: 84.64287596940994
time_elpased: 1.895
batch start
#iterations: 65
currently lose_sum: 84.78521418571472
time_elpased: 1.913
batch start
#iterations: 66
currently lose_sum: 84.90264964103699
time_elpased: 1.937
batch start
#iterations: 67
currently lose_sum: 84.29631993174553
time_elpased: 1.917
batch start
#iterations: 68
currently lose_sum: 83.88051915168762
time_elpased: 1.894
batch start
#iterations: 69
currently lose_sum: 83.32616490125656
time_elpased: 1.889
batch start
#iterations: 70
currently lose_sum: 83.82592701911926
time_elpased: 1.896
batch start
#iterations: 71
currently lose_sum: 83.18542703986168
time_elpased: 1.9
batch start
#iterations: 72
currently lose_sum: 83.10694581270218
time_elpased: 1.899
batch start
#iterations: 73
currently lose_sum: 83.19259285926819
time_elpased: 1.944
batch start
#iterations: 74
currently lose_sum: 82.99697044491768
time_elpased: 1.92
batch start
#iterations: 75
currently lose_sum: 82.72901797294617
time_elpased: 1.947
batch start
#iterations: 76
currently lose_sum: 83.22291252017021
time_elpased: 1.915
batch start
#iterations: 77
currently lose_sum: 82.32219287753105
time_elpased: 1.927
batch start
#iterations: 78
currently lose_sum: 82.75834608078003
time_elpased: 1.897
batch start
#iterations: 79
currently lose_sum: 82.8068699836731
time_elpased: 1.984
start validation test
0.581391752577
0.627743072241
0.403355290243
0.49113352967
0.581685906171
69.684
batch start
#iterations: 80
currently lose_sum: 82.16092681884766
time_elpased: 1.909
batch start
#iterations: 81
currently lose_sum: 82.0664034485817
time_elpased: 1.917
batch start
#iterations: 82
currently lose_sum: 82.92370057106018
time_elpased: 1.968
batch start
#iterations: 83
currently lose_sum: 82.33720862865448
time_elpased: 1.912
batch start
#iterations: 84
currently lose_sum: 81.71118608117104
time_elpased: 1.923
batch start
#iterations: 85
currently lose_sum: 81.94940581917763
time_elpased: 1.917
batch start
#iterations: 86
currently lose_sum: 81.4330785870552
time_elpased: 1.883
batch start
#iterations: 87
currently lose_sum: 81.4250636100769
time_elpased: 1.927
batch start
#iterations: 88
currently lose_sum: 81.16249153017998
time_elpased: 1.887
batch start
#iterations: 89
currently lose_sum: 81.60293340682983
time_elpased: 2.057
batch start
#iterations: 90
currently lose_sum: 80.76980829238892
time_elpased: 2.002
batch start
#iterations: 91
currently lose_sum: 81.40315216779709
time_elpased: 2.069
batch start
#iterations: 92
currently lose_sum: 80.60858792066574
time_elpased: 2.023
batch start
#iterations: 93
currently lose_sum: 80.96040838956833
time_elpased: 2.094
batch start
#iterations: 94
currently lose_sum: 80.68347206711769
time_elpased: 2.065
batch start
#iterations: 95
currently lose_sum: 80.21567261219025
time_elpased: 2.061
batch start
#iterations: 96
currently lose_sum: 80.35649773478508
time_elpased: 2.019
batch start
#iterations: 97
currently lose_sum: 79.56297156214714
time_elpased: 2.063
batch start
#iterations: 98
currently lose_sum: 80.0226631462574
time_elpased: 2.079
batch start
#iterations: 99
currently lose_sum: 80.29898259043694
time_elpased: 2.052
start validation test
0.583711340206
0.615233277122
0.450596953479
0.520199619772
0.583931273105
71.367
batch start
#iterations: 100
currently lose_sum: 80.04844215512276
time_elpased: 2.102
batch start
#iterations: 101
currently lose_sum: 80.7899360358715
time_elpased: 2.065
batch start
#iterations: 102
currently lose_sum: 80.11736962199211
time_elpased: 2.023
batch start
#iterations: 103
currently lose_sum: 79.94589871168137
time_elpased: 2.072
batch start
#iterations: 104
currently lose_sum: 79.39836123585701
time_elpased: 2.069
batch start
#iterations: 105
currently lose_sum: 79.08126240968704
time_elpased: 2.025
batch start
#iterations: 106
currently lose_sum: 79.22388252615929
time_elpased: 2.068
batch start
#iterations: 107
currently lose_sum: 79.17267683148384
time_elpased: 2.088
batch start
#iterations: 108
currently lose_sum: 78.74877300858498
time_elpased: 2.117
batch start
#iterations: 109
currently lose_sum: 79.35810106992722
time_elpased: 2.076
batch start
#iterations: 110
currently lose_sum: 78.47402116656303
time_elpased: 2.092
batch start
#iterations: 111
currently lose_sum: 78.96999338269234
time_elpased: 2.025
batch start
#iterations: 112
currently lose_sum: 78.44516670703888
time_elpased: 2.051
batch start
#iterations: 113
currently lose_sum: 78.1331025660038
time_elpased: 2.092
batch start
#iterations: 114
currently lose_sum: 78.69889384508133
time_elpased: 2.061
batch start
#iterations: 115
currently lose_sum: 78.7657345533371
time_elpased: 2.032
batch start
#iterations: 116
currently lose_sum: 78.48550671339035
time_elpased: 2.06
batch start
#iterations: 117
currently lose_sum: 78.09238421916962
time_elpased: 2.025
batch start
#iterations: 118
currently lose_sum: 78.3168143928051
time_elpased: 2.055
batch start
#iterations: 119
currently lose_sum: 78.12058645486832
time_elpased: 2.02
start validation test
0.586030927835
0.612498330885
0.472107863318
0.533217088056
0.586219152642
72.685
batch start
#iterations: 120
currently lose_sum: 77.93914431333542
time_elpased: 2.087
batch start
#iterations: 121
currently lose_sum: 77.75836271047592
time_elpased: 2.048
batch start
#iterations: 122
currently lose_sum: 77.45970311760902
time_elpased: 2.065
batch start
#iterations: 123
currently lose_sum: 78.01667374372482
time_elpased: 2.102
batch start
#iterations: 124
currently lose_sum: 77.38018223643303
time_elpased: 2.024
batch start
#iterations: 125
currently lose_sum: 77.43150362372398
time_elpased: 2.073
batch start
#iterations: 126
currently lose_sum: 76.71147549152374
time_elpased: 2.05
batch start
#iterations: 127
currently lose_sum: 76.90649229288101
time_elpased: 2.08
batch start
#iterations: 128
currently lose_sum: 77.15289363265038
time_elpased: 2.072
batch start
#iterations: 129
currently lose_sum: 76.5822162926197
time_elpased: 2.061
batch start
#iterations: 130
currently lose_sum: 77.23868468403816
time_elpased: 2.097
batch start
#iterations: 131
currently lose_sum: 77.05926474928856
time_elpased: 2.05
batch start
#iterations: 132
currently lose_sum: 76.22781330347061
time_elpased: 2.101
batch start
#iterations: 133
currently lose_sum: 76.85227873921394
time_elpased: 2.108
batch start
#iterations: 134
currently lose_sum: 77.38997387886047
time_elpased: 2.039
batch start
#iterations: 135
currently lose_sum: 76.85609516501427
time_elpased: 2.071
batch start
#iterations: 136
currently lose_sum: 76.83846321702003
time_elpased: 2.048
batch start
#iterations: 137
currently lose_sum: 76.69277694821358
time_elpased: 2.067
batch start
#iterations: 138
currently lose_sum: 75.9110760986805
time_elpased: 2.02
batch start
#iterations: 139
currently lose_sum: 75.8835459947586
time_elpased: 2.078
start validation test
0.584329896907
0.619710144928
0.440098806093
0.51468464131
0.584568196933
75.629
batch start
#iterations: 140
currently lose_sum: 75.83192044496536
time_elpased: 2.052
batch start
#iterations: 141
currently lose_sum: 75.84446310997009
time_elpased: 2.09
batch start
#iterations: 142
currently lose_sum: 75.1592218875885
time_elpased: 2.031
batch start
#iterations: 143
currently lose_sum: 75.97541570663452
time_elpased: 2.037
batch start
#iterations: 144
currently lose_sum: 75.35323724150658
time_elpased: 2.08
batch start
#iterations: 145
currently lose_sum: 75.70894682407379
time_elpased: 2.032
batch start
#iterations: 146
currently lose_sum: 75.3331513106823
time_elpased: 2.076
batch start
#iterations: 147
currently lose_sum: 75.5540041923523
time_elpased: 2.117
batch start
#iterations: 148
currently lose_sum: 75.43089434504509
time_elpased: 2.107
batch start
#iterations: 149
currently lose_sum: 74.88188111782074
time_elpased: 2.022
batch start
#iterations: 150
currently lose_sum: 75.54417315125465
time_elpased: 2.083
batch start
#iterations: 151
currently lose_sum: 75.25528851151466
time_elpased: 2.069
batch start
#iterations: 152
currently lose_sum: 75.19557926058769
time_elpased: 2.097
batch start
#iterations: 153
currently lose_sum: 75.3046405017376
time_elpased: 2.07
batch start
#iterations: 154
currently lose_sum: 75.66072690486908
time_elpased: 2.114
batch start
#iterations: 155
currently lose_sum: 74.99160051345825
time_elpased: 2.021
batch start
#iterations: 156
currently lose_sum: 74.6541508436203
time_elpased: 2.067
batch start
#iterations: 157
currently lose_sum: 74.3778692483902
time_elpased: 2.057
batch start
#iterations: 158
currently lose_sum: 74.58993417024612
time_elpased: 2.095
batch start
#iterations: 159
currently lose_sum: 74.19858130812645
time_elpased: 2.019
start validation test
0.571237113402
0.606037621359
0.411177439275
0.48994358597
0.571501565569
78.135
batch start
#iterations: 160
currently lose_sum: 74.95177882909775
time_elpased: 2.223
batch start
#iterations: 161
currently lose_sum: 73.83931583166122
time_elpased: 2.023
batch start
#iterations: 162
currently lose_sum: 74.18137854337692
time_elpased: 2.058
batch start
#iterations: 163
currently lose_sum: 74.25742655992508
time_elpased: 2.028
batch start
#iterations: 164
currently lose_sum: 74.5567424595356
time_elpased: 2.032
batch start
#iterations: 165
currently lose_sum: 74.74825870990753
time_elpased: 2.009
batch start
#iterations: 166
currently lose_sum: 74.13809829950333
time_elpased: 2.034
batch start
#iterations: 167
currently lose_sum: 73.96782383322716
time_elpased: 2.057
batch start
#iterations: 168
currently lose_sum: 74.1783716082573
time_elpased: 2.071
batch start
#iterations: 169
currently lose_sum: 73.3702481687069
time_elpased: 2.045
batch start
#iterations: 170
currently lose_sum: 73.2072486281395
time_elpased: 2.054
batch start
#iterations: 171
currently lose_sum: 73.86412692070007
time_elpased: 2.071
batch start
#iterations: 172
currently lose_sum: 73.65139257907867
time_elpased: 2.092
batch start
#iterations: 173
currently lose_sum: 73.43854880332947
time_elpased: 2.06
batch start
#iterations: 174
currently lose_sum: 72.82873886823654
time_elpased: 2.064
batch start
#iterations: 175
currently lose_sum: 73.12160220742226
time_elpased: 2.021
batch start
#iterations: 176
currently lose_sum: 73.01001933217049
time_elpased: 2.074
batch start
#iterations: 177
currently lose_sum: 72.72134190797806
time_elpased: 2.079
batch start
#iterations: 178
currently lose_sum: 73.38204142451286
time_elpased: 2.071
batch start
#iterations: 179
currently lose_sum: 73.2494036257267
time_elpased: 2.041
start validation test
0.569536082474
0.618386816999
0.366920543434
0.460564563013
0.56987084586
82.290
batch start
#iterations: 180
currently lose_sum: 73.43888247013092
time_elpased: 2.017
batch start
#iterations: 181
currently lose_sum: 73.81745517253876
time_elpased: 2.066
batch start
#iterations: 182
currently lose_sum: 73.48804876208305
time_elpased: 2.065
batch start
#iterations: 183
currently lose_sum: 72.37775790691376
time_elpased: 2.047
batch start
#iterations: 184
currently lose_sum: 73.26499676704407
time_elpased: 2.084
batch start
#iterations: 185
currently lose_sum: 72.4304603934288
time_elpased: 2.082
batch start
#iterations: 186
currently lose_sum: 73.70424437522888
time_elpased: 2.06
batch start
#iterations: 187
currently lose_sum: 71.6641785800457
time_elpased: 2.098
batch start
#iterations: 188
currently lose_sum: 72.88214766979218
time_elpased: 2.103
batch start
#iterations: 189
currently lose_sum: 72.54225811362267
time_elpased: 2.073
batch start
#iterations: 190
currently lose_sum: 72.30228903889656
time_elpased: 2.059
batch start
#iterations: 191
currently lose_sum: 71.74117347598076
time_elpased: 2.094
batch start
#iterations: 192
currently lose_sum: 72.14742243289948
time_elpased: 2.035
batch start
#iterations: 193
currently lose_sum: 72.53646451234818
time_elpased: 2.044
batch start
#iterations: 194
currently lose_sum: 72.09413489699364
time_elpased: 2.081
batch start
#iterations: 195
currently lose_sum: 71.69481313228607
time_elpased: 2.115
batch start
#iterations: 196
currently lose_sum: 72.10441628098488
time_elpased: 2.072
batch start
#iterations: 197
currently lose_sum: 72.32887050509453
time_elpased: 2.029
batch start
#iterations: 198
currently lose_sum: 72.21065309643745
time_elpased: 2.053
batch start
#iterations: 199
currently lose_sum: 71.46393883228302
time_elpased: 2.066
start validation test
0.56175257732
0.604547020324
0.361259777686
0.452261306533
0.562083833494
82.910
batch start
#iterations: 200
currently lose_sum: 71.68964087963104
time_elpased: 2.054
batch start
#iterations: 201
currently lose_sum: 71.91341426968575
time_elpased: 2.049
batch start
#iterations: 202
currently lose_sum: 71.76963305473328
time_elpased: 2.053
batch start
#iterations: 203
currently lose_sum: 71.91256958246231
time_elpased: 1.984
batch start
#iterations: 204
currently lose_sum: 72.24332842230797
time_elpased: 2.099
batch start
#iterations: 205
currently lose_sum: 71.74085786938667
time_elpased: 2.028
batch start
#iterations: 206
currently lose_sum: 71.40969595313072
time_elpased: 2.09
batch start
#iterations: 207
currently lose_sum: 71.35371473431587
time_elpased: 2.06
batch start
#iterations: 208
currently lose_sum: 71.33072763681412
time_elpased: 2.091
batch start
#iterations: 209
currently lose_sum: 70.64119303226471
time_elpased: 2.004
batch start
#iterations: 210
currently lose_sum: 71.09143036603928
time_elpased: 2.07
batch start
#iterations: 211
currently lose_sum: 71.37586683034897
time_elpased: 2.044
batch start
#iterations: 212
currently lose_sum: 71.40835705399513
time_elpased: 2.102
batch start
#iterations: 213
currently lose_sum: 70.36406183242798
time_elpased: 2.013
batch start
#iterations: 214
currently lose_sum: 70.55913117527962
time_elpased: 2.049
batch start
#iterations: 215
currently lose_sum: 71.30200362205505
time_elpased: 2.03
batch start
#iterations: 216
currently lose_sum: 71.02888175845146
time_elpased: 2.057
batch start
#iterations: 217
currently lose_sum: 70.67187342047691
time_elpased: 2.03
batch start
#iterations: 218
currently lose_sum: 71.31606125831604
time_elpased: 2.044
batch start
#iterations: 219
currently lose_sum: 69.75300621986389
time_elpased: 2.052
start validation test
0.57206185567
0.610849796174
0.40098806093
0.484155585933
0.572344505476
83.126
batch start
#iterations: 220
currently lose_sum: 70.25896486639977
time_elpased: 2.043
batch start
#iterations: 221
currently lose_sum: 70.87585437297821
time_elpased: 2.062
batch start
#iterations: 222
currently lose_sum: 70.8101355433464
time_elpased: 2.047
batch start
#iterations: 223
currently lose_sum: 70.5213083922863
time_elpased: 2.046
batch start
#iterations: 224
currently lose_sum: 70.55405220389366
time_elpased: 2.057
batch start
#iterations: 225
currently lose_sum: 69.88841012120247
time_elpased: 2.028
batch start
#iterations: 226
currently lose_sum: 69.91638842225075
time_elpased: 2.068
batch start
#iterations: 227
currently lose_sum: 69.83950936794281
time_elpased: 2.044
batch start
#iterations: 228
currently lose_sum: 69.51861920952797
time_elpased: 2.067
batch start
#iterations: 229
currently lose_sum: 69.48440009355545
time_elpased: 2.023
batch start
#iterations: 230
currently lose_sum: 70.38358440995216
time_elpased: 2.055
batch start
#iterations: 231
currently lose_sum: 70.51383465528488
time_elpased: 2.098
batch start
#iterations: 232
currently lose_sum: 70.467295140028
time_elpased: 2.072
batch start
#iterations: 233
currently lose_sum: 70.2873503267765
time_elpased: 2.059
batch start
#iterations: 234
currently lose_sum: 70.38785547018051
time_elpased: 2.024
batch start
#iterations: 235
currently lose_sum: 70.41909506917
time_elpased: 2.051
batch start
#iterations: 236
currently lose_sum: 69.87697222828865
time_elpased: 2.035
batch start
#iterations: 237
currently lose_sum: 69.6349081993103
time_elpased: 2.068
batch start
#iterations: 238
currently lose_sum: 70.1725752055645
time_elpased: 2.013
batch start
#iterations: 239
currently lose_sum: 69.82029837369919
time_elpased: 2.061
start validation test
0.572577319588
0.614653784219
0.392857142857
0.479341956549
0.57287425503
85.873
batch start
#iterations: 240
currently lose_sum: 69.64267778396606
time_elpased: 2.053
batch start
#iterations: 241
currently lose_sum: 69.14012172818184
time_elpased: 2.093
batch start
#iterations: 242
currently lose_sum: 69.40956032276154
time_elpased: 2.039
batch start
#iterations: 243
currently lose_sum: 69.14536979794502
time_elpased: 2.049
batch start
#iterations: 244
currently lose_sum: 69.43912747502327
time_elpased: 2.06
batch start
#iterations: 245
currently lose_sum: 70.20673069357872
time_elpased: 2.076
batch start
#iterations: 246
currently lose_sum: 69.06946539878845
time_elpased: 2.028
batch start
#iterations: 247
currently lose_sum: 68.95050209760666
time_elpased: 2.052
batch start
#iterations: 248
currently lose_sum: 68.60387527942657
time_elpased: 2.051
batch start
#iterations: 249
currently lose_sum: 69.70509487390518
time_elpased: 2.003
batch start
#iterations: 250
currently lose_sum: 68.63935688138008
time_elpased: 2.099
batch start
#iterations: 251
currently lose_sum: 68.78348416090012
time_elpased: 2.068
batch start
#iterations: 252
currently lose_sum: 68.83347436785698
time_elpased: 2.089
batch start
#iterations: 253
currently lose_sum: 69.4849306344986
time_elpased: 2.07
batch start
#iterations: 254
currently lose_sum: 68.25394615530968
time_elpased: 2.063
batch start
#iterations: 255
currently lose_sum: 68.47360435128212
time_elpased: 2.032
batch start
#iterations: 256
currently lose_sum: 69.10778421163559
time_elpased: 2.059
batch start
#iterations: 257
currently lose_sum: 68.6558849811554
time_elpased: 2.115
batch start
#iterations: 258
currently lose_sum: 68.64800864458084
time_elpased: 2.041
batch start
#iterations: 259
currently lose_sum: 69.23541095852852
time_elpased: 2.038
start validation test
0.557010309278
0.608974358974
0.322663647592
0.421824542519
0.557397499137
91.149
batch start
#iterations: 260
currently lose_sum: 68.71569120883942
time_elpased: 2.083
batch start
#iterations: 261
currently lose_sum: 68.34838703274727
time_elpased: 2.011
batch start
#iterations: 262
currently lose_sum: 68.50582057237625
time_elpased: 2.058
batch start
#iterations: 263
currently lose_sum: 68.38901114463806
time_elpased: 2.032
batch start
#iterations: 264
currently lose_sum: 68.22171166539192
time_elpased: 2.092
batch start
#iterations: 265
currently lose_sum: 68.74581655859947
time_elpased: 2.064
batch start
#iterations: 266
currently lose_sum: 68.17673403024673
time_elpased: 2.051
batch start
#iterations: 267
currently lose_sum: 68.72744432091713
time_elpased: 2.069
batch start
#iterations: 268
currently lose_sum: 67.57511296868324
time_elpased: 2.094
batch start
#iterations: 269
currently lose_sum: 68.37358531355858
time_elpased: 2.025
batch start
#iterations: 270
currently lose_sum: 67.32771545648575
time_elpased: 2.085
batch start
#iterations: 271
currently lose_sum: 68.4801938533783
time_elpased: 2.062
batch start
#iterations: 272
currently lose_sum: 68.14944550395012
time_elpased: 2.02
batch start
#iterations: 273
currently lose_sum: 67.43775582313538
time_elpased: 2.049
batch start
#iterations: 274
currently lose_sum: 67.70908343791962
time_elpased: 2.098
batch start
#iterations: 275
currently lose_sum: 68.12045574188232
time_elpased: 2.021
batch start
#iterations: 276
currently lose_sum: 66.7927111685276
time_elpased: 2.053
batch start
#iterations: 277
currently lose_sum: 66.42869877815247
time_elpased: 2.071
batch start
#iterations: 278
currently lose_sum: 67.13683587312698
time_elpased: 2.048
batch start
#iterations: 279
currently lose_sum: 68.00394707918167
time_elpased: 2.072
start validation test
0.559175257732
0.598111935266
0.365170852203
0.453476482618
0.559495793718
91.255
batch start
#iterations: 280
currently lose_sum: 67.55513581633568
time_elpased: 2.016
batch start
#iterations: 281
currently lose_sum: 66.74882647395134
time_elpased: 2.026
batch start
#iterations: 282
currently lose_sum: 67.29596617817879
time_elpased: 2.011
batch start
#iterations: 283
currently lose_sum: 66.95898979902267
time_elpased: 2.101
batch start
#iterations: 284
currently lose_sum: 66.59016761183739
time_elpased: 2.017
batch start
#iterations: 285
currently lose_sum: 67.51347428560257
time_elpased: 2.064
batch start
#iterations: 286
currently lose_sum: 67.16063779592514
time_elpased: 2.023
batch start
#iterations: 287
currently lose_sum: 67.64542204141617
time_elpased: 2.024
batch start
#iterations: 288
currently lose_sum: 67.1881014406681
time_elpased: 2.035
batch start
#iterations: 289
currently lose_sum: 67.37569606304169
time_elpased: 2.079
batch start
#iterations: 290
currently lose_sum: 66.83631670475006
time_elpased: 2.033
batch start
#iterations: 291
currently lose_sum: 67.55417940020561
time_elpased: 2.038
batch start
#iterations: 292
currently lose_sum: 66.3061572611332
time_elpased: 1.994
batch start
#iterations: 293
currently lose_sum: 66.01333671808243
time_elpased: 2.11
batch start
#iterations: 294
currently lose_sum: 66.21655756235123
time_elpased: 2.083
batch start
#iterations: 295
currently lose_sum: 65.85582467913628
time_elpased: 2.102
batch start
#iterations: 296
currently lose_sum: 66.95053592324257
time_elpased: 2.014
batch start
#iterations: 297
currently lose_sum: 66.8353923857212
time_elpased: 2.058
batch start
#iterations: 298
currently lose_sum: 66.4896790087223
time_elpased: 2.053
batch start
#iterations: 299
currently lose_sum: 66.94055989384651
time_elpased: 2.069
start validation test
0.564175257732
0.60706401766
0.367949773569
0.458186478693
0.564499463406
92.010
batch start
#iterations: 300
currently lose_sum: 66.37168574333191
time_elpased: 2.034
batch start
#iterations: 301
currently lose_sum: 66.5333680510521
time_elpased: 2.03
batch start
#iterations: 302
currently lose_sum: 66.21606007218361
time_elpased: 2.032
batch start
#iterations: 303
currently lose_sum: 65.86219558119774
time_elpased: 2.027
batch start
#iterations: 304
currently lose_sum: 65.47293463349342
time_elpased: 2.157
batch start
#iterations: 305
currently lose_sum: 66.5495896935463
time_elpased: 2.056
batch start
#iterations: 306
currently lose_sum: 67.67600765824318
time_elpased: 2.046
batch start
#iterations: 307
currently lose_sum: 66.65138784050941
time_elpased: 2.035
batch start
#iterations: 308
currently lose_sum: 65.62238609790802
time_elpased: 2.049
batch start
#iterations: 309
currently lose_sum: 65.71027547121048
time_elpased: 2.067
batch start
#iterations: 310
currently lose_sum: 66.22871926426888
time_elpased: 2.039
batch start
#iterations: 311
currently lose_sum: 65.52377846837044
time_elpased: 2.017
batch start
#iterations: 312
currently lose_sum: 66.41409862041473
time_elpased: 2.048
batch start
#iterations: 313
currently lose_sum: 65.78562930226326
time_elpased: 2.041
batch start
#iterations: 314
currently lose_sum: 65.35122323036194
time_elpased: 2.033
batch start
#iterations: 315
currently lose_sum: 65.54074054956436
time_elpased: 2.113
batch start
#iterations: 316
currently lose_sum: 65.45325344800949
time_elpased: 2.073
batch start
#iterations: 317
currently lose_sum: 65.13224923610687
time_elpased: 2.024
batch start
#iterations: 318
currently lose_sum: 65.32272818684578
time_elpased: 2.067
batch start
#iterations: 319
currently lose_sum: 66.24701115489006
time_elpased: 2.012
start validation test
0.557422680412
0.604552183568
0.336249485385
0.432142857143
0.557788104939
95.484
batch start
#iterations: 320
currently lose_sum: 65.96341070532799
time_elpased: 2.059
batch start
#iterations: 321
currently lose_sum: 65.6021411716938
time_elpased: 2.037
batch start
#iterations: 322
currently lose_sum: 64.97026133537292
time_elpased: 2.038
batch start
#iterations: 323
currently lose_sum: 65.91914334893227
time_elpased: 2.058
batch start
#iterations: 324
currently lose_sum: 65.68443167209625
time_elpased: 2.093
batch start
#iterations: 325
currently lose_sum: 65.18266069889069
time_elpased: 2.096
batch start
#iterations: 326
currently lose_sum: 64.32192319631577
time_elpased: 2.071
batch start
#iterations: 327
currently lose_sum: 64.62536942958832
time_elpased: 2.032
batch start
#iterations: 328
currently lose_sum: 65.19160282611847
time_elpased: 2.055
batch start
#iterations: 329
currently lose_sum: 65.48765757679939
time_elpased: 2.089
batch start
#iterations: 330
currently lose_sum: 65.00090059638023
time_elpased: 2.008
batch start
#iterations: 331
currently lose_sum: 65.13475430011749
time_elpased: 2.072
batch start
#iterations: 332
currently lose_sum: 65.74387729167938
time_elpased: 1.993
batch start
#iterations: 333
currently lose_sum: 64.16492304205894
time_elpased: 2.031
batch start
#iterations: 334
currently lose_sum: 64.75881311297417
time_elpased: 2.007
batch start
#iterations: 335
currently lose_sum: 65.63642033934593
time_elpased: 2.061
batch start
#iterations: 336
currently lose_sum: 64.34880873560905
time_elpased: 2.043
batch start
#iterations: 337
currently lose_sum: 64.7972055375576
time_elpased: 2.026
batch start
#iterations: 338
currently lose_sum: 64.53383532166481
time_elpased: 2.036
batch start
#iterations: 339
currently lose_sum: 64.82263147830963
time_elpased: 2.043
start validation test
0.560257731959
0.606163769934
0.348188554961
0.442308949467
0.560608114738
96.695
batch start
#iterations: 340
currently lose_sum: 64.70244297385216
time_elpased: 2.072
batch start
#iterations: 341
currently lose_sum: 64.26895081996918
time_elpased: 2.016
batch start
#iterations: 342
currently lose_sum: 64.73196494579315
time_elpased: 2.028
batch start
#iterations: 343
currently lose_sum: 64.38289675116539
time_elpased: 2.087
batch start
#iterations: 344
currently lose_sum: 64.27194169163704
time_elpased: 2.017
batch start
#iterations: 345
currently lose_sum: 63.50593474507332
time_elpased: 2.085
batch start
#iterations: 346
currently lose_sum: 64.24207147955894
time_elpased: 1.996
batch start
#iterations: 347
currently lose_sum: 64.18994715809822
time_elpased: 2.053
batch start
#iterations: 348
currently lose_sum: nan
time_elpased: 2.09
train finish final lose is: nan
acc: 0.631
pre: 0.648
rec: 0.576
F1: 0.610
auc: 0.631
