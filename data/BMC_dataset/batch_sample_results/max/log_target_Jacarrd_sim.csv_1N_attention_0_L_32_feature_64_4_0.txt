start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.30636233091354
time_elpased: 2.157
batch start
#iterations: 1
currently lose_sum: 99.78998565673828
time_elpased: 2.135
batch start
#iterations: 2
currently lose_sum: 99.52091836929321
time_elpased: 2.136
batch start
#iterations: 3
currently lose_sum: 98.78589355945587
time_elpased: 2.14
batch start
#iterations: 4
currently lose_sum: 98.58515191078186
time_elpased: 2.145
batch start
#iterations: 5
currently lose_sum: 98.25037956237793
time_elpased: 2.145
batch start
#iterations: 6
currently lose_sum: 97.65520972013474
time_elpased: 2.141
batch start
#iterations: 7
currently lose_sum: 97.35529333353043
time_elpased: 2.144
batch start
#iterations: 8
currently lose_sum: 96.70190244913101
time_elpased: 2.148
batch start
#iterations: 9
currently lose_sum: 96.52989739179611
time_elpased: 2.165
batch start
#iterations: 10
currently lose_sum: 95.84510141611099
time_elpased: 2.137
batch start
#iterations: 11
currently lose_sum: 95.81278085708618
time_elpased: 2.135
batch start
#iterations: 12
currently lose_sum: 95.62136828899384
time_elpased: 2.174
batch start
#iterations: 13
currently lose_sum: 94.86942279338837
time_elpased: 2.181
batch start
#iterations: 14
currently lose_sum: 94.56217968463898
time_elpased: 2.155
batch start
#iterations: 15
currently lose_sum: 94.75202578306198
time_elpased: 2.143
batch start
#iterations: 16
currently lose_sum: 94.15195059776306
time_elpased: 2.153
batch start
#iterations: 17
currently lose_sum: 94.08696633577347
time_elpased: 2.168
batch start
#iterations: 18
currently lose_sum: 93.56152707338333
time_elpased: 2.143
batch start
#iterations: 19
currently lose_sum: 93.21839731931686
time_elpased: 2.143
start validation test
0.634278350515
0.624430523918
0.677060821241
0.649681528662
0.634203239289
62.138
batch start
#iterations: 20
currently lose_sum: 93.29408621788025
time_elpased: 2.17
batch start
#iterations: 21
currently lose_sum: 92.77355426549911
time_elpased: 2.157
batch start
#iterations: 22
currently lose_sum: 92.20791739225388
time_elpased: 2.132
batch start
#iterations: 23
currently lose_sum: 92.47091573476791
time_elpased: 2.15
batch start
#iterations: 24
currently lose_sum: 91.76610642671585
time_elpased: 2.149
batch start
#iterations: 25
currently lose_sum: 91.08084124326706
time_elpased: 2.129
batch start
#iterations: 26
currently lose_sum: 91.32148814201355
time_elpased: 2.162
batch start
#iterations: 27
currently lose_sum: 90.80133664608002
time_elpased: 2.146
batch start
#iterations: 28
currently lose_sum: 90.67241299152374
time_elpased: 2.152
batch start
#iterations: 29
currently lose_sum: 90.47944796085358
time_elpased: 2.144
batch start
#iterations: 30
currently lose_sum: 89.95936119556427
time_elpased: 2.155
batch start
#iterations: 31
currently lose_sum: 89.87537544965744
time_elpased: 2.145
batch start
#iterations: 32
currently lose_sum: 88.98958510160446
time_elpased: 2.151
batch start
#iterations: 33
currently lose_sum: 89.62700402736664
time_elpased: 2.126
batch start
#iterations: 34
currently lose_sum: 89.26471847295761
time_elpased: 2.117
batch start
#iterations: 35
currently lose_sum: 88.74830067157745
time_elpased: 2.148
batch start
#iterations: 36
currently lose_sum: 88.41609919071198
time_elpased: 2.147
batch start
#iterations: 37
currently lose_sum: 87.99710887670517
time_elpased: 2.156
batch start
#iterations: 38
currently lose_sum: 88.14733892679214
time_elpased: 2.134
batch start
#iterations: 39
currently lose_sum: 88.06671679019928
time_elpased: 2.182
start validation test
0.61587628866
0.627090113343
0.575074611506
0.599957053897
0.615947922297
64.540
batch start
#iterations: 40
currently lose_sum: 87.47929209470749
time_elpased: 2.138
batch start
#iterations: 41
currently lose_sum: 87.42552876472473
time_elpased: 2.142
batch start
#iterations: 42
currently lose_sum: 87.3140874505043
time_elpased: 2.128
batch start
#iterations: 43
currently lose_sum: 86.44103503227234
time_elpased: 2.171
batch start
#iterations: 44
currently lose_sum: 86.679523229599
time_elpased: 2.137
batch start
#iterations: 45
currently lose_sum: 86.28319227695465
time_elpased: 2.143
batch start
#iterations: 46
currently lose_sum: 85.82999312877655
time_elpased: 2.13
batch start
#iterations: 47
currently lose_sum: 85.43046021461487
time_elpased: 2.132
batch start
#iterations: 48
currently lose_sum: 86.5462554693222
time_elpased: 2.141
batch start
#iterations: 49
currently lose_sum: 86.19059681892395
time_elpased: 2.14
batch start
#iterations: 50
currently lose_sum: 85.58078736066818
time_elpased: 2.148
batch start
#iterations: 51
currently lose_sum: 85.24838203191757
time_elpased: 2.188
batch start
#iterations: 52
currently lose_sum: 85.3474286198616
time_elpased: 2.148
batch start
#iterations: 53
currently lose_sum: 84.84557428956032
time_elpased: 2.114
batch start
#iterations: 54
currently lose_sum: 84.64843666553497
time_elpased: 2.178
batch start
#iterations: 55
currently lose_sum: 84.28234788775444
time_elpased: 2.185
batch start
#iterations: 56
currently lose_sum: 84.10904663801193
time_elpased: 2.182
batch start
#iterations: 57
currently lose_sum: 84.2560424208641
time_elpased: 2.169
batch start
#iterations: 58
currently lose_sum: 84.14479380846024
time_elpased: 2.189
batch start
#iterations: 59
currently lose_sum: 84.27306741476059
time_elpased: 2.155
start validation test
0.59912371134
0.626367899948
0.494802922713
0.552866095556
0.599306862575
68.591
batch start
#iterations: 60
currently lose_sum: 83.23660409450531
time_elpased: 2.152
batch start
#iterations: 61
currently lose_sum: 84.02857911586761
time_elpased: 2.211
batch start
#iterations: 62
currently lose_sum: 83.21429854631424
time_elpased: 2.191
batch start
#iterations: 63
currently lose_sum: 82.76080369949341
time_elpased: 2.18
batch start
#iterations: 64
currently lose_sum: 83.31756520271301
time_elpased: 2.154
batch start
#iterations: 65
currently lose_sum: 82.2426515519619
time_elpased: 2.198
batch start
#iterations: 66
currently lose_sum: 81.488430082798
time_elpased: 2.157
batch start
#iterations: 67
currently lose_sum: 82.58296185731888
time_elpased: 2.184
batch start
#iterations: 68
currently lose_sum: 82.6528796851635
time_elpased: 2.185
batch start
#iterations: 69
currently lose_sum: 81.86071494221687
time_elpased: 2.182
batch start
#iterations: 70
currently lose_sum: 81.79914605617523
time_elpased: 2.159
batch start
#iterations: 71
currently lose_sum: 81.49293571710587
time_elpased: 2.2
batch start
#iterations: 72
currently lose_sum: 80.78695434331894
time_elpased: 2.194
batch start
#iterations: 73
currently lose_sum: 80.95834010839462
time_elpased: 2.192
batch start
#iterations: 74
currently lose_sum: 80.8309616446495
time_elpased: 2.203
batch start
#iterations: 75
currently lose_sum: 81.30742412805557
time_elpased: 2.177
batch start
#iterations: 76
currently lose_sum: 80.13046923279762
time_elpased: 2.171
batch start
#iterations: 77
currently lose_sum: 80.58976283669472
time_elpased: 2.179
batch start
#iterations: 78
currently lose_sum: 80.46477583050728
time_elpased: 2.176
batch start
#iterations: 79
currently lose_sum: 79.95821070671082
time_elpased: 2.172
start validation test
0.605824742268
0.624041227229
0.535864978903
0.576601517081
0.605947567423
73.003
batch start
#iterations: 80
currently lose_sum: 80.5810806453228
time_elpased: 2.154
batch start
#iterations: 81
currently lose_sum: 80.20231369137764
time_elpased: 2.169
batch start
#iterations: 82
currently lose_sum: 80.09635987877846
time_elpased: 2.141
batch start
#iterations: 83
currently lose_sum: 80.17949801683426
time_elpased: 2.168
batch start
#iterations: 84
currently lose_sum: 79.48359915614128
time_elpased: 2.156
batch start
#iterations: 85
currently lose_sum: 79.87726730108261
time_elpased: 2.287
batch start
#iterations: 86
currently lose_sum: 79.28094634413719
time_elpased: 2.163
batch start
#iterations: 87
currently lose_sum: 79.42612498998642
time_elpased: 2.191
batch start
#iterations: 88
currently lose_sum: 78.90705695748329
time_elpased: 2.166
batch start
#iterations: 89
currently lose_sum: 78.38398098945618
time_elpased: 2.169
batch start
#iterations: 90
currently lose_sum: 79.40367397665977
time_elpased: 2.174
batch start
#iterations: 91
currently lose_sum: 78.79114571213722
time_elpased: 2.15
batch start
#iterations: 92
currently lose_sum: 78.52872517704964
time_elpased: 2.221
batch start
#iterations: 93
currently lose_sum: 78.8032797574997
time_elpased: 2.148
batch start
#iterations: 94
currently lose_sum: 77.84818616509438
time_elpased: 2.209
batch start
#iterations: 95
currently lose_sum: 77.71461483836174
time_elpased: 2.158
batch start
#iterations: 96
currently lose_sum: 78.30459696054459
time_elpased: 2.172
batch start
#iterations: 97
currently lose_sum: 77.451750934124
time_elpased: 2.169
batch start
#iterations: 98
currently lose_sum: 77.96401351690292
time_elpased: 2.191
batch start
#iterations: 99
currently lose_sum: 77.87231108546257
time_elpased: 2.193
start validation test
0.60587628866
0.634045307443
0.50406504065
0.561632840271
0.606055034009
77.533
batch start
#iterations: 100
currently lose_sum: 77.15108495950699
time_elpased: 2.154
batch start
#iterations: 101
currently lose_sum: 77.38163414597511
time_elpased: 2.19
batch start
#iterations: 102
currently lose_sum: 76.98276484012604
time_elpased: 2.148
batch start
#iterations: 103
currently lose_sum: 77.28223928809166
time_elpased: 2.173
batch start
#iterations: 104
currently lose_sum: 76.60932165384293
time_elpased: 2.14
batch start
#iterations: 105
currently lose_sum: 76.64011341333389
time_elpased: 2.17
batch start
#iterations: 106
currently lose_sum: 76.75290158390999
time_elpased: 2.163
batch start
#iterations: 107
currently lose_sum: 76.272789478302
time_elpased: 2.187
batch start
#iterations: 108
currently lose_sum: 76.24150240421295
time_elpased: 2.159
batch start
#iterations: 109
currently lose_sum: 77.1462898850441
time_elpased: 2.187
batch start
#iterations: 110
currently lose_sum: 75.39197063446045
time_elpased: 2.177
batch start
#iterations: 111
currently lose_sum: 76.05404654145241
time_elpased: 2.156
batch start
#iterations: 112
currently lose_sum: 75.52725952863693
time_elpased: 2.171
batch start
#iterations: 113
currently lose_sum: 75.81374070048332
time_elpased: 2.194
batch start
#iterations: 114
currently lose_sum: 75.17454800009727
time_elpased: 2.188
batch start
#iterations: 115
currently lose_sum: 76.17503908276558
time_elpased: 2.191
batch start
#iterations: 116
currently lose_sum: 75.91935801506042
time_elpased: 2.168
batch start
#iterations: 117
currently lose_sum: 75.52900090813637
time_elpased: 2.171
batch start
#iterations: 118
currently lose_sum: 75.05436778068542
time_elpased: 2.17
batch start
#iterations: 119
currently lose_sum: 75.18472456932068
time_elpased: 2.184
start validation test
0.590515463918
0.61824729892
0.476999073788
0.538515162077
0.590714759449
79.221
batch start
#iterations: 120
currently lose_sum: 74.71326437592506
time_elpased: 2.164
batch start
#iterations: 121
currently lose_sum: 74.96401420235634
time_elpased: 2.191
batch start
#iterations: 122
currently lose_sum: 74.85854053497314
time_elpased: 2.152
batch start
#iterations: 123
currently lose_sum: 75.24618059396744
time_elpased: 2.148
batch start
#iterations: 124
currently lose_sum: 75.18542397022247
time_elpased: 2.141
batch start
#iterations: 125
currently lose_sum: 74.75945723056793
time_elpased: 2.178
batch start
#iterations: 126
currently lose_sum: 74.75136235356331
time_elpased: 2.166
batch start
#iterations: 127
currently lose_sum: 73.73117992281914
time_elpased: 2.181
batch start
#iterations: 128
currently lose_sum: 74.27691167593002
time_elpased: 2.166
batch start
#iterations: 129
currently lose_sum: 74.24630284309387
time_elpased: 2.171
batch start
#iterations: 130
currently lose_sum: 74.52542459964752
time_elpased: 2.149
batch start
#iterations: 131
currently lose_sum: 74.3313438296318
time_elpased: 2.16
batch start
#iterations: 132
currently lose_sum: 73.99121445417404
time_elpased: 2.155
batch start
#iterations: 133
currently lose_sum: 73.41266071796417
time_elpased: 2.212
batch start
#iterations: 134
currently lose_sum: 74.21023979783058
time_elpased: 2.178
batch start
#iterations: 135
currently lose_sum: 74.19961366057396
time_elpased: 2.233
batch start
#iterations: 136
currently lose_sum: 73.8384203016758
time_elpased: 2.196
batch start
#iterations: 137
currently lose_sum: 72.60606196522713
time_elpased: 2.172
batch start
#iterations: 138
currently lose_sum: 73.62066760659218
time_elpased: 2.189
batch start
#iterations: 139
currently lose_sum: 72.78921815752983
time_elpased: 2.158
start validation test
0.589432989691
0.633740458015
0.427189461768
0.510358394295
0.589717833228
87.098
batch start
#iterations: 140
currently lose_sum: 72.84844386577606
time_elpased: 2.155
batch start
#iterations: 141
currently lose_sum: 72.43291160464287
time_elpased: 2.2
batch start
#iterations: 142
currently lose_sum: 73.37490028142929
time_elpased: 2.15
batch start
#iterations: 143
currently lose_sum: 72.7129856646061
time_elpased: 2.146
batch start
#iterations: 144
currently lose_sum: 72.2549339234829
time_elpased: 2.142
batch start
#iterations: 145
currently lose_sum: 72.17373687028885
time_elpased: 2.177
batch start
#iterations: 146
currently lose_sum: 72.37406727671623
time_elpased: 2.158
batch start
#iterations: 147
currently lose_sum: 72.12456968426704
time_elpased: 2.171
batch start
#iterations: 148
currently lose_sum: 72.80503723025322
time_elpased: 2.157
batch start
#iterations: 149
currently lose_sum: 71.72189566493034
time_elpased: 2.168
batch start
#iterations: 150
currently lose_sum: 72.05832594633102
time_elpased: 2.174
batch start
#iterations: 151
currently lose_sum: 72.30897408723831
time_elpased: 2.185
batch start
#iterations: 152
currently lose_sum: 72.15300637483597
time_elpased: 2.175
batch start
#iterations: 153
currently lose_sum: 71.49201220273972
time_elpased: 2.2
batch start
#iterations: 154
currently lose_sum: 71.80550196766853
time_elpased: 2.231
batch start
#iterations: 155
currently lose_sum: 71.53222692012787
time_elpased: 2.284
batch start
#iterations: 156
currently lose_sum: 70.67047715187073
time_elpased: 2.196
batch start
#iterations: 157
currently lose_sum: 71.56625100970268
time_elpased: 2.18
batch start
#iterations: 158
currently lose_sum: 71.22544121742249
time_elpased: 2.188
batch start
#iterations: 159
currently lose_sum: 72.08460655808449
time_elpased: 2.183
start validation test
0.578659793814
0.630873621713
0.382731295667
0.476428388419
0.579003776513
90.619
batch start
#iterations: 160
currently lose_sum: 71.3366671204567
time_elpased: 2.153
batch start
#iterations: 161
currently lose_sum: 70.95245543122292
time_elpased: 2.185
batch start
#iterations: 162
currently lose_sum: 71.16878864169121
time_elpased: 2.152
batch start
#iterations: 163
currently lose_sum: 70.47967460751534
time_elpased: 2.182
batch start
#iterations: 164
currently lose_sum: 70.9825197160244
time_elpased: 2.17
batch start
#iterations: 165
currently lose_sum: 71.28909128904343
time_elpased: 2.181
batch start
#iterations: 166
currently lose_sum: 70.52054604887962
time_elpased: 2.162
batch start
#iterations: 167
currently lose_sum: 70.01388368010521
time_elpased: 2.16
batch start
#iterations: 168
currently lose_sum: 70.35599061846733
time_elpased: 2.161
batch start
#iterations: 169
currently lose_sum: 69.91648417711258
time_elpased: 2.131
batch start
#iterations: 170
currently lose_sum: 70.04313379526138
time_elpased: 2.197
batch start
#iterations: 171
currently lose_sum: 70.08928740024567
time_elpased: 2.17
batch start
#iterations: 172
currently lose_sum: 69.8816311955452
time_elpased: 2.18
batch start
#iterations: 173
currently lose_sum: 69.27879849076271
time_elpased: 2.165
batch start
#iterations: 174
currently lose_sum: 69.95815953612328
time_elpased: 2.178
batch start
#iterations: 175
currently lose_sum: 69.97267463803291
time_elpased: 2.192
batch start
#iterations: 176
currently lose_sum: 69.27585074305534
time_elpased: 2.19
batch start
#iterations: 177
currently lose_sum: 69.11319562792778
time_elpased: 2.191
batch start
#iterations: 178
currently lose_sum: 69.14833524823189
time_elpased: 2.171
batch start
#iterations: 179
currently lose_sum: 68.88692733645439
time_elpased: 2.168
start validation test
0.578608247423
0.617745876604
0.416280745086
0.497387027359
0.57889323839
94.997
batch start
#iterations: 180
currently lose_sum: 68.78366753458977
time_elpased: 2.134
batch start
#iterations: 181
currently lose_sum: 69.038319170475
time_elpased: 2.17
batch start
#iterations: 182
currently lose_sum: 69.012840539217
time_elpased: 2.173
batch start
#iterations: 183
currently lose_sum: 68.02961990237236
time_elpased: 2.173
batch start
#iterations: 184
currently lose_sum: 68.93692833185196
time_elpased: 2.174
batch start
#iterations: 185
currently lose_sum: 68.72048625349998
time_elpased: 2.186
batch start
#iterations: 186
currently lose_sum: 68.4438048005104
time_elpased: 2.182
batch start
#iterations: 187
currently lose_sum: 68.25001427531242
time_elpased: 2.18
batch start
#iterations: 188
currently lose_sum: 68.00347313284874
time_elpased: 2.169
batch start
#iterations: 189
currently lose_sum: 69.07751604914665
time_elpased: 2.163
batch start
#iterations: 190
currently lose_sum: 67.92315837740898
time_elpased: 2.158
batch start
#iterations: 191
currently lose_sum: 68.05876195430756
time_elpased: 2.186
batch start
#iterations: 192
currently lose_sum: 67.13910195231438
time_elpased: 2.189
batch start
#iterations: 193
currently lose_sum: 68.40721374750137
time_elpased: 2.106
batch start
#iterations: 194
currently lose_sum: 67.57353287935257
time_elpased: 2.196
batch start
#iterations: 195
currently lose_sum: 67.88233050704002
time_elpased: 2.186
batch start
#iterations: 196
currently lose_sum: 67.58490914106369
time_elpased: 2.201
batch start
#iterations: 197
currently lose_sum: 68.22097334265709
time_elpased: 2.135
batch start
#iterations: 198
currently lose_sum: 66.99376705288887
time_elpased: 2.152
batch start
#iterations: 199
currently lose_sum: 68.00287610292435
time_elpased: 2.165
start validation test
0.587164948454
0.625036603221
0.439333127508
0.515984770653
0.587424490017
98.797
batch start
#iterations: 200
currently lose_sum: 67.79552060365677
time_elpased: 2.172
batch start
#iterations: 201
currently lose_sum: 67.40418118238449
time_elpased: 2.186
batch start
#iterations: 202
currently lose_sum: 66.89724871516228
time_elpased: 2.24
batch start
#iterations: 203
currently lose_sum: 67.47772672772408
time_elpased: 2.169
batch start
#iterations: 204
currently lose_sum: 67.18741178512573
time_elpased: 2.173
batch start
#iterations: 205
currently lose_sum: 67.28251358866692
time_elpased: 2.182
batch start
#iterations: 206
currently lose_sum: 67.40405943989754
time_elpased: 2.137
batch start
#iterations: 207
currently lose_sum: 66.03677609562874
time_elpased: 2.195
batch start
#iterations: 208
currently lose_sum: 66.64997181296349
time_elpased: 2.179
batch start
#iterations: 209
currently lose_sum: 65.95289379358292
time_elpased: 2.171
batch start
#iterations: 210
currently lose_sum: 66.88218247890472
time_elpased: 2.185
batch start
#iterations: 211
currently lose_sum: 66.09085354208946
time_elpased: 2.182
batch start
#iterations: 212
currently lose_sum: 65.82240587472916
time_elpased: 2.184
batch start
#iterations: 213
currently lose_sum: 65.98526453971863
time_elpased: 2.174
batch start
#iterations: 214
currently lose_sum: 66.3490520119667
time_elpased: 2.186
batch start
#iterations: 215
currently lose_sum: 66.66801804304123
time_elpased: 2.154
batch start
#iterations: 216
currently lose_sum: 66.24911364912987
time_elpased: 2.204
batch start
#iterations: 217
currently lose_sum: 65.49138250946999
time_elpased: 2.177
batch start
#iterations: 218
currently lose_sum: 65.25470516085625
time_elpased: 2.195
batch start
#iterations: 219
currently lose_sum: 66.00171431899071
time_elpased: 2.175
start validation test
0.583453608247
0.613991081382
0.453432129258
0.521636180667
0.583681881008
100.123
batch start
#iterations: 220
currently lose_sum: 66.59285631775856
time_elpased: 2.156
batch start
#iterations: 221
currently lose_sum: 65.82192778587341
time_elpased: 2.168
batch start
#iterations: 222
currently lose_sum: 65.53718921542168
time_elpased: 2.165
batch start
#iterations: 223
currently lose_sum: 65.45608431100845
time_elpased: 2.178
batch start
#iterations: 224
currently lose_sum: 66.23621988296509
time_elpased: 2.173
batch start
#iterations: 225
currently lose_sum: 65.6832644045353
time_elpased: 2.151
batch start
#iterations: 226
currently lose_sum: 64.9841040968895
time_elpased: 2.214
batch start
#iterations: 227
currently lose_sum: 65.13441348075867
time_elpased: 2.18
batch start
#iterations: 228
currently lose_sum: 65.26489055156708
time_elpased: 2.171
batch start
#iterations: 229
currently lose_sum: 65.26889395713806
time_elpased: 2.15
batch start
#iterations: 230
currently lose_sum: 64.45247846841812
time_elpased: 2.159
batch start
#iterations: 231
currently lose_sum: 65.10129326581955
time_elpased: 2.153
batch start
#iterations: 232
currently lose_sum: 64.15271389484406
time_elpased: 2.176
batch start
#iterations: 233
currently lose_sum: 64.5988661646843
time_elpased: 2.184
batch start
#iterations: 234
currently lose_sum: 64.04271897673607
time_elpased: 2.174
batch start
#iterations: 235
currently lose_sum: 64.17327830195427
time_elpased: 2.185
batch start
#iterations: 236
currently lose_sum: 64.99016776680946
time_elpased: 2.221
batch start
#iterations: 237
currently lose_sum: 64.22893977165222
time_elpased: 2.205
batch start
#iterations: 238
currently lose_sum: 63.76046934723854
time_elpased: 2.187
batch start
#iterations: 239
currently lose_sum: 64.21748375892639
time_elpased: 2.193
start validation test
0.563298969072
0.635768811341
0.299989708758
0.407635295763
0.563761249091
106.133
batch start
#iterations: 240
currently lose_sum: 63.49470233917236
time_elpased: 2.169
batch start
#iterations: 241
currently lose_sum: 63.77653795480728
time_elpased: 2.166
batch start
#iterations: 242
currently lose_sum: 64.1822372674942
time_elpased: 2.169
batch start
#iterations: 243
currently lose_sum: 63.826608180999756
time_elpased: 2.134
batch start
#iterations: 244
currently lose_sum: 63.71620401740074
time_elpased: 2.134
batch start
#iterations: 245
currently lose_sum: 63.74160638451576
time_elpased: 2.181
batch start
#iterations: 246
currently lose_sum: 63.05948135256767
time_elpased: 2.153
batch start
#iterations: 247
currently lose_sum: 63.104087829589844
time_elpased: 2.187
batch start
#iterations: 248
currently lose_sum: 62.98866903781891
time_elpased: 2.184
batch start
#iterations: 249
currently lose_sum: 63.53902179002762
time_elpased: 2.149
batch start
#iterations: 250
currently lose_sum: 62.92785128951073
time_elpased: 2.167
batch start
#iterations: 251
currently lose_sum: 62.71388217806816
time_elpased: 2.183
batch start
#iterations: 252
currently lose_sum: 63.15010032057762
time_elpased: 2.211
batch start
#iterations: 253
currently lose_sum: 62.72782441973686
time_elpased: 2.178
batch start
#iterations: 254
currently lose_sum: 62.84036785364151
time_elpased: 2.206
batch start
#iterations: 255
currently lose_sum: 62.70784944295883
time_elpased: 2.15
batch start
#iterations: 256
currently lose_sum: 62.82134988903999
time_elpased: 2.166
batch start
#iterations: 257
currently lose_sum: 62.71054396033287
time_elpased: 2.266
batch start
#iterations: 258
currently lose_sum: 62.49003079533577
time_elpased: 2.195
batch start
#iterations: 259
currently lose_sum: 62.29019120335579
time_elpased: 2.216
start validation test
0.576134020619
0.626567265334
0.380570134815
0.47352583392
0.576477363184
109.264
batch start
#iterations: 260
currently lose_sum: 61.110432118177414
time_elpased: 2.19
batch start
#iterations: 261
currently lose_sum: 61.63239762187004
time_elpased: 2.2
batch start
#iterations: 262
currently lose_sum: 62.31762778759003
time_elpased: 2.155
batch start
#iterations: 263
currently lose_sum: 62.2029145359993
time_elpased: 2.185
batch start
#iterations: 264
currently lose_sum: 61.60812935233116
time_elpased: 2.207
batch start
#iterations: 265
currently lose_sum: 61.06282442808151
time_elpased: 2.18
batch start
#iterations: 266
currently lose_sum: 62.3864940404892
time_elpased: 2.186
batch start
#iterations: 267
currently lose_sum: 60.94909280538559
time_elpased: 2.173
batch start
#iterations: 268
currently lose_sum: 61.465018182992935
time_elpased: 2.173
batch start
#iterations: 269
currently lose_sum: 61.29218515753746
time_elpased: 2.16
batch start
#iterations: 270
currently lose_sum: 61.74859353899956
time_elpased: 2.177
batch start
#iterations: 271
currently lose_sum: 60.67597481608391
time_elpased: 2.181
batch start
#iterations: 272
currently lose_sum: 61.3026827275753
time_elpased: 2.188
batch start
#iterations: 273
currently lose_sum: 61.58834049105644
time_elpased: 2.193
batch start
#iterations: 274
currently lose_sum: 60.84880980849266
time_elpased: 2.191
batch start
#iterations: 275
currently lose_sum: 61.391039967536926
time_elpased: 2.161
batch start
#iterations: 276
currently lose_sum: 60.377982556819916
time_elpased: 2.176
batch start
#iterations: 277
currently lose_sum: 60.52910766005516
time_elpased: 2.237
batch start
#iterations: 278
currently lose_sum: 60.59178739786148
time_elpased: 2.22
batch start
#iterations: 279
currently lose_sum: 60.26300433278084
time_elpased: 2.173
start validation test
0.56793814433
0.624093697713
0.345476999074
0.444753577107
0.568328709183
117.512
batch start
#iterations: 280
currently lose_sum: 60.29847800731659
time_elpased: 2.138
batch start
#iterations: 281
currently lose_sum: 60.450724333524704
time_elpased: 2.209
batch start
#iterations: 282
currently lose_sum: 59.5937736928463
time_elpased: 2.18
batch start
#iterations: 283
currently lose_sum: 59.48327833414078
time_elpased: 2.189
batch start
#iterations: 284
currently lose_sum: 59.50389415025711
time_elpased: 2.171
batch start
#iterations: 285
currently lose_sum: 59.40149614214897
time_elpased: 2.182
batch start
#iterations: 286
currently lose_sum: 59.98690450191498
time_elpased: 2.218
batch start
#iterations: 287
currently lose_sum: 59.40929552912712
time_elpased: 2.161
batch start
#iterations: 288
currently lose_sum: 59.43451026082039
time_elpased: 2.176
batch start
#iterations: 289
currently lose_sum: 58.67151302099228
time_elpased: 2.167
batch start
#iterations: 290
currently lose_sum: 59.26296874880791
time_elpased: 2.216
batch start
#iterations: 291
currently lose_sum: 58.82843226194382
time_elpased: 2.173
batch start
#iterations: 292
currently lose_sum: 59.251852601766586
time_elpased: 2.196
batch start
#iterations: 293
currently lose_sum: 58.907998919487
time_elpased: 2.159
batch start
#iterations: 294
currently lose_sum: 59.28320625424385
time_elpased: 2.183
batch start
#iterations: 295
currently lose_sum: 58.76613187789917
time_elpased: 2.18
batch start
#iterations: 296
currently lose_sum: 58.86818066239357
time_elpased: 2.172
batch start
#iterations: 297
currently lose_sum: 58.950385481119156
time_elpased: 2.172
batch start
#iterations: 298
currently lose_sum: 58.72602131962776
time_elpased: 2.235
batch start
#iterations: 299
currently lose_sum: 57.854007571935654
time_elpased: 2.139
start validation test
0.561443298969
0.625963742446
0.309148914274
0.41388812345
0.561886240675
125.189
batch start
#iterations: 300
currently lose_sum: 58.77638599276543
time_elpased: 2.124
batch start
#iterations: 301
currently lose_sum: 59.16863638162613
time_elpased: 2.147
batch start
#iterations: 302
currently lose_sum: 59.05895185470581
time_elpased: 2.134
batch start
#iterations: 303
currently lose_sum: 57.85422241687775
time_elpased: 2.12
batch start
#iterations: 304
currently lose_sum: 57.66372153162956
time_elpased: 2.148
batch start
#iterations: 305
currently lose_sum: 58.04062768816948
time_elpased: 2.133
batch start
#iterations: 306
currently lose_sum: 58.43564286828041
time_elpased: 2.154
batch start
#iterations: 307
currently lose_sum: 57.82401606440544
time_elpased: 2.132
batch start
#iterations: 308
currently lose_sum: 58.851496279239655
time_elpased: 2.17
batch start
#iterations: 309
currently lose_sum: 57.43467700481415
time_elpased: 2.147
batch start
#iterations: 310
currently lose_sum: 57.19464358687401
time_elpased: 2.131
batch start
#iterations: 311
currently lose_sum: 57.46617937088013
time_elpased: 2.111
batch start
#iterations: 312
currently lose_sum: 56.924575209617615
time_elpased: 2.126
batch start
#iterations: 313
currently lose_sum: 57.70739388465881
time_elpased: 2.127
batch start
#iterations: 314
currently lose_sum: 57.75074455142021
time_elpased: 2.137
batch start
#iterations: 315
currently lose_sum: 57.04059374332428
time_elpased: 2.134
batch start
#iterations: 316
currently lose_sum: 56.781792402267456
time_elpased: 2.132
batch start
#iterations: 317
currently lose_sum: 57.04843080043793
time_elpased: 2.141
batch start
#iterations: 318
currently lose_sum: 57.5394926071167
time_elpased: 2.146
batch start
#iterations: 319
currently lose_sum: 56.30914947390556
time_elpased: 2.136
start validation test
0.559329896907
0.630298973672
0.29072759082
0.397915346151
0.559801469685
127.694
batch start
#iterations: 320
currently lose_sum: 57.71721962094307
time_elpased: 2.126
batch start
#iterations: 321
currently lose_sum: 57.21353632211685
time_elpased: 2.141
batch start
#iterations: 322
currently lose_sum: 57.37246924638748
time_elpased: 2.147
batch start
#iterations: 323
currently lose_sum: 57.2700572013855
time_elpased: 2.136
batch start
#iterations: 324
currently lose_sum: 56.19352778792381
time_elpased: 2.149
batch start
#iterations: 325
currently lose_sum: 56.782299250364304
time_elpased: 2.117
batch start
#iterations: 326
currently lose_sum: 56.07473638653755
time_elpased: 2.145
batch start
#iterations: 327
currently lose_sum: 57.14632937312126
time_elpased: 2.125
batch start
#iterations: 328
currently lose_sum: 56.05413445830345
time_elpased: 2.149
batch start
#iterations: 329
currently lose_sum: 56.218615621328354
time_elpased: 2.105
batch start
#iterations: 330
currently lose_sum: 55.483752995729446
time_elpased: 2.158
batch start
#iterations: 331
currently lose_sum: 55.22145786881447
time_elpased: 2.126
batch start
#iterations: 332
currently lose_sum: 55.16188871860504
time_elpased: 2.16
batch start
#iterations: 333
currently lose_sum: 56.34774851799011
time_elpased: 2.15
batch start
#iterations: 334
currently lose_sum: 56.035747200250626
time_elpased: 2.133
batch start
#iterations: 335
currently lose_sum: 56.56148797273636
time_elpased: 2.14
batch start
#iterations: 336
currently lose_sum: 55.6300992667675
time_elpased: 2.155
batch start
#iterations: 337
currently lose_sum: 56.28213885426521
time_elpased: 2.175
batch start
#iterations: 338
currently lose_sum: 55.56952828168869
time_elpased: 2.162
batch start
#iterations: 339
currently lose_sum: 54.69167056679726
time_elpased: 2.131
start validation test
0.557577319588
0.608829174664
0.326438201091
0.425001674817
0.557983119961
133.858
batch start
#iterations: 340
currently lose_sum: 55.26542094349861
time_elpased: 2.148
batch start
#iterations: 341
currently lose_sum: 54.62021246552467
time_elpased: 2.14
batch start
#iterations: 342
currently lose_sum: 55.49465501308441
time_elpased: 2.132
batch start
#iterations: 343
currently lose_sum: 55.30507332086563
time_elpased: 2.132
batch start
#iterations: 344
currently lose_sum: 54.655823945999146
time_elpased: 2.135
batch start
#iterations: 345
currently lose_sum: 54.555848360061646
time_elpased: 2.156
batch start
#iterations: 346
currently lose_sum: 55.046691089868546
time_elpased: 2.136
batch start
#iterations: 347
currently lose_sum: 55.26527354121208
time_elpased: 2.146
batch start
#iterations: 348
currently lose_sum: 55.61793911457062
time_elpased: 2.161
batch start
#iterations: 349
currently lose_sum: 54.50942203402519
time_elpased: 2.136
batch start
#iterations: 350
currently lose_sum: 54.7100775539875
time_elpased: 2.136
batch start
#iterations: 351
currently lose_sum: 54.79574051499367
time_elpased: 2.149
batch start
#iterations: 352
currently lose_sum: 54.932666659355164
time_elpased: 2.147
batch start
#iterations: 353
currently lose_sum: 54.422073036432266
time_elpased: 2.135
batch start
#iterations: 354
currently lose_sum: 53.867785066366196
time_elpased: 2.148
batch start
#iterations: 355
currently lose_sum: 54.571262538433075
time_elpased: 2.141
batch start
#iterations: 356
currently lose_sum: 53.50183519721031
time_elpased: 2.21
batch start
#iterations: 357
currently lose_sum: 53.26830929517746
time_elpased: 2.127
batch start
#iterations: 358
currently lose_sum: 53.06318336725235
time_elpased: 2.124
batch start
#iterations: 359
currently lose_sum: 53.44902956485748
time_elpased: 2.183
start validation test
0.552989690722
0.593654776842
0.340845940105
0.433054393305
0.553362141797
133.466
batch start
#iterations: 360
currently lose_sum: 53.915057957172394
time_elpased: 2.14
batch start
#iterations: 361
currently lose_sum: 53.44057133793831
time_elpased: 2.116
batch start
#iterations: 362
currently lose_sum: 52.940519541502
time_elpased: 2.119
batch start
#iterations: 363
currently lose_sum: 53.46447631716728
time_elpased: 2.123
batch start
#iterations: 364
currently lose_sum: 54.49460726976395
time_elpased: 2.118
batch start
#iterations: 365
currently lose_sum: 53.630690932273865
time_elpased: 2.142
batch start
#iterations: 366
currently lose_sum: 52.955479115247726
time_elpased: 2.132
batch start
#iterations: 367
currently lose_sum: 52.888209760189056
time_elpased: 2.123
batch start
#iterations: 368
currently lose_sum: 52.67291909456253
time_elpased: 2.117
batch start
#iterations: 369
currently lose_sum: 52.76805308461189
time_elpased: 2.143
batch start
#iterations: 370
currently lose_sum: 52.537687510252
time_elpased: 2.111
batch start
#iterations: 371
currently lose_sum: 53.45722874999046
time_elpased: 2.154
batch start
#iterations: 372
currently lose_sum: 52.3692087829113
time_elpased: 2.11
batch start
#iterations: 373
currently lose_sum: 52.89736822247505
time_elpased: 2.151
batch start
#iterations: 374
currently lose_sum: 52.620296895504
time_elpased: 2.109
batch start
#iterations: 375
currently lose_sum: 52.39886447787285
time_elpased: 2.187
batch start
#iterations: 376
currently lose_sum: 52.48929610848427
time_elpased: 2.125
batch start
#iterations: 377
currently lose_sum: 52.43326586484909
time_elpased: 2.119
batch start
#iterations: 378
currently lose_sum: 52.689387530088425
time_elpased: 2.121
batch start
#iterations: 379
currently lose_sum: 53.09516951441765
time_elpased: 2.107
start validation test
0.55
0.608485381403
0.284861582793
0.388055516613
0.550465491386
146.583
batch start
#iterations: 380
currently lose_sum: 51.373726576566696
time_elpased: 2.145
batch start
#iterations: 381
currently lose_sum: 51.513556480407715
time_elpased: 2.106
batch start
#iterations: 382
currently lose_sum: 51.999172896146774
time_elpased: 2.121
batch start
#iterations: 383
currently lose_sum: 51.90350368618965
time_elpased: 2.132
batch start
#iterations: 384
currently lose_sum: 51.070326045155525
time_elpased: 2.116
batch start
#iterations: 385
currently lose_sum: 51.710401237010956
time_elpased: 2.165
batch start
#iterations: 386
currently lose_sum: 50.86600559949875
time_elpased: 2.143
batch start
#iterations: 387
currently lose_sum: 51.29232436418533
time_elpased: 2.149
batch start
#iterations: 388
currently lose_sum: 51.33671775460243
time_elpased: 2.12
batch start
#iterations: 389
currently lose_sum: 51.521526366472244
time_elpased: 2.159
batch start
#iterations: 390
currently lose_sum: 52.39575079083443
time_elpased: 2.179
batch start
#iterations: 391
currently lose_sum: 51.429877519607544
time_elpased: 2.154
batch start
#iterations: 392
currently lose_sum: 50.92990717291832
time_elpased: 2.147
batch start
#iterations: 393
currently lose_sum: 50.66967582702637
time_elpased: 2.161
batch start
#iterations: 394
currently lose_sum: 52.26160064339638
time_elpased: 2.161
batch start
#iterations: 395
currently lose_sum: 50.54990169405937
time_elpased: 2.173
batch start
#iterations: 396
currently lose_sum: 50.62744542956352
time_elpased: 2.148
batch start
#iterations: 397
currently lose_sum: 50.99225401878357
time_elpased: 2.158
batch start
#iterations: 398
currently lose_sum: 51.42514255642891
time_elpased: 2.117
batch start
#iterations: 399
currently lose_sum: 50.77667161822319
time_elpased: 2.132
start validation test
0.54675257732
0.603263299061
0.277760625708
0.380381932211
0.54722483418
147.787
acc: 0.633
pre: 0.623
rec: 0.674
F1: 0.648
auc: 0.633
