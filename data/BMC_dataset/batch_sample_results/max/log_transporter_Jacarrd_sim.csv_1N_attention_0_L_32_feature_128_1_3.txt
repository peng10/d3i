start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.61839061975479
time_elpased: 2.224
batch start
#iterations: 1
currently lose_sum: 100.28451496362686
time_elpased: 2.194
batch start
#iterations: 2
currently lose_sum: 100.03687399625778
time_elpased: 2.142
batch start
#iterations: 3
currently lose_sum: 99.78089421987534
time_elpased: 2.194
batch start
#iterations: 4
currently lose_sum: 99.79012280702591
time_elpased: 2.231
batch start
#iterations: 5
currently lose_sum: 99.63219267129898
time_elpased: 2.145
batch start
#iterations: 6
currently lose_sum: 99.82867896556854
time_elpased: 2.164
batch start
#iterations: 7
currently lose_sum: 99.52538740634918
time_elpased: 2.155
batch start
#iterations: 8
currently lose_sum: 99.63681274652481
time_elpased: 2.16
batch start
#iterations: 9
currently lose_sum: 99.59421700239182
time_elpased: 2.154
batch start
#iterations: 10
currently lose_sum: 99.47660809755325
time_elpased: 2.145
batch start
#iterations: 11
currently lose_sum: 99.47746115922928
time_elpased: 2.12
batch start
#iterations: 12
currently lose_sum: 99.35970890522003
time_elpased: 2.183
batch start
#iterations: 13
currently lose_sum: 99.48984497785568
time_elpased: 2.134
batch start
#iterations: 14
currently lose_sum: 99.38202452659607
time_elpased: 2.231
batch start
#iterations: 15
currently lose_sum: 99.39005380868912
time_elpased: 2.092
batch start
#iterations: 16
currently lose_sum: 99.34838479757309
time_elpased: 2.14
batch start
#iterations: 17
currently lose_sum: 99.33389616012573
time_elpased: 2.148
batch start
#iterations: 18
currently lose_sum: 99.22684788703918
time_elpased: 2.154
batch start
#iterations: 19
currently lose_sum: 99.24404782056808
time_elpased: 2.196
start validation test
0.576804123711
0.606687446869
0.440716344175
0.510552044831
0.577028969279
65.753
batch start
#iterations: 20
currently lose_sum: 99.22278356552124
time_elpased: 2.094
batch start
#iterations: 21
currently lose_sum: 99.15179544687271
time_elpased: 2.197
batch start
#iterations: 22
currently lose_sum: 98.95505279302597
time_elpased: 2.115
batch start
#iterations: 23
currently lose_sum: 98.94437062740326
time_elpased: 2.196
batch start
#iterations: 24
currently lose_sum: 98.828542470932
time_elpased: 2.139
batch start
#iterations: 25
currently lose_sum: 98.98314452171326
time_elpased: 2.198
batch start
#iterations: 26
currently lose_sum: 99.12691569328308
time_elpased: 2.133
batch start
#iterations: 27
currently lose_sum: 99.09738618135452
time_elpased: 2.156
batch start
#iterations: 28
currently lose_sum: 98.86781191825867
time_elpased: 2.151
batch start
#iterations: 29
currently lose_sum: 98.92745220661163
time_elpased: 2.098
batch start
#iterations: 30
currently lose_sum: 98.78095918893814
time_elpased: 2.179
batch start
#iterations: 31
currently lose_sum: 99.04722768068314
time_elpased: 2.118
batch start
#iterations: 32
currently lose_sum: 98.86926448345184
time_elpased: 2.171
batch start
#iterations: 33
currently lose_sum: 99.14273953437805
time_elpased: 2.182
batch start
#iterations: 34
currently lose_sum: 98.70922827720642
time_elpased: 2.177
batch start
#iterations: 35
currently lose_sum: 99.00092452764511
time_elpased: 2.152
batch start
#iterations: 36
currently lose_sum: 98.5452544093132
time_elpased: 2.161
batch start
#iterations: 37
currently lose_sum: 98.69526314735413
time_elpased: 2.136
batch start
#iterations: 38
currently lose_sum: 98.68632823228836
time_elpased: 2.151
batch start
#iterations: 39
currently lose_sum: 98.5847669839859
time_elpased: 2.165
start validation test
0.582164948454
0.602862254025
0.485590778098
0.537909018356
0.582324509247
65.445
batch start
#iterations: 40
currently lose_sum: 98.6581124663353
time_elpased: 2.137
batch start
#iterations: 41
currently lose_sum: 98.61548388004303
time_elpased: 2.172
batch start
#iterations: 42
currently lose_sum: 98.50772726535797
time_elpased: 2.164
batch start
#iterations: 43
currently lose_sum: 98.5526972413063
time_elpased: 2.177
batch start
#iterations: 44
currently lose_sum: 98.61931991577148
time_elpased: 2.19
batch start
#iterations: 45
currently lose_sum: 98.57952439785004
time_elpased: 2.183
batch start
#iterations: 46
currently lose_sum: 98.47937393188477
time_elpased: 2.237
batch start
#iterations: 47
currently lose_sum: 98.62121248245239
time_elpased: 2.186
batch start
#iterations: 48
currently lose_sum: 98.41497611999512
time_elpased: 2.153
batch start
#iterations: 49
currently lose_sum: 98.42475873231888
time_elpased: 2.158
batch start
#iterations: 50
currently lose_sum: 98.22658222913742
time_elpased: 2.147
batch start
#iterations: 51
currently lose_sum: 98.06807148456573
time_elpased: 2.135
batch start
#iterations: 52
currently lose_sum: 98.70377731323242
time_elpased: 2.181
batch start
#iterations: 53
currently lose_sum: 98.253990650177
time_elpased: 2.131
batch start
#iterations: 54
currently lose_sum: 98.36079382896423
time_elpased: 2.139
batch start
#iterations: 55
currently lose_sum: 98.62827461957932
time_elpased: 2.143
batch start
#iterations: 56
currently lose_sum: 98.33778458833694
time_elpased: 2.16
batch start
#iterations: 57
currently lose_sum: 98.1831305027008
time_elpased: 2.084
batch start
#iterations: 58
currently lose_sum: 98.20411908626556
time_elpased: 2.168
batch start
#iterations: 59
currently lose_sum: 98.31695806980133
time_elpased: 2.144
start validation test
0.572680412371
0.619872225958
0.379477151091
0.470760980592
0.572999624699
65.807
batch start
#iterations: 60
currently lose_sum: 98.16225916147232
time_elpased: 2.185
batch start
#iterations: 61
currently lose_sum: 98.4758785367012
time_elpased: 2.195
batch start
#iterations: 62
currently lose_sum: 98.18058550357819
time_elpased: 2.15
batch start
#iterations: 63
currently lose_sum: 98.19720602035522
time_elpased: 2.185
batch start
#iterations: 64
currently lose_sum: 98.08982396125793
time_elpased: 2.177
batch start
#iterations: 65
currently lose_sum: 98.16734498739243
time_elpased: 2.23
batch start
#iterations: 66
currently lose_sum: 98.31332391500473
time_elpased: 2.173
batch start
#iterations: 67
currently lose_sum: 98.13557612895966
time_elpased: 2.164
batch start
#iterations: 68
currently lose_sum: 97.82442009449005
time_elpased: 2.146
batch start
#iterations: 69
currently lose_sum: 98.25523120164871
time_elpased: 2.185
batch start
#iterations: 70
currently lose_sum: 97.97376990318298
time_elpased: 2.125
batch start
#iterations: 71
currently lose_sum: 97.9396687746048
time_elpased: 2.173
batch start
#iterations: 72
currently lose_sum: 98.02858418226242
time_elpased: 2.17
batch start
#iterations: 73
currently lose_sum: 98.02383625507355
time_elpased: 2.192
batch start
#iterations: 74
currently lose_sum: 98.12218302488327
time_elpased: 2.186
batch start
#iterations: 75
currently lose_sum: 98.2380633354187
time_elpased: 2.147
batch start
#iterations: 76
currently lose_sum: 98.12030881643295
time_elpased: 2.162
batch start
#iterations: 77
currently lose_sum: 97.94460129737854
time_elpased: 2.184
batch start
#iterations: 78
currently lose_sum: 97.86805999279022
time_elpased: 2.185
batch start
#iterations: 79
currently lose_sum: 98.11048913002014
time_elpased: 2.187
start validation test
0.581082474227
0.606004002668
0.467476327707
0.527801987101
0.581270175419
65.511
batch start
#iterations: 80
currently lose_sum: 97.76307135820389
time_elpased: 2.162
batch start
#iterations: 81
currently lose_sum: 97.98073333501816
time_elpased: 2.17
batch start
#iterations: 82
currently lose_sum: 97.81557929515839
time_elpased: 2.132
batch start
#iterations: 83
currently lose_sum: 97.94326037168503
time_elpased: 2.161
batch start
#iterations: 84
currently lose_sum: 97.67166638374329
time_elpased: 2.156
batch start
#iterations: 85
currently lose_sum: 97.66881567239761
time_elpased: 2.154
batch start
#iterations: 86
currently lose_sum: 97.94691699743271
time_elpased: 2.14
batch start
#iterations: 87
currently lose_sum: 97.6220850944519
time_elpased: 2.186
batch start
#iterations: 88
currently lose_sum: 97.89994084835052
time_elpased: 2.146
batch start
#iterations: 89
currently lose_sum: 97.80953741073608
time_elpased: 2.16
batch start
#iterations: 90
currently lose_sum: 97.65414106845856
time_elpased: 2.147
batch start
#iterations: 91
currently lose_sum: 97.52932578325272
time_elpased: 2.202
batch start
#iterations: 92
currently lose_sum: 97.55913835763931
time_elpased: 2.169
batch start
#iterations: 93
currently lose_sum: 97.63885349035263
time_elpased: 2.129
batch start
#iterations: 94
currently lose_sum: 97.83599764108658
time_elpased: 2.138
batch start
#iterations: 95
currently lose_sum: 97.85184955596924
time_elpased: 2.141
batch start
#iterations: 96
currently lose_sum: 97.56399315595627
time_elpased: 2.182
batch start
#iterations: 97
currently lose_sum: 97.47118747234344
time_elpased: 2.158
batch start
#iterations: 98
currently lose_sum: 97.50302284955978
time_elpased: 2.168
batch start
#iterations: 99
currently lose_sum: 97.61654835939407
time_elpased: 2.1
start validation test
0.579278350515
0.612903225806
0.434129271305
0.508254006507
0.579518167251
65.684
batch start
#iterations: 100
currently lose_sum: 97.70762401819229
time_elpased: 2.177
batch start
#iterations: 101
currently lose_sum: 97.65849173069
time_elpased: 2.16
batch start
#iterations: 102
currently lose_sum: 97.81394046545029
time_elpased: 2.163
batch start
#iterations: 103
currently lose_sum: 97.41156506538391
time_elpased: 2.185
batch start
#iterations: 104
currently lose_sum: 97.3881242275238
time_elpased: 2.172
batch start
#iterations: 105
currently lose_sum: 97.37525147199631
time_elpased: 2.134
batch start
#iterations: 106
currently lose_sum: 97.27508771419525
time_elpased: 2.131
batch start
#iterations: 107
currently lose_sum: 97.51938235759735
time_elpased: 2.127
batch start
#iterations: 108
currently lose_sum: 97.53279209136963
time_elpased: 2.13
batch start
#iterations: 109
currently lose_sum: 97.28634941577911
time_elpased: 2.127
batch start
#iterations: 110
currently lose_sum: 97.2096899151802
time_elpased: 2.181
batch start
#iterations: 111
currently lose_sum: 97.64040225744247
time_elpased: 2.183
batch start
#iterations: 112
currently lose_sum: 97.13227742910385
time_elpased: 2.128
batch start
#iterations: 113
currently lose_sum: 97.41378456354141
time_elpased: 2.195
batch start
#iterations: 114
currently lose_sum: 97.31290709972382
time_elpased: 2.196
batch start
#iterations: 115
currently lose_sum: 97.38077276945114
time_elpased: 2.028
batch start
#iterations: 116
currently lose_sum: 97.32617288827896
time_elpased: 2.036
batch start
#iterations: 117
currently lose_sum: 97.26710349321365
time_elpased: 2.023
batch start
#iterations: 118
currently lose_sum: 97.5095346570015
time_elpased: 2.048
batch start
#iterations: 119
currently lose_sum: 97.23252069950104
time_elpased: 2.023
start validation test
0.562525773196
0.614283057467
0.339954713874
0.437686344663
0.562893507288
66.693
batch start
#iterations: 120
currently lose_sum: 97.20513969659805
time_elpased: 2.04
batch start
#iterations: 121
currently lose_sum: 97.04848062992096
time_elpased: 1.975
batch start
#iterations: 122
currently lose_sum: 97.00071340799332
time_elpased: 2.034
batch start
#iterations: 123
currently lose_sum: 97.17572551965714
time_elpased: 1.979
batch start
#iterations: 124
currently lose_sum: 97.18367630243301
time_elpased: 2.043
batch start
#iterations: 125
currently lose_sum: 97.15975427627563
time_elpased: 2.003
batch start
#iterations: 126
currently lose_sum: 96.88567143678665
time_elpased: 2.017
batch start
#iterations: 127
currently lose_sum: 97.29086673259735
time_elpased: 1.994
batch start
#iterations: 128
currently lose_sum: 96.9187730550766
time_elpased: 2.021
batch start
#iterations: 129
currently lose_sum: 96.98129791021347
time_elpased: 2.005
batch start
#iterations: 130
currently lose_sum: 97.23584139347076
time_elpased: 2.011
batch start
#iterations: 131
currently lose_sum: 96.83395612239838
time_elpased: 2.045
batch start
#iterations: 132
currently lose_sum: 97.1632170677185
time_elpased: 2.029
batch start
#iterations: 133
currently lose_sum: 96.67788827419281
time_elpased: 1.98
batch start
#iterations: 134
currently lose_sum: 97.02478033304214
time_elpased: 2.057
batch start
#iterations: 135
currently lose_sum: 96.75863909721375
time_elpased: 2.044
batch start
#iterations: 136
currently lose_sum: 97.0524275302887
time_elpased: 2.096
batch start
#iterations: 137
currently lose_sum: 96.81800490617752
time_elpased: 2.036
batch start
#iterations: 138
currently lose_sum: 96.86578100919724
time_elpased: 2.084
batch start
#iterations: 139
currently lose_sum: 96.90868282318115
time_elpased: 1.981
start validation test
0.572525773196
0.605391793808
0.420646356525
0.49638671282
0.572776709861
66.159
batch start
#iterations: 140
currently lose_sum: 96.73680055141449
time_elpased: 2.026
batch start
#iterations: 141
currently lose_sum: 96.70568579435349
time_elpased: 2.062
batch start
#iterations: 142
currently lose_sum: 96.81616228818893
time_elpased: 2.035
batch start
#iterations: 143
currently lose_sum: 96.95436245203018
time_elpased: 2.003
batch start
#iterations: 144
currently lose_sum: 96.93949329853058
time_elpased: 1.995
batch start
#iterations: 145
currently lose_sum: 96.69582623243332
time_elpased: 2.026
batch start
#iterations: 146
currently lose_sum: 96.7028660774231
time_elpased: 2.049
batch start
#iterations: 147
currently lose_sum: 96.85838675498962
time_elpased: 2.013
batch start
#iterations: 148
currently lose_sum: 96.51084262132645
time_elpased: 2.03
batch start
#iterations: 149
currently lose_sum: 96.7121587395668
time_elpased: 1.965
batch start
#iterations: 150
currently lose_sum: 96.56459826231003
time_elpased: 1.994
batch start
#iterations: 151
currently lose_sum: 96.86027401685715
time_elpased: 1.989
batch start
#iterations: 152
currently lose_sum: 96.44559544324875
time_elpased: 1.984
batch start
#iterations: 153
currently lose_sum: 96.92024004459381
time_elpased: 2.034
batch start
#iterations: 154
currently lose_sum: 96.6821151971817
time_elpased: 1.981
batch start
#iterations: 155
currently lose_sum: 96.44728553295135
time_elpased: 1.991
batch start
#iterations: 156
currently lose_sum: 96.3189452290535
time_elpased: 1.994
batch start
#iterations: 157
currently lose_sum: 96.28228706121445
time_elpased: 1.965
batch start
#iterations: 158
currently lose_sum: 96.49410331249237
time_elpased: 2.008
batch start
#iterations: 159
currently lose_sum: 96.53923445940018
time_elpased: 1.969
start validation test
0.577886597938
0.607884696905
0.442774804446
0.512356338951
0.578109830972
66.198
batch start
#iterations: 160
currently lose_sum: 96.10947483778
time_elpased: 2.09
batch start
#iterations: 161
currently lose_sum: 96.54806381464005
time_elpased: 2.15
batch start
#iterations: 162
currently lose_sum: 96.41572403907776
time_elpased: 1.964
batch start
#iterations: 163
currently lose_sum: 96.48573929071426
time_elpased: 2.05
batch start
#iterations: 164
currently lose_sum: 96.53337347507477
time_elpased: 1.986
batch start
#iterations: 165
currently lose_sum: 96.4599758386612
time_elpased: 2.014
batch start
#iterations: 166
currently lose_sum: 96.56575500965118
time_elpased: 1.986
batch start
#iterations: 167
currently lose_sum: 96.21760928630829
time_elpased: 2.014
batch start
#iterations: 168
currently lose_sum: 96.0934094786644
time_elpased: 1.989
batch start
#iterations: 169
currently lose_sum: 96.46755766868591
time_elpased: 2.033
batch start
#iterations: 170
currently lose_sum: 96.11975479125977
time_elpased: 1.974
batch start
#iterations: 171
currently lose_sum: 96.37855738401413
time_elpased: 2.063
batch start
#iterations: 172
currently lose_sum: 96.23776340484619
time_elpased: 1.983
batch start
#iterations: 173
currently lose_sum: 96.16268050670624
time_elpased: 2.031
batch start
#iterations: 174
currently lose_sum: 96.22554582357407
time_elpased: 2.001
batch start
#iterations: 175
currently lose_sum: 96.28244096040726
time_elpased: 2.028
batch start
#iterations: 176
currently lose_sum: 95.85334289073944
time_elpased: 2.009
batch start
#iterations: 177
currently lose_sum: 95.91987854242325
time_elpased: 2.003
batch start
#iterations: 178
currently lose_sum: 96.13495057821274
time_elpased: 1.977
batch start
#iterations: 179
currently lose_sum: 96.45641106367111
time_elpased: 2.057
start validation test
0.561958762887
0.602456258412
0.368567311651
0.457343550447
0.562278286143
67.221
batch start
#iterations: 180
currently lose_sum: 96.0351819396019
time_elpased: 2.031
batch start
#iterations: 181
currently lose_sum: 95.9169008731842
time_elpased: 2.031
batch start
#iterations: 182
currently lose_sum: 96.39186573028564
time_elpased: 1.988
batch start
#iterations: 183
currently lose_sum: 95.91887873411179
time_elpased: 2.11
batch start
#iterations: 184
currently lose_sum: 95.91715812683105
time_elpased: 2.016
batch start
#iterations: 185
currently lose_sum: 95.71092963218689
time_elpased: 2.141
batch start
#iterations: 186
currently lose_sum: 95.99900084733963
time_elpased: 2.015
batch start
#iterations: 187
currently lose_sum: 96.2160297036171
time_elpased: 2.01
batch start
#iterations: 188
currently lose_sum: 95.94744312763214
time_elpased: 2.001
batch start
#iterations: 189
currently lose_sum: 96.15814155340195
time_elpased: 2.016
batch start
#iterations: 190
currently lose_sum: 95.97335374355316
time_elpased: 1.983
batch start
#iterations: 191
currently lose_sum: 95.91646218299866
time_elpased: 2.043
batch start
#iterations: 192
currently lose_sum: 95.72911047935486
time_elpased: 1.969
batch start
#iterations: 193
currently lose_sum: 95.83071047067642
time_elpased: 2.09
batch start
#iterations: 194
currently lose_sum: 95.85271865129471
time_elpased: 2.017
batch start
#iterations: 195
currently lose_sum: 96.00084990262985
time_elpased: 2.039
batch start
#iterations: 196
currently lose_sum: 96.35094398260117
time_elpased: 1.979
batch start
#iterations: 197
currently lose_sum: 95.78813707828522
time_elpased: 2.032
batch start
#iterations: 198
currently lose_sum: 95.6927934885025
time_elpased: 1.977
batch start
#iterations: 199
currently lose_sum: 95.76795667409897
time_elpased: 2.009
start validation test
0.572783505155
0.598727876106
0.445656648827
0.510974746283
0.572993545396
66.601
batch start
#iterations: 200
currently lose_sum: 95.68622761964798
time_elpased: 1.982
batch start
#iterations: 201
currently lose_sum: 95.50377678871155
time_elpased: 2.014
batch start
#iterations: 202
currently lose_sum: 95.73207712173462
time_elpased: 2.005
batch start
#iterations: 203
currently lose_sum: 95.79233467578888
time_elpased: 1.993
batch start
#iterations: 204
currently lose_sum: 96.02273488044739
time_elpased: 2.026
batch start
#iterations: 205
currently lose_sum: 95.78564941883087
time_elpased: 2.024
batch start
#iterations: 206
currently lose_sum: 95.89363604784012
time_elpased: 2.027
batch start
#iterations: 207
currently lose_sum: 95.62658888101578
time_elpased: 1.99
batch start
#iterations: 208
currently lose_sum: 95.39000183343887
time_elpased: 2.017
batch start
#iterations: 209
currently lose_sum: 95.57164359092712
time_elpased: 1.894
batch start
#iterations: 210
currently lose_sum: 95.62037009000778
time_elpased: 1.932
batch start
#iterations: 211
currently lose_sum: 95.12411111593246
time_elpased: 1.869
batch start
#iterations: 212
currently lose_sum: 95.33224159479141
time_elpased: 1.882
batch start
#iterations: 213
currently lose_sum: 95.24640417098999
time_elpased: 1.906
batch start
#iterations: 214
currently lose_sum: 95.60542863607407
time_elpased: 1.877
batch start
#iterations: 215
currently lose_sum: 95.81968456506729
time_elpased: 1.871
batch start
#iterations: 216
currently lose_sum: 95.6004610657692
time_elpased: 1.854
batch start
#iterations: 217
currently lose_sum: 95.39703088998795
time_elpased: 1.875
batch start
#iterations: 218
currently lose_sum: 95.38390809297562
time_elpased: 1.908
batch start
#iterations: 219
currently lose_sum: 95.34422367811203
time_elpased: 1.929
start validation test
0.565309278351
0.611082251082
0.363215314944
0.455619391905
0.565643179984
67.507
batch start
#iterations: 220
currently lose_sum: 95.21728962659836
time_elpased: 1.885
batch start
#iterations: 221
currently lose_sum: 94.77616357803345
time_elpased: 1.882
batch start
#iterations: 222
currently lose_sum: 95.06534713506699
time_elpased: 1.996
batch start
#iterations: 223
currently lose_sum: 95.17352545261383
time_elpased: 1.824
batch start
#iterations: 224
currently lose_sum: 95.13020777702332
time_elpased: 1.824
batch start
#iterations: 225
currently lose_sum: 95.56079262495041
time_elpased: 1.873
batch start
#iterations: 226
currently lose_sum: 95.13347804546356
time_elpased: 1.883
batch start
#iterations: 227
currently lose_sum: 95.0889475941658
time_elpased: 1.863
batch start
#iterations: 228
currently lose_sum: 95.32169222831726
time_elpased: 1.861
batch start
#iterations: 229
currently lose_sum: 95.29515844583511
time_elpased: 1.894
batch start
#iterations: 230
currently lose_sum: 95.44147968292236
time_elpased: 1.877
batch start
#iterations: 231
currently lose_sum: 95.46072208881378
time_elpased: 1.854
batch start
#iterations: 232
currently lose_sum: 95.542959690094
time_elpased: 1.881
batch start
#iterations: 233
currently lose_sum: 95.16920703649521
time_elpased: 1.933
batch start
#iterations: 234
currently lose_sum: 95.24747097492218
time_elpased: 1.916
batch start
#iterations: 235
currently lose_sum: 95.52315080165863
time_elpased: 1.882
batch start
#iterations: 236
currently lose_sum: 95.45884257555008
time_elpased: 1.881
batch start
#iterations: 237
currently lose_sum: 95.01040083169937
time_elpased: 1.905
batch start
#iterations: 238
currently lose_sum: 94.8507621884346
time_elpased: 1.874
batch start
#iterations: 239
currently lose_sum: 95.27722489833832
time_elpased: 1.871
start validation test
0.573350515464
0.588750462563
0.491251543845
0.535600067329
0.573486160192
66.683
batch start
#iterations: 240
currently lose_sum: 95.46807050704956
time_elpased: 1.915
batch start
#iterations: 241
currently lose_sum: 95.07512247562408
time_elpased: 1.854
batch start
#iterations: 242
currently lose_sum: 94.74699980020523
time_elpased: 1.872
batch start
#iterations: 243
currently lose_sum: 95.32988065481186
time_elpased: 1.84
batch start
#iterations: 244
currently lose_sum: 94.88242608308792
time_elpased: 1.898
batch start
#iterations: 245
currently lose_sum: 94.81506949663162
time_elpased: 1.842
batch start
#iterations: 246
currently lose_sum: 95.05086261034012
time_elpased: 1.887
batch start
#iterations: 247
currently lose_sum: 95.0609519481659
time_elpased: 1.911
batch start
#iterations: 248
currently lose_sum: 95.08132082223892
time_elpased: 1.879
batch start
#iterations: 249
currently lose_sum: 95.21520859003067
time_elpased: 1.878
batch start
#iterations: 250
currently lose_sum: 95.01706713438034
time_elpased: 1.862
batch start
#iterations: 251
currently lose_sum: 95.34169661998749
time_elpased: 1.907
batch start
#iterations: 252
currently lose_sum: 94.87045860290527
time_elpased: 1.899
batch start
#iterations: 253
currently lose_sum: 94.51629114151001
time_elpased: 1.921
batch start
#iterations: 254
currently lose_sum: 94.7988069653511
time_elpased: 1.955
batch start
#iterations: 255
currently lose_sum: 95.03449910879135
time_elpased: 1.895
batch start
#iterations: 256
currently lose_sum: 94.87476408481598
time_elpased: 1.897
batch start
#iterations: 257
currently lose_sum: 95.27546578645706
time_elpased: 1.922
batch start
#iterations: 258
currently lose_sum: 94.97520756721497
time_elpased: 1.927
batch start
#iterations: 259
currently lose_sum: 94.76514387130737
time_elpased: 1.872
start validation test
0.56293814433
0.593783169067
0.403046521202
0.480166758629
0.563202318841
67.468
batch start
#iterations: 260
currently lose_sum: 94.73603975772858
time_elpased: 1.92
batch start
#iterations: 261
currently lose_sum: 94.82700389623642
time_elpased: 1.907
batch start
#iterations: 262
currently lose_sum: 94.68137747049332
time_elpased: 1.903
batch start
#iterations: 263
currently lose_sum: 94.81119465827942
time_elpased: 1.95
batch start
#iterations: 264
currently lose_sum: 94.62289553880692
time_elpased: 1.909
batch start
#iterations: 265
currently lose_sum: 94.6247643828392
time_elpased: 1.919
batch start
#iterations: 266
currently lose_sum: 94.55576825141907
time_elpased: 1.873
batch start
#iterations: 267
currently lose_sum: 94.84099245071411
time_elpased: 1.903
batch start
#iterations: 268
currently lose_sum: 94.33320641517639
time_elpased: 1.906
batch start
#iterations: 269
currently lose_sum: 94.39194595813751
time_elpased: 1.887
batch start
#iterations: 270
currently lose_sum: 94.31219482421875
time_elpased: 1.917
batch start
#iterations: 271
currently lose_sum: 94.4882583618164
time_elpased: 1.889
batch start
#iterations: 272
currently lose_sum: 94.57176268100739
time_elpased: 1.891
batch start
#iterations: 273
currently lose_sum: 94.5496997833252
time_elpased: 1.865
batch start
#iterations: 274
currently lose_sum: 94.78544646501541
time_elpased: 1.893
batch start
#iterations: 275
currently lose_sum: 94.16274976730347
time_elpased: 1.923
batch start
#iterations: 276
currently lose_sum: 94.55352783203125
time_elpased: 1.936
batch start
#iterations: 277
currently lose_sum: 94.39875954389572
time_elpased: 1.894
batch start
#iterations: 278
currently lose_sum: 94.68883699178696
time_elpased: 1.906
batch start
#iterations: 279
currently lose_sum: 94.20298624038696
time_elpased: 1.881
start validation test
0.560051546392
0.604828688088
0.350658707287
0.443937715812
0.560397507299
68.348
batch start
#iterations: 280
currently lose_sum: 94.41546046733856
time_elpased: 1.928
batch start
#iterations: 281
currently lose_sum: 94.13396310806274
time_elpased: 1.959
batch start
#iterations: 282
currently lose_sum: 94.1839554309845
time_elpased: 1.916
batch start
#iterations: 283
currently lose_sum: 94.41436290740967
time_elpased: 1.875
batch start
#iterations: 284
currently lose_sum: 94.32615369558334
time_elpased: 1.885
batch start
#iterations: 285
currently lose_sum: 94.61374390125275
time_elpased: 1.877
batch start
#iterations: 286
currently lose_sum: 94.41977232694626
time_elpased: 1.866
batch start
#iterations: 287
currently lose_sum: 94.24592065811157
time_elpased: 1.844
batch start
#iterations: 288
currently lose_sum: 94.38477355241776
time_elpased: 1.895
batch start
#iterations: 289
currently lose_sum: 94.08619278669357
time_elpased: 1.9
batch start
#iterations: 290
currently lose_sum: 94.19042629003525
time_elpased: 1.872
batch start
#iterations: 291
currently lose_sum: 94.48552244901657
time_elpased: 1.904
batch start
#iterations: 292
currently lose_sum: 93.80647140741348
time_elpased: 1.929
batch start
#iterations: 293
currently lose_sum: 93.97408759593964
time_elpased: 1.884
batch start
#iterations: 294
currently lose_sum: 94.24924540519714
time_elpased: 1.923
batch start
#iterations: 295
currently lose_sum: 94.11381393671036
time_elpased: 1.88
batch start
#iterations: 296
currently lose_sum: 94.080049097538
time_elpased: 1.92
batch start
#iterations: 297
currently lose_sum: 94.04559618234634
time_elpased: 1.914
batch start
#iterations: 298
currently lose_sum: 93.917609333992
time_elpased: 1.894
batch start
#iterations: 299
currently lose_sum: 94.76188373565674
time_elpased: 1.891
start validation test
0.561649484536
0.599901088032
0.374536846439
0.461158281587
0.561958633876
68.148
batch start
#iterations: 300
currently lose_sum: 94.08601015806198
time_elpased: 1.928
batch start
#iterations: 301
currently lose_sum: 94.14725279808044
time_elpased: 1.897
batch start
#iterations: 302
currently lose_sum: 94.57267320156097
time_elpased: 1.882
batch start
#iterations: 303
currently lose_sum: 94.24509388208389
time_elpased: 1.9
batch start
#iterations: 304
currently lose_sum: 93.53161597251892
time_elpased: 1.89
batch start
#iterations: 305
currently lose_sum: 94.16062813997269
time_elpased: 1.901
batch start
#iterations: 306
currently lose_sum: 94.11501747369766
time_elpased: 1.89
batch start
#iterations: 307
currently lose_sum: 94.00244122743607
time_elpased: 1.882
batch start
#iterations: 308
currently lose_sum: 94.14888972043991
time_elpased: 1.906
batch start
#iterations: 309
currently lose_sum: 94.21720772981644
time_elpased: 1.894
batch start
#iterations: 310
currently lose_sum: 94.10052382946014
time_elpased: 1.894
batch start
#iterations: 311
currently lose_sum: 93.81180864572525
time_elpased: 1.897
batch start
#iterations: 312
currently lose_sum: 94.03949719667435
time_elpased: 1.866
batch start
#iterations: 313
currently lose_sum: 93.73860067129135
time_elpased: 1.887
batch start
#iterations: 314
currently lose_sum: 93.95025724172592
time_elpased: 1.912
batch start
#iterations: 315
currently lose_sum: 94.30341750383377
time_elpased: 1.907
batch start
#iterations: 316
currently lose_sum: 93.91934198141098
time_elpased: 1.896
batch start
#iterations: 317
currently lose_sum: 93.91576474905014
time_elpased: 1.848
batch start
#iterations: 318
currently lose_sum: 93.99940431118011
time_elpased: 1.885
batch start
#iterations: 319
currently lose_sum: 94.02843183279037
time_elpased: 1.89
start validation test
0.569793814433
0.58811422691
0.470564018114
0.522813036021
0.569957762878
67.458
batch start
#iterations: 320
currently lose_sum: 93.49147135019302
time_elpased: 1.915
batch start
#iterations: 321
currently lose_sum: 93.86955916881561
time_elpased: 1.921
batch start
#iterations: 322
currently lose_sum: 94.172272503376
time_elpased: 1.909
batch start
#iterations: 323
currently lose_sum: 93.48771804571152
time_elpased: 1.919
batch start
#iterations: 324
currently lose_sum: 93.72427326440811
time_elpased: 1.933
batch start
#iterations: 325
currently lose_sum: 93.54935818910599
time_elpased: 1.882
batch start
#iterations: 326
currently lose_sum: 94.013811647892
time_elpased: 1.919
batch start
#iterations: 327
currently lose_sum: 93.91951709985733
time_elpased: 1.905
batch start
#iterations: 328
currently lose_sum: 93.88324636220932
time_elpased: 1.966
batch start
#iterations: 329
currently lose_sum: 93.61291325092316
time_elpased: 1.895
batch start
#iterations: 330
currently lose_sum: 93.97880810499191
time_elpased: 1.879
batch start
#iterations: 331
currently lose_sum: 93.63040059804916
time_elpased: 1.856
batch start
#iterations: 332
currently lose_sum: 93.81988137960434
time_elpased: 1.911
batch start
#iterations: 333
currently lose_sum: 93.91917300224304
time_elpased: 1.883
batch start
#iterations: 334
currently lose_sum: 93.48362165689468
time_elpased: 1.934
batch start
#iterations: 335
currently lose_sum: 93.52931654453278
time_elpased: 1.917
batch start
#iterations: 336
currently lose_sum: 93.69780141115189
time_elpased: 1.891
batch start
#iterations: 337
currently lose_sum: 93.50030487775803
time_elpased: 1.939
batch start
#iterations: 338
currently lose_sum: 93.0490152835846
time_elpased: 1.928
batch start
#iterations: 339
currently lose_sum: 93.45817524194717
time_elpased: 2.002
start validation test
0.558969072165
0.597873776578
0.364656237135
0.453011123897
0.559290117741
68.971
batch start
#iterations: 340
currently lose_sum: 93.46969252824783
time_elpased: 2.057
batch start
#iterations: 341
currently lose_sum: 93.62049126625061
time_elpased: 2.0
batch start
#iterations: 342
currently lose_sum: 93.68215906620026
time_elpased: 1.974
batch start
#iterations: 343
currently lose_sum: 93.72534507513046
time_elpased: 1.943
batch start
#iterations: 344
currently lose_sum: 93.471999168396
time_elpased: 1.886
batch start
#iterations: 345
currently lose_sum: 93.47241461277008
time_elpased: 1.891
batch start
#iterations: 346
currently lose_sum: 93.52839142084122
time_elpased: 1.942
batch start
#iterations: 347
currently lose_sum: 93.1750293970108
time_elpased: 1.918
batch start
#iterations: 348
currently lose_sum: 93.55449372529984
time_elpased: 1.898
batch start
#iterations: 349
currently lose_sum: 93.53284984827042
time_elpased: 1.917
batch start
#iterations: 350
currently lose_sum: 93.62683206796646
time_elpased: 1.892
batch start
#iterations: 351
currently lose_sum: 93.66153091192245
time_elpased: 1.913
batch start
#iterations: 352
currently lose_sum: 93.28528141975403
time_elpased: 1.914
batch start
#iterations: 353
currently lose_sum: 93.20735651254654
time_elpased: 1.883
batch start
#iterations: 354
currently lose_sum: 93.32297784090042
time_elpased: 1.888
batch start
#iterations: 355
currently lose_sum: 93.4076241850853
time_elpased: 1.92
batch start
#iterations: 356
currently lose_sum: 93.07576620578766
time_elpased: 1.925
batch start
#iterations: 357
currently lose_sum: 93.31103920936584
time_elpased: 1.877
batch start
#iterations: 358
currently lose_sum: 93.31429952383041
time_elpased: 1.912
batch start
#iterations: 359
currently lose_sum: 92.98629128932953
time_elpased: 1.926
start validation test
0.566804123711
0.599333737129
0.407369287773
0.485049019608
0.567067543515
68.367
batch start
#iterations: 360
currently lose_sum: 93.34370571374893
time_elpased: 1.916
batch start
#iterations: 361
currently lose_sum: 93.3635606765747
time_elpased: 1.895
batch start
#iterations: 362
currently lose_sum: 93.45614725351334
time_elpased: 1.946
batch start
#iterations: 363
currently lose_sum: 93.03272533416748
time_elpased: 1.944
batch start
#iterations: 364
currently lose_sum: 93.18471759557724
time_elpased: 1.96
batch start
#iterations: 365
currently lose_sum: 93.1577108502388
time_elpased: 1.894
batch start
#iterations: 366
currently lose_sum: 93.25421524047852
time_elpased: 1.893
batch start
#iterations: 367
currently lose_sum: 92.99746829271317
time_elpased: 1.917
batch start
#iterations: 368
currently lose_sum: 92.64833319187164
time_elpased: 1.971
batch start
#iterations: 369
currently lose_sum: 92.97920906543732
time_elpased: 1.913
batch start
#iterations: 370
currently lose_sum: 93.21249431371689
time_elpased: 2.136
batch start
#iterations: 371
currently lose_sum: 93.28829580545425
time_elpased: 2.115
batch start
#iterations: 372
currently lose_sum: 93.1385390162468
time_elpased: 2.165
batch start
#iterations: 373
currently lose_sum: 92.66466248035431
time_elpased: 2.12
batch start
#iterations: 374
currently lose_sum: 93.00910294055939
time_elpased: 1.979
batch start
#iterations: 375
currently lose_sum: 93.02115070819855
time_elpased: 1.971
batch start
#iterations: 376
currently lose_sum: 92.8357122540474
time_elpased: 1.99
batch start
#iterations: 377
currently lose_sum: 93.08862733840942
time_elpased: 1.981
batch start
#iterations: 378
currently lose_sum: 93.2613918185234
time_elpased: 1.964
batch start
#iterations: 379
currently lose_sum: 92.73336970806122
time_elpased: 2.085
start validation test
0.555257731959
0.60625
0.31947303417
0.418441628471
0.555647297754
69.545
batch start
#iterations: 380
currently lose_sum: 92.78290522098541
time_elpased: 1.914
batch start
#iterations: 381
currently lose_sum: 92.40795558691025
time_elpased: 1.95
batch start
#iterations: 382
currently lose_sum: 93.25146651268005
time_elpased: 1.928
batch start
#iterations: 383
currently lose_sum: 92.93952512741089
time_elpased: 1.91
batch start
#iterations: 384
currently lose_sum: 93.02717649936676
time_elpased: 1.886
batch start
#iterations: 385
currently lose_sum: 92.76300919055939
time_elpased: 1.952
batch start
#iterations: 386
currently lose_sum: 93.2048928141594
time_elpased: 2.016
batch start
#iterations: 387
currently lose_sum: 92.86095857620239
time_elpased: 1.96
batch start
#iterations: 388
currently lose_sum: 92.74975281953812
time_elpased: 1.976
batch start
#iterations: 389
currently lose_sum: 92.53922688961029
time_elpased: 2.007
batch start
#iterations: 390
currently lose_sum: 92.57583767175674
time_elpased: 1.945
batch start
#iterations: 391
currently lose_sum: 93.01785278320312
time_elpased: 1.997
batch start
#iterations: 392
currently lose_sum: 93.19976896047592
time_elpased: 2.013
batch start
#iterations: 393
currently lose_sum: 92.85099017620087
time_elpased: 1.988
batch start
#iterations: 394
currently lose_sum: 92.74479466676712
time_elpased: 1.964
batch start
#iterations: 395
currently lose_sum: 92.84189414978027
time_elpased: 1.98
batch start
#iterations: 396
currently lose_sum: 92.94866293668747
time_elpased: 2.091
batch start
#iterations: 397
currently lose_sum: 92.18481642007828
time_elpased: 2.103
batch start
#iterations: 398
currently lose_sum: 92.12701839208603
time_elpased: 2.088
batch start
#iterations: 399
currently lose_sum: 93.00098544359207
time_elpased: 2.155
start validation test
0.563092783505
0.584653194975
0.440716344175
0.502582159624
0.563294975061
68.492
acc: 0.584
pre: 0.604
rec: 0.492
F1: 0.542
auc: 0.584
