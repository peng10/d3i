start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.38274890184402
time_elpased: 3.138
batch start
#iterations: 1
currently lose_sum: 100.33870446681976
time_elpased: 3.179
batch start
#iterations: 2
currently lose_sum: 100.18360978364944
time_elpased: 3.156
batch start
#iterations: 3
currently lose_sum: 100.14689791202545
time_elpased: 3.13
batch start
#iterations: 4
currently lose_sum: 100.14602386951447
time_elpased: 3.16
batch start
#iterations: 5
currently lose_sum: 100.1060563325882
time_elpased: 3.125
batch start
#iterations: 6
currently lose_sum: 100.07900381088257
time_elpased: 3.18
batch start
#iterations: 7
currently lose_sum: 99.88818746805191
time_elpased: 3.149
batch start
#iterations: 8
currently lose_sum: 99.85748589038849
time_elpased: 3.137
batch start
#iterations: 9
currently lose_sum: 99.8576973080635
time_elpased: 3.17
batch start
#iterations: 10
currently lose_sum: 99.83214682340622
time_elpased: 3.153
batch start
#iterations: 11
currently lose_sum: 99.79830092191696
time_elpased: 3.198
batch start
#iterations: 12
currently lose_sum: 99.78516513109207
time_elpased: 3.185
batch start
#iterations: 13
currently lose_sum: 99.54543405771255
time_elpased: 3.066
batch start
#iterations: 14
currently lose_sum: 99.64778411388397
time_elpased: 3.043
batch start
#iterations: 15
currently lose_sum: 99.48307013511658
time_elpased: 3.138
batch start
#iterations: 16
currently lose_sum: 99.38047367334366
time_elpased: 3.047
batch start
#iterations: 17
currently lose_sum: 99.51807564496994
time_elpased: 3.09
batch start
#iterations: 18
currently lose_sum: 99.46519887447357
time_elpased: 3.137
batch start
#iterations: 19
currently lose_sum: 99.41015368700027
time_elpased: 3.082
start validation test
0.579690721649
0.603936693709
0.467325306164
0.526920399165
0.579887996468
65.835
batch start
#iterations: 20
currently lose_sum: 99.50435549020767
time_elpased: 3.13
batch start
#iterations: 21
currently lose_sum: 99.27696055173874
time_elpased: 3.025
batch start
#iterations: 22
currently lose_sum: 99.09357076883316
time_elpased: 3.009
batch start
#iterations: 23
currently lose_sum: 99.20127379894257
time_elpased: 3.062
batch start
#iterations: 24
currently lose_sum: 99.1267147064209
time_elpased: 3.057
batch start
#iterations: 25
currently lose_sum: 99.17976820468903
time_elpased: 3.077
batch start
#iterations: 26
currently lose_sum: 99.20757299661636
time_elpased: 3.005
batch start
#iterations: 27
currently lose_sum: 98.92928338050842
time_elpased: 3.036
batch start
#iterations: 28
currently lose_sum: 99.14544415473938
time_elpased: 3.077
batch start
#iterations: 29
currently lose_sum: 99.09304320812225
time_elpased: 3.083
batch start
#iterations: 30
currently lose_sum: 99.02163934707642
time_elpased: 3.07
batch start
#iterations: 31
currently lose_sum: 98.93208926916122
time_elpased: 3.079
batch start
#iterations: 32
currently lose_sum: 98.65456056594849
time_elpased: 3.0
batch start
#iterations: 33
currently lose_sum: 99.14878135919571
time_elpased: 2.994
batch start
#iterations: 34
currently lose_sum: 99.237091422081
time_elpased: 3.001
batch start
#iterations: 35
currently lose_sum: 99.00546377897263
time_elpased: 3.125
batch start
#iterations: 36
currently lose_sum: 98.92055386304855
time_elpased: 3.101
batch start
#iterations: 37
currently lose_sum: 99.04189944267273
time_elpased: 2.997
batch start
#iterations: 38
currently lose_sum: 98.93956500291824
time_elpased: 3.064
batch start
#iterations: 39
currently lose_sum: 98.9537535905838
time_elpased: 3.042
start validation test
0.591237113402
0.615573664468
0.489760214058
0.545506648326
0.591415271751
65.290
batch start
#iterations: 40
currently lose_sum: 99.07250732183456
time_elpased: 3.013
batch start
#iterations: 41
currently lose_sum: 98.99009901285172
time_elpased: 3.037
batch start
#iterations: 42
currently lose_sum: 98.87492418289185
time_elpased: 3.007
batch start
#iterations: 43
currently lose_sum: 98.56216156482697
time_elpased: 3.054
batch start
#iterations: 44
currently lose_sum: 98.74819779396057
time_elpased: 3.065
batch start
#iterations: 45
currently lose_sum: 98.34671247005463
time_elpased: 3.026
batch start
#iterations: 46
currently lose_sum: 98.742271900177
time_elpased: 2.974
batch start
#iterations: 47
currently lose_sum: 99.04140836000443
time_elpased: 3.055
batch start
#iterations: 48
currently lose_sum: 98.75467425584793
time_elpased: 3.019
batch start
#iterations: 49
currently lose_sum: 98.85782778263092
time_elpased: 3.164
batch start
#iterations: 50
currently lose_sum: 98.86858642101288
time_elpased: 3.019
batch start
#iterations: 51
currently lose_sum: 98.73756635189056
time_elpased: 3.018
batch start
#iterations: 52
currently lose_sum: 98.56114727258682
time_elpased: 3.022
batch start
#iterations: 53
currently lose_sum: 98.74422937631607
time_elpased: 3.049
batch start
#iterations: 54
currently lose_sum: 98.71906971931458
time_elpased: 2.974
batch start
#iterations: 55
currently lose_sum: 98.64854091405869
time_elpased: 3.023
batch start
#iterations: 56
currently lose_sum: 98.65637350082397
time_elpased: 3.035
batch start
#iterations: 57
currently lose_sum: 98.54304987192154
time_elpased: 3.047
batch start
#iterations: 58
currently lose_sum: 98.62395679950714
time_elpased: 3.06
batch start
#iterations: 59
currently lose_sum: 98.7444349527359
time_elpased: 3.049
start validation test
0.587577319588
0.634146341463
0.417412781723
0.503444423757
0.58787606968
65.514
batch start
#iterations: 60
currently lose_sum: 98.40430581569672
time_elpased: 3.068
batch start
#iterations: 61
currently lose_sum: 98.42769485712051
time_elpased: 2.979
batch start
#iterations: 62
currently lose_sum: 98.0584990978241
time_elpased: 3.013
batch start
#iterations: 63
currently lose_sum: 98.3262727856636
time_elpased: 3.143
batch start
#iterations: 64
currently lose_sum: 98.4608855843544
time_elpased: 3.11
batch start
#iterations: 65
currently lose_sum: 98.33049005270004
time_elpased: 3.097
batch start
#iterations: 66
currently lose_sum: 98.43788069486618
time_elpased: 2.979
batch start
#iterations: 67
currently lose_sum: 98.192542552948
time_elpased: 2.996
batch start
#iterations: 68
currently lose_sum: 97.86072450876236
time_elpased: 3.072
batch start
#iterations: 69
currently lose_sum: 97.99731004238129
time_elpased: 3.063
batch start
#iterations: 70
currently lose_sum: 98.03312146663666
time_elpased: 3.031
batch start
#iterations: 71
currently lose_sum: 98.0583713054657
time_elpased: 3.067
batch start
#iterations: 72
currently lose_sum: 97.77315473556519
time_elpased: 3.064
batch start
#iterations: 73
currently lose_sum: 97.60939240455627
time_elpased: 3.076
batch start
#iterations: 74
currently lose_sum: 97.68576216697693
time_elpased: 2.981
batch start
#iterations: 75
currently lose_sum: 98.11755508184433
time_elpased: 2.981
batch start
#iterations: 76
currently lose_sum: 97.95427918434143
time_elpased: 2.957
batch start
#iterations: 77
currently lose_sum: 97.79548662900925
time_elpased: 3.006
batch start
#iterations: 78
currently lose_sum: 97.78580302000046
time_elpased: 3.07
batch start
#iterations: 79
currently lose_sum: 97.67316782474518
time_elpased: 3.091
start validation test
0.587319587629
0.610458360232
0.48656992899
0.541518726377
0.587496469194
65.479
batch start
#iterations: 80
currently lose_sum: 97.76704794168472
time_elpased: 3.048
batch start
#iterations: 81
currently lose_sum: 97.60814374685287
time_elpased: 3.026
batch start
#iterations: 82
currently lose_sum: 97.69866698980331
time_elpased: 3.039
batch start
#iterations: 83
currently lose_sum: 97.49172848463058
time_elpased: 3.082
batch start
#iterations: 84
currently lose_sum: 97.35154122114182
time_elpased: 3.004
batch start
#iterations: 85
currently lose_sum: 97.74571669101715
time_elpased: 3.051
batch start
#iterations: 86
currently lose_sum: 97.6245629787445
time_elpased: 3.102
batch start
#iterations: 87
currently lose_sum: 97.32701122760773
time_elpased: 3.061
batch start
#iterations: 88
currently lose_sum: 97.82697284221649
time_elpased: 3.007
batch start
#iterations: 89
currently lose_sum: 96.90804886817932
time_elpased: 3.075
batch start
#iterations: 90
currently lose_sum: 97.2434583902359
time_elpased: 3.026
batch start
#iterations: 91
currently lose_sum: 97.3440648317337
time_elpased: 3.018
batch start
#iterations: 92
currently lose_sum: 97.19969153404236
time_elpased: 2.987
batch start
#iterations: 93
currently lose_sum: 97.11279332637787
time_elpased: 3.077
batch start
#iterations: 94
currently lose_sum: 96.9167457818985
time_elpased: 3.118
batch start
#iterations: 95
currently lose_sum: 97.0977047085762
time_elpased: 3.025
batch start
#iterations: 96
currently lose_sum: 97.15417742729187
time_elpased: 3.056
batch start
#iterations: 97
currently lose_sum: 97.03809303045273
time_elpased: 3.107
batch start
#iterations: 98
currently lose_sum: 97.25434297323227
time_elpased: 3.043
batch start
#iterations: 99
currently lose_sum: 97.30641281604767
time_elpased: 3.114
start validation test
0.58587628866
0.621376027694
0.443346711948
0.517477477477
0.586126521315
66.087
batch start
#iterations: 100
currently lose_sum: 96.99032020568848
time_elpased: 3.12
batch start
#iterations: 101
currently lose_sum: 96.85806477069855
time_elpased: 3.065
batch start
#iterations: 102
currently lose_sum: 96.73257756233215
time_elpased: 3.068
batch start
#iterations: 103
currently lose_sum: 96.91767024993896
time_elpased: 3.041
batch start
#iterations: 104
currently lose_sum: 95.9597880244255
time_elpased: 3.057
batch start
#iterations: 105
currently lose_sum: 96.76019310951233
time_elpased: 3.158
batch start
#iterations: 106
currently lose_sum: 96.77280306816101
time_elpased: 3.046
batch start
#iterations: 107
currently lose_sum: 96.23210895061493
time_elpased: 3.181
batch start
#iterations: 108
currently lose_sum: 96.3451320528984
time_elpased: 3.093
batch start
#iterations: 109
currently lose_sum: 96.63396245241165
time_elpased: 3.069
batch start
#iterations: 110
currently lose_sum: 96.1724237203598
time_elpased: 3.048
batch start
#iterations: 111
currently lose_sum: 96.49476510286331
time_elpased: 2.993
batch start
#iterations: 112
currently lose_sum: 96.35772305727005
time_elpased: 3.072
batch start
#iterations: 113
currently lose_sum: 96.52802741527557
time_elpased: 3.127
batch start
#iterations: 114
currently lose_sum: 96.3218680024147
time_elpased: 3.103
batch start
#iterations: 115
currently lose_sum: 96.29187387228012
time_elpased: 3.111
batch start
#iterations: 116
currently lose_sum: 95.97364634275436
time_elpased: 3.079
batch start
#iterations: 117
currently lose_sum: 96.2043684720993
time_elpased: 3.096
batch start
#iterations: 118
currently lose_sum: 95.9809513092041
time_elpased: 3.098
batch start
#iterations: 119
currently lose_sum: 96.03335505723953
time_elpased: 3.018
start validation test
0.580515463918
0.601360893568
0.482041782443
0.535130812293
0.580688349654
67.098
batch start
#iterations: 120
currently lose_sum: 96.36088800430298
time_elpased: 3.044
batch start
#iterations: 121
currently lose_sum: 95.99202716350555
time_elpased: 2.952
batch start
#iterations: 122
currently lose_sum: 96.00204199552536
time_elpased: 2.979
batch start
#iterations: 123
currently lose_sum: 96.01127254962921
time_elpased: 3.062
batch start
#iterations: 124
currently lose_sum: 95.87392592430115
time_elpased: 3.037
batch start
#iterations: 125
currently lose_sum: 95.85686337947845
time_elpased: 3.146
batch start
#iterations: 126
currently lose_sum: 95.75152814388275
time_elpased: 2.962
batch start
#iterations: 127
currently lose_sum: 95.21038115024567
time_elpased: 3.074
batch start
#iterations: 128
currently lose_sum: 95.67686784267426
time_elpased: 2.945
batch start
#iterations: 129
currently lose_sum: 95.5734771490097
time_elpased: 3.012
batch start
#iterations: 130
currently lose_sum: 95.35762095451355
time_elpased: 3.037
batch start
#iterations: 131
currently lose_sum: 95.38314086198807
time_elpased: 3.037
batch start
#iterations: 132
currently lose_sum: 95.66086566448212
time_elpased: 3.038
batch start
#iterations: 133
currently lose_sum: 95.20771223306656
time_elpased: 3.037
batch start
#iterations: 134
currently lose_sum: 94.79764252901077
time_elpased: 3.053
batch start
#iterations: 135
currently lose_sum: 95.14294844865799
time_elpased: 3.095
batch start
#iterations: 136
currently lose_sum: 95.22898387908936
time_elpased: 3.046
batch start
#iterations: 137
currently lose_sum: 95.22418993711472
time_elpased: 2.961
batch start
#iterations: 138
currently lose_sum: 94.675466299057
time_elpased: 2.969
batch start
#iterations: 139
currently lose_sum: 94.60662579536438
time_elpased: 3.064
start validation test
0.583608247423
0.617356437061
0.443655449213
0.51628742515
0.583853956146
68.709
batch start
#iterations: 140
currently lose_sum: 94.85108572244644
time_elpased: 3.037
batch start
#iterations: 141
currently lose_sum: 94.89259052276611
time_elpased: 3.033
batch start
#iterations: 142
currently lose_sum: 95.2273063659668
time_elpased: 3.027
batch start
#iterations: 143
currently lose_sum: 94.48002541065216
time_elpased: 3.103
batch start
#iterations: 144
currently lose_sum: 94.52024066448212
time_elpased: 2.972
batch start
#iterations: 145
currently lose_sum: 94.58508163690567
time_elpased: 3.031
batch start
#iterations: 146
currently lose_sum: 94.56220537424088
time_elpased: 3.025
batch start
#iterations: 147
currently lose_sum: 94.17362213134766
time_elpased: 3.019
batch start
#iterations: 148
currently lose_sum: 94.56959807872772
time_elpased: 2.989
batch start
#iterations: 149
currently lose_sum: 94.06151497364044
time_elpased: 3.132
batch start
#iterations: 150
currently lose_sum: 94.16854149103165
time_elpased: 3.022
batch start
#iterations: 151
currently lose_sum: 94.14185130596161
time_elpased: 3.035
batch start
#iterations: 152
currently lose_sum: 94.1237508058548
time_elpased: 3.004
batch start
#iterations: 153
currently lose_sum: 93.58528876304626
time_elpased: 3.026
batch start
#iterations: 154
currently lose_sum: 93.99551141262054
time_elpased: 3.122
batch start
#iterations: 155
currently lose_sum: 94.01356410980225
time_elpased: 2.996
batch start
#iterations: 156
currently lose_sum: 93.81317210197449
time_elpased: 3.021
batch start
#iterations: 157
currently lose_sum: 93.41577535867691
time_elpased: 3.136
batch start
#iterations: 158
currently lose_sum: 93.42589420080185
time_elpased: 3.149
batch start
#iterations: 159
currently lose_sum: 93.68806636333466
time_elpased: 3.06
start validation test
0.57618556701
0.614559386973
0.412678810332
0.493781553996
0.576472628341
69.465
batch start
#iterations: 160
currently lose_sum: 93.47811281681061
time_elpased: 3.105
batch start
#iterations: 161
currently lose_sum: 93.73941749334335
time_elpased: 3.014
batch start
#iterations: 162
currently lose_sum: 93.35333442687988
time_elpased: 3.055
batch start
#iterations: 163
currently lose_sum: 93.2584056854248
time_elpased: 3.087
batch start
#iterations: 164
currently lose_sum: 93.28871268033981
time_elpased: 3.028
batch start
#iterations: 165
currently lose_sum: 93.3736343383789
time_elpased: 3.12
batch start
#iterations: 166
currently lose_sum: 93.26663315296173
time_elpased: 3.043
batch start
#iterations: 167
currently lose_sum: 93.05128806829453
time_elpased: 3.094
batch start
#iterations: 168
currently lose_sum: 93.01073694229126
time_elpased: 3.008
batch start
#iterations: 169
currently lose_sum: 92.62717497348785
time_elpased: 3.058
batch start
#iterations: 170
currently lose_sum: 93.28386652469635
time_elpased: 3.05
batch start
#iterations: 171
currently lose_sum: 92.82069784402847
time_elpased: 3.088
batch start
#iterations: 172
currently lose_sum: 93.00906777381897
time_elpased: 3.105
batch start
#iterations: 173
currently lose_sum: 92.36975014209747
time_elpased: 3.05
batch start
#iterations: 174
currently lose_sum: 92.57053679227829
time_elpased: 3.063
batch start
#iterations: 175
currently lose_sum: 92.55381298065186
time_elpased: 3.163
batch start
#iterations: 176
currently lose_sum: 92.39676833152771
time_elpased: 3.14
batch start
#iterations: 177
currently lose_sum: 92.28034198284149
time_elpased: 3.061
batch start
#iterations: 178
currently lose_sum: 91.89402663707733
time_elpased: 3.018
batch start
#iterations: 179
currently lose_sum: 92.12614727020264
time_elpased: 2.949
start validation test
0.580206185567
0.626793487022
0.400123494906
0.488442211055
0.580522348506
72.048
batch start
#iterations: 180
currently lose_sum: 92.181401014328
time_elpased: 2.968
batch start
#iterations: 181
currently lose_sum: 91.67658030986786
time_elpased: 3.213
batch start
#iterations: 182
currently lose_sum: 92.13874411582947
time_elpased: 3.117
batch start
#iterations: 183
currently lose_sum: 91.99170160293579
time_elpased: 3.098
batch start
#iterations: 184
currently lose_sum: 91.47040164470673
time_elpased: 3.005
batch start
#iterations: 185
currently lose_sum: 91.33841919898987
time_elpased: 3.037
batch start
#iterations: 186
currently lose_sum: 91.26478010416031
time_elpased: 3.13
batch start
#iterations: 187
currently lose_sum: 91.14402347803116
time_elpased: 3.013
batch start
#iterations: 188
currently lose_sum: 91.38142257928848
time_elpased: 3.005
batch start
#iterations: 189
currently lose_sum: 91.91899144649506
time_elpased: 3.003
batch start
#iterations: 190
currently lose_sum: 91.01579165458679
time_elpased: 3.075
batch start
#iterations: 191
currently lose_sum: 90.70591592788696
time_elpased: 2.962
batch start
#iterations: 192
currently lose_sum: 91.08395147323608
time_elpased: 2.986
batch start
#iterations: 193
currently lose_sum: 91.3141108751297
time_elpased: 3.024
batch start
#iterations: 194
currently lose_sum: 90.90876853466034
time_elpased: 3.002
batch start
#iterations: 195
currently lose_sum: 91.10304379463196
time_elpased: 2.977
batch start
#iterations: 196
currently lose_sum: 90.53665351867676
time_elpased: 3.053
batch start
#iterations: 197
currently lose_sum: 91.08855539560318
time_elpased: 3.03
batch start
#iterations: 198
currently lose_sum: 90.94445931911469
time_elpased: 3.09
batch start
#iterations: 199
currently lose_sum: 90.33919525146484
time_elpased: 3.157
start validation test
0.574329896907
0.624381926684
0.37686528764
0.470029521242
0.574676576486
72.474
batch start
#iterations: 200
currently lose_sum: 90.58115381002426
time_elpased: 3.086
batch start
#iterations: 201
currently lose_sum: 90.81310433149338
time_elpased: 3.033
batch start
#iterations: 202
currently lose_sum: 90.16976726055145
time_elpased: 2.996
batch start
#iterations: 203
currently lose_sum: 90.65707224607468
time_elpased: 2.983
batch start
#iterations: 204
currently lose_sum: 90.012570977211
time_elpased: 3.075
batch start
#iterations: 205
currently lose_sum: 90.41716039180756
time_elpased: 3.05
batch start
#iterations: 206
currently lose_sum: 89.92099869251251
time_elpased: 3.043
batch start
#iterations: 207
currently lose_sum: 90.04691982269287
time_elpased: 3.026
batch start
#iterations: 208
currently lose_sum: 89.30758255720139
time_elpased: 3.06
batch start
#iterations: 209
currently lose_sum: 89.29227614402771
time_elpased: 2.999
batch start
#iterations: 210
currently lose_sum: 89.37679880857468
time_elpased: 3.004
batch start
#iterations: 211
currently lose_sum: 89.03455466032028
time_elpased: 3.068
batch start
#iterations: 212
currently lose_sum: 89.50558453798294
time_elpased: 2.99
batch start
#iterations: 213
currently lose_sum: 89.35058069229126
time_elpased: 3.066
batch start
#iterations: 214
currently lose_sum: 89.08280909061432
time_elpased: 3.004
batch start
#iterations: 215
currently lose_sum: 89.15459299087524
time_elpased: 3.003
batch start
#iterations: 216
currently lose_sum: 88.74841231107712
time_elpased: 3.013
batch start
#iterations: 217
currently lose_sum: 89.17802065610886
time_elpased: 3.098
batch start
#iterations: 218
currently lose_sum: 89.02592986822128
time_elpased: 2.975
batch start
#iterations: 219
currently lose_sum: 89.01209181547165
time_elpased: 2.974
start validation test
0.546597938144
0.539592468403
0.645878357518
0.587970770096
0.546423636055
80.262
batch start
#iterations: 220
currently lose_sum: 88.70671737194061
time_elpased: 3.132
batch start
#iterations: 221
currently lose_sum: 89.1070568561554
time_elpased: 2.942
batch start
#iterations: 222
currently lose_sum: 88.45041805505753
time_elpased: 2.982
batch start
#iterations: 223
currently lose_sum: 87.79265367984772
time_elpased: 3.095
batch start
#iterations: 224
currently lose_sum: 87.94821977615356
time_elpased: 3.042
batch start
#iterations: 225
currently lose_sum: 88.1200949549675
time_elpased: 3.055
batch start
#iterations: 226
currently lose_sum: 88.31745898723602
time_elpased: 2.987
batch start
#iterations: 227
currently lose_sum: 88.35937088727951
time_elpased: 3.079
batch start
#iterations: 228
currently lose_sum: 88.38697576522827
time_elpased: 3.032
batch start
#iterations: 229
currently lose_sum: 87.08259630203247
time_elpased: 2.998
batch start
#iterations: 230
currently lose_sum: 87.19503790140152
time_elpased: 3.067
batch start
#iterations: 231
currently lose_sum: 87.92183661460876
time_elpased: 3.018
batch start
#iterations: 232
currently lose_sum: 87.26710003614426
time_elpased: 3.041
batch start
#iterations: 233
currently lose_sum: 87.23697763681412
time_elpased: 3.031
batch start
#iterations: 234
currently lose_sum: 87.556691467762
time_elpased: 3.072
batch start
#iterations: 235
currently lose_sum: 87.21044462919235
time_elpased: 2.998
batch start
#iterations: 236
currently lose_sum: 87.43349117040634
time_elpased: 2.995
batch start
#iterations: 237
currently lose_sum: 87.46851849555969
time_elpased: 3.032
batch start
#iterations: 238
currently lose_sum: 87.74707752466202
time_elpased: 3.047
batch start
#iterations: 239
currently lose_sum: 87.04289948940277
time_elpased: 2.963
start validation test
0.561237113402
0.635302043566
0.291139240506
0.399294283698
0.561711311878
83.234
batch start
#iterations: 240
currently lose_sum: 87.33101856708527
time_elpased: 2.951
batch start
#iterations: 241
currently lose_sum: 87.09382730722427
time_elpased: 3.026
batch start
#iterations: 242
currently lose_sum: 86.30128425359726
time_elpased: 3.007
batch start
#iterations: 243
currently lose_sum: 87.13769751787186
time_elpased: 3.074
batch start
#iterations: 244
currently lose_sum: 87.13695925474167
time_elpased: 3.037
batch start
#iterations: 245
currently lose_sum: 86.45985901355743
time_elpased: 3.069
batch start
#iterations: 246
currently lose_sum: 86.00337475538254
time_elpased: 3.035
batch start
#iterations: 247
currently lose_sum: 86.3685799241066
time_elpased: 3.069
batch start
#iterations: 248
currently lose_sum: 86.73463433980942
time_elpased: 3.013
batch start
#iterations: 249
currently lose_sum: 86.39141869544983
time_elpased: 3.06
batch start
#iterations: 250
currently lose_sum: 86.2503759264946
time_elpased: 3.12
batch start
#iterations: 251
currently lose_sum: 86.29184114933014
time_elpased: 3.068
batch start
#iterations: 252
currently lose_sum: 86.35563838481903
time_elpased: 3.055
batch start
#iterations: 253
currently lose_sum: 85.81468564271927
time_elpased: 2.988
batch start
#iterations: 254
currently lose_sum: 86.16409999132156
time_elpased: 2.969
batch start
#iterations: 255
currently lose_sum: 85.58509945869446
time_elpased: 3.044
batch start
#iterations: 256
currently lose_sum: 85.67998558282852
time_elpased: 3.116
batch start
#iterations: 257
currently lose_sum: 86.02699798345566
time_elpased: 3.116
batch start
#iterations: 258
currently lose_sum: 86.08083659410477
time_elpased: 3.005
batch start
#iterations: 259
currently lose_sum: 85.84102809429169
time_elpased: 2.973
start validation test
0.567835051546
0.629191703819
0.334053720284
0.436407636461
0.568245490732
84.470
batch start
#iterations: 260
currently lose_sum: 85.89512467384338
time_elpased: 3.05
batch start
#iterations: 261
currently lose_sum: 85.60035812854767
time_elpased: 3.039
batch start
#iterations: 262
currently lose_sum: 84.98161071538925
time_elpased: 3.065
batch start
#iterations: 263
currently lose_sum: 85.22867095470428
time_elpased: 2.981
batch start
#iterations: 264
currently lose_sum: 85.16540950536728
time_elpased: 3.011
batch start
#iterations: 265
currently lose_sum: 84.78209668397903
time_elpased: 3.063
batch start
#iterations: 266
currently lose_sum: 84.8038170337677
time_elpased: 3.045
batch start
#iterations: 267
currently lose_sum: 85.58943009376526
time_elpased: 3.035
batch start
#iterations: 268
currently lose_sum: 85.36143642663956
time_elpased: 2.998
batch start
#iterations: 269
currently lose_sum: 85.0330423116684
time_elpased: 3.047
batch start
#iterations: 270
currently lose_sum: 85.43722176551819
time_elpased: 3.021
batch start
#iterations: 271
currently lose_sum: 84.61273527145386
time_elpased: 2.999
batch start
#iterations: 272
currently lose_sum: 84.55395245552063
time_elpased: 2.979
batch start
#iterations: 273
currently lose_sum: 84.52838563919067
time_elpased: 2.967
batch start
#iterations: 274
currently lose_sum: 85.15227711200714
time_elpased: 3.099
batch start
#iterations: 275
currently lose_sum: 84.7709349989891
time_elpased: 3.024
batch start
#iterations: 276
currently lose_sum: 84.73978793621063
time_elpased: 3.044
batch start
#iterations: 277
currently lose_sum: 84.60489755868912
time_elpased: 3.0
batch start
#iterations: 278
currently lose_sum: 84.69851183891296
time_elpased: 2.973
batch start
#iterations: 279
currently lose_sum: 84.61519446969032
time_elpased: 2.905
start validation test
0.563402061856
0.597498045348
0.393228362663
0.474304865938
0.563700828032
93.270
batch start
#iterations: 280
currently lose_sum: 83.38255822658539
time_elpased: 3.094
batch start
#iterations: 281
currently lose_sum: 83.72018951177597
time_elpased: 3.078
batch start
#iterations: 282
currently lose_sum: 84.10870856046677
time_elpased: 3.004
batch start
#iterations: 283
currently lose_sum: 84.09283471107483
time_elpased: 3.037
batch start
#iterations: 284
currently lose_sum: 84.49990004301071
time_elpased: 3.127
batch start
#iterations: 285
currently lose_sum: 83.25867432355881
time_elpased: 2.979
batch start
#iterations: 286
currently lose_sum: 83.26565659046173
time_elpased: 3.054
batch start
#iterations: 287
currently lose_sum: 84.16714441776276
time_elpased: 2.999
batch start
#iterations: 288
currently lose_sum: 83.15584200620651
time_elpased: 2.928
batch start
#iterations: 289
currently lose_sum: 83.21857684850693
time_elpased: 2.967
batch start
#iterations: 290
currently lose_sum: 83.44743609428406
time_elpased: 3.052
batch start
#iterations: 291
currently lose_sum: 83.18919718265533
time_elpased: 3.075
batch start
#iterations: 292
currently lose_sum: 83.60345175862312
time_elpased: 3.098
batch start
#iterations: 293
currently lose_sum: 82.78950917720795
time_elpased: 3.101
batch start
#iterations: 294
currently lose_sum: 83.93304181098938
time_elpased: 2.967
batch start
#iterations: 295
currently lose_sum: 83.04174071550369
time_elpased: 3.024
batch start
#iterations: 296
currently lose_sum: 82.7290210723877
time_elpased: 3.02
batch start
#iterations: 297
currently lose_sum: 83.003861784935
time_elpased: 2.971
batch start
#iterations: 298
currently lose_sum: 83.23585551977158
time_elpased: 3.052
batch start
#iterations: 299
currently lose_sum: 82.93684959411621
time_elpased: 2.97
start validation test
0.560721649485
0.599038620918
0.371925491407
0.458920634921
0.56105311026
105.275
batch start
#iterations: 300
currently lose_sum: 82.41625627875328
time_elpased: 3.012
batch start
#iterations: 301
currently lose_sum: 83.23410648107529
time_elpased: 3.012
batch start
#iterations: 302
currently lose_sum: 83.26648253202438
time_elpased: 3.021
batch start
#iterations: 303
currently lose_sum: 82.57279908657074
time_elpased: 2.969
batch start
#iterations: 304
currently lose_sum: 83.0784615278244
time_elpased: 2.964
batch start
#iterations: 305
currently lose_sum: 83.24819701910019
time_elpased: 3.094
batch start
#iterations: 306
currently lose_sum: 83.26455974578857
time_elpased: 3.072
batch start
#iterations: 307
currently lose_sum: 82.39466017484665
time_elpased: 3.033
batch start
#iterations: 308
currently lose_sum: 82.57226496934891
time_elpased: 3.034
batch start
#iterations: 309
currently lose_sum: 82.95339733362198
time_elpased: 2.984
batch start
#iterations: 310
currently lose_sum: 83.0385131239891
time_elpased: 3.156
batch start
#iterations: 311
currently lose_sum: 82.14805191755295
time_elpased: 3.029
batch start
#iterations: 312
currently lose_sum: 81.84376519918442
time_elpased: 3.002
batch start
#iterations: 313
currently lose_sum: 81.69859394431114
time_elpased: 3.207
batch start
#iterations: 314
currently lose_sum: 82.18695318698883
time_elpased: 3.082
batch start
#iterations: 315
currently lose_sum: 81.65364041924477
time_elpased: 3.086
batch start
#iterations: 316
currently lose_sum: 82.2331553697586
time_elpased: 3.099
batch start
#iterations: 317
currently lose_sum: 81.0807409286499
time_elpased: 3.056
batch start
#iterations: 318
currently lose_sum: 81.85813710093498
time_elpased: 3.03
batch start
#iterations: 319
currently lose_sum: 81.694758862257
time_elpased: 3.0
start validation test
0.557577319588
0.595971563981
0.362354636205
0.450688
0.557920063119
112.984
batch start
#iterations: 320
currently lose_sum: 81.97197699546814
time_elpased: 3.026
batch start
#iterations: 321
currently lose_sum: 81.97908729314804
time_elpased: 3.085
batch start
#iterations: 322
currently lose_sum: 82.39825576543808
time_elpased: 3.046
batch start
#iterations: 323
currently lose_sum: 82.302803337574
time_elpased: 3.137
batch start
#iterations: 324
currently lose_sum: 82.24837306141853
time_elpased: 3.047
batch start
#iterations: 325
currently lose_sum: 81.73958796262741
time_elpased: 3.032
batch start
#iterations: 326
currently lose_sum: 82.0220610499382
time_elpased: 2.981
batch start
#iterations: 327
currently lose_sum: 81.68965342640877
time_elpased: 2.947
batch start
#iterations: 328
currently lose_sum: 81.11058229207993
time_elpased: 3.006
batch start
#iterations: 329
currently lose_sum: 82.03177917003632
time_elpased: 3.028
batch start
#iterations: 330
currently lose_sum: 80.96769404411316
time_elpased: 3.13
batch start
#iterations: 331
currently lose_sum: 82.0314904153347
time_elpased: 3.067
batch start
#iterations: 332
currently lose_sum: 81.66481202840805
time_elpased: 3.027
batch start
#iterations: 333
currently lose_sum: 81.95790386199951
time_elpased: 2.987
batch start
#iterations: 334
currently lose_sum: 81.70732814073563
time_elpased: 3.023
batch start
#iterations: 335
currently lose_sum: 82.78385162353516
time_elpased: 3.0
batch start
#iterations: 336
currently lose_sum: 80.81094521284103
time_elpased: 3.052
batch start
#iterations: 337
currently lose_sum: 81.15681087970734
time_elpased: 3.005
batch start
#iterations: 338
currently lose_sum: 80.8689723610878
time_elpased: 2.987
batch start
#iterations: 339
currently lose_sum: 81.54040294885635
time_elpased: 3.021
start validation test
0.56
0.601652608022
0.359678913245
0.450212546696
0.560351694565
109.952
batch start
#iterations: 340
currently lose_sum: 81.06824889779091
time_elpased: 3.089
batch start
#iterations: 341
currently lose_sum: 80.82425159215927
time_elpased: 3.014
batch start
#iterations: 342
currently lose_sum: 82.48227107524872
time_elpased: 3.04
batch start
#iterations: 343
currently lose_sum: 81.67334073781967
time_elpased: 3.092
batch start
#iterations: 344
currently lose_sum: 81.07420194149017
time_elpased: 2.975
batch start
#iterations: 345
currently lose_sum: 79.95225271582603
time_elpased: 2.98
batch start
#iterations: 346
currently lose_sum: 80.64135086536407
time_elpased: 3.101
batch start
#iterations: 347
currently lose_sum: 81.16315999627113
time_elpased: 3.006
batch start
#iterations: 348
currently lose_sum: 80.76875230669975
time_elpased: 2.998
batch start
#iterations: 349
currently lose_sum: 81.19960695505142
time_elpased: 3.037
batch start
#iterations: 350
currently lose_sum: 79.99233284592628
time_elpased: 3.095
batch start
#iterations: 351
currently lose_sum: 79.79034355282784
time_elpased: 3.018
batch start
#iterations: 352
currently lose_sum: 80.49418115615845
time_elpased: 3.191
batch start
#iterations: 353
currently lose_sum: 80.57764592766762
time_elpased: 3.051
batch start
#iterations: 354
currently lose_sum: 80.89801144599915
time_elpased: 3.026
batch start
#iterations: 355
currently lose_sum: 80.43742415308952
time_elpased: 3.04
batch start
#iterations: 356
currently lose_sum: 80.1454128921032
time_elpased: 3.031
batch start
#iterations: 357
currently lose_sum: 79.93814259767532
time_elpased: 3.135
batch start
#iterations: 358
currently lose_sum: 80.04276305437088
time_elpased: 3.159
batch start
#iterations: 359
currently lose_sum: 80.52449241280556
time_elpased: 3.01
start validation test
0.562319587629
0.614365671642
0.338890604096
0.436824301917
0.562711851671
113.945
batch start
#iterations: 360
currently lose_sum: 80.4679793715477
time_elpased: 3.053
batch start
#iterations: 361
currently lose_sum: 79.86332470178604
time_elpased: 3.076
batch start
#iterations: 362
currently lose_sum: 79.73686635494232
time_elpased: 3.101
batch start
#iterations: 363
currently lose_sum: 79.84222063422203
time_elpased: 3.057
batch start
#iterations: 364
currently lose_sum: 80.60386124253273
time_elpased: 3.064
batch start
#iterations: 365
currently lose_sum: 79.84354051947594
time_elpased: 3.08
batch start
#iterations: 366
currently lose_sum: 79.80947950482368
time_elpased: 3.111
batch start
#iterations: 367
currently lose_sum: 80.11639860272408
time_elpased: 3.078
batch start
#iterations: 368
currently lose_sum: 79.13694334030151
time_elpased: 3.113
batch start
#iterations: 369
currently lose_sum: 79.19529914855957
time_elpased: 3.027
batch start
#iterations: 370
currently lose_sum: 79.97640931606293
time_elpased: 3.069
batch start
#iterations: 371
currently lose_sum: 80.31453117728233
time_elpased: 3.114
batch start
#iterations: 372
currently lose_sum: 79.6128922700882
time_elpased: 3.105
batch start
#iterations: 373
currently lose_sum: 79.5545925796032
time_elpased: 3.12
batch start
#iterations: 374
currently lose_sum: 79.99494272470474
time_elpased: 3.076
batch start
#iterations: 375
currently lose_sum: 79.60983508825302
time_elpased: 2.999
batch start
#iterations: 376
currently lose_sum: 79.197195738554
time_elpased: 3.087
batch start
#iterations: 377
currently lose_sum: 80.25018882751465
time_elpased: 2.968
batch start
#iterations: 378
currently lose_sum: 79.91735085844994
time_elpased: 2.977
batch start
#iterations: 379
currently lose_sum: 79.3296331167221
time_elpased: 3.002
start validation test
0.559948453608
0.632643884892
0.289595554183
0.397317331451
0.560423099822
117.130
batch start
#iterations: 380
currently lose_sum: 78.69154688715935
time_elpased: 3.013
batch start
#iterations: 381
currently lose_sum: 78.6369939148426
time_elpased: 3.099
batch start
#iterations: 382
currently lose_sum: 79.30176317691803
time_elpased: 3.011
batch start
#iterations: 383
currently lose_sum: 79.71085423231125
time_elpased: 3.057
batch start
#iterations: 384
currently lose_sum: 79.92804980278015
time_elpased: 2.999
batch start
#iterations: 385
currently lose_sum: 78.27957889437675
time_elpased: 3.122
batch start
#iterations: 386
currently lose_sum: 79.98372209072113
time_elpased: 2.969
batch start
#iterations: 387
currently lose_sum: 79.40649503469467
time_elpased: 2.991
batch start
#iterations: 388
currently lose_sum: 79.16605311632156
time_elpased: 3.019
batch start
#iterations: 389
currently lose_sum: 79.28121075034142
time_elpased: 3.024
batch start
#iterations: 390
currently lose_sum: 79.07670909166336
time_elpased: 3.007
batch start
#iterations: 391
currently lose_sum: 79.7118536233902
time_elpased: 3.004
batch start
#iterations: 392
currently lose_sum: 79.14093896746635
time_elpased: 3.054
batch start
#iterations: 393
currently lose_sum: 79.0098424255848
time_elpased: 3.01
batch start
#iterations: 394
currently lose_sum: 78.63135698437691
time_elpased: 3.084
batch start
#iterations: 395
currently lose_sum: 78.49966135621071
time_elpased: 3.085
batch start
#iterations: 396
currently lose_sum: 79.14263847470284
time_elpased: 3.035
batch start
#iterations: 397
currently lose_sum: 80.11620017886162
time_elpased: 3.063
batch start
#iterations: 398
currently lose_sum: 78.81830623745918
time_elpased: 3.017
batch start
#iterations: 399
currently lose_sum: 78.88711893558502
time_elpased: 3.023
start validation test
0.554845360825
0.635023732201
0.261603375527
0.37055393586
0.555360192359
125.550
acc: 0.598
pre: 0.622
rec: 0.501
F1: 0.555
auc: 0.598
