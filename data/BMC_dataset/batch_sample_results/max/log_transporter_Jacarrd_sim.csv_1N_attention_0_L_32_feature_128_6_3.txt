start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.44415628910065
time_elpased: 3.326
batch start
#iterations: 1
currently lose_sum: 100.2668542265892
time_elpased: 3.374
batch start
#iterations: 2
currently lose_sum: 100.23041152954102
time_elpased: 3.194
batch start
#iterations: 3
currently lose_sum: 100.1038743853569
time_elpased: 3.293
batch start
#iterations: 4
currently lose_sum: 100.03445607423782
time_elpased: 3.306
batch start
#iterations: 5
currently lose_sum: 99.86685752868652
time_elpased: 3.372
batch start
#iterations: 6
currently lose_sum: 99.96352422237396
time_elpased: 3.362
batch start
#iterations: 7
currently lose_sum: 99.71897053718567
time_elpased: 3.341
batch start
#iterations: 8
currently lose_sum: 99.80590349435806
time_elpased: 3.384
batch start
#iterations: 9
currently lose_sum: 99.67764526605606
time_elpased: 3.375
batch start
#iterations: 10
currently lose_sum: 99.63533294200897
time_elpased: 3.26
batch start
#iterations: 11
currently lose_sum: 99.6991359591484
time_elpased: 3.243
batch start
#iterations: 12
currently lose_sum: 99.48740434646606
time_elpased: 3.195
batch start
#iterations: 13
currently lose_sum: 99.59207731485367
time_elpased: 3.14
batch start
#iterations: 14
currently lose_sum: 99.58538520336151
time_elpased: 3.209
batch start
#iterations: 15
currently lose_sum: 99.53599852323532
time_elpased: 3.109
batch start
#iterations: 16
currently lose_sum: 99.49402076005936
time_elpased: 3.146
batch start
#iterations: 17
currently lose_sum: 99.45366662740707
time_elpased: 3.147
batch start
#iterations: 18
currently lose_sum: 99.28000909090042
time_elpased: 3.205
batch start
#iterations: 19
currently lose_sum: 99.44777154922485
time_elpased: 3.267
start validation test
0.585103092784
0.604619053596
0.495780156443
0.544817055929
0.585250673017
65.827
batch start
#iterations: 20
currently lose_sum: 99.36533683538437
time_elpased: 3.123
batch start
#iterations: 21
currently lose_sum: 99.2493222951889
time_elpased: 3.084
batch start
#iterations: 22
currently lose_sum: 99.02258586883545
time_elpased: 3.069
batch start
#iterations: 23
currently lose_sum: 99.14919435977936
time_elpased: 3.103
batch start
#iterations: 24
currently lose_sum: 98.95477426052094
time_elpased: 3.109
batch start
#iterations: 25
currently lose_sum: 99.1356086730957
time_elpased: 3.123
batch start
#iterations: 26
currently lose_sum: 99.27134430408478
time_elpased: 3.059
batch start
#iterations: 27
currently lose_sum: 99.08855324983597
time_elpased: 3.078
batch start
#iterations: 28
currently lose_sum: 99.0179494023323
time_elpased: 3.096
batch start
#iterations: 29
currently lose_sum: 99.12586116790771
time_elpased: 3.092
batch start
#iterations: 30
currently lose_sum: 98.87694185972214
time_elpased: 3.125
batch start
#iterations: 31
currently lose_sum: 99.26717734336853
time_elpased: 3.108
batch start
#iterations: 32
currently lose_sum: 98.99377477169037
time_elpased: 3.049
batch start
#iterations: 33
currently lose_sum: 99.22456097602844
time_elpased: 3.109
batch start
#iterations: 34
currently lose_sum: 98.83283442258835
time_elpased: 3.165
batch start
#iterations: 35
currently lose_sum: 99.14873975515366
time_elpased: 3.092
batch start
#iterations: 36
currently lose_sum: 98.73798257112503
time_elpased: 3.152
batch start
#iterations: 37
currently lose_sum: 98.79540115594864
time_elpased: 3.091
batch start
#iterations: 38
currently lose_sum: 98.68002784252167
time_elpased: 3.1
batch start
#iterations: 39
currently lose_sum: 98.67136764526367
time_elpased: 3.095
start validation test
0.572989690722
0.59019904258
0.48219431865
0.530757901892
0.573139703728
66.104
batch start
#iterations: 40
currently lose_sum: 98.77428597211838
time_elpased: 3.112
batch start
#iterations: 41
currently lose_sum: 98.72537189722061
time_elpased: 3.06
batch start
#iterations: 42
currently lose_sum: 98.60755270719528
time_elpased: 3.116
batch start
#iterations: 43
currently lose_sum: 98.52961999177933
time_elpased: 3.162
batch start
#iterations: 44
currently lose_sum: 98.73515832424164
time_elpased: 3.139
batch start
#iterations: 45
currently lose_sum: 98.68136012554169
time_elpased: 3.073
batch start
#iterations: 46
currently lose_sum: 98.50617688894272
time_elpased: 3.16
batch start
#iterations: 47
currently lose_sum: 98.65382188558578
time_elpased: 3.208
batch start
#iterations: 48
currently lose_sum: 98.42272466421127
time_elpased: 3.095
batch start
#iterations: 49
currently lose_sum: 98.40244662761688
time_elpased: 3.071
batch start
#iterations: 50
currently lose_sum: 98.28633856773376
time_elpased: 3.169
batch start
#iterations: 51
currently lose_sum: 98.16648763418198
time_elpased: 3.071
batch start
#iterations: 52
currently lose_sum: 98.57340657711029
time_elpased: 3.125
batch start
#iterations: 53
currently lose_sum: 98.25972068309784
time_elpased: 3.109
batch start
#iterations: 54
currently lose_sum: 98.23777318000793
time_elpased: 3.108
batch start
#iterations: 55
currently lose_sum: 98.66993725299835
time_elpased: 3.196
batch start
#iterations: 56
currently lose_sum: 98.04588758945465
time_elpased: 3.151
batch start
#iterations: 57
currently lose_sum: 98.21287554502487
time_elpased: 3.087
batch start
#iterations: 58
currently lose_sum: 98.29029989242554
time_elpased: 3.085
batch start
#iterations: 59
currently lose_sum: 98.30915242433548
time_elpased: 3.13
start validation test
0.583659793814
0.622771535581
0.427850967476
0.507229577207
0.583917222689
65.161
batch start
#iterations: 60
currently lose_sum: 97.88918542861938
time_elpased: 3.023
batch start
#iterations: 61
currently lose_sum: 98.49707102775574
time_elpased: 3.129
batch start
#iterations: 62
currently lose_sum: 97.98070108890533
time_elpased: 3.107
batch start
#iterations: 63
currently lose_sum: 97.94319623708725
time_elpased: 3.113
batch start
#iterations: 64
currently lose_sum: 97.98342835903168
time_elpased: 3.098
batch start
#iterations: 65
currently lose_sum: 97.84236001968384
time_elpased: 3.129
batch start
#iterations: 66
currently lose_sum: 98.02842140197754
time_elpased: 3.144
batch start
#iterations: 67
currently lose_sum: 98.1260821223259
time_elpased: 3.142
batch start
#iterations: 68
currently lose_sum: 97.45180106163025
time_elpased: 3.099
batch start
#iterations: 69
currently lose_sum: 97.81724119186401
time_elpased: 3.129
batch start
#iterations: 70
currently lose_sum: 97.66298669576645
time_elpased: 3.142
batch start
#iterations: 71
currently lose_sum: 97.501817882061
time_elpased: 3.237
batch start
#iterations: 72
currently lose_sum: 97.64615070819855
time_elpased: 3.109
batch start
#iterations: 73
currently lose_sum: 97.43953269720078
time_elpased: 3.032
batch start
#iterations: 74
currently lose_sum: 97.7276953458786
time_elpased: 3.114
batch start
#iterations: 75
currently lose_sum: 97.69526612758636
time_elpased: 3.092
batch start
#iterations: 76
currently lose_sum: 97.69848817586899
time_elpased: 3.191
batch start
#iterations: 77
currently lose_sum: 97.2364906668663
time_elpased: 3.07
batch start
#iterations: 78
currently lose_sum: 97.35781013965607
time_elpased: 3.1
batch start
#iterations: 79
currently lose_sum: 97.49764215946198
time_elpased: 3.128
start validation test
0.590309278351
0.614035087719
0.489913544669
0.544996565148
0.590475153169
65.261
batch start
#iterations: 80
currently lose_sum: 97.20680445432663
time_elpased: 3.103
batch start
#iterations: 81
currently lose_sum: 97.3215406537056
time_elpased: 3.236
batch start
#iterations: 82
currently lose_sum: 97.0411319732666
time_elpased: 3.109
batch start
#iterations: 83
currently lose_sum: 97.28063070774078
time_elpased: 3.14
batch start
#iterations: 84
currently lose_sum: 96.67240965366364
time_elpased: 3.146
batch start
#iterations: 85
currently lose_sum: 96.82232135534286
time_elpased: 3.157
batch start
#iterations: 86
currently lose_sum: 97.30192840099335
time_elpased: 3.109
batch start
#iterations: 87
currently lose_sum: 96.72449070215225
time_elpased: 3.151
batch start
#iterations: 88
currently lose_sum: 97.06536918878555
time_elpased: 3.111
batch start
#iterations: 89
currently lose_sum: 96.94748711585999
time_elpased: 3.112
batch start
#iterations: 90
currently lose_sum: 96.76999843120575
time_elpased: 3.049
batch start
#iterations: 91
currently lose_sum: 96.68365043401718
time_elpased: 3.159
batch start
#iterations: 92
currently lose_sum: 96.43700855970383
time_elpased: 3.158
batch start
#iterations: 93
currently lose_sum: 96.45409005880356
time_elpased: 3.183
batch start
#iterations: 94
currently lose_sum: 96.83861708641052
time_elpased: 3.007
batch start
#iterations: 95
currently lose_sum: 96.81847685575485
time_elpased: 3.093
batch start
#iterations: 96
currently lose_sum: 96.46047991514206
time_elpased: 3.055
batch start
#iterations: 97
currently lose_sum: 96.24478358030319
time_elpased: 3.087
batch start
#iterations: 98
currently lose_sum: 96.38461250066757
time_elpased: 3.164
batch start
#iterations: 99
currently lose_sum: 96.55737882852554
time_elpased: 3.081
start validation test
0.582680412371
0.63222331048
0.398620831618
0.488953414973
0.58298451742
66.457
batch start
#iterations: 100
currently lose_sum: 96.57936090230942
time_elpased: 3.119
batch start
#iterations: 101
currently lose_sum: 96.39028298854828
time_elpased: 3.136
batch start
#iterations: 102
currently lose_sum: 96.78049737215042
time_elpased: 3.084
batch start
#iterations: 103
currently lose_sum: 96.1560810804367
time_elpased: 3.189
batch start
#iterations: 104
currently lose_sum: 96.14967960119247
time_elpased: 3.164
batch start
#iterations: 105
currently lose_sum: 96.05569291114807
time_elpased: 3.161
batch start
#iterations: 106
currently lose_sum: 95.74303114414215
time_elpased: 3.081
batch start
#iterations: 107
currently lose_sum: 96.14435374736786
time_elpased: 3.1
batch start
#iterations: 108
currently lose_sum: 95.9265370965004
time_elpased: 3.052
batch start
#iterations: 109
currently lose_sum: 95.97911149263382
time_elpased: 3.046
batch start
#iterations: 110
currently lose_sum: 95.52249413728714
time_elpased: 3.138
batch start
#iterations: 111
currently lose_sum: 95.9035793542862
time_elpased: 3.134
batch start
#iterations: 112
currently lose_sum: 95.72114753723145
time_elpased: 3.074
batch start
#iterations: 113
currently lose_sum: 95.6218250989914
time_elpased: 3.156
batch start
#iterations: 114
currently lose_sum: 95.80184203386307
time_elpased: 3.123
batch start
#iterations: 115
currently lose_sum: 95.73129171133041
time_elpased: 3.174
batch start
#iterations: 116
currently lose_sum: 95.66707921028137
time_elpased: 3.033
batch start
#iterations: 117
currently lose_sum: 95.46985071897507
time_elpased: 3.085
batch start
#iterations: 118
currently lose_sum: 95.56081432104111
time_elpased: 3.052
batch start
#iterations: 119
currently lose_sum: 95.68171101808548
time_elpased: 3.145
start validation test
0.569072164948
0.623903508772
0.351379168382
0.449565446405
0.569431839457
67.822
batch start
#iterations: 120
currently lose_sum: 95.1866329908371
time_elpased: 3.041
batch start
#iterations: 121
currently lose_sum: 95.01501470804214
time_elpased: 3.072
batch start
#iterations: 122
currently lose_sum: 95.02746969461441
time_elpased: 3.074
batch start
#iterations: 123
currently lose_sum: 95.34019327163696
time_elpased: 3.055
batch start
#iterations: 124
currently lose_sum: 95.37268912792206
time_elpased: 3.004
batch start
#iterations: 125
currently lose_sum: 95.07327634096146
time_elpased: 3.029
batch start
#iterations: 126
currently lose_sum: 94.67816668748856
time_elpased: 3.006
batch start
#iterations: 127
currently lose_sum: 95.26926720142365
time_elpased: 3.144
batch start
#iterations: 128
currently lose_sum: 95.01879215240479
time_elpased: 3.087
batch start
#iterations: 129
currently lose_sum: 94.67403042316437
time_elpased: 3.13
batch start
#iterations: 130
currently lose_sum: 94.76463145017624
time_elpased: 3.117
batch start
#iterations: 131
currently lose_sum: 94.53738230466843
time_elpased: 3.171
batch start
#iterations: 132
currently lose_sum: 94.57078808546066
time_elpased: 3.119
batch start
#iterations: 133
currently lose_sum: 94.50188881158829
time_elpased: 3.06
batch start
#iterations: 134
currently lose_sum: 94.60609620809555
time_elpased: 3.007
batch start
#iterations: 135
currently lose_sum: 93.94581824541092
time_elpased: 3.132
batch start
#iterations: 136
currently lose_sum: 94.90906149148941
time_elpased: 3.093
batch start
#iterations: 137
currently lose_sum: 94.32444149255753
time_elpased: 3.107
batch start
#iterations: 138
currently lose_sum: 94.31366991996765
time_elpased: 3.108
batch start
#iterations: 139
currently lose_sum: 94.38108289241791
time_elpased: 3.109
start validation test
0.574845360825
0.611890243902
0.413132976534
0.493241582698
0.575112543616
68.735
batch start
#iterations: 140
currently lose_sum: 93.77621972560883
time_elpased: 3.08
batch start
#iterations: 141
currently lose_sum: 94.18831270933151
time_elpased: 3.114
batch start
#iterations: 142
currently lose_sum: 94.17375558614731
time_elpased: 3.117
batch start
#iterations: 143
currently lose_sum: 94.04113698005676
time_elpased: 3.113
batch start
#iterations: 144
currently lose_sum: 93.77313941717148
time_elpased: 3.108
batch start
#iterations: 145
currently lose_sum: 93.58592855930328
time_elpased: 3.091
batch start
#iterations: 146
currently lose_sum: 93.65055286884308
time_elpased: 3.122
batch start
#iterations: 147
currently lose_sum: 93.71681666374207
time_elpased: 3.085
batch start
#iterations: 148
currently lose_sum: 93.48174107074738
time_elpased: 3.02
batch start
#iterations: 149
currently lose_sum: 93.7781395316124
time_elpased: 3.068
batch start
#iterations: 150
currently lose_sum: 93.4577186703682
time_elpased: 3.238
batch start
#iterations: 151
currently lose_sum: 93.9456033706665
time_elpased: 3.088
batch start
#iterations: 152
currently lose_sum: 93.16940248012543
time_elpased: 3.065
batch start
#iterations: 153
currently lose_sum: 93.73151355981827
time_elpased: 3.182
batch start
#iterations: 154
currently lose_sum: 93.32045340538025
time_elpased: 3.202
batch start
#iterations: 155
currently lose_sum: 93.37629961967468
time_elpased: 3.154
batch start
#iterations: 156
currently lose_sum: 93.16102290153503
time_elpased: 3.092
batch start
#iterations: 157
currently lose_sum: 92.78468465805054
time_elpased: 3.336
batch start
#iterations: 158
currently lose_sum: 93.08893948793411
time_elpased: 3.065
batch start
#iterations: 159
currently lose_sum: 92.69409853219986
time_elpased: 3.079
start validation test
0.579639175258
0.613197969543
0.435158501441
0.509060261273
0.579877887647
70.307
batch start
#iterations: 160
currently lose_sum: 92.78381782770157
time_elpased: 3.121
batch start
#iterations: 161
currently lose_sum: 92.97549968957901
time_elpased: 3.146
batch start
#iterations: 162
currently lose_sum: 92.91642743349075
time_elpased: 3.08
batch start
#iterations: 163
currently lose_sum: 92.98082113265991
time_elpased: 3.146
batch start
#iterations: 164
currently lose_sum: 92.73224377632141
time_elpased: 3.074
batch start
#iterations: 165
currently lose_sum: 92.58163458108902
time_elpased: 3.123
batch start
#iterations: 166
currently lose_sum: 92.1470530629158
time_elpased: 3.097
batch start
#iterations: 167
currently lose_sum: 92.50179141759872
time_elpased: 3.151
batch start
#iterations: 168
currently lose_sum: 92.20788145065308
time_elpased: 3.161
batch start
#iterations: 169
currently lose_sum: 92.59987306594849
time_elpased: 3.19
batch start
#iterations: 170
currently lose_sum: 92.01570439338684
time_elpased: 3.168
batch start
#iterations: 171
currently lose_sum: 91.97558254003525
time_elpased: 3.27
batch start
#iterations: 172
currently lose_sum: 91.83839619159698
time_elpased: 3.161
batch start
#iterations: 173
currently lose_sum: 92.27922999858856
time_elpased: 3.044
batch start
#iterations: 174
currently lose_sum: 92.30337929725647
time_elpased: 3.06
batch start
#iterations: 175
currently lose_sum: 91.58042871952057
time_elpased: 3.061
batch start
#iterations: 176
currently lose_sum: 91.75366532802582
time_elpased: 3.086
batch start
#iterations: 177
currently lose_sum: 91.66916984319687
time_elpased: 3.245
batch start
#iterations: 178
currently lose_sum: 91.83870673179626
time_elpased: 3.123
batch start
#iterations: 179
currently lose_sum: 91.67578512430191
time_elpased: 3.112
start validation test
0.57618556701
0.617158092848
0.40500205846
0.489062888392
0.576468398086
71.793
batch start
#iterations: 180
currently lose_sum: 91.06482267379761
time_elpased: 3.089
batch start
#iterations: 181
currently lose_sum: 90.89786338806152
time_elpased: 3.073
batch start
#iterations: 182
currently lose_sum: 91.61917972564697
time_elpased: 3.108
batch start
#iterations: 183
currently lose_sum: 90.90104097127914
time_elpased: 3.058
batch start
#iterations: 184
currently lose_sum: 91.21365654468536
time_elpased: 3.086
batch start
#iterations: 185
currently lose_sum: 90.84333527088165
time_elpased: 3.096
batch start
#iterations: 186
currently lose_sum: 90.85557734966278
time_elpased: 3.034
batch start
#iterations: 187
currently lose_sum: 90.84434819221497
time_elpased: 3.078
batch start
#iterations: 188
currently lose_sum: 90.99858063459396
time_elpased: 3.059
batch start
#iterations: 189
currently lose_sum: 90.60517019033432
time_elpased: 3.042
batch start
#iterations: 190
currently lose_sum: 91.02730149030685
time_elpased: 3.134
batch start
#iterations: 191
currently lose_sum: 90.55212318897247
time_elpased: 3.134
batch start
#iterations: 192
currently lose_sum: 90.10479921102524
time_elpased: 3.094
batch start
#iterations: 193
currently lose_sum: 90.07354527711868
time_elpased: 3.108
batch start
#iterations: 194
currently lose_sum: 90.20540302991867
time_elpased: 3.133
batch start
#iterations: 195
currently lose_sum: 90.04909771680832
time_elpased: 3.151
batch start
#iterations: 196
currently lose_sum: 90.05459171533585
time_elpased: 3.086
batch start
#iterations: 197
currently lose_sum: 90.39774179458618
time_elpased: 3.109
batch start
#iterations: 198
currently lose_sum: 89.6415627002716
time_elpased: 3.101
batch start
#iterations: 199
currently lose_sum: 89.66390585899353
time_elpased: 3.157
start validation test
0.570824742268
0.628134218289
0.350658707287
0.450066050198
0.571188502755
75.101
batch start
#iterations: 200
currently lose_sum: 90.00623202323914
time_elpased: 3.164
batch start
#iterations: 201
currently lose_sum: 89.41289240121841
time_elpased: 3.141
batch start
#iterations: 202
currently lose_sum: 89.82379418611526
time_elpased: 3.155
batch start
#iterations: 203
currently lose_sum: 90.07759839296341
time_elpased: 3.198
batch start
#iterations: 204
currently lose_sum: 89.3651932477951
time_elpased: 3.105
batch start
#iterations: 205
currently lose_sum: 88.88656747341156
time_elpased: 3.085
batch start
#iterations: 206
currently lose_sum: 89.0598641037941
time_elpased: 3.127
batch start
#iterations: 207
currently lose_sum: 89.19415825605392
time_elpased: 3.207
batch start
#iterations: 208
currently lose_sum: 88.60733902454376
time_elpased: 3.107
batch start
#iterations: 209
currently lose_sum: 89.00989151000977
time_elpased: 3.233
batch start
#iterations: 210
currently lose_sum: 88.65599882602692
time_elpased: 3.201
batch start
#iterations: 211
currently lose_sum: 88.64147418737411
time_elpased: 3.085
batch start
#iterations: 212
currently lose_sum: 88.5974953174591
time_elpased: 3.141
batch start
#iterations: 213
currently lose_sum: 88.07150936126709
time_elpased: 3.164
batch start
#iterations: 214
currently lose_sum: 88.83580446243286
time_elpased: 3.129
batch start
#iterations: 215
currently lose_sum: 88.76580357551575
time_elpased: 3.147
batch start
#iterations: 216
currently lose_sum: 88.59461295604706
time_elpased: 3.168
batch start
#iterations: 217
currently lose_sum: 88.36597269773483
time_elpased: 3.208
batch start
#iterations: 218
currently lose_sum: 88.69972622394562
time_elpased: 3.264
batch start
#iterations: 219
currently lose_sum: 88.02508753538132
time_elpased: 3.217
start validation test
0.574639175258
0.618985695709
0.391930835735
0.479959667255
0.574941047772
77.063
batch start
#iterations: 220
currently lose_sum: 88.32067143917084
time_elpased: 3.214
batch start
#iterations: 221
currently lose_sum: 87.98066002130508
time_elpased: 3.1
batch start
#iterations: 222
currently lose_sum: 87.64628154039383
time_elpased: 3.044
batch start
#iterations: 223
currently lose_sum: 87.22014486789703
time_elpased: 3.17
batch start
#iterations: 224
currently lose_sum: 87.49947500228882
time_elpased: 3.188
batch start
#iterations: 225
currently lose_sum: 87.67824351787567
time_elpased: 3.19
batch start
#iterations: 226
currently lose_sum: 87.54900586605072
time_elpased: 3.229
batch start
#iterations: 227
currently lose_sum: 87.58561021089554
time_elpased: 2.998
batch start
#iterations: 228
currently lose_sum: 87.94066220521927
time_elpased: 3.108
batch start
#iterations: 229
currently lose_sum: 88.05564367771149
time_elpased: 3.101
batch start
#iterations: 230
currently lose_sum: 86.81259167194366
time_elpased: 3.025
batch start
#iterations: 231
currently lose_sum: 88.26316094398499
time_elpased: 3.158
batch start
#iterations: 232
currently lose_sum: 88.07713162899017
time_elpased: 3.15
batch start
#iterations: 233
currently lose_sum: 87.71219629049301
time_elpased: 3.079
batch start
#iterations: 234
currently lose_sum: 86.63238096237183
time_elpased: 3.124
batch start
#iterations: 235
currently lose_sum: 87.23758804798126
time_elpased: 3.112
batch start
#iterations: 236
currently lose_sum: 86.84489756822586
time_elpased: 3.067
batch start
#iterations: 237
currently lose_sum: 87.02612048387527
time_elpased: 3.085
batch start
#iterations: 238
currently lose_sum: 86.32045072317123
time_elpased: 3.122
batch start
#iterations: 239
currently lose_sum: 87.32683271169662
time_elpased: 3.19
start validation test
0.572371134021
0.619972963839
0.377624536846
0.469361647691
0.572692896263
79.066
batch start
#iterations: 240
currently lose_sum: 87.12747091054916
time_elpased: 3.149
batch start
#iterations: 241
currently lose_sum: 86.71392178535461
time_elpased: 3.129
batch start
#iterations: 242
currently lose_sum: 86.6000582575798
time_elpased: 3.194
batch start
#iterations: 243
currently lose_sum: 86.89406991004944
time_elpased: 3.111
batch start
#iterations: 244
currently lose_sum: 86.37140208482742
time_elpased: 3.147
batch start
#iterations: 245
currently lose_sum: 85.69367927312851
time_elpased: 3.115
batch start
#iterations: 246
currently lose_sum: 85.93760192394257
time_elpased: 3.151
batch start
#iterations: 247
currently lose_sum: 86.42361861467361
time_elpased: 3.142
batch start
#iterations: 248
currently lose_sum: 86.30508440732956
time_elpased: 3.075
batch start
#iterations: 249
currently lose_sum: 85.8794327378273
time_elpased: 3.119
batch start
#iterations: 250
currently lose_sum: 86.57542473077774
time_elpased: 3.299
batch start
#iterations: 251
currently lose_sum: 86.2975664138794
time_elpased: 3.156
batch start
#iterations: 252
currently lose_sum: 85.94042629003525
time_elpased: 3.169
batch start
#iterations: 253
currently lose_sum: 85.86932229995728
time_elpased: 3.087
batch start
#iterations: 254
currently lose_sum: 85.76343178749084
time_elpased: 3.156
batch start
#iterations: 255
currently lose_sum: 86.23268109560013
time_elpased: 3.11
batch start
#iterations: 256
currently lose_sum: 85.95708668231964
time_elpased: 3.141
batch start
#iterations: 257
currently lose_sum: 86.03597432374954
time_elpased: 3.069
batch start
#iterations: 258
currently lose_sum: 85.50355279445648
time_elpased: 3.157
batch start
#iterations: 259
currently lose_sum: 85.39219063520432
time_elpased: 3.21
start validation test
0.563969072165
0.622634146341
0.328427336352
0.430024930935
0.564358236536
87.019
batch start
#iterations: 260
currently lose_sum: 85.45636069774628
time_elpased: 3.189
batch start
#iterations: 261
currently lose_sum: 85.10157692432404
time_elpased: 3.221
batch start
#iterations: 262
currently lose_sum: 85.44809454679489
time_elpased: 3.165
batch start
#iterations: 263
currently lose_sum: 85.4103821516037
time_elpased: 3.196
batch start
#iterations: 264
currently lose_sum: 85.39870005846024
time_elpased: 3.1
batch start
#iterations: 265
currently lose_sum: 85.08617931604385
time_elpased: 3.091
batch start
#iterations: 266
currently lose_sum: 84.2519006729126
time_elpased: 3.068
batch start
#iterations: 267
currently lose_sum: 84.59194320440292
time_elpased: 3.165
batch start
#iterations: 268
currently lose_sum: 85.43207132816315
time_elpased: 3.123
batch start
#iterations: 269
currently lose_sum: 84.39572781324387
time_elpased: 3.169
batch start
#iterations: 270
currently lose_sum: 84.59777927398682
time_elpased: 3.094
batch start
#iterations: 271
currently lose_sum: 85.03222131729126
time_elpased: 3.184
batch start
#iterations: 272
currently lose_sum: 84.58155328035355
time_elpased: 3.095
batch start
#iterations: 273
currently lose_sum: 84.10644072294235
time_elpased: 3.111
batch start
#iterations: 274
currently lose_sum: 85.0429093837738
time_elpased: 3.191
batch start
#iterations: 275
currently lose_sum: 83.48639953136444
time_elpased: 3.181
batch start
#iterations: 276
currently lose_sum: 84.86766445636749
time_elpased: 3.249
batch start
#iterations: 277
currently lose_sum: 84.85001701116562
time_elpased: 3.113
batch start
#iterations: 278
currently lose_sum: 84.31089377403259
time_elpased: 3.092
batch start
#iterations: 279
currently lose_sum: 84.07129389047623
time_elpased: 3.096
start validation test
0.565463917526
0.604586857515
0.382564841499
0.46860816944
0.565766105177
96.941
batch start
#iterations: 280
currently lose_sum: 84.7276970744133
time_elpased: 3.031
batch start
#iterations: 281
currently lose_sum: 84.33417975902557
time_elpased: 3.051
batch start
#iterations: 282
currently lose_sum: 83.93581736087799
time_elpased: 3.103
batch start
#iterations: 283
currently lose_sum: 84.33405262231827
time_elpased: 3.106
batch start
#iterations: 284
currently lose_sum: 83.64859247207642
time_elpased: 3.205
batch start
#iterations: 285
currently lose_sum: 84.09815609455109
time_elpased: 3.123
batch start
#iterations: 286
currently lose_sum: 84.41977828741074
time_elpased: 3.094
batch start
#iterations: 287
currently lose_sum: 84.17365109920502
time_elpased: 3.136
batch start
#iterations: 288
currently lose_sum: 84.25400984287262
time_elpased: 3.116
batch start
#iterations: 289
currently lose_sum: 83.37304186820984
time_elpased: 3.124
batch start
#iterations: 290
currently lose_sum: 84.00128948688507
time_elpased: 3.131
batch start
#iterations: 291
currently lose_sum: 84.26674389839172
time_elpased: 3.158
batch start
#iterations: 292
currently lose_sum: 83.04363784193993
time_elpased: 3.173
batch start
#iterations: 293
currently lose_sum: 83.22483056783676
time_elpased: 3.153
batch start
#iterations: 294
currently lose_sum: 82.95848745107651
time_elpased: 3.147
batch start
#iterations: 295
currently lose_sum: 83.19060850143433
time_elpased: 3.114
batch start
#iterations: 296
currently lose_sum: 83.92456740140915
time_elpased: 3.154
batch start
#iterations: 297
currently lose_sum: 84.26731783151627
time_elpased: 3.178
batch start
#iterations: 298
currently lose_sum: 83.62657302618027
time_elpased: 3.154
batch start
#iterations: 299
currently lose_sum: 83.93098312616348
time_elpased: 3.087
start validation test
0.536804123711
0.532409873912
0.617126389461
0.571646486796
0.536671414474
91.611
batch start
#iterations: 300
currently lose_sum: 83.36452713608742
time_elpased: 3.16
batch start
#iterations: 301
currently lose_sum: 82.87999612092972
time_elpased: 3.131
batch start
#iterations: 302
currently lose_sum: 83.47773003578186
time_elpased: 3.113
batch start
#iterations: 303
currently lose_sum: 83.49064034223557
time_elpased: 3.164
batch start
#iterations: 304
currently lose_sum: 82.5101358294487
time_elpased: 3.14
batch start
#iterations: 305
currently lose_sum: 82.98933535814285
time_elpased: 3.181
batch start
#iterations: 306
currently lose_sum: 82.92155736684799
time_elpased: 3.236
batch start
#iterations: 307
currently lose_sum: 82.75721597671509
time_elpased: 3.114
batch start
#iterations: 308
currently lose_sum: 84.1604031920433
time_elpased: 3.195
batch start
#iterations: 309
currently lose_sum: 83.29022690653801
time_elpased: 3.17
batch start
#iterations: 310
currently lose_sum: 82.0705019235611
time_elpased: 3.134
batch start
#iterations: 311
currently lose_sum: 82.46731269359589
time_elpased: 3.088
batch start
#iterations: 312
currently lose_sum: 82.37261652946472
time_elpased: 3.191
batch start
#iterations: 313
currently lose_sum: 81.84522819519043
time_elpased: 3.242
batch start
#iterations: 314
currently lose_sum: 82.12833192944527
time_elpased: 3.23
batch start
#iterations: 315
currently lose_sum: 82.4127345085144
time_elpased: 3.15
batch start
#iterations: 316
currently lose_sum: 82.04481774568558
time_elpased: 3.168
batch start
#iterations: 317
currently lose_sum: 82.5879693031311
time_elpased: 3.151
batch start
#iterations: 318
currently lose_sum: 82.79660856723785
time_elpased: 3.015
batch start
#iterations: 319
currently lose_sum: 82.69212383031845
time_elpased: 3.076
start validation test
0.568505154639
0.617837743122
0.362906545904
0.457239188225
0.568844846682
94.594
batch start
#iterations: 320
currently lose_sum: 82.23847633600235
time_elpased: 3.123
batch start
#iterations: 321
currently lose_sum: 82.0933775305748
time_elpased: 3.206
batch start
#iterations: 322
currently lose_sum: 82.06544774770737
time_elpased: 3.118
batch start
#iterations: 323
currently lose_sum: 81.50219875574112
time_elpased: 3.198
batch start
#iterations: 324
currently lose_sum: 82.45471167564392
time_elpased: 3.118
batch start
#iterations: 325
currently lose_sum: 81.71792787313461
time_elpased: 3.16
batch start
#iterations: 326
currently lose_sum: 82.04112255573273
time_elpased: 3.075
batch start
#iterations: 327
currently lose_sum: 82.50810784101486
time_elpased: 3.178
batch start
#iterations: 328
currently lose_sum: 82.50470328330994
time_elpased: 3.036
batch start
#iterations: 329
currently lose_sum: 82.67232882976532
time_elpased: 2.993
batch start
#iterations: 330
currently lose_sum: 82.32212859392166
time_elpased: 3.091
batch start
#iterations: 331
currently lose_sum: 81.29227042198181
time_elpased: 3.038
batch start
#iterations: 332
currently lose_sum: 81.68032962083817
time_elpased: 3.155
batch start
#iterations: 333
currently lose_sum: 82.24345993995667
time_elpased: 3.127
batch start
#iterations: 334
currently lose_sum: 81.66367471218109
time_elpased: 3.089
batch start
#iterations: 335
currently lose_sum: 81.13599929213524
time_elpased: 3.099
batch start
#iterations: 336
currently lose_sum: 80.8346893787384
time_elpased: 3.083
batch start
#iterations: 337
currently lose_sum: 82.22325587272644
time_elpased: 3.117
batch start
#iterations: 338
currently lose_sum: 80.9889768064022
time_elpased: 3.1
batch start
#iterations: 339
currently lose_sum: 81.34278246760368
time_elpased: 3.152
start validation test
0.56175257732
0.63218641115
0.29878550844
0.405786972323
0.562187054096
101.222
batch start
#iterations: 340
currently lose_sum: 81.42759618163109
time_elpased: 3.13
batch start
#iterations: 341
currently lose_sum: 81.90252369642258
time_elpased: 3.158
batch start
#iterations: 342
currently lose_sum: 81.65804809331894
time_elpased: 3.127
batch start
#iterations: 343
currently lose_sum: 80.89585784077644
time_elpased: 3.225
batch start
#iterations: 344
currently lose_sum: 80.64644259214401
time_elpased: 3.012
batch start
#iterations: 345
currently lose_sum: 82.03687554597855
time_elpased: 3.167
batch start
#iterations: 346
currently lose_sum: 81.6479280591011
time_elpased: 3.108
batch start
#iterations: 347
currently lose_sum: 80.71501162648201
time_elpased: 3.19
batch start
#iterations: 348
currently lose_sum: 80.68819600343704
time_elpased: 3.135
batch start
#iterations: 349
currently lose_sum: 81.13125115633011
time_elpased: 3.081
batch start
#iterations: 350
currently lose_sum: 80.61254367232323
time_elpased: 3.072
batch start
#iterations: 351
currently lose_sum: 81.20439538359642
time_elpased: 3.134
batch start
#iterations: 352
currently lose_sum: 80.9950761795044
time_elpased: 3.166
batch start
#iterations: 353
currently lose_sum: 80.42387348413467
time_elpased: 3.111
batch start
#iterations: 354
currently lose_sum: 80.39721354842186
time_elpased: 3.195
batch start
#iterations: 355
currently lose_sum: 81.25104674696922
time_elpased: 3.128
batch start
#iterations: 356
currently lose_sum: 80.67808127403259
time_elpased: 3.063
batch start
#iterations: 357
currently lose_sum: 81.1716343164444
time_elpased: 3.162
batch start
#iterations: 358
currently lose_sum: 80.41991424560547
time_elpased: 3.152
batch start
#iterations: 359
currently lose_sum: 79.59227806329727
time_elpased: 3.228
start validation test
0.561237113402
0.605985915493
0.354261012762
0.447129124448
0.56157908135
109.946
batch start
#iterations: 360
currently lose_sum: 80.20530924201012
time_elpased: 3.129
batch start
#iterations: 361
currently lose_sum: 80.20437729358673
time_elpased: 3.127
batch start
#iterations: 362
currently lose_sum: 81.05977246165276
time_elpased: 3.201
batch start
#iterations: 363
currently lose_sum: 80.43603378534317
time_elpased: 3.16
batch start
#iterations: 364
currently lose_sum: 79.73241648077965
time_elpased: 3.229
batch start
#iterations: 365
currently lose_sum: 80.43452382087708
time_elpased: 3.149
batch start
#iterations: 366
currently lose_sum: 80.57980588078499
time_elpased: 3.196
batch start
#iterations: 367
currently lose_sum: 80.21049121022224
time_elpased: 3.099
batch start
#iterations: 368
currently lose_sum: 80.03451156616211
time_elpased: 3.067
batch start
#iterations: 369
currently lose_sum: 80.19345653057098
time_elpased: 3.089
batch start
#iterations: 370
currently lose_sum: 79.91516274213791
time_elpased: 2.812
batch start
#iterations: 371
currently lose_sum: 80.35427114367485
time_elpased: 3.124
batch start
#iterations: 372
currently lose_sum: 80.15884700417519
time_elpased: 3.097
batch start
#iterations: 373
currently lose_sum: 79.41720297932625
time_elpased: 3.145
batch start
#iterations: 374
currently lose_sum: 79.52433529496193
time_elpased: 3.143
batch start
#iterations: 375
currently lose_sum: 80.0075010061264
time_elpased: 3.092
batch start
#iterations: 376
currently lose_sum: 79.72354847192764
time_elpased: 3.069
batch start
#iterations: 377
currently lose_sum: 79.49104729294777
time_elpased: 3.11
batch start
#iterations: 378
currently lose_sum: 79.68462407588959
time_elpased: 3.097
batch start
#iterations: 379
currently lose_sum: 80.20583128929138
time_elpased: 3.155
start validation test
0.560051546392
0.619898477157
0.314223960478
0.417048015846
0.560457705146
114.521
batch start
#iterations: 380
currently lose_sum: 79.96925246715546
time_elpased: 3.138
batch start
#iterations: 381
currently lose_sum: 79.58298581838608
time_elpased: 3.145
batch start
#iterations: 382
currently lose_sum: 80.700669080019
time_elpased: 3.112
batch start
#iterations: 383
currently lose_sum: 79.60007756948471
time_elpased: 3.127
batch start
#iterations: 384
currently lose_sum: 79.0893365740776
time_elpased: 3.112
batch start
#iterations: 385
currently lose_sum: 79.25189280509949
time_elpased: 3.07
batch start
#iterations: 386
currently lose_sum: 79.91227999329567
time_elpased: 3.236
batch start
#iterations: 387
currently lose_sum: 79.78462582826614
time_elpased: 3.171
batch start
#iterations: 388
currently lose_sum: 79.05444440245628
time_elpased: 3.089
batch start
#iterations: 389
currently lose_sum: 79.7932967543602
time_elpased: 3.116
batch start
#iterations: 390
currently lose_sum: 78.80907040834427
time_elpased: 2.979
batch start
#iterations: 391
currently lose_sum: 80.10420018434525
time_elpased: 2.925
batch start
#iterations: 392
currently lose_sum: 79.14402928948402
time_elpased: 2.612
batch start
#iterations: 393
currently lose_sum: 80.21044772863388
time_elpased: 2.581
batch start
#iterations: 394
currently lose_sum: 79.34573265910149
time_elpased: 2.52
batch start
#iterations: 395
currently lose_sum: 78.6260698735714
time_elpased: 2.568
batch start
#iterations: 396
currently lose_sum: 79.62181869149208
time_elpased: 2.545
batch start
#iterations: 397
currently lose_sum: 78.70592939853668
time_elpased: 2.524
batch start
#iterations: 398
currently lose_sum: 78.82168304920197
time_elpased: 2.528
batch start
#iterations: 399
currently lose_sum: 79.0499418079853
time_elpased: 2.466
start validation test
0.55087628866
0.634126771864
0.244030465212
0.352434039391
0.551383262346
122.816
acc: 0.587
pre: 0.627
rec: 0.433
F1: 0.512
auc: 0.587
