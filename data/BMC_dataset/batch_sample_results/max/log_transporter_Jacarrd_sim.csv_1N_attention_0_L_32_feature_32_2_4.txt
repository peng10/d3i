start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.37938749790192
time_elpased: 2.302
batch start
#iterations: 1
currently lose_sum: 100.28466260433197
time_elpased: 2.341
batch start
#iterations: 2
currently lose_sum: 100.17162024974823
time_elpased: 2.289
batch start
#iterations: 3
currently lose_sum: 100.0535404086113
time_elpased: 2.096
batch start
#iterations: 4
currently lose_sum: 100.07397848367691
time_elpased: 1.926
batch start
#iterations: 5
currently lose_sum: 99.9443090558052
time_elpased: 2.289
batch start
#iterations: 6
currently lose_sum: 99.78648769855499
time_elpased: 2.295
batch start
#iterations: 7
currently lose_sum: 99.84981161355972
time_elpased: 2.352
batch start
#iterations: 8
currently lose_sum: 99.8366162776947
time_elpased: 2.226
batch start
#iterations: 9
currently lose_sum: 99.89991271495819
time_elpased: 2.263
batch start
#iterations: 10
currently lose_sum: 99.54214322566986
time_elpased: 2.149
batch start
#iterations: 11
currently lose_sum: 99.61157977581024
time_elpased: 2.285
batch start
#iterations: 12
currently lose_sum: 99.56085526943207
time_elpased: 1.923
batch start
#iterations: 13
currently lose_sum: 99.714928150177
time_elpased: 2.215
batch start
#iterations: 14
currently lose_sum: 99.44756132364273
time_elpased: 2.212
batch start
#iterations: 15
currently lose_sum: 99.6091166138649
time_elpased: 2.376
batch start
#iterations: 16
currently lose_sum: 99.55187159776688
time_elpased: 2.135
batch start
#iterations: 17
currently lose_sum: 99.58810830116272
time_elpased: 2.408
batch start
#iterations: 18
currently lose_sum: 99.3339769244194
time_elpased: 2.256
batch start
#iterations: 19
currently lose_sum: 99.4320017695427
time_elpased: 2.289
start validation test
0.574278350515
0.589185221434
0.495626222085
0.538371248114
0.574416436458
65.782
batch start
#iterations: 20
currently lose_sum: 99.34005326032639
time_elpased: 2.371
batch start
#iterations: 21
currently lose_sum: 99.26959931850433
time_elpased: 2.381
batch start
#iterations: 22
currently lose_sum: 99.16550576686859
time_elpased: 2.149
batch start
#iterations: 23
currently lose_sum: 99.40120857954025
time_elpased: 2.212
batch start
#iterations: 24
currently lose_sum: 99.20770001411438
time_elpased: 2.298
batch start
#iterations: 25
currently lose_sum: 99.07024270296097
time_elpased: 2.298
batch start
#iterations: 26
currently lose_sum: 99.27577894926071
time_elpased: 2.136
batch start
#iterations: 27
currently lose_sum: 98.97932320833206
time_elpased: 2.109
batch start
#iterations: 28
currently lose_sum: 99.1326574087143
time_elpased: 1.923
batch start
#iterations: 29
currently lose_sum: 99.2089518904686
time_elpased: 1.479
batch start
#iterations: 30
currently lose_sum: 99.20832538604736
time_elpased: 1.696
batch start
#iterations: 31
currently lose_sum: 98.93822854757309
time_elpased: 2.38
batch start
#iterations: 32
currently lose_sum: 99.19958871603012
time_elpased: 2.01
batch start
#iterations: 33
currently lose_sum: 98.98757266998291
time_elpased: 2.198
batch start
#iterations: 34
currently lose_sum: 98.8366140127182
time_elpased: 2.37
batch start
#iterations: 35
currently lose_sum: 98.89490246772766
time_elpased: 2.155
batch start
#iterations: 36
currently lose_sum: 99.05632650852203
time_elpased: 2.105
batch start
#iterations: 37
currently lose_sum: 98.96679109334946
time_elpased: 1.528
batch start
#iterations: 38
currently lose_sum: 98.85572797060013
time_elpased: 1.479
batch start
#iterations: 39
currently lose_sum: 98.84330350160599
time_elpased: 1.991
start validation test
0.587113402062
0.621149751597
0.450344756612
0.522133396969
0.587353520514
65.492
batch start
#iterations: 40
currently lose_sum: 98.9796969294548
time_elpased: 1.606
batch start
#iterations: 41
currently lose_sum: 98.80946934223175
time_elpased: 1.452
batch start
#iterations: 42
currently lose_sum: 98.76601737737656
time_elpased: 1.852
batch start
#iterations: 43
currently lose_sum: 98.95479840040207
time_elpased: 2.428
batch start
#iterations: 44
currently lose_sum: 98.71442592144012
time_elpased: 2.43
batch start
#iterations: 45
currently lose_sum: 98.79498052597046
time_elpased: 2.202
batch start
#iterations: 46
currently lose_sum: 98.80268806219101
time_elpased: 2.236
batch start
#iterations: 47
currently lose_sum: 98.72117775678635
time_elpased: 2.267
batch start
#iterations: 48
currently lose_sum: 98.85269063711166
time_elpased: 2.421
batch start
#iterations: 49
currently lose_sum: 98.58898365497589
time_elpased: 2.404
batch start
#iterations: 50
currently lose_sum: 98.58261662721634
time_elpased: 2.461
batch start
#iterations: 51
currently lose_sum: 98.6557726264
time_elpased: 2.456
batch start
#iterations: 52
currently lose_sum: 98.73979216814041
time_elpased: 2.459
batch start
#iterations: 53
currently lose_sum: 98.88202607631683
time_elpased: 2.385
batch start
#iterations: 54
currently lose_sum: 98.7811678647995
time_elpased: 2.288
batch start
#iterations: 55
currently lose_sum: 98.68885612487793
time_elpased: 2.098
batch start
#iterations: 56
currently lose_sum: 98.62570309638977
time_elpased: 2.41
batch start
#iterations: 57
currently lose_sum: 98.54132217168808
time_elpased: 2.182
batch start
#iterations: 58
currently lose_sum: 98.7645355463028
time_elpased: 2.281
batch start
#iterations: 59
currently lose_sum: 98.66067498922348
time_elpased: 2.349
start validation test
0.56912371134
0.635420821699
0.327878974992
0.432557192316
0.569547253684
66.014
batch start
#iterations: 60
currently lose_sum: 98.57639414072037
time_elpased: 2.257
batch start
#iterations: 61
currently lose_sum: 98.71197992563248
time_elpased: 2.407
batch start
#iterations: 62
currently lose_sum: 98.59056675434113
time_elpased: 2.082
batch start
#iterations: 63
currently lose_sum: 98.61958110332489
time_elpased: 2.156
batch start
#iterations: 64
currently lose_sum: 98.65761476755142
time_elpased: 2.157
batch start
#iterations: 65
currently lose_sum: 98.38362085819244
time_elpased: 2.268
batch start
#iterations: 66
currently lose_sum: 98.61092376708984
time_elpased: 2.001
batch start
#iterations: 67
currently lose_sum: 98.2631813287735
time_elpased: 2.037
batch start
#iterations: 68
currently lose_sum: 98.36842566728592
time_elpased: 2.219
batch start
#iterations: 69
currently lose_sum: 98.58143693208694
time_elpased: 2.357
batch start
#iterations: 70
currently lose_sum: 98.35930389165878
time_elpased: 2.127
batch start
#iterations: 71
currently lose_sum: 98.44814389944077
time_elpased: 2.222
batch start
#iterations: 72
currently lose_sum: 98.42138206958771
time_elpased: 2.291
batch start
#iterations: 73
currently lose_sum: 98.33078843355179
time_elpased: 2.152
batch start
#iterations: 74
currently lose_sum: 98.24146682024002
time_elpased: 2.196
batch start
#iterations: 75
currently lose_sum: 98.41154825687408
time_elpased: 2.341
batch start
#iterations: 76
currently lose_sum: 98.16072070598602
time_elpased: 2.369
batch start
#iterations: 77
currently lose_sum: 98.24652248620987
time_elpased: 2.073
batch start
#iterations: 78
currently lose_sum: 98.31671869754791
time_elpased: 2.363
batch start
#iterations: 79
currently lose_sum: 98.44184994697571
time_elpased: 2.287
start validation test
0.590463917526
0.622850804215
0.46228259751
0.530686986827
0.590688959604
65.422
batch start
#iterations: 80
currently lose_sum: 98.17948353290558
time_elpased: 2.214
batch start
#iterations: 81
currently lose_sum: 98.32701861858368
time_elpased: 2.109
batch start
#iterations: 82
currently lose_sum: 98.28985118865967
time_elpased: 2.301
batch start
#iterations: 83
currently lose_sum: 98.01799368858337
time_elpased: 2.365
batch start
#iterations: 84
currently lose_sum: 98.31857913732529
time_elpased: 2.249
batch start
#iterations: 85
currently lose_sum: 98.34207028150558
time_elpased: 2.343
batch start
#iterations: 86
currently lose_sum: 98.12472343444824
time_elpased: 2.442
batch start
#iterations: 87
currently lose_sum: 98.27292776107788
time_elpased: 2.16
batch start
#iterations: 88
currently lose_sum: 98.14013105630875
time_elpased: 2.423
batch start
#iterations: 89
currently lose_sum: 98.26429522037506
time_elpased: 2.282
batch start
#iterations: 90
currently lose_sum: 97.89214789867401
time_elpased: 2.322
batch start
#iterations: 91
currently lose_sum: 98.17853337526321
time_elpased: 2.222
batch start
#iterations: 92
currently lose_sum: 97.96402531862259
time_elpased: 2.275
batch start
#iterations: 93
currently lose_sum: 98.08839625120163
time_elpased: 2.326
batch start
#iterations: 94
currently lose_sum: 98.02277159690857
time_elpased: 2.305
batch start
#iterations: 95
currently lose_sum: 98.14102166891098
time_elpased: 2.584
batch start
#iterations: 96
currently lose_sum: 98.2322695851326
time_elpased: 1.895
batch start
#iterations: 97
currently lose_sum: 98.22358793020248
time_elpased: 2.329
batch start
#iterations: 98
currently lose_sum: 98.02818495035172
time_elpased: 2.072
batch start
#iterations: 99
currently lose_sum: 97.81023037433624
time_elpased: 2.576
start validation test
0.585309278351
0.590163934426
0.563136770608
0.57633366686
0.585348205608
65.497
batch start
#iterations: 100
currently lose_sum: 97.89879953861237
time_elpased: 2.394
batch start
#iterations: 101
currently lose_sum: 97.84669232368469
time_elpased: 2.262
batch start
#iterations: 102
currently lose_sum: 97.64228504896164
time_elpased: 2.406
batch start
#iterations: 103
currently lose_sum: 98.0738735795021
time_elpased: 2.155
batch start
#iterations: 104
currently lose_sum: 97.6380386352539
time_elpased: 2.323
batch start
#iterations: 105
currently lose_sum: 97.88991343975067
time_elpased: 2.315
batch start
#iterations: 106
currently lose_sum: 97.77825725078583
time_elpased: 2.348
batch start
#iterations: 107
currently lose_sum: 97.80317449569702
time_elpased: 2.256
batch start
#iterations: 108
currently lose_sum: 98.03208005428314
time_elpased: 2.161
batch start
#iterations: 109
currently lose_sum: 97.58435636758804
time_elpased: 2.185
batch start
#iterations: 110
currently lose_sum: 97.90123873949051
time_elpased: 2.083
batch start
#iterations: 111
currently lose_sum: 97.99628329277039
time_elpased: 2.124
batch start
#iterations: 112
currently lose_sum: 97.7770277261734
time_elpased: 2.335
batch start
#iterations: 113
currently lose_sum: 97.57607519626617
time_elpased: 2.179
batch start
#iterations: 114
currently lose_sum: 97.81740617752075
time_elpased: 2.342
batch start
#iterations: 115
currently lose_sum: 97.64003270864487
time_elpased: 2.116
batch start
#iterations: 116
currently lose_sum: 97.79452484846115
time_elpased: 2.19
batch start
#iterations: 117
currently lose_sum: 97.53233897686005
time_elpased: 2.335
batch start
#iterations: 118
currently lose_sum: 97.58598333597183
time_elpased: 2.064
batch start
#iterations: 119
currently lose_sum: 97.93537271022797
time_elpased: 2.106
start validation test
0.581443298969
0.611631483294
0.450241844191
0.51867219917
0.581673643359
65.787
batch start
#iterations: 120
currently lose_sum: 97.75822007656097
time_elpased: 2.284
batch start
#iterations: 121
currently lose_sum: 97.87226462364197
time_elpased: 2.245
batch start
#iterations: 122
currently lose_sum: 97.7668924331665
time_elpased: 2.494
batch start
#iterations: 123
currently lose_sum: 97.4291443824768
time_elpased: 2.175
batch start
#iterations: 124
currently lose_sum: 97.52075451612473
time_elpased: 2.203
batch start
#iterations: 125
currently lose_sum: 97.40658956766129
time_elpased: 2.026
batch start
#iterations: 126
currently lose_sum: 97.37732011079788
time_elpased: 2.193
batch start
#iterations: 127
currently lose_sum: 97.24999630451202
time_elpased: 2.315
batch start
#iterations: 128
currently lose_sum: 97.41377902030945
time_elpased: 2.191
batch start
#iterations: 129
currently lose_sum: 97.72625654935837
time_elpased: 2.117
batch start
#iterations: 130
currently lose_sum: 97.29610460996628
time_elpased: 2.025
batch start
#iterations: 131
currently lose_sum: 97.56020826101303
time_elpased: 1.976
batch start
#iterations: 132
currently lose_sum: 97.89811992645264
time_elpased: 2.306
batch start
#iterations: 133
currently lose_sum: 97.27854782342911
time_elpased: 2.344
batch start
#iterations: 134
currently lose_sum: 97.19008255004883
time_elpased: 2.223
batch start
#iterations: 135
currently lose_sum: 97.19198614358902
time_elpased: 2.248
batch start
#iterations: 136
currently lose_sum: 97.52935129404068
time_elpased: 2.313
batch start
#iterations: 137
currently lose_sum: 97.48534494638443
time_elpased: 2.381
batch start
#iterations: 138
currently lose_sum: 97.33389741182327
time_elpased: 2.339
batch start
#iterations: 139
currently lose_sum: 97.11330944299698
time_elpased: 2.383
start validation test
0.58175257732
0.620762392647
0.423999176701
0.503852268558
0.582029537746
65.868
batch start
#iterations: 140
currently lose_sum: 97.2537876367569
time_elpased: 2.233
batch start
#iterations: 141
currently lose_sum: 97.40601795911789
time_elpased: 2.418
batch start
#iterations: 142
currently lose_sum: 97.34673857688904
time_elpased: 2.376
batch start
#iterations: 143
currently lose_sum: 97.36350136995316
time_elpased: 2.101
batch start
#iterations: 144
currently lose_sum: 97.02702111005783
time_elpased: 2.571
batch start
#iterations: 145
currently lose_sum: 97.25884103775024
time_elpased: 2.608
batch start
#iterations: 146
currently lose_sum: 96.80784237384796
time_elpased: 2.429
batch start
#iterations: 147
currently lose_sum: 97.22638213634491
time_elpased: 2.241
batch start
#iterations: 148
currently lose_sum: 97.0651291012764
time_elpased: 1.964
batch start
#iterations: 149
currently lose_sum: 97.33101934194565
time_elpased: 2.487
batch start
#iterations: 150
currently lose_sum: 97.12301641702652
time_elpased: 2.486
batch start
#iterations: 151
currently lose_sum: 97.03159844875336
time_elpased: 2.297
batch start
#iterations: 152
currently lose_sum: 97.0408062338829
time_elpased: 2.145
batch start
#iterations: 153
currently lose_sum: 97.15420895814896
time_elpased: 2.248
batch start
#iterations: 154
currently lose_sum: 96.81527906656265
time_elpased: 2.204
batch start
#iterations: 155
currently lose_sum: 96.89035999774933
time_elpased: 2.308
batch start
#iterations: 156
currently lose_sum: 97.5672407746315
time_elpased: 2.174
batch start
#iterations: 157
currently lose_sum: 96.97829639911652
time_elpased: 2.133
batch start
#iterations: 158
currently lose_sum: 96.90736305713654
time_elpased: 2.382
batch start
#iterations: 159
currently lose_sum: 96.97931516170502
time_elpased: 2.424
start validation test
0.582113402062
0.61943620178
0.429659359885
0.507382876587
0.582381058647
65.998
batch start
#iterations: 160
currently lose_sum: 96.88619178533554
time_elpased: 2.403
batch start
#iterations: 161
currently lose_sum: 97.0565636754036
time_elpased: 2.358
batch start
#iterations: 162
currently lose_sum: 96.98211574554443
time_elpased: 2.183
batch start
#iterations: 163
currently lose_sum: 97.12643939256668
time_elpased: 2.259
batch start
#iterations: 164
currently lose_sum: 97.12022107839584
time_elpased: 2.216
batch start
#iterations: 165
currently lose_sum: 96.84357261657715
time_elpased: 1.947
batch start
#iterations: 166
currently lose_sum: 96.80181592702866
time_elpased: 2.122
batch start
#iterations: 167
currently lose_sum: 97.04992151260376
time_elpased: 1.977
batch start
#iterations: 168
currently lose_sum: 96.97434622049332
time_elpased: 2.275
batch start
#iterations: 169
currently lose_sum: 96.96235746145248
time_elpased: 2.101
batch start
#iterations: 170
currently lose_sum: 96.36210185289383
time_elpased: 2.132
batch start
#iterations: 171
currently lose_sum: 96.9591514468193
time_elpased: 2.274
batch start
#iterations: 172
currently lose_sum: 96.66609311103821
time_elpased: 2.246
batch start
#iterations: 173
currently lose_sum: 96.61124593019485
time_elpased: 2.129
batch start
#iterations: 174
currently lose_sum: 96.81319785118103
time_elpased: 2.293
batch start
#iterations: 175
currently lose_sum: 96.572804749012
time_elpased: 2.188
batch start
#iterations: 176
currently lose_sum: 96.69171035289764
time_elpased: 2.01
batch start
#iterations: 177
currently lose_sum: 96.72497737407684
time_elpased: 2.093
batch start
#iterations: 178
currently lose_sum: 96.83994567394257
time_elpased: 2.419
batch start
#iterations: 179
currently lose_sum: 96.49200546741486
time_elpased: 2.216
start validation test
0.585979381443
0.640159707203
0.396006998045
0.489318413021
0.586312907264
66.386
batch start
#iterations: 180
currently lose_sum: 96.58332979679108
time_elpased: 2.338
batch start
#iterations: 181
currently lose_sum: 96.60415858030319
time_elpased: 2.245
batch start
#iterations: 182
currently lose_sum: 96.61349827051163
time_elpased: 2.084
batch start
#iterations: 183
currently lose_sum: 96.64729499816895
time_elpased: 2.353
batch start
#iterations: 184
currently lose_sum: 96.59158080816269
time_elpased: 2.101
batch start
#iterations: 185
currently lose_sum: 96.56567668914795
time_elpased: 2.306
batch start
#iterations: 186
currently lose_sum: 96.46393084526062
time_elpased: 2.453
batch start
#iterations: 187
currently lose_sum: 96.66476052999496
time_elpased: 2.239
batch start
#iterations: 188
currently lose_sum: 96.43085211515427
time_elpased: 2.335
batch start
#iterations: 189
currently lose_sum: 96.73339664936066
time_elpased: 2.216
batch start
#iterations: 190
currently lose_sum: 96.36589473485947
time_elpased: 2.275
batch start
#iterations: 191
currently lose_sum: 96.59849816560745
time_elpased: 2.183
batch start
#iterations: 192
currently lose_sum: 96.39836114645004
time_elpased: 2.582
batch start
#iterations: 193
currently lose_sum: 96.6703029870987
time_elpased: 2.474
batch start
#iterations: 194
currently lose_sum: 96.58026081323624
time_elpased: 2.519
batch start
#iterations: 195
currently lose_sum: 96.60289704799652
time_elpased: 2.527
batch start
#iterations: 196
currently lose_sum: 96.60071700811386
time_elpased: 2.464
batch start
#iterations: 197
currently lose_sum: 96.2659040093422
time_elpased: 2.46
batch start
#iterations: 198
currently lose_sum: 96.4292151927948
time_elpased: 2.322
batch start
#iterations: 199
currently lose_sum: 96.53535860776901
time_elpased: 2.179
start validation test
0.575927835052
0.620550161812
0.394669136565
0.482480971252
0.576246062654
66.714
batch start
#iterations: 200
currently lose_sum: 96.0792036652565
time_elpased: 2.114
batch start
#iterations: 201
currently lose_sum: 96.49670469760895
time_elpased: 2.0
batch start
#iterations: 202
currently lose_sum: 96.61905497312546
time_elpased: 2.2
batch start
#iterations: 203
currently lose_sum: 96.68290305137634
time_elpased: 2.048
batch start
#iterations: 204
currently lose_sum: 96.12915307283401
time_elpased: 2.036
batch start
#iterations: 205
currently lose_sum: 96.16173279285431
time_elpased: 2.202
batch start
#iterations: 206
currently lose_sum: 96.07211971282959
time_elpased: 2.378
batch start
#iterations: 207
currently lose_sum: 96.28261739015579
time_elpased: 2.256
batch start
#iterations: 208
currently lose_sum: 96.3272066116333
time_elpased: 2.384
batch start
#iterations: 209
currently lose_sum: 96.32110947370529
time_elpased: 2.056
batch start
#iterations: 210
currently lose_sum: 96.37869203090668
time_elpased: 2.243
batch start
#iterations: 211
currently lose_sum: 96.12086975574493
time_elpased: 2.227
batch start
#iterations: 212
currently lose_sum: 96.03598636388779
time_elpased: 2.274
batch start
#iterations: 213
currently lose_sum: 96.00850927829742
time_elpased: 2.313
batch start
#iterations: 214
currently lose_sum: 96.15088278055191
time_elpased: 2.31
batch start
#iterations: 215
currently lose_sum: 96.10340291261673
time_elpased: 2.343
batch start
#iterations: 216
currently lose_sum: 96.09992444515228
time_elpased: 2.325
batch start
#iterations: 217
currently lose_sum: 96.37474912405014
time_elpased: 2.351
batch start
#iterations: 218
currently lose_sum: 95.96153843402863
time_elpased: 2.269
batch start
#iterations: 219
currently lose_sum: 95.91232055425644
time_elpased: 2.336
start validation test
0.580567010309
0.61337543054
0.439847689616
0.512316451903
0.580814064781
66.230
batch start
#iterations: 220
currently lose_sum: 95.95137447118759
time_elpased: 2.157
batch start
#iterations: 221
currently lose_sum: 95.76714783906937
time_elpased: 2.348
batch start
#iterations: 222
currently lose_sum: 96.37053662538528
time_elpased: 2.223
batch start
#iterations: 223
currently lose_sum: 96.16422015428543
time_elpased: 2.333
batch start
#iterations: 224
currently lose_sum: 96.39991277456284
time_elpased: 2.335
batch start
#iterations: 225
currently lose_sum: 95.88748729228973
time_elpased: 2.283
batch start
#iterations: 226
currently lose_sum: 95.83261126279831
time_elpased: 2.162
batch start
#iterations: 227
currently lose_sum: 96.08648705482483
time_elpased: 2.487
batch start
#iterations: 228
currently lose_sum: 96.00347661972046
time_elpased: 2.261
batch start
#iterations: 229
currently lose_sum: 96.52034437656403
time_elpased: 2.402
batch start
#iterations: 230
currently lose_sum: 96.07281631231308
time_elpased: 2.256
batch start
#iterations: 231
currently lose_sum: 95.83425796031952
time_elpased: 2.19
batch start
#iterations: 232
currently lose_sum: 95.90173918008804
time_elpased: 2.244
batch start
#iterations: 233
currently lose_sum: 95.53593510389328
time_elpased: 2.261
batch start
#iterations: 234
currently lose_sum: 95.77236711978912
time_elpased: 2.365
batch start
#iterations: 235
currently lose_sum: 95.86083126068115
time_elpased: 2.343
batch start
#iterations: 236
currently lose_sum: 95.69989198446274
time_elpased: 2.471
batch start
#iterations: 237
currently lose_sum: 95.92264181375504
time_elpased: 2.327
batch start
#iterations: 238
currently lose_sum: 95.87237751483917
time_elpased: 2.358
batch start
#iterations: 239
currently lose_sum: 95.53134608268738
time_elpased: 2.342
start validation test
0.583092783505
0.626259494652
0.415766182978
0.499752597724
0.583386551161
66.493
batch start
#iterations: 240
currently lose_sum: 95.93659669160843
time_elpased: 2.532
batch start
#iterations: 241
currently lose_sum: 95.95591795444489
time_elpased: 2.496
batch start
#iterations: 242
currently lose_sum: 95.66509771347046
time_elpased: 2.284
batch start
#iterations: 243
currently lose_sum: 95.60382497310638
time_elpased: 2.337
batch start
#iterations: 244
currently lose_sum: 95.75834053754807
time_elpased: 2.475
batch start
#iterations: 245
currently lose_sum: 95.76711064577103
time_elpased: 2.421
batch start
#iterations: 246
currently lose_sum: 95.64812058210373
time_elpased: 2.493
batch start
#iterations: 247
currently lose_sum: 95.76018220186234
time_elpased: 1.977
batch start
#iterations: 248
currently lose_sum: 95.79923617839813
time_elpased: 1.899
batch start
#iterations: 249
currently lose_sum: 95.67771524190903
time_elpased: 2.361
batch start
#iterations: 250
currently lose_sum: 95.59649622440338
time_elpased: 2.331
batch start
#iterations: 251
currently lose_sum: 95.91111689805984
time_elpased: 2.33
batch start
#iterations: 252
currently lose_sum: 95.74789303541183
time_elpased: 2.098
batch start
#iterations: 253
currently lose_sum: 95.56050896644592
time_elpased: 2.25
batch start
#iterations: 254
currently lose_sum: 95.55348205566406
time_elpased: 2.405
batch start
#iterations: 255
currently lose_sum: 95.4205801486969
time_elpased: 2.216
batch start
#iterations: 256
currently lose_sum: 95.39062875509262
time_elpased: 2.267
batch start
#iterations: 257
currently lose_sum: 95.75440281629562
time_elpased: 2.266
batch start
#iterations: 258
currently lose_sum: 95.54492551088333
time_elpased: 2.29
batch start
#iterations: 259
currently lose_sum: 95.5525860786438
time_elpased: 2.398
start validation test
0.586237113402
0.622641509434
0.441494288361
0.516649605588
0.586491231756
66.973
batch start
#iterations: 260
currently lose_sum: 95.68274998664856
time_elpased: 2.31
batch start
#iterations: 261
currently lose_sum: 95.91643911600113
time_elpased: 2.217
batch start
#iterations: 262
currently lose_sum: 95.8271718621254
time_elpased: 2.005
batch start
#iterations: 263
currently lose_sum: 95.58655250072479
time_elpased: 2.384
batch start
#iterations: 264
currently lose_sum: 95.60142004489899
time_elpased: 2.396
batch start
#iterations: 265
currently lose_sum: 95.44838500022888
time_elpased: 2.176
batch start
#iterations: 266
currently lose_sum: 95.30597811937332
time_elpased: 2.191
batch start
#iterations: 267
currently lose_sum: 95.64562225341797
time_elpased: 2.271
batch start
#iterations: 268
currently lose_sum: 95.30995780229568
time_elpased: 2.221
batch start
#iterations: 269
currently lose_sum: 95.5197536945343
time_elpased: 2.252
batch start
#iterations: 270
currently lose_sum: 95.42277562618256
time_elpased: 2.235
batch start
#iterations: 271
currently lose_sum: 95.38794481754303
time_elpased: 2.324
batch start
#iterations: 272
currently lose_sum: 95.75779908895493
time_elpased: 2.304
batch start
#iterations: 273
currently lose_sum: 95.59733629226685
time_elpased: 2.153
batch start
#iterations: 274
currently lose_sum: 95.44685101509094
time_elpased: 2.343
batch start
#iterations: 275
currently lose_sum: 95.56688445806503
time_elpased: 2.439
batch start
#iterations: 276
currently lose_sum: 95.45737999677658
time_elpased: 2.35
batch start
#iterations: 277
currently lose_sum: 95.61995512247086
time_elpased: 2.475
batch start
#iterations: 278
currently lose_sum: 95.4385672211647
time_elpased: 2.41
batch start
#iterations: 279
currently lose_sum: 95.13425499200821
time_elpased: 2.249
start validation test
0.584020618557
0.61499790532
0.453226304415
0.52186277995
0.584250248149
66.915
batch start
#iterations: 280
currently lose_sum: 95.48663425445557
time_elpased: 2.287
batch start
#iterations: 281
currently lose_sum: 95.35142290592194
time_elpased: 2.278
batch start
#iterations: 282
currently lose_sum: 95.47988718748093
time_elpased: 2.23
batch start
#iterations: 283
currently lose_sum: 95.30525785684586
time_elpased: 2.327
batch start
#iterations: 284
currently lose_sum: 95.35044133663177
time_elpased: 2.52
batch start
#iterations: 285
currently lose_sum: 95.3041079044342
time_elpased: 2.466
batch start
#iterations: 286
currently lose_sum: 95.15923184156418
time_elpased: 2.177
batch start
#iterations: 287
currently lose_sum: 95.12485963106155
time_elpased: 2.415
batch start
#iterations: 288
currently lose_sum: 95.00634318590164
time_elpased: 2.443
batch start
#iterations: 289
currently lose_sum: 95.02153050899506
time_elpased: 2.266
batch start
#iterations: 290
currently lose_sum: 95.03894132375717
time_elpased: 2.284
batch start
#iterations: 291
currently lose_sum: 94.9741302728653
time_elpased: 2.318
batch start
#iterations: 292
currently lose_sum: 95.27456444501877
time_elpased: 2.167
batch start
#iterations: 293
currently lose_sum: 94.98399806022644
time_elpased: 1.966
batch start
#iterations: 294
currently lose_sum: 95.08184391260147
time_elpased: 2.316
batch start
#iterations: 295
currently lose_sum: 95.15063697099686
time_elpased: 1.815
batch start
#iterations: 296
currently lose_sum: 95.02805584669113
time_elpased: 2.3
batch start
#iterations: 297
currently lose_sum: 95.3174090385437
time_elpased: 2.152
batch start
#iterations: 298
currently lose_sum: 95.09292709827423
time_elpased: 2.158
batch start
#iterations: 299
currently lose_sum: 95.02161461114883
time_elpased: 2.257
start validation test
0.58206185567
0.634284760474
0.391067201811
0.483829895595
0.582397176244
67.651
batch start
#iterations: 300
currently lose_sum: 94.75590884685516
time_elpased: 2.339
batch start
#iterations: 301
currently lose_sum: 94.78032678365707
time_elpased: 2.292
batch start
#iterations: 302
currently lose_sum: 95.07110637426376
time_elpased: 2.122
batch start
#iterations: 303
currently lose_sum: 94.84538322687149
time_elpased: 2.265
batch start
#iterations: 304
currently lose_sum: 95.0689144730568
time_elpased: 2.455
batch start
#iterations: 305
currently lose_sum: 94.88863307237625
time_elpased: 2.429
batch start
#iterations: 306
currently lose_sum: 95.2226727604866
time_elpased: 2.139
batch start
#iterations: 307
currently lose_sum: 94.6715002655983
time_elpased: 2.069
batch start
#iterations: 308
currently lose_sum: 95.10682570934296
time_elpased: 2.236
batch start
#iterations: 309
currently lose_sum: 94.846759557724
time_elpased: 2.118
batch start
#iterations: 310
currently lose_sum: 95.23555767536163
time_elpased: 2.325
batch start
#iterations: 311
currently lose_sum: 94.5058245062828
time_elpased: 2.244
batch start
#iterations: 312
currently lose_sum: 94.71617013216019
time_elpased: 2.253
batch start
#iterations: 313
currently lose_sum: 95.04199308156967
time_elpased: 2.331
batch start
#iterations: 314
currently lose_sum: 95.07564121484756
time_elpased: 2.264
batch start
#iterations: 315
currently lose_sum: 94.71739423274994
time_elpased: 2.201
batch start
#iterations: 316
currently lose_sum: 94.80839598178864
time_elpased: 2.373
batch start
#iterations: 317
currently lose_sum: 94.4802176952362
time_elpased: 2.107
batch start
#iterations: 318
currently lose_sum: 94.64595931768417
time_elpased: 2.222
batch start
#iterations: 319
currently lose_sum: 94.79608857631683
time_elpased: 2.176
start validation test
0.581494845361
0.60817763336
0.46228259751
0.525287961176
0.581704140849
67.104
batch start
#iterations: 320
currently lose_sum: 94.78781825304031
time_elpased: 2.201
batch start
#iterations: 321
currently lose_sum: 95.01767545938492
time_elpased: 1.943
batch start
#iterations: 322
currently lose_sum: 95.22383004426956
time_elpased: 2.013
batch start
#iterations: 323
currently lose_sum: 94.61160337924957
time_elpased: 2.292
batch start
#iterations: 324
currently lose_sum: 94.76222097873688
time_elpased: 2.459
batch start
#iterations: 325
currently lose_sum: 94.5861423611641
time_elpased: 2.466
batch start
#iterations: 326
currently lose_sum: 94.94294029474258
time_elpased: 2.398
batch start
#iterations: 327
currently lose_sum: 94.89184951782227
time_elpased: 2.259
batch start
#iterations: 328
currently lose_sum: 94.53059321641922
time_elpased: 2.161
batch start
#iterations: 329
currently lose_sum: 94.87479835748672
time_elpased: 2.117
batch start
#iterations: 330
currently lose_sum: 94.44995474815369
time_elpased: 2.226
batch start
#iterations: 331
currently lose_sum: 94.73489946126938
time_elpased: 2.383
batch start
#iterations: 332
currently lose_sum: 94.61166894435883
time_elpased: 2.16
batch start
#iterations: 333
currently lose_sum: 94.69176214933395
time_elpased: 2.603
batch start
#iterations: 334
currently lose_sum: 94.76113307476044
time_elpased: 2.416
batch start
#iterations: 335
currently lose_sum: 94.69989657402039
time_elpased: 2.097
batch start
#iterations: 336
currently lose_sum: 95.00501918792725
time_elpased: 2.284
batch start
#iterations: 337
currently lose_sum: 94.60158050060272
time_elpased: 2.405
batch start
#iterations: 338
currently lose_sum: 94.73833364248276
time_elpased: 2.4
batch start
#iterations: 339
currently lose_sum: 94.4463741183281
time_elpased: 2.113
start validation test
0.576958762887
0.629369431117
0.377997324277
0.472320452646
0.57730807038
68.309
batch start
#iterations: 340
currently lose_sum: 94.88007420301437
time_elpased: 2.096
batch start
#iterations: 341
currently lose_sum: 94.872325360775
time_elpased: 2.536
batch start
#iterations: 342
currently lose_sum: 94.74883407354355
time_elpased: 2.549
batch start
#iterations: 343
currently lose_sum: 94.5577991604805
time_elpased: 2.334
batch start
#iterations: 344
currently lose_sum: 94.64351832866669
time_elpased: 2.128
batch start
#iterations: 345
currently lose_sum: 94.74816918373108
time_elpased: 2.164
batch start
#iterations: 346
currently lose_sum: 94.88465160131454
time_elpased: 2.227
batch start
#iterations: 347
currently lose_sum: 94.46561247110367
time_elpased: 2.186
batch start
#iterations: 348
currently lose_sum: 94.82856756448746
time_elpased: 2.168
batch start
#iterations: 349
currently lose_sum: 94.51248848438263
time_elpased: 2.336
batch start
#iterations: 350
currently lose_sum: 94.62721461057663
time_elpased: 2.272
batch start
#iterations: 351
currently lose_sum: 94.5022668838501
time_elpased: 1.983
batch start
#iterations: 352
currently lose_sum: 94.07380479574203
time_elpased: 2.35
batch start
#iterations: 353
currently lose_sum: 94.45102971792221
time_elpased: 2.166
batch start
#iterations: 354
currently lose_sum: 94.5415466427803
time_elpased: 2.349
batch start
#iterations: 355
currently lose_sum: 94.56156975030899
time_elpased: 2.403
batch start
#iterations: 356
currently lose_sum: 94.56703007221222
time_elpased: 2.252
batch start
#iterations: 357
currently lose_sum: 94.22141045331955
time_elpased: 2.437
batch start
#iterations: 358
currently lose_sum: 94.3369928598404
time_elpased: 2.32
batch start
#iterations: 359
currently lose_sum: 94.5535494685173
time_elpased: 1.95
start validation test
0.577319587629
0.621108095162
0.400329319749
0.486858573217
0.577630321343
68.087
batch start
#iterations: 360
currently lose_sum: 94.33524423837662
time_elpased: 2.17
batch start
#iterations: 361
currently lose_sum: 94.63833671808243
time_elpased: 2.302
batch start
#iterations: 362
currently lose_sum: 94.52783727645874
time_elpased: 2.016
batch start
#iterations: 363
currently lose_sum: 94.36503964662552
time_elpased: 2.072
batch start
#iterations: 364
currently lose_sum: 94.28793913125992
time_elpased: 2.026
batch start
#iterations: 365
currently lose_sum: 94.07821053266525
time_elpased: 2.137
batch start
#iterations: 366
currently lose_sum: 94.48811608552933
time_elpased: 2.35
batch start
#iterations: 367
currently lose_sum: 94.59421914815903
time_elpased: 2.355
batch start
#iterations: 368
currently lose_sum: 94.31665235757828
time_elpased: 2.307
batch start
#iterations: 369
currently lose_sum: 94.38617104291916
time_elpased: 2.374
batch start
#iterations: 370
currently lose_sum: 94.03624814748764
time_elpased: 2.161
batch start
#iterations: 371
currently lose_sum: 93.86590963602066
time_elpased: 2.265
batch start
#iterations: 372
currently lose_sum: 94.10411673784256
time_elpased: 2.42
batch start
#iterations: 373
currently lose_sum: 94.42920297384262
time_elpased: 2.259
batch start
#iterations: 374
currently lose_sum: 94.35839146375656
time_elpased: 2.129
batch start
#iterations: 375
currently lose_sum: 94.57825791835785
time_elpased: 1.908
batch start
#iterations: 376
currently lose_sum: 94.24037104845047
time_elpased: 2.194
batch start
#iterations: 377
currently lose_sum: 94.36321008205414
time_elpased: 2.215
batch start
#iterations: 378
currently lose_sum: 94.33040124177933
time_elpased: 2.448
batch start
#iterations: 379
currently lose_sum: 94.3451492190361
time_elpased: 2.503
start validation test
0.578711340206
0.634917860888
0.373880827416
0.470626335903
0.579070951764
68.963
batch start
#iterations: 380
currently lose_sum: 94.43216210603714
time_elpased: 2.245
batch start
#iterations: 381
currently lose_sum: 94.46095812320709
time_elpased: 2.199
batch start
#iterations: 382
currently lose_sum: 94.2344502210617
time_elpased: 2.47
batch start
#iterations: 383
currently lose_sum: 94.21538835763931
time_elpased: 2.394
batch start
#iterations: 384
currently lose_sum: 94.30106890201569
time_elpased: 1.748
batch start
#iterations: 385
currently lose_sum: 94.20382326841354
time_elpased: 2.172
batch start
#iterations: 386
currently lose_sum: 94.03846734762192
time_elpased: 2.26
batch start
#iterations: 387
currently lose_sum: 93.90598601102829
time_elpased: 2.16
batch start
#iterations: 388
currently lose_sum: 94.22825461626053
time_elpased: 2.138
batch start
#iterations: 389
currently lose_sum: 94.43101316690445
time_elpased: 2.258
batch start
#iterations: 390
currently lose_sum: 93.97985106706619
time_elpased: 2.326
batch start
#iterations: 391
currently lose_sum: 94.00053143501282
time_elpased: 2.414
batch start
#iterations: 392
currently lose_sum: 93.88847327232361
time_elpased: 2.054
batch start
#iterations: 393
currently lose_sum: 94.20163530111313
time_elpased: 2.457
batch start
#iterations: 394
currently lose_sum: 93.86185306310654
time_elpased: 2.083
batch start
#iterations: 395
currently lose_sum: 94.0095899105072
time_elpased: 2.346
batch start
#iterations: 396
currently lose_sum: 93.74713689088821
time_elpased: 2.444
batch start
#iterations: 397
currently lose_sum: 93.77041113376617
time_elpased: 2.413
batch start
#iterations: 398
currently lose_sum: 94.17824047803879
time_elpased: 1.993
batch start
#iterations: 399
currently lose_sum: 93.9226365685463
time_elpased: 2.01
start validation test
0.579432989691
0.620775193798
0.412061335803
0.495329993196
0.579726836444
68.366
acc: 0.585
pre: 0.616
rec: 0.456
F1: 0.524
auc: 0.585
