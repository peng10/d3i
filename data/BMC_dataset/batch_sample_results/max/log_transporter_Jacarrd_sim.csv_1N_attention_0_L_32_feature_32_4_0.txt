start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.35692518949509
time_elpased: 2.013
batch start
#iterations: 1
currently lose_sum: 100.30825078487396
time_elpased: 1.923
batch start
#iterations: 2
currently lose_sum: 100.08000832796097
time_elpased: 1.926
batch start
#iterations: 3
currently lose_sum: 100.18413639068604
time_elpased: 1.917
batch start
#iterations: 4
currently lose_sum: 100.03371286392212
time_elpased: 1.923
batch start
#iterations: 5
currently lose_sum: 100.08013534545898
time_elpased: 1.914
batch start
#iterations: 6
currently lose_sum: 99.93789434432983
time_elpased: 1.982
batch start
#iterations: 7
currently lose_sum: 99.83088624477386
time_elpased: 1.933
batch start
#iterations: 8
currently lose_sum: 99.84195476770401
time_elpased: 1.917
batch start
#iterations: 9
currently lose_sum: 99.81183618307114
time_elpased: 1.922
batch start
#iterations: 10
currently lose_sum: 99.77106022834778
time_elpased: 1.902
batch start
#iterations: 11
currently lose_sum: 99.7234069108963
time_elpased: 1.946
batch start
#iterations: 12
currently lose_sum: 99.73824018239975
time_elpased: 1.928
batch start
#iterations: 13
currently lose_sum: 99.50147032737732
time_elpased: 1.981
batch start
#iterations: 14
currently lose_sum: 99.53007739782333
time_elpased: 1.913
batch start
#iterations: 15
currently lose_sum: 99.39569330215454
time_elpased: 1.949
batch start
#iterations: 16
currently lose_sum: 99.47669637203217
time_elpased: 1.918
batch start
#iterations: 17
currently lose_sum: 99.50121003389359
time_elpased: 1.952
batch start
#iterations: 18
currently lose_sum: 99.4074541926384
time_elpased: 1.918
batch start
#iterations: 19
currently lose_sum: 99.42347365617752
time_elpased: 1.947
start validation test
0.585
0.595374398901
0.535144591952
0.563655086445
0.585087528858
65.581
batch start
#iterations: 20
currently lose_sum: 99.4067702293396
time_elpased: 1.978
batch start
#iterations: 21
currently lose_sum: 99.28071200847626
time_elpased: 1.948
batch start
#iterations: 22
currently lose_sum: 99.25416058301926
time_elpased: 1.911
batch start
#iterations: 23
currently lose_sum: 99.22581535577774
time_elpased: 1.884
batch start
#iterations: 24
currently lose_sum: 99.28826534748077
time_elpased: 1.886
batch start
#iterations: 25
currently lose_sum: 99.10196250677109
time_elpased: 1.929
batch start
#iterations: 26
currently lose_sum: 98.94820582866669
time_elpased: 1.872
batch start
#iterations: 27
currently lose_sum: 99.06749713420868
time_elpased: 1.915
batch start
#iterations: 28
currently lose_sum: 99.05020070075989
time_elpased: 1.917
batch start
#iterations: 29
currently lose_sum: 99.11380898952484
time_elpased: 1.901
batch start
#iterations: 30
currently lose_sum: 98.99257904291153
time_elpased: 1.894
batch start
#iterations: 31
currently lose_sum: 99.01141268014908
time_elpased: 1.919
batch start
#iterations: 32
currently lose_sum: 98.63172519207001
time_elpased: 1.906
batch start
#iterations: 33
currently lose_sum: 99.16331732273102
time_elpased: 1.893
batch start
#iterations: 34
currently lose_sum: 98.85264241695404
time_elpased: 1.896
batch start
#iterations: 35
currently lose_sum: 98.957515001297
time_elpased: 1.913
batch start
#iterations: 36
currently lose_sum: 99.03259295225143
time_elpased: 1.885
batch start
#iterations: 37
currently lose_sum: 99.17990112304688
time_elpased: 1.909
batch start
#iterations: 38
currently lose_sum: 98.96917426586151
time_elpased: 1.92
batch start
#iterations: 39
currently lose_sum: 98.78989511728287
time_elpased: 1.934
start validation test
0.587113402062
0.593904719991
0.555521251415
0.574072104647
0.587168866955
65.514
batch start
#iterations: 40
currently lose_sum: 98.9004048705101
time_elpased: 1.906
batch start
#iterations: 41
currently lose_sum: 98.67453116178513
time_elpased: 1.971
batch start
#iterations: 42
currently lose_sum: 98.74681323766708
time_elpased: 1.933
batch start
#iterations: 43
currently lose_sum: 98.59066206216812
time_elpased: 1.932
batch start
#iterations: 44
currently lose_sum: 98.77793484926224
time_elpased: 1.943
batch start
#iterations: 45
currently lose_sum: 98.63215905427933
time_elpased: 2.013
batch start
#iterations: 46
currently lose_sum: 98.53599721193314
time_elpased: 1.97
batch start
#iterations: 47
currently lose_sum: 98.6446543931961
time_elpased: 1.915
batch start
#iterations: 48
currently lose_sum: 98.58010745048523
time_elpased: 1.934
batch start
#iterations: 49
currently lose_sum: 98.69227755069733
time_elpased: 1.927
batch start
#iterations: 50
currently lose_sum: 98.91532844305038
time_elpased: 1.95
batch start
#iterations: 51
currently lose_sum: 98.56842237710953
time_elpased: 1.92
batch start
#iterations: 52
currently lose_sum: 98.59214621782303
time_elpased: 1.977
batch start
#iterations: 53
currently lose_sum: 98.62494331598282
time_elpased: 1.962
batch start
#iterations: 54
currently lose_sum: 98.73060578107834
time_elpased: 1.915
batch start
#iterations: 55
currently lose_sum: 98.48282462358475
time_elpased: 1.953
batch start
#iterations: 56
currently lose_sum: 98.70985168218613
time_elpased: 1.934
batch start
#iterations: 57
currently lose_sum: 98.51576924324036
time_elpased: 1.955
batch start
#iterations: 58
currently lose_sum: 98.34778892993927
time_elpased: 1.912
batch start
#iterations: 59
currently lose_sum: 98.52775758504868
time_elpased: 1.884
start validation test
0.572525773196
0.614175753688
0.394154574457
0.480160471385
0.572838931347
66.175
batch start
#iterations: 60
currently lose_sum: 98.46940684318542
time_elpased: 1.882
batch start
#iterations: 61
currently lose_sum: 98.21611768007278
time_elpased: 1.957
batch start
#iterations: 62
currently lose_sum: 98.13347774744034
time_elpased: 1.92
batch start
#iterations: 63
currently lose_sum: 98.60998350381851
time_elpased: 1.888
batch start
#iterations: 64
currently lose_sum: 98.61396789550781
time_elpased: 1.923
batch start
#iterations: 65
currently lose_sum: 98.16860073804855
time_elpased: 1.889
batch start
#iterations: 66
currently lose_sum: 98.12414997816086
time_elpased: 1.895
batch start
#iterations: 67
currently lose_sum: 98.21577221155167
time_elpased: 1.916
batch start
#iterations: 68
currently lose_sum: 98.17759591341019
time_elpased: 1.925
batch start
#iterations: 69
currently lose_sum: 98.35348218679428
time_elpased: 1.944
batch start
#iterations: 70
currently lose_sum: 98.01205825805664
time_elpased: 1.952
batch start
#iterations: 71
currently lose_sum: 97.99192661046982
time_elpased: 1.913
batch start
#iterations: 72
currently lose_sum: 97.841093480587
time_elpased: 1.924
batch start
#iterations: 73
currently lose_sum: 97.95163905620575
time_elpased: 1.935
batch start
#iterations: 74
currently lose_sum: 97.75238806009293
time_elpased: 1.939
batch start
#iterations: 75
currently lose_sum: 98.00977593660355
time_elpased: 1.935
batch start
#iterations: 76
currently lose_sum: 97.6787291765213
time_elpased: 1.939
batch start
#iterations: 77
currently lose_sum: 98.02792292833328
time_elpased: 1.929
batch start
#iterations: 78
currently lose_sum: 97.78983348608017
time_elpased: 1.943
batch start
#iterations: 79
currently lose_sum: 98.16200590133667
time_elpased: 1.939
start validation test
0.589175257732
0.60020649306
0.538437789441
0.56764673972
0.589264335183
65.291
batch start
#iterations: 80
currently lose_sum: 97.67402452230453
time_elpased: 1.931
batch start
#iterations: 81
currently lose_sum: 97.99778288602829
time_elpased: 1.995
batch start
#iterations: 82
currently lose_sum: 97.814806163311
time_elpased: 1.942
batch start
#iterations: 83
currently lose_sum: 97.84799426794052
time_elpased: 1.91
batch start
#iterations: 84
currently lose_sum: 97.72672909498215
time_elpased: 1.916
batch start
#iterations: 85
currently lose_sum: 98.07076561450958
time_elpased: 2.042
batch start
#iterations: 86
currently lose_sum: 97.67271882295609
time_elpased: 1.96
batch start
#iterations: 87
currently lose_sum: 97.52838569879532
time_elpased: 1.975
batch start
#iterations: 88
currently lose_sum: 97.86587864160538
time_elpased: 1.906
batch start
#iterations: 89
currently lose_sum: 97.62059623003006
time_elpased: 1.926
batch start
#iterations: 90
currently lose_sum: 97.71599078178406
time_elpased: 1.942
batch start
#iterations: 91
currently lose_sum: 97.76048791408539
time_elpased: 1.958
batch start
#iterations: 92
currently lose_sum: 97.26101839542389
time_elpased: 1.964
batch start
#iterations: 93
currently lose_sum: 97.73788559436798
time_elpased: 1.934
batch start
#iterations: 94
currently lose_sum: 97.4992504119873
time_elpased: 1.952
batch start
#iterations: 95
currently lose_sum: 97.78870737552643
time_elpased: 1.933
batch start
#iterations: 96
currently lose_sum: 97.55935579538345
time_elpased: 1.963
batch start
#iterations: 97
currently lose_sum: 97.44603788852692
time_elpased: 1.959
batch start
#iterations: 98
currently lose_sum: 97.43708491325378
time_elpased: 1.955
batch start
#iterations: 99
currently lose_sum: 97.49842357635498
time_elpased: 1.96
start validation test
0.585412371134
0.606218274112
0.491612637645
0.542933454566
0.585577051034
65.526
batch start
#iterations: 100
currently lose_sum: 97.66629594564438
time_elpased: 1.92
batch start
#iterations: 101
currently lose_sum: 97.69533067941666
time_elpased: 1.929
batch start
#iterations: 102
currently lose_sum: 97.36888939142227
time_elpased: 1.916
batch start
#iterations: 103
currently lose_sum: 97.44272696971893
time_elpased: 1.913
batch start
#iterations: 104
currently lose_sum: 97.06499576568604
time_elpased: 1.899
batch start
#iterations: 105
currently lose_sum: 97.40604037046432
time_elpased: 1.902
batch start
#iterations: 106
currently lose_sum: 97.5276306271553
time_elpased: 1.92
batch start
#iterations: 107
currently lose_sum: 97.1087617278099
time_elpased: 1.931
batch start
#iterations: 108
currently lose_sum: 97.31063061952591
time_elpased: 1.907
batch start
#iterations: 109
currently lose_sum: 97.26606595516205
time_elpased: 1.945
batch start
#iterations: 110
currently lose_sum: 97.40965515375137
time_elpased: 1.965
batch start
#iterations: 111
currently lose_sum: 96.92266064882278
time_elpased: 1.976
batch start
#iterations: 112
currently lose_sum: 97.18903028964996
time_elpased: 1.913
batch start
#iterations: 113
currently lose_sum: 97.0585697889328
time_elpased: 1.967
batch start
#iterations: 114
currently lose_sum: 97.36078643798828
time_elpased: 1.977
batch start
#iterations: 115
currently lose_sum: 97.40191096067429
time_elpased: 1.971
batch start
#iterations: 116
currently lose_sum: 97.10218864679337
time_elpased: 1.908
batch start
#iterations: 117
currently lose_sum: 97.23844301700592
time_elpased: 1.966
batch start
#iterations: 118
currently lose_sum: 97.1078131198883
time_elpased: 1.968
batch start
#iterations: 119
currently lose_sum: 97.30897414684296
time_elpased: 1.966
start validation test
0.584226804124
0.602178487437
0.50066893074
0.546752079119
0.584373502858
65.870
batch start
#iterations: 120
currently lose_sum: 97.05369681119919
time_elpased: 1.963
batch start
#iterations: 121
currently lose_sum: 97.04253923892975
time_elpased: 1.976
batch start
#iterations: 122
currently lose_sum: 96.99159795045853
time_elpased: 1.962
batch start
#iterations: 123
currently lose_sum: 97.22901475429535
time_elpased: 1.949
batch start
#iterations: 124
currently lose_sum: 97.37466424703598
time_elpased: 1.941
batch start
#iterations: 125
currently lose_sum: 97.13576358556747
time_elpased: 1.958
batch start
#iterations: 126
currently lose_sum: 96.82092124223709
time_elpased: 1.921
batch start
#iterations: 127
currently lose_sum: 96.7645543217659
time_elpased: 1.932
batch start
#iterations: 128
currently lose_sum: 97.18201780319214
time_elpased: 1.948
batch start
#iterations: 129
currently lose_sum: 97.24405246973038
time_elpased: 1.984
batch start
#iterations: 130
currently lose_sum: 96.7586230635643
time_elpased: 1.934
batch start
#iterations: 131
currently lose_sum: 96.83720284700394
time_elpased: 1.932
batch start
#iterations: 132
currently lose_sum: 96.88942301273346
time_elpased: 1.917
batch start
#iterations: 133
currently lose_sum: 96.65643233060837
time_elpased: 1.998
batch start
#iterations: 134
currently lose_sum: 96.67208445072174
time_elpased: 1.941
batch start
#iterations: 135
currently lose_sum: 96.89031881093979
time_elpased: 1.955
batch start
#iterations: 136
currently lose_sum: 96.61280542612076
time_elpased: 1.925
batch start
#iterations: 137
currently lose_sum: 96.79142552614212
time_elpased: 1.945
batch start
#iterations: 138
currently lose_sum: 96.29411298036575
time_elpased: 1.935
batch start
#iterations: 139
currently lose_sum: 96.77873194217682
time_elpased: 1.968
start validation test
0.586597938144
0.613088098094
0.473397139035
0.534262485482
0.586796679607
66.182
batch start
#iterations: 140
currently lose_sum: 96.73622018098831
time_elpased: 1.911
batch start
#iterations: 141
currently lose_sum: 96.53285026550293
time_elpased: 1.966
batch start
#iterations: 142
currently lose_sum: 96.59310585260391
time_elpased: 1.963
batch start
#iterations: 143
currently lose_sum: 96.52957332134247
time_elpased: 1.961
batch start
#iterations: 144
currently lose_sum: 96.81391829252243
time_elpased: 1.958
batch start
#iterations: 145
currently lose_sum: 96.84088051319122
time_elpased: 1.982
batch start
#iterations: 146
currently lose_sum: 96.58123445510864
time_elpased: 1.932
batch start
#iterations: 147
currently lose_sum: 96.84978729486465
time_elpased: 1.929
batch start
#iterations: 148
currently lose_sum: 96.50496906042099
time_elpased: 1.942
batch start
#iterations: 149
currently lose_sum: 96.45990753173828
time_elpased: 1.963
batch start
#iterations: 150
currently lose_sum: 96.5852158665657
time_elpased: 1.961
batch start
#iterations: 151
currently lose_sum: 96.51734173297882
time_elpased: 1.957
batch start
#iterations: 152
currently lose_sum: 96.40326285362244
time_elpased: 1.925
batch start
#iterations: 153
currently lose_sum: 96.51184552907944
time_elpased: 1.95
batch start
#iterations: 154
currently lose_sum: 96.64699822664261
time_elpased: 1.933
batch start
#iterations: 155
currently lose_sum: 96.47244161367416
time_elpased: 1.951
batch start
#iterations: 156
currently lose_sum: 96.12690895795822
time_elpased: 1.959
batch start
#iterations: 157
currently lose_sum: 96.59031468629837
time_elpased: 1.941
batch start
#iterations: 158
currently lose_sum: 96.40613031387329
time_elpased: 1.948
batch start
#iterations: 159
currently lose_sum: 96.60487389564514
time_elpased: 1.974
start validation test
0.589484536082
0.619789531229
0.466707831635
0.532464482799
0.589700089524
66.375
batch start
#iterations: 160
currently lose_sum: 95.92669349908829
time_elpased: 1.945
batch start
#iterations: 161
currently lose_sum: 96.13932120800018
time_elpased: 1.939
batch start
#iterations: 162
currently lose_sum: 96.22460216283798
time_elpased: 1.927
batch start
#iterations: 163
currently lose_sum: 96.34930157661438
time_elpased: 1.977
batch start
#iterations: 164
currently lose_sum: 96.54126054048538
time_elpased: 1.947
batch start
#iterations: 165
currently lose_sum: 96.12951624393463
time_elpased: 1.991
batch start
#iterations: 166
currently lose_sum: 96.04731458425522
time_elpased: 1.953
batch start
#iterations: 167
currently lose_sum: 96.30303072929382
time_elpased: 1.948
batch start
#iterations: 168
currently lose_sum: 96.2300950884819
time_elpased: 1.919
batch start
#iterations: 169
currently lose_sum: 96.07877397537231
time_elpased: 1.983
batch start
#iterations: 170
currently lose_sum: 96.3836720585823
time_elpased: 1.964
batch start
#iterations: 171
currently lose_sum: 95.99602741003036
time_elpased: 1.968
batch start
#iterations: 172
currently lose_sum: 96.17890655994415
time_elpased: 1.944
batch start
#iterations: 173
currently lose_sum: 96.10462611913681
time_elpased: 1.986
batch start
#iterations: 174
currently lose_sum: 95.74824810028076
time_elpased: 1.99
batch start
#iterations: 175
currently lose_sum: 96.08241474628448
time_elpased: 1.95
batch start
#iterations: 176
currently lose_sum: 96.3114869594574
time_elpased: 1.961
batch start
#iterations: 177
currently lose_sum: 96.18259662389755
time_elpased: 1.953
batch start
#iterations: 178
currently lose_sum: 95.86684137582779
time_elpased: 1.935
batch start
#iterations: 179
currently lose_sum: 95.9299505352974
time_elpased: 1.975
start validation test
0.588298969072
0.620205669817
0.459298137285
0.527759711465
0.588525449929
66.836
batch start
#iterations: 180
currently lose_sum: 95.99935013055801
time_elpased: 1.94
batch start
#iterations: 181
currently lose_sum: 95.9868540763855
time_elpased: 1.95
batch start
#iterations: 182
currently lose_sum: 95.91590559482574
time_elpased: 1.97
batch start
#iterations: 183
currently lose_sum: 95.75653785467148
time_elpased: 1.98
batch start
#iterations: 184
currently lose_sum: 95.8193142414093
time_elpased: 1.964
batch start
#iterations: 185
currently lose_sum: 95.8691834807396
time_elpased: 2.01
batch start
#iterations: 186
currently lose_sum: 95.87387186288834
time_elpased: 1.935
batch start
#iterations: 187
currently lose_sum: 95.68930107355118
time_elpased: 1.999
batch start
#iterations: 188
currently lose_sum: 95.67755323648453
time_elpased: 1.951
batch start
#iterations: 189
currently lose_sum: 95.45807367563248
time_elpased: 1.974
batch start
#iterations: 190
currently lose_sum: 95.4018018245697
time_elpased: 1.97
batch start
#iterations: 191
currently lose_sum: 95.763707280159
time_elpased: 1.944
batch start
#iterations: 192
currently lose_sum: 95.63798213005066
time_elpased: 1.941
batch start
#iterations: 193
currently lose_sum: 95.64532458782196
time_elpased: 1.967
batch start
#iterations: 194
currently lose_sum: 95.37366914749146
time_elpased: 1.968
batch start
#iterations: 195
currently lose_sum: 96.13512521982193
time_elpased: 1.953
batch start
#iterations: 196
currently lose_sum: 95.72133833169937
time_elpased: 1.971
batch start
#iterations: 197
currently lose_sum: 95.8556958436966
time_elpased: 1.991
batch start
#iterations: 198
currently lose_sum: 95.33702170848846
time_elpased: 1.922
batch start
#iterations: 199
currently lose_sum: 95.75510531663895
time_elpased: 1.946
start validation test
0.588505154639
0.622250423012
0.454152516209
0.525075852222
0.588741031418
67.321
batch start
#iterations: 200
currently lose_sum: 96.06332087516785
time_elpased: 1.938
batch start
#iterations: 201
currently lose_sum: 95.82213205099106
time_elpased: 1.943
batch start
#iterations: 202
currently lose_sum: 95.5664958357811
time_elpased: 1.927
batch start
#iterations: 203
currently lose_sum: 95.91232508420944
time_elpased: 1.901
batch start
#iterations: 204
currently lose_sum: 95.5215585231781
time_elpased: 1.935
batch start
#iterations: 205
currently lose_sum: 95.39030748605728
time_elpased: 1.927
batch start
#iterations: 206
currently lose_sum: 95.77813988924026
time_elpased: 1.93
batch start
#iterations: 207
currently lose_sum: 95.5908317565918
time_elpased: 1.912
batch start
#iterations: 208
currently lose_sum: 95.16535013914108
time_elpased: 1.924
batch start
#iterations: 209
currently lose_sum: 95.36311423778534
time_elpased: 1.927
batch start
#iterations: 210
currently lose_sum: 95.40409278869629
time_elpased: 1.907
batch start
#iterations: 211
currently lose_sum: 95.265644967556
time_elpased: 1.942
batch start
#iterations: 212
currently lose_sum: 95.09867125749588
time_elpased: 1.931
batch start
#iterations: 213
currently lose_sum: 94.96429544687271
time_elpased: 2.011
batch start
#iterations: 214
currently lose_sum: 95.20966279506683
time_elpased: 1.962
batch start
#iterations: 215
currently lose_sum: 95.19186294078827
time_elpased: 1.895
batch start
#iterations: 216
currently lose_sum: 95.215212225914
time_elpased: 1.937
batch start
#iterations: 217
currently lose_sum: 95.04257506132126
time_elpased: 1.924
batch start
#iterations: 218
currently lose_sum: 95.1226247549057
time_elpased: 1.905
batch start
#iterations: 219
currently lose_sum: 95.29255813360214
time_elpased: 1.918
start validation test
0.579793814433
0.611071682044
0.443037974684
0.513661854194
0.580033910403
68.294
batch start
#iterations: 220
currently lose_sum: 95.3048803806305
time_elpased: 1.899
batch start
#iterations: 221
currently lose_sum: 95.33218955993652
time_elpased: 1.909
batch start
#iterations: 222
currently lose_sum: 94.69593441486359
time_elpased: 1.916
batch start
#iterations: 223
currently lose_sum: 95.04462313652039
time_elpased: 1.922
batch start
#iterations: 224
currently lose_sum: 95.13941878080368
time_elpased: 1.921
batch start
#iterations: 225
currently lose_sum: 95.05247992277145
time_elpased: 1.945
batch start
#iterations: 226
currently lose_sum: 94.8290628194809
time_elpased: 1.922
batch start
#iterations: 227
currently lose_sum: 94.86983424425125
time_elpased: 1.918
batch start
#iterations: 228
currently lose_sum: 94.67326438426971
time_elpased: 1.948
batch start
#iterations: 229
currently lose_sum: 94.72804027795792
time_elpased: 1.92
batch start
#iterations: 230
currently lose_sum: 94.57694059610367
time_elpased: 1.923
batch start
#iterations: 231
currently lose_sum: 94.8761745095253
time_elpased: 1.955
batch start
#iterations: 232
currently lose_sum: 94.65501582622528
time_elpased: 1.922
batch start
#iterations: 233
currently lose_sum: 94.63051390647888
time_elpased: 1.944
batch start
#iterations: 234
currently lose_sum: 94.72218483686447
time_elpased: 1.885
batch start
#iterations: 235
currently lose_sum: 94.90123295783997
time_elpased: 1.913
batch start
#iterations: 236
currently lose_sum: 94.56433606147766
time_elpased: 1.945
batch start
#iterations: 237
currently lose_sum: 95.2189434170723
time_elpased: 1.916
batch start
#iterations: 238
currently lose_sum: 94.88805800676346
time_elpased: 1.9
batch start
#iterations: 239
currently lose_sum: 94.61346119642258
time_elpased: 1.937
start validation test
0.581701030928
0.611684327942
0.451476793249
0.519509740067
0.581929659663
68.309
batch start
#iterations: 240
currently lose_sum: 94.56175994873047
time_elpased: 1.913
batch start
#iterations: 241
currently lose_sum: 95.17605596780777
time_elpased: 1.956
batch start
#iterations: 242
currently lose_sum: 94.90729355812073
time_elpased: 1.898
batch start
#iterations: 243
currently lose_sum: 94.90928393602371
time_elpased: 1.921
batch start
#iterations: 244
currently lose_sum: 94.56401842832565
time_elpased: 1.895
batch start
#iterations: 245
currently lose_sum: 94.92026680707932
time_elpased: 1.903
batch start
#iterations: 246
currently lose_sum: 94.63479202985764
time_elpased: 1.914
batch start
#iterations: 247
currently lose_sum: 94.6928174495697
time_elpased: 1.925
batch start
#iterations: 248
currently lose_sum: 94.63953292369843
time_elpased: 1.897
batch start
#iterations: 249
currently lose_sum: 94.56767576932907
time_elpased: 1.919
batch start
#iterations: 250
currently lose_sum: 94.50899267196655
time_elpased: 1.902
batch start
#iterations: 251
currently lose_sum: 94.40517503023148
time_elpased: 1.918
batch start
#iterations: 252
currently lose_sum: 94.9039101600647
time_elpased: 1.911
batch start
#iterations: 253
currently lose_sum: 94.64798939228058
time_elpased: 1.935
batch start
#iterations: 254
currently lose_sum: 94.23526030778885
time_elpased: 1.896
batch start
#iterations: 255
currently lose_sum: 94.20031648874283
time_elpased: 1.92
batch start
#iterations: 256
currently lose_sum: 94.47485601902008
time_elpased: 1.898
batch start
#iterations: 257
currently lose_sum: 94.56343162059784
time_elpased: 1.901
batch start
#iterations: 258
currently lose_sum: 94.73597031831741
time_elpased: 1.928
batch start
#iterations: 259
currently lose_sum: 94.4756383895874
time_elpased: 1.958
start validation test
0.582886597938
0.627371061295
0.41185551096
0.49726640159
0.583186869391
68.841
batch start
#iterations: 260
currently lose_sum: 94.15993112325668
time_elpased: 1.97
batch start
#iterations: 261
currently lose_sum: 94.4817743897438
time_elpased: 1.946
batch start
#iterations: 262
currently lose_sum: 94.59779751300812
time_elpased: 1.923
batch start
#iterations: 263
currently lose_sum: 94.41577529907227
time_elpased: 1.972
batch start
#iterations: 264
currently lose_sum: 94.26201796531677
time_elpased: 1.906
batch start
#iterations: 265
currently lose_sum: 94.0454568862915
time_elpased: 1.939
batch start
#iterations: 266
currently lose_sum: 94.42560160160065
time_elpased: 1.98
batch start
#iterations: 267
currently lose_sum: 94.39242005348206
time_elpased: 1.945
batch start
#iterations: 268
currently lose_sum: 94.3902438879013
time_elpased: 1.99
batch start
#iterations: 269
currently lose_sum: 94.47878855466843
time_elpased: 1.938
batch start
#iterations: 270
currently lose_sum: 94.0884513258934
time_elpased: 1.945
batch start
#iterations: 271
currently lose_sum: 94.27692925930023
time_elpased: 1.959
batch start
#iterations: 272
currently lose_sum: 94.63648289442062
time_elpased: 1.941
batch start
#iterations: 273
currently lose_sum: 94.05272805690765
time_elpased: 1.956
batch start
#iterations: 274
currently lose_sum: 93.80761915445328
time_elpased: 1.93
batch start
#iterations: 275
currently lose_sum: 94.62245392799377
time_elpased: 1.979
batch start
#iterations: 276
currently lose_sum: 93.94880264997482
time_elpased: 1.922
batch start
#iterations: 277
currently lose_sum: 94.44250440597534
time_elpased: 1.956
batch start
#iterations: 278
currently lose_sum: 94.38576656579971
time_elpased: 1.94
batch start
#iterations: 279
currently lose_sum: 94.2645149230957
time_elpased: 1.965
start validation test
0.583505154639
0.618194945848
0.440568076567
0.51448143252
0.583756102726
69.434
batch start
#iterations: 280
currently lose_sum: 94.2620877623558
time_elpased: 1.922
batch start
#iterations: 281
currently lose_sum: 93.9879464507103
time_elpased: 1.937
batch start
#iterations: 282
currently lose_sum: 94.19908571243286
time_elpased: 1.922
batch start
#iterations: 283
currently lose_sum: 93.79570120573044
time_elpased: 1.951
batch start
#iterations: 284
currently lose_sum: 94.24255156517029
time_elpased: 1.944
batch start
#iterations: 285
currently lose_sum: 94.09211510419846
time_elpased: 1.94
batch start
#iterations: 286
currently lose_sum: 93.70877933502197
time_elpased: 1.934
batch start
#iterations: 287
currently lose_sum: 93.66243422031403
time_elpased: 1.952
batch start
#iterations: 288
currently lose_sum: 93.77259492874146
time_elpased: 1.92
batch start
#iterations: 289
currently lose_sum: 93.55753874778748
time_elpased: 1.968
batch start
#iterations: 290
currently lose_sum: 94.01826053857803
time_elpased: 1.92
batch start
#iterations: 291
currently lose_sum: 93.76101315021515
time_elpased: 1.944
batch start
#iterations: 292
currently lose_sum: 93.91783928871155
time_elpased: 2.267
batch start
#iterations: 293
currently lose_sum: 93.75825130939484
time_elpased: 2.321
batch start
#iterations: 294
currently lose_sum: 93.61642956733704
time_elpased: 2.008
batch start
#iterations: 295
currently lose_sum: 93.58412939310074
time_elpased: 1.971
batch start
#iterations: 296
currently lose_sum: 93.61644268035889
time_elpased: 1.948
batch start
#iterations: 297
currently lose_sum: 94.0295849442482
time_elpased: 1.927
batch start
#iterations: 298
currently lose_sum: 93.64047008752823
time_elpased: 1.912
batch start
#iterations: 299
currently lose_sum: 93.64604157209396
time_elpased: 1.941
start validation test
0.58087628866
0.620516717325
0.420191417104
0.501073817267
0.581158395736
69.234
batch start
#iterations: 300
currently lose_sum: 93.78498017787933
time_elpased: 1.941
batch start
#iterations: 301
currently lose_sum: 93.48763811588287
time_elpased: 1.939
batch start
#iterations: 302
currently lose_sum: 93.84960460662842
time_elpased: 1.969
batch start
#iterations: 303
currently lose_sum: 93.69321382045746
time_elpased: 1.95
batch start
#iterations: 304
currently lose_sum: 94.29828602075577
time_elpased: 1.957
batch start
#iterations: 305
currently lose_sum: 93.79471516609192
time_elpased: 1.992
batch start
#iterations: 306
currently lose_sum: 93.89457184076309
time_elpased: 1.96
batch start
#iterations: 307
currently lose_sum: 93.71572047472
time_elpased: 1.934
batch start
#iterations: 308
currently lose_sum: 93.75668978691101
time_elpased: 1.955
batch start
#iterations: 309
currently lose_sum: 94.18524968624115
time_elpased: 2.002
batch start
#iterations: 310
currently lose_sum: 93.82260143756866
time_elpased: 1.957
batch start
#iterations: 311
currently lose_sum: 93.43947410583496
time_elpased: 1.968
batch start
#iterations: 312
currently lose_sum: 93.22022253274918
time_elpased: 1.937
batch start
#iterations: 313
currently lose_sum: 93.38183212280273
time_elpased: 1.941
batch start
#iterations: 314
currently lose_sum: 92.98756039142609
time_elpased: 1.937
batch start
#iterations: 315
currently lose_sum: 93.58653008937836
time_elpased: 1.917
batch start
#iterations: 316
currently lose_sum: 93.73164767026901
time_elpased: 1.928
batch start
#iterations: 317
currently lose_sum: 93.63905173540115
time_elpased: 1.993
batch start
#iterations: 318
currently lose_sum: 93.24227279424667
time_elpased: 1.931
batch start
#iterations: 319
currently lose_sum: 93.32398670911789
time_elpased: 1.943
start validation test
0.57706185567
0.627401415571
0.383142945354
0.475752348093
0.577402310227
71.170
batch start
#iterations: 320
currently lose_sum: 93.27571827173233
time_elpased: 1.98
batch start
#iterations: 321
currently lose_sum: 93.20877850055695
time_elpased: 1.966
batch start
#iterations: 322
currently lose_sum: 93.37698811292648
time_elpased: 1.939
batch start
#iterations: 323
currently lose_sum: 93.09163582324982
time_elpased: 1.951
batch start
#iterations: 324
currently lose_sum: 93.48805636167526
time_elpased: 1.938
batch start
#iterations: 325
currently lose_sum: 93.45483785867691
time_elpased: 1.956
batch start
#iterations: 326
currently lose_sum: 93.21274918317795
time_elpased: 1.947
batch start
#iterations: 327
currently lose_sum: 93.57220596075058
time_elpased: 1.945
batch start
#iterations: 328
currently lose_sum: 93.5207092165947
time_elpased: 1.954
batch start
#iterations: 329
currently lose_sum: 93.2677087187767
time_elpased: 1.959
batch start
#iterations: 330
currently lose_sum: 93.23036551475525
time_elpased: 1.945
batch start
#iterations: 331
currently lose_sum: 93.37486034631729
time_elpased: 1.939
batch start
#iterations: 332
currently lose_sum: 93.31329369544983
time_elpased: 1.899
batch start
#iterations: 333
currently lose_sum: 93.53667789697647
time_elpased: 1.911
batch start
#iterations: 334
currently lose_sum: 93.3346483707428
time_elpased: 1.915
batch start
#iterations: 335
currently lose_sum: 93.4237779378891
time_elpased: 1.886
batch start
#iterations: 336
currently lose_sum: 93.11770039796829
time_elpased: 1.917
batch start
#iterations: 337
currently lose_sum: 93.5032821893692
time_elpased: 1.914
batch start
#iterations: 338
currently lose_sum: 93.13719016313553
time_elpased: 1.879
batch start
#iterations: 339
currently lose_sum: 93.31721967458725
time_elpased: 1.918
start validation test
0.577835051546
0.617371252882
0.413296284862
0.495130070275
0.57812392473
71.431
batch start
#iterations: 340
currently lose_sum: 92.90075182914734
time_elpased: 1.937
batch start
#iterations: 341
currently lose_sum: 92.9779641032219
time_elpased: 1.918
batch start
#iterations: 342
currently lose_sum: 93.34919536113739
time_elpased: 1.955
batch start
#iterations: 343
currently lose_sum: 93.45192843675613
time_elpased: 1.962
batch start
#iterations: 344
currently lose_sum: 93.00237387418747
time_elpased: 1.921
batch start
#iterations: 345
currently lose_sum: 93.44401931762695
time_elpased: 1.994
batch start
#iterations: 346
currently lose_sum: 92.85133880376816
time_elpased: 2.005
batch start
#iterations: 347
currently lose_sum: 92.92432069778442
time_elpased: 1.93
batch start
#iterations: 348
currently lose_sum: 93.273835003376
time_elpased: 1.946
batch start
#iterations: 349
currently lose_sum: 92.71309024095535
time_elpased: 1.965
batch start
#iterations: 350
currently lose_sum: 93.09496879577637
time_elpased: 2.017
batch start
#iterations: 351
currently lose_sum: 93.03037440776825
time_elpased: 1.974
batch start
#iterations: 352
currently lose_sum: 92.70966708660126
time_elpased: 1.947
batch start
#iterations: 353
currently lose_sum: 93.17446690797806
time_elpased: 1.944
batch start
#iterations: 354
currently lose_sum: 92.8317278623581
time_elpased: 1.937
batch start
#iterations: 355
currently lose_sum: 93.08534723520279
time_elpased: 1.961
batch start
#iterations: 356
currently lose_sum: 92.94812494516373
time_elpased: 1.927
batch start
#iterations: 357
currently lose_sum: 92.77338844537735
time_elpased: 1.96
batch start
#iterations: 358
currently lose_sum: 92.86100071668625
time_elpased: 1.916
batch start
#iterations: 359
currently lose_sum: 92.76065927743912
time_elpased: 1.92
start validation test
0.58087628866
0.612738129087
0.443552536791
0.514596143514
0.581117381687
71.747
batch start
#iterations: 360
currently lose_sum: 93.03968435525894
time_elpased: 1.945
batch start
#iterations: 361
currently lose_sum: 92.84012925624847
time_elpased: 1.965
batch start
#iterations: 362
currently lose_sum: 92.70059180259705
time_elpased: 1.957
batch start
#iterations: 363
currently lose_sum: 92.72056722640991
time_elpased: 1.97
batch start
#iterations: 364
currently lose_sum: 92.48202741146088
time_elpased: 1.931
batch start
#iterations: 365
currently lose_sum: 92.52243793010712
time_elpased: 1.982
batch start
#iterations: 366
currently lose_sum: 92.68955314159393
time_elpased: 1.98
batch start
#iterations: 367
currently lose_sum: 92.55163049697876
time_elpased: 1.976
batch start
#iterations: 368
currently lose_sum: 92.7551698088646
time_elpased: 1.929
batch start
#iterations: 369
currently lose_sum: 92.06532007455826
time_elpased: 1.961
batch start
#iterations: 370
currently lose_sum: 92.5950573682785
time_elpased: 1.958
batch start
#iterations: 371
currently lose_sum: 92.63870334625244
time_elpased: 1.964
batch start
#iterations: 372
currently lose_sum: 92.29406386613846
time_elpased: 1.962
batch start
#iterations: 373
currently lose_sum: 92.59947389364243
time_elpased: 1.956
batch start
#iterations: 374
currently lose_sum: 92.55036473274231
time_elpased: 1.988
batch start
#iterations: 375
currently lose_sum: 92.69531774520874
time_elpased: 1.987
batch start
#iterations: 376
currently lose_sum: 92.19688892364502
time_elpased: 1.973
batch start
#iterations: 377
currently lose_sum: 92.34301388263702
time_elpased: 1.959
batch start
#iterations: 378
currently lose_sum: 92.62008863687515
time_elpased: 1.915
batch start
#iterations: 379
currently lose_sum: 92.43592339754105
time_elpased: 1.948
start validation test
0.580051546392
0.625760974047
0.401975918493
0.489504354909
0.580364185623
71.709
batch start
#iterations: 380
currently lose_sum: 92.29979330301285
time_elpased: 1.902
batch start
#iterations: 381
currently lose_sum: 91.95692253112793
time_elpased: 1.942
batch start
#iterations: 382
currently lose_sum: 92.31989634037018
time_elpased: 1.922
batch start
#iterations: 383
currently lose_sum: 92.29826802015305
time_elpased: 1.996
batch start
#iterations: 384
currently lose_sum: 92.47889429330826
time_elpased: 1.925
batch start
#iterations: 385
currently lose_sum: 92.09617030620575
time_elpased: 1.95
batch start
#iterations: 386
currently lose_sum: 92.54644614458084
time_elpased: 1.957
batch start
#iterations: 387
currently lose_sum: 92.23296356201172
time_elpased: 1.978
batch start
#iterations: 388
currently lose_sum: 92.24108332395554
time_elpased: 1.94
batch start
#iterations: 389
currently lose_sum: 92.23017597198486
time_elpased: 1.951
batch start
#iterations: 390
currently lose_sum: 92.84393393993378
time_elpased: 1.933
batch start
#iterations: 391
currently lose_sum: 92.79358875751495
time_elpased: 1.938
batch start
#iterations: 392
currently lose_sum: 91.92852628231049
time_elpased: 1.938
batch start
#iterations: 393
currently lose_sum: 91.85859829187393
time_elpased: 1.956
batch start
#iterations: 394
currently lose_sum: 92.52659785747528
time_elpased: 1.921
batch start
#iterations: 395
currently lose_sum: 91.97315174341202
time_elpased: 1.971
batch start
#iterations: 396
currently lose_sum: 92.40314221382141
time_elpased: 1.966
batch start
#iterations: 397
currently lose_sum: 92.21342647075653
time_elpased: 1.98
batch start
#iterations: 398
currently lose_sum: 91.75551456212997
time_elpased: 1.939
batch start
#iterations: 399
currently lose_sum: 92.35704696178436
time_elpased: 1.939
start validation test
0.575824742268
0.614921223355
0.409694350108
0.491754678525
0.576116409795
72.913
acc: 0.596
pre: 0.607
rec: 0.552
F1: 0.578
auc: 0.596
