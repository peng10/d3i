start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.42786145210266
time_elpased: 3.154
batch start
#iterations: 1
currently lose_sum: 100.31102216243744
time_elpased: 3.21
batch start
#iterations: 2
currently lose_sum: 100.25413888692856
time_elpased: 3.079
batch start
#iterations: 3
currently lose_sum: 100.13799434900284
time_elpased: 2.557
batch start
#iterations: 4
currently lose_sum: 100.22944390773773
time_elpased: 2.583
batch start
#iterations: 5
currently lose_sum: 99.93441730737686
time_elpased: 2.948
batch start
#iterations: 6
currently lose_sum: 99.84006136655807
time_elpased: 3.134
batch start
#iterations: 7
currently lose_sum: 99.85549956560135
time_elpased: 3.038
batch start
#iterations: 8
currently lose_sum: 99.94391393661499
time_elpased: 3.096
batch start
#iterations: 9
currently lose_sum: 99.70526903867722
time_elpased: 2.893
batch start
#iterations: 10
currently lose_sum: 99.81836092472076
time_elpased: 2.794
batch start
#iterations: 11
currently lose_sum: 99.72760426998138
time_elpased: 3.039
batch start
#iterations: 12
currently lose_sum: 99.68695598840714
time_elpased: 3.102
batch start
#iterations: 13
currently lose_sum: 99.60944747924805
time_elpased: 3.166
batch start
#iterations: 14
currently lose_sum: 99.55725461244583
time_elpased: 2.913
batch start
#iterations: 15
currently lose_sum: 99.65851956605911
time_elpased: 3.109
batch start
#iterations: 16
currently lose_sum: 99.50119334459305
time_elpased: 2.884
batch start
#iterations: 17
currently lose_sum: 99.48898684978485
time_elpased: 3.114
batch start
#iterations: 18
currently lose_sum: 99.39990746974945
time_elpased: 2.936
batch start
#iterations: 19
currently lose_sum: 99.47403228282928
time_elpased: 2.946
start validation test
0.586443298969
0.592730457631
0.55716785016
0.574399236115
0.586494696535
65.882
batch start
#iterations: 20
currently lose_sum: 99.43336033821106
time_elpased: 3.114
batch start
#iterations: 21
currently lose_sum: 99.1571273803711
time_elpased: 2.911
batch start
#iterations: 22
currently lose_sum: 99.35326319932938
time_elpased: 2.349
batch start
#iterations: 23
currently lose_sum: 99.11984050273895
time_elpased: 2.553
batch start
#iterations: 24
currently lose_sum: 99.3023607134819
time_elpased: 3.152
batch start
#iterations: 25
currently lose_sum: 99.09637701511383
time_elpased: 3.218
batch start
#iterations: 26
currently lose_sum: 99.19134891033173
time_elpased: 2.884
batch start
#iterations: 27
currently lose_sum: 99.10837197303772
time_elpased: 2.849
batch start
#iterations: 28
currently lose_sum: 99.10853523015976
time_elpased: 2.607
batch start
#iterations: 29
currently lose_sum: 99.13067734241486
time_elpased: 2.56
batch start
#iterations: 30
currently lose_sum: 99.08280056715012
time_elpased: 3.026
batch start
#iterations: 31
currently lose_sum: 98.9430320262909
time_elpased: 3.131
batch start
#iterations: 32
currently lose_sum: 99.05606615543365
time_elpased: 3.228
batch start
#iterations: 33
currently lose_sum: 98.89279901981354
time_elpased: 2.891
batch start
#iterations: 34
currently lose_sum: 98.69857484102249
time_elpased: 3.205
batch start
#iterations: 35
currently lose_sum: 99.01969027519226
time_elpased: 3.168
batch start
#iterations: 36
currently lose_sum: 98.93724817037582
time_elpased: 4.46
batch start
#iterations: 37
currently lose_sum: 98.76996266841888
time_elpased: 4.7
batch start
#iterations: 38
currently lose_sum: 98.5030437707901
time_elpased: 4.837
batch start
#iterations: 39
currently lose_sum: 98.98686182498932
time_elpased: 5.226
start validation test
0.594793814433
0.597458517118
0.58546876608
0.591402879568
0.594810185994
65.515
batch start
#iterations: 40
currently lose_sum: 98.81764608621597
time_elpased: 5.236
batch start
#iterations: 41
currently lose_sum: 98.71204215288162
time_elpased: 5.216
batch start
#iterations: 42
currently lose_sum: 98.65672445297241
time_elpased: 2.683
batch start
#iterations: 43
currently lose_sum: 98.75681191682816
time_elpased: 2.228
batch start
#iterations: 44
currently lose_sum: 98.60574746131897
time_elpased: 2.912
batch start
#iterations: 45
currently lose_sum: 98.54327094554901
time_elpased: 2.336
batch start
#iterations: 46
currently lose_sum: 98.5107558965683
time_elpased: 2.945
batch start
#iterations: 47
currently lose_sum: 98.63529741764069
time_elpased: 2.879
batch start
#iterations: 48
currently lose_sum: 98.5016393661499
time_elpased: 2.7
batch start
#iterations: 49
currently lose_sum: 98.48534083366394
time_elpased: 3.166
batch start
#iterations: 50
currently lose_sum: 98.42134636640549
time_elpased: 3.088
batch start
#iterations: 51
currently lose_sum: 98.5272244811058
time_elpased: 3.197
batch start
#iterations: 52
currently lose_sum: 98.63065522909164
time_elpased: 2.8
batch start
#iterations: 53
currently lose_sum: 98.6941659450531
time_elpased: 2.995
batch start
#iterations: 54
currently lose_sum: 98.5103730559349
time_elpased: 3.098
batch start
#iterations: 55
currently lose_sum: 98.21308290958405
time_elpased: 3.037
batch start
#iterations: 56
currently lose_sum: 98.32627874612808
time_elpased: 2.964
batch start
#iterations: 57
currently lose_sum: 98.33139532804489
time_elpased: 2.818
batch start
#iterations: 58
currently lose_sum: 98.11289280653
time_elpased: 3.193
batch start
#iterations: 59
currently lose_sum: 98.42267972230911
time_elpased: 3.206
start validation test
0.58912371134
0.621048252912
0.46094473603
0.52915116073
0.589348749302
66.028
batch start
#iterations: 60
currently lose_sum: 98.204356610775
time_elpased: 2.906
batch start
#iterations: 61
currently lose_sum: 98.44905656576157
time_elpased: 2.934
batch start
#iterations: 62
currently lose_sum: 98.11277568340302
time_elpased: 3.223
batch start
#iterations: 63
currently lose_sum: 98.17829096317291
time_elpased: 2.742
batch start
#iterations: 64
currently lose_sum: 98.35868990421295
time_elpased: 3.178
batch start
#iterations: 65
currently lose_sum: 97.77469217777252
time_elpased: 3.293
batch start
#iterations: 66
currently lose_sum: 98.07345592975616
time_elpased: 3.09
batch start
#iterations: 67
currently lose_sum: 97.85164278745651
time_elpased: 2.879
batch start
#iterations: 68
currently lose_sum: 97.99177664518356
time_elpased: 3.022
batch start
#iterations: 69
currently lose_sum: 98.05416983366013
time_elpased: 3.08
batch start
#iterations: 70
currently lose_sum: 97.64577579498291
time_elpased: 2.843
batch start
#iterations: 71
currently lose_sum: 97.73862314224243
time_elpased: 2.964
batch start
#iterations: 72
currently lose_sum: 97.81400638818741
time_elpased: 2.946
batch start
#iterations: 73
currently lose_sum: 97.59991294145584
time_elpased: 2.953
batch start
#iterations: 74
currently lose_sum: 97.80259019136429
time_elpased: 3.077
batch start
#iterations: 75
currently lose_sum: 97.81844580173492
time_elpased: 2.994
batch start
#iterations: 76
currently lose_sum: 97.25842386484146
time_elpased: 3.056
batch start
#iterations: 77
currently lose_sum: 97.81512254476547
time_elpased: 3.041
batch start
#iterations: 78
currently lose_sum: 97.58822798728943
time_elpased: 3.003
batch start
#iterations: 79
currently lose_sum: 97.39191371202469
time_elpased: 3.13
start validation test
0.593092783505
0.620425419474
0.483276731501
0.543329862316
0.593285582522
66.013
batch start
#iterations: 80
currently lose_sum: 97.64514780044556
time_elpased: 2.764
batch start
#iterations: 81
currently lose_sum: 97.37183600664139
time_elpased: 2.837
batch start
#iterations: 82
currently lose_sum: 97.65155911445618
time_elpased: 3.023
batch start
#iterations: 83
currently lose_sum: 97.27766114473343
time_elpased: 3.016
batch start
#iterations: 84
currently lose_sum: 97.79551088809967
time_elpased: 2.827
batch start
#iterations: 85
currently lose_sum: 97.382768034935
time_elpased: 2.986
batch start
#iterations: 86
currently lose_sum: 97.7018609046936
time_elpased: 3.048
batch start
#iterations: 87
currently lose_sum: 97.4316583275795
time_elpased: 3.017
batch start
#iterations: 88
currently lose_sum: 97.24844294786453
time_elpased: 3.103
batch start
#iterations: 89
currently lose_sum: 97.46494680643082
time_elpased: 2.809
batch start
#iterations: 90
currently lose_sum: 97.10665893554688
time_elpased: 2.807
batch start
#iterations: 91
currently lose_sum: 97.27960187196732
time_elpased: 3.113
batch start
#iterations: 92
currently lose_sum: 97.32497698068619
time_elpased: 2.635
batch start
#iterations: 93
currently lose_sum: 97.08032500743866
time_elpased: 3.063
batch start
#iterations: 94
currently lose_sum: 97.03951847553253
time_elpased: 3.042
batch start
#iterations: 95
currently lose_sum: 97.22350215911865
time_elpased: 3.039
batch start
#iterations: 96
currently lose_sum: 97.13899433612823
time_elpased: 2.184
batch start
#iterations: 97
currently lose_sum: 97.0816752910614
time_elpased: 2.631
batch start
#iterations: 98
currently lose_sum: 96.81921821832657
time_elpased: 2.919
batch start
#iterations: 99
currently lose_sum: 96.85377252101898
time_elpased: 2.349
start validation test
0.591546391753
0.614627285513
0.494700010291
0.548181092485
0.591716420513
65.884
batch start
#iterations: 100
currently lose_sum: 96.66637086868286
time_elpased: 3.183
batch start
#iterations: 101
currently lose_sum: 96.5475606918335
time_elpased: 3.019
batch start
#iterations: 102
currently lose_sum: 96.6811928153038
time_elpased: 2.933
batch start
#iterations: 103
currently lose_sum: 96.8474890589714
time_elpased: 2.998
batch start
#iterations: 104
currently lose_sum: 96.63824594020844
time_elpased: 2.975
batch start
#iterations: 105
currently lose_sum: 96.58442986011505
time_elpased: 2.654
batch start
#iterations: 106
currently lose_sum: 96.77558904886246
time_elpased: 2.923
batch start
#iterations: 107
currently lose_sum: 96.38778978586197
time_elpased: 2.938
batch start
#iterations: 108
currently lose_sum: 96.69878679513931
time_elpased: 3.009
batch start
#iterations: 109
currently lose_sum: 96.63384079933167
time_elpased: 3.116
batch start
#iterations: 110
currently lose_sum: 96.63006031513214
time_elpased: 3.065
batch start
#iterations: 111
currently lose_sum: 96.61107349395752
time_elpased: 2.983
batch start
#iterations: 112
currently lose_sum: 96.31099665164948
time_elpased: 2.758
batch start
#iterations: 113
currently lose_sum: 96.39809930324554
time_elpased: 2.724
batch start
#iterations: 114
currently lose_sum: 96.65312910079956
time_elpased: 2.391
batch start
#iterations: 115
currently lose_sum: 96.28068053722382
time_elpased: 2.654
batch start
#iterations: 116
currently lose_sum: 96.18555915355682
time_elpased: 2.706
batch start
#iterations: 117
currently lose_sum: 96.1175948381424
time_elpased: 2.816
batch start
#iterations: 118
currently lose_sum: 96.33719098567963
time_elpased: 2.862
batch start
#iterations: 119
currently lose_sum: 96.26779145002365
time_elpased: 2.849
start validation test
0.58175257732
0.617677286742
0.432952557374
0.509075508228
0.582013818705
67.341
batch start
#iterations: 120
currently lose_sum: 96.58146858215332
time_elpased: 2.625
batch start
#iterations: 121
currently lose_sum: 96.28644496202469
time_elpased: 2.406
batch start
#iterations: 122
currently lose_sum: 96.0137152671814
time_elpased: 2.726
batch start
#iterations: 123
currently lose_sum: 96.2754163146019
time_elpased: 2.896
batch start
#iterations: 124
currently lose_sum: 96.02397590875626
time_elpased: 2.896
batch start
#iterations: 125
currently lose_sum: 95.95909249782562
time_elpased: 2.922
batch start
#iterations: 126
currently lose_sum: 95.65935343503952
time_elpased: 2.419
batch start
#iterations: 127
currently lose_sum: 95.87482088804245
time_elpased: 3.065
batch start
#iterations: 128
currently lose_sum: 95.6695088148117
time_elpased: 3.337
batch start
#iterations: 129
currently lose_sum: 95.64338171482086
time_elpased: 3.022
batch start
#iterations: 130
currently lose_sum: 95.70438069105148
time_elpased: 3.207
batch start
#iterations: 131
currently lose_sum: 95.91255694627762
time_elpased: 3.107
batch start
#iterations: 132
currently lose_sum: 96.22009652853012
time_elpased: 2.523
batch start
#iterations: 133
currently lose_sum: 95.49957299232483
time_elpased: 2.33
batch start
#iterations: 134
currently lose_sum: 95.63702541589737
time_elpased: 2.641
batch start
#iterations: 135
currently lose_sum: 95.392558157444
time_elpased: 3.04
batch start
#iterations: 136
currently lose_sum: 95.43008410930634
time_elpased: 2.993
batch start
#iterations: 137
currently lose_sum: 95.5608429312706
time_elpased: 3.112
batch start
#iterations: 138
currently lose_sum: 95.34033799171448
time_elpased: 2.908
batch start
#iterations: 139
currently lose_sum: 95.15998619794846
time_elpased: 3.005
start validation test
0.583608247423
0.632497978981
0.402593393023
0.492013583197
0.583926046919
67.926
batch start
#iterations: 140
currently lose_sum: 95.42429852485657
time_elpased: 3.036
batch start
#iterations: 141
currently lose_sum: 95.20207047462463
time_elpased: 2.978
batch start
#iterations: 142
currently lose_sum: 95.61438351869583
time_elpased: 2.803
batch start
#iterations: 143
currently lose_sum: 95.12767285108566
time_elpased: 3.219
batch start
#iterations: 144
currently lose_sum: 95.10715979337692
time_elpased: 3.071
batch start
#iterations: 145
currently lose_sum: 95.10420191287994
time_elpased: 3.08
batch start
#iterations: 146
currently lose_sum: 95.05119574069977
time_elpased: 3.233
batch start
#iterations: 147
currently lose_sum: 95.03437167406082
time_elpased: 2.732
batch start
#iterations: 148
currently lose_sum: 94.9296709895134
time_elpased: 2.669
batch start
#iterations: 149
currently lose_sum: 95.28064674139023
time_elpased: 2.601
batch start
#iterations: 150
currently lose_sum: 94.97422111034393
time_elpased: 2.824
batch start
#iterations: 151
currently lose_sum: 94.60820174217224
time_elpased: 2.846
batch start
#iterations: 152
currently lose_sum: 94.80430632829666
time_elpased: 2.912
batch start
#iterations: 153
currently lose_sum: 94.97888386249542
time_elpased: 3.073
batch start
#iterations: 154
currently lose_sum: 94.74048912525177
time_elpased: 3.071
batch start
#iterations: 155
currently lose_sum: 94.81686264276505
time_elpased: 3.1
batch start
#iterations: 156
currently lose_sum: 94.98725765943527
time_elpased: 2.943
batch start
#iterations: 157
currently lose_sum: 94.99411696195602
time_elpased: 2.793
batch start
#iterations: 158
currently lose_sum: 94.44655632972717
time_elpased: 3.09
batch start
#iterations: 159
currently lose_sum: 94.649931371212
time_elpased: 2.601
start validation test
0.588453608247
0.608461634748
0.500257281054
0.54907940811
0.588608450503
68.232
batch start
#iterations: 160
currently lose_sum: 94.69469261169434
time_elpased: 2.595
batch start
#iterations: 161
currently lose_sum: 94.55999231338501
time_elpased: 2.443
batch start
#iterations: 162
currently lose_sum: 94.5061354637146
time_elpased: 2.305
batch start
#iterations: 163
currently lose_sum: 94.87278163433075
time_elpased: 2.895
batch start
#iterations: 164
currently lose_sum: 94.18459516763687
time_elpased: 3.17
batch start
#iterations: 165
currently lose_sum: 94.3263823390007
time_elpased: 3.246
batch start
#iterations: 166
currently lose_sum: 94.53168696165085
time_elpased: 3.289
batch start
#iterations: 167
currently lose_sum: 94.49047434329987
time_elpased: 2.526
batch start
#iterations: 168
currently lose_sum: 94.63233596086502
time_elpased: 2.279
batch start
#iterations: 169
currently lose_sum: 94.2667800784111
time_elpased: 2.071
batch start
#iterations: 170
currently lose_sum: 94.28061026334763
time_elpased: 2.253
batch start
#iterations: 171
currently lose_sum: 94.25087827444077
time_elpased: 2.807
batch start
#iterations: 172
currently lose_sum: 93.9877296090126
time_elpased: 3.158
batch start
#iterations: 173
currently lose_sum: 93.82980960607529
time_elpased: 2.979
batch start
#iterations: 174
currently lose_sum: 94.31347453594208
time_elpased: 3.078
batch start
#iterations: 175
currently lose_sum: 93.75687551498413
time_elpased: 2.93
batch start
#iterations: 176
currently lose_sum: 94.36896973848343
time_elpased: 3.038
batch start
#iterations: 177
currently lose_sum: 93.50614982843399
time_elpased: 3.178
batch start
#iterations: 178
currently lose_sum: 93.81772500276566
time_elpased: 3.089
batch start
#iterations: 179
currently lose_sum: 94.13730603456497
time_elpased: 3.033
start validation test
0.581443298969
0.634179129558
0.388391478851
0.481746234363
0.581782231215
69.799
batch start
#iterations: 180
currently lose_sum: 93.9677906036377
time_elpased: 3.056
batch start
#iterations: 181
currently lose_sum: 94.02441161870956
time_elpased: 3.012
batch start
#iterations: 182
currently lose_sum: 93.60709697008133
time_elpased: 3.199
batch start
#iterations: 183
currently lose_sum: 93.41944581270218
time_elpased: 3.077
batch start
#iterations: 184
currently lose_sum: 93.5316504240036
time_elpased: 3.157
batch start
#iterations: 185
currently lose_sum: 93.42739230394363
time_elpased: 3.103
batch start
#iterations: 186
currently lose_sum: 93.19926798343658
time_elpased: 2.845
batch start
#iterations: 187
currently lose_sum: 93.5928213596344
time_elpased: 2.844
batch start
#iterations: 188
currently lose_sum: 93.02931040525436
time_elpased: 2.82
batch start
#iterations: 189
currently lose_sum: 93.61044579744339
time_elpased: 3.059
batch start
#iterations: 190
currently lose_sum: 93.35030376911163
time_elpased: 2.907
batch start
#iterations: 191
currently lose_sum: 93.72622817754745
time_elpased: 3.078
batch start
#iterations: 192
currently lose_sum: 93.36784702539444
time_elpased: 3.1
batch start
#iterations: 193
currently lose_sum: 93.11415988206863
time_elpased: 2.847
batch start
#iterations: 194
currently lose_sum: 93.63999158143997
time_elpased: 2.608
batch start
#iterations: 195
currently lose_sum: 93.15593707561493
time_elpased: 3.137
batch start
#iterations: 196
currently lose_sum: 93.14245653152466
time_elpased: 2.977
batch start
#iterations: 197
currently lose_sum: 93.1411082148552
time_elpased: 3.27
batch start
#iterations: 198
currently lose_sum: 93.32640874385834
time_elpased: 3.3
batch start
#iterations: 199
currently lose_sum: 92.53148007392883
time_elpased: 3.343
start validation test
0.588298969072
0.621762387387
0.454564165895
0.525176862256
0.588533761146
70.584
batch start
#iterations: 200
currently lose_sum: 93.1660366654396
time_elpased: 2.724
batch start
#iterations: 201
currently lose_sum: 93.2287358045578
time_elpased: 2.892
batch start
#iterations: 202
currently lose_sum: 92.92292338609695
time_elpased: 3.019
batch start
#iterations: 203
currently lose_sum: 92.72693371772766
time_elpased: 3.089
batch start
#iterations: 204
currently lose_sum: 92.8417575955391
time_elpased: 2.995
batch start
#iterations: 205
currently lose_sum: 92.67173898220062
time_elpased: 2.943
batch start
#iterations: 206
currently lose_sum: 92.47350591421127
time_elpased: 3.084
batch start
#iterations: 207
currently lose_sum: 92.77606403827667
time_elpased: 2.993
batch start
#iterations: 208
currently lose_sum: 92.45094603300095
time_elpased: 3.145
batch start
#iterations: 209
currently lose_sum: 93.10169142484665
time_elpased: 2.835
batch start
#iterations: 210
currently lose_sum: 92.14773738384247
time_elpased: 3.068
batch start
#iterations: 211
currently lose_sum: 92.38474237918854
time_elpased: 3.195
batch start
#iterations: 212
currently lose_sum: 92.02852028608322
time_elpased: 3.384
batch start
#iterations: 213
currently lose_sum: 92.06265318393707
time_elpased: 3.168
batch start
#iterations: 214
currently lose_sum: 92.03734880685806
time_elpased: 3.037
batch start
#iterations: 215
currently lose_sum: 92.19909912347794
time_elpased: 3.073
batch start
#iterations: 216
currently lose_sum: 92.4015890955925
time_elpased: 2.899
batch start
#iterations: 217
currently lose_sum: 92.20481699705124
time_elpased: 3.132
batch start
#iterations: 218
currently lose_sum: 91.79250299930573
time_elpased: 2.851
batch start
#iterations: 219
currently lose_sum: 91.33236515522003
time_elpased: 2.841
start validation test
0.579896907216
0.630735858502
0.389008953381
0.481222151496
0.580232040462
74.582
batch start
#iterations: 220
currently lose_sum: 92.04256749153137
time_elpased: 3.037
batch start
#iterations: 221
currently lose_sum: 92.11278921365738
time_elpased: 3.098
batch start
#iterations: 222
currently lose_sum: 91.84756410121918
time_elpased: 3.297
batch start
#iterations: 223
currently lose_sum: 92.50740087032318
time_elpased: 2.831
batch start
#iterations: 224
currently lose_sum: 92.24975472688675
time_elpased: 3.028
batch start
#iterations: 225
currently lose_sum: 91.58933311700821
time_elpased: 2.913
batch start
#iterations: 226
currently lose_sum: 91.55530631542206
time_elpased: 2.936
batch start
#iterations: 227
currently lose_sum: 92.15228152275085
time_elpased: 3.085
batch start
#iterations: 228
currently lose_sum: 91.88620048761368
time_elpased: 3.397
batch start
#iterations: 229
currently lose_sum: 91.96034210920334
time_elpased: 3.479
batch start
#iterations: 230
currently lose_sum: 91.68763691186905
time_elpased: 3.951
batch start
#iterations: 231
currently lose_sum: 91.29872745275497
time_elpased: 3.352
batch start
#iterations: 232
currently lose_sum: 91.19596791267395
time_elpased: 2.753
batch start
#iterations: 233
currently lose_sum: 91.51697814464569
time_elpased: 3.03
batch start
#iterations: 234
currently lose_sum: 91.20899426937103
time_elpased: 3.214
batch start
#iterations: 235
currently lose_sum: 91.26468938589096
time_elpased: 3.057
batch start
#iterations: 236
currently lose_sum: 91.43532866239548
time_elpased: 3.005
batch start
#iterations: 237
currently lose_sum: 91.3526885509491
time_elpased: 3.13
batch start
#iterations: 238
currently lose_sum: 91.01087993383408
time_elpased: 2.849
batch start
#iterations: 239
currently lose_sum: 91.02307766675949
time_elpased: 2.885
start validation test
0.58381443299
0.63062490062
0.408150663785
0.495564163439
0.584122837831
73.929
batch start
#iterations: 240
currently lose_sum: 90.95509052276611
time_elpased: 3.283
batch start
#iterations: 241
currently lose_sum: 91.4473084807396
time_elpased: 2.999
batch start
#iterations: 242
currently lose_sum: 91.0900519490242
time_elpased: 2.787
batch start
#iterations: 243
currently lose_sum: 90.95711523294449
time_elpased: 2.698
batch start
#iterations: 244
currently lose_sum: 90.86076378822327
time_elpased: 3.243
batch start
#iterations: 245
currently lose_sum: 90.67217737436295
time_elpased: 3.073
batch start
#iterations: 246
currently lose_sum: 90.51061946153641
time_elpased: 2.833
batch start
#iterations: 247
currently lose_sum: 90.89067739248276
time_elpased: 3.482
batch start
#iterations: 248
currently lose_sum: 90.4568400979042
time_elpased: 3.163
batch start
#iterations: 249
currently lose_sum: 90.91947013139725
time_elpased: 2.996
batch start
#iterations: 250
currently lose_sum: 90.6048493385315
time_elpased: 2.892
batch start
#iterations: 251
currently lose_sum: 90.5237300992012
time_elpased: 2.851
batch start
#iterations: 252
currently lose_sum: 90.39405071735382
time_elpased: 3.051
batch start
#iterations: 253
currently lose_sum: 90.169260263443
time_elpased: 3.266
batch start
#iterations: 254
currently lose_sum: 90.38666081428528
time_elpased: 3.333
batch start
#iterations: 255
currently lose_sum: 89.91034895181656
time_elpased: 3.166
batch start
#iterations: 256
currently lose_sum: 90.5351979136467
time_elpased: 3.01
batch start
#iterations: 257
currently lose_sum: 90.64005368947983
time_elpased: 2.803
batch start
#iterations: 258
currently lose_sum: 89.69835311174393
time_elpased: 2.956
batch start
#iterations: 259
currently lose_sum: 89.89265394210815
time_elpased: 3.025
start validation test
0.577474226804
0.638736765243
0.360090562931
0.460546232313
0.577855877355
76.843
batch start
#iterations: 260
currently lose_sum: 90.37882667779922
time_elpased: 2.822
batch start
#iterations: 261
currently lose_sum: 90.41582977771759
time_elpased: 2.829
batch start
#iterations: 262
currently lose_sum: 89.72553533315659
time_elpased: 2.828
batch start
#iterations: 263
currently lose_sum: 89.97540748119354
time_elpased: 3.185
batch start
#iterations: 264
currently lose_sum: 90.4849020242691
time_elpased: 3.203
batch start
#iterations: 265
currently lose_sum: 89.72674876451492
time_elpased: 2.897
batch start
#iterations: 266
currently lose_sum: 89.72329342365265
time_elpased: 2.589
batch start
#iterations: 267
currently lose_sum: 90.0725868344307
time_elpased: 2.671
batch start
#iterations: 268
currently lose_sum: 89.8225828409195
time_elpased: 2.555
batch start
#iterations: 269
currently lose_sum: 89.90065217018127
time_elpased: 2.619
batch start
#iterations: 270
currently lose_sum: 89.56294512748718
time_elpased: 2.37
batch start
#iterations: 271
currently lose_sum: 89.46150320768356
time_elpased: 3.062
batch start
#iterations: 272
currently lose_sum: 89.81238222122192
time_elpased: 3.185
batch start
#iterations: 273
currently lose_sum: 89.7030018568039
time_elpased: 3.043
batch start
#iterations: 274
currently lose_sum: 89.22055345773697
time_elpased: 2.948
batch start
#iterations: 275
currently lose_sum: 89.52608925104141
time_elpased: 3.135
batch start
#iterations: 276
currently lose_sum: 89.1128968000412
time_elpased: 3.196
batch start
#iterations: 277
currently lose_sum: 88.98592209815979
time_elpased: 3.169
batch start
#iterations: 278
currently lose_sum: 89.10639202594757
time_elpased: 2.772
batch start
#iterations: 279
currently lose_sum: 88.83225953578949
time_elpased: 2.87
start validation test
0.57087628866
0.649100257069
0.311824637234
0.421272158498
0.57133109379
79.757
batch start
#iterations: 280
currently lose_sum: 89.62855273485184
time_elpased: 2.966
batch start
#iterations: 281
currently lose_sum: 89.31623321771622
time_elpased: 2.676
batch start
#iterations: 282
currently lose_sum: 88.98372280597687
time_elpased: 2.955
batch start
#iterations: 283
currently lose_sum: 88.69919109344482
time_elpased: 3.253
batch start
#iterations: 284
currently lose_sum: 89.03862059116364
time_elpased: 3.227
batch start
#iterations: 285
currently lose_sum: 88.67820316553116
time_elpased: 3.084
batch start
#iterations: 286
currently lose_sum: 89.21090352535248
time_elpased: 2.787
batch start
#iterations: 287
currently lose_sum: 88.26739692687988
time_elpased: 2.709
batch start
#iterations: 288
currently lose_sum: 88.48595553636551
time_elpased: 3.263
batch start
#iterations: 289
currently lose_sum: 88.58608937263489
time_elpased: 3.226
batch start
#iterations: 290
currently lose_sum: 88.85233813524246
time_elpased: 3.196
batch start
#iterations: 291
currently lose_sum: 88.2020497918129
time_elpased: 2.871
batch start
#iterations: 292
currently lose_sum: 89.14297223091125
time_elpased: 2.561
batch start
#iterations: 293
currently lose_sum: 88.79963821172714
time_elpased: 2.693
batch start
#iterations: 294
currently lose_sum: 88.16525101661682
time_elpased: 2.746
batch start
#iterations: 295
currently lose_sum: 88.68177860975266
time_elpased: 3.319
batch start
#iterations: 296
currently lose_sum: 87.68956327438354
time_elpased: 2.495
batch start
#iterations: 297
currently lose_sum: 88.35345321893692
time_elpased: 2.878
batch start
#iterations: 298
currently lose_sum: 88.62554067373276
time_elpased: 2.369
batch start
#iterations: 299
currently lose_sum: 87.18001747131348
time_elpased: 2.874
start validation test
0.581288659794
0.61966966967
0.424719563651
0.50399951151
0.581563540991
82.496
batch start
#iterations: 300
currently lose_sum: 87.84275609254837
time_elpased: 2.37
batch start
#iterations: 301
currently lose_sum: 87.75046563148499
time_elpased: 2.766
batch start
#iterations: 302
currently lose_sum: 87.66877126693726
time_elpased: 2.98
batch start
#iterations: 303
currently lose_sum: 88.44746607542038
time_elpased: 2.65
batch start
#iterations: 304
currently lose_sum: 87.84662163257599
time_elpased: 2.382
batch start
#iterations: 305
currently lose_sum: 88.0102368593216
time_elpased: 2.22
batch start
#iterations: 306
currently lose_sum: 88.18817591667175
time_elpased: 2.227
batch start
#iterations: 307
currently lose_sum: 87.88122254610062
time_elpased: 2.084
batch start
#iterations: 308
currently lose_sum: 87.15576869249344
time_elpased: 2.459
batch start
#iterations: 309
currently lose_sum: 87.62728309631348
time_elpased: 2.198
batch start
#iterations: 310
currently lose_sum: 87.36606913805008
time_elpased: 2.206
batch start
#iterations: 311
currently lose_sum: 87.83490407466888
time_elpased: 2.782
batch start
#iterations: 312
currently lose_sum: 87.29590159654617
time_elpased: 3.151
batch start
#iterations: 313
currently lose_sum: 87.34060454368591
time_elpased: 2.799
batch start
#iterations: 314
currently lose_sum: 86.94707006216049
time_elpased: 3.141
batch start
#iterations: 315
currently lose_sum: 87.00260388851166
time_elpased: 2.813
batch start
#iterations: 316
currently lose_sum: 87.53106862306595
time_elpased: 3.024
batch start
#iterations: 317
currently lose_sum: 87.02562123537064
time_elpased: 2.927
batch start
#iterations: 318
currently lose_sum: 86.75053453445435
time_elpased: 3.153
batch start
#iterations: 319
currently lose_sum: 86.80528926849365
time_elpased: 2.808
start validation test
0.575618556701
0.618190506531
0.399403107955
0.485276648953
0.575927930101
85.657
batch start
#iterations: 320
currently lose_sum: 86.71906417608261
time_elpased: 3.056
batch start
#iterations: 321
currently lose_sum: 87.66758912801743
time_elpased: 3.088
batch start
#iterations: 322
currently lose_sum: 86.68027848005295
time_elpased: 2.901
batch start
#iterations: 323
currently lose_sum: 87.07805097103119
time_elpased: 3.066
batch start
#iterations: 324
currently lose_sum: 86.60921555757523
time_elpased: 3.147
batch start
#iterations: 325
currently lose_sum: 87.26437777280807
time_elpased: 2.764
batch start
#iterations: 326
currently lose_sum: 87.71314203739166
time_elpased: 2.861
batch start
#iterations: 327
currently lose_sum: 86.85320115089417
time_elpased: 3.127
batch start
#iterations: 328
currently lose_sum: 87.06600350141525
time_elpased: 3.056
batch start
#iterations: 329
currently lose_sum: 86.85605877637863
time_elpased: 3.23
batch start
#iterations: 330
currently lose_sum: 86.99286633729935
time_elpased: 2.652
batch start
#iterations: 331
currently lose_sum: 86.82176500558853
time_elpased: 3.225
batch start
#iterations: 332
currently lose_sum: 86.20780539512634
time_elpased: 3.16
batch start
#iterations: 333
currently lose_sum: 86.49690872430801
time_elpased: 2.965
batch start
#iterations: 334
currently lose_sum: 86.83079928159714
time_elpased: 3.161
batch start
#iterations: 335
currently lose_sum: 85.78513485193253
time_elpased: 3.173
batch start
#iterations: 336
currently lose_sum: 86.33567583560944
time_elpased: 3.209
batch start
#iterations: 337
currently lose_sum: 86.09584486484528
time_elpased: 3.183
batch start
#iterations: 338
currently lose_sum: 86.21789425611496
time_elpased: 3.046
batch start
#iterations: 339
currently lose_sum: 86.36695408821106
time_elpased: 2.734
start validation test
0.572525773196
0.624171607953
0.368323556653
0.463270985697
0.572884281683
88.594
batch start
#iterations: 340
currently lose_sum: 86.30031335353851
time_elpased: 3.334
batch start
#iterations: 341
currently lose_sum: 85.99027395248413
time_elpased: 3.205
batch start
#iterations: 342
currently lose_sum: 86.24360227584839
time_elpased: 3.214
batch start
#iterations: 343
currently lose_sum: 86.81096291542053
time_elpased: 3.205
batch start
#iterations: 344
currently lose_sum: 86.15025389194489
time_elpased: 3.143
batch start
#iterations: 345
currently lose_sum: 86.28141939640045
time_elpased: 2.682
batch start
#iterations: 346
currently lose_sum: 86.17129588127136
time_elpased: 3.121
batch start
#iterations: 347
currently lose_sum: 86.45321440696716
time_elpased: 3.131
batch start
#iterations: 348
currently lose_sum: 85.90820962190628
time_elpased: 3.142
batch start
#iterations: 349
currently lose_sum: 84.94324910640717
time_elpased: 3.135
batch start
#iterations: 350
currently lose_sum: 86.15171706676483
time_elpased: 3.13
batch start
#iterations: 351
currently lose_sum: 85.86739242076874
time_elpased: 3.087
batch start
#iterations: 352
currently lose_sum: 85.00926381349564
time_elpased: 3.081
batch start
#iterations: 353
currently lose_sum: 85.24746018648148
time_elpased: 3.154
batch start
#iterations: 354
currently lose_sum: 85.72265094518661
time_elpased: 3.208
batch start
#iterations: 355
currently lose_sum: 85.74985682964325
time_elpased: 3.059
batch start
#iterations: 356
currently lose_sum: 85.5068307518959
time_elpased: 2.929
batch start
#iterations: 357
currently lose_sum: 85.47867846488953
time_elpased: 3.03
batch start
#iterations: 358
currently lose_sum: 85.04862332344055
time_elpased: 2.896
batch start
#iterations: 359
currently lose_sum: 85.43783164024353
time_elpased: 2.777
start validation test
0.575824742268
0.619575699132
0.396727384995
0.483719179371
0.576139175303
89.357
batch start
#iterations: 360
currently lose_sum: 85.57711589336395
time_elpased: 3.097
batch start
#iterations: 361
currently lose_sum: 85.7167734503746
time_elpased: 3.048
batch start
#iterations: 362
currently lose_sum: 85.79305320978165
time_elpased: 3.285
batch start
#iterations: 363
currently lose_sum: 84.76877456903458
time_elpased: 2.991
batch start
#iterations: 364
currently lose_sum: 85.05369246006012
time_elpased: 3.059
batch start
#iterations: 365
currently lose_sum: 85.2704513669014
time_elpased: 3.155
batch start
#iterations: 366
currently lose_sum: 84.65643829107285
time_elpased: 2.93
batch start
#iterations: 367
currently lose_sum: 85.46634566783905
time_elpased: 3.182
batch start
#iterations: 368
currently lose_sum: 84.70487529039383
time_elpased: 3.136
batch start
#iterations: 369
currently lose_sum: 84.28487831354141
time_elpased: 3.017
batch start
#iterations: 370
currently lose_sum: 84.32010418176651
time_elpased: 3.196
batch start
#iterations: 371
currently lose_sum: 84.68715959787369
time_elpased: 3.259
batch start
#iterations: 372
currently lose_sum: 84.73721051216125
time_elpased: 3.06
batch start
#iterations: 373
currently lose_sum: 85.01120364665985
time_elpased: 2.838
batch start
#iterations: 374
currently lose_sum: 84.72258740663528
time_elpased: 2.995
batch start
#iterations: 375
currently lose_sum: 84.83881056308746
time_elpased: 2.505
batch start
#iterations: 376
currently lose_sum: 84.10213339328766
time_elpased: 2.401
batch start
#iterations: 377
currently lose_sum: 84.00889319181442
time_elpased: 1.979
batch start
#iterations: 378
currently lose_sum: 85.02580815553665
time_elpased: 3.24
batch start
#iterations: 379
currently lose_sum: 84.60316467285156
time_elpased: 2.965
start validation test
0.56793814433
0.616228452029
0.364207059792
0.457826649418
0.568295825672
101.362
batch start
#iterations: 380
currently lose_sum: 84.48123919963837
time_elpased: 3.042
batch start
#iterations: 381
currently lose_sum: 85.81931626796722
time_elpased: 2.741
batch start
#iterations: 382
currently lose_sum: 83.84993010759354
time_elpased: 2.869
batch start
#iterations: 383
currently lose_sum: 84.59400528669357
time_elpased: 2.899
batch start
#iterations: 384
currently lose_sum: 84.38538086414337
time_elpased: 3.06
batch start
#iterations: 385
currently lose_sum: 84.23945850133896
time_elpased: 2.828
batch start
#iterations: 386
currently lose_sum: 84.14499253034592
time_elpased: 2.918
batch start
#iterations: 387
currently lose_sum: 83.98010551929474
time_elpased: 3.159
batch start
#iterations: 388
currently lose_sum: 84.57838159799576
time_elpased: 3.078
batch start
#iterations: 389
currently lose_sum: 84.26672780513763
time_elpased: 3.104
batch start
#iterations: 390
currently lose_sum: 84.60563707351685
time_elpased: 3.226
batch start
#iterations: 391
currently lose_sum: 83.67318278551102
time_elpased: 3.042
batch start
#iterations: 392
currently lose_sum: 84.28254926204681
time_elpased: 2.556
batch start
#iterations: 393
currently lose_sum: 84.76446825265884
time_elpased: 2.423
batch start
#iterations: 394
currently lose_sum: 83.44427573680878
time_elpased: 2.487
batch start
#iterations: 395
currently lose_sum: 83.91762316226959
time_elpased: 2.48
batch start
#iterations: 396
currently lose_sum: 83.73243057727814
time_elpased: 2.819
batch start
#iterations: 397
currently lose_sum: 84.0396648645401
time_elpased: 2.986
batch start
#iterations: 398
currently lose_sum: 83.98713952302933
time_elpased: 2.747
batch start
#iterations: 399
currently lose_sum: 84.87280911207199
time_elpased: 2.258
start validation test
0.570515463918
0.618517884648
0.371925491407
0.464524421594
0.570864119245
103.195
acc: 0.591
pre: 0.594
rec: 0.579
F1: 0.586
auc: 0.591
