start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.51653409004211
time_elpased: 2.06
batch start
#iterations: 1
currently lose_sum: 100.39469110965729
time_elpased: 1.995
batch start
#iterations: 2
currently lose_sum: 100.27464324235916
time_elpased: 2.001
batch start
#iterations: 3
currently lose_sum: 100.18580800294876
time_elpased: 1.936
batch start
#iterations: 4
currently lose_sum: 100.12195688486099
time_elpased: 1.976
batch start
#iterations: 5
currently lose_sum: 99.93137925863266
time_elpased: 1.992
batch start
#iterations: 6
currently lose_sum: 99.88774144649506
time_elpased: 2.005
batch start
#iterations: 7
currently lose_sum: 100.29739475250244
time_elpased: 1.972
batch start
#iterations: 8
currently lose_sum: 99.69187831878662
time_elpased: 1.956
batch start
#iterations: 9
currently lose_sum: 99.69052636623383
time_elpased: 2.009
batch start
#iterations: 10
currently lose_sum: 99.7396467924118
time_elpased: 1.987
batch start
#iterations: 11
currently lose_sum: 99.50357872247696
time_elpased: 1.996
batch start
#iterations: 12
currently lose_sum: 99.3868790268898
time_elpased: 1.979
batch start
#iterations: 13
currently lose_sum: 99.49431359767914
time_elpased: 2.01
batch start
#iterations: 14
currently lose_sum: 99.35112869739532
time_elpased: 1.94
batch start
#iterations: 15
currently lose_sum: 99.0806200504303
time_elpased: 2.008
batch start
#iterations: 16
currently lose_sum: 98.77762740850449
time_elpased: 2.031
batch start
#iterations: 17
currently lose_sum: 99.05111223459244
time_elpased: 2.003
batch start
#iterations: 18
currently lose_sum: 98.94862824678421
time_elpased: 1.948
batch start
#iterations: 19
currently lose_sum: 99.03948616981506
time_elpased: 2.024
start validation test
0.540618556701
0.650692624485
0.178861788618
0.280594123345
0.541253676505
66.664
batch start
#iterations: 20
currently lose_sum: 98.68079543113708
time_elpased: 2.007
batch start
#iterations: 21
currently lose_sum: 98.82893806695938
time_elpased: 1.954
batch start
#iterations: 22
currently lose_sum: 98.58635765314102
time_elpased: 1.945
batch start
#iterations: 23
currently lose_sum: 99.4790769815445
time_elpased: 2.001
batch start
#iterations: 24
currently lose_sum: 98.791439473629
time_elpased: 2.012
batch start
#iterations: 25
currently lose_sum: 100.13931155204773
time_elpased: 2.003
batch start
#iterations: 26
currently lose_sum: 98.43146592378616
time_elpased: 1.955
batch start
#iterations: 27
currently lose_sum: 98.98865008354187
time_elpased: 1.948
batch start
#iterations: 28
currently lose_sum: 98.31524044275284
time_elpased: 1.977
batch start
#iterations: 29
currently lose_sum: 98.93179899454117
time_elpased: 1.956
batch start
#iterations: 30
currently lose_sum: 98.75178289413452
time_elpased: 1.963
batch start
#iterations: 31
currently lose_sum: 98.41594189405441
time_elpased: 2.042
batch start
#iterations: 32
currently lose_sum: 98.45103645324707
time_elpased: 2.008
batch start
#iterations: 33
currently lose_sum: 99.22081005573273
time_elpased: 1.947
batch start
#iterations: 34
currently lose_sum: 97.88839495182037
time_elpased: 1.978
batch start
#iterations: 35
currently lose_sum: 99.52790987491608
time_elpased: 1.988
batch start
#iterations: 36
currently lose_sum: 99.98052841424942
time_elpased: 1.962
batch start
#iterations: 37
currently lose_sum: 100.07377916574478
time_elpased: 1.996
batch start
#iterations: 38
currently lose_sum: 98.40358746051788
time_elpased: 1.988
batch start
#iterations: 39
currently lose_sum: 98.52912992238998
time_elpased: 1.965
start validation test
0.500927835052
0.500902108356
1.0
0.667468058799
0.500051636889
67.228
batch start
#iterations: 40
currently lose_sum: 100.2005963921547
time_elpased: 1.988
batch start
#iterations: 41
currently lose_sum: 97.92365550994873
time_elpased: 1.948
batch start
#iterations: 42
currently lose_sum: 100.39377003908157
time_elpased: 1.966
batch start
#iterations: 43
currently lose_sum: 99.38719391822815
time_elpased: 1.965
batch start
#iterations: 44
currently lose_sum: 100.5315813422203
time_elpased: 2.027
batch start
#iterations: 45
currently lose_sum: 100.47776037454605
time_elpased: 1.947
batch start
#iterations: 46
currently lose_sum: 99.6085997223854
time_elpased: 1.985
batch start
#iterations: 47
currently lose_sum: 100.13459640741348
time_elpased: 1.954
batch start
#iterations: 48
currently lose_sum: 99.66275823116302
time_elpased: 1.992
batch start
#iterations: 49
currently lose_sum: 100.50538456439972
time_elpased: 1.997
batch start
#iterations: 50
currently lose_sum: 100.50479716062546
time_elpased: 2.013
batch start
#iterations: 51
currently lose_sum: 100.50410318374634
time_elpased: 1.999
batch start
#iterations: 52
currently lose_sum: 100.50228524208069
time_elpased: 1.988
batch start
#iterations: 53
currently lose_sum: 100.49956458806992
time_elpased: 1.977
batch start
#iterations: 54
currently lose_sum: 100.48110127449036
time_elpased: 1.938
batch start
#iterations: 55
currently lose_sum: 99.88485234975815
time_elpased: 2.011
batch start
#iterations: 56
currently lose_sum: 98.8274712562561
time_elpased: 1.971
batch start
#iterations: 57
currently lose_sum: 98.72047132253647
time_elpased: 1.996
batch start
#iterations: 58
currently lose_sum: 99.64098346233368
time_elpased: 1.955
batch start
#iterations: 59
currently lose_sum: 100.50081783533096
time_elpased: 2.0
start validation test
0.566134020619
0.56415317805
0.588247401461
0.575948410499
0.566095197168
67.227
batch start
#iterations: 60
currently lose_sum: 100.49218881130219
time_elpased: 1.981
batch start
#iterations: 61
currently lose_sum: 100.08857667446136
time_elpased: 1.953
batch start
#iterations: 62
currently lose_sum: 99.2187151312828
time_elpased: 1.994
batch start
#iterations: 63
currently lose_sum: 98.81161278486252
time_elpased: 2.024
batch start
#iterations: 64
currently lose_sum: 98.33011531829834
time_elpased: 2.006
batch start
#iterations: 65
currently lose_sum: 100.32700455188751
time_elpased: 1.967
batch start
#iterations: 66
currently lose_sum: 99.86501389741898
time_elpased: 2.025
batch start
#iterations: 67
currently lose_sum: 99.90077072381973
time_elpased: 1.99
batch start
#iterations: 68
currently lose_sum: 100.50502729415894
time_elpased: 1.978
batch start
#iterations: 69
currently lose_sum: 100.50469136238098
time_elpased: 1.993
batch start
#iterations: 70
currently lose_sum: 100.50414508581161
time_elpased: 2.04
batch start
#iterations: 71
currently lose_sum: 100.50244241952896
time_elpased: 1.95
batch start
#iterations: 72
currently lose_sum: 100.49925410747528
time_elpased: 2.006
batch start
#iterations: 73
currently lose_sum: 100.46591275930405
time_elpased: 1.965
batch start
#iterations: 74
currently lose_sum: 99.75559729337692
time_elpased: 1.972
batch start
#iterations: 75
currently lose_sum: 100.47641003131866
time_elpased: 2.026
batch start
#iterations: 76
currently lose_sum: 100.50619566440582
time_elpased: 1.992
batch start
#iterations: 77
currently lose_sum: 100.50618803501129
time_elpased: 1.978
batch start
#iterations: 78
currently lose_sum: 100.50619637966156
time_elpased: 1.999
batch start
#iterations: 79
currently lose_sum: 100.5061565041542
time_elpased: 2.0
start validation test
0.534639175258
0.528078898036
0.666769579088
0.589375056854
0.534407199954
67.235
batch start
#iterations: 80
currently lose_sum: 100.5061674118042
time_elpased: 1.997
batch start
#iterations: 81
currently lose_sum: 100.5061509013176
time_elpased: 1.984
batch start
#iterations: 82
currently lose_sum: 100.50610661506653
time_elpased: 1.982
batch start
#iterations: 83
currently lose_sum: 100.50608724355698
time_elpased: 1.96
batch start
#iterations: 84
currently lose_sum: 100.50614446401596
time_elpased: 1.986
batch start
#iterations: 85
currently lose_sum: 100.50605750083923
time_elpased: 2.021
batch start
#iterations: 86
currently lose_sum: 100.50610250234604
time_elpased: 2.003
batch start
#iterations: 87
currently lose_sum: 100.50602179765701
time_elpased: 1.97
batch start
#iterations: 88
currently lose_sum: 100.50599336624146
time_elpased: 2.011
batch start
#iterations: 89
currently lose_sum: 100.50592654943466
time_elpased: 1.961
batch start
#iterations: 90
currently lose_sum: 100.505843937397
time_elpased: 1.942
batch start
#iterations: 91
currently lose_sum: 100.5058331489563
time_elpased: 1.993
batch start
#iterations: 92
currently lose_sum: 100.50562047958374
time_elpased: 1.953
batch start
#iterations: 93
currently lose_sum: 100.50490760803223
time_elpased: 1.988
batch start
#iterations: 94
currently lose_sum: 100.50200170278549
time_elpased: 1.96
batch start
#iterations: 95
currently lose_sum: 100.48081237077713
time_elpased: 1.992
batch start
#iterations: 96
currently lose_sum: 100.04566943645477
time_elpased: 2.011
batch start
#iterations: 97
currently lose_sum: 99.94834172725677
time_elpased: 1.953
batch start
#iterations: 98
currently lose_sum: 100.50568997859955
time_elpased: 1.95
batch start
#iterations: 99
currently lose_sum: 100.5050521492958
time_elpased: 1.976
start validation test
0.550309278351
0.543381389253
0.640012349491
0.587751630281
0.550151790773
67.234
batch start
#iterations: 100
currently lose_sum: 100.50496274232864
time_elpased: 2.013
batch start
#iterations: 101
currently lose_sum: 100.50361746549606
time_elpased: 2.003
batch start
#iterations: 102
currently lose_sum: 100.50026214122772
time_elpased: 2.008
batch start
#iterations: 103
currently lose_sum: 100.56200164556503
time_elpased: 1.972
batch start
#iterations: 104
currently lose_sum: 100.53052085638046
time_elpased: 1.989
batch start
#iterations: 105
currently lose_sum: 100.49855625629425
time_elpased: 1.956
batch start
#iterations: 106
currently lose_sum: 100.45810097455978
time_elpased: 2.011
batch start
#iterations: 107
currently lose_sum: 99.64197391271591
time_elpased: 1.978
batch start
#iterations: 108
currently lose_sum: 100.14624184370041
time_elpased: 2.056
batch start
#iterations: 109
currently lose_sum: 100.48867011070251
time_elpased: 1.959
batch start
#iterations: 110
currently lose_sum: 99.77559494972229
time_elpased: 2.002
batch start
#iterations: 111
currently lose_sum: 100.50427109003067
time_elpased: 1.989
batch start
#iterations: 112
currently lose_sum: 100.49712193012238
time_elpased: 1.965
batch start
#iterations: 113
currently lose_sum: 100.10261732339859
time_elpased: 1.96
batch start
#iterations: 114
currently lose_sum: 99.22660672664642
time_elpased: 2.004
batch start
#iterations: 115
currently lose_sum: 99.3287907242775
time_elpased: 1.953
batch start
#iterations: 116
currently lose_sum: 100.4938235282898
time_elpased: 2.018
batch start
#iterations: 117
currently lose_sum: 99.61022746562958
time_elpased: 2.01
batch start
#iterations: 118
currently lose_sum: 98.07481813430786
time_elpased: 1.984
batch start
#iterations: 119
currently lose_sum: 98.96759247779846
time_elpased: 1.979
start validation test
0.504278350515
0.502591210614
0.998044663991
0.668527901286
0.503411467594
67.235
batch start
#iterations: 120
currently lose_sum: 98.3256396651268
time_elpased: 1.958
batch start
#iterations: 121
currently lose_sum: 98.0707288980484
time_elpased: 2.001
batch start
#iterations: 122
currently lose_sum: 97.97731268405914
time_elpased: 2.012
batch start
#iterations: 123
currently lose_sum: 97.92665600776672
time_elpased: 1.947
batch start
#iterations: 124
currently lose_sum: 98.10797947645187
time_elpased: 2.012
batch start
#iterations: 125
currently lose_sum: 98.36309438943863
time_elpased: 1.954
batch start
#iterations: 126
currently lose_sum: 97.60680997371674
time_elpased: 1.995
batch start
#iterations: 127
currently lose_sum: 98.1569893360138
time_elpased: 1.985
batch start
#iterations: 128
currently lose_sum: 97.39616012573242
time_elpased: 1.999
batch start
#iterations: 129
currently lose_sum: 99.82693839073181
time_elpased: 1.98
batch start
#iterations: 130
currently lose_sum: 99.80478763580322
time_elpased: 1.997
batch start
#iterations: 131
currently lose_sum: 97.40421664714813
time_elpased: 2.012
batch start
#iterations: 132
currently lose_sum: 97.12017607688904
time_elpased: 2.017
batch start
#iterations: 133
currently lose_sum: 96.91465443372726
time_elpased: 2.001
batch start
#iterations: 134
currently lose_sum: 100.03689402341843
time_elpased: 2.011
batch start
#iterations: 135
currently lose_sum: 100.50265455245972
time_elpased: 2.017
batch start
#iterations: 136
currently lose_sum: 100.48444855213165
time_elpased: 1.982
batch start
#iterations: 137
currently lose_sum: 98.36889278888702
time_elpased: 1.99
batch start
#iterations: 138
currently lose_sum: 97.61603856086731
time_elpased: 1.996
batch start
#iterations: 139
currently lose_sum: 98.69736331701279
time_elpased: 2.026
start validation test
0.65118556701
0.652565163426
0.649274467428
0.650915656435
0.65118892224
62.174
batch start
#iterations: 140
currently lose_sum: 97.26076012849808
time_elpased: 2.022
batch start
#iterations: 141
currently lose_sum: 96.92676341533661
time_elpased: 1.947
batch start
#iterations: 142
currently lose_sum: 96.8826385140419
time_elpased: 2.018
batch start
#iterations: 143
currently lose_sum: 96.97105234861374
time_elpased: 1.969
batch start
#iterations: 144
currently lose_sum: 97.05335783958435
time_elpased: 2.003
batch start
#iterations: 145
currently lose_sum: 97.19981944561005
time_elpased: 1.952
batch start
#iterations: 146
currently lose_sum: 97.20099365711212
time_elpased: 1.979
batch start
#iterations: 147
currently lose_sum: 96.98687118291855
time_elpased: 2.012
batch start
#iterations: 148
currently lose_sum: 97.309013068676
time_elpased: 1.981
batch start
#iterations: 149
currently lose_sum: 99.49296808242798
time_elpased: 1.998
batch start
#iterations: 150
currently lose_sum: 100.50617241859436
time_elpased: 2.001
batch start
#iterations: 151
currently lose_sum: 100.50612932443619
time_elpased: 1.978
batch start
#iterations: 152
currently lose_sum: 100.50606399774551
time_elpased: 1.96
batch start
#iterations: 153
currently lose_sum: 100.50593638420105
time_elpased: 1.955
batch start
#iterations: 154
currently lose_sum: 100.5058159828186
time_elpased: 1.974
batch start
#iterations: 155
currently lose_sum: 100.50543314218521
time_elpased: 1.924
batch start
#iterations: 156
currently lose_sum: 100.50461900234222
time_elpased: 1.943
batch start
#iterations: 157
currently lose_sum: 100.50047481060028
time_elpased: 1.969
batch start
#iterations: 158
currently lose_sum: 100.42694848775864
time_elpased: 1.99
batch start
#iterations: 159
currently lose_sum: 100.00449675321579
time_elpased: 1.965
start validation test
0.49912371134
0.0
0.0
nan
0.5
67.230
acc: 0.649
pre: 0.651
rec: 0.645
F1: 0.648
auc: 0.649
