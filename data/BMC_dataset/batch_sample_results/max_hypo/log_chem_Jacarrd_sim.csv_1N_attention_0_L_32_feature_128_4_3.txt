start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.49399793148041
time_elpased: 2.042
batch start
#iterations: 1
currently lose_sum: 100.36960697174072
time_elpased: 2.038
batch start
#iterations: 2
currently lose_sum: 100.30540382862091
time_elpased: 1.97
batch start
#iterations: 3
currently lose_sum: 100.21514612436295
time_elpased: 2.0
batch start
#iterations: 4
currently lose_sum: 100.15405011177063
time_elpased: 2.017
batch start
#iterations: 5
currently lose_sum: 100.10969084501266
time_elpased: 1.995
batch start
#iterations: 6
currently lose_sum: 100.04677772521973
time_elpased: 2.002
batch start
#iterations: 7
currently lose_sum: 100.02721440792084
time_elpased: 1.999
batch start
#iterations: 8
currently lose_sum: 99.74611842632294
time_elpased: 2.0
batch start
#iterations: 9
currently lose_sum: 99.63078665733337
time_elpased: 2.003
batch start
#iterations: 10
currently lose_sum: 99.6789830327034
time_elpased: 2.062
batch start
#iterations: 11
currently lose_sum: 99.36640858650208
time_elpased: 1.991
batch start
#iterations: 12
currently lose_sum: 99.43576830625534
time_elpased: 2.003
batch start
#iterations: 13
currently lose_sum: 99.3440397977829
time_elpased: 1.965
batch start
#iterations: 14
currently lose_sum: 99.54215556383133
time_elpased: 2.053
batch start
#iterations: 15
currently lose_sum: 99.16137158870697
time_elpased: 1.996
batch start
#iterations: 16
currently lose_sum: 99.32203781604767
time_elpased: 2.026
batch start
#iterations: 17
currently lose_sum: 99.22436678409576
time_elpased: 1.964
batch start
#iterations: 18
currently lose_sum: 99.23623222112656
time_elpased: 2.009
batch start
#iterations: 19
currently lose_sum: 98.93574750423431
time_elpased: 1.973
start validation test
0.620309278351
0.626398450947
0.59932070811
0.612560488113
0.620343955873
64.395
batch start
#iterations: 20
currently lose_sum: 99.02772837877274
time_elpased: 1.976
batch start
#iterations: 21
currently lose_sum: 99.00234031677246
time_elpased: 1.963
batch start
#iterations: 22
currently lose_sum: 98.85327589511871
time_elpased: 2.068
batch start
#iterations: 23
currently lose_sum: 98.82602888345718
time_elpased: 1.958
batch start
#iterations: 24
currently lose_sum: 98.67454373836517
time_elpased: 1.997
batch start
#iterations: 25
currently lose_sum: 98.61319774389267
time_elpased: 2.011
batch start
#iterations: 26
currently lose_sum: 98.8232810497284
time_elpased: 2.043
batch start
#iterations: 27
currently lose_sum: 98.91628873348236
time_elpased: 1.981
batch start
#iterations: 28
currently lose_sum: 98.42156457901001
time_elpased: 2.016
batch start
#iterations: 29
currently lose_sum: 98.65182065963745
time_elpased: 1.982
batch start
#iterations: 30
currently lose_sum: 98.37064868211746
time_elpased: 2.057
batch start
#iterations: 31
currently lose_sum: 98.65439200401306
time_elpased: 1.999
batch start
#iterations: 32
currently lose_sum: 98.83162933588028
time_elpased: 1.989
batch start
#iterations: 33
currently lose_sum: 98.62005317211151
time_elpased: 1.96
batch start
#iterations: 34
currently lose_sum: 97.92526710033417
time_elpased: 1.978
batch start
#iterations: 35
currently lose_sum: 98.56946122646332
time_elpased: 1.996
batch start
#iterations: 36
currently lose_sum: 99.81313353776932
time_elpased: 2.034
batch start
#iterations: 37
currently lose_sum: 98.60063236951828
time_elpased: 2.05
batch start
#iterations: 38
currently lose_sum: 99.08149236440659
time_elpased: 1.988
batch start
#iterations: 39
currently lose_sum: 97.68175411224365
time_elpased: 2.04
start validation test
0.643711340206
0.634419942474
0.681041580897
0.656904596446
0.643649662816
62.542
batch start
#iterations: 40
currently lose_sum: 100.14337009191513
time_elpased: 1.957
batch start
#iterations: 41
currently lose_sum: 98.46112996339798
time_elpased: 2.002
batch start
#iterations: 42
currently lose_sum: 98.02924889326096
time_elpased: 1.971
batch start
#iterations: 43
currently lose_sum: 98.35623836517334
time_elpased: 1.991
batch start
#iterations: 44
currently lose_sum: 99.44069874286652
time_elpased: 1.961
batch start
#iterations: 45
currently lose_sum: 98.37365114688873
time_elpased: 1.968
batch start
#iterations: 46
currently lose_sum: 98.6919459104538
time_elpased: 1.987
batch start
#iterations: 47
currently lose_sum: 99.1546026468277
time_elpased: 1.977
batch start
#iterations: 48
currently lose_sum: 98.29498642683029
time_elpased: 2.0
batch start
#iterations: 49
currently lose_sum: 98.53215754032135
time_elpased: 2.008
batch start
#iterations: 50
currently lose_sum: 97.45270329713821
time_elpased: 1.969
batch start
#iterations: 51
currently lose_sum: 97.9802395105362
time_elpased: 2.035
batch start
#iterations: 52
currently lose_sum: 98.0809885263443
time_elpased: 1.987
batch start
#iterations: 53
currently lose_sum: 98.94418132305145
time_elpased: 1.99
batch start
#iterations: 54
currently lose_sum: 97.61779284477234
time_elpased: 1.982
batch start
#iterations: 55
currently lose_sum: 97.70033645629883
time_elpased: 1.979
batch start
#iterations: 56
currently lose_sum: 99.19917237758636
time_elpased: 1.962
batch start
#iterations: 57
currently lose_sum: 100.3495683670044
time_elpased: 1.968
batch start
#iterations: 58
currently lose_sum: 98.06234610080719
time_elpased: 2.037
batch start
#iterations: 59
currently lose_sum: 98.90823847055435
time_elpased: 2.037
start validation test
0.649381443299
0.624850042845
0.750514615068
0.681941457028
0.649214350078
62.651
batch start
#iterations: 60
currently lose_sum: 100.22259384393692
time_elpased: 2.025
batch start
#iterations: 61
currently lose_sum: 100.50046813488007
time_elpased: 1.988
batch start
#iterations: 62
currently lose_sum: 100.48262739181519
time_elpased: 2.063
batch start
#iterations: 63
currently lose_sum: 99.70744609832764
time_elpased: 2.028
batch start
#iterations: 64
currently lose_sum: 98.24259096384048
time_elpased: 1.969
batch start
#iterations: 65
currently lose_sum: 98.02499896287918
time_elpased: 1.982
batch start
#iterations: 66
currently lose_sum: 98.5661570429802
time_elpased: 1.999
batch start
#iterations: 67
currently lose_sum: 100.14372617006302
time_elpased: 1.975
batch start
#iterations: 68
currently lose_sum: 100.46592080593109
time_elpased: 1.982
batch start
#iterations: 69
currently lose_sum: 98.79603779315948
time_elpased: 1.999
batch start
#iterations: 70
currently lose_sum: 100.5185518860817
time_elpased: 1.981
batch start
#iterations: 71
currently lose_sum: 100.50557696819305
time_elpased: 1.986
batch start
#iterations: 72
currently lose_sum: 100.50516426563263
time_elpased: 1.993
batch start
#iterations: 73
currently lose_sum: 100.50480610132217
time_elpased: 1.979
batch start
#iterations: 74
currently lose_sum: 100.50415033102036
time_elpased: 1.983
batch start
#iterations: 75
currently lose_sum: 100.50355339050293
time_elpased: 2.005
batch start
#iterations: 76
currently lose_sum: 100.50089758634567
time_elpased: 2.026
batch start
#iterations: 77
currently lose_sum: 100.49154847860336
time_elpased: 1.968
batch start
#iterations: 78
currently lose_sum: 99.77379494905472
time_elpased: 1.956
batch start
#iterations: 79
currently lose_sum: 100.52268958091736
time_elpased: 1.98
start validation test
0.499175257732
0.0
0.0
nan
0.5
67.240
acc: 0.648
pre: 0.639
rec: 0.682
F1: 0.660
auc: 0.648
