start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.64796775579453
time_elpased: 1.399
batch start
#iterations: 1
currently lose_sum: 100.41243118047714
time_elpased: 1.366
batch start
#iterations: 2
currently lose_sum: 100.31849032640457
time_elpased: 1.401
batch start
#iterations: 3
currently lose_sum: 100.24357980489731
time_elpased: 1.397
batch start
#iterations: 4
currently lose_sum: 100.21482044458389
time_elpased: 1.354
batch start
#iterations: 5
currently lose_sum: 100.188352227211
time_elpased: 1.373
batch start
#iterations: 6
currently lose_sum: 100.01321297883987
time_elpased: 1.36
batch start
#iterations: 7
currently lose_sum: 99.9408865571022
time_elpased: 1.372
batch start
#iterations: 8
currently lose_sum: 99.92104148864746
time_elpased: 1.363
batch start
#iterations: 9
currently lose_sum: 99.89106529951096
time_elpased: 1.356
batch start
#iterations: 10
currently lose_sum: 99.76734834909439
time_elpased: 1.386
batch start
#iterations: 11
currently lose_sum: 99.6611539721489
time_elpased: 1.36
batch start
#iterations: 12
currently lose_sum: 99.50317823886871
time_elpased: 1.419
batch start
#iterations: 13
currently lose_sum: 99.50261133909225
time_elpased: 1.399
batch start
#iterations: 14
currently lose_sum: 99.11011105775833
time_elpased: 1.407
batch start
#iterations: 15
currently lose_sum: 99.0807518362999
time_elpased: 1.375
batch start
#iterations: 16
currently lose_sum: 98.98452299833298
time_elpased: 1.364
batch start
#iterations: 17
currently lose_sum: 98.98711758852005
time_elpased: 1.391
batch start
#iterations: 18
currently lose_sum: 98.95140773057938
time_elpased: 1.353
batch start
#iterations: 19
currently lose_sum: 98.62735223770142
time_elpased: 1.404
start validation test
0.554536082474
0.661942098914
0.225916014821
0.3368631062
0.555079031781
65.623
batch start
#iterations: 20
currently lose_sum: 98.86817944049835
time_elpased: 1.359
batch start
#iterations: 21
currently lose_sum: 98.51999551057816
time_elpased: 1.361
batch start
#iterations: 22
currently lose_sum: 98.43721961975098
time_elpased: 1.412
batch start
#iterations: 23
currently lose_sum: 98.50342571735382
time_elpased: 1.351
batch start
#iterations: 24
currently lose_sum: 98.41125631332397
time_elpased: 1.369
batch start
#iterations: 25
currently lose_sum: 98.14184445142746
time_elpased: 1.367
batch start
#iterations: 26
currently lose_sum: 98.21423035860062
time_elpased: 1.401
batch start
#iterations: 27
currently lose_sum: 97.96843218803406
time_elpased: 1.378
batch start
#iterations: 28
currently lose_sum: 98.19420582056046
time_elpased: 1.397
batch start
#iterations: 29
currently lose_sum: 98.2117275595665
time_elpased: 1.358
batch start
#iterations: 30
currently lose_sum: 98.10683250427246
time_elpased: 1.355
batch start
#iterations: 31
currently lose_sum: 97.77726626396179
time_elpased: 1.378
batch start
#iterations: 32
currently lose_sum: 97.77352571487427
time_elpased: 1.37
batch start
#iterations: 33
currently lose_sum: 97.6549506187439
time_elpased: 1.377
batch start
#iterations: 34
currently lose_sum: 97.67074382305145
time_elpased: 1.361
batch start
#iterations: 35
currently lose_sum: 97.73759377002716
time_elpased: 1.375
batch start
#iterations: 36
currently lose_sum: 97.66279685497284
time_elpased: 1.395
batch start
#iterations: 37
currently lose_sum: 97.66571801900864
time_elpased: 1.358
batch start
#iterations: 38
currently lose_sum: 97.58596813678741
time_elpased: 1.362
batch start
#iterations: 39
currently lose_sum: 97.5312220454216
time_elpased: 1.378
start validation test
0.614329896907
0.642401835798
0.518629065459
0.573917995444
0.614488014762
63.622
batch start
#iterations: 40
currently lose_sum: 97.39600360393524
time_elpased: 1.423
batch start
#iterations: 41
currently lose_sum: 97.52501666545868
time_elpased: 1.356
batch start
#iterations: 42
currently lose_sum: 97.27589863538742
time_elpased: 1.349
batch start
#iterations: 43
currently lose_sum: 97.07499134540558
time_elpased: 1.374
batch start
#iterations: 44
currently lose_sum: 97.10186040401459
time_elpased: 1.352
batch start
#iterations: 45
currently lose_sum: 97.25897717475891
time_elpased: 1.374
batch start
#iterations: 46
currently lose_sum: 97.04054069519043
time_elpased: 1.383
batch start
#iterations: 47
currently lose_sum: 97.1044591665268
time_elpased: 1.456
batch start
#iterations: 48
currently lose_sum: 97.13263064622879
time_elpased: 1.373
batch start
#iterations: 49
currently lose_sum: 97.31103682518005
time_elpased: 1.368
batch start
#iterations: 50
currently lose_sum: 96.93603450059891
time_elpased: 1.388
batch start
#iterations: 51
currently lose_sum: 97.08555763959885
time_elpased: 1.376
batch start
#iterations: 52
currently lose_sum: 96.83688217401505
time_elpased: 1.366
batch start
#iterations: 53
currently lose_sum: 96.56914329528809
time_elpased: 1.356
batch start
#iterations: 54
currently lose_sum: 97.21564465761185
time_elpased: 1.399
batch start
#iterations: 55
currently lose_sum: 96.91495406627655
time_elpased: 1.377
batch start
#iterations: 56
currently lose_sum: 97.38591378927231
time_elpased: 1.419
batch start
#iterations: 57
currently lose_sum: 96.71538865566254
time_elpased: 1.369
batch start
#iterations: 58
currently lose_sum: 96.64722263813019
time_elpased: 1.358
batch start
#iterations: 59
currently lose_sum: 96.8876930475235
time_elpased: 1.368
start validation test
0.63381443299
0.657160048135
0.562062577192
0.605902585155
0.633932982111
62.309
batch start
#iterations: 60
currently lose_sum: 96.56246137619019
time_elpased: 1.366
batch start
#iterations: 61
currently lose_sum: 96.90932148694992
time_elpased: 1.37
batch start
#iterations: 62
currently lose_sum: 97.04583263397217
time_elpased: 1.398
batch start
#iterations: 63
currently lose_sum: 96.97373121976852
time_elpased: 1.368
batch start
#iterations: 64
currently lose_sum: 96.89215224981308
time_elpased: 1.384
batch start
#iterations: 65
currently lose_sum: 96.75087368488312
time_elpased: 1.37
batch start
#iterations: 66
currently lose_sum: 96.79359531402588
time_elpased: 1.384
batch start
#iterations: 67
currently lose_sum: 96.64335525035858
time_elpased: 1.371
batch start
#iterations: 68
currently lose_sum: 96.46779412031174
time_elpased: 1.358
batch start
#iterations: 69
currently lose_sum: 96.32044553756714
time_elpased: 1.393
batch start
#iterations: 70
currently lose_sum: 96.367580473423
time_elpased: 1.364
batch start
#iterations: 71
currently lose_sum: 96.52547681331635
time_elpased: 1.379
batch start
#iterations: 72
currently lose_sum: 96.68062627315521
time_elpased: 1.435
batch start
#iterations: 73
currently lose_sum: 96.32828968763351
time_elpased: 1.373
batch start
#iterations: 74
currently lose_sum: 96.06610023975372
time_elpased: 1.395
batch start
#iterations: 75
currently lose_sum: 96.25101095438004
time_elpased: 1.384
batch start
#iterations: 76
currently lose_sum: 96.27563923597336
time_elpased: 1.398
batch start
#iterations: 77
currently lose_sum: 96.10607504844666
time_elpased: 1.379
batch start
#iterations: 78
currently lose_sum: 96.3340744972229
time_elpased: 1.41
batch start
#iterations: 79
currently lose_sum: 96.28778719902039
time_elpased: 1.368
start validation test
0.605824742268
0.672273105745
0.415500205846
0.513580561033
0.606139198338
62.787
batch start
#iterations: 80
currently lose_sum: 96.09238749742508
time_elpased: 1.373
batch start
#iterations: 81
currently lose_sum: 96.54119116067886
time_elpased: 1.379
batch start
#iterations: 82
currently lose_sum: 96.02568608522415
time_elpased: 1.381
batch start
#iterations: 83
currently lose_sum: 96.04380106925964
time_elpased: 1.379
batch start
#iterations: 84
currently lose_sum: 95.76304543018341
time_elpased: 1.375
batch start
#iterations: 85
currently lose_sum: 95.71001607179642
time_elpased: 1.401
batch start
#iterations: 86
currently lose_sum: 95.93844050168991
time_elpased: 1.387
batch start
#iterations: 87
currently lose_sum: 96.09600573778152
time_elpased: 1.392
batch start
#iterations: 88
currently lose_sum: 95.54526799917221
time_elpased: 1.443
batch start
#iterations: 89
currently lose_sum: 95.95791643857956
time_elpased: 1.432
batch start
#iterations: 90
currently lose_sum: 95.79372942447662
time_elpased: 1.367
batch start
#iterations: 91
currently lose_sum: 95.87565022706985
time_elpased: 1.368
batch start
#iterations: 92
currently lose_sum: 96.25972747802734
time_elpased: 1.374
batch start
#iterations: 93
currently lose_sum: 95.68521976470947
time_elpased: 1.374
batch start
#iterations: 94
currently lose_sum: 96.1975349187851
time_elpased: 1.384
batch start
#iterations: 95
currently lose_sum: 95.85668164491653
time_elpased: 1.393
batch start
#iterations: 96
currently lose_sum: 95.65258407592773
time_elpased: 1.356
batch start
#iterations: 97
currently lose_sum: 95.68080121278763
time_elpased: 1.354
batch start
#iterations: 98
currently lose_sum: 96.03590619564056
time_elpased: 1.38
batch start
#iterations: 99
currently lose_sum: 95.43565464019775
time_elpased: 1.394
start validation test
0.663505154639
0.640292202077
0.748764923837
0.690293196698
0.66336428761
61.438
batch start
#iterations: 100
currently lose_sum: 96.05735009908676
time_elpased: 1.38
batch start
#iterations: 101
currently lose_sum: 95.69401180744171
time_elpased: 1.387
batch start
#iterations: 102
currently lose_sum: 95.9489820599556
time_elpased: 1.377
batch start
#iterations: 103
currently lose_sum: 96.17418396472931
time_elpased: 1.348
batch start
#iterations: 104
currently lose_sum: 96.0042382478714
time_elpased: 1.388
batch start
#iterations: 105
currently lose_sum: 95.51252794265747
time_elpased: 1.383
batch start
#iterations: 106
currently lose_sum: 95.85335725545883
time_elpased: 1.367
batch start
#iterations: 107
currently lose_sum: 95.65332943201065
time_elpased: 1.439
batch start
#iterations: 108
currently lose_sum: 95.50338000059128
time_elpased: 1.398
batch start
#iterations: 109
currently lose_sum: 95.59690713882446
time_elpased: 1.356
batch start
#iterations: 110
currently lose_sum: 96.07906341552734
time_elpased: 1.363
batch start
#iterations: 111
currently lose_sum: 95.28574526309967
time_elpased: 1.381
batch start
#iterations: 112
currently lose_sum: 95.6117314696312
time_elpased: 1.392
batch start
#iterations: 113
currently lose_sum: 95.31604170799255
time_elpased: 1.393
batch start
#iterations: 114
currently lose_sum: 95.71960014104843
time_elpased: 1.403
batch start
#iterations: 115
currently lose_sum: 95.34994453191757
time_elpased: 1.355
batch start
#iterations: 116
currently lose_sum: 95.7986763715744
time_elpased: 1.385
batch start
#iterations: 117
currently lose_sum: 94.94145822525024
time_elpased: 1.345
batch start
#iterations: 118
currently lose_sum: 95.43118327856064
time_elpased: 1.405
batch start
#iterations: 119
currently lose_sum: 95.73878556489944
time_elpased: 1.361
start validation test
0.639381443299
0.594235033259
0.882667764512
0.710286566175
0.638979483247
62.972
batch start
#iterations: 120
currently lose_sum: 95.55099356174469
time_elpased: 1.367
batch start
#iterations: 121
currently lose_sum: 95.21061623096466
time_elpased: 1.387
batch start
#iterations: 122
currently lose_sum: 95.45445895195007
time_elpased: 1.376
batch start
#iterations: 123
currently lose_sum: 95.56359845399857
time_elpased: 1.396
batch start
#iterations: 124
currently lose_sum: 95.57615256309509
time_elpased: 1.38
batch start
#iterations: 125
currently lose_sum: 95.1266416311264
time_elpased: 1.378
batch start
#iterations: 126
currently lose_sum: 94.72037029266357
time_elpased: 1.412
batch start
#iterations: 127
currently lose_sum: 95.39956295490265
time_elpased: 1.387
batch start
#iterations: 128
currently lose_sum: 95.49799090623856
time_elpased: 1.382
batch start
#iterations: 129
currently lose_sum: 95.29997909069061
time_elpased: 1.413
batch start
#iterations: 130
currently lose_sum: 95.53887021541595
time_elpased: 1.353
batch start
#iterations: 131
currently lose_sum: 95.82772970199585
time_elpased: 1.37
batch start
#iterations: 132
currently lose_sum: 95.22567284107208
time_elpased: 1.406
batch start
#iterations: 133
currently lose_sum: 95.05762660503387
time_elpased: 1.365
batch start
#iterations: 134
currently lose_sum: 95.00399154424667
time_elpased: 1.376
batch start
#iterations: 135
currently lose_sum: 96.10303419828415
time_elpased: 1.393
batch start
#iterations: 136
currently lose_sum: 95.64869183301926
time_elpased: 1.378
batch start
#iterations: 137
currently lose_sum: 95.27094846963882
time_elpased: 1.371
batch start
#iterations: 138
currently lose_sum: 95.44497269392014
time_elpased: 1.373
batch start
#iterations: 139
currently lose_sum: 95.02092456817627
time_elpased: 1.416
start validation test
0.664226804124
0.626082847693
0.818237958007
0.709378067279
0.663972345381
60.626
batch start
#iterations: 140
currently lose_sum: 94.95907336473465
time_elpased: 1.421
batch start
#iterations: 141
currently lose_sum: 94.75402301549911
time_elpased: 1.367
batch start
#iterations: 142
currently lose_sum: 95.52754771709442
time_elpased: 1.365
batch start
#iterations: 143
currently lose_sum: 95.03648638725281
time_elpased: 1.372
batch start
#iterations: 144
currently lose_sum: 95.02077567577362
time_elpased: 1.357
batch start
#iterations: 145
currently lose_sum: 95.47648459672928
time_elpased: 1.378
batch start
#iterations: 146
currently lose_sum: 94.89625436067581
time_elpased: 1.377
batch start
#iterations: 147
currently lose_sum: 95.1585858464241
time_elpased: 1.372
batch start
#iterations: 148
currently lose_sum: 94.93450319766998
time_elpased: 1.396
batch start
#iterations: 149
currently lose_sum: 94.84447884559631
time_elpased: 1.361
batch start
#iterations: 150
currently lose_sum: 95.22955644130707
time_elpased: 1.364
batch start
#iterations: 151
currently lose_sum: 95.30579781532288
time_elpased: 1.353
batch start
#iterations: 152
currently lose_sum: 95.80167365074158
time_elpased: 1.373
batch start
#iterations: 153
currently lose_sum: 94.70934450626373
time_elpased: 1.366
batch start
#iterations: 154
currently lose_sum: 95.09693562984467
time_elpased: 1.402
batch start
#iterations: 155
currently lose_sum: 95.47634601593018
time_elpased: 1.38
batch start
#iterations: 156
currently lose_sum: 94.86742198467255
time_elpased: 1.377
batch start
#iterations: 157
currently lose_sum: 95.01917523145676
time_elpased: 1.363
batch start
#iterations: 158
currently lose_sum: 95.06542146205902
time_elpased: 1.371
batch start
#iterations: 159
currently lose_sum: 95.6593526005745
time_elpased: 1.362
start validation test
0.637474226804
0.589666466145
0.907986825854
0.714997771204
0.637027283229
61.995
batch start
#iterations: 160
currently lose_sum: 95.78107982873917
time_elpased: 1.472
batch start
#iterations: 161
currently lose_sum: 94.93627619743347
time_elpased: 1.348
batch start
#iterations: 162
currently lose_sum: 95.47488522529602
time_elpased: 1.376
batch start
#iterations: 163
currently lose_sum: 94.84546530246735
time_elpased: 1.356
batch start
#iterations: 164
currently lose_sum: 95.01272755861282
time_elpased: 1.375
batch start
#iterations: 165
currently lose_sum: 95.36556071043015
time_elpased: 1.361
batch start
#iterations: 166
currently lose_sum: 95.87784945964813
time_elpased: 1.41
batch start
#iterations: 167
currently lose_sum: 94.88935106992722
time_elpased: 1.351
batch start
#iterations: 168
currently lose_sum: 95.48286473751068
time_elpased: 1.383
batch start
#iterations: 169
currently lose_sum: 94.86788272857666
time_elpased: 1.395
batch start
#iterations: 170
currently lose_sum: 95.55662328004837
time_elpased: 1.39
batch start
#iterations: 171
currently lose_sum: 94.95362037420273
time_elpased: 1.384
batch start
#iterations: 172
currently lose_sum: 94.51472592353821
time_elpased: 1.401
batch start
#iterations: 173
currently lose_sum: 95.04775267839432
time_elpased: 1.384
batch start
#iterations: 174
currently lose_sum: 94.98617547750473
time_elpased: 1.392
batch start
#iterations: 175
currently lose_sum: 94.82813328504562
time_elpased: 1.374
batch start
#iterations: 176
currently lose_sum: 95.79619431495667
time_elpased: 1.395
batch start
#iterations: 177
currently lose_sum: 95.00667095184326
time_elpased: 1.394
batch start
#iterations: 178
currently lose_sum: 94.77010196447372
time_elpased: 1.388
batch start
#iterations: 179
currently lose_sum: 94.46034622192383
time_elpased: 1.41
start validation test
0.665
0.659367878728
0.684952655414
0.671916805492
0.664967034027
60.307
batch start
#iterations: 180
currently lose_sum: 94.29311633110046
time_elpased: 1.374
batch start
#iterations: 181
currently lose_sum: 94.55272859334946
time_elpased: 1.381
batch start
#iterations: 182
currently lose_sum: 95.15802550315857
time_elpased: 1.356
batch start
#iterations: 183
currently lose_sum: 94.87467741966248
time_elpased: 1.373
batch start
#iterations: 184
currently lose_sum: 94.53004443645477
time_elpased: 1.379
batch start
#iterations: 185
currently lose_sum: 94.55008214712143
time_elpased: 1.343
batch start
#iterations: 186
currently lose_sum: 94.44811874628067
time_elpased: 1.342
batch start
#iterations: 187
currently lose_sum: 95.00626438856125
time_elpased: 1.364
batch start
#iterations: 188
currently lose_sum: 94.57226765155792
time_elpased: 1.376
batch start
#iterations: 189
currently lose_sum: 94.99608272314072
time_elpased: 1.363
batch start
#iterations: 190
currently lose_sum: 95.02005213499069
time_elpased: 1.411
batch start
#iterations: 191
currently lose_sum: 94.57704013586044
time_elpased: 1.359
batch start
#iterations: 192
currently lose_sum: 94.64525467157364
time_elpased: 1.368
batch start
#iterations: 193
currently lose_sum: 94.90958368778229
time_elpased: 1.355
batch start
#iterations: 194
currently lose_sum: 95.08483070135117
time_elpased: 1.377
batch start
#iterations: 195
currently lose_sum: 95.26861995458603
time_elpased: 1.35
batch start
#iterations: 196
currently lose_sum: 93.96748405694962
time_elpased: 1.373
batch start
#iterations: 197
currently lose_sum: 94.40184980630875
time_elpased: 1.368
batch start
#iterations: 198
currently lose_sum: 94.10828655958176
time_elpased: 1.355
batch start
#iterations: 199
currently lose_sum: 94.23651945590973
time_elpased: 1.351
start validation test
0.665670103093
0.628911238825
0.810930424043
0.708415752562
0.665430102563
60.965
batch start
#iterations: 200
currently lose_sum: 95.08912301063538
time_elpased: 1.407
batch start
#iterations: 201
currently lose_sum: 94.67995476722717
time_elpased: 1.373
batch start
#iterations: 202
currently lose_sum: 94.73692923784256
time_elpased: 1.364
batch start
#iterations: 203
currently lose_sum: 94.64342951774597
time_elpased: 1.382
batch start
#iterations: 204
currently lose_sum: 94.49368584156036
time_elpased: 1.36
batch start
#iterations: 205
currently lose_sum: 94.46630221605301
time_elpased: 1.359
batch start
#iterations: 206
currently lose_sum: 94.52139645814896
time_elpased: 1.366
batch start
#iterations: 207
currently lose_sum: 95.84871232509613
time_elpased: 1.39
batch start
#iterations: 208
currently lose_sum: 94.55393028259277
time_elpased: 1.373
batch start
#iterations: 209
currently lose_sum: 94.46635228395462
time_elpased: 1.405
batch start
#iterations: 210
currently lose_sum: 94.28844314813614
time_elpased: 1.34
batch start
#iterations: 211
currently lose_sum: 94.29546910524368
time_elpased: 1.368
batch start
#iterations: 212
currently lose_sum: 94.87463504076004
time_elpased: 1.368
batch start
#iterations: 213
currently lose_sum: 94.46946996450424
time_elpased: 1.39
batch start
#iterations: 214
currently lose_sum: 94.64525133371353
time_elpased: 1.383
batch start
#iterations: 215
currently lose_sum: 94.70128047466278
time_elpased: 1.431
batch start
#iterations: 216
currently lose_sum: 94.30607062578201
time_elpased: 1.356
batch start
#iterations: 217
currently lose_sum: 93.68777191638947
time_elpased: 1.374
batch start
#iterations: 218
currently lose_sum: 94.87300008535385
time_elpased: 1.345
batch start
#iterations: 219
currently lose_sum: 94.42936098575592
time_elpased: 1.341
start validation test
0.667525773196
0.654435407604
0.712227254014
0.682109413504
0.66745191697
60.324
batch start
#iterations: 220
currently lose_sum: 93.82134813070297
time_elpased: 1.402
batch start
#iterations: 221
currently lose_sum: 94.27346110343933
time_elpased: 1.359
batch start
#iterations: 222
currently lose_sum: 95.68862736225128
time_elpased: 1.369
batch start
#iterations: 223
currently lose_sum: 93.34958606958389
time_elpased: 1.37
batch start
#iterations: 224
currently lose_sum: 94.81688618659973
time_elpased: 1.402
batch start
#iterations: 225
currently lose_sum: 94.45910120010376
time_elpased: 1.388
batch start
#iterations: 226
currently lose_sum: 94.60188180208206
time_elpased: 1.356
batch start
#iterations: 227
currently lose_sum: 94.09928065538406
time_elpased: 1.35
batch start
#iterations: 228
currently lose_sum: 93.86533844470978
time_elpased: 1.42
batch start
#iterations: 229
currently lose_sum: 93.93589353561401
time_elpased: 1.358
batch start
#iterations: 230
currently lose_sum: 93.66726291179657
time_elpased: 1.36
batch start
#iterations: 231
currently lose_sum: 94.296213388443
time_elpased: 1.35
batch start
#iterations: 232
currently lose_sum: 94.38301330804825
time_elpased: 1.414
batch start
#iterations: 233
currently lose_sum: 94.40019708871841
time_elpased: 1.388
batch start
#iterations: 234
currently lose_sum: 94.64607095718384
time_elpased: 1.364
batch start
#iterations: 235
currently lose_sum: 95.16980385780334
time_elpased: 1.386
batch start
#iterations: 236
currently lose_sum: 94.25417798757553
time_elpased: 1.372
batch start
#iterations: 237
currently lose_sum: 94.26784074306488
time_elpased: 1.352
batch start
#iterations: 238
currently lose_sum: 94.135427236557
time_elpased: 1.415
batch start
#iterations: 239
currently lose_sum: 94.20897138118744
time_elpased: 1.351
start validation test
0.663298969072
0.660808080808
0.673322354879
0.667006525285
0.663282408336
60.509
batch start
#iterations: 240
currently lose_sum: 93.7274512052536
time_elpased: 1.366
batch start
#iterations: 241
currently lose_sum: 93.41556143760681
time_elpased: 1.389
batch start
#iterations: 242
currently lose_sum: 94.60657268762589
time_elpased: 1.348
batch start
#iterations: 243
currently lose_sum: 94.21272242069244
time_elpased: 1.344
batch start
#iterations: 244
currently lose_sum: 93.9736779332161
time_elpased: 1.351
batch start
#iterations: 245
currently lose_sum: 94.69167643785477
time_elpased: 1.366
batch start
#iterations: 246
currently lose_sum: 93.88835257291794
time_elpased: 1.381
batch start
#iterations: 247
currently lose_sum: 94.0587186217308
time_elpased: 1.375
batch start
#iterations: 248
currently lose_sum: 93.89515608549118
time_elpased: 1.365
batch start
#iterations: 249
currently lose_sum: 94.21936410665512
time_elpased: 1.402
batch start
#iterations: 250
currently lose_sum: 93.81838876008987
time_elpased: 1.349
batch start
#iterations: 251
currently lose_sum: 94.86033964157104
time_elpased: 1.364
batch start
#iterations: 252
currently lose_sum: 94.49656289815903
time_elpased: 1.349
batch start
#iterations: 253
currently lose_sum: 93.84080696105957
time_elpased: 1.36
batch start
#iterations: 254
currently lose_sum: 94.0089281797409
time_elpased: 1.378
batch start
#iterations: 255
currently lose_sum: 93.84018194675446
time_elpased: 1.355
batch start
#iterations: 256
currently lose_sum: 93.62962847948074
time_elpased: 1.414
batch start
#iterations: 257
currently lose_sum: 93.83882629871368
time_elpased: 1.375
batch start
#iterations: 258
currently lose_sum: 93.99458241462708
time_elpased: 1.366
batch start
#iterations: 259
currently lose_sum: 93.81894320249557
time_elpased: 1.38
start validation test
0.667371134021
0.661742837315
0.687011115685
0.674140281775
0.66733868465
60.348
batch start
#iterations: 260
currently lose_sum: 93.89031928777695
time_elpased: 1.376
batch start
#iterations: 261
currently lose_sum: 93.65267443656921
time_elpased: 1.352
batch start
#iterations: 262
currently lose_sum: 94.9411610364914
time_elpased: 1.331
batch start
#iterations: 263
currently lose_sum: 93.82415753602982
time_elpased: 1.386
batch start
#iterations: 264
currently lose_sum: 94.42701333761215
time_elpased: 1.382
batch start
#iterations: 265
currently lose_sum: 94.46450680494308
time_elpased: 1.356
batch start
#iterations: 266
currently lose_sum: 93.3914122581482
time_elpased: 1.356
batch start
#iterations: 267
currently lose_sum: 93.42224657535553
time_elpased: 1.358
batch start
#iterations: 268
currently lose_sum: 93.97051048278809
time_elpased: 1.371
batch start
#iterations: 269
currently lose_sum: 93.78684151172638
time_elpased: 1.419
batch start
#iterations: 270
currently lose_sum: 93.64565271139145
time_elpased: 1.368
batch start
#iterations: 271
currently lose_sum: 94.45802915096283
time_elpased: 1.406
batch start
#iterations: 272
currently lose_sum: 93.81852978467941
time_elpased: 1.356
batch start
#iterations: 273
currently lose_sum: 93.79270190000534
time_elpased: 1.391
batch start
#iterations: 274
currently lose_sum: 93.34498625993729
time_elpased: 1.361
batch start
#iterations: 275
currently lose_sum: 93.69114238023758
time_elpased: 1.366
batch start
#iterations: 276
currently lose_sum: 94.54242742061615
time_elpased: 1.39
batch start
#iterations: 277
currently lose_sum: 93.4147133231163
time_elpased: 1.35
batch start
#iterations: 278
currently lose_sum: 93.98224401473999
time_elpased: 1.364
batch start
#iterations: 279
currently lose_sum: 94.18725091218948
time_elpased: 1.39
start validation test
0.664072164948
0.663665200041
0.667558666118
0.665606239417
0.664066404517
60.338
batch start
#iterations: 280
currently lose_sum: 94.10747855901718
time_elpased: 1.351
batch start
#iterations: 281
currently lose_sum: 93.71131545305252
time_elpased: 1.359
batch start
#iterations: 282
currently lose_sum: 93.16335409879684
time_elpased: 1.36
batch start
#iterations: 283
currently lose_sum: 94.05563002824783
time_elpased: 1.375
batch start
#iterations: 284
currently lose_sum: 93.74144721031189
time_elpased: 1.358
batch start
#iterations: 285
currently lose_sum: 93.45817178487778
time_elpased: 1.396
batch start
#iterations: 286
currently lose_sum: 93.84564357995987
time_elpased: 1.364
batch start
#iterations: 287
currently lose_sum: 93.32691043615341
time_elpased: 1.377
batch start
#iterations: 288
currently lose_sum: 93.7784281373024
time_elpased: 1.367
batch start
#iterations: 289
currently lose_sum: 93.95144462585449
time_elpased: 1.381
batch start
#iterations: 290
currently lose_sum: 94.47284412384033
time_elpased: 1.377
batch start
#iterations: 291
currently lose_sum: 93.16578984260559
time_elpased: 1.361
batch start
#iterations: 292
currently lose_sum: 94.29860925674438
time_elpased: 1.4
batch start
#iterations: 293
currently lose_sum: 93.83138364553452
time_elpased: 1.381
batch start
#iterations: 294
currently lose_sum: 93.62909036874771
time_elpased: 1.358
batch start
#iterations: 295
currently lose_sum: 93.89979976415634
time_elpased: 1.39
batch start
#iterations: 296
currently lose_sum: 93.54642415046692
time_elpased: 1.388
batch start
#iterations: 297
currently lose_sum: 95.879918217659
time_elpased: 1.38
batch start
#iterations: 298
currently lose_sum: 93.18980687856674
time_elpased: 1.359
batch start
#iterations: 299
currently lose_sum: 93.49204766750336
time_elpased: 1.362
start validation test
0.61587628866
0.572360010228
0.921572663648
0.706151419558
0.615371214104
63.883
batch start
#iterations: 300
currently lose_sum: 93.81209939718246
time_elpased: 1.395
batch start
#iterations: 301
currently lose_sum: 93.18944185972214
time_elpased: 1.334
batch start
#iterations: 302
currently lose_sum: 93.21894544363022
time_elpased: 1.365
batch start
#iterations: 303
currently lose_sum: 93.34512537717819
time_elpased: 1.354
batch start
#iterations: 304
currently lose_sum: 93.49582624435425
time_elpased: 1.359
batch start
#iterations: 305
currently lose_sum: 93.10016691684723
time_elpased: 1.359
batch start
#iterations: 306
currently lose_sum: 93.23690086603165
time_elpased: 1.363
batch start
#iterations: 307
currently lose_sum: 93.48706096410751
time_elpased: 1.364
batch start
#iterations: 308
currently lose_sum: 93.08240139484406
time_elpased: 1.357
batch start
#iterations: 309
currently lose_sum: 93.98864930868149
time_elpased: 1.354
batch start
#iterations: 310
currently lose_sum: 93.31635022163391
time_elpased: 1.392
batch start
#iterations: 311
currently lose_sum: 93.10745829343796
time_elpased: 1.37
batch start
#iterations: 312
currently lose_sum: 93.35612428188324
time_elpased: 1.363
batch start
#iterations: 313
currently lose_sum: 94.13594764471054
time_elpased: 1.355
batch start
#iterations: 314
currently lose_sum: 93.72198349237442
time_elpased: 1.342
batch start
#iterations: 315
currently lose_sum: 93.02468925714493
time_elpased: 1.361
batch start
#iterations: 316
currently lose_sum: 93.44787395000458
time_elpased: 1.372
batch start
#iterations: 317
currently lose_sum: 92.41535806655884
time_elpased: 1.351
batch start
#iterations: 318
currently lose_sum: 93.15721613168716
time_elpased: 1.397
batch start
#iterations: 319
currently lose_sum: 93.2215381860733
time_elpased: 1.388
start validation test
0.655670103093
0.610835280374
0.861053931659
0.71467623441
0.655330765912
61.001
batch start
#iterations: 320
currently lose_sum: 93.62264585494995
time_elpased: 1.348
batch start
#iterations: 321
currently lose_sum: 93.73841834068298
time_elpased: 1.373
batch start
#iterations: 322
currently lose_sum: 93.3340875506401
time_elpased: 1.366
batch start
#iterations: 323
currently lose_sum: 92.95693969726562
time_elpased: 1.411
batch start
#iterations: 324
currently lose_sum: 93.41795563697815
time_elpased: 1.403
batch start
#iterations: 325
currently lose_sum: 93.02221769094467
time_elpased: 1.394
batch start
#iterations: 326
currently lose_sum: 93.10395985841751
time_elpased: 1.351
batch start
#iterations: 327
currently lose_sum: 94.05157089233398
time_elpased: 1.406
batch start
#iterations: 328
currently lose_sum: 93.26261579990387
time_elpased: 1.396
batch start
#iterations: 329
currently lose_sum: 93.79991638660431
time_elpased: 1.369
batch start
#iterations: 330
currently lose_sum: 93.42917454242706
time_elpased: 1.348
batch start
#iterations: 331
currently lose_sum: 93.52029299736023
time_elpased: 1.394
batch start
#iterations: 332
currently lose_sum: 92.76807230710983
time_elpased: 1.414
batch start
#iterations: 333
currently lose_sum: 92.75520265102386
time_elpased: 1.379
batch start
#iterations: 334
currently lose_sum: 92.89398002624512
time_elpased: 1.356
batch start
#iterations: 335
currently lose_sum: 93.20213216543198
time_elpased: 1.333
batch start
#iterations: 336
currently lose_sum: 93.66406524181366
time_elpased: 1.401
batch start
#iterations: 337
currently lose_sum: 93.32613360881805
time_elpased: 1.351
batch start
#iterations: 338
currently lose_sum: 93.06311476230621
time_elpased: 1.36
batch start
#iterations: 339
currently lose_sum: 92.93538534641266
time_elpased: 1.384
start validation test
0.670824742268
0.648026315789
0.750205846027
0.69538256058
0.670693588028
59.684
batch start
#iterations: 340
currently lose_sum: 93.03618234395981
time_elpased: 1.368
batch start
#iterations: 341
currently lose_sum: 92.89807069301605
time_elpased: 1.369
batch start
#iterations: 342
currently lose_sum: 92.9030447602272
time_elpased: 1.366
batch start
#iterations: 343
currently lose_sum: 93.77269595861435
time_elpased: 1.423
batch start
#iterations: 344
currently lose_sum: 93.45597797632217
time_elpased: 1.4
batch start
#iterations: 345
currently lose_sum: 93.59988766908646
time_elpased: 1.371
batch start
#iterations: 346
currently lose_sum: 93.73779761791229
time_elpased: 1.35
batch start
#iterations: 347
currently lose_sum: 92.97443348169327
time_elpased: 1.37
batch start
#iterations: 348
currently lose_sum: 93.1306300163269
time_elpased: 1.424
batch start
#iterations: 349
currently lose_sum: 93.21938371658325
time_elpased: 1.356
batch start
#iterations: 350
currently lose_sum: 92.676877617836
time_elpased: 1.401
batch start
#iterations: 351
currently lose_sum: 93.17014974355698
time_elpased: 1.382
batch start
#iterations: 352
currently lose_sum: 93.01828294992447
time_elpased: 1.392
batch start
#iterations: 353
currently lose_sum: 93.91858619451523
time_elpased: 1.375
batch start
#iterations: 354
currently lose_sum: 92.94349789619446
time_elpased: 1.378
batch start
#iterations: 355
currently lose_sum: 93.53277802467346
time_elpased: 1.387
batch start
#iterations: 356
currently lose_sum: 92.80042397975922
time_elpased: 1.356
batch start
#iterations: 357
currently lose_sum: 93.09032183885574
time_elpased: 1.366
batch start
#iterations: 358
currently lose_sum: 93.26332432031631
time_elpased: 1.365
batch start
#iterations: 359
currently lose_sum: 92.42743527889252
time_elpased: 1.376
start validation test
0.668659793814
0.657622243528
0.705948950185
0.680929216718
0.668598184304
59.856
batch start
#iterations: 360
currently lose_sum: 93.03020405769348
time_elpased: 1.335
batch start
#iterations: 361
currently lose_sum: 93.08856576681137
time_elpased: 1.365
batch start
#iterations: 362
currently lose_sum: 93.12681466341019
time_elpased: 1.378
batch start
#iterations: 363
currently lose_sum: 93.21788769960403
time_elpased: 1.4
batch start
#iterations: 364
currently lose_sum: 92.75550055503845
time_elpased: 1.423
batch start
#iterations: 365
currently lose_sum: 93.36442619562149
time_elpased: 1.351
batch start
#iterations: 366
currently lose_sum: 93.01205223798752
time_elpased: 1.381
batch start
#iterations: 367
currently lose_sum: 92.78058296442032
time_elpased: 1.403
batch start
#iterations: 368
currently lose_sum: 92.3054524064064
time_elpased: 1.379
batch start
#iterations: 369
currently lose_sum: 93.5287207365036
time_elpased: 1.411
batch start
#iterations: 370
currently lose_sum: 93.11324560642242
time_elpased: 1.368
batch start
#iterations: 371
currently lose_sum: 93.60570764541626
time_elpased: 1.355
batch start
#iterations: 372
currently lose_sum: 92.6120348572731
time_elpased: 1.404
batch start
#iterations: 373
currently lose_sum: 92.54930847883224
time_elpased: 1.376
batch start
#iterations: 374
currently lose_sum: 92.9666256904602
time_elpased: 1.353
batch start
#iterations: 375
currently lose_sum: 93.41476213932037
time_elpased: 1.367
batch start
#iterations: 376
currently lose_sum: 92.84861618280411
time_elpased: 1.361
batch start
#iterations: 377
currently lose_sum: 93.03823328018188
time_elpased: 1.364
batch start
#iterations: 378
currently lose_sum: 93.72431313991547
time_elpased: 1.374
batch start
#iterations: 379
currently lose_sum: 92.98605251312256
time_elpased: 1.364
start validation test
0.644793814433
0.677427458862
0.555063812268
0.610171409176
0.644942067225
61.673
batch start
#iterations: 380
currently lose_sum: 92.98825061321259
time_elpased: 1.351
batch start
#iterations: 381
currently lose_sum: 92.03218472003937
time_elpased: 1.365
batch start
#iterations: 382
currently lose_sum: 92.99521428346634
time_elpased: 1.368
batch start
#iterations: 383
currently lose_sum: 92.72073990106583
time_elpased: 1.33
batch start
#iterations: 384
currently lose_sum: 91.86098033189774
time_elpased: 1.356
batch start
#iterations: 385
currently lose_sum: 93.06108635663986
time_elpased: 1.352
batch start
#iterations: 386
currently lose_sum: 92.76376181840897
time_elpased: 1.348
batch start
#iterations: 387
currently lose_sum: 92.89145785570145
time_elpased: 1.344
batch start
#iterations: 388
currently lose_sum: 92.93801939487457
time_elpased: 1.357
batch start
#iterations: 389
currently lose_sum: 92.71894681453705
time_elpased: 1.373
batch start
#iterations: 390
currently lose_sum: 93.15529102087021
time_elpased: 1.365
batch start
#iterations: 391
currently lose_sum: 92.51522517204285
time_elpased: 1.366
batch start
#iterations: 392
currently lose_sum: 92.04608792066574
time_elpased: 1.396
batch start
#iterations: 393
currently lose_sum: 92.53403133153915
time_elpased: 1.352
batch start
#iterations: 394
currently lose_sum: 92.18370735645294
time_elpased: 1.348
batch start
#iterations: 395
currently lose_sum: 93.93347382545471
time_elpased: 1.37
batch start
#iterations: 396
currently lose_sum: 91.82853132486343
time_elpased: 1.361
batch start
#iterations: 397
currently lose_sum: 93.00384712219238
time_elpased: 1.367
batch start
#iterations: 398
currently lose_sum: 92.24521005153656
time_elpased: 1.356
batch start
#iterations: 399
currently lose_sum: 92.33929508924484
time_elpased: 1.378
start validation test
0.667680412371
0.655444602948
0.709345409634
0.68133063121
0.667611573053
59.926
acc: 0.675
pre: 0.651
rec: 0.758
F1: 0.700
auc: 0.722
