start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.4979680776596
time_elpased: 1.517
batch start
#iterations: 1
currently lose_sum: 100.43816584348679
time_elpased: 1.546
batch start
#iterations: 2
currently lose_sum: 100.40811461210251
time_elpased: 1.505
batch start
#iterations: 3
currently lose_sum: 100.29963153600693
time_elpased: 1.505
batch start
#iterations: 4
currently lose_sum: 100.38880896568298
time_elpased: 1.491
batch start
#iterations: 5
currently lose_sum: 100.33305150270462
time_elpased: 1.506
batch start
#iterations: 6
currently lose_sum: 100.2936218380928
time_elpased: 1.525
batch start
#iterations: 7
currently lose_sum: 100.24072444438934
time_elpased: 1.52
batch start
#iterations: 8
currently lose_sum: 100.23990273475647
time_elpased: 1.52
batch start
#iterations: 9
currently lose_sum: 100.19948160648346
time_elpased: 1.523
batch start
#iterations: 10
currently lose_sum: 100.13457709550858
time_elpased: 1.495
batch start
#iterations: 11
currently lose_sum: 100.0341786146164
time_elpased: 1.513
batch start
#iterations: 12
currently lose_sum: 99.9900803565979
time_elpased: 1.483
batch start
#iterations: 13
currently lose_sum: 99.91339021921158
time_elpased: 1.537
batch start
#iterations: 14
currently lose_sum: 99.83431905508041
time_elpased: 1.524
batch start
#iterations: 15
currently lose_sum: 99.68539011478424
time_elpased: 1.503
batch start
#iterations: 16
currently lose_sum: 99.53999352455139
time_elpased: 1.495
batch start
#iterations: 17
currently lose_sum: 99.61184149980545
time_elpased: 1.546
batch start
#iterations: 18
currently lose_sum: 99.39092463254929
time_elpased: 1.523
batch start
#iterations: 19
currently lose_sum: 99.53425353765488
time_elpased: 1.538
start validation test
0.593144329897
0.576165024219
0.70999279613
0.636116361625
0.592939184392
65.561
batch start
#iterations: 20
currently lose_sum: 99.1826479434967
time_elpased: 1.521
batch start
#iterations: 21
currently lose_sum: 99.02426719665527
time_elpased: 1.541
batch start
#iterations: 22
currently lose_sum: 99.31625896692276
time_elpased: 1.523
batch start
#iterations: 23
currently lose_sum: 98.88412380218506
time_elpased: 1.528
batch start
#iterations: 24
currently lose_sum: 99.14203935861588
time_elpased: 1.509
batch start
#iterations: 25
currently lose_sum: 98.62745535373688
time_elpased: 1.522
batch start
#iterations: 26
currently lose_sum: 99.00516146421432
time_elpased: 1.501
batch start
#iterations: 27
currently lose_sum: 98.65080910921097
time_elpased: 1.545
batch start
#iterations: 28
currently lose_sum: 98.85960704088211
time_elpased: 1.522
batch start
#iterations: 29
currently lose_sum: 98.68314969539642
time_elpased: 1.523
batch start
#iterations: 30
currently lose_sum: 98.7619908452034
time_elpased: 1.546
batch start
#iterations: 31
currently lose_sum: 99.41588550806046
time_elpased: 1.501
batch start
#iterations: 32
currently lose_sum: 98.55253392457962
time_elpased: 1.516
batch start
#iterations: 33
currently lose_sum: 98.2907857298851
time_elpased: 1.521
batch start
#iterations: 34
currently lose_sum: 98.47942262887955
time_elpased: 1.491
batch start
#iterations: 35
currently lose_sum: 98.40748506784439
time_elpased: 1.52
batch start
#iterations: 36
currently lose_sum: 98.26886892318726
time_elpased: 1.506
batch start
#iterations: 37
currently lose_sum: 98.32836228609085
time_elpased: 1.522
batch start
#iterations: 38
currently lose_sum: 98.06186485290527
time_elpased: 1.541
batch start
#iterations: 39
currently lose_sum: 97.9350318312645
time_elpased: 1.514
start validation test
0.622164948454
0.649168853893
0.534527117423
0.586296421718
0.622318810183
63.375
batch start
#iterations: 40
currently lose_sum: 98.34611350297928
time_elpased: 1.508
batch start
#iterations: 41
currently lose_sum: 97.8594965338707
time_elpased: 1.505
batch start
#iterations: 42
currently lose_sum: 98.26472169160843
time_elpased: 1.53
batch start
#iterations: 43
currently lose_sum: 98.0298770070076
time_elpased: 1.492
batch start
#iterations: 44
currently lose_sum: 99.43748086690903
time_elpased: 1.515
batch start
#iterations: 45
currently lose_sum: 97.87036496400833
time_elpased: 1.523
batch start
#iterations: 46
currently lose_sum: 97.56379079818726
time_elpased: 1.496
batch start
#iterations: 47
currently lose_sum: 98.48700869083405
time_elpased: 1.489
batch start
#iterations: 48
currently lose_sum: 97.63973706960678
time_elpased: 1.481
batch start
#iterations: 49
currently lose_sum: 98.14380812644958
time_elpased: 1.528
batch start
#iterations: 50
currently lose_sum: 97.65329056978226
time_elpased: 1.51
batch start
#iterations: 51
currently lose_sum: 98.61840981245041
time_elpased: 1.517
batch start
#iterations: 52
currently lose_sum: 97.82172006368637
time_elpased: 1.523
batch start
#iterations: 53
currently lose_sum: 97.34537267684937
time_elpased: 1.528
batch start
#iterations: 54
currently lose_sum: 97.92952245473862
time_elpased: 1.601
batch start
#iterations: 55
currently lose_sum: 99.42614352703094
time_elpased: 1.539
batch start
#iterations: 56
currently lose_sum: 97.5259450674057
time_elpased: 1.52
batch start
#iterations: 57
currently lose_sum: 97.59334415197372
time_elpased: 1.486
batch start
#iterations: 58
currently lose_sum: 99.25124043226242
time_elpased: 1.496
batch start
#iterations: 59
currently lose_sum: 98.59067910909653
time_elpased: 1.515
start validation test
0.626649484536
0.586455129997
0.863538129052
0.698522372529
0.626233589983
63.014
batch start
#iterations: 60
currently lose_sum: 97.37817323207855
time_elpased: 1.501
batch start
#iterations: 61
currently lose_sum: 97.33066260814667
time_elpased: 1.504
batch start
#iterations: 62
currently lose_sum: 100.30469232797623
time_elpased: 1.519
batch start
#iterations: 63
currently lose_sum: 100.47974073886871
time_elpased: 1.533
batch start
#iterations: 64
currently lose_sum: 99.38404822349548
time_elpased: 1.523
batch start
#iterations: 65
currently lose_sum: 97.3951306939125
time_elpased: 1.505
batch start
#iterations: 66
currently lose_sum: 97.16539597511292
time_elpased: 1.516
batch start
#iterations: 67
currently lose_sum: 97.51822596788406
time_elpased: 1.52
batch start
#iterations: 68
currently lose_sum: 97.85253393650055
time_elpased: 1.567
batch start
#iterations: 69
currently lose_sum: 97.40588796138763
time_elpased: 1.526
batch start
#iterations: 70
currently lose_sum: 97.51749962568283
time_elpased: 1.525
batch start
#iterations: 71
currently lose_sum: 98.31945019960403
time_elpased: 1.493
batch start
#iterations: 72
currently lose_sum: 100.4913939833641
time_elpased: 1.498
batch start
#iterations: 73
currently lose_sum: 99.02823913097382
time_elpased: 1.555
batch start
#iterations: 74
currently lose_sum: 100.49435275793076
time_elpased: 1.548
batch start
#iterations: 75
currently lose_sum: 99.54918652772903
time_elpased: 1.538
batch start
#iterations: 76
currently lose_sum: 96.97114545106888
time_elpased: 1.522
batch start
#iterations: 77
currently lose_sum: 97.89381980895996
time_elpased: 1.5
batch start
#iterations: 78
currently lose_sum: 97.24329590797424
time_elpased: 1.492
batch start
#iterations: 79
currently lose_sum: 98.44056236743927
time_elpased: 1.486
start validation test
0.628917525773
0.586256508633
0.880621591026
0.70390326163
0.628475620464
63.838
batch start
#iterations: 80
currently lose_sum: 97.02725833654404
time_elpased: 1.528
batch start
#iterations: 81
currently lose_sum: 97.25034010410309
time_elpased: 1.556
batch start
#iterations: 82
currently lose_sum: 97.04635959863663
time_elpased: 1.481
batch start
#iterations: 83
currently lose_sum: 97.35875689983368
time_elpased: 1.55
batch start
#iterations: 84
currently lose_sum: 96.98178398609161
time_elpased: 1.575
batch start
#iterations: 85
currently lose_sum: 96.95448964834213
time_elpased: 1.51
batch start
#iterations: 86
currently lose_sum: 97.10326564311981
time_elpased: 1.493
batch start
#iterations: 87
currently lose_sum: 97.49816244840622
time_elpased: 1.513
batch start
#iterations: 88
currently lose_sum: 96.28383857011795
time_elpased: 1.47
batch start
#iterations: 89
currently lose_sum: 98.40144968032837
time_elpased: 1.513
batch start
#iterations: 90
currently lose_sum: 100.5047989487648
time_elpased: 1.496
batch start
#iterations: 91
currently lose_sum: 100.50414431095123
time_elpased: 1.499
batch start
#iterations: 92
currently lose_sum: 100.503149330616
time_elpased: 1.497
batch start
#iterations: 93
currently lose_sum: 100.50130063295364
time_elpased: 1.492
batch start
#iterations: 94
currently lose_sum: 100.4932764172554
time_elpased: 1.489
batch start
#iterations: 95
currently lose_sum: 99.83713978528976
time_elpased: 1.5
batch start
#iterations: 96
currently lose_sum: 98.91996365785599
time_elpased: 1.532
batch start
#iterations: 97
currently lose_sum: 98.96225064992905
time_elpased: 1.477
batch start
#iterations: 98
currently lose_sum: 97.23808461427689
time_elpased: 1.523
batch start
#iterations: 99
currently lose_sum: 98.77952748537064
time_elpased: 1.591
start validation test
0.49912371134
0.0
0.0
nan
0.5
67.207
acc: 0.624
pre: 0.585
rec: 0.863
F1: 0.697
auc: 0.624
