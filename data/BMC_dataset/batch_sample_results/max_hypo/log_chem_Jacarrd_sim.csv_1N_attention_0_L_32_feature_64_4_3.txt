start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.51569730043411
time_elpased: 1.689
batch start
#iterations: 1
currently lose_sum: 100.34684437513351
time_elpased: 1.645
batch start
#iterations: 2
currently lose_sum: 100.31261968612671
time_elpased: 1.671
batch start
#iterations: 3
currently lose_sum: 100.25727170705795
time_elpased: 1.668
batch start
#iterations: 4
currently lose_sum: 100.17817288637161
time_elpased: 1.618
batch start
#iterations: 5
currently lose_sum: 100.09403556585312
time_elpased: 1.616
batch start
#iterations: 6
currently lose_sum: 100.03730618953705
time_elpased: 1.628
batch start
#iterations: 7
currently lose_sum: 99.92513829469681
time_elpased: 1.667
batch start
#iterations: 8
currently lose_sum: 99.77786922454834
time_elpased: 1.651
batch start
#iterations: 9
currently lose_sum: 99.6536995768547
time_elpased: 1.648
batch start
#iterations: 10
currently lose_sum: 99.61838120222092
time_elpased: 1.632
batch start
#iterations: 11
currently lose_sum: 99.57075154781342
time_elpased: 1.678
batch start
#iterations: 12
currently lose_sum: 99.36098951101303
time_elpased: 1.623
batch start
#iterations: 13
currently lose_sum: 99.27008605003357
time_elpased: 1.661
batch start
#iterations: 14
currently lose_sum: 99.05209577083588
time_elpased: 1.636
batch start
#iterations: 15
currently lose_sum: 99.06303972005844
time_elpased: 1.647
batch start
#iterations: 16
currently lose_sum: 99.11321699619293
time_elpased: 1.597
batch start
#iterations: 17
currently lose_sum: 99.06684243679047
time_elpased: 1.637
batch start
#iterations: 18
currently lose_sum: 98.86609601974487
time_elpased: 1.615
batch start
#iterations: 19
currently lose_sum: 98.92389351129532
time_elpased: 1.601
start validation test
0.605979381443
0.58325297332
0.747015232606
0.655054151625
0.605746360624
64.695
batch start
#iterations: 20
currently lose_sum: 98.53733319044113
time_elpased: 1.634
batch start
#iterations: 21
currently lose_sum: 98.63568580150604
time_elpased: 1.645
batch start
#iterations: 22
currently lose_sum: 98.65788215398788
time_elpased: 1.615
batch start
#iterations: 23
currently lose_sum: 98.49162775278091
time_elpased: 1.657
batch start
#iterations: 24
currently lose_sum: 98.33185291290283
time_elpased: 1.624
batch start
#iterations: 25
currently lose_sum: 98.75763404369354
time_elpased: 1.653
batch start
#iterations: 26
currently lose_sum: 98.09149861335754
time_elpased: 1.627
batch start
#iterations: 27
currently lose_sum: 98.2663072347641
time_elpased: 1.635
batch start
#iterations: 28
currently lose_sum: 98.54646641016006
time_elpased: 1.64
batch start
#iterations: 29
currently lose_sum: 98.52022248506546
time_elpased: 1.625
batch start
#iterations: 30
currently lose_sum: 97.85475450754166
time_elpased: 1.65
batch start
#iterations: 31
currently lose_sum: 98.1129338145256
time_elpased: 1.63
batch start
#iterations: 32
currently lose_sum: 98.21262663602829
time_elpased: 1.624
batch start
#iterations: 33
currently lose_sum: 98.4076035618782
time_elpased: 1.648
batch start
#iterations: 34
currently lose_sum: 97.45824366807938
time_elpased: 1.621
batch start
#iterations: 35
currently lose_sum: 98.16732424497604
time_elpased: 1.658
batch start
#iterations: 36
currently lose_sum: 97.89727860689163
time_elpased: 1.679
batch start
#iterations: 37
currently lose_sum: 98.40316128730774
time_elpased: 1.681
batch start
#iterations: 38
currently lose_sum: 97.55640822649002
time_elpased: 1.605
batch start
#iterations: 39
currently lose_sum: 98.33572816848755
time_elpased: 1.644
start validation test
0.632886597938
0.644883824844
0.594174557431
0.618491536319
0.632950558352
62.672
batch start
#iterations: 40
currently lose_sum: 97.80732661485672
time_elpased: 1.702
batch start
#iterations: 41
currently lose_sum: 97.85010200738907
time_elpased: 1.604
batch start
#iterations: 42
currently lose_sum: 98.02046173810959
time_elpased: 1.682
batch start
#iterations: 43
currently lose_sum: 97.04927563667297
time_elpased: 1.61
batch start
#iterations: 44
currently lose_sum: 99.17248457670212
time_elpased: 1.702
batch start
#iterations: 45
currently lose_sum: 97.55289965867996
time_elpased: 1.632
batch start
#iterations: 46
currently lose_sum: 97.52815812826157
time_elpased: 1.663
batch start
#iterations: 47
currently lose_sum: 98.28649640083313
time_elpased: 1.616
batch start
#iterations: 48
currently lose_sum: 97.84916472434998
time_elpased: 1.621
batch start
#iterations: 49
currently lose_sum: 98.46019822359085
time_elpased: 1.701
batch start
#iterations: 50
currently lose_sum: 97.35124886035919
time_elpased: 1.571
batch start
#iterations: 51
currently lose_sum: 97.27806723117828
time_elpased: 1.625
batch start
#iterations: 52
currently lose_sum: 97.69726181030273
time_elpased: 1.659
batch start
#iterations: 53
currently lose_sum: 98.17105436325073
time_elpased: 1.696
batch start
#iterations: 54
currently lose_sum: 97.30856585502625
time_elpased: 1.653
batch start
#iterations: 55
currently lose_sum: 98.39008927345276
time_elpased: 1.615
batch start
#iterations: 56
currently lose_sum: 99.70991313457489
time_elpased: 1.622
batch start
#iterations: 57
currently lose_sum: 97.198745906353
time_elpased: 1.665
batch start
#iterations: 58
currently lose_sum: 97.22553998231888
time_elpased: 1.616
batch start
#iterations: 59
currently lose_sum: 97.6378442645073
time_elpased: 1.638
start validation test
0.591855670103
0.555221130221
0.930321119802
0.695414679181
0.59129645416
64.878
batch start
#iterations: 60
currently lose_sum: 97.08900111913681
time_elpased: 1.603
batch start
#iterations: 61
currently lose_sum: 97.12075728178024
time_elpased: 1.627
batch start
#iterations: 62
currently lose_sum: 99.53799968957901
time_elpased: 1.665
batch start
#iterations: 63
currently lose_sum: 98.61611819267273
time_elpased: 1.63
batch start
#iterations: 64
currently lose_sum: 97.16112464666367
time_elpased: 1.617
batch start
#iterations: 65
currently lose_sum: 97.22415941953659
time_elpased: 1.65
batch start
#iterations: 66
currently lose_sum: 96.73271709680557
time_elpased: 1.624
batch start
#iterations: 67
currently lose_sum: 97.01689410209656
time_elpased: 1.644
batch start
#iterations: 68
currently lose_sum: 97.0803661942482
time_elpased: 1.664
batch start
#iterations: 69
currently lose_sum: 97.11834102869034
time_elpased: 1.656
batch start
#iterations: 70
currently lose_sum: 96.6137125492096
time_elpased: 1.611
batch start
#iterations: 71
currently lose_sum: 96.82508909702301
time_elpased: 1.633
batch start
#iterations: 72
currently lose_sum: 100.2078069448471
time_elpased: 1.646
batch start
#iterations: 73
currently lose_sum: 98.25991052389145
time_elpased: 1.629
batch start
#iterations: 74
currently lose_sum: 100.53157901763916
time_elpased: 1.711
batch start
#iterations: 75
currently lose_sum: 99.34525966644287
time_elpased: 1.59
batch start
#iterations: 76
currently lose_sum: 97.02314686775208
time_elpased: 1.621
batch start
#iterations: 77
currently lose_sum: 98.04978787899017
time_elpased: 1.628
batch start
#iterations: 78
currently lose_sum: 97.1225575208664
time_elpased: 1.613
batch start
#iterations: 79
currently lose_sum: 97.75078970193863
time_elpased: 1.633
start validation test
0.645773195876
0.607694637988
0.825854261013
0.70017452007
0.64547566417
62.386
batch start
#iterations: 80
currently lose_sum: 96.90505701303482
time_elpased: 1.628
batch start
#iterations: 81
currently lose_sum: 96.45302855968475
time_elpased: 1.627
batch start
#iterations: 82
currently lose_sum: 97.55478000640869
time_elpased: 1.623
batch start
#iterations: 83
currently lose_sum: 96.75314408540726
time_elpased: 1.635
batch start
#iterations: 84
currently lose_sum: 100.32650762796402
time_elpased: 1.649
batch start
#iterations: 85
currently lose_sum: 98.32103914022446
time_elpased: 1.703
batch start
#iterations: 86
currently lose_sum: 98.04605633020401
time_elpased: 1.625
batch start
#iterations: 87
currently lose_sum: 98.95694851875305
time_elpased: 1.611
batch start
#iterations: 88
currently lose_sum: 100.39875841140747
time_elpased: 1.661
batch start
#iterations: 89
currently lose_sum: 96.80412536859512
time_elpased: 1.609
batch start
#iterations: 90
currently lose_sum: 97.8883615732193
time_elpased: 1.615
batch start
#iterations: 91
currently lose_sum: 98.7760140299797
time_elpased: 1.599
batch start
#iterations: 92
currently lose_sum: 98.60135799646378
time_elpased: 1.606
batch start
#iterations: 93
currently lose_sum: 96.27013903856277
time_elpased: 1.674
batch start
#iterations: 94
currently lose_sum: 97.21107786893845
time_elpased: 1.609
batch start
#iterations: 95
currently lose_sum: 96.42286199331284
time_elpased: 1.617
batch start
#iterations: 96
currently lose_sum: 97.2482059597969
time_elpased: 1.652
batch start
#iterations: 97
currently lose_sum: 96.34291863441467
time_elpased: 1.667
batch start
#iterations: 98
currently lose_sum: 97.66293835639954
time_elpased: 1.636
batch start
#iterations: 99
currently lose_sum: 98.14952820539474
time_elpased: 1.66
start validation test
0.562731958763
0.536676780296
0.928468505558
0.680188501414
0.562127685245
67.111
batch start
#iterations: 100
currently lose_sum: 97.4780615568161
time_elpased: 1.663
batch start
#iterations: 101
currently lose_sum: 97.04400217533112
time_elpased: 1.627
batch start
#iterations: 102
currently lose_sum: 96.58195167779922
time_elpased: 1.622
batch start
#iterations: 103
currently lose_sum: 96.85860520601273
time_elpased: 1.68
batch start
#iterations: 104
currently lose_sum: 97.9834977388382
time_elpased: 1.655
batch start
#iterations: 105
currently lose_sum: 98.31190818548203
time_elpased: 1.64
batch start
#iterations: 106
currently lose_sum: 96.69379901885986
time_elpased: 1.636
batch start
#iterations: 107
currently lose_sum: 96.67732512950897
time_elpased: 1.641
batch start
#iterations: 108
currently lose_sum: 95.99030488729477
time_elpased: 1.641
batch start
#iterations: 109
currently lose_sum: 97.04671859741211
time_elpased: 1.622
batch start
#iterations: 110
currently lose_sum: 100.3201465010643
time_elpased: 1.643
batch start
#iterations: 111
currently lose_sum: 100.49774414300919
time_elpased: 1.62
batch start
#iterations: 112
currently lose_sum: 100.47953724861145
time_elpased: 1.657
batch start
#iterations: 113
currently lose_sum: 98.8089485168457
time_elpased: 1.652
batch start
#iterations: 114
currently lose_sum: 96.80753594636917
time_elpased: 1.649
batch start
#iterations: 115
currently lose_sum: 96.28417205810547
time_elpased: 1.616
batch start
#iterations: 116
currently lose_sum: 96.0895305275917
time_elpased: 1.636
batch start
#iterations: 117
currently lose_sum: 96.07257461547852
time_elpased: 1.625
batch start
#iterations: 118
currently lose_sum: 100.6163694858551
time_elpased: 1.67
batch start
#iterations: 119
currently lose_sum: 100.50638043880463
time_elpased: 1.645
start validation test
0.511597938144
0.506768522159
0.928571428571
0.655692430684
0.510909010444
67.235
batch start
#iterations: 120
currently lose_sum: 100.50628703832626
time_elpased: 1.642
batch start
#iterations: 121
currently lose_sum: 100.5062147974968
time_elpased: 1.632
batch start
#iterations: 122
currently lose_sum: 100.5061644911766
time_elpased: 1.686
batch start
#iterations: 123
currently lose_sum: 100.50606292486191
time_elpased: 1.635
batch start
#iterations: 124
currently lose_sum: 100.5059706568718
time_elpased: 1.642
batch start
#iterations: 125
currently lose_sum: 100.50588572025299
time_elpased: 1.615
batch start
#iterations: 126
currently lose_sum: 100.5058017373085
time_elpased: 1.663
batch start
#iterations: 127
currently lose_sum: 100.50564122200012
time_elpased: 1.663
batch start
#iterations: 128
currently lose_sum: 100.50546526908875
time_elpased: 1.624
batch start
#iterations: 129
currently lose_sum: 100.5050557255745
time_elpased: 1.604
batch start
#iterations: 130
currently lose_sum: 100.50451785326004
time_elpased: 1.667
batch start
#iterations: 131
currently lose_sum: 100.50285935401917
time_elpased: 1.668
batch start
#iterations: 132
currently lose_sum: 100.49423968791962
time_elpased: 1.652
batch start
#iterations: 133
currently lose_sum: 99.48612874746323
time_elpased: 1.683
batch start
#iterations: 134
currently lose_sum: 100.41269755363464
time_elpased: 1.684
batch start
#iterations: 135
currently lose_sum: 100.50577127933502
time_elpased: 1.617
batch start
#iterations: 136
currently lose_sum: 100.50562089681625
time_elpased: 1.603
batch start
#iterations: 137
currently lose_sum: 100.50566977262497
time_elpased: 1.616
batch start
#iterations: 138
currently lose_sum: 100.50553447008133
time_elpased: 1.664
batch start
#iterations: 139
currently lose_sum: 100.50530809164047
time_elpased: 1.654
start validation test
0.555618556701
0.548966997585
0.63174145739
0.587452744413
0.555492785696
67.234
batch start
#iterations: 140
currently lose_sum: 100.50514286756516
time_elpased: 1.617
batch start
#iterations: 141
currently lose_sum: 100.50511395931244
time_elpased: 1.628
batch start
#iterations: 142
currently lose_sum: 100.50484448671341
time_elpased: 1.652
batch start
#iterations: 143
currently lose_sum: 100.50463718175888
time_elpased: 1.617
batch start
#iterations: 144
currently lose_sum: 100.50385677814484
time_elpased: 1.631
batch start
#iterations: 145
currently lose_sum: 100.50239950418472
time_elpased: 1.66
batch start
#iterations: 146
currently lose_sum: 100.49594974517822
time_elpased: 1.686
batch start
#iterations: 147
currently lose_sum: 100.0804113149643
time_elpased: 1.656
batch start
#iterations: 148
currently lose_sum: 100.47612190246582
time_elpased: 1.666
batch start
#iterations: 149
currently lose_sum: 100.50795239210129
time_elpased: 1.632
batch start
#iterations: 150
currently lose_sum: 100.5053358078003
time_elpased: 1.663
batch start
#iterations: 151
currently lose_sum: 100.50510120391846
time_elpased: 1.626
batch start
#iterations: 152
currently lose_sum: 100.50499713420868
time_elpased: 1.639
batch start
#iterations: 153
currently lose_sum: 100.50503140687943
time_elpased: 1.611
batch start
#iterations: 154
currently lose_sum: 100.50500375032425
time_elpased: 1.614
batch start
#iterations: 155
currently lose_sum: 100.50417977571487
time_elpased: 1.603
batch start
#iterations: 156
currently lose_sum: 100.50407016277313
time_elpased: 1.667
batch start
#iterations: 157
currently lose_sum: 100.50137436389923
time_elpased: 1.615
batch start
#iterations: 158
currently lose_sum: 100.49198490381241
time_elpased: 1.593
batch start
#iterations: 159
currently lose_sum: 100.28214800357819
time_elpased: 1.625
start validation test
0.579072164948
0.593059558117
0.5083367641
0.547439592108
0.579189034673
66.161
batch start
#iterations: 160
currently lose_sum: 99.45789581537247
time_elpased: 1.662
batch start
#iterations: 161
currently lose_sum: 98.56184720993042
time_elpased: 1.628
batch start
#iterations: 162
currently lose_sum: 99.20687180757523
time_elpased: 1.605
batch start
#iterations: 163
currently lose_sum: 97.95860660076141
time_elpased: 1.611
batch start
#iterations: 164
currently lose_sum: 98.46058291196823
time_elpased: 1.612
batch start
#iterations: 165
currently lose_sum: 97.90330559015274
time_elpased: 1.657
batch start
#iterations: 166
currently lose_sum: 99.2400239109993
time_elpased: 1.698
batch start
#iterations: 167
currently lose_sum: 100.50524908304214
time_elpased: 1.653
batch start
#iterations: 168
currently lose_sum: 100.50523781776428
time_elpased: 1.647
batch start
#iterations: 169
currently lose_sum: 100.50493794679642
time_elpased: 1.711
batch start
#iterations: 170
currently lose_sum: 100.50495940446854
time_elpased: 1.709
batch start
#iterations: 171
currently lose_sum: 100.50477296113968
time_elpased: 1.622
batch start
#iterations: 172
currently lose_sum: 100.5047019124031
time_elpased: 1.628
batch start
#iterations: 173
currently lose_sum: 100.504554271698
time_elpased: 1.646
batch start
#iterations: 174
currently lose_sum: 100.5044156908989
time_elpased: 1.665
batch start
#iterations: 175
currently lose_sum: 100.50416505336761
time_elpased: 1.687
batch start
#iterations: 176
currently lose_sum: 100.50367951393127
time_elpased: 1.648
batch start
#iterations: 177
currently lose_sum: 100.50275182723999
time_elpased: 1.654
batch start
#iterations: 178
currently lose_sum: 100.50138300657272
time_elpased: 1.662
batch start
#iterations: 179
currently lose_sum: 100.49599432945251
time_elpased: 1.642
start validation test
0.593453608247
0.582335464122
0.665706051873
0.621236133122
0.59333423205
67.217
batch start
#iterations: 180
currently lose_sum: 100.55214375257492
time_elpased: 1.616
batch start
#iterations: 181
currently lose_sum: 100.48509562015533
time_elpased: 1.668
batch start
#iterations: 182
currently lose_sum: 99.6204804778099
time_elpased: 1.669
batch start
#iterations: 183
currently lose_sum: 99.95877730846405
time_elpased: 1.643
batch start
#iterations: 184
currently lose_sum: 99.612453520298
time_elpased: 1.689
batch start
#iterations: 185
currently lose_sum: 100.50592708587646
time_elpased: 1.672
batch start
#iterations: 186
currently lose_sum: 100.5058805346489
time_elpased: 1.598
batch start
#iterations: 187
currently lose_sum: 100.50589334964752
time_elpased: 1.641
batch start
#iterations: 188
currently lose_sum: 100.50586754083633
time_elpased: 1.604
batch start
#iterations: 189
currently lose_sum: 100.50588566064835
time_elpased: 1.621
batch start
#iterations: 190
currently lose_sum: 100.5059665441513
time_elpased: 1.663
batch start
#iterations: 191
currently lose_sum: 100.50583690404892
time_elpased: 1.611
batch start
#iterations: 192
currently lose_sum: 100.50582402944565
time_elpased: 1.586
batch start
#iterations: 193
currently lose_sum: 100.5057435631752
time_elpased: 1.632
batch start
#iterations: 194
currently lose_sum: 100.50572896003723
time_elpased: 1.65
batch start
#iterations: 195
currently lose_sum: 100.50567841529846
time_elpased: 1.613
batch start
#iterations: 196
currently lose_sum: 100.50563353300095
time_elpased: 1.622
batch start
#iterations: 197
currently lose_sum: 100.50536578893661
time_elpased: 1.614
batch start
#iterations: 198
currently lose_sum: 100.50537425279617
time_elpased: 1.638
batch start
#iterations: 199
currently lose_sum: 100.50519317388535
time_elpased: 1.633
start validation test
0.551082474227
0.542920467138
0.655516673528
0.59392922087
0.550909927016
67.234
batch start
#iterations: 200
currently lose_sum: 100.50518751144409
time_elpased: 1.634
batch start
#iterations: 201
currently lose_sum: 100.50456857681274
time_elpased: 1.583
batch start
#iterations: 202
currently lose_sum: 100.50386548042297
time_elpased: 1.618
batch start
#iterations: 203
currently lose_sum: 100.5012555718422
time_elpased: 1.58
batch start
#iterations: 204
currently lose_sum: 100.4857514500618
time_elpased: 1.613
batch start
#iterations: 205
currently lose_sum: 99.8205851316452
time_elpased: 1.642
batch start
#iterations: 206
currently lose_sum: 100.4724109172821
time_elpased: 1.618
batch start
#iterations: 207
currently lose_sum: 99.23103767633438
time_elpased: 1.649
batch start
#iterations: 208
currently lose_sum: 100.07861679792404
time_elpased: 1.603
batch start
#iterations: 209
currently lose_sum: 100.50499856472015
time_elpased: 1.619
batch start
#iterations: 210
currently lose_sum: 100.50465339422226
time_elpased: 1.619
batch start
#iterations: 211
currently lose_sum: 100.50436109304428
time_elpased: 1.605
batch start
#iterations: 212
currently lose_sum: 100.50427269935608
time_elpased: 1.613
batch start
#iterations: 213
currently lose_sum: 100.50447207689285
time_elpased: 1.641
batch start
#iterations: 214
currently lose_sum: 100.50388580560684
time_elpased: 1.643
batch start
#iterations: 215
currently lose_sum: 100.50307732820511
time_elpased: 1.657
batch start
#iterations: 216
currently lose_sum: 100.50178837776184
time_elpased: 1.651
batch start
#iterations: 217
currently lose_sum: 100.49863702058792
time_elpased: 1.58
batch start
#iterations: 218
currently lose_sum: 100.4398318529129
time_elpased: 1.588
batch start
#iterations: 219
currently lose_sum: 99.22280788421631
time_elpased: 1.626
start validation test
0.499226804124
1.0
0.000102923013586
0.000205824843059
0.500051461507
66.237
batch start
#iterations: 220
currently lose_sum: 97.79506886005402
time_elpased: 1.611
batch start
#iterations: 221
currently lose_sum: 97.29180234670639
time_elpased: 1.613
batch start
#iterations: 222
currently lose_sum: 97.51596504449844
time_elpased: 1.617
batch start
#iterations: 223
currently lose_sum: 100.51649588346481
time_elpased: 1.596
batch start
#iterations: 224
currently lose_sum: 100.50551861524582
time_elpased: 1.665
batch start
#iterations: 225
currently lose_sum: 100.50542974472046
time_elpased: 1.631
batch start
#iterations: 226
currently lose_sum: 100.50544506311417
time_elpased: 1.643
batch start
#iterations: 227
currently lose_sum: 100.50527393817902
time_elpased: 1.621
batch start
#iterations: 228
currently lose_sum: 100.50542271137238
time_elpased: 1.614
batch start
#iterations: 229
currently lose_sum: 100.5053676366806
time_elpased: 1.712
batch start
#iterations: 230
currently lose_sum: 100.5051982998848
time_elpased: 1.619
batch start
#iterations: 231
currently lose_sum: 100.50527441501617
time_elpased: 1.677
batch start
#iterations: 232
currently lose_sum: 100.50529289245605
time_elpased: 1.617
batch start
#iterations: 233
currently lose_sum: 100.505042552948
time_elpased: 1.627
batch start
#iterations: 234
currently lose_sum: 100.50500500202179
time_elpased: 1.644
batch start
#iterations: 235
currently lose_sum: 100.50497257709503
time_elpased: 1.629
batch start
#iterations: 236
currently lose_sum: 100.50459855794907
time_elpased: 1.613
batch start
#iterations: 237
currently lose_sum: 100.50423634052277
time_elpased: 1.626
batch start
#iterations: 238
currently lose_sum: 100.50362980365753
time_elpased: 1.67
batch start
#iterations: 239
currently lose_sum: 100.50270050764084
time_elpased: 1.68
start validation test
0.571030927835
0.552461237393
0.75545491972
0.638205373446
0.570726220703
67.231
batch start
#iterations: 240
currently lose_sum: 100.50172328948975
time_elpased: 1.627
batch start
#iterations: 241
currently lose_sum: 100.49618273973465
time_elpased: 1.591
batch start
#iterations: 242
currently lose_sum: 100.55725640058517
time_elpased: 1.608
batch start
#iterations: 243
currently lose_sum: 100.50635176897049
time_elpased: 1.595
batch start
#iterations: 244
currently lose_sum: 100.50620722770691
time_elpased: 1.669
batch start
#iterations: 245
currently lose_sum: 100.50621342658997
time_elpased: 1.626
batch start
#iterations: 246
currently lose_sum: 100.50621968507767
time_elpased: 1.577
batch start
#iterations: 247
currently lose_sum: 100.50622743368149
time_elpased: 1.622
batch start
#iterations: 248
currently lose_sum: 100.50621348619461
time_elpased: 1.672
batch start
#iterations: 249
currently lose_sum: 100.506203353405
time_elpased: 1.672
batch start
#iterations: 250
currently lose_sum: 100.50616729259491
time_elpased: 1.638
batch start
#iterations: 251
currently lose_sum: 100.50620478391647
time_elpased: 1.663
batch start
#iterations: 252
currently lose_sum: 100.50621622800827
time_elpased: 1.644
batch start
#iterations: 253
currently lose_sum: 100.506218791008
time_elpased: 1.587
batch start
#iterations: 254
currently lose_sum: 100.50619637966156
time_elpased: 1.621
batch start
#iterations: 255
currently lose_sum: 100.50620478391647
time_elpased: 1.598
batch start
#iterations: 256
currently lose_sum: 100.50619846582413
time_elpased: 1.663
batch start
#iterations: 257
currently lose_sum: 100.50617402791977
time_elpased: 1.6
batch start
#iterations: 258
currently lose_sum: 100.50619721412659
time_elpased: 1.633
batch start
#iterations: 259
currently lose_sum: 100.50615525245667
time_elpased: 1.617
start validation test
0.531443298969
0.525988043839
0.652017291066
0.582261029412
0.531244085434
67.235
batch start
#iterations: 260
currently lose_sum: 100.50615108013153
time_elpased: 1.631
batch start
#iterations: 261
currently lose_sum: 100.50614041090012
time_elpased: 1.602
batch start
#iterations: 262
currently lose_sum: 100.50612258911133
time_elpased: 1.688
batch start
#iterations: 263
currently lose_sum: 100.50618922710419
time_elpased: 1.641
batch start
#iterations: 264
currently lose_sum: 100.50606495141983
time_elpased: 1.615
batch start
#iterations: 265
currently lose_sum: 100.50600934028625
time_elpased: 1.606
batch start
#iterations: 266
currently lose_sum: 100.5060287117958
time_elpased: 1.636
batch start
#iterations: 267
currently lose_sum: 100.50608164072037
time_elpased: 1.632
batch start
#iterations: 268
currently lose_sum: 100.50600099563599
time_elpased: 1.635
batch start
#iterations: 269
currently lose_sum: 100.50590127706528
time_elpased: 1.649
batch start
#iterations: 270
currently lose_sum: 100.50577741861343
time_elpased: 1.662
batch start
#iterations: 271
currently lose_sum: 100.50534439086914
time_elpased: 1.618
batch start
#iterations: 272
currently lose_sum: 100.50521975755692
time_elpased: 1.62
batch start
#iterations: 273
currently lose_sum: 100.50244724750519
time_elpased: 1.63
batch start
#iterations: 274
currently lose_sum: 100.47334909439087
time_elpased: 1.64
batch start
#iterations: 275
currently lose_sum: 99.7620325088501
time_elpased: 1.635
batch start
#iterations: 276
currently lose_sum: 98.37237721681595
time_elpased: 1.655
batch start
#iterations: 277
currently lose_sum: 97.94626599550247
time_elpased: 1.64
batch start
#iterations: 278
currently lose_sum: 97.79377466440201
time_elpased: 1.659
batch start
#iterations: 279
currently lose_sum: 97.23592102527618
time_elpased: 1.61
start validation test
0.648092783505
0.654872949501
0.628653766982
0.641495562674
0.648124900839
62.880
batch start
#iterations: 280
currently lose_sum: 96.98291230201721
time_elpased: 1.719
batch start
#iterations: 281
currently lose_sum: 96.20454102754593
time_elpased: 1.602
batch start
#iterations: 282
currently lose_sum: 96.73957407474518
time_elpased: 1.616
batch start
#iterations: 283
currently lose_sum: 96.79881346225739
time_elpased: 1.599
batch start
#iterations: 284
currently lose_sum: 96.3727719783783
time_elpased: 1.622
batch start
#iterations: 285
currently lose_sum: 96.5551946759224
time_elpased: 1.621
batch start
#iterations: 286
currently lose_sum: 95.75566017627716
time_elpased: 1.617
batch start
#iterations: 287
currently lose_sum: 95.5150294303894
time_elpased: 1.683
batch start
#iterations: 288
currently lose_sum: 96.20399194955826
time_elpased: 1.617
batch start
#iterations: 289
currently lose_sum: 96.04110634326935
time_elpased: 1.623
batch start
#iterations: 290
currently lose_sum: 95.66790741682053
time_elpased: 1.615
batch start
#iterations: 291
currently lose_sum: 95.48987329006195
time_elpased: 1.634
batch start
#iterations: 292
currently lose_sum: 95.96028524637222
time_elpased: 1.647
batch start
#iterations: 293
currently lose_sum: 95.77138644456863
time_elpased: 1.646
batch start
#iterations: 294
currently lose_sum: 96.06952518224716
time_elpased: 1.639
batch start
#iterations: 295
currently lose_sum: 96.20044320821762
time_elpased: 1.647
batch start
#iterations: 296
currently lose_sum: 95.91501486301422
time_elpased: 1.618
batch start
#iterations: 297
currently lose_sum: 98.20992767810822
time_elpased: 1.66
batch start
#iterations: 298
currently lose_sum: 100.50220793485641
time_elpased: 1.663
batch start
#iterations: 299
currently lose_sum: 100.50249350070953
time_elpased: 1.674
start validation test
0.563659793814
0.537651236983
0.919308357349
0.678491397319
0.563072187762
67.229
batch start
#iterations: 300
currently lose_sum: 100.49396687746048
time_elpased: 1.609
batch start
#iterations: 301
currently lose_sum: 97.73709636926651
time_elpased: 1.621
batch start
#iterations: 302
currently lose_sum: 96.40352064371109
time_elpased: 1.635
batch start
#iterations: 303
currently lose_sum: 95.62586253881454
time_elpased: 1.657
batch start
#iterations: 304
currently lose_sum: 95.09111452102661
time_elpased: 1.654
batch start
#iterations: 305
currently lose_sum: 96.121058344841
time_elpased: 1.706
batch start
#iterations: 306
currently lose_sum: 96.34494280815125
time_elpased: 1.648
batch start
#iterations: 307
currently lose_sum: 96.11767464876175
time_elpased: 1.64
batch start
#iterations: 308
currently lose_sum: 96.22919708490372
time_elpased: 1.652
batch start
#iterations: 309
currently lose_sum: 95.24010783433914
time_elpased: 1.614
batch start
#iterations: 310
currently lose_sum: 95.72074347734451
time_elpased: 1.657
batch start
#iterations: 311
currently lose_sum: 95.33495885133743
time_elpased: 1.614
batch start
#iterations: 312
currently lose_sum: 95.31579542160034
time_elpased: 1.589
batch start
#iterations: 313
currently lose_sum: 95.97420805692673
time_elpased: 1.608
batch start
#iterations: 314
currently lose_sum: 95.36604952812195
time_elpased: 1.646
batch start
#iterations: 315
currently lose_sum: 94.69906377792358
time_elpased: 1.646
batch start
#iterations: 316
currently lose_sum: 94.80425089597702
time_elpased: 1.638
batch start
#iterations: 317
currently lose_sum: 95.29175001382828
time_elpased: 1.626
batch start
#iterations: 318
currently lose_sum: 95.24656468629837
time_elpased: 1.611
batch start
#iterations: 319
currently lose_sum: 95.57393151521683
time_elpased: 1.585
start validation test
0.649278350515
0.607011612524
0.850041169205
0.708258296887
0.648946648213
61.647
batch start
#iterations: 320
currently lose_sum: 95.11045145988464
time_elpased: 1.634
batch start
#iterations: 321
currently lose_sum: 94.73490405082703
time_elpased: 1.671
batch start
#iterations: 322
currently lose_sum: 95.24118220806122
time_elpased: 1.649
batch start
#iterations: 323
currently lose_sum: 95.41883838176727
time_elpased: 1.632
batch start
#iterations: 324
currently lose_sum: 95.14249920845032
time_elpased: 1.696
batch start
#iterations: 325
currently lose_sum: 100.54823464155197
time_elpased: 1.617
batch start
#iterations: 326
currently lose_sum: 100.50898146629333
time_elpased: 1.682
batch start
#iterations: 327
currently lose_sum: 100.50879848003387
time_elpased: 1.618
batch start
#iterations: 328
currently lose_sum: 100.50808316469193
time_elpased: 1.656
batch start
#iterations: 329
currently lose_sum: 100.50763642787933
time_elpased: 1.617
batch start
#iterations: 330
currently lose_sum: 100.50742763280869
time_elpased: 1.646
batch start
#iterations: 331
currently lose_sum: 100.50695544481277
time_elpased: 1.603
batch start
#iterations: 332
currently lose_sum: 100.50656419992447
time_elpased: 1.678
batch start
#iterations: 333
currently lose_sum: 100.50583326816559
time_elpased: 1.601
batch start
#iterations: 334
currently lose_sum: 100.50358945131302
time_elpased: 1.618
batch start
#iterations: 335
currently lose_sum: 100.49403113126755
time_elpased: 1.624
batch start
#iterations: 336
currently lose_sum: 99.97654694318771
time_elpased: 1.663
batch start
#iterations: 337
currently lose_sum: 97.17096787691116
time_elpased: 1.623
batch start
#iterations: 338
currently lose_sum: 95.7972601056099
time_elpased: 1.609
batch start
#iterations: 339
currently lose_sum: 95.82108551263809
time_elpased: 1.668
start validation test
0.657371134021
0.645243729295
0.701626183615
0.672254819782
0.657298015393
61.150
batch start
#iterations: 340
currently lose_sum: 95.64149540662766
time_elpased: 1.623
batch start
#iterations: 341
currently lose_sum: 95.39079719781876
time_elpased: 1.612
batch start
#iterations: 342
currently lose_sum: 95.47996819019318
time_elpased: 1.646
batch start
#iterations: 343
currently lose_sum: 96.40595299005508
time_elpased: 1.659
batch start
#iterations: 344
currently lose_sum: 95.93072891235352
time_elpased: 1.627
batch start
#iterations: 345
currently lose_sum: 94.95874619483948
time_elpased: 1.629
batch start
#iterations: 346
currently lose_sum: 96.53427952528
time_elpased: 1.661
batch start
#iterations: 347
currently lose_sum: 94.99993699789047
time_elpased: 1.634
batch start
#iterations: 348
currently lose_sum: 95.15169674158096
time_elpased: 1.665
batch start
#iterations: 349
currently lose_sum: 94.77230840921402
time_elpased: 1.624
batch start
#iterations: 350
currently lose_sum: 95.46472227573395
time_elpased: 1.674
batch start
#iterations: 351
currently lose_sum: 95.62775629758835
time_elpased: 1.638
batch start
#iterations: 352
currently lose_sum: 95.65722018480301
time_elpased: 1.656
batch start
#iterations: 353
currently lose_sum: 95.38841223716736
time_elpased: 1.626
batch start
#iterations: 354
currently lose_sum: 94.67885887622833
time_elpased: 1.635
batch start
#iterations: 355
currently lose_sum: 94.66974520683289
time_elpased: 1.659
batch start
#iterations: 356
currently lose_sum: 95.33758306503296
time_elpased: 1.69
batch start
#iterations: 357
currently lose_sum: 96.03836101293564
time_elpased: 1.633
batch start
#iterations: 358
currently lose_sum: 94.85420399904251
time_elpased: 1.629
batch start
#iterations: 359
currently lose_sum: 94.68750512599945
time_elpased: 1.588
start validation test
0.662268041237
0.652291105121
0.697406340058
0.674094707521
0.662209985394
60.647
batch start
#iterations: 360
currently lose_sum: 96.1598659157753
time_elpased: 1.652
batch start
#iterations: 361
currently lose_sum: 95.42110741138458
time_elpased: 1.609
batch start
#iterations: 362
currently lose_sum: 95.29565966129303
time_elpased: 1.644
batch start
#iterations: 363
currently lose_sum: 95.3349175453186
time_elpased: 1.66
batch start
#iterations: 364
currently lose_sum: 94.78495121002197
time_elpased: 1.606
batch start
#iterations: 365
currently lose_sum: 95.24093240499496
time_elpased: 1.634
batch start
#iterations: 366
currently lose_sum: 95.71653753519058
time_elpased: 1.721
batch start
#iterations: 367
currently lose_sum: 94.09738945960999
time_elpased: 1.652
batch start
#iterations: 368
currently lose_sum: 95.04054576158524
time_elpased: 1.652
batch start
#iterations: 369
currently lose_sum: 95.45279097557068
time_elpased: 1.622
batch start
#iterations: 370
currently lose_sum: 95.5990651845932
time_elpased: 1.621
batch start
#iterations: 371
currently lose_sum: 94.64878714084625
time_elpased: 1.596
batch start
#iterations: 372
currently lose_sum: 94.54789388179779
time_elpased: 1.657
batch start
#iterations: 373
currently lose_sum: 94.89907938241959
time_elpased: 1.638
batch start
#iterations: 374
currently lose_sum: 95.11887627840042
time_elpased: 1.648
batch start
#iterations: 375
currently lose_sum: 94.89847546815872
time_elpased: 1.593
batch start
#iterations: 376
currently lose_sum: 94.6095415353775
time_elpased: 1.644
batch start
#iterations: 377
currently lose_sum: 94.81598824262619
time_elpased: 1.661
batch start
#iterations: 378
currently lose_sum: 94.95256644487381
time_elpased: 1.601
batch start
#iterations: 379
currently lose_sum: 94.59372389316559
time_elpased: 1.657
start validation test
0.662731958763
0.627891979041
0.80166735282
0.704217711677
0.662502408339
60.742
batch start
#iterations: 380
currently lose_sum: 94.50122892856598
time_elpased: 1.646
batch start
#iterations: 381
currently lose_sum: 95.27801555395126
time_elpased: 1.66
batch start
#iterations: 382
currently lose_sum: 94.49866509437561
time_elpased: 1.598
batch start
#iterations: 383
currently lose_sum: 94.47941732406616
time_elpased: 1.644
batch start
#iterations: 384
currently lose_sum: 94.67269176244736
time_elpased: 1.647
batch start
#iterations: 385
currently lose_sum: 95.0495737195015
time_elpased: 1.626
batch start
#iterations: 386
currently lose_sum: 94.43137109279633
time_elpased: 1.66
batch start
#iterations: 387
currently lose_sum: 93.91219240427017
time_elpased: 1.627
batch start
#iterations: 388
currently lose_sum: 95.66831004619598
time_elpased: 1.65
batch start
#iterations: 389
currently lose_sum: 94.49942898750305
time_elpased: 1.641
batch start
#iterations: 390
currently lose_sum: 94.02989745140076
time_elpased: 1.676
batch start
#iterations: 391
currently lose_sum: 94.41729182004929
time_elpased: 1.688
batch start
#iterations: 392
currently lose_sum: 93.96233361959457
time_elpased: 1.633
batch start
#iterations: 393
currently lose_sum: 94.19093006849289
time_elpased: 1.656
batch start
#iterations: 394
currently lose_sum: 94.41451066732407
time_elpased: 1.637
batch start
#iterations: 395
currently lose_sum: 94.21984285116196
time_elpased: 1.615
batch start
#iterations: 396
currently lose_sum: 94.17091029882431
time_elpased: 1.598
batch start
#iterations: 397
currently lose_sum: 94.12444412708282
time_elpased: 1.601
batch start
#iterations: 398
currently lose_sum: 94.00821554660797
time_elpased: 1.638
batch start
#iterations: 399
currently lose_sum: 93.85277563333511
time_elpased: 1.614
start validation test
0.663556701031
0.653096495439
0.700082338411
0.67577368238
0.663496353014
60.866
acc: 0.672
pre: 0.661
rec: 0.707
F1: 0.683
auc: 0.716
