start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.71470957994461
time_elpased: 1.663
batch start
#iterations: 1
currently lose_sum: 99.99628233909607
time_elpased: 1.663
batch start
#iterations: 2
currently lose_sum: 99.76933908462524
time_elpased: 1.658
batch start
#iterations: 3
currently lose_sum: 99.73104071617126
time_elpased: 1.627
batch start
#iterations: 4
currently lose_sum: 99.51946830749512
time_elpased: 1.63
batch start
#iterations: 5
currently lose_sum: 99.46035081148148
time_elpased: 1.642
batch start
#iterations: 6
currently lose_sum: 99.38426852226257
time_elpased: 1.678
batch start
#iterations: 7
currently lose_sum: 99.19737839698792
time_elpased: 1.633
batch start
#iterations: 8
currently lose_sum: 99.04720866680145
time_elpased: 1.64
batch start
#iterations: 9
currently lose_sum: 99.30458855628967
time_elpased: 1.641
batch start
#iterations: 10
currently lose_sum: 98.96720290184021
time_elpased: 1.655
batch start
#iterations: 11
currently lose_sum: 99.05729329586029
time_elpased: 1.744
batch start
#iterations: 12
currently lose_sum: 98.79433697462082
time_elpased: 1.711
batch start
#iterations: 13
currently lose_sum: 98.56624394655228
time_elpased: 1.659
batch start
#iterations: 14
currently lose_sum: 98.90326982736588
time_elpased: 1.641
batch start
#iterations: 15
currently lose_sum: 98.56606179475784
time_elpased: 1.641
batch start
#iterations: 16
currently lose_sum: 98.88368797302246
time_elpased: 1.648
batch start
#iterations: 17
currently lose_sum: 98.69860798120499
time_elpased: 1.643
batch start
#iterations: 18
currently lose_sum: 98.42600113153458
time_elpased: 1.69
batch start
#iterations: 19
currently lose_sum: 98.74072164297104
time_elpased: 1.675
start validation test
0.586288659794
0.633086730678
0.413913759391
0.500560049782
0.586591290518
65.251
batch start
#iterations: 20
currently lose_sum: 98.34471648931503
time_elpased: 1.637
batch start
#iterations: 21
currently lose_sum: 98.18684363365173
time_elpased: 1.627
batch start
#iterations: 22
currently lose_sum: 98.20628821849823
time_elpased: 1.643
batch start
#iterations: 23
currently lose_sum: 98.32133781909943
time_elpased: 1.667
batch start
#iterations: 24
currently lose_sum: 98.00628340244293
time_elpased: 1.644
batch start
#iterations: 25
currently lose_sum: 98.13278740644455
time_elpased: 1.629
batch start
#iterations: 26
currently lose_sum: 98.33600091934204
time_elpased: 1.639
batch start
#iterations: 27
currently lose_sum: 98.12821245193481
time_elpased: 1.675
batch start
#iterations: 28
currently lose_sum: 97.7870888710022
time_elpased: 1.665
batch start
#iterations: 29
currently lose_sum: 97.87756752967834
time_elpased: 1.643
batch start
#iterations: 30
currently lose_sum: 97.99264222383499
time_elpased: 1.656
batch start
#iterations: 31
currently lose_sum: 97.98180216550827
time_elpased: 1.657
batch start
#iterations: 32
currently lose_sum: 97.55808144807816
time_elpased: 1.634
batch start
#iterations: 33
currently lose_sum: 97.56956267356873
time_elpased: 1.649
batch start
#iterations: 34
currently lose_sum: 97.60883939266205
time_elpased: 1.68
batch start
#iterations: 35
currently lose_sum: 97.69930183887482
time_elpased: 1.659
batch start
#iterations: 36
currently lose_sum: 97.8272123336792
time_elpased: 1.625
batch start
#iterations: 37
currently lose_sum: 97.43185186386108
time_elpased: 1.657
batch start
#iterations: 38
currently lose_sum: 97.50923299789429
time_elpased: 1.681
batch start
#iterations: 39
currently lose_sum: 97.61647629737854
time_elpased: 1.66
start validation test
0.598505154639
0.617934915586
0.519810641144
0.56464143983
0.598643314995
64.751
batch start
#iterations: 40
currently lose_sum: 97.86505854129791
time_elpased: 1.675
batch start
#iterations: 41
currently lose_sum: 97.4043453335762
time_elpased: 1.632
batch start
#iterations: 42
currently lose_sum: 97.27933865785599
time_elpased: 1.666
batch start
#iterations: 43
currently lose_sum: 97.15988904237747
time_elpased: 1.635
batch start
#iterations: 44
currently lose_sum: 97.42760384082794
time_elpased: 1.627
batch start
#iterations: 45
currently lose_sum: 97.02993434667587
time_elpased: 1.656
batch start
#iterations: 46
currently lose_sum: 97.21843296289444
time_elpased: 1.632
batch start
#iterations: 47
currently lose_sum: 97.4792754650116
time_elpased: 1.635
batch start
#iterations: 48
currently lose_sum: 97.12674409151077
time_elpased: 1.67
batch start
#iterations: 49
currently lose_sum: 97.17007738351822
time_elpased: 1.682
batch start
#iterations: 50
currently lose_sum: 97.17981904745102
time_elpased: 1.66
batch start
#iterations: 51
currently lose_sum: 97.00678515434265
time_elpased: 1.637
batch start
#iterations: 52
currently lose_sum: 96.9522413611412
time_elpased: 1.64
batch start
#iterations: 53
currently lose_sum: 97.098448574543
time_elpased: 1.643
batch start
#iterations: 54
currently lose_sum: 96.65702074766159
time_elpased: 1.649
batch start
#iterations: 55
currently lose_sum: 96.89953756332397
time_elpased: 1.628
batch start
#iterations: 56
currently lose_sum: 96.88778537511826
time_elpased: 1.641
batch start
#iterations: 57
currently lose_sum: 96.69986432790756
time_elpased: 1.65
batch start
#iterations: 58
currently lose_sum: 96.79541045427322
time_elpased: 1.625
batch start
#iterations: 59
currently lose_sum: 96.61907535791397
time_elpased: 1.632
start validation test
0.583195876289
0.616450092817
0.444272923742
0.516387559809
0.583439776959
65.313
batch start
#iterations: 60
currently lose_sum: 96.7484769821167
time_elpased: 1.634
batch start
#iterations: 61
currently lose_sum: 96.89591157436371
time_elpased: 1.626
batch start
#iterations: 62
currently lose_sum: 96.52433520555496
time_elpased: 1.649
batch start
#iterations: 63
currently lose_sum: 96.35853815078735
time_elpased: 1.671
batch start
#iterations: 64
currently lose_sum: 96.47993350028992
time_elpased: 1.646
batch start
#iterations: 65
currently lose_sum: 96.23375660181046
time_elpased: 1.628
batch start
#iterations: 66
currently lose_sum: 96.22645872831345
time_elpased: 1.63
batch start
#iterations: 67
currently lose_sum: 96.2541692852974
time_elpased: 1.67
batch start
#iterations: 68
currently lose_sum: 96.1264163851738
time_elpased: 1.654
batch start
#iterations: 69
currently lose_sum: 95.89721912145615
time_elpased: 1.657
batch start
#iterations: 70
currently lose_sum: 95.71398931741714
time_elpased: 1.65
batch start
#iterations: 71
currently lose_sum: 96.15144693851471
time_elpased: 1.656
batch start
#iterations: 72
currently lose_sum: 95.45710253715515
time_elpased: 1.645
batch start
#iterations: 73
currently lose_sum: 95.80914157629013
time_elpased: 1.633
batch start
#iterations: 74
currently lose_sum: 95.55262732505798
time_elpased: 1.663
batch start
#iterations: 75
currently lose_sum: 95.74897289276123
time_elpased: 1.667
batch start
#iterations: 76
currently lose_sum: 95.93952310085297
time_elpased: 1.643
batch start
#iterations: 77
currently lose_sum: 95.43677186965942
time_elpased: 1.631
batch start
#iterations: 78
currently lose_sum: 95.89375180006027
time_elpased: 1.67
batch start
#iterations: 79
currently lose_sum: 95.7118906378746
time_elpased: 1.639
start validation test
0.577371134021
0.619867340493
0.403931254502
0.489127048414
0.57767563448
65.648
batch start
#iterations: 80
currently lose_sum: 96.1592606306076
time_elpased: 1.664
batch start
#iterations: 81
currently lose_sum: 95.64759856462479
time_elpased: 1.636
batch start
#iterations: 82
currently lose_sum: 95.51908850669861
time_elpased: 1.63
batch start
#iterations: 83
currently lose_sum: 95.19837582111359
time_elpased: 1.675
batch start
#iterations: 84
currently lose_sum: 95.3434990644455
time_elpased: 1.663
batch start
#iterations: 85
currently lose_sum: 95.46067833900452
time_elpased: 1.632
batch start
#iterations: 86
currently lose_sum: 95.31612575054169
time_elpased: 1.67
batch start
#iterations: 87
currently lose_sum: 95.39333999156952
time_elpased: 1.661
batch start
#iterations: 88
currently lose_sum: 95.4744416475296
time_elpased: 1.64
batch start
#iterations: 89
currently lose_sum: 95.36902850866318
time_elpased: 1.671
batch start
#iterations: 90
currently lose_sum: 94.83415311574936
time_elpased: 1.658
batch start
#iterations: 91
currently lose_sum: 94.86744982004166
time_elpased: 1.691
batch start
#iterations: 92
currently lose_sum: 95.31454598903656
time_elpased: 1.695
batch start
#iterations: 93
currently lose_sum: 94.79277157783508
time_elpased: 1.68
batch start
#iterations: 94
currently lose_sum: 94.8870238661766
time_elpased: 1.651
batch start
#iterations: 95
currently lose_sum: 95.22518301010132
time_elpased: 1.628
batch start
#iterations: 96
currently lose_sum: 94.79518747329712
time_elpased: 1.664
batch start
#iterations: 97
currently lose_sum: 94.65790724754333
time_elpased: 1.641
batch start
#iterations: 98
currently lose_sum: 94.91943907737732
time_elpased: 1.622
batch start
#iterations: 99
currently lose_sum: 94.48365199565887
time_elpased: 1.649
start validation test
0.580154639175
0.613126079447
0.438406915715
0.511251125113
0.580403499167
65.858
batch start
#iterations: 100
currently lose_sum: 94.97644072771072
time_elpased: 1.608
batch start
#iterations: 101
currently lose_sum: 94.37532895803452
time_elpased: 1.66
batch start
#iterations: 102
currently lose_sum: 94.38020706176758
time_elpased: 1.66
batch start
#iterations: 103
currently lose_sum: 94.33629763126373
time_elpased: 1.633
batch start
#iterations: 104
currently lose_sum: 94.44925367832184
time_elpased: 1.626
batch start
#iterations: 105
currently lose_sum: 94.26154607534409
time_elpased: 1.639
batch start
#iterations: 106
currently lose_sum: 94.48200982809067
time_elpased: 1.628
batch start
#iterations: 107
currently lose_sum: 93.87025147676468
time_elpased: 1.637
batch start
#iterations: 108
currently lose_sum: 94.08157050609589
time_elpased: 1.676
batch start
#iterations: 109
currently lose_sum: 94.34089696407318
time_elpased: 1.642
batch start
#iterations: 110
currently lose_sum: 94.27378928661346
time_elpased: 1.675
batch start
#iterations: 111
currently lose_sum: 94.05017232894897
time_elpased: 1.615
batch start
#iterations: 112
currently lose_sum: 93.93872666358948
time_elpased: 1.653
batch start
#iterations: 113
currently lose_sum: 93.59793531894684
time_elpased: 1.654
batch start
#iterations: 114
currently lose_sum: 93.59949505329132
time_elpased: 1.63
batch start
#iterations: 115
currently lose_sum: 93.77147960662842
time_elpased: 1.657
batch start
#iterations: 116
currently lose_sum: 93.85464787483215
time_elpased: 1.638
batch start
#iterations: 117
currently lose_sum: 93.95801371335983
time_elpased: 1.678
batch start
#iterations: 118
currently lose_sum: 93.73331326246262
time_elpased: 1.671
batch start
#iterations: 119
currently lose_sum: 93.1907474398613
time_elpased: 1.66
start validation test
0.572731958763
0.617087569695
0.387259442215
0.475877331647
0.573057584373
66.637
batch start
#iterations: 120
currently lose_sum: 93.70429295301437
time_elpased: 1.639
batch start
#iterations: 121
currently lose_sum: 93.30714321136475
time_elpased: 1.622
batch start
#iterations: 122
currently lose_sum: 93.67425417900085
time_elpased: 1.635
batch start
#iterations: 123
currently lose_sum: 93.45836853981018
time_elpased: 1.627
batch start
#iterations: 124
currently lose_sum: 93.97509336471558
time_elpased: 1.659
batch start
#iterations: 125
currently lose_sum: 93.62071186304092
time_elpased: 1.648
batch start
#iterations: 126
currently lose_sum: 93.18313282728195
time_elpased: 1.636
batch start
#iterations: 127
currently lose_sum: 93.12949591875076
time_elpased: 1.657
batch start
#iterations: 128
currently lose_sum: 93.11744153499603
time_elpased: 1.66
batch start
#iterations: 129
currently lose_sum: 93.60478836297989
time_elpased: 1.666
batch start
#iterations: 130
currently lose_sum: 93.06089901924133
time_elpased: 1.662
batch start
#iterations: 131
currently lose_sum: 93.33994960784912
time_elpased: 1.649
batch start
#iterations: 132
currently lose_sum: 92.89650040864944
time_elpased: 1.673
batch start
#iterations: 133
currently lose_sum: 93.41956299543381
time_elpased: 1.639
batch start
#iterations: 134
currently lose_sum: 93.04839313030243
time_elpased: 1.673
batch start
#iterations: 135
currently lose_sum: 93.40635567903519
time_elpased: 1.694
batch start
#iterations: 136
currently lose_sum: 92.56167376041412
time_elpased: 1.671
batch start
#iterations: 137
currently lose_sum: 92.79011404514313
time_elpased: 1.648
batch start
#iterations: 138
currently lose_sum: 92.61409389972687
time_elpased: 1.65
batch start
#iterations: 139
currently lose_sum: 92.5637543797493
time_elpased: 1.661
start validation test
0.57793814433
0.606491154757
0.448080683338
0.515388257576
0.578166129132
66.402
batch start
#iterations: 140
currently lose_sum: 92.83881962299347
time_elpased: 1.657
batch start
#iterations: 141
currently lose_sum: 92.77870404720306
time_elpased: 1.635
batch start
#iterations: 142
currently lose_sum: 92.58964395523071
time_elpased: 1.638
batch start
#iterations: 143
currently lose_sum: 92.59229165315628
time_elpased: 1.723
batch start
#iterations: 144
currently lose_sum: 92.41604620218277
time_elpased: 1.66
batch start
#iterations: 145
currently lose_sum: 92.73410296440125
time_elpased: 1.672
batch start
#iterations: 146
currently lose_sum: 92.65733814239502
time_elpased: 1.627
batch start
#iterations: 147
currently lose_sum: 92.82151198387146
time_elpased: 1.641
batch start
#iterations: 148
currently lose_sum: 92.28306376934052
time_elpased: 1.644
batch start
#iterations: 149
currently lose_sum: 92.49183088541031
time_elpased: 1.672
batch start
#iterations: 150
currently lose_sum: 92.29462748765945
time_elpased: 1.668
batch start
#iterations: 151
currently lose_sum: 92.05629169940948
time_elpased: 1.642
batch start
#iterations: 152
currently lose_sum: 92.56378298997879
time_elpased: 1.663
batch start
#iterations: 153
currently lose_sum: 92.44086319208145
time_elpased: 1.644
batch start
#iterations: 154
currently lose_sum: 92.07513689994812
time_elpased: 1.637
batch start
#iterations: 155
currently lose_sum: 91.95324748754501
time_elpased: 1.663
batch start
#iterations: 156
currently lose_sum: 92.17485576868057
time_elpased: 1.627
batch start
#iterations: 157
currently lose_sum: 91.97117191553116
time_elpased: 1.637
batch start
#iterations: 158
currently lose_sum: 91.86965119838715
time_elpased: 1.631
batch start
#iterations: 159
currently lose_sum: 91.9440381526947
time_elpased: 1.63
start validation test
0.565309278351
0.618713017751
0.344344962437
0.442446280992
0.565697215289
68.287
batch start
#iterations: 160
currently lose_sum: 91.799221098423
time_elpased: 1.655
batch start
#iterations: 161
currently lose_sum: 92.26134121417999
time_elpased: 1.702
batch start
#iterations: 162
currently lose_sum: 92.34204798936844
time_elpased: 1.654
batch start
#iterations: 163
currently lose_sum: 91.8862321972847
time_elpased: 1.67
batch start
#iterations: 164
currently lose_sum: 92.03786033391953
time_elpased: 1.691
batch start
#iterations: 165
currently lose_sum: 91.66590690612793
time_elpased: 1.665
batch start
#iterations: 166
currently lose_sum: 91.3657476902008
time_elpased: 1.658
batch start
#iterations: 167
currently lose_sum: 91.42253929376602
time_elpased: 1.643
batch start
#iterations: 168
currently lose_sum: 91.51662337779999
time_elpased: 1.672
batch start
#iterations: 169
currently lose_sum: 91.40364074707031
time_elpased: 1.641
batch start
#iterations: 170
currently lose_sum: 91.53075909614563
time_elpased: 1.669
batch start
#iterations: 171
currently lose_sum: 91.39816898107529
time_elpased: 1.659
batch start
#iterations: 172
currently lose_sum: 91.10647630691528
time_elpased: 1.644
batch start
#iterations: 173
currently lose_sum: 90.73017197847366
time_elpased: 1.663
batch start
#iterations: 174
currently lose_sum: 91.545558989048
time_elpased: 1.68
batch start
#iterations: 175
currently lose_sum: 91.2227411866188
time_elpased: 1.659
batch start
#iterations: 176
currently lose_sum: 91.06726211309433
time_elpased: 1.637
batch start
#iterations: 177
currently lose_sum: 91.30969554185867
time_elpased: 1.638
batch start
#iterations: 178
currently lose_sum: 91.1458198428154
time_elpased: 1.658
batch start
#iterations: 179
currently lose_sum: 90.77108609676361
time_elpased: 1.676
start validation test
0.570721649485
0.601135867191
0.424822476073
0.497829232996
0.570977797987
67.439
batch start
#iterations: 180
currently lose_sum: 91.63197189569473
time_elpased: 1.663
batch start
#iterations: 181
currently lose_sum: 90.53609782457352
time_elpased: 1.635
batch start
#iterations: 182
currently lose_sum: 91.01733732223511
time_elpased: 1.701
batch start
#iterations: 183
currently lose_sum: 91.16637670993805
time_elpased: 1.627
batch start
#iterations: 184
currently lose_sum: 90.95789283514023
time_elpased: 1.668
batch start
#iterations: 185
currently lose_sum: 90.50398987531662
time_elpased: 1.626
batch start
#iterations: 186
currently lose_sum: 90.5043198466301
time_elpased: 1.674
batch start
#iterations: 187
currently lose_sum: 90.33320242166519
time_elpased: 1.628
batch start
#iterations: 188
currently lose_sum: 90.69555765390396
time_elpased: 1.641
batch start
#iterations: 189
currently lose_sum: 91.00918608903885
time_elpased: 1.656
batch start
#iterations: 190
currently lose_sum: 91.38483548164368
time_elpased: 1.659
batch start
#iterations: 191
currently lose_sum: 90.52821749448776
time_elpased: 1.631
batch start
#iterations: 192
currently lose_sum: 90.30096328258514
time_elpased: 1.63
batch start
#iterations: 193
currently lose_sum: 90.17700672149658
time_elpased: 1.639
batch start
#iterations: 194
currently lose_sum: 90.24071723222733
time_elpased: 1.667
batch start
#iterations: 195
currently lose_sum: 90.19229882955551
time_elpased: 1.639
batch start
#iterations: 196
currently lose_sum: 90.60532480478287
time_elpased: 1.632
batch start
#iterations: 197
currently lose_sum: 90.90990841388702
time_elpased: 1.632
batch start
#iterations: 198
currently lose_sum: 90.25939118862152
time_elpased: 1.643
batch start
#iterations: 199
currently lose_sum: 90.22467291355133
time_elpased: 1.669
start validation test
0.575
0.620734908136
0.389420603067
0.478593562259
0.575325813255
68.092
batch start
#iterations: 200
currently lose_sum: 90.19771957397461
time_elpased: 1.617
batch start
#iterations: 201
currently lose_sum: 90.29488468170166
time_elpased: 1.63
batch start
#iterations: 202
currently lose_sum: 89.87733769416809
time_elpased: 1.637
batch start
#iterations: 203
currently lose_sum: 90.08107662200928
time_elpased: 1.627
batch start
#iterations: 204
currently lose_sum: 90.03230875730515
time_elpased: 1.619
batch start
#iterations: 205
currently lose_sum: 90.28909128904343
time_elpased: 1.618
batch start
#iterations: 206
currently lose_sum: 89.34094208478928
time_elpased: 1.657
batch start
#iterations: 207
currently lose_sum: 89.9555116891861
time_elpased: 1.636
batch start
#iterations: 208
currently lose_sum: 89.97143483161926
time_elpased: 1.69
batch start
#iterations: 209
currently lose_sum: 90.13071775436401
time_elpased: 1.675
batch start
#iterations: 210
currently lose_sum: 89.46536928415298
time_elpased: 1.634
batch start
#iterations: 211
currently lose_sum: 89.14714342355728
time_elpased: 1.682
batch start
#iterations: 212
currently lose_sum: 89.99351370334625
time_elpased: 1.617
batch start
#iterations: 213
currently lose_sum: 89.87203627824783
time_elpased: 1.669
batch start
#iterations: 214
currently lose_sum: 89.71486049890518
time_elpased: 1.642
batch start
#iterations: 215
currently lose_sum: 89.85139572620392
time_elpased: 1.662
batch start
#iterations: 216
currently lose_sum: 89.64186197519302
time_elpased: 1.635
batch start
#iterations: 217
currently lose_sum: 89.2575495839119
time_elpased: 1.647
batch start
#iterations: 218
currently lose_sum: 89.82153797149658
time_elpased: 1.644
batch start
#iterations: 219
currently lose_sum: 89.21906626224518
time_elpased: 1.642
start validation test
0.576546391753
0.595159655347
0.483379643923
0.53347719916
0.576709960348
67.873
batch start
#iterations: 220
currently lose_sum: 89.63881546258926
time_elpased: 1.642
batch start
#iterations: 221
currently lose_sum: 89.72384715080261
time_elpased: 1.68
batch start
#iterations: 222
currently lose_sum: 88.80662071704865
time_elpased: 1.74
batch start
#iterations: 223
currently lose_sum: 88.78051048517227
time_elpased: 1.637
batch start
#iterations: 224
currently lose_sum: 89.2932841181755
time_elpased: 1.65
batch start
#iterations: 225
currently lose_sum: 89.57098227739334
time_elpased: 1.625
batch start
#iterations: 226
currently lose_sum: 89.35307604074478
time_elpased: 1.641
batch start
#iterations: 227
currently lose_sum: 89.33886909484863
time_elpased: 1.69
batch start
#iterations: 228
currently lose_sum: 89.09725630283356
time_elpased: 1.668
batch start
#iterations: 229
currently lose_sum: 89.04442995786667
time_elpased: 1.655
batch start
#iterations: 230
currently lose_sum: 88.87605249881744
time_elpased: 1.629
batch start
#iterations: 231
currently lose_sum: 89.05313891172409
time_elpased: 1.655
batch start
#iterations: 232
currently lose_sum: 88.98267668485641
time_elpased: 1.637
batch start
#iterations: 233
currently lose_sum: 89.48444384336472
time_elpased: 1.633
batch start
#iterations: 234
currently lose_sum: 89.45971667766571
time_elpased: 1.64
batch start
#iterations: 235
currently lose_sum: 88.65868204832077
time_elpased: 1.65
batch start
#iterations: 236
currently lose_sum: 88.87860840559006
time_elpased: 1.658
batch start
#iterations: 237
currently lose_sum: 89.16911351680756
time_elpased: 1.621
batch start
#iterations: 238
currently lose_sum: 89.06529247760773
time_elpased: 1.62
batch start
#iterations: 239
currently lose_sum: 88.77361154556274
time_elpased: 1.67
start validation test
0.558505154639
0.611541440744
0.324997427189
0.424433841812
0.558915113471
70.627
batch start
#iterations: 240
currently lose_sum: 89.14071983098984
time_elpased: 1.64
batch start
#iterations: 241
currently lose_sum: 88.39830899238586
time_elpased: 1.628
batch start
#iterations: 242
currently lose_sum: 88.77329874038696
time_elpased: 1.655
batch start
#iterations: 243
currently lose_sum: 88.43794685602188
time_elpased: 1.632
batch start
#iterations: 244
currently lose_sum: 88.3022603392601
time_elpased: 1.665
batch start
#iterations: 245
currently lose_sum: 88.62243521213531
time_elpased: 1.625
batch start
#iterations: 246
currently lose_sum: 88.55109947919846
time_elpased: 1.653
batch start
#iterations: 247
currently lose_sum: 88.96584409475327
time_elpased: 1.655
batch start
#iterations: 248
currently lose_sum: 88.5221855044365
time_elpased: 1.626
batch start
#iterations: 249
currently lose_sum: 88.64763832092285
time_elpased: 1.66
batch start
#iterations: 250
currently lose_sum: 88.33658450841904
time_elpased: 1.71
batch start
#iterations: 251
currently lose_sum: 87.93302172422409
time_elpased: 1.661
batch start
#iterations: 252
currently lose_sum: 88.52282392978668
time_elpased: 1.632
batch start
#iterations: 253
currently lose_sum: 88.83449470996857
time_elpased: 1.641
batch start
#iterations: 254
currently lose_sum: 88.49337393045425
time_elpased: 1.62
batch start
#iterations: 255
currently lose_sum: 88.0856060385704
time_elpased: 1.647
batch start
#iterations: 256
currently lose_sum: 88.37101531028748
time_elpased: 1.657
batch start
#iterations: 257
currently lose_sum: 88.56788736581802
time_elpased: 1.67
batch start
#iterations: 258
currently lose_sum: 88.26689529418945
time_elpased: 1.648
batch start
#iterations: 259
currently lose_sum: 88.09679889678955
time_elpased: 1.652
start validation test
0.567164948454
0.601757631822
0.401667181229
0.481762636549
0.56745550531
69.607
batch start
#iterations: 260
currently lose_sum: 88.60515344142914
time_elpased: 1.647
batch start
#iterations: 261
currently lose_sum: 88.25739049911499
time_elpased: 1.629
batch start
#iterations: 262
currently lose_sum: 87.24543958902359
time_elpased: 1.658
batch start
#iterations: 263
currently lose_sum: 87.86608427762985
time_elpased: 1.652
batch start
#iterations: 264
currently lose_sum: 88.22566711902618
time_elpased: 1.657
batch start
#iterations: 265
currently lose_sum: 87.78422701358795
time_elpased: 1.686
batch start
#iterations: 266
currently lose_sum: 87.25075620412827
time_elpased: 1.656
batch start
#iterations: 267
currently lose_sum: 88.47684091329575
time_elpased: 1.644
batch start
#iterations: 268
currently lose_sum: 87.98709833621979
time_elpased: 1.645
batch start
#iterations: 269
currently lose_sum: 88.10784953832626
time_elpased: 1.647
batch start
#iterations: 270
currently lose_sum: 87.7875629067421
time_elpased: 1.668
batch start
#iterations: 271
currently lose_sum: 87.9574014544487
time_elpased: 1.638
batch start
#iterations: 272
currently lose_sum: 87.19443774223328
time_elpased: 1.678
batch start
#iterations: 273
currently lose_sum: 87.77912884950638
time_elpased: 1.655
batch start
#iterations: 274
currently lose_sum: 87.49936664104462
time_elpased: 1.641
batch start
#iterations: 275
currently lose_sum: 87.65130704641342
time_elpased: 1.655
batch start
#iterations: 276
currently lose_sum: 88.14495569467545
time_elpased: 1.638
batch start
#iterations: 277
currently lose_sum: 87.54322749376297
time_elpased: 1.665
batch start
#iterations: 278
currently lose_sum: 87.8118503689766
time_elpased: 1.637
batch start
#iterations: 279
currently lose_sum: 87.54092353582382
time_elpased: 1.637
start validation test
0.568144329897
0.595547309833
0.429453535042
0.499043291079
0.568387822979
70.246
batch start
#iterations: 280
currently lose_sum: 87.54409062862396
time_elpased: 1.677
batch start
#iterations: 281
currently lose_sum: 87.61291235685349
time_elpased: 1.643
batch start
#iterations: 282
currently lose_sum: 87.07290238142014
time_elpased: 1.676
batch start
#iterations: 283
currently lose_sum: 87.80971390008926
time_elpased: 1.626
batch start
#iterations: 284
currently lose_sum: 87.46951311826706
time_elpased: 1.645
batch start
#iterations: 285
currently lose_sum: 86.74198800325394
time_elpased: 1.618
batch start
#iterations: 286
currently lose_sum: 87.36022514104843
time_elpased: 1.659
batch start
#iterations: 287
currently lose_sum: 87.21335405111313
time_elpased: 1.64
batch start
#iterations: 288
currently lose_sum: 86.82543367147446
time_elpased: 1.636
batch start
#iterations: 289
currently lose_sum: 87.01957076787949
time_elpased: 1.638
batch start
#iterations: 290
currently lose_sum: 86.77254849672318
time_elpased: 1.675
batch start
#iterations: 291
currently lose_sum: 87.06767290830612
time_elpased: 1.639
batch start
#iterations: 292
currently lose_sum: 86.96805745363235
time_elpased: 1.633
batch start
#iterations: 293
currently lose_sum: 87.05098563432693
time_elpased: 1.649
batch start
#iterations: 294
currently lose_sum: 87.39701676368713
time_elpased: 1.653
batch start
#iterations: 295
currently lose_sum: 87.43257248401642
time_elpased: 1.689
batch start
#iterations: 296
currently lose_sum: 86.95456850528717
time_elpased: 1.677
batch start
#iterations: 297
currently lose_sum: 86.73906594514847
time_elpased: 1.633
batch start
#iterations: 298
currently lose_sum: 87.00454759597778
time_elpased: 1.636
batch start
#iterations: 299
currently lose_sum: 86.43482458591461
time_elpased: 1.674
start validation test
0.564072164948
0.590647482014
0.422455490378
0.492590148197
0.564320794864
70.191
batch start
#iterations: 300
currently lose_sum: 87.01357179880142
time_elpased: 1.656
batch start
#iterations: 301
currently lose_sum: 87.20240527391434
time_elpased: 1.633
batch start
#iterations: 302
currently lose_sum: 86.92489570379257
time_elpased: 1.631
batch start
#iterations: 303
currently lose_sum: 86.68545013666153
time_elpased: 1.664
batch start
#iterations: 304
currently lose_sum: 86.96291041374207
time_elpased: 1.633
batch start
#iterations: 305
currently lose_sum: 86.90809667110443
time_elpased: 1.665
batch start
#iterations: 306
currently lose_sum: 86.80079585313797
time_elpased: 1.682
batch start
#iterations: 307
currently lose_sum: 87.10560584068298
time_elpased: 1.666
batch start
#iterations: 308
currently lose_sum: 86.00097966194153
time_elpased: 1.643
batch start
#iterations: 309
currently lose_sum: 86.75754857063293
time_elpased: 1.629
batch start
#iterations: 310
currently lose_sum: 86.84128302335739
time_elpased: 1.668
batch start
#iterations: 311
currently lose_sum: 86.16986173391342
time_elpased: 1.641
batch start
#iterations: 312
currently lose_sum: 86.27030158042908
time_elpased: 1.66
batch start
#iterations: 313
currently lose_sum: 86.2669677734375
time_elpased: 1.678
batch start
#iterations: 314
currently lose_sum: 86.22087693214417
time_elpased: 1.647
batch start
#iterations: 315
currently lose_sum: 86.21248960494995
time_elpased: 1.67
batch start
#iterations: 316
currently lose_sum: 86.38479590415955
time_elpased: 1.647
batch start
#iterations: 317
currently lose_sum: 85.75345504283905
time_elpased: 1.646
batch start
#iterations: 318
currently lose_sum: 86.29646122455597
time_elpased: 1.691
batch start
#iterations: 319
currently lose_sum: 86.42156898975372
time_elpased: 1.635
start validation test
0.549896907216
0.6017772267
0.299680971493
0.400109920308
0.550336199885
74.953
batch start
#iterations: 320
currently lose_sum: 86.8091185092926
time_elpased: 1.667
batch start
#iterations: 321
currently lose_sum: 85.8617799282074
time_elpased: 1.64
batch start
#iterations: 322
currently lose_sum: 86.32606220245361
time_elpased: 1.627
batch start
#iterations: 323
currently lose_sum: 86.18536639213562
time_elpased: 1.664
batch start
#iterations: 324
currently lose_sum: 85.96453338861465
time_elpased: 1.664
batch start
#iterations: 325
currently lose_sum: 85.9048290848732
time_elpased: 1.655
batch start
#iterations: 326
currently lose_sum: 85.76409387588501
time_elpased: 1.629
batch start
#iterations: 327
currently lose_sum: 86.26785635948181
time_elpased: 1.645
batch start
#iterations: 328
currently lose_sum: 85.8397725224495
time_elpased: 1.633
batch start
#iterations: 329
currently lose_sum: 86.55649769306183
time_elpased: 1.689
batch start
#iterations: 330
currently lose_sum: 85.91514122486115
time_elpased: 1.636
batch start
#iterations: 331
currently lose_sum: 85.84032899141312
time_elpased: 1.67
batch start
#iterations: 332
currently lose_sum: 86.39571338891983
time_elpased: 1.636
batch start
#iterations: 333
currently lose_sum: 86.13732087612152
time_elpased: 1.709
batch start
#iterations: 334
currently lose_sum: 85.83183711767197
time_elpased: 1.62
batch start
#iterations: 335
currently lose_sum: 85.8149887919426
time_elpased: 1.608
batch start
#iterations: 336
currently lose_sum: 85.35556280612946
time_elpased: 1.662
batch start
#iterations: 337
currently lose_sum: 85.84099173545837
time_elpased: 1.692
batch start
#iterations: 338
currently lose_sum: 85.62288415431976
time_elpased: 1.636
batch start
#iterations: 339
currently lose_sum: 86.0007593035698
time_elpased: 1.687
start validation test
0.563092783505
0.589914505144
0.418956468046
0.489950655915
0.563345837038
71.486
batch start
#iterations: 340
currently lose_sum: 85.67493331432343
time_elpased: 1.65
batch start
#iterations: 341
currently lose_sum: 85.39562028646469
time_elpased: 1.635
batch start
#iterations: 342
currently lose_sum: 86.22841197252274
time_elpased: 1.657
batch start
#iterations: 343
currently lose_sum: 85.77950114011765
time_elpased: 1.639
batch start
#iterations: 344
currently lose_sum: 85.77287060022354
time_elpased: 1.614
batch start
#iterations: 345
currently lose_sum: 84.96289551258087
time_elpased: 1.63
batch start
#iterations: 346
currently lose_sum: 85.58192533254623
time_elpased: 1.628
batch start
#iterations: 347
currently lose_sum: 84.91508430242538
time_elpased: 1.634
batch start
#iterations: 348
currently lose_sum: 85.6554102897644
time_elpased: 1.634
batch start
#iterations: 349
currently lose_sum: 85.43944537639618
time_elpased: 1.685
batch start
#iterations: 350
currently lose_sum: 85.18237662315369
time_elpased: 1.646
batch start
#iterations: 351
currently lose_sum: 85.00482279062271
time_elpased: 1.639
batch start
#iterations: 352
currently lose_sum: 85.55439698696136
time_elpased: 1.666
batch start
#iterations: 353
currently lose_sum: 85.30508345365524
time_elpased: 1.626
batch start
#iterations: 354
currently lose_sum: 85.35000556707382
time_elpased: 1.664
batch start
#iterations: 355
currently lose_sum: 85.36340254545212
time_elpased: 1.66
batch start
#iterations: 356
currently lose_sum: 85.26133239269257
time_elpased: 1.622
batch start
#iterations: 357
currently lose_sum: 85.30669510364532
time_elpased: 1.639
batch start
#iterations: 358
currently lose_sum: 85.26928114891052
time_elpased: 1.666
batch start
#iterations: 359
currently lose_sum: 84.51904863119125
time_elpased: 1.645
start validation test
0.558041237113
0.591718825229
0.379438098178
0.462377727615
0.558354802471
72.107
batch start
#iterations: 360
currently lose_sum: 85.38078343868256
time_elpased: 1.637
batch start
#iterations: 361
currently lose_sum: 85.10167855024338
time_elpased: 1.673
batch start
#iterations: 362
currently lose_sum: 85.11406219005585
time_elpased: 1.661
batch start
#iterations: 363
currently lose_sum: 84.93945354223251
time_elpased: 1.646
batch start
#iterations: 364
currently lose_sum: 85.26560419797897
time_elpased: 1.642
batch start
#iterations: 365
currently lose_sum: 85.36776214838028
time_elpased: 1.643
batch start
#iterations: 366
currently lose_sum: 84.62370699644089
time_elpased: 1.633
batch start
#iterations: 367
currently lose_sum: 84.84815502166748
time_elpased: 1.655
batch start
#iterations: 368
currently lose_sum: 84.67990988492966
time_elpased: 1.641
batch start
#iterations: 369
currently lose_sum: 85.53441214561462
time_elpased: 1.652
batch start
#iterations: 370
currently lose_sum: 84.89337408542633
time_elpased: 1.636
batch start
#iterations: 371
currently lose_sum: 84.5008994936943
time_elpased: 1.651
batch start
#iterations: 372
currently lose_sum: 85.18020874261856
time_elpased: 1.649
batch start
#iterations: 373
currently lose_sum: 84.70887362957001
time_elpased: 1.647
batch start
#iterations: 374
currently lose_sum: 85.04605984687805
time_elpased: 1.636
batch start
#iterations: 375
currently lose_sum: 84.81923586130142
time_elpased: 1.649
batch start
#iterations: 376
currently lose_sum: 84.83627605438232
time_elpased: 1.63
batch start
#iterations: 377
currently lose_sum: 85.10953509807587
time_elpased: 1.634
batch start
#iterations: 378
currently lose_sum: 84.99004769325256
time_elpased: 1.67
batch start
#iterations: 379
currently lose_sum: 84.66860455274582
time_elpased: 1.63
start validation test
0.562989690722
0.595351700785
0.398065246475
0.477118539534
0.563279241021
71.989
batch start
#iterations: 380
currently lose_sum: 84.26529562473297
time_elpased: 1.673
batch start
#iterations: 381
currently lose_sum: 84.90796864032745
time_elpased: 1.635
batch start
#iterations: 382
currently lose_sum: 84.25814020633698
time_elpased: 1.631
batch start
#iterations: 383
currently lose_sum: 84.90804249048233
time_elpased: 1.636
batch start
#iterations: 384
currently lose_sum: 84.9352440237999
time_elpased: 1.659
batch start
#iterations: 385
currently lose_sum: 83.9884979724884
time_elpased: 1.661
batch start
#iterations: 386
currently lose_sum: 84.18268233537674
time_elpased: 1.641
batch start
#iterations: 387
currently lose_sum: 84.05413097143173
time_elpased: 1.661
batch start
#iterations: 388
currently lose_sum: 84.70539754629135
time_elpased: 1.661
batch start
#iterations: 389
currently lose_sum: 84.81146758794785
time_elpased: 1.642
batch start
#iterations: 390
currently lose_sum: 84.38531649112701
time_elpased: 1.648
batch start
#iterations: 391
currently lose_sum: 83.94503104686737
time_elpased: 1.678
batch start
#iterations: 392
currently lose_sum: 84.07351100444794
time_elpased: 1.631
batch start
#iterations: 393
currently lose_sum: 84.25581377744675
time_elpased: 1.647
batch start
#iterations: 394
currently lose_sum: 84.17165982723236
time_elpased: 1.628
batch start
#iterations: 395
currently lose_sum: 83.88067227602005
time_elpased: 1.656
batch start
#iterations: 396
currently lose_sum: 84.7343555688858
time_elpased: 1.65
batch start
#iterations: 397
currently lose_sum: 84.78348726034164
time_elpased: 1.621
batch start
#iterations: 398
currently lose_sum: 83.81354141235352
time_elpased: 1.631
batch start
#iterations: 399
currently lose_sum: 83.95189243555069
time_elpased: 1.625
start validation test
0.564432989691
0.591295575731
0.422249665535
0.4926753122
0.564682614447
71.520
acc: 0.600
pre: 0.620
rec: 0.521
F1: 0.566
auc: 0.639
