start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.83668327331543
time_elpased: 1.622
batch start
#iterations: 1
currently lose_sum: 100.03602111339569
time_elpased: 1.663
batch start
#iterations: 2
currently lose_sum: 99.81926721334457
time_elpased: 1.633
batch start
#iterations: 3
currently lose_sum: 99.60469484329224
time_elpased: 1.641
batch start
#iterations: 4
currently lose_sum: 99.38552850484848
time_elpased: 1.594
batch start
#iterations: 5
currently lose_sum: 99.32338207960129
time_elpased: 1.62
batch start
#iterations: 6
currently lose_sum: 99.2498699426651
time_elpased: 1.632
batch start
#iterations: 7
currently lose_sum: 99.37743943929672
time_elpased: 1.612
batch start
#iterations: 8
currently lose_sum: 99.00535714626312
time_elpased: 1.601
batch start
#iterations: 9
currently lose_sum: 99.0418108701706
time_elpased: 1.609
batch start
#iterations: 10
currently lose_sum: 98.9377453327179
time_elpased: 1.62
batch start
#iterations: 11
currently lose_sum: 98.96920138597488
time_elpased: 1.626
batch start
#iterations: 12
currently lose_sum: 98.95463424921036
time_elpased: 1.611
batch start
#iterations: 13
currently lose_sum: 98.46740025281906
time_elpased: 1.666
batch start
#iterations: 14
currently lose_sum: 98.59734380245209
time_elpased: 1.618
batch start
#iterations: 15
currently lose_sum: 98.55961066484451
time_elpased: 1.622
batch start
#iterations: 16
currently lose_sum: 98.42797672748566
time_elpased: 1.62
batch start
#iterations: 17
currently lose_sum: 98.69754660129547
time_elpased: 1.635
batch start
#iterations: 18
currently lose_sum: 97.99206531047821
time_elpased: 1.619
batch start
#iterations: 19
currently lose_sum: 98.38737612962723
time_elpased: 1.638
start validation test
0.591494845361
0.58956417433
0.60697746218
0.598144110339
0.591467663239
65.075
batch start
#iterations: 20
currently lose_sum: 98.45191764831543
time_elpased: 1.633
batch start
#iterations: 21
currently lose_sum: 98.13615322113037
time_elpased: 1.586
batch start
#iterations: 22
currently lose_sum: 98.56088757514954
time_elpased: 1.599
batch start
#iterations: 23
currently lose_sum: 98.13053703308105
time_elpased: 1.637
batch start
#iterations: 24
currently lose_sum: 98.18666791915894
time_elpased: 1.613
batch start
#iterations: 25
currently lose_sum: 98.16354674100876
time_elpased: 1.658
batch start
#iterations: 26
currently lose_sum: 98.16633027791977
time_elpased: 1.631
batch start
#iterations: 27
currently lose_sum: 97.96224248409271
time_elpased: 1.618
batch start
#iterations: 28
currently lose_sum: 98.01093256473541
time_elpased: 1.61
batch start
#iterations: 29
currently lose_sum: 98.05957007408142
time_elpased: 1.594
batch start
#iterations: 30
currently lose_sum: 97.95271635055542
time_elpased: 1.642
batch start
#iterations: 31
currently lose_sum: 97.93583315610886
time_elpased: 1.632
batch start
#iterations: 32
currently lose_sum: 97.78366911411285
time_elpased: 1.603
batch start
#iterations: 33
currently lose_sum: 97.78171223402023
time_elpased: 1.62
batch start
#iterations: 34
currently lose_sum: 97.59365117549896
time_elpased: 1.645
batch start
#iterations: 35
currently lose_sum: 97.72483998537064
time_elpased: 1.588
batch start
#iterations: 36
currently lose_sum: 97.47880589962006
time_elpased: 1.616
batch start
#iterations: 37
currently lose_sum: 97.43916708230972
time_elpased: 1.623
batch start
#iterations: 38
currently lose_sum: 97.55517268180847
time_elpased: 1.607
batch start
#iterations: 39
currently lose_sum: 97.477583527565
time_elpased: 1.613
start validation test
0.594742268041
0.612274543034
0.520531028095
0.562687729447
0.594872557319
64.770
batch start
#iterations: 40
currently lose_sum: 97.33357256650925
time_elpased: 1.625
batch start
#iterations: 41
currently lose_sum: 97.47199702262878
time_elpased: 1.607
batch start
#iterations: 42
currently lose_sum: 97.29502040147781
time_elpased: 1.615
batch start
#iterations: 43
currently lose_sum: 97.19025474786758
time_elpased: 1.596
batch start
#iterations: 44
currently lose_sum: 97.05480235815048
time_elpased: 1.618
batch start
#iterations: 45
currently lose_sum: 97.10621964931488
time_elpased: 1.598
batch start
#iterations: 46
currently lose_sum: 97.19438791275024
time_elpased: 1.645
batch start
#iterations: 47
currently lose_sum: 96.89080083370209
time_elpased: 1.632
batch start
#iterations: 48
currently lose_sum: 97.16702860593796
time_elpased: 1.602
batch start
#iterations: 49
currently lose_sum: 97.07880038022995
time_elpased: 1.606
batch start
#iterations: 50
currently lose_sum: 96.89728301763535
time_elpased: 1.613
batch start
#iterations: 51
currently lose_sum: 97.08471500873566
time_elpased: 1.611
batch start
#iterations: 52
currently lose_sum: 96.93990081548691
time_elpased: 1.685
batch start
#iterations: 53
currently lose_sum: 96.86674571037292
time_elpased: 1.612
batch start
#iterations: 54
currently lose_sum: 96.72260147333145
time_elpased: 1.632
batch start
#iterations: 55
currently lose_sum: 96.88722485303879
time_elpased: 1.635
batch start
#iterations: 56
currently lose_sum: 96.62210935354233
time_elpased: 1.619
batch start
#iterations: 57
currently lose_sum: 96.92042380571365
time_elpased: 1.607
batch start
#iterations: 58
currently lose_sum: 96.83537304401398
time_elpased: 1.623
batch start
#iterations: 59
currently lose_sum: 96.6657183766365
time_elpased: 1.615
start validation test
0.58
0.616342874092
0.427704023876
0.504981773998
0.580267379076
65.046
batch start
#iterations: 60
currently lose_sum: 96.57225459814072
time_elpased: 1.597
batch start
#iterations: 61
currently lose_sum: 96.68386107683182
time_elpased: 1.608
batch start
#iterations: 62
currently lose_sum: 96.42016178369522
time_elpased: 1.601
batch start
#iterations: 63
currently lose_sum: 96.53703618049622
time_elpased: 1.623
batch start
#iterations: 64
currently lose_sum: 96.38290601968765
time_elpased: 1.628
batch start
#iterations: 65
currently lose_sum: 96.40577095746994
time_elpased: 1.61
batch start
#iterations: 66
currently lose_sum: 96.08459675312042
time_elpased: 1.65
batch start
#iterations: 67
currently lose_sum: 96.37531685829163
time_elpased: 1.617
batch start
#iterations: 68
currently lose_sum: 96.21889650821686
time_elpased: 1.675
batch start
#iterations: 69
currently lose_sum: 96.16217803955078
time_elpased: 1.618
batch start
#iterations: 70
currently lose_sum: 95.79475438594818
time_elpased: 1.605
batch start
#iterations: 71
currently lose_sum: 95.972452044487
time_elpased: 1.594
batch start
#iterations: 72
currently lose_sum: 96.08844703435898
time_elpased: 1.627
batch start
#iterations: 73
currently lose_sum: 95.68995362520218
time_elpased: 1.609
batch start
#iterations: 74
currently lose_sum: 95.96029418706894
time_elpased: 1.598
batch start
#iterations: 75
currently lose_sum: 95.84866154193878
time_elpased: 1.639
batch start
#iterations: 76
currently lose_sum: 95.82404828071594
time_elpased: 1.605
batch start
#iterations: 77
currently lose_sum: 95.89312899112701
time_elpased: 1.599
batch start
#iterations: 78
currently lose_sum: 96.0389918088913
time_elpased: 1.638
batch start
#iterations: 79
currently lose_sum: 95.75242894887924
time_elpased: 1.61
start validation test
0.574948453608
0.624682149517
0.379232273335
0.471951844262
0.57529206355
65.651
batch start
#iterations: 80
currently lose_sum: 95.41932964324951
time_elpased: 1.672
batch start
#iterations: 81
currently lose_sum: 95.3634661436081
time_elpased: 1.772
batch start
#iterations: 82
currently lose_sum: 95.2145968079567
time_elpased: 1.644
batch start
#iterations: 83
currently lose_sum: 95.38406002521515
time_elpased: 1.605
batch start
#iterations: 84
currently lose_sum: 95.37263041734695
time_elpased: 1.616
batch start
#iterations: 85
currently lose_sum: 95.08974325656891
time_elpased: 1.607
batch start
#iterations: 86
currently lose_sum: 95.27941590547562
time_elpased: 1.677
batch start
#iterations: 87
currently lose_sum: 95.12567609548569
time_elpased: 1.639
batch start
#iterations: 88
currently lose_sum: 95.19241803884506
time_elpased: 1.623
batch start
#iterations: 89
currently lose_sum: 94.94460690021515
time_elpased: 1.593
batch start
#iterations: 90
currently lose_sum: 94.90416091680527
time_elpased: 1.649
batch start
#iterations: 91
currently lose_sum: 95.15652161836624
time_elpased: 1.601
batch start
#iterations: 92
currently lose_sum: 94.97824537754059
time_elpased: 1.62
batch start
#iterations: 93
currently lose_sum: 95.0558266043663
time_elpased: 1.622
batch start
#iterations: 94
currently lose_sum: 94.85903519392014
time_elpased: 1.666
batch start
#iterations: 95
currently lose_sum: 94.94176292419434
time_elpased: 1.613
batch start
#iterations: 96
currently lose_sum: 95.17055439949036
time_elpased: 1.618
batch start
#iterations: 97
currently lose_sum: 94.81823867559433
time_elpased: 1.642
batch start
#iterations: 98
currently lose_sum: 94.34966045618057
time_elpased: 1.595
batch start
#iterations: 99
currently lose_sum: 94.45594727993011
time_elpased: 1.612
start validation test
0.562886597938
0.62333000997
0.321704229701
0.42438229704
0.563310030785
66.914
batch start
#iterations: 100
currently lose_sum: 94.69081425666809
time_elpased: 1.636
batch start
#iterations: 101
currently lose_sum: 94.77540773153305
time_elpased: 1.616
batch start
#iterations: 102
currently lose_sum: 94.43053758144379
time_elpased: 1.595
batch start
#iterations: 103
currently lose_sum: 94.49140721559525
time_elpased: 1.607
batch start
#iterations: 104
currently lose_sum: 94.18753480911255
time_elpased: 1.618
batch start
#iterations: 105
currently lose_sum: 94.02613347768784
time_elpased: 1.619
batch start
#iterations: 106
currently lose_sum: 94.14883297681808
time_elpased: 1.618
batch start
#iterations: 107
currently lose_sum: 94.17815560102463
time_elpased: 1.634
batch start
#iterations: 108
currently lose_sum: 94.08573949337006
time_elpased: 1.624
batch start
#iterations: 109
currently lose_sum: 94.235611140728
time_elpased: 1.606
batch start
#iterations: 110
currently lose_sum: 94.2816202044487
time_elpased: 1.664
batch start
#iterations: 111
currently lose_sum: 94.082923412323
time_elpased: 1.627
batch start
#iterations: 112
currently lose_sum: 93.9134983420372
time_elpased: 1.609
batch start
#iterations: 113
currently lose_sum: 93.69196230173111
time_elpased: 1.632
batch start
#iterations: 114
currently lose_sum: 93.69900089502335
time_elpased: 1.612
batch start
#iterations: 115
currently lose_sum: 94.10064804553986
time_elpased: 1.649
batch start
#iterations: 116
currently lose_sum: 93.97030478715897
time_elpased: 1.618
batch start
#iterations: 117
currently lose_sum: 93.73409128189087
time_elpased: 1.621
batch start
#iterations: 118
currently lose_sum: 93.67326670885086
time_elpased: 1.65
batch start
#iterations: 119
currently lose_sum: 93.62375992536545
time_elpased: 1.598
start validation test
0.570463917526
0.601288056206
0.422764227642
0.496465043205
0.570723227112
66.394
batch start
#iterations: 120
currently lose_sum: 93.81204181909561
time_elpased: 1.613
batch start
#iterations: 121
currently lose_sum: 93.16845917701721
time_elpased: 1.625
batch start
#iterations: 122
currently lose_sum: 93.91150277853012
time_elpased: 1.593
batch start
#iterations: 123
currently lose_sum: 93.1553573012352
time_elpased: 1.601
batch start
#iterations: 124
currently lose_sum: 93.34754067659378
time_elpased: 1.614
batch start
#iterations: 125
currently lose_sum: 93.62707763910294
time_elpased: 1.64
batch start
#iterations: 126
currently lose_sum: 93.28903478384018
time_elpased: 1.628
batch start
#iterations: 127
currently lose_sum: 93.31793719530106
time_elpased: 1.667
batch start
#iterations: 128
currently lose_sum: 93.04845821857452
time_elpased: 1.668
batch start
#iterations: 129
currently lose_sum: 93.10923218727112
time_elpased: 1.611
batch start
#iterations: 130
currently lose_sum: 93.59498208761215
time_elpased: 1.611
batch start
#iterations: 131
currently lose_sum: 93.03912729024887
time_elpased: 1.639
batch start
#iterations: 132
currently lose_sum: 92.82741767168045
time_elpased: 1.609
batch start
#iterations: 133
currently lose_sum: 92.64365082979202
time_elpased: 1.591
batch start
#iterations: 134
currently lose_sum: 93.03088623285294
time_elpased: 1.646
batch start
#iterations: 135
currently lose_sum: 93.07582432031631
time_elpased: 1.619
batch start
#iterations: 136
currently lose_sum: 93.16165447235107
time_elpased: 1.623
batch start
#iterations: 137
currently lose_sum: 93.01609027385712
time_elpased: 1.616
batch start
#iterations: 138
currently lose_sum: 92.49072152376175
time_elpased: 1.651
batch start
#iterations: 139
currently lose_sum: 92.7598911523819
time_elpased: 1.633
start validation test
0.570670103093
0.606670765447
0.406195327776
0.48659310855
0.570958863929
66.589
batch start
#iterations: 140
currently lose_sum: 92.62197637557983
time_elpased: 1.604
batch start
#iterations: 141
currently lose_sum: 92.17521768808365
time_elpased: 1.607
batch start
#iterations: 142
currently lose_sum: 93.26819676160812
time_elpased: 1.623
batch start
#iterations: 143
currently lose_sum: 92.3585387468338
time_elpased: 1.608
batch start
#iterations: 144
currently lose_sum: 92.60354155302048
time_elpased: 1.628
batch start
#iterations: 145
currently lose_sum: 92.83981275558472
time_elpased: 1.6
batch start
#iterations: 146
currently lose_sum: 92.47423899173737
time_elpased: 1.614
batch start
#iterations: 147
currently lose_sum: 91.97496724128723
time_elpased: 1.612
batch start
#iterations: 148
currently lose_sum: 92.61115324497223
time_elpased: 1.626
batch start
#iterations: 149
currently lose_sum: 92.35772800445557
time_elpased: 1.611
batch start
#iterations: 150
currently lose_sum: 92.32915598154068
time_elpased: 1.622
batch start
#iterations: 151
currently lose_sum: 92.31667977571487
time_elpased: 1.616
batch start
#iterations: 152
currently lose_sum: 92.33732908964157
time_elpased: 1.621
batch start
#iterations: 153
currently lose_sum: 91.99938929080963
time_elpased: 1.621
batch start
#iterations: 154
currently lose_sum: 92.5531587600708
time_elpased: 1.609
batch start
#iterations: 155
currently lose_sum: 91.89288526773453
time_elpased: 1.604
batch start
#iterations: 156
currently lose_sum: 91.74428427219391
time_elpased: 1.614
batch start
#iterations: 157
currently lose_sum: 92.35161054134369
time_elpased: 1.63
batch start
#iterations: 158
currently lose_sum: 92.44310337305069
time_elpased: 1.607
batch start
#iterations: 159
currently lose_sum: 92.183034658432
time_elpased: 1.61
start validation test
0.568659793814
0.600656618415
0.414222496655
0.49031550737
0.56893093231
66.859
batch start
#iterations: 160
currently lose_sum: 91.95054471492767
time_elpased: 1.618
batch start
#iterations: 161
currently lose_sum: 91.93023377656937
time_elpased: 1.597
batch start
#iterations: 162
currently lose_sum: 92.06009984016418
time_elpased: 1.645
batch start
#iterations: 163
currently lose_sum: 91.45117002725601
time_elpased: 1.64
batch start
#iterations: 164
currently lose_sum: 91.97394162416458
time_elpased: 1.594
batch start
#iterations: 165
currently lose_sum: 91.82218915224075
time_elpased: 1.633
batch start
#iterations: 166
currently lose_sum: 91.42900955677032
time_elpased: 1.65
batch start
#iterations: 167
currently lose_sum: 91.74168992042542
time_elpased: 1.601
batch start
#iterations: 168
currently lose_sum: 91.05042803287506
time_elpased: 1.627
batch start
#iterations: 169
currently lose_sum: 91.1661548614502
time_elpased: 1.607
batch start
#iterations: 170
currently lose_sum: 91.27767819166183
time_elpased: 1.653
batch start
#iterations: 171
currently lose_sum: 91.56487184762955
time_elpased: 1.613
batch start
#iterations: 172
currently lose_sum: 91.30854773521423
time_elpased: 1.634
batch start
#iterations: 173
currently lose_sum: 91.44586598873138
time_elpased: 1.613
batch start
#iterations: 174
currently lose_sum: 91.38497465848923
time_elpased: 1.615
batch start
#iterations: 175
currently lose_sum: 90.74541169404984
time_elpased: 1.627
batch start
#iterations: 176
currently lose_sum: 91.42788422107697
time_elpased: 1.639
batch start
#iterations: 177
currently lose_sum: 91.57635855674744
time_elpased: 1.608
batch start
#iterations: 178
currently lose_sum: 91.56390118598938
time_elpased: 1.656
batch start
#iterations: 179
currently lose_sum: 91.22806966304779
time_elpased: 1.619
start validation test
0.575824742268
0.600377765785
0.457960275805
0.519586665888
0.576031671518
66.857
batch start
#iterations: 180
currently lose_sum: 90.66779416799545
time_elpased: 1.6
batch start
#iterations: 181
currently lose_sum: 90.73954963684082
time_elpased: 1.635
batch start
#iterations: 182
currently lose_sum: 91.1827118396759
time_elpased: 1.649
batch start
#iterations: 183
currently lose_sum: 90.26027327775955
time_elpased: 1.643
batch start
#iterations: 184
currently lose_sum: 90.87092185020447
time_elpased: 1.628
batch start
#iterations: 185
currently lose_sum: 90.95244479179382
time_elpased: 1.598
batch start
#iterations: 186
currently lose_sum: 90.71888226270676
time_elpased: 1.627
batch start
#iterations: 187
currently lose_sum: 90.65095680952072
time_elpased: 1.633
batch start
#iterations: 188
currently lose_sum: 90.7593521475792
time_elpased: 1.609
batch start
#iterations: 189
currently lose_sum: 90.75953179597855
time_elpased: 1.598
batch start
#iterations: 190
currently lose_sum: 90.9454955458641
time_elpased: 1.614
batch start
#iterations: 191
currently lose_sum: 90.46659469604492
time_elpased: 1.642
batch start
#iterations: 192
currently lose_sum: 90.55939823389053
time_elpased: 1.609
batch start
#iterations: 193
currently lose_sum: 90.39377802610397
time_elpased: 1.64
batch start
#iterations: 194
currently lose_sum: 90.41610395908356
time_elpased: 1.603
batch start
#iterations: 195
currently lose_sum: 90.41743069887161
time_elpased: 1.597
batch start
#iterations: 196
currently lose_sum: 90.30529683828354
time_elpased: 1.628
batch start
#iterations: 197
currently lose_sum: 90.72567749023438
time_elpased: 1.593
batch start
#iterations: 198
currently lose_sum: 90.2842760682106
time_elpased: 1.606
batch start
#iterations: 199
currently lose_sum: 90.45675760507584
time_elpased: 1.643
start validation test
0.553350515464
0.627299128751
0.266748996604
0.374323055816
0.553853688636
70.729
batch start
#iterations: 200
currently lose_sum: 90.59425747394562
time_elpased: 1.611
batch start
#iterations: 201
currently lose_sum: 90.0461773276329
time_elpased: 1.601
batch start
#iterations: 202
currently lose_sum: 89.93789887428284
time_elpased: 1.606
batch start
#iterations: 203
currently lose_sum: 89.9707242846489
time_elpased: 1.637
batch start
#iterations: 204
currently lose_sum: 89.93485087156296
time_elpased: 1.607
batch start
#iterations: 205
currently lose_sum: 90.06184554100037
time_elpased: 1.617
batch start
#iterations: 206
currently lose_sum: 89.90749281644821
time_elpased: 1.63
batch start
#iterations: 207
currently lose_sum: 89.77026605606079
time_elpased: 1.617
batch start
#iterations: 208
currently lose_sum: 90.17019474506378
time_elpased: 1.632
batch start
#iterations: 209
currently lose_sum: 89.20482349395752
time_elpased: 1.619
batch start
#iterations: 210
currently lose_sum: 89.48059034347534
time_elpased: 1.629
batch start
#iterations: 211
currently lose_sum: 89.5144636631012
time_elpased: 1.603
batch start
#iterations: 212
currently lose_sum: 89.67915618419647
time_elpased: 1.672
batch start
#iterations: 213
currently lose_sum: 89.99086552858353
time_elpased: 1.632
batch start
#iterations: 214
currently lose_sum: 89.64052653312683
time_elpased: 1.635
batch start
#iterations: 215
currently lose_sum: 89.61692237854004
time_elpased: 1.65
batch start
#iterations: 216
currently lose_sum: 89.24315458536148
time_elpased: 1.615
batch start
#iterations: 217
currently lose_sum: 89.7548817396164
time_elpased: 1.622
batch start
#iterations: 218
currently lose_sum: 89.4917299747467
time_elpased: 1.624
batch start
#iterations: 219
currently lose_sum: 89.5047996044159
time_elpased: 1.634
start validation test
0.572474226804
0.589104571071
0.484100030874
0.531465371145
0.572629381336
67.641
batch start
#iterations: 220
currently lose_sum: 89.34688240289688
time_elpased: 1.628
batch start
#iterations: 221
currently lose_sum: 89.53551530838013
time_elpased: 1.634
batch start
#iterations: 222
currently lose_sum: 89.01071727275848
time_elpased: 1.681
batch start
#iterations: 223
currently lose_sum: 89.30157297849655
time_elpased: 1.637
batch start
#iterations: 224
currently lose_sum: 88.96374654769897
time_elpased: 1.647
batch start
#iterations: 225
currently lose_sum: 88.94604420661926
time_elpased: 1.663
batch start
#iterations: 226
currently lose_sum: 89.03153252601624
time_elpased: 1.617
batch start
#iterations: 227
currently lose_sum: 89.30198627710342
time_elpased: 1.606
batch start
#iterations: 228
currently lose_sum: 89.14940530061722
time_elpased: 1.608
batch start
#iterations: 229
currently lose_sum: 89.12670642137527
time_elpased: 1.62
batch start
#iterations: 230
currently lose_sum: 89.00811696052551
time_elpased: 1.61
batch start
#iterations: 231
currently lose_sum: 88.92575639486313
time_elpased: 1.624
batch start
#iterations: 232
currently lose_sum: 89.31531971693039
time_elpased: 1.671
batch start
#iterations: 233
currently lose_sum: 89.06214678287506
time_elpased: 1.67
batch start
#iterations: 234
currently lose_sum: 88.87420934438705
time_elpased: 1.631
batch start
#iterations: 235
currently lose_sum: 88.80419850349426
time_elpased: 1.652
batch start
#iterations: 236
currently lose_sum: 88.98113375902176
time_elpased: 1.625
batch start
#iterations: 237
currently lose_sum: 89.00974154472351
time_elpased: 1.587
batch start
#iterations: 238
currently lose_sum: 88.8659331202507
time_elpased: 1.675
batch start
#iterations: 239
currently lose_sum: 89.0130809545517
time_elpased: 1.655
start validation test
0.568453608247
0.596360510102
0.428321498405
0.498562529947
0.56869963178
69.366
batch start
#iterations: 240
currently lose_sum: 89.11005783081055
time_elpased: 1.629
batch start
#iterations: 241
currently lose_sum: 88.41129678487778
time_elpased: 1.616
batch start
#iterations: 242
currently lose_sum: 88.14420926570892
time_elpased: 1.631
batch start
#iterations: 243
currently lose_sum: 88.9989265203476
time_elpased: 1.628
batch start
#iterations: 244
currently lose_sum: 88.30893331766129
time_elpased: 1.625
batch start
#iterations: 245
currently lose_sum: 88.3951855301857
time_elpased: 1.617
batch start
#iterations: 246
currently lose_sum: 88.79252904653549
time_elpased: 1.604
batch start
#iterations: 247
currently lose_sum: 88.57085806131363
time_elpased: 1.601
batch start
#iterations: 248
currently lose_sum: 88.48925769329071
time_elpased: 1.627
batch start
#iterations: 249
currently lose_sum: 88.80648177862167
time_elpased: 1.643
batch start
#iterations: 250
currently lose_sum: 88.83292353153229
time_elpased: 1.65
batch start
#iterations: 251
currently lose_sum: 88.29972088336945
time_elpased: 1.619
batch start
#iterations: 252
currently lose_sum: 88.19699138402939
time_elpased: 1.627
batch start
#iterations: 253
currently lose_sum: 87.95603114366531
time_elpased: 1.608
batch start
#iterations: 254
currently lose_sum: 88.15607589483261
time_elpased: 1.623
batch start
#iterations: 255
currently lose_sum: 88.14857125282288
time_elpased: 1.599
batch start
#iterations: 256
currently lose_sum: 88.05805897712708
time_elpased: 1.625
batch start
#iterations: 257
currently lose_sum: 88.24318355321884
time_elpased: 1.637
batch start
#iterations: 258
currently lose_sum: 87.76000553369522
time_elpased: 1.67
batch start
#iterations: 259
currently lose_sum: 87.35501354932785
time_elpased: 1.638
start validation test
0.568298969072
0.583003463632
0.485026242667
0.529520813437
0.568445167187
69.205
batch start
#iterations: 260
currently lose_sum: 87.8992577791214
time_elpased: 1.621
batch start
#iterations: 261
currently lose_sum: 87.35727435350418
time_elpased: 1.586
batch start
#iterations: 262
currently lose_sum: 87.6235368847847
time_elpased: 1.622
batch start
#iterations: 263
currently lose_sum: 88.04550939798355
time_elpased: 1.613
batch start
#iterations: 264
currently lose_sum: 87.90200608968735
time_elpased: 1.619
batch start
#iterations: 265
currently lose_sum: 88.16007560491562
time_elpased: 1.644
batch start
#iterations: 266
currently lose_sum: 87.90701907873154
time_elpased: 1.615
batch start
#iterations: 267
currently lose_sum: 88.02389949560165
time_elpased: 1.65
batch start
#iterations: 268
currently lose_sum: 87.76851397752762
time_elpased: 1.613
batch start
#iterations: 269
currently lose_sum: 87.82254475355148
time_elpased: 1.603
batch start
#iterations: 270
currently lose_sum: 87.71679866313934
time_elpased: 1.646
batch start
#iterations: 271
currently lose_sum: 87.4553769826889
time_elpased: 1.593
batch start
#iterations: 272
currently lose_sum: 88.02542823553085
time_elpased: 1.636
batch start
#iterations: 273
currently lose_sum: 88.02204370498657
time_elpased: 1.591
batch start
#iterations: 274
currently lose_sum: 87.90099215507507
time_elpased: 1.658
batch start
#iterations: 275
currently lose_sum: 87.98234403133392
time_elpased: 1.605
batch start
#iterations: 276
currently lose_sum: 87.36607128381729
time_elpased: 1.641
batch start
#iterations: 277
currently lose_sum: 87.39593124389648
time_elpased: 1.589
batch start
#iterations: 278
currently lose_sum: 87.34585571289062
time_elpased: 1.644
batch start
#iterations: 279
currently lose_sum: 87.87060004472733
time_elpased: 1.611
start validation test
0.553762886598
0.615670013095
0.290315941134
0.394573047066
0.554225408345
73.175
batch start
#iterations: 280
currently lose_sum: 87.34336191415787
time_elpased: 1.625
batch start
#iterations: 281
currently lose_sum: 87.12343060970306
time_elpased: 1.608
batch start
#iterations: 282
currently lose_sum: 87.27878373861313
time_elpased: 1.646
batch start
#iterations: 283
currently lose_sum: 87.44692027568817
time_elpased: 1.598
batch start
#iterations: 284
currently lose_sum: 87.32236194610596
time_elpased: 1.658
batch start
#iterations: 285
currently lose_sum: 87.30245965719223
time_elpased: 1.617
batch start
#iterations: 286
currently lose_sum: 87.03289979696274
time_elpased: 1.609
batch start
#iterations: 287
currently lose_sum: 87.71357649564743
time_elpased: 1.594
batch start
#iterations: 288
currently lose_sum: 87.04431337118149
time_elpased: 1.645
batch start
#iterations: 289
currently lose_sum: 87.50788050889969
time_elpased: 1.625
batch start
#iterations: 290
currently lose_sum: 87.4106285572052
time_elpased: 1.626
batch start
#iterations: 291
currently lose_sum: 86.63581150770187
time_elpased: 1.65
batch start
#iterations: 292
currently lose_sum: 86.81414026021957
time_elpased: 1.599
batch start
#iterations: 293
currently lose_sum: 87.2581399679184
time_elpased: 1.604
batch start
#iterations: 294
currently lose_sum: 86.8093911409378
time_elpased: 1.624
batch start
#iterations: 295
currently lose_sum: 86.82176184654236
time_elpased: 1.682
batch start
#iterations: 296
currently lose_sum: 86.92304873466492
time_elpased: 1.602
batch start
#iterations: 297
currently lose_sum: 87.39359843730927
time_elpased: 1.592
batch start
#iterations: 298
currently lose_sum: 86.27283293008804
time_elpased: 1.602
batch start
#iterations: 299
currently lose_sum: 86.41960644721985
time_elpased: 1.63
start validation test
0.569948453608
0.60201960202
0.41720695688
0.492857577047
0.570216614865
69.701
batch start
#iterations: 300
currently lose_sum: 86.78219389915466
time_elpased: 1.601
batch start
#iterations: 301
currently lose_sum: 86.93671357631683
time_elpased: 1.604
batch start
#iterations: 302
currently lose_sum: 87.03670746088028
time_elpased: 1.628
batch start
#iterations: 303
currently lose_sum: 86.74330657720566
time_elpased: 1.602
batch start
#iterations: 304
currently lose_sum: 86.3907812833786
time_elpased: 1.625
batch start
#iterations: 305
currently lose_sum: 86.49770575761795
time_elpased: 1.614
batch start
#iterations: 306
currently lose_sum: 86.64372009038925
time_elpased: 1.635
batch start
#iterations: 307
currently lose_sum: 86.37866914272308
time_elpased: 1.606
batch start
#iterations: 308
currently lose_sum: 86.40269327163696
time_elpased: 1.65
batch start
#iterations: 309
currently lose_sum: 86.72470939159393
time_elpased: 1.606
batch start
#iterations: 310
currently lose_sum: 85.98287570476532
time_elpased: 1.616
batch start
#iterations: 311
currently lose_sum: 86.33242684602737
time_elpased: 1.635
batch start
#iterations: 312
currently lose_sum: 86.19376868009567
time_elpased: 1.606
batch start
#iterations: 313
currently lose_sum: 86.23570853471756
time_elpased: 1.653
batch start
#iterations: 314
currently lose_sum: 86.14154452085495
time_elpased: 1.624
batch start
#iterations: 315
currently lose_sum: 86.25878638029099
time_elpased: 1.59
batch start
#iterations: 316
currently lose_sum: 86.16174179315567
time_elpased: 1.625
batch start
#iterations: 317
currently lose_sum: 86.2579894065857
time_elpased: 1.586
batch start
#iterations: 318
currently lose_sum: 85.98723870515823
time_elpased: 1.635
batch start
#iterations: 319
currently lose_sum: 86.10743999481201
time_elpased: 1.608
start validation test
0.55706185567
0.593854375418
0.365956570958
0.452849411016
0.557397370473
71.739
batch start
#iterations: 320
currently lose_sum: 86.40656667947769
time_elpased: 1.621
batch start
#iterations: 321
currently lose_sum: 86.31884014606476
time_elpased: 1.621
batch start
#iterations: 322
currently lose_sum: 86.33449387550354
time_elpased: 1.641
batch start
#iterations: 323
currently lose_sum: 86.63673102855682
time_elpased: 1.646
batch start
#iterations: 324
currently lose_sum: 85.91766154766083
time_elpased: 1.63
batch start
#iterations: 325
currently lose_sum: 85.9517018198967
time_elpased: 1.613
batch start
#iterations: 326
currently lose_sum: 85.63520842790604
time_elpased: 1.608
batch start
#iterations: 327
currently lose_sum: 85.62762367725372
time_elpased: 1.594
batch start
#iterations: 328
currently lose_sum: 86.15839737653732
time_elpased: 1.616
batch start
#iterations: 329
currently lose_sum: 85.87270724773407
time_elpased: 1.637
batch start
#iterations: 330
currently lose_sum: 85.19158297777176
time_elpased: 1.614
batch start
#iterations: 331
currently lose_sum: 85.99020564556122
time_elpased: 1.627
batch start
#iterations: 332
currently lose_sum: 85.39381390810013
time_elpased: 1.614
batch start
#iterations: 333
currently lose_sum: 85.87914896011353
time_elpased: 1.59
batch start
#iterations: 334
currently lose_sum: 85.47330677509308
time_elpased: 1.591
batch start
#iterations: 335
currently lose_sum: 85.66082501411438
time_elpased: 1.625
batch start
#iterations: 336
currently lose_sum: 85.95618009567261
time_elpased: 1.626
batch start
#iterations: 337
currently lose_sum: 86.04306441545486
time_elpased: 1.594
batch start
#iterations: 338
currently lose_sum: 85.52380150556564
time_elpased: 1.613
batch start
#iterations: 339
currently lose_sum: 84.86193996667862
time_elpased: 1.631
start validation test
0.562628865979
0.588991620919
0.419573942575
0.490053488791
0.562880020962
71.171
batch start
#iterations: 340
currently lose_sum: 85.67554706335068
time_elpased: 1.601
batch start
#iterations: 341
currently lose_sum: 85.4071062207222
time_elpased: 1.642
batch start
#iterations: 342
currently lose_sum: 85.1596251130104
time_elpased: 1.613
batch start
#iterations: 343
currently lose_sum: 85.78215563297272
time_elpased: 1.607
batch start
#iterations: 344
currently lose_sum: 84.98965060710907
time_elpased: 1.634
batch start
#iterations: 345
currently lose_sum: 85.64622563123703
time_elpased: 1.6
batch start
#iterations: 346
currently lose_sum: 85.30671173334122
time_elpased: 1.618
batch start
#iterations: 347
currently lose_sum: 84.98420512676239
time_elpased: 1.593
batch start
#iterations: 348
currently lose_sum: 85.15432459115982
time_elpased: 1.639
batch start
#iterations: 349
currently lose_sum: 85.02634769678116
time_elpased: 1.607
batch start
#iterations: 350
currently lose_sum: 85.33162552118301
time_elpased: 1.622
batch start
#iterations: 351
currently lose_sum: 85.86989200115204
time_elpased: 1.622
batch start
#iterations: 352
currently lose_sum: 84.81728446483612
time_elpased: 1.657
batch start
#iterations: 353
currently lose_sum: 85.26454466581345
time_elpased: 1.612
batch start
#iterations: 354
currently lose_sum: 84.62697869539261
time_elpased: 1.62
batch start
#iterations: 355
currently lose_sum: 85.43206650018692
time_elpased: 1.637
batch start
#iterations: 356
currently lose_sum: 85.47222352027893
time_elpased: 1.633
batch start
#iterations: 357
currently lose_sum: 84.71209132671356
time_elpased: 1.593
batch start
#iterations: 358
currently lose_sum: 85.30783724784851
time_elpased: 1.624
batch start
#iterations: 359
currently lose_sum: 84.95914554595947
time_elpased: 1.594
start validation test
0.564072164948
0.586752960617
0.438509828136
0.501914129218
0.564292608997
71.295
batch start
#iterations: 360
currently lose_sum: 85.11545270681381
time_elpased: 1.608
batch start
#iterations: 361
currently lose_sum: 85.34771686792374
time_elpased: 1.64
batch start
#iterations: 362
currently lose_sum: 85.22936022281647
time_elpased: 1.63
batch start
#iterations: 363
currently lose_sum: 84.74137139320374
time_elpased: 1.643
batch start
#iterations: 364
currently lose_sum: 85.25432181358337
time_elpased: 1.632
batch start
#iterations: 365
currently lose_sum: 84.4791732430458
time_elpased: 1.613
batch start
#iterations: 366
currently lose_sum: 84.7790402173996
time_elpased: 1.61
batch start
#iterations: 367
currently lose_sum: 84.42061620950699
time_elpased: 1.637
batch start
#iterations: 368
currently lose_sum: 85.40439331531525
time_elpased: 1.652
batch start
#iterations: 369
currently lose_sum: 84.92530781030655
time_elpased: 1.622
batch start
#iterations: 370
currently lose_sum: 84.77848744392395
time_elpased: 1.648
batch start
#iterations: 371
currently lose_sum: 85.22048729658127
time_elpased: 1.611
batch start
#iterations: 372
currently lose_sum: 84.78934723138809
time_elpased: 1.618
batch start
#iterations: 373
currently lose_sum: 84.3080542087555
time_elpased: 1.64
batch start
#iterations: 374
currently lose_sum: 84.54947453737259
time_elpased: 1.652
batch start
#iterations: 375
currently lose_sum: 84.779352247715
time_elpased: 1.619
batch start
#iterations: 376
currently lose_sum: 84.59688448905945
time_elpased: 1.616
batch start
#iterations: 377
currently lose_sum: 84.91307348012924
time_elpased: 1.649
batch start
#iterations: 378
currently lose_sum: 84.3289703130722
time_elpased: 1.613
batch start
#iterations: 379
currently lose_sum: 84.60082405805588
time_elpased: 1.644
start validation test
0.555773195876
0.604328839947
0.327570237728
0.424853176722
0.556173841367
74.703
batch start
#iterations: 380
currently lose_sum: 84.62750554084778
time_elpased: 1.61
batch start
#iterations: 381
currently lose_sum: 84.51923090219498
time_elpased: 1.625
batch start
#iterations: 382
currently lose_sum: 84.78583854436874
time_elpased: 1.624
batch start
#iterations: 383
currently lose_sum: 84.59594261646271
time_elpased: 1.589
batch start
#iterations: 384
currently lose_sum: 84.21812427043915
time_elpased: 1.642
batch start
#iterations: 385
currently lose_sum: 84.66341024637222
time_elpased: 1.594
batch start
#iterations: 386
currently lose_sum: 84.30947607755661
time_elpased: 1.618
batch start
#iterations: 387
currently lose_sum: 84.43886443972588
time_elpased: 1.624
batch start
#iterations: 388
currently lose_sum: 84.59591102600098
time_elpased: 1.639
batch start
#iterations: 389
currently lose_sum: 84.49059635400772
time_elpased: 1.638
batch start
#iterations: 390
currently lose_sum: 84.01918834447861
time_elpased: 1.608
batch start
#iterations: 391
currently lose_sum: 84.52626127004623
time_elpased: 1.633
batch start
#iterations: 392
currently lose_sum: 84.25427997112274
time_elpased: 1.623
batch start
#iterations: 393
currently lose_sum: 84.12494218349457
time_elpased: 1.606
batch start
#iterations: 394
currently lose_sum: 84.12727063894272
time_elpased: 1.61
batch start
#iterations: 395
currently lose_sum: 83.79432362318039
time_elpased: 1.61
batch start
#iterations: 396
currently lose_sum: 83.9199731349945
time_elpased: 1.638
batch start
#iterations: 397
currently lose_sum: 83.56451451778412
time_elpased: 1.6
batch start
#iterations: 398
currently lose_sum: 83.79633814096451
time_elpased: 1.639
batch start
#iterations: 399
currently lose_sum: 84.06906020641327
time_elpased: 1.646
start validation test
0.561443298969
0.59679743795
0.38355459504
0.466984087207
0.561755610026
72.544
acc: 0.596
pre: 0.613
rec: 0.522
F1: 0.564
auc: 0.631
