start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.57878494262695
time_elpased: 1.802
batch start
#iterations: 1
currently lose_sum: 100.15138745307922
time_elpased: 1.784
batch start
#iterations: 2
currently lose_sum: 99.86468440294266
time_elpased: 1.769
batch start
#iterations: 3
currently lose_sum: 99.75460493564606
time_elpased: 1.722
batch start
#iterations: 4
currently lose_sum: 99.63091313838959
time_elpased: 1.75
batch start
#iterations: 5
currently lose_sum: 99.47388911247253
time_elpased: 1.736
batch start
#iterations: 6
currently lose_sum: 99.28893631696701
time_elpased: 1.738
batch start
#iterations: 7
currently lose_sum: 99.45775479078293
time_elpased: 1.75
batch start
#iterations: 8
currently lose_sum: 99.28159266710281
time_elpased: 1.719
batch start
#iterations: 9
currently lose_sum: 99.0430303812027
time_elpased: 1.734
batch start
#iterations: 10
currently lose_sum: 98.98404115438461
time_elpased: 1.785
batch start
#iterations: 11
currently lose_sum: 99.04556638002396
time_elpased: 1.781
batch start
#iterations: 12
currently lose_sum: 98.93819969892502
time_elpased: 1.75
batch start
#iterations: 13
currently lose_sum: 99.04767084121704
time_elpased: 1.734
batch start
#iterations: 14
currently lose_sum: 98.69495499134064
time_elpased: 1.705
batch start
#iterations: 15
currently lose_sum: 98.7184944152832
time_elpased: 1.74
batch start
#iterations: 16
currently lose_sum: 98.4751985669136
time_elpased: 1.771
batch start
#iterations: 17
currently lose_sum: 98.44472461938858
time_elpased: 1.727
batch start
#iterations: 18
currently lose_sum: 98.55601441860199
time_elpased: 1.764
batch start
#iterations: 19
currently lose_sum: 98.3156550526619
time_elpased: 1.749
start validation test
0.595618556701
0.620805369128
0.495008747556
0.550815917549
0.595795192739
64.863
batch start
#iterations: 20
currently lose_sum: 98.24913239479065
time_elpased: 1.741
batch start
#iterations: 21
currently lose_sum: 98.20000874996185
time_elpased: 1.755
batch start
#iterations: 22
currently lose_sum: 98.05099844932556
time_elpased: 1.737
batch start
#iterations: 23
currently lose_sum: 98.17713689804077
time_elpased: 1.801
batch start
#iterations: 24
currently lose_sum: 98.08524030447006
time_elpased: 1.757
batch start
#iterations: 25
currently lose_sum: 97.95573949813843
time_elpased: 1.773
batch start
#iterations: 26
currently lose_sum: 97.59845739603043
time_elpased: 1.754
batch start
#iterations: 27
currently lose_sum: 97.92621582746506
time_elpased: 1.74
batch start
#iterations: 28
currently lose_sum: 97.75411039590836
time_elpased: 1.724
batch start
#iterations: 29
currently lose_sum: 97.6958931684494
time_elpased: 1.757
batch start
#iterations: 30
currently lose_sum: 97.53015625476837
time_elpased: 1.717
batch start
#iterations: 31
currently lose_sum: 97.5374538898468
time_elpased: 1.765
batch start
#iterations: 32
currently lose_sum: 97.5850133895874
time_elpased: 1.709
batch start
#iterations: 33
currently lose_sum: 97.61182594299316
time_elpased: 1.758
batch start
#iterations: 34
currently lose_sum: 97.13462352752686
time_elpased: 1.747
batch start
#iterations: 35
currently lose_sum: 97.29329246282578
time_elpased: 1.768
batch start
#iterations: 36
currently lose_sum: 97.17918729782104
time_elpased: 1.732
batch start
#iterations: 37
currently lose_sum: 97.36574971675873
time_elpased: 1.721
batch start
#iterations: 38
currently lose_sum: 96.8725625872612
time_elpased: 1.757
batch start
#iterations: 39
currently lose_sum: 96.9218675494194
time_elpased: 1.732
start validation test
0.603350515464
0.616313851818
0.551301842132
0.581997935792
0.603441894938
64.425
batch start
#iterations: 40
currently lose_sum: 97.08535015583038
time_elpased: 1.753
batch start
#iterations: 41
currently lose_sum: 96.72769355773926
time_elpased: 1.754
batch start
#iterations: 42
currently lose_sum: 96.61371093988419
time_elpased: 1.72
batch start
#iterations: 43
currently lose_sum: 96.73436707258224
time_elpased: 1.751
batch start
#iterations: 44
currently lose_sum: 96.83159899711609
time_elpased: 1.734
batch start
#iterations: 45
currently lose_sum: 96.53270435333252
time_elpased: 1.794
batch start
#iterations: 46
currently lose_sum: 96.35490995645523
time_elpased: 1.755
batch start
#iterations: 47
currently lose_sum: 96.41802710294724
time_elpased: 1.749
batch start
#iterations: 48
currently lose_sum: 96.23889321088791
time_elpased: 1.724
batch start
#iterations: 49
currently lose_sum: 96.29758143424988
time_elpased: 1.72
batch start
#iterations: 50
currently lose_sum: 96.38803517818451
time_elpased: 1.716
batch start
#iterations: 51
currently lose_sum: 96.18738430738449
time_elpased: 1.765
batch start
#iterations: 52
currently lose_sum: 96.10517954826355
time_elpased: 1.743
batch start
#iterations: 53
currently lose_sum: 95.63939082622528
time_elpased: 1.734
batch start
#iterations: 54
currently lose_sum: 96.13278222084045
time_elpased: 1.738
batch start
#iterations: 55
currently lose_sum: 95.95680940151215
time_elpased: 1.732
batch start
#iterations: 56
currently lose_sum: 95.63935363292694
time_elpased: 1.755
batch start
#iterations: 57
currently lose_sum: 95.4611747264862
time_elpased: 1.768
batch start
#iterations: 58
currently lose_sum: 95.60680466890335
time_elpased: 1.785
batch start
#iterations: 59
currently lose_sum: 95.70440739393234
time_elpased: 1.753
start validation test
0.592474226804
0.623990141038
0.468971904909
0.535487661575
0.592691054179
64.896
batch start
#iterations: 60
currently lose_sum: 95.32765567302704
time_elpased: 1.762
batch start
#iterations: 61
currently lose_sum: 95.65917164087296
time_elpased: 1.721
batch start
#iterations: 62
currently lose_sum: 95.56183475255966
time_elpased: 1.726
batch start
#iterations: 63
currently lose_sum: 94.97094631195068
time_elpased: 1.738
batch start
#iterations: 64
currently lose_sum: 94.93264490365982
time_elpased: 1.756
batch start
#iterations: 65
currently lose_sum: 95.26381349563599
time_elpased: 1.756
batch start
#iterations: 66
currently lose_sum: 95.00230926275253
time_elpased: 1.768
batch start
#iterations: 67
currently lose_sum: 94.62401622533798
time_elpased: 1.764
batch start
#iterations: 68
currently lose_sum: 94.98246389627457
time_elpased: 1.753
batch start
#iterations: 69
currently lose_sum: 94.96429133415222
time_elpased: 1.836
batch start
#iterations: 70
currently lose_sum: 94.73816758394241
time_elpased: 1.769
batch start
#iterations: 71
currently lose_sum: 94.61818838119507
time_elpased: 1.769
batch start
#iterations: 72
currently lose_sum: 94.34036892652512
time_elpased: 1.762
batch start
#iterations: 73
currently lose_sum: 94.10068172216415
time_elpased: 1.784
batch start
#iterations: 74
currently lose_sum: 94.59813863039017
time_elpased: 1.765
batch start
#iterations: 75
currently lose_sum: 94.53145986795425
time_elpased: 1.727
batch start
#iterations: 76
currently lose_sum: 94.27554428577423
time_elpased: 1.744
batch start
#iterations: 77
currently lose_sum: 94.2844220995903
time_elpased: 1.751
batch start
#iterations: 78
currently lose_sum: 94.31885331869125
time_elpased: 1.777
batch start
#iterations: 79
currently lose_sum: 94.09136617183685
time_elpased: 1.772
start validation test
0.584845360825
0.614044712659
0.460738911187
0.526458137347
0.585063248839
65.623
batch start
#iterations: 80
currently lose_sum: 94.16203737258911
time_elpased: 1.756
batch start
#iterations: 81
currently lose_sum: 93.31294369697571
time_elpased: 1.752
batch start
#iterations: 82
currently lose_sum: 93.7781652212143
time_elpased: 1.721
batch start
#iterations: 83
currently lose_sum: 93.54512757062912
time_elpased: 1.76
batch start
#iterations: 84
currently lose_sum: 93.44727492332458
time_elpased: 1.781
batch start
#iterations: 85
currently lose_sum: 93.58472591638565
time_elpased: 1.715
batch start
#iterations: 86
currently lose_sum: 93.35103303194046
time_elpased: 1.763
batch start
#iterations: 87
currently lose_sum: 93.44785535335541
time_elpased: 1.764
batch start
#iterations: 88
currently lose_sum: 93.32539403438568
time_elpased: 1.741
batch start
#iterations: 89
currently lose_sum: 93.26547396183014
time_elpased: 1.729
batch start
#iterations: 90
currently lose_sum: 92.97203850746155
time_elpased: 1.707
batch start
#iterations: 91
currently lose_sum: 92.78824865818024
time_elpased: 1.754
batch start
#iterations: 92
currently lose_sum: 92.38351160287857
time_elpased: 1.749
batch start
#iterations: 93
currently lose_sum: 92.86825281381607
time_elpased: 1.732
batch start
#iterations: 94
currently lose_sum: 92.48701196908951
time_elpased: 1.706
batch start
#iterations: 95
currently lose_sum: 92.80141693353653
time_elpased: 1.768
batch start
#iterations: 96
currently lose_sum: 92.55370128154755
time_elpased: 1.745
batch start
#iterations: 97
currently lose_sum: 92.35042208433151
time_elpased: 1.756
batch start
#iterations: 98
currently lose_sum: 92.92990428209305
time_elpased: 1.732
batch start
#iterations: 99
currently lose_sum: 91.95686084032059
time_elpased: 1.759
start validation test
0.572474226804
0.631832499537
0.350931357415
0.451237263464
0.572863179482
67.910
batch start
#iterations: 100
currently lose_sum: 92.08959358930588
time_elpased: 1.73
batch start
#iterations: 101
currently lose_sum: 92.21073871850967
time_elpased: 1.739
batch start
#iterations: 102
currently lose_sum: 92.27923679351807
time_elpased: 1.751
batch start
#iterations: 103
currently lose_sum: 91.80658411979675
time_elpased: 1.779
batch start
#iterations: 104
currently lose_sum: 92.08827519416809
time_elpased: 1.769
batch start
#iterations: 105
currently lose_sum: 91.25258159637451
time_elpased: 1.755
batch start
#iterations: 106
currently lose_sum: 91.42738938331604
time_elpased: 1.723
batch start
#iterations: 107
currently lose_sum: 91.59049767255783
time_elpased: 1.74
batch start
#iterations: 108
currently lose_sum: 91.81072890758514
time_elpased: 1.774
batch start
#iterations: 109
currently lose_sum: 91.3254941701889
time_elpased: 1.757
batch start
#iterations: 110
currently lose_sum: 91.00686144828796
time_elpased: 1.747
batch start
#iterations: 111
currently lose_sum: 90.9098129272461
time_elpased: 1.73
batch start
#iterations: 112
currently lose_sum: 91.03663611412048
time_elpased: 1.754
batch start
#iterations: 113
currently lose_sum: 91.05283391475677
time_elpased: 1.769
batch start
#iterations: 114
currently lose_sum: 90.65618151426315
time_elpased: 1.765
batch start
#iterations: 115
currently lose_sum: 90.87497955560684
time_elpased: 1.731
batch start
#iterations: 116
currently lose_sum: 90.40193581581116
time_elpased: 1.753
batch start
#iterations: 117
currently lose_sum: 90.40401214361191
time_elpased: 1.706
batch start
#iterations: 118
currently lose_sum: 90.79223370552063
time_elpased: 1.801
batch start
#iterations: 119
currently lose_sum: 90.75760006904602
time_elpased: 1.731
start validation test
0.581391752577
0.597674418605
0.502521354327
0.54598311623
0.581530221726
67.776
batch start
#iterations: 120
currently lose_sum: 89.98303401470184
time_elpased: 1.732
batch start
#iterations: 121
currently lose_sum: 90.05122715234756
time_elpased: 1.738
batch start
#iterations: 122
currently lose_sum: 90.3116158246994
time_elpased: 1.827
batch start
#iterations: 123
currently lose_sum: 89.92075550556183
time_elpased: 1.744
batch start
#iterations: 124
currently lose_sum: 90.12258803844452
time_elpased: 1.772
batch start
#iterations: 125
currently lose_sum: 89.94119811058044
time_elpased: 1.736
batch start
#iterations: 126
currently lose_sum: 89.81961727142334
time_elpased: 1.738
batch start
#iterations: 127
currently lose_sum: 89.86160868406296
time_elpased: 1.715
batch start
#iterations: 128
currently lose_sum: 89.5194541811943
time_elpased: 1.766
batch start
#iterations: 129
currently lose_sum: 89.89667278528214
time_elpased: 1.754
batch start
#iterations: 130
currently lose_sum: 89.54715484380722
time_elpased: 1.743
batch start
#iterations: 131
currently lose_sum: 89.97908765077591
time_elpased: 1.76
batch start
#iterations: 132
currently lose_sum: 89.40720146894455
time_elpased: 1.716
batch start
#iterations: 133
currently lose_sum: 89.29813331365585
time_elpased: 1.738
batch start
#iterations: 134
currently lose_sum: 89.1038892865181
time_elpased: 1.781
batch start
#iterations: 135
currently lose_sum: 89.2785052061081
time_elpased: 1.752
batch start
#iterations: 136
currently lose_sum: 88.17415380477905
time_elpased: 1.751
batch start
#iterations: 137
currently lose_sum: 88.87110906839371
time_elpased: 1.724
batch start
#iterations: 138
currently lose_sum: 88.96558541059494
time_elpased: 1.794
batch start
#iterations: 139
currently lose_sum: 88.35517328977585
time_elpased: 1.766
start validation test
0.570567010309
0.601345422638
0.423175877328
0.496768347931
0.570825778177
69.972
batch start
#iterations: 140
currently lose_sum: 88.47574198246002
time_elpased: 1.729
batch start
#iterations: 141
currently lose_sum: 88.2630460858345
time_elpased: 1.747
batch start
#iterations: 142
currently lose_sum: 88.74537056684494
time_elpased: 1.757
batch start
#iterations: 143
currently lose_sum: 88.5947613120079
time_elpased: 1.77
batch start
#iterations: 144
currently lose_sum: 88.13974887132645
time_elpased: 1.763
batch start
#iterations: 145
currently lose_sum: 88.26718533039093
time_elpased: 1.732
batch start
#iterations: 146
currently lose_sum: 88.2456785440445
time_elpased: 1.78
batch start
#iterations: 147
currently lose_sum: 87.66839188337326
time_elpased: 1.77
batch start
#iterations: 148
currently lose_sum: 87.91418641805649
time_elpased: 1.739
batch start
#iterations: 149
currently lose_sum: 87.29081845283508
time_elpased: 1.733
batch start
#iterations: 150
currently lose_sum: 88.08825445175171
time_elpased: 1.765
batch start
#iterations: 151
currently lose_sum: 87.80846524238586
time_elpased: 1.756
batch start
#iterations: 152
currently lose_sum: 87.32584321498871
time_elpased: 1.742
batch start
#iterations: 153
currently lose_sum: 87.34533709287643
time_elpased: 1.711
batch start
#iterations: 154
currently lose_sum: 87.63614338636398
time_elpased: 1.752
batch start
#iterations: 155
currently lose_sum: 87.33897924423218
time_elpased: 1.734
batch start
#iterations: 156
currently lose_sum: 86.65125155448914
time_elpased: 1.757
batch start
#iterations: 157
currently lose_sum: 86.18475824594498
time_elpased: 1.733
batch start
#iterations: 158
currently lose_sum: 86.83192300796509
time_elpased: 1.767
batch start
#iterations: 159
currently lose_sum: 86.68644899129868
time_elpased: 1.756
start validation test
0.571597938144
0.593335103558
0.459915611814
0.518174966665
0.571794013694
71.068
batch start
#iterations: 160
currently lose_sum: 86.30203640460968
time_elpased: 1.822
batch start
#iterations: 161
currently lose_sum: 86.80831468105316
time_elpased: 1.73
batch start
#iterations: 162
currently lose_sum: 86.92618745565414
time_elpased: 1.774
batch start
#iterations: 163
currently lose_sum: 86.87373518943787
time_elpased: 1.725
batch start
#iterations: 164
currently lose_sum: 85.8824405670166
time_elpased: 1.755
batch start
#iterations: 165
currently lose_sum: 86.2113983631134
time_elpased: 1.699
batch start
#iterations: 166
currently lose_sum: 85.91257721185684
time_elpased: 1.766
batch start
#iterations: 167
currently lose_sum: 86.15438294410706
time_elpased: 1.779
batch start
#iterations: 168
currently lose_sum: 86.30524641275406
time_elpased: 1.784
batch start
#iterations: 169
currently lose_sum: 85.97313517332077
time_elpased: 1.728
batch start
#iterations: 170
currently lose_sum: 86.00625747442245
time_elpased: 1.754
batch start
#iterations: 171
currently lose_sum: 86.28740388154984
time_elpased: 1.732
batch start
#iterations: 172
currently lose_sum: 85.83557444810867
time_elpased: 1.766
batch start
#iterations: 173
currently lose_sum: 85.39710855484009
time_elpased: 1.738
batch start
#iterations: 174
currently lose_sum: 85.8532697558403
time_elpased: 1.809
batch start
#iterations: 175
currently lose_sum: 84.9037715792656
time_elpased: 1.782
batch start
#iterations: 176
currently lose_sum: 85.77562040090561
time_elpased: 1.754
batch start
#iterations: 177
currently lose_sum: 84.97171348333359
time_elpased: 1.768
batch start
#iterations: 178
currently lose_sum: 85.44612002372742
time_elpased: 1.729
batch start
#iterations: 179
currently lose_sum: 84.1761018037796
time_elpased: 1.719
start validation test
0.56793814433
0.594882729211
0.4306884841
0.499641833811
0.568179107278
73.936
batch start
#iterations: 180
currently lose_sum: 85.10668432712555
time_elpased: 1.745
batch start
#iterations: 181
currently lose_sum: 84.94572561979294
time_elpased: 1.723
batch start
#iterations: 182
currently lose_sum: 84.20626771450043
time_elpased: 1.77
batch start
#iterations: 183
currently lose_sum: 84.65640449523926
time_elpased: 1.737
batch start
#iterations: 184
currently lose_sum: 84.68692409992218
time_elpased: 1.794
batch start
#iterations: 185
currently lose_sum: 84.837406873703
time_elpased: 1.781
batch start
#iterations: 186
currently lose_sum: 84.65167951583862
time_elpased: 1.744
batch start
#iterations: 187
currently lose_sum: 84.01333531737328
time_elpased: 1.804
batch start
#iterations: 188
currently lose_sum: 83.81559836864471
time_elpased: 1.734
batch start
#iterations: 189
currently lose_sum: 83.83246344327927
time_elpased: 1.743
batch start
#iterations: 190
currently lose_sum: 84.46402418613434
time_elpased: 1.706
batch start
#iterations: 191
currently lose_sum: 84.52162861824036
time_elpased: 1.75
batch start
#iterations: 192
currently lose_sum: 83.3800408244133
time_elpased: 1.779
batch start
#iterations: 193
currently lose_sum: 83.64034533500671
time_elpased: 1.756
batch start
#iterations: 194
currently lose_sum: 83.49118721485138
time_elpased: 1.751
batch start
#iterations: 195
currently lose_sum: 83.46471107006073
time_elpased: 1.77
batch start
#iterations: 196
currently lose_sum: 83.26466581225395
time_elpased: 1.775
batch start
#iterations: 197
currently lose_sum: 83.5348949432373
time_elpased: 1.742
batch start
#iterations: 198
currently lose_sum: 84.19437175989151
time_elpased: 1.74
batch start
#iterations: 199
currently lose_sum: 82.78588378429413
time_elpased: 1.719
start validation test
0.571340206186
0.603394833948
0.420705979212
0.495755517827
0.571604667805
73.689
batch start
#iterations: 200
currently lose_sum: 83.30609142780304
time_elpased: 1.771
batch start
#iterations: 201
currently lose_sum: 83.18405312299728
time_elpased: 1.733
batch start
#iterations: 202
currently lose_sum: 83.2482351064682
time_elpased: 1.753
batch start
#iterations: 203
currently lose_sum: 82.73002445697784
time_elpased: 1.757
batch start
#iterations: 204
currently lose_sum: 82.71898347139359
time_elpased: 1.817
batch start
#iterations: 205
currently lose_sum: 82.78643238544464
time_elpased: 1.735
batch start
#iterations: 206
currently lose_sum: 82.53287556767464
time_elpased: 1.751
batch start
#iterations: 207
currently lose_sum: 82.62230032682419
time_elpased: 1.79
batch start
#iterations: 208
currently lose_sum: 82.29708254337311
time_elpased: 1.766
batch start
#iterations: 209
currently lose_sum: 82.59176760911942
time_elpased: 1.782
batch start
#iterations: 210
currently lose_sum: 81.91101032495499
time_elpased: 1.766
batch start
#iterations: 211
currently lose_sum: 82.45283311605453
time_elpased: 1.756
batch start
#iterations: 212
currently lose_sum: 82.0632792711258
time_elpased: 1.756
batch start
#iterations: 213
currently lose_sum: 81.86844703555107
time_elpased: 1.756
batch start
#iterations: 214
currently lose_sum: 81.82126042246819
time_elpased: 1.739
batch start
#iterations: 215
currently lose_sum: 81.25343197584152
time_elpased: 1.729
batch start
#iterations: 216
currently lose_sum: 81.26241821050644
time_elpased: 1.796
batch start
#iterations: 217
currently lose_sum: 81.90622180700302
time_elpased: 1.704
batch start
#iterations: 218
currently lose_sum: 81.33068203926086
time_elpased: 1.739
batch start
#iterations: 219
currently lose_sum: 81.14135971665382
time_elpased: 1.72
start validation test
0.559329896907
0.581678321678
0.42801276114
0.493152309243
0.559560444393
76.413
batch start
#iterations: 220
currently lose_sum: 81.65866479277611
time_elpased: 1.765
batch start
#iterations: 221
currently lose_sum: 81.16095343232155
time_elpased: 1.732
batch start
#iterations: 222
currently lose_sum: 80.9858295917511
time_elpased: 1.721
batch start
#iterations: 223
currently lose_sum: 81.23699200153351
time_elpased: 1.723
batch start
#iterations: 224
currently lose_sum: 81.4688905775547
time_elpased: 1.744
batch start
#iterations: 225
currently lose_sum: 80.74191123247147
time_elpased: 1.739
batch start
#iterations: 226
currently lose_sum: 80.68349927663803
time_elpased: 1.743
batch start
#iterations: 227
currently lose_sum: 80.70770335197449
time_elpased: 1.747
batch start
#iterations: 228
currently lose_sum: 80.59789830446243
time_elpased: 1.756
batch start
#iterations: 229
currently lose_sum: 80.25400960445404
time_elpased: 1.757
batch start
#iterations: 230
currently lose_sum: 80.75726714730263
time_elpased: 1.767
batch start
#iterations: 231
currently lose_sum: 80.05332159996033
time_elpased: 1.775
batch start
#iterations: 232
currently lose_sum: 80.03634890913963
time_elpased: 1.829
batch start
#iterations: 233
currently lose_sum: 80.5089800953865
time_elpased: 1.747
batch start
#iterations: 234
currently lose_sum: 80.12026128172874
time_elpased: 1.783
batch start
#iterations: 235
currently lose_sum: 80.1147773861885
time_elpased: 1.757
batch start
#iterations: 236
currently lose_sum: 79.72530084848404
time_elpased: 1.779
batch start
#iterations: 237
currently lose_sum: 80.04081496596336
time_elpased: 1.746
batch start
#iterations: 238
currently lose_sum: 79.85421842336655
time_elpased: 1.754
batch start
#iterations: 239
currently lose_sum: 80.12934696674347
time_elpased: 1.746
start validation test
0.555721649485
0.587699680511
0.378614798806
0.460537021969
0.556032587878
78.111
batch start
#iterations: 240
currently lose_sum: 79.3400768339634
time_elpased: 1.757
batch start
#iterations: 241
currently lose_sum: 79.83398625254631
time_elpased: 1.748
batch start
#iterations: 242
currently lose_sum: 79.91784906387329
time_elpased: 1.76
batch start
#iterations: 243
currently lose_sum: 79.13870084285736
time_elpased: 1.758
batch start
#iterations: 244
currently lose_sum: 79.13363420963287
time_elpased: 1.781
batch start
#iterations: 245
currently lose_sum: 78.99701943993568
time_elpased: 1.796
batch start
#iterations: 246
currently lose_sum: 79.38458603620529
time_elpased: 1.828
batch start
#iterations: 247
currently lose_sum: 79.03492787480354
time_elpased: 1.731
batch start
#iterations: 248
currently lose_sum: 78.85503640770912
time_elpased: 1.74
batch start
#iterations: 249
currently lose_sum: 79.4429440498352
time_elpased: 1.757
batch start
#iterations: 250
currently lose_sum: 79.06648281216621
time_elpased: 1.806
batch start
#iterations: 251
currently lose_sum: 78.90696680545807
time_elpased: 1.733
batch start
#iterations: 252
currently lose_sum: 78.59503284096718
time_elpased: 1.774
batch start
#iterations: 253
currently lose_sum: 78.97526037693024
time_elpased: 1.759
batch start
#iterations: 254
currently lose_sum: 78.1412325501442
time_elpased: 1.753
batch start
#iterations: 255
currently lose_sum: 78.44703397154808
time_elpased: 1.771
batch start
#iterations: 256
currently lose_sum: 78.2477730512619
time_elpased: 1.77
batch start
#iterations: 257
currently lose_sum: 78.02542340755463
time_elpased: 1.756
batch start
#iterations: 258
currently lose_sum: 78.07147100567818
time_elpased: 1.747
batch start
#iterations: 259
currently lose_sum: 78.23635986447334
time_elpased: 1.707
start validation test
0.561391752577
0.597325169191
0.381496346609
0.465615775922
0.561707586709
80.886
batch start
#iterations: 260
currently lose_sum: 78.17454713582993
time_elpased: 1.73
batch start
#iterations: 261
currently lose_sum: 78.17968437075615
time_elpased: 1.741
batch start
#iterations: 262
currently lose_sum: 78.45815688371658
time_elpased: 1.81
batch start
#iterations: 263
currently lose_sum: 78.48475533723831
time_elpased: 1.758
batch start
#iterations: 264
currently lose_sum: 77.32467564940453
time_elpased: 1.735
batch start
#iterations: 265
currently lose_sum: 77.76011797785759
time_elpased: 1.761
batch start
#iterations: 266
currently lose_sum: 77.5421604514122
time_elpased: 1.769
batch start
#iterations: 267
currently lose_sum: 77.33041939139366
time_elpased: 1.735
batch start
#iterations: 268
currently lose_sum: 77.51699104905128
time_elpased: 1.721
batch start
#iterations: 269
currently lose_sum: 77.56815311312675
time_elpased: 1.723
batch start
#iterations: 270
currently lose_sum: 77.6537259221077
time_elpased: 1.754
batch start
#iterations: 271
currently lose_sum: 77.12846365571022
time_elpased: 1.741
batch start
#iterations: 272
currently lose_sum: 76.6755687892437
time_elpased: 1.741
batch start
#iterations: 273
currently lose_sum: 77.20574155449867
time_elpased: 1.726
batch start
#iterations: 274
currently lose_sum: 77.27027294039726
time_elpased: 1.732
batch start
#iterations: 275
currently lose_sum: 77.75641420483589
time_elpased: 1.739
batch start
#iterations: 276
currently lose_sum: 76.83751228451729
time_elpased: 1.755
batch start
#iterations: 277
currently lose_sum: 76.59250551462173
time_elpased: 1.712
batch start
#iterations: 278
currently lose_sum: 76.72940522432327
time_elpased: 1.745
batch start
#iterations: 279
currently lose_sum: 76.58888933062553
time_elpased: 1.732
start validation test
0.553092783505
0.603848442769
0.313265411135
0.412522021954
0.553513837448
86.729
batch start
#iterations: 280
currently lose_sum: 76.77802294492722
time_elpased: 1.73
batch start
#iterations: 281
currently lose_sum: 76.12107449769974
time_elpased: 1.724
batch start
#iterations: 282
currently lose_sum: 76.13069412112236
time_elpased: 1.766
batch start
#iterations: 283
currently lose_sum: 77.04555681347847
time_elpased: 1.743
batch start
#iterations: 284
currently lose_sum: 76.63878491520882
time_elpased: 1.725
batch start
#iterations: 285
currently lose_sum: 76.19473797082901
time_elpased: 1.741
batch start
#iterations: 286
currently lose_sum: 76.97441306710243
time_elpased: 1.763
batch start
#iterations: 287
currently lose_sum: 75.7201681137085
time_elpased: 1.741
batch start
#iterations: 288
currently lose_sum: 76.45172953605652
time_elpased: 1.753
batch start
#iterations: 289
currently lose_sum: 76.37805712223053
time_elpased: 1.736
batch start
#iterations: 290
currently lose_sum: 75.01027446985245
time_elpased: 1.75
batch start
#iterations: 291
currently lose_sum: 75.78717955946922
time_elpased: 1.761
batch start
#iterations: 292
currently lose_sum: 75.87596473097801
time_elpased: 1.746
batch start
#iterations: 293
currently lose_sum: 75.99982821941376
time_elpased: 1.76
batch start
#iterations: 294
currently lose_sum: 75.85230234265327
time_elpased: 1.753
batch start
#iterations: 295
currently lose_sum: 75.36270168423653
time_elpased: 1.713
batch start
#iterations: 296
currently lose_sum: 75.82339814305305
time_elpased: 1.782
batch start
#iterations: 297
currently lose_sum: 75.48749220371246
time_elpased: 1.767
batch start
#iterations: 298
currently lose_sum: 75.20082312822342
time_elpased: 1.782
batch start
#iterations: 299
currently lose_sum: 75.71430578827858
time_elpased: 1.706
start validation test
0.551030927835
0.587671948459
0.347329422661
0.436610608021
0.551388557246
87.786
batch start
#iterations: 300
currently lose_sum: 75.39126589894295
time_elpased: 1.711
batch start
#iterations: 301
currently lose_sum: 75.23065200448036
time_elpased: 1.743
batch start
#iterations: 302
currently lose_sum: 75.4146930873394
time_elpased: 1.74
batch start
#iterations: 303
currently lose_sum: 75.15758699178696
time_elpased: 1.739
batch start
#iterations: 304
currently lose_sum: 75.42034634947777
time_elpased: 1.78
batch start
#iterations: 305
currently lose_sum: 75.01125451922417
time_elpased: 1.716
batch start
#iterations: 306
currently lose_sum: 75.0722618997097
time_elpased: 1.805
batch start
#iterations: 307
currently lose_sum: 74.96783697605133
time_elpased: 1.696
batch start
#iterations: 308
currently lose_sum: 75.65767487883568
time_elpased: 1.742
batch start
#iterations: 309
currently lose_sum: 74.3042773604393
time_elpased: 1.731
batch start
#iterations: 310
currently lose_sum: 74.91254663467407
time_elpased: 1.765
batch start
#iterations: 311
currently lose_sum: 75.12354856729507
time_elpased: 1.729
batch start
#iterations: 312
currently lose_sum: 74.5641782283783
time_elpased: 1.787
batch start
#iterations: 313
currently lose_sum: 74.31970989704132
time_elpased: 1.749
batch start
#iterations: 314
currently lose_sum: 74.04051074385643
time_elpased: 1.766
batch start
#iterations: 315
currently lose_sum: 73.90830740332603
time_elpased: 1.715
batch start
#iterations: 316
currently lose_sum: 74.40539216995239
time_elpased: 1.762
batch start
#iterations: 317
currently lose_sum: 73.94832226634026
time_elpased: 1.785
batch start
#iterations: 318
currently lose_sum: 74.71712118387222
time_elpased: 1.771
batch start
#iterations: 319
currently lose_sum: 74.23070734739304
time_elpased: 1.709
start validation test
0.549175257732
0.585100788782
0.343521663065
0.432888081961
0.549536314337
89.272
batch start
#iterations: 320
currently lose_sum: 73.47596615552902
time_elpased: 1.735
batch start
#iterations: 321
currently lose_sum: 74.22365820407867
time_elpased: 1.788
batch start
#iterations: 322
currently lose_sum: 73.41554373502731
time_elpased: 1.751
batch start
#iterations: 323
currently lose_sum: 73.81480830907822
time_elpased: 1.768
batch start
#iterations: 324
currently lose_sum: 73.46944406628609
time_elpased: 1.733
batch start
#iterations: 325
currently lose_sum: 73.04072120785713
time_elpased: 1.76
batch start
#iterations: 326
currently lose_sum: 73.29341340065002
time_elpased: 1.755
batch start
#iterations: 327
currently lose_sum: 73.11935052275658
time_elpased: 1.769
batch start
#iterations: 328
currently lose_sum: 73.60461324453354
time_elpased: 1.739
batch start
#iterations: 329
currently lose_sum: 73.66387978196144
time_elpased: 1.744
batch start
#iterations: 330
currently lose_sum: 73.4689527451992
time_elpased: 1.733
batch start
#iterations: 331
currently lose_sum: 73.9741863310337
time_elpased: 1.736
batch start
#iterations: 332
currently lose_sum: 73.22996547818184
time_elpased: 1.778
batch start
#iterations: 333
currently lose_sum: 73.33707508444786
time_elpased: 1.732
batch start
#iterations: 334
currently lose_sum: 73.9633337855339
time_elpased: 1.774
batch start
#iterations: 335
currently lose_sum: 72.4658370912075
time_elpased: 1.766
batch start
#iterations: 336
currently lose_sum: 73.61418879032135
time_elpased: 1.749
batch start
#iterations: 337
currently lose_sum: 73.05871316790581
time_elpased: 1.732
batch start
#iterations: 338
currently lose_sum: 72.849634796381
time_elpased: 1.736
batch start
#iterations: 339
currently lose_sum: 73.10038876533508
time_elpased: 1.716
start validation test
0.551391752577
0.577689243028
0.387979829165
0.464199963061
0.551678647413
85.344
batch start
#iterations: 340
currently lose_sum: 72.90418019890785
time_elpased: 1.763
batch start
#iterations: 341
currently lose_sum: 72.33194255828857
time_elpased: 1.746
batch start
#iterations: 342
currently lose_sum: 72.5077731013298
time_elpased: 1.775
batch start
#iterations: 343
currently lose_sum: 72.83723005652428
time_elpased: 1.74
batch start
#iterations: 344
currently lose_sum: 72.74976739287376
time_elpased: 1.745
batch start
#iterations: 345
currently lose_sum: 72.17756861448288
time_elpased: 1.759
batch start
#iterations: 346
currently lose_sum: 72.43249157071114
time_elpased: 1.787
batch start
#iterations: 347
currently lose_sum: 71.73325315117836
time_elpased: 1.727
batch start
#iterations: 348
currently lose_sum: 72.0926665365696
time_elpased: 1.789
batch start
#iterations: 349
currently lose_sum: 71.81334099173546
time_elpased: 1.723
batch start
#iterations: 350
currently lose_sum: 71.93655103445053
time_elpased: 1.783
batch start
#iterations: 351
currently lose_sum: 71.76721552014351
time_elpased: 1.747
batch start
#iterations: 352
currently lose_sum: 72.81379973888397
time_elpased: 1.763
batch start
#iterations: 353
currently lose_sum: 72.15651971101761
time_elpased: 1.74
batch start
#iterations: 354
currently lose_sum: 72.51592734456062
time_elpased: 1.764
batch start
#iterations: 355
currently lose_sum: 71.75590777397156
time_elpased: 1.74
batch start
#iterations: 356
currently lose_sum: 72.3500127196312
time_elpased: 1.765
batch start
#iterations: 357
currently lose_sum: 72.24662524461746
time_elpased: 1.735
batch start
#iterations: 358
currently lose_sum: 72.29632744193077
time_elpased: 1.75
batch start
#iterations: 359
currently lose_sum: 71.52432635426521
time_elpased: 1.786
start validation test
0.543969072165
0.604869816779
0.258207265617
0.36191849982
0.544470771092
107.224
batch start
#iterations: 360
currently lose_sum: 71.98545521497726
time_elpased: 1.755
batch start
#iterations: 361
currently lose_sum: 71.22848761081696
time_elpased: 1.731
batch start
#iterations: 362
currently lose_sum: 72.00720456242561
time_elpased: 1.808
batch start
#iterations: 363
currently lose_sum: 71.4305211007595
time_elpased: 1.728
batch start
#iterations: 364
currently lose_sum: 71.75872987508774
time_elpased: 1.769
batch start
#iterations: 365
currently lose_sum: 71.71796295046806
time_elpased: 1.736
batch start
#iterations: 366
currently lose_sum: 71.27664974331856
time_elpased: 1.724
batch start
#iterations: 367
currently lose_sum: 70.9948248565197
time_elpased: 1.73
batch start
#iterations: 368
currently lose_sum: 71.7506950199604
time_elpased: 1.747
batch start
#iterations: 369
currently lose_sum: 70.8175657093525
time_elpased: 1.776
batch start
#iterations: 370
currently lose_sum: 71.01677957177162
time_elpased: 1.76
batch start
#iterations: 371
currently lose_sum: 71.14726346731186
time_elpased: 1.748
batch start
#iterations: 372
currently lose_sum: 71.41259154677391
time_elpased: 1.796
batch start
#iterations: 373
currently lose_sum: 70.54190039634705
time_elpased: 1.751
batch start
#iterations: 374
currently lose_sum: 71.42873647809029
time_elpased: 1.762
batch start
#iterations: 375
currently lose_sum: 70.73632392287254
time_elpased: 1.749
batch start
#iterations: 376
currently lose_sum: 71.02896469831467
time_elpased: 1.804
batch start
#iterations: 377
currently lose_sum: 71.00337615609169
time_elpased: 1.768
batch start
#iterations: 378
currently lose_sum: 70.70313513278961
time_elpased: 1.763
batch start
#iterations: 379
currently lose_sum: 70.13295236229897
time_elpased: 1.748
start validation test
0.550463917526
0.592737430168
0.327570237728
0.421952674488
0.55085524176
95.333
batch start
#iterations: 380
currently lose_sum: 71.0923960506916
time_elpased: 1.787
batch start
#iterations: 381
currently lose_sum: 70.69663947820663
time_elpased: 1.733
batch start
#iterations: 382
currently lose_sum: nan
time_elpased: 1.811
train finish final lose is: nan
acc: 0.596
pre: 0.609
rec: 0.539
F1: 0.572
auc: 0.596
