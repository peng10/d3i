start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.64096242189407
time_elpased: 1.764
batch start
#iterations: 1
currently lose_sum: 100.22874784469604
time_elpased: 1.769
batch start
#iterations: 2
currently lose_sum: 100.16378968954086
time_elpased: 1.792
batch start
#iterations: 3
currently lose_sum: 99.91442263126373
time_elpased: 1.759
batch start
#iterations: 4
currently lose_sum: 99.90974432229996
time_elpased: 1.732
batch start
#iterations: 5
currently lose_sum: 99.68715977668762
time_elpased: 1.761
batch start
#iterations: 6
currently lose_sum: 99.59275102615356
time_elpased: 1.709
batch start
#iterations: 7
currently lose_sum: 99.55939102172852
time_elpased: 1.73
batch start
#iterations: 8
currently lose_sum: 99.51938271522522
time_elpased: 1.745
batch start
#iterations: 9
currently lose_sum: 99.33576905727386
time_elpased: 1.732
batch start
#iterations: 10
currently lose_sum: 99.32732504606247
time_elpased: 1.733
batch start
#iterations: 11
currently lose_sum: 99.41479051113129
time_elpased: 1.729
batch start
#iterations: 12
currently lose_sum: 99.04738026857376
time_elpased: 1.721
batch start
#iterations: 13
currently lose_sum: 99.20952320098877
time_elpased: 1.76
batch start
#iterations: 14
currently lose_sum: 98.83753317594528
time_elpased: 1.727
batch start
#iterations: 15
currently lose_sum: 98.90632855892181
time_elpased: 1.75
batch start
#iterations: 16
currently lose_sum: 98.88545829057693
time_elpased: 1.725
batch start
#iterations: 17
currently lose_sum: 98.795962870121
time_elpased: 1.722
batch start
#iterations: 18
currently lose_sum: 98.44487076997757
time_elpased: 1.759
batch start
#iterations: 19
currently lose_sum: 98.71747922897339
time_elpased: 1.757
start validation test
0.599793814433
0.601160261059
0.597200782134
0.599173980382
0.599798366901
64.794
batch start
#iterations: 20
currently lose_sum: 98.6952736377716
time_elpased: 1.734
batch start
#iterations: 21
currently lose_sum: 98.31900119781494
time_elpased: 1.725
batch start
#iterations: 22
currently lose_sum: 98.44397753477097
time_elpased: 1.75
batch start
#iterations: 23
currently lose_sum: 98.64473807811737
time_elpased: 1.712
batch start
#iterations: 24
currently lose_sum: 98.14716619253159
time_elpased: 1.721
batch start
#iterations: 25
currently lose_sum: 98.26970916986465
time_elpased: 1.77
batch start
#iterations: 26
currently lose_sum: 98.41187340021133
time_elpased: 1.727
batch start
#iterations: 27
currently lose_sum: 97.93489617109299
time_elpased: 1.704
batch start
#iterations: 28
currently lose_sum: 97.89811432361603
time_elpased: 1.792
batch start
#iterations: 29
currently lose_sum: 97.64864885807037
time_elpased: 1.743
batch start
#iterations: 30
currently lose_sum: 98.09327846765518
time_elpased: 1.707
batch start
#iterations: 31
currently lose_sum: 97.64691334962845
time_elpased: 1.764
batch start
#iterations: 32
currently lose_sum: 97.73239040374756
time_elpased: 1.764
batch start
#iterations: 33
currently lose_sum: 97.5432648062706
time_elpased: 1.741
batch start
#iterations: 34
currently lose_sum: 97.52470630407333
time_elpased: 1.73
batch start
#iterations: 35
currently lose_sum: 97.61072140932083
time_elpased: 1.751
batch start
#iterations: 36
currently lose_sum: 97.36663460731506
time_elpased: 1.732
batch start
#iterations: 37
currently lose_sum: 97.46134012937546
time_elpased: 1.739
batch start
#iterations: 38
currently lose_sum: 97.39531177282333
time_elpased: 1.784
batch start
#iterations: 39
currently lose_sum: 97.07860195636749
time_elpased: 1.751
start validation test
0.60675257732
0.617567567568
0.564371719667
0.589772543959
0.606826983452
64.218
batch start
#iterations: 40
currently lose_sum: 97.50655710697174
time_elpased: 1.748
batch start
#iterations: 41
currently lose_sum: 97.03810822963715
time_elpased: 1.752
batch start
#iterations: 42
currently lose_sum: 97.09684365987778
time_elpased: 1.793
batch start
#iterations: 43
currently lose_sum: 96.94667768478394
time_elpased: 1.714
batch start
#iterations: 44
currently lose_sum: 97.02051717042923
time_elpased: 1.753
batch start
#iterations: 45
currently lose_sum: 96.80198383331299
time_elpased: 1.753
batch start
#iterations: 46
currently lose_sum: 96.80647677183151
time_elpased: 1.747
batch start
#iterations: 47
currently lose_sum: 96.92083871364594
time_elpased: 1.743
batch start
#iterations: 48
currently lose_sum: 96.69797241687775
time_elpased: 1.728
batch start
#iterations: 49
currently lose_sum: 96.18373793363571
time_elpased: 1.783
batch start
#iterations: 50
currently lose_sum: 96.32712733745575
time_elpased: 1.732
batch start
#iterations: 51
currently lose_sum: 96.50321072340012
time_elpased: 1.784
batch start
#iterations: 52
currently lose_sum: 96.35872250795364
time_elpased: 1.738
batch start
#iterations: 53
currently lose_sum: 96.26704674959183
time_elpased: 1.736
batch start
#iterations: 54
currently lose_sum: 95.98854202032089
time_elpased: 1.721
batch start
#iterations: 55
currently lose_sum: 96.04484778642654
time_elpased: 1.76
batch start
#iterations: 56
currently lose_sum: 96.06129670143127
time_elpased: 1.754
batch start
#iterations: 57
currently lose_sum: 96.18669009208679
time_elpased: 1.731
batch start
#iterations: 58
currently lose_sum: 95.66528344154358
time_elpased: 1.736
batch start
#iterations: 59
currently lose_sum: 96.20025396347046
time_elpased: 1.764
start validation test
0.592835051546
0.611835629921
0.511783472265
0.557355001401
0.592977350095
64.810
batch start
#iterations: 60
currently lose_sum: 95.64194148778915
time_elpased: 1.724
batch start
#iterations: 61
currently lose_sum: 96.03307145833969
time_elpased: 1.746
batch start
#iterations: 62
currently lose_sum: 95.23513168096542
time_elpased: 1.749
batch start
#iterations: 63
currently lose_sum: 95.4028931260109
time_elpased: 1.698
batch start
#iterations: 64
currently lose_sum: 95.6282342672348
time_elpased: 1.753
batch start
#iterations: 65
currently lose_sum: 95.31838393211365
time_elpased: 1.712
batch start
#iterations: 66
currently lose_sum: 95.62262350320816
time_elpased: 1.741
batch start
#iterations: 67
currently lose_sum: 94.63681644201279
time_elpased: 1.764
batch start
#iterations: 68
currently lose_sum: 95.14589023590088
time_elpased: 1.743
batch start
#iterations: 69
currently lose_sum: 95.14579355716705
time_elpased: 1.722
batch start
#iterations: 70
currently lose_sum: 95.15975213050842
time_elpased: 1.732
batch start
#iterations: 71
currently lose_sum: 95.00012803077698
time_elpased: 1.761
batch start
#iterations: 72
currently lose_sum: 94.57056963443756
time_elpased: 1.738
batch start
#iterations: 73
currently lose_sum: 94.80999928712845
time_elpased: 1.778
batch start
#iterations: 74
currently lose_sum: 93.93089991807938
time_elpased: 1.718
batch start
#iterations: 75
currently lose_sum: 94.71041184663773
time_elpased: 1.743
batch start
#iterations: 76
currently lose_sum: 94.39885747432709
time_elpased: 1.718
batch start
#iterations: 77
currently lose_sum: 94.70209193229675
time_elpased: 1.736
batch start
#iterations: 78
currently lose_sum: 94.4851222038269
time_elpased: 1.764
batch start
#iterations: 79
currently lose_sum: 94.04199939966202
time_elpased: 1.715
start validation test
0.592474226804
0.62388835682
0.469280642174
0.535651356749
0.592690512143
65.405
batch start
#iterations: 80
currently lose_sum: 94.06560271978378
time_elpased: 1.757
batch start
#iterations: 81
currently lose_sum: 93.9519647359848
time_elpased: 1.704
batch start
#iterations: 82
currently lose_sum: 94.04295879602432
time_elpased: 1.736
batch start
#iterations: 83
currently lose_sum: 93.80263924598694
time_elpased: 1.76
batch start
#iterations: 84
currently lose_sum: 93.77993112802505
time_elpased: 1.717
batch start
#iterations: 85
currently lose_sum: 93.9220649600029
time_elpased: 1.714
batch start
#iterations: 86
currently lose_sum: 93.65439867973328
time_elpased: 1.712
batch start
#iterations: 87
currently lose_sum: 93.54516977071762
time_elpased: 1.709
batch start
#iterations: 88
currently lose_sum: 93.44296389818192
time_elpased: 1.774
batch start
#iterations: 89
currently lose_sum: 93.4227836728096
time_elpased: 1.721
batch start
#iterations: 90
currently lose_sum: 92.87954676151276
time_elpased: 1.713
batch start
#iterations: 91
currently lose_sum: 93.44600182771683
time_elpased: 1.744
batch start
#iterations: 92
currently lose_sum: 93.32867538928986
time_elpased: 1.727
batch start
#iterations: 93
currently lose_sum: 92.53562647104263
time_elpased: 1.744
batch start
#iterations: 94
currently lose_sum: 93.30952751636505
time_elpased: 1.754
batch start
#iterations: 95
currently lose_sum: 93.1743426322937
time_elpased: 1.783
batch start
#iterations: 96
currently lose_sum: 92.33408552408218
time_elpased: 1.747
batch start
#iterations: 97
currently lose_sum: 92.93551236391068
time_elpased: 1.776
batch start
#iterations: 98
currently lose_sum: 92.57644003629684
time_elpased: 1.727
batch start
#iterations: 99
currently lose_sum: 92.64488977193832
time_elpased: 1.773
start validation test
0.580979381443
0.613137646053
0.44283214984
0.514251568569
0.581221920216
66.097
batch start
#iterations: 100
currently lose_sum: 92.17672723531723
time_elpased: 1.738
batch start
#iterations: 101
currently lose_sum: 92.23425459861755
time_elpased: 1.765
batch start
#iterations: 102
currently lose_sum: 92.59436255693436
time_elpased: 1.728
batch start
#iterations: 103
currently lose_sum: 92.20563197135925
time_elpased: 1.72
batch start
#iterations: 104
currently lose_sum: 92.15316814184189
time_elpased: 1.711
batch start
#iterations: 105
currently lose_sum: 92.2870301604271
time_elpased: 1.743
batch start
#iterations: 106
currently lose_sum: 92.07993876934052
time_elpased: 1.729
batch start
#iterations: 107
currently lose_sum: 91.62842208147049
time_elpased: 1.736
batch start
#iterations: 108
currently lose_sum: 91.28107303380966
time_elpased: 1.72
batch start
#iterations: 109
currently lose_sum: 91.6150706410408
time_elpased: 1.726
batch start
#iterations: 110
currently lose_sum: 91.32283627986908
time_elpased: 1.745
batch start
#iterations: 111
currently lose_sum: 91.73506391048431
time_elpased: 1.775
batch start
#iterations: 112
currently lose_sum: 91.1182935833931
time_elpased: 1.767
batch start
#iterations: 113
currently lose_sum: 90.98896092176437
time_elpased: 1.727
batch start
#iterations: 114
currently lose_sum: 91.36577117443085
time_elpased: 1.747
batch start
#iterations: 115
currently lose_sum: 90.7999854683876
time_elpased: 1.739
batch start
#iterations: 116
currently lose_sum: 91.19574326276779
time_elpased: 1.765
batch start
#iterations: 117
currently lose_sum: 91.1386690735817
time_elpased: 1.746
batch start
#iterations: 118
currently lose_sum: 91.29391062259674
time_elpased: 1.754
batch start
#iterations: 119
currently lose_sum: 90.48363173007965
time_elpased: 1.746
start validation test
0.588298969072
0.622625460731
0.451991355357
0.523761254547
0.588538278112
67.153
batch start
#iterations: 120
currently lose_sum: 90.41178637742996
time_elpased: 1.75
batch start
#iterations: 121
currently lose_sum: 90.13485085964203
time_elpased: 1.789
batch start
#iterations: 122
currently lose_sum: 90.22333198785782
time_elpased: 1.747
batch start
#iterations: 123
currently lose_sum: 90.38852936029434
time_elpased: 1.752
batch start
#iterations: 124
currently lose_sum: 90.11078798770905
time_elpased: 1.733
batch start
#iterations: 125
currently lose_sum: 90.5133770108223
time_elpased: 1.756
batch start
#iterations: 126
currently lose_sum: 89.87080991268158
time_elpased: 1.744
batch start
#iterations: 127
currently lose_sum: 90.41788053512573
time_elpased: 1.739
batch start
#iterations: 128
currently lose_sum: 89.9834486246109
time_elpased: 1.741
batch start
#iterations: 129
currently lose_sum: 90.10806208848953
time_elpased: 1.733
batch start
#iterations: 130
currently lose_sum: 89.38824528455734
time_elpased: 1.711
batch start
#iterations: 131
currently lose_sum: 89.71566188335419
time_elpased: 1.732
batch start
#iterations: 132
currently lose_sum: 89.28350472450256
time_elpased: 1.732
batch start
#iterations: 133
currently lose_sum: 89.73143059015274
time_elpased: 1.735
batch start
#iterations: 134
currently lose_sum: 89.24253934621811
time_elpased: 1.715
batch start
#iterations: 135
currently lose_sum: 89.5700341463089
time_elpased: 1.702
batch start
#iterations: 136
currently lose_sum: 89.13273620605469
time_elpased: 1.729
batch start
#iterations: 137
currently lose_sum: 88.78648239374161
time_elpased: 1.752
batch start
#iterations: 138
currently lose_sum: 88.79125773906708
time_elpased: 1.741
batch start
#iterations: 139
currently lose_sum: 88.93163335323334
time_elpased: 1.74
start validation test
0.584484536082
0.599043062201
0.515385407019
0.554074237982
0.584605850261
68.261
batch start
#iterations: 140
currently lose_sum: 88.27935379743576
time_elpased: 1.763
batch start
#iterations: 141
currently lose_sum: 89.05951088666916
time_elpased: 1.706
batch start
#iterations: 142
currently lose_sum: 88.84357333183289
time_elpased: 1.736
batch start
#iterations: 143
currently lose_sum: 87.86565893888474
time_elpased: 1.74
batch start
#iterations: 144
currently lose_sum: 88.69635343551636
time_elpased: 1.725
batch start
#iterations: 145
currently lose_sum: 88.3312548995018
time_elpased: 1.782
batch start
#iterations: 146
currently lose_sum: 88.3722739815712
time_elpased: 1.765
batch start
#iterations: 147
currently lose_sum: 88.31904464960098
time_elpased: 1.761
batch start
#iterations: 148
currently lose_sum: 88.04428631067276
time_elpased: 1.713
batch start
#iterations: 149
currently lose_sum: 87.91261279582977
time_elpased: 1.725
batch start
#iterations: 150
currently lose_sum: 87.85279756784439
time_elpased: 1.741
batch start
#iterations: 151
currently lose_sum: 87.87900549173355
time_elpased: 1.778
batch start
#iterations: 152
currently lose_sum: 87.52081298828125
time_elpased: 1.751
batch start
#iterations: 153
currently lose_sum: 88.03715479373932
time_elpased: 1.709
batch start
#iterations: 154
currently lose_sum: 87.31101590394974
time_elpased: 1.757
batch start
#iterations: 155
currently lose_sum: 87.11389172077179
time_elpased: 1.699
batch start
#iterations: 156
currently lose_sum: 88.02680057287216
time_elpased: 1.792
batch start
#iterations: 157
currently lose_sum: 86.83415704965591
time_elpased: 1.718
batch start
#iterations: 158
currently lose_sum: 87.03178006410599
time_elpased: 1.76
batch start
#iterations: 159
currently lose_sum: 87.21196854114532
time_elpased: 1.701
start validation test
0.568608247423
0.58838185156
0.461768035402
0.517442195699
0.568795821894
71.410
batch start
#iterations: 160
currently lose_sum: 87.01369416713715
time_elpased: 1.806
batch start
#iterations: 161
currently lose_sum: 87.16936266422272
time_elpased: 1.714
batch start
#iterations: 162
currently lose_sum: 86.97774088382721
time_elpased: 1.728
batch start
#iterations: 163
currently lose_sum: 86.95501625537872
time_elpased: 1.705
batch start
#iterations: 164
currently lose_sum: 87.10656607151031
time_elpased: 1.743
batch start
#iterations: 165
currently lose_sum: 86.07460355758667
time_elpased: 1.734
batch start
#iterations: 166
currently lose_sum: 85.903764128685
time_elpased: 1.729
batch start
#iterations: 167
currently lose_sum: 86.382248878479
time_elpased: 1.761
batch start
#iterations: 168
currently lose_sum: 86.23465913534164
time_elpased: 1.741
batch start
#iterations: 169
currently lose_sum: 85.86831897497177
time_elpased: 1.738
batch start
#iterations: 170
currently lose_sum: 85.95193946361542
time_elpased: 1.767
batch start
#iterations: 171
currently lose_sum: 86.16414624452591
time_elpased: 1.737
batch start
#iterations: 172
currently lose_sum: 86.03567087650299
time_elpased: 1.751
batch start
#iterations: 173
currently lose_sum: 85.33077949285507
time_elpased: 1.75
batch start
#iterations: 174
currently lose_sum: 85.34038144350052
time_elpased: 1.731
batch start
#iterations: 175
currently lose_sum: 85.48853802680969
time_elpased: 1.753
batch start
#iterations: 176
currently lose_sum: 85.77696710824966
time_elpased: 1.726
batch start
#iterations: 177
currently lose_sum: 84.61587792634964
time_elpased: 1.78
batch start
#iterations: 178
currently lose_sum: 85.36051988601685
time_elpased: 1.722
batch start
#iterations: 179
currently lose_sum: 85.05554264783859
time_elpased: 1.737
start validation test
0.579896907216
0.630214392554
0.390243902439
0.482013474005
0.580229872318
72.946
batch start
#iterations: 180
currently lose_sum: 85.2656432390213
time_elpased: 1.72
batch start
#iterations: 181
currently lose_sum: 85.71301335096359
time_elpased: 1.757
batch start
#iterations: 182
currently lose_sum: 84.95738035440445
time_elpased: 1.739
batch start
#iterations: 183
currently lose_sum: 85.31327825784683
time_elpased: 1.737
batch start
#iterations: 184
currently lose_sum: 85.11838346719742
time_elpased: 1.739
batch start
#iterations: 185
currently lose_sum: 84.22536331415176
time_elpased: 1.8
batch start
#iterations: 186
currently lose_sum: 84.62217366695404
time_elpased: 1.738
batch start
#iterations: 187
currently lose_sum: 85.02551990747452
time_elpased: 1.787
batch start
#iterations: 188
currently lose_sum: 84.42476803064346
time_elpased: 1.773
batch start
#iterations: 189
currently lose_sum: 84.05867838859558
time_elpased: 1.725
batch start
#iterations: 190
currently lose_sum: 83.96760642528534
time_elpased: 1.763
batch start
#iterations: 191
currently lose_sum: 84.15833193063736
time_elpased: 1.723
batch start
#iterations: 192
currently lose_sum: 84.37990355491638
time_elpased: 1.737
batch start
#iterations: 193
currently lose_sum: 83.8209558725357
time_elpased: 1.714
batch start
#iterations: 194
currently lose_sum: 83.83594632148743
time_elpased: 1.735
batch start
#iterations: 195
currently lose_sum: 83.54977232217789
time_elpased: 1.732
batch start
#iterations: 196
currently lose_sum: 83.27888596057892
time_elpased: 1.739
batch start
#iterations: 197
currently lose_sum: 83.9480813741684
time_elpased: 1.717
batch start
#iterations: 198
currently lose_sum: 83.2338736653328
time_elpased: 1.801
batch start
#iterations: 199
currently lose_sum: 83.33768218755722
time_elpased: 1.739
start validation test
0.563969072165
0.59930533628
0.390655552125
0.472992336926
0.564273350781
75.998
batch start
#iterations: 200
currently lose_sum: 83.0487453341484
time_elpased: 1.764
batch start
#iterations: 201
currently lose_sum: 83.37908482551575
time_elpased: 1.732
batch start
#iterations: 202
currently lose_sum: 82.77020663022995
time_elpased: 1.741
batch start
#iterations: 203
currently lose_sum: 83.0721185207367
time_elpased: 1.708
batch start
#iterations: 204
currently lose_sum: 83.57348531484604
time_elpased: 1.76
batch start
#iterations: 205
currently lose_sum: 83.12249797582626
time_elpased: 1.746
batch start
#iterations: 206
currently lose_sum: 82.88231256604195
time_elpased: 1.724
batch start
#iterations: 207
currently lose_sum: 82.42598271369934
time_elpased: 1.74
batch start
#iterations: 208
currently lose_sum: 82.79468107223511
time_elpased: 1.733
batch start
#iterations: 209
currently lose_sum: 82.41415765881538
time_elpased: 1.769
batch start
#iterations: 210
currently lose_sum: 82.3163520693779
time_elpased: 1.788
batch start
#iterations: 211
currently lose_sum: 82.34529781341553
time_elpased: 1.791
batch start
#iterations: 212
currently lose_sum: 81.92614793777466
time_elpased: 1.777
batch start
#iterations: 213
currently lose_sum: 81.46358281373978
time_elpased: 1.818
batch start
#iterations: 214
currently lose_sum: 82.51095449924469
time_elpased: 1.782
batch start
#iterations: 215
currently lose_sum: 82.09848839044571
time_elpased: 1.744
batch start
#iterations: 216
currently lose_sum: 81.56476384401321
time_elpased: 1.79
batch start
#iterations: 217
currently lose_sum: 81.98723900318146
time_elpased: 1.739
batch start
#iterations: 218
currently lose_sum: 81.93073892593384
time_elpased: 1.774
batch start
#iterations: 219
currently lose_sum: 81.44151279330254
time_elpased: 1.716
start validation test
0.556907216495
0.58329618071
0.403931254502
0.477319713
0.557175789391
81.242
batch start
#iterations: 220
currently lose_sum: 81.8492078781128
time_elpased: 1.711
batch start
#iterations: 221
currently lose_sum: 81.56720063090324
time_elpased: 1.782
batch start
#iterations: 222
currently lose_sum: 81.35512644052505
time_elpased: 1.769
batch start
#iterations: 223
currently lose_sum: 81.58890971541405
time_elpased: 1.734
batch start
#iterations: 224
currently lose_sum: 80.92871269583702
time_elpased: 1.719
batch start
#iterations: 225
currently lose_sum: 81.26580119132996
time_elpased: 1.76
batch start
#iterations: 226
currently lose_sum: 81.20267164707184
time_elpased: 1.763
batch start
#iterations: 227
currently lose_sum: 81.09171879291534
time_elpased: 1.738
batch start
#iterations: 228
currently lose_sum: 80.69705447554588
time_elpased: 1.758
batch start
#iterations: 229
currently lose_sum: 81.07723784446716
time_elpased: 1.75
batch start
#iterations: 230
currently lose_sum: 81.20124211907387
time_elpased: 1.759
batch start
#iterations: 231
currently lose_sum: 80.57464802265167
time_elpased: 1.719
batch start
#iterations: 232
currently lose_sum: 80.91484570503235
time_elpased: 1.725
batch start
#iterations: 233
currently lose_sum: 80.91137486696243
time_elpased: 1.744
batch start
#iterations: 234
currently lose_sum: 80.59464329481125
time_elpased: 1.714
batch start
#iterations: 235
currently lose_sum: 80.24407088756561
time_elpased: 1.752
batch start
#iterations: 236
currently lose_sum: 80.0080853998661
time_elpased: 1.702
batch start
#iterations: 237
currently lose_sum: 79.6023019850254
time_elpased: 1.769
batch start
#iterations: 238
currently lose_sum: 79.8951167166233
time_elpased: 1.729
batch start
#iterations: 239
currently lose_sum: 79.885148614645
time_elpased: 1.754
start validation test
0.561649484536
0.58626084483
0.424205001544
0.49223787915
0.561890789525
78.481
batch start
#iterations: 240
currently lose_sum: 80.34717011451721
time_elpased: 1.746
batch start
#iterations: 241
currently lose_sum: 80.05188488960266
time_elpased: 1.714
batch start
#iterations: 242
currently lose_sum: 79.32282796502113
time_elpased: 1.761
batch start
#iterations: 243
currently lose_sum: 79.38906076550484
time_elpased: 1.733
batch start
#iterations: 244
currently lose_sum: 79.31099674105644
time_elpased: 1.746
batch start
#iterations: 245
currently lose_sum: 79.67910051345825
time_elpased: 1.732
batch start
#iterations: 246
currently lose_sum: 79.28212505578995
time_elpased: 1.722
batch start
#iterations: 247
currently lose_sum: 78.84273374080658
time_elpased: 1.748
batch start
#iterations: 248
currently lose_sum: 79.1905164718628
time_elpased: 1.725
batch start
#iterations: 249
currently lose_sum: 78.54562175273895
time_elpased: 1.772
batch start
#iterations: 250
currently lose_sum: 79.49650982022285
time_elpased: 1.777
batch start
#iterations: 251
currently lose_sum: 78.78479743003845
time_elpased: 1.769
batch start
#iterations: 252
currently lose_sum: 78.63445702195168
time_elpased: 1.72
batch start
#iterations: 253
currently lose_sum: 78.72818818688393
time_elpased: 1.741
batch start
#iterations: 254
currently lose_sum: 78.3548583984375
time_elpased: 1.716
batch start
#iterations: 255
currently lose_sum: 79.452028632164
time_elpased: 1.75
batch start
#iterations: 256
currently lose_sum: 78.39730522036552
time_elpased: 1.73
batch start
#iterations: 257
currently lose_sum: 79.47813975811005
time_elpased: 1.762
batch start
#iterations: 258
currently lose_sum: 78.17476359009743
time_elpased: 1.752
batch start
#iterations: 259
currently lose_sum: 78.00027793645859
time_elpased: 1.781
start validation test
0.562731958763
0.591083554768
0.412061335803
0.485598205082
0.562996484281
80.686
batch start
#iterations: 260
currently lose_sum: 77.61145406961441
time_elpased: 1.753
batch start
#iterations: 261
currently lose_sum: 77.85249164700508
time_elpased: 1.75
batch start
#iterations: 262
currently lose_sum: 78.17356488108635
time_elpased: 1.716
batch start
#iterations: 263
currently lose_sum: 77.27208551764488
time_elpased: 1.71
batch start
#iterations: 264
currently lose_sum: 78.15483865141869
time_elpased: 1.736
batch start
#iterations: 265
currently lose_sum: 78.34672665596008
time_elpased: 1.773
batch start
#iterations: 266
currently lose_sum: 77.77410930395126
time_elpased: 1.732
batch start
#iterations: 267
currently lose_sum: 77.39362531900406
time_elpased: 1.708
batch start
#iterations: 268
currently lose_sum: 77.79955902695656
time_elpased: 1.739
batch start
#iterations: 269
currently lose_sum: 77.88767078518867
time_elpased: 1.756
batch start
#iterations: 270
currently lose_sum: 77.11522981524467
time_elpased: 1.798
batch start
#iterations: 271
currently lose_sum: 77.55371779203415
time_elpased: 1.726
batch start
#iterations: 272
currently lose_sum: 77.72379037737846
time_elpased: 1.73
batch start
#iterations: 273
currently lose_sum: 77.47246032953262
time_elpased: 1.734
batch start
#iterations: 274
currently lose_sum: 77.37106958031654
time_elpased: 1.745
batch start
#iterations: 275
currently lose_sum: 76.9448256790638
time_elpased: 1.79
batch start
#iterations: 276
currently lose_sum: 77.07566139101982
time_elpased: 1.769
batch start
#iterations: 277
currently lose_sum: 77.20187875628471
time_elpased: 1.736
batch start
#iterations: 278
currently lose_sum: 76.23369511961937
time_elpased: 1.727
batch start
#iterations: 279
currently lose_sum: 76.71241158246994
time_elpased: 1.731
start validation test
0.555773195876
0.590008190008
0.370690542348
0.455315383643
0.556098137022
84.895
batch start
#iterations: 280
currently lose_sum: 76.43630802631378
time_elpased: 1.726
batch start
#iterations: 281
currently lose_sum: 76.90108701586723
time_elpased: 1.767
batch start
#iterations: 282
currently lose_sum: 76.76169368624687
time_elpased: 1.778
batch start
#iterations: 283
currently lose_sum: 76.70534154772758
time_elpased: 1.74
batch start
#iterations: 284
currently lose_sum: 76.79385271668434
time_elpased: 1.71
batch start
#iterations: 285
currently lose_sum: 76.31886377930641
time_elpased: 1.731
batch start
#iterations: 286
currently lose_sum: 76.83486732840538
time_elpased: 1.752
batch start
#iterations: 287
currently lose_sum: 76.30321049690247
time_elpased: 1.744
batch start
#iterations: 288
currently lose_sum: 76.11442571878433
time_elpased: 1.73
batch start
#iterations: 289
currently lose_sum: 76.27437126636505
time_elpased: 1.724
batch start
#iterations: 290
currently lose_sum: 76.1049944460392
time_elpased: 1.787
batch start
#iterations: 291
currently lose_sum: 75.7486357986927
time_elpased: 1.738
batch start
#iterations: 292
currently lose_sum: 75.44036358594894
time_elpased: 1.781
batch start
#iterations: 293
currently lose_sum: 76.01890793442726
time_elpased: 1.726
batch start
#iterations: 294
currently lose_sum: 75.72305610775948
time_elpased: 1.74
batch start
#iterations: 295
currently lose_sum: 75.26792922616005
time_elpased: 1.744
batch start
#iterations: 296
currently lose_sum: 75.45169362425804
time_elpased: 1.746
batch start
#iterations: 297
currently lose_sum: 75.62541925907135
time_elpased: 1.716
batch start
#iterations: 298
currently lose_sum: 75.59743332862854
time_elpased: 1.732
batch start
#iterations: 299
currently lose_sum: 75.4451647400856
time_elpased: 1.71
start validation test
0.555618556701
0.584987593052
0.388185654008
0.466687287349
0.555912510986
86.626
batch start
#iterations: 300
currently lose_sum: 75.18950146436691
time_elpased: 1.705
batch start
#iterations: 301
currently lose_sum: 75.56956228613853
time_elpased: 1.765
batch start
#iterations: 302
currently lose_sum: 75.28434586524963
time_elpased: 1.755
batch start
#iterations: 303
currently lose_sum: 75.38583064079285
time_elpased: 1.726
batch start
#iterations: 304
currently lose_sum: 74.94680532813072
time_elpased: 1.753
batch start
#iterations: 305
currently lose_sum: 74.89612174034119
time_elpased: 1.716
batch start
#iterations: 306
currently lose_sum: 75.1225113272667
time_elpased: 1.71
batch start
#iterations: 307
currently lose_sum: 74.63342091441154
time_elpased: 1.729
batch start
#iterations: 308
currently lose_sum: 75.62633463740349
time_elpased: 1.72
batch start
#iterations: 309
currently lose_sum: 75.00603976845741
time_elpased: 1.745
batch start
#iterations: 310
currently lose_sum: 74.77260828018188
time_elpased: 1.741
batch start
#iterations: 311
currently lose_sum: 74.40049150586128
time_elpased: 1.767
batch start
#iterations: 312
currently lose_sum: 74.20370650291443
time_elpased: 1.772
batch start
#iterations: 313
currently lose_sum: 74.16966971755028
time_elpased: 1.711
batch start
#iterations: 314
currently lose_sum: 74.12354612350464
time_elpased: 1.742
batch start
#iterations: 315
currently lose_sum: 74.34458017349243
time_elpased: 1.71
batch start
#iterations: 316
currently lose_sum: 74.30793741345406
time_elpased: 1.727
batch start
#iterations: 317
currently lose_sum: 74.12961116433144
time_elpased: 1.734
batch start
#iterations: 318
currently lose_sum: 73.84189975261688
time_elpased: 1.758
batch start
#iterations: 319
currently lose_sum: 73.64254820346832
time_elpased: 1.753
start validation test
0.560257731959
0.597181252049
0.375012864053
0.460711802263
0.560582957896
90.021
batch start
#iterations: 320
currently lose_sum: 74.00517371296883
time_elpased: 1.728
batch start
#iterations: 321
currently lose_sum: 74.21829125285149
time_elpased: 1.743
batch start
#iterations: 322
currently lose_sum: 74.16824489831924
time_elpased: 1.732
batch start
#iterations: 323
currently lose_sum: 73.94136267900467
time_elpased: 1.723
batch start
#iterations: 324
currently lose_sum: 74.14479523897171
time_elpased: 1.715
batch start
#iterations: 325
currently lose_sum: 73.75862327218056
time_elpased: 1.771
batch start
#iterations: 326
currently lose_sum: 74.41621172428131
time_elpased: 1.723
batch start
#iterations: 327
currently lose_sum: 73.51882901787758
time_elpased: 1.755
batch start
#iterations: 328
currently lose_sum: 72.90442365407944
time_elpased: 1.742
batch start
#iterations: 329
currently lose_sum: 73.83469671010971
time_elpased: 1.711
batch start
#iterations: 330
currently lose_sum: 73.4296004474163
time_elpased: 1.755
batch start
#iterations: 331
currently lose_sum: 73.59137886762619
time_elpased: 1.762
batch start
#iterations: 332
currently lose_sum: 73.2343202829361
time_elpased: 1.743
batch start
#iterations: 333
currently lose_sum: 72.9944414794445
time_elpased: 1.733
batch start
#iterations: 334
currently lose_sum: 73.14984047412872
time_elpased: 1.728
batch start
#iterations: 335
currently lose_sum: 72.54256847500801
time_elpased: 1.743
batch start
#iterations: 336
currently lose_sum: 73.08503058552742
time_elpased: 1.73
batch start
#iterations: 337
currently lose_sum: 72.81813278794289
time_elpased: 1.731
batch start
#iterations: 338
currently lose_sum: 73.14064371585846
time_elpased: 1.718
batch start
#iterations: 339
currently lose_sum: 73.17599961161613
time_elpased: 1.774
start validation test
0.545257731959
0.57265789901
0.362972110734
0.444318468128
0.545577762482
93.934
batch start
#iterations: 340
currently lose_sum: 73.18387812376022
time_elpased: 1.727
batch start
#iterations: 341
currently lose_sum: 73.415373980999
time_elpased: 1.74
batch start
#iterations: 342
currently lose_sum: 73.05716317892075
time_elpased: 1.714
batch start
#iterations: 343
currently lose_sum: 73.29314956068993
time_elpased: 1.746
batch start
#iterations: 344
currently lose_sum: 72.41316649317741
time_elpased: 1.734
batch start
#iterations: 345
currently lose_sum: 72.08367991447449
time_elpased: 1.719
batch start
#iterations: 346
currently lose_sum: 72.57271081209183
time_elpased: 1.797
batch start
#iterations: 347
currently lose_sum: 71.89933302998543
time_elpased: 1.728
batch start
#iterations: 348
currently lose_sum: 72.44026267528534
time_elpased: 1.731
batch start
#iterations: 349
currently lose_sum: 72.50400683283806
time_elpased: 1.724
batch start
#iterations: 350
currently lose_sum: 71.6287776529789
time_elpased: 1.832
batch start
#iterations: 351
currently lose_sum: 72.22029128670692
time_elpased: 1.748
batch start
#iterations: 352
currently lose_sum: 71.63653895258904
time_elpased: 1.754
batch start
#iterations: 353
currently lose_sum: 72.1133044064045
time_elpased: 1.733
batch start
#iterations: 354
currently lose_sum: 71.97606647014618
time_elpased: 1.705
batch start
#iterations: 355
currently lose_sum: 71.41302490234375
time_elpased: 1.744
batch start
#iterations: 356
currently lose_sum: 71.52056288719177
time_elpased: 1.758
batch start
#iterations: 357
currently lose_sum: 71.97604024410248
time_elpased: 1.743
batch start
#iterations: 358
currently lose_sum: 71.58617687225342
time_elpased: 1.722
batch start
#iterations: 359
currently lose_sum: 71.8364639878273
time_elpased: 1.726
start validation test
0.54206185567
0.579257849667
0.313265411135
0.406625701309
0.542463543118
96.989
batch start
#iterations: 360
currently lose_sum: 71.65552765130997
time_elpased: 1.71
batch start
#iterations: 361
currently lose_sum: 71.2825493812561
time_elpased: 1.705
batch start
#iterations: 362
currently lose_sum: 71.50219297409058
time_elpased: 1.703
batch start
#iterations: 363
currently lose_sum: 71.06188929080963
time_elpased: 1.756
batch start
#iterations: 364
currently lose_sum: 71.37208923697472
time_elpased: 1.731
batch start
#iterations: 365
currently lose_sum: 71.78599095344543
time_elpased: 1.736
batch start
#iterations: 366
currently lose_sum: 72.0024020075798
time_elpased: 1.74
batch start
#iterations: 367
currently lose_sum: 71.3496322631836
time_elpased: 1.707
batch start
#iterations: 368
currently lose_sum: 70.84296289086342
time_elpased: 1.767
batch start
#iterations: 369
currently lose_sum: 71.06111907958984
time_elpased: 1.751
batch start
#iterations: 370
currently lose_sum: 70.26580101251602
time_elpased: 1.725
batch start
#iterations: 371
currently lose_sum: 70.3006517291069
time_elpased: 1.742
batch start
#iterations: 372
currently lose_sum: 70.64677467942238
time_elpased: 1.783
batch start
#iterations: 373
currently lose_sum: 71.35369125008583
time_elpased: 1.745
batch start
#iterations: 374
currently lose_sum: 70.74991032481194
time_elpased: 1.723
batch start
#iterations: 375
currently lose_sum: 71.08668193221092
time_elpased: 1.709
batch start
#iterations: 376
currently lose_sum: 70.87643504142761
time_elpased: 1.745
batch start
#iterations: 377
currently lose_sum: 70.95136964321136
time_elpased: 1.709
batch start
#iterations: 378
currently lose_sum: 71.26465055346489
time_elpased: 1.741
batch start
#iterations: 379
currently lose_sum: 70.7101015150547
time_elpased: 1.735
start validation test
0.540515463918
0.57506075902
0.316558608624
0.408336652064
0.54090865472
101.087
batch start
#iterations: 380
currently lose_sum: 70.78706234693527
time_elpased: 1.735
batch start
#iterations: 381
currently lose_sum: 70.58570459485054
time_elpased: 1.762
batch start
#iterations: 382
currently lose_sum: 70.1205366551876
time_elpased: 1.727
batch start
#iterations: 383
currently lose_sum: 70.25707215070724
time_elpased: 1.74
batch start
#iterations: 384
currently lose_sum: 69.99669975042343
time_elpased: 1.76
batch start
#iterations: 385
currently lose_sum: 70.09467831254005
time_elpased: 1.747
batch start
#iterations: 386
currently lose_sum: 70.24508753418922
time_elpased: 1.745
batch start
#iterations: 387
currently lose_sum: 70.52100792527199
time_elpased: 1.726
batch start
#iterations: 388
currently lose_sum: 70.07120847702026
time_elpased: 1.725
batch start
#iterations: 389
currently lose_sum: 70.00133258104324
time_elpased: 1.744
batch start
#iterations: 390
currently lose_sum: 69.95782220363617
time_elpased: 1.786
batch start
#iterations: 391
currently lose_sum: 69.75659209489822
time_elpased: 1.761
batch start
#iterations: 392
currently lose_sum: 69.6428672671318
time_elpased: 1.738
batch start
#iterations: 393
currently lose_sum: 70.06373649835587
time_elpased: 1.715
batch start
#iterations: 394
currently lose_sum: 70.49638989567757
time_elpased: 1.745
batch start
#iterations: 395
currently lose_sum: 69.77652978897095
time_elpased: 1.764
batch start
#iterations: 396
currently lose_sum: 69.18988087773323
time_elpased: 1.735
batch start
#iterations: 397
currently lose_sum: 69.99155893921852
time_elpased: 1.726
batch start
#iterations: 398
currently lose_sum: 70.0907450914383
time_elpased: 1.734
batch start
#iterations: 399
currently lose_sum: 69.64386054873466
time_elpased: 1.772
start validation test
0.545515463918
0.596732588134
0.285684882165
0.386387361681
0.54597163658
105.370
acc: 0.606
pre: 0.617
rec: 0.562
F1: 0.588
auc: 0.642
