start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.25720876455307
time_elpased: 1.591
batch start
#iterations: 1
currently lose_sum: 100.15338444709778
time_elpased: 1.582
batch start
#iterations: 2
currently lose_sum: 99.8478547334671
time_elpased: 1.544
batch start
#iterations: 3
currently lose_sum: 99.8875641822815
time_elpased: 1.653
batch start
#iterations: 4
currently lose_sum: 99.65254586935043
time_elpased: 1.581
batch start
#iterations: 5
currently lose_sum: 99.63432890176773
time_elpased: 1.58
batch start
#iterations: 6
currently lose_sum: 99.57516121864319
time_elpased: 1.583
batch start
#iterations: 7
currently lose_sum: 99.22281801700592
time_elpased: 1.552
batch start
#iterations: 8
currently lose_sum: 99.18783098459244
time_elpased: 1.575
batch start
#iterations: 9
currently lose_sum: 99.29337054491043
time_elpased: 1.591
batch start
#iterations: 10
currently lose_sum: 99.10197973251343
time_elpased: 1.583
batch start
#iterations: 11
currently lose_sum: 99.0004170536995
time_elpased: 1.581
batch start
#iterations: 12
currently lose_sum: 99.21528649330139
time_elpased: 1.557
batch start
#iterations: 13
currently lose_sum: 98.6792721748352
time_elpased: 1.626
batch start
#iterations: 14
currently lose_sum: 98.97257071733475
time_elpased: 1.606
batch start
#iterations: 15
currently lose_sum: 98.48634856939316
time_elpased: 1.585
batch start
#iterations: 16
currently lose_sum: 98.86873054504395
time_elpased: 1.55
batch start
#iterations: 17
currently lose_sum: 98.62165629863739
time_elpased: 1.608
batch start
#iterations: 18
currently lose_sum: 98.54140591621399
time_elpased: 1.574
batch start
#iterations: 19
currently lose_sum: 98.67435038089752
time_elpased: 1.554
start validation test
0.608092783505
0.62400281558
0.547391170114
0.583191710981
0.60819935455
64.610
batch start
#iterations: 20
currently lose_sum: 98.34009683132172
time_elpased: 1.571
batch start
#iterations: 21
currently lose_sum: 98.25108033418655
time_elpased: 1.591
batch start
#iterations: 22
currently lose_sum: 98.49067848920822
time_elpased: 1.619
batch start
#iterations: 23
currently lose_sum: 98.1382348537445
time_elpased: 1.56
batch start
#iterations: 24
currently lose_sum: 98.35081887245178
time_elpased: 1.598
batch start
#iterations: 25
currently lose_sum: 98.07566577196121
time_elpased: 1.544
batch start
#iterations: 26
currently lose_sum: 97.90656459331512
time_elpased: 1.552
batch start
#iterations: 27
currently lose_sum: 97.9319794178009
time_elpased: 1.613
batch start
#iterations: 28
currently lose_sum: 97.85522711277008
time_elpased: 1.563
batch start
#iterations: 29
currently lose_sum: 98.22568303346634
time_elpased: 1.573
batch start
#iterations: 30
currently lose_sum: 97.90799987316132
time_elpased: 1.561
batch start
#iterations: 31
currently lose_sum: 98.02393633127213
time_elpased: 1.616
batch start
#iterations: 32
currently lose_sum: 97.34512382745743
time_elpased: 1.569
batch start
#iterations: 33
currently lose_sum: 97.76017343997955
time_elpased: 1.614
batch start
#iterations: 34
currently lose_sum: 97.68318343162537
time_elpased: 1.638
batch start
#iterations: 35
currently lose_sum: 97.67407864332199
time_elpased: 1.578
batch start
#iterations: 36
currently lose_sum: 97.8487251996994
time_elpased: 1.573
batch start
#iterations: 37
currently lose_sum: 97.61977219581604
time_elpased: 1.591
batch start
#iterations: 38
currently lose_sum: 97.70558232069016
time_elpased: 1.584
batch start
#iterations: 39
currently lose_sum: 97.89326286315918
time_elpased: 1.567
start validation test
0.61881443299
0.623747601791
0.602243490789
0.612806953244
0.618843525835
64.194
batch start
#iterations: 40
currently lose_sum: 97.58052223920822
time_elpased: 1.543
batch start
#iterations: 41
currently lose_sum: 97.53474593162537
time_elpased: 1.59
batch start
#iterations: 42
currently lose_sum: 97.44366919994354
time_elpased: 1.541
batch start
#iterations: 43
currently lose_sum: 97.48939365148544
time_elpased: 1.562
batch start
#iterations: 44
currently lose_sum: 97.36010211706161
time_elpased: 1.554
batch start
#iterations: 45
currently lose_sum: 97.1373605132103
time_elpased: 1.541
batch start
#iterations: 46
currently lose_sum: 97.38870024681091
time_elpased: 1.549
batch start
#iterations: 47
currently lose_sum: 97.22869527339935
time_elpased: 1.585
batch start
#iterations: 48
currently lose_sum: 97.4337866306305
time_elpased: 1.605
batch start
#iterations: 49
currently lose_sum: 97.36906802654266
time_elpased: 1.538
batch start
#iterations: 50
currently lose_sum: 97.61309587955475
time_elpased: 1.575
batch start
#iterations: 51
currently lose_sum: 97.03265953063965
time_elpased: 1.568
batch start
#iterations: 52
currently lose_sum: 97.02692800760269
time_elpased: 1.552
batch start
#iterations: 53
currently lose_sum: 97.41257274150848
time_elpased: 1.597
batch start
#iterations: 54
currently lose_sum: 97.05137538909912
time_elpased: 1.54
batch start
#iterations: 55
currently lose_sum: 96.91525989770889
time_elpased: 1.639
batch start
#iterations: 56
currently lose_sum: 97.185595870018
time_elpased: 1.556
batch start
#iterations: 57
currently lose_sum: 96.90235590934753
time_elpased: 1.574
batch start
#iterations: 58
currently lose_sum: 96.92888849973679
time_elpased: 1.549
batch start
#iterations: 59
currently lose_sum: 97.00177997350693
time_elpased: 1.595
start validation test
0.611546391753
0.607491375062
0.634249253885
0.62058201591
0.611506533376
63.942
batch start
#iterations: 60
currently lose_sum: 96.80851352214813
time_elpased: 1.565
batch start
#iterations: 61
currently lose_sum: 97.04039794206619
time_elpased: 1.58
batch start
#iterations: 62
currently lose_sum: 96.76555901765823
time_elpased: 1.598
batch start
#iterations: 63
currently lose_sum: 97.14722275733948
time_elpased: 1.628
batch start
#iterations: 64
currently lose_sum: 97.06173062324524
time_elpased: 1.582
batch start
#iterations: 65
currently lose_sum: 96.48429840803146
time_elpased: 1.553
batch start
#iterations: 66
currently lose_sum: 96.17970144748688
time_elpased: 1.572
batch start
#iterations: 67
currently lose_sum: 96.29398930072784
time_elpased: 1.6
batch start
#iterations: 68
currently lose_sum: 96.55880463123322
time_elpased: 1.585
batch start
#iterations: 69
currently lose_sum: 96.44738590717316
time_elpased: 1.586
batch start
#iterations: 70
currently lose_sum: 96.06225991249084
time_elpased: 1.547
batch start
#iterations: 71
currently lose_sum: 96.0867315530777
time_elpased: 1.564
batch start
#iterations: 72
currently lose_sum: 96.00234216451645
time_elpased: 1.56
batch start
#iterations: 73
currently lose_sum: 96.17464655637741
time_elpased: 1.606
batch start
#iterations: 74
currently lose_sum: 96.18536925315857
time_elpased: 1.619
batch start
#iterations: 75
currently lose_sum: 95.8607308268547
time_elpased: 1.561
batch start
#iterations: 76
currently lose_sum: 96.24046581983566
time_elpased: 1.603
batch start
#iterations: 77
currently lose_sum: 95.844622194767
time_elpased: 1.583
batch start
#iterations: 78
currently lose_sum: 96.14691716432571
time_elpased: 1.608
batch start
#iterations: 79
currently lose_sum: 96.29222005605698
time_elpased: 1.585
start validation test
0.589175257732
0.590023704009
0.589173613255
0.589598352214
0.589175260619
65.079
batch start
#iterations: 80
currently lose_sum: 96.02563148736954
time_elpased: 1.659
batch start
#iterations: 81
currently lose_sum: 96.28918927907944
time_elpased: 1.552
batch start
#iterations: 82
currently lose_sum: 96.0679007768631
time_elpased: 1.539
batch start
#iterations: 83
currently lose_sum: 96.03262215852737
time_elpased: 1.562
batch start
#iterations: 84
currently lose_sum: 95.95831364393234
time_elpased: 1.596
batch start
#iterations: 85
currently lose_sum: 96.01953488588333
time_elpased: 1.544
batch start
#iterations: 86
currently lose_sum: 95.72023469209671
time_elpased: 1.602
batch start
#iterations: 87
currently lose_sum: 95.6936731338501
time_elpased: 1.568
batch start
#iterations: 88
currently lose_sum: 95.74920058250427
time_elpased: 1.603
batch start
#iterations: 89
currently lose_sum: 95.75327789783478
time_elpased: 1.589
batch start
#iterations: 90
currently lose_sum: 95.48928791284561
time_elpased: 1.605
batch start
#iterations: 91
currently lose_sum: 95.56537866592407
time_elpased: 1.562
batch start
#iterations: 92
currently lose_sum: 95.61209672689438
time_elpased: 1.577
batch start
#iterations: 93
currently lose_sum: 95.49415022134781
time_elpased: 1.588
batch start
#iterations: 94
currently lose_sum: 95.41142427921295
time_elpased: 1.587
batch start
#iterations: 95
currently lose_sum: 95.58234751224518
time_elpased: 1.565
batch start
#iterations: 96
currently lose_sum: 95.48948127031326
time_elpased: 1.574
batch start
#iterations: 97
currently lose_sum: 95.38800430297852
time_elpased: 1.565
batch start
#iterations: 98
currently lose_sum: 95.80293935537338
time_elpased: 1.574
batch start
#iterations: 99
currently lose_sum: 95.4006182551384
time_elpased: 1.58
start validation test
0.601082474227
0.622249690977
0.518061129978
0.565395630932
0.601228231002
64.740
batch start
#iterations: 100
currently lose_sum: 95.31623548269272
time_elpased: 1.579
batch start
#iterations: 101
currently lose_sum: 95.24395751953125
time_elpased: 1.539
batch start
#iterations: 102
currently lose_sum: 95.16489547491074
time_elpased: 1.58
batch start
#iterations: 103
currently lose_sum: 95.12162202596664
time_elpased: 1.574
batch start
#iterations: 104
currently lose_sum: 94.8006888628006
time_elpased: 1.571
batch start
#iterations: 105
currently lose_sum: 94.68657350540161
time_elpased: 1.572
batch start
#iterations: 106
currently lose_sum: 95.50027233362198
time_elpased: 1.564
batch start
#iterations: 107
currently lose_sum: 94.95352470874786
time_elpased: 1.615
batch start
#iterations: 108
currently lose_sum: 94.81683081388474
time_elpased: 1.595
batch start
#iterations: 109
currently lose_sum: 94.68723237514496
time_elpased: 1.579
batch start
#iterations: 110
currently lose_sum: 95.16819858551025
time_elpased: 1.575
batch start
#iterations: 111
currently lose_sum: 95.05167144536972
time_elpased: 1.615
batch start
#iterations: 112
currently lose_sum: 95.08361059427261
time_elpased: 1.568
batch start
#iterations: 113
currently lose_sum: 94.45827841758728
time_elpased: 1.64
batch start
#iterations: 114
currently lose_sum: 94.63366150856018
time_elpased: 1.581
batch start
#iterations: 115
currently lose_sum: 94.3253526687622
time_elpased: 1.573
batch start
#iterations: 116
currently lose_sum: 94.81313931941986
time_elpased: 1.575
batch start
#iterations: 117
currently lose_sum: 94.67708748579025
time_elpased: 1.579
batch start
#iterations: 118
currently lose_sum: 94.596031665802
time_elpased: 1.559
batch start
#iterations: 119
currently lose_sum: 94.4333524107933
time_elpased: 1.554
start validation test
0.595567010309
0.634083416941
0.455284552846
0.530010782317
0.5958132978
66.010
batch start
#iterations: 120
currently lose_sum: 94.32261550426483
time_elpased: 1.565
batch start
#iterations: 121
currently lose_sum: 94.38596713542938
time_elpased: 1.577
batch start
#iterations: 122
currently lose_sum: 94.45678126811981
time_elpased: 1.577
batch start
#iterations: 123
currently lose_sum: 94.54076904058456
time_elpased: 1.547
batch start
#iterations: 124
currently lose_sum: 94.58249825239182
time_elpased: 1.537
batch start
#iterations: 125
currently lose_sum: 94.74384850263596
time_elpased: 1.579
batch start
#iterations: 126
currently lose_sum: 94.28835624456406
time_elpased: 1.601
batch start
#iterations: 127
currently lose_sum: 94.65454226732254
time_elpased: 1.542
batch start
#iterations: 128
currently lose_sum: 94.45011228322983
time_elpased: 1.597
batch start
#iterations: 129
currently lose_sum: 94.68726581335068
time_elpased: 1.584
batch start
#iterations: 130
currently lose_sum: 94.38075196743011
time_elpased: 1.588
batch start
#iterations: 131
currently lose_sum: 94.30286490917206
time_elpased: 1.554
batch start
#iterations: 132
currently lose_sum: 93.9666520357132
time_elpased: 1.572
batch start
#iterations: 133
currently lose_sum: 94.2399109005928
time_elpased: 1.563
batch start
#iterations: 134
currently lose_sum: 93.88866478204727
time_elpased: 1.588
batch start
#iterations: 135
currently lose_sum: 94.2902010679245
time_elpased: 1.572
batch start
#iterations: 136
currently lose_sum: 93.80952775478363
time_elpased: 1.583
batch start
#iterations: 137
currently lose_sum: 93.98680460453033
time_elpased: 1.567
batch start
#iterations: 138
currently lose_sum: 93.74071300029755
time_elpased: 1.588
batch start
#iterations: 139
currently lose_sum: 93.75044685602188
time_elpased: 1.542
start validation test
0.596340206186
0.610576923077
0.535864978903
0.570786516854
0.596446379775
66.362
batch start
#iterations: 140
currently lose_sum: 93.88642692565918
time_elpased: 1.564
batch start
#iterations: 141
currently lose_sum: 94.3378598690033
time_elpased: 1.582
batch start
#iterations: 142
currently lose_sum: 93.60861337184906
time_elpased: 1.551
batch start
#iterations: 143
currently lose_sum: 93.79871618747711
time_elpased: 1.541
batch start
#iterations: 144
currently lose_sum: 93.85323566198349
time_elpased: 1.584
batch start
#iterations: 145
currently lose_sum: 93.80642092227936
time_elpased: 1.536
batch start
#iterations: 146
currently lose_sum: 94.05740267038345
time_elpased: 1.583
batch start
#iterations: 147
currently lose_sum: 94.47020381689072
time_elpased: 1.573
batch start
#iterations: 148
currently lose_sum: 93.72349143028259
time_elpased: 1.602
batch start
#iterations: 149
currently lose_sum: 94.11937671899796
time_elpased: 1.57
batch start
#iterations: 150
currently lose_sum: 93.96836125850677
time_elpased: 1.602
batch start
#iterations: 151
currently lose_sum: 93.75961881875992
time_elpased: 1.564
batch start
#iterations: 152
currently lose_sum: 93.62956029176712
time_elpased: 1.549
batch start
#iterations: 153
currently lose_sum: 93.563447535038
time_elpased: 1.572
batch start
#iterations: 154
currently lose_sum: 93.599578499794
time_elpased: 1.594
batch start
#iterations: 155
currently lose_sum: 93.6063803434372
time_elpased: 1.554
batch start
#iterations: 156
currently lose_sum: 93.21994656324387
time_elpased: 1.551
batch start
#iterations: 157
currently lose_sum: 93.61419403553009
time_elpased: 1.552
batch start
#iterations: 158
currently lose_sum: 93.74404901266098
time_elpased: 1.553
batch start
#iterations: 159
currently lose_sum: 93.70526766777039
time_elpased: 1.557
start validation test
0.591855670103
0.645291552253
0.411135124009
0.502263012321
0.592172952896
66.206
batch start
#iterations: 160
currently lose_sum: 93.45802795886993
time_elpased: 1.545
batch start
#iterations: 161
currently lose_sum: 93.35847628116608
time_elpased: 1.558
batch start
#iterations: 162
currently lose_sum: 93.09768271446228
time_elpased: 1.574
batch start
#iterations: 163
currently lose_sum: 93.72274875640869
time_elpased: 1.577
batch start
#iterations: 164
currently lose_sum: 94.08240348100662
time_elpased: 1.576
batch start
#iterations: 165
currently lose_sum: 93.75394827127457
time_elpased: 1.589
batch start
#iterations: 166
currently lose_sum: 92.97200137376785
time_elpased: 1.553
batch start
#iterations: 167
currently lose_sum: 92.9129289984703
time_elpased: 1.571
batch start
#iterations: 168
currently lose_sum: 93.01684898138046
time_elpased: 1.585
batch start
#iterations: 169
currently lose_sum: 92.94647318124771
time_elpased: 1.547
batch start
#iterations: 170
currently lose_sum: 93.2230229973793
time_elpased: 1.556
batch start
#iterations: 171
currently lose_sum: 92.96403700113297
time_elpased: 1.578
batch start
#iterations: 172
currently lose_sum: 92.97852045297623
time_elpased: 1.575
batch start
#iterations: 173
currently lose_sum: 92.88908869028091
time_elpased: 1.577
batch start
#iterations: 174
currently lose_sum: 92.58651864528656
time_elpased: 1.608
batch start
#iterations: 175
currently lose_sum: 93.08989173173904
time_elpased: 1.571
batch start
#iterations: 176
currently lose_sum: 93.04061311483383
time_elpased: 1.565
batch start
#iterations: 177
currently lose_sum: 93.23999345302582
time_elpased: 1.58
batch start
#iterations: 178
currently lose_sum: 93.01058977842331
time_elpased: 1.582
batch start
#iterations: 179
currently lose_sum: 92.73012614250183
time_elpased: 1.592
start validation test
0.591288659794
0.606479275846
0.524030050427
0.562248109093
0.591406742656
67.454
batch start
#iterations: 180
currently lose_sum: 93.11143362522125
time_elpased: 1.592
batch start
#iterations: 181
currently lose_sum: 92.691046833992
time_elpased: 1.572
batch start
#iterations: 182
currently lose_sum: 93.05662256479263
time_elpased: 1.566
batch start
#iterations: 183
currently lose_sum: 92.68002009391785
time_elpased: 1.576
batch start
#iterations: 184
currently lose_sum: 92.55657678842545
time_elpased: 1.586
batch start
#iterations: 185
currently lose_sum: 92.64364242553711
time_elpased: 1.632
batch start
#iterations: 186
currently lose_sum: 92.37427198886871
time_elpased: 1.559
batch start
#iterations: 187
currently lose_sum: 92.59149467945099
time_elpased: 1.585
batch start
#iterations: 188
currently lose_sum: 92.52226567268372
time_elpased: 1.599
batch start
#iterations: 189
currently lose_sum: 92.93101865053177
time_elpased: 1.607
batch start
#iterations: 190
currently lose_sum: 92.50061225891113
time_elpased: 1.56
batch start
#iterations: 191
currently lose_sum: 92.14239251613617
time_elpased: 1.554
batch start
#iterations: 192
currently lose_sum: 92.3236585855484
time_elpased: 1.693
batch start
#iterations: 193
currently lose_sum: 92.19639903306961
time_elpased: 1.557
batch start
#iterations: 194
currently lose_sum: 92.46454590559006
time_elpased: 1.567
batch start
#iterations: 195
currently lose_sum: 92.18538635969162
time_elpased: 1.572
batch start
#iterations: 196
currently lose_sum: 92.45453345775604
time_elpased: 1.567
batch start
#iterations: 197
currently lose_sum: 92.79208314418793
time_elpased: 1.603
batch start
#iterations: 198
currently lose_sum: 91.98478615283966
time_elpased: 1.557
batch start
#iterations: 199
currently lose_sum: 92.64834851026535
time_elpased: 1.571
start validation test
0.604587628866
0.628067100651
0.516311618812
0.566732561423
0.604742611017
66.946
batch start
#iterations: 200
currently lose_sum: 92.75163400173187
time_elpased: 1.568
batch start
#iterations: 201
currently lose_sum: 92.39468330144882
time_elpased: 1.609
batch start
#iterations: 202
currently lose_sum: 91.97706043720245
time_elpased: 1.59
batch start
#iterations: 203
currently lose_sum: 92.0034709572792
time_elpased: 1.571
batch start
#iterations: 204
currently lose_sum: 92.14982944726944
time_elpased: 1.548
batch start
#iterations: 205
currently lose_sum: 92.00265967845917
time_elpased: 1.591
batch start
#iterations: 206
currently lose_sum: 92.33640646934509
time_elpased: 1.602
batch start
#iterations: 207
currently lose_sum: 91.83469521999359
time_elpased: 1.55
batch start
#iterations: 208
currently lose_sum: 92.07661557197571
time_elpased: 1.58
batch start
#iterations: 209
currently lose_sum: 91.46529912948608
time_elpased: 1.561
batch start
#iterations: 210
currently lose_sum: 92.25127625465393
time_elpased: 1.581
batch start
#iterations: 211
currently lose_sum: 92.05685937404633
time_elpased: 1.585
batch start
#iterations: 212
currently lose_sum: 91.94120436906815
time_elpased: 1.607
batch start
#iterations: 213
currently lose_sum: 91.92014282941818
time_elpased: 1.573
batch start
#iterations: 214
currently lose_sum: 91.62155276536942
time_elpased: 1.577
batch start
#iterations: 215
currently lose_sum: 91.8420073390007
time_elpased: 1.621
batch start
#iterations: 216
currently lose_sum: 92.19237357378006
time_elpased: 1.588
batch start
#iterations: 217
currently lose_sum: 91.31071138381958
time_elpased: 1.57
batch start
#iterations: 218
currently lose_sum: 92.07976549863815
time_elpased: 1.617
batch start
#iterations: 219
currently lose_sum: 92.08263546228409
time_elpased: 1.555
start validation test
0.590309278351
0.606220727753
0.51950190388
0.559521170472
0.590433591618
68.034
batch start
#iterations: 220
currently lose_sum: 91.98582166433334
time_elpased: 1.569
batch start
#iterations: 221
currently lose_sum: 91.8186644911766
time_elpased: 1.603
batch start
#iterations: 222
currently lose_sum: 91.66951233148575
time_elpased: 1.569
batch start
#iterations: 223
currently lose_sum: 91.60850656032562
time_elpased: 1.551
batch start
#iterations: 224
currently lose_sum: 91.65016043186188
time_elpased: 1.602
batch start
#iterations: 225
currently lose_sum: 91.6508184671402
time_elpased: 1.584
batch start
#iterations: 226
currently lose_sum: 91.89493316411972
time_elpased: 1.582
batch start
#iterations: 227
currently lose_sum: 91.64169496297836
time_elpased: 1.654
batch start
#iterations: 228
currently lose_sum: 91.51745736598969
time_elpased: 1.584
batch start
#iterations: 229
currently lose_sum: 92.01880395412445
time_elpased: 1.618
batch start
#iterations: 230
currently lose_sum: 91.69939297437668
time_elpased: 1.613
batch start
#iterations: 231
currently lose_sum: 91.31338560581207
time_elpased: 1.551
batch start
#iterations: 232
currently lose_sum: 91.19887441396713
time_elpased: 1.576
batch start
#iterations: 233
currently lose_sum: 91.2852194905281
time_elpased: 1.593
batch start
#iterations: 234
currently lose_sum: 91.68407332897186
time_elpased: 1.573
batch start
#iterations: 235
currently lose_sum: 90.82946640253067
time_elpased: 1.54
batch start
#iterations: 236
currently lose_sum: 91.4722643494606
time_elpased: 1.565
batch start
#iterations: 237
currently lose_sum: 91.61140751838684
time_elpased: 1.582
batch start
#iterations: 238
currently lose_sum: 91.23870372772217
time_elpased: 1.587
batch start
#iterations: 239
currently lose_sum: 90.95987874269485
time_elpased: 1.595
start validation test
0.591958762887
0.634583769242
0.436966141813
0.517552413457
0.592230876339
70.869
batch start
#iterations: 240
currently lose_sum: 91.20841610431671
time_elpased: 1.562
batch start
#iterations: 241
currently lose_sum: 91.18117767572403
time_elpased: 1.617
batch start
#iterations: 242
currently lose_sum: 91.1615070104599
time_elpased: 1.569
batch start
#iterations: 243
currently lose_sum: 90.90099829435349
time_elpased: 1.54
batch start
#iterations: 244
currently lose_sum: 91.0168467760086
time_elpased: 1.578
batch start
#iterations: 245
currently lose_sum: 91.67103445529938
time_elpased: 1.555
batch start
#iterations: 246
currently lose_sum: 91.12281948328018
time_elpased: 1.555
batch start
#iterations: 247
currently lose_sum: 90.95917350053787
time_elpased: 1.568
batch start
#iterations: 248
currently lose_sum: 90.59505897760391
time_elpased: 1.57
batch start
#iterations: 249
currently lose_sum: 91.00407665967941
time_elpased: 1.583
batch start
#iterations: 250
currently lose_sum: 90.70888543128967
time_elpased: 1.58
batch start
#iterations: 251
currently lose_sum: 90.59282296895981
time_elpased: 1.567
batch start
#iterations: 252
currently lose_sum: 91.14739787578583
time_elpased: 1.574
batch start
#iterations: 253
currently lose_sum: 90.79066997766495
time_elpased: 1.585
batch start
#iterations: 254
currently lose_sum: 91.04351097345352
time_elpased: 1.556
batch start
#iterations: 255
currently lose_sum: 90.61840546131134
time_elpased: 1.564
batch start
#iterations: 256
currently lose_sum: 90.51363044977188
time_elpased: 1.562
batch start
#iterations: 257
currently lose_sum: 90.78061544895172
time_elpased: 1.543
batch start
#iterations: 258
currently lose_sum: 90.67247104644775
time_elpased: 1.612
batch start
#iterations: 259
currently lose_sum: 90.36120492219925
time_elpased: 1.556
start validation test
0.590257731959
0.613712374582
0.490995163116
0.545537705105
0.590432002709
71.600
batch start
#iterations: 260
currently lose_sum: 90.66752845048904
time_elpased: 1.597
batch start
#iterations: 261
currently lose_sum: 90.84558463096619
time_elpased: 1.548
batch start
#iterations: 262
currently lose_sum: 90.59900063276291
time_elpased: 1.561
batch start
#iterations: 263
currently lose_sum: 91.09206742048264
time_elpased: 1.57
batch start
#iterations: 264
currently lose_sum: 90.70167529582977
time_elpased: 1.528
batch start
#iterations: 265
currently lose_sum: 90.3994145989418
time_elpased: 1.564
batch start
#iterations: 266
currently lose_sum: 90.12477052211761
time_elpased: 1.574
batch start
#iterations: 267
currently lose_sum: 90.31986409425735
time_elpased: 1.537
batch start
#iterations: 268
currently lose_sum: 90.23850601911545
time_elpased: 1.548
batch start
#iterations: 269
currently lose_sum: 90.06145644187927
time_elpased: 1.587
batch start
#iterations: 270
currently lose_sum: 90.25729775428772
time_elpased: 1.565
batch start
#iterations: 271
currently lose_sum: 90.69072997570038
time_elpased: 1.561
batch start
#iterations: 272
currently lose_sum: 90.31048136949539
time_elpased: 1.599
batch start
#iterations: 273
currently lose_sum: 90.28367078304291
time_elpased: 1.586
batch start
#iterations: 274
currently lose_sum: 90.28112947940826
time_elpased: 1.577
batch start
#iterations: 275
currently lose_sum: 90.12542569637299
time_elpased: 1.627
batch start
#iterations: 276
currently lose_sum: 90.51492166519165
time_elpased: 1.568
batch start
#iterations: 277
currently lose_sum: 90.11867529153824
time_elpased: 1.605
batch start
#iterations: 278
currently lose_sum: 90.54548162221909
time_elpased: 1.632
batch start
#iterations: 279
currently lose_sum: 90.36199641227722
time_elpased: 1.601
start validation test
0.597628865979
0.627757721621
0.48317381908
0.54605722261
0.597829809468
71.114
batch start
#iterations: 280
currently lose_sum: 90.33939689397812
time_elpased: 1.569
batch start
#iterations: 281
currently lose_sum: 89.7535600066185
time_elpased: 1.553
batch start
#iterations: 282
currently lose_sum: 90.28299736976624
time_elpased: 1.55
batch start
#iterations: 283
currently lose_sum: 90.29382228851318
time_elpased: 1.56
batch start
#iterations: 284
currently lose_sum: 90.1695727109909
time_elpased: 1.605
batch start
#iterations: 285
currently lose_sum: 89.8698268532753
time_elpased: 1.552
batch start
#iterations: 286
currently lose_sum: 90.20817464590073
time_elpased: 1.544
batch start
#iterations: 287
currently lose_sum: 90.32008868455887
time_elpased: 1.6
batch start
#iterations: 288
currently lose_sum: 89.97850733995438
time_elpased: 1.574
batch start
#iterations: 289
currently lose_sum: 89.71807354688644
time_elpased: 1.576
batch start
#iterations: 290
currently lose_sum: 89.82937157154083
time_elpased: 1.584
batch start
#iterations: 291
currently lose_sum: 89.55634254217148
time_elpased: 1.56
batch start
#iterations: 292
currently lose_sum: 89.4692491889
time_elpased: 1.619
batch start
#iterations: 293
currently lose_sum: 89.48929238319397
time_elpased: 1.578
batch start
#iterations: 294
currently lose_sum: 89.957210958004
time_elpased: 1.564
batch start
#iterations: 295
currently lose_sum: 88.98265409469604
time_elpased: 1.575
batch start
#iterations: 296
currently lose_sum: 89.75193846225739
time_elpased: 1.569
batch start
#iterations: 297
currently lose_sum: 89.35319083929062
time_elpased: 1.557
batch start
#iterations: 298
currently lose_sum: 89.44584220647812
time_elpased: 1.562
batch start
#iterations: 299
currently lose_sum: 89.0186094045639
time_elpased: 1.563
start validation test
0.591340206186
0.618148197068
0.481630132757
0.541416011106
0.591532819141
72.272
batch start
#iterations: 300
currently lose_sum: 89.86258506774902
time_elpased: 1.558
batch start
#iterations: 301
currently lose_sum: 89.38952523469925
time_elpased: 1.549
batch start
#iterations: 302
currently lose_sum: 89.5526385307312
time_elpased: 1.592
batch start
#iterations: 303
currently lose_sum: 89.12099796533585
time_elpased: 1.556
batch start
#iterations: 304
currently lose_sum: 89.42032265663147
time_elpased: 1.564
batch start
#iterations: 305
currently lose_sum: 88.97646367549896
time_elpased: 1.586
batch start
#iterations: 306
currently lose_sum: 89.42841732501984
time_elpased: 1.566
batch start
#iterations: 307
currently lose_sum: 89.28417652845383
time_elpased: 1.559
batch start
#iterations: 308
currently lose_sum: 89.17227494716644
time_elpased: 1.554
batch start
#iterations: 309
currently lose_sum: 89.46833044290543
time_elpased: 1.569
batch start
#iterations: 310
currently lose_sum: 89.68027877807617
time_elpased: 1.577
batch start
#iterations: 311
currently lose_sum: 88.65246087312698
time_elpased: 1.592
batch start
#iterations: 312
currently lose_sum: 89.32839208841324
time_elpased: 1.546
batch start
#iterations: 313
currently lose_sum: 88.71426343917847
time_elpased: 1.569
batch start
#iterations: 314
currently lose_sum: 89.02872502803802
time_elpased: 1.548
batch start
#iterations: 315
currently lose_sum: 88.5248710513115
time_elpased: 1.553
batch start
#iterations: 316
currently lose_sum: 89.16140550374985
time_elpased: 1.567
batch start
#iterations: 317
currently lose_sum: 89.29436844587326
time_elpased: 1.555
batch start
#iterations: 318
currently lose_sum: 88.85656350851059
time_elpased: 1.578
batch start
#iterations: 319
currently lose_sum: 88.7927268743515
time_elpased: 1.569
start validation test
0.590463917526
0.620021674343
0.47103015334
0.535352944617
0.59067360192
72.429
batch start
#iterations: 320
currently lose_sum: 88.50112855434418
time_elpased: 1.562
batch start
#iterations: 321
currently lose_sum: 88.73072242736816
time_elpased: 1.572
batch start
#iterations: 322
currently lose_sum: 88.95920586585999
time_elpased: 1.571
batch start
#iterations: 323
currently lose_sum: 88.69848519563675
time_elpased: 1.579
batch start
#iterations: 324
currently lose_sum: 88.9451419711113
time_elpased: 1.556
batch start
#iterations: 325
currently lose_sum: 88.90130454301834
time_elpased: 1.584
batch start
#iterations: 326
currently lose_sum: 88.5306988954544
time_elpased: 1.595
batch start
#iterations: 327
currently lose_sum: 88.60141485929489
time_elpased: 1.544
batch start
#iterations: 328
currently lose_sum: 88.22758269309998
time_elpased: 1.542
batch start
#iterations: 329
currently lose_sum: 88.79128551483154
time_elpased: 1.619
batch start
#iterations: 330
currently lose_sum: 88.79722118377686
time_elpased: 1.56
batch start
#iterations: 331
currently lose_sum: 88.68586373329163
time_elpased: 1.564
batch start
#iterations: 332
currently lose_sum: 88.10364317893982
time_elpased: 1.551
batch start
#iterations: 333
currently lose_sum: 88.43144315481186
time_elpased: 1.59
batch start
#iterations: 334
currently lose_sum: 88.86153239011765
time_elpased: 1.583
batch start
#iterations: 335
currently lose_sum: 88.65476018190384
time_elpased: 1.545
batch start
#iterations: 336
currently lose_sum: 87.82725590467453
time_elpased: 1.555
batch start
#iterations: 337
currently lose_sum: 89.10967743396759
time_elpased: 1.566
batch start
#iterations: 338
currently lose_sum: 88.86916625499725
time_elpased: 1.618
batch start
#iterations: 339
currently lose_sum: 88.41758120059967
time_elpased: 1.561
start validation test
0.58587628866
0.604547148714
0.500771843161
0.54778790949
0.58602570264
76.130
batch start
#iterations: 340
currently lose_sum: 88.22263038158417
time_elpased: 1.545
batch start
#iterations: 341
currently lose_sum: 88.41512668132782
time_elpased: 1.589
batch start
#iterations: 342
currently lose_sum: 88.12175166606903
time_elpased: 1.582
batch start
#iterations: 343
currently lose_sum: 88.48441606760025
time_elpased: 1.556
batch start
#iterations: 344
currently lose_sum: 87.99978923797607
time_elpased: 1.609
batch start
#iterations: 345
currently lose_sum: 88.08786582946777
time_elpased: 1.56
batch start
#iterations: 346
currently lose_sum: 87.67011654376984
time_elpased: 1.566
batch start
#iterations: 347
currently lose_sum: 88.18594986200333
time_elpased: 1.577
batch start
#iterations: 348
currently lose_sum: 87.7501630783081
time_elpased: 1.611
batch start
#iterations: 349
currently lose_sum: 88.04504936933517
time_elpased: 1.585
batch start
#iterations: 350
currently lose_sum: 87.93580085039139
time_elpased: 1.561
batch start
#iterations: 351
currently lose_sum: 88.23401135206223
time_elpased: 1.552
batch start
#iterations: 352
currently lose_sum: 87.73312646150589
time_elpased: 1.546
batch start
#iterations: 353
currently lose_sum: 88.31823682785034
time_elpased: 1.596
batch start
#iterations: 354
currently lose_sum: 88.07800102233887
time_elpased: 1.567
batch start
#iterations: 355
currently lose_sum: 88.1357393860817
time_elpased: 1.545
batch start
#iterations: 356
currently lose_sum: 88.1052805185318
time_elpased: 1.598
batch start
#iterations: 357
currently lose_sum: 88.15673911571503
time_elpased: 1.557
batch start
#iterations: 358
currently lose_sum: 87.60234183073044
time_elpased: 1.546
batch start
#iterations: 359
currently lose_sum: 87.52892637252808
time_elpased: 1.577
start validation test
0.583453608247
0.615929705215
0.447257383966
0.518213795982
0.583692721726
79.062
batch start
#iterations: 360
currently lose_sum: 87.84771448373795
time_elpased: 1.572
batch start
#iterations: 361
currently lose_sum: 87.75080251693726
time_elpased: 1.585
batch start
#iterations: 362
currently lose_sum: 87.54767793416977
time_elpased: 1.599
batch start
#iterations: 363
currently lose_sum: 87.35937231779099
time_elpased: 1.569
batch start
#iterations: 364
currently lose_sum: 87.62531751394272
time_elpased: 1.548
batch start
#iterations: 365
currently lose_sum: 87.75730073451996
time_elpased: 1.57
batch start
#iterations: 366
currently lose_sum: 87.19047790765762
time_elpased: 1.573
batch start
#iterations: 367
currently lose_sum: 88.1067721247673
time_elpased: 1.552
batch start
#iterations: 368
currently lose_sum: 87.08224320411682
time_elpased: 1.588
batch start
#iterations: 369
currently lose_sum: 87.61160236597061
time_elpased: 1.569
batch start
#iterations: 370
currently lose_sum: 87.59607809782028
time_elpased: 1.544
batch start
#iterations: 371
currently lose_sum: 87.3383304476738
time_elpased: 1.588
batch start
#iterations: 372
currently lose_sum: 87.00894540548325
time_elpased: 1.573
batch start
#iterations: 373
currently lose_sum: 87.24216681718826
time_elpased: 1.579
batch start
#iterations: 374
currently lose_sum: 87.25631880760193
time_elpased: 1.555
batch start
#iterations: 375
currently lose_sum: 87.90450924634933
time_elpased: 1.538
batch start
#iterations: 376
currently lose_sum: 87.35483801364899
time_elpased: 1.591
batch start
#iterations: 377
currently lose_sum: 87.66774559020996
time_elpased: 1.544
batch start
#iterations: 378
currently lose_sum: 86.86713713407516
time_elpased: 1.588
batch start
#iterations: 379
currently lose_sum: 87.20335811376572
time_elpased: 1.591
start validation test
0.584690721649
0.603957915832
0.496243696614
0.544827975821
0.584846004044
77.591
batch start
#iterations: 380
currently lose_sum: 87.17939466238022
time_elpased: 1.55
batch start
#iterations: 381
currently lose_sum: 86.90006130933762
time_elpased: 1.573
batch start
#iterations: 382
currently lose_sum: 86.7329973578453
time_elpased: 1.573
batch start
#iterations: 383
currently lose_sum: 86.84262311458588
time_elpased: 1.573
batch start
#iterations: 384
currently lose_sum: 87.15873569250107
time_elpased: 1.581
batch start
#iterations: 385
currently lose_sum: 87.18945097923279
time_elpased: 1.567
batch start
#iterations: 386
currently lose_sum: 87.02475613355637
time_elpased: 1.586
batch start
#iterations: 387
currently lose_sum: 86.64125829935074
time_elpased: 1.586
batch start
#iterations: 388
currently lose_sum: 87.1499617099762
time_elpased: 1.548
batch start
#iterations: 389
currently lose_sum: 86.38372248411179
time_elpased: 1.539
batch start
#iterations: 390
currently lose_sum: 85.94997572898865
time_elpased: 1.574
batch start
#iterations: 391
currently lose_sum: 86.94127923250198
time_elpased: 1.601
batch start
#iterations: 392
currently lose_sum: 86.28916484117508
time_elpased: 1.538
batch start
#iterations: 393
currently lose_sum: 86.62814509868622
time_elpased: 1.539
batch start
#iterations: 394
currently lose_sum: 87.02385705709457
time_elpased: 1.579
batch start
#iterations: 395
currently lose_sum: 86.53002965450287
time_elpased: 1.566
batch start
#iterations: 396
currently lose_sum: 86.74407494068146
time_elpased: 1.554
batch start
#iterations: 397
currently lose_sum: 86.39717358350754
time_elpased: 1.537
batch start
#iterations: 398
currently lose_sum: 86.40416997671127
time_elpased: 1.62
batch start
#iterations: 399
currently lose_sum: 86.14536267518997
time_elpased: 1.507
start validation test
0.580927835052
0.624042519931
0.410826386745
0.495469777833
0.58122647438
79.937
acc: 0.612
pre: 0.608
rec: 0.638
F1: 0.622
auc: 0.648
