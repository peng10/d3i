start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.51772701740265
time_elpased: 1.495
batch start
#iterations: 1
currently lose_sum: 100.07343798875809
time_elpased: 1.412
batch start
#iterations: 2
currently lose_sum: 99.92599731683731
time_elpased: 1.398
batch start
#iterations: 3
currently lose_sum: 99.5670657157898
time_elpased: 1.425
batch start
#iterations: 4
currently lose_sum: 99.40263503789902
time_elpased: 1.437
batch start
#iterations: 5
currently lose_sum: 99.34476661682129
time_elpased: 1.402
batch start
#iterations: 6
currently lose_sum: 99.30443489551544
time_elpased: 1.425
batch start
#iterations: 7
currently lose_sum: 99.10205900669098
time_elpased: 1.477
batch start
#iterations: 8
currently lose_sum: 99.32592177391052
time_elpased: 1.41
batch start
#iterations: 9
currently lose_sum: 99.06089788675308
time_elpased: 1.397
batch start
#iterations: 10
currently lose_sum: 98.93249380588531
time_elpased: 1.414
batch start
#iterations: 11
currently lose_sum: 98.8854108452797
time_elpased: 1.444
batch start
#iterations: 12
currently lose_sum: 98.99139434099197
time_elpased: 1.42
batch start
#iterations: 13
currently lose_sum: 98.71806818246841
time_elpased: 1.421
batch start
#iterations: 14
currently lose_sum: 98.84669864177704
time_elpased: 1.455
batch start
#iterations: 15
currently lose_sum: 98.60454267263412
time_elpased: 1.476
batch start
#iterations: 16
currently lose_sum: 98.53668123483658
time_elpased: 1.43
batch start
#iterations: 17
currently lose_sum: 98.63623803853989
time_elpased: 1.407
batch start
#iterations: 18
currently lose_sum: 98.48507356643677
time_elpased: 1.429
batch start
#iterations: 19
currently lose_sum: 98.39295208454132
time_elpased: 1.406
start validation test
0.589536082474
0.624680125107
0.4521971802
0.524625395499
0.5897772021
65.170
batch start
#iterations: 20
currently lose_sum: 98.447703063488
time_elpased: 1.461
batch start
#iterations: 21
currently lose_sum: 98.34472471475601
time_elpased: 1.438
batch start
#iterations: 22
currently lose_sum: 98.12175226211548
time_elpased: 1.41
batch start
#iterations: 23
currently lose_sum: 98.33694422245026
time_elpased: 1.432
batch start
#iterations: 24
currently lose_sum: 98.04286259412766
time_elpased: 1.456
batch start
#iterations: 25
currently lose_sum: 98.33357381820679
time_elpased: 1.438
batch start
#iterations: 26
currently lose_sum: 98.30885046720505
time_elpased: 1.405
batch start
#iterations: 27
currently lose_sum: 98.09251517057419
time_elpased: 1.45
batch start
#iterations: 28
currently lose_sum: 98.0260562300682
time_elpased: 1.436
batch start
#iterations: 29
currently lose_sum: 98.18249046802521
time_elpased: 1.444
batch start
#iterations: 30
currently lose_sum: 97.98556345701218
time_elpased: 1.408
batch start
#iterations: 31
currently lose_sum: 97.7820640206337
time_elpased: 1.403
batch start
#iterations: 32
currently lose_sum: 97.97105318307877
time_elpased: 1.407
batch start
#iterations: 33
currently lose_sum: 97.97818166017532
time_elpased: 1.47
batch start
#iterations: 34
currently lose_sum: 97.6691472530365
time_elpased: 1.421
batch start
#iterations: 35
currently lose_sum: 98.0062426328659
time_elpased: 1.466
batch start
#iterations: 36
currently lose_sum: 97.6174955368042
time_elpased: 1.411
batch start
#iterations: 37
currently lose_sum: 97.5295342206955
time_elpased: 1.457
batch start
#iterations: 38
currently lose_sum: 97.61968529224396
time_elpased: 1.416
batch start
#iterations: 39
currently lose_sum: 97.73801779747009
time_elpased: 1.425
start validation test
0.598659793814
0.628101366591
0.48718740352
0.548742320621
0.598855500789
64.774
batch start
#iterations: 40
currently lose_sum: 97.52737534046173
time_elpased: 1.439
batch start
#iterations: 41
currently lose_sum: 97.57827138900757
time_elpased: 1.415
batch start
#iterations: 42
currently lose_sum: 97.64556020498276
time_elpased: 1.401
batch start
#iterations: 43
currently lose_sum: 97.25230902433395
time_elpased: 1.437
batch start
#iterations: 44
currently lose_sum: 97.76024109125137
time_elpased: 1.464
batch start
#iterations: 45
currently lose_sum: 97.30521446466446
time_elpased: 1.433
batch start
#iterations: 46
currently lose_sum: 97.27599942684174
time_elpased: 1.41
batch start
#iterations: 47
currently lose_sum: 97.17860925197601
time_elpased: 1.441
batch start
#iterations: 48
currently lose_sum: 97.15820556879044
time_elpased: 1.422
batch start
#iterations: 49
currently lose_sum: 97.34372180700302
time_elpased: 1.452
batch start
#iterations: 50
currently lose_sum: 97.0859397649765
time_elpased: 1.421
batch start
#iterations: 51
currently lose_sum: 97.10615885257721
time_elpased: 1.436
batch start
#iterations: 52
currently lose_sum: 96.9670382142067
time_elpased: 1.428
batch start
#iterations: 53
currently lose_sum: 97.0617406964302
time_elpased: 1.444
batch start
#iterations: 54
currently lose_sum: 96.99683403968811
time_elpased: 1.44
batch start
#iterations: 55
currently lose_sum: 97.12964296340942
time_elpased: 1.425
batch start
#iterations: 56
currently lose_sum: 97.06918340921402
time_elpased: 1.397
batch start
#iterations: 57
currently lose_sum: 97.01780831813812
time_elpased: 1.428
batch start
#iterations: 58
currently lose_sum: 96.67689615488052
time_elpased: 1.433
batch start
#iterations: 59
currently lose_sum: 96.73787409067154
time_elpased: 1.445
start validation test
0.585721649485
0.620759056929
0.444375836163
0.517963173994
0.585969803861
65.024
batch start
#iterations: 60
currently lose_sum: 96.90705174207687
time_elpased: 1.422
batch start
#iterations: 61
currently lose_sum: 96.69387263059616
time_elpased: 1.437
batch start
#iterations: 62
currently lose_sum: 96.80992156267166
time_elpased: 1.481
batch start
#iterations: 63
currently lose_sum: 96.79749935865402
time_elpased: 1.437
batch start
#iterations: 64
currently lose_sum: 96.8115423321724
time_elpased: 1.463
batch start
#iterations: 65
currently lose_sum: 96.27481925487518
time_elpased: 1.475
batch start
#iterations: 66
currently lose_sum: 96.5942410826683
time_elpased: 1.525
batch start
#iterations: 67
currently lose_sum: 96.38359147310257
time_elpased: 1.441
batch start
#iterations: 68
currently lose_sum: 96.7787675857544
time_elpased: 1.469
batch start
#iterations: 69
currently lose_sum: 96.54084378480911
time_elpased: 1.485
batch start
#iterations: 70
currently lose_sum: 96.52290201187134
time_elpased: 1.417
batch start
#iterations: 71
currently lose_sum: 96.68197774887085
time_elpased: 1.427
batch start
#iterations: 72
currently lose_sum: 96.25229984521866
time_elpased: 1.442
batch start
#iterations: 73
currently lose_sum: 96.28151869773865
time_elpased: 1.427
batch start
#iterations: 74
currently lose_sum: 96.34866142272949
time_elpased: 1.41
batch start
#iterations: 75
currently lose_sum: 96.59959852695465
time_elpased: 1.434
batch start
#iterations: 76
currently lose_sum: 96.09446150064468
time_elpased: 1.423
batch start
#iterations: 77
currently lose_sum: 96.43094140291214
time_elpased: 1.443
batch start
#iterations: 78
currently lose_sum: 96.37235909700394
time_elpased: 1.431
batch start
#iterations: 79
currently lose_sum: 96.03358018398285
time_elpased: 1.467
start validation test
0.593762886598
0.619127952245
0.490995163116
0.547666877116
0.593943311187
64.778
batch start
#iterations: 80
currently lose_sum: 96.11671358346939
time_elpased: 1.43
batch start
#iterations: 81
currently lose_sum: 95.99783301353455
time_elpased: 1.477
batch start
#iterations: 82
currently lose_sum: 96.06698513031006
time_elpased: 1.431
batch start
#iterations: 83
currently lose_sum: 95.76043897867203
time_elpased: 1.439
batch start
#iterations: 84
currently lose_sum: 95.99124503135681
time_elpased: 1.441
batch start
#iterations: 85
currently lose_sum: 96.1217211484909
time_elpased: 1.454
batch start
#iterations: 86
currently lose_sum: 95.64883661270142
time_elpased: 1.39
batch start
#iterations: 87
currently lose_sum: 95.83497071266174
time_elpased: 1.423
batch start
#iterations: 88
currently lose_sum: 95.85636132955551
time_elpased: 1.435
batch start
#iterations: 89
currently lose_sum: 95.58001518249512
time_elpased: 1.487
batch start
#iterations: 90
currently lose_sum: 95.89280188083649
time_elpased: 1.422
batch start
#iterations: 91
currently lose_sum: 95.47918605804443
time_elpased: 1.44
batch start
#iterations: 92
currently lose_sum: 95.4440485239029
time_elpased: 1.43
batch start
#iterations: 93
currently lose_sum: 95.676802277565
time_elpased: 1.506
batch start
#iterations: 94
currently lose_sum: 95.72450000047684
time_elpased: 1.452
batch start
#iterations: 95
currently lose_sum: 95.37609499692917
time_elpased: 1.426
batch start
#iterations: 96
currently lose_sum: 95.4270955324173
time_elpased: 1.394
batch start
#iterations: 97
currently lose_sum: 95.40383797883987
time_elpased: 1.422
batch start
#iterations: 98
currently lose_sum: 95.39888936281204
time_elpased: 1.404
batch start
#iterations: 99
currently lose_sum: 95.28541278839111
time_elpased: 1.438
start validation test
0.582628865979
0.615979381443
0.442729237419
0.515178731812
0.582874481355
65.264
batch start
#iterations: 100
currently lose_sum: 95.76060891151428
time_elpased: 1.46
batch start
#iterations: 101
currently lose_sum: 95.44400095939636
time_elpased: 1.446
batch start
#iterations: 102
currently lose_sum: 95.79950845241547
time_elpased: 1.45
batch start
#iterations: 103
currently lose_sum: 95.0830043554306
time_elpased: 1.419
batch start
#iterations: 104
currently lose_sum: 95.42340314388275
time_elpased: 1.422
batch start
#iterations: 105
currently lose_sum: 95.05385333299637
time_elpased: 1.409
batch start
#iterations: 106
currently lose_sum: 95.16060698032379
time_elpased: 1.465
batch start
#iterations: 107
currently lose_sum: 95.25256443023682
time_elpased: 1.438
batch start
#iterations: 108
currently lose_sum: 95.04257792234421
time_elpased: 1.468
batch start
#iterations: 109
currently lose_sum: 95.27244728803635
time_elpased: 1.428
batch start
#iterations: 110
currently lose_sum: 95.01525145769119
time_elpased: 1.5
batch start
#iterations: 111
currently lose_sum: 95.008360683918
time_elpased: 1.43
batch start
#iterations: 112
currently lose_sum: 94.8640803694725
time_elpased: 1.44
batch start
#iterations: 113
currently lose_sum: 94.93945777416229
time_elpased: 1.452
batch start
#iterations: 114
currently lose_sum: 94.85317671298981
time_elpased: 1.444
batch start
#iterations: 115
currently lose_sum: 95.11867982149124
time_elpased: 1.474
batch start
#iterations: 116
currently lose_sum: 94.5370061993599
time_elpased: 1.436
batch start
#iterations: 117
currently lose_sum: 94.83402979373932
time_elpased: 1.434
batch start
#iterations: 118
currently lose_sum: 94.7762998342514
time_elpased: 1.448
batch start
#iterations: 119
currently lose_sum: 94.81729155778885
time_elpased: 1.442
start validation test
0.589226804124
0.608088053426
0.506020376659
0.55237881256
0.589372885841
65.213
batch start
#iterations: 120
currently lose_sum: 94.90113192796707
time_elpased: 1.431
batch start
#iterations: 121
currently lose_sum: 94.95826250314713
time_elpased: 1.43
batch start
#iterations: 122
currently lose_sum: 94.70336270332336
time_elpased: 1.41
batch start
#iterations: 123
currently lose_sum: 94.78182953596115
time_elpased: 1.475
batch start
#iterations: 124
currently lose_sum: 94.76922804117203
time_elpased: 1.418
batch start
#iterations: 125
currently lose_sum: 94.4765955209732
time_elpased: 1.447
batch start
#iterations: 126
currently lose_sum: 94.56336694955826
time_elpased: 1.437
batch start
#iterations: 127
currently lose_sum: 94.61027359962463
time_elpased: 1.413
batch start
#iterations: 128
currently lose_sum: 94.29459410905838
time_elpased: 1.456
batch start
#iterations: 129
currently lose_sum: 95.06496685743332
time_elpased: 1.437
batch start
#iterations: 130
currently lose_sum: 94.31042224168777
time_elpased: 1.422
batch start
#iterations: 131
currently lose_sum: 94.88341838121414
time_elpased: 1.425
batch start
#iterations: 132
currently lose_sum: 94.2543157339096
time_elpased: 1.41
batch start
#iterations: 133
currently lose_sum: 94.3618586063385
time_elpased: 1.405
batch start
#iterations: 134
currently lose_sum: 94.34264487028122
time_elpased: 1.422
batch start
#iterations: 135
currently lose_sum: 94.39982122182846
time_elpased: 1.426
batch start
#iterations: 136
currently lose_sum: 94.42426496744156
time_elpased: 1.415
batch start
#iterations: 137
currently lose_sum: 94.15428984165192
time_elpased: 1.452
batch start
#iterations: 138
currently lose_sum: 94.05702847242355
time_elpased: 1.451
batch start
#iterations: 139
currently lose_sum: 94.12258672714233
time_elpased: 1.483
start validation test
0.583350515464
0.599367550474
0.507152413296
0.549417470316
0.583484292985
65.627
batch start
#iterations: 140
currently lose_sum: 94.31315022706985
time_elpased: 1.421
batch start
#iterations: 141
currently lose_sum: 94.63648641109467
time_elpased: 1.432
batch start
#iterations: 142
currently lose_sum: 94.09724253416061
time_elpased: 1.431
batch start
#iterations: 143
currently lose_sum: 94.17304968833923
time_elpased: 1.411
batch start
#iterations: 144
currently lose_sum: 94.45073789358139
time_elpased: 1.415
batch start
#iterations: 145
currently lose_sum: 94.19088840484619
time_elpased: 1.43
batch start
#iterations: 146
currently lose_sum: 93.94782626628876
time_elpased: 1.482
batch start
#iterations: 147
currently lose_sum: 94.09667962789536
time_elpased: 1.424
batch start
#iterations: 148
currently lose_sum: 94.21885633468628
time_elpased: 1.431
batch start
#iterations: 149
currently lose_sum: 94.27092570066452
time_elpased: 1.439
batch start
#iterations: 150
currently lose_sum: 93.90989369153976
time_elpased: 1.448
batch start
#iterations: 151
currently lose_sum: 93.70708858966827
time_elpased: 1.443
batch start
#iterations: 152
currently lose_sum: 93.92117780447006
time_elpased: 1.409
batch start
#iterations: 153
currently lose_sum: 93.90674698352814
time_elpased: 1.44
batch start
#iterations: 154
currently lose_sum: 93.66677850484848
time_elpased: 1.445
batch start
#iterations: 155
currently lose_sum: 93.4705468416214
time_elpased: 1.44
batch start
#iterations: 156
currently lose_sum: 93.83212125301361
time_elpased: 1.447
batch start
#iterations: 157
currently lose_sum: 93.648322224617
time_elpased: 1.427
batch start
#iterations: 158
currently lose_sum: 93.48805195093155
time_elpased: 1.411
batch start
#iterations: 159
currently lose_sum: 93.40839159488678
time_elpased: 1.425
start validation test
0.571597938144
0.621584226911
0.369867242976
0.463771856249
0.571952107495
67.120
batch start
#iterations: 160
currently lose_sum: 93.26621961593628
time_elpased: 1.438
batch start
#iterations: 161
currently lose_sum: 93.85607838630676
time_elpased: 1.424
batch start
#iterations: 162
currently lose_sum: 93.63955295085907
time_elpased: 1.412
batch start
#iterations: 163
currently lose_sum: 93.80024015903473
time_elpased: 1.424
batch start
#iterations: 164
currently lose_sum: 93.4141743183136
time_elpased: 1.467
batch start
#iterations: 165
currently lose_sum: 93.21399784088135
time_elpased: 1.452
batch start
#iterations: 166
currently lose_sum: 93.22596657276154
time_elpased: 1.441
batch start
#iterations: 167
currently lose_sum: 93.23739862442017
time_elpased: 1.429
batch start
#iterations: 168
currently lose_sum: 92.88876485824585
time_elpased: 1.512
batch start
#iterations: 169
currently lose_sum: 93.65315359830856
time_elpased: 1.456
batch start
#iterations: 170
currently lose_sum: 93.33479636907578
time_elpased: 1.434
batch start
#iterations: 171
currently lose_sum: 93.71653658151627
time_elpased: 1.449
batch start
#iterations: 172
currently lose_sum: 93.31359535455704
time_elpased: 1.45
batch start
#iterations: 173
currently lose_sum: 93.1683646440506
time_elpased: 1.45
batch start
#iterations: 174
currently lose_sum: 93.00698435306549
time_elpased: 1.449
batch start
#iterations: 175
currently lose_sum: 92.89361798763275
time_elpased: 1.427
batch start
#iterations: 176
currently lose_sum: 93.05489790439606
time_elpased: 1.44
batch start
#iterations: 177
currently lose_sum: 93.07867455482483
time_elpased: 1.501
batch start
#iterations: 178
currently lose_sum: 93.34979951381683
time_elpased: 1.417
batch start
#iterations: 179
currently lose_sum: 92.80599808692932
time_elpased: 1.426
start validation test
0.577680412371
0.619398307741
0.406812802305
0.491086402882
0.577980396815
66.954
batch start
#iterations: 180
currently lose_sum: 93.25928258895874
time_elpased: 1.418
batch start
#iterations: 181
currently lose_sum: 92.85057371854782
time_elpased: 1.445
batch start
#iterations: 182
currently lose_sum: 92.87891906499863
time_elpased: 1.423
batch start
#iterations: 183
currently lose_sum: 92.70843851566315
time_elpased: 1.451
batch start
#iterations: 184
currently lose_sum: 92.89486634731293
time_elpased: 1.423
batch start
#iterations: 185
currently lose_sum: 93.05667132139206
time_elpased: 1.444
batch start
#iterations: 186
currently lose_sum: 92.89708465337753
time_elpased: 1.419
batch start
#iterations: 187
currently lose_sum: 92.85991662740707
time_elpased: 1.42
batch start
#iterations: 188
currently lose_sum: 92.49769932031631
time_elpased: 1.409
batch start
#iterations: 189
currently lose_sum: 92.87685054540634
time_elpased: 1.442
batch start
#iterations: 190
currently lose_sum: 92.97386258840561
time_elpased: 1.432
batch start
#iterations: 191
currently lose_sum: 92.67294025421143
time_elpased: 1.446
batch start
#iterations: 192
currently lose_sum: 93.03601264953613
time_elpased: 1.416
batch start
#iterations: 193
currently lose_sum: 92.72274643182755
time_elpased: 1.501
batch start
#iterations: 194
currently lose_sum: 92.49235504865646
time_elpased: 1.418
batch start
#iterations: 195
currently lose_sum: 92.45569962263107
time_elpased: 1.429
batch start
#iterations: 196
currently lose_sum: 92.55338543653488
time_elpased: 1.429
batch start
#iterations: 197
currently lose_sum: 92.81744587421417
time_elpased: 1.445
batch start
#iterations: 198
currently lose_sum: 92.71062606573105
time_elpased: 1.454
batch start
#iterations: 199
currently lose_sum: 92.6164540052414
time_elpased: 1.423
start validation test
0.577525773196
0.596669632643
0.483070906658
0.5338944495
0.577691603283
66.591
batch start
#iterations: 200
currently lose_sum: 92.84887892007828
time_elpased: 1.437
batch start
#iterations: 201
currently lose_sum: 92.70485216379166
time_elpased: 1.461
batch start
#iterations: 202
currently lose_sum: 92.25226676464081
time_elpased: 1.474
batch start
#iterations: 203
currently lose_sum: 92.38804811239243
time_elpased: 1.429
batch start
#iterations: 204
currently lose_sum: 92.03091388940811
time_elpased: 1.43
batch start
#iterations: 205
currently lose_sum: 92.45138412714005
time_elpased: 1.454
batch start
#iterations: 206
currently lose_sum: 92.54568779468536
time_elpased: 1.42
batch start
#iterations: 207
currently lose_sum: 92.49586975574493
time_elpased: 1.453
batch start
#iterations: 208
currently lose_sum: 92.07556039094925
time_elpased: 1.474
batch start
#iterations: 209
currently lose_sum: 92.13102304935455
time_elpased: 1.443
batch start
#iterations: 210
currently lose_sum: 92.23151242733002
time_elpased: 1.442
batch start
#iterations: 211
currently lose_sum: 92.60085725784302
time_elpased: 1.462
batch start
#iterations: 212
currently lose_sum: 92.3457642197609
time_elpased: 1.443
batch start
#iterations: 213
currently lose_sum: 92.4343574643135
time_elpased: 1.462
batch start
#iterations: 214
currently lose_sum: 92.22205144166946
time_elpased: 1.451
batch start
#iterations: 215
currently lose_sum: 92.26891249418259
time_elpased: 1.442
batch start
#iterations: 216
currently lose_sum: 91.82930099964142
time_elpased: 1.436
batch start
#iterations: 217
currently lose_sum: 91.61648052930832
time_elpased: 1.435
batch start
#iterations: 218
currently lose_sum: 91.97883576154709
time_elpased: 1.45
batch start
#iterations: 219
currently lose_sum: 91.73686730861664
time_elpased: 1.449
start validation test
0.560824742268
0.597427966791
0.377688587012
0.462799495586
0.561146266035
68.578
batch start
#iterations: 220
currently lose_sum: 91.98635679483414
time_elpased: 1.427
batch start
#iterations: 221
currently lose_sum: 91.7455815076828
time_elpased: 1.421
batch start
#iterations: 222
currently lose_sum: 91.87516820430756
time_elpased: 1.485
batch start
#iterations: 223
currently lose_sum: 91.64254796504974
time_elpased: 1.422
batch start
#iterations: 224
currently lose_sum: 92.17478907108307
time_elpased: 1.439
batch start
#iterations: 225
currently lose_sum: 92.25463527441025
time_elpased: 1.422
batch start
#iterations: 226
currently lose_sum: 91.88474661111832
time_elpased: 1.42
batch start
#iterations: 227
currently lose_sum: 91.81568360328674
time_elpased: 1.416
batch start
#iterations: 228
currently lose_sum: 92.00076639652252
time_elpased: 1.441
batch start
#iterations: 229
currently lose_sum: 91.63704401254654
time_elpased: 1.388
batch start
#iterations: 230
currently lose_sum: 91.8515859246254
time_elpased: 1.42
batch start
#iterations: 231
currently lose_sum: 91.21172696352005
time_elpased: 1.437
batch start
#iterations: 232
currently lose_sum: 91.62556082010269
time_elpased: 1.444
batch start
#iterations: 233
currently lose_sum: 91.9485365152359
time_elpased: 1.44
batch start
#iterations: 234
currently lose_sum: 91.38269191980362
time_elpased: 1.44
batch start
#iterations: 235
currently lose_sum: 91.73240619897842
time_elpased: 1.439
batch start
#iterations: 236
currently lose_sum: 91.60379952192307
time_elpased: 1.46
batch start
#iterations: 237
currently lose_sum: 91.76657497882843
time_elpased: 1.478
batch start
#iterations: 238
currently lose_sum: 92.12130844593048
time_elpased: 1.404
batch start
#iterations: 239
currently lose_sum: 91.25771164894104
time_elpased: 1.439
start validation test
0.570721649485
0.594786406442
0.448492333025
0.511382304623
0.570936241902
67.423
batch start
#iterations: 240
currently lose_sum: 91.19868266582489
time_elpased: 1.431
batch start
#iterations: 241
currently lose_sum: 91.31715840101242
time_elpased: 1.425
batch start
#iterations: 242
currently lose_sum: 91.69816547632217
time_elpased: 1.433
batch start
#iterations: 243
currently lose_sum: 91.4801087975502
time_elpased: 1.448
batch start
#iterations: 244
currently lose_sum: 91.79443508386612
time_elpased: 1.418
batch start
#iterations: 245
currently lose_sum: 91.45551043748856
time_elpased: 1.421
batch start
#iterations: 246
currently lose_sum: 91.36680048704147
time_elpased: 1.441
batch start
#iterations: 247
currently lose_sum: 91.14907693862915
time_elpased: 1.436
batch start
#iterations: 248
currently lose_sum: 91.14063566923141
time_elpased: 1.465
batch start
#iterations: 249
currently lose_sum: 91.52757000923157
time_elpased: 1.39
batch start
#iterations: 250
currently lose_sum: 91.3358706831932
time_elpased: 1.459
batch start
#iterations: 251
currently lose_sum: 91.44435209035873
time_elpased: 1.449
batch start
#iterations: 252
currently lose_sum: 90.82396906614304
time_elpased: 1.429
batch start
#iterations: 253
currently lose_sum: 90.92635303735733
time_elpased: 1.414
batch start
#iterations: 254
currently lose_sum: 91.42936962842941
time_elpased: 1.41
batch start
#iterations: 255
currently lose_sum: 91.2046662569046
time_elpased: 1.41
batch start
#iterations: 256
currently lose_sum: 90.80885738134384
time_elpased: 1.418
batch start
#iterations: 257
currently lose_sum: 90.97540456056595
time_elpased: 1.428
batch start
#iterations: 258
currently lose_sum: 91.35337054729462
time_elpased: 1.407
batch start
#iterations: 259
currently lose_sum: 91.12449204921722
time_elpased: 1.43
start validation test
0.567525773196
0.604669506231
0.394463311722
0.47745391131
0.56782961104
67.968
batch start
#iterations: 260
currently lose_sum: 90.78693187236786
time_elpased: 1.425
batch start
#iterations: 261
currently lose_sum: 90.7618995308876
time_elpased: 1.395
batch start
#iterations: 262
currently lose_sum: 90.93291169404984
time_elpased: 1.467
batch start
#iterations: 263
currently lose_sum: 91.3054010272026
time_elpased: 1.439
batch start
#iterations: 264
currently lose_sum: 91.01409530639648
time_elpased: 1.42
batch start
#iterations: 265
currently lose_sum: 90.91503542661667
time_elpased: 1.414
batch start
#iterations: 266
currently lose_sum: 91.15488356351852
time_elpased: 1.474
batch start
#iterations: 267
currently lose_sum: 91.14214599132538
time_elpased: 1.414
batch start
#iterations: 268
currently lose_sum: 90.83876383304596
time_elpased: 1.451
batch start
#iterations: 269
currently lose_sum: 90.95236599445343
time_elpased: 1.432
batch start
#iterations: 270
currently lose_sum: 90.92724817991257
time_elpased: 1.458
batch start
#iterations: 271
currently lose_sum: 91.4593511223793
time_elpased: 1.457
batch start
#iterations: 272
currently lose_sum: 90.7012568116188
time_elpased: 1.42
batch start
#iterations: 273
currently lose_sum: 90.87962800264359
time_elpased: 1.477
batch start
#iterations: 274
currently lose_sum: 91.17895901203156
time_elpased: 1.442
batch start
#iterations: 275
currently lose_sum: 90.75903671979904
time_elpased: 1.4
batch start
#iterations: 276
currently lose_sum: 90.71632689237595
time_elpased: 1.449
batch start
#iterations: 277
currently lose_sum: 90.96251821517944
time_elpased: 1.407
batch start
#iterations: 278
currently lose_sum: 91.18505918979645
time_elpased: 1.423
batch start
#iterations: 279
currently lose_sum: 90.73510718345642
time_elpased: 1.428
start validation test
0.569381443299
0.600264822716
0.419882679839
0.494126195955
0.569643911437
68.017
batch start
#iterations: 280
currently lose_sum: 90.89857006072998
time_elpased: 1.464
batch start
#iterations: 281
currently lose_sum: 90.44983923435211
time_elpased: 1.397
batch start
#iterations: 282
currently lose_sum: 90.58229613304138
time_elpased: 1.422
batch start
#iterations: 283
currently lose_sum: 90.85172408819199
time_elpased: 1.414
batch start
#iterations: 284
currently lose_sum: 90.49688369035721
time_elpased: 1.436
batch start
#iterations: 285
currently lose_sum: 90.52570080757141
time_elpased: 1.41
batch start
#iterations: 286
currently lose_sum: 90.84572505950928
time_elpased: 1.457
batch start
#iterations: 287
currently lose_sum: 90.39612054824829
time_elpased: 1.425
batch start
#iterations: 288
currently lose_sum: 91.12556964159012
time_elpased: 1.425
batch start
#iterations: 289
currently lose_sum: 90.41403472423553
time_elpased: 1.419
batch start
#iterations: 290
currently lose_sum: 90.84836089611053
time_elpased: 1.44
batch start
#iterations: 291
currently lose_sum: 90.6293665766716
time_elpased: 1.46
batch start
#iterations: 292
currently lose_sum: 90.23344147205353
time_elpased: 1.412
batch start
#iterations: 293
currently lose_sum: 90.38972645998001
time_elpased: 1.404
batch start
#iterations: 294
currently lose_sum: 90.57660800218582
time_elpased: 1.437
batch start
#iterations: 295
currently lose_sum: 90.52585822343826
time_elpased: 1.453
batch start
#iterations: 296
currently lose_sum: 90.80523014068604
time_elpased: 1.445
batch start
#iterations: 297
currently lose_sum: 90.18493556976318
time_elpased: 1.429
batch start
#iterations: 298
currently lose_sum: 90.53561401367188
time_elpased: 1.401
batch start
#iterations: 299
currently lose_sum: 90.63205945491791
time_elpased: 1.415
start validation test
0.573556701031
0.588894360995
0.492230112175
0.536240820674
0.573699482401
67.853
batch start
#iterations: 300
currently lose_sum: 90.51386052370071
time_elpased: 1.398
batch start
#iterations: 301
currently lose_sum: 90.36585223674774
time_elpased: 1.405
batch start
#iterations: 302
currently lose_sum: 90.42029350996017
time_elpased: 1.405
batch start
#iterations: 303
currently lose_sum: 90.24060344696045
time_elpased: 1.441
batch start
#iterations: 304
currently lose_sum: 90.49992501735687
time_elpased: 1.414
batch start
#iterations: 305
currently lose_sum: 90.38395416736603
time_elpased: 1.424
batch start
#iterations: 306
currently lose_sum: 90.15353107452393
time_elpased: 1.427
batch start
#iterations: 307
currently lose_sum: 90.52283179759979
time_elpased: 1.448
batch start
#iterations: 308
currently lose_sum: 89.99140256643295
time_elpased: 1.51
batch start
#iterations: 309
currently lose_sum: 89.82835549116135
time_elpased: 1.431
batch start
#iterations: 310
currently lose_sum: 90.796455681324
time_elpased: 1.407
batch start
#iterations: 311
currently lose_sum: 89.88634806871414
time_elpased: 1.442
batch start
#iterations: 312
currently lose_sum: 90.38490551710129
time_elpased: 1.418
batch start
#iterations: 313
currently lose_sum: 90.11799365282059
time_elpased: 1.421
batch start
#iterations: 314
currently lose_sum: 90.30748480558395
time_elpased: 1.425
batch start
#iterations: 315
currently lose_sum: 89.54958981275558
time_elpased: 1.408
batch start
#iterations: 316
currently lose_sum: 90.1725417971611
time_elpased: 1.414
batch start
#iterations: 317
currently lose_sum: 90.08784478902817
time_elpased: 1.425
batch start
#iterations: 318
currently lose_sum: 89.88061422109604
time_elpased: 1.396
batch start
#iterations: 319
currently lose_sum: 89.92875361442566
time_elpased: 1.488
start validation test
0.572577319588
0.587757112945
0.491098075538
0.535097555506
0.572720368968
67.555
batch start
#iterations: 320
currently lose_sum: 89.85299634933472
time_elpased: 1.425
batch start
#iterations: 321
currently lose_sum: 89.97875744104385
time_elpased: 1.424
batch start
#iterations: 322
currently lose_sum: 89.7808033823967
time_elpased: 1.424
batch start
#iterations: 323
currently lose_sum: 89.69333374500275
time_elpased: 1.441
batch start
#iterations: 324
currently lose_sum: 90.18185889720917
time_elpased: 1.401
batch start
#iterations: 325
currently lose_sum: 90.13718193769455
time_elpased: 1.428
batch start
#iterations: 326
currently lose_sum: 89.62768167257309
time_elpased: 1.426
batch start
#iterations: 327
currently lose_sum: 89.59909576177597
time_elpased: 1.402
batch start
#iterations: 328
currently lose_sum: 90.00245207548141
time_elpased: 1.453
batch start
#iterations: 329
currently lose_sum: 89.62224012613297
time_elpased: 1.435
batch start
#iterations: 330
currently lose_sum: 90.0078074336052
time_elpased: 1.473
batch start
#iterations: 331
currently lose_sum: 90.18713748455048
time_elpased: 1.433
batch start
#iterations: 332
currently lose_sum: 89.37385416030884
time_elpased: 1.415
batch start
#iterations: 333
currently lose_sum: 89.78275537490845
time_elpased: 1.413
batch start
#iterations: 334
currently lose_sum: 89.67892044782639
time_elpased: 1.408
batch start
#iterations: 335
currently lose_sum: 89.89354521036148
time_elpased: 1.471
batch start
#iterations: 336
currently lose_sum: 89.90798562765121
time_elpased: 1.438
batch start
#iterations: 337
currently lose_sum: 90.06658136844635
time_elpased: 1.431
batch start
#iterations: 338
currently lose_sum: 89.6619371175766
time_elpased: 1.482
batch start
#iterations: 339
currently lose_sum: 89.51004213094711
time_elpased: 1.441
start validation test
0.561443298969
0.595724465558
0.387156529793
0.469311377246
0.561749286274
69.018
batch start
#iterations: 340
currently lose_sum: 89.56641465425491
time_elpased: 1.434
batch start
#iterations: 341
currently lose_sum: 89.84575074911118
time_elpased: 1.445
batch start
#iterations: 342
currently lose_sum: 89.32804322242737
time_elpased: 1.423
batch start
#iterations: 343
currently lose_sum: 89.69367748498917
time_elpased: 1.422
batch start
#iterations: 344
currently lose_sum: 89.1315850019455
time_elpased: 1.404
batch start
#iterations: 345
currently lose_sum: 89.53052020072937
time_elpased: 1.437
batch start
#iterations: 346
currently lose_sum: 89.50182098150253
time_elpased: 1.463
batch start
#iterations: 347
currently lose_sum: 89.7137445807457
time_elpased: 1.453
batch start
#iterations: 348
currently lose_sum: 89.11510533094406
time_elpased: 1.433
batch start
#iterations: 349
currently lose_sum: 89.52705591917038
time_elpased: 1.411
batch start
#iterations: 350
currently lose_sum: 89.53829026222229
time_elpased: 1.424
batch start
#iterations: 351
currently lose_sum: 89.14205932617188
time_elpased: 1.477
batch start
#iterations: 352
currently lose_sum: 89.29388892650604
time_elpased: 1.413
batch start
#iterations: 353
currently lose_sum: 89.18759471178055
time_elpased: 1.464
batch start
#iterations: 354
currently lose_sum: 89.62963140010834
time_elpased: 1.419
batch start
#iterations: 355
currently lose_sum: 88.9415225982666
time_elpased: 1.424
batch start
#iterations: 356
currently lose_sum: 89.70924508571625
time_elpased: 1.43
batch start
#iterations: 357
currently lose_sum: 89.68260025978088
time_elpased: 1.436
batch start
#iterations: 358
currently lose_sum: 89.54494869709015
time_elpased: 1.463
batch start
#iterations: 359
currently lose_sum: 89.44317066669464
time_elpased: 1.419
start validation test
0.566082474227
0.594997806055
0.418647730781
0.491482421167
0.566341318659
69.481
batch start
#iterations: 360
currently lose_sum: 89.37747287750244
time_elpased: 1.429
batch start
#iterations: 361
currently lose_sum: 89.07530504465103
time_elpased: 1.42
batch start
#iterations: 362
currently lose_sum: 88.87924993038177
time_elpased: 1.406
batch start
#iterations: 363
currently lose_sum: 89.44863694906235
time_elpased: 1.46
batch start
#iterations: 364
currently lose_sum: 89.07664120197296
time_elpased: 1.417
batch start
#iterations: 365
currently lose_sum: 89.09357380867004
time_elpased: 1.396
batch start
#iterations: 366
currently lose_sum: 89.60816460847855
time_elpased: 1.426
batch start
#iterations: 367
currently lose_sum: 89.47653222084045
time_elpased: 1.443
batch start
#iterations: 368
currently lose_sum: 89.61918818950653
time_elpased: 1.429
batch start
#iterations: 369
currently lose_sum: 89.51289319992065
time_elpased: 1.418
batch start
#iterations: 370
currently lose_sum: 88.74307990074158
time_elpased: 1.408
batch start
#iterations: 371
currently lose_sum: 89.09307253360748
time_elpased: 1.43
batch start
#iterations: 372
currently lose_sum: 88.76679223775864
time_elpased: 1.423
batch start
#iterations: 373
currently lose_sum: 89.3124675154686
time_elpased: 1.42
batch start
#iterations: 374
currently lose_sum: 89.51289629936218
time_elpased: 1.458
batch start
#iterations: 375
currently lose_sum: 88.96334171295166
time_elpased: 1.408
batch start
#iterations: 376
currently lose_sum: 89.5755045413971
time_elpased: 1.413
batch start
#iterations: 377
currently lose_sum: 89.31405419111252
time_elpased: 1.45
batch start
#iterations: 378
currently lose_sum: 88.91100639104843
time_elpased: 1.432
batch start
#iterations: 379
currently lose_sum: 88.82893425226212
time_elpased: 1.423
start validation test
0.57618556701
0.604181184669
0.446125347329
0.513260715131
0.576413907786
68.613
batch start
#iterations: 380
currently lose_sum: 89.80720275640488
time_elpased: 1.441
batch start
#iterations: 381
currently lose_sum: 88.85556465387344
time_elpased: 1.402
batch start
#iterations: 382
currently lose_sum: 88.7448034286499
time_elpased: 1.423
batch start
#iterations: 383
currently lose_sum: 88.7854260802269
time_elpased: 1.417
batch start
#iterations: 384
currently lose_sum: 88.89332163333893
time_elpased: 1.418
batch start
#iterations: 385
currently lose_sum: 88.83904016017914
time_elpased: 1.431
batch start
#iterations: 386
currently lose_sum: 88.52667438983917
time_elpased: 1.408
batch start
#iterations: 387
currently lose_sum: 88.86865103244781
time_elpased: 1.464
batch start
#iterations: 388
currently lose_sum: 89.02879422903061
time_elpased: 1.405
batch start
#iterations: 389
currently lose_sum: 89.33648067712784
time_elpased: 1.448
batch start
#iterations: 390
currently lose_sum: 88.67822527885437
time_elpased: 1.509
batch start
#iterations: 391
currently lose_sum: 88.89768809080124
time_elpased: 1.442
batch start
#iterations: 392
currently lose_sum: 88.83161807060242
time_elpased: 1.405
batch start
#iterations: 393
currently lose_sum: 88.43025851249695
time_elpased: 1.422
batch start
#iterations: 394
currently lose_sum: 89.15042036771774
time_elpased: 1.41
batch start
#iterations: 395
currently lose_sum: 88.62202960252762
time_elpased: 1.432
batch start
#iterations: 396
currently lose_sum: 88.74030834436417
time_elpased: 1.44
batch start
#iterations: 397
currently lose_sum: 88.62975406646729
time_elpased: 1.454
batch start
#iterations: 398
currently lose_sum: 88.56356185674667
time_elpased: 1.451
batch start
#iterations: 399
currently lose_sum: 88.74003684520721
time_elpased: 1.397
start validation test
0.562628865979
0.592243186583
0.407018627148
0.482464165904
0.562902063755
69.524
acc: 0.591
pre: 0.621
rec: 0.474
F1: 0.537
auc: 0.631
