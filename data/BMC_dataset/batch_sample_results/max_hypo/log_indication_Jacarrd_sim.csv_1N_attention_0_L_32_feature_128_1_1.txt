start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 99.85867130756378
time_elpased: 2.812
batch start
#iterations: 1
currently lose_sum: 98.776751101017
time_elpased: 3.195
batch start
#iterations: 2
currently lose_sum: 98.30986243486404
time_elpased: 3.08
batch start
#iterations: 3
currently lose_sum: 97.69619476795197
time_elpased: 2.978
batch start
#iterations: 4
currently lose_sum: 97.2377872467041
time_elpased: 3.306
batch start
#iterations: 5
currently lose_sum: 96.8665160536766
time_elpased: 2.839
batch start
#iterations: 6
currently lose_sum: 96.33872592449188
time_elpased: 2.576
batch start
#iterations: 7
currently lose_sum: 95.9109998345375
time_elpased: 2.98
batch start
#iterations: 8
currently lose_sum: 95.75988829135895
time_elpased: 2.681
batch start
#iterations: 9
currently lose_sum: 95.16910779476166
time_elpased: 2.918
batch start
#iterations: 10
currently lose_sum: 95.22915631532669
time_elpased: 2.615
batch start
#iterations: 11
currently lose_sum: 94.68278980255127
time_elpased: 3.159
batch start
#iterations: 12
currently lose_sum: 94.10806792974472
time_elpased: 3.132
batch start
#iterations: 13
currently lose_sum: 93.59553009271622
time_elpased: 2.74
batch start
#iterations: 14
currently lose_sum: 93.49182218313217
time_elpased: 2.684
batch start
#iterations: 15
currently lose_sum: 93.17854511737823
time_elpased: 2.89
batch start
#iterations: 16
currently lose_sum: 92.77755618095398
time_elpased: 2.92
batch start
#iterations: 17
currently lose_sum: 92.87967163324356
time_elpased: 2.833
batch start
#iterations: 18
currently lose_sum: 92.13608402013779
time_elpased: 3.026
batch start
#iterations: 19
currently lose_sum: 92.08226013183594
time_elpased: 2.476
start validation test
0.650103092784
0.65979268958
0.622311412988
0.640504183879
0.650151885364
60.461
batch start
#iterations: 20
currently lose_sum: 91.73269045352936
time_elpased: 2.715
batch start
#iterations: 21
currently lose_sum: 91.38507902622223
time_elpased: 2.945
batch start
#iterations: 22
currently lose_sum: 91.23527413606644
time_elpased: 2.718
batch start
#iterations: 23
currently lose_sum: 90.277028799057
time_elpased: 2.584
batch start
#iterations: 24
currently lose_sum: 90.20077872276306
time_elpased: 2.627
batch start
#iterations: 25
currently lose_sum: 90.13371962308884
time_elpased: 2.674
batch start
#iterations: 26
currently lose_sum: 89.73447006940842
time_elpased: 2.77
batch start
#iterations: 27
currently lose_sum: 89.69593060016632
time_elpased: 2.489
batch start
#iterations: 28
currently lose_sum: 89.18590873479843
time_elpased: 2.795
batch start
#iterations: 29
currently lose_sum: 88.73097234964371
time_elpased: 2.551
batch start
#iterations: 30
currently lose_sum: 88.41406148672104
time_elpased: 2.715
batch start
#iterations: 31
currently lose_sum: 88.46686041355133
time_elpased: 2.933
batch start
#iterations: 32
currently lose_sum: 87.83701491355896
time_elpased: 2.704
batch start
#iterations: 33
currently lose_sum: 88.16225576400757
time_elpased: 2.645
batch start
#iterations: 34
currently lose_sum: 87.8237851858139
time_elpased: 2.638
batch start
#iterations: 35
currently lose_sum: 87.61067014932632
time_elpased: 2.755
batch start
#iterations: 36
currently lose_sum: 86.35365802049637
time_elpased: 2.619
batch start
#iterations: 37
currently lose_sum: 87.07697546482086
time_elpased: 2.708
batch start
#iterations: 38
currently lose_sum: 86.19622993469238
time_elpased: 2.695
batch start
#iterations: 39
currently lose_sum: 85.95902860164642
time_elpased: 2.595
start validation test
0.633659793814
0.665273556231
0.540598950293
0.59649122807
0.633823176479
60.878
batch start
#iterations: 40
currently lose_sum: 85.95346146821976
time_elpased: 2.676
batch start
#iterations: 41
currently lose_sum: 85.1108711361885
time_elpased: 2.458
batch start
#iterations: 42
currently lose_sum: 85.03167021274567
time_elpased: 2.247
batch start
#iterations: 43
currently lose_sum: 84.87382072210312
time_elpased: 2.634
batch start
#iterations: 44
currently lose_sum: 84.88999617099762
time_elpased: 3.372
batch start
#iterations: 45
currently lose_sum: 84.69401395320892
time_elpased: 3.147
batch start
#iterations: 46
currently lose_sum: 84.1372041106224
time_elpased: 3.317
batch start
#iterations: 47
currently lose_sum: 84.71351504325867
time_elpased: 3.264
batch start
#iterations: 48
currently lose_sum: 84.66908997297287
time_elpased: 2.951
batch start
#iterations: 49
currently lose_sum: 83.96341973543167
time_elpased: 2.932
batch start
#iterations: 50
currently lose_sum: 83.4536908864975
time_elpased: 2.832
batch start
#iterations: 51
currently lose_sum: 82.94975888729095
time_elpased: 3.134
batch start
#iterations: 52
currently lose_sum: 83.34424448013306
time_elpased: 2.99
batch start
#iterations: 53
currently lose_sum: 81.92806994915009
time_elpased: 2.472
batch start
#iterations: 54
currently lose_sum: 82.30894666910172
time_elpased: 2.971
batch start
#iterations: 55
currently lose_sum: 82.24347007274628
time_elpased: 2.909
batch start
#iterations: 56
currently lose_sum: 82.05656504631042
time_elpased: 3.067
batch start
#iterations: 57
currently lose_sum: 81.76379817724228
time_elpased: 3.006
batch start
#iterations: 58
currently lose_sum: 81.10428616404533
time_elpased: 2.791
batch start
#iterations: 59
currently lose_sum: 81.39625692367554
time_elpased: 3.12
start validation test
0.618711340206
0.668751818446
0.47308840177
0.554155867639
0.618967003735
62.830
batch start
#iterations: 60
currently lose_sum: 80.73442605137825
time_elpased: 2.718
batch start
#iterations: 61
currently lose_sum: 81.20436435937881
time_elpased: 2.769
batch start
#iterations: 62
currently lose_sum: 80.56802541017532
time_elpased: 2.841
batch start
#iterations: 63
currently lose_sum: 80.48744374513626
time_elpased: 2.738
batch start
#iterations: 64
currently lose_sum: 79.8612011373043
time_elpased: 2.563
batch start
#iterations: 65
currently lose_sum: 79.17651852965355
time_elpased: 2.816
batch start
#iterations: 66
currently lose_sum: 80.13853865861893
time_elpased: 2.72
batch start
#iterations: 67
currently lose_sum: 79.27399522066116
time_elpased: 2.459
batch start
#iterations: 68
currently lose_sum: 79.58094501495361
time_elpased: 2.77
batch start
#iterations: 69
currently lose_sum: 79.26532852649689
time_elpased: 2.624
batch start
#iterations: 70
currently lose_sum: 79.1345047056675
time_elpased: 2.354
batch start
#iterations: 71
currently lose_sum: 78.56841838359833
time_elpased: 2.686
batch start
#iterations: 72
currently lose_sum: 77.61223077774048
time_elpased: 2.688
batch start
#iterations: 73
currently lose_sum: 78.06246542930603
time_elpased: 2.456
batch start
#iterations: 74
currently lose_sum: 78.43519884347916
time_elpased: 2.806
batch start
#iterations: 75
currently lose_sum: 77.9418796300888
time_elpased: 2.804
batch start
#iterations: 76
currently lose_sum: 77.95636054873466
time_elpased: 2.783
batch start
#iterations: 77
currently lose_sum: 77.50355425477028
time_elpased: 2.581
batch start
#iterations: 78
currently lose_sum: 77.5132252573967
time_elpased: 2.755
batch start
#iterations: 79
currently lose_sum: 77.26733312010765
time_elpased: 2.769
start validation test
0.607525773196
0.668213085906
0.429865184728
0.523171342685
0.607837683761
65.264
batch start
#iterations: 80
currently lose_sum: 76.85713905096054
time_elpased: 2.589
batch start
#iterations: 81
currently lose_sum: 76.99662110209465
time_elpased: 2.643
batch start
#iterations: 82
currently lose_sum: 76.39550253748894
time_elpased: 2.689
batch start
#iterations: 83
currently lose_sum: 76.4946229159832
time_elpased: 2.656
batch start
#iterations: 84
currently lose_sum: 76.46556252241135
time_elpased: 2.458
batch start
#iterations: 85
currently lose_sum: 76.20401141047478
time_elpased: 2.49
batch start
#iterations: 86
currently lose_sum: 75.96810910105705
time_elpased: 2.702
batch start
#iterations: 87
currently lose_sum: 75.28073978424072
time_elpased: 2.697
batch start
#iterations: 88
currently lose_sum: 75.58556607365608
time_elpased: 2.446
batch start
#iterations: 89
currently lose_sum: 75.2922443151474
time_elpased: 2.741
batch start
#iterations: 90
currently lose_sum: 74.54957363009453
time_elpased: 2.737
batch start
#iterations: 91
currently lose_sum: 74.94587936997414
time_elpased: 2.6
batch start
#iterations: 92
currently lose_sum: 74.7056354880333
time_elpased: 2.743
batch start
#iterations: 93
currently lose_sum: 74.62598004937172
time_elpased: 2.545
batch start
#iterations: 94
currently lose_sum: 74.17486318945885
time_elpased: 2.86
batch start
#iterations: 95
currently lose_sum: 74.52345198392868
time_elpased: 2.969
batch start
#iterations: 96
currently lose_sum: 74.0219010412693
time_elpased: 2.736
batch start
#iterations: 97
currently lose_sum: 73.74388313293457
time_elpased: 2.699
batch start
#iterations: 98
currently lose_sum: 73.17054742574692
time_elpased: 2.646
batch start
#iterations: 99
currently lose_sum: 73.42734211683273
time_elpased: 2.46
start validation test
0.594793814433
0.664131588256
0.386436142842
0.48858239542
0.595159618462
67.998
batch start
#iterations: 100
currently lose_sum: 73.36889207363129
time_elpased: 2.816
batch start
#iterations: 101
currently lose_sum: 73.40184473991394
time_elpased: 2.973
batch start
#iterations: 102
currently lose_sum: 72.76384112238884
time_elpased: 2.701
batch start
#iterations: 103
currently lose_sum: 72.93584954738617
time_elpased: 2.852
batch start
#iterations: 104
currently lose_sum: 72.24714329838753
time_elpased: 3.048
batch start
#iterations: 105
currently lose_sum: 72.50430753827095
time_elpased: 2.429
batch start
#iterations: 106
currently lose_sum: 72.22091433405876
time_elpased: 2.847
batch start
#iterations: 107
currently lose_sum: 72.5659947693348
time_elpased: 2.711
batch start
#iterations: 108
currently lose_sum: 72.18915566802025
time_elpased: 2.824
batch start
#iterations: 109
currently lose_sum: 71.51811897754669
time_elpased: 2.586
batch start
#iterations: 110
currently lose_sum: 71.73700001835823
time_elpased: 2.54
batch start
#iterations: 111
currently lose_sum: 71.95863401889801
time_elpased: 2.622
batch start
#iterations: 112
currently lose_sum: 71.18707397580147
time_elpased: 2.716
batch start
#iterations: 113
currently lose_sum: 70.77489370107651
time_elpased: 2.35
batch start
#iterations: 114
currently lose_sum: 70.69494998455048
time_elpased: 2.675
batch start
#iterations: 115
currently lose_sum: 70.48556324839592
time_elpased: 2.974
batch start
#iterations: 116
currently lose_sum: 70.9357136785984
time_elpased: 2.737
batch start
#iterations: 117
currently lose_sum: 70.56637009978294
time_elpased: 2.669
batch start
#iterations: 118
currently lose_sum: 70.60482585430145
time_elpased: 2.855
batch start
#iterations: 119
currently lose_sum: 69.87249010801315
time_elpased: 2.922
start validation test
0.599639175258
0.660309108846
0.413296284862
0.508386606747
0.599966328943
68.144
batch start
#iterations: 120
currently lose_sum: 69.42725563049316
time_elpased: 2.675
batch start
#iterations: 121
currently lose_sum: 70.25507625937462
time_elpased: 2.888
batch start
#iterations: 122
currently lose_sum: 70.04083287715912
time_elpased: 2.742
batch start
#iterations: 123
currently lose_sum: 69.92295134067535
time_elpased: 2.542
batch start
#iterations: 124
currently lose_sum: 68.81320586800575
time_elpased: 2.675
batch start
#iterations: 125
currently lose_sum: 69.94478631019592
time_elpased: 2.44
batch start
#iterations: 126
currently lose_sum: 68.94353318214417
time_elpased: 2.663
batch start
#iterations: 127
currently lose_sum: 68.29124563932419
time_elpased: 2.576
batch start
#iterations: 128
currently lose_sum: 68.89299130439758
time_elpased: 2.737
batch start
#iterations: 129
currently lose_sum: 68.01232647895813
time_elpased: 2.798
batch start
#iterations: 130
currently lose_sum: 68.95569315552711
time_elpased: 2.972
batch start
#iterations: 131
currently lose_sum: 67.95258063077927
time_elpased: 2.637
batch start
#iterations: 132
currently lose_sum: 68.70446327328682
time_elpased: 2.853
batch start
#iterations: 133
currently lose_sum: 67.84539037942886
time_elpased: 2.765
batch start
#iterations: 134
currently lose_sum: 67.83032703399658
time_elpased: 2.778
batch start
#iterations: 135
currently lose_sum: 67.77960574626923
time_elpased: 2.806
batch start
#iterations: 136
currently lose_sum: 67.43638297915459
time_elpased: 2.572
batch start
#iterations: 137
currently lose_sum: 66.52714958786964
time_elpased: 2.504
batch start
#iterations: 138
currently lose_sum: 67.11550599336624
time_elpased: 2.587
batch start
#iterations: 139
currently lose_sum: 67.46733203530312
time_elpased: 2.664
start validation test
0.606804123711
0.642554933806
0.48451168056
0.552452475945
0.607018826958
68.650
batch start
#iterations: 140
currently lose_sum: 67.24724555015564
time_elpased: 2.937
batch start
#iterations: 141
currently lose_sum: 66.68274009227753
time_elpased: 2.809
batch start
#iterations: 142
currently lose_sum: 67.4299291074276
time_elpased: 2.717
batch start
#iterations: 143
currently lose_sum: 66.55068579316139
time_elpased: 2.638
batch start
#iterations: 144
currently lose_sum: 66.15476763248444
time_elpased: 2.6
batch start
#iterations: 145
currently lose_sum: 66.39585137367249
time_elpased: 2.436
batch start
#iterations: 146
currently lose_sum: 65.58672988414764
time_elpased: 2.661
batch start
#iterations: 147
currently lose_sum: 65.66490200161934
time_elpased: 2.565
batch start
#iterations: 148
currently lose_sum: 66.51122060418129
time_elpased: 2.744
batch start
#iterations: 149
currently lose_sum: 65.85874593257904
time_elpased: 3.106
batch start
#iterations: 150
currently lose_sum: 65.78977319598198
time_elpased: 3.146
batch start
#iterations: 151
currently lose_sum: 65.74596855044365
time_elpased: 2.825
batch start
#iterations: 152
currently lose_sum: 65.51006376743317
time_elpased: 2.903
batch start
#iterations: 153
currently lose_sum: 65.40016984939575
time_elpased: 2.845
batch start
#iterations: 154
currently lose_sum: 66.02233517169952
time_elpased: 2.554
batch start
#iterations: 155
currently lose_sum: 65.78249254822731
time_elpased: 2.647
batch start
#iterations: 156
currently lose_sum: 65.13679537177086
time_elpased: 2.899
batch start
#iterations: 157
currently lose_sum: 65.19048342108727
time_elpased: 3.011
batch start
#iterations: 158
currently lose_sum: 64.91116786003113
time_elpased: 2.657
batch start
#iterations: 159
currently lose_sum: 64.32091170549393
time_elpased: 2.912
start validation test
0.588092783505
0.657309515129
0.371102192035
0.474380056568
0.588473743957
73.517
batch start
#iterations: 160
currently lose_sum: 64.65908545255661
time_elpased: 3.044
batch start
#iterations: 161
currently lose_sum: 63.92355594038963
time_elpased: 2.79
batch start
#iterations: 162
currently lose_sum: 64.10925158858299
time_elpased: 2.851
batch start
#iterations: 163
currently lose_sum: 63.35757073760033
time_elpased: 2.801
batch start
#iterations: 164
currently lose_sum: 63.94170606136322
time_elpased: 2.867
batch start
#iterations: 165
currently lose_sum: 63.80937722325325
time_elpased: 2.808
batch start
#iterations: 166
currently lose_sum: 64.12966999411583
time_elpased: 2.884
batch start
#iterations: 167
currently lose_sum: 63.73666462302208
time_elpased: 2.535
batch start
#iterations: 168
currently lose_sum: 62.489554077386856
time_elpased: 2.755
batch start
#iterations: 169
currently lose_sum: 63.49819338321686
time_elpased: 2.638
batch start
#iterations: 170
currently lose_sum: 62.81153082847595
time_elpased: 2.594
batch start
#iterations: 171
currently lose_sum: 62.972484678030014
time_elpased: 2.517
batch start
#iterations: 172
currently lose_sum: 63.14036509394646
time_elpased: 2.633
batch start
#iterations: 173
currently lose_sum: 63.46395501494408
time_elpased: 2.653
batch start
#iterations: 174
currently lose_sum: 62.88249683380127
time_elpased: 2.753
batch start
#iterations: 175
currently lose_sum: 62.28512695431709
time_elpased: 2.847
batch start
#iterations: 176
currently lose_sum: 62.098003566265106
time_elpased: 2.676
batch start
#iterations: 177
currently lose_sum: 62.39670816063881
time_elpased: 2.608
batch start
#iterations: 178
currently lose_sum: 62.08643242716789
time_elpased: 2.663
batch start
#iterations: 179
currently lose_sum: 62.09831315279007
time_elpased: 2.626
start validation test
0.565927835052
0.665813715455
0.267778120819
0.381944954128
0.566451282861
80.384
batch start
#iterations: 180
currently lose_sum: 61.883748561143875
time_elpased: 2.627
batch start
#iterations: 181
currently lose_sum: 61.58380904793739
time_elpased: 2.619
batch start
#iterations: 182
currently lose_sum: 62.014524310827255
time_elpased: 2.81
batch start
#iterations: 183
currently lose_sum: 61.20464986562729
time_elpased: 2.855
batch start
#iterations: 184
currently lose_sum: 62.20197752118111
time_elpased: 2.731
batch start
#iterations: 185
currently lose_sum: 61.19436663389206
time_elpased: 2.495
batch start
#iterations: 186
currently lose_sum: 61.41956156492233
time_elpased: 2.613
batch start
#iterations: 187
currently lose_sum: 60.83326479792595
time_elpased: 2.492
batch start
#iterations: 188
currently lose_sum: 60.86134201288223
time_elpased: 2.489
batch start
#iterations: 189
currently lose_sum: 61.33451056480408
time_elpased: 2.678
batch start
#iterations: 190
currently lose_sum: 61.06963670253754
time_elpased: 2.612
batch start
#iterations: 191
currently lose_sum: 60.51839876174927
time_elpased: 2.643
batch start
#iterations: 192
currently lose_sum: 60.70798006653786
time_elpased: 2.515
batch start
#iterations: 193
currently lose_sum: 61.21557483077049
time_elpased: 2.693
batch start
#iterations: 194
currently lose_sum: 60.29619160294533
time_elpased: 2.87
batch start
#iterations: 195
currently lose_sum: 61.096850991249084
time_elpased: 2.674
batch start
#iterations: 196
currently lose_sum: 60.36401826143265
time_elpased: 2.384
batch start
#iterations: 197
currently lose_sum: 59.64136266708374
time_elpased: 2.021
batch start
#iterations: 198
currently lose_sum: 59.66331347823143
time_elpased: 2.095
batch start
#iterations: 199
currently lose_sum: 59.874292731285095
time_elpased: 2.009
start validation test
0.566958762887
0.655115511551
0.28599361943
0.398166057741
0.567452040532
81.067
batch start
#iterations: 200
currently lose_sum: 59.77081835269928
time_elpased: 2.038
batch start
#iterations: 201
currently lose_sum: 59.98444268107414
time_elpased: 2.059
batch start
#iterations: 202
currently lose_sum: 59.44641616940498
time_elpased: 2.543
batch start
#iterations: 203
currently lose_sum: 59.173687279224396
time_elpased: 2.634
batch start
#iterations: 204
currently lose_sum: 59.7497563958168
time_elpased: 2.876
batch start
#iterations: 205
currently lose_sum: 59.09158504009247
time_elpased: 3.086
batch start
#iterations: 206
currently lose_sum: 58.84233242273331
time_elpased: 2.869
batch start
#iterations: 207
currently lose_sum: 59.14823204278946
time_elpased: 2.568
batch start
#iterations: 208
currently lose_sum: 58.75885343551636
time_elpased: 2.878
batch start
#iterations: 209
currently lose_sum: 58.18378958106041
time_elpased: 2.834
batch start
#iterations: 210
currently lose_sum: 58.4532208442688
time_elpased: 2.922
batch start
#iterations: 211
currently lose_sum: 59.04799008369446
time_elpased: 2.57
batch start
#iterations: 212
currently lose_sum: 58.3441122174263
time_elpased: 2.579
batch start
#iterations: 213
currently lose_sum: 57.75767520070076
time_elpased: 2.786
batch start
#iterations: 214
currently lose_sum: 58.318051129579544
time_elpased: 2.984
batch start
#iterations: 215
currently lose_sum: 58.582460194826126
time_elpased: 2.893
batch start
#iterations: 216
currently lose_sum: 57.69899669289589
time_elpased: 2.888
batch start
#iterations: 217
currently lose_sum: 58.65526270866394
time_elpased: 2.672
batch start
#iterations: 218
currently lose_sum: 57.422621965408325
time_elpased: 2.757
batch start
#iterations: 219
currently lose_sum: 57.51962488889694
time_elpased: 3.162
start validation test
0.575721649485
0.646087298466
0.338170217145
0.443964061339
0.576138707664
78.986
batch start
#iterations: 220
currently lose_sum: 57.75314775109291
time_elpased: 2.634
batch start
#iterations: 221
currently lose_sum: 57.591997504234314
time_elpased: 2.394
batch start
#iterations: 222
currently lose_sum: 57.56521359086037
time_elpased: 2.707
batch start
#iterations: 223
currently lose_sum: 57.62063783407211
time_elpased: 3.148
batch start
#iterations: 224
currently lose_sum: 56.87457713484764
time_elpased: 2.918
batch start
#iterations: 225
currently lose_sum: 56.884083569049835
time_elpased: 2.862
batch start
#iterations: 226
currently lose_sum: 57.11925485730171
time_elpased: 3.078
batch start
#iterations: 227
currently lose_sum: 57.45265531539917
time_elpased: 2.788
batch start
#iterations: 228
currently lose_sum: 56.68520945310593
time_elpased: 2.627
batch start
#iterations: 229
currently lose_sum: 57.21763977408409
time_elpased: 2.586
batch start
#iterations: 230
currently lose_sum: 57.18856796622276
time_elpased: 2.709
batch start
#iterations: 231
currently lose_sum: 57.25322073698044
time_elpased: 2.74
batch start
#iterations: 232
currently lose_sum: 56.95346003770828
time_elpased: 3.008
batch start
#iterations: 233
currently lose_sum: 56.62799370288849
time_elpased: 3.116
batch start
#iterations: 234
currently lose_sum: 56.40825659036636
time_elpased: 2.922
batch start
#iterations: 235
currently lose_sum: 56.7670259475708
time_elpased: 2.798
batch start
#iterations: 236
currently lose_sum: 56.13214036822319
time_elpased: 2.503
batch start
#iterations: 237
currently lose_sum: 56.179989755153656
time_elpased: 2.775
batch start
#iterations: 238
currently lose_sum: 56.407675951719284
time_elpased: 2.764
batch start
#iterations: 239
currently lose_sum: 56.81999760866165
time_elpased: 2.686
start validation test
0.55881443299
0.645843828715
0.263867448801
0.374662088113
0.559332257913
88.097
batch start
#iterations: 240
currently lose_sum: 55.975879460573196
time_elpased: 2.778
batch start
#iterations: 241
currently lose_sum: 55.75223255157471
time_elpased: 2.568
batch start
#iterations: 242
currently lose_sum: 55.442076325416565
time_elpased: 2.588
batch start
#iterations: 243
currently lose_sum: 56.17292633652687
time_elpased: 2.597
batch start
#iterations: 244
currently lose_sum: 54.62838464975357
time_elpased: 2.73
batch start
#iterations: 245
currently lose_sum: 55.57970264554024
time_elpased: 2.92
batch start
#iterations: 246
currently lose_sum: 54.829009652137756
time_elpased: 3.025
batch start
#iterations: 247
currently lose_sum: 55.20742905139923
time_elpased: 2.841
batch start
#iterations: 248
currently lose_sum: 54.71871665120125
time_elpased: 3.07
batch start
#iterations: 249
currently lose_sum: 54.421763211488724
time_elpased: 2.627
batch start
#iterations: 250
currently lose_sum: 55.086914509534836
time_elpased: 2.664
batch start
#iterations: 251
currently lose_sum: 55.17007663846016
time_elpased: 2.689
batch start
#iterations: 252
currently lose_sum: 54.50073382258415
time_elpased: 2.737
batch start
#iterations: 253
currently lose_sum: 55.15311923623085
time_elpased: 2.769
batch start
#iterations: 254
currently lose_sum: 55.157841086387634
time_elpased: 2.601
batch start
#iterations: 255
currently lose_sum: 54.722741693258286
time_elpased: 2.742
batch start
#iterations: 256
currently lose_sum: 54.18150183558464
time_elpased: 2.378
batch start
#iterations: 257
currently lose_sum: 54.462813168764114
time_elpased: 2.592
batch start
#iterations: 258
currently lose_sum: 54.18179768323898
time_elpased: 2.764
batch start
#iterations: 259
currently lose_sum: 54.40739217400551
time_elpased: 2.611
start validation test
0.573453608247
0.650145772595
0.321292580014
0.430057166472
0.573896315826
83.366
batch start
#iterations: 260
currently lose_sum: 54.123828023672104
time_elpased: 2.757
batch start
#iterations: 261
currently lose_sum: 54.40100431442261
time_elpased: 2.817
batch start
#iterations: 262
currently lose_sum: 54.25259104371071
time_elpased: 2.766
batch start
#iterations: 263
currently lose_sum: 54.09926736354828
time_elpased: 2.686
batch start
#iterations: 264
currently lose_sum: 53.49448439478874
time_elpased: 2.842
batch start
#iterations: 265
currently lose_sum: 54.45834365487099
time_elpased: 2.573
batch start
#iterations: 266
currently lose_sum: 53.86985197663307
time_elpased: 2.698
batch start
#iterations: 267
currently lose_sum: 53.60649052262306
time_elpased: 2.715
batch start
#iterations: 268
currently lose_sum: 52.837482154369354
time_elpased: 2.681
batch start
#iterations: 269
currently lose_sum: 53.8791278898716
time_elpased: 2.774
batch start
#iterations: 270
currently lose_sum: 53.217158794403076
time_elpased: 2.671
batch start
#iterations: 271
currently lose_sum: 52.946535766124725
time_elpased: 2.454
batch start
#iterations: 272
currently lose_sum: 53.64612942934036
time_elpased: 2.427
batch start
#iterations: 273
currently lose_sum: 53.35904544591904
time_elpased: 2.585
batch start
#iterations: 274
currently lose_sum: 53.29313367605209
time_elpased: 2.865
batch start
#iterations: 275
currently lose_sum: 54.011013716459274
time_elpased: 2.736
batch start
#iterations: 276
currently lose_sum: 51.936657756567
time_elpased: 2.88
batch start
#iterations: 277
currently lose_sum: 52.9778313934803
time_elpased: 2.688
batch start
#iterations: 278
currently lose_sum: 52.9245867729187
time_elpased: 2.849
batch start
#iterations: 279
currently lose_sum: 52.26919278502464
time_elpased: 2.93
start validation test
0.566134020619
0.660652496293
0.275084902748
0.388432754487
0.566645002236
88.904
batch start
#iterations: 280
currently lose_sum: 52.263896495103836
time_elpased: 2.915
batch start
#iterations: 281
currently lose_sum: 53.081154614686966
time_elpased: 2.713
batch start
#iterations: 282
currently lose_sum: 53.192101299762726
time_elpased: 2.701
batch start
#iterations: 283
currently lose_sum: 52.256391644477844
time_elpased: 2.722
batch start
#iterations: 284
currently lose_sum: 51.96842822432518
time_elpased: 2.747
batch start
#iterations: 285
currently lose_sum: 51.96487909555435
time_elpased: 2.608
batch start
#iterations: 286
currently lose_sum: 52.22534888982773
time_elpased: 2.776
batch start
#iterations: 287
currently lose_sum: 52.24130043387413
time_elpased: 2.494
batch start
#iterations: 288
currently lose_sum: 52.57129865884781
time_elpased: 2.723
batch start
#iterations: 289
currently lose_sum: 51.77494555711746
time_elpased: 3.007
batch start
#iterations: 290
currently lose_sum: 51.61157366633415
time_elpased: 2.919
batch start
#iterations: 291
currently lose_sum: 51.450920075178146
time_elpased: 2.791
batch start
#iterations: 292
currently lose_sum: 51.36364954710007
time_elpased: 2.486
batch start
#iterations: 293
currently lose_sum: 51.31361702084541
time_elpased: 2.748
batch start
#iterations: 294
currently lose_sum: 51.83387130498886
time_elpased: 2.773
batch start
#iterations: 295
currently lose_sum: 51.563259333372116
time_elpased: 2.525
batch start
#iterations: 296
currently lose_sum: 51.879893481731415
time_elpased: 2.449
batch start
#iterations: 297
currently lose_sum: 52.29127150774002
time_elpased: 2.457
batch start
#iterations: 298
currently lose_sum: 50.80163452029228
time_elpased: 2.885
batch start
#iterations: 299
currently lose_sum: 51.126646637916565
time_elpased: 2.748
start validation test
0.574793814433
0.643527571373
0.338684779253
0.443800148338
0.575208340262
84.183
batch start
#iterations: 300
currently lose_sum: 51.072341322898865
time_elpased: 2.647
batch start
#iterations: 301
currently lose_sum: 51.05509144067764
time_elpased: 2.519
batch start
#iterations: 302
currently lose_sum: 50.02158361673355
time_elpased: 2.716
batch start
#iterations: 303
currently lose_sum: 50.28417810797691
time_elpased: 2.701
batch start
#iterations: 304
currently lose_sum: 50.906590431928635
time_elpased: 2.59
batch start
#iterations: 305
currently lose_sum: 50.4099001288414
time_elpased: 2.545
batch start
#iterations: 306
currently lose_sum: 51.09328046441078
time_elpased: 2.609
batch start
#iterations: 307
currently lose_sum: 50.606903463602066
time_elpased: 2.588
batch start
#iterations: 308
currently lose_sum: 50.274220526218414
time_elpased: 2.829
batch start
#iterations: 309
currently lose_sum: 50.15902617573738
time_elpased: 2.958
batch start
#iterations: 310
currently lose_sum: 50.4752576649189
time_elpased: 3.208
batch start
#iterations: 311
currently lose_sum: 49.72004413604736
time_elpased: 2.756
batch start
#iterations: 312
currently lose_sum: 49.98291453719139
time_elpased: 3.05
batch start
#iterations: 313
currently lose_sum: 49.76284271478653
time_elpased: 2.787
batch start
#iterations: 314
currently lose_sum: 49.851280838251114
time_elpased: 2.714
batch start
#iterations: 315
currently lose_sum: 50.00557002425194
time_elpased: 2.182
batch start
#iterations: 316
currently lose_sum: 49.14234933257103
time_elpased: 2.399
batch start
#iterations: 317
currently lose_sum: 49.478639990091324
time_elpased: 2.048
batch start
#iterations: 318
currently lose_sum: 50.159568816423416
time_elpased: 2.109
batch start
#iterations: 319
currently lose_sum: 49.714109778404236
time_elpased: 2.118
start validation test
0.573298969072
0.642221782961
0.334362457549
0.439767190038
0.573718458972
85.259
batch start
#iterations: 320
currently lose_sum: 49.219406336545944
time_elpased: 2.168
batch start
#iterations: 321
currently lose_sum: 49.782236605882645
time_elpased: 2.472
batch start
#iterations: 322
currently lose_sum: 49.685476899147034
time_elpased: 2.82
batch start
#iterations: 323
currently lose_sum: 49.49603769183159
time_elpased: 2.805
batch start
#iterations: 324
currently lose_sum: 48.56110295653343
time_elpased: 2.686
batch start
#iterations: 325
currently lose_sum: 49.36989179253578
time_elpased: 2.799
batch start
#iterations: 326
currently lose_sum: 49.10371744632721
time_elpased: 2.984
batch start
#iterations: 327
currently lose_sum: 49.224687308073044
time_elpased: 2.574
batch start
#iterations: 328
currently lose_sum: 48.42848062515259
time_elpased: 2.704
batch start
#iterations: 329
currently lose_sum: 48.28160473704338
time_elpased: 3.067
batch start
#iterations: 330
currently lose_sum: 48.2916876077652
time_elpased: 2.985
batch start
#iterations: 331
currently lose_sum: 49.102599024772644
time_elpased: 2.853
batch start
#iterations: 332
currently lose_sum: 48.407780200242996
time_elpased: 2.895
batch start
#iterations: 333
currently lose_sum: 48.857411086559296
time_elpased: 2.881
batch start
#iterations: 334
currently lose_sum: 48.820169642567635
time_elpased: 2.743
batch start
#iterations: 335
currently lose_sum: 48.316743075847626
time_elpased: 2.957
batch start
#iterations: 336
currently lose_sum: 48.558473005890846
time_elpased: 2.975
batch start
#iterations: 337
currently lose_sum: 47.696719378232956
time_elpased: 3.101
batch start
#iterations: 338
currently lose_sum: 48.68938007950783
time_elpased: 2.687
batch start
#iterations: 339
currently lose_sum: 48.97543561458588
time_elpased: 2.837
start validation test
0.549536082474
0.65358040201
0.214160749202
0.322610650337
0.5501248856
98.161
batch start
#iterations: 340
currently lose_sum: 48.43638589978218
time_elpased: 2.875
batch start
#iterations: 341
currently lose_sum: 48.33267068862915
time_elpased: 2.951
batch start
#iterations: 342
currently lose_sum: 48.579804480075836
time_elpased: 2.978
batch start
#iterations: 343
currently lose_sum: 47.50502493977547
time_elpased: 2.66
batch start
#iterations: 344
currently lose_sum: 47.868764728307724
time_elpased: 2.604
batch start
#iterations: 345
currently lose_sum: 47.3505274951458
time_elpased: 2.949
batch start
#iterations: 346
currently lose_sum: 48.33930994570255
time_elpased: 3.031
batch start
#iterations: 347
currently lose_sum: 48.321970999240875
time_elpased: 2.918
batch start
#iterations: 348
currently lose_sum: 47.923034995794296
time_elpased: 2.723
batch start
#iterations: 349
currently lose_sum: 48.21149703860283
time_elpased: 2.8
batch start
#iterations: 350
currently lose_sum: 47.9115232527256
time_elpased: 2.778
batch start
#iterations: 351
currently lose_sum: 47.999513149261475
time_elpased: 2.664
batch start
#iterations: 352
currently lose_sum: 47.55400547385216
time_elpased: 2.696
batch start
#iterations: 353
currently lose_sum: 47.44717459380627
time_elpased: 2.729
batch start
#iterations: 354
currently lose_sum: 46.55158869922161
time_elpased: 2.63
batch start
#iterations: 355
currently lose_sum: 46.672566294670105
time_elpased: 3.262
batch start
#iterations: 356
currently lose_sum: 46.670054882764816
time_elpased: 2.762
batch start
#iterations: 357
currently lose_sum: 47.026339411735535
time_elpased: 2.629
batch start
#iterations: 358
currently lose_sum: 47.395426362752914
time_elpased: 2.347
batch start
#iterations: 359
currently lose_sum: 46.84603662788868
time_elpased: 2.723
start validation test
0.545515463918
0.659123055163
0.191828753731
0.297170187326
0.54613641549
102.798
batch start
#iterations: 360
currently lose_sum: 47.081005454063416
time_elpased: 3.304
batch start
#iterations: 361
currently lose_sum: 46.84424805641174
time_elpased: 3.005
batch start
#iterations: 362
currently lose_sum: 46.379155710339546
time_elpased: 2.461
batch start
#iterations: 363
currently lose_sum: 46.805120795965195
time_elpased: 2.761
batch start
#iterations: 364
currently lose_sum: 46.6995866894722
time_elpased: 2.288
batch start
#iterations: 365
currently lose_sum: 45.92170509696007
time_elpased: 2.595
batch start
#iterations: 366
currently lose_sum: 47.417604714632034
time_elpased: 2.469
batch start
#iterations: 367
currently lose_sum: 46.158985301852226
time_elpased: 2.232
batch start
#iterations: 368
currently lose_sum: 45.64552630484104
time_elpased: 2.377
batch start
#iterations: 369
currently lose_sum: 46.91264571249485
time_elpased: 2.864
batch start
#iterations: 370
currently lose_sum: 46.52300176024437
time_elpased: 3.11
batch start
#iterations: 371
currently lose_sum: 46.68413086235523
time_elpased: 2.922
batch start
#iterations: 372
currently lose_sum: 45.9168976098299
time_elpased: 2.5
batch start
#iterations: 373
currently lose_sum: 46.06701138615608
time_elpased: 2.744
batch start
#iterations: 374
currently lose_sum: 46.190110981464386
time_elpased: 2.683
batch start
#iterations: 375
currently lose_sum: 46.09826038777828
time_elpased: 2.713
batch start
#iterations: 376
currently lose_sum: 45.88916702568531
time_elpased: 2.582
batch start
#iterations: 377
currently lose_sum: 46.71550613641739
time_elpased: 2.717
batch start
#iterations: 378
currently lose_sum: 46.24870416522026
time_elpased: 2.635
batch start
#iterations: 379
currently lose_sum: 46.00068202614784
time_elpased: 2.598
start validation test
0.540618556701
0.659405940594
0.171349181846
0.272014376736
0.541266866044
108.057
batch start
#iterations: 380
currently lose_sum: 45.94401316344738
time_elpased: 2.733
batch start
#iterations: 381
currently lose_sum: 45.898665726184845
time_elpased: 2.953
batch start
#iterations: 382
currently lose_sum: 46.188582211732864
time_elpased: 2.633
batch start
#iterations: 383
currently lose_sum: 45.686432763934135
time_elpased: 2.751
batch start
#iterations: 384
currently lose_sum: 45.472778752446175
time_elpased: 2.69
batch start
#iterations: 385
currently lose_sum: 45.678959995508194
time_elpased: 3.027
batch start
#iterations: 386
currently lose_sum: 45.5173964202404
time_elpased: 2.431
batch start
#iterations: 387
currently lose_sum: 45.573351219296455
time_elpased: 2.058
batch start
#iterations: 388
currently lose_sum: 44.947821483016014
time_elpased: 2.255
batch start
#iterations: 389
currently lose_sum: 44.905542105436325
time_elpased: 2.251
batch start
#iterations: 390
currently lose_sum: 44.98188629746437
time_elpased: 2.618
batch start
#iterations: 391
currently lose_sum: 45.33149775862694
time_elpased: 2.453
batch start
#iterations: 392
currently lose_sum: 44.84357210993767
time_elpased: 2.523
batch start
#iterations: 393
currently lose_sum: 44.71489928662777
time_elpased: 2.601
batch start
#iterations: 394
currently lose_sum: 45.93934315443039
time_elpased: 2.709
batch start
#iterations: 395
currently lose_sum: 45.179817736148834
time_elpased: 3.081
batch start
#iterations: 396
currently lose_sum: 44.96764422953129
time_elpased: 2.892
batch start
#iterations: 397
currently lose_sum: 45.2094562202692
time_elpased: 2.705
batch start
#iterations: 398
currently lose_sum: 44.3334059715271
time_elpased: 2.443
batch start
#iterations: 399
currently lose_sum: 44.59360659122467
time_elpased: 2.74
start validation test
0.549896907216
0.645753181415
0.224554903777
0.333231521075
0.550468095284
101.413
acc: 0.653
pre: 0.662
rec: 0.627
F1: 0.644
auc: 0.712
