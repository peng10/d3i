start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 99.86037296056747
time_elpased: 3.969
batch start
#iterations: 1
currently lose_sum: 98.71888250112534
time_elpased: 3.721
batch start
#iterations: 2
currently lose_sum: 98.15784734487534
time_elpased: 3.583
batch start
#iterations: 3
currently lose_sum: 97.27774119377136
time_elpased: 3.682
batch start
#iterations: 4
currently lose_sum: 96.50675743818283
time_elpased: 4.182
batch start
#iterations: 5
currently lose_sum: 95.5906947851181
time_elpased: 4.097
batch start
#iterations: 6
currently lose_sum: 94.57823586463928
time_elpased: 3.477
batch start
#iterations: 7
currently lose_sum: 94.32178020477295
time_elpased: 3.683
batch start
#iterations: 8
currently lose_sum: 93.5544126033783
time_elpased: 4.015
batch start
#iterations: 9
currently lose_sum: 93.37247568368912
time_elpased: 4.078
batch start
#iterations: 10
currently lose_sum: 92.9168107509613
time_elpased: 3.195
batch start
#iterations: 11
currently lose_sum: 92.21245247125626
time_elpased: 3.706
batch start
#iterations: 12
currently lose_sum: 91.58218348026276
time_elpased: 3.926
batch start
#iterations: 13
currently lose_sum: 91.78289711475372
time_elpased: 3.474
batch start
#iterations: 14
currently lose_sum: 90.82978105545044
time_elpased: 3.692
batch start
#iterations: 15
currently lose_sum: 90.59550493955612
time_elpased: 3.921
batch start
#iterations: 16
currently lose_sum: 90.08792054653168
time_elpased: 3.873
batch start
#iterations: 17
currently lose_sum: 89.66040349006653
time_elpased: 3.886
batch start
#iterations: 18
currently lose_sum: 89.50322562456131
time_elpased: 3.309
batch start
#iterations: 19
currently lose_sum: 88.95616102218628
time_elpased: 3.765
start validation test
0.676494845361
0.663374798215
0.718946176804
0.690043461083
0.676420315501
59.190
batch start
#iterations: 20
currently lose_sum: 88.31984210014343
time_elpased: 4.097
batch start
#iterations: 21
currently lose_sum: 87.75459110736847
time_elpased: 3.955
batch start
#iterations: 22
currently lose_sum: 87.51804083585739
time_elpased: 3.766
batch start
#iterations: 23
currently lose_sum: 87.80967772006989
time_elpased: 3.356
batch start
#iterations: 24
currently lose_sum: 86.91245430707932
time_elpased: 3.822
batch start
#iterations: 25
currently lose_sum: 86.60381066799164
time_elpased: 3.897
batch start
#iterations: 26
currently lose_sum: 86.58601313829422
time_elpased: 3.87
batch start
#iterations: 27
currently lose_sum: 87.0873572230339
time_elpased: 3.788
batch start
#iterations: 28
currently lose_sum: 85.49736696481705
time_elpased: 3.638
batch start
#iterations: 29
currently lose_sum: 85.15780675411224
time_elpased: 3.59
batch start
#iterations: 30
currently lose_sum: 85.3711171746254
time_elpased: 3.556
batch start
#iterations: 31
currently lose_sum: 85.05085217952728
time_elpased: 3.807
batch start
#iterations: 32
currently lose_sum: 84.44359350204468
time_elpased: 3.927
batch start
#iterations: 33
currently lose_sum: 83.73243659734726
time_elpased: 4.109
batch start
#iterations: 34
currently lose_sum: 83.11656710505486
time_elpased: 3.606
batch start
#iterations: 35
currently lose_sum: 83.20629423856735
time_elpased: 3.556
batch start
#iterations: 36
currently lose_sum: 82.82387000322342
time_elpased: 3.595
batch start
#iterations: 37
currently lose_sum: 82.66147184371948
time_elpased: 4.008
batch start
#iterations: 38
currently lose_sum: 82.22468227148056
time_elpased: 4.572
batch start
#iterations: 39
currently lose_sum: 81.42442280054092
time_elpased: 4.754
start validation test
0.632886597938
0.653969384122
0.567150355048
0.607473544974
0.633002008052
62.558
batch start
#iterations: 40
currently lose_sum: 81.35880923271179
time_elpased: 4.044
batch start
#iterations: 41
currently lose_sum: 81.6752073764801
time_elpased: 3.817
batch start
#iterations: 42
currently lose_sum: 80.0877660214901
time_elpased: 3.821
batch start
#iterations: 43
currently lose_sum: 80.2745463848114
time_elpased: 4.008
batch start
#iterations: 44
currently lose_sum: 79.90361699461937
time_elpased: 4.127
batch start
#iterations: 45
currently lose_sum: 80.21114647388458
time_elpased: 3.952
batch start
#iterations: 46
currently lose_sum: 79.18478232622147
time_elpased: 3.578
batch start
#iterations: 47
currently lose_sum: 78.92737853527069
time_elpased: 3.857
batch start
#iterations: 48
currently lose_sum: 78.60884115099907
time_elpased: 3.701
batch start
#iterations: 49
currently lose_sum: 78.00081899762154
time_elpased: 3.89
batch start
#iterations: 50
currently lose_sum: 77.59062147140503
time_elpased: 3.782
batch start
#iterations: 51
currently lose_sum: 77.4753886461258
time_elpased: 4.244
batch start
#iterations: 52
currently lose_sum: 78.10030791163445
time_elpased: 3.823
batch start
#iterations: 53
currently lose_sum: 76.99432718753815
time_elpased: 3.993
batch start
#iterations: 54
currently lose_sum: 77.11717620491982
time_elpased: 3.775
batch start
#iterations: 55
currently lose_sum: 76.24366867542267
time_elpased: 3.915
batch start
#iterations: 56
currently lose_sum: 76.83241415023804
time_elpased: 3.55
batch start
#iterations: 57
currently lose_sum: 75.69851759076118
time_elpased: 3.381
batch start
#iterations: 58
currently lose_sum: 76.37909162044525
time_elpased: 3.771
batch start
#iterations: 59
currently lose_sum: 75.39048290252686
time_elpased: 3.778
start validation test
0.618195876289
0.675106124924
0.45826901307
0.545944951879
0.618476652564
71.268
batch start
#iterations: 60
currently lose_sum: 74.40291285514832
time_elpased: 3.597
batch start
#iterations: 61
currently lose_sum: 74.92941644787788
time_elpased: 3.794
batch start
#iterations: 62
currently lose_sum: 73.88378915190697
time_elpased: 3.695
batch start
#iterations: 63
currently lose_sum: 73.74368032813072
time_elpased: 3.827
batch start
#iterations: 64
currently lose_sum: 73.77807751297951
time_elpased: 3.543
batch start
#iterations: 65
currently lose_sum: 74.00437024235725
time_elpased: 3.578
batch start
#iterations: 66
currently lose_sum: 72.86732053756714
time_elpased: 3.462
batch start
#iterations: 67
currently lose_sum: 73.79061901569366
time_elpased: 3.653
batch start
#iterations: 68
currently lose_sum: 72.20897209644318
time_elpased: 3.384
batch start
#iterations: 69
currently lose_sum: 72.01713466644287
time_elpased: 3.596
batch start
#iterations: 70
currently lose_sum: 71.3635902106762
time_elpased: 3.878
batch start
#iterations: 71
currently lose_sum: 71.24116370081902
time_elpased: 3.346
batch start
#iterations: 72
currently lose_sum: 70.1668171286583
time_elpased: 3.732
batch start
#iterations: 73
currently lose_sum: 70.70857614278793
time_elpased: 3.734
batch start
#iterations: 74
currently lose_sum: 70.16070160269737
time_elpased: 3.632
batch start
#iterations: 75
currently lose_sum: 70.9515016078949
time_elpased: 3.367
batch start
#iterations: 76
currently lose_sum: 69.45299530029297
time_elpased: 3.467
batch start
#iterations: 77
currently lose_sum: 70.24877309799194
time_elpased: 3.816
batch start
#iterations: 78
currently lose_sum: 69.75277644395828
time_elpased: 3.634
batch start
#iterations: 79
currently lose_sum: 69.6701110303402
time_elpased: 3.637
start validation test
0.626649484536
0.666666666667
0.509210661727
0.577396580897
0.626855666503
75.938
batch start
#iterations: 80
currently lose_sum: 69.31294390559196
time_elpased: 3.567
batch start
#iterations: 81
currently lose_sum: 68.26612874865532
time_elpased: 3.499
batch start
#iterations: 82
currently lose_sum: 68.96791955828667
time_elpased: 3.695
batch start
#iterations: 83
currently lose_sum: 67.57216775417328
time_elpased: 3.758
batch start
#iterations: 84
currently lose_sum: 67.09158489108086
time_elpased: 3.889
batch start
#iterations: 85
currently lose_sum: 66.80863523483276
time_elpased: 3.306
batch start
#iterations: 86
currently lose_sum: 67.04203155636787
time_elpased: 3.675
batch start
#iterations: 87
currently lose_sum: 67.787667542696
time_elpased: 3.888
batch start
#iterations: 88
currently lose_sum: 66.96161606907845
time_elpased: 3.854
batch start
#iterations: 89
currently lose_sum: 65.79228088259697
time_elpased: 3.616
batch start
#iterations: 90
currently lose_sum: 66.58074063062668
time_elpased: 3.721
batch start
#iterations: 91
currently lose_sum: 66.01456379890442
time_elpased: 3.88
batch start
#iterations: 92
currently lose_sum: 66.12385821342468
time_elpased: 3.292
batch start
#iterations: 93
currently lose_sum: 65.15922427177429
time_elpased: 3.818
batch start
#iterations: 94
currently lose_sum: 63.83753973245621
time_elpased: 3.56
batch start
#iterations: 95
currently lose_sum: 64.93287545442581
time_elpased: 3.768
batch start
#iterations: 96
currently lose_sum: 65.09384071826935
time_elpased: 3.581
batch start
#iterations: 97
currently lose_sum: 63.89814782142639
time_elpased: 3.59
batch start
#iterations: 98
currently lose_sum: 64.71628153324127
time_elpased: 3.34
batch start
#iterations: 99
currently lose_sum: 63.82371497154236
time_elpased: 2.894
start validation test
0.612319587629
0.67345971564
0.438715652979
0.531314264348
0.612624376113
89.488
batch start
#iterations: 100
currently lose_sum: 63.41601046919823
time_elpased: 3.583
batch start
#iterations: 101
currently lose_sum: 62.75161647796631
time_elpased: 3.682
batch start
#iterations: 102
currently lose_sum: 63.24038150906563
time_elpased: 3.177
batch start
#iterations: 103
currently lose_sum: 62.518215864896774
time_elpased: 3.17
batch start
#iterations: 104
currently lose_sum: 62.41163620352745
time_elpased: 3.575
batch start
#iterations: 105
currently lose_sum: 60.806322515010834
time_elpased: 3.553
batch start
#iterations: 106
currently lose_sum: 62.094440668821335
time_elpased: 3.571
batch start
#iterations: 107
currently lose_sum: 62.05537471175194
time_elpased: 3.342
batch start
#iterations: 108
currently lose_sum: 60.62961629033089
time_elpased: 3.286
batch start
#iterations: 109
currently lose_sum: 61.22748216986656
time_elpased: 3.45
batch start
#iterations: 110
currently lose_sum: 61.232094407081604
time_elpased: 3.502
batch start
#iterations: 111
currently lose_sum: 60.67804315686226
time_elpased: 3.436
batch start
#iterations: 112
currently lose_sum: 60.6341909468174
time_elpased: 3.848
batch start
#iterations: 113
currently lose_sum: 60.7225678563118
time_elpased: 3.767
batch start
#iterations: 114
currently lose_sum: 59.59126877784729
time_elpased: 3.765
batch start
#iterations: 115
currently lose_sum: 60.30883032083511
time_elpased: 3.308
batch start
#iterations: 116
currently lose_sum: 59.71330711245537
time_elpased: 3.487
batch start
#iterations: 117
currently lose_sum: 58.94472852349281
time_elpased: 3.518
batch start
#iterations: 118
currently lose_sum: 58.52706143260002
time_elpased: 3.59
batch start
#iterations: 119
currently lose_sum: 58.98046126961708
time_elpased: 3.756
start validation test
0.618556701031
0.657426280745
0.497890295359
0.566643241977
0.618768549518
86.425
batch start
#iterations: 120
currently lose_sum: 58.349079579114914
time_elpased: 3.342
batch start
#iterations: 121
currently lose_sum: 59.203662037849426
time_elpased: 3.21
batch start
#iterations: 122
currently lose_sum: 58.805035412311554
time_elpased: 2.886
batch start
#iterations: 123
currently lose_sum: 58.65458211302757
time_elpased: 3.067
batch start
#iterations: 124
currently lose_sum: 58.156301856040955
time_elpased: 4.069
batch start
#iterations: 125
currently lose_sum: 57.75421845912933
time_elpased: 4.36
batch start
#iterations: 126
currently lose_sum: 58.27286410331726
time_elpased: 4.308
batch start
#iterations: 127
currently lose_sum: 58.23938289284706
time_elpased: 3.913
batch start
#iterations: 128
currently lose_sum: 57.7452891767025
time_elpased: 4.038
batch start
#iterations: 129
currently lose_sum: 56.08024123311043
time_elpased: 5.162
batch start
#iterations: 130
currently lose_sum: 56.861278384923935
time_elpased: 3.823
batch start
#iterations: 131
currently lose_sum: 56.73244097828865
time_elpased: 3.676
batch start
#iterations: 132
currently lose_sum: 57.1200729906559
time_elpased: 3.657
batch start
#iterations: 133
currently lose_sum: 56.09258219599724
time_elpased: 4.031
batch start
#iterations: 134
currently lose_sum: 55.95201274752617
time_elpased: 4.369
batch start
#iterations: 135
currently lose_sum: 55.17667177319527
time_elpased: 3.6
batch start
#iterations: 136
currently lose_sum: 55.59806165099144
time_elpased: 3.863
batch start
#iterations: 137
currently lose_sum: 54.51595976948738
time_elpased: 4.272
batch start
#iterations: 138
currently lose_sum: 54.813068151474
time_elpased: 3.766
batch start
#iterations: 139
currently lose_sum: 53.44507896900177
time_elpased: 3.874
start validation test
0.584690721649
0.659922928709
0.352475043738
0.459515663782
0.585098412089
106.601
batch start
#iterations: 140
currently lose_sum: 54.72516494989395
time_elpased: 3.744
batch start
#iterations: 141
currently lose_sum: 53.96747323870659
time_elpased: 4.538
batch start
#iterations: 142
currently lose_sum: 53.841736167669296
time_elpased: 3.956
batch start
#iterations: 143
currently lose_sum: 54.12779772281647
time_elpased: 4.387
batch start
#iterations: 144
currently lose_sum: 53.057079166173935
time_elpased: 3.709
batch start
#iterations: 145
currently lose_sum: 53.691319197416306
time_elpased: 3.71
batch start
#iterations: 146
currently lose_sum: 53.63960048556328
time_elpased: 3.22
batch start
#iterations: 147
currently lose_sum: 52.78225952386856
time_elpased: 3.776
batch start
#iterations: 148
currently lose_sum: 53.38548254966736
time_elpased: 3.779
batch start
#iterations: 149
currently lose_sum: 52.53420686721802
time_elpased: 3.759
batch start
#iterations: 150
currently lose_sum: 51.96030393242836
time_elpased: 3.836
batch start
#iterations: 151
currently lose_sum: 51.99694558978081
time_elpased: 4.182
batch start
#iterations: 152
currently lose_sum: 51.31213289499283
time_elpased: 3.762
batch start
#iterations: 153
currently lose_sum: 50.357330441474915
time_elpased: 3.462
batch start
#iterations: 154
currently lose_sum: 50.90032058954239
time_elpased: 3.597
batch start
#iterations: 155
currently lose_sum: 51.34480473399162
time_elpased: 3.342
batch start
#iterations: 156
currently lose_sum: 50.73665860295296
time_elpased: 3.144
batch start
#iterations: 157
currently lose_sum: 49.97383211553097
time_elpased: 3.317
batch start
#iterations: 158
currently lose_sum: 50.54658621549606
time_elpased: 4.175
batch start
#iterations: 159
currently lose_sum: 49.75626763701439
time_elpased: 4.562
start validation test
0.569639175258
0.6589958159
0.291756715036
0.404451102076
0.570127040777
124.020
batch start
#iterations: 160
currently lose_sum: 50.59891331195831
time_elpased: 3.497
batch start
#iterations: 161
currently lose_sum: 49.74777266383171
time_elpased: 3.965
batch start
#iterations: 162
currently lose_sum: 49.18715658783913
time_elpased: 3.907
batch start
#iterations: 163
currently lose_sum: 49.854211419820786
time_elpased: 3.985
batch start
#iterations: 164
currently lose_sum: 51.00241369009018
time_elpased: 3.676
batch start
#iterations: 165
currently lose_sum: 49.65963935852051
time_elpased: 3.654
batch start
#iterations: 166
currently lose_sum: 49.65328796207905
time_elpased: 3.359
batch start
#iterations: 167
currently lose_sum: 48.77103178203106
time_elpased: 3.188
batch start
#iterations: 168
currently lose_sum: 48.46975329518318
time_elpased: 3.82
batch start
#iterations: 169
currently lose_sum: 48.456964030861855
time_elpased: 3.894
batch start
#iterations: 170
currently lose_sum: 48.2115672826767
time_elpased: 3.512
batch start
#iterations: 171
currently lose_sum: 48.15715254843235
time_elpased: 3.492
batch start
#iterations: 172
currently lose_sum: 47.217401057481766
time_elpased: 3.462
batch start
#iterations: 173
currently lose_sum: 46.14586868882179
time_elpased: 3.181
batch start
#iterations: 174
currently lose_sum: 48.240421175956726
time_elpased: 3.226
batch start
#iterations: 175
currently lose_sum: 46.14851093292236
time_elpased: 3.214
batch start
#iterations: 176
currently lose_sum: 46.1102200448513
time_elpased: 3.246
batch start
#iterations: 177
currently lose_sum: 45.038279950618744
time_elpased: 3.63
batch start
#iterations: 178
currently lose_sum: 45.12471820414066
time_elpased: 3.543
batch start
#iterations: 179
currently lose_sum: 45.61747673153877
time_elpased: 3.614
start validation test
0.557113402062
0.671546203111
0.226613152207
0.338873499538
0.557693646227
153.031
batch start
#iterations: 180
currently lose_sum: 46.09156000614166
time_elpased: 3.737
batch start
#iterations: 181
currently lose_sum: 45.78139954805374
time_elpased: 3.634
batch start
#iterations: 182
currently lose_sum: 46.418446600437164
time_elpased: 3.693
batch start
#iterations: 183
currently lose_sum: 44.89121301472187
time_elpased: 3.641
batch start
#iterations: 184
currently lose_sum: 45.20272286236286
time_elpased: 3.62
batch start
#iterations: 185
currently lose_sum: 44.698209032416344
time_elpased: 3.854
batch start
#iterations: 186
currently lose_sum: 44.56868954002857
time_elpased: 4.009
batch start
#iterations: 187
currently lose_sum: 45.468834578990936
time_elpased: 3.685
batch start
#iterations: 188
currently lose_sum: 44.12977088987827
time_elpased: 3.334
batch start
#iterations: 189
currently lose_sum: 43.97849766910076
time_elpased: 3.889
batch start
#iterations: 190
currently lose_sum: 44.164570197463036
time_elpased: 4.372
batch start
#iterations: 191
currently lose_sum: 43.73253636062145
time_elpased: 4.428
batch start
#iterations: 192
currently lose_sum: 43.173475444316864
time_elpased: 4.372
batch start
#iterations: 193
currently lose_sum: 42.993614718317986
time_elpased: 3.974
batch start
#iterations: 194
currently lose_sum: 42.79762980341911
time_elpased: 4.37
batch start
#iterations: 195
currently lose_sum: 43.111201003193855
time_elpased: 3.671
batch start
#iterations: 196
currently lose_sum: 41.81922769546509
time_elpased: 4.054
batch start
#iterations: 197
currently lose_sum: 41.894111946225166
time_elpased: 3.582
batch start
#iterations: 198
currently lose_sum: 42.59483024477959
time_elpased: 3.613
batch start
#iterations: 199
currently lose_sum: 41.825564444065094
time_elpased: 3.9
start validation test
0.582164948454
0.652067207854
0.355459503962
0.460103903024
0.582562964828
129.239
batch start
#iterations: 200
currently lose_sum: 42.01124493777752
time_elpased: 3.847
batch start
#iterations: 201
currently lose_sum: 41.10962130129337
time_elpased: 4.036
batch start
#iterations: 202
currently lose_sum: 42.38653506338596
time_elpased: 3.538
batch start
#iterations: 203
currently lose_sum: 41.216645643115044
time_elpased: 3.442
batch start
#iterations: 204
currently lose_sum: 41.4260338395834
time_elpased: 3.192
batch start
#iterations: 205
currently lose_sum: 40.302169770002365
time_elpased: 3.12
batch start
#iterations: 206
currently lose_sum: 41.16998998820782
time_elpased: 3.293
batch start
#iterations: 207
currently lose_sum: 40.38954308629036
time_elpased: 4.323
batch start
#iterations: 208
currently lose_sum: 40.09400483965874
time_elpased: 3.943
batch start
#iterations: 209
currently lose_sum: 39.79555755853653
time_elpased: 4.086
batch start
#iterations: 210
currently lose_sum: 38.931775346398354
time_elpased: 3.552
batch start
#iterations: 211
currently lose_sum: 38.6946789175272
time_elpased: 3.796
batch start
#iterations: 212
currently lose_sum: 38.87572391331196
time_elpased: 4.116
batch start
#iterations: 213
currently lose_sum: 37.642633721232414
time_elpased: 3.762
batch start
#iterations: 214
currently lose_sum: 38.056116938591
time_elpased: 4.49
batch start
#iterations: 215
currently lose_sum: 37.53268040716648
time_elpased: 3.995
batch start
#iterations: 216
currently lose_sum: 37.80657722055912
time_elpased: 3.825
batch start
#iterations: 217
currently lose_sum: 38.634123012423515
time_elpased: 3.697
batch start
#iterations: 218
currently lose_sum: 38.062440037727356
time_elpased: 3.84
batch start
#iterations: 219
currently lose_sum: 36.98989041149616
time_elpased: 3.699
start validation test
0.556391752577
0.665672532061
0.229700524853
0.341545524101
0.556965309416
171.796
batch start
#iterations: 220
currently lose_sum: 38.56557273864746
time_elpased: 3.483
batch start
#iterations: 221
currently lose_sum: 36.595882803201675
time_elpased: 3.469
batch start
#iterations: 222
currently lose_sum: 37.8442969173193
time_elpased: 3.793
batch start
#iterations: 223
currently lose_sum: 36.98967722058296
time_elpased: 3.464
batch start
#iterations: 224
currently lose_sum: 35.20272922515869
time_elpased: 3.541
batch start
#iterations: 225
currently lose_sum: 35.82186195254326
time_elpased: 3.329
batch start
#iterations: 226
currently lose_sum: 35.17079907655716
time_elpased: 3.504
batch start
#iterations: 227
currently lose_sum: 35.147673174738884
time_elpased: 3.666
batch start
#iterations: 228
currently lose_sum: 34.94301976263523
time_elpased: 3.706
batch start
#iterations: 229
currently lose_sum: 34.46914152801037
time_elpased: 3.205
batch start
#iterations: 230
currently lose_sum: 35.32581126689911
time_elpased: 3.752
batch start
#iterations: 231
currently lose_sum: 35.418023467063904
time_elpased: 3.593
batch start
#iterations: 232
currently lose_sum: 34.78681428730488
time_elpased: 3.524
batch start
#iterations: 233
currently lose_sum: 34.63718721270561
time_elpased: 3.406
batch start
#iterations: 234
currently lose_sum: 34.38805274665356
time_elpased: 3.635
batch start
#iterations: 235
currently lose_sum: 33.784542232751846
time_elpased: 3.786
batch start
#iterations: 236
currently lose_sum: 33.06409095227718
time_elpased: 3.713
batch start
#iterations: 237
currently lose_sum: 34.121040508151054
time_elpased: 3.597
batch start
#iterations: 238
currently lose_sum: 33.560973800718784
time_elpased: 3.587
batch start
#iterations: 239
currently lose_sum: 33.12166194617748
time_elpased: 3.411
start validation test
0.54324742268
0.653515064562
0.187506432026
0.291403438625
0.543871980859
187.574
batch start
#iterations: 240
currently lose_sum: 32.236769899725914
time_elpased: 3.436
batch start
#iterations: 241
currently lose_sum: 33.938323587179184
time_elpased: 3.495
batch start
#iterations: 242
currently lose_sum: 32.678000286221504
time_elpased: 3.427
batch start
#iterations: 243
currently lose_sum: 32.09628401696682
time_elpased: 3.551
batch start
#iterations: 244
currently lose_sum: 30.69579689204693
time_elpased: 3.809
batch start
#iterations: 245
currently lose_sum: 31.371665567159653
time_elpased: 3.512
batch start
#iterations: 246
currently lose_sum: 32.187269285321236
time_elpased: 3.61
batch start
#iterations: 247
currently lose_sum: 30.80875039100647
time_elpased: 3.741
batch start
#iterations: 248
currently lose_sum: 30.536035731434822
time_elpased: 3.726
batch start
#iterations: 249
currently lose_sum: 30.186206489801407
time_elpased: 3.696
batch start
#iterations: 250
currently lose_sum: 29.6848803460598
time_elpased: 3.791
batch start
#iterations: 251
currently lose_sum: nan
time_elpased: 3.874
train finish final lose is: nan
acc: 0.672
pre: 0.657
rec: 0.721
F1: 0.688
auc: 0.672
