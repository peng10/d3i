start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 99.95412158966064
time_elpased: 4.162
batch start
#iterations: 1
currently lose_sum: 98.85938394069672
time_elpased: 4.102
batch start
#iterations: 2
currently lose_sum: 97.99815374612808
time_elpased: 4.351
batch start
#iterations: 3
currently lose_sum: 96.5663709640503
time_elpased: 4.09
batch start
#iterations: 4
currently lose_sum: 95.92205822467804
time_elpased: 4.493
batch start
#iterations: 5
currently lose_sum: 95.35335594415665
time_elpased: 4.257
batch start
#iterations: 6
currently lose_sum: 94.53046572208405
time_elpased: 4.224
batch start
#iterations: 7
currently lose_sum: 93.94627159833908
time_elpased: 4.159
batch start
#iterations: 8
currently lose_sum: 93.25886929035187
time_elpased: 4.509
batch start
#iterations: 9
currently lose_sum: 92.66378378868103
time_elpased: 4.254
batch start
#iterations: 10
currently lose_sum: 92.75772976875305
time_elpased: 4.477
batch start
#iterations: 11
currently lose_sum: 92.1634116768837
time_elpased: 4.022
batch start
#iterations: 12
currently lose_sum: 91.67628645896912
time_elpased: 4.159
batch start
#iterations: 13
currently lose_sum: 90.96789634227753
time_elpased: 4.465
batch start
#iterations: 14
currently lose_sum: 90.69829261302948
time_elpased: 4.276
batch start
#iterations: 15
currently lose_sum: 90.52193081378937
time_elpased: 4.336
batch start
#iterations: 16
currently lose_sum: 90.67543476819992
time_elpased: 4.434
batch start
#iterations: 17
currently lose_sum: 89.55613434314728
time_elpased: 4.395
batch start
#iterations: 18
currently lose_sum: 89.27418398857117
time_elpased: 4.098
batch start
#iterations: 19
currently lose_sum: 89.02550804615021
time_elpased: 4.05
start validation test
0.678041237113
0.671339717642
0.699804466399
0.685276630051
0.678003028408
58.777
batch start
#iterations: 20
currently lose_sum: 88.69610172510147
time_elpased: 3.719
batch start
#iterations: 21
currently lose_sum: 88.00411921739578
time_elpased: 4.442
batch start
#iterations: 22
currently lose_sum: 88.21689808368683
time_elpased: 4.236
batch start
#iterations: 23
currently lose_sum: 87.96437412500381
time_elpased: 4.493
batch start
#iterations: 24
currently lose_sum: 87.33445227146149
time_elpased: 4.18
batch start
#iterations: 25
currently lose_sum: 86.70525139570236
time_elpased: 4.636
batch start
#iterations: 26
currently lose_sum: 87.14552438259125
time_elpased: 4.621
batch start
#iterations: 27
currently lose_sum: 86.26341193914413
time_elpased: 4.491
batch start
#iterations: 28
currently lose_sum: 85.71663653850555
time_elpased: 3.981
batch start
#iterations: 29
currently lose_sum: 85.40685844421387
time_elpased: 4.343
batch start
#iterations: 30
currently lose_sum: 85.61600810289383
time_elpased: 4.385
batch start
#iterations: 31
currently lose_sum: 85.22508609294891
time_elpased: 4.712
batch start
#iterations: 32
currently lose_sum: 84.74625939130783
time_elpased: 4.104
batch start
#iterations: 33
currently lose_sum: 83.9495638012886
time_elpased: 4.267
batch start
#iterations: 34
currently lose_sum: 83.71633237600327
time_elpased: 4.631
batch start
#iterations: 35
currently lose_sum: 83.27976459264755
time_elpased: 4.596
batch start
#iterations: 36
currently lose_sum: 83.10343986749649
time_elpased: 4.873
batch start
#iterations: 37
currently lose_sum: 82.4125725030899
time_elpased: 5.172
batch start
#iterations: 38
currently lose_sum: 82.50811141729355
time_elpased: 4.197
batch start
#iterations: 39
currently lose_sum: 82.0518302321434
time_elpased: 4.16
start validation test
0.622628865979
0.680367359229
0.465061232891
0.55247875787
0.622905500262
62.681
batch start
#iterations: 40
currently lose_sum: 82.14070957899094
time_elpased: 4.346
batch start
#iterations: 41
currently lose_sum: 81.54000222682953
time_elpased: 4.052
batch start
#iterations: 42
currently lose_sum: 81.1630996465683
time_elpased: 4.188
batch start
#iterations: 43
currently lose_sum: 80.92810717225075
time_elpased: 5.672
batch start
#iterations: 44
currently lose_sum: 80.6289986371994
time_elpased: 5.125
batch start
#iterations: 45
currently lose_sum: 79.81733727455139
time_elpased: 4.552
batch start
#iterations: 46
currently lose_sum: 79.68382531404495
time_elpased: 5.222
batch start
#iterations: 47
currently lose_sum: 79.27092096209526
time_elpased: 4.979
batch start
#iterations: 48
currently lose_sum: 79.16215071082115
time_elpased: 4.686
batch start
#iterations: 49
currently lose_sum: 78.47130689024925
time_elpased: 4.812
batch start
#iterations: 50
currently lose_sum: 78.5545152425766
time_elpased: 4.384
batch start
#iterations: 51
currently lose_sum: 78.09073340892792
time_elpased: 4.577
batch start
#iterations: 52
currently lose_sum: 78.21572822332382
time_elpased: 4.291
batch start
#iterations: 53
currently lose_sum: 77.72326126694679
time_elpased: 4.703
batch start
#iterations: 54
currently lose_sum: 76.47536414861679
time_elpased: 4.354
batch start
#iterations: 55
currently lose_sum: 77.29800620675087
time_elpased: 4.563
batch start
#iterations: 56
currently lose_sum: 76.60336756706238
time_elpased: 4.414
batch start
#iterations: 57
currently lose_sum: 76.65096125006676
time_elpased: 3.887
batch start
#iterations: 58
currently lose_sum: 75.93040004372597
time_elpased: 3.61
batch start
#iterations: 59
currently lose_sum: 77.29265040159225
time_elpased: 4.08
start validation test
0.61412371134
0.635162971041
0.539466913656
0.583416805787
0.614254782864
65.359
batch start
#iterations: 60
currently lose_sum: 76.18743669986725
time_elpased: 4.131
batch start
#iterations: 61
currently lose_sum: 75.40626949071884
time_elpased: 4.367
batch start
#iterations: 62
currently lose_sum: 74.95356622338295
time_elpased: 3.717
batch start
#iterations: 63
currently lose_sum: 74.36387971043587
time_elpased: 4.018
batch start
#iterations: 64
currently lose_sum: 74.84503415226936
time_elpased: 4.364
batch start
#iterations: 65
currently lose_sum: 73.54644855856895
time_elpased: 4.144
batch start
#iterations: 66
currently lose_sum: 74.19000321626663
time_elpased: 4.181
batch start
#iterations: 67
currently lose_sum: 73.21709483861923
time_elpased: 3.984
batch start
#iterations: 68
currently lose_sum: 72.80160883069038
time_elpased: 4.495
batch start
#iterations: 69
currently lose_sum: 72.91682881116867
time_elpased: 4.214
batch start
#iterations: 70
currently lose_sum: 73.4948223233223
time_elpased: 4.06
batch start
#iterations: 71
currently lose_sum: 72.01829382777214
time_elpased: 4.132
batch start
#iterations: 72
currently lose_sum: 71.69470947980881
time_elpased: 3.837
batch start
#iterations: 73
currently lose_sum: 70.88460192084312
time_elpased: 4.056
batch start
#iterations: 74
currently lose_sum: 70.35776898264885
time_elpased: 4.223
batch start
#iterations: 75
currently lose_sum: 72.02408599853516
time_elpased: 4.562
batch start
#iterations: 76
currently lose_sum: 70.52790209650993
time_elpased: 4.39
batch start
#iterations: 77
currently lose_sum: 70.61724185943604
time_elpased: 4.309
batch start
#iterations: 78
currently lose_sum: 70.33802178502083
time_elpased: 3.99
batch start
#iterations: 79
currently lose_sum: 69.88417884707451
time_elpased: 4.464
start validation test
0.600154639175
0.65081563558
0.435216630647
0.521615787851
0.600444213289
76.529
batch start
#iterations: 80
currently lose_sum: 69.50124591588974
time_elpased: 4.027
batch start
#iterations: 81
currently lose_sum: 69.63346302509308
time_elpased: 4.472
batch start
#iterations: 82
currently lose_sum: 69.90946981310844
time_elpased: 4.591
batch start
#iterations: 83
currently lose_sum: 69.30888509750366
time_elpased: 3.962
batch start
#iterations: 84
currently lose_sum: 69.0709578692913
time_elpased: 4.322
batch start
#iterations: 85
currently lose_sum: 68.2310374379158
time_elpased: 4.029
batch start
#iterations: 86
currently lose_sum: 67.89538952708244
time_elpased: 4.346
batch start
#iterations: 87
currently lose_sum: 67.74711385369301
time_elpased: 4.748
batch start
#iterations: 88
currently lose_sum: 67.84649822115898
time_elpased: 4.318
batch start
#iterations: 89
currently lose_sum: 67.31271252036095
time_elpased: 3.901
batch start
#iterations: 90
currently lose_sum: 66.32448130846024
time_elpased: 4.152
batch start
#iterations: 91
currently lose_sum: 66.52635923027992
time_elpased: 4.821
batch start
#iterations: 92
currently lose_sum: 66.59547120332718
time_elpased: 4.805
batch start
#iterations: 93
currently lose_sum: 65.7079162299633
time_elpased: 3.732
batch start
#iterations: 94
currently lose_sum: 66.3883943259716
time_elpased: 4.373
batch start
#iterations: 95
currently lose_sum: 64.9588794708252
time_elpased: 3.815
batch start
#iterations: 96
currently lose_sum: 66.19072169065475
time_elpased: 4.659
batch start
#iterations: 97
currently lose_sum: 66.2082069516182
time_elpased: 4.674
batch start
#iterations: 98
currently lose_sum: 64.68357813358307
time_elpased: 4.232
batch start
#iterations: 99
currently lose_sum: 65.51274001598358
time_elpased: 4.713
start validation test
0.600412371134
0.641877256318
0.457445713698
0.534190602091
0.600663371152
81.267
batch start
#iterations: 100
currently lose_sum: 65.32501900196075
time_elpased: 4.766
batch start
#iterations: 101
currently lose_sum: 64.07921183109283
time_elpased: 4.123
batch start
#iterations: 102
currently lose_sum: 65.00431248545647
time_elpased: 4.035
batch start
#iterations: 103
currently lose_sum: 63.46558517217636
time_elpased: 4.656
batch start
#iterations: 104
currently lose_sum: 63.76295682787895
time_elpased: 5.006
batch start
#iterations: 105
currently lose_sum: 63.356867492198944
time_elpased: 4.763
batch start
#iterations: 106
currently lose_sum: 64.26373934745789
time_elpased: 4.726
batch start
#iterations: 107
currently lose_sum: 63.27907198667526
time_elpased: 4.89
batch start
#iterations: 108
currently lose_sum: 63.214793771505356
time_elpased: 4.466
batch start
#iterations: 109
currently lose_sum: 62.840491741895676
time_elpased: 4.963
batch start
#iterations: 110
currently lose_sum: 62.05524057149887
time_elpased: 4.694
batch start
#iterations: 111
currently lose_sum: 61.54648840427399
time_elpased: 4.675
batch start
#iterations: 112
currently lose_sum: 61.52944540977478
time_elpased: 4.469
batch start
#iterations: 113
currently lose_sum: 61.76518672704697
time_elpased: 3.919
batch start
#iterations: 114
currently lose_sum: 61.631863832473755
time_elpased: 3.54
batch start
#iterations: 115
currently lose_sum: 61.0242423415184
time_elpased: 3.634
batch start
#iterations: 116
currently lose_sum: 61.34908238053322
time_elpased: 4.07
batch start
#iterations: 117
currently lose_sum: 60.47884851694107
time_elpased: 4.618
batch start
#iterations: 118
currently lose_sum: 61.360355257987976
time_elpased: 5.308
batch start
#iterations: 119
currently lose_sum: 59.85274749994278
time_elpased: 4.27
start validation test
0.592371134021
0.66247530088
0.3795410106
0.482596179011
0.592744790129
89.551
batch start
#iterations: 120
currently lose_sum: 60.19366681575775
time_elpased: 4.036
batch start
#iterations: 121
currently lose_sum: 60.42661428451538
time_elpased: 4.865
batch start
#iterations: 122
currently lose_sum: 60.30517640709877
time_elpased: 4.816
batch start
#iterations: 123
currently lose_sum: 59.46481296420097
time_elpased: 4.348
batch start
#iterations: 124
currently lose_sum: 59.443581610918045
time_elpased: 4.009
batch start
#iterations: 125
currently lose_sum: 59.39058753848076
time_elpased: 4.303
batch start
#iterations: 126
currently lose_sum: 58.51866817474365
time_elpased: 4.438
batch start
#iterations: 127
currently lose_sum: 59.28298917412758
time_elpased: 3.876
batch start
#iterations: 128
currently lose_sum: 59.42461025714874
time_elpased: 4.471
batch start
#iterations: 129
currently lose_sum: 58.75906550884247
time_elpased: 5.002
batch start
#iterations: 130
currently lose_sum: 57.505205541849136
time_elpased: 4.707
batch start
#iterations: 131
currently lose_sum: 56.80094987154007
time_elpased: 3.962
batch start
#iterations: 132
currently lose_sum: 57.51879805326462
time_elpased: 3.234
batch start
#iterations: 133
currently lose_sum: 58.68648108839989
time_elpased: 3.355
batch start
#iterations: 134
currently lose_sum: 57.75366851687431
time_elpased: 3.188
batch start
#iterations: 135
currently lose_sum: 58.301531076431274
time_elpased: 3.7
batch start
#iterations: 136
currently lose_sum: 56.96988120675087
time_elpased: 4.598
batch start
#iterations: 137
currently lose_sum: 57.07887801527977
time_elpased: 4.271
batch start
#iterations: 138
currently lose_sum: 56.028462648391724
time_elpased: 4.757
batch start
#iterations: 139
currently lose_sum: 55.704619854688644
time_elpased: 4.587
start validation test
0.576237113402
0.658676283411
0.319543068848
0.430323608898
0.57668777939
102.454
batch start
#iterations: 140
currently lose_sum: 56.30524134635925
time_elpased: 4.194
batch start
#iterations: 141
currently lose_sum: 56.016630440950394
time_elpased: 4.524
batch start
#iterations: 142
currently lose_sum: 55.38327431678772
time_elpased: 4.099
batch start
#iterations: 143
currently lose_sum: 55.10496282577515
time_elpased: 4.584
batch start
#iterations: 144
currently lose_sum: 55.634573727846146
time_elpased: 4.425
batch start
#iterations: 145
currently lose_sum: 54.74975502490997
time_elpased: 4.014
batch start
#iterations: 146
currently lose_sum: 54.70564192533493
time_elpased: 3.862
batch start
#iterations: 147
currently lose_sum: 54.835886269807816
time_elpased: 5.302
batch start
#iterations: 148
currently lose_sum: 54.082125663757324
time_elpased: 5.312
batch start
#iterations: 149
currently lose_sum: 54.19483008980751
time_elpased: 4.687
batch start
#iterations: 150
currently lose_sum: 53.65334349870682
time_elpased: 4.482
batch start
#iterations: 151
currently lose_sum: 54.51810961961746
time_elpased: 4.12
batch start
#iterations: 152
currently lose_sum: 53.51626658439636
time_elpased: 4.279
batch start
#iterations: 153
currently lose_sum: 53.06876550614834
time_elpased: 3.969
batch start
#iterations: 154
currently lose_sum: 52.514639019966125
time_elpased: 3.902
batch start
#iterations: 155
currently lose_sum: 53.035676062107086
time_elpased: 3.949
batch start
#iterations: 156
currently lose_sum: 53.034394323825836
time_elpased: 3.912
batch start
#iterations: 157
currently lose_sum: 53.308766692876816
time_elpased: 4.653
batch start
#iterations: 158
currently lose_sum: 53.83105355501175
time_elpased: 4.624
batch start
#iterations: 159
currently lose_sum: 53.59790742397308
time_elpased: 4.399
start validation test
0.574175257732
0.650537634409
0.323762478131
0.432350718065
0.57461489599
100.811
batch start
#iterations: 160
currently lose_sum: 51.17185312509537
time_elpased: 5.017
batch start
#iterations: 161
currently lose_sum: 51.41520869731903
time_elpased: 4.366
batch start
#iterations: 162
currently lose_sum: 51.678932934999466
time_elpased: 4.164
batch start
#iterations: 163
currently lose_sum: 51.414625346660614
time_elpased: 4.215
batch start
#iterations: 164
currently lose_sum: 51.42766144871712
time_elpased: 4.506
batch start
#iterations: 165
currently lose_sum: 50.47630751132965
time_elpased: 4.643
batch start
#iterations: 166
currently lose_sum: 50.566930413246155
time_elpased: 4.209
batch start
#iterations: 167
currently lose_sum: 49.74541109800339
time_elpased: 4.494
batch start
#iterations: 168
currently lose_sum: 49.712319046258926
time_elpased: 4.164
batch start
#iterations: 169
currently lose_sum: 50.658603489398956
time_elpased: 4.551
batch start
#iterations: 170
currently lose_sum: 49.05568191409111
time_elpased: 4.24
batch start
#iterations: 171
currently lose_sum: 49.048968493938446
time_elpased: 3.848
batch start
#iterations: 172
currently lose_sum: 49.00158040225506
time_elpased: 4.469
batch start
#iterations: 173
currently lose_sum: 49.8139486014843
time_elpased: 4.036
batch start
#iterations: 174
currently lose_sum: 49.586300149559975
time_elpased: 3.959
batch start
#iterations: 175
currently lose_sum: 48.61032159626484
time_elpased: 4.192
batch start
#iterations: 176
currently lose_sum: 47.324315175414085
time_elpased: 4.464
batch start
#iterations: 177
currently lose_sum: 48.036318227648735
time_elpased: 4.228
batch start
#iterations: 178
currently lose_sum: 47.9745118021965
time_elpased: 4.447
batch start
#iterations: 179
currently lose_sum: 46.983006685972214
time_elpased: 4.051
start validation test
0.565
0.657855731225
0.274055778532
0.386923356339
0.565510797456
130.786
batch start
#iterations: 180
currently lose_sum: 47.423983469605446
time_elpased: 4.67
batch start
#iterations: 181
currently lose_sum: 46.93788869678974
time_elpased: 4.195
batch start
#iterations: 182
currently lose_sum: 48.38387395441532
time_elpased: 4.466
batch start
#iterations: 183
currently lose_sum: 46.34953485429287
time_elpased: 4.465
batch start
#iterations: 184
currently lose_sum: 46.738006338477135
time_elpased: 3.797
batch start
#iterations: 185
currently lose_sum: 47.64075544476509
time_elpased: 4.84
batch start
#iterations: 186
currently lose_sum: 46.632601112127304
time_elpased: 4.235
batch start
#iterations: 187
currently lose_sum: 45.09010085463524
time_elpased: 4.564
batch start
#iterations: 188
currently lose_sum: 46.04574991762638
time_elpased: 4.554
batch start
#iterations: 189
currently lose_sum: 44.84954859316349
time_elpased: 4.419
batch start
#iterations: 190
currently lose_sum: 45.671592995524406
time_elpased: 3.853
batch start
#iterations: 191
currently lose_sum: 44.57739798724651
time_elpased: 4.312
batch start
#iterations: 192
currently lose_sum: 45.42005218565464
time_elpased: 4.243
batch start
#iterations: 193
currently lose_sum: 45.37298908829689
time_elpased: 4.155
batch start
#iterations: 194
currently lose_sum: 44.494165524840355
time_elpased: 3.992
batch start
#iterations: 195
currently lose_sum: 42.96636877954006
time_elpased: 4.133
batch start
#iterations: 196
currently lose_sum: 44.617844969034195
time_elpased: 3.869
batch start
#iterations: 197
currently lose_sum: 42.729435458779335
time_elpased: 4.444
batch start
#iterations: 198
currently lose_sum: 42.88545402884483
time_elpased: 4.47
batch start
#iterations: 199
currently lose_sum: 44.60733936727047
time_elpased: 4.126
start validation test
0.550567010309
0.640801354402
0.233714109293
0.342508106478
0.551123294448
141.459
batch start
#iterations: 200
currently lose_sum: 43.210572957992554
time_elpased: 4.305
batch start
#iterations: 201
currently lose_sum: 43.27872584760189
time_elpased: 4.517
batch start
#iterations: 202
currently lose_sum: 42.47987547516823
time_elpased: 4.004
batch start
#iterations: 203
currently lose_sum: 42.09607592225075
time_elpased: 3.888
batch start
#iterations: 204
currently lose_sum: 40.73933517932892
time_elpased: 4.251
batch start
#iterations: 205
currently lose_sum: 40.778408750891685
time_elpased: 4.089
batch start
#iterations: 206
currently lose_sum: 42.35239568352699
time_elpased: 4.341
batch start
#iterations: 207
currently lose_sum: 41.03044427931309
time_elpased: 4.165
batch start
#iterations: 208
currently lose_sum: 41.01869587600231
time_elpased: 3.901
batch start
#iterations: 209
currently lose_sum: 40.31438998878002
time_elpased: 3.989
batch start
#iterations: 210
currently lose_sum: 39.83714972436428
time_elpased: 4.391
batch start
#iterations: 211
currently lose_sum: 40.650470450520515
time_elpased: 4.195
batch start
#iterations: 212
currently lose_sum: 40.58128501474857
time_elpased: 3.828
batch start
#iterations: 213
currently lose_sum: 39.30975356698036
time_elpased: 4.296
batch start
#iterations: 214
currently lose_sum: 38.66592988371849
time_elpased: 4.719
batch start
#iterations: 215
currently lose_sum: 39.56627084314823
time_elpased: 4.692
batch start
#iterations: 216
currently lose_sum: 39.313581332564354
time_elpased: 3.875
batch start
#iterations: 217
currently lose_sum: 38.71269103884697
time_elpased: 4.147
batch start
#iterations: 218
currently lose_sum: 37.549908727407455
time_elpased: 4.219
batch start
#iterations: 219
currently lose_sum: 37.63973394036293
time_elpased: 4.487
start validation test
0.550257731959
0.658466453674
0.212102500772
0.320853117459
0.550851415624
150.293
batch start
#iterations: 220
currently lose_sum: 37.31705752015114
time_elpased: 4.232
batch start
#iterations: 221
currently lose_sum: 37.873156785964966
time_elpased: 4.264
batch start
#iterations: 222
currently lose_sum: 38.911018401384354
time_elpased: 4.408
batch start
#iterations: 223
currently lose_sum: 36.67485083639622
time_elpased: 4.256
batch start
#iterations: 224
currently lose_sum: 37.7338033169508
time_elpased: 4.554
batch start
#iterations: 225
currently lose_sum: 36.91292557120323
time_elpased: 4.007
batch start
#iterations: 226
currently lose_sum: 35.708185866475105
time_elpased: 4.562
batch start
#iterations: 227
currently lose_sum: 36.823218300938606
time_elpased: 4.264
batch start
#iterations: 228
currently lose_sum: 36.39882355928421
time_elpased: 4.105
batch start
#iterations: 229
currently lose_sum: 37.157107681035995
time_elpased: 4.357
batch start
#iterations: 230
currently lose_sum: 35.22161529958248
time_elpased: 4.381
batch start
#iterations: 231
currently lose_sum: 34.95045284926891
time_elpased: 4.105
batch start
#iterations: 232
currently lose_sum: 35.29520320892334
time_elpased: 4.194
batch start
#iterations: 233
currently lose_sum: 34.44285698235035
time_elpased: 4.1
batch start
#iterations: 234
currently lose_sum: 34.416247844696045
time_elpased: 4.207
batch start
#iterations: 235
currently lose_sum: 36.07484878599644
time_elpased: 4.108
batch start
#iterations: 236
currently lose_sum: 35.13426297903061
time_elpased: 4.384
batch start
#iterations: 237
currently lose_sum: 34.50849387049675
time_elpased: 4.477
batch start
#iterations: 238
currently lose_sum: 34.797589644789696
time_elpased: 4.526
batch start
#iterations: 239
currently lose_sum: 34.1531627625227
time_elpased: 3.561
start validation test
0.548144329897
0.671104713926
0.191931666152
0.298495518566
0.54876971617
184.718
batch start
#iterations: 240
currently lose_sum: 33.863962441682816
time_elpased: 3.196
batch start
#iterations: 241
currently lose_sum: 33.600221782922745
time_elpased: 4.611
batch start
#iterations: 242
currently lose_sum: 31.991479098796844
time_elpased: 4.361
batch start
#iterations: 243
currently lose_sum: 32.27545502781868
time_elpased: 4.27
batch start
#iterations: 244
currently lose_sum: 32.13005842268467
time_elpased: 4.653
batch start
#iterations: 245
currently lose_sum: 32.297390818595886
time_elpased: 5.204
batch start
#iterations: 246
currently lose_sum: 31.499895736575127
time_elpased: 4.204
batch start
#iterations: 247
currently lose_sum: 32.54896452277899
time_elpased: 3.756
batch start
#iterations: 248
currently lose_sum: 30.25055806338787
time_elpased: 4.765
batch start
#iterations: 249
currently lose_sum: 31.907695770263672
time_elpased: 4.583
batch start
#iterations: 250
currently lose_sum: 30.53452004492283
time_elpased: 4.22
batch start
#iterations: 251
currently lose_sum: 32.34045785665512
time_elpased: 4.344
batch start
#iterations: 252
currently lose_sum: 29.15488625317812
time_elpased: 4.708
batch start
#iterations: 253
currently lose_sum: 29.976737916469574
time_elpased: 4.865
batch start
#iterations: 254
currently lose_sum: 30.445002287626266
time_elpased: 4.591
batch start
#iterations: 255
currently lose_sum: 31.285547003149986
time_elpased: 4.291
batch start
#iterations: 256
currently lose_sum: 30.13584679365158
time_elpased: 4.229
batch start
#iterations: 257
currently lose_sum: 29.186681538820267
time_elpased: 4.93
batch start
#iterations: 258
currently lose_sum: 30.41876772046089
time_elpased: 4.598
batch start
#iterations: 259
currently lose_sum: 29.384015157818794
time_elpased: 4.247
start validation test
0.541237113402
0.649578908825
0.182566635793
0.285025706941
0.541866814747
187.078
batch start
#iterations: 260
currently lose_sum: 28.428793773055077
time_elpased: 4.705
batch start
#iterations: 261
currently lose_sum: 28.006864599883556
time_elpased: 4.142
batch start
#iterations: 262
currently lose_sum: 29.38934875279665
time_elpased: 4.503
batch start
#iterations: 263
currently lose_sum: 28.027598649263382
time_elpased: 4.311
batch start
#iterations: 264
currently lose_sum: 28.6336370408535
time_elpased: 4.254
batch start
#iterations: 265
currently lose_sum: 27.89863357692957
time_elpased: 4.237
batch start
#iterations: 266
currently lose_sum: 28.307917416095734
time_elpased: 4.412
batch start
#iterations: 267
currently lose_sum: 26.318139754235744
time_elpased: 4.659
batch start
#iterations: 268
currently lose_sum: 26.956321381032467
time_elpased: 4.655
batch start
#iterations: 269
currently lose_sum: 26.40988902002573
time_elpased: 4.778
batch start
#iterations: 270
currently lose_sum: 26.17711039632559
time_elpased: 3.818
batch start
#iterations: 271
currently lose_sum: 25.478943929076195
time_elpased: 4.331
batch start
#iterations: 272
currently lose_sum: 28.02224186807871
time_elpased: 4.424
batch start
#iterations: 273
currently lose_sum: 25.01213524490595
time_elpased: 4.031
batch start
#iterations: 274
currently lose_sum: 26.502839833498
time_elpased: 4.247
batch start
#iterations: 275
currently lose_sum: 25.818122692406178
time_elpased: 4.356
batch start
#iterations: 276
currently lose_sum: 25.24923324584961
time_elpased: 4.459
batch start
#iterations: 277
currently lose_sum: 23.821530409157276
time_elpased: 4.385
batch start
#iterations: 278
currently lose_sum: 23.991107113659382
time_elpased: 4.803
batch start
#iterations: 279
currently lose_sum: 24.462632901966572
time_elpased: 4.047
start validation test
0.528298969072
0.628753412193
0.142224966553
0.23197650021
0.528976781531
223.977
batch start
#iterations: 280
currently lose_sum: 25.7256977930665
time_elpased: 4.153
batch start
#iterations: 281
currently lose_sum: 23.936964832246304
time_elpased: 4.827
batch start
#iterations: 282
currently lose_sum: 24.523090608417988
time_elpased: 4.281
batch start
#iterations: 283
currently lose_sum: 22.841487146914005
time_elpased: 4.304
batch start
#iterations: 284
currently lose_sum: 24.458199575543404
time_elpased: 4.181
batch start
#iterations: 285
currently lose_sum: 23.63235904276371
time_elpased: 4.173
batch start
#iterations: 286
currently lose_sum: 23.164377823472023
time_elpased: 4.794
batch start
#iterations: 287
currently lose_sum: 23.929403476417065
time_elpased: 5.213
batch start
#iterations: 288
currently lose_sum: 22.329532511532307
time_elpased: 4.229
batch start
#iterations: 289
currently lose_sum: 21.48347517848015
time_elpased: 4.299
batch start
#iterations: 290
currently lose_sum: 22.144605733454227
time_elpased: 4.607
batch start
#iterations: 291
currently lose_sum: 23.80628778040409
time_elpased: 4.607
batch start
#iterations: 292
currently lose_sum: 22.621795505285263
time_elpased: 3.996
batch start
#iterations: 293
currently lose_sum: 23.27221741527319
time_elpased: 4.102
batch start
#iterations: 294
currently lose_sum: 20.314968593418598
time_elpased: 4.1
batch start
#iterations: 295
currently lose_sum: 20.581577517092228
time_elpased: 3.939
batch start
#iterations: 296
currently lose_sum: 21.466412112116814
time_elpased: 4.145
batch start
#iterations: 297
currently lose_sum: 22.057875156402588
time_elpased: 5.043
batch start
#iterations: 298
currently lose_sum: 20.655898675322533
time_elpased: 4.447
batch start
#iterations: 299
currently lose_sum: 21.012016654014587
time_elpased: 4.367
start validation test
0.527371134021
0.618205349439
0.147473500051
0.238138761944
0.528038102912
218.944
batch start
#iterations: 300
currently lose_sum: 21.86085644364357
time_elpased: 4.325
batch start
#iterations: 301
currently lose_sum: 21.720743030309677
time_elpased: 4.175
batch start
#iterations: 302
currently lose_sum: 19.950829528272152
time_elpased: 4.195
batch start
#iterations: 303
currently lose_sum: 21.690140157938004
time_elpased: 4.191
batch start
#iterations: 304
currently lose_sum: 20.841587141156197
time_elpased: 4.239
batch start
#iterations: 305
currently lose_sum: 21.865272358059883
time_elpased: 4.374
batch start
#iterations: 306
currently lose_sum: 21.680907495319843
time_elpased: 4.722
batch start
#iterations: 307
currently lose_sum: 20.979262843728065
time_elpased: 4.065
batch start
#iterations: 308
currently lose_sum: 20.77755779027939
time_elpased: 3.837
batch start
#iterations: 309
currently lose_sum: 19.331378683447838
time_elpased: 4.569
batch start
#iterations: 310
currently lose_sum: 17.820969555526972
time_elpased: 4.56
batch start
#iterations: 311
currently lose_sum: 18.39752011746168
time_elpased: 3.825
batch start
#iterations: 312
currently lose_sum: 19.40189915150404
time_elpased: 3.749
batch start
#iterations: 313
currently lose_sum: 18.44239179044962
time_elpased: 3.732
batch start
#iterations: 314
currently lose_sum: 18.196207597851753
time_elpased: 4.08
batch start
#iterations: 315
currently lose_sum: 18.968749817460775
time_elpased: 4.526
batch start
#iterations: 316
currently lose_sum: 17.674943204969168
time_elpased: 4.873
batch start
#iterations: 317
currently lose_sum: 17.639255598187447
time_elpased: 4.428
batch start
#iterations: 318
currently lose_sum: 19.186108075082302
time_elpased: 4.262
batch start
#iterations: 319
currently lose_sum: 17.299659315496683
time_elpased: 4.529
start validation test
0.515567010309
0.635284139101
0.0770814037254
0.13748164464
0.516336839423
284.296
batch start
#iterations: 320
currently lose_sum: 17.15727100148797
time_elpased: 4.144
batch start
#iterations: 321
currently lose_sum: 18.614045336842537
time_elpased: 4.461
batch start
#iterations: 322
currently lose_sum: 16.708514116704464
time_elpased: 3.789
batch start
#iterations: 323
currently lose_sum: 18.16543996334076
time_elpased: 3.924
batch start
#iterations: 324
currently lose_sum: 17.329409182071686
time_elpased: 4.031
batch start
#iterations: 325
currently lose_sum: 15.051522590219975
time_elpased: 4.392
batch start
#iterations: 326
currently lose_sum: 16.406824585050344
time_elpased: 4.22
batch start
#iterations: 327
currently lose_sum: 18.05529325455427
time_elpased: 4.351
batch start
#iterations: 328
currently lose_sum: 18.73268249630928
time_elpased: 4.336
batch start
#iterations: 329
currently lose_sum: 17.62242178246379
time_elpased: 4.203
batch start
#iterations: 330
currently lose_sum: 16.89839193224907
time_elpased: 3.786
batch start
#iterations: 331
currently lose_sum: 15.921122267842293
time_elpased: 4.451
batch start
#iterations: 332
currently lose_sum: nan
time_elpased: 4.032
train finish final lose is: nan
acc: 0.673
pre: 0.667
rec: 0.693
F1: 0.680
auc: 0.673
