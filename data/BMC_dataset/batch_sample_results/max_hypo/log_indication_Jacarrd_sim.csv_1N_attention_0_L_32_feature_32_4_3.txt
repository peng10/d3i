start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.07569807767868
time_elpased: 2.482
batch start
#iterations: 1
currently lose_sum: 98.91262763738632
time_elpased: 2.66
batch start
#iterations: 2
currently lose_sum: 98.21673601865768
time_elpased: 2.576
batch start
#iterations: 3
currently lose_sum: 97.08097720146179
time_elpased: 2.497
batch start
#iterations: 4
currently lose_sum: 96.70132595300674
time_elpased: 2.112
batch start
#iterations: 5
currently lose_sum: 95.66892468929291
time_elpased: 2.129
batch start
#iterations: 6
currently lose_sum: 94.81432366371155
time_elpased: 2.368
batch start
#iterations: 7
currently lose_sum: 94.2351438999176
time_elpased: 2.105
batch start
#iterations: 8
currently lose_sum: 93.6068884730339
time_elpased: 2.188
batch start
#iterations: 9
currently lose_sum: 93.23050194978714
time_elpased: 2.14
batch start
#iterations: 10
currently lose_sum: 93.01164847612381
time_elpased: 2.251
batch start
#iterations: 11
currently lose_sum: 92.59087830781937
time_elpased: 2.386
batch start
#iterations: 12
currently lose_sum: 92.30105900764465
time_elpased: 2.487
batch start
#iterations: 13
currently lose_sum: 92.32231748104095
time_elpased: 2.387
batch start
#iterations: 14
currently lose_sum: 91.83820515871048
time_elpased: 2.346
batch start
#iterations: 15
currently lose_sum: 91.43057733774185
time_elpased: 2.381
batch start
#iterations: 16
currently lose_sum: 91.07981526851654
time_elpased: 2.177
batch start
#iterations: 17
currently lose_sum: 91.21752369403839
time_elpased: 2.39
batch start
#iterations: 18
currently lose_sum: 90.60004311800003
time_elpased: 2.144
batch start
#iterations: 19
currently lose_sum: 90.15260112285614
time_elpased: 2.379
start validation test
0.661546391753
0.649743297205
0.703375874846
0.675496688742
0.661477280669
59.704
batch start
#iterations: 20
currently lose_sum: 90.07134854793549
time_elpased: 2.462
batch start
#iterations: 21
currently lose_sum: 90.14927345514297
time_elpased: 2.468
batch start
#iterations: 22
currently lose_sum: 89.8314637541771
time_elpased: 2.264
batch start
#iterations: 23
currently lose_sum: 89.76045620441437
time_elpased: 2.385
batch start
#iterations: 24
currently lose_sum: 88.63176989555359
time_elpased: 2.379
batch start
#iterations: 25
currently lose_sum: 88.76373666524887
time_elpased: 2.402
batch start
#iterations: 26
currently lose_sum: 89.1229630112648
time_elpased: 2.534
batch start
#iterations: 27
currently lose_sum: 88.54201120138168
time_elpased: 2.21
batch start
#iterations: 28
currently lose_sum: 88.5149387717247
time_elpased: 2.288
batch start
#iterations: 29
currently lose_sum: 88.72149527072906
time_elpased: 2.017
batch start
#iterations: 30
currently lose_sum: 88.51414597034454
time_elpased: 1.973
batch start
#iterations: 31
currently lose_sum: 87.60051238536835
time_elpased: 2.097
batch start
#iterations: 32
currently lose_sum: 87.30869340896606
time_elpased: 2.266
batch start
#iterations: 33
currently lose_sum: 88.09272021055222
time_elpased: 2.386
batch start
#iterations: 34
currently lose_sum: 88.14392328262329
time_elpased: 2.338
batch start
#iterations: 35
currently lose_sum: 87.77969419956207
time_elpased: 2.407
batch start
#iterations: 36
currently lose_sum: 87.71398156881332
time_elpased: 2.364
batch start
#iterations: 37
currently lose_sum: 87.18691056966782
time_elpased: 1.976
batch start
#iterations: 38
currently lose_sum: 86.71684247255325
time_elpased: 2.289
batch start
#iterations: 39
currently lose_sum: 87.34953689575195
time_elpased: 2.268
start validation test
0.663659793814
0.646201777696
0.725813091807
0.683697707111
0.663557103524
59.210
batch start
#iterations: 40
currently lose_sum: 86.45631164312363
time_elpased: 2.247
batch start
#iterations: 41
currently lose_sum: 86.75938212871552
time_elpased: 2.202
batch start
#iterations: 42
currently lose_sum: 87.21874350309372
time_elpased: 2.361
batch start
#iterations: 43
currently lose_sum: 86.29189759492874
time_elpased: 2.448
batch start
#iterations: 44
currently lose_sum: 86.51088184118271
time_elpased: 2.116
batch start
#iterations: 45
currently lose_sum: 86.47975927591324
time_elpased: 2.193
batch start
#iterations: 46
currently lose_sum: 85.84695827960968
time_elpased: 2.047
batch start
#iterations: 47
currently lose_sum: 85.7587480545044
time_elpased: 2.516
batch start
#iterations: 48
currently lose_sum: 86.27373194694519
time_elpased: 2.384
batch start
#iterations: 49
currently lose_sum: 85.55759382247925
time_elpased: 2.063
batch start
#iterations: 50
currently lose_sum: 85.84829449653625
time_elpased: 2.563
batch start
#iterations: 51
currently lose_sum: 86.02704054117203
time_elpased: 2.457
batch start
#iterations: 52
currently lose_sum: 85.40038323402405
time_elpased: 2.447
batch start
#iterations: 53
currently lose_sum: 85.43529373407364
time_elpased: 2.461
batch start
#iterations: 54
currently lose_sum: 85.31486463546753
time_elpased: 2.599
batch start
#iterations: 55
currently lose_sum: 85.25190514326096
time_elpased: 2.586
batch start
#iterations: 56
currently lose_sum: 84.99884617328644
time_elpased: 2.329
batch start
#iterations: 57
currently lose_sum: 84.75981044769287
time_elpased: 2.403
batch start
#iterations: 58
currently lose_sum: 84.79551863670349
time_elpased: 2.409
batch start
#iterations: 59
currently lose_sum: 85.42279309034348
time_elpased: 2.105
start validation test
0.663917525773
0.662895005097
0.669308357349
0.666086243982
0.663908618988
61.241
batch start
#iterations: 60
currently lose_sum: 84.40821045637131
time_elpased: 2.038
batch start
#iterations: 61
currently lose_sum: 85.0760834813118
time_elpased: 2.412
batch start
#iterations: 62
currently lose_sum: 84.5961366891861
time_elpased: 2.288
batch start
#iterations: 63
currently lose_sum: 84.35976910591125
time_elpased: 2.382
batch start
#iterations: 64
currently lose_sum: 84.81963855028152
time_elpased: 2.323
batch start
#iterations: 65
currently lose_sum: 84.43260425329208
time_elpased: 2.387
batch start
#iterations: 66
currently lose_sum: 84.21186995506287
time_elpased: 2.384
batch start
#iterations: 67
currently lose_sum: 84.09486413002014
time_elpased: 2.54
batch start
#iterations: 68
currently lose_sum: 84.4599621295929
time_elpased: 2.457
batch start
#iterations: 69
currently lose_sum: 83.78475272655487
time_elpased: 2.348
batch start
#iterations: 70
currently lose_sum: 83.91480940580368
time_elpased: 2.343
batch start
#iterations: 71
currently lose_sum: 83.29422426223755
time_elpased: 2.453
batch start
#iterations: 72
currently lose_sum: 83.63766676187515
time_elpased: 2.322
batch start
#iterations: 73
currently lose_sum: 83.34002751111984
time_elpased: 2.494
batch start
#iterations: 74
currently lose_sum: 83.44666987657547
time_elpased: 2.49
batch start
#iterations: 75
currently lose_sum: 83.127312541008
time_elpased: 2.443
batch start
#iterations: 76
currently lose_sum: 83.55038520693779
time_elpased: 2.46
batch start
#iterations: 77
currently lose_sum: 83.0286255478859
time_elpased: 2.366
batch start
#iterations: 78
currently lose_sum: 83.56687659025192
time_elpased: 2.329
batch start
#iterations: 79
currently lose_sum: 83.86236810684204
time_elpased: 2.117
start validation test
0.658762886598
0.652362204724
0.682173734047
0.666934996981
0.658724206966
61.646
batch start
#iterations: 80
currently lose_sum: 82.69091862440109
time_elpased: 2.344
batch start
#iterations: 81
currently lose_sum: 83.232062458992
time_elpased: 2.368
batch start
#iterations: 82
currently lose_sum: 82.98079019784927
time_elpased: 2.378
batch start
#iterations: 83
currently lose_sum: 83.2160113453865
time_elpased: 2.389
batch start
#iterations: 84
currently lose_sum: 82.79466110467911
time_elpased: 2.093
batch start
#iterations: 85
currently lose_sum: 82.17479112744331
time_elpased: 2.123
batch start
#iterations: 86
currently lose_sum: 82.74198630452156
time_elpased: 1.967
batch start
#iterations: 87
currently lose_sum: 81.78791397809982
time_elpased: 1.686
batch start
#iterations: 88
currently lose_sum: 82.22032329440117
time_elpased: 1.784
batch start
#iterations: 89
currently lose_sum: 81.89875954389572
time_elpased: 1.828
batch start
#iterations: 90
currently lose_sum: 83.25067895650864
time_elpased: 1.912
batch start
#iterations: 91
currently lose_sum: 81.99230480194092
time_elpased: 2.048
batch start
#iterations: 92
currently lose_sum: 82.13442286849022
time_elpased: 1.821
batch start
#iterations: 93
currently lose_sum: 82.01503920555115
time_elpased: 1.902
batch start
#iterations: 94
currently lose_sum: 82.29299327731133
time_elpased: 1.753
batch start
#iterations: 95
currently lose_sum: 82.26868957281113
time_elpased: 1.929
batch start
#iterations: 96
currently lose_sum: 81.27873280644417
time_elpased: 1.754
batch start
#iterations: 97
currently lose_sum: 81.84786638617516
time_elpased: 1.984
batch start
#iterations: 98
currently lose_sum: 81.70632502436638
time_elpased: 1.864
batch start
#iterations: 99
currently lose_sum: 80.94783121347427
time_elpased: 1.831
start validation test
0.651082474227
0.648853419537
0.661074516262
0.65490695896
0.651065965277
64.096
batch start
#iterations: 100
currently lose_sum: 81.75243586301804
time_elpased: 2.169
batch start
#iterations: 101
currently lose_sum: 81.96785786747932
time_elpased: 2.167
batch start
#iterations: 102
currently lose_sum: 81.32863959670067
time_elpased: 2.462
batch start
#iterations: 103
currently lose_sum: 81.92942768335342
time_elpased: 2.663
batch start
#iterations: 104
currently lose_sum: 80.71972489356995
time_elpased: 2.306
batch start
#iterations: 105
currently lose_sum: 81.07512831687927
time_elpased: 2.522
batch start
#iterations: 106
currently lose_sum: 80.73959320783615
time_elpased: 2.424
batch start
#iterations: 107
currently lose_sum: 81.2930836379528
time_elpased: 2.342
batch start
#iterations: 108
currently lose_sum: 80.72199457883835
time_elpased: 2.178
batch start
#iterations: 109
currently lose_sum: 80.51437819004059
time_elpased: 2.331
batch start
#iterations: 110
currently lose_sum: 81.20650607347488
time_elpased: 2.395
batch start
#iterations: 111
currently lose_sum: 80.60516086220741
time_elpased: 2.411
batch start
#iterations: 112
currently lose_sum: 80.98173969984055
time_elpased: 2.439
batch start
#iterations: 113
currently lose_sum: 79.99042066931725
time_elpased: 2.373
batch start
#iterations: 114
currently lose_sum: 80.76425260305405
time_elpased: 2.264
batch start
#iterations: 115
currently lose_sum: 80.24478501081467
time_elpased: 2.434
batch start
#iterations: 116
currently lose_sum: 80.59748548269272
time_elpased: 2.435
batch start
#iterations: 117
currently lose_sum: 80.33962032198906
time_elpased: 2.168
batch start
#iterations: 118
currently lose_sum: 80.87148377299309
time_elpased: 2.333
batch start
#iterations: 119
currently lose_sum: 80.34931200742722
time_elpased: 2.317
start validation test
0.643556701031
0.647095893289
0.634108686702
0.640536466185
0.643572311133
65.161
batch start
#iterations: 120
currently lose_sum: 79.62792950868607
time_elpased: 2.291
batch start
#iterations: 121
currently lose_sum: 79.79926618933678
time_elpased: 2.225
batch start
#iterations: 122
currently lose_sum: 80.54231292009354
time_elpased: 2.379
batch start
#iterations: 123
currently lose_sum: 80.0471502840519
time_elpased: 2.305
batch start
#iterations: 124
currently lose_sum: 80.48171544075012
time_elpased: 2.123
batch start
#iterations: 125
currently lose_sum: 80.23126605153084
time_elpased: 2.221
batch start
#iterations: 126
currently lose_sum: 79.25995129346848
time_elpased: 2.338
batch start
#iterations: 127
currently lose_sum: 79.12808102369308
time_elpased: 2.237
batch start
#iterations: 128
currently lose_sum: 80.09892466664314
time_elpased: 2.338
batch start
#iterations: 129
currently lose_sum: 78.73378601670265
time_elpased: 2.462
batch start
#iterations: 130
currently lose_sum: 80.36430057883263
time_elpased: 2.199
batch start
#iterations: 131
currently lose_sum: 79.68912190198898
time_elpased: 2.375
batch start
#iterations: 132
currently lose_sum: 79.30234533548355
time_elpased: 2.262
batch start
#iterations: 133
currently lose_sum: 79.40497595071793
time_elpased: 2.416
batch start
#iterations: 134
currently lose_sum: 79.69990411400795
time_elpased: 2.414
batch start
#iterations: 135
currently lose_sum: 79.70064035058022
time_elpased: 2.462
batch start
#iterations: 136
currently lose_sum: 79.24386239051819
time_elpased: 2.297
batch start
#iterations: 137
currently lose_sum: 79.17050623893738
time_elpased: 2.175
batch start
#iterations: 138
currently lose_sum: 79.49301040172577
time_elpased: 2.273
batch start
#iterations: 139
currently lose_sum: 78.25723984837532
time_elpased: 2.507
start validation test
0.641391752577
0.643712886759
0.635961300947
0.63981361636
0.641400724823
68.436
batch start
#iterations: 140
currently lose_sum: 79.41185837984085
time_elpased: 2.493
batch start
#iterations: 141
currently lose_sum: 78.90344843268394
time_elpased: 2.348
batch start
#iterations: 142
currently lose_sum: 78.52746120095253
time_elpased: 2.391
batch start
#iterations: 143
currently lose_sum: 78.94790232181549
time_elpased: 2.478
batch start
#iterations: 144
currently lose_sum: 79.46237432956696
time_elpased: 2.456
batch start
#iterations: 145
currently lose_sum: 77.98498079180717
time_elpased: 2.439
batch start
#iterations: 146
currently lose_sum: 79.00580072402954
time_elpased: 2.377
batch start
#iterations: 147
currently lose_sum: 78.76384034752846
time_elpased: 2.374
batch start
#iterations: 148
currently lose_sum: 78.74276587367058
time_elpased: 2.429
batch start
#iterations: 149
currently lose_sum: 78.90869361162186
time_elpased: 2.414
batch start
#iterations: 150
currently lose_sum: 78.59869903326035
time_elpased: 2.161
batch start
#iterations: 151
currently lose_sum: 79.1456869840622
time_elpased: 2.056
batch start
#iterations: 152
currently lose_sum: 78.45051142573357
time_elpased: 1.919
batch start
#iterations: 153
currently lose_sum: 78.74339032173157
time_elpased: 2.325
batch start
#iterations: 154
currently lose_sum: 78.74371021986008
time_elpased: 2.459
batch start
#iterations: 155
currently lose_sum: 78.14445692300797
time_elpased: 2.203
batch start
#iterations: 156
currently lose_sum: 78.26743939518929
time_elpased: 2.333
batch start
#iterations: 157
currently lose_sum: 78.27131694555283
time_elpased: 2.343
batch start
#iterations: 158
currently lose_sum: 78.58687949180603
time_elpased: 2.504
batch start
#iterations: 159
currently lose_sum: 78.3981739282608
time_elpased: 2.165
start validation test
0.642474226804
0.646531730972
0.631226842322
0.638787626289
0.642492809843
68.584
batch start
#iterations: 160
currently lose_sum: 78.87986090779305
time_elpased: 2.409
batch start
#iterations: 161
currently lose_sum: 78.11253049969673
time_elpased: 2.376
batch start
#iterations: 162
currently lose_sum: 78.53963214159012
time_elpased: 2.393
batch start
#iterations: 163
currently lose_sum: 78.43880718946457
time_elpased: 2.503
batch start
#iterations: 164
currently lose_sum: 77.5909711420536
time_elpased: 2.226
batch start
#iterations: 165
currently lose_sum: 78.10936170816422
time_elpased: 2.47
batch start
#iterations: 166
currently lose_sum: 77.75789541006088
time_elpased: 1.869
batch start
#iterations: 167
currently lose_sum: 77.72080740332603
time_elpased: 2.292
batch start
#iterations: 168
currently lose_sum: 77.803655564785
time_elpased: 2.207
batch start
#iterations: 169
currently lose_sum: 77.56581848859787
time_elpased: 2.37
batch start
#iterations: 170
currently lose_sum: 77.85462138056755
time_elpased: 2.238
batch start
#iterations: 171
currently lose_sum: 78.3893786072731
time_elpased: 2.498
batch start
#iterations: 172
currently lose_sum: 77.0535292327404
time_elpased: 2.571
batch start
#iterations: 173
currently lose_sum: 77.52352702617645
time_elpased: 2.421
batch start
#iterations: 174
currently lose_sum: 77.32900086045265
time_elpased: 2.421
batch start
#iterations: 175
currently lose_sum: 77.25704309344292
time_elpased: 2.447
batch start
#iterations: 176
currently lose_sum: 77.0025150179863
time_elpased: 2.423
batch start
#iterations: 177
currently lose_sum: 77.32148894667625
time_elpased: 2.278
batch start
#iterations: 178
currently lose_sum: 77.23187917470932
time_elpased: 2.262
batch start
#iterations: 179
currently lose_sum: 76.3971434533596
time_elpased: 2.144
start validation test
0.630824742268
0.639807313335
0.601482091396
0.620053050398
0.630873222484
72.094
batch start
#iterations: 180
currently lose_sum: 77.39810740947723
time_elpased: 2.311
batch start
#iterations: 181
currently lose_sum: 77.47580847144127
time_elpased: 2.346
batch start
#iterations: 182
currently lose_sum: 77.59258118271828
time_elpased: 2.292
batch start
#iterations: 183
currently lose_sum: 76.64132234454155
time_elpased: 2.185
batch start
#iterations: 184
currently lose_sum: 76.40465956926346
time_elpased: 2.364
batch start
#iterations: 185
currently lose_sum: 77.50915798544884
time_elpased: 2.208
batch start
#iterations: 186
currently lose_sum: 76.91214045882225
time_elpased: 2.26
batch start
#iterations: 187
currently lose_sum: 77.05670273303986
time_elpased: 2.423
batch start
#iterations: 188
currently lose_sum: 76.81864669919014
time_elpased: 2.162
batch start
#iterations: 189
currently lose_sum: 76.81913232803345
time_elpased: 2.275
batch start
#iterations: 190
currently lose_sum: 76.39227467775345
time_elpased: 1.901
batch start
#iterations: 191
currently lose_sum: 76.66425070166588
time_elpased: 1.993
batch start
#iterations: 192
currently lose_sum: 77.1479474902153
time_elpased: 1.796
batch start
#iterations: 193
currently lose_sum: 76.47046685218811
time_elpased: 1.807
batch start
#iterations: 194
currently lose_sum: 77.02610608935356
time_elpased: 1.989
batch start
#iterations: 195
currently lose_sum: 76.48045516014099
time_elpased: 1.904
batch start
#iterations: 196
currently lose_sum: 76.48642432689667
time_elpased: 2.351
batch start
#iterations: 197
currently lose_sum: 76.02949768304825
time_elpased: 2.172
batch start
#iterations: 198
currently lose_sum: 76.74118024110794
time_elpased: 2.185
batch start
#iterations: 199
currently lose_sum: 75.90588438510895
time_elpased: 2.338
start validation test
0.623144329897
0.636198890022
0.578118567312
0.605769749259
0.623218721904
72.083
batch start
#iterations: 200
currently lose_sum: 75.72297585010529
time_elpased: 2.511
batch start
#iterations: 201
currently lose_sum: 75.8892610669136
time_elpased: 1.827
batch start
#iterations: 202
currently lose_sum: 76.0807132422924
time_elpased: 2.211
batch start
#iterations: 203
currently lose_sum: 76.42934396862984
time_elpased: 2.051
batch start
#iterations: 204
currently lose_sum: 76.24210268259048
time_elpased: 2.032
batch start
#iterations: 205
currently lose_sum: 76.00574457645416
time_elpased: 2.378
batch start
#iterations: 206
currently lose_sum: 76.90882763266563
time_elpased: 2.479
batch start
#iterations: 207
currently lose_sum: 76.14584106206894
time_elpased: 2.242
batch start
#iterations: 208
currently lose_sum: 75.8632572889328
time_elpased: 2.42
batch start
#iterations: 209
currently lose_sum: 75.68586051464081
time_elpased: 2.346
batch start
#iterations: 210
currently lose_sum: 75.73432442545891
time_elpased: 2.371
batch start
#iterations: 211
currently lose_sum: 75.24206894636154
time_elpased: 2.171
batch start
#iterations: 212
currently lose_sum: 76.00935426354408
time_elpased: 2.436
batch start
#iterations: 213
currently lose_sum: 76.41679641604424
time_elpased: 2.488
batch start
#iterations: 214
currently lose_sum: 75.4901615679264
time_elpased: 2.582
batch start
#iterations: 215
currently lose_sum: 75.42029866576195
time_elpased: 2.62
batch start
#iterations: 216
currently lose_sum: 75.52620279788971
time_elpased: 2.373
batch start
#iterations: 217
currently lose_sum: 75.47978615760803
time_elpased: 2.369
batch start
#iterations: 218
currently lose_sum: 75.17153161764145
time_elpased: 2.56
batch start
#iterations: 219
currently lose_sum: 75.56779882311821
time_elpased: 2.382
start validation test
0.629381443299
0.642905634759
0.584808563195
0.612482483561
0.62945508705
74.680
batch start
#iterations: 220
currently lose_sum: 74.87977522611618
time_elpased: 2.554
batch start
#iterations: 221
currently lose_sum: 74.95012140274048
time_elpased: 2.406
batch start
#iterations: 222
currently lose_sum: 75.18626660108566
time_elpased: 2.466
batch start
#iterations: 223
currently lose_sum: 74.27561014890671
time_elpased: 2.556
batch start
#iterations: 224
currently lose_sum: 75.52878865599632
time_elpased: 2.624
batch start
#iterations: 225
currently lose_sum: 75.4398503601551
time_elpased: 2.439
batch start
#iterations: 226
currently lose_sum: 74.8317398428917
time_elpased: 1.865
batch start
#iterations: 227
currently lose_sum: 74.71411043405533
time_elpased: 2.342
batch start
#iterations: 228
currently lose_sum: 74.66723698377609
time_elpased: 2.589
batch start
#iterations: 229
currently lose_sum: 75.06275594234467
time_elpased: 2.187
batch start
#iterations: 230
currently lose_sum: 74.63653334975243
time_elpased: 2.396
batch start
#iterations: 231
currently lose_sum: 75.48681125044823
time_elpased: 2.484
batch start
#iterations: 232
currently lose_sum: 75.12208619713783
time_elpased: 1.97
batch start
#iterations: 233
currently lose_sum: 74.73468592762947
time_elpased: 2.062
batch start
#iterations: 234
currently lose_sum: 75.17665579915047
time_elpased: 2.389
batch start
#iterations: 235
currently lose_sum: 74.94526153802872
time_elpased: 2.4
batch start
#iterations: 236
currently lose_sum: 74.9673522412777
time_elpased: 2.566
batch start
#iterations: 237
currently lose_sum: 74.28201499581337
time_elpased: 2.491
batch start
#iterations: 238
currently lose_sum: 75.33835664391518
time_elpased: 2.365
batch start
#iterations: 239
currently lose_sum: 75.2465531527996
time_elpased: 2.34
start validation test
0.626701030928
0.630100967606
0.616611774393
0.623283395755
0.626717700497
73.254
batch start
#iterations: 240
currently lose_sum: 74.32346454262733
time_elpased: 2.049
batch start
#iterations: 241
currently lose_sum: 74.45694872736931
time_elpased: 2.499
batch start
#iterations: 242
currently lose_sum: 74.33515083789825
time_elpased: 1.997
batch start
#iterations: 243
currently lose_sum: 74.54825165867805
time_elpased: 2.125
batch start
#iterations: 244
currently lose_sum: 74.11730283498764
time_elpased: 2.142
batch start
#iterations: 245
currently lose_sum: 74.81825879216194
time_elpased: 2.076
batch start
#iterations: 246
currently lose_sum: 73.73181110620499
time_elpased: 2.242
batch start
#iterations: 247
currently lose_sum: 74.46703946590424
time_elpased: 2.336
batch start
#iterations: 248
currently lose_sum: 74.41434237360954
time_elpased: 2.459
batch start
#iterations: 249
currently lose_sum: 74.83141648769379
time_elpased: 1.915
batch start
#iterations: 250
currently lose_sum: 74.4035978615284
time_elpased: 2.38
batch start
#iterations: 251
currently lose_sum: 74.66649240255356
time_elpased: 2.39
batch start
#iterations: 252
currently lose_sum: 74.67850080132484
time_elpased: 2.408
batch start
#iterations: 253
currently lose_sum: 74.3551516532898
time_elpased: 2.453
batch start
#iterations: 254
currently lose_sum: 73.80401721596718
time_elpased: 2.354
batch start
#iterations: 255
currently lose_sum: 73.76912158727646
time_elpased: 2.429
batch start
#iterations: 256
currently lose_sum: 74.71372810006142
time_elpased: 2.411
batch start
#iterations: 257
currently lose_sum: 74.01297229528427
time_elpased: 2.156
batch start
#iterations: 258
currently lose_sum: 74.46238142251968
time_elpased: 2.08
batch start
#iterations: 259
currently lose_sum: 73.87708139419556
time_elpased: 2.307
start validation test
0.622422680412
0.640928916657
0.559592424866
0.597505357437
0.622526489178
77.903
batch start
#iterations: 260
currently lose_sum: 74.54596900939941
time_elpased: 2.1
batch start
#iterations: 261
currently lose_sum: 74.78065234422684
time_elpased: 2.52
batch start
#iterations: 262
currently lose_sum: 73.61897471547127
time_elpased: 2.026
batch start
#iterations: 263
currently lose_sum: 73.6070242524147
time_elpased: 1.918
batch start
#iterations: 264
currently lose_sum: 74.10631689429283
time_elpased: 2.269
batch start
#iterations: 265
currently lose_sum: 73.93403419852257
time_elpased: 2.375
batch start
#iterations: 266
currently lose_sum: 73.2456074655056
time_elpased: 2.338
batch start
#iterations: 267
currently lose_sum: 73.87930491566658
time_elpased: 2.234
batch start
#iterations: 268
currently lose_sum: 73.72168016433716
time_elpased: 2.078
batch start
#iterations: 269
currently lose_sum: 73.78728973865509
time_elpased: 2.411
batch start
#iterations: 270
currently lose_sum: 73.49698558449745
time_elpased: 2.362
batch start
#iterations: 271
currently lose_sum: 74.00757962465286
time_elpased: 2.283
batch start
#iterations: 272
currently lose_sum: 73.22317281365395
time_elpased: 2.598
batch start
#iterations: 273
currently lose_sum: 73.48427459597588
time_elpased: 2.356
batch start
#iterations: 274
currently lose_sum: 73.54925388097763
time_elpased: 2.441
batch start
#iterations: 275
currently lose_sum: 73.21238228678703
time_elpased: 2.275
batch start
#iterations: 276
currently lose_sum: 73.27797523140907
time_elpased: 2.04
batch start
#iterations: 277
currently lose_sum: 72.94361650943756
time_elpased: 2.409
batch start
#iterations: 278
currently lose_sum: 73.35158857703209
time_elpased: 2.612
batch start
#iterations: 279
currently lose_sum: 73.01565673947334
time_elpased: 2.201
start validation test
0.629175257732
0.657467532468
0.541889666529
0.594109681787
0.629319471844
89.073
batch start
#iterations: 280
currently lose_sum: 73.55416962504387
time_elpased: 2.561
batch start
#iterations: 281
currently lose_sum: 72.76778158545494
time_elpased: 2.347
batch start
#iterations: 282
currently lose_sum: 72.55137920379639
time_elpased: 2.317
batch start
#iterations: 283
currently lose_sum: 73.48271787166595
time_elpased: 2.338
batch start
#iterations: 284
currently lose_sum: 73.77060186862946
time_elpased: 2.453
batch start
#iterations: 285
currently lose_sum: 72.61342576146126
time_elpased: 2.387
batch start
#iterations: 286
currently lose_sum: 72.31962272524834
time_elpased: 2.112
batch start
#iterations: 287
currently lose_sum: 73.1684844493866
time_elpased: 2.334
batch start
#iterations: 288
currently lose_sum: 72.599578499794
time_elpased: 2.372
batch start
#iterations: 289
currently lose_sum: 72.90834337472916
time_elpased: 2.378
batch start
#iterations: 290
currently lose_sum: 72.9299528002739
time_elpased: 2.342
batch start
#iterations: 291
currently lose_sum: 73.03157684206963
time_elpased: 2.011
batch start
#iterations: 292
currently lose_sum: 72.63308176398277
time_elpased: 2.353
batch start
#iterations: 293
currently lose_sum: 72.9176913201809
time_elpased: 2.332
batch start
#iterations: 294
currently lose_sum: 71.9619308412075
time_elpased: 2.386
batch start
#iterations: 295
currently lose_sum: 72.8791269659996
time_elpased: 2.522
batch start
#iterations: 296
currently lose_sum: 72.98446354269981
time_elpased: 2.107
batch start
#iterations: 297
currently lose_sum: 73.11855000257492
time_elpased: 2.322
batch start
#iterations: 298
currently lose_sum: 72.19351291656494
time_elpased: 2.371
batch start
#iterations: 299
currently lose_sum: 72.66005140542984
time_elpased: 2.307
start validation test
0.626546391753
0.64499471893
0.565664882668
0.602730712288
0.62664698078
81.114
batch start
#iterations: 300
currently lose_sum: 72.10986948013306
time_elpased: 2.019
batch start
#iterations: 301
currently lose_sum: 72.43849319219589
time_elpased: 2.365
batch start
#iterations: 302
currently lose_sum: 71.60112366080284
time_elpased: 1.71
batch start
#iterations: 303
currently lose_sum: 72.15057525038719
time_elpased: 2.06
batch start
#iterations: 304
currently lose_sum: 73.03386640548706
time_elpased: 2.372
batch start
#iterations: 305
currently lose_sum: 71.63358446955681
time_elpased: 2.364
batch start
#iterations: 306
currently lose_sum: 72.10834071040154
time_elpased: 2.361
batch start
#iterations: 307
currently lose_sum: 73.21090289950371
time_elpased: 2.42
batch start
#iterations: 308
currently lose_sum: 72.47235837578773
time_elpased: 2.241
batch start
#iterations: 309
currently lose_sum: 71.48564288020134
time_elpased: 2.225
batch start
#iterations: 310
currently lose_sum: 71.95910426974297
time_elpased: 2.375
batch start
#iterations: 311
currently lose_sum: 72.51519173383713
time_elpased: 2.428
batch start
#iterations: 312
currently lose_sum: 72.02759391069412
time_elpased: 2.237
batch start
#iterations: 313
currently lose_sum: 72.23692086338997
time_elpased: 2.4
batch start
#iterations: 314
currently lose_sum: 72.15418004989624
time_elpased: 2.384
batch start
#iterations: 315
currently lose_sum: 71.92773297429085
time_elpased: 1.981
batch start
#iterations: 316
currently lose_sum: 71.86320453882217
time_elpased: 2.232
batch start
#iterations: 317
currently lose_sum: 71.98288032412529
time_elpased: 2.58
batch start
#iterations: 318
currently lose_sum: 71.93754258751869
time_elpased: 2.552
batch start
#iterations: 319
currently lose_sum: 72.19197764992714
time_elpased: 2.412
start validation test
0.617371134021
0.63553611538
0.553314121037
0.591581843191
0.617476969647
81.503
batch start
#iterations: 320
currently lose_sum: 72.29854890704155
time_elpased: 2.35
batch start
#iterations: 321
currently lose_sum: 71.78354534506798
time_elpased: 2.342
batch start
#iterations: 322
currently lose_sum: 72.18891736865044
time_elpased: 2.346
batch start
#iterations: 323
currently lose_sum: 71.40975132584572
time_elpased: 2.484
batch start
#iterations: 324
currently lose_sum: 71.72489872574806
time_elpased: 2.504
batch start
#iterations: 325
currently lose_sum: 71.73583942651749
time_elpased: 2.296
batch start
#iterations: 326
currently lose_sum: 71.75659200549126
time_elpased: 2.417
batch start
#iterations: 327
currently lose_sum: 71.079281270504
time_elpased: 2.261
batch start
#iterations: 328
currently lose_sum: 71.37633463740349
time_elpased: 2.31
batch start
#iterations: 329
currently lose_sum: 71.86928415298462
time_elpased: 2.291
batch start
#iterations: 330
currently lose_sum: 71.23857772350311
time_elpased: 2.319
batch start
#iterations: 331
currently lose_sum: 70.51260587573051
time_elpased: 2.385
batch start
#iterations: 332
currently lose_sum: 71.56812217831612
time_elpased: 2.461
batch start
#iterations: 333
currently lose_sum: 71.16092255711555
time_elpased: 2.016
batch start
#iterations: 334
currently lose_sum: 71.12927529215813
time_elpased: 2.165
batch start
#iterations: 335
currently lose_sum: 72.23341310024261
time_elpased: 1.994
batch start
#iterations: 336
currently lose_sum: 70.80423492193222
time_elpased: 1.694
batch start
#iterations: 337
currently lose_sum: 71.29358333349228
time_elpased: 1.737
batch start
#iterations: 338
currently lose_sum: 71.61888727545738
time_elpased: 1.792
batch start
#iterations: 339
currently lose_sum: 71.08101508021355
time_elpased: 1.719
start validation test
0.62675257732
0.655172413793
0.537772745986
0.59069583404
0.626899590672
94.445
batch start
#iterations: 340
currently lose_sum: 71.24850159883499
time_elpased: 2.13
batch start
#iterations: 341
currently lose_sum: 71.13839048147202
time_elpased: 2.554
batch start
#iterations: 342
currently lose_sum: 71.6987771987915
time_elpased: 2.611
batch start
#iterations: 343
currently lose_sum: 71.19272619485855
time_elpased: 2.535
batch start
#iterations: 344
currently lose_sum: 70.77366325259209
time_elpased: 2.315
batch start
#iterations: 345
currently lose_sum: 71.14664024114609
time_elpased: 2.128
batch start
#iterations: 346
currently lose_sum: 70.39527300000191
time_elpased: 2.209
batch start
#iterations: 347
currently lose_sum: 70.36028808355331
time_elpased: 2.359
batch start
#iterations: 348
currently lose_sum: 70.93828064203262
time_elpased: 1.838
batch start
#iterations: 349
currently lose_sum: 71.53763017058372
time_elpased: 2.516
batch start
#iterations: 350
currently lose_sum: 70.17432373762131
time_elpased: 2.276
batch start
#iterations: 351
currently lose_sum: 70.89894378185272
time_elpased: 2.421
batch start
#iterations: 352
currently lose_sum: 70.95703542232513
time_elpased: 2.668
batch start
#iterations: 353
currently lose_sum: 70.82822161912918
time_elpased: 2.488
batch start
#iterations: 354
currently lose_sum: 71.15317457914352
time_elpased: 2.507
batch start
#iterations: 355
currently lose_sum: 71.00697636604309
time_elpased: 2.367
batch start
#iterations: 356
currently lose_sum: 71.0194151699543
time_elpased: 2.289
batch start
#iterations: 357
currently lose_sum: 71.36784172058105
time_elpased: 2.536
batch start
#iterations: 358
currently lose_sum: 70.41878569126129
time_elpased: 2.643
batch start
#iterations: 359
currently lose_sum: 69.88040280342102
time_elpased: 2.451
start validation test
0.619742268041
0.642465586551
0.542815973652
0.588451882845
0.619869366421
88.995
batch start
#iterations: 360
currently lose_sum: 70.64730656147003
time_elpased: 2.124
batch start
#iterations: 361
currently lose_sum: 70.51527228951454
time_elpased: 2.06
batch start
#iterations: 362
currently lose_sum: 70.95912566781044
time_elpased: 2.351
batch start
#iterations: 363
currently lose_sum: 70.37595081329346
time_elpased: 2.413
batch start
#iterations: 364
currently lose_sum: 70.32135340571404
time_elpased: 2.431
batch start
#iterations: 365
currently lose_sum: 70.97042071819305
time_elpased: 2.257
batch start
#iterations: 366
currently lose_sum: 70.07766073942184
time_elpased: 2.258
batch start
#iterations: 367
currently lose_sum: 69.56005343794823
time_elpased: 2.338
batch start
#iterations: 368
currently lose_sum: 69.81632679700851
time_elpased: 2.407
batch start
#iterations: 369
currently lose_sum: 70.74117615818977
time_elpased: 2.418
batch start
#iterations: 370
currently lose_sum: 70.84082618355751
time_elpased: 2.125
batch start
#iterations: 371
currently lose_sum: 69.91691768169403
time_elpased: 2.208
batch start
#iterations: 372
currently lose_sum: 70.68794605135918
time_elpased: 2.357
batch start
#iterations: 373
currently lose_sum: 70.21136632561684
time_elpased: 2.317
batch start
#iterations: 374
currently lose_sum: 70.73364192247391
time_elpased: 2.372
batch start
#iterations: 375
currently lose_sum: 69.68200528621674
time_elpased: 2.358
batch start
#iterations: 376
currently lose_sum: 69.86455684900284
time_elpased: 2.003
batch start
#iterations: 377
currently lose_sum: 69.49418726563454
time_elpased: 2.283
batch start
#iterations: 378
currently lose_sum: 69.90260317921638
time_elpased: 2.479
batch start
#iterations: 379
currently lose_sum: 70.11333593726158
time_elpased: 2.427
start validation test
0.616082474227
0.646322580645
0.515541375051
0.573571510363
0.616248589219
91.615
batch start
#iterations: 380
currently lose_sum: 69.8022213280201
time_elpased: 2.301
batch start
#iterations: 381
currently lose_sum: 69.5723924934864
time_elpased: 2.354
batch start
#iterations: 382
currently lose_sum: 70.86507171392441
time_elpased: 2.134
batch start
#iterations: 383
currently lose_sum: 69.36273348331451
time_elpased: 2.463
batch start
#iterations: 384
currently lose_sum: 69.1374391913414
time_elpased: 2.267
batch start
#iterations: 385
currently lose_sum: 69.54034176468849
time_elpased: 2.072
batch start
#iterations: 386
currently lose_sum: 70.0929499566555
time_elpased: 2.299
batch start
#iterations: 387
currently lose_sum: 69.24834129214287
time_elpased: 2.374
batch start
#iterations: 388
currently lose_sum: 69.60429441928864
time_elpased: 2.48
batch start
#iterations: 389
currently lose_sum: 69.836590975523
time_elpased: 2.302
batch start
#iterations: 390
currently lose_sum: 69.71065324544907
time_elpased: 2.319
batch start
#iterations: 391
currently lose_sum: 69.07101425528526
time_elpased: 2.278
batch start
#iterations: 392
currently lose_sum: 69.1608918607235
time_elpased: 2.037
batch start
#iterations: 393
currently lose_sum: 69.12046226859093
time_elpased: 2.176
batch start
#iterations: 394
currently lose_sum: 69.01796707510948
time_elpased: 2.157
batch start
#iterations: 395
currently lose_sum: 69.88903206586838
time_elpased: 2.673
batch start
#iterations: 396
currently lose_sum: 69.34408885240555
time_elpased: 2.429
batch start
#iterations: 397
currently lose_sum: 68.63080450892448
time_elpased: 2.368
batch start
#iterations: 398
currently lose_sum: 69.13826715946198
time_elpased: 2.384
batch start
#iterations: 399
currently lose_sum: 68.79363924264908
time_elpased: 2.299
start validation test
0.620463917526
0.647931598139
0.530362289008
0.583281453393
0.620612784322
92.599
acc: 0.669
pre: 0.651
rec: 0.731
F1: 0.689
auc: 0.724
