start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 99.92721527814865
time_elpased: 3.465
batch start
#iterations: 1
currently lose_sum: 98.81847375631332
time_elpased: 2.875
batch start
#iterations: 2
currently lose_sum: 98.42301070690155
time_elpased: 3.064
batch start
#iterations: 3
currently lose_sum: 97.85182899236679
time_elpased: 3.375
batch start
#iterations: 4
currently lose_sum: 97.58163839578629
time_elpased: 3.273
batch start
#iterations: 5
currently lose_sum: 97.11953592300415
time_elpased: 3.118
batch start
#iterations: 6
currently lose_sum: 96.26967471837997
time_elpased: 3.378
batch start
#iterations: 7
currently lose_sum: 96.43760699033737
time_elpased: 3.288
batch start
#iterations: 8
currently lose_sum: 95.9358338713646
time_elpased: 3.479
batch start
#iterations: 9
currently lose_sum: 95.62576025724411
time_elpased: 4.942
batch start
#iterations: 10
currently lose_sum: 95.46633160114288
time_elpased: 18.222
batch start
#iterations: 11
currently lose_sum: 95.34075218439102
time_elpased: 2.123
batch start
#iterations: 12
currently lose_sum: 94.82720601558685
time_elpased: 2.56
batch start
#iterations: 13
currently lose_sum: 94.30490392446518
time_elpased: 2.564
batch start
#iterations: 14
currently lose_sum: 93.9924709200859
time_elpased: 2.406
batch start
#iterations: 15
currently lose_sum: 93.88778275251389
time_elpased: 2.179
batch start
#iterations: 16
currently lose_sum: 93.71219658851624
time_elpased: 2.567
batch start
#iterations: 17
currently lose_sum: 93.55982983112335
time_elpased: 2.735
batch start
#iterations: 18
currently lose_sum: 92.88195580244064
time_elpased: 2.356
batch start
#iterations: 19
currently lose_sum: 92.95149874687195
time_elpased: 2.64
start validation test
0.651546391753
0.670984156355
0.597097869713
0.631888477456
0.651641984531
60.588
batch start
#iterations: 20
currently lose_sum: 92.71321195363998
time_elpased: 2.59
batch start
#iterations: 21
currently lose_sum: 92.39203453063965
time_elpased: 2.873
batch start
#iterations: 22
currently lose_sum: 92.3891686797142
time_elpased: 3.029
batch start
#iterations: 23
currently lose_sum: 92.13837319612503
time_elpased: 2.924
batch start
#iterations: 24
currently lose_sum: 91.27765864133835
time_elpased: 2.93
batch start
#iterations: 25
currently lose_sum: 91.30882126092911
time_elpased: 2.967
batch start
#iterations: 26
currently lose_sum: 91.06482338905334
time_elpased: 3.484
batch start
#iterations: 27
currently lose_sum: 90.76022082567215
time_elpased: 3.528
batch start
#iterations: 28
currently lose_sum: 90.35359567403793
time_elpased: 2.921
batch start
#iterations: 29
currently lose_sum: 90.88597470521927
time_elpased: 2.512
batch start
#iterations: 30
currently lose_sum: 89.97764617204666
time_elpased: 2.494
batch start
#iterations: 31
currently lose_sum: 89.6270180940628
time_elpased: 2.609
batch start
#iterations: 32
currently lose_sum: 89.78059375286102
time_elpased: 2.755
batch start
#iterations: 33
currently lose_sum: 89.89153057336807
time_elpased: 2.79
batch start
#iterations: 34
currently lose_sum: 89.62716734409332
time_elpased: 3.132
batch start
#iterations: 35
currently lose_sum: 89.57557970285416
time_elpased: 3.048
batch start
#iterations: 36
currently lose_sum: 88.69030117988586
time_elpased: 2.548
batch start
#iterations: 37
currently lose_sum: 88.8188009262085
time_elpased: 3.02
batch start
#iterations: 38
currently lose_sum: 88.29520225524902
time_elpased: 2.391
batch start
#iterations: 39
currently lose_sum: 87.8890580534935
time_elpased: 2.701
start validation test
0.644793814433
0.676184538653
0.558094061953
0.611490105429
0.644946029221
60.368
batch start
#iterations: 40
currently lose_sum: 88.43272662162781
time_elpased: 2.691
batch start
#iterations: 41
currently lose_sum: 87.5352812409401
time_elpased: 2.571
batch start
#iterations: 42
currently lose_sum: 87.46515566110611
time_elpased: 2.611
batch start
#iterations: 43
currently lose_sum: 87.31497371196747
time_elpased: 2.699
batch start
#iterations: 44
currently lose_sum: 87.67035984992981
time_elpased: 2.839
batch start
#iterations: 45
currently lose_sum: 87.01393860578537
time_elpased: 3.108
batch start
#iterations: 46
currently lose_sum: 86.84844404459
time_elpased: 3.532
batch start
#iterations: 47
currently lose_sum: 86.93185698986053
time_elpased: 3.671
batch start
#iterations: 48
currently lose_sum: 87.27328097820282
time_elpased: 3.851
batch start
#iterations: 49
currently lose_sum: 86.57295495271683
time_elpased: 3.874
batch start
#iterations: 50
currently lose_sum: 86.894755423069
time_elpased: 3.871
batch start
#iterations: 51
currently lose_sum: 85.8939705491066
time_elpased: 3.772
batch start
#iterations: 52
currently lose_sum: 86.12232655286789
time_elpased: 3.408
batch start
#iterations: 53
currently lose_sum: 86.53723645210266
time_elpased: 3.492
batch start
#iterations: 54
currently lose_sum: 85.28865694999695
time_elpased: 3.301
batch start
#iterations: 55
currently lose_sum: 85.01186901330948
time_elpased: 3.84
batch start
#iterations: 56
currently lose_sum: 85.42476391792297
time_elpased: 3.518
batch start
#iterations: 57
currently lose_sum: 85.02577131986618
time_elpased: 3.106
batch start
#iterations: 58
currently lose_sum: 85.09133809804916
time_elpased: 3.575
batch start
#iterations: 59
currently lose_sum: 85.0474870800972
time_elpased: 3.291
start validation test
0.632113402062
0.677441540578
0.506843676032
0.579855183376
0.632333332387
61.087
batch start
#iterations: 60
currently lose_sum: 84.2834746837616
time_elpased: 4.275
batch start
#iterations: 61
currently lose_sum: 84.84337800741196
time_elpased: 3.637
batch start
#iterations: 62
currently lose_sum: 84.5835970044136
time_elpased: 3.517
batch start
#iterations: 63
currently lose_sum: 84.49432444572449
time_elpased: 3.762
batch start
#iterations: 64
currently lose_sum: 83.9587168097496
time_elpased: 3.511
batch start
#iterations: 65
currently lose_sum: 83.4791334271431
time_elpased: 4.061
batch start
#iterations: 66
currently lose_sum: 84.15293264389038
time_elpased: 3.574
batch start
#iterations: 67
currently lose_sum: 83.31742691993713
time_elpased: 3.641
batch start
#iterations: 68
currently lose_sum: 84.09806454181671
time_elpased: 3.622
batch start
#iterations: 69
currently lose_sum: 83.56443339586258
time_elpased: 3.648
batch start
#iterations: 70
currently lose_sum: 83.2249094247818
time_elpased: 3.764
batch start
#iterations: 71
currently lose_sum: 83.56501668691635
time_elpased: 3.769
batch start
#iterations: 72
currently lose_sum: 83.03732490539551
time_elpased: 3.621
batch start
#iterations: 73
currently lose_sum: 82.7057945728302
time_elpased: 3.757
batch start
#iterations: 74
currently lose_sum: 82.68645507097244
time_elpased: 3.242
batch start
#iterations: 75
currently lose_sum: 82.45732343196869
time_elpased: 3.582
batch start
#iterations: 76
currently lose_sum: 82.56262481212616
time_elpased: 3.527
batch start
#iterations: 77
currently lose_sum: 82.40411865711212
time_elpased: 3.446
batch start
#iterations: 78
currently lose_sum: 82.3826271891594
time_elpased: 3.412
batch start
#iterations: 79
currently lose_sum: 81.94517394900322
time_elpased: 3.253
start validation test
0.627371134021
0.658430973001
0.532057219306
0.588536627013
0.627538472299
61.840
batch start
#iterations: 80
currently lose_sum: 81.95519107580185
time_elpased: 3.47
batch start
#iterations: 81
currently lose_sum: 82.21992063522339
time_elpased: 3.348
batch start
#iterations: 82
currently lose_sum: 82.4328281879425
time_elpased: 3.533
batch start
#iterations: 83
currently lose_sum: 81.58198922872543
time_elpased: 4.061
batch start
#iterations: 84
currently lose_sum: 81.72371253371239
time_elpased: 3.846
batch start
#iterations: 85
currently lose_sum: 81.99604132771492
time_elpased: 3.647
batch start
#iterations: 86
currently lose_sum: 81.70528364181519
time_elpased: 3.42
batch start
#iterations: 87
currently lose_sum: 81.29718413949013
time_elpased: 3.295
batch start
#iterations: 88
currently lose_sum: 80.91869983077049
time_elpased: 3.729
batch start
#iterations: 89
currently lose_sum: 81.03535103797913
time_elpased: 3.497
batch start
#iterations: 90
currently lose_sum: 81.3840149641037
time_elpased: 3.696
batch start
#iterations: 91
currently lose_sum: 80.47188973426819
time_elpased: 3.531
batch start
#iterations: 92
currently lose_sum: 80.77308389544487
time_elpased: 2.919
batch start
#iterations: 93
currently lose_sum: 80.329525411129
time_elpased: 3.44
batch start
#iterations: 94
currently lose_sum: 80.31906905770302
time_elpased: 3.465
batch start
#iterations: 95
currently lose_sum: 80.4502347111702
time_elpased: 3.686
batch start
#iterations: 96
currently lose_sum: 80.47566518187523
time_elpased: 3.636
batch start
#iterations: 97
currently lose_sum: 79.92755752801895
time_elpased: 3.227
batch start
#iterations: 98
currently lose_sum: 79.80982160568237
time_elpased: 3.348
batch start
#iterations: 99
currently lose_sum: 80.28779298067093
time_elpased: 3.605
start validation test
0.613865979381
0.671865348981
0.447771946074
0.53739270055
0.614157583075
63.769
batch start
#iterations: 100
currently lose_sum: 79.06614968180656
time_elpased: 3.817
batch start
#iterations: 101
currently lose_sum: 79.80517667531967
time_elpased: 3.506
batch start
#iterations: 102
currently lose_sum: 79.23132684826851
time_elpased: 3.736
batch start
#iterations: 103
currently lose_sum: 79.19202595949173
time_elpased: 3.792
batch start
#iterations: 104
currently lose_sum: 79.67731913924217
time_elpased: 3.341
batch start
#iterations: 105
currently lose_sum: 79.40758928656578
time_elpased: 3.74
batch start
#iterations: 106
currently lose_sum: 79.00604036450386
time_elpased: 3.149
batch start
#iterations: 107
currently lose_sum: 78.7855831682682
time_elpased: 3.599
batch start
#iterations: 108
currently lose_sum: 79.52730691432953
time_elpased: 3.254
batch start
#iterations: 109
currently lose_sum: 79.05705213546753
time_elpased: 3.398
batch start
#iterations: 110
currently lose_sum: 78.17527198791504
time_elpased: 3.399
batch start
#iterations: 111
currently lose_sum: 78.36369994282722
time_elpased: 3.555
batch start
#iterations: 112
currently lose_sum: 78.8731127679348
time_elpased: 3.47
batch start
#iterations: 113
currently lose_sum: 78.55268877744675
time_elpased: 3.66
batch start
#iterations: 114
currently lose_sum: 78.145224660635
time_elpased: 3.692
batch start
#iterations: 115
currently lose_sum: 77.66428419947624
time_elpased: 3.803
batch start
#iterations: 116
currently lose_sum: 78.12437736988068
time_elpased: 3.468
batch start
#iterations: 117
currently lose_sum: 77.84061861038208
time_elpased: 3.388
batch start
#iterations: 118
currently lose_sum: 77.56846761703491
time_elpased: 3.272
batch start
#iterations: 119
currently lose_sum: 77.86770525574684
time_elpased: 3.889
start validation test
0.619536082474
0.659214830971
0.497684470516
0.567172931449
0.619750011773
63.421
batch start
#iterations: 120
currently lose_sum: 78.02166989445686
time_elpased: 3.61
batch start
#iterations: 121
currently lose_sum: 77.33555191755295
time_elpased: 3.575
batch start
#iterations: 122
currently lose_sum: 77.31841838359833
time_elpased: 3.501
batch start
#iterations: 123
currently lose_sum: 77.39225545525551
time_elpased: 3.709
batch start
#iterations: 124
currently lose_sum: 77.25193935632706
time_elpased: 3.945
batch start
#iterations: 125
currently lose_sum: 77.58036214113235
time_elpased: 3.74
batch start
#iterations: 126
currently lose_sum: 76.8381561934948
time_elpased: 3.948
batch start
#iterations: 127
currently lose_sum: 77.25894409418106
time_elpased: 3.736
batch start
#iterations: 128
currently lose_sum: 76.66226518154144
time_elpased: 2.883
batch start
#iterations: 129
currently lose_sum: 76.08301442861557
time_elpased: 3.329
batch start
#iterations: 130
currently lose_sum: 76.72135552763939
time_elpased: 3.845
batch start
#iterations: 131
currently lose_sum: 76.19764092564583
time_elpased: 3.798
batch start
#iterations: 132
currently lose_sum: 76.84746551513672
time_elpased: 3.945
batch start
#iterations: 133
currently lose_sum: 75.84297007322311
time_elpased: 3.7
batch start
#iterations: 134
currently lose_sum: 76.14990064501762
time_elpased: 3.826
batch start
#iterations: 135
currently lose_sum: 76.13187000155449
time_elpased: 3.481
batch start
#iterations: 136
currently lose_sum: 76.4221453666687
time_elpased: 3.538
batch start
#iterations: 137
currently lose_sum: 76.21991068124771
time_elpased: 3.526
batch start
#iterations: 138
currently lose_sum: 75.56600821018219
time_elpased: 3.692
batch start
#iterations: 139
currently lose_sum: 75.7654404938221
time_elpased: 2.52
start validation test
0.626958762887
0.655310621242
0.538437789441
0.591153042201
0.627114175109
63.532
batch start
#iterations: 140
currently lose_sum: 76.18140131235123
time_elpased: 16.894
batch start
#iterations: 141
currently lose_sum: 75.57172870635986
time_elpased: 4.142
batch start
#iterations: 142
currently lose_sum: 76.44282805919647
time_elpased: 3.103
batch start
#iterations: 143
currently lose_sum: 75.48130643367767
time_elpased: 3.054
batch start
#iterations: 144
currently lose_sum: 76.20126977562904
time_elpased: 3.172
batch start
#iterations: 145
currently lose_sum: 75.7541201710701
time_elpased: 2.988
batch start
#iterations: 146
currently lose_sum: 74.6466019153595
time_elpased: 2.802
batch start
#iterations: 147
currently lose_sum: 75.63610309362411
time_elpased: 1.997
batch start
#iterations: 148
currently lose_sum: 75.04231169819832
time_elpased: 1.522
batch start
#iterations: 149
currently lose_sum: 75.37714302539825
time_elpased: 1.51
batch start
#iterations: 150
currently lose_sum: 75.19246366620064
time_elpased: 1.747
batch start
#iterations: 151
currently lose_sum: 75.15724295377731
time_elpased: 3.611
batch start
#iterations: 152
currently lose_sum: 75.63158276677132
time_elpased: 4.358
batch start
#iterations: 153
currently lose_sum: 74.94577115774155
time_elpased: 3.376
batch start
#iterations: 154
currently lose_sum: 75.1264716386795
time_elpased: 2.792
batch start
#iterations: 155
currently lose_sum: 75.05062609910965
time_elpased: 4.298
batch start
#iterations: 156
currently lose_sum: 75.12934449315071
time_elpased: 2.062
batch start
#iterations: 157
currently lose_sum: 74.57964825630188
time_elpased: 1.641
batch start
#iterations: 158
currently lose_sum: 75.27971982955933
time_elpased: 2.552
batch start
#iterations: 159
currently lose_sum: 74.28533923625946
time_elpased: 2.804
start validation test
0.60912371134
0.656359906213
0.46094473603
0.541563387945
0.609383862386
66.233
batch start
#iterations: 160
currently lose_sum: 74.59517708420753
time_elpased: 4.004
batch start
#iterations: 161
currently lose_sum: 73.76751464605331
time_elpased: 1.753
batch start
#iterations: 162
currently lose_sum: 74.36857822537422
time_elpased: 2.286
batch start
#iterations: 163
currently lose_sum: 73.83802261948586
time_elpased: 2.322
batch start
#iterations: 164
currently lose_sum: 74.50956133008003
time_elpased: 1.672
batch start
#iterations: 165
currently lose_sum: 73.8155859708786
time_elpased: 2.099
batch start
#iterations: 166
currently lose_sum: 74.24435025453568
time_elpased: 4.567
batch start
#iterations: 167
currently lose_sum: 73.55263340473175
time_elpased: 1.75
batch start
#iterations: 168
currently lose_sum: 73.71692278981209
time_elpased: 24.099
batch start
#iterations: 169
currently lose_sum: 73.32746362686157
time_elpased: 2.345
batch start
#iterations: 170
currently lose_sum: 73.39937943220139
time_elpased: 2.274
batch start
#iterations: 171
currently lose_sum: 73.4374215900898
time_elpased: 2.582
batch start
#iterations: 172
currently lose_sum: 73.29534673690796
time_elpased: 2.513
batch start
#iterations: 173
currently lose_sum: 73.3660651743412
time_elpased: 3.101
batch start
#iterations: 174
currently lose_sum: 73.65363216400146
time_elpased: 2.331
batch start
#iterations: 175
currently lose_sum: 73.86715695261955
time_elpased: 2.587
batch start
#iterations: 176
currently lose_sum: 72.88623467087746
time_elpased: 2.165
batch start
#iterations: 177
currently lose_sum: 72.55482840538025
time_elpased: 2.467
batch start
#iterations: 178
currently lose_sum: 73.16826540231705
time_elpased: 2.127
batch start
#iterations: 179
currently lose_sum: 73.29293030500412
time_elpased: 2.01
start validation test
0.592680412371
0.666117517847
0.374498301945
0.479446640316
0.593063464718
69.315
batch start
#iterations: 180
currently lose_sum: 73.59387668967247
time_elpased: 1.85
batch start
#iterations: 181
currently lose_sum: 72.16883319616318
time_elpased: 2.174
batch start
#iterations: 182
currently lose_sum: 72.2641966342926
time_elpased: 2.121
batch start
#iterations: 183
currently lose_sum: 72.15026634931564
time_elpased: 2.142
batch start
#iterations: 184
currently lose_sum: 73.06622326374054
time_elpased: 2.3
batch start
#iterations: 185
currently lose_sum: 72.63325920701027
time_elpased: 1.843
batch start
#iterations: 186
currently lose_sum: 73.02062356472015
time_elpased: 1.817
batch start
#iterations: 187
currently lose_sum: 72.83786019682884
time_elpased: 1.601
batch start
#iterations: 188
currently lose_sum: 72.61488071084023
time_elpased: 3.21
batch start
#iterations: 189
currently lose_sum: 72.314637362957
time_elpased: 12.096
batch start
#iterations: 190
currently lose_sum: 73.06437155604362
time_elpased: 2.305
batch start
#iterations: 191
currently lose_sum: 72.00650763511658
time_elpased: 2.306
batch start
#iterations: 192
currently lose_sum: 72.4284899532795
time_elpased: 1.932
batch start
#iterations: 193
currently lose_sum: 72.31441527605057
time_elpased: 1.815
batch start
#iterations: 194
currently lose_sum: 71.89291015267372
time_elpased: 2.184
batch start
#iterations: 195
currently lose_sum: 72.10277983546257
time_elpased: 1.6
batch start
#iterations: 196
currently lose_sum: 72.05328878760338
time_elpased: 1.812
batch start
#iterations: 197
currently lose_sum: 71.74245175719261
time_elpased: 1.826
batch start
#iterations: 198
currently lose_sum: 71.5908368229866
time_elpased: 1.887
batch start
#iterations: 199
currently lose_sum: 71.22711017727852
time_elpased: 2.088
start validation test
0.593041237113
0.66158212132
0.383863332304
0.485835232823
0.593408481189
69.656
batch start
#iterations: 200
currently lose_sum: 71.33758771419525
time_elpased: 1.964
batch start
#iterations: 201
currently lose_sum: 71.50656533241272
time_elpased: 1.826
batch start
#iterations: 202
currently lose_sum: 72.28567823767662
time_elpased: 2.136
batch start
#iterations: 203
currently lose_sum: 71.60963401198387
time_elpased: 2.523
batch start
#iterations: 204
currently lose_sum: 71.55905479192734
time_elpased: 3.112
batch start
#iterations: 205
currently lose_sum: 70.98602989315987
time_elpased: 2.889
batch start
#iterations: 206
currently lose_sum: 71.94812643527985
time_elpased: 2.735
batch start
#iterations: 207
currently lose_sum: 70.96076169610023
time_elpased: 2.813
batch start
#iterations: 208
currently lose_sum: 71.4716046154499
time_elpased: 2.82
batch start
#iterations: 209
currently lose_sum: 70.74724772572517
time_elpased: 2.396
batch start
#iterations: 210
currently lose_sum: 71.27182444930077
time_elpased: 3.239
batch start
#iterations: 211
currently lose_sum: 70.51883620023727
time_elpased: 2.637
batch start
#iterations: 212
currently lose_sum: 71.42263680696487
time_elpased: 2.638
batch start
#iterations: 213
currently lose_sum: 70.96968364715576
time_elpased: 2.621
batch start
#iterations: 214
currently lose_sum: 70.95455706119537
time_elpased: 2.291
batch start
#iterations: 215
currently lose_sum: 70.90954932570457
time_elpased: 2.499
batch start
#iterations: 216
currently lose_sum: 70.58338275551796
time_elpased: 2.783
batch start
#iterations: 217
currently lose_sum: 70.82550522685051
time_elpased: 2.818
batch start
#iterations: 218
currently lose_sum: 70.71165320277214
time_elpased: 2.745
batch start
#iterations: 219
currently lose_sum: 70.77986827492714
time_elpased: 1.628
start validation test
0.60087628866
0.664884731039
0.409591437687
0.506909507737
0.601212118719
69.701
batch start
#iterations: 220
currently lose_sum: 70.03812956809998
time_elpased: 16.662
batch start
#iterations: 221
currently lose_sum: 70.41231805086136
time_elpased: 2.794
batch start
#iterations: 222
currently lose_sum: 70.42128619551659
time_elpased: 125.14
batch start
#iterations: 223
currently lose_sum: 69.90923371911049
time_elpased: 2.051
batch start
#iterations: 224
currently lose_sum: 70.30974918603897
time_elpased: 1.895
batch start
#iterations: 225
currently lose_sum: 70.90892133116722
time_elpased: 2.072
batch start
#iterations: 226
currently lose_sum: 69.90749615430832
time_elpased: 2.248
batch start
#iterations: 227
currently lose_sum: 69.97511491179466
time_elpased: 2.361
batch start
#iterations: 228
currently lose_sum: 70.04185557365417
time_elpased: 2.999
batch start
#iterations: 229
currently lose_sum: 70.40955418348312
time_elpased: 2.714
batch start
#iterations: 230
currently lose_sum: 70.119684278965
time_elpased: 2.837
batch start
#iterations: 231
currently lose_sum: 70.64779743552208
time_elpased: 3.177
batch start
#iterations: 232
currently lose_sum: 70.09998553991318
time_elpased: 3.049
batch start
#iterations: 233
currently lose_sum: 70.47526451945305
time_elpased: 2.772
batch start
#iterations: 234
currently lose_sum: 69.60374993085861
time_elpased: 2.877
batch start
#iterations: 235
currently lose_sum: 69.86426737904549
time_elpased: 2.892
batch start
#iterations: 236
currently lose_sum: 69.8395086824894
time_elpased: 2.903
batch start
#iterations: 237
currently lose_sum: 69.26366299390793
time_elpased: 2.656
batch start
#iterations: 238
currently lose_sum: 69.48654145002365
time_elpased: 2.744
batch start
#iterations: 239
currently lose_sum: 69.45697093009949
time_elpased: 2.966
start validation test
0.602164948454
0.644876068996
0.457857363384
0.53550794415
0.602418302677
69.233
batch start
#iterations: 240
currently lose_sum: 70.22959014773369
time_elpased: 3.21
batch start
#iterations: 241
currently lose_sum: 68.9439072906971
time_elpased: 3.601
batch start
#iterations: 242
currently lose_sum: 69.12829175591469
time_elpased: 3.219
batch start
#iterations: 243
currently lose_sum: 69.72540137171745
time_elpased: 3.471
batch start
#iterations: 244
currently lose_sum: 69.48203760385513
time_elpased: 3.399
batch start
#iterations: 245
currently lose_sum: 68.71614599227905
time_elpased: 3.338
batch start
#iterations: 246
currently lose_sum: 68.4805788397789
time_elpased: 3.299
batch start
#iterations: 247
currently lose_sum: 69.30368053913116
time_elpased: 3.32
batch start
#iterations: 248
currently lose_sum: 68.95586481690407
time_elpased: 3.3
batch start
#iterations: 249
currently lose_sum: 68.41279247403145
time_elpased: 3.492
batch start
#iterations: 250
currently lose_sum: 68.58946138620377
time_elpased: 3.191
batch start
#iterations: 251
currently lose_sum: 69.27773615717888
time_elpased: 3.169
batch start
#iterations: 252
currently lose_sum: 68.98318621516228
time_elpased: 3.148
batch start
#iterations: 253
currently lose_sum: 68.43680810928345
time_elpased: 3.098
batch start
#iterations: 254
currently lose_sum: 68.90360423922539
time_elpased: 3.134
batch start
#iterations: 255
currently lose_sum: 68.57891964912415
time_elpased: 3.136
batch start
#iterations: 256
currently lose_sum: 68.77176347374916
time_elpased: 3.255
batch start
#iterations: 257
currently lose_sum: 68.44788897037506
time_elpased: 2.958
batch start
#iterations: 258
currently lose_sum: 68.41996088624
time_elpased: 2.985
batch start
#iterations: 259
currently lose_sum: 69.05730250477791
time_elpased: 3.094
start validation test
0.580515463918
0.654410326618
0.344344962437
0.451247471342
0.58093009766
73.884
batch start
#iterations: 260
currently lose_sum: 67.80400401353836
time_elpased: 3.371
batch start
#iterations: 261
currently lose_sum: 69.05461716651917
time_elpased: 3.301
batch start
#iterations: 262
currently lose_sum: 68.19163462519646
time_elpased: 3.07
batch start
#iterations: 263
currently lose_sum: 67.55089327692986
time_elpased: 3.186
batch start
#iterations: 264
currently lose_sum: 68.57329171895981
time_elpased: 3.74
batch start
#iterations: 265
currently lose_sum: 68.68446108698845
time_elpased: 3.932
batch start
#iterations: 266
currently lose_sum: 68.68831673264503
time_elpased: 88.783
batch start
#iterations: 267
currently lose_sum: 68.53044727444649
time_elpased: 10.001
batch start
#iterations: 268
currently lose_sum: 68.75041207671165
time_elpased: 5.458
batch start
#iterations: 269
currently lose_sum: 68.47411486506462
time_elpased: 2.6
batch start
#iterations: 270
currently lose_sum: 68.66855356097221
time_elpased: 2.099
batch start
#iterations: 271
currently lose_sum: 68.25012019276619
time_elpased: 2.327
batch start
#iterations: 272
currently lose_sum: 68.21059659123421
time_elpased: 1.928
batch start
#iterations: 273
currently lose_sum: 67.89123266935349
time_elpased: 2.278
batch start
#iterations: 274
currently lose_sum: 68.88159185647964
time_elpased: 2.401
batch start
#iterations: 275
currently lose_sum: 68.00091657042503
time_elpased: 2.231
batch start
#iterations: 276
currently lose_sum: 67.50150871276855
time_elpased: 1.884
batch start
#iterations: 277
currently lose_sum: 67.98436939716339
time_elpased: 1.785
batch start
#iterations: 278
currently lose_sum: 67.79718464612961
time_elpased: 1.846
batch start
#iterations: 279
currently lose_sum: 67.14893099665642
time_elpased: 1.673
start validation test
0.58881443299
0.660280029477
0.368838118761
0.473291515352
0.589200635338
73.907
batch start
#iterations: 280
currently lose_sum: 67.50700256228447
time_elpased: 1.531
batch start
#iterations: 281
currently lose_sum: 67.4318009018898
time_elpased: 1.775
batch start
#iterations: 282
currently lose_sum: 67.79935112595558
time_elpased: 1.519
batch start
#iterations: 283
currently lose_sum: 68.251779794693
time_elpased: 2.362
batch start
#iterations: 284
currently lose_sum: 66.67111110687256
time_elpased: 1.49
batch start
#iterations: 285
currently lose_sum: 67.25011596083641
time_elpased: 1.555
batch start
#iterations: 286
currently lose_sum: 67.27946081757545
time_elpased: 1.738
batch start
#iterations: 287
currently lose_sum: 67.35360658168793
time_elpased: 1.584
batch start
#iterations: 288
currently lose_sum: 67.22037011384964
time_elpased: 1.818
batch start
#iterations: 289
currently lose_sum: 66.72756499052048
time_elpased: 1.56
batch start
#iterations: 290
currently lose_sum: 67.77602759003639
time_elpased: 2.033
batch start
#iterations: 291
currently lose_sum: 67.48382195830345
time_elpased: 2.179
batch start
#iterations: 292
currently lose_sum: 67.81633132696152
time_elpased: 2.841
batch start
#iterations: 293
currently lose_sum: 66.52708068490028
time_elpased: 2.961
batch start
#iterations: 294
currently lose_sum: 67.18987873196602
time_elpased: 2.801
batch start
#iterations: 295
currently lose_sum: 67.1426557302475
time_elpased: 2.982
batch start
#iterations: 296
currently lose_sum: 67.09255966544151
time_elpased: 2.848
batch start
#iterations: 297
currently lose_sum: 67.96586090326309
time_elpased: 2.964
batch start
#iterations: 298
currently lose_sum: 67.26718509197235
time_elpased: 2.91
batch start
#iterations: 299
currently lose_sum: 66.97870260477066
time_elpased: 2.433
start validation test
0.577113402062
0.66244363324
0.317484820418
0.429247251983
0.577569220082
76.585
batch start
#iterations: 300
currently lose_sum: 66.88765120506287
time_elpased: 3.166
batch start
#iterations: 301
currently lose_sum: 66.95544815063477
time_elpased: 3.143
batch start
#iterations: 302
currently lose_sum: 67.23459124565125
time_elpased: 3.046
batch start
#iterations: 303
currently lose_sum: 66.85712087154388
time_elpased: 2.69
batch start
#iterations: 304
currently lose_sum: 66.62381184101105
time_elpased: 3.088
batch start
#iterations: 305
currently lose_sum: 66.33476942777634
time_elpased: 2.925
batch start
#iterations: 306
currently lose_sum: 66.60840928554535
time_elpased: 2.989
batch start
#iterations: 307
currently lose_sum: 66.58399778604507
time_elpased: 3.121
batch start
#iterations: 308
currently lose_sum: 67.00706616044044
time_elpased: 2.958
batch start
#iterations: 309
currently lose_sum: 66.75914591550827
time_elpased: 3.046
batch start
#iterations: 310
currently lose_sum: 66.37608191370964
time_elpased: 3.105
batch start
#iterations: 311
currently lose_sum: 65.69463381171227
time_elpased: 2.967
batch start
#iterations: 312
currently lose_sum: 65.69932699203491
time_elpased: 2.908
batch start
#iterations: 313
currently lose_sum: 66.1816676557064
time_elpased: 2.811
batch start
#iterations: 314
currently lose_sum: 65.62772634625435
time_elpased: 2.893
batch start
#iterations: 315
currently lose_sum: 66.51569122076035
time_elpased: 2.618
batch start
#iterations: 316
currently lose_sum: 65.96637997031212
time_elpased: 2.683
batch start
#iterations: 317
currently lose_sum: 65.95208197832108
time_elpased: 2.836
batch start
#iterations: 318
currently lose_sum: 66.30000495910645
time_elpased: 2.425
batch start
#iterations: 319
currently lose_sum: 66.0957573056221
time_elpased: 1.787
start validation test
0.576907216495
0.65008951661
0.336317793558
0.443298969072
0.577329608335
76.528
batch start
#iterations: 320
currently lose_sum: 65.51625734567642
time_elpased: 2.42
batch start
#iterations: 321
currently lose_sum: 66.02370494604111
time_elpased: 2.661
batch start
#iterations: 322
currently lose_sum: 65.80172553658485
time_elpased: 2.005
batch start
#iterations: 323
currently lose_sum: 66.34263214468956
time_elpased: 2.153
batch start
#iterations: 324
currently lose_sum: 66.18605428934097
time_elpased: 2.557
batch start
#iterations: 325
currently lose_sum: 65.98339700698853
time_elpased: 2.289
batch start
#iterations: 326
currently lose_sum: 65.53578561544418
time_elpased: 2.132
batch start
#iterations: 327
currently lose_sum: 65.70547983050346
time_elpased: 1.474
batch start
#iterations: 328
currently lose_sum: 65.43845871090889
time_elpased: 1.447
batch start
#iterations: 329
currently lose_sum: 66.45951822400093
time_elpased: 1.608
batch start
#iterations: 330
currently lose_sum: 65.85105362534523
time_elpased: 1.513
batch start
#iterations: 331
currently lose_sum: 65.20908033847809
time_elpased: 1.915
batch start
#iterations: 332
currently lose_sum: 65.70580074191093
time_elpased: 2.079
batch start
#iterations: 333
currently lose_sum: 65.84738731384277
time_elpased: 2.478
batch start
#iterations: 334
currently lose_sum: 64.69829180836678
time_elpased: 1.849
batch start
#iterations: 335
currently lose_sum: 65.02126443386078
time_elpased: 1.642
batch start
#iterations: 336
currently lose_sum: 65.59682595729828
time_elpased: 2.339
batch start
#iterations: 337
currently lose_sum: 65.3907036781311
time_elpased: 2.347
batch start
#iterations: 338
currently lose_sum: 64.9639567732811
time_elpased: 2.359
batch start
#iterations: 339
currently lose_sum: 65.25951018929482
time_elpased: 2.248
start validation test
0.596804123711
0.650516282764
0.421426366162
0.511491381464
0.597112026415
72.606
batch start
#iterations: 340
currently lose_sum: 65.57046419382095
time_elpased: 3.249
batch start
#iterations: 341
currently lose_sum: 65.50152119994164
time_elpased: 2.452
batch start
#iterations: 342
currently lose_sum: 65.59779676795006
time_elpased: 2.68
batch start
#iterations: 343
currently lose_sum: 64.54885050654411
time_elpased: 3.056
batch start
#iterations: 344
currently lose_sum: 64.35673502087593
time_elpased: 3.267
batch start
#iterations: 345
currently lose_sum: 65.52351325750351
time_elpased: 2.894
batch start
#iterations: 346
currently lose_sum: 65.45430210232735
time_elpased: 2.884
batch start
#iterations: 347
currently lose_sum: 64.83416560292244
time_elpased: 2.918
batch start
#iterations: 348
currently lose_sum: 65.86365076899529
time_elpased: 2.749
batch start
#iterations: 349
currently lose_sum: 65.0246434211731
time_elpased: 2.782
batch start
#iterations: 350
currently lose_sum: 64.40951415896416
time_elpased: 2.724
batch start
#iterations: 351
currently lose_sum: 65.51601850986481
time_elpased: 2.559
batch start
#iterations: 352
currently lose_sum: 64.61157351732254
time_elpased: 2.665
batch start
#iterations: 353
currently lose_sum: 65.37424784898758
time_elpased: 3.071
batch start
#iterations: 354
currently lose_sum: 65.0850397348404
time_elpased: 3.057
batch start
#iterations: 355
currently lose_sum: 64.48278230428696
time_elpased: 3.405
batch start
#iterations: 356
currently lose_sum: 65.20398834347725
time_elpased: 3.152
batch start
#iterations: 357
currently lose_sum: 64.32214346528053
time_elpased: 3.227
batch start
#iterations: 358
currently lose_sum: 65.07374542951584
time_elpased: 2.995
batch start
#iterations: 359
currently lose_sum: 64.04997682571411
time_elpased: 3.135
start validation test
0.583453608247
0.650201983107
0.364412884635
0.467057970059
0.583838168022
76.096
batch start
#iterations: 360
currently lose_sum: 64.44475600123405
time_elpased: 2.645
batch start
#iterations: 361
currently lose_sum: 64.44692322611809
time_elpased: 3.022
batch start
#iterations: 362
currently lose_sum: 64.07146337628365
time_elpased: 2.46
batch start
#iterations: 363
currently lose_sum: 65.22704181075096
time_elpased: 2.242
batch start
#iterations: 364
currently lose_sum: 64.11659708619118
time_elpased: 1.973
batch start
#iterations: 365
currently lose_sum: 64.32783615589142
time_elpased: 1.491
batch start
#iterations: 366
currently lose_sum: 64.70793548226357
time_elpased: 1.464
batch start
#iterations: 367
currently lose_sum: 63.66128161549568
time_elpased: 1.543
batch start
#iterations: 368
currently lose_sum: 64.52152535319328
time_elpased: 3.683
batch start
#iterations: 369
currently lose_sum: 64.27773827314377
time_elpased: 1.49
batch start
#iterations: 370
currently lose_sum: 64.17334935069084
time_elpased: 1.508
batch start
#iterations: 371
currently lose_sum: 63.70564144849777
time_elpased: 1.427
batch start
#iterations: 372
currently lose_sum: 64.05243235826492
time_elpased: 15.546
batch start
#iterations: 373
currently lose_sum: 63.53054615855217
time_elpased: 1.489
batch start
#iterations: 374
currently lose_sum: 64.24621069431305
time_elpased: 1.453
batch start
#iterations: 375
currently lose_sum: 63.950685024261475
time_elpased: 1.44
batch start
#iterations: 376
currently lose_sum: 64.30761605501175
time_elpased: 1.607
batch start
#iterations: 377
currently lose_sum: 64.34942677617073
time_elpased: 1.763
batch start
#iterations: 378
currently lose_sum: 63.930701315402985
time_elpased: 1.986
batch start
#iterations: 379
currently lose_sum: 64.25735223293304
time_elpased: 2.076
start validation test
0.58175257732
0.645278230923
0.366368220644
0.467375607194
0.582130717778
77.424
batch start
#iterations: 380
currently lose_sum: 63.941429525613785
time_elpased: 2.153
batch start
#iterations: 381
currently lose_sum: 64.81052061915398
time_elpased: 2.032
batch start
#iterations: 382
currently lose_sum: 63.47352024912834
time_elpased: 2.081
batch start
#iterations: 383
currently lose_sum: 63.68506994843483
time_elpased: 1.978
batch start
#iterations: 384
currently lose_sum: 63.67778754234314
time_elpased: 2.481
batch start
#iterations: 385
currently lose_sum: 63.666293263435364
time_elpased: 2.508
batch start
#iterations: 386
currently lose_sum: 63.74384614825249
time_elpased: 2.375
batch start
#iterations: 387
currently lose_sum: 63.67913281917572
time_elpased: 2.903
batch start
#iterations: 388
currently lose_sum: 64.28679817914963
time_elpased: 2.912
batch start
#iterations: 389
currently lose_sum: 63.048616886138916
time_elpased: 2.194
batch start
#iterations: 390
currently lose_sum: 63.576747089624405
time_elpased: 1.568
batch start
#iterations: 391
currently lose_sum: 63.78527697920799
time_elpased: 16.547
batch start
#iterations: 392
currently lose_sum: 63.23561057448387
time_elpased: 3.196
batch start
#iterations: 393
currently lose_sum: 63.00741359591484
time_elpased: 2.757
batch start
#iterations: 394
currently lose_sum: 63.680561542510986
time_elpased: 2.523
batch start
#iterations: 395
currently lose_sum: 64.09631213545799
time_elpased: 2.275
batch start
#iterations: 396
currently lose_sum: 63.2099272608757
time_elpased: 1.955
batch start
#iterations: 397
currently lose_sum: 63.62209606170654
time_elpased: 2.724
batch start
#iterations: 398
currently lose_sum: 63.455163419246674
time_elpased: 1.598
batch start
#iterations: 399
currently lose_sum: 63.31291550397873
time_elpased: 1.468
start validation test
0.593144329897
0.647001934236
0.413090460019
0.504239683437
0.593460442237
74.980
acc: 0.645
pre: 0.676
rec: 0.558
F1: 0.611
auc: 0.719
