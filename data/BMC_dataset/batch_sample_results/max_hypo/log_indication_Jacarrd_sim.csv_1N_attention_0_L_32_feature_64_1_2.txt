start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 99.88055819272995
time_elpased: 2.345
batch start
#iterations: 1
currently lose_sum: 98.84444797039032
time_elpased: 2.256
batch start
#iterations: 2
currently lose_sum: 98.32619416713715
time_elpased: 2.324
batch start
#iterations: 3
currently lose_sum: 97.27316796779633
time_elpased: 2.467
batch start
#iterations: 4
currently lose_sum: 97.30683141946793
time_elpased: 2.406
batch start
#iterations: 5
currently lose_sum: 97.22328805923462
time_elpased: 2.306
batch start
#iterations: 6
currently lose_sum: 96.22203069925308
time_elpased: 2.375
batch start
#iterations: 7
currently lose_sum: 96.29223561286926
time_elpased: 1.958
batch start
#iterations: 8
currently lose_sum: 95.83970218896866
time_elpased: 2.294
batch start
#iterations: 9
currently lose_sum: 95.64225488901138
time_elpased: 2.348
batch start
#iterations: 10
currently lose_sum: 95.22156524658203
time_elpased: 2.31
batch start
#iterations: 11
currently lose_sum: 94.95656234025955
time_elpased: 2.45
batch start
#iterations: 12
currently lose_sum: 94.68944215774536
time_elpased: 2.673
batch start
#iterations: 13
currently lose_sum: 94.47142940759659
time_elpased: 2.497
batch start
#iterations: 14
currently lose_sum: 94.08399534225464
time_elpased: 2.567
batch start
#iterations: 15
currently lose_sum: 93.66208404302597
time_elpased: 2.326
batch start
#iterations: 16
currently lose_sum: 93.50147664546967
time_elpased: 1.8
batch start
#iterations: 17
currently lose_sum: 93.18437612056732
time_elpased: 2.508
batch start
#iterations: 18
currently lose_sum: 93.21181786060333
time_elpased: 1.966
batch start
#iterations: 19
currently lose_sum: 92.76896923780441
time_elpased: 2.169
start validation test
0.638659793814
0.668955186618
0.551507666975
0.60458032491
0.638812802815
60.934
batch start
#iterations: 20
currently lose_sum: 92.35103476047516
time_elpased: 2.349
batch start
#iterations: 21
currently lose_sum: 92.14858651161194
time_elpased: 2.348
batch start
#iterations: 22
currently lose_sum: 91.85943359136581
time_elpased: 2.273
batch start
#iterations: 23
currently lose_sum: 91.30518966913223
time_elpased: 2.155
batch start
#iterations: 24
currently lose_sum: 91.28431975841522
time_elpased: 2.19
batch start
#iterations: 25
currently lose_sum: 91.50117552280426
time_elpased: 1.791
batch start
#iterations: 26
currently lose_sum: 90.64965707063675
time_elpased: 1.947
batch start
#iterations: 27
currently lose_sum: 91.04407358169556
time_elpased: 2.356
batch start
#iterations: 28
currently lose_sum: 90.26026123762131
time_elpased: 2.55
batch start
#iterations: 29
currently lose_sum: 90.38229721784592
time_elpased: 2.693
batch start
#iterations: 30
currently lose_sum: 89.65343672037125
time_elpased: 2.484
batch start
#iterations: 31
currently lose_sum: 89.67865318059921
time_elpased: 2.561
batch start
#iterations: 32
currently lose_sum: 89.30414843559265
time_elpased: 2.239
batch start
#iterations: 33
currently lose_sum: 89.48318821191788
time_elpased: 2.221
batch start
#iterations: 34
currently lose_sum: 89.22811406850815
time_elpased: 2.212
batch start
#iterations: 35
currently lose_sum: 89.23627269268036
time_elpased: 2.3
batch start
#iterations: 36
currently lose_sum: 88.35115468502045
time_elpased: 2.358
batch start
#iterations: 37
currently lose_sum: 88.35424470901489
time_elpased: 2.371
batch start
#iterations: 38
currently lose_sum: 88.38238340616226
time_elpased: 2.452
batch start
#iterations: 39
currently lose_sum: 87.97962588071823
time_elpased: 2.473
start validation test
0.646546391753
0.658431198759
0.611608521149
0.634156751854
0.646607730573
60.072
batch start
#iterations: 40
currently lose_sum: 87.88668912649155
time_elpased: 2.262
batch start
#iterations: 41
currently lose_sum: 87.53373086452484
time_elpased: 2.549
batch start
#iterations: 42
currently lose_sum: 87.7700782418251
time_elpased: 2.539
batch start
#iterations: 43
currently lose_sum: 87.35004234313965
time_elpased: 2.484
batch start
#iterations: 44
currently lose_sum: 86.76852387189865
time_elpased: 2.447
batch start
#iterations: 45
currently lose_sum: 86.86134684085846
time_elpased: 2.284
batch start
#iterations: 46
currently lose_sum: 86.26314890384674
time_elpased: 2.181
batch start
#iterations: 47
currently lose_sum: 86.62331902980804
time_elpased: 2.429
batch start
#iterations: 48
currently lose_sum: 86.68598866462708
time_elpased: 2.36
batch start
#iterations: 49
currently lose_sum: 86.30831825733185
time_elpased: 2.436
batch start
#iterations: 50
currently lose_sum: 86.10068607330322
time_elpased: 2.552
batch start
#iterations: 51
currently lose_sum: 85.45667344331741
time_elpased: 2.61
batch start
#iterations: 52
currently lose_sum: 85.78568196296692
time_elpased: 2.358
batch start
#iterations: 53
currently lose_sum: 85.38774073123932
time_elpased: 2.461
batch start
#iterations: 54
currently lose_sum: 85.33444380760193
time_elpased: 2.349
batch start
#iterations: 55
currently lose_sum: 85.5324159860611
time_elpased: 2.101
batch start
#iterations: 56
currently lose_sum: 85.27223032712936
time_elpased: 2.538
batch start
#iterations: 57
currently lose_sum: 84.81791424751282
time_elpased: 2.319
batch start
#iterations: 58
currently lose_sum: 84.92235624790192
time_elpased: 2.263
batch start
#iterations: 59
currently lose_sum: 84.69929337501526
time_elpased: 2.348
start validation test
0.644226804124
0.658627296292
0.601420191417
0.628725121033
0.644301957735
60.460
batch start
#iterations: 60
currently lose_sum: 84.55268341302872
time_elpased: 2.298
batch start
#iterations: 61
currently lose_sum: 84.18222534656525
time_elpased: 2.133
batch start
#iterations: 62
currently lose_sum: 84.70813608169556
time_elpased: 2.427
batch start
#iterations: 63
currently lose_sum: 84.25764971971512
time_elpased: 2.196
batch start
#iterations: 64
currently lose_sum: 84.03511166572571
time_elpased: 2.314
batch start
#iterations: 65
currently lose_sum: 83.8672459423542
time_elpased: 2.077
batch start
#iterations: 66
currently lose_sum: 83.30169427394867
time_elpased: 2.362
batch start
#iterations: 67
currently lose_sum: 83.57042056322098
time_elpased: 2.35
batch start
#iterations: 68
currently lose_sum: 82.91792315244675
time_elpased: 2.235
batch start
#iterations: 69
currently lose_sum: 83.26872217655182
time_elpased: 2.308
batch start
#iterations: 70
currently lose_sum: 83.19978547096252
time_elpased: 1.935
batch start
#iterations: 71
currently lose_sum: 83.19142532348633
time_elpased: 2.354
batch start
#iterations: 72
currently lose_sum: 82.64458590745926
time_elpased: 2.356
batch start
#iterations: 73
currently lose_sum: 82.80320698022842
time_elpased: 2.197
batch start
#iterations: 74
currently lose_sum: 82.60791674256325
time_elpased: 2.161
batch start
#iterations: 75
currently lose_sum: 82.29992258548737
time_elpased: 1.973
batch start
#iterations: 76
currently lose_sum: 82.44018203020096
time_elpased: 2.167
batch start
#iterations: 77
currently lose_sum: 82.26520764827728
time_elpased: 2.725
batch start
#iterations: 78
currently lose_sum: 81.95178842544556
time_elpased: 2.099
batch start
#iterations: 79
currently lose_sum: 82.56843042373657
time_elpased: 2.392
start validation test
0.622886597938
0.670307845084
0.486261191726
0.56364070142
0.623126464912
62.625
batch start
#iterations: 80
currently lose_sum: 81.85238534212112
time_elpased: 2.154
batch start
#iterations: 81
currently lose_sum: 81.79103177785873
time_elpased: 2.301
batch start
#iterations: 82
currently lose_sum: 81.50520092248917
time_elpased: 2.568
batch start
#iterations: 83
currently lose_sum: 81.3713406920433
time_elpased: 2.675
batch start
#iterations: 84
currently lose_sum: 81.79056024551392
time_elpased: 2.348
batch start
#iterations: 85
currently lose_sum: 81.51756036281586
time_elpased: 2.158
batch start
#iterations: 86
currently lose_sum: 80.88316589593887
time_elpased: 2.323
batch start
#iterations: 87
currently lose_sum: 80.83484950661659
time_elpased: 2.317
batch start
#iterations: 88
currently lose_sum: 80.88278669118881
time_elpased: 2.438
batch start
#iterations: 89
currently lose_sum: 81.04744344949722
time_elpased: 2.266
batch start
#iterations: 90
currently lose_sum: 80.84851852059364
time_elpased: 2.439
batch start
#iterations: 91
currently lose_sum: 80.7649318575859
time_elpased: 2.154
batch start
#iterations: 92
currently lose_sum: 80.22325178980827
time_elpased: 2.527
batch start
#iterations: 93
currently lose_sum: 80.42517918348312
time_elpased: 2.31
batch start
#iterations: 94
currently lose_sum: 80.1041664481163
time_elpased: 2.291
batch start
#iterations: 95
currently lose_sum: 79.40632903575897
time_elpased: 2.204
batch start
#iterations: 96
currently lose_sum: 79.54449823498726
time_elpased: 2.196
batch start
#iterations: 97
currently lose_sum: 79.66810789704323
time_elpased: 2.114
batch start
#iterations: 98
currently lose_sum: 79.78988999128342
time_elpased: 2.35
batch start
#iterations: 99
currently lose_sum: 79.50479450821877
time_elpased: 2.417
start validation test
0.616701030928
0.67009694258
0.462385509931
0.54719279016
0.616971955626
63.631
batch start
#iterations: 100
currently lose_sum: 80.4410110116005
time_elpased: 2.543
batch start
#iterations: 101
currently lose_sum: 79.69147965312004
time_elpased: 2.202
batch start
#iterations: 102
currently lose_sum: 79.19555938243866
time_elpased: 2.669
batch start
#iterations: 103
currently lose_sum: 79.10958331823349
time_elpased: 2.364
batch start
#iterations: 104
currently lose_sum: 79.2255642414093
time_elpased: 2.223
batch start
#iterations: 105
currently lose_sum: 78.15009295940399
time_elpased: 2.301
batch start
#iterations: 106
currently lose_sum: 78.07932156324387
time_elpased: 2.243
batch start
#iterations: 107
currently lose_sum: 78.9679092168808
time_elpased: 2.419
batch start
#iterations: 108
currently lose_sum: 78.29550424218178
time_elpased: 2.49
batch start
#iterations: 109
currently lose_sum: 78.81176441907883
time_elpased: 2.408
batch start
#iterations: 110
currently lose_sum: 78.80177068710327
time_elpased: 2.589
batch start
#iterations: 111
currently lose_sum: 78.35763141512871
time_elpased: 1.906
batch start
#iterations: 112
currently lose_sum: 78.35511991381645
time_elpased: 1.949
batch start
#iterations: 113
currently lose_sum: 78.01600041985512
time_elpased: 2.367
batch start
#iterations: 114
currently lose_sum: 78.3596017062664
time_elpased: 2.413
batch start
#iterations: 115
currently lose_sum: 78.36055099964142
time_elpased: 2.442
batch start
#iterations: 116
currently lose_sum: 77.61554673314095
time_elpased: 2.336
batch start
#iterations: 117
currently lose_sum: 78.2698310315609
time_elpased: 2.119
batch start
#iterations: 118
currently lose_sum: 77.42025130987167
time_elpased: 2.309
batch start
#iterations: 119
currently lose_sum: 76.86679580807686
time_elpased: 2.348
start validation test
0.623041237113
0.664928649835
0.498713594731
0.569950014702
0.623259513466
63.930
batch start
#iterations: 120
currently lose_sum: 77.24029770493507
time_elpased: 2.464
batch start
#iterations: 121
currently lose_sum: 77.19857820868492
time_elpased: 2.478
batch start
#iterations: 122
currently lose_sum: 77.26234287023544
time_elpased: 1.96
batch start
#iterations: 123
currently lose_sum: 77.19908800721169
time_elpased: 2.287
batch start
#iterations: 124
currently lose_sum: 77.56980431079865
time_elpased: 2.374
batch start
#iterations: 125
currently lose_sum: 76.46006146073341
time_elpased: 2.298
batch start
#iterations: 126
currently lose_sum: 77.26507356762886
time_elpased: 2.18
batch start
#iterations: 127
currently lose_sum: 77.19605764746666
time_elpased: 2.324
batch start
#iterations: 128
currently lose_sum: 76.76872333884239
time_elpased: 2.342
batch start
#iterations: 129
currently lose_sum: 77.26867061853409
time_elpased: 2.275
batch start
#iterations: 130
currently lose_sum: 76.55687090754509
time_elpased: 2.034
batch start
#iterations: 131
currently lose_sum: 76.26909250020981
time_elpased: 2.272
batch start
#iterations: 132
currently lose_sum: 76.12588649988174
time_elpased: 2.202
batch start
#iterations: 133
currently lose_sum: 75.93370807170868
time_elpased: 2.079
batch start
#iterations: 134
currently lose_sum: 76.25500655174255
time_elpased: 2.218
batch start
#iterations: 135
currently lose_sum: 76.08287626504898
time_elpased: 2.373
batch start
#iterations: 136
currently lose_sum: 76.56704220175743
time_elpased: 2.305
batch start
#iterations: 137
currently lose_sum: 75.6198907494545
time_elpased: 2.302
batch start
#iterations: 138
currently lose_sum: 75.17265748977661
time_elpased: 2.413
batch start
#iterations: 139
currently lose_sum: 75.47881534695625
time_elpased: 2.392
start validation test
0.610773195876
0.669749216301
0.439744777195
0.530906380071
0.611073462645
66.184
batch start
#iterations: 140
currently lose_sum: 75.62834799289703
time_elpased: 2.177
batch start
#iterations: 141
currently lose_sum: 75.72756895422935
time_elpased: 1.998
batch start
#iterations: 142
currently lose_sum: 75.31465443968773
time_elpased: 2.267
batch start
#iterations: 143
currently lose_sum: 75.32874271273613
time_elpased: 2.285
batch start
#iterations: 144
currently lose_sum: 76.21530151367188
time_elpased: 2.1
batch start
#iterations: 145
currently lose_sum: 75.41085651516914
time_elpased: 2.055
batch start
#iterations: 146
currently lose_sum: 75.20509603619576
time_elpased: 2.101
batch start
#iterations: 147
currently lose_sum: 75.42839416861534
time_elpased: 2.109
batch start
#iterations: 148
currently lose_sum: 75.2659159898758
time_elpased: 2.443
batch start
#iterations: 149
currently lose_sum: 74.8796152472496
time_elpased: 2.617
batch start
#iterations: 150
currently lose_sum: 74.80100080370903
time_elpased: 2.205
batch start
#iterations: 151
currently lose_sum: 74.88935983181
time_elpased: 2.14
batch start
#iterations: 152
currently lose_sum: 74.6158102452755
time_elpased: 2.363
batch start
#iterations: 153
currently lose_sum: 74.68444553017616
time_elpased: 2.346
batch start
#iterations: 154
currently lose_sum: 74.76287251710892
time_elpased: 2.309
batch start
#iterations: 155
currently lose_sum: 74.68850621581078
time_elpased: 2.25
batch start
#iterations: 156
currently lose_sum: 74.82459166646004
time_elpased: 2.373
batch start
#iterations: 157
currently lose_sum: 74.22232630848885
time_elpased: 2.415
batch start
#iterations: 158
currently lose_sum: 74.80643171072006
time_elpased: 2.372
batch start
#iterations: 159
currently lose_sum: 73.71157023310661
time_elpased: 2.469
start validation test
0.596134020619
0.662802768166
0.394257486879
0.494418274505
0.596488446011
68.365
batch start
#iterations: 160
currently lose_sum: 74.0209372639656
time_elpased: 2.764
batch start
#iterations: 161
currently lose_sum: 73.33769676089287
time_elpased: 2.202
batch start
#iterations: 162
currently lose_sum: 74.14491549134254
time_elpased: 2.367
batch start
#iterations: 163
currently lose_sum: 74.11483523249626
time_elpased: 2.343
batch start
#iterations: 164
currently lose_sum: 73.31306824088097
time_elpased: 2.406
batch start
#iterations: 165
currently lose_sum: 73.81508332490921
time_elpased: 2.606
batch start
#iterations: 166
currently lose_sum: 74.17016696929932
time_elpased: 2.361
batch start
#iterations: 167
currently lose_sum: 73.53020432591438
time_elpased: 2.311
batch start
#iterations: 168
currently lose_sum: 73.51431754231453
time_elpased: 2.346
batch start
#iterations: 169
currently lose_sum: 73.70082345604897
time_elpased: 2.31
batch start
#iterations: 170
currently lose_sum: 74.36335778236389
time_elpased: 2.413
batch start
#iterations: 171
currently lose_sum: 73.90389034152031
time_elpased: 2.541
batch start
#iterations: 172
currently lose_sum: 72.87652191519737
time_elpased: 2.255
batch start
#iterations: 173
currently lose_sum: 73.09203314781189
time_elpased: 2.259
batch start
#iterations: 174
currently lose_sum: 72.45846992731094
time_elpased: 2.311
batch start
#iterations: 175
currently lose_sum: 73.12976983189583
time_elpased: 2.48
batch start
#iterations: 176
currently lose_sum: 73.1771931052208
time_elpased: 2.283
batch start
#iterations: 177
currently lose_sum: 72.51690965890884
time_elpased: 2.19
batch start
#iterations: 178
currently lose_sum: 72.1869564652443
time_elpased: 2.246
batch start
#iterations: 179
currently lose_sum: 72.4440279006958
time_elpased: 2.479
start validation test
0.593195876289
0.670848155776
0.368735206339
0.47589321291
0.593589951615
69.675
batch start
#iterations: 180
currently lose_sum: 73.22105517983437
time_elpased: 2.337
batch start
#iterations: 181
currently lose_sum: 72.75956937670708
time_elpased: 2.353
batch start
#iterations: 182
currently lose_sum: 72.6696447134018
time_elpased: 2.51
batch start
#iterations: 183
currently lose_sum: 72.67439505457878
time_elpased: 2.514
batch start
#iterations: 184
currently lose_sum: 72.95479732751846
time_elpased: 2.331
batch start
#iterations: 185
currently lose_sum: 72.50720772147179
time_elpased: 2.382
batch start
#iterations: 186
currently lose_sum: 71.99544662237167
time_elpased: 2.475
batch start
#iterations: 187
currently lose_sum: 72.16697323322296
time_elpased: 2.448
batch start
#iterations: 188
currently lose_sum: 72.60952973365784
time_elpased: 2.227
batch start
#iterations: 189
currently lose_sum: 72.69269421696663
time_elpased: 2.373
batch start
#iterations: 190
currently lose_sum: 71.97721400856972
time_elpased: 2.246
batch start
#iterations: 191
currently lose_sum: 72.56230211257935
time_elpased: 2.203
batch start
#iterations: 192
currently lose_sum: 72.24508506059647
time_elpased: 2.575
batch start
#iterations: 193
currently lose_sum: 72.10889858007431
time_elpased: 2.571
batch start
#iterations: 194
currently lose_sum: 72.18468230962753
time_elpased: 2.506
batch start
#iterations: 195
currently lose_sum: 71.8891276717186
time_elpased: 2.779
batch start
#iterations: 196
currently lose_sum: 71.64208912849426
time_elpased: 2.349
batch start
#iterations: 197
currently lose_sum: 71.6694572865963
time_elpased: 2.511
batch start
#iterations: 198
currently lose_sum: 70.36134493350983
time_elpased: 2.598
batch start
#iterations: 199
currently lose_sum: 71.15406262874603
time_elpased: 2.184
start validation test
0.593865979381
0.660608178958
0.389008953381
0.489669020014
0.594225637488
70.049
batch start
#iterations: 200
currently lose_sum: 71.46350184082985
time_elpased: 2.486
batch start
#iterations: 201
currently lose_sum: 71.63381922245026
time_elpased: 2.393
batch start
#iterations: 202
currently lose_sum: 71.52182930707932
time_elpased: 2.56
batch start
#iterations: 203
currently lose_sum: 70.9556850194931
time_elpased: 2.505
batch start
#iterations: 204
currently lose_sum: 71.16449221968651
time_elpased: 2.349
batch start
#iterations: 205
currently lose_sum: 71.25438198447227
time_elpased: 2.096
batch start
#iterations: 206
currently lose_sum: 71.19065853953362
time_elpased: 2.474
batch start
#iterations: 207
currently lose_sum: 72.00773894786835
time_elpased: 2.36
batch start
#iterations: 208
currently lose_sum: 71.22393345832825
time_elpased: 2.367
batch start
#iterations: 209
currently lose_sum: 70.8072216808796
time_elpased: 2.277
batch start
#iterations: 210
currently lose_sum: 71.02288600802422
time_elpased: 2.178
batch start
#iterations: 211
currently lose_sum: 71.00671979784966
time_elpased: 2.439
batch start
#iterations: 212
currently lose_sum: 70.94054728746414
time_elpased: 2.353
batch start
#iterations: 213
currently lose_sum: 69.99882727861404
time_elpased: 2.124
batch start
#iterations: 214
currently lose_sum: 70.50629952549934
time_elpased: 2.329
batch start
#iterations: 215
currently lose_sum: 70.2969274520874
time_elpased: 2.312
batch start
#iterations: 216
currently lose_sum: 70.08738681674004
time_elpased: 2.279
batch start
#iterations: 217
currently lose_sum: 70.01088565587997
time_elpased: 2.346
batch start
#iterations: 218
currently lose_sum: 69.97894367575645
time_elpased: 2.415
batch start
#iterations: 219
currently lose_sum: 70.326431453228
time_elpased: 2.334
start validation test
0.587422680412
0.66557123526
0.354327467325
0.46245802552
0.587831915011
72.317
batch start
#iterations: 220
currently lose_sum: 70.60636886954308
time_elpased: 2.291
batch start
#iterations: 221
currently lose_sum: 70.53834357857704
time_elpased: 2.198
batch start
#iterations: 222
currently lose_sum: 69.36449590325356
time_elpased: 2.431
batch start
#iterations: 223
currently lose_sum: 69.91837319731712
time_elpased: 2.115
batch start
#iterations: 224
currently lose_sum: 69.64529010653496
time_elpased: 1.999
batch start
#iterations: 225
currently lose_sum: 70.28552666306496
time_elpased: 2.247
batch start
#iterations: 226
currently lose_sum: 70.12117770314217
time_elpased: 2.249
batch start
#iterations: 227
currently lose_sum: 70.01432853937149
time_elpased: 2.354
batch start
#iterations: 228
currently lose_sum: 69.82489284873009
time_elpased: 2.553
batch start
#iterations: 229
currently lose_sum: 69.85394170880318
time_elpased: 2.328
batch start
#iterations: 230
currently lose_sum: 69.8424836397171
time_elpased: 2.225
batch start
#iterations: 231
currently lose_sum: 69.34205374121666
time_elpased: 2.301
batch start
#iterations: 232
currently lose_sum: 69.83373358845711
time_elpased: 2.426
batch start
#iterations: 233
currently lose_sum: 70.1328952908516
time_elpased: 2.392
batch start
#iterations: 234
currently lose_sum: 69.89216616749763
time_elpased: 2.451
batch start
#iterations: 235
currently lose_sum: 68.76916280388832
time_elpased: 2.419
batch start
#iterations: 236
currently lose_sum: 69.76557978987694
time_elpased: 2.296
batch start
#iterations: 237
currently lose_sum: 69.19022902846336
time_elpased: 2.307
batch start
#iterations: 238
currently lose_sum: 69.71330949664116
time_elpased: 2.38
batch start
#iterations: 239
currently lose_sum: 69.2696980535984
time_elpased: 2.276
start validation test
0.578711340206
0.660699417152
0.326644025934
0.437159975208
0.579153883255
74.504
batch start
#iterations: 240
currently lose_sum: 69.0302462875843
time_elpased: 2.346
batch start
#iterations: 241
currently lose_sum: 69.33862948417664
time_elpased: 2.251
batch start
#iterations: 242
currently lose_sum: 68.88060447573662
time_elpased: 2.155
batch start
#iterations: 243
currently lose_sum: 69.0117412507534
time_elpased: 2.28
batch start
#iterations: 244
currently lose_sum: 69.0909777879715
time_elpased: 2.369
batch start
#iterations: 245
currently lose_sum: 68.69244965910912
time_elpased: 2.206
batch start
#iterations: 246
currently lose_sum: 69.54306501150131
time_elpased: 2.269
batch start
#iterations: 247
currently lose_sum: 68.90250539779663
time_elpased: 2.447
batch start
#iterations: 248
currently lose_sum: 68.5426077246666
time_elpased: 2.27
batch start
#iterations: 249
currently lose_sum: 69.34200087189674
time_elpased: 2.272
batch start
#iterations: 250
currently lose_sum: 68.17689335346222
time_elpased: 2.423
batch start
#iterations: 251
currently lose_sum: 68.87945327162743
time_elpased: 2.244
batch start
#iterations: 252
currently lose_sum: 69.01125440001488
time_elpased: 2.327
batch start
#iterations: 253
currently lose_sum: 67.88425686955452
time_elpased: 2.343
batch start
#iterations: 254
currently lose_sum: 68.54616430401802
time_elpased: 2.29
batch start
#iterations: 255
currently lose_sum: 68.89111414551735
time_elpased: 2.32
batch start
#iterations: 256
currently lose_sum: 68.76703706383705
time_elpased: 2.273
batch start
#iterations: 257
currently lose_sum: 68.52352494001389
time_elpased: 2.084
batch start
#iterations: 258
currently lose_sum: 68.72058865427971
time_elpased: 2.282
batch start
#iterations: 259
currently lose_sum: 68.33056351542473
time_elpased: 2.335
start validation test
0.596958762887
0.663733609386
0.395904085623
0.495971120995
0.597311745383
71.385
batch start
#iterations: 260
currently lose_sum: 68.25213474035263
time_elpased: 2.373
batch start
#iterations: 261
currently lose_sum: 66.88542520999908
time_elpased: 2.466
batch start
#iterations: 262
currently lose_sum: 68.10191631317139
time_elpased: 2.362
batch start
#iterations: 263
currently lose_sum: 68.33158877491951
time_elpased: 2.344
batch start
#iterations: 264
currently lose_sum: 67.81051683425903
time_elpased: 2.409
batch start
#iterations: 265
currently lose_sum: 67.8479879796505
time_elpased: 2.605
batch start
#iterations: 266
currently lose_sum: 67.99753043055534
time_elpased: 2.526
batch start
#iterations: 267
currently lose_sum: 67.96537482738495
time_elpased: 2.287
batch start
#iterations: 268
currently lose_sum: 67.99866053462029
time_elpased: 2.406
batch start
#iterations: 269
currently lose_sum: 68.19298085570335
time_elpased: 2.452
batch start
#iterations: 270
currently lose_sum: 68.04253548383713
time_elpased: 2.286
batch start
#iterations: 271
currently lose_sum: 67.22739642858505
time_elpased: 2.187
batch start
#iterations: 272
currently lose_sum: 67.41765394806862
time_elpased: 2.442
batch start
#iterations: 273
currently lose_sum: 67.97583758831024
time_elpased: 2.479
batch start
#iterations: 274
currently lose_sum: 67.52918815612793
time_elpased: 2.4
batch start
#iterations: 275
currently lose_sum: 67.71626633405685
time_elpased: 2.38
batch start
#iterations: 276
currently lose_sum: 67.66316846013069
time_elpased: 2.233
batch start
#iterations: 277
currently lose_sum: 66.79633790254593
time_elpased: 1.986
batch start
#iterations: 278
currently lose_sum: 67.62158852815628
time_elpased: 2.625
batch start
#iterations: 279
currently lose_sum: 67.6247507929802
time_elpased: 2.599
start validation test
0.596030927835
0.660848733744
0.397447771946
0.496369127948
0.596379571195
72.334
batch start
#iterations: 280
currently lose_sum: 67.91256377100945
time_elpased: 2.273
batch start
#iterations: 281
currently lose_sum: 68.15668550133705
time_elpased: 2.466
batch start
#iterations: 282
currently lose_sum: 67.62078082561493
time_elpased: 2.131
batch start
#iterations: 283
currently lose_sum: 67.59831374883652
time_elpased: 2.078
batch start
#iterations: 284
currently lose_sum: 66.74323123693466
time_elpased: 2.326
batch start
#iterations: 285
currently lose_sum: 66.65562796592712
time_elpased: 2.345
batch start
#iterations: 286
currently lose_sum: 67.4209001660347
time_elpased: 2.32
batch start
#iterations: 287
currently lose_sum: 66.83814987540245
time_elpased: 2.253
batch start
#iterations: 288
currently lose_sum: 66.90365642309189
time_elpased: 2.302
batch start
#iterations: 289
currently lose_sum: 67.02338975667953
time_elpased: 2.219
batch start
#iterations: 290
currently lose_sum: 67.06897500157356
time_elpased: 1.915
batch start
#iterations: 291
currently lose_sum: 66.30724519491196
time_elpased: 2.305
batch start
#iterations: 292
currently lose_sum: 66.77981334924698
time_elpased: 2.29
batch start
#iterations: 293
currently lose_sum: 66.20082986354828
time_elpased: 2.473
batch start
#iterations: 294
currently lose_sum: 66.38656803965569
time_elpased: 2.26
batch start
#iterations: 295
currently lose_sum: 67.15770107507706
time_elpased: 2.23
batch start
#iterations: 296
currently lose_sum: 66.26875132322311
time_elpased: 2.063
batch start
#iterations: 297
currently lose_sum: 66.94432431459427
time_elpased: 2.26
batch start
#iterations: 298
currently lose_sum: 66.46382284164429
time_elpased: 2.158
batch start
#iterations: 299
currently lose_sum: 66.61754125356674
time_elpased: 2.277
start validation test
0.578556701031
0.669601584856
0.313059586292
0.426647966339
0.579022822166
77.791
batch start
#iterations: 300
currently lose_sum: 66.02976915240288
time_elpased: 2.464
batch start
#iterations: 301
currently lose_sum: 66.43620032072067
time_elpased: 2.511
batch start
#iterations: 302
currently lose_sum: 66.21474239230156
time_elpased: 2.092
batch start
#iterations: 303
currently lose_sum: 66.90682154893875
time_elpased: 1.871
batch start
#iterations: 304
currently lose_sum: 66.66223305463791
time_elpased: 2.044
batch start
#iterations: 305
currently lose_sum: 65.64837330579758
time_elpased: 2.024
batch start
#iterations: 306
currently lose_sum: 66.44173783063889
time_elpased: 2.196
batch start
#iterations: 307
currently lose_sum: 66.71051588654518
time_elpased: 2.088
batch start
#iterations: 308
currently lose_sum: 65.99675151705742
time_elpased: 1.832
batch start
#iterations: 309
currently lose_sum: 66.50626727938652
time_elpased: 1.934
batch start
#iterations: 310
currently lose_sum: 66.33240666985512
time_elpased: 2.515
batch start
#iterations: 311
currently lose_sum: 64.91769731044769
time_elpased: 2.601
batch start
#iterations: 312
currently lose_sum: 65.98842066526413
time_elpased: 2.574
batch start
#iterations: 313
currently lose_sum: 66.50199481844902
time_elpased: 2.423
batch start
#iterations: 314
currently lose_sum: 66.1496110856533
time_elpased: 2.688
batch start
#iterations: 315
currently lose_sum: 65.87345871329308
time_elpased: 2.476
batch start
#iterations: 316
currently lose_sum: 66.23884400725365
time_elpased: 1.874
batch start
#iterations: 317
currently lose_sum: 66.04361599683762
time_elpased: 2.149
batch start
#iterations: 318
currently lose_sum: 65.46899053454399
time_elpased: 2.298
batch start
#iterations: 319
currently lose_sum: 66.04964032769203
time_elpased: 2.496
start validation test
0.580927835052
0.658858858859
0.338684779253
0.447389885808
0.581353130099
76.795
batch start
#iterations: 320
currently lose_sum: 66.25275322794914
time_elpased: 2.295
batch start
#iterations: 321
currently lose_sum: 65.87386178970337
time_elpased: 2.549
batch start
#iterations: 322
currently lose_sum: 66.39635345339775
time_elpased: 2.052
batch start
#iterations: 323
currently lose_sum: 65.2097235918045
time_elpased: 2.191
batch start
#iterations: 324
currently lose_sum: 65.52972248196602
time_elpased: 2.26
batch start
#iterations: 325
currently lose_sum: 65.59006184339523
time_elpased: 2.341
batch start
#iterations: 326
currently lose_sum: 65.46690133213997
time_elpased: 2.436
batch start
#iterations: 327
currently lose_sum: 65.43944442272186
time_elpased: 2.128
batch start
#iterations: 328
currently lose_sum: 65.15109339356422
time_elpased: 2.088
batch start
#iterations: 329
currently lose_sum: 65.4215420782566
time_elpased: 2.209
batch start
#iterations: 330
currently lose_sum: 65.75910317897797
time_elpased: 2.233
batch start
#iterations: 331
currently lose_sum: 65.26008972525597
time_elpased: 2.337
batch start
#iterations: 332
currently lose_sum: 64.5289158821106
time_elpased: 2.34
batch start
#iterations: 333
currently lose_sum: 65.07894018292427
time_elpased: 2.273
batch start
#iterations: 334
currently lose_sum: 64.97105491161346
time_elpased: 2.566
batch start
#iterations: 335
currently lose_sum: 64.88179805874825
time_elpased: 2.239
batch start
#iterations: 336
currently lose_sum: 65.31488880515099
time_elpased: 2.29
batch start
#iterations: 337
currently lose_sum: 65.40688061714172
time_elpased: 2.342
batch start
#iterations: 338
currently lose_sum: 65.1173403263092
time_elpased: 2.329
batch start
#iterations: 339
currently lose_sum: 65.20381554961205
time_elpased: 2.149
start validation test
0.587113402062
0.646423057128
0.387774004322
0.48475492088
0.587463373121
74.368
batch start
#iterations: 340
currently lose_sum: 64.95237186551094
time_elpased: 2.122
batch start
#iterations: 341
currently lose_sum: 64.94266813993454
time_elpased: 2.364
batch start
#iterations: 342
currently lose_sum: 65.25288626551628
time_elpased: 2.566
batch start
#iterations: 343
currently lose_sum: 64.83626395463943
time_elpased: 2.603
batch start
#iterations: 344
currently lose_sum: 64.48002207279205
time_elpased: 2.307
batch start
#iterations: 345
currently lose_sum: 65.15901297330856
time_elpased: 2.319
batch start
#iterations: 346
currently lose_sum: 64.894511282444
time_elpased: 2.064
batch start
#iterations: 347
currently lose_sum: 64.87538179755211
time_elpased: 2.514
batch start
#iterations: 348
currently lose_sum: 65.36065438389778
time_elpased: 2.758
batch start
#iterations: 349
currently lose_sum: 65.50813427567482
time_elpased: 2.38
batch start
#iterations: 350
currently lose_sum: 64.4255488216877
time_elpased: 2.543
batch start
#iterations: 351
currently lose_sum: 64.57229462265968
time_elpased: 2.567
batch start
#iterations: 352
currently lose_sum: 64.21581375598907
time_elpased: 2.357
batch start
#iterations: 353
currently lose_sum: 64.38389012217522
time_elpased: 2.425
batch start
#iterations: 354
currently lose_sum: 64.20546424388885
time_elpased: 2.372
batch start
#iterations: 355
currently lose_sum: 64.62242683768272
time_elpased: 2.284
batch start
#iterations: 356
currently lose_sum: 64.32995277643204
time_elpased: 2.197
batch start
#iterations: 357
currently lose_sum: 63.97003075480461
time_elpased: 2.111
batch start
#iterations: 358
currently lose_sum: 64.09504291415215
time_elpased: 2.32
batch start
#iterations: 359
currently lose_sum: 63.258435279130936
time_elpased: 2.328
start validation test
0.593762886598
0.641187326976
0.429041885356
0.514088414822
0.594052079722
73.588
batch start
#iterations: 360
currently lose_sum: 64.34176397323608
time_elpased: 2.564
batch start
#iterations: 361
currently lose_sum: 64.51975679397583
time_elpased: 2.433
batch start
#iterations: 362
currently lose_sum: 64.49955904483795
time_elpased: 2.38
batch start
#iterations: 363
currently lose_sum: 64.60367584228516
time_elpased: 2.12
batch start
#iterations: 364
currently lose_sum: 63.94978055357933
time_elpased: 2.276
batch start
#iterations: 365
currently lose_sum: 64.77268028259277
time_elpased: 2.178
batch start
#iterations: 366
currently lose_sum: 64.55590131878853
time_elpased: 2.232
batch start
#iterations: 367
currently lose_sum: 64.37202537059784
time_elpased: 2.354
batch start
#iterations: 368
currently lose_sum: 64.00751078128815
time_elpased: 2.476
batch start
#iterations: 369
currently lose_sum: 64.26472261548042
time_elpased: 2.441
batch start
#iterations: 370
currently lose_sum: 63.648736506700516
time_elpased: 2.121
batch start
#iterations: 371
currently lose_sum: 64.43492075800896
time_elpased: 2.229
batch start
#iterations: 372
currently lose_sum: 63.506261467933655
time_elpased: 2.362
batch start
#iterations: 373
currently lose_sum: 63.445302456617355
time_elpased: 2.393
batch start
#iterations: 374
currently lose_sum: 64.61733528971672
time_elpased: 2.518
batch start
#iterations: 375
currently lose_sum: 63.16687598824501
time_elpased: 2.474
batch start
#iterations: 376
currently lose_sum: 64.26503175497055
time_elpased: 2.321
batch start
#iterations: 377
currently lose_sum: 63.94780811667442
time_elpased: 2.417
batch start
#iterations: 378
currently lose_sum: 63.498663902282715
time_elpased: 2.452
batch start
#iterations: 379
currently lose_sum: 63.643745481967926
time_elpased: 2.468
start validation test
0.58087628866
0.661836734694
0.333744983019
0.443729903537
0.581310165784
78.742
batch start
#iterations: 380
currently lose_sum: 63.126444190740585
time_elpased: 2.09
batch start
#iterations: 381
currently lose_sum: 63.825296342372894
time_elpased: 2.399
batch start
#iterations: 382
currently lose_sum: 64.09347543120384
time_elpased: 2.366
batch start
#iterations: 383
currently lose_sum: 63.416020303964615
time_elpased: 2.384
batch start
#iterations: 384
currently lose_sum: 63.70472779870033
time_elpased: 2.386
batch start
#iterations: 385
currently lose_sum: 64.71516308188438
time_elpased: 2.327
batch start
#iterations: 386
currently lose_sum: 62.50033235549927
time_elpased: 2.275
batch start
#iterations: 387
currently lose_sum: 63.62823811173439
time_elpased: 2.235
batch start
#iterations: 388
currently lose_sum: 63.28763434290886
time_elpased: 2.231
batch start
#iterations: 389
currently lose_sum: 63.496750086545944
time_elpased: 2.309
batch start
#iterations: 390
currently lose_sum: 63.6606240272522
time_elpased: 2.311
batch start
#iterations: 391
currently lose_sum: 63.04132452607155
time_elpased: 2.13
batch start
#iterations: 392
currently lose_sum: 63.03404572606087
time_elpased: 2.193
batch start
#iterations: 393
currently lose_sum: 62.46012803912163
time_elpased: 2.36
batch start
#iterations: 394
currently lose_sum: 63.630455166101456
time_elpased: 2.176
batch start
#iterations: 395
currently lose_sum: 63.40721669793129
time_elpased: 2.521
batch start
#iterations: 396
currently lose_sum: 62.710120379924774
time_elpased: 2.507
batch start
#iterations: 397
currently lose_sum: 63.56006160378456
time_elpased: 2.459
batch start
#iterations: 398
currently lose_sum: 62.71947908401489
time_elpased: 2.374
batch start
#iterations: 399
currently lose_sum: 63.09096318483353
time_elpased: 2.348
start validation test
0.57912371134
0.656199677939
0.335494494185
0.443990466462
0.579551440008
79.583
acc: 0.644
pre: 0.656
rec: 0.608
F1: 0.631
auc: 0.704
