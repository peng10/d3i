start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 99.9977234005928
time_elpased: 1.71
batch start
#iterations: 1
currently lose_sum: 98.91125285625458
time_elpased: 1.676
batch start
#iterations: 2
currently lose_sum: 98.52023476362228
time_elpased: 1.669
batch start
#iterations: 3
currently lose_sum: 97.48466616868973
time_elpased: 1.682
batch start
#iterations: 4
currently lose_sum: 96.86266696453094
time_elpased: 1.692
batch start
#iterations: 5
currently lose_sum: 95.81645780801773
time_elpased: 1.704
batch start
#iterations: 6
currently lose_sum: 94.57433372735977
time_elpased: 1.635
batch start
#iterations: 7
currently lose_sum: 94.54768866300583
time_elpased: 1.699
batch start
#iterations: 8
currently lose_sum: 93.99371612071991
time_elpased: 1.694
batch start
#iterations: 9
currently lose_sum: 93.26182550191879
time_elpased: 2.181
batch start
#iterations: 10
currently lose_sum: 92.93164032697678
time_elpased: 2.991
batch start
#iterations: 11
currently lose_sum: 92.43801909685135
time_elpased: 3.409
batch start
#iterations: 12
currently lose_sum: 91.82347774505615
time_elpased: 3.19
batch start
#iterations: 13
currently lose_sum: 91.864854991436
time_elpased: 3.165
batch start
#iterations: 14
currently lose_sum: 91.13615435361862
time_elpased: 2.581
batch start
#iterations: 15
currently lose_sum: 91.23896312713623
time_elpased: 2.883
batch start
#iterations: 16
currently lose_sum: 90.77082705497742
time_elpased: 2.927
batch start
#iterations: 17
currently lose_sum: 90.73417663574219
time_elpased: 2.488
batch start
#iterations: 18
currently lose_sum: 89.92113411426544
time_elpased: 2.696
batch start
#iterations: 19
currently lose_sum: 90.26562088727951
time_elpased: 2.796
start validation test
0.658659793814
0.644992035979
0.708449109808
0.675232957332
0.658572380991
59.631
batch start
#iterations: 20
currently lose_sum: 89.60074746608734
time_elpased: 3.171
batch start
#iterations: 21
currently lose_sum: 89.59490394592285
time_elpased: 3.294
batch start
#iterations: 22
currently lose_sum: 89.24954921007156
time_elpased: 2.879
batch start
#iterations: 23
currently lose_sum: 88.96840238571167
time_elpased: 2.888
batch start
#iterations: 24
currently lose_sum: 88.37433874607086
time_elpased: 2.774
batch start
#iterations: 25
currently lose_sum: 88.33440220355988
time_elpased: 2.761
batch start
#iterations: 26
currently lose_sum: 87.75192511081696
time_elpased: 2.885
batch start
#iterations: 27
currently lose_sum: 87.57269287109375
time_elpased: 3.069
batch start
#iterations: 28
currently lose_sum: 86.99063205718994
time_elpased: 2.405
batch start
#iterations: 29
currently lose_sum: 87.83058828115463
time_elpased: 2.306
batch start
#iterations: 30
currently lose_sum: 87.12261438369751
time_elpased: 2.396
batch start
#iterations: 31
currently lose_sum: 86.72299426794052
time_elpased: 2.441
batch start
#iterations: 32
currently lose_sum: 86.30685836076736
time_elpased: 2.61
batch start
#iterations: 33
currently lose_sum: 86.8386664390564
time_elpased: 2.795
batch start
#iterations: 34
currently lose_sum: 85.77981823682785
time_elpased: 2.656
batch start
#iterations: 35
currently lose_sum: 86.31136685609818
time_elpased: 3.255
batch start
#iterations: 36
currently lose_sum: 85.26114273071289
time_elpased: 3.372
batch start
#iterations: 37
currently lose_sum: 85.75910753011703
time_elpased: 2.805
batch start
#iterations: 38
currently lose_sum: 85.30212670564651
time_elpased: 2.765
batch start
#iterations: 39
currently lose_sum: 84.34574618935585
time_elpased: 2.871
start validation test
0.66618556701
0.65963944439
0.68910157456
0.674048721562
0.666145334425
59.718
batch start
#iterations: 40
currently lose_sum: 84.20434832572937
time_elpased: 2.767
batch start
#iterations: 41
currently lose_sum: 84.71272522211075
time_elpased: 2.681
batch start
#iterations: 42
currently lose_sum: 83.99560242891312
time_elpased: 2.602
batch start
#iterations: 43
currently lose_sum: 83.67198258638382
time_elpased: 2.806
batch start
#iterations: 44
currently lose_sum: 83.6575516462326
time_elpased: 2.893
batch start
#iterations: 45
currently lose_sum: 83.473723590374
time_elpased: 2.817
batch start
#iterations: 46
currently lose_sum: 83.15527629852295
time_elpased: 2.78
batch start
#iterations: 47
currently lose_sum: 83.94412702322006
time_elpased: 2.766
batch start
#iterations: 48
currently lose_sum: 83.91292822360992
time_elpased: 2.794
batch start
#iterations: 49
currently lose_sum: 82.75156381726265
time_elpased: 3.114
batch start
#iterations: 50
currently lose_sum: 83.06138798594475
time_elpased: 3.096
batch start
#iterations: 51
currently lose_sum: 82.26504042744637
time_elpased: 2.441
batch start
#iterations: 52
currently lose_sum: 82.47877496480942
time_elpased: 2.664
batch start
#iterations: 53
currently lose_sum: 82.49066871404648
time_elpased: 2.882
batch start
#iterations: 54
currently lose_sum: 81.79465126991272
time_elpased: 2.895
batch start
#iterations: 55
currently lose_sum: 81.06334719061852
time_elpased: 2.664
batch start
#iterations: 56
currently lose_sum: 81.92131730914116
time_elpased: 2.526
batch start
#iterations: 57
currently lose_sum: 81.19404968619347
time_elpased: 2.461
batch start
#iterations: 58
currently lose_sum: 80.85891464352608
time_elpased: 2.62
batch start
#iterations: 59
currently lose_sum: 80.92433741688728
time_elpased: 2.824
start validation test
0.64587628866
0.663002404672
0.595862920655
0.627642276423
0.645964094841
64.015
batch start
#iterations: 60
currently lose_sum: 80.73706266283989
time_elpased: 2.77
batch start
#iterations: 61
currently lose_sum: 80.47307190299034
time_elpased: 2.578
batch start
#iterations: 62
currently lose_sum: 80.23532903194427
time_elpased: 2.862
batch start
#iterations: 63
currently lose_sum: 80.20448061823845
time_elpased: 2.689
batch start
#iterations: 64
currently lose_sum: 79.45784574747086
time_elpased: 2.798
batch start
#iterations: 65
currently lose_sum: 79.67036274075508
time_elpased: 2.803
batch start
#iterations: 66
currently lose_sum: 79.95948773622513
time_elpased: 2.822
batch start
#iterations: 67
currently lose_sum: 79.16689392924309
time_elpased: 2.707
batch start
#iterations: 68
currently lose_sum: 79.64880323410034
time_elpased: 2.864
batch start
#iterations: 69
currently lose_sum: 79.2847292125225
time_elpased: 2.643
batch start
#iterations: 70
currently lose_sum: 78.77412953972816
time_elpased: 2.446
batch start
#iterations: 71
currently lose_sum: 78.5278931260109
time_elpased: 2.81
batch start
#iterations: 72
currently lose_sum: 78.62569320201874
time_elpased: 2.725
batch start
#iterations: 73
currently lose_sum: 78.17353975772858
time_elpased: 2.701
batch start
#iterations: 74
currently lose_sum: 78.242565959692
time_elpased: 2.529
batch start
#iterations: 75
currently lose_sum: 77.6826434135437
time_elpased: 2.627
batch start
#iterations: 76
currently lose_sum: 78.46167552471161
time_elpased: 2.818
batch start
#iterations: 77
currently lose_sum: 77.81342321634293
time_elpased: 2.952
batch start
#iterations: 78
currently lose_sum: 77.92954862117767
time_elpased: 3.071
batch start
#iterations: 79
currently lose_sum: 77.56886383891106
time_elpased: 3.015
start validation test
0.639278350515
0.647852093529
0.61304929505
0.629970389171
0.639324399668
66.630
batch start
#iterations: 80
currently lose_sum: 76.81480503082275
time_elpased: 2.818
batch start
#iterations: 81
currently lose_sum: 78.63523936271667
time_elpased: 2.855
batch start
#iterations: 82
currently lose_sum: 77.84266957640648
time_elpased: 2.8
batch start
#iterations: 83
currently lose_sum: 76.62875100970268
time_elpased: 2.783
batch start
#iterations: 84
currently lose_sum: 76.6014215350151
time_elpased: 2.763
batch start
#iterations: 85
currently lose_sum: 76.61872029304504
time_elpased: 2.977
batch start
#iterations: 86
currently lose_sum: 76.76280578970909
time_elpased: 2.573
batch start
#iterations: 87
currently lose_sum: 75.93446284532547
time_elpased: 2.644
batch start
#iterations: 88
currently lose_sum: 76.06903982162476
time_elpased: 2.751
batch start
#iterations: 89
currently lose_sum: 75.71770441532135
time_elpased: 2.624
batch start
#iterations: 90
currently lose_sum: 75.8625391125679
time_elpased: 2.818
batch start
#iterations: 91
currently lose_sum: 75.75203785300255
time_elpased: 2.698
batch start
#iterations: 92
currently lose_sum: 75.29978322982788
time_elpased: 2.883
batch start
#iterations: 93
currently lose_sum: 76.19610938429832
time_elpased: 2.923
batch start
#iterations: 94
currently lose_sum: 75.44325378537178
time_elpased: 2.805
batch start
#iterations: 95
currently lose_sum: 75.9398342370987
time_elpased: 2.671
batch start
#iterations: 96
currently lose_sum: 75.36949703097343
time_elpased: 2.834
batch start
#iterations: 97
currently lose_sum: 74.95343491435051
time_elpased: 2.88
batch start
#iterations: 98
currently lose_sum: 74.27574211359024
time_elpased: 2.821
batch start
#iterations: 99
currently lose_sum: 75.39813780784607
time_elpased: 2.736
start validation test
0.617113402062
0.654474288028
0.499022331995
0.566273502277
0.61732072915
73.879
batch start
#iterations: 100
currently lose_sum: 73.73791670799255
time_elpased: 2.877
batch start
#iterations: 101
currently lose_sum: 74.19921490550041
time_elpased: 2.804
batch start
#iterations: 102
currently lose_sum: 74.33694386482239
time_elpased: 2.741
batch start
#iterations: 103
currently lose_sum: 74.70527082681656
time_elpased: 2.862
batch start
#iterations: 104
currently lose_sum: 74.00112473964691
time_elpased: 2.698
batch start
#iterations: 105
currently lose_sum: 74.01309207081795
time_elpased: 2.831
batch start
#iterations: 106
currently lose_sum: 73.52308800816536
time_elpased: 2.554
batch start
#iterations: 107
currently lose_sum: 73.23021054267883
time_elpased: 2.69
batch start
#iterations: 108
currently lose_sum: 73.60022956132889
time_elpased: 2.868
batch start
#iterations: 109
currently lose_sum: 73.27539420127869
time_elpased: 2.798
batch start
#iterations: 110
currently lose_sum: 72.40106517076492
time_elpased: 2.792
batch start
#iterations: 111
currently lose_sum: 73.01509734988213
time_elpased: 2.85
batch start
#iterations: 112
currently lose_sum: 73.17230504751205
time_elpased: 2.837
batch start
#iterations: 113
currently lose_sum: 72.94011834263802
time_elpased: 2.807
batch start
#iterations: 114
currently lose_sum: 71.93704217672348
time_elpased: 2.601
batch start
#iterations: 115
currently lose_sum: 72.76959469914436
time_elpased: 2.729
batch start
#iterations: 116
currently lose_sum: 72.38357925415039
time_elpased: 2.856
batch start
#iterations: 117
currently lose_sum: 72.03900471329689
time_elpased: 2.62
batch start
#iterations: 118
currently lose_sum: 71.74652057886124
time_elpased: 2.521
batch start
#iterations: 119
currently lose_sum: 72.16927069425583
time_elpased: 2.49
start validation test
0.619329896907
0.648308318494
0.524544612535
0.579896467376
0.619496307094
74.589
batch start
#iterations: 120
currently lose_sum: 72.61720806360245
time_elpased: 2.717
batch start
#iterations: 121
currently lose_sum: 71.20874556899071
time_elpased: 2.713
batch start
#iterations: 122
currently lose_sum: 71.34992757439613
time_elpased: 2.652
batch start
#iterations: 123
currently lose_sum: 71.26177150011063
time_elpased: 2.616
batch start
#iterations: 124
currently lose_sum: 72.23718822002411
time_elpased: 2.771
batch start
#iterations: 125
currently lose_sum: 71.44015750288963
time_elpased: 2.828
batch start
#iterations: 126
currently lose_sum: 71.31278306245804
time_elpased: 2.901
batch start
#iterations: 127
currently lose_sum: 72.20879170298576
time_elpased: 3.059
batch start
#iterations: 128
currently lose_sum: 70.27400678396225
time_elpased: 2.814
batch start
#iterations: 129
currently lose_sum: 70.83316564559937
time_elpased: 2.797
batch start
#iterations: 130
currently lose_sum: 70.53544360399246
time_elpased: 2.981
batch start
#iterations: 131
currently lose_sum: 70.43484902381897
time_elpased: 2.903
batch start
#iterations: 132
currently lose_sum: 71.18001824617386
time_elpased: 2.99
batch start
#iterations: 133
currently lose_sum: 69.76336443424225
time_elpased: 2.878
batch start
#iterations: 134
currently lose_sum: 69.88419681787491
time_elpased: 2.716
batch start
#iterations: 135
currently lose_sum: 70.11950621008873
time_elpased: 2.972
batch start
#iterations: 136
currently lose_sum: 69.77771982550621
time_elpased: 2.948
batch start
#iterations: 137
currently lose_sum: 68.99256631731987
time_elpased: 3.183
batch start
#iterations: 138
currently lose_sum: 70.20411449670792
time_elpased: 3.098
batch start
#iterations: 139
currently lose_sum: 69.30667081475258
time_elpased: 2.843
start validation test
0.598350515464
0.631614932312
0.475352475044
0.542454492073
0.598566457495
77.819
batch start
#iterations: 140
currently lose_sum: 69.71059709787369
time_elpased: 3.144
batch start
#iterations: 141
currently lose_sum: 68.83965194225311
time_elpased: 2.85
batch start
#iterations: 142
currently lose_sum: 69.83153399825096
time_elpased: 2.985
batch start
#iterations: 143
currently lose_sum: 68.92011976242065
time_elpased: 3.191
batch start
#iterations: 144
currently lose_sum: 69.85468089580536
time_elpased: 3.105
batch start
#iterations: 145
currently lose_sum: 69.2766854763031
time_elpased: 2.931
batch start
#iterations: 146
currently lose_sum: 68.43206870555878
time_elpased: 2.647
batch start
#iterations: 147
currently lose_sum: 69.57128074765205
time_elpased: 2.913
batch start
#iterations: 148
currently lose_sum: 68.43463903665543
time_elpased: 2.425
batch start
#iterations: 149
currently lose_sum: 69.01059409976006
time_elpased: 2.691
batch start
#iterations: 150
currently lose_sum: 67.99875336885452
time_elpased: 2.915
batch start
#iterations: 151
currently lose_sum: 68.64139473438263
time_elpased: 2.85
batch start
#iterations: 152
currently lose_sum: 68.37227535247803
time_elpased: 2.736
batch start
#iterations: 153
currently lose_sum: 68.3226008117199
time_elpased: 2.916
batch start
#iterations: 154
currently lose_sum: 68.35294172167778
time_elpased: 3.019
batch start
#iterations: 155
currently lose_sum: 67.75933107733727
time_elpased: 2.931
batch start
#iterations: 156
currently lose_sum: 68.16080790758133
time_elpased: 2.737
batch start
#iterations: 157
currently lose_sum: 66.85928997397423
time_elpased: 2.99
batch start
#iterations: 158
currently lose_sum: 67.8778233230114
time_elpased: 2.922
batch start
#iterations: 159
currently lose_sum: 67.28392839431763
time_elpased: 2.723
start validation test
0.58706185567
0.64302481556
0.394669136565
0.489126968943
0.587399630763
89.089
batch start
#iterations: 160
currently lose_sum: 67.18017077445984
time_elpased: 3.068
batch start
#iterations: 161
currently lose_sum: 66.66102465987206
time_elpased: 2.787
batch start
#iterations: 162
currently lose_sum: 66.64613366127014
time_elpased: 2.987
batch start
#iterations: 163
currently lose_sum: 67.01244494318962
time_elpased: 2.622
batch start
#iterations: 164
currently lose_sum: 67.84710946679115
time_elpased: 2.765
batch start
#iterations: 165
currently lose_sum: 66.70164784789085
time_elpased: 2.807
batch start
#iterations: 166
currently lose_sum: 67.4154914021492
time_elpased: 2.873
batch start
#iterations: 167
currently lose_sum: 66.01806190609932
time_elpased: 2.848
batch start
#iterations: 168
currently lose_sum: 66.29849073290825
time_elpased: 2.917
batch start
#iterations: 169
currently lose_sum: 66.46225881576538
time_elpased: 2.555
batch start
#iterations: 170
currently lose_sum: 65.7953591644764
time_elpased: 3.233
batch start
#iterations: 171
currently lose_sum: 66.36714014410973
time_elpased: 2.783
batch start
#iterations: 172
currently lose_sum: 64.98622262477875
time_elpased: 2.825
batch start
#iterations: 173
currently lose_sum: 65.63913199305534
time_elpased: 2.976
batch start
#iterations: 174
currently lose_sum: 66.22398468852043
time_elpased: 2.848
batch start
#iterations: 175
currently lose_sum: 65.82339265942574
time_elpased: 2.671
batch start
#iterations: 176
currently lose_sum: 65.47745370864868
time_elpased: 2.581
batch start
#iterations: 177
currently lose_sum: 64.79967585206032
time_elpased: 2.697
batch start
#iterations: 178
currently lose_sum: 65.35866284370422
time_elpased: 2.95
batch start
#iterations: 179
currently lose_sum: 65.42323803901672
time_elpased: 2.916
start validation test
0.609484536082
0.635044783651
0.518061129978
0.570618907277
0.609645043973
82.823
batch start
#iterations: 180
currently lose_sum: 65.04080519080162
time_elpased: 3.094
batch start
#iterations: 181
currently lose_sum: 64.39369094371796
time_elpased: 2.94
batch start
#iterations: 182
currently lose_sum: 64.49931347370148
time_elpased: 2.702
batch start
#iterations: 183
currently lose_sum: 64.79867163300514
time_elpased: 2.476
batch start
#iterations: 184
currently lose_sum: 64.97788724303246
time_elpased: 2.466
batch start
#iterations: 185
currently lose_sum: 64.91369214653969
time_elpased: 2.851
batch start
#iterations: 186
currently lose_sum: 64.72168812155724
time_elpased: 2.733
batch start
#iterations: 187
currently lose_sum: 64.72366365790367
time_elpased: 2.875
batch start
#iterations: 188
currently lose_sum: 64.48154157400131
time_elpased: 2.887
batch start
#iterations: 189
currently lose_sum: 63.82991832494736
time_elpased: 2.731
batch start
#iterations: 190
currently lose_sum: 64.06209856271744
time_elpased: 2.704
batch start
#iterations: 191
currently lose_sum: 63.25567102432251
time_elpased: 2.808
batch start
#iterations: 192
currently lose_sum: 64.4760477244854
time_elpased: 2.648
batch start
#iterations: 193
currently lose_sum: 64.13397839665413
time_elpased: 2.852
batch start
#iterations: 194
currently lose_sum: 64.21386927366257
time_elpased: 2.978
batch start
#iterations: 195
currently lose_sum: 63.44309484958649
time_elpased: 2.633
batch start
#iterations: 196
currently lose_sum: 62.96767583489418
time_elpased: 2.665
batch start
#iterations: 197
currently lose_sum: 62.71783271431923
time_elpased: 2.555
batch start
#iterations: 198
currently lose_sum: 63.33210536837578
time_elpased: 3.085
batch start
#iterations: 199
currently lose_sum: 62.69633278250694
time_elpased: 2.795
start validation test
0.593762886598
0.655857385399
0.397550684368
0.49503427949
0.594107367383
95.039
batch start
#iterations: 200
currently lose_sum: 64.1754742860794
time_elpased: 2.923
batch start
#iterations: 201
currently lose_sum: 62.518951773643494
time_elpased: 2.797
batch start
#iterations: 202
currently lose_sum: 62.81404548883438
time_elpased: 2.977
batch start
#iterations: 203
currently lose_sum: 62.30016562342644
time_elpased: 3.172
batch start
#iterations: 204
currently lose_sum: 62.98930814862251
time_elpased: 3.192
batch start
#iterations: 205
currently lose_sum: 62.68454250693321
time_elpased: 2.593
batch start
#iterations: 206
currently lose_sum: 62.618971943855286
time_elpased: 2.778
batch start
#iterations: 207
currently lose_sum: 61.387213706970215
time_elpased: 2.859
batch start
#iterations: 208
currently lose_sum: 62.34188589453697
time_elpased: 2.777
batch start
#iterations: 209
currently lose_sum: 62.32479092478752
time_elpased: 2.991
batch start
#iterations: 210
currently lose_sum: 61.99372389912605
time_elpased: 2.724
batch start
#iterations: 211
currently lose_sum: 61.2651541531086
time_elpased: 2.454
batch start
#iterations: 212
currently lose_sum: 61.89162719249725
time_elpased: 3.04
batch start
#iterations: 213
currently lose_sum: 60.723080933094025
time_elpased: 3.202
batch start
#iterations: 214
currently lose_sum: 62.059597849845886
time_elpased: 3.056
batch start
#iterations: 215
currently lose_sum: 61.86107316613197
time_elpased: 2.988
batch start
#iterations: 216
currently lose_sum: 60.70382234454155
time_elpased: 2.637
batch start
#iterations: 217
currently lose_sum: 60.785641223192215
time_elpased: 3.164
batch start
#iterations: 218
currently lose_sum: 62.20267391204834
time_elpased: 2.859
batch start
#iterations: 219
currently lose_sum: 61.56888031959534
time_elpased: 2.796
start validation test
0.588505154639
0.651732586629
0.383245857775
0.482664765731
0.588865518994
96.769
batch start
#iterations: 220
currently lose_sum: 59.974959909915924
time_elpased: 3.053
batch start
#iterations: 221
currently lose_sum: 60.41507929563522
time_elpased: 3.101
batch start
#iterations: 222
currently lose_sum: 60.15796113014221
time_elpased: 3.08
batch start
#iterations: 223
currently lose_sum: 60.15743625164032
time_elpased: 2.846
batch start
#iterations: 224
currently lose_sum: 60.227330684661865
time_elpased: 2.91
batch start
#iterations: 225
currently lose_sum: 59.40096575021744
time_elpased: 3.129
batch start
#iterations: 226
currently lose_sum: 60.33640047907829
time_elpased: 3.03
batch start
#iterations: 227
currently lose_sum: 59.71935659646988
time_elpased: 2.955
batch start
#iterations: 228
currently lose_sum: 60.654129058122635
time_elpased: 3.13
batch start
#iterations: 229
currently lose_sum: 59.353917717933655
time_elpased: 2.845
batch start
#iterations: 230
currently lose_sum: 59.98893290758133
time_elpased: 2.897
batch start
#iterations: 231
currently lose_sum: 60.127686232328415
time_elpased: 2.969
batch start
#iterations: 232
currently lose_sum: 59.15344390273094
time_elpased: 2.95
batch start
#iterations: 233
currently lose_sum: 60.573862224817276
time_elpased: 2.968
batch start
#iterations: 234
currently lose_sum: 59.70452505350113
time_elpased: 2.598
batch start
#iterations: 235
currently lose_sum: 59.53046503663063
time_elpased: 2.73
batch start
#iterations: 236
currently lose_sum: 58.826955527067184
time_elpased: 2.756
batch start
#iterations: 237
currently lose_sum: 58.94533529877663
time_elpased: 2.979
batch start
#iterations: 238
currently lose_sum: 58.74887973070145
time_elpased: 2.846
batch start
#iterations: 239
currently lose_sum: 58.90777736902237
time_elpased: 2.693
start validation test
0.580463917526
0.619835965978
0.419985592261
0.500705478192
0.580745661978
96.160
batch start
#iterations: 240
currently lose_sum: 59.4738872051239
time_elpased: 2.594
batch start
#iterations: 241
currently lose_sum: 58.86544120311737
time_elpased: 2.849
batch start
#iterations: 242
currently lose_sum: 57.79332134127617
time_elpased: 2.83
batch start
#iterations: 243
currently lose_sum: 58.355908304452896
time_elpased: 2.769
batch start
#iterations: 244
currently lose_sum: 57.98570263385773
time_elpased: 3.03
batch start
#iterations: 245
currently lose_sum: 58.5354508459568
time_elpased: 2.654
batch start
#iterations: 246
currently lose_sum: 58.56369590759277
time_elpased: 2.771
batch start
#iterations: 247
currently lose_sum: 57.76773598790169
time_elpased: 2.646
batch start
#iterations: 248
currently lose_sum: 57.09173542261124
time_elpased: 2.354
batch start
#iterations: 249
currently lose_sum: 58.36853989958763
time_elpased: 2.591
batch start
#iterations: 250
currently lose_sum: 57.214018166065216
time_elpased: 2.657
batch start
#iterations: 251
currently lose_sum: 57.97337859869003
time_elpased: 2.963
batch start
#iterations: 252
currently lose_sum: 56.875130385160446
time_elpased: 2.699
batch start
#iterations: 253
currently lose_sum: 57.5084128677845
time_elpased: 2.725
batch start
#iterations: 254
currently lose_sum: 57.121728003025055
time_elpased: 2.77
batch start
#iterations: 255
currently lose_sum: 57.0487425327301
time_elpased: 2.738
batch start
#iterations: 256
currently lose_sum: 57.003025472164154
time_elpased: 2.464
batch start
#iterations: 257
currently lose_sum: 56.928217351436615
time_elpased: 2.397
batch start
#iterations: 258
currently lose_sum: 56.36119559407234
time_elpased: 2.643
batch start
#iterations: 259
currently lose_sum: 56.87963110208511
time_elpased: 2.864
start validation test
0.580773195876
0.654025670945
0.346094473603
0.452654956592
0.58118521057
112.846
batch start
#iterations: 260
currently lose_sum: 56.9668704867363
time_elpased: 2.902
batch start
#iterations: 261
currently lose_sum: 57.3246283531189
time_elpased: 2.997
batch start
#iterations: 262
currently lose_sum: 55.66752454638481
time_elpased: 2.81
batch start
#iterations: 263
currently lose_sum: 55.447428584098816
time_elpased: 2.645
batch start
#iterations: 264
currently lose_sum: 55.8296582698822
time_elpased: 2.719
batch start
#iterations: 265
currently lose_sum: 56.391165524721146
time_elpased: 2.882
batch start
#iterations: 266
currently lose_sum: 56.457350105047226
time_elpased: 2.69
batch start
#iterations: 267
currently lose_sum: 55.77011987566948
time_elpased: 2.645
batch start
#iterations: 268
currently lose_sum: 55.044165283441544
time_elpased: 2.281
batch start
#iterations: 269
currently lose_sum: 55.065440118312836
time_elpased: 2.295
batch start
#iterations: 270
currently lose_sum: 54.603198140859604
time_elpased: 2.302
batch start
#iterations: 271
currently lose_sum: 55.774569898843765
time_elpased: 2.272
batch start
#iterations: 272
currently lose_sum: 55.897914439439774
time_elpased: 2.267
batch start
#iterations: 273
currently lose_sum: 55.30073645710945
time_elpased: 2.688
batch start
#iterations: 274
currently lose_sum: 55.80986875295639
time_elpased: 3.09
batch start
#iterations: 275
currently lose_sum: 54.3368376493454
time_elpased: 2.729
batch start
#iterations: 276
currently lose_sum: 54.12872368097305
time_elpased: 2.844
batch start
#iterations: 277
currently lose_sum: 54.75589206814766
time_elpased: 2.97
batch start
#iterations: 278
currently lose_sum: 55.44103521108627
time_elpased: 2.586
batch start
#iterations: 279
currently lose_sum: 54.180252224206924
time_elpased: 2.418
start validation test
0.579329896907
0.645366218236
0.355459503962
0.458424580264
0.579722935912
113.737
batch start
#iterations: 280
currently lose_sum: 53.536915719509125
time_elpased: 2.576
batch start
#iterations: 281
currently lose_sum: 54.69179901480675
time_elpased: 2.567
batch start
#iterations: 282
currently lose_sum: 54.071454495191574
time_elpased: 2.696
batch start
#iterations: 283
currently lose_sum: 54.1947762966156
time_elpased: 2.621
batch start
#iterations: 284
currently lose_sum: 53.78080067038536
time_elpased: 2.763
batch start
#iterations: 285
currently lose_sum: 53.84080448746681
time_elpased: 2.607
batch start
#iterations: 286
currently lose_sum: 53.115759283304214
time_elpased: 2.822
batch start
#iterations: 287
currently lose_sum: 53.39147022366524
time_elpased: 2.764
batch start
#iterations: 288
currently lose_sum: 53.38609537482262
time_elpased: 2.557
batch start
#iterations: 289
currently lose_sum: 53.630891531705856
time_elpased: 2.778
batch start
#iterations: 290
currently lose_sum: 53.67684534192085
time_elpased: 2.932
batch start
#iterations: 291
currently lose_sum: 52.53498391807079
time_elpased: 2.856
batch start
#iterations: 292
currently lose_sum: 52.841895282268524
time_elpased: 2.789
batch start
#iterations: 293
currently lose_sum: 51.90720543265343
time_elpased: 2.738
batch start
#iterations: 294
currently lose_sum: 51.934414744377136
time_elpased: 2.705
batch start
#iterations: 295
currently lose_sum: 52.34466668963432
time_elpased: 2.642
batch start
#iterations: 296
currently lose_sum: 52.75622981786728
time_elpased: 2.723
batch start
#iterations: 297
currently lose_sum: 52.98229652643204
time_elpased: 2.751
batch start
#iterations: 298
currently lose_sum: 53.00749492645264
time_elpased: 2.221
batch start
#iterations: 299
currently lose_sum: 51.64890679717064
time_elpased: 2.613
start validation test
0.561649484536
0.643890865955
0.27930431203
0.389606660924
0.562145185035
125.717
batch start
#iterations: 300
currently lose_sum: 51.460643857717514
time_elpased: 2.891
batch start
#iterations: 301
currently lose_sum: 52.820834428071976
time_elpased: 2.921
batch start
#iterations: 302
currently lose_sum: 50.76874679327011
time_elpased: 2.395
batch start
#iterations: 303
currently lose_sum: 50.73569419980049
time_elpased: 2.738
batch start
#iterations: 304
currently lose_sum: 50.99653887748718
time_elpased: 2.513
batch start
#iterations: 305
currently lose_sum: 51.19059047102928
time_elpased: 2.574
batch start
#iterations: 306
currently lose_sum: 52.428954511880875
time_elpased: 2.373
batch start
#iterations: 307
currently lose_sum: 51.54511061310768
time_elpased: 2.242
batch start
#iterations: 308
currently lose_sum: 52.29714074730873
time_elpased: 2.278
batch start
#iterations: 309
currently lose_sum: 52.025938749313354
time_elpased: 2.279
batch start
#iterations: 310
currently lose_sum: 50.95006510615349
time_elpased: 2.508
batch start
#iterations: 311
currently lose_sum: 50.47578176856041
time_elpased: 2.662
batch start
#iterations: 312
currently lose_sum: 49.990119099617004
time_elpased: 2.877
batch start
#iterations: 313
currently lose_sum: 50.00467765331268
time_elpased: 2.847
batch start
#iterations: 314
currently lose_sum: 49.92912995815277
time_elpased: 2.793
batch start
#iterations: 315
currently lose_sum: 50.425888389348984
time_elpased: 2.659
batch start
#iterations: 316
currently lose_sum: 50.0259545147419
time_elpased: 2.626
batch start
#iterations: 317
currently lose_sum: 50.11167734861374
time_elpased: 2.22
batch start
#iterations: 318
currently lose_sum: 50.18311822414398
time_elpased: 2.31
batch start
#iterations: 319
currently lose_sum: 49.77192181348801
time_elpased: 2.997
start validation test
0.566494845361
0.642467843907
0.303282906247
0.412052572707
0.566956954518
128.422
batch start
#iterations: 320
currently lose_sum: 49.89635917544365
time_elpased: 3.132
batch start
#iterations: 321
currently lose_sum: 50.21179500222206
time_elpased: 3.125
batch start
#iterations: 322
currently lose_sum: 49.334152579307556
time_elpased: 2.784
batch start
#iterations: 323
currently lose_sum: 49.152538537979126
time_elpased: 3.023
batch start
#iterations: 324
currently lose_sum: 49.43297207355499
time_elpased: 2.707
batch start
#iterations: 325
currently lose_sum: 49.378355503082275
time_elpased: 3.101
batch start
#iterations: 326
currently lose_sum: 49.68293754756451
time_elpased: 3.011
batch start
#iterations: 327
currently lose_sum: 49.10329535603523
time_elpased: 2.51
batch start
#iterations: 328
currently lose_sum: 48.95020788908005
time_elpased: 2.415
batch start
#iterations: 329
currently lose_sum: 48.909187108278275
time_elpased: 2.104
batch start
#iterations: 330
currently lose_sum: 48.98035481572151
time_elpased: 2.132
batch start
#iterations: 331
currently lose_sum: 49.728605434298515
time_elpased: 2.477
batch start
#iterations: 332
currently lose_sum: 48.19318324327469
time_elpased: 2.607
batch start
#iterations: 333
currently lose_sum: 48.884638383984566
time_elpased: 3.073
batch start
#iterations: 334
currently lose_sum: 47.396991193294525
time_elpased: 2.899
batch start
#iterations: 335
currently lose_sum: 49.04350942373276
time_elpased: 2.842
batch start
#iterations: 336
currently lose_sum: 48.6837072968483
time_elpased: 2.69
batch start
#iterations: 337
currently lose_sum: 46.57610401511192
time_elpased: 2.746
batch start
#iterations: 338
currently lose_sum: 46.70309540629387
time_elpased: 2.744
batch start
#iterations: 339
currently lose_sum: 47.541600197553635
time_elpased: 2.951
start validation test
0.555257731959
0.616720257235
0.29607903674
0.40008343763
0.555712760134
130.984
batch start
#iterations: 340
currently lose_sum: 48.50827448070049
time_elpased: 3.02
batch start
#iterations: 341
currently lose_sum: 46.91293314099312
time_elpased: 2.987
batch start
#iterations: 342
currently lose_sum: 47.233335077762604
time_elpased: 2.645
batch start
#iterations: 343
currently lose_sum: 47.53598064184189
time_elpased: 2.87
batch start
#iterations: 344
currently lose_sum: 47.15180982649326
time_elpased: 2.723
batch start
#iterations: 345
currently lose_sum: 46.77375365793705
time_elpased: 3.172
batch start
#iterations: 346
currently lose_sum: 47.41778486967087
time_elpased: 3.051
batch start
#iterations: 347
currently lose_sum: 47.566031858325005
time_elpased: 2.641
batch start
#iterations: 348
currently lose_sum: 46.655287370085716
time_elpased: 2.607
batch start
#iterations: 349
currently lose_sum: 45.75422175228596
time_elpased: 2.941
batch start
#iterations: 350
currently lose_sum: 45.36870130896568
time_elpased: 2.681
batch start
#iterations: 351
currently lose_sum: 47.91376443207264
time_elpased: 2.705
batch start
#iterations: 352
currently lose_sum: 46.422815412282944
time_elpased: 2.921
batch start
#iterations: 353
currently lose_sum: 46.13146476447582
time_elpased: 2.878
batch start
#iterations: 354
currently lose_sum: 46.49597932398319
time_elpased: 3.354
batch start
#iterations: 355
currently lose_sum: 44.62676031887531
time_elpased: 2.952
batch start
#iterations: 356
currently lose_sum: 46.07949170470238
time_elpased: 2.864
batch start
#iterations: 357
currently lose_sum: 45.077191174030304
time_elpased: 2.655
batch start
#iterations: 358
currently lose_sum: 45.29539914429188
time_elpased: 3.02
batch start
#iterations: 359
currently lose_sum: 46.208041578531265
time_elpased: 2.652
start validation test
0.555103092784
0.631924198251
0.267675208398
0.376057254392
0.555607716767
137.414
batch start
#iterations: 360
currently lose_sum: 45.777937069535255
time_elpased: 2.853
batch start
#iterations: 361
currently lose_sum: 44.670041874051094
time_elpased: 2.874
batch start
#iterations: 362
currently lose_sum: 45.67776466906071
time_elpased: 2.978
batch start
#iterations: 363
currently lose_sum: 44.9044591486454
time_elpased: 3.154
batch start
#iterations: 364
currently lose_sum: 44.4242545068264
time_elpased: 2.783
batch start
#iterations: 365
currently lose_sum: 44.92608594894409
time_elpased: 3.107
batch start
#iterations: 366
currently lose_sum: 45.60441079735756
time_elpased: 2.598
batch start
#iterations: 367
currently lose_sum: 44.54873412847519
time_elpased: 2.965
batch start
#iterations: 368
currently lose_sum: 45.091796323657036
time_elpased: 3.021
batch start
#iterations: 369
currently lose_sum: 43.651222839951515
time_elpased: 2.678
batch start
#iterations: 370
currently lose_sum: 44.40047067403793
time_elpased: 2.771
batch start
#iterations: 371
currently lose_sum: 44.76188771426678
time_elpased: 2.92
batch start
#iterations: 372
currently lose_sum: 44.718654066324234
time_elpased: 2.499
batch start
#iterations: 373
currently lose_sum: 43.53348986804485
time_elpased: 2.749
batch start
#iterations: 374
currently lose_sum: 44.20100989937782
time_elpased: 2.831
batch start
#iterations: 375
currently lose_sum: 43.201232209801674
time_elpased: 2.809
batch start
#iterations: 376
currently lose_sum: 43.293615862727165
time_elpased: 2.768
batch start
#iterations: 377
currently lose_sum: 44.64624936878681
time_elpased: 2.846
batch start
#iterations: 378
currently lose_sum: 44.10350024700165
time_elpased: 2.715
batch start
#iterations: 379
currently lose_sum: 44.46858912706375
time_elpased: 2.761
start validation test
0.552680412371
0.621634277687
0.27323247916
0.379611095224
0.55317102632
137.303
batch start
#iterations: 380
currently lose_sum: 43.42497205734253
time_elpased: 2.876
batch start
#iterations: 381
currently lose_sum: 44.07637506723404
time_elpased: 2.836
batch start
#iterations: 382
currently lose_sum: 44.35645309090614
time_elpased: 2.917
batch start
#iterations: 383
currently lose_sum: 42.828255742788315
time_elpased: 2.87
batch start
#iterations: 384
currently lose_sum: 42.63252608478069
time_elpased: 2.952
batch start
#iterations: 385
currently lose_sum: 43.40424835681915
time_elpased: 2.61
batch start
#iterations: 386
currently lose_sum: 41.97734418511391
time_elpased: 2.664
batch start
#iterations: 387
currently lose_sum: 42.475816518068314
time_elpased: 2.841
batch start
#iterations: 388
currently lose_sum: 42.25761806964874
time_elpased: 2.666
batch start
#iterations: 389
currently lose_sum: 41.61032836139202
time_elpased: 2.871
batch start
#iterations: 390
currently lose_sum: 42.32875803112984
time_elpased: 2.808
batch start
#iterations: 391
currently lose_sum: 41.670835852622986
time_elpased: 2.838
batch start
#iterations: 392
currently lose_sum: 41.792161494493484
time_elpased: 2.825
batch start
#iterations: 393
currently lose_sum: 41.59461672604084
time_elpased: 2.747
batch start
#iterations: 394
currently lose_sum: 41.876471012830734
time_elpased: 2.745
batch start
#iterations: 395
currently lose_sum: 42.198087602853775
time_elpased: 2.861
batch start
#iterations: 396
currently lose_sum: 40.909353867173195
time_elpased: 2.883
batch start
#iterations: 397
currently lose_sum: 41.07049281895161
time_elpased: 2.778
batch start
#iterations: 398
currently lose_sum: 41.56548199057579
time_elpased: 2.788
batch start
#iterations: 399
currently lose_sum: 42.12761300802231
time_elpased: 2.632
start validation test
0.545927835052
0.629566210046
0.227024801894
0.333711519552
0.546487718514
153.960
acc: 0.656
pre: 0.642
rec: 0.706
F1: 0.673
auc: 0.710
