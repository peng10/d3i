start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.03421914577484
time_elpased: 2.052
batch start
#iterations: 1
currently lose_sum: 99.49698460102081
time_elpased: 2.035
batch start
#iterations: 2
currently lose_sum: 98.81993454694748
time_elpased: 1.994
batch start
#iterations: 3
currently lose_sum: 98.60357010364532
time_elpased: 2.003
batch start
#iterations: 4
currently lose_sum: 98.50142586231232
time_elpased: 2.021
batch start
#iterations: 5
currently lose_sum: 97.8939089179039
time_elpased: 2.04
batch start
#iterations: 6
currently lose_sum: 97.89209938049316
time_elpased: 2.006
batch start
#iterations: 7
currently lose_sum: 97.8322274684906
time_elpased: 2.025
batch start
#iterations: 8
currently lose_sum: 97.19037073850632
time_elpased: 1.995
batch start
#iterations: 9
currently lose_sum: 96.97313314676285
time_elpased: 2.005
batch start
#iterations: 10
currently lose_sum: 97.20299363136292
time_elpased: 2.011
batch start
#iterations: 11
currently lose_sum: 96.7186895608902
time_elpased: 2.025
batch start
#iterations: 12
currently lose_sum: 97.0823490023613
time_elpased: 2.045
batch start
#iterations: 13
currently lose_sum: 95.56186389923096
time_elpased: 1.989
batch start
#iterations: 14
currently lose_sum: 97.78725987672806
time_elpased: 2.012
batch start
#iterations: 15
currently lose_sum: 96.11953884363174
time_elpased: 2.022
batch start
#iterations: 16
currently lose_sum: 97.00730562210083
time_elpased: 2.036
batch start
#iterations: 17
currently lose_sum: 96.35838133096695
time_elpased: 2.033
batch start
#iterations: 18
currently lose_sum: 95.94294077157974
time_elpased: 1.985
batch start
#iterations: 19
currently lose_sum: 96.46117264032364
time_elpased: 2.022
start validation test
0.530206185567
0.676832844575
0.118760934445
0.202066188058
0.530928541166
66.150
batch start
#iterations: 20
currently lose_sum: 96.33749842643738
time_elpased: 2.009
batch start
#iterations: 21
currently lose_sum: 96.49460363388062
time_elpased: 2.04
batch start
#iterations: 22
currently lose_sum: 95.54972213506699
time_elpased: 2.019
batch start
#iterations: 23
currently lose_sum: 95.19626331329346
time_elpased: 2.008
batch start
#iterations: 24
currently lose_sum: 96.8703140616417
time_elpased: 2.026
batch start
#iterations: 25
currently lose_sum: 97.2423922419548
time_elpased: 2.004
batch start
#iterations: 26
currently lose_sum: 95.83109694719315
time_elpased: 2.072
batch start
#iterations: 27
currently lose_sum: 96.60087156295776
time_elpased: 2.02
batch start
#iterations: 28
currently lose_sum: 94.363212287426
time_elpased: 2.011
batch start
#iterations: 29
currently lose_sum: 94.99451947212219
time_elpased: 2.044
batch start
#iterations: 30
currently lose_sum: 95.2209090590477
time_elpased: 2.037
batch start
#iterations: 31
currently lose_sum: 94.6923435330391
time_elpased: 1.992
batch start
#iterations: 32
currently lose_sum: 95.21276688575745
time_elpased: 2.01
batch start
#iterations: 33
currently lose_sum: 95.5785476565361
time_elpased: 1.998
batch start
#iterations: 34
currently lose_sum: 94.25178456306458
time_elpased: 1.996
batch start
#iterations: 35
currently lose_sum: 94.87730920314789
time_elpased: 2.014
batch start
#iterations: 36
currently lose_sum: 94.3778932094574
time_elpased: 1.99
batch start
#iterations: 37
currently lose_sum: 93.62072056531906
time_elpased: 1.988
batch start
#iterations: 38
currently lose_sum: 93.54140168428421
time_elpased: 1.979
batch start
#iterations: 39
currently lose_sum: 94.54858231544495
time_elpased: 2.039
start validation test
0.663505154639
0.67710763079
0.627354121642
0.651282051282
0.663568623354
59.743
batch start
#iterations: 40
currently lose_sum: 94.82358241081238
time_elpased: 2.024
batch start
#iterations: 41
currently lose_sum: 96.6154590845108
time_elpased: 2.033
batch start
#iterations: 42
currently lose_sum: 95.11523908376694
time_elpased: 2.025
batch start
#iterations: 43
currently lose_sum: 93.19870388507843
time_elpased: 1.991
batch start
#iterations: 44
currently lose_sum: 93.57833802700043
time_elpased: 2.02
batch start
#iterations: 45
currently lose_sum: 97.15383726358414
time_elpased: 2.014
batch start
#iterations: 46
currently lose_sum: 98.17566812038422
time_elpased: 1.977
batch start
#iterations: 47
currently lose_sum: 93.83256387710571
time_elpased: 2.0
batch start
#iterations: 48
currently lose_sum: 95.0527862906456
time_elpased: 2.048
batch start
#iterations: 49
currently lose_sum: 94.73317098617554
time_elpased: 2.01
batch start
#iterations: 50
currently lose_sum: 93.9377692937851
time_elpased: 2.026
batch start
#iterations: 51
currently lose_sum: 99.5520783662796
time_elpased: 2.071
batch start
#iterations: 52
currently lose_sum: 94.9813626408577
time_elpased: 2.022
batch start
#iterations: 53
currently lose_sum: 93.74305361509323
time_elpased: 2.023
batch start
#iterations: 54
currently lose_sum: 94.12289214134216
time_elpased: 2.019
batch start
#iterations: 55
currently lose_sum: 93.77587622404099
time_elpased: 2.059
batch start
#iterations: 56
currently lose_sum: 94.43317806720734
time_elpased: 1.997
batch start
#iterations: 57
currently lose_sum: 99.76988625526428
time_elpased: 2.031
batch start
#iterations: 58
currently lose_sum: 93.5462201833725
time_elpased: 2.008
batch start
#iterations: 59
currently lose_sum: 93.22705692052841
time_elpased: 2.068
start validation test
0.680309278351
0.631500187056
0.868580837707
0.731305779395
0.679978738589
59.263
batch start
#iterations: 60
currently lose_sum: 93.95766538381577
time_elpased: 2.006
batch start
#iterations: 61
currently lose_sum: 93.66347771883011
time_elpased: 2.017
batch start
#iterations: 62
currently lose_sum: 93.18066781759262
time_elpased: 1.991
batch start
#iterations: 63
currently lose_sum: 92.92765778303146
time_elpased: 2.018
batch start
#iterations: 64
currently lose_sum: 92.71042144298553
time_elpased: 2.032
batch start
#iterations: 65
currently lose_sum: 92.20562672615051
time_elpased: 2.017
batch start
#iterations: 66
currently lose_sum: 93.89708930253983
time_elpased: 2.076
batch start
#iterations: 67
currently lose_sum: 95.86252355575562
time_elpased: 2.029
batch start
#iterations: 68
currently lose_sum: 97.8275836110115
time_elpased: 1.977
batch start
#iterations: 69
currently lose_sum: 94.25586742162704
time_elpased: 2.004
batch start
#iterations: 70
currently lose_sum: 92.44764578342438
time_elpased: 2.024
batch start
#iterations: 71
currently lose_sum: 93.40776717662811
time_elpased: 2.046
batch start
#iterations: 72
currently lose_sum: 91.7551736831665
time_elpased: 2.047
batch start
#iterations: 73
currently lose_sum: 98.30703020095825
time_elpased: 1.997
batch start
#iterations: 74
currently lose_sum: 98.060966193676
time_elpased: 2.015
batch start
#iterations: 75
currently lose_sum: 93.22074162960052
time_elpased: 2.048
batch start
#iterations: 76
currently lose_sum: 93.1997349858284
time_elpased: 1.98
batch start
#iterations: 77
currently lose_sum: 92.51379406452179
time_elpased: 2.029
batch start
#iterations: 78
currently lose_sum: 93.10395920276642
time_elpased: 1.994
batch start
#iterations: 79
currently lose_sum: 91.9316121339798
time_elpased: 2.014
start validation test
0.637783505155
0.690834279228
0.501080580426
0.580852967492
0.638023508224
60.706
batch start
#iterations: 80
currently lose_sum: 92.78861683607101
time_elpased: 2.021
batch start
#iterations: 81
currently lose_sum: 98.29751515388489
time_elpased: 1.996
batch start
#iterations: 82
currently lose_sum: 100.50156950950623
time_elpased: 1.987
batch start
#iterations: 83
currently lose_sum: 100.35972237586975
time_elpased: 2.053
batch start
#iterations: 84
currently lose_sum: 94.06927049160004
time_elpased: 1.982
batch start
#iterations: 85
currently lose_sum: 92.22471863031387
time_elpased: 2.076
batch start
#iterations: 86
currently lose_sum: 94.03647756576538
time_elpased: 2.044
batch start
#iterations: 87
currently lose_sum: 92.83050203323364
time_elpased: 2.025
batch start
#iterations: 88
currently lose_sum: 92.11845201253891
time_elpased: 2.022
batch start
#iterations: 89
currently lose_sum: 91.46733546257019
time_elpased: 2.061
batch start
#iterations: 90
currently lose_sum: 92.31638264656067
time_elpased: 2.02
batch start
#iterations: 91
currently lose_sum: 91.028539955616
time_elpased: 2.018
batch start
#iterations: 92
currently lose_sum: 92.12193250656128
time_elpased: 1.999
batch start
#iterations: 93
currently lose_sum: 90.98777312040329
time_elpased: 2.1
batch start
#iterations: 94
currently lose_sum: 91.32155233621597
time_elpased: 1.978
batch start
#iterations: 95
currently lose_sum: 91.55615663528442
time_elpased: 2.047
batch start
#iterations: 96
currently lose_sum: 91.97299593687057
time_elpased: 2.031
batch start
#iterations: 97
currently lose_sum: 90.89814579486847
time_elpased: 2.025
batch start
#iterations: 98
currently lose_sum: 91.84865802526474
time_elpased: 2.093
batch start
#iterations: 99
currently lose_sum: 92.01711177825928
time_elpased: 2.002
start validation test
0.603865979381
0.698360015619
0.36811773181
0.482107958757
0.604279871792
66.272
batch start
#iterations: 100
currently lose_sum: 91.6891114115715
time_elpased: 2.056
batch start
#iterations: 101
currently lose_sum: 91.53658419847488
time_elpased: 2.034
batch start
#iterations: 102
currently lose_sum: 90.24957007169724
time_elpased: 2.003
batch start
#iterations: 103
currently lose_sum: 91.91780638694763
time_elpased: 2.013
batch start
#iterations: 104
currently lose_sum: 89.41229057312012
time_elpased: 2.013
batch start
#iterations: 105
currently lose_sum: 90.81776744127274
time_elpased: 2.039
batch start
#iterations: 106
currently lose_sum: 90.09109395742416
time_elpased: 2.0
batch start
#iterations: 107
currently lose_sum: 90.41827410459518
time_elpased: 2.045
batch start
#iterations: 108
currently lose_sum: 90.21168464422226
time_elpased: 1.996
batch start
#iterations: 109
currently lose_sum: 90.3589077591896
time_elpased: 1.997
batch start
#iterations: 110
currently lose_sum: 90.85614615678787
time_elpased: 2.0
batch start
#iterations: 111
currently lose_sum: 90.14357787370682
time_elpased: 2.053
batch start
#iterations: 112
currently lose_sum: 91.15270602703094
time_elpased: 2.019
batch start
#iterations: 113
currently lose_sum: 90.58389836549759
time_elpased: 2.027
batch start
#iterations: 114
currently lose_sum: 89.62463790178299
time_elpased: 2.024
batch start
#iterations: 115
currently lose_sum: 93.26059597730637
time_elpased: 1.995
batch start
#iterations: 116
currently lose_sum: 90.99452501535416
time_elpased: 2.008
batch start
#iterations: 117
currently lose_sum: 90.60524159669876
time_elpased: 2.087
batch start
#iterations: 118
currently lose_sum: 90.90508133172989
time_elpased: 1.988
batch start
#iterations: 119
currently lose_sum: 90.68366193771362
time_elpased: 2.019
start validation test
0.645206185567
0.637626262626
0.67562004734
0.656073552191
0.645152789342
58.890
batch start
#iterations: 120
currently lose_sum: 90.221226811409
time_elpased: 2.026
batch start
#iterations: 121
currently lose_sum: 90.1389769911766
time_elpased: 2.023
batch start
#iterations: 122
currently lose_sum: 89.88628435134888
time_elpased: 2.003
batch start
#iterations: 123
currently lose_sum: 90.27103173732758
time_elpased: 2.031
batch start
#iterations: 124
currently lose_sum: 89.70871835947037
time_elpased: 2.011
batch start
#iterations: 125
currently lose_sum: 90.19437348842621
time_elpased: 2.028
batch start
#iterations: 126
currently lose_sum: 90.7099398970604
time_elpased: 2.063
batch start
#iterations: 127
currently lose_sum: 91.68784320354462
time_elpased: 2.066
batch start
#iterations: 128
currently lose_sum: 98.87734085321426
time_elpased: 2.077
batch start
#iterations: 129
currently lose_sum: 90.22321248054504
time_elpased: 2.018
batch start
#iterations: 130
currently lose_sum: 90.62090814113617
time_elpased: 2.057
batch start
#iterations: 131
currently lose_sum: 89.84064662456512
time_elpased: 2.066
batch start
#iterations: 132
currently lose_sum: 89.25047397613525
time_elpased: 2.043
batch start
#iterations: 133
currently lose_sum: 90.90836107730865
time_elpased: 2.005
batch start
#iterations: 134
currently lose_sum: 89.71836966276169
time_elpased: 2.058
batch start
#iterations: 135
currently lose_sum: 89.34352546930313
time_elpased: 2.051
batch start
#iterations: 136
currently lose_sum: 89.96430826187134
time_elpased: 2.11
batch start
#iterations: 137
currently lose_sum: 89.79084259271622
time_elpased: 2.049
batch start
#iterations: 138
currently lose_sum: 89.55536329746246
time_elpased: 2.026
batch start
#iterations: 139
currently lose_sum: 90.09237259626389
time_elpased: 2.069
start validation test
0.689742268041
0.676701070336
0.728722856849
0.701749170011
0.689673831605
56.880
batch start
#iterations: 140
currently lose_sum: 89.35884642601013
time_elpased: 2.058
batch start
#iterations: 141
currently lose_sum: 89.39261609315872
time_elpased: 2.065
batch start
#iterations: 142
currently lose_sum: 89.22609388828278
time_elpased: 2.051
batch start
#iterations: 143
currently lose_sum: 88.49614930152893
time_elpased: 2.063
batch start
#iterations: 144
currently lose_sum: 89.55164968967438
time_elpased: 2.062
batch start
#iterations: 145
currently lose_sum: 89.46894717216492
time_elpased: 2.069
batch start
#iterations: 146
currently lose_sum: 88.90031164884567
time_elpased: 2.011
batch start
#iterations: 147
currently lose_sum: 88.73117256164551
time_elpased: 2.088
batch start
#iterations: 148
currently lose_sum: 89.8926956653595
time_elpased: 2.054
batch start
#iterations: 149
currently lose_sum: 88.90711760520935
time_elpased: 2.047
batch start
#iterations: 150
currently lose_sum: 89.36980277299881
time_elpased: 2.021
batch start
#iterations: 151
currently lose_sum: 88.94114971160889
time_elpased: 2.045
batch start
#iterations: 152
currently lose_sum: 89.14856719970703
time_elpased: 2.059
batch start
#iterations: 153
currently lose_sum: 88.6903635263443
time_elpased: 2.055
batch start
#iterations: 154
currently lose_sum: 90.39706265926361
time_elpased: 2.022
batch start
#iterations: 155
currently lose_sum: 88.11080294847488
time_elpased: 2.044
batch start
#iterations: 156
currently lose_sum: 88.42871177196503
time_elpased: 2.062
batch start
#iterations: 157
currently lose_sum: 88.60162025690079
time_elpased: 2.079
batch start
#iterations: 158
currently lose_sum: 88.11393511295319
time_elpased: 2.042
batch start
#iterations: 159
currently lose_sum: 88.56545376777649
time_elpased: 2.064
start validation test
0.680670103093
0.68244923332
0.677884120613
0.680159016986
0.680674994315
57.178
batch start
#iterations: 160
currently lose_sum: 88.9032369852066
time_elpased: 2.044
batch start
#iterations: 161
currently lose_sum: 89.40333616733551
time_elpased: 2.045
batch start
#iterations: 162
currently lose_sum: 89.34509629011154
time_elpased: 2.081
batch start
#iterations: 163
currently lose_sum: 89.62781178951263
time_elpased: 2.032
batch start
#iterations: 164
currently lose_sum: 88.17140543460846
time_elpased: 2.031
batch start
#iterations: 165
currently lose_sum: 88.08266496658325
time_elpased: 2.102
batch start
#iterations: 166
currently lose_sum: 88.30502223968506
time_elpased: 2.045
batch start
#iterations: 167
currently lose_sum: 88.44236129522324
time_elpased: 2.1
batch start
#iterations: 168
currently lose_sum: 87.97566092014313
time_elpased: 2.051
batch start
#iterations: 169
currently lose_sum: 88.65931296348572
time_elpased: 2.072
batch start
#iterations: 170
currently lose_sum: 88.38254922628403
time_elpased: 2.086
batch start
#iterations: 171
currently lose_sum: 88.05567163228989
time_elpased: 2.047
batch start
#iterations: 172
currently lose_sum: 87.65682953596115
time_elpased: 2.041
batch start
#iterations: 173
currently lose_sum: 87.79011207818985
time_elpased: 2.066
batch start
#iterations: 174
currently lose_sum: 88.17912918329239
time_elpased: 2.025
batch start
#iterations: 175
currently lose_sum: 87.35540395975113
time_elpased: 2.026
batch start
#iterations: 176
currently lose_sum: 87.66738396883011
time_elpased: 2.055
batch start
#iterations: 177
currently lose_sum: 87.77253544330597
time_elpased: 2.042
batch start
#iterations: 178
currently lose_sum: 87.46424949169159
time_elpased: 2.051
batch start
#iterations: 179
currently lose_sum: 87.81629204750061
time_elpased: 2.063
start validation test
0.657525773196
0.622537682431
0.803334362458
0.701473759885
0.657269783728
58.549
batch start
#iterations: 180
currently lose_sum: 88.14584571123123
time_elpased: 2.043
batch start
#iterations: 181
currently lose_sum: 87.54102826118469
time_elpased: 2.034
batch start
#iterations: 182
currently lose_sum: 87.6520094871521
time_elpased: 2.074
batch start
#iterations: 183
currently lose_sum: 87.16116118431091
time_elpased: 2.067
batch start
#iterations: 184
currently lose_sum: 87.96029168367386
time_elpased: 2.03
batch start
#iterations: 185
currently lose_sum: 87.61467009782791
time_elpased: 2.048
batch start
#iterations: 186
currently lose_sum: 88.2008570432663
time_elpased: 2.084
batch start
#iterations: 187
currently lose_sum: 87.20630502700806
time_elpased: 2.045
batch start
#iterations: 188
currently lose_sum: 87.4348372220993
time_elpased: 2.054
batch start
#iterations: 189
currently lose_sum: 88.47498321533203
time_elpased: 2.068
batch start
#iterations: 190
currently lose_sum: 87.5576206445694
time_elpased: 2.078
batch start
#iterations: 191
currently lose_sum: 86.2475233078003
time_elpased: 2.075
batch start
#iterations: 192
currently lose_sum: 87.67731010913849
time_elpased: 2.05
batch start
#iterations: 193
currently lose_sum: 87.16016632318497
time_elpased: 2.033
batch start
#iterations: 194
currently lose_sum: 88.29419708251953
time_elpased: 2.049
batch start
#iterations: 195
currently lose_sum: 87.48900729417801
time_elpased: 2.056
batch start
#iterations: 196
currently lose_sum: 87.87582808732986
time_elpased: 2.027
batch start
#iterations: 197
currently lose_sum: 86.95713871717453
time_elpased: 2.026
batch start
#iterations: 198
currently lose_sum: 87.15725523233414
time_elpased: 2.093
batch start
#iterations: 199
currently lose_sum: 86.97726327180862
time_elpased: 2.101
start validation test
0.671494845361
0.670160797883
0.67767829577
0.673898582613
0.67148398936
57.853
batch start
#iterations: 200
currently lose_sum: 86.67879545688629
time_elpased: 2.033
batch start
#iterations: 201
currently lose_sum: 87.84836161136627
time_elpased: 2.106
batch start
#iterations: 202
currently lose_sum: 87.10172671079636
time_elpased: 2.043
batch start
#iterations: 203
currently lose_sum: 87.06264060735703
time_elpased: 2.057
batch start
#iterations: 204
currently lose_sum: 87.33574932813644
time_elpased: 2.008
batch start
#iterations: 205
currently lose_sum: 87.16325187683105
time_elpased: 2.084
batch start
#iterations: 206
currently lose_sum: 87.02070736885071
time_elpased: 2.052
batch start
#iterations: 207
currently lose_sum: 86.53289222717285
time_elpased: 2.087
batch start
#iterations: 208
currently lose_sum: 86.89129036664963
time_elpased: 2.068
batch start
#iterations: 209
currently lose_sum: 86.3635635972023
time_elpased: 2.072
batch start
#iterations: 210
currently lose_sum: 85.82244259119034
time_elpased: 2.075
batch start
#iterations: 211
currently lose_sum: 86.63414514064789
time_elpased: 2.075
batch start
#iterations: 212
currently lose_sum: 85.80750226974487
time_elpased: 2.037
batch start
#iterations: 213
currently lose_sum: 87.3294249176979
time_elpased: 2.109
batch start
#iterations: 214
currently lose_sum: 86.0619044303894
time_elpased: 2.095
batch start
#iterations: 215
currently lose_sum: 87.83315145969391
time_elpased: 2.108
batch start
#iterations: 216
currently lose_sum: 86.95652741193771
time_elpased: 2.026
batch start
#iterations: 217
currently lose_sum: 85.67784178256989
time_elpased: 2.012
batch start
#iterations: 218
currently lose_sum: 88.24424064159393
time_elpased: 2.07
batch start
#iterations: 219
currently lose_sum: 86.93092459440231
time_elpased: 2.062
start validation test
0.658711340206
0.630522765599
0.769579088196
0.693145478982
0.658516694774
58.512
batch start
#iterations: 220
currently lose_sum: 86.7645897269249
time_elpased: 2.058
batch start
#iterations: 221
currently lose_sum: 86.88241529464722
time_elpased: 2.084
batch start
#iterations: 222
currently lose_sum: 86.14549040794373
time_elpased: 2.037
batch start
#iterations: 223
currently lose_sum: 85.0055992603302
time_elpased: 2.064
batch start
#iterations: 224
currently lose_sum: 86.13300001621246
time_elpased: 2.076
batch start
#iterations: 225
currently lose_sum: 86.18455070257187
time_elpased: 2.068
batch start
#iterations: 226
currently lose_sum: 85.468410551548
time_elpased: 2.039
batch start
#iterations: 227
currently lose_sum: 86.0491338968277
time_elpased: 2.069
batch start
#iterations: 228
currently lose_sum: 85.3973001241684
time_elpased: 2.051
batch start
#iterations: 229
currently lose_sum: 84.46676933765411
time_elpased: 2.023
batch start
#iterations: 230
currently lose_sum: 85.93122005462646
time_elpased: 2.018
batch start
#iterations: 231
currently lose_sum: 86.54848170280457
time_elpased: 2.081
batch start
#iterations: 232
currently lose_sum: 84.81099426746368
time_elpased: 2.045
batch start
#iterations: 233
currently lose_sum: 85.30451822280884
time_elpased: 2.051
batch start
#iterations: 234
currently lose_sum: 86.7587319612503
time_elpased: 2.105
batch start
#iterations: 235
currently lose_sum: 86.77416354417801
time_elpased: 2.068
batch start
#iterations: 236
currently lose_sum: 85.05464971065521
time_elpased: 2.054
batch start
#iterations: 237
currently lose_sum: 85.63544631004333
time_elpased: 2.029
batch start
#iterations: 238
currently lose_sum: 86.59211647510529
time_elpased: 2.043
batch start
#iterations: 239
currently lose_sum: 84.5934725701809
time_elpased: 2.042
start validation test
0.685
0.67933160931
0.702891839045
0.69091093015
0.684968588117
56.961
batch start
#iterations: 240
currently lose_sum: 85.51599901914597
time_elpased: 2.021
batch start
#iterations: 241
currently lose_sum: 85.10947686433792
time_elpased: 2.092
batch start
#iterations: 242
currently lose_sum: 84.81162858009338
time_elpased: 2.07
batch start
#iterations: 243
currently lose_sum: 85.4244110584259
time_elpased: 2.074
batch start
#iterations: 244
currently lose_sum: 84.99496459960938
time_elpased: 2.031
batch start
#iterations: 245
currently lose_sum: 85.58738201856613
time_elpased: 2.047
batch start
#iterations: 246
currently lose_sum: 85.97348511219025
time_elpased: 2.044
batch start
#iterations: 247
currently lose_sum: 85.21780443191528
time_elpased: 2.093
batch start
#iterations: 248
currently lose_sum: 85.3720675110817
time_elpased: 2.046
batch start
#iterations: 249
currently lose_sum: 84.73223730921745
time_elpased: 2.072
batch start
#iterations: 250
currently lose_sum: 83.94817054271698
time_elpased: 2.112
batch start
#iterations: 251
currently lose_sum: 85.71364206075668
time_elpased: 2.049
batch start
#iterations: 252
currently lose_sum: 84.35114485025406
time_elpased: 2.053
batch start
#iterations: 253
currently lose_sum: 83.96808159351349
time_elpased: 2.052
batch start
#iterations: 254
currently lose_sum: 85.19301354885101
time_elpased: 2.058
batch start
#iterations: 255
currently lose_sum: 84.625512778759
time_elpased: 2.084
batch start
#iterations: 256
currently lose_sum: 84.08187538385391
time_elpased: 2.07
batch start
#iterations: 257
currently lose_sum: 84.56211590766907
time_elpased: 2.117
batch start
#iterations: 258
currently lose_sum: 85.49970507621765
time_elpased: 2.118
batch start
#iterations: 259
currently lose_sum: 85.43530005216599
time_elpased: 2.077
start validation test
0.676030927835
0.683568677792
0.657610373572
0.670338316286
0.676063267959
58.433
batch start
#iterations: 260
currently lose_sum: 83.95561200380325
time_elpased: 2.05
batch start
#iterations: 261
currently lose_sum: 84.300009816885
time_elpased: 2.043
batch start
#iterations: 262
currently lose_sum: 85.22624272108078
time_elpased: 2.052
batch start
#iterations: 263
currently lose_sum: 85.54194611310959
time_elpased: 2.079
batch start
#iterations: 264
currently lose_sum: 85.0910672545433
time_elpased: 2.034
batch start
#iterations: 265
currently lose_sum: 83.53536891937256
time_elpased: 2.059
batch start
#iterations: 266
currently lose_sum: 84.28545695543289
time_elpased: 2.038
batch start
#iterations: 267
currently lose_sum: 84.23704850673676
time_elpased: 2.101
batch start
#iterations: 268
currently lose_sum: 83.9101602435112
time_elpased: 2.068
batch start
#iterations: 269
currently lose_sum: 84.17372277379036
time_elpased: 2.074
batch start
#iterations: 270
currently lose_sum: 85.52821379899979
time_elpased: 2.062
batch start
#iterations: 271
currently lose_sum: 84.6272554397583
time_elpased: 2.136
batch start
#iterations: 272
currently lose_sum: 83.20770132541656
time_elpased: 2.094
batch start
#iterations: 273
currently lose_sum: 84.39169424772263
time_elpased: 2.068
batch start
#iterations: 274
currently lose_sum: 84.87945210933685
time_elpased: 2.093
batch start
#iterations: 275
currently lose_sum: 84.3866765499115
time_elpased: 2.057
batch start
#iterations: 276
currently lose_sum: 83.36159738898277
time_elpased: 2.019
batch start
#iterations: 277
currently lose_sum: 84.02772590517998
time_elpased: 2.097
batch start
#iterations: 278
currently lose_sum: 84.43554550409317
time_elpased: 2.048
batch start
#iterations: 279
currently lose_sum: 83.53734743595123
time_elpased: 2.072
start validation test
0.656391752577
0.673057288712
0.610579396933
0.640297863156
0.656472183234
59.405
batch start
#iterations: 280
currently lose_sum: 83.82137358188629
time_elpased: 2.074
batch start
#iterations: 281
currently lose_sum: 82.68527835607529
time_elpased: 2.017
batch start
#iterations: 282
currently lose_sum: 84.0887758731842
time_elpased: 2.072
batch start
#iterations: 283
currently lose_sum: 83.4501713514328
time_elpased: 2.069
batch start
#iterations: 284
currently lose_sum: 81.98213896155357
time_elpased: 2.042
batch start
#iterations: 285
currently lose_sum: 83.72099849581718
time_elpased: 2.025
batch start
#iterations: 286
currently lose_sum: 82.83104845881462
time_elpased: 2.047
batch start
#iterations: 287
currently lose_sum: 82.90941748023033
time_elpased: 2.036
batch start
#iterations: 288
currently lose_sum: 83.4865807890892
time_elpased: 2.071
batch start
#iterations: 289
currently lose_sum: 82.65657645463943
time_elpased: 2.025
batch start
#iterations: 290
currently lose_sum: 81.93838450312614
time_elpased: 2.03
batch start
#iterations: 291
currently lose_sum: 82.92801144719124
time_elpased: 2.047
batch start
#iterations: 292
currently lose_sum: 83.85831251740456
time_elpased: 2.078
batch start
#iterations: 293
currently lose_sum: 82.37973567843437
time_elpased: 2.095
batch start
#iterations: 294
currently lose_sum: 83.3181648850441
time_elpased: 2.069
batch start
#iterations: 295
currently lose_sum: 82.61816081404686
time_elpased: 2.086
batch start
#iterations: 296
currently lose_sum: 81.70117577910423
time_elpased: 2.088
batch start
#iterations: 297
currently lose_sum: 81.34729313850403
time_elpased: 2.029
batch start
#iterations: 298
currently lose_sum: 82.7941447198391
time_elpased: 2.046
batch start
#iterations: 299
currently lose_sum: 83.18072468042374
time_elpased: 2.065
start validation test
0.618195876289
0.596733668342
0.733251003396
0.657985870619
0.617993879267
62.564
batch start
#iterations: 300
currently lose_sum: 84.03433310985565
time_elpased: 2.091
batch start
#iterations: 301
currently lose_sum: 83.01781192421913
time_elpased: 2.045
batch start
#iterations: 302
currently lose_sum: 82.24184793233871
time_elpased: 2.02
batch start
#iterations: 303
currently lose_sum: 83.00281858444214
time_elpased: 2.08
batch start
#iterations: 304
currently lose_sum: 82.52511101961136
time_elpased: 2.056
batch start
#iterations: 305
currently lose_sum: 82.00275826454163
time_elpased: 2.075
batch start
#iterations: 306
currently lose_sum: 82.3046547472477
time_elpased: 2.057
batch start
#iterations: 307
currently lose_sum: 81.54756596684456
time_elpased: 2.051
batch start
#iterations: 308
currently lose_sum: 81.98550528287888
time_elpased: 2.035
batch start
#iterations: 309
currently lose_sum: 83.05202382802963
time_elpased: 2.087
batch start
#iterations: 310
currently lose_sum: 82.43724691867828
time_elpased: 2.083
batch start
#iterations: 311
currently lose_sum: 81.38018530607224
time_elpased: 2.056
batch start
#iterations: 312
currently lose_sum: 81.21795588731766
time_elpased: 2.047
batch start
#iterations: 313
currently lose_sum: 81.49086964130402
time_elpased: 2.052
batch start
#iterations: 314
currently lose_sum: 81.90742436051369
time_elpased: 2.054
batch start
#iterations: 315
currently lose_sum: 81.07965338230133
time_elpased: 2.006
batch start
#iterations: 316
currently lose_sum: 80.46856766939163
time_elpased: 2.06
batch start
#iterations: 317
currently lose_sum: 81.35421979427338
time_elpased: 2.06
batch start
#iterations: 318
currently lose_sum: 81.39015156030655
time_elpased: 2.079
batch start
#iterations: 319
currently lose_sum: 80.62120744585991
time_elpased: 2.082
start validation test
0.644742268041
0.682990024615
0.542554286302
0.604725854554
0.644921674805
62.273
batch start
#iterations: 320
currently lose_sum: 80.63139554858208
time_elpased: 2.104
batch start
#iterations: 321
currently lose_sum: 81.21333423256874
time_elpased: 2.006
batch start
#iterations: 322
currently lose_sum: 80.44061359763145
time_elpased: 2.114
batch start
#iterations: 323
currently lose_sum: 80.81934988498688
time_elpased: 2.115
batch start
#iterations: 324
currently lose_sum: 80.80712080001831
time_elpased: 2.049
batch start
#iterations: 325
currently lose_sum: 79.60529765486717
time_elpased: 2.051
batch start
#iterations: 326
currently lose_sum: 81.48662087321281
time_elpased: 2.072
batch start
#iterations: 327
currently lose_sum: 81.8910616338253
time_elpased: 2.053
batch start
#iterations: 328
currently lose_sum: 80.86670434474945
time_elpased: 2.067
batch start
#iterations: 329
currently lose_sum: 79.74542579054832
time_elpased: 2.131
batch start
#iterations: 330
currently lose_sum: 79.67135632038116
time_elpased: 2.083
batch start
#iterations: 331
currently lose_sum: 80.43137383460999
time_elpased: 2.075
batch start
#iterations: 332
currently lose_sum: 79.76310297846794
time_elpased: 2.061
batch start
#iterations: 333
currently lose_sum: 79.87497651576996
time_elpased: 2.073
batch start
#iterations: 334
currently lose_sum: 79.94802251458168
time_elpased: 2.078
batch start
#iterations: 335
currently lose_sum: 81.61164698004723
time_elpased: 2.052
batch start
#iterations: 336
currently lose_sum: 79.35237923264503
time_elpased: 2.076
batch start
#iterations: 337
currently lose_sum: 80.2871142923832
time_elpased: 2.104
batch start
#iterations: 338
currently lose_sum: 80.50495260953903
time_elpased: 2.068
batch start
#iterations: 339
currently lose_sum: 79.46731492877007
time_elpased: 2.134
start validation test
0.669587628866
0.706506806544
0.582175568591
0.638343489054
0.66974109422
64.683
batch start
#iterations: 340
currently lose_sum: 79.59059226512909
time_elpased: 2.043
batch start
#iterations: 341
currently lose_sum: 81.62150511145592
time_elpased: 2.061
batch start
#iterations: 342
currently lose_sum: 78.74261063337326
time_elpased: 2.045
batch start
#iterations: 343
currently lose_sum: 78.67106533050537
time_elpased: 2.052
batch start
#iterations: 344
currently lose_sum: 80.11456346511841
time_elpased: 2.049
batch start
#iterations: 345
currently lose_sum: 79.81196311116219
time_elpased: 2.021
batch start
#iterations: 346
currently lose_sum: 79.05983886122704
time_elpased: 2.106
batch start
#iterations: 347
currently lose_sum: 78.95133703947067
time_elpased: 2.121
batch start
#iterations: 348
currently lose_sum: 78.66404691338539
time_elpased: 2.091
batch start
#iterations: 349
currently lose_sum: 78.41977402567863
time_elpased: 2.042
batch start
#iterations: 350
currently lose_sum: 80.08542060852051
time_elpased: 2.07
batch start
#iterations: 351
currently lose_sum: 78.48559013009071
time_elpased: 2.045
batch start
#iterations: 352
currently lose_sum: 78.5054047703743
time_elpased: 2.038
batch start
#iterations: 353
currently lose_sum: 78.40610566735268
time_elpased: 2.074
batch start
#iterations: 354
currently lose_sum: 78.70458197593689
time_elpased: 2.084
batch start
#iterations: 355
currently lose_sum: 78.43207624554634
time_elpased: 2.034
batch start
#iterations: 356
currently lose_sum: 78.894560277462
time_elpased: 2.059
batch start
#iterations: 357
currently lose_sum: 78.17897853255272
time_elpased: 2.039
batch start
#iterations: 358
currently lose_sum: 78.52402487397194
time_elpased: 2.066
batch start
#iterations: 359
currently lose_sum: 78.71740299463272
time_elpased: 2.054
start validation test
0.660309278351
0.645023652722
0.715652979315
0.678505220021
0.660212113947
60.093
batch start
#iterations: 360
currently lose_sum: 77.3671188056469
time_elpased: 2.09
batch start
#iterations: 361
currently lose_sum: 77.78648880124092
time_elpased: 2.04
batch start
#iterations: 362
currently lose_sum: 78.48420649766922
time_elpased: 2.085
batch start
#iterations: 363
currently lose_sum: 76.79947453737259
time_elpased: 2.029
batch start
#iterations: 364
currently lose_sum: 76.7647243142128
time_elpased: 2.094
batch start
#iterations: 365
currently lose_sum: 77.89046901464462
time_elpased: 2.054
batch start
#iterations: 366
currently lose_sum: 77.6687608063221
time_elpased: 2.035
batch start
#iterations: 367
currently lose_sum: 76.32587614655495
time_elpased: 2.041
batch start
#iterations: 368
currently lose_sum: 76.79006189107895
time_elpased: 2.045
batch start
#iterations: 369
currently lose_sum: 76.55183067917824
time_elpased: 2.06
batch start
#iterations: 370
currently lose_sum: 77.81281530857086
time_elpased: 2.067
batch start
#iterations: 371
currently lose_sum: 77.02820590138435
time_elpased: 2.103
batch start
#iterations: 372
currently lose_sum: 78.04250374436378
time_elpased: 2.07
batch start
#iterations: 373
currently lose_sum: 75.90798291563988
time_elpased: 2.035
batch start
#iterations: 374
currently lose_sum: 77.50238552689552
time_elpased: 2.058
batch start
#iterations: 375
currently lose_sum: 76.91839635372162
time_elpased: 2.087
batch start
#iterations: 376
currently lose_sum: 76.0336147248745
time_elpased: 2.059
batch start
#iterations: 377
currently lose_sum: 77.18199798464775
time_elpased: 2.102
batch start
#iterations: 378
currently lose_sum: 76.9447250366211
time_elpased: 2.077
batch start
#iterations: 379
currently lose_sum: 74.68713384866714
time_elpased: 2.086
start validation test
0.634742268041
0.677362815154
0.517032005763
0.586436325435
0.634948926562
65.648
batch start
#iterations: 380
currently lose_sum: 74.43402224779129
time_elpased: 2.054
batch start
#iterations: 381
currently lose_sum: 76.8234649002552
time_elpased: 2.038
batch start
#iterations: 382
currently lose_sum: 75.75575014948845
time_elpased: 2.088
batch start
#iterations: 383
currently lose_sum: 75.32107418775558
time_elpased: 2.071
batch start
#iterations: 384
currently lose_sum: 75.71194249391556
time_elpased: 2.039
batch start
#iterations: 385
currently lose_sum: 75.06466689705849
time_elpased: 2.088
batch start
#iterations: 386
currently lose_sum: 74.06749644875526
time_elpased: 2.049
batch start
#iterations: 387
currently lose_sum: 76.36719271540642
time_elpased: 2.041
batch start
#iterations: 388
currently lose_sum: 74.22068405151367
time_elpased: 2.047
batch start
#iterations: 389
currently lose_sum: 75.76592805981636
time_elpased: 2.05
batch start
#iterations: 390
currently lose_sum: 74.31640660762787
time_elpased: 1.999
batch start
#iterations: 391
currently lose_sum: 73.97984325885773
time_elpased: 2.111
batch start
#iterations: 392
currently lose_sum: 74.1398429274559
time_elpased: 2.046
batch start
#iterations: 393
currently lose_sum: 73.08080342411995
time_elpased: 2.085
batch start
#iterations: 394
currently lose_sum: 73.45805442333221
time_elpased: 2.057
batch start
#iterations: 395
currently lose_sum: 75.15563535690308
time_elpased: 2.088
batch start
#iterations: 396
currently lose_sum: 73.42807510495186
time_elpased: 2.031
batch start
#iterations: 397
currently lose_sum: 74.11492338776588
time_elpased: 2.043
batch start
#iterations: 398
currently lose_sum: 73.52538999915123
time_elpased: 2.051
batch start
#iterations: 399
currently lose_sum: 74.93606558442116
time_elpased: 2.046
start validation test
0.629587628866
0.661642610806
0.533086343522
0.590447965348
0.629757051757
65.458
acc: 0.691
pre: 0.681
rec: 0.723
F1: 0.701
auc: 0.750
