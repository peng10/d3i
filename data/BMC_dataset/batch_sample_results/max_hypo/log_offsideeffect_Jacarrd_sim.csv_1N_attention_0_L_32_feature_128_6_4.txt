start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.061618745327
time_elpased: 2.249
batch start
#iterations: 1
currently lose_sum: 99.49045491218567
time_elpased: 2.21
batch start
#iterations: 2
currently lose_sum: 99.0965530872345
time_elpased: 2.24
batch start
#iterations: 3
currently lose_sum: 99.00613874197006
time_elpased: 2.224
batch start
#iterations: 4
currently lose_sum: 98.45391172170639
time_elpased: 2.229
batch start
#iterations: 5
currently lose_sum: 97.74352717399597
time_elpased: 2.216
batch start
#iterations: 6
currently lose_sum: 98.17487275600433
time_elpased: 2.261
batch start
#iterations: 7
currently lose_sum: 98.6308690905571
time_elpased: 2.227
batch start
#iterations: 8
currently lose_sum: 97.64647001028061
time_elpased: 2.199
batch start
#iterations: 9
currently lose_sum: 97.20984655618668
time_elpased: 2.226
batch start
#iterations: 10
currently lose_sum: 97.59215623140335
time_elpased: 2.229
batch start
#iterations: 11
currently lose_sum: 97.44481265544891
time_elpased: 2.317
batch start
#iterations: 12
currently lose_sum: 97.34418374300003
time_elpased: 2.288
batch start
#iterations: 13
currently lose_sum: 97.04452526569366
time_elpased: 2.226
batch start
#iterations: 14
currently lose_sum: 96.81404328346252
time_elpased: 2.237
batch start
#iterations: 15
currently lose_sum: 96.2406354546547
time_elpased: 2.256
batch start
#iterations: 16
currently lose_sum: 97.42825901508331
time_elpased: 2.283
batch start
#iterations: 17
currently lose_sum: 97.5732187628746
time_elpased: 2.21
batch start
#iterations: 18
currently lose_sum: 96.02342504262924
time_elpased: 2.259
batch start
#iterations: 19
currently lose_sum: 97.2895450592041
time_elpased: 2.21
start validation test
0.65293814433
0.613253377865
0.831429453535
0.705866934603
0.652624775306
61.786
batch start
#iterations: 20
currently lose_sum: 96.77576339244843
time_elpased: 2.213
batch start
#iterations: 21
currently lose_sum: 96.01513957977295
time_elpased: 2.237
batch start
#iterations: 22
currently lose_sum: 96.21177017688751
time_elpased: 2.192
batch start
#iterations: 23
currently lose_sum: 96.94391196966171
time_elpased: 2.231
batch start
#iterations: 24
currently lose_sum: 97.80011475086212
time_elpased: 2.239
batch start
#iterations: 25
currently lose_sum: 97.74014675617218
time_elpased: 2.261
batch start
#iterations: 26
currently lose_sum: 96.43478482961655
time_elpased: 2.228
batch start
#iterations: 27
currently lose_sum: 95.53408724069595
time_elpased: 2.209
batch start
#iterations: 28
currently lose_sum: 95.4105054140091
time_elpased: 2.232
batch start
#iterations: 29
currently lose_sum: 95.97007751464844
time_elpased: 2.231
batch start
#iterations: 30
currently lose_sum: 95.75880533456802
time_elpased: 2.276
batch start
#iterations: 31
currently lose_sum: 94.71578824520111
time_elpased: 2.246
batch start
#iterations: 32
currently lose_sum: 96.24729686975479
time_elpased: 2.226
batch start
#iterations: 33
currently lose_sum: 95.28082197904587
time_elpased: 2.215
batch start
#iterations: 34
currently lose_sum: 95.90648484230042
time_elpased: 2.233
batch start
#iterations: 35
currently lose_sum: 93.97875994443893
time_elpased: 2.257
batch start
#iterations: 36
currently lose_sum: 95.44257599115372
time_elpased: 2.308
batch start
#iterations: 37
currently lose_sum: 95.33073961734772
time_elpased: 2.251
batch start
#iterations: 38
currently lose_sum: 94.35294544696808
time_elpased: 2.264
batch start
#iterations: 39
currently lose_sum: 94.29203075170517
time_elpased: 2.224
start validation test
0.542783505155
0.523017555302
0.990326232376
0.684521269028
0.541997774868
66.326
batch start
#iterations: 40
currently lose_sum: 95.0829513669014
time_elpased: 2.248
batch start
#iterations: 41
currently lose_sum: 95.16613566875458
time_elpased: 2.226
batch start
#iterations: 42
currently lose_sum: 94.55219209194183
time_elpased: 2.243
batch start
#iterations: 43
currently lose_sum: 94.40716791152954
time_elpased: 2.234
batch start
#iterations: 44
currently lose_sum: 94.52528876066208
time_elpased: 2.219
batch start
#iterations: 45
currently lose_sum: 93.90149766206741
time_elpased: 2.196
batch start
#iterations: 46
currently lose_sum: 100.102095246315
time_elpased: 2.289
batch start
#iterations: 47
currently lose_sum: 97.26862531900406
time_elpased: 2.245
batch start
#iterations: 48
currently lose_sum: 95.21605241298676
time_elpased: 2.255
batch start
#iterations: 49
currently lose_sum: 94.12589782476425
time_elpased: 2.25
batch start
#iterations: 50
currently lose_sum: 94.39628428220749
time_elpased: 2.229
batch start
#iterations: 51
currently lose_sum: 94.25585126876831
time_elpased: 2.245
batch start
#iterations: 52
currently lose_sum: 94.72472661733627
time_elpased: 2.219
batch start
#iterations: 53
currently lose_sum: 93.17165577411652
time_elpased: 2.227
batch start
#iterations: 54
currently lose_sum: 93.73722875118256
time_elpased: 2.239
batch start
#iterations: 55
currently lose_sum: 94.04542362689972
time_elpased: 2.245
batch start
#iterations: 56
currently lose_sum: 94.77460086345673
time_elpased: 2.241
batch start
#iterations: 57
currently lose_sum: 93.06556963920593
time_elpased: 2.291
batch start
#iterations: 58
currently lose_sum: 93.76769495010376
time_elpased: 2.216
batch start
#iterations: 59
currently lose_sum: 94.14553034305573
time_elpased: 2.206
start validation test
0.684072164948
0.640090582539
0.843573119275
0.727878168983
0.683792136422
59.246
batch start
#iterations: 60
currently lose_sum: 94.18696624040604
time_elpased: 2.234
batch start
#iterations: 61
currently lose_sum: 93.03224354982376
time_elpased: 2.229
batch start
#iterations: 62
currently lose_sum: 96.01059460639954
time_elpased: 2.24
batch start
#iterations: 63
currently lose_sum: 92.86845058202744
time_elpased: 2.269
batch start
#iterations: 64
currently lose_sum: 95.71141391992569
time_elpased: 2.208
batch start
#iterations: 65
currently lose_sum: 98.260597884655
time_elpased: 2.222
batch start
#iterations: 66
currently lose_sum: 94.72827446460724
time_elpased: 2.247
batch start
#iterations: 67
currently lose_sum: 94.17659193277359
time_elpased: 2.29
batch start
#iterations: 68
currently lose_sum: 92.64117723703384
time_elpased: 2.271
batch start
#iterations: 69
currently lose_sum: 92.43424087762833
time_elpased: 2.21
batch start
#iterations: 70
currently lose_sum: 92.97439098358154
time_elpased: 2.235
batch start
#iterations: 71
currently lose_sum: 93.20120078325272
time_elpased: 2.229
batch start
#iterations: 72
currently lose_sum: 92.97006076574326
time_elpased: 2.261
batch start
#iterations: 73
currently lose_sum: 93.41866034269333
time_elpased: 2.24
batch start
#iterations: 74
currently lose_sum: 95.88032788038254
time_elpased: 2.249
batch start
#iterations: 75
currently lose_sum: 93.72107291221619
time_elpased: 2.259
batch start
#iterations: 76
currently lose_sum: 91.94129014015198
time_elpased: 2.233
batch start
#iterations: 77
currently lose_sum: 92.93633449077606
time_elpased: 2.283
batch start
#iterations: 78
currently lose_sum: 92.93757170438766
time_elpased: 2.218
batch start
#iterations: 79
currently lose_sum: 92.53115820884705
time_elpased: 2.23
start validation test
0.673195876289
0.619930392784
0.898219615108
0.733568667003
0.672800812408
59.297
batch start
#iterations: 80
currently lose_sum: 92.83223062753677
time_elpased: 2.225
batch start
#iterations: 81
currently lose_sum: 92.24530297517776
time_elpased: 2.215
batch start
#iterations: 82
currently lose_sum: 93.28694659471512
time_elpased: 2.261
batch start
#iterations: 83
currently lose_sum: 91.9396390914917
time_elpased: 2.265
batch start
#iterations: 84
currently lose_sum: 93.94253140687943
time_elpased: 2.253
batch start
#iterations: 85
currently lose_sum: 92.80001765489578
time_elpased: 2.264
batch start
#iterations: 86
currently lose_sum: 92.64299583435059
time_elpased: 2.24
batch start
#iterations: 87
currently lose_sum: 93.06528264284134
time_elpased: 2.267
batch start
#iterations: 88
currently lose_sum: 92.68308281898499
time_elpased: 2.205
batch start
#iterations: 89
currently lose_sum: 92.03859972953796
time_elpased: 2.253
batch start
#iterations: 90
currently lose_sum: 91.4380014538765
time_elpased: 2.226
batch start
#iterations: 91
currently lose_sum: 92.5187509059906
time_elpased: 2.225
batch start
#iterations: 92
currently lose_sum: 92.45665663480759
time_elpased: 2.194
batch start
#iterations: 93
currently lose_sum: 92.02638101577759
time_elpased: 2.247
batch start
#iterations: 94
currently lose_sum: 91.39340591430664
time_elpased: 2.223
batch start
#iterations: 95
currently lose_sum: 92.0918961763382
time_elpased: 2.231
batch start
#iterations: 96
currently lose_sum: 91.79343777894974
time_elpased: 2.23
batch start
#iterations: 97
currently lose_sum: 91.88718295097351
time_elpased: 2.241
batch start
#iterations: 98
currently lose_sum: 94.95193809270859
time_elpased: 2.237
batch start
#iterations: 99
currently lose_sum: 92.46578693389893
time_elpased: 2.236
start validation test
0.696804123711
0.678156647775
0.751157764742
0.71279296875
0.696708697511
57.433
batch start
#iterations: 100
currently lose_sum: 92.98729908466339
time_elpased: 2.175
batch start
#iterations: 101
currently lose_sum: 91.17566782236099
time_elpased: 2.256
batch start
#iterations: 102
currently lose_sum: 91.35135287046432
time_elpased: 2.248
batch start
#iterations: 103
currently lose_sum: 91.93135458230972
time_elpased: 2.229
batch start
#iterations: 104
currently lose_sum: 91.47623717784882
time_elpased: 2.249
batch start
#iterations: 105
currently lose_sum: 91.34023660421371
time_elpased: 2.233
batch start
#iterations: 106
currently lose_sum: 95.26825857162476
time_elpased: 2.215
batch start
#iterations: 107
currently lose_sum: 92.53469657897949
time_elpased: 2.247
batch start
#iterations: 108
currently lose_sum: 91.2950736284256
time_elpased: 2.228
batch start
#iterations: 109
currently lose_sum: 92.78231424093246
time_elpased: 2.241
batch start
#iterations: 110
currently lose_sum: 90.7846474647522
time_elpased: 2.202
batch start
#iterations: 111
currently lose_sum: 91.18035227060318
time_elpased: 2.224
batch start
#iterations: 112
currently lose_sum: 91.23990643024445
time_elpased: 2.207
batch start
#iterations: 113
currently lose_sum: 97.76511520147324
time_elpased: 2.245
batch start
#iterations: 114
currently lose_sum: 92.45969098806381
time_elpased: 2.209
batch start
#iterations: 115
currently lose_sum: 91.17963141202927
time_elpased: 2.234
batch start
#iterations: 116
currently lose_sum: 91.51303517818451
time_elpased: 2.236
batch start
#iterations: 117
currently lose_sum: 90.96816903352737
time_elpased: 2.225
batch start
#iterations: 118
currently lose_sum: 89.90821409225464
time_elpased: 2.263
batch start
#iterations: 119
currently lose_sum: 91.94363796710968
time_elpased: 2.286
start validation test
0.689484536082
0.683749626829
0.707111248328
0.695234240615
0.68945358967
57.592
batch start
#iterations: 120
currently lose_sum: 91.07088607549667
time_elpased: 2.279
batch start
#iterations: 121
currently lose_sum: 91.51193457841873
time_elpased: 2.219
batch start
#iterations: 122
currently lose_sum: 90.54127162694931
time_elpased: 2.205
batch start
#iterations: 123
currently lose_sum: 91.02266710996628
time_elpased: 2.236
batch start
#iterations: 124
currently lose_sum: 91.82762211561203
time_elpased: 2.274
batch start
#iterations: 125
currently lose_sum: 91.4274360537529
time_elpased: 2.281
batch start
#iterations: 126
currently lose_sum: 90.23902779817581
time_elpased: 2.285
batch start
#iterations: 127
currently lose_sum: 90.84795379638672
time_elpased: 2.238
batch start
#iterations: 128
currently lose_sum: 90.50385910272598
time_elpased: 2.227
batch start
#iterations: 129
currently lose_sum: 90.5396044254303
time_elpased: 2.251
batch start
#iterations: 130
currently lose_sum: 91.89820373058319
time_elpased: 2.201
batch start
#iterations: 131
currently lose_sum: 89.65082740783691
time_elpased: 2.265
batch start
#iterations: 132
currently lose_sum: 90.72439086437225
time_elpased: 2.273
batch start
#iterations: 133
currently lose_sum: 90.74335360527039
time_elpased: 2.245
batch start
#iterations: 134
currently lose_sum: 90.84678393602371
time_elpased: 2.269
batch start
#iterations: 135
currently lose_sum: 90.83706164360046
time_elpased: 2.228
batch start
#iterations: 136
currently lose_sum: 90.00670838356018
time_elpased: 2.207
batch start
#iterations: 137
currently lose_sum: 90.64235889911652
time_elpased: 2.195
batch start
#iterations: 138
currently lose_sum: 89.87400686740875
time_elpased: 2.237
batch start
#iterations: 139
currently lose_sum: 90.75608342885971
time_elpased: 2.232
start validation test
0.652319587629
0.617137001419
0.805701348153
0.698924251216
0.652050302291
59.056
batch start
#iterations: 140
currently lose_sum: 90.54018867015839
time_elpased: 2.224
batch start
#iterations: 141
currently lose_sum: 91.30865442752838
time_elpased: 2.254
batch start
#iterations: 142
currently lose_sum: 90.12926417589188
time_elpased: 2.239
batch start
#iterations: 143
currently lose_sum: 88.801608979702
time_elpased: 2.274
batch start
#iterations: 144
currently lose_sum: 89.93817955255508
time_elpased: 2.219
batch start
#iterations: 145
currently lose_sum: 89.7541880607605
time_elpased: 2.265
batch start
#iterations: 146
currently lose_sum: 89.7045710682869
time_elpased: 2.252
batch start
#iterations: 147
currently lose_sum: 88.85726135969162
time_elpased: 2.238
batch start
#iterations: 148
currently lose_sum: 90.98154538869858
time_elpased: 2.269
batch start
#iterations: 149
currently lose_sum: 89.66281604766846
time_elpased: 2.243
batch start
#iterations: 150
currently lose_sum: 88.90745502710342
time_elpased: 2.267
batch start
#iterations: 151
currently lose_sum: 89.70681029558182
time_elpased: 2.263
batch start
#iterations: 152
currently lose_sum: 88.58546960353851
time_elpased: 2.225
batch start
#iterations: 153
currently lose_sum: 90.69052511453629
time_elpased: 2.226
batch start
#iterations: 154
currently lose_sum: 89.43301957845688
time_elpased: 2.256
batch start
#iterations: 155
currently lose_sum: 89.64911484718323
time_elpased: 2.266
batch start
#iterations: 156
currently lose_sum: 90.33188855648041
time_elpased: 2.241
batch start
#iterations: 157
currently lose_sum: 89.95386683940887
time_elpased: 2.23
batch start
#iterations: 158
currently lose_sum: 90.74234342575073
time_elpased: 2.224
batch start
#iterations: 159
currently lose_sum: 89.40302956104279
time_elpased: 2.214
start validation test
0.676494845361
0.640391676867
0.807656684162
0.714363735664
0.676264570522
57.781
batch start
#iterations: 160
currently lose_sum: 88.55446124076843
time_elpased: 2.222
batch start
#iterations: 161
currently lose_sum: 89.46317732334137
time_elpased: 2.219
batch start
#iterations: 162
currently lose_sum: 89.34553968906403
time_elpased: 2.215
batch start
#iterations: 163
currently lose_sum: 88.93636184930801
time_elpased: 2.233
batch start
#iterations: 164
currently lose_sum: 88.68259769678116
time_elpased: 2.233
batch start
#iterations: 165
currently lose_sum: 88.29498755931854
time_elpased: 2.199
batch start
#iterations: 166
currently lose_sum: 88.56783330440521
time_elpased: 2.231
batch start
#iterations: 167
currently lose_sum: 89.1032891869545
time_elpased: 2.347
batch start
#iterations: 168
currently lose_sum: 90.75243586301804
time_elpased: 2.245
batch start
#iterations: 169
currently lose_sum: 88.5133565068245
time_elpased: 2.194
batch start
#iterations: 170
currently lose_sum: 89.3938980102539
time_elpased: 2.229
batch start
#iterations: 171
currently lose_sum: 89.0414622426033
time_elpased: 2.238
batch start
#iterations: 172
currently lose_sum: 89.22055071592331
time_elpased: 2.231
batch start
#iterations: 173
currently lose_sum: 88.68711549043655
time_elpased: 2.231
batch start
#iterations: 174
currently lose_sum: 90.06593859195709
time_elpased: 2.247
batch start
#iterations: 175
currently lose_sum: 88.59106171131134
time_elpased: 2.247
batch start
#iterations: 176
currently lose_sum: 89.31011021137238
time_elpased: 2.293
batch start
#iterations: 177
currently lose_sum: 88.46696329116821
time_elpased: 2.267
batch start
#iterations: 178
currently lose_sum: 89.24838495254517
time_elpased: 2.236
batch start
#iterations: 179
currently lose_sum: 87.54486203193665
time_elpased: 2.264
start validation test
0.390927835052
0.383557084212
0.355768241227
0.369140416444
0.390989563142
74.938
batch start
#iterations: 180
currently lose_sum: 90.86785441637039
time_elpased: 2.232
batch start
#iterations: 181
currently lose_sum: 89.25911402702332
time_elpased: 2.214
batch start
#iterations: 182
currently lose_sum: 88.39880609512329
time_elpased: 2.205
batch start
#iterations: 183
currently lose_sum: 88.52204835414886
time_elpased: 2.228
batch start
#iterations: 184
currently lose_sum: 88.03785759210587
time_elpased: 2.23
batch start
#iterations: 185
currently lose_sum: 88.30606758594513
time_elpased: 2.261
batch start
#iterations: 186
currently lose_sum: 88.54009771347046
time_elpased: 2.195
batch start
#iterations: 187
currently lose_sum: 88.67147129774094
time_elpased: 2.215
batch start
#iterations: 188
currently lose_sum: 88.2048796415329
time_elpased: 2.249
batch start
#iterations: 189
currently lose_sum: 88.17988246679306
time_elpased: 2.215
batch start
#iterations: 190
currently lose_sum: 89.30899846553802
time_elpased: 2.23
batch start
#iterations: 191
currently lose_sum: 89.16566932201385
time_elpased: 2.271
batch start
#iterations: 192
currently lose_sum: 87.85616022348404
time_elpased: 2.226
batch start
#iterations: 193
currently lose_sum: 90.18530476093292
time_elpased: 2.242
batch start
#iterations: 194
currently lose_sum: 87.42018902301788
time_elpased: 2.225
batch start
#iterations: 195
currently lose_sum: 87.91079699993134
time_elpased: 2.249
batch start
#iterations: 196
currently lose_sum: 88.02877271175385
time_elpased: 2.252
batch start
#iterations: 197
currently lose_sum: 89.58183693885803
time_elpased: 2.209
batch start
#iterations: 198
currently lose_sum: 87.625235080719
time_elpased: 2.242
batch start
#iterations: 199
currently lose_sum: 88.86731272935867
time_elpased: 2.259
start validation test
0.692113402062
0.694837635304
0.687043326129
0.690918499353
0.692122303362
56.651
batch start
#iterations: 200
currently lose_sum: 88.00981056690216
time_elpased: 2.259
batch start
#iterations: 201
currently lose_sum: 88.74163115024567
time_elpased: 2.201
batch start
#iterations: 202
currently lose_sum: 88.95481204986572
time_elpased: 2.23
batch start
#iterations: 203
currently lose_sum: 87.62524902820587
time_elpased: 2.254
batch start
#iterations: 204
currently lose_sum: 87.91511750221252
time_elpased: 2.21
batch start
#iterations: 205
currently lose_sum: 87.73021930456161
time_elpased: 2.237
batch start
#iterations: 206
currently lose_sum: 87.94765079021454
time_elpased: 2.243
batch start
#iterations: 207
currently lose_sum: 87.3264507651329
time_elpased: 2.243
batch start
#iterations: 208
currently lose_sum: 88.41767752170563
time_elpased: 2.272
batch start
#iterations: 209
currently lose_sum: 88.01020324230194
time_elpased: 2.216
batch start
#iterations: 210
currently lose_sum: 87.52661627531052
time_elpased: 2.247
batch start
#iterations: 211
currently lose_sum: 88.25907802581787
time_elpased: 2.235
batch start
#iterations: 212
currently lose_sum: 87.15886741876602
time_elpased: 2.256
batch start
#iterations: 213
currently lose_sum: 88.46220368146896
time_elpased: 2.236
batch start
#iterations: 214
currently lose_sum: 87.73566842079163
time_elpased: 2.249
batch start
#iterations: 215
currently lose_sum: 88.14975589513779
time_elpased: 2.234
batch start
#iterations: 216
currently lose_sum: 87.20903843641281
time_elpased: 2.241
batch start
#iterations: 217
currently lose_sum: 87.1683002114296
time_elpased: 2.23
batch start
#iterations: 218
currently lose_sum: 86.5018857717514
time_elpased: 2.241
batch start
#iterations: 219
currently lose_sum: 88.10267096757889
time_elpased: 2.229
start validation test
0.648556701031
0.695534871172
0.530616445405
0.60198482195
0.648763763341
59.727
batch start
#iterations: 220
currently lose_sum: 86.49453622102737
time_elpased: 2.215
batch start
#iterations: 221
currently lose_sum: 87.5724128484726
time_elpased: 2.238
batch start
#iterations: 222
currently lose_sum: 87.61013752222061
time_elpased: 2.226
batch start
#iterations: 223
currently lose_sum: 87.42901629209518
time_elpased: 2.264
batch start
#iterations: 224
currently lose_sum: 88.24574452638626
time_elpased: 2.202
batch start
#iterations: 225
currently lose_sum: 87.2315194606781
time_elpased: 2.219
batch start
#iterations: 226
currently lose_sum: 86.4973748922348
time_elpased: 2.228
batch start
#iterations: 227
currently lose_sum: 87.21938210725784
time_elpased: 2.212
batch start
#iterations: 228
currently lose_sum: 88.80987006425858
time_elpased: 2.242
batch start
#iterations: 229
currently lose_sum: 88.96958220005035
time_elpased: 2.272
batch start
#iterations: 230
currently lose_sum: 87.3459821343422
time_elpased: 2.204
batch start
#iterations: 231
currently lose_sum: 86.98410481214523
time_elpased: 2.232
batch start
#iterations: 232
currently lose_sum: 89.75339663028717
time_elpased: 2.222
batch start
#iterations: 233
currently lose_sum: 87.04678875207901
time_elpased: 2.224
batch start
#iterations: 234
currently lose_sum: 87.29935103654861
time_elpased: 2.21
batch start
#iterations: 235
currently lose_sum: 88.19077280163765
time_elpased: 2.256
batch start
#iterations: 236
currently lose_sum: 87.67773568630219
time_elpased: 2.222
batch start
#iterations: 237
currently lose_sum: 86.73522871732712
time_elpased: 2.285
batch start
#iterations: 238
currently lose_sum: 86.94608354568481
time_elpased: 2.299
batch start
#iterations: 239
currently lose_sum: 87.45100837945938
time_elpased: 2.212
start validation test
0.694329896907
0.677443538562
0.743953895235
0.709142632921
0.694242774324
56.139
batch start
#iterations: 240
currently lose_sum: 86.93589222431183
time_elpased: 2.212
batch start
#iterations: 241
currently lose_sum: 86.42255359888077
time_elpased: 2.258
batch start
#iterations: 242
currently lose_sum: 86.69915872812271
time_elpased: 2.229
batch start
#iterations: 243
currently lose_sum: 86.42785167694092
time_elpased: 2.248
batch start
#iterations: 244
currently lose_sum: 87.10738056898117
time_elpased: 2.22
batch start
#iterations: 245
currently lose_sum: 86.5711140036583
time_elpased: 2.237
batch start
#iterations: 246
currently lose_sum: 86.83806705474854
time_elpased: 2.257
batch start
#iterations: 247
currently lose_sum: 87.78144800662994
time_elpased: 2.235
batch start
#iterations: 248
currently lose_sum: 87.13638609647751
time_elpased: 2.214
batch start
#iterations: 249
currently lose_sum: 85.76518511772156
time_elpased: 2.273
batch start
#iterations: 250
currently lose_sum: 86.23326182365417
time_elpased: 2.219
batch start
#iterations: 251
currently lose_sum: 86.64388567209244
time_elpased: 2.261
batch start
#iterations: 252
currently lose_sum: 86.07034051418304
time_elpased: 2.255
batch start
#iterations: 253
currently lose_sum: 85.27743154764175
time_elpased: 2.262
batch start
#iterations: 254
currently lose_sum: 85.49498826265335
time_elpased: 2.232
batch start
#iterations: 255
currently lose_sum: 86.51236391067505
time_elpased: 2.237
batch start
#iterations: 256
currently lose_sum: 87.04794120788574
time_elpased: 2.232
batch start
#iterations: 257
currently lose_sum: 87.20779103040695
time_elpased: 2.233
batch start
#iterations: 258
currently lose_sum: 86.00852745771408
time_elpased: 2.212
batch start
#iterations: 259
currently lose_sum: 87.39158833026886
time_elpased: 2.236
start validation test
0.689381443299
0.689223828566
0.69177729752
0.690498202363
0.689377237007
57.390
batch start
#iterations: 260
currently lose_sum: 86.6306746006012
time_elpased: 2.244
batch start
#iterations: 261
currently lose_sum: 85.57502329349518
time_elpased: 2.227
batch start
#iterations: 262
currently lose_sum: 86.67604559659958
time_elpased: 2.236
batch start
#iterations: 263
currently lose_sum: 85.39574527740479
time_elpased: 2.226
batch start
#iterations: 264
currently lose_sum: 86.96582973003387
time_elpased: 2.232
batch start
#iterations: 265
currently lose_sum: 85.26364970207214
time_elpased: 2.207
batch start
#iterations: 266
currently lose_sum: 86.20177376270294
time_elpased: 2.223
batch start
#iterations: 267
currently lose_sum: 85.57026982307434
time_elpased: 2.234
batch start
#iterations: 268
currently lose_sum: 86.25812268257141
time_elpased: 2.241
batch start
#iterations: 269
currently lose_sum: 85.84276801347733
time_elpased: 2.275
batch start
#iterations: 270
currently lose_sum: 84.81380069255829
time_elpased: 2.245
batch start
#iterations: 271
currently lose_sum: 84.61628544330597
time_elpased: 2.275
batch start
#iterations: 272
currently lose_sum: 85.81783360242844
time_elpased: 2.245
batch start
#iterations: 273
currently lose_sum: 86.52572333812714
time_elpased: 2.225
batch start
#iterations: 274
currently lose_sum: 85.65948277711868
time_elpased: 2.247
batch start
#iterations: 275
currently lose_sum: 85.76944613456726
time_elpased: 2.254
batch start
#iterations: 276
currently lose_sum: 86.85623055696487
time_elpased: 2.234
batch start
#iterations: 277
currently lose_sum: 86.57280743122101
time_elpased: 2.284
batch start
#iterations: 278
currently lose_sum: 85.49893856048584
time_elpased: 2.251
batch start
#iterations: 279
currently lose_sum: 86.04903841018677
time_elpased: 2.263
start validation test
0.682731958763
0.658480156611
0.761551919317
0.706275351945
0.682593578165
57.674
batch start
#iterations: 280
currently lose_sum: 84.75026696920395
time_elpased: 2.248
batch start
#iterations: 281
currently lose_sum: 85.09330916404724
time_elpased: 2.309
batch start
#iterations: 282
currently lose_sum: 85.52406162023544
time_elpased: 2.238
batch start
#iterations: 283
currently lose_sum: 85.09982174634933
time_elpased: 2.226
batch start
#iterations: 284
currently lose_sum: 85.11851140856743
time_elpased: 2.186
batch start
#iterations: 285
currently lose_sum: 85.55299091339111
time_elpased: 2.256
batch start
#iterations: 286
currently lose_sum: 86.21261918544769
time_elpased: 2.184
batch start
#iterations: 287
currently lose_sum: 84.98495930433273
time_elpased: 2.254
batch start
#iterations: 288
currently lose_sum: 86.72132629156113
time_elpased: 2.233
batch start
#iterations: 289
currently lose_sum: 84.6997749209404
time_elpased: 2.239
batch start
#iterations: 290
currently lose_sum: 85.32490491867065
time_elpased: 2.21
batch start
#iterations: 291
currently lose_sum: 84.31021565198898
time_elpased: 2.201
batch start
#iterations: 292
currently lose_sum: 85.10001331567764
time_elpased: 2.22
batch start
#iterations: 293
currently lose_sum: 85.76414096355438
time_elpased: 2.24
batch start
#iterations: 294
currently lose_sum: 84.23007994890213
time_elpased: 2.201
batch start
#iterations: 295
currently lose_sum: 84.52161055803299
time_elpased: 2.239
batch start
#iterations: 296
currently lose_sum: 84.31756484508514
time_elpased: 2.175
batch start
#iterations: 297
currently lose_sum: 85.25664764642715
time_elpased: 2.199
batch start
#iterations: 298
currently lose_sum: 84.66238754987717
time_elpased: 2.215
batch start
#iterations: 299
currently lose_sum: 84.423204600811
time_elpased: 2.234
start validation test
0.684484536082
0.692505353319
0.665637542451
0.678805688199
0.684517624887
57.476
batch start
#iterations: 300
currently lose_sum: 84.44122141599655
time_elpased: 2.209
batch start
#iterations: 301
currently lose_sum: 83.34744477272034
time_elpased: 2.218
batch start
#iterations: 302
currently lose_sum: 84.501893222332
time_elpased: 2.257
batch start
#iterations: 303
currently lose_sum: 84.44433054327965
time_elpased: 2.213
batch start
#iterations: 304
currently lose_sum: 85.14507359266281
time_elpased: 2.206
batch start
#iterations: 305
currently lose_sum: 83.8551185131073
time_elpased: 2.224
batch start
#iterations: 306
currently lose_sum: 84.32872295379639
time_elpased: 2.229
batch start
#iterations: 307
currently lose_sum: 84.53457397222519
time_elpased: 2.232
batch start
#iterations: 308
currently lose_sum: 83.96883788704872
time_elpased: 2.264
batch start
#iterations: 309
currently lose_sum: 83.13525548577309
time_elpased: 2.203
batch start
#iterations: 310
currently lose_sum: 84.95659977197647
time_elpased: 2.271
batch start
#iterations: 311
currently lose_sum: 83.56346598267555
time_elpased: 2.224
batch start
#iterations: 312
currently lose_sum: 84.27308881282806
time_elpased: 2.249
batch start
#iterations: 313
currently lose_sum: 84.13961040973663
time_elpased: 2.238
batch start
#iterations: 314
currently lose_sum: 83.59594404697418
time_elpased: 2.237
batch start
#iterations: 315
currently lose_sum: 83.33503305912018
time_elpased: 2.249
batch start
#iterations: 316
currently lose_sum: 83.792220890522
time_elpased: 2.279
batch start
#iterations: 317
currently lose_sum: 83.7750855088234
time_elpased: 2.209
batch start
#iterations: 318
currently lose_sum: 83.74743655323982
time_elpased: 2.226
batch start
#iterations: 319
currently lose_sum: 83.38176310062408
time_elpased: 2.241
start validation test
0.657525773196
0.657251049023
0.660903571061
0.659072249589
0.657519842951
58.957
batch start
#iterations: 320
currently lose_sum: 85.03347367048264
time_elpased: 2.224
batch start
#iterations: 321
currently lose_sum: 83.46325534582138
time_elpased: 2.252
batch start
#iterations: 322
currently lose_sum: 83.30675441026688
time_elpased: 2.211
batch start
#iterations: 323
currently lose_sum: 82.7302156984806
time_elpased: 2.247
batch start
#iterations: 324
currently lose_sum: 82.85638040304184
time_elpased: 2.211
batch start
#iterations: 325
currently lose_sum: 83.5906258225441
time_elpased: 2.262
batch start
#iterations: 326
currently lose_sum: 82.83413738012314
time_elpased: 2.255
batch start
#iterations: 327
currently lose_sum: 84.48662853240967
time_elpased: 2.214
batch start
#iterations: 328
currently lose_sum: 83.55690762400627
time_elpased: 2.237
batch start
#iterations: 329
currently lose_sum: 83.26999509334564
time_elpased: 2.246
batch start
#iterations: 330
currently lose_sum: 83.06752380728722
time_elpased: 2.252
batch start
#iterations: 331
currently lose_sum: 82.2392572760582
time_elpased: 2.29
batch start
#iterations: 332
currently lose_sum: 84.99174213409424
time_elpased: 2.23
batch start
#iterations: 333
currently lose_sum: 82.6600022315979
time_elpased: 2.258
batch start
#iterations: 334
currently lose_sum: 83.6801081597805
time_elpased: 2.244
batch start
#iterations: 335
currently lose_sum: 83.78190049529076
time_elpased: 2.243
batch start
#iterations: 336
currently lose_sum: 82.05840009450912
time_elpased: 2.256
batch start
#iterations: 337
currently lose_sum: 83.7133013010025
time_elpased: 2.223
batch start
#iterations: 338
currently lose_sum: 82.27510648965836
time_elpased: 2.219
batch start
#iterations: 339
currently lose_sum: 84.14575493335724
time_elpased: 2.293
start validation test
0.666030927835
0.678737028042
0.632705567562
0.654913448735
0.666089435645
60.239
batch start
#iterations: 340
currently lose_sum: 83.82036310434341
time_elpased: 2.254
batch start
#iterations: 341
currently lose_sum: 83.1968948841095
time_elpased: 2.228
batch start
#iterations: 342
currently lose_sum: 82.43852299451828
time_elpased: 2.256
batch start
#iterations: 343
currently lose_sum: 81.92683613300323
time_elpased: 2.246
batch start
#iterations: 344
currently lose_sum: 82.25356063246727
time_elpased: 2.214
batch start
#iterations: 345
currently lose_sum: 82.02666172385216
time_elpased: 2.232
batch start
#iterations: 346
currently lose_sum: 82.22642013430595
time_elpased: 2.207
batch start
#iterations: 347
currently lose_sum: 82.88620463013649
time_elpased: 2.23
batch start
#iterations: 348
currently lose_sum: 82.04012754559517
time_elpased: 2.203
batch start
#iterations: 349
currently lose_sum: 82.19206511974335
time_elpased: 2.242
batch start
#iterations: 350
currently lose_sum: 82.81301772594452
time_elpased: 2.247
batch start
#iterations: 351
currently lose_sum: 81.87212207913399
time_elpased: 2.238
batch start
#iterations: 352
currently lose_sum: 81.91812342405319
time_elpased: 2.206
batch start
#iterations: 353
currently lose_sum: 81.10526275634766
time_elpased: 2.193
batch start
#iterations: 354
currently lose_sum: 81.62291160225868
time_elpased: 2.182
batch start
#iterations: 355
currently lose_sum: 81.79385364055634
time_elpased: 2.269
batch start
#iterations: 356
currently lose_sum: 82.38195636868477
time_elpased: 2.225
batch start
#iterations: 357
currently lose_sum: 81.31408655643463
time_elpased: 2.233
batch start
#iterations: 358
currently lose_sum: 82.64148789644241
time_elpased: 2.248
batch start
#iterations: 359
currently lose_sum: 82.96228393912315
time_elpased: 2.244
start validation test
0.626082474227
0.616409868608
0.671091900792
0.642589672842
0.626003453236
61.853
batch start
#iterations: 360
currently lose_sum: 82.82721409201622
time_elpased: 2.251
batch start
#iterations: 361
currently lose_sum: 82.1382777094841
time_elpased: 2.205
batch start
#iterations: 362
currently lose_sum: 80.94336220622063
time_elpased: 2.216
batch start
#iterations: 363
currently lose_sum: 81.8330409526825
time_elpased: 2.259
batch start
#iterations: 364
currently lose_sum: 81.2595583498478
time_elpased: 2.231
batch start
#iterations: 365
currently lose_sum: 80.83884158730507
time_elpased: 2.227
batch start
#iterations: 366
currently lose_sum: 82.35658150911331
time_elpased: 2.206
batch start
#iterations: 367
currently lose_sum: 81.04084578156471
time_elpased: 2.196
batch start
#iterations: 368
currently lose_sum: 81.25275152921677
time_elpased: 2.238
batch start
#iterations: 369
currently lose_sum: 80.93671518564224
time_elpased: 2.24
batch start
#iterations: 370
currently lose_sum: 80.59280762076378
time_elpased: 2.233
batch start
#iterations: 371
currently lose_sum: 79.65559723973274
time_elpased: 2.243
batch start
#iterations: 372
currently lose_sum: 79.97445321083069
time_elpased: 2.207
batch start
#iterations: 373
currently lose_sum: 81.23721593618393
time_elpased: 2.246
batch start
#iterations: 374
currently lose_sum: 79.95023766160011
time_elpased: 2.27
batch start
#iterations: 375
currently lose_sum: 80.18811023235321
time_elpased: 2.22
batch start
#iterations: 376
currently lose_sum: 81.02052894234657
time_elpased: 2.279
batch start
#iterations: 377
currently lose_sum: 80.6609964966774
time_elpased: 2.227
batch start
#iterations: 378
currently lose_sum: 80.79294717311859
time_elpased: 2.236
batch start
#iterations: 379
currently lose_sum: 80.39910426735878
time_elpased: 2.232
start validation test
0.63793814433
0.632256163442
0.662447257384
0.646999698462
0.637895114802
62.010
batch start
#iterations: 380
currently lose_sum: 79.34843790531158
time_elpased: 2.256
batch start
#iterations: 381
currently lose_sum: 78.32209381461143
time_elpased: 2.25
batch start
#iterations: 382
currently lose_sum: 79.04848763346672
time_elpased: 2.231
batch start
#iterations: 383
currently lose_sum: 79.13387250900269
time_elpased: 2.254
batch start
#iterations: 384
currently lose_sum: 77.96558347344398
time_elpased: 2.259
batch start
#iterations: 385
currently lose_sum: 78.86646342277527
time_elpased: 2.304
batch start
#iterations: 386
currently lose_sum: 78.42128095030785
time_elpased: 2.256
batch start
#iterations: 387
currently lose_sum: 79.48035162687302
time_elpased: 2.233
batch start
#iterations: 388
currently lose_sum: 78.34443804621696
time_elpased: 2.267
batch start
#iterations: 389
currently lose_sum: 78.80168411135674
time_elpased: 2.258
batch start
#iterations: 390
currently lose_sum: 79.11824235320091
time_elpased: 2.295
batch start
#iterations: 391
currently lose_sum: 78.81262800097466
time_elpased: 2.217
batch start
#iterations: 392
currently lose_sum: 77.63068553805351
time_elpased: 2.233
batch start
#iterations: 393
currently lose_sum: 77.91071552038193
time_elpased: 2.23
batch start
#iterations: 394
currently lose_sum: 78.18605551123619
time_elpased: 2.249
batch start
#iterations: 395
currently lose_sum: 77.60179051756859
time_elpased: 2.211
batch start
#iterations: 396
currently lose_sum: 77.19607722759247
time_elpased: 2.221
batch start
#iterations: 397
currently lose_sum: 78.67880949378014
time_elpased: 2.286
batch start
#iterations: 398
currently lose_sum: 79.7076513171196
time_elpased: 2.23
batch start
#iterations: 399
currently lose_sum: 77.46006512641907
time_elpased: 2.274
start validation test
0.666030927835
0.689356725146
0.606565812494
0.645316691301
0.666135328017
64.768
acc: 0.688
pre: 0.671
rec: 0.738
F1: 0.703
auc: 0.746
