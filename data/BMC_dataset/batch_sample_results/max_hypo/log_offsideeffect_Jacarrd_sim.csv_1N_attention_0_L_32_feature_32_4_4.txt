start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.17000758647919
time_elpased: 2.5
batch start
#iterations: 1
currently lose_sum: 99.65332210063934
time_elpased: 2.543
batch start
#iterations: 2
currently lose_sum: 98.90276390314102
time_elpased: 2.471
batch start
#iterations: 3
currently lose_sum: 98.51702159643173
time_elpased: 2.321
batch start
#iterations: 4
currently lose_sum: 98.32202595472336
time_elpased: 2.539
batch start
#iterations: 5
currently lose_sum: 97.84464424848557
time_elpased: 2.379
batch start
#iterations: 6
currently lose_sum: 97.35012716054916
time_elpased: 2.222
batch start
#iterations: 7
currently lose_sum: 97.55806165933609
time_elpased: 2.343
batch start
#iterations: 8
currently lose_sum: 97.21664047241211
time_elpased: 2.45
batch start
#iterations: 9
currently lose_sum: 96.68393194675446
time_elpased: 2.259
batch start
#iterations: 10
currently lose_sum: 96.77559822797775
time_elpased: 2.37
batch start
#iterations: 11
currently lose_sum: 96.11267250776291
time_elpased: 2.581
batch start
#iterations: 12
currently lose_sum: 96.17320400476456
time_elpased: 2.079
batch start
#iterations: 13
currently lose_sum: 96.12656110525131
time_elpased: 2.227
batch start
#iterations: 14
currently lose_sum: 95.82818073034286
time_elpased: 2.346
batch start
#iterations: 15
currently lose_sum: 95.59986448287964
time_elpased: 2.111
batch start
#iterations: 16
currently lose_sum: 96.29971319437027
time_elpased: 2.289
batch start
#iterations: 17
currently lose_sum: 95.27146250009537
time_elpased: 2.559
batch start
#iterations: 18
currently lose_sum: 94.65468126535416
time_elpased: 2.607
batch start
#iterations: 19
currently lose_sum: 95.4870576262474
time_elpased: 2.352
start validation test
0.648298969072
0.600444259336
0.890192446228
0.7171578991
0.647874287764
60.817
batch start
#iterations: 20
currently lose_sum: 95.58779096603394
time_elpased: 2.105
batch start
#iterations: 21
currently lose_sum: 95.07940590381622
time_elpased: 2.505
batch start
#iterations: 22
currently lose_sum: 94.59689581394196
time_elpased: 2.386
batch start
#iterations: 23
currently lose_sum: 95.04345232248306
time_elpased: 2.37
batch start
#iterations: 24
currently lose_sum: 95.1726701259613
time_elpased: 2.404
batch start
#iterations: 25
currently lose_sum: 94.96643960475922
time_elpased: 2.443
batch start
#iterations: 26
currently lose_sum: 94.80986261367798
time_elpased: 2.413
batch start
#iterations: 27
currently lose_sum: 95.3182641863823
time_elpased: 2.385
batch start
#iterations: 28
currently lose_sum: 93.9538881778717
time_elpased: 2.654
batch start
#iterations: 29
currently lose_sum: 94.91472017765045
time_elpased: 2.342
batch start
#iterations: 30
currently lose_sum: 94.49376112222672
time_elpased: 2.378
batch start
#iterations: 31
currently lose_sum: 93.74387294054031
time_elpased: 2.4
batch start
#iterations: 32
currently lose_sum: 94.20861148834229
time_elpased: 2.556
batch start
#iterations: 33
currently lose_sum: 94.12779676914215
time_elpased: 2.542
batch start
#iterations: 34
currently lose_sum: 94.56131625175476
time_elpased: 2.271
batch start
#iterations: 35
currently lose_sum: 93.23323446512222
time_elpased: 2.264
batch start
#iterations: 36
currently lose_sum: 93.72713381052017
time_elpased: 2.605
batch start
#iterations: 37
currently lose_sum: 94.03898286819458
time_elpased: 2.535
batch start
#iterations: 38
currently lose_sum: 92.88622850179672
time_elpased: 2.54
batch start
#iterations: 39
currently lose_sum: 95.34149318933487
time_elpased: 2.325
start validation test
0.685463917526
0.653216919556
0.793043120305
0.716370735335
0.685275045642
59.377
batch start
#iterations: 40
currently lose_sum: 93.42410945892334
time_elpased: 2.503
batch start
#iterations: 41
currently lose_sum: 94.3215292096138
time_elpased: 2.324
batch start
#iterations: 42
currently lose_sum: 93.56831043958664
time_elpased: 2.48
batch start
#iterations: 43
currently lose_sum: 94.25410491228104
time_elpased: 2.334
batch start
#iterations: 44
currently lose_sum: 93.77371031045914
time_elpased: 2.627
batch start
#iterations: 45
currently lose_sum: 94.09580612182617
time_elpased: 2.244
batch start
#iterations: 46
currently lose_sum: 92.62106156349182
time_elpased: 2.497
batch start
#iterations: 47
currently lose_sum: 92.62954407930374
time_elpased: 2.504
batch start
#iterations: 48
currently lose_sum: 93.27569299936295
time_elpased: 2.373
batch start
#iterations: 49
currently lose_sum: 93.87087219953537
time_elpased: 2.447
batch start
#iterations: 50
currently lose_sum: 93.5215322971344
time_elpased: 2.459
batch start
#iterations: 51
currently lose_sum: 94.00234639644623
time_elpased: 2.308
batch start
#iterations: 52
currently lose_sum: 92.68253695964813
time_elpased: 2.283
batch start
#iterations: 53
currently lose_sum: 92.56572943925858
time_elpased: 2.517
batch start
#iterations: 54
currently lose_sum: 92.97082430124283
time_elpased: 2.464
batch start
#iterations: 55
currently lose_sum: 92.32855415344238
time_elpased: 2.46
batch start
#iterations: 56
currently lose_sum: 92.30119061470032
time_elpased: 2.326
batch start
#iterations: 57
currently lose_sum: 92.75137609243393
time_elpased: 2.248
batch start
#iterations: 58
currently lose_sum: 93.26721757650375
time_elpased: 2.598
batch start
#iterations: 59
currently lose_sum: 92.82497036457062
time_elpased: 1.842
start validation test
0.692577319588
0.658287642345
0.803128537614
0.723530502503
0.692383229873
58.150
batch start
#iterations: 60
currently lose_sum: 93.38185805082321
time_elpased: 2.38
batch start
#iterations: 61
currently lose_sum: 92.6728635430336
time_elpased: 2.251
batch start
#iterations: 62
currently lose_sum: 92.37449669837952
time_elpased: 2.528
batch start
#iterations: 63
currently lose_sum: 93.1763471364975
time_elpased: 2.232
batch start
#iterations: 64
currently lose_sum: 92.67976188659668
time_elpased: 2.3
batch start
#iterations: 65
currently lose_sum: 92.44285488128662
time_elpased: 2.575
batch start
#iterations: 66
currently lose_sum: 92.28170561790466
time_elpased: 2.249
batch start
#iterations: 67
currently lose_sum: 91.40535748004913
time_elpased: 2.192
batch start
#iterations: 68
currently lose_sum: 91.99643951654434
time_elpased: 2.185
batch start
#iterations: 69
currently lose_sum: 92.80574858188629
time_elpased: 2.343
batch start
#iterations: 70
currently lose_sum: 93.07543748617172
time_elpased: 2.235
batch start
#iterations: 71
currently lose_sum: 92.53057861328125
time_elpased: 2.225
batch start
#iterations: 72
currently lose_sum: 91.74851018190384
time_elpased: 2.433
batch start
#iterations: 73
currently lose_sum: 93.13409578800201
time_elpased: 2.441
batch start
#iterations: 74
currently lose_sum: 91.11009103059769
time_elpased: 2.407
batch start
#iterations: 75
currently lose_sum: 91.43648356199265
time_elpased: 2.42
batch start
#iterations: 76
currently lose_sum: 92.23208153247833
time_elpased: 2.356
batch start
#iterations: 77
currently lose_sum: 91.91817438602448
time_elpased: 2.398
batch start
#iterations: 78
currently lose_sum: 91.3643935918808
time_elpased: 2.538
batch start
#iterations: 79
currently lose_sum: 90.75320225954056
time_elpased: 2.464
start validation test
0.680721649485
0.64315318976
0.814448903983
0.718735809645
0.680486870663
57.856
batch start
#iterations: 80
currently lose_sum: 91.94431459903717
time_elpased: 2.399
batch start
#iterations: 81
currently lose_sum: 92.31877928972244
time_elpased: 2.439
batch start
#iterations: 82
currently lose_sum: 91.4648465514183
time_elpased: 2.366
batch start
#iterations: 83
currently lose_sum: 91.1096750497818
time_elpased: 2.364
batch start
#iterations: 84
currently lose_sum: 92.03217202425003
time_elpased: 2.125
batch start
#iterations: 85
currently lose_sum: 91.90967655181885
time_elpased: 2.424
batch start
#iterations: 86
currently lose_sum: 91.3010960817337
time_elpased: 2.585
batch start
#iterations: 87
currently lose_sum: 92.04751235246658
time_elpased: 2.485
batch start
#iterations: 88
currently lose_sum: 91.28343564271927
time_elpased: 2.443
batch start
#iterations: 89
currently lose_sum: 92.45166593790054
time_elpased: 2.417
batch start
#iterations: 90
currently lose_sum: 91.17546850442886
time_elpased: 2.241
batch start
#iterations: 91
currently lose_sum: 91.2029482126236
time_elpased: 2.097
batch start
#iterations: 92
currently lose_sum: 91.5401149392128
time_elpased: 2.49
batch start
#iterations: 93
currently lose_sum: 90.75866425037384
time_elpased: 2.229
batch start
#iterations: 94
currently lose_sum: 91.2051374912262
time_elpased: 2.175
batch start
#iterations: 95
currently lose_sum: 91.15259170532227
time_elpased: 2.208
batch start
#iterations: 96
currently lose_sum: 90.66898268461227
time_elpased: 2.389
batch start
#iterations: 97
currently lose_sum: 90.6758069396019
time_elpased: 2.406
batch start
#iterations: 98
currently lose_sum: 90.79524618387222
time_elpased: 2.598
batch start
#iterations: 99
currently lose_sum: 91.8047354221344
time_elpased: 2.233
start validation test
0.69381443299
0.673687114872
0.753833487702
0.711510441962
0.693709060282
57.209
batch start
#iterations: 100
currently lose_sum: 90.80025321245193
time_elpased: 2.36
batch start
#iterations: 101
currently lose_sum: 91.94261032342911
time_elpased: 2.448
batch start
#iterations: 102
currently lose_sum: 90.59744137525558
time_elpased: 2.113
batch start
#iterations: 103
currently lose_sum: 90.51112788915634
time_elpased: 2.338
batch start
#iterations: 104
currently lose_sum: 90.60570919513702
time_elpased: 2.226
batch start
#iterations: 105
currently lose_sum: 92.33613759279251
time_elpased: 2.402
batch start
#iterations: 106
currently lose_sum: 90.53378987312317
time_elpased: 2.398
batch start
#iterations: 107
currently lose_sum: 90.8243818283081
time_elpased: 2.5
batch start
#iterations: 108
currently lose_sum: 90.38184124231339
time_elpased: 2.21
batch start
#iterations: 109
currently lose_sum: 90.66368269920349
time_elpased: 2.159
batch start
#iterations: 110
currently lose_sum: 90.378144800663
time_elpased: 2.149
batch start
#iterations: 111
currently lose_sum: 90.10859698057175
time_elpased: 2.445
batch start
#iterations: 112
currently lose_sum: 90.5417270064354
time_elpased: 2.488
batch start
#iterations: 113
currently lose_sum: 89.83496189117432
time_elpased: 2.315
batch start
#iterations: 114
currently lose_sum: 90.80663472414017
time_elpased: 2.514
batch start
#iterations: 115
currently lose_sum: 90.453054189682
time_elpased: 2.499
batch start
#iterations: 116
currently lose_sum: 90.44574958086014
time_elpased: 2.37
batch start
#iterations: 117
currently lose_sum: 89.27909409999847
time_elpased: 2.328
batch start
#iterations: 118
currently lose_sum: 90.05556958913803
time_elpased: 2.493
batch start
#iterations: 119
currently lose_sum: 90.2274631857872
time_elpased: 2.396
start validation test
0.664742268041
0.701593675493
0.57538334877
0.63225149836
0.664899151407
59.933
batch start
#iterations: 120
currently lose_sum: 89.92218083143234
time_elpased: 2.448
batch start
#iterations: 121
currently lose_sum: 90.84450566768646
time_elpased: 2.367
batch start
#iterations: 122
currently lose_sum: 89.97878831624985
time_elpased: 2.407
batch start
#iterations: 123
currently lose_sum: 89.90742254257202
time_elpased: 2.394
batch start
#iterations: 124
currently lose_sum: 89.9641547203064
time_elpased: 2.392
batch start
#iterations: 125
currently lose_sum: 90.50969451665878
time_elpased: 2.374
batch start
#iterations: 126
currently lose_sum: 89.8242267370224
time_elpased: 2.409
batch start
#iterations: 127
currently lose_sum: 89.85988742113113
time_elpased: 2.016
batch start
#iterations: 128
currently lose_sum: 90.44776350259781
time_elpased: 2.12
batch start
#iterations: 129
currently lose_sum: 90.1989336013794
time_elpased: 2.413
batch start
#iterations: 130
currently lose_sum: 90.6110063791275
time_elpased: 2.398
batch start
#iterations: 131
currently lose_sum: 90.1212147474289
time_elpased: 2.544
batch start
#iterations: 132
currently lose_sum: 90.26810944080353
time_elpased: 2.349
batch start
#iterations: 133
currently lose_sum: 89.90083360671997
time_elpased: 2.363
batch start
#iterations: 134
currently lose_sum: 89.69238221645355
time_elpased: 2.054
batch start
#iterations: 135
currently lose_sum: 89.4512289762497
time_elpased: 2.333
batch start
#iterations: 136
currently lose_sum: 89.55741113424301
time_elpased: 2.393
batch start
#iterations: 137
currently lose_sum: 90.45212113857269
time_elpased: 2.314
batch start
#iterations: 138
currently lose_sum: 90.10224843025208
time_elpased: 2.443
batch start
#iterations: 139
currently lose_sum: 89.94360852241516
time_elpased: 2.506
start validation test
0.693144329897
0.660937232769
0.795410106
0.721965344916
0.692964786554
56.675
batch start
#iterations: 140
currently lose_sum: 90.45027840137482
time_elpased: 2.4
batch start
#iterations: 141
currently lose_sum: 90.39375531673431
time_elpased: 2.244
batch start
#iterations: 142
currently lose_sum: 89.6949872970581
time_elpased: 2.222
batch start
#iterations: 143
currently lose_sum: 89.18931496143341
time_elpased: 2.327
batch start
#iterations: 144
currently lose_sum: 89.73675066232681
time_elpased: 2.503
batch start
#iterations: 145
currently lose_sum: 89.33724415302277
time_elpased: 2.279
batch start
#iterations: 146
currently lose_sum: 89.45630633831024
time_elpased: 2.165
batch start
#iterations: 147
currently lose_sum: 89.36260050535202
time_elpased: 2.529
batch start
#iterations: 148
currently lose_sum: 88.96097308397293
time_elpased: 2.567
batch start
#iterations: 149
currently lose_sum: 89.3066069483757
time_elpased: 2.369
batch start
#iterations: 150
currently lose_sum: 89.54008734226227
time_elpased: 2.445
batch start
#iterations: 151
currently lose_sum: 90.13299828767776
time_elpased: 2.424
batch start
#iterations: 152
currently lose_sum: 89.09771031141281
time_elpased: 2.571
batch start
#iterations: 153
currently lose_sum: 89.43244856595993
time_elpased: 2.578
batch start
#iterations: 154
currently lose_sum: 89.48615205287933
time_elpased: 2.46
batch start
#iterations: 155
currently lose_sum: 89.95368552207947
time_elpased: 2.511
batch start
#iterations: 156
currently lose_sum: 89.99892789125443
time_elpased: 2.271
batch start
#iterations: 157
currently lose_sum: 88.81222432851791
time_elpased: 2.275
batch start
#iterations: 158
currently lose_sum: 90.41538178920746
time_elpased: 2.487
batch start
#iterations: 159
currently lose_sum: 89.62898516654968
time_elpased: 2.302
start validation test
0.675309278351
0.671897002615
0.687454975816
0.679586957628
0.675287954705
57.289
batch start
#iterations: 160
currently lose_sum: 88.53466802835464
time_elpased: 2.344
batch start
#iterations: 161
currently lose_sum: 89.23074811697006
time_elpased: 2.2
batch start
#iterations: 162
currently lose_sum: 89.66797596216202
time_elpased: 2.393
batch start
#iterations: 163
currently lose_sum: 88.53257870674133
time_elpased: 2.615
batch start
#iterations: 164
currently lose_sum: 89.58925169706345
time_elpased: 1.983
batch start
#iterations: 165
currently lose_sum: 89.77433747053146
time_elpased: 2.4
batch start
#iterations: 166
currently lose_sum: 88.82068538665771
time_elpased: 2.44
batch start
#iterations: 167
currently lose_sum: 89.46906042098999
time_elpased: 2.379
batch start
#iterations: 168
currently lose_sum: 89.40271812677383
time_elpased: 2.043
batch start
#iterations: 169
currently lose_sum: 88.49359184503555
time_elpased: 1.887
batch start
#iterations: 170
currently lose_sum: 88.48736208677292
time_elpased: 2.058
batch start
#iterations: 171
currently lose_sum: 88.73235201835632
time_elpased: 2.089
batch start
#iterations: 172
currently lose_sum: 89.18443650007248
time_elpased: 1.948
batch start
#iterations: 173
currently lose_sum: 88.16810423135757
time_elpased: 1.964
batch start
#iterations: 174
currently lose_sum: 88.3983901143074
time_elpased: 2.235
batch start
#iterations: 175
currently lose_sum: 89.32336604595184
time_elpased: 2.36
batch start
#iterations: 176
currently lose_sum: 88.48555940389633
time_elpased: 2.29
batch start
#iterations: 177
currently lose_sum: 88.49210369586945
time_elpased: 2.461
batch start
#iterations: 178
currently lose_sum: 88.20333534479141
time_elpased: 2.338
batch start
#iterations: 179
currently lose_sum: 88.29225474596024
time_elpased: 2.341
start validation test
0.567371134021
0.547804737146
0.780693629721
0.643836197751
0.566996613477
65.070
batch start
#iterations: 180
currently lose_sum: 89.47206604480743
time_elpased: 2.438
batch start
#iterations: 181
currently lose_sum: 88.52415245771408
time_elpased: 2.132
batch start
#iterations: 182
currently lose_sum: 88.28527057170868
time_elpased: 2.181
batch start
#iterations: 183
currently lose_sum: 88.95182859897614
time_elpased: 2.483
batch start
#iterations: 184
currently lose_sum: 89.08406692743301
time_elpased: 2.325
batch start
#iterations: 185
currently lose_sum: 88.41101187467575
time_elpased: 2.318
batch start
#iterations: 186
currently lose_sum: 88.61948704719543
time_elpased: 2.368
batch start
#iterations: 187
currently lose_sum: 88.38930654525757
time_elpased: 2.302
batch start
#iterations: 188
currently lose_sum: 88.63504832983017
time_elpased: 2.233
batch start
#iterations: 189
currently lose_sum: 89.34086066484451
time_elpased: 2.466
batch start
#iterations: 190
currently lose_sum: 87.57893627882004
time_elpased: 2.528
batch start
#iterations: 191
currently lose_sum: 89.64863127470016
time_elpased: 2.229
batch start
#iterations: 192
currently lose_sum: 87.98702001571655
time_elpased: 2.003
batch start
#iterations: 193
currently lose_sum: 88.69188857078552
time_elpased: 2.178
batch start
#iterations: 194
currently lose_sum: 87.98787772655487
time_elpased: 2.164
batch start
#iterations: 195
currently lose_sum: 88.73510593175888
time_elpased: 2.116
batch start
#iterations: 196
currently lose_sum: 88.43652653694153
time_elpased: 2.243
batch start
#iterations: 197
currently lose_sum: 89.73592525720596
time_elpased: 2.395
batch start
#iterations: 198
currently lose_sum: 88.4427073597908
time_elpased: 2.374
batch start
#iterations: 199
currently lose_sum: 88.24100488424301
time_elpased: 2.389
start validation test
0.603711340206
0.568128399705
0.870639086138
0.687581274382
0.603242707377
63.078
batch start
#iterations: 200
currently lose_sum: 88.37697690725327
time_elpased: 2.482
batch start
#iterations: 201
currently lose_sum: 88.05693882703781
time_elpased: 2.66
batch start
#iterations: 202
currently lose_sum: 88.41816413402557
time_elpased: 2.55
batch start
#iterations: 203
currently lose_sum: 88.36643463373184
time_elpased: 2.372
batch start
#iterations: 204
currently lose_sum: 88.54265320301056
time_elpased: 2.492
batch start
#iterations: 205
currently lose_sum: 87.879858314991
time_elpased: 2.635
batch start
#iterations: 206
currently lose_sum: 88.35767412185669
time_elpased: 2.588
batch start
#iterations: 207
currently lose_sum: 87.94524478912354
time_elpased: 2.658
batch start
#iterations: 208
currently lose_sum: 89.13354653120041
time_elpased: 2.294
batch start
#iterations: 209
currently lose_sum: 87.80175948143005
time_elpased: 2.421
batch start
#iterations: 210
currently lose_sum: 87.85109758377075
time_elpased: 2.015
batch start
#iterations: 211
currently lose_sum: 87.48447108268738
time_elpased: 2.443
batch start
#iterations: 212
currently lose_sum: 87.57565242052078
time_elpased: 2.267
batch start
#iterations: 213
currently lose_sum: 88.31523275375366
time_elpased: 2.564
batch start
#iterations: 214
currently lose_sum: 88.17541909217834
time_elpased: 2.475
batch start
#iterations: 215
currently lose_sum: 87.1291481256485
time_elpased: 2.364
batch start
#iterations: 216
currently lose_sum: 87.6641263961792
time_elpased: 2.338
batch start
#iterations: 217
currently lose_sum: 87.97799301147461
time_elpased: 2.334
batch start
#iterations: 218
currently lose_sum: 88.39825564622879
time_elpased: 1.983
batch start
#iterations: 219
currently lose_sum: 88.19848090410233
time_elpased: 2.314
start validation test
0.661701030928
0.709540260431
0.549552330966
0.619381778113
0.661897925268
59.168
batch start
#iterations: 220
currently lose_sum: 87.90125554800034
time_elpased: 2.324
batch start
#iterations: 221
currently lose_sum: 87.23849445581436
time_elpased: 2.49
batch start
#iterations: 222
currently lose_sum: 88.29194110631943
time_elpased: 2.594
batch start
#iterations: 223
currently lose_sum: 88.7614044547081
time_elpased: 2.533
batch start
#iterations: 224
currently lose_sum: 87.97529375553131
time_elpased: 2.176
batch start
#iterations: 225
currently lose_sum: 88.17302751541138
time_elpased: 2.361
batch start
#iterations: 226
currently lose_sum: 87.89954590797424
time_elpased: 2.581
batch start
#iterations: 227
currently lose_sum: 87.96424692869186
time_elpased: 2.45
batch start
#iterations: 228
currently lose_sum: 87.8379687666893
time_elpased: 2.49
batch start
#iterations: 229
currently lose_sum: 87.70227789878845
time_elpased: 2.397
batch start
#iterations: 230
currently lose_sum: 88.29966825246811
time_elpased: 2.428
batch start
#iterations: 231
currently lose_sum: 88.00062936544418
time_elpased: 2.419
batch start
#iterations: 232
currently lose_sum: 88.46035897731781
time_elpased: 2.191
batch start
#iterations: 233
currently lose_sum: 87.78911596536636
time_elpased: 2.142
batch start
#iterations: 234
currently lose_sum: 87.39670300483704
time_elpased: 2.305
batch start
#iterations: 235
currently lose_sum: 87.52772361040115
time_elpased: 2.351
batch start
#iterations: 236
currently lose_sum: 87.97379922866821
time_elpased: 1.981
batch start
#iterations: 237
currently lose_sum: 87.63483822345734
time_elpased: 2.376
batch start
#iterations: 238
currently lose_sum: 88.02202343940735
time_elpased: 2.474
batch start
#iterations: 239
currently lose_sum: 87.66616415977478
time_elpased: 2.523
start validation test
0.664484536082
0.634835238736
0.777194607389
0.698838661916
0.664286656168
58.083
batch start
#iterations: 240
currently lose_sum: 88.39092427492142
time_elpased: 1.998
batch start
#iterations: 241
currently lose_sum: 88.95336723327637
time_elpased: 2.396
batch start
#iterations: 242
currently lose_sum: 87.26644140481949
time_elpased: 2.245
batch start
#iterations: 243
currently lose_sum: 88.90519362688065
time_elpased: 2.474
batch start
#iterations: 244
currently lose_sum: 87.31576216220856
time_elpased: 2.427
batch start
#iterations: 245
currently lose_sum: 87.45062536001205
time_elpased: 2.283
batch start
#iterations: 246
currently lose_sum: 87.54267793893814
time_elpased: 2.42
batch start
#iterations: 247
currently lose_sum: 87.45583772659302
time_elpased: 2.612
batch start
#iterations: 248
currently lose_sum: 87.79240626096725
time_elpased: 2.485
batch start
#iterations: 249
currently lose_sum: 87.05672538280487
time_elpased: 2.439
batch start
#iterations: 250
currently lose_sum: 87.64105170965195
time_elpased: 2.308
batch start
#iterations: 251
currently lose_sum: 87.54931247234344
time_elpased: 2.38
batch start
#iterations: 252
currently lose_sum: 87.65194690227509
time_elpased: 2.35
batch start
#iterations: 253
currently lose_sum: 86.87155646085739
time_elpased: 2.574
batch start
#iterations: 254
currently lose_sum: 86.87418699264526
time_elpased: 2.501
batch start
#iterations: 255
currently lose_sum: 87.90738052129745
time_elpased: 2.475
batch start
#iterations: 256
currently lose_sum: 87.2490884065628
time_elpased: 2.36
batch start
#iterations: 257
currently lose_sum: 87.92235714197159
time_elpased: 2.388
batch start
#iterations: 258
currently lose_sum: 86.616739153862
time_elpased: 2.225
batch start
#iterations: 259
currently lose_sum: 87.17890632152557
time_elpased: 2.398
start validation test
0.691237113402
0.682035752662
0.718534527117
0.699809561993
0.691189188582
56.551
batch start
#iterations: 260
currently lose_sum: 87.63134014606476
time_elpased: 2.451
batch start
#iterations: 261
currently lose_sum: 86.89456623792648
time_elpased: 2.289
batch start
#iterations: 262
currently lose_sum: 87.9223421216011
time_elpased: 2.495
batch start
#iterations: 263
currently lose_sum: 86.88058996200562
time_elpased: 2.301
batch start
#iterations: 264
currently lose_sum: 87.16244953870773
time_elpased: 2.06
batch start
#iterations: 265
currently lose_sum: 87.54158329963684
time_elpased: 2.059
batch start
#iterations: 266
currently lose_sum: 86.85402739048004
time_elpased: 2.254
batch start
#iterations: 267
currently lose_sum: 86.79091417789459
time_elpased: 2.545
batch start
#iterations: 268
currently lose_sum: 87.77060317993164
time_elpased: 2.35
batch start
#iterations: 269
currently lose_sum: 87.51556658744812
time_elpased: 2.428
batch start
#iterations: 270
currently lose_sum: 87.47287172079086
time_elpased: 2.327
batch start
#iterations: 271
currently lose_sum: 87.29783231019974
time_elpased: 2.36
batch start
#iterations: 272
currently lose_sum: 87.31848579645157
time_elpased: 2.483
batch start
#iterations: 273
currently lose_sum: 86.84643733501434
time_elpased: 2.167
batch start
#iterations: 274
currently lose_sum: 87.09801518917084
time_elpased: 2.216
batch start
#iterations: 275
currently lose_sum: 86.86874306201935
time_elpased: 2.212
batch start
#iterations: 276
currently lose_sum: 86.72064340114594
time_elpased: 2.311
batch start
#iterations: 277
currently lose_sum: 87.02016109228134
time_elpased: 2.461
batch start
#iterations: 278
currently lose_sum: 85.98873847723007
time_elpased: 2.618
batch start
#iterations: 279
currently lose_sum: 86.82320606708527
time_elpased: 2.669
start validation test
0.674072164948
0.645790378007
0.773592672636
0.703937818982
0.673897441347
57.319
batch start
#iterations: 280
currently lose_sum: 87.40428012609482
time_elpased: 2.144
batch start
#iterations: 281
currently lose_sum: 87.81944799423218
time_elpased: 2.395
batch start
#iterations: 282
currently lose_sum: 87.30451786518097
time_elpased: 2.203
batch start
#iterations: 283
currently lose_sum: 87.61306643486023
time_elpased: 2.584
batch start
#iterations: 284
currently lose_sum: 86.6004239320755
time_elpased: 2.524
batch start
#iterations: 285
currently lose_sum: 87.15312838554382
time_elpased: 2.588
batch start
#iterations: 286
currently lose_sum: 86.87223428487778
time_elpased: 2.294
batch start
#iterations: 287
currently lose_sum: 86.52298492193222
time_elpased: 2.424
batch start
#iterations: 288
currently lose_sum: 86.85251837968826
time_elpased: 2.398
batch start
#iterations: 289
currently lose_sum: 86.28279387950897
time_elpased: 2.214
batch start
#iterations: 290
currently lose_sum: 87.59125000238419
time_elpased: 2.492
batch start
#iterations: 291
currently lose_sum: 86.79000872373581
time_elpased: 2.439
batch start
#iterations: 292
currently lose_sum: 86.47752130031586
time_elpased: 2.314
batch start
#iterations: 293
currently lose_sum: 86.96893101930618
time_elpased: 2.416
batch start
#iterations: 294
currently lose_sum: 86.29384696483612
time_elpased: 2.432
batch start
#iterations: 295
currently lose_sum: 86.34314602613449
time_elpased: 2.433
batch start
#iterations: 296
currently lose_sum: 86.43555700778961
time_elpased: 2.406
batch start
#iterations: 297
currently lose_sum: 87.05275213718414
time_elpased: 2.346
batch start
#iterations: 298
currently lose_sum: 87.27471160888672
time_elpased: 2.424
batch start
#iterations: 299
currently lose_sum: 85.84248453378677
time_elpased: 2.34
start validation test
0.680515463918
0.639631775256
0.829474117526
0.722286943274
0.680253944026
56.972
batch start
#iterations: 300
currently lose_sum: 86.3023607134819
time_elpased: 2.316
batch start
#iterations: 301
currently lose_sum: 86.54755395650864
time_elpased: 2.307
batch start
#iterations: 302
currently lose_sum: 86.47066795825958
time_elpased: 2.352
batch start
#iterations: 303
currently lose_sum: 86.38441520929337
time_elpased: 2.465
batch start
#iterations: 304
currently lose_sum: 87.14656203985214
time_elpased: 2.308
batch start
#iterations: 305
currently lose_sum: 86.28147566318512
time_elpased: 2.276
batch start
#iterations: 306
currently lose_sum: 86.76193875074387
time_elpased: 1.964
batch start
#iterations: 307
currently lose_sum: 86.8779958486557
time_elpased: 2.295
batch start
#iterations: 308
currently lose_sum: 86.16693669557571
time_elpased: 2.239
batch start
#iterations: 309
currently lose_sum: 85.98527604341507
time_elpased: 2.487
batch start
#iterations: 310
currently lose_sum: 86.13493341207504
time_elpased: 2.376
batch start
#iterations: 311
currently lose_sum: 85.61356055736542
time_elpased: 2.411
batch start
#iterations: 312
currently lose_sum: 86.39882975816727
time_elpased: 2.432
batch start
#iterations: 313
currently lose_sum: 86.85515207052231
time_elpased: 2.563
batch start
#iterations: 314
currently lose_sum: 85.8839522600174
time_elpased: 2.449
batch start
#iterations: 315
currently lose_sum: 85.7961795926094
time_elpased: 2.238
batch start
#iterations: 316
currently lose_sum: 85.22364300489426
time_elpased: 2.224
batch start
#iterations: 317
currently lose_sum: 86.3671224117279
time_elpased: 2.472
batch start
#iterations: 318
currently lose_sum: 86.49210858345032
time_elpased: 2.398
batch start
#iterations: 319
currently lose_sum: 86.49225461483002
time_elpased: 2.284
start validation test
0.636082474227
0.629043224866
0.666460841824
0.647211672996
0.636029140317
59.800
batch start
#iterations: 320
currently lose_sum: 86.43455266952515
time_elpased: 2.435
batch start
#iterations: 321
currently lose_sum: 86.25115436315536
time_elpased: 2.3
batch start
#iterations: 322
currently lose_sum: 86.57649058103561
time_elpased: 2.422
batch start
#iterations: 323
currently lose_sum: 86.55401134490967
time_elpased: 2.349
batch start
#iterations: 324
currently lose_sum: 86.12331575155258
time_elpased: 2.482
batch start
#iterations: 325
currently lose_sum: 86.57484382390976
time_elpased: 2.295
batch start
#iterations: 326
currently lose_sum: 86.80901581048965
time_elpased: 2.283
batch start
#iterations: 327
currently lose_sum: 86.72610169649124
time_elpased: 2.137
batch start
#iterations: 328
currently lose_sum: 86.59071040153503
time_elpased: 2.473
batch start
#iterations: 329
currently lose_sum: 86.376311480999
time_elpased: 2.403
batch start
#iterations: 330
currently lose_sum: 86.2348365187645
time_elpased: 2.474
batch start
#iterations: 331
currently lose_sum: 87.0035999417305
time_elpased: 2.365
batch start
#iterations: 332
currently lose_sum: 85.91093307733536
time_elpased: 2.627
batch start
#iterations: 333
currently lose_sum: 85.23204720020294
time_elpased: 2.497
batch start
#iterations: 334
currently lose_sum: 85.95085203647614
time_elpased: 2.481
batch start
#iterations: 335
currently lose_sum: 86.88290387392044
time_elpased: 2.284
batch start
#iterations: 336
currently lose_sum: 86.08922320604324
time_elpased: 2.159
batch start
#iterations: 337
currently lose_sum: 86.5907991528511
time_elpased: 2.155
batch start
#iterations: 338
currently lose_sum: 85.66768312454224
time_elpased: 2.393
batch start
#iterations: 339
currently lose_sum: 86.52253723144531
time_elpased: 2.423
start validation test
0.662783505155
0.637101649538
0.759184933621
0.692806160781
0.662614257578
58.241
batch start
#iterations: 340
currently lose_sum: 87.44096332788467
time_elpased: 2.463
batch start
#iterations: 341
currently lose_sum: 86.61075913906097
time_elpased: 2.218
batch start
#iterations: 342
currently lose_sum: 85.55473738908768
time_elpased: 1.924
batch start
#iterations: 343
currently lose_sum: 85.6594409942627
time_elpased: 2.248
batch start
#iterations: 344
currently lose_sum: 86.91200441122055
time_elpased: 2.288
batch start
#iterations: 345
currently lose_sum: 86.36031246185303
time_elpased: 2.487
batch start
#iterations: 346
currently lose_sum: 85.52676939964294
time_elpased: 2.4
batch start
#iterations: 347
currently lose_sum: 86.10719084739685
time_elpased: 2.13
batch start
#iterations: 348
currently lose_sum: 85.73056852817535
time_elpased: 2.436
batch start
#iterations: 349
currently lose_sum: 85.72448402643204
time_elpased: 2.788
batch start
#iterations: 350
currently lose_sum: 85.93895107507706
time_elpased: 2.349
batch start
#iterations: 351
currently lose_sum: 86.58700549602509
time_elpased: 2.307
batch start
#iterations: 352
currently lose_sum: 85.3238713145256
time_elpased: 2.366
batch start
#iterations: 353
currently lose_sum: 86.0884605050087
time_elpased: 2.371
batch start
#iterations: 354
currently lose_sum: 84.9312555193901
time_elpased: 2.559
batch start
#iterations: 355
currently lose_sum: 86.00743842124939
time_elpased: 2.382
batch start
#iterations: 356
currently lose_sum: 85.60355907678604
time_elpased: 2.521
batch start
#iterations: 357
currently lose_sum: 85.33715015649796
time_elpased: 2.246
batch start
#iterations: 358
currently lose_sum: 85.81127816438675
time_elpased: 2.136
batch start
#iterations: 359
currently lose_sum: 87.61133402585983
time_elpased: 2.271
start validation test
0.680979381443
0.67714400482
0.693938458372
0.685438373571
0.680956629785
56.664
batch start
#iterations: 360
currently lose_sum: 85.63463747501373
time_elpased: 2.472
batch start
#iterations: 361
currently lose_sum: 86.34178799390793
time_elpased: 2.073
batch start
#iterations: 362
currently lose_sum: 86.1942190527916
time_elpased: 2.105
batch start
#iterations: 363
currently lose_sum: 86.3489727973938
time_elpased: 2.378
batch start
#iterations: 364
currently lose_sum: 85.66203761100769
time_elpased: 2.441
batch start
#iterations: 365
currently lose_sum: 85.16345477104187
time_elpased: 2.719
batch start
#iterations: 366
currently lose_sum: 85.51019167900085
time_elpased: 2.595
batch start
#iterations: 367
currently lose_sum: 85.5281143784523
time_elpased: 2.473
batch start
#iterations: 368
currently lose_sum: 85.46095341444016
time_elpased: 2.359
batch start
#iterations: 369
currently lose_sum: 85.81701737642288
time_elpased: 2.324
batch start
#iterations: 370
currently lose_sum: 85.67884093523026
time_elpased: 2.667
batch start
#iterations: 371
currently lose_sum: 85.60953956842422
time_elpased: 2.543
batch start
#iterations: 372
currently lose_sum: 84.89863795042038
time_elpased: 2.819
batch start
#iterations: 373
currently lose_sum: 86.3324294090271
time_elpased: 2.365
batch start
#iterations: 374
currently lose_sum: 85.28934383392334
time_elpased: 2.377
batch start
#iterations: 375
currently lose_sum: 87.27185744047165
time_elpased: 2.474
batch start
#iterations: 376
currently lose_sum: 86.20598596334457
time_elpased: 2.201
batch start
#iterations: 377
currently lose_sum: 85.40493130683899
time_elpased: 2.138
batch start
#iterations: 378
currently lose_sum: 85.46444422006607
time_elpased: 2.425
batch start
#iterations: 379
currently lose_sum: 85.77268534898758
time_elpased: 2.588
start validation test
0.696804123711
0.693980778958
0.705979211691
0.699928578716
0.696788015429
56.156
batch start
#iterations: 380
currently lose_sum: 86.63654887676239
time_elpased: 2.517
batch start
#iterations: 381
currently lose_sum: 85.9233905673027
time_elpased: 2.562
batch start
#iterations: 382
currently lose_sum: 85.30166190862656
time_elpased: 2.411
batch start
#iterations: 383
currently lose_sum: 84.99869853258133
time_elpased: 2.506
batch start
#iterations: 384
currently lose_sum: 84.66490811109543
time_elpased: 2.318
batch start
#iterations: 385
currently lose_sum: 85.22860181331635
time_elpased: 2.395
batch start
#iterations: 386
currently lose_sum: 85.02027243375778
time_elpased: 2.463
batch start
#iterations: 387
currently lose_sum: 85.16625028848648
time_elpased: 2.54
batch start
#iterations: 388
currently lose_sum: 85.05794674158096
time_elpased: 2.642
batch start
#iterations: 389
currently lose_sum: 85.744644343853
time_elpased: 2.526
batch start
#iterations: 390
currently lose_sum: 86.3105999827385
time_elpased: 2.719
batch start
#iterations: 391
currently lose_sum: 85.94065934419632
time_elpased: 2.406
batch start
#iterations: 392
currently lose_sum: 85.09151947498322
time_elpased: 2.604
batch start
#iterations: 393
currently lose_sum: 84.7439792752266
time_elpased: 2.44
batch start
#iterations: 394
currently lose_sum: 86.00481653213501
time_elpased: 2.107
batch start
#iterations: 395
currently lose_sum: 86.20860302448273
time_elpased: 2.206
batch start
#iterations: 396
currently lose_sum: 85.84709507226944
time_elpased: 2.327
batch start
#iterations: 397
currently lose_sum: 86.10335433483124
time_elpased: 2.515
batch start
#iterations: 398
currently lose_sum: 85.18260031938553
time_elpased: 2.372
batch start
#iterations: 399
currently lose_sum: 85.31856191158295
time_elpased: 2.421
start validation test
0.558556701031
0.545750337275
0.707728722857
0.616273859665
0.558294806538
65.857
acc: 0.691
pre: 0.687
rec: 0.706
F1: 0.696
auc: 0.750
