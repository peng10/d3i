start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.17680490016937
time_elpased: 1.818
batch start
#iterations: 1
currently lose_sum: 99.55858433246613
time_elpased: 1.826
batch start
#iterations: 2
currently lose_sum: 99.1054139137268
time_elpased: 1.791
batch start
#iterations: 3
currently lose_sum: 98.48718339204788
time_elpased: 1.805
batch start
#iterations: 4
currently lose_sum: 97.8351702094078
time_elpased: 1.789
batch start
#iterations: 5
currently lose_sum: 98.39266711473465
time_elpased: 1.793
batch start
#iterations: 6
currently lose_sum: 97.650295317173
time_elpased: 1.834
batch start
#iterations: 7
currently lose_sum: 97.907863676548
time_elpased: 1.796
batch start
#iterations: 8
currently lose_sum: 97.29545855522156
time_elpased: 1.773
batch start
#iterations: 9
currently lose_sum: 98.06391030550003
time_elpased: 1.794
batch start
#iterations: 10
currently lose_sum: 98.26001459360123
time_elpased: 1.817
batch start
#iterations: 11
currently lose_sum: 97.3488958477974
time_elpased: 1.79
batch start
#iterations: 12
currently lose_sum: 98.84179878234863
time_elpased: 1.773
batch start
#iterations: 13
currently lose_sum: 97.34808796644211
time_elpased: 1.817
batch start
#iterations: 14
currently lose_sum: 98.29582625627518
time_elpased: 1.816
batch start
#iterations: 15
currently lose_sum: 97.26428323984146
time_elpased: 1.85
batch start
#iterations: 16
currently lose_sum: 99.91433924436569
time_elpased: 1.799
batch start
#iterations: 17
currently lose_sum: 96.98058515787125
time_elpased: 1.787
batch start
#iterations: 18
currently lose_sum: 96.27943587303162
time_elpased: 1.796
batch start
#iterations: 19
currently lose_sum: 98.21116405725479
time_elpased: 1.79
start validation test
0.619948453608
0.603716814159
0.702068539673
0.649188751963
0.619804279131
67.096
batch start
#iterations: 20
currently lose_sum: 97.5699811577797
time_elpased: 1.743
batch start
#iterations: 21
currently lose_sum: 96.208027780056
time_elpased: 1.773
batch start
#iterations: 22
currently lose_sum: 96.19151717424393
time_elpased: 1.824
batch start
#iterations: 23
currently lose_sum: 96.37857055664062
time_elpased: 1.83
batch start
#iterations: 24
currently lose_sum: 95.65321749448776
time_elpased: 1.771
batch start
#iterations: 25
currently lose_sum: 95.88904297351837
time_elpased: 1.816
batch start
#iterations: 26
currently lose_sum: 95.86864471435547
time_elpased: 1.842
batch start
#iterations: 27
currently lose_sum: 95.76668787002563
time_elpased: 1.838
batch start
#iterations: 28
currently lose_sum: 95.56711572408676
time_elpased: 1.818
batch start
#iterations: 29
currently lose_sum: 97.04571968317032
time_elpased: 1.797
batch start
#iterations: 30
currently lose_sum: 94.9122451543808
time_elpased: 1.764
batch start
#iterations: 31
currently lose_sum: 94.78031611442566
time_elpased: 1.786
batch start
#iterations: 32
currently lose_sum: 94.57443177700043
time_elpased: 1.831
batch start
#iterations: 33
currently lose_sum: 95.03759932518005
time_elpased: 1.809
batch start
#iterations: 34
currently lose_sum: 94.70699137449265
time_elpased: 1.783
batch start
#iterations: 35
currently lose_sum: 94.4375022649765
time_elpased: 1.77
batch start
#iterations: 36
currently lose_sum: 94.28917413949966
time_elpased: 1.779
batch start
#iterations: 37
currently lose_sum: 94.96345889568329
time_elpased: 1.774
batch start
#iterations: 38
currently lose_sum: 94.60461497306824
time_elpased: 1.772
batch start
#iterations: 39
currently lose_sum: 94.28827172517776
time_elpased: 1.782
start validation test
0.671958762887
0.651953231215
0.74024904806
0.693301204819
0.671838868758
59.281
batch start
#iterations: 40
currently lose_sum: 93.91291463375092
time_elpased: 1.78
batch start
#iterations: 41
currently lose_sum: 94.37260621786118
time_elpased: 1.771
batch start
#iterations: 42
currently lose_sum: 94.42691880464554
time_elpased: 1.733
batch start
#iterations: 43
currently lose_sum: 94.2046183347702
time_elpased: 1.8
batch start
#iterations: 44
currently lose_sum: 100.23556274175644
time_elpased: 1.798
batch start
#iterations: 45
currently lose_sum: 100.46276158094406
time_elpased: 1.843
batch start
#iterations: 46
currently lose_sum: 97.60344815254211
time_elpased: 1.787
batch start
#iterations: 47
currently lose_sum: 96.56195467710495
time_elpased: 1.789
batch start
#iterations: 48
currently lose_sum: 99.2219266295433
time_elpased: 1.753
batch start
#iterations: 49
currently lose_sum: 95.38215988874435
time_elpased: 1.811
batch start
#iterations: 50
currently lose_sum: 94.07615315914154
time_elpased: 1.799
batch start
#iterations: 51
currently lose_sum: 94.6762587428093
time_elpased: 1.843
batch start
#iterations: 52
currently lose_sum: 94.094642162323
time_elpased: 1.8
batch start
#iterations: 53
currently lose_sum: 94.31721776723862
time_elpased: 1.791
batch start
#iterations: 54
currently lose_sum: 94.00558745861053
time_elpased: 1.763
batch start
#iterations: 55
currently lose_sum: 93.49537390470505
time_elpased: 1.798
batch start
#iterations: 56
currently lose_sum: 93.70734691619873
time_elpased: 1.774
batch start
#iterations: 57
currently lose_sum: 93.62052994966507
time_elpased: 1.792
batch start
#iterations: 58
currently lose_sum: 93.95770567655563
time_elpased: 1.777
batch start
#iterations: 59
currently lose_sum: 93.74675643444061
time_elpased: 1.776
start validation test
0.682474226804
0.660732037958
0.752392713801
0.703589644885
0.682351474116
58.363
batch start
#iterations: 60
currently lose_sum: 94.69285243749619
time_elpased: 1.773
batch start
#iterations: 61
currently lose_sum: 94.30071359872818
time_elpased: 1.809
batch start
#iterations: 62
currently lose_sum: 94.11599206924438
time_elpased: 1.757
batch start
#iterations: 63
currently lose_sum: 93.9326885342598
time_elpased: 1.788
batch start
#iterations: 64
currently lose_sum: 94.37742239236832
time_elpased: 1.835
batch start
#iterations: 65
currently lose_sum: 92.50394600629807
time_elpased: 1.797
batch start
#iterations: 66
currently lose_sum: 93.26737701892853
time_elpased: 1.802
batch start
#iterations: 67
currently lose_sum: 93.06893110275269
time_elpased: 1.809
batch start
#iterations: 68
currently lose_sum: 93.54179626703262
time_elpased: 1.826
batch start
#iterations: 69
currently lose_sum: 93.9073491692543
time_elpased: 1.813
batch start
#iterations: 70
currently lose_sum: 92.27360039949417
time_elpased: 1.756
batch start
#iterations: 71
currently lose_sum: 93.6373393535614
time_elpased: 1.789
batch start
#iterations: 72
currently lose_sum: 95.1308987736702
time_elpased: 1.783
batch start
#iterations: 73
currently lose_sum: 92.55155956745148
time_elpased: 1.79
batch start
#iterations: 74
currently lose_sum: 92.79747951030731
time_elpased: 1.829
batch start
#iterations: 75
currently lose_sum: 93.87219995260239
time_elpased: 1.804
batch start
#iterations: 76
currently lose_sum: 93.07055062055588
time_elpased: 1.771
batch start
#iterations: 77
currently lose_sum: 92.81810456514359
time_elpased: 1.818
batch start
#iterations: 78
currently lose_sum: 92.74323344230652
time_elpased: 1.76
batch start
#iterations: 79
currently lose_sum: 93.21079188585281
time_elpased: 1.772
start validation test
0.684020618557
0.673570115165
0.716270453844
0.694264339152
0.683963998997
59.041
batch start
#iterations: 80
currently lose_sum: 93.82490807771683
time_elpased: 1.84
batch start
#iterations: 81
currently lose_sum: 93.05452698469162
time_elpased: 1.769
batch start
#iterations: 82
currently lose_sum: 92.79144132137299
time_elpased: 1.787
batch start
#iterations: 83
currently lose_sum: 92.43141984939575
time_elpased: 1.762
batch start
#iterations: 84
currently lose_sum: 93.14494371414185
time_elpased: 1.786
batch start
#iterations: 85
currently lose_sum: 92.82808935642242
time_elpased: 1.802
batch start
#iterations: 86
currently lose_sum: 94.18467378616333
time_elpased: 1.771
batch start
#iterations: 87
currently lose_sum: 92.97066450119019
time_elpased: 1.781
batch start
#iterations: 88
currently lose_sum: 96.44934391975403
time_elpased: 1.742
batch start
#iterations: 89
currently lose_sum: 99.70531040430069
time_elpased: 1.802
batch start
#iterations: 90
currently lose_sum: 92.40935450792313
time_elpased: 1.766
batch start
#iterations: 91
currently lose_sum: 93.3361884355545
time_elpased: 1.792
batch start
#iterations: 92
currently lose_sum: 92.6618303656578
time_elpased: 1.729
batch start
#iterations: 93
currently lose_sum: 93.16487151384354
time_elpased: 1.804
batch start
#iterations: 94
currently lose_sum: 92.39860785007477
time_elpased: 1.769
batch start
#iterations: 95
currently lose_sum: 92.19548690319061
time_elpased: 1.769
batch start
#iterations: 96
currently lose_sum: 92.54217225313187
time_elpased: 1.763
batch start
#iterations: 97
currently lose_sum: 92.16373986005783
time_elpased: 1.764
batch start
#iterations: 98
currently lose_sum: 92.23071891069412
time_elpased: 1.773
batch start
#iterations: 99
currently lose_sum: 92.71485018730164
time_elpased: 1.796
start validation test
0.683144329897
0.641196013289
0.834208088916
0.725077150141
0.682879114168
57.651
batch start
#iterations: 100
currently lose_sum: 91.985644698143
time_elpased: 1.823
batch start
#iterations: 101
currently lose_sum: 91.96092039346695
time_elpased: 1.777
batch start
#iterations: 102
currently lose_sum: 92.11711466312408
time_elpased: 1.794
batch start
#iterations: 103
currently lose_sum: 91.76251167058945
time_elpased: 1.82
batch start
#iterations: 104
currently lose_sum: 92.05070322751999
time_elpased: 1.776
batch start
#iterations: 105
currently lose_sum: 92.25400841236115
time_elpased: 1.798
batch start
#iterations: 106
currently lose_sum: 91.61230146884918
time_elpased: 1.768
batch start
#iterations: 107
currently lose_sum: 92.1673054099083
time_elpased: 1.784
batch start
#iterations: 108
currently lose_sum: 91.55769562721252
time_elpased: 1.782
batch start
#iterations: 109
currently lose_sum: 91.57342517375946
time_elpased: 1.789
batch start
#iterations: 110
currently lose_sum: 91.80234467983246
time_elpased: 1.784
batch start
#iterations: 111
currently lose_sum: 91.97733688354492
time_elpased: 1.814
batch start
#iterations: 112
currently lose_sum: 91.3780534863472
time_elpased: 1.824
batch start
#iterations: 113
currently lose_sum: 91.35316199064255
time_elpased: 1.798
batch start
#iterations: 114
currently lose_sum: 94.81464606523514
time_elpased: 1.782
batch start
#iterations: 115
currently lose_sum: 91.61961257457733
time_elpased: 1.808
batch start
#iterations: 116
currently lose_sum: 91.19754499197006
time_elpased: 1.794
batch start
#iterations: 117
currently lose_sum: 91.44478160142899
time_elpased: 1.791
batch start
#iterations: 118
currently lose_sum: 91.02532356977463
time_elpased: 1.763
batch start
#iterations: 119
currently lose_sum: 91.27040433883667
time_elpased: 1.767
start validation test
0.625927835052
0.586086226204
0.861788617886
0.69768798167
0.625513745068
61.920
batch start
#iterations: 120
currently lose_sum: 91.54271703958511
time_elpased: 1.849
batch start
#iterations: 121
currently lose_sum: 91.20398908853531
time_elpased: 1.838
batch start
#iterations: 122
currently lose_sum: 90.67875158786774
time_elpased: 1.747
batch start
#iterations: 123
currently lose_sum: 91.56934958696365
time_elpased: 1.764
batch start
#iterations: 124
currently lose_sum: 91.26798576116562
time_elpased: 1.751
batch start
#iterations: 125
currently lose_sum: 90.4976903796196
time_elpased: 1.781
batch start
#iterations: 126
currently lose_sum: 91.71964108943939
time_elpased: 1.811
batch start
#iterations: 127
currently lose_sum: 91.45847970247269
time_elpased: 1.792
batch start
#iterations: 128
currently lose_sum: 91.03896176815033
time_elpased: 1.754
batch start
#iterations: 129
currently lose_sum: 91.88532388210297
time_elpased: 1.843
batch start
#iterations: 130
currently lose_sum: 90.62658935785294
time_elpased: 1.773
batch start
#iterations: 131
currently lose_sum: 91.52971374988556
time_elpased: 1.773
batch start
#iterations: 132
currently lose_sum: 91.01616895198822
time_elpased: 1.765
batch start
#iterations: 133
currently lose_sum: 90.86292040348053
time_elpased: 1.803
batch start
#iterations: 134
currently lose_sum: 90.48152548074722
time_elpased: 1.803
batch start
#iterations: 135
currently lose_sum: 93.57720947265625
time_elpased: 1.797
batch start
#iterations: 136
currently lose_sum: 90.88343930244446
time_elpased: 1.82
batch start
#iterations: 137
currently lose_sum: 91.08600682020187
time_elpased: 1.787
batch start
#iterations: 138
currently lose_sum: 90.54810953140259
time_elpased: 1.806
batch start
#iterations: 139
currently lose_sum: 90.67025727033615
time_elpased: 1.802
start validation test
0.65912371134
0.620403413499
0.82299063497
0.707479984076
0.658836017681
59.311
batch start
#iterations: 140
currently lose_sum: 90.91968184709549
time_elpased: 1.772
batch start
#iterations: 141
currently lose_sum: 90.24838995933533
time_elpased: 1.821
batch start
#iterations: 142
currently lose_sum: 90.37376266717911
time_elpased: 1.824
batch start
#iterations: 143
currently lose_sum: 90.60630244016647
time_elpased: 1.775
batch start
#iterations: 144
currently lose_sum: 89.97626769542694
time_elpased: 1.821
batch start
#iterations: 145
currently lose_sum: 90.90844792127609
time_elpased: 1.893
batch start
#iterations: 146
currently lose_sum: 90.92750191688538
time_elpased: 1.793
batch start
#iterations: 147
currently lose_sum: 89.67203164100647
time_elpased: 1.831
batch start
#iterations: 148
currently lose_sum: 90.29201865196228
time_elpased: 1.783
batch start
#iterations: 149
currently lose_sum: 91.10833430290222
time_elpased: 1.791
batch start
#iterations: 150
currently lose_sum: 90.89855629205704
time_elpased: 1.77
batch start
#iterations: 151
currently lose_sum: 90.05758053064346
time_elpased: 1.841
batch start
#iterations: 152
currently lose_sum: 90.53856128454208
time_elpased: 1.77
batch start
#iterations: 153
currently lose_sum: 89.72409611940384
time_elpased: 1.841
batch start
#iterations: 154
currently lose_sum: 89.00137430429459
time_elpased: 1.828
batch start
#iterations: 155
currently lose_sum: 89.78613209724426
time_elpased: 1.825
batch start
#iterations: 156
currently lose_sum: 90.43428146839142
time_elpased: 1.847
batch start
#iterations: 157
currently lose_sum: 89.91855001449585
time_elpased: 1.806
batch start
#iterations: 158
currently lose_sum: 89.78693985939026
time_elpased: 1.784
batch start
#iterations: 159
currently lose_sum: 90.04313218593597
time_elpased: 1.849
start validation test
0.692371134021
0.655935446302
0.811464443758
0.725457723802
0.692162047346
56.636
batch start
#iterations: 160
currently lose_sum: 89.46688574552536
time_elpased: 1.783
batch start
#iterations: 161
currently lose_sum: 90.46128565073013
time_elpased: 1.786
batch start
#iterations: 162
currently lose_sum: 89.67048925161362
time_elpased: 1.824
batch start
#iterations: 163
currently lose_sum: 90.19517368078232
time_elpased: 1.802
batch start
#iterations: 164
currently lose_sum: 90.23322224617004
time_elpased: 1.808
batch start
#iterations: 165
currently lose_sum: 90.14250880479813
time_elpased: 1.805
batch start
#iterations: 166
currently lose_sum: 90.86534887552261
time_elpased: 1.776
batch start
#iterations: 167
currently lose_sum: 89.31239229440689
time_elpased: 1.794
batch start
#iterations: 168
currently lose_sum: 90.61747968196869
time_elpased: 1.824
batch start
#iterations: 169
currently lose_sum: 91.23964494466782
time_elpased: 1.82
batch start
#iterations: 170
currently lose_sum: 90.30903142690659
time_elpased: 1.76
batch start
#iterations: 171
currently lose_sum: 90.19484531879425
time_elpased: 1.797
batch start
#iterations: 172
currently lose_sum: 89.71537882089615
time_elpased: 1.802
batch start
#iterations: 173
currently lose_sum: 89.920450091362
time_elpased: 1.766
batch start
#iterations: 174
currently lose_sum: 89.82150274515152
time_elpased: 1.832
batch start
#iterations: 175
currently lose_sum: 89.79731667041779
time_elpased: 1.791
batch start
#iterations: 176
currently lose_sum: 89.52266639471054
time_elpased: 1.841
batch start
#iterations: 177
currently lose_sum: 89.62097817659378
time_elpased: 1.776
batch start
#iterations: 178
currently lose_sum: 90.04907095432281
time_elpased: 1.784
batch start
#iterations: 179
currently lose_sum: 88.46229642629623
time_elpased: 1.816
start validation test
0.692577319588
0.679414858017
0.731295667387
0.704401268834
0.692509343556
56.867
batch start
#iterations: 180
currently lose_sum: 90.13648015260696
time_elpased: 1.729
batch start
#iterations: 181
currently lose_sum: 89.48050808906555
time_elpased: 1.775
batch start
#iterations: 182
currently lose_sum: 88.98385179042816
time_elpased: 1.827
batch start
#iterations: 183
currently lose_sum: 88.3554328083992
time_elpased: 1.795
batch start
#iterations: 184
currently lose_sum: 89.83751785755157
time_elpased: 1.755
batch start
#iterations: 185
currently lose_sum: 89.17139804363251
time_elpased: 1.808
batch start
#iterations: 186
currently lose_sum: 89.90526294708252
time_elpased: 1.808
batch start
#iterations: 187
currently lose_sum: 89.66101562976837
time_elpased: 1.815
batch start
#iterations: 188
currently lose_sum: 89.0746460556984
time_elpased: 1.741
batch start
#iterations: 189
currently lose_sum: 89.86475855112076
time_elpased: 1.809
batch start
#iterations: 190
currently lose_sum: 89.66184240579605
time_elpased: 1.771
batch start
#iterations: 191
currently lose_sum: 88.85855036973953
time_elpased: 1.797
batch start
#iterations: 192
currently lose_sum: 89.37350523471832
time_elpased: 1.831
batch start
#iterations: 193
currently lose_sum: 88.93483394384384
time_elpased: 1.791
batch start
#iterations: 194
currently lose_sum: 89.21939432621002
time_elpased: 1.805
batch start
#iterations: 195
currently lose_sum: 89.14161813259125
time_elpased: 1.754
batch start
#iterations: 196
currently lose_sum: 90.09446758031845
time_elpased: 1.798
batch start
#iterations: 197
currently lose_sum: 89.19771432876587
time_elpased: 1.81
batch start
#iterations: 198
currently lose_sum: 88.36472225189209
time_elpased: 1.823
batch start
#iterations: 199
currently lose_sum: 88.11124348640442
time_elpased: 1.84
start validation test
0.639484536082
0.601916311101
0.827518781517
0.696914543248
0.639154412962
60.474
batch start
#iterations: 200
currently lose_sum: 88.96367704868317
time_elpased: 1.762
batch start
#iterations: 201
currently lose_sum: 89.05039244890213
time_elpased: 1.773
batch start
#iterations: 202
currently lose_sum: 88.76405894756317
time_elpased: 1.801
batch start
#iterations: 203
currently lose_sum: 88.93012148141861
time_elpased: 1.781
batch start
#iterations: 204
currently lose_sum: 88.69317752122879
time_elpased: 1.799
batch start
#iterations: 205
currently lose_sum: 88.39697265625
time_elpased: 1.859
batch start
#iterations: 206
currently lose_sum: 90.81492292881012
time_elpased: 1.776
batch start
#iterations: 207
currently lose_sum: 88.47128611803055
time_elpased: 1.799
batch start
#iterations: 208
currently lose_sum: 88.97162252664566
time_elpased: 1.814
batch start
#iterations: 209
currently lose_sum: 89.79973375797272
time_elpased: 1.812
batch start
#iterations: 210
currently lose_sum: 88.9516184926033
time_elpased: 1.795
batch start
#iterations: 211
currently lose_sum: 89.06608468294144
time_elpased: 1.796
batch start
#iterations: 212
currently lose_sum: 88.17579483985901
time_elpased: 1.823
batch start
#iterations: 213
currently lose_sum: 88.1632781624794
time_elpased: 1.839
batch start
#iterations: 214
currently lose_sum: 89.4894050359726
time_elpased: 1.797
batch start
#iterations: 215
currently lose_sum: 87.96656185388565
time_elpased: 1.749
batch start
#iterations: 216
currently lose_sum: 88.5876681804657
time_elpased: 1.792
batch start
#iterations: 217
currently lose_sum: 88.56035536527634
time_elpased: 1.815
batch start
#iterations: 218
currently lose_sum: 88.24760377407074
time_elpased: 1.86
batch start
#iterations: 219
currently lose_sum: 89.11057490110397
time_elpased: 1.757
start validation test
0.690773195876
0.689887640449
0.695070495009
0.69246936997
0.690765651305
56.436
batch start
#iterations: 220
currently lose_sum: 88.76832330226898
time_elpased: 1.769
batch start
#iterations: 221
currently lose_sum: 88.29825013875961
time_elpased: 1.806
batch start
#iterations: 222
currently lose_sum: 88.28615301847458
time_elpased: 1.766
batch start
#iterations: 223
currently lose_sum: 88.06553953886032
time_elpased: 1.775
batch start
#iterations: 224
currently lose_sum: 87.9929204583168
time_elpased: 1.783
batch start
#iterations: 225
currently lose_sum: 89.07595127820969
time_elpased: 1.795
batch start
#iterations: 226
currently lose_sum: 87.63563650846481
time_elpased: 1.781
batch start
#iterations: 227
currently lose_sum: 89.05847293138504
time_elpased: 1.786
batch start
#iterations: 228
currently lose_sum: 89.43310815095901
time_elpased: 1.795
batch start
#iterations: 229
currently lose_sum: 88.41177660226822
time_elpased: 1.8
batch start
#iterations: 230
currently lose_sum: 88.19897300004959
time_elpased: 1.807
batch start
#iterations: 231
currently lose_sum: 87.90366971492767
time_elpased: 1.803
batch start
#iterations: 232
currently lose_sum: 88.21055334806442
time_elpased: 1.799
batch start
#iterations: 233
currently lose_sum: 88.78954309225082
time_elpased: 1.803
batch start
#iterations: 234
currently lose_sum: 88.89238387346268
time_elpased: 1.792
batch start
#iterations: 235
currently lose_sum: 88.2778834104538
time_elpased: 1.763
batch start
#iterations: 236
currently lose_sum: 87.98446649312973
time_elpased: 1.825
batch start
#iterations: 237
currently lose_sum: 88.27368462085724
time_elpased: 1.828
batch start
#iterations: 238
currently lose_sum: 88.83465957641602
time_elpased: 1.806
batch start
#iterations: 239
currently lose_sum: 88.35536295175552
time_elpased: 1.791
start validation test
0.693659793814
0.667733333333
0.773078110528
0.716554585778
0.69352036271
56.116
batch start
#iterations: 240
currently lose_sum: 87.35739123821259
time_elpased: 1.817
batch start
#iterations: 241
currently lose_sum: 88.13513815402985
time_elpased: 1.859
batch start
#iterations: 242
currently lose_sum: 87.63802540302277
time_elpased: 1.78
batch start
#iterations: 243
currently lose_sum: 87.54348856210709
time_elpased: 1.815
batch start
#iterations: 244
currently lose_sum: 87.72934544086456
time_elpased: 1.864
batch start
#iterations: 245
currently lose_sum: 87.85716259479523
time_elpased: 1.815
batch start
#iterations: 246
currently lose_sum: 87.35430890321732
time_elpased: 1.802
batch start
#iterations: 247
currently lose_sum: 86.7280176281929
time_elpased: 1.754
batch start
#iterations: 248
currently lose_sum: 87.95172852277756
time_elpased: 1.784
batch start
#iterations: 249
currently lose_sum: 86.52118676900864
time_elpased: 1.806
batch start
#iterations: 250
currently lose_sum: 87.09654325246811
time_elpased: 1.78
batch start
#iterations: 251
currently lose_sum: 87.48162692785263
time_elpased: 1.807
batch start
#iterations: 252
currently lose_sum: 87.93904042243958
time_elpased: 1.795
batch start
#iterations: 253
currently lose_sum: 87.88337260484695
time_elpased: 1.798
batch start
#iterations: 254
currently lose_sum: 88.67105156183243
time_elpased: 1.792
batch start
#iterations: 255
currently lose_sum: 87.58759796619415
time_elpased: 1.815
batch start
#iterations: 256
currently lose_sum: 86.60920840501785
time_elpased: 1.776
batch start
#iterations: 257
currently lose_sum: 86.9446616768837
time_elpased: 1.816
batch start
#iterations: 258
currently lose_sum: 88.83320993185043
time_elpased: 1.796
batch start
#iterations: 259
currently lose_sum: 87.4300565123558
time_elpased: 1.856
start validation test
0.690979381443
0.671552359882
0.749716990841
0.708485290542
0.69087625851
56.154
batch start
#iterations: 260
currently lose_sum: 86.94696801900864
time_elpased: 1.771
batch start
#iterations: 261
currently lose_sum: 87.14222663640976
time_elpased: 1.78
batch start
#iterations: 262
currently lose_sum: 87.16631120443344
time_elpased: 1.821
batch start
#iterations: 263
currently lose_sum: 87.57905369997025
time_elpased: 1.741
batch start
#iterations: 264
currently lose_sum: 87.35451149940491
time_elpased: 1.84
batch start
#iterations: 265
currently lose_sum: 87.19109082221985
time_elpased: 1.865
batch start
#iterations: 266
currently lose_sum: 87.2501711845398
time_elpased: 1.747
batch start
#iterations: 267
currently lose_sum: 86.82520014047623
time_elpased: 1.804
batch start
#iterations: 268
currently lose_sum: 86.46392923593521
time_elpased: 1.78
batch start
#iterations: 269
currently lose_sum: 87.14298677444458
time_elpased: 1.821
batch start
#iterations: 270
currently lose_sum: 88.25307559967041
time_elpased: 1.775
batch start
#iterations: 271
currently lose_sum: 86.59854155778885
time_elpased: 1.826
batch start
#iterations: 272
currently lose_sum: 87.21995079517365
time_elpased: 1.82
batch start
#iterations: 273
currently lose_sum: 87.1683161854744
time_elpased: 1.771
batch start
#iterations: 274
currently lose_sum: 87.0281525850296
time_elpased: 1.845
batch start
#iterations: 275
currently lose_sum: 86.2241558432579
time_elpased: 1.782
batch start
#iterations: 276
currently lose_sum: 88.289460003376
time_elpased: 1.809
batch start
#iterations: 277
currently lose_sum: 86.70517289638519
time_elpased: 1.815
batch start
#iterations: 278
currently lose_sum: 86.65222293138504
time_elpased: 1.806
batch start
#iterations: 279
currently lose_sum: 87.05638808012009
time_elpased: 1.777
start validation test
0.693402061856
0.678270740706
0.737882062365
0.706821766562
0.693323970354
55.887
batch start
#iterations: 280
currently lose_sum: 86.68722474575043
time_elpased: 1.785
batch start
#iterations: 281
currently lose_sum: 86.46274101734161
time_elpased: 1.806
batch start
#iterations: 282
currently lose_sum: 87.47117793560028
time_elpased: 1.794
batch start
#iterations: 283
currently lose_sum: 86.83215469121933
time_elpased: 1.808
batch start
#iterations: 284
currently lose_sum: 86.85430812835693
time_elpased: 1.768
batch start
#iterations: 285
currently lose_sum: 86.5290657877922
time_elpased: 1.78
batch start
#iterations: 286
currently lose_sum: 86.09859192371368
time_elpased: 1.803
batch start
#iterations: 287
currently lose_sum: 86.23603785037994
time_elpased: 1.832
batch start
#iterations: 288
currently lose_sum: 87.28783112764359
time_elpased: 1.801
batch start
#iterations: 289
currently lose_sum: 85.44702178239822
time_elpased: 1.779
batch start
#iterations: 290
currently lose_sum: 86.3591661453247
time_elpased: 1.771
batch start
#iterations: 291
currently lose_sum: 87.03279709815979
time_elpased: 1.783
batch start
#iterations: 292
currently lose_sum: 85.98063415288925
time_elpased: 1.859
batch start
#iterations: 293
currently lose_sum: 86.04117554426193
time_elpased: 1.79
batch start
#iterations: 294
currently lose_sum: 85.90049999952316
time_elpased: 1.788
batch start
#iterations: 295
currently lose_sum: 87.31709498167038
time_elpased: 1.821
batch start
#iterations: 296
currently lose_sum: 86.69005048274994
time_elpased: 1.788
batch start
#iterations: 297
currently lose_sum: 87.28544837236404
time_elpased: 1.816
batch start
#iterations: 298
currently lose_sum: 86.76895660161972
time_elpased: 1.755
batch start
#iterations: 299
currently lose_sum: 86.41608613729477
time_elpased: 1.832
start validation test
0.685618556701
0.670403165034
0.732427704024
0.700044263021
0.685536376023
56.702
batch start
#iterations: 300
currently lose_sum: 86.07127124071121
time_elpased: 1.76
batch start
#iterations: 301
currently lose_sum: 85.8254001736641
time_elpased: 1.768
batch start
#iterations: 302
currently lose_sum: 85.91384536027908
time_elpased: 1.859
batch start
#iterations: 303
currently lose_sum: 87.05931800603867
time_elpased: 1.791
batch start
#iterations: 304
currently lose_sum: 85.93695342540741
time_elpased: 1.806
batch start
#iterations: 305
currently lose_sum: 85.21282863616943
time_elpased: 1.784
batch start
#iterations: 306
currently lose_sum: 85.1174629330635
time_elpased: 1.81
batch start
#iterations: 307
currently lose_sum: 85.74691641330719
time_elpased: 1.822
batch start
#iterations: 308
currently lose_sum: 85.78348928689957
time_elpased: 1.807
batch start
#iterations: 309
currently lose_sum: 86.29838413000107
time_elpased: 1.797
batch start
#iterations: 310
currently lose_sum: 85.4370796084404
time_elpased: 1.793
batch start
#iterations: 311
currently lose_sum: 85.91640406847
time_elpased: 1.816
batch start
#iterations: 312
currently lose_sum: 86.35752129554749
time_elpased: 1.765
batch start
#iterations: 313
currently lose_sum: 86.2110435962677
time_elpased: 1.841
batch start
#iterations: 314
currently lose_sum: 86.01228994131088
time_elpased: 1.802
batch start
#iterations: 315
currently lose_sum: 85.47697234153748
time_elpased: 1.764
batch start
#iterations: 316
currently lose_sum: 86.50834900140762
time_elpased: 1.756
batch start
#iterations: 317
currently lose_sum: 86.67116743326187
time_elpased: 1.788
batch start
#iterations: 318
currently lose_sum: 85.5976687669754
time_elpased: 1.792
batch start
#iterations: 319
currently lose_sum: 85.91777044534683
time_elpased: 1.788
start validation test
0.687731958763
0.696066873861
0.668416177833
0.681961360773
0.687765870596
56.900
batch start
#iterations: 320
currently lose_sum: 85.478342294693
time_elpased: 1.769
batch start
#iterations: 321
currently lose_sum: 86.18441724777222
time_elpased: 1.785
batch start
#iterations: 322
currently lose_sum: 85.31268608570099
time_elpased: 1.795
batch start
#iterations: 323
currently lose_sum: 85.87416011095047
time_elpased: 1.808
batch start
#iterations: 324
currently lose_sum: 84.90956121683121
time_elpased: 1.777
batch start
#iterations: 325
currently lose_sum: 87.11207538843155
time_elpased: 1.77
batch start
#iterations: 326
currently lose_sum: 85.804343521595
time_elpased: 1.814
batch start
#iterations: 327
currently lose_sum: 85.59649896621704
time_elpased: 1.784
batch start
#iterations: 328
currently lose_sum: 85.46714234352112
time_elpased: 1.799
batch start
#iterations: 329
currently lose_sum: 85.36389619112015
time_elpased: 1.759
batch start
#iterations: 330
currently lose_sum: 86.14003211259842
time_elpased: 1.783
batch start
#iterations: 331
currently lose_sum: 86.98404574394226
time_elpased: 1.819
batch start
#iterations: 332
currently lose_sum: 85.53157246112823
time_elpased: 1.771
batch start
#iterations: 333
currently lose_sum: 84.81781753897667
time_elpased: 1.866
batch start
#iterations: 334
currently lose_sum: 84.5768233537674
time_elpased: 1.822
batch start
#iterations: 335
currently lose_sum: 84.63899946212769
time_elpased: 1.784
batch start
#iterations: 336
currently lose_sum: 84.7612407207489
time_elpased: 1.84
batch start
#iterations: 337
currently lose_sum: 84.98784512281418
time_elpased: 1.749
batch start
#iterations: 338
currently lose_sum: 85.14469730854034
time_elpased: 1.789
batch start
#iterations: 339
currently lose_sum: 85.30838352441788
time_elpased: 1.824
start validation test
0.696701030928
0.681400851869
0.740866522589
0.709890543339
0.696623491595
56.699
batch start
#iterations: 340
currently lose_sum: 85.10275435447693
time_elpased: 1.81
batch start
#iterations: 341
currently lose_sum: 84.81226789951324
time_elpased: 1.759
batch start
#iterations: 342
currently lose_sum: 85.4230917096138
time_elpased: 1.8
batch start
#iterations: 343
currently lose_sum: 84.22288626432419
time_elpased: 1.831
batch start
#iterations: 344
currently lose_sum: 84.76301646232605
time_elpased: 1.81
batch start
#iterations: 345
currently lose_sum: 85.65467309951782
time_elpased: 1.744
batch start
#iterations: 346
currently lose_sum: 85.4988699555397
time_elpased: 1.826
batch start
#iterations: 347
currently lose_sum: 84.28239846229553
time_elpased: 1.845
batch start
#iterations: 348
currently lose_sum: 84.87470209598541
time_elpased: 1.789
batch start
#iterations: 349
currently lose_sum: 86.11200535297394
time_elpased: 1.83
batch start
#iterations: 350
currently lose_sum: 84.58628699183464
time_elpased: 1.826
batch start
#iterations: 351
currently lose_sum: 84.41324317455292
time_elpased: 1.761
batch start
#iterations: 352
currently lose_sum: 85.29914975166321
time_elpased: 1.76
batch start
#iterations: 353
currently lose_sum: 84.68009984493256
time_elpased: 1.803
batch start
#iterations: 354
currently lose_sum: 84.82544553279877
time_elpased: 1.793
batch start
#iterations: 355
currently lose_sum: 83.8862914443016
time_elpased: 1.802
batch start
#iterations: 356
currently lose_sum: 85.16550958156586
time_elpased: 1.751
batch start
#iterations: 357
currently lose_sum: 84.71931844949722
time_elpased: 1.751
batch start
#iterations: 358
currently lose_sum: 84.36344289779663
time_elpased: 1.788
batch start
#iterations: 359
currently lose_sum: 84.80331319570541
time_elpased: 1.798
start validation test
0.686443298969
0.678487229862
0.710816095503
0.694275518922
0.686400508766
57.138
batch start
#iterations: 360
currently lose_sum: 84.2352842092514
time_elpased: 1.791
batch start
#iterations: 361
currently lose_sum: 84.67841267585754
time_elpased: 1.79
batch start
#iterations: 362
currently lose_sum: 83.88122016191483
time_elpased: 1.781
batch start
#iterations: 363
currently lose_sum: 83.37822097539902
time_elpased: 1.876
batch start
#iterations: 364
currently lose_sum: 83.9936209321022
time_elpased: 1.81
batch start
#iterations: 365
currently lose_sum: 85.84149694442749
time_elpased: 1.743
batch start
#iterations: 366
currently lose_sum: 84.83306044340134
time_elpased: 1.8
batch start
#iterations: 367
currently lose_sum: 83.85661995410919
time_elpased: 1.76
batch start
#iterations: 368
currently lose_sum: 84.3011040687561
time_elpased: 1.891
batch start
#iterations: 369
currently lose_sum: 84.23435509204865
time_elpased: 1.787
batch start
#iterations: 370
currently lose_sum: 84.26794081926346
time_elpased: 1.797
batch start
#iterations: 371
currently lose_sum: 84.72387903928757
time_elpased: 1.795
batch start
#iterations: 372
currently lose_sum: 83.86805999279022
time_elpased: 1.774
batch start
#iterations: 373
currently lose_sum: 85.1053341627121
time_elpased: 1.773
batch start
#iterations: 374
currently lose_sum: 84.87400859594345
time_elpased: 1.788
batch start
#iterations: 375
currently lose_sum: 83.51521652936935
time_elpased: 1.744
batch start
#iterations: 376
currently lose_sum: 84.27602237462997
time_elpased: 1.791
batch start
#iterations: 377
currently lose_sum: 83.03611370921135
time_elpased: 1.788
batch start
#iterations: 378
currently lose_sum: 84.18056792020798
time_elpased: 1.837
batch start
#iterations: 379
currently lose_sum: 83.79980105161667
time_elpased: 1.766
start validation test
0.685206185567
0.681772406848
0.696717093753
0.689163740011
0.685185976392
57.629
batch start
#iterations: 380
currently lose_sum: 83.76113200187683
time_elpased: 1.803
batch start
#iterations: 381
currently lose_sum: 84.2308823466301
time_elpased: 1.787
batch start
#iterations: 382
currently lose_sum: 84.44424468278885
time_elpased: 1.791
batch start
#iterations: 383
currently lose_sum: 84.33283931016922
time_elpased: 1.83
batch start
#iterations: 384
currently lose_sum: 83.60310047864914
time_elpased: 1.834
batch start
#iterations: 385
currently lose_sum: 84.35879546403885
time_elpased: 1.773
batch start
#iterations: 386
currently lose_sum: 82.90168398618698
time_elpased: 1.778
batch start
#iterations: 387
currently lose_sum: 83.18856340646744
time_elpased: 1.798
batch start
#iterations: 388
currently lose_sum: 83.39462465047836
time_elpased: 1.803
batch start
#iterations: 389
currently lose_sum: 84.03230741620064
time_elpased: 1.785
batch start
#iterations: 390
currently lose_sum: 83.5439241528511
time_elpased: 1.825
batch start
#iterations: 391
currently lose_sum: 83.05751568078995
time_elpased: 1.797
batch start
#iterations: 392
currently lose_sum: 83.821544110775
time_elpased: 1.86
batch start
#iterations: 393
currently lose_sum: 84.32011342048645
time_elpased: 1.782
batch start
#iterations: 394
currently lose_sum: 83.77920725941658
time_elpased: 1.795
batch start
#iterations: 395
currently lose_sum: 84.83422636985779
time_elpased: 1.751
batch start
#iterations: 396
currently lose_sum: 83.78637960553169
time_elpased: 1.784
batch start
#iterations: 397
currently lose_sum: 84.15548086166382
time_elpased: 1.779
batch start
#iterations: 398
currently lose_sum: 83.04741823673248
time_elpased: 1.805
batch start
#iterations: 399
currently lose_sum: 84.0858781337738
time_elpased: 1.772
start validation test
0.685618556701
0.663769690386
0.754553874653
0.706256321341
0.685497530118
57.025
acc: 0.689
pre: 0.674
rec: 0.734
F1: 0.702
auc: 0.748
