start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.43360322713852
time_elpased: 2.277
batch start
#iterations: 1
currently lose_sum: 100.19862574338913
time_elpased: 2.227
batch start
#iterations: 2
currently lose_sum: 99.96894943714142
time_elpased: 2.202
batch start
#iterations: 3
currently lose_sum: 99.56616926193237
time_elpased: 2.248
batch start
#iterations: 4
currently lose_sum: 99.55325889587402
time_elpased: 2.268
batch start
#iterations: 5
currently lose_sum: 99.2815677523613
time_elpased: 2.23
batch start
#iterations: 6
currently lose_sum: 98.80216842889786
time_elpased: 2.264
batch start
#iterations: 7
currently lose_sum: 98.8059012889862
time_elpased: 2.252
batch start
#iterations: 8
currently lose_sum: 98.29965108633041
time_elpased: 2.25
batch start
#iterations: 9
currently lose_sum: 98.44618582725525
time_elpased: 2.297
batch start
#iterations: 10
currently lose_sum: 98.25271075963974
time_elpased: 2.282
batch start
#iterations: 11
currently lose_sum: 98.06960999965668
time_elpased: 2.221
batch start
#iterations: 12
currently lose_sum: 97.60602003335953
time_elpased: 2.256
batch start
#iterations: 13
currently lose_sum: 97.97166705131531
time_elpased: 2.217
batch start
#iterations: 14
currently lose_sum: 97.5862842798233
time_elpased: 2.2
batch start
#iterations: 15
currently lose_sum: 97.23698878288269
time_elpased: 2.227
batch start
#iterations: 16
currently lose_sum: 97.29940950870514
time_elpased: 2.221
batch start
#iterations: 17
currently lose_sum: 96.82407766580582
time_elpased: 2.261
batch start
#iterations: 18
currently lose_sum: 97.42109507322311
time_elpased: 2.267
batch start
#iterations: 19
currently lose_sum: 96.74879455566406
time_elpased: 2.271
start validation test
0.595103092784
0.568707081149
0.792713050638
0.662281267466
0.594776599669
64.306
batch start
#iterations: 20
currently lose_sum: 96.84055578708649
time_elpased: 2.235
batch start
#iterations: 21
currently lose_sum: 96.50574380159378
time_elpased: 2.207
batch start
#iterations: 22
currently lose_sum: 96.58663684129715
time_elpased: 2.166
batch start
#iterations: 23
currently lose_sum: 96.55076003074646
time_elpased: 2.288
batch start
#iterations: 24
currently lose_sum: 96.36647909879684
time_elpased: 2.225
batch start
#iterations: 25
currently lose_sum: 95.8642526268959
time_elpased: 2.235
batch start
#iterations: 26
currently lose_sum: 95.84079170227051
time_elpased: 2.235
batch start
#iterations: 27
currently lose_sum: 95.65549945831299
time_elpased: 2.242
batch start
#iterations: 28
currently lose_sum: 95.542216360569
time_elpased: 2.216
batch start
#iterations: 29
currently lose_sum: 95.64417427778244
time_elpased: 2.241
batch start
#iterations: 30
currently lose_sum: 95.39760738611221
time_elpased: 2.216
batch start
#iterations: 31
currently lose_sum: 95.5438043475151
time_elpased: 2.207
batch start
#iterations: 32
currently lose_sum: 95.31234127283096
time_elpased: 2.236
batch start
#iterations: 33
currently lose_sum: 95.38690859079361
time_elpased: 2.217
batch start
#iterations: 34
currently lose_sum: 94.56865793466568
time_elpased: 2.225
batch start
#iterations: 35
currently lose_sum: 95.01979547739029
time_elpased: 2.228
batch start
#iterations: 36
currently lose_sum: 94.40521478652954
time_elpased: 2.184
batch start
#iterations: 37
currently lose_sum: 95.10169297456741
time_elpased: 2.19
batch start
#iterations: 38
currently lose_sum: 94.31001257896423
time_elpased: 2.293
batch start
#iterations: 39
currently lose_sum: 94.71754848957062
time_elpased: 2.251
start validation test
0.604278350515
0.602555074942
0.616508851379
0.609452103576
0.604258143162
63.294
batch start
#iterations: 40
currently lose_sum: 94.15447968244553
time_elpased: 2.217
batch start
#iterations: 41
currently lose_sum: 94.40592247247696
time_elpased: 2.267
batch start
#iterations: 42
currently lose_sum: 93.98475742340088
time_elpased: 2.306
batch start
#iterations: 43
currently lose_sum: 93.72919249534607
time_elpased: 2.213
batch start
#iterations: 44
currently lose_sum: 94.16701579093933
time_elpased: 2.208
batch start
#iterations: 45
currently lose_sum: 93.78109431266785
time_elpased: 2.183
batch start
#iterations: 46
currently lose_sum: 94.0196105837822
time_elpased: 2.232
batch start
#iterations: 47
currently lose_sum: 93.52451032400131
time_elpased: 2.264
batch start
#iterations: 48
currently lose_sum: 93.90768337249756
time_elpased: 2.217
batch start
#iterations: 49
currently lose_sum: 93.35596179962158
time_elpased: 2.235
batch start
#iterations: 50
currently lose_sum: 93.7370331287384
time_elpased: 2.181
batch start
#iterations: 51
currently lose_sum: 93.38645368814468
time_elpased: 2.206
batch start
#iterations: 52
currently lose_sum: 93.55703729391098
time_elpased: 2.195
batch start
#iterations: 53
currently lose_sum: 93.21015065908432
time_elpased: 2.243
batch start
#iterations: 54
currently lose_sum: 93.10137313604355
time_elpased: 2.212
batch start
#iterations: 55
currently lose_sum: 92.97814071178436
time_elpased: 2.202
batch start
#iterations: 56
currently lose_sum: 93.3641254901886
time_elpased: 2.208
batch start
#iterations: 57
currently lose_sum: 93.04895180463791
time_elpased: 2.226
batch start
#iterations: 58
currently lose_sum: 92.75183176994324
time_elpased: 2.244
batch start
#iterations: 59
currently lose_sum: 92.4502210021019
time_elpased: 2.223
start validation test
0.628659793814
0.606027351005
0.738884314533
0.665893701883
0.628477679778
62.441
batch start
#iterations: 60
currently lose_sum: 92.58826631307602
time_elpased: 2.174
batch start
#iterations: 61
currently lose_sum: 92.81697970628738
time_elpased: 2.204
batch start
#iterations: 62
currently lose_sum: 92.58532053232193
time_elpased: 2.266
batch start
#iterations: 63
currently lose_sum: 92.729168176651
time_elpased: 2.244
batch start
#iterations: 64
currently lose_sum: 92.47301787137985
time_elpased: 2.212
batch start
#iterations: 65
currently lose_sum: 92.0098317861557
time_elpased: 2.198
batch start
#iterations: 66
currently lose_sum: 92.30775624513626
time_elpased: 2.236
batch start
#iterations: 67
currently lose_sum: 92.18791264295578
time_elpased: 2.283
batch start
#iterations: 68
currently lose_sum: 91.35905051231384
time_elpased: 2.18
batch start
#iterations: 69
currently lose_sum: 91.11531096696854
time_elpased: 2.244
batch start
#iterations: 70
currently lose_sum: 91.44130229949951
time_elpased: 2.207
batch start
#iterations: 71
currently lose_sum: 91.17297339439392
time_elpased: 2.188
batch start
#iterations: 72
currently lose_sum: 91.09561115503311
time_elpased: 2.215
batch start
#iterations: 73
currently lose_sum: 90.5932548046112
time_elpased: 2.273
batch start
#iterations: 74
currently lose_sum: 91.05527430772781
time_elpased: 2.219
batch start
#iterations: 75
currently lose_sum: 91.10441797971725
time_elpased: 2.203
batch start
#iterations: 76
currently lose_sum: 90.91911906003952
time_elpased: 2.23
batch start
#iterations: 77
currently lose_sum: 90.88851314783096
time_elpased: 2.199
batch start
#iterations: 78
currently lose_sum: 91.60150504112244
time_elpased: 2.248
batch start
#iterations: 79
currently lose_sum: 90.3561018705368
time_elpased: 2.207
start validation test
0.625154639175
0.654448938322
0.532935364347
0.58747447243
0.625307004768
64.755
batch start
#iterations: 80
currently lose_sum: 90.57767987251282
time_elpased: 2.191
batch start
#iterations: 81
currently lose_sum: 90.38010710477829
time_elpased: 2.216
batch start
#iterations: 82
currently lose_sum: 90.15383744239807
time_elpased: 2.217
batch start
#iterations: 83
currently lose_sum: 90.4474321603775
time_elpased: 2.204
batch start
#iterations: 84
currently lose_sum: 89.96536076068878
time_elpased: 2.215
batch start
#iterations: 85
currently lose_sum: 89.70206725597382
time_elpased: 2.264
batch start
#iterations: 86
currently lose_sum: 89.69993841648102
time_elpased: 2.208
batch start
#iterations: 87
currently lose_sum: 89.51663428544998
time_elpased: 2.257
batch start
#iterations: 88
currently lose_sum: 90.31672614812851
time_elpased: 2.192
batch start
#iterations: 89
currently lose_sum: 89.91478872299194
time_elpased: 2.217
batch start
#iterations: 90
currently lose_sum: 89.64522737264633
time_elpased: 2.23
batch start
#iterations: 91
currently lose_sum: 89.24648213386536
time_elpased: 2.261
batch start
#iterations: 92
currently lose_sum: 89.22914499044418
time_elpased: 2.253
batch start
#iterations: 93
currently lose_sum: 89.05075198411942
time_elpased: 2.192
batch start
#iterations: 94
currently lose_sum: 88.63880741596222
time_elpased: 2.156
batch start
#iterations: 95
currently lose_sum: 89.57781541347504
time_elpased: 2.232
batch start
#iterations: 96
currently lose_sum: 88.5067566037178
time_elpased: 2.243
batch start
#iterations: 97
currently lose_sum: 89.31846076250076
time_elpased: 2.251
batch start
#iterations: 98
currently lose_sum: 88.76175618171692
time_elpased: 2.174
batch start
#iterations: 99
currently lose_sum: 88.6210110783577
time_elpased: 2.239
start validation test
0.613195876289
0.620400609623
0.586558254426
0.603004973019
0.613239887229
65.610
batch start
#iterations: 100
currently lose_sum: 88.73711639642715
time_elpased: 2.222
batch start
#iterations: 101
currently lose_sum: 88.89261442422867
time_elpased: 2.26
batch start
#iterations: 102
currently lose_sum: 88.35357367992401
time_elpased: 2.191
batch start
#iterations: 103
currently lose_sum: 87.75987094640732
time_elpased: 2.303
batch start
#iterations: 104
currently lose_sum: 87.3430967926979
time_elpased: 2.275
batch start
#iterations: 105
currently lose_sum: 87.71104550361633
time_elpased: 2.269
batch start
#iterations: 106
currently lose_sum: 87.60791218280792
time_elpased: 2.226
batch start
#iterations: 107
currently lose_sum: 88.05234497785568
time_elpased: 2.211
batch start
#iterations: 108
currently lose_sum: 87.65831261873245
time_elpased: 2.232
batch start
#iterations: 109
currently lose_sum: 88.14088135957718
time_elpased: 2.248
batch start
#iterations: 110
currently lose_sum: 87.22495621442795
time_elpased: 2.238
batch start
#iterations: 111
currently lose_sum: 86.52315211296082
time_elpased: 2.283
batch start
#iterations: 112
currently lose_sum: 87.29598897695541
time_elpased: 2.259
batch start
#iterations: 113
currently lose_sum: 86.78325295448303
time_elpased: 2.214
batch start
#iterations: 114
currently lose_sum: 86.8797578215599
time_elpased: 2.268
batch start
#iterations: 115
currently lose_sum: 86.96610844135284
time_elpased: 2.228
batch start
#iterations: 116
currently lose_sum: 87.403151512146
time_elpased: 2.279
batch start
#iterations: 117
currently lose_sum: 86.86292487382889
time_elpased: 2.233
batch start
#iterations: 118
currently lose_sum: 86.3181968331337
time_elpased: 2.231
batch start
#iterations: 119
currently lose_sum: 86.61250221729279
time_elpased: 2.229
start validation test
0.592783505155
0.625
0.46727048168
0.534746760895
0.592990879006
69.142
batch start
#iterations: 120
currently lose_sum: 86.02180081605911
time_elpased: 2.272
batch start
#iterations: 121
currently lose_sum: 86.02186048030853
time_elpased: 2.206
batch start
#iterations: 122
currently lose_sum: 86.28815019130707
time_elpased: 2.239
batch start
#iterations: 123
currently lose_sum: 86.14178937673569
time_elpased: 2.229
batch start
#iterations: 124
currently lose_sum: 85.90068984031677
time_elpased: 2.211
batch start
#iterations: 125
currently lose_sum: 85.75232857465744
time_elpased: 2.298
batch start
#iterations: 126
currently lose_sum: 84.98752164840698
time_elpased: 2.229
batch start
#iterations: 127
currently lose_sum: 85.69440245628357
time_elpased: 2.284
batch start
#iterations: 128
currently lose_sum: 85.48499900102615
time_elpased: 2.284
batch start
#iterations: 129
currently lose_sum: 84.46563705801964
time_elpased: 2.221
batch start
#iterations: 130
currently lose_sum: 84.83839529752731
time_elpased: 2.216
batch start
#iterations: 131
currently lose_sum: 84.30154794454575
time_elpased: 2.229
batch start
#iterations: 132
currently lose_sum: 84.29550588130951
time_elpased: 2.248
batch start
#iterations: 133
currently lose_sum: 84.86787304282188
time_elpased: 2.249
batch start
#iterations: 134
currently lose_sum: 84.32525932788849
time_elpased: 2.229
batch start
#iterations: 135
currently lose_sum: 84.07695174217224
time_elpased: 2.301
batch start
#iterations: 136
currently lose_sum: 84.05759742856026
time_elpased: 2.198
batch start
#iterations: 137
currently lose_sum: 83.72448873519897
time_elpased: 2.267
batch start
#iterations: 138
currently lose_sum: 83.82378256320953
time_elpased: 2.24
batch start
#iterations: 139
currently lose_sum: 83.27962696552277
time_elpased: 2.227
start validation test
0.60706185567
0.647124982427
0.473754631536
0.547031909204
0.607282107176
74.041
batch start
#iterations: 140
currently lose_sum: 82.84878116846085
time_elpased: 2.193
batch start
#iterations: 141
currently lose_sum: 83.17893522977829
time_elpased: 2.175
batch start
#iterations: 142
currently lose_sum: 82.54647827148438
time_elpased: 2.209
batch start
#iterations: 143
currently lose_sum: 82.84031575918198
time_elpased: 2.249
batch start
#iterations: 144
currently lose_sum: 82.80253204703331
time_elpased: 2.186
batch start
#iterations: 145
currently lose_sum: 82.52725127339363
time_elpased: 2.263
batch start
#iterations: 146
currently lose_sum: 81.65553268790245
time_elpased: 2.192
batch start
#iterations: 147
currently lose_sum: 82.53029149770737
time_elpased: 2.229
batch start
#iterations: 148
currently lose_sum: 83.46818920969963
time_elpased: 2.251
batch start
#iterations: 149
currently lose_sum: 82.69742932915688
time_elpased: 2.177
batch start
#iterations: 150
currently lose_sum: 81.70574188232422
time_elpased: 2.235
batch start
#iterations: 151
currently lose_sum: 83.00036111474037
time_elpased: 2.204
batch start
#iterations: 152
currently lose_sum: 82.39147931337357
time_elpased: 2.243
batch start
#iterations: 153
currently lose_sum: 82.83719009160995
time_elpased: 2.229
batch start
#iterations: 154
currently lose_sum: 81.38401195406914
time_elpased: 2.21
batch start
#iterations: 155
currently lose_sum: 81.26809391379356
time_elpased: 2.207
batch start
#iterations: 156
currently lose_sum: 80.6187487244606
time_elpased: 2.189
batch start
#iterations: 157
currently lose_sum: 80.9580651819706
time_elpased: 2.195
batch start
#iterations: 158
currently lose_sum: 80.28337994217873
time_elpased: 2.211
batch start
#iterations: 159
currently lose_sum: 80.97407117486
time_elpased: 2.261
start validation test
0.612783505155
0.622771836007
0.575339645945
0.598116841429
0.612845370267
69.249
batch start
#iterations: 160
currently lose_sum: 80.81916004419327
time_elpased: 2.324
batch start
#iterations: 161
currently lose_sum: 80.33686196804047
time_elpased: 2.191
batch start
#iterations: 162
currently lose_sum: 79.9060517847538
time_elpased: 2.247
batch start
#iterations: 163
currently lose_sum: 80.10827878117561
time_elpased: 2.235
batch start
#iterations: 164
currently lose_sum: 79.73311150074005
time_elpased: 2.213
batch start
#iterations: 165
currently lose_sum: 80.18794596195221
time_elpased: 2.207
batch start
#iterations: 166
currently lose_sum: 79.27133297920227
time_elpased: 2.221
batch start
#iterations: 167
currently lose_sum: 79.34216165542603
time_elpased: 2.245
batch start
#iterations: 168
currently lose_sum: 79.11261928081512
time_elpased: 2.232
batch start
#iterations: 169
currently lose_sum: 78.78567031025887
time_elpased: 2.219
batch start
#iterations: 170
currently lose_sum: 78.16347914934158
time_elpased: 2.184
batch start
#iterations: 171
currently lose_sum: 78.67112323641777
time_elpased: 2.259
batch start
#iterations: 172
currently lose_sum: 77.96938049793243
time_elpased: 2.199
batch start
#iterations: 173
currently lose_sum: 77.44682464003563
time_elpased: 2.229
batch start
#iterations: 174
currently lose_sum: 79.37659478187561
time_elpased: 2.258
batch start
#iterations: 175
currently lose_sum: 77.8461822271347
time_elpased: 2.208
batch start
#iterations: 176
currently lose_sum: 77.08614230155945
time_elpased: 2.232
batch start
#iterations: 177
currently lose_sum: 76.94317424297333
time_elpased: 2.21
batch start
#iterations: 178
currently lose_sum: 77.67881408333778
time_elpased: 2.247
batch start
#iterations: 179
currently lose_sum: 77.01069408655167
time_elpased: 2.203
start validation test
0.585257731959
0.633942893808
0.406751749691
0.495548589342
0.585552661297
86.203
batch start
#iterations: 180
currently lose_sum: 76.70397379994392
time_elpased: 2.233
batch start
#iterations: 181
currently lose_sum: 77.31957146525383
time_elpased: 2.22
batch start
#iterations: 182
currently lose_sum: 76.43256843090057
time_elpased: 2.191
batch start
#iterations: 183
currently lose_sum: 75.25098675489426
time_elpased: 2.192
batch start
#iterations: 184
currently lose_sum: 75.82467928528786
time_elpased: 2.233
batch start
#iterations: 185
currently lose_sum: 76.48793911933899
time_elpased: 2.195
batch start
#iterations: 186
currently lose_sum: 75.44704815745354
time_elpased: 2.206
batch start
#iterations: 187
currently lose_sum: 75.40319022536278
time_elpased: 2.297
batch start
#iterations: 188
currently lose_sum: 76.06157791614532
time_elpased: 2.231
batch start
#iterations: 189
currently lose_sum: 74.87726157903671
time_elpased: 2.204
batch start
#iterations: 190
currently lose_sum: 75.03909942507744
time_elpased: 2.245
batch start
#iterations: 191
currently lose_sum: 74.9254443347454
time_elpased: 2.223
batch start
#iterations: 192
currently lose_sum: 75.6551488339901
time_elpased: 2.198
batch start
#iterations: 193
currently lose_sum: 74.64193508028984
time_elpased: 2.228
batch start
#iterations: 194
currently lose_sum: 73.98636165261269
time_elpased: 2.224
batch start
#iterations: 195
currently lose_sum: 73.86594158411026
time_elpased: 2.249
batch start
#iterations: 196
currently lose_sum: 74.42223733663559
time_elpased: 2.206
batch start
#iterations: 197
currently lose_sum: 73.88617777824402
time_elpased: 2.274
batch start
#iterations: 198
currently lose_sum: 73.19415673613548
time_elpased: 2.259
batch start
#iterations: 199
currently lose_sum: 73.38027122616768
time_elpased: 2.208
start validation test
0.585206185567
0.627462960134
0.422807739811
0.505195843325
0.585474501876
83.610
batch start
#iterations: 200
currently lose_sum: 73.50781181454659
time_elpased: 2.235
batch start
#iterations: 201
currently lose_sum: 72.94510787725449
time_elpased: 2.263
batch start
#iterations: 202
currently lose_sum: 72.76919665932655
time_elpased: 2.228
batch start
#iterations: 203
currently lose_sum: 72.60704708099365
time_elpased: 2.262
batch start
#iterations: 204
currently lose_sum: 72.3485923409462
time_elpased: 2.243
batch start
#iterations: 205
currently lose_sum: 73.11756771802902
time_elpased: 2.229
batch start
#iterations: 206
currently lose_sum: 71.74228733778
time_elpased: 2.21
batch start
#iterations: 207
currently lose_sum: 71.57823994755745
time_elpased: 2.175
batch start
#iterations: 208
currently lose_sum: 71.1883251965046
time_elpased: 2.234
batch start
#iterations: 209
currently lose_sum: 70.8551310300827
time_elpased: 2.269
batch start
#iterations: 210
currently lose_sum: 71.76754751801491
time_elpased: 2.246
batch start
#iterations: 211
currently lose_sum: 70.29077368974686
time_elpased: 2.234
batch start
#iterations: 212
currently lose_sum: 69.93580484390259
time_elpased: 2.22
batch start
#iterations: 213
currently lose_sum: 70.20527392625809
time_elpased: 2.235
batch start
#iterations: 214
currently lose_sum: 70.20936197042465
time_elpased: 2.219
batch start
#iterations: 215
currently lose_sum: 69.88062113523483
time_elpased: 2.237
batch start
#iterations: 216
currently lose_sum: 69.90353938937187
time_elpased: 2.228
batch start
#iterations: 217
currently lose_sum: 69.34453803300858
time_elpased: 2.259
batch start
#iterations: 218
currently lose_sum: 68.97305163741112
time_elpased: 2.201
batch start
#iterations: 219
currently lose_sum: 68.79967704415321
time_elpased: 2.236
start validation test
0.576546391753
0.605244706212
0.444215726636
0.512376090699
0.576765029778
99.300
batch start
#iterations: 220
currently lose_sum: 68.00163421034813
time_elpased: 2.24
batch start
#iterations: 221
currently lose_sum: 69.19969719648361
time_elpased: 2.191
batch start
#iterations: 222
currently lose_sum: 68.86974048614502
time_elpased: 2.224
batch start
#iterations: 223
currently lose_sum: 67.89118754863739
time_elpased: 2.257
batch start
#iterations: 224
currently lose_sum: 68.32466930150986
time_elpased: 2.234
batch start
#iterations: 225
currently lose_sum: 67.75808915495872
time_elpased: 2.241
batch start
#iterations: 226
currently lose_sum: 67.2569091618061
time_elpased: 2.23
batch start
#iterations: 227
currently lose_sum: 67.24525189399719
time_elpased: 2.232
batch start
#iterations: 228
currently lose_sum: 68.00609710812569
time_elpased: 2.26
batch start
#iterations: 229
currently lose_sum: 66.94026353955269
time_elpased: 2.288
batch start
#iterations: 230
currently lose_sum: 67.29934844374657
time_elpased: 2.168
batch start
#iterations: 231
currently lose_sum: 67.38488680124283
time_elpased: 2.214
batch start
#iterations: 232
currently lose_sum: 67.03361758589745
time_elpased: 2.253
batch start
#iterations: 233
currently lose_sum: 66.38826176524162
time_elpased: 2.256
batch start
#iterations: 234
currently lose_sum: 66.60708719491959
time_elpased: 2.263
batch start
#iterations: 235
currently lose_sum: 66.01347041130066
time_elpased: 2.272
batch start
#iterations: 236
currently lose_sum: 66.11160826683044
time_elpased: 2.207
batch start
#iterations: 237
currently lose_sum: 65.57551631331444
time_elpased: 2.253
batch start
#iterations: 238
currently lose_sum: 64.82221874594688
time_elpased: 2.217
batch start
#iterations: 239
currently lose_sum: 65.0880221426487
time_elpased: 2.228
start validation test
0.579226804124
0.607177363699
0.452758336764
0.518719415129
0.579435756569
100.232
batch start
#iterations: 240
currently lose_sum: 65.74280315637589
time_elpased: 2.214
batch start
#iterations: 241
currently lose_sum: 65.34496307373047
time_elpased: 2.209
batch start
#iterations: 242
currently lose_sum: 63.54589906334877
time_elpased: 2.214
batch start
#iterations: 243
currently lose_sum: 64.574227809906
time_elpased: 2.229
batch start
#iterations: 244
currently lose_sum: 63.35467594861984
time_elpased: 2.247
batch start
#iterations: 245
currently lose_sum: 64.36986324191093
time_elpased: 2.218
batch start
#iterations: 246
currently lose_sum: 63.316606253385544
time_elpased: 2.241
batch start
#iterations: 247
currently lose_sum: 63.56018677353859
time_elpased: 2.256
batch start
#iterations: 248
currently lose_sum: 62.40318715572357
time_elpased: 2.254
batch start
#iterations: 249
currently lose_sum: 63.457745373249054
time_elpased: 2.243
batch start
#iterations: 250
currently lose_sum: 62.795659482479095
time_elpased: 2.298
batch start
#iterations: 251
currently lose_sum: 61.838676393032074
time_elpased: 2.228
batch start
#iterations: 252
currently lose_sum: 61.65084168314934
time_elpased: 2.238
batch start
#iterations: 253
currently lose_sum: 61.95193129777908
time_elpased: 2.172
batch start
#iterations: 254
currently lose_sum: 63.428143322467804
time_elpased: 2.22
batch start
#iterations: 255
currently lose_sum: 60.746488243341446
time_elpased: 2.242
batch start
#iterations: 256
currently lose_sum: 62.02362349629402
time_elpased: 2.214
batch start
#iterations: 257
currently lose_sum: 61.147658318281174
time_elpased: 2.235
batch start
#iterations: 258
currently lose_sum: 60.29830414056778
time_elpased: 2.254
batch start
#iterations: 259
currently lose_sum: 60.97911447286606
time_elpased: 2.213
start validation test
0.556494845361
0.589909443726
0.375463153561
0.458867924528
0.556793947702
126.334
batch start
#iterations: 260
currently lose_sum: 60.314022183418274
time_elpased: 2.28
batch start
#iterations: 261
currently lose_sum: 60.047050416469574
time_elpased: 2.194
batch start
#iterations: 262
currently lose_sum: 61.18476963043213
time_elpased: 2.223
batch start
#iterations: 263
currently lose_sum: 59.04641172289848
time_elpased: 2.217
batch start
#iterations: 264
currently lose_sum: 60.19625407457352
time_elpased: 2.202
batch start
#iterations: 265
currently lose_sum: 61.59434309601784
time_elpased: 2.172
batch start
#iterations: 266
currently lose_sum: 59.80205723643303
time_elpased: 2.264
batch start
#iterations: 267
currently lose_sum: 58.18785071372986
time_elpased: 2.236
batch start
#iterations: 268
currently lose_sum: 59.69310104846954
time_elpased: 2.213
batch start
#iterations: 269
currently lose_sum: 59.166517943143845
time_elpased: 2.196
batch start
#iterations: 270
currently lose_sum: 58.082938104867935
time_elpased: 2.252
batch start
#iterations: 271
currently lose_sum: 58.97050920128822
time_elpased: 2.231
batch start
#iterations: 272
currently lose_sum: 59.94090577960014
time_elpased: 2.211
batch start
#iterations: 273
currently lose_sum: 58.58829337358475
time_elpased: 2.253
batch start
#iterations: 274
currently lose_sum: 58.581501960754395
time_elpased: 2.357
batch start
#iterations: 275
currently lose_sum: 57.453313797712326
time_elpased: 2.253
batch start
#iterations: 276
currently lose_sum: 57.86486917734146
time_elpased: 2.201
batch start
#iterations: 277
currently lose_sum: 56.84254467487335
time_elpased: 2.223
batch start
#iterations: 278
currently lose_sum: 56.937391459941864
time_elpased: 2.189
batch start
#iterations: 279
currently lose_sum: 56.93370524048805
time_elpased: 2.172
start validation test
0.563969072165
0.61829474873
0.338102099629
0.437154833988
0.564342251797
135.504
batch start
#iterations: 280
currently lose_sum: 56.77310034632683
time_elpased: 2.25
batch start
#iterations: 281
currently lose_sum: 56.83166068792343
time_elpased: 2.232
batch start
#iterations: 282
currently lose_sum: 56.84033805131912
time_elpased: 2.203
batch start
#iterations: 283
currently lose_sum: 57.73012736439705
time_elpased: 2.198
batch start
#iterations: 284
currently lose_sum: 57.33204808831215
time_elpased: 2.222
batch start
#iterations: 285
currently lose_sum: 55.2535146176815
time_elpased: 2.195
batch start
#iterations: 286
currently lose_sum: 55.727729111909866
time_elpased: 2.223
batch start
#iterations: 287
currently lose_sum: 55.42548853158951
time_elpased: 2.221
batch start
#iterations: 288
currently lose_sum: 54.727781534194946
time_elpased: 2.177
batch start
#iterations: 289
currently lose_sum: 57.10965967178345
time_elpased: 2.213
batch start
#iterations: 290
currently lose_sum: 56.10244092345238
time_elpased: 2.284
batch start
#iterations: 291
currently lose_sum: 54.81623017787933
time_elpased: 2.245
batch start
#iterations: 292
currently lose_sum: 55.557780891656876
time_elpased: 2.219
batch start
#iterations: 293
currently lose_sum: 55.31291148066521
time_elpased: 2.213
batch start
#iterations: 294
currently lose_sum: 54.417817652225494
time_elpased: 2.224
batch start
#iterations: 295
currently lose_sum: 55.66469746828079
time_elpased: 2.175
batch start
#iterations: 296
currently lose_sum: 54.32869437336922
time_elpased: 2.241
batch start
#iterations: 297
currently lose_sum: 55.62275269627571
time_elpased: 2.265
batch start
#iterations: 298
currently lose_sum: 53.95759227871895
time_elpased: 2.227
batch start
#iterations: 299
currently lose_sum: 54.2933269739151
time_elpased: 2.198
start validation test
0.543917525773
0.589043906442
0.295491972005
0.39355723098
0.544327976915
156.386
batch start
#iterations: 300
currently lose_sum: 54.35966491699219
time_elpased: 2.245
batch start
#iterations: 301
currently lose_sum: 54.15082114934921
time_elpased: 2.199
batch start
#iterations: 302
currently lose_sum: 53.692851185798645
time_elpased: 2.261
batch start
#iterations: 303
currently lose_sum: 52.527919709682465
time_elpased: 2.259
batch start
#iterations: 304
currently lose_sum: 54.12284964323044
time_elpased: 2.224
batch start
#iterations: 305
currently lose_sum: 51.769119918346405
time_elpased: 2.274
batch start
#iterations: 306
currently lose_sum: 52.94755765795708
time_elpased: 2.213
batch start
#iterations: 307
currently lose_sum: 54.3901541531086
time_elpased: 2.291
batch start
#iterations: 308
currently lose_sum: 52.15660557150841
time_elpased: 2.22
batch start
#iterations: 309
currently lose_sum: 53.232024639844894
time_elpased: 2.233
batch start
#iterations: 310
currently lose_sum: 52.91375055909157
time_elpased: 2.285
batch start
#iterations: 311
currently lose_sum: 53.596056401729584
time_elpased: 2.165
batch start
#iterations: 312
currently lose_sum: 52.35865533351898
time_elpased: 2.249
batch start
#iterations: 313
currently lose_sum: 52.65509197115898
time_elpased: 2.248
batch start
#iterations: 314
currently lose_sum: 51.16686096787453
time_elpased: 2.206
batch start
#iterations: 315
currently lose_sum: 52.95941090583801
time_elpased: 2.225
batch start
#iterations: 316
currently lose_sum: 51.79715731739998
time_elpased: 2.267
batch start
#iterations: 317
currently lose_sum: 51.69448286294937
time_elpased: 2.192
batch start
#iterations: 318
currently lose_sum: 50.39182963967323
time_elpased: 2.215
batch start
#iterations: 319
currently lose_sum: 52.59407094120979
time_elpased: 2.254
start validation test
0.543350515464
0.601037491158
0.26235076163
0.365264741707
0.54381478602
173.610
batch start
#iterations: 320
currently lose_sum: 50.846805930137634
time_elpased: 2.224
batch start
#iterations: 321
currently lose_sum: 52.286205649375916
time_elpased: 2.217
batch start
#iterations: 322
currently lose_sum: 50.32497826218605
time_elpased: 2.249
batch start
#iterations: 323
currently lose_sum: 50.51885235309601
time_elpased: 2.204
batch start
#iterations: 324
currently lose_sum: 49.92587774991989
time_elpased: 2.21
batch start
#iterations: 325
currently lose_sum: 49.06372258067131
time_elpased: 2.209
batch start
#iterations: 326
currently lose_sum: 50.32415151596069
time_elpased: 2.188
batch start
#iterations: 327
currently lose_sum: 49.912214666604996
time_elpased: 2.248
batch start
#iterations: 328
currently lose_sum: 50.11294940114021
time_elpased: 2.26
batch start
#iterations: 329
currently lose_sum: 48.6212425082922
time_elpased: 2.215
batch start
#iterations: 330
currently lose_sum: 49.39718535542488
time_elpased: 2.251
batch start
#iterations: 331
currently lose_sum: 50.735573172569275
time_elpased: 2.243
batch start
#iterations: 332
currently lose_sum: 49.24262025952339
time_elpased: 2.257
batch start
#iterations: 333
currently lose_sum: 50.107017785310745
time_elpased: 2.177
batch start
#iterations: 334
currently lose_sum: 49.96081927418709
time_elpased: 2.257
batch start
#iterations: 335
currently lose_sum: 49.72147551178932
time_elpased: 2.187
batch start
#iterations: 336
currently lose_sum: 50.10514935851097
time_elpased: 2.217
batch start
#iterations: 337
currently lose_sum: 49.45030039548874
time_elpased: 2.219
batch start
#iterations: 338
currently lose_sum: 49.319140046834946
time_elpased: 2.248
batch start
#iterations: 339
currently lose_sum: 48.80943292379379
time_elpased: 2.2
start validation test
0.542989690722
0.584191759113
0.303519967065
0.399485234354
0.543385344953
174.952
batch start
#iterations: 340
currently lose_sum: 48.48685497045517
time_elpased: 2.202
batch start
#iterations: 341
currently lose_sum: 48.44654926657677
time_elpased: 2.288
batch start
#iterations: 342
currently lose_sum: 47.655518889427185
time_elpased: 2.233
batch start
#iterations: 343
currently lose_sum: 49.69435432553291
time_elpased: 2.311
batch start
#iterations: 344
currently lose_sum: 48.144637644290924
time_elpased: 2.227
batch start
#iterations: 345
currently lose_sum: 48.90620177984238
time_elpased: 2.248
batch start
#iterations: 346
currently lose_sum: 46.60888281464577
time_elpased: 2.246
batch start
#iterations: 347
currently lose_sum: 47.859047651290894
time_elpased: 2.26
batch start
#iterations: 348
currently lose_sum: 48.44870473444462
time_elpased: 2.309
batch start
#iterations: 349
currently lose_sum: 47.59429794549942
time_elpased: 2.19
batch start
#iterations: 350
currently lose_sum: 47.33165800571442
time_elpased: 2.228
batch start
#iterations: 351
currently lose_sum: 47.96258017420769
time_elpased: 2.233
batch start
#iterations: 352
currently lose_sum: 45.98917239904404
time_elpased: 2.263
batch start
#iterations: 353
currently lose_sum: 46.955802261829376
time_elpased: 2.204
batch start
#iterations: 354
currently lose_sum: 47.7503342628479
time_elpased: 2.246
batch start
#iterations: 355
currently lose_sum: 48.303794503211975
time_elpased: 2.23
batch start
#iterations: 356
currently lose_sum: 47.49101313948631
time_elpased: 2.187
batch start
#iterations: 357
currently lose_sum: 45.639157354831696
time_elpased: 2.211
batch start
#iterations: 358
currently lose_sum: 45.431217670440674
time_elpased: 2.277
batch start
#iterations: 359
currently lose_sum: 47.09431567788124
time_elpased: 2.251
start validation test
0.539845360825
0.591003460208
0.263688760807
0.364671553626
0.540301629474
194.029
batch start
#iterations: 360
currently lose_sum: 46.96718214452267
time_elpased: 2.229
batch start
#iterations: 361
currently lose_sum: 47.225690484046936
time_elpased: 2.206
batch start
#iterations: 362
currently lose_sum: 47.15698918700218
time_elpased: 2.213
batch start
#iterations: 363
currently lose_sum: 44.96076114475727
time_elpased: 2.24
batch start
#iterations: 364
currently lose_sum: 45.30541780591011
time_elpased: 2.236
batch start
#iterations: 365
currently lose_sum: 45.798991441726685
time_elpased: 2.23
batch start
#iterations: 366
currently lose_sum: 47.55870634317398
time_elpased: 2.208
batch start
#iterations: 367
currently lose_sum: 46.51375874876976
time_elpased: 2.302
batch start
#iterations: 368
currently lose_sum: 45.95802354812622
time_elpased: 2.167
batch start
#iterations: 369
currently lose_sum: 47.287276938557625
time_elpased: 2.212
batch start
#iterations: 370
currently lose_sum: 46.59060713648796
time_elpased: 2.197
batch start
#iterations: 371
currently lose_sum: 46.15086604654789
time_elpased: 2.232
batch start
#iterations: 372
currently lose_sum: 46.143823340535164
time_elpased: 2.278
batch start
#iterations: 373
currently lose_sum: 44.745251804590225
time_elpased: 2.21
batch start
#iterations: 374
currently lose_sum: 45.77210685610771
time_elpased: 2.202
batch start
#iterations: 375
currently lose_sum: 47.59333127737045
time_elpased: 2.265
batch start
#iterations: 376
currently lose_sum: 45.71475873887539
time_elpased: 2.264
batch start
#iterations: 377
currently lose_sum: 45.136963710188866
time_elpased: 2.205
batch start
#iterations: 378
currently lose_sum: 44.899194940924644
time_elpased: 2.189
batch start
#iterations: 379
currently lose_sum: 43.866652116179466
time_elpased: 2.172
start validation test
0.536855670103
0.594762768991
0.236105393166
0.338024018272
0.537352572667
220.479
batch start
#iterations: 380
currently lose_sum: 44.04634687304497
time_elpased: 2.248
batch start
#iterations: 381
currently lose_sum: 46.10932545363903
time_elpased: 2.273
batch start
#iterations: 382
currently lose_sum: 43.23679059743881
time_elpased: 2.249
batch start
#iterations: 383
currently lose_sum: 43.125780299305916
time_elpased: 2.203
batch start
#iterations: 384
currently lose_sum: 45.27984577417374
time_elpased: 2.272
batch start
#iterations: 385
currently lose_sum: 46.42642416059971
time_elpased: 2.215
batch start
#iterations: 386
currently lose_sum: 45.32926966249943
time_elpased: 2.244
batch start
#iterations: 387
currently lose_sum: 43.227650821208954
time_elpased: 2.24
batch start
#iterations: 388
currently lose_sum: 44.74260573089123
time_elpased: 2.255
batch start
#iterations: 389
currently lose_sum: 44.34519697725773
time_elpased: 2.238
batch start
#iterations: 390
currently lose_sum: 44.57194097340107
time_elpased: 2.176
batch start
#iterations: 391
currently lose_sum: 43.009376749396324
time_elpased: 2.251
batch start
#iterations: 392
currently lose_sum: 44.9539707750082
time_elpased: 2.182
batch start
#iterations: 393
currently lose_sum: 44.43968091905117
time_elpased: 2.312
batch start
#iterations: 394
currently lose_sum: 43.62010020017624
time_elpased: 2.216
batch start
#iterations: 395
currently lose_sum: 45.33389176428318
time_elpased: 2.221
batch start
#iterations: 396
currently lose_sum: 43.96446554362774
time_elpased: 2.206
batch start
#iterations: 397
currently lose_sum: 45.266012981534004
time_elpased: 2.241
batch start
#iterations: 398
currently lose_sum: 42.93527136743069
time_elpased: 2.221
batch start
#iterations: 399
currently lose_sum: 43.736991837620735
time_elpased: 2.23
start validation test
0.526804123711
0.620612061206
0.141930835735
0.231026972692
0.527440015141
238.669
acc: 0.635
pre: 0.612
rec: 0.743
F1: 0.671
auc: 0.679
