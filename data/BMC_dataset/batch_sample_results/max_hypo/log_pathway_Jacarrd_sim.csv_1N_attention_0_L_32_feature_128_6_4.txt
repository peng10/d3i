start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.45536810159683
time_elpased: 2.256
batch start
#iterations: 1
currently lose_sum: 100.14351415634155
time_elpased: 2.259
batch start
#iterations: 2
currently lose_sum: 100.05362510681152
time_elpased: 2.23
batch start
#iterations: 3
currently lose_sum: 99.76810795068741
time_elpased: 2.263
batch start
#iterations: 4
currently lose_sum: 99.5865426659584
time_elpased: 2.2
batch start
#iterations: 5
currently lose_sum: 99.44092571735382
time_elpased: 2.269
batch start
#iterations: 6
currently lose_sum: 99.1415183544159
time_elpased: 2.247
batch start
#iterations: 7
currently lose_sum: 98.83409601449966
time_elpased: 2.257
batch start
#iterations: 8
currently lose_sum: 98.68484538793564
time_elpased: 2.251
batch start
#iterations: 9
currently lose_sum: 98.50507426261902
time_elpased: 2.208
batch start
#iterations: 10
currently lose_sum: 98.21911162137985
time_elpased: 2.228
batch start
#iterations: 11
currently lose_sum: 98.12050771713257
time_elpased: 2.232
batch start
#iterations: 12
currently lose_sum: 97.81135243177414
time_elpased: 2.3
batch start
#iterations: 13
currently lose_sum: 97.85212767124176
time_elpased: 2.25
batch start
#iterations: 14
currently lose_sum: 97.62509250640869
time_elpased: 2.27
batch start
#iterations: 15
currently lose_sum: 97.36987060308456
time_elpased: 2.226
batch start
#iterations: 16
currently lose_sum: 97.32028514146805
time_elpased: 2.213
batch start
#iterations: 17
currently lose_sum: 97.13974767923355
time_elpased: 2.212
batch start
#iterations: 18
currently lose_sum: 96.5836091041565
time_elpased: 2.267
batch start
#iterations: 19
currently lose_sum: 96.70363223552704
time_elpased: 2.241
start validation test
0.633144329897
0.613457846046
0.723371410929
0.663896103896
0.63298592234
62.613
batch start
#iterations: 20
currently lose_sum: 96.77199703454971
time_elpased: 2.275
batch start
#iterations: 21
currently lose_sum: 96.13464796543121
time_elpased: 2.242
batch start
#iterations: 22
currently lose_sum: 96.50218719244003
time_elpased: 2.26
batch start
#iterations: 23
currently lose_sum: 96.57038867473602
time_elpased: 2.212
batch start
#iterations: 24
currently lose_sum: 95.87066406011581
time_elpased: 2.232
batch start
#iterations: 25
currently lose_sum: 95.88215756416321
time_elpased: 2.224
batch start
#iterations: 26
currently lose_sum: 96.66894799470901
time_elpased: 2.262
batch start
#iterations: 27
currently lose_sum: 95.35686141252518
time_elpased: 2.243
batch start
#iterations: 28
currently lose_sum: 95.41781044006348
time_elpased: 2.243
batch start
#iterations: 29
currently lose_sum: 95.01828896999359
time_elpased: 2.241
batch start
#iterations: 30
currently lose_sum: 95.252745449543
time_elpased: 2.297
batch start
#iterations: 31
currently lose_sum: 95.16799676418304
time_elpased: 2.267
batch start
#iterations: 32
currently lose_sum: 95.06706720590591
time_elpased: 2.271
batch start
#iterations: 33
currently lose_sum: 94.79495167732239
time_elpased: 2.273
batch start
#iterations: 34
currently lose_sum: 94.61040127277374
time_elpased: 2.215
batch start
#iterations: 35
currently lose_sum: 94.62722927331924
time_elpased: 2.237
batch start
#iterations: 36
currently lose_sum: 94.41492420434952
time_elpased: 2.276
batch start
#iterations: 37
currently lose_sum: 94.46008044481277
time_elpased: 2.209
batch start
#iterations: 38
currently lose_sum: 94.39064300060272
time_elpased: 2.249
batch start
#iterations: 39
currently lose_sum: 94.30636465549469
time_elpased: 2.257
start validation test
0.635206185567
0.61879049676
0.707625810435
0.660233328532
0.635079041746
62.164
batch start
#iterations: 40
currently lose_sum: 93.83763670921326
time_elpased: 2.25
batch start
#iterations: 41
currently lose_sum: 94.18211859464645
time_elpased: 2.256
batch start
#iterations: 42
currently lose_sum: 93.98328852653503
time_elpased: 2.265
batch start
#iterations: 43
currently lose_sum: 93.9550188779831
time_elpased: 2.239
batch start
#iterations: 44
currently lose_sum: 93.73926329612732
time_elpased: 2.293
batch start
#iterations: 45
currently lose_sum: 93.97052270174026
time_elpased: 2.204
batch start
#iterations: 46
currently lose_sum: 93.15743011236191
time_elpased: 2.242
batch start
#iterations: 47
currently lose_sum: 93.7425467967987
time_elpased: 2.263
batch start
#iterations: 48
currently lose_sum: 93.4931611418724
time_elpased: 2.221
batch start
#iterations: 49
currently lose_sum: 93.05899930000305
time_elpased: 2.211
batch start
#iterations: 50
currently lose_sum: 93.35411483049393
time_elpased: 2.24
batch start
#iterations: 51
currently lose_sum: 92.89380013942719
time_elpased: 2.239
batch start
#iterations: 52
currently lose_sum: 93.08766305446625
time_elpased: 2.243
batch start
#iterations: 53
currently lose_sum: 92.62029564380646
time_elpased: 2.265
batch start
#iterations: 54
currently lose_sum: 92.68073117733002
time_elpased: 2.308
batch start
#iterations: 55
currently lose_sum: 92.45997327566147
time_elpased: 2.221
batch start
#iterations: 56
currently lose_sum: 92.92933684587479
time_elpased: 2.299
batch start
#iterations: 57
currently lose_sum: 92.46980011463165
time_elpased: 2.21
batch start
#iterations: 58
currently lose_sum: 92.47065353393555
time_elpased: 2.329
batch start
#iterations: 59
currently lose_sum: 93.07313793897629
time_elpased: 2.275
start validation test
0.621391752577
0.605197800248
0.702171452094
0.650088133009
0.621249931355
62.226
batch start
#iterations: 60
currently lose_sum: 92.36952632665634
time_elpased: 2.225
batch start
#iterations: 61
currently lose_sum: 92.03275674581528
time_elpased: 2.234
batch start
#iterations: 62
currently lose_sum: 92.09808856248856
time_elpased: 2.212
batch start
#iterations: 63
currently lose_sum: 91.79540473222733
time_elpased: 2.245
batch start
#iterations: 64
currently lose_sum: 91.88443440198898
time_elpased: 2.199
batch start
#iterations: 65
currently lose_sum: 91.88006919622421
time_elpased: 2.222
batch start
#iterations: 66
currently lose_sum: 91.87404209375381
time_elpased: 2.259
batch start
#iterations: 67
currently lose_sum: 91.58365070819855
time_elpased: 2.262
batch start
#iterations: 68
currently lose_sum: 90.90763187408447
time_elpased: 2.256
batch start
#iterations: 69
currently lose_sum: 90.97529882192612
time_elpased: 2.214
batch start
#iterations: 70
currently lose_sum: 91.7471981048584
time_elpased: 2.237
batch start
#iterations: 71
currently lose_sum: 91.71236211061478
time_elpased: 2.247
batch start
#iterations: 72
currently lose_sum: 90.29450589418411
time_elpased: 2.207
batch start
#iterations: 73
currently lose_sum: 91.09884196519852
time_elpased: 2.267
batch start
#iterations: 74
currently lose_sum: 90.04994094371796
time_elpased: 2.215
batch start
#iterations: 75
currently lose_sum: 91.28765225410461
time_elpased: 2.274
batch start
#iterations: 76
currently lose_sum: 90.63791424036026
time_elpased: 2.259
batch start
#iterations: 77
currently lose_sum: 90.31041592359543
time_elpased: 2.241
batch start
#iterations: 78
currently lose_sum: 90.6615571975708
time_elpased: 2.25
batch start
#iterations: 79
currently lose_sum: 90.24657541513443
time_elpased: 2.217
start validation test
0.595567010309
0.600949606129
0.573119275497
0.586704593342
0.59560642077
64.289
batch start
#iterations: 80
currently lose_sum: 89.85649400949478
time_elpased: 2.243
batch start
#iterations: 81
currently lose_sum: 90.6303179860115
time_elpased: 2.25
batch start
#iterations: 82
currently lose_sum: 90.63488984107971
time_elpased: 2.255
batch start
#iterations: 83
currently lose_sum: 89.9061706662178
time_elpased: 2.238
batch start
#iterations: 84
currently lose_sum: 89.93608808517456
time_elpased: 2.264
batch start
#iterations: 85
currently lose_sum: 89.66453558206558
time_elpased: 2.329
batch start
#iterations: 86
currently lose_sum: 89.87398666143417
time_elpased: 2.275
batch start
#iterations: 87
currently lose_sum: 89.40258538722992
time_elpased: 2.238
batch start
#iterations: 88
currently lose_sum: 89.67751550674438
time_elpased: 2.291
batch start
#iterations: 89
currently lose_sum: 89.08778727054596
time_elpased: 2.229
batch start
#iterations: 90
currently lose_sum: 88.96196031570435
time_elpased: 2.25
batch start
#iterations: 91
currently lose_sum: 89.78285872936249
time_elpased: 2.268
batch start
#iterations: 92
currently lose_sum: 89.28165584802628
time_elpased: 2.252
batch start
#iterations: 93
currently lose_sum: 88.86073929071426
time_elpased: 2.252
batch start
#iterations: 94
currently lose_sum: 89.24938195943832
time_elpased: 2.234
batch start
#iterations: 95
currently lose_sum: 88.78039348125458
time_elpased: 2.235
batch start
#iterations: 96
currently lose_sum: 89.12600445747375
time_elpased: 2.239
batch start
#iterations: 97
currently lose_sum: 88.79902648925781
time_elpased: 2.287
batch start
#iterations: 98
currently lose_sum: 88.5279329419136
time_elpased: 2.269
batch start
#iterations: 99
currently lose_sum: 88.80244451761246
time_elpased: 2.265
start validation test
0.621546391753
0.628698385174
0.596994957291
0.612436655405
0.621589495583
64.299
batch start
#iterations: 100
currently lose_sum: 88.24485731124878
time_elpased: 2.232
batch start
#iterations: 101
currently lose_sum: 88.10723543167114
time_elpased: 2.273
batch start
#iterations: 102
currently lose_sum: 87.98776513338089
time_elpased: 2.231
batch start
#iterations: 103
currently lose_sum: 87.43180501461029
time_elpased: 2.247
batch start
#iterations: 104
currently lose_sum: 87.32384324073792
time_elpased: 2.258
batch start
#iterations: 105
currently lose_sum: 87.1278263926506
time_elpased: 2.228
batch start
#iterations: 106
currently lose_sum: 87.0552915930748
time_elpased: 2.233
batch start
#iterations: 107
currently lose_sum: 86.62035739421844
time_elpased: 2.266
batch start
#iterations: 108
currently lose_sum: 87.70693862438202
time_elpased: 2.291
batch start
#iterations: 109
currently lose_sum: 86.98202347755432
time_elpased: 2.237
batch start
#iterations: 110
currently lose_sum: 86.54785543680191
time_elpased: 2.22
batch start
#iterations: 111
currently lose_sum: 86.42473989725113
time_elpased: 2.245
batch start
#iterations: 112
currently lose_sum: 86.08482539653778
time_elpased: 2.231
batch start
#iterations: 113
currently lose_sum: 85.86556500196457
time_elpased: 2.24
batch start
#iterations: 114
currently lose_sum: 85.98077046871185
time_elpased: 2.238
batch start
#iterations: 115
currently lose_sum: 85.8795166015625
time_elpased: 2.287
batch start
#iterations: 116
currently lose_sum: 85.84998905658722
time_elpased: 2.241
batch start
#iterations: 117
currently lose_sum: 86.22685867547989
time_elpased: 2.228
batch start
#iterations: 118
currently lose_sum: 86.36979013681412
time_elpased: 2.228
batch start
#iterations: 119
currently lose_sum: 85.79540264606476
time_elpased: 2.246
start validation test
0.587268041237
0.609307082588
0.490480601009
0.543474542448
0.587437966517
69.064
batch start
#iterations: 120
currently lose_sum: 85.0784877538681
time_elpased: 2.23
batch start
#iterations: 121
currently lose_sum: 85.31974244117737
time_elpased: 2.242
batch start
#iterations: 122
currently lose_sum: 85.66223233938217
time_elpased: 2.233
batch start
#iterations: 123
currently lose_sum: 85.84458297491074
time_elpased: 2.272
batch start
#iterations: 124
currently lose_sum: 85.20879131555557
time_elpased: 2.294
batch start
#iterations: 125
currently lose_sum: 84.32402014732361
time_elpased: 2.251
batch start
#iterations: 126
currently lose_sum: 84.6622262597084
time_elpased: 2.24
batch start
#iterations: 127
currently lose_sum: 84.57774686813354
time_elpased: 2.266
batch start
#iterations: 128
currently lose_sum: 84.49737411737442
time_elpased: 2.204
batch start
#iterations: 129
currently lose_sum: 83.78132206201553
time_elpased: 2.279
batch start
#iterations: 130
currently lose_sum: 83.99713772535324
time_elpased: 2.252
batch start
#iterations: 131
currently lose_sum: 83.78563115000725
time_elpased: 2.274
batch start
#iterations: 132
currently lose_sum: 83.63807252049446
time_elpased: 2.271
batch start
#iterations: 133
currently lose_sum: 84.06224647164345
time_elpased: 2.214
batch start
#iterations: 134
currently lose_sum: 82.89551717042923
time_elpased: 2.226
batch start
#iterations: 135
currently lose_sum: 84.06412822008133
time_elpased: 2.26
batch start
#iterations: 136
currently lose_sum: 83.3015907406807
time_elpased: 2.279
batch start
#iterations: 137
currently lose_sum: 83.31944647431374
time_elpased: 2.245
batch start
#iterations: 138
currently lose_sum: 82.78865242004395
time_elpased: 2.228
batch start
#iterations: 139
currently lose_sum: 83.12592989206314
time_elpased: 2.217
start validation test
0.584329896907
0.62095711986
0.436657404549
0.512749244713
0.584589158744
73.325
batch start
#iterations: 140
currently lose_sum: 82.78623679280281
time_elpased: 2.254
batch start
#iterations: 141
currently lose_sum: 82.13864624500275
time_elpased: 2.266
batch start
#iterations: 142
currently lose_sum: 82.62414249777794
time_elpased: 2.254
batch start
#iterations: 143
currently lose_sum: 81.4159567952156
time_elpased: 2.249
batch start
#iterations: 144
currently lose_sum: 81.46346175670624
time_elpased: 2.322
batch start
#iterations: 145
currently lose_sum: 80.63140833377838
time_elpased: 2.285
batch start
#iterations: 146
currently lose_sum: 81.24514836072922
time_elpased: 2.28
batch start
#iterations: 147
currently lose_sum: 81.36066058278084
time_elpased: 2.254
batch start
#iterations: 148
currently lose_sum: 80.94226878881454
time_elpased: 2.245
batch start
#iterations: 149
currently lose_sum: 80.93298026919365
time_elpased: 2.245
batch start
#iterations: 150
currently lose_sum: 80.27832502126694
time_elpased: 2.244
batch start
#iterations: 151
currently lose_sum: 79.66719606518745
time_elpased: 2.253
batch start
#iterations: 152
currently lose_sum: 80.38501942157745
time_elpased: 2.266
batch start
#iterations: 153
currently lose_sum: 80.12246310710907
time_elpased: 2.252
batch start
#iterations: 154
currently lose_sum: 80.12312668561935
time_elpased: 2.241
batch start
#iterations: 155
currently lose_sum: 80.09223994612694
time_elpased: 2.253
batch start
#iterations: 156
currently lose_sum: 80.39700010418892
time_elpased: 2.223
batch start
#iterations: 157
currently lose_sum: 79.59526851773262
time_elpased: 2.268
batch start
#iterations: 158
currently lose_sum: 79.08321177959442
time_elpased: 2.263
batch start
#iterations: 159
currently lose_sum: 79.65679958462715
time_elpased: 2.247
start validation test
0.578917525773
0.61777236762
0.417824431409
0.49849591749
0.579200349547
81.822
batch start
#iterations: 160
currently lose_sum: 78.69540944695473
time_elpased: 2.241
batch start
#iterations: 161
currently lose_sum: 78.6829984486103
time_elpased: 2.247
batch start
#iterations: 162
currently lose_sum: 78.59500369429588
time_elpased: 2.274
batch start
#iterations: 163
currently lose_sum: 78.39440304040909
time_elpased: 2.279
batch start
#iterations: 164
currently lose_sum: 78.38068163394928
time_elpased: 2.235
batch start
#iterations: 165
currently lose_sum: 77.36026930809021
time_elpased: 2.244
batch start
#iterations: 166
currently lose_sum: 77.08566787838936
time_elpased: 2.235
batch start
#iterations: 167
currently lose_sum: 77.55807703733444
time_elpased: 2.276
batch start
#iterations: 168
currently lose_sum: 77.7769401371479
time_elpased: 2.228
batch start
#iterations: 169
currently lose_sum: 76.90928646922112
time_elpased: 2.273
batch start
#iterations: 170
currently lose_sum: 76.01182982325554
time_elpased: 2.262
batch start
#iterations: 171
currently lose_sum: 76.88525876402855
time_elpased: 2.254
batch start
#iterations: 172
currently lose_sum: 76.6184758245945
time_elpased: 2.203
batch start
#iterations: 173
currently lose_sum: 76.10929730534554
time_elpased: 2.206
batch start
#iterations: 174
currently lose_sum: 75.95056536793709
time_elpased: 2.253
batch start
#iterations: 175
currently lose_sum: 75.45160737633705
time_elpased: 2.25
batch start
#iterations: 176
currently lose_sum: 75.3554655611515
time_elpased: 2.274
batch start
#iterations: 177
currently lose_sum: 74.98360115289688
time_elpased: 2.212
batch start
#iterations: 178
currently lose_sum: 74.64116328954697
time_elpased: 2.266
batch start
#iterations: 179
currently lose_sum: 74.16450506448746
time_elpased: 2.297
start validation test
0.582628865979
0.62542582843
0.415663270557
0.499412673879
0.582921999835
85.492
batch start
#iterations: 180
currently lose_sum: 75.3395254611969
time_elpased: 2.236
batch start
#iterations: 181
currently lose_sum: 73.85341110825539
time_elpased: 2.276
batch start
#iterations: 182
currently lose_sum: 74.48508587479591
time_elpased: 2.199
batch start
#iterations: 183
currently lose_sum: 73.78903785347939
time_elpased: 2.218
batch start
#iterations: 184
currently lose_sum: 73.88529700040817
time_elpased: 2.258
batch start
#iterations: 185
currently lose_sum: 73.84811788797379
time_elpased: 2.285
batch start
#iterations: 186
currently lose_sum: 73.44605913758278
time_elpased: 2.248
batch start
#iterations: 187
currently lose_sum: 73.11369106173515
time_elpased: 2.275
batch start
#iterations: 188
currently lose_sum: 73.40086117386818
time_elpased: 2.271
batch start
#iterations: 189
currently lose_sum: 72.52903375029564
time_elpased: 2.26
batch start
#iterations: 190
currently lose_sum: 71.7450279891491
time_elpased: 2.252
batch start
#iterations: 191
currently lose_sum: 72.24299293756485
time_elpased: 2.24
batch start
#iterations: 192
currently lose_sum: 71.70209467411041
time_elpased: 2.263
batch start
#iterations: 193
currently lose_sum: 73.16492173075676
time_elpased: 2.283
batch start
#iterations: 194
currently lose_sum: 72.45882800221443
time_elpased: 2.261
batch start
#iterations: 195
currently lose_sum: 71.44896712899208
time_elpased: 2.261
batch start
#iterations: 196
currently lose_sum: 71.44737353920937
time_elpased: 2.23
batch start
#iterations: 197
currently lose_sum: 70.70231831073761
time_elpased: 2.293
batch start
#iterations: 198
currently lose_sum: 70.7063460946083
time_elpased: 2.242
batch start
#iterations: 199
currently lose_sum: 70.42782002687454
time_elpased: 2.277
start validation test
0.574536082474
0.605815130913
0.430997221365
0.503668069753
0.574788087084
100.015
batch start
#iterations: 200
currently lose_sum: 70.42947944998741
time_elpased: 2.239
batch start
#iterations: 201
currently lose_sum: 69.42575067281723
time_elpased: 2.224
batch start
#iterations: 202
currently lose_sum: 69.14908164739609
time_elpased: 2.234
batch start
#iterations: 203
currently lose_sum: 69.91601145267487
time_elpased: 2.255
batch start
#iterations: 204
currently lose_sum: 69.48008418083191
time_elpased: 2.262
batch start
#iterations: 205
currently lose_sum: 69.33718129992485
time_elpased: 2.263
batch start
#iterations: 206
currently lose_sum: 68.95700681209564
time_elpased: 2.266
batch start
#iterations: 207
currently lose_sum: 68.63480201363564
time_elpased: 2.257
batch start
#iterations: 208
currently lose_sum: 67.86663898825645
time_elpased: 2.215
batch start
#iterations: 209
currently lose_sum: 68.51571071147919
time_elpased: 2.236
batch start
#iterations: 210
currently lose_sum: 67.02446562051773
time_elpased: 2.264
batch start
#iterations: 211
currently lose_sum: 68.10802072286606
time_elpased: 2.297
batch start
#iterations: 212
currently lose_sum: 66.97267383337021
time_elpased: 2.23
batch start
#iterations: 213
currently lose_sum: 67.01500922441483
time_elpased: 2.228
batch start
#iterations: 214
currently lose_sum: 66.2955370247364
time_elpased: 2.238
batch start
#iterations: 215
currently lose_sum: 67.84170523285866
time_elpased: 2.271
batch start
#iterations: 216
currently lose_sum: 66.34243613481522
time_elpased: 2.268
batch start
#iterations: 217
currently lose_sum: 66.0425637960434
time_elpased: 2.223
batch start
#iterations: 218
currently lose_sum: 66.07553428411484
time_elpased: 2.224
batch start
#iterations: 219
currently lose_sum: 65.29188829660416
time_elpased: 2.252
start validation test
0.56881443299
0.620973514674
0.357106102707
0.453446586083
0.569186119617
109.892
batch start
#iterations: 220
currently lose_sum: 65.34865874052048
time_elpased: 2.256
batch start
#iterations: 221
currently lose_sum: 65.93498200178146
time_elpased: 2.282
batch start
#iterations: 222
currently lose_sum: 66.03318437933922
time_elpased: 2.237
batch start
#iterations: 223
currently lose_sum: 65.17708241939545
time_elpased: 2.263
batch start
#iterations: 224
currently lose_sum: 65.00870832800865
time_elpased: 2.235
batch start
#iterations: 225
currently lose_sum: 64.7889958024025
time_elpased: 2.268
batch start
#iterations: 226
currently lose_sum: 63.87687623500824
time_elpased: 2.28
batch start
#iterations: 227
currently lose_sum: 63.93434727191925
time_elpased: 2.206
batch start
#iterations: 228
currently lose_sum: 63.32860469818115
time_elpased: 2.28
batch start
#iterations: 229
currently lose_sum: 63.41988390684128
time_elpased: 2.25
batch start
#iterations: 230
currently lose_sum: 64.00387373566628
time_elpased: 2.296
batch start
#iterations: 231
currently lose_sum: 64.11448985338211
time_elpased: 2.297
batch start
#iterations: 232
currently lose_sum: 63.30675455927849
time_elpased: 2.275
batch start
#iterations: 233
currently lose_sum: 62.77041506767273
time_elpased: 2.207
batch start
#iterations: 234
currently lose_sum: 63.27263793349266
time_elpased: 2.239
batch start
#iterations: 235
currently lose_sum: 62.71486860513687
time_elpased: 2.269
batch start
#iterations: 236
currently lose_sum: 62.24782958626747
time_elpased: 2.277
batch start
#iterations: 237
currently lose_sum: 61.8317956328392
time_elpased: 2.251
batch start
#iterations: 238
currently lose_sum: 61.5147300362587
time_elpased: 2.234
batch start
#iterations: 239
currently lose_sum: 61.929396986961365
time_elpased: 2.243
start validation test
0.561340206186
0.583645183645
0.43336420706
0.497401370187
0.561564887791
120.423
batch start
#iterations: 240
currently lose_sum: 61.89622029662132
time_elpased: 2.243
batch start
#iterations: 241
currently lose_sum: 60.943685591220856
time_elpased: 2.256
batch start
#iterations: 242
currently lose_sum: 60.31388011574745
time_elpased: 2.255
batch start
#iterations: 243
currently lose_sum: 60.60826486349106
time_elpased: 2.287
batch start
#iterations: 244
currently lose_sum: 61.30613166093826
time_elpased: 2.256
batch start
#iterations: 245
currently lose_sum: 60.557883977890015
time_elpased: 2.242
batch start
#iterations: 246
currently lose_sum: 60.225395143032074
time_elpased: 2.265
batch start
#iterations: 247
currently lose_sum: 59.03561806678772
time_elpased: 2.288
batch start
#iterations: 248
currently lose_sum: 58.72671899199486
time_elpased: 2.235
batch start
#iterations: 249
currently lose_sum: 59.995138972997665
time_elpased: 2.228
batch start
#iterations: 250
currently lose_sum: 58.23648127913475
time_elpased: 2.25
batch start
#iterations: 251
currently lose_sum: 59.74496692419052
time_elpased: 2.282
batch start
#iterations: 252
currently lose_sum: 59.60052102804184
time_elpased: 2.256
batch start
#iterations: 253
currently lose_sum: 58.68439534306526
time_elpased: 2.233
batch start
#iterations: 254
currently lose_sum: 59.090231478214264
time_elpased: 2.234
batch start
#iterations: 255
currently lose_sum: 57.93694192171097
time_elpased: 2.233
batch start
#iterations: 256
currently lose_sum: 57.80032381415367
time_elpased: 2.235
batch start
#iterations: 257
currently lose_sum: 59.080697268247604
time_elpased: 2.279
batch start
#iterations: 258
currently lose_sum: 58.234341472387314
time_elpased: 2.278
batch start
#iterations: 259
currently lose_sum: 56.726129323244095
time_elpased: 2.226
start validation test
0.550515463918
0.591652877367
0.331172172481
0.42465030351
0.550900554897
147.153
batch start
#iterations: 260
currently lose_sum: 58.295633763074875
time_elpased: 2.244
batch start
#iterations: 261
currently lose_sum: 57.75220590829849
time_elpased: 2.238
batch start
#iterations: 262
currently lose_sum: 57.68150797486305
time_elpased: 2.255
batch start
#iterations: 263
currently lose_sum: 55.625670582056046
time_elpased: 2.26
batch start
#iterations: 264
currently lose_sum: 57.19547337293625
time_elpased: 2.267
batch start
#iterations: 265
currently lose_sum: 57.1486197412014
time_elpased: 2.221
batch start
#iterations: 266
currently lose_sum: 57.11155104637146
time_elpased: 2.184
batch start
#iterations: 267
currently lose_sum: 56.274404644966125
time_elpased: 2.247
batch start
#iterations: 268
currently lose_sum: 56.260511696338654
time_elpased: 2.277
batch start
#iterations: 269
currently lose_sum: 56.54016527533531
time_elpased: 2.251
batch start
#iterations: 270
currently lose_sum: 57.30675023794174
time_elpased: 2.216
batch start
#iterations: 271
currently lose_sum: 54.98939993977547
time_elpased: 2.216
batch start
#iterations: 272
currently lose_sum: 55.82981052994728
time_elpased: 2.227
batch start
#iterations: 273
currently lose_sum: 56.47125270962715
time_elpased: 2.249
batch start
#iterations: 274
currently lose_sum: 56.67642438411713
time_elpased: 2.266
batch start
#iterations: 275
currently lose_sum: 54.07704213261604
time_elpased: 2.248
batch start
#iterations: 276
currently lose_sum: 54.17996171116829
time_elpased: 2.215
batch start
#iterations: 277
currently lose_sum: 54.683780521154404
time_elpased: 2.252
batch start
#iterations: 278
currently lose_sum: 54.54293531179428
time_elpased: 2.245
batch start
#iterations: 279
currently lose_sum: 54.32738330960274
time_elpased: 2.258
start validation test
0.55412371134
0.600489734413
0.328084799835
0.424331159324
0.554520557513
147.076
batch start
#iterations: 280
currently lose_sum: 52.95712134242058
time_elpased: 2.221
batch start
#iterations: 281
currently lose_sum: 53.95708104968071
time_elpased: 2.235
batch start
#iterations: 282
currently lose_sum: 55.07389956712723
time_elpased: 2.284
batch start
#iterations: 283
currently lose_sum: 55.414521008729935
time_elpased: 2.212
batch start
#iterations: 284
currently lose_sum: 53.35660430788994
time_elpased: 2.275
batch start
#iterations: 285
currently lose_sum: 53.23593091964722
time_elpased: 2.228
batch start
#iterations: 286
currently lose_sum: 52.89132761955261
time_elpased: 2.243
batch start
#iterations: 287
currently lose_sum: 53.022142469882965
time_elpased: 2.247
batch start
#iterations: 288
currently lose_sum: 53.22873529791832
time_elpased: 2.272
batch start
#iterations: 289
currently lose_sum: 52.76253405213356
time_elpased: 2.243
batch start
#iterations: 290
currently lose_sum: 53.557346403598785
time_elpased: 2.224
batch start
#iterations: 291
currently lose_sum: 52.1436810195446
time_elpased: 2.265
batch start
#iterations: 292
currently lose_sum: 51.50233635306358
time_elpased: 2.251
batch start
#iterations: 293
currently lose_sum: 53.51678916811943
time_elpased: 2.219
batch start
#iterations: 294
currently lose_sum: 52.32878440618515
time_elpased: 2.2
batch start
#iterations: 295
currently lose_sum: 51.53927180171013
time_elpased: 2.215
batch start
#iterations: 296
currently lose_sum: 51.09507243335247
time_elpased: 2.223
batch start
#iterations: 297
currently lose_sum: 52.78878170251846
time_elpased: 2.289
batch start
#iterations: 298
currently lose_sum: 51.75639036297798
time_elpased: 2.232
batch start
#iterations: 299
currently lose_sum: 51.59373638033867
time_elpased: 2.268
start validation test
0.532422680412
0.603525641026
0.19378408974
0.293370725247
0.53301721269
182.177
batch start
#iterations: 300
currently lose_sum: 51.156046867370605
time_elpased: 2.268
batch start
#iterations: 301
currently lose_sum: 50.59689199924469
time_elpased: 2.211
batch start
#iterations: 302
currently lose_sum: 50.796998888254166
time_elpased: 2.252
batch start
#iterations: 303
currently lose_sum: 51.55765226483345
time_elpased: 2.271
batch start
#iterations: 304
currently lose_sum: 51.935759514570236
time_elpased: 2.277
batch start
#iterations: 305
currently lose_sum: 50.55268168449402
time_elpased: 2.259
batch start
#iterations: 306
currently lose_sum: 50.62141913175583
time_elpased: 2.248
batch start
#iterations: 307
currently lose_sum: 50.07733851671219
time_elpased: 2.227
batch start
#iterations: 308
currently lose_sum: 50.39553725719452
time_elpased: 2.25
batch start
#iterations: 309
currently lose_sum: 50.23446121811867
time_elpased: 2.268
batch start
#iterations: 310
currently lose_sum: 51.04679372906685
time_elpased: 2.28
batch start
#iterations: 311
currently lose_sum: 50.717063277959824
time_elpased: 2.276
batch start
#iterations: 312
currently lose_sum: 50.2695674598217
time_elpased: 2.231
batch start
#iterations: 313
currently lose_sum: 49.85178279876709
time_elpased: 2.225
batch start
#iterations: 314
currently lose_sum: 49.16737528145313
time_elpased: 2.232
batch start
#iterations: 315
currently lose_sum: 49.232206493616104
time_elpased: 2.263
batch start
#iterations: 316
currently lose_sum: 51.29577708244324
time_elpased: 2.224
batch start
#iterations: 317
currently lose_sum: 49.18587824702263
time_elpased: 2.256
batch start
#iterations: 318
currently lose_sum: 47.971880704164505
time_elpased: 2.2
batch start
#iterations: 319
currently lose_sum: 50.04221099615097
time_elpased: 2.234
start validation test
0.538402061856
0.582112068966
0.277966450551
0.376262450373
0.538859296741
189.244
batch start
#iterations: 320
currently lose_sum: 49.58237239718437
time_elpased: 2.253
batch start
#iterations: 321
currently lose_sum: 49.450273513793945
time_elpased: 2.289
batch start
#iterations: 322
currently lose_sum: 48.996312230825424
time_elpased: 2.238
batch start
#iterations: 323
currently lose_sum: 48.26640883088112
time_elpased: 2.245
batch start
#iterations: 324
currently lose_sum: 48.80241519212723
time_elpased: 2.228
batch start
#iterations: 325
currently lose_sum: 47.99281248450279
time_elpased: 2.251
batch start
#iterations: 326
currently lose_sum: 48.48474082350731
time_elpased: 2.255
batch start
#iterations: 327
currently lose_sum: 48.39354610443115
time_elpased: 2.238
batch start
#iterations: 328
currently lose_sum: 48.66638779640198
time_elpased: 2.191
batch start
#iterations: 329
currently lose_sum: 48.65197533369064
time_elpased: 2.238
batch start
#iterations: 330
currently lose_sum: 48.347845643758774
time_elpased: 2.243
batch start
#iterations: 331
currently lose_sum: 48.94104966521263
time_elpased: 2.28
batch start
#iterations: 332
currently lose_sum: 47.108729884028435
time_elpased: 2.218
batch start
#iterations: 333
currently lose_sum: 48.236200630664825
time_elpased: 2.25
batch start
#iterations: 334
currently lose_sum: 48.11245456337929
time_elpased: 2.215
batch start
#iterations: 335
currently lose_sum: 48.02107375860214
time_elpased: 2.298
batch start
#iterations: 336
currently lose_sum: 47.34498171508312
time_elpased: 2.241
batch start
#iterations: 337
currently lose_sum: 46.55027416348457
time_elpased: 2.208
batch start
#iterations: 338
currently lose_sum: 46.75801944732666
time_elpased: 2.247
batch start
#iterations: 339
currently lose_sum: 47.760514467954636
time_elpased: 2.264
start validation test
0.537319587629
0.579048431833
0.27930431203
0.376839766731
0.537772573241
190.635
batch start
#iterations: 340
currently lose_sum: 47.81060719490051
time_elpased: 2.203
batch start
#iterations: 341
currently lose_sum: 47.052731931209564
time_elpased: 2.266
batch start
#iterations: 342
currently lose_sum: 47.1180676817894
time_elpased: 2.24
batch start
#iterations: 343
currently lose_sum: 48.00481255352497
time_elpased: 2.258
batch start
#iterations: 344
currently lose_sum: 46.22170889377594
time_elpased: 2.231
batch start
#iterations: 345
currently lose_sum: 46.049470990896225
time_elpased: 2.277
batch start
#iterations: 346
currently lose_sum: 46.46216508746147
time_elpased: 2.253
batch start
#iterations: 347
currently lose_sum: 46.05407166481018
time_elpased: 2.243
batch start
#iterations: 348
currently lose_sum: 46.07846483588219
time_elpased: 2.229
batch start
#iterations: 349
currently lose_sum: 44.69785390794277
time_elpased: 2.279
batch start
#iterations: 350
currently lose_sum: 45.81210504472256
time_elpased: 2.272
batch start
#iterations: 351
currently lose_sum: 46.59252345561981
time_elpased: 2.236
batch start
#iterations: 352
currently lose_sum: 46.07304176688194
time_elpased: 2.261
batch start
#iterations: 353
currently lose_sum: 45.24661202728748
time_elpased: 2.271
batch start
#iterations: 354
currently lose_sum: 46.48664890229702
time_elpased: 2.256
batch start
#iterations: 355
currently lose_sum: 45.61578223109245
time_elpased: 2.253
batch start
#iterations: 356
currently lose_sum: 44.59214210510254
time_elpased: 2.238
batch start
#iterations: 357
currently lose_sum: 44.36779494583607
time_elpased: 2.294
batch start
#iterations: 358
currently lose_sum: 44.9511861205101
time_elpased: 2.236
batch start
#iterations: 359
currently lose_sum: 46.21804270148277
time_elpased: 2.271
start validation test
0.534587628866
0.589350649351
0.23350828445
0.334488096116
0.535116220093
202.645
batch start
#iterations: 360
currently lose_sum: 45.29030227661133
time_elpased: 2.279
batch start
#iterations: 361
currently lose_sum: 45.54003648459911
time_elpased: 2.286
batch start
#iterations: 362
currently lose_sum: 47.79201556742191
time_elpased: 2.241
batch start
#iterations: 363
currently lose_sum: 45.84375163912773
time_elpased: 2.284
batch start
#iterations: 364
currently lose_sum: 45.089832216501236
time_elpased: 2.22
batch start
#iterations: 365
currently lose_sum: 45.913916781544685
time_elpased: 2.282
batch start
#iterations: 366
currently lose_sum: 44.8181818574667
time_elpased: 2.24
batch start
#iterations: 367
currently lose_sum: 44.440386056900024
time_elpased: 2.219
batch start
#iterations: 368
currently lose_sum: 43.81745286285877
time_elpased: 2.303
batch start
#iterations: 369
currently lose_sum: 46.551118314266205
time_elpased: 2.25
batch start
#iterations: 370
currently lose_sum: 43.56405681371689
time_elpased: 2.264
batch start
#iterations: 371
currently lose_sum: 44.103364542126656
time_elpased: 2.252
batch start
#iterations: 372
currently lose_sum: 45.371847718954086
time_elpased: 2.214
batch start
#iterations: 373
currently lose_sum: 44.325965851545334
time_elpased: 2.238
batch start
#iterations: 374
currently lose_sum: 42.908512979745865
time_elpased: 2.255
batch start
#iterations: 375
currently lose_sum: 43.124635234475136
time_elpased: 2.235
batch start
#iterations: 376
currently lose_sum: 44.80112035572529
time_elpased: 2.281
batch start
#iterations: 377
currently lose_sum: 44.470440581440926
time_elpased: 2.259
batch start
#iterations: 378
currently lose_sum: 45.22456820309162
time_elpased: 2.229
batch start
#iterations: 379
currently lose_sum: 44.080639123916626
time_elpased: 2.285
start validation test
0.533865979381
0.57819025522
0.256457754451
0.355314750125
0.534353012308
215.052
batch start
#iterations: 380
currently lose_sum: 42.47798125445843
time_elpased: 2.242
batch start
#iterations: 381
currently lose_sum: 42.42347288131714
time_elpased: 2.277
batch start
#iterations: 382
currently lose_sum: 44.696554124355316
time_elpased: 2.277
batch start
#iterations: 383
currently lose_sum: 44.15435601770878
time_elpased: 2.319
batch start
#iterations: 384
currently lose_sum: 42.80387432873249
time_elpased: 2.271
batch start
#iterations: 385
currently lose_sum: 43.14017079770565
time_elpased: 2.283
batch start
#iterations: 386
currently lose_sum: 44.899021074175835
time_elpased: 2.249
batch start
#iterations: 387
currently lose_sum: 42.88939128816128
time_elpased: 2.291
batch start
#iterations: 388
currently lose_sum: 43.25452587008476
time_elpased: 2.241
batch start
#iterations: 389
currently lose_sum: 44.14201799035072
time_elpased: 2.251
batch start
#iterations: 390
currently lose_sum: 42.199273347854614
time_elpased: 2.276
batch start
#iterations: 391
currently lose_sum: 42.81600786745548
time_elpased: 2.229
batch start
#iterations: 392
currently lose_sum: 42.689266726374626
time_elpased: 2.229
batch start
#iterations: 393
currently lose_sum: 43.605550080537796
time_elpased: 2.347
batch start
#iterations: 394
currently lose_sum: 43.023427590727806
time_elpased: 2.287
batch start
#iterations: 395
currently lose_sum: 43.90608933568001
time_elpased: 2.251
batch start
#iterations: 396
currently lose_sum: 43.71976225078106
time_elpased: 2.299
batch start
#iterations: 397
currently lose_sum: 44.1571923494339
time_elpased: 2.229
batch start
#iterations: 398
currently lose_sum: 44.50150291621685
time_elpased: 2.219
batch start
#iterations: 399
currently lose_sum: 42.84717454016209
time_elpased: 2.331
start validation test
0.526804123711
0.556753329106
0.271071318308
0.364617940199
0.527253102095
242.157
acc: 0.624
pre: 0.609
rec: 0.696
F1: 0.650
auc: 0.666
