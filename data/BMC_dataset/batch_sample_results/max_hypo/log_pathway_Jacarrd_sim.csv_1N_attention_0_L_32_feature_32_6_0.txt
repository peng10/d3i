start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.29391247034073
time_elpased: 1.615
batch start
#iterations: 1
currently lose_sum: 100.06942516565323
time_elpased: 1.574
batch start
#iterations: 2
currently lose_sum: 100.08932518959045
time_elpased: 1.598
batch start
#iterations: 3
currently lose_sum: 99.66963690519333
time_elpased: 1.595
batch start
#iterations: 4
currently lose_sum: 99.61688756942749
time_elpased: 1.6
batch start
#iterations: 5
currently lose_sum: 99.4061473608017
time_elpased: 1.612
batch start
#iterations: 6
currently lose_sum: 99.10099029541016
time_elpased: 1.584
batch start
#iterations: 7
currently lose_sum: 98.90896081924438
time_elpased: 1.545
batch start
#iterations: 8
currently lose_sum: 98.79526275396347
time_elpased: 1.61
batch start
#iterations: 9
currently lose_sum: 98.6780298948288
time_elpased: 1.574
batch start
#iterations: 10
currently lose_sum: 98.33589321374893
time_elpased: 1.585
batch start
#iterations: 11
currently lose_sum: 98.18904012441635
time_elpased: 1.616
batch start
#iterations: 12
currently lose_sum: 98.17480850219727
time_elpased: 1.635
batch start
#iterations: 13
currently lose_sum: 97.96416711807251
time_elpased: 1.633
batch start
#iterations: 14
currently lose_sum: 97.68198192119598
time_elpased: 1.601
batch start
#iterations: 15
currently lose_sum: 97.32126462459564
time_elpased: 1.592
batch start
#iterations: 16
currently lose_sum: 97.40240037441254
time_elpased: 1.55
batch start
#iterations: 17
currently lose_sum: 97.34029567241669
time_elpased: 1.548
batch start
#iterations: 18
currently lose_sum: 96.90465486049652
time_elpased: 1.56
batch start
#iterations: 19
currently lose_sum: 96.88629841804504
time_elpased: 1.581
start validation test
0.609639175258
0.603574879227
0.642893897293
0.622614242288
0.609580791464
64.190
batch start
#iterations: 20
currently lose_sum: 96.79343736171722
time_elpased: 1.615
batch start
#iterations: 21
currently lose_sum: 96.50907135009766
time_elpased: 1.586
batch start
#iterations: 22
currently lose_sum: 96.7669386267662
time_elpased: 1.539
batch start
#iterations: 23
currently lose_sum: 96.8176776766777
time_elpased: 1.577
batch start
#iterations: 24
currently lose_sum: 96.44115900993347
time_elpased: 1.576
batch start
#iterations: 25
currently lose_sum: 96.32982397079468
time_elpased: 1.596
batch start
#iterations: 26
currently lose_sum: 96.2150005698204
time_elpased: 1.54
batch start
#iterations: 27
currently lose_sum: 95.75010180473328
time_elpased: 1.562
batch start
#iterations: 28
currently lose_sum: 95.92722344398499
time_elpased: 1.599
batch start
#iterations: 29
currently lose_sum: 96.07153505086899
time_elpased: 1.578
batch start
#iterations: 30
currently lose_sum: 95.37175196409225
time_elpased: 1.574
batch start
#iterations: 31
currently lose_sum: 95.41437947750092
time_elpased: 1.577
batch start
#iterations: 32
currently lose_sum: 95.35732209682465
time_elpased: 1.615
batch start
#iterations: 33
currently lose_sum: 95.17472010850906
time_elpased: 1.58
batch start
#iterations: 34
currently lose_sum: 95.55037754774094
time_elpased: 1.574
batch start
#iterations: 35
currently lose_sum: 95.09404981136322
time_elpased: 1.59
batch start
#iterations: 36
currently lose_sum: 95.58124655485153
time_elpased: 1.597
batch start
#iterations: 37
currently lose_sum: 94.8541379570961
time_elpased: 1.583
batch start
#iterations: 38
currently lose_sum: 94.8648299574852
time_elpased: 1.598
batch start
#iterations: 39
currently lose_sum: 94.93371319770813
time_elpased: 1.549
start validation test
0.630515463918
0.612439347155
0.714418030256
0.659509785294
0.630368160021
62.405
batch start
#iterations: 40
currently lose_sum: 94.8693864941597
time_elpased: 1.549
batch start
#iterations: 41
currently lose_sum: 95.12563562393188
time_elpased: 1.607
batch start
#iterations: 42
currently lose_sum: 94.66526424884796
time_elpased: 1.571
batch start
#iterations: 43
currently lose_sum: 94.76583021879196
time_elpased: 1.592
batch start
#iterations: 44
currently lose_sum: 94.7319848537445
time_elpased: 1.597
batch start
#iterations: 45
currently lose_sum: 94.20802241563797
time_elpased: 1.568
batch start
#iterations: 46
currently lose_sum: 94.40985280275345
time_elpased: 1.557
batch start
#iterations: 47
currently lose_sum: 94.36949902772903
time_elpased: 1.576
batch start
#iterations: 48
currently lose_sum: 94.56278151273727
time_elpased: 1.569
batch start
#iterations: 49
currently lose_sum: 94.39146918058395
time_elpased: 1.62
batch start
#iterations: 50
currently lose_sum: 94.86764752864838
time_elpased: 1.6
batch start
#iterations: 51
currently lose_sum: 94.00499296188354
time_elpased: 1.595
batch start
#iterations: 52
currently lose_sum: 94.05804979801178
time_elpased: 1.634
batch start
#iterations: 53
currently lose_sum: 93.91839975118637
time_elpased: 1.612
batch start
#iterations: 54
currently lose_sum: 94.2074384689331
time_elpased: 1.557
batch start
#iterations: 55
currently lose_sum: 93.95743590593338
time_elpased: 1.622
batch start
#iterations: 56
currently lose_sum: 93.93338626623154
time_elpased: 1.608
batch start
#iterations: 57
currently lose_sum: 93.60476404428482
time_elpased: 1.585
batch start
#iterations: 58
currently lose_sum: 93.78338301181793
time_elpased: 1.561
batch start
#iterations: 59
currently lose_sum: 93.94533824920654
time_elpased: 1.601
start validation test
0.630103092784
0.631589849819
0.627559946486
0.629568449308
0.630107557669
62.364
batch start
#iterations: 60
currently lose_sum: 94.08329874277115
time_elpased: 1.574
batch start
#iterations: 61
currently lose_sum: 93.69587886333466
time_elpased: 1.559
batch start
#iterations: 62
currently lose_sum: 93.65929210186005
time_elpased: 1.572
batch start
#iterations: 63
currently lose_sum: 93.8554984331131
time_elpased: 1.602
batch start
#iterations: 64
currently lose_sum: 93.74054372310638
time_elpased: 1.58
batch start
#iterations: 65
currently lose_sum: 93.23202908039093
time_elpased: 1.573
batch start
#iterations: 66
currently lose_sum: 93.34780251979828
time_elpased: 1.625
batch start
#iterations: 67
currently lose_sum: 93.01788365840912
time_elpased: 1.565
batch start
#iterations: 68
currently lose_sum: 93.06109994649887
time_elpased: 1.595
batch start
#iterations: 69
currently lose_sum: 93.62369322776794
time_elpased: 1.554
batch start
#iterations: 70
currently lose_sum: 92.91723728179932
time_elpased: 1.57
batch start
#iterations: 71
currently lose_sum: 92.9471989274025
time_elpased: 1.559
batch start
#iterations: 72
currently lose_sum: 92.81202220916748
time_elpased: 1.587
batch start
#iterations: 73
currently lose_sum: 92.6122636795044
time_elpased: 1.581
batch start
#iterations: 74
currently lose_sum: 92.6942303776741
time_elpased: 1.623
batch start
#iterations: 75
currently lose_sum: 93.2050267457962
time_elpased: 1.601
batch start
#iterations: 76
currently lose_sum: 92.28702127933502
time_elpased: 1.569
batch start
#iterations: 77
currently lose_sum: 93.01975893974304
time_elpased: 1.579
batch start
#iterations: 78
currently lose_sum: 92.87063068151474
time_elpased: 1.567
batch start
#iterations: 79
currently lose_sum: 92.76982021331787
time_elpased: 1.612
start validation test
0.615618556701
0.615518298916
0.619635690028
0.617570131802
0.615611504004
63.059
batch start
#iterations: 80
currently lose_sum: 92.3336683511734
time_elpased: 1.584
batch start
#iterations: 81
currently lose_sum: 93.11070877313614
time_elpased: 1.549
batch start
#iterations: 82
currently lose_sum: 92.2067911028862
time_elpased: 1.59
batch start
#iterations: 83
currently lose_sum: 92.63829237222672
time_elpased: 1.596
batch start
#iterations: 84
currently lose_sum: 92.4276230931282
time_elpased: 1.552
batch start
#iterations: 85
currently lose_sum: 92.52889657020569
time_elpased: 1.599
batch start
#iterations: 86
currently lose_sum: 92.66268014907837
time_elpased: 1.578
batch start
#iterations: 87
currently lose_sum: 92.53867012262344
time_elpased: 1.613
batch start
#iterations: 88
currently lose_sum: 92.65609300136566
time_elpased: 1.553
batch start
#iterations: 89
currently lose_sum: 92.35730308294296
time_elpased: 1.625
batch start
#iterations: 90
currently lose_sum: 92.28760397434235
time_elpased: 1.59
batch start
#iterations: 91
currently lose_sum: 92.2574292421341
time_elpased: 1.584
batch start
#iterations: 92
currently lose_sum: 92.19281589984894
time_elpased: 1.592
batch start
#iterations: 93
currently lose_sum: 92.2658417224884
time_elpased: 1.571
batch start
#iterations: 94
currently lose_sum: 92.0819000005722
time_elpased: 1.61
batch start
#iterations: 95
currently lose_sum: 92.51884883642197
time_elpased: 1.595
batch start
#iterations: 96
currently lose_sum: 92.00251090526581
time_elpased: 1.575
batch start
#iterations: 97
currently lose_sum: 92.32705056667328
time_elpased: 1.572
batch start
#iterations: 98
currently lose_sum: 92.05873453617096
time_elpased: 1.581
batch start
#iterations: 99
currently lose_sum: 91.95923435688019
time_elpased: 1.61
start validation test
0.625206185567
0.638067283811
0.581661006483
0.608559892328
0.625282635845
63.057
batch start
#iterations: 100
currently lose_sum: 91.75918465852737
time_elpased: 1.583
batch start
#iterations: 101
currently lose_sum: 91.83661139011383
time_elpased: 1.586
batch start
#iterations: 102
currently lose_sum: 91.6664366722107
time_elpased: 1.54
batch start
#iterations: 103
currently lose_sum: 91.69423222541809
time_elpased: 1.587
batch start
#iterations: 104
currently lose_sum: 91.5997822880745
time_elpased: 1.605
batch start
#iterations: 105
currently lose_sum: 91.22298574447632
time_elpased: 1.556
batch start
#iterations: 106
currently lose_sum: 91.75521969795227
time_elpased: 1.569
batch start
#iterations: 107
currently lose_sum: 91.83792650699615
time_elpased: 1.594
batch start
#iterations: 108
currently lose_sum: 91.1018654704094
time_elpased: 1.569
batch start
#iterations: 109
currently lose_sum: 91.05651861429214
time_elpased: 1.596
batch start
#iterations: 110
currently lose_sum: 91.90898483991623
time_elpased: 1.618
batch start
#iterations: 111
currently lose_sum: 91.50103437900543
time_elpased: 1.651
batch start
#iterations: 112
currently lose_sum: 91.87575000524521
time_elpased: 1.614
batch start
#iterations: 113
currently lose_sum: 91.18057578802109
time_elpased: 1.574
batch start
#iterations: 114
currently lose_sum: 91.88219743967056
time_elpased: 1.553
batch start
#iterations: 115
currently lose_sum: 91.16304749250412
time_elpased: 1.604
batch start
#iterations: 116
currently lose_sum: 91.22296351194382
time_elpased: 1.595
batch start
#iterations: 117
currently lose_sum: 91.34060913324356
time_elpased: 1.545
batch start
#iterations: 118
currently lose_sum: 91.28654366731644
time_elpased: 1.621
batch start
#iterations: 119
currently lose_sum: 90.91966009140015
time_elpased: 1.58
start validation test
0.621391752577
0.64435248296
0.544818359576
0.590419896281
0.621526188979
63.734
batch start
#iterations: 120
currently lose_sum: 91.71029794216156
time_elpased: 1.611
batch start
#iterations: 121
currently lose_sum: 91.17121529579163
time_elpased: 1.556
batch start
#iterations: 122
currently lose_sum: 91.08509588241577
time_elpased: 1.621
batch start
#iterations: 123
currently lose_sum: 91.20671200752258
time_elpased: 1.648
batch start
#iterations: 124
currently lose_sum: 91.7522297501564
time_elpased: 1.545
batch start
#iterations: 125
currently lose_sum: 91.43342953920364
time_elpased: 1.591
batch start
#iterations: 126
currently lose_sum: 91.3641345500946
time_elpased: 1.568
batch start
#iterations: 127
currently lose_sum: 91.1794223189354
time_elpased: 1.588
batch start
#iterations: 128
currently lose_sum: 90.94522351026535
time_elpased: 1.574
batch start
#iterations: 129
currently lose_sum: 90.74012666940689
time_elpased: 1.583
batch start
#iterations: 130
currently lose_sum: 90.67946362495422
time_elpased: 1.58
batch start
#iterations: 131
currently lose_sum: 90.69657999277115
time_elpased: 1.562
batch start
#iterations: 132
currently lose_sum: 90.86284750699997
time_elpased: 1.564
batch start
#iterations: 133
currently lose_sum: 90.86918061971664
time_elpased: 1.557
batch start
#iterations: 134
currently lose_sum: 90.29943281412125
time_elpased: 1.583
batch start
#iterations: 135
currently lose_sum: 91.08757853507996
time_elpased: 1.544
batch start
#iterations: 136
currently lose_sum: 91.07315665483475
time_elpased: 1.55
batch start
#iterations: 137
currently lose_sum: 90.42485326528549
time_elpased: 1.621
batch start
#iterations: 138
currently lose_sum: 90.24297022819519
time_elpased: 1.587
batch start
#iterations: 139
currently lose_sum: 90.81335002183914
time_elpased: 1.576
start validation test
0.622989690722
0.629151886488
0.602346403211
0.615457413249
0.623025933197
63.002
batch start
#iterations: 140
currently lose_sum: 90.75448650121689
time_elpased: 1.599
batch start
#iterations: 141
currently lose_sum: 90.85211771726608
time_elpased: 1.571
batch start
#iterations: 142
currently lose_sum: 90.54310750961304
time_elpased: 1.571
batch start
#iterations: 143
currently lose_sum: 90.8627900481224
time_elpased: 1.598
batch start
#iterations: 144
currently lose_sum: 90.01763671636581
time_elpased: 1.563
batch start
#iterations: 145
currently lose_sum: 90.50649976730347
time_elpased: 1.597
batch start
#iterations: 146
currently lose_sum: 90.48568445444107
time_elpased: 1.581
batch start
#iterations: 147
currently lose_sum: 90.46923619508743
time_elpased: 1.588
batch start
#iterations: 148
currently lose_sum: 90.4299259185791
time_elpased: 1.572
batch start
#iterations: 149
currently lose_sum: 90.89527398347855
time_elpased: 1.573
batch start
#iterations: 150
currently lose_sum: 90.37851256132126
time_elpased: 1.555
batch start
#iterations: 151
currently lose_sum: 90.8921047449112
time_elpased: 1.578
batch start
#iterations: 152
currently lose_sum: 89.98671120405197
time_elpased: 1.565
batch start
#iterations: 153
currently lose_sum: 90.25518828630447
time_elpased: 1.609
batch start
#iterations: 154
currently lose_sum: 90.39187306165695
time_elpased: 1.565
batch start
#iterations: 155
currently lose_sum: 90.4939751625061
time_elpased: 1.624
batch start
#iterations: 156
currently lose_sum: 89.79168558120728
time_elpased: 1.592
batch start
#iterations: 157
currently lose_sum: 89.84134459495544
time_elpased: 1.606
batch start
#iterations: 158
currently lose_sum: 90.8038677573204
time_elpased: 1.594
batch start
#iterations: 159
currently lose_sum: 90.57860070466995
time_elpased: 1.582
start validation test
0.601340206186
0.618899148579
0.531131007513
0.571665928223
0.601463469263
64.802
batch start
#iterations: 160
currently lose_sum: 90.37199121713638
time_elpased: 1.661
batch start
#iterations: 161
currently lose_sum: 89.796501994133
time_elpased: 1.614
batch start
#iterations: 162
currently lose_sum: 89.51475912332535
time_elpased: 1.551
batch start
#iterations: 163
currently lose_sum: 89.8847291469574
time_elpased: 1.601
batch start
#iterations: 164
currently lose_sum: 90.07386910915375
time_elpased: 1.599
batch start
#iterations: 165
currently lose_sum: 90.05897182226181
time_elpased: 1.605
batch start
#iterations: 166
currently lose_sum: 89.87265330553055
time_elpased: 1.58
batch start
#iterations: 167
currently lose_sum: 90.09499084949493
time_elpased: 1.606
batch start
#iterations: 168
currently lose_sum: 89.58829540014267
time_elpased: 1.632
batch start
#iterations: 169
currently lose_sum: 89.70671826601028
time_elpased: 1.57
batch start
#iterations: 170
currently lose_sum: 89.60506075620651
time_elpased: 1.573
batch start
#iterations: 171
currently lose_sum: 89.43132787942886
time_elpased: 1.579
batch start
#iterations: 172
currently lose_sum: 89.5097427368164
time_elpased: 1.598
batch start
#iterations: 173
currently lose_sum: 89.7442119717598
time_elpased: 1.567
batch start
#iterations: 174
currently lose_sum: 89.4726830124855
time_elpased: 1.57
batch start
#iterations: 175
currently lose_sum: 89.70648223161697
time_elpased: 1.576
batch start
#iterations: 176
currently lose_sum: 89.37142789363861
time_elpased: 1.574
batch start
#iterations: 177
currently lose_sum: 89.1840210556984
time_elpased: 1.614
batch start
#iterations: 178
currently lose_sum: 89.12001121044159
time_elpased: 1.55
batch start
#iterations: 179
currently lose_sum: 89.71369075775146
time_elpased: 1.57
start validation test
0.607731958763
0.607052128849
0.614798806216
0.610900910114
0.607719551822
64.821
batch start
#iterations: 180
currently lose_sum: 89.43997168540955
time_elpased: 1.592
batch start
#iterations: 181
currently lose_sum: 88.59000337123871
time_elpased: 1.579
batch start
#iterations: 182
currently lose_sum: 89.55978399515152
time_elpased: 1.561
batch start
#iterations: 183
currently lose_sum: 89.03877574205399
time_elpased: 1.593
batch start
#iterations: 184
currently lose_sum: 89.41636455059052
time_elpased: 1.642
batch start
#iterations: 185
currently lose_sum: 89.00074434280396
time_elpased: 1.57
batch start
#iterations: 186
currently lose_sum: 89.07140344381332
time_elpased: 1.575
batch start
#iterations: 187
currently lose_sum: 89.6683618426323
time_elpased: 1.597
batch start
#iterations: 188
currently lose_sum: 89.13208001852036
time_elpased: 1.613
batch start
#iterations: 189
currently lose_sum: 88.65012121200562
time_elpased: 1.594
batch start
#iterations: 190
currently lose_sum: 89.19097185134888
time_elpased: 1.588
batch start
#iterations: 191
currently lose_sum: 88.35233300924301
time_elpased: 1.535
batch start
#iterations: 192
currently lose_sum: 88.80647331476212
time_elpased: 1.557
batch start
#iterations: 193
currently lose_sum: 88.95596992969513
time_elpased: 1.559
batch start
#iterations: 194
currently lose_sum: 88.2793487906456
time_elpased: 1.571
batch start
#iterations: 195
currently lose_sum: 88.61729425191879
time_elpased: 1.589
batch start
#iterations: 196
currently lose_sum: 88.51115250587463
time_elpased: 1.552
batch start
#iterations: 197
currently lose_sum: 88.32715886831284
time_elpased: 1.548
batch start
#iterations: 198
currently lose_sum: 89.0307412147522
time_elpased: 1.55
batch start
#iterations: 199
currently lose_sum: 89.15961891412735
time_elpased: 1.552
start validation test
0.596597938144
0.617584877503
0.511063085314
0.559297218155
0.596748107771
67.197
batch start
#iterations: 200
currently lose_sum: 89.13293391466141
time_elpased: 1.615
batch start
#iterations: 201
currently lose_sum: 88.50932329893112
time_elpased: 1.565
batch start
#iterations: 202
currently lose_sum: 88.87322920560837
time_elpased: 1.553
batch start
#iterations: 203
currently lose_sum: 88.52055060863495
time_elpased: 1.573
batch start
#iterations: 204
currently lose_sum: 88.76140481233597
time_elpased: 1.574
batch start
#iterations: 205
currently lose_sum: 88.58063596487045
time_elpased: 1.543
batch start
#iterations: 206
currently lose_sum: 88.5420354604721
time_elpased: 1.609
batch start
#iterations: 207
currently lose_sum: 88.21850556135178
time_elpased: 1.528
batch start
#iterations: 208
currently lose_sum: 88.39617085456848
time_elpased: 1.571
batch start
#iterations: 209
currently lose_sum: 88.25260776281357
time_elpased: 1.562
batch start
#iterations: 210
currently lose_sum: 88.06415635347366
time_elpased: 1.552
batch start
#iterations: 211
currently lose_sum: 88.38260698318481
time_elpased: 1.565
batch start
#iterations: 212
currently lose_sum: 88.01790994405746
time_elpased: 1.575
batch start
#iterations: 213
currently lose_sum: 87.74737668037415
time_elpased: 1.552
batch start
#iterations: 214
currently lose_sum: 87.61964678764343
time_elpased: 1.592
batch start
#iterations: 215
currently lose_sum: 88.01499956846237
time_elpased: 1.568
batch start
#iterations: 216
currently lose_sum: 87.86663323640823
time_elpased: 1.535
batch start
#iterations: 217
currently lose_sum: 87.6217451095581
time_elpased: 1.594
batch start
#iterations: 218
currently lose_sum: 88.34216803312302
time_elpased: 1.614
batch start
#iterations: 219
currently lose_sum: 88.37869399785995
time_elpased: 1.551
start validation test
0.60706185567
0.625932162617
0.535556241638
0.577228107149
0.607187394805
67.648
batch start
#iterations: 220
currently lose_sum: 88.47172915935516
time_elpased: 1.538
batch start
#iterations: 221
currently lose_sum: 88.10685402154922
time_elpased: 1.551
batch start
#iterations: 222
currently lose_sum: 88.00994896888733
time_elpased: 1.571
batch start
#iterations: 223
currently lose_sum: 87.96730375289917
time_elpased: 1.594
batch start
#iterations: 224
currently lose_sum: 88.56268501281738
time_elpased: 1.581
batch start
#iterations: 225
currently lose_sum: 88.01805186271667
time_elpased: 1.556
batch start
#iterations: 226
currently lose_sum: 88.08570694923401
time_elpased: 1.579
batch start
#iterations: 227
currently lose_sum: 87.59487599134445
time_elpased: 1.579
batch start
#iterations: 228
currently lose_sum: 87.68115890026093
time_elpased: 1.572
batch start
#iterations: 229
currently lose_sum: 87.68114948272705
time_elpased: 1.513
batch start
#iterations: 230
currently lose_sum: 87.38294786214828
time_elpased: 1.567
batch start
#iterations: 231
currently lose_sum: 87.14829194545746
time_elpased: 1.541
batch start
#iterations: 232
currently lose_sum: 87.45314002037048
time_elpased: 1.549
batch start
#iterations: 233
currently lose_sum: 87.48987120389938
time_elpased: 1.544
batch start
#iterations: 234
currently lose_sum: 87.72696131467819
time_elpased: 1.559
batch start
#iterations: 235
currently lose_sum: 86.98752349615097
time_elpased: 1.58
batch start
#iterations: 236
currently lose_sum: 87.56946396827698
time_elpased: 1.552
batch start
#iterations: 237
currently lose_sum: 88.05755591392517
time_elpased: 1.577
batch start
#iterations: 238
currently lose_sum: 87.35513889789581
time_elpased: 1.547
batch start
#iterations: 239
currently lose_sum: 87.4791077375412
time_elpased: 1.566
start validation test
0.608711340206
0.611262298514
0.601008541731
0.60609205542
0.608724863657
66.847
batch start
#iterations: 240
currently lose_sum: 87.90675389766693
time_elpased: 1.546
batch start
#iterations: 241
currently lose_sum: 87.69428336620331
time_elpased: 1.559
batch start
#iterations: 242
currently lose_sum: 87.09484314918518
time_elpased: 1.596
batch start
#iterations: 243
currently lose_sum: 87.04668241739273
time_elpased: 1.538
batch start
#iterations: 244
currently lose_sum: 87.32074850797653
time_elpased: 1.561
batch start
#iterations: 245
currently lose_sum: 87.1839856505394
time_elpased: 1.537
batch start
#iterations: 246
currently lose_sum: 87.45806515216827
time_elpased: 1.553
batch start
#iterations: 247
currently lose_sum: 87.00863122940063
time_elpased: 1.578
batch start
#iterations: 248
currently lose_sum: 87.38159334659576
time_elpased: 1.528
batch start
#iterations: 249
currently lose_sum: 87.29636710882187
time_elpased: 1.565
batch start
#iterations: 250
currently lose_sum: 87.6340399980545
time_elpased: 1.559
batch start
#iterations: 251
currently lose_sum: 86.47966295480728
time_elpased: 1.561
batch start
#iterations: 252
currently lose_sum: 87.1805191040039
time_elpased: 1.558
batch start
#iterations: 253
currently lose_sum: 87.26051270961761
time_elpased: 1.545
batch start
#iterations: 254
currently lose_sum: 86.98260706663132
time_elpased: 1.555
batch start
#iterations: 255
currently lose_sum: 86.80413043498993
time_elpased: 1.558
batch start
#iterations: 256
currently lose_sum: 87.30279123783112
time_elpased: 1.528
batch start
#iterations: 257
currently lose_sum: 87.70539432764053
time_elpased: 1.568
batch start
#iterations: 258
currently lose_sum: 86.72183161973953
time_elpased: 1.61
batch start
#iterations: 259
currently lose_sum: 86.89518910646439
time_elpased: 1.574
start validation test
0.607886597938
0.61790344211
0.569002778635
0.592445754085
0.60795486448
66.400
batch start
#iterations: 260
currently lose_sum: 86.52162277698517
time_elpased: 1.583
batch start
#iterations: 261
currently lose_sum: 86.98686158657074
time_elpased: 1.573
batch start
#iterations: 262
currently lose_sum: 87.10865479707718
time_elpased: 1.574
batch start
#iterations: 263
currently lose_sum: 86.75400066375732
time_elpased: 1.565
batch start
#iterations: 264
currently lose_sum: 86.89445143938065
time_elpased: 1.598
batch start
#iterations: 265
currently lose_sum: 86.39285582304001
time_elpased: 1.575
batch start
#iterations: 266
currently lose_sum: 87.14792943000793
time_elpased: 1.624
batch start
#iterations: 267
currently lose_sum: 86.57789742946625
time_elpased: 1.573
batch start
#iterations: 268
currently lose_sum: 87.26632928848267
time_elpased: 1.589
batch start
#iterations: 269
currently lose_sum: 86.41694736480713
time_elpased: 1.585
batch start
#iterations: 270
currently lose_sum: 86.93391340970993
time_elpased: 1.628
batch start
#iterations: 271
currently lose_sum: 86.37952089309692
time_elpased: 1.565
batch start
#iterations: 272
currently lose_sum: 85.59725314378738
time_elpased: 1.58
batch start
#iterations: 273
currently lose_sum: 86.84419399499893
time_elpased: 1.557
batch start
#iterations: 274
currently lose_sum: 86.04891204833984
time_elpased: 1.563
batch start
#iterations: 275
currently lose_sum: 86.6880972981453
time_elpased: 1.572
batch start
#iterations: 276
currently lose_sum: 86.43569964170456
time_elpased: 1.562
batch start
#iterations: 277
currently lose_sum: 85.59174573421478
time_elpased: 1.578
batch start
#iterations: 278
currently lose_sum: 86.32313793897629
time_elpased: 1.545
batch start
#iterations: 279
currently lose_sum: 85.80880266427994
time_elpased: 1.646
start validation test
0.611030927835
0.61509914113
0.596994957291
0.605911844579
0.611055570146
67.745
batch start
#iterations: 280
currently lose_sum: 86.42136251926422
time_elpased: 1.585
batch start
#iterations: 281
currently lose_sum: 86.54651647806168
time_elpased: 1.547
batch start
#iterations: 282
currently lose_sum: 85.89661157131195
time_elpased: 1.602
batch start
#iterations: 283
currently lose_sum: 86.06372666358948
time_elpased: 1.557
batch start
#iterations: 284
currently lose_sum: 86.04854327440262
time_elpased: 1.602
batch start
#iterations: 285
currently lose_sum: 85.8269716501236
time_elpased: 1.557
batch start
#iterations: 286
currently lose_sum: 85.69280207157135
time_elpased: 1.56
batch start
#iterations: 287
currently lose_sum: 85.90621131658554
time_elpased: 1.552
batch start
#iterations: 288
currently lose_sum: 85.75892680883408
time_elpased: 1.553
batch start
#iterations: 289
currently lose_sum: 85.42896807193756
time_elpased: 1.555
batch start
#iterations: 290
currently lose_sum: 86.16357618570328
time_elpased: 1.56
batch start
#iterations: 291
currently lose_sum: 85.33655422925949
time_elpased: 1.551
batch start
#iterations: 292
currently lose_sum: 86.07262355089188
time_elpased: 1.55
batch start
#iterations: 293
currently lose_sum: 85.47681564092636
time_elpased: 1.592
batch start
#iterations: 294
currently lose_sum: 85.08858239650726
time_elpased: 1.596
batch start
#iterations: 295
currently lose_sum: 85.10475695133209
time_elpased: 1.596
batch start
#iterations: 296
currently lose_sum: 85.63786500692368
time_elpased: 1.627
batch start
#iterations: 297
currently lose_sum: 85.72302037477493
time_elpased: 1.574
batch start
#iterations: 298
currently lose_sum: 85.7561885714531
time_elpased: 1.551
batch start
#iterations: 299
currently lose_sum: 85.55778908729553
time_elpased: 1.562
start validation test
0.601804123711
0.606206014075
0.585057116394
0.595443833464
0.601833525666
68.328
batch start
#iterations: 300
currently lose_sum: 85.69917422533035
time_elpased: 1.555
batch start
#iterations: 301
currently lose_sum: 85.86057925224304
time_elpased: 1.582
batch start
#iterations: 302
currently lose_sum: 85.44756436347961
time_elpased: 1.598
batch start
#iterations: 303
currently lose_sum: 85.6065583229065
time_elpased: 1.555
batch start
#iterations: 304
currently lose_sum: 85.34483706951141
time_elpased: 1.541
batch start
#iterations: 305
currently lose_sum: 84.93187010288239
time_elpased: 1.561
batch start
#iterations: 306
currently lose_sum: 85.70425432920456
time_elpased: 1.597
batch start
#iterations: 307
currently lose_sum: 84.70441499352455
time_elpased: 1.567
batch start
#iterations: 308
currently lose_sum: 85.460215985775
time_elpased: 1.565
batch start
#iterations: 309
currently lose_sum: 85.3811166882515
time_elpased: 1.562
batch start
#iterations: 310
currently lose_sum: 85.48080313205719
time_elpased: 1.583
batch start
#iterations: 311
currently lose_sum: 85.14576977491379
time_elpased: 1.6
batch start
#iterations: 312
currently lose_sum: 85.71336501836777
time_elpased: 1.596
batch start
#iterations: 313
currently lose_sum: 84.75229525566101
time_elpased: 1.581
batch start
#iterations: 314
currently lose_sum: 85.02244329452515
time_elpased: 1.586
batch start
#iterations: 315
currently lose_sum: 85.12081903219223
time_elpased: 1.595
batch start
#iterations: 316
currently lose_sum: 85.18044304847717
time_elpased: 1.624
batch start
#iterations: 317
currently lose_sum: 85.00552016496658
time_elpased: 1.568
batch start
#iterations: 318
currently lose_sum: 85.76197504997253
time_elpased: 1.585
batch start
#iterations: 319
currently lose_sum: 85.03966212272644
time_elpased: 1.571
start validation test
0.584742268041
0.607508090615
0.482967994237
0.538126361656
0.584920948477
70.186
batch start
#iterations: 320
currently lose_sum: 85.07297837734222
time_elpased: 1.614
batch start
#iterations: 321
currently lose_sum: 84.71435910463333
time_elpased: 1.591
batch start
#iterations: 322
currently lose_sum: 84.93333706259727
time_elpased: 1.568
batch start
#iterations: 323
currently lose_sum: 84.848259806633
time_elpased: 1.545
batch start
#iterations: 324
currently lose_sum: 85.49710804224014
time_elpased: 1.555
batch start
#iterations: 325
currently lose_sum: 84.6873037815094
time_elpased: 1.54
batch start
#iterations: 326
currently lose_sum: 84.88294011354446
time_elpased: 1.585
batch start
#iterations: 327
currently lose_sum: 84.79620558023453
time_elpased: 1.597
batch start
#iterations: 328
currently lose_sum: 84.65481397509575
time_elpased: 1.537
batch start
#iterations: 329
currently lose_sum: 84.88824355602264
time_elpased: 1.601
batch start
#iterations: 330
currently lose_sum: 84.39763033390045
time_elpased: 1.593
batch start
#iterations: 331
currently lose_sum: 84.79297152161598
time_elpased: 1.57
batch start
#iterations: 332
currently lose_sum: 84.02199357748032
time_elpased: 1.601
batch start
#iterations: 333
currently lose_sum: 84.67791119217873
time_elpased: 1.591
batch start
#iterations: 334
currently lose_sum: 85.12249076366425
time_elpased: 1.547
batch start
#iterations: 335
currently lose_sum: 84.7522075176239
time_elpased: 1.598
batch start
#iterations: 336
currently lose_sum: 84.02719739079475
time_elpased: 1.572
batch start
#iterations: 337
currently lose_sum: 84.56534856557846
time_elpased: 1.575
batch start
#iterations: 338
currently lose_sum: 84.29545480012894
time_elpased: 1.56
batch start
#iterations: 339
currently lose_sum: 84.51027232408524
time_elpased: 1.587
start validation test
0.601340206186
0.617741360884
0.535350416795
0.57360238174
0.601456061439
69.668
batch start
#iterations: 340
currently lose_sum: 84.2041517496109
time_elpased: 1.56
batch start
#iterations: 341
currently lose_sum: 84.35432302951813
time_elpased: 1.586
batch start
#iterations: 342
currently lose_sum: 84.41195487976074
time_elpased: 1.588
batch start
#iterations: 343
currently lose_sum: 84.01091134548187
time_elpased: 1.57
batch start
#iterations: 344
currently lose_sum: 84.2494261264801
time_elpased: 1.586
batch start
#iterations: 345
currently lose_sum: 84.03533267974854
time_elpased: 1.573
batch start
#iterations: 346
currently lose_sum: 84.13669073581696
time_elpased: 1.587
batch start
#iterations: 347
currently lose_sum: 84.12297350168228
time_elpased: 1.534
batch start
#iterations: 348
currently lose_sum: 83.92758125066757
time_elpased: 1.558
batch start
#iterations: 349
currently lose_sum: 84.52666127681732
time_elpased: 1.59
batch start
#iterations: 350
currently lose_sum: 84.13292396068573
time_elpased: 1.573
batch start
#iterations: 351
currently lose_sum: 83.52351519465446
time_elpased: 1.58
batch start
#iterations: 352
currently lose_sum: 83.38778394460678
time_elpased: 1.594
batch start
#iterations: 353
currently lose_sum: 84.4432367682457
time_elpased: 1.576
batch start
#iterations: 354
currently lose_sum: 84.2198166847229
time_elpased: 1.563
batch start
#iterations: 355
currently lose_sum: 83.63057547807693
time_elpased: 1.552
batch start
#iterations: 356
currently lose_sum: 83.91029745340347
time_elpased: 1.575
batch start
#iterations: 357
currently lose_sum: 83.43247410655022
time_elpased: 1.588
batch start
#iterations: 358
currently lose_sum: 84.18734958767891
time_elpased: 1.588
batch start
#iterations: 359
currently lose_sum: 83.21583819389343
time_elpased: 1.574
start validation test
0.594175257732
0.631040363843
0.45693115159
0.530054318629
0.594416210929
71.919
batch start
#iterations: 360
currently lose_sum: 83.62231212854385
time_elpased: 1.585
batch start
#iterations: 361
currently lose_sum: 84.17665028572083
time_elpased: 1.561
batch start
#iterations: 362
currently lose_sum: 83.92546850442886
time_elpased: 1.579
batch start
#iterations: 363
currently lose_sum: 83.67877697944641
time_elpased: 1.542
batch start
#iterations: 364
currently lose_sum: 84.1644759774208
time_elpased: 1.581
batch start
#iterations: 365
currently lose_sum: 83.92147359251976
time_elpased: 1.629
batch start
#iterations: 366
currently lose_sum: 83.6293334364891
time_elpased: 1.576
batch start
#iterations: 367
currently lose_sum: 83.58993059396744
time_elpased: 1.575
batch start
#iterations: 368
currently lose_sum: 83.7884908914566
time_elpased: 1.64
batch start
#iterations: 369
currently lose_sum: 83.4431063234806
time_elpased: 1.579
batch start
#iterations: 370
currently lose_sum: 83.1347925066948
time_elpased: 1.603
batch start
#iterations: 371
currently lose_sum: 83.07268142700195
time_elpased: 1.586
batch start
#iterations: 372
currently lose_sum: 83.00809586048126
time_elpased: 1.554
batch start
#iterations: 373
currently lose_sum: 83.08404517173767
time_elpased: 1.548
batch start
#iterations: 374
currently lose_sum: 83.1931562423706
time_elpased: 1.647
batch start
#iterations: 375
currently lose_sum: 82.79536336660385
time_elpased: 1.561
batch start
#iterations: 376
currently lose_sum: 82.76025420427322
time_elpased: 1.61
batch start
#iterations: 377
currently lose_sum: 83.2164694070816
time_elpased: 1.558
batch start
#iterations: 378
currently lose_sum: 83.1876711845398
time_elpased: 1.597
batch start
#iterations: 379
currently lose_sum: 83.34683546423912
time_elpased: 1.575
start validation test
0.58793814433
0.601103156906
0.527117423073
0.561684395219
0.588044924487
72.967
batch start
#iterations: 380
currently lose_sum: 83.21188360452652
time_elpased: 1.533
batch start
#iterations: 381
currently lose_sum: 83.19175893068314
time_elpased: 1.559
batch start
#iterations: 382
currently lose_sum: 82.99803304672241
time_elpased: 1.579
batch start
#iterations: 383
currently lose_sum: 83.17292088270187
time_elpased: 1.665
batch start
#iterations: 384
currently lose_sum: 83.27327135205269
time_elpased: 1.574
batch start
#iterations: 385
currently lose_sum: 82.8361841738224
time_elpased: 1.565
batch start
#iterations: 386
currently lose_sum: 82.64062631130219
time_elpased: 1.586
batch start
#iterations: 387
currently lose_sum: 83.05138683319092
time_elpased: 1.55
batch start
#iterations: 388
currently lose_sum: 83.24430179595947
time_elpased: 1.581
batch start
#iterations: 389
currently lose_sum: 83.11568406224251
time_elpased: 1.572
batch start
#iterations: 390
currently lose_sum: 82.58626586198807
time_elpased: 1.566
batch start
#iterations: 391
currently lose_sum: 82.62096500396729
time_elpased: 1.594
batch start
#iterations: 392
currently lose_sum: 82.5006075501442
time_elpased: 1.566
batch start
#iterations: 393
currently lose_sum: 82.99941217899323
time_elpased: 1.61
batch start
#iterations: 394
currently lose_sum: 82.51423490047455
time_elpased: 1.569
batch start
#iterations: 395
currently lose_sum: 82.87844154238701
time_elpased: 1.594
batch start
#iterations: 396
currently lose_sum: 82.81176435947418
time_elpased: 1.58
batch start
#iterations: 397
currently lose_sum: 82.36017951369286
time_elpased: 1.583
batch start
#iterations: 398
currently lose_sum: 82.52130502462387
time_elpased: 1.58
batch start
#iterations: 399
currently lose_sum: 82.90079566836357
time_elpased: 1.538
start validation test
0.592113402062
0.614496064991
0.498199032623
0.550269963058
0.592278283223
73.098
acc: 0.630
pre: 0.631
rec: 0.628
F1: 0.630
auc: 0.680
