start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.7747637629509
time_elpased: 1.444
batch start
#iterations: 1
currently lose_sum: 99.99551123380661
time_elpased: 1.474
batch start
#iterations: 2
currently lose_sum: 99.81057250499725
time_elpased: 1.424
batch start
#iterations: 3
currently lose_sum: 99.63484591245651
time_elpased: 1.481
batch start
#iterations: 4
currently lose_sum: 99.26014715433121
time_elpased: 1.421
batch start
#iterations: 5
currently lose_sum: 98.97724670171738
time_elpased: 1.419
batch start
#iterations: 6
currently lose_sum: 98.7571496963501
time_elpased: 1.42
batch start
#iterations: 7
currently lose_sum: 98.80369246006012
time_elpased: 1.424
batch start
#iterations: 8
currently lose_sum: 98.67944097518921
time_elpased: 1.451
batch start
#iterations: 9
currently lose_sum: 98.51880079507828
time_elpased: 1.44
batch start
#iterations: 10
currently lose_sum: 98.53498578071594
time_elpased: 1.458
batch start
#iterations: 11
currently lose_sum: 98.17495846748352
time_elpased: 1.427
batch start
#iterations: 12
currently lose_sum: 98.63471323251724
time_elpased: 1.444
batch start
#iterations: 13
currently lose_sum: 98.30363595485687
time_elpased: 1.486
batch start
#iterations: 14
currently lose_sum: 97.87062102556229
time_elpased: 1.43
batch start
#iterations: 15
currently lose_sum: 97.7834095954895
time_elpased: 1.422
batch start
#iterations: 16
currently lose_sum: 97.76815700531006
time_elpased: 1.43
batch start
#iterations: 17
currently lose_sum: 98.01583802700043
time_elpased: 1.443
batch start
#iterations: 18
currently lose_sum: 97.62787413597107
time_elpased: 1.439
batch start
#iterations: 19
currently lose_sum: 97.54600149393082
time_elpased: 1.436
start validation test
0.613659793814
0.624356391314
0.57404548729
0.598144871589
0.61372934284
63.985
batch start
#iterations: 20
currently lose_sum: 97.8876223564148
time_elpased: 1.448
batch start
#iterations: 21
currently lose_sum: 97.53336727619171
time_elpased: 1.446
batch start
#iterations: 22
currently lose_sum: 97.30536955595016
time_elpased: 1.422
batch start
#iterations: 23
currently lose_sum: 97.55131822824478
time_elpased: 1.476
batch start
#iterations: 24
currently lose_sum: 97.16924858093262
time_elpased: 1.43
batch start
#iterations: 25
currently lose_sum: 97.53753393888474
time_elpased: 1.427
batch start
#iterations: 26
currently lose_sum: 97.29240590333939
time_elpased: 1.424
batch start
#iterations: 27
currently lose_sum: 97.16362822055817
time_elpased: 1.43
batch start
#iterations: 28
currently lose_sum: 96.7368438243866
time_elpased: 1.442
batch start
#iterations: 29
currently lose_sum: 97.48349559307098
time_elpased: 1.463
batch start
#iterations: 30
currently lose_sum: 96.85553985834122
time_elpased: 1.425
batch start
#iterations: 31
currently lose_sum: 96.86602795124054
time_elpased: 1.468
batch start
#iterations: 32
currently lose_sum: 96.79661673307419
time_elpased: 1.442
batch start
#iterations: 33
currently lose_sum: 96.50981879234314
time_elpased: 1.43
batch start
#iterations: 34
currently lose_sum: 96.65758806467056
time_elpased: 1.417
batch start
#iterations: 35
currently lose_sum: 96.75452250242233
time_elpased: 1.431
batch start
#iterations: 36
currently lose_sum: 96.64214324951172
time_elpased: 1.408
batch start
#iterations: 37
currently lose_sum: 96.34153807163239
time_elpased: 1.472
batch start
#iterations: 38
currently lose_sum: 96.16707682609558
time_elpased: 1.438
batch start
#iterations: 39
currently lose_sum: 96.23300725221634
time_elpased: 1.478
start validation test
0.614793814433
0.623134328358
0.584336729443
0.603112220511
0.614847286543
63.444
batch start
#iterations: 40
currently lose_sum: 96.3817378282547
time_elpased: 1.453
batch start
#iterations: 41
currently lose_sum: 96.00661617517471
time_elpased: 1.441
batch start
#iterations: 42
currently lose_sum: 96.09756511449814
time_elpased: 1.459
batch start
#iterations: 43
currently lose_sum: 95.81662458181381
time_elpased: 1.42
batch start
#iterations: 44
currently lose_sum: 95.92672348022461
time_elpased: 1.433
batch start
#iterations: 45
currently lose_sum: 95.46839147806168
time_elpased: 1.48
batch start
#iterations: 46
currently lose_sum: 95.79718059301376
time_elpased: 1.415
batch start
#iterations: 47
currently lose_sum: 95.77035391330719
time_elpased: 1.475
batch start
#iterations: 48
currently lose_sum: 96.05352199077606
time_elpased: 1.436
batch start
#iterations: 49
currently lose_sum: 95.76374042034149
time_elpased: 1.475
batch start
#iterations: 50
currently lose_sum: 96.06723308563232
time_elpased: 1.446
batch start
#iterations: 51
currently lose_sum: 95.49629819393158
time_elpased: 1.446
batch start
#iterations: 52
currently lose_sum: 95.47116965055466
time_elpased: 1.447
batch start
#iterations: 53
currently lose_sum: 95.92808103561401
time_elpased: 1.462
batch start
#iterations: 54
currently lose_sum: 95.44800621271133
time_elpased: 1.459
batch start
#iterations: 55
currently lose_sum: 95.20418572425842
time_elpased: 1.428
batch start
#iterations: 56
currently lose_sum: 95.36014944314957
time_elpased: 1.436
batch start
#iterations: 57
currently lose_sum: 95.220898270607
time_elpased: 1.422
batch start
#iterations: 58
currently lose_sum: 95.14863383769989
time_elpased: 1.439
batch start
#iterations: 59
currently lose_sum: 95.35479658842087
time_elpased: 1.454
start validation test
0.618092783505
0.610706062932
0.655140475455
0.632143389107
0.618027740568
63.352
batch start
#iterations: 60
currently lose_sum: 95.1000143289566
time_elpased: 1.454
batch start
#iterations: 61
currently lose_sum: 95.35721385478973
time_elpased: 1.438
batch start
#iterations: 62
currently lose_sum: 94.93787151575089
time_elpased: 1.421
batch start
#iterations: 63
currently lose_sum: 95.06566089391708
time_elpased: 1.452
batch start
#iterations: 64
currently lose_sum: 94.7451342344284
time_elpased: 1.433
batch start
#iterations: 65
currently lose_sum: 94.60521894693375
time_elpased: 1.419
batch start
#iterations: 66
currently lose_sum: 94.61959999799728
time_elpased: 1.475
batch start
#iterations: 67
currently lose_sum: 94.83623319864273
time_elpased: 1.428
batch start
#iterations: 68
currently lose_sum: 94.99838590621948
time_elpased: 1.442
batch start
#iterations: 69
currently lose_sum: 94.75888305902481
time_elpased: 1.45
batch start
#iterations: 70
currently lose_sum: 94.69796830415726
time_elpased: 1.431
batch start
#iterations: 71
currently lose_sum: 94.47132635116577
time_elpased: 1.46
batch start
#iterations: 72
currently lose_sum: 94.68826657533646
time_elpased: 1.43
batch start
#iterations: 73
currently lose_sum: 94.43039155006409
time_elpased: 1.426
batch start
#iterations: 74
currently lose_sum: 94.49492335319519
time_elpased: 1.459
batch start
#iterations: 75
currently lose_sum: 94.22756922245026
time_elpased: 1.466
batch start
#iterations: 76
currently lose_sum: 94.18861162662506
time_elpased: 1.437
batch start
#iterations: 77
currently lose_sum: 94.4633339047432
time_elpased: 1.438
batch start
#iterations: 78
currently lose_sum: 94.5041069984436
time_elpased: 1.468
batch start
#iterations: 79
currently lose_sum: 94.21579599380493
time_elpased: 1.435
start validation test
0.605206185567
0.613400925722
0.572810538232
0.59241126071
0.605263061123
63.649
batch start
#iterations: 80
currently lose_sum: 94.561472594738
time_elpased: 1.46
batch start
#iterations: 81
currently lose_sum: 94.13778561353683
time_elpased: 1.437
batch start
#iterations: 82
currently lose_sum: 94.02078938484192
time_elpased: 1.46
batch start
#iterations: 83
currently lose_sum: 93.97268384695053
time_elpased: 1.441
batch start
#iterations: 84
currently lose_sum: 94.0515501499176
time_elpased: 1.464
batch start
#iterations: 85
currently lose_sum: 93.89054983854294
time_elpased: 1.435
batch start
#iterations: 86
currently lose_sum: 94.12542992830276
time_elpased: 1.43
batch start
#iterations: 87
currently lose_sum: 93.85394793748856
time_elpased: 1.445
batch start
#iterations: 88
currently lose_sum: 93.66580575704575
time_elpased: 1.439
batch start
#iterations: 89
currently lose_sum: 93.52218180894852
time_elpased: 1.453
batch start
#iterations: 90
currently lose_sum: 93.80768591165543
time_elpased: 1.456
batch start
#iterations: 91
currently lose_sum: 93.65889543294907
time_elpased: 1.482
batch start
#iterations: 92
currently lose_sum: 93.55638092756271
time_elpased: 1.463
batch start
#iterations: 93
currently lose_sum: 93.73646891117096
time_elpased: 1.462
batch start
#iterations: 94
currently lose_sum: 93.72167402505875
time_elpased: 1.449
batch start
#iterations: 95
currently lose_sum: 93.2101981639862
time_elpased: 1.456
batch start
#iterations: 96
currently lose_sum: 93.45360332727432
time_elpased: 1.465
batch start
#iterations: 97
currently lose_sum: 93.41782343387604
time_elpased: 1.454
batch start
#iterations: 98
currently lose_sum: 92.95008534193039
time_elpased: 1.465
batch start
#iterations: 99
currently lose_sum: 92.93695050477982
time_elpased: 1.481
start validation test
0.598505154639
0.633444075305
0.470927240918
0.540227849596
0.598729137344
64.067
batch start
#iterations: 100
currently lose_sum: 93.13643169403076
time_elpased: 1.427
batch start
#iterations: 101
currently lose_sum: 93.33757257461548
time_elpased: 1.462
batch start
#iterations: 102
currently lose_sum: 92.78835588693619
time_elpased: 1.431
batch start
#iterations: 103
currently lose_sum: 93.27883243560791
time_elpased: 1.457
batch start
#iterations: 104
currently lose_sum: 93.12711584568024
time_elpased: 1.436
batch start
#iterations: 105
currently lose_sum: 93.27638113498688
time_elpased: 1.452
batch start
#iterations: 106
currently lose_sum: 92.69589430093765
time_elpased: 1.434
batch start
#iterations: 107
currently lose_sum: 92.97463762760162
time_elpased: 1.42
batch start
#iterations: 108
currently lose_sum: 92.93866950273514
time_elpased: 1.466
batch start
#iterations: 109
currently lose_sum: 93.10821485519409
time_elpased: 1.47
batch start
#iterations: 110
currently lose_sum: 92.65591162443161
time_elpased: 1.436
batch start
#iterations: 111
currently lose_sum: 92.82255983352661
time_elpased: 1.44
batch start
#iterations: 112
currently lose_sum: 92.5181155204773
time_elpased: 1.435
batch start
#iterations: 113
currently lose_sum: 92.93390369415283
time_elpased: 1.485
batch start
#iterations: 114
currently lose_sum: 92.55146789550781
time_elpased: 1.443
batch start
#iterations: 115
currently lose_sum: 92.42844837903976
time_elpased: 1.459
batch start
#iterations: 116
currently lose_sum: 92.56506252288818
time_elpased: 1.457
batch start
#iterations: 117
currently lose_sum: 92.79298114776611
time_elpased: 1.425
batch start
#iterations: 118
currently lose_sum: 92.33645153045654
time_elpased: 1.438
batch start
#iterations: 119
currently lose_sum: 92.59275341033936
time_elpased: 1.44
start validation test
0.57912371134
0.625120928733
0.398991458269
0.487090897669
0.579439961294
65.099
batch start
#iterations: 120
currently lose_sum: 92.46488243341446
time_elpased: 1.456
batch start
#iterations: 121
currently lose_sum: 91.94539612531662
time_elpased: 1.425
batch start
#iterations: 122
currently lose_sum: 92.48380225896835
time_elpased: 1.467
batch start
#iterations: 123
currently lose_sum: 91.87668764591217
time_elpased: 1.46
batch start
#iterations: 124
currently lose_sum: 92.14165550470352
time_elpased: 1.445
batch start
#iterations: 125
currently lose_sum: 91.74802708625793
time_elpased: 1.449
batch start
#iterations: 126
currently lose_sum: 92.33993011713028
time_elpased: 1.464
batch start
#iterations: 127
currently lose_sum: 92.20518684387207
time_elpased: 1.446
batch start
#iterations: 128
currently lose_sum: 91.74128371477127
time_elpased: 1.45
batch start
#iterations: 129
currently lose_sum: 91.86523467302322
time_elpased: 1.426
batch start
#iterations: 130
currently lose_sum: 91.86481881141663
time_elpased: 1.455
batch start
#iterations: 131
currently lose_sum: 91.56686615943909
time_elpased: 1.46
batch start
#iterations: 132
currently lose_sum: 92.23980987071991
time_elpased: 1.462
batch start
#iterations: 133
currently lose_sum: 91.21308195590973
time_elpased: 1.478
batch start
#iterations: 134
currently lose_sum: 91.97353702783585
time_elpased: 1.426
batch start
#iterations: 135
currently lose_sum: 91.5176409482956
time_elpased: 1.432
batch start
#iterations: 136
currently lose_sum: 92.02070325613022
time_elpased: 1.456
batch start
#iterations: 137
currently lose_sum: 91.70439821481705
time_elpased: 1.459
batch start
#iterations: 138
currently lose_sum: 90.88076001405716
time_elpased: 1.479
batch start
#iterations: 139
currently lose_sum: 90.92528259754181
time_elpased: 1.449
start validation test
0.595824742268
0.615773882992
0.51343007101
0.559964083282
0.595969398822
64.273
batch start
#iterations: 140
currently lose_sum: 91.5207114815712
time_elpased: 1.446
batch start
#iterations: 141
currently lose_sum: 91.09199357032776
time_elpased: 1.442
batch start
#iterations: 142
currently lose_sum: 91.91202408075333
time_elpased: 1.434
batch start
#iterations: 143
currently lose_sum: 91.32124370336533
time_elpased: 1.424
batch start
#iterations: 144
currently lose_sum: 91.8194950222969
time_elpased: 1.441
batch start
#iterations: 145
currently lose_sum: 91.53971165418625
time_elpased: 1.448
batch start
#iterations: 146
currently lose_sum: 90.86596739292145
time_elpased: 1.492
batch start
#iterations: 147
currently lose_sum: 91.2997614145279
time_elpased: 1.415
batch start
#iterations: 148
currently lose_sum: 91.20700430870056
time_elpased: 1.417
batch start
#iterations: 149
currently lose_sum: 91.05027461051941
time_elpased: 1.491
batch start
#iterations: 150
currently lose_sum: 91.1560869216919
time_elpased: 1.439
batch start
#iterations: 151
currently lose_sum: 91.48079347610474
time_elpased: 1.498
batch start
#iterations: 152
currently lose_sum: 91.41894859075546
time_elpased: 1.465
batch start
#iterations: 153
currently lose_sum: 91.06578785181046
time_elpased: 1.444
batch start
#iterations: 154
currently lose_sum: 91.04952490329742
time_elpased: 1.45
batch start
#iterations: 155
currently lose_sum: 91.01558554172516
time_elpased: 1.456
batch start
#iterations: 156
currently lose_sum: 91.11536771059036
time_elpased: 1.423
batch start
#iterations: 157
currently lose_sum: 91.02012646198273
time_elpased: 1.452
batch start
#iterations: 158
currently lose_sum: 91.42859148979187
time_elpased: 1.42
batch start
#iterations: 159
currently lose_sum: 90.83041822910309
time_elpased: 1.455
start validation test
0.593659793814
0.620594423987
0.485643717197
0.544887708562
0.593849432697
64.972
batch start
#iterations: 160
currently lose_sum: 90.7399110198021
time_elpased: 1.44
batch start
#iterations: 161
currently lose_sum: 90.38673388957977
time_elpased: 1.426
batch start
#iterations: 162
currently lose_sum: 90.86283242702484
time_elpased: 1.451
batch start
#iterations: 163
currently lose_sum: 90.7276828289032
time_elpased: 1.442
batch start
#iterations: 164
currently lose_sum: 90.78254354000092
time_elpased: 1.458
batch start
#iterations: 165
currently lose_sum: 90.83822357654572
time_elpased: 1.443
batch start
#iterations: 166
currently lose_sum: 90.18552827835083
time_elpased: 1.438
batch start
#iterations: 167
currently lose_sum: 90.2074134349823
time_elpased: 1.456
batch start
#iterations: 168
currently lose_sum: 90.72570872306824
time_elpased: 1.442
batch start
#iterations: 169
currently lose_sum: 90.33956378698349
time_elpased: 1.454
batch start
#iterations: 170
currently lose_sum: 90.64483135938644
time_elpased: 1.501
batch start
#iterations: 171
currently lose_sum: 90.2103967666626
time_elpased: 1.436
batch start
#iterations: 172
currently lose_sum: 90.59309321641922
time_elpased: 1.447
batch start
#iterations: 173
currently lose_sum: 90.39316463470459
time_elpased: 1.417
batch start
#iterations: 174
currently lose_sum: 90.46642017364502
time_elpased: 1.429
batch start
#iterations: 175
currently lose_sum: 89.68005388975143
time_elpased: 1.476
batch start
#iterations: 176
currently lose_sum: 89.93200773000717
time_elpased: 1.427
batch start
#iterations: 177
currently lose_sum: 90.2917851805687
time_elpased: 1.412
batch start
#iterations: 178
currently lose_sum: 90.04661583900452
time_elpased: 1.454
batch start
#iterations: 179
currently lose_sum: 90.12179934978485
time_elpased: 1.446
start validation test
0.58618556701
0.613310076479
0.47041267881
0.532440302854
0.586388824172
65.749
batch start
#iterations: 180
currently lose_sum: 90.32633006572723
time_elpased: 1.463
batch start
#iterations: 181
currently lose_sum: 90.01791578531265
time_elpased: 1.449
batch start
#iterations: 182
currently lose_sum: 89.95029002428055
time_elpased: 1.438
batch start
#iterations: 183
currently lose_sum: 89.91688770055771
time_elpased: 1.42
batch start
#iterations: 184
currently lose_sum: 89.91984558105469
time_elpased: 1.453
batch start
#iterations: 185
currently lose_sum: 89.54550814628601
time_elpased: 1.469
batch start
#iterations: 186
currently lose_sum: 89.40479642152786
time_elpased: 1.457
batch start
#iterations: 187
currently lose_sum: 89.99339479207993
time_elpased: 1.429
batch start
#iterations: 188
currently lose_sum: 89.76832056045532
time_elpased: 1.449
batch start
#iterations: 189
currently lose_sum: 89.73496466875076
time_elpased: 1.447
batch start
#iterations: 190
currently lose_sum: 90.29158300161362
time_elpased: 1.459
batch start
#iterations: 191
currently lose_sum: 89.73760789632797
time_elpased: 1.434
batch start
#iterations: 192
currently lose_sum: 89.48313653469086
time_elpased: 1.469
batch start
#iterations: 193
currently lose_sum: 89.99058681726456
time_elpased: 1.451
batch start
#iterations: 194
currently lose_sum: 89.9100843667984
time_elpased: 1.458
batch start
#iterations: 195
currently lose_sum: 89.4664694070816
time_elpased: 1.478
batch start
#iterations: 196
currently lose_sum: 89.93746453523636
time_elpased: 1.439
batch start
#iterations: 197
currently lose_sum: 89.80365657806396
time_elpased: 1.434
batch start
#iterations: 198
currently lose_sum: 89.64978581666946
time_elpased: 1.456
batch start
#iterations: 199
currently lose_sum: 89.3238582611084
time_elpased: 1.436
start validation test
0.598505154639
0.620771736407
0.509931048678
0.559918639471
0.598660660144
65.196
batch start
#iterations: 200
currently lose_sum: 89.23394948244095
time_elpased: 1.498
batch start
#iterations: 201
currently lose_sum: 89.7788822054863
time_elpased: 1.442
batch start
#iterations: 202
currently lose_sum: 89.482137799263
time_elpased: 1.454
batch start
#iterations: 203
currently lose_sum: 88.8055489063263
time_elpased: 1.427
batch start
#iterations: 204
currently lose_sum: 89.10724526643753
time_elpased: 1.426
batch start
#iterations: 205
currently lose_sum: 89.66909098625183
time_elpased: 1.42
batch start
#iterations: 206
currently lose_sum: 89.72185409069061
time_elpased: 1.46
batch start
#iterations: 207
currently lose_sum: 89.0250295996666
time_elpased: 1.419
batch start
#iterations: 208
currently lose_sum: 88.94280630350113
time_elpased: 1.438
batch start
#iterations: 209
currently lose_sum: 89.18251210451126
time_elpased: 1.444
batch start
#iterations: 210
currently lose_sum: 88.88925671577454
time_elpased: 1.433
batch start
#iterations: 211
currently lose_sum: 88.86284267902374
time_elpased: 1.458
batch start
#iterations: 212
currently lose_sum: 89.27777147293091
time_elpased: 1.451
batch start
#iterations: 213
currently lose_sum: 89.03704196214676
time_elpased: 1.474
batch start
#iterations: 214
currently lose_sum: 88.84008264541626
time_elpased: 1.461
batch start
#iterations: 215
currently lose_sum: 88.86802637577057
time_elpased: 1.435
batch start
#iterations: 216
currently lose_sum: 88.68212765455246
time_elpased: 1.432
batch start
#iterations: 217
currently lose_sum: 88.78931993246078
time_elpased: 1.475
batch start
#iterations: 218
currently lose_sum: 89.26060616970062
time_elpased: 1.468
batch start
#iterations: 219
currently lose_sum: 89.20928424596786
time_elpased: 1.428
start validation test
0.595051546392
0.613517140417
0.517546567871
0.561460310372
0.595187618336
65.529
batch start
#iterations: 220
currently lose_sum: 88.32412999868393
time_elpased: 1.456
batch start
#iterations: 221
currently lose_sum: 89.13300603628159
time_elpased: 1.462
batch start
#iterations: 222
currently lose_sum: 88.86206018924713
time_elpased: 1.454
batch start
#iterations: 223
currently lose_sum: 88.46007120609283
time_elpased: 1.5
batch start
#iterations: 224
currently lose_sum: 88.62070977687836
time_elpased: 1.44
batch start
#iterations: 225
currently lose_sum: 88.60771811008453
time_elpased: 1.458
batch start
#iterations: 226
currently lose_sum: 88.38995438814163
time_elpased: 1.464
batch start
#iterations: 227
currently lose_sum: 88.6334422826767
time_elpased: 1.441
batch start
#iterations: 228
currently lose_sum: 88.36048483848572
time_elpased: 1.434
batch start
#iterations: 229
currently lose_sum: 88.47893726825714
time_elpased: 1.443
batch start
#iterations: 230
currently lose_sum: 88.90243643522263
time_elpased: 1.436
batch start
#iterations: 231
currently lose_sum: 88.34519582986832
time_elpased: 1.486
batch start
#iterations: 232
currently lose_sum: 88.42393976449966
time_elpased: 1.426
batch start
#iterations: 233
currently lose_sum: 88.48444002866745
time_elpased: 1.453
batch start
#iterations: 234
currently lose_sum: 89.10447138547897
time_elpased: 1.427
batch start
#iterations: 235
currently lose_sum: 87.9911258816719
time_elpased: 1.456
batch start
#iterations: 236
currently lose_sum: 88.71066588163376
time_elpased: 1.435
batch start
#iterations: 237
currently lose_sum: 88.24304038286209
time_elpased: 1.488
batch start
#iterations: 238
currently lose_sum: 88.9087952375412
time_elpased: 1.427
batch start
#iterations: 239
currently lose_sum: 88.27762371301651
time_elpased: 1.45
start validation test
0.59175257732
0.606042723947
0.528455284553
0.564595931831
0.59186370548
65.747
batch start
#iterations: 240
currently lose_sum: 88.32023507356644
time_elpased: 1.423
batch start
#iterations: 241
currently lose_sum: 88.48161512613297
time_elpased: 1.43
batch start
#iterations: 242
currently lose_sum: 88.01818019151688
time_elpased: 1.466
batch start
#iterations: 243
currently lose_sum: 89.20317769050598
time_elpased: 1.441
batch start
#iterations: 244
currently lose_sum: 88.74839848279953
time_elpased: 1.423
batch start
#iterations: 245
currently lose_sum: 88.0573695898056
time_elpased: 1.437
batch start
#iterations: 246
currently lose_sum: 87.68568962812424
time_elpased: 1.428
batch start
#iterations: 247
currently lose_sum: 88.13235819339752
time_elpased: 1.43
batch start
#iterations: 248
currently lose_sum: 87.89992696046829
time_elpased: 1.426
batch start
#iterations: 249
currently lose_sum: 88.16000491380692
time_elpased: 1.534
batch start
#iterations: 250
currently lose_sum: 88.37986820936203
time_elpased: 1.487
batch start
#iterations: 251
currently lose_sum: 88.21838593482971
time_elpased: 1.431
batch start
#iterations: 252
currently lose_sum: 87.97932648658752
time_elpased: 1.449
batch start
#iterations: 253
currently lose_sum: 88.43231499195099
time_elpased: 1.423
batch start
#iterations: 254
currently lose_sum: 88.03138548135757
time_elpased: 1.465
batch start
#iterations: 255
currently lose_sum: 87.8184267282486
time_elpased: 1.435
batch start
#iterations: 256
currently lose_sum: 87.65452522039413
time_elpased: 1.477
batch start
#iterations: 257
currently lose_sum: 87.74621438980103
time_elpased: 1.417
batch start
#iterations: 258
currently lose_sum: 87.52612888813019
time_elpased: 1.492
batch start
#iterations: 259
currently lose_sum: 87.7414625287056
time_elpased: 1.458
start validation test
0.578608247423
0.61186883343
0.433981681589
0.507796977542
0.578862161666
67.415
batch start
#iterations: 260
currently lose_sum: 87.55196112394333
time_elpased: 1.454
batch start
#iterations: 261
currently lose_sum: 87.82356888055801
time_elpased: 1.451
batch start
#iterations: 262
currently lose_sum: 87.40028834342957
time_elpased: 1.432
batch start
#iterations: 263
currently lose_sum: 87.6126167178154
time_elpased: 1.448
batch start
#iterations: 264
currently lose_sum: 88.08844792842865
time_elpased: 1.448
batch start
#iterations: 265
currently lose_sum: 87.74544405937195
time_elpased: 1.439
batch start
#iterations: 266
currently lose_sum: 87.79337751865387
time_elpased: 1.43
batch start
#iterations: 267
currently lose_sum: 87.73958897590637
time_elpased: 1.452
batch start
#iterations: 268
currently lose_sum: 87.52266043424606
time_elpased: 1.447
batch start
#iterations: 269
currently lose_sum: 87.51242101192474
time_elpased: 1.444
batch start
#iterations: 270
currently lose_sum: 87.58949965238571
time_elpased: 1.464
batch start
#iterations: 271
currently lose_sum: 87.48818892240524
time_elpased: 1.444
batch start
#iterations: 272
currently lose_sum: 87.66683638095856
time_elpased: 1.438
batch start
#iterations: 273
currently lose_sum: 87.14353668689728
time_elpased: 1.44
batch start
#iterations: 274
currently lose_sum: 87.54110473394394
time_elpased: 1.42
batch start
#iterations: 275
currently lose_sum: 87.41789507865906
time_elpased: 1.429
batch start
#iterations: 276
currently lose_sum: 87.5337804555893
time_elpased: 1.436
batch start
#iterations: 277
currently lose_sum: 87.58443820476532
time_elpased: 1.435
batch start
#iterations: 278
currently lose_sum: 87.40888553857803
time_elpased: 1.421
batch start
#iterations: 279
currently lose_sum: 86.87797003984451
time_elpased: 1.438
start validation test
0.591546391753
0.608679839981
0.516723268499
0.558944673272
0.591677755286
66.447
batch start
#iterations: 280
currently lose_sum: 87.64065200090408
time_elpased: 1.413
batch start
#iterations: 281
currently lose_sum: 87.07953250408173
time_elpased: 1.458
batch start
#iterations: 282
currently lose_sum: 87.26997059583664
time_elpased: 1.428
batch start
#iterations: 283
currently lose_sum: 87.27818459272385
time_elpased: 1.455
batch start
#iterations: 284
currently lose_sum: 86.64801275730133
time_elpased: 1.457
batch start
#iterations: 285
currently lose_sum: 87.34149545431137
time_elpased: 1.433
batch start
#iterations: 286
currently lose_sum: 87.10851013660431
time_elpased: 1.437
batch start
#iterations: 287
currently lose_sum: 86.64842504262924
time_elpased: 1.45
batch start
#iterations: 288
currently lose_sum: 87.21160930395126
time_elpased: 1.426
batch start
#iterations: 289
currently lose_sum: 87.35969024896622
time_elpased: 1.447
batch start
#iterations: 290
currently lose_sum: 87.3317221403122
time_elpased: 1.45
batch start
#iterations: 291
currently lose_sum: 87.2804913520813
time_elpased: 1.419
batch start
#iterations: 292
currently lose_sum: 86.90597724914551
time_elpased: 1.435
batch start
#iterations: 293
currently lose_sum: 86.69294327497482
time_elpased: 1.43
batch start
#iterations: 294
currently lose_sum: 86.74022787809372
time_elpased: 1.457
batch start
#iterations: 295
currently lose_sum: 86.98732668161392
time_elpased: 1.46
batch start
#iterations: 296
currently lose_sum: 86.96801173686981
time_elpased: 1.477
batch start
#iterations: 297
currently lose_sum: 86.3672861456871
time_elpased: 1.428
batch start
#iterations: 298
currently lose_sum: 86.8224505186081
time_elpased: 1.43
batch start
#iterations: 299
currently lose_sum: 86.62982827425003
time_elpased: 1.439
start validation test
0.589587628866
0.605863192182
0.51682618092
0.557814061979
0.589715372811
66.509
batch start
#iterations: 300
currently lose_sum: 86.78948837518692
time_elpased: 1.449
batch start
#iterations: 301
currently lose_sum: 86.68079650402069
time_elpased: 1.443
batch start
#iterations: 302
currently lose_sum: 86.93529695272446
time_elpased: 1.429
batch start
#iterations: 303
currently lose_sum: 86.64606684446335
time_elpased: 1.448
batch start
#iterations: 304
currently lose_sum: 86.6592218875885
time_elpased: 1.466
batch start
#iterations: 305
currently lose_sum: 86.30935150384903
time_elpased: 1.458
batch start
#iterations: 306
currently lose_sum: 87.34093421697617
time_elpased: 1.435
batch start
#iterations: 307
currently lose_sum: 86.89202618598938
time_elpased: 1.434
batch start
#iterations: 308
currently lose_sum: 86.56651854515076
time_elpased: 1.438
batch start
#iterations: 309
currently lose_sum: 86.46748465299606
time_elpased: 1.425
batch start
#iterations: 310
currently lose_sum: 86.9231168627739
time_elpased: 1.452
batch start
#iterations: 311
currently lose_sum: 86.18114763498306
time_elpased: 1.425
batch start
#iterations: 312
currently lose_sum: 85.79459965229034
time_elpased: 1.42
batch start
#iterations: 313
currently lose_sum: 86.59096473455429
time_elpased: 1.436
batch start
#iterations: 314
currently lose_sum: 85.89743024110794
time_elpased: 1.441
batch start
#iterations: 315
currently lose_sum: 86.71299809217453
time_elpased: 1.463
batch start
#iterations: 316
currently lose_sum: 86.19808918237686
time_elpased: 1.413
batch start
#iterations: 317
currently lose_sum: 86.46970117092133
time_elpased: 1.43
batch start
#iterations: 318
currently lose_sum: 86.2694274187088
time_elpased: 1.442
batch start
#iterations: 319
currently lose_sum: 86.5139519572258
time_elpased: 1.409
start validation test
0.580979381443
0.610955841252
0.449933106926
0.518224382149
0.581209453391
67.749
batch start
#iterations: 320
currently lose_sum: 86.54000914096832
time_elpased: 1.443
batch start
#iterations: 321
currently lose_sum: 86.57142984867096
time_elpased: 1.43
batch start
#iterations: 322
currently lose_sum: 87.04276758432388
time_elpased: 1.458
batch start
#iterations: 323
currently lose_sum: 86.76710021495819
time_elpased: 1.458
batch start
#iterations: 324
currently lose_sum: 86.741266310215
time_elpased: 1.475
batch start
#iterations: 325
currently lose_sum: 86.46222847700119
time_elpased: 1.476
batch start
#iterations: 326
currently lose_sum: 86.59918969869614
time_elpased: 1.431
batch start
#iterations: 327
currently lose_sum: 85.85299456119537
time_elpased: 1.455
batch start
#iterations: 328
currently lose_sum: 85.96027511358261
time_elpased: 1.424
batch start
#iterations: 329
currently lose_sum: 86.22438615560532
time_elpased: 1.436
batch start
#iterations: 330
currently lose_sum: 85.77792489528656
time_elpased: 1.416
batch start
#iterations: 331
currently lose_sum: 86.10944700241089
time_elpased: 1.428
batch start
#iterations: 332
currently lose_sum: 86.11448502540588
time_elpased: 1.422
batch start
#iterations: 333
currently lose_sum: 85.57695081830025
time_elpased: 1.47
batch start
#iterations: 334
currently lose_sum: 85.82375782728195
time_elpased: 1.422
batch start
#iterations: 335
currently lose_sum: 85.19733035564423
time_elpased: 1.446
batch start
#iterations: 336
currently lose_sum: 86.36092346906662
time_elpased: 1.449
batch start
#iterations: 337
currently lose_sum: 85.90106981992722
time_elpased: 1.431
batch start
#iterations: 338
currently lose_sum: 86.1372743844986
time_elpased: 1.456
batch start
#iterations: 339
currently lose_sum: 86.02398252487183
time_elpased: 1.485
start validation test
0.59706185567
0.604764005293
0.564371719667
0.58387010913
0.597119248246
66.262
batch start
#iterations: 340
currently lose_sum: 85.85407042503357
time_elpased: 1.459
batch start
#iterations: 341
currently lose_sum: 85.90630972385406
time_elpased: 1.474
batch start
#iterations: 342
currently lose_sum: 86.05149614810944
time_elpased: 1.529
batch start
#iterations: 343
currently lose_sum: 86.00278824567795
time_elpased: 1.479
batch start
#iterations: 344
currently lose_sum: 86.12140721082687
time_elpased: 1.432
batch start
#iterations: 345
currently lose_sum: 85.85090798139572
time_elpased: 1.432
batch start
#iterations: 346
currently lose_sum: 86.16901296377182
time_elpased: 1.429
batch start
#iterations: 347
currently lose_sum: 85.48555064201355
time_elpased: 1.429
batch start
#iterations: 348
currently lose_sum: 85.77968162298203
time_elpased: 1.444
batch start
#iterations: 349
currently lose_sum: 86.11965721845627
time_elpased: 1.44
batch start
#iterations: 350
currently lose_sum: 85.67963236570358
time_elpased: 1.45
batch start
#iterations: 351
currently lose_sum: 85.41455692052841
time_elpased: 1.451
batch start
#iterations: 352
currently lose_sum: 85.894052028656
time_elpased: 1.459
batch start
#iterations: 353
currently lose_sum: 86.50030988454819
time_elpased: 1.433
batch start
#iterations: 354
currently lose_sum: 85.9387189745903
time_elpased: 1.435
batch start
#iterations: 355
currently lose_sum: 86.04541051387787
time_elpased: 1.443
batch start
#iterations: 356
currently lose_sum: 85.41072797775269
time_elpased: 1.425
batch start
#iterations: 357
currently lose_sum: 85.79799091815948
time_elpased: 1.426
batch start
#iterations: 358
currently lose_sum: 86.37010020017624
time_elpased: 1.436
batch start
#iterations: 359
currently lose_sum: 84.75083136558533
time_elpased: 1.453
start validation test
0.585463917526
0.614553412666
0.462385509931
0.527719050975
0.585680000654
67.778
batch start
#iterations: 360
currently lose_sum: 85.41371589899063
time_elpased: 1.508
batch start
#iterations: 361
currently lose_sum: 85.8849509358406
time_elpased: 1.453
batch start
#iterations: 362
currently lose_sum: 85.04472160339355
time_elpased: 1.438
batch start
#iterations: 363
currently lose_sum: 85.28056716918945
time_elpased: 1.464
batch start
#iterations: 364
currently lose_sum: 85.89139461517334
time_elpased: 1.46
batch start
#iterations: 365
currently lose_sum: 85.48821103572845
time_elpased: 1.429
batch start
#iterations: 366
currently lose_sum: 85.78153812885284
time_elpased: 1.425
batch start
#iterations: 367
currently lose_sum: 84.9778835773468
time_elpased: 1.431
batch start
#iterations: 368
currently lose_sum: 85.6497774720192
time_elpased: 1.424
batch start
#iterations: 369
currently lose_sum: 85.31599152088165
time_elpased: 1.44
batch start
#iterations: 370
currently lose_sum: 84.97079783678055
time_elpased: 1.431
batch start
#iterations: 371
currently lose_sum: 84.91553747653961
time_elpased: 1.464
batch start
#iterations: 372
currently lose_sum: 85.4468183517456
time_elpased: 1.455
batch start
#iterations: 373
currently lose_sum: 85.22234815359116
time_elpased: 1.454
batch start
#iterations: 374
currently lose_sum: 84.84645247459412
time_elpased: 1.47
batch start
#iterations: 375
currently lose_sum: 84.3933619260788
time_elpased: 1.446
batch start
#iterations: 376
currently lose_sum: 84.97345787286758
time_elpased: 1.42
batch start
#iterations: 377
currently lose_sum: 85.24553734064102
time_elpased: 1.443
batch start
#iterations: 378
currently lose_sum: 85.03652435541153
time_elpased: 1.444
batch start
#iterations: 379
currently lose_sum: 85.36193120479584
time_elpased: 1.447
start validation test
0.589536082474
0.602024197301
0.532571781414
0.565172281985
0.589636092091
67.338
batch start
#iterations: 380
currently lose_sum: 85.13844668865204
time_elpased: 1.424
batch start
#iterations: 381
currently lose_sum: 85.19405138492584
time_elpased: 1.429
batch start
#iterations: 382
currently lose_sum: 85.24469816684723
time_elpased: 1.406
batch start
#iterations: 383
currently lose_sum: 85.21649277210236
time_elpased: 1.491
batch start
#iterations: 384
currently lose_sum: 85.34381520748138
time_elpased: 1.45
batch start
#iterations: 385
currently lose_sum: 85.12998425960541
time_elpased: 1.472
batch start
#iterations: 386
currently lose_sum: 84.67155158519745
time_elpased: 1.434
batch start
#iterations: 387
currently lose_sum: 85.18010324239731
time_elpased: 1.437
batch start
#iterations: 388
currently lose_sum: 84.87200272083282
time_elpased: 1.42
batch start
#iterations: 389
currently lose_sum: 84.93180423974991
time_elpased: 1.441
batch start
#iterations: 390
currently lose_sum: 85.1617174744606
time_elpased: 1.443
batch start
#iterations: 391
currently lose_sum: 84.19086956977844
time_elpased: 1.445
batch start
#iterations: 392
currently lose_sum: 84.44746720790863
time_elpased: 1.421
batch start
#iterations: 393
currently lose_sum: 84.97296726703644
time_elpased: 1.44
batch start
#iterations: 394
currently lose_sum: 85.22631323337555
time_elpased: 1.442
batch start
#iterations: 395
currently lose_sum: 85.29222971200943
time_elpased: 1.412
batch start
#iterations: 396
currently lose_sum: 84.5178154706955
time_elpased: 1.456
batch start
#iterations: 397
currently lose_sum: 84.51830297708511
time_elpased: 1.445
batch start
#iterations: 398
currently lose_sum: 85.09327918291092
time_elpased: 1.47
batch start
#iterations: 399
currently lose_sum: 84.48268818855286
time_elpased: 1.424
start validation test
0.581907216495
0.605992608237
0.472470927241
0.530966286937
0.58209934878
68.366
acc: 0.621
pre: 0.613
rec: 0.658
F1: 0.635
auc: 0.663
