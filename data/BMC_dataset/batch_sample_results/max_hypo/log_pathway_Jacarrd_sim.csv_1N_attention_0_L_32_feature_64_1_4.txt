start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.62958389520645
time_elpased: 1.454
batch start
#iterations: 1
currently lose_sum: 99.94305408000946
time_elpased: 1.47
batch start
#iterations: 2
currently lose_sum: 99.67472499608994
time_elpased: 1.426
batch start
#iterations: 3
currently lose_sum: 99.36264181137085
time_elpased: 1.471
batch start
#iterations: 4
currently lose_sum: 99.25781989097595
time_elpased: 1.434
batch start
#iterations: 5
currently lose_sum: 98.98935341835022
time_elpased: 1.45
batch start
#iterations: 6
currently lose_sum: 98.94811487197876
time_elpased: 1.417
batch start
#iterations: 7
currently lose_sum: 98.89217221736908
time_elpased: 1.454
batch start
#iterations: 8
currently lose_sum: 98.6685540676117
time_elpased: 1.447
batch start
#iterations: 9
currently lose_sum: 98.37864929437637
time_elpased: 1.469
batch start
#iterations: 10
currently lose_sum: 98.2822772860527
time_elpased: 1.429
batch start
#iterations: 11
currently lose_sum: 98.2760761976242
time_elpased: 1.44
batch start
#iterations: 12
currently lose_sum: 98.15119731426239
time_elpased: 1.421
batch start
#iterations: 13
currently lose_sum: 98.3531436920166
time_elpased: 1.483
batch start
#iterations: 14
currently lose_sum: 98.07328057289124
time_elpased: 1.44
batch start
#iterations: 15
currently lose_sum: 97.94490122795105
time_elpased: 1.427
batch start
#iterations: 16
currently lose_sum: 97.84288084506989
time_elpased: 1.434
batch start
#iterations: 17
currently lose_sum: 97.3866319656372
time_elpased: 1.438
batch start
#iterations: 18
currently lose_sum: 97.69011622667313
time_elpased: 1.438
batch start
#iterations: 19
currently lose_sum: 97.52146351337433
time_elpased: 1.428
start validation test
0.614278350515
0.601971882417
0.678604507564
0.637995259059
0.614165416025
63.925
batch start
#iterations: 20
currently lose_sum: 97.52395468950272
time_elpased: 1.463
batch start
#iterations: 21
currently lose_sum: 97.21368092298508
time_elpased: 1.415
batch start
#iterations: 22
currently lose_sum: 97.19627594947815
time_elpased: 1.429
batch start
#iterations: 23
currently lose_sum: 97.31223344802856
time_elpased: 1.439
batch start
#iterations: 24
currently lose_sum: 97.0094964504242
time_elpased: 1.443
batch start
#iterations: 25
currently lose_sum: 97.01627033948898
time_elpased: 1.422
batch start
#iterations: 26
currently lose_sum: 97.19315510988235
time_elpased: 1.42
batch start
#iterations: 27
currently lose_sum: 96.76587617397308
time_elpased: 1.487
batch start
#iterations: 28
currently lose_sum: 97.0459634065628
time_elpased: 1.452
batch start
#iterations: 29
currently lose_sum: 96.28714936971664
time_elpased: 1.439
batch start
#iterations: 30
currently lose_sum: 96.60384666919708
time_elpased: 1.445
batch start
#iterations: 31
currently lose_sum: 96.41373360157013
time_elpased: 1.442
batch start
#iterations: 32
currently lose_sum: 96.42953109741211
time_elpased: 1.49
batch start
#iterations: 33
currently lose_sum: 96.54092389345169
time_elpased: 1.458
batch start
#iterations: 34
currently lose_sum: 96.25215470790863
time_elpased: 1.473
batch start
#iterations: 35
currently lose_sum: 96.19835877418518
time_elpased: 1.438
batch start
#iterations: 36
currently lose_sum: 96.11483746767044
time_elpased: 1.43
batch start
#iterations: 37
currently lose_sum: 96.14866453409195
time_elpased: 1.473
batch start
#iterations: 38
currently lose_sum: 95.75453841686249
time_elpased: 1.48
batch start
#iterations: 39
currently lose_sum: 96.14463597536087
time_elpased: 1.443
start validation test
0.627010309278
0.625773091352
0.635175465679
0.630439223698
0.626995974087
63.107
batch start
#iterations: 40
currently lose_sum: 95.87207055091858
time_elpased: 1.46
batch start
#iterations: 41
currently lose_sum: 95.83464980125427
time_elpased: 1.442
batch start
#iterations: 42
currently lose_sum: 95.93198323249817
time_elpased: 1.443
batch start
#iterations: 43
currently lose_sum: 95.96465164422989
time_elpased: 1.442
batch start
#iterations: 44
currently lose_sum: 96.17522019147873
time_elpased: 1.439
batch start
#iterations: 45
currently lose_sum: 95.6718829870224
time_elpased: 1.455
batch start
#iterations: 46
currently lose_sum: 95.56469094753265
time_elpased: 1.461
batch start
#iterations: 47
currently lose_sum: 95.8687008023262
time_elpased: 1.413
batch start
#iterations: 48
currently lose_sum: 95.83619195222855
time_elpased: 1.451
batch start
#iterations: 49
currently lose_sum: 95.58198684453964
time_elpased: 1.463
batch start
#iterations: 50
currently lose_sum: 95.59470063447952
time_elpased: 1.445
batch start
#iterations: 51
currently lose_sum: 95.35287165641785
time_elpased: 1.45
batch start
#iterations: 52
currently lose_sum: 95.29017847776413
time_elpased: 1.465
batch start
#iterations: 53
currently lose_sum: 94.98117280006409
time_elpased: 1.468
batch start
#iterations: 54
currently lose_sum: 95.20319491624832
time_elpased: 1.442
batch start
#iterations: 55
currently lose_sum: 94.66921746730804
time_elpased: 1.418
batch start
#iterations: 56
currently lose_sum: 94.94589507579803
time_elpased: 1.43
batch start
#iterations: 57
currently lose_sum: 95.06614392995834
time_elpased: 1.476
batch start
#iterations: 58
currently lose_sum: 94.93614596128464
time_elpased: 1.443
batch start
#iterations: 59
currently lose_sum: 95.27300560474396
time_elpased: 1.505
start validation test
0.603711340206
0.627594013332
0.513532983431
0.564863029205
0.603869662221
63.509
batch start
#iterations: 60
currently lose_sum: 94.89536786079407
time_elpased: 1.461
batch start
#iterations: 61
currently lose_sum: 95.53393977880478
time_elpased: 1.477
batch start
#iterations: 62
currently lose_sum: 94.69122844934464
time_elpased: 1.474
batch start
#iterations: 63
currently lose_sum: 94.80813854932785
time_elpased: 1.545
batch start
#iterations: 64
currently lose_sum: 95.10089957714081
time_elpased: 1.419
batch start
#iterations: 65
currently lose_sum: 94.70739132165909
time_elpased: 1.484
batch start
#iterations: 66
currently lose_sum: 94.26049315929413
time_elpased: 1.508
batch start
#iterations: 67
currently lose_sum: 94.41550695896149
time_elpased: 1.429
batch start
#iterations: 68
currently lose_sum: 94.60916256904602
time_elpased: 1.438
batch start
#iterations: 69
currently lose_sum: 94.26680111885071
time_elpased: 1.45
batch start
#iterations: 70
currently lose_sum: 94.5263951420784
time_elpased: 1.439
batch start
#iterations: 71
currently lose_sum: 94.349434196949
time_elpased: 1.449
batch start
#iterations: 72
currently lose_sum: 93.93422102928162
time_elpased: 1.437
batch start
#iterations: 73
currently lose_sum: 94.18108916282654
time_elpased: 1.46
batch start
#iterations: 74
currently lose_sum: 93.81153732538223
time_elpased: 1.417
batch start
#iterations: 75
currently lose_sum: 94.2152938246727
time_elpased: 1.424
batch start
#iterations: 76
currently lose_sum: 94.31095761060715
time_elpased: 1.443
batch start
#iterations: 77
currently lose_sum: 94.19902193546295
time_elpased: 1.494
batch start
#iterations: 78
currently lose_sum: 94.15362185239792
time_elpased: 1.455
batch start
#iterations: 79
currently lose_sum: 93.73403859138489
time_elpased: 1.481
start validation test
0.614793814433
0.631443298969
0.554697952043
0.590587848573
0.614899321989
63.215
batch start
#iterations: 80
currently lose_sum: 93.93387395143509
time_elpased: 1.461
batch start
#iterations: 81
currently lose_sum: 93.65202206373215
time_elpased: 1.44
batch start
#iterations: 82
currently lose_sum: 93.95187735557556
time_elpased: 1.481
batch start
#iterations: 83
currently lose_sum: 93.54784220457077
time_elpased: 1.433
batch start
#iterations: 84
currently lose_sum: 93.72405797243118
time_elpased: 1.434
batch start
#iterations: 85
currently lose_sum: 93.9930848479271
time_elpased: 1.471
batch start
#iterations: 86
currently lose_sum: 93.61776101589203
time_elpased: 1.439
batch start
#iterations: 87
currently lose_sum: 93.75953757762909
time_elpased: 1.446
batch start
#iterations: 88
currently lose_sum: 93.50578588247299
time_elpased: 1.444
batch start
#iterations: 89
currently lose_sum: 93.51309049129486
time_elpased: 1.434
batch start
#iterations: 90
currently lose_sum: 93.4392454624176
time_elpased: 1.449
batch start
#iterations: 91
currently lose_sum: 93.40991079807281
time_elpased: 1.462
batch start
#iterations: 92
currently lose_sum: 92.77720350027084
time_elpased: 1.455
batch start
#iterations: 93
currently lose_sum: 93.34458327293396
time_elpased: 1.435
batch start
#iterations: 94
currently lose_sum: 93.33558464050293
time_elpased: 1.453
batch start
#iterations: 95
currently lose_sum: 92.88968920707703
time_elpased: 1.464
batch start
#iterations: 96
currently lose_sum: 93.27314019203186
time_elpased: 1.455
batch start
#iterations: 97
currently lose_sum: 93.15528362989426
time_elpased: 1.44
batch start
#iterations: 98
currently lose_sum: 93.23875486850739
time_elpased: 1.44
batch start
#iterations: 99
currently lose_sum: 93.10307663679123
time_elpased: 1.449
start validation test
0.609690721649
0.624376667053
0.554080477514
0.587131952017
0.60978835401
63.412
batch start
#iterations: 100
currently lose_sum: 93.3993906378746
time_elpased: 1.445
batch start
#iterations: 101
currently lose_sum: 92.87310087680817
time_elpased: 1.458
batch start
#iterations: 102
currently lose_sum: 92.8062835931778
time_elpased: 1.435
batch start
#iterations: 103
currently lose_sum: 92.51599705219269
time_elpased: 1.436
batch start
#iterations: 104
currently lose_sum: 92.56847387552261
time_elpased: 1.468
batch start
#iterations: 105
currently lose_sum: 92.50012928247452
time_elpased: 1.474
batch start
#iterations: 106
currently lose_sum: 92.68488663434982
time_elpased: 1.443
batch start
#iterations: 107
currently lose_sum: 92.39171540737152
time_elpased: 1.472
batch start
#iterations: 108
currently lose_sum: 92.1839388012886
time_elpased: 1.443
batch start
#iterations: 109
currently lose_sum: 92.59631389379501
time_elpased: 1.435
batch start
#iterations: 110
currently lose_sum: 92.57046979665756
time_elpased: 1.469
batch start
#iterations: 111
currently lose_sum: 92.89643198251724
time_elpased: 1.455
batch start
#iterations: 112
currently lose_sum: 92.5040253996849
time_elpased: 1.432
batch start
#iterations: 113
currently lose_sum: 91.55815494060516
time_elpased: 1.457
batch start
#iterations: 114
currently lose_sum: 92.55004930496216
time_elpased: 1.423
batch start
#iterations: 115
currently lose_sum: 92.14778506755829
time_elpased: 1.438
batch start
#iterations: 116
currently lose_sum: 92.20879638195038
time_elpased: 1.443
batch start
#iterations: 117
currently lose_sum: 92.10362613201141
time_elpased: 1.44
batch start
#iterations: 118
currently lose_sum: 92.19530773162842
time_elpased: 1.419
batch start
#iterations: 119
currently lose_sum: 91.93526309728622
time_elpased: 1.435
start validation test
0.607989690722
0.616917626218
0.573428012761
0.594378366846
0.608050369078
63.621
batch start
#iterations: 120
currently lose_sum: 92.16906934976578
time_elpased: 1.445
batch start
#iterations: 121
currently lose_sum: 91.8440637588501
time_elpased: 1.458
batch start
#iterations: 122
currently lose_sum: 92.08669710159302
time_elpased: 1.436
batch start
#iterations: 123
currently lose_sum: 91.97397261857986
time_elpased: 1.454
batch start
#iterations: 124
currently lose_sum: 92.22417259216309
time_elpased: 1.452
batch start
#iterations: 125
currently lose_sum: 91.96437793970108
time_elpased: 1.477
batch start
#iterations: 126
currently lose_sum: 91.37879359722137
time_elpased: 1.447
batch start
#iterations: 127
currently lose_sum: 91.8867147564888
time_elpased: 1.463
batch start
#iterations: 128
currently lose_sum: 91.55493593215942
time_elpased: 1.427
batch start
#iterations: 129
currently lose_sum: 91.32159167528152
time_elpased: 1.445
batch start
#iterations: 130
currently lose_sum: 91.78421396017075
time_elpased: 1.447
batch start
#iterations: 131
currently lose_sum: 91.63009905815125
time_elpased: 1.53
batch start
#iterations: 132
currently lose_sum: 91.23225229978561
time_elpased: 1.431
batch start
#iterations: 133
currently lose_sum: 91.40741473436356
time_elpased: 1.469
batch start
#iterations: 134
currently lose_sum: 91.2905620932579
time_elpased: 1.44
batch start
#iterations: 135
currently lose_sum: 91.28750395774841
time_elpased: 1.467
batch start
#iterations: 136
currently lose_sum: 91.44655078649521
time_elpased: 1.454
batch start
#iterations: 137
currently lose_sum: 91.23287105560303
time_elpased: 1.472
batch start
#iterations: 138
currently lose_sum: 91.24547475576401
time_elpased: 1.436
batch start
#iterations: 139
currently lose_sum: 91.36799919605255
time_elpased: 1.449
start validation test
0.607319587629
0.607718361901
0.609344447875
0.608530318602
0.607316032674
63.767
batch start
#iterations: 140
currently lose_sum: 91.24227577447891
time_elpased: 1.441
batch start
#iterations: 141
currently lose_sum: 91.64737701416016
time_elpased: 1.466
batch start
#iterations: 142
currently lose_sum: 91.3510394692421
time_elpased: 1.433
batch start
#iterations: 143
currently lose_sum: 91.35152637958527
time_elpased: 1.453
batch start
#iterations: 144
currently lose_sum: 91.07329976558685
time_elpased: 1.423
batch start
#iterations: 145
currently lose_sum: 90.8642737865448
time_elpased: 1.481
batch start
#iterations: 146
currently lose_sum: 90.65627521276474
time_elpased: 1.469
batch start
#iterations: 147
currently lose_sum: 90.6988394856453
time_elpased: 1.46
batch start
#iterations: 148
currently lose_sum: 91.02658784389496
time_elpased: 1.44
batch start
#iterations: 149
currently lose_sum: 91.23243850469589
time_elpased: 1.439
batch start
#iterations: 150
currently lose_sum: 90.99650073051453
time_elpased: 1.475
batch start
#iterations: 151
currently lose_sum: 90.64402085542679
time_elpased: 1.46
batch start
#iterations: 152
currently lose_sum: 90.67999643087387
time_elpased: 1.444
batch start
#iterations: 153
currently lose_sum: 90.7977728843689
time_elpased: 1.454
batch start
#iterations: 154
currently lose_sum: 90.56585991382599
time_elpased: 1.449
batch start
#iterations: 155
currently lose_sum: 91.05464154481888
time_elpased: 1.437
batch start
#iterations: 156
currently lose_sum: 90.71215373277664
time_elpased: 1.457
batch start
#iterations: 157
currently lose_sum: 91.04812681674957
time_elpased: 1.461
batch start
#iterations: 158
currently lose_sum: 90.99951684474945
time_elpased: 1.444
batch start
#iterations: 159
currently lose_sum: 90.64666545391083
time_elpased: 1.419
start validation test
0.582525773196
0.635966386555
0.389420603067
0.483053552052
0.582864799106
65.763
batch start
#iterations: 160
currently lose_sum: 91.13856595754623
time_elpased: 1.449
batch start
#iterations: 161
currently lose_sum: 90.14723062515259
time_elpased: 1.457
batch start
#iterations: 162
currently lose_sum: 89.8245479464531
time_elpased: 1.412
batch start
#iterations: 163
currently lose_sum: 90.25359904766083
time_elpased: 1.454
batch start
#iterations: 164
currently lose_sum: 90.29538142681122
time_elpased: 1.455
batch start
#iterations: 165
currently lose_sum: 89.94366407394409
time_elpased: 1.468
batch start
#iterations: 166
currently lose_sum: 90.08098328113556
time_elpased: 1.433
batch start
#iterations: 167
currently lose_sum: 90.09426659345627
time_elpased: 1.453
batch start
#iterations: 168
currently lose_sum: 90.4606990814209
time_elpased: 1.446
batch start
#iterations: 169
currently lose_sum: 90.44629991054535
time_elpased: 1.467
batch start
#iterations: 170
currently lose_sum: 90.007588326931
time_elpased: 1.445
batch start
#iterations: 171
currently lose_sum: 89.7996169924736
time_elpased: 1.47
batch start
#iterations: 172
currently lose_sum: 90.4034321308136
time_elpased: 1.434
batch start
#iterations: 173
currently lose_sum: 90.05753898620605
time_elpased: 1.508
batch start
#iterations: 174
currently lose_sum: 89.9816118478775
time_elpased: 1.431
batch start
#iterations: 175
currently lose_sum: 90.16362899541855
time_elpased: 1.465
batch start
#iterations: 176
currently lose_sum: 90.2783260345459
time_elpased: 1.437
batch start
#iterations: 177
currently lose_sum: 89.40519499778748
time_elpased: 1.487
batch start
#iterations: 178
currently lose_sum: 89.23772841691971
time_elpased: 1.44
batch start
#iterations: 179
currently lose_sum: 89.9842882156372
time_elpased: 1.518
start validation test
0.585670103093
0.612911903161
0.468971904909
0.531366604478
0.585874984779
65.608
batch start
#iterations: 180
currently lose_sum: 89.61409139633179
time_elpased: 1.436
batch start
#iterations: 181
currently lose_sum: 89.69642853736877
time_elpased: 1.481
batch start
#iterations: 182
currently lose_sum: 89.69158548116684
time_elpased: 1.43
batch start
#iterations: 183
currently lose_sum: 89.33391779661179
time_elpased: 1.484
batch start
#iterations: 184
currently lose_sum: 89.12246608734131
time_elpased: 1.417
batch start
#iterations: 185
currently lose_sum: 89.70666354894638
time_elpased: 1.462
batch start
#iterations: 186
currently lose_sum: 89.53877502679825
time_elpased: 1.429
batch start
#iterations: 187
currently lose_sum: 89.32765609025955
time_elpased: 1.469
batch start
#iterations: 188
currently lose_sum: 89.76127624511719
time_elpased: 1.438
batch start
#iterations: 189
currently lose_sum: 89.46817290782928
time_elpased: 1.482
batch start
#iterations: 190
currently lose_sum: 89.6546516418457
time_elpased: 1.421
batch start
#iterations: 191
currently lose_sum: 89.21176892518997
time_elpased: 1.471
batch start
#iterations: 192
currently lose_sum: 88.88555616140366
time_elpased: 1.451
batch start
#iterations: 193
currently lose_sum: 89.33918696641922
time_elpased: 1.452
batch start
#iterations: 194
currently lose_sum: 89.05017387866974
time_elpased: 1.431
batch start
#iterations: 195
currently lose_sum: 89.38339149951935
time_elpased: 1.485
batch start
#iterations: 196
currently lose_sum: 89.0982893705368
time_elpased: 1.427
batch start
#iterations: 197
currently lose_sum: 89.2263600230217
time_elpased: 1.445
batch start
#iterations: 198
currently lose_sum: 88.78579151630402
time_elpased: 1.422
batch start
#iterations: 199
currently lose_sum: 89.03770160675049
time_elpased: 1.438
start validation test
0.593505154639
0.604425687236
0.545332921684
0.573360744428
0.593589728424
64.854
batch start
#iterations: 200
currently lose_sum: 89.04805094003677
time_elpased: 1.434
batch start
#iterations: 201
currently lose_sum: 89.0742216706276
time_elpased: 1.453
batch start
#iterations: 202
currently lose_sum: 89.05561685562134
time_elpased: 1.424
batch start
#iterations: 203
currently lose_sum: 89.02229738235474
time_elpased: 1.455
batch start
#iterations: 204
currently lose_sum: 88.80017238855362
time_elpased: 1.453
batch start
#iterations: 205
currently lose_sum: 89.08079034090042
time_elpased: 1.464
batch start
#iterations: 206
currently lose_sum: 88.6300379037857
time_elpased: 1.48
batch start
#iterations: 207
currently lose_sum: 88.42357462644577
time_elpased: 1.449
batch start
#iterations: 208
currently lose_sum: 88.3742544054985
time_elpased: 1.466
batch start
#iterations: 209
currently lose_sum: 88.8266087770462
time_elpased: 1.477
batch start
#iterations: 210
currently lose_sum: 88.79741221666336
time_elpased: 1.447
batch start
#iterations: 211
currently lose_sum: 89.15942358970642
time_elpased: 1.488
batch start
#iterations: 212
currently lose_sum: 88.19410544633865
time_elpased: 1.439
batch start
#iterations: 213
currently lose_sum: 88.57564604282379
time_elpased: 1.485
batch start
#iterations: 214
currently lose_sum: 88.65580916404724
time_elpased: 1.418
batch start
#iterations: 215
currently lose_sum: 88.80189085006714
time_elpased: 1.478
batch start
#iterations: 216
currently lose_sum: 88.6752820611
time_elpased: 1.424
batch start
#iterations: 217
currently lose_sum: 88.59488040208817
time_elpased: 1.451
batch start
#iterations: 218
currently lose_sum: 88.30149871110916
time_elpased: 1.436
batch start
#iterations: 219
currently lose_sum: 88.53818756341934
time_elpased: 1.452
start validation test
0.605154639175
0.608939730961
0.591643511372
0.600167032049
0.605178360044
64.500
batch start
#iterations: 220
currently lose_sum: 88.58842658996582
time_elpased: 1.447
batch start
#iterations: 221
currently lose_sum: 88.44161230325699
time_elpased: 1.441
batch start
#iterations: 222
currently lose_sum: 88.23693782091141
time_elpased: 1.497
batch start
#iterations: 223
currently lose_sum: 88.00965565443039
time_elpased: 1.448
batch start
#iterations: 224
currently lose_sum: 88.16124665737152
time_elpased: 1.446
batch start
#iterations: 225
currently lose_sum: 88.0677804350853
time_elpased: 1.493
batch start
#iterations: 226
currently lose_sum: 87.93876737356186
time_elpased: 1.429
batch start
#iterations: 227
currently lose_sum: 88.12318801879883
time_elpased: 1.465
batch start
#iterations: 228
currently lose_sum: 87.89318126440048
time_elpased: 1.436
batch start
#iterations: 229
currently lose_sum: 88.58979272842407
time_elpased: 1.488
batch start
#iterations: 230
currently lose_sum: 87.67525362968445
time_elpased: 1.433
batch start
#iterations: 231
currently lose_sum: 88.06800091266632
time_elpased: 1.485
batch start
#iterations: 232
currently lose_sum: 88.03385895490646
time_elpased: 1.42
batch start
#iterations: 233
currently lose_sum: 88.07618391513824
time_elpased: 1.455
batch start
#iterations: 234
currently lose_sum: 88.0740875005722
time_elpased: 1.446
batch start
#iterations: 235
currently lose_sum: 88.47059321403503
time_elpased: 1.453
batch start
#iterations: 236
currently lose_sum: 87.56683611869812
time_elpased: 1.467
batch start
#iterations: 237
currently lose_sum: 88.39973193407059
time_elpased: 1.446
batch start
#iterations: 238
currently lose_sum: 87.84271931648254
time_elpased: 1.492
batch start
#iterations: 239
currently lose_sum: 87.54140311479568
time_elpased: 1.44
start validation test
0.593041237113
0.608452380952
0.525985386436
0.564221449467
0.593158964002
65.491
batch start
#iterations: 240
currently lose_sum: 87.98550951480865
time_elpased: 1.417
batch start
#iterations: 241
currently lose_sum: 88.62942171096802
time_elpased: 1.44
batch start
#iterations: 242
currently lose_sum: 87.91549277305603
time_elpased: 1.442
batch start
#iterations: 243
currently lose_sum: 87.95910459756851
time_elpased: 1.459
batch start
#iterations: 244
currently lose_sum: 87.99180281162262
time_elpased: 1.436
batch start
#iterations: 245
currently lose_sum: 87.4071986079216
time_elpased: 1.469
batch start
#iterations: 246
currently lose_sum: 87.90604341030121
time_elpased: 1.429
batch start
#iterations: 247
currently lose_sum: 87.64911240339279
time_elpased: 1.47
batch start
#iterations: 248
currently lose_sum: 87.66638708114624
time_elpased: 1.432
batch start
#iterations: 249
currently lose_sum: 87.44399005174637
time_elpased: 1.469
batch start
#iterations: 250
currently lose_sum: 87.77689564228058
time_elpased: 1.46
batch start
#iterations: 251
currently lose_sum: 87.24824976921082
time_elpased: 1.453
batch start
#iterations: 252
currently lose_sum: 88.21758371591568
time_elpased: 1.482
batch start
#iterations: 253
currently lose_sum: 87.27830654382706
time_elpased: 1.458
batch start
#iterations: 254
currently lose_sum: 87.81501793861389
time_elpased: 1.437
batch start
#iterations: 255
currently lose_sum: 87.73787665367126
time_elpased: 1.433
batch start
#iterations: 256
currently lose_sum: 87.23810827732086
time_elpased: 1.503
batch start
#iterations: 257
currently lose_sum: 87.92136371135712
time_elpased: 1.431
batch start
#iterations: 258
currently lose_sum: 87.24055659770966
time_elpased: 1.457
batch start
#iterations: 259
currently lose_sum: 87.21216696500778
time_elpased: 1.492
start validation test
0.588092783505
0.606097860831
0.507358238139
0.552350008403
0.588234525452
66.212
batch start
#iterations: 260
currently lose_sum: 87.80994838476181
time_elpased: 1.437
batch start
#iterations: 261
currently lose_sum: 87.53019553422928
time_elpased: 1.427
batch start
#iterations: 262
currently lose_sum: 87.57035261392593
time_elpased: 1.421
batch start
#iterations: 263
currently lose_sum: 87.20800095796585
time_elpased: 1.454
batch start
#iterations: 264
currently lose_sum: 88.00541454553604
time_elpased: 1.438
batch start
#iterations: 265
currently lose_sum: 87.19810712337494
time_elpased: 1.436
batch start
#iterations: 266
currently lose_sum: 87.12530344724655
time_elpased: 1.46
batch start
#iterations: 267
currently lose_sum: 87.1106738448143
time_elpased: 1.477
batch start
#iterations: 268
currently lose_sum: 87.31680411100388
time_elpased: 1.464
batch start
#iterations: 269
currently lose_sum: 87.56223225593567
time_elpased: 1.503
batch start
#iterations: 270
currently lose_sum: 86.80246031284332
time_elpased: 1.45
batch start
#iterations: 271
currently lose_sum: 87.2855052947998
time_elpased: 1.431
batch start
#iterations: 272
currently lose_sum: 87.02357029914856
time_elpased: 1.429
batch start
#iterations: 273
currently lose_sum: 87.1378282904625
time_elpased: 1.444
batch start
#iterations: 274
currently lose_sum: 88.06227457523346
time_elpased: 1.432
batch start
#iterations: 275
currently lose_sum: 86.97261840105057
time_elpased: 1.465
batch start
#iterations: 276
currently lose_sum: 86.70749759674072
time_elpased: 1.466
batch start
#iterations: 277
currently lose_sum: 86.83140051364899
time_elpased: 1.478
batch start
#iterations: 278
currently lose_sum: 87.46160489320755
time_elpased: 1.445
batch start
#iterations: 279
currently lose_sum: 86.84690856933594
time_elpased: 1.441
start validation test
0.583762886598
0.607827685842
0.476278686838
0.534071894293
0.583951591689
67.123
batch start
#iterations: 280
currently lose_sum: 87.5408245921135
time_elpased: 1.448
batch start
#iterations: 281
currently lose_sum: 87.02268022298813
time_elpased: 1.473
batch start
#iterations: 282
currently lose_sum: 86.65626311302185
time_elpased: 1.437
batch start
#iterations: 283
currently lose_sum: 86.45321464538574
time_elpased: 1.463
batch start
#iterations: 284
currently lose_sum: 86.53719866275787
time_elpased: 1.461
batch start
#iterations: 285
currently lose_sum: 87.16649997234344
time_elpased: 1.466
batch start
#iterations: 286
currently lose_sum: 86.73794370889664
time_elpased: 1.432
batch start
#iterations: 287
currently lose_sum: 87.0759665966034
time_elpased: 1.487
batch start
#iterations: 288
currently lose_sum: 86.60262954235077
time_elpased: 1.421
batch start
#iterations: 289
currently lose_sum: 86.4468446969986
time_elpased: 1.461
batch start
#iterations: 290
currently lose_sum: 87.378457903862
time_elpased: 1.426
batch start
#iterations: 291
currently lose_sum: 86.39558678865433
time_elpased: 1.46
batch start
#iterations: 292
currently lose_sum: 86.61625510454178
time_elpased: 1.436
batch start
#iterations: 293
currently lose_sum: 86.03976231813431
time_elpased: 1.469
batch start
#iterations: 294
currently lose_sum: 86.50419008731842
time_elpased: 1.483
batch start
#iterations: 295
currently lose_sum: 86.32833254337311
time_elpased: 1.483
batch start
#iterations: 296
currently lose_sum: 86.31129783391953
time_elpased: 1.42
batch start
#iterations: 297
currently lose_sum: 86.43816578388214
time_elpased: 1.439
batch start
#iterations: 298
currently lose_sum: 86.09201443195343
time_elpased: 1.451
batch start
#iterations: 299
currently lose_sum: 85.84925037622452
time_elpased: 1.439
start validation test
0.588865979381
0.613835491042
0.483070906658
0.54065883437
0.589051718949
66.772
batch start
#iterations: 300
currently lose_sum: 86.37002432346344
time_elpased: 1.456
batch start
#iterations: 301
currently lose_sum: 86.48899400234222
time_elpased: 1.425
batch start
#iterations: 302
currently lose_sum: 87.16496884822845
time_elpased: 1.426
batch start
#iterations: 303
currently lose_sum: 86.13587552309036
time_elpased: 1.523
batch start
#iterations: 304
currently lose_sum: 86.59730964899063
time_elpased: 1.434
batch start
#iterations: 305
currently lose_sum: 86.46199655532837
time_elpased: 1.467
batch start
#iterations: 306
currently lose_sum: 85.9848228096962
time_elpased: 1.429
batch start
#iterations: 307
currently lose_sum: 86.44212079048157
time_elpased: 1.468
batch start
#iterations: 308
currently lose_sum: 86.05929052829742
time_elpased: 1.433
batch start
#iterations: 309
currently lose_sum: 86.14659959077835
time_elpased: 1.453
batch start
#iterations: 310
currently lose_sum: 85.56966239213943
time_elpased: 1.431
batch start
#iterations: 311
currently lose_sum: 86.24255686998367
time_elpased: 1.459
batch start
#iterations: 312
currently lose_sum: 85.56934213638306
time_elpased: 1.432
batch start
#iterations: 313
currently lose_sum: 86.1418708562851
time_elpased: 1.46
batch start
#iterations: 314
currently lose_sum: 86.1674730181694
time_elpased: 1.428
batch start
#iterations: 315
currently lose_sum: 85.95773178339005
time_elpased: 1.434
batch start
#iterations: 316
currently lose_sum: 86.18607687950134
time_elpased: 1.444
batch start
#iterations: 317
currently lose_sum: 86.1607438325882
time_elpased: 1.468
batch start
#iterations: 318
currently lose_sum: 86.21869027614594
time_elpased: 1.45
batch start
#iterations: 319
currently lose_sum: 85.77134984731674
time_elpased: 1.472
start validation test
0.587989690722
0.615829078205
0.471647627869
0.534180313538
0.588193947158
67.172
batch start
#iterations: 320
currently lose_sum: 86.2619194984436
time_elpased: 1.434
batch start
#iterations: 321
currently lose_sum: 86.594662129879
time_elpased: 1.483
batch start
#iterations: 322
currently lose_sum: 86.26345551013947
time_elpased: 1.448
batch start
#iterations: 323
currently lose_sum: 86.75722509622574
time_elpased: 1.45
batch start
#iterations: 324
currently lose_sum: 86.00223356485367
time_elpased: 1.474
batch start
#iterations: 325
currently lose_sum: 86.25702458620071
time_elpased: 1.447
batch start
#iterations: 326
currently lose_sum: 85.50409436225891
time_elpased: 1.453
batch start
#iterations: 327
currently lose_sum: 86.28080582618713
time_elpased: 1.441
batch start
#iterations: 328
currently lose_sum: 85.61451590061188
time_elpased: 1.426
batch start
#iterations: 329
currently lose_sum: 85.80770951509476
time_elpased: 1.45
batch start
#iterations: 330
currently lose_sum: 85.53304207324982
time_elpased: 1.43
batch start
#iterations: 331
currently lose_sum: 85.53541630506516
time_elpased: 1.43
batch start
#iterations: 332
currently lose_sum: 85.59021413326263
time_elpased: 1.449
batch start
#iterations: 333
currently lose_sum: 85.43621379137039
time_elpased: 1.478
batch start
#iterations: 334
currently lose_sum: 85.65602451562881
time_elpased: 1.431
batch start
#iterations: 335
currently lose_sum: 85.07898950576782
time_elpased: 1.434
batch start
#iterations: 336
currently lose_sum: 85.8369710445404
time_elpased: 1.44
batch start
#iterations: 337
currently lose_sum: 85.6303848028183
time_elpased: 1.451
batch start
#iterations: 338
currently lose_sum: 85.25708812475204
time_elpased: 1.46
batch start
#iterations: 339
currently lose_sum: 85.70210057497025
time_elpased: 1.48
start validation test
0.575773195876
0.617735550277
0.401461356386
0.486651696607
0.576079227196
68.857
batch start
#iterations: 340
currently lose_sum: 85.28308111429214
time_elpased: 1.411
batch start
#iterations: 341
currently lose_sum: 86.01218777894974
time_elpased: 1.445
batch start
#iterations: 342
currently lose_sum: 85.57669460773468
time_elpased: 1.519
batch start
#iterations: 343
currently lose_sum: 85.6670104265213
time_elpased: 1.451
batch start
#iterations: 344
currently lose_sum: 85.59085267782211
time_elpased: 1.464
batch start
#iterations: 345
currently lose_sum: 85.12743246555328
time_elpased: 1.472
batch start
#iterations: 346
currently lose_sum: 85.37490689754486
time_elpased: 1.432
batch start
#iterations: 347
currently lose_sum: 85.3862891793251
time_elpased: 1.446
batch start
#iterations: 348
currently lose_sum: 85.30437338352203
time_elpased: 1.428
batch start
#iterations: 349
currently lose_sum: 85.41536474227905
time_elpased: 1.475
batch start
#iterations: 350
currently lose_sum: 85.22288399934769
time_elpased: 1.414
batch start
#iterations: 351
currently lose_sum: 84.94834125041962
time_elpased: 1.463
batch start
#iterations: 352
currently lose_sum: 84.89479857683182
time_elpased: 1.455
batch start
#iterations: 353
currently lose_sum: 84.80018085241318
time_elpased: 1.438
batch start
#iterations: 354
currently lose_sum: 85.5003399848938
time_elpased: 1.437
batch start
#iterations: 355
currently lose_sum: 85.41669994592667
time_elpased: 1.454
batch start
#iterations: 356
currently lose_sum: 84.99959260225296
time_elpased: 1.423
batch start
#iterations: 357
currently lose_sum: 85.10236719250679
time_elpased: 1.442
batch start
#iterations: 358
currently lose_sum: 85.30547946691513
time_elpased: 1.473
batch start
#iterations: 359
currently lose_sum: 84.9887284040451
time_elpased: 1.483
start validation test
0.582783505155
0.613164133315
0.452505917464
0.520724774988
0.583012227554
67.731
batch start
#iterations: 360
currently lose_sum: 85.26098501682281
time_elpased: 1.44
batch start
#iterations: 361
currently lose_sum: 85.43714773654938
time_elpased: 1.448
batch start
#iterations: 362
currently lose_sum: 85.51871621608734
time_elpased: 1.433
batch start
#iterations: 363
currently lose_sum: 85.29977545142174
time_elpased: 1.439
batch start
#iterations: 364
currently lose_sum: 84.87307649850845
time_elpased: 1.416
batch start
#iterations: 365
currently lose_sum: 84.94459909200668
time_elpased: 1.451
batch start
#iterations: 366
currently lose_sum: 84.69726979732513
time_elpased: 1.434
batch start
#iterations: 367
currently lose_sum: 84.84257727861404
time_elpased: 1.435
batch start
#iterations: 368
currently lose_sum: 85.16618657112122
time_elpased: 1.436
batch start
#iterations: 369
currently lose_sum: 84.81568992137909
time_elpased: 1.437
batch start
#iterations: 370
currently lose_sum: 84.90235501527786
time_elpased: 1.414
batch start
#iterations: 371
currently lose_sum: 84.56289267539978
time_elpased: 1.471
batch start
#iterations: 372
currently lose_sum: 84.73016476631165
time_elpased: 1.426
batch start
#iterations: 373
currently lose_sum: 85.15544283390045
time_elpased: 1.473
batch start
#iterations: 374
currently lose_sum: 84.77057242393494
time_elpased: 1.434
batch start
#iterations: 375
currently lose_sum: 84.95930522680283
time_elpased: 1.493
batch start
#iterations: 376
currently lose_sum: 85.21326166391373
time_elpased: 1.443
batch start
#iterations: 377
currently lose_sum: 84.68300849199295
time_elpased: 1.475
batch start
#iterations: 378
currently lose_sum: 84.71295511722565
time_elpased: 1.449
batch start
#iterations: 379
currently lose_sum: 84.7985817193985
time_elpased: 1.452
start validation test
0.577113402062
0.608770668584
0.435731192755
0.507917466411
0.577361620337
68.746
batch start
#iterations: 380
currently lose_sum: 84.71465617418289
time_elpased: 1.424
batch start
#iterations: 381
currently lose_sum: 84.7365083694458
time_elpased: 1.429
batch start
#iterations: 382
currently lose_sum: 84.81062626838684
time_elpased: 1.466
batch start
#iterations: 383
currently lose_sum: 84.60213959217072
time_elpased: 1.455
batch start
#iterations: 384
currently lose_sum: 84.7023137807846
time_elpased: 1.455
batch start
#iterations: 385
currently lose_sum: 84.41199696063995
time_elpased: 1.443
batch start
#iterations: 386
currently lose_sum: 84.91003084182739
time_elpased: 1.449
batch start
#iterations: 387
currently lose_sum: 84.79967564344406
time_elpased: 1.463
batch start
#iterations: 388
currently lose_sum: 84.4786845445633
time_elpased: 1.462
batch start
#iterations: 389
currently lose_sum: 85.00184452533722
time_elpased: 1.45
batch start
#iterations: 390
currently lose_sum: 84.29968225955963
time_elpased: 1.434
batch start
#iterations: 391
currently lose_sum: 84.65649795532227
time_elpased: 1.474
batch start
#iterations: 392
currently lose_sum: 84.70583087205887
time_elpased: 1.419
batch start
#iterations: 393
currently lose_sum: 84.50164049863815
time_elpased: 1.456
batch start
#iterations: 394
currently lose_sum: 84.60434848070145
time_elpased: 1.434
batch start
#iterations: 395
currently lose_sum: 84.69176959991455
time_elpased: 1.471
batch start
#iterations: 396
currently lose_sum: 84.60940927267075
time_elpased: 1.443
batch start
#iterations: 397
currently lose_sum: 84.27896684408188
time_elpased: 1.449
batch start
#iterations: 398
currently lose_sum: 85.12254673242569
time_elpased: 1.44
batch start
#iterations: 399
currently lose_sum: 84.44475024938583
time_elpased: 1.476
start validation test
0.581030927835
0.591648402353
0.527837810024
0.557924507778
0.581124316558
67.733
acc: 0.618
pre: 0.616
rec: 0.628
F1: 0.622
auc: 0.658
