start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.44430273771286
time_elpased: 1.521
batch start
#iterations: 1
currently lose_sum: 100.19064021110535
time_elpased: 1.499
batch start
#iterations: 2
currently lose_sum: 99.99062728881836
time_elpased: 1.471
batch start
#iterations: 3
currently lose_sum: 99.61038303375244
time_elpased: 1.516
batch start
#iterations: 4
currently lose_sum: 99.57527029514313
time_elpased: 1.494
batch start
#iterations: 5
currently lose_sum: 99.54194194078445
time_elpased: 1.512
batch start
#iterations: 6
currently lose_sum: 99.09875851869583
time_elpased: 1.526
batch start
#iterations: 7
currently lose_sum: 99.19986575841904
time_elpased: 1.507
batch start
#iterations: 8
currently lose_sum: 98.66830545663834
time_elpased: 1.54
batch start
#iterations: 9
currently lose_sum: 98.54595518112183
time_elpased: 1.545
batch start
#iterations: 10
currently lose_sum: 98.53914189338684
time_elpased: 1.519
batch start
#iterations: 11
currently lose_sum: 98.63895046710968
time_elpased: 1.47
batch start
#iterations: 12
currently lose_sum: 98.57493537664413
time_elpased: 1.498
batch start
#iterations: 13
currently lose_sum: 98.22681802511215
time_elpased: 1.51
batch start
#iterations: 14
currently lose_sum: 98.12391126155853
time_elpased: 1.493
batch start
#iterations: 15
currently lose_sum: 98.06731659173965
time_elpased: 1.507
batch start
#iterations: 16
currently lose_sum: 97.80309700965881
time_elpased: 1.509
batch start
#iterations: 17
currently lose_sum: 97.99211889505386
time_elpased: 1.495
batch start
#iterations: 18
currently lose_sum: 97.70065355300903
time_elpased: 1.496
batch start
#iterations: 19
currently lose_sum: 97.64235353469849
time_elpased: 1.513
start validation test
0.611391752577
0.617831638174
0.587629926932
0.602352444749
0.611433470127
63.600
batch start
#iterations: 20
currently lose_sum: 97.27107393741608
time_elpased: 1.483
batch start
#iterations: 21
currently lose_sum: 97.50322890281677
time_elpased: 1.501
batch start
#iterations: 22
currently lose_sum: 97.38550180196762
time_elpased: 1.486
batch start
#iterations: 23
currently lose_sum: 97.0477944612503
time_elpased: 1.504
batch start
#iterations: 24
currently lose_sum: 96.98935008049011
time_elpased: 1.486
batch start
#iterations: 25
currently lose_sum: 97.35129928588867
time_elpased: 1.483
batch start
#iterations: 26
currently lose_sum: 96.84225022792816
time_elpased: 1.523
batch start
#iterations: 27
currently lose_sum: 97.02337169647217
time_elpased: 1.519
batch start
#iterations: 28
currently lose_sum: 97.09381359815598
time_elpased: 1.49
batch start
#iterations: 29
currently lose_sum: 96.94310265779495
time_elpased: 1.488
batch start
#iterations: 30
currently lose_sum: 96.46881073713303
time_elpased: 1.505
batch start
#iterations: 31
currently lose_sum: 96.40772598981857
time_elpased: 1.498
batch start
#iterations: 32
currently lose_sum: 96.34970480203629
time_elpased: 1.499
batch start
#iterations: 33
currently lose_sum: 96.40617060661316
time_elpased: 1.486
batch start
#iterations: 34
currently lose_sum: 96.24412578344345
time_elpased: 1.564
batch start
#iterations: 35
currently lose_sum: 96.33631747961044
time_elpased: 1.497
batch start
#iterations: 36
currently lose_sum: 95.86080032587051
time_elpased: 1.512
batch start
#iterations: 37
currently lose_sum: 95.94782131910324
time_elpased: 1.504
batch start
#iterations: 38
currently lose_sum: 95.65097612142563
time_elpased: 1.486
batch start
#iterations: 39
currently lose_sum: 95.92301970720291
time_elpased: 1.497
start validation test
0.625567010309
0.611489864558
0.692291859627
0.649387006468
0.625449864545
63.062
batch start
#iterations: 40
currently lose_sum: 95.62431561946869
time_elpased: 1.53
batch start
#iterations: 41
currently lose_sum: 95.52168875932693
time_elpased: 1.501
batch start
#iterations: 42
currently lose_sum: 95.32185995578766
time_elpased: 1.473
batch start
#iterations: 43
currently lose_sum: 95.1488327383995
time_elpased: 1.506
batch start
#iterations: 44
currently lose_sum: 95.28149771690369
time_elpased: 1.493
batch start
#iterations: 45
currently lose_sum: 95.15568697452545
time_elpased: 1.557
batch start
#iterations: 46
currently lose_sum: 95.06792944669724
time_elpased: 1.478
batch start
#iterations: 47
currently lose_sum: 95.18539917469025
time_elpased: 1.499
batch start
#iterations: 48
currently lose_sum: 95.08230084180832
time_elpased: 1.482
batch start
#iterations: 49
currently lose_sum: 94.871955037117
time_elpased: 1.511
batch start
#iterations: 50
currently lose_sum: 94.95780092477798
time_elpased: 1.537
batch start
#iterations: 51
currently lose_sum: 94.79854929447174
time_elpased: 1.495
batch start
#iterations: 52
currently lose_sum: 94.78532737493515
time_elpased: 1.518
batch start
#iterations: 53
currently lose_sum: 94.61445021629333
time_elpased: 1.529
batch start
#iterations: 54
currently lose_sum: 94.05029368400574
time_elpased: 1.496
batch start
#iterations: 55
currently lose_sum: 94.38324296474457
time_elpased: 1.475
batch start
#iterations: 56
currently lose_sum: 94.30178970098495
time_elpased: 1.489
batch start
#iterations: 57
currently lose_sum: 94.06182116270065
time_elpased: 1.481
batch start
#iterations: 58
currently lose_sum: 94.16496908664703
time_elpased: 1.518
batch start
#iterations: 59
currently lose_sum: 94.19668310880661
time_elpased: 1.497
start validation test
0.62175257732
0.64371148967
0.548317381908
0.592197399133
0.621881504132
62.682
batch start
#iterations: 60
currently lose_sum: 94.26180928945541
time_elpased: 1.585
batch start
#iterations: 61
currently lose_sum: 93.51898699998856
time_elpased: 1.501
batch start
#iterations: 62
currently lose_sum: 94.17168003320694
time_elpased: 1.534
batch start
#iterations: 63
currently lose_sum: 93.8975345492363
time_elpased: 1.489
batch start
#iterations: 64
currently lose_sum: 93.6025573015213
time_elpased: 1.495
batch start
#iterations: 65
currently lose_sum: 93.03479397296906
time_elpased: 1.502
batch start
#iterations: 66
currently lose_sum: 93.2202565073967
time_elpased: 1.497
batch start
#iterations: 67
currently lose_sum: 93.1161316037178
time_elpased: 1.509
batch start
#iterations: 68
currently lose_sum: 92.62142843008041
time_elpased: 1.502
batch start
#iterations: 69
currently lose_sum: 93.34638565778732
time_elpased: 1.504
batch start
#iterations: 70
currently lose_sum: 92.77543312311172
time_elpased: 1.468
batch start
#iterations: 71
currently lose_sum: 93.22209292650223
time_elpased: 1.479
batch start
#iterations: 72
currently lose_sum: 92.82782340049744
time_elpased: 1.527
batch start
#iterations: 73
currently lose_sum: 93.1198570728302
time_elpased: 1.515
batch start
#iterations: 74
currently lose_sum: 93.12414628267288
time_elpased: 1.556
batch start
#iterations: 75
currently lose_sum: 92.63368546962738
time_elpased: 1.487
batch start
#iterations: 76
currently lose_sum: 92.56973999738693
time_elpased: 1.484
batch start
#iterations: 77
currently lose_sum: 92.20203685760498
time_elpased: 1.513
batch start
#iterations: 78
currently lose_sum: 92.9126324057579
time_elpased: 1.475
batch start
#iterations: 79
currently lose_sum: 92.64567041397095
time_elpased: 1.522
start validation test
0.610824742268
0.6245545465
0.559123186169
0.590030408341
0.610915512324
62.920
batch start
#iterations: 80
currently lose_sum: 92.670359313488
time_elpased: 1.484
batch start
#iterations: 81
currently lose_sum: 92.2488185763359
time_elpased: 1.482
batch start
#iterations: 82
currently lose_sum: 92.33230525255203
time_elpased: 1.536
batch start
#iterations: 83
currently lose_sum: 91.85266929864883
time_elpased: 1.466
batch start
#iterations: 84
currently lose_sum: 92.19965445995331
time_elpased: 1.523
batch start
#iterations: 85
currently lose_sum: 92.23587894439697
time_elpased: 1.492
batch start
#iterations: 86
currently lose_sum: 91.78945595026016
time_elpased: 1.478
batch start
#iterations: 87
currently lose_sum: 92.01348149776459
time_elpased: 1.509
batch start
#iterations: 88
currently lose_sum: 92.10768407583237
time_elpased: 1.49
batch start
#iterations: 89
currently lose_sum: 91.76827383041382
time_elpased: 1.535
batch start
#iterations: 90
currently lose_sum: 92.05011004209518
time_elpased: 1.508
batch start
#iterations: 91
currently lose_sum: 91.4940083026886
time_elpased: 1.5
batch start
#iterations: 92
currently lose_sum: 91.78740054368973
time_elpased: 1.543
batch start
#iterations: 93
currently lose_sum: 91.56082707643509
time_elpased: 1.522
batch start
#iterations: 94
currently lose_sum: 91.23209881782532
time_elpased: 1.542
batch start
#iterations: 95
currently lose_sum: 91.25688290596008
time_elpased: 1.478
batch start
#iterations: 96
currently lose_sum: 91.186363697052
time_elpased: 1.47
batch start
#iterations: 97
currently lose_sum: 91.2897692322731
time_elpased: 1.497
batch start
#iterations: 98
currently lose_sum: 91.4284924864769
time_elpased: 1.482
batch start
#iterations: 99
currently lose_sum: 91.35219299793243
time_elpased: 1.474
start validation test
0.623144329897
0.623308733087
0.62581043532
0.624557079033
0.623139649138
62.921
batch start
#iterations: 100
currently lose_sum: 91.72056180238724
time_elpased: 1.481
batch start
#iterations: 101
currently lose_sum: 91.40427482128143
time_elpased: 1.515
batch start
#iterations: 102
currently lose_sum: 91.24437922239304
time_elpased: 1.482
batch start
#iterations: 103
currently lose_sum: 91.33406913280487
time_elpased: 1.469
batch start
#iterations: 104
currently lose_sum: 90.89367580413818
time_elpased: 1.547
batch start
#iterations: 105
currently lose_sum: 90.0714311003685
time_elpased: 1.474
batch start
#iterations: 106
currently lose_sum: 90.53393137454987
time_elpased: 1.482
batch start
#iterations: 107
currently lose_sum: 90.80340319871902
time_elpased: 1.478
batch start
#iterations: 108
currently lose_sum: 90.48748511075974
time_elpased: 1.532
batch start
#iterations: 109
currently lose_sum: 90.61889094114304
time_elpased: 1.514
batch start
#iterations: 110
currently lose_sum: 91.09679901599884
time_elpased: 1.49
batch start
#iterations: 111
currently lose_sum: 90.11545610427856
time_elpased: 1.487
batch start
#iterations: 112
currently lose_sum: 90.18034791946411
time_elpased: 1.486
batch start
#iterations: 113
currently lose_sum: 90.43024080991745
time_elpased: 1.566
batch start
#iterations: 114
currently lose_sum: 90.66391378641129
time_elpased: 1.542
batch start
#iterations: 115
currently lose_sum: 90.38551884889603
time_elpased: 1.522
batch start
#iterations: 116
currently lose_sum: 89.332848072052
time_elpased: 1.503
batch start
#iterations: 117
currently lose_sum: 89.68152803182602
time_elpased: 1.485
batch start
#iterations: 118
currently lose_sum: 89.8603515625
time_elpased: 1.516
batch start
#iterations: 119
currently lose_sum: 89.64415049552917
time_elpased: 1.495
start validation test
0.601649484536
0.610340619106
0.566121230833
0.587399893219
0.601711859865
63.918
batch start
#iterations: 120
currently lose_sum: 89.45205438137054
time_elpased: 1.502
batch start
#iterations: 121
currently lose_sum: 89.60537678003311
time_elpased: 1.549
batch start
#iterations: 122
currently lose_sum: 89.41745841503143
time_elpased: 1.516
batch start
#iterations: 123
currently lose_sum: 90.16136765480042
time_elpased: 1.481
batch start
#iterations: 124
currently lose_sum: 90.28397613763809
time_elpased: 1.505
batch start
#iterations: 125
currently lose_sum: 89.30957996845245
time_elpased: 1.488
batch start
#iterations: 126
currently lose_sum: 89.97715610265732
time_elpased: 1.488
batch start
#iterations: 127
currently lose_sum: 89.4331379532814
time_elpased: 1.527
batch start
#iterations: 128
currently lose_sum: 89.32073700428009
time_elpased: 1.493
batch start
#iterations: 129
currently lose_sum: 89.90344911813736
time_elpased: 1.516
batch start
#iterations: 130
currently lose_sum: 89.23222959041595
time_elpased: 1.481
batch start
#iterations: 131
currently lose_sum: 89.49677497148514
time_elpased: 1.49
batch start
#iterations: 132
currently lose_sum: 88.63640981912613
time_elpased: 1.491
batch start
#iterations: 133
currently lose_sum: 89.2513085603714
time_elpased: 1.532
batch start
#iterations: 134
currently lose_sum: 89.91594797372818
time_elpased: 1.494
batch start
#iterations: 135
currently lose_sum: 89.08351844549179
time_elpased: 1.48
batch start
#iterations: 136
currently lose_sum: 89.07951247692108
time_elpased: 1.508
batch start
#iterations: 137
currently lose_sum: 89.50826919078827
time_elpased: 1.499
batch start
#iterations: 138
currently lose_sum: 88.6025362610817
time_elpased: 1.533
batch start
#iterations: 139
currently lose_sum: 88.61407345533371
time_elpased: 1.49
start validation test
0.613608247423
0.613629387087
0.617165791911
0.615392508979
0.613602001605
63.881
batch start
#iterations: 140
currently lose_sum: 88.8175173997879
time_elpased: 1.503
batch start
#iterations: 141
currently lose_sum: 88.51734334230423
time_elpased: 1.534
batch start
#iterations: 142
currently lose_sum: 88.53800535202026
time_elpased: 1.508
batch start
#iterations: 143
currently lose_sum: 88.23017799854279
time_elpased: 1.498
batch start
#iterations: 144
currently lose_sum: 88.65956526994705
time_elpased: 1.482
batch start
#iterations: 145
currently lose_sum: 88.30333971977234
time_elpased: 1.471
batch start
#iterations: 146
currently lose_sum: 88.23246502876282
time_elpased: 1.505
batch start
#iterations: 147
currently lose_sum: 88.8135889172554
time_elpased: 1.498
batch start
#iterations: 148
currently lose_sum: 88.213653922081
time_elpased: 1.487
batch start
#iterations: 149
currently lose_sum: 87.8771653175354
time_elpased: 1.548
batch start
#iterations: 150
currently lose_sum: 88.31084138154984
time_elpased: 1.498
batch start
#iterations: 151
currently lose_sum: 88.2229511141777
time_elpased: 1.508
batch start
#iterations: 152
currently lose_sum: 88.38940966129303
time_elpased: 1.513
batch start
#iterations: 153
currently lose_sum: 87.73096787929535
time_elpased: 1.524
batch start
#iterations: 154
currently lose_sum: 87.94085365533829
time_elpased: 1.497
batch start
#iterations: 155
currently lose_sum: 87.7153804898262
time_elpased: 1.484
batch start
#iterations: 156
currently lose_sum: 88.13038170337677
time_elpased: 1.595
batch start
#iterations: 157
currently lose_sum: 86.95924699306488
time_elpased: 1.494
batch start
#iterations: 158
currently lose_sum: 88.1573965549469
time_elpased: 1.492
batch start
#iterations: 159
currently lose_sum: 88.00952595472336
time_elpased: 1.529
start validation test
0.609329896907
0.60881514658
0.615519193167
0.612148815311
0.609319030643
64.849
batch start
#iterations: 160
currently lose_sum: 87.820052921772
time_elpased: 1.559
batch start
#iterations: 161
currently lose_sum: 87.47214841842651
time_elpased: 1.506
batch start
#iterations: 162
currently lose_sum: 87.93591529130936
time_elpased: 1.524
batch start
#iterations: 163
currently lose_sum: 87.89759600162506
time_elpased: 1.51
batch start
#iterations: 164
currently lose_sum: 87.03824830055237
time_elpased: 1.495
batch start
#iterations: 165
currently lose_sum: 86.96379500627518
time_elpased: 1.509
batch start
#iterations: 166
currently lose_sum: 87.33137893676758
time_elpased: 1.483
batch start
#iterations: 167
currently lose_sum: 87.28754901885986
time_elpased: 1.501
batch start
#iterations: 168
currently lose_sum: 87.3095161318779
time_elpased: 1.501
batch start
#iterations: 169
currently lose_sum: 87.48999691009521
time_elpased: 1.516
batch start
#iterations: 170
currently lose_sum: 87.24688249826431
time_elpased: 1.5
batch start
#iterations: 171
currently lose_sum: 87.34838652610779
time_elpased: 1.526
batch start
#iterations: 172
currently lose_sum: 87.0836610198021
time_elpased: 1.506
batch start
#iterations: 173
currently lose_sum: 87.67234843969345
time_elpased: 1.488
batch start
#iterations: 174
currently lose_sum: 87.01876336336136
time_elpased: 1.579
batch start
#iterations: 175
currently lose_sum: 86.62538087368011
time_elpased: 1.496
batch start
#iterations: 176
currently lose_sum: 86.88792318105698
time_elpased: 1.491
batch start
#iterations: 177
currently lose_sum: 86.31867545843124
time_elpased: 1.502
batch start
#iterations: 178
currently lose_sum: 86.81419932842255
time_elpased: 1.484
batch start
#iterations: 179
currently lose_sum: 86.37189775705338
time_elpased: 1.528
start validation test
0.591443298969
0.624773582277
0.461459298137
0.530839351249
0.591671505931
67.491
batch start
#iterations: 180
currently lose_sum: 87.28942453861237
time_elpased: 1.501
batch start
#iterations: 181
currently lose_sum: 86.6539397239685
time_elpased: 1.495
batch start
#iterations: 182
currently lose_sum: 86.58066898584366
time_elpased: 1.504
batch start
#iterations: 183
currently lose_sum: 86.01078701019287
time_elpased: 1.508
batch start
#iterations: 184
currently lose_sum: 86.62264376878738
time_elpased: 1.48
batch start
#iterations: 185
currently lose_sum: 86.30264377593994
time_elpased: 1.513
batch start
#iterations: 186
currently lose_sum: 86.73484194278717
time_elpased: 1.508
batch start
#iterations: 187
currently lose_sum: 86.48646885156631
time_elpased: 1.495
batch start
#iterations: 188
currently lose_sum: 86.33796101808548
time_elpased: 1.518
batch start
#iterations: 189
currently lose_sum: 86.88731789588928
time_elpased: 1.516
batch start
#iterations: 190
currently lose_sum: 86.52146047353745
time_elpased: 1.515
batch start
#iterations: 191
currently lose_sum: 86.23290765285492
time_elpased: 1.512
batch start
#iterations: 192
currently lose_sum: 86.93683809041977
time_elpased: 1.478
batch start
#iterations: 193
currently lose_sum: 86.11559098958969
time_elpased: 1.484
batch start
#iterations: 194
currently lose_sum: 86.21669936180115
time_elpased: 1.501
batch start
#iterations: 195
currently lose_sum: 86.48209589719772
time_elpased: 1.487
batch start
#iterations: 196
currently lose_sum: 86.18754231929779
time_elpased: 1.487
batch start
#iterations: 197
currently lose_sum: 86.00551074743271
time_elpased: 1.529
batch start
#iterations: 198
currently lose_sum: 86.4403344988823
time_elpased: 1.483
batch start
#iterations: 199
currently lose_sum: 85.2684086561203
time_elpased: 1.491
start validation test
0.589845360825
0.606822044185
0.514459195225
0.556836535784
0.589977712866
68.087
batch start
#iterations: 200
currently lose_sum: 85.81300395727158
time_elpased: 1.477
batch start
#iterations: 201
currently lose_sum: 86.05649900436401
time_elpased: 1.552
batch start
#iterations: 202
currently lose_sum: 85.29123413562775
time_elpased: 1.526
batch start
#iterations: 203
currently lose_sum: 85.30671566724777
time_elpased: 1.5
batch start
#iterations: 204
currently lose_sum: 85.47240793704987
time_elpased: 1.483
batch start
#iterations: 205
currently lose_sum: 85.30746728181839
time_elpased: 1.527
batch start
#iterations: 206
currently lose_sum: 85.61656934022903
time_elpased: 1.464
batch start
#iterations: 207
currently lose_sum: 85.56721979379654
time_elpased: 1.522
batch start
#iterations: 208
currently lose_sum: 85.83140242099762
time_elpased: 1.497
batch start
#iterations: 209
currently lose_sum: 85.87063789367676
time_elpased: 1.519
batch start
#iterations: 210
currently lose_sum: 85.16915768384933
time_elpased: 1.538
batch start
#iterations: 211
currently lose_sum: 85.78001773357391
time_elpased: 1.484
batch start
#iterations: 212
currently lose_sum: 85.18955636024475
time_elpased: 1.528
batch start
#iterations: 213
currently lose_sum: 85.17020815610886
time_elpased: 1.484
batch start
#iterations: 214
currently lose_sum: 85.40192574262619
time_elpased: 1.538
batch start
#iterations: 215
currently lose_sum: 84.89447885751724
time_elpased: 1.51
batch start
#iterations: 216
currently lose_sum: 84.81716752052307
time_elpased: 1.463
batch start
#iterations: 217
currently lose_sum: 85.54677665233612
time_elpased: 1.479
batch start
#iterations: 218
currently lose_sum: 84.74243474006653
time_elpased: 1.49
batch start
#iterations: 219
currently lose_sum: 84.68244677782059
time_elpased: 1.532
start validation test
0.572216494845
0.617345249917
0.383863332304
0.473380290628
0.572547177874
71.781
batch start
#iterations: 220
currently lose_sum: 84.58575439453125
time_elpased: 1.467
batch start
#iterations: 221
currently lose_sum: 85.52494847774506
time_elpased: 1.485
batch start
#iterations: 222
currently lose_sum: 84.80323630571365
time_elpased: 1.539
batch start
#iterations: 223
currently lose_sum: 84.61869603395462
time_elpased: 1.477
batch start
#iterations: 224
currently lose_sum: 84.36346703767776
time_elpased: 1.511
batch start
#iterations: 225
currently lose_sum: 85.39478898048401
time_elpased: 1.477
batch start
#iterations: 226
currently lose_sum: 84.2854574918747
time_elpased: 1.487
batch start
#iterations: 227
currently lose_sum: 84.55170971155167
time_elpased: 1.5
batch start
#iterations: 228
currently lose_sum: 84.66685968637466
time_elpased: 1.494
batch start
#iterations: 229
currently lose_sum: 84.65981537103653
time_elpased: 1.489
batch start
#iterations: 230
currently lose_sum: 84.58677369356155
time_elpased: 1.483
batch start
#iterations: 231
currently lose_sum: 84.37169361114502
time_elpased: 1.55
batch start
#iterations: 232
currently lose_sum: 85.02874490618706
time_elpased: 1.499
batch start
#iterations: 233
currently lose_sum: 84.38912400603294
time_elpased: 1.497
batch start
#iterations: 234
currently lose_sum: 84.65340888500214
time_elpased: 1.484
batch start
#iterations: 235
currently lose_sum: 83.92994302511215
time_elpased: 1.468
batch start
#iterations: 236
currently lose_sum: 84.09322088956833
time_elpased: 1.507
batch start
#iterations: 237
currently lose_sum: 83.8794516324997
time_elpased: 1.516
batch start
#iterations: 238
currently lose_sum: 84.08881789445877
time_elpased: 1.513
batch start
#iterations: 239
currently lose_sum: 83.71994453668594
time_elpased: 1.5
start validation test
0.593144329897
0.626913442805
0.463620458989
0.533041471928
0.59337172903
68.418
batch start
#iterations: 240
currently lose_sum: 83.59223854541779
time_elpased: 1.495
batch start
#iterations: 241
currently lose_sum: 83.96680629253387
time_elpased: 1.487
batch start
#iterations: 242
currently lose_sum: 84.0870451927185
time_elpased: 1.482
batch start
#iterations: 243
currently lose_sum: 84.18620491027832
time_elpased: 1.521
batch start
#iterations: 244
currently lose_sum: 84.5513316988945
time_elpased: 1.481
batch start
#iterations: 245
currently lose_sum: 83.637291431427
time_elpased: 1.477
batch start
#iterations: 246
currently lose_sum: 83.90618407726288
time_elpased: 1.514
batch start
#iterations: 247
currently lose_sum: 83.5022257566452
time_elpased: 1.494
batch start
#iterations: 248
currently lose_sum: 83.1115043759346
time_elpased: 1.509
batch start
#iterations: 249
currently lose_sum: 83.57023793458939
time_elpased: 1.523
batch start
#iterations: 250
currently lose_sum: 84.0987491607666
time_elpased: 1.48
batch start
#iterations: 251
currently lose_sum: 83.661197245121
time_elpased: 1.509
batch start
#iterations: 252
currently lose_sum: 83.41965973377228
time_elpased: 1.492
batch start
#iterations: 253
currently lose_sum: 83.01097500324249
time_elpased: 1.521
batch start
#iterations: 254
currently lose_sum: 83.88354158401489
time_elpased: 1.507
batch start
#iterations: 255
currently lose_sum: 83.99530780315399
time_elpased: 1.485
batch start
#iterations: 256
currently lose_sum: 83.33624282479286
time_elpased: 1.515
batch start
#iterations: 257
currently lose_sum: 83.50916647911072
time_elpased: 1.504
batch start
#iterations: 258
currently lose_sum: 83.64771172404289
time_elpased: 1.504
batch start
#iterations: 259
currently lose_sum: 83.11602160334587
time_elpased: 1.514
start validation test
0.592113402062
0.622023809524
0.473191314192
0.537494885733
0.59232218813
70.737
batch start
#iterations: 260
currently lose_sum: 83.63037967681885
time_elpased: 1.548
batch start
#iterations: 261
currently lose_sum: 82.77004051208496
time_elpased: 1.519
batch start
#iterations: 262
currently lose_sum: 83.2095975279808
time_elpased: 1.463
batch start
#iterations: 263
currently lose_sum: 83.40678906440735
time_elpased: 1.491
batch start
#iterations: 264
currently lose_sum: 82.53481605648994
time_elpased: 1.501
batch start
#iterations: 265
currently lose_sum: 83.01662492752075
time_elpased: 1.486
batch start
#iterations: 266
currently lose_sum: 82.92193001508713
time_elpased: 1.543
batch start
#iterations: 267
currently lose_sum: 82.97677594423294
time_elpased: 1.507
batch start
#iterations: 268
currently lose_sum: 82.74431210756302
time_elpased: 1.514
batch start
#iterations: 269
currently lose_sum: 82.7696932554245
time_elpased: 1.47
batch start
#iterations: 270
currently lose_sum: 83.57984685897827
time_elpased: 1.488
batch start
#iterations: 271
currently lose_sum: 83.03771603107452
time_elpased: 1.549
batch start
#iterations: 272
currently lose_sum: 82.66410309076309
time_elpased: 1.478
batch start
#iterations: 273
currently lose_sum: 82.37687236070633
time_elpased: 1.492
batch start
#iterations: 274
currently lose_sum: 82.2605453133583
time_elpased: 1.488
batch start
#iterations: 275
currently lose_sum: 82.65387445688248
time_elpased: 1.484
batch start
#iterations: 276
currently lose_sum: 82.4163461625576
time_elpased: 1.536
batch start
#iterations: 277
currently lose_sum: 82.1040745973587
time_elpased: 1.527
batch start
#iterations: 278
currently lose_sum: 82.04328778386116
time_elpased: 1.487
batch start
#iterations: 279
currently lose_sum: 83.09924727678299
time_elpased: 1.509
start validation test
0.584536082474
0.606313358142
0.486261191726
0.539691604797
0.584708619203
70.985
batch start
#iterations: 280
currently lose_sum: 82.69450008869171
time_elpased: 1.51
batch start
#iterations: 281
currently lose_sum: 82.32316675782204
time_elpased: 1.514
batch start
#iterations: 282
currently lose_sum: 81.59288769960403
time_elpased: 1.488
batch start
#iterations: 283
currently lose_sum: 82.24647384881973
time_elpased: 1.529
batch start
#iterations: 284
currently lose_sum: 81.66558676958084
time_elpased: 1.53
batch start
#iterations: 285
currently lose_sum: 81.94940918684006
time_elpased: 1.479
batch start
#iterations: 286
currently lose_sum: 81.53078457713127
time_elpased: 1.523
batch start
#iterations: 287
currently lose_sum: 82.13948825001717
time_elpased: 1.51
batch start
#iterations: 288
currently lose_sum: 82.39580273628235
time_elpased: 1.472
batch start
#iterations: 289
currently lose_sum: 81.45974600315094
time_elpased: 1.492
batch start
#iterations: 290
currently lose_sum: 82.35791033506393
time_elpased: 1.517
batch start
#iterations: 291
currently lose_sum: 82.12121394276619
time_elpased: 1.506
batch start
#iterations: 292
currently lose_sum: 81.11799463629723
time_elpased: 1.483
batch start
#iterations: 293
currently lose_sum: 81.45959889888763
time_elpased: 1.504
batch start
#iterations: 294
currently lose_sum: 82.20481759309769
time_elpased: 1.469
batch start
#iterations: 295
currently lose_sum: 81.66702124476433
time_elpased: 1.492
batch start
#iterations: 296
currently lose_sum: 82.34243673086166
time_elpased: 1.488
batch start
#iterations: 297
currently lose_sum: 81.9212141931057
time_elpased: 1.493
batch start
#iterations: 298
currently lose_sum: 81.92043536901474
time_elpased: 1.49
batch start
#iterations: 299
currently lose_sum: 81.80787664651871
time_elpased: 1.536
start validation test
0.58675257732
0.614216608439
0.47041267881
0.532781630631
0.586956829956
70.683
batch start
#iterations: 300
currently lose_sum: 81.73959025740623
time_elpased: 1.5
batch start
#iterations: 301
currently lose_sum: 81.23765259981155
time_elpased: 1.507
batch start
#iterations: 302
currently lose_sum: 82.0103267133236
time_elpased: 1.516
batch start
#iterations: 303
currently lose_sum: 81.263850659132
time_elpased: 1.472
batch start
#iterations: 304
currently lose_sum: 81.54264488816261
time_elpased: 1.546
batch start
#iterations: 305
currently lose_sum: 81.39009296894073
time_elpased: 1.507
batch start
#iterations: 306
currently lose_sum: 80.92403426766396
time_elpased: 1.488
batch start
#iterations: 307
currently lose_sum: 81.224435120821
time_elpased: 1.489
batch start
#iterations: 308
currently lose_sum: 81.22864681482315
time_elpased: 1.533
batch start
#iterations: 309
currently lose_sum: 81.33026766777039
time_elpased: 1.484
batch start
#iterations: 310
currently lose_sum: 81.31048339605331
time_elpased: 1.484
batch start
#iterations: 311
currently lose_sum: 81.24287378787994
time_elpased: 1.5
batch start
#iterations: 312
currently lose_sum: 80.88110372424126
time_elpased: 1.496
batch start
#iterations: 313
currently lose_sum: 80.11236771941185
time_elpased: 1.563
batch start
#iterations: 314
currently lose_sum: 81.08652055263519
time_elpased: 1.489
batch start
#iterations: 315
currently lose_sum: 80.58453929424286
time_elpased: 1.537
batch start
#iterations: 316
currently lose_sum: 81.25285485386848
time_elpased: 1.51
batch start
#iterations: 317
currently lose_sum: 80.62624868750572
time_elpased: 1.51
batch start
#iterations: 318
currently lose_sum: 80.9572069644928
time_elpased: 1.48
batch start
#iterations: 319
currently lose_sum: 80.38587021827698
time_elpased: 1.528
start validation test
0.578092783505
0.598079385403
0.480703920963
0.533006218976
0.578263764675
71.209
batch start
#iterations: 320
currently lose_sum: 81.08919396996498
time_elpased: 1.522
batch start
#iterations: 321
currently lose_sum: 80.64840239286423
time_elpased: 1.504
batch start
#iterations: 322
currently lose_sum: 81.05825972557068
time_elpased: 1.506
batch start
#iterations: 323
currently lose_sum: 80.76885506510735
time_elpased: 1.52
batch start
#iterations: 324
currently lose_sum: 80.67331656813622
time_elpased: 1.489
batch start
#iterations: 325
currently lose_sum: 80.2679853439331
time_elpased: 1.479
batch start
#iterations: 326
currently lose_sum: 80.52070671319962
time_elpased: 1.496
batch start
#iterations: 327
currently lose_sum: 80.633229970932
time_elpased: 1.515
batch start
#iterations: 328
currently lose_sum: 80.13592857122421
time_elpased: 1.511
batch start
#iterations: 329
currently lose_sum: 80.37470644712448
time_elpased: 1.483
batch start
#iterations: 330
currently lose_sum: 81.00950893759727
time_elpased: 1.511
batch start
#iterations: 331
currently lose_sum: 80.10832008719444
time_elpased: 1.53
batch start
#iterations: 332
currently lose_sum: 80.23711851239204
time_elpased: 1.504
batch start
#iterations: 333
currently lose_sum: 80.50901731848717
time_elpased: 1.55
batch start
#iterations: 334
currently lose_sum: 80.14427372813225
time_elpased: 1.568
batch start
#iterations: 335
currently lose_sum: 80.62279978394508
time_elpased: 1.478
batch start
#iterations: 336
currently lose_sum: 80.1985804438591
time_elpased: 1.556
batch start
#iterations: 337
currently lose_sum: 80.43975350260735
time_elpased: 1.516
batch start
#iterations: 338
currently lose_sum: 80.72440940141678
time_elpased: 1.505
batch start
#iterations: 339
currently lose_sum: 80.11163344979286
time_elpased: 1.505
start validation test
0.573505154639
0.604247941049
0.430379746835
0.502704652001
0.573756433368
71.060
batch start
#iterations: 340
currently lose_sum: 80.4156234562397
time_elpased: 1.493
batch start
#iterations: 341
currently lose_sum: 80.09745278954506
time_elpased: 1.474
batch start
#iterations: 342
currently lose_sum: 80.19155901670456
time_elpased: 1.53
batch start
#iterations: 343
currently lose_sum: 79.47418117523193
time_elpased: 1.508
batch start
#iterations: 344
currently lose_sum: 80.21680873632431
time_elpased: 1.498
batch start
#iterations: 345
currently lose_sum: 80.16771712899208
time_elpased: 1.547
batch start
#iterations: 346
currently lose_sum: 80.03754809498787
time_elpased: 1.518
batch start
#iterations: 347
currently lose_sum: 79.51106449961662
time_elpased: 1.495
batch start
#iterations: 348
currently lose_sum: 79.67725250124931
time_elpased: 1.488
batch start
#iterations: 349
currently lose_sum: 80.3197371661663
time_elpased: 1.52
batch start
#iterations: 350
currently lose_sum: 79.78314507007599
time_elpased: 1.533
batch start
#iterations: 351
currently lose_sum: 80.1115782558918
time_elpased: 1.53
batch start
#iterations: 352
currently lose_sum: 79.80692166090012
time_elpased: 1.559
batch start
#iterations: 353
currently lose_sum: 79.06004056334496
time_elpased: 1.523
batch start
#iterations: 354
currently lose_sum: 79.82663118839264
time_elpased: 1.542
batch start
#iterations: 355
currently lose_sum: 79.07657787203789
time_elpased: 1.476
batch start
#iterations: 356
currently lose_sum: 79.84615445137024
time_elpased: 1.51
batch start
#iterations: 357
currently lose_sum: 78.91471245884895
time_elpased: 1.458
batch start
#iterations: 358
currently lose_sum: 79.40216881036758
time_elpased: 1.488
batch start
#iterations: 359
currently lose_sum: 79.49367991089821
time_elpased: 1.496
start validation test
0.570154639175
0.596960315227
0.436554492127
0.504309576175
0.57038919484
73.201
batch start
#iterations: 360
currently lose_sum: 79.1519322693348
time_elpased: 1.511
batch start
#iterations: 361
currently lose_sum: 79.51365014910698
time_elpased: 1.502
batch start
#iterations: 362
currently lose_sum: 79.29056680202484
time_elpased: 1.473
batch start
#iterations: 363
currently lose_sum: 79.20327869057655
time_elpased: 1.5
batch start
#iterations: 364
currently lose_sum: 79.55079063773155
time_elpased: 1.524
batch start
#iterations: 365
currently lose_sum: 79.6739970445633
time_elpased: 1.504
batch start
#iterations: 366
currently lose_sum: 79.09464266896248
time_elpased: 1.529
batch start
#iterations: 367
currently lose_sum: 79.34959828853607
time_elpased: 1.54
batch start
#iterations: 368
currently lose_sum: 79.14958545565605
time_elpased: 1.506
batch start
#iterations: 369
currently lose_sum: 79.64732465147972
time_elpased: 1.524
batch start
#iterations: 370
currently lose_sum: 79.20926848053932
time_elpased: 1.494
batch start
#iterations: 371
currently lose_sum: 78.53938356041908
time_elpased: 1.54
batch start
#iterations: 372
currently lose_sum: 78.75620117783546
time_elpased: 1.552
batch start
#iterations: 373
currently lose_sum: 79.27921241521835
time_elpased: 1.491
batch start
#iterations: 374
currently lose_sum: 79.00919058918953
time_elpased: 1.498
batch start
#iterations: 375
currently lose_sum: 78.706736266613
time_elpased: 1.486
batch start
#iterations: 376
currently lose_sum: 79.34176051616669
time_elpased: 1.505
batch start
#iterations: 377
currently lose_sum: 78.96887049078941
time_elpased: 1.508
batch start
#iterations: 378
currently lose_sum: 79.10885626077652
time_elpased: 1.521
batch start
#iterations: 379
currently lose_sum: 78.4246512055397
time_elpased: 1.531
start validation test
0.571958762887
0.619522923363
0.37686528764
0.468646019964
0.572301279573
74.434
batch start
#iterations: 380
currently lose_sum: 78.15930604934692
time_elpased: 1.532
batch start
#iterations: 381
currently lose_sum: 78.60678991675377
time_elpased: 1.512
batch start
#iterations: 382
currently lose_sum: 78.84980082511902
time_elpased: 1.511
batch start
#iterations: 383
currently lose_sum: 78.70866149663925
time_elpased: 1.528
batch start
#iterations: 384
currently lose_sum: 78.8431051671505
time_elpased: 1.529
batch start
#iterations: 385
currently lose_sum: 78.60260397195816
time_elpased: 1.5
batch start
#iterations: 386
currently lose_sum: 77.658826649189
time_elpased: 1.544
batch start
#iterations: 387
currently lose_sum: 78.64993759989738
time_elpased: 1.51
batch start
#iterations: 388
currently lose_sum: 78.89700338244438
time_elpased: 1.484
batch start
#iterations: 389
currently lose_sum: 78.21697613596916
time_elpased: 1.483
batch start
#iterations: 390
currently lose_sum: 78.70312914252281
time_elpased: 1.533
batch start
#iterations: 391
currently lose_sum: 78.15769731998444
time_elpased: 1.504
batch start
#iterations: 392
currently lose_sum: 77.68963065743446
time_elpased: 1.492
batch start
#iterations: 393
currently lose_sum: 77.93151557445526
time_elpased: 1.499
batch start
#iterations: 394
currently lose_sum: 78.54089832305908
time_elpased: 1.506
batch start
#iterations: 395
currently lose_sum: 77.88786041736603
time_elpased: 1.506
batch start
#iterations: 396
currently lose_sum: 78.12777581810951
time_elpased: 1.474
batch start
#iterations: 397
currently lose_sum: 78.24043828248978
time_elpased: 1.505
batch start
#iterations: 398
currently lose_sum: 77.69871985912323
time_elpased: 1.536
batch start
#iterations: 399
currently lose_sum: 77.6284456551075
time_elpased: 1.489
start validation test
0.563917525773
0.615130976369
0.345579911495
0.44254085398
0.564300851131
79.794
acc: 0.620
pre: 0.642
rec: 0.545
F1: 0.589
auc: 0.675
