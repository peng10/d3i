start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.43536680936813
time_elpased: 1.741
batch start
#iterations: 1
currently lose_sum: 99.77897077798843
time_elpased: 1.73
batch start
#iterations: 2
currently lose_sum: 99.40283763408661
time_elpased: 1.748
batch start
#iterations: 3
currently lose_sum: 99.05159568786621
time_elpased: 1.737
batch start
#iterations: 4
currently lose_sum: 98.51750230789185
time_elpased: 1.726
batch start
#iterations: 5
currently lose_sum: 98.15820145606995
time_elpased: 1.773
batch start
#iterations: 6
currently lose_sum: 97.93411493301392
time_elpased: 1.721
batch start
#iterations: 7
currently lose_sum: 97.68883752822876
time_elpased: 1.741
batch start
#iterations: 8
currently lose_sum: 97.53589820861816
time_elpased: 1.745
batch start
#iterations: 9
currently lose_sum: 96.88470363616943
time_elpased: 1.741
batch start
#iterations: 10
currently lose_sum: 96.99576371908188
time_elpased: 1.739
batch start
#iterations: 11
currently lose_sum: 96.45875811576843
time_elpased: 1.763
batch start
#iterations: 12
currently lose_sum: 96.23523557186127
time_elpased: 1.719
batch start
#iterations: 13
currently lose_sum: 96.06059449911118
time_elpased: 1.761
batch start
#iterations: 14
currently lose_sum: 95.86051708459854
time_elpased: 1.712
batch start
#iterations: 15
currently lose_sum: 95.72716695070267
time_elpased: 1.725
batch start
#iterations: 16
currently lose_sum: 95.45779299736023
time_elpased: 1.753
batch start
#iterations: 17
currently lose_sum: 95.17336297035217
time_elpased: 1.742
batch start
#iterations: 18
currently lose_sum: 94.85352677106857
time_elpased: 1.732
batch start
#iterations: 19
currently lose_sum: 94.66337978839874
time_elpased: 1.762
start validation test
0.626288659794
0.62132389102
0.6500977668
0.635385234359
0.626246859234
62.675
batch start
#iterations: 20
currently lose_sum: 94.1260941028595
time_elpased: 1.727
batch start
#iterations: 21
currently lose_sum: 93.93711477518082
time_elpased: 1.748
batch start
#iterations: 22
currently lose_sum: 93.78993278741837
time_elpased: 1.736
batch start
#iterations: 23
currently lose_sum: 93.63595193624496
time_elpased: 1.768
batch start
#iterations: 24
currently lose_sum: 93.06664007902145
time_elpased: 1.743
batch start
#iterations: 25
currently lose_sum: 93.37104946374893
time_elpased: 1.763
batch start
#iterations: 26
currently lose_sum: 92.53762477636337
time_elpased: 1.734
batch start
#iterations: 27
currently lose_sum: 92.14883428812027
time_elpased: 1.732
batch start
#iterations: 28
currently lose_sum: 91.87817454338074
time_elpased: 1.745
batch start
#iterations: 29
currently lose_sum: 91.61558425426483
time_elpased: 1.733
batch start
#iterations: 30
currently lose_sum: 91.37878453731537
time_elpased: 1.732
batch start
#iterations: 31
currently lose_sum: 90.94141393899918
time_elpased: 1.734
batch start
#iterations: 32
currently lose_sum: 90.75029242038727
time_elpased: 1.718
batch start
#iterations: 33
currently lose_sum: 90.93520885705948
time_elpased: 1.73
batch start
#iterations: 34
currently lose_sum: 90.16430276632309
time_elpased: 1.714
batch start
#iterations: 35
currently lose_sum: 89.61376029253006
time_elpased: 1.727
batch start
#iterations: 36
currently lose_sum: 88.74272209405899
time_elpased: 1.739
batch start
#iterations: 37
currently lose_sum: 89.18090468645096
time_elpased: 1.748
batch start
#iterations: 38
currently lose_sum: 88.63762217760086
time_elpased: 1.71
batch start
#iterations: 39
currently lose_sum: 88.03972399234772
time_elpased: 1.73
start validation test
0.606494845361
0.625225441866
0.535144591952
0.576688477321
0.606620111736
64.464
batch start
#iterations: 40
currently lose_sum: 87.82112830877304
time_elpased: 1.698
batch start
#iterations: 41
currently lose_sum: 87.27580833435059
time_elpased: 1.718
batch start
#iterations: 42
currently lose_sum: 86.69765347242355
time_elpased: 1.708
batch start
#iterations: 43
currently lose_sum: 86.25724846124649
time_elpased: 1.769
batch start
#iterations: 44
currently lose_sum: 86.98550069332123
time_elpased: 1.741
batch start
#iterations: 45
currently lose_sum: 86.15686786174774
time_elpased: 1.755
batch start
#iterations: 46
currently lose_sum: 85.99113649129868
time_elpased: 1.736
batch start
#iterations: 47
currently lose_sum: 85.83748412132263
time_elpased: 1.738
batch start
#iterations: 48
currently lose_sum: 86.5397435426712
time_elpased: 1.713
batch start
#iterations: 49
currently lose_sum: 85.31526666879654
time_elpased: 1.729
batch start
#iterations: 50
currently lose_sum: 84.68778097629547
time_elpased: 1.726
batch start
#iterations: 51
currently lose_sum: 84.40325576066971
time_elpased: 1.75
batch start
#iterations: 52
currently lose_sum: 83.76500898599625
time_elpased: 1.721
batch start
#iterations: 53
currently lose_sum: 83.55038404464722
time_elpased: 1.706
batch start
#iterations: 54
currently lose_sum: 83.253502368927
time_elpased: 1.743
batch start
#iterations: 55
currently lose_sum: 83.02693951129913
time_elpased: 1.721
batch start
#iterations: 56
currently lose_sum: 83.70783132314682
time_elpased: 1.716
batch start
#iterations: 57
currently lose_sum: 82.42206585407257
time_elpased: 1.754
batch start
#iterations: 58
currently lose_sum: 82.06773075461388
time_elpased: 1.721
batch start
#iterations: 59
currently lose_sum: 81.96440917253494
time_elpased: 1.759
start validation test
0.579742268041
0.631340275445
0.386847792529
0.479739646481
0.580080924045
70.864
batch start
#iterations: 60
currently lose_sum: 81.38451659679413
time_elpased: 1.757
batch start
#iterations: 61
currently lose_sum: 81.70686972141266
time_elpased: 1.759
batch start
#iterations: 62
currently lose_sum: 81.26879873871803
time_elpased: 1.729
batch start
#iterations: 63
currently lose_sum: 81.01136791706085
time_elpased: 1.783
batch start
#iterations: 64
currently lose_sum: 80.21265050768852
time_elpased: 1.73
batch start
#iterations: 65
currently lose_sum: 79.78893083333969
time_elpased: 1.75
batch start
#iterations: 66
currently lose_sum: 80.67261609435081
time_elpased: 1.777
batch start
#iterations: 67
currently lose_sum: 79.64574217796326
time_elpased: 1.729
batch start
#iterations: 68
currently lose_sum: 79.51378145813942
time_elpased: 1.768
batch start
#iterations: 69
currently lose_sum: 79.73438560962677
time_elpased: 1.803
batch start
#iterations: 70
currently lose_sum: 79.4775305390358
time_elpased: 1.702
batch start
#iterations: 71
currently lose_sum: 78.86931958794594
time_elpased: 1.739
batch start
#iterations: 72
currently lose_sum: 78.25204458832741
time_elpased: 1.747
batch start
#iterations: 73
currently lose_sum: 77.74651747941971
time_elpased: 1.741
batch start
#iterations: 74
currently lose_sum: 78.87204679846764
time_elpased: 1.72
batch start
#iterations: 75
currently lose_sum: 78.09995353221893
time_elpased: 1.775
batch start
#iterations: 76
currently lose_sum: 77.68383541703224
time_elpased: 1.755
batch start
#iterations: 77
currently lose_sum: 77.90351876616478
time_elpased: 1.732
batch start
#iterations: 78
currently lose_sum: 77.40156304836273
time_elpased: 1.793
batch start
#iterations: 79
currently lose_sum: 76.85559642314911
time_elpased: 1.752
start validation test
0.577577319588
0.60828116107
0.439950602038
0.510600179158
0.577818944518
74.859
batch start
#iterations: 80
currently lose_sum: 76.61743128299713
time_elpased: 1.714
batch start
#iterations: 81
currently lose_sum: 76.08008235692978
time_elpased: 1.735
batch start
#iterations: 82
currently lose_sum: 76.28890433907509
time_elpased: 1.748
batch start
#iterations: 83
currently lose_sum: 76.17380201816559
time_elpased: 1.742
batch start
#iterations: 84
currently lose_sum: 75.66601857542992
time_elpased: 1.768
batch start
#iterations: 85
currently lose_sum: 75.199942111969
time_elpased: 1.746
batch start
#iterations: 86
currently lose_sum: 75.47071620821953
time_elpased: 1.784
batch start
#iterations: 87
currently lose_sum: 74.59423220157623
time_elpased: 1.725
batch start
#iterations: 88
currently lose_sum: 74.6708254814148
time_elpased: 1.699
batch start
#iterations: 89
currently lose_sum: 74.2795794904232
time_elpased: 1.752
batch start
#iterations: 90
currently lose_sum: 73.88861536979675
time_elpased: 1.737
batch start
#iterations: 91
currently lose_sum: 74.12726154923439
time_elpased: 1.74
batch start
#iterations: 92
currently lose_sum: 73.86780765652657
time_elpased: 1.746
batch start
#iterations: 93
currently lose_sum: 73.74312388896942
time_elpased: 1.74
batch start
#iterations: 94
currently lose_sum: 73.28639030456543
time_elpased: 1.729
batch start
#iterations: 95
currently lose_sum: 73.4179622232914
time_elpased: 1.762
batch start
#iterations: 96
currently lose_sum: 73.1276436150074
time_elpased: 1.723
batch start
#iterations: 97
currently lose_sum: 72.79717889428139
time_elpased: 1.769
batch start
#iterations: 98
currently lose_sum: 71.82233849167824
time_elpased: 1.707
batch start
#iterations: 99
currently lose_sum: 71.64068454504013
time_elpased: 1.756
start validation test
0.558298969072
0.607894736842
0.332818771226
0.43013899049
0.558694834337
83.612
batch start
#iterations: 100
currently lose_sum: 72.84158310294151
time_elpased: 1.737
batch start
#iterations: 101
currently lose_sum: 72.01103311777115
time_elpased: 1.708
batch start
#iterations: 102
currently lose_sum: 71.73669829964638
time_elpased: 1.729
batch start
#iterations: 103
currently lose_sum: 71.40622451901436
time_elpased: 1.734
batch start
#iterations: 104
currently lose_sum: 71.03864106535912
time_elpased: 1.722
batch start
#iterations: 105
currently lose_sum: 70.77737414836884
time_elpased: 1.745
batch start
#iterations: 106
currently lose_sum: 71.5405498445034
time_elpased: 1.75
batch start
#iterations: 107
currently lose_sum: 70.91732600331306
time_elpased: 1.726
batch start
#iterations: 108
currently lose_sum: 70.48577672243118
time_elpased: 1.741
batch start
#iterations: 109
currently lose_sum: 70.05474814772606
time_elpased: 1.741
batch start
#iterations: 110
currently lose_sum: 69.94934287667274
time_elpased: 1.736
batch start
#iterations: 111
currently lose_sum: 70.6416462957859
time_elpased: 1.744
batch start
#iterations: 112
currently lose_sum: 70.15384563803673
time_elpased: 1.723
batch start
#iterations: 113
currently lose_sum: 69.33741429448128
time_elpased: 1.753
batch start
#iterations: 114
currently lose_sum: 68.96014153957367
time_elpased: 1.724
batch start
#iterations: 115
currently lose_sum: 68.23800021409988
time_elpased: 1.73
batch start
#iterations: 116
currently lose_sum: 69.2328716814518
time_elpased: 1.729
batch start
#iterations: 117
currently lose_sum: 68.88887962698936
time_elpased: 1.737
batch start
#iterations: 118
currently lose_sum: 68.91010171175003
time_elpased: 1.76
batch start
#iterations: 119
currently lose_sum: 67.93020156025887
time_elpased: 1.733
start validation test
0.562268041237
0.617765814266
0.330657610374
0.430754792868
0.562674669072
88.713
batch start
#iterations: 120
currently lose_sum: 67.93576616048813
time_elpased: 1.768
batch start
#iterations: 121
currently lose_sum: 68.55035284161568
time_elpased: 1.751
batch start
#iterations: 122
currently lose_sum: 68.51908349990845
time_elpased: 1.752
batch start
#iterations: 123
currently lose_sum: 67.64136561751366
time_elpased: 1.777
batch start
#iterations: 124
currently lose_sum: 68.17798665165901
time_elpased: 1.757
batch start
#iterations: 125
currently lose_sum: 67.53362601995468
time_elpased: 1.751
batch start
#iterations: 126
currently lose_sum: 67.17906188964844
time_elpased: 1.747
batch start
#iterations: 127
currently lose_sum: 66.41067370772362
time_elpased: 1.701
batch start
#iterations: 128
currently lose_sum: 66.85740366578102
time_elpased: 1.777
batch start
#iterations: 129
currently lose_sum: 66.2251381278038
time_elpased: 1.728
batch start
#iterations: 130
currently lose_sum: 66.50360703468323
time_elpased: 1.744
batch start
#iterations: 131
currently lose_sum: 65.73431885242462
time_elpased: 1.738
batch start
#iterations: 132
currently lose_sum: 66.8777405321598
time_elpased: 1.766
batch start
#iterations: 133
currently lose_sum: 65.78658989071846
time_elpased: 1.712
batch start
#iterations: 134
currently lose_sum: 65.2856881916523
time_elpased: 1.742
batch start
#iterations: 135
currently lose_sum: 65.43572011590004
time_elpased: 1.713
batch start
#iterations: 136
currently lose_sum: 65.77907931804657
time_elpased: 1.76
batch start
#iterations: 137
currently lose_sum: 64.47981289029121
time_elpased: 1.724
batch start
#iterations: 138
currently lose_sum: 65.03955519199371
time_elpased: 1.735
batch start
#iterations: 139
currently lose_sum: 65.37593793869019
time_elpased: 1.706
start validation test
0.555154639175
0.606589527358
0.31830811979
0.417521598272
0.555570459771
94.123
batch start
#iterations: 140
currently lose_sum: 64.64058247208595
time_elpased: 1.731
batch start
#iterations: 141
currently lose_sum: 64.9202344417572
time_elpased: 1.743
batch start
#iterations: 142
currently lose_sum: 64.60612285137177
time_elpased: 1.782
batch start
#iterations: 143
currently lose_sum: 64.32740885019302
time_elpased: 1.741
batch start
#iterations: 144
currently lose_sum: 64.44361793994904
time_elpased: 1.753
batch start
#iterations: 145
currently lose_sum: 64.43677538633347
time_elpased: 1.692
batch start
#iterations: 146
currently lose_sum: 64.23830330371857
time_elpased: 1.716
batch start
#iterations: 147
currently lose_sum: 63.486075431108475
time_elpased: 1.721
batch start
#iterations: 148
currently lose_sum: 64.3077103793621
time_elpased: 1.755
batch start
#iterations: 149
currently lose_sum: 63.572590589523315
time_elpased: 1.698
batch start
#iterations: 150
currently lose_sum: 63.1228945851326
time_elpased: 1.8
batch start
#iterations: 151
currently lose_sum: 63.48643234372139
time_elpased: 1.719
batch start
#iterations: 152
currently lose_sum: 63.368737787008286
time_elpased: 1.709
batch start
#iterations: 153
currently lose_sum: 62.49072661995888
time_elpased: 1.698
batch start
#iterations: 154
currently lose_sum: 63.38061019778252
time_elpased: 1.695
batch start
#iterations: 155
currently lose_sum: 63.347406566143036
time_elpased: 1.716
batch start
#iterations: 156
currently lose_sum: 62.78874921798706
time_elpased: 1.702
batch start
#iterations: 157
currently lose_sum: 62.59153673052788
time_elpased: 1.721
batch start
#iterations: 158
currently lose_sum: 63.0099101960659
time_elpased: 1.731
batch start
#iterations: 159
currently lose_sum: 61.901663184165955
time_elpased: 1.7
start validation test
0.558402061856
0.605272793848
0.340228465576
0.435601818302
0.558785099255
100.169
batch start
#iterations: 160
currently lose_sum: 62.61956363916397
time_elpased: 1.799
batch start
#iterations: 161
currently lose_sum: 62.45080304145813
time_elpased: 1.731
batch start
#iterations: 162
currently lose_sum: 61.99211296439171
time_elpased: 1.776
batch start
#iterations: 163
currently lose_sum: 61.59793186187744
time_elpased: 1.757
batch start
#iterations: 164
currently lose_sum: 62.041756361722946
time_elpased: 1.759
batch start
#iterations: 165
currently lose_sum: 60.64011824131012
time_elpased: 1.746
batch start
#iterations: 166
currently lose_sum: 61.74667891860008
time_elpased: 1.732
batch start
#iterations: 167
currently lose_sum: 60.87479057908058
time_elpased: 1.723
batch start
#iterations: 168
currently lose_sum: 61.649099826812744
time_elpased: 1.772
batch start
#iterations: 169
currently lose_sum: 60.19917702674866
time_elpased: 1.762
batch start
#iterations: 170
currently lose_sum: 59.72032564878464
time_elpased: 1.722
batch start
#iterations: 171
currently lose_sum: 60.130945563316345
time_elpased: 1.795
batch start
#iterations: 172
currently lose_sum: 60.01777055859566
time_elpased: 1.728
batch start
#iterations: 173
currently lose_sum: 59.84237599372864
time_elpased: 1.771
batch start
#iterations: 174
currently lose_sum: 59.82330933213234
time_elpased: 1.745
batch start
#iterations: 175
currently lose_sum: 59.6160734295845
time_elpased: 1.746
batch start
#iterations: 176
currently lose_sum: 59.8719447851181
time_elpased: 1.732
batch start
#iterations: 177
currently lose_sum: 59.32433179020882
time_elpased: 1.7
batch start
#iterations: 178
currently lose_sum: 58.91683426499367
time_elpased: 1.733
batch start
#iterations: 179
currently lose_sum: 59.267455011606216
time_elpased: 1.715
start validation test
0.552525773196
0.600699844479
0.317999382525
0.415853576475
0.552937520448
106.625
batch start
#iterations: 180
currently lose_sum: 59.81895241141319
time_elpased: 1.734
batch start
#iterations: 181
currently lose_sum: 59.485556960105896
time_elpased: 1.733
batch start
#iterations: 182
currently lose_sum: 59.30528596043587
time_elpased: 1.713
batch start
#iterations: 183
currently lose_sum: 58.63998666405678
time_elpased: 1.722
batch start
#iterations: 184
currently lose_sum: 59.08216243982315
time_elpased: 1.695
batch start
#iterations: 185
currently lose_sum: 58.23731940984726
time_elpased: 1.71
batch start
#iterations: 186
currently lose_sum: 58.34996458888054
time_elpased: 1.756
batch start
#iterations: 187
currently lose_sum: 58.28894290328026
time_elpased: 1.74
batch start
#iterations: 188
currently lose_sum: 58.866339325904846
time_elpased: 1.724
batch start
#iterations: 189
currently lose_sum: 58.26939496397972
time_elpased: 1.694
batch start
#iterations: 190
currently lose_sum: 58.323511987924576
time_elpased: 1.766
batch start
#iterations: 191
currently lose_sum: 58.14994102716446
time_elpased: 1.772
batch start
#iterations: 192
currently lose_sum: 57.210603922605515
time_elpased: 1.709
batch start
#iterations: 193
currently lose_sum: 58.41649878025055
time_elpased: 1.706
batch start
#iterations: 194
currently lose_sum: 57.558581441640854
time_elpased: 1.741
batch start
#iterations: 195
currently lose_sum: 58.72834640741348
time_elpased: 1.7
batch start
#iterations: 196
currently lose_sum: 57.41251274943352
time_elpased: 1.757
batch start
#iterations: 197
currently lose_sum: 57.52763703465462
time_elpased: 1.749
batch start
#iterations: 198
currently lose_sum: 58.01044908165932
time_elpased: 1.699
batch start
#iterations: 199
currently lose_sum: 56.69369187951088
time_elpased: 1.744
start validation test
0.545103092784
0.594331641286
0.289183904497
0.389061959155
0.545552398391
114.801
batch start
#iterations: 200
currently lose_sum: 57.33697611093521
time_elpased: 1.733
batch start
#iterations: 201
currently lose_sum: 56.76598444581032
time_elpased: 1.707
batch start
#iterations: 202
currently lose_sum: 55.881782948970795
time_elpased: 1.74
batch start
#iterations: 203
currently lose_sum: 56.97249564528465
time_elpased: 1.703
batch start
#iterations: 204
currently lose_sum: 57.08112135529518
time_elpased: 1.706
batch start
#iterations: 205
currently lose_sum: 56.3632253408432
time_elpased: 1.737
batch start
#iterations: 206
currently lose_sum: 56.306374073028564
time_elpased: 1.713
batch start
#iterations: 207
currently lose_sum: 56.59133693575859
time_elpased: 1.709
batch start
#iterations: 208
currently lose_sum: nan
time_elpased: 1.736
train finish final lose is: nan
acc: 0.628
pre: 0.623
rec: 0.650
F1: 0.636
auc: 0.628
