start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.4954092502594
time_elpased: 1.364
batch start
#iterations: 1
currently lose_sum: 99.70131874084473
time_elpased: 1.35
batch start
#iterations: 2
currently lose_sum: 99.43032658100128
time_elpased: 1.346
batch start
#iterations: 3
currently lose_sum: 99.18604117631912
time_elpased: 1.348
batch start
#iterations: 4
currently lose_sum: 99.03369742631912
time_elpased: 1.344
batch start
#iterations: 5
currently lose_sum: 98.38445490598679
time_elpased: 1.322
batch start
#iterations: 6
currently lose_sum: 98.34451299905777
time_elpased: 1.349
batch start
#iterations: 7
currently lose_sum: 97.85302823781967
time_elpased: 1.351
batch start
#iterations: 8
currently lose_sum: 98.06486290693283
time_elpased: 1.354
batch start
#iterations: 9
currently lose_sum: 97.62922430038452
time_elpased: 1.372
batch start
#iterations: 10
currently lose_sum: 97.4546325802803
time_elpased: 1.315
batch start
#iterations: 11
currently lose_sum: 97.23952674865723
time_elpased: 1.332
batch start
#iterations: 12
currently lose_sum: 96.94060963392258
time_elpased: 1.331
batch start
#iterations: 13
currently lose_sum: 97.08676147460938
time_elpased: 1.356
batch start
#iterations: 14
currently lose_sum: 96.76836901903152
time_elpased: 1.341
batch start
#iterations: 15
currently lose_sum: 96.46747690439224
time_elpased: 1.332
batch start
#iterations: 16
currently lose_sum: 96.45596861839294
time_elpased: 1.323
batch start
#iterations: 17
currently lose_sum: 96.73056828975677
time_elpased: 1.366
batch start
#iterations: 18
currently lose_sum: 96.0520675778389
time_elpased: 1.333
batch start
#iterations: 19
currently lose_sum: 96.06055384874344
time_elpased: 1.347
start validation test
0.62618556701
0.646638905413
0.559329011012
0.599823419049
0.626302944006
62.982
batch start
#iterations: 20
currently lose_sum: 95.95294433832169
time_elpased: 1.346
batch start
#iterations: 21
currently lose_sum: 95.62957698106766
time_elpased: 1.336
batch start
#iterations: 22
currently lose_sum: 95.6717199087143
time_elpased: 1.351
batch start
#iterations: 23
currently lose_sum: 95.53965973854065
time_elpased: 1.332
batch start
#iterations: 24
currently lose_sum: 95.82819956541061
time_elpased: 1.324
batch start
#iterations: 25
currently lose_sum: 95.47486060857773
time_elpased: 1.335
batch start
#iterations: 26
currently lose_sum: 95.17014193534851
time_elpased: 1.322
batch start
#iterations: 27
currently lose_sum: 95.08583503961563
time_elpased: 1.331
batch start
#iterations: 28
currently lose_sum: 94.61430048942566
time_elpased: 1.337
batch start
#iterations: 29
currently lose_sum: 94.8425961136818
time_elpased: 1.347
batch start
#iterations: 30
currently lose_sum: 94.94157183170319
time_elpased: 1.351
batch start
#iterations: 31
currently lose_sum: 94.46083617210388
time_elpased: 1.344
batch start
#iterations: 32
currently lose_sum: 94.35492014884949
time_elpased: 1.314
batch start
#iterations: 33
currently lose_sum: 94.14900034666061
time_elpased: 1.341
batch start
#iterations: 34
currently lose_sum: 94.43651789426804
time_elpased: 1.33
batch start
#iterations: 35
currently lose_sum: 94.20494192838669
time_elpased: 1.328
batch start
#iterations: 36
currently lose_sum: 94.13338327407837
time_elpased: 1.352
batch start
#iterations: 37
currently lose_sum: 94.06684297323227
time_elpased: 1.322
batch start
#iterations: 38
currently lose_sum: 93.56891638040543
time_elpased: 1.333
batch start
#iterations: 39
currently lose_sum: 92.98535591363907
time_elpased: 1.34
start validation test
0.627422680412
0.645097353387
0.569414428321
0.604897780693
0.627524522846
62.242
batch start
#iterations: 40
currently lose_sum: 93.65399020910263
time_elpased: 1.351
batch start
#iterations: 41
currently lose_sum: 93.46876460313797
time_elpased: 1.349
batch start
#iterations: 42
currently lose_sum: 93.02358049154282
time_elpased: 1.335
batch start
#iterations: 43
currently lose_sum: 92.83379542827606
time_elpased: 1.34
batch start
#iterations: 44
currently lose_sum: 92.71081018447876
time_elpased: 1.338
batch start
#iterations: 45
currently lose_sum: 93.1341667175293
time_elpased: 1.342
batch start
#iterations: 46
currently lose_sum: 92.49024474620819
time_elpased: 1.338
batch start
#iterations: 47
currently lose_sum: 92.97457814216614
time_elpased: 1.329
batch start
#iterations: 48
currently lose_sum: 93.04524248838425
time_elpased: 1.394
batch start
#iterations: 49
currently lose_sum: 92.76371455192566
time_elpased: 1.329
batch start
#iterations: 50
currently lose_sum: 93.01978462934494
time_elpased: 1.33
batch start
#iterations: 51
currently lose_sum: 92.27082550525665
time_elpased: 1.331
batch start
#iterations: 52
currently lose_sum: 92.34176933765411
time_elpased: 1.344
batch start
#iterations: 53
currently lose_sum: 92.2505698800087
time_elpased: 1.369
batch start
#iterations: 54
currently lose_sum: 92.69550967216492
time_elpased: 1.362
batch start
#iterations: 55
currently lose_sum: 91.99004811048508
time_elpased: 1.353
batch start
#iterations: 56
currently lose_sum: 91.56392413377762
time_elpased: 1.337
batch start
#iterations: 57
currently lose_sum: 91.66278731822968
time_elpased: 1.315
batch start
#iterations: 58
currently lose_sum: 92.07113528251648
time_elpased: 1.331
batch start
#iterations: 59
currently lose_sum: 91.54832077026367
time_elpased: 1.403
start validation test
0.623556701031
0.641169590643
0.564165894824
0.600208025401
0.623660970752
62.323
batch start
#iterations: 60
currently lose_sum: 91.3131040930748
time_elpased: 1.375
batch start
#iterations: 61
currently lose_sum: 91.58264285326004
time_elpased: 1.356
batch start
#iterations: 62
currently lose_sum: 91.37183374166489
time_elpased: 1.34
batch start
#iterations: 63
currently lose_sum: 91.7161779999733
time_elpased: 1.345
batch start
#iterations: 64
currently lose_sum: 91.51510894298553
time_elpased: 1.367
batch start
#iterations: 65
currently lose_sum: 90.67896115779877
time_elpased: 1.333
batch start
#iterations: 66
currently lose_sum: 91.08483254909515
time_elpased: 1.337
batch start
#iterations: 67
currently lose_sum: 91.09666401147842
time_elpased: 1.332
batch start
#iterations: 68
currently lose_sum: 90.93280565738678
time_elpased: 1.354
batch start
#iterations: 69
currently lose_sum: 91.1152538061142
time_elpased: 1.349
batch start
#iterations: 70
currently lose_sum: 91.00952112674713
time_elpased: 1.335
batch start
#iterations: 71
currently lose_sum: 90.62177526950836
time_elpased: 1.329
batch start
#iterations: 72
currently lose_sum: 90.64679563045502
time_elpased: 1.327
batch start
#iterations: 73
currently lose_sum: 90.46077942848206
time_elpased: 1.321
batch start
#iterations: 74
currently lose_sum: 90.34448826313019
time_elpased: 1.343
batch start
#iterations: 75
currently lose_sum: 90.471470952034
time_elpased: 1.346
batch start
#iterations: 76
currently lose_sum: 89.86138945817947
time_elpased: 1.32
batch start
#iterations: 77
currently lose_sum: 90.58254039287567
time_elpased: 1.325
batch start
#iterations: 78
currently lose_sum: 90.58942288160324
time_elpased: 1.34
batch start
#iterations: 79
currently lose_sum: 90.43151187896729
time_elpased: 1.334
start validation test
0.602731958763
0.643080865604
0.464855408048
0.539633235768
0.602974022314
63.849
batch start
#iterations: 80
currently lose_sum: 89.85927098989487
time_elpased: 1.342
batch start
#iterations: 81
currently lose_sum: 89.80795586109161
time_elpased: 1.323
batch start
#iterations: 82
currently lose_sum: 90.12789195775986
time_elpased: 1.333
batch start
#iterations: 83
currently lose_sum: 89.76776814460754
time_elpased: 1.34
batch start
#iterations: 84
currently lose_sum: 89.75322914123535
time_elpased: 1.393
batch start
#iterations: 85
currently lose_sum: 89.64204156398773
time_elpased: 1.336
batch start
#iterations: 86
currently lose_sum: 89.77775675058365
time_elpased: 1.334
batch start
#iterations: 87
currently lose_sum: 89.55637508630753
time_elpased: 1.328
batch start
#iterations: 88
currently lose_sum: 89.69525146484375
time_elpased: 1.333
batch start
#iterations: 89
currently lose_sum: 88.94680118560791
time_elpased: 1.368
batch start
#iterations: 90
currently lose_sum: 89.1419432759285
time_elpased: 1.338
batch start
#iterations: 91
currently lose_sum: 89.77513206005096
time_elpased: 1.356
batch start
#iterations: 92
currently lose_sum: 89.06607341766357
time_elpased: 1.381
batch start
#iterations: 93
currently lose_sum: 89.20268023014069
time_elpased: 1.338
batch start
#iterations: 94
currently lose_sum: 88.92704701423645
time_elpased: 1.345
batch start
#iterations: 95
currently lose_sum: 88.90936076641083
time_elpased: 1.392
batch start
#iterations: 96
currently lose_sum: 89.2423512339592
time_elpased: 1.33
batch start
#iterations: 97
currently lose_sum: 88.637590944767
time_elpased: 1.352
batch start
#iterations: 98
currently lose_sum: 88.82213854789734
time_elpased: 1.366
batch start
#iterations: 99
currently lose_sum: 88.24735862016678
time_elpased: 1.341
start validation test
0.617835051546
0.643061249845
0.532674693836
0.582686029495
0.617984563689
63.089
batch start
#iterations: 100
currently lose_sum: 88.52881544828415
time_elpased: 1.359
batch start
#iterations: 101
currently lose_sum: 88.51443821191788
time_elpased: 1.363
batch start
#iterations: 102
currently lose_sum: 88.59498244524002
time_elpased: 1.37
batch start
#iterations: 103
currently lose_sum: 88.11853551864624
time_elpased: 1.331
batch start
#iterations: 104
currently lose_sum: 88.35208666324615
time_elpased: 1.351
batch start
#iterations: 105
currently lose_sum: 88.50298810005188
time_elpased: 1.345
batch start
#iterations: 106
currently lose_sum: 88.20652484893799
time_elpased: 1.329
batch start
#iterations: 107
currently lose_sum: 88.62420564889908
time_elpased: 1.349
batch start
#iterations: 108
currently lose_sum: 88.11594676971436
time_elpased: 1.321
batch start
#iterations: 109
currently lose_sum: 88.60046637058258
time_elpased: 1.369
batch start
#iterations: 110
currently lose_sum: 88.22660422325134
time_elpased: 1.328
batch start
#iterations: 111
currently lose_sum: 88.1262755393982
time_elpased: 1.343
batch start
#iterations: 112
currently lose_sum: 87.89799308776855
time_elpased: 1.364
batch start
#iterations: 113
currently lose_sum: 87.5235385298729
time_elpased: 1.341
batch start
#iterations: 114
currently lose_sum: 87.91317754983902
time_elpased: 1.337
batch start
#iterations: 115
currently lose_sum: 87.87671899795532
time_elpased: 1.337
batch start
#iterations: 116
currently lose_sum: 87.75082439184189
time_elpased: 1.354
batch start
#iterations: 117
currently lose_sum: 87.59805542230606
time_elpased: 1.344
batch start
#iterations: 118
currently lose_sum: 88.1303573846817
time_elpased: 1.344
batch start
#iterations: 119
currently lose_sum: 87.34672594070435
time_elpased: 1.32
start validation test
0.612268041237
0.648571815351
0.493053411547
0.560219831618
0.612477340907
63.791
batch start
#iterations: 120
currently lose_sum: 87.72823691368103
time_elpased: 1.348
batch start
#iterations: 121
currently lose_sum: 87.60685133934021
time_elpased: 1.326
batch start
#iterations: 122
currently lose_sum: 87.3853816986084
time_elpased: 1.342
batch start
#iterations: 123
currently lose_sum: 87.11704379320145
time_elpased: 1.333
batch start
#iterations: 124
currently lose_sum: 87.47518038749695
time_elpased: 1.352
batch start
#iterations: 125
currently lose_sum: 87.35070192813873
time_elpased: 1.326
batch start
#iterations: 126
currently lose_sum: 87.3712871670723
time_elpased: 1.334
batch start
#iterations: 127
currently lose_sum: 87.55101317167282
time_elpased: 1.349
batch start
#iterations: 128
currently lose_sum: 87.58485674858093
time_elpased: 1.37
batch start
#iterations: 129
currently lose_sum: 86.91591322422028
time_elpased: 1.327
batch start
#iterations: 130
currently lose_sum: 87.06039553880692
time_elpased: 1.342
batch start
#iterations: 131
currently lose_sum: 87.07082879543304
time_elpased: 1.34
batch start
#iterations: 132
currently lose_sum: 86.7259252667427
time_elpased: 1.352
batch start
#iterations: 133
currently lose_sum: 86.84166556596756
time_elpased: 1.357
batch start
#iterations: 134
currently lose_sum: 86.6233742237091
time_elpased: 1.336
batch start
#iterations: 135
currently lose_sum: 87.08978253602982
time_elpased: 1.356
batch start
#iterations: 136
currently lose_sum: 86.35303378105164
time_elpased: 1.381
batch start
#iterations: 137
currently lose_sum: 86.45506566762924
time_elpased: 1.334
batch start
#iterations: 138
currently lose_sum: 87.48779326677322
time_elpased: 1.34
batch start
#iterations: 139
currently lose_sum: 86.46086066961288
time_elpased: 1.315
start validation test
0.617680412371
0.641905231984
0.535350416795
0.58380562258
0.617824955377
63.573
batch start
#iterations: 140
currently lose_sum: 86.73121303319931
time_elpased: 1.344
batch start
#iterations: 141
currently lose_sum: 86.95029431581497
time_elpased: 1.331
batch start
#iterations: 142
currently lose_sum: 86.69772803783417
time_elpased: 1.36
batch start
#iterations: 143
currently lose_sum: 86.7630444765091
time_elpased: 1.341
batch start
#iterations: 144
currently lose_sum: 86.20912516117096
time_elpased: 1.355
batch start
#iterations: 145
currently lose_sum: 86.7314805984497
time_elpased: 1.315
batch start
#iterations: 146
currently lose_sum: 86.15354681015015
time_elpased: 1.336
batch start
#iterations: 147
currently lose_sum: 85.89272445440292
time_elpased: 1.334
batch start
#iterations: 148
currently lose_sum: 86.90750247240067
time_elpased: 1.334
batch start
#iterations: 149
currently lose_sum: 86.521877348423
time_elpased: 1.351
batch start
#iterations: 150
currently lose_sum: 86.35105192661285
time_elpased: 1.342
batch start
#iterations: 151
currently lose_sum: 86.1592983007431
time_elpased: 1.347
batch start
#iterations: 152
currently lose_sum: 86.35155844688416
time_elpased: 1.338
batch start
#iterations: 153
currently lose_sum: 86.19739466905594
time_elpased: 1.339
batch start
#iterations: 154
currently lose_sum: 86.91427046060562
time_elpased: 1.345
batch start
#iterations: 155
currently lose_sum: 86.23056411743164
time_elpased: 1.345
batch start
#iterations: 156
currently lose_sum: 85.69678062200546
time_elpased: 1.339
batch start
#iterations: 157
currently lose_sum: 86.39303398132324
time_elpased: 1.346
batch start
#iterations: 158
currently lose_sum: 86.5597494840622
time_elpased: 1.363
batch start
#iterations: 159
currently lose_sum: 86.57232147455215
time_elpased: 1.354
start validation test
0.612886597938
0.638300538915
0.524132962849
0.575610307414
0.613042418634
64.095
batch start
#iterations: 160
currently lose_sum: 85.89669835567474
time_elpased: 1.359
batch start
#iterations: 161
currently lose_sum: 85.98245739936829
time_elpased: 1.327
batch start
#iterations: 162
currently lose_sum: 85.63256680965424
time_elpased: 1.324
batch start
#iterations: 163
currently lose_sum: 85.74024438858032
time_elpased: 1.346
batch start
#iterations: 164
currently lose_sum: 86.06357526779175
time_elpased: 1.343
batch start
#iterations: 165
currently lose_sum: 85.8239198923111
time_elpased: 1.349
batch start
#iterations: 166
currently lose_sum: 85.89627462625504
time_elpased: 1.328
batch start
#iterations: 167
currently lose_sum: 85.67542779445648
time_elpased: 1.354
batch start
#iterations: 168
currently lose_sum: 85.5055525302887
time_elpased: 1.339
batch start
#iterations: 169
currently lose_sum: 85.57725214958191
time_elpased: 1.344
batch start
#iterations: 170
currently lose_sum: 85.46278381347656
time_elpased: 1.362
batch start
#iterations: 171
currently lose_sum: 85.98273706436157
time_elpased: 1.335
batch start
#iterations: 172
currently lose_sum: 85.86688071489334
time_elpased: 1.346
batch start
#iterations: 173
currently lose_sum: 85.37932643294334
time_elpased: 1.354
batch start
#iterations: 174
currently lose_sum: 85.09672594070435
time_elpased: 1.335
batch start
#iterations: 175
currently lose_sum: 85.29669106006622
time_elpased: 1.335
batch start
#iterations: 176
currently lose_sum: 86.0636316537857
time_elpased: 1.364
batch start
#iterations: 177
currently lose_sum: 85.04020154476166
time_elpased: 1.342
batch start
#iterations: 178
currently lose_sum: 85.20657098293304
time_elpased: 1.341
batch start
#iterations: 179
currently lose_sum: 85.74527168273926
time_elpased: 1.351
start validation test
0.596649484536
0.645583256387
0.431717608315
0.517422139994
0.596939047884
66.094
batch start
#iterations: 180
currently lose_sum: 85.27772307395935
time_elpased: 1.352
batch start
#iterations: 181
currently lose_sum: 84.99460160732269
time_elpased: 1.354
batch start
#iterations: 182
currently lose_sum: 85.50072377920151
time_elpased: 1.325
batch start
#iterations: 183
currently lose_sum: 85.15665745735168
time_elpased: 1.335
batch start
#iterations: 184
currently lose_sum: 85.5615336894989
time_elpased: 1.351
batch start
#iterations: 185
currently lose_sum: 84.944060921669
time_elpased: 1.346
batch start
#iterations: 186
currently lose_sum: 85.17054229974747
time_elpased: 1.33
batch start
#iterations: 187
currently lose_sum: 85.12326335906982
time_elpased: 1.331
batch start
#iterations: 188
currently lose_sum: 85.49316728115082
time_elpased: 1.336
batch start
#iterations: 189
currently lose_sum: 85.01559007167816
time_elpased: 1.355
batch start
#iterations: 190
currently lose_sum: 84.68944019079208
time_elpased: 1.37
batch start
#iterations: 191
currently lose_sum: 85.1708151102066
time_elpased: 1.318
batch start
#iterations: 192
currently lose_sum: 84.66609209775925
time_elpased: 1.386
batch start
#iterations: 193
currently lose_sum: 84.92791336774826
time_elpased: 1.339
batch start
#iterations: 194
currently lose_sum: 84.90543395280838
time_elpased: 1.356
batch start
#iterations: 195
currently lose_sum: 85.10066801309586
time_elpased: 1.345
batch start
#iterations: 196
currently lose_sum: 85.2280370593071
time_elpased: 1.357
batch start
#iterations: 197
currently lose_sum: 85.04393166303635
time_elpased: 1.34
batch start
#iterations: 198
currently lose_sum: 84.65748471021652
time_elpased: 1.326
batch start
#iterations: 199
currently lose_sum: 84.66171032190323
time_elpased: 1.351
start validation test
0.603762886598
0.645
0.464649583205
0.540168690555
0.604007121459
65.787
batch start
#iterations: 200
currently lose_sum: 85.0540281534195
time_elpased: 1.344
batch start
#iterations: 201
currently lose_sum: 84.38023263216019
time_elpased: 1.342
batch start
#iterations: 202
currently lose_sum: 84.20704835653305
time_elpased: 1.36
batch start
#iterations: 203
currently lose_sum: 84.37254798412323
time_elpased: 1.329
batch start
#iterations: 204
currently lose_sum: 84.36324959993362
time_elpased: 1.344
batch start
#iterations: 205
currently lose_sum: 84.05721631646156
time_elpased: 1.373
batch start
#iterations: 206
currently lose_sum: 84.9178284406662
time_elpased: 1.348
batch start
#iterations: 207
currently lose_sum: 84.68759047985077
time_elpased: 1.368
batch start
#iterations: 208
currently lose_sum: 84.00970131158829
time_elpased: 1.343
batch start
#iterations: 209
currently lose_sum: 83.88931286334991
time_elpased: 1.348
batch start
#iterations: 210
currently lose_sum: 83.99246776103973
time_elpased: 1.349
batch start
#iterations: 211
currently lose_sum: 84.17420521378517
time_elpased: 1.324
batch start
#iterations: 212
currently lose_sum: 84.26752033829689
time_elpased: 1.389
batch start
#iterations: 213
currently lose_sum: 84.1029605269432
time_elpased: 1.345
batch start
#iterations: 214
currently lose_sum: 84.13508599996567
time_elpased: 1.345
batch start
#iterations: 215
currently lose_sum: 83.5804149210453
time_elpased: 1.34
batch start
#iterations: 216
currently lose_sum: 83.97447353601456
time_elpased: 1.352
batch start
#iterations: 217
currently lose_sum: 84.53958809375763
time_elpased: 1.335
batch start
#iterations: 218
currently lose_sum: 84.372061252594
time_elpased: 1.39
batch start
#iterations: 219
currently lose_sum: 84.09482824802399
time_elpased: 1.391
start validation test
0.605309278351
0.64722698685
0.465987444685
0.541853646862
0.605553879318
65.433
batch start
#iterations: 220
currently lose_sum: 84.2871749997139
time_elpased: 1.361
batch start
#iterations: 221
currently lose_sum: 83.83774244785309
time_elpased: 1.343
batch start
#iterations: 222
currently lose_sum: 84.26198625564575
time_elpased: 1.405
batch start
#iterations: 223
currently lose_sum: 83.50981831550598
time_elpased: 1.349
batch start
#iterations: 224
currently lose_sum: 84.11394047737122
time_elpased: 1.342
batch start
#iterations: 225
currently lose_sum: 83.88706988096237
time_elpased: 1.345
batch start
#iterations: 226
currently lose_sum: 83.89286893606186
time_elpased: 1.397
batch start
#iterations: 227
currently lose_sum: 83.53288292884827
time_elpased: 1.329
batch start
#iterations: 228
currently lose_sum: 83.3177872300148
time_elpased: 1.355
batch start
#iterations: 229
currently lose_sum: 83.60656860470772
time_elpased: 1.338
batch start
#iterations: 230
currently lose_sum: 84.16053855419159
time_elpased: 1.34
batch start
#iterations: 231
currently lose_sum: 84.29440069198608
time_elpased: 1.346
batch start
#iterations: 232
currently lose_sum: 83.35001546144485
time_elpased: 1.351
batch start
#iterations: 233
currently lose_sum: 83.8475513458252
time_elpased: 1.374
batch start
#iterations: 234
currently lose_sum: 83.78194642066956
time_elpased: 1.334
batch start
#iterations: 235
currently lose_sum: 83.78357261419296
time_elpased: 1.348
batch start
#iterations: 236
currently lose_sum: 83.4725170135498
time_elpased: 1.356
batch start
#iterations: 237
currently lose_sum: 83.68057590723038
time_elpased: 1.342
batch start
#iterations: 238
currently lose_sum: 83.60833609104156
time_elpased: 1.392
batch start
#iterations: 239
currently lose_sum: 83.79267281293869
time_elpased: 1.321
start validation test
0.593711340206
0.639884128678
0.431923433158
0.515728680265
0.593995383831
67.596
batch start
#iterations: 240
currently lose_sum: 83.5951583981514
time_elpased: 1.332
batch start
#iterations: 241
currently lose_sum: 83.54771018028259
time_elpased: 1.339
batch start
#iterations: 242
currently lose_sum: 83.72718751430511
time_elpased: 1.343
batch start
#iterations: 243
currently lose_sum: 83.79571208357811
time_elpased: 1.327
batch start
#iterations: 244
currently lose_sum: 83.98115974664688
time_elpased: 1.342
batch start
#iterations: 245
currently lose_sum: 83.64721575379372
time_elpased: 1.336
batch start
#iterations: 246
currently lose_sum: 83.73715192079544
time_elpased: 1.346
batch start
#iterations: 247
currently lose_sum: 83.7049834728241
time_elpased: 1.334
batch start
#iterations: 248
currently lose_sum: 83.77722638845444
time_elpased: 1.339
batch start
#iterations: 249
currently lose_sum: 83.7377381324768
time_elpased: 1.329
batch start
#iterations: 250
currently lose_sum: 83.63469296693802
time_elpased: 1.35
batch start
#iterations: 251
currently lose_sum: 83.19805645942688
time_elpased: 1.335
batch start
#iterations: 252
currently lose_sum: 83.8775082230568
time_elpased: 1.349
batch start
#iterations: 253
currently lose_sum: 83.30689027905464
time_elpased: 1.339
batch start
#iterations: 254
currently lose_sum: 83.24669256806374
time_elpased: 1.346
batch start
#iterations: 255
currently lose_sum: 82.22759246826172
time_elpased: 1.331
batch start
#iterations: 256
currently lose_sum: 82.58294987678528
time_elpased: 1.366
batch start
#iterations: 257
currently lose_sum: 83.24451819062233
time_elpased: 1.313
batch start
#iterations: 258
currently lose_sum: 82.81059974431992
time_elpased: 1.333
batch start
#iterations: 259
currently lose_sum: 82.52180668711662
time_elpased: 1.373
start validation test
0.601804123711
0.642530051517
0.462076772666
0.537563603711
0.602049436628
66.379
batch start
#iterations: 260
currently lose_sum: 82.98651877045631
time_elpased: 1.372
batch start
#iterations: 261
currently lose_sum: 82.97925707697868
time_elpased: 1.341
batch start
#iterations: 262
currently lose_sum: 82.82652962207794
time_elpased: 1.35
batch start
#iterations: 263
currently lose_sum: 82.94532468914986
time_elpased: 1.342
batch start
#iterations: 264
currently lose_sum: 83.06029272079468
time_elpased: 1.341
batch start
#iterations: 265
currently lose_sum: 83.02101761102676
time_elpased: 1.324
batch start
#iterations: 266
currently lose_sum: 83.30170994997025
time_elpased: 1.369
batch start
#iterations: 267
currently lose_sum: 83.17128539085388
time_elpased: 1.328
batch start
#iterations: 268
currently lose_sum: 82.81085470318794
time_elpased: 1.366
batch start
#iterations: 269
currently lose_sum: 83.14778220653534
time_elpased: 1.327
batch start
#iterations: 270
currently lose_sum: 82.19870844483376
time_elpased: 1.347
batch start
#iterations: 271
currently lose_sum: 83.25977593660355
time_elpased: 1.32
batch start
#iterations: 272
currently lose_sum: 83.14791107177734
time_elpased: 1.351
batch start
#iterations: 273
currently lose_sum: 82.82356885075569
time_elpased: 1.341
batch start
#iterations: 274
currently lose_sum: 83.15516602993011
time_elpased: 1.355
batch start
#iterations: 275
currently lose_sum: 82.92802411317825
time_elpased: 1.328
batch start
#iterations: 276
currently lose_sum: 82.22018975019455
time_elpased: 1.365
batch start
#iterations: 277
currently lose_sum: 82.90935277938843
time_elpased: 1.328
batch start
#iterations: 278
currently lose_sum: 82.86160880327225
time_elpased: 1.385
batch start
#iterations: 279
currently lose_sum: 82.16476702690125
time_elpased: 1.327
start validation test
0.598041237113
0.642337932058
0.445610785222
0.526187872159
0.598308852282
67.076
batch start
#iterations: 280
currently lose_sum: 82.57212546467781
time_elpased: 1.347
batch start
#iterations: 281
currently lose_sum: 82.99200916290283
time_elpased: 1.326
batch start
#iterations: 282
currently lose_sum: 82.26894313097
time_elpased: 1.345
batch start
#iterations: 283
currently lose_sum: 82.2851397395134
time_elpased: 1.334
batch start
#iterations: 284
currently lose_sum: 82.00505191087723
time_elpased: 1.372
batch start
#iterations: 285
currently lose_sum: 82.1296415925026
time_elpased: 1.36
batch start
#iterations: 286
currently lose_sum: 82.55967563390732
time_elpased: 1.338
batch start
#iterations: 287
currently lose_sum: 81.93337526917458
time_elpased: 1.338
batch start
#iterations: 288
currently lose_sum: 82.23242974281311
time_elpased: 1.38
batch start
#iterations: 289
currently lose_sum: 82.07851600646973
time_elpased: 1.342
batch start
#iterations: 290
currently lose_sum: 82.75787419080734
time_elpased: 1.367
batch start
#iterations: 291
currently lose_sum: 81.97366493940353
time_elpased: 1.362
batch start
#iterations: 292
currently lose_sum: 82.60512870550156
time_elpased: 1.368
batch start
#iterations: 293
currently lose_sum: 82.17353057861328
time_elpased: 1.36
batch start
#iterations: 294
currently lose_sum: 82.3064381480217
time_elpased: 1.371
batch start
#iterations: 295
currently lose_sum: 82.06354162096977
time_elpased: 1.327
batch start
#iterations: 296
currently lose_sum: 82.4661220908165
time_elpased: 1.377
batch start
#iterations: 297
currently lose_sum: 82.13459149003029
time_elpased: 1.346
batch start
#iterations: 298
currently lose_sum: 82.49201893806458
time_elpased: 1.345
batch start
#iterations: 299
currently lose_sum: 82.29133975505829
time_elpased: 1.314
start validation test
0.601443298969
0.645549200763
0.453020479572
0.532414126754
0.601703878121
67.507
batch start
#iterations: 300
currently lose_sum: 81.9175195991993
time_elpased: 1.381
batch start
#iterations: 301
currently lose_sum: 82.81002333760262
time_elpased: 1.349
batch start
#iterations: 302
currently lose_sum: 81.87190270423889
time_elpased: 1.353
batch start
#iterations: 303
currently lose_sum: 82.6675289273262
time_elpased: 1.337
batch start
#iterations: 304
currently lose_sum: 82.22573092579842
time_elpased: 1.357
batch start
#iterations: 305
currently lose_sum: 81.54917275905609
time_elpased: 1.327
batch start
#iterations: 306
currently lose_sum: 82.3603820502758
time_elpased: 1.36
batch start
#iterations: 307
currently lose_sum: 82.28650724887848
time_elpased: 1.339
batch start
#iterations: 308
currently lose_sum: 82.50849854946136
time_elpased: 1.358
batch start
#iterations: 309
currently lose_sum: 81.78365397453308
time_elpased: 1.323
batch start
#iterations: 310
currently lose_sum: 82.6261337697506
time_elpased: 1.341
batch start
#iterations: 311
currently lose_sum: 82.06744655966759
time_elpased: 1.351
batch start
#iterations: 312
currently lose_sum: 81.87505650520325
time_elpased: 1.388
batch start
#iterations: 313
currently lose_sum: 81.82887861132622
time_elpased: 1.35
batch start
#iterations: 314
currently lose_sum: 81.98605677485466
time_elpased: 1.351
batch start
#iterations: 315
currently lose_sum: 81.72698125243187
time_elpased: 1.332
batch start
#iterations: 316
currently lose_sum: 81.88945084810257
time_elpased: 1.347
batch start
#iterations: 317
currently lose_sum: 81.76365992426872
time_elpased: 1.327
batch start
#iterations: 318
currently lose_sum: 81.84523195028305
time_elpased: 1.369
batch start
#iterations: 319
currently lose_sum: 82.20440995693207
time_elpased: 1.387
start validation test
0.598298969072
0.641887905605
0.447874858495
0.527611080803
0.5985630618
67.947
batch start
#iterations: 320
currently lose_sum: 82.26726683974266
time_elpased: 1.349
batch start
#iterations: 321
currently lose_sum: 81.96476870775223
time_elpased: 1.339
batch start
#iterations: 322
currently lose_sum: 81.91313487291336
time_elpased: 1.343
batch start
#iterations: 323
currently lose_sum: 81.65988230705261
time_elpased: 1.334
batch start
#iterations: 324
currently lose_sum: 82.49913817644119
time_elpased: 1.351
batch start
#iterations: 325
currently lose_sum: 82.45236372947693
time_elpased: 1.351
batch start
#iterations: 326
currently lose_sum: 81.56886956095695
time_elpased: 1.357
batch start
#iterations: 327
currently lose_sum: 81.59808614850044
time_elpased: 1.354
batch start
#iterations: 328
currently lose_sum: 81.66196751594543
time_elpased: 1.343
batch start
#iterations: 329
currently lose_sum: 81.48603793978691
time_elpased: 1.329
batch start
#iterations: 330
currently lose_sum: 81.43209278583527
time_elpased: 1.364
batch start
#iterations: 331
currently lose_sum: 82.03356593847275
time_elpased: 1.314
batch start
#iterations: 332
currently lose_sum: 81.47874516248703
time_elpased: 1.387
batch start
#iterations: 333
currently lose_sum: 80.86123865842819
time_elpased: 1.33
batch start
#iterations: 334
currently lose_sum: 81.24632108211517
time_elpased: 1.334
batch start
#iterations: 335
currently lose_sum: 81.67258933186531
time_elpased: 1.331
batch start
#iterations: 336
currently lose_sum: 81.21847262978554
time_elpased: 1.334
batch start
#iterations: 337
currently lose_sum: 81.74183529615402
time_elpased: 1.333
batch start
#iterations: 338
currently lose_sum: 81.56835809350014
time_elpased: 1.369
batch start
#iterations: 339
currently lose_sum: 81.27024182677269
time_elpased: 1.347
start validation test
0.597319587629
0.638103523271
0.45291756715
0.529794149512
0.597573107648
67.683
batch start
#iterations: 340
currently lose_sum: 81.70516812801361
time_elpased: 1.334
batch start
#iterations: 341
currently lose_sum: 81.45949563384056
time_elpased: 1.337
batch start
#iterations: 342
currently lose_sum: 82.22480949759483
time_elpased: 1.35
batch start
#iterations: 343
currently lose_sum: 81.93551152944565
time_elpased: 1.336
batch start
#iterations: 344
currently lose_sum: 81.11727103590965
time_elpased: 1.342
batch start
#iterations: 345
currently lose_sum: 81.24025568366051
time_elpased: 1.341
batch start
#iterations: 346
currently lose_sum: 81.36092448234558
time_elpased: 1.336
batch start
#iterations: 347
currently lose_sum: 82.10984319448471
time_elpased: 1.346
batch start
#iterations: 348
currently lose_sum: 82.01622873544693
time_elpased: 1.35
batch start
#iterations: 349
currently lose_sum: 81.35120570659637
time_elpased: 1.342
batch start
#iterations: 350
currently lose_sum: 81.63936951756477
time_elpased: 1.346
batch start
#iterations: 351
currently lose_sum: 81.16407522559166
time_elpased: 1.35
batch start
#iterations: 352
currently lose_sum: 81.4477588236332
time_elpased: 1.331
batch start
#iterations: 353
currently lose_sum: 81.78553375601768
time_elpased: 1.333
batch start
#iterations: 354
currently lose_sum: 81.85658252239227
time_elpased: 1.333
batch start
#iterations: 355
currently lose_sum: 82.1855328977108
time_elpased: 1.337
batch start
#iterations: 356
currently lose_sum: 81.31033200025558
time_elpased: 1.358
batch start
#iterations: 357
currently lose_sum: 81.11313730478287
time_elpased: 1.363
batch start
#iterations: 358
currently lose_sum: 81.72789013385773
time_elpased: 1.333
batch start
#iterations: 359
currently lose_sum: 81.88854613900185
time_elpased: 1.363
start validation test
0.595618556701
0.637971698113
0.445404960379
0.524574268226
0.595882279838
68.553
batch start
#iterations: 360
currently lose_sum: 81.98413780331612
time_elpased: 1.368
batch start
#iterations: 361
currently lose_sum: 81.34498301148415
time_elpased: 1.329
batch start
#iterations: 362
currently lose_sum: 81.18104347586632
time_elpased: 1.383
batch start
#iterations: 363
currently lose_sum: 80.90505760908127
time_elpased: 1.386
batch start
#iterations: 364
currently lose_sum: 81.47606164216995
time_elpased: 1.342
batch start
#iterations: 365
currently lose_sum: 81.05145600438118
time_elpased: 1.351
batch start
#iterations: 366
currently lose_sum: 81.54661571979523
time_elpased: 1.347
batch start
#iterations: 367
currently lose_sum: 81.3823399245739
time_elpased: 1.343
batch start
#iterations: 368
currently lose_sum: 81.00599163770676
time_elpased: 1.342
batch start
#iterations: 369
currently lose_sum: 80.72563579678535
time_elpased: 1.339
batch start
#iterations: 370
currently lose_sum: 81.06893122196198
time_elpased: 1.357
batch start
#iterations: 371
currently lose_sum: 80.77380019426346
time_elpased: 1.342
batch start
#iterations: 372
currently lose_sum: 81.29556804895401
time_elpased: 1.364
batch start
#iterations: 373
currently lose_sum: 81.09114062786102
time_elpased: 1.347
batch start
#iterations: 374
currently lose_sum: 80.9172328710556
time_elpased: 1.362
batch start
#iterations: 375
currently lose_sum: 81.08448776602745
time_elpased: 1.322
batch start
#iterations: 376
currently lose_sum: 80.90069469809532
time_elpased: 1.361
batch start
#iterations: 377
currently lose_sum: 81.6393226981163
time_elpased: 1.348
batch start
#iterations: 378
currently lose_sum: 80.95927506685257
time_elpased: 1.349
batch start
#iterations: 379
currently lose_sum: 81.06552290916443
time_elpased: 1.325
start validation test
0.594020618557
0.639787395596
0.433570031903
0.516869095816
0.594302314309
69.196
batch start
#iterations: 380
currently lose_sum: 81.18304267525673
time_elpased: 1.352
batch start
#iterations: 381
currently lose_sum: 81.13514891266823
time_elpased: 1.333
batch start
#iterations: 382
currently lose_sum: 81.21153795719147
time_elpased: 1.355
batch start
#iterations: 383
currently lose_sum: 81.14716327190399
time_elpased: 1.341
batch start
#iterations: 384
currently lose_sum: 80.56171825528145
time_elpased: 1.342
batch start
#iterations: 385
currently lose_sum: 81.57492244243622
time_elpased: 1.335
batch start
#iterations: 386
currently lose_sum: 81.01039776206017
time_elpased: 1.416
batch start
#iterations: 387
currently lose_sum: 80.88166403770447
time_elpased: 1.335
batch start
#iterations: 388
currently lose_sum: 80.30063369870186
time_elpased: 1.351
batch start
#iterations: 389
currently lose_sum: 80.88319942355156
time_elpased: 1.333
batch start
#iterations: 390
currently lose_sum: 80.6378321647644
time_elpased: 1.34
batch start
#iterations: 391
currently lose_sum: 80.60838136076927
time_elpased: 1.349
batch start
#iterations: 392
currently lose_sum: 80.4608351290226
time_elpased: 1.347
batch start
#iterations: 393
currently lose_sum: 79.86037561297417
time_elpased: 1.315
batch start
#iterations: 394
currently lose_sum: 80.99787589907646
time_elpased: 1.361
batch start
#iterations: 395
currently lose_sum: 81.51422846317291
time_elpased: 1.35
batch start
#iterations: 396
currently lose_sum: 81.16758468747139
time_elpased: 1.348
batch start
#iterations: 397
currently lose_sum: 80.87014573812485
time_elpased: 1.329
batch start
#iterations: 398
currently lose_sum: 80.47091591358185
time_elpased: 1.352
batch start
#iterations: 399
currently lose_sum: 81.51883429288864
time_elpased: 1.337
start validation test
0.596134020619
0.637814879906
0.44818359576
0.526443034149
0.596393770409
68.765
acc: 0.640
pre: 0.659
rec: 0.581
F1: 0.617
auc: 0.689
