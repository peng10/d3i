start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.3753610253334
time_elpased: 1.478
batch start
#iterations: 1
currently lose_sum: 99.88973248004913
time_elpased: 1.41
batch start
#iterations: 2
currently lose_sum: 99.48250246047974
time_elpased: 1.415
batch start
#iterations: 3
currently lose_sum: 98.85899895429611
time_elpased: 1.471
batch start
#iterations: 4
currently lose_sum: 98.6075935959816
time_elpased: 1.471
batch start
#iterations: 5
currently lose_sum: 98.03164219856262
time_elpased: 1.425
batch start
#iterations: 6
currently lose_sum: 97.48604363203049
time_elpased: 1.424
batch start
#iterations: 7
currently lose_sum: 96.89468598365784
time_elpased: 1.442
batch start
#iterations: 8
currently lose_sum: 96.6134123802185
time_elpased: 1.418
batch start
#iterations: 9
currently lose_sum: 96.33462625741959
time_elpased: 1.448
batch start
#iterations: 10
currently lose_sum: 95.64973038434982
time_elpased: 1.436
batch start
#iterations: 11
currently lose_sum: 95.20172870159149
time_elpased: 1.45
batch start
#iterations: 12
currently lose_sum: 95.32277655601501
time_elpased: 1.423
batch start
#iterations: 13
currently lose_sum: 95.87720781564713
time_elpased: 1.455
batch start
#iterations: 14
currently lose_sum: 95.05112910270691
time_elpased: 1.416
batch start
#iterations: 15
currently lose_sum: 95.05857783555984
time_elpased: 1.417
batch start
#iterations: 16
currently lose_sum: 94.75970667600632
time_elpased: 1.413
batch start
#iterations: 17
currently lose_sum: 94.66137534379959
time_elpased: 1.447
batch start
#iterations: 18
currently lose_sum: 94.19315475225449
time_elpased: 1.411
batch start
#iterations: 19
currently lose_sum: 93.91308981180191
time_elpased: 1.43
start validation test
0.634845360825
0.618858381503
0.70522848909
0.659226476814
0.634729073128
62.061
batch start
#iterations: 20
currently lose_sum: 93.49147462844849
time_elpased: 1.46
batch start
#iterations: 21
currently lose_sum: 93.58640986680984
time_elpased: 1.42
batch start
#iterations: 22
currently lose_sum: 93.39953011274338
time_elpased: 1.474
batch start
#iterations: 23
currently lose_sum: 93.36411666870117
time_elpased: 1.431
batch start
#iterations: 24
currently lose_sum: 93.23036104440689
time_elpased: 1.409
batch start
#iterations: 25
currently lose_sum: 93.19255709648132
time_elpased: 1.426
batch start
#iterations: 26
currently lose_sum: 92.8676118850708
time_elpased: 1.419
batch start
#iterations: 27
currently lose_sum: 92.47699254751205
time_elpased: 1.421
batch start
#iterations: 28
currently lose_sum: 92.3406274318695
time_elpased: 1.441
batch start
#iterations: 29
currently lose_sum: 92.65694814920425
time_elpased: 1.465
batch start
#iterations: 30
currently lose_sum: 92.16949218511581
time_elpased: 1.394
batch start
#iterations: 31
currently lose_sum: 92.07363820075989
time_elpased: 1.463
batch start
#iterations: 32
currently lose_sum: 91.7919420003891
time_elpased: 1.404
batch start
#iterations: 33
currently lose_sum: 91.35676074028015
time_elpased: 1.423
batch start
#iterations: 34
currently lose_sum: 91.17721742391586
time_elpased: 1.455
batch start
#iterations: 35
currently lose_sum: 92.07901018857956
time_elpased: 1.42
batch start
#iterations: 36
currently lose_sum: 91.49969553947449
time_elpased: 1.403
batch start
#iterations: 37
currently lose_sum: 90.91149687767029
time_elpased: 1.451
batch start
#iterations: 38
currently lose_sum: 91.03572255373001
time_elpased: 1.45
batch start
#iterations: 39
currently lose_sum: 90.78989267349243
time_elpased: 1.449
start validation test
0.617164948454
0.603884905147
0.684746809387
0.641778806733
0.617053289039
62.877
batch start
#iterations: 40
currently lose_sum: 90.82120156288147
time_elpased: 1.43
batch start
#iterations: 41
currently lose_sum: 90.96493381261826
time_elpased: 1.413
batch start
#iterations: 42
currently lose_sum: 90.49096143245697
time_elpased: 1.395
batch start
#iterations: 43
currently lose_sum: 90.24873566627502
time_elpased: 1.418
batch start
#iterations: 44
currently lose_sum: 90.36989498138428
time_elpased: 1.418
batch start
#iterations: 45
currently lose_sum: 90.36862808465958
time_elpased: 1.436
batch start
#iterations: 46
currently lose_sum: 89.73020720481873
time_elpased: 1.425
batch start
#iterations: 47
currently lose_sum: 89.49329382181168
time_elpased: 1.443
batch start
#iterations: 48
currently lose_sum: 89.7583698630333
time_elpased: 1.441
batch start
#iterations: 49
currently lose_sum: 89.51786762475967
time_elpased: 1.407
batch start
#iterations: 50
currently lose_sum: 89.58070904016495
time_elpased: 1.447
batch start
#iterations: 51
currently lose_sum: 89.2441309094429
time_elpased: 1.384
batch start
#iterations: 52
currently lose_sum: 89.64343893527985
time_elpased: 1.447
batch start
#iterations: 53
currently lose_sum: 88.74273157119751
time_elpased: 1.426
batch start
#iterations: 54
currently lose_sum: 89.31545478105545
time_elpased: 1.465
batch start
#iterations: 55
currently lose_sum: 88.93939661979675
time_elpased: 1.439
batch start
#iterations: 56
currently lose_sum: 88.96596395969391
time_elpased: 1.408
batch start
#iterations: 57
currently lose_sum: 88.94273430109024
time_elpased: 1.404
batch start
#iterations: 58
currently lose_sum: 88.51851308345795
time_elpased: 1.387
batch start
#iterations: 59
currently lose_sum: 89.03776496648788
time_elpased: 1.4
start validation test
0.614690721649
0.615147466858
0.616097159325
0.61562194683
0.614688397919
63.747
batch start
#iterations: 60
currently lose_sum: 88.42522066831589
time_elpased: 1.415
batch start
#iterations: 61
currently lose_sum: 88.81069213151932
time_elpased: 1.411
batch start
#iterations: 62
currently lose_sum: 88.37198460102081
time_elpased: 1.411
batch start
#iterations: 63
currently lose_sum: 88.62215650081635
time_elpased: 1.442
batch start
#iterations: 64
currently lose_sum: 88.0998854637146
time_elpased: 1.4
batch start
#iterations: 65
currently lose_sum: 88.06951105594635
time_elpased: 1.483
batch start
#iterations: 66
currently lose_sum: 88.25807082653046
time_elpased: 1.432
batch start
#iterations: 67
currently lose_sum: 88.03658241033554
time_elpased: 1.423
batch start
#iterations: 68
currently lose_sum: 87.87505161762238
time_elpased: 1.419
batch start
#iterations: 69
currently lose_sum: 87.18004494905472
time_elpased: 1.457
batch start
#iterations: 70
currently lose_sum: 87.52410781383514
time_elpased: 1.402
batch start
#iterations: 71
currently lose_sum: 86.99175959825516
time_elpased: 1.431
batch start
#iterations: 72
currently lose_sum: 87.1194161772728
time_elpased: 1.483
batch start
#iterations: 73
currently lose_sum: 86.93269485235214
time_elpased: 1.41
batch start
#iterations: 74
currently lose_sum: 86.92599564790726
time_elpased: 1.437
batch start
#iterations: 75
currently lose_sum: 87.08403754234314
time_elpased: 1.454
batch start
#iterations: 76
currently lose_sum: 87.24426800012589
time_elpased: 1.425
batch start
#iterations: 77
currently lose_sum: 86.81115734577179
time_elpased: 1.482
batch start
#iterations: 78
currently lose_sum: 87.0910108089447
time_elpased: 1.403
batch start
#iterations: 79
currently lose_sum: 87.12417858839035
time_elpased: 1.413
start validation test
0.629742268041
0.626057529611
0.647385755455
0.636543034964
0.629713117298
64.718
batch start
#iterations: 80
currently lose_sum: 86.80166006088257
time_elpased: 1.434
batch start
#iterations: 81
currently lose_sum: 86.75196397304535
time_elpased: 1.446
batch start
#iterations: 82
currently lose_sum: 86.1175981760025
time_elpased: 1.413
batch start
#iterations: 83
currently lose_sum: 86.37795913219452
time_elpased: 1.455
batch start
#iterations: 84
currently lose_sum: 86.315409719944
time_elpased: 1.412
batch start
#iterations: 85
currently lose_sum: 85.82013887166977
time_elpased: 1.53
batch start
#iterations: 86
currently lose_sum: 86.38754081726074
time_elpased: 1.41
batch start
#iterations: 87
currently lose_sum: 85.79557985067368
time_elpased: 1.458
batch start
#iterations: 88
currently lose_sum: 86.24636554718018
time_elpased: 1.432
batch start
#iterations: 89
currently lose_sum: 85.99976170063019
time_elpased: 1.425
batch start
#iterations: 90
currently lose_sum: 86.32970434427261
time_elpased: 1.411
batch start
#iterations: 91
currently lose_sum: 85.559112906456
time_elpased: 1.477
batch start
#iterations: 92
currently lose_sum: 85.66566723585129
time_elpased: 1.43
batch start
#iterations: 93
currently lose_sum: 85.05679160356522
time_elpased: 1.405
batch start
#iterations: 94
currently lose_sum: 86.194096326828
time_elpased: 1.436
batch start
#iterations: 95
currently lose_sum: 85.27516037225723
time_elpased: 1.432
batch start
#iterations: 96
currently lose_sum: 85.14115905761719
time_elpased: 1.404
batch start
#iterations: 97
currently lose_sum: 85.42032524943352
time_elpased: 1.404
batch start
#iterations: 98
currently lose_sum: 84.83155822753906
time_elpased: 1.404
batch start
#iterations: 99
currently lose_sum: 84.87001746892929
time_elpased: 1.417
start validation test
0.623041237113
0.624546491137
0.620111156855
0.622320921345
0.623046078221
66.772
batch start
#iterations: 100
currently lose_sum: 85.77716982364655
time_elpased: 1.418
batch start
#iterations: 101
currently lose_sum: 85.94616335630417
time_elpased: 1.416
batch start
#iterations: 102
currently lose_sum: 85.18697875738144
time_elpased: 1.401
batch start
#iterations: 103
currently lose_sum: 85.01489794254303
time_elpased: 1.424
batch start
#iterations: 104
currently lose_sum: 84.70200103521347
time_elpased: 1.418
batch start
#iterations: 105
currently lose_sum: 84.94512194395065
time_elpased: 1.475
batch start
#iterations: 106
currently lose_sum: 84.63663345575333
time_elpased: 1.426
batch start
#iterations: 107
currently lose_sum: 84.80596768856049
time_elpased: 1.418
batch start
#iterations: 108
currently lose_sum: 84.43104431033134
time_elpased: 1.453
batch start
#iterations: 109
currently lose_sum: 84.43233680725098
time_elpased: 1.436
batch start
#iterations: 110
currently lose_sum: 84.58611053228378
time_elpased: 1.444
batch start
#iterations: 111
currently lose_sum: 85.25496566295624
time_elpased: 1.429
batch start
#iterations: 112
currently lose_sum: 84.67697215080261
time_elpased: 1.43
batch start
#iterations: 113
currently lose_sum: 84.00605005025864
time_elpased: 1.404
batch start
#iterations: 114
currently lose_sum: 84.44273614883423
time_elpased: 1.486
batch start
#iterations: 115
currently lose_sum: 83.79081106185913
time_elpased: 1.441
batch start
#iterations: 116
currently lose_sum: 84.37339925765991
time_elpased: 1.431
batch start
#iterations: 117
currently lose_sum: 83.98988181352615
time_elpased: 1.415
batch start
#iterations: 118
currently lose_sum: 84.2032618522644
time_elpased: 1.425
batch start
#iterations: 119
currently lose_sum: 83.99203020334244
time_elpased: 1.414
start validation test
0.616288659794
0.625802879291
0.581617949774
0.602901952417
0.616345943082
68.368
batch start
#iterations: 120
currently lose_sum: 83.3527022600174
time_elpased: 1.43
batch start
#iterations: 121
currently lose_sum: 84.49077689647675
time_elpased: 1.429
batch start
#iterations: 122
currently lose_sum: 83.77961081266403
time_elpased: 1.434
batch start
#iterations: 123
currently lose_sum: 83.43436223268509
time_elpased: 1.393
batch start
#iterations: 124
currently lose_sum: 83.79192078113556
time_elpased: 1.405
batch start
#iterations: 125
currently lose_sum: 83.92009717226028
time_elpased: 1.412
batch start
#iterations: 126
currently lose_sum: 82.66638800501823
time_elpased: 1.413
batch start
#iterations: 127
currently lose_sum: 83.26204162836075
time_elpased: 1.437
batch start
#iterations: 128
currently lose_sum: 83.3846874833107
time_elpased: 1.457
batch start
#iterations: 129
currently lose_sum: 82.83070981502533
time_elpased: 1.431
batch start
#iterations: 130
currently lose_sum: 83.90150612592697
time_elpased: 1.424
batch start
#iterations: 131
currently lose_sum: 83.44684845209122
time_elpased: 1.434
batch start
#iterations: 132
currently lose_sum: 82.92392158508301
time_elpased: 1.412
batch start
#iterations: 133
currently lose_sum: 83.38435381650925
time_elpased: 1.442
batch start
#iterations: 134
currently lose_sum: 82.990198969841
time_elpased: 1.436
batch start
#iterations: 135
currently lose_sum: 83.20979392528534
time_elpased: 1.47
batch start
#iterations: 136
currently lose_sum: 83.09081488847733
time_elpased: 1.406
batch start
#iterations: 137
currently lose_sum: 82.67488604784012
time_elpased: 1.46
batch start
#iterations: 138
currently lose_sum: 82.82304146885872
time_elpased: 1.411
batch start
#iterations: 139
currently lose_sum: 82.1328154206276
time_elpased: 1.457
start validation test
0.624536082474
0.632635253054
0.596953478798
0.614276636306
0.624581654723
71.390
batch start
#iterations: 140
currently lose_sum: 82.532211124897
time_elpased: 1.427
batch start
#iterations: 141
currently lose_sum: 82.36193716526031
time_elpased: 1.481
batch start
#iterations: 142
currently lose_sum: 81.74734792113304
time_elpased: 1.427
batch start
#iterations: 143
currently lose_sum: 82.960125207901
time_elpased: 1.459
batch start
#iterations: 144
currently lose_sum: 81.82614549994469
time_elpased: 1.421
batch start
#iterations: 145
currently lose_sum: 81.90302038192749
time_elpased: 1.424
batch start
#iterations: 146
currently lose_sum: 82.56993973255157
time_elpased: 1.386
batch start
#iterations: 147
currently lose_sum: 82.3983501791954
time_elpased: 1.475
batch start
#iterations: 148
currently lose_sum: 82.47056165337563
time_elpased: 1.427
batch start
#iterations: 149
currently lose_sum: 81.88929030299187
time_elpased: 1.465
batch start
#iterations: 150
currently lose_sum: 82.36970940232277
time_elpased: 1.412
batch start
#iterations: 151
currently lose_sum: 82.58110186457634
time_elpased: 1.405
batch start
#iterations: 152
currently lose_sum: 81.90589690208435
time_elpased: 1.422
batch start
#iterations: 153
currently lose_sum: 82.07936424016953
time_elpased: 1.405
batch start
#iterations: 154
currently lose_sum: 82.20631319284439
time_elpased: 1.413
batch start
#iterations: 155
currently lose_sum: 82.01974356174469
time_elpased: 1.426
batch start
#iterations: 156
currently lose_sum: 81.35942298173904
time_elpased: 1.412
batch start
#iterations: 157
currently lose_sum: 81.0949276983738
time_elpased: 1.407
batch start
#iterations: 158
currently lose_sum: 81.87089809775352
time_elpased: 1.416
batch start
#iterations: 159
currently lose_sum: 82.05022722482681
time_elpased: 1.427
start validation test
0.608298969072
0.617467539674
0.572663647592
0.594222245955
0.608357846101
71.170
batch start
#iterations: 160
currently lose_sum: 81.81662213802338
time_elpased: 1.427
batch start
#iterations: 161
currently lose_sum: 81.4080428481102
time_elpased: 1.426
batch start
#iterations: 162
currently lose_sum: 81.65584498643875
time_elpased: 1.409
batch start
#iterations: 163
currently lose_sum: 81.6478796005249
time_elpased: 1.446
batch start
#iterations: 164
currently lose_sum: 81.06588798761368
time_elpased: 1.442
batch start
#iterations: 165
currently lose_sum: 80.95225241780281
time_elpased: 1.427
batch start
#iterations: 166
currently lose_sum: 81.40292984247208
time_elpased: 1.448
batch start
#iterations: 167
currently lose_sum: 81.11772403120995
time_elpased: 1.528
batch start
#iterations: 168
currently lose_sum: 81.22626772522926
time_elpased: 1.421
batch start
#iterations: 169
currently lose_sum: 81.24306014180183
time_elpased: 1.423
batch start
#iterations: 170
currently lose_sum: 80.92737671732903
time_elpased: 1.443
batch start
#iterations: 171
currently lose_sum: 80.86725595593452
time_elpased: 1.435
batch start
#iterations: 172
currently lose_sum: 80.9662082195282
time_elpased: 1.41
batch start
#iterations: 173
currently lose_sum: 80.91671469807625
time_elpased: 1.422
batch start
#iterations: 174
currently lose_sum: 80.58092719316483
time_elpased: 1.511
batch start
#iterations: 175
currently lose_sum: 80.27447566390038
time_elpased: 1.425
batch start
#iterations: 176
currently lose_sum: 80.86838001012802
time_elpased: 1.397
batch start
#iterations: 177
currently lose_sum: 80.46284061670303
time_elpased: 1.431
batch start
#iterations: 178
currently lose_sum: 80.45572313666344
time_elpased: 1.429
batch start
#iterations: 179
currently lose_sum: 80.64832359552383
time_elpased: 1.419
start validation test
0.613041237113
0.633700520518
0.538801976122
0.582410858319
0.61316389595
74.348
batch start
#iterations: 180
currently lose_sum: 80.31331941485405
time_elpased: 1.433
batch start
#iterations: 181
currently lose_sum: 81.04037848114967
time_elpased: 1.446
batch start
#iterations: 182
currently lose_sum: 80.96384072303772
time_elpased: 1.464
batch start
#iterations: 183
currently lose_sum: 80.28522551059723
time_elpased: 1.447
batch start
#iterations: 184
currently lose_sum: 80.69653263688087
time_elpased: 1.425
batch start
#iterations: 185
currently lose_sum: 80.05430403351784
time_elpased: 1.424
batch start
#iterations: 186
currently lose_sum: 79.91127955913544
time_elpased: 1.402
batch start
#iterations: 187
currently lose_sum: 80.07982882857323
time_elpased: 1.414
batch start
#iterations: 188
currently lose_sum: 80.52500969171524
time_elpased: 1.436
batch start
#iterations: 189
currently lose_sum: 80.37887379527092
time_elpased: 1.435
batch start
#iterations: 190
currently lose_sum: 80.0925881266594
time_elpased: 1.407
batch start
#iterations: 191
currently lose_sum: 79.90716022253036
time_elpased: 1.44
batch start
#iterations: 192
currently lose_sum: 80.4193764925003
time_elpased: 1.453
batch start
#iterations: 193
currently lose_sum: 79.82969152927399
time_elpased: 1.4
batch start
#iterations: 194
currently lose_sum: 80.08155798912048
time_elpased: 1.466
batch start
#iterations: 195
currently lose_sum: 80.61108464002609
time_elpased: 1.406
batch start
#iterations: 196
currently lose_sum: 79.98754754662514
time_elpased: 1.419
batch start
#iterations: 197
currently lose_sum: 79.9350596666336
time_elpased: 1.418
batch start
#iterations: 198
currently lose_sum: 80.0678323507309
time_elpased: 1.405
batch start
#iterations: 199
currently lose_sum: 79.47039839625359
time_elpased: 1.406
start validation test
0.608041237113
0.629221732746
0.529230135858
0.574910554562
0.60817144959
76.991
batch start
#iterations: 200
currently lose_sum: 79.13582280278206
time_elpased: 1.418
batch start
#iterations: 201
currently lose_sum: 79.72895842790604
time_elpased: 1.468
batch start
#iterations: 202
currently lose_sum: 80.08441159129143
time_elpased: 1.445
batch start
#iterations: 203
currently lose_sum: 79.18414056301117
time_elpased: 1.404
batch start
#iterations: 204
currently lose_sum: 79.3608708679676
time_elpased: 1.401
batch start
#iterations: 205
currently lose_sum: 79.71839961409569
time_elpased: 1.454
batch start
#iterations: 206
currently lose_sum: 79.49393233656883
time_elpased: 1.399
batch start
#iterations: 207
currently lose_sum: 79.59303414821625
time_elpased: 1.391
batch start
#iterations: 208
currently lose_sum: 79.04735872149467
time_elpased: 1.447
batch start
#iterations: 209
currently lose_sum: 79.35022160410881
time_elpased: 1.406
batch start
#iterations: 210
currently lose_sum: 78.98494324088097
time_elpased: 1.429
batch start
#iterations: 211
currently lose_sum: 78.50190430879593
time_elpased: 1.421
batch start
#iterations: 212
currently lose_sum: 79.15419355034828
time_elpased: 1.431
batch start
#iterations: 213
currently lose_sum: 79.3317214846611
time_elpased: 1.408
batch start
#iterations: 214
currently lose_sum: 78.87729123234749
time_elpased: 1.43
batch start
#iterations: 215
currently lose_sum: 78.5905313193798
time_elpased: 1.501
batch start
#iterations: 216
currently lose_sum: 78.86796125769615
time_elpased: 1.436
batch start
#iterations: 217
currently lose_sum: 79.5699297785759
time_elpased: 1.451
batch start
#iterations: 218
currently lose_sum: 78.71368980407715
time_elpased: 1.415
batch start
#iterations: 219
currently lose_sum: 78.53955101966858
time_elpased: 1.45
start validation test
0.611649484536
0.639656938044
0.514306298888
0.570173436787
0.611810315904
79.040
batch start
#iterations: 220
currently lose_sum: 78.63106387853622
time_elpased: 1.427
batch start
#iterations: 221
currently lose_sum: 78.79246914386749
time_elpased: 1.41
batch start
#iterations: 222
currently lose_sum: 78.87721863389015
time_elpased: 1.486
batch start
#iterations: 223
currently lose_sum: 78.08422720432281
time_elpased: 1.402
batch start
#iterations: 224
currently lose_sum: 78.99025642871857
time_elpased: 1.453
batch start
#iterations: 225
currently lose_sum: 79.09284150600433
time_elpased: 1.425
batch start
#iterations: 226
currently lose_sum: 77.92658296227455
time_elpased: 1.439
batch start
#iterations: 227
currently lose_sum: 78.19291743636131
time_elpased: 1.406
batch start
#iterations: 228
currently lose_sum: 78.78547731041908
time_elpased: 1.449
batch start
#iterations: 229
currently lose_sum: 77.40742713212967
time_elpased: 1.43
batch start
#iterations: 230
currently lose_sum: 78.30855736136436
time_elpased: 1.413
batch start
#iterations: 231
currently lose_sum: 78.02006760239601
time_elpased: 1.428
batch start
#iterations: 232
currently lose_sum: 78.71416234970093
time_elpased: 1.405
batch start
#iterations: 233
currently lose_sum: 78.69755166769028
time_elpased: 1.43
batch start
#iterations: 234
currently lose_sum: 78.66729271411896
time_elpased: 1.4
batch start
#iterations: 235
currently lose_sum: 78.46841359138489
time_elpased: 1.44
batch start
#iterations: 236
currently lose_sum: 78.2475388944149
time_elpased: 1.413
batch start
#iterations: 237
currently lose_sum: 77.86997154355049
time_elpased: 1.425
batch start
#iterations: 238
currently lose_sum: 78.01922476291656
time_elpased: 1.433
batch start
#iterations: 239
currently lose_sum: 78.70987468957901
time_elpased: 1.414
start validation test
0.603298969072
0.634918514561
0.489193083573
0.552610161609
0.603487495938
81.214
batch start
#iterations: 240
currently lose_sum: 77.6223766207695
time_elpased: 1.435
batch start
#iterations: 241
currently lose_sum: 77.45304062962532
time_elpased: 1.407
batch start
#iterations: 242
currently lose_sum: 77.90476757287979
time_elpased: 1.409
batch start
#iterations: 243
currently lose_sum: 78.02805772423744
time_elpased: 1.432
batch start
#iterations: 244
currently lose_sum: 78.21113061904907
time_elpased: 1.43
batch start
#iterations: 245
currently lose_sum: 78.18829664587975
time_elpased: 1.407
batch start
#iterations: 246
currently lose_sum: 77.97334948182106
time_elpased: 1.403
batch start
#iterations: 247
currently lose_sum: 77.53549826145172
time_elpased: 1.445
batch start
#iterations: 248
currently lose_sum: 78.01340809464455
time_elpased: 1.44
batch start
#iterations: 249
currently lose_sum: 77.62161204218864
time_elpased: 1.422
batch start
#iterations: 250
currently lose_sum: 77.84592118859291
time_elpased: 1.416
batch start
#iterations: 251
currently lose_sum: 76.98339313268661
time_elpased: 1.413
batch start
#iterations: 252
currently lose_sum: 77.59135606884956
time_elpased: 1.426
batch start
#iterations: 253
currently lose_sum: 77.67649218440056
time_elpased: 1.445
batch start
#iterations: 254
currently lose_sum: 77.51709851622581
time_elpased: 1.426
batch start
#iterations: 255
currently lose_sum: 77.07773903012276
time_elpased: 1.416
batch start
#iterations: 256
currently lose_sum: 77.72916892170906
time_elpased: 1.432
batch start
#iterations: 257
currently lose_sum: 77.3112396299839
time_elpased: 1.49
batch start
#iterations: 258
currently lose_sum: 77.63560125231743
time_elpased: 1.43
batch start
#iterations: 259
currently lose_sum: 77.21840310096741
time_elpased: 1.406
start validation test
0.596649484536
0.618172728409
0.509057225196
0.558333803691
0.596794205328
81.498
batch start
#iterations: 260
currently lose_sum: 77.65390974283218
time_elpased: 1.428
batch start
#iterations: 261
currently lose_sum: 77.27749490737915
time_elpased: 1.407
batch start
#iterations: 262
currently lose_sum: 76.42655736207962
time_elpased: 1.407
batch start
#iterations: 263
currently lose_sum: 75.8638068139553
time_elpased: 1.409
batch start
#iterations: 264
currently lose_sum: 77.07018673419952
time_elpased: 1.432
batch start
#iterations: 265
currently lose_sum: 76.69993329048157
time_elpased: 1.416
batch start
#iterations: 266
currently lose_sum: 76.90264624357224
time_elpased: 1.424
batch start
#iterations: 267
currently lose_sum: 76.75383687019348
time_elpased: 1.466
batch start
#iterations: 268
currently lose_sum: 77.59585359692574
time_elpased: 1.398
batch start
#iterations: 269
currently lose_sum: 77.24440735578537
time_elpased: 1.432
batch start
#iterations: 270
currently lose_sum: 75.72186610102654
time_elpased: 1.43
batch start
#iterations: 271
currently lose_sum: 77.26016780734062
time_elpased: 1.43
batch start
#iterations: 272
currently lose_sum: 76.51993063092232
time_elpased: 1.487
batch start
#iterations: 273
currently lose_sum: 76.90316191315651
time_elpased: 1.473
batch start
#iterations: 274
currently lose_sum: 77.25842094421387
time_elpased: 1.43
batch start
#iterations: 275
currently lose_sum: 76.44315427541733
time_elpased: 1.4
batch start
#iterations: 276
currently lose_sum: 75.69068679213524
time_elpased: 1.445
batch start
#iterations: 277
currently lose_sum: 75.91259187459946
time_elpased: 1.416
batch start
#iterations: 278
currently lose_sum: 76.3872701227665
time_elpased: 1.433
batch start
#iterations: 279
currently lose_sum: 76.12769544124603
time_elpased: 1.404
start validation test
0.596030927835
0.615346838551
0.515850144092
0.56122277588
0.596163403314
82.391
batch start
#iterations: 280
currently lose_sum: 76.22587412595749
time_elpased: 1.426
batch start
#iterations: 281
currently lose_sum: 75.79922968149185
time_elpased: 1.409
batch start
#iterations: 282
currently lose_sum: 76.29278436303139
time_elpased: 1.459
batch start
#iterations: 283
currently lose_sum: 76.25985726714134
time_elpased: 1.399
batch start
#iterations: 284
currently lose_sum: 76.22647023200989
time_elpased: 1.429
batch start
#iterations: 285
currently lose_sum: 76.1488593518734
time_elpased: 1.407
batch start
#iterations: 286
currently lose_sum: 75.72643500566483
time_elpased: 1.401
batch start
#iterations: 287
currently lose_sum: 76.46644580364227
time_elpased: 1.501
batch start
#iterations: 288
currently lose_sum: 76.07806971669197
time_elpased: 1.468
batch start
#iterations: 289
currently lose_sum: 77.05478763580322
time_elpased: 1.428
batch start
#iterations: 290
currently lose_sum: 76.04451182484627
time_elpased: 1.439
batch start
#iterations: 291
currently lose_sum: 76.20994663238525
time_elpased: 1.385
batch start
#iterations: 292
currently lose_sum: 75.64929556846619
time_elpased: 1.403
batch start
#iterations: 293
currently lose_sum: 76.48375803232193
time_elpased: 1.411
batch start
#iterations: 294
currently lose_sum: 75.40609282255173
time_elpased: 1.461
batch start
#iterations: 295
currently lose_sum: 75.20501419901848
time_elpased: 1.425
batch start
#iterations: 296
currently lose_sum: 75.92755988240242
time_elpased: 1.41
batch start
#iterations: 297
currently lose_sum: 75.98414450883865
time_elpased: 1.41
batch start
#iterations: 298
currently lose_sum: 75.47147843241692
time_elpased: 1.493
batch start
#iterations: 299
currently lose_sum: 75.77892255783081
time_elpased: 1.436
start validation test
0.604278350515
0.6295261085
0.509983532318
0.56348439188
0.604434145341
83.996
batch start
#iterations: 300
currently lose_sum: 75.05981892347336
time_elpased: 1.408
batch start
#iterations: 301
currently lose_sum: 76.4881646335125
time_elpased: 1.394
batch start
#iterations: 302
currently lose_sum: 75.04954755306244
time_elpased: 1.451
batch start
#iterations: 303
currently lose_sum: 75.03499031066895
time_elpased: 1.419
batch start
#iterations: 304
currently lose_sum: 75.63231191039085
time_elpased: 1.44
batch start
#iterations: 305
currently lose_sum: 75.74679154157639
time_elpased: 1.44
batch start
#iterations: 306
currently lose_sum: 75.79120633006096
time_elpased: 1.412
batch start
#iterations: 307
currently lose_sum: 75.82326239347458
time_elpased: 1.41
batch start
#iterations: 308
currently lose_sum: 75.40152332186699
time_elpased: 1.415
batch start
#iterations: 309
currently lose_sum: 75.06655195355415
time_elpased: 1.431
batch start
#iterations: 310
currently lose_sum: 74.87359744310379
time_elpased: 1.413
batch start
#iterations: 311
currently lose_sum: 75.24939653277397
time_elpased: 1.423
batch start
#iterations: 312
currently lose_sum: 75.21677166223526
time_elpased: 1.43
batch start
#iterations: 313
currently lose_sum: 75.37875229120255
time_elpased: 1.395
batch start
#iterations: 314
currently lose_sum: 74.92904460430145
time_elpased: 1.403
batch start
#iterations: 315
currently lose_sum: 74.59525272250175
time_elpased: 1.427
batch start
#iterations: 316
currently lose_sum: 74.8514384329319
time_elpased: 1.464
batch start
#iterations: 317
currently lose_sum: 74.19903925061226
time_elpased: 1.44
batch start
#iterations: 318
currently lose_sum: 75.32428368926048
time_elpased: 1.431
batch start
#iterations: 319
currently lose_sum: 75.19191074371338
time_elpased: 1.429
start validation test
0.604278350515
0.633250555483
0.498662000823
0.557954741752
0.604452850887
86.435
batch start
#iterations: 320
currently lose_sum: 75.03575003147125
time_elpased: 1.417
batch start
#iterations: 321
currently lose_sum: 75.58930206298828
time_elpased: 1.43
batch start
#iterations: 322
currently lose_sum: 74.27399253845215
time_elpased: 1.431
batch start
#iterations: 323
currently lose_sum: 75.00661611557007
time_elpased: 1.427
batch start
#iterations: 324
currently lose_sum: 74.97222712635994
time_elpased: 1.411
batch start
#iterations: 325
currently lose_sum: 74.71694549918175
time_elpased: 1.424
batch start
#iterations: 326
currently lose_sum: 74.51651966571808
time_elpased: 1.401
batch start
#iterations: 327
currently lose_sum: 74.97549283504486
time_elpased: 1.455
batch start
#iterations: 328
currently lose_sum: 74.24312841892242
time_elpased: 1.425
batch start
#iterations: 329
currently lose_sum: 75.2982567846775
time_elpased: 1.434
batch start
#iterations: 330
currently lose_sum: 74.7592704296112
time_elpased: 1.443
batch start
#iterations: 331
currently lose_sum: 74.34491989016533
time_elpased: 1.431
batch start
#iterations: 332
currently lose_sum: 74.7992559671402
time_elpased: 1.411
batch start
#iterations: 333
currently lose_sum: 74.37399592995644
time_elpased: 1.403
batch start
#iterations: 334
currently lose_sum: 74.86596584320068
time_elpased: 1.432
batch start
#iterations: 335
currently lose_sum: 74.40935680270195
time_elpased: 1.488
batch start
#iterations: 336
currently lose_sum: 74.05909135937691
time_elpased: 1.405
batch start
#iterations: 337
currently lose_sum: 74.02371716499329
time_elpased: 1.447
batch start
#iterations: 338
currently lose_sum: 73.79075720906258
time_elpased: 1.419
batch start
#iterations: 339
currently lose_sum: 74.27406325936317
time_elpased: 1.429
start validation test
0.597525773196
0.614470842333
0.527068752573
0.567423822715
0.597642182978
86.285
batch start
#iterations: 340
currently lose_sum: 75.02805680036545
time_elpased: 1.421
batch start
#iterations: 341
currently lose_sum: 73.95690819621086
time_elpased: 1.437
batch start
#iterations: 342
currently lose_sum: 74.59476917982101
time_elpased: 1.406
batch start
#iterations: 343
currently lose_sum: 74.31402733922005
time_elpased: 1.448
batch start
#iterations: 344
currently lose_sum: 74.34051072597504
time_elpased: 1.404
batch start
#iterations: 345
currently lose_sum: 73.76168811321259
time_elpased: 1.424
batch start
#iterations: 346
currently lose_sum: 73.88981127738953
time_elpased: 1.431
batch start
#iterations: 347
currently lose_sum: 73.65732938051224
time_elpased: 1.405
batch start
#iterations: 348
currently lose_sum: 73.782869130373
time_elpased: 1.419
batch start
#iterations: 349
currently lose_sum: 73.48278364539146
time_elpased: 1.437
batch start
#iterations: 350
currently lose_sum: 73.19934704899788
time_elpased: 1.448
batch start
#iterations: 351
currently lose_sum: 73.70252564549446
time_elpased: 1.423
batch start
#iterations: 352
currently lose_sum: 73.47111225128174
time_elpased: 1.396
batch start
#iterations: 353
currently lose_sum: 73.66104623675346
time_elpased: 1.402
batch start
#iterations: 354
currently lose_sum: 73.87115597724915
time_elpased: 1.429
batch start
#iterations: 355
currently lose_sum: 73.42134296894073
time_elpased: 1.446
batch start
#iterations: 356
currently lose_sum: 73.19584918022156
time_elpased: 1.447
batch start
#iterations: 357
currently lose_sum: 73.27505153417587
time_elpased: 1.406
batch start
#iterations: 358
currently lose_sum: 73.85982236266136
time_elpased: 1.415
batch start
#iterations: 359
currently lose_sum: 73.54426297545433
time_elpased: 1.488
start validation test
0.599226804124
0.634139599171
0.472210786332
0.541324995575
0.599436661237
90.387
batch start
#iterations: 360
currently lose_sum: 74.1488636136055
time_elpased: 1.423
batch start
#iterations: 361
currently lose_sum: 73.248021453619
time_elpased: 1.423
batch start
#iterations: 362
currently lose_sum: 73.6134705543518
time_elpased: 1.419
batch start
#iterations: 363
currently lose_sum: 73.72226792573929
time_elpased: 1.403
batch start
#iterations: 364
currently lose_sum: 73.44255629181862
time_elpased: 1.403
batch start
#iterations: 365
currently lose_sum: 73.86359828710556
time_elpased: 1.431
batch start
#iterations: 366
currently lose_sum: 73.40162369608879
time_elpased: 1.386
batch start
#iterations: 367
currently lose_sum: 73.57356756925583
time_elpased: 1.439
batch start
#iterations: 368
currently lose_sum: 72.94690719246864
time_elpased: 1.43
batch start
#iterations: 369
currently lose_sum: 73.7050693333149
time_elpased: 1.446
batch start
#iterations: 370
currently lose_sum: 72.83909818530083
time_elpased: 1.437
batch start
#iterations: 371
currently lose_sum: 73.13472473621368
time_elpased: 1.44
batch start
#iterations: 372
currently lose_sum: 73.99020281434059
time_elpased: 1.465
batch start
#iterations: 373
currently lose_sum: 73.51591470837593
time_elpased: 1.436
batch start
#iterations: 374
currently lose_sum: 73.80835843086243
time_elpased: 1.397
batch start
#iterations: 375
currently lose_sum: 72.52356547117233
time_elpased: 1.417
batch start
#iterations: 376
currently lose_sum: 72.97948119044304
time_elpased: 1.426
batch start
#iterations: 377
currently lose_sum: 73.12526258826256
time_elpased: 1.474
batch start
#iterations: 378
currently lose_sum: 73.67988231778145
time_elpased: 1.47
batch start
#iterations: 379
currently lose_sum: 73.26551204919815
time_elpased: 1.442
start validation test
0.595360824742
0.624003189793
0.483223548786
0.544663573086
0.595546099052
90.494
batch start
#iterations: 380
currently lose_sum: 72.90035203099251
time_elpased: 1.458
batch start
#iterations: 381
currently lose_sum: 72.7203117609024
time_elpased: 1.398
batch start
#iterations: 382
currently lose_sum: 73.1506036221981
time_elpased: 1.431
batch start
#iterations: 383
currently lose_sum: 72.24932792782784
time_elpased: 1.459
batch start
#iterations: 384
currently lose_sum: 72.36899092793465
time_elpased: 1.426
batch start
#iterations: 385
currently lose_sum: 72.4646712243557
time_elpased: 1.427
batch start
#iterations: 386
currently lose_sum: 72.11995482444763
time_elpased: 1.439
batch start
#iterations: 387
currently lose_sum: 72.33469569683075
time_elpased: 1.411
batch start
#iterations: 388
currently lose_sum: 72.90594130754471
time_elpased: 1.482
batch start
#iterations: 389
currently lose_sum: 72.48017212748528
time_elpased: 1.414
batch start
#iterations: 390
currently lose_sum: 71.69929164648056
time_elpased: 1.419
batch start
#iterations: 391
currently lose_sum: 72.30125385522842
time_elpased: 1.408
batch start
#iterations: 392
currently lose_sum: 72.89331966638565
time_elpased: 1.455
batch start
#iterations: 393
currently lose_sum: 71.93065729737282
time_elpased: 1.399
batch start
#iterations: 394
currently lose_sum: 72.16262605786324
time_elpased: 1.401
batch start
#iterations: 395
currently lose_sum: 72.5001871585846
time_elpased: 1.418
batch start
#iterations: 396
currently lose_sum: 72.28047439455986
time_elpased: 1.441
batch start
#iterations: 397
currently lose_sum: 71.96613377332687
time_elpased: 1.424
batch start
#iterations: 398
currently lose_sum: 72.46572250127792
time_elpased: 1.436
batch start
#iterations: 399
currently lose_sum: 72.22364810109138
time_elpased: 1.487
start validation test
0.589226804124
0.614168082604
0.48363524084
0.54114124489
0.589401263543
92.911
acc: 0.639
pre: 0.623
rec: 0.708
F1: 0.663
auc: 0.682
