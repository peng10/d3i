start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.2115530371666
time_elpased: 1.452
batch start
#iterations: 1
currently lose_sum: 99.54187178611755
time_elpased: 1.447
batch start
#iterations: 2
currently lose_sum: 99.31677401065826
time_elpased: 1.478
batch start
#iterations: 3
currently lose_sum: 99.13338494300842
time_elpased: 1.408
batch start
#iterations: 4
currently lose_sum: 98.54359203577042
time_elpased: 1.466
batch start
#iterations: 5
currently lose_sum: 98.2131660580635
time_elpased: 1.455
batch start
#iterations: 6
currently lose_sum: 97.81919091939926
time_elpased: 1.432
batch start
#iterations: 7
currently lose_sum: 97.78144907951355
time_elpased: 1.43
batch start
#iterations: 8
currently lose_sum: 97.63255077600479
time_elpased: 1.434
batch start
#iterations: 9
currently lose_sum: 97.26037400960922
time_elpased: 1.419
batch start
#iterations: 10
currently lose_sum: 97.23350501060486
time_elpased: 1.442
batch start
#iterations: 11
currently lose_sum: 96.63460892438889
time_elpased: 1.43
batch start
#iterations: 12
currently lose_sum: 96.72348368167877
time_elpased: 1.429
batch start
#iterations: 13
currently lose_sum: 96.53414559364319
time_elpased: 1.509
batch start
#iterations: 14
currently lose_sum: 96.1357753276825
time_elpased: 1.421
batch start
#iterations: 15
currently lose_sum: 96.21982145309448
time_elpased: 1.427
batch start
#iterations: 16
currently lose_sum: 96.17790949344635
time_elpased: 1.426
batch start
#iterations: 17
currently lose_sum: 95.88073378801346
time_elpased: 1.482
batch start
#iterations: 18
currently lose_sum: 95.37913662195206
time_elpased: 1.452
batch start
#iterations: 19
currently lose_sum: 95.26572209596634
time_elpased: 1.479
start validation test
0.624329896907
0.642363146173
0.56396006998
0.600613765892
0.62443588545
62.804
batch start
#iterations: 20
currently lose_sum: 95.49912422895432
time_elpased: 1.432
batch start
#iterations: 21
currently lose_sum: 94.85306131839752
time_elpased: 1.449
batch start
#iterations: 22
currently lose_sum: 94.96989715099335
time_elpased: 1.427
batch start
#iterations: 23
currently lose_sum: 95.06209284067154
time_elpased: 1.438
batch start
#iterations: 24
currently lose_sum: 94.4845489859581
time_elpased: 1.431
batch start
#iterations: 25
currently lose_sum: 94.69657415151596
time_elpased: 1.438
batch start
#iterations: 26
currently lose_sum: 94.36436581611633
time_elpased: 1.428
batch start
#iterations: 27
currently lose_sum: 94.00243937969208
time_elpased: 1.425
batch start
#iterations: 28
currently lose_sum: 93.55053853988647
time_elpased: 1.456
batch start
#iterations: 29
currently lose_sum: 94.11243325471878
time_elpased: 1.465
batch start
#iterations: 30
currently lose_sum: 93.62732481956482
time_elpased: 1.445
batch start
#iterations: 31
currently lose_sum: 93.03681349754333
time_elpased: 1.425
batch start
#iterations: 32
currently lose_sum: 93.08503711223602
time_elpased: 1.439
batch start
#iterations: 33
currently lose_sum: 93.21594351530075
time_elpased: 1.445
batch start
#iterations: 34
currently lose_sum: 93.02170777320862
time_elpased: 1.454
batch start
#iterations: 35
currently lose_sum: 92.77029377222061
time_elpased: 1.45
batch start
#iterations: 36
currently lose_sum: 92.57163870334625
time_elpased: 1.423
batch start
#iterations: 37
currently lose_sum: 92.1674553155899
time_elpased: 1.426
batch start
#iterations: 38
currently lose_sum: 91.81253343820572
time_elpased: 1.461
batch start
#iterations: 39
currently lose_sum: 91.92837518453598
time_elpased: 1.488
start validation test
0.612783505155
0.649289099526
0.493465061233
0.560753128289
0.612992987087
63.111
batch start
#iterations: 40
currently lose_sum: 91.79194056987762
time_elpased: 1.431
batch start
#iterations: 41
currently lose_sum: 91.50967615842819
time_elpased: 1.47
batch start
#iterations: 42
currently lose_sum: 91.43348723649979
time_elpased: 1.415
batch start
#iterations: 43
currently lose_sum: 90.9762567281723
time_elpased: 1.449
batch start
#iterations: 44
currently lose_sum: 90.92698353528976
time_elpased: 1.466
batch start
#iterations: 45
currently lose_sum: 90.74168920516968
time_elpased: 1.439
batch start
#iterations: 46
currently lose_sum: 90.98463702201843
time_elpased: 1.416
batch start
#iterations: 47
currently lose_sum: 90.58897668123245
time_elpased: 1.469
batch start
#iterations: 48
currently lose_sum: 91.34186714887619
time_elpased: 1.418
batch start
#iterations: 49
currently lose_sum: 90.53899091482162
time_elpased: 1.448
batch start
#iterations: 50
currently lose_sum: 90.81536155939102
time_elpased: 1.436
batch start
#iterations: 51
currently lose_sum: 89.80893385410309
time_elpased: 1.431
batch start
#iterations: 52
currently lose_sum: 89.94894725084305
time_elpased: 1.44
batch start
#iterations: 53
currently lose_sum: 90.34159463644028
time_elpased: 1.433
batch start
#iterations: 54
currently lose_sum: 89.91403543949127
time_elpased: 1.429
batch start
#iterations: 55
currently lose_sum: 89.1019715666771
time_elpased: 1.466
batch start
#iterations: 56
currently lose_sum: 89.57068967819214
time_elpased: 1.455
batch start
#iterations: 57
currently lose_sum: 89.18273037672043
time_elpased: 1.431
batch start
#iterations: 58
currently lose_sum: 88.97183066606522
time_elpased: 1.456
batch start
#iterations: 59
currently lose_sum: 88.94052630662918
time_elpased: 1.449
start validation test
0.612731958763
0.639564336373
0.519707728723
0.573440072674
0.612895277147
63.350
batch start
#iterations: 60
currently lose_sum: 88.57183909416199
time_elpased: 1.436
batch start
#iterations: 61
currently lose_sum: 88.91348832845688
time_elpased: 1.466
batch start
#iterations: 62
currently lose_sum: 88.3286719918251
time_elpased: 1.464
batch start
#iterations: 63
currently lose_sum: 88.70421034097672
time_elpased: 1.442
batch start
#iterations: 64
currently lose_sum: 88.13469296693802
time_elpased: 1.429
batch start
#iterations: 65
currently lose_sum: 87.73689818382263
time_elpased: 1.472
batch start
#iterations: 66
currently lose_sum: 88.2043993473053
time_elpased: 1.442
batch start
#iterations: 67
currently lose_sum: 87.86395913362503
time_elpased: 1.428
batch start
#iterations: 68
currently lose_sum: 88.28232568502426
time_elpased: 1.445
batch start
#iterations: 69
currently lose_sum: 87.53819280862808
time_elpased: 1.441
batch start
#iterations: 70
currently lose_sum: 87.30068331956863
time_elpased: 1.424
batch start
#iterations: 71
currently lose_sum: 87.36497247219086
time_elpased: 1.458
batch start
#iterations: 72
currently lose_sum: 87.42755085229874
time_elpased: 1.444
batch start
#iterations: 73
currently lose_sum: 86.93256366252899
time_elpased: 1.436
batch start
#iterations: 74
currently lose_sum: 87.22046774625778
time_elpased: 1.436
batch start
#iterations: 75
currently lose_sum: 86.57075190544128
time_elpased: 1.468
batch start
#iterations: 76
currently lose_sum: 86.7664155960083
time_elpased: 1.431
batch start
#iterations: 77
currently lose_sum: 86.82205808162689
time_elpased: 1.445
batch start
#iterations: 78
currently lose_sum: 87.21702963113785
time_elpased: 1.442
batch start
#iterations: 79
currently lose_sum: 86.79437375068665
time_elpased: 1.473
start validation test
0.608608247423
0.635944700461
0.511268910157
0.566832106794
0.608779141643
64.383
batch start
#iterations: 80
currently lose_sum: 86.07095718383789
time_elpased: 1.437
batch start
#iterations: 81
currently lose_sum: 86.31195420026779
time_elpased: 1.441
batch start
#iterations: 82
currently lose_sum: 85.95251089334488
time_elpased: 1.466
batch start
#iterations: 83
currently lose_sum: 85.91826647520065
time_elpased: 1.448
batch start
#iterations: 84
currently lose_sum: 85.6788958311081
time_elpased: 1.441
batch start
#iterations: 85
currently lose_sum: 85.77322798967361
time_elpased: 1.442
batch start
#iterations: 86
currently lose_sum: 86.06677001714706
time_elpased: 1.422
batch start
#iterations: 87
currently lose_sum: 85.6971418261528
time_elpased: 1.461
batch start
#iterations: 88
currently lose_sum: 85.47052365541458
time_elpased: 1.438
batch start
#iterations: 89
currently lose_sum: 84.48904722929001
time_elpased: 1.465
batch start
#iterations: 90
currently lose_sum: 85.03053796291351
time_elpased: 1.474
batch start
#iterations: 91
currently lose_sum: 85.03648561239243
time_elpased: 1.452
batch start
#iterations: 92
currently lose_sum: 85.25463908910751
time_elpased: 1.444
batch start
#iterations: 93
currently lose_sum: 84.67696130275726
time_elpased: 1.43
batch start
#iterations: 94
currently lose_sum: 84.99524253606796
time_elpased: 1.475
batch start
#iterations: 95
currently lose_sum: 84.65214574337006
time_elpased: 1.485
batch start
#iterations: 96
currently lose_sum: 84.31988495588303
time_elpased: 1.428
batch start
#iterations: 97
currently lose_sum: 84.22914999723434
time_elpased: 1.452
batch start
#iterations: 98
currently lose_sum: 83.90447151660919
time_elpased: 1.439
batch start
#iterations: 99
currently lose_sum: 84.01737993955612
time_elpased: 1.446
start validation test
0.598865979381
0.636402086564
0.464546670783
0.537061273052
0.599101797645
65.154
batch start
#iterations: 100
currently lose_sum: 83.88933950662613
time_elpased: 1.42
batch start
#iterations: 101
currently lose_sum: 83.97841995954514
time_elpased: 1.435
batch start
#iterations: 102
currently lose_sum: 83.98008251190186
time_elpased: 1.454
batch start
#iterations: 103
currently lose_sum: 82.9789891242981
time_elpased: 1.429
batch start
#iterations: 104
currently lose_sum: 84.21482878923416
time_elpased: 1.42
batch start
#iterations: 105
currently lose_sum: 83.72492265701294
time_elpased: 1.464
batch start
#iterations: 106
currently lose_sum: 83.69430002570152
time_elpased: 1.42
batch start
#iterations: 107
currently lose_sum: 83.63352394104004
time_elpased: 1.408
batch start
#iterations: 108
currently lose_sum: 83.51430779695511
time_elpased: 1.427
batch start
#iterations: 109
currently lose_sum: 83.69867026805878
time_elpased: 1.468
batch start
#iterations: 110
currently lose_sum: 83.08549708127975
time_elpased: 1.486
batch start
#iterations: 111
currently lose_sum: 83.30728322267532
time_elpased: 1.437
batch start
#iterations: 112
currently lose_sum: 82.7832522392273
time_elpased: 1.428
batch start
#iterations: 113
currently lose_sum: 82.77087205648422
time_elpased: 1.427
batch start
#iterations: 114
currently lose_sum: 82.86510655283928
time_elpased: 1.432
batch start
#iterations: 115
currently lose_sum: 82.49433445930481
time_elpased: 1.432
batch start
#iterations: 116
currently lose_sum: 82.49817699193954
time_elpased: 1.441
batch start
#iterations: 117
currently lose_sum: 82.77953046560287
time_elpased: 1.462
batch start
#iterations: 118
currently lose_sum: 82.52610206604004
time_elpased: 1.457
batch start
#iterations: 119
currently lose_sum: 81.87130013108253
time_elpased: 1.446
start validation test
0.597010309278
0.635083226633
0.459401049707
0.533142242924
0.597251903558
66.306
batch start
#iterations: 120
currently lose_sum: 82.6112089753151
time_elpased: 1.459
batch start
#iterations: 121
currently lose_sum: 82.13654232025146
time_elpased: 1.452
batch start
#iterations: 122
currently lose_sum: 82.00150972604752
time_elpased: 1.501
batch start
#iterations: 123
currently lose_sum: 82.24603843688965
time_elpased: 1.44
batch start
#iterations: 124
currently lose_sum: 81.7738467156887
time_elpased: 1.421
batch start
#iterations: 125
currently lose_sum: 82.34858793020248
time_elpased: 1.468
batch start
#iterations: 126
currently lose_sum: 81.77194875478745
time_elpased: 1.43
batch start
#iterations: 127
currently lose_sum: 81.78220853209496
time_elpased: 1.465
batch start
#iterations: 128
currently lose_sum: 81.6753785610199
time_elpased: 1.429
batch start
#iterations: 129
currently lose_sum: 81.53596067428589
time_elpased: 1.474
batch start
#iterations: 130
currently lose_sum: 81.32659795880318
time_elpased: 1.504
batch start
#iterations: 131
currently lose_sum: 81.07244861125946
time_elpased: 1.461
batch start
#iterations: 132
currently lose_sum: 81.31220400333405
time_elpased: 1.434
batch start
#iterations: 133
currently lose_sum: 81.01862040162086
time_elpased: 1.468
batch start
#iterations: 134
currently lose_sum: 81.15684312582016
time_elpased: 1.413
batch start
#iterations: 135
currently lose_sum: 80.68702763319016
time_elpased: 1.441
batch start
#iterations: 136
currently lose_sum: 80.89469799399376
time_elpased: 1.476
batch start
#iterations: 137
currently lose_sum: 81.11503082513809
time_elpased: 1.505
batch start
#iterations: 138
currently lose_sum: 80.92946195602417
time_elpased: 1.435
batch start
#iterations: 139
currently lose_sum: 80.22374531626701
time_elpased: 1.426
start validation test
0.587989690722
0.630250831067
0.429247710199
0.51068258341
0.588268386753
67.830
batch start
#iterations: 140
currently lose_sum: 80.52725559473038
time_elpased: 1.468
batch start
#iterations: 141
currently lose_sum: 80.69599729776382
time_elpased: 1.447
batch start
#iterations: 142
currently lose_sum: 80.48931822180748
time_elpased: 1.436
batch start
#iterations: 143
currently lose_sum: 80.14098313450813
time_elpased: 1.436
batch start
#iterations: 144
currently lose_sum: 80.92130455374718
time_elpased: 1.443
batch start
#iterations: 145
currently lose_sum: 80.64885613322258
time_elpased: 1.475
batch start
#iterations: 146
currently lose_sum: 79.62511011958122
time_elpased: 1.465
batch start
#iterations: 147
currently lose_sum: 80.65228095650673
time_elpased: 1.426
batch start
#iterations: 148
currently lose_sum: 79.83754509687424
time_elpased: 1.435
batch start
#iterations: 149
currently lose_sum: 79.76029473543167
time_elpased: 1.43
batch start
#iterations: 150
currently lose_sum: 79.86725720763206
time_elpased: 1.468
batch start
#iterations: 151
currently lose_sum: 79.73070701956749
time_elpased: 1.456
batch start
#iterations: 152
currently lose_sum: 80.10762494802475
time_elpased: 1.464
batch start
#iterations: 153
currently lose_sum: 79.5834448337555
time_elpased: 1.426
batch start
#iterations: 154
currently lose_sum: 79.84099185466766
time_elpased: 1.448
batch start
#iterations: 155
currently lose_sum: 80.07486405968666
time_elpased: 1.481
batch start
#iterations: 156
currently lose_sum: 79.78975734114647
time_elpased: 1.418
batch start
#iterations: 157
currently lose_sum: 79.66946110129356
time_elpased: 1.43
batch start
#iterations: 158
currently lose_sum: 80.42123651504517
time_elpased: 1.468
batch start
#iterations: 159
currently lose_sum: 78.98430076241493
time_elpased: 1.513
start validation test
0.588298969072
0.629529799341
0.432643820109
0.512839280268
0.588572245694
68.501
batch start
#iterations: 160
currently lose_sum: 79.2569854259491
time_elpased: 1.455
batch start
#iterations: 161
currently lose_sum: 78.86022689938545
time_elpased: 1.436
batch start
#iterations: 162
currently lose_sum: 79.05750432610512
time_elpased: 1.472
batch start
#iterations: 163
currently lose_sum: 78.67367205023766
time_elpased: 1.479
batch start
#iterations: 164
currently lose_sum: 79.30217051506042
time_elpased: 1.459
batch start
#iterations: 165
currently lose_sum: 79.3002542257309
time_elpased: 1.473
batch start
#iterations: 166
currently lose_sum: 79.13417926430702
time_elpased: 1.436
batch start
#iterations: 167
currently lose_sum: 78.45586404204369
time_elpased: 1.456
batch start
#iterations: 168
currently lose_sum: 78.78594243526459
time_elpased: 1.45
batch start
#iterations: 169
currently lose_sum: 78.25921466946602
time_elpased: 1.474
batch start
#iterations: 170
currently lose_sum: 78.72085881233215
time_elpased: 1.42
batch start
#iterations: 171
currently lose_sum: 78.24005943536758
time_elpased: 1.464
batch start
#iterations: 172
currently lose_sum: 78.39048162102699
time_elpased: 1.451
batch start
#iterations: 173
currently lose_sum: 78.18937212228775
time_elpased: 1.435
batch start
#iterations: 174
currently lose_sum: 78.09819734096527
time_elpased: 1.431
batch start
#iterations: 175
currently lose_sum: 78.14013656973839
time_elpased: 1.429
batch start
#iterations: 176
currently lose_sum: 78.37585499882698
time_elpased: 1.412
batch start
#iterations: 177
currently lose_sum: 77.64234319329262
time_elpased: 1.444
batch start
#iterations: 178
currently lose_sum: 78.20864328742027
time_elpased: 1.434
batch start
#iterations: 179
currently lose_sum: 78.0032690167427
time_elpased: 1.433
start validation test
0.586494845361
0.625723186471
0.43408459401
0.512577469923
0.586762425065
69.300
batch start
#iterations: 180
currently lose_sum: 77.66103595495224
time_elpased: 1.454
batch start
#iterations: 181
currently lose_sum: 77.5326626598835
time_elpased: 1.449
batch start
#iterations: 182
currently lose_sum: 78.09618425369263
time_elpased: 1.471
batch start
#iterations: 183
currently lose_sum: 78.10303956270218
time_elpased: 1.451
batch start
#iterations: 184
currently lose_sum: 78.38928520679474
time_elpased: 1.443
batch start
#iterations: 185
currently lose_sum: 77.38792851567268
time_elpased: 1.455
batch start
#iterations: 186
currently lose_sum: 77.63086387515068
time_elpased: 1.417
batch start
#iterations: 187
currently lose_sum: 78.0488443672657
time_elpased: 1.42
batch start
#iterations: 188
currently lose_sum: 77.49754527211189
time_elpased: 1.445
batch start
#iterations: 189
currently lose_sum: 76.61346560716629
time_elpased: 1.451
batch start
#iterations: 190
currently lose_sum: 77.73767212033272
time_elpased: 1.439
batch start
#iterations: 191
currently lose_sum: 77.26107692718506
time_elpased: 1.428
batch start
#iterations: 192
currently lose_sum: 76.96759730577469
time_elpased: 1.456
batch start
#iterations: 193
currently lose_sum: 77.78355196118355
time_elpased: 1.446
batch start
#iterations: 194
currently lose_sum: 77.71965783834457
time_elpased: 1.435
batch start
#iterations: 195
currently lose_sum: 77.08559447526932
time_elpased: 1.451
batch start
#iterations: 196
currently lose_sum: 76.5389313697815
time_elpased: 1.522
batch start
#iterations: 197
currently lose_sum: 76.61968383193016
time_elpased: 1.438
batch start
#iterations: 198
currently lose_sum: 76.04347348213196
time_elpased: 1.514
batch start
#iterations: 199
currently lose_sum: 76.65495184063911
time_elpased: 1.435
start validation test
0.583711340206
0.634266077565
0.398888545847
0.489764973465
0.584035825129
71.347
batch start
#iterations: 200
currently lose_sum: 76.78891250491142
time_elpased: 1.465
batch start
#iterations: 201
currently lose_sum: 76.79357907176018
time_elpased: 1.446
batch start
#iterations: 202
currently lose_sum: 76.8288007080555
time_elpased: 1.447
batch start
#iterations: 203
currently lose_sum: 75.81758552789688
time_elpased: 1.446
batch start
#iterations: 204
currently lose_sum: 76.21335646510124
time_elpased: 1.443
batch start
#iterations: 205
currently lose_sum: 76.72870826721191
time_elpased: 1.444
batch start
#iterations: 206
currently lose_sum: 76.59111687541008
time_elpased: 1.499
batch start
#iterations: 207
currently lose_sum: 76.12063056230545
time_elpased: 1.428
batch start
#iterations: 208
currently lose_sum: 75.98533597588539
time_elpased: 1.434
batch start
#iterations: 209
currently lose_sum: 75.99949193000793
time_elpased: 1.435
batch start
#iterations: 210
currently lose_sum: 75.76236507296562
time_elpased: 1.433
batch start
#iterations: 211
currently lose_sum: 75.77805459499359
time_elpased: 1.478
batch start
#iterations: 212
currently lose_sum: 75.99006375670433
time_elpased: 1.464
batch start
#iterations: 213
currently lose_sum: 76.4697730243206
time_elpased: 1.434
batch start
#iterations: 214
currently lose_sum: 75.46593937277794
time_elpased: 1.44
batch start
#iterations: 215
currently lose_sum: 75.80809673666954
time_elpased: 1.478
batch start
#iterations: 216
currently lose_sum: 75.65154352784157
time_elpased: 1.479
batch start
#iterations: 217
currently lose_sum: 75.74067276716232
time_elpased: 1.427
batch start
#iterations: 218
currently lose_sum: 75.50125113129616
time_elpased: 1.485
batch start
#iterations: 219
currently lose_sum: 75.63575628399849
time_elpased: 1.453
start validation test
0.585309278351
0.628220858896
0.421529278584
0.504526698282
0.585596819401
71.146
batch start
#iterations: 220
currently lose_sum: 75.59403455257416
time_elpased: 1.511
batch start
#iterations: 221
currently lose_sum: 75.51709458231926
time_elpased: 1.467
batch start
#iterations: 222
currently lose_sum: 75.3231270313263
time_elpased: 1.542
batch start
#iterations: 223
currently lose_sum: 75.90970784425735
time_elpased: 1.43
batch start
#iterations: 224
currently lose_sum: 75.67456325888634
time_elpased: 1.434
batch start
#iterations: 225
currently lose_sum: 75.01583305001259
time_elpased: 1.435
batch start
#iterations: 226
currently lose_sum: 75.2842757999897
time_elpased: 1.432
batch start
#iterations: 227
currently lose_sum: 75.63538852334023
time_elpased: 1.467
batch start
#iterations: 228
currently lose_sum: 75.89627328515053
time_elpased: 1.438
batch start
#iterations: 229
currently lose_sum: 75.31509131193161
time_elpased: 1.445
batch start
#iterations: 230
currently lose_sum: 75.21110934019089
time_elpased: 1.462
batch start
#iterations: 231
currently lose_sum: 75.23626402020454
time_elpased: 1.426
batch start
#iterations: 232
currently lose_sum: 75.18554154038429
time_elpased: 1.431
batch start
#iterations: 233
currently lose_sum: 74.95594647526741
time_elpased: 1.459
batch start
#iterations: 234
currently lose_sum: 74.81895396113396
time_elpased: 1.43
batch start
#iterations: 235
currently lose_sum: 74.42434895038605
time_elpased: 1.452
batch start
#iterations: 236
currently lose_sum: 75.49931725859642
time_elpased: 1.464
batch start
#iterations: 237
currently lose_sum: 74.91377031803131
time_elpased: 1.425
batch start
#iterations: 238
currently lose_sum: 74.74504390358925
time_elpased: 1.479
batch start
#iterations: 239
currently lose_sum: 74.01680892705917
time_elpased: 1.424
start validation test
0.581649484536
0.6265212581
0.407944838942
0.494141111942
0.581954449834
72.679
batch start
#iterations: 240
currently lose_sum: 74.88963496685028
time_elpased: 1.437
batch start
#iterations: 241
currently lose_sum: 74.64613100886345
time_elpased: 1.473
batch start
#iterations: 242
currently lose_sum: 74.23476791381836
time_elpased: 1.446
batch start
#iterations: 243
currently lose_sum: 74.9042256474495
time_elpased: 1.465
batch start
#iterations: 244
currently lose_sum: 74.94536715745926
time_elpased: 1.444
batch start
#iterations: 245
currently lose_sum: 74.65368586778641
time_elpased: 1.443
batch start
#iterations: 246
currently lose_sum: 73.82689598202705
time_elpased: 1.429
batch start
#iterations: 247
currently lose_sum: 74.72125551104546
time_elpased: 1.419
batch start
#iterations: 248
currently lose_sum: 74.056611597538
time_elpased: 1.485
batch start
#iterations: 249
currently lose_sum: 74.29062378406525
time_elpased: 1.455
batch start
#iterations: 250
currently lose_sum: 74.00502067804337
time_elpased: 1.555
batch start
#iterations: 251
currently lose_sum: 74.69911059737206
time_elpased: 1.43
batch start
#iterations: 252
currently lose_sum: 74.27037236094475
time_elpased: 1.44
batch start
#iterations: 253
currently lose_sum: 73.92557036876678
time_elpased: 1.453
batch start
#iterations: 254
currently lose_sum: 73.54230597615242
time_elpased: 1.477
batch start
#iterations: 255
currently lose_sum: 73.32665196061134
time_elpased: 1.503
batch start
#iterations: 256
currently lose_sum: 73.96178284287453
time_elpased: 1.485
batch start
#iterations: 257
currently lose_sum: 74.17982739210129
time_elpased: 1.453
batch start
#iterations: 258
currently lose_sum: 73.32002186775208
time_elpased: 1.465
batch start
#iterations: 259
currently lose_sum: 73.43139725923538
time_elpased: 1.481
start validation test
0.578505154639
0.630641330166
0.382525470824
0.476202677599
0.578849227202
74.271
batch start
#iterations: 260
currently lose_sum: 73.41919556260109
time_elpased: 1.439
batch start
#iterations: 261
currently lose_sum: 73.16832348704338
time_elpased: 1.43
batch start
#iterations: 262
currently lose_sum: 73.42586776614189
time_elpased: 1.433
batch start
#iterations: 263
currently lose_sum: 72.99828597903252
time_elpased: 1.458
batch start
#iterations: 264
currently lose_sum: 74.37760093808174
time_elpased: 1.451
batch start
#iterations: 265
currently lose_sum: 73.64852210879326
time_elpased: 1.462
batch start
#iterations: 266
currently lose_sum: 73.78495198488235
time_elpased: 1.458
batch start
#iterations: 267
currently lose_sum: 73.50431713461876
time_elpased: 1.448
batch start
#iterations: 268
currently lose_sum: 73.14426958560944
time_elpased: 1.478
batch start
#iterations: 269
currently lose_sum: 73.34199979901314
time_elpased: 1.445
batch start
#iterations: 270
currently lose_sum: 73.23572498559952
time_elpased: 1.455
batch start
#iterations: 271
currently lose_sum: 73.71334686875343
time_elpased: 1.443
batch start
#iterations: 272
currently lose_sum: 73.2631967663765
time_elpased: 1.446
batch start
#iterations: 273
currently lose_sum: 72.53549984097481
time_elpased: 1.438
batch start
#iterations: 274
currently lose_sum: 74.4833171069622
time_elpased: 1.515
batch start
#iterations: 275
currently lose_sum: 72.75889185070992
time_elpased: 1.442
batch start
#iterations: 276
currently lose_sum: 72.72661137580872
time_elpased: 1.475
batch start
#iterations: 277
currently lose_sum: 73.56008842587471
time_elpased: 1.436
batch start
#iterations: 278
currently lose_sum: 73.08757975697517
time_elpased: 1.439
batch start
#iterations: 279
currently lose_sum: 72.80244824290276
time_elpased: 1.426
start validation test
0.577835051546
0.626344530862
0.389523515488
0.480329949239
0.578165661493
74.441
batch start
#iterations: 280
currently lose_sum: 73.01625373959541
time_elpased: 1.504
batch start
#iterations: 281
currently lose_sum: 73.23570612072945
time_elpased: 1.467
batch start
#iterations: 282
currently lose_sum: 72.96435546875
time_elpased: 1.531
batch start
#iterations: 283
currently lose_sum: 72.73590072989464
time_elpased: 1.431
batch start
#iterations: 284
currently lose_sum: 72.64452889561653
time_elpased: 1.454
batch start
#iterations: 285
currently lose_sum: 71.85573279857635
time_elpased: 1.43
batch start
#iterations: 286
currently lose_sum: 72.47762215137482
time_elpased: 1.444
batch start
#iterations: 287
currently lose_sum: 72.62529477477074
time_elpased: 1.446
batch start
#iterations: 288
currently lose_sum: 72.2235454916954
time_elpased: 1.486
batch start
#iterations: 289
currently lose_sum: 72.2789377272129
time_elpased: 1.453
batch start
#iterations: 290
currently lose_sum: 72.3213503062725
time_elpased: 1.456
batch start
#iterations: 291
currently lose_sum: 72.41158813238144
time_elpased: 1.457
batch start
#iterations: 292
currently lose_sum: 72.64521822333336
time_elpased: 1.454
batch start
#iterations: 293
currently lose_sum: 71.76262566447258
time_elpased: 1.426
batch start
#iterations: 294
currently lose_sum: 72.55051848292351
time_elpased: 1.458
batch start
#iterations: 295
currently lose_sum: 72.38489291071892
time_elpased: 1.466
batch start
#iterations: 296
currently lose_sum: 72.33326295018196
time_elpased: 1.444
batch start
#iterations: 297
currently lose_sum: 72.03998482227325
time_elpased: 1.437
batch start
#iterations: 298
currently lose_sum: 72.60387858748436
time_elpased: 1.452
batch start
#iterations: 299
currently lose_sum: 72.30799841880798
time_elpased: 1.428
start validation test
0.571546391753
0.636328352416
0.337449830195
0.441022192334
0.571957384373
78.740
batch start
#iterations: 300
currently lose_sum: 72.07123214006424
time_elpased: 1.512
batch start
#iterations: 301
currently lose_sum: 72.36293518543243
time_elpased: 1.449
batch start
#iterations: 302
currently lose_sum: 72.57427060604095
time_elpased: 1.487
batch start
#iterations: 303
currently lose_sum: 71.63822370767593
time_elpased: 1.452
batch start
#iterations: 304
currently lose_sum: 71.82310459017754
time_elpased: 1.445
batch start
#iterations: 305
currently lose_sum: 71.18432906270027
time_elpased: 1.489
batch start
#iterations: 306
currently lose_sum: 72.074147939682
time_elpased: 1.453
batch start
#iterations: 307
currently lose_sum: 72.76974987983704
time_elpased: 1.46
batch start
#iterations: 308
currently lose_sum: 71.90956383943558
time_elpased: 1.452
batch start
#iterations: 309
currently lose_sum: 71.35327705740929
time_elpased: 1.438
batch start
#iterations: 310
currently lose_sum: 72.24952381849289
time_elpased: 1.423
batch start
#iterations: 311
currently lose_sum: 71.26006042957306
time_elpased: 1.46
batch start
#iterations: 312
currently lose_sum: 71.03472536802292
time_elpased: 1.448
batch start
#iterations: 313
currently lose_sum: 71.9316323697567
time_elpased: 1.454
batch start
#iterations: 314
currently lose_sum: 71.83600655198097
time_elpased: 1.433
batch start
#iterations: 315
currently lose_sum: 72.04101577401161
time_elpased: 1.497
batch start
#iterations: 316
currently lose_sum: 71.28344625234604
time_elpased: 1.492
batch start
#iterations: 317
currently lose_sum: 72.01520997285843
time_elpased: 1.484
batch start
#iterations: 318
currently lose_sum: 71.78910821676254
time_elpased: 1.454
batch start
#iterations: 319
currently lose_sum: 71.9928959608078
time_elpased: 1.436
start validation test
0.577783505155
0.626199139927
0.38962642791
0.480365412675
0.578113843925
75.755
batch start
#iterations: 320
currently lose_sum: 71.68462097644806
time_elpased: 1.477
batch start
#iterations: 321
currently lose_sum: 71.59054559469223
time_elpased: 1.428
batch start
#iterations: 322
currently lose_sum: 71.1009011566639
time_elpased: 1.466
batch start
#iterations: 323
currently lose_sum: 71.55240547657013
time_elpased: 1.476
batch start
#iterations: 324
currently lose_sum: 71.90682420134544
time_elpased: 1.467
batch start
#iterations: 325
currently lose_sum: 71.9137372970581
time_elpased: 1.429
batch start
#iterations: 326
currently lose_sum: 71.498236566782
time_elpased: 1.449
batch start
#iterations: 327
currently lose_sum: 71.33836171030998
time_elpased: 1.439
batch start
#iterations: 328
currently lose_sum: 71.0038845539093
time_elpased: 1.496
batch start
#iterations: 329
currently lose_sum: 71.41573518514633
time_elpased: 1.472
batch start
#iterations: 330
currently lose_sum: 71.35510605573654
time_elpased: 1.451
batch start
#iterations: 331
currently lose_sum: 70.51633962988853
time_elpased: 1.442
batch start
#iterations: 332
currently lose_sum: 70.14648753404617
time_elpased: 1.439
batch start
#iterations: 333
currently lose_sum: 70.54939898848534
time_elpased: 1.428
batch start
#iterations: 334
currently lose_sum: 71.02500754594803
time_elpased: 1.505
batch start
#iterations: 335
currently lose_sum: 69.87468680739403
time_elpased: 1.478
batch start
#iterations: 336
currently lose_sum: 70.95821085572243
time_elpased: 1.42
batch start
#iterations: 337
currently lose_sum: 70.89439842104912
time_elpased: 1.44
batch start
#iterations: 338
currently lose_sum: 70.48933634161949
time_elpased: 1.442
batch start
#iterations: 339
currently lose_sum: 70.44731503725052
time_elpased: 1.446
start validation test
0.574639175258
0.625837484968
0.374909951631
0.468914918265
0.574989830716
77.198
batch start
#iterations: 340
currently lose_sum: 70.80160865187645
time_elpased: 1.463
batch start
#iterations: 341
currently lose_sum: 71.07617631554604
time_elpased: 1.444
batch start
#iterations: 342
currently lose_sum: 70.99816337227821
time_elpased: 1.446
batch start
#iterations: 343
currently lose_sum: 70.19549310207367
time_elpased: 1.455
batch start
#iterations: 344
currently lose_sum: 70.39128676056862
time_elpased: 1.448
batch start
#iterations: 345
currently lose_sum: 69.89167562127113
time_elpased: 1.49
batch start
#iterations: 346
currently lose_sum: 70.76923656463623
time_elpased: 1.441
batch start
#iterations: 347
currently lose_sum: 70.80499675869942
time_elpased: 1.442
batch start
#iterations: 348
currently lose_sum: 70.76391857862473
time_elpased: 1.454
batch start
#iterations: 349
currently lose_sum: 70.89568367600441
time_elpased: 1.454
batch start
#iterations: 350
currently lose_sum: 70.39129674434662
time_elpased: 1.517
batch start
#iterations: 351
currently lose_sum: 70.57021018862724
time_elpased: 1.41
batch start
#iterations: 352
currently lose_sum: 70.60234922170639
time_elpased: 1.439
batch start
#iterations: 353
currently lose_sum: 70.73939964175224
time_elpased: 1.446
batch start
#iterations: 354
currently lose_sum: 70.83326107263565
time_elpased: 1.449
batch start
#iterations: 355
currently lose_sum: 70.20927855372429
time_elpased: 1.454
batch start
#iterations: 356
currently lose_sum: 69.64645728468895
time_elpased: 1.458
batch start
#iterations: 357
currently lose_sum: 70.60455572605133
time_elpased: 1.441
batch start
#iterations: 358
currently lose_sum: 70.9640910923481
time_elpased: 1.465
batch start
#iterations: 359
currently lose_sum: 70.52401608228683
time_elpased: 1.44
start validation test
0.567886597938
0.627728839525
0.337346917773
0.438851328737
0.568291345905
79.588
batch start
#iterations: 360
currently lose_sum: 70.24710562825203
time_elpased: 1.45
batch start
#iterations: 361
currently lose_sum: 69.91980996727943
time_elpased: 1.423
batch start
#iterations: 362
currently lose_sum: 69.51974305510521
time_elpased: 1.45
batch start
#iterations: 363
currently lose_sum: 70.20372474193573
time_elpased: 1.431
batch start
#iterations: 364
currently lose_sum: 70.1610192656517
time_elpased: 1.457
batch start
#iterations: 365
currently lose_sum: 70.17943188548088
time_elpased: 1.452
batch start
#iterations: 366
currently lose_sum: 70.36758407950401
time_elpased: 1.47
batch start
#iterations: 367
currently lose_sum: 69.6895179450512
time_elpased: 1.442
batch start
#iterations: 368
currently lose_sum: 69.11422565579414
time_elpased: 1.482
batch start
#iterations: 369
currently lose_sum: 68.87208005785942
time_elpased: 1.466
batch start
#iterations: 370
currently lose_sum: 69.22741654515266
time_elpased: 1.458
batch start
#iterations: 371
currently lose_sum: 69.72963371872902
time_elpased: 1.441
batch start
#iterations: 372
currently lose_sum: 69.90063148736954
time_elpased: 1.442
batch start
#iterations: 373
currently lose_sum: 70.16281828284264
time_elpased: 1.439
batch start
#iterations: 374
currently lose_sum: 69.28592658042908
time_elpased: 1.444
batch start
#iterations: 375
currently lose_sum: 69.9876606464386
time_elpased: 1.458
batch start
#iterations: 376
currently lose_sum: 70.07409325242043
time_elpased: 1.465
batch start
#iterations: 377
currently lose_sum: 69.48617574572563
time_elpased: 1.474
batch start
#iterations: 378
currently lose_sum: 69.88828083872795
time_elpased: 1.448
batch start
#iterations: 379
currently lose_sum: 69.104911506176
time_elpased: 1.437
start validation test
0.564639175258
0.626669324297
0.323556653288
0.426768019547
0.565062432809
81.068
batch start
#iterations: 380
currently lose_sum: 70.27510258555412
time_elpased: 1.492
batch start
#iterations: 381
currently lose_sum: 69.45641341805458
time_elpased: 1.448
batch start
#iterations: 382
currently lose_sum: 69.51385208964348
time_elpased: 1.47
batch start
#iterations: 383
currently lose_sum: 69.22016695141792
time_elpased: 1.477
batch start
#iterations: 384
currently lose_sum: 69.35800698399544
time_elpased: 1.476
batch start
#iterations: 385
currently lose_sum: 69.95245632529259
time_elpased: 1.45
batch start
#iterations: 386
currently lose_sum: 68.94952297210693
time_elpased: 1.436
batch start
#iterations: 387
currently lose_sum: 69.27041244506836
time_elpased: 1.484
batch start
#iterations: 388
currently lose_sum: 69.25999042391777
time_elpased: 1.456
batch start
#iterations: 389
currently lose_sum: 68.949508279562
time_elpased: 1.469
batch start
#iterations: 390
currently lose_sum: 69.1573443710804
time_elpased: 1.475
batch start
#iterations: 391
currently lose_sum: 68.81990838050842
time_elpased: 1.451
batch start
#iterations: 392
currently lose_sum: 68.70927834510803
time_elpased: 1.443
batch start
#iterations: 393
currently lose_sum: 68.7611234486103
time_elpased: 1.427
batch start
#iterations: 394
currently lose_sum: 69.43204355239868
time_elpased: 1.46
batch start
#iterations: 395
currently lose_sum: 69.34452664852142
time_elpased: 1.42
batch start
#iterations: 396
currently lose_sum: 68.87273862957954
time_elpased: 1.464
batch start
#iterations: 397
currently lose_sum: 68.8658895790577
time_elpased: 1.451
batch start
#iterations: 398
currently lose_sum: 69.57188078761101
time_elpased: 1.462
batch start
#iterations: 399
currently lose_sum: 68.43300259113312
time_elpased: 1.457
start validation test
0.570309278351
0.611460855529
0.389832252753
0.476118652589
0.570626133606
78.394
acc: 0.629
pre: 0.648
rec: 0.567
F1: 0.604
auc: 0.684
