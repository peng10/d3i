start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.23405516147614
time_elpased: 1.49
batch start
#iterations: 1
currently lose_sum: 99.65306794643402
time_elpased: 1.445
batch start
#iterations: 2
currently lose_sum: 99.0917974114418
time_elpased: 1.446
batch start
#iterations: 3
currently lose_sum: 98.75101894140244
time_elpased: 1.485
batch start
#iterations: 4
currently lose_sum: 98.54116660356522
time_elpased: 1.466
batch start
#iterations: 5
currently lose_sum: 98.38376653194427
time_elpased: 1.429
batch start
#iterations: 6
currently lose_sum: 97.94359004497528
time_elpased: 1.498
batch start
#iterations: 7
currently lose_sum: 97.78024709224701
time_elpased: 1.453
batch start
#iterations: 8
currently lose_sum: 97.31204575300217
time_elpased: 1.473
batch start
#iterations: 9
currently lose_sum: 97.4294793009758
time_elpased: 1.459
batch start
#iterations: 10
currently lose_sum: 96.9430183172226
time_elpased: 1.467
batch start
#iterations: 11
currently lose_sum: 96.63524204492569
time_elpased: 1.469
batch start
#iterations: 12
currently lose_sum: 96.8851089477539
time_elpased: 1.483
batch start
#iterations: 13
currently lose_sum: 96.70473438501358
time_elpased: 1.422
batch start
#iterations: 14
currently lose_sum: 96.08621400594711
time_elpased: 1.468
batch start
#iterations: 15
currently lose_sum: 96.09635931253433
time_elpased: 1.429
batch start
#iterations: 16
currently lose_sum: 96.06181585788727
time_elpased: 1.418
batch start
#iterations: 17
currently lose_sum: 96.10230511426926
time_elpased: 1.451
batch start
#iterations: 18
currently lose_sum: 95.60402917861938
time_elpased: 1.434
batch start
#iterations: 19
currently lose_sum: 95.28194522857666
time_elpased: 1.435
start validation test
0.63381443299
0.643421919613
0.603025936599
0.622569333758
0.633865302046
62.713
batch start
#iterations: 20
currently lose_sum: 94.98365288972855
time_elpased: 1.512
batch start
#iterations: 21
currently lose_sum: 95.1415376663208
time_elpased: 1.447
batch start
#iterations: 22
currently lose_sum: 95.01509004831314
time_elpased: 1.45
batch start
#iterations: 23
currently lose_sum: 94.59406417608261
time_elpased: 1.417
batch start
#iterations: 24
currently lose_sum: 94.53936159610748
time_elpased: 1.436
batch start
#iterations: 25
currently lose_sum: 94.47923064231873
time_elpased: 1.424
batch start
#iterations: 26
currently lose_sum: 94.25404113531113
time_elpased: 1.417
batch start
#iterations: 27
currently lose_sum: 94.21923840045929
time_elpased: 1.441
batch start
#iterations: 28
currently lose_sum: 93.66910272836685
time_elpased: 1.471
batch start
#iterations: 29
currently lose_sum: 94.00591534376144
time_elpased: 1.461
batch start
#iterations: 30
currently lose_sum: 93.74570333957672
time_elpased: 1.45
batch start
#iterations: 31
currently lose_sum: 93.21658229827881
time_elpased: 1.437
batch start
#iterations: 32
currently lose_sum: 93.10783088207245
time_elpased: 1.48
batch start
#iterations: 33
currently lose_sum: 93.08463263511658
time_elpased: 1.473
batch start
#iterations: 34
currently lose_sum: 92.93247973918915
time_elpased: 1.445
batch start
#iterations: 35
currently lose_sum: 93.28344005346298
time_elpased: 1.441
batch start
#iterations: 36
currently lose_sum: 92.34342271089554
time_elpased: 1.433
batch start
#iterations: 37
currently lose_sum: 92.55881810188293
time_elpased: 1.467
batch start
#iterations: 38
currently lose_sum: 91.60447818040848
time_elpased: 1.431
batch start
#iterations: 39
currently lose_sum: 92.05252349376678
time_elpased: 1.442
start validation test
0.614793814433
0.645027802923
0.513379991766
0.571723307926
0.614961371348
63.261
batch start
#iterations: 40
currently lose_sum: 92.0831567645073
time_elpased: 1.513
batch start
#iterations: 41
currently lose_sum: 91.69407367706299
time_elpased: 1.449
batch start
#iterations: 42
currently lose_sum: 91.56251168251038
time_elpased: 1.453
batch start
#iterations: 43
currently lose_sum: 91.31684809923172
time_elpased: 1.44
batch start
#iterations: 44
currently lose_sum: 91.29495388269424
time_elpased: 1.528
batch start
#iterations: 45
currently lose_sum: 91.22055166959763
time_elpased: 1.436
batch start
#iterations: 46
currently lose_sum: 90.9400565624237
time_elpased: 1.426
batch start
#iterations: 47
currently lose_sum: 90.85631567239761
time_elpased: 1.456
batch start
#iterations: 48
currently lose_sum: 90.36910736560822
time_elpased: 1.429
batch start
#iterations: 49
currently lose_sum: 90.94667136669159
time_elpased: 1.448
batch start
#iterations: 50
currently lose_sum: 90.7334748506546
time_elpased: 1.469
batch start
#iterations: 51
currently lose_sum: 90.27339106798172
time_elpased: 1.423
batch start
#iterations: 52
currently lose_sum: 90.22160637378693
time_elpased: 1.507
batch start
#iterations: 53
currently lose_sum: 90.09810411930084
time_elpased: 1.488
batch start
#iterations: 54
currently lose_sum: 90.06163716316223
time_elpased: 1.444
batch start
#iterations: 55
currently lose_sum: 89.38751244544983
time_elpased: 1.46
batch start
#iterations: 56
currently lose_sum: 89.66099721193314
time_elpased: 1.449
batch start
#iterations: 57
currently lose_sum: 89.51538807153702
time_elpased: 1.468
batch start
#iterations: 58
currently lose_sum: 89.46399933099747
time_elpased: 1.491
batch start
#iterations: 59
currently lose_sum: 89.04834055900574
time_elpased: 1.42
start validation test
0.601288659794
0.639487396141
0.467373404693
0.540048760183
0.601509915895
63.813
batch start
#iterations: 60
currently lose_sum: 88.72517824172974
time_elpased: 1.47
batch start
#iterations: 61
currently lose_sum: 89.24753314256668
time_elpased: 1.459
batch start
#iterations: 62
currently lose_sum: 89.06890553236008
time_elpased: 1.469
batch start
#iterations: 63
currently lose_sum: 88.81115794181824
time_elpased: 1.524
batch start
#iterations: 64
currently lose_sum: 88.1868799328804
time_elpased: 1.421
batch start
#iterations: 65
currently lose_sum: 88.77372121810913
time_elpased: 1.445
batch start
#iterations: 66
currently lose_sum: 88.24401444196701
time_elpased: 1.449
batch start
#iterations: 67
currently lose_sum: 88.14461988210678
time_elpased: 1.45
batch start
#iterations: 68
currently lose_sum: 87.834039747715
time_elpased: 1.46
batch start
#iterations: 69
currently lose_sum: 87.48219692707062
time_elpased: 1.449
batch start
#iterations: 70
currently lose_sum: 87.9039204120636
time_elpased: 1.444
batch start
#iterations: 71
currently lose_sum: 87.47084593772888
time_elpased: 1.439
batch start
#iterations: 72
currently lose_sum: 87.01454484462738
time_elpased: 1.445
batch start
#iterations: 73
currently lose_sum: 87.35622316598892
time_elpased: 1.448
batch start
#iterations: 74
currently lose_sum: 87.17858558893204
time_elpased: 1.478
batch start
#iterations: 75
currently lose_sum: 87.10789030790329
time_elpased: 1.451
batch start
#iterations: 76
currently lose_sum: 87.44037944078445
time_elpased: 1.45
batch start
#iterations: 77
currently lose_sum: 86.7141764163971
time_elpased: 1.427
batch start
#iterations: 78
currently lose_sum: 86.90172135829926
time_elpased: 1.461
batch start
#iterations: 79
currently lose_sum: 86.91166710853577
time_elpased: 1.448
start validation test
0.593092783505
0.633460298857
0.445039110745
0.522790472736
0.593337399239
65.261
batch start
#iterations: 80
currently lose_sum: 86.7608762383461
time_elpased: 1.454
batch start
#iterations: 81
currently lose_sum: 86.09414768218994
time_elpased: 1.488
batch start
#iterations: 82
currently lose_sum: 86.53619515895844
time_elpased: 1.434
batch start
#iterations: 83
currently lose_sum: 86.57351648807526
time_elpased: 1.463
batch start
#iterations: 84
currently lose_sum: 86.01134395599365
time_elpased: 1.444
batch start
#iterations: 85
currently lose_sum: 86.1211649775505
time_elpased: 1.452
batch start
#iterations: 86
currently lose_sum: 85.51193523406982
time_elpased: 1.435
batch start
#iterations: 87
currently lose_sum: 85.78928858041763
time_elpased: 1.435
batch start
#iterations: 88
currently lose_sum: 85.25864624977112
time_elpased: 1.478
batch start
#iterations: 89
currently lose_sum: 86.186862885952
time_elpased: 1.463
batch start
#iterations: 90
currently lose_sum: 85.19010710716248
time_elpased: 1.444
batch start
#iterations: 91
currently lose_sum: 85.49998158216476
time_elpased: 1.444
batch start
#iterations: 92
currently lose_sum: 85.26389998197556
time_elpased: 1.442
batch start
#iterations: 93
currently lose_sum: 85.23506397008896
time_elpased: 1.448
batch start
#iterations: 94
currently lose_sum: 84.90502041578293
time_elpased: 1.487
batch start
#iterations: 95
currently lose_sum: 84.65770971775055
time_elpased: 1.447
batch start
#iterations: 96
currently lose_sum: 84.60001170635223
time_elpased: 1.426
batch start
#iterations: 97
currently lose_sum: 84.53046947717667
time_elpased: 1.441
batch start
#iterations: 98
currently lose_sum: 84.67389124631882
time_elpased: 1.468
batch start
#iterations: 99
currently lose_sum: 84.4264200925827
time_elpased: 1.474
start validation test
0.59881443299
0.628506847494
0.48651708522
0.548471311713
0.598999971772
65.602
batch start
#iterations: 100
currently lose_sum: 84.99503004550934
time_elpased: 1.44
batch start
#iterations: 101
currently lose_sum: 84.61748099327087
time_elpased: 1.455
batch start
#iterations: 102
currently lose_sum: 84.31849402189255
time_elpased: 1.463
batch start
#iterations: 103
currently lose_sum: 84.33036696910858
time_elpased: 1.479
batch start
#iterations: 104
currently lose_sum: 84.50370597839355
time_elpased: 1.461
batch start
#iterations: 105
currently lose_sum: 83.68530625104904
time_elpased: 1.434
batch start
#iterations: 106
currently lose_sum: 83.47955298423767
time_elpased: 1.478
batch start
#iterations: 107
currently lose_sum: 83.66191273927689
time_elpased: 1.433
batch start
#iterations: 108
currently lose_sum: 83.85709425806999
time_elpased: 1.464
batch start
#iterations: 109
currently lose_sum: 83.96409994363785
time_elpased: 1.403
batch start
#iterations: 110
currently lose_sum: 83.5577745437622
time_elpased: 1.444
batch start
#iterations: 111
currently lose_sum: 83.59735983610153
time_elpased: 1.444
batch start
#iterations: 112
currently lose_sum: 83.04676127433777
time_elpased: 1.412
batch start
#iterations: 113
currently lose_sum: 83.07091867923737
time_elpased: 1.446
batch start
#iterations: 114
currently lose_sum: 83.52843707799911
time_elpased: 1.43
batch start
#iterations: 115
currently lose_sum: 83.3509870171547
time_elpased: 1.483
batch start
#iterations: 116
currently lose_sum: 83.16959196329117
time_elpased: 1.459
batch start
#iterations: 117
currently lose_sum: 82.72151872515678
time_elpased: 1.44
batch start
#iterations: 118
currently lose_sum: 82.70053374767303
time_elpased: 1.413
batch start
#iterations: 119
currently lose_sum: 83.11094164848328
time_elpased: 1.418
start validation test
0.593298969072
0.616454081633
0.49742692466
0.550580997949
0.593457369806
66.625
batch start
#iterations: 120
currently lose_sum: 82.83787989616394
time_elpased: 1.494
batch start
#iterations: 121
currently lose_sum: 82.68134468793869
time_elpased: 1.424
batch start
#iterations: 122
currently lose_sum: 82.73325917124748
time_elpased: 1.443
batch start
#iterations: 123
currently lose_sum: 82.54604434967041
time_elpased: 1.446
batch start
#iterations: 124
currently lose_sum: 82.27105844020844
time_elpased: 1.463
batch start
#iterations: 125
currently lose_sum: 82.00305950641632
time_elpased: 1.417
batch start
#iterations: 126
currently lose_sum: 81.87054488062859
time_elpased: 1.444
batch start
#iterations: 127
currently lose_sum: 82.11405539512634
time_elpased: 1.429
batch start
#iterations: 128
currently lose_sum: 81.75806659460068
time_elpased: 1.426
batch start
#iterations: 129
currently lose_sum: 81.37106645107269
time_elpased: 1.446
batch start
#iterations: 130
currently lose_sum: 82.01552510261536
time_elpased: 1.423
batch start
#iterations: 131
currently lose_sum: 81.73058521747589
time_elpased: 1.44
batch start
#iterations: 132
currently lose_sum: 81.2364849448204
time_elpased: 1.475
batch start
#iterations: 133
currently lose_sum: 81.44757062196732
time_elpased: 1.422
batch start
#iterations: 134
currently lose_sum: 82.2729423046112
time_elpased: 1.511
batch start
#iterations: 135
currently lose_sum: 81.33719176054001
time_elpased: 1.427
batch start
#iterations: 136
currently lose_sum: 81.86951500177383
time_elpased: 1.433
batch start
#iterations: 137
currently lose_sum: 81.74612295627594
time_elpased: 1.437
batch start
#iterations: 138
currently lose_sum: 80.58585453033447
time_elpased: 1.467
batch start
#iterations: 139
currently lose_sum: 80.81567957997322
time_elpased: 1.491
start validation test
0.593608247423
0.63104434907
0.453993412927
0.52807374596
0.593838920425
67.459
batch start
#iterations: 140
currently lose_sum: 81.05866721272469
time_elpased: 1.448
batch start
#iterations: 141
currently lose_sum: 81.20145991444588
time_elpased: 1.465
batch start
#iterations: 142
currently lose_sum: 80.22381019592285
time_elpased: 1.467
batch start
#iterations: 143
currently lose_sum: 80.98806831240654
time_elpased: 1.419
batch start
#iterations: 144
currently lose_sum: 80.54507637023926
time_elpased: 1.451
batch start
#iterations: 145
currently lose_sum: 80.85954332351685
time_elpased: 1.439
batch start
#iterations: 146
currently lose_sum: 80.64892077445984
time_elpased: 1.429
batch start
#iterations: 147
currently lose_sum: 79.97941729426384
time_elpased: 1.437
batch start
#iterations: 148
currently lose_sum: 80.18562105298042
time_elpased: 1.431
batch start
#iterations: 149
currently lose_sum: 80.58593246340752
time_elpased: 1.444
batch start
#iterations: 150
currently lose_sum: 80.69569060206413
time_elpased: 1.447
batch start
#iterations: 151
currently lose_sum: 80.61902233958244
time_elpased: 1.46
batch start
#iterations: 152
currently lose_sum: 79.89021563529968
time_elpased: 1.465
batch start
#iterations: 153
currently lose_sum: 80.2490366101265
time_elpased: 1.445
batch start
#iterations: 154
currently lose_sum: 80.42697778344154
time_elpased: 1.439
batch start
#iterations: 155
currently lose_sum: 80.16508135199547
time_elpased: 1.43
batch start
#iterations: 156
currently lose_sum: 79.81058484315872
time_elpased: 1.43
batch start
#iterations: 157
currently lose_sum: 79.16769534349442
time_elpased: 1.432
batch start
#iterations: 158
currently lose_sum: 79.33251771330833
time_elpased: 1.44
batch start
#iterations: 159
currently lose_sum: 79.60179916024208
time_elpased: 1.424
start validation test
0.579329896907
0.633293331047
0.380197612186
0.475143096019
0.579658905226
70.407
batch start
#iterations: 160
currently lose_sum: 79.43033128976822
time_elpased: 1.457
batch start
#iterations: 161
currently lose_sum: 79.10325199365616
time_elpased: 1.471
batch start
#iterations: 162
currently lose_sum: 79.44164171814919
time_elpased: 1.471
batch start
#iterations: 163
currently lose_sum: 79.57486718893051
time_elpased: 1.446
batch start
#iterations: 164
currently lose_sum: 79.80752584338188
time_elpased: 1.421
batch start
#iterations: 165
currently lose_sum: 79.42738020420074
time_elpased: 1.441
batch start
#iterations: 166
currently lose_sum: 79.55815753340721
time_elpased: 1.45
batch start
#iterations: 167
currently lose_sum: 79.00516140460968
time_elpased: 1.483
batch start
#iterations: 168
currently lose_sum: 78.77979519963264
time_elpased: 1.424
batch start
#iterations: 169
currently lose_sum: 78.58725708723068
time_elpased: 1.492
batch start
#iterations: 170
currently lose_sum: 77.97423088550568
time_elpased: 1.414
batch start
#iterations: 171
currently lose_sum: 79.12112909555435
time_elpased: 1.441
batch start
#iterations: 172
currently lose_sum: 78.80248561501503
time_elpased: 1.414
batch start
#iterations: 173
currently lose_sum: 78.87309807538986
time_elpased: 1.452
batch start
#iterations: 174
currently lose_sum: 77.67427769303322
time_elpased: 1.506
batch start
#iterations: 175
currently lose_sum: 78.32699957489967
time_elpased: 1.503
batch start
#iterations: 176
currently lose_sum: 78.23223492503166
time_elpased: 1.434
batch start
#iterations: 177
currently lose_sum: 78.28918641805649
time_elpased: 1.443
batch start
#iterations: 178
currently lose_sum: 78.01714852452278
time_elpased: 1.437
batch start
#iterations: 179
currently lose_sum: 78.03063780069351
time_elpased: 1.451
start validation test
0.585927835052
0.623622741296
0.436908192672
0.513829207771
0.58617404677
69.843
batch start
#iterations: 180
currently lose_sum: 78.5447843670845
time_elpased: 1.443
batch start
#iterations: 181
currently lose_sum: 78.43610873818398
time_elpased: 1.462
batch start
#iterations: 182
currently lose_sum: 78.81582713127136
time_elpased: 1.451
batch start
#iterations: 183
currently lose_sum: 77.59565237164497
time_elpased: 1.443
batch start
#iterations: 184
currently lose_sum: 78.49656218290329
time_elpased: 1.424
batch start
#iterations: 185
currently lose_sum: 76.97031027078629
time_elpased: 1.421
batch start
#iterations: 186
currently lose_sum: 78.50510159134865
time_elpased: 1.437
batch start
#iterations: 187
currently lose_sum: 76.86913499236107
time_elpased: 1.466
batch start
#iterations: 188
currently lose_sum: 78.22557765245438
time_elpased: 1.439
batch start
#iterations: 189
currently lose_sum: 78.29207932949066
time_elpased: 1.44
batch start
#iterations: 190
currently lose_sum: 77.62675881385803
time_elpased: 1.499
batch start
#iterations: 191
currently lose_sum: 77.47426697611809
time_elpased: 1.495
batch start
#iterations: 192
currently lose_sum: 77.7730344235897
time_elpased: 1.471
batch start
#iterations: 193
currently lose_sum: 77.47087341547012
time_elpased: 1.417
batch start
#iterations: 194
currently lose_sum: 77.5076413154602
time_elpased: 1.436
batch start
#iterations: 195
currently lose_sum: 77.16143983602524
time_elpased: 1.441
batch start
#iterations: 196
currently lose_sum: 77.1359860599041
time_elpased: 1.484
batch start
#iterations: 197
currently lose_sum: 76.87790733575821
time_elpased: 1.442
batch start
#iterations: 198
currently lose_sum: 77.34398829936981
time_elpased: 1.432
batch start
#iterations: 199
currently lose_sum: 76.82484272122383
time_elpased: 1.447
start validation test
0.576134020619
0.627367343457
0.378447920955
0.472106310586
0.576460639536
72.775
batch start
#iterations: 200
currently lose_sum: 76.63887959718704
time_elpased: 1.416
batch start
#iterations: 201
currently lose_sum: 77.39115762710571
time_elpased: 1.44
batch start
#iterations: 202
currently lose_sum: 77.49910759925842
time_elpased: 1.459
batch start
#iterations: 203
currently lose_sum: 77.03960898518562
time_elpased: 1.44
batch start
#iterations: 204
currently lose_sum: 77.27594459056854
time_elpased: 1.425
batch start
#iterations: 205
currently lose_sum: 76.93717762827873
time_elpased: 1.467
batch start
#iterations: 206
currently lose_sum: 76.43951690196991
time_elpased: 1.427
batch start
#iterations: 207
currently lose_sum: 76.68731048703194
time_elpased: 1.442
batch start
#iterations: 208
currently lose_sum: 76.62403947114944
time_elpased: 1.454
batch start
#iterations: 209
currently lose_sum: 75.65276011824608
time_elpased: 1.421
batch start
#iterations: 210
currently lose_sum: 75.92263334989548
time_elpased: 1.421
batch start
#iterations: 211
currently lose_sum: 76.58326449990273
time_elpased: 1.499
batch start
#iterations: 212
currently lose_sum: 76.57896393537521
time_elpased: 1.418
batch start
#iterations: 213
currently lose_sum: 76.20867079496384
time_elpased: 1.469
batch start
#iterations: 214
currently lose_sum: 75.78012326359749
time_elpased: 1.454
batch start
#iterations: 215
currently lose_sum: 76.3311457335949
time_elpased: 1.427
batch start
#iterations: 216
currently lose_sum: 76.09815013408661
time_elpased: 1.451
batch start
#iterations: 217
currently lose_sum: 75.85974669456482
time_elpased: 1.427
batch start
#iterations: 218
currently lose_sum: 76.47565254569054
time_elpased: 1.425
batch start
#iterations: 219
currently lose_sum: 75.44631722569466
time_elpased: 1.476
start validation test
0.579381443299
0.628213579433
0.392342527789
0.483020780537
0.579690470834
72.304
batch start
#iterations: 220
currently lose_sum: 76.1344682276249
time_elpased: 1.427
batch start
#iterations: 221
currently lose_sum: 75.89499181509018
time_elpased: 1.426
batch start
#iterations: 222
currently lose_sum: 75.33501881361008
time_elpased: 1.499
batch start
#iterations: 223
currently lose_sum: 75.63450062274933
time_elpased: 1.43
batch start
#iterations: 224
currently lose_sum: 75.75624081492424
time_elpased: 1.455
batch start
#iterations: 225
currently lose_sum: 75.15894398093224
time_elpased: 1.435
batch start
#iterations: 226
currently lose_sum: 75.45040303468704
time_elpased: 1.459
batch start
#iterations: 227
currently lose_sum: 75.51730200648308
time_elpased: 1.464
batch start
#iterations: 228
currently lose_sum: 75.06538984179497
time_elpased: 1.422
batch start
#iterations: 229
currently lose_sum: 75.09976902604103
time_elpased: 1.469
batch start
#iterations: 230
currently lose_sum: 75.41466075181961
time_elpased: 1.445
batch start
#iterations: 231
currently lose_sum: 76.3251533806324
time_elpased: 1.485
batch start
#iterations: 232
currently lose_sum: 76.11460903286934
time_elpased: 1.442
batch start
#iterations: 233
currently lose_sum: 75.90287497639656
time_elpased: 1.415
batch start
#iterations: 234
currently lose_sum: 75.81877353787422
time_elpased: 1.495
batch start
#iterations: 235
currently lose_sum: 75.40225318074226
time_elpased: 1.431
batch start
#iterations: 236
currently lose_sum: 75.10920333862305
time_elpased: 1.435
batch start
#iterations: 237
currently lose_sum: 75.10424464941025
time_elpased: 1.499
batch start
#iterations: 238
currently lose_sum: 75.16652703285217
time_elpased: 1.464
batch start
#iterations: 239
currently lose_sum: 75.27921852469444
time_elpased: 1.437
start validation test
0.576597938144
0.624958402662
0.386578839028
0.477680274704
0.576911889568
73.314
batch start
#iterations: 240
currently lose_sum: 75.04636001586914
time_elpased: 1.448
batch start
#iterations: 241
currently lose_sum: 75.38018080592155
time_elpased: 1.429
batch start
#iterations: 242
currently lose_sum: 74.89732867479324
time_elpased: 1.433
batch start
#iterations: 243
currently lose_sum: 75.26595577597618
time_elpased: 1.48
batch start
#iterations: 244
currently lose_sum: 74.89483976364136
time_elpased: 1.43
batch start
#iterations: 245
currently lose_sum: 75.3501161634922
time_elpased: 1.449
batch start
#iterations: 246
currently lose_sum: 74.22431445121765
time_elpased: 1.499
batch start
#iterations: 247
currently lose_sum: 75.02728819847107
time_elpased: 1.409
batch start
#iterations: 248
currently lose_sum: 74.41985002160072
time_elpased: 1.462
batch start
#iterations: 249
currently lose_sum: 74.70979222655296
time_elpased: 1.451
batch start
#iterations: 250
currently lose_sum: 74.23621588945389
time_elpased: 1.424
batch start
#iterations: 251
currently lose_sum: 74.97439980506897
time_elpased: 1.474
batch start
#iterations: 252
currently lose_sum: 74.61940801143646
time_elpased: 1.429
batch start
#iterations: 253
currently lose_sum: 74.34525454044342
time_elpased: 1.421
batch start
#iterations: 254
currently lose_sum: 73.04341626167297
time_elpased: 1.429
batch start
#iterations: 255
currently lose_sum: 74.37804156541824
time_elpased: 1.455
batch start
#iterations: 256
currently lose_sum: 74.76612281799316
time_elpased: 1.451
batch start
#iterations: 257
currently lose_sum: 74.90653058886528
time_elpased: 1.442
batch start
#iterations: 258
currently lose_sum: 74.43712794780731
time_elpased: 1.446
batch start
#iterations: 259
currently lose_sum: 74.47453048825264
time_elpased: 1.481
start validation test
0.575463917526
0.627762430939
0.374228077398
0.468919267475
0.575796401359
74.919
batch start
#iterations: 260
currently lose_sum: 73.96331772208214
time_elpased: 1.424
batch start
#iterations: 261
currently lose_sum: 73.9660977423191
time_elpased: 1.469
batch start
#iterations: 262
currently lose_sum: 73.56408435106277
time_elpased: 1.458
batch start
#iterations: 263
currently lose_sum: 74.33322131633759
time_elpased: 1.431
batch start
#iterations: 264
currently lose_sum: 73.86412101984024
time_elpased: 1.485
batch start
#iterations: 265
currently lose_sum: 74.35466620326042
time_elpased: 1.437
batch start
#iterations: 266
currently lose_sum: 73.9082758128643
time_elpased: 1.43
batch start
#iterations: 267
currently lose_sum: 73.84768772125244
time_elpased: 1.419
batch start
#iterations: 268
currently lose_sum: 73.71472325921059
time_elpased: 1.459
batch start
#iterations: 269
currently lose_sum: 74.14643287658691
time_elpased: 1.427
batch start
#iterations: 270
currently lose_sum: 73.27661901712418
time_elpased: 1.454
batch start
#iterations: 271
currently lose_sum: 74.04318326711655
time_elpased: 1.427
batch start
#iterations: 272
currently lose_sum: 74.06501761078835
time_elpased: 1.431
batch start
#iterations: 273
currently lose_sum: 73.14347359538078
time_elpased: 1.427
batch start
#iterations: 274
currently lose_sum: 74.21451434493065
time_elpased: 1.488
batch start
#iterations: 275
currently lose_sum: 73.66027861833572
time_elpased: 1.421
batch start
#iterations: 276
currently lose_sum: 73.2308469414711
time_elpased: 1.443
batch start
#iterations: 277
currently lose_sum: 72.8643853366375
time_elpased: 1.444
batch start
#iterations: 278
currently lose_sum: 73.34731394052505
time_elpased: 1.415
batch start
#iterations: 279
currently lose_sum: 73.6991278231144
time_elpased: 1.475
start validation test
0.572989690722
0.635145337863
0.346335940716
0.448248301585
0.573364170276
76.325
batch start
#iterations: 280
currently lose_sum: 72.93169936537743
time_elpased: 1.417
batch start
#iterations: 281
currently lose_sum: 72.53414753079414
time_elpased: 1.415
batch start
#iterations: 282
currently lose_sum: 73.53715354204178
time_elpased: 1.413
batch start
#iterations: 283
currently lose_sum: 73.31258276104927
time_elpased: 1.429
batch start
#iterations: 284
currently lose_sum: 72.81753253936768
time_elpased: 1.43
batch start
#iterations: 285
currently lose_sum: 73.1020157635212
time_elpased: 1.438
batch start
#iterations: 286
currently lose_sum: 72.94367215037346
time_elpased: 1.419
batch start
#iterations: 287
currently lose_sum: 73.64603412151337
time_elpased: 1.447
batch start
#iterations: 288
currently lose_sum: 73.69404250383377
time_elpased: 1.456
batch start
#iterations: 289
currently lose_sum: 72.94104489684105
time_elpased: 1.423
batch start
#iterations: 290
currently lose_sum: 73.40509089827538
time_elpased: 1.435
batch start
#iterations: 291
currently lose_sum: 73.25231382250786
time_elpased: 1.44
batch start
#iterations: 292
currently lose_sum: 72.29575815796852
time_elpased: 1.443
batch start
#iterations: 293
currently lose_sum: 72.42450347542763
time_elpased: 1.436
batch start
#iterations: 294
currently lose_sum: 72.04247403144836
time_elpased: 1.45
batch start
#iterations: 295
currently lose_sum: 72.35907280445099
time_elpased: 1.426
batch start
#iterations: 296
currently lose_sum: 72.96653991937637
time_elpased: 1.467
batch start
#iterations: 297
currently lose_sum: 73.21695682406425
time_elpased: 1.446
batch start
#iterations: 298
currently lose_sum: 72.41615200042725
time_elpased: 1.424
batch start
#iterations: 299
currently lose_sum: 72.92902767658234
time_elpased: 1.464
start validation test
0.569226804124
0.62406426876
0.351790860436
0.449944053182
0.569586053927
77.351
batch start
#iterations: 300
currently lose_sum: 72.9192508161068
time_elpased: 1.416
batch start
#iterations: 301
currently lose_sum: 71.9457315504551
time_elpased: 1.44
batch start
#iterations: 302
currently lose_sum: 72.478814214468
time_elpased: 1.446
batch start
#iterations: 303
currently lose_sum: 72.6210378408432
time_elpased: 1.426
batch start
#iterations: 304
currently lose_sum: 72.13643872737885
time_elpased: 1.439
batch start
#iterations: 305
currently lose_sum: 72.66371467709541
time_elpased: 1.463
batch start
#iterations: 306
currently lose_sum: 72.85623890161514
time_elpased: 1.454
batch start
#iterations: 307
currently lose_sum: 72.85670882463455
time_elpased: 1.429
batch start
#iterations: 308
currently lose_sum: 72.27803629636765
time_elpased: 1.423
batch start
#iterations: 309
currently lose_sum: 71.69621714949608
time_elpased: 1.44
batch start
#iterations: 310
currently lose_sum: 72.15738981962204
time_elpased: 1.442
batch start
#iterations: 311
currently lose_sum: 71.98145419359207
time_elpased: 1.441
batch start
#iterations: 312
currently lose_sum: 72.54061585664749
time_elpased: 1.421
batch start
#iterations: 313
currently lose_sum: 72.23963129520416
time_elpased: 1.417
batch start
#iterations: 314
currently lose_sum: 71.85510182380676
time_elpased: 1.426
batch start
#iterations: 315
currently lose_sum: 71.96214443445206
time_elpased: 1.476
batch start
#iterations: 316
currently lose_sum: 71.95492577552795
time_elpased: 1.414
batch start
#iterations: 317
currently lose_sum: 71.25631731748581
time_elpased: 1.477
batch start
#iterations: 318
currently lose_sum: 72.09530475735664
time_elpased: 1.432
batch start
#iterations: 319
currently lose_sum: 72.7788947224617
time_elpased: 1.486
start validation test
0.566030927835
0.630509156772
0.322457801564
0.426693905346
0.566433361749
79.209
batch start
#iterations: 320
currently lose_sum: 71.73510351777077
time_elpased: 1.45
batch start
#iterations: 321
currently lose_sum: 71.53199177980423
time_elpased: 1.433
batch start
#iterations: 322
currently lose_sum: 71.98183500766754
time_elpased: 1.465
batch start
#iterations: 323
currently lose_sum: 71.82155269384384
time_elpased: 1.431
batch start
#iterations: 324
currently lose_sum: 72.34445056319237
time_elpased: 1.454
batch start
#iterations: 325
currently lose_sum: 71.77460440993309
time_elpased: 1.438
batch start
#iterations: 326
currently lose_sum: 71.19295257329941
time_elpased: 1.445
batch start
#iterations: 327
currently lose_sum: 71.70022210478783
time_elpased: 1.442
batch start
#iterations: 328
currently lose_sum: 71.77548974752426
time_elpased: 1.453
batch start
#iterations: 329
currently lose_sum: 71.96910190582275
time_elpased: 1.452
batch start
#iterations: 330
currently lose_sum: 71.0724626481533
time_elpased: 1.445
batch start
#iterations: 331
currently lose_sum: 71.76690518856049
time_elpased: 1.439
batch start
#iterations: 332
currently lose_sum: 72.25964668393135
time_elpased: 1.432
batch start
#iterations: 333
currently lose_sum: 71.31613910198212
time_elpased: 1.436
batch start
#iterations: 334
currently lose_sum: 71.48408862948418
time_elpased: 1.429
batch start
#iterations: 335
currently lose_sum: 71.5488531589508
time_elpased: 1.437
batch start
#iterations: 336
currently lose_sum: 70.71094608306885
time_elpased: 1.442
batch start
#iterations: 337
currently lose_sum: 70.87015655636787
time_elpased: 1.427
batch start
#iterations: 338
currently lose_sum: 71.22795045375824
time_elpased: 1.434
batch start
#iterations: 339
currently lose_sum: 71.27052500844002
time_elpased: 1.432
start validation test
0.571855670103
0.621050824176
0.37227254014
0.465508365508
0.572185423312
77.772
batch start
#iterations: 340
currently lose_sum: 71.27502191066742
time_elpased: 1.442
batch start
#iterations: 341
currently lose_sum: 71.17974501848221
time_elpased: 1.406
batch start
#iterations: 342
currently lose_sum: 71.35762336850166
time_elpased: 1.441
batch start
#iterations: 343
currently lose_sum: 70.42776346206665
time_elpased: 1.45
batch start
#iterations: 344
currently lose_sum: 70.92680737376213
time_elpased: 1.408
batch start
#iterations: 345
currently lose_sum: 70.69620969891548
time_elpased: 1.443
batch start
#iterations: 346
currently lose_sum: 70.32913196086884
time_elpased: 1.413
batch start
#iterations: 347
currently lose_sum: 70.55868282914162
time_elpased: 1.43
batch start
#iterations: 348
currently lose_sum: 71.20313042402267
time_elpased: 1.446
batch start
#iterations: 349
currently lose_sum: 70.47911813855171
time_elpased: 1.431
batch start
#iterations: 350
currently lose_sum: 71.4388886988163
time_elpased: 1.435
batch start
#iterations: 351
currently lose_sum: 70.83430203795433
time_elpased: 1.423
batch start
#iterations: 352
currently lose_sum: 70.47199842333794
time_elpased: 1.423
batch start
#iterations: 353
currently lose_sum: 71.19508811831474
time_elpased: 1.446
batch start
#iterations: 354
currently lose_sum: 70.91984522342682
time_elpased: 1.446
batch start
#iterations: 355
currently lose_sum: 70.16963976621628
time_elpased: 1.433
batch start
#iterations: 356
currently lose_sum: 71.14855915307999
time_elpased: 1.445
batch start
#iterations: 357
currently lose_sum: 69.97861117124557
time_elpased: 1.436
batch start
#iterations: 358
currently lose_sum: 70.49543699622154
time_elpased: 1.471
batch start
#iterations: 359
currently lose_sum: 70.20075777173042
time_elpased: 1.459
start validation test
0.574072164948
0.616669343183
0.39522437217
0.481716113655
0.57436765903
76.884
batch start
#iterations: 360
currently lose_sum: 70.58590838313103
time_elpased: 1.452
batch start
#iterations: 361
currently lose_sum: 70.5475994348526
time_elpased: 1.481
batch start
#iterations: 362
currently lose_sum: 70.49294877052307
time_elpased: 1.415
batch start
#iterations: 363
currently lose_sum: 70.8099253475666
time_elpased: 1.421
batch start
#iterations: 364
currently lose_sum: 71.03888872265816
time_elpased: 1.463
batch start
#iterations: 365
currently lose_sum: 70.2500328719616
time_elpased: 1.446
batch start
#iterations: 366
currently lose_sum: 70.89907446503639
time_elpased: 1.481
batch start
#iterations: 367
currently lose_sum: 70.02059003710747
time_elpased: 1.471
batch start
#iterations: 368
currently lose_sum: 70.6351004242897
time_elpased: 1.436
batch start
#iterations: 369
currently lose_sum: 69.79911375045776
time_elpased: 1.424
batch start
#iterations: 370
currently lose_sum: 70.03379824757576
time_elpased: 1.421
batch start
#iterations: 371
currently lose_sum: 70.4512910246849
time_elpased: 1.424
batch start
#iterations: 372
currently lose_sum: 71.27302953600883
time_elpased: 1.426
batch start
#iterations: 373
currently lose_sum: 70.24930647015572
time_elpased: 1.456
batch start
#iterations: 374
currently lose_sum: 70.49195674061775
time_elpased: 1.438
batch start
#iterations: 375
currently lose_sum: 70.17081731557846
time_elpased: 1.432
batch start
#iterations: 376
currently lose_sum: 69.98114910721779
time_elpased: 1.474
batch start
#iterations: 377
currently lose_sum: 70.27415210008621
time_elpased: 1.442
batch start
#iterations: 378
currently lose_sum: 70.18652322888374
time_elpased: 1.441
batch start
#iterations: 379
currently lose_sum: 69.96935492753983
time_elpased: 1.429
start validation test
0.56912371134
0.620450914255
0.359715932483
0.455404260864
0.569469696931
79.415
batch start
#iterations: 380
currently lose_sum: 69.47723281383514
time_elpased: 1.449
batch start
#iterations: 381
currently lose_sum: 70.18953341245651
time_elpased: 1.448
batch start
#iterations: 382
currently lose_sum: 70.66339659690857
time_elpased: 1.42
batch start
#iterations: 383
currently lose_sum: 69.76172751188278
time_elpased: 1.443
batch start
#iterations: 384
currently lose_sum: 69.74954205751419
time_elpased: 1.443
batch start
#iterations: 385
currently lose_sum: 69.52976804971695
time_elpased: 1.414
batch start
#iterations: 386
currently lose_sum: 70.35267233848572
time_elpased: 1.427
batch start
#iterations: 387
currently lose_sum: 69.8042862713337
time_elpased: 1.459
batch start
#iterations: 388
currently lose_sum: 69.75418055057526
time_elpased: 1.422
batch start
#iterations: 389
currently lose_sum: 69.76332187652588
time_elpased: 1.414
batch start
#iterations: 390
currently lose_sum: 69.89700093865395
time_elpased: 1.471
batch start
#iterations: 391
currently lose_sum: 69.50407966971397
time_elpased: 1.434
batch start
#iterations: 392
currently lose_sum: 69.21342822909355
time_elpased: 1.425
batch start
#iterations: 393
currently lose_sum: 69.74519702792168
time_elpased: 1.428
batch start
#iterations: 394
currently lose_sum: 69.2513462305069
time_elpased: 1.432
batch start
#iterations: 395
currently lose_sum: 70.08668464422226
time_elpased: 1.453
batch start
#iterations: 396
currently lose_sum: 68.6823813021183
time_elpased: 1.435
batch start
#iterations: 397
currently lose_sum: 69.35533824563026
time_elpased: 1.429
batch start
#iterations: 398
currently lose_sum: 69.95047008991241
time_elpased: 1.475
batch start
#iterations: 399
currently lose_sum: 69.1891677081585
time_elpased: 1.44
start validation test
0.578195876289
0.624532900081
0.395636064224
0.484405519501
0.578497503405
77.611
acc: 0.635
pre: 0.645
rec: 0.604
F1: 0.624
auc: 0.686
