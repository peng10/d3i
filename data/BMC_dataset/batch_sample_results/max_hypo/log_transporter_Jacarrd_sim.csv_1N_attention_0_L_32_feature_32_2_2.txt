start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.53139954805374
time_elpased: 1.411
batch start
#iterations: 1
currently lose_sum: 100.19729614257812
time_elpased: 1.356
batch start
#iterations: 2
currently lose_sum: 100.1355499625206
time_elpased: 1.378
batch start
#iterations: 3
currently lose_sum: 100.12798178195953
time_elpased: 1.364
batch start
#iterations: 4
currently lose_sum: 99.89163595438004
time_elpased: 1.371
batch start
#iterations: 5
currently lose_sum: 99.93519723415375
time_elpased: 1.372
batch start
#iterations: 6
currently lose_sum: 99.75615215301514
time_elpased: 1.384
batch start
#iterations: 7
currently lose_sum: 99.78092294931412
time_elpased: 1.432
batch start
#iterations: 8
currently lose_sum: 99.63688915967941
time_elpased: 1.407
batch start
#iterations: 9
currently lose_sum: 99.57040214538574
time_elpased: 1.409
batch start
#iterations: 10
currently lose_sum: 99.60428434610367
time_elpased: 1.358
batch start
#iterations: 11
currently lose_sum: 99.7726538181305
time_elpased: 1.373
batch start
#iterations: 12
currently lose_sum: 99.636274933815
time_elpased: 1.367
batch start
#iterations: 13
currently lose_sum: 99.72387731075287
time_elpased: 1.368
batch start
#iterations: 14
currently lose_sum: 99.61268371343613
time_elpased: 1.402
batch start
#iterations: 15
currently lose_sum: 99.48953753709793
time_elpased: 1.38
batch start
#iterations: 16
currently lose_sum: 99.40560138225555
time_elpased: 1.377
batch start
#iterations: 17
currently lose_sum: 99.55137395858765
time_elpased: 1.367
batch start
#iterations: 18
currently lose_sum: 99.41888159513474
time_elpased: 1.363
batch start
#iterations: 19
currently lose_sum: 99.20533847808838
time_elpased: 1.361
start validation test
0.583298969072
0.608102740633
0.472676752084
0.531905037638
0.583493183436
65.597
batch start
#iterations: 20
currently lose_sum: 99.34097373485565
time_elpased: 1.375
batch start
#iterations: 21
currently lose_sum: 99.29838371276855
time_elpased: 1.361
batch start
#iterations: 22
currently lose_sum: 99.44569993019104
time_elpased: 1.372
batch start
#iterations: 23
currently lose_sum: 99.44846200942993
time_elpased: 1.4
batch start
#iterations: 24
currently lose_sum: 99.21224892139435
time_elpased: 1.35
batch start
#iterations: 25
currently lose_sum: 99.19151878356934
time_elpased: 1.373
batch start
#iterations: 26
currently lose_sum: 99.30368262529373
time_elpased: 1.38
batch start
#iterations: 27
currently lose_sum: 99.09382027387619
time_elpased: 1.35
batch start
#iterations: 28
currently lose_sum: 98.99997806549072
time_elpased: 1.398
batch start
#iterations: 29
currently lose_sum: 99.09358763694763
time_elpased: 1.381
batch start
#iterations: 30
currently lose_sum: 99.12395858764648
time_elpased: 1.368
batch start
#iterations: 31
currently lose_sum: 98.99977415800095
time_elpased: 1.392
batch start
#iterations: 32
currently lose_sum: 99.00008302927017
time_elpased: 1.361
batch start
#iterations: 33
currently lose_sum: 98.89810585975647
time_elpased: 1.366
batch start
#iterations: 34
currently lose_sum: 98.90511393547058
time_elpased: 1.433
batch start
#iterations: 35
currently lose_sum: 98.9392232298851
time_elpased: 1.392
batch start
#iterations: 36
currently lose_sum: 98.99954116344452
time_elpased: 1.364
batch start
#iterations: 37
currently lose_sum: 98.92168778181076
time_elpased: 1.366
batch start
#iterations: 38
currently lose_sum: 98.96902531385422
time_elpased: 1.375
batch start
#iterations: 39
currently lose_sum: 98.96487319469452
time_elpased: 1.401
start validation test
0.584020618557
0.619330531807
0.439847689616
0.514381995427
0.584273736371
65.462
batch start
#iterations: 40
currently lose_sum: 98.81429427862167
time_elpased: 1.374
batch start
#iterations: 41
currently lose_sum: 98.94531381130219
time_elpased: 1.361
batch start
#iterations: 42
currently lose_sum: 98.67342585325241
time_elpased: 1.363
batch start
#iterations: 43
currently lose_sum: 98.7811131477356
time_elpased: 1.365
batch start
#iterations: 44
currently lose_sum: 98.89057445526123
time_elpased: 1.424
batch start
#iterations: 45
currently lose_sum: 98.91309416294098
time_elpased: 1.372
batch start
#iterations: 46
currently lose_sum: 98.49532198905945
time_elpased: 1.366
batch start
#iterations: 47
currently lose_sum: 98.7730763554573
time_elpased: 1.359
batch start
#iterations: 48
currently lose_sum: 98.80467796325684
time_elpased: 1.385
batch start
#iterations: 49
currently lose_sum: 98.55572420358658
time_elpased: 1.368
batch start
#iterations: 50
currently lose_sum: 98.64071470499039
time_elpased: 1.408
batch start
#iterations: 51
currently lose_sum: 98.46271646022797
time_elpased: 1.364
batch start
#iterations: 52
currently lose_sum: 98.710504591465
time_elpased: 1.385
batch start
#iterations: 53
currently lose_sum: 98.61590760946274
time_elpased: 1.385
batch start
#iterations: 54
currently lose_sum: 98.66481518745422
time_elpased: 1.429
batch start
#iterations: 55
currently lose_sum: 98.54321748018265
time_elpased: 1.366
batch start
#iterations: 56
currently lose_sum: 98.54003232717514
time_elpased: 1.371
batch start
#iterations: 57
currently lose_sum: 98.6987379193306
time_elpased: 1.38
batch start
#iterations: 58
currently lose_sum: 98.64925664663315
time_elpased: 1.385
batch start
#iterations: 59
currently lose_sum: 98.56622463464737
time_elpased: 1.366
start validation test
0.581391752577
0.628875968992
0.400740969435
0.48953422591
0.581708912891
65.630
batch start
#iterations: 60
currently lose_sum: 98.30198842287064
time_elpased: 1.377
batch start
#iterations: 61
currently lose_sum: 98.53678822517395
time_elpased: 1.369
batch start
#iterations: 62
currently lose_sum: 98.52122294902802
time_elpased: 1.373
batch start
#iterations: 63
currently lose_sum: 98.82256573438644
time_elpased: 1.354
batch start
#iterations: 64
currently lose_sum: 98.48749762773514
time_elpased: 1.348
batch start
#iterations: 65
currently lose_sum: 98.41931372880936
time_elpased: 1.381
batch start
#iterations: 66
currently lose_sum: 98.11900544166565
time_elpased: 1.37
batch start
#iterations: 67
currently lose_sum: 98.40694457292557
time_elpased: 1.389
batch start
#iterations: 68
currently lose_sum: 98.46502792835236
time_elpased: 1.368
batch start
#iterations: 69
currently lose_sum: 98.3774237036705
time_elpased: 1.375
batch start
#iterations: 70
currently lose_sum: 98.44803750514984
time_elpased: 1.369
batch start
#iterations: 71
currently lose_sum: 98.39081853628159
time_elpased: 1.4
batch start
#iterations: 72
currently lose_sum: 98.42034465074539
time_elpased: 1.368
batch start
#iterations: 73
currently lose_sum: 98.2168180346489
time_elpased: 1.38
batch start
#iterations: 74
currently lose_sum: 98.34338456392288
time_elpased: 1.398
batch start
#iterations: 75
currently lose_sum: 98.32064604759216
time_elpased: 1.382
batch start
#iterations: 76
currently lose_sum: 98.28773325681686
time_elpased: 1.393
batch start
#iterations: 77
currently lose_sum: 98.75351244211197
time_elpased: 1.41
batch start
#iterations: 78
currently lose_sum: 98.23525166511536
time_elpased: 1.368
batch start
#iterations: 79
currently lose_sum: 98.50881665945053
time_elpased: 1.405
start validation test
0.587783505155
0.612801678909
0.480806833385
0.538838590623
0.587971319202
65.279
batch start
#iterations: 80
currently lose_sum: 98.3198105096817
time_elpased: 1.386
batch start
#iterations: 81
currently lose_sum: 98.24701219797134
time_elpased: 1.388
batch start
#iterations: 82
currently lose_sum: 98.4185466170311
time_elpased: 1.377
batch start
#iterations: 83
currently lose_sum: 98.18053317070007
time_elpased: 1.376
batch start
#iterations: 84
currently lose_sum: 98.29282826185226
time_elpased: 1.413
batch start
#iterations: 85
currently lose_sum: 98.1453503370285
time_elpased: 1.389
batch start
#iterations: 86
currently lose_sum: 98.15019249916077
time_elpased: 1.349
batch start
#iterations: 87
currently lose_sum: 98.19596284627914
time_elpased: 1.376
batch start
#iterations: 88
currently lose_sum: 98.18746900558472
time_elpased: 1.384
batch start
#iterations: 89
currently lose_sum: 98.18179833889008
time_elpased: 1.376
batch start
#iterations: 90
currently lose_sum: 98.3417381644249
time_elpased: 1.395
batch start
#iterations: 91
currently lose_sum: 98.1981989145279
time_elpased: 1.392
batch start
#iterations: 92
currently lose_sum: 98.06818413734436
time_elpased: 1.362
batch start
#iterations: 93
currently lose_sum: 98.0384738445282
time_elpased: 1.355
batch start
#iterations: 94
currently lose_sum: 98.23845106363297
time_elpased: 1.37
batch start
#iterations: 95
currently lose_sum: 98.21486538648605
time_elpased: 1.367
batch start
#iterations: 96
currently lose_sum: 97.9289259314537
time_elpased: 1.366
batch start
#iterations: 97
currently lose_sum: 97.84798032045364
time_elpased: 1.411
batch start
#iterations: 98
currently lose_sum: 98.00970947742462
time_elpased: 1.43
batch start
#iterations: 99
currently lose_sum: 97.82934665679932
time_elpased: 1.382
start validation test
0.582525773196
0.605475880052
0.477925285582
0.534192212573
0.582709415485
65.418
batch start
#iterations: 100
currently lose_sum: 97.85694259405136
time_elpased: 1.369
batch start
#iterations: 101
currently lose_sum: 98.1197401881218
time_elpased: 1.357
batch start
#iterations: 102
currently lose_sum: 98.32924836874008
time_elpased: 1.378
batch start
#iterations: 103
currently lose_sum: 97.86376976966858
time_elpased: 1.383
batch start
#iterations: 104
currently lose_sum: 98.04078906774521
time_elpased: 1.381
batch start
#iterations: 105
currently lose_sum: 97.8101469874382
time_elpased: 1.384
batch start
#iterations: 106
currently lose_sum: 97.85511922836304
time_elpased: 1.374
batch start
#iterations: 107
currently lose_sum: 97.75125426054001
time_elpased: 1.371
batch start
#iterations: 108
currently lose_sum: 98.10174262523651
time_elpased: 1.351
batch start
#iterations: 109
currently lose_sum: 97.92113363742828
time_elpased: 1.386
batch start
#iterations: 110
currently lose_sum: 97.83784890174866
time_elpased: 1.393
batch start
#iterations: 111
currently lose_sum: 97.59636896848679
time_elpased: 1.384
batch start
#iterations: 112
currently lose_sum: 97.8500012755394
time_elpased: 1.395
batch start
#iterations: 113
currently lose_sum: 97.56522780656815
time_elpased: 1.366
batch start
#iterations: 114
currently lose_sum: 97.70495736598969
time_elpased: 1.351
batch start
#iterations: 115
currently lose_sum: 97.65672564506531
time_elpased: 1.393
batch start
#iterations: 116
currently lose_sum: 97.71296948194504
time_elpased: 1.448
batch start
#iterations: 117
currently lose_sum: 97.24545335769653
time_elpased: 1.394
batch start
#iterations: 118
currently lose_sum: 97.92202353477478
time_elpased: 1.363
batch start
#iterations: 119
currently lose_sum: 98.06133478879929
time_elpased: 1.349
start validation test
0.578711340206
0.618732697631
0.414016671812
0.496084838769
0.579000487099
65.628
batch start
#iterations: 120
currently lose_sum: 97.77314114570618
time_elpased: 1.375
batch start
#iterations: 121
currently lose_sum: 97.40253102779388
time_elpased: 1.364
batch start
#iterations: 122
currently lose_sum: 97.96343272924423
time_elpased: 1.379
batch start
#iterations: 123
currently lose_sum: 97.6009111404419
time_elpased: 1.405
batch start
#iterations: 124
currently lose_sum: 97.72397500276566
time_elpased: 1.465
batch start
#iterations: 125
currently lose_sum: 97.69571596384048
time_elpased: 1.408
batch start
#iterations: 126
currently lose_sum: 97.51409584283829
time_elpased: 1.432
batch start
#iterations: 127
currently lose_sum: 97.57467466592789
time_elpased: 1.451
batch start
#iterations: 128
currently lose_sum: 97.5918550491333
time_elpased: 1.466
batch start
#iterations: 129
currently lose_sum: 97.64320284128189
time_elpased: 1.383
batch start
#iterations: 130
currently lose_sum: 97.58923780918121
time_elpased: 1.357
batch start
#iterations: 131
currently lose_sum: 97.5966939330101
time_elpased: 1.364
batch start
#iterations: 132
currently lose_sum: 97.83661168813705
time_elpased: 1.378
batch start
#iterations: 133
currently lose_sum: 97.59024608135223
time_elpased: 1.386
batch start
#iterations: 134
currently lose_sum: 97.69022858142853
time_elpased: 1.413
batch start
#iterations: 135
currently lose_sum: 97.26813423633575
time_elpased: 1.366
batch start
#iterations: 136
currently lose_sum: 97.29044967889786
time_elpased: 1.361
batch start
#iterations: 137
currently lose_sum: 97.58571648597717
time_elpased: 1.449
batch start
#iterations: 138
currently lose_sum: 97.37947708368301
time_elpased: 1.367
batch start
#iterations: 139
currently lose_sum: 97.34517532587051
time_elpased: 1.408
start validation test
0.58293814433
0.608631747728
0.468766080066
0.529620370909
0.583138590999
65.475
batch start
#iterations: 140
currently lose_sum: 97.2310202717781
time_elpased: 1.403
batch start
#iterations: 141
currently lose_sum: 97.66285842657089
time_elpased: 1.357
batch start
#iterations: 142
currently lose_sum: 97.14387112855911
time_elpased: 1.372
batch start
#iterations: 143
currently lose_sum: 97.75684583187103
time_elpased: 1.361
batch start
#iterations: 144
currently lose_sum: 97.32503378391266
time_elpased: 1.381
batch start
#iterations: 145
currently lose_sum: 97.47005188465118
time_elpased: 1.399
batch start
#iterations: 146
currently lose_sum: 97.1856170296669
time_elpased: 1.369
batch start
#iterations: 147
currently lose_sum: 97.11821162700653
time_elpased: 1.37
batch start
#iterations: 148
currently lose_sum: 97.02637135982513
time_elpased: 1.368
batch start
#iterations: 149
currently lose_sum: 97.41222113370895
time_elpased: 1.379
batch start
#iterations: 150
currently lose_sum: 97.2050753235817
time_elpased: 1.389
batch start
#iterations: 151
currently lose_sum: 97.27083909511566
time_elpased: 1.368
batch start
#iterations: 152
currently lose_sum: 97.13682979345322
time_elpased: 1.375
batch start
#iterations: 153
currently lose_sum: 97.2018254995346
time_elpased: 1.375
batch start
#iterations: 154
currently lose_sum: 97.36703771352768
time_elpased: 1.354
batch start
#iterations: 155
currently lose_sum: 97.15502732992172
time_elpased: 1.387
batch start
#iterations: 156
currently lose_sum: 97.02890491485596
time_elpased: 1.392
batch start
#iterations: 157
currently lose_sum: 96.90725272893906
time_elpased: 1.373
batch start
#iterations: 158
currently lose_sum: 96.99911212921143
time_elpased: 1.357
batch start
#iterations: 159
currently lose_sum: 97.12021726369858
time_elpased: 1.365
start validation test
0.586494845361
0.604642548463
0.503962128229
0.549730579255
0.586639744276
65.477
batch start
#iterations: 160
currently lose_sum: 97.13368946313858
time_elpased: 1.444
batch start
#iterations: 161
currently lose_sum: 96.91426932811737
time_elpased: 1.373
batch start
#iterations: 162
currently lose_sum: 97.1594825387001
time_elpased: 1.399
batch start
#iterations: 163
currently lose_sum: 97.12325286865234
time_elpased: 1.367
batch start
#iterations: 164
currently lose_sum: 96.77882772684097
time_elpased: 1.372
batch start
#iterations: 165
currently lose_sum: 96.95743203163147
time_elpased: 1.385
batch start
#iterations: 166
currently lose_sum: 96.81713783740997
time_elpased: 1.375
batch start
#iterations: 167
currently lose_sum: 97.19403129816055
time_elpased: 1.366
batch start
#iterations: 168
currently lose_sum: 96.83392333984375
time_elpased: 1.377
batch start
#iterations: 169
currently lose_sum: 97.3070832490921
time_elpased: 1.38
batch start
#iterations: 170
currently lose_sum: 97.0348191857338
time_elpased: 1.365
batch start
#iterations: 171
currently lose_sum: 97.18710005283356
time_elpased: 1.37
batch start
#iterations: 172
currently lose_sum: 96.7312622666359
time_elpased: 1.434
batch start
#iterations: 173
currently lose_sum: 97.14584976434708
time_elpased: 1.385
batch start
#iterations: 174
currently lose_sum: 96.92702805995941
time_elpased: 1.359
batch start
#iterations: 175
currently lose_sum: 96.7025545835495
time_elpased: 1.403
batch start
#iterations: 176
currently lose_sum: 96.74433273077011
time_elpased: 1.405
batch start
#iterations: 177
currently lose_sum: 96.86056864261627
time_elpased: 1.374
batch start
#iterations: 178
currently lose_sum: 96.94291722774506
time_elpased: 1.364
batch start
#iterations: 179
currently lose_sum: 96.56713485717773
time_elpased: 1.389
start validation test
0.585412371134
0.613630192778
0.465164145312
0.529181057191
0.585623485441
65.615
batch start
#iterations: 180
currently lose_sum: 96.46201479434967
time_elpased: 1.375
batch start
#iterations: 181
currently lose_sum: 97.01841115951538
time_elpased: 1.353
batch start
#iterations: 182
currently lose_sum: 97.02944123744965
time_elpased: 1.382
batch start
#iterations: 183
currently lose_sum: 96.46735578775406
time_elpased: 1.367
batch start
#iterations: 184
currently lose_sum: 96.75724333524704
time_elpased: 1.359
batch start
#iterations: 185
currently lose_sum: 96.83114778995514
time_elpased: 1.369
batch start
#iterations: 186
currently lose_sum: 96.62001484632492
time_elpased: 1.356
batch start
#iterations: 187
currently lose_sum: 96.58697885274887
time_elpased: 1.415
batch start
#iterations: 188
currently lose_sum: 96.8050827383995
time_elpased: 1.366
batch start
#iterations: 189
currently lose_sum: 96.87333691120148
time_elpased: 1.381
batch start
#iterations: 190
currently lose_sum: 96.83895599842072
time_elpased: 1.388
batch start
#iterations: 191
currently lose_sum: 96.31550222635269
time_elpased: 1.368
batch start
#iterations: 192
currently lose_sum: 96.47852873802185
time_elpased: 1.381
batch start
#iterations: 193
currently lose_sum: 96.65558272600174
time_elpased: 1.417
batch start
#iterations: 194
currently lose_sum: 96.34875667095184
time_elpased: 1.383
batch start
#iterations: 195
currently lose_sum: 96.69103991985321
time_elpased: 1.382
batch start
#iterations: 196
currently lose_sum: 96.49769872426987
time_elpased: 1.36
batch start
#iterations: 197
currently lose_sum: 96.52889204025269
time_elpased: 1.455
batch start
#iterations: 198
currently lose_sum: 96.62971663475037
time_elpased: 1.372
batch start
#iterations: 199
currently lose_sum: 96.8801526427269
time_elpased: 1.376
start validation test
0.583917525773
0.613370089593
0.457960275805
0.524393118077
0.584138663153
65.726
batch start
#iterations: 200
currently lose_sum: 96.79498380422592
time_elpased: 1.367
batch start
#iterations: 201
currently lose_sum: 96.5983138680458
time_elpased: 1.431
batch start
#iterations: 202
currently lose_sum: 96.48029363155365
time_elpased: 1.424
batch start
#iterations: 203
currently lose_sum: 96.2188315987587
time_elpased: 1.404
batch start
#iterations: 204
currently lose_sum: 96.3385066986084
time_elpased: 1.386
batch start
#iterations: 205
currently lose_sum: 96.5252816081047
time_elpased: 1.441
batch start
#iterations: 206
currently lose_sum: 96.40159577131271
time_elpased: 1.372
batch start
#iterations: 207
currently lose_sum: 96.64297837018967
time_elpased: 1.366
batch start
#iterations: 208
currently lose_sum: 96.45747703313828
time_elpased: 1.361
batch start
#iterations: 209
currently lose_sum: 96.2729474902153
time_elpased: 1.376
batch start
#iterations: 210
currently lose_sum: 96.48469883203506
time_elpased: 1.379
batch start
#iterations: 211
currently lose_sum: 96.39879566431046
time_elpased: 1.397
batch start
#iterations: 212
currently lose_sum: 96.16641587018967
time_elpased: 1.361
batch start
#iterations: 213
currently lose_sum: 96.2313842177391
time_elpased: 1.442
batch start
#iterations: 214
currently lose_sum: 96.5268497467041
time_elpased: 1.361
batch start
#iterations: 215
currently lose_sum: 96.53608936071396
time_elpased: 1.384
batch start
#iterations: 216
currently lose_sum: 96.3201397061348
time_elpased: 1.388
batch start
#iterations: 217
currently lose_sum: 96.16286820173264
time_elpased: 1.374
batch start
#iterations: 218
currently lose_sum: 95.96870875358582
time_elpased: 1.388
batch start
#iterations: 219
currently lose_sum: 96.28243792057037
time_elpased: 1.387
start validation test
0.577113402062
0.614742909146
0.417104044458
0.496995708155
0.577394323169
66.398
batch start
#iterations: 220
currently lose_sum: 96.4450426697731
time_elpased: 1.375
batch start
#iterations: 221
currently lose_sum: 96.07378482818604
time_elpased: 1.407
batch start
#iterations: 222
currently lose_sum: 96.3871101140976
time_elpased: 1.36
batch start
#iterations: 223
currently lose_sum: 96.38958513736725
time_elpased: 1.384
batch start
#iterations: 224
currently lose_sum: 96.15139073133469
time_elpased: 1.384
batch start
#iterations: 225
currently lose_sum: 95.95155304670334
time_elpased: 1.369
batch start
#iterations: 226
currently lose_sum: 96.33991426229477
time_elpased: 1.397
batch start
#iterations: 227
currently lose_sum: 95.86423790454865
time_elpased: 1.401
batch start
#iterations: 228
currently lose_sum: 96.22405850887299
time_elpased: 1.357
batch start
#iterations: 229
currently lose_sum: 96.31800299882889
time_elpased: 1.39
batch start
#iterations: 230
currently lose_sum: 96.02146792411804
time_elpased: 1.364
batch start
#iterations: 231
currently lose_sum: 96.06857287883759
time_elpased: 1.432
batch start
#iterations: 232
currently lose_sum: 95.88073027133942
time_elpased: 1.372
batch start
#iterations: 233
currently lose_sum: 95.82096660137177
time_elpased: 1.424
batch start
#iterations: 234
currently lose_sum: 95.79178285598755
time_elpased: 1.379
batch start
#iterations: 235
currently lose_sum: 96.11499720811844
time_elpased: 1.446
batch start
#iterations: 236
currently lose_sum: 96.04170632362366
time_elpased: 1.363
batch start
#iterations: 237
currently lose_sum: 95.97212117910385
time_elpased: 1.384
batch start
#iterations: 238
currently lose_sum: 96.10308104753494
time_elpased: 1.412
batch start
#iterations: 239
currently lose_sum: 96.05223780870438
time_elpased: 1.374
start validation test
0.579536082474
0.606295993459
0.457857363384
0.521723834653
0.579749708233
66.339
batch start
#iterations: 240
currently lose_sum: 95.81882947683334
time_elpased: 1.353
batch start
#iterations: 241
currently lose_sum: 96.06787174940109
time_elpased: 1.381
batch start
#iterations: 242
currently lose_sum: 95.76573020219803
time_elpased: 1.389
batch start
#iterations: 243
currently lose_sum: 95.89778232574463
time_elpased: 1.383
batch start
#iterations: 244
currently lose_sum: 96.02571839094162
time_elpased: 1.359
batch start
#iterations: 245
currently lose_sum: 96.0332260131836
time_elpased: 1.391
batch start
#iterations: 246
currently lose_sum: 95.84302031993866
time_elpased: 1.374
batch start
#iterations: 247
currently lose_sum: 95.82527947425842
time_elpased: 1.373
batch start
#iterations: 248
currently lose_sum: 95.95696151256561
time_elpased: 1.364
batch start
#iterations: 249
currently lose_sum: 95.77696114778519
time_elpased: 1.389
batch start
#iterations: 250
currently lose_sum: 95.63838011026382
time_elpased: 1.374
batch start
#iterations: 251
currently lose_sum: 95.69538015127182
time_elpased: 1.384
batch start
#iterations: 252
currently lose_sum: 95.77134430408478
time_elpased: 1.377
batch start
#iterations: 253
currently lose_sum: 95.83540540933609
time_elpased: 1.386
batch start
#iterations: 254
currently lose_sum: 95.63823533058167
time_elpased: 1.355
batch start
#iterations: 255
currently lose_sum: 95.91119748353958
time_elpased: 1.378
batch start
#iterations: 256
currently lose_sum: 95.77674823999405
time_elpased: 1.369
batch start
#iterations: 257
currently lose_sum: 95.80176591873169
time_elpased: 1.379
batch start
#iterations: 258
currently lose_sum: 95.77346384525299
time_elpased: 1.348
batch start
#iterations: 259
currently lose_sum: 95.68782985210419
time_elpased: 1.403
start validation test
0.582577319588
0.615692439617
0.443346711948
0.515495991384
0.582821760394
66.310
batch start
#iterations: 260
currently lose_sum: 95.90872836112976
time_elpased: 1.393
batch start
#iterations: 261
currently lose_sum: 95.39617031812668
time_elpased: 1.369
batch start
#iterations: 262
currently lose_sum: 95.39893877506256
time_elpased: 1.359
batch start
#iterations: 263
currently lose_sum: 95.82350468635559
time_elpased: 1.394
batch start
#iterations: 264
currently lose_sum: 95.50230664014816
time_elpased: 1.362
batch start
#iterations: 265
currently lose_sum: 95.1984726190567
time_elpased: 1.375
batch start
#iterations: 266
currently lose_sum: 95.58430135250092
time_elpased: 1.381
batch start
#iterations: 267
currently lose_sum: 95.81650644540787
time_elpased: 1.41
batch start
#iterations: 268
currently lose_sum: 95.75250208377838
time_elpased: 1.361
batch start
#iterations: 269
currently lose_sum: 95.98021638393402
time_elpased: 1.388
batch start
#iterations: 270
currently lose_sum: 95.79946208000183
time_elpased: 1.364
batch start
#iterations: 271
currently lose_sum: 95.41392999887466
time_elpased: 1.399
batch start
#iterations: 272
currently lose_sum: 95.47453558444977
time_elpased: 1.381
batch start
#iterations: 273
currently lose_sum: 95.39990556240082
time_elpased: 1.367
batch start
#iterations: 274
currently lose_sum: 96.00132519006729
time_elpased: 1.375
batch start
#iterations: 275
currently lose_sum: 95.78862529993057
time_elpased: 1.357
batch start
#iterations: 276
currently lose_sum: 95.46718072891235
time_elpased: 1.389
batch start
#iterations: 277
currently lose_sum: 95.58660244941711
time_elpased: 1.394
batch start
#iterations: 278
currently lose_sum: 95.28609770536423
time_elpased: 1.356
batch start
#iterations: 279
currently lose_sum: 95.75697559118271
time_elpased: 1.415
start validation test
0.575360824742
0.606265267998
0.434187506432
0.505996641881
0.575608676277
66.755
batch start
#iterations: 280
currently lose_sum: 95.5684180855751
time_elpased: 1.383
batch start
#iterations: 281
currently lose_sum: 95.08894383907318
time_elpased: 1.403
batch start
#iterations: 282
currently lose_sum: 95.59970545768738
time_elpased: 1.373
batch start
#iterations: 283
currently lose_sum: 95.29944890737534
time_elpased: 1.403
batch start
#iterations: 284
currently lose_sum: 95.67024713754654
time_elpased: 1.349
batch start
#iterations: 285
currently lose_sum: 95.5802817940712
time_elpased: 1.373
batch start
#iterations: 286
currently lose_sum: 95.43817830085754
time_elpased: 1.367
batch start
#iterations: 287
currently lose_sum: 95.61836385726929
time_elpased: 1.417
batch start
#iterations: 288
currently lose_sum: 95.47156023979187
time_elpased: 1.344
batch start
#iterations: 289
currently lose_sum: 95.33202463388443
time_elpased: 1.356
batch start
#iterations: 290
currently lose_sum: 95.19895792007446
time_elpased: 1.362
batch start
#iterations: 291
currently lose_sum: 95.3375449180603
time_elpased: 1.383
batch start
#iterations: 292
currently lose_sum: 94.8939677476883
time_elpased: 1.36
batch start
#iterations: 293
currently lose_sum: 95.34174972772598
time_elpased: 1.392
batch start
#iterations: 294
currently lose_sum: 95.56934124231339
time_elpased: 1.357
batch start
#iterations: 295
currently lose_sum: 95.46829450130463
time_elpased: 1.389
batch start
#iterations: 296
currently lose_sum: 95.18153721094131
time_elpased: 1.373
batch start
#iterations: 297
currently lose_sum: 95.33644890785217
time_elpased: 1.346
batch start
#iterations: 298
currently lose_sum: 95.24794620275497
time_elpased: 1.364
batch start
#iterations: 299
currently lose_sum: 95.5504344701767
time_elpased: 1.365
start validation test
0.579020618557
0.610714285714
0.439950602038
0.511455404678
0.579264777421
66.922
batch start
#iterations: 300
currently lose_sum: 95.46336609125137
time_elpased: 1.36
batch start
#iterations: 301
currently lose_sum: 95.55192106962204
time_elpased: 1.358
batch start
#iterations: 302
currently lose_sum: 95.21512550115585
time_elpased: 1.357
batch start
#iterations: 303
currently lose_sum: 95.64896810054779
time_elpased: 1.348
batch start
#iterations: 304
currently lose_sum: 95.31439554691315
time_elpased: 1.377
batch start
#iterations: 305
currently lose_sum: 94.91037285327911
time_elpased: 1.375
batch start
#iterations: 306
currently lose_sum: 94.95798200368881
time_elpased: 1.388
batch start
#iterations: 307
currently lose_sum: 95.49853456020355
time_elpased: 1.361
batch start
#iterations: 308
currently lose_sum: 95.01723921298981
time_elpased: 1.363
batch start
#iterations: 309
currently lose_sum: 95.09940898418427
time_elpased: 1.361
batch start
#iterations: 310
currently lose_sum: 94.99110335111618
time_elpased: 1.379
batch start
#iterations: 311
currently lose_sum: 95.10398679971695
time_elpased: 1.393
batch start
#iterations: 312
currently lose_sum: 95.26289796829224
time_elpased: 1.354
batch start
#iterations: 313
currently lose_sum: 94.9526754617691
time_elpased: 1.385
batch start
#iterations: 314
currently lose_sum: 95.03096705675125
time_elpased: 1.391
batch start
#iterations: 315
currently lose_sum: 94.74914115667343
time_elpased: 1.385
batch start
#iterations: 316
currently lose_sum: 94.91897058486938
time_elpased: 1.341
batch start
#iterations: 317
currently lose_sum: 95.25345885753632
time_elpased: 1.368
batch start
#iterations: 318
currently lose_sum: 95.14151179790497
time_elpased: 1.361
batch start
#iterations: 319
currently lose_sum: 95.39995497465134
time_elpased: 1.358
start validation test
0.57881443299
0.606886061947
0.451682618092
0.51790666116
0.579037632499
66.860
batch start
#iterations: 320
currently lose_sum: 94.95723217725754
time_elpased: 1.397
batch start
#iterations: 321
currently lose_sum: 95.2821673154831
time_elpased: 1.375
batch start
#iterations: 322
currently lose_sum: 95.08199369907379
time_elpased: 1.339
batch start
#iterations: 323
currently lose_sum: 94.97235870361328
time_elpased: 1.376
batch start
#iterations: 324
currently lose_sum: 95.45004838705063
time_elpased: 1.395
batch start
#iterations: 325
currently lose_sum: 95.30205065011978
time_elpased: 1.363
batch start
#iterations: 326
currently lose_sum: 94.79781252145767
time_elpased: 1.365
batch start
#iterations: 327
currently lose_sum: 94.98075735569
time_elpased: 1.348
batch start
#iterations: 328
currently lose_sum: 95.21867942810059
time_elpased: 1.363
batch start
#iterations: 329
currently lose_sum: 94.99946224689484
time_elpased: 1.367
batch start
#iterations: 330
currently lose_sum: 95.02103340625763
time_elpased: 1.366
batch start
#iterations: 331
currently lose_sum: 95.11433577537537
time_elpased: 1.372
batch start
#iterations: 332
currently lose_sum: 94.57587379217148
time_elpased: 1.367
batch start
#iterations: 333
currently lose_sum: 94.81710815429688
time_elpased: 1.384
batch start
#iterations: 334
currently lose_sum: 95.0358207821846
time_elpased: 1.368
batch start
#iterations: 335
currently lose_sum: 94.7685118317604
time_elpased: 1.386
batch start
#iterations: 336
currently lose_sum: 94.8835238814354
time_elpased: 1.355
batch start
#iterations: 337
currently lose_sum: 95.36511397361755
time_elpased: 1.387
batch start
#iterations: 338
currently lose_sum: 94.84678655862808
time_elpased: 1.371
batch start
#iterations: 339
currently lose_sum: 95.0646316409111
time_elpased: 1.373
start validation test
0.581237113402
0.610640366718
0.452403005043
0.519744620478
0.581463301551
66.801
batch start
#iterations: 340
currently lose_sum: 94.94858914613724
time_elpased: 1.366
batch start
#iterations: 341
currently lose_sum: 94.63114202022552
time_elpased: 1.364
batch start
#iterations: 342
currently lose_sum: 95.3895725607872
time_elpased: 1.387
batch start
#iterations: 343
currently lose_sum: 94.87544739246368
time_elpased: 1.366
batch start
#iterations: 344
currently lose_sum: 94.37612265348434
time_elpased: 1.372
batch start
#iterations: 345
currently lose_sum: 94.7484872341156
time_elpased: 1.363
batch start
#iterations: 346
currently lose_sum: 95.03319907188416
time_elpased: 1.374
batch start
#iterations: 347
currently lose_sum: 94.5685185790062
time_elpased: 1.352
batch start
#iterations: 348
currently lose_sum: 94.74836772680283
time_elpased: 1.367
batch start
#iterations: 349
currently lose_sum: 94.73523682355881
time_elpased: 1.395
batch start
#iterations: 350
currently lose_sum: 95.12382221221924
time_elpased: 1.369
batch start
#iterations: 351
currently lose_sum: 95.11058807373047
time_elpased: 1.366
batch start
#iterations: 352
currently lose_sum: 94.61336076259613
time_elpased: 1.403
batch start
#iterations: 353
currently lose_sum: 94.56063890457153
time_elpased: 1.371
batch start
#iterations: 354
currently lose_sum: 95.01403433084488
time_elpased: 1.371
batch start
#iterations: 355
currently lose_sum: 94.36601012945175
time_elpased: 1.382
batch start
#iterations: 356
currently lose_sum: 94.56607520580292
time_elpased: 1.361
batch start
#iterations: 357
currently lose_sum: 94.88409847021103
time_elpased: 1.356
batch start
#iterations: 358
currently lose_sum: 94.67331355810165
time_elpased: 1.395
batch start
#iterations: 359
currently lose_sum: 94.92209416627884
time_elpased: 1.361
start validation test
0.576546391753
0.607995398332
0.435113718226
0.507228120689
0.576794698625
67.444
batch start
#iterations: 360
currently lose_sum: 94.8378399014473
time_elpased: 1.371
batch start
#iterations: 361
currently lose_sum: 95.10996603965759
time_elpased: 1.359
batch start
#iterations: 362
currently lose_sum: 94.72125482559204
time_elpased: 1.372
batch start
#iterations: 363
currently lose_sum: 94.64900577068329
time_elpased: 1.348
batch start
#iterations: 364
currently lose_sum: 94.59082633256912
time_elpased: 1.4
batch start
#iterations: 365
currently lose_sum: 94.69210469722748
time_elpased: 1.368
batch start
#iterations: 366
currently lose_sum: 94.64323270320892
time_elpased: 1.362
batch start
#iterations: 367
currently lose_sum: 94.6416032910347
time_elpased: 1.36
batch start
#iterations: 368
currently lose_sum: 94.8993256688118
time_elpased: 1.364
batch start
#iterations: 369
currently lose_sum: 94.42153376340866
time_elpased: 1.385
batch start
#iterations: 370
currently lose_sum: 94.83862376213074
time_elpased: 1.373
batch start
#iterations: 371
currently lose_sum: 94.70966625213623
time_elpased: 1.396
batch start
#iterations: 372
currently lose_sum: 94.61096656322479
time_elpased: 1.391
batch start
#iterations: 373
currently lose_sum: 94.36390095949173
time_elpased: 1.381
batch start
#iterations: 374
currently lose_sum: 94.91372168064117
time_elpased: 1.354
batch start
#iterations: 375
currently lose_sum: 94.43957149982452
time_elpased: 1.358
batch start
#iterations: 376
currently lose_sum: 94.57943749427795
time_elpased: 1.442
batch start
#iterations: 377
currently lose_sum: 94.23847490549088
time_elpased: 1.421
batch start
#iterations: 378
currently lose_sum: 94.87310922145844
time_elpased: 1.366
batch start
#iterations: 379
currently lose_sum: 94.7957444190979
time_elpased: 1.402
start validation test
0.578762886598
0.607068607069
0.450756406298
0.517363571935
0.578987621718
67.245
batch start
#iterations: 380
currently lose_sum: 94.4596363902092
time_elpased: 1.379
batch start
#iterations: 381
currently lose_sum: 94.29411256313324
time_elpased: 1.358
batch start
#iterations: 382
currently lose_sum: 94.40456467866898
time_elpased: 1.346
batch start
#iterations: 383
currently lose_sum: 94.37497872114182
time_elpased: 1.358
batch start
#iterations: 384
currently lose_sum: 94.7028020620346
time_elpased: 1.375
batch start
#iterations: 385
currently lose_sum: 94.65888398885727
time_elpased: 1.389
batch start
#iterations: 386
currently lose_sum: 94.72941988706589
time_elpased: 1.36
batch start
#iterations: 387
currently lose_sum: 94.5425198674202
time_elpased: 1.38
batch start
#iterations: 388
currently lose_sum: 94.2874351143837
time_elpased: 1.379
batch start
#iterations: 389
currently lose_sum: 94.56630253791809
time_elpased: 1.365
batch start
#iterations: 390
currently lose_sum: 94.48687583208084
time_elpased: 1.358
batch start
#iterations: 391
currently lose_sum: 94.38778465986252
time_elpased: 1.374
batch start
#iterations: 392
currently lose_sum: 94.84349304437637
time_elpased: 1.353
batch start
#iterations: 393
currently lose_sum: 94.75805079936981
time_elpased: 1.363
batch start
#iterations: 394
currently lose_sum: 94.08296644687653
time_elpased: 1.391
batch start
#iterations: 395
currently lose_sum: 94.20864433050156
time_elpased: 1.36
batch start
#iterations: 396
currently lose_sum: 94.02628576755524
time_elpased: 1.358
batch start
#iterations: 397
currently lose_sum: 94.4290309548378
time_elpased: 1.35
batch start
#iterations: 398
currently lose_sum: 94.73892837762833
time_elpased: 1.427
batch start
#iterations: 399
currently lose_sum: 94.01881450414658
time_elpased: 1.451
start validation test
0.581855670103
0.619829774526
0.427189461768
0.505787742171
0.582127210487
67.244
acc: 0.586
pre: 0.612
rec: 0.474
F1: 0.534
auc: 0.613
