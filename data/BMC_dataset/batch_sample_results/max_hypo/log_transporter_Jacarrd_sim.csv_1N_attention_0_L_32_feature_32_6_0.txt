start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.29466652870178
time_elpased: 1.577
batch start
#iterations: 1
currently lose_sum: 100.31997871398926
time_elpased: 1.54
batch start
#iterations: 2
currently lose_sum: 100.09412330389023
time_elpased: 1.561
batch start
#iterations: 3
currently lose_sum: 100.20308011770248
time_elpased: 1.597
batch start
#iterations: 4
currently lose_sum: 100.02348911762238
time_elpased: 1.563
batch start
#iterations: 5
currently lose_sum: 100.0585572719574
time_elpased: 1.547
batch start
#iterations: 6
currently lose_sum: 99.93616044521332
time_elpased: 1.562
batch start
#iterations: 7
currently lose_sum: 99.7582077383995
time_elpased: 1.557
batch start
#iterations: 8
currently lose_sum: 99.81087374687195
time_elpased: 1.538
batch start
#iterations: 9
currently lose_sum: 99.774627327919
time_elpased: 1.571
batch start
#iterations: 10
currently lose_sum: 99.72813618183136
time_elpased: 1.551
batch start
#iterations: 11
currently lose_sum: 99.63961976766586
time_elpased: 1.593
batch start
#iterations: 12
currently lose_sum: 99.7413580417633
time_elpased: 1.522
batch start
#iterations: 13
currently lose_sum: 99.48200505971909
time_elpased: 1.557
batch start
#iterations: 14
currently lose_sum: 99.51737374067307
time_elpased: 1.575
batch start
#iterations: 15
currently lose_sum: 99.43770068883896
time_elpased: 1.615
batch start
#iterations: 16
currently lose_sum: 99.47575014829636
time_elpased: 1.568
batch start
#iterations: 17
currently lose_sum: 99.42732083797455
time_elpased: 1.567
batch start
#iterations: 18
currently lose_sum: 99.31662803888321
time_elpased: 1.55
batch start
#iterations: 19
currently lose_sum: 99.43032550811768
time_elpased: 1.593
start validation test
0.584381443299
0.5838062424
0.59287846043
0.588307378095
0.584366525475
65.561
batch start
#iterations: 20
currently lose_sum: 99.39612847566605
time_elpased: 1.57
batch start
#iterations: 21
currently lose_sum: 99.2368278503418
time_elpased: 1.58
batch start
#iterations: 22
currently lose_sum: 99.25804424285889
time_elpased: 1.549
batch start
#iterations: 23
currently lose_sum: 99.23838275671005
time_elpased: 1.541
batch start
#iterations: 24
currently lose_sum: 99.2806505560875
time_elpased: 1.612
batch start
#iterations: 25
currently lose_sum: 99.13865828514099
time_elpased: 1.547
batch start
#iterations: 26
currently lose_sum: 99.00194388628006
time_elpased: 1.559
batch start
#iterations: 27
currently lose_sum: 99.07596403360367
time_elpased: 1.549
batch start
#iterations: 28
currently lose_sum: 99.04153174161911
time_elpased: 1.608
batch start
#iterations: 29
currently lose_sum: 99.12245506048203
time_elpased: 1.547
batch start
#iterations: 30
currently lose_sum: 98.93234348297119
time_elpased: 1.554
batch start
#iterations: 31
currently lose_sum: 99.03262090682983
time_elpased: 1.585
batch start
#iterations: 32
currently lose_sum: 98.71638053655624
time_elpased: 1.597
batch start
#iterations: 33
currently lose_sum: 99.15569692850113
time_elpased: 1.585
batch start
#iterations: 34
currently lose_sum: 98.94763171672821
time_elpased: 1.535
batch start
#iterations: 35
currently lose_sum: 99.01348829269409
time_elpased: 1.559
batch start
#iterations: 36
currently lose_sum: 99.13417446613312
time_elpased: 1.56
batch start
#iterations: 37
currently lose_sum: 99.2789534330368
time_elpased: 1.583
batch start
#iterations: 38
currently lose_sum: 99.00868952274323
time_elpased: 1.559
batch start
#iterations: 39
currently lose_sum: 98.95981109142303
time_elpased: 1.555
start validation test
0.590051546392
0.602130616026
0.535144591952
0.566664850433
0.590147944019
65.352
batch start
#iterations: 40
currently lose_sum: 98.94502490758896
time_elpased: 1.549
batch start
#iterations: 41
currently lose_sum: 98.8207255601883
time_elpased: 1.537
batch start
#iterations: 42
currently lose_sum: 98.8508802652359
time_elpased: 1.555
batch start
#iterations: 43
currently lose_sum: 98.74848353862762
time_elpased: 1.555
batch start
#iterations: 44
currently lose_sum: 98.8311373591423
time_elpased: 1.55
batch start
#iterations: 45
currently lose_sum: 98.7920196056366
time_elpased: 1.547
batch start
#iterations: 46
currently lose_sum: 98.74911236763
time_elpased: 1.576
batch start
#iterations: 47
currently lose_sum: 98.81807112693787
time_elpased: 1.555
batch start
#iterations: 48
currently lose_sum: 98.68866682052612
time_elpased: 1.562
batch start
#iterations: 49
currently lose_sum: 98.82565099000931
time_elpased: 1.519
batch start
#iterations: 50
currently lose_sum: 99.10420453548431
time_elpased: 1.539
batch start
#iterations: 51
currently lose_sum: 98.73819994926453
time_elpased: 1.546
batch start
#iterations: 52
currently lose_sum: 98.7481654882431
time_elpased: 1.543
batch start
#iterations: 53
currently lose_sum: 98.80634754896164
time_elpased: 1.565
batch start
#iterations: 54
currently lose_sum: 98.92337328195572
time_elpased: 1.546
batch start
#iterations: 55
currently lose_sum: 98.80096697807312
time_elpased: 1.565
batch start
#iterations: 56
currently lose_sum: 98.94005292654037
time_elpased: 1.569
batch start
#iterations: 57
currently lose_sum: 98.79408198595047
time_elpased: 1.548
batch start
#iterations: 58
currently lose_sum: 98.62137758731842
time_elpased: 1.578
batch start
#iterations: 59
currently lose_sum: 98.68602162599564
time_elpased: 1.554
start validation test
0.579948453608
0.622730118973
0.409385612843
0.494008072027
0.580247902983
65.802
batch start
#iterations: 60
currently lose_sum: 98.68739259243011
time_elpased: 1.556
batch start
#iterations: 61
currently lose_sum: 98.5987845659256
time_elpased: 1.577
batch start
#iterations: 62
currently lose_sum: 98.49023896455765
time_elpased: 1.547
batch start
#iterations: 63
currently lose_sum: 98.8351936340332
time_elpased: 1.519
batch start
#iterations: 64
currently lose_sum: 98.79894369840622
time_elpased: 1.531
batch start
#iterations: 65
currently lose_sum: 98.38975381851196
time_elpased: 1.547
batch start
#iterations: 66
currently lose_sum: 98.41996902227402
time_elpased: 1.55
batch start
#iterations: 67
currently lose_sum: 98.3558480143547
time_elpased: 1.523
batch start
#iterations: 68
currently lose_sum: 98.47614926099777
time_elpased: 1.517
batch start
#iterations: 69
currently lose_sum: 98.58887511491776
time_elpased: 1.577
batch start
#iterations: 70
currently lose_sum: 98.34489274024963
time_elpased: 1.549
batch start
#iterations: 71
currently lose_sum: 98.3717959523201
time_elpased: 1.522
batch start
#iterations: 72
currently lose_sum: 98.21434098482132
time_elpased: 1.528
batch start
#iterations: 73
currently lose_sum: 98.273222386837
time_elpased: 1.55
batch start
#iterations: 74
currently lose_sum: 97.98553907871246
time_elpased: 1.566
batch start
#iterations: 75
currently lose_sum: 98.26473253965378
time_elpased: 1.55
batch start
#iterations: 76
currently lose_sum: 98.11494863033295
time_elpased: 1.551
batch start
#iterations: 77
currently lose_sum: 98.31823593378067
time_elpased: 1.586
batch start
#iterations: 78
currently lose_sum: 98.13827550411224
time_elpased: 1.548
batch start
#iterations: 79
currently lose_sum: 98.30707782506943
time_elpased: 1.633
start validation test
0.589226804124
0.597566421076
0.550890192446
0.57327978581
0.589294109959
65.115
batch start
#iterations: 80
currently lose_sum: 97.94800502061844
time_elpased: 1.565
batch start
#iterations: 81
currently lose_sum: 98.34400427341461
time_elpased: 1.601
batch start
#iterations: 82
currently lose_sum: 98.13349914550781
time_elpased: 1.556
batch start
#iterations: 83
currently lose_sum: 98.12439095973969
time_elpased: 1.578
batch start
#iterations: 84
currently lose_sum: 98.01886689662933
time_elpased: 1.54
batch start
#iterations: 85
currently lose_sum: 98.40591359138489
time_elpased: 1.571
batch start
#iterations: 86
currently lose_sum: 98.10594439506531
time_elpased: 1.558
batch start
#iterations: 87
currently lose_sum: 98.0491573214531
time_elpased: 1.541
batch start
#iterations: 88
currently lose_sum: 98.11953854560852
time_elpased: 1.522
batch start
#iterations: 89
currently lose_sum: 97.83127290010452
time_elpased: 1.593
batch start
#iterations: 90
currently lose_sum: 98.06788748502731
time_elpased: 1.647
batch start
#iterations: 91
currently lose_sum: 98.01601505279541
time_elpased: 1.623
batch start
#iterations: 92
currently lose_sum: 97.59316849708557
time_elpased: 1.532
batch start
#iterations: 93
currently lose_sum: 97.90077322721481
time_elpased: 1.617
batch start
#iterations: 94
currently lose_sum: 97.91353219747543
time_elpased: 1.548
batch start
#iterations: 95
currently lose_sum: 98.02455788850784
time_elpased: 1.566
batch start
#iterations: 96
currently lose_sum: 97.87121033668518
time_elpased: 1.596
batch start
#iterations: 97
currently lose_sum: 97.83128291368484
time_elpased: 1.575
batch start
#iterations: 98
currently lose_sum: 97.87136191129684
time_elpased: 1.533
batch start
#iterations: 99
currently lose_sum: 98.01621866226196
time_elpased: 1.555
start validation test
0.589432989691
0.618538565629
0.47041267881
0.534401122348
0.589641948204
65.390
batch start
#iterations: 100
currently lose_sum: 97.9516030550003
time_elpased: 1.601
batch start
#iterations: 101
currently lose_sum: 97.8753809928894
time_elpased: 1.573
batch start
#iterations: 102
currently lose_sum: 97.58861792087555
time_elpased: 1.573
batch start
#iterations: 103
currently lose_sum: 97.76614934206009
time_elpased: 1.548
batch start
#iterations: 104
currently lose_sum: 97.37690323591232
time_elpased: 1.569
batch start
#iterations: 105
currently lose_sum: 97.73990762233734
time_elpased: 1.557
batch start
#iterations: 106
currently lose_sum: 97.77069067955017
time_elpased: 1.56
batch start
#iterations: 107
currently lose_sum: 97.5235202908516
time_elpased: 1.57
batch start
#iterations: 108
currently lose_sum: 97.53451639413834
time_elpased: 1.573
batch start
#iterations: 109
currently lose_sum: 97.6596748828888
time_elpased: 1.612
batch start
#iterations: 110
currently lose_sum: 97.74503606557846
time_elpased: 1.556
batch start
#iterations: 111
currently lose_sum: 97.16915148496628
time_elpased: 1.586
batch start
#iterations: 112
currently lose_sum: 97.48433357477188
time_elpased: 1.531
batch start
#iterations: 113
currently lose_sum: 97.54830640554428
time_elpased: 1.59
batch start
#iterations: 114
currently lose_sum: 97.73789757490158
time_elpased: 1.555
batch start
#iterations: 115
currently lose_sum: 97.5838673710823
time_elpased: 1.569
batch start
#iterations: 116
currently lose_sum: 97.47782564163208
time_elpased: 1.566
batch start
#iterations: 117
currently lose_sum: 97.49799299240112
time_elpased: 1.548
batch start
#iterations: 118
currently lose_sum: 97.3802598118782
time_elpased: 1.537
batch start
#iterations: 119
currently lose_sum: 97.64619809389114
time_elpased: 1.574
start validation test
0.585773195876
0.603344399361
0.504991252444
0.549803921569
0.585915021038
65.686
batch start
#iterations: 120
currently lose_sum: 97.4941274523735
time_elpased: 1.566
batch start
#iterations: 121
currently lose_sum: 97.39335805177689
time_elpased: 1.556
batch start
#iterations: 122
currently lose_sum: 97.1324412226677
time_elpased: 1.609
batch start
#iterations: 123
currently lose_sum: 97.64842790365219
time_elpased: 1.594
batch start
#iterations: 124
currently lose_sum: 97.47508400678635
time_elpased: 1.54
batch start
#iterations: 125
currently lose_sum: 97.40795361995697
time_elpased: 1.587
batch start
#iterations: 126
currently lose_sum: 97.14685571193695
time_elpased: 1.532
batch start
#iterations: 127
currently lose_sum: 97.02457213401794
time_elpased: 1.575
batch start
#iterations: 128
currently lose_sum: 97.62066984176636
time_elpased: 1.621
batch start
#iterations: 129
currently lose_sum: 97.53541660308838
time_elpased: 1.564
batch start
#iterations: 130
currently lose_sum: 97.3076793551445
time_elpased: 1.532
batch start
#iterations: 131
currently lose_sum: 97.17607021331787
time_elpased: 1.57
batch start
#iterations: 132
currently lose_sum: 97.15355408191681
time_elpased: 1.549
batch start
#iterations: 133
currently lose_sum: 97.0527896285057
time_elpased: 1.582
batch start
#iterations: 134
currently lose_sum: 96.88060337305069
time_elpased: 1.563
batch start
#iterations: 135
currently lose_sum: 97.18591022491455
time_elpased: 1.538
batch start
#iterations: 136
currently lose_sum: 97.12155950069427
time_elpased: 1.562
batch start
#iterations: 137
currently lose_sum: 97.05717033147812
time_elpased: 1.552
batch start
#iterations: 138
currently lose_sum: 96.85389202833176
time_elpased: 1.559
batch start
#iterations: 139
currently lose_sum: 97.04233032464981
time_elpased: 1.559
start validation test
0.583608247423
0.613112491373
0.457136976433
0.523758990685
0.583830287246
66.061
batch start
#iterations: 140
currently lose_sum: 96.93978774547577
time_elpased: 1.622
batch start
#iterations: 141
currently lose_sum: 96.85250520706177
time_elpased: 1.556
batch start
#iterations: 142
currently lose_sum: 97.11784136295319
time_elpased: 1.601
batch start
#iterations: 143
currently lose_sum: 96.78121709823608
time_elpased: 1.559
batch start
#iterations: 144
currently lose_sum: 97.09059065580368
time_elpased: 1.542
batch start
#iterations: 145
currently lose_sum: 96.9577026963234
time_elpased: 1.565
batch start
#iterations: 146
currently lose_sum: 97.02194958925247
time_elpased: 1.536
batch start
#iterations: 147
currently lose_sum: 97.17629051208496
time_elpased: 1.585
batch start
#iterations: 148
currently lose_sum: 96.92892944812775
time_elpased: 1.553
batch start
#iterations: 149
currently lose_sum: 96.96977031230927
time_elpased: 1.55
batch start
#iterations: 150
currently lose_sum: 96.89578384160995
time_elpased: 1.55
batch start
#iterations: 151
currently lose_sum: 96.87179923057556
time_elpased: 1.549
batch start
#iterations: 152
currently lose_sum: 96.86330497264862
time_elpased: 1.559
batch start
#iterations: 153
currently lose_sum: 96.88864380121231
time_elpased: 1.578
batch start
#iterations: 154
currently lose_sum: 96.85439330339432
time_elpased: 1.539
batch start
#iterations: 155
currently lose_sum: 96.98493492603302
time_elpased: 1.553
batch start
#iterations: 156
currently lose_sum: 96.62213563919067
time_elpased: 1.557
batch start
#iterations: 157
currently lose_sum: 96.70814090967178
time_elpased: 1.623
batch start
#iterations: 158
currently lose_sum: 96.45776903629303
time_elpased: 1.569
batch start
#iterations: 159
currently lose_sum: 96.9118891954422
time_elpased: 1.539
start validation test
0.587371134021
0.638421733506
0.406298240198
0.496572542607
0.587689035414
66.195
batch start
#iterations: 160
currently lose_sum: 96.31513649225235
time_elpased: 1.56
batch start
#iterations: 161
currently lose_sum: 96.59997725486755
time_elpased: 1.571
batch start
#iterations: 162
currently lose_sum: 96.48446172475815
time_elpased: 1.559
batch start
#iterations: 163
currently lose_sum: 96.84619349241257
time_elpased: 1.564
batch start
#iterations: 164
currently lose_sum: 96.64758110046387
time_elpased: 1.57
batch start
#iterations: 165
currently lose_sum: 96.54867219924927
time_elpased: 1.575
batch start
#iterations: 166
currently lose_sum: 96.50590640306473
time_elpased: 1.612
batch start
#iterations: 167
currently lose_sum: 96.49803864955902
time_elpased: 1.558
batch start
#iterations: 168
currently lose_sum: 96.53853696584702
time_elpased: 1.538
batch start
#iterations: 169
currently lose_sum: 96.54783248901367
time_elpased: 1.571
batch start
#iterations: 170
currently lose_sum: 96.91535884141922
time_elpased: 1.567
batch start
#iterations: 171
currently lose_sum: 96.3483675122261
time_elpased: 1.597
batch start
#iterations: 172
currently lose_sum: 96.55341702699661
time_elpased: 1.596
batch start
#iterations: 173
currently lose_sum: 96.60202449560165
time_elpased: 1.569
batch start
#iterations: 174
currently lose_sum: 96.18973779678345
time_elpased: 1.57
batch start
#iterations: 175
currently lose_sum: 96.37746703624725
time_elpased: 1.545
batch start
#iterations: 176
currently lose_sum: 96.59423059225082
time_elpased: 1.571
batch start
#iterations: 177
currently lose_sum: 96.60447961091995
time_elpased: 1.553
batch start
#iterations: 178
currently lose_sum: 96.04499053955078
time_elpased: 1.558
batch start
#iterations: 179
currently lose_sum: 96.3660728931427
time_elpased: 1.555
start validation test
0.586546391753
0.615625852195
0.464649583205
0.529587707466
0.586760400401
66.006
batch start
#iterations: 180
currently lose_sum: 96.59909456968307
time_elpased: 1.594
batch start
#iterations: 181
currently lose_sum: 96.1477073431015
time_elpased: 1.565
batch start
#iterations: 182
currently lose_sum: 96.41343003511429
time_elpased: 1.623
batch start
#iterations: 183
currently lose_sum: 96.16806620359421
time_elpased: 1.56
batch start
#iterations: 184
currently lose_sum: 96.30135017633438
time_elpased: 1.588
batch start
#iterations: 185
currently lose_sum: 96.14922881126404
time_elpased: 1.576
batch start
#iterations: 186
currently lose_sum: 96.0968981385231
time_elpased: 1.591
batch start
#iterations: 187
currently lose_sum: 96.1325609087944
time_elpased: 1.56
batch start
#iterations: 188
currently lose_sum: 96.26112502813339
time_elpased: 1.574
batch start
#iterations: 189
currently lose_sum: 96.07611918449402
time_elpased: 1.544
batch start
#iterations: 190
currently lose_sum: 95.94311153888702
time_elpased: 1.587
batch start
#iterations: 191
currently lose_sum: 96.20213913917542
time_elpased: 1.584
batch start
#iterations: 192
currently lose_sum: 96.15813362598419
time_elpased: 1.615
batch start
#iterations: 193
currently lose_sum: 96.24114388227463
time_elpased: 1.534
batch start
#iterations: 194
currently lose_sum: 95.7738573551178
time_elpased: 1.535
batch start
#iterations: 195
currently lose_sum: 96.59466570615768
time_elpased: 1.591
batch start
#iterations: 196
currently lose_sum: 96.16685199737549
time_elpased: 1.594
batch start
#iterations: 197
currently lose_sum: 96.13271504640579
time_elpased: 1.561
batch start
#iterations: 198
currently lose_sum: 95.71352523565292
time_elpased: 1.563
batch start
#iterations: 199
currently lose_sum: 96.0635586977005
time_elpased: 1.549
start validation test
0.582989690722
0.609444369703
0.466193269528
0.528279883382
0.583194744854
66.836
batch start
#iterations: 200
currently lose_sum: 96.37076562643051
time_elpased: 1.57
batch start
#iterations: 201
currently lose_sum: 95.98706710338593
time_elpased: 1.574
batch start
#iterations: 202
currently lose_sum: 96.12286341190338
time_elpased: 1.58
batch start
#iterations: 203
currently lose_sum: 95.8535887002945
time_elpased: 1.624
batch start
#iterations: 204
currently lose_sum: 95.92814069986343
time_elpased: 1.558
batch start
#iterations: 205
currently lose_sum: 95.98843812942505
time_elpased: 1.547
batch start
#iterations: 206
currently lose_sum: 96.23970639705658
time_elpased: 1.572
batch start
#iterations: 207
currently lose_sum: 95.45456176996231
time_elpased: 1.565
batch start
#iterations: 208
currently lose_sum: 95.85872840881348
time_elpased: 1.562
batch start
#iterations: 209
currently lose_sum: 95.75226092338562
time_elpased: 1.578
batch start
#iterations: 210
currently lose_sum: 95.73418945074081
time_elpased: 1.528
batch start
#iterations: 211
currently lose_sum: 95.57383555173874
time_elpased: 1.553
batch start
#iterations: 212
currently lose_sum: 95.67344772815704
time_elpased: 1.575
batch start
#iterations: 213
currently lose_sum: 95.45527964830399
time_elpased: 1.581
batch start
#iterations: 214
currently lose_sum: 95.8090660572052
time_elpased: 1.549
batch start
#iterations: 215
currently lose_sum: 95.28713458776474
time_elpased: 1.553
batch start
#iterations: 216
currently lose_sum: 95.40279531478882
time_elpased: 1.584
batch start
#iterations: 217
currently lose_sum: 95.24717426300049
time_elpased: 1.584
batch start
#iterations: 218
currently lose_sum: 95.23914510011673
time_elpased: 1.552
batch start
#iterations: 219
currently lose_sum: 95.6152281165123
time_elpased: 1.546
start validation test
0.584329896907
0.614236351071
0.457342801276
0.524303916942
0.58455284234
67.319
batch start
#iterations: 220
currently lose_sum: 95.59016263484955
time_elpased: 1.596
batch start
#iterations: 221
currently lose_sum: 95.44593554735184
time_elpased: 1.57
batch start
#iterations: 222
currently lose_sum: 95.37569111585617
time_elpased: 1.612
batch start
#iterations: 223
currently lose_sum: 95.18171119689941
time_elpased: 1.598
batch start
#iterations: 224
currently lose_sum: 95.4441037774086
time_elpased: 1.578
batch start
#iterations: 225
currently lose_sum: 95.69804656505585
time_elpased: 1.572
batch start
#iterations: 226
currently lose_sum: 95.23012387752533
time_elpased: 1.562
batch start
#iterations: 227
currently lose_sum: 95.04049479961395
time_elpased: 1.553
batch start
#iterations: 228
currently lose_sum: 95.39091032743454
time_elpased: 1.582
batch start
#iterations: 229
currently lose_sum: 95.35171163082123
time_elpased: 1.539
batch start
#iterations: 230
currently lose_sum: 95.29188579320908
time_elpased: 1.56
batch start
#iterations: 231
currently lose_sum: 95.38984924554825
time_elpased: 1.573
batch start
#iterations: 232
currently lose_sum: 95.26894533634186
time_elpased: 1.562
batch start
#iterations: 233
currently lose_sum: 94.8430135846138
time_elpased: 1.585
batch start
#iterations: 234
currently lose_sum: 95.2372162938118
time_elpased: 1.614
batch start
#iterations: 235
currently lose_sum: 95.21875578165054
time_elpased: 1.563
batch start
#iterations: 236
currently lose_sum: 94.94254064559937
time_elpased: 1.568
batch start
#iterations: 237
currently lose_sum: 95.73136538267136
time_elpased: 1.544
batch start
#iterations: 238
currently lose_sum: 95.35213470458984
time_elpased: 1.557
batch start
#iterations: 239
currently lose_sum: 95.18390482664108
time_elpased: 1.59
start validation test
0.585618556701
0.625674056321
0.429865184728
0.50960775941
0.585892005769
67.089
batch start
#iterations: 240
currently lose_sum: 95.27838480472565
time_elpased: 1.556
batch start
#iterations: 241
currently lose_sum: 95.31922763586044
time_elpased: 1.578
batch start
#iterations: 242
currently lose_sum: 95.24978321790695
time_elpased: 1.545
batch start
#iterations: 243
currently lose_sum: 94.93954610824585
time_elpased: 1.566
batch start
#iterations: 244
currently lose_sum: 94.95592129230499
time_elpased: 1.57
batch start
#iterations: 245
currently lose_sum: 95.03084218502045
time_elpased: 1.543
batch start
#iterations: 246
currently lose_sum: 95.00748682022095
time_elpased: 1.635
batch start
#iterations: 247
currently lose_sum: 95.16441857814789
time_elpased: 1.543
batch start
#iterations: 248
currently lose_sum: 95.07935518026352
time_elpased: 1.54
batch start
#iterations: 249
currently lose_sum: 94.95785528421402
time_elpased: 1.56
batch start
#iterations: 250
currently lose_sum: 95.03049319982529
time_elpased: 1.574
batch start
#iterations: 251
currently lose_sum: 94.95415210723877
time_elpased: 1.665
batch start
#iterations: 252
currently lose_sum: 95.255954682827
time_elpased: 1.603
batch start
#iterations: 253
currently lose_sum: 95.1417224407196
time_elpased: 1.596
batch start
#iterations: 254
currently lose_sum: 94.70285242795944
time_elpased: 1.606
batch start
#iterations: 255
currently lose_sum: 94.70524698495865
time_elpased: 1.572
batch start
#iterations: 256
currently lose_sum: 94.71563255786896
time_elpased: 1.593
batch start
#iterations: 257
currently lose_sum: 94.89780563116074
time_elpased: 1.568
batch start
#iterations: 258
currently lose_sum: 94.96644276380539
time_elpased: 1.609
batch start
#iterations: 259
currently lose_sum: 94.66853213310242
time_elpased: 1.622
start validation test
0.584742268041
0.627788890599
0.419882679839
0.503206709423
0.585031704476
67.308
batch start
#iterations: 260
currently lose_sum: 94.8809734582901
time_elpased: 1.579
batch start
#iterations: 261
currently lose_sum: 94.8134645819664
time_elpased: 1.593
batch start
#iterations: 262
currently lose_sum: 94.71827501058578
time_elpased: 1.587
batch start
#iterations: 263
currently lose_sum: 94.44734108448029
time_elpased: 1.568
batch start
#iterations: 264
currently lose_sum: 95.02130395174026
time_elpased: 1.544
batch start
#iterations: 265
currently lose_sum: 94.52471458911896
time_elpased: 1.556
batch start
#iterations: 266
currently lose_sum: 94.38097709417343
time_elpased: 1.603
batch start
#iterations: 267
currently lose_sum: 94.72280550003052
time_elpased: 1.551
batch start
#iterations: 268
currently lose_sum: 94.37914651632309
time_elpased: 1.58
batch start
#iterations: 269
currently lose_sum: 94.53435218334198
time_elpased: 1.562
batch start
#iterations: 270
currently lose_sum: 94.60563045740128
time_elpased: 1.569
batch start
#iterations: 271
currently lose_sum: 94.58410948514938
time_elpased: 1.568
batch start
#iterations: 272
currently lose_sum: 94.51736634969711
time_elpased: 1.577
batch start
#iterations: 273
currently lose_sum: 94.39179539680481
time_elpased: 1.557
batch start
#iterations: 274
currently lose_sum: 94.06778770685196
time_elpased: 1.572
batch start
#iterations: 275
currently lose_sum: 94.26755052804947
time_elpased: 1.56
batch start
#iterations: 276
currently lose_sum: 95.24301642179489
time_elpased: 1.556
batch start
#iterations: 277
currently lose_sum: 94.55599182844162
time_elpased: 1.608
batch start
#iterations: 278
currently lose_sum: 94.4347894191742
time_elpased: 1.582
batch start
#iterations: 279
currently lose_sum: 94.35245782136917
time_elpased: 1.554
start validation test
0.580515463918
0.606015845307
0.464443758362
0.525868096015
0.580719245699
68.430
batch start
#iterations: 280
currently lose_sum: 94.32264202833176
time_elpased: 1.574
batch start
#iterations: 281
currently lose_sum: 94.3756532073021
time_elpased: 1.58
batch start
#iterations: 282
currently lose_sum: 94.34308868646622
time_elpased: 1.61
batch start
#iterations: 283
currently lose_sum: 94.03306752443314
time_elpased: 1.604
batch start
#iterations: 284
currently lose_sum: 94.22684973478317
time_elpased: 1.563
batch start
#iterations: 285
currently lose_sum: 94.37042760848999
time_elpased: 1.562
batch start
#iterations: 286
currently lose_sum: 94.00743675231934
time_elpased: 1.559
batch start
#iterations: 287
currently lose_sum: 93.84673088788986
time_elpased: 1.544
batch start
#iterations: 288
currently lose_sum: 94.04160553216934
time_elpased: 1.537
batch start
#iterations: 289
currently lose_sum: 94.12487322092056
time_elpased: 1.576
batch start
#iterations: 290
currently lose_sum: 94.24283850193024
time_elpased: 1.613
batch start
#iterations: 291
currently lose_sum: 94.50413811206818
time_elpased: 1.58
batch start
#iterations: 292
currently lose_sum: 94.1644178032875
time_elpased: 1.584
batch start
#iterations: 293
currently lose_sum: 93.93578737974167
time_elpased: 1.569
batch start
#iterations: 294
currently lose_sum: 94.09839242696762
time_elpased: 1.577
batch start
#iterations: 295
currently lose_sum: 93.893106341362
time_elpased: 1.54
batch start
#iterations: 296
currently lose_sum: 93.54548168182373
time_elpased: 1.543
batch start
#iterations: 297
currently lose_sum: 94.091033577919
time_elpased: 1.654
batch start
#iterations: 298
currently lose_sum: 93.88966625928879
time_elpased: 1.595
batch start
#iterations: 299
currently lose_sum: 93.89833766222
time_elpased: 1.572
start validation test
0.579845360825
0.635420269803
0.378100236699
0.47409510291
0.580199555507
70.263
batch start
#iterations: 300
currently lose_sum: 94.34363776445389
time_elpased: 1.587
batch start
#iterations: 301
currently lose_sum: 94.02054315805435
time_elpased: 1.545
batch start
#iterations: 302
currently lose_sum: 93.79687410593033
time_elpased: 1.578
batch start
#iterations: 303
currently lose_sum: 93.9456467628479
time_elpased: 1.624
batch start
#iterations: 304
currently lose_sum: 94.22983318567276
time_elpased: 1.56
batch start
#iterations: 305
currently lose_sum: 94.25898414850235
time_elpased: 1.558
batch start
#iterations: 306
currently lose_sum: 93.8105036020279
time_elpased: 1.571
batch start
#iterations: 307
currently lose_sum: 94.10686498880386
time_elpased: 1.584
batch start
#iterations: 308
currently lose_sum: 94.1036611199379
time_elpased: 1.594
batch start
#iterations: 309
currently lose_sum: 93.90692460536957
time_elpased: 1.62
batch start
#iterations: 310
currently lose_sum: 93.5716940164566
time_elpased: 1.526
batch start
#iterations: 311
currently lose_sum: 93.36566853523254
time_elpased: 1.601
batch start
#iterations: 312
currently lose_sum: 93.52210813760757
time_elpased: 1.644
batch start
#iterations: 313
currently lose_sum: 93.55171078443527
time_elpased: 1.6
batch start
#iterations: 314
currently lose_sum: 93.45076990127563
time_elpased: 1.545
batch start
#iterations: 315
currently lose_sum: 93.60992586612701
time_elpased: 1.632
batch start
#iterations: 316
currently lose_sum: 93.9786182641983
time_elpased: 1.589
batch start
#iterations: 317
currently lose_sum: 93.60474056005478
time_elpased: 1.59
batch start
#iterations: 318
currently lose_sum: 93.5190532207489
time_elpased: 1.544
batch start
#iterations: 319
currently lose_sum: 93.82175362110138
time_elpased: 1.602
start validation test
0.581340206186
0.629191641017
0.39971184522
0.488860918817
0.581659082787
70.226
batch start
#iterations: 320
currently lose_sum: 93.71197253465652
time_elpased: 1.552
batch start
#iterations: 321
currently lose_sum: 93.64384913444519
time_elpased: 1.59
batch start
#iterations: 322
currently lose_sum: 93.57730913162231
time_elpased: 1.574
batch start
#iterations: 323
currently lose_sum: 93.49676817655563
time_elpased: 1.568
batch start
#iterations: 324
currently lose_sum: 93.55893915891647
time_elpased: 1.584
batch start
#iterations: 325
currently lose_sum: 93.43019783496857
time_elpased: 1.611
batch start
#iterations: 326
currently lose_sum: 93.30397999286652
time_elpased: 1.563
batch start
#iterations: 327
currently lose_sum: 93.76939052343369
time_elpased: 1.557
batch start
#iterations: 328
currently lose_sum: 93.19187957048416
time_elpased: 1.562
batch start
#iterations: 329
currently lose_sum: 93.70325696468353
time_elpased: 1.572
batch start
#iterations: 330
currently lose_sum: 93.88775444030762
time_elpased: 1.57
batch start
#iterations: 331
currently lose_sum: 93.72584754228592
time_elpased: 1.595
batch start
#iterations: 332
currently lose_sum: 93.62102860212326
time_elpased: 1.553
batch start
#iterations: 333
currently lose_sum: 93.51234167814255
time_elpased: 1.561
batch start
#iterations: 334
currently lose_sum: 93.52842807769775
time_elpased: 1.578
batch start
#iterations: 335
currently lose_sum: 93.36195546388626
time_elpased: 1.543
batch start
#iterations: 336
currently lose_sum: 93.3062390089035
time_elpased: 1.622
batch start
#iterations: 337
currently lose_sum: 93.44755238294601
time_elpased: 1.531
batch start
#iterations: 338
currently lose_sum: 92.92757368087769
time_elpased: 1.556
batch start
#iterations: 339
currently lose_sum: 93.33098268508911
time_elpased: 1.56
start validation test
0.579484536082
0.608188757807
0.450962231141
0.517905684907
0.579710176812
70.884
batch start
#iterations: 340
currently lose_sum: 93.27966445684433
time_elpased: 1.591
batch start
#iterations: 341
currently lose_sum: 93.4119371175766
time_elpased: 1.544
batch start
#iterations: 342
currently lose_sum: 93.49203616380692
time_elpased: 1.591
batch start
#iterations: 343
currently lose_sum: 93.5401594042778
time_elpased: 1.554
batch start
#iterations: 344
currently lose_sum: 93.32403618097305
time_elpased: 1.591
batch start
#iterations: 345
currently lose_sum: 93.16913777589798
time_elpased: 1.555
batch start
#iterations: 346
currently lose_sum: 93.20577722787857
time_elpased: 1.627
batch start
#iterations: 347
currently lose_sum: 93.01447087526321
time_elpased: 1.533
batch start
#iterations: 348
currently lose_sum: 93.62834799289703
time_elpased: 1.587
batch start
#iterations: 349
currently lose_sum: 92.98275852203369
time_elpased: 1.619
batch start
#iterations: 350
currently lose_sum: 92.74588507413864
time_elpased: 1.597
batch start
#iterations: 351
currently lose_sum: 93.0729308128357
time_elpased: 1.59
batch start
#iterations: 352
currently lose_sum: 93.15555238723755
time_elpased: 1.582
batch start
#iterations: 353
currently lose_sum: 93.50446754693985
time_elpased: 1.584
batch start
#iterations: 354
currently lose_sum: 93.20886808633804
time_elpased: 1.594
batch start
#iterations: 355
currently lose_sum: 93.21331882476807
time_elpased: 1.539
batch start
#iterations: 356
currently lose_sum: 92.43865829706192
time_elpased: 1.556
batch start
#iterations: 357
currently lose_sum: 92.7742075920105
time_elpased: 1.578
batch start
#iterations: 358
currently lose_sum: 92.90303158760071
time_elpased: 1.575
batch start
#iterations: 359
currently lose_sum: 92.88848131895065
time_elpased: 1.553
start validation test
0.57675257732
0.614751600122
0.415148708449
0.495607838319
0.577036297837
71.376
batch start
#iterations: 360
currently lose_sum: 93.42587858438492
time_elpased: 1.578
batch start
#iterations: 361
currently lose_sum: 92.89676493406296
time_elpased: 1.571
batch start
#iterations: 362
currently lose_sum: 92.87014538049698
time_elpased: 1.565
batch start
#iterations: 363
currently lose_sum: 92.94588881731033
time_elpased: 1.566
batch start
#iterations: 364
currently lose_sum: 93.00753337144852
time_elpased: 1.592
batch start
#iterations: 365
currently lose_sum: 93.2860734462738
time_elpased: 1.577
batch start
#iterations: 366
currently lose_sum: 92.59957706928253
time_elpased: 1.574
batch start
#iterations: 367
currently lose_sum: 93.02702695131302
time_elpased: 1.565
batch start
#iterations: 368
currently lose_sum: 92.58800464868546
time_elpased: 1.576
batch start
#iterations: 369
currently lose_sum: 92.39431738853455
time_elpased: 1.579
batch start
#iterations: 370
currently lose_sum: 92.6156410574913
time_elpased: 1.589
batch start
#iterations: 371
currently lose_sum: 92.78183645009995
time_elpased: 1.554
batch start
#iterations: 372
currently lose_sum: 92.7508043050766
time_elpased: 1.571
batch start
#iterations: 373
currently lose_sum: 92.49996322393417
time_elpased: 1.559
batch start
#iterations: 374
currently lose_sum: 92.43377584218979
time_elpased: 1.577
batch start
#iterations: 375
currently lose_sum: 92.87469279766083
time_elpased: 1.618
batch start
#iterations: 376
currently lose_sum: 92.68537223339081
time_elpased: 1.576
batch start
#iterations: 377
currently lose_sum: 92.17485785484314
time_elpased: 1.58
batch start
#iterations: 378
currently lose_sum: 93.06381368637085
time_elpased: 1.586
batch start
#iterations: 379
currently lose_sum: 92.71222537755966
time_elpased: 1.564
start validation test
0.574381443299
0.633211678832
0.357106102707
0.456669079424
0.574762903672
73.230
batch start
#iterations: 380
currently lose_sum: 92.7465335726738
time_elpased: 1.566
batch start
#iterations: 381
currently lose_sum: 92.32186156511307
time_elpased: 1.595
batch start
#iterations: 382
currently lose_sum: 92.78645426034927
time_elpased: 1.557
batch start
#iterations: 383
currently lose_sum: 92.40325319766998
time_elpased: 1.573
batch start
#iterations: 384
currently lose_sum: 92.6569430232048
time_elpased: 1.566
batch start
#iterations: 385
currently lose_sum: 92.37156105041504
time_elpased: 1.559
batch start
#iterations: 386
currently lose_sum: 92.79507446289062
time_elpased: 1.635
batch start
#iterations: 387
currently lose_sum: 92.501005589962
time_elpased: 1.55
batch start
#iterations: 388
currently lose_sum: 92.53797370195389
time_elpased: 1.566
batch start
#iterations: 389
currently lose_sum: 92.8778766989708
time_elpased: 1.599
batch start
#iterations: 390
currently lose_sum: 92.6539797782898
time_elpased: 1.608
batch start
#iterations: 391
currently lose_sum: 92.54085677862167
time_elpased: 1.583
batch start
#iterations: 392
currently lose_sum: 92.18201959133148
time_elpased: 1.57
batch start
#iterations: 393
currently lose_sum: 92.62120032310486
time_elpased: 1.616
batch start
#iterations: 394
currently lose_sum: 92.52601289749146
time_elpased: 1.595
batch start
#iterations: 395
currently lose_sum: 92.22780007123947
time_elpased: 1.627
batch start
#iterations: 396
currently lose_sum: 92.67084968090057
time_elpased: 1.572
batch start
#iterations: 397
currently lose_sum: 92.2449117898941
time_elpased: 1.55
batch start
#iterations: 398
currently lose_sum: 92.23991167545319
time_elpased: 1.543
batch start
#iterations: 399
currently lose_sum: 92.32999575138092
time_elpased: 1.541
start validation test
0.57618556701
0.6178464449
0.403313779973
0.48804483188
0.576489070096
75.096
acc: 0.595
pre: 0.603
rec: 0.562
F1: 0.582
auc: 0.624
