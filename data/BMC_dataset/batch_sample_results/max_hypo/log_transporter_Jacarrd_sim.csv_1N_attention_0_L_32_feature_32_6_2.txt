start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.36268508434296
time_elpased: 1.528
batch start
#iterations: 1
currently lose_sum: 100.27177292108536
time_elpased: 1.509
batch start
#iterations: 2
currently lose_sum: 100.16627615690231
time_elpased: 1.516
batch start
#iterations: 3
currently lose_sum: 100.20820301771164
time_elpased: 1.556
batch start
#iterations: 4
currently lose_sum: 99.9659258723259
time_elpased: 1.546
batch start
#iterations: 5
currently lose_sum: 99.96415454149246
time_elpased: 1.567
batch start
#iterations: 6
currently lose_sum: 99.75369501113892
time_elpased: 1.517
batch start
#iterations: 7
currently lose_sum: 99.72359818220139
time_elpased: 1.529
batch start
#iterations: 8
currently lose_sum: 99.69204515218735
time_elpased: 1.508
batch start
#iterations: 9
currently lose_sum: 99.52836889028549
time_elpased: 1.516
batch start
#iterations: 10
currently lose_sum: 99.50730723142624
time_elpased: 1.544
batch start
#iterations: 11
currently lose_sum: 99.74088609218597
time_elpased: 1.517
batch start
#iterations: 12
currently lose_sum: 99.6178988814354
time_elpased: 1.515
batch start
#iterations: 13
currently lose_sum: 99.74085581302643
time_elpased: 1.556
batch start
#iterations: 14
currently lose_sum: 99.54673165082932
time_elpased: 1.562
batch start
#iterations: 15
currently lose_sum: 99.46923035383224
time_elpased: 1.551
batch start
#iterations: 16
currently lose_sum: 99.31001538038254
time_elpased: 1.53
batch start
#iterations: 17
currently lose_sum: 99.55540865659714
time_elpased: 1.528
batch start
#iterations: 18
currently lose_sum: 99.36982148885727
time_elpased: 1.519
batch start
#iterations: 19
currently lose_sum: 99.20249956846237
time_elpased: 1.567
start validation test
0.589432989691
0.594234079174
0.568488216528
0.581076105822
0.589469761471
65.280
batch start
#iterations: 20
currently lose_sum: 99.22691488265991
time_elpased: 1.585
batch start
#iterations: 21
currently lose_sum: 99.20902287960052
time_elpased: 1.523
batch start
#iterations: 22
currently lose_sum: 99.39166468381882
time_elpased: 1.511
batch start
#iterations: 23
currently lose_sum: 99.33911502361298
time_elpased: 1.517
batch start
#iterations: 24
currently lose_sum: 99.1447285413742
time_elpased: 1.53
batch start
#iterations: 25
currently lose_sum: 99.08762443065643
time_elpased: 1.57
batch start
#iterations: 26
currently lose_sum: 99.30635434389114
time_elpased: 1.521
batch start
#iterations: 27
currently lose_sum: 98.97025364637375
time_elpased: 1.494
batch start
#iterations: 28
currently lose_sum: 98.85396361351013
time_elpased: 1.641
batch start
#iterations: 29
currently lose_sum: 99.05920833349228
time_elpased: 1.545
batch start
#iterations: 30
currently lose_sum: 99.03793770074844
time_elpased: 1.51
batch start
#iterations: 31
currently lose_sum: 98.92083263397217
time_elpased: 1.526
batch start
#iterations: 32
currently lose_sum: 98.98434722423553
time_elpased: 1.515
batch start
#iterations: 33
currently lose_sum: 98.79742830991745
time_elpased: 1.516
batch start
#iterations: 34
currently lose_sum: 98.9322121143341
time_elpased: 1.539
batch start
#iterations: 35
currently lose_sum: 98.8440933227539
time_elpased: 1.538
batch start
#iterations: 36
currently lose_sum: 99.03171873092651
time_elpased: 1.528
batch start
#iterations: 37
currently lose_sum: 98.88444322347641
time_elpased: 1.522
batch start
#iterations: 38
currently lose_sum: 98.86297625303268
time_elpased: 1.53
batch start
#iterations: 39
currently lose_sum: 98.9251851439476
time_elpased: 1.519
start validation test
0.58618556701
0.614384396587
0.466810744057
0.530526315789
0.586395147924
65.358
batch start
#iterations: 40
currently lose_sum: 98.74313253164291
time_elpased: 1.592
batch start
#iterations: 41
currently lose_sum: 98.89591389894485
time_elpased: 1.555
batch start
#iterations: 42
currently lose_sum: 98.6963404417038
time_elpased: 1.5
batch start
#iterations: 43
currently lose_sum: 98.75120455026627
time_elpased: 1.532
batch start
#iterations: 44
currently lose_sum: 98.8089342713356
time_elpased: 1.501
batch start
#iterations: 45
currently lose_sum: 98.8494440317154
time_elpased: 1.576
batch start
#iterations: 46
currently lose_sum: 98.45034563541412
time_elpased: 1.52
batch start
#iterations: 47
currently lose_sum: 98.75912606716156
time_elpased: 1.552
batch start
#iterations: 48
currently lose_sum: 98.68730968236923
time_elpased: 1.517
batch start
#iterations: 49
currently lose_sum: 98.46959006786346
time_elpased: 1.506
batch start
#iterations: 50
currently lose_sum: 98.66243606805801
time_elpased: 1.543
batch start
#iterations: 51
currently lose_sum: 98.49062103033066
time_elpased: 1.53
batch start
#iterations: 52
currently lose_sum: 98.65458405017853
time_elpased: 1.551
batch start
#iterations: 53
currently lose_sum: 98.6384208202362
time_elpased: 1.54
batch start
#iterations: 54
currently lose_sum: 98.69138944149017
time_elpased: 1.573
batch start
#iterations: 55
currently lose_sum: 98.55451434850693
time_elpased: 1.548
batch start
#iterations: 56
currently lose_sum: 98.60732436180115
time_elpased: 1.525
batch start
#iterations: 57
currently lose_sum: 98.68388086557388
time_elpased: 1.494
batch start
#iterations: 58
currently lose_sum: 98.51466208696365
time_elpased: 1.528
batch start
#iterations: 59
currently lose_sum: 98.5040055513382
time_elpased: 1.496
start validation test
0.588608247423
0.621875877562
0.455799114953
0.526040738761
0.588841414339
65.269
batch start
#iterations: 60
currently lose_sum: 98.24050062894821
time_elpased: 1.529
batch start
#iterations: 61
currently lose_sum: 98.62145537137985
time_elpased: 1.507
batch start
#iterations: 62
currently lose_sum: 98.42883491516113
time_elpased: 1.489
batch start
#iterations: 63
currently lose_sum: 98.78648394346237
time_elpased: 1.531
batch start
#iterations: 64
currently lose_sum: 98.39838874340057
time_elpased: 1.512
batch start
#iterations: 65
currently lose_sum: 98.37094855308533
time_elpased: 1.532
batch start
#iterations: 66
currently lose_sum: 98.10937017202377
time_elpased: 1.492
batch start
#iterations: 67
currently lose_sum: 98.27256381511688
time_elpased: 1.555
batch start
#iterations: 68
currently lose_sum: 98.44380801916122
time_elpased: 1.539
batch start
#iterations: 69
currently lose_sum: 98.25949311256409
time_elpased: 1.539
batch start
#iterations: 70
currently lose_sum: 98.1986477971077
time_elpased: 1.542
batch start
#iterations: 71
currently lose_sum: 98.43852818012238
time_elpased: 1.54
batch start
#iterations: 72
currently lose_sum: 98.30002081394196
time_elpased: 1.569
batch start
#iterations: 73
currently lose_sum: 98.18241477012634
time_elpased: 1.519
batch start
#iterations: 74
currently lose_sum: 98.33075034618378
time_elpased: 1.537
batch start
#iterations: 75
currently lose_sum: 98.17576825618744
time_elpased: 1.519
batch start
#iterations: 76
currently lose_sum: 98.1083573102951
time_elpased: 1.562
batch start
#iterations: 77
currently lose_sum: 98.50455021858215
time_elpased: 1.513
batch start
#iterations: 78
currently lose_sum: 98.04528963565826
time_elpased: 1.539
batch start
#iterations: 79
currently lose_sum: 98.38419306278229
time_elpased: 1.528
start validation test
0.592268041237
0.609661366671
0.516929093342
0.559478725774
0.59240031038
64.892
batch start
#iterations: 80
currently lose_sum: 98.08977597951889
time_elpased: 1.554
batch start
#iterations: 81
currently lose_sum: 97.98890489339828
time_elpased: 1.554
batch start
#iterations: 82
currently lose_sum: 98.34448206424713
time_elpased: 1.54
batch start
#iterations: 83
currently lose_sum: 97.99922466278076
time_elpased: 1.517
batch start
#iterations: 84
currently lose_sum: 98.08290195465088
time_elpased: 1.495
batch start
#iterations: 85
currently lose_sum: 97.81074523925781
time_elpased: 1.521
batch start
#iterations: 86
currently lose_sum: 98.23459506034851
time_elpased: 1.505
batch start
#iterations: 87
currently lose_sum: 98.01225304603577
time_elpased: 1.522
batch start
#iterations: 88
currently lose_sum: 97.97224080562592
time_elpased: 1.49
batch start
#iterations: 89
currently lose_sum: 98.02039134502411
time_elpased: 1.495
batch start
#iterations: 90
currently lose_sum: 97.81147307157516
time_elpased: 1.518
batch start
#iterations: 91
currently lose_sum: 98.05141305923462
time_elpased: 1.514
batch start
#iterations: 92
currently lose_sum: 97.9137111902237
time_elpased: 1.537
batch start
#iterations: 93
currently lose_sum: 97.86836183071136
time_elpased: 1.511
batch start
#iterations: 94
currently lose_sum: 98.06713485717773
time_elpased: 1.557
batch start
#iterations: 95
currently lose_sum: 97.9412779211998
time_elpased: 1.488
batch start
#iterations: 96
currently lose_sum: 97.66254651546478
time_elpased: 1.563
batch start
#iterations: 97
currently lose_sum: 97.60915184020996
time_elpased: 1.522
batch start
#iterations: 98
currently lose_sum: 97.66333556175232
time_elpased: 1.525
batch start
#iterations: 99
currently lose_sum: 97.56106561422348
time_elpased: 1.51
start validation test
0.589948453608
0.613414006179
0.490377688587
0.545038604518
0.590123265444
65.468
batch start
#iterations: 100
currently lose_sum: 97.76324492692947
time_elpased: 1.554
batch start
#iterations: 101
currently lose_sum: 97.59272736310959
time_elpased: 1.59
batch start
#iterations: 102
currently lose_sum: 98.1408833861351
time_elpased: 1.511
batch start
#iterations: 103
currently lose_sum: 97.48507291078568
time_elpased: 1.52
batch start
#iterations: 104
currently lose_sum: 97.71215331554413
time_elpased: 1.543
batch start
#iterations: 105
currently lose_sum: 97.62086415290833
time_elpased: 1.547
batch start
#iterations: 106
currently lose_sum: 97.8409675359726
time_elpased: 1.525
batch start
#iterations: 107
currently lose_sum: 97.48281276226044
time_elpased: 1.535
batch start
#iterations: 108
currently lose_sum: 97.73281854391098
time_elpased: 1.515
batch start
#iterations: 109
currently lose_sum: 97.44998049736023
time_elpased: 1.523
batch start
#iterations: 110
currently lose_sum: 97.56622004508972
time_elpased: 1.505
batch start
#iterations: 111
currently lose_sum: 97.39441961050034
time_elpased: 1.56
batch start
#iterations: 112
currently lose_sum: 97.64749699831009
time_elpased: 1.594
batch start
#iterations: 113
currently lose_sum: 97.25552606582642
time_elpased: 1.519
batch start
#iterations: 114
currently lose_sum: 97.29681223630905
time_elpased: 1.521
batch start
#iterations: 115
currently lose_sum: 97.40967082977295
time_elpased: 1.55
batch start
#iterations: 116
currently lose_sum: 97.48121500015259
time_elpased: 1.533
batch start
#iterations: 117
currently lose_sum: 96.8708758354187
time_elpased: 1.516
batch start
#iterations: 118
currently lose_sum: 97.45623576641083
time_elpased: 1.529
batch start
#iterations: 119
currently lose_sum: 97.55151093006134
time_elpased: 1.519
start validation test
0.583195876289
0.612374259336
0.457342801276
0.523624366678
0.583416830773
66.402
batch start
#iterations: 120
currently lose_sum: 97.44676840305328
time_elpased: 1.528
batch start
#iterations: 121
currently lose_sum: 97.16179764270782
time_elpased: 1.529
batch start
#iterations: 122
currently lose_sum: 97.62148141860962
time_elpased: 1.548
batch start
#iterations: 123
currently lose_sum: 97.45322734117508
time_elpased: 1.532
batch start
#iterations: 124
currently lose_sum: 97.14780694246292
time_elpased: 1.504
batch start
#iterations: 125
currently lose_sum: 97.37489753961563
time_elpased: 1.492
batch start
#iterations: 126
currently lose_sum: 96.93546855449677
time_elpased: 1.523
batch start
#iterations: 127
currently lose_sum: 97.0796440243721
time_elpased: 1.516
batch start
#iterations: 128
currently lose_sum: 97.5221117734909
time_elpased: 1.521
batch start
#iterations: 129
currently lose_sum: 97.33468037843704
time_elpased: 1.514
batch start
#iterations: 130
currently lose_sum: 97.1429448723793
time_elpased: 1.531
batch start
#iterations: 131
currently lose_sum: 97.11956185102463
time_elpased: 1.539
batch start
#iterations: 132
currently lose_sum: 97.28497552871704
time_elpased: 1.529
batch start
#iterations: 133
currently lose_sum: 97.25973272323608
time_elpased: 1.522
batch start
#iterations: 134
currently lose_sum: 97.18989908695221
time_elpased: 1.549
batch start
#iterations: 135
currently lose_sum: 96.97130566835403
time_elpased: 1.537
batch start
#iterations: 136
currently lose_sum: 96.92343032360077
time_elpased: 1.56
batch start
#iterations: 137
currently lose_sum: 97.29939723014832
time_elpased: 1.514
batch start
#iterations: 138
currently lose_sum: 96.63778245449066
time_elpased: 1.53
batch start
#iterations: 139
currently lose_sum: 97.01768016815186
time_elpased: 1.54
start validation test
0.58706185567
0.599370922647
0.529484408768
0.562264357139
0.587162941759
65.691
batch start
#iterations: 140
currently lose_sum: 96.7480599284172
time_elpased: 1.544
batch start
#iterations: 141
currently lose_sum: 97.00414854288101
time_elpased: 1.51
batch start
#iterations: 142
currently lose_sum: 96.71998238563538
time_elpased: 1.565
batch start
#iterations: 143
currently lose_sum: 97.25581765174866
time_elpased: 1.494
batch start
#iterations: 144
currently lose_sum: 96.92615222930908
time_elpased: 1.502
batch start
#iterations: 145
currently lose_sum: 96.83182567358017
time_elpased: 1.557
batch start
#iterations: 146
currently lose_sum: 96.52834105491638
time_elpased: 1.518
batch start
#iterations: 147
currently lose_sum: 96.46546524763107
time_elpased: 1.567
batch start
#iterations: 148
currently lose_sum: 96.54307919740677
time_elpased: 1.511
batch start
#iterations: 149
currently lose_sum: 96.75115275382996
time_elpased: 1.501
batch start
#iterations: 150
currently lose_sum: 96.46114075183868
time_elpased: 1.569
batch start
#iterations: 151
currently lose_sum: 96.81889909505844
time_elpased: 1.516
batch start
#iterations: 152
currently lose_sum: 96.68263936042786
time_elpased: 1.544
batch start
#iterations: 153
currently lose_sum: 96.84201794862747
time_elpased: 1.587
batch start
#iterations: 154
currently lose_sum: 96.7982080578804
time_elpased: 1.536
batch start
#iterations: 155
currently lose_sum: 96.5563702583313
time_elpased: 1.55
batch start
#iterations: 156
currently lose_sum: 96.5592913031578
time_elpased: 1.514
batch start
#iterations: 157
currently lose_sum: 96.3810727596283
time_elpased: 1.528
batch start
#iterations: 158
currently lose_sum: 96.30020248889923
time_elpased: 1.523
batch start
#iterations: 159
currently lose_sum: 96.55180805921555
time_elpased: 1.517
start validation test
0.582113402062
0.623542050338
0.418133168673
0.500585227623
0.582401294654
65.685
batch start
#iterations: 160
currently lose_sum: 96.65246027708054
time_elpased: 1.519
batch start
#iterations: 161
currently lose_sum: 96.59787321090698
time_elpased: 1.53
batch start
#iterations: 162
currently lose_sum: 96.52337980270386
time_elpased: 1.525
batch start
#iterations: 163
currently lose_sum: 96.73835694789886
time_elpased: 1.514
batch start
#iterations: 164
currently lose_sum: 96.14209187030792
time_elpased: 1.518
batch start
#iterations: 165
currently lose_sum: 96.6491910815239
time_elpased: 1.532
batch start
#iterations: 166
currently lose_sum: 96.09101492166519
time_elpased: 1.506
batch start
#iterations: 167
currently lose_sum: 96.65124809741974
time_elpased: 1.507
batch start
#iterations: 168
currently lose_sum: 96.28370875120163
time_elpased: 1.496
batch start
#iterations: 169
currently lose_sum: 96.54526370763779
time_elpased: 1.527
batch start
#iterations: 170
currently lose_sum: 96.40138363838196
time_elpased: 1.517
batch start
#iterations: 171
currently lose_sum: 96.28821229934692
time_elpased: 1.528
batch start
#iterations: 172
currently lose_sum: 96.39749324321747
time_elpased: 1.523
batch start
#iterations: 173
currently lose_sum: 96.1898033618927
time_elpased: 1.531
batch start
#iterations: 174
currently lose_sum: 96.20693415403366
time_elpased: 1.496
batch start
#iterations: 175
currently lose_sum: 96.10793596506119
time_elpased: 1.516
batch start
#iterations: 176
currently lose_sum: 96.11220669746399
time_elpased: 1.514
batch start
#iterations: 177
currently lose_sum: 96.24737322330475
time_elpased: 1.541
batch start
#iterations: 178
currently lose_sum: 96.4122987985611
time_elpased: 1.499
batch start
#iterations: 179
currently lose_sum: 95.88328784704208
time_elpased: 1.555
start validation test
0.58706185567
0.620548332391
0.451888442935
0.522955993569
0.587299173445
66.128
batch start
#iterations: 180
currently lose_sum: 96.18929475545883
time_elpased: 1.59
batch start
#iterations: 181
currently lose_sum: 96.22692722082138
time_elpased: 1.58
batch start
#iterations: 182
currently lose_sum: 96.37431347370148
time_elpased: 1.508
batch start
#iterations: 183
currently lose_sum: 95.98772370815277
time_elpased: 1.53
batch start
#iterations: 184
currently lose_sum: 96.22445237636566
time_elpased: 1.519
batch start
#iterations: 185
currently lose_sum: 96.27366930246353
time_elpased: 1.516
batch start
#iterations: 186
currently lose_sum: 96.02785497903824
time_elpased: 1.507
batch start
#iterations: 187
currently lose_sum: 96.05222147703171
time_elpased: 1.543
batch start
#iterations: 188
currently lose_sum: 96.1515007019043
time_elpased: 1.532
batch start
#iterations: 189
currently lose_sum: 96.09344291687012
time_elpased: 1.509
batch start
#iterations: 190
currently lose_sum: 95.95345401763916
time_elpased: 1.525
batch start
#iterations: 191
currently lose_sum: 95.72014206647873
time_elpased: 1.498
batch start
#iterations: 192
currently lose_sum: 95.72195595502853
time_elpased: 1.534
batch start
#iterations: 193
currently lose_sum: 96.15093892812729
time_elpased: 1.533
batch start
#iterations: 194
currently lose_sum: 95.87533444166183
time_elpased: 1.595
batch start
#iterations: 195
currently lose_sum: 95.97011309862137
time_elpased: 1.574
batch start
#iterations: 196
currently lose_sum: 95.82371085882187
time_elpased: 1.518
batch start
#iterations: 197
currently lose_sum: 95.7764224410057
time_elpased: 1.518
batch start
#iterations: 198
currently lose_sum: 95.71262472867966
time_elpased: 1.527
batch start
#iterations: 199
currently lose_sum: 96.1913595199585
time_elpased: 1.52
start validation test
0.584896907216
0.614568989259
0.459298137285
0.525708227811
0.585117415229
66.344
batch start
#iterations: 200
currently lose_sum: 96.07413738965988
time_elpased: 1.53
batch start
#iterations: 201
currently lose_sum: 96.02898126840591
time_elpased: 1.487
batch start
#iterations: 202
currently lose_sum: 95.9505142569542
time_elpased: 1.589
batch start
#iterations: 203
currently lose_sum: 95.63526058197021
time_elpased: 1.507
batch start
#iterations: 204
currently lose_sum: 95.71187388896942
time_elpased: 1.524
batch start
#iterations: 205
currently lose_sum: 95.61810564994812
time_elpased: 1.528
batch start
#iterations: 206
currently lose_sum: 95.74983304738998
time_elpased: 1.519
batch start
#iterations: 207
currently lose_sum: 95.93684935569763
time_elpased: 1.528
batch start
#iterations: 208
currently lose_sum: 95.60417377948761
time_elpased: 1.578
batch start
#iterations: 209
currently lose_sum: 95.6696344614029
time_elpased: 1.522
batch start
#iterations: 210
currently lose_sum: 95.55187863111496
time_elpased: 1.542
batch start
#iterations: 211
currently lose_sum: 95.61069285869598
time_elpased: 1.527
batch start
#iterations: 212
currently lose_sum: 95.76081442832947
time_elpased: 1.573
batch start
#iterations: 213
currently lose_sum: 95.53666013479233
time_elpased: 1.539
batch start
#iterations: 214
currently lose_sum: 95.69285500049591
time_elpased: 1.538
batch start
#iterations: 215
currently lose_sum: 95.73891758918762
time_elpased: 1.532
batch start
#iterations: 216
currently lose_sum: 95.61190676689148
time_elpased: 1.538
batch start
#iterations: 217
currently lose_sum: 95.55997610092163
time_elpased: 1.504
batch start
#iterations: 218
currently lose_sum: 95.33206951618195
time_elpased: 1.502
batch start
#iterations: 219
currently lose_sum: 95.35007405281067
time_elpased: 1.529
start validation test
0.580463917526
0.62624
0.402799217866
0.49026116365
0.580775835309
67.543
batch start
#iterations: 220
currently lose_sum: 95.96104329824448
time_elpased: 1.545
batch start
#iterations: 221
currently lose_sum: 95.2486629486084
time_elpased: 1.543
batch start
#iterations: 222
currently lose_sum: 95.61555761098862
time_elpased: 1.533
batch start
#iterations: 223
currently lose_sum: 95.4899315237999
time_elpased: 1.529
batch start
#iterations: 224
currently lose_sum: 95.58696937561035
time_elpased: 1.496
batch start
#iterations: 225
currently lose_sum: 95.40961706638336
time_elpased: 1.573
batch start
#iterations: 226
currently lose_sum: 95.57794147729874
time_elpased: 1.566
batch start
#iterations: 227
currently lose_sum: 95.26966065168381
time_elpased: 1.53
batch start
#iterations: 228
currently lose_sum: 95.59846037626266
time_elpased: 1.512
batch start
#iterations: 229
currently lose_sum: 95.67533975839615
time_elpased: 1.523
batch start
#iterations: 230
currently lose_sum: 95.06921815872192
time_elpased: 1.535
batch start
#iterations: 231
currently lose_sum: 94.91418939828873
time_elpased: 1.516
batch start
#iterations: 232
currently lose_sum: 95.06167018413544
time_elpased: 1.524
batch start
#iterations: 233
currently lose_sum: 94.94843822717667
time_elpased: 1.518
batch start
#iterations: 234
currently lose_sum: 95.104685485363
time_elpased: 1.559
batch start
#iterations: 235
currently lose_sum: 95.34856194257736
time_elpased: 1.534
batch start
#iterations: 236
currently lose_sum: 95.36018735170364
time_elpased: 1.548
batch start
#iterations: 237
currently lose_sum: 94.92418307065964
time_elpased: 1.519
batch start
#iterations: 238
currently lose_sum: 95.1690661907196
time_elpased: 1.506
batch start
#iterations: 239
currently lose_sum: 94.9752881526947
time_elpased: 1.533
start validation test
0.580618556701
0.622577143743
0.41319337244
0.496721514289
0.580912497436
67.586
batch start
#iterations: 240
currently lose_sum: 95.10953843593597
time_elpased: 1.507
batch start
#iterations: 241
currently lose_sum: 95.29405277967453
time_elpased: 1.571
batch start
#iterations: 242
currently lose_sum: 94.95729517936707
time_elpased: 1.547
batch start
#iterations: 243
currently lose_sum: 95.10882991552353
time_elpased: 1.499
batch start
#iterations: 244
currently lose_sum: 95.15331602096558
time_elpased: 1.54
batch start
#iterations: 245
currently lose_sum: 94.99377244710922
time_elpased: 1.506
batch start
#iterations: 246
currently lose_sum: 95.09719729423523
time_elpased: 1.514
batch start
#iterations: 247
currently lose_sum: 95.00642997026443
time_elpased: 1.571
batch start
#iterations: 248
currently lose_sum: 95.2169868350029
time_elpased: 1.58
batch start
#iterations: 249
currently lose_sum: 94.643445789814
time_elpased: 1.538
batch start
#iterations: 250
currently lose_sum: 95.07415115833282
time_elpased: 1.54
batch start
#iterations: 251
currently lose_sum: 94.77062654495239
time_elpased: 1.526
batch start
#iterations: 252
currently lose_sum: 94.78713208436966
time_elpased: 1.52
batch start
#iterations: 253
currently lose_sum: 94.87667483091354
time_elpased: 1.559
batch start
#iterations: 254
currently lose_sum: 94.85987639427185
time_elpased: 1.505
batch start
#iterations: 255
currently lose_sum: 95.04754549264908
time_elpased: 1.528
batch start
#iterations: 256
currently lose_sum: 94.75371193885803
time_elpased: 1.505
batch start
#iterations: 257
currently lose_sum: 94.9500362277031
time_elpased: 1.529
batch start
#iterations: 258
currently lose_sum: 94.9565099477768
time_elpased: 1.544
batch start
#iterations: 259
currently lose_sum: 94.47643095254898
time_elpased: 1.516
start validation test
0.582216494845
0.61905465288
0.431305958629
0.508400558015
0.582481441568
68.092
batch start
#iterations: 260
currently lose_sum: 94.83170837163925
time_elpased: 1.514
batch start
#iterations: 261
currently lose_sum: 94.44098573923111
time_elpased: 1.519
batch start
#iterations: 262
currently lose_sum: 94.63129132986069
time_elpased: 1.485
batch start
#iterations: 263
currently lose_sum: 94.77690804004669
time_elpased: 1.514
batch start
#iterations: 264
currently lose_sum: 94.78926914930344
time_elpased: 1.512
batch start
#iterations: 265
currently lose_sum: 94.29575687646866
time_elpased: 1.535
batch start
#iterations: 266
currently lose_sum: 94.66344618797302
time_elpased: 1.522
batch start
#iterations: 267
currently lose_sum: 94.9909405708313
time_elpased: 1.525
batch start
#iterations: 268
currently lose_sum: 94.8148934841156
time_elpased: 1.521
batch start
#iterations: 269
currently lose_sum: 94.70217138528824
time_elpased: 1.505
batch start
#iterations: 270
currently lose_sum: 94.73299705982208
time_elpased: 1.56
batch start
#iterations: 271
currently lose_sum: 94.41319769620895
time_elpased: 1.503
batch start
#iterations: 272
currently lose_sum: 94.59750097990036
time_elpased: 1.517
batch start
#iterations: 273
currently lose_sum: 94.47553318738937
time_elpased: 1.52
batch start
#iterations: 274
currently lose_sum: 95.12820219993591
time_elpased: 1.497
batch start
#iterations: 275
currently lose_sum: 94.82116377353668
time_elpased: 1.525
batch start
#iterations: 276
currently lose_sum: 94.86563456058502
time_elpased: 1.557
batch start
#iterations: 277
currently lose_sum: 94.46700835227966
time_elpased: 1.52
batch start
#iterations: 278
currently lose_sum: 94.27982211112976
time_elpased: 1.503
batch start
#iterations: 279
currently lose_sum: 94.80355060100555
time_elpased: 1.552
start validation test
0.578556701031
0.610783608914
0.437171966656
0.509596928983
0.578804923739
68.334
batch start
#iterations: 280
currently lose_sum: 94.38055211305618
time_elpased: 1.515
batch start
#iterations: 281
currently lose_sum: 93.93132477998734
time_elpased: 1.523
batch start
#iterations: 282
currently lose_sum: 94.399658203125
time_elpased: 1.543
batch start
#iterations: 283
currently lose_sum: 94.16129511594772
time_elpased: 1.554
batch start
#iterations: 284
currently lose_sum: 94.21127372980118
time_elpased: 1.527
batch start
#iterations: 285
currently lose_sum: 94.22251665592194
time_elpased: 1.505
batch start
#iterations: 286
currently lose_sum: 94.60266679525375
time_elpased: 1.509
batch start
#iterations: 287
currently lose_sum: 94.28803426027298
time_elpased: 1.533
batch start
#iterations: 288
currently lose_sum: 94.26931685209274
time_elpased: 1.515
batch start
#iterations: 289
currently lose_sum: 94.30674117803574
time_elpased: 1.532
batch start
#iterations: 290
currently lose_sum: 94.19213956594467
time_elpased: 1.536
batch start
#iterations: 291
currently lose_sum: 94.26958924531937
time_elpased: 1.5
batch start
#iterations: 292
currently lose_sum: 94.07685893774033
time_elpased: 1.511
batch start
#iterations: 293
currently lose_sum: 94.2943822145462
time_elpased: 1.525
batch start
#iterations: 294
currently lose_sum: 94.50773763656616
time_elpased: 1.519
batch start
#iterations: 295
currently lose_sum: 94.65135776996613
time_elpased: 1.523
batch start
#iterations: 296
currently lose_sum: 94.07463377714157
time_elpased: 1.553
batch start
#iterations: 297
currently lose_sum: 93.95227289199829
time_elpased: 1.526
batch start
#iterations: 298
currently lose_sum: 93.9356918334961
time_elpased: 1.53
batch start
#iterations: 299
currently lose_sum: 94.32386833429337
time_elpased: 1.509
start validation test
0.582113402062
0.618591632292
0.432129258001
0.508815510451
0.58237672236
68.346
batch start
#iterations: 300
currently lose_sum: 93.89881819486618
time_elpased: 1.518
batch start
#iterations: 301
currently lose_sum: 94.36571753025055
time_elpased: 1.563
batch start
#iterations: 302
currently lose_sum: 93.81207245588303
time_elpased: 1.512
batch start
#iterations: 303
currently lose_sum: 93.93538028001785
time_elpased: 1.521
batch start
#iterations: 304
currently lose_sum: 94.31995415687561
time_elpased: 1.528
batch start
#iterations: 305
currently lose_sum: 93.78909343481064
time_elpased: 1.523
batch start
#iterations: 306
currently lose_sum: 93.93639707565308
time_elpased: 1.526
batch start
#iterations: 307
currently lose_sum: 94.07095557451248
time_elpased: 1.548
batch start
#iterations: 308
currently lose_sum: 93.88312643766403
time_elpased: 1.495
batch start
#iterations: 309
currently lose_sum: 93.96937447786331
time_elpased: 1.601
batch start
#iterations: 310
currently lose_sum: 93.73152929544449
time_elpased: 1.494
batch start
#iterations: 311
currently lose_sum: 93.73685365915298
time_elpased: 1.551
batch start
#iterations: 312
currently lose_sum: 94.08119887113571
time_elpased: 1.528
batch start
#iterations: 313
currently lose_sum: 93.56113868951797
time_elpased: 1.577
batch start
#iterations: 314
currently lose_sum: 93.87172102928162
time_elpased: 1.509
batch start
#iterations: 315
currently lose_sum: 93.82582032680511
time_elpased: 1.501
batch start
#iterations: 316
currently lose_sum: 93.53119629621506
time_elpased: 1.519
batch start
#iterations: 317
currently lose_sum: 94.02067577838898
time_elpased: 1.537
batch start
#iterations: 318
currently lose_sum: 93.64578503370285
time_elpased: 1.511
batch start
#iterations: 319
currently lose_sum: 93.84027308225632
time_elpased: 1.508
start validation test
0.582268041237
0.613639566014
0.44818359576
0.518020697038
0.582503447162
70.085
batch start
#iterations: 320
currently lose_sum: 93.39119058847427
time_elpased: 1.538
batch start
#iterations: 321
currently lose_sum: 93.67256432771683
time_elpased: 1.543
batch start
#iterations: 322
currently lose_sum: 93.62211519479752
time_elpased: 1.524
batch start
#iterations: 323
currently lose_sum: 93.55150610208511
time_elpased: 1.556
batch start
#iterations: 324
currently lose_sum: 93.89779704809189
time_elpased: 1.518
batch start
#iterations: 325
currently lose_sum: 94.05104887485504
time_elpased: 1.509
batch start
#iterations: 326
currently lose_sum: 93.48370450735092
time_elpased: 1.53
batch start
#iterations: 327
currently lose_sum: 93.46867114305496
time_elpased: 1.531
batch start
#iterations: 328
currently lose_sum: 94.1324742436409
time_elpased: 1.529
batch start
#iterations: 329
currently lose_sum: 93.0343713760376
time_elpased: 1.565
batch start
#iterations: 330
currently lose_sum: 93.87094229459763
time_elpased: 1.567
batch start
#iterations: 331
currently lose_sum: 93.55694687366486
time_elpased: 1.529
batch start
#iterations: 332
currently lose_sum: 93.36452585458755
time_elpased: 1.526
batch start
#iterations: 333
currently lose_sum: 93.74769079685211
time_elpased: 1.576
batch start
#iterations: 334
currently lose_sum: 93.96559476852417
time_elpased: 1.527
batch start
#iterations: 335
currently lose_sum: 93.1069506406784
time_elpased: 1.527
batch start
#iterations: 336
currently lose_sum: 93.32108092308044
time_elpased: 1.514
batch start
#iterations: 337
currently lose_sum: 93.89801049232483
time_elpased: 1.528
batch start
#iterations: 338
currently lose_sum: 93.40633189678192
time_elpased: 1.565
batch start
#iterations: 339
currently lose_sum: 93.38483107089996
time_elpased: 1.52
start validation test
0.580670103093
0.630055902664
0.3943603993
0.485093993291
0.580997198514
69.462
batch start
#iterations: 340
currently lose_sum: 93.2676237821579
time_elpased: 1.524
batch start
#iterations: 341
currently lose_sum: 93.06317031383514
time_elpased: 1.523
batch start
#iterations: 342
currently lose_sum: 93.40544468164444
time_elpased: 1.532
batch start
#iterations: 343
currently lose_sum: 92.63142734766006
time_elpased: 1.536
batch start
#iterations: 344
currently lose_sum: 92.98203074932098
time_elpased: 1.551
batch start
#iterations: 345
currently lose_sum: 93.49060583114624
time_elpased: 1.519
batch start
#iterations: 346
currently lose_sum: 93.58149445056915
time_elpased: 1.528
batch start
#iterations: 347
currently lose_sum: 92.52155524492264
time_elpased: 1.56
batch start
#iterations: 348
currently lose_sum: 93.47012907266617
time_elpased: 1.606
batch start
#iterations: 349
currently lose_sum: 92.96912997961044
time_elpased: 1.55
batch start
#iterations: 350
currently lose_sum: 93.2361211180687
time_elpased: 1.559
batch start
#iterations: 351
currently lose_sum: 93.3834040760994
time_elpased: 1.514
batch start
#iterations: 352
currently lose_sum: 93.03475964069366
time_elpased: 1.549
batch start
#iterations: 353
currently lose_sum: 92.87911301851273
time_elpased: 1.521
batch start
#iterations: 354
currently lose_sum: 93.34027433395386
time_elpased: 1.543
batch start
#iterations: 355
currently lose_sum: 92.84744679927826
time_elpased: 1.546
batch start
#iterations: 356
currently lose_sum: 92.69540894031525
time_elpased: 1.574
batch start
#iterations: 357
currently lose_sum: 92.9861091375351
time_elpased: 1.531
batch start
#iterations: 358
currently lose_sum: 92.7747665643692
time_elpased: 1.555
batch start
#iterations: 359
currently lose_sum: 93.41874474287033
time_elpased: 1.58
start validation test
0.57824742268
0.621690185508
0.403519604816
0.48939091363
0.578554184315
70.114
batch start
#iterations: 360
currently lose_sum: 92.7708352804184
time_elpased: 1.505
batch start
#iterations: 361
currently lose_sum: 93.10365843772888
time_elpased: 1.547
batch start
#iterations: 362
currently lose_sum: 92.69844079017639
time_elpased: 1.52
batch start
#iterations: 363
currently lose_sum: 92.91974079608917
time_elpased: 1.544
batch start
#iterations: 364
currently lose_sum: 92.81045466661453
time_elpased: 1.518
batch start
#iterations: 365
currently lose_sum: 92.92037677764893
time_elpased: 1.563
batch start
#iterations: 366
currently lose_sum: 92.91517102718353
time_elpased: 1.55
batch start
#iterations: 367
currently lose_sum: 92.68584090471268
time_elpased: 1.556
batch start
#iterations: 368
currently lose_sum: 93.08208405971527
time_elpased: 1.512
batch start
#iterations: 369
currently lose_sum: 92.89646595716476
time_elpased: 1.531
batch start
#iterations: 370
currently lose_sum: 92.67666280269623
time_elpased: 1.501
batch start
#iterations: 371
currently lose_sum: 92.39168691635132
time_elpased: 1.55
batch start
#iterations: 372
currently lose_sum: 92.43443578481674
time_elpased: 1.54
batch start
#iterations: 373
currently lose_sum: 92.71232974529266
time_elpased: 1.488
batch start
#iterations: 374
currently lose_sum: 92.92311185598373
time_elpased: 1.525
batch start
#iterations: 375
currently lose_sum: 92.46065431833267
time_elpased: 1.524
batch start
#iterations: 376
currently lose_sum: 92.52604687213898
time_elpased: 1.521
batch start
#iterations: 377
currently lose_sum: 92.6869957447052
time_elpased: 1.531
batch start
#iterations: 378
currently lose_sum: 92.74252390861511
time_elpased: 1.53
batch start
#iterations: 379
currently lose_sum: 92.78156685829163
time_elpased: 1.551
start validation test
0.57618556701
0.611218568665
0.422764227642
0.499817496046
0.576454921835
70.857
batch start
#iterations: 380
currently lose_sum: 92.21503937244415
time_elpased: 1.55
batch start
#iterations: 381
currently lose_sum: 92.53294849395752
time_elpased: 1.52
batch start
#iterations: 382
currently lose_sum: 92.33605027198792
time_elpased: 1.505
batch start
#iterations: 383
currently lose_sum: 92.37132668495178
time_elpased: 1.517
batch start
#iterations: 384
currently lose_sum: 92.77822208404541
time_elpased: 1.538
batch start
#iterations: 385
currently lose_sum: 92.60704350471497
time_elpased: 1.543
batch start
#iterations: 386
currently lose_sum: 92.7655798792839
time_elpased: 1.521
batch start
#iterations: 387
currently lose_sum: 92.38539350032806
time_elpased: 1.524
batch start
#iterations: 388
currently lose_sum: 92.18067246675491
time_elpased: 1.532
batch start
#iterations: 389
currently lose_sum: 92.34280335903168
time_elpased: 1.546
batch start
#iterations: 390
currently lose_sum: 92.42879420518875
time_elpased: 1.517
batch start
#iterations: 391
currently lose_sum: 92.61931520700455
time_elpased: 1.509
batch start
#iterations: 392
currently lose_sum: 92.48880243301392
time_elpased: 1.503
batch start
#iterations: 393
currently lose_sum: 92.40929335355759
time_elpased: 1.52
batch start
#iterations: 394
currently lose_sum: 92.09115052223206
time_elpased: 1.535
batch start
#iterations: 395
currently lose_sum: 91.81485468149185
time_elpased: 1.545
batch start
#iterations: 396
currently lose_sum: 91.9828639626503
time_elpased: 1.552
batch start
#iterations: 397
currently lose_sum: 92.49161970615387
time_elpased: 1.523
batch start
#iterations: 398
currently lose_sum: 92.63715475797653
time_elpased: 1.546
batch start
#iterations: 399
currently lose_sum: 92.07218164205551
time_elpased: 1.513
start validation test
0.581958762887
0.628129484931
0.405372028404
0.492744558419
0.582268788136
71.452
acc: 0.596
pre: 0.615
rec: 0.514
F1: 0.560
auc: 0.623
