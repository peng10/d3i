start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.33874368667603
time_elpased: 1.536
batch start
#iterations: 1
currently lose_sum: 100.31103515625
time_elpased: 1.518
batch start
#iterations: 2
currently lose_sum: 100.16656047105789
time_elpased: 1.569
batch start
#iterations: 3
currently lose_sum: 100.0576828122139
time_elpased: 1.509
batch start
#iterations: 4
currently lose_sum: 99.90412896871567
time_elpased: 1.543
batch start
#iterations: 5
currently lose_sum: 99.86492985486984
time_elpased: 1.529
batch start
#iterations: 6
currently lose_sum: 99.84972012042999
time_elpased: 1.521
batch start
#iterations: 7
currently lose_sum: 99.5477055311203
time_elpased: 1.522
batch start
#iterations: 8
currently lose_sum: 99.74603694677353
time_elpased: 1.491
batch start
#iterations: 9
currently lose_sum: 99.78769493103027
time_elpased: 1.488
batch start
#iterations: 10
currently lose_sum: 99.42884182929993
time_elpased: 1.54
batch start
#iterations: 11
currently lose_sum: 99.46848303079605
time_elpased: 1.543
batch start
#iterations: 12
currently lose_sum: 99.49360287189484
time_elpased: 1.568
batch start
#iterations: 13
currently lose_sum: 99.66457360982895
time_elpased: 1.539
batch start
#iterations: 14
currently lose_sum: 99.51119720935822
time_elpased: 1.511
batch start
#iterations: 15
currently lose_sum: 99.46342712640762
time_elpased: 1.525
batch start
#iterations: 16
currently lose_sum: 99.49745339155197
time_elpased: 1.508
batch start
#iterations: 17
currently lose_sum: 99.45493012666702
time_elpased: 1.501
batch start
#iterations: 18
currently lose_sum: 99.34246730804443
time_elpased: 1.488
batch start
#iterations: 19
currently lose_sum: 99.18936067819595
time_elpased: 1.505
start validation test
0.588195876289
0.591871475689
0.572560724578
0.582055976981
0.58822170884
65.578
batch start
#iterations: 20
currently lose_sum: 99.354467689991
time_elpased: 1.604
batch start
#iterations: 21
currently lose_sum: 99.31234163045883
time_elpased: 1.563
batch start
#iterations: 22
currently lose_sum: 99.2598340511322
time_elpased: 1.488
batch start
#iterations: 23
currently lose_sum: 99.04300063848495
time_elpased: 1.505
batch start
#iterations: 24
currently lose_sum: 98.94682002067566
time_elpased: 1.493
batch start
#iterations: 25
currently lose_sum: 98.99137663841248
time_elpased: 1.545
batch start
#iterations: 26
currently lose_sum: 99.10895735025406
time_elpased: 1.53
batch start
#iterations: 27
currently lose_sum: 99.20926332473755
time_elpased: 1.525
batch start
#iterations: 28
currently lose_sum: 99.16115325689316
time_elpased: 1.475
batch start
#iterations: 29
currently lose_sum: 99.24978345632553
time_elpased: 1.554
batch start
#iterations: 30
currently lose_sum: 98.87462258338928
time_elpased: 1.517
batch start
#iterations: 31
currently lose_sum: 98.92678815126419
time_elpased: 1.527
batch start
#iterations: 32
currently lose_sum: 99.17426216602325
time_elpased: 1.617
batch start
#iterations: 33
currently lose_sum: 99.02877134084702
time_elpased: 1.51
batch start
#iterations: 34
currently lose_sum: 99.06123471260071
time_elpased: 1.538
batch start
#iterations: 35
currently lose_sum: 99.01310956478119
time_elpased: 1.522
batch start
#iterations: 36
currently lose_sum: 99.02069646120071
time_elpased: 1.518
batch start
#iterations: 37
currently lose_sum: 98.94159638881683
time_elpased: 1.529
batch start
#iterations: 38
currently lose_sum: 98.83051747083664
time_elpased: 1.483
batch start
#iterations: 39
currently lose_sum: 98.66758292913437
time_elpased: 1.503
start validation test
0.584536082474
0.588085106383
0.568958419103
0.578363674409
0.584561820043
65.544
batch start
#iterations: 40
currently lose_sum: 98.82999980449677
time_elpased: 1.537
batch start
#iterations: 41
currently lose_sum: 98.96030920743942
time_elpased: 1.553
batch start
#iterations: 42
currently lose_sum: 98.86386853456497
time_elpased: 1.5
batch start
#iterations: 43
currently lose_sum: 98.76158136129379
time_elpased: 1.495
batch start
#iterations: 44
currently lose_sum: 98.58004599809647
time_elpased: 1.521
batch start
#iterations: 45
currently lose_sum: 99.06027978658676
time_elpased: 1.541
batch start
#iterations: 46
currently lose_sum: 98.53420585393906
time_elpased: 1.483
batch start
#iterations: 47
currently lose_sum: 98.81737697124481
time_elpased: 1.545
batch start
#iterations: 48
currently lose_sum: 98.65297424793243
time_elpased: 1.492
batch start
#iterations: 49
currently lose_sum: 98.79552018642426
time_elpased: 1.49
batch start
#iterations: 50
currently lose_sum: 98.77300757169724
time_elpased: 1.501
batch start
#iterations: 51
currently lose_sum: 98.58215814828873
time_elpased: 1.532
batch start
#iterations: 52
currently lose_sum: 98.34846293926239
time_elpased: 1.51
batch start
#iterations: 53
currently lose_sum: 98.40729814767838
time_elpased: 1.505
batch start
#iterations: 54
currently lose_sum: 98.55746448040009
time_elpased: 1.52
batch start
#iterations: 55
currently lose_sum: 98.67912936210632
time_elpased: 1.556
batch start
#iterations: 56
currently lose_sum: 98.47962832450867
time_elpased: 1.474
batch start
#iterations: 57
currently lose_sum: 98.61850368976593
time_elpased: 1.527
batch start
#iterations: 58
currently lose_sum: 98.56116384267807
time_elpased: 1.594
batch start
#iterations: 59
currently lose_sum: 98.53670573234558
time_elpased: 1.51
start validation test
0.581958762887
0.624226485149
0.415294359819
0.498763906057
0.582234127452
65.483
batch start
#iterations: 60
currently lose_sum: 98.33476287126541
time_elpased: 1.531
batch start
#iterations: 61
currently lose_sum: 98.54812282323837
time_elpased: 1.521
batch start
#iterations: 62
currently lose_sum: 98.46715569496155
time_elpased: 1.512
batch start
#iterations: 63
currently lose_sum: 98.35154408216476
time_elpased: 1.535
batch start
#iterations: 64
currently lose_sum: 98.3968842625618
time_elpased: 1.551
batch start
#iterations: 65
currently lose_sum: 98.14487898349762
time_elpased: 1.494
batch start
#iterations: 66
currently lose_sum: 98.41765576601028
time_elpased: 1.517
batch start
#iterations: 67
currently lose_sum: 98.30743265151978
time_elpased: 1.532
batch start
#iterations: 68
currently lose_sum: 98.31752300262451
time_elpased: 1.52
batch start
#iterations: 69
currently lose_sum: 98.21301692724228
time_elpased: 1.54
batch start
#iterations: 70
currently lose_sum: 98.17106503248215
time_elpased: 1.527
batch start
#iterations: 71
currently lose_sum: 98.25568652153015
time_elpased: 1.46
batch start
#iterations: 72
currently lose_sum: 98.03184527158737
time_elpased: 1.49
batch start
#iterations: 73
currently lose_sum: 98.0714082121849
time_elpased: 1.544
batch start
#iterations: 74
currently lose_sum: 98.1751401424408
time_elpased: 1.548
batch start
#iterations: 75
currently lose_sum: 98.21642535924911
time_elpased: 1.594
batch start
#iterations: 76
currently lose_sum: 98.21924155950546
time_elpased: 1.524
batch start
#iterations: 77
currently lose_sum: 97.82534945011139
time_elpased: 1.558
batch start
#iterations: 78
currently lose_sum: 98.00831472873688
time_elpased: 1.493
batch start
#iterations: 79
currently lose_sum: 98.23817026615143
time_elpased: 1.504
start validation test
0.585
0.614970307969
0.458316179498
0.525210827387
0.585209308254
65.286
batch start
#iterations: 80
currently lose_sum: 98.06923407316208
time_elpased: 1.497
batch start
#iterations: 81
currently lose_sum: 98.12754935026169
time_elpased: 1.497
batch start
#iterations: 82
currently lose_sum: 97.9530274271965
time_elpased: 1.505
batch start
#iterations: 83
currently lose_sum: 97.68568116426468
time_elpased: 1.513
batch start
#iterations: 84
currently lose_sum: 97.87825959920883
time_elpased: 1.532
batch start
#iterations: 85
currently lose_sum: 97.95165079832077
time_elpased: 1.542
batch start
#iterations: 86
currently lose_sum: 98.17163532972336
time_elpased: 1.499
batch start
#iterations: 87
currently lose_sum: 97.61844092607498
time_elpased: 1.497
batch start
#iterations: 88
currently lose_sum: 97.58499372005463
time_elpased: 1.496
batch start
#iterations: 89
currently lose_sum: 97.71731525659561
time_elpased: 1.535
batch start
#iterations: 90
currently lose_sum: 97.81872314214706
time_elpased: 1.505
batch start
#iterations: 91
currently lose_sum: 97.73634278774261
time_elpased: 1.549
batch start
#iterations: 92
currently lose_sum: 97.69574731588364
time_elpased: 1.519
batch start
#iterations: 93
currently lose_sum: 97.44697481393814
time_elpased: 1.54
batch start
#iterations: 94
currently lose_sum: 97.59227532148361
time_elpased: 1.52
batch start
#iterations: 95
currently lose_sum: 97.72983080148697
time_elpased: 1.497
batch start
#iterations: 96
currently lose_sum: 97.46526938676834
time_elpased: 1.469
batch start
#iterations: 97
currently lose_sum: 97.64315265417099
time_elpased: 1.495
batch start
#iterations: 98
currently lose_sum: 97.5142976641655
time_elpased: 1.549
batch start
#iterations: 99
currently lose_sum: 97.55394852161407
time_elpased: 1.513
start validation test
0.582216494845
0.611426200028
0.454919720049
0.521687813514
0.582426815828
65.731
batch start
#iterations: 100
currently lose_sum: 97.8526440858841
time_elpased: 1.504
batch start
#iterations: 101
currently lose_sum: 97.58431708812714
time_elpased: 1.493
batch start
#iterations: 102
currently lose_sum: 97.6785386800766
time_elpased: 1.529
batch start
#iterations: 103
currently lose_sum: 97.54372191429138
time_elpased: 1.541
batch start
#iterations: 104
currently lose_sum: 97.51772969961166
time_elpased: 1.516
batch start
#iterations: 105
currently lose_sum: 97.5007643699646
time_elpased: 1.509
batch start
#iterations: 106
currently lose_sum: 97.36682677268982
time_elpased: 1.505
batch start
#iterations: 107
currently lose_sum: 97.41018062829971
time_elpased: 1.528
batch start
#iterations: 108
currently lose_sum: 97.24657809734344
time_elpased: 1.483
batch start
#iterations: 109
currently lose_sum: 97.31924766302109
time_elpased: 1.532
batch start
#iterations: 110
currently lose_sum: 97.3914293050766
time_elpased: 1.489
batch start
#iterations: 111
currently lose_sum: 97.36876583099365
time_elpased: 1.547
batch start
#iterations: 112
currently lose_sum: 97.29123663902283
time_elpased: 1.521
batch start
#iterations: 113
currently lose_sum: 97.21950489282608
time_elpased: 1.494
batch start
#iterations: 114
currently lose_sum: 97.48947721719742
time_elpased: 1.532
batch start
#iterations: 115
currently lose_sum: 97.0313748717308
time_elpased: 1.514
batch start
#iterations: 116
currently lose_sum: 97.22585082054138
time_elpased: 1.521
batch start
#iterations: 117
currently lose_sum: 97.29145669937134
time_elpased: 1.592
batch start
#iterations: 118
currently lose_sum: 97.43349993228912
time_elpased: 1.548
batch start
#iterations: 119
currently lose_sum: 97.0984256863594
time_elpased: 1.487
start validation test
0.578762886598
0.623559539052
0.40098806093
0.488098220997
0.579056607913
66.392
batch start
#iterations: 120
currently lose_sum: 96.98545742034912
time_elpased: 1.548
batch start
#iterations: 121
currently lose_sum: 97.21123456954956
time_elpased: 1.519
batch start
#iterations: 122
currently lose_sum: 97.30952137708664
time_elpased: 1.555
batch start
#iterations: 123
currently lose_sum: 97.25264072418213
time_elpased: 1.557
batch start
#iterations: 124
currently lose_sum: 96.84326976537704
time_elpased: 1.506
batch start
#iterations: 125
currently lose_sum: 96.89175713062286
time_elpased: 1.497
batch start
#iterations: 126
currently lose_sum: 97.08094137907028
time_elpased: 1.481
batch start
#iterations: 127
currently lose_sum: 96.995088160038
time_elpased: 1.492
batch start
#iterations: 128
currently lose_sum: 96.9657729268074
time_elpased: 1.525
batch start
#iterations: 129
currently lose_sum: 96.57107853889465
time_elpased: 1.496
batch start
#iterations: 130
currently lose_sum: 97.01897042989731
time_elpased: 1.534
batch start
#iterations: 131
currently lose_sum: 96.94454264640808
time_elpased: 1.5
batch start
#iterations: 132
currently lose_sum: 97.03001856803894
time_elpased: 1.499
batch start
#iterations: 133
currently lose_sum: 97.00606119632721
time_elpased: 1.494
batch start
#iterations: 134
currently lose_sum: 96.5828360915184
time_elpased: 1.509
batch start
#iterations: 135
currently lose_sum: 97.05392408370972
time_elpased: 1.529
batch start
#iterations: 136
currently lose_sum: 96.74543732404709
time_elpased: 1.517
batch start
#iterations: 137
currently lose_sum: 96.81776732206345
time_elpased: 1.502
batch start
#iterations: 138
currently lose_sum: 96.8096312880516
time_elpased: 1.502
batch start
#iterations: 139
currently lose_sum: 96.69679635763168
time_elpased: 1.496
start validation test
0.588092783505
0.619941593659
0.458830794566
0.527355533211
0.588306351434
66.809
batch start
#iterations: 140
currently lose_sum: 96.78538531064987
time_elpased: 1.477
batch start
#iterations: 141
currently lose_sum: 97.00708431005478
time_elpased: 1.517
batch start
#iterations: 142
currently lose_sum: 96.73002308607101
time_elpased: 1.546
batch start
#iterations: 143
currently lose_sum: 96.79569488763809
time_elpased: 1.518
batch start
#iterations: 144
currently lose_sum: 96.72191768884659
time_elpased: 1.597
batch start
#iterations: 145
currently lose_sum: 96.26283603906631
time_elpased: 1.523
batch start
#iterations: 146
currently lose_sum: 96.8158289194107
time_elpased: 1.549
batch start
#iterations: 147
currently lose_sum: 96.47728025913239
time_elpased: 1.506
batch start
#iterations: 148
currently lose_sum: 96.77693110704422
time_elpased: 1.503
batch start
#iterations: 149
currently lose_sum: 96.4291632771492
time_elpased: 1.562
batch start
#iterations: 150
currently lose_sum: 96.77494597434998
time_elpased: 1.567
batch start
#iterations: 151
currently lose_sum: 96.51588702201843
time_elpased: 1.515
batch start
#iterations: 152
currently lose_sum: 96.50375384092331
time_elpased: 1.508
batch start
#iterations: 153
currently lose_sum: 96.72668087482452
time_elpased: 1.478
batch start
#iterations: 154
currently lose_sum: 96.60952478647232
time_elpased: 1.594
batch start
#iterations: 155
currently lose_sum: 96.59298557043076
time_elpased: 1.569
batch start
#iterations: 156
currently lose_sum: 96.31297105550766
time_elpased: 1.532
batch start
#iterations: 157
currently lose_sum: 96.47172540426254
time_elpased: 1.52
batch start
#iterations: 158
currently lose_sum: 96.58060377836227
time_elpased: 1.536
batch start
#iterations: 159
currently lose_sum: 96.49016624689102
time_elpased: 1.487
start validation test
0.585051546392
0.608350676379
0.481370934541
0.537462652264
0.585222848518
66.582
batch start
#iterations: 160
currently lose_sum: 96.20469218492508
time_elpased: 1.477
batch start
#iterations: 161
currently lose_sum: 96.67922830581665
time_elpased: 1.494
batch start
#iterations: 162
currently lose_sum: 96.34175926446915
time_elpased: 1.475
batch start
#iterations: 163
currently lose_sum: 96.63811016082764
time_elpased: 1.496
batch start
#iterations: 164
currently lose_sum: 96.39471107721329
time_elpased: 1.538
batch start
#iterations: 165
currently lose_sum: 96.2274278998375
time_elpased: 1.497
batch start
#iterations: 166
currently lose_sum: 96.20836126804352
time_elpased: 1.494
batch start
#iterations: 167
currently lose_sum: 96.51802885532379
time_elpased: 1.487
batch start
#iterations: 168
currently lose_sum: 96.29853636026382
time_elpased: 1.517
batch start
#iterations: 169
currently lose_sum: 96.26984792947769
time_elpased: 1.494
batch start
#iterations: 170
currently lose_sum: 96.12175673246384
time_elpased: 1.517
batch start
#iterations: 171
currently lose_sum: 96.4192333817482
time_elpased: 1.509
batch start
#iterations: 172
currently lose_sum: 96.49225854873657
time_elpased: 1.473
batch start
#iterations: 173
currently lose_sum: 95.97767752408981
time_elpased: 1.503
batch start
#iterations: 174
currently lose_sum: 96.20421141386032
time_elpased: 1.475
batch start
#iterations: 175
currently lose_sum: 96.0209630727768
time_elpased: 1.55
batch start
#iterations: 176
currently lose_sum: 96.23986196517944
time_elpased: 1.521
batch start
#iterations: 177
currently lose_sum: 96.24042356014252
time_elpased: 1.537
batch start
#iterations: 178
currently lose_sum: 95.80122965574265
time_elpased: 1.52
batch start
#iterations: 179
currently lose_sum: 96.11482900381088
time_elpased: 1.486
start validation test
0.582113402062
0.601977436938
0.488781391519
0.53950582221
0.582267606127
66.874
batch start
#iterations: 180
currently lose_sum: 95.8521797657013
time_elpased: 1.508
batch start
#iterations: 181
currently lose_sum: 96.22757071256638
time_elpased: 1.475
batch start
#iterations: 182
currently lose_sum: 96.15636414289474
time_elpased: 1.479
batch start
#iterations: 183
currently lose_sum: 95.86278051137924
time_elpased: 1.502
batch start
#iterations: 184
currently lose_sum: 95.69535404443741
time_elpased: 1.486
batch start
#iterations: 185
currently lose_sum: 95.9861969947815
time_elpased: 1.505
batch start
#iterations: 186
currently lose_sum: 95.9296777844429
time_elpased: 1.486
batch start
#iterations: 187
currently lose_sum: 95.88315415382385
time_elpased: 1.555
batch start
#iterations: 188
currently lose_sum: 96.09038788080215
time_elpased: 1.537
batch start
#iterations: 189
currently lose_sum: 95.90109699964523
time_elpased: 1.524
batch start
#iterations: 190
currently lose_sum: 96.01652067899704
time_elpased: 1.513
batch start
#iterations: 191
currently lose_sum: 95.3807522058487
time_elpased: 1.488
batch start
#iterations: 192
currently lose_sum: 95.91915708780289
time_elpased: 1.468
batch start
#iterations: 193
currently lose_sum: 95.96508383750916
time_elpased: 1.485
batch start
#iterations: 194
currently lose_sum: 95.88114386796951
time_elpased: 1.505
batch start
#iterations: 195
currently lose_sum: 95.92074418067932
time_elpased: 1.486
batch start
#iterations: 196
currently lose_sum: 95.97121280431747
time_elpased: 1.531
batch start
#iterations: 197
currently lose_sum: 95.57926970720291
time_elpased: 1.508
batch start
#iterations: 198
currently lose_sum: 96.02884292602539
time_elpased: 1.538
batch start
#iterations: 199
currently lose_sum: 95.93092465400696
time_elpased: 1.482
start validation test
0.577680412371
0.599503462694
0.472210786332
0.528297541597
0.577854670324
66.854
batch start
#iterations: 200
currently lose_sum: 95.88041299581528
time_elpased: 1.522
batch start
#iterations: 201
currently lose_sum: 95.47214311361313
time_elpased: 1.521
batch start
#iterations: 202
currently lose_sum: 95.78591823577881
time_elpased: 1.487
batch start
#iterations: 203
currently lose_sum: 95.55833035707474
time_elpased: 1.515
batch start
#iterations: 204
currently lose_sum: 95.51162660121918
time_elpased: 1.519
batch start
#iterations: 205
currently lose_sum: 95.76197052001953
time_elpased: 1.463
batch start
#iterations: 206
currently lose_sum: 96.01615524291992
time_elpased: 1.483
batch start
#iterations: 207
currently lose_sum: 95.6746364235878
time_elpased: 1.485
batch start
#iterations: 208
currently lose_sum: 95.54218703508377
time_elpased: 1.485
batch start
#iterations: 209
currently lose_sum: 95.59291356801987
time_elpased: 1.483
batch start
#iterations: 210
currently lose_sum: 95.6834841966629
time_elpased: 1.481
batch start
#iterations: 211
currently lose_sum: 94.92894971370697
time_elpased: 1.496
batch start
#iterations: 212
currently lose_sum: 95.0990982055664
time_elpased: 1.487
batch start
#iterations: 213
currently lose_sum: 95.2722430229187
time_elpased: 1.493
batch start
#iterations: 214
currently lose_sum: 95.47103559970856
time_elpased: 1.5
batch start
#iterations: 215
currently lose_sum: 95.45363575220108
time_elpased: 1.525
batch start
#iterations: 216
currently lose_sum: 95.60940301418304
time_elpased: 1.475
batch start
#iterations: 217
currently lose_sum: 95.1028283238411
time_elpased: 1.551
batch start
#iterations: 218
currently lose_sum: 95.18588364124298
time_elpased: 1.504
batch start
#iterations: 219
currently lose_sum: 95.14183461666107
time_elpased: 1.506
start validation test
0.582164948454
0.617381160688
0.435776039522
0.510920719199
0.582406813648
67.584
batch start
#iterations: 220
currently lose_sum: 95.276995241642
time_elpased: 1.504
batch start
#iterations: 221
currently lose_sum: 95.04716920852661
time_elpased: 1.509
batch start
#iterations: 222
currently lose_sum: 95.37225997447968
time_elpased: 1.517
batch start
#iterations: 223
currently lose_sum: 94.97297769784927
time_elpased: 1.536
batch start
#iterations: 224
currently lose_sum: 95.65255737304688
time_elpased: 1.474
batch start
#iterations: 225
currently lose_sum: 95.1116561293602
time_elpased: 1.538
batch start
#iterations: 226
currently lose_sum: 94.88079333305359
time_elpased: 1.516
batch start
#iterations: 227
currently lose_sum: 95.12548410892487
time_elpased: 1.524
batch start
#iterations: 228
currently lose_sum: 95.3492648601532
time_elpased: 1.538
batch start
#iterations: 229
currently lose_sum: 94.94920909404755
time_elpased: 1.505
batch start
#iterations: 230
currently lose_sum: 95.21887564659119
time_elpased: 1.49
batch start
#iterations: 231
currently lose_sum: 95.3607285618782
time_elpased: 1.508
batch start
#iterations: 232
currently lose_sum: 95.46883344650269
time_elpased: 1.583
batch start
#iterations: 233
currently lose_sum: 94.8352233171463
time_elpased: 1.517
batch start
#iterations: 234
currently lose_sum: 95.30773437023163
time_elpased: 1.5
batch start
#iterations: 235
currently lose_sum: 95.75515574216843
time_elpased: 1.517
batch start
#iterations: 236
currently lose_sum: 95.62416368722916
time_elpased: 1.542
batch start
#iterations: 237
currently lose_sum: 95.05159622430801
time_elpased: 1.473
batch start
#iterations: 238
currently lose_sum: 95.3629874587059
time_elpased: 1.474
batch start
#iterations: 239
currently lose_sum: 95.55725592374802
time_elpased: 1.577
start validation test
0.576340206186
0.615848939793
0.409530671058
0.491932991284
0.576615810539
67.894
batch start
#iterations: 240
currently lose_sum: 94.95827353000641
time_elpased: 1.512
batch start
#iterations: 241
currently lose_sum: 95.32027906179428
time_elpased: 1.49
batch start
#iterations: 242
currently lose_sum: 94.54396921396255
time_elpased: 1.497
batch start
#iterations: 243
currently lose_sum: 95.23211008310318
time_elpased: 1.488
batch start
#iterations: 244
currently lose_sum: 94.82044786214828
time_elpased: 1.491
batch start
#iterations: 245
currently lose_sum: 95.02019369602203
time_elpased: 1.559
batch start
#iterations: 246
currently lose_sum: 95.05707275867462
time_elpased: 1.496
batch start
#iterations: 247
currently lose_sum: 95.14507150650024
time_elpased: 1.494
batch start
#iterations: 248
currently lose_sum: 95.27994012832642
time_elpased: 1.503
batch start
#iterations: 249
currently lose_sum: 94.82953226566315
time_elpased: 1.488
batch start
#iterations: 250
currently lose_sum: 94.98874777555466
time_elpased: 1.528
batch start
#iterations: 251
currently lose_sum: 94.79742902517319
time_elpased: 1.47
batch start
#iterations: 252
currently lose_sum: 94.85643553733826
time_elpased: 1.498
batch start
#iterations: 253
currently lose_sum: 94.82770669460297
time_elpased: 1.54
batch start
#iterations: 254
currently lose_sum: 94.80734133720398
time_elpased: 1.498
batch start
#iterations: 255
currently lose_sum: 94.95734870433807
time_elpased: 1.532
batch start
#iterations: 256
currently lose_sum: 94.77615064382553
time_elpased: 1.517
batch start
#iterations: 257
currently lose_sum: 95.05280286073685
time_elpased: 1.549
batch start
#iterations: 258
currently lose_sum: 94.73821812868118
time_elpased: 1.541
batch start
#iterations: 259
currently lose_sum: 94.8528168797493
time_elpased: 1.522
start validation test
0.581443298969
0.600757575758
0.489707698641
0.539578135632
0.58159486543
67.827
batch start
#iterations: 260
currently lose_sum: 95.13467824459076
time_elpased: 1.479
batch start
#iterations: 261
currently lose_sum: 94.89330267906189
time_elpased: 1.504
batch start
#iterations: 262
currently lose_sum: 94.92057681083679
time_elpased: 1.5
batch start
#iterations: 263
currently lose_sum: 94.74160039424896
time_elpased: 1.489
batch start
#iterations: 264
currently lose_sum: 94.29076796770096
time_elpased: 1.5
batch start
#iterations: 265
currently lose_sum: 94.49117434024811
time_elpased: 1.493
batch start
#iterations: 266
currently lose_sum: 94.69337463378906
time_elpased: 1.502
batch start
#iterations: 267
currently lose_sum: 94.80748188495636
time_elpased: 1.477
batch start
#iterations: 268
currently lose_sum: 94.48387861251831
time_elpased: 1.479
batch start
#iterations: 269
currently lose_sum: 94.58040648698807
time_elpased: 1.545
batch start
#iterations: 270
currently lose_sum: 94.54265969991684
time_elpased: 1.512
batch start
#iterations: 271
currently lose_sum: 94.73175323009491
time_elpased: 1.5
batch start
#iterations: 272
currently lose_sum: 94.6087960600853
time_elpased: 1.515
batch start
#iterations: 273
currently lose_sum: 94.33083003759384
time_elpased: 1.487
batch start
#iterations: 274
currently lose_sum: 94.97670251131058
time_elpased: 1.474
batch start
#iterations: 275
currently lose_sum: 94.51899290084839
time_elpased: 1.497
batch start
#iterations: 276
currently lose_sum: 94.55670803785324
time_elpased: 1.503
batch start
#iterations: 277
currently lose_sum: 94.88573563098907
time_elpased: 1.487
batch start
#iterations: 278
currently lose_sum: 94.5269005894661
time_elpased: 1.503
batch start
#iterations: 279
currently lose_sum: 94.55890822410583
time_elpased: 1.498
start validation test
0.580721649485
0.626156299841
0.404075751338
0.49117978231
0.581013505574
69.185
batch start
#iterations: 280
currently lose_sum: 94.12003576755524
time_elpased: 1.491
batch start
#iterations: 281
currently lose_sum: 94.25303226709366
time_elpased: 1.553
batch start
#iterations: 282
currently lose_sum: 94.26713716983795
time_elpased: 1.493
batch start
#iterations: 283
currently lose_sum: 94.58330726623535
time_elpased: 1.499
batch start
#iterations: 284
currently lose_sum: 94.49919551610947
time_elpased: 1.495
batch start
#iterations: 285
currently lose_sum: 94.38671880960464
time_elpased: 1.537
batch start
#iterations: 286
currently lose_sum: 94.4775955080986
time_elpased: 1.483
batch start
#iterations: 287
currently lose_sum: 94.27125191688538
time_elpased: 1.532
batch start
#iterations: 288
currently lose_sum: 94.76385349035263
time_elpased: 1.48
batch start
#iterations: 289
currently lose_sum: 94.03575265407562
time_elpased: 1.516
batch start
#iterations: 290
currently lose_sum: 93.81610071659088
time_elpased: 1.533
batch start
#iterations: 291
currently lose_sum: 94.17346751689911
time_elpased: 1.499
batch start
#iterations: 292
currently lose_sum: 93.86913913488388
time_elpased: 1.531
batch start
#iterations: 293
currently lose_sum: 94.25076937675476
time_elpased: 1.524
batch start
#iterations: 294
currently lose_sum: 94.26054185628891
time_elpased: 1.52
batch start
#iterations: 295
currently lose_sum: 94.16763532161713
time_elpased: 1.507
batch start
#iterations: 296
currently lose_sum: 94.30436360836029
time_elpased: 1.536
batch start
#iterations: 297
currently lose_sum: 94.38387161493301
time_elpased: 1.526
batch start
#iterations: 298
currently lose_sum: 94.36326938867569
time_elpased: 1.49
batch start
#iterations: 299
currently lose_sum: 94.26893782615662
time_elpased: 1.545
start validation test
0.581237113402
0.620862435469
0.420852202552
0.501656238498
0.581502102928
69.034
batch start
#iterations: 300
currently lose_sum: 94.23497247695923
time_elpased: 1.493
batch start
#iterations: 301
currently lose_sum: 93.92101138830185
time_elpased: 1.466
batch start
#iterations: 302
currently lose_sum: 94.44747442007065
time_elpased: 1.527
batch start
#iterations: 303
currently lose_sum: 94.28915393352509
time_elpased: 1.49
batch start
#iterations: 304
currently lose_sum: 94.06967210769653
time_elpased: 1.509
batch start
#iterations: 305
currently lose_sum: 94.02862006425858
time_elpased: 1.499
batch start
#iterations: 306
currently lose_sum: 93.74684882164001
time_elpased: 1.572
batch start
#iterations: 307
currently lose_sum: 93.91679835319519
time_elpased: 1.517
batch start
#iterations: 308
currently lose_sum: 94.22379302978516
time_elpased: 1.527
batch start
#iterations: 309
currently lose_sum: 94.18254274129868
time_elpased: 1.5
batch start
#iterations: 310
currently lose_sum: 94.0827203989029
time_elpased: 1.479
batch start
#iterations: 311
currently lose_sum: 93.58184760808945
time_elpased: 1.506
batch start
#iterations: 312
currently lose_sum: 94.24685478210449
time_elpased: 1.494
batch start
#iterations: 313
currently lose_sum: 93.9315715432167
time_elpased: 1.512
batch start
#iterations: 314
currently lose_sum: 93.71461302042007
time_elpased: 1.488
batch start
#iterations: 315
currently lose_sum: 93.80135750770569
time_elpased: 1.489
batch start
#iterations: 316
currently lose_sum: 93.7391043305397
time_elpased: 1.508
batch start
#iterations: 317
currently lose_sum: 93.67609679698944
time_elpased: 1.503
batch start
#iterations: 318
currently lose_sum: 93.74866360425949
time_elpased: 1.492
batch start
#iterations: 319
currently lose_sum: 94.06441158056259
time_elpased: 1.522
start validation test
0.577010309278
0.623325710552
0.392754219844
0.481879025129
0.577314739001
70.983
batch start
#iterations: 320
currently lose_sum: 93.71051573753357
time_elpased: 1.498
batch start
#iterations: 321
currently lose_sum: 93.97078603506088
time_elpased: 1.5
batch start
#iterations: 322
currently lose_sum: 93.90247368812561
time_elpased: 1.491
batch start
#iterations: 323
currently lose_sum: 93.60184526443481
time_elpased: 1.492
batch start
#iterations: 324
currently lose_sum: 93.62033993005753
time_elpased: 1.52
batch start
#iterations: 325
currently lose_sum: 93.559275329113
time_elpased: 1.597
batch start
#iterations: 326
currently lose_sum: 93.87163281440735
time_elpased: 1.484
batch start
#iterations: 327
currently lose_sum: 93.91586685180664
time_elpased: 1.536
batch start
#iterations: 328
currently lose_sum: 93.25525426864624
time_elpased: 1.511
batch start
#iterations: 329
currently lose_sum: 93.82472670078278
time_elpased: 1.506
batch start
#iterations: 330
currently lose_sum: 93.61144345998764
time_elpased: 1.53
batch start
#iterations: 331
currently lose_sum: 93.52233481407166
time_elpased: 1.537
batch start
#iterations: 332
currently lose_sum: 93.36333775520325
time_elpased: 1.537
batch start
#iterations: 333
currently lose_sum: 94.06381821632385
time_elpased: 1.489
batch start
#iterations: 334
currently lose_sum: 93.70869839191437
time_elpased: 1.533
batch start
#iterations: 335
currently lose_sum: 93.3864951133728
time_elpased: 1.5
batch start
#iterations: 336
currently lose_sum: 93.02698928117752
time_elpased: 1.505
batch start
#iterations: 337
currently lose_sum: 93.80843579769135
time_elpased: 1.54
batch start
#iterations: 338
currently lose_sum: 93.43837958574295
time_elpased: 1.475
batch start
#iterations: 339
currently lose_sum: 92.94232958555222
time_elpased: 1.473
start validation test
0.57618556701
0.618949044586
0.400061753808
0.485996499125
0.576476560506
71.638
batch start
#iterations: 340
currently lose_sum: 93.15432351827621
time_elpased: 1.504
batch start
#iterations: 341
currently lose_sum: 93.41134697198868
time_elpased: 1.512
batch start
#iterations: 342
currently lose_sum: 93.76837021112442
time_elpased: 1.544
batch start
#iterations: 343
currently lose_sum: 93.85869246721268
time_elpased: 1.508
batch start
#iterations: 344
currently lose_sum: 93.31259626150131
time_elpased: 1.482
batch start
#iterations: 345
currently lose_sum: 93.2828294634819
time_elpased: 1.493
batch start
#iterations: 346
currently lose_sum: 93.48848980665207
time_elpased: 1.546
batch start
#iterations: 347
currently lose_sum: 93.39938986301422
time_elpased: 1.515
batch start
#iterations: 348
currently lose_sum: 93.60383117198944
time_elpased: 1.51
batch start
#iterations: 349
currently lose_sum: 93.3139271736145
time_elpased: 1.49
batch start
#iterations: 350
currently lose_sum: 93.27752435207367
time_elpased: 1.528
batch start
#iterations: 351
currently lose_sum: 93.39017915725708
time_elpased: 1.514
batch start
#iterations: 352
currently lose_sum: 93.51090967655182
time_elpased: 1.529
batch start
#iterations: 353
currently lose_sum: 93.25472462177277
time_elpased: 1.493
batch start
#iterations: 354
currently lose_sum: 93.14537870883942
time_elpased: 1.52
batch start
#iterations: 355
currently lose_sum: 93.00197196006775
time_elpased: 1.488
batch start
#iterations: 356
currently lose_sum: 93.1215649843216
time_elpased: 1.515
batch start
#iterations: 357
currently lose_sum: 93.18365168571472
time_elpased: 1.524
batch start
#iterations: 358
currently lose_sum: 93.09077894687653
time_elpased: 1.528
batch start
#iterations: 359
currently lose_sum: 93.09116119146347
time_elpased: 1.517
start validation test
0.576804123711
0.598870798319
0.469431864965
0.526309716132
0.576981525213
70.490
batch start
#iterations: 360
currently lose_sum: 93.12985759973526
time_elpased: 1.531
batch start
#iterations: 361
currently lose_sum: 92.81990998983383
time_elpased: 1.541
batch start
#iterations: 362
currently lose_sum: 92.90473121404648
time_elpased: 1.526
batch start
#iterations: 363
currently lose_sum: 93.23544883728027
time_elpased: 1.502
batch start
#iterations: 364
currently lose_sum: 93.0082437992096
time_elpased: 1.516
batch start
#iterations: 365
currently lose_sum: 93.061472594738
time_elpased: 1.481
batch start
#iterations: 366
currently lose_sum: 92.95830714702606
time_elpased: 1.513
batch start
#iterations: 367
currently lose_sum: 92.34475189447403
time_elpased: 1.521
batch start
#iterations: 368
currently lose_sum: 92.8019899725914
time_elpased: 1.505
batch start
#iterations: 369
currently lose_sum: 92.67744398117065
time_elpased: 1.511
batch start
#iterations: 370
currently lose_sum: 93.02510768175125
time_elpased: 1.534
batch start
#iterations: 371
currently lose_sum: 92.43131411075592
time_elpased: 1.498
batch start
#iterations: 372
currently lose_sum: 92.66150736808777
time_elpased: 1.518
batch start
#iterations: 373
currently lose_sum: 92.9256443977356
time_elpased: 1.533
batch start
#iterations: 374
currently lose_sum: 92.95108288526535
time_elpased: 1.511
batch start
#iterations: 375
currently lose_sum: 92.71426969766617
time_elpased: 1.546
batch start
#iterations: 376
currently lose_sum: 92.47807693481445
time_elpased: 1.491
batch start
#iterations: 377
currently lose_sum: 93.00737851858139
time_elpased: 1.485
batch start
#iterations: 378
currently lose_sum: 92.75541126728058
time_elpased: 1.596
batch start
#iterations: 379
currently lose_sum: 92.52664148807526
time_elpased: 1.477
start validation test
0.576546391753
0.623417201118
0.390181144504
0.479964550231
0.576854306246
72.078
batch start
#iterations: 380
currently lose_sum: 92.51631534099579
time_elpased: 1.534
batch start
#iterations: 381
currently lose_sum: 92.29751652479172
time_elpased: 1.544
batch start
#iterations: 382
currently lose_sum: 92.44664466381073
time_elpased: 1.5
batch start
#iterations: 383
currently lose_sum: 92.59948617219925
time_elpased: 1.488
batch start
#iterations: 384
currently lose_sum: 93.02614742517471
time_elpased: 1.495
batch start
#iterations: 385
currently lose_sum: 92.78586387634277
time_elpased: 1.49
batch start
#iterations: 386
currently lose_sum: 92.59297686815262
time_elpased: 1.47
batch start
#iterations: 387
currently lose_sum: 92.6685848236084
time_elpased: 1.471
batch start
#iterations: 388
currently lose_sum: 92.65002393722534
time_elpased: 1.497
batch start
#iterations: 389
currently lose_sum: 92.66046112775803
time_elpased: 1.566
batch start
#iterations: 390
currently lose_sum: 92.184650182724
time_elpased: 1.497
batch start
#iterations: 391
currently lose_sum: 92.47541791200638
time_elpased: 1.537
batch start
#iterations: 392
currently lose_sum: 92.42522966861725
time_elpased: 1.528
batch start
#iterations: 393
currently lose_sum: 92.37814962863922
time_elpased: 1.53
batch start
#iterations: 394
currently lose_sum: 91.87711906433105
time_elpased: 1.482
batch start
#iterations: 395
currently lose_sum: 92.11780405044556
time_elpased: 1.579
batch start
#iterations: 396
currently lose_sum: 92.23353636264801
time_elpased: 1.514
batch start
#iterations: 397
currently lose_sum: 92.58139902353287
time_elpased: 1.528
batch start
#iterations: 398
currently lose_sum: 92.56824105978012
time_elpased: 1.506
batch start
#iterations: 399
currently lose_sum: 92.10006314516068
time_elpased: 1.494
start validation test
0.57793814433
0.61355529132
0.424866200082
0.502067623449
0.578191051301
73.011
acc: 0.593
pre: 0.624
rec: 0.471
F1: 0.537
auc: 0.625
