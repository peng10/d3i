start to construct graph
graph construct over
29151
epochs start
batch start
#iterations: 0
currently lose_sum: 100.3233614563942
time_elpased: 1.529
batch start
#iterations: 1
currently lose_sum: 100.32663065195084
time_elpased: 1.552
batch start
#iterations: 2
currently lose_sum: 100.17362016439438
time_elpased: 1.512
batch start
#iterations: 3
currently lose_sum: 100.14252430200577
time_elpased: 1.542
batch start
#iterations: 4
currently lose_sum: 100.09483867883682
time_elpased: 1.559
batch start
#iterations: 5
currently lose_sum: 99.9999607205391
time_elpased: 1.531
batch start
#iterations: 6
currently lose_sum: 99.75397539138794
time_elpased: 1.512
batch start
#iterations: 7
currently lose_sum: 99.83567994832993
time_elpased: 1.522
batch start
#iterations: 8
currently lose_sum: 99.80124241113663
time_elpased: 1.533
batch start
#iterations: 9
currently lose_sum: 99.92535334825516
time_elpased: 1.522
batch start
#iterations: 10
currently lose_sum: 99.59457778930664
time_elpased: 1.532
batch start
#iterations: 11
currently lose_sum: 99.5948457121849
time_elpased: 1.537
batch start
#iterations: 12
currently lose_sum: 99.54690396785736
time_elpased: 1.508
batch start
#iterations: 13
currently lose_sum: 99.66874426603317
time_elpased: 1.543
batch start
#iterations: 14
currently lose_sum: 99.36734229326248
time_elpased: 1.533
batch start
#iterations: 15
currently lose_sum: 99.57066237926483
time_elpased: 1.536
batch start
#iterations: 16
currently lose_sum: 99.50065636634827
time_elpased: 1.505
batch start
#iterations: 17
currently lose_sum: 99.4823169708252
time_elpased: 1.579
batch start
#iterations: 18
currently lose_sum: 99.27310925722122
time_elpased: 1.511
batch start
#iterations: 19
currently lose_sum: 99.3493213057518
time_elpased: 1.516
start validation test
0.582680412371
0.590975418117
0.541833899352
0.565338773757
0.582752124725
65.643
batch start
#iterations: 20
currently lose_sum: 99.27134567499161
time_elpased: 1.546
batch start
#iterations: 21
currently lose_sum: 99.15581089258194
time_elpased: 1.51
batch start
#iterations: 22
currently lose_sum: 99.16367119550705
time_elpased: 1.505
batch start
#iterations: 23
currently lose_sum: 99.29288417100906
time_elpased: 1.484
batch start
#iterations: 24
currently lose_sum: 99.24451595544815
time_elpased: 1.526
batch start
#iterations: 25
currently lose_sum: 98.93049418926239
time_elpased: 1.514
batch start
#iterations: 26
currently lose_sum: 99.20531833171844
time_elpased: 1.509
batch start
#iterations: 27
currently lose_sum: 98.9105389714241
time_elpased: 1.522
batch start
#iterations: 28
currently lose_sum: 99.02867782115936
time_elpased: 1.495
batch start
#iterations: 29
currently lose_sum: 99.12781274318695
time_elpased: 1.627
batch start
#iterations: 30
currently lose_sum: 99.03064829111099
time_elpased: 1.513
batch start
#iterations: 31
currently lose_sum: 99.02965658903122
time_elpased: 1.525
batch start
#iterations: 32
currently lose_sum: 99.1490266919136
time_elpased: 1.507
batch start
#iterations: 33
currently lose_sum: 98.91908621788025
time_elpased: 1.527
batch start
#iterations: 34
currently lose_sum: 98.80288338661194
time_elpased: 1.527
batch start
#iterations: 35
currently lose_sum: 98.84978723526001
time_elpased: 1.541
batch start
#iterations: 36
currently lose_sum: 98.9914180636406
time_elpased: 1.532
batch start
#iterations: 37
currently lose_sum: 99.00381630659103
time_elpased: 1.519
batch start
#iterations: 38
currently lose_sum: 98.67282378673553
time_elpased: 1.509
batch start
#iterations: 39
currently lose_sum: 98.76965069770813
time_elpased: 1.533
start validation test
0.599587628866
0.615475767271
0.534527117423
0.572152456488
0.599701852629
65.401
batch start
#iterations: 40
currently lose_sum: 98.88242822885513
time_elpased: 1.568
batch start
#iterations: 41
currently lose_sum: 98.69890308380127
time_elpased: 1.548
batch start
#iterations: 42
currently lose_sum: 98.65516114234924
time_elpased: 1.53
batch start
#iterations: 43
currently lose_sum: 98.93694251775742
time_elpased: 1.549
batch start
#iterations: 44
currently lose_sum: 98.65990883111954
time_elpased: 1.525
batch start
#iterations: 45
currently lose_sum: 98.69955617189407
time_elpased: 1.512
batch start
#iterations: 46
currently lose_sum: 98.77384960651398
time_elpased: 1.544
batch start
#iterations: 47
currently lose_sum: 98.67783010005951
time_elpased: 1.551
batch start
#iterations: 48
currently lose_sum: 98.59798842668533
time_elpased: 1.516
batch start
#iterations: 49
currently lose_sum: 98.4964451789856
time_elpased: 1.517
batch start
#iterations: 50
currently lose_sum: 98.47931116819382
time_elpased: 1.5
batch start
#iterations: 51
currently lose_sum: 98.53837221860886
time_elpased: 1.547
batch start
#iterations: 52
currently lose_sum: 98.59257990121841
time_elpased: 1.554
batch start
#iterations: 53
currently lose_sum: 98.81879770755768
time_elpased: 1.543
batch start
#iterations: 54
currently lose_sum: 98.72194558382034
time_elpased: 1.527
batch start
#iterations: 55
currently lose_sum: 98.60497111082077
time_elpased: 1.541
batch start
#iterations: 56
currently lose_sum: 98.54979985952377
time_elpased: 1.497
batch start
#iterations: 57
currently lose_sum: 98.44452852010727
time_elpased: 1.516
batch start
#iterations: 58
currently lose_sum: 98.5148133635521
time_elpased: 1.519
batch start
#iterations: 59
currently lose_sum: 98.40665572881699
time_elpased: 1.524
start validation test
0.59293814433
0.630185979971
0.453329216836
0.52732387622
0.593183249335
65.741
batch start
#iterations: 60
currently lose_sum: 98.51677858829498
time_elpased: 1.542
batch start
#iterations: 61
currently lose_sum: 98.5017602443695
time_elpased: 1.526
batch start
#iterations: 62
currently lose_sum: 98.60702782869339
time_elpased: 1.536
batch start
#iterations: 63
currently lose_sum: 98.45202255249023
time_elpased: 1.52
batch start
#iterations: 64
currently lose_sum: 98.5905105471611
time_elpased: 1.538
batch start
#iterations: 65
currently lose_sum: 98.20749491453171
time_elpased: 1.533
batch start
#iterations: 66
currently lose_sum: 98.43576979637146
time_elpased: 1.529
batch start
#iterations: 67
currently lose_sum: 98.15778237581253
time_elpased: 1.527
batch start
#iterations: 68
currently lose_sum: 98.23587518930435
time_elpased: 1.535
batch start
#iterations: 69
currently lose_sum: 98.43639087677002
time_elpased: 1.575
batch start
#iterations: 70
currently lose_sum: 98.27523845434189
time_elpased: 1.521
batch start
#iterations: 71
currently lose_sum: 98.33882755041122
time_elpased: 1.559
batch start
#iterations: 72
currently lose_sum: 98.04573249816895
time_elpased: 1.556
batch start
#iterations: 73
currently lose_sum: 98.0880531668663
time_elpased: 1.538
batch start
#iterations: 74
currently lose_sum: 98.037906229496
time_elpased: 1.524
batch start
#iterations: 75
currently lose_sum: 98.322525203228
time_elpased: 1.512
batch start
#iterations: 76
currently lose_sum: 97.98794382810593
time_elpased: 1.546
batch start
#iterations: 77
currently lose_sum: 97.91379934549332
time_elpased: 1.516
batch start
#iterations: 78
currently lose_sum: 98.26976138353348
time_elpased: 1.526
batch start
#iterations: 79
currently lose_sum: 98.16077655553818
time_elpased: 1.544
start validation test
0.599536082474
0.619950738916
0.518061129978
0.564444693614
0.59967912432
65.565
batch start
#iterations: 80
currently lose_sum: 97.93440490961075
time_elpased: 1.55
batch start
#iterations: 81
currently lose_sum: 98.1270409822464
time_elpased: 1.53
batch start
#iterations: 82
currently lose_sum: 97.90979254245758
time_elpased: 1.515
batch start
#iterations: 83
currently lose_sum: 97.81769943237305
time_elpased: 1.521
batch start
#iterations: 84
currently lose_sum: 98.16193509101868
time_elpased: 1.511
batch start
#iterations: 85
currently lose_sum: 98.11915546655655
time_elpased: 1.578
batch start
#iterations: 86
currently lose_sum: 98.1500792503357
time_elpased: 1.535
batch start
#iterations: 87
currently lose_sum: 97.79506325721741
time_elpased: 1.532
batch start
#iterations: 88
currently lose_sum: 97.82211589813232
time_elpased: 1.53
batch start
#iterations: 89
currently lose_sum: 98.06811237335205
time_elpased: 1.527
batch start
#iterations: 90
currently lose_sum: 97.74815344810486
time_elpased: 1.562
batch start
#iterations: 91
currently lose_sum: 98.03425151109695
time_elpased: 1.513
batch start
#iterations: 92
currently lose_sum: 97.50492089986801
time_elpased: 1.52
batch start
#iterations: 93
currently lose_sum: 97.84509581327438
time_elpased: 1.518
batch start
#iterations: 94
currently lose_sum: 97.6947814822197
time_elpased: 1.538
batch start
#iterations: 95
currently lose_sum: 97.99397403001785
time_elpased: 1.512
batch start
#iterations: 96
currently lose_sum: 97.91763681173325
time_elpased: 1.504
batch start
#iterations: 97
currently lose_sum: 97.8226346373558
time_elpased: 1.569
batch start
#iterations: 98
currently lose_sum: 97.62998861074448
time_elpased: 1.516
batch start
#iterations: 99
currently lose_sum: 97.61699050664902
time_elpased: 1.517
start validation test
0.592731958763
0.607941036614
0.526294123701
0.56417893982
0.59284860063
65.653
batch start
#iterations: 100
currently lose_sum: 97.63705325126648
time_elpased: 1.496
batch start
#iterations: 101
currently lose_sum: 97.52903097867966
time_elpased: 1.559
batch start
#iterations: 102
currently lose_sum: 97.2128621339798
time_elpased: 1.532
batch start
#iterations: 103
currently lose_sum: 97.84784585237503
time_elpased: 1.509
batch start
#iterations: 104
currently lose_sum: 97.28288912773132
time_elpased: 1.522
batch start
#iterations: 105
currently lose_sum: 97.46618634462357
time_elpased: 1.501
batch start
#iterations: 106
currently lose_sum: 97.32660573720932
time_elpased: 1.515
batch start
#iterations: 107
currently lose_sum: 97.55393505096436
time_elpased: 1.512
batch start
#iterations: 108
currently lose_sum: 97.6909927725792
time_elpased: 1.524
batch start
#iterations: 109
currently lose_sum: 97.31572502851486
time_elpased: 1.553
batch start
#iterations: 110
currently lose_sum: 97.4776291847229
time_elpased: 1.504
batch start
#iterations: 111
currently lose_sum: 97.79747426509857
time_elpased: 1.509
batch start
#iterations: 112
currently lose_sum: 97.32185852527618
time_elpased: 1.545
batch start
#iterations: 113
currently lose_sum: 97.25003945827484
time_elpased: 1.524
batch start
#iterations: 114
currently lose_sum: 97.47995436191559
time_elpased: 1.548
batch start
#iterations: 115
currently lose_sum: 97.33913940191269
time_elpased: 1.52
batch start
#iterations: 116
currently lose_sum: 97.37022012472153
time_elpased: 1.526
batch start
#iterations: 117
currently lose_sum: 97.06947129964828
time_elpased: 1.517
batch start
#iterations: 118
currently lose_sum: 97.4274440407753
time_elpased: 1.53
batch start
#iterations: 119
currently lose_sum: 97.3944935798645
time_elpased: 1.516
start validation test
0.586340206186
0.613374430448
0.47103015334
0.532859887071
0.586542650769
66.135
batch start
#iterations: 120
currently lose_sum: 97.32117104530334
time_elpased: 1.568
batch start
#iterations: 121
currently lose_sum: 97.50696980953217
time_elpased: 1.505
batch start
#iterations: 122
currently lose_sum: 97.30844223499298
time_elpased: 1.528
batch start
#iterations: 123
currently lose_sum: 97.01403802633286
time_elpased: 1.509
batch start
#iterations: 124
currently lose_sum: 97.26508319377899
time_elpased: 1.513
batch start
#iterations: 125
currently lose_sum: 97.08610379695892
time_elpased: 1.53
batch start
#iterations: 126
currently lose_sum: 97.10204517841339
time_elpased: 1.54
batch start
#iterations: 127
currently lose_sum: 97.14232164621353
time_elpased: 1.546
batch start
#iterations: 128
currently lose_sum: 96.93150609731674
time_elpased: 1.575
batch start
#iterations: 129
currently lose_sum: 97.30670005083084
time_elpased: 1.514
batch start
#iterations: 130
currently lose_sum: 96.94987434148788
time_elpased: 1.53
batch start
#iterations: 131
currently lose_sum: 97.32368314266205
time_elpased: 1.536
batch start
#iterations: 132
currently lose_sum: 97.51284372806549
time_elpased: 1.539
batch start
#iterations: 133
currently lose_sum: 96.8333175778389
time_elpased: 1.492
batch start
#iterations: 134
currently lose_sum: 96.99346274137497
time_elpased: 1.581
batch start
#iterations: 135
currently lose_sum: 97.02095884084702
time_elpased: 1.509
batch start
#iterations: 136
currently lose_sum: 96.98991572856903
time_elpased: 1.511
batch start
#iterations: 137
currently lose_sum: 96.93455880880356
time_elpased: 1.496
batch start
#iterations: 138
currently lose_sum: 96.85168218612671
time_elpased: 1.51
batch start
#iterations: 139
currently lose_sum: 96.76627188920975
time_elpased: 1.527
start validation test
0.589793814433
0.619286586193
0.469898116703
0.534347571679
0.590004309823
66.368
batch start
#iterations: 140
currently lose_sum: 96.94615060091019
time_elpased: 1.516
batch start
#iterations: 141
currently lose_sum: 96.72282361984253
time_elpased: 1.523
batch start
#iterations: 142
currently lose_sum: 97.05167573690414
time_elpased: 1.505
batch start
#iterations: 143
currently lose_sum: 96.80993437767029
time_elpased: 1.532
batch start
#iterations: 144
currently lose_sum: 96.69150215387344
time_elpased: 1.568
batch start
#iterations: 145
currently lose_sum: 96.85035395622253
time_elpased: 1.524
batch start
#iterations: 146
currently lose_sum: 96.51787829399109
time_elpased: 1.518
batch start
#iterations: 147
currently lose_sum: 96.91405236721039
time_elpased: 1.501
batch start
#iterations: 148
currently lose_sum: 96.5749340057373
time_elpased: 1.536
batch start
#iterations: 149
currently lose_sum: 96.67854237556458
time_elpased: 1.518
batch start
#iterations: 150
currently lose_sum: 96.92437160015106
time_elpased: 1.572
batch start
#iterations: 151
currently lose_sum: 96.6643557548523
time_elpased: 1.622
batch start
#iterations: 152
currently lose_sum: 96.54335218667984
time_elpased: 1.505
batch start
#iterations: 153
currently lose_sum: 96.58864116668701
time_elpased: 1.514
batch start
#iterations: 154
currently lose_sum: 96.5448152422905
time_elpased: 1.568
batch start
#iterations: 155
currently lose_sum: 96.37219083309174
time_elpased: 1.523
batch start
#iterations: 156
currently lose_sum: 96.8208498954773
time_elpased: 1.492
batch start
#iterations: 157
currently lose_sum: 96.50499272346497
time_elpased: 1.584
batch start
#iterations: 158
currently lose_sum: 96.50959008932114
time_elpased: 1.512
batch start
#iterations: 159
currently lose_sum: 96.46046805381775
time_elpased: 1.518
start validation test
0.587835051546
0.621935666714
0.451682618092
0.523309884345
0.588074088143
66.448
batch start
#iterations: 160
currently lose_sum: 96.46346682310104
time_elpased: 1.521
batch start
#iterations: 161
currently lose_sum: 96.58424347639084
time_elpased: 1.527
batch start
#iterations: 162
currently lose_sum: 96.6045161485672
time_elpased: 1.551
batch start
#iterations: 163
currently lose_sum: 96.55758100748062
time_elpased: 1.523
batch start
#iterations: 164
currently lose_sum: 96.76142466068268
time_elpased: 1.543
batch start
#iterations: 165
currently lose_sum: 96.46641391515732
time_elpased: 1.525
batch start
#iterations: 166
currently lose_sum: 96.53061747550964
time_elpased: 1.583
batch start
#iterations: 167
currently lose_sum: 96.79756021499634
time_elpased: 1.529
batch start
#iterations: 168
currently lose_sum: 96.22981834411621
time_elpased: 1.523
batch start
#iterations: 169
currently lose_sum: 96.28693276643753
time_elpased: 1.528
batch start
#iterations: 170
currently lose_sum: 96.08665543794632
time_elpased: 1.571
batch start
#iterations: 171
currently lose_sum: 96.41737860441208
time_elpased: 1.571
batch start
#iterations: 172
currently lose_sum: 96.40651404857635
time_elpased: 1.512
batch start
#iterations: 173
currently lose_sum: 95.97796273231506
time_elpased: 1.524
batch start
#iterations: 174
currently lose_sum: 96.34963488578796
time_elpased: 1.553
batch start
#iterations: 175
currently lose_sum: 96.17504262924194
time_elpased: 1.515
batch start
#iterations: 176
currently lose_sum: 96.09164869785309
time_elpased: 1.492
batch start
#iterations: 177
currently lose_sum: 96.37714868783951
time_elpased: 1.519
batch start
#iterations: 178
currently lose_sum: 96.14296054840088
time_elpased: 1.577
batch start
#iterations: 179
currently lose_sum: 96.08226442337036
time_elpased: 1.526
start validation test
0.593969072165
0.633333333333
0.449727282083
0.525967382801
0.594222310875
66.891
batch start
#iterations: 180
currently lose_sum: 96.12548238039017
time_elpased: 1.57
batch start
#iterations: 181
currently lose_sum: 96.23334848880768
time_elpased: 1.561
batch start
#iterations: 182
currently lose_sum: 96.05395674705505
time_elpased: 1.526
batch start
#iterations: 183
currently lose_sum: 95.78432166576385
time_elpased: 1.518
batch start
#iterations: 184
currently lose_sum: 96.10491704940796
time_elpased: 1.545
batch start
#iterations: 185
currently lose_sum: 96.00441187620163
time_elpased: 1.52
batch start
#iterations: 186
currently lose_sum: 96.02814829349518
time_elpased: 1.539
batch start
#iterations: 187
currently lose_sum: 96.05725395679474
time_elpased: 1.568
batch start
#iterations: 188
currently lose_sum: 95.71355044841766
time_elpased: 1.524
batch start
#iterations: 189
currently lose_sum: 96.27438467741013
time_elpased: 1.531
batch start
#iterations: 190
currently lose_sum: 95.67006552219391
time_elpased: 1.517
batch start
#iterations: 191
currently lose_sum: 95.89611953496933
time_elpased: 1.523
batch start
#iterations: 192
currently lose_sum: 95.69614034891129
time_elpased: 1.519
batch start
#iterations: 193
currently lose_sum: 95.68076330423355
time_elpased: 1.524
batch start
#iterations: 194
currently lose_sum: 96.05300283432007
time_elpased: 1.548
batch start
#iterations: 195
currently lose_sum: 96.09380239248276
time_elpased: 1.522
batch start
#iterations: 196
currently lose_sum: 95.9980543255806
time_elpased: 1.57
batch start
#iterations: 197
currently lose_sum: 96.03626257181168
time_elpased: 1.541
batch start
#iterations: 198
currently lose_sum: 95.84461307525635
time_elpased: 1.537
batch start
#iterations: 199
currently lose_sum: 95.80445247888565
time_elpased: 1.529
start validation test
0.584432989691
0.617793594306
0.446639909437
0.518456576275
0.584674906696
67.188
batch start
#iterations: 200
currently lose_sum: 95.55337834358215
time_elpased: 1.537
batch start
#iterations: 201
currently lose_sum: 95.83993059396744
time_elpased: 1.529
batch start
#iterations: 202
currently lose_sum: 95.90994679927826
time_elpased: 1.509
batch start
#iterations: 203
currently lose_sum: 95.8690778017044
time_elpased: 1.523
batch start
#iterations: 204
currently lose_sum: 95.64539194107056
time_elpased: 1.569
batch start
#iterations: 205
currently lose_sum: 95.42175889015198
time_elpased: 1.534
batch start
#iterations: 206
currently lose_sum: 95.45489925146103
time_elpased: 1.518
batch start
#iterations: 207
currently lose_sum: 95.12436187267303
time_elpased: 1.518
batch start
#iterations: 208
currently lose_sum: 95.47938680648804
time_elpased: 1.501
batch start
#iterations: 209
currently lose_sum: 95.55981713533401
time_elpased: 1.529
batch start
#iterations: 210
currently lose_sum: 95.43070727586746
time_elpased: 1.502
batch start
#iterations: 211
currently lose_sum: 95.428087413311
time_elpased: 1.508
batch start
#iterations: 212
currently lose_sum: 95.26763516664505
time_elpased: 1.531
batch start
#iterations: 213
currently lose_sum: 95.54514008760452
time_elpased: 1.545
batch start
#iterations: 214
currently lose_sum: 95.52639216184616
time_elpased: 1.508
batch start
#iterations: 215
currently lose_sum: 95.64149928092957
time_elpased: 1.536
batch start
#iterations: 216
currently lose_sum: 95.48393458127975
time_elpased: 1.521
batch start
#iterations: 217
currently lose_sum: 95.43200302124023
time_elpased: 1.543
batch start
#iterations: 218
currently lose_sum: 95.30930322408676
time_elpased: 1.519
batch start
#iterations: 219
currently lose_sum: 95.2349261045456
time_elpased: 1.543
start validation test
0.576804123711
0.639511201629
0.355459503962
0.456938748512
0.577192728331
69.111
batch start
#iterations: 220
currently lose_sum: 95.30153065919876
time_elpased: 1.527
batch start
#iterations: 221
currently lose_sum: 95.08117473125458
time_elpased: 1.542
batch start
#iterations: 222
currently lose_sum: 95.5249217748642
time_elpased: 1.505
batch start
#iterations: 223
currently lose_sum: 95.51640260219574
time_elpased: 1.504
batch start
#iterations: 224
currently lose_sum: 95.34637904167175
time_elpased: 1.51
batch start
#iterations: 225
currently lose_sum: 95.3227698802948
time_elpased: 1.557
batch start
#iterations: 226
currently lose_sum: 95.05052602291107
time_elpased: 1.505
batch start
#iterations: 227
currently lose_sum: 95.17420917749405
time_elpased: 1.501
batch start
#iterations: 228
currently lose_sum: 95.2429706454277
time_elpased: 1.551
batch start
#iterations: 229
currently lose_sum: 95.4045741558075
time_elpased: 1.509
batch start
#iterations: 230
currently lose_sum: 94.85632109642029
time_elpased: 1.547
batch start
#iterations: 231
currently lose_sum: 95.41405892372131
time_elpased: 1.517
batch start
#iterations: 232
currently lose_sum: 94.93278324604034
time_elpased: 1.529
batch start
#iterations: 233
currently lose_sum: 95.02525573968887
time_elpased: 1.513
batch start
#iterations: 234
currently lose_sum: 95.159716963768
time_elpased: 1.552
batch start
#iterations: 235
currently lose_sum: 94.97190308570862
time_elpased: 1.517
batch start
#iterations: 236
currently lose_sum: 94.633534014225
time_elpased: 1.543
batch start
#iterations: 237
currently lose_sum: 94.99325853586197
time_elpased: 1.517
batch start
#iterations: 238
currently lose_sum: 95.08690869808197
time_elpased: 1.542
batch start
#iterations: 239
currently lose_sum: 94.81065028905869
time_elpased: 1.544
start validation test
0.58912371134
0.617686708008
0.471544715447
0.534811788737
0.589330139403
67.733
batch start
#iterations: 240
currently lose_sum: 95.05161213874817
time_elpased: 1.525
batch start
#iterations: 241
currently lose_sum: 95.25817662477493
time_elpased: 1.505
batch start
#iterations: 242
currently lose_sum: 94.60573893785477
time_elpased: 1.513
batch start
#iterations: 243
currently lose_sum: 94.90882706642151
time_elpased: 1.499
batch start
#iterations: 244
currently lose_sum: 94.68331319093704
time_elpased: 1.518
batch start
#iterations: 245
currently lose_sum: 94.75725471973419
time_elpased: 1.508
batch start
#iterations: 246
currently lose_sum: 95.10732680559158
time_elpased: 1.532
batch start
#iterations: 247
currently lose_sum: 94.44236660003662
time_elpased: 1.533
batch start
#iterations: 248
currently lose_sum: 94.74047404527664
time_elpased: 1.543
batch start
#iterations: 249
currently lose_sum: 94.50559592247009
time_elpased: 1.535
batch start
#iterations: 250
currently lose_sum: 94.73555380105972
time_elpased: 1.562
batch start
#iterations: 251
currently lose_sum: 94.59034526348114
time_elpased: 1.541
batch start
#iterations: 252
currently lose_sum: 94.38681435585022
time_elpased: 1.539
batch start
#iterations: 253
currently lose_sum: 94.76958149671555
time_elpased: 1.514
batch start
#iterations: 254
currently lose_sum: 94.99198991060257
time_elpased: 1.522
batch start
#iterations: 255
currently lose_sum: 94.41743165254593
time_elpased: 1.52
batch start
#iterations: 256
currently lose_sum: 94.31162345409393
time_elpased: 1.524
batch start
#iterations: 257
currently lose_sum: 94.84971648454666
time_elpased: 1.55
batch start
#iterations: 258
currently lose_sum: 94.93363857269287
time_elpased: 1.518
batch start
#iterations: 259
currently lose_sum: 94.40739625692368
time_elpased: 1.531
start validation test
0.585051546392
0.623207686622
0.433878769167
0.511588399466
0.585316953519
69.505
batch start
#iterations: 260
currently lose_sum: 94.58352476358414
time_elpased: 1.53
batch start
#iterations: 261
currently lose_sum: 94.32301276922226
time_elpased: 1.559
batch start
#iterations: 262
currently lose_sum: 94.55730646848679
time_elpased: 1.535
batch start
#iterations: 263
currently lose_sum: 94.37616801261902
time_elpased: 1.503
batch start
#iterations: 264
currently lose_sum: 94.54473608732224
time_elpased: 1.52
batch start
#iterations: 265
currently lose_sum: 94.2283690571785
time_elpased: 1.531
batch start
#iterations: 266
currently lose_sum: 94.52057087421417
time_elpased: 1.495
batch start
#iterations: 267
currently lose_sum: 94.80830818414688
time_elpased: 1.522
batch start
#iterations: 268
currently lose_sum: 94.19266158342361
time_elpased: 1.522
batch start
#iterations: 269
currently lose_sum: 94.4099200963974
time_elpased: 1.529
batch start
#iterations: 270
currently lose_sum: 94.3906557559967
time_elpased: 1.515
batch start
#iterations: 271
currently lose_sum: 94.0755243897438
time_elpased: 1.533
batch start
#iterations: 272
currently lose_sum: 94.70933139324188
time_elpased: 1.53
batch start
#iterations: 273
currently lose_sum: 94.50950914621353
time_elpased: 1.513
batch start
#iterations: 274
currently lose_sum: 93.96842712163925
time_elpased: 1.533
batch start
#iterations: 275
currently lose_sum: 94.25779205560684
time_elpased: 1.557
batch start
#iterations: 276
currently lose_sum: 94.17523521184921
time_elpased: 1.557
batch start
#iterations: 277
currently lose_sum: 94.38950616121292
time_elpased: 1.538
batch start
#iterations: 278
currently lose_sum: 94.01175612211227
time_elpased: 1.524
batch start
#iterations: 279
currently lose_sum: 94.05079847574234
time_elpased: 1.542
start validation test
0.58118556701
0.608535587674
0.459298137285
0.523488358454
0.581399559193
69.110
batch start
#iterations: 280
currently lose_sum: 94.34359985589981
time_elpased: 1.55
batch start
#iterations: 281
currently lose_sum: 93.71228587627411
time_elpased: 1.515
batch start
#iterations: 282
currently lose_sum: 94.17131209373474
time_elpased: 1.5
batch start
#iterations: 283
currently lose_sum: 94.03681254386902
time_elpased: 1.51
batch start
#iterations: 284
currently lose_sum: 94.19001650810242
time_elpased: 1.51
batch start
#iterations: 285
currently lose_sum: 93.94322037696838
time_elpased: 1.568
batch start
#iterations: 286
currently lose_sum: 94.16359478235245
time_elpased: 1.567
batch start
#iterations: 287
currently lose_sum: 93.89531356096268
time_elpased: 1.538
batch start
#iterations: 288
currently lose_sum: 93.85634696483612
time_elpased: 1.561
batch start
#iterations: 289
currently lose_sum: 93.69327956438065
time_elpased: 1.525
batch start
#iterations: 290
currently lose_sum: 94.14787435531616
time_elpased: 1.53
batch start
#iterations: 291
currently lose_sum: 93.81535214185715
time_elpased: 1.526
batch start
#iterations: 292
currently lose_sum: 94.08168053627014
time_elpased: 1.528
batch start
#iterations: 293
currently lose_sum: 94.05193173885345
time_elpased: 1.523
batch start
#iterations: 294
currently lose_sum: 93.83288198709488
time_elpased: 1.563
batch start
#iterations: 295
currently lose_sum: 94.04443579912186
time_elpased: 1.54
batch start
#iterations: 296
currently lose_sum: 93.95243811607361
time_elpased: 1.49
batch start
#iterations: 297
currently lose_sum: 93.63378393650055
time_elpased: 1.512
batch start
#iterations: 298
currently lose_sum: 93.84772235155106
time_elpased: 1.509
batch start
#iterations: 299
currently lose_sum: 93.68212532997131
time_elpased: 1.519
start validation test
0.584587628866
0.620564281559
0.439127302665
0.51431326463
0.584843006904
70.247
batch start
#iterations: 300
currently lose_sum: 93.54303282499313
time_elpased: 1.543
batch start
#iterations: 301
currently lose_sum: 93.20956844091415
time_elpased: 1.543
batch start
#iterations: 302
currently lose_sum: 93.61878973245621
time_elpased: 1.499
batch start
#iterations: 303
currently lose_sum: 93.42202144861221
time_elpased: 1.547
batch start
#iterations: 304
currently lose_sum: 93.72769582271576
time_elpased: 1.571
batch start
#iterations: 305
currently lose_sum: 93.47981226444244
time_elpased: 1.539
batch start
#iterations: 306
currently lose_sum: 93.9518232345581
time_elpased: 1.503
batch start
#iterations: 307
currently lose_sum: 93.47732776403427
time_elpased: 1.501
batch start
#iterations: 308
currently lose_sum: 93.48067039251328
time_elpased: 1.528
batch start
#iterations: 309
currently lose_sum: 93.6838196516037
time_elpased: 1.529
batch start
#iterations: 310
currently lose_sum: 94.18551683425903
time_elpased: 1.514
batch start
#iterations: 311
currently lose_sum: 93.24462878704071
time_elpased: 1.545
batch start
#iterations: 312
currently lose_sum: 93.17472940683365
time_elpased: 1.546
batch start
#iterations: 313
currently lose_sum: 93.45817279815674
time_elpased: 1.511
batch start
#iterations: 314
currently lose_sum: 93.40589863061905
time_elpased: 1.53
batch start
#iterations: 315
currently lose_sum: 92.9180897474289
time_elpased: 1.503
batch start
#iterations: 316
currently lose_sum: 93.47865778207779
time_elpased: 1.518
batch start
#iterations: 317
currently lose_sum: 93.45370399951935
time_elpased: 1.508
batch start
#iterations: 318
currently lose_sum: 93.40374165773392
time_elpased: 1.512
batch start
#iterations: 319
currently lose_sum: 93.77820628881454
time_elpased: 1.565
start validation test
0.581804123711
0.626299212598
0.409282700422
0.495051969876
0.58210701168
70.241
batch start
#iterations: 320
currently lose_sum: 93.16102176904678
time_elpased: 1.535
batch start
#iterations: 321
currently lose_sum: 94.16072154045105
time_elpased: 1.523
batch start
#iterations: 322
currently lose_sum: 93.2037301659584
time_elpased: 1.529
batch start
#iterations: 323
currently lose_sum: 92.91238445043564
time_elpased: 1.556
batch start
#iterations: 324
currently lose_sum: 92.96673512458801
time_elpased: 1.521
batch start
#iterations: 325
currently lose_sum: 93.75005435943604
time_elpased: 1.519
batch start
#iterations: 326
currently lose_sum: 93.49470436573029
time_elpased: 1.514
batch start
#iterations: 327
currently lose_sum: 93.48240786790848
time_elpased: 1.505
batch start
#iterations: 328
currently lose_sum: 93.07752901315689
time_elpased: 1.524
batch start
#iterations: 329
currently lose_sum: 93.67507362365723
time_elpased: 1.498
batch start
#iterations: 330
currently lose_sum: 93.2568142414093
time_elpased: 1.527
batch start
#iterations: 331
currently lose_sum: 93.12409317493439
time_elpased: 1.523
batch start
#iterations: 332
currently lose_sum: 92.82712668180466
time_elpased: 1.586
batch start
#iterations: 333
currently lose_sum: 93.52300453186035
time_elpased: 1.518
batch start
#iterations: 334
currently lose_sum: 93.0906012058258
time_elpased: 1.513
batch start
#iterations: 335
currently lose_sum: 93.09672743082047
time_elpased: 1.511
batch start
#iterations: 336
currently lose_sum: 93.3893603682518
time_elpased: 1.498
batch start
#iterations: 337
currently lose_sum: 92.92138648033142
time_elpased: 1.498
batch start
#iterations: 338
currently lose_sum: 93.45686423778534
time_elpased: 1.521
batch start
#iterations: 339
currently lose_sum: 93.06365585327148
time_elpased: 1.529
start validation test
0.582268041237
0.616529403265
0.439127302665
0.512922226229
0.582519346882
71.685
batch start
#iterations: 340
currently lose_sum: 92.67644172906876
time_elpased: 1.574
batch start
#iterations: 341
currently lose_sum: 93.66220653057098
time_elpased: 1.523
batch start
#iterations: 342
currently lose_sum: 93.07874077558517
time_elpased: 1.519
batch start
#iterations: 343
currently lose_sum: 92.58144694566727
time_elpased: 1.521
batch start
#iterations: 344
currently lose_sum: 93.18870055675507
time_elpased: 1.512
batch start
#iterations: 345
currently lose_sum: 92.93194943666458
time_elpased: 1.555
batch start
#iterations: 346
currently lose_sum: 93.07601231336594
time_elpased: 1.512
batch start
#iterations: 347
currently lose_sum: 92.91380321979523
time_elpased: 1.492
batch start
#iterations: 348
currently lose_sum: 92.87290561199188
time_elpased: 1.497
batch start
#iterations: 349
currently lose_sum: 93.04523092508316
time_elpased: 1.526
batch start
#iterations: 350
currently lose_sum: 92.90648436546326
time_elpased: 1.564
batch start
#iterations: 351
currently lose_sum: 92.85048204660416
time_elpased: 1.503
batch start
#iterations: 352
currently lose_sum: 92.55962294340134
time_elpased: 1.5
batch start
#iterations: 353
currently lose_sum: 93.00161856412888
time_elpased: 1.543
batch start
#iterations: 354
currently lose_sum: 92.61455285549164
time_elpased: 1.577
batch start
#iterations: 355
currently lose_sum: 92.79714101552963
time_elpased: 1.524
batch start
#iterations: 356
currently lose_sum: 93.02587711811066
time_elpased: 1.54
batch start
#iterations: 357
currently lose_sum: 92.99365097284317
time_elpased: 1.541
batch start
#iterations: 358
currently lose_sum: 92.54111576080322
time_elpased: 1.589
batch start
#iterations: 359
currently lose_sum: 92.4180708527565
time_elpased: 1.504
start validation test
0.580927835052
0.628502024291
0.399403107955
0.48842184747
0.581246529708
72.633
batch start
#iterations: 360
currently lose_sum: 92.56621319055557
time_elpased: 1.531
batch start
#iterations: 361
currently lose_sum: 92.63178962469101
time_elpased: 1.539
batch start
#iterations: 362
currently lose_sum: 92.51878654956818
time_elpased: 1.509
batch start
#iterations: 363
currently lose_sum: 92.3342918753624
time_elpased: 1.518
batch start
#iterations: 364
currently lose_sum: 92.47125327587128
time_elpased: 1.511
batch start
#iterations: 365
currently lose_sum: 92.32387459278107
time_elpased: 1.528
batch start
#iterations: 366
currently lose_sum: 92.50475788116455
time_elpased: 1.552
batch start
#iterations: 367
currently lose_sum: 92.35664129257202
time_elpased: 1.529
batch start
#iterations: 368
currently lose_sum: 92.3335679769516
time_elpased: 1.534
batch start
#iterations: 369
currently lose_sum: 92.33498644828796
time_elpased: 1.515
batch start
#iterations: 370
currently lose_sum: 92.29224216938019
time_elpased: 1.537
batch start
#iterations: 371
currently lose_sum: 91.99306857585907
time_elpased: 1.536
batch start
#iterations: 372
currently lose_sum: 92.29337811470032
time_elpased: 1.563
batch start
#iterations: 373
currently lose_sum: 92.50033700466156
time_elpased: 1.545
batch start
#iterations: 374
currently lose_sum: 92.2481540441513
time_elpased: 1.527
batch start
#iterations: 375
currently lose_sum: 92.50388294458389
time_elpased: 1.525
batch start
#iterations: 376
currently lose_sum: 91.82766026258469
time_elpased: 1.557
batch start
#iterations: 377
currently lose_sum: 92.46484589576721
time_elpased: 1.505
batch start
#iterations: 378
currently lose_sum: 92.33506369590759
time_elpased: 1.517
batch start
#iterations: 379
currently lose_sum: 91.9955045580864
time_elpased: 1.523
start validation test
0.577319587629
0.612021857923
0.426469074817
0.502668607472
0.577584428971
75.168
batch start
#iterations: 380
currently lose_sum: 92.56534093618393
time_elpased: 1.555
batch start
#iterations: 381
currently lose_sum: 92.61279970407486
time_elpased: 1.51
batch start
#iterations: 382
currently lose_sum: 92.14730507135391
time_elpased: 1.511
batch start
#iterations: 383
currently lose_sum: 92.45069003105164
time_elpased: 1.548
batch start
#iterations: 384
currently lose_sum: 92.26952886581421
time_elpased: 1.526
batch start
#iterations: 385
currently lose_sum: 92.21058636903763
time_elpased: 1.593
batch start
#iterations: 386
currently lose_sum: 92.61400479078293
time_elpased: 1.522
batch start
#iterations: 387
currently lose_sum: 92.2188230752945
time_elpased: 1.559
batch start
#iterations: 388
currently lose_sum: 92.55045437812805
time_elpased: 1.543
batch start
#iterations: 389
currently lose_sum: 92.30855542421341
time_elpased: 1.5
batch start
#iterations: 390
currently lose_sum: 91.63995814323425
time_elpased: 1.563
batch start
#iterations: 391
currently lose_sum: 91.79230207204819
time_elpased: 1.515
batch start
#iterations: 392
currently lose_sum: 92.26777112483978
time_elpased: 1.557
batch start
#iterations: 393
currently lose_sum: 92.36740636825562
time_elpased: 1.578
batch start
#iterations: 394
currently lose_sum: 91.82583904266357
time_elpased: 1.526
batch start
#iterations: 395
currently lose_sum: 92.25719165802002
time_elpased: 1.538
batch start
#iterations: 396
currently lose_sum: 91.5728058218956
time_elpased: 1.525
batch start
#iterations: 397
currently lose_sum: 91.64712083339691
time_elpased: 1.533
batch start
#iterations: 398
currently lose_sum: 91.76839202642441
time_elpased: 1.526
batch start
#iterations: 399
currently lose_sum: 91.5209282040596
time_elpased: 1.525
start validation test
0.582113402062
0.616700492896
0.437789441186
0.512067408968
0.582366785036
75.192
acc: 0.595
pre: 0.612
rec: 0.526
F1: 0.566
auc: 0.622
