start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.50033694505692
time_elpased: 1.483
batch start
#iterations: 1
currently lose_sum: 100.04159420728683
time_elpased: 1.457
batch start
#iterations: 2
currently lose_sum: 100.16863870620728
time_elpased: 1.482
batch start
#iterations: 3
currently lose_sum: 99.86703032255173
time_elpased: 1.473
batch start
#iterations: 4
currently lose_sum: 99.8943018913269
time_elpased: 1.467
batch start
#iterations: 5
currently lose_sum: 99.91937559843063
time_elpased: 1.507
batch start
#iterations: 6
currently lose_sum: 99.66293948888779
time_elpased: 1.464
batch start
#iterations: 7
currently lose_sum: 99.71169531345367
time_elpased: 1.477
batch start
#iterations: 8
currently lose_sum: 99.7032163143158
time_elpased: 1.487
batch start
#iterations: 9
currently lose_sum: 99.6094576716423
time_elpased: 1.489
batch start
#iterations: 10
currently lose_sum: 99.61188489198685
time_elpased: 1.47
batch start
#iterations: 11
currently lose_sum: 99.58843463659286
time_elpased: 1.455
batch start
#iterations: 12
currently lose_sum: 99.5905876159668
time_elpased: 1.493
batch start
#iterations: 13
currently lose_sum: 99.35883951187134
time_elpased: 1.461
batch start
#iterations: 14
currently lose_sum: 99.42574363946915
time_elpased: 1.471
batch start
#iterations: 15
currently lose_sum: 99.58166289329529
time_elpased: 1.482
batch start
#iterations: 16
currently lose_sum: 99.23240602016449
time_elpased: 1.454
batch start
#iterations: 17
currently lose_sum: 99.39969575405121
time_elpased: 1.5
batch start
#iterations: 18
currently lose_sum: 99.28461527824402
time_elpased: 1.451
batch start
#iterations: 19
currently lose_sum: 99.40296590328217
time_elpased: 1.46
start validation test
0.575360824742
0.59853431046
0.46228259751
0.521658343979
0.575559351011
65.845
batch start
#iterations: 20
currently lose_sum: 99.32358503341675
time_elpased: 1.51
batch start
#iterations: 21
currently lose_sum: 99.13998919725418
time_elpased: 1.461
batch start
#iterations: 22
currently lose_sum: 99.22609704732895
time_elpased: 1.491
batch start
#iterations: 23
currently lose_sum: 99.09027111530304
time_elpased: 1.461
batch start
#iterations: 24
currently lose_sum: 99.05323761701584
time_elpased: 1.456
batch start
#iterations: 25
currently lose_sum: 99.06224066019058
time_elpased: 1.449
batch start
#iterations: 26
currently lose_sum: 99.0324165225029
time_elpased: 1.511
batch start
#iterations: 27
currently lose_sum: 98.88200199604034
time_elpased: 1.468
batch start
#iterations: 28
currently lose_sum: 98.96753054857254
time_elpased: 1.492
batch start
#iterations: 29
currently lose_sum: 98.86745232343674
time_elpased: 1.468
batch start
#iterations: 30
currently lose_sum: 98.82844203710556
time_elpased: 1.479
batch start
#iterations: 31
currently lose_sum: 98.89347928762436
time_elpased: 1.441
batch start
#iterations: 32
currently lose_sum: 98.48479217290878
time_elpased: 1.516
batch start
#iterations: 33
currently lose_sum: 98.88915073871613
time_elpased: 1.456
batch start
#iterations: 34
currently lose_sum: 98.98074942827225
time_elpased: 1.463
batch start
#iterations: 35
currently lose_sum: 98.90212798118591
time_elpased: 1.469
batch start
#iterations: 36
currently lose_sum: 99.00503277778625
time_elpased: 1.483
batch start
#iterations: 37
currently lose_sum: 98.87469905614853
time_elpased: 1.475
batch start
#iterations: 38
currently lose_sum: 98.78549391031265
time_elpased: 1.508
batch start
#iterations: 39
currently lose_sum: 98.92532885074615
time_elpased: 1.482
start validation test
0.58587628866
0.620197114698
0.44684573428
0.519440124417
0.586120378242
65.544
batch start
#iterations: 40
currently lose_sum: 98.78171241283417
time_elpased: 1.475
batch start
#iterations: 41
currently lose_sum: 98.64747589826584
time_elpased: 1.458
batch start
#iterations: 42
currently lose_sum: 98.70376235246658
time_elpased: 1.447
batch start
#iterations: 43
currently lose_sum: 98.50046217441559
time_elpased: 1.469
batch start
#iterations: 44
currently lose_sum: 98.76953262090683
time_elpased: 1.508
batch start
#iterations: 45
currently lose_sum: 98.45852822065353
time_elpased: 1.459
batch start
#iterations: 46
currently lose_sum: 98.45589029788971
time_elpased: 1.476
batch start
#iterations: 47
currently lose_sum: 98.7177112698555
time_elpased: 1.446
batch start
#iterations: 48
currently lose_sum: 98.57535803318024
time_elpased: 1.448
batch start
#iterations: 49
currently lose_sum: 98.82951754331589
time_elpased: 1.451
batch start
#iterations: 50
currently lose_sum: 98.5192442536354
time_elpased: 1.519
batch start
#iterations: 51
currently lose_sum: 98.60053580999374
time_elpased: 1.449
batch start
#iterations: 52
currently lose_sum: 98.55960190296173
time_elpased: 1.458
batch start
#iterations: 53
currently lose_sum: 98.69515550136566
time_elpased: 1.442
batch start
#iterations: 54
currently lose_sum: 98.51167571544647
time_elpased: 1.482
batch start
#iterations: 55
currently lose_sum: 98.5768666267395
time_elpased: 1.493
batch start
#iterations: 56
currently lose_sum: 98.66667580604553
time_elpased: 1.479
batch start
#iterations: 57
currently lose_sum: 98.43995088338852
time_elpased: 1.472
batch start
#iterations: 58
currently lose_sum: 98.38235056400299
time_elpased: 1.482
batch start
#iterations: 59
currently lose_sum: 98.70023638010025
time_elpased: 1.509
start validation test
0.571907216495
0.61394448031
0.391478851497
0.478099666939
0.572223986319
65.958
batch start
#iterations: 60
currently lose_sum: 98.2237451672554
time_elpased: 1.471
batch start
#iterations: 61
currently lose_sum: 98.4494218826294
time_elpased: 1.469
batch start
#iterations: 62
currently lose_sum: 98.45263659954071
time_elpased: 1.469
batch start
#iterations: 63
currently lose_sum: 98.53854560852051
time_elpased: 1.441
batch start
#iterations: 64
currently lose_sum: 98.38119542598724
time_elpased: 1.483
batch start
#iterations: 65
currently lose_sum: 98.21529752016068
time_elpased: 1.463
batch start
#iterations: 66
currently lose_sum: 98.23334902524948
time_elpased: 1.468
batch start
#iterations: 67
currently lose_sum: 98.18602514266968
time_elpased: 1.444
batch start
#iterations: 68
currently lose_sum: 98.44874083995819
time_elpased: 1.473
batch start
#iterations: 69
currently lose_sum: 98.10245740413666
time_elpased: 1.455
batch start
#iterations: 70
currently lose_sum: 98.02282047271729
time_elpased: 1.449
batch start
#iterations: 71
currently lose_sum: 98.1808454990387
time_elpased: 1.506
batch start
#iterations: 72
currently lose_sum: 98.11633843183517
time_elpased: 1.471
batch start
#iterations: 73
currently lose_sum: 97.91960996389389
time_elpased: 1.461
batch start
#iterations: 74
currently lose_sum: 97.91348624229431
time_elpased: 1.452
batch start
#iterations: 75
currently lose_sum: 97.77840149402618
time_elpased: 1.467
batch start
#iterations: 76
currently lose_sum: 98.03739988803864
time_elpased: 1.463
batch start
#iterations: 77
currently lose_sum: 98.09824562072754
time_elpased: 1.471
batch start
#iterations: 78
currently lose_sum: 98.11001372337341
time_elpased: 1.473
batch start
#iterations: 79
currently lose_sum: 98.22540944814682
time_elpased: 1.46
start validation test
0.576237113402
0.602860286029
0.451168055984
0.516098652069
0.576456691423
65.871
batch start
#iterations: 80
currently lose_sum: 98.15234875679016
time_elpased: 1.504
batch start
#iterations: 81
currently lose_sum: 98.10363525152206
time_elpased: 1.469
batch start
#iterations: 82
currently lose_sum: 98.12046360969543
time_elpased: 1.461
batch start
#iterations: 83
currently lose_sum: 98.11247420310974
time_elpased: 1.45
batch start
#iterations: 84
currently lose_sum: 97.97594344615936
time_elpased: 1.475
batch start
#iterations: 85
currently lose_sum: 98.03407442569733
time_elpased: 1.45
batch start
#iterations: 86
currently lose_sum: 98.10052132606506
time_elpased: 1.486
batch start
#iterations: 87
currently lose_sum: 97.85455346107483
time_elpased: 1.46
batch start
#iterations: 88
currently lose_sum: 97.9706180691719
time_elpased: 1.463
batch start
#iterations: 89
currently lose_sum: 97.87435632944107
time_elpased: 1.467
batch start
#iterations: 90
currently lose_sum: 97.92528188228607
time_elpased: 1.456
batch start
#iterations: 91
currently lose_sum: 97.60611289739609
time_elpased: 1.489
batch start
#iterations: 92
currently lose_sum: 97.78779137134552
time_elpased: 1.474
batch start
#iterations: 93
currently lose_sum: 97.74129337072372
time_elpased: 1.45
batch start
#iterations: 94
currently lose_sum: 97.96615934371948
time_elpased: 1.476
batch start
#iterations: 95
currently lose_sum: 97.81784957647324
time_elpased: 1.493
batch start
#iterations: 96
currently lose_sum: 97.72040039300919
time_elpased: 1.471
batch start
#iterations: 97
currently lose_sum: 97.88110655546188
time_elpased: 1.462
batch start
#iterations: 98
currently lose_sum: 97.99873983860016
time_elpased: 1.482
batch start
#iterations: 99
currently lose_sum: 97.83541089296341
time_elpased: 1.488
start validation test
0.586030927835
0.598550385784
0.52691159823
0.560450987904
0.586134720937
65.454
batch start
#iterations: 100
currently lose_sum: 97.7980118393898
time_elpased: 1.47
batch start
#iterations: 101
currently lose_sum: 97.81692266464233
time_elpased: 1.481
batch start
#iterations: 102
currently lose_sum: 97.59058040380478
time_elpased: 1.469
batch start
#iterations: 103
currently lose_sum: 97.65926724672318
time_elpased: 1.467
batch start
#iterations: 104
currently lose_sum: 97.60752862691879
time_elpased: 1.445
batch start
#iterations: 105
currently lose_sum: 97.81827807426453
time_elpased: 1.445
batch start
#iterations: 106
currently lose_sum: 97.87393516302109
time_elpased: 1.474
batch start
#iterations: 107
currently lose_sum: 97.63809204101562
time_elpased: 1.498
batch start
#iterations: 108
currently lose_sum: 97.66059803962708
time_elpased: 1.504
batch start
#iterations: 109
currently lose_sum: 97.95145660638809
time_elpased: 1.476
batch start
#iterations: 110
currently lose_sum: 97.56388020515442
time_elpased: 1.487
batch start
#iterations: 111
currently lose_sum: 97.46097987890244
time_elpased: 1.449
batch start
#iterations: 112
currently lose_sum: 97.64553111791611
time_elpased: 1.473
batch start
#iterations: 113
currently lose_sum: 97.77072048187256
time_elpased: 1.448
batch start
#iterations: 114
currently lose_sum: 97.55569243431091
time_elpased: 1.512
batch start
#iterations: 115
currently lose_sum: 97.86889725923538
time_elpased: 1.491
batch start
#iterations: 116
currently lose_sum: 97.5404816865921
time_elpased: 1.456
batch start
#iterations: 117
currently lose_sum: 97.5029524564743
time_elpased: 1.481
batch start
#iterations: 118
currently lose_sum: 97.53312849998474
time_elpased: 1.487
batch start
#iterations: 119
currently lose_sum: 97.59029650688171
time_elpased: 1.514
start validation test
0.580360824742
0.613610149942
0.437995266029
0.51113913409
0.580610769439
65.656
batch start
#iterations: 120
currently lose_sum: 97.5704015493393
time_elpased: 1.53
batch start
#iterations: 121
currently lose_sum: 97.36805027723312
time_elpased: 1.476
batch start
#iterations: 122
currently lose_sum: 97.52454555034637
time_elpased: 1.457
batch start
#iterations: 123
currently lose_sum: 97.56386739015579
time_elpased: 1.445
batch start
#iterations: 124
currently lose_sum: 97.61667436361313
time_elpased: 1.475
batch start
#iterations: 125
currently lose_sum: 97.39980870485306
time_elpased: 1.497
batch start
#iterations: 126
currently lose_sum: 97.33692806959152
time_elpased: 1.499
batch start
#iterations: 127
currently lose_sum: 97.27228337526321
time_elpased: 1.438
batch start
#iterations: 128
currently lose_sum: 97.69361138343811
time_elpased: 1.479
batch start
#iterations: 129
currently lose_sum: 97.58165347576141
time_elpased: 1.463
batch start
#iterations: 130
currently lose_sum: 97.48088896274567
time_elpased: 1.448
batch start
#iterations: 131
currently lose_sum: 97.29187232255936
time_elpased: 1.467
batch start
#iterations: 132
currently lose_sum: 97.2189125418663
time_elpased: 1.46
batch start
#iterations: 133
currently lose_sum: 97.30173146724701
time_elpased: 1.478
batch start
#iterations: 134
currently lose_sum: 97.39141404628754
time_elpased: 1.502
batch start
#iterations: 135
currently lose_sum: 97.40078872442245
time_elpased: 1.469
batch start
#iterations: 136
currently lose_sum: 97.22735607624054
time_elpased: 1.507
batch start
#iterations: 137
currently lose_sum: 97.33709639310837
time_elpased: 1.533
batch start
#iterations: 138
currently lose_sum: 97.33382046222687
time_elpased: 1.527
batch start
#iterations: 139
currently lose_sum: 97.40946191549301
time_elpased: 1.483
start validation test
0.582989690722
0.601725647118
0.495214572399
0.543299085469
0.58314379348
65.594
batch start
#iterations: 140
currently lose_sum: 97.20960855484009
time_elpased: 1.486
batch start
#iterations: 141
currently lose_sum: 97.20931339263916
time_elpased: 1.459
batch start
#iterations: 142
currently lose_sum: 97.27120518684387
time_elpased: 1.475
batch start
#iterations: 143
currently lose_sum: 97.43202012777328
time_elpased: 1.452
batch start
#iterations: 144
currently lose_sum: 97.41764307022095
time_elpased: 1.481
batch start
#iterations: 145
currently lose_sum: 97.14943319559097
time_elpased: 1.489
batch start
#iterations: 146
currently lose_sum: 97.28390979766846
time_elpased: 1.519
batch start
#iterations: 147
currently lose_sum: 97.30820989608765
time_elpased: 1.494
batch start
#iterations: 148
currently lose_sum: 97.64401912689209
time_elpased: 1.471
batch start
#iterations: 149
currently lose_sum: 96.91979646682739
time_elpased: 1.483
batch start
#iterations: 150
currently lose_sum: 97.24851906299591
time_elpased: 1.507
batch start
#iterations: 151
currently lose_sum: 97.21275055408478
time_elpased: 1.493
batch start
#iterations: 152
currently lose_sum: 97.17405205965042
time_elpased: 1.461
batch start
#iterations: 153
currently lose_sum: 97.07569307088852
time_elpased: 1.491
batch start
#iterations: 154
currently lose_sum: 97.31120997667313
time_elpased: 1.452
batch start
#iterations: 155
currently lose_sum: 97.29742556810379
time_elpased: 1.455
batch start
#iterations: 156
currently lose_sum: 96.87698823213577
time_elpased: 1.491
batch start
#iterations: 157
currently lose_sum: 97.19151222705841
time_elpased: 1.441
batch start
#iterations: 158
currently lose_sum: 97.02162510156631
time_elpased: 1.463
batch start
#iterations: 159
currently lose_sum: 96.9178454875946
time_elpased: 1.447
start validation test
0.579072164948
0.611534589386
0.437583616342
0.510137972406
0.579320569919
65.998
batch start
#iterations: 160
currently lose_sum: 96.83529251813889
time_elpased: 1.467
batch start
#iterations: 161
currently lose_sum: 97.41762572526932
time_elpased: 1.449
batch start
#iterations: 162
currently lose_sum: 97.30317091941833
time_elpased: 1.524
batch start
#iterations: 163
currently lose_sum: 97.17506235837936
time_elpased: 1.445
batch start
#iterations: 164
currently lose_sum: 96.89860022068024
time_elpased: 1.499
batch start
#iterations: 165
currently lose_sum: 96.82669675350189
time_elpased: 1.488
batch start
#iterations: 166
currently lose_sum: 97.25411432981491
time_elpased: 1.486
batch start
#iterations: 167
currently lose_sum: 96.90618741512299
time_elpased: 1.462
batch start
#iterations: 168
currently lose_sum: 96.94756698608398
time_elpased: 1.499
batch start
#iterations: 169
currently lose_sum: 96.72693991661072
time_elpased: 1.457
batch start
#iterations: 170
currently lose_sum: 97.32191300392151
time_elpased: 1.504
batch start
#iterations: 171
currently lose_sum: 97.03644716739655
time_elpased: 1.462
batch start
#iterations: 172
currently lose_sum: 97.22229939699173
time_elpased: 1.461
batch start
#iterations: 173
currently lose_sum: 96.62768346071243
time_elpased: 1.456
batch start
#iterations: 174
currently lose_sum: 97.04377752542496
time_elpased: 1.493
batch start
#iterations: 175
currently lose_sum: 96.97919881343842
time_elpased: 1.466
batch start
#iterations: 176
currently lose_sum: 97.31806164979935
time_elpased: 1.509
batch start
#iterations: 177
currently lose_sum: 96.95340114831924
time_elpased: 1.511
batch start
#iterations: 178
currently lose_sum: 96.79233509302139
time_elpased: 1.529
batch start
#iterations: 179
currently lose_sum: 96.74886327981949
time_elpased: 1.444
start validation test
0.568969072165
0.615871387036
0.370587629927
0.462734515549
0.569317361385
66.661
batch start
#iterations: 180
currently lose_sum: 96.78342634439468
time_elpased: 1.512
batch start
#iterations: 181
currently lose_sum: 96.97240793704987
time_elpased: 1.437
batch start
#iterations: 182
currently lose_sum: 96.87042474746704
time_elpased: 1.495
batch start
#iterations: 183
currently lose_sum: 96.62510257959366
time_elpased: 1.512
batch start
#iterations: 184
currently lose_sum: 96.79708331823349
time_elpased: 1.503
batch start
#iterations: 185
currently lose_sum: 96.90406376123428
time_elpased: 1.482
batch start
#iterations: 186
currently lose_sum: 96.70113134384155
time_elpased: 1.474
batch start
#iterations: 187
currently lose_sum: 96.52039760351181
time_elpased: 1.46
batch start
#iterations: 188
currently lose_sum: 96.7788257598877
time_elpased: 1.479
batch start
#iterations: 189
currently lose_sum: 96.78009736537933
time_elpased: 1.503
batch start
#iterations: 190
currently lose_sum: 96.79832458496094
time_elpased: 1.473
batch start
#iterations: 191
currently lose_sum: 96.8098503947258
time_elpased: 1.473
batch start
#iterations: 192
currently lose_sum: 96.41747587919235
time_elpased: 1.466
batch start
#iterations: 193
currently lose_sum: 96.4230306148529
time_elpased: 1.47
batch start
#iterations: 194
currently lose_sum: 96.88164871931076
time_elpased: 1.468
batch start
#iterations: 195
currently lose_sum: 96.88529407978058
time_elpased: 1.491
batch start
#iterations: 196
currently lose_sum: 96.85610574483871
time_elpased: 1.555
batch start
#iterations: 197
currently lose_sum: 96.95555025339127
time_elpased: 1.482
batch start
#iterations: 198
currently lose_sum: 96.33283388614655
time_elpased: 1.467
batch start
#iterations: 199
currently lose_sum: 96.86586129665375
time_elpased: 1.456
start validation test
0.575257731959
0.594837549762
0.476690336524
0.529250457038
0.575430782224
66.077
batch start
#iterations: 200
currently lose_sum: 96.84518831968307
time_elpased: 1.473
batch start
#iterations: 201
currently lose_sum: 96.78246891498566
time_elpased: 1.502
batch start
#iterations: 202
currently lose_sum: 96.53069734573364
time_elpased: 1.474
batch start
#iterations: 203
currently lose_sum: 96.80383390188217
time_elpased: 1.47
batch start
#iterations: 204
currently lose_sum: 96.52884083986282
time_elpased: 1.462
batch start
#iterations: 205
currently lose_sum: 96.73852717876434
time_elpased: 1.454
batch start
#iterations: 206
currently lose_sum: 96.47066646814346
time_elpased: 1.5
batch start
#iterations: 207
currently lose_sum: 96.77748250961304
time_elpased: 1.494
batch start
#iterations: 208
currently lose_sum: 96.62994754314423
time_elpased: 1.476
batch start
#iterations: 209
currently lose_sum: 96.4060846567154
time_elpased: 1.464
batch start
#iterations: 210
currently lose_sum: 96.45169538259506
time_elpased: 1.465
batch start
#iterations: 211
currently lose_sum: 96.06434041261673
time_elpased: 1.517
batch start
#iterations: 212
currently lose_sum: 96.46653068065643
time_elpased: 1.477
batch start
#iterations: 213
currently lose_sum: 96.16945421695709
time_elpased: 1.461
batch start
#iterations: 214
currently lose_sum: 96.39296787977219
time_elpased: 1.481
batch start
#iterations: 215
currently lose_sum: 96.22508364915848
time_elpased: 1.46
batch start
#iterations: 216
currently lose_sum: 96.33010476827621
time_elpased: 1.456
batch start
#iterations: 217
currently lose_sum: 96.10414916276932
time_elpased: 1.461
batch start
#iterations: 218
currently lose_sum: 96.58630108833313
time_elpased: 1.491
batch start
#iterations: 219
currently lose_sum: 96.47889745235443
time_elpased: 1.47
start validation test
0.571958762887
0.600312366889
0.435113718226
0.504534606205
0.572199015469
66.497
batch start
#iterations: 220
currently lose_sum: 96.3670225739479
time_elpased: 1.466
batch start
#iterations: 221
currently lose_sum: 96.45943254232407
time_elpased: 1.473
batch start
#iterations: 222
currently lose_sum: 96.26348918676376
time_elpased: 1.536
batch start
#iterations: 223
currently lose_sum: 96.2359511256218
time_elpased: 1.47
batch start
#iterations: 224
currently lose_sum: 96.49263441562653
time_elpased: 1.487
batch start
#iterations: 225
currently lose_sum: 96.0126188993454
time_elpased: 1.46
batch start
#iterations: 226
currently lose_sum: 96.42065405845642
time_elpased: 1.456
batch start
#iterations: 227
currently lose_sum: 96.18576920032501
time_elpased: 1.472
batch start
#iterations: 228
currently lose_sum: 96.13354736566544
time_elpased: 1.443
batch start
#iterations: 229
currently lose_sum: 96.12045556306839
time_elpased: 1.441
batch start
#iterations: 230
currently lose_sum: 95.86351299285889
time_elpased: 1.468
batch start
#iterations: 231
currently lose_sum: 96.56677973270416
time_elpased: 1.479
batch start
#iterations: 232
currently lose_sum: 96.25058776140213
time_elpased: 1.465
batch start
#iterations: 233
currently lose_sum: 96.11562836170197
time_elpased: 1.48
batch start
#iterations: 234
currently lose_sum: 95.9623344540596
time_elpased: 1.471
batch start
#iterations: 235
currently lose_sum: 96.01927626132965
time_elpased: 1.471
batch start
#iterations: 236
currently lose_sum: 96.29082441329956
time_elpased: 1.512
batch start
#iterations: 237
currently lose_sum: 96.17605417966843
time_elpased: 1.464
batch start
#iterations: 238
currently lose_sum: 96.15718829631805
time_elpased: 1.508
batch start
#iterations: 239
currently lose_sum: 96.16201335191727
time_elpased: 1.452
start validation test
0.573298969072
0.613360642823
0.400638057013
0.48468625498
0.573602101934
66.502
batch start
#iterations: 240
currently lose_sum: 96.14241516590118
time_elpased: 1.478
batch start
#iterations: 241
currently lose_sum: 96.40825581550598
time_elpased: 1.495
batch start
#iterations: 242
currently lose_sum: 95.96044462919235
time_elpased: 1.469
batch start
#iterations: 243
currently lose_sum: 96.2022510766983
time_elpased: 1.456
batch start
#iterations: 244
currently lose_sum: 96.22351002693176
time_elpased: 1.52
batch start
#iterations: 245
currently lose_sum: 96.28397303819656
time_elpased: 1.493
batch start
#iterations: 246
currently lose_sum: 95.76839983463287
time_elpased: 1.477
batch start
#iterations: 247
currently lose_sum: 95.97538763284683
time_elpased: 1.482
batch start
#iterations: 248
currently lose_sum: 96.49354135990143
time_elpased: 1.483
batch start
#iterations: 249
currently lose_sum: 96.2410700917244
time_elpased: 1.455
batch start
#iterations: 250
currently lose_sum: 95.89885425567627
time_elpased: 1.465
batch start
#iterations: 251
currently lose_sum: 95.98961013555527
time_elpased: 1.487
batch start
#iterations: 252
currently lose_sum: 96.25317406654358
time_elpased: 1.547
batch start
#iterations: 253
currently lose_sum: 96.10290729999542
time_elpased: 1.481
batch start
#iterations: 254
currently lose_sum: 95.7437275648117
time_elpased: 1.484
batch start
#iterations: 255
currently lose_sum: 96.00682508945465
time_elpased: 1.456
batch start
#iterations: 256
currently lose_sum: 95.98462623357773
time_elpased: 1.479
batch start
#iterations: 257
currently lose_sum: 96.08228254318237
time_elpased: 1.451
batch start
#iterations: 258
currently lose_sum: 96.28943747282028
time_elpased: 1.493
batch start
#iterations: 259
currently lose_sum: 95.99649435281754
time_elpased: 1.486
start validation test
0.569484536082
0.602492866797
0.412884635175
0.489985344406
0.569759471362
66.989
batch start
#iterations: 260
currently lose_sum: 95.64884853363037
time_elpased: 1.472
batch start
#iterations: 261
currently lose_sum: 96.13479882478714
time_elpased: 1.512
batch start
#iterations: 262
currently lose_sum: 95.96177333593369
time_elpased: 1.498
batch start
#iterations: 263
currently lose_sum: 95.86791062355042
time_elpased: 1.458
batch start
#iterations: 264
currently lose_sum: 95.69486337900162
time_elpased: 1.464
batch start
#iterations: 265
currently lose_sum: 95.81222647428513
time_elpased: 1.461
batch start
#iterations: 266
currently lose_sum: 95.71428495645523
time_elpased: 1.486
batch start
#iterations: 267
currently lose_sum: 96.02205717563629
time_elpased: 1.479
batch start
#iterations: 268
currently lose_sum: 95.68164873123169
time_elpased: 1.498
batch start
#iterations: 269
currently lose_sum: 95.83685153722763
time_elpased: 1.482
batch start
#iterations: 270
currently lose_sum: 95.73187655210495
time_elpased: 1.456
batch start
#iterations: 271
currently lose_sum: 95.50425344705582
time_elpased: 1.474
batch start
#iterations: 272
currently lose_sum: 95.75513672828674
time_elpased: 1.488
batch start
#iterations: 273
currently lose_sum: 95.53032356500626
time_elpased: 1.469
batch start
#iterations: 274
currently lose_sum: 95.76512634754181
time_elpased: 1.483
batch start
#iterations: 275
currently lose_sum: 95.86289513111115
time_elpased: 1.522
batch start
#iterations: 276
currently lose_sum: 95.92596358060837
time_elpased: 1.492
batch start
#iterations: 277
currently lose_sum: 95.86134994029999
time_elpased: 1.476
batch start
#iterations: 278
currently lose_sum: 95.59018385410309
time_elpased: 1.45
batch start
#iterations: 279
currently lose_sum: 96.08516758680344
time_elpased: 1.467
start validation test
0.571907216495
0.613141025641
0.393742924771
0.479538760419
0.572220011389
66.914
batch start
#iterations: 280
currently lose_sum: 95.62519967556
time_elpased: 1.47
batch start
#iterations: 281
currently lose_sum: 95.68103605508804
time_elpased: 1.479
batch start
#iterations: 282
currently lose_sum: 95.65636259317398
time_elpased: 1.496
batch start
#iterations: 283
currently lose_sum: 95.5474420785904
time_elpased: 1.48
batch start
#iterations: 284
currently lose_sum: 95.4257281422615
time_elpased: 1.493
batch start
#iterations: 285
currently lose_sum: 95.85184854269028
time_elpased: 1.492
batch start
#iterations: 286
currently lose_sum: 95.31820845603943
time_elpased: 1.515
batch start
#iterations: 287
currently lose_sum: 95.51173502206802
time_elpased: 1.468
batch start
#iterations: 288
currently lose_sum: 95.44632536172867
time_elpased: 1.468
batch start
#iterations: 289
currently lose_sum: 95.6485703587532
time_elpased: 1.486
batch start
#iterations: 290
currently lose_sum: 95.39545994997025
time_elpased: 1.467
batch start
#iterations: 291
currently lose_sum: 95.3131656050682
time_elpased: 1.497
batch start
#iterations: 292
currently lose_sum: 95.73348653316498
time_elpased: 1.489
batch start
#iterations: 293
currently lose_sum: 95.30206286907196
time_elpased: 1.483
batch start
#iterations: 294
currently lose_sum: 95.24090498685837
time_elpased: 1.479
batch start
#iterations: 295
currently lose_sum: 95.40316820144653
time_elpased: 1.455
batch start
#iterations: 296
currently lose_sum: 95.77078664302826
time_elpased: 1.484
batch start
#iterations: 297
currently lose_sum: 95.50885212421417
time_elpased: 1.463
batch start
#iterations: 298
currently lose_sum: 95.53540068864822
time_elpased: 1.469
batch start
#iterations: 299
currently lose_sum: 95.42632240056992
time_elpased: 1.462
start validation test
0.56412371134
0.594969121856
0.406504065041
0.483003179261
0.56440043694
67.219
batch start
#iterations: 300
currently lose_sum: 95.48490101099014
time_elpased: 1.495
batch start
#iterations: 301
currently lose_sum: 95.17043697834015
time_elpased: 1.458
batch start
#iterations: 302
currently lose_sum: 95.601775765419
time_elpased: 1.466
batch start
#iterations: 303
currently lose_sum: 95.49299377202988
time_elpased: 1.479
batch start
#iterations: 304
currently lose_sum: 95.46570074558258
time_elpased: 1.487
batch start
#iterations: 305
currently lose_sum: 95.38533240556717
time_elpased: 1.467
batch start
#iterations: 306
currently lose_sum: 95.68473190069199
time_elpased: 1.5
batch start
#iterations: 307
currently lose_sum: 95.6630135178566
time_elpased: 1.493
batch start
#iterations: 308
currently lose_sum: 95.62024664878845
time_elpased: 1.502
batch start
#iterations: 309
currently lose_sum: 95.15941780805588
time_elpased: 1.481
batch start
#iterations: 310
currently lose_sum: 95.50518840551376
time_elpased: 1.495
batch start
#iterations: 311
currently lose_sum: 95.66437423229218
time_elpased: 1.481
batch start
#iterations: 312
currently lose_sum: 95.29807275533676
time_elpased: 1.48
batch start
#iterations: 313
currently lose_sum: 95.09955859184265
time_elpased: 1.504
batch start
#iterations: 314
currently lose_sum: 95.11129277944565
time_elpased: 1.452
batch start
#iterations: 315
currently lose_sum: 95.48280817270279
time_elpased: 1.449
batch start
#iterations: 316
currently lose_sum: 95.49007767438889
time_elpased: 1.482
batch start
#iterations: 317
currently lose_sum: 95.48877251148224
time_elpased: 1.45
batch start
#iterations: 318
currently lose_sum: 95.57114535570145
time_elpased: 1.454
batch start
#iterations: 319
currently lose_sum: 95.26599109172821
time_elpased: 1.458
start validation test
0.569329896907
0.611675959331
0.383863332304
0.471704078407
0.569655512068
67.406
batch start
#iterations: 320
currently lose_sum: 95.54723858833313
time_elpased: 1.475
batch start
#iterations: 321
currently lose_sum: 95.11844772100449
time_elpased: 1.461
batch start
#iterations: 322
currently lose_sum: 95.28526717424393
time_elpased: 1.459
batch start
#iterations: 323
currently lose_sum: 95.0706262588501
time_elpased: 1.463
batch start
#iterations: 324
currently lose_sum: 95.21372616291046
time_elpased: 1.499
batch start
#iterations: 325
currently lose_sum: 95.62689924240112
time_elpased: 1.48
batch start
#iterations: 326
currently lose_sum: 95.14335471391678
time_elpased: 1.456
batch start
#iterations: 327
currently lose_sum: 95.44698172807693
time_elpased: 1.476
batch start
#iterations: 328
currently lose_sum: 95.13678854703903
time_elpased: 1.496
batch start
#iterations: 329
currently lose_sum: 95.73761397600174
time_elpased: 1.44
batch start
#iterations: 330
currently lose_sum: 94.80674290657043
time_elpased: 1.487
batch start
#iterations: 331
currently lose_sum: 95.38602900505066
time_elpased: 1.479
batch start
#iterations: 332
currently lose_sum: 95.15558183193207
time_elpased: 1.484
batch start
#iterations: 333
currently lose_sum: 95.30042141675949
time_elpased: 1.468
batch start
#iterations: 334
currently lose_sum: 95.6677777171135
time_elpased: 1.469
batch start
#iterations: 335
currently lose_sum: 95.08539098501205
time_elpased: 1.46
batch start
#iterations: 336
currently lose_sum: 95.32021844387054
time_elpased: 1.507
batch start
#iterations: 337
currently lose_sum: 95.16827040910721
time_elpased: 1.492
batch start
#iterations: 338
currently lose_sum: 95.08172285556793
time_elpased: 1.459
batch start
#iterations: 339
currently lose_sum: 95.2540727853775
time_elpased: 1.449
start validation test
0.564793814433
0.614609571788
0.351548831944
0.447266775777
0.565168198891
68.081
batch start
#iterations: 340
currently lose_sum: 95.1103343963623
time_elpased: 1.488
batch start
#iterations: 341
currently lose_sum: 95.22356635332108
time_elpased: 1.483
batch start
#iterations: 342
currently lose_sum: 95.30711334943771
time_elpased: 1.506
batch start
#iterations: 343
currently lose_sum: 95.60328751802444
time_elpased: 1.458
batch start
#iterations: 344
currently lose_sum: 95.19642776250839
time_elpased: 1.5
batch start
#iterations: 345
currently lose_sum: 94.92185062170029
time_elpased: 1.469
batch start
#iterations: 346
currently lose_sum: 94.85935872793198
time_elpased: 1.465
batch start
#iterations: 347
currently lose_sum: 95.19087928533554
time_elpased: 1.492
batch start
#iterations: 348
currently lose_sum: 95.5283243060112
time_elpased: 1.479
batch start
#iterations: 349
currently lose_sum: 95.12377059459686
time_elpased: 1.478
batch start
#iterations: 350
currently lose_sum: 94.989910364151
time_elpased: 1.542
batch start
#iterations: 351
currently lose_sum: 94.92866659164429
time_elpased: 1.5
batch start
#iterations: 352
currently lose_sum: 95.28246861696243
time_elpased: 1.516
batch start
#iterations: 353
currently lose_sum: 95.38987970352173
time_elpased: 1.476
batch start
#iterations: 354
currently lose_sum: 95.13455724716187
time_elpased: 1.469
batch start
#iterations: 355
currently lose_sum: 94.85207551717758
time_elpased: 1.507
batch start
#iterations: 356
currently lose_sum: 94.81981074810028
time_elpased: 1.506
batch start
#iterations: 357
currently lose_sum: 94.81053638458252
time_elpased: 1.461
batch start
#iterations: 358
currently lose_sum: 94.85047441720963
time_elpased: 1.49
batch start
#iterations: 359
currently lose_sum: 94.93056571483612
time_elpased: 1.446
start validation test
0.569020618557
0.618531468531
0.364104147371
0.45837921876
0.569380381028
67.349
batch start
#iterations: 360
currently lose_sum: 95.16777139902115
time_elpased: 1.494
batch start
#iterations: 361
currently lose_sum: 94.67416632175446
time_elpased: 1.47
batch start
#iterations: 362
currently lose_sum: 94.95299708843231
time_elpased: 1.475
batch start
#iterations: 363
currently lose_sum: 94.6885444521904
time_elpased: 1.46
batch start
#iterations: 364
currently lose_sum: 95.13654446601868
time_elpased: 1.491
batch start
#iterations: 365
currently lose_sum: 94.9097580909729
time_elpased: 1.446
batch start
#iterations: 366
currently lose_sum: 94.95069074630737
time_elpased: 1.507
batch start
#iterations: 367
currently lose_sum: 95.05485248565674
time_elpased: 1.433
batch start
#iterations: 368
currently lose_sum: 94.88650596141815
time_elpased: 1.495
batch start
#iterations: 369
currently lose_sum: 94.889783680439
time_elpased: 1.44
batch start
#iterations: 370
currently lose_sum: 94.64690816402435
time_elpased: 1.504
batch start
#iterations: 371
currently lose_sum: 94.69003486633301
time_elpased: 1.461
batch start
#iterations: 372
currently lose_sum: 94.87694269418716
time_elpased: 1.485
batch start
#iterations: 373
currently lose_sum: 94.74249565601349
time_elpased: 1.465
batch start
#iterations: 374
currently lose_sum: 95.1701592206955
time_elpased: 1.458
batch start
#iterations: 375
currently lose_sum: 94.9366112947464
time_elpased: 1.454
batch start
#iterations: 376
currently lose_sum: 94.7388853430748
time_elpased: 1.46
batch start
#iterations: 377
currently lose_sum: 94.64953178167343
time_elpased: 1.485
batch start
#iterations: 378
currently lose_sum: 94.97845757007599
time_elpased: 1.452
batch start
#iterations: 379
currently lose_sum: 94.80681025981903
time_elpased: 1.502
start validation test
0.571288659794
0.599914359121
0.432540907688
0.5026610058
0.571532252873
67.285
batch start
#iterations: 380
currently lose_sum: 94.98651278018951
time_elpased: 1.493
batch start
#iterations: 381
currently lose_sum: 94.63071602582932
time_elpased: 1.462
batch start
#iterations: 382
currently lose_sum: 95.00794738531113
time_elpased: 1.466
batch start
#iterations: 383
currently lose_sum: 94.69222348928452
time_elpased: 1.47
batch start
#iterations: 384
currently lose_sum: 94.94051939249039
time_elpased: 1.499
batch start
#iterations: 385
currently lose_sum: 94.83341962099075
time_elpased: 1.477
batch start
#iterations: 386
currently lose_sum: 94.77064746618271
time_elpased: 1.472
batch start
#iterations: 387
currently lose_sum: 94.7224749326706
time_elpased: 1.494
batch start
#iterations: 388
currently lose_sum: 94.76324826478958
time_elpased: 1.47
batch start
#iterations: 389
currently lose_sum: 94.7646849155426
time_elpased: 1.486
batch start
#iterations: 390
currently lose_sum: 94.92059242725372
time_elpased: 1.516
batch start
#iterations: 391
currently lose_sum: 94.85566198825836
time_elpased: 1.468
batch start
#iterations: 392
currently lose_sum: 94.67661738395691
time_elpased: 1.447
batch start
#iterations: 393
currently lose_sum: 94.54021394252777
time_elpased: 1.465
batch start
#iterations: 394
currently lose_sum: 94.47698134183884
time_elpased: 1.468
batch start
#iterations: 395
currently lose_sum: 94.47002136707306
time_elpased: 1.492
batch start
#iterations: 396
currently lose_sum: 94.75379848480225
time_elpased: 1.458
batch start
#iterations: 397
currently lose_sum: 94.91796070337296
time_elpased: 1.496
batch start
#iterations: 398
currently lose_sum: 94.60145723819733
time_elpased: 1.462
batch start
#iterations: 399
currently lose_sum: 94.6626706123352
time_elpased: 1.474
start validation test
0.56087628866
0.605643738977
0.353401255532
0.446350815624
0.561240543081
68.102
acc: 0.588
pre: 0.600
rec: 0.533
F1: 0.565
auc: 0.611
