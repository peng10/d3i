start to construct graph
graph construct over
29150
epochs start
batch start
#iterations: 0
currently lose_sum: 100.57876044511795
time_elpased: 1.436
batch start
#iterations: 1
currently lose_sum: 100.17368167638779
time_elpased: 1.462
batch start
#iterations: 2
currently lose_sum: 100.09349298477173
time_elpased: 1.437
batch start
#iterations: 3
currently lose_sum: 99.94998788833618
time_elpased: 1.456
batch start
#iterations: 4
currently lose_sum: 99.82618707418442
time_elpased: 1.415
batch start
#iterations: 5
currently lose_sum: 99.74451541900635
time_elpased: 1.417
batch start
#iterations: 6
currently lose_sum: 99.7187597155571
time_elpased: 1.422
batch start
#iterations: 7
currently lose_sum: 99.60501623153687
time_elpased: 1.421
batch start
#iterations: 8
currently lose_sum: 99.51022529602051
time_elpased: 1.458
batch start
#iterations: 9
currently lose_sum: 99.43479627370834
time_elpased: 1.465
batch start
#iterations: 10
currently lose_sum: 99.69332134723663
time_elpased: 1.465
batch start
#iterations: 11
currently lose_sum: 99.5184833407402
time_elpased: 1.414
batch start
#iterations: 12
currently lose_sum: 99.53224325180054
time_elpased: 1.489
batch start
#iterations: 13
currently lose_sum: 99.45510989427567
time_elpased: 1.461
batch start
#iterations: 14
currently lose_sum: 99.59280204772949
time_elpased: 1.443
batch start
#iterations: 15
currently lose_sum: 99.3442667722702
time_elpased: 1.438
batch start
#iterations: 16
currently lose_sum: 99.33694207668304
time_elpased: 1.449
batch start
#iterations: 17
currently lose_sum: 99.59210735559464
time_elpased: 1.449
batch start
#iterations: 18
currently lose_sum: 99.08703994750977
time_elpased: 1.429
batch start
#iterations: 19
currently lose_sum: 99.30101293325424
time_elpased: 1.429
start validation test
0.581340206186
0.611491681812
0.450138931769
0.518553645525
0.581570550259
65.673
batch start
#iterations: 20
currently lose_sum: 99.20552730560303
time_elpased: 1.422
batch start
#iterations: 21
currently lose_sum: 99.32598346471786
time_elpased: 1.445
batch start
#iterations: 22
currently lose_sum: 99.27762293815613
time_elpased: 1.417
batch start
#iterations: 23
currently lose_sum: 99.22636896371841
time_elpased: 1.447
batch start
#iterations: 24
currently lose_sum: 99.01549643278122
time_elpased: 1.423
batch start
#iterations: 25
currently lose_sum: 99.29651141166687
time_elpased: 1.432
batch start
#iterations: 26
currently lose_sum: 98.96979576349258
time_elpased: 1.42
batch start
#iterations: 27
currently lose_sum: 99.03928923606873
time_elpased: 1.432
batch start
#iterations: 28
currently lose_sum: 98.82045829296112
time_elpased: 1.488
batch start
#iterations: 29
currently lose_sum: 99.00392234325409
time_elpased: 1.425
batch start
#iterations: 30
currently lose_sum: 98.85947626829147
time_elpased: 1.451
batch start
#iterations: 31
currently lose_sum: 98.79072850942612
time_elpased: 1.425
batch start
#iterations: 32
currently lose_sum: 98.8411865234375
time_elpased: 1.439
batch start
#iterations: 33
currently lose_sum: 98.8804926276207
time_elpased: 1.436
batch start
#iterations: 34
currently lose_sum: 98.85704356431961
time_elpased: 1.435
batch start
#iterations: 35
currently lose_sum: 98.76519340276718
time_elpased: 1.434
batch start
#iterations: 36
currently lose_sum: 98.70180839300156
time_elpased: 1.441
batch start
#iterations: 37
currently lose_sum: 99.03042334318161
time_elpased: 1.429
batch start
#iterations: 38
currently lose_sum: 98.80623942613602
time_elpased: 1.417
batch start
#iterations: 39
currently lose_sum: 98.8893626332283
time_elpased: 1.456
start validation test
0.583969072165
0.608375032921
0.475455387465
0.53376465831
0.584159584676
65.642
batch start
#iterations: 40
currently lose_sum: 98.5909218788147
time_elpased: 1.455
batch start
#iterations: 41
currently lose_sum: 98.76535958051682
time_elpased: 1.46
batch start
#iterations: 42
currently lose_sum: 98.6080669760704
time_elpased: 1.428
batch start
#iterations: 43
currently lose_sum: 98.7108570933342
time_elpased: 1.43
batch start
#iterations: 44
currently lose_sum: 98.7519593834877
time_elpased: 1.447
batch start
#iterations: 45
currently lose_sum: 98.72088992595673
time_elpased: 1.428
batch start
#iterations: 46
currently lose_sum: 98.34380596876144
time_elpased: 1.487
batch start
#iterations: 47
currently lose_sum: 98.54744857549667
time_elpased: 1.463
batch start
#iterations: 48
currently lose_sum: 98.80172485113144
time_elpased: 1.427
batch start
#iterations: 49
currently lose_sum: 98.30737274885178
time_elpased: 1.451
batch start
#iterations: 50
currently lose_sum: 98.35076236724854
time_elpased: 1.413
batch start
#iterations: 51
currently lose_sum: 98.34973728656769
time_elpased: 1.432
batch start
#iterations: 52
currently lose_sum: 98.62718033790588
time_elpased: 1.441
batch start
#iterations: 53
currently lose_sum: 98.5004757642746
time_elpased: 1.439
batch start
#iterations: 54
currently lose_sum: 98.43051880598068
time_elpased: 1.45
batch start
#iterations: 55
currently lose_sum: 98.40125149488449
time_elpased: 1.452
batch start
#iterations: 56
currently lose_sum: 98.4857981801033
time_elpased: 1.446
batch start
#iterations: 57
currently lose_sum: 98.37087994813919
time_elpased: 1.43
batch start
#iterations: 58
currently lose_sum: 98.34695613384247
time_elpased: 1.466
batch start
#iterations: 59
currently lose_sum: 98.56498008966446
time_elpased: 1.451
start validation test
0.574329896907
0.631512529295
0.360502212617
0.458988469602
0.574705304388
65.675
batch start
#iterations: 60
currently lose_sum: 98.02511870861053
time_elpased: 1.418
batch start
#iterations: 61
currently lose_sum: 98.45838409662247
time_elpased: 1.495
batch start
#iterations: 62
currently lose_sum: 98.49994349479675
time_elpased: 1.435
batch start
#iterations: 63
currently lose_sum: 98.61164718866348
time_elpased: 1.424
batch start
#iterations: 64
currently lose_sum: 98.19832116365433
time_elpased: 1.426
batch start
#iterations: 65
currently lose_sum: 97.99305301904678
time_elpased: 1.432
batch start
#iterations: 66
currently lose_sum: 98.11832958459854
time_elpased: 1.45
batch start
#iterations: 67
currently lose_sum: 98.17251467704773
time_elpased: 1.447
batch start
#iterations: 68
currently lose_sum: 98.20792579650879
time_elpased: 1.453
batch start
#iterations: 69
currently lose_sum: 98.13203626871109
time_elpased: 1.477
batch start
#iterations: 70
currently lose_sum: 98.35538601875305
time_elpased: 1.403
batch start
#iterations: 71
currently lose_sum: 98.2467594742775
time_elpased: 1.43
batch start
#iterations: 72
currently lose_sum: 98.05689424276352
time_elpased: 1.41
batch start
#iterations: 73
currently lose_sum: 98.1513666510582
time_elpased: 1.462
batch start
#iterations: 74
currently lose_sum: 98.18202608823776
time_elpased: 1.451
batch start
#iterations: 75
currently lose_sum: 98.13961052894592
time_elpased: 1.469
batch start
#iterations: 76
currently lose_sum: 98.23709547519684
time_elpased: 1.449
batch start
#iterations: 77
currently lose_sum: 98.31659775972366
time_elpased: 1.445
batch start
#iterations: 78
currently lose_sum: 98.11845582723618
time_elpased: 1.442
batch start
#iterations: 79
currently lose_sum: 98.23935997486115
time_elpased: 1.472
start validation test
0.580670103093
0.623555139019
0.410826386745
0.495316086606
0.580968289933
65.585
batch start
#iterations: 80
currently lose_sum: 97.999058842659
time_elpased: 1.41
batch start
#iterations: 81
currently lose_sum: 97.9446188211441
time_elpased: 1.446
batch start
#iterations: 82
currently lose_sum: 98.10944712162018
time_elpased: 1.431
batch start
#iterations: 83
currently lose_sum: 97.9184884428978
time_elpased: 1.439
batch start
#iterations: 84
currently lose_sum: 97.96281552314758
time_elpased: 1.416
batch start
#iterations: 85
currently lose_sum: 97.97043877840042
time_elpased: 1.437
batch start
#iterations: 86
currently lose_sum: 97.88030689954758
time_elpased: 1.438
batch start
#iterations: 87
currently lose_sum: 98.1335956454277
time_elpased: 1.474
batch start
#iterations: 88
currently lose_sum: 97.85972839593887
time_elpased: 1.435
batch start
#iterations: 89
currently lose_sum: 97.96999794244766
time_elpased: 1.446
batch start
#iterations: 90
currently lose_sum: 98.00447809696198
time_elpased: 1.452
batch start
#iterations: 91
currently lose_sum: 98.02546691894531
time_elpased: 1.455
batch start
#iterations: 92
currently lose_sum: 97.73887628316879
time_elpased: 1.492
batch start
#iterations: 93
currently lose_sum: 98.19372218847275
time_elpased: 1.464
batch start
#iterations: 94
currently lose_sum: 97.89804667234421
time_elpased: 1.419
batch start
#iterations: 95
currently lose_sum: 97.71346038579941
time_elpased: 1.45
batch start
#iterations: 96
currently lose_sum: 97.6641035079956
time_elpased: 1.452
batch start
#iterations: 97
currently lose_sum: 97.82692229747772
time_elpased: 1.423
batch start
#iterations: 98
currently lose_sum: 97.74926489591599
time_elpased: 1.462
batch start
#iterations: 99
currently lose_sum: 97.51892560720444
time_elpased: 1.445
start validation test
0.584587628866
0.606884992264
0.484408768138
0.538774108625
0.584763508308
65.332
batch start
#iterations: 100
currently lose_sum: 97.86688071489334
time_elpased: 1.442
batch start
#iterations: 101
currently lose_sum: 97.8363966345787
time_elpased: 1.422
batch start
#iterations: 102
currently lose_sum: 97.8475894331932
time_elpased: 1.428
batch start
#iterations: 103
currently lose_sum: 97.84570151567459
time_elpased: 1.413
batch start
#iterations: 104
currently lose_sum: 97.82035654783249
time_elpased: 1.455
batch start
#iterations: 105
currently lose_sum: 97.4138252735138
time_elpased: 1.451
batch start
#iterations: 106
currently lose_sum: 97.70301562547684
time_elpased: 1.427
batch start
#iterations: 107
currently lose_sum: 97.73807030916214
time_elpased: 1.448
batch start
#iterations: 108
currently lose_sum: 97.56924593448639
time_elpased: 1.428
batch start
#iterations: 109
currently lose_sum: 97.80419218540192
time_elpased: 1.416
batch start
#iterations: 110
currently lose_sum: 97.58632469177246
time_elpased: 1.449
batch start
#iterations: 111
currently lose_sum: 97.30525761842728
time_elpased: 1.416
batch start
#iterations: 112
currently lose_sum: 97.48217421770096
time_elpased: 1.461
batch start
#iterations: 113
currently lose_sum: 97.58259427547455
time_elpased: 1.437
batch start
#iterations: 114
currently lose_sum: 97.2175230383873
time_elpased: 1.427
batch start
#iterations: 115
currently lose_sum: 97.62524771690369
time_elpased: 1.436
batch start
#iterations: 116
currently lose_sum: 97.15361446142197
time_elpased: 1.429
batch start
#iterations: 117
currently lose_sum: 97.47061389684677
time_elpased: 1.506
batch start
#iterations: 118
currently lose_sum: 97.63755679130554
time_elpased: 1.437
batch start
#iterations: 119
currently lose_sum: 97.71806305646896
time_elpased: 1.428
start validation test
0.572422680412
0.606341609333
0.41720695688
0.494299823203
0.572695185555
65.850
batch start
#iterations: 120
currently lose_sum: 97.14973610639572
time_elpased: 1.43
batch start
#iterations: 121
currently lose_sum: 97.34923124313354
time_elpased: 1.431
batch start
#iterations: 122
currently lose_sum: 97.45902508497238
time_elpased: 1.437
batch start
#iterations: 123
currently lose_sum: 97.42829525470734
time_elpased: 1.424
batch start
#iterations: 124
currently lose_sum: 97.44273954629898
time_elpased: 1.433
batch start
#iterations: 125
currently lose_sum: 97.2946308851242
time_elpased: 1.434
batch start
#iterations: 126
currently lose_sum: 97.4040904045105
time_elpased: 1.426
batch start
#iterations: 127
currently lose_sum: 97.4173064827919
time_elpased: 1.426
batch start
#iterations: 128
currently lose_sum: 97.45916587114334
time_elpased: 1.429
batch start
#iterations: 129
currently lose_sum: 97.67086136341095
time_elpased: 1.428
batch start
#iterations: 130
currently lose_sum: 97.31173360347748
time_elpased: 1.447
batch start
#iterations: 131
currently lose_sum: 97.57363760471344
time_elpased: 1.425
batch start
#iterations: 132
currently lose_sum: 97.33684837818146
time_elpased: 1.454
batch start
#iterations: 133
currently lose_sum: 97.3460842370987
time_elpased: 1.483
batch start
#iterations: 134
currently lose_sum: 97.23152488470078
time_elpased: 1.463
batch start
#iterations: 135
currently lose_sum: 97.14346927404404
time_elpased: 1.46
batch start
#iterations: 136
currently lose_sum: 97.39900064468384
time_elpased: 1.429
batch start
#iterations: 137
currently lose_sum: 97.00939923524857
time_elpased: 1.445
batch start
#iterations: 138
currently lose_sum: 96.98162746429443
time_elpased: 1.425
batch start
#iterations: 139
currently lose_sum: 97.34119272232056
time_elpased: 1.501
start validation test
0.582680412371
0.59574719433
0.518987341772
0.554724452755
0.582792235381
65.601
batch start
#iterations: 140
currently lose_sum: 97.19591426849365
time_elpased: 1.417
batch start
#iterations: 141
currently lose_sum: 97.10767441987991
time_elpased: 1.431
batch start
#iterations: 142
currently lose_sum: 97.30709290504456
time_elpased: 1.426
batch start
#iterations: 143
currently lose_sum: 97.21922039985657
time_elpased: 1.461
batch start
#iterations: 144
currently lose_sum: 97.15930891036987
time_elpased: 1.429
batch start
#iterations: 145
currently lose_sum: 96.94222337007523
time_elpased: 1.426
batch start
#iterations: 146
currently lose_sum: 96.90739291906357
time_elpased: 1.466
batch start
#iterations: 147
currently lose_sum: 96.81431889533997
time_elpased: 1.438
batch start
#iterations: 148
currently lose_sum: 97.26784479618073
time_elpased: 1.425
batch start
#iterations: 149
currently lose_sum: 96.86428278684616
time_elpased: 1.491
batch start
#iterations: 150
currently lose_sum: 97.23823320865631
time_elpased: 1.515
batch start
#iterations: 151
currently lose_sum: 96.9550029039383
time_elpased: 1.488
batch start
#iterations: 152
currently lose_sum: 97.17884927988052
time_elpased: 1.448
batch start
#iterations: 153
currently lose_sum: 97.13623249530792
time_elpased: 1.456
batch start
#iterations: 154
currently lose_sum: 97.09380674362183
time_elpased: 1.448
batch start
#iterations: 155
currently lose_sum: 96.93615788221359
time_elpased: 1.463
batch start
#iterations: 156
currently lose_sum: 96.75627774000168
time_elpased: 1.475
batch start
#iterations: 157
currently lose_sum: 96.83061647415161
time_elpased: 1.439
batch start
#iterations: 158
currently lose_sum: 96.72434443235397
time_elpased: 1.418
batch start
#iterations: 159
currently lose_sum: 96.99717688560486
time_elpased: 1.462
start validation test
0.576546391753
0.592328497664
0.495832046928
0.539801691782
0.576688098234
65.811
batch start
#iterations: 160
currently lose_sum: 96.84507179260254
time_elpased: 1.45
batch start
#iterations: 161
currently lose_sum: 96.99204963445663
time_elpased: 1.519
batch start
#iterations: 162
currently lose_sum: 97.08496397733688
time_elpased: 1.423
batch start
#iterations: 163
currently lose_sum: 96.89911270141602
time_elpased: 1.432
batch start
#iterations: 164
currently lose_sum: 96.7386121749878
time_elpased: 1.429
batch start
#iterations: 165
currently lose_sum: 96.89821672439575
time_elpased: 1.445
batch start
#iterations: 166
currently lose_sum: 96.58952462673187
time_elpased: 1.467
batch start
#iterations: 167
currently lose_sum: 96.94083946943283
time_elpased: 1.443
batch start
#iterations: 168
currently lose_sum: 96.6997463107109
time_elpased: 1.433
batch start
#iterations: 169
currently lose_sum: 96.96161651611328
time_elpased: 1.438
batch start
#iterations: 170
currently lose_sum: 96.96323001384735
time_elpased: 1.496
batch start
#iterations: 171
currently lose_sum: 96.96807765960693
time_elpased: 1.434
batch start
#iterations: 172
currently lose_sum: 96.7474410533905
time_elpased: 1.45
batch start
#iterations: 173
currently lose_sum: 96.89974987506866
time_elpased: 1.432
batch start
#iterations: 174
currently lose_sum: 96.71600890159607
time_elpased: 1.43
batch start
#iterations: 175
currently lose_sum: 96.61452007293701
time_elpased: 1.415
batch start
#iterations: 176
currently lose_sum: 96.85512030124664
time_elpased: 1.452
batch start
#iterations: 177
currently lose_sum: 96.72300976514816
time_elpased: 1.46
batch start
#iterations: 178
currently lose_sum: 96.66323447227478
time_elpased: 1.438
batch start
#iterations: 179
currently lose_sum: 96.51180100440979
time_elpased: 1.429
start validation test
0.57793814433
0.612047486443
0.429762272306
0.504957678356
0.578198289928
65.981
batch start
#iterations: 180
currently lose_sum: 96.69327360391617
time_elpased: 1.425
batch start
#iterations: 181
currently lose_sum: 96.58871012926102
time_elpased: 1.494
batch start
#iterations: 182
currently lose_sum: 96.66439151763916
time_elpased: 1.437
batch start
#iterations: 183
currently lose_sum: 96.38711255788803
time_elpased: 1.487
batch start
#iterations: 184
currently lose_sum: 96.73092699050903
time_elpased: 1.446
batch start
#iterations: 185
currently lose_sum: 96.6218466758728
time_elpased: 1.478
batch start
#iterations: 186
currently lose_sum: 96.6449726819992
time_elpased: 1.455
batch start
#iterations: 187
currently lose_sum: 96.48133534193039
time_elpased: 1.449
batch start
#iterations: 188
currently lose_sum: 96.46443444490433
time_elpased: 1.411
batch start
#iterations: 189
currently lose_sum: 96.80272340774536
time_elpased: 1.446
batch start
#iterations: 190
currently lose_sum: 96.36160826683044
time_elpased: 1.439
batch start
#iterations: 191
currently lose_sum: 96.5172513127327
time_elpased: 1.453
batch start
#iterations: 192
currently lose_sum: 96.55390691757202
time_elpased: 1.445
batch start
#iterations: 193
currently lose_sum: 96.54550683498383
time_elpased: 1.445
batch start
#iterations: 194
currently lose_sum: 96.38426774740219
time_elpased: 1.466
batch start
#iterations: 195
currently lose_sum: 96.6344313621521
time_elpased: 1.45
batch start
#iterations: 196
currently lose_sum: 96.4292608499527
time_elpased: 1.417
batch start
#iterations: 197
currently lose_sum: 96.31603044271469
time_elpased: 1.472
batch start
#iterations: 198
currently lose_sum: 96.62478959560394
time_elpased: 1.442
batch start
#iterations: 199
currently lose_sum: 96.65765655040741
time_elpased: 1.435
start validation test
0.575463917526
0.595216664524
0.476381599259
0.529210014862
0.575637871818
65.860
batch start
#iterations: 200
currently lose_sum: 96.65605705976486
time_elpased: 1.426
batch start
#iterations: 201
currently lose_sum: 96.66394019126892
time_elpased: 1.471
batch start
#iterations: 202
currently lose_sum: 96.47477555274963
time_elpased: 1.434
batch start
#iterations: 203
currently lose_sum: 96.19215428829193
time_elpased: 1.455
batch start
#iterations: 204
currently lose_sum: 96.41563081741333
time_elpased: 1.447
batch start
#iterations: 205
currently lose_sum: 96.41682189702988
time_elpased: 1.443
batch start
#iterations: 206
currently lose_sum: 96.5153939127922
time_elpased: 1.442
batch start
#iterations: 207
currently lose_sum: 96.37251019477844
time_elpased: 1.454
batch start
#iterations: 208
currently lose_sum: 96.14283376932144
time_elpased: 1.44
batch start
#iterations: 209
currently lose_sum: 96.28339213132858
time_elpased: 1.473
batch start
#iterations: 210
currently lose_sum: 96.1807472705841
time_elpased: 1.416
batch start
#iterations: 211
currently lose_sum: 96.29306417703629
time_elpased: 1.433
batch start
#iterations: 212
currently lose_sum: 96.214251101017
time_elpased: 1.447
batch start
#iterations: 213
currently lose_sum: 96.3687749505043
time_elpased: 1.439
batch start
#iterations: 214
currently lose_sum: 96.18127995729446
time_elpased: 1.474
batch start
#iterations: 215
currently lose_sum: 96.30628257989883
time_elpased: 1.471
batch start
#iterations: 216
currently lose_sum: 96.0408626794815
time_elpased: 1.443
batch start
#iterations: 217
currently lose_sum: 96.05816751718521
time_elpased: 1.466
batch start
#iterations: 218
currently lose_sum: 95.95753061771393
time_elpased: 1.428
batch start
#iterations: 219
currently lose_sum: 96.4358594417572
time_elpased: 1.459
start validation test
0.576134020619
0.59718969555
0.472368014819
0.527495259438
0.576316197847
66.059
batch start
#iterations: 220
currently lose_sum: 96.20941257476807
time_elpased: 1.418
batch start
#iterations: 221
currently lose_sum: 96.21992433071136
time_elpased: 1.451
batch start
#iterations: 222
currently lose_sum: 96.02944678068161
time_elpased: 1.502
batch start
#iterations: 223
currently lose_sum: 96.26441997289658
time_elpased: 1.464
batch start
#iterations: 224
currently lose_sum: 96.21908688545227
time_elpased: 1.412
batch start
#iterations: 225
currently lose_sum: 96.18088257312775
time_elpased: 1.425
batch start
#iterations: 226
currently lose_sum: 96.12165492773056
time_elpased: 1.463
batch start
#iterations: 227
currently lose_sum: 95.86556452512741
time_elpased: 1.441
batch start
#iterations: 228
currently lose_sum: 96.30694460868835
time_elpased: 1.435
batch start
#iterations: 229
currently lose_sum: 96.00688809156418
time_elpased: 1.432
batch start
#iterations: 230
currently lose_sum: 96.01112776994705
time_elpased: 1.446
batch start
#iterations: 231
currently lose_sum: 95.92951542139053
time_elpased: 1.458
batch start
#iterations: 232
currently lose_sum: 95.6517305970192
time_elpased: 1.473
batch start
#iterations: 233
currently lose_sum: 95.85975283384323
time_elpased: 1.465
batch start
#iterations: 234
currently lose_sum: 95.97275900840759
time_elpased: 1.469
batch start
#iterations: 235
currently lose_sum: 95.77380067110062
time_elpased: 1.478
batch start
#iterations: 236
currently lose_sum: 95.72668117284775
time_elpased: 1.432
batch start
#iterations: 237
currently lose_sum: 95.98196041584015
time_elpased: 1.45
batch start
#iterations: 238
currently lose_sum: 96.04112428426743
time_elpased: 1.44
batch start
#iterations: 239
currently lose_sum: 95.66302525997162
time_elpased: 1.469
start validation test
0.571134020619
0.60458152418
0.415560358135
0.492559160771
0.571407154179
66.523
batch start
#iterations: 240
currently lose_sum: 95.82826048135757
time_elpased: 1.427
batch start
#iterations: 241
currently lose_sum: 95.82589596509933
time_elpased: 1.509
batch start
#iterations: 242
currently lose_sum: 96.04810327291489
time_elpased: 1.424
batch start
#iterations: 243
currently lose_sum: 95.83590012788773
time_elpased: 1.47
batch start
#iterations: 244
currently lose_sum: 95.92678248882294
time_elpased: 1.448
batch start
#iterations: 245
currently lose_sum: 95.96479570865631
time_elpased: 1.47
batch start
#iterations: 246
currently lose_sum: 95.78908008337021
time_elpased: 1.43
batch start
#iterations: 247
currently lose_sum: 95.61954742670059
time_elpased: 1.458
batch start
#iterations: 248
currently lose_sum: 95.96854984760284
time_elpased: 1.472
batch start
#iterations: 249
currently lose_sum: 95.85640919208527
time_elpased: 1.44
batch start
#iterations: 250
currently lose_sum: 95.82340157032013
time_elpased: 1.425
batch start
#iterations: 251
currently lose_sum: 95.78104394674301
time_elpased: 1.455
batch start
#iterations: 252
currently lose_sum: 96.15564334392548
time_elpased: 1.427
batch start
#iterations: 253
currently lose_sum: 95.62448930740356
time_elpased: 1.457
batch start
#iterations: 254
currently lose_sum: 95.9566855430603
time_elpased: 1.414
batch start
#iterations: 255
currently lose_sum: 95.91647970676422
time_elpased: 1.439
batch start
#iterations: 256
currently lose_sum: 95.7541030049324
time_elpased: 1.431
batch start
#iterations: 257
currently lose_sum: 96.08978456258774
time_elpased: 1.423
batch start
#iterations: 258
currently lose_sum: 95.86236888170242
time_elpased: 1.424
batch start
#iterations: 259
currently lose_sum: 95.81847709417343
time_elpased: 1.457
start validation test
0.575206185567
0.619224555735
0.394463311722
0.481926196014
0.57552350756
66.633
batch start
#iterations: 260
currently lose_sum: 95.54971981048584
time_elpased: 1.416
batch start
#iterations: 261
currently lose_sum: 95.26015019416809
time_elpased: 1.512
batch start
#iterations: 262
currently lose_sum: 95.67987877130508
time_elpased: 1.418
batch start
#iterations: 263
currently lose_sum: 95.63837498426437
time_elpased: 1.46
batch start
#iterations: 264
currently lose_sum: 95.58441054821014
time_elpased: 1.449
batch start
#iterations: 265
currently lose_sum: 95.48736673593521
time_elpased: 1.426
batch start
#iterations: 266
currently lose_sum: 95.63788664340973
time_elpased: 1.48
batch start
#iterations: 267
currently lose_sum: 96.01545345783234
time_elpased: 1.432
batch start
#iterations: 268
currently lose_sum: 95.69961935281754
time_elpased: 1.425
batch start
#iterations: 269
currently lose_sum: 95.66805332899094
time_elpased: 1.482
batch start
#iterations: 270
currently lose_sum: 95.74585604667664
time_elpased: 1.436
batch start
#iterations: 271
currently lose_sum: 95.62962633371353
time_elpased: 1.437
batch start
#iterations: 272
currently lose_sum: 95.38735687732697
time_elpased: 1.453
batch start
#iterations: 273
currently lose_sum: 95.79157656431198
time_elpased: 1.473
batch start
#iterations: 274
currently lose_sum: 95.64254504442215
time_elpased: 1.439
batch start
#iterations: 275
currently lose_sum: 95.6647098660469
time_elpased: 1.448
batch start
#iterations: 276
currently lose_sum: 95.80224770307541
time_elpased: 1.478
batch start
#iterations: 277
currently lose_sum: 95.47028303146362
time_elpased: 1.453
batch start
#iterations: 278
currently lose_sum: 95.57278072834015
time_elpased: 1.42
batch start
#iterations: 279
currently lose_sum: 95.77888816595078
time_elpased: 1.49
start validation test
0.572164948454
0.601664514278
0.431511783472
0.502577010668
0.572411886779
66.565
batch start
#iterations: 280
currently lose_sum: 95.24324488639832
time_elpased: 1.466
batch start
#iterations: 281
currently lose_sum: 95.42392987012863
time_elpased: 1.437
batch start
#iterations: 282
currently lose_sum: 95.47306489944458
time_elpased: 1.436
batch start
#iterations: 283
currently lose_sum: 95.60319977998734
time_elpased: 1.454
batch start
#iterations: 284
currently lose_sum: 95.35165244340897
time_elpased: 1.446
batch start
#iterations: 285
currently lose_sum: 95.45595461130142
time_elpased: 1.426
batch start
#iterations: 286
currently lose_sum: 95.53017485141754
time_elpased: 1.447
batch start
#iterations: 287
currently lose_sum: 95.37766253948212
time_elpased: 1.436
batch start
#iterations: 288
currently lose_sum: 95.45254737138748
time_elpased: 1.428
batch start
#iterations: 289
currently lose_sum: 95.04258728027344
time_elpased: 1.423
batch start
#iterations: 290
currently lose_sum: 95.55879312753677
time_elpased: 1.446
batch start
#iterations: 291
currently lose_sum: 95.33178859949112
time_elpased: 1.442
batch start
#iterations: 292
currently lose_sum: 95.29418051242828
time_elpased: 1.46
batch start
#iterations: 293
currently lose_sum: 95.6380952000618
time_elpased: 1.441
batch start
#iterations: 294
currently lose_sum: 95.53177952766418
time_elpased: 1.447
batch start
#iterations: 295
currently lose_sum: 95.43580460548401
time_elpased: 1.46
batch start
#iterations: 296
currently lose_sum: 95.20737439393997
time_elpased: 1.438
batch start
#iterations: 297
currently lose_sum: 95.68576073646545
time_elpased: 1.502
batch start
#iterations: 298
currently lose_sum: 95.23463809490204
time_elpased: 1.432
batch start
#iterations: 299
currently lose_sum: 95.16023123264313
time_elpased: 1.436
start validation test
0.569690721649
0.612305168171
0.384069157147
0.472046546926
0.570016608936
67.004
batch start
#iterations: 300
currently lose_sum: 95.3054433465004
time_elpased: 1.425
batch start
#iterations: 301
currently lose_sum: 95.4689330458641
time_elpased: 1.474
batch start
#iterations: 302
currently lose_sum: 95.40374529361725
time_elpased: 1.415
batch start
#iterations: 303
currently lose_sum: 95.68257093429565
time_elpased: 1.458
batch start
#iterations: 304
currently lose_sum: 95.17466908693314
time_elpased: 1.435
batch start
#iterations: 305
currently lose_sum: 94.88285058736801
time_elpased: 1.435
batch start
#iterations: 306
currently lose_sum: 95.03708183765411
time_elpased: 1.436
batch start
#iterations: 307
currently lose_sum: 95.45477795600891
time_elpased: 1.453
batch start
#iterations: 308
currently lose_sum: 95.23838597536087
time_elpased: 1.428
batch start
#iterations: 309
currently lose_sum: 95.30993032455444
time_elpased: 1.465
batch start
#iterations: 310
currently lose_sum: 95.3281432390213
time_elpased: 1.474
batch start
#iterations: 311
currently lose_sum: 95.04786813259125
time_elpased: 1.446
batch start
#iterations: 312
currently lose_sum: 95.51392215490341
time_elpased: 1.439
batch start
#iterations: 313
currently lose_sum: 94.95294231176376
time_elpased: 1.49
batch start
#iterations: 314
currently lose_sum: 95.08542346954346
time_elpased: 1.443
batch start
#iterations: 315
currently lose_sum: 94.94308859109879
time_elpased: 1.486
batch start
#iterations: 316
currently lose_sum: 94.95944929122925
time_elpased: 1.432
batch start
#iterations: 317
currently lose_sum: 95.10486233234406
time_elpased: 1.431
batch start
#iterations: 318
currently lose_sum: 95.2751402258873
time_elpased: 1.419
batch start
#iterations: 319
currently lose_sum: 95.25257581472397
time_elpased: 1.45
start validation test
0.56587628866
0.619796484736
0.344756612123
0.443063086893
0.566264498357
67.574
batch start
#iterations: 320
currently lose_sum: 95.0414999127388
time_elpased: 1.478
batch start
#iterations: 321
currently lose_sum: 95.2743730545044
time_elpased: 1.431
batch start
#iterations: 322
currently lose_sum: 94.77212828397751
time_elpased: 1.46
batch start
#iterations: 323
currently lose_sum: 95.63123828172684
time_elpased: 1.439
batch start
#iterations: 324
currently lose_sum: 95.13331079483032
time_elpased: 1.47
batch start
#iterations: 325
currently lose_sum: 95.37053310871124
time_elpased: 1.447
batch start
#iterations: 326
currently lose_sum: 94.87245506048203
time_elpased: 1.454
batch start
#iterations: 327
currently lose_sum: 95.27134919166565
time_elpased: 1.498
batch start
#iterations: 328
currently lose_sum: 95.29300636053085
time_elpased: 1.479
batch start
#iterations: 329
currently lose_sum: 95.17792665958405
time_elpased: 1.476
batch start
#iterations: 330
currently lose_sum: 95.29019689559937
time_elpased: 1.424
batch start
#iterations: 331
currently lose_sum: 94.89859622716904
time_elpased: 1.436
batch start
#iterations: 332
currently lose_sum: 94.85503047704697
time_elpased: 1.445
batch start
#iterations: 333
currently lose_sum: 94.88374257087708
time_elpased: 1.451
batch start
#iterations: 334
currently lose_sum: 95.2932876944542
time_elpased: 1.457
batch start
#iterations: 335
currently lose_sum: 95.08412206172943
time_elpased: 1.461
batch start
#iterations: 336
currently lose_sum: 95.12942379713058
time_elpased: 1.451
batch start
#iterations: 337
currently lose_sum: 95.1303049325943
time_elpased: 1.451
batch start
#iterations: 338
currently lose_sum: 94.93822473287582
time_elpased: 1.499
batch start
#iterations: 339
currently lose_sum: 95.16399383544922
time_elpased: 1.467
start validation test
0.566597938144
0.597381342062
0.41319337244
0.488502250882
0.56686726352
67.200
batch start
#iterations: 340
currently lose_sum: 94.77953445911407
time_elpased: 1.458
batch start
#iterations: 341
currently lose_sum: 95.06076318025589
time_elpased: 1.438
batch start
#iterations: 342
currently lose_sum: 95.4458435177803
time_elpased: 1.424
batch start
#iterations: 343
currently lose_sum: 94.5934510231018
time_elpased: 1.474
batch start
#iterations: 344
currently lose_sum: 94.96767085790634
time_elpased: 1.423
batch start
#iterations: 345
currently lose_sum: 95.0860720872879
time_elpased: 1.438
batch start
#iterations: 346
currently lose_sum: 95.05739390850067
time_elpased: 1.42
batch start
#iterations: 347
currently lose_sum: 94.767946600914
time_elpased: 1.446
batch start
#iterations: 348
currently lose_sum: 94.86940622329712
time_elpased: 1.421
batch start
#iterations: 349
currently lose_sum: 94.98031163215637
time_elpased: 1.459
batch start
#iterations: 350
currently lose_sum: 95.04623931646347
time_elpased: 1.42
batch start
#iterations: 351
currently lose_sum: 95.02694940567017
time_elpased: 1.479
batch start
#iterations: 352
currently lose_sum: 94.80939537286758
time_elpased: 1.429
batch start
#iterations: 353
currently lose_sum: 95.07294046878815
time_elpased: 1.491
batch start
#iterations: 354
currently lose_sum: 94.79587143659592
time_elpased: 1.431
batch start
#iterations: 355
currently lose_sum: 94.75009214878082
time_elpased: 1.46
batch start
#iterations: 356
currently lose_sum: 94.98924684524536
time_elpased: 1.418
batch start
#iterations: 357
currently lose_sum: 94.86726295948029
time_elpased: 1.426
batch start
#iterations: 358
currently lose_sum: 94.70183080434799
time_elpased: 1.434
batch start
#iterations: 359
currently lose_sum: 95.16203022003174
time_elpased: 1.428
start validation test
0.558041237113
0.62319465402
0.297519810641
0.402758428532
0.55849862266
68.297
batch start
#iterations: 360
currently lose_sum: 95.15732818841934
time_elpased: 1.443
batch start
#iterations: 361
currently lose_sum: 94.8942745923996
time_elpased: 1.45
batch start
#iterations: 362
currently lose_sum: 95.13462936878204
time_elpased: 1.449
batch start
#iterations: 363
currently lose_sum: 94.71579712629318
time_elpased: 1.471
batch start
#iterations: 364
currently lose_sum: 94.5614800453186
time_elpased: 1.424
batch start
#iterations: 365
currently lose_sum: 94.76472568511963
time_elpased: 1.457
batch start
#iterations: 366
currently lose_sum: 94.63427740335464
time_elpased: 1.425
batch start
#iterations: 367
currently lose_sum: 94.77532309293747
time_elpased: 1.448
batch start
#iterations: 368
currently lose_sum: 94.80129301548004
time_elpased: 1.428
batch start
#iterations: 369
currently lose_sum: 94.92988872528076
time_elpased: 1.456
batch start
#iterations: 370
currently lose_sum: 94.60796630382538
time_elpased: 1.43
batch start
#iterations: 371
currently lose_sum: 95.09138280153275
time_elpased: 1.441
batch start
#iterations: 372
currently lose_sum: 94.30724054574966
time_elpased: 1.45
batch start
#iterations: 373
currently lose_sum: 94.88356161117554
time_elpased: 1.446
batch start
#iterations: 374
currently lose_sum: 94.59885174036026
time_elpased: 1.451
batch start
#iterations: 375
currently lose_sum: 94.422423183918
time_elpased: 1.456
batch start
#iterations: 376
currently lose_sum: 94.69680374860764
time_elpased: 1.433
batch start
#iterations: 377
currently lose_sum: 94.7775205373764
time_elpased: 1.462
batch start
#iterations: 378
currently lose_sum: 95.10245031118393
time_elpased: 1.45
batch start
#iterations: 379
currently lose_sum: 94.29381769895554
time_elpased: 1.488
start validation test
0.567113402062
0.59987884295
0.407636101677
0.485416666667
0.56739338906
67.457
batch start
#iterations: 380
currently lose_sum: 94.5821772813797
time_elpased: 1.461
batch start
#iterations: 381
currently lose_sum: 94.61397385597229
time_elpased: 1.42
batch start
#iterations: 382
currently lose_sum: 94.54067742824554
time_elpased: 1.412
batch start
#iterations: 383
currently lose_sum: 94.44630700349808
time_elpased: 1.5
batch start
#iterations: 384
currently lose_sum: 94.8351075053215
time_elpased: 1.467
batch start
#iterations: 385
currently lose_sum: 95.02211260795593
time_elpased: 1.472
batch start
#iterations: 386
currently lose_sum: 94.7187831401825
time_elpased: 1.416
batch start
#iterations: 387
currently lose_sum: 94.74706143140793
time_elpased: 1.454
batch start
#iterations: 388
currently lose_sum: 94.59854525327682
time_elpased: 1.448
batch start
#iterations: 389
currently lose_sum: 94.70645505189896
time_elpased: 1.518
batch start
#iterations: 390
currently lose_sum: 94.58709043264389
time_elpased: 1.428
batch start
#iterations: 391
currently lose_sum: 94.99300736188889
time_elpased: 1.459
batch start
#iterations: 392
currently lose_sum: 94.55086547136307
time_elpased: 1.435
batch start
#iterations: 393
currently lose_sum: 94.66387194395065
time_elpased: 1.467
batch start
#iterations: 394
currently lose_sum: 94.54984605312347
time_elpased: 1.435
batch start
#iterations: 395
currently lose_sum: 94.22490763664246
time_elpased: 1.464
batch start
#iterations: 396
currently lose_sum: 94.22646468877792
time_elpased: 1.418
batch start
#iterations: 397
currently lose_sum: 94.81730425357819
time_elpased: 1.454
batch start
#iterations: 398
currently lose_sum: 94.51468050479889
time_elpased: 1.499
batch start
#iterations: 399
currently lose_sum: 94.46887856721878
time_elpased: 1.462
start validation test
0.565103092784
0.629922858303
0.319337244005
0.423820255412
0.565534572638
68.392
acc: 0.587
pre: 0.612
rec: 0.480
F1: 0.538
auc: 0.613
